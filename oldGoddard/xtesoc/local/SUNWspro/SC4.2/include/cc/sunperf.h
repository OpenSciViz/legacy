/*
 * Copyright 05/08/1996 Sun Microsystems, Inc.  All Rights Reserved.
 */

#ifndef _SUNPERF_H
#define _SUNPERF_H

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _SUNPERF_COMPLEX
#define _SUNPERF_COMPLEX
typedef struct {
	float r;
	float i;
} complex;
typedef struct {
	double r;
	double i;
} doublecomplex;
#endif	/* !defined(_SUNPERF_COMPLEX) */

extern int allocate_int_workspace (int *workspace, int length);

extern int allocate_workspace(char * workspace, int length);

extern int allocate_workspace_double (double *workspace, int length);

extern float work_zero_float();

extern double work_zero_double_float();

extern complex *work_zero_complex();

extern doublecomplex *work_zero_double_complex();

extern void caxpy(int n, complex *ca, complex *cx, int 
 incx, complex *cy, int incy);

extern void ccopy(int n, complex *cx, int incx, complex *
 cy, int incy);

extern complex *cdotc(int n, complex *cx, int 
 incx, complex *cy, int incy);

extern complex *cdotu(int n, complex *cx, int 
 incx, complex *cy, int incy);

extern void cgbmv(char trans, int m, int n, int kl, 
 int ku, complex *alpha, complex *a, int lda, complex *x, 
 int incx, complex *beta, complex *y, int incy);

extern void cgemm(char transa, char transb, int m, int 
 n, int k, complex *alpha, complex *a, int lda, complex *b, 
 int ldb, complex *beta, complex *c, int ldc);

extern void cgemv(char trans, int m, int n, complex 
 *alpha, complex *a, int lda, complex *x, int incx, complex 
 *beta, complex *y, int incy);

extern void cgerc(int m, int n, complex *alpha, complex *
 x, int incx, complex *y, int incy, complex *a, int lda);

extern void cgeru(int m, int n, complex *alpha, complex *
 x, int incx, complex *y, int incy, complex *a, int lda);

extern void chbmv(char uplo, int n, int k, complex *
 alpha, complex *a, int lda, complex *x, int incx, complex *
 beta, complex *y, int incy);

extern void chemm(char side, char uplo, int m, int n, 
 complex *alpha, complex *a, int lda, complex *b, int ldb, 
 complex *beta, complex *c, int ldc);

extern void chemv(char uplo, int n, complex *alpha, complex *
 a, int lda, complex *x, int incx, complex *beta, complex *y,
  int incy);

extern void cher(char uplo, int n, float alpha, complex *x, 
 int incx, complex *a, int lda);

extern void cher2(char uplo, int n, complex *alpha, complex *
 x, int incx, complex *y, int incy, complex *a, int lda);

extern void cher2k(char uplo, char trans, int n, int k, 
 complex *alpha, complex *a, int lda, complex *b, int ldb, 
 float beta, complex *c, int ldc);

extern void cherk(char uplo, char trans, int n, int k, 
 float alpha, complex *a, int lda, float beta, complex *c, 
 int ldc);

extern void chpmv(char uplo, int n, complex *alpha, complex *
 ap, complex *x, int incx, complex *beta, complex *y, int 
 incy);

extern void chpr(char uplo, int n, float alpha, complex *x, 
 int incx, complex *ap);

extern void chpr2(char uplo, int n, complex *alpha, complex *
 x, int incx, complex *y, int incy, complex *ap);

extern void crotg(complex *ca, complex *cb, float *c, complex *s);

extern void cscal(int n, complex *ca, complex *cx, int 
 incx);

extern void csscal(int n, float sa, complex *cx, int incx);

extern void cswap(int n, complex *cx, int incx, complex *
 cy, int incy);

extern void csymm(char side, char uplo, int m, int n, 
 complex *alpha, complex *a, int lda, complex *b, int ldb, 
 complex *beta, complex *c, int ldc);

extern void csyr2k(char uplo, char trans, int n, int k, 
 complex *alpha, complex *a, int lda, complex *b, int ldb, 
 complex *beta, complex *c, int ldc);

extern void csyrk(char uplo, char trans, int n, int k, 
 complex *alpha, complex *a, int lda, complex *beta, complex *c, 
 int ldc);

extern void ctbmv(char uplo, char trans, char diag, int n, 
 int k, complex *a, int lda, complex *x, int incx);

extern void ctbsv(char uplo, char trans, char diag, int n, 
 int k, complex *a, int lda, complex *x, int incx);

extern void ctpmv(char uplo, char trans, char diag, int n, 
 complex *ap, complex *x, int incx);

extern void ctpsv(char uplo, char trans, char diag, int n, 
 complex *ap, complex *x, int incx);

extern void ctrmm(char side, char uplo, char transa, char diag, 
 int m, int n, complex *alpha, complex *a, int lda, 
 complex *b, int ldb);

extern void ctrmv(char uplo, char trans, char diag, int n, 
 complex *a, int lda, complex *x, int incx);

extern void ctrsm ( char side, char uplo, char transa, char diag, int m, 
         int n, complex *alpha, complex *a, int lda, complex *b, int ldb );

extern void ctrsv(char uplo, char trans, char diag, int n, 
 complex *a, int lda, complex *x, int incx);

extern double dasum(int n, double *dx, int incx);

extern void daxpy(int n, double da, double *dx, 
 int incx, double *dy, int incy);

extern double dcabs1(doublecomplex *z);

extern void dcopy(int n, double *dx, int incx, 
 double *dy, int incy);

extern double ddot(int n, double *dx, int incx, double *dy, 
 int incy);

extern void dgbmv(char trans, int m, int n, int kl, 
 int ku, double alpha, double *a, int lda, 
 double *x, int incx, double beta, double *y, 
 int incy);

extern void dgemm(char transa, char transb, int m, int 
 n, int k, double alpha, double *a, int lda, 
 double *b, int ldb, double beta, double *c, int 
 ldc);

extern void dgemv(char trans, int m, int n, double 
 alpha, double *a, int lda, double *x, int incx, 
 double beta, double *y, int incy);

extern void dger(int m, int n, double alpha, 
 double *x, int incx, double *y, int incy, 
 double *a, int lda);

extern double dnrm2(int n, double *x, int incx);

extern double dqdota(int n, double b, long double *c, double *dx, 
 int incx, double *dy, int incy);

extern double dqdoti(int n, double b, long double *c, double *dx,
 int incx, double *dy, int incy);

extern void drot(int n, double *dx, int incx, 
 double *dy, int incy, double c, double s);

extern void drotg(double *da, double *db, double *c, 
 double *s);

extern void drotm(int n, double *dx, int incx, 
 double *dy, int incy, double *dparam);

extern void drotmg_(double *dd1, double *dd2, double *db1,
 double *db2, double *dparam);

extern void dsbmv(char uplo, int n, int k, double 
 alpha, double *a, int lda, double *x, int incx, 
 double beta, double *y, int incy);

extern void dscal(int n, double da, double *dx, 
 int incx);

extern double dsdot(int n, float *dx, int incx, float *dy,
 int incy);

extern void dspmv(char uplo, int n, double alpha, 
 double *ap, double *x, int incx, double beta, 
 double *y, int incy);

extern void dspr(char uplo, int n, double alpha, 
 double *x, int incx, double *ap);

extern void dspr2(char uplo, int n, double alpha, 
 double *x, int incx, double *y, int incy, 
 double *ap);

extern void dswap(int n, double *dx, int incx, 
 double *dy, int incy);

extern void dsymm(char side, char uplo, int m, int n, 
 double alpha, double *a, int lda, double *b, 
 int ldb, double beta, double *c, int ldc);

extern void dsymv(char uplo, int n, double alpha, 
 double *a, int lda, double *x, int incx, double 
 beta, double *y, int incy);

extern void dsyr(char uplo, int n, double alpha, 
 double *x, int incx, double *a, int lda);

extern void dsyr2(char uplo, int n, double alpha, 
 double *x, int incx, double *y, int incy, 
 double *a, int lda);

extern void dsyr2k(char uplo, char trans, int n, int k, 
 double alpha, double *a, int lda, double *b, 
 int ldb, double beta, double *c, int ldc);

extern void dsyrk(char uplo, char trans, int n, int k, 
 double alpha, double *a, int lda, double beta, 
 double *c, int ldc);

extern void dtbmv(char uplo, char trans, char diag, int n, 
 int k, double *a, int lda, double *x, int incx);

extern void dtbsv(char uplo, char trans, char diag, int n, 
 int k, double *a, int lda, double *x, int incx);

extern void dtpmv(char uplo, char trans, char diag, int n, 
 double *ap, double *x, int incx);

extern void dtpsv(char uplo, char trans, char diag, int n, 
 double *ap, double *x, int incx);

extern void dtrmm(char side, char uplo, char transa, char diag, 
 int m, int n, double alpha, double *a, int 
 lda, double *b, int ldb);

extern void dtrmv(char uplo, char trans, char diag, int n, 
 double *a, int lda, double *x, int incx);

extern void dtrsm(char side, char uplo, char transa, char diag, 
 int m, int n, double alpha, double *a, int 
 lda, double *b, int ldb);

extern void dtrsv(char uplo, char trans, char diag, int n, 
 double *a, int lda, double *x, int incx);

extern double dzasum(int n, doublecomplex *zx, int incx);

extern double dznrm2(int n, doublecomplex *x, int incx);

extern int icamax(int n, complex *cx, int incx);

extern int idamax(int n, double *dx, int incx);

extern int isamax(int n, float *sx, int incx);

extern int izamax(int n, doublecomplex *zx, int incx);

extern float sasum(int n, float *sx, int incx);

extern void saxpy(int n, float sa, float *sx, int incx, 
 float *sy, int incy);

extern float scasum(int n, complex *cx, int incx);

extern float scnrm2(int n, complex *x, int incx);

extern void scopy(int n, float *sx, int incx, float *sy, 
 int incy);

extern float sdot(int n, float *sx, int incx, float *sy, int incy);

extern float sdsdot(int n, float b, float *dx, int incx, float *dy,
 int incy);

extern void sgbmv(char trans, int m, int n, int kl, 
 int ku, float alpha, float *a, int lda, float *x, int 
 incx, float beta, float *y, int incy);

extern void sgemm(char transa, char transb, int m, int 
 n, int k, float alpha, float *a, int lda, float *b, int 
 ldb, float beta, float *c, int ldc);

extern void sgemv(char trans, int m, int n, float alpha, 
 float *a, int lda, float *x, int incx, float beta, float *y, 
 int incy);

extern void sger(int m, int n, float alpha, float *x, 
 int incx, float *y, int incy, float *a, int lda);

extern float snrm2(int n, float *x, int incx);

extern void srot(int n, float *sx, int incx, float *sy, 
 int incy, float c, float s);

extern void srotg(float *sa, float *sb, float *c, float *s);

extern void srotm(int n, float *dx, int incx,
 float *dy, int incy, float *sparam);

extern void srotmg(float *dd1, float *dd2, float *db1,
 float *db2, float *sparam);

extern void ssbmv(char uplo, int n, int k, float alpha, 
 float *a, int lda, float *x, int incx, float beta, float *y, 
 int incy);

extern void sscal(int n, float sa, float *sx, int incx);

extern void sspmv(char uplo, int n, float alpha, float *ap, 
 float *x, int incx, float beta, float *y, int incy);

extern void sspr(char uplo, int n, float alpha, float *x, 
 int incx, float *ap);

extern void sspr2(char uplo, int n, float alpha, float *x, 
 int incx, float *y, int incy, float *ap);

extern void sswap(int n, float *sx, int incx, float *sy, 
 int incy);

extern void ssymm(char side, char uplo, int m, int n, 
 float alpha, float *a, int lda, float *b, int ldb, float beta,
  float *c, int ldc);

extern void ssymv(char uplo, int n, float alpha, float *a, 
 int lda, float *x, int incx, float beta, float *y, int 
 incy);

extern void ssyr(char uplo, int n, float alpha, float *x, 
 int incx, float *a, int lda);

extern void ssyr2(char uplo, int n, float alpha, float *x, 
 int incx, float *y, int incy, float *a, int lda);

extern void ssyr2k(char uplo, char trans, int n, int k, 
 float alpha, float *a, int lda, float *b, int ldb, float beta,
  float *c, int ldc);

extern void ssyrk(char uplo, char trans, int n, int k, 
 float alpha, float *a, int lda, float beta, float *c, int ldc);

extern void stbmv(char uplo, char trans, char diag, int n, 
 int k, float *a, int lda, float *x, int incx);

extern void stbsv(char uplo, char trans, char diag, int n, 
 int k, float *a, int lda, float *x, int incx);

extern void stpmv(char uplo, char trans, char diag, int n, 
 float *ap, float *x, int incx);

extern void stpsv(char uplo, char trans, char diag, int n, 
 float *ap, float *x, int incx);

extern void strmm(char side, char uplo, char transa, char diag, 
 int m, int n, float alpha, float *a, int lda, float *b, 
 int ldb);

extern void strmv(char uplo, char trans, char diag, int n, 
 float *a, int lda, float *x, int incx);

extern void strsm(char side, char uplo, char transa, char diag, 
 int m, int n, float alpha, float *a, int lda, float *b, 
 int ldb);

extern void strsv(char uplo, char trans, char diag, int n, 
 float *a, int lda, float *x, int incx);

extern void xerbla(char *srname, int *info);

extern void zaxpy(int n, doublecomplex *za, doublecomplex *zx, 
 int incx, doublecomplex *zy, int incy);

extern void zcopy(int n, doublecomplex *zx, int incx, 
 doublecomplex *zy, int incy);

extern doublecomplex *zdotc(int n, doublecomplex *cx, int 
 incx, doublecomplex *cy, int incy);

extern doublecomplex *zdotu(int n, doublecomplex *cx, int 
 incx, doublecomplex *cy, int incy);

extern void zdscal(int n, double da, doublecomplex *zx, 
 int incx);

extern void zgbmv(char trans, int m, int n, int kl, 
 int ku, doublecomplex *alpha, doublecomplex *a, int lda, 
 doublecomplex *x, int incx, doublecomplex *beta, doublecomplex *
 y, int incy);

extern void zgemm(char transa, char transb, int m, int 
 n, int k, doublecomplex *alpha, doublecomplex *a, int lda, 
 doublecomplex *b, int ldb, doublecomplex *beta, doublecomplex *c,
  int ldc);

extern void zgemv(char trans, int m, int n, 
 doublecomplex *alpha, doublecomplex *a, int lda, doublecomplex *
 x, int incx, doublecomplex *beta, doublecomplex *y, int 
 incy);

extern void zgerc(int m, int n, doublecomplex *alpha, 
 doublecomplex *x, int incx, doublecomplex *y, int incy, 
 doublecomplex *a, int lda);

extern void zgeru(int m, int n, doublecomplex *alpha, 
 doublecomplex *x, int incx, doublecomplex *y, int incy, 
 doublecomplex *a, int lda);

extern void zhbmv(char uplo, int n, int k, doublecomplex 
 *alpha, doublecomplex *a, int lda, doublecomplex *x, int 
 incx, doublecomplex *beta, doublecomplex *y, int incy);

extern void zhemm(char side, char uplo, int m, int n, 
 doublecomplex *alpha, doublecomplex *a, int lda, doublecomplex *
 b, int ldb, doublecomplex *beta, doublecomplex *c, int ldc);

extern void zhemv(char uplo, int n, doublecomplex *alpha, 
 doublecomplex *a, int lda, doublecomplex *x, int incx, 
 doublecomplex *beta, doublecomplex *y, int incy);

extern void zher(char uplo, int n, double alpha, 
 doublecomplex *x, int incx, doublecomplex *a, int lda);

extern void zher2(char uplo, int n, doublecomplex *alpha, 
 doublecomplex *x, int incx, doublecomplex *y, int incy, 
 doublecomplex *a, int lda);

extern void zher2k(char uplo, char trans, int n, int k, 
 doublecomplex *alpha, doublecomplex *a, int lda, doublecomplex *
 b, int ldb, double beta, doublecomplex *c, int ldc);

extern void zherk(char uplo, char trans, int n, int k, 
 double alpha, doublecomplex *a, int lda, double beta, 
 doublecomplex *c, int ldc);

extern void zhpmv(char uplo, int n, doublecomplex *alpha, 
 doublecomplex *ap, doublecomplex *x, int incx, doublecomplex *
 beta, doublecomplex *y, int incy);

extern void zhpr(char uplo, int n, double *alpha, 
 doublecomplex *x, int incx, doublecomplex *ap);

extern void zhpr2(char uplo, int n, doublecomplex *alpha, 
 doublecomplex *x, int incx, doublecomplex *y, int incy, 
 doublecomplex *ap);

extern void zrotg(doublecomplex *ca, doublecomplex *cb, double *
 c, doublecomplex *s);

extern void zscal(int n, doublecomplex *za, doublecomplex *zx, 
 int incx);

extern void zswap(int n, doublecomplex *zx, int incx, 
 doublecomplex *zy, int incy);

extern void zsymm(char side, char uplo, int m, int n, 
 doublecomplex *alpha, doublecomplex *a, int lda, doublecomplex *
 b, int ldb, doublecomplex *beta, doublecomplex *c, int ldc);

extern void zsyr2k(char uplo, char trans, int n, int k, 
 doublecomplex *alpha, doublecomplex *a, int lda, doublecomplex *
 b, int ldb, doublecomplex *beta, doublecomplex *c, int ldc);

extern void zsyrk(char uplo, char trans, int n, int k, 
 doublecomplex *alpha, doublecomplex *a, int lda, doublecomplex * 
 beta, doublecomplex *c, int ldc);

extern void ztbmv(char uplo, char trans, char diag, int n, 
 int k, doublecomplex *a, int lda, doublecomplex *x, int 
        incx);

extern void ztbsv(char uplo, char trans, char diag, int n, 
 int k, doublecomplex *a, int lda, doublecomplex *x, int 
 incx);

extern void ztpmv(char uplo, char trans, char diag, int n, 
 doublecomplex *ap, doublecomplex *x, int incx);

extern void ztpsv(char uplo, char trans, char diag, int n, 
 doublecomplex *ap, doublecomplex *x, int incx);

extern void ztrmm(char side, char uplo, char transa, char diag, 
 int m, int n, doublecomplex *alpha, doublecomplex *a, 
 int lda, doublecomplex *b, int ldb);

extern void ztrmv(char uplo, char trans, char diag, int n, 
 doublecomplex *a, int lda, doublecomplex *x, int incx);

extern void ztrsm ( char side, char uplo, char transa, char diag, int m, 
         int n, doublecomplex *alpha, doublecomplex *a, int lda,
         doublecomplex *b, int ldb );

extern void ztrsv(char uplo, char trans, char diag, int n, 
 doublecomplex *a, int lda, doublecomplex *x, int incx);

extern void cfftb (int n, complex *c, complex *wsave);

extern void cfftf (int n, complex *c, complex *wsave);

extern void cffti (int n, complex *wsave);

extern void cosqb (int n,float *x,float *wsave);

extern void cosqf (int n,float *x,float *wsave);

extern void cosqi (int n,float *wsave);

extern void cost (int n,float *x,float *wsave);

extern void costi (int n,float *wsave);

extern void dcosqb (int n,double *x,double *wsave);

extern void dcosqf (int n,double *x,double *wsave);

extern void dcosqi (int n,double *wsave);

extern void dcost (int n,double *x,double *wsave);

extern void dcosti (int n,double *wsave);

extern void dezftb (int n,double *r,double *azero,double *a,double *b,double *wsave);

extern void dezfftf (int n,double *r,double *azero,double *a,double *b,double *wsave);

extern void dezfti (int n,double *wsave);

extern void dfftb (int n,double *r,double *wsave);

extern void dfftf (int n,double *r,double *wsave);

extern void dffti (int n,double *wsave);

extern void dsinqb (int n,double *x,double *wsave);

extern void dsinqf (int n,double *x,double *wsave);

extern void dsinqi (int n,double *wsave);

extern void dsint (int n,double *x,double *wsave);

extern void dsinti (int n,double *wsave);

extern void ezfftb (int n,float *r,float *azero,float *a,float *b,float *wsave);

extern void ezfftf (int n,float *r,float *azero,float *a,float *b,float *wsave);

extern void ezffti (int n,float *wsave);

extern void rfftb (int n,float *r,float *wsave);

extern void rfftf (int n,float *r,float *wsave);

extern void rffti (int n,float *wsave);

extern void sinqb (int n,float *x,float *wsave);

extern void sinqf (int n,float *x,float *wsave);

extern void sinqi (int n,float *wsave);

extern void sint (int n,float *x,float *wsave);

extern void sinti (int n,float *wsave);

extern void zfftb (int n,doublecomplex *c,doublecomplex *wsave);

extern void zfftf (int n,doublecomplex *c,doublecomplex *wsave);

extern void zffti (int n,doublecomplex *wsave);

extern void cchdc(complex *a,int lda,int p,int *jpvt,int job,int *info);

extern void cchdd(complex *r,int ldr,int p,complex *x,complex *z,int ldz,int nz,complex *y,float *rho,
 float *c,complex *s,int *info);

extern void cchex(complex*r,int ldr,int p,int k,int l,complex *z,int ldz,int nz,float *c,complex *s,int job);

extern void cchud(complex *r,int ldr,int p,complex *x,complex *z,int ldz,int nz,complex *y,float *rho,float *c,complex *s);

extern void cgbco(complex *abd,int lda,int n,int ml,int mu,int *ipvt,float *rcond);

extern void cgbdi(complex *abd,int lda,int n,int ml,int mu,int *ipvt,complex *det);

extern void cgbfa(complex *abd,int lda,int n,int ml,int mu,int *ipvt,int *info);

extern void cgbsl(complex *abd,int lda,int n,int ml,int mu,int *ipvt,complex *b,int job);

extern void cgeco(complex *a,int lda,int n,int *ipvt,float *rcond);

extern void cgedi(complex *a,int lda,int n,int *ipvt,complex *det,int job);

extern void cgefa(complex *a,int lda,int n,int *ipvt,int *info);

extern void cgesl(complex *a,int lda,int n,int *ipvt,complex *b,int job);

extern void cgtsl(int n,complex *c,complex *d,complex *e,complex *b,int *info);

extern void chico(complex *a,int lda,int n,int *ipivit, float *rcond);

extern void chidi(complex *a,int lda,int n,int *ipivot, float *det,int *inert, int job);

extern void chifa(complex *a,int lda,int n,int *ipivot, int *info);

extern void chisl(complex *a,int lda, int n,int *ipivot,complex *b);

extern void chpco(complex *a,int n,int *ipivit, float *rcond);

extern void chpdi(complex *a,int n,int *ipivot, float *det,int *inert, int job);

extern void chpfa(complex *a,int n,int *ipivot, int *info);

extern void chpsl(complex *a,int n,int *ipivot ,complex *b);

extern void cpbco(complex *abd,int lda,int n,int m,float *rcond,int *info);

extern void cpbdi(complex *abd,int lda,int n,int m,float *det);

extern void cpbfa(complex *abd,int lda,int n,int m,int *info);

extern void cpbsl(complex *abd,int lda,int n,int m,complex *b);

extern void cpoco(complex *a,int lda,int n,float *rcond,int *info);

extern void cpodi(complex *a,int lda,int n,float *det,int job);

extern void cpofa(complex *a,int lda,int n,int *info);

extern void cposl(complex *a,int lda,int n,complex *b);

extern void cppco(complex *ap,int n,float *rcond,int *info);

extern void cppdi(complex *ap,int n,float *det,int job);

extern void cppfa(complex *ap,int n,int *info);

extern void cppsl(complex *ap,int n,complex *b);

extern void cptsl(int n,complex *d,complex *e,complex *b);

extern void cqrdc(complex *x,int ldx,int n,int p,complex *qraux,int *jpvt,int job);

extern void cqrsl(complex *x,int ldx,int n,int k,complex *qraux,complex *y,complex *qy,complex *qty,complex *b,
 complex *rsd,complex *xb,int job,int *info);

extern void csico(complex *a,int lda,int n,int *kpvt,float *rcond);

extern void csidi(complex *a,int lda,int n,int *kpvt,complex *det,int job);

extern void csifa(complex *a,int lda,int n,int *kpvt,int *info);

extern void csisl(complex *a,int lda,int n,int *kpvt,complex *b);

extern void cspco(complex *ap,int n,int *kpvt,float *rcond);

extern void cspdi(complex *ap,int n,int *kpvt,complex *det,int job);

extern void cspfa(complex *ap,int n,int *kpvt,int *info);

extern void cspsl(complex *ap,int n,int *kpvt,complex *b);

extern void csvdc(complex *x,int ldx,int n,int p,complex *s,complex *e,complex *u,int ldu,complex *v,
 int ldv,int job,int *info);

extern void ctrco(complex *t,int ldt,int n,float *rcond,int job);

extern void ctrdi(complex *t,int ldt,int n,complex *det,int job,int *info);

extern void ctrsl(complex *t,int ldt,int n,complex *b,int job,int *info);

extern void dchdc(double *a,int lda,int p,int *jpvt,int job,int *info);

extern void dchdd(double *r,int ldr,int p,double *x,double *z,int ldz,int nz,double *y,
 double *rho,double *c,double *s,int *info);

extern void dchex(double*r,int ldr,int p,int k,int l,double *z,int ldz,int nz,
 double *c,double *s,int job);

extern void dchud(double *r,int ldr,int p,double *x,double *z,int ldz,int nz,
 double *y,double *rho,double *c,double *s);

extern void dgbco(double *abd,int lda,int n,int ml,int mu,int *ipvt,double *rcond);

extern void dgbdi(double *abd,int lda,int n,int ml,int mu,int *ipvt,double *det);

extern void dgbfa(double *abd,int lda,int n,int ml,int mu,int *ipvt,int *info);

extern void dgbsl(double *abd,int lda,int n,int ml,int mu,int *ipvt,double *b,int job);

extern void dgeco(double *a,int lda,int n,int *ipvt,double *rcond);

extern void dgedi(double *a,int lda,int n,int *ipvt,double *det,int job);

extern void dgefa(double *a,int lda,int n,int *ipvt,int *info);

extern void dgesl(double *a,int lda,int n,int *ipvt,double *b,int job);

extern void dgtsl(int n,double *c,double *d,double *e,double *b,int *info);

extern void dpbco(double *abd,int lda,int n,int m,double *rcond,int *info);

extern void dpbdi(double *abd,int lda,int n,int m,double *det);

extern void dpbfa(double *abd,int lda,int n,int m,int *info);

extern void dpbsl(double *abd,int lda,int n,int m,double *b);

extern void dpoco(double *a,int lda,int n,double *rcond,int *info);

extern void dpodi(double *a,int lda,int n,double *det,int job);

extern void dpofa(double *a,int lda,int n,int *info);

extern void dposl(double *a,int lda,int n,double *b);

extern void dppco(double *ap,int n,double *rcond,int *info);

extern void dppdi(double *ap,int n,double *det,int job);

extern void dppfa(double *ap,int n,int *info);

extern void dppsl(double *ap,int n,double *b);

extern void dptsl(int n,double *d,double *e,double *b);

extern void dqrdc(double *x,int ldx,int n,int p,double *qraux,int *jpvt,int job);

extern void dqrsl(double *x,int ldx,int n,int k,double *qraux,double *y,double *qy,
 double *qty,double *b,double *rsd,double *xb,int job,int *info);

extern void dsico(double *a,int lda,int n,int *kpvt,double *rcond);

extern void dsidi(double *a,int lda,int n,int *kpvt,double *det,int *inert,int job);

extern void dsifa(double *a,int lda,int n,int *kpvt,int *info);

extern void dsisl(double *a,int lda,int n,int *kpvt,double *b);

extern void dspco(double *ap,int n,int *kpvt,double *rcond);

extern void dspdi(double *ap,int n,int *kpvt,double *det,int *inert,int job);

extern void dspfa(double *ap,int n,int *kpvt,int *info);

extern void dspsl(double *ap,int n,int *kpvt,double *b);

extern void dsvdc(double *x,int ldx,int n,int p,double *s,double *e,double *u,
 int ldu,double *v,int ldv,int job,int *info);

extern void dtrco(double *t,int ldt,int n,double *rcond,int job);

extern void dtrdi(double *t,int ldt,int n,double *det,int job,int *info);

extern void dtrsl(double *t,int ldt,int n,double *b,int job,int *info);

extern void schdc(float *a,int lda,int p,int *jpvt,int job,int *info);

extern void schdd(float *r,int ldr,int p,float *x,float *z,int ldz,int nz,float *y,float *rho,float *c,float *s,int *info);

extern void schex(float*r,int ldr,int p,int k,int l,float *z,int ldz,int nz,float *c,float *s,int job);

extern void schud(float *r,int ldr,int p,float *x,float *z,int ldz,int nz,float *y,float *rho,float *c,float *s);

extern void sgbco(float *abd,int lda,int n,int ml,int mu,int *ipvt,float *rcond);

extern void sgbdi(float *abd,int lda,int n,int ml,int mu,int *ipvt,float *det);

extern void sgbfa(float *abd,int lda,int n,int ml,int mu,int *ipvt,int *info);

extern void sgbsl(float *abd,int lda,int n,int ml,int mu,int *ipvt,float *b,int job);

extern void sgeco(float *a,int lda,int n,int *ipvt,float *rcond);

extern void sgedi(float *a,int lda,int n,int *ipvt,float *det,int job);

extern void sgefa(float *a,int lda,int n,int *ipvt,int *info);

extern void sgesl(float *a,int lda,int n,int *ipvt,float *b,int job);

extern void sgtsl(int n,float *c,float *d,float *e,float *b,int *info);

extern void spbco(float *abd,int lda,int n,int m,float *rcond,int *info);

extern void spbdi(float *abd,int lda,int n,int m,float *det);

extern void spbfa(float *abd,int lda,int n,int m,int *info);

extern void spbsl(float *abd,int lda,int n,int m,float *b);

extern void spoco(float *a,int lda,int n,float *rcond,int *info);

extern void spodi(float *a,int lda,int n,float *det,int job);

extern void spofa(float *a,int lda,int n,int *info);

extern void sposl(float *a,int lda,int n,float *b);

extern void sppco(float *ap,int n,float *rcond,int *info);

extern void sppdi(float *ap,int n,float *det,int job);

extern void sppfa(float *ap,int n,int *info);

extern void sppsl(float *ap,int n,float *b);

extern void sptsl(int n,float *d,float *e,float *b);

extern void sqrdc(float *x,int ldx,int n,int p,float *qraux,int *jpvt,int job);

extern void sqrsl(float *x,int ldx,int n,int k,float *qraux,float *y,float *qy,float *qty,float *b,float *rsd,
 float *xb,int job,int *info);

extern void ssico(float *a,int lda,int n,int *kpvt,float *rcond);

extern void ssidi(float *a,int lda,int n,int *kpvt,float *det,int *inert,int job);

extern void ssifa(float *a,int lda,int n,int *kpvt,int *info);

extern void ssisl(float *a,int lda,int n,int *kpvt,float *b);

extern void sspco(float *ap,int n,int *kpvt,float *rcond);

extern void sspdi(float *ap,int n,int *kpvt,float *det,int *inert,int job);

extern void sspfa(float *ap,int n,int *kpvt,int *info);

extern void sspsl(float *ap,int n,int *kpvt,float *b);

extern void ssvdc(float *x,int ldx,int n,int p,float *s,float *e,float *u,int ldu,float *v,
 int ldv,int job,int *info);

extern void strco(float *t,int ldt,int n,float *rcond,int job);

extern void strdi(float *t,int ldt,int n,float *det,int job,int *info);

extern void strsl(float *t,int ldt,int n,float *b,int job,int *info);

extern void zchdc(doublecomplex *a,int lda,int p,int *jpvt,int job,int *info);

extern void zchdd(doublecomplex *r,int ldr,int p,doublecomplex *x,doublecomplex *z,int ldz,
 int nz,doublecomplex *y,double *rho,double *c,doublecomplex *s,int *info);

extern void zchex(doublecomplex*r,int ldr,int p,int k,int l,doublecomplex *z,int ldz,
 int nz,double *c,doublecomplex *s,int job);

extern void zchud(doublecomplex *r,int ldr,int p,doublecomplex *x,doublecomplex *z,int ldz,
 int nz,doublecomplex *y,double *rho,double *c,doublecomplex *s);

extern void zgbco(doublecomplex *abd,int lda,int n,int ml,int mu,int *ipvt,double *rcond);

extern void zgbdi(doublecomplex *abd,int lda,int n,int ml,int mu,int *ipvt,doublecomplex *det);

extern void zgbfa(doublecomplex *abd,int lda,int n,int ml,int mu,int *ipvt,int *info);

extern void zgbsl(doublecomplex *abd,int lda,int n,int ml,int mu,int *ipvt,doublecomplex *b,int job);

extern void zgeco(doublecomplex *a,int lda,int n,int *ipvt,double *rcond);

extern void zgedi(doublecomplex *a,int lda,int n,int *ipvt,doublecomplex *det,int job);

extern void zgefa(doublecomplex *a,int lda,int n,int *ipvt,int *info);

extern void zgesl(doublecomplex *a,int lda,int n,int *ipvt,doublecomplex *b,int job);

extern void zgtsl(int n,doublecomplex *c,doublecomplex *d,doublecomplex *e,doublecomplex *b,int *info);

extern void zhico(doublecomplex *a,int lda,int n,int *ipivit, double *rcond);

extern void zhidi(doublecomplex *a,int lda,int n,int *ipivot, double *det,int *inert, int job);

extern void zhifa(doublecomplex *a,int lda,int n,int *ipivot, int *info);

extern void zhisl(doublecomplex *a,int lda, int n,int *ipivot,doublecomplex *b);

extern void zhpco(doublecomplex *a,int n,int *ipivit, double *rcond);

extern void zhpdi(doublecomplex *a,int n,int *ipivot, double *det,int *inert, int job);

extern void zhpfa(doublecomplex *a,int n,int *ipivot, int *info);

extern void zhpsl(doublecomplex *a,int n,int *ipivot ,doublecomplex *b);

extern void zpbco(doublecomplex *abd,int lda,int n,int m,double *rcond,int *info);

extern void zpbdi(doublecomplex *abd,int lda,int n,int m,double *det);

extern void zpbfa(doublecomplex *abd,int lda,int n,int m,int *info);

extern void zpbsl(doublecomplex *abd,int lda,int n,int m,doublecomplex *b);

extern void zpoco(doublecomplex *a,int lda,int n,double *rcond,int *info);

extern void zpodi(doublecomplex *a,int lda,int n,double *det,int job);

extern void zpofa(doublecomplex *a,int lda,int n,int *info);

extern void zposl(doublecomplex *a,int lda,int n,doublecomplex *b);

extern void zppco(doublecomplex *ap,int n,double *rcond,int *info);

extern void zppdi(doublecomplex *ap,int n,double *det,int job);

extern void zppfa(doublecomplex *ap,int n,int *info);

extern void zppsl(doublecomplex *ap,int n,doublecomplex *b);

extern void zptsl(int n,doublecomplex *d,doublecomplex *e,doublecomplex *b);

extern void zqrdc(doublecomplex *x,int ldx,int n,int p,doublecomplex *qraux,int *jpvt,int job);

extern void zqrsl(doublecomplex *x,int ldx,int n,int k,doublecomplex *qraux,doublecomplex *y,
 doublecomplex *qy,doublecomplex *qty,doublecomplex *b,doublecomplex *rsd,doublecomplex *xb,int job,int *info);

extern void zsico(doublecomplex *a,int lda,int n,int *kpvt,double *rcond);

extern void zsidi(doublecomplex *a,int lda,int n,int *kpvt,doublecomplex *det,int job);

extern void zsifa(doublecomplex *a,int lda,int n,int *kpvt,int *info);

extern void zsisl(doublecomplex *a,int lda,int n,int *kpvt,doublecomplex *b);

extern void zspco(doublecomplex *ap,int n,int *kpvt,double *rcond);

extern void zspdi(doublecomplex *ap,int n,int *kpvt,doublecomplex *det,int job);

extern void zspfa(doublecomplex *ap,int n,int *kpvt,int *info);

extern void zspsl(doublecomplex *ap,int n,int *kpvt,doublecomplex *b);

extern void zsvdc(doublecomplex *x,int ldx,int n,int p,doublecomplex *s,doublecomplex *e,
 doublecomplex *u,int ldu,doublecomplex *v,int ldv,int job,int *info);

extern void ztrco(doublecomplex *t,int ldt,int n,double *rcond,int job);

extern void ztrdi(doublecomplex *t,int ldt,int n,doublecomplex *det,int job,int *info);

extern void ztrsl(doublecomplex *t,int ldt,int n,doublecomplex *b,int job,int *info);

extern void vcosqb(int m,int n, float *x, int mdimx,float *wsave);


extern void vcosqf(int m,int n,float *x, int mdimx,float *wsave);


extern void vcosqi(int n,float *wsave);


extern void vcost(int m,int n,float *x, int mdimx,float *wsave);


extern void vcosti(int n,float *wsave);


extern void vdcosqb(int m,int n,double *x, int mdimx,double *wsave);


extern void vdcosqf(int m,int n,double *x, int mdimx,double *wsave);


extern void vdcosqi(int n,double *wsave);


extern void vdcost(int m,int n,double *x,int mdimx,double *wsave);


extern void vdcosti(int n,double *wsave);


extern void vdfftb(int m,int n,double *r, int mdimr,double *wsave);


extern void vdfftf (int m,int n,double *r, int mdimr,double *wsave);


extern void vdffti (int n,double *wsave);


extern void vdsinqb(int m,int n,double *x,int mdimx,double *wsave);


extern void vdsinqf(int m,int n,double *x, int mdimx,double *wsave);


extern void vdsinqi(int n,double *wsave);


extern void vdsint(int m,int n,double *x, int mdimx,double *wsave);


extern void vdsinti(int n,double *wsave);


extern void vrfftb(int m,int n,float *r,int mdimr,float *wsave);


extern void vrfftf (int m,int n,float *r, int mdimr,float *wsave);


extern void vrffti (int n,float *wsave);


extern void vsinqb(int m,int n,float *x, int mdimx,float *wsave);


extern void vsinqf(int m,int n,float *x,int mdimx,float *wsave);


extern void vsinqi(int n,float *wsave);


extern void vsint(int m,int n, float *x, int mdimx,float *wsave);


extern void vsinti(int n,float *wsave);


void cbdsqr(char uplo, int n, int ncvt, int nru, int ncc, float *d, float *e, complex *vt, int ldvt, 
 complex *u, int ldu, complex *c, int ldc, int *info);

void cgbbrd(char vect, int m, int n, int ncc,int kl, int ku, complex *ab, int ldab,
 float *d, float *e, complex *q, int ldq, complex *pt, int ldpt, complex *c, int ldc, int *info);

void cgbcon(char norm, int n, int kl, int ku,
  complex *ab, int ldab, int *ipiv, float anorm, float *rcond, 
 int *info);

void cgbequ(int m, int n, int kl, int ku,
  complex *ab, int ldab, float *r, float *c, float *rowcnd, float *colcnd, float *amax, int *info);

void cgbrfs(char trans, int n, int kl, int ku, int nrhs, complex *ab, int ldab, complex *afb, int 
 ldafb, int *ipiv, complex *b, int ldb, complex *x, int ldx, float *ferr, float *berr, int * info);

void cgbsv(int n, int kl, int ku, int nrhs, complex *ab,
           int ldab, int *ipiv, complex *b, int
           ldb, int *info);

void cgbsvx(char fact, char trans, int n, int kl,
  int ku, int nrhs, complex *ab, int ldab, complex *afb,
  int ldafb, int *ipiv, char *equed, float *r, float *c, 
 complex *b, int ldb, complex *x, int ldx, float *rcond, float 
 *ferr, float *berr, int *info);

void cgbtf2(int m, int n, int kl, int ku,
  complex *ab, int ldab, int *ipiv, int *info);

void cgbtrf(int m, int n, int kl, int ku,
   complex *ab, int ldab, int *ipiv, int *info);

void cgbtrs(char trans, int n, int kl, int ku, int nrhs, complex *ab, int ldab, int *ipiv, complex 
 *b, int ldb, int *info);

void cgebak(char job, char side, int n, int ilo, 
 int ihi, float *scale, int m, complex *v, int ldv, 
 int *info);

void cgebal(char job, int n, complex *a, int lda, 
 int *ilo, int *ihi, float *scale, int *info);

void cgebd2(int m, int n, complex *a, int lda,
  float *d, float *e, complex *tauq, complex *taup, int *info);

void cgebrd(int m, int n, complex *a, int lda,
  float *d, float *e, complex *tauq, complex *taup, int *info);

void cgecon(char norm, int n, complex *a, int lda,
  float anorm, float *rcond, int *info);

void cgeequ(int m, int n, complex *a, int lda,
  float *r, float *c, float *rowcnd, float *colcnd, float *amax, int * info);

void cgees(char jobvs, char sort, int (*select)(), int n, 
 complex *a, int lda, int *sdim, complex *w, complex *vs, 
 int ldvs, int *info);

void cgeesx(char jobvs, char sort, int (*select)(), char sense, int n, complex *a, int lda, int *sdim, complex *
 w, complex *vs, int ldvs, float *rconde, float *rcondv, int *info);

void cgeev(char jobvl, char jobvr, int n, complex *a, 
 int lda, complex *w, complex *vl, int ldvl, complex *vr, 
 int ldvr, int * info);

void cgeevx(char balanc, char jobvl, char jobvr, char sense, int n, complex *a, int lda, complex *w, complex *vl, 
 int ldvl, complex *vr, int ldvr, int *ilo, int *ihi,
  float *scale, float *abnrm, float *rconde, float *rcondv, int *info);

void cgegs(char jobvsl, char jobvsr, int n, complex * a, int lda, complex *b, int ldb, complex *alpha, complex *
 beta, complex *vsl, int ldvsl, complex *vsr, int ldvsr, 
 int *info);

void cgegv(char jobvl, char jobvr, int n, complex *a, 
 int lda, complex *b, int ldb, complex *alpha, complex *beta,
  complex *vl, int ldvl, complex *vr, int ldvr, int *info);

void cgehd2(int n, int ilo, int ihi, complex * a, int lda, complex *tau, int *info);

void cgehrd(int n, int ilo, int ihi, complex * a, int lda, complex *tau, int *info);

void cgelq2(int m, int n, complex *a, int lda,
  complex *tau, int *info);

void cgelqf(int m, int n, complex *a, int lda,
  complex *tau, int *info);

void cgels(char trans, int m, int n, int nrhs, complex *a, int lda, complex *b, int ldb, int *info);

void cgelss(int m, int n, int nrhs, complex *a, int lda, complex *b, int ldb, float *s, float rcond, 
 int *rank, int * info);

void cgelsx(int m, int n, int nrhs, complex *a, int lda, complex *b, int ldb, int *jpvt, float rcond,
  int *rank, int *info);

void cgeql2(int m, int n, complex *a, int lda,
  complex *tau, int *info);

void cgeqlf(int m, int n, complex *a, int lda,
  complex *tau, int *info);

void cgeqpf(int m, int n, complex *a, int lda,
  int *jpvt, complex *tau, int * info);

void cgeqr2(int m, int n, complex *a, int lda,
  complex *tau, int *info);

void cgeqrf(int m, int n, complex *a, int lda,
  complex *tau, int *info);

void cgerfs(char trans, int n, int nrhs, complex *a, int lda, complex *af, int ldaf, int *ipiv, complex *
        b, int ldb, complex *x, int ldx, float *ferr, float *berr, 
 int *info);

void cgerq2(int m, int n, complex *a, int lda,
  complex *tau, int *info);

void cgerqf(int m, int n, complex *a, int lda,
  complex *tau, int *info);

void cgesv(int n, int nrhs, complex *a, int lda, int *ipiv, complex *b, int ldb, int *info);

void cgesvd(char jobu, char jobvt, int m, int n, 
 complex *a, int lda, float *s, complex *u, int ldu, complex * vt, int ldvt, int *info);

void cgesvx(char fact, char trans, int n, int nrhs, complex *a, int lda, complex *af, int ldaf, int *
 ipiv, char *equed, float *r, float *c, complex *b, int ldb, 
 complex *x, int ldx, float *rcond, float *ferr, float *berr, 
 int *info);

void cgetf2(int m, int n, complex *a, int lda,
  int *ipiv, int *info);

void cgetrf(int m, int n, complex *a, int lda,
  int *ipiv, int *info);

void cgetri(int n, complex *a, int lda, int * ipiv, int *info);

void cgetrs(char trans, int n, int nrhs, complex * a, int lda, int *ipiv, complex *b, int ldb, int *
 info);

void cggbak(char job, char side, int n, int ilo, 
 int ihi, float *lscale, float *rscale, int m, complex *v, 
 int ldv, int *info);

void cggbal(char job, int n, complex *a, int lda, 
 complex *b, int ldb, int *ilo, int *ihi, float *lscale, 
 float *rscale, int *info);

void cggglm(int n, int m, int p, complex *a, 
 int lda, complex *b, int ldb, complex *d, complex *x, 
 complex *y, int *info);

void cgghrd(char compq, char compz, int n, int ilo, int ihi, complex *a, int lda, complex *b, int ldb,
  complex *q, int ldq, complex *z, int ldz, int *info);

void cgglse(int m, int n, int p, complex *a, 
 int lda, complex *b, int ldb, complex *c, complex *d, 
 complex *x, int *info);

void cggqrf(int n, int m, int p, complex *a, 
 int lda, complex *taua, complex *b, int ldb, complex *taub, 
 int *info);

void cggrqf(int m, int p, int n, complex *a, 
 int lda, complex *taua, complex *b, int ldb, complex *taub, 
 int *info);

void cggsvd(char jobu, char jobv, char jobq, int m, 
 int n, int p, int *k, int *l, complex *a, int lda, complex *b, int ldb, float *alpha, float *beta, complex *u, 
 int ldu, complex *v, int ldv, complex *q, int ldq, 
 int *info);

void cggsvp(char jobu, char jobv, char jobq, int m, 
 int p, int n, complex *a, int lda, complex *b, int 
 ldb, float tola, float tolb, int *k, int *l, complex *u, 
 int ldu, complex *v, int ldv, complex *q, int ldq, 
 complex *tau, int * info);

void cgtcon(char norm, int n, complex *dl, complex *d, 
 complex *du, complex *du2, int *ipiv, float anorm, float *rcond, 
 int *info);

void cgtrfs(char trans, int n, int nrhs, complex *dl, complex *d, complex *du, complex *dlf, complex *df, complex *duf, 
 complex *du2, int *ipiv, complex *b, int ldb, complex *x, 
 int ldx, float *ferr, float *berr, int *info);

void cgtsv(int n, int nrhs, complex *dl, complex * d, complex *du, complex *b, int ldb, int *info);

void cgtsvx(char fact, char trans, int n, int nrhs, complex *dl, complex *d, complex *du, complex *dlf, complex *df,
  complex *duf, complex *du2, int *ipiv, complex *b, int ldb, 
 complex *x, int ldx, float *rcond, float *ferr, float *berr, 
 int *info);

void cgttrf(int n, complex *dl, complex *d, complex *du,
  complex *du2, int *ipiv, int *info);

void cgttrs(char trans, int n, int nrhs, complex * dl, complex *d, complex *du, complex *du2, int *ipiv, complex *b, 
 int ldb, int *info);

void chbev(char jobz, char uplo, int n, int kd, 
 complex *ab, int ldab, float *w, complex *z, int ldz, 
 int *info);

void chbevd(char jobz, char uplo, int n, int kd, 
 complex *ab, int ldab, float *w, complex *z, int ldz, 
 int *info);

void chbevx(char jobz, char range, char uplo, int n, 
 int kd, complex *ab, int ldab, complex *q, int ldq, 
 float vl, float vu, int il, int iu, float abstol, int *m, float *w, complex *z, int ldz, int *ifail, int *info);

void chbgst(char vect, char uplo, int n, int ka, 
 int kb, complex *ab, int ldab, complex *bb, int ldbb, 
 complex *x, int ldx, int *info);

void chbgv(char jobz, char uplo, int n, int ka, 
 int kb, complex *ab, int ldab, complex *bb, int ldbb, 
 float *w, complex *z, int ldz, int *info);

void chbtrd(char vect, char uplo, int n, int kd, 
 complex *ab, int ldab, float *d, float *e, complex *q, int ldq, int *info);

void checon(char uplo, int n, complex *a, int lda,
  int *ipiv, float anorm, float *rcond, int * info);

void cheev(char jobz, char uplo, int n, complex *a, 
 int lda, float *w, int *info);

void cheevd(char jobz, char uplo, int n, complex *a, 
 int lda, float *w, int *info);

void cheevx(char jobz, char range, char uplo, int n, 
 complex *a, int lda, float vl, float vu, int il, int iu, float abstol, int *m, float *w, complex *z, int ldz, 
 int * ifail, int *info);

void chegs2(int itype, char uplo, int n, complex *a, int lda, complex *b, int ldb, int *info);

void chegst(int itype, char uplo, int n, complex * a, int lda, complex *b, int ldb, int *info);

void chegv(int itype, char jobz, char uplo, int n, complex *a, int lda, complex *b, int ldb, float *w, int *info);

void cherfs(char uplo, int n, int nrhs, complex * a, int lda, complex *af, int ldaf, int *ipiv, complex *
 b, int ldb, complex *x, int ldx, float *ferr, float *berr, 
 int *info);

void chesv(char uplo, int n, int nrhs, complex *a,
  int lda, int *ipiv, complex *b, int ldb, int *info);

void chesvx(char fact, char uplo, int n, int nrhs, complex *a, int lda, complex *af, int ldaf, int *
 ipiv, complex *b, int ldb, complex *x, int ldx, float *rcond,
  float *ferr, float *berr, int *info);

void chetd2(char uplo, int n, complex *a, int lda,
  float *d, float *e, complex *tau, int *info);

void chetf2(char uplo, int n, complex *a, int lda,
  int *ipiv, int *info);

void chetrd(char uplo, int n, complex *a, int lda,
  float *d, float *e, complex *tau, int *info);

void chetrf(char uplo, int n, complex *a, int lda,
  int *ipiv, int *info);

void chetri(char uplo, int n, complex *a, int lda,
  int *ipiv, int *info);

void chetrs(char uplo, int n, int nrhs, complex * a, int lda, int *ipiv, complex *b, int ldb, int *
 info);

void chgeqz(char job, char compq, char compz, int n, 
 int ilo, int ihi, complex *a, int lda, complex *b, 
 int ldb, complex *alpha, complex *beta, complex *q, int ldq,
  complex *z, int ldz, int *info);

void chpcon(char uplo, int n, complex *ap, int *ipiv, float anorm, float *rcond, int *info);

void chpev(char jobz, char uplo, int n, complex *ap, 
 float *w, complex *z, int ldz, int *info);

void chpevd(char jobz, char uplo, int n, complex *ap, 
 float *w, complex *z, int ldz, int *info);

void chpevx(char jobz, char range, char uplo, int n, 
 complex *ap, float vl, float vu, int il, int iu, float abstol, 
        int *m, float *w, complex *z, int ldz, int *ifail, int *info);

void chpgst(int itype, char uplo, int n, complex *ap, complex *bp, int *info);

void chpgv(int itype, char jobz, char uplo, int n, complex *ap, 
        complex *bp, float *w, complex *z, int ldz, 
 int *info);

void chprfs(char uplo, int n, int nrhs, complex *ap, complex *afp, 
         int *ipiv, complex *b, int ldb, complex *x,
  int ldx, float *ferr, float *berr, int *info);

void chpsv(char uplo, int n, int nrhs, complex *ap, int *ipiv, complex *b, int ldb, int *info);

void chpsvx(char fact, char uplo, int n, int nrhs, complex *ap, complex *afp, 
        int *ipiv, complex *b, int 
 ldb, complex *x, int ldx, float *rcond, float *ferr, float *berr, 
 int *info);

void chptrd(char uplo, int n, complex *ap, float *d, 
 float *e, complex *tau, int *info);

void chptrf(char uplo, int n, complex *ap, int *ipiv, int *info);

void chptri(char uplo, int n, complex *ap, int *ipiv, int *info);

void chptrs(char uplo, int n, int nrhs, complex *ap, int *ipiv, complex *b, int ldb, int *info);

void chsein(char side, char eigsrc, char initv, int * select, int n, complex *h, int ldh, complex *w, complex *vl,
  int ldvl, complex *vr, int ldvr, int mm, int *m, 
 int *ifaill, int *ifailr, int 
 *info);

void chseqr(char job, char compz, int n, int ilo,
  int ihi, complex *h, int ldh, complex *w, complex *z, 
 int ldz, int *info);

void clabrd(int m, int n, int nb, complex *a, 
 int lda, float *d, float *e, complex *tauq, complex *taup, complex 
 *x, int ldx, complex *y, int *ldy);

void clacgv(int n, complex *x, int incx);

void clacon(int n, complex *v, complex *x, float *est, 
 int *kase);

void clacpy(char uplo, int m, int n, complex *a, 
 int lda, complex *b, int ldb);

void clacrm(int m, int n, complex *a, int lda,
  float *b, int ldb, complex *c, int ldc);

void clacrt(int n, complex *cx, int incx, complex * cy, int incy, complex *c, complex *s);

extern complex *cladiv(complex *x, complex *y);

void claed0(int qsiz, int n, float *d, float *e, 
 complex *q, int ldq, complex *qstore, int ldqs, int *info);

void claed7(int n, int cutpnt, int qsiz, 
 int tlvls, int curlvl, int curpbm, float *d, complex *q,
  int ldq, float rho, int *indxq, float *qstore, int *qptr,
  int *prmptr, int *perm, int *givptr, int *givcol, 
 float *givnum, int * info);

void claed8(int *k, int n, int qsiz, complex * q, int ldq, float *d, float *rho, int cutpnt, float *z, float *
 dlamda, complex *q2, int ldq2, float *w, int *indxp, int * indx, int *indxq, int *perm, int *givptr, int *givcol,
  float *givnum, int *info);

void claein(int rightv, int noinit, int n, 
 complex *h, int ldh, complex *w, complex *v, complex *b, int 
 ldb, float eps3, float smlnum, int *info);

void claesy(complex *a, complex *b, complex *c, complex *rt1,
  complex *rt2, complex *evscal, complex *cs1, complex *sn1);

void claev2(complex *a, complex *b, complex *c, float *rt1, 
 float *rt2, float *cs1, complex *sn1);

void clags2(int upper, float a1, complex *a2, float a3, 
 float b1, complex *b2, float b3, float *csu, complex *snu, float *csv, 
 complex *snv, float *csq, complex *snq);

void clagtm(char trans, int n, int nrhs, float alpha, complex *dl, complex *d, complex *du, complex *x, int ldx,
  float beta, complex *b, int ldb);

void clahef(char uplo, int n, int nb, int *kb,
  complex *a, int lda, int *ipiv, complex *w, int ldw, 
 int *info);

void clahqr(int wantt, int wantz, int n, 
 int ilo, int ihi, complex *h, int ldh, complex *w, 
 int iloz, int ihiz, complex *z, int ldz, int *info);

void clahrd(int n, int k, int nb, complex *a, 
 int lda, complex *tau, complex *t, int ldt, complex *y, 
 int ldy);

void claic1(int job, int j, complex *x, float sest,
  complex *w, complex *gamma, float *sestpr, complex *s, complex *c);

extern float clangb(char norm, int n, int kl, int ku, complex *ab, int ldab);

extern float clange(char norm, int m, int n, complex *a, int lda);

extern float clangt(char norm, int n, complex *dl, complex *d, complex * du) ;

extern float clanhb(char norm, char uplo, int n, int k, complex *ab, int ldab);

extern float clanhe(char norm, char uplo, int n, complex *a, int lda);

extern float clanhp(char norm, char uplo, int n, complex *ap);

extern float clanhs(char norm, int n, complex *a, int lda);

extern float clanht(char norm, int n, float *d, complex *e);

extern float clansb(char norm, char uplo, int n, int k, complex *ab, int ldab);

extern float clansp(char norm, char uplo, int n, complex *ap);

extern double zlanst(char norm, int n, double *d, doublecomplex *e);

extern float clansy(char norm, char uplo, int n, complex *a, int lda);

extern float clantb(char norm, char uplo, char diag, int n, int k,
  complex *ab, int ldab);

extern float clantp(char norm, char uplo, char diag, int n, complex * ap);

extern float clantr(char norm, char uplo, char diag, int m, int n,
  complex *a, int lda);

void clapll(int n, complex *x, int incx, complex * y, int incy, float *ssmin);

void clapmt(int forwrd, int m, int n, complex 
 *x, int ldx, int *k);

void claqgb(int m, int n, int kl, int ku,
  complex *ab, int ldab, float *r, float *c, float *rowcnd, float * colcnd, float amax, char *equed);

void claqge(int m, int n, complex *a, int lda,
  float *r, float *c, float rowcnd, float colcnd, float amax, char * equed);

void claqhb(char uplo, int n, int kd, complex *ab,
  int ldab, float *s, float scond, float amax, char *equed);

void claqhe(char uplo, int n, complex *a, int lda,
  float *s, float scond, float amax, char *equed);

void claqhp(char uplo, int n, complex *ap, float *s, 
 float scond, float amax, char *equed);

void claqsb(char uplo, int n, int kd, complex *ab,
  int ldab, float *s, float scond, float amax, char *equed);

void claqsp(char uplo, int n, complex *ap, float *s, 
 float scond, float amax, char *equed);

void claqsy(char uplo, int n, complex *a, int lda,
  float *s, float scond, float amax, char *equed);

void clar2v(int n, complex *x, complex *y, complex *z, 
 int incx, float *c, complex *s, int incc);

void clarf(char side, int m, int n, complex *v, 
 int incv, complex *tau, complex *c, int ldc);

void clarfb(char side, char trans, char direct, char storev, int m, int n, int k, complex *v, int ldv, 
 complex *t, int ldt, complex *c, int ldc);

void clarfg(int n, complex *alpha, complex *x, int incx, complex *tau);

void clarft(char direct, char storev, int n, int k, complex *v, int ldv, complex *tau, complex *t, int ldt);

void clarfx(char side, int m, int n, complex *v, 
 complex *tau, complex *c, int ldc);

void clargv(int n, complex *x, int incx, complex * y, int incy, float *c, int incc);

void clarnv(int idist, int *iseed, int n, 
 complex *x);

void clartg(complex *f, complex *g, float *cs, complex *sn, 
 complex *r);

void clartv(int n, complex *x, int incx, complex * y, int incy, float *c, complex *s, int incc);

void clascl(char type, int kl, int ku, float cfrom, float cto, int m, int n, complex *a, int lda, 
 int *info);

void claset(char uplo, int m, int n, complex * alpha, complex *beta, complex *a, int lda);

void clasr(char side, char pivot, char direct, int m,
  int n, float *c, float *s, complex *a, int lda);

void classq(int n, complex *x, int incx, float * scale, float *sumsq);

void claswp(int n, complex *a, int lda, int k1, int k2, int *ipiv, int incx);

void clasyf(char uplo, int n, int nb, int *kb,
  complex *a, int lda, int *ipiv, complex *w, int ldw, 
 int *info);

void clatbs(char uplo, char trans, char diag, char normin, int n, int kd, complex *ab, int ldab, complex *
 x, float *scale, float *cnorm, int *info);

void clatps(char uplo, char trans, char diag, char normin, int n, complex *ap, complex *x, float *scale, float *cnorm,
  int *info);

void clatrd(char uplo, int n, int nb, complex *a, 
 int lda, float *e, complex *tau, complex *w, int ldw);

void clatrs(char uplo, char trans, char diag, char normin, int n, complex *a, int lda, complex *x, float *scale,
  float *cnorm, int *info);

void clatzm(char side, int m, int n, complex *v, 
 int incv, complex *tau, complex *c1, complex *c2, int ldc);

void clauu2(char uplo, int n, complex *a, int lda,
  int *info);

void clauum(char uplo, int n, complex *a, int lda,
  int *info);

void clazro(int m, int n, complex *alpha, complex *beta, complex *a, 
           int lda);

void cpbcon(char uplo, int n, int kd, complex *ab,
  int ldab, float anorm, float *rcond, int *info);

void cpbequ(char uplo, int n, int kd, complex *ab,
  int ldab, float *s, float *scond, float *amax, int *info);

void cpbrfs(char uplo, int n, int kd, int nrhs, complex *ab, int ldab, complex *afb, int ldafb, 
 complex *b, int ldb, complex *x, int ldx, float *ferr, float * berr, int *info);

void cpbstf(char uplo, int n, int kd, complex *ab,
  int ldab, int *info);

void cpbsv(char uplo, int n, int kd, int nrhs, complex *ab, int ldab, complex *b, int ldb, int *
 info);

void cpbsvx(char fact, char uplo, int n, int kd, 
 int nrhs, complex *ab, int ldab, complex *afb, int ldafb, char *equed, float *s, complex *b, int ldb, complex *x, 
 int ldx, float *rcond, float *ferr, float *berr, int *info);

void cpbtf2(char uplo, int n, int kd, complex *ab,
  int ldab, int *info);

void cpbtrf(char uplo, int n, int kd, complex *ab,
  int ldab, int *info);

void cpbtrs(char uplo, int n, int kd, int nrhs, complex *ab, int ldab, complex *b, int ldb, int *
 info);

void cpocon(char uplo, int n, complex *a, int lda,
  float anorm, float *rcond, int *info);

void cpoequ(int n, complex *a, int lda, float *s, 
 float *scond, float *amax, int *info);

void cporfs(char uplo, int n, int nrhs, complex *a, int lda, complex *af, int ldaf, complex *b, int ldb,
  complex *x, int ldx, float *ferr, float *berr, int *info);

void cposv(char uplo, int n, int nrhs, complex *a,
  int lda, complex *b, int ldb, int *info);

void cposvx(char fact, char uplo, int n, int nrhs, complex *a, int lda,
        complex *af, int ldaf, char *equed, float *s, complex *b, int ldb,
        complex *x, int ldx, float *rcond, float *ferr, float *berr, int *info);

void cpotf2(char uplo, int n, complex *a, int lda,
  int *info);

void cpotrf(char uplo, int n, complex *a, int lda,
  int *info);

void cpotri(char uplo, int n, complex *a, int lda,
  int *info);

void cpotrs(char uplo, int n, int nrhs, complex *a, int lda, complex *b, int ldb, int *info);

void cppcon(char uplo, int n, complex *ap, float anorm,
  float *rcond, int *info);

void cppequ(char uplo, int n, complex *ap, float *s, 
 float *scond, float *amax, int *info);

void cpprfs(char uplo, int n, int nrhs, complex *ap, complex *afp, complex *b, int ldb, complex *x, int ldx, 
 float *ferr, float *berr, int *info);

void cppsv(char uplo, int n, int nrhs, complex *ap, complex *b, int ldb, int *info);

void cppsvx(char fact, char uplo, int n, int nrhs, complex *ap, complex *afp, char *equed, float *s, complex *b, 
 int ldb, complex *x, int ldx, float *rcond, float *ferr, float 
 *berr, int *info);

void cpptrf(char uplo, int n, complex *ap, int *info);

void cpptri(char uplo, int n, complex *ap, int *info);

void cpptrs(char uplo, int n, int nrhs, complex *ap, complex *b, int ldb, int *info);

void cptcon(int n, float *d, complex *e, float anorm, 
 float *rcond, int *info);

void cpteqr(char compz, int n, float *d, float *e, 
 complex *z, int ldz, int *info);

void cptrfs(char uplo, int n, int nrhs, float *d, 
 complex *e, float *df, complex *ef, complex *b, int ldb, complex *x, int ldx, float *ferr, float *berr, int *info);

void cptsv(int n, int nrhs, float *d, complex *e, 
 complex *b, int ldb, int *info);

void cptsvx(char fact, int n, int nrhs, float *d, 
 complex *e, float *df, complex *ef, complex *b, int ldb, complex *x, int ldx, float *rcond, float *ferr, float *berr, int *info);

void cpttrf(int n, float *d, complex *e, int *info);

void cpttrs(char uplo, int n, int nrhs, float *d, 
 complex *e, complex *b, int ldb, int *info);

void crot(int n, complex *cx, int incx, complex *cy, int incy, float c, complex *s);

void cspcon(char uplo, int n, complex *ap, int *ipiv, float anorm, float *rcond, int *info);

void cspmv(char uplo, int n, complex *alpha, complex *ap, complex *x, int incx, complex *beta, complex *y, int 
 incy);

void cspr(char uplo, int n, complex *alpha, complex *x,
  int incx, complex *ap);

void csprfs(char uplo, int n, int nrhs, complex *ap, complex *afp, int *ipiv, complex *b, int ldb, complex *x,
  int ldx, float *ferr, float *berr, 
 int *info);

void cspsv(char uplo, int n, int nrhs, complex *ap, int *ipiv, complex *b, int ldb, int *info);

void cspsvx(char fact, char uplo, int n, int nrhs, complex *ap, complex *afp, int *ipiv, complex *b, int 
 ldb, complex *x, int ldx, float *rcond, float *ferr, float *berr, 
 int *info);

void csptrf(char uplo, int n, complex *ap, int *ipiv, int *info);

void csptri(char uplo, int n, complex *ap, int *ipiv, int *info);

void csptrs(char uplo, int n, int nrhs, complex *ap, int *ipiv, complex *b, int ldb, int *info);

void csrot(int  n, complex *cx, int  incx, complex *cy, int  incy, float *c, float *s);

void csrscl(int n, float sa, complex *sx, int incx);

void cstedc(char compz, int n, float *d, float *e, 
 complex *z, int ldz, int *info);

void cstein(int n, float *d, float *e, int m, float *w, int *iblock, int *isplit, complex *z, int ldz, int *ifail, int *info);

void csteqr(char compz, int n, float *d, float *e, 
 complex *z, int ldz, int *info);

void csycon(char uplo, int n, complex *a, int lda,
  int *ipiv, float anorm, float *rcond, int *info);

void csymv(char uplo, int n, complex *alpha, complex *a, int lda, complex *x, int incx, complex *beta, complex *y,
  int incy);

void csyr(char uplo, int n, complex *alpha, complex *x,
  int incx, complex *a, int lda);

void csyrfs(char uplo, int n, int nrhs, complex *a, int lda, complex *af, int ldaf, int *ipiv, complex *
 b, int ldb, complex *x, int ldx, float *ferr, float *berr, 
 int *info);

void csysv(char uplo, int n, int nrhs, complex *a,
  int lda, int *ipiv, complex *b, int ldb, int *info);

void csysvx(char fact, char uplo, int n, int nrhs, complex *a, int lda, complex *af, int ldaf, int *
 ipiv, complex *b, int ldb, complex *x, int ldx, float *rcond,
  float *ferr, float *berr, int *info);

void csytf2(char uplo, int n, complex *a, int lda,
  int *ipiv, int *info);

void csytrf(char uplo, int n, complex *a, int lda,
  int *ipiv, int *info);

void csytri(char uplo, int n, complex *a, int lda,
  int *ipiv, int *info);

void csytrs(char uplo, int n, int nrhs, complex *a, int lda, int *ipiv, complex *b, int ldb, int *info);

void ctbcon(char norm, char uplo, char diag, int n, 
 int kd, complex *ab, int ldab, float *rcond, int *info);

void ctbrfs(char uplo, char trans, char diag, int n, 
 int kd, int nrhs, complex *ab, int ldab, complex *b, 
 int ldb, complex *x, int ldx, float *ferr, float *berr, 
 int *info);

void ctbtrs(char uplo, char trans, char diag, int n, 
 int kd, int nrhs, complex *ab, int ldab, complex *b, 
 int ldb, int *info);

void ctgevc(char side, char howmny, int *select, 
 int n, complex *a, int lda, complex *b, int ldb, 
 complex *vl, int ldvl, complex *vr, int ldvr, int mm, 
 int *m, int *info);

void ctgsja(char jobu, char jobv, char jobq, int m, 
 int p, int n, int k, int l, complex *a, int lda, complex *b, int ldb, float tola, float tolb, float *alpha, 
 float *beta, complex *u, int ldu, complex *v, int ldv, 
 complex *q, int ldq, int *ncycle, int *
 info);

void ctpcon(char norm, char uplo, char diag, int n, 
 complex *ap, float *rcond, int *info);

void ctprfs(char uplo, char trans, char diag, int n, 
 int nrhs, complex *ap, complex *b, int ldb, complex *x, 
 int ldx, float *ferr, float *berr, int *info);

void ctptri(char uplo, char diag, int n, complex *ap, 
 int *info);

void ctptrs(char uplo, char trans, char diag, int n, 
 int nrhs, complex *ap, complex *b, int ldb, int *info);

void ctrcon(char norm, char uplo, char diag, int n, 
 complex *a, int lda, float *rcond, 
 int *info);

void ctrevc(char side, char howmny, int *select, 
 int n, complex *t, int ldt, complex *vl, int ldvl, 
 complex *vr, int ldvr, int mm, int *m, int *info);

void ctrexc(char compq, int n, complex *t, int ldt, complex *q, int ldq, int ifst, int ilst, int *
 info);

void ctrrfs(char uplo, char trans, char diag, int n, 
 int nrhs, complex *a, int lda, complex *b, int ldb, 
 complex *x, int ldx, float *ferr, float *berr, int *info);

void ctrsen(char job, char compq, int *select, int 
 n, complex *t, int ldt, complex *q, int ldq, complex *w, 
 int *m, float *s, float *sep, int *info);

void ctrsna(char job, char howmny, int *select, 
 int n, complex *t, int ldt, complex *vl, int ldvl, 
 complex *vr, int ldvr, float *s, float *sep, int mm, int *m, int *info);

void ctrsyl(char trana, char tranb, int isgn, int 
 m, int n, complex *a, int lda, complex *b, int ldb, 
 complex *c, int ldc, float *scale, int *info);

void ctrti2(char uplo, char diag, int n, complex *a, 
 int lda, int *info);

void ctrtri(char uplo, char diag, int n, complex *a, 
 int lda, int *info);

void ctrtrs(char uplo, char trans, char diag, int n, 
 int nrhs, complex *a, int lda, complex *b, int ldb, 
 int *info);

void ctzrqf(int m, int n, complex *a, int lda,
  complex *tau, int *info);

void cung2l(int m, int n, int k, complex *a, 
 int lda, complex *tau, int *info);

void cung2r(int m, int n, int k, complex *a, 
 int lda, complex *tau, int *info);

void cungbr(char vect, int m, int n, int k, 
 complex *a, int lda, complex *tau, int *info);

void cunghr(int n, int ilo, int ihi, complex *a, int lda, complex *tau, int *info);

void cungl2(int m, int n, int k, complex *a, 
 int lda, complex *tau, int *info);

void cunglq(int m, int n, int k, complex *a, 
 int lda, complex *tau, int * info);

void cungql(int m, int n, int k, complex *a, 
 int lda, complex *tau, int *info);

void cungqr(int m, int n, int k, complex *a, 
 int lda, complex *tau, int * info);

void cungr2(int m, int n, int k, complex *a, 
 int lda, complex *tau, int *info);

void cungrq(int m, int n, int k, complex *a, 
 int lda, complex *tau, int *info);

void cungtr(char uplo, int n, complex *a, int lda,
  complex *tau, int *info);

void cunm2l(char side, char trans, int m, int n, 
 int k, complex *a, int lda, complex *tau, complex *c, 
 int ldc, int *info);

void cunm2r(char side, char trans, int m, int n, 
 int k, complex *a, int lda, complex *tau, complex *c, 
 int ldc, int *info);

void cunmbr(char vect, char side, char trans, int m, 
 int n, int k, complex *a, int lda, complex *tau, 
 complex *c, int ldc, int *info);

void cunmhr(char side, char trans, int m, int n, 
 int ilo, int ihi, complex *a, int lda, complex *tau, 
 complex *c, int ldc, int *info);

void cunml2(char side, char trans, int m, int n, 
 int k, complex *a, int lda, complex *tau, complex *c, 
 int ldc, int *info);

void cunmlq(char side, char trans, int m, int n, 
 int k, complex *a, int lda, complex *tau, complex *c, 
 int ldc, int *info);

void cunmql(char side, char trans, int m, int n, 
 int k, complex *a, int lda, complex *tau, complex *c, 
 int ldc, int *info);

void cunmqr(char side, char trans, int m, int n, 
 int k, complex *a, int lda, complex *tau, complex *c, 
 int ldc, int *info);

void cunmr2(char side, char trans, int m, int n, 
 int k, complex *a, int lda, complex *tau, complex *c, 
 int ldc, int *info);

void cunmrq(char side, char trans, int m, int n, 
 int k, complex *a, int lda, complex *tau, complex *c, 
 int ldc, int *info);

void cunmtr(char side, char uplo, char trans, int m, 
 int n, complex *a, int lda, complex *tau, complex *c, 
 int ldc, int *info);

void cupgtr(char uplo, int n, complex *ap, complex * tau, complex *q, int ldq, int *info);

void cupmtr(char side, char uplo, char trans, int m, 
 int n, complex *ap, complex *tau, complex *c, 
 int ldc, int *info);

void dbdsqr(char uplo, int n, int ncvt, int nru, int ncc, double *d, double *e, double *vt, int ldvt, double *u,
  int ldu, double *c, int ldc, int *info);

void ddisna(char job, int m, int n, double *d,
  double *sep, int *info);

void dgbbrd(char vect, int m, int n, int ncc,
  int kl, int ku, double *ab, int ldab, double *d, double *e, 
 double *q, int ldq, double *pt, int ldpt, double *c, int ldc,
  int *info);

void dgbcon(char norm, int n, int kl, int ku,
  double *ab, int ldab, int *ipiv, double anorm, double *rcond, 
 int *info);

void dgbequ(int m, int n, int kl, int ku,
  double *ab, int ldab, double *r, double *c, 
 double *rowcnd, double *colcnd, double *amax, int * info);

void dgbrfs(char trans, int n, int kl, int ku, int nrhs, double *ab, int ldab, double *afb, int ldafb,
  int *ipiv, double *b, int ldb, double *x, int ldx, double * ferr, double *berr, int *info);

void dgbsv(int n, int kl, int ku, int nrhs, double *ab, int ldab, int *ipiv, double *b, 
 int ldb, int *info);

void dgbsvx(char fact, char trans, int n, int kl,
  int ku, int nrhs, double *ab, int ldab, double *afb, 
 int ldafb, int *ipiv, char *equed, double *r, double *c, double *b,
  int ldb, double *x, int ldx, double *rcond, double *ferr, double * berr, 
   int *info);

void dgbtf2(int m, int n, int kl, int ku,
  double *ab, int ldab, int *ipiv, int *info);

void dgbtrf(int m, int n, int kl, int ku,
  double *ab, int ldab, int *ipiv, int *info);

void dgbtrs(char trans, int n, int kl, int ku, int nrhs, double *ab, int ldab, int *ipiv, 
 double *b, int ldb, int *info);

void dgebak(char job, char side, int n, int ilo, 
 int ihi, double *scale, int m, double *v, int ldv, int *info);

void dgebal(char job, int n, double *a, int lda, int *ilo, int *ihi, double *scale, int *info);

void dgebd2(int m, int n, double *a, int lda, 
 double *d, double *e, double *tauq, double *taup, int *info);

void dgebrd(int m, int n, double *a, int lda, 
 double *d, double *e, double *tauq, double *taup, int *info);

void dgecon(char norm, int n, double *a, int lda, 
 double anorm, double *rcond, int *info);

void dgeequ(int m, int n, double *a, int lda, double *r, double *c, double *rowcnd, double *
 colcnd, double *amax, int *info);

void dgees(char jobvs, char sort, int (*select)(), int n, 
 double *a, int lda, int *sdim, double *wr, double *wi, double *vs, 
 int ldvs, int * info);

void dgeesx(char jobvs, char sort, int (*select)(), char sense, int n, double *a, int lda, int *sdim, double *wr, 
 double *wi, double *vs, int ldvs, double *rconde, double *rcondv, int *info);

void dgeev(char jobvl, char jobvr, int n, double *a, 
 int lda, double *wr, double *wi, double *vl, int ldvl, double *vr, 
 int ldvr, int *info);

void dgeevx(char balanc, char jobvl, char jobvr, char sense, int n, double *a, int lda, double *wr, double *wi, double *
 vl, int ldvl, double *vr, int ldvr, int *ilo, int *ihi, double *scale, double *abnrm, double *rconde, double *rcondv, int *info);

void dgegs(char jobvsl, char jobvsr, int n, double *a, 
 int lda, double *b, int ldb, double *alphar, double *alphai, double 
 *beta, double *vsl, int ldvsl, double *vsr, int ldvsr, int *info);

void dgegv(char jobvl, char jobvr, int n, double *a, 
 int lda, double *b, int ldb, double *alphar, double *alphai, double 
 *beta, double *vl, int ldvl, double *vr, int ldvr, int *info);

void dgehd2(int n, int ilo, int ihi, double *a, 
 int lda, double *tau, int *info);

void dgehrd(int n, int ilo, int ihi, double *a, 
 int lda, double *tau, int *info);

void dgelq2(int m, int n, double *a, int lda, 
 double *tau, int *info);

void dgelqf(int m, int n, double *a, int lda, 
 double *tau, int *info);

void dgels(char trans, int m, int n, int nrhs, double *a, int lda, double *b, int ldb, int *info);

void dgelss(int m, int n, int nrhs, double *a, 
 int lda, double *b, int ldb, double *s, double rcond, int * rank,
  int *info);

void dgelsx(int m, int n, int nrhs, double *a, 
 int lda, double *b, int ldb, int *jpvt, double rcond, 
 int *rank, int *info);

void dgeql2(int m, int n, double *a, int lda, 
 double *tau, int *info);

void dgeqlf(int m, int n, double *a, int lda, 
 double *tau, int *info);

void dgeqpf(int m, int n, double *a, int lda, 
 int *jpvt, double *tau, int *info);

void dgeqr2(int m, int n, double *a, int lda, 
 double *tau, int *info);

void dgeqrf(int m, int n, double *a, int lda, 
 double *tau, int *info);

void dgerfs(char trans, int n, int nrhs, double *a, 
 int lda, double *af, int ldaf, int *ipiv, double *b, 
 int ldb, double *x, int ldx, double *ferr, double *berr, int *info);

void dgerq2(int m, int n, double *a, int lda, double *tau, double *work, int *info);

void dgerqf(int m, int n, double *a, int lda, 
 double *tau, int *info);

void dgesv(int n, int nrhs, double *a, int 
 lda, int *ipiv, double *b, int ldb, int *info);

void dgesvd(char jobu, char jobvt, int m, int n, 
 double *a, int lda, double *s, double *u, int ldu, double *vt, 
 int ldvt, int *info);

void dgesvx(char fact, char trans, int n, int nrhs, double *a, int lda, double *af, int ldaf, int *ipiv, 
 char *equed, double *r, double *c, double *b, int ldb, double *x, 
 int ldx, double *rcond, double *ferr, double *berr, int *info);

void dgetf2(int m, int n, double *a, int lda, int *ipiv, int *info);

void dgetrf(int m, int n, double *a, int lda, int *ipiv, int *info);

void dgetri(int n, double *a, int lda, int *ipiv,
  int *info);

void dgetrs(char trans, int n, int nrhs, 
 double *a, int lda, int *ipiv, double *b, int ldb, int *info);

void dggbak(char job, char side, int n, int ilo, 
 int ihi, double *lscale, double *rscale, int m, 
 double *v, int ldv, int *info);

void dggbal(char job, int n, double *a, int lda, 
 double *b, int ldb, int *ilo, int *ihi, double *lscale, double 
 *rscale, int *info);

void dggglm(int n, int m, int p, double *a, 
 int lda, double *b, int ldb, double *d, double *x, double *y, int *info);

void dgghrd(char compq, char compz, int n, int ilo, int ihi, double *a, int lda, double *b, 
 int ldb, double *q, int ldq, double *z, int ldz, int *info);

void dgglse(int m, int n, int p, double *a, 
 int lda, double *b, int ldb, double *c, double *d, double *x, int *info);

void dggqrf(int n, int m, int p, double *a, 
 int lda, double *taua, double *b, int ldb, double *taub, int *info);

void dggrqf(int m, int p, int n, double *a, 
 int lda, double *taua, double *b, int ldb, double *taub, int *info);

void dggsvd(char jobu, char jobv, char jobq, int m, 
 int n, int p, int *k, int *l, double *a, int lda,
  double *b, int ldb, double *alpha, double *beta, double *u, int ldu, double *v, int ldv,
  double *q, int ldq, int *info);

void dggsvp(char jobu, char jobv, char jobq, int m, 
 int p, int n, double *a, int lda, double *b, int ldb, 
 double tola, double tolb, int *k, int *l, double *u, int ldu,
  double *v, int ldv, double *q, int ldq, double * tau, int *info);

void dgtcon(char norm, int n, double *dl, double *d, double * du, double *du2, int *ipiv, double anorm, double *rcond,
  int *info);

void dgtrfs(char trans, int n, int nrhs, double *dl,
  double *d, double *du, double *dlf, double *df, double *duf, double *du2, 
 int *ipiv, double *b, int ldb, double *x, int ldx, double * ferr, double *berr, int *info);

void dgtsv(int n, int nrhs, double *dl, 
 double *d, double *du, double *b, int ldb, int * info);

void dgtsvx(char fact, char trans, int n, int nrhs, double *dl, double *d, double *du, double *dlf,
 double *df, double *duf, double *du2, int *ipiv, double *b, int ldb, double *x, int ldx,
 double *rcond, double *ferr, double *berr, int *info);

void dgttrf(int n, double *dl, double *d, 
 double *du, double *du2, int *ipiv, int *info);

void dgttrs(char trans, int n, int nrhs, 
 double *dl, double *d, double *du, double *du2, 
 int *ipiv, double *b, int ldb, int *info);

void dhgeqz(char job, char compq, char compz, int n, 
 int ilo, int ihi, double *a, int lda, double *b, int ldb, double *alphar, double *alphai,
 double *beta, double *q, int ldq, double *z, int ldz, int *info);

void dhsein(char side, char eigsrc, char initv, int * select, int n, double *h, int ldh,
 double *wr, double *wi, double *vl, int ldvl, double *vr, int ldvr, int mm, int *m, 
 int *ifaill, int *ifailr, int *info);

void dhseqr(char job, char compz, int n, int ilo,
  int ihi, double *h, int ldh, double *wr, double *wi, double *z, 
 int ldz, int *info);

void dlabad(double *small, double *large);

void dlabrd(int m, int n, int nb, double * a, int lda, double *d, double *e, double *tauq, 
 double *taup, double *x, int ldx, double *y, int 
 *ldy);

void dlacon(int n, double *v, double *x, 
 int *isgn, double *est, int *kase);

void dlacpy(char uplo, int m, int n, double * a, int lda, double *b, int ldb);

void dladiv(double a, double b, double c, 
 double d, double *p, double *q);

void dlae2(double a, double b, double c, 
 double *rt1, double *rt2);

void dlaebz(int ijob, int nitmax, int n, 
 int mmax, int minp, int nbmin, double abstol, double reltol, double pivmin, double *d, double *e, double *e2,
        int *nval, double *ab, double *c, int *mout, int *nab, int *info);

void dlaed0(int icompq, int qsiz, int n, double 
 *d, double *e, double *q, int ldq, double *qstore, int ldqs, int *info);

void dlaed1(int n, double *d, double *q, int ldq, 
 int *indxq, double rho, int cutpnt, int *info);

void dlaed2(int *k, int n, double *d, 
 double *q, int ldq, int *indxq, double *rho, int 
 cutpnt, double *z, double *dlamda, double *q2, int ldq2, int *indxc, double *w, int *indxp, int *indx, 
 int *coltyp, int *info);

void dlaed3(int k, int kstart, int kstop, 
 int n, double *d, double *q, int ldq, double rho, int cutpnt, double *dlamda, double *q2, int 
 ldq2, int *indxc, int *ctot, double *w, double *s, 
 int lds, int *info);

void dlaed4(int n, int i, double *d, 
 double *z, double *delta, double rho, double *dlam, 
 int *info);

void dlaed5(int i, double *d, double *z, 
 double *delta, double rho, double *dlam);

void dlaed6(int kniter, int orgati, double rho, double *d, double *z, double finit, double *tau,
  int *info);

void dlaed7(int icompq, int n, int qsiz, 
 int tlvls, int curlvl, int curpbm, double *d, double *q, 
 int ldq, int *indxq, double rho, int cutpnt, double * qstore, int *qptr, int *prmptr, int *perm, int *
 givptr, int *givcol, double *givnum, int *info);

void dlaed8(int icompq, int *k, int n, int 
 qsiz, double *d, double *q, int ldq, int *indxq, 
 double *rho, int cutpnt, double *z, double *dlamda, 
 double *q2, int ldq2, double *w, int *perm, int * givptr, int *givcol, double *givnum, int *indxp, int *
 indx, int *info);

void dlaed9(int k, int kstart, int kstop, 
 int n, double *d, double *q, int ldq, double rho, double *dlamda, double *w, double *s, int lds, 
 int *info);

void dlaeda(int n, int tlvls, int curlvl, 
 int curpbm, int *prmptr, int *perm, int *givptr, 
 int *givcol, double *givnum, double *q, int *qptr, 
 double *z, double *ztemp, int *info);

void dlaein(int rightv, int noinit, int n, 
 double *h, int ldh, double wr, double wi, double *vr, double *vi, double *b, int ldb, double eps3,
        double smlnum, double bignum, int *info);

void dlaev2(double a, double b, double c, 
 double *rt1, double *rt2, double *cs1, double *sn1);

void dlaexc(int wantq, int n, double *t, int ldt, double *q, int ldq, int j1, int n1, int n2, 
 int *info);

void dlag2(double *a, int lda, double *b, 
 int ldb, double safmin, double *scale1, double * scale2, double *wr1, double *wr2, double *wi);

void dlags2(int upper, double a1, double a2, 
 double a3, double b1, double b2, double b3, 
 double *csu, double *snu, double *csv, double *snv, 
 double *csq, double *snq);

void dlagtf(int n, double *a, double lambda, 
 double *b, double *c, double tol, double *d, int 
 *in, int *info);

void dlagtm(char trans, int n, int nrhs, 
 double alpha, double *dl, double *d, double *du, 
 double *x, int ldx, double beta, double *b, int 
 ldb);

void dlagts(int job, int n, double *a, 
 double *b, double *c, double *d, int *in, double * y, double *tol, int *info);

void dlahqr(int wantt, int wantz, int n, 
 int ilo, int ihi, double *h, int ldh, double * wr, double *wi, int iloz, int ihiz, double *z, 
 int ldz, int *info);

void dlahrd(int n, int k, int nb, double * a, int lda, double *tau, double *t, int ldt, 
 double *y, int ldy);

void dlaic1(int job, int j, double *x, 
 double sest, double *w, double gamma, double * sestpr, double *s, double *c);

void dlaln2(int ltrans, int na, int nw, 
 double smin, double ca, double *a, int lda, 
 double d1, double d2, double *b, int ldb, 
 double wr, double wi, double *x, int ldx, 
 double *scale, double *xnorm, int *info);

extern double dlamch(char cmach);

void dlamrg(int n1, int n2, double *a, int 
 dtrd1, int dtrd2, int *index);

extern double dlangb(char norm, int n, int kl, int ku, double *ab,
  int ldab );

extern double dlange(char norm, int m, int n, double *a, int lda );

extern double dlangt(char norm, int n, double *dl, double *d, 
 double *du);

extern double dlanhs(char norm, int n, double *a, int lda);

extern double dlansb(char norm, char uplo, int n, int k, double *ab, 
 int ldab );

extern double dlansp(char norm, char uplo, int n, double *ap);

extern double dlanst(char norm, int n, double *d, double *e);

extern double dlansy(char norm, char uplo, int n, double *a, int lda);

extern double dlantb(char norm, char uplo, char diag, int n, int k,
  double *ab, int ldab);

extern double dlantp(char norm, char uplo, char diag, int n, double *ap);

extern double dlantr(char norm, char uplo, char diag, int m, int n,
  double *a, int lda);

void dlanv2(double *a, double *b, double *c, 
 double *d, double *rt1r, double *rt1i, double *rt2r, 
 double *rt2i, double *cs, double *sn);

void dlapll(int n, double *x, int incx, 
 double *y, int incy, double *ssmin);

void dlapmt(int forwrd, int m, int n, 
 double *x, int ldx, int *k);

extern double dlapy2(double x, double y);

extern double dlapy3(double x, double y, double z);

void dlaqgb(int m, int n, int kl, int ku,
  double *ab, int ldab, double *r, double *c, 
 double *rowcnd, double *colcnd, double amax, char *equed);

void dlaqge(int m, int n, double *a, int lda, double *r, double *c, double rowcnd, double 
 colcnd, double amax, char *equed);

void dlaqsb(char uplo, int n, int kd, double * ab, int ldab, double *s, double scond, double amax,
  char *equed);

void dlaqsp(char uplo, int n, double *ap, 
 double *s, double scond, double amax, char *equed);

void dlaqsy(char uplo, int n, double *a, int lda, double *s, double scond, double amax, char *equed);

void dlaqtr(int ltran, int ldouble, int n, double 
 *t, int ldt, double *b, double w, double *scale, double *x, 
 int *info);

void dlar2v(int n, double *x, double *y, 
 double *z, int incx, double *c, double *s, int incc);

void dlarf(char side, int m, int n, double *v, 
 int incv, double tau, double *c, int ldc);

void dlarfb(char side, char trans, char direct, char storev, int m, int n, int k, double *v, int ldv, 
 double *t, int ldt, double *c, int ldc);

void dlarfg(int n, double *alpha, double *x, 
 int incx, double *tau);

void dlarft(char direct, char storev, int n, int k, double *v, int ldv, double *tau, double *t, 
 int ldt);

void dlarfx(char side, int m, int n, double *v, 
 double tau, double *c, int ldc);

void dlargv(int n, double *x, int incx, 
 double *y, int incy, double *c, int incc);

void dlarnv(int idist, int *iseed, int n, 
 double *x);

void dlartg(double f, double g, double *cs, 
 double *sn, double *r);

void dlartv(int n, double *x, int incx, 
 double *y, int incy, double *c, double *s, int incc);

void dlaruv(int *iseed, int n, double *x);

void dlas2(double f, double g, double h, 
 double *ssmin, double *ssmax);

void dlascl(char type, int kl, int ku, double 
 cfrom, double cto, int m, int n, double *a, 
 int lda, int *info);

void dlaset(char uplo, int m, int n, double alpha, double beta, double *a, int lda);

void dlasq1(int n, double *d, double *e, int *info);

void dlasq2(int m, double *q, double *e, 
 double *qq, double *ee, double eps, double tol2, 
 double small2, double *sup, int *kend, int *info);

void dlasq3(int *n, double *q, double *e, 
 double *qq, double *ee, double *sup, double *sigma, 
 int *kend, int *off, int *iphase, int iconv, 
 double eps, double tol2, double small2);

void dlasq4(int n, double *q, double *e, 
 double *tau, double *sup);

void dlasr(char side, char pivot, char direct, int m,
  int n, double *c, double *s, double *a, int lda);

void dlasrt(char id, int n, double *d, int * info);

void dlassq(int n, double *x, int incx, 
 double *scale, double *sumsq);

void dlasv2(double f, double g, double h, 
 double *ssmin, double *ssmax, double *snr, double * csr, double *snl, double *csl);

void dlaswp(int n, double *a, int lda, int 
 k1, int k2, int *ipiv, int incx);

void dlasy2(int ltranl, int ltranr, int isgn, 
 int n1, int n2, double *tl, int ldtl, double * tr, int ldtr, double *b, int ldb, double *scale, 
 double *x, int ldx, double *xnorm, int *info);

void dlasyf(char uplo, int n, int nb, int *kb,
  double *a, int lda, int *ipiv, double *w, int ldw, int *info);

void dlatbs(char uplo, char trans, char diag, char normin, int n, int kd, double *ab, int ldab, 
 double *x, double *scale, double *cnorm, int *info);

void dlatps(char uplo, char trans, char diag, char normin, int n, double *ap, double *x, double *scale, 
 double *cnorm, int *info);

void dlatrd(char uplo, int n, int nb, double * a, int lda, double *e, double *tau, double *w, 
 int ldw);

void dlatrs(char uplo, char trans, char diag, char normin, int n, double *a, int lda, double *x, 
 double *scale, double *cnorm, int *info);

void dlatzm(char side, int m, int n, double *v, 
 int incv, double tau, double *c1, double *c2, int ldc);

void dlauu2(char uplo, int n, double *a, int lda, int *info);

void dlauum(char uplo, int n, double *a, int lda, int *info);

void dlazro(int m, int n, double alpha, double beta, 
            double *a, int lda);

void dopgtr(char uplo, int n, double *ap, double *tau, 
 double *q, int ldq, int *info);

void dopmtr(char side, char uplo, char trans, int m, 
 int n, double *ap, double *tau, double *c, int ldc, int *info);

void dorg2l(int m, int n, int k, double *a, 
 int lda, double *tau, int *info);

void dorg2r(int m, int n, int k, double *a, 
 int lda, double *tau, int *info);

void dorgbr(char vect, int m, int n, int k, 
 double *a, int lda, double *tau, int 
 *info);

void dorghr(int n, int ilo, int ihi, double *a, 
 int lda, double *tau, int *info);

void dorgl2(int m, int n, int k, double *a, 
 int lda, double *tau, int *info);

void dorglq(int m, int n, int k, double *a, 
 int lda, double *tau, int *info);

void dorgql(int m, int n, int k, double *a, 
 int lda, double *tau, int *info);

void dorgqr(int m, int n, int k, double *a, 
 int lda, double *tau, int *info);

void dorgr2(int m, int n, int k, double *a, 
 int lda, double *tau, int *info);

void dorgrq(int m, int n, int k, double *a, 
 int lda, double *tau, int *info);

void dorgtr(char uplo, int n, double *a, int lda, 
 double *tau, int *info);

void dorm2l(char side, char trans, int m, int n, 
 int k, double *a, int lda, double *tau, double *c, int ldc, 
 int *info);

void dorm2r(char side, char trans, int m, int n, 
 int k, double *a, int lda, double *tau, double *c, int ldc, 
 int *info);

void dormbr(char vect, char side, char trans, int m, 
 int n, int k, double *a, int lda, double *tau, double *c, 
 int ldc, int *info);

void dormhr(char side, char trans, int m, int n, 
 int ilo, int ihi, double *a, int lda, double *tau, double *c,
  int ldc, int *info);

void dorml2(char side, char trans, int m, int n, 
 int k, double *a, int lda, double *tau, double *c, int ldc, 
 int *info);

void dormlq(char side, char trans, int m, int n, 
 int k, double *a, int lda, double *tau, double *c, int ldc, 
 int *info);

void dormql(char side, char trans, int m, int n, 
 int k, double *a, int lda, double *tau, double *c, int ldc, 
 int *info);

void dormqr(char side, char trans, int m, int n, 
 int k, double *a, int lda, double *tau, double *c, int ldc, 
 int *info);

void dormr2(char side, char trans, int m, int n, 
 int k, double *a, int lda, double *tau, double *c, int ldc, 
 int *info);

void dormrq(char side, char trans, int m, int n, 
 int k, double *a, int lda, double *tau, double *c, int ldc, 
 int *info);

void dormtr(char side, char uplo, char trans, int m, 
 int n, double *a, int lda, double *tau, double *c, int ldc, 
 int *info);

void dpbcon(char uplo, int n, int kd, double *ab, 
 int ldab, double anorm, double *rcond, int *info);

void dpbequ(char uplo, int n, int kd, double * ab, int ldab, double *s, double *scond, double *amax,
  int *info);

void dpbrfs(char uplo, int n, int kd, int nrhs, double *ab, int ldab, double *afb, int ldafb, double *b, 
 int ldb, double *x, int ldx, double *ferr, double *berr, int *info);

void dpbstf(char uplo, int n, int kd, double * ab, int ldab, int *info);

void dpbsv(char uplo, int n, int kd, int nrhs, double *ab, int ldab, double *b, int ldb, 
 int *info);

void dpbsvx(char fact, char uplo, int n, int kd, 
 int nrhs, double *ab, int ldab, double *afb, int ldafb, 
 char *equed, double *s, double *b, int ldb, double *x, int ldx, 
 double *rcond, double *ferr, double *berr, int *info);

void dpbtf2(char uplo, int n, int kd, double * ab, int ldab, int *info);

void dpbtrf(char uplo, int n, int kd, double * ab, int ldab, int *info);

void dpbtrs(char uplo, int n, int kd, int nrhs, double *ab, int ldab, double *b, int ldb, 
 int *info);

void dpocon(char uplo, int n, double *a, int lda, 
 double anorm, double *rcond, int *info);

void dpoequ(int n, double *a, int lda, 
 double *s, double *scond, double *amax, int *info);

void dporfs(char uplo, int n, int nrhs, double *a, 
 int lda, double *af, int ldaf, double *b, int ldb, double *x,
  int ldx, double *ferr, double *berr, int *info);

void dposv(char uplo, int n, int nrhs, double 
 *a, int lda, double *b, int ldb, int *info);

void dposvx(char fact, char uplo, int n, int nrhs, double *a, int lda, double *af, int ldaf, char *equed, 
 double *s, double *b, int ldb, double *x, int ldx, double *rcond, 
 double *ferr, double *berr, int *info);

void dpotf2(char uplo, int n, double *a, int lda, int *info);

void dpotrf(char uplo, int n, double *a, int lda, int *info);

void dpotri(char uplo, int n, double *a, int lda, int *info);

void dpotrs(char uplo, int n, int nrhs, 
 double *a, int lda, double *b, int ldb, int * info);

void dppcon(char uplo, int n, double *ap, double anorm, 
 double *rcond, int *info);

void dppequ(char uplo, int n, double *ap, 
 double *s, double *scond, double *amax, int *info);

void dpprfs(char uplo, int n, int nrhs, double *ap, 
 double *afp, double *b, int ldb, double *x, int ldx, double *ferr, 
 double *berr, int *info);

void dppsv(char uplo, int n, int nrhs, double 
 *ap, double *b, int ldb, int *info);

void dppsvx(char fact, char uplo, int n, int nrhs, double *ap, double *afp, char *equed, double *s, double *b, int 
 ldb, double *x, int ldx, double *rcond, double *ferr, double *berr, int *info);

void dpptrf(char uplo, int n, double *ap, int * info);

void dpptri(char uplo, int n, double *ap, int * info);

void dpptrs(char uplo, int n, int nrhs, 
 double *ap, double *b, int ldb, int *info);

void dptcon(int n, double *d, double *e, double anorm, double * rcond, int *info);

void dpteqr(char compz, int n, double *d, double *e, double * z, int ldz, int *info);

void dptrfs(int n, int nrhs, double *d, double *e, 
 double *df, double *ef, double *b, int ldb, double *x, int ldx, 
 double *ferr, double *berr, int *info);

void dptsv(int n, int nrhs, double *d, 
 double *e, double *b, int ldb, int *info);

void dptsvx(char fact, int n, int nrhs, double *d, 
 double *e, double *df, double *ef, double *b, int ldb, double *x, int ldx, double *rcond, double *ferr, double *berr, int *info);

void dpttrf(int n, double *d, double *e, 
 int *info);

void dpttrs(int n, int nrhs, double *d, 
 double *e, double *b, int ldb, int *info);

void drscl(int n, double sa, double *sx, 
 int incx);

void dsbev(char jobz, char uplo, int n, int kd, 
 double *ab, int ldab, double *w, double *z, int ldz, int *info);

void dsbevd(char jobz, char uplo, int n, int kd, 
 double *ab, int ldab, double *w, double *z, int ldz, int *info);

void dsbevx(char jobz, char range, char uplo, int n, 
 int kd, double *ab, int ldab, double *q, int ldq, double vl,
  double vu, int il, int iu, double abstol, int *m, double * w, double *z, int ldz, int *ifail, 
 int *info);

void dsbgst(char vect, char uplo, int n, int ka, 
 int kb, double *ab, int ldab, double *bb, int ldbb, double * x, int ldx, int *info);

void dsbgv(char jobz, char uplo, int n, int ka, 
 int kb, double *ab, int ldab, double *bb, int ldbb, double * w, double *z, int ldz, int *info);

void dsbtrd(char vect, char uplo, int n, int kd, 
 double *ab, int ldab, double *d, double *e, double *q, int ldq, 
 int *info);

extern double dsecnd() ;

void dspcon(char uplo, int n, double *ap, int *ipiv, 
 double anorm, double *rcond, int *info);

void dspev(char jobz, char uplo, int n, double *ap, 
 double *w, double *z, int ldz, int *info);

void dspevd(char jobz, char uplo, int n, double *ap, 
 double *w, double *z, int ldz, int *info);

void dspevx(char jobz, char range, char uplo, int n, 
 double *ap, double vl, double vu, int il, int iu, double abstol, 
 int *m, double *w, double *z, int ldz, int *ifail, int *info);

void dspgst(int itype, char uplo, int n, 
 double *ap, double *bp, int *info);

void dspgv(int itype, char jobz, char uplo, int n, double *ap, double *bp, double *w, double *z, int ldz, int *info);

void dsprfs(char uplo, int n, int nrhs, double *ap, 
 double *afp, int *ipiv, double *b, int ldb, double *x, int ldx, double *ferr, double *berr, int *
 info);

void dspsv(char uplo, int n, int nrhs, double 
 *ap, int *ipiv, double *b, int ldb, int *info);

void dspsvx(char fact, char uplo, int n, int nrhs, double *ap, double *afp, int *ipiv, double *b, int ldb, double 
 *x, int ldx, double *rcond, double *ferr, double *berr, int *info);

void dsptrd(char uplo, int n, double *ap, 
 double *d, double *e, double *tau, int *info);

void dsptrf(char uplo, int n, double *ap, int * ipiv, int *info);

void dsptri(char uplo, int n, double *ap, int *ipiv, 
 int *info);

void dsptrs(char uplo, int n, int nrhs, 
 double *ap, int *ipiv, double *b, int ldb, int * info);

void dstebz(char range, char order, int n, double vl, 
 double vu, int il, int iu, double abstol, double *d, double *e, 
 int *m, int *nsplit, double *w, int *iblock, int *isplit, int *info);

void dstedc(char compz, int n, double *d, double *e, double * z, int ldz, int *info);

void dstein(int n, double *d, double *e, int m, double * w, int *iblock, int *isplit, double *z, int ldz, int *ifail, int *info);

void dsteqr(char compz, int n, double *d, double *e, double * z, int ldz, int *info);

void dsterf(int n, double *d, double *e, 
 int *info);

void dstev(char jobz, int n, double *d, double *e, double *z,
  int ldz, int *info);

void dstevd(char jobz, int n, double *d, double *e, double * z, int ldz, int *info);

void dstevx(char jobz, char range, int n, double *d, 
 double *e, double vl, double vu, int il, int iu, double abstol, 
 int *m, double *w, double *z, int ldz, int *ifail, int *info);

void dsycon(char uplo, int n, double *a, int lda, 
 int *ipiv, double anorm, double *rcond, int *info);

void dsyev(char jobz, char uplo, int n, double *a, 
 int lda, double *w, int *info);

void dsyevd(char jobz, char uplo, int n, double *a, 
 int lda, double *w, int *info);

void dsyevx(char jobz, char range, char uplo, int n, 
 double *a, int lda, double vl, double vu, int il, int iu, 
 double abstol, int *m, double *w, double *z, int ldz, int *ifail, int *info);

void dsygs2(int itype, char uplo, int n, 
 double *a, int lda, double *b, int ldb, int * info);

void dsygst(int itype, char uplo, int n, 
 double *a, int lda, double *b, int ldb, int * info);

void dsygv(int itype, char jobz, char uplo, int n, double *a, int lda, double *b, int ldb, double *w, int *info);

void dsyrfs(char uplo, int n, int nrhs, double *a, 
 int lda, double *af, int ldaf, int *ipiv, double *b, 
 int ldb, double *x, int ldx, double *ferr, double *berr, int *info);

void dsysv(char uplo, int n, int nrhs, double *a, 
 int lda, int *ipiv, double *b, int ldb, int *info);

void dsysvx(char fact, char uplo, int n, int nrhs, double *a, int lda, double *af, int ldaf, int *ipiv, 
 double *b, int ldb, double *x, int ldx, double *rcond, double *ferr,
  double *berr, int * info);

void dsytd2(char uplo, int n, double *a, int lda, double *d, double *e, double *tau, int *info);

void dsytf2(char uplo, int n, double *a, int lda, int *ipiv, int *info);

void dsytrd(char uplo, int n, double *a, int lda, 
 double *d, double *e, double *tau, int * info);

void dsytrf(char uplo, int n, double *a, int lda, 
 int *ipiv, int *info);

void dsytri(char uplo, int n, double *a, int lda, 
 int *ipiv, int *info);

void dsytrs(char uplo, int n, int nrhs, 
 double *a, int lda, int *ipiv, double *b, int ldb, int *info);

void dtbcon(char norm, char uplo, char diag, int n, 
 int kd, double *ab, int ldab, double *rcond, int *info);

void dtbrfs(char uplo, char trans, char diag, int n, 
 int kd, int nrhs, double *ab, int ldab, double *b, int 
 ldb, double *x, int ldx, double *ferr, double *berr, int *info);

void dtbtrs(char uplo, char trans, char diag, int n, 
 int kd, int nrhs, double *ab, int ldab, double 
 *b, int ldb, int *info);

void dtgevc(char side, char howmny, int *select, 
 int n, double *a, int lda, double *b, int ldb, double *vl, 
 int ldvl, double *vr, int ldvr, int mm, int *m, int *info);

void dtgsja(char jobu, char jobv, char jobq, int m, 
 int p, int n, int k, int l, double *a, int lda,
  double *b, int ldb, double tola, double tolb, double *alpha, double * beta, double *u,
 int ldu, double *v, int ldv, double *q, int 
 ldq, int *ncycle, int *info);

void dtpcon(char norm, char uplo, char diag, int n, 
 double *ap, double *rcond, int *info);

void dtprfs(char uplo, char trans, char diag, int n, 
 int nrhs, double *ap, double *b, int ldb, double *x, int ldx,
  double *ferr, double *berr, int *info);

void dtptri(char uplo, char diag, int n, double * ap, int *info);

void dtptrs(char uplo, char trans, char diag, int n, 
 int nrhs, double *ap, double *b, int ldb, int * info);

void dtrcon(char norm, char uplo, char diag, int n, 
 double *a, int lda, double *rcond, int *info);

void dtrevc(char side, char howmny, int *select, 
 int n, double *t, int ldt, double *vl, int ldvl, double *vr, 
 int ldvr, int mm, int *m, int *info);

void dtrexc(char compq, int n, double *t, int ldt, 
 double *q, int ldq, int *ifst, int *ilst, int *info);

void dtrrfs(char uplo, char trans, char diag, int n, 
 int nrhs, double *a, int lda, double *b, int ldb, double *x, 
 int ldx, double *ferr, double *berr, int *info);

void dtrsen(char job, char compq, int *select, int 
 n, double *t, int ldt, double *q, int ldq, double *wr, double *wi, 
 int *m, double *s, double *sep, int *info);

void dtrsna(char job, char howmny, int *select, 
 int n, double *t, int ldt, double *vl, int ldvl, double *vr, 
 int ldvr, double *s, double *sep, int mm, int *m, 
  int *info);

void dtrsyl(char trana, char tranb, int isgn, int 
 m, int n, double *a, int lda, double *b, int ldb, double *c, int ldc, double *scale, int *info);

void dtrti2(char uplo, char diag, int n, double * a, int lda, int *info);

void dtrtri(char uplo, char diag, int n, double * a, int lda, int *info);

void dtrtrs(char uplo, char trans, char diag, int n, 
 int nrhs, double *a, int lda, double *b, int ldb, int *info);

void dtzrqf(int m, int n, double *a, int lda, double *tau, int *info);

extern double dzsum1(int n, doublecomplex *cx, int incx);

int icmax1(int n, complex *cx, int incx);

int ilaenv(int ispec, char name, char opts, int n1, int n2, int n3, int n4 );

int izmax1(int n, doublecomplex *cx, int incx);

int lsame_(char *ca, char *cb);

int lsamen(int n, char ca, char *cb);

void sbdsqr(char uplo, int n, int ncvt, int nru, int ncc, float *d, float *e, float *vt, int ldvt, float *u,
  int ldu, float *c, int ldc, int *info);

extern float scsum1(int n, complex *cx, int incx);

void sdisna(char job, int m, int n, float *d, float 
 *sep, int *info);

extern float second();

void sgbbrd(char vect, int m, int n, int ncc,
  int kl, int ku, float *ab, int ldab, float *d, float *e, 
 float *q, int ldq, float *pt, int ldpt, float *c, int ldc,
  int *info);

void sgbcon(char norm, int n, int kl, int ku,
  float *ab, int ldab, int *ipiv, float anorm, float *rcond, 
 int *info);

void sgbequ(int m, int n, int kl, int ku,
  float *ab, int ldab, float *r, float *c, float *rowcnd, float *colcnd, float *amax, int *info);

void sgbrfs(char trans, int n, int kl, int ku, int nrhs, float *ab, int ldab, float *afb, int ldafb,
  int *ipiv, float *b, int ldb, float *x, int ldx, float *ferr, float *berr, int *info);

void sgbsv(int n, int kl, int ku, int nrhs, float *ab, int ldab, int *ipiv, float *b, int ldb, 
 int *info);

void sgbsvx(char fact, char trans, int n, int kl,
  int ku, int nrhs, float *ab, int ldab, float *afb, 
 int ldafb, int *ipiv, char *equed, float *r, float *c, float *b,
  int ldb, float *x, int ldx, float *rcond, float *ferr, float * berr, 
   int *info);

void sgbtf2(int m, int n, int kl, int ku,
  float *ab, int ldab, int *ipiv, int *info);

void sgbtrf(int m, int n, int kl, int ku,
  float *ab, int ldab, int *ipiv, int *info);

void sgbtrs(char trans, int n, int kl, int ku, int nrhs, float *ab, int ldab, int *ipiv, float *b, 
 int ldb, int *info);

void sgebak(char job, char side, int n, int ilo, 
 int ihi, float *scale, int m, float *v, int ldv, int 
 *info);

void sgebal(char job, int n, float *a, int lda, 
 int *ilo, int *ihi, float *scale, int *info);

void sgebd2(int m, int n, float *a, int lda, 
 float *d, float *e, float *tauq, float *taup, int *info);

void sgebrd(int m, int n, float *a, int lda, 
 float *d, float *e, float *tauq, float *taup, int *info);

void sgecon(char norm, int n, float *a, int lda, 
 float anorm, float *rcond, int *info);

void sgeequ(int m, int n, float *a, int lda, 
 float *r, float *c, float *rowcnd, float *colcnd, float *amax, int *info);

void sgees(char jobvs, char sort, int (*select)(), int n, 
 float *a, int lda, int *sdim, float *wr, float *wi, float *vs, 
 int ldvs, int * info);

void sgeesx(char jobvs, char sort, int (*select)(), char sense, int n, float *a, int lda, int *sdim, float *wr, 
 float *wi, float *vs, int ldvs, float *rconde, float *rcondv, int *info);

void sgeev(char jobvl, char jobvr, int n, float *a, 
 int lda, float *wr, float *wi, float *vl, int ldvl, float *vr, 
 int ldvr, int *info);

void sgeevx(char balanc, char jobvl, char jobvr, char sense, int n, float *a, int lda, float *wr, float *wi, float *
 vl, int ldvl, float *vr, int ldvr, int *ilo, int *ihi, float *scale, float *abnrm, float *rconde, float *rcondv, int *info);

void sgegs(char jobvsl, char jobvsr, int n, float *a, 
 int lda, float *b, int ldb, float *alphar, float *alphai, float 
 *beta, float *vsl, int ldvsl, float *vsr, int ldvsr, int *info);

void sgegv(char jobvl, char jobvr, int n, float *a, 
 int lda, float *b, int ldb, float *alphar, float *alphai, float 
 *beta, float *vl, int ldvl, float *vr, int ldvr, int *info);

void sgehd2(int n, int ilo, int ihi, float *a, 
 int lda, float *tau, int *info);

void sgehrd(int n, int ilo, int ihi, float *a, 
 int lda, float *tau, int *info);

void sgelq2(int m, int n, float *a, int lda, 
 float *tau, int *info);

void sgelqf(int m, int n, float *a, int lda, 
 float *tau, int *info);

void sgels(char trans, int m, int n, int nrhs, float *a, int lda, float *b, int ldb, int *info);

void sgelss(int m, int n, int nrhs, float *a, 
 int lda, float *b, int ldb, float *s, float rcond, int *rank,
  int *info);

void sgelsx(int m, int n, int nrhs, float *a, 
 int lda, float *b, int ldb, int *jpvt, float rcond, 
 int *rank, int *info);

void sgeql2(int m, int n, float *a, int lda, 
 float *tau, int *info);

void sgeqlf(int m, int n, float *a, int lda, 
 float *tau, int *info);

void sgeqpf(int m, int n, float *a, int lda, 
 int *jpvt, float *tau, int *info);

void sgeqr2(int m, int n, float *a, int lda, 
 float *tau, int *info);

void sgeqrf(int m, int n, float *a, int lda, 
 float *tau, int *info);

void sgerfs(char trans, int n, int nrhs, float *a, 
 int lda, float *af, int ldaf, int *ipiv, float *b, 
 int ldb, float *x, int ldx, float *ferr, float *berr, int *info);

void sgerq2(int m, int n, float *a, int lda, 
 float *tau, int *info);

void sgerqf(int m, int n, float *a, int lda, 
 float *tau, int *info);

void sgesv(int n, int nrhs, float *a, int lda, 
 int *ipiv, float *b, int ldb, int *info);

void sgesvd(char jobu, char jobvt, int m, int n, 
 float *a, int lda, float *s, float *u, int ldu, float *vt, 
 int ldvt, int *info);

void sgesvx(char fact, char trans, int n, int nrhs, float *a, int lda, float *af, int ldaf, int *ipiv, 
 char *equed, float *r, float *c, float *b, int ldb, float *x, 
 int ldx, float *rcond, float *ferr, float *berr, int *info);

void sgetf2(int m, int n, float *a, int lda, 
 int *ipiv, int *info);

void sgetrf(int m, int n, float *a, int lda, 
 int *ipiv, int *info);

void sgetri(int n, float *a, int lda, int *ipiv,
  int *info);

void sgetrs(char trans, int n, int nrhs, float *a, 
 int lda, int *ipiv, float *b, int ldb, int *info);

void sggbak(char job, char side, int n, int ilo, 
 int ihi, float *lscale, float *rscale, int m, float *v, 
 int ldv, int *info);

void sggbal(char job, int n, float *a, int lda, 
 float *b, int ldb, int *ilo, int *ihi, float *lscale, float 
 *rscale, int *info);

void sggglm(int n, int m, int p, float *a, 
 int lda, float *b, int ldb, float *d, float *x, float *y, int *info);

void sgghrd(char compq, char compz, int n, int ilo, int ihi, float *a, int lda, float *b, int ldb, float 
 *q, int ldq, float *z, int ldz, int *info);

void sgglse(int m, int n, int p, float *a, 
 int lda, float *b, int ldb, float *c, float *d, float *x, int *info);

void sggqrf(int n, int m, int p, float *a, 
 int lda, float *taua, float *b, int ldb, float *taub, int *info);

void sggrqf(int m, int p, int n, float *a, 
 int lda, float *taua, float *b, int ldb, float *taub, int *info);

void sggsvd(char jobu, char jobv, char jobq, int m, 
 int n, int p, int *k, int *l, float *a, int lda,
  float *b, int ldb, float *alpha, float *beta, float *u, int ldu, float *v, int ldv, float *q, int ldq, int *info);

void sggsvp(char jobu, char jobv, char jobq, int m, 
 int p, int n, float *a, int lda, float *b, int ldb, 
 float tola, float tolb, int *k, int *l, float *u, int ldu,
  float *v, int ldv, float *q, int ldq, float * tau, int *info);

void sgtcon(char norm, int n, float *dl, float *d, float * du, float *du2, int *ipiv, float anorm, float *rcond, int *info);

void sgtrfs(char trans, int n, int nrhs, float *dl,
  float *d, float *du, float *dlf, float *df, float *duf, float *du2, 
 int *ipiv, float *b, int ldb, float *x, int ldx, float * ferr, float *berr, int *info);

void sgtsv(int n, int nrhs, float *dl, float *d, 
 float *du, float *b, int ldb, int *info);

void sgtsvx(char fact, char trans, int n, int nrhs, float *dl, float *d, float *du, float *dlf, float *df, float *duf, 
 float *du2, int *ipiv, float *b, int ldb, float *x, int ldx, float *rcond, float *ferr, float *berr, int *info);

void sgttrf(int n, float *dl, float *d, float *du, float * du2, int *ipiv, int *info);

void sgttrs(char trans, int n, int nrhs, float *dl,
  float *d, float *du, float *du2, int *ipiv, float *b, int ldb, 
 int *info);

void shgeqz(char job, char compq, char compz, int n, 
 int ilo, int ihi, float *a, int lda, float *b, int ldb, float *alphar, float *alphai, float *beta, float *q, int ldq, 
 float *z, int ldz, int *info);

void shsein(char side, char eigsrc, char initv, int * select, int n, float *h, int ldh, float *wr, float *wi, float *
 vl, int ldvl, float *vr, int ldvr, int mm, int *m, 
 int *ifaill, int *ifailr, int *info);

void shseqr(char job, char compz, int n, int ilo,
  int ihi, float *h, int ldh, float *wr, float *wi, float *z, 
 int ldz, int *info);

void slabad(float *small, float *large);

void slabrd(int m, int n, int nb, float *a, 
 int lda, float *d, float *e, float *tauq, float *taup, float *x, 
 int ldx, float *y, int *ldy);

void slacon(int n, float *v, float *x, int *isgn, 
 float *est, int *kase);

void slacpy(char uplo, int m, int n, float *a, 
 int lda, float *b, int ldb);

void sladiv(float a, float b, float c, float d, float *p, 
 float *q);

void slae2(float a, float b, float c, float *rt1, float *rt2);

void slaebz(int ijob, int nitmax, int n, 
 int mmax, int minp, int nbmin, float abstol, float reltol, float pivmin, float *d, float *e, float *e2, int *nval, float 
 *ab, float *c, int *mout, int *nab, 
  int *info);

void slaed0(int icompq, int qsiz, int n, float 
 *d, float *e, float *q, int ldq, float *qstore, int ldqs, int *info);

void slaed1(int n, float *d, float *q, int ldq, 
 int *indxq, float rho, int cutpnt, int *info);

void slaed2(int *k, int n, float *d, float *q, 
 int ldq, int *indxq, float *rho, int cutpnt, float *z, 
 float *dlamda, float *q2, int ldq2, int *indxc, float *w, 
 int *indxp, int *indx, int *coltyp, int *info);

void slaed3(int k, int kstart, int kstop, 
 int n, float *d, float *q, int ldq, float rho, int cutpnt, float *dlamda, float *q2, int ldq2, int *indxc, 
 int *ctot, float *w, float *s, int lds, int *info);

void slaed4(int n, int i, float *d, float *z, float * delta, float rho, float *dlam, int *info);

void slaed5(int i, float *d, float *z, float *delta, float rho, float *dlam);

void slaed6(int kniter, int orgati, float rho, 
 float *d, float *z, float finit, float *tau, int *info);

void slaed7(int icompq, int n, int qsiz, 
 int tlvls, int curlvl, int curpbm, float *d, float *q, 
 int ldq, int *indxq, float rho, int cutpnt, float * qstore, int *qptr, int *prmptr, int *perm, int *
 givptr, int *givcol, float *givnum, int *info);

void slaed8(int icompq, int *k, int n, int 
 qsiz, float *d, float *q, int ldq, int *indxq, float *rho, 
 int cutpnt, float *z, float *dlamda, float *q2, int ldq2, float 
 *w, int *perm, int *givptr, int *givcol, float *givnum, 
 int *indxp, int *indx, int *info);

void slaed9(int k, int kstart, int kstop, 
 int n, float *d, float *q, int ldq, float rho, float *dlamda, 
 float *w, float *s, int lds, int *info);

void slaeda(int n, int tlvls, int curlvl, 
 int curpbm, int *prmptr, int *perm, int *givptr, 
 int *givcol, float *givnum, float *q, int *qptr, float *z, float * ztemp, int *info);

void slaein(int rightv, int noinit, int n, 
 float *h, int ldh, float wr, float wi, float *vr, float *vi, float * b, int ldb, float eps3, float smlnum, float bignum, 
 int *info);

void slaev2(float a, float b, float c, float *rt1, float *rt2, 
 float *cs1, float *sn1);

void slaexc(int wantq, int n, float *t, int ldt, float *q, int ldq, int j1, int n1, int n2, 
 int *info);

void slag2(float *a, int lda, float *b, int ldb, 
 float safmin, float *scale1, float *scale2, float *wr1, float *wr2, float * wi);

void slags2(int upper, float a1, float a2, float a3, 
 float b1, float b2, float b3, float *csu, float *snu, float *csv, float * snv, float *csq, float *snq);

void slagtf(int n, float *a, float lambda, float *b, float 
 *c, float tol, float *d, int *in, int *info);

void slagtm(char trans, int n, int nrhs, float alpha, float *dl, float *d, float *du, float *x, int ldx, float beta,
  float *b, int ldb);

void slagts(int job, int n, float *a, float *b, float 
 *c, float *d, int *in, float *y, float *tol, int *info);

void slahqr(int wantt, int wantz, int n, 
 int ilo, int ihi, float *h, int ldh, float *wr, float *wi,
  int iloz, int ihiz, float *z, int ldz, int *info);

void slahrd(int n, int k, int nb, float *a, 
 int lda, float *tau, float *t, int ldt, float *y, int ldy);

void slaic1(int job, int j, float *x, float sest, 
 float *w, float gamma, float *sestpr, float *s, float *c);

void slaln2(int ltrans, int na, int nw, float smin, float ca, float *a, int lda, float d1, float d2, float *b, 
 int ldb, float wr, float wi, float *x, int ldx, float *scale, 
 float *xnorm, int *info);

extern float slamch(char cmach);

void slamrg(int n1, int n2, float *a, int strd1, int strd2, int *index);

extern float slangb(char norm, int n, int kl, int ku, float *ab,
  int ldab );

extern float slange(char norm, int m, int n, float *a, int lda );

extern float slangt(char norm, int n, float *dl, float *d, float *du);

extern float slanhs(char norm, int n, float *a, int lda);

extern float slansb(char norm, char uplo, int n, int k, float *ab, 
 int ldab );

extern float slansp(char norm, char uplo, int n, float *ap);

extern float slanst(char norm, int n, float *d, float *e);

extern float slansy(char norm, char uplo, int n, float *a, int lda);

extern float slantb(char norm, char uplo, char diag, int n, int k,
  float *ab, int ldab);

extern float slantp(char norm, char uplo, char diag, int n, float *ap);

extern float slantr(char norm, char uplo, char diag, int m, int n,
  float *a, int lda);

void slanv2(float *a, float *b, float *c, float *d, float *rt1r, 
 float *rt1i, float *rt2r, float *rt2i, float *cs, float *sn);

void slapll(int n, float *x, int incx, float *y, 
 int incy, float *ssmin);

void slapmt(int forwrd, int m, int n, float *x,
  int ldx, int *k);

extern float slapy2(float x, float y);

extern float slapy3(float x, float y, float z);

void slaqgb(int m, int n, int kl, int ku,
  float *ab, int ldab, float *r, float *c, float *rowcnd, float * colcnd, float amax, char *equed);

void slaqge(int m, int n, float *a, int lda, 
 float *r, float *c, float rowcnd, float colcnd, float amax, char *equed);

void slaqsb(char uplo, int n, int kd, float *ab, 
 int ldab, float *s, float scond, float amax, char *equed);

void slaqsp(char uplo, int n, float *ap, float *s, float scond, float amax, char *equed);

void slaqsy(char uplo, int n, float *a, int lda, 
 float *s, float scond, float amax, char *equed);

void slaqtr(int ltran, int lfloat, int n, float 
 *t, int ldt, float *b, float w, float *scale, float *x, 
 int *info);

void slar2v(int n, float *x, float *y, float *z, int incx, float *c, float *s, int incc);

void slarf(char side, int m, int n, float *v, 
 int incv, float tau, float *c, int ldc);

void slarfb(char side, char trans, char direct, char storev, int m, int n, int k, float *v, int ldv, 
 float *t, int ldt, float *c, int ldc);

void slarfg(int n, float *alpha, float *x, int incx, 
 float *tau);

void slarft(char direct, char storev, int n, int k, float *v, int ldv, float *tau, float *t, int ldt);

void slarfx(char side, int m, int n, float *v, 
 float tau, float *c, int ldc);

void slargv(int n, float *x, int incx, float *y, 
 int incy, float *c, int incc);

void slarnv(int idist, int *iseed, int n, float 
 *x);

void slartg(float f, float g, float *cs, float *sn, float *r);

void slartv(int n, float *x, int incx, float *y, 
 int incy, float *c, float *s, int incc);

void slaruv(int *iseed, int n, float *x);

void slas2(float f, float g, float h, float *ssmin, float * ssmax);

void slascl(char type, int kl, int ku, float cfrom, float cto, int m, int n, float *a, int lda, 
 int *info);

void slaset(char uplo, int m, int n, float alpha, 
 float beta, float *a, int lda);

void slasq1(int n, float *d, float *e, int *info);

void slasq2(int m, float *q, float *e, float *qq, float *ee,
  float eps, float tol2, float small2, float *sup, int *kend, 
 int *info);

void slasq3(int *n, float *q, float *e, float *qq, float *ee,
  float *sup, float *sigma, int *kend, int *off, int *iphase,
  int iconv, float eps, float tol2, float small2);

void slasq4(int n, float *q, float *e, float *tau, float * sup);

void slasr(char side, char pivot, char direct, int m,
  int n, float *c, float *s, float *a, int lda);

void slasrt(char id, int n, float *d, int *info);

void slassq(int n, float *x, int incx, float *scale, 
 float *sumsq);

void slasv2(float f, float g, float h, float *ssmin, float * ssmax, float *snr, float *csr, float *snl, float *csl);

void slaswp(int n, float *a, int lda, int k1, 
 int k2, int *ipiv, int incx);

void slasy2(int ltranl, int ltranr, int isgn, 
 int n1, int n2, float *tl, int ldtl, float *tr, int ldtr, float *b, int ldb, float *scale, float *x, int ldx, float 
 *xnorm, int *info);

void slasyf(char uplo, int n, int nb, int *kb,
  float *a, int lda, int *ipiv, float *w, int ldw, int 
 *info);

void slatbs(char uplo, char trans, char diag, char normin, int n, int kd, float *ab, int ldab, float *x, 
 float *scale, float *cnorm, int *info);

void slatps(char uplo, char trans, char diag, char normin, int n, float *ap, float *x, float *scale, float *cnorm, 
 int *info);

void slatrd(char uplo, int n, int nb, float *a, 
 int lda, float *e, float *tau, float *w, int ldw);

void slatrs(char uplo, char trans, char diag, char normin, int n, float *a, int lda, float *x, float *scale, float 
 *cnorm, int *info);

void slatzm(char side, int m, int n, float *v, 
 int incv, float tau, float *c1, float *c2, int ldc);

void slauu2(char uplo, int n, float *a, int lda, 
 int *info);

void slauum(char uplo, int n, float *a, int lda, 
 int *info);

void slazro(int m, int n, float alpha, float beta, 
            float *a, int lda);

void sopgtr(char uplo, int n, float *ap, float *tau, 
 float *q, int ldq, int *info);

void sopmtr(char side, char uplo, char trans, int m, 
 int n, float *ap, float *tau, float *c, int ldc, int *info);

void sorg2l(int m, int n, int k, float *a, 
 int lda, float *tau, int *info);

void sorg2r(int m, int n, int k, float *a, 
 int lda, float *tau, int *info);

void sorgbr(char vect, int m, int n, int k, 
 float *a, int lda, float *tau, int 
 *info);

void sorghr(int n, int ilo, int ihi, float *a, 
 int lda, float *tau, int *info);

void sorgl2(int m, int n, int k, float *a, 
 int lda, float *tau, int *info);

void sorglq(int m, int n, int k, float *a, 
 int lda, float *tau, int *info);

void sorgql(int m, int n, int k, float *a, 
 int lda, float *tau, int *info);

void sorgqr(int m, int n, int k, float *a, 
 int lda, float *tau, int *info);

void sorgr2(int m, int n, int k, float *a, 
 int lda, float *tau, int *info);

void sorgrq(int m, int n, int k, float *a, 
 int lda, float *tau, int *info);

void sorgtr(char uplo, int n, float *a, int lda, 
 float *tau, int *info);

void sorm2l(char side, char trans, int m, int n, 
 int k, float *a, int lda, float *tau, float *c, int ldc, 
 int *info);

void sorm2r(char side, char trans, int m, int n, 
 int k, float *a, int lda, float *tau, float *c, int ldc, 
 int *info);

void sormbr(char vect, char side, char trans, int m, 
 int n, int k, float *a, int lda, float *tau, float *c, 
 int ldc, int *info);

void sormhr(char side, char trans, int m, int n, 
 int ilo, int ihi, float *a, int lda, float *tau, float *c,
  int ldc, int *info);

void sorml2(char side, char trans, int m, int n, 
 int k, float *a, int lda, float *tau, float *c, int ldc, 
 int *info);

void sormlq(char side, char trans, int m, int n, 
 int k, float *a, int lda, float *tau, float *c, int ldc, 
 int *info);

void sormql(char side, char trans, int m, int n, 
 int k, float *a, int lda, float *tau, float *c, int ldc, 
 int *info);

void sormqr(char side, char trans, int m, int n, 
 int k, float *a, int lda, float *tau, float *c, int ldc, 
 int *info);

void sormr2(char side, char trans, int m, int n, 
 int k, float *a, int lda, float *tau, float *c, int ldc, 
 int *info);

void sormrq(char side, char trans, int m, int n, 
 int k, float *a, int lda, float *tau, float *c, int ldc, 
 int *info);

void sormtr(char side, char uplo, char trans, int m, 
 int n, float *a, int lda, float *tau, float *c, int ldc, 
 int *info);

void spbcon(char uplo, int n, int kd, float *ab, 
 int ldab, float anorm, float *rcond, int *info);

void spbequ(char uplo, int n, int kd, float *ab, 
 int ldab, float *s, float *scond, float *amax, int *info);

void spbrfs(char uplo, int n, int kd, int nrhs, float *ab, int ldab, float *afb, int ldafb, float *b, 
 int ldb, float *x, int ldx, float *ferr, float *berr, int *info);

void spbstf(char uplo, int n, int kd, float *ab, 
 int ldab, int *info);

void spbsv(char uplo, int n, int kd, int nrhs, float *ab, int ldab, float *b, int ldb, int *info);

void spbsvx(char fact, char uplo, int n, int kd, 
 int nrhs, float *ab, int ldab, float *afb, int ldafb, 
 char *equed, float *s, float *b, int ldb, float *x, int ldx, 
 float *rcond, float *ferr, float *berr, int *info);

void spbtf2(char uplo, int n, int kd, float *ab, 
 int ldab, int *info);

void spbtrf(char uplo, int n, int kd, float *ab, 
 int ldab, int *info);

void spbtrs(char uplo, int n, int kd, int nrhs, float *ab, int ldab, float *b, int ldb, int *info);

void spocon(char uplo, int n, float *a, int lda, 
 float anorm, float *rcond, int *info);

void spoequ(int n, float *a, int lda, float *s, float 
 *scond, float *amax, int *info);

void sporfs(char uplo, int n, int nrhs, float *a, 
 int lda, float *af, int ldaf, float *b, int ldb, float *x,
  int ldx, float *ferr, float *berr, int *info);

void sposv(char uplo, int n, int nrhs, float *a, 
 int lda, float *b, int ldb, int *info);

void sposvx(char fact, char uplo, int n, int nrhs, float *a, int lda, float *af, int ldaf, char *equed, 
 float *s, float *b, int ldb, float *x, int ldx, float *rcond, 
 float *ferr, float *berr, int *info);

void spotf2(char uplo, int n, float *a, int lda, 
 int *info);

void spotrf(char uplo, int n, float *a, int lda, 
 int *info);

void spotri(char uplo, int n, float *a, int lda, 
 int *info);

void spotrs(char uplo, int n, int nrhs, float *a, 
 int lda, float *b, int ldb, int *info);

void sppcon(char uplo, int n, float *ap, float anorm, 
 float *rcond, int *info);

void sppequ(char uplo, int n, float *ap, float *s, float * scond, float *amax, int *info);

void spprfs(char uplo, int n, int nrhs, float *ap, 
 float *afp, float *b, int ldb, float *x, int ldx, float *ferr, 
 float *berr, int *info);

void sppsv(char uplo, int n, int nrhs, float *ap, 
 float *b, int ldb, int *info);

void sppsvx(char fact, char uplo, int n, int nrhs, float *ap, float *afp, char *equed, float *s, float *b, int 
 ldb, float *x, int ldx, float *rcond, float *ferr, float *berr, int *info);

void spptrf(char uplo, int n, float *ap, int *info);

void spptri(char uplo, int n, float *ap, int *info);

void spptrs(char uplo, int n, int nrhs, float *ap, 
 float *b, int ldb, int *info);

void sptcon(int n, float *d, float *e, float anorm, float * rcond, int *info);

void spteqr(char compz, int n, float *d, float *e, float * z, int ldz, int *info);

void sptrfs(int n, int nrhs, float *d, float *e, 
 float *df, float *ef, float *b, int ldb, float *x, int ldx, 
 float *ferr, float *berr, int *info);

void sptsv(int n, int nrhs, float *d, float *e, float 
 *b, int ldb, int *info);

void sptsvx(char fact, int n, int nrhs, float *d, 
 float *e, float *df, float *ef, float *b, int ldb, float *x, int ldx, float *rcond, float *ferr, float *berr, int *info);

void spttrf(int n, float *d, float *e, int *info);

void spttrs(int n, int nrhs, float *d, float *e, 
 float *b, int ldb, int *info);

void srscl(int n, float sa, float *sx, int incx);

void ssbev(char jobz, char uplo, int n, int kd, 
 float *ab, int ldab, float *w, float *z, int ldz, int *info);

void ssbevd(char jobz, char uplo, int n, int kd, 
 float *ab, int ldab, float *w, float *z, int ldz, int *info);

void ssbevx(char jobz, char range, char uplo, int n, 
 int kd, float *ab, int ldab, float *q, int ldq, float vl,
  float vu, int il, int iu, float abstol, int *m, float * w, float *z, int ldz, int *ifail, 
 int *info);

void ssbgst(char vect, char uplo, int n, int ka, 
 int kb, float *ab, int ldab, float *bb, int ldbb, float * x, int ldx, int *info);

void ssbgv(char jobz, char uplo, int n, int ka, 
 int kb, float *ab, int ldab, float *bb, int ldbb, float * w, float *z, int ldz, int *info);

void ssbtrd(char vect, char uplo, int n, int kd, 
 float *ab, int ldab, float *d, float *e, float *q, int ldq, 
 int *info);

void sspcon(char uplo, int n, float *ap, int *ipiv, 
 float anorm, float *rcond, int *info);

void sspev(char jobz, char uplo, int n, float *ap, 
 float *w, float *z, int ldz, int *info);

void sspevd(char jobz, char uplo, int n, float *ap, 
 float *w, float *z, int ldz, int *info);

void sspevx(char jobz, char range, char uplo, int n, 
 float *ap, float vl, float vu, int il, int iu, float abstol, 
 int *m, float *w, float *z, int ldz, int *ifail, int *info);

void sspgst(int itype, char uplo, int n, float *ap,
  float *bp, int *info);

void sspgv(int itype, char jobz, char uplo, int n, float *ap, float *bp, float *w, float *z, int ldz, int *info);

void ssprfs(char uplo, int n, int nrhs, float *ap, 
 float *afp, int *ipiv, float *b, int ldb, float *x, int ldx, float *ferr, float *berr, int *
 info);

void sspsv(char uplo, int n, int nrhs, float *ap, 
 int *ipiv, float *b, int ldb, int *info);

void sspsvx(char fact, char uplo, int n, int nrhs, float *ap, float *afp, int *ipiv, float *b, int ldb, float 
 *x, int ldx, float *rcond, float *ferr, float *berr, int *info);

void ssptrd(char uplo, int n, float *ap, float *d, float * e, float *tau, int *info);

void ssptrf(char uplo, int n, float *ap, int *ipiv, 
 int *info);

void ssptri(char uplo, int n, float *ap, int *ipiv, 
 int *info);

void ssptrs(char uplo, int n, int nrhs, float *ap, 
 int *ipiv, float *b, int ldb, int *info);

void sstebz(char range, char order, int n, float vl, 
 float vu, int il, int iu, float abstol, float *d, float *e, 
 int *m, int *nsplit, float *w, int *iblock, int *isplit, int *info);

void sstedc(char compz, int n, float *d, float *e, float * z, int ldz, int *info);

void sstein(int n, float *d, float *e, int m, float * w, int *iblock, int *isplit, float *z, int ldz, int *ifail, int *info);

void ssteqr(char compz, int n, float *d, float *e, float * z, int ldz, int *info);

void ssterf(int n, float *d, float *e, int *info);

void sstev(char jobz, int n, float *d, float *e, float *z,
  int ldz, int *info);

void sstevd(char jobz, int n, float *d, float *e, float * z, int ldz, int *info);

void sstevx(char jobz, char range, int n, float *d, 
 float *e, float vl, float vu, int il, int iu, float abstol, 
 int *m, float *w, float *z, int ldz, int *ifail, int *info);

void ssycon(char uplo, int n, float *a, int lda, 
 int *ipiv, float anorm, float *rcond, int *info);

void ssyev(char jobz, char uplo, int n, float *a, 
 int lda, float *w, int *info);

void ssyevd(char jobz, char uplo, int n, float *a, 
 int lda, float *w, int *info);

void ssyevx(char jobz, char range, char uplo, int n, 
 float *a, int lda, float vl, float vu, int il, int iu, 
 float abstol, int *m, float *w, float *z, int ldz, 
  int *ifail, int *info);

void ssygs2(int itype, char uplo, int n, float *a, 
 int lda, float *b, int ldb, int *info);

void ssygst(int itype, char uplo, int n, float *a, 
 int lda, float *b, int ldb, int *info);

void ssygv(int itype, char jobz, char uplo, int n, float *a, int lda, float *b, int ldb, float *w, int *info);

void ssyrfs(char uplo, int n, int nrhs, float *a, 
 int lda, float *af, int ldaf, int *ipiv, float *b, 
 int ldb, float *x, int ldx, float *ferr, float *berr, int *info);

void ssysv(char uplo, int n, int nrhs, float *a, 
 int lda, int *ipiv, float *b, int ldb, int *info);

void ssysvx(char fact, char uplo, int n, int nrhs, float *a, int lda, float *af, int ldaf, int *ipiv, 
 float *b, int ldb, float *x, int ldx, float *rcond, float *ferr,
  float *berr, int * info);

void ssytd2(char uplo, int n, float *a, int lda, 
 float *d, float *e, float *tau, int *info);

void ssytf2(char uplo, int n, float *a, int lda, 
 int *ipiv, int *info);

void ssytrd(char uplo, int n, float *a, int lda, 
 float *d, float *e, float *tau, int * info);

void ssytrf(char uplo, int n, float *a, int lda, 
 int *ipiv, int *info);

void ssytri(char uplo, int n, float *a, int lda, 
 int *ipiv, int *info);

void ssytrs(char uplo, int n, int nrhs, float *a, 
 int lda, int *ipiv, float *b, int ldb, int *info);

void stbcon(char norm, char uplo, char diag, int n, 
 int kd, float *ab, int ldab, float *rcond, int *info);

void stbrfs(char uplo, char trans, char diag, int n, 
 int kd, int nrhs, float *ab, int ldab, float *b, int 
 ldb, float *x, int ldx, float *ferr, float *berr, int *info);

void stbtrs(char uplo, char trans, char diag, int n, 
 int kd, int nrhs, float *ab, int ldab, float *b, int 
 ldb, int *info);

void stgevc(char side, char howmny, int *select, 
 int n, float *a, int lda, float *b, int ldb, float *vl, 
 int ldvl, float *vr, int ldvr, int mm, int *m, int *info);

void stgsja(char jobu, char jobv, char jobq, int m, 
 int p, int n, int k, int l, float *a, int lda,
  float *b, int ldb, float tola, float tolb, float *alpha, float * beta, float *u, int ldu, float *v, int ldv, float *q, int 
 ldq, int *ncycle, int *info);

void stpcon(char norm, char uplo, char diag, int n, 
 float *ap, float *rcond, int *info);

void stprfs(char uplo, char trans, char diag, int n, 
 int nrhs, float *ap, float *b, int ldb, float *x, int ldx,
  float *ferr, float *berr, int *info);

void stptri(char uplo, char diag, int n, float *ap, 
 int *info);

void stptrs(char uplo, char trans, char diag, int n, 
 int nrhs, float *ap, float *b, int ldb, int *info);

void strcon(char norm, char uplo, char diag, int n, 
 float *a, int lda, float *rcond, int *info);

void strevc(char side, char howmny, int *select, 
 int n, float *t, int ldt, float *vl, int ldvl, float *vr, 
 int ldvr, int mm, int *m, int *info);

void strexc(char compq, int n, float *t, int ldt, 
 float *q, int ldq, int *ifst, int *ilst, int *info);

void strrfs(char uplo, char trans, char diag, int n, 
 int nrhs, float *a, int lda, float *b, int ldb, float *x, 
 int ldx, float *ferr, float *berr, int *info);

void strsen(char job, char compq, int *select, int 
 n, float *t, int ldt, float *q, int ldq, float *wr, float *wi, 
 int *m, float *s, float *sep, int *info);

void strsna(char job, char howmny, int *select, 
 int n, float *t, int ldt, float *vl, int ldvl, float *vr, 
 int ldvr, float *s, float *sep, int mm, int *m, 
  int *info);

void strsyl(char trana, char tranb, int isgn, int 
 m, int n, float *a, int lda, float *b, int ldb, float *c,
  int ldc, float *scale, int *info);

void strti2(char uplo, char diag, int n, float *a, 
 int lda, int *info);

void strtri(char uplo, char diag, int n, float *a, 
 int lda, int *info);

void strtrs(char uplo, char trans, char diag, int n, 
 int nrhs, float *a, int lda, float *b, int ldb, int * info);

void stzrqf(int m, int n, float *a, int lda, 
 float *tau, int *info);

void zbdsqr(char uplo, int n, int ncvt, int nru, int ncc, double *d, double *e, doublecomplex *vt, int ldvt, 
 doublecomplex *u, int ldu, doublecomplex *c, int ldc, int *info);

void zdrot(int n, doublecomplex *cx, int incx, 
 doublecomplex *cy, int incy, double *c, double *s);

void zdrscl(int n, double sa, doublecomplex *sx, 
 int incx);

void zgbbrd(char vect, int m, int n, int ncc,
  int kl, int ku, doublecomplex *ab, int ldab, double *d, double * e, doublecomplex *q, int ldq, doublecomplex *pt, int ldpt, doublecomplex *c, 
 int ldc, int *info);

void zgbcon(char norm, int n, int kl, int ku,
  doublecomplex *ab, int ldab, int *ipiv, double anorm, double *rcond, 
 int *info);

void zgbequ(int m, int n, int kl, int ku,
  doublecomplex *ab, int ldab, double *r, double *c, 
 double *rowcnd, double *colcnd, double *amax, int * info);

void zgbrfs(char trans, int n, int kl, int ku, int nrhs, doublecomplex *ab, int ldab, doublecomplex *afb, int 
 ldafb, int *ipiv, doublecomplex *b, int ldb, doublecomplex *x, int ldx, double *ferr, double *berr, int * info);

void zgbsv(int n, int kl, int ku, int nrhs, doublecomplex *ab, int ldab, int *ipiv, doublecomplex *
 b, int ldb, int *info);

void zgbsvx(char fact, char trans, int n, int kl,
  int ku, int nrhs, doublecomplex *ab, int ldab, doublecomplex *afb,
  int ldafb, int *ipiv, char *equed, double *r, double *c, 
 doublecomplex *b, int ldb, doublecomplex *x, int ldx, double *rcond, double 
 *ferr, double *berr, int *info);

void zgbtf2(int m, int n, int kl, int ku,
  doublecomplex *ab, int ldab, int *ipiv, int *info);

void zgbtrf(int m, int n, int kl, int ku,
  doublecomplex *ab, int ldab, int *ipiv, int *info);

void zgbtrs(char trans, int n, int kl, int ku, int nrhs, doublecomplex *ab, int ldab, int *ipiv, 
 doublecomplex *b, int ldb, int *info);

void zgebak(char job, char side, int n, int ilo, 
 int ihi, double *scale, int m, doublecomplex *v, 
 int ldv, int *info);

void zgebal(char job, int n, doublecomplex *a, int 
 lda, int *ilo, int *ihi, double *scale, int *info);

void zgebd2(int m, int n, doublecomplex *a, int lda,
  double *d, double *e, doublecomplex *tauq, doublecomplex *taup, int *info);

void zgebrd(int m, int n, doublecomplex *a, int lda,
  double *d, double *e, doublecomplex *tauq, doublecomplex *taup, int *info);

void zgecon(char norm, int n, doublecomplex *a, int lda,
  double anorm, double *rcond, int *info);

void zgeequ(int m, int n, doublecomplex *a, 
 int lda, double *r, double *c, double *rowcnd, 
 double *colcnd, double *amax, int *info);

void zgees(char jobvs, char sort, int (*select)(), int n, 
 doublecomplex *a, int lda, int *sdim, doublecomplex *w, doublecomplex *vs, 
 int ldvs, int *info);

void zgeesx(char jobvs, char sort, int (*select)(), char sense, int n, doublecomplex *a, int lda, int *sdim, doublecomplex *
 w, doublecomplex *vs, int ldvs, double *rconde, double *rcondv, int *info);

void zgeev(char jobvl, char jobvr, int n, doublecomplex *a, 
 int lda, doublecomplex *w, doublecomplex *vl, int ldvl, doublecomplex *vr, 
 int ldvr, int * info);

void zgeevx(char balanc, char jobvl, char jobvr, char sense, int n, doublecomplex *a, int lda, doublecomplex *w, doublecomplex *vl, 
 int ldvl, doublecomplex *vr, int ldvr, int *ilo, int *ihi,
  double *scale, double *abnrm, double *rconde, double *rcondv, int *info);

void zgegs(char jobvsl, char jobvsr, int n, doublecomplex * a, int lda, doublecomplex *b, int ldb, doublecomplex *alpha, doublecomplex *
 beta, doublecomplex *vsl, int ldvsl, doublecomplex *vsr, int ldvsr, 
 int *info);

void zgegv(char jobvl, char jobvr, int n, doublecomplex *a, 
 int lda, doublecomplex *b, int ldb, doublecomplex *alpha, doublecomplex *beta,
  doublecomplex *vl, int ldvl, doublecomplex *vr, int ldvr, int *info);

void zgehd2(int n, int ilo, int ihi, doublecomplex * a, int lda, doublecomplex *tau, int *info);

void zgehrd(int n, int ilo, int ihi, doublecomplex * a, int lda, doublecomplex *tau, int *info);

void zgelq2(int m, int n, doublecomplex *a, int lda,
  doublecomplex *tau, int *info);

void zgelqf(int m, int n, doublecomplex *a, int lda,
  doublecomplex *tau, int *info);

void zgels(char trans, int m, int n, int nrhs, doublecomplex *a, int lda, doublecomplex *b, int ldb, int *info);

void zgelss(int m, int n, int nrhs, doublecomplex * a, int lda, doublecomplex *b, int ldb, double *s, double rcond, 
 int *rank, int * info);

void zgelsx(int m, int n, int nrhs, doublecomplex * a, int lda, doublecomplex *b, int ldb, int *jpvt, double rcond,
  int *rank, int *info);

void zgeql2(int m, int n, doublecomplex *a, int lda,
  doublecomplex *tau, int *info);

void zgeqlf(int m, int n, doublecomplex *a, int lda,
  doublecomplex *tau, int *info);

void zgeqpf(int m, int n, doublecomplex *a, int lda,
  int *jpvt, doublecomplex *tau, int * info);

void zgeqr2(int m, int n, doublecomplex *a, int lda,
  doublecomplex *tau, int *info);

void zgeqrf(int m, int n, doublecomplex *a, int lda,
  doublecomplex *tau, int *info);

void zgerfs(char trans, int n, int nrhs, doublecomplex * a, int lda, doublecomplex *af, int ldaf, int *ipiv, doublecomplex *
 b, int ldb, doublecomplex *x, int ldx, double *ferr, double *berr, 
 int *info);

void zgerq2(int m, int n, doublecomplex *a, int lda,
  doublecomplex *tau, int *info);

void zgerqf(int m, int n, doublecomplex *a, int lda,
  doublecomplex *tau, int *info);

void zgesv(int n, int nrhs, doublecomplex *a, 
 int lda, int *ipiv, doublecomplex *b, int ldb, int * info);

void zgesvd(char jobu, char jobvt, int m, int n, 
 doublecomplex *a, int lda, double *s, doublecomplex *u, int ldu, doublecomplex * vt, int ldvt, int *info);

void zgesvx(char fact, char trans, int n, int nrhs, doublecomplex *a, int lda, doublecomplex *af, int ldaf, int *
 ipiv, char *equed, double *r, double *c, doublecomplex *b, int ldb, 
 doublecomplex *x, int ldx, double *rcond, double *ferr, double *berr, 
 int *info);

void zgetf2(int m, int n, doublecomplex *a, 
 int lda, int *ipiv, int *info);

void zgetrf(int m, int n, doublecomplex *a, 
 int lda, int *ipiv, int *info);

void zgetri(int n, doublecomplex *a, int lda, int * ipiv, int *info);

void zgetrs(char trans, int n, int nrhs, 
 doublecomplex *a, int lda, int *ipiv, doublecomplex *b, 
 int ldb, int *info);

void zggbak(char job, char side, int n, int ilo, 
 int ihi, double *lscale, double *rscale, int m, 
 doublecomplex *v, int ldv, int *info);

void zggbal(char job, int n, doublecomplex *a, int lda, 
 doublecomplex *b, int ldb, int *ilo, int *ihi, double *lscale, 
 double *rscale, int *info);

void zggglm(int n, int m, int p, doublecomplex *a, 
 int lda, doublecomplex *b, int ldb, doublecomplex *d, doublecomplex *x, 
 doublecomplex *y, int *info);

void zgghrd(char compq, char compz, int n, int ilo, int ihi, doublecomplex *a, int lda, doublecomplex *b, 
 int ldb, doublecomplex *q, int ldq, doublecomplex *z, 
 int ldz, int *info);

void zgglse(int m, int n, int p, doublecomplex *a, 
 int lda, doublecomplex *b, int ldb, doublecomplex *c, doublecomplex *d, 
 doublecomplex *x, int *info);

void zggqrf(int n, int m, int p, doublecomplex *a, 
 int lda, doublecomplex *taua, doublecomplex *b, int ldb, doublecomplex *taub, 
 int *info);

void zggrqf(int m, int p, int n, doublecomplex *a, 
 int lda, doublecomplex *taua, doublecomplex *b, int ldb, doublecomplex *taub, 
 int *info);

void zggsvd(char jobu, char jobv, char jobq, int m, 
 int n, int p, int *k, int *l, doublecomplex *a, int lda, doublecomplex *b, int ldb, double *alpha, double *beta,
        doublecomplex *u, int ldu, doublecomplex *v, int ldv, doublecomplex *q, int ldq, int *info);

void zggsvp(char jobu, char jobv, char jobq, int m, 
 int p, int n, doublecomplex *a, int lda, doublecomplex *b, int 
 ldb, double tola, double tolb, int *k, int *l, doublecomplex *u, 
 int ldu, doublecomplex *v, int ldv, doublecomplex *q, int ldq, 
 doublecomplex *tau, int * info);

void zgtcon(char norm, int n, doublecomplex *dl, doublecomplex *d, 
 doublecomplex *du, doublecomplex *du2, int *ipiv, double anorm, double *rcond, 
 int *info);

void zgtrfs(char trans, int n, int nrhs, doublecomplex *dl, doublecomplex *d, doublecomplex *du, doublecomplex *dlf, doublecomplex *df,
        doublecomplex *duf, doublecomplex *du2, int *ipiv, doublecomplex *b, int ldb, doublecomplex *x, int ldx, double *ferr,
        double *berr, int *info);

void zgtsv(int n, int nrhs, doublecomplex *dl, 
 doublecomplex *d, doublecomplex *du, doublecomplex *b, int ldb, 
 int *info);

void zgtsvx(char fact, char trans, int n, int nrhs, doublecomplex *dl, doublecomplex *d, doublecomplex *du, doublecomplex *dlf, doublecomplex *df,
  doublecomplex *duf, doublecomplex *du2, int *ipiv, doublecomplex *b, int ldb, 
 doublecomplex *x, int ldx, double *rcond, double *ferr, double *berr, 
 int *info);

void zgttrf(int n, doublecomplex *dl, doublecomplex *d, 
 doublecomplex *du, doublecomplex *du2, int *ipiv, int *info);

void zgttrs(char trans, int n, int nrhs, 
 doublecomplex *dl, doublecomplex *d, doublecomplex *du, doublecomplex 
 *du2, int *ipiv, doublecomplex *b, int ldb, int *info);

void zhbev(char jobz, char uplo, int n, int kd, 
 doublecomplex *ab, int ldab, double *w, doublecomplex *z, int ldz, 
 int *info);

void zhbevd(char jobz, char uplo, int n, int kd, 
 doublecomplex *ab, int ldab, double *w, doublecomplex *z, int ldz, 
 int *info);

void zhbevx(char jobz, char range, char uplo, int n, 
 int kd, doublecomplex *ab, int ldab, doublecomplex *q, int ldq, 
 double vl, double vu, int il, int iu, double abstol, int * m, double *w, doublecomplex *z, int ldz, int *ifail, int *info);

void zhbgst(char vect, char uplo, int n, int ka, 
 int kb, doublecomplex *ab, int ldab, doublecomplex *bb, int ldbb, 
 doublecomplex *x, int ldx, int *info);

void zhbgv(char jobz, char uplo, int n, int ka, 
 int kb, doublecomplex *ab, int ldab, doublecomplex *bb, int ldbb, 
 double *w, doublecomplex *z, int ldz, int *info);

void zhbtrd(char vect, char uplo, int n, int kd, 
 doublecomplex *ab, int ldab, double *d, double *e, doublecomplex *q, int ldq, int *info);

void zhecon(char uplo, int n, doublecomplex *a, int lda,
  int *ipiv, double anorm, double *rcond, int * info);

void zheev(char jobz, char uplo, int n, doublecomplex *a, 
 int lda, double *w, int *info);

void zheevd(char jobz, char uplo, int n, doublecomplex *a, 
 int lda, double *w, int *info);

void zheevx(char jobz, char range, char uplo, int n, 
 doublecomplex *a, int lda, double vl, double vu, int il, int iu, double abstol, int *m, double *w,
        doublecomplex *z, int ldz, int * ifail, int *info);

void zhegs2(int itype, char uplo, int n, 
 doublecomplex *a, int lda, doublecomplex *b, int ldb, 
 int *info);

void zhegst(int itype, char uplo, int n, 
 doublecomplex *a, int lda, doublecomplex *b, int ldb, 
 int *info);

void zhegv(int itype, char jobz, char uplo, int n, doublecomplex *a, int lda, doublecomplex *b, int ldb, double *w, int *info);

void zherfs(char uplo, int n, int nrhs, doublecomplex * a, int lda, doublecomplex *af, int ldaf, int *ipiv, doublecomplex *
 b, int ldb, doublecomplex *x, int ldx, double *ferr, double *berr, 
 int *info);

void zhesv(char uplo, int n, int nrhs, doublecomplex *a,
  int lda, int *ipiv, doublecomplex *b, int ldb, int *info);

void zhesvx(char fact, char uplo, int n, int nrhs, doublecomplex *a, int lda, doublecomplex *af, int ldaf, int *
 ipiv, doublecomplex *b, int ldb, doublecomplex *x, int ldx, double *rcond,
  double *ferr, double *berr, int *info);

void zhetd2(char uplo, int n, doublecomplex *a, 
 int lda, double *d, double *e, doublecomplex *tau, 
 int *info);

void zhetf2(char uplo, int n, doublecomplex *a, 
 int lda, int *ipiv, int *info);

void zhetrd(char uplo, int n, doublecomplex *a, int lda,
  double *d, double *e, doublecomplex *tau, int *info);

void zhetrf(char uplo, int n, doublecomplex *a, int lda,
  int *ipiv, int *info);

void zhetri(char uplo, int n, doublecomplex *a, int lda,
  int *ipiv, int *info);

void zhetrs(char uplo, int n, int nrhs, 
 doublecomplex *a, int lda, int *ipiv, doublecomplex *b, 
 int ldb, int *info);

void zhgeqz(char job, char compq, char compz, int n, 
 int ilo, int ihi, doublecomplex *a, int lda, doublecomplex *b, 
 int ldb, doublecomplex *alpha, doublecomplex *beta, doublecomplex *q, int ldq,
  doublecomplex *z, int ldz, int *info);

void zhpcon(char uplo, int n, doublecomplex *ap, int * ipiv, double anorm, double *rcond, int *info);

void zhpev(char jobz, char uplo, int n, doublecomplex *ap, 
 double *w, doublecomplex *z, int ldz, int *info);

void zhpevd(char jobz, char uplo, int n, doublecomplex *ap, 
 double *w, doublecomplex *z, int ldz, int *info);

void zhpevx(char jobz, char range, char uplo, int n, 
 doublecomplex *ap, double vl, double vu, int il, int iu, double abstol, int *m, double *w, doublecomplex *z,
        int ldz, int *ifail, int *info);

void zhpgst(int itype, char uplo, int n, 
 doublecomplex *ap, doublecomplex *bp, int *info);

void zhpgv(int itype, char jobz, char uplo, int n, doublecomplex *ap, doublecomplex *bp, double *w, doublecomplex *z, int ldz, 
 int *info);

void zhprfs(char uplo, int n, int nrhs, doublecomplex * ap, doublecomplex *afp, int *ipiv, doublecomplex *b, int ldb, doublecomplex *x,
  int ldx, double *ferr, double *berr, int *info);

void zhpsv(char uplo, int n, int nrhs, 
 doublecomplex *ap, int *ipiv, doublecomplex *b, int ldb, 
 int *info);

void zhpsvx(char fact, char uplo, int n, int nrhs, doublecomplex *ap, doublecomplex *afp, int *ipiv, doublecomplex *b, int 
 ldb, doublecomplex *x, int ldx, double *rcond, double *ferr, double *berr, 
 int *info);

void zhptrd(char uplo, int n, doublecomplex *ap, 
 double *d, double *e, doublecomplex *tau, int *info);

void zhptrf(char uplo, int n, doublecomplex *ap, 
 int *ipiv, int *info);

void zhptri(char uplo, int n, doublecomplex *ap, int * ipiv, int *info);

void zhptrs(char uplo, int n, int nrhs, 
 doublecomplex *ap, int *ipiv, doublecomplex *b, int ldb, 
 int *info);

void zhsein(char side, char eigsrc, char initv, int * select, int n, doublecomplex *h, int ldh, doublecomplex *w, doublecomplex *vl,
  int ldvl, doublecomplex *vr, int ldvr, int mm, int *m, 
 int *ifaill, int *ifailr, int 
 *info);

void zhseqr(char job, char compz, int n, int ilo,
  int ihi, doublecomplex *h, int ldh, doublecomplex *w, doublecomplex *z, 
 int ldz, int *info);

void zlabrd(int m, int n, int nb, 
 doublecomplex *a, int lda, double *d, double *e, 
 doublecomplex *tauq, doublecomplex *taup, doublecomplex *x, int ldx, doublecomplex *y, int *ldy);

void zlacgv(int n, doublecomplex *x, int incx);

void zlacon(int n, doublecomplex *v, doublecomplex *x, 
 double *est, int *kase);

void zlacpy(char uplo, int m, int n, 
 doublecomplex *a, int lda, doublecomplex *b, int ldb);

void zlacrm(int m, int n, doublecomplex *a, int lda,
  double *b, int ldb, doublecomplex *c, int ldc);

void zlacrt(int n, doublecomplex *cx, int incx, 
 doublecomplex *cy, int incy, doublecomplex *c, doublecomplex *s);

extern doublecomplex *zladiv(doublecomplex *x, doublecomplex *y);

void zlaed0(int qsiz, int n, double *d, double *e, 
 doublecomplex *q, int ldq, doublecomplex *qstore, int ldqs, int *info);

void zlaed7(int n, int cutpnt, int qsiz, 
 int tlvls, int curlvl, int curpbm, double *d, doublecomplex *q,
  int ldq, double rho, int *indxq, double *qstore, int *qptr,
  int *prmptr, int *perm, int *givptr, int *givcol, 
 double *givnum, int * info);

void zlaed8(int *k, int n, int qsiz, 
 doublecomplex *q, int ldq, double *d, double *rho, 
 int cutpnt, double *z, double *dlamda, doublecomplex *q2,
  int ldq2, double *w, int *indxp, int *indx, int 
 *indxq, int *perm, int *givptr, int *givcol, double * givnum, int *info);

void zlaein(int rightv, int noinit, int n, 
 doublecomplex *h, int ldh, doublecomplex *w, doublecomplex *v, doublecomplex *b, int 
 ldb, double eps3, double smlnum, int *info);

void zlaesy(doublecomplex *a, doublecomplex *b, 
 doublecomplex *c, doublecomplex *rt1, doublecomplex *rt2, 
 doublecomplex *evscal, doublecomplex *cs1, doublecomplex *sn1);

void zlaev2(doublecomplex *a, doublecomplex *b, 
 doublecomplex *c, double *rt1, double *rt2, double *cs1, 
 doublecomplex *sn1);

void zlags2(int upper, double a1, doublecomplex * a2, double a3, double b1, doublecomplex *b2, double b3,
  double *csu, doublecomplex *snu, double *csv, doublecomplex * snv, double *csq, doublecomplex *snq);

void zlagtm(char trans, int n, int nrhs, 
 double alpha, doublecomplex *dl, doublecomplex *d, doublecomplex 
 *du, doublecomplex *x, int ldx, double beta, doublecomplex * b, int ldb);

void zlahef(char uplo, int n, int nb, int *kb,
  doublecomplex *a, int lda, int *ipiv, doublecomplex *w, 
 int ldw, int *info);

void zlahqr(int wantt, int wantz, int n, 
 int ilo, int ihi, doublecomplex *h, int ldh, 
 doublecomplex *w, int iloz, int ihiz, doublecomplex *z, 
 int ldz, int *info);

void zlahrd(int n, int k, int nb, 
 doublecomplex *a, int lda, doublecomplex *tau, doublecomplex *t, 
 int ldt, doublecomplex *y, int ldy);

void zlaic1(int job, int j, doublecomplex *x, 
 double sest, doublecomplex *w, doublecomplex *gamma, double * sestpr, doublecomplex *s, doublecomplex *c);

extern double zlangb(char norm, int n, int kl, int ku, doublecomplex * ab, int ldab);

extern double zlange(char norm, int m, int n, doublecomplex *a, int lda);

extern double zlangt(char norm, int n, doublecomplex *dl, doublecomplex * d, doublecomplex *du);

extern double zlanhb(char norm, char uplo, int n, int k, doublecomplex * ab, int ldab);

extern double zlanhe(char norm, char uplo, int n, doublecomplex *a, int lda);

extern double zlanhp(char norm, char uplo, int n, doublecomplex *ap);

extern double zlanhs(char norm, int n, doublecomplex *a, int lda);

extern double zlanht(char norm, int n, double *d, doublecomplex *e);

extern double zlansb(char norm, char uplo, int n, int k, doublecomplex * ab, int ldab);

extern double zlansp(char norm, char uplo, int n, doublecomplex *ap);

extern double zlanst(char norm, int n, double *d, doublecomplex *e);

extern double zlansy(char norm, char uplo, int n, doublecomplex *a, int lda);

extern double zlantb(char norm, char uplo, char diag, int n, int k,
  doublecomplex *ab, int ldab);

extern double zlantp(char norm, char uplo, char diag, int n, doublecomplex * ap);

extern double zlantr(char norm, char uplo, char diag, int m, int n,
  doublecomplex *a, int lda);

void zlapll(int n, doublecomplex *x, int incx, 
 doublecomplex *y, int incy, double *ssmin);

void zlapmt(int forwrd, int m, int n, 
 doublecomplex *x, int ldx, int *k);

void zlaqgb(int m, int n, int kl, int ku,
  doublecomplex *ab, int ldab, double *r, double *c, 
 double *rowcnd, double *colcnd, double amax, char *equed);

void zlaqge(int m, int n, doublecomplex *a, 
 int lda, double *r, double *c, double rowcnd, 
 double colcnd, double amax, char *equed);

void zlaqhb(char uplo, int n, int kd, 
 doublecomplex *ab, int ldab, double *s, double scond, 
 double amax, char *equed);

void zlaqhe(char uplo, int n, doublecomplex *a, 
 int lda, double *s, double scond, double amax, 
 char *equed);

void zlaqhp(char uplo, int n, doublecomplex *ap, 
 double *s, double scond, double amax, char *equed);

void zlaqsb(char uplo, int n, int kd, 
 doublecomplex *ab, int ldab, double *s, double scond, 
 double amax, char *equed);

void zlaqsp(char uplo, int n, doublecomplex *ap, 
 double *s, double scond, double amax, char *equed);

void zlaqsy(char uplo, int n, doublecomplex *a, 
 int lda, double *s, double scond, double amax, 
 char *equed);

void zlar2v(int n, doublecomplex *x, doublecomplex *y, 
 doublecomplex *z, int incx, double *c, doublecomplex *s, 
 int incc);

void zlarf(char side, int m, int n, doublecomplex *v, 
 int incv, doublecomplex *tau, doublecomplex *c, int ldc);

void zlarfb(char side, char trans, char direct, char storev, int m, int n, int k, doublecomplex *v, int ldv, 
 doublecomplex *t, int ldt, doublecomplex *c, int ldc);

void zlarfg(int n, doublecomplex *alpha, doublecomplex * x, int incx, doublecomplex *tau);

void zlarft(char direct, char storev, int n, int k, doublecomplex *v, int ldv, doublecomplex *tau, doublecomplex *
 t, int ldt);

void zlarfx(char side, int m, int n, doublecomplex *v, 
 doublecomplex *tau, doublecomplex *c, int ldc);

void zlargv(int n, doublecomplex *x, int incx, 
 doublecomplex *y, int incy, double *c, int incc);

void zlarnv(int idist, int *iseed, int n, 
 doublecomplex *x);

void zlartg(doublecomplex *f, doublecomplex *g, double * cs, doublecomplex *sn, doublecomplex *r);

void zlartv(int n, doublecomplex *x, int incx, 
 doublecomplex *y, int incy, double *c, doublecomplex *s, 
 int incc);

void zlascl(char type, int kl, int ku, double 
 cfrom, double cto, int m, int n, doublecomplex *a, 
 int lda, int *info);

void zlaset(char uplo, int m, int n, 
 doublecomplex *alpha, doublecomplex *beta, doublecomplex *a, int lda);

void zlasr(char side, char pivot, char direct, int m,
  int n, double *c, double *s, doublecomplex *a, int lda);

void zlassq(int n, doublecomplex *x, int incx, 
 double *scale, double *sumsq);

void zlaswp(int n, doublecomplex *a, int lda, 
 int k1, int k2, int *ipiv, int incx);

void zlasyf(char uplo, int n, int nb, int *kb,
  doublecomplex *a, int lda, int *ipiv, doublecomplex *w, 
 int ldw, int *info);

void zlatbs(char uplo, char trans, char diag, char normin, int n, int kd, doublecomplex *ab, int ldab, 
 doublecomplex *x, double *scale, double *cnorm, int *info);

void zlatps(char uplo, char trans, char diag, char normin, int n, doublecomplex *ap, doublecomplex *x, double *
 scale, double *cnorm, int *info);

void zlatrd(char uplo, int n, int nb, 
 doublecomplex *a, int lda, double *e, doublecomplex *tau, 
 doublecomplex *w, int ldw);

void zlatrs(char uplo, char trans, char diag, char normin, int n, doublecomplex *a, int lda, doublecomplex *x, 
 double *scale, double *cnorm, int *info);

void zlatzm(char side, int m, int n, doublecomplex *v, 
 int incv, doublecomplex *tau, doublecomplex *c1, doublecomplex *c2, int ldc);

void zlauu2(char uplo, int n, doublecomplex *a, 
 int lda, int *info);

void zlauum(char uplo, int n, doublecomplex *a, 
 int lda, int *info);

void zlazro(int m, int n, doublecomplex *alpha, doublecomplex *beta, doublecomplex *a, 
           int lda);

void zpbcon(char uplo, int n, int kd, doublecomplex *ab,
  int ldab, double anorm, double *rcond, int *info);

void zpbequ(char uplo, int n, int kd, 
 doublecomplex *ab, int ldab, double *s, double *scond, 
 double *amax, int *info);

void zpbrfs(char uplo, int n, int kd, int nrhs, doublecomplex *ab, int ldab, doublecomplex *afb, int ldafb, 
 doublecomplex *b, int ldb, doublecomplex *x, int ldx, double *ferr, double * berr, int *info);

void zpbstf(char uplo, int n, int kd, 
 doublecomplex *ab, int ldab, int *info);

void zpbsv(char uplo, int n, int kd, int nrhs, doublecomplex *ab, int ldab, doublecomplex *b, int 
 ldb, int *info);

void zpbsvx(char fact, char uplo, int n, int kd, 
 int nrhs, doublecomplex *ab, int ldab, doublecomplex *afb, int ldafb, char *equed, double *s, doublecomplex *b,
        int ldb, doublecomplex *x, int ldx, double *rcond, double *ferr, double *berr, int *info);

void zpbtf2(char uplo, int n, int kd, 
 doublecomplex *ab, int ldab, int *info);

void zpbtrf(char uplo, int n, int kd, 
 doublecomplex *ab, int ldab, int *info);

void zpbtrs(char uplo, int n, int kd, int nrhs, doublecomplex *ab, int ldab, doublecomplex *b, int 
 ldb, int *info);

void zpocon(char uplo, int n, doublecomplex *a, int lda,
  double anorm, double *rcond, int *info);

void zpoequ(int n, doublecomplex *a, int lda, 
 double *s, double *scond, double *amax, int *info);

void zporfs(char uplo, int n, int nrhs, doublecomplex * a, int lda, doublecomplex *af, int ldaf, doublecomplex *b, int ldb,
  doublecomplex *x, int ldx, double *ferr, double *berr, int *info);

void zposv(char uplo, int n, int nrhs, 
 doublecomplex *a, int lda, doublecomplex *b, int ldb, 
 int *info);

void zposvx(char fact, char uplo, int n, int nrhs, doublecomplex *a, int lda, doublecomplex *af, int ldaf, char *
 equed, double *s, doublecomplex *b, int ldb, doublecomplex *x, int ldx, 
 double *rcond, double *ferr, double *berr, int *info);

void zpotf2(char uplo, int n, doublecomplex *a, 
 int lda, int *info);

void zpotrf(char uplo, int n, doublecomplex *a, 
 int lda, int *info);

void zpotri(char uplo, int n, doublecomplex *a, 
 int lda, int *info);

void zpotrs(char uplo, int n, int nrhs, 
 doublecomplex *a, int lda, doublecomplex *b, int ldb, 
 int *info);

void zppcon(char uplo, int n, doublecomplex *ap, double anorm,
  double *rcond, int *info);

void zppequ(char uplo, int n, doublecomplex *ap, 
 double *s, double *scond, double *amax, int *info);

void zpprfs(char uplo, int n, int nrhs, doublecomplex * ap, doublecomplex *afp, doublecomplex *b, int ldb, doublecomplex *x, int ldx, 
 double *ferr, double *berr, int *info);

void zppsv(char uplo, int n, int nrhs, 
 doublecomplex *ap, doublecomplex *b, int ldb, int *info);

void zppsvx(char fact, char uplo, int n, int nrhs, doublecomplex *ap, doublecomplex *afp, char *equed, double *s, doublecomplex *b, 
 int ldb, doublecomplex *x, int ldx, double *rcond, double *ferr, double 
 *berr, int *info);

void zpptrf(char uplo, int n, doublecomplex *ap, 
 int *info);

void zpptri(char uplo, int n, doublecomplex *ap, 
 int *info);

void zpptrs(char uplo, int n, int nrhs, 
 doublecomplex *ap, doublecomplex *b, int ldb, int *info);

void zptcon(int n, double *d, doublecomplex *e, double anorm, 
 double *rcond, int *info);

void zpteqr(char compz, int n, double *d, double *e, 
 doublecomplex *z, int ldz, int *info);

void zptrfs(char uplo, int n, int nrhs, double *d, 
 doublecomplex *e, double *df, doublecomplex *ef, doublecomplex *b, int ldb, doublecomplex * x, int ldx, double *ferr, double *berr, int *info);

void zptsv(int n, int nrhs, double *d, 
 doublecomplex *e, doublecomplex *b, int ldb, int *info);

void zptsvx(char fact, int n, int nrhs, double *d, 
 doublecomplex *e, double *df, doublecomplex *ef, doublecomplex *b, int ldb, doublecomplex * x, int ldx, double *rcond, double *ferr, double *berr, int *info);

void zpttrf(int n, double *d, doublecomplex *e, 
 int *info);

void zpttrs(char uplo, int n, int nrhs, 
 double *d, doublecomplex *e, doublecomplex *b, int ldb, 
 int *info);

void zrot(int n, doublecomplex *cx, int incx, 
 doublecomplex *cy, int incy, double c, doublecomplex *s);

void zspcon(char uplo, int n, doublecomplex *ap, int * ipiv, double anorm, double *rcond, int *info);

void zspmv(char uplo, int n, doublecomplex *alpha, 
 doublecomplex *ap, doublecomplex *x, int incx, doublecomplex * beta, doublecomplex *y, int incy);

void zspr(char uplo, int n, doublecomplex *alpha, 
 doublecomplex *x, int incx, doublecomplex *ap);

void zsprfs(char uplo, int n, int nrhs, doublecomplex * ap, doublecomplex *afp, int *ipiv, doublecomplex *b, int ldb, doublecomplex *x,
  int ldx, double *ferr, double *berr, 
 int *info);

void zspsv(char uplo, int n, int nrhs, 
 doublecomplex *ap, int *ipiv, doublecomplex *b, int ldb, 
 int *info);

void zspsvx(char fact, char uplo, int n, int nrhs, doublecomplex *ap, doublecomplex *afp, int *ipiv, doublecomplex *b, int 
 ldb, doublecomplex *x, int ldx, double *rcond, double *ferr, double *berr, 
 int *info);

void zsptrf(char uplo, int n, doublecomplex *ap, 
 int *ipiv, int *info);

void zsptri(char uplo, int n, doublecomplex *ap, int * ipiv, int *info);

void zsptrs(char uplo, int n, int nrhs, 
 doublecomplex *ap, int *ipiv, doublecomplex *b, int ldb, 
 int *info);

void zstedc(char compz, int n, double *d, double *e, 
 doublecomplex *z, int ldz, int *info);

void zstein(int n, double *d, double *e, int m, double * w, int *iblock, int *isplit, doublecomplex *z, int ldz, int *ifail, int *info);

void zsteqr(char compz, int n, double *d, double *e, 
 doublecomplex *z, int ldz, int *info);

void zsycon(char uplo, int n, doublecomplex *a, int lda,
  int *ipiv, double anorm, double *rcond, int * info);

void zsymv(char uplo, int n, doublecomplex *alpha, 
 doublecomplex *a, int lda, doublecomplex *x, int incx, 
 doublecomplex *beta, doublecomplex *y, int incy);

void zsyr(char uplo, int n, doublecomplex *alpha, 
 doublecomplex *x, int incx, doublecomplex *a, int lda);

void zsyrfs(char uplo, int n, int nrhs, doublecomplex * a, int lda, doublecomplex *af, int ldaf, int *ipiv, doublecomplex *
 b, int ldb, doublecomplex *x, int ldx, double *ferr, double *berr, 
 int *info);

void zsysv(char uplo, int n, int nrhs, doublecomplex *a,
  int lda, int *ipiv, doublecomplex *b, int ldb, int *info);

void zsysvx(char fact, char uplo, int n, int nrhs, doublecomplex *a, int lda, doublecomplex *af, int ldaf, int *
 ipiv, doublecomplex *b, int ldb, doublecomplex *x, int ldx, double *rcond,
  double *ferr, double *berr, int *info);

void zsytf2(char uplo, int n, doublecomplex *a, 
 int lda, int *ipiv, int *info);

void zsytrf(char uplo, int n, doublecomplex *a, int lda,
  int *ipiv, int *info);

void zsytri(char uplo, int n, doublecomplex *a, int lda,
  int *ipiv, int *info);

void zsytrs(char uplo, int n, int nrhs, 
 doublecomplex *a, int lda, int *ipiv, doublecomplex *b, 
 int ldb, int *info);

void ztbcon(char norm, char uplo, char diag, int n, 
 int kd, doublecomplex *ab, int ldab, double *rcond, int *info);

void ztbrfs(char uplo, char trans, char diag, int n, 
 int kd, int nrhs, doublecomplex *ab, int ldab, doublecomplex *b, 
 int ldb, doublecomplex *x, int ldx, double *ferr, double *berr, 
 int *info);

void ztbtrs(char uplo, char trans, char diag, int n, 
 int kd, int nrhs, doublecomplex *ab, int ldab, 
 doublecomplex *b, int ldb, int *info);

void ztgevc(char side, char howmny, int *select, 
 int n, doublecomplex *a, int lda, doublecomplex *b, int ldb, 
 doublecomplex *vl, int ldvl, doublecomplex *vr, int ldvr, int mm, 
 int *m, int *info);

void ztgsja(char jobu, char jobv, char jobq, int m, 
 int p, int n, int k, int l, doublecomplex *a, int lda, doublecomplex *b, int ldb, double tola, double tolb,
        double *alpha, double *beta, doublecomplex *u, int ldu, doublecomplex *v, int ldv, 
 doublecomplex *q, int ldq, int *ncycle, int *info);

void ztpcon(char norm, char uplo, char diag, int n, 
 doublecomplex *ap, double *rcond, int *info);

void ztprfs(char uplo, char trans, char diag, int n, 
 int nrhs, doublecomplex *ap, doublecomplex *b, int ldb, doublecomplex *x, 
 int ldx, double *ferr, double *berr, int *info);

void ztptri(char uplo, char diag, int n, 
 doublecomplex *ap, int *info);

void ztptrs(char uplo, char trans, char diag, int n, 
 int nrhs, doublecomplex *ap, doublecomplex *b, int ldb, int *info);

void ztrcon(char norm, char uplo, char diag, int n, 
 doublecomplex *a, int lda, double *rcond, 
 int *info);

void ztrevc(char side, char howmny, int *select, 
 int n, doublecomplex *t, int ldt, doublecomplex *vl, int ldvl, 
 doublecomplex *vr, int ldvr, int mm, int *m, int *info);

void ztrexc(char compq, int n, doublecomplex *t, 
 int ldt, doublecomplex *q, int ldq, int ifst, int ilst, int *info);

void ztrrfs(char uplo, char trans, char diag, int n, 
 int nrhs, doublecomplex *a, int lda, doublecomplex *b, int ldb, 
 doublecomplex *x, int ldx, double *ferr, double *berr, int *info);

void ztrsen(char job, char compq, int *select, int 
 n, doublecomplex *t, int ldt, doublecomplex *q, int ldq, doublecomplex *w, 
 int *m, double *s, double *sep, int *info);

void ztrsna(char job, char howmny, int *select, 
 int n, doublecomplex *t, int ldt, doublecomplex *vl, int ldvl, 
 doublecomplex *vr, int ldvr, double *s, double *sep, int mm, int * m, int *info);

void ztrsyl(char trana, char tranb, int isgn, int 
 m, int n, doublecomplex *a, int lda, doublecomplex *b, 
 int ldb, doublecomplex *c, int ldc, double *scale, 
 int *info);

void ztrti2(char uplo, char diag, int n, 
 doublecomplex *a, int lda, int *info);

void ztrtri(char uplo, char diag, int n, 
 doublecomplex *a, int lda, int *info);

void ztrtrs(char uplo, char trans, char diag, int n, 
 int nrhs, doublecomplex *a, int lda, doublecomplex *b, 
 int ldb, int *info);

void ztzrqf(int m, int n, doublecomplex *a, 
 int lda, doublecomplex *tau, int *info);

void zung2l(int m, int n, int k, doublecomplex *a, 
 int lda, doublecomplex *tau, int *info);

void zung2r(int m, int n, int k, doublecomplex *a, 
 int lda, doublecomplex *tau, int *info);

void zungbr(char vect, int m, int n, int k, 
 doublecomplex *a, int lda, doublecomplex *tau, int *info);

void zunghr(int n, int ilo, int ihi, doublecomplex * a, int lda, doublecomplex *tau, int *info);

void zungl2(int m, int n, int k, doublecomplex *a, 
 int lda, doublecomplex *tau, int *info);

void zunglq(int m, int n, int k, doublecomplex *a, 
 int lda, doublecomplex *tau, int * info);

void zungql(int m, int n, int k, doublecomplex *a, 
 int lda, doublecomplex *tau, int *info);

void zungqr(int m, int n, int k, doublecomplex *a, 
 int lda, doublecomplex *tau, int * info);

void zungr2(int m, int n, int k, doublecomplex *a, 
 int lda, doublecomplex *tau, int *info);

void zungrq(int m, int n, int k, doublecomplex *a, 
 int lda, doublecomplex *tau, int *info);

void zungtr(char uplo, int n, doublecomplex *a, int lda,
  doublecomplex *tau, int *info);

void zunm2l(char side, char trans, int m, int n, 
 int k, doublecomplex *a, int lda, doublecomplex *tau, doublecomplex *c, 
 int ldc, int *info);

void zunm2r(char side, char trans, int m, int n, 
 int k, doublecomplex *a, int lda, doublecomplex *tau, doublecomplex *c, 
 int ldc, int *info);

void zunmbr(char vect, char side, char trans, int m, 
 int n, int k, doublecomplex *a, int lda, doublecomplex *tau, 
 doublecomplex *c, int ldc, int * info);

void zunmhr(char side, char trans, int m, int n, 
 int ilo, int ihi, doublecomplex *a, int lda, doublecomplex *tau, 
 doublecomplex *c, int ldc, int *info);

void zunml2(char side, char trans, int m, int n, 
 int k, doublecomplex *a, int lda, doublecomplex *tau, doublecomplex *c, 
 int ldc, int *info);

void zunmlq(char side, char trans, int m, int n, 
 int k, doublecomplex *a, int lda, doublecomplex *tau, doublecomplex *c, 
 int ldc, int *info);

void zunmql(char side, char trans, int m, int n, 
 int k, doublecomplex *a, int lda, doublecomplex *tau, doublecomplex *c, 
 int ldc, int *info);

void zunmqr(char side, char trans, int m, int n, 
 int k, doublecomplex *a, int lda, doublecomplex *tau, doublecomplex *c, 
 int ldc, int *info);

void zunmr2(char side, char trans, int m, int n, 
 int k, doublecomplex *a, int lda, doublecomplex *tau, doublecomplex *c, 
 int ldc, int *info);

void zunmrq(char side, char trans, int m, int n, 
 int k, doublecomplex *a, int lda, doublecomplex *tau, doublecomplex *c, 
 int ldc, int *info);

void zunmtr(char side, char uplo, char trans, int m, 
 int n, doublecomplex *a, int lda, doublecomplex *tau, doublecomplex *c, 
 int ldc, int *info);

void zupgtr(char uplo, int n, doublecomplex *ap, doublecomplex * tau, doublecomplex *q, int ldq, int *info);

void zupmtr(char side, char uplo, char trans, int m, 
 int n, doublecomplex *ap, doublecomplex *tau, doublecomplex *c, 
 int ldc, int *info);

#ifdef __cplusplus
}
#endif

#endif	/* !defined(_SUNPERF_H) */


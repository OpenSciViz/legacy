#pragma ident	"@(#)llib-lsunmath 1.4 93/11/14 SMI"

/* LINTLIBRARY */
/* PROTOLIB1 */

typedef unsigned fp_exception_field_type;

/* global declarations/variables */

extern int signgamf;
extern int signgaml;

typedef void (*sigfpe_handler_type)();

/* trigonometric functions */

extern float		cosf(float);
extern long double	cosl(long double);
extern double		cosd(double);
extern float		cosdf(float);
extern long double	cosdl(long double);
extern double		cosp(double);
extern float		cospf(float);
extern long double	cospl(long double);
extern double		cospi(double);
extern float		cospif(float);
extern long double	cospil(long double);
extern double		d_cos_(double *);
extern double		d_cosd_(double *);
extern double		d_cosp_(double *);
extern double		d_cospi_(double *);
extern float		r_cos_(float *);
extern float		r_cosd_(float *);
extern float		r_cosp_(float *);
extern float		r_cospi_(float *);
extern float		sinf(float);
extern long double	sinl(long double);
extern double		sind(double);
extern float		sindf(float);
extern long double	sindl(long double);
extern double		sinp(double);
extern float		sinpf(float);
extern long double	sinpl(long double);
extern double		sinpi(double);
extern float		sinpif(float);
extern long double	sinpil(long double);
extern double		d_sin_(double *);
extern double		d_sind_(double *);
extern double		d_sinp_(double *);
extern double		d_sinpi_(double *);
extern float		r_sin_(float *);
extern float		r_sind_(float *);
extern float		r_sinp_(float *);
extern float		r_sinpi_(float *);
extern float		tanf(float);
extern long double	tanl(long double);
extern double		tand(double);
extern float		tandf(float);
extern long double	tandl(long double);
extern double		tanp(double);
extern float		tanpf(float);
extern long double	tanpl(long double);
extern double		tanpi(double);
extern float		tanpif(float);
extern long double	tanpil(long double);
extern double		d_tan_(double *);
extern double		d_tand_(double *);
extern double		d_tanp_(double *);
extern double		d_tanpi_(double *);
extern float		r_tan_(float *);
extern float		r_tand_(float *);
extern float		r_tanp_(float *);
extern float		r_tanpi_(float *);
extern void		sincos(double, double *, double *);
extern void		sincosf(float, float *, float *);
extern void		sincosl(long double, long double *, long double *);
extern void		sincosd(double, double *, double *);
extern void		sincosdf(float, float *, float *);
extern void		sincosdl(long double, long double *, long double *);
extern void		sincosp(double, double *, double *);
extern void		sincospf(float, float *, float *);
extern void		sincospl(long double, long double *, long double *);
extern void		sincospi(double, double *, double *);
extern void		sincospif(float, float *, float *);
extern void		sincospil(long double, long double *, long double *);
extern void		d_sincos_(double *, double *, double *);
extern void		d_sincosd_(double *, double *, double *);
extern void		d_sincosp_(double *, double *, double *);
extern void		d_sincospi_(double *, double *, double *);
extern void		r_sincos_(float *, float *, float *);
extern void		r_sincosd_(float *, float *, float *);
extern void		r_sincosp_(float *, float *, float *);
extern void		r_sincospi_(float *, float *, float *);

extern float		acosf(float);
extern long double	acosl(long double);
extern double		acosd(double);
extern float		acosdf(float);
extern long double	acosdl(long double);
extern double		acosp(double);
extern float		acospf(float);
extern long double	acospl(long double);
extern double		acospi(double);
extern float		acospif(float);
extern long double	acospil(long double);
extern double		d_acos_(double *);
extern double		d_acosd_(double *);
extern double		d_acosp_(double *);
extern double		d_acospi_(double *);
extern float		r_acos_(float *);
extern float		r_acosd_(float *);
extern float		r_acosp_(float *);
extern float		r_acospi_(float *);
extern float		asinf(float);
extern long double	asinl(long double);
extern double		asind(double);
extern float		asindf(float);
extern long double	asindl(long double);
extern double		asinp(double);
extern float		asinpf(float);
extern long double	asinpl(long double);
extern double		asinpi(double);
extern float		asinpif(float);
extern long double	asinpil(long double);
extern double		d_asin_(double *);
extern double		d_asind_(double *);
extern double		d_asinp_(double *);
extern double		d_asinpi_(double *);
extern float		r_asin_(float *);
extern float		r_asind_(float *);
extern float		r_asinp_(float *);
extern float		r_asinpi_(float *);
extern float		atanf(float);
extern long double	atanl(long double);
extern double		atand(double);
extern float		atandf(float);
extern long double	atandl(long double);
extern double		atanp(double);
extern float		atanpf(float);
extern long double	atanpl(long double);
extern double		atanpi(double);
extern float		atanpif(float);
extern long double	atanpil(long double);
extern double		d_atan_(double *);
extern double		d_atand_(double *);
extern double		d_atanp_(double *);
extern double		d_atanpi_(double *);
extern float		r_atan_(float *);
extern float		r_atand_(float *);
extern float		r_atanp_(float *);
extern float		r_atanpi_(float *);
extern float		atan2f(float, float);
extern long double	atan2l(long double, long double);
extern double		atan2d(double, double);
extern float		atan2df(float, float);
extern long double	atan2dl(long double, long double);
extern double		atan2pi(double, double);
extern float		atan2pif(float, float);
extern long double	atan2pil(long double, long double);
extern double		d_atan2_(double *, double *);
extern double		d_atan2d_(double *, double *);
extern double		d_atan2pi_(double *, double *);
extern float		r_atan2_(float *, float *);
extern float		r_atan2d_(float *, float *);
extern float		r_atan2pi_(float *, float *);

/* hyperbolic functions */

extern float		coshf(float);
extern long double	coshl(long double);
extern double		d_cosh_(double *);
extern float		r_cosh_(float *);
extern float		sinhf(float);
extern long double	sinhl(long double);
extern double		d_sinh_(double *);
extern float		r_sinh_(float *);
extern float		tanhf(float);
extern long double	tanhl(long double);
extern double		d_tanh_(double *);
extern float		r_tanh_(float *);

extern float		acoshf(float);
extern long double	acoshl(long double);
extern double		d_acosh_(double *);
extern float		r_acosh_(float *);
extern float		asinhf(float);
extern long double	asinhl(long double);
extern double		d_asinh_(double *);
extern float		r_asinh_(float *);
extern float		atanhf(float);
extern long double	atanhl(long double);
extern double		d_atanh_(double *);
extern float		r_atanh_(float *);

/* aint, anint, ceil, floor,  rint,  irint,  nint  */

extern double		aint(double);
extern float		aintf(float);
extern long double	aintl(long double);
extern double		d_aint_(double *);
extern float		r_aint_(float *);
extern double		anint(double);
extern float		anintf(float);
extern long double	anintl(long double);
extern double		d_anint_(double *);
extern float		r_anint_(float *);
extern int		irint(double);
extern int		irintf(float);
extern int		irintl(long double);
extern int		id_irint_(double *);
extern int		ir_irint_(float *);
extern int		nint(double);
extern int		nintf(float);
extern int		nintl(long double);
extern int		id_nint_(double *);
extern int		ir_nint_(float *);
extern float		rintf(float);
extern long double	rintl(long double);
extern double		d_rint_(double *);
extern float		r_rint_(float *);
extern float		ceilf(float);
extern long double	ceill(long double);
extern double		d_ceil_(double *);
extern float		r_ceil_(float *);
extern float		floorf(float);
extern long double	floorl(long double);
extern double		d_floor_(double *);
extern float		r_floor_(float *);

/* exponential, logarithm, power, compound, annuity */

extern float		expf(float);
extern long double	expl(long double);
extern double		d_exp_(double *);
extern float		r_exp_(float *);
extern double		exp2(double);
extern float		exp2f(float);
extern long double	exp2l(long double);
extern double		d_exp2_(double *);
extern float		r_exp2_(float *);
extern double		exp10(double);
extern float		exp10f(float);
extern long double	exp10l(long double);
extern double		d_exp10_(double *);
extern float		r_exp10_(float *);
extern float		expm1f(float);
extern long double	expm1l(long double);
extern double		d_expm1_(double *);
extern float		r_expm1_(float *);
extern float		logf(float);
extern long double	logl(long double);
extern double		d_log_(double *);
extern float		r_log_(float *);
extern double		log2(double);
extern float		log2f(float);
extern long double	log2l(long double);
extern double		d_log2_(double *);
extern float		r_log2_(float *);
extern float		log10f(float);
extern long double	log10l(long double);
extern double		d_log10_(double *);
extern float		r_log10_(float *);
extern float		log1pf(float);
extern long double	log1pl(long double);
extern double		d_log1p_(double *);
extern float		r_log1p_(float *);
extern float		powf(float, float);
extern long double	powl(long double, long double);
extern double		d_pow_(double *, double *);
extern float		r_pow_(float *, float *);
extern double		compound(double, double);
extern float		compoundf(float, float);
extern long double	compoundl(long double, long double);
extern double		d_compound_(double *, double *);
extern float		r_compound_(float *, float *);
extern double		annuity(double, double);
extern float		annuityf(float, float);
extern long double	annuityl(long double, long double);
extern double		d_annuity_(double *, double *);
extern float		r_annuity_(float *, float *);

/* IEEE functions */

enum fp_class_type {
	fp_zero         = 0,
	fp_subnormal    = 1,
	fp_normal       = 2,
	fp_infinity     = 3,
	fp_quiet        = 4,
	fp_signaling    = 5
};

extern enum fp_class_type fp_class(double);
extern enum fp_class_type fp_classf(float);
extern enum fp_class_type fp_classl(long double);
extern enum fp_class_type id_fp_class_(double *);
extern enum fp_class_type ir_fp_class_(float *);
extern enum fp_class_type iq_fp_class_(long double *);
extern int		finitef(float);
extern int		finitel(long double);
extern int		id_finite_(double *);
extern int		ir_finite_(float *);
extern int		iq_finite_(long double *);
extern int		ilogbf(float);
extern int		ilogbl(long double);
extern int		id_ilogb_(double *);
extern int		ir_ilogb_(float *);
extern int		iq_ilogb_(long double *);
extern int		isinf(double);
extern int		isinff(float);
extern int		isinfl(long double);
extern int		id_isinf_(double *);
extern int		ir_isinf_(float *);
extern int		iq_isinf_(long double *);
extern int		isnanf(float);
extern int		isnanl(long double);
extern int		id_isnan_(double *);
extern int		ir_isnan_(float *);
extern int		iq_isnan_(long double *);
extern int		isnormal(double);
extern int		isnormalf(float);
extern int		isnormall(long double);
extern int		id_isnormal_(double *);
extern int		ir_isnormal_(float *);
extern int		iq_isnormal_(long double *);
extern int		issubnormal(double);
extern int		issubnormalf(float);
extern int		issubnormall(long double);
extern int		id_issubnormal_(double *);
extern int		ir_issubnormal_(float *);
extern int		iq_issubnormal_(long double *);
extern int		iszero(double);
extern int		iszerof(float);
extern int		iszerol(long double);
extern int		id_iszero_(double *);
extern int		ir_iszero_(float *);
extern int		iq_iszero_(long double *);
extern int		signbit(double);
extern int		signbitf(float);
extern int		signbitl(long double);
extern int		id_signbit_(double *);
extern int		ir_signbit_(float *);
extern int		iq_signbit_(long double *);
extern float		copysignf(float, float);
extern long double	copysignl(long double, long double);
extern double		d_copysign_(double *, double *);
extern float		r_copysign_(float *, float *);
extern long double	q_copysign_(long double *, long double *);
extern float		fabsf(float);
extern long double	fabsl(long double);
extern double		d_fabs_(double *);
extern float		r_fabs_(float *);
extern long double	q_fabs_(long double *);
extern float		fmodf(float, float);
extern long double	fmodl(long double, long double);
extern double		d_fmod_(double *, double *);
extern float		r_fmod_(float *, float *);
extern long double	q_fmod_(long double *, long double *);
extern float		nextafterf(float, float);
extern long double	nextafterl(long double, long double);
extern double		d_nextafter_(double *, double *);
extern float		r_nextafter_(float *, float *);
extern long double	q_nextafter_(long double *, long double *);
extern float		remainderf(float, float);
extern long double	remainderl(long double, long double);
extern double		d_remainder_(double *, double *);
extern float		r_remainder_(float *, float *);
extern long double	q_remainder_(long double *, long double *);
extern float		scalbnf(float, int);
extern long double	scalbnl(long double, int);
extern double		d_scalbn_(double *, int *);
extern float		r_scalbn_(float *, int *);
extern long double	q_scalbn_(long double *, int *);

/* IEEE values */

extern double		min_subnormal(void);
extern float		min_subnormalf(void);
extern long double	min_subnormall(void);
extern double		d_min_subnormal_(void);
extern float		r_min_subnormal_(void);
extern long double	q_min_subnormal_(void);
extern double		max_subnormal(void);
extern float		max_subnormalf(void);
extern long double	max_subnormall(void);
extern double		d_max_subnormal_(void);
extern float		r_max_subnormal_(void);
extern long double	q_max_subnormal_(void);
extern double		min_normal(void);
extern float		min_normalf(void);
extern long double	min_normall(void);
extern double		d_min_normal_(void);
extern float		r_min_normal_(void);
extern long double	q_min_normal_(void);
extern double		max_normal(void);
extern float		max_normalf(void);
extern long double	max_normall(void);
extern double		d_max_normal_(void);
extern float		r_max_normal_(void);
extern long double	q_max_normal_(void);
extern double		infinity(void);
extern float		infinityf(void);
extern long double	infinityl(void);
extern double		d_infinity_(void);
extern float		r_infinity_(void);
extern long double	q_infinity_(void);
extern double		quiet_nan(long);
extern float		quiet_nanf(long);
extern long double	quiet_nanl(long);
extern double		d_quiet_nan_(long *);
extern float		r_quiet_nan_(long *);
extern long double	q_quiet_nan_(long *);
extern double		signaling_nan(long);
extern float		signaling_nanf(long);
extern long double	signaling_nanl(long);
extern double		d_signaling_nan_(long *);
extern float		r_signaling_nan_(long *);
extern long double	q_signaling_nan_(long *);

/* sqrt, cbrt */

extern float		sqrtf(float);
extern long double	sqrtl(long double);
extern double		d_sqrt_(double *);
extern float		r_sqrt_(float *);
extern float		cbrtf(float);
extern long double	cbrtl(long double);
extern double		d_cbrt_(double *);
extern float		r_cbrt_(float *);

/* error functions */

extern float		erff(float);
extern long double	erfl(long double);
extern double		d_erf_(double *);
extern float		r_erf_(float *);
extern float		erfcf(float);
extern long double	erfcl(long double);
extern double		d_erfc_(double *);
extern float		r_erfc_(float *);

/* log gamma functions */

extern float		gammaf(float);
extern long double	gammal(long double);
extern float		lgammaf(float);
extern long double	lgammal(long double);
extern double		d_lgamma_(double *);
extern float		r_lgamma_(float *);
extern float		gammaf_r(float, int *);
extern long double	gammal_r(long double, int *);
extern float		lgammaf_r(float, int *);
extern long double	lgammal_r(long double, int *);
extern double		d_lgamma_r_(double *, int *);
extern float		r_lgamma_r_(float *, int *);

/* Bessel functions */

extern float		j0f(float);
extern long double	j0l(long double);
extern double		d_j0_(double *);
extern float		r_j0_(float *);
extern float		j1f(float);
extern long double	j1l(long double);
extern double		d_j1_(double *);
extern float		r_j1_(float *);
extern float		jnf(int, float);
extern long double	jnl(int, long double);
extern double		d_jn_(int *, double *);
extern float		r_jn_(int *, float *);
extern float		y0f(float);
extern long double	y0l(long double);
extern double		d_y0_(double *);
extern float		r_y0_(float *);
extern float		y1f(float);
extern long double	y1l(long double);
extern double		d_y1_(double *);
extern float		r_y1_(float *);
extern float		ynf(int, float);
extern long double	ynl(int, long double);
extern double		d_yn_(int *, double *);
extern float		r_yn_(int *, float *);

/* IEEE test functions for verifying standard compliance */

extern float		logbf(float);
extern long double	logbl(long double);
extern double		d_logb_(double *);
extern float		r_logb_(float *);
extern float		scalbf(float, float);
extern long double	scalbl(long double, long double);
extern double		d_scalb_(double *, double *);
extern float		r_scalb_(float *, float *);
extern float		significandf(float);
extern long double	significandl(long double);
extern double		d_significand_(double *);
extern float		r_significand_(float *);

/* Euclidean distance */

extern float		hypotf(float, float);
extern long double	hypotl(long double, long double);
extern double		d_hypot_(double *, double *);
extern float		r_hypot_(float *, float *);

/* additive pseudo-random number generators */

extern int		i_addran_(void);
extern double		d_addran_(void);
extern float		r_addran_(void);
extern void		i_addrans_(int *, int *, int *, int *);
extern void		u_addrans_(unsigned *, int *, unsigned *, unsigned *);
extern void		d_addrans_(double *, int *, double *, double *);
extern void		r_addrans_(float *, int *, float *, float *);
extern void		i_get_addrans_(int *);
extern void		i_set_addrans_(int *);
extern void		d_get_addrans_(double *);
extern void		d_set_addrans_(double *);
extern void		r_get_addrans_(float *);
extern void		r_set_addrans_(float *);
extern void		i_init_addrans_(void);
extern void		d_init_addrans_(void);
extern void		r_init_addrans_(void);

/* linear congruential pseudo-random number generators */

extern int		i_lcran_(void);
extern double		d_lcran_(void);
extern float		r_lcran_(void);
extern void		u_lcrans_(unsigned *, int *, unsigned *, unsigned *);
extern void		i_lcrans_(int *, int *, int *, int *);
extern void		d_lcrans_(double *, int *, double *, double *);
extern void		r_lcrans_(float *, int *, float *, float *);
extern void		i_get_lcrans_(int *);
extern void		i_set_lcrans_(int *);
extern void		i_init_lcrans_(void);

/* random number shufflers */

extern void		i_shufrans_(int *, int *, int *, int *);
extern void             u_shufrans_(unsigned *, int *, unsigned *, unsigned *);
extern void             d_shufrans_(double *, int *, double *, double *);
extern void             r_shufrans_(float *, int *, float *, float *);

/* convert external binary data formats */

enum convert_external_arch_t {
	convert_external_sparc, convert_external_pc,
	convert_external_vax, convert_external_vaxg,
	convert_external_s370, convert_external_cray
};

enum convert_external_type_t {
	convert_external_signed,
	convert_external_unsigned,
	convert_external_float
};

typedef struct {
	enum convert_external_arch_t arch;
	enum convert_external_type_t type;
	int size ;
} convert_external_t;

extern	fp_exception_field_type	convert_external
   (const char *, convert_external_t, char *, convert_external_t, int, int);

extern	fp_exception_field_type	convert_external_
   (const char *, const convert_external_t *, char *,
   const convert_external_t *, const int *, const int *);

/* sigfpe - signal handling for specific SIGFPE codes */

typedef int sigfpe_code_type;
extern sigfpe_handler_type sigfpe(sigfpe_code_type, sigfpe_handler_type);
extern sigfpe_handler_type sigfpe_(sigfpe_code_type *, sigfpe_handler_type);

/* ieee_handler - IEEE exception trap handler function */

extern int	ieee_handler(const char *, const char *, sigfpe_handler_type);
extern int	ieee_handler_(const char *, const char *, sigfpe_handler_type,
				int, int);

/* ieee_flags - mode and status function for IEEE standard arithmetic */

extern int	ieee_flags(const char *, const char *, const char *, char **);
extern int	ieee_flags_(const char *, const char *, const char *, char *,
				int, int, int, int);

/* ieee_retrospective */

typedef struct
{
        int		_cnt;
        unsigned char	*_ptr;
        unsigned char	*_base;
        unsigned char	_flag;
        unsigned char	_file;
} FILE;

extern void	ieee_retrospective(FILE *);
extern void	ieee_retrospective_(void);

extern void		abrupt_underflow_(void);
extern void		gradual_underflow_(void);
extern void		standard_arithmetic(void);
extern void		standard_arithmetic_(void);
extern void		nonstandard_arithmetic(void);
extern void		nonstandard_arithmetic_(void);

/* BSD misc stuff */

extern double		drem(double, double);

/* base conversion routines in libc */

typedef float single;
typedef unsigned extended[3];
typedef long double quadruple;

typedef char decimal_string[512];

enum fp_direction_type {
	fp_nearest	= 0,
	fp_tozero	= 1,
	fp_positive	= 2,
	fp_negative	= 3
};

typedef struct {
	enum fp_class_type fpclass;
	int	sign;
	int	exponent;
	decimal_string ds;
	int	more;
	int 	ndigits;
} decimal_record;

enum decimal_form {
	fixed_form,
	floating_form
};

typedef struct {
	enum fp_direction_type rd;
	enum decimal_form df;
	int ndigits;
} decimal_mode;

enum decimal_string_form {
	invalid_form,
	whitespace_form,
	fixed_int_form,
	fixed_intdot_form,
	fixed_dotfrac_form,
	fixed_intdotfrac_form,
	floating_int_form,
	floating_intdot_form,
	floating_dotfrac_form,
	floating_intdotfrac_form,
	inf_form,
	infinity_form,
	nan_form,
	nanstring_form
};

extern void single_to_decimal(single *, decimal_mode *, decimal_record *,
				fp_exception_field_type *);
extern void double_to_decimal(double *, decimal_mode *, decimal_record *,
				fp_exception_field_type *);
extern void extended_to_decimal(extended *, decimal_mode *,
				decimal_record *, fp_exception_field_type *);
extern void quadruple_to_decimal(quadruple *, decimal_mode *,
				decimal_record *, fp_exception_field_type *);
extern void decimal_to_single(single *, decimal_mode *, decimal_record *,
				fp_exception_field_type *);
extern void decimal_to_double(double *, decimal_mode *, decimal_record *,
				fp_exception_field_type *);
extern void decimal_to_extended(extended *, decimal_mode *,
				decimal_record *, fp_exception_field_type *);
extern void decimal_to_quadruple(quadruple *, decimal_mode *,
				decimal_record *, fp_exception_field_type *);
extern void string_to_decimal(char **, int, int, decimal_record *,
				enum decimal_string_form *, char **);
extern void func_to_decimal(char **, int, int, decimal_record *,
				enum decimal_string_form *, char **,
				int (*)(void), int *, int (*)(int));
extern void file_to_decimal(char **, int, int, decimal_record *,
				enum decimal_string_form *, char **,
				FILE *, int *);
extern char *seconvert(single *, int, int *, int *, char *);
extern char *sfconvert(single *, int, int *, int *, char *);
extern char *sgconvert(single *, int, int, char *);
extern char *econvert(double, int, int *, int *, char *);
extern char *fconvert(double, int, int *, int *, char *);
extern char *gconvert(double, int, int, char *);
extern char *qeconvert(quadruple *, int, int *, int *, char *);
extern char *qfconvert(quadruple *, int, int *, int *, char *);
extern char *qgconvert(quadruple *, int, int, char *);

.TH tcov 1 "30 Oct 1996"
.SH NAME
tcov \- construct test coverage analysis and statement-by-statement profile
.SH SYNOPSIS
.B tcov
[
.B \-a
] 
[ 
\-\f2n\f1
]
[
.B \-o
\f2filename\f1
]
[
.B \-p
\f2oldpath newpath\f1
]
.if n .ti +5n
[
.B \-x
\f2myprog\f1
]
\f2files ...\f1
.SH MT-Level
Running an MT program compiled with the 
.B -a 
(or 
.B -xa) 
option produces
unpredictable behavior.  
The
.if t .br
.B -xprofile=tcov 
option may be used with
MT programs, 
but the output of 
\f4tcov\f1 is valid only for a zero or
non-zero value.  
The actual count may be incorrect.
.SH DESCRIPTION
.IX  "tcov command"  ""  "\fLtcov\fP \(em code coverage tool"
.IX  "code coverage tool"  ""  "code coverage tool \(em \fLtcov\fP"
.IX  "programming tools"  tcov  ""  "\fLtcov\fP \(em code coverage tool"
.IX  "C programming language"  tcov  ""  "\fLtcov\fP \(em code coverage tool"
.IX  languages  tcov  ""  "\fLtcov\fP \(em code coverage tool"
.LP
This man page describes the updated \f4tcov\f1 program called 
\f3TCOV Enhanced\f1. For a description of the original version
of \f4tcov\f1, see the "\f2Performance Profiling Tools\f1" manual.
\f4tcov\f1
produces a test coverage analysis of a compiled program.  
\f4tcov\f1
takes source files as arguments and produces an annotated source
listing.  Each straight-line segment of code (or each line if the 
.B -a
option to 
\f4tcov\f1 
is specified) is prefixed with the number of times it
has been executed; lines that have not been executed are prefixed
with "#####".
.LP
Without the 
.B -x 
option, 
\f4tcov\f1 
uses the corresponding 
\f4file.d\f1.  
This is
created at compile time when the 
.B \-a 
flag is used in C++, pascal, and
fortran, or the 
.B \-xa 
option for C. 
By default, the compilers create
\f4file.d\f1 in the same directory
that the source file is located in.
The directory that 
\f4file.d\f1
is created in can be overridden at compile time
with the TCOVDIR environment variable (the location of
\f4file.d\f1 cannot be overridden at run time 
when you use the \f3\-xa\f1 or \f3\-a\f1 flags). 
Each time the program is executed, test
coverage information is accumulated in 
\f4file.d\f1.  
The data for each
object file is zeroed out the next time the corresponding source file
is compiled.  
.LP
With the 
.B -x 
option, 
\f4tcov\f1 
uses the 
\f4myprog.profile/tcovd\f1 
file
as data.  This file is created the first time a program that has
object files compiled with the 
\f3-xprofile=tcov\f1 
flag is
executed.  
The value of 
\f2myprog\f1 is the base name of the executable.
.LP
By default, the name of the directory the \f4tcov\f1 file is stored in
is derived from the name of the executable. 
Furthermore, that directory is created in the directory the 
executable was run in (the original \f4tcov\f1 created the .d files in 
the directory the modules were compiled in).
.LP
The name of the directory the \f4tcovd\f1 file is stored in is 
also known
as the "profile bucket". The profile bucket can be overridden by 
using the SUN_PROFDATA environment variable.
One case this might be useful for is if the name of the executable
is not the same as the value in \f3argv[0]\f1 (for example, the invocation
of the executable was through a symbolic link with a different name).
.LP
You can also override the directory the profile bucket is created
in.
To specify a location different than the run directory, specify 
the path using the SUN_PROFDATA_DIR environment variable.
Note that either absolute or relative pathnames can be specified 
in this variable.
Relative pathnames are relative to the program's current working
directory.
.LP
TCOVDIR is supported as a synonym for SUN_PROFDATA_DIR for the 
sake of backwards compatibility.
Any setting of SUN_PROFDATA_DIR causes TCOVDIR to be ignored.
If both SUN_PROFDATA_DIR and TCOVDIR are set, 
a warning is issued when the profile bucket is being generated.
It is used at runtime by a program compiled with \f3-xprofile=tcov\f1
and it is used by the \f4TCOV\f1 command.
.LP
Each subsequent run accumulates more coverage data into the
\f4myprog.tcovd\f1 
file.  
Data for each object file is zeroed out the
first time the program is executed \f2after\f1 the 
corresponding 
source file
has been recompiled.  
Data for the entire program is zeroed out by
removing the 
\f4myprog.tcovd\f1 
file.
.LP
By default, the output is written to .\f4/file.c.tcov\f1, 
where "c" designates the compiler 
being used.
The directory to which this file is written can be 
overridden by using TCOVDIR.
Note that leading directory names are stripped from
each source-file argument before the .\f4\tcov\f1 file name is generated.
Alternatively, the output can be redirected by using 
the \f3\-o\f1 option.
.LP
Note: the profile produced includes only the number of times each
statement was executed, not execution times; to obtain times for
routines use gprof(1) or prof(1).
.SH OPTIONS
.TP
.B \-a
Display an execution count for each statement; if
.B \-a
is not specified, an execution count
is displayed only for the first statement
of each straight-line segment of code.
.TP
\f2\-n\f1
Display table of the line numbers of the
.I n
most frequently executed statements and their execution counts.
.TP
\f3\-o\f1 \f2filename\f1
Direct the output to 
\f2filename\f1 
instead of 
\f4file.tcov\f1.  
If
\f2filename\f1 is "\f3\-\f1",
then direct the output to the standard output
instead of 
\f4file.tcov\f1.  This option overrides any directory specified
in the TCOVDIR environment variable (see ENVIRONMENT below).
.TP
\f3\-p\f1 \f2originalpath newpath\f1
The
.B -x 
option can get confused when the source files have a
different path when you run 
\f4tcov\f1 than they had when you compiled
them.  
This frequently happens because you are using two different
machines that have different mount points for the same directory.
This option allows you to tell 
\f4tcov\f1 
that all source files that had a
path prefix of 
\f2originalpath\f1 at compile time, now have a path
prefix of 
\f2newpath\f1 at
\f4tcov\f1 runtime.  
You may specify as many 
.B -p
options as you wish.
.TP
\f3\-x\f1 \f2myprog\f1
This option turns on the new 
\f4tcov\f1 
processing 
(specified by the 
.B -xprofile=tcov 
compiler flag) and also provides the
name of the executable that is being analyzed.  
If the TCOVDIR
environment variable is not set, 
then with the argument of 
\f2/foo/bar\f1,
\f4tcov\f1 uses the coverage file in 
\f4/foo/bar.profile/bar.tcovd\f1.  
If the TCOVDIR environment variable is set, 
then 
\f4tcov\f1 uses the
coverage file of 
\f4$TCOVDIR/bar.tcovd\f1.
.SH "EXAMPLES"
.br
.SH
.br
.sp
.sp
.br
.SH ENVIRONMENT
.TP 10
.SB TCOVDIR  
Without the 
.B \-x 
option: if the TCOVDIR environment
variable is set at compile time, it specifies the location of the
\f4file.d\f1.  If it is set at 
\f4tcov\f1 runtime, 
it specifies the location of
\f4file.d\f1 and \f4file.tcov\f1.  
It has no effect at program runtime.
.TP 10
.SB 
With the 
.B \-x 
option:  if the TCOVDIR environment
variable is set at program runtime, then the 
\f4a.out.tcovd\f1 is directed into
that directory.  If it is set at 
\f4tcov\f1 runtime, it  specifies the location of
\f4a.out.tcovd\f1 and \f4file.tcov\f1.  It has no effect at compile time.
.TP 10
.SB TCOVEXE 
When set at runtime with the 
\f3-xprofile=tcov\f1 
option,
this will specify the \f4a.out\f1 name 
portion of the profiling data file.
That is, if the variable is set to
\f2myprog\f1 then the profile data will
be stored in 
\f4myprog.profile/myprog.tcovd\f1 
(or if the TCOVDIR
environment variable is set it will go into 
\f4$TCOVDIR/myprog.tcovd\f1).
.SH FILES
.TP 10
Without \f3-x\f1 flag:
\f4file.d\f1 input test coverage data file
.br
.sp
\f4file.tcov\f1	output test coverage analysis listing file
.TP 10
With \f3-x\f1 flag:
\f4myprog.profile/myprog.tcovd\f1 input test coverage data file
.br
.sp
\f4file.c.tcov\f1 output test coverage analysis listing file
.br
.sp
See the use of the TCOVDIR and TCOVEXE environment variables for how
these names can change.
.br
.sp        
\f4bb_link.o\f1 entry and exit routines for test coverage analysis
.PD
.SH "SEE ALSO"
.BR acc (1)
.BR cc (1),
.BR CC (1),
.BR f77 (1),
.BR gprof (1),
.BR pcc (1),
.BR prof (1),
.BR exit (2)
.SH ONLINE DOCUMENTATION
.TP
.B AnswerBook: 
A script has been provided to automatically setup your 
environment to view the available WorkShop and compiler 
manuals. 
.sp 
Type the following at a command prompt: 
.sp 
      workshop-answerbooks 
.sp 
The script sets the AB_CARDCATALOG environment variable 
and starts /usr/openwin/bin/answerbook.  
.sp 
When the AnswerBook Navigator opens, add the WorkShop and 
and compiler manuals to your doc library by using the 
"Modify Library" button. 
.sp 
(For more information on running AnswerBook, see the 
answerbook(1) man page.) 
.sp
.TP
.B HTML: 
HTML versions of the WorkShop and compiler manuals have also 
been provided. These may be viewed with any HTML browser 
capable of displaying tables (HTML 3.0).  
.sp 
Point your browser to the URL: 
file:/opt/SUNWspro/DOC4.0/lib/locale/C/html_docs/index.html 
.sp
.SH DIAGNOSTICS
.TP
.B "premature end of file"
Issued for routines containing no statements.
.SH NOTES
.LP
The count of basic blocks cannot exceed the value
represented by an
.BR "unsigned int" .
.LP
tcov does not support files which contain #line or #file directives.
.SH BUGS
.LP
The analyzed program must call
.BR exit (2)
or return normally for
the coverage information to be saved in the
.BI . d
file.

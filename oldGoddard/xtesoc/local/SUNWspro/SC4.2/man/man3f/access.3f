.\" @(#)access.3f 1.4 86/01/02 SMI; from UCB 4.2
.\"
.TH ACCESS 3F "07 July 1995"
.SH NAME
access \- return access mode (r,w,x) or existence of a file
.SH SYNOPSIS
.\"
.IX access "" \fLaccess\fP
.IX mode "of file"
.IX "existence of file"
.IX file mode
.\"
.B integer function access (name, mode)
.br
.B character*(*) name, mode
.\"
.SH USAGE
.\"
Example: 
.B "ierr = access ( 'MyFile', 'r' )"
.\"
.SH DESCRIPTION
.\"
The
.B access
function checks the given file,
.I name,
for accessability with respect to the caller according to
.I mode.
The
.I mode
argument can include, in any order, and in any 
combination, one or more of the following:
.IP "\fBr\fP"
Test for read permission
.IP "\fBw\fP"
Test for write permission
.IP "\fBx\fP"
Test for execute permission
.IP "(blank)"
Test for existence
.PP
An error code is returned if either the argument is illegal, or if the 
file cannot be accessed in all of the specified modes.  0 is returned 
if the specified access would be successful.
.SH FILES
.B libF77.a
.SH "SEE ALSO"
.BR access (2), 
.BR perror (3F)

.\" @(#)date.3f 1.4 93/02/01 SMI; from UCB 4.2
.TH DATE 3F "02 October 1995"
.SH NAME
date \- return date in character form
.SH SYNOPSIS
.\"
.IX date  ""  \fLdate\fP
.IX "date in character form"
.\"
.B subroutine date (str)
.br
.B character*9 str
.\"
.SH DESCRIPTION
.\"
The
.B date
subroutine gets the current date and puts it into the character string
.BR str .
The form is
.BR "dd-mmm-yy" .
.LP
.B dd
is the day of the month as a two-digit integer.
.LP
.B mmm
is the name of the month as a three-letter abbreviation.  The month is in 
the range 1-12.
.LP
.B yy
is the year as a two-digit integer.
.\"
.SH NOTES
.\"
Usage:
\.in +2
.sp .1
.nf
.ft 3
character*9 str
call date (str)
.fi
\.in -2
.ft 1
.LP
Example:
.sp .1
.ft 3
.nf
      demo% cat dat1.f
      * dat1.f -- Get the date as a character string.
              character c*9
              call date ( c )
              write (*, "(' The date today is: ', A9 )" ) c
              end
      demo% f77 \ \-silent \ dat1.f \ 
      demo% a.out
       The date today is: 2-Oct-95
      demo%
.ft 1
.fi
\fP
.\"
.SH FILES
.B libF77.a
.SH "SEE ALSO"
.BR idate (3f),
.BR ctime (3F), 
.BR fdate (3F),
and the
.I "FORTRAN 77 Reference Manual" 

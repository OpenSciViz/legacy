.\" @(#)ttynam.3f 1.3 86/01/02 SMI; from UCB 4.2
.TH TTYNAM 3F "07 July 1995"
.SH NAME
ttynam, isatty \- find name of terminal port; also: is unit a terminal?
.SH SYNOPSIS
.\"
.IX ttynam "" \fLttynam\fP
.IX isatty "" \fLisatty\fP
.IX name "of a terminal port"
.IX "terminal port name"
.\"
.B character*(*) function ttynam (lunit)
.sp 1
.B logical function isatty (lunit)
.SH DESCRIPTION
The function
.B ttynam
returns a blank-padded path name
of the terminal device associated with logical unit
.IR lunit .
.PP
The function
.B isatty
returns
.B .true.
if
.I lunit
is associated with a terminal device,
.B .false.
otherwise.
.SH FILES
.B /dev/\(**
.br
.B libF77.a
.SH DIAGNOSTICS
The function
.B ttynam
returns an empty string (all blanks) if
.I lunit
is not associated with a terminal device in the directory 
.BR/dev .

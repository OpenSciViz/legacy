.\" @(#)idate.3f 1.4 86/01/02 SMI; from UCB 4.2
.TH IDATE 3F "07 July 1995"
.SH NAME
idate \- return date in numerical form
.SH SYNOPSIS
.\"
.IX idate  ""  \fLidate\fP
.IX "date in numerical form"
.\"
.PP
.B idate
has a standard version and a VMS version:
.TP 10
Standard 
Without the
.B \-lV77
compiler option, you get the standard version:
.in +2
.br
.B "subroutine  idate(iarray)"
.br
.B "integer  iarray(\&3)"
.in -2
.TP 10
VMS
With the
.B \-lV77
compiler option, you get the VMS version:
.in +2
.br
.B "subroutine  idate(m, d, y)"
.br
.B "integer  m, d, y"
.in -2
.\"
.SH DESCRIPTION
.\"
.PP
Standard Version (without
.BR \-lV77 ):
.PP
.in +2
The
.B idate
subroutine puts the current date into the integer array
.BR iarray .
The order is: day, month, year.
The month is in the range 1-12.
The year is four digits, such as 1993.
.sp .2
Example:
.sp .2
.nf
.ft 3
demo% cat  ida2.f
	integer  a(3)
	call  idate( a )
	write(*, "(' The date is: ',3i5)" ) a
	end
demo% f77 \-silent  ida2.f
demo% a.out
 The date is:   23    1   93
demo%
.ft 1
.fi
.in -2
.PP
VMS Version (with
.BR \-lV77 ):
.PP
.in +2
The
.B idate
subroutine puts the current date into the 
integers
.BR m ,
.BR d ,
and
.BR y .
.sp .2
Example:
.sp .2
.nf
.ft 3
demo% cat ida1.f
	integer  m,  d,  y
	call idate( m, d, y )
	write(*, "(' The date is: ',3i3)" ) m, d, y
	end
demo% f77 \-silent  ida1.f  \-lV77
demo% a.out
 The date is:   7 23 89
demo%
.ft 1
.fi
.\"
.SH FILES
.\"
.BR libF77.a , 
.B libV77.a
.\"
.SH "NOTE"
.\"
If you use
.BR \-lV77 ,
then invoking
.B idate()
or
.B time()
gets the VMS version.
.\"
.SH "SEE ALSO"
.\"
.BR date (3f) ,
.BR fdate (3F) ,
and the
.I "FORTRAN 77 Reference Manual"

.TH GETFILEP 3F "07 July 1995"
.SH NAME
getfilep \- get the file pointer of an external unit number
.SH DESCRIPTION
.\"
.IX getfilep "" \fLgetfilep\fP
.IX file "pointer get getfilep" "" "pointer get: \&\fLgetfilep\fP"
.IX get "file pointer getfilep" "" "file pointer: \&\fLgetfilep\fP"
.IX pointer "file get getfilep" "" "file get: \&\fLgetfilep\fP"
.\"
.B getfilep
returns the file pointer of an external unit number
if the unit is connected.
.br
If the unit is not connected, the value returned is -1.
.\"
.SH NOTE
.\"
This function is used for mixing standard FORTRAN I/O 
with C I/O.
Such mixing in nonportable, and is not
guaranteed for subsequent releases
of the operating system or FORTRAN.
Use of this function is not recommended,
and no direct interface is provided.
You must enter your own C routine to use
the value returned by 
.BR getfilep.
A sample C routine is shown below.
.\"
.SH USAGE
.\"
Following is a sample FORTRAN main program with 
.BR getfilep , 
the file
.BR MixStdinmain.f :
.sp
.ft 3 
.nf
      character*1  inbyte
      integer*4    c_read,  getfilep, unitn / 5 /
      external     getfilep
      write(*,'(a,$)') 'What is the digit? '

      irtn = c_read ( getfilep ( unitn ), inbyte, 1 )

      write(*,9)  inbyte
    9 format('The digit read by C is ', a )
      end
.\"
.sp
.fi
.ft 1
Following is a sample C function that actually uses 
.BR getfilep , 
the file
.BR MixStdin.c :
.sp
.ft 3 
.nf
#include <stdio.h>
int c_read_ ( fd, buf, nbytes, buf_len )
FILE **fd ;
char *buf ;
int *nbytes, buf_len ;
{
   return fread ( buf, 1, *nbytes, *fd ) ;
}
.\"
.sp
.fi
.ft 1
A sample compile/build/run:
.sp
demo% 
.B "cc -c MixStdin.c"
.br
demo% 
.B "f77 MixStdin.o MixStdinmain.f"
.br
.B MixStdinmain.f:
.br
.B " MAIN:"
.br
demo% 
.br
.B a.out
.br
What is the digit? 
.B 3
.br
The digit read by C is 
.B 3
.PP
See the chapter on the C-FORTRAN interface in the
.IR "FORTRAN 77 User's Guide" .
.\"
.SH FILES
.\"
.B libF77.a
.SH "SEE ALSO"
.BR open (2)

.\" @(#)unlink.3f 1.3 86/01/02 SMI; from UCB 4.2
.TH UNLINK 3F "07 July 1995"
.SH NAME
unlink \- remove a file
.SH SYNOPSIS
.\"
.IX unlink "" \fLunlink\fP
.IX "remove a file"
.IX file remove
.\"
.B integer function unlink ( name )
.br
.B character *(*) name
.SH DESCRIPTION
The function
.B unlink
removes the file specified by the path name
.IR name .
If this is the last link to the file, the contents of 
the file are lost.  The returned value is zero 
if successful, a system error code otherwise.
.SH EXAMPLE
The following program removes the file 
.BR stuff :
.PP
demo% 
.B ls
.br
.nf
a.f     other   stuff
.fi
.br
demo% 
.B "cat a.f"
.br
.nf
.ft 3
        external unlink 
        call unlink( 'stuff' )
        stop
        end
.ft 1
.fi
.br
demo% 
.B "f77 a.f"
.br
a.f:
.br
\ MAIN:
.br
demo% 
.B ls
.br
.nf
a.f     a.out*  other   stuff
.fi
.br
demo% 
.B a.out
.br
demo% 
.B ls
.br
.nf
a.f     a.out*  other
.fi
.br
demo% 
.SH FILES
.B libF77.a
.SH "SEE ALSO"
.BR unlink (2), 
.BR link (3F), 
.BR perror (3F)
.SH BUGS
Path names cannot be longer than 
.B MAXPATHLEN 
as defined in 
.BR <sys/param.h> .

.TH zgeevx 3P "20 Sep 1996" "Sun, Inc." "Sun Performance Library"
.SH NAME
zgeevx - compute for an N-by-N complex nonsymmetric matrix A, the eigenvalues and, optionally, the left and/or right eigenvectors
.SH SYNOPSIS
.TP 19
SUBROUTINE ZGEEVX(
BALANC, JOBVL, JOBVR, SENSE, N, A, LDA, W, VL,
LDVL, VR, LDVR, ILO, IHI, SCALE, ABNRM, RCONDE,
RCONDV, WORK, LWORK, RWORK, INFO )
.TP 12
void zgeevx(char balanc, char jobvl, char jobvr, char sense, 
long int n, doublecomplex *za, long int lda, doublecomplex *w, doublecomplex *vl, 
long int ldvl, doublecomplex *vr, long int ldvr, long int *ilo, long int *ihi,
 double *dscale, double *abnrm, double *rconde, double *rcondv, long int *info)
.TP 19
.ti +4
CHARACTER
BALANC, JOBVL, JOBVR, SENSE
.TP 19
.ti +4
INTEGER
IHI, ILO, INFO, LDA, LDVL, LDVR, LWORK, N
.TP 19
.ti +4
DOUBLE
PRECISION ABNRM
.TP 19
.ti +4
DOUBLE
PRECISION RCONDE( * ), RCONDV( * ), RWORK( * ),
SCALE( * )
.TP 19
.ti +4
COMPLEX*16
A( LDA, * ), VL( LDVL, * ), VR( LDVR, * ),
W( * ), WORK( * )
.SH PURPOSE
ZGEEVX computes for an N-by-N complex nonsymmetric matrix A, the
eigenvalues and, optionally, the left and/or right eigenvectors.

Optionally also, it computes a balancing transformation to improve
the conditioning of the eigenvalues and eigenvectors (ILO, IHI,
SCALE, and ABNRM), reciprocal condition numbers for the eigenvalues
(RCONDE), and reciprocal condition numbers for the right
.br
eigenvectors (RCONDV).
.br

The right eigenvector v(j) of A satisfies
.br
                 A * v(j) = lambda(j) * v(j)
.br
where lambda(j) is its eigenvalue.
.br
The left eigenvector u(j) of A satisfies
.br
              u(j)**H * A = lambda(j) * u(j)**H
.br
where u(j)**H denotes the conjugate transpose of u(j).
.br

The computed eigenvectors are normalized to have Euclidean norm
equal to 1 and largest component real.
.br

Balancing a matrix means permuting the rows and columns to make it
more nearly upper triangular, and applying a diagonal similarity
transformation D * A * D**(-1), where D is a diagonal matrix, to
make its rows and columns closer in norm and the condition numbers
of its eigenvalues and eigenvectors smaller.  The computed
reciprocal condition numbers correspond to the balanced matrix.
Permuting rows and columns will not change the condition numbers
(in exact arithmetic) but diagonal scaling will.  For further
explanation of balancing, see section 4.10.2 of the LAPACK
Users' Guide.
.br

.SH ARGUMENTS
.TP 8
BALANC  (input) CHARACTER*1
Indicates how the input matrix should be diagonally scaled
and/or permuted to improve the conditioning of its
eigenvalues.
= 'N': Do not diagonally scale or permute;
.br
= 'P': Perform permutations to make the matrix more nearly
upper triangular. Do not diagonally scale;
= 'S': Diagonally scale the matrix, ie. replace A by
D*A*D**(-1), where D is a diagonal matrix chosen
to make the rows and columns of A more equal in
norm. Do not permute;
= 'B': Both diagonally scale and permute A.

Computed reciprocal condition numbers will be for the matrix
after balancing and/or permuting. Permuting does not change
condition numbers (in exact arithmetic), but balancing does.
.TP 8
JOBVL   (input) CHARACTER*1
= 'N': left eigenvectors of A are not computed;
.br
= 'V': left eigenvectors of A are computed.
If SENSE = 'E' or 'B', JOBVL must = 'V'.
.TP 8
JOBVR   (input) CHARACTER*1
= 'N': right eigenvectors of A are not computed;
.br
= 'V': right eigenvectors of A are computed.
If SENSE = 'E' or 'B', JOBVR must = 'V'.
.TP 8
SENSE   (input) CHARACTER*1
Determines which reciprocal condition numbers are computed.
= 'N': None are computed;
.br
= 'E': Computed for eigenvalues only;
.br
= 'V': Computed for right eigenvectors only;
.br
= 'B': Computed for eigenvalues and right eigenvectors.

If SENSE = 'E' or 'B', both left and right eigenvectors
must also be computed (JOBVL = 'V' and JOBVR = 'V').
.TP 8
N       (input) INTEGER
The order of the matrix A. N >= 0.
.TP 8
A       (input/output) COMPLEX*16 array, dimension (LDA,N)
On entry, the N-by-N matrix A.
On exit, A has been overwritten.  If JOBVL = 'V' or
JOBVR = 'V', A contains the Schur form of the balanced
version of the matrix A.
.TP 8
LDA     (input) INTEGER
The leading dimension of the array A.  LDA >= max(1,N).
.TP 8
W       (output) COMPLEX*16 array, dimension (N)
W contains the computed eigenvalues.
.TP 8
VL      (output) COMPLEX*16 array, dimension (LDVL,N)
If JOBVL = 'V', the left eigenvectors u(j) are stored one
after another in the columns of VL, in the same order
as their eigenvalues.
If JOBVL = 'N', VL is not referenced.
u(j) = VL(:,j), the j-th column of VL.
.TP 8
LDVL    (input) INTEGER
The leading dimension of the array VL.  LDVL >= 1; if
JOBVL = 'V', LDVL >= N.
.TP 8
VR      (output) COMPLEX*16 array, dimension (LDVR,N)
If JOBVR = 'V', the right eigenvectors v(j) are stored one
after another in the columns of VR, in the same order
as their eigenvalues.
If JOBVR = 'N', VR is not referenced.
v(j) = VR(:,j), the j-th column of VR.
.TP 8
LDVR    (input) INTEGER
The leading dimension of the array VR.  LDVR >= 1; if
JOBVR = 'V', LDVR >= N.

ILO,IHI (output) INTEGER
ILO and IHI are integer values determined when A was
balanced.  The balanced A(i,j) = 0 if I > J and
J = 1,...,ILO-1 or I = IHI+1,...,N.
.TP 8
SCALE   (output) DOUBLE PRECISION array, dimension (N)
Details of the permutations and scaling factors applied
when balancing A.  If P(j) is the index of the row and column
interchanged with row and column j, and D(j) is the scaling
factor applied to row and column j, then
SCALE(J) = P(J),    for J = 1,...,ILO-1
= D(J),    for J = ILO,...,IHI
= P(J)     for J = IHI+1,...,N.
The order in which the interchanges are made is N to IHI+1,
then 1 to ILO-1.
.TP 8
ABNRM   (output) DOUBLE PRECISION
The one-norm of the balanced matrix (the maximum
of the sum of absolute values of elements of any column).
.TP 8
RCONDE  (output) DOUBLE PRECISION array, dimension (N)
RCONDE(j) is the reciprocal condition number of the j-th
eigenvalue.
.TP 8
RCONDV  (output) DOUBLE PRECISION array, dimension (N)
RCONDV(j) is the reciprocal condition number of the j-th
right eigenvector.
.TP 8
WORK    (workspace/output) COMPLEX*16 array, dimension (LWORK)
On exit, if INFO = 0, WORK(1) returns the optimal LWORK.
.TP 8
LWORK   (input) INTEGER
The dimension of the array WORK.  If SENSE = 'N' or 'E',
LWORK >= max(1,2*N), and if SENSE = 'V' or 'B',
LWORK >= N*N+2*N.
For good performance, LWORK must generally be larger.
.TP 8
RWORK   (workspace) DOUBLE PRECISION array, dimension (2*N)
.TP 8
INFO    (output) INTEGER
= 0:  successful exit
.br
< 0:  if INFO = -i, the i-th argument had an illegal value.
.br
> 0:  if INFO = i, the QR algorithm failed to compute all the
eigenvalues, and no eigenvectors or condition numbers
have been computed; elements 1:ILO-1 and i+1:N of W
contain eigenvalues which have converged.

.TH sgtsvx 3P "20 Sep 1996" "Sun, Inc." "Sun Performance Library"
.SH NAME
sgtsvx - use the LU factorization to compute the solution to a real system of linear equations A * X = B or A**T * X = B,
.SH SYNOPSIS
.TP 19
SUBROUTINE SGTSVX(
FACT, TRANS, N, NRHS, DL, D, DU, DLF, DF, DUF,
DU2, IPIV, B, LDB, X, LDX, RCOND, FERR, BERR,
WORK, IWORK, INFO )
.TP 12
void sgtsvx(char fact, char trans, long int n, long int nrhs, 
float *dl, float *d, float *du, float *dlf, float *df, float *duf, 
float *du2, long int *ipivot, float *sb, long int ldb, float *sx, long int ldx, float *srcond, float *ferr, float *berr, long int *info)
.TP 19
.ti +4
CHARACTER
FACT, TRANS
.TP 19
.ti +4
INTEGER
INFO, LDB, LDX, N, NRHS
.TP 19
.ti +4
REAL
RCOND
.TP 19
.ti +4
INTEGER
IPIV( * ), IWORK( * )
.TP 19
.ti +4
REAL
B( LDB, * ), BERR( * ), D( * ), DF( * ),
DL( * ), DLF( * ), DU( * ), DU2( * ), DUF( * ),
FERR( * ), WORK( * ), X( LDX, * )
.SH PURPOSE
SGTSVX uses the LU factorization to compute the solution to a real
system of linear equations A * X = B or A**T * X = B,
where A is a tridiagonal matrix of order N and X and B are N-by-NRHS
matrices.
.br

Error bounds on the solution and a condition estimate are also
provided.
.br

.SH DESCRIPTION
The following steps are performed:
.br

1. If FACT = 'N', the LU decomposition is used to factor the matrix A
   as A = L * U, where L is a product of permutation and unit lower
   bidiagonal matrices and U is upper triangular with nonzeros in
   only the main diagonal and first two superdiagonals.
.br

2. The factored form of A is used to estimate the condition number
   of the matrix A.  If the reciprocal of the condition number is
   less than machine precision, steps 3 and 4 are skipped.

3. The system of equations is solved for X using the factored form
   of A.
.br

4. Iterative refinement is applied to improve the computed solution
   matrix and calculate error bounds and backward error estimates
   for it.
.br

.SH ARGUMENTS
.TP 8
FACT    (input) CHARACTER*1
Specifies whether or not the factored form of A has been
supplied on entry.
= 'F':  DLF, DF, DUF, DU2, and IPIV contain the factored
form of A; DL, D, DU, DLF, DF, DUF, DU2 and IPIV
will not be modified.
= 'N':  The matrix will be copied to DLF, DF, and DUF
and factored.
.TP 8
TRANS   (input) CHARACTER*1
Specifies the form of the system of equations:
.br
= 'N':  A * X = B     (No transpose)
.br
= 'T':  A**T * X = B  (Transpose)
.br
= 'C':  A**H * X = B  (Conjugate transpose = Transpose)
.TP 8
N       (input) INTEGER
The order of the matrix A.  N >= 0.
.TP 8
NRHS    (input) INTEGER
The number of right hand sides, i.e., the number of columns
of the matrix B.  NRHS >= 0.
.TP 8
DL      (input) REAL array, dimension (N-1)
The (n-1) subdiagonal elements of A.
.TP 8
D       (input) REAL array, dimension (N)
The n diagonal elements of A.
.TP 8
DU      (input) REAL array, dimension (N-1)
The (n-1) superdiagonal elements of A.
.TP 8
DLF     (input or output) REAL array, dimension (N-1)
If FACT = 'F', then DLF is an input argument and on entry
contains the (n-1) multipliers that define the matrix L from
the LU factorization of A as computed by SGTTRF.

If FACT = 'N', then DLF is an output argument and on exit
contains the (n-1) multipliers that define the matrix L from
the LU factorization of A.
.TP 8
DF      (input or output) REAL array, dimension (N)
If FACT = 'F', then DF is an input argument and on entry
contains the n diagonal elements of the upper triangular
matrix U from the LU factorization of A.

If FACT = 'N', then DF is an output argument and on exit
contains the n diagonal elements of the upper triangular
matrix U from the LU factorization of A.
.TP 8
DUF     (input or output) REAL array, dimension (N-1)
If FACT = 'F', then DUF is an input argument and on entry
contains the (n-1) elements of the first superdiagonal of U.

If FACT = 'N', then DUF is an output argument and on exit
contains the (n-1) elements of the first superdiagonal of U.
.TP 8
DU2     (input or output) REAL array, dimension (N-2)
If FACT = 'F', then DU2 is an input argument and on entry
contains the (n-2) elements of the second superdiagonal of
U.

If FACT = 'N', then DU2 is an output argument and on exit
contains the (n-2) elements of the second superdiagonal of
U.
.TP 8
IPIV    (input or output) INTEGER array, dimension (N)
If FACT = 'F', then IPIV is an input argument and on entry
contains the pivot indices from the LU factorization of A as
computed by SGTTRF.

If FACT = 'N', then IPIV is an output argument and on exit
contains the pivot indices from the LU factorization of A;
row i of the matrix was interchanged with row IPIV(i).
IPIV(i) will always be either i or i+1; IPIV(i) = i indicates
a row interchange was not required.
.TP 8
B       (input) REAL array, dimension (LDB,NRHS)
The N-by-NRHS right hand side matrix B.
.TP 8
LDB     (input) INTEGER
The leading dimension of the array B.  LDB >= max(1,N).
.TP 8
X       (output) REAL array, dimension (LDX,NRHS)
If INFO = 0, the N-by-NRHS solution matrix X.
.TP 8
LDX     (input) INTEGER
The leading dimension of the array X.  LDX >= max(1,N).
.TP 8
RCOND   (output) REAL
The estimate of the reciprocal condition number of the matrix
A.  If RCOND is less than the machine precision (in
particular, if RCOND = 0), the matrix is singular to working
precision.  This condition is indicated by a return code of
INFO > 0, and the solution and error bounds are not computed.
.TP 8
FERR    (output) REAL array, dimension (NRHS)
The estimated forward error bound for each solution vector
X(j) (the j-th column of the solution matrix X).
If XTRUE is the true solution corresponding to X(j), FERR(j)
is an estimated upper bound for the magnitude of the largest
element in (X(j) - XTRUE) divided by the magnitude of the
largest element in X(j).  The estimate is as reliable as
the estimate for RCOND, and is almost always a slight
overestimate of the true error.
.TP 8
BERR    (output) REAL array, dimension (NRHS)
The componentwise relative backward error of each solution
vector X(j) (i.e., the smallest relative change in
any element of A or B that makes X(j) an exact solution).
.TP 8
WORK    (workspace) REAL array, dimension (3*N)
.TP 8
IWORK   (workspace) INTEGER array, dimension (N)
.TP 8
INFO    (output) INTEGER
= 0:  successful exit
.br
< 0:  if INFO = -i, the i-th argument had an illegal value
.br
> 0:  if INFO = i, and i is
.br
<= N:  U(i,i) is exactly zero.  The factorization
has not been completed unless i = N, but the
factor U is exactly singular, so the solution
and error bounds could not be computed.
= N+1:  RCOND is less than machine precision.  The
factorization has been completed, but the
matrix is singular to working precision, and
the solution and error bounds have not been 
computed.

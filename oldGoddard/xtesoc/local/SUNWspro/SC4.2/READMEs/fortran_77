December 1996                                          SC 4.2 Release 
                        FORTRAN 77 4.2 
=============================================================================


Introduction: 


f77 4.2 runs on SPARC(TM), Intel(R) x86, and PowerPC  systems, with the
Solaris(TM) 2 operating environment.

Note that with this release the f77 User's Guide has been combined
with the f90 User's Guide into a single manual. The set of Fortran 
manuals are now:

      Fortran User's Guide (for f77 4.2 and f90 1.2)
      Fortran Programmer's Guide (for f77 4.2 and f90 1.2)
      Fortran Library Reference (for f77 4.2 and f90 1.2)
      Fortran 77 Language Reference (for f77 4.2)

These manuals are also available online in AnswerBook and HTML formats:

  AnswerBook:
  A script has been provided to automatically setup your
  environment to view the available WorkShop and compiler
  manuals.

  Type the following at a command prompt:

      workshop-answerbooks

  The script sets the AB_CARDCATALOG environment variable
  and starts /usr/openwin/bin/answerbook. 

  When the AnswerBook Navigator opens, add the WorkShop and
  and compiler manuals to your doc library by using the
  "Modify Library" button.

  (For more information on running AnswerBook, see the
  answerbook(1) man page.)

  HTML:
  HTML versions of the WorkShop and compiler manuals have also
  been provided. These may be viewed with any HTML browser
  capable of displaying tables (HTML 3.0). 

  Point your browser to the URL:
    file:/opt/SUNWspro/DOC4.0/lib/locale/C/html_docs/index.html

=============================================================================


____________________________________________________________________________
Contents:

          A. New and Changed Features
          B. Software Incompatibilities
          C. Current Software Bugs
          D. Fixed Software Bugs
          E. Documentation Errata
          F. Shippable Libraries


____________________________________________________________________________
A. New and Changed Features


 f77 4.2 has the following features new or changed since 4.0:

 o All flags are accepted across all platforms. However, on a platform
   where a flag is not valid it will be silently accepted by the compiler
   and no warning will be given.  The compiler's -flags print of all 
   options will only show the valid flags for the platform on which it is
   running. 

 o The default source file pre-processor is fpp with this release. 
   In previous releases the C-language preprocessor, cpp, was used. fpp is
   similar to cpp except that fpp fully recognizes Fortran syntax. See the
   fpp(1) man page for more information. fpp is now called automatically
   by f77 when processing source files with .F suffix. To force use of
   cpp rather than fpp, specify the new -xpp=cpp option.


 o New Options


      -dbl_align_all=no|yes
         Permit unconditionally 8-byte alignment for all data. Default is "no"

      -erroff=tag
         Suppress warning messages specified by "tag", a comma-separated list
         of error tags, or %none (enables all warning messages), or %all 
         (supresses all warnings; equivalent to -w).

      -errtags=yes|no
         Display the compiler's error tags associated with any warning message.
          
      -stop_status=no|yes
         Permit an integer constant on STOP statement to return an exit
         status value. "no" is default.

      -xcrossfile    (SPARC only)
         Apply optimizations globally across all source files listed on the
         command line as if they had all been concatenated into a single file.

      -xlic_lib=list        
         Link Sun licensed libraries specified in <list>. For example,
         -xlic_lib=sunperf accesses the Sun Performance Library.

      -xpp=fpp|cpp
         Specify the source pre-processor to use for .F files. Default is
         fpp (see man page fpp(1)). cpp is the C language pre-processor.
         Previous versions of f77 utilized cpp.

      -xtypemap=type:usr,type:usr,...
         General specification of default sizes for data types.
          <type>=integer|real|double
          <usr>= specification varies depending on <type>:
                 integer:mixed|64
                 real:64
                 double:64|128
          "mixed" implies 64-bit integers but 32-bit arithmetic
          double:128 is only available on SPARC and PPC

         Redefining -r8 and -dbl in terms of -xtypemap:
          -r8 =
                -xtypemap=real:64,double:128,integer:mixed  (SPARC & PPC)
                -xtypemap=real:64,double:64,integer:mixed   (Intel x86)
          -dbl =
                -xtypemap=real:64,double:128,integer:64     (SPARC & PPC)
                -xtypemap=real:64,double:64,integer:64      (Intel x86)


   Changed Options

          -vax=  Set VAX options. Expanded:
             -vax=keyword to enable,  or  -vax=no%keyword to disable
             New keyword functions:
                logical_name       VMS style logical file name
                struct_align       align structures as in VMS Fortran
                bslash             backslash as ordinary character in
                                   character constants
                debug              debugging lines beginning with "D"
                rsize              specify unformatted record sizes in
                                   words rather than bytes
                oct_const          double quote introduces octal constants 
                no%misalign        negates existing misalign option
                no%align           negates existing align option
                %all               select all VMS features
                %none              disable all VMS features (default if
                                   -vax= not specified)

          The following options, previously SPARC only, are now
          available on Intel and PowerPC:
                -fround, -fsimple, -ftrap, -xprofile=tcov, -xspace,
                -xunroll=n

          The following options, previously SPARC only, are now
          available in restricted form on Intel and PowerPC:
                 -xtarget=
                           Intel accepts: generic, native, 386,
                                          486, pentium, pentium_pro
                         PowerPC accepts: generic, native
                 -xarch=
                           Intel accepts: generic, 386, pentium_pro
                         PowerPC accepts: generic, ppc, ppc_nofma
                 -xchip=
                           Intel accepts: generic, 386, 486, pentium,
                                          pentium_pro
                         PowerPC accepts: generic, 603, 604



____________________________________________________________________________
B. Software Incompatibilities

   Libraries

       If you compile with f77 4.2, you can link only with the 4.2 
       Sun libraries and not with earlier versions. Programs already
       compiled with f77 4.0 or 3.0.1 can also link with the 4.2 libraries.

   I/O in Mixed F77/F90 Programs

       Fortran 90 and FORTRAN 77 use different I/O libraries. However,
       this should be transparent to the user. That is, programs can do I/O
       to the same unit from both the Fortran 90 and FORTRAN 77 parts of
       the code.

       Caveat: I/O compatibility requires that FORTRAN 77 4.2 programs
        be linked with programs compiled with Fortran 90 1.2. FORTRAN 77 4.2 
        I/O is only compatible with FORTRAN 90 1.1 or 1.2, and not f90 1.0 .

   -U and f90 
 
       There is not equivalent to -U in f90. Upper and lower case are 
       considered equivalent with f90. Any f77 program compiled with 
       the -U option may not be compatible with f90. So, an f77 program 
       with entry points subX and SUBx can not be differentiated from 
       an f90 program. 



____________________________________________________________________________
C. Current Software Bugs

   There is no new information at this time.   

____________________________________________________________________________
E. Fixed Software Bugs

   There is no new information at this time.   

____________________________________________________________________________
E. Documentation Errata

   Fortran User's Guide:
      o Chapter 3 page 96:
        -xnolibmopt is not available on PowerPC or Intel.
      o Chapter 3 page 99:
        Under the no%float suboption of -xregs, the sentence "With this
        option, a source program cannot contain any floating-point code" is
        not correct. It should read: "With this option, the compiler will 
        use the floating-point registers only for floating-point operations,
        and not for block or structure copy operations."

____________________________________________________________________________
F. Shippable Libraries

        If your executable uses a Sun dynamic library listed in the
        file named below, your license includes the right to
        redistribute the library to your customer.

        Standard Install        /opt/SUNWspro/READMEs/runtime.libraries

        Install to /my/dir/     /my/dir/SUNWspro/READMEs/runtime.libraries

        You may not redistribute or otherwise disclose the header files,
        source code, object modules, or static libraries of object modules
        in any form.

        The License to Use appears in the End User Object Code License,
        viewable from the back of the plastic case containing the CDROM.


=============================================================================

Solaris, SunOS, and OpenWindows are trademarks or registered trademarks
of Sun Microsystems, Inc.  All SPARC trademarks, including the SCD Compliant
Logo, are trademarks or registered trademarks of SPARC International, Inc.

AnswerBook is a registered trademark of Sun Microsystems, Inc.

Intel is a registered trademark of Intel Corporation.


Last Date Changed 5 Nov 1996

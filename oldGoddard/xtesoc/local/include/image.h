/*
  Image define declarations.
*/
#define AbsoluteValue(x)  ((x) < 0 ? -(x) : (x))
#define ColorMatch(color,target,delta) \
  ((((int) ((color).red)-delta) <= (int) ((target).red)) && \
    ((int) ((target).red) <= ((int) ((color).red)+delta)) && \
   (((int) ((color).green)-delta) <= (int) ((target).green)) && \
    ((int) ((target).green) <= ((int) ((color).green)+delta)) && \
   (((int) ((color).red)-delta) <= (int) ((target).red)) && \
    ((int) ((target).red) <= ((int) ((color).red)+delta)))
#define DegreesToRadians(x) ((x)*M_PI/180.0)
#define Intensity(color)  \
  ((unsigned int) ((color).red*77+(color).green*150+(color).blue*29) >> 8)
#define IsGray(color)  \
  (((color).red == (color).green) && ((color).green == (color).blue))
#define MatteMatch(color,target,delta) \
  (ColorMatch(color,target,delta) && ((color).index == (target).index))
#define MaxColormapSize  65535L
#define MaxStacksize  (1 << 15)
#define MaxTextLength  2048
#define Opaque  255
#define Push(up,left,right,delta) \
  if ((p < (segment_stack+MaxStacksize)) && (((up)+(delta)) >= 0) && \
      (((up)+(delta)) < image->rows)) \
    { \
      p->y1=(up); \
      p->x1=(left); \
      p->x2=(right); \
      p->y2=(delta); \
      p++; \
    }
#define RadiansToDegrees(x) ((x)*180/M_PI)
#define ReadQuantum(quantum,p)  \
{  \
  if (image->depth == 8) \
    quantum=UpScale(*p++); \
  else \
    { \
      value=(*p++) << 8;  \
      value|=(*p++);  \
      quantum=value >> (image->depth-QuantumDepth); \
    } \
}
#define ReadQuantumFile(quantum)  \
{  \
  if (image->depth == 8) \
    quantum=UpScale(fgetc(image->file)); \
  else \
    quantum=MSBFirstReadShort(image->file) >> (image->depth-QuantumDepth); \
}
#define SharpenFactor  70.0
#define SmoothingThreshold  1.5
#define Transparent  0
#define WriteQuantum(quantum,q)  \
{  \
  if (image->depth == 8) \
    *q++=DownScale(quantum); \
  else \
    { \
      value=(quantum); \
      if ((QuantumDepth-image->depth) > 0) \
        value*=257; \
      *q++=value >> 8; \
      *q++=value; \
    } \
}
#define WriteQuantumFile(quantum)  \
{  \
  if (image->depth == 8) \
    (void) fputc(DownScale(quantum),image->file); \
  else \
    if ((QuantumDepth-image->depth) > 0) \
      MSBFirstWriteShort((quantum)*257,image->file); \
    else \
      MSBFirstWriteShort(quantum,image->file); \
}

/*
  Image Id's
*/
#define UndefinedId  0
#define ImageMagickId  1
/*
  Image classes:
*/
#define UndefinedClass  0
#define DirectClass  1
#define PseudoClass  2
/*
  Image filters:
*/
#define BoxFilter  0
#define MitchellFilter  1
#define TriangleFilter  2
/*
  Drawing primitives.
*/
#define UndefinedPrimitive  0
#define PointPrimitive  1
#define LinePrimitive  2
#define RectanglePrimitive  3
#define FillRectanglePrimitive  4
#define EllipsePrimitive  5
#define FillEllipsePrimitive  6
#define PolygonPrimitive  7
#define FillPolygonPrimitive  8
#define ColorPrimitive  9
#define MattePrimitive  10
#define TextPrimitive  11
#define ImagePrimitive  12

#ifdef QuantumLeap
/*
  Color quantum is [0..65535].
*/
#define DownScale(quantum)  (((unsigned int) (quantum)) >> 8)
#define MaxRGB  65535L
#define MaxRunlength  65535L
#define QuantumDepth  16
#define UpScale(quantum)  (((unsigned int) (quantum))*257)
#define XDownScale(color)  ((unsigned int) (color))
#define XUpScale(color)  ((unsigned int) (color))

typedef unsigned short Quantum;
#else
/*
  Color quantum is [0..255].
*/
#define DownScale(quantum)  ((unsigned int) (quantum))
#define MaxRGB  255
#define MaxRunlength  255
#define QuantumDepth  8
#define UpScale(quantum)  ((unsigned int) (quantum))
#define XDownScale(color)  (((unsigned int) (color)) >> 8)
#define XUpScale(color)  (((unsigned int) (color))*257)

typedef unsigned char Quantum;
#endif

/*
  Typedef declarations.
*/
typedef struct _AnnotateInfo
{
  char
    *server_name,
    *font;

  unsigned int
    pointsize;

  char
    *box,
    *pen,
    *geometry,
    *text,
    *primitive;

  unsigned int
    center;
} AnnotateInfo;

typedef struct _ColorPacket
{
  Quantum
    red,
    green,
    blue;

  unsigned char
    flags;

  unsigned short
    index;
} ColorPacket;

typedef struct _FrameInfo
{
  int
    x,
    y;

  unsigned int
    width,
    height;

  int
    inner_bevel,
    outer_bevel;

  ColorPacket
    matte_color,
    highlight_color,
    shadow_color;
} FrameInfo;

typedef struct _ImageInfo
{
  char
    *filename,
    magick[MaxTextLength];

  unsigned int
    assert,
    subimage,
    subrange;

  char
    *server_name,
    *font,
    *size,
    *tile,
    *density,
    *page,
    *delay,
    *texture;

  unsigned int
    adjoin,
    compression,
    dispose,
    dither,
    interlace,
    iterations,
    monochrome,
    pointsize,
    quality,
    verbose;

  char
    *undercolor;
} ImageInfo;

typedef struct _PrimitiveInfo
{
  unsigned int
    primitive;

  unsigned int
    coordinates;

  int
    x,
    y;

  unsigned int
    method;

  char
    *text;
} PrimitiveInfo;

typedef struct _RectangleInfo
{
  unsigned int
    width,
    height;

  int
    x,
    y;
} RectangleInfo;

typedef struct _RunlengthPacket
{
  Quantum
    red,
    green,
    blue,
    length;

  unsigned short
    index;
} RunlengthPacket;

typedef struct _Image
{
  FILE
    *file;

  int
    status,
    temporary;

  char
    filename[MaxTextLength];

  long int
    filesize;

  int
    pipe;

  char
    magick[MaxTextLength],
    *comments,
    *label,
    *text;

  unsigned int
    id,
#if defined(__cplusplus) || defined(c_plusplus)
    c_class,
#else
    class,
#endif
    matte,
    compression,
    columns,
    rows,
    depth,
    interlace,
    scene;

  char
    *montage,
    *directory;

  ColorPacket
    *colormap;

  unsigned int
    colorspace,
    colors;

  double
    gamma;

  short int
    units;

  float
    x_resolution,
    y_resolution;

  unsigned int
    mean_error_per_pixel;

  double
    normalized_mean_error,
    normalized_maximum_error;

  unsigned long
    total_colors;

  char
    *signature;

  RunlengthPacket
    *pixels,
    *packet;

  unsigned int
    packets,
    runlength,
    packet_size;

  unsigned char
    *packed_pixels;

  long int
    magick_time;

  char
    magick_filename[MaxTextLength];

  unsigned int
    magick_columns,
    magick_rows;

  char
    *geometry,
    *page,
    *delay;

  unsigned int
    orphan;

  struct _Image
    *previous,
    *list,
    *next;
} Image;

/*
  Image utilities routines.
*/
extern void
  CommentImage(Image *,char *),
  LabelImage(Image *,char *);

extern Image
  *AllocateImage(const ImageInfo *),
  *BorderImage(Image *,RectangleInfo *,ColorPacket *),
  *BlurImage(Image *,double),
  *ChopImage(Image *,RectangleInfo *),
  *CopyImage(Image *,const unsigned int,const unsigned int,const unsigned int),
  *CropImage(Image *,RectangleInfo *),
  *DespeckleImage(Image *),
  *EdgeImage(Image *,double),
  *EmbossImage(Image *),
  *EnhanceImage(Image *),
  *FlipImage(Image *),
  *FlopImage(Image *),
  *FrameImage(Image *,FrameInfo *),
  *ImplodeImage(Image *,double),
  *MagnifyImage(Image *),
  *MinifyImage(Image *),
  *NoisyImage(Image *),
  *OilPaintImage(Image *,const unsigned int),
  *ReadImage(ImageInfo *),
  *RollImage(Image *,int,int),
  *RotateImage(Image *,double,ColorPacket *,const unsigned int,
    const unsigned int),
  *SampleImage(Image *,unsigned int,unsigned int),
  *ScaleImage(Image *,const unsigned int,const unsigned int),
  *SharpenImage(Image *,double),
  *ShearImage(Image *,double,double,ColorPacket *,const unsigned int),
  *SpreadImage(Image *,unsigned int),
  *StereoImage(Image *,Image *),
  *SwirlImage(Image *,double),
  *ZoomImage(Image *,const unsigned int,const unsigned int,const unsigned int);

extern unsigned int
  IsGeometry(char *),
  IsGrayImage(Image *),
  IsPseudoClass(Image *),
  UncompressImage(Image *),
  WriteImage(ImageInfo *,Image *);

extern void
  AnnotateImage(Image *,AnnotateInfo *),
  CloseImage(Image *),
  ColorFloodfillImage(Image *,int,int,const ColorPacket *,const int),
  CompositeImage(Image *,const unsigned int,Image *,const int,const int),
  CompressColormap(Image *),
  CompressImage(Image *),
  ContrastImage(Image *,const unsigned int),
  DescribeImage(Image *,FILE *,const unsigned int),
  DestroyImage(Image *),
  DestroyImages(Image *),
  DrawImage(Image *,AnnotateInfo *),
  EqualizeImage(Image *),
  GammaImage(Image *,char *),
  GetAnnotateInfo(AnnotateInfo *),
  GetImageInfo(ImageInfo *),
  MapImage(Image *,Image *,const unsigned int),
  MatteFloodfillImage(Image *,int,int,const unsigned int,const int),
  ModulateImage(Image *,char *),
  MogrifyImage(ImageInfo *,int,char **,Image **),
  MogrifyImages(ImageInfo *,int,char **,unsigned int,Image **),
  NegateImage(Image *),
  NormalizeImage(Image *),
  NumberColors(Image *,FILE *),
  OpaqueImage(Image *,char *,char *),
  OpenImage(const ImageInfo *,Image *,const char *),
  ParseImageGeometry(char *,unsigned int *,unsigned int *),
  QuantizationError(Image *),
  QuantizeImage(Image *,unsigned int,const unsigned int,unsigned int,
    const unsigned int),
  QuantizeImages(Image **,const unsigned int,unsigned int,
    const unsigned int,unsigned int,const unsigned int),
  RaiseImage(Image *,const int,const unsigned int),
  RGBTransformImage(Image *,const unsigned int),
  SegmentImage(Image *,const unsigned int,const unsigned int,const double,
    const double),
  SetImageInfo(ImageInfo *),
  SignatureImage(Image *),
  SolarizeImage(Image *,const double),
  SortColormapByIntensity(Image *),
  SyncImage(Image *),
  TextureImage(Image *,char *),
  TransformImage(Image **,char *,char *),
  TransformRGBImage(Image *,const unsigned int),
  TransparentImage(Image *,char *);

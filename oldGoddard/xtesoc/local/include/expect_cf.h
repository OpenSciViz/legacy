/* expect_cf.h.  Generated automatically by configure.  */
/*
 * Check for headers
 */
#ifndef __EXPECT_CF_H__
#define __EXPECT_CF_H__

/* #undef NO_STDLIB_H */		/* Tcl requires this name */
#define HAVE_STROPTS_H 1
#define HAVE_SYSCONF_H 1
#define HAVE_SYS_FCNTL_H 1
#define HAVE_SYS_WAIT_H 1
/* #undef HAVE_SYS_BSDTYPES_H */	/* nice ISC special */
#define HAVE_SYS_SELECT_H 1	/* nice ISC special */
#define HAVE_SYS_TIME_H 1		/* nice ISC special */
/* #undef HAVE_SYS_PTEM_H */		/* SCO needs this for window size */
#define HAVE_UNISTD_H 1
#define HAVE_SYSMACROS_H 1
/* #undef HAVE_NO_TIOCGWINSZ_IN_TERMIOS_H */

/* #undef pid_t */
#define RETSIGTYPE void

/*
 * This section is for compile macros needed by
 * everything else.
 */

/*
 * Check for functions
 */
#define HAVE_MEMCPY 1
#define HAVE_WAITPID 1
#define HAVE_SYSCONF 1
/* #undef HAVE_WAIT4 */
/* #undef SIMPLE_EVENT */
#define HAVE_STRFTIME 1
#define HAVE_MEMMOVE 1

/*
 * timezone
 */
#define HAVE_SV_TIMEZONE 1

/*
 * wait status type
 */
#define NO_UNION_WAIT 1

/*
 * Signal stuff. Setup the return type
 * and if signals need to be re-armed.
 */
/*#ifndef RETSIGTYPE*/
/*#define RETSIGTYPE void*/
/*#endif*/
/*#undef REARM_SIG*/

/*
 * Generate correct type for select mask
 */
#ifndef SELECT_MASK_TYPE
#define SELECT_MASK_TYPE fd_set
#endif

/*
 * Check how stty works
 */
/* #undef STTY_READS_STDOUT */

/*
 * Check for tty/pty functions and structures
 */
#define POSIX 1
#define HAVE_TERMIO 1
#define HAVE_TERMIOS 1
#define HAVE_SGTTYB 1
/* #undef HAVE__GETPTY */
/* #undef HAVE_GETPTY */
/* #undef HAVE_PTC */
/* #undef HAVE_PTC_PTS */
/* #undef HAVE_PTYM */
/* #undef HAVE_PTYTRAP */
#define HAVE_PTMX 1
/* #undef HAVE_PTMX_BSD */

/*
 * Special hacks
 */
/* #undef CONVEX */


#endif	/* __EXPECT_CF_H__ */

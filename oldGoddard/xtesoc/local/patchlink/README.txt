                        PatchLink Update Agent 6.4185
Supported Platforms
-------------------

    +------------------+-----------------+---------------+-------------+
    + Operating System + OS Version(s)   + OS Edition    + Minimum JRE +
    +==================+=================+===============+=============+
    + Apple Mac OS X   + 10.2.8 . 10.5   + All           + JRE 1.4.0+  +
    +------------------+-----------------+---------------+-------------+
    + HP-UX            + 11.00 . 11i v2  + All           + JRE 1.4.0+  +
    +------------------+-----------------+---------------+-------------+
    + IBM AIX          + 5.1 . 5.3       + All           + JRE 1.4.0+  +
    +------------------+-----------------+---------------+-------------+
    + Red Hat Linux    + 2.1 . 5         + Enterprise,   + JRE 1.4.0+  +
    +                  +                 + AS, ES, or WS +             +
    +------------------+-----------------+---------------+-------------+
    + Novell SUSE      + 9 . 10          + Enterprise    + JRE 1.4.0+  +
    + Novell Linux     +                 + Server/Desktop+             +
    +------------------+-----------------+---------------+-------------+
    + Sun Solaris      + 2.6 . 10        + All           + JRE 1.4.0+  +
    +------------------+-----------------+---------------+-------------+


System Requirements
-------------------

  * Superuser privileges to the target machine
  * Minimum 105 Mb free disk space for the agent installation
  * Network connectivity to a Update Server 
  * Presence of /tmp directory (/var/tmp directory on Solaris) for temporary
    file storage and processing
  * Sufficient free disk space to download and install patches (varies per
    patch)


Installation Notes
------------------

   WARNING: The install (and uninstall) must to be done by the root user (the 
            superuser)

   WARNING: There must be no whitespaces (space, tab, or newline characters) 
            in the absolute path of the installation directory

1. Copy the UnixUpdateAgent.tar file to the target machine
2. Untar UnixUpdateAgent.tar using the following command:
       tar -xvf UnixUpdateAgent.tar
3. The following eight files should be visible:
      * install
      * install.ncf
      * InstallArchive.jar
      * support.tar
      * patchagent.tar
      * README.txt (this file)
      * patchagent.properties
      * env.class
4. For a MANUAL INSTALLATION, start the installation using the following 
   command:
       ./install [-reinstall] [-user XXXX] [-detectonly]
   This method will:
      a. untar the patchagent.tar file to a new directory called patchagent
      b. Gather the installation settings, by prompting the user, prior to 
         installing
         the Update Agent
5. For a SILENT INSTALLATION, start the installation using the following 
   command:
       ./install -silent [-user XXXX] [-detectonly] 
       -d <install directory> -p <PLUS address> -sno <serial number> 
      [-proxy <proxy server address> -port <proxy port> -an <agent
process nice value> -g <distinguished group names>]
   This method will:
      a. untar the patchagent.tar file to a new directory called patchagent
      b. Install the Update Agent using the provided settings
   If using the -g flag, the entire distinguished name must be entered.
   For example: OU=Test,OU=MY Groups
   If multiple groups are to be entered they must be seperated by a |.
   For example: OU=Test,OU=MY Groups | OU=Test2,OU=Test,OU=My Groups
   If your group name contains the ! character, please use the single 
	 quote around the group names.
   
Installation Modifiers
----------------------
  -user XXXX  : To make XXXX user the owner of the agent process
  -detectonly : To prevent the agent from getting any tasks from the
                Update Server 
  -reinstall  : To reinstall the agent and use the agent id from the
                previous installation
6. To remove all patchagent related system files, invoke the following
   command. Invoking this command will make the current instance of 
   the agent unusable. This command is only to be used if you intend
   to do a fresh agent install afterwards.
       ./install -cleanup

Managing the Agent
-------------------
The script patchservice (found in the patchagent directory) can be used to 
manage and debug agent functionality

USAGE: patchservice
{info|status|daustatus|detect|start|stop|restart|patchdirectory|
setmacro {ROOTDIR|TEMP|BOOTDIR|WINDIR}|trimlogs|archivelogs|
clearAgentLog|clearErrLog|clearDetectLog|proxysetup|setagentnice|help}

    info           : Returns information about the Update Agent
    status         : Returns the status of the agent process
    daustatus      : Returns the status of the detection task
    detect         : Start the detection task
    start          : Starts the agent process
    stop           : Stops the agent process
    restart        : Stops and restarts the agent process
    patchdirectory : Sets the temporarily download directory for patches
    setmacro       : Specifies the macro definition used by the agent
    trimlogs       : Trims the Update Agent logs
    archivelogs    : Archives the Update Agent logs
    clearAgentLog  : Clear the Update Agent log file
    clearErrLog    : Clear the Update Agent error log file
    clearDetectLog : Clear the Update Agent detection log file
    proxysetup     : Sets the proxy being used by the agent
    setagentnice   : Sets the agent nice values for detection and deployment
	help           : Displays the patchservice script usage information

Uninstall Notes
---------------

1. Change your working directory to the patchagent directory created during
the 
install
2. Uninstall the Update Agent using the following command:
       ./uninstall
   This will do the following:
      a. Un-register the agent from your Update Server 
      b. Terminate the agent process
      c. Prompt the user to delete the patchagent directory

         WARNING: Deleting the patchagent directory will delete all of 
                  Update Agent logs (stored under the patchagent/update/log).  
                  If necessary, create a backup of the logs prior to deleting 
                  the directory




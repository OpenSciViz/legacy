#!/bin/sh
#
# $Id: extract.sgi,v 1.2 1997/05/27 13:05:38 csm Exp lss $
#
# Usage: extract.sgi [-qp] [-d <tarfile>] <fileset> ...
#
#	-q: Perform extraction without asking questions.
#	-p: Silence all prompts.
#
#	<tarfile>: device or filename containing the tar archive.
#	<fileset>: The name of an installation fileset.
#

# subroutines to put 8-bit name(s) in place after tar extraction completes
#
# fix_8bit_names()	Does the fix for all files.
# fix_check_mv()	Internal, called by fix_8bit_names(): fixes one file.
. .install/fix_8bit_names.inc

PATH=/usr/bsd:$PATH
export PATH
scriptname=$0
query=true
extract=true
device="-"
compress="false"
prompt=true
host=#local
user=`whoami`

if test "`echo -n a`" = a
then NNN=-n CCC=        # BSD echo
else NNN= CCC='\c'      # USG echo
fi

set -- `getopt d:ceqr: $*`
for o in $*
do
   case $o in
      -c) compress="true"; shift ;;
      -d) device=$2; shift 2 ;;
      -q) query=false; shift ;;
      -p) prompt=false; shift ;;
      -r) host=$2; shift 2 ;;
      --) shift; break ;;
   esac
done

include_list="$*"
devicetype=file

[ $query = true ] && {
   echo $NNN "
   The following commands will be performed by the 
   script to finish the installation:
   
   "$CCC
      echo "	tar xbf 20 $device $include_list"

   [ $prompt = true ] && {
      echo ""
      echo "Current directory is `pwd`"

      msg="Would you like to continue? [y]:"
      echo $NNN $msg$CCC
      read answer
      until [ "$answer" = "" -o "$answer" = "y" -o "$answer" = "n" ];
      do
	 echo "Please type 'y' or 'n'."
	 echo $NNN $msg$CCC
	 read answer
      done
      [ "$answer" = "n" ] && {
	 echo ""
	 echo "The Adobe FrameMaker software has NOT been installed."
	 exit 1
      }

      #Verbosity
      V=
      msg="Would you like a listing of the files as they are installed? [y]:"
      echo $NNN $msg$CCC
      read answer
      until [ "$answer" = "" -o "$answer" = "y" -o "$answer" = "n" ];
      do
         echo "Please type 'y' or 'n'."
         echo $NNN $msg$CCC
         read answer
      done
      [ "$answer" = "" -o "$answer" = "y" ] &&  V=v
	echo "Extracting the files. . . . ."
	[ "$V" = "v" ] && {
        echo "        You may have a wait of several minutes before the
        list displays. The archive is being searched for your selections."
	} || {
        echo "        This could take more than a half hour, depending upon
        your workstation type, CD-ROM drive speed and network load."
	} # end warnings
   } # end prompt == true
} # end text == true
#
#         Read in Frame filesets
#

case $compress in
false)
   tar xo${V}bf 20 $device $include_list
   status=$? ;;
true)
   dd if=$device bs=20b | uncompress -cf | tar xo${V}bf 20 - $include_list
   status=0 ;;
esac

fix_8bit_names

[ "$status" != "0" ] && {
   echo "$scriptname: Could not read $devicetype." 1>&2
   exit 1
}

exit 0

# subroutines to put 8-bit name(s) in place after tar extraction completes
#
# fix_8bit_names()	Does the fix for all files.
# fix_check_mv()	Internal, called by fix_8bit_names(): fixes one file.

fix_check_mv()
{
	OLD_NAME="$1"
	NEW_NAME="$2"

	if [ -f "$NEW_NAME" ]
	then
		# do nothing if a file with the new name is already in place
		return 0
	fi

	if [ -f "$OLD_NAME" ]
	then
		# since there was no *file* in place, clean up
		rm -f "$NEW_NAME" < /dev/null > /dev/null 2>& 1
		rmdir "$NEW_NAME" < /dev/null > /dev/null 2>& 1

		# make a copy of the file
		cp -p "$OLD_NAME" "$NEW_NAME"

		return 0
	fi

	# if we get here the old name didn't exist and there is nothing to do
	return 0
}

fix_8bit_names()
{
	if [ ! -d fminit/deutsch ]
	then
		# No fminit/deutsch directory - nothing to do
		exit 0
	fi
	cd fminit/deutsch

	# This is a hard-coded list of 8-bit after-tar patch-up work.
	# It is not automatically generated.  If/when additional 8-bit
	# file names are needed this list must be updated.
	( fix_check_mv Uebersicht.fm �bersicht.fm )
}

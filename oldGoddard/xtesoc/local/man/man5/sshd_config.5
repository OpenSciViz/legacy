.TH SSHD_CONFIG 5 "September 25, 1999" ""
.SH NAME
\fBsshd_config\fP
\- OpenSSH SSH daemon configuration file
.SH SYNOPSIS
.TP
.B /etc/ssh/sshd_config
.SH DESCRIPTION
\fBsshd\fP
reads configuration data from
\fI/etc/ssh/sshd_config\fP
(or the file specified with
\fB\-f\fP
on the command line).
The file contains keyword-argument pairs, one per line.
Lines starting with
`#'
and empty lines are interpreted as comments.

The possible
keywords and their meanings are as follows (note that
keywords are case-insensitive and arguments are case-sensitive):
.TP
\fBAllowGroups\fP
This keyword can be followed by a list of group name patterns, separated
by spaces.
If specified, login is allowed only for users whose primary
group or supplementary group list matches one of the patterns.
`\&*'
and
`\&?'
can be used as
wildcards in the patterns.
Only group names are valid; a numerical group ID is not recognized.
By default, login is allowed for all groups.

.TP
\fBAllowTcpForwarding\fP
Specifies whether TCP forwarding is permitted.
The default is
``yes''.
Note that disabling TCP forwarding does not improve security unless
users are also denied shell access, as they can always install their
own forwarders.

.TP
\fBAllowUsers\fP
This keyword can be followed by a list of user name patterns, separated
by spaces.
If specified, login is allowed only for user names that
match one of the patterns.
`\&*'
and
`\&?'
can be used as
wildcards in the patterns.
Only user names are valid; a numerical user ID is not recognized.
By default, login is allowed for all users.
If the pattern takes the form USER@HOST then USER and HOST
are separately checked, restricting logins to particular
users from particular hosts.

.TP
\fBAuthorizedKeysFile\fP
Specifies the file that contains the public keys that can be used
for user authentication.
\fBAuthorizedKeysFile\fP
may contain tokens of the form %T which are substituted during connection
set-up.
The following tokens are defined: %% is replaced by a literal '%',
%h is replaced by the home directory of the user being authenticated and
%u is replaced by the username of that user.
After expansion,
\fBAuthorizedKeysFile\fP
is taken to be an absolute path or one relative to the user's home
directory.
The default is
``.ssh/authorized_keys''.
.TP
\fBBanner\fP
In some jurisdictions, sending a warning message before authentication
may be relevant for getting legal protection.
The contents of the specified file are sent to the remote user before
authentication is allowed.
This option is only available for protocol version 2.
By default, no banner is displayed.

.TP
\fBChallengeResponseAuthentication\fP
Specifies whether challenge response authentication is allowed.
All authentication styles from
\fBlogin.conf\fP(5)
are supported.
The default is
``yes''.
.TP
\fBCiphers\fP
Specifies the ciphers allowed for protocol version 2.
Multiple ciphers must be comma-separated.
The default is


  ``aes128-cbc,3des-cbc,blowfish-cbc,cast128-cbc,arcfour,
.br
    aes192-cbc,aes256-cbc,aes128-ctr,aes192-ctr,aes256-ctr''
.br
.TP
\fBClientAliveInterval\fP
Sets a timeout interval in seconds after which if no data has been received
from the client,
\fBsshd\fP
will send a message through the encrypted
channel to request a response from the client.
The default
is 0, indicating that these messages will not be sent to the client.
This option applies to protocol version 2 only.
.TP
\fBClientAliveCountMax\fP
Sets the number of client alive messages (see above) which may be
sent without
\fBsshd\fP
receiving any messages back from the client.
If this threshold is reached while client alive messages are being sent,
\fBsshd\fP
will disconnect the client, terminating the session.
It is important to note that the use of client alive messages is very
different from
\fBKeepAlive\fP
(below).
The client alive messages are sent through the encrypted channel
and therefore will not be spoofable.
The TCP keepalive option enabled by
\fBKeepAlive\fP
is spoofable.
The client alive mechanism is valuable when the client or
server depend on knowing when a connection has become inactive.

The default value is 3.
If
\fBClientAliveInterval\fP
(above) is set to 15, and
\fBClientAliveCountMax\fP
is left at the default, unresponsive ssh clients
will be disconnected after approximately 45 seconds.
.TP
\fBCompression\fP
Specifies whether compression is allowed.
The argument must be
``yes''
or
``no''.
The default is
``yes''.
.TP
\fBDenyGroups\fP
This keyword can be followed by a list of group name patterns, separated
by spaces.
Login is disallowed for users whose primary group or supplementary
group list matches one of the patterns.
`\&*'
and
`\&?'
can be used as
wildcards in the patterns.
Only group names are valid; a numerical group ID is not recognized.
By default, login is allowed for all groups.

.TP
\fBDenyUsers\fP
This keyword can be followed by a list of user name patterns, separated
by spaces.
Login is disallowed for user names that match one of the patterns.
`\&*'
and
`\&?'
can be used as wildcards in the patterns.
Only user names are valid; a numerical user ID is not recognized.
By default, login is allowed for all users.
If the pattern takes the form USER@HOST then USER and HOST
are separately checked, restricting logins to particular
users from particular hosts.
.TP
\fBGatewayPorts\fP
Specifies whether remote hosts are allowed to connect to ports
forwarded for the client.
By default,
\fBsshd\fP
binds remote port forwardings to the loopback address.
This prevents other remote hosts from connecting to forwarded ports.
\fBGatewayPorts\fP
can be used to specify that
\fBsshd\fP
should bind remote port forwardings to the wildcard address,
thus allowing remote hosts to connect to forwarded ports.
The argument must be
``yes''
or
``no''.
The default is
``no''.
.TP
\fBGSSAPIAuthentication\fP
Specifies whether user authentication based on GSSAPI is allowed.
The default is 
``no''.
Note that this option applies to protocol version 2 only.
.TP
\fBGSSAPICleanupCredentials\fP
Specifies whether to automatically destroy the user's credentials cache
on logout.
The default is
``yes''.
Note that this option applies to protocol version 2 only.
.TP
\fBHostbasedAuthentication\fP
Specifies whether rhosts or /etc/hosts.equiv authentication together
with successful public key client host authentication is allowed
(hostbased authentication).
This option is similar to
\fBRhostsRSAAuthentication\fP
and applies to protocol version 2 only.
The default is
``no''.
.TP
\fBHostKey\fP
Specifies a file containing a private host key
used by SSH.
The default is
\fI/etc/ssh/ssh_host_key\fP
for protocol version 1, and
\fI/etc/ssh/ssh_host_rsa_key\fP
and
\fI/etc/ssh/ssh_host_dsa_key\fP
for protocol version 2.
Note that
\fBsshd\fP
will refuse to use a file if it is group/world-accessible.
It is possible to have multiple host key files.
``rsa1''
keys are used for version 1 and
``dsa''
or
``rsa''
are used for version 2 of the SSH protocol.
.TP
\fBIgnoreRhosts\fP
Specifies that
\fI\&.rhosts\fP
and
\fI\&.shosts\fP
files will not be used in
\fBRhostsRSAAuthentication\fP
or
\fBHostbasedAuthentication\fP.

\fI/etc/hosts.equiv\fP
and
\fI/etc/ssh/shosts.equiv\fP
are still used.
The default is
``yes''.
.TP
\fBIgnoreUserKnownHosts\fP
Specifies whether
\fBsshd\fP
should ignore the user's
\fI$HOME/.ssh/known_hosts\fP
during
\fBRhostsRSAAuthentication\fP
or
\fBHostbasedAuthentication\fP.
The default is
``no''.
.TP
\fBKeepAlive\fP
Specifies whether the system should send TCP keepalive messages to the
other side.
If they are sent, death of the connection or crash of one
of the machines will be properly noticed.
However, this means that
connections will die if the route is down temporarily, and some people
find it annoying.
On the other hand, if keepalives are not sent,
sessions may hang indefinitely on the server, leaving
``ghost''
users and consuming server resources.

The default is
``yes''
(to send keepalives), and the server will notice
if the network goes down or the client host crashes.
This avoids infinitely hanging sessions.

To disable keepalives, the value should be set to
``no''.
.TP
\fBKerberosAuthentication\fP
Specifies whether the password provided by the user for
\fBPasswordAuthentication\fP
will be validated through the Kerberos KDC.
To use this option, the server needs a
Kerberos servtab which allows the verification of the KDC's identity.
Default is
``no''.
.TP
\fBKerberosOrLocalPasswd\fP
If set then if password authentication through Kerberos fails then
the password will be validated via any additional local mechanism
such as
\fI/etc/passwd\fP.
Default is
``yes''.
.TP
\fBKerberosTicketCleanup\fP
Specifies whether to automatically destroy the user's ticket cache
file on logout.
Default is
``yes''.
.TP
\fBKeyRegenerationInterval\fP
In protocol version 1, the ephemeral server key is automatically regenerated
after this many seconds (if it has been used).
The purpose of regeneration is to prevent
decrypting captured sessions by later breaking into the machine and
stealing the keys.
The key is never stored anywhere.
If the value is 0, the key is never regenerated.
The default is 3600 (seconds).
.TP
\fBListenAddress\fP
Specifies the local addresses
\fBsshd\fP
should listen on.
The following forms may be used:

.IP
\fBListenAddress\fP
\fIhost\fP|\fIIPv4_addr\fP|\fIIPv6_addr\fP
.IP
\fBListenAddress\fP
\fIhost\fP|\fIIPv4_addr\fP:\fIport\fP
.IP
\fBListenAddress\fP
[\fIhost\fP|\fIIPv6_addr\fP]:\fIport\fP

If
\fIport\fP
is not specified,
\fBsshd\fP
will listen on the address and all prior
\fBPort\fP
options specified.
The default is to listen on all local addresses.
Multiple
\fBListenAddress\fP
options are permitted.
Additionally, any
\fBPort\fP
options must precede this option for non port qualified addresses.
.TP
\fBLoginGraceTime\fP
The server disconnects after this time if the user has not
successfully logged in.
If the value is 0, there is no time limit.
The default is 120 seconds.
.TP
\fBLogLevel\fP
Gives the verbosity level that is used when logging messages from
\fBsshd\fP.
The possible values are:
QUIET, FATAL, ERROR, INFO, VERBOSE, DEBUG, DEBUG1, DEBUG2 and DEBUG3.
The default is INFO.
DEBUG and DEBUG1 are equivalent.
DEBUG2 and DEBUG3 each specify higher levels of debugging output.
Logging with a DEBUG level violates the privacy of users and is not recommended.
.TP
\fBMACs\fP
Specifies the available MAC (message authentication code) algorithms.
The MAC algorithm is used in protocol version 2
for data integrity protection.
Multiple algorithms must be comma-separated.
The default is
``hmac-md5,hmac-sha1,hmac-ripemd160,hmac-sha1-96,hmac-md5-96''.
.TP
\fBMaxStartups\fP
Specifies the maximum number of concurrent unauthenticated connections to the
\fBsshd\fP
daemon.
Additional connections will be dropped until authentication succeeds or the
\fBLoginGraceTime\fP
expires for a connection.
The default is 10.

Alternatively, random early drop can be enabled by specifying
the three colon separated values
``start:rate:full''
(e.g., "10:30:60").
\fBsshd\fP
will refuse connection attempts with a probability of
``rate/100''
(30%)
if there are currently
``start''
(10)
unauthenticated connections.
The probability increases linearly and all connection attempts
are refused if the number of unauthenticated connections reaches
``full''
(60).
.TP
\fBPasswordAuthentication\fP
Specifies whether password authentication is allowed.
The default is
``yes''.
.TP
\fBPermitEmptyPasswords\fP
When password authentication is allowed, it specifies whether the
server allows login to accounts with empty password strings.
The default is
``no''.
.TP
\fBPermitRootLogin\fP
Specifies whether root can login using
\fBssh\fP(1).
The argument must be
``yes'',
``without-password'',
``forced-commands-only''
or
``no''.
The default is
``yes''.

If this option is set to
``without-password''
password authentication is disabled for root.

If this option is set to
``forced-commands-only''
root login with public key authentication will be allowed,
but only if the
\fIcommand\fP
option has been specified
(which may be useful for taking remote backups even if root login is
normally not allowed).
All other authentication methods are disabled for root.

If this option is set to
``no''
root is not allowed to login.
.TP
\fBPermitUserEnvironment\fP
Specifies whether
\fI~/.ssh/environment\fP
and
\fBenvironment=\fP
options in
\fI~/.ssh/authorized_keys\fP
are processed by
\fBsshd\fP.
The default is
``no''.
Enabling environment processing may enable users to bypass access
restrictions in some configurations using mechanisms such as
.IR LD_PRELOAD .
.TP
\fBPidFile\fP
Specifies the file that contains the process ID of the
\fBsshd\fP
daemon.
The default is
\fI/etc/ssh/sshd.pid\fP.
.TP
\fBPort\fP
Specifies the port number that
\fBsshd\fP
listens on.
The default is 22.
Multiple options of this type are permitted.
See also
\fBListenAddress\fP.
.TP
\fBPrintLastLog\fP
Specifies whether
\fBsshd\fP
should print the date and time when the user last logged in.
The default is
``yes''.
.TP
\fBPrintMotd\fP
Specifies whether
\fBsshd\fP
should print
\fI/etc/motd\fP
when a user logs in interactively.
(On some systems it is also printed by the shell,
\fI/etc/profile\fP,
or equivalent.)
The default is
``yes''.
.TP
\fBProtocol\fP
Specifies the protocol versions
\fBsshd\fP
supports.
The possible values are
``1''
and
``2''.
Multiple versions must be comma-separated.
The default is
``2,1''.
Note that the order of the protocol list does not indicate preference,
because the client selects among multiple protocol versions offered
by the server.
Specifying
``2,1''
is identical to
``1,2''.
.TP
\fBPubkeyAuthentication\fP
Specifies whether public key authentication is allowed.
The default is
``yes''.
Note that this option applies to protocol version 2 only.
\fBRhostsRSAAuthentication\fP
should be used
instead, because it performs RSA-based host authentication in addition
to normal rhosts or /etc/hosts.equiv authentication.
The default is
``no''.
This option applies to protocol version 1 only.
.TP
\fBRhostsRSAAuthentication\fP
Specifies whether rhosts or /etc/hosts.equiv authentication together
with successful RSA host authentication is allowed.
The default is
``no''.
This option applies to protocol version 1 only.
.TP
\fBRSAAuthentication\fP
Specifies whether pure RSA authentication is allowed.
The default is
``yes''.
This option applies to protocol version 1 only.
.TP
\fBServerKeyBits\fP
Defines the number of bits in the ephemeral protocol version 1 server key.
The minimum value is 512, and the default is 768.
.TP
\fBStrictModes\fP
Specifies whether
\fBsshd\fP
should check file modes and ownership of the
user's files and home directory before accepting login.
This is normally desirable because novices sometimes accidentally leave their
directory or files world-writable.
The default is
``yes''.
.TP
\fBSubsystem\fP
Configures an external subsystem (e.g., file transfer daemon).
Arguments should be a subsystem name and a command to execute upon subsystem
request.
The command
\fBsftp-server\fP(8)
implements the
``sftp''
file transfer subsystem.
By default no subsystems are defined.
Note that this option applies to protocol version 2 only.
.TP
\fBSyslogFacility\fP
Gives the facility code that is used when logging messages from
\fBsshd\fP.
The possible values are: DAEMON, USER, AUTH, LOCAL0, LOCAL1, LOCAL2,
LOCAL3, LOCAL4, LOCAL5, LOCAL6, LOCAL7.
The default is AUTH.
.TP
\fBUseDNS\fP
Specifies whether
\fBsshd\fP
should lookup the remote host name and check that
the resolved host name for the remote IP address maps back to the
very same IP address.
The default is
``yes''.
.TP
\fBUseLogin\fP
Specifies whether
\fBlogin\fP(1)
is used for interactive login sessions.
The default is
``no''.
Note that
\fBlogin\fP(1)
is never used for remote command execution.
Note also, that if this is enabled,
\fBX11Forwarding\fP
will be disabled because
\fBlogin\fP(1)
does not know how to handle
\fBxauth\fP(1)
cookies.
If
\fBUsePrivilegeSeparation\fP
is specified, it will be disabled after authentication.
.TP
\fBUsePAM\fP
Enables PAM authentication (via challenge-response) and session set up. 
If you enable this, you should probably disable 
\fBPasswordAuthentication\fP.
If you enable 
CM UsePAM
then you will not be able to run sshd as a non-root user.
.TP
\fBUsePrivilegeSeparation\fP
Specifies whether
\fBsshd\fP
separates privileges by creating an unprivileged child process
to deal with incoming network traffic.
After successful authentication, another process will be created that has
the privilege of the authenticated user.
The goal of privilege separation is to prevent privilege
escalation by containing any corruption within the unprivileged processes.
The default is
``yes''.
.TP
\fBX11DisplayOffset\fP
Specifies the first display number available for
\fBsshd\fP's
X11 forwarding.
This prevents
\fBsshd\fP
from interfering with real X11 servers.
The default is 10.
.TP
\fBX11Forwarding\fP
Specifies whether X11 forwarding is permitted.
The argument must be
``yes''
or
``no''.
The default is
``no''.

When X11 forwarding is enabled, there may be additional exposure to
the server and to client displays if the
\fBsshd\fP
proxy display is configured to listen on the wildcard address (see
\fBX11UseLocalhost\fP
below), however this is not the default.
Additionally, the authentication spoofing and authentication data
verification and substitution occur on the client side.
The security risk of using X11 forwarding is that the client's X11
display server may be exposed to attack when the ssh client requests
forwarding (see the warnings for
\fBForwardX11\fP
in
\fBssh_config\fP(5)) .
A system administrator may have a stance in which they want to
protect clients that may expose themselves to attack by unwittingly
requesting X11 forwarding, which can warrant a
``no''
setting.

Note that disabling X11 forwarding does not prevent users from
forwarding X11 traffic, as users can always install their own forwarders.
X11 forwarding is automatically disabled if
\fBUseLogin\fP
is enabled.
.TP
\fBX11UseLocalhost\fP
Specifies whether
\fBsshd\fP
should bind the X11 forwarding server to the loopback address or to
the wildcard address.
By default,
\fBsshd\fP
binds the forwarding server to the loopback address and sets the
hostname part of the
.IR DISPLAY
environment variable to
``localhost''.
This prevents remote hosts from connecting to the proxy display.
However, some older X11 clients may not function with this
configuration.
\fBX11UseLocalhost\fP
may be set to
``no''
to specify that the forwarding server should be bound to the wildcard
address.
The argument must be
``yes''
or
``no''.
The default is
``yes''.
.TP
\fBXAuthLocation\fP
Specifies the full pathname of the
\fBxauth\fP(1)
program.
The default is
\fI/usr/openwin/bin/xauth\fP.
.SS Time Formats
\fBsshd\fP
command-line arguments and configuration file options that specify time
may be expressed using a sequence of the form:
\fItime\fP[\fIqualifier\fP,]
where
\fItime\fP
is a positive integer value and
\fIqualifier\fP
is one of the following:

.TP
\fB<none>\fP
seconds
.TP
\fBs\fP | \fBS\fP
seconds
.TP
\fBm\fP | \fBM\fP
minutes
.TP
\fBh\fP | \fBH\fP
hours
.TP
\fBd\fP | \fBD\fP
days
.TP
\fBw\fP | \fBW\fP
weeks

Each member of the sequence is added together to calculate
the total time value.

Time format examples:

.TP
600
600 seconds (10 minutes)
.TP
10m
10 minutes
.TP
1h30m
1 hour 30 minutes (90 minutes)
.SH FILES
.TP
.B /etc/ssh/sshd_config
Contains configuration data for
\fBsshd\fP.
This file should be writable by root only, but it is recommended
(though not necessary) that it be world-readable.
.SH SEE ALSO
\fBsshd\fP(8)
.SH AUTHORS
OpenSSH is a derivative of the original and free
ssh 1.2.12 release by Tatu Ylonen.
Aaron Campbell, Bob Beck, Markus Friedl, Niels Provos,
Theo de Raadt and Dug Song
removed many bugs, re-added newer features and
created OpenSSH.
Markus Friedl contributed the support for SSH
protocol versions 1.5 and 2.0.
Niels Provos and Markus Friedl contributed support
for privilege separation.

.TH SSH_CONFIG 5 "September 25, 1999" ""
.SH NAME
\fBssh_config\fP
\- OpenSSH SSH client configuration files
.SH SYNOPSIS
.TP
.B $HOME/.ssh/config
.TP
.B /etc/ssh/ssh_config
.SH DESCRIPTION
\fBssh\fP
obtains configuration data from the following sources in
the following order:
.IP 1.
command-line options
.IP 2.
user's configuration file
(\fI$HOME/.ssh/config\fP)
.IP 3.
system-wide configuration file
(\fI/etc/ssh/ssh_config\fP)

For each parameter, the first obtained value
will be used.
The configuration files contain sections bracketed by
``Host''
specifications, and that section is only applied for hosts that
match one of the patterns given in the specification.
The matched host name is the one given on the command line.

Since the first obtained value for each parameter is used, more
host-specific declarations should be given near the beginning of the
file, and general defaults at the end.

The configuration file has the following format:

Empty lines and lines starting with
`#'
are comments.

Otherwise a line is of the format
``keyword arguments''.
Configuration options may be separated by whitespace or
optional whitespace and exactly one
`=' ;
the latter format is useful to avoid the need to quote whitespace
when specifying configuration options using the
\fBssh\fP,
\fBscp\fP
and
\fBsftp\fP
\fB\-o\fP
option.

The possible
keywords and their meanings are as follows (note that
keywords are case-insensitive and arguments are case-sensitive):
.TP
\fBHost\fP
Restricts the following declarations (up to the next
\fBHost\fP
keyword) to be only for those hosts that match one of the patterns
given after the keyword.
`\&*'
and
`\&?'
can be used as wildcards in the
patterns.
A single
`\&*'
as a pattern can be used to provide global
defaults for all hosts.
The host is the
\fIhostname\fP
argument given on the command line (i.e., the name is not converted to
a canonicalized host name before matching).
.TP
\fBAddressFamily\fP
Specifies which address family to use when connecting.
Valid arguments are
``any'',
``inet''
(Use IPv4 only) or
``inet6''
(Use IPv6 only.)
.TP
\fBBatchMode\fP
If set to
``yes'',
passphrase/password querying will be disabled.
This option is useful in scripts and other batch jobs where no user
is present to supply the password.
The argument must be
``yes''
or
``no''.
The default is
``no''.
.TP
\fBBindAddress\fP
Specify the interface to transmit from on machines with multiple
interfaces or aliased addresses.
Note that this option does not work if
\fBUsePrivilegedPort\fP
is set to
``yes''.
.TP
\fBChallengeResponseAuthentication\fP
Specifies whether to use challenge response authentication.
The argument to this keyword must be
``yes''
or
``no''.
The default is
``yes''.
.TP
\fBCheckHostIP\fP
If this flag is set to
``yes'',
ssh will additionally check the host IP address in the
\fIknown_hosts\fP
file.
This allows ssh to detect if a host key changed due to DNS spoofing.
If the option is set to
``no'',
the check will not be executed.
The default is
``yes''.
.TP
\fBCipher\fP
Specifies the cipher to use for encrypting the session
in protocol version 1.
Currently,
``blowfish'',
``3des'',
and
``des''
are supported.
\fIdes\fP
is only supported in the
\fBssh\fP
client for interoperability with legacy protocol 1 implementations
that do not support the
\fI3des\fP
cipher.
Its use is strongly discouraged due to cryptographic weaknesses.
The default is
``3des''.
.TP
\fBCiphers\fP
Specifies the ciphers allowed for protocol version 2
in order of preference.
Multiple ciphers must be comma-separated.
The default is


  ``aes128-cbc,3des-cbc,blowfish-cbc,cast128-cbc,arcfour,
.br
    aes192-cbc,aes256-cbc''
.br
.TP
\fBClearAllForwardings\fP
Specifies that all local, remote and dynamic port forwardings
specified in the configuration files or on the command line be
cleared.
This option is primarily useful when used from the
\fBssh\fP
command line to clear port forwardings set in
configuration files, and is automatically set by
\fBscp\fP(1)
and
\fBsftp\fP(1).
The argument must be
``yes''
or
``no''.
The default is
``no''.
.TP
\fBCompression\fP
Specifies whether to use compression.
The argument must be
``yes''
or
``no''.
The default is
``no''.
.TP
\fBCompressionLevel\fP
Specifies the compression level to use if compression is enabled.
The argument must be an integer from 1 (fast) to 9 (slow, best).
The default level is 6, which is good for most applications.
The meaning of the values is the same as in
\fBgzip\fP(1).
Note that this option applies to protocol version 1 only.
.TP
\fBConnectionAttempts\fP
Specifies the number of tries (one per second) to make before exiting.
The argument must be an integer.
This may be useful in scripts if the connection sometimes fails.
The default is 1.
.TP
\fBConnectTimeout\fP
Specifies the timeout (in seconds) used when connecting to the ssh
server, instead of using the default system TCP timeout.
This value is used only when the target is down or really unreachable,
not when it refuses the connection.
.TP
\fBDynamicForward\fP
Specifies that a TCP/IP port on the local machine be forwarded
over the secure channel, and the application
protocol is then used to determine where to connect to from the
remote machine.
The argument must be a port number.
Currently the SOCKS4 and SOCKS5 protocols are supported, and
\fBssh\fP
will act as a SOCKS server.
Multiple forwardings may be specified, and
additional forwardings can be given on the command line.
Only the superuser can forward privileged ports.
.TP
\fBEnableSSHKeysign\fP
Setting this option to
``yes''
in the global client configuration file
\fI/etc/ssh/ssh_config\fP
enables the use of the helper program
\fBssh-keysign\fP(8)
during
\fBHostbasedAuthentication\fP.
The argument must be
``yes''
or
``no''.
The default is
``no''.
See
\fBssh-keysign\fP(8)
for more information.
.TP
\fBEscapeChar\fP
Sets the escape character (default:
`~' ) .
The escape character can also
be set on the command line.
The argument should be a single character,
`^'
followed by a letter, or
``none''
to disable the escape
character entirely (making the connection transparent for binary
data).
.TP
\fBForwardAgent\fP
Specifies whether the connection to the authentication agent (if any)
will be forwarded to the remote machine.
The argument must be
``yes''
or
``no''.
The default is
``no''.

Agent forwarding should be enabled with caution.
Users with the ability to bypass file permissions on the remote host
(for the agent's Unix-domain socket)
can access the local agent through the forwarded connection.
An attacker cannot obtain key material from the agent,
however they can perform operations on the keys that enable them to
authenticate using the identities loaded into the agent.
.TP
\fBForwardX11\fP
Specifies whether X11 connections will be automatically redirected
over the secure channel and
.IR DISPLAY
set.
The argument must be
``yes''
or
``no''.
The default is
``no''.

X11 forwarding should be enabled with caution.
Users with the ability to bypass file permissions on the remote host
(for the user's X authorization database)
can access the local X11 display through the forwarded connection.
An attacker may then be able to perform activities such as keystroke monitoring.
.TP
\fBGatewayPorts\fP
Specifies whether remote hosts are allowed to connect to local
forwarded ports.
By default,
\fBssh\fP
binds local port forwardings to the loopback address.
This prevents other remote hosts from connecting to forwarded ports.
\fBGatewayPorts\fP
can be used to specify that
\fBssh\fP
should bind local port forwardings to the wildcard address,
thus allowing remote hosts to connect to forwarded ports.
The argument must be
``yes''
or
``no''.
The default is
``no''.
.TP
\fBGlobalKnownHostsFile\fP
Specifies a file to use for the global
host key database instead of
\fI/etc/ssh/ssh_known_hosts\fP.
.TP
\fBGSSAPIAuthentication\fP
Specifies whether authentication based on GSSAPI may be used, either using
the result of a successful key exchange, or using GSSAPI user
authentication.
The default is
``yes''.
Note that this option applies to protocol version 2 only.
.TP
\fBGSSAPIDelegateCredentials\fP
Forward (delegate) credentials to the server.
The default is
``no''.
Note that this option applies to protocol version 2 only.
.TP
\fBHostbasedAuthentication\fP
Specifies whether to try rhosts based authentication with public key
authentication.
The argument must be
``yes''
or
``no''.
The default is
``no''.
This option applies to protocol version 2 only and
is similar to
\fBRhostsRSAAuthentication\fP.
.TP
\fBHostKeyAlgorithms\fP
Specifies the protocol version 2 host key algorithms
that the client wants to use in order of preference.
The default for this option is:
``ssh-rsa,ssh-dss''.
.TP
\fBHostKeyAlias\fP
Specifies an alias that should be used instead of the
real host name when looking up or saving the host key
in the host key database files.
This option is useful for tunneling ssh connections
or for multiple servers running on a single host.
.TP
\fBHostName\fP
Specifies the real host name to log into.
This can be used to specify nicknames or abbreviations for hosts.
Default is the name given on the command line.
Numeric IP addresses are also permitted (both on the command line and in
\fBHostName\fP
specifications).
.TP
\fBIdentityFile\fP
Specifies a file from which the user's RSA or DSA authentication identity
is read.
The default is
\fI$HOME/.ssh/identity\fP
for protocol version 1, and
\fI$HOME/.ssh/id_rsa\fP
and
\fI$HOME/.ssh/id_dsa\fP
for protocol version 2.
Additionally, any identities represented by the authentication agent
will be used for authentication.
The file name may use the tilde
syntax to refer to a user's home directory.
It is possible to have
multiple identity files specified in configuration files; all these
identities will be tried in sequence.
.TP
\fBKeepAlive\fP
Specifies whether the system should send TCP keepalive messages to the
other side.
If they are sent, death of the connection or crash of one
of the machines will be properly noticed.
However, this means that
connections will die if the route is down temporarily, and some people
find it annoying.

The default is
``yes''
(to send keepalives), and the client will notice
if the network goes down or the remote host dies.
This is important in scripts, and many users want it too.

To disable keepalives, the value should be set to
``no''.
.TP
\fBLocalForward\fP
Specifies that a TCP/IP port on the local machine be forwarded over
the secure channel to the specified host and port from the remote machine.
The first argument must be a port number, and the second must be
\fIhost:port\fP.
IPv6 addresses can be specified with an alternative syntax:
\fIhost/port\fP.
Multiple forwardings may be specified, and additional
forwardings can be given on the command line.
Only the superuser can forward privileged ports.
.TP
\fBLogLevel\fP
Gives the verbosity level that is used when logging messages from
\fBssh\fP.
The possible values are:
QUIET, FATAL, ERROR, INFO, VERBOSE, DEBUG, DEBUG1, DEBUG2 and DEBUG3.
The default is INFO.
DEBUG and DEBUG1 are equivalent.
DEBUG2 and DEBUG3 each specify higher levels of verbose output.
.TP
\fBMACs\fP
Specifies the MAC (message authentication code) algorithms
in order of preference.
The MAC algorithm is used in protocol version 2
for data integrity protection.
Multiple algorithms must be comma-separated.
The default is
``hmac-md5,hmac-sha1,hmac-ripemd160,hmac-sha1-96,hmac-md5-96''.
.TP
\fBNoHostAuthenticationForLocalhost\fP
This option can be used if the home directory is shared across machines.
In this case localhost will refer to a different machine on each of
the machines and the user will get many warnings about changed host keys.
However, this option disables host authentication for localhost.
The argument to this keyword must be
``yes''
or
``no''.
The default is to check the host key for localhost.
.TP
\fBNumberOfPasswordPrompts\fP
Specifies the number of password prompts before giving up.
The argument to this keyword must be an integer.
Default is 3.
.TP
\fBPasswordAuthentication\fP
Specifies whether to use password authentication.
The argument to this keyword must be
``yes''
or
``no''.
The default is
``yes''.
.TP
\fBPort\fP
Specifies the port number to connect on the remote host.
Default is 22.
.TP
\fBPreferredAuthentications\fP
Specifies the order in which the client should try protocol 2
authentication methods.
This allows a client to prefer one method (e.g.
\fBkeyboard-interactive\fP)
over another method (e.g.
\fBpassword\fP)
The default for this option is:
``hostbased,publickey,keyboard-interactive,password''.
.TP
\fBProtocol\fP
Specifies the protocol versions
\fBssh\fP
should support in order of preference.
The possible values are
``1''
and
``2''.
Multiple versions must be comma-separated.
The default is
``2,1''.
This means that
\fBssh\fP
tries version 2 and falls back to version 1
if version 2 is not available.
.TP
\fBProxyCommand\fP
Specifies the command to use to connect to the server.
The command
string extends to the end of the line, and is executed with
\fI/bin/sh\fP.
In the command string,
`%h'
will be substituted by the host name to
connect and
`%p'
by the port.
The command can be basically anything,
and should read from its standard input and write to its standard output.
It should eventually connect an
\fBsshd\fP(8)
server running on some machine, or execute
\fBsshd -i\fP
somewhere.
Host key management will be done using the
HostName of the host being connected (defaulting to the name typed by
the user).
Setting the command to
``none''
disables this option entirely.
Note that
\fBCheckHostIP\fP
is not available for connects with a proxy command.

.TP
\fBPubkeyAuthentication\fP
Specifies whether to try public key authentication.
The argument to this keyword must be
``yes''
or
``no''.
The default is
``yes''.
This option applies to protocol version 2 only.
.TP
\fBRemoteForward\fP
Specifies that a TCP/IP port on the remote machine be forwarded over
the secure channel to the specified host and port from the local machine.
The first argument must be a port number, and the second must be
\fIhost:port\fP.
IPv6 addresses can be specified with an alternative syntax:
\fIhost/port\fP.
Multiple forwardings may be specified, and additional
forwardings can be given on the command line.
Only the superuser can forward privileged ports.
.TP
\fBRhostsRSAAuthentication\fP
Specifies whether to try rhosts based authentication with RSA host
authentication.
The argument must be
``yes''
or
``no''.
The default is
``no''.
This option applies to protocol version 1 only and requires
\fBssh\fP
to be setuid root.
.TP
\fBRSAAuthentication\fP
Specifies whether to try RSA authentication.
The argument to this keyword must be
``yes''
or
``no''.
RSA authentication will only be
attempted if the identity file exists, or an authentication agent is
running.
The default is
``yes''.
Note that this option applies to protocol version 1 only.
.TP
\fBSmartcardDevice\fP
Specifies which smartcard device to use.
The argument to this keyword is the device
\fBssh\fP
should use to communicate with a smartcard used for storing the user's
private RSA key.
By default, no device is specified and smartcard support is not activated.
.TP
\fBStrictHostKeyChecking\fP
If this flag is set to
``yes'',
\fBssh\fP
will never automatically add host keys to the
\fI$HOME/.ssh/known_hosts\fP
file, and refuses to connect to hosts whose host key has changed.
This provides maximum protection against trojan horse attacks,
however, can be annoying when the
\fI/etc/ssh/ssh_known_hosts\fP
file is poorly maintained, or connections to new hosts are
frequently made.
This option forces the user to manually
add all new hosts.
If this flag is set to
``no'',
\fBssh\fP
will automatically add new host keys to the
user known hosts files.
If this flag is set to
``ask'',
new host keys
will be added to the user known host files only after the user
has confirmed that is what they really want to do, and
\fBssh\fP
will refuse to connect to hosts whose host key has changed.
The host keys of
known hosts will be verified automatically in all cases.
The argument must be
``yes'',
``no''
or
``ask''.
The default is
``ask''.
.TP
\fBUsePrivilegedPort\fP
Specifies whether to use a privileged port for outgoing connections.
The argument must be
``yes''
or
``no''.
The default is
``no''.
If set to
``yes''
\fBssh\fP
must be setuid root.
Note that this option must be set to
``yes''
for
\fBRhostsRSAAuthentication\fP
with older servers.
.TP
\fBUser\fP
Specifies the user to log in as.
This can be useful when a different user name is used on different machines.
This saves the trouble of
having to remember to give the user name on the command line.
.TP
\fBUserKnownHostsFile\fP
Specifies a file to use for the user
host key database instead of
\fI$HOME/.ssh/known_hosts\fP.
.TP
\fBVerifyHostKeyDNS\fP
Specifies whether to verify the remote key using DNS and SSHFP resource
records.
The default is
``no''.
Note that this option applies to protocol version 2 only.
.TP
\fBXAuthLocation\fP
Specifies the full pathname of the
\fBxauth\fP(1)
program.
The default is
\fI/usr/openwin/bin/xauth\fP.
.SH FILES
.TP
.B $HOME/.ssh/config
This is the per-user configuration file.
The format of this file is described above.
This file is used by the
\fBssh\fP
client.
This file does not usually contain any sensitive information,
but the recommended permissions are read/write for the user, and not
accessible by others.
.TP
.B /etc/ssh/ssh_config
Systemwide configuration file.
This file provides defaults for those
values that are not specified in the user's configuration file, and
for those users who do not have a configuration file.
This file must be world-readable.
.SH SEE ALSO
\fBssh\fP(1)
.SH AUTHORS
OpenSSH is a derivative of the original and free
ssh 1.2.12 release by Tatu Ylonen.
Aaron Campbell, Bob Beck, Markus Friedl, Niels Provos,
Theo de Raadt and Dug Song
removed many bugs, re-added newer features and
created OpenSSH.
Markus Friedl contributed the support for SSH
protocol versions 1.5 and 2.0.

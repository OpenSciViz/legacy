'\"
'\" Copyright (c) 1994 The Regents of the University of California.
'\" Copyright (c) 1994 Sun Microsystems, Inc.
'\"
'\" See the file "license.terms" for information on usage and redistribution
'\" of this file, and for a DISCLAIMER OF ALL WARRANTIES.
'\" 
'\" @(#) clipboard.n 1.2 94/12/17 16:04:31
'/" 
'\" The definitions below are for supplemental macros used in Tcl/Tk
'\" manual entries.
'\"
'\" .HS name section [date [version]]
'\"	Replacement for .TH in other man pages.  See below for valid
'\"	section names.
'\"
'\" .AP type name in/out [indent]
'\"	Start paragraph describing an argument to a library procedure.
'\"	type is type of argument (int, etc.), in/out is either "in", "out",
'\"	or "in/out" to describe whether procedure reads or modifies arg,
'\"	and indent is equivalent to second arg of .IP (shouldn't ever be
'\"	needed;  use .AS below instead)
'\"
'\" .AS [type [name]]
'\"	Give maximum sizes of arguments for setting tab stops.  Type and
'\"	name are examples of largest possible arguments that will be passed
'\"	to .AP later.  If args are omitted, default tab stops are used.
'\"
'\" .BS
'\"	Start box enclosure.  From here until next .BE, everything will be
'\"	enclosed in one large box.
'\"
'\" .BE
'\"	End of box enclosure.
'\"
'\" .VS
'\"	Begin vertical sidebar, for use in marking newly-changed parts
'\"	of man pages.
'\"
'\" .VE
'\"	End of vertical sidebar.
'\"
'\" .DS
'\"	Begin an indented unfilled display.
'\"
'\" .DE
'\"	End of indented unfilled display.
'\"
'\" @(#) man.macros 1.1 94/08/09 13:07:19
.\"
'\"	# Heading for Tcl/Tk man pages
.de HS
.ds ^3 \\0
.if !"\\$3"" .ds ^3 \\$3
.if '\\$2'cmds'      .TH "\\$1" 1 "\\*(^3" "\\$4" "\\$5"
.if '\\$2'lib'       .TH "\\$1" 3 "\\*(^3" "\\$4" "\\$5"
.if '\\$2'ncmds'     .TH "\\$1" n "\\*(^3" "\\$4" "\\$5"
.if '\\$2'tcl'       .TH "\\$1" n "\\*(^3"  Tcl   "Tcl Built-In Commands"
.if '\\$2'tk'        .TH "\\$1" n "\\*(^3"  Tk    "Tk Commands"
.if '\\$2'tclc'      .TH "\\$1" 3 "\\*(^3"  Tcl   "Tcl Library Procedures"
.if '\\$2'tkc'       .TH "\\$1" 3 "\\*(^3"  Tk    "Tk Library Procedures"
.if '\\$2'tclcmds'   .TH "\\$1" 1 "\\*(^3"  Tk    "Tcl Applications"
.if '\\$2'tkcmds'    .TH "\\$1" 1 "\\*(^3"  Tk    "Tk Applications"
.if t .wh -1.3i ^B
.nr ^l \\n(.l
.ad b
..
'\"	# Start an argument description
.de AP
.ie !"\\$4"" .TP \\$4
.el \{\
.   ie !"\\$2"" .TP \\n()Cu
.   el          .TP 15
.\}
.ie !"\\$3"" \{\
.ta \\n()Au \\n()Bu
\&\\$1	\\fI\\$2\\fP	(\\$3)
.\".b
.\}
.el \{\
.br
.ie !"\\$2"" \{\
\&\\$1	\\fI\\$2\\fP
.\}
.el \{\
\&\\fI\\$1\\fP
.\}
.\}
..
'\"	# define tabbing values for .AP
.de AS
.nr )A 10n
.if !"\\$1"" .nr )A \\w'\\$1'u+3n
.nr )B \\n()Au+15n
.\"
.if !"\\$2"" .nr )B \\w'\\$2'u+\\n()Au+3n
.nr )C \\n()Bu+\\w'(in/out)'u+2n
..
'\"	# BS - start boxed text
'\"	# ^y = starting y location
'\"	# ^b = 1
.de BS
.br
.mk ^y
.nr ^b 1u
.if n .nf
.if n .ti 0
.if n \l'\\n(.lu\(ul'
.if n .fi
..
'\"	# BE - end boxed text (draw box now)
.de BE
.nf
.ti 0
.mk ^t
.ie n \l'\\n(^lu\(ul'
.el \{\
.\"	Draw four-sided box normally, but don't draw top of
.\"	box if the box started on an earlier page.
.ie !\\n(^b-1 \{\
\h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.el \}\
\h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.\}
.fi
.br
.nr ^b 0
..
'\"	# VS - start vertical sidebar
'\"	# ^Y = starting y location
'\"	# ^v = 1 (for troff;  for nroff this doesn't matter)
.de VS
.mk ^Y
.ie n 'mc \s12\(br\s0
.el .nr ^v 1u
..
'\"	# VE - end of vertical sidebar
.de VE
.ie n 'mc
.el \{\
.ev 2
.nf
.ti 0
.mk ^t
\h'|\\n(^lu+3n'\L'|\\n(^Yu-1v\(bv'\v'\\n(^tu+1v-\\n(^Yu'\h'-|\\n(^lu+3n'
.sp -1
.fi
.ev
.\}
.nr ^v 0
..
'\"	# Special macro to handle page bottom:  finish off current
'\"	# box/sidebar if in box/sidebar mode, then invoked standard
'\"	# page bottom macro.
.de ^B
.ev 2
'ti 0
'nf
.mk ^t
.if \\n(^b \{\
.\"	Draw three-sided box if this is the box's first page,
.\"	draw two sides but no top otherwise.
.ie !\\n(^b-1 \h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.el \h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.\}
.if \\n(^v \{\
.nr ^x \\n(^tu+1v-\\n(^Yu
\kx\h'-\\nxu'\h'|\\n(^lu+3n'\ky\L'-\\n(^xu'\v'\\n(^xu'\h'|0u'\c
.\}
.bp
'fi
.ev
.if \\n(^b \{\
.mk ^y
.nr ^b 2
.\}
.if \\n(^v \{\
.mk ^Y
.\}
..
'\"	# DS - begin display
.de DS
.RS
.nf
.sp
..
'\"	# DE - end display
.de DE
.fi
.RE
.sp
..
.HS clipboard tk 4.0
.BS
'\" Note:  do not modify the .SH NAME line immediately below!
.SH NAME
clipboard \- Manipulate Tk clipboard
.SH SYNOPSIS
\fBclipboard \fIoption\fR ?\fIarg arg ...\fR?
.BE

.SH DESCRIPTION
.PP
This command provides a Tcl interface to the Tk clipboard,
which stores data for later retrieval using the selection mechanism.
In order to copy data into the clipboard, \fBclipboard clear\fR must
be called, followed by a sequence of one or more calls to \fBclipboard
append\fR.  To ensure that the clipboard is updated atomically, all
appends should be completed before returning to the event loop.
.PP
The first argument to \fBclipboard\fR determines the format of the
rest of the arguments and the behavior of the command.  The following
forms are currently supported:
.PP
.TP
\fBclipboard clear\fR ?\fB\-displayof\fR \fIwindow\fR?
Claim ownership of the clipboard on \fIwindow\fR's display and remove
any previous contents.  \fIWindow\fR defaults to ``.''.  Returns an
empty string.
.TP
\fBclipboard append\fR ?\fB\-displayof\fR \fIwindow\fR? ?\fB\-format\fR \fIformat\fR? ?\fB\-type\fR \fItype\fR? \fIdata\fR
Appends \fIdata\fR to the clipboard on \fIwindow\fR's
display in the form given by \fItype\fR with the representation given
by \fIformat\fR.  All items appended to the clipboard with the same
\fItype\fR must have the same \fIformat\fR.
.RS
.PP
\fIType\fR specifies the form in which the selection is to be returned
(the desired ``target'' for conversion, in ICCCM terminology), and
should be an atom name such as STRING or FILE_NAME; see the
Inter-Client Communication Conventions Manual for complete details.
\fIType\fR defaults to STRING.
.PP
The \fIformat\fR argument specifies the representation that should be
used to transmit the selection to the requester (the second column of
Table 2 of the ICCCM), and defaults to STRING.  If \fIformat\fR is
STRING, the selection is transmitted as 8-bit ASCII characters.  If
\fIformat\fR is ATOM, then the \fIdata\fR is
divided into fields separated by white space; each field is converted
to its atom value, and the 32-bit atom value is transmitted instead of
the atom name.  For any other \fIformat\fR,  \fIdata\fR is divided
into fields separated by white space and each 
field is converted to a 32-bit integer; an array of integers is
transmitted to the selection requester.  Note that strings passed to
\fBclipboard append\fR are concatenated before conversion, so the
caller must take care to ensure appropriate spacing across string
boundaries.
.PP
The \fIformat\fR argument is needed only for compatibility with
clipboard requesters that don't use Tk.  If the Tk toolkit is being
used to retrieve the CLIPBOARD selection then the value is converted back to
a string at the requesting end, so \fIformat\fR is
irrelevant.

.SH KEYWORDS
clear, format, clipboard, append, selection, type

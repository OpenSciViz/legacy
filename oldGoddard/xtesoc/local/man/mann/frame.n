'\"
'\" Copyright (c) 1990-1994 The Regents of the University of California.
'\" Copyright (c) 1994 Sun Microsystems, Inc.
'\"
'\" See the file "license.terms" for information on usage and redistribution
'\" of this file, and for a DISCLAIMER OF ALL WARRANTIES.
'\" 
'\"
'\" @(#) frame.n 1.14 94/12/17 16:04:39
'/" 
'\" The definitions below are for supplemental macros used in Tcl/Tk
'\" manual entries.
'\"
'\" .HS name section [date [version]]
'\"	Replacement for .TH in other man pages.  See below for valid
'\"	section names.
'\"
'\" .AP type name in/out [indent]
'\"	Start paragraph describing an argument to a library procedure.
'\"	type is type of argument (int, etc.), in/out is either "in", "out",
'\"	or "in/out" to describe whether procedure reads or modifies arg,
'\"	and indent is equivalent to second arg of .IP (shouldn't ever be
'\"	needed;  use .AS below instead)
'\"
'\" .AS [type [name]]
'\"	Give maximum sizes of arguments for setting tab stops.  Type and
'\"	name are examples of largest possible arguments that will be passed
'\"	to .AP later.  If args are omitted, default tab stops are used.
'\"
'\" .BS
'\"	Start box enclosure.  From here until next .BE, everything will be
'\"	enclosed in one large box.
'\"
'\" .BE
'\"	End of box enclosure.
'\"
'\" .VS
'\"	Begin vertical sidebar, for use in marking newly-changed parts
'\"	of man pages.
'\"
'\" .VE
'\"	End of vertical sidebar.
'\"
'\" .DS
'\"	Begin an indented unfilled display.
'\"
'\" .DE
'\"	End of indented unfilled display.
'\"
'\" @(#) man.macros 1.1 94/08/09 13:07:19
.\"
'\"	# Heading for Tcl/Tk man pages
.de HS
.ds ^3 \\0
.if !"\\$3"" .ds ^3 \\$3
.if '\\$2'cmds'      .TH "\\$1" 1 "\\*(^3" "\\$4" "\\$5"
.if '\\$2'lib'       .TH "\\$1" 3 "\\*(^3" "\\$4" "\\$5"
.if '\\$2'ncmds'     .TH "\\$1" n "\\*(^3" "\\$4" "\\$5"
.if '\\$2'tcl'       .TH "\\$1" n "\\*(^3"  Tcl   "Tcl Built-In Commands"
.if '\\$2'tk'        .TH "\\$1" n "\\*(^3"  Tk    "Tk Commands"
.if '\\$2'tclc'      .TH "\\$1" 3 "\\*(^3"  Tcl   "Tcl Library Procedures"
.if '\\$2'tkc'       .TH "\\$1" 3 "\\*(^3"  Tk    "Tk Library Procedures"
.if '\\$2'tclcmds'   .TH "\\$1" 1 "\\*(^3"  Tk    "Tcl Applications"
.if '\\$2'tkcmds'    .TH "\\$1" 1 "\\*(^3"  Tk    "Tk Applications"
.if t .wh -1.3i ^B
.nr ^l \\n(.l
.ad b
..
'\"	# Start an argument description
.de AP
.ie !"\\$4"" .TP \\$4
.el \{\
.   ie !"\\$2"" .TP \\n()Cu
.   el          .TP 15
.\}
.ie !"\\$3"" \{\
.ta \\n()Au \\n()Bu
\&\\$1	\\fI\\$2\\fP	(\\$3)
.\".b
.\}
.el \{\
.br
.ie !"\\$2"" \{\
\&\\$1	\\fI\\$2\\fP
.\}
.el \{\
\&\\fI\\$1\\fP
.\}
.\}
..
'\"	# define tabbing values for .AP
.de AS
.nr )A 10n
.if !"\\$1"" .nr )A \\w'\\$1'u+3n
.nr )B \\n()Au+15n
.\"
.if !"\\$2"" .nr )B \\w'\\$2'u+\\n()Au+3n
.nr )C \\n()Bu+\\w'(in/out)'u+2n
..
'\"	# BS - start boxed text
'\"	# ^y = starting y location
'\"	# ^b = 1
.de BS
.br
.mk ^y
.nr ^b 1u
.if n .nf
.if n .ti 0
.if n \l'\\n(.lu\(ul'
.if n .fi
..
'\"	# BE - end boxed text (draw box now)
.de BE
.nf
.ti 0
.mk ^t
.ie n \l'\\n(^lu\(ul'
.el \{\
.\"	Draw four-sided box normally, but don't draw top of
.\"	box if the box started on an earlier page.
.ie !\\n(^b-1 \{\
\h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.el \}\
\h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.\}
.fi
.br
.nr ^b 0
..
'\"	# VS - start vertical sidebar
'\"	# ^Y = starting y location
'\"	# ^v = 1 (for troff;  for nroff this doesn't matter)
.de VS
.mk ^Y
.ie n 'mc \s12\(br\s0
.el .nr ^v 1u
..
'\"	# VE - end of vertical sidebar
.de VE
.ie n 'mc
.el \{\
.ev 2
.nf
.ti 0
.mk ^t
\h'|\\n(^lu+3n'\L'|\\n(^Yu-1v\(bv'\v'\\n(^tu+1v-\\n(^Yu'\h'-|\\n(^lu+3n'
.sp -1
.fi
.ev
.\}
.nr ^v 0
..
'\"	# Special macro to handle page bottom:  finish off current
'\"	# box/sidebar if in box/sidebar mode, then invoked standard
'\"	# page bottom macro.
.de ^B
.ev 2
'ti 0
'nf
.mk ^t
.if \\n(^b \{\
.\"	Draw three-sided box if this is the box's first page,
.\"	draw two sides but no top otherwise.
.ie !\\n(^b-1 \h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.el \h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.\}
.if \\n(^v \{\
.nr ^x \\n(^tu+1v-\\n(^Yu
\kx\h'-\\nxu'\h'|\\n(^lu+3n'\ky\L'-\\n(^xu'\v'\\n(^xu'\h'|0u'\c
.\}
.bp
'fi
.ev
.if \\n(^b \{\
.mk ^y
.nr ^b 2
.\}
.if \\n(^v \{\
.mk ^Y
.\}
..
'\"	# DS - begin display
.de DS
.RS
.nf
.sp
..
'\"	# DE - end display
.de DE
.fi
.RE
.sp
..
.HS frame tk 4.0
.BS
'\" Note:  do not modify the .SH NAME line immediately below!
.SH NAME
frame \- Create and manipulate frame widgets
.SH SYNOPSIS
\fBframe\fI \fIpathName ?\fIoptions\fR?
.SH "STANDARD OPTIONS"
.LP
.nf
.ta 4c 8c 12c
.VS
\fBborderWidth\fR	\fBhighlightColor\fR	\fBrelief\fR
.VE
\fBcursor\fR	\fBhighlightThickness\fR
.fi
.LP
See the ``options'' manual entry for details on the standard options.
.SH "WIDGET-SPECIFIC OPTIONS"
.LP
.nf
.VS
Name:	\fBbackground\fR
Class:	\fBBackground\fR
Command-Line Switch:	\fB\-background\fR
.fi
.IP
This option is the same as the standard \fBbackground\fR option
except that its value may also be specified as an empty string.
In this case, the widget will display no background or border, and
no colors will be consumed from its colormap for its background
and border.
.LP
.nf
Name:	\fBclass\fR
Class:	\fBClass\fR
Command-Line Switch:	\fB\-class\fR
.fi
.IP
Specifies a class for the window.
This class will be used when querying the option database for
the window's other options, and it will also be used later for
other purposes such as bindings.
The \fBclass\fR option may not be changed with the \fBconfigure\fR
widget command.
.LP
.nf
Name:	\fBcolormap\fR
Class:	\fBColormap\fR
Command-Line Switch:	\fB\-colormap\fR
.fi
.IP
Specifies a colormap to use for the window.
The value may be either \fBnew\fR, in which case a new colormap is
created for the window and its children, or the name of another
window (which must be on the same screen and have the same visual
as \fIpathName\fR), in which case the new window will use the colormap
from the specified window.
If the \fBcolormap\fR option is not specified, the new window
uses the same colormap as its parent.
This option may not be changed with the \fBconfigure\fR
widget command.
.VE
.LP
.nf
Name:	\fBheight\fR
Class:	\fBHeight\fR
Command-Line Switch:	\fB\-height\fR
.fi
.IP
Specifies the desired height for the window in any of the forms
acceptable to \fBTk_GetPixels\fR.
If this option is less than or equal to zero then the window will
not request any size at all.
.LP
.nf
.VS
Name:	\fBvisual\fR
Class:	\fBVisual\fR
Command-Line Switch:	\fB\-visual\fR
.fi
.IP
Specifies visual information for the new window in any of the
forms accepted by \fBTk_GetVisual\fR.
If this option is not specified, the new window will use the same
visual as its parent.
The \fBvisual\fR option may not be modified with the \fBconfigure\fR
widget command.
.VE
.LP
.nf
Name:	\fBwidth\fR
Class:	\fBWidth\fR
Command-Line Switch:	\fB\-width\fR
.fi
.IP
Specifies the desired width for the window in any of the forms
acceptable to \fBTk_GetPixels\fR.
If this option is less than or equal to zero then the window will
not request any size at all.
.BE

.SH DESCRIPTION
.PP
The \fBframe\fR command creates a new window (given by the
\fIpathName\fR argument) and makes it into a frame widget.
Additional
options, described above, may be specified on the command line
or in the option database
to configure aspects of the frame such as its background color
and relief.  The \fBframe\fR command returns the
path name of the new window.
.PP
A frame is a simple widget.  Its primary purpose is to act as a
spacer or container for complex window layouts.  The only features
of a frame are its background color and an optional 3-D border to make the
frame appear raised or sunken.

.SH "WIDGET COMMAND"
.PP
The \fBframe\fR command creates a new Tcl command whose
name is the same as the path name of the frame's window.  This
command may be used to invoke various
operations on the widget.  It has the following general form:
.DS C
\fIpathName option \fR?\fIarg arg ...\fR?
.DE
\fIPathName\fR is the name of the command, which is the same as
the frame widget's path name.  \fIOption\fR and the \fIarg\fRs
determine the exact behavior of the command.  The following
commands are possible for frame widgets:
.TP
\fIpathName \fBcget\fR \fIoption\fR
.VS
Returns the current value of the configuration option given
by \fIoption\fR.
\fIOption\fR may have any of the values accepted by the \fBframe\fR
command.
.VE
.TP
\fIpathName \fBconfigure\fR ?\fIoption\fR? \fI?value option value ...\fR?
Query or modify the configuration options of the widget.
If no \fIoption\fR is specified, returns a list describing all of
the available options for \fIpathName\fR (see \fBTk_ConfigureInfo\fR for
information on the format of this list).  If \fIoption\fR is specified
with no \fIvalue\fR, then the command returns a list describing the
one named option (this list will be identical to the corresponding
sublist of the value returned if no \fIoption\fR is specified).  If
one or more \fIoption\-value\fR pairs are specified, then the command
modifies the given widget option(s) to have the given value(s);  in
this case the command returns an empty string.
\fIOption\fR may have any of the values accepted by the \fBframe\fR
command.

.SH BINDINGS
.PP
When a new frame is created, it has no default event bindings:
frames are not intended to be interactive.

.SH KEYWORDS
frame, widget

'\"
'\" Copyright (c) 1990-1994 The Regents of the University of California.
'\" Copyright (c) 1994 Sun Microsystems, Inc.
'\"
'\" See the file "license.terms" for information on usage and redistribution
'\" of this file, and for a DISCLAIMER OF ALL WARRANTIES.
'\" 
'\" @(#) focus.n 1.13 94/12/17 16:04:37
'/" 
'\" The definitions below are for supplemental macros used in Tcl/Tk
'\" manual entries.
'\"
'\" .HS name section [date [version]]
'\"	Replacement for .TH in other man pages.  See below for valid
'\"	section names.
'\"
'\" .AP type name in/out [indent]
'\"	Start paragraph describing an argument to a library procedure.
'\"	type is type of argument (int, etc.), in/out is either "in", "out",
'\"	or "in/out" to describe whether procedure reads or modifies arg,
'\"	and indent is equivalent to second arg of .IP (shouldn't ever be
'\"	needed;  use .AS below instead)
'\"
'\" .AS [type [name]]
'\"	Give maximum sizes of arguments for setting tab stops.  Type and
'\"	name are examples of largest possible arguments that will be passed
'\"	to .AP later.  If args are omitted, default tab stops are used.
'\"
'\" .BS
'\"	Start box enclosure.  From here until next .BE, everything will be
'\"	enclosed in one large box.
'\"
'\" .BE
'\"	End of box enclosure.
'\"
'\" .VS
'\"	Begin vertical sidebar, for use in marking newly-changed parts
'\"	of man pages.
'\"
'\" .VE
'\"	End of vertical sidebar.
'\"
'\" .DS
'\"	Begin an indented unfilled display.
'\"
'\" .DE
'\"	End of indented unfilled display.
'\"
'\" @(#) man.macros 1.1 94/08/09 13:07:19
.\"
'\"	# Heading for Tcl/Tk man pages
.de HS
.ds ^3 \\0
.if !"\\$3"" .ds ^3 \\$3
.if '\\$2'cmds'      .TH "\\$1" 1 "\\*(^3" "\\$4" "\\$5"
.if '\\$2'lib'       .TH "\\$1" 3 "\\*(^3" "\\$4" "\\$5"
.if '\\$2'ncmds'     .TH "\\$1" n "\\*(^3" "\\$4" "\\$5"
.if '\\$2'tcl'       .TH "\\$1" n "\\*(^3"  Tcl   "Tcl Built-In Commands"
.if '\\$2'tk'        .TH "\\$1" n "\\*(^3"  Tk    "Tk Commands"
.if '\\$2'tclc'      .TH "\\$1" 3 "\\*(^3"  Tcl   "Tcl Library Procedures"
.if '\\$2'tkc'       .TH "\\$1" 3 "\\*(^3"  Tk    "Tk Library Procedures"
.if '\\$2'tclcmds'   .TH "\\$1" 1 "\\*(^3"  Tk    "Tcl Applications"
.if '\\$2'tkcmds'    .TH "\\$1" 1 "\\*(^3"  Tk    "Tk Applications"
.if t .wh -1.3i ^B
.nr ^l \\n(.l
.ad b
..
'\"	# Start an argument description
.de AP
.ie !"\\$4"" .TP \\$4
.el \{\
.   ie !"\\$2"" .TP \\n()Cu
.   el          .TP 15
.\}
.ie !"\\$3"" \{\
.ta \\n()Au \\n()Bu
\&\\$1	\\fI\\$2\\fP	(\\$3)
.\".b
.\}
.el \{\
.br
.ie !"\\$2"" \{\
\&\\$1	\\fI\\$2\\fP
.\}
.el \{\
\&\\fI\\$1\\fP
.\}
.\}
..
'\"	# define tabbing values for .AP
.de AS
.nr )A 10n
.if !"\\$1"" .nr )A \\w'\\$1'u+3n
.nr )B \\n()Au+15n
.\"
.if !"\\$2"" .nr )B \\w'\\$2'u+\\n()Au+3n
.nr )C \\n()Bu+\\w'(in/out)'u+2n
..
'\"	# BS - start boxed text
'\"	# ^y = starting y location
'\"	# ^b = 1
.de BS
.br
.mk ^y
.nr ^b 1u
.if n .nf
.if n .ti 0
.if n \l'\\n(.lu\(ul'
.if n .fi
..
'\"	# BE - end boxed text (draw box now)
.de BE
.nf
.ti 0
.mk ^t
.ie n \l'\\n(^lu\(ul'
.el \{\
.\"	Draw four-sided box normally, but don't draw top of
.\"	box if the box started on an earlier page.
.ie !\\n(^b-1 \{\
\h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.el \}\
\h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.\}
.fi
.br
.nr ^b 0
..
'\"	# VS - start vertical sidebar
'\"	# ^Y = starting y location
'\"	# ^v = 1 (for troff;  for nroff this doesn't matter)
.de VS
.mk ^Y
.ie n 'mc \s12\(br\s0
.el .nr ^v 1u
..
'\"	# VE - end of vertical sidebar
.de VE
.ie n 'mc
.el \{\
.ev 2
.nf
.ti 0
.mk ^t
\h'|\\n(^lu+3n'\L'|\\n(^Yu-1v\(bv'\v'\\n(^tu+1v-\\n(^Yu'\h'-|\\n(^lu+3n'
.sp -1
.fi
.ev
.\}
.nr ^v 0
..
'\"	# Special macro to handle page bottom:  finish off current
'\"	# box/sidebar if in box/sidebar mode, then invoked standard
'\"	# page bottom macro.
.de ^B
.ev 2
'ti 0
'nf
.mk ^t
.if \\n(^b \{\
.\"	Draw three-sided box if this is the box's first page,
.\"	draw two sides but no top otherwise.
.ie !\\n(^b-1 \h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.el \h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.\}
.if \\n(^v \{\
.nr ^x \\n(^tu+1v-\\n(^Yu
\kx\h'-\\nxu'\h'|\\n(^lu+3n'\ky\L'-\\n(^xu'\v'\\n(^xu'\h'|0u'\c
.\}
.bp
'fi
.ev
.if \\n(^b \{\
.mk ^y
.nr ^b 2
.\}
.if \\n(^v \{\
.mk ^Y
.\}
..
'\"	# DS - begin display
.de DS
.RS
.nf
.sp
..
'\"	# DE - end display
.de DE
.fi
.RE
.sp
..
.HS focus tk 4.0
.BS
'\" Note:  do not modify the .SH NAME line immediately below!
.SH NAME
focus \- Manage the input focus
.SH SYNOPSIS
\fBfocus\fR
.br
\fBfocus \fIwindow\fR
.br
\fBfocus \fIoption\fR ?\fIarg arg ...\fR?
.BE

.SH DESCRIPTION
.PP
The \fBfocus\fR command is used to manage the Tk input focus.
.VS
At any given time, one window on each display is designated as
the \fIfocus window\fR;  any key press or key release events for the
display are sent to that window.
It is up to the window manager to redirect the focus among the
top-level windows of a display.  For example, some window managers
automatically set the input focus to a top-level window whenever
the mouse enters it;  others redirect the input focus only when
the user clicks on a window.
Usually the window manager will set the focus
only to top-level windows, leaving it up to the application to
redirect the focus among the children of the top-level.
.PP
Tk remembers one focus window for each top-level (the most recent
descendant of that top-level to receive the focus);  when the window
manager gives the focus
to a top-level, Tk automatically redirects it to the remembered
window.  Within a top-level Tk uses an \fIexplicit\fR focus model
by default.  Moving the mouse within a top-level does not normally
change the focus;  the focus changes only when a widget
decides explicitly to claim the focus (e.g., because of a button
click), or when the user types a key such as Tab that moves the
focus.
.PP
The Tcl procedure \fBtk_focusFollowsMouse\fR may be invoked to
create an \fIimplicit\fR focus model:  it reconfigures Tk so that
the focus is set to a window whenever the mouse enters it.
The Tcl procedures \fBtk_focusNext\fR and \fBtk_focusPrev\fR
implement a focus order among the windows of a top-level;  they
are used in the default bindings for Tab and Shift-Tab, among other
things.
.PP
The \fBfocus\fR command can take any of the following forms:
.TP
\fBfocus\fR
Returns the path name of the focus window on the display containing
the application's main window,  or an empty string if no window in
this application has the focus on that display.   Note:  it is
better to specify the display explicitly using \fB\-displayof\fR
(see below) so that the code will work in applications using multiple
displays.
.TP
\fBfocus \fIwindow\fR
Sets the input focus for \fIwindow\fR's display to \fIwindow\fR and
returns an empty string.  If \fIwindow\fR isn't viewable at the
time this command is invoked, X will refuse to actually set the focus;
nonetheless, \fIwindow\fR will be marked by Tk as the focus window
for its display.
.TP
\fBfocus \fB\-displayof\fR \fIwindow\fR
Returns the name of the focus window on the display containing \fIwindow\fR.
If the focus window for \fIwindow\fR's display isn't in this
application, the return value is an empty string.
.TP
\fBfocus \fB\-lastfor\fR \fIwindow\fR
Returns the name of the most recent window to have the input focus
among all the windows in the same top-level as \fIwindow\fR.
If no window in that top-level has ever had the input focus, or
if the most recent focus window has been deleted, then the name
of the top-level is returned.  The return value is the window that
will receive the input focus the next time the window manager gives
the focus to the top-level.

.SH "BUGS"
.PP
When Tk redirects the input focus, either because of a \fBfocus\fR
command or because the window manager set the focus to a top-level
and Tk is redirecting it to an internal window in the top-level,
there may already be keyboard events queued.  If this happens,
the FocusIn and FocusOut events for the focus change will not be
received until after the keyboard events;  however, the keyboard
events will be directed to the new focus window.  This means that
a window can receive keyboard events before receiving a FocusIn
event.
.VE

.SH KEYWORDS
events, focus, keyboard, top-level, window manager

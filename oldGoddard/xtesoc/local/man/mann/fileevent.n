'\"
'\" Copyright (c) 1994 The Regents of the University of California.
'\" Copyright (c) 1994 Sun Microsystems, Inc.
'\"
'\" See the file "license.terms" for information on usage and redistribution
'\" of this file, and for a DISCLAIMER OF ALL WARRANTIES.
'\" 
'\" @(#) fileevent.n 1.4 94/12/17 16:04:36
'\" 
'\" The definitions below are for supplemental macros used in Tcl/Tk
'\" manual entries.
'\"
'\" .HS name section [date [version]]
'\"	Replacement for .TH in other man pages.  See below for valid
'\"	section names.
'\"
'\" .AP type name in/out [indent]
'\"	Start paragraph describing an argument to a library procedure.
'\"	type is type of argument (int, etc.), in/out is either "in", "out",
'\"	or "in/out" to describe whether procedure reads or modifies arg,
'\"	and indent is equivalent to second arg of .IP (shouldn't ever be
'\"	needed;  use .AS below instead)
'\"
'\" .AS [type [name]]
'\"	Give maximum sizes of arguments for setting tab stops.  Type and
'\"	name are examples of largest possible arguments that will be passed
'\"	to .AP later.  If args are omitted, default tab stops are used.
'\"
'\" .BS
'\"	Start box enclosure.  From here until next .BE, everything will be
'\"	enclosed in one large box.
'\"
'\" .BE
'\"	End of box enclosure.
'\"
'\" .VS
'\"	Begin vertical sidebar, for use in marking newly-changed parts
'\"	of man pages.
'\"
'\" .VE
'\"	End of vertical sidebar.
'\"
'\" .DS
'\"	Begin an indented unfilled display.
'\"
'\" .DE
'\"	End of indented unfilled display.
'\"
'\" @(#) man.macros 1.1 94/08/09 13:07:19
.\"
'\"	# Heading for Tcl/Tk man pages
.de HS
.ds ^3 \\0
.if !"\\$3"" .ds ^3 \\$3
.if '\\$2'cmds'      .TH "\\$1" 1 "\\*(^3" "\\$4" "\\$5"
.if '\\$2'lib'       .TH "\\$1" 3 "\\*(^3" "\\$4" "\\$5"
.if '\\$2'ncmds'     .TH "\\$1" n "\\*(^3" "\\$4" "\\$5"
.if '\\$2'tcl'       .TH "\\$1" n "\\*(^3"  Tcl   "Tcl Built-In Commands"
.if '\\$2'tk'        .TH "\\$1" n "\\*(^3"  Tk    "Tk Commands"
.if '\\$2'tclc'      .TH "\\$1" 3 "\\*(^3"  Tcl   "Tcl Library Procedures"
.if '\\$2'tkc'       .TH "\\$1" 3 "\\*(^3"  Tk    "Tk Library Procedures"
.if '\\$2'tclcmds'   .TH "\\$1" 1 "\\*(^3"  Tk    "Tcl Applications"
.if '\\$2'tkcmds'    .TH "\\$1" 1 "\\*(^3"  Tk    "Tk Applications"
.if t .wh -1.3i ^B
.nr ^l \\n(.l
.ad b
..
'\"	# Start an argument description
.de AP
.ie !"\\$4"" .TP \\$4
.el \{\
.   ie !"\\$2"" .TP \\n()Cu
.   el          .TP 15
.\}
.ie !"\\$3"" \{\
.ta \\n()Au \\n()Bu
\&\\$1	\\fI\\$2\\fP	(\\$3)
.\".b
.\}
.el \{\
.br
.ie !"\\$2"" \{\
\&\\$1	\\fI\\$2\\fP
.\}
.el \{\
\&\\fI\\$1\\fP
.\}
.\}
..
'\"	# define tabbing values for .AP
.de AS
.nr )A 10n
.if !"\\$1"" .nr )A \\w'\\$1'u+3n
.nr )B \\n()Au+15n
.\"
.if !"\\$2"" .nr )B \\w'\\$2'u+\\n()Au+3n
.nr )C \\n()Bu+\\w'(in/out)'u+2n
..
'\"	# BS - start boxed text
'\"	# ^y = starting y location
'\"	# ^b = 1
.de BS
.br
.mk ^y
.nr ^b 1u
.if n .nf
.if n .ti 0
.if n \l'\\n(.lu\(ul'
.if n .fi
..
'\"	# BE - end boxed text (draw box now)
.de BE
.nf
.ti 0
.mk ^t
.ie n \l'\\n(^lu\(ul'
.el \{\
.\"	Draw four-sided box normally, but don't draw top of
.\"	box if the box started on an earlier page.
.ie !\\n(^b-1 \{\
\h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.el \}\
\h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.\}
.fi
.br
.nr ^b 0
..
'\"	# VS - start vertical sidebar
'\"	# ^Y = starting y location
'\"	# ^v = 1 (for troff;  for nroff this doesn't matter)
.de VS
.mk ^Y
.ie n 'mc \s12\(br\s0
.el .nr ^v 1u
..
'\"	# VE - end of vertical sidebar
.de VE
.ie n 'mc
.el \{\
.ev 2
.nf
.ti 0
.mk ^t
\h'|\\n(^lu+3n'\L'|\\n(^Yu-1v\(bv'\v'\\n(^tu+1v-\\n(^Yu'\h'-|\\n(^lu+3n'
.sp -1
.fi
.ev
.\}
.nr ^v 0
..
'\"	# Special macro to handle page bottom:  finish off current
'\"	# box/sidebar if in box/sidebar mode, then invoked standard
'\"	# page bottom macro.
.de ^B
.ev 2
'ti 0
'nf
.mk ^t
.if \\n(^b \{\
.\"	Draw three-sided box if this is the box's first page,
.\"	draw two sides but no top otherwise.
.ie !\\n(^b-1 \h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.el \h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.\}
.if \\n(^v \{\
.nr ^x \\n(^tu+1v-\\n(^Yu
\kx\h'-\\nxu'\h'|\\n(^lu+3n'\ky\L'-\\n(^xu'\v'\\n(^xu'\h'|0u'\c
.\}
.bp
'fi
.ev
.if \\n(^b \{\
.mk ^y
.nr ^b 2
.\}
.if \\n(^v \{\
.mk ^Y
.\}
..
'\"	# DS - begin display
.de DS
.RS
.nf
.sp
..
'\"	# DE - end display
.de DE
.fi
.RE
.sp
..
.HS fileevent tk 4.0
.BS
'\" Note:  do not modify the .SH NAME line immediately below!
.SH NAME
fileevent \- Execute a script when a file becomes readable or writable
.SH SYNOPSIS
\fBfileevent \fIfileId \fBreadable \fR?\fIscript\fR?
.br
\fBfileevent \fIfileId \fBwritable \fR?\fIscript\fR?
.BE

.SH DESCRIPTION
.PP
This command is used to create \fIfile event handlers\fR.
A file event handler is a binding between a file and a script,
such that the script is evaluated whenever the file becomes
readable or writable.
File event handlers are most commonly used to allow data to be
received from a child process on an event-driven basis, so that
the receiver can continue to interact with the user while
waiting for the data to arrive.
If an application invokes \fBgets\fR or \fBread\fR when there
is no input data available, the process will block;  until the
input data arrives, it will not be able to service other events,
so it will appear to the user to ``freeze up''.
With \fBfileevent\fR, the process can tell when data is present
and only invoke \fBgets\fR or \fBread\fR when they won't block.
.PP
The \fIfileId\fR argument to \fBfileevent\fR refers to an open file;
it must be \fBstdin\fR, \fBstdout\fR, \fBstderr\fR, or the return value
from some previous \fBopen\fR command.
If the \fIscript\fR argument is specified, then \fBfileevent\fR
creates a new event handler:  \fIscript\fR will be evaluated
whenever the file becomes readable or writable (depending on the
second argument to \fBfileevent\fR).
In this case \fBfileevent\fR returns an empty string.
The \fBreadable\fR and \fBwritable\fR event handlers for a file
are independent, and may be created and deleted separately.
However, there may be at most one \fBreadable\fR and one \fBwritable\fR
handler for a file at a given time.
If \fBfileevent\fR is called when the specified handler already
exists, the new script replaces the old one.
.PP
If the \fIscript\fR argument is not specified, \fBfileevent\fR
returns the current script for \fIfileId\fR, or an empty string
if there is none.
If the \fIscript\fR argument is specified as an empty string
then the event handler is deleted, so that no script will be invoked.
A file event handler is also deleted automatically whenever
its file is closed or its interpreter is deleted.
.PP
A file is considered to be readable whenever the \fBgets\fR
and \fBread\fR commands can return without blocking.
A file is also considered to be readable if an end-of-file or
error condition is present.
It is important for \fIscript\fR to check for these conditions
and handle them appropriately;  for example, if there is no special
check for end-of-file, an infinite loop may occur where \fIscript\fR
reads no data, returns, and is immediately invoked again.
.PP
When using \fBfileevent\fR for event-driven I/O, it's important
to read the file in the same units that are written
from the other end.
For example, suppose that you are using \fBfileevent\fR to
read data generated by a child process.
If the child process is writing whole lines, then you should use
\fBgets\fR to read those lines.
If the child generates one line at a time then you shouldn't
make more than a single call to \fBgets\fR in \fIscript\fR: the first call
will consume all the available data, so the second call may block.
You can also use \fBread\fR to read the child's data, but only
if you know how many bytes the child is writing at a time:  if
you try to read more bytes than the child has written, the
\fBread\fR call will block.
.PP
A file is considered to be writable if at least one byte of data
can be written to the file without blocking, or if an error condition
is present.
Write handlers are probably not very useful without additional command
support.
The \fBputs\fR command is dangerous since it write more than
one byte at a time and may thus block.
What is really needed is a new non-blocking form of write that
saves any data that couldn't be written to the file.
.PP
The script for a file event is executed at global level (outside the
context of any Tcl procedure).
If an error occurs while executing the script then the
\fBtkerror\fR mechanism is used to report the error.
In addition, the file event handler is deleted if it ever returns
an error;  this is done in order to prevent infinite loops due to
buggy handlers.

.SH CREDITS
.PP
\fBfileevent\fR is based on the \fBaddinput\fR command created
by Mark Diekhans.

.SH "SEE ALSO"
tkerror

.SH KEYWORDS
asynchronous I/O, event handler, file, readable, script, writable

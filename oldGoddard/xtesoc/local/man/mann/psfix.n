.TH PSFIX 1l "March 1992"
.SH NAME
psfix \- 
The general fixer for single page images. 
.SH SYNOPSIS
\fIpsfix\fP [options] [\fIFilename\fP]
.SH DESCRIPTION
.B psfix
is a filter to generate/modify conformant single page postscript documents
from other postscript documents. It enables the user to arbitarily scale
and rotate the original image. The resulting image will be alligned to
a specified bounding box (A4 by default).

.SH OPTIONS

.B \-v Print to standard error lots of interesting information on
progress of program.
.TP
.B \-o \fP filename
Define the filename to
which the output is to be directed, defaults to the standard output.
.TP
.B \-sp,\-nsp
Disable the showpage operator before including the image document,
and in the latter add a normal showpage after the end of the document.
.TP
.B \-ibbox\fP "llx lly urx ury | page | dsc | a4"
The defines the bounding box for the images in the document. If given
as `page' takes the bounding box of the printable region, if dsc it takes
the bounding box from the file comments, if a4 it is set to the size of an
a4 page. If the image bbox is not defined in the options list it
is derived from the comments section of the document or taken to be `page'
by default.
.TP
.B \-fbbox \fP "llx lly urx ury | page |a4"
Defines the bounding box within which the image will be alligned.
If given as `page' this is done dynamically when the image is process
on the printer based on the current clippath. This feature may be used
to conveninetly scale-up pictures for A3 printers.
.TP
.B \-usefbbox
Insert the /fIfbbox/fP as the BoundingBox in the DSC header for the image.
This can be used to incorporate a margin around an image.
.TP
.B \-margin\fIMarginXY-fP
Leave a minimum margin of \fIMarginXY\fPn (in points) between the \fIfbbox\fP
and the \fIibbox\fP (most relevant when using \fIfbbox\fP = page).
.TP
.B \-margin \fIMarginX MarginY\fP
Leave a minimum margin of \fIMarginX\fP on left and right sides, and
a margin of \fIMarginY\fP on top and bottom.
.TP
.B \-r \fP angle
Rotate the document counter clockwise by /fIangle/fP degrees.
.TP
.B \-s\fIScaleXY\fP
Scale the image in both the x and y direction by a factor of \fIScaleXY\fP.
(Note that there must be no space between the argument and the scale factor).
.TP
.B \-s \fIScaleX\fP \fIScaleY\fP
Scale the image by a factor of \fIScaleX\fP in the x direction
and \fIScaleY\fP in the y direction.
.B \-maxpect
Scale the image to its maximum size within the bounds of the \fIfbbox\fP
while maintaining its aspect ratio.
.TP
.B \-maxxy
Scale the image to its maximum x and y dimension within the bounds of the
\fIfbbox\fP. Note that this will probably change the images original
aspect ratio.
.B \-maxx,\-maxy
Scale the image to its maximum size in the x/y direction.
.TP
.B \-centerx,\-left,\-right
Allign the image on the center/left/right of the x-axis of the \fIfbbox\fP.
.TP
.B \-centery,\-top,\-bottom
Allign the image on the center/left/right of the x-axis of the \fIfbbox\fP.
.TP
.B \-select \fP [ordinal | label] pageN
Select the page from the document, by default this is based
on the absolute numbering (ordinal), but it may also be done
according to the page labels is which case `label' must be specified
before page reference.
.TP \-border \fIBorderName\fP
Add one of the borders from the psmulti library.
.SH EXAMPLES
.TP
1. 
.B "eg% psfix -r -90 myfile.ps | lpr -Pcw"
.LP
.RS
.RS
Print out a postscript document myfile.ps in landscape orientation.
.RE
.RE
.TP 
2.
.B "eg% psfix -fbbox page -maxpect | lpr"
.LP
.RS
.RS
Print the image as large as possible on the default printer while preserving
the aspect ratio.
.RE
.RE
.TP
3.
.B "eg% psfix -nsp -fbbox 0 0 200 400 image.ps >! image.eps"
.LP
.RS
.RS
Produce an encapsulated form of image.ps that fits within the stated
bounding box.

.SH DIAGNOSTICS
The diagnostics are supposed to be self-explanatory.

.SH AUTHOR
.B psfix
was written and developed by D.Murray.Laing using utilities developed
originally for the psmulti package.
.RS
.nf
Department of Chemical Engineering, 
Edinburgh University,
The Kings Buildings, 
Mayfield Road, 
Edinburgh. 
EH9 3JL
Scotland. UK.
.IP
Email : murray@uk.ac.ed.chemeng
.IP
.SH BUGS
Only those intended to be there !




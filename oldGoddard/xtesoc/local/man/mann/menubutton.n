'\"
'\" Copyright (c) 1990-1994 The Regents of the University of California.
'\" Copyright (c) 1994 Sun Microsystems, Inc.
'\"
'\" See the file "license.terms" for information on usage and redistribution
'\" of this file, and for a DISCLAIMER OF ALL WARRANTIES.
'\" 
'\" @(#) menubutton.n 1.20 94/12/17 16:04:48
'/" 
'\" The definitions below are for supplemental macros used in Tcl/Tk
'\" manual entries.
'\"
'\" .HS name section [date [version]]
'\"	Replacement for .TH in other man pages.  See below for valid
'\"	section names.
'\"
'\" .AP type name in/out [indent]
'\"	Start paragraph describing an argument to a library procedure.
'\"	type is type of argument (int, etc.), in/out is either "in", "out",
'\"	or "in/out" to describe whether procedure reads or modifies arg,
'\"	and indent is equivalent to second arg of .IP (shouldn't ever be
'\"	needed;  use .AS below instead)
'\"
'\" .AS [type [name]]
'\"	Give maximum sizes of arguments for setting tab stops.  Type and
'\"	name are examples of largest possible arguments that will be passed
'\"	to .AP later.  If args are omitted, default tab stops are used.
'\"
'\" .BS
'\"	Start box enclosure.  From here until next .BE, everything will be
'\"	enclosed in one large box.
'\"
'\" .BE
'\"	End of box enclosure.
'\"
'\" .VS
'\"	Begin vertical sidebar, for use in marking newly-changed parts
'\"	of man pages.
'\"
'\" .VE
'\"	End of vertical sidebar.
'\"
'\" .DS
'\"	Begin an indented unfilled display.
'\"
'\" .DE
'\"	End of indented unfilled display.
'\"
'\" @(#) man.macros 1.1 94/08/09 13:07:19
.\"
'\"	# Heading for Tcl/Tk man pages
.de HS
.ds ^3 \\0
.if !"\\$3"" .ds ^3 \\$3
.if '\\$2'cmds'      .TH "\\$1" 1 "\\*(^3" "\\$4" "\\$5"
.if '\\$2'lib'       .TH "\\$1" 3 "\\*(^3" "\\$4" "\\$5"
.if '\\$2'ncmds'     .TH "\\$1" n "\\*(^3" "\\$4" "\\$5"
.if '\\$2'tcl'       .TH "\\$1" n "\\*(^3"  Tcl   "Tcl Built-In Commands"
.if '\\$2'tk'        .TH "\\$1" n "\\*(^3"  Tk    "Tk Commands"
.if '\\$2'tclc'      .TH "\\$1" 3 "\\*(^3"  Tcl   "Tcl Library Procedures"
.if '\\$2'tkc'       .TH "\\$1" 3 "\\*(^3"  Tk    "Tk Library Procedures"
.if '\\$2'tclcmds'   .TH "\\$1" 1 "\\*(^3"  Tk    "Tcl Applications"
.if '\\$2'tkcmds'    .TH "\\$1" 1 "\\*(^3"  Tk    "Tk Applications"
.if t .wh -1.3i ^B
.nr ^l \\n(.l
.ad b
..
'\"	# Start an argument description
.de AP
.ie !"\\$4"" .TP \\$4
.el \{\
.   ie !"\\$2"" .TP \\n()Cu
.   el          .TP 15
.\}
.ie !"\\$3"" \{\
.ta \\n()Au \\n()Bu
\&\\$1	\\fI\\$2\\fP	(\\$3)
.\".b
.\}
.el \{\
.br
.ie !"\\$2"" \{\
\&\\$1	\\fI\\$2\\fP
.\}
.el \{\
\&\\fI\\$1\\fP
.\}
.\}
..
'\"	# define tabbing values for .AP
.de AS
.nr )A 10n
.if !"\\$1"" .nr )A \\w'\\$1'u+3n
.nr )B \\n()Au+15n
.\"
.if !"\\$2"" .nr )B \\w'\\$2'u+\\n()Au+3n
.nr )C \\n()Bu+\\w'(in/out)'u+2n
..
'\"	# BS - start boxed text
'\"	# ^y = starting y location
'\"	# ^b = 1
.de BS
.br
.mk ^y
.nr ^b 1u
.if n .nf
.if n .ti 0
.if n \l'\\n(.lu\(ul'
.if n .fi
..
'\"	# BE - end boxed text (draw box now)
.de BE
.nf
.ti 0
.mk ^t
.ie n \l'\\n(^lu\(ul'
.el \{\
.\"	Draw four-sided box normally, but don't draw top of
.\"	box if the box started on an earlier page.
.ie !\\n(^b-1 \{\
\h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.el \}\
\h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.\}
.fi
.br
.nr ^b 0
..
'\"	# VS - start vertical sidebar
'\"	# ^Y = starting y location
'\"	# ^v = 1 (for troff;  for nroff this doesn't matter)
.de VS
.mk ^Y
.ie n 'mc \s12\(br\s0
.el .nr ^v 1u
..
'\"	# VE - end of vertical sidebar
.de VE
.ie n 'mc
.el \{\
.ev 2
.nf
.ti 0
.mk ^t
\h'|\\n(^lu+3n'\L'|\\n(^Yu-1v\(bv'\v'\\n(^tu+1v-\\n(^Yu'\h'-|\\n(^lu+3n'
.sp -1
.fi
.ev
.\}
.nr ^v 0
..
'\"	# Special macro to handle page bottom:  finish off current
'\"	# box/sidebar if in box/sidebar mode, then invoked standard
'\"	# page bottom macro.
.de ^B
.ev 2
'ti 0
'nf
.mk ^t
.if \\n(^b \{\
.\"	Draw three-sided box if this is the box's first page,
.\"	draw two sides but no top otherwise.
.ie !\\n(^b-1 \h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.el \h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.\}
.if \\n(^v \{\
.nr ^x \\n(^tu+1v-\\n(^Yu
\kx\h'-\\nxu'\h'|\\n(^lu+3n'\ky\L'-\\n(^xu'\v'\\n(^xu'\h'|0u'\c
.\}
.bp
'fi
.ev
.if \\n(^b \{\
.mk ^y
.nr ^b 2
.\}
.if \\n(^v \{\
.mk ^Y
.\}
..
'\"	# DS - begin display
.de DS
.RS
.nf
.sp
..
'\"	# DE - end display
.de DE
.fi
.RE
.sp
..
.HS menubutton tk 4.0
.BS
'\" Note:  do not modify the .SH NAME line immediately below!
.SH NAME
menubutton \- Create and manipulate menubutton widgets
.SH SYNOPSIS
\fBmenubutton\fI \fIpathName \fR?\fIoptions\fR?
.SH "STANDARD OPTIONS"
.LP
.nf
.ta 4c 8c 12c
.VS
\fBactiveBackground\fR	\fBcursor\fR	\fBimage\fR	\fBtextVariable\fR
\fBactiveForeground\fR	\fBdisabledForeground\fR	\fBjustify\fR	\fBunderline\fR
\fBanchor\fR	\fBfont\fR	\fBpadX\fR	\fBwrapLength\fR
\fBbackground\fR	\fBforeground\fR	\fBpadY\fR
\fBbitmap\fR	\fBhighlightColor\fR	\fBrelief\fR
.VE
\fBborderWidth\fR	\fBhighlightThickness\fR	\fBtext\fR
.fi
.LP
See the ``options'' manual entry for details on the standard options.
.SH "WIDGET-SPECIFIC OPTIONS"
.ta 4c
.LP
.nf
Name:	\fBheight\fR
Class:	\fBHeight\fR
Command-Line Switch:	\fB\-height\fR
.fi
.IP
Specifies a desired height for the menubutton.
If an image or bitmap is being displayed in the menubutton then the value is in
screen units (i.e. any of the forms acceptable to \fBTk_GetPixels\fR);
for text it is in lines of text.
If this option isn't specified, the menubutton's desired height is computed
from the size of the image or bitmap or text being displayed in it.
.LP
.nf
.VS
Name:	\fBindicatorOn\fR
Class:	\fBIndicatorOn\fR
Command-Line Switch:	\fB\-indicatoron\fR
.fi
.IP
The value must be a proper boolean value.  If it is true then
a small indicator rectangle will be displayed on the right side
of the menubutton and the default menu bindings will treat this
as an option menubutton.  If false then no indicator will be
displayed.
.VE
.LP
.nf
Name:	\fBmenu\fR
Class:	\fBMenuName\fR
Command-Line Switch:	\fB\-menu\fR
.fi
.IP
Specifies the path name of the menu associated with this menubutton.
The menu must be a child of the menubutton.
.LP
.nf
Name:	\fBstate\fR
Class:	\fBState\fR
Command-Line Switch:	\fB\-state\fR
.fi
.IP
Specifies one of three states for the menubutton:  \fBnormal\fR, \fBactive\fR,
or \fBdisabled\fR.  In normal state the menubutton is displayed using the
\fBforeground\fR and \fBbackground\fR options.  The active state is
typically used when the pointer is over the menubutton.  In active state
the menubutton is displayed using the \fBactiveForeground\fR and
\fBactiveBackground\fR options.  Disabled state means that the menubutton
is insensitive:  it doesn't activate and doesn't respond to mouse
button presses.  In this state the \fBdisabledForeground\fR and
\fBbackground\fR options determine how the button is displayed.
.LP
.nf
Name:	\fBwidth\fR
Class:	\fBWidth\fR
Command-Line Switch:	\fB\-width\fR
.fi
.IP
Specifies a desired width for the menubutton.
If an image or bitmap is being displayed in the menubutton then the value is in
screen units (i.e. any of the forms acceptable to \fBTk_GetPixels\fR);
for text it is in characters.
If this option isn't specified, the menubutton's desired width is computed
from the size of the image or bitmap or text being displayed in it.
.BE

.SH INTRODUCTION
.PP
The \fBmenubutton\fR command creates a new window (given by the
\fIpathName\fR argument) and makes it into a menubutton widget.
Additional
options, described above, may be specified on the command line
or in the option database
to configure aspects of the menubutton such as its colors, font,
text, and initial relief.  The \fBmenubutton\fR command returns its
\fIpathName\fR argument.  At the time this command is invoked,
there must not exist a window named \fIpathName\fR, but
\fIpathName\fR's parent must exist.
.PP
A menubutton is a widget that displays a
.VS
textual string, bitmap, or image
and is associated with a menu widget.
If text is displayed, it must all be in a single font, but it
can occupy multiple lines on the screen (if it contains newlines
or if wrapping occurs because of the \fBwrapLength\fR option) and
one of the characters may optionally be underlined using the
\fBunderline\fR option.
.VE
In normal usage, pressing
mouse button 1 over the menubutton causes the associated menu to
be posted just underneath the menubutton.  If the mouse is moved over
the menu before releasing the mouse button, the button release
causes the underlying menu entry to be invoked.  When the button
is released, the menu is unposted.
.PP
Menubuttons are typically organized into groups called menu bars
that allow scanning:
if the mouse button is pressed over one menubutton (causing it
to post its menu) and the mouse is moved over another menubutton
in the same menu bar without releasing the mouse button, then the
menu of the first menubutton is unposted and the menu of the
new menubutton is posted instead.
.PP
.VS
There are several interactions between menubuttons and menus;  see
the \fBmenu\fR manual entry for information on various menu configurations,
such as pulldown menus and option menus.
.VS

.SH "WIDGET COMMAND"
.PP
The \fBmenubutton\fR command creates a new Tcl command whose
name is \fIpathName\fR.  This
command may be used to invoke various
operations on the widget.  It has the following general form:
.DS C
\fIpathName option \fR?\fIarg arg ...\fR?
.DE
\fIOption\fR and the \fIarg\fRs
determine the exact behavior of the command.  The following
commands are possible for menubutton widgets:
.TP
\fIpathName \fBcget\fR \fIoption\fR
.VS
Returns the current value of the configuration option given
by \fIoption\fR.
\fIOption\fR may have any of the values accepted by the \fBmenubutton\fR
command.
.VE
.TP
\fIpathName \fBconfigure\fR ?\fIoption\fR? ?\fIvalue option value ...\fR?
Query or modify the configuration options of the widget.
If no \fIoption\fR is specified, returns a list describing all of
the available options for \fIpathName\fR (see \fBTk_ConfigureInfo\fR for
information on the format of this list).  If \fIoption\fR is specified
with no \fIvalue\fR, then the command returns a list describing the
one named option (this list will be identical to the corresponding
sublist of the value returned if no \fIoption\fR is specified).  If
one or more \fIoption\-value\fR pairs are specified, then the command
modifies the given widget option(s) to have the given value(s);  in
this case the command returns an empty string.
\fIOption\fR may have any of the values accepted by the \fBmenubutton\fR
command.

.SH "DEFAULT BINDINGS"
.PP
Tk automatically creates class bindings for menubuttons that give them
the following default behavior:
.IP [1]
.VS
A menubutton activates whenever the mouse passes over it and deactivates
whenever the mouse leaves it.
.IP [2]
Pressing mouse button 1 over a menubutton posts the menubutton:
its relief changes to raised and its associated menu is posted
under the menubutton.  If the mouse is dragged down into the menu
with the button still down, and if the mouse button is then
released over an entry in the menu, the menubutton is unposted
and the menu entry is invoked.
.IP [3]
If button 1 is pressed over a menubutton and then released over that
menubutton, the menubutton stays posted: you can still move the mouse
over the menu and click button 1 on an entry to invoke it.
Once a menu entry has been invoked, the menubutton unposts itself.
.IP [4]
If button 1 is pressed over a menubutton and then dragged over some
other menubutton, the original menubutton unposts itself and the
new menubutton posts.
.IP [5]
If button 1 is pressed over a menubutton and released outside
any menubutton or menu, the menubutton unposts without invoking
any menu entry.
.IP [6]
When a menubutton is posted, its associated menu claims the input
focus to allow keyboard traversal of the menu and its submenus.
See the \fBmenu\fR manual entry for details on these bindings.
.IP [7]
If the \fBunderline\fR option has been specified for a menubutton
then keyboard traversal may be used to post the menubutton:
Alt+\fIx\fR, where \fIx\fR is the underlined character (or its
lower-case or upper-case equivalent), may be typed in any window
under the menubutton's toplevel to post the menubutton.
.IP [8]
The F10 key may be typed in any window to post the first menubutton
under its toplevel window that isn't disabled.
.IP [9]
If a menubutton has the input focus, the space and return keys
post the menubutton.
.VE
.PP
If the menubutton's state is \fBdisabled\fR then none of the above
actions occur:  the menubutton is completely non-responsive.
.PP
The behavior of menubuttons can be changed by defining new bindings for
individual widgets or by redefining the class bindings.

.SH KEYWORDS
menubutton, widget

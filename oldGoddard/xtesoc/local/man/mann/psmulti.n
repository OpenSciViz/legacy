.TH PSMULTI 1l "March 1992"
.SH NAME
psmulti \- 
The Multi-talented postscript filter.
.SH SYNOPSIS
\f psmulti\fP
[\fBGeneral Style Options\fP\]
[\fB\Default Document Options\fP\]
[\fIFilename(s)\fP [\fB\Document Specific Options\fP\]\]
.SH DESCRIPTION
.B psmulti
is a development of an original program called multips, primarily designed
to print multiple logical postscript pages onto a single physical page.
.B psmulti
provides this facility in a more convenient form and has additional
capablilities such as the selection of specific pages from a
structured document, encapsulation of postcript images within defined
bounding boxes in EPS compliant format.

.SH OPTIONS

The options for psmulti are divided into two parts, general style
options, document specific options. The style options determine the
overall layout of the logical pages on the physical page, the document
options define how the pages of the specific document will be laid out
in their allocated space. Document options that are given before the
first filename are taken as defaults for all the documents processed,
these may be overriden for a specific document by document options
given after the corresponding filename.

If no filename is given or the filename is a `-' it is assumed that the
document is to be read from the standard input.

The default layout for
.B psmulti
is a 1x2 grid with the logical pages rotated
by -90 degrees (landscape orientation).

.B psmulti
uses the bounding box information in the the documents comment section if
available to to scale and center the image on the logical page. This provides
a convenient way to preview images that are actually larger than the
physical page.

.SH General Options
.TP 1i
.B \-v
Print to standard error lots of interesting information on progress of
program.
.TP 1i
.B \-format \fP name
This option allows for the selection of alternative document formatters,
currently only two are available, viz.
.RS
.RS
.TP
\fI standard \fP - The formatter for normal uses.
.TP
\fI raw \fP - Use this for simple jobs, or when simplicity is desired
.RE
.TP
`standard' is the default formatter, but raw may be useful where you have
something that is a bit stubborn!
.RE
.TP 1i
.B \-border \fP name
This option provides different alternatives for framing the pages,
currently available styles are
.RS
.RS
.TP
\fI pborder \fP - A simple border around each logical page.
.TP
\fI iborder \fP - A simple border around each images Bounding Box.
.TP
\fI 2border \fP - A combination of the 2 above.
.TP
\fI shadow \fP - Prints a shadow under the image bounding box.
.TP
\fI frame \fP - Prints a frame around the logical page.
.TP
\fI psmulti \fP - Prints a banner for psmulti around the image.
.RE
.TP
The default style is determined by the format used. Other local border
styles may be available see your administrator.
.RE
.TP 1i
.B \-nodecor
Do not print any border decoration around the logical pages.
.TP 1i
.B \-pages
Defines the number of logical pages to print on a physical page.
This utilises a rough and ready algorithm that determines a grid
geometry and image rotation that minimises scaling.
.TP 1i
.B \-geom\fP RxC
Define the grid geometry that the logical pages will be tiled over
onto the physical page, default value 1x2.
.TP 1i
.B \-ttob
Fill the grid from top to bottom, this is the default.
.TP 1i
.B \-btot
Fill the grid from bottom to top.
.TP 1i
.B \-ltor
Fill the grid from left to right, this is the default.
.TP 1i
.B \-rtol
Fill the grid from right to left.
.TP 1i
.B \-rmajor
Fill the rows first then the columns, this is the default.
.TP 1i
.B \-cmajor
Fill the columns first then the rows.
.TP 1i
.B \-pbbox \fP "llx lly urx ury | page | a4 | letter ..."
Defines the physical page bounding box, within which the logical pages
will be tiled. If given as `page' it takes the bounding box of the
printable region. The bounding box may also be given as a standard
papers size e.g. a4, legal, letter.
.TP 1i
.B \-o \fP filename
Define the filename to which the output is to be directed, defaults to
the standard output.

.SH Document Specific Options
.TP 1i
.B \-margin \fP num
Specify the size of margin (in points) to leave around the border of the
image, the default is 5.
.TP 1i
.B \-eps,\-neps
Declare that the document is/is'nt encapsulated postscript, if this is not
defined in the options it is determined from the version comment at the
beginning of the document.
.TP 1i
.B \-maxpect
Scale document as large as possible to fit logical page while maintaning
the aspect ratio of the image.
.B \-maxxy
Scale the document to fill the page in both height and width. This
will result in a loss of proportion in the figure.
.TP 1i
.B \-ibbox\fP "llx lly urx ury | page | dsc | a4 | letter ..."
The defines the bounding box for the images in the document. If given
as `page' the bounding box is determined by the printable region of
the printer. If dsc it takes the bounding box from the file comments.
It may also be given as letter, legal, a4 etc which identifies a standard
paper size. If the image bbox is not defined in the options list it
is derived from the comments section of the document or taken to be `page'
by default.
.TP 1i
.B \-p
This set up psmulti to lay pages out in portrait fashion, i.e.
default rotation 0, ordering left to right, top to bottom, row major.
.TP 1i
.B \-l
This set up psmulti to lay pages out in landscape fashion, i.e.
default rotation -90, ordering top to bottom, right to left, column major.
.TP 1i
.B \-r[otate] \fP degrees
Specify the angle the images of this document are to be rotated to when
printing.
.TP 1i
.B \-select \fP [ordinal | label] pageN:pageM,pageA, ...
Select the specified range of pages from the document, by default this is
done by their absolute numbering (ordinal), but it may also be done
according to their page labels is which case `label' must be specified
before the start of the page range. A sequence of pages from N to M is
specified by N:M, if N is missing the sequence is taken from the first page
to M, and similarily if M is missing the sequence is taken from M to the
last page.
.TP 1i
.B \-reverse
Reverse the order of the pages being printed from the document.
.SH EXAMPLES
.TP
1. 
.B "eg% psmulti -pages 4 myfile.ps | lpr -Pcw"
.LP
.RS
.RS
Print out a postscript document myfile.ps with four logical pages to one 
physical page.
.RE
.RE
.TP 
2.
.B "eg% dvips chap.dvi | psmulti -border psmulti -select 2,5:8 -reverse | lpr"
.LP
.RS
.RS
Print the selected pages from chap.dvi in the order 8,7,6,5,2 with the
ecosse border printed on them.
.RE
.RE
.TP
3.
.B "eg% psmulti -pages 1 -border shadow slides.ps | lpr"
.LP
.RS
.RS
Produce a copy of slides.ps with the ecosse border superimposed.
.RE
.RE
.TP
4.
.B "eg% psmulti -ibbox page doc.ps | lpr"
.LP
.RS
.RS
Override the bounding box defined in the DSC comments of doc.ps and
use the bounding box of the page clippath.
.RE
.RE
.TP
5.
.B "eg% psmulti -ibbox a4 -pages 1 doc.ps | lpr -Pcs23
.LP
.RS
.RS
Print an A4 sized image on the colour A3 printer, in full A3 size.
.RE
.RE
.TP
6.
.B "eq% psmulti -pages 1 -margin 0 -nodecor idraw.ps | lpr"
.LP
.RS
.RS
Preview the idraw image which is actually larger than a4, but will be scaled
to fit onto the page by psmulti.
.SH DIAGNOSTICS
The diagnostics are supposed to be self-explanatory.
.SH AUTHOR
.B psmulti
was written and developed by D.Murray.Laing
.RS
.nf
Department of Chemical Engineering, 
Edinburgh University,
The Kings Buildings, 
Mayfield Road, 
Edinburgh. 
EH9 3JL
Scotland. UK.
.fi
.RE
.IP
Email : murray@uk.ac.ed.chemeng
.IP
.LP
The original postscript prolog from which this was developed was
written by Ross Cartlidge
.RS
.nf
University Computing Service,
Building H08, Sydney University,
NSW, 2006, Australia
.fi
.RE
.IP
Email : rossc@extro.ucc.su.oz.au 
.IP 
.SH BUGS
Only those intended to be there !




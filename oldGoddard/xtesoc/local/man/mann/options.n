'\"
'\" Copyright (c) 1990-1994 The Regents of the University of California.
'\" Copyright (c) 1994 Sun Microsystems, Inc.
'\"
'\" See the file "license.terms" for information on usage and redistribution
'\" of this file, and for a DISCLAIMER OF ALL WARRANTIES.
'\" 
'\" @(#) options.n 1.26 94/12/17 16:04:52
'/" 
'\" The definitions below are for supplemental macros used in Tcl/Tk
'\" manual entries.
'\"
'\" .HS name section [date [version]]
'\"	Replacement for .TH in other man pages.  See below for valid
'\"	section names.
'\"
'\" .AP type name in/out [indent]
'\"	Start paragraph describing an argument to a library procedure.
'\"	type is type of argument (int, etc.), in/out is either "in", "out",
'\"	or "in/out" to describe whether procedure reads or modifies arg,
'\"	and indent is equivalent to second arg of .IP (shouldn't ever be
'\"	needed;  use .AS below instead)
'\"
'\" .AS [type [name]]
'\"	Give maximum sizes of arguments for setting tab stops.  Type and
'\"	name are examples of largest possible arguments that will be passed
'\"	to .AP later.  If args are omitted, default tab stops are used.
'\"
'\" .BS
'\"	Start box enclosure.  From here until next .BE, everything will be
'\"	enclosed in one large box.
'\"
'\" .BE
'\"	End of box enclosure.
'\"
'\" .VS
'\"	Begin vertical sidebar, for use in marking newly-changed parts
'\"	of man pages.
'\"
'\" .VE
'\"	End of vertical sidebar.
'\"
'\" .DS
'\"	Begin an indented unfilled display.
'\"
'\" .DE
'\"	End of indented unfilled display.
'\"
'\" @(#) man.macros 1.1 94/08/09 13:07:19
.\"
'\"	# Heading for Tcl/Tk man pages
.de HS
.ds ^3 \\0
.if !"\\$3"" .ds ^3 \\$3
.if '\\$2'cmds'      .TH "\\$1" 1 "\\*(^3" "\\$4" "\\$5"
.if '\\$2'lib'       .TH "\\$1" 3 "\\*(^3" "\\$4" "\\$5"
.if '\\$2'ncmds'     .TH "\\$1" n "\\*(^3" "\\$4" "\\$5"
.if '\\$2'tcl'       .TH "\\$1" n "\\*(^3"  Tcl   "Tcl Built-In Commands"
.if '\\$2'tk'        .TH "\\$1" n "\\*(^3"  Tk    "Tk Commands"
.if '\\$2'tclc'      .TH "\\$1" 3 "\\*(^3"  Tcl   "Tcl Library Procedures"
.if '\\$2'tkc'       .TH "\\$1" 3 "\\*(^3"  Tk    "Tk Library Procedures"
.if '\\$2'tclcmds'   .TH "\\$1" 1 "\\*(^3"  Tk    "Tcl Applications"
.if '\\$2'tkcmds'    .TH "\\$1" 1 "\\*(^3"  Tk    "Tk Applications"
.if t .wh -1.3i ^B
.nr ^l \\n(.l
.ad b
..
'\"	# Start an argument description
.de AP
.ie !"\\$4"" .TP \\$4
.el \{\
.   ie !"\\$2"" .TP \\n()Cu
.   el          .TP 15
.\}
.ie !"\\$3"" \{\
.ta \\n()Au \\n()Bu
\&\\$1	\\fI\\$2\\fP	(\\$3)
.\".b
.\}
.el \{\
.br
.ie !"\\$2"" \{\
\&\\$1	\\fI\\$2\\fP
.\}
.el \{\
\&\\fI\\$1\\fP
.\}
.\}
..
'\"	# define tabbing values for .AP
.de AS
.nr )A 10n
.if !"\\$1"" .nr )A \\w'\\$1'u+3n
.nr )B \\n()Au+15n
.\"
.if !"\\$2"" .nr )B \\w'\\$2'u+\\n()Au+3n
.nr )C \\n()Bu+\\w'(in/out)'u+2n
..
'\"	# BS - start boxed text
'\"	# ^y = starting y location
'\"	# ^b = 1
.de BS
.br
.mk ^y
.nr ^b 1u
.if n .nf
.if n .ti 0
.if n \l'\\n(.lu\(ul'
.if n .fi
..
'\"	# BE - end boxed text (draw box now)
.de BE
.nf
.ti 0
.mk ^t
.ie n \l'\\n(^lu\(ul'
.el \{\
.\"	Draw four-sided box normally, but don't draw top of
.\"	box if the box started on an earlier page.
.ie !\\n(^b-1 \{\
\h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.el \}\
\h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.\}
.fi
.br
.nr ^b 0
..
'\"	# VS - start vertical sidebar
'\"	# ^Y = starting y location
'\"	# ^v = 1 (for troff;  for nroff this doesn't matter)
.de VS
.mk ^Y
.ie n 'mc \s12\(br\s0
.el .nr ^v 1u
..
'\"	# VE - end of vertical sidebar
.de VE
.ie n 'mc
.el \{\
.ev 2
.nf
.ti 0
.mk ^t
\h'|\\n(^lu+3n'\L'|\\n(^Yu-1v\(bv'\v'\\n(^tu+1v-\\n(^Yu'\h'-|\\n(^lu+3n'
.sp -1
.fi
.ev
.\}
.nr ^v 0
..
'\"	# Special macro to handle page bottom:  finish off current
'\"	# box/sidebar if in box/sidebar mode, then invoked standard
'\"	# page bottom macro.
.de ^B
.ev 2
'ti 0
'nf
.mk ^t
.if \\n(^b \{\
.\"	Draw three-sided box if this is the box's first page,
.\"	draw two sides but no top otherwise.
.ie !\\n(^b-1 \h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.el \h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.\}
.if \\n(^v \{\
.nr ^x \\n(^tu+1v-\\n(^Yu
\kx\h'-\\nxu'\h'|\\n(^lu+3n'\ky\L'-\\n(^xu'\v'\\n(^xu'\h'|0u'\c
.\}
.bp
'fi
.ev
.if \\n(^b \{\
.mk ^y
.nr ^b 2
.\}
.if \\n(^v \{\
.mk ^Y
.\}
..
'\"	# DS - begin display
.de DS
.RS
.nf
.sp
..
'\"	# DE - end display
.de DE
.fi
.RE
.sp
..
.HS options tk 4.0
.BS
'\" Note:  do not modify the .SH NAME line immediately below!
.SH NAME
options \- Standard options supported by widgets
.BE

.SH DESCRIPTION
This manual entry describes the common configuration options supported
by widgets in the Tk toolkit.  Every widget does not necessarily support
every option (see the manual entries for individual widgets for a list
of the standard options supported by that widget), but if a widget does
support an option with one of the names listed below, then the option
has exactly the effect described below.
.PP
In the descriptions below,
``Name'' refers to the option's name in the option database (e.g.
in .Xdefaults files).  ``Class'' refers to the option's class value
in the option database.  ``Command-Line Switch'' refers to the
switch used in widget-creation and \fBconfigure\fR widget commands to
set this value.  For example, if an option's command-line switch is
\fB\-foreground\fR and there exists a widget \fB.a.b.c\fR, then the
command
.DS
\&\fB.a.b.c\0\0configure\0\0\-foreground black\fR
.DE
may be used to specify the value \fBblack\fR for the option in the
the widget \fB.a.b.c\fR.  Command-line switches may be abbreviated,
as long as the abbreviation is unambiguous.
.ta 4c
.LP
.nf
Name:	\fBactiveBackground\fR
Class:	\fBForeground\fR
Command-Line Switch:	\fB\-activebackground\fR
.fi
.IP
Specifies background color to use when drawing active elements.
An element (a widget or portion of a widget) is active if the
mouse cursor is positioned over the element and pressing a mouse button
will cause some action to occur.
.LP
.nf
Name:	\fBactiveBorderWidth\fR
Class:	\fBBorderWidth\fR
Command-Line Switch:	\fB\-activeborderwidth\fR
.fi
.IP
Specifies a non-negative value indicating
the width of the 3-D border drawn around active elements.  See above for
definition of active elements.
The value may have any of the forms acceptable to \fBTk_GetPixels\fR.
This option is typically only available in widgets displaying more
than one element at a time (e.g. menus but not buttons).
.LP
.nf
Name:	\fBactiveForeground\fR
Class:	\fBBackground\fR
Command-Line Switch:	\fB\-activeforeground\fR
.fi
.IP
Specifies foreground color to use when drawing active elements.
See above for definition of active elements.
.LP
.nf
Name:	\fBanchor\fR
Class:	\fBAnchor\fR
Command-Line Switch:	\fB\-anchor\fR
.fi
.IP
Specifies how the information in a widget (e.g. text or a bitmap)
is to be displayed in the widget.
Must be one of the values \fBn\fR, \fBne\fR, \fBe\fR, \fBse\fR,
\fBs\fR, \fBsw\fR, \fBw\fR, \fBnw\fR, or \fBcenter\fR.
For example, \fBnw\fR means display the information such that its
top-left corner is at the top-left corner of the widget.
.LP
.nf
Name:	\fBbackground\fR
Class:	\fBBackground\fR
Command-Line Switch:	\fB\-background or \-bg\fR
.fi
.IP
Specifies the normal background color to use when displaying the
widget.
.LP
.nf
Name:	\fBbitmap\fR
Class:	\fBBitmap\fR
Command-Line Switch:	\fB\-bitmap\fR
.fi
.IP
Specifies a bitmap to display in the widget, in any of the forms
acceptable to \fBTk_GetBitmap\fR.
The exact way in which the bitmap is displayed may be affected by
other options such as \fBanchor\fR or \fBjustify\fR.
Typically, if this option is specified then it overrides other
options that specify a textual value to display in the widget;
the \fBbitmap\fR option may be reset to an empty string to re-enable
a text display.
.VS
In widgets that support both \fBbitmap\fR and \fBimage\fR options,
\fBimage\fR will usually override \fBbitmap\fR.
.VE
.LP
.nf
Name:	\fBborderWidth\fR
Class:	\fBBorderWidth\fR
Command-Line Switch:	\fB\-borderwidth or \-bd\fR
.fi
.IP
Specifies a non-negative value indicating the width
of the 3-D border to draw around the outside of the widget (if such a
border is being drawn;  the \fBrelief\fR option typically determines
this).  The value may also be used when drawing 3-D effects in the
interior of the widget.
The value may have any of the forms acceptable to \fBTk_GetPixels\fR.
.LP
.nf
Name:	\fBcursor\fR
Class:	\fBCursor\fR
Command-Line Switch:	\fB\-cursor\fR
.fi
.IP
Specifies the mouse cursor to be used for the widget.
The value may have any of the forms acceptable to \fBTk_GetCursor\fR.
.LP
.nf
Name:	\fBdisabledForeground\fR
Class:	\fBDisabledForeground\fR
Command-Line Switch:	\fB\-disabledforeground\fR
.fi
.IP
Specifies foreground color to use when drawing a disabled element.
If the option is specified as an empty string (which is typically the
case on monochrome displays), disabled elements are drawn with the
normal fooreground color but they are dimmed by drawing them
with a stippled fill pattern.
.LP
.nf
Name:	\fBexportSelection\fR
Class:	\fBExportSelection\fR
Command-Line Switch:	\fB\-exportselection\fR
.fi
.IP
Specifies whether or not a selection in the widget should also be
the X selection.
The value may have any of the forms accepted by \fBTcl_GetBoolean\fR,
such as \fBtrue\fR, \fBfalse\fR, \fB0\fR, \fB1\fR, \fByes\fR, or \fBno\fR.
If the selection is exported, then selecting in the widget deselects
the current X selection, selecting outside the widget deselects any
widget selection, and the widget will respond to selection retrieval
requests when it has a selection.  The default is usually for widgets
to export selections.
.LP
.nf
Name:	\fBfont\fR
Class:	\fBFont\fR
Command-Line Switch:	\fB\-font\fR
.fi
.IP
Specifies the font to use when drawing text inside the widget.
.LP
.nf
Name:	\fBforeground\fR
Class:	\fBForeground\fR
Command-Line Switch:	\fB\-foreground or \-fg\fR
.fi
.IP
Specifies the normal foreground color to use when displaying the widget.
.LP
.nf
Name:	\fBgeometry\fR
Class:	\fBGeometry\fR
Command-Line Switch:	\fB\-geometry\fR
.fi
.IP
Specifies the desired geometry for the widget's window, in the
form \fIwidth\fBx\fIheight\fR, where \fIwidth\fR is the desired
width of the window and \fIheight\fR is the desired height.  The
units for \fIwidth\fR and \fIheight\fR depend on the particular
widget.  For widgets displaying text the units are usually the
size of the characters in the font being displayed;  for other
widgets the units are usually pixels.
.LP
.nf
.VS
Name:	\fBhighlightColor\fR
Class:	\fBHighlightColor\fR
Command-Line Switch:	\fB\-highlightcolor\fR
.fi
.IP
Specifies the color to use for the highlight rectangle that is
drawn around the widget when it has the input focus.
.LP
.nf
Name:	\fBhighlightThickness\fR
Class:	\fBHighlightThickness\fR
Command-Line Switch:	\fB\-highlightthickness\fR
.fi
.IP
Specifies a non-negative value indicating the width of the highlight
rectangle to draw around the outside of the widget when it has the
input focus.
The value may have any of the forms acceptable to \fBTk_GetPixels\fR.
If the value is zero, no focus highlight is drawn around the widget.
.LP
.nf
Name:	\fBimage\fR
Class:	\fBImage\fR
Command-Line Switch:	\fB\-image\fR
.fi
.IP
Specifies an image to display in the widget, which must have been
created with the \fBimage create\fR command.
Typically, if the \fBimage\fR option is specified then it overrides other
options that specify a bitmap or textual value to display in the widget;
the \fBimage\fR option may be reset to an empty string to re-enable
a bitmap or text display.
.VE
.LP
.nf
Name:	\fBinsertBackground\fR
Class:	\fBForeground\fR
Command-Line Switch:	\fB\-insertbackground\fR
.fi
.IP
Specifies the color to use as background in the area covered by the
insertion cursor.  This color will normally override either the normal
background for the widget (or the selection background if the insertion
cursor happens to fall in the selection).
.LP
.nf
Name:	\fBinsertBorderWidth\fR
Class:	\fBBorderWidth\fR
Command-Line Switch:	\fB\-insertborderwidth\fR
.fi
.IP
Specifies a non-negative value indicating the width
of the 3-D border to draw around the insertion cursor.
The value may have any of the forms acceptable to \fBTk_GetPixels\fR.
.LP
.nf
Name:	\fBinsertOffTime\fR
Class:	\fBOffTime\fR
Command-Line Switch:	\fB\-insertofftime\fR
.fi
.IP
Specifies a non-negative integer value indicating the number of
milliseconds the insertion cursor should remain ``off'' in each blink cycle.
If this option is zero then the cursor doesn't blink:  it is on
all the time.
.LP
.nf
Name:	\fBinsertOnTime\fR
Class:	\fBOnTime\fR
Command-Line Switch:	\fB\-insertontime\fR
.fi
.IP
Specifies a non-negative integer value indicating the number of
milliseconds the insertion cursor should remain ``on'' in each blink cycle.
.LP
.nf
Name:	\fBinsertWidth\fR
Class:	\fBInsertWidth\fR
Command-Line Switch:	\fB\-insertwidth\fR
.fi
.IP
Specifies a  value indicating the total width of the insertion cursor.
The value may have any of the forms acceptable to \fBTk_GetPixels\fR.
If a border has been specified for the insertion
cursor (using the \fBinsertBorderWidth\fR option), the border
will be drawn inside the width specified by the \fBinsertWidth\fR
option.
.LP
.nf
.VS
Name:	\fBjump\fR
Class:	\fBJump\fR
Command-Line Switch:	\fB\-jump\fR
.fi
.IP
For widgets with a slider that can be dragged to adjust a value,
such as scrollbars and scales, this option determines when
notifications are made about changes in the value.
The option's value must be a boolean of the form accepted by
\fBTcl_GetBoolean\fR.
If the value is false, updates are made continuously as the
slider is dragged.
If the value is true, updates are delayed until the mouse button
is released to end the drag;  at that point a single notification
is made (the value ``jumps'' rather than changing smoothly).
.LP
.nf
Name:	\fBjustify\fR
Class:	\fBJustify\fR
Command-Line Switch:	\fB\-justify\fR
.fi
.IP
When there are multiple lines of text displayed in a widget, this
option determines how the lines line up with each other.
Must be one of \fBleft\fR, \fBcenter\fR, or \fBright\fR.
\fBLeft\fR means that the lines' left edges all line up, \fBcenter\fR
means that the lines' centers are aligned, and \fBright\fR means
that the lines' right edges line up.
.VE
.LP
.nf
Name:	\fBorient\fR
Class:	\fBOrient\fR
Command-Line Switch:	\fB\-orient\fR
.fi
.IP
For widgets that can lay themselves out with either a horizontal
or vertical orientation, such as scrollbars, this option specifies
which orientation should be used.  Must be either \fBhorizontal\fR
or \fBvertical\fR or an abbreviation of one of these.
.LP
.nf
Name:	\fBpadX\fR
Class:	\fBPad\fR
Command-Line Switch:	\fB\-padx\fR
.fi
.IP
Specifies a non-negative value indicating how much extra space
to request for the widget in the X-direction.
The value may have any of the forms acceptable to \fBTk_GetPixels\fR.
When computing how large a window it needs, the widget will
add this amount to the width it would normally need (as determined
by the width of the things displayed in the widget);  if the geometry
manager can satisfy this request, the widget will end up with extra
internal space to the left and/or right of what it displays inside.
.VS
Most widgets only use this option for padding text:  if they are
displaying a bitmap or image, then they usually ignore padding
options.
.VE
.LP
.nf
Name:	\fBpadY\fR
Class:	\fBPad\fR
Command-Line Switch:	\fB\-pady\fR
.fi
.IP
Specifies a non-negative value indicating how much extra space
to request for the widget in the Y-direction.
The value may have any of the forms acceptable to \fBTk_GetPixels\fR.
When computing how large a window it needs, the widget will add
this amount to the height it would normally need (as determined by
the height of the things displayed in the widget);  if the geometry
manager can satisfy this request, the widget will end up with extra
internal space above and/or below what it displays inside.
.VS
Most widgets only use this option for padding text:  if they are
displaying a bitmap or image, then they usually ignore padding
options.
.LP
.nf
Name:	\fBrelief\fR
Class:	\fBRelief\fR
Command-Line Switch:	\fB\-relief\fR
.fi
.IP
Specifies the 3-D effect desired for the widget.  Acceptable
values are \fBraised\fR, \fBsunken\fR, \fBflat\fR, \fBridge\fR,
and \fBgroove\fR.
The value
indicates how the interior of the widget should appear relative
to its exterior;  for example, \fBraised\fR means the interior of
the widget should appear to protrude from the screen, relative to
the exterior of the widget.
.LP
.nf
Name:	\fBrepeatDelay\fR
Class:	\fBRepeatDelay\fR
Command-Line Switch:	\fB\-repeatdelay\fR
.fi
.IP
Specifies the number of milliseconds a button or key must be held
down before it begins to auto-repeat.  Used, for example, on the
up- and down-arrows in scrollbars.
.LP
.nf
Name:	\fBrepeatInterval\fR
Class:	\fBRepeatInterval\fR
Command-Line Switch:	\fB\-repeatinterval\fR
.fi
.IP
Used in conjunction with \fBrepeatDelay\fR:  once auto-repeat
begins, this option determines the number of milliseconds between
auto-repeats.
.LP
.nf
Name:	\fBselectBackground\fR
Class:	\fBForeground\fR
Command-Line Switch:	\fB\-selectbackground\fR
.fi
.IP
Specifies the background color to use when displaying selected
items.
.LP
.nf
Name:	\fBselectBorderWidth\fR
Class:	\fBBorderWidth\fR
Command-Line Switch:	\fB\-selectborderwidth\fR
.fi
.IP
Specifies a non-negative value indicating the width
of the 3-D border to draw around selected items.
The value may have any of the forms acceptable to \fBTk_GetPixels\fR.
.LP
.nf
Name:	\fBselectForeground\fR
Class:	\fBBackground\fR
Command-Line Switch:	\fB\-selectforeground\fR
.fi
.IP
Specifies the foreground color to use when displaying selected
items.
.LP
.nf
Name:	\fBsetGrid\fR
Class:	\fBSetGrid\fR
Command-Line Switch:	\fB\-setgrid\fR
.fi
.IP
Specifies a boolean value that determines whether this widget controls the
resizing grid for its top-level window.
This option is typically used in text widgets, where the information
in the widget has a natural size (the size of a character) and it makes
sense for the window's dimensions to be integral numbers of these units.
These natural window sizes form a grid.
If the \fBsetGrid\fR option is set to true then the widget will
communicate with the window manager so that when the user interactively
resizes the top-level window that contains the widget, the dimensions of
the window will be displayed to the user in grid units and the window
size will be constrained to integral numbers of grid units.
See the section GRIDDED GEOMETRY MANAGEMENT in the \fBwm\fR manual
entry for more details.
.LP
.nf
Name:	\fBtext\fR
Class:	\fBText\fR
Command-Line Switch:	\fB\-text\fR
.fi
.IP
Specifies a string to be displayed inside the widget.  The way in which
the string is displayed depends on the particular widget and may be
determined by other options, such as \fBanchor\fR or \fBjustify\fR.
.LP
.nf
Name:	\fBtextVariable\fR
Class:	\fBVariable\fR
Command-Line Switch:	\fB\-textvariable\fR
.fi
.IP
Specifies the name of a variable.  The value of the variable is a text
string to be displayed inside the widget;  if the variable value changes
then the widget will automatically update itself to reflect the new value.
The way in which the string is displayed in the widget depends on the
particular widget and may be determined by other options, such as
\fBanchor\fR or \fBjustify\fR.
.LP
.nf
.VS
Name:	\fBtroughColor\fR
Class:	\fBBackground\fR
Command-Line Switch:	\fB\-troughcolor\fR
.fi
.IP
Specifies the color to use for the rectangular trough areas
in widgets such as scrollbars and scales.
.VE
.LP
.nf
Name:	\fBunderline\fR
Class:	\fBUnderline\fR
Command-Line Switch:	\fB\-underline\fR
.fi
.IP
Specifies the integer index of a character to underline in the widget.
This option is used by the default bindings to implement keyboard
traversal for menu buttons and menu entries.
0 corresponds to the first character of the text displayed in the
widget, 1 to the next character, and so on.
.LP
.nf
.VS
Name:	\fBwrapLength\fR
Class:	\fBWrapLength\fR
Command-Line Switch:	\fB\-wraplength\fR
.fi
.IP
For widgets that can perform word-wrapping, this option specifies
the maximum line length.
Lines that would exceed this length are wrapped onto the next line,
so that no line is longer than the specified length.
The value may be specified in any of the standard forms for
screen distances.
If this value is less than or equal to 0 then no wrapping is done:  lines
will break only at newline characters in the text.
.VE
.LP
.nf
Name:	\fBxScrollCommand\fR
Class:	\fBScrollCommand\fR
Command-Line Switch:	\fB\-xscrollcommand\fR
.fi
.IP
Specifies the prefix for a command used to communicate with horizontal
scrollbars.
When the view in the widget's window changes (or
whenever anything else occurs that could change the display in a
scrollbar, such as a change in the total size of the widget's
contents), the widget will
generate a Tcl command by concatenating the scroll command and
.VS
two numbers.
Each of the numbers is a fraction between 0 and 1, which indicates
a position in the document.  0 indicates the beginning of the document,
1 indicates the end, .333 indicates a position one third the way through
the document, and so on.
The first fraction indicates the first information in the document
that is visible in the window, and the second fraction indicates
the information just after the last portion that is visible.
The command is
.VE
then passed to the Tcl interpreter for execution.  Typically the
\fBxScrollCommand\fR option consists of the path name of a scrollbar
widget followed by ``set'', e.g. ``.x.scrollbar set'':  this will cause
the scrollbar to be updated whenever the view in the window changes.
If this option is not specified, then no command will be executed.
.LP
.nf
Name:	\fByScrollCommand\fR
Class:	\fBScrollCommand\fR
Command-Line Switch:	\fB\-yscrollcommand\fR
.fi
.IP
Specifies the prefix for a command used to communicate with vertical
scrollbars.  This option is treated in the same way as the
\fByScrollCommand\fR option, except that it is used for vertical
scrollbars associated with widgets that support both horizontal
and vertical scrolling.
See the description of \fBxScrollCommand\fR for details
on how this option is used.

.SH KEYWORDS
class, name, standard option, switch

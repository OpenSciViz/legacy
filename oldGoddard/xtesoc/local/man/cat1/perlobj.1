


PERLOBJ(1)                User Commands                PERLOBJ(1)



NAME
     perlobj - Perl objects

DESCRIPTION
     First of all, you need to understand what references are in
     Perl.  See the _p_e_r_l_r_e_f manpage for that.

     Here are three very simple definitions that you should find
     reassuring.

     1.  An object is simply a reference that happens to know
         which class it belongs to.

     2.  A class is simply a package that happens to provide
         methods to deal with object references.

     3.  A method is simply a subroutine that expects an object
         reference (or a package name, for static methods) as the
         first argument.

     We'll cover these points now in more depth.

     An Object is Simply a Reference

     Unlike say C++, Perl doesn't provide any special syntax for
     constructors.  A constructor is merely a subroutine that
     returns a reference that has been "blessed" into a class,
     generally the class that the subroutine is defined in.  Here
     is a typical constructor:

         package Critter;
         sub new { bless {} }

     The {} constructs a reference to an anonymous hash
     containing no key/value pairs.  The _b_l_e_s_s() takes that
     reference and tells the object it references that it's now a
     Critter, and returns the reference.  This is for
     convenience, since the referenced object itself knows that
     it has been blessed, and its reference to it could have been
     returned directly, like this:

         sub new {
             my $self = {};
             bless $self;
             return $self;
         }

     In fact, you often see such a thing in more complicated
     constructors that wish to call methods in the class as part
     of the construction:





SunOS 5.4    Last change: Release 5.0 Patchlevel 01             1






PERLOBJ(1)                User Commands                PERLOBJ(1)



         sub new {
             my $self = {}
             bless $self;
             $self->initialize();
             $self;
         }

     Within the class package, the methods will typically deal
     with the reference as an ordinary reference.  Outside the
     class package, the reference is generally treated as an
     opaque value that may only be accessed through the class's
     methods.

     A constructor may re-bless a referenced object currently
     belonging to another class, but then the new class is
     responsible for all cleanup later.  The previous blessing is
     forgotten, as an object may only belong to one class at a
     time.  (Although of course it's free to inherit methods from
     many classes.)

     A clarification:  Perl objects are blessed.  References are
     not.  Objects know which package they belong to.  References
     do not.  The _b_l_e_s_s() function simply uses the reference in
     order to find the object.  Consider the following example:

         $a = {};
         $b = $a;
         bless $a, BLAH;
         print "\$b is a ", ref($b), "\n";

     This reports $b as being a BLAH, so obviously _b_l_e_s_s()
     operated on the object and not on the reference.

     A Class is Simply a Package

     Unlike say C++, Perl doesn't provide any special syntax for
     class definitions.  You just use a package as a class by
     putting method definitions into the class.

     There is a special array within each package called @ISA
     which says where else to look for a method if you can't find
     it in the current package.  This is how Perl implements
     inheritance.  Each element of the @ISA array is just the
     name of another package that happens to be a class package.
     The classes are searched (depth first) for missing methods
     in the order that they occur in @ISA.  The classes
     accessible through @ISA are known as base classes of the
     current class.

     If a missing method is found in one of the base classes, it
     is cached in the current class for efficiency.  Changing
     @ISA or defining new subroutines invalidates the cache and



SunOS 5.4    Last change: Release 5.0 Patchlevel 01             2






PERLOBJ(1)                User Commands                PERLOBJ(1)



     causes Perl to do the lookup again.

     If a method isn't found, but an AUTOLOAD routine is found,
     then that is called on behalf of the missing method.

     If neither a method nor an AUTOLOAD routine is found in
     @ISA, then one last try is made for the method (or an
     AUTOLOAD routine) in a class called UNIVERSAL.  If that
     doesn't work, Perl finally gives up and complains.

     Perl classes only do method inheritance.  Data inheritance
     is left up to the class itself.  By and large, this is not a
     problem in Perl, because most classes model the attributes
     of their object using an anonymous hash, which serves as its
     own little namespace to be carved up by the various classes
     that might want to do something with the object.

     A Method is Simply a Subroutine

     Unlike say C++, Perl doesn't provide any special syntax for
     method definition.  (It does provide a little syntax for
     method invocation though.  More on that later.)  A method
     expects its first argument to be the object or package it is
     being invoked on.  There are just two types of methods,
     which we'll call static and virtual, in honor of the two C++
     method types they most closely resemble.

     A static method expects a class name as the first argument.
     It provides functionality for the class as a whole, not for
     any individual object belonging to the class.  Constructors
     are typically static methods.  Many static methods simply
     ignore their first argument, since they already know what
     package they're in, and don't care what package they were
     invoked via.  (These aren't necessarily the same, since
     static methods follow the inheritance tree just like
     ordinary virtual methods.)  Another typical use for static
     methods is to look up an object by name:

         sub find {
             my ($class, $name) = @_;
             $objtable{$name};
         }

     A virtual method expects an object reference as its first
     argument.  Typically it shifts the first argument into a
     "self" or "this" variable, and then uses that as an ordinary
     reference.








SunOS 5.4    Last change: Release 5.0 Patchlevel 01             3






PERLOBJ(1)                User Commands                PERLOBJ(1)



         sub display {
             my $self = shift;
             my @keys = @_ ? @_ : sort keys %$self;
             foreach $key (@keys) {
                 print "\t$key => $self->{$key}\n";
             }
         }


     Method Invocation

     There are two ways to invoke a method, one of which you're
     already familiar with, and the other of which will look
     familiar.  Perl 4 already had an "indirect object" syntax
     that you use when you say

         print STDERR "help!!!\n";

     This same syntax can be used to call either static or
     virtual methods.  We'll use the two methods defined above,
     the static method to lookup an object reference and the
     virtual method to print out its attributes.

         $fred = find Critter "Fred";
         display $fred 'Height', 'Weight';

     These could be combined into one statement by using a BLOCK
     in the indirect object slot:

         display {find Critter "Fred"} 'Height', 'Weight';

     For C++ fans, there's also a syntax using -> notation that
     does exactly the same thing.  The parentheses are required
     if there are any arguments.

         $fred = Critter->find("Fred");
         $fred->display('Height', 'Weight');

     or in one statement,

         Critter->find("Fred")->display('Height', 'Weight');

     There are times when one syntax is more readable, and times
     when the other syntax is more readable.  The indirect object
     syntax is less cluttered, but it has the same ambiguity as
     ordinary list operators.  Indirect object method calls are
     parsed using the same rule as list operators: "If it looks
     like a function, it is a function".  (Presuming for the
     moment that you think two words in a row can look like a
     function name.  C++ programmers seem to think so with some
     regularity, especially when the first word is "new".)  Thus,
     the parens of



SunOS 5.4    Last change: Release 5.0 Patchlevel 01             4






PERLOBJ(1)                User Commands                PERLOBJ(1)



         new Critter ('Barney', 1.5, 70)

     are assumed to surround ALL the arguments of the method
     call, regardless of what comes after.  Saying

         new Critter ('Bam' x 2), 1.4, 45

     would be equivalent to

         Critter->new('Bam' x 2), 1.4, 45

     which is unlikely to do what you want.

     There are times when you wish to specify which class's
     method to use.  In this case, you can call your method as an
     ordinary subroutine call, being sure to pass the requisite
     first argument explicitly:

         $fred =  MyCritter::find("Critter", "Fred");
         MyCritter::display($fred, 'Height', 'Weight');

     Note however, that this does not do any inheritance.  If you
     merely wish to specify that Perl should _S_T_A_R_T looking for a
     method in a particular package, use an ordinary method call,
     but qualify the method name with the package like this:

         $fred = Critter->MyCritter::find("Fred");
         $fred->MyCritter::display('Height', 'Weight');

     Sometimes you want to call a method when you don't know the
     method name ahead of time.  You can use the arrow form,
     replacing the method name with a simple scalar variable
     containing the method name:

         $method = $fast ? "findfirst" : "findbest";
         $fred->$method(@args);


     Destructors

     When the last reference to an object goes away, the object
     is automatically destroyed.  (This may even be after you
     exit, if you've stored references in global variables.)  If
     you want to capture control just before the object is freed,
     you may define a DESTROY method in your class.  It will
     automatically be called at the appropriate moment, and you
     can do any extra cleanup you need to do.

     Perl doesn't do nested destruction for you.  If your
     constructor reblessed a reference from one of your base
     classes, your DESTROY may need to call DESTROY for any base
     classes that need it.  But this only applies to reblessed



SunOS 5.4    Last change: Release 5.0 Patchlevel 01             5






PERLOBJ(1)                User Commands                PERLOBJ(1)



     objects--an object reference that is merely _C_O_N_T_A_I_N_E_D in the
     current object will be freed and destroyed automatically
     when the current object is freed.

     WARNING

     An indirect object is limited to a name, a scalar variable,
     or a block, because it would have to do too much lookahead
     otherwise, just like any other postfix dereference in the
     language.  The left side of -> is not so limited, because
     it's an infix operator, not a postfix operator.

     That means that below, A and B are equivalent to each other,
     and C and D are equivalent, but AB and CD are different:

         A: method $obref->{"fieldname"}
         B: (method $obref)->{"fieldname"}
         C: $obref->{"fieldname"}->method()
         D: method {$obref->{"fieldname"}}


     Summary

     That's about all there is to it.  Now you just need to go
     off and buy a book about object-oriented design methodology,
     and bang your forehead with it for the next six months or
     so.

SEE ALSO
     You should also check out the _p_e_r_l_b_o_t manpage for other
     object tricks, traps, and tips.
























SunOS 5.4    Last change: Release 5.0 Patchlevel 01             6




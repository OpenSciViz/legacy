


PERLSYN(1)                User Commands                PERLSYN(1)



NAME
     perlsyn - Perl syntax

DESCRIPTION
     A Perl script consists of a sequence of declarations and
     statements.  The only things that need to be declared in
     Perl are report formats and subroutines.  See the sections
     below for more information on those declarations.  All
     uninitialized user-created objects are assumed to start with
     a null or 0 value until they are defined by some explicit
     operation such as assignment.  (Though you can get warnings
     about the use of undefined values if you like.)  The
     sequence of statements is executed just once, unlike in sed
     and awk scripts, where the sequence of statements is
     executed for each input line.  While this means that you
     must explicitly loop over the lines of your input file (or
     files), it also means you have much more control over which
     files and which lines you look at.  (Actually, I'm lying--it
     is possible to do an implicit loop with either the -n or -p
     switch.  It's just not the mandatory default like it is in
     sed and awk.)

     Perl is, for the most part, a free-form language.  (The only
     exception to this is format declarations, for obvious
     reasons.) Comments are indicated by the "#" character, and
     extend to the end of the line.  If you attempt to use /* */
     C-style comments, it will be interpreted either as division
     or pattern matching, depending on the context, and C++ //
     comments just look like a null regular expression, So don't
     do that.

     A declaration can be put anywhere a statement can, but has
     no effect on the execution of the primary sequence of
     statements--declarations all take effect at compile time.
     Typically all the declarations are put at the beginning or
     the end of the script.

     As of Perl 5, declaring a subroutine allows a subroutine
     name to be used as if it were a list operator from that
     point forward in the program.  You can declare a subroutine
     without defining it by saying just

         sub myname;
         $me = myname $0             or die "can't get myname";

     Note that it functions as a list operator though, not a
     unary operator, so be careful to use or instead of || there.

     Subroutines declarations can also be imported by a use
     statement.





SunOS 5.4    Last change: Release 5.0 Patchlevel 01             1






PERLSYN(1)                User Commands                PERLSYN(1)



     Also as of Perl 5, a statement sequence may contain
     declarations of lexically scoped variables, but apart from
     declaring a variable name, the declaration acts like an
     ordinary statement, and is elaborated within the sequence of
     statements as if it were an ordinary statement.

     Simple statements

     The only kind of simple statement is an expression evaluated
     for its side effects.  Every simple statement must be
     terminated with a semicolon, unless it is the final
     statement in a block, in which case the semicolon is
     optional.  (A semicolon is still encouraged there if the
     block takes up more than one line, since you may eventually
     add another line.)  Note that there are some operators like
     eval {} and do {} that look like compound statements, but
     aren't (they're just TERMs in an expression), and thus need
     an explicit termination if used as the last item in a
     statement.

     Any simple statement may optionally be followed by a _S_I_N_G_L_E
     modifier, just before the terminating semicolon (or block
     ending).  The possible modifiers are:

         if EXPR
         unless EXPR
         while EXPR
         until EXPR

     The if and unless modifiers have the expected semantics,
     presuming you're a speaker of English.  The while and until
     modifiers also have the usual "while loop" semantics
     (conditional evaluated first), except when applied to a do-
     BLOCK (or to the now-deprecated do-SUBROUTINE statement), in
     which case the block executes once before the conditional is
     evaluated.  This is so that you can write loops like:

         do {
             $_ = <STDIN>;
             ...
         } until $_ eq ".\n";

     See the do entry in the _p_e_r_l_f_u_n_c manpage.  Note also that
     the loop control statements described later will _N_O_T work in
     this construct, since modifiers don't take loop labels.
     Sorry.  You can always wrap another block around it to do
     that sort of thing.)

     Compound statements

     In Perl, a sequence of statements that defines a scope is
     called a block.  Sometimes a block is delimited by the file



SunOS 5.4    Last change: Release 5.0 Patchlevel 01             2






PERLSYN(1)                User Commands                PERLSYN(1)



     containing it (in the case of a required file, or the
     program as a whole), and sometimes a block is delimited by
     the extent of a string (in the case of an eval).

     But generally, a block is delimited by curly brackets, also
     known as braces.  We will call this syntactic construct a
     BLOCK.

     The following compound statements may be used to control
     flow:

         if (EXPR) BLOCK
         if (EXPR) BLOCK else BLOCK
         if (EXPR) BLOCK elsif (EXPR) BLOCK ... else BLOCK
         LABEL while (EXPR) BLOCK
         LABEL while (EXPR) BLOCK continue BLOCK
         LABEL for (EXPR; EXPR; EXPR) BLOCK
         LABEL foreach VAR (LIST) BLOCK
         LABEL BLOCK continue BLOCK

     Note that, unlike C and Pascal, these are defined in terms
     of BLOCKs, not statements.  This means that the curly
     brackets are _r_e_q_u_i_r_e_d--no dangling statements allowed.  If
     you want to write conditionals without curly brackets there
     are several other ways to do it.  The following all do the
     same thing:

         if (!open(FOO)) { die "Can't open $FOO: $!"; }
         die "Can't open $FOO: $!" unless open(FOO);
         open(FOO) or die "Can't open $FOO: $!";     # FOO or bust!
         open(FOO) ? 'hi mom' : die "Can't open $FOO: $!";
                             # a bit exotic, that last one

     The if statement is straightforward.  Since BLOCKs are
     always bounded by curly brackets, there is never any
     ambiguity about which if an else goes with.  If you use
     unless in place of if, the sense of the test is reversed.

     The while statement executes the block as long as the
     expression is true (does not evaluate to the null string or
     0 or "0").  The LABEL is optional, and if present, consists
     of an identifier followed by a colon.  The LABEL identifies
     the loop for the loop control statements next, last, and
     redo (see below).  If there is a continue BLOCK, it is
     always executed just before the conditional is about to be
     evaluated again, just like the third part of a for loop in
     C.  Thus it can be used to increment a loop variable, even
     when the loop has been continued via the next statement
     (which is similar to the C continue statement).

     If the word while is replaced by the word until, the sense
     of the test is reversed, but the conditional is still tested



SunOS 5.4    Last change: Release 5.0 Patchlevel 01             3






PERLSYN(1)                User Commands                PERLSYN(1)



     before the first iteration.

     In either the if or the while statement, you may replace
     "(EXPR)" with a BLOCK, and the conditional is true if the
     value of the last statement in that block is true.  (This
     feature continues to work in Perl 5 but is deprecated.
     Please change any occurrences of "if BLOCK" to "if (do
     BLOCK)".)

     The C-style for loop works exactly like the corresponding
     while loop:

         for ($i = 1; $i < 10; $i++) {
             ...
         }

     is the same as

         $i = 1;
         while ($i < 10) {
             ...
         } continue {
             $i++;
         }

     The foreach loop iterates over a normal list value and sets
     the variable VAR to be each element of the list in turn.
     The variable is implicitly local to the loop and regains its
     former value upon exiting the loop.  (If the variable was
     previously declared with my, it uses that variable instead
     of the global one, but it's still localized to the loop.)
     The foreach keyword is actually a synonym for the for
     keyword, so you can use foreach for readability or for for
     brevity.  If VAR is omitted, $_ is set to each value.  If
     LIST is an actual array (as opposed to an expression
     returning a list value), you can modify each element of the
     array by modifying VAR inside the loop.  Examples:

         for (@ary) { s/foo/bar/; }

         foreach $elem (@elements) {
             $elem *= 2;
         }

         for ((10,9,8,7,6,5,4,3,2,1,'BOOM')) {
             print $_, "\n"; sleep(1);
         }

         for (1..15) { print "Merry Christmas\n"; }






SunOS 5.4    Last change: Release 5.0 Patchlevel 01             4






PERLSYN(1)                User Commands                PERLSYN(1)



         foreach $item (split(/:[\\\n:]*/, $ENV{'TERMCAP'})) {
             print "Item: $item\n";
         }

     A BLOCK by itself (labeled or not) is semantically
     equivalent to a loop that executes once.  Thus you can use
     any of the loop control statements in it to leave or restart
     the block.  The continue block is optional.  This construct
     is particularly nice for doing case structures.

         SWITCH: {
             if (/^abc/) { $abc = 1; last SWITCH; }
             if (/^def/) { $def = 1; last SWITCH; }
             if (/^xyz/) { $xyz = 1; last SWITCH; }
             $nothing = 1;
         }

     There is no official switch statement in Perl, because there
     are already several ways to write the equivalent.  In
     addition to the above, you could write

         SWITCH: {
             $abc = 1, last SWITCH  if /^abc/;
             $def = 1, last SWITCH  if /^def/;
             $xyz = 1, last SWITCH  if /^xyz/;
             $nothing = 1;
         }

     (That's actually not as strange as it looks one you realize
     that you can use loop control "operators" within an
     expression,  That's just the normal C comma operator.)

     or

         SWITCH: {
             /^abc/ && do { $abc = 1; last SWITCH; };
             /^def/ && do { $def = 1; last SWITCH; };
             /^xyz/ && do { $xyz = 1; last SWITCH; };
             $nothing = 1;
         }

     or formatted so it stands out more as a "proper" switch
     statement:

         SWITCH: {
             /^abc/      && do {
                                 $abc = 1;
                                 last SWITCH;
                            };






SunOS 5.4    Last change: Release 5.0 Patchlevel 01             5






PERLSYN(1)                User Commands                PERLSYN(1)



             /^def/      && do {
                                 $def = 1;
                                 last SWITCH;
                            };

             /^xyz/      && do {
                                 $xyz = 1;
                                 last SWITCH;
                             };
             $nothing = 1;
         }

     or

         SWITCH: {
             /^abc/ and $abc = 1, last SWITCH;
             /^def/ and $def = 1, last SWITCH;
             /^xyz/ and $xyz = 1, last SWITCH;
             $nothing = 1;
         }

     or even, horrors,

         if (/^abc/)
             { $abc = 1 }
         elsif (/^def/)
             { $def = 1 }
         elsif (/^xyz/)
             { $xyz = 1 }
         else
             { $nothing = 1 }
























SunOS 5.4    Last change: Release 5.0 Patchlevel 01             6




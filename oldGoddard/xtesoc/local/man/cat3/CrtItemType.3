


Tk_CreateItemType(3)  Tk Library Procedures  Tk_CreateItemType(3)



_________________________________________________________________

NAME
     Tk_CreateItemType - define new kind of canvas item

SYNOPSIS
     #include <tk.h>

     Tk_CreateItemType(_t_y_p_e_P_t_r)

ARGUMENTS
     Tk_ItemType   *_t_y_p_e_P_t_r   (in)      Structure  that   defines
                                        the  new  type  of canvas
                                        item.
_________________________________________________________________


INTRODUCTION
     Tk_CreateItemType is invoked to define a new kind of  canvas
     item  described  by  the  _t_y_p_e_P_t_r  argument.   An  item type
     corresponds to a particular value of the  _t_y_p_e  argument  to
     the  create  widget  command for canvases, and the code that
     implements a canvas item type is called a _t_y_p_e _m_a_n_a_g_e_r.   Tk
     defines  several  built-in item types, such as rectangle and
     text and image, but Tk_CreateItemType allows additional item
     types  to  be  defined.  Once Tk_CreateItemType returns, the
     new item type may be used in new or existing canvas  widgets
     just like the built-in item types.

     You may find it easier to understand the rest of this manual
     entry  by  looking  at  the code for an existing canvas item
     type  such   as   bitmap   (file   tkCanvBmap.c)   or   text
     (tkCanvText.c).   The  easiest  way  to  create  a  new type
     manager is to copy the code for an existing type and  modify
     it for the new type.

     Tk provides a number of utility procedures for  the  use  of
     canvas   type   managers,   such   as   Tk_CanvasCoords  and
     Tk_CanvasPsColor; these are  described  in  separate  manual
     entries.


DATA STRUCTURES
     A type manager consists of a collection of  procedures  that
     provide  a standard set of operations on items of that type.
     The type manager deals with three kinds of data  structures.
     The  first  data  structure  is  a  Tk_ItemType; it contains
     information such as the name of the type and pointers to the
     standard procedures implemented by the type manager:
          typedef struct Tk_ItemType {
               char *_n_a_m_e;
               int _i_t_e_m_S_i_z_e;



Tk                      Last change: 4.0                        1






Tk_CreateItemType(3)  Tk Library Procedures  Tk_CreateItemType(3)



               Tk_ItemCreateProc *_c_r_e_a_t_e_P_r_o_c;
               Tk_ConfigSpec *_c_o_n_f_i_g_S_p_e_c_s;
               Tk_ItemConfigureProc *_c_o_n_f_i_g_P_r_o_c;
               Tk_ItemCoordProc *_c_o_o_r_d_P_r_o_c;
               Tk_ItemDeleteProc *_d_e_l_e_t_e_P_r_o_c;
               Tk_ItemDisplayProc *_d_i_s_p_l_a_y_P_r_o_c;
               int _a_l_w_a_y_s_R_e_d_r_a_w;
               Tk_ItemPointProc *_p_o_i_n_t_P_r_o_c;
               Tk_ItemAreaProc *_a_r_e_a_P_r_o_c;
               Tk_ItemPostscriptProc *_p_o_s_t_s_c_r_i_p_t_P_r_o_c;
               Tk_ItemScaleProc *_s_c_a_l_e_P_r_o_c;
               Tk_ItemTranslateProc *_t_r_a_n_s_l_a_t_e_P_r_o_c;
               Tk_ItemIndexProc *_i_n_d_e_x_P_r_o_c;
               Tk_ItemCursorProc *_i_c_u_r_s_o_r_P_r_o_c;
               Tk_ItemSelectionProc *_s_e_l_e_c_t_i_o_n_P_r_o_c;
               Tk_ItemInsertProc *_i_n_s_e_r_t_P_r_o_c;
               Tk_ItemDCharsProc *_d_C_h_a_r_s_P_r_o_c;
               Tk_ItemType *_n_e_x_t_P_t_r;
          } Tk_ItemType;

     The fields of a Tk_ItemType structure are described in  more
     detail  later  in this manual entry.  When Tk_CreateItemType
     is called, its _t_y_p_e_P_t_r argument must point  to  a  structure
     with  all of the fields initialized except _n_e_x_t_P_t_r, which is
     for Tk's internal use.  The structure must be  in  permanent
     memory (either statically allocated or dynamically allocated
     but never freed);  Tk retains a pointer to this structure.

     The second data structure manipulated by a type  manager  is
     an  _i_t_e_m _r_e_c_o_r_d.  For each item in a canvas there exists one
     item record.  All of the items of  a  given  type  generally
     have  item  records  with  the same structure, but different
     types usually have different formats for their item records.
     The  first part of each item record is a header with a stan-
     dard structure defined by Tk via the type Tk_Item;  the rest
     of  the  item record is defined by the type manager.  A type
     manager must define its item records with a Tk_Item  as  the
     first  field.  For example, the item record for bitmap items
     is defined as follows:
          typedef struct BitmapItem {
               Tk_Item _h_e_a_d_e_r;
               double _x, _y;
               Tk_Anchor _a_n_c_h_o_r;
               Pixmap _b_i_t_m_a_p;
               XColor *_f_g_C_o_l_o_r;
               XColor *_b_g_C_o_l_o_r;
               GC _g_c;
          } BitmapItem;
     The _h_e_a_d_e_r substructure contains information used by  Tk  to
     manage  the item, such as its identifer, its tags, its type,
     and its bounding box.  The fields starting with _x belong  to
     the  type  manager:   Tk will never read or write them.  The



Tk                      Last change: 4.0                        2






Tk_CreateItemType(3)  Tk Library Procedures  Tk_CreateItemType(3)



     type manager should not need to read or  write  any  of  the
     fields  in the header except for four fields whose names are
     _x_1, _y_1, _x_2, and _y_2.  These fields give a  bounding  box  for
     the items using integer canvas coordinates:  the item should
     not cover any pixels with x-coordinate lower than _x_1  or  y-
     coordinate  lower  than  _y_1,  nor should it cover any pixels
     with  x-coordinate  greater  than  or  equal  to  _x_2  or  y-
     coordinate  greater  than  or  equal to _y_2.  It is up to the
     type manager to keep the bounding box up to date as the item
     is moved and reconfigured.

     Whenever Tk calls a procedure in a type manager it passes in
     a  pointer to an item record.  The argument is always passed
     as a pointer to a Tk_Item;  the type manager will  typically
     cast  this  into a pointer to its own specific type, such as
     BitmapItem.

     The third data structure used  by  type  managers  has  type
     Tk_Canvas;   it  serves  as  an opaque handle for the canvas
     widget as a whole.  Type managers  need  not  know  anything
     about the contents of this structure.  A Tk_Canvas handle is
     typically passed in to the procedures of a type manager, and
     the  type  manager  can pass the handle back to library pro-
     cedures such as Tk_CanvasTkwin to  fetch  information  about
     the canvas.


NAME
     This section and the ones that follow describe each  of  the
     fields in a Tk_ItemType structure in detail.  The _n_a_m_e field
     provides  a  string  name   for   the   item   type.    Once
     Tk_CreateImageType  returns, this name may be used in create
     widget commands to create items of the new type.   If  there
     already  existed an item type by this name then the new item
     type replaces the old one.


ITEMSIZE
     _t_y_p_e_P_t_r->_i_t_e_m_S_i_z_e gives the size in bytes of item records of
     this  type, including the Tk_Item header.  Tk uses this size
     to allocate memory space for items of the type.  All of  the
     item  records  for a given type must have the same size.  If
     variable length fields are needed for an  item  (such  as  a
     list of points for a polygon), the type manager can allocate
     a separate object of variable length and keep a  pointer  to
     it in the item record.


CREATEPROC
     _t_y_p_e_P_t_r->_c_r_e_a_t_e_P_r_o_c points to a procedure  for  Tk  to  call
     whenever  a  new  item  of  this  type is created.  _t_y_p_e_P_t_r-
     >_c_r_e_a_t_e_P_r_o_c must match the following prototype:



Tk                      Last change: 4.0                        3






Tk_CreateItemType(3)  Tk Library Procedures  Tk_CreateItemType(3)



          typedef int Tk_ItemCreateProc(
               Tcl_Interp *_i_n_t_e_r_p,
               Tk_Canvas _c_a_n_v_a_s,
               Tk_Item *_i_t_e_m_P_t_r,
               int _a_r_g_c,
               char **_a_r_g_v);
     The _i_n_t_e_r_p argument is the interpreter in which the canvas's
     create  widget  command  was invoked, and _c_a_n_v_a_s is a handle
     for the canvas widget.  _i_t_e_m_P_t_r is a  pointer  to  a  newly-
     allocated  item  of  size _t_y_p_e_P_t_r->_i_t_e_m_S_i_z_e.  Tk has already
     initialized the item's header (the first sizeof(Tk_ItemType)
     bytes).   The  _a_r_g_c  and  _a_r_g_v arguments describe all of the
     arguments to the create command  after  the  _t_y_p_e  argument.
     For example, in the widget command

          .c create rectangle 10 20 50 50 -fill black

     _a_r_g_c will be 6 and _a_r_g_v[0] will contain the string 10.

     _c_r_e_a_t_e_P_r_o_c should use _a_r_g_c and _a_r_g_v to initialize the  type-
     specific  parts  of the item record and set an initial value
     for the bounding box in the item's header.  It should return
     a standard Tcl completion code and leave an error message in
     _i_n_t_e_r_p->_r_e_s_u_l_t if an error occurs.  If an  error  occurs  Tk
     will  free  the  item  record, so _c_r_e_a_t_e_P_r_o_c must be sure to
     leave the item record in a clean  state  if  it  returns  an
     error  (e.g.,  it  must  free  any additional memory that it
     allocated for the item).


CONFIGSPECS
     Each type manager must provide a standard  table  describing
     its  configuration  options, in a form suitable for use with
     Tk_ConfigureWidget.  This table will  normally  be  used  by
     _t_y_p_e_P_t_r->_c_r_e_a_t_e_P_r_o_c  and  _t_y_p_e_P_t_r->_c_o_n_f_i_g_P_r_o_c,  but  Tk also
     uses it directly  to  retrieve  option  information  in  the
     itemcget   and   itemconfigure  widget  commands.   _t_y_p_e_P_t_r-
     >_c_o_n_f_i_g_S_p_e_c_s must point to the configuration table for  this
     type.     Note:   Tk   provides   a   custom   option   type
     tk_CanvasTagsOption for implementing the -tags option;   see
     an  existing type manager for an example of how to use it in
     _c_o_n_f_i_g_S_p_e_c_s.


CONFIGPROC
     _t_y_p_e_P_t_r->_c_o_n_f_i_g_P_r_o_c is called by Tk whenever the itemconfig-
     ure  widget  command  is invoked to change the configuration
     options for a canvas item.  This procedure  must  match  the
     following prototype:
          typedef int Tk_ItemConfigureProc(
               Tcl_Interp *_i_n_t_e_r_p,
               Tk_Canvas _c_a_n_v_a_s,



Tk                      Last change: 4.0                        4






Tk_CreateItemType(3)  Tk Library Procedures  Tk_CreateItemType(3)



               Tk_Item *_i_t_e_m_P_t_r,
               int _a_r_g_c,
               char **_a_r_g_v,
               int _f_l_a_g_s);
     The _i_n_t_e_r_p argument identifies the interpreter in which  the
     widget command was invoked,  _c_a_n_v_a_s is a handle for the can-
     vas widget, and _i_t_e_m_P_t_r is a pointer to the item being  con-
     figured.   _a_r_g_c  and _a_r_g_v contain the configuration options.
     For example, if the following command is invoked:

          .c itemconfigure 2 -fill red -outline black

     _a_r_g_c is 4 and _a_r_g_v contains the strings -fill through black.
     _a_r_g_c will always be an even value.  The  _f_l_a_g_s argument con-
     tains flags to pass to  Tk_ConfigureWidget;  currently  this
     value   is   always   TK_CONFIG_ARGV_ONLY  when  Tk  invokes
     _t_y_p_e_P_t_r->_c_o_n_f_i_g_P_r_o_c, but the type manager's _c_r_e_a_t_e_P_r_o_c  pro-
     cedure  will  usually  invoke _c_o_n_f_i_g_P_r_o_c with different flag
     values.

     _t_y_p_e_P_t_r->_c_o_n_f_i_g_P_r_o_c returns a standard Tcl  completion  code
     and  leaves  an  error message in _i_n_t_e_r_p->_r_e_s_u_l_t if an error
     occurs.  It must update the item's bounding box  to  reflect
     the new configuration options.


COORDPROC
     _t_y_p_e_P_t_r->_c_o_o_r_d_P_r_o_c is invoked by Tk to implement the  coords
     widget  command  for  an  item.  It must match the following
     prototype:
          typedef int Tk_ItemCoordProc(
               Tcl_Interp *_i_n_t_e_r_p,
               Tk_Canvas _c_a_n_v_a_s,
               Tk_Item *_i_t_e_m_P_t_r,
               int _a_r_g_c,
               char **_a_r_g_v);
     The arguments _i_n_t_e_r_p, _c_a_n_v_a_s, and _i_t_e_m_P_t_r all have the stan-
     dard  meanings,  and  _a_r_g_c  and _a_r_g_v describe the coordinate
     arguments.  For example, if the following widget command  is
     invoked:

          .c coords 2 30 90

     _a_r_g_c will be 2 and argv will contain the  string  values  30
     and 90.

     The _c_o_o_r_d_P_r_o_c procedure should process the new  coordinates,
     update  the  item  appropriately  (e.g.,  it  must reset the
     bounding box in the item's header), and  return  a  standard
     Tcl  completion  code.   If  an error occurs, _c_o_o_r_d_P_r_o_c must
     leave an error message in _i_n_t_e_r_p->_r_e_s_u_l_t.




Tk                      Last change: 4.0                        5






Tk_CreateItemType(3)  Tk Library Procedures  Tk_CreateItemType(3)



DELETEPROC
     _t_y_p_e_P_t_r->_d_e_l_e_t_e_P_r_o_c is invoked by Tk to delete an  item  and
     free  any resources allocated to it.  It must match the fol-
     lowing prototype:
          typedef void Tk_ItemDeleteProc(
               Tk_Canvas _c_a_n_v_a_s,
               Tk_Item *_i_t_e_m_P_t_r,
               Display *_d_i_s_p_l_a_y);
     The _c_a_n_v_a_s and _i_t_e_m_P_t_r arguments have the usual  interpreta-
     tions,  and  _d_i_s_p_l_a_y identifies the X display containing the
     canvas.  _d_e_l_e_t_e_P_r_o_c must free up any resources allocated for
     the  item,  so that Tk can free the item record.  _d_e_l_e_t_e_P_r_o_c
     should not actually free the item record;  this will be done
     by Tk when _d_e_l_e_t_e_P_r_o_c returns.


DISPLAYPROC AND ALWAYSREDRAW
     _t_y_p_e_P_t_r->_d_i_s_p_l_a_y_P_r_o_c is invoked by Tk to redraw an  item  on
     the screen.  It must match the following prototype:
          typedef void Tk_ItemDisplayProc(
               Tk_Canvas _c_a_n_v_a_s,
               Tk_Item *_i_t_e_m_P_t_r,
               Display *_d_i_s_p_l_a_y,
               Drawable _d_s_t,
               int _x,
               int _y,
               int _w_i_d_t_h,
               int _h_e_i_g_h_t);
     The _c_a_n_v_a_s and _i_t_e_m_P_t_r arguments  have  the  usual  meaning.
     _d_i_s_p_l_a_y  identifies  the  display containing the canvas, and
     _d_s_t specifies a drawable in which the item  should  be  ren-
     dered; typically this is an off-screen pixmap, which Tk will
     copy into the canvas's window once all relevant  items  have
     been  drawn.   _x, _y, _w_i_d_t_h, and _h_e_i_g_h_t specify a rectangular
     region in canvas  coordinates,  which  is  the  area  to  be
     redrawn;  only  information that overlaps this area needs to
     be redrawn.  Tk will not call _d_i_s_p_l_a_y_P_r_o_c unless the  item's
     bounding  box overlaps the redraw area, but the type manager
     may wish to use the redraw area to optimize the redisplay of
     the item.

     Because of scrolling and the use of off-screen  pixmaps  for
     double-buffered  redisplay,  the  item's  coordinates in _d_s_t
     will not necessarily be the same as  those  in  the  canvas.
     _d_i_s_p_l_a_y_P_r_o_c should call Tk_CanvasDrawableCoords to transform
     coordinates from those of the canvas to those of _d_s_t.

     Normally an item's _d_i_s_p_l_a_y_P_r_o_c is only invoked if  the  item
     overlaps  the  area  being  displayed.  However, if _t_y_p_e_P_t_r-
     >_a_l_w_a_y_s_R_e_d_r_a_w has a  non-zero  value,  then  _d_i_s_p_l_a_y_P_r_o_c  is
     invoked  during  every redisplay operation, even if the item
     doesn't overlap the area of redisplay.  _a_l_w_a_y_s_R_e_d_r_a_w  should



Tk                      Last change: 4.0                        6






Tk_CreateItemType(3)  Tk Library Procedures  Tk_CreateItemType(3)



     normally  be set to 0;  it is only set to 1 in special cases
     such as window items that need to be unmapped when they  are
     off-screen.


POINTPROC
     _t_y_p_e_P_t_r->_p_o_i_n_t_P_r_o_c is invoked by Tk to find out how close  a
     given point is to a canvas item.  Tk uses this procedure for
     purposes such as locating the item under the mouse or  find-
     ing  the  closest item to a given point.  The procedure must
     match the following prototype:
          typedef double Tk_ItemPointProc(
               Tk_Canvas _c_a_n_v_a_s,
               Tk_Item *_i_t_e_m_P_t_r,
               double *_p_o_i_n_t_P_t_r);
     _c_a_n_v_a_s and _i_t_e_m_P_t_r have the usual meaning.  _p_o_i_n_t_P_t_r  points
     to an array of two numbers giving the x and y coordinates of
     a point.  _p_o_i_n_t_P_r_o_c must return a real value giving the dis-
     tance  from  the  point  to the item, or 0 if the point lies
     inside the item.


AREAPROC
     _t_y_p_e_P_t_r->_a_r_e_a_P_r_o_c is invoked by Tk to find out the relation-
     ship  between an item and a rectangular area.  It must match
     the following prototype:
          typedef int Tk_ItemAreaProc(
               Tk_Canvas _c_a_n_v_a_s,
               Tk_Item *_i_t_e_m_P_t_r,
               double *_r_e_c_t_P_t_r);
     _c_a_n_v_a_s and _i_t_e_m_P_t_r have the usual meaning.   _r_e_c_t_P_t_r  points
     to  an  array of four real numbers; the first two give the x
     and y coordinates of the upper left corner of  a  rectangle,
     and the second two give the x and y coordinates of the lower
     right corner.  _a_r_e_a_P_r_o_c must return - 1  if  the  item  lies
     entirely  outside  the  given  area,  0 if it lies partially
     inside and partially outside the area,  and  1  if  it  lies
     entirely inside the area.


POSTSCRIPTPROC
     _t_y_p_e_P_t_r->_p_o_s_t_s_c_r_i_p_t_P_r_o_c  is  invoked  by  Tk   to   generate
     Postcript  for an item during the postscript widget command.
     If the type manager is not capable of generating  Postscript
     then  _t_y_p_e_P_t_r->_p_o_s_t_s_c_r_i_p_t_P_r_o_c should be NULL.  The procedure
     must match the following prototype:
          typedef int Tk_ItemPostscriptProc(
               Tcl_Interp *_i_n_t_e_r_p,
               Tk_Canvas _c_a_n_v_a_s,
               Tk_Item *_i_t_e_m_P_t_r,
               int _p_r_e_p_a_s_s);
     The _i_n_t_e_r_p, _c_a_n_v_a_s, and _i_t_e_m_P_t_r arguments all have  standard



Tk                      Last change: 4.0                        7






Tk_CreateItemType(3)  Tk Library Procedures  Tk_CreateItemType(3)



     meanings;     _p_r_e_p_a_s_s   will   be   described   below.    If
     _p_o_s_t_s_c_r_i_p_t_P_r_o_c  completes  successfully,  it  should  append
     Postscript for the item to the information in _i_n_t_e_r_p->_r_e_s_u_l_t
     (e.g. by calling Tcl_AppendResult,  not  Tcl_SetResult)  and
     return  TCL_OK.   If  an error occurs, _p_o_s_t_s_c_r_i_p_t_P_r_o_c should
     clear the result and replace its contents with an error mes-
     sage;  then it should return TCL_ERROR.

     Tk provides a collection of utility procedures  to  simplify
     _p_o_s_t_s_c_r_i_p_t_P_r_o_c.  For example, Tk_CanvasPsColor will generate
     Postscript to set the current color to a given Tk color  and
     Tk_CanvasPsFont will set up font information.  When generat-
     ing Postscript, the type  manager  is  free  to  change  the
     graphics  state  of  the  Postscript  interpreter,  since Tk
     places gsave and grestore commands around the Postscript for
     the  item.   The  type  manager can use canvas x coordinates
     directly in its Postscript, but it must call Tk_CanvasPsY to
     convert  y  coordinates  from the space of the canvas (where
     the origin is at the upper left) to the space of  Postscript
     (where the origin is at the lower left).

     In order to generate Postscript that complies with the Adobe
     Document  Structuring  Conventions,  Tk  actually  generates
     Postscript  in   two   passes.    It   calls   each   item's
     _p_o_s_t_s_c_r_i_p_t_P_r_o_c  in each pass.  The only purpose of the first
     pass is to  collect  font  information  (which  is  done  by
     Tk_CanvPsFont);   the  actual  Postscript  is discarded.  Tk
     sets the _p_r_e_p_a_s_s argument to _p_o_s_t_s_c_r_i_p_t_P_r_o_c to 1 during  the
     first  pass;   the  type manager can use _p_r_e_p_a_s_s to skip all
     Postscript generation except for calls  to  Tk_CanvasPsFont.
     During  the  second  pass  _p_r_e_p_a_s_s  will  be  0, so the type
     manager must generate complete Postscript.


SCALEPROC
     _t_y_p_e_P_t_r->_s_c_a_l_e_P_r_o_c is invoked by Tk to rescale a canvas item
     during  the  scale widget command.  The procedure must match
     the following prototype:
          typedef void Tk_ItemScaleProc(
               Tk_Canvas _c_a_n_v_a_s,
               Tk_Item *_i_t_e_m_P_t_r,
               double _o_r_i_g_i_n_X,
               double _o_r_i_g_i_n_Y,
               double _s_c_a_l_e_X,
               double _s_c_a_l_e_Y);
     The _c_a_n_v_a_s and _i_t_e_m_P_t_r arguments  have  the  usual  meaning.
     _o_r_i_g_i_n_X  and _o_r_i_g_i_n_Y specify an origin relative to which the
     item is to be scaled, and _s_c_a_l_e_X and _s_c_a_l_e_Y give the x and y
     scale  factors.   The  item should adjust its coordinates so
     that a point in the item that used to have coordinates _x and
     _y will have new coordinates _x' and _y', where




Tk                      Last change: 4.0                        8






Tk_CreateItemType(3)  Tk Library Procedures  Tk_CreateItemType(3)



          _x' = _o_r_i_g_i_n_X  + _s_c_a_l_e_X*(_x-_o_r_i_g_i_n_X)
          _y' = _o_r_i_g_i_n_Y + _s_c_a_l_e_Y*(_y-_o_r_i_g_i_n_Y)

     _s_c_a_l_e_P_r_o_c must also update the bounding box  in  the  item's
     header.


TRANSLATEPROC
     _t_y_p_e_P_t_r->_t_r_a_n_s_l_a_t_e_P_r_o_c is invoked by Tk to translate a  can-
     vas item during the move widget command.  The procedure must
     match the following prototype:
          typedef void Tk_ItemTranslateProc(
               Tk_Canvas _c_a_n_v_a_s,
               Tk_Item *_i_t_e_m_P_t_r,
               double _d_e_l_t_a_X,
               double _d_e_l_t_a_Y);
     The _c_a_n_v_a_s and _i_t_e_m_P_t_r arguments have the usual meaning, and
     _d_e_l_t_a_X  and  _d_e_l_t_a_Y give the amounts that should be added to
     each x and y coordinate within the item.  The  type  manager
     should adjust the item's coordinates and update the bounding
     box in the item's header.


INDEXPROC
     _t_y_p_e_P_t_r->_i_n_d_e_x_P_r_o_c is invoked by Tk to  translate  a  string
     index specification into a numerical index, for example dur-
     ing the index widget command.  It is only relevant for  item
     types that support indexable text; _t_y_p_e_P_t_r->_i_n_d_e_x_P_r_o_c may be
     specified as NULL for non-textual item types.  The procedure
     must match the following prototype:
          typedef int Tk_ItemIndexProc(
               Tcl_Interp *_i_n_t_e_r_p,
               Tk_Canvas _c_a_n_v_a_s,
               Tk_Item *_i_t_e_m_P_t_r,
               char _i_n_d_e_x_S_t_r_i_n_g,
               int *_i_n_d_e_x_P_t_r);
     The _i_n_t_e_r_p, _c_a_n_v_a_s, and _i_t_e_m_P_t_r arguments all have the usual
     meaning.   _i_n_d_e_x_S_t_r_i_n_g  contains a textual description of an
     index, and _i_n_d_e_x_P_t_r points to an integer value  that  should
     be  filled  in with a numerical index.  It is up to the type
     manager to decide what forms of index are  supported  (e.g.,
     numbers,  insert,   sel.first, end, etc.).  _i_n_d_e_x_P_r_o_c should
     return a Tcl completion code and set _i_n_t_e_r_p->_r_e_s_u_l_t  in  the
     event of an error.


ICURSORPROC
     _t_y_p_e_P_t_r->_i_c_u_r_s_o_r_P_r_o_c is invoked by  Tk  during  the  icursor
     widget  command  to set the position of the insertion cursor
     in a textual item.  It is only relevant for item types  that
     support  an  insertion  cursor;  _t_y_p_e_P_t_r->_i_c_u_r_s_o_r_P_r_o_c may be
     specified as NULL for  item  types  that  don't  support  an



Tk                      Last change: 4.0                        9






Tk_CreateItemType(3)  Tk Library Procedures  Tk_CreateItemType(3)



     insertion  cursor.   The  procedure must match the following
     prototype:
          typedef void Tk_ItemIndexProc(
               Tk_Canvas _c_a_n_v_a_s,
               Tk_Item *_i_t_e_m_P_t_r,
               int _i_n_d_e_x);
     _c_a_n_v_a_s and _i_t_e_m_P_t_r have the usual meanings, and _i_n_d_e_x is  an
     index  into  the item's text, as returned by a previous call
     to _t_y_p_e_P_t_r->_i_n_s_e_r_t_P_r_o_c.  The type  manager  should  position
     the  insertion  cursor in the item just before the character
     given by _i_n_d_e_x.  Whether or  not  to  actually  display  the
     insertion cursor is determined by other information provided
     by Tk_CanvasGetTextInfo.


SELECTIONPROC
     _t_y_p_e_P_t_r->_s_e_l_e_c_t_i_o_n_P_r_o_c is invoked  by  Tk  during  selection
     retrievals;  it must return part or all of the selected text
     in the item (if any).  It is only relevant  for  item  types
     that  support  text; _t_y_p_e_P_t_r->_s_e_l_e_c_t_i_o_n_P_r_o_c may be specified
     as NULL for non-textual  item  types.   The  procedure  must
     match the following prototype:
          typedef int Tk_ItemSelectionProc(
               Tk_Canvas _c_a_n_v_a_s,
               Tk_Item *_i_t_e_m_P_t_r,
               int _o_f_f_s_e_t,
               char *_b_u_f_f_e_r,
               int _m_a_x_B_y_t_e_s);
     _c_a_n_v_a_s and _i_t_e_m_P_t_r have the usual meanings.   _o_f_f_s_e_t  is  an
     offset  in  bytes  into  the selection where 0 refers to the
     first byte of the selection;  it identifies the first  char-
     acter that is to be returned in this call.  _b_u_f_f_e_r points to
     an area of memory in which to store the requested bytes, and
     _m_a_x_B_y_t_e_s  specifies  the  maximum number of bytes to return.
     _s_e_l_e_c_t_i_o_n_P_r_o_c should extract up to _m_a_x_B_y_t_e_s characters  from
     the selection and copy them to _m_a_x_B_y_t_e_s;  it should return a
     count of the number of bytes actually copied, which  may  be
     less  than _m_a_x_B_y_t_e_s if there aren't _o_f_f_s_e_t+_m_a_x_B_y_t_e_s bytes in
     the selection.


INSERTPROC
     _t_y_p_e_P_t_r->_i_n_s_e_r_t_P_r_o_c is  invoked  by  Tk  during  the  insert
     widget command to insert new text into a canvas item.  It is
     only relevant for item types  that  support  text;  _t_y_p_e_P_t_r-
     >_i_n_s_e_r_t_P_r_o_c  may  be  specified as NULL for non-textual item
     types.  The procedure must match the following prototype:
          typedef void Tk_ItemInsertProc(
               Tk_Canvas _c_a_n_v_a_s,
               Tk_Item *_i_t_e_m_P_t_r,
               int _i_n_d_e_x,
               char *_s_t_r_i_n_g);



Tk                      Last change: 4.0                       10






Tk_CreateItemType(3)  Tk Library Procedures  Tk_CreateItemType(3)



     _c_a_n_v_a_s and _i_t_e_m_P_t_r have the usual  meanings.   _i_n_d_e_x  is  an
     index  into  the item's text, as returned by a previous call
     to _t_y_p_e_P_t_r->_i_n_s_e_r_t_P_r_o_c, and  _s_t_r_i_n_g  contains  new  text  to
     insert  just  before the character given by _i_n_d_e_x.  The type
     manager should insert the text and  recompute  the  bounding
     box in the item's header.


DCHARSPROC
     _t_y_p_e_P_t_r->_d_C_h_a_r_s_P_r_o_c is  invoked  by  Tk  during  the  dchars
     widget command to delete a range of text from a canvas item.
     It is only  relevant  for  item  types  that  support  text;
     _t_y_p_e_P_t_r->_d_C_h_a_r_s_P_r_o_c may be specified as NULL for non-textual
     item types.  The procedure must match the  following  proto-
     type:
          typedef void Tk_ItemDCharsProc(
               Tk_Canvas _c_a_n_v_a_s,
               Tk_Item *_i_t_e_m_P_t_r,
               int _f_i_r_s_t,
               int _l_a_s_t);
     _c_a_n_v_a_s and _i_t_e_m_P_t_r have the usual meanings.  _f_i_r_s_t and  _l_a_s_t
     give  the indices of the first and last bytes to be deleted,
     as returned by previous calls  to  _t_y_p_e_P_t_r->_i_n_d_e_x_P_r_o_c.   The
     type  manager  should  delete  the  specified characters and
     update the bounding box in the item's header.


SEE ALSO
     Tk_CanvasPsY, Tk_CanvasTextInfo, Tk_CanvasTkwin


KEYWORDS
     canvas, focus, item type, selection, type manager






















Tk                      Last change: 4.0                       11







Tcl_Interp(3)        Tcl Library Procedures         Tcl_Interp(3)



_________________________________________________________________

NAME
     Tcl_Interp - client-visible fields of interpreter structures

SYNOPSIS
     #include <tcl.h>

     typedef struct {
          char *_r_e_s_u_l_t;
          Tcl_FreeProc *_f_r_e_e_P_r_o_c;
          int _e_r_r_o_r_L_i_n_e;
     } Tcl_Interp;

     typedef void Tcl_FreeProc(char *_b_l_o_c_k_P_t_r);
_________________________________________________________________


DESCRIPTION
     The  Tcl_CreateInterp  procedure  returns  a  pointer  to  a
     Tcl_Interp  structure.   This  pointer  is  then passed into
     other Tcl procedures to process commands in the  interpreter
     and  perform  other  operations  on the interpreter.  Inter-
     preter structures contain many many fields that are used  by
     Tcl,  but  only  three  that  may  be  accessed  by clients:
     _r_e_s_u_l_t, _f_r_e_e_P_r_o_c, and _e_r_r_o_r_L_i_n_e.

     The _r_e_s_u_l_t and _f_r_e_e_P_r_o_c fields are used to return results or
     error  messages from commands.  This information is returned
     by command procedures back to Tcl_Eval, and by Tcl_Eval back
     to  its callers.  The _r_e_s_u_l_t field points to the string that
     represents the result or error  message,  and  the  _f_r_e_e_P_r_o_c
     field  tells  how  to  dispose of the storage for the string
     when it isn't needed anymore.  The easiest way  for  command
     procedures  to manipulate these fields is to call procedures
     like Tcl_SetResult or Tcl_AppendResult;  they will hide  all
     the  details  of managing the fields.  The description below
     is for those procedures that manipulate the fields directly.

     Whenever a command procedure returns, it  must  ensure  that
     the  _r_e_s_u_l_t  field  of  its interpreter points to the string
     being returned by the command.  The _r_e_s_u_l_t field must always
     point  to  a valid string.  If a command wishes to return no
     result then _i_n_t_e_r_p->_r_e_s_u_l_t should point to an empty  string.
     Normally,  results  are  assumed to be statically allocated,
     which means that the contents will  not  change  before  the
     next time Tcl_Eval is called or some other command procedure
     is invoked.  In this case, the _f_r_e_e_P_r_o_c field must be  zero.
     Alternatively,  a command procedure may dynamically allocate
     its return value (e.g. using malloc) and store a pointer  to
     it  in  _i_n_t_e_r_p->_r_e_s_u_l_t.  In this case, the command procedure
     must also set _i_n_t_e_r_p->_f_r_e_e_P_r_o_c to the address of a procedure



Tcl                      Last change:                           1






Tcl_Interp(3)        Tcl Library Procedures         Tcl_Interp(3)



     that can free the value (usually free).  If _i_n_t_e_r_p->_f_r_e_e_P_r_o_c
     is non-zero, then Tcl will call _f_r_e_e_P_r_o_c to free  the  space
     pointed to by _i_n_t_e_r_p->_r_e_s_u_l_t before it invokes the next com-
     mand.  If a client procedure overwrites _i_n_t_e_r_p->_r_e_s_u_l_t  when
     _i_n_t_e_r_p->_f_r_e_e_P_r_o_c  is  non-zero,  then  it is responsible for
     calling  _f_r_e_e_P_r_o_c  to  free  the  old  _i_n_t_e_r_p->_r_e_s_u_l_t   (the
     Tcl_FreeResult macro should be used for this purpose).

     _F_r_e_e_P_r_o_c should have arguments and  result  that  match  the
     Tcl_FreeProc  declaration above:  it receives a single argu-
     ment which is a pointer to the result  value  to  free.   In
     most  applications free is the only non-zero value ever used
     for _f_r_e_e_P_r_o_c.  However, an application may store a different
     procedure  address  in _f_r_e_e_P_r_o_c in order to use an alternate
     memory allocator or in order to do other  cleanup  when  the
     result memory is freed.

     As part of processing  each  command,  Tcl_Eval  initializes
     _i_n_t_e_r_p->_r_e_s_u_l_t  and _i_n_t_e_r_p->_f_r_e_e_P_r_o_c just before calling the
     command procedure for the command.  The _f_r_e_e_P_r_o_c field  will
     be  initialized to zero, and _i_n_t_e_r_p->_r_e_s_u_l_t will point to an
     empty string.  Commands that do not  return  any  value  can
     simply  leave  the  fields  alone.   Furthermore,  the empty
     string pointed to by _r_e_s_u_l_t is actually part of an array  of
     TCL_RESULT_SIZE  characters  (approximately 200).  If a com-
     mand wishes to return a short string, it can simply copy  it
     to  the  area  pointed to by _i_n_t_e_r_p->_r_e_s_u_l_t.  Or, it can use
     the sprintf procedure to generate a short result  string  at
     the location pointed to by _i_n_t_e_r_p->_r_e_s_u_l_t.

     It is a general convention in  Tcl-based  applications  that
     the  result of an interpreter is normally in the initialized
     state described in the previous paragraph.  Procedures  that
     manipulate  an  interpreter's  result  (e.g. by returning an
     error) will generally assume that the result has  been  ini-
     tialized  when the procedure is called.  If such a procedure
     is to be called after the  result  has  been  changed,  then
     Tcl_ResetResult  should  be called first to reset the result
     to its initialized state.

     The _e_r_r_o_r_L_i_n_e field is valid only after Tcl_Eval  returns  a
     TCL_ERROR  return  code.   In  this  situation the _e_r_r_o_r_L_i_n_e
     field identifies the line number of the command  being  exe-
     cuted  when  the error occurred.  The line numbers are rela-
     tive to the command being executed:  1 means the first  line
     of  the command passed to Tcl_Eval, 2 means the second line,
     and so on.  The _e_r_r_o_r_L_i_n_e field is typically  used  in  con-
     junction  with  Tcl_AddErrorInfo to report information about
     where an error occurred.  _E_r_r_o_r_L_i_n_e should not  normally  be
     modified except by Tcl_Eval.





Tcl                      Last change:                           2






Tcl_Interp(3)        Tcl Library Procedures         Tcl_Interp(3)



KEYWORDS
     free, initialized, interpreter, malloc, result





















































Tcl                      Last change:                           3




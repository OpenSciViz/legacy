.TH SSHD 8 "September 25, 1999" ""
.SH NAME
\fBsshd\fP
\- OpenSSH SSH daemon
.SH SYNOPSIS
.br
\fBsshd\fP
Bk -words
[\fB\-deiqtD46\fP]
[\fB\-b\fP \fIbits\fP]
[\fB\-f\fP \fIconfig_file\fP]
[\fB\-g\fP \fIlogin_grace_time\fP]
[\fB\-h\fP \fIhost_key_file\fP]
[\fB\-k\fP \fIkey_gen_time\fP]
[\fB\-o\fP \fIoption\fP]
[\fB\-p\fP \fIport\fP]
[\fB\-u\fP \fIlen\fP]
Ek
.SH DESCRIPTION
\fBsshd\fP
(SSH Daemon) is the daemon program for
\fBssh\fP(1).
Together these programs replace rlogin and rsh, and
provide secure encrypted communications between two untrusted hosts
over an insecure network.
The programs are intended to be as easy to
install and use as possible.

\fBsshd\fP
is the daemon that listens for connections from clients.
It is normally started at boot from
\fI/etc/rc\fP.
It forks a new
daemon for each incoming connection.
The forked daemons handle
key exchange, encryption, authentication, command execution,
and data exchange.
This implementation of
\fBsshd\fP
supports both SSH protocol version 1 and 2 simultaneously.
\fBsshd\fP
works as follows:

.SS SSH protocol version 1

Each host has a host-specific RSA key
(normally 1024 bits) used to identify the host.
Additionally, when
the daemon starts, it generates a server RSA key (normally 768 bits).
This key is normally regenerated every hour if it has been used, and
is never stored on disk.

Whenever a client connects, the daemon responds with its public
host and server keys.
The client compares the
RSA host key against its own database to verify that it has not changed.
The client then generates a 256 bit random number.
It encrypts this
random number using both the host key and the server key, and sends
the encrypted number to the server.
Both sides then use this
random number as a session key which is used to encrypt all further
communications in the session.
The rest of the session is encrypted
using a conventional cipher, currently Blowfish or 3DES, with 3DES
being used by default.
The client selects the encryption algorithm
to use from those offered by the server.

Next, the server and the client enter an authentication dialog.
The client tries to authenticate itself using
\fI\&.rhosts\fP
authentication,
\fI\&.rhosts\fP
authentication combined with RSA host
authentication, RSA challenge-response authentication, or password
based authentication.

Regardless of the authentication type, the account is checked to
ensure that it is accessible.  An account is not accessible if it is
locked, listed in
\fBDenyUsers\fP
or its group is listed in
\fBDenyGroups\fP
\&.  The definition of a locked account is system dependant. Some platforms
have their own account database (eg AIX) and some modify the passwd field (
`\&*LK\&*'
on Solaris,
`\&*'
on HP-UX, containing
`Nologin'
on Tru64 and a leading
`\&!!'
on Linux).  If there is a requirement to disable password authentication
for the account while allowing still public-key, then the passwd field
should be set to something other than these values (eg
`NP'
or
`\&*NP\&*'
).

Rhosts authentication is normally disabled
because it is fundamentally insecure, but can be enabled in the server
configuration file if desired.
System security is not improved unless
\fBrshd\fP,
\fBrlogind\fP,
and
\fBrexecd\fP
are disabled (thus completely disabling
\fBrlogin\fP()
and
\fBrsh\fP()
into the machine).

.SS SSH protocol version 2

Version 2 works similarly:
Each host has a host-specific key (RSA or DSA) used to identify the host.
However, when the daemon starts, it does not generate a server key.
Forward security is provided through a Diffie-Hellman key agreement.
This key agreement results in a shared session key.

The rest of the session is encrypted using a symmetric cipher, currently
128 bit AES, Blowfish, 3DES, CAST128, Arcfour, 192 bit AES, or 256 bit AES.
The client selects the encryption algorithm
to use from those offered by the server.
Additionally, session integrity is provided
through a cryptographic message authentication code
(hmac-sha1 or hmac-md5).

Protocol version 2 provides a public key based
user (PubkeyAuthentication) or
client host (HostbasedAuthentication) authentication method,
conventional password authentication and challenge response based methods.

.SS Command execution and data forwarding

If the client successfully authenticates itself, a dialog for
preparing the session is entered.
At this time the client may request
things like allocating a pseudo-tty, forwarding X11 connections,
forwarding TCP/IP connections, or forwarding the authentication agent
connection over the secure channel.

Finally, the client either requests a shell or execution of a command.
The sides then enter session mode.
In this mode, either side may send
data at any time, and such data is forwarded to/from the shell or
command on the server side, and the user terminal in the client side.

When the user program terminates and all forwarded X11 and other
connections have been closed, the server sends command exit status to
the client, and both sides exit.

\fBsshd\fP
can be configured using command-line options or a configuration
file.
Command-line options override values specified in the
configuration file.

\fBsshd\fP
rereads its configuration file when it receives a hangup signal,
.BR SIGHUP ,
by executing itself with the name it was started as, i.e.,
\fI/usr/sbin/sshd\fP.

The options are as follows:
.TP
\fB\-b\fP \fIbits\fP
Specifies the number of bits in the ephemeral protocol version 1
server key (default 768).
.TP
\fB\-d\fP
Debug mode.
The server sends verbose debug output to the system
log, and does not put itself in the background.
The server also will not fork and will only process one connection.
This option is only intended for debugging for the server.
Multiple
\fB\-d\fP
options increase the debugging level.
Maximum is 3.
.TP
\fB\-e\fP
When this option is specified,
\fBsshd\fP
will send the output to the standard error instead of the system log.
.TP
\fB\-f\fP \fIconfiguration_file\fP
Specifies the name of the configuration file.
The default is
\fI/etc/ssh/sshd_config\fP.
\fBsshd\fP
refuses to start if there is no configuration file.
.TP
\fB\-g\fP \fIlogin_grace_time\fP
Gives the grace time for clients to authenticate themselves (default
120 seconds).
If the client fails to authenticate the user within
this many seconds, the server disconnects and exits.
A value of zero indicates no limit.
.TP
\fB\-h\fP \fIhost_key_file\fP
Specifies a file from which a host key is read.
This option must be given if
\fBsshd\fP
is not run as root (as the normal
host key files are normally not readable by anyone but root).
The default is
\fI/etc/ssh/ssh_host_key\fP
for protocol version 1, and
\fI/etc/ssh/ssh_host_rsa_key\fP
and
\fI/etc/ssh/ssh_host_dsa_key\fP
for protocol version 2.
It is possible to have multiple host key files for
the different protocol versions and host key algorithms.
.TP
\fB\-i\fP
Specifies that
\fBsshd\fP
is being run from
\fBinetd\fP(8).
\fBsshd\fP
is normally not run
from inetd because it needs to generate the server key before it can
respond to the client, and this may take tens of seconds.
Clients would have to wait too long if the key was regenerated every time.
However, with small key sizes (e.g., 512) using
\fBsshd\fP
from inetd may
be feasible.
.TP
\fB\-k\fP \fIkey_gen_time\fP
Specifies how often the ephemeral protocol version 1 server key is
regenerated (default 3600 seconds, or one hour).
The motivation for regenerating the key fairly
often is that the key is not stored anywhere, and after about an hour,
it becomes impossible to recover the key for decrypting intercepted
communications even if the machine is cracked into or physically
seized.
A value of zero indicates that the key will never be regenerated.
.TP
\fB\-o\fP \fIoption\fP
Can be used to give options in the format used in the configuration file.
This is useful for specifying options for which there is no separate
command-line flag.
.TP
\fB\-p\fP \fIport\fP
Specifies the port on which the server listens for connections
(default 22).
Multiple port options are permitted.
Ports specified in the configuration file are ignored when a
command-line port is specified.
.TP
\fB\-q\fP
Quiet mode.
Nothing is sent to the system log.
Normally the beginning,
authentication, and termination of each connection is logged.
.TP
\fB\-t\fP
Test mode.
Only check the validity of the configuration file and sanity of the keys.
This is useful for updating
\fBsshd\fP
reliably as configuration options may change.
.TP
\fB\-u\fP \fIlen\fP
This option is used to specify the size of the field
in the
utmp
structure that holds the remote host name.
If the resolved host name is longer than
\fIlen\fP,
the dotted decimal value will be used instead.
This allows hosts with very long host names that
overflow this field to still be uniquely identified.
Specifying
\fB\-u0\fP
indicates that only dotted decimal addresses
should be put into the
\fIutmp\fP
file.
\fB\-u0\fP
may also be used to prevent
\fBsshd\fP
from making DNS requests unless the authentication
mechanism or configuration requires it.
Authentication mechanisms that may require DNS include
\fBRhostsRSAAuthentication\fP,
\fBHostbasedAuthentication\fP
and using a
\fBfrom="pattern-list"\fP
option in a key file.
Configuration options that require DNS include using a
USER@HOST pattern in
\fBAllowUsers\fP
or
\fBDenyUsers\fP.
.TP
\fB\-D\fP
When this option is specified
\fBsshd\fP
will not detach and does not become a daemon.
This allows easy monitoring of
\fBsshd\fP.
.TP
\fB\-4\fP
Forces
\fBsshd\fP
to use IPv4 addresses only.
.TP
\fB\-6\fP
Forces
\fBsshd\fP
to use IPv6 addresses only.
.SH CONFIGURATION FILE
\fBsshd\fP
reads configuration data from
\fI/etc/ssh/sshd_config\fP
(or the file specified with
\fB\-f\fP
on the command line).
The file format and configuration options are described in
\fBsshd_config\fP(5).
.SH LOGIN PROCESS
When a user successfully logs in,
\fBsshd\fP
does the following:
.IP 1.
If the login is on a tty, and no command has been specified,
prints last login time and
\fI/etc/motd\fP
(unless prevented in the configuration file or by
\fI$HOME/.hushlogin\fP;
see the
.B FILES
section).
.IP 2.
If the login is on a tty, records login time.
.IP 3.
Checks
\fI/etc/nologin\fP;
if it exists, prints contents and quits
(unless root).
.IP 4.
Changes to run with normal user privileges.
.IP 5.
Sets up basic environment.
.IP 6.
Reads
\fI$HOME/.ssh/environment\fP
if it exists and users are allowed to change their environment.
See the
\fBPermitUserEnvironment\fP
option in
\fBsshd_config\fP(5).
.IP 7.
Changes to user's home directory.
.IP 8.
If
\fI$HOME/.ssh/rc\fP
exists, runs it; else if
\fI/etc/ssh/sshrc\fP
exists, runs
it; otherwise runs xauth.
The
``rc''
files are given the X11
authentication protocol and cookie in standard input.
.IP 9.
Runs user's shell or command.
.SH AUTHORIZED_KEYS FILE FORMAT
\fI$HOME/.ssh/authorized_keys\fP
is the default file that lists the public keys that are
permitted for RSA authentication in protocol version 1
and for public key authentication (PubkeyAuthentication)
in protocol version 2.
\fBAuthorizedKeysFile\fP
may be used to specify an alternative file.

Each line of the file contains one
key (empty lines and lines starting with a
`#'
are ignored as
comments).
Each RSA public key consists of the following fields, separated by
spaces: options, bits, exponent, modulus, comment.
Each protocol version 2 public key consists of:
options, keytype, base64 encoded key, comment.
The options field
is optional; its presence is determined by whether the line starts
with a number or not (the options field never starts with a number).
The bits, exponent, modulus and comment fields give the RSA key for
protocol version 1; the
comment field is not used for anything (but may be convenient for the
user to identify the key).
For protocol version 2 the keytype is
``ssh-dss''
or
``ssh-rsa''.

Note that lines in this file are usually several hundred bytes long
(because of the size of the public key encoding).
You don't want to type them in; instead, copy the
\fIidentity.pub\fP,
\fIid_dsa.pub\fP
or the
\fIid_rsa.pub\fP
file and edit it.

\fBsshd\fP
enforces a minimum RSA key modulus size for protocol 1
and protocol 2 keys of 768 bits.

The options (if present) consist of comma-separated option
specifications.
No spaces are permitted, except within double quotes.
The following option specifications are supported (note
that option keywords are case-insensitive):
.TP
\fBfrom="pattern-list"\fP
Specifies that in addition to public key authentication, the canonical name
of the remote host must be present in the comma-separated list of
patterns
(`\&*'
and
`\&?'
serve as wildcards).
The list may also contain
patterns negated by prefixing them with
`\&!' ;
if the canonical host name matches a negated pattern, the key is not accepted.
The purpose
of this option is to optionally increase security: public key authentication
by itself does not trust the network or name servers or anything (but
the key); however, if somebody somehow steals the key, the key
permits an intruder to log in from anywhere in the world.
This additional option makes using a stolen key more difficult (name
servers and/or routers would have to be compromised in addition to
just the key).
.TP
\fBcommand="command"\fP
Specifies that the command is executed whenever this key is used for
authentication.
The command supplied by the user (if any) is ignored.
The command is run on a pty if the client requests a pty;
otherwise it is run without a tty.
If an 8-bit clean channel is required,
one must not request a pty or should specify
\fBno-pty\fP.
A quote may be included in the command by quoting it with a backslash.
This option might be useful
to restrict certain public keys to perform just a specific operation.
An example might be a key that permits remote backups but nothing else.
Note that the client may specify TCP/IP and/or X11
forwarding unless they are explicitly prohibited.
Note that this option applies to shell, command or subsystem execution.
.TP
\fBenvironment="NAME=value"\fP
Specifies that the string is to be added to the environment when
logging in using this key.
Environment variables set this way
override other default environment values.
Multiple options of this type are permitted.
Environment processing is disabled by default and is
controlled via the
\fBPermitUserEnvironment\fP
option.
This option is automatically disabled if
\fBUseLogin\fP
is enabled.
.TP
\fBno-port-forwarding\fP
Forbids TCP/IP forwarding when this key is used for authentication.
Any port forward requests by the client will return an error.
This might be used, e.g., in connection with the
\fBcommand\fP
option.
.TP
\fBno-X11-forwarding\fP
Forbids X11 forwarding when this key is used for authentication.
Any X11 forward requests by the client will return an error.
.TP
\fBno-agent-forwarding\fP
Forbids authentication agent forwarding when this key is used for
authentication.
.TP
\fBno-pty\fP
Prevents tty allocation (a request to allocate a pty will fail).
.TP
\fBpermitopen="host:port"\fP
Limit local
``ssh -L''
port forwarding such that it may only connect to the specified host and
port.
IPv6 addresses can be specified with an alternative syntax:
\fIhost/port\fP.
Multiple
\fBpermitopen\fP
options may be applied separated by commas.
No pattern matching is performed on the specified hostnames,
they must be literal domains or addresses.
.SS Examples
1024 33 12121.\|.\|.\|312314325 ylo@foo.bar

from="*.niksula.hut.fi,!pc.niksula.hut.fi" 1024 35 23.\|.\|.\|2334 ylo@niksula

command="dump /home",no-pty,no-port-forwarding 1024 33 23.\|.\|.\|2323 backup.hut.fi

permitopen="10.2.1.55:80",permitopen="10.2.1.56:25" 1024 33 23.\|.\|.\|2323
.SH SSH_KNOWN_HOSTS FILE FORMAT
The
\fI/etc/ssh/ssh_known_hosts\fP
and
\fI$HOME/.ssh/known_hosts\fP
files contain host public keys for all known hosts.
The global file should
be prepared by the administrator (optional), and the per-user file is
maintained automatically: whenever the user connects from an unknown host
its key is added to the per-user file.

Each line in these files contains the following fields: hostnames,
bits, exponent, modulus, comment.
The fields are separated by spaces.

Hostnames is a comma-separated list of patterns
(`\&*'
and
`\&?'
act as
wildcards); each pattern in turn is matched against the canonical host
name (when authenticating a client) or against the user-supplied
name (when authenticating a server).
A pattern may also be preceded by
`\&!'
to indicate negation: if the host name matches a negated
pattern, it is not accepted (by that line) even if it matched another
pattern on the line.

Bits, exponent, and modulus are taken directly from the RSA host key; they
can be obtained, e.g., from
\fI/etc/ssh/ssh_host_key.pub\fP.
The optional comment field continues to the end of the line, and is not used.

Lines starting with
`#'
and empty lines are ignored as comments.

When performing host authentication, authentication is accepted if any
matching line has the proper key.
It is thus permissible (but not
recommended) to have several lines or different host keys for the same
names.
This will inevitably happen when short forms of host names
from different domains are put in the file.
It is possible
that the files contain conflicting information; authentication is
accepted if valid information can be found from either file.

Note that the lines in these files are typically hundreds of characters
long, and you definitely don't want to type in the host keys by hand.
Rather, generate them by a script
or by taking
\fI/etc/ssh/ssh_host_key.pub\fP
and adding the host names at the front.
.SS Examples

closenet,.\|.\|.\|,130.233.208.41 1024 37 159.\|.\|.93 closenet.hut.fi
.br
cvs.openbsd.org,199.185.137.3 ssh-rsa AAAA1234.....=
.br
.SH FILES
.TP
.B /etc/ssh/sshd_config
Contains configuration data for
\fBsshd\fP.
The file format and configuration options are described in
\fBsshd_config\fP(5).
.TP
.B /etc/ssh/ssh_host_key, /etc/ssh/ssh_host_dsa_key, /etc/ssh/ssh_host_rsa_key
These three files contain the private parts of the host keys.
These files should only be owned by root, readable only by root, and not
accessible to others.
Note that
\fBsshd\fP
does not start if this file is group/world-accessible.
.TP
.B /etc/ssh/ssh_host_key.pub, /etc/ssh/ssh_host_dsa_key.pub, /etc/ssh/ssh_host_rsa_key.pub
These three files contain the public parts of the host keys.
These files should be world-readable but writable only by
root.
Their contents should match the respective private parts.
These files are not
really used for anything; they are provided for the convenience of
the user so their contents can be copied to known hosts files.
These files are created using
\fBssh-keygen\fP(1).
.TP
.B /etc/moduli
Contains Diffie-Hellman groups used for the "Diffie-Hellman Group Exchange".
The file format is described in
\fBmoduli\fP(5).
.TP
.B /var/empty
\fBchroot\fP(2)
directory used by
\fBsshd\fP
during privilege separation in the pre-authentication phase.
The directory should not contain any files and must be owned by root
and not group or world-writable.
.TP
.B /etc/ssh/sshd.pid
Contains the process ID of the
\fBsshd\fP
listening for connections (if there are several daemons running
concurrently for different ports, this contains the process ID of the one
started last).
The content of this file is not sensitive; it can be world-readable.
.TP
.B $HOME/.ssh/authorized_keys
Lists the public keys (RSA or DSA) that can be used to log into the user's account.
This file must be readable by root (which may on some machines imply
it being world-readable if the user's home directory resides on an NFS
volume).
It is recommended that it not be accessible by others.
The format of this file is described above.
Users will place the contents of their
\fIidentity.pub\fP,
\fIid_dsa.pub\fP
and/or
\fIid_rsa.pub\fP
files into this file, as described in
\fBssh-keygen\fP(1).
.TP
.B "/etc/ssh/ssh_known_hosts" and "$HOME/.ssh/known_hosts"
These files are consulted when using rhosts with RSA host
authentication or protocol version 2 hostbased authentication
to check the public key of the host.
The key must be listed in one of these files to be accepted.
The client uses the same files
to verify that it is connecting to the correct remote host.
These files should be writable only by root/the owner.
\fI/etc/ssh/ssh_known_hosts\fP
should be world-readable, and
\fI$HOME/.ssh/known_hosts\fP
can, but need not be, world-readable.
.TP
.B /etc/nologin
If this file exists,
\fBsshd\fP
refuses to let anyone except root log in.
The contents of the file
are displayed to anyone trying to log in, and non-root connections are
refused.
The file should be world-readable.
.TP
.B /etc/hosts.allow, /etc/hosts.deny
Access controls that should be enforced by tcp-wrappers are defined here.
Further details are described in
\fBhosts_access\fP(5).
.TP
.B $HOME/.rhosts
This file contains host-username pairs, separated by a space, one per
line.
The given user on the corresponding host is permitted to log in
without a password.
The same file is used by rlogind and rshd.
The file must
be writable only by the user; it is recommended that it not be
accessible by others.

If is also possible to use netgroups in the file.
Either host or user
name may be of the form +@groupname to specify all hosts or all users
in the group.
.TP
.B $HOME/.shosts
For ssh,
this file is exactly the same as for
\fI\&.rhosts\fP.
However, this file is
not used by rlogin and rshd, so using this permits access using SSH only.
.TP
.B /etc/hosts.equiv
This file is used during
\fI\&.rhosts\fP
authentication.
In the simplest form, this file contains host names, one per line.
Users on
those hosts are permitted to log in without a password, provided they
have the same user name on both machines.
The host name may also be
followed by a user name; such users are permitted to log in as
.IR any
user on this machine (except root).
Additionally, the syntax
``+@group''
can be used to specify netgroups.
Negated entries start with
`\&-'.

If the client host/user is successfully matched in this file, login is
automatically permitted provided the client and server user names are the
same.
Additionally, successful RSA host authentication is normally required.
This file must be writable only by root; it is recommended
that it be world-readable.

.B "Warning: It is almost never a good idea to use user names in"
\fIhosts.equiv\fP.
Beware that it really means that the named user(s) can log in as
.IR anybody ,
which includes bin, daemon, adm, and other accounts that own critical
binaries and directories.
Using a user name practically grants the user root access.
The only valid use for user names that I can think
of is in negative entries.

Note that this warning also applies to rsh/rlogin.
.TP
.B /etc/ssh/shosts.equiv
This is processed exactly as
\fI/etc/hosts.equiv\fP.
However, this file may be useful in environments that want to run both
rsh/rlogin and ssh.
.TP
.B $HOME/.ssh/environment
This file is read into the environment at login (if it exists).
It can only contain empty lines, comment lines (that start with
`#' ) ,
and assignment lines of the form name=value.
The file should be writable
only by the user; it need not be readable by anyone else.
Environment processing is disabled by default and is
controlled via the
\fBPermitUserEnvironment\fP
option.
.TP
.B $HOME/.ssh/rc
If this file exists, it is run with
\fI/bin/sh\fP
after reading the
environment files but before starting the user's shell or command.
It must not produce any output on stdout; stderr must be used
instead.
If X11 forwarding is in use, it will receive the "proto cookie" pair in
its standard input (and
.IR DISPLAY
in its environment).
The script must call
\fBxauth\fP(1)
because
\fBsshd\fP
will not run xauth automatically to add X11 cookies.

The primary purpose of this file is to run any initialization routines
which may be needed before the user's home directory becomes
accessible; AFS is a particular example of such an environment.

This file will probably contain some initialization code followed by
something similar to:

if read proto cookie && [ -n "$DISPLAY" ]; then
.br
	if [ `echo $DISPLAY | cut -c1-10` = 'localhost:' ]; then
.br
		# X11UseLocalhost=yes
.br
		echo add unix:`echo $DISPLAY |
.br
		    cut -c11-` $proto $cookie
.br
	else
.br
		# X11UseLocalhost=no
.br
		echo add $DISPLAY $proto $cookie
.br
	fi | xauth -q -
.br
fi
.br

If this file does not exist,
\fI/etc/ssh/sshrc\fP
is run, and if that
does not exist either, xauth is used to add the cookie.

This file should be writable only by the user, and need not be
readable by anyone else.
.TP
.B /etc/ssh/sshrc
Like
\fI$HOME/.ssh/rc\fP.
This can be used to specify
machine-specific login-time initializations globally.
This file should be writable only by root, and should be world-readable.
.SH SEE ALSO
\fBscp\fP(1),
\fBsftp\fP(1),
\fBssh\fP(1),
\fBssh-add\fP(1),
\fBssh-agent\fP(1),
\fBssh-keygen\fP(1),
\fBlogin.conf\fP(5),
\fBmoduli\fP(5),
\fBsshd_config\fP(5),
\fBsftp-server\fP(8)

S. Lehtinen, T. Rinne, M. Saarinen, T. Kivinen and T. Ylonen, \fISSH Protocol Architecture\fP, draft-ietf-secsh-architecture-12.txt, January 2002, work in progress material.

W. A. Simpson, N. Provos and M. Friedl, \fIDiffie-Hellman Group Exchange for the SSH Transport Layer Protocol\fP, draft-ietf-secsh-dh-group-exchange-02.txt, January 2002, work in progress material.
.SH AUTHORS
OpenSSH is a derivative of the original and free
ssh 1.2.12 release by Tatu Ylonen.
Aaron Campbell, Bob Beck, Markus Friedl, Niels Provos,
Theo de Raadt and Dug Song
removed many bugs, re-added newer features and
created OpenSSH.
Markus Friedl contributed the support for SSH
protocol versions 1.5 and 2.0.
Niels Provos and Markus Friedl contributed support
for privilege separation.

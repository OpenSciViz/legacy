


rename(n)             Tcl Built-In Commands             rename(n)



_________________________________________________________________

NAME
     rename - Rename or delete a command

SYNOPSIS
     rename _o_l_d_N_a_m_e _n_e_w_N_a_m_e
_________________________________________________________________


DESCRIPTION
     Rename the command that used to be called _o_l_d_N_a_m_e so that it
     is  now  called _n_e_w_N_a_m_e.  If _n_e_w_N_a_m_e is an empty string then
     _o_l_d_N_a_m_e is deleted.  The rename  command  returns  an  empty
     string as result.


KEYWORDS
     command, delete, rename




































Tcl                      Last change:                           1




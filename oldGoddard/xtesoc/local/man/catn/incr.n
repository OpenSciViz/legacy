


incr(n)               Tcl Built-In Commands               incr(n)



_________________________________________________________________

NAME
     incr - Increment the value of a variable

SYNOPSIS
     incr _v_a_r_N_a_m_e ?_i_n_c_r_e_m_e_n_t?
_________________________________________________________________


DESCRIPTION
     Increments the value stored in the variable  whose  name  is
     _v_a_r_N_a_m_e.   The value of the variable must be an integer.  If
     _i_n_c_r_e_m_e_n_t is supplied then  its  value  (which  must  be  an
     integer)  is added to the value of variable _v_a_r_N_a_m_e;  other-
     wise 1 is added to _v_a_r_N_a_m_e.  The new value is  stored  as  a
     decimal  string  in  variable  _v_a_r_N_a_m_e  and also returned as
     result.


KEYWORDS
     add, increment, variable, value

































Tcl                      Last change:                           1







tk_optionMenu(n)           Tk Commands           tk_optionMenu(n)



_________________________________________________________________

NAME
     tk_optionMenu - Create an option menubutton and its menu

SYNOPSIS
     tk_optionMenu _w _v_a_r_N_a_m_e _v_a_l_u_e ?_v_a_l_u_e _v_a_l_u_e ...?
_________________________________________________________________


DESCRIPTION
     This procedure creates an option menubutton whose name is _w,
     plus  an  associated  menu.  Together they allow the user to
     select one of the values given by the _v_a_l_u_e arguments.   The
     current  value  will  be stored in the global variable whose
     name is given by _v_a_r_N_a_m_e and it will also  be  displayed  as
     the  label  in the option menubutton.  The user can click on
     the menubutton to display  a  menu  containing  all  of  the
     _v_a_l_u_es  and thereby select a new value.  Once a new value is
     selected, it will be stored in the variable  and  appear  in
     the  option  menubutton.   The  current  value  can  also be
     changed by setting the variable.

     The return value from tk_optionMenu is the name of the  menu
     associated  with _w, so that the caller can change its confi-
     guration options or manipulate it in other ways.


KEYWORDS
     option menu

























Tk                      Last change: 4.0                        1




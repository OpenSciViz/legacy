


close(n)              Tcl Built-In Commands              close(n)



_________________________________________________________________

NAME
     close - Close an open file

SYNOPSIS
     close _f_i_l_e_I_d
_________________________________________________________________


DESCRIPTION
     Closes the file given by _f_i_l_e_I_d.  _F_i_l_e_I_d must be the  return
     value  from a previous invocation of the open command; after
     this command, it should not  be  used  anymore.   If  _f_i_l_e_I_d
     refers  to  a command pipeline instead of a file, then close
     waits for the children to complete.  The  normal  result  of
     this  command is an empty string, but errors are returned if
     there are problems in closing the file or waiting for  chil-
     dren to complete.


KEYWORDS
     close, file
































Tcl                      Last change:                           1







unset(n)              Tcl Built-In Commands              unset(n)



_________________________________________________________________

NAME
     unset - Delete variables

SYNOPSIS
     unset _n_a_m_e ?_n_a_m_e _n_a_m_e ...?
_________________________________________________________________


DESCRIPTION
     This command removes one or more variables.  Each _n_a_m_e is  a
     variable  name,  specified  in any of the ways acceptable to
     the set command.  If a _n_a_m_e refers to an element of an array
     then  that  element is removed without affecting the rest of
     the array.  If a _n_a_m_e consists of  an  array  name  with  no
     parenthesized  index, then the entire array is deleted.  The
     unset command returns an empty string as result.   An  error
     occurs  if any of the variables doesn't exist, and any vari-
     ables after the non-existent one are not deleted.


KEYWORDS
     remove, variable































Tcl                      Last change:                           1







exec(n)               Tcl Built-In Commands               exec(n)



_________________________________________________________________

NAME
     exec - Invoke subprocess(es)

SYNOPSIS
     exec ?_s_w_i_t_c_h_e_s? _a_r_g ?_a_r_g ...?
_________________________________________________________________


DESCRIPTION
     This command treats its arguments as  the  specification  of
     one or more subprocesses to execute.  The arguments take the
     form of a standard shell pipeline where each _a_r_g becomes one
     word  of a command, and each distinct command becomes a sub-
     process.

     If the initial arguments to exec start with - then they  are  |
     treated  as  command-line  switches  and are not part of the  |
     pipeline  specification.    The   following   switches   are  |
     currently supported:                                          |

                              -                          keepnew-  |
                  line                                          |
                  |                                                |
                  Retains a trailing newline  in  the  pipeline's  |
                  output.   Normally  a  trailing newline will be  |
                  deleted.                                         |

                                  -                                                             |
                  -                                             |
                  |                                                |
                  Marks the end of switches.  The  argument  fol-  |
                  lowing  this  one  will be treated as the first  |
                  _a_r_g even if it starts with a -.

     If an _a_r_g (or pair of _a_r_g's) has one of the forms  described
     below  then  it is used by exec to control the flow of input
     and output among the subprocess(es).   Such  arguments  will
     not  be  passed to the subprocess(es).  In forms such as ``<  |
     _f_i_l_e_N_a_m_e'' _f_i_l_e_N_a_m_e may either be  in  a  separate  argument  |
     from ``<'' or in the same argument with no intervening space  |
     (i.e. ``<_f_i_l_e_N_a_m_e'').

     |              Separates distinct commands in the  pipeline.
                    The  standard output of the preceding command
                    will be piped into the standard input of  the
                    next command.

     |&             Separates distinct commands in the  pipeline.
                    Both  standard  output  and standard error of
                    the preceding command will be piped into  the



Tcl                     Last change: 7.0                        1






exec(n)               Tcl Built-In Commands               exec(n)



                    standard  input  of  the  next command.  This
                    form of redirection overrides forms  such  as
                    2> and >&.

     < _f_i_l_e_N_a_m_e     The file named by _f_i_l_e_N_a_m_e is opened and used
                    as  the  standard input for the first command
                    in the pipeline.

     <@ _f_i_l_e_I_d      _F_i_l_e_I_d must be the  identifier  for  an  open  |
                    file,  such as the return value from a previ-  |
                    ous call to open.  It is used as the standard  |
                    input  for the first command in the pipeline.  |
                    _F_i_l_e_I_d must have been opened for reading.

     << _v_a_l_u_e       _V_a_l_u_e is passed to the first command  as  its
                    standard input.

     > _f_i_l_e_N_a_m_e     Standard output  from  the  last  command  is
                    redirected   to   the  file  named  _f_i_l_e_N_a_m_e,
                    overwriting its previous contents.

     2> _f_i_l_e_N_a_m_e    Standard error from all commands in the pipe-  |
                    line   is   redirected   to  the  file  named  |
                    _f_i_l_e_N_a_m_e, overwriting its previous contents.   |

     >& _f_i_l_e_N_a_m_e                                                |
                    |                                              |
                    Both standard output from  the  last  command  |
                    and  standard  error  from  all  commands are  |
                    redirected  to  the  file   named   _f_i_l_e_N_a_m_e,  |
                    overwriting its previous contents.

     >> _f_i_l_e_N_a_m_e    Standard output  from  the  last  command  is
                    redirected   to   the  file  named  _f_i_l_e_N_a_m_e,
                    appending to it rather than overwriting it.

     2>> _f_i_l_e_N_a_m_e   Standard error from all commands in the pipe-  |
                    line   is   redirected   to  the  file  named  |
                    _f_i_l_e_N_a_m_e,  appending  to   it   rather   than  |
                    overwriting it.                                |

     >>& _f_i_l_e_N_a_m_e                                               |
                    |                                              |
                    Both standard output from  the  last  command  |
                    and  standard  error  from  all  commands are  |
                    redirected  to  the  file   named   _f_i_l_e_N_a_m_e,  |
                    appending to it rather than overwriting it.    |

     >@ _f_i_l_e_I_d                                                  |
                    |                                              |
                    _F_i_l_e_I_d must be the  identifier  for  an  open  |
                    file,   such  as  the  return  value  from  a  |



Tcl                     Last change: 7.0                        2






exec(n)               Tcl Built-In Commands               exec(n)



                    previous call to open.  Standard output  from  |
                    the  last  command  is redirected to _f_i_l_e_I_d's  |
                    file, which must have been opened  for  writ-  |
                    ing.                                           |

     2>@ _f_i_l_e_I_d                                                 |
                    |                                              |
                    _F_i_l_e_I_d must be the  identifier  for  an  open  |
                    file,  such as the return value from a previ-  |
                    ous call to open.  Standard  error  from  all  |
                    commands  in  the  pipeline  is redirected to  |
                    _f_i_l_e_I_d's  file.   The  file  must  have  been  |
                    opened for writing.                            |

     >&@ _f_i_l_e_I_d                                                 |
                    |                                              |
                    _F_i_l_e_I_d must be the  identifier  for  an  open  |
                    file,  such as the return value from a previ-  |
                    ous call to open.  Both standard output  from  |
                    the  last command and standard error from all  |
                    commands are  redirected  to  _f_i_l_e_I_d's  file.  |
                    The file must have been opened for writing.

     If standard output has not been  redirected  then  the  exec
     command returns the standard output from the last command in
     the pipeline.  If any of the commands in the  pipeline  exit
     abnormally or are killed or suspended, then exec will return
     an error and the error message will include  the  pipeline's
     output  followed  by  error messages describing the abnormal
     terminations; the errorCode variable will contain additional
     information about the last abnormal termination encountered.
     If any of the commands writes to its standard error file and
     that  standard error isn't redirected, then exec will return
     an error;  the error message  will  include  the  pipeline's
     standard  output, followed by messages about abnormal termi-
     nations (if any), followed by the standard error output.

     If the last character of the result or error  message  is  a
     newline  then  that  character  is normally deleted from the
     result or error message.  This is consistent with other  Tcl
     return values, which don't normally end with newlines.  How-  |
     ever, if -keepnewline is specified then the trailing newline  |
     is retained.

     If standard input isn't redirected with ``<'' or  ``<<''  or
     ``<@''  then the standard input for the first command in the
     pipeline is taken from the  application's  current  standard
     input.

     If the last _a_r_g is ``&'' then the pipeline will be  executed
     in  background.  In this case the exec command will return a  |
     list whose elements are the process identifiers for  all  of  |



Tcl                     Last change: 7.0                        3






exec(n)               Tcl Built-In Commands               exec(n)



     the  subprocesses in the pipeline.  The standard output from
     the  last  command  in  the  pipeline   will   go   to   the
     application's  standard output if it hasn't been redirected,
     and error output from all of the commands  in  the  pipeline
     will  go  to  the  application's  standard error file unless
     redirected.

     The first word in each command is taken as the command name;
     tilde-substitution  is  performed  on  it, and if the result
     contains  no  slashes  then  the  directories  in  the  PATH
     environment  variable  are searched for an executable by the
     given name.  If the name contains a slash then it must refer
     to  an  executable reachable from the current directory.  No
     ``glob'' expansion or  other  shell-like  substitutions  are
     performed on the arguments to commands.


KEYWORDS
     execute, pipeline, redirection, subprocess




































Tcl                     Last change: 7.0                        4




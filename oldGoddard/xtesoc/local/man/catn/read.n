


read(n)               Tcl Built-In Commands               read(n)



_________________________________________________________________

NAME
     read - Read from a file

SYNOPSIS
     read ?-nonewline? _f_i_l_e_I_d
     read _f_i_l_e_I_d _n_u_m_B_y_t_e_s
_________________________________________________________________


DESCRIPTION
     In the first form, all of the remaining bytes are read  from
     the file given by _f_i_l_e_I_d; they are returned as the result of
     the command.  If the -nonewline switch is specified then the
     last  character of the file is discarded if it is a newline.
     In the second form, the extra argument  specifies  how  many
     bytes  to  read;  exactly  this  many bytes will be read and
     returned, unless there are fewer than _n_u_m_B_y_t_e_s bytes left in
     the  file;  in  this  case,  all  the  remaining  bytes  are
     returned.  _F_i_l_e_I_d must be stdin or the return value  from  a
     previous  call  to  open;  it  must refer to a file that was
     opened for reading.  Any existing end-of-file or error  con-  |
     dition  on  the file is cleared at the beginning of the read  |
     command.


KEYWORDS
     file, read


























Tcl                      Last change:                           1







string(n)             Tcl Built-In Commands             string(n)



_________________________________________________________________

NAME
     string - Manipulate strings

SYNOPSIS
     string _o_p_t_i_o_n _a_r_g ?_a_r_g ...?
_________________________________________________________________


DESCRIPTION
     Performs one of  several  string  operations,  depending  on
     _o_p_t_i_o_n.  The legal _o_p_t_i_o_ns (which may be abbreviated) are:

     string compare _s_t_r_i_n_g_1 _s_t_r_i_n_g_2
          Perform a character-by-character comparison of  strings
          _s_t_r_i_n_g_1  and  _s_t_r_i_n_g_2  in  the same way as the C strcmp
          procedure.  Return -1, 0, or 1,  depending  on  whether
          _s_t_r_i_n_g_1  is  lexicographically  less than, equal to, or
          greater than _s_t_r_i_n_g_2.

     string first _s_t_r_i_n_g_1 _s_t_r_i_n_g_2
          Search  _s_t_r_i_n_g_2  for  a  sequence  of  characters  that
          exactly  match  the  characters  in _s_t_r_i_n_g_1.  If found,
          return the index of the first character  in  the  first
          such match within _s_t_r_i_n_g_2.  If not found, return -1.

     string index _s_t_r_i_n_g _c_h_a_r_I_n_d_e_x
          Returns the _c_h_a_r_I_n_d_e_x'th character of the _s_t_r_i_n_g  argu-
          ment.   A _c_h_a_r_I_n_d_e_x of 0 corresponds to the first char-
          acter of the string.  If _c_h_a_r_I_n_d_e_x is less  than  0  or
          greater  than or equal to the length of the string then
          an empty string is returned.

     string last _s_t_r_i_n_g_1 _s_t_r_i_n_g_2
          Search  _s_t_r_i_n_g_2  for  a  sequence  of  characters  that
          exactly  match  the  characters  in _s_t_r_i_n_g_1.  If found,
          return the index of the first  character  in  the  last
          such  match within _s_t_r_i_n_g_2.  If there is no match, then
          return -1.

     string length _s_t_r_i_n_g
          Returns a decimal string giving the number  of  charac-
          ters in _s_t_r_i_n_g.

     string match _p_a_t_t_e_r_n _s_t_r_i_n_g
          See if _p_a_t_t_e_r_n matches _s_t_r_i_n_g; return 1 if it  does,  0
          if  it  doesn't.  Matching is done in a fashion similar
          to that used by the C-shell.  For the  two  strings  to
          match, their contents must be identical except that the
          following special sequences may appear in _p_a_t_t_e_r_n:




Tcl                     Last change: 7.4                        1






string(n)             Tcl Built-In Commands             string(n)



          *         Matches any sequence of characters in _s_t_r_i_n_g,
                    including a null string.

          ?         Matches any single character in _s_t_r_i_n_g.

          [_c_h_a_r_s]   Matches any character in  the  set  given  by
                    _c_h_a_r_s.  If a sequence of the form _x-_y appears
                    in _c_h_a_r_s, then any character between _x and _y,
                    inclusive, will match.

          \_x        Matches the single character  _x.   This  pro-
                    vides a way of avoiding the special interpre-
                    tation of the characters *?[]\ in _p_a_t_t_e_r_n.

     string range _s_t_r_i_n_g _f_i_r_s_t _l_a_s_t
          Returns a range of consecutive characters from  _s_t_r_i_n_g,
          starting  with  the  character whose index is _f_i_r_s_t and
          ending with the character  whose  index  is  _l_a_s_t.   An
          index of 0 refers to the first character of the string.
          _L_a_s_t may be end (or any abbreviation of it) to refer to
          the  last  character  of  the string.  If _f_i_r_s_t is less
          than zero then it is treated as if it were zero, and if
          _l_a_s_t  is  greater  than  or  equal to the length of the
          string then it is treated as if it were end.  If  _f_i_r_s_t
          is greater than _l_a_s_t then an empty string is returned.

     string tolower _s_t_r_i_n_g
          Returns a value equal to _s_t_r_i_n_g except that  all  upper
          case letters have been converted to lower case.

     string toupper _s_t_r_i_n_g
          Returns a value equal to _s_t_r_i_n_g except that  all  lower
          case letters have been converted to upper case.

     string trim _s_t_r_i_n_g ?_c_h_a_r_s?
          Returns a value equal to _s_t_r_i_n_g except that any leading
          or  trailing characters from the set given by _c_h_a_r_s are
          removed.  If _c_h_a_r_s is not specified then white space is
          removed (spaces, tabs, newlines, and carriage returns).

     string trimleft _s_t_r_i_n_g ?_c_h_a_r_s?
          Returns a value equal to _s_t_r_i_n_g except that any leading
          characters from the set given by _c_h_a_r_s are removed.  If
          _c_h_a_r_s is not specified  then  white  space  is  removed
          (spaces, tabs, newlines, and carriage returns).

     string trimright _s_t_r_i_n_g ?_c_h_a_r_s?
          Returns a value equal to _s_t_r_i_n_g except that any  trail-
          ing characters from the set given by _c_h_a_r_s are removed.
          If _c_h_a_r_s is not specified then white space  is  removed
          (spaces, tabs, newlines, and carriage returns).




Tcl                     Last change: 7.4                        2






string(n)             Tcl Built-In Commands             string(n)



     string wordend _s_t_r_i_n_g _i_n_d_e_x
          Returns the index of the character just after the  last  |
          one  in  the word containing character _i_n_d_e_x of _s_t_r_i_n_g.  |
          A word is considered to  be  any  contiguous  range  of  |
          alphanumeric  or  underscore  chracters,  or any single  |
          character other than these.                              |

     string wordstart _s_t_r_i_n_g _i_n_d_e_x                              |
          |                                                        |
          Returns the index of the first character  in  the  word  |
          containing  character  _i_n_d_e_x of _s_t_r_i_n_g.  A word is con-  |
          sidered to be any contiguous range of  alphanumeric  or  |
          underscore  chracters,  or  any  single character other  |
          than these.


KEYWORDS
     case conversion, compare,  index,  match,  pattern,  string,
     word




































Tcl                     Last change: 7.4                        3




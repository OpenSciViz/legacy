


continue(n)           Tcl Built-In Commands           continue(n)



_________________________________________________________________

NAME
     continue - Skip to the next iteration of a loop

SYNOPSIS
     continue
_________________________________________________________________


DESCRIPTION
     This command is typically invoked inside the body of a loop-
     ing  command  such as for or foreach or while.  It returns a
     TCL_CONTINUE code, which  causes  a  continue  exception  to
     occur.   The  exception  causes  the  current  script  to be
     aborted out to the the innermost  containing  loop  command,
     which  then  continues  with the next iteration of the loop.
     Catch exceptions are also handled in a few other situations,
     such  as the catch command and the outermost scripts of pro-
     cedure bodies.


KEYWORDS
     continue, iteration, loop































Tcl                      Last change:                           1




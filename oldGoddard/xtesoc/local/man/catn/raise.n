


raise(n)                   Tk Commands                   raise(n)



_________________________________________________________________

NAME
     raise - Change a window's position in the stacking order

SYNOPSIS
     raise _w_i_n_d_o_w ?_a_b_o_v_e_T_h_i_s?
_________________________________________________________________


DESCRIPTION
     If the _a_b_o_v_e_T_h_i_s argument is omitted then the command raises
     _w_i_n_d_o_w so that it is above all of its siblings in the stack-
     ing order (it will not be obscured by any siblings and  will
     obscure  any  siblings  that  overlap  it).  If _a_b_o_v_e_T_h_i_s is
     specified then it must be the path name of a window that  is
     either a sibling of _w_i_n_d_o_w or the descendant of a sibling of
     _w_i_n_d_o_w.  In this case the raise command will  insert  _w_i_n_d_o_w
     into  the stacking order just above _a_b_o_v_e_T_h_i_s (or the ances-
     tor of _a_b_o_v_e_T_h_i_s that is a sibling of  _w_i_n_d_o_w);  this  could
     end up either raising or lowering _w_i_n_d_o_w.


KEYWORDS
     obscure, raise, stacking order






























Tk                      Last change: 3.3                        1




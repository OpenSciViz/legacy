


tk_focusNext(n)            Tk Commands            tk_focusNext(n)



_________________________________________________________________

NAME
     tk_focusNext, tk_focusPrev, tk_focusFollowsMouse  -  Utility
     procedures for managing the input focus.

SYNOPSIS
     tk_focusNext _w_i_n_d_o_w
     tk_focusPrev _w_i_n_d_o_w
     tk_focusContinue _w_i_n_d_o_w
     tk_focusFollowsMouse
_________________________________________________________________


DESCRIPTION
     tk_focusNext  is  a  utility  procedure  used  for  keyboard
     traversal.   It  finds  the  ``next'' window after _w_i_n_d_o_w in
     focus order and sets the input focus to  that  window.   The
     focus  order  is determined by the stacking order of windows
     and the structure of the window hierarchy.  Among  siblings,
     the  focus order is the same as the stacking order, with the
     lowest window being first.  Frames do not receive the  focus
     at  all,   but  if  a  frame has children, then its children
     appear in the frame's place in the focus  order.   Top-level
     windows  are  skipped,  so  tk_focusNext will never move the
     focus from one top-level to another.  If  the  last  of  all
     descendants  of  a  top-level  is reached, the focus rotates
     circularly back to the first descendant.  If  _w_i_n_d_o_w's  top-
     level  has  no descendants that are capable of receiving the
     focus, then tk_focusNext has no effect.

     tk_focusPrev is similar to tk_focusNext except that it moves
     the  focus  to  the  window  just before _w_i_n_d_o_w in the focus
     order.

     tk_focusContinue is typically invoked by windows that  don't
     have  any  use  for  the input focus.  When they receive the
     focus, they call tk_focusContinue with  their  window  name.
     tk_focusContinue   then   calls   either   tk_focusNext   or
     tk_focusPrev (whichever was called last) to pass  the  focus
     on  to either the next or previous window, effectively skip-
     ping this window in the traversal order.

     tk_focusFollowsMouse changes the focus model for the  appli-
     cation  to  an implicit one where the window under the mouse
     gets the focus.  After this procedure  is  called,  whenever
     the  mouse enters a window Tk will automatically give it the
     input focus.  The focus command may  be  used  to  move  the
     focus to a window other than the one under the mouse, but as
     soon as the mouse moves into a new  window  the  focus  will
     jump  to that window.  Note: at present there is no built-in
     support for returning the application to an  explicit  focus



Tk                      Last change: 4.0                        1






tk_focusNext(n)            Tk Commands            tk_focusNext(n)



     model;   to  do  this  you'll  have  to  write a script that
     deletes the bindings created by tk_focusFollowsMouse.


KEYWORDS
     focus, keyboard traversal, top-level

















































Tk                      Last change: 4.0                        2




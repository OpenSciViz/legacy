


clipboard(n)               Tk Commands               clipboard(n)



_________________________________________________________________

NAME
     clipboard - Manipulate Tk clipboard

SYNOPSIS
     clipboard _o_p_t_i_o_n ?_a_r_g _a_r_g ...?
_________________________________________________________________


DESCRIPTION
     This command provides a Tcl interface to the  Tk  clipboard,
     which  stores  data  for later retrieval using the selection
     mechanism.  In order to copy data into the clipboard,  clip-
     board clear must be called, followed by a sequence of one or
     more calls to clipboard append.  To ensure  that  the  clip-
     board is updated atomically, all appends should be completed
     before returning to the event loop.

     The first argument to clipboard determines the format of the
     rest  of the arguments and the behavior of the command.  The
     following forms are currently supported:

     clipboard clear ?-displayof _w_i_n_d_o_w?
          Claim ownership of the clipboard  on  _w_i_n_d_o_w's  display
          and  remove  any previous contents.  _W_i_n_d_o_w defaults to
          ``.''.  Returns an empty string.

_t_y_p_e? _d_a_t_a
     clipboard append ?-displayof _w_i_n_d_o_w? ?- format  _f_o_r_m_a_t?  ? -
           type
          Appends _d_a_t_a to the clipboard on  _w_i_n_d_o_w's  display  in
          the form given by _t_y_p_e with the representation given by
          _f_o_r_m_a_t.  All items appended to the clipboard  with  the
          same _t_y_p_e must have the same _f_o_r_m_a_t.

          _T_y_p_e specifies the form in which the selection is to be
          returned  (the  desired  ``target''  for conversion, in
          ICCCM terminology), and should be an atom name such  as
          STRING or FILE_NAME; see the Inter-Client Communication
          Conventions Manual for complete details.  _T_y_p_e defaults
          to STRING.

          The _f_o_r_m_a_t argument specifies the  representation  that
          should be used to transmit the selection to the reques-
          ter (the second column of Table 2 of  the  ICCCM),  and
          defaults to STRING.  If _f_o_r_m_a_t is STRING, the selection
          is transmitted as 8-bit ASCII characters.  If _f_o_r_m_a_t is
          ATOM, then the _d_a_t_a is divided into fields separated by
          white space; each field is converted to its atom value,
          and the 32-bit atom value is transmitted instead of the
          atom name.  For any other _f_o_r_m_a_t,  _d_a_t_a is divided into



Tk                      Last change: 4.0                        1






clipboard(n)               Tk Commands               clipboard(n)



          fields  separated by white space and each field is con-
          verted to a 32-bit integer; an  array  of  integers  is
          transmitted  to  the  selection  requester.   Note that
          strings passed to  clipboard  append  are  concatenated
          before  conversion,  so  the  caller  must take care to
          ensure appropriate spacing across string boundaries.

          The _f_o_r_m_a_t argument is needed  only  for  compatibility
          with clipboard requesters that don't use Tk.  If the Tk
          toolkit is being used to retrieve the CLIPBOARD  selec-
          tion  then  the  value is converted back to a string at
          the requesting end, so _f_o_r_m_a_t is irrelevant.


KEYWORDS
     clear, format, clipboard, append, selection, type







































Tk                      Last change: 4.0                        2




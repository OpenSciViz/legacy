


bind(n)                    Tk Commands                    bind(n)



_________________________________________________________________

NAME
     bind - Arrange for X events to invoke Tcl scripts

SYNOPSIS
     bind _t_a_g
     bind _t_a_g _s_e_q_u_e_n_c_e
     bind _t_a_g _s_e_q_u_e_n_c_e _s_c_r_i_p_t
     bind _t_a_g _s_e_q_u_e_n_c_e +_s_c_r_i_p_t
_________________________________________________________________


INTRODUCTION
     The bind command associates Tcl scripts with X  events.   If
     all  three  arguments  are  specified, bind will arrange for
     _s_c_r_i_p_t (a Tcl script) to be evaluated whenever the  event(s)
     given  by _s_e_q_u_e_n_c_e occur in the window(s) identified by _t_a_g.
     If _s_c_r_i_p_t is prefixed with a ``+'', then it is  appended  to
     any   existing   binding  for  _s_e_q_u_e_n_c_e;   otherwise  _s_c_r_i_p_t
     replaces any existing binding.  If _s_c_r_i_p_t is an empty string
     then  the current binding for _s_e_q_u_e_n_c_e is destroyed, leaving
     _s_e_q_u_e_n_c_e unbound.  In all of the cases where a _s_c_r_i_p_t  argu-
     ment is provided, bind returns an empty string.

     If _s_e_q_u_e_n_c_e is specified without a _s_c_r_i_p_t, then  the  script
     currently  bound to _s_e_q_u_e_n_c_e is returned, or an empty string
     is returned if there is no binding for _s_e_q_u_e_n_c_e.  If neither
     _s_e_q_u_e_n_c_e nor _s_c_r_i_p_t is specified, then the return value is a
     list whose elements are all the sequences  for  which  there  |
     exist bindings for _t_a_g.                                       |

     The _t_a_g argument  determines  which  window(s)  the  binding  |
     applies to.  If _t_a_g begins with a dot, as in .a.b.c, then it  |
     must be the path name for a window; otherwise it may  be  an  |
     arbitrary  string.   Each  window  has an associated list of  |
     tags, and a binding applies to a particular  window  if  its  |
     tag  is  among those specified for the window.  Although the  |
     bindtags command may be used to assign an arbitrary  set  of  |
     binding  tags  to a window, the default binding tags provide  |
     the following behavior:                                       |

                                                                |
          |                                                        |
          If a tag is the name of an internal window the  binding  |
          applies to that window.                                  |

                                                                |
          |                                                        |
          If the tag is the name of a toplevel window the binding  |
          applies  to  the  toplevel  window and all its internal  |
          windows.                                                 |



Tk                      Last change: 4.0                        1






bind(n)                    Tk Commands                    bind(n)



                                                                |
          |                                                        |
          If the tag is the name of a class of widgets,  such  as  |
          Button,  the  binding  applies  to  all widgets in that  |
          class;                                                   |

                                                                |
          |                                                        |
          If _t_a_g has the value all, the binding  applies  to  all  |
          windows in the application.


EVENT PATTERNS
     The _s_e_q_u_e_n_c_e argument specifies a sequence of  one  or  more
     event  patterns,  with optional white space between the pat-
     terns.  Each event pattern may take either of two forms.  In
     the  simplest  case it is a single printing ASCII character,
     such as a or [.  The character may not be a space  character
     or the character <.  This form of pattern matches a KeyPress
     event for the particular character.  The second form of pat-
     tern  is longer but more general.  It has the following syn-
     tax:

          <_m_o_d_i_f_i_e_r-_m_o_d_i_f_i_e_r-_t_y_p_e-_d_e_t_a_i_l>

     The entire event pattern is surrounded  by  angle  brackets.
     Inside  the  angle  brackets  are zero or more modifiers, an
     event type, and an extra piece of information (_d_e_t_a_i_l) iden-
     tifying  a  particular  button or keysym.  Any of the fields
     may be omitted, as long as at least one of _t_y_p_e  and  _d_e_t_a_i_l
     is  present.  The fields must be separated by white space or
     dashes.


MODIFIERS
     Modifiers consist of any of the following values:             |

          Control                 Mod2, M2                         |
          Shift                   Mod3, M3                         |
          Lock                    Mod4, M4                         |
          Button1, B1             Mod5, M5                         |
          Button2, B2             Meta, M                          |
          Button3, B3             Alt                              |
          Button4, B4             Double                           |
          Button5, B5             Triple                           |
          Mod1, M1                                                 |

     Where more than one value is listed,  separated  by  commas,  |
     the  values  are equivalent.  Most of the modifiers have the  |
     obvious X meanings.  For example, Button1 requires that but-  |
     ton  1 be depressed when the event occurs.  For a binding to  |
     match a given event, the modifiers in the event must include  |



Tk                      Last change: 4.0                        2






bind(n)                    Tk Commands                    bind(n)



     all  of  those specified in the event pattern.  An event may  |
     also contain additional modifiers not specified in the bind-  |
     ing.   For  example,  if button 1 is pressed while the shift  |
     and control keys are down,  the  pattern  <Control-Button-1>  |
     will  match  the event, but <Mod1-Button-1> will not.  If no  |
     modifiers are specified, then any combination  of  modifiers  |
     may be present in the event.                                  |

     Meta and M refer to whichever of the M1 through M5 modifiers  |
     is  associated with the meta key(s) on the keyboard (keysyms  |
     Meta_R and Meta_L).  If there are no meta keys, or  if  they  |
     are  not associated with any modifiers, then Meta and M will  |
     not match any events.  Similarly, the Alt modifier refers to  |
     whichever  modifier is associated with the alt key(s) on the  |
     keyboard (keysyms Alt_L and Alt_R).

     The Double and Triple modifiers are a convenience for speci-
     fying  double  mouse  clicks and other repeated events. They
     cause a particular event pattern  to  be  repeated  2  or  3
     times,  and  also  place a time and space requirement on the
     sequence:  for a sequence of events to  match  a  Double  or
     Triple  pattern, all of the events must occur close together
     in time and without substantial  mouse  motion  in  between.
     For  example,  <Double-Button-1>  is  equivalent to <Button-
     1><Button-1> with the extra time and space requirement.


EVENT TYPES
     The _t_y_p_e field may be any of the  standard  X  event  types,
     with  a few extra abbreviations.  Below is a list of all the
     valid types; where  two  names  appear  together,  they  are
     synonyms.

          ButtonPress, Button Expose             Motion
          ButtonRelease       FocusIn            Leave
          Circulate           FocusOut           Map
          Colormap            Gravity            Property
          Configure           Keymap             Reparent
          Destroy             KeyPress, Key      Unmap
          Enter               KeyRelease         Visibility        |


     The last part of a long event specification is  _d_e_t_a_i_l.   In
     the  case of a ButtonPress or ButtonRelease event, it is the
     number of a button (1-5).  If a button number is given, then
     only  an  event on that particular button will match;  if no
     button number is given, then an event  on  any  button  will
     match.   Note:  giving a specific button number is different
     than specifying a button modifier; in  the  first  case,  it
     refers  to  a button being pressed or released, while in the
     second it refers  to  some  other  button  that  is  already
     depressed  when  the  matching  event  occurs.   If a button



Tk                      Last change: 4.0                        3






bind(n)                    Tk Commands                    bind(n)



     number is given then _t_y_p_e may be omitted:  if  will  default
     to   ButtonPress.    For   example,  the  specifier  <1>  is
     equivalent to <ButtonPress-1>.

     If the event type is KeyPress or KeyRelease, then _d_e_t_a_i_l may
     be  specified  in the form of an X keysym.  Keysyms are tex-
     tual specifications for particular  keys  on  the  keyboard;
     they  include  all  the  alphanumeric ASCII characters (e.g.
     ``a'' is the keysym for the  ASCII  character  ``a''),  plus
     descriptions  for  non-alphanumeric characters (``comma'' is
     the keysym for the comma character), plus  descriptions  for
     all  the  non-ASCII keys on the keyboard (``Shift_L'' is the
     keysm for the left shift key, and ``F1'' is the  keysym  for
     the  F1  function  key, if it exists).  The complete list of
     keysyms is not presented here;  it is available in  other  X
     documentation and may vary from system to system.  If neces-
     sary, you can use the %K notation described below  to  print
     out  the  keysym  name  for  a  particular key.  If a keysym
     _d_e_t_a_i_l is given, then the _t_y_p_e field  may  be  omitted;   it
     will  default  to KeyPress.  For example, <Control-comma> is
     equivalent to <Control-KeyPress-comma>.


BINDING SCRIPTS AND SUBSTITUTIONS
     The _s_c_r_i_p_t argument to bind is a Tcl script, which  will  be
     executed  whenever the given event sequence occurs.  _C_o_m_m_a_n_d
     will be executed in the same interpreter that the bind  com-
     mand  was executed in, and it will run at global level (only
     global variables will be accessible).   If  _s_c_r_i_p_t  contains
     any  %  characters,  then  the  script  will not be executed
     directly.  Instead,  a  new  script  will  be  generated  by
     replacing  each  %,  and  the  character  following it, with
     information from the current event.  The replacement depends
     on  the  character  following  the %, as defined in the list
     below.  Unless otherwise indicated, the  replacement  string  |
     is  the  decimal  value  of the given field from the current  |
     event.  Some of the substitutions are only valid for certain
     types of events;  if they are used for other types of events
     the value substituted is undefined.

     %%   Replaced with a single percent.

     %#   The number of the last client request processed by  the
          server  (the  _s_e_r_i_a_l  field from the event).  Valid for
          all event types.

     %a   The _a_b_o_v_e field from the event, formatted as a  hexade-  |
          cimal number.  Valid only for Configure events.

     %b   The number of the button that was pressed or  released.
          Valid only for ButtonPress and ButtonRelease events.




Tk                      Last change: 4.0                        4






bind(n)                    Tk Commands                    bind(n)



     %c   The _c_o_u_n_t field from the event.  Valid only for  Expose
          events.

     %d   The _d_e_t_a_i_l field from the event.  The %d is replaced by
          a  string  identifying  the  detail.  For Enter, Leave,
          FocusIn, and FocusOut events, the string will be one of
          the following:

               NotifyAncestor          NotifyNonlinearVirtual
               NotifyDetailNone        NotifyPointer
               NotifyInferior          NotifyPointerRoot
               NotifyNonlinear         NotifyVirtual

          For events other than these, the substituted string  is
          undefined.

     %f   The _f_o_c_u_s field from the event (0 or  1).   Valid  only
          for Enter and Leave events.

     %h   The _h_e_i_g_h_t field from the event.  Valid only  for  Con-
          figure, Expose, and GraphicsExpose events.

     %k   The _k_e_y_c_o_d_e field  from  the  event.   Valid  only  for
          KeyPress and KeyRelease events.

     %m   The _m_o_d_e field from the event.  The substituted  string
          is  one  of  NotifyNormal, NotifyGrab, NotifyUngrab, or
          NotifyWhileGrabbed.  Valid only for EnterWindow,  Focu-
          sIn, FocusOut, and LeaveWindow events.

     %o   The _o_v_e_r_r_i_d_e__r_e_d_i_r_e_c_t field from the event.  Valid only
          for Map, Reparent, and Configure events.

     %p   The _p_l_a_c_e field from the event, substituted as  one  of
          the  strings  PlaceOnTop  or PlaceOnBottom.  Valid only
          for Circulate events.

     %s   The _s_t_a_t_e field from the event.  For ButtonPress,  But-
          tonRelease,  Enter,  KeyPress,  KeyRelease,  Leave, and
          Motion events, a decimal string  is  substituted.   For  |
          Visibility,  one  of  the strings VisibilityUnobscured,
          VisibilityPartiallyObscured,   and   VisibilityFullyOb-
          scured is substituted.

     %t   The _t_i_m_e field from the event.  Valid only  for  events
          that contain a _t_i_m_e field.

     %w   The _w_i_d_t_h field from the event.  Valid only for Config-
          ure, Expose, and GraphicsExpose events.

     %x   The _x field from the event.  Valid only for events con-
          taining an _x field.



Tk                      Last change: 4.0                        5






bind(n)                    Tk Commands                    bind(n)



     %y   The _y field from the event.  Valid only for events con-
          taining a _y field.

     %A   Substitutes the ASCII character  corresponding  to  the
          event,  or  the  empty  string  if  the  event  doesn't
          correspond to an ASCII character (e.g.  the  shift  key
          was  pressed).   XLookupString  does  all  the  work of
          translating from  the  event  to  an  ASCII  character.
          Valid only for KeyPress and KeyRelease events.

     %B   The _b_o_r_d_e_r__w_i_d_t_h field from the event.  Valid only  for
          Configure events.

     %D   The _d_i_s_p_l_a_y field from the event.  Valid for all  event
          types.

     %E   The _s_e_n_d__e_v_e_n_t field from the  event.   Valid  for  all
          event types.

     %K   The keysym corresponding to the event, substituted as a
          textual string.  Valid only for KeyPress and KeyRelease
          events.

     %N   The keysym corresponding to the event, substituted as a  |
          decimal number.  Valid only for KeyPress and KeyRelease
          events.

     %R   The _r_o_o_t window identifier from the event.  Valid  only
          for events containing a _r_o_o_t field.

     %S   The _s_u_b_w_i_n_d_o_w window identifier from the event, format-  |
          ted  as  a  hexadecimal  number.  Valid only for events
          containing a _s_u_b_w_i_n_d_o_w field.

     %T   The _t_y_p_e field from the event.   Valid  for  all  event
          types.

     %W   The path name of the window  to  which  the  event  was
          reported  (the _w_i_n_d_o_w field from the event).  Valid for
          all event types.

     %X   The _x__r_o_o_t field from the  event.   If  a  virtual-root  |
          window manager is being used then the substituted value  |
          is the corresponding x-coordinate in the virtual  root.
          Valid  only  for  ButtonPress, ButtonRelease, KeyPress,
          KeyRelease, and Motion events.

     %Y   The _y__r_o_o_t field from the  event.   If  a  virtual-root  |
          window manager is being used then the substituted value  |
          is the corresponding y-coordinate in the virtual  root.
          Valid  only  for  ButtonPress, ButtonRelease, KeyPress,
          KeyRelease, and Motion events.



Tk                      Last change: 4.0                        6






bind(n)                    Tk Commands                    bind(n)



     The replacement string for a %-replacement is formatted as a  |
     proper  Tcl  list  element.  This means that it will be sur-  |
     rounded with braces if it contains spaces, or special  char-  |
     acters such as $ and { may be preceded by backslashes.  This  |
     guarantees that the string will be passed  through  the  Tcl  |
     parser  when the binding script is evaluated.  Most replace-  |
     ments are numbers or well-defined  strings  such  as  Above;  |
     for  these replacements no special formatting is ever neces-  |
     sary.  The most common case where reformatting occurs is for  |
     the %A substitution.  For example, if _s_c_r_i_p_t is               |

          insert %A                                                |

     and the character typed is an open square bracket, then  the  |
     script actually executed will be                              |

          insert \[                                                |

     This will cause the insert to receive the original  replace-  |
     ment string (open square bracket) as its first argument.  If  |
     the extra backslash hadn't been added, Tcl  would  not  have  |
     been able to parse the script correctly.


MULTIPLE MATCHES
     It is possible for several  bindings  to  match  a  given  X  |
     event.  If the bindings are associated with different _t_a_g's,  |
     then each of the bindings will be executed,  in  order.   By  |
     default, a class binding will be executed first, followed by  |
     a binding for the widget, a binding for its toplevel, and an  |
     all  binding.   The  bindtags  command may be used to change  |
     this order for a particular window  or  to  associate  addi-  |
     tional binding tags with the window.                          |

     The continue and break commands may be used inside a binding  |
     script  to  control  the processing of matching scripts.  If  |
     continue is invoked, then the current binding script is ter-  |
     minated  but  Tk  will  continue  processing binding scripts  |
     associated with  other  _t_a_g's.   If  the  break  command  is  |
     invoked within a binding script, then that script terminates  |
     and no other scripts will be invoked for the event.           |

     If more than one binding matches a particular event and they  |
     have  the same _t_a_g, then the most specific binding is chosen  |
     and its  script  is  evaluated.   The  following  tests  are  |
     applied,  in  order,  to determine which of several matching  |
     sequences is more specific:  (a) a longer sequence (in terms  |
     of number of events matched) is more specific than a shorter  |
     sequence; (b) an event pattern  that  specifies  a  specific  |
     button or key is more specific than one that doesn't; (c) if  |
     the modifiers specified in one pattern are a subset  of  the  |
     modifiers  in  another  pattern,  then the pattern with more  |



Tk                      Last change: 4.0                        7






bind(n)                    Tk Commands                    bind(n)



     modifiers is more specific.  If the matching sequences  con-
     tain  more than one event, then tests (c)-(e) are applied in
     order from the most recent event to the least  recent  event
     in  the  sequences.   If  these  tests  fail  to determine a
     winner, then the most recently registered  sequence  is  the
     winner.

     If an X event does not match any of the  existing  bindings,
     then  the  event  is  ignored.  An unbound event is not con-
     sidered to be an error.


MULTI-EVENT SEQUENCES AND IGNORED EVENTS
     When a _s_e_q_u_e_n_c_e specified in a bind  command  contains  more
     than one event pattern, then its script is executed whenever
     the recent events (leading up to and including  the  current
     event)  match  the given sequence.  This means, for example,
     that  if  button  1  is  clicked  repeatedly  the   sequence
     <Double-ButtonPress-1>  will match each button press but the
     first.  If extraneous events  that  would  prevent  a  match  |
     occur in the middle of an event sequence then the extraneous  |
     events are ignored unless they are KeyPress  or  ButtonPress  |
     events.   For  example,  <Double-ButtonPress-1> will match a  |
     sequence of presses of button 1, even though there  will  be  |
     ButtonRelease  events  (and  possibly Motion events) between  |
     the ButtonPress events.  Furthermore, a KeyPress  event  may  |
     be  preceded  by  any  number  of  other KeyPress events for  |
     modifier keys without the modifier keys preventing a  match.  |
     For example, the event sequence aB will match a press of the  |
     a key, a release of the a key, a press of the Shift key, and  |
     a press of the b key:  the press of Shift is ignored because  |
     it is a modifier key.  Finally,  if  several  Motion  events
     occur  in  a  row, only the last one is used for purposes of
     matching binding sequences.


ERRORS
     If an error occurs in executing the  script  for  a  binding
     then the tkerror mechanism is used to report the error.  The  |
     tkerror command will be executed at  global  level  (outside  |
     the context of any Tcl procedure).


SEE ALSO
     tkerror


KEYWORDS
     form, manual






Tk                      Last change: 4.0                        8







tk_menuBar(n)              Tk Commands              tk_menuBar(n)



_________________________________________________________________

NAME
     tk_menuBar, tk_bindForTraversal - Obsolete support for  menu
     bars

SYNOPSIS
     tk_menuBar _f_r_a_m_e ?_m_e_n_u _m_e_n_u ...?

     tk_bindForTraversal _a_r_g _a_r_g ...
_________________________________________________________________


DESCRIPTION
     These procedures were used in Tk 3.6 and earlier releases to  |
     help manage pulldown menus and to implement keyboard traver-  |
     sal of menus.  In Tk 4.0 and  later  releases  they  are  no  |
     longer   needed.   Stubs  for  these  procedures  have  been  |
     retained  for  backward  compatibility,  but  they  have  no  |
     effect.   You  should  remove calls to these procedures from  |
     your code, since eventually the procedures will go away.


KEYWORDS
     keyboard traversal, menu, menu bar, post






























Tk                       Last change:                           1







linsert(n)            Tcl Built-In Commands            linsert(n)



_________________________________________________________________

NAME
     linsert - Insert elements into a list

SYNOPSIS
     linsert _l_i_s_t _i_n_d_e_x _e_l_e_m_e_n_t ?_e_l_e_m_e_n_t _e_l_e_m_e_n_t ...?
_________________________________________________________________


DESCRIPTION
     This command produces a new list from _l_i_s_t by inserting  all
     of  the _e_l_e_m_e_n_t arguments just before the _i_n_d_e_xth element of
     _l_i_s_t.  Each _e_l_e_m_e_n_t argument will become a separate  element
     of  the  new  list.  If _i_n_d_e_x is less than or equal to zero,
     then the new elements are inserted at the beginning  of  the
     list.   If _i_n_d_e_x has the value end, or if it is greater than  |
     or equal to the number of elements in the list, then the new
     elements are appended to the list.


KEYWORDS
     element, insert, list
































Tcl                      Last change:                           1




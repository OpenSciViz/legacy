


option(n)                  Tk Commands                  option(n)



_________________________________________________________________

NAME
     option - Add/retrieve  window  options  to/from  the  option
     database

SYNOPSIS
     option add _p_a_t_t_e_r_n _v_a_l_u_e ?_p_r_i_o_r_i_t_y?

     option clear

     option get _w_i_n_d_o_w _n_a_m_e _c_l_a_s_s

     option readfile _f_i_l_e_N_a_m_e ?_p_r_i_o_r_i_t_y?
_________________________________________________________________


DESCRIPTION
     The option command allows you  to  add  entries  to  the  Tk
     option  database  or  to retrieve options from the database.
     The add form of the command adds a new option to  the  data-
     base.  _P_a_t_t_e_r_n contains the option being specified, and con-
     sists of names and/or  classes  separated  by  asterisks  or
     dots,  in  the usual X format.  _V_a_l_u_e contains a text string
     to associate with _p_a_t_t_e_r_n;  this is the value that  will  be
     returned  in  calls to Tk_GetOption or by invocations of the
     option get command.  If _p_r_i_o_r_i_t_y is specified, it  indicates
     the  priority  level  for  this  option (see below for legal
     values);  it defaults to interactive.  This  command  always
     returns an empty string.

     The  option  clear  command  clears  the  option   database.
     Default  options  (from the RESOURCE_MANAGER property or the
     .Xdefaults file) will be  reloaded  automatically  the  next
     time  an option is added to the database or removed from it.
     This command always returns an empty string.

     The option get command  returns  the  value  of  the  option
     specified  for  _w_i_n_d_o_w  under  _n_a_m_e  and  _c_l_a_s_s.  If several
     entries in the  option  database  match  _w_i_n_d_o_w,  _n_a_m_e,  and
     _c_l_a_s_s,  then  the command returns whichever was created with
     highest _p_r_i_o_r_i_t_y  level.   If  there  are  several  matching
     entries  at  the same priority level, then it returns which-
     ever entry was most recently entered into the  option  data-
     base.   If  there  are  no  matching entries, then the empty
     string is returned.

     The readfile form  of  the  command  reads  _f_i_l_e_N_a_m_e,  which
     should  have  the standard format for an X resource database
     such as .Xdefaults, and adds all the  options  specified  in
     that file to the option database.  If _p_r_i_o_r_i_t_y is specified,
     it indicates the  priority  level  at  which  to  enter  the



Tk                       Last change:                           1






option(n)                  Tk Commands                  option(n)



     options;  _p_r_i_o_r_i_t_y defaults to interactive.

     The _p_r_i_o_r_i_t_y arguments to the option  command  are  normally
     specified symbolically using one of the following values:

     widgetDefault
          Level 20.  Used  for  default  values  hard-coded  into
          widgets.

     startupFile
          Level 40.  Used for options specified  in  application-
          specific startup files.

     userDefault
          Level 60.  Used for options specified in  user-specific
          defaults  files, such as .Xdefaults, resource databases
          loaded into the  X  server,  or  user-specific  startup
          files.

     interactive
          Level 80.  Used  for  options  specified  interactively
          after  the  application  starts  running.   If _p_r_i_o_r_i_t_y
          isn't specified, it defaults to this level.

     Any of the above keywords may be abbreviated.  In  addition,
     priorities  may  be  specified  numerically  using  integers
     between 0 and 100, inclusive.  The numeric form is  probably
     a  bad  idea  except  for new priority levels other than the
     ones given above.


KEYWORDS
     database, option, priority, retrieve






















Tk                       Last change:                           2




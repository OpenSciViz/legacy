


checkbutton(n)             Tk Commands             checkbutton(n)



_________________________________________________________________

NAME
     checkbutton - Create and manipulate checkbutton widgets

SYNOPSIS
     checkbutton _p_a_t_h_N_a_m_e ?_o_p_t_i_o_n_s?

STANDARD OPTIONS
     activeBackground               cursor          imagetextVariable|
     activeForeground               disabledForegroundjustifyunderline|
     anchor          font           padX            wrapLength     |
     background      foreground     padY                           |
     bitmap          highlightColor relief                         |
     borderWidth     highlightThickness             text

     See the ``options'' manual entry for details on the standard
     options.

WIDGET-SPECIFIC OPTIONS
     Name:           command
     Class:          Command
     Command-Line Switch:-command

          Specifies a Tcl command to associate with  the  button.
          This  command  is typically invoked when mouse button 1
          is released over the button window.  The button's  glo-
          bal  variable (-variable option) will be updated before
          the command is invoked.

     Name:           height
     Class:          Height
     Command-Line Switch:-height

          Specifies a desired height for the button.  If an image
          or  bitmap  is  being  displayed in the button then the
          value is in screen units (i.e. any of the forms accept-
          able to Tk_GetPixels); for text it is in lines of text.
          If this option isn't specified,  the  button's  desired
          height is computed from the size of the image or bitmap
          or text being displayed in it.

     Name:           indicatorOn                                   |
     Class:          IndicatorOn                                   |
     Command-Line Switch:-indicatoron                              |

                                                                |
          |                                                        |
          Specifies whether or not the indicator should be drawn.  |
          Must  be  a proper boolean value.  If false, the relief  |
          option is ignored and the  widget's  relief  is  always  |
          sunken if the widget is selected and raised otherwise.



Tk                      Last change: 4.0                        1






checkbutton(n)             Tk Commands             checkbutton(n)



     Name:           offValue
     Class:          Value
     Command-Line Switch:-offvalue

          Specifies value to store  in  the  button's  associated
          variable  whenever this button is deselected.  Defaults
          to ``0''.

     Name:           onValue
     Class:          Value
     Command-Line Switch:-onvalue

          Specifies value to store  in  the  button's  associated
          variable whenever this button is selected.  Defaults to
          ``1''.

     Name:           selectColor                                   |
     Class:          Background                                    |
     Command-Line Switch:-selectcolor                              |

                                                                |
          |                                                        |
          Specifies a background color to use when the button  is  |
          selected.  If indicatorOn is true then the color appli-  |
          cies to the indicator.  If indicatorOn is  false,  this  |
          color  is used as the background for the entire widget,  |
          in place of background  or  activeBackground,  whenever  |
          the  widget  is  selected.   If  specified  as an empty  |
          string then no special color  is  used  for  displaying  |
          when the widget is selected.                             |

     Name:           selectImage                                   |
     Class:          SelectImage                                   |
     Command-Line Switch:-selectimage                              |

                                                                |
          |                                                        |
          Specifies an image to display (in place  of  the  image  |
          option)  when the checkbutton is selected.  This option  |
          is ignored unless the image option has been specified.

     Name:           state
     Class:          State
     Command-Line Switch:-state

          Specifies one of  three  states  for  the  checkbutton:
          normal,  active,  or  disabled.   In  normal  state the
          checkbutton is displayed using the foreground and back-
          ground  options.   The  active  state is typically used
          when the pointer is over the  checkbutton.   In  active
          state   the   checkbutton   is   displayed   using  the
          activeForeground    and    activeBackground    options.



Tk                      Last change: 4.0                        2






checkbutton(n)             Tk Commands             checkbutton(n)



          Disabled  state  means that the checkbutton is insensi-
          tive:  it doesn't activate and doesn't respond to mouse
          button  presses.   In this state the disabledForeground
          and background options determine how the checkbutton is
          displayed.

     Name:           variable
     Class:          Variable
     Command-Line Switch:-variable

          Specifies name of global variable to  set  to  indicate
          whether  or  not  this button is selected.  Defaults to
          the name of the button within its parent (i.e. the last
          element of the button window's path name).

     Name:           width
     Class:          Width
     Command-Line Switch:-width

          Specifies a desired width for the button.  If an  image
          or  bitmap  is  being  displayed in the button then the
          value is in screen units (i.e. any of the forms accept-
          able  to  Tk_GetPixels);  for text it is in characters.
          If this option isn't specified,  the  button's  desired
          width  is computed from the size of the image or bitmap
          or text being displayed in it.
_________________________________________________________________


DESCRIPTION
     The checkbutton command creates a new window (given  by  the
     _p_a_t_h_N_a_m_e  argument)  and makes it into a checkbutton widget.
     Additional options, described above, may be specified on the
     command  line or in the option database to configure aspects
     of the checkbutton such as its colors, font, text, and  ini-
     tial  relief.   The checkbutton command returns its _p_a_t_h_N_a_m_e
     argument.  At the time this command is invoked,  there  must
     not  exist  a  window  named _p_a_t_h_N_a_m_e, but _p_a_t_h_N_a_m_e's parent
     must exist.

     A checkbutton is a widget that displays  a  textual  string,  |
     bitmap  or  image and a square called an _i_n_d_i_c_a_t_o_r.  If text  |
     is displayed, it must all be in a single font,  but  it  can  |
     occupy multiple lines on the screen (if it contains newlines  |
     or if wrapping occurs because of the wrapLength option)  and  |
     one of the characters may optionally be underlined using the  |
     underline option.  A checkbutton has all of the behavior  of
     a  simple  button,  including  the following: it can display
     itself in either of three different ways, according  to  the
     state  option;  it  can be made to appear raised, sunken, or
     flat; it can be made to flash; and it invokes a Tcl  command
     whenever mouse button 1 is clicked over the checkbutton.



Tk                      Last change: 4.0                        3






checkbutton(n)             Tk Commands             checkbutton(n)



     In addition, checkbuttons can be _s_e_l_e_c_t_e_d.  If a checkbutton  |
     is  selected  then  the  indicator  is normally drawn with a  |
     sunken relief and a special color, and a Tcl variable  asso-  |
     ciated  with  the  checkbutton  is set to a particular value  |
     (normally 1).  If the checkbutton is not selected, then  the  |
     indicator  is  drawn  with  a  raised  relief and no special  |
     color, and the associated variable is  set  to  a  different  |
     value  (typically  0).  By default, the name of the variable  |
     associated with a checkbutton is the same as the  _n_a_m_e  used  |
     to  create  the  checkbutton.   The  variable  name, and the  |
     ``on'' and ``off'' values stored in it, may be modified with  |
     options on the command line or in the option database.  Con-  |
     figuration options may also be used to modify  the  way  the  |
     indicator  is displayed (or whether it is displayed at all).
     By  default  a  checkbutton  is  configured  to  select  and
     deselect  itself  on  alternate button clicks.  In addition,
     each  checkbutton  monitors  its  associated  variable   and
     automatically  selects  and  deselects itself when the vari-
     ables value changes to and from the button's ``on'' value.


WIDGET COMMAND
     The checkbutton command creates a new Tcl command whose name
     is  _p_a_t_h_N_a_m_e.   This  command  may be used to invoke various
     operations on the widget.   It  has  the  following  general
     form:

          _p_a_t_h_N_a_m_e _o_p_t_i_o_n ?_a_r_g _a_r_g ...?

     _O_p_t_i_o_n and the _a_r_gs determine the exact behavior of the com-
     mand.   The  following commands are possible for checkbutton
     widgets:

     _p_a_t_h_N_a_m_e cget _o_p_t_i_o_n
          Returns the current value of the  configuration  option  |
          given  by  _o_p_t_i_o_n.   _O_p_t_i_o_n  may have any of the values  |
          accepted by the checkbutton command.

     _p_a_t_h_N_a_m_e configure ?_o_p_t_i_o_n? ?_v_a_l_u_e _o_p_t_i_o_n _v_a_l_u_e ...?
          Query  or  modify  the  configuration  options  of  the
          widget.   If  no  _o_p_t_i_o_n  is  specified, returns a list
          describing all of the available  options  for  _p_a_t_h_N_a_m_e
          (see  Tk_ConfigureInfo for information on the format of
          this list).  If _o_p_t_i_o_n is specified with no _v_a_l_u_e, then
          the  command  returns  a  list describing the one named
          option (this list will be identical to the  correspond-
          ing  sublist  of  the  value  returned  if no _o_p_t_i_o_n is
          specified).  If one or more  _o_p_t_i_o_n - _v_a_l_u_e  pairs  are
          specified,  then  the command modifies the given widget
          option(s) to have the given value(s);  in this case the
          command  returns  an empty string.  _O_p_t_i_o_n may have any
          of the values accepted by the checkbutton command.



Tk                      Last change: 4.0                        4






checkbutton(n)             Tk Commands             checkbutton(n)



     _p_a_t_h_N_a_m_e deselect
          Deselects the checkbutton and sets the associated vari-
          able to its ``off'' value.

     _p_a_t_h_N_a_m_e flash
          Flashes  the  checkbutton.   This  is  accomplished  by
          redisplaying the checkbutton several times, alternating
          between active and normal colors.  At the  end  of  the
          flash the checkbutton is left in the same normal/active
          state as when the command was invoked.  This command is
          ignored if the checkbutton's state is disabled.

     _p_a_t_h_N_a_m_e invoke
          Does just what would have happened if the user  invoked
          the  checkbutton  with  the mouse: toggle the selection
          state of the button and invoke the Tcl command  associ-
          ated with the checkbutton, if there is one.  The return
          value is the return value from the Tcl command,  or  an
          empty string if there is no command associated with the
          checkbutton.   This   command   is   ignored   if   the
          checkbutton's state is disabled.

     _p_a_t_h_N_a_m_e select
          Selects the checkbutton and sets the  associated  vari-
          able to its ``on'' value.

     _p_a_t_h_N_a_m_e toggle
          Toggles the selection state of the button, redisplaying
          it and modifying its associated variable to reflect the
          new state.


BINDINGS
     Tk automatically creates  class  bindings  for  checkbuttons
     that give them the following default behavior:

     [1]  A checkbutton activates whenever the mouse passes  over
          it and deactivates whenever the mouse leaves the check-
          button.

     [2]  When mouse button 1 is pressed over a checkbutton it is  |
          invoked  (its  selection  state toggles and the command  |
          associated with the button  is  invoked,  if  there  is  |
          one).                                                    |

     [3]                                                        |
          |                                                        |
          When a checkbutton has the input focus, the  space  key  |
          causes the checkbutton to be invoked.

     If the checkbutton's state is  disabled  then  none  of  the
     above  actions  occur:   the  checkbutton is completely non-



Tk                      Last change: 4.0                        5






checkbutton(n)             Tk Commands             checkbutton(n)



     responsive.

     The behavior of checkbuttons can be changed by defining  new
     bindings  for  individual widgets or by redefining the class
     bindings.


KEYWORDS
     checkbutton, widget














































Tk                      Last change: 4.0                        6




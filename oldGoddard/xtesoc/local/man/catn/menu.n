


menu(n)                    Tk Commands                    menu(n)



_________________________________________________________________

NAME
     menu - Create and manipulate menu widgets

SYNOPSIS
     menu _p_a_t_h_N_a_m_e ?_o_p_t_i_o_n_s?

STANDARD OPTIONS
     activeBackground               background      disabledForegroundrelief|
     activeBorderWidth              borderWidth     font
     activeForeground               cursor          foreground

     See the ``options'' manual entry for details on the standard
     options.

WIDGET-SPECIFIC OPTIONS
     Name:           postCommand
     Class:          Command
     Command-Line Switch:-postcommand

          If this option is specified then it provides a Tcl com-
          mand to execute each time the menu is posted.  The com-
          mand is invoked by the post widget command before post-
          ing the menu.

     Name:           selectColor
     Class:          Background                                    |
     Command-Line Switch:-selectcolor                              |

          For menu entries that are check buttons or  radio  but-
          tons, this option specifies the color to display in the
          indicator when the check  button  or  radio  button  is
          selected.

     Name:           tearOff                                       |
     Class:          TearOff                                       |
     Command-Line Switch:-tearoff                                  |

                                                                |
          |                                                        |
          This option must have a  proper  boolean  value,  which  |
          specifies  whether  or  not  the  menu should include a  |
          tear-off entry at the top.  If so,  it  will  exist  as  |
          entry  0  of the menu and the other entries will number  |
          starting at 1.  The default menu bindings  arrange  for  |
          the  menu  to  be  torn  off when the tear-off entry is  |
          invoked.
_________________________________________________________________






Tk                      Last change: 4.0                        1






menu(n)                    Tk Commands                    menu(n)



INTRODUCTION
     The menu command creates a new top-level  window  (given  by
     the  _p_a_t_h_N_a_m_e  argument)  and  makes  it into a menu widget.
     Additional options, described above, may be specified on the
     command  line or in the option database to configure aspects
     of the menu such as its colors and font.  The  menu  command
     returns  its _p_a_t_h_N_a_m_e argument.  At the time this command is
     invoked, there must not exist a window named  _p_a_t_h_N_a_m_e,  but
     _p_a_t_h_N_a_m_e's parent must exist.

     A menu is a widget that displays a  collection  of  one-line
     entries arranged in a column.  There exist several different
     types of entries, each with different  properties.   Entries
     of  different  types may be combined in a single menu.  Menu
     entries are not the same as entry widgets.   In  fact,  menu
     entries  are  not  even distinct widgets; the entire menu is
     one widget.

     Menu entries are displayed with up to three separate fields.  |
     The  main  field  is a label in the form of a text string, a  |
     bitmap, or an image, controlled by the -label, -bitmap,  and  |
     - image  options for the entry.  If the  -accelerator option
     is specified for an entry then a  second  textual  field  is
     displayed  to the right of the label.  The accelerator typi-
     cally describes a keystroke sequence that may  be  typed  in
     the  application  to  cause  the same result as invoking the
     menu entry.  The third field is an _i_n_d_i_c_a_t_o_r.  The indicator
     is  present only for checkbutton or radiobutton entries.  It
     indicates whether the entry  is  selected  or  not,  and  is
     displayed to the left of the entry's string.

     In normal use, an entry becomes active (displays itself dif-
     ferently)  whenever the mouse pointer is over the entry.  If
     a mouse button is released over the entry then the entry  is
     _i_n_v_o_k_e_d.   The  effect  of  invocation is different for each
     type of entry; these effects are described below in the sec-
     tions on individual entries.

     Entries may be  _d_i_s_a_b_l_e_d,  which  causes  their  labels  and
     accelerators to be displayed with dimmer colors.  A disabled
     entry cannot be activated or invoked.  Disabled entries  may
     be  re-enabled,  at  which  point  it  becomes  possible  to
     activate and invoke them again.


COMMAND ENTRIES
     The most common kind of menu entry is a command entry, which
     behaves  much like a button widget.  When a command entry is
     invoked, a Tcl command is  executed.   The  Tcl  command  is
     specified with the -command option.





Tk                      Last change: 4.0                        2






menu(n)                    Tk Commands                    menu(n)



SEPARATOR ENTRIES
     A separator is an entry that is displayed  as  a  horizontal
     dividing line.  A separator may not be activated or invoked,
     and it has no behavior other than its display appearance.


CHECKBUTTON ENTRIES
     A checkbutton menu entry behaves  much  like  a  checkbutton
     widget.   When  it  is  invoked  it  toggles  back and forth
     between the selected and deselected states.  When the  entry
     is  selected,  a  particular value is stored in a particular
     global variable (as determined by the -onvalue and -variable
     options  for  the  entry);   when  the  entry  is deselected
     another value (determined by the -offvalue option) is stored
     in  the  global  variable.  An indicator box is displayed to
     the left of the label in a checkbutton entry.  If the  entry
     is  selected then the indicator's center is displayed in the
     color given by the -selectcolor option for the entry; other-
     wise  the  indicator's center is displayed in the background
     color for the menu.  If a -command option is specified for a
     checkbutton entry, then its value is evaluated as a Tcl com-
     mand each time the entry is  invoked;   this  happens  after
     toggling the entry's selected state.


RADIOBUTTON ENTRIES
     A radiobutton menu entry behaves  much  like  a  radiobutton
     widget.   Radiobutton  entries  are  organized  in groups of
     which only one entry may be selected at a time.  Whenever  a
     particular  entry  becomes  selected  it stores a particular
     value into a particular global variable  (as  determined  by
     the  - value  and  - variable  options for the entry).  This
     action causes any  previously-selected  entry  in  the  same
     group   to  deselect  itself.   Once  an  entry  has  become
     selected, any change to the entry's associated variable will
     cause the entry to deselect itself.  Grouping of radiobutton
     entries is determined by their associated variables:  if two
     entries  have  the same associated variable then they are in
     the same group.  An indicator diamond is  displayed  to  the
     left  of  the label in each radiobutton entry.  If the entry
     is selected then the indicator's center is displayed in  the
     color given by the -selectcolor option for the entry; other-
     wise the indicator's center is displayed in  the  background
     color for the menu.  If a -command option is specified for a
     radiobutton entry, then its value is evaluated as a Tcl com-
     mand  each  time  the  entry is invoked;  this happens after
     selecting the entry.


CASCADE ENTRIES
     A cascade entry is one with an associated  menu  (determined
     by   the   -  menu   option).   Cascade  entries  allow  the



Tk                      Last change: 4.0                        3






menu(n)                    Tk Commands                    menu(n)



     construction  of  cascading  menus.   When  the   entry   is
     activated,  the  associated menu is posted just to the right
     of the entry; that menu remains  posted  until  the  higher-
     level  menu  is  unposted  or  until  some  other  entry  is
     activated in the higher-level  menu.   The  associated  menu
     should  normally  be a child of the menu containing the cas-
     cade entry, in order for menu traversal to work correctly.

     A cascade entry posts its associated menu by invoking a  Tcl
     command of the form

               _m_e_n_u post _x _y

     where _m_e_n_u is the path name of the associated  menu,  and  _x
     and  _y  are  the  root-window coordinates of the upper-right
     corner of  the  cascade  entry.   The  lower-level  menu  is
     unposted by executing a Tcl command with the form

               _m_e_n_u unpost

     where _m_e_n_u is the name of the associated menu.

     If a -command option is specified for a cascade  entry  then
     it  is  evaluated  as a Tcl command each time the associated
     menu is posted (the evaluation occurs  before  the  menu  is
     posted).


TEAR-OFF ENTRIES
     A tear-off entry appears at the top of the menu  if  enabled  |
     with  the tearOff option.  It is not like other menu entries  |
     in that it cannot be created with the add widget command and  |
     cannot  be  deleted  with the delete widget command.  When a  |
     tear-off entry is created it appears as a dashed line at the  |
     top  of  the menu.  Under the default bindings, invoking the  |
     tear-off entry causes a torn-off copy to be made of the menu  |
     and all of its submenus.


WIDGET COMMAND
     The menu command creates a new Tcl  command  whose  name  is
     _p_a_t_h_N_a_m_e.  This command may be used to invoke various opera-
     tions on the widget.  It has the following general form:

          _p_a_t_h_N_a_m_e _o_p_t_i_o_n ?_a_r_g _a_r_g ...?

     _O_p_t_i_o_n and the _a_r_gs determine the exact behavior of the com-
     mand.

     Many of the widget commands for a menu take as one  argument
     an  indicator  of  which  entry  of  the menu to operate on.
     These indicators are called _i_n_d_e_xes and may be specified  in



Tk                      Last change: 4.0                        4






menu(n)                    Tk Commands                    menu(n)



     any of the following forms:

     _n_u_m_b_e_r      Specifies  the  entry   numerically,   where   0
                 corresponds to the top-most entry of the menu, 1
                 to the entry below it, and so on.

     active      Indicates the entry that  is  currently  active.
                 If   no  entry  is  active  then  this  form  is
                 equivalent to none.  This form may not be abbre-
                 viated.

     last        Indicates the bottommost entry in the menu.   If
                 there  are no entries in the menu then this form
                 is equivalent to none.  This  form  may  not  be
                 abbreviated.

     none        Indicates ``no entry at  all'';   this  is  used
                 most  commonly with the activate option to deac-
                 tivate all the entries in  the  menu.   In  most
                 cases  the  specification of none causes nothing
                 to happen in the widget command.  This form  may
                 not be abbreviated.

     @_n_u_m_b_e_r     In  this  form,  _n_u_m_b_e_r  is  treated  as  a   y-
                 coordinate  in  the  menu's  window;   the entry  |
                 closest to that y-coordinate is used.  For exam-
                 ple,  ``@0'' indicates the top-most entry in the
                 window.

     _p_a_t_t_e_r_n     If the index doesn't satisfy one  of  the  above
                 forms  then  this  form  is  used.   _P_a_t_t_e_r_n  is
                 pattern-matched against the label of each  entry
                 in the menu, in order from the top down, until a
                 matching  entry  is   found.    The   rules   of
                 Tcl_StringMatch are used.

     The following widget commands are possible for menu widgets:

     _p_a_t_h_N_a_m_e activate _i_n_d_e_x
          Change the state of the entry  indicated  by  _i_n_d_e_x  to
          active  and  redisplay it using its active colors.  Any
          previously-active entry is deactivated.   If  _i_n_d_e_x  is
          specified  as  none,  or if the specified entry is dis-
          abled, then the menu ends  up  with  no  active  entry.
          Returns an empty string.

     _p_a_t_h_N_a_m_e add _t_y_p_e ?_o_p_t_i_o_n _v_a_l_u_e _o_p_t_i_o_n _v_a_l_u_e ...?
          Add a new entry to the bottom of  the  menu.   The  new
          entry's  type  is given by _t_y_p_e and must be one of cas-
          cade, checkbutton, command, radiobutton, or  separator,
          or a unique abbreviation of one of the above.  If addi-
          tional arguments are present, they specify any  of  the



Tk                      Last change: 4.0                        5






menu(n)                    Tk Commands                    menu(n)



          following options:

          -activebackground _v_a_l_u_e
               Specifies a background color to use for displaying
               this  entry  when it is active.  If this option is
               specified as an empty string (the  default),  then
               the  activeBackground  option for the overall menu
               is used.  This option is not available for separa-
               tor or tear-off entries.

          -activeforeground _v_a_l_u_e
               Specifies a foreground color to use for displaying
               this  entry  when it is active.  If this option is
               specified as an empty string (the  default),  then
               the  activeForeground  option for the overall menu
               is used.  This option is not available for separa-
               tor or tear-off entries.

          -accelerator _v_a_l_u_e
               Specifies a string to display at the right side of
               the menu entry.  Normally describes an accelerator
               keystroke sequence that may be typed to invoke the
               same  function  as the menu entry.  This option is
               not available for separator or tear-off entries.

          -background _v_a_l_u_e
               Specifies a background color to use for displaying
               this entry when it is in the normal state (neither
               active nor disabled).  If this option is specified
               as  an  empty string (the default), then the back-
               ground option for the overall menu is used.   This
               option  is not available for separator or tear-off
               entries.

          -bitmap _v_a_l_u_e
               Specifies a bitmap to display in the menu  instead
               of  a  textual label, in any of the forms accepted
               by Tk_GetBitmap.  This option overrides the -label
               option  but  may  be  reset  to an empty string to
               enable a textual label to be displayed.   If  a  -
               image  option  has  been specified, it overrides -
               bitmap.  This option is not available for  separa-
               tor or tear-off entries.

          -command _v_a_l_u_e
               For command, checkbutton, and radiobutton entries,
               specifies  a  Tcl command to execute when the menu
               entry is invoked.  For cascade entries,  specifies
               a  Tcl  command  to  execute  when  the  entry  is
               activated  (i.e.  just  before  its   submenu   is
               posted).   Not available for separator or tear-off
               entries.



Tk                      Last change: 4.0                        6






menu(n)                    Tk Commands                    menu(n)



          -font _v_a_l_u_e
               Specifies the font to use when drawing  the  label
               or  accelerator  string  in  this  entry.  If this
               option  is  specified  as  an  empty  string  (the
               default) then the font option for the overall menu
               is used.  This option is not available for separa-
               tor or tear-off entries.

          -foreground _v_a_l_u_e
               Specifies a foreground color to use for displaying
               this entry when it is in the normal state (neither
               active nor disabled).  If this option is specified
               as  an  empty string (the default), then the fore-
               ground option for the overall menu is used.   This
               option  is not available for separator or tear-off
               entries.

          -image _v_a_l_u_e
               Specifies an image to display in the menu  instead  |
               of  a  text  string  or bitmap The image must have  |
               been created by some previous invocation of  image  |
               create.   This  option  overrides the -label and -  |
               bitmap options but may be reset to an empty string  |
               to   enable  a  textual  or  bitmap  label  to  be  |
               displayed.   This  option  is  not  available  for  |
               separator or tear-off entries.                      |

                                                                -                                                        |
               indicatoron _v_a_l_u_e                                |
               |                                                   |
               Available only  for  checkbutton  and  radiobutton  |
               entries.   _V_a_l_u_e  is  a  boolean  that  determines  |
               whether or not the indicator should be displayed.

          -label _v_a_l_u_e
               Specifies a string to display  as  an  identifying
               label  in  the  menu  entry.   Not  available  for
               separator or tear-off entries.

          -menu _v_a_l_u_e
               Available only for cascade entries.  Specifies the
               path  name  of  the  submenu  associated with this
               entry.  The submenu must be a child of the menu.    |

          -offvalue _v_a_l_u_e
               Available only for checkbutton entries.  Specifies
               the value to store in the entry's associated vari-
               able when the entry is deselected.

          -onvalue _v_a_l_u_e
               Available only for checkbutton entries.  Specifies
               the  value  to  store  in  the  entry's associated



Tk                      Last change: 4.0                        7






menu(n)                    Tk Commands                    menu(n)



               variable when the entry is selected.

          -selectcolor _v_a_l_u_e
               Available only  for  checkbutton  and  radiobutton  |
               entries.   Specifies  the  color to display in the  |
               indicator when the  entry  is  selected.   If  the  |
               value  is  an  empty string (the default) then the  |
               selectColor option for  the  menu  determines  the  |
               indicator color.                                    |

                                                                -                                                        |
               selectimage _v_a_l_u_e                                |
               |                                                   |
               Available only  for  checkbutton  and  radiobutton  |
               entries.   Specifies  an  image  to display in the  |
               entry (in place of the -image option) when  it  is  |
               selected.   _V_a_l_u_e  is  the name of an image, which  |
               must have been created by some previous invocation  |
               of  image  create.   This option is ignored unless  |
               the -image option has been specified.

          -state _v_a_l_u_e
               Specifies one of three states for the entry:  nor-
               mal,  active,  or  disabled.   In normal state the
               entry is displayed using the foreground option for
               the  menu and the background option from the entry
               or the menu.  The active state is  typically  used
               when  the  pointer  is  over the entry.  In active
               state the entry is displayed using the activeFore-
               ground   option   for  the  menu  along  with  the
               activebackground option from the entry.   Disabled
               state  means  that  the  entry is insensitive:  it
               doesn't activate and doesn't respond to mouse but-
               ton  presses or releases.  In this state the entry
               is displayed according to  the  disabledForeground
               option for the menu and the background option from
               the entry.   This  option  is  not  available  for
               separator entries.

          -underline _v_a_l_u_e
               Specifies the integer  index  of  a  character  to
               underline  in  the  entry.   This  option  is also
               queried by the default bindings and used to imple-
               ment  keyboard  traversal.   0  corresponds to the
               first character  of  the  text  displayed  in  the
               entry,  1  to the next character, and so on.  If a
               bitmap or image is displayed  in  the  entry  then
               this option is ignored.  This option is not avail-
               able for separator or tear-off entries.

          -value _v_a_l_u_e
               Available only for radiobutton entries.  Specifies



Tk                      Last change: 4.0                        8






menu(n)                    Tk Commands                    menu(n)



               the value to store in the entry's associated vari-
               able when the entry is selected.

          -variable _v_a_l_u_e
               Available only  for  checkbutton  and  radiobutton
               entries.   Specifies the name of a global value to
               set when the entry is selected.   For  checkbutton
               entries the variable is also set when the entry is
               deselected.  For radiobutton entries, changing the
               variable  causes  the  currently-selected entry to
               deselect itself.

          The add widget command returns an empty string.

     _p_a_t_h_N_a_m_e cget _o_p_t_i_o_n
          Returns the current value of the  configuration  option  |
          given  by  _o_p_t_i_o_n.   _O_p_t_i_o_n  may have any of the values  |
          accepted by the menu command.

     _p_a_t_h_N_a_m_e configure ?_o_p_t_i_o_n? ?_v_a_l_u_e _o_p_t_i_o_n _v_a_l_u_e ...?
          Query  or  modify  the  configuration  options  of  the
          widget.   If  no  _o_p_t_i_o_n  is  specified, returns a list
          describing all of the available  options  for  _p_a_t_h_N_a_m_e
          (see  Tk_ConfigureInfo for information on the format of
          this list).  If _o_p_t_i_o_n is specified with no _v_a_l_u_e, then
          the  command  returns  a  list describing the one named
          option (this list will be identical to the  correspond-
          ing  sublist  of  the  value  returned  if no _o_p_t_i_o_n is
          specified).  If one or more  _o_p_t_i_o_n - _v_a_l_u_e  pairs  are
          specified,  then  the command modifies the given widget
          option(s) to have the given value(s);  in this case the
          command  returns  an empty string.  _O_p_t_i_o_n may have any
          of the values accepted by the menu command.

     _p_a_t_h_N_a_m_e delete _i_n_d_e_x_1 ?_i_n_d_e_x_2?
          Delete all of  the  menu  entries  between  _i_n_d_e_x_1  and
          _i_n_d_e_x_2   inclusive.   If  _i_n_d_e_x_2  is  omitted  then  it
          defaults to _i_n_d_e_x_1.  Attempts to delete a tear-off menu  |
          entry are ignored (instead, you should change the tear-  |
          Off option to remove the tear-off entry).                |

     _p_a_t_h_N_a_m_e entrycget _i_n_d_e_x _o_p_t_i_o_n                            |
          |                                                        |
          Returns the current value of a configuration option for  |
          the  entry  given by _i_n_d_e_x.  _O_p_t_i_o_n may have any of the  |
          values accepted by the add widget command.

     _p_a_t_h_N_a_m_e entryconfigure _i_n_d_e_x ?_o_p_t_i_o_n_s?
          This command  is  similar  to  the  configure  command,
          except that it applies to the options for an individual
          entry, whereas configure applies to the options for the
          menu  as  a  whole.  _O_p_t_i_o_n_s may have any of the values



Tk                      Last change: 4.0                        9






menu(n)                    Tk Commands                    menu(n)



          accepted by the add widget  command.   If  _o_p_t_i_o_n_s  are
          specified,  options  are  modified  as indicated in the
          command and the command returns an empty string.  If no
          _o_p_t_i_o_n_s  are  specified,  returns a list describing the
          current options for entry _i_n_d_e_x  (see  Tk_ConfigureInfo
          for information on the format of this list).

     _p_a_t_h_N_a_m_e index _i_n_d_e_x
          Returns the numerical index corresponding to _i_n_d_e_x,  or
          none if _i_n_d_e_x was specified as none.

     _p_a_t_h_N_a_m_e invoke _i_n_d_e_x
          Invoke the action of the menu entry.  See the  sections
          on  the  individual  entries  above for details on what
          happens.  If the menu entry is  disabled  then  nothing
          happens.  If the entry has a command associated with it
          then the result of that  command  is  returned  as  the
          result  of  the  invoke  widget command.  Otherwise the
          result is an empty  string.   Note:   invoking  a  menu
          entry  does  not  automatically  unpost  the menu;  the
          default bindings normally  take  care  of  this  before
          invoking the invoke widget command.

     _p_a_t_h_N_a_m_e post _x _y
          Arrange for the menu to be displayed on the  screen  at
          the  root-window  coordinates  given by _x and _y.  These
          coordinates are adjusted if necessary to guarantee that
          the entire menu is visible on the screen.  This command
          normally returns an empty string.  If  the  postCommand
          option  has  been specified, then its value is executed
          as a Tcl script before posting the menu and the  result
          of  that  script  is returned as the result of the post
          widget command.  If an error  returns  while  executing
          the command, then the error is returned without posting
          the menu.

     _p_a_t_h_N_a_m_e type _i_n_d_e_x
          Returns the type of the  menu  entry  given  by  _i_n_d_e_x.  |
          This is the _t_y_p_e argument passed to the add widget com-  |
          mand when the entry was created,  such  as  command  or  |
          separator, or tearoff for a tear-off entry.

     _p_a_t_h_N_a_m_e unpost
          Unmap the window so that it is no longer displayed.  If
          a  lower-level  cascaded  menu  is  posted, unpost that
          menu.  Returns an empty string.

     _p_a_t_h_N_a_m_e yposition _i_n_d_e_x
          Returns a decimal string giving the y-coordinate within
          the  menu  window  of  the  topmost  pixel in the entry
          specified by _i_n_d_e_x.




Tk                      Last change: 4.0                       10






menu(n)                    Tk Commands                    menu(n)



MENU CONFIGURATIONS
     The default bindings support four different  ways  of  using  |
     menus:                                                        |

     Pulldown Menus                                             |
          |                                                        |
          This is the most common case.  You create one  menubut-  |
          ton  widget  for each top-level menu, and typically you  |
          arrange a series of menubuttons in a row in  a  menubar  |
          window.   You  also  create the top-level menus and any  |
          cascaded submenus, and tie them  together  with  - menu  |
          options  in  menubuttons and cascade menu entries.  The  |
          top-level menu must be a child of the  menubutton,  and  |
          each submenu must be a child of the menu that refers to  |
          it.  Once you have done this, the default bindings will  |
          allow  users  to  traverse and invoke the tree of menus  |
          via its menubutton;  see the  menubutton  manual  entry  |
          for details.                                             |

     Popup Menus                                                |
          |                                                        |
          Popup menus typically post in response to a mouse  but-  |
          ton press or keystroke.  You create the popup menus and  |
          any cascaded submenus, then you call the tk_popup  pro-  |
          cedure  at  the  appropriate time to post the top-level  |
          menu.                                                    |

     Option Menus                                               |
          |                                                        |
          An option menu consists of a menubutton with an associ-  |
          ated  menu  that  allows  you  to select one of several  |
          values.  The current value is displayed in the menubut-  |
          ton  and  is also stored in a global variable.  Use the  |
          tk_optionMenu procedure to  create  option  menubuttons  |
          and their menus.                                         |

     Torn-  |
          off Menus                                             |
          |                                                        |
          You create a torn-off menu  by  invoking  the  tear-off  |
          entry  at  the  top  of  an existing menu.  The default  |
          bindings will create a new menu that is a copy  of  the  |
          original  menu  and  leave  it  permanently posted as a  |
          top-level window.  The torn-off menu behaves  just  the  |
          same as the original menu.                               |


DEFAULT BINDINGS                                                   |
     Tk automatically creates class bindings for menus that  give  |
     them the following default behavior:                          |

     [1]                                                        |



Tk                      Last change: 4.0                       11






menu(n)                    Tk Commands                    menu(n)



          |When the mouse enters a menu, the entry underneath the  |
          mouse cursor activates;  as the mouse moves around  the  |
          menu, the active entry changes to track the mouse.       |

     [2]                                                        |
          |                                                        |
          When the mouse leaves a menu all of the entries in  the  |
          menu  deactivate,  except in the special case where the  |
          mouse moves from a menu to a cascaded submenu.           |

     [3]                                                        |
          |                                                        |
          When a button is released over a menu, the active entry  |
          (if  any)  is invoked.  The menu also unposts unless it  |
          is a torn-off menu.                                      |

     [4]                                                        |
          |                                                        |
          The Space and Return keys invoke the active  entry  and  |
          unpost the menu.                                         |

     [5]                                                        |
          |                                                        |
          If any of the entries in a menu have letters underlined  |
          with  with  -underline option, then pressing one of the  |
          underlined letters (or  its  upper-case  or  lower-case  |
          equivalent) invokes that entry and unposts the menu.     |

     [6]                                                        |
          |                                                        |
          The Escape key aborts  a  menu  selection  in  progress  |
          without  invoking  any entry.  It also unposts the menu  |
          unless it is a torn-off menu.                            |

     [7]                                                        |
          |                                                        |
          The Up and Down keys activate the next higher or  lower  |
          entry  in  the  menu.   When  one  end  of  the menu is  |
          reached, the active entry wraps  around  to  the  other  |
          end.                                                     |

     [8]                                                        |
          |                                                        |
          The Left key moves to the next menu to  the  left.   If  |
          the  current  menu is a cascaded submenu, then the sub-  |
          menu is unposted and the current menu entry becomes the  |
          cascade  entry in the parent.  If the current menu is a  |
          top-level menu  posted  from  a  menubutton,  then  the  |
          current  menubutton is unposted and the next menubutton  |
          to the left  is  posted.   Otherwise  the  key  has  no  |
          effect.   The left-right order of menubuttons is deter-  |
          mined by their stacking order:   Tk  assumes  that  the  |



Tk                      Last change: 4.0                       12






menu(n)                    Tk Commands                    menu(n)



          lowest  menubutton  (which  by default is the first one  |
          created) is on the left.                                 |

     [9]                                                        |
          |                                                        |
          The Right key moves to the next menu to the right.   If  |
          the  current entry is a cascade entry, then the submenu  |
          is posted and the  current menu entry becomes the first  |
          entry  in  the submenu.  Otherwise, if the current menu  |
          was posted from a menubutton, then the current menubut-  |
          ton is unposted and the next menubutton to the right is  |
          posted.

     Disabled  menu  entries  are  non-responsive:   they   don't
     activate and they ignore mouse button presses and releases.

     The behavior of menus can be changed by defining  new  bind-
     ings for individual widgets or by redefining the class bind-
     ings.


BUGS
     At present it isn't possible to use the option  database  to
     specify values for the options to individual entries.


KEYWORDS
     menu, widget



























Tk                      Last change: 4.0                       13







scan(n)               Tcl Built-In Commands               scan(n)



_________________________________________________________________

NAME
     scan - Parse string using conversion specifiers in the style
     of sscanf

SYNOPSIS
     scan _s_t_r_i_n_g _f_o_r_m_a_t _v_a_r_N_a_m_e ?_v_a_r_N_a_m_e ...?
_________________________________________________________________


INTRODUCTION
     This command parses fields from an input string in the  same
     fashion  as  the ANSI C sscanf procedure and returns a count
     of the number of conversions performed, or -1 if the end  of  |
     the input string is reached before any conversions have been  |
     performed.  _S_t_r_i_n_g gives the input to be parsed  and  _f_o_r_m_a_t
     indicates  how to parse it, using % conversion specifiers as
     in sscanf.  Each _v_a_r_N_a_m_e gives the name of a variable;  when
     a  field is scanned from _s_t_r_i_n_g the result is converted back
     into a string and assigned to the corresponding variable.


DETAILS ON SCANNING
     Scan operates by scanning _s_t_r_i_n_g and _f_o_r_m_a_t_S_t_r_i_n_g  together.
     If the next character in _f_o_r_m_a_t_S_t_r_i_n_g is a blank or tab then
     it matches any number of white space  characters  in  _s_t_r_i_n_g
     (including zero).  Otherwise, if it isn't a % character then
     it must match the next character of _s_t_r_i_n_g.   When  a  %  is
     encountered  in  _f_o_r_m_a_t_S_t_r_i_n_g,  it  indicates the start of a
     conversion specifier.  A conversion specifier contains three
     fields after the %:  a *, which indicates that the converted
     value is to be discarded instead of assigned to a  variable;
     a  number indicating a maximum field width; and a conversion
     character.  All of these fields are optional except for  the
     conversion character.

     When scan finds a conversion specifier in  _f_o_r_m_a_t_S_t_r_i_n_g,  it
     first  skips  any white-space characters in _s_t_r_i_n_g.  Then it
     converts the next input characters according to the  conver-
     sion  specifier  and stores the result in the variable given
     by the next argument  to  scan.   The  following  conversion
     characters are supported:

     d         The input field must be a decimal integer.  It  is
               read in and the value is stored in the variable as
               a decimal string.

     o         The input field must be an octal  integer.  It  is
               read in and the value is stored in the variable as
               a decimal string.




Tcl                      Last change:                           1






scan(n)               Tcl Built-In Commands               scan(n)



     x         The input field must be a hexadecimal integer.  It
               is read in and the value is stored in the variable
               as a decimal string.

     c         A single character is read in and its binary value
               is  stored  in  the  variable as a decimal string.
               Initial white space is not skipped in  this  case,
               so the input field may be a white-space character.
               This conversion is different from the  ANSI  stan-
               dard  in that the input field always consists of a
               single character and no field width may be  speci-
               fied.

     s         The input field consists of all the characters  up
               to  the next white-space character; the characters
               are copied to the variable.

     e or f or g
               The input field must be  a  floating-point  number
               consisting  of  an  optional  sign,  a  string  of
               decimal digits  possibly  con  taining  a  decimal
               point, and an optional exponent consisting of an e
               or E followed by an optional sign and a string  of
               decimal  digits.   It is read in and stored in the
               variable as a floating-point string.

     [_c_h_a_r_s]   The input field consists of any number of  charac-
               ters  in  _c_h_a_r_s.  The matching string is stored in
               the variable.  If the first character between  the
               brackets  is  a  ]  then  it is treated as part of
               _c_h_a_r_s rather than the closing bracket for the set.

     [^_c_h_a_r_s]  The input field consists of any number of  charac-
               ters  not in _c_h_a_r_s.  The matching string is stored
               in the variable.   If  the  character  immediately
               following  the ^ is a ] then it is treated as part
               of the set rather than the closing bracket for the
               set.

     The number of characters read from the input for  a  conver-
     sion is the largest number that makes sense for that partic-
     ular conversion (e.g.  as many decimal  digits  as  possible
     for %d, as many octal digits as possible for %o, and so on).
     The input field for a  given  conversion  terminates  either
     when a white-space character is encountered or when the max-
     imum field width has been reached,  whichever  comes  first.
     If  a * is present in the conversion specifier then no vari-
     able is assigned and the next scan argument is not consumed.


DIFFERENCES FROM ANSI SSCANF




Tcl                      Last change:                           2






scan(n)               Tcl Built-In Commands               scan(n)



     The behavior of the scan command is the same as the behavior
     of  the  ANSI  C  sscanf  procedure except for the following
     differences:

     [1]  %p and %n conversion specifiers are not currently  sup-  |
          ported.

     [2]  For %c conversions a single  character  value  is  con-
          verted  to  a decimal string, which is then assigned to
          the corresponding _v_a_r_N_a_m_e; no field width may be speci-
          fied for this conversion.

     [3]  The l, h, and L modifiers are ignored;  integer  values  |
          are  always  converted  as  if  there  were no modifier  |
          present and real values are always converted as if  the  |
          l  modifier  were present (i.e. type double is used for  |
          the internal representation).


KEYWORDS
     conversion specifier, parse, scan


































Tcl                      Last change:                           3




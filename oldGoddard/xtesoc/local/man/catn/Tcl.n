


Tcl(n)                Tcl Built-In Commands                Tcl(n)



_________________________________________________________________

NAME
     Tcl - Summary of Tcl language syntax.
_________________________________________________________________


DESCRIPTION
     The following rules define the syntax and semantics  of  the
     Tcl language:

     [1]  A Tcl script is a string containing one  or  more  com-
          mands.  Semi-colons and newlines are command separators
          unless quoted as described below.  Close  brackets  are
          command  terminators  during  command substitution (see
          below) unless quoted.

     [2]  A command is evaluated in two steps.   First,  the  Tcl
          interpreter  breaks the command into _w_o_r_d_s and performs
          substitutions as described below.  These  substitutions
          are  performed  in  the same way for all commands.  The
          first word is used to locate  a  command  procedure  to
          carry  out  the  command,  then all of the words of the
          command are passed to the command procedure.  The  com-
          mand  procedure  is free to interpret each of its words
          in any way it likes, such as an integer, variable name,
          list,  or  Tcl  script.   Different  commands interpret
          their words differently.

     [3]  Words of a command are separated by white space (except
          for newlines, which are command separators).

     [4]  If the  first  character  of  a  word  is  double-quote
          (``"'')  then  the  word  is  terminated  by  the  next
          double-quote character.  If semi-colons,  close  brack-
          ets,  or  white  space  characters (including newlines)
          appear between the quotes  then  they  are  treated  as
          ordinary  characters and included in the word.  Command
          substitution, variable substitution, and backslash sub-
          stitution  are  performed on the characters between the
          quotes as described below.  The double-quotes  are  not
          retained as part of the word.

     [5]  If the first character of  a  word  is  an  open  brace
          (``{'')  then  the  word  is terminated by the matching
          close brace (``}'').  Braces nest within the word:  for
          each  additional open brace there must be an additional
          close brace (however, if an open brace or  close  brace
          within  the  word is quoted with a backslash then it is
          not counted in locating the matching close brace).   No
          substitutions  are  performed on the characters between
          the braces except for  backslash-newline  substitutions



Tcl                      Last change:                           1






Tcl(n)                Tcl Built-In Commands                Tcl(n)



          described  below,  nor  do semi-colons, newlines, close
          brackets, or white space receive any special  interpre-
          tation.   The  word will consist of exactly the charac-
          ters between the outer braces, not including the braces
          themselves.

     [6]  If a word contains an open  bracket  (``['')  then  Tcl
          performs  _c_o_m_m_a_n_d  _s_u_b_s_t_i_t_u_t_i_o_n.  To do this it invokes
          the Tcl interpreter recursively to process the  charac-
          ters  following  the open bracket as a Tcl script.  The
          script may contain any number of commands and  must  be
          terminated  by  a close bracket (``]'').  The result of
          the script (i.e. the result of  its  last  command)  is
          substituted  into the word in place of the brackets and
          all of the characters between them.  There may  be  any
          number of command substitutions in a single word.  Com-
          mand substitution is not performed on words enclosed in
          braces.

     [7]  If a word contains a dollar-sign (``$'') then Tcl  per-
          forms  _v_a_r_i_a_b_l_e  _s_u_b_s_t_i_t_u_t_i_o_n:  the dollar-sign and the
          following characters are replaced in the  word  by  the
          value  of a variable.  Variable substition may take any
          of the following forms:

          $_n_a_m_e          _N_a_m_e is the name of a  scalar  variable;
                         the  name is terminated by any character
                         that isn't a letter,  digit,  or  under-
                         score.

          $_n_a_m_e(_i_n_d_e_x)   _N_a_m_e gives the name of an array variable
                         and  _i_n_d_e_x  gives the name of an element
                         within that array.   _N_a_m_e  must  contain
                         only  letters,  digits, and underscores.
                         Command substitutions, variable  substi-
                         tutions, and backslash substitutions are
                         performed on the characters of _i_n_d_e_x.

          ${_n_a_m_e}        _N_a_m_e is the name of a  scalar  variable.
                         It may contain any characters whatsoever
                         except for close braces.

     There may be any number of variable substitutions in a  sin-
     gle  word.   Variable substitution is not performed on words
     enclosed in braces.

     [8]  If a backslash  (``\'')  appears  within  a  word  then
          _b_a_c_k_s_l_a_s_h  _s_u_b_s_t_i_t_u_t_i_o_n occurs.  In all cases but those  |
          described below the backslash is dropped and  the  fol-  |
          lowing  character  is  treated as an ordinary character  |
          and included in the word.  This allows characters  such
          as  double  quotes, close brackets, and dollar signs to



Tcl                      Last change:                           2






Tcl(n)                Tcl Built-In Commands                Tcl(n)



          be included in words without  triggering  special  pro-
          cessing.   The  following  table  lists  the  backslash
          sequences that are handled specially,  along  with  the
          value that replaces each sequence.

          \a                                                    |
                |                                                  |
                Audible alert (bell) (0x7).

          \b    Backspace (0x8).

          \f    Form feed (0xc).

          \n    Newline (0xa).

          \r    Carriage-return (0xd).

          \t    Tab (0x9).

          \v    Vertical tab (0xb).

          \<newline>_w_h_i_t_e_S_p_a_c_e
                A single space character replaces the  backslash,  |
                newline,  and  all white space after the newline.  |
                This backslash sequence is unique in that  it  is  |
                replaced  in  a separate pre-pass before the com-  |
                mand is actually parsed.  This means that it will  |
                be  replaced  even when it occurs between braces,  |
                and the resulting space will be treated as a word  |
                separator if it isn't in braces or quotes.

          \\    Backslash (``\'').

          \_o_o_o  The digits _o_o_o (one, two, or three of them)  give
                the octal value of the character.

          \x_h_h  The hexadecimal digits _h_h  give  the  hexadecimal  |
                value of the character.  Any number of digits may  |
                be present.

     Backslash substitution is not performed on words enclosed in
     braces, except for backslash-newline as described above.

     [9]  If a hash character (``#'') appears at  a  point  where
          Tcl  is expecting the first character of the first word
          of a command, then the hash character and  the  charac-
          ters  that  follow it, up through the next newline, are
          treated as a comment and ignored.  The comment  charac-
          ter only has significance when it appears at the begin-
          ning of a command.

     [10] Each character is processed exactly  once  by  the  Tcl



Tcl                      Last change:                           3






Tcl(n)                Tcl Built-In Commands                Tcl(n)



          interpreter as part of creating the words of a command.
          For example, if  variable  substition  occurs  then  no
          further  substitions  are performed on the value of the
          variable;  the value is inserted into the  word  verba-
          tim.   If  command  substitution occurs then the nested
          command is processed entirely by the recursive call  to
          the  Tcl  interpreter;  no  substitutions  are perfomed
          before making the recursive call and no additional sub-
          stitutions  are  performed  on the result of the nested
          script.

     [11] Substitutions do not affect the word  boundaries  of  a
          command.  For example, during variable substitution the
          entire value of the variable becomes part of  a  single
          word, even if the variable's value contains spaces.








































Tcl                      Last change:                           4







image(n)                   Tk Commands                   image(n)



_________________________________________________________________

NAME
     image - Create and manipulate images

SYNOPSIS
     image _o_p_t_i_o_n ?_a_r_g _a_r_g ...?
_________________________________________________________________


DESCRIPTION
     The image command is  used  to  create,  delete,  and  query
     images.   It  can take several different forms, depending on
     the _o_p_t_i_o_n argument.  The legal forms are:

     image create _t_y_p_e ?_n_a_m_e? ?_o_p_t_i_o_n _v_a_l_u_e ...?
          Creates a new image and returns its name.  _t_y_p_e  speci-
          fies  the  type  of the image, which must be one of the
          types currently defined (e.g., bitmap).  _n_a_m_e specifies
          the name for the image;  if it is omitted then Tk picks
          a name of the form  image_x,  where  _x  is  an  integer.
          There  may  be  any number of _o_p_t_i_o_n-_v_a_l_u_e pairs, which
          provide configuration options for the new  image.   The
          legal  set  of  options  is defined separately for each
          image type;  see below for details on the  options  for
          built-in  image  types.   If an image already exists by
          the given name then it is replaced with the  new  image
          and any instances of that image will redisplay with the
          new contents.

     image delete _n_a_m_e
          Deletes the image  named  _n_a_m_e  and  returns  an  empty
          string.   If there are instances of the image displayed
          in widgets, the image won't actually be  deleted  until
          all  of the instances are released.  However, the asso-
          ciation between the instances  and  the  image  manager
          will  be dropped.  Existing instances will retain their
          sizes but redisplay as empty areas.  If a deleted image
          is  recreated  with  another  call to image create, the
          existing instances will use the new image.

     image height _n_a_m_e
          Returns a decimal string giving  the  height  of  image
          _n_a_m_e in pixels.

     image names
          Returns a list containing the  names  of  all  existing
          images.

     image type _n_a_m_e
          Returns the type of image _n_a_m_e (the value of  the  _t_y_p_e
          argument to image create when the image was created).



Tk                      Last change: 4.0                        1






image(n)                   Tk Commands                   image(n)



     image types
          Returns a list whose elements  are  all  of  the  valid
          image  types  (i.e., all of the values that may be sup-
          plied for the _t_y_p_e argument to image create).

     image width _n_a_m_e
          Returns a decimal string giving the width of image _n_a_m_e
          in pixels.


BUILT-IN IMAGE TYPES
     The following image types are defined by Tk so they will  be
     available in any Tk application.  Individual applications or
     extensions may define additional types.

     bitmap
          Each pixel in the image displays a foreground color,  a
          background  color,  or  nothing.  See the bitmap manual
          entry for more information.

     photo
          Displays a variety of full-color images, using  dither-
          ing  to  approximate  colors  on  displays with limited
          color capabilities.  See the  photo  manual  entry  for
          more information.


KEYWORDS
     height, image, types of images, width


























Tk                      Last change: 4.0                        2




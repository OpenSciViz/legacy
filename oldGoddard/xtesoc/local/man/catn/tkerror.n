


tkerror(n)                 Tk Commands                 tkerror(n)



_________________________________________________________________

NAME
     tkerror - Command invoked to process background errors

SYNOPSIS
     tkerror _m_e_s_s_a_g_e
_________________________________________________________________


DESCRIPTION
     The tkerror command doesn't exist as built-in  part  of  Tk.
     Instead, individual applications or users can define a tker-
     ror command (e.g. as a Tcl procedure) if they wish to handle
     background errors.

     A background error is one that  occurs  in  a  command  that
     didn't  originate  with the application.  For example, if an
     error occurs while executing a command specified with a bind
     of  after  command,  then  it  is a background error.  For a
     non-background error, the error can simply  be  returned  up
     through  nested Tcl command evaluations until it reaches the
     top-level code in the application; then the application  can
     report  the  error  in whatever way it wishes.  When a back-
     ground error occurs, the unwinding ends in  the  Tk  library
     and there is no obvious way for Tk to report the error.

     When Tk detects a background  error,  it  saves  information  |
     about  the  error and invokes the tkerror command later when  |
     Tk is  idle.   Before  invoking  tkerror,  Tk  restores  the  |
     errorInfo  and  errorCode  variables  to their values at the  |
     time the error occurred, then it invokes  tkerror  with  the
     error  message  as  its  only argument.  Tk assumes that the
     application has implemented the tkerror  command,  and  that
     the  command will report the error in a way that makes sense
     for the application.  Tk will ignore any result returned  by
     the tkerror command.

     If another Tcl error occurs within the tkerror command  (for
     example,  because  no tkerror command has been defined) then
     Tk reports the error itself by writing a message to stderr.

     If several background errors accumulate  before  tkerror  is  |
     invoked  to  process  them, tkerror will be invoked once for  |
     each error, in the order they occurred.  However, if tkerror  |
     returns  with  a  break exception, then any remaining errors  |
     are skipped without calling tkerror.

     The Tk script library includes a default  tkerror  procedure
     that  posts  a  dialog  box containing the error message and
     offers the user a chance to see a stack trace showing  where
     the error occurred.



Tk                      Last change: 4.0                        1






tkerror(n)                 Tk Commands                 tkerror(n)



KEYWORDS
     background error, reporting





















































Tk                      Last change: 4.0                        2




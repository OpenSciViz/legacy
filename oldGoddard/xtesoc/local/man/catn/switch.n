


switch(n)             Tcl Built-In Commands             switch(n)



_________________________________________________________________

NAME
     switch - Evaluate one of several  scripts,  depending  on  a
     given value

SYNOPSIS
     switch ?_o_p_t_i_o_n_s? _s_t_r_i_n_g _p_a_t_t_e_r_n _b_o_d_y ?_p_a_t_t_e_r_n _b_o_d_y ...?
     switch ?_o_p_t_i_o_n_s? _s_t_r_i_n_g {_p_a_t_t_e_r_n _b_o_d_y ?_p_a_t_t_e_r_n _b_o_d_y ...?}
_________________________________________________________________


DESCRIPTION
     The switch command matches its _s_t_r_i_n_g argument against  each
     of  the  _p_a_t_t_e_r_n  arguments in order.  As soon as it finds a
     _p_a_t_t_e_r_n that matches _s_t_r_i_n_g it evaluates the following  _b_o_d_y
     argument  by  passing  it recursively to the Tcl interpreter
     and returns the result of that evaluation.  If the last _p_a_t_-
     _t_e_r_n  argument  is  default then it matches anything.  If no
     _p_a_t_t_e_r_n argument matches _s_t_r_i_n_g and  no  default  is  given,
     then the switch command returns an empty string.

     If the initial arguments to switch start with  -  then  they
     are treated as options.  The following options are currently
     supported:

     -exact     Use exact matching when  comparing  _s_t_r_i_n_g  to  a
               pattern.  This is the default.

     -glob      When matching _s_t_r_i_n_g to the patterns,  use  glob-
               style  matching  (i.e.  the same as implemented by
               the string match command).

     -regexp    When matching _s_t_r_i_n_g to the patterns, use regular
               expression  matching (i.e. the same as implemented
               by the regexp command).

     --          Marks the end of options.  The argument  follow-
               ing  this one will be treated as _s_t_r_i_n_g even if it
               starts with a -.

     Two syntaxes are provided for the  _p_a_t_t_e_r_n  and  _b_o_d_y  argu-
     ments.   The  first uses a separate argument for each of the
     patterns and commands; this form is convenient if  substitu-
     tions  are desired on some of the patterns or commands.  The
     second form places all of the patterns and commands together
     into  a  single argument; the argument must have proper list
     structure, with the elements of the list being the  patterns
     and  commands.   The  second form makes it easy to construct
     multi-line switch commands,  since  the  braces  around  the
     whole list make it unnecessary to include a backslash at the
     end of each line.  Since the _p_a_t_t_e_r_n arguments are in braces



Tcl                     Last change: 7.0                        1






switch(n)             Tcl Built-In Commands             switch(n)



     in the second form, no command or variable substitutions are
     performed on them;  this makes the behavior  of  the  second
     form different than the first form in some cases.

     If a _b_o_d_y is specified as ``-'' it means that the  _b_o_d_y  for
     the  next  pattern  should also be used as the body for this
     pattern (if the next pattern also has a body of ``- ''  then
     the body after that is used, and so on).  This feature makes
     it possible to share a single _b_o_d_y among several patterns.

     Below are some examples of switch commands:

          switch abc a - b {format 1} abc {format 2} default {format 3}

     will return 2,

          switch -regexp aaab {
            ^a.*b$ -
            b {format 1}
            a* {format 2}
            default {format 3}
          }

     will return 1, and

          switch xyz {
            a
              -
            b
              {format 1}
            a*
              {format 2}
            default
              {format 3}
          }

     will return 3.


KEYWORDS
     switch, match, regular expression














Tcl                     Last change: 7.0                        2




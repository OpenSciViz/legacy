


lreplace(n)           Tcl Built-In Commands           lreplace(n)



_________________________________________________________________

NAME
     lreplace - Replace elements in a list with new elements

SYNOPSIS
     lreplace _l_i_s_t _f_i_r_s_t _l_a_s_t ?_e_l_e_m_e_n_t _e_l_e_m_e_n_t ...?
_________________________________________________________________


DESCRIPTION
     Lreplace returns a new list formed by replacing one or  more
     elements  of  _l_i_s_t  with the _e_l_e_m_e_n_t arguments.  _F_i_r_s_t gives
     the index in _l_i_s_t of the first element  to  be  replaced  (0
     refers  to  the  first element).  If _f_i_r_s_t is less than zero
     then it refers to the first element of  _l_i_s_t;   the  element
     indicated  by  _f_i_r_s_t must exist in the list.  _L_a_s_t gives the
     index in _l_i_s_t of the last element to be replaced;   it  must
     be greater than or equal to _f_i_r_s_t.  _F_i_r_s_t or _l_a_s_t may be end  |
     (or any abbreviation of it) to refer to the last element  of
     the  list.   The  _e_l_e_m_e_n_t arguments specify zero or more new
     arguments to be added to the list in  place  of  those  that
     were  deleted.  Each _e_l_e_m_e_n_t argument will become a separate
     element of the list.  If no _e_l_e_m_e_n_t arguments are specified,
     then the elements between _f_i_r_s_t and _l_a_s_t are simply deleted.


KEYWORDS
     element, list, replace


























Tcl                      Last change:                           1




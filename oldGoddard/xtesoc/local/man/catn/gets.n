


gets(n)               Tcl Built-In Commands               gets(n)



_________________________________________________________________

NAME
     gets - Read a line from a file

SYNOPSIS
     gets _f_i_l_e_I_d ?_v_a_r_N_a_m_e?
_________________________________________________________________


DESCRIPTION
     This command reads the next line  from  the  file  given  by
     _f_i_l_e_I_d  and  discards the terminating newline character.  If
     _v_a_r_N_a_m_e is specified then the line is placed in the variable
     by  that  name and the return value is a count of the number
     of characters read (not including the newline).  If the  end
     of the file is reached before reading any characters then -1
     is returned and _v_a_r_N_a_m_e is set to an empty string.  If  _v_a_r_-
     _N_a_m_e is not specified then the return value will be the line
     (minus the newline character) or an empty string if the  end
     of  the  file  is reached before reading any characters.  An
     empty string will also be returned if  a  line  contains  no
     characters except the newline, so eof may have to be used to
     determine what really happened.  If the  last  character  in
     the  file is not a newline character then gets behaves as if
     there were an additional newline character at the end of the
     file.   _F_i_l_e_I_d must be stdin or the return value from a pre-
     vious call to open; it must refer to a file that was  opened
     for reading.  Any existing end-of-file or error condition on  |
     the file is cleared at the beginning of the gets command.


KEYWORDS
     file, line, read





















Tcl                      Last change:                           1




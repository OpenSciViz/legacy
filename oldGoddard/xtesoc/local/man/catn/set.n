


set(n)                Tcl Built-In Commands                set(n)



_________________________________________________________________

NAME
     set - Read and write variables

SYNOPSIS
     set _v_a_r_N_a_m_e ?_v_a_l_u_e?
_________________________________________________________________


DESCRIPTION
     Returns the value of variable _v_a_r_N_a_m_e.  If _v_a_l_u_e  is  speci-
     fied, then set the value of _v_a_r_N_a_m_e to _v_a_l_u_e, creating a new
     variable if one doesn't already exist, and return its value.
     If  _v_a_r_N_a_m_e  contains  an  open  parenthesis and ends with a
     close parenthesis, then it refers to an array element:   the
     characters before the first open parenthesis are the name of
     the array, and the characters between  the  parentheses  are
     the  index  within the array.  Otherwise _v_a_r_N_a_m_e refers to a
     scalar variable.  If no procedure is  active,  then  _v_a_r_N_a_m_e
     refers to a global variable.  If a procedure is active, then
     _v_a_r_N_a_m_e refers to a parameter or local variable of the  pro-
     cedure unless the _g_l_o_b_a_l command has been invoked to declare
     _v_a_r_N_a_m_e to be global.


KEYWORDS
     read, write, variable



























Tcl                      Last change:                           1




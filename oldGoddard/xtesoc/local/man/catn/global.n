


global(n)             Tcl Built-In Commands             global(n)



_________________________________________________________________

NAME
     global - Access global variables

SYNOPSIS
     global _v_a_r_n_a_m_e ?_v_a_r_n_a_m_e ...?
_________________________________________________________________


DESCRIPTION
     This command is ignored unless  a  Tcl  procedure  is  being
     interpreted.   If so then it declares the given _v_a_r_n_a_m_e's to
     be global variables rather than local ones.  For  the  dura-
     tion  of  the current procedure (and only while executing in
     the current procedure), any reference to any of the _v_a_r_n_a_m_es
     will refer to the global variable by the same name.


KEYWORDS
     global, procedure, variable


































Tcl                      Last change:                           1







exit(n)                    Tk Commands                    exit(n)



_________________________________________________________________

NAME
     exit - Exit the process

SYNOPSIS
     exit ?_r_e_t_u_r_n_C_o_d_e?
_________________________________________________________________


DESCRIPTION
     Terminate the process, returning _r_e_t_u_r_n_C_o_d_e (an integer)  to
     the  system  as the exit status.  If _r_e_t_u_r_n_C_o_d_e isn't speci-
     fied then it defaults to 0.  This command replaces  the  Tcl
     command  by  the  same  name.  It is identical to Tcl's exit
     command except that before exiting it destroys all the  win-
     dows  managed  by  the process.  This allows various cleanup
     operations to be performed,  such  as  removing  application
     names from the global registry of applications.


KEYWORDS
     exit, process
































Tk                       Last change:                           1




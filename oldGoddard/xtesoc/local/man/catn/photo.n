


photo(n)                   Tk Commands                   photo(n)



_________________________________________________________________

NAME
     photo - Full-color images

SYNOPSIS
     image create photo ?_n_a_m_e? ?_o_p_t_i_o_n_s?
_________________________________________________________________


DESCRIPTION
     A photo is an image whose pixels can display any color or be
     transparent.   A  photo  image  is stored internally in full
     color (24 bits per pixel), and is displayed using  dithering
     if  necessary.  Image data for a photo image can be obtained
     from a file or a string, or it can be supplied from  C  code
     through  a  procedural  interface.  At present, only the raw
     PPM file format is supported; an interface exists  to  allow
     additional  image  file formats to be added easily.  A photo
     image is transparent in regions where no image data has been
     supplied.


CREATING PHOTOS
     Like all images, photos are created using the  image  create
     command.  Photos support the following _o_p_t_i_o_n_s:

     -data _s_t_r_i_n_g
          Specifies the contents of the image as a  string.   The
          format  of  the  string  must be one of those for which
          there is an image file format handler that will  accept
          string  data.   If both the -data and -file options are
          specified, the -file option takes precedence.

     -format _f_o_r_m_a_t-_n_a_m_e
          Specifies the name of the  file  format  for  the  data
          specified with the -data or -file option.

     -file _n_a_m_e
          _n_a_m_e gives the name of a file that is  to  be  read  to
          supply  data for the photo image.  The file format must
          be one of those for which there is an image file format
          handler that can read data from a file.

     -gamma _v_a_l_u_e
          Specifies that the colors allocated for displaying this
          image  in a window should be corrected for a non-linear
          display with the specified gamma exponent value.   (The
          intensity  produced  by  most  CRT  displays is a power
          function of the input value, to a  good  approximation;
          gamma  is the exponent and is typically around 2).  The
          value specified must be greater than zero.  The default



Tk                      Last change: 4.0                        1






photo(n)                   Tk Commands                   photo(n)



          value  is  one  (no  correction).   In  general, values
          greater than one  will  make  the  image  lighter,  and
          values less than one will make it darker.

     -height _n_u_m_b_e_r
          Specifies the height of the  image,  in  pixels.   This
          option is useful primarily in situations where the user
          wishes to build up the contents of the image  piece  by
          piece.   A value of zero (the default) allows the image
          to expand or shrink vertically to fit the  data  stored
          in it.

     -palette _p_a_l_e_t_t_e-_s_p_e_c
          Specifies the resolution of the color cube to be  allo-
          cated for displaying this image, and thus the number of
          colors used from the colormaps of the windows where  it
          is  displayed.  The _p_a_l_e_t_t_e-_s_p_e_c string may be either a
          single decimal number, specifying the number of  shades
          of  gray  to use, or three decimal numbers separated by
          slashes (/), specifying the number of  shades  of  red,
          green and blue to use, respectively.  If the first form
          (a single number) is used, the image will be  displayed
          in monochrome (i.e., grayscale).

     -width _n_u_m_b_e_r
          Specifies the width of the image,  in  pixels.     This
          option is useful primarily in situations where the user
          wishes to build up the contents of the image  piece  by
          piece.   A value of zero (the default) allows the image
          to expand or shrink horizontally to fit the data stored
          in it.


IMAGE COMMAND
     When a photo image is created, Tk also creates a new command
     whose  name  is  the same as the image.  This command may be
     used to invoke various operations on the image.  It has  the
     following general form:

          _i_m_a_g_e_N_a_m_e _o_p_t_i_o_n ?_a_r_g _a_r_g ...?

     _O_p_t_i_o_n and the _a_r_gs determine the exact behavior of the com-
     mand.

     Those options that write data to the image generally  expand
     the size of the image, if necessary, to accommodate the data
     written to the image, unless the user has specified non-zero
     values  for the -width and/or -height configuration options,
     in which case the width and/or height, respectively, of  the
     image will not be changed.





Tk                      Last change: 4.0                        2






photo(n)                   Tk Commands                   photo(n)



     The following commands are possible for photo images:

     _i_m_a_g_e_N_a_m_e blank
          Blank the image; that is, set the entire image to  have
          no  data,  so  it will be displayed as transparent, and
          the background of whatever window it  is  displayed  in
          will show through.

     _i_m_a_g_e_N_a_m_e cget _o_p_t_i_o_n
          Returns the current value of the  configuration  option
          given  by  _o_p_t_i_o_n.   _O_p_t_i_o_n  may have any of the values
          accepted by the image create photo command.

     _i_m_a_g_e_N_a_m_e configure ?_o_p_t_i_o_n? ?_v_a_l_u_e _o_p_t_i_o_n _v_a_l_u_e ...?
          Query or  modify  the  configuration  options  for  the
          image.   If  no  _o_p_t_i_o_n  is  specified,  returns a list
          describing all of the available options  for  _i_m_a_g_e_N_a_m_e
          (see  Tk_ConfigureInfo for information on the format of
          this list).  If _o_p_t_i_o_n is specified with no _v_a_l_u_e, then
          the  command  returns  a  list describing the one named
          option (this list will be identical to the  correspond-
          ing  sublist  of  the  value  returned  if no _o_p_t_i_o_n is
          specified).  If one or more  _o_p_t_i_o_n - _v_a_l_u_e  pairs  are
          specified,   then   the   command  modifies  the  given
          option(s) to have the given value(s);  in this case the
          command  returns  an empty string.  _O_p_t_i_o_n may have any
          of the values accepted by the image create  photo  com-
          mand.

     _i_m_a_g_e_N_a_m_e copy _s_o_u_r_c_e_I_m_a_g_e ?_o_p_t_i_o_n _v_a_l_u_e(_s) ...?
          Copies a  region  from  the  image  called  _s_o_u_r_c_e_I_m_a_g_e
          (which  must  be  a  photo  image)  to the image called
          _i_m_a_g_e_N_a_m_e, possibly with pixel zooming  and/or  decima-
          tion.  If no options are specified, this command copies
          the whole of _s_o_u_r_c_e_I_m_a_g_e into  _i_m_a_g_e_N_a_m_e,  starting  at
          coordinates  (0,0) in _i_m_a_g_e_N_a_m_e.  The following options
          may be specified:

          -from _x_1 _y_1 _x_2 _y_2
               Specifies a rectangular sub-region of  the  source
               image  to  be copied.  (_x_1,_y_1) and (_x_2,_y_2) specify
               diagonally opposite corners of the rectangle.   If
               _x_2  and _y_2 are not specified, the default value is
               the bottom-right corner of the source image.   The
               pixels  copied will include the left and top edges
               of the specified rectangle but not the  bottom  or
               right  edges.   If  the -from option is not given,
               the default is the whole source image.

          -to _x_1 _y_1 _x_2 _y_2
               Specifies a rectangular sub-region of the destina-
               tion  image  to  be affected.  (_x_1,_y_1) and (_x_2,_y_2)



Tk                      Last change: 4.0                        3






photo(n)                   Tk Commands                   photo(n)



               specify diagonally opposite corners of the rectan-
               gle.   If _x_2 and _y_2 are not specified, the default
               value is (_x_1,_y_1)  plus  the  size  of  the  source
               region  (after  decimation  and zooming, if speci-
               fied).  If _x_2 and _y_2  are  specified,  the  source
               region will be replicated if necessary to fill the
               destination region in a tiled fashion.

          -shrink
               Specifies that the size of the  destination  image
               should  be  reduced,  if  necessary,  so  that the
               region being copied into is  at  the  bottom-right
               corner  of the image.  This option will not affect
               the width or height of the image if the  user  has
               specified  a  non-zero  value  for the -width or -
               height configuration option, respectively.

          -zoom _x _y
               Specifies that the source region should be  magni-
               fied  by a factor of _x in the X direction and _y in
               the Y direction.  If _y is not given,  the  default
               value  is  the  same as _x.  With this option, each
               pixel in the source image will be expanded into  a
               block  of  _x  x _y pixels in the destination image,
               all the same color.  _x and _y must be greater  than
               0.

          -decimate _x _y
               Specifies that the source image should be  reduced
               in  size  by  using  only every _xth pixel in the X
               direction and _yth pixel in the Y direction.  Nega-
               tive  values  will  cause  the image to be flipped
               about the Y or X axes, respectively.  If _y is  not
               given, the default value is the same as _x.

     _i_m_a_g_e_N_a_m_e get _x _y
          Returns the color of the pixel at coordinates (_x,_y)  in
          the  image  as  a  list of three integers between 0 and
          255, representing the red, green  and  blue  components
          respectively.

     _i_m_a_g_e_N_a_m_e put _d_a_t_a ?-to _x_1 _y_1 _x_2 _y_2?
          Sets pixels in _i_m_a_g_e_N_a_m_e to  the  colors  specified  in
          _d_a_t_a.   _d_a_t_a is used to form a two-dimensional array of
          pixels that are then copied into the  _i_m_a_g_e_N_a_m_e.   _d_a_t_a
          is structured as a list of horizontal rows, from top to
          bottom, each of which is a list of colors, listed  from
          left  to  right.   Each  color may be specified by name
          (e.g., blue) or in hexadecimal  form  (e.g.,  #2376af).
          The  - to  option  can  be  used to specify the area of
          _i_m_a_g_e_N_a_m_e to be affected.  If only _x_1 and _y_1 are given,
          the  area  affected  has its top-left corner at (_x_1,_y_1)



Tk                      Last change: 4.0                        4






photo(n)                   Tk Commands                   photo(n)



          and is the same size as the array given  in  _d_a_t_a.   If
          all four coordinates are given, they specify diagonally
          opposite corners of the  affected  rectangle,  and  the
          array  given in _d_a_t_a will be replicated as necessary in
          the X and Y directions to fill the rectangle.

     _i_m_a_g_e_N_a_m_e read _f_i_l_e_n_a_m_e ?_o_p_t_i_o_n _v_a_l_u_e(_s) ...?
          Reads image data from the file named _f_i_l_e_n_a_m_e into  the
          image.   This  command first searches the list of image
          file format handlers for a handler that  can  interpret
          the  data  in  _f_i_l_e_n_a_m_e,  and  then  reads the image in
          _f_i_l_e_n_a_m_e into _i_m_a_g_e_N_a_m_e (the destination  image).   The
          following options may be specified:

          -format _f_o_r_m_a_t-_n_a_m_e
               Specifies  the  format  of  the  image   data   in
               _f_i_l_e_n_a_m_e.   Specifically,  only  image file format
               handlers whose names begin with  _f_o_r_m_a_t-_n_a_m_e  will
               be  used  while searching for an image data format
               handler to read the data.

          -from _x_1 _y_1 _x_2 _y_2
               Specifies a rectangular sub-region  of  the  image
               file  data  to be copied to the destination image.
               If only  _x_1  and  _y_1  are  specified,  the  region
               extends from (_x_1,_y_1) to the bottom-right corner of
               the image in the image file.  If all four  coordi-
               nates are specified, they specify diagonally oppo-
               site corners or the region.  The default, if  this
               option is not specified, is the whole of the image
               in the image file.

          -shrink
               If this option, the  size  of  _i_m_a_g_e_N_a_m_e  will  be
               reduced,  if  necessary,  so  that the region into
               which the image file  data  are  read  is  at  the
               bottom-right corner of the _i_m_a_g_e_N_a_m_e.  This option
               will not affect the width or height of  the  image
               if the user has specified a non-zero value for the
               -width or -height  configuration  option,  respec-
               tively.

          -to _x _y
               Specifies the coordinates of the  top-left  corner
               of  the  region  of _i_m_a_g_e_N_a_m_e into which data from
               _f_i_l_e_n_a_m_e are to be read.  The default is (0,0).

     _i_m_a_g_e_N_a_m_e redither
          The dithering algorithm used in displaying photo images
          propagates  quantization  errors  from one pixel to its
          neighbors.  If the image data for _i_m_a_g_e_N_a_m_e is supplied
          in  pieces,  the  dithered  image  may  not  be exactly



Tk                      Last change: 4.0                        5






photo(n)                   Tk Commands                   photo(n)



          correct.  Normally the difference  is  not  noticeable,
          but  if  it  is  a problem, this command can be used to
          recalculate the dithered image in each window where the
          image is displayed.

     _i_m_a_g_e_N_a_m_e write _f_i_l_e_n_a_m_e ?_o_p_t_i_o_n _v_a_l_u_e(_s) ...?
          Writes image  data  from  _i_m_a_g_e_N_a_m_e  to  a  file  named
          _f_i_l_e_n_a_m_e.  The following options may be specified:

          -format _f_o_r_m_a_t-_n_a_m_e
               Specifies  the  name  of  the  image  file  format
               handler  to be used to write the data to the file.
               Specifically, this  subcommand  searches  for  the
               first  handler  whose  name matches a initial sub-
               string of _f_o_r_m_a_t-_n_a_m_e and which has the capability
               to  write  an  image  file.  If this option is not
               given, this subcommand uses the first handler that
               has the capability to write an image file.

          -from _x_1 _y_1 _x_2 _y_2
               Specifies a rectangular region of _i_m_a_g_e_N_a_m_e to  be
               written  to the image file.  If only _x_1 and _y_1 are
               specified, the region extends from (_x_1,_y_1) to  the
               bottom-right  corner  of  _i_m_a_g_e_N_a_m_e.   If all four
               coordinates are  given,  they  specify  diagonally
               opposite  corners  of the rectangular region.  The
               default, if this option is not given, is the whole
               image.


IMAGE FORMATS
     The photo image code is structured  to  allow  handlers  for
     additional image file formats to be added easily.  The photo
     image code maintains a list of these handlers.  Handlers are
     added  to  the  list  by  registering  them  with  a call to
     Tk_CreatePhotoImageFormat.   The  standard  Tk  distribution
     comes  with  a handler for raw PPM files, which is automati-
     cally registered on initialization.

     When reading an image file or processing string data  speci-
     fied  with  the  -data configuration option, the photo image
     code invokes each handler in turn until one  is  found  that
     claims  to  be  able to read the data in the file or string.
     Usually this will  find  the  correct  handler,  but  if  it
     doesn't,  the  user  may give a format name with the -format
     option to specify which handler to use.  In fact  the  photo
     image  code  will  try those handlers whose names begin with
     the string specified for the -format option (the  comparison
     is  case-insensitive).  For example, if the user specifies -
     format gif, then a handler  named  GIF87  or  GIF89  may  be
     invoked,  but  a  handler  named JPEG may not (assuming that
     such handlers had been registered).



Tk                      Last change: 4.0                        6






photo(n)                   Tk Commands                   photo(n)



     When writing image data to a file, the processing of  the  -
     format  option is slightly different: the string value given
     for the -format option must begin with the complete name  of
     the  requested  handler, and may contain additional informa-
     tion following that, which the handler can use, for example,
     to  specify which variant to use of the formats supported by
     the handler.


COLOR ALLOCATION
     When a photo image is displayed in a window, the photo image
     code  allocates colors to use to display the image and dith-
     ers the image, if necessary, to display a reasonable approx-
     imation  to  the  image using the colors that are available.
     The colors are allocated as  a  color  cube,  that  is,  the
     number  of  colors allocated is the product of the number of
     shades of red, green and blue.

     Normally, the number of colors allocated is chosen based  on
     the  depth  of  the  window.  For example, in an 8-bit Pseu-
     doColor window, the photo image code will attempt  to  allo-
     cate  seven  shades  of  red, seven shades of green and four
     shades of blue, for a total of 198 colors.  In a 1-bit  Sta-
     ticGray  (monochrome)  window,  it will allocate two colors,
     black and white.  In a 24-bit DirectColor or TrueColor  win-
     dow,  it  will  allocate  256  shades each of red, green and
     blue.  Fortunately, because of the way that pixel values can
     be  combined in DirectColor and TrueColor windows, this only
     requires 256 colors to be allocated.   If  not  all  of  the
     colors  can  be  allocated, the photo image code reduces the
     number of shades of each primary color and tries again.

     The user can exercise some control over the number of colors
     that  a  photo  image  uses  with the -palette configuration
     option.  If this option is used, it  specifies  the  maximum
     number  of  shades of each primary color to try to allocate.
     It can also be used to force the image to  be  displayed  in
     shades  of gray, even on a color display, by giving a single
     number rather than three numbers separated by slashes.


CREDITS
     The photo image type was designed and  implemented  by  Paul
     Mackerras,  based  on  his  earlier  photo  widget  and some
     suggestions from John Ousterhout.


KEYWORDS
     photo, image, color






Tk                      Last change: 4.0                        7




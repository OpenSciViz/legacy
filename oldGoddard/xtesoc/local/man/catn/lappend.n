


lappend(n)            Tcl Built-In Commands            lappend(n)



_________________________________________________________________

NAME
     lappend - Append list elements onto a variable

SYNOPSIS
     lappend _v_a_r_N_a_m_e ?_v_a_l_u_e _v_a_l_u_e _v_a_l_u_e ...?
_________________________________________________________________


DESCRIPTION
     This command treats the variable given by _v_a_r_N_a_m_e as a  list
     and  appends  each  of the _v_a_l_u_e arguments to that list as a
     separate element, with spaces between elements.  If  _v_a_r_N_a_m_e
     doesn't  exist,  it is created as a list with elements given
     by the _v_a_l_u_e arguments.  Lappend is similar to append except
     that  the  _v_a_l_u_es  are appended as list elements rather than
     raw text.  This command provides a relatively efficient  way
     to  build  up large lists.  For example, ``lappend a $b'' is
     much more efficient than ``set a  [concat  $a  [list  $b]]''
     when $a is long.


KEYWORDS
     append, element, list, variable






























Tcl                      Last change:                           1




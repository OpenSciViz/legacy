


pack-old(n)                Tk Commands                pack-old(n)



_________________________________________________________________

NAME
     pack - Obsolete syntax for packer geometry manager

SYNOPSIS
     pack after _s_i_b_l_i_n_g _w_i_n_d_o_w _o_p_t_i_o_n_s ?_w_i_n_d_o_w _o_p_t_i_o_n_s ...?

     pack append _p_a_r_e_n_t _w_i_n_d_o_w _o_p_t_i_o_n_s ?_w_i_n_d_o_w _o_p_t_i_o_n_s ...?

     pack before _s_i_b_l_i_n_g _w_i_n_d_o_w _o_p_t_i_o_n_s ?_w_i_n_d_o_w _o_p_t_i_o_n_s ...?

     pack unpack _w_i_n_d_o_w
_________________________________________________________________


DESCRIPTION
     _N_o_t_e: _t_h_i_s _m_a_n_u_a_l _e_n_t_r_y _d_e_s_c_r_i_b_e_s _t_h_e _s_y_n_t_a_x  _f_o_r  _t_h_e  pack
     _c_o_m_m_a_n_d  _a_s _i_t _e_x_i_s_t_e_d _b_e_f_o_r_e _T_k _v_e_r_s_i_o_n _3._3.  _A_l_t_h_o_u_g_h _t_h_i_s
     _s_y_n_t_a_x _c_o_n_t_i_n_u_e_s _t_o _b_e _s_u_p_p_o_r_t_e_d _f_o_r _b_a_c_k_w_a_r_d _c_o_m_p_a_t_i_b_i_l_i_t_y,
     _i_t  _i_s  _o_b_s_o_l_e_t_e  _a_n_d  _s_h_o_u_l_d  _n_o_t _b_e _u_s_e_d _a_n_y_m_o_r_e.  _A_t _s_o_m_e
     _p_o_i_n_t _i_n _t_h_e _f_u_t_u_r_e _i_t _m_a_y _c_e_a_s_e _t_o _b_e _s_u_p_p_o_r_t_e_d.

     The packer is a geometry manager that arranges the  children
     of a parent by packing them in order around the edges of the
     parent.  The first child is placed against one side  of  the
     window,  occupying  the entire span of the window along that
     side.  This reduces the space remaining for  other  children
     as  if  the  side had been moved in by the size of the first
     child.  Then the next child is placed against  one  side  of
     the remaining cavity, and so on until all children have been
     placed or there is no space left in the cavity.

     The before, after, and append forms of the pack command  are
     used  to  insert one or more children into the packing order
     for their parent.  The  before  form  inserts  the  children
     before  window  _s_i_b_l_i_n_g in the order;  all of the other win-
     dows must be siblings of _s_i_b_l_i_n_g.  The  after  form  inserts
     the  windows  after _s_i_b_l_i_n_g, and the append form appends one
     or more windows to the end of the packing order for  _p_a_r_e_n_t.
     If a _w_i_n_d_o_w named in any of these commands is already packed
     in its parent, it is removed from its  current  position  in
     the  packing order and repositioned as indicated by the com-
     mand.  All of these  commands  return  an  empty  string  as
     result.

     The unpack form of the pack command removes _w_i_n_d_o_w from  the
     packing order of its parent and unmaps it.  After the execu-
     tion of this  command  the  packer  will  no  longer  manage
     _w_i_n_d_o_w's geometry.





Tk                      Last change: 4.0                        1






pack-old(n)                Tk Commands                pack-old(n)



     The placement of each child is actually a four-step process;
     the  _o_p_t_i_o_n_s  argument  following  each _w_i_n_d_o_w consists of a
     list of one or more fields that govern the placement of that
     window.   In the discussion below, the term _c_a_v_i_t_y refers to
     the space left in a parent when a particular child is placed
     (i.e.  all the space that wasn't claimed by earlier children
     in the packing order).  The term _p_a_r_c_e_l refers to the  space
     allocated  to  a  particular child;  this is not necessarily
     the same as the child window's final geometry.

     The first step in placing a child is to determine which side
     of the cavity it will lie against.  Any one of the following
     options may be used to specify a side:

     top  Position the child's parcel against the top of the cav-
          ity, occupying the full width of the cavity.

     bottom
          Position the child's parcel against the bottom  of  the
          cavity, occupying the full width of the cavity.

     left Position the child's parcel against the  left  side  of
          the cavity, occupying the full height of the cavity.

     right
          Position the child's parcel against the right  side  of
          the cavity, occupying the full height of the cavity.

     At most one of these options should  be  specified  for  any
     given  window.  If no side is specified, then the default is
     top.

     The second step is to decide on a parcel for the child.  For
     top and bottom windows, the desired parcel width is normally
     the cavity width  and  the  desired  parcel  height  is  the
     window's  requested height, as passed to Tk_GeometryRequest.
     For left and right windows, the  desired  parcel  height  is
     normally  the  cavity  height  and  the desired width is the
     window's requested  width.   However,  extra  space  may  be
     requested for the window using any of the following options:

     padx _n_u_m    Add _n_u_m pixels to the window's  requested  width
                 before  computing  the  parcel size as described
                 above.

     pady _n_u_m    Add _n_u_m pixels to the window's requested  height
                 before  computing  the  parcel size as described
                 above.

     expand      This option requests that  the  window's  parcel
                 absorb any extra space left over in the parent's
                 cavity after  packing  all  the  children.   The



Tk                      Last change: 4.0                        2






pack-old(n)                Tk Commands                pack-old(n)



                 amount  of  space left over depends on the sizes
                 requested by the  other  children,  and  may  be
                 zero.   If  several  windows  have all specified
                 expand then the  extra  width  will  be  divided
                 equally  among  all  the  left and right windows
                 that specified expand and the extra height  will
                 be  divided equally among all the top and bottom
                 windows that specified expand.

     If the desired width or height for a parcel is  larger  than
     the corresponding dimension of the cavity, then the cavity's
     dimension is used instead.

     The third step in placing the window is  to  decide  on  the
     window's width and height.  The default is for the window to
     receive either its requested width and height or  the  those
     of  the  parcel,  whichever  is  smaller.   If the parcel is
     larger than the window's requested size, then the  following
     options  may  be  used  to expand the window to partially or
     completely fill the parcel:

     fill Set the window's size to equal the parcel size.

     fillx
          Increase the  window's  width  to  equal  the  parcel's
          width, but retain the window's requested height.

     filly
          Increase the window's  height  to  equal  the  parcel's
          height, but retain the window's requested width.

     The last step is to decide the window's location within  its
     parcel.  If the window's size equals the parcel's size, then
     the window simply fills the entire parcel.  If the parcel is
     larger  than  the  window, then one of the following options
     may be used to specify where the window should be positioned
     within its parcel:

     frame center   Center the window in its parcel.  This is the
                    default if no framing option is specified.

     frame n        Position the window with its  top  edge  cen-
                    tered on the top edge of the parcel.

     frame ne       Position  the  window  with  its  upper-right
                    corner  at the upper-right corner of the par-
                    cel.

     frame e        Position the window with its right edge  cen-
                    tered on the right edge of the parcel.

     frame se       Position  the  window  with  its  lower-right



Tk                      Last change: 4.0                        3






pack-old(n)                Tk Commands                pack-old(n)



                    corner  at the lower-right corner of the par-
                    cel.

     frame s        Position the window with its bottom edge cen-
                    tered on the bottom edge of the parcel.

     frame sw       Position  the  window  with  its   lower-left
                    corner  at  the lower-left corner of the par-
                    cel.

     frame w        Position the window with its left  edge  cen-
                    tered on the left edge of the parcel.

     frame nw       Position  the  window  with  its   upper-left
                    corner  at  the upper-left corner of the par-
                    cel.

     The packer manages the  mapped/unmapped  state  of  all  the
     packed  children windows.  It automatically maps the windows
     when it packs them, and it  unmaps  any  windows  for  which
     there was no space left in the cavity.

     The packer makes geometry requests on behalf of  the  parent
     windows  it  manages.   For each parent window it requests a
     size large enough to accommodate all the  options  specified
     by  all  the  packed children, such that zero space would be
     leftover for expand options.


KEYWORDS
     geometry manager, location, packer, parcel, size
























Tk                      Last change: 4.0                        4




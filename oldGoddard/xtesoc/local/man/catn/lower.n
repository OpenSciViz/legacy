


lower(n)                   Tk Commands                   lower(n)



_________________________________________________________________

NAME
     lower - Change a window's position in the stacking order

SYNOPSIS
     lower _w_i_n_d_o_w ?_b_e_l_o_w_T_h_i_s?
_________________________________________________________________


DESCRIPTION
     If the _b_e_l_o_w_T_h_i_s argument is omitted then the command lowers
     _w_i_n_d_o_w so that it is below all of its siblings in the stack-
     ing order (it will be obscured by any siblings that  overlap
     it  and  will  not  obscure  any siblings).  If _b_e_l_o_w_T_h_i_s is
     specified then it must be the path name of a window that  is
     either a sibling of _w_i_n_d_o_w or the descendant of a sibling of
     _w_i_n_d_o_w.  In this case the lower command will  insert  _w_i_n_d_o_w
     into  the stacking order just below _b_e_l_o_w_T_h_i_s (or the ances-
     tor of _b_e_l_o_w_T_h_i_s that is a sibling of  _w_i_n_d_o_w);  this  could
     end up either raising or lowering _w_i_n_d_o_w.


KEYWORDS
     lower, obscure, stacking order






























Tk                      Last change: 3.3                        1




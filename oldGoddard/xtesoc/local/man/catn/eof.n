


eof(n)                Tcl Built-In Commands                eof(n)



_________________________________________________________________

NAME
     eof - Check for end-of-file condition on open file

SYNOPSIS
     eof _f_i_l_e_I_d
_________________________________________________________________


DESCRIPTION
     Returns 1  if  an  end-of-file  condition  has  occurred  on
     _f_i_l_e_I_d, 0 otherwise.  _F_i_l_e_I_d must have been the return value
     from a previous call to open, or it may be stdin, stdout, or
     stderr to refer to one of the standard I/O channels.


KEYWORDS
     end, file




































Tcl                      Last change:                           1




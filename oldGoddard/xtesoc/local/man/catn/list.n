


list(n)               Tcl Built-In Commands               list(n)



_________________________________________________________________

NAME
     list - Create a list

SYNOPSIS
     list ?_a_r_g _a_r_g ...?                                            |
_________________________________________________________________


DESCRIPTION
     This command returns a list comprised of all the _a_r_gs, or an  |
     empty   string   if  no  _a_r_gs  are  specified.   Braces  and
     backslashes get added as necessary, so that the  index  com-
     mand  may  be  used on the result to re-extract the original
     arguments, and also so that eval may be used to execute  the
     resulting  list, with _a_r_g_1 comprising the command's name and
     the other _a_r_gs  comprising  its  arguments.   List  produces
     slightly  different results than concat:  concat removes one
     level of grouping before forming the list, while list  works
     directly from the original arguments.  For example, the com-
     mand

          list a b {c d e} {f {g h}}

     will return

          a b {c d e} {f {g h}}

     while concat with the same arguments will return

          a b c d e f {g h}



KEYWORDS
     element, list


















Tcl                      Last change:                           1




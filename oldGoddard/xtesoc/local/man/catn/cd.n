


cd(n)                 Tcl Built-In Commands                 cd(n)



_________________________________________________________________

NAME
     cd - Change working directory

SYNOPSIS
     cd ?_d_i_r_N_a_m_e?
_________________________________________________________________


DESCRIPTION
     Change the current working directory to _d_i_r_N_a_m_e, or  to  the
     home  directory  (as specified in the HOME environment vari-
     able) if _d_i_r_N_a_m_e is not given.  If  _d_i_r_N_a_m_e  starts  with  a
     tilde,   then  tilde-expansion  is  done  as  described  for
     Tcl_TildeSubst.  Returns an empty string.


KEYWORDS
     working directory



































Tcl                      Last change:                           1







for(n)                Tcl Built-In Commands                for(n)



_________________________________________________________________

NAME
     for - ``For'' loop

SYNOPSIS
     for _s_t_a_r_t _t_e_s_t _n_e_x_t _b_o_d_y
_________________________________________________________________


DESCRIPTION
     For is a looping command, similar in structure to the C  for
     statement.   The _s_t_a_r_t, _n_e_x_t, and _b_o_d_y arguments must be Tcl
     command strings, and _t_e_s_t is an expression string.  The  for
     command  first invokes the Tcl interpreter to execute _s_t_a_r_t.
     Then it repeatedly evaluates _t_e_s_t as an expression;  if  the
     result  is  non-zero it invokes the Tcl interpreter on _b_o_d_y,
     then invokes the Tcl interpreter on _n_e_x_t, then  repeats  the
     loop.   The command terminates when _t_e_s_t evaluates to 0.  If
     a continue command is invoked within _b_o_d_y then any remaining
     commands  in the current execution of _b_o_d_y are skipped; pro-
     cessing continues by invoking the Tcl interpreter  on  _n_e_x_t,
     then  evaluating  _t_e_s_t,  and  so  on.  If a break command is
     invoked within _b_o_d_y or  _n_e_x_t,  then  the  for  command  will
     return immediately.  The operation of break and continue are
     similar to the corresponding statements in C.   For  returns
     an empty string.


KEYWORDS
     for, iteration, looping
























Tcl                      Last change:                           1




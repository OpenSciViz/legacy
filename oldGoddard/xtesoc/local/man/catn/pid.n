


pid(n)                Tcl Built-In Commands                pid(n)



_________________________________________________________________

NAME
     pid - Retrieve process id(s)

SYNOPSIS
     pid ?_f_i_l_e_I_d?
_________________________________________________________________


DESCRIPTION
     If the _f_i_l_e_I_d argument is  given  then  it  should  normally
     refer  to  a process pipeline created with the open command.
     In this case the pid command will return a list  whose  ele-
     ments  are  the  process identifiers of all the processes in
     the pipeline, in order.  The list will be  empty  if  _f_i_l_e_I_d
     refers to an open file that isn't a process pipeline.  If no
     _f_i_l_e_I_d argument is given then pid returns the process  iden-
     tifier  of the current process.  All process identifiers are
     returned as decimal strings.


KEYWORDS
     file, pipeline, process identifier































Tcl                     Last change: 7.0                        1




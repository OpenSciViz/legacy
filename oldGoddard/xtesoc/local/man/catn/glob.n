


glob(n)               Tcl Built-In Commands               glob(n)



_________________________________________________________________

NAME
     glob - Return names of files that match patterns

SYNOPSIS
     glob ?_s_w_i_t_c_h_e_s? _p_a_t_t_e_r_n ?_p_a_t_t_e_r_n ...?
_________________________________________________________________


DESCRIPTION
     This command performs file name ``globbing''  in  a  fashion
     similar  to  the  csh shell.  It returns a list of the files
     whose names match any of the _p_a_t_t_e_r_n arguments.

     If the initial arguments to glob start with - then they  are  |
     treated  as  switches.  The following switches are currently  |
     supported:                                                    |

                               -                           nocom-  |
                    plain                                       |
                    |                                              |
                    Allows an empty list to be  returned  without  |
                    error;   without  this  switch  an  error  is  |
                    returned if the result list would be empty.    |

                                  -                                                             |
                    -                                           |
                    |                                              |
                    Marks the end of switches.  The argument fol-  |
                    lowing  this one will be treated as a _p_a_t_t_e_r_n  |
                    even if it starts with a -.

     The _p_a_t_t_e_r_n arguments may contain any of the following  spe-
     cial characters:

     ?         Matches any single character.

     *         Matches any sequence of zero or more characters.

     [_c_h_a_r_s]   Matches any single character in _c_h_a_r_s.   If  _c_h_a_r_s
               contains a sequence of the form _a-_b then any char-
               acter between _a and _b (inclusive) will match.

     \_x        Matches the character _x.

     {_a,_b,...} Matches any of the strings _a, _b, etc.

     As with csh, a  ``.'' at the beginning of a file's  name  or
     just  after  a ``/'' must be matched explicitly or with a {}
     construct.   In  addition,  all  ``/''  characters  must  be
     matched explicitly.



Tcl                     Last change: 7.0                        1






glob(n)               Tcl Built-In Commands               glob(n)



     If the first character in a _p_a_t_t_e_r_n is ``~'' then it  refers
     to  the  home  directory for the user whose name follows the
     ``~''.  If the ``~'' is followed immediately by  ``/''  then
     the value of the HOME environment variable is used.

     The glob command differs from  csh  globbing  in  two  ways.
     First,  it does not sort its result list (use the lsort com-
     mand if you  want  the  list  sorted).   Second,  glob  only  |
     returns  the  names of files that actually exist;  in csh no  |
     check for existence is made unless a pattern contains  a  ?,  |
     *, or [] construct.


KEYWORDS
     exist, file, glob, pattern








































Tcl                     Last change: 7.0                        2







case(n)               Tcl Built-In Commands               case(n)



_________________________________________________________________

NAME
     case - Evaluate one of several scripts, depending on a given
     value

SYNOPSIS
     case _s_t_r_i_n_g ?in? _p_a_t_L_i_s_t _b_o_d_y ?_p_a_t_L_i_s_t _b_o_d_y ...?
     case _s_t_r_i_n_g ?in? {_p_a_t_L_i_s_t _b_o_d_y ?_p_a_t_L_i_s_t _b_o_d_y ...?}
_________________________________________________________________


DESCRIPTION
     _N_o_t_e: _t_h_e case _c_o_m_m_a_n_d _i_s _o_b_s_o_l_e_t_e _a_n_d _i_s _s_u_p_p_o_r_t_e_d _o_n_l_y _f_o_r
     _b_a_c_k_w_a_r_d  _c_o_m_p_a_t_i_b_i_l_i_t_y.  _A_t _s_o_m_e _p_o_i_n_t _i_n _t_h_e _f_u_t_u_r_e _i_t _m_a_y
     _b_e _r_e_m_o_v_e_d _e_n_t_i_r_e_l_y.  _Y_o_u  _s_h_o_u_l_d  _u_s_e  _t_h_e  switch  _c_o_m_m_a_n_d
     _i_n_s_t_e_a_d.

     The case command matches _s_t_r_i_n_g against each of the  _p_a_t_L_i_s_t
     arguments  in order.  Each _p_a_t_L_i_s_t argument is a list of one
     or more patterns.  If any of these patterns  matches  _s_t_r_i_n_g
     then  case  evaluates the following _b_o_d_y argument by passing
     it recursively to the Tcl interpreter and returns the result
     of  that  evaluation.   Each  _p_a_t_L_i_s_t argument consists of a
     single pattern or list of patterns.  Each pattern  may  con-
     tain any of the wild-cards described under string match.  If
     a _p_a_t_L_i_s_t argument is default, the corresponding  body  will
     be  evaluated  if  no _p_a_t_L_i_s_t matches _s_t_r_i_n_g.  If no _p_a_t_L_i_s_t
     argument matches _s_t_r_i_n_g and no default is  given,  then  the
     case command returns an empty string.

     Two syntaxes are provided for the  _p_a_t_L_i_s_t  and  _b_o_d_y  argu-
     ments.   The  first uses a separate argument for each of the
     patterns and commands; this form is convenient if  substitu-
     tions  are desired on some of the patterns or commands.  The
     second form places all of the patterns and commands together
     into  a  single argument; the argument must have proper list
     structure, with the elements of the list being the  patterns
     and  commands.   The  second form makes it easy to construct
     multi-line case commands, since the braces around the  whole
     list  make  it unnecessary to include a backslash at the end
     of each line.  Since the _p_a_t_L_i_s_t arguments are in braces  in
     the  second  form,  no command or variable substitutions are
     performed on them;  this makes the behavior  of  the  second
     form different than the first form in some cases.


KEYWORDS
     case, match, regular expression






Tcl                     Last change: 7.0                        1







scrollbar(n)               Tk Commands               scrollbar(n)



_________________________________________________________________

NAME
     scrollbar - Create and manipulate scrollbar widgets

SYNOPSIS
     scrollbar _p_a_t_h_N_a_m_e ?_o_p_t_i_o_n_s?

STANDARD OPTIONS
     activeBackground               highlightColor  relief         |
     background      highlightThickness             repeatDelay    |
     borderWidth     jump           repeatInterval                 |
     cursor          orient         troughColor

     See the ``options'' manual entry for details on the standard
     options.

WIDGET-SPECIFIC OPTIONS
     Name:           activeRelief                                  |
     Class:          ActiveRelief                                  |
     Command-Line Switch:-activerelief                             |

                                                                |
          |                                                        |
          Specifies the relief to use when displaying the element  |
          that is active, if any.  Elements other than the active  |
          element are always displayed with a raised relief.

     Name:           command
     Class:          Command
     Command-Line Switch:-command

          Specifies the prefix of a  Tcl  command  to  invoke  to
          change  the  view  in  the  widget  associated with the
          scrollbar.  When a user requests a view change by mani-
          pulating  the scrollbar, a Tcl command is invoked.  The
          actual command consists  of  this  option  followed  by  |
          additional information as described later.

     Name:           width
     Class:          Width
     Command-Line Switch:-width

          Specifies the desired narrow dimension of the scrollbar
          window, not including 3-D border, if any.  For vertical
          scrollbars this will be the width  and  for  horizontal
          scrollbars this will be the height.  The value may have
          any of the forms acceptable to Tk_GetPixels.
_________________________________________________________________






Tk                      Last change: 4.0                        1






scrollbar(n)               Tk Commands               scrollbar(n)



DESCRIPTION
     The scrollbar command creates a new  window  (given  by  the
     _p_a_t_h_N_a_m_e  argument)  and  makes  it into a scrollbar widget.
     Additional options, described above, may be specified on the
     command  line or in the option database to configure aspects
     of the  scrollbar  such  as  its  colors,  orientation,  and
     relief.   The  scrollbar  command returns its _p_a_t_h_N_a_m_e argu-
     ment.  At the time this command is invoked, there  must  not
     exist  a  window  named _p_a_t_h_N_a_m_e, but _p_a_t_h_N_a_m_e's parent must
     exist.

     A scrollbar is a widget that displays  two  arrows,  one  at
     each  end  of the scrollbar, and a _s_l_i_d_e_r in the middle por-
     tion of the scrollbar.  It provides information  about  what
     is visible in an _a_s_s_o_c_i_a_t_e_d _w_i_n_d_o_w that displays an document
     of some sort (such as a file being  edited  or  a  drawing).
     The  position  and size of the slider indicate which portion
     of the document is visible in the  associated  window.   For
     example,  if  the  slider in a vertical scrollbar covers the
     top third of the area between the two arrows, it means  that
     the  associated  window  displays the top third of its docu-
     ment.

     Scrollbars can be used to adjust the view in the  associated
     window  by  clicking  or  dragging  with the mouse.  See the
     BINDINGS section below for details.


ELEMENTS
     A scrollbar displays five elements, which are referred to in  |
     the widget commands for the scrollbar:                        |

     arrow1                                                     |
               |                                                   |
               The top or left arrow in the scrollbar.             |

     trough1                                                    |
               |                                                   |
               The region between the slider and arrow1.           |

     slider                                                     |
               |                                                   |
               The rectangle that indicates what  is  visible  in  |
               the associated widget.                              |

     trough2                                                    |
               |                                                   |
               The region between the slider and arrow2.           |

     arrow2                                                     |
               |                                                   |
               The bottom or right arrow in the scrollbar.         |



Tk                      Last change: 4.0                        2






scrollbar(n)               Tk Commands               scrollbar(n)



WIDGET COMMAND                                                     |
     The scrollbar command creates a new Tcl command  whose  name  |
     is  _p_a_t_h_N_a_m_e.   This  command  may be used to invoke various  |
     operations on the widget.   It  has  the  following  general  |
     form:                                                         |

          _p_a_t_h_N_a_m_e _o_p_t_i_o_n ?_a_r_g _a_r_g ...?                            |

     _O_p_t_i_o_n and the _a_r_gs determine the exact behavior of the com-  |
     mand.   The  following  commands  are possible for scrollbar  |
     widgets:                                                      |

     _p_a_t_h_N_a_m_e activate ?_e_l_e_m_e_n_t?                                |
          |                                                        |
          Marks the element indicated by _e_l_e_m_e_n_t as active, which  |
          causes   it   to  be  displayed  as  specified  by  the  |
          activeBackground and activeRelief  options.   The  only  |
          element  values  understood by this command are arrow1,  |
          slider, or arrow2.  If any  other  value  is  specified  |
          then  no  element  of the scrollbar will be active.  If  |
          _e_l_e_m_e_n_t is not specified, the command returns the  name  |
          of  the  element  that is currently active, or an empty  |
          string if no element is active.                          |

     _p_a_t_h_N_a_m_e cget _o_p_t_i_o_n                                       |
          |                                                        |
          Returns the current value of the  configuration  option  |
          given  by  _o_p_t_i_o_n.   _O_p_t_i_o_n  may have any of the values  |
          accepted by the scrollbar command.

     _p_a_t_h_N_a_m_e configure ?_o_p_t_i_o_n? ?_v_a_l_u_e _o_p_t_i_o_n _v_a_l_u_e ...?
          Query  or  modify  the  configuration  options  of  the
          widget.   If  no  _o_p_t_i_o_n  is  specified, returns a list
          describing all of the available  options  for  _p_a_t_h_N_a_m_e
          (see  Tk_ConfigureInfo for information on the format of
          this list).  If _o_p_t_i_o_n is specified with no _v_a_l_u_e, then
          the  command  returns  a  list describing the one named
          option (this list will be identical to the  correspond-
          ing  sublist  of  the  value  returned  if no _o_p_t_i_o_n is
          specified).  If one or more  _o_p_t_i_o_n - _v_a_l_u_e  pairs  are
          specified,  then  the command modifies the given widget
          option(s) to have the given value(s);  in this case the
          command  returns  an empty string.  _O_p_t_i_o_n may have any
          of the values accepted by the scrollbar command.

     _p_a_t_h_N_a_m_e fraction _x _y
          Returns a real number between 0 and 1 indicating  where  |
          the  point  given by _x and _y lies in the trough area of  |
          the scrollbar.  The value 0 corresponds to the  top  or  |
          left of the trough, the value 1 corresponds to the bot-  |
          tom or right, 0.5 corresponds to the middle, and so on.  |
          _X  and  _y  must  be  pixel  coordinates relative to the  |



Tk                      Last change: 4.0                        3






scrollbar(n)               Tk Commands               scrollbar(n)



          scrollbar widget.  If _x and _y refer to a point  outside  |
          the trough, the closest point in the trough is used.     |

     _p_a_t_h_N_a_m_e get                                               |
          |                                                        |
          Returns the scrollbar settings in the form  of  a  list  |
          whose elements are the arguments to the most recent set  |
          widget command.                                          |

     _p_a_t_h_N_a_m_e identify _x _y                                      |
          |                                                        |
          Returns the name of the element under the  point  given  |
          by  _x and _y (such as arrow1), or an empty string if the  |
          point does not lie in any element of the scrollbar.   _X  |
          and  _y  must  be  pixel  coordinates  relative  to  the  |
          scrollbar widget.                                        |

     _p_a_t_h_N_a_m_e set _f_i_r_s_t _l_a_s_t                                    |
          |                                                        |
          This command is invoked by the  scrollbar's  associated  |
          widget  to tell the scrollbar about the current view in  |
          the widget.  The command takes two arguments,  each  of  |
          which  is  a  real fraction between 0 and 1.  The frac-  |
          tions describe the range of the document that is  visi-  |
          ble in the associated widget.  For example, if _f_i_r_s_t is  |
          0.2 and _l_a_s_t is 0.4, it means that the  first  part  of  |
          the  document  visible  in the window is 20% of the way  |
          through the document, and the last visible part is  40%  |
          of the way through.                                      |


SCROLLING COMMANDS                                                 |
     When the user interacts with the scrollbar, for  example  by  |
     dragging  the  slider, the scrollbar notifies the associated  |
     widget that it must change its view.   The  scrollbar  makes  |
     the  notification by evaluating a Tcl command generated from  |
     the scrollbar's -command option.  The command may  take  any  |
     of  the  following  forms.  In each case, _p_r_e_f_i_x is the con-  |
     tents of the -command option, which usually has a form  like  |
     .t yview                                                      |

     _p_r_e_f_i_x moveto _f_r_a_c_t_i_o_n                                     |
          |                                                        |
          _F_r_a_c_t_i_o_n is a real number between 0 and 1.  The  widget  |
          should adjust its view so that the point given by _f_r_a_c_-  |
          _t_i_o_n appears at the beginning of the widget.  If  _f_r_a_c_-  |
          _t_i_o_n  is  0 it refers to the beginning of the document.  |
          1.0 refers to the end of the document, 0.333 refers  to  |
          a  point one-third of the way through the document, and  |
          so on.                                                   |

     _p_r_e_f_i_x scroll _n_u_m_b_e_r unit                                  |



Tk                      Last change: 4.0                        4






scrollbar(n)               Tk Commands               scrollbar(n)



          | The  widget  should  adjust its view by _n_u_m_b_e_r units.  |
          The units are defined in whatever way makes  sense  for  |
          the  widget,  such  as  characters  or  lines in a text  |
          widget.  _N_u_m_b_e_r is  either  1,  which  means  one  unit  |
          should scroll off the top or left of the window, or -1,  |
          which means that one unit should scroll off the  bottom  |
          or right of the window.                                  |

     _p_r_e_f_i_x scroll _n_u_m_b_e_r page                                  |
          |                                                        |
          The widget should adjust its view by _n_u_m_b_e_r pages.   It  |
          is  up  to  the widget to define the meaning of a page;  |
          typically it is slightly less than  what  fits  in  the  |
          window,  so  that there is a slight overlap between the  |
          old and new views.  _N_u_m_b_e_r is either 1, which means the  |
          next  page  should  become  visible, or -1, which means  |
          that the previous page should become visible.            |


OLD COMMAND SYNTAX                                                 |
     In versions of Tk before 4.0, the set and  get  widget  com-  |
     mands  used  a different form.  This form is still supported  |
     for backward compatibility, but it is  deprecated.   In  the  |
     old command syntax, the set widget command has the following  |
     form:                                                         |

     _p_a_t_h_N_a_m_e set _t_o_t_a_l_U_n_i_t_s _w_i_n_d_o_w_U_n_i_t_s _f_i_r_s_t_U_n_i_t _l_a_s_t_U_n_i_t     |
          |                                                        |
          In this form the arguments are all integers.   _T_o_t_a_l_U_n_-  |
          _i_t_s  gives the total size of the object being displayed  |
          in the associated widget.   The  meaning  of  one  unit  |
          depends  on  the  associated widget;  for example, in a  |
          text editor widget units might correspond to  lines  of  |
          text.   _W_i_n_d_o_w_U_n_i_t_s indicates the total number of units  |
          that can fit in the  associated  window  at  one  time.  |
          _F_i_r_s_t_U_n_i_t  and  _l_a_s_t_U_n_i_t  give the indices of the first  |
          and last units currently visible in the associated win-  |
          dow (zero corresponds to the first unit of the object).  |

     Under the old syntax the get widget command returns  a  list  |
     of four integers, consisting of the _t_o_t_a_l_U_n_i_t_s, _w_i_n_d_o_w_U_n_i_t_s,  |
     _f_i_r_s_t_U_n_i_t, and _l_a_s_t_U_n_i_t values from the last set widget com-  |
     mand.                                                         |

     The commands generated by scrollbars also have  a  different  |
     form when the old syntax is being used:                       |

     _p_r_e_f_i_x _u_n_i_t                                                |
          |                                                        |
          _U_n_i_t is an integer that indicates what should appear at  |
          the  top or left of the associated widget's window.  It  |
          has the same meaning  as  the  _f_i_r_s_t_U_n_i_t  and  _l_a_s_t_U_n_i_t  |



Tk                      Last change: 4.0                        5






scrollbar(n)               Tk Commands               scrollbar(n)



          arguments to the set widget command.                     |

     The most recent set widget command determines whether or not  |
     to  use  the  old syntax.  If it is given two real arguments  |
     then the new syntax will be used in the future, and if it is  |
     given  four  integer  arguments  then the old syntax will be  |
     used.                                                         |


BINDINGS                                                           |
     Tk automatically creates class bindings for scrollbars  that  |
     give  them  the following default behavior.  If the behavior  |
     is different for vertical  and  horizontal  scrollbars,  the  |
     horizontal behavior is described in parentheses.              |


     [1]                                                        |
          |                                                        |
          Pressing button 1 over arrow1 causes the  view  in  the  |
          associated  widget  to  shift  up (left) by one unit so  |
          that the document appears  to  move  down  (right)  one  |
          unit.   If  the  button  is held down, the action auto-  |
          repeats.                                                 |

     [2]                                                        |
          |                                                        |
          Pressing button 1 over trough1 causes the view  in  the  |
          associated  widget  to shift up (left) by one screenful  |
          so that the document appears to move down  (right)  one  |
          screenful.   If  the  button  is  held down, the action  |
          auto-repeats.                                            |

     [3]                                                        |
          |                                                        |
          Pressing button 1 over the slider and  dragging  causes  |
          the  view  to  drag  with  the slider.  If the mouse is  |
          moved well outside the widget before releasing the but-  |
          ton,  the view returns to what it was before the button  |
          was pressed.  If the jump option is true, then the view  |
          doesn't  drag  along  with the slider;  it changes only  |
          when the mouse button is released.                       |

     [4]                                                        |
          |                                                        |
          Pressing button 1 over trough2 causes the view  in  the  |
          associated  widget to shift down (right) by one screen-  |
          ful so that the document appears to move up (left)  one  |
          screenful.   If  the  button  is  held down, the action  |
          auto-repeats.                                            |

     [5]                                                        |
          |                                                        |



Tk                      Last change: 4.0                        6






scrollbar(n)               Tk Commands               scrollbar(n)



          Pressing button 1 over arrow2 causes the  view  in  the  |
          associated  widget to shift down (right) by one unit so  |
          that the document appears to move up (left)  one  unit.  |
          If the button is held down, the action auto-repeats.     |

     [6]                                                        |
          |                                                        |
          All of the above actions may be invoked with  button  2  |
          as well as with button 1.                                |

     [7]                                                        |
          |                                                        |
          If button 1 is pressed with the Control key down,  then  |
          if the mouse is over arrow1 or trough1 the view changes  |
          to the very top (left) of the document;  if  the  mouse  |
          is  over arrow2 or trough2 the view changes to the very  |
          bottom (right) of the document;  if the mouse  is  any-  |
          where else then the button press has no effect.          |

     [8]                                                        |
          |                                                        |
          In vertical scrollbars the Up and Down  keys  have  the  |
          same  behavior  as mouse clicks over arrow1 and arrow2,  |
          respectively.  In horizontal scrollbars these keys have  |
          no effect.                                               |

     [9]                                                        |
          |                                                        |
          In vertical scrollbars Control-Up and Control-Down have  |
          the  same  behavior  as  mouse  clicks over trough1 and  |
          trough2, respectively.  In horizontal scrollbars  these  |
          keys have no effect.                                     |

     [10]                                                       |
          |                                                        |
          In horizontal scrollbars the Up and Down keys have  the  |
          same  behavior  as mouse clicks over arrow1 and arrow2,  |
          respectively.  In vertical scrollbars these  keys  have  |
          no effect.                                               |

     [11]                                                       |
          |                                                        |
          In horizontal scrollbars  Control-Up  and  Control-Down  |
          have the same behavior as mouse clicks over trough1 and  |
          trough2, respectively.  In  vertical  scrollbars  these  |
          keys have no effect.                                     |

     [12]                                                       |
          |                                                        |
          The Prior and Next keys have the same behavior as mouse  |
          clicks over trough1 and trough2, respectively.           |




Tk                      Last change: 4.0                        7






scrollbar(n)               Tk Commands               scrollbar(n)



     [13]                                                       |
          |                                                        |
          The Home key adjusts the view to the top (left edge) of  |
          the document.                                            |

     [14]                                                       |
          |                                                        |
          The End key adjusts the view to the bottom (right edge)  |
          of the document.


KEYWORDS
     scrollbar, widget










































Tk                      Last change: 4.0                        8







entry(n)                   Tk Commands                   entry(n)



_________________________________________________________________

NAME
     entry - Create and manipulate entry widgets

SYNOPSIS
     entry _p_a_t_h_N_a_m_e ?_o_p_t_i_o_n_s?

STANDARD OPTIONS
     background      foreground     insertOffTime   selectBackground
     borderWidth     highlightColor insertOnTime    selectBorderWidth|
     cursor          highlightThickness             insertWidthselectForeground|
     exportSelection insertBackground               justifytextVariable|
     font            insertBorderWidth              reliefxScrollCommand

     See the ``options'' manual entry for details on the standard
     options.

WIDGET-SPECIFIC OPTIONS
     Name:           show                                          |
     Class:          Show                                          |
     Command-Line Switch:-show                                     |

                                                                |
          |                                                        |
          If this option is specified, then the true contents  of  |
          the  entry  are  not displayed in the window.  Instead,  |
          each character in the entry's value will  be  displayed  |
          as  the  first  character  in the value of this option,  |
          such as ``*''.  This is useful,  for  example,  if  the  |
          entry is to be used to enter a password.  If characters  |
          in the entry are selected  and  copied  elsewhere,  the  |
          information  copied  will be what is displayed, not the  |
          true contents of the entry.

     Name:           state
     Class:          State
     Command-Line Switch:-state

          Specifies one of two states for the entry:   normal  or
          disabled.   If the entry is disabled then the value may
          not be changed using widget commands and  no  insertion
          cursor will be displayed, even if the input focus is in
          the widget.

     Name:           width
     Class:          Width
     Command-Line Switch:-width

          Specifies an integer value indicating the desired width
          of  the entry window, in average-size characters of the
          widget's font.  If the value is less than or  equal  to  |



Tk                      Last change: 4.0                        1






entry(n)                   Tk Commands                   entry(n)



          zero, the widget picks a size just large enough to hold  |
          its current text.
_________________________________________________________________


DESCRIPTION
     The entry command creates a new window (given by  the  _p_a_t_h_-
     _N_a_m_e  argument)  and  makes  it into an entry widget.  Addi-
     tional options, described above, may  be  specified  on  the
     command  line or in the option database to configure aspects
     of the entry such as its  colors,  font,  and  relief.   The
     entry  command  returns  its _p_a_t_h_N_a_m_e argument.  At the time
     this command is invoked, there must not exist a window named
     _p_a_t_h_N_a_m_e, but _p_a_t_h_N_a_m_e's parent must exist.

     An entry is a widget that displays a  one-line  text  string
     and  allows  that  string to be edited using widget commands
     described below, which are typically bound to keystrokes and
     mouse  actions.   When  first  created, an entry's string is
     empty.  A portion of the entry may be selected as  described
     below.   If  an  entry  is  exporting its selection (see the
     exportSelection option), then it will observe  the  standard
     X11  protocols for handling the selection;  entry selections
     are available as type  STRING.   Entries  also  observe  the
     standard Tk rules for dealing with the input focus.  When an
     entry has the input focus it displays an _i_n_s_e_r_t_i_o_n _c_u_r_s_o_r to
     indicate where new characters will be inserted.

     Entries are capable of displaying strings that are too  long
     to  fit  entirely within the widget's window.  In this case,
     only a portion of the string will  be  displayed;   commands
     described  below  may be used to change the view in the win-
     dow.  Entries use the standard xScrollCommand mechanism  for
     interacting  with  scrollbars  (see  the  description of the
     xScrollCommand option for details).  They also support scan-
     ning, as described below.


WIDGET COMMAND
     The entry command creates a new Tcl command  whose  name  is
     _p_a_t_h_N_a_m_e.  This command may be used to invoke various opera-
     tions on the widget.  It has the following general form:

          _p_a_t_h_N_a_m_e _o_p_t_i_o_n ?_a_r_g _a_r_g ...?

     _O_p_t_i_o_n and the _a_r_gs determine the exact behavior of the com-
     mand.

     Many of the widget commands for entries  take  one  or  more
     indices as arguments.  An index specifies a particular char-
     acter in the entry's string, in any of the following ways:




Tk                      Last change: 4.0                        2






entry(n)                   Tk Commands                   entry(n)



     _n_u_m_b_e_r      Specifies the character as  a  numerical  index,
                 where  0  corresponds  to the first character in
                 the string.

     anchor      Indicates the anchor point  for  the  selection,  |
                 which  is  set  with  the select from and select  |
                 adjust widget commands.

     end         Indicates the character just after the last  one
                 in  the  entry's  string.  This is equivalent to
                 specifying a numerical index equal to the length
                 of the entry's string.

     insert      Indicates the character adjacent to and  immedi-
                 ately following the insertion cursor.

     sel.first   Indicates the first character in the  selection.
                 It is an error to use this form if the selection
                 isn't in the entry window.

     sel.last    Indicates the character just after the last  one  |
                 in  the  selection.   It is an error to use this
                 form if the selection isn't in the entry window.

     @_n_u_m_b_e_r     In  this  form,  _n_u_m_b_e_r  is  treated  as  an  x-
                 coordinate in the entry's window;  the character
                 spanning that x-coordinate is used.   For  exam-
                 ple, ``@0'' indicates the left-most character in
                 the window.

     Abbreviations may be used for any of the forms  above,  e.g.
     ``e''  or  ``sel.f''.   In general, out-of-range indices are
     automatically rounded to the nearest legal value.

     The following commands are possible for entry widgets:

     _p_a_t_h_N_a_m_e cget _o_p_t_i_o_n
          Returns the current value of the  configuration  option  |
          given  by  _o_p_t_i_o_n.   _O_p_t_i_o_n  may have any of the values  |
          accepted by the entry command.

     _p_a_t_h_N_a_m_e configure ?_o_p_t_i_o_n? ?_v_a_l_u_e _o_p_t_i_o_n _v_a_l_u_e ...?
          Query  or  modify  the  configuration  options  of  the
          widget.   If  no  _o_p_t_i_o_n  is  specified, returns a list
          describing all of the available  options  for  _p_a_t_h_N_a_m_e
          (see  Tk_ConfigureInfo for information on the format of
          this list).  If _o_p_t_i_o_n is specified with no _v_a_l_u_e, then
          the  command  returns  a  list describing the one named
          option (this list will be identical to the  correspond-
          ing  sublist  of  the  value  returned  if no _o_p_t_i_o_n is
          specified).  If one or more  _o_p_t_i_o_n - _v_a_l_u_e  pairs  are
          specified,  then  the command modifies the given widget



Tk                      Last change: 4.0                        3






entry(n)                   Tk Commands                   entry(n)



          option(s) to have the given value(s);  in this case the
          command  returns  an empty string.  _O_p_t_i_o_n may have any
          of the values accepted by the entry command.

     _p_a_t_h_N_a_m_e delete _f_i_r_s_t ?_l_a_s_t?
          Delete one or more elements of the entry.  _F_i_r_s_t is the
          index of the first character to delete, and _l_a_s_t is the  |
          index of the character  just  after  the  last  one  to  |
          delete.    If  _l_a_s_t  isn't  specified  it  defaults  to
          _f_i_r_s_t+1, i.e. a single character is deleted.  This com-
          mand returns an empty string.

     _p_a_t_h_N_a_m_e get
          Returns the entry's string.

     _p_a_t_h_N_a_m_e icursor _i_n_d_e_x
          Arrange for the insertion cursor to be  displayed  just
          before  the character given by _i_n_d_e_x.  Returns an empty
          string.

     _p_a_t_h_N_a_m_e index _i_n_d_e_x
          Returns the numerical index corresponding to _i_n_d_e_x.

     _p_a_t_h_N_a_m_e insert _i_n_d_e_x _s_t_r_i_n_g
          Insert the characters of _s_t_r_i_n_g just before the charac-
          ter indicated by _i_n_d_e_x.  Returns an empty string.

     _p_a_t_h_N_a_m_e scan _o_p_t_i_o_n _a_r_g_s
          This command is used to implement scanning on  entries.
          It has two forms, depending on _o_p_t_i_o_n:

          _p_a_t_h_N_a_m_e scan mark _x
               Records _x and the current view in the  entry  win-
               dow;   used  in conjunction with later scan dragto
               commands.  Typically this  command  is  associated
               with  a  mouse  button  press  in  the widget.  It
               returns an empty string.

          _p_a_t_h_N_a_m_e scan dragto _x
               This command computes the difference between its _x
               argument  and the _x argument to the last scan mark
               command for the widget.  It then adjusts the  view
               left  or  right  by  10 times the difference in x-
               coordinates.  This command is typically associated
               with mouse motion events in the widget, to produce
               the effect of dragging the  entry  at  high  speed
               through  the window.  The return value is an empty
               string.

     _p_a_t_h_N_a_m_e selection _o_p_t_i_o_n _a_r_g
          This command is used to adjust the selection within  an
          entry.  It has several forms, depending on _o_p_t_i_o_n:



Tk                      Last change: 4.0                        4






entry(n)                   Tk Commands                   entry(n)



          _p_a_t_h_N_a_m_e selection adjust _i_n_d_e_x
               Locate the end of the  selection  nearest  to  the
               character  given  by _i_n_d_e_x, and adjust that end of
               the selection to be at _i_n_d_e_x  (i.e  including  but
               not  going  beyond  _i_n_d_e_x).   The other end of the
               selection is made  the  anchor  point  for  future
               select   to  commands.   If  the  selection  isn't
               currently in the entry, then a  new  selection  is
               created  to  include  the characters between _i_n_d_e_x
               and  the  most  recent  selection  anchor   point,
               inclusive.  Returns an empty string.

          _p_a_t_h_N_a_m_e selection clear
               Clear the selection if it  is  currently  in  this
               widget.   If  the  selection  isn't in this widget
               then the command has no effect.  Returns an  empty
               string.

          _p_a_t_h_N_a_m_e selection from _i_n_d_e_x
               Set the selection anchor point to just before  the
               character  given  by  _i_n_d_e_x.   Doesn't  change the
               selection.  Returns an empty string.

          _p_a_t_h_N_a_m_e selection present
               Returns 1 if there is are characters  selected  in  |
               the entry, 0 if nothing is selected.                |

          _p_a_t_h_N_a_m_e selection range _s_t_a_r_t _e_n_d                    |
               |                                                   |
               Sets  the  selection  to  include  the  characters  |
               starting  with the one indexed by _s_t_a_r_t and ending  |
               with the one just before _e_n_d.  If  _e_n_d  refers  to  |
               the  same  character  as  _s_t_a_r_t or an earlier one,  |
               then the entry's selection is cleared.

          _p_a_t_h_N_a_m_e selection to _i_n_d_e_x
               If _i_n_d_e_x is  before  the  anchor  point,  set  the
               selection  to  the characters from _i_n_d_e_x up to but
               not including the anchor point.  If _i_n_d_e_x  is  the
               same as the anchor point, do nothing.  If _i_n_d_e_x is
               after the anchor point, set the selection  to  the
               characters  from  the  anchor  point up to but not
               including _i_n_d_e_x.  The anchor point  is  determined
               by  the  most  recent select from or select adjust
               command in this widget.  If the selection isn't in
               this  widget then a new selection is created using
               the most recent anchor  point  specified  for  the
               widget.  Returns an empty string.

     _p_a_t_h_N_a_m_e xview _a_r_g_s                                        |
          |                                                        |
          This command is used to query and change the horizontal  |



Tk                      Last change: 4.0                        5






entry(n)                   Tk Commands                   entry(n)



          position  of  the  text in the widget's window.  It can  |
          take any of the following forms:                         |

          _p_a_t_h_N_a_m_e xview                                        |
               |                                                   |
               Returns a list containing two elements.  Each ele-  |
               ment is a real fraction between 0 and 1;  together  |
               they describe the horizontal span that is  visible  |
               in  the window.  For example, if the first element  |
               is .2 and the second element is  .6,  20%  of  the  |
               entry's text is off-screen to the left, the middle  |
               40% is visible in the window, and 40% of the  text  |
               is  off-screen  to  the right.  These are the same  |
               values passed to scrollbars via the  - xscrollcom-  |
               mand option.                                        |

          _p_a_t_h_N_a_m_e xview _i_n_d_e_x                                  |
               |                                                   |
               Adjusts the view in the window so that the charac-  |
               ter  given  by _i_n_d_e_x is displayed at the left edge  |
               of the window.                                      |

          _p_a_t_h_N_a_m_e xview moveto _f_r_a_c_t_i_o_n                        |
               |                                                   |
               Adjusts the view in the window so that the charac-  |
               ter  _f_r_a_c_t_i_o_n  of the way through the text appears  |
               at the left edge of the window.  _F_r_a_c_t_i_o_n must  be  |
               a fraction between 0 and 1.                         |

          _p_a_t_h_N_a_m_e xview scroll _n_u_m_b_e_r _w_h_a_t                     |
               |                                                   |
               This command shifts the view in the window left or  |
               right  according  to _n_u_m_b_e_r and _w_h_a_t.  _N_u_m_b_e_r must  |
               be an integer.  _W_h_a_t must be either units or pages  |
               or  an  abbreviation  of one of these.  If _w_h_a_t is  |
               units, the view adjusts left or  right  by  _n_u_m_b_e_r  |
               average-width characters on the display;  if it is  |
               pages then the view adjusts by _n_u_m_b_e_r  screenfuls.  |
               If  _n_u_m_b_e_r  is negative then characters farther to  |
               the left become visible;  if it is  positive  then  |
               characters farther to the right become visible.     |


DEFAULT BINDINGS
     Tk automatically creates class  bindings  for  entries  that
     give  them  the following default behavior.  In the descrip-  |
     tions below,  ``word''  refers  to  a  contiguous  group  of  |
     letters,  digits, or ``_'' characters, or any single charac-  |
     ter other than these.                                         |

     [1]                                                        |
          |                                                        |



Tk                      Last change: 4.0                        6






entry(n)                   Tk Commands                   entry(n)



          Clicking mouse button 1 positions the insertion  cursor  |
          just  before the character underneath the mouse cursor,  |
          sets the input focus to this  widget,  and  clears  any  |
          selection  in the widget.  Dragging with mouse button 1  |
          strokes out a selection between  the  insertion  cursor  |
          and the character under the mouse.                       |

     [2]                                                        |
          |                                                        |
          Double-clicking with mouse button 1  selects  the  word  |
          under  the  mouse and positions the insertion cursor at  |
          the beginning of the word.   Dragging  after  a  double  |
          click  will  stroke out a selection consisting of whole  |
          words.                                                   |

     [3]                                                        |
          |                                                        |
          Triple-clicking with mouse button 1 selects all of  the  |
          text  in  the  entry and positions the insertion cursor  |
          before the first character.                              |

     [4]                                                        |
          |                                                        |
          The ends of the selection can be adjusted  by  dragging  |
          with  mouse button 1 while the Shift key is down;  this  |
          will adjust the end of the selection that  was  nearest  |
          to  the mouse cursor when button 1 was pressed.  If the  |
          button  is  double-clicked  before  dragging  then  the  |
          selection will be adjusted in units of whole words.      |

     [5]                                                        |
          |                                                        |
          Clicking mouse button 1 with the Control key down  will  |
          position  the  insertion  cursor  in  the entry without  |
          affecting the selection.                                 |

     [6]                                                        |
          |                                                        |
          If any normal  printing  characters  are  typed  in  an  |
          entry,  they are inserted at the point of the insertion  |
          cursor.                                                  |

     [7]                                                        |
          |                                                        |
          The view in the entry can be adjusted by dragging  with  |
          mouse  button  2.  If mouse button 2 is clicked without  |
          moving the mouse, the  selection  is  copied  into  the  |
          entry   at   the  position  of  the  insertion  cursor.  |
          Control-v also inserts the selection.                    |

     [8]                                                        |
          |                                                        |



Tk                      Last change: 4.0                        7






entry(n)                   Tk Commands                   entry(n)



          If the mouse is dragged out of the entry on the left or  |
          right  sides  while button 1 is pressed, the entry will  |
          automatically scroll to  make  more  text  visible  (if  |
          there  is  more  text  off-screen on the side where the  |
          mouse left the window).                                  |

     [9]                                                        |
          |                                                        |
          The Left and Right keys move the insertion  cursor  one  |
          character  to  the  left or right;  they also clear any  |
          selection in the entry and set  the  selection  anchor.  |
          If Left or Right is typed with the Shift key down, then  |
          the  insertion  cursor  moves  and  the  selection   is  |
          extended  to  include  the new character.  Control-Left  |
          and Control-Right move the insertion cursor  by  words,  |
          and Control-Shift-Left and Control-Shift-Right move the  |
          insertion cursor by words and also  extend  the  selec-  |
          tion.   Control-b and Control-f behave the same as Left  |
          and Right, respectively.  Meta-b and Meta-f behave  the  |
          same as Control-Left and Control-Right, respectively.    |

     [10]                                                       |
          |                                                        |
          The Home key, or Control-a,  will  move  the  insertion  |
          cursor  to  the  beginning  of  the entry and clear any  |
          selection in the entry.  Shift-Home moves the insertion  |
          cursor  to  the beginning of the entry and also extends  |
          the selection to that point.                             |

     [11]                                                       |
          |                                                        |
          The End key, or Control-e, will move the insertion cur-  |
          sor  to the end of the entry and clear any selection in  |
          the entry.  Shift-End moves the cursor to the  end  and  |
          extends the selection to that point.                     |

     [12]                                                       |
          |                                                        |
          The Select key  and  Control-Space  set  the  selection  |
          anchor  to  the position of the insertion cursor.  They  |
          don't affect the current selection.   Shift-Select  and  |
          Control-Shift-Space adjust the selection to the current  |
          position of the insertion cursor,  selecting  from  the  |
          anchor  to  the  insertion  cursor if there was not any  |
          selection previously.                                    |

     [13]                                                       |
          |                                                        |
          Control-/ selects all the text in the entry.             |

     [14]                                                       |
          |                                                        |



Tk                      Last change: 4.0                        8






entry(n)                   Tk Commands                   entry(n)



          Control-\ clears any selection in the entry.             |

     [15]                                                       |
          |                                                        |
          The F16 key (labelled Copy on many Sun workstations) or  |
          Meta-w  copies the selection in the widget to the clip-  |
          board, if there is a selection.                          |

     [16]                                                       |
          |                                                        |
          The F20 key (labelled Cut on many Sun workstations)  or  |
          Control-w  copies  the  selection  in the widget to the  |
          clipboard and deletes the selection.  If  there  is  no  |
          selection in the widget then these keys have no effect.  |

     [17]                                                       |
          |                                                        |
          The F18 key (labelled Paste on many  Sun  workstations)  |
          or  Control-y  inserts the contents of the clipboard at  |
          the position of the insertion cursor.                    |

     [18]                                                       |
          |                                                        |
          The Delete key deletes the selection, if there  is  one  |
          in the entry.  If there is no selection, it deletes the  |
          character to the right of the insertion cursor.          |

     [19]                                                       |
          |                                                        |
          The BackSpace key and Control-h delete  the  selection,  |
          if  there  is  one in the entry.  If there is no selec-  |
          tion, it deletes the  character  to  the  left  of  the  |
          insertion cursor.                                        |

     [20]                                                       |
          |                                                        |
          Control-d deletes the character to  the  right  of  the  |
          insertion cursor.                                        |

     [21]                                                       |
          |                                                        |
          Meta-d deletes the word to the right of  the  insertion  |
          cursor.                                                  |

     [22]                                                       |
          |                                                        |
          Control-k deletes all the characters to  the  right  of  |
          the insertion cursor.                                    |

     [23]                                                       |
          |                                                        |
          Control-w deletes the word to the left of the insertion  |



Tk                      Last change: 4.0                        9






entry(n)                   Tk Commands                   entry(n)



          cursor.                                                  |

     [24]                                                       |
          |                                                        |
          Control-x deletes the selection, if there is one in the  |
          entry.                                                   |

     [25]                                                       |
          |                                                        |
          Control-t reverses the order of the two  characters  to  |
          the right of the insertion cursor.

     If the entry is disabled using the -state option,  then  the
     entry's view can still be adjusted and text in the entry can
     still be selected, but no insertion cursor will be displayed
     and no text modifications will take place.

     The behavior of entries can be changed by defining new bind-
     ings for individual widgets or by redefining the class bind-
     ings.


KEYWORDS
     entry, widget































Tk                      Last change: 4.0                       10




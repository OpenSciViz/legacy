


open(n)               Tcl Built-In Commands               open(n)



_________________________________________________________________

NAME
     open - Open a file

SYNOPSIS
     open _f_i_l_e_N_a_m_e ?_a_c_c_e_s_s? ?_p_e_r_m_i_s_s_i_o_n_s?                          |
_________________________________________________________________


DESCRIPTION
     This command opens a file and returns an identifier that may
     be  used  in future invocations of commands like read, puts,
     and close.  _F_i_l_e_N_a_m_e gives the name of the file to open;  if
     it  starts with a tilde then tilde substitution is performed
     as described for Tcl_TildeSubst.  If the first character  of
     _f_i_l_e_N_a_m_e  is ``|'' then the remaining characters of _f_i_l_e_N_a_m_e
     are treated as a command pipeline to  invoke,  in  the  same
     style as for exec.  In this case, the identifier returned by
     open may be used to write to the  command's  input  pipe  or
     read from its output pipe.

     The _a_c_c_e_s_s argument indicates the way in which the file  (or
     command pipeline) is to be accessed.  It may take two forms,  |
     either a string in the form that  would  be  passed  to  the  |
     fopen library procedure or a list of POSIX access flags.  It  |
     defaults to ``r''.  In the first form _a_c_c_e_s_s may have any of  |
     the following values:

     r              Open the file for reading only; the file must
                    already exist.

     r+             Open the file for both reading  and  writing;
                    the file must already exist.

     w              Open the file for writing only.  Truncate  it
                    if  it exists.  If it doesn't exist, create a
                    new file.

     w+             Open the file for reading and writing.  Trun-
                    cate  it  if it exists.  If it doesn't exist,
                    create a new file.

     a              Open the file for  writing  only.   The  file
                    must  already  exist,  and  the file is posi-
                    tioned so that new data is  appended  to  the
                    file.

     a+             Open the file for reading  and  writing.   If
                    the  file  doesn't  exist, create a new empty
                    file.  Set the initial  access  position   to
                    the end of the file.



Tcl                     Last change: 7.0                        1






open(n)               Tcl Built-In Commands               open(n)



     In the second form, _a_c_c_e_s_s consists of a list of any of  the  |
     following  flags, all of which have the standard POSIX mean-  |
     ings.  One of the flags must be  either  RDONLY,  WRONLY  or  |
     RDWR.                                                         |

     RDONLY                                                     |
                    |                                              |
                    Open the file for reading only.                |

     WRONLY                                                     |
                    |                                              |
                    Open the file for writing only.                |

     RDWR                                                       |
                    |                                              |
                    Open the file for both reading and writing.    |

     APPEND                                                     |
                    |                                              |
                    Set the file pointer to the end of  the  file  |
                    prior to each write.                           |

     CREAT                                                      |
                    |                                              |
                    Create the file if it doesn't  already  exist  |
                    (without  this  flag  it  is an error for the  |
                    file not to exist).                            |

     EXCL                                                       |
                    |                                              |
                    If CREAT  is  specified  also,  an  error  is  |
                    returned if the file already exists.           |

     NOCTTY                                                     |
                    |                                              |
                    If the file is a terminal device,  this  flag  |
                    prevents  the file from becoming the control-  |
                    ling terminal of the process.                  |

     NON-  |
                    BLOCK                                       |
                    |                                              |
                    Prevents  the  process  from  blocking  while  |
                    opening  the file.  For details refer to your  |
                    system  documentation  on  the  open   system  |
                    call's O_NONBLOCK flag.                        |

     TRUNC                                                      |
                    |                                              |
                    If the file exists it is  truncated  to  zero  |
                    length.                                        |




Tcl                     Last change: 7.0                        2






open(n)               Tcl Built-In Commands               open(n)



     If a new file is created as part of opening it,  _p_e_r_m_i_s_s_i_o_n_s  |
     (an integer) is used to set the permissions for the new file  |
     in conjunction with the process's file mode  creation  mask.  |
     _P_e_r_m_i_s_s_i_o_n_s defaults to 0666.

     If a file is opened for both reading and writing  then  seek
     must  be  invoked  between a read and a write, or vice versa
     (this restriction does not apply to command pipelines opened
     with  open).  When _f_i_l_e_N_a_m_e specifies a command pipeline and
     a write-only access is used, then standard output  from  the
     pipeline  is  directed to the current standard output unless
     overridden by the command.  When _f_i_l_e_N_a_m_e specifies  a  com-
     mand  pipeline and a read-only access is used, then standard
     input from the pipeline is taken from the  current  standard
     input unless overridden by the command.


KEYWORDS
     access mode, append,  controlling  terminal,  create,  file,
     non-blocking, open, permissions, pipeline, process



































Tcl                     Last change: 7.0                        3




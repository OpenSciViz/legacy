


tkvars(n)                  Tk Commands                  tkvars(n)



_________________________________________________________________

NAME
     tkvars - Variables used or set by Tk
_________________________________________________________________


DESCRIPTION
     The following Tcl variables are either set or used by Tk  at
     various times in its execution:

     tk_library     Tk sets this variable  hold  the  name  of  a
                    directory containing a library of Tcl scripts
                    related to Tk.  These scripts include an ini-
                    tialization  file  that is normally processed
                    whenever a Tk  application  starts  up,  plus
                    other files containing procedures that imple-
                    ment  default  behaviors  for  widgets.   The
                    value  of  this  variable  is  taken from the
                    TK_LIBRARY  environment  variable,   if   one
                    exists, or else from a default value compiled
                    into Tk.

     tk_patchLevel  Contains a decimal integer giving the current
                    patch  level  for  Tk.   The  patch  level is
                    incremented for each new  release  or  patch,
                    and  it  uniquely identifies an official ver-
                    sion of Tk.

     tkPriv         This variable is an array containing  several  |
                    pieces of information that are private to Tk.  |
                    The elements of tkPriv are used by Tk library
                    procedures and default bindings.  They should
                    not be accessed by any code outside Tk.

     tk_strictMotif This variable is set to zero by default.   If
                    an  application  sets  it  to  one,  then  Tk
                    attempts to adhere as closely as possible  to
                    Motif  look-and-feel standards.  For example,
                    active elements such as buttons and scrollbar
                    sliders   will  not  change  color  when  the
                    pointer passes over them.

     tk_version     Tk sets this variable in the interpreter  for
                    each  application.   The  variable  holds the
                    current version number of the Tk  library  in
                    the  form  _m_a_j_o_r._m_i_n_o_r.   _M_a_j_o_r and _m_i_n_o_r are
                    integers.  The major version number increases
                    in  any Tk release that includes changes that
                    are not backward  compatible  (i.e.  whenever
                    existing Tk applications and scripts may have
                    to change to work with the new release).  The



Tk                      Last change: 4.0                        1






tkvars(n)                  Tk Commands                  tkvars(n)



                    minor  version number increases with each new
                    release of Tk, except that it resets to  zero
                    whenever the major version number changes.


KEYWORDS
     variables, version
















































Tk                      Last change: 4.0                        2




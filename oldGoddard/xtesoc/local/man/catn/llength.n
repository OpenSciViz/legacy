


llength(n)            Tcl Built-In Commands            llength(n)



_________________________________________________________________

NAME
     llength - Count the number of elements in a list

SYNOPSIS
     llength _l_i_s_t
_________________________________________________________________


DESCRIPTION
     Treats _l_i_s_t as a list and returns a  decimal  string  giving
     the number of elements in it.


KEYWORDS
     element, list, length






































Tcl                      Last change:                           1




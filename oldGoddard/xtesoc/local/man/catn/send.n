


send(n)                    Tk Commands                    send(n)



_________________________________________________________________

NAME
     send - Execute a command in a different application

SYNOPSIS
     send ?_o_p_t_i_o_n_s? _a_p_p _c_m_d ?_a_r_g _a_r_g ...?                          |
_________________________________________________________________


DESCRIPTION
     This command arranges for _c_m_d (and _a_r_gs) to be  executed  in
     the  application  named  by  _a_p_p.   It returns the result or
     error from that command execution.  _A_p_p may be the  name  of
     any application whose main window is on the display contain-
     ing the sender's main window;  it need  not  be  within  the
     same  process.   If  no  _a_r_g arguments are present, then the
     command to be executed is contained entirely within the  _c_m_d
     argument.   If  one  or more _a_r_gs are present, they are con-
     catenated to form the command to be executed,  just  as  for
     the eval command.

     If the initial arguments of the command begin  with  `` - ''  |
     they  are  treated  as  options.   The following options are  |
     currently defined:                                            |

                                                                -                                                             |
          async                                                 |
          |                                                        |
          Requests asynchronous invocation.   In  this  case  the  |
          send  command will complete immediately without waiting  |
          for _c_m_d to complete in the target application.  If  the  |
          target  application is in the same process as the send-  |
          ing application then the -async option is ignored.       |

                                                                -                                                             |
          displayof _p_a_t_h_N_a_m_e                                    |
          |                                                        |
          Specifies that the target application's main window  is  |
          on the display of the window given by _p_a_t_h_N_a_m_e, instead  |
          of the display containing the application's  main  win-  |
          dow.                                                     |

                                  -                                                             |
          -                                                     |
          |                                                        |
          Serves no purpose  except  to  terminate  the  list  of  |
          options.   This option is needed only if _a_p_p could con-  |
          tain a leading ``-'' character.                          |






Tk                      Last change: 4.0                        1






send(n)                    Tk Commands                    send(n)



APPLICATION NAMES                                                  |
     The name of an application is set initially from the name of  |
     the program or script that created the application.  You can  |
     query and change the name of  an  application  with  the  tk  |
     appname command.                                              |


DISABLING SENDS                                                    |
     If the send command is removed  from  an  application  (e.g.  |
     with  the  command rename send {}) then the application will  |
     not respond to incoming send requests anymore,  nor will  it  |
     be  able  to  issue outgoing requests.  Communication can be  |
     reenabled by invoking the tk appname command.


SECURITY
     The send command is potentially a serious security loophole,
     since  any application that can connect to your X server can
     send scripts to your applications.  These  incoming  scripts
     can  use  Tcl  to  read and write your files and invoke sub-
     processes under your name.  Host-based access  control  such
     as that provided by xhost is particularly insecure, since it
     allows anyone with an account on particular hosts to connect
     to your server, and if disabled it allows anyone anywhere to
     connect to your server.  In order  to  provide  at  least  a
     small amount of security, Tk checks the access control being
     used by the server and rejects  incoming  sends  unless  (a)
     xhost-style  access  control  is  enabled (i.e. only certain
     hosts can establish connections) and (b) the list of enabled
     hosts is empty.  This means that applications cannot connect
     to your server unless they use some other form of authoriza-
     tion such as that provide by xauth.


KEYWORDS
     application, name, remote execution, security, send



















Tk                      Last change: 4.0                        2







lrange(n)             Tcl Built-In Commands             lrange(n)



_________________________________________________________________

NAME
     lrange - Return one or more adjacent elements from a list

SYNOPSIS
     lrange _l_i_s_t _f_i_r_s_t _l_a_s_t
_________________________________________________________________


DESCRIPTION
     _L_i_s_t must be a valid Tcl list.  This command will  return  a
     new   list   consisting  of  elements  _f_i_r_s_t  through  _l_a_s_t,
     inclusive.  _F_i_r_s_t or _l_a_s_t may be end (or any abbreviation of  |
     it)  to  refer to the last element of the list.  If _f_i_r_s_t is
     less than zero, it is treated as if it were zero.   If  _l_a_s_t
     is  greater  than  or equal to the number of elements in the
     list, then it is treated as if it were  end.   If  _f_i_r_s_t  is
     greater  than  _l_a_s_t then an empty string is returned.  Note:
     ``lrange _l_i_s_t _f_i_r_s_t _f_i_r_s_t'' does not always produce the same
     result  as ``lindex _l_i_s_t _f_i_r_s_t'' (although it often does for
     simple fields that aren't enclosed in braces); it does, how-
     ever,  produce  exactly  the  same results as ``list [lindex
     _l_i_s_t _f_i_r_s_t]''


KEYWORDS
     element, list, range, sublist



























Tcl                      Last change:                           1







library(n)            Tcl Built-In Commands            library(n)



_________________________________________________________________

NAME
     library - standard library of Tcl procedures

SYNOPSIS
     auto_execok _c_m_d
     auto_load _c_m_d
     auto_mkindex _d_i_r _p_a_t_t_e_r_n _p_a_t_t_e_r_n ...
     auto_reset
     parray _a_r_r_a_y_N_a_m_e
     unknown _c_m_d ?_a_r_g _a_r_g ...?
_________________________________________________________________


INTRODUCTION
     Tcl includes a library of Tcl procedures for commonly-needed
     functions.   The  procedures  defined in the Tcl library are
     generic ones suitable for use  by  many  different  applica-
     tions.   The  location of the Tcl library is returned by the
     info library command.  In addition to the Tcl library,  each
     application  will  normally  have its own library of support
     procedures as well;  the location of this  library  is  nor-
     mally  given  by  the value of the $_a_p_p_library global vari-
     able, where _a_p_p is the name of the application.   For  exam-
     ple,  the location of the Tk library is kept in the variable
     $tk_library.

     To access the procedures in the Tcl library, an  application
     should  source the file init.tcl in the library, for example
     with the Tcl command

          source [info library]/init.tcl

     This will define the unknown procedure and arrange  for  the
     other  procedures to be loaded on-demand using the auto-load
     mechanism defined below.


COMMAND PROCEDURES
     The following procedures are provided in the Tcl library:

     auto_execok _c_m_d
          Determines whether there is an executable file  by  the
          name _c_m_d.  This command examines the directories in the
          current search path  (given  by  the  PATH  enviornment
          variable)  to  see if there is an executable file named
          _c_m_d in any of those directories.  If so, it returns  1;
          if  not  it returns 0.  Auto_exec remembers information
          about previous searches in an array  named  auto_execs;
          this  avoids  the  path  search in future calls for the
          same _c_m_d.  The command auto_reset may be used to  force



Tcl                      Last change:                           1






library(n)            Tcl Built-In Commands            library(n)



          auto_execok to forget its cached information.

     auto_load _c_m_d
          This command attempts to load the definition for a  Tcl
          command  named  _c_m_d.   To do this, it searches an _a_u_t_o-
          _l_o_a_d _p_a_t_h, which is a list of one or more  directories.
          The  auto-load  path  is  given  by the global variable
          $auto_path if it exists.  If  there  is  no  $auto_path
          variable,  then  the TCLLIBPATH environment variable is
          used, if it exists.  Otherwise the auto-load path  con-
          sists  of  just the Tcl library directory.  Within each
          directory in the auto-load path there must  be  a  file
          tclIndex that describes one or more commands defined in  |
          that directory and a script to evaluate to load each of  |
          the  commands.   The  tclIndex file should be generated  |
          with the auto_mkindex command.  If _c_m_d is found  in  an  |
          index file, then the appropriate script is evaluated to  |
          create the command.  The auto_load command returns 1 if
          _c_m_d was successfully created.  The command returns 0 if
          there was no index entry  for  _c_m_d  or  if  the  script
          didn't actually define _c_m_d (e.g. because index informa-
          tion is out of date).  If an error  occurs  while  pro-
          cessing  the  script,  then  that  error  is  returned.
          Auto_load only reads the  index  information  once  and
          saves  it  in  the  array  auto_index;  future calls to
          auto_load check for _c_m_d in the array  rather  than  re-
          reading  the index files.  The cached index information
          may be deleted with the command auto_reset.  This  will
          force  the  next  auto_load command to reload the index
          database from disk.

     auto_mkindex _d_i_r _p_a_t_t_e_r_n _p_a_t_t_e_r_n ...
          Generates an index suitable for use by auto_load.   The  |
          command  searches  _d_i_r  for all files whose names match  |
          any of the _p_a_t_t_e_r_n arguments (matching is done with the
          glob  command),  generates an index of all the Tcl com-
          mand procedures defined in all the matching files,  and
          stores  the  index information in a file named tclIndex
          in _d_i_r.  For example, the command

               auto_mkindex foo *.tcl


          will read all the .tcl files in  subdirectory  foo  and
          generate a new index file foo/tclIndex.

          Auto_mkindex parses the Tcl  scripts  in  a  relatively
          unsophisticated  way:   if  any  line contains the word
          proc as its first characters then it is assumed to be a
          procedure  definition  and the next word of the line is
          taken as the procedure's name.   Procedure  definitions
          that  don't  appear  in this way (e.g. they have spaces



Tcl                      Last change:                           2






library(n)            Tcl Built-In Commands            library(n)



          before the proc) will not be indexed.

     auto_reset
          Destroys all the information cached by auto_execok  and
          auto_load.   This information will be re-read from disk
          the next time it is needed.   Auto_reset  also  deletes
          any  procedures  listed in the auto-load index, so that
          fresh copies of them will be loaded the next time  that
          they're used.

     parray _a_r_r_a_y_N_a_m_e
          Prints on standard output the names and values  of  all
          the elements in the array _a_r_r_a_y_N_a_m_e.  ArrayName must be
          an array accessible to the caller of parray.  It may be
          either local or global.

     unknown _c_m_d ?_a_r_g _a_r_g ...?
          This procedure is  invoked  automatically  by  the  Tcl
          interpreter  whenever  the  name  of  a command doesn't
          exist.  The unknown procedure receives as its arguments
          the name and arguments of the missing command.  Unknown  |
          first calls auto_load to load  the  command.   If  this
          succeeds,  then  it  executes the original command with
          its original arguments.  If the  auto-load  fails  then
          unknown calls auto_execok to see if there is an execut-
          able file by the name _c_m_d.  If so, it invokes  the  Tcl
          exec  command  with  _c_m_d and all the _a_r_g_s as arguments.
          If _c_m_d can't be auto-executed, unknown checks to see if
          the command was invoked at top-level and outside of any
          script.  If so, then unknown takes takes two additional
          steps.   First, it sees if _c_m_d has one of the following
          three forms:  !!, !_e_v_e_n_t, or ^_o_l_d^_n_e_w?^?.  If so,  then
          unknown  carries  out  history substitution in the same
          way that csh would for these constructs.   Second,  and
          last, unknown checks to see if _c_m_d is a unique abbrevi-
          ation for an existing Tcl command.  If so,  it  expands
          the command name and executes the command with the ori-
          ginal arguments.  If none of the above efforts has been
          able to execute the command, unknown generates an error
          return.  If the global variable auto_noload is defined,
          then  the  auto-load  step  is  skipped.  If the global
          variable auto_noexec is defined then the auto-exec step
          is  skipped.   Under  normal  circumstances  the return
          value from unknown is the return value from the command
          that was eventually executed.


VARIABLES
     The following global variables are defined or  used  by  the
     procedures in the Tcl library:

     auto_execs



Tcl                      Last change:                           3






library(n)            Tcl Built-In Commands            library(n)



          Used by auto_execok to record information about whether
          particular commands exist as executable files.

     auto_index
          Used by auto_load to save the  index  information  read
          from disk.

     auto_noexec
          If set to any value, then unknown will not  attempt  to
          auto-exec any commands.

     auto_noload
          If set to any value, then unknown will not  attempt  to
          auto-load any commands.

     auto_path
          If set, then it must contain a valid  Tcl  list  giving
          directories to search during auto-load operations.

     env(TCL_LIBRARY)
          If set, then it specifies the location of the directory
          containing  library scripts (the value of this variable
          will be returned by the command info library).  If this
          variable isn't set then a default value is used.

     env(TCLLIBPATH)
          If set, then it must contain a valid  Tcl  list  giving
          directories  to  search  during  auto-load  operations.
          This variable is only used if auto_path is not defined.

     unknown_active
          This variable is set by unknown to indicate that it  is
          active.   It  is  used  to  detect errors where unknown
          recurses on itself infinitely.  The variable  is  unset
          before unknown returns.


KEYWORDS
     auto-exec, auto-load, library, unknown
















Tcl                      Last change:                           4




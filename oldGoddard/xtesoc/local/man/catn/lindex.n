


lindex(n)             Tcl Built-In Commands             lindex(n)



_________________________________________________________________

NAME
     lindex - Retrieve an element from a list

SYNOPSIS
     lindex _l_i_s_t _i_n_d_e_x
_________________________________________________________________


DESCRIPTION
     This command treats _l_i_s_t as  a  Tcl  list  and  returns  the
     _i_n_d_e_x'th  element  from it (0 refers to the first element of
     the list).  In extracting the element, _l_i_n_d_e_x  observes  the
     same  rules  concerning braces and quotes and backslashes as
     the Tcl command interpreter; however, variable  substitution
     and command substitution do not occur.  If _i_n_d_e_x is negative
     or greater than or equal to the number of elements in _v_a_l_u_e,
     then  an  empty  string is returned.  If _i_n_d_e_x has the value  |
     end, it refers to the last element in the list.


KEYWORDS
     element, index, list































Tcl                      Last change:                           1







catch(n)              Tcl Built-In Commands              catch(n)



_________________________________________________________________

NAME
     catch - Evaluate script and trap exceptional returns

SYNOPSIS
     catch _s_c_r_i_p_t ?_v_a_r_N_a_m_e?
_________________________________________________________________


DESCRIPTION
     The catch command may be used to prevent errors from  abort-
     ing command interpretation.  Catch calls the Tcl interpreter
     recursively to execute _s_c_r_i_p_t, and always returns  a  TCL_OK
     code,  regardless  of any errors that might occur while exe-
     cuting _s_c_r_i_p_t.  The return value from  catch  is  a  decimal
     string giving the code returned by the Tcl interpreter after
     executing _s_c_r_i_p_t.  This will be 0 (TCL_OK) if there were  no
     errors  in  _s_c_r_i_p_t;  otherwise it will have a non-zero value
     corresponding to one of the exceptional  return  codes  (see
     tcl.h  for  the definitions of code values).  If the _v_a_r_N_a_m_e
     argument is given, then it gives the  name  of  a  variable;
     catch  will  set  the  variable  to the string returned from
     _s_c_r_i_p_t (either a result or an error message).

     Note that catch catches all exceptions, including those gen-
     erated by break and continue as well as errors.


KEYWORDS
     catch, error
























Tcl                      Last change:                           1




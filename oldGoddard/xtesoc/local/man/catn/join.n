


join(n)               Tcl Built-In Commands               join(n)



_________________________________________________________________

NAME
     join - Create a string by joining together list elements

SYNOPSIS
     join _l_i_s_t ?_j_o_i_n_S_t_r_i_n_g?
_________________________________________________________________


DESCRIPTION
     The _l_i_s_t argument must be a valid Tcl  list.   This  command
     returns  the string formed by joining all of the elements of
     _l_i_s_t together with _j_o_i_n_S_t_r_i_n_g separating each adjacent  pair
     of  elements.   The  _j_o_i_n_S_t_r_i_n_g argument defaults to a space
     character.


KEYWORDS
     element, join, list, separator



































Tcl                      Last change:                           1




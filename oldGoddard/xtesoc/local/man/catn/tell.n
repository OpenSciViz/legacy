


tell(n)               Tcl Built-In Commands               tell(n)



_________________________________________________________________

NAME
     tell - Return current access position for an open file

SYNOPSIS
     tell _f_i_l_e_I_d
_________________________________________________________________


DESCRIPTION
     Returns a decimal string giving the current access  position
     in  _f_i_l_e_I_d.   _F_i_l_e_I_d  must have been the return value from a
     previous call to open, or it may be stdin, stdout, or stderr
     to refer to one of the standard I/O channels.


KEYWORDS
     access position, file




































Tcl                      Last change:                           1







listbox(n)                 Tk Commands                 listbox(n)



_________________________________________________________________

NAME
     listbox - Create and manipulate listbox widgets

SYNOPSIS
     listbox _p_a_t_h_N_a_m_e ?_o_p_t_i_o_n_s?

STANDARD OPTIONS
     background      font           selectBackgroundxScrollCommand
     borderWidth     height         selectBorderWidthyScrollCommand
     cursor          highlightColor selectForeground               |
     exportSelection highlightThickness             setGrid
     foreground      relief         width

     See the ``options'' manual entry for details on the standard
     options.

WIDGET-SPECIFIC OPTIONS
     Name:           height
     Class:          Height
     Command-Line Switch:           -height

          Specifies the desired height for the window, in  lines.  |
          If zero or less, then the desired height for the window  |
          is made just large enough to hold all the  elements  in  |
          the listbox.                                             |

     Name:           selectMode                                    |
     Class:          SelectMode                                    |
     Command-Line Switch:           -selectmode                    |

                                                                |
          |                                                        |
          Specifies one of several styles  for  manipulating  the  |
          selection.   The  value of the option may be arbitrary,  |
          but the default bindings expect it to be either single,  |
          browse,  multiple,  or  extended;  the default value is  |
          browse.

     Name:           width
     Class:          Width
     Command-Line Switch:           -width

          Specifies the desired width for the window  in  charac-
          ters.   If  the  font doesn't have a uniform width then
          the width of the character ``0'' is used in translating
          from character units to screen units.  If zero or less,  |
          then the desired width for  the  window  is  made  just  |
          large enough to hold all the elements in the listbox.
_________________________________________________________________




Tk                       Last change:                           1






listbox(n)                 Tk Commands                 listbox(n)



DESCRIPTION
     The listbox command creates a new window (given by the _p_a_t_h_-
     _N_a_m_e  argument)  and  makes it into a listbox widget.  Addi-
     tional options, described above, may  be  specified  on  the
     command  line or in the option database to configure aspects
     of the listbox such as its colors, font, text,  and  relief.
     The  listbox  command returns its _p_a_t_h_N_a_m_e argument.  At the
     time this command is invoked, there must not exist a  window
     named _p_a_t_h_N_a_m_e, but _p_a_t_h_N_a_m_e's parent must exist.

     A listbox is a widget that displays a list of  strings,  one
     per  line.   When  first  created, a new listbox has no ele-
     ments.  Elements may be added or deleted using  widget  com-
     mands  described  below.   In addition, one or more elements
     may be selected as described below.  If a listbox is export-
     ing its selection (see exportSelection option), then it will
     observe the standard X11 protocols for handling  the  selec-
     tion.   Listbox selections are available as type STRING; the  |
     value of the selection will be the text of the selected ele-  |
     ments, with newlines separating the elements.                 |

     It is not necessary for all the elements to be displayed  in  |
     the listbox window at once;  commands described below may be  |
     used to change the view  in  the  window.   Listboxes  allow  |
     scrolling  in both directions using the standard xScrollCom-  |
     mand and yScrollCommand options.  They  also  support  scan-  |
     ning, as described below.                                     |


INDICES                                                            |
     Many of the widget commands for listboxes take one  or  more  |
     indices  as arguments.  An index specifies a particular ele-  |
     ment of the listbox, in any of the following ways:            |

     _n_u_m_b_e_r                                                     |
                 |                                                 |
                 Specifies the  element  as  a  numerical  index,  |
                 where  0 corresponds to the first element in the  |
                 listbox.                                          |

     active                                                     |
                 |                                                 |
                 Indicates the element that has the location cur-  |
                 sor.   This  element  will  be displayed with an  |
                 underline when  the  listbox  has  the  keyboard  |
                 focus,  and  it  is  specified with the activate  |
                 widget command.                                   |

     anchor                                                     |
                 |                                                 |
                 Indicates the anchor point  for  the  selection,  |
                 which  is  set  with the selection anchor widget  |



Tk                       Last change:                           2






listbox(n)                 Tk Commands                 listbox(n)



                 command.

     end         Indicates the end of the listbox.  For some com-
                 mands  this  means  just after the last element;
                 for other commands it means the last element.

     @_x,_y        Indicates the element that covers the  point  in  |
                 the  listbox  window  specified  by  _x and _y (in  |
                 pixel coordinates).  If no element  covers  that  |
                 point, then the closest element to that point is  |
                 used.                                             |

     In the widget command descriptions  below,  arguments  named  |
     _i_n_d_e_x, _f_i_r_s_t, and _l_a_s_t always contain text indices in one of  |
     the above forms.


WIDGET COMMAND
     The listbox command creates a new Tcl command whose name  is
     _p_a_t_h_N_a_m_e.  This command may be used to invoke various opera-
     tions on the widget.  It has the following general form:

          _p_a_t_h_N_a_m_e _o_p_t_i_o_n ?_a_r_g _a_r_g ...?

     _O_p_t_i_o_n and the _a_r_gs determine the exact behavior of the com-
     mand.  The following commands are possible for listbox widg-
     ets:

     _p_a_t_h_N_a_m_e activate _i_n_d_e_x
          Sets the active element to the one indicated by  _i_n_d_e_x.  |
          The  active element is drawn with an underline when the  |
          widget has the  input  focus,  and  its  index  may  be  |
          retrieved with the index active.                         |

     _p_a_t_h_N_a_m_e bbox _i_n_d_e_x                                        |
          |                                                        |
          Returns a list of four numbers describing the  bounding  |
          box  of  the  text  in the element given by _i_n_d_e_x.  The  |
          first two elements of the list give the x and y coordi-  |
          nates  of  the  upper-left  corner  of  the screen area  |
          covered by the text (specified in  pixels  relative  to  |
          the  widget)  and  the last two elements give the width  |
          and height of the area, in pixels.                       |

     _p_a_t_h_N_a_m_e cget _o_p_t_i_o_n                                       |
          |                                                        |
          Returns the current value of the  configuration  option  |
          given  by  _o_p_t_i_o_n.   _O_p_t_i_o_n  may have any of the values  |
          accepted by the listbox command.

     _p_a_t_h_N_a_m_e configure ?_o_p_t_i_o_n? ?_v_a_l_u_e _o_p_t_i_o_n _v_a_l_u_e ...?
          Query  or  modify  the  configuration  options  of  the



Tk                       Last change:                           3






listbox(n)                 Tk Commands                 listbox(n)



          widget.   If  no  _o_p_t_i_o_n  is  specified, returns a list
          describing all of the available  options  for  _p_a_t_h_N_a_m_e
          (see  Tk_ConfigureInfo for information on the format of
          this list).  If _o_p_t_i_o_n is specified with no _v_a_l_u_e, then
          the  command  returns  a  list describing the one named
          option (this list will be identical to the  correspond-
          ing  sublist  of  the  value  returned  if no _o_p_t_i_o_n is
          specified).  If one or more  _o_p_t_i_o_n - _v_a_l_u_e  pairs  are
          specified,  then  the command modifies the given widget
          option(s) to have the given value(s);  in this case the
          command  returns  an empty string.  _O_p_t_i_o_n may have any
          of the values accepted by the listbox command.

     _p_a_t_h_N_a_m_e curselection
          Returns a list containing the numerical indices of  all
          of  the  elements  in  the  listbox  that are currently
          selected.  If there are no  elements  selected  in  the
          listbox then an empty string is returned.

     _p_a_t_h_N_a_m_e delete _f_i_r_s_t ?_l_a_s_t?
          Deletes one or more elements of the listbox.  _F_i_r_s_t and
          _l_a_s_t are indices specifying the first and last elements
          in the range to delete.  If  _l_a_s_t  isn't  specified  it
          defaults to _f_i_r_s_t, i.e. a single element is deleted.

     _p_a_t_h_N_a_m_e get _f_i_r_s_t ?_l_a_s_t?
          If _l_a_s_t is omitted, returns the contents of the listbox
          element  indicated by _f_i_r_s_t.  If _l_a_s_t is specified, the  |
          command returns a list whose elements are  all  of  the  |
          listbox  elements  between  _f_i_r_s_t  and _l_a_s_t, inclusive.  |
          Both _f_i_r_s_t and _l_a_s_t may have any of the standard  forms  |
          for indices.                                             |

     _p_a_t_h_N_a_m_e index _i_n_d_e_x                                       |
          |                                                        |
          Returns a decimal string giving the integer index value  |
          that corresponds to _i_n_d_e_x.                               |

     _p_a_t_h_N_a_m_e insert _i_n_d_e_x ?_e_l_e_m_e_n_t _e_l_e_m_e_n_t ...?                |
          |                                                        |
          Inserts zero or more new  elements  in  the  list  just  |
          before  the element given by _i_n_d_e_x.  If _i_n_d_e_x is speci-  |
          fied as end then the new elements are added to the  end  |
          of the list.  Returns an empty string.                   |

     _p_a_t_h_N_a_m_e nearest _y                                         |
          |                                                        |
          Given a y-coordinate within the  listbox  window,  this  |
          command returns the index of the (visible) listbox ele-  |
          ment nearest to that y-coordinate.                       |

     _p_a_t_h_N_a_m_e scan _o_p_t_i_o_n _a_r_g_s                                  |



Tk                       Last change:                           4






listbox(n)                 Tk Commands                 listbox(n)



          | This  command  is used to implement scanning on list-  |
          boxes.  It has two forms, depending on _o_p_t_i_o_n:           |

          _p_a_t_h_N_a_m_e scan mark _x _y                                |
               |                                                   |
               Records _x and _y and the current view in the  list-  |
               box  window;   used in conjunction with later scan  |
               dragto commands.  Typically this command is  asso-  |
               ciated  with  a  mouse button press in the widget.  |
               It returns an empty string.                         |

          _p_a_t_h_N_a_m_e scan dragto _x _y.                             |
               |                                                   |
               This command computes the difference between its _x  |
               and  _y  arguments and the _x and _y arguments to the  |
               last scan mark command for the  widget.   It  then  |
               adjusts  the  view  by  10 times the difference in  |
               coordinates.  This command is typically associated  |
               with mouse motion events in the widget, to produce  |
               the effect of dragging  the  list  at  high  speed  |
               through  the window.  The return value is an empty  |
               string.                                             |

     _p_a_t_h_N_a_m_e see _i_n_d_e_x                                         |
          |                                                        |
          Adjust the view in the  listbox  so  that  the  element  |
          given  by  _i_n_d_e_x is visible.  If the element is already  |
          visible then the command has no effect; if the  element  |
          is near one edge of the window then the listbox scrolls  |
          to bring the element into view at the edge;   otherwise  |
          the listbox scrolls to center the element.               |

     _p_a_t_h_N_a_m_e selection _o_p_t_i_o_n _a_r_g                              |
          |                                                        |
          This command is used to adjust the selection  within  a  |
          listbox.  It has several forms, depending on _o_p_t_i_o_n:     |

          _p_a_t_h_N_a_m_e selection anchor _i_n_d_e_x                       |
               |                                                   |
               Sets the selection anchor to the element given  by  |
               _i_n_d_e_x.   The  selection  anchor  is the end of the  |
               selection that  is  fixed  while  dragging  out  a  |
               selection with the mouse.  The index anchor may be  |
               used to refer to the anchor element.                |

          _p_a_t_h_N_a_m_e selection clear _f_i_r_s_t ?_l_a_s_t?                 |
               |                                                   |
               If any of the  elements  between  _f_i_r_s_t  and  _l_a_s_t  |
               (inclusive)  are  selected,  they  are deselected.  |
               The selection state is not  changed  for  elements  |
               outside this range.                                 |




Tk                       Last change:                           5






listbox(n)                 Tk Commands                 listbox(n)



          _p_a_t_h_N_a_m_e selection includes _i_n_d_e_x                     |
               |                                                   |
               Returns 1 if the element  indicated  by  _i_n_d_e_x  is  |
               currently selected, 0 if it isn't.                  |

          _p_a_t_h_N_a_m_e selection set _f_i_r_s_t ?_l_a_s_t?                   |
               |                                                   |
               Selects all of the elements in the  range  between  |
               _f_i_r_s_t  and  _l_a_s_t, inclusive, without affecting the  |
               selection state of elements outside that range.     |

     _p_a_t_h_N_a_m_e size                                              |
          |                                                        |
          Returns a decimal string indicating the total number of  |
          elements in the listbox.                                 |

     _p_a_t_h_N_a_m_e xview _a_r_g_s                                        |
          |                                                        |
          This command is used to query and change the horizontal  |
          position of the information in the widget's window.  It  |
          can take any of the following forms:                     |

          _p_a_t_h_N_a_m_e xview                                        |
               |                                                   |
               Returns a list containing two elements.  Each ele-  |
               ment is a real fraction between 0 and 1;  together  |
               they describe the horizontal span that is  visible  |
               in  the window.  For example, if the first element  |
               is .2 and the second element is  .6,  20%  of  the  |
               listbox's text is off-screen to the left, the mid-  |
               dle 40% is visible in the window, and 40%  of  the  |
               text  is  off-screen  to the right.  These are the  |
               same  values  passed  to  scrollbars  via  the   -   |
               xscrollcommand option.                              |

          _p_a_t_h_N_a_m_e xview _i_n_d_e_x                                  |
               |                                                   |
               Adjusts the view in the window so that the charac-  |
               ter  position  given  by _i_n_d_e_x is displayed at the  |
               left edge of the window.  Character positions  are  |
               defined by the width of the character 0.            |

          _p_a_t_h_N_a_m_e xview moveto _f_r_a_c_t_i_o_n                        |
               |                                                   |
               Adjusts the view in the window so that _f_r_a_c_t_i_o_n of  |
               the  total width of the listbox text is off-screen  |
               to the left.  _f_r_a_c_t_i_o_n must be a fraction  between  |
               0 and 1.                                            |

          _p_a_t_h_N_a_m_e xview scroll _n_u_m_b_e_r _w_h_a_t                     |
               |                                                   |
               This command shifts the view in the window left or  |



Tk                       Last change:                           6






listbox(n)                 Tk Commands                 listbox(n)



               right  according  to _n_u_m_b_e_r and _w_h_a_t.  _N_u_m_b_e_r must  |
               be an integer.  _W_h_a_t must be either units or pages  |
               or  an  abbreviation  of one of these.  If _w_h_a_t is  |
               units, the view adjusts left or  right  by  _n_u_m_b_e_r  |
               character  units (the width of the 0 character) on  |
               the display;  if it is pages then the view adjusts  |
               by  _n_u_m_b_e_r screenfuls.  If _n_u_m_b_e_r is negative then  |
               characters farther to the left become visible;  if  |
               it  is  positive  then  characters  farther to the  |
               right become visible.                               |

     _p_a_t_h_N_a_m_e yview ?_a_r_g_s?                                      |
          |                                                        |
          This command is used to query and change  the  vertical  |
          position  of  the  text in the widget's window.  It can  |
          take any of the following forms:                         |

          _p_a_t_h_N_a_m_e yview                                        |
               |                                                   |
               Returns a list containing two  elements,  both  of  |
               which  are  real  fractions  between 0 and 1.  The  |
               first element gives the position  of  the  listbox  |
               element  at the top of the window, relative to the  |
               listbox as  a  whole  (0.5  means  it  is  halfway  |
               through  the  listbox,  for  example).  The second  |
               element gives the position of the listbox  element  |
               just after the last one in the window, relative to  |
               the listbox as a whole.  These are the same values  |
               passed  to  scrollbars  via  the  - yscrollcommand  |
               option.                                             |

          _p_a_t_h_N_a_m_e yview _i_n_d_e_x                                  |
               |                                                   |
               Adjusts the view in the window so that the element  |
               given by _i_n_d_e_x is displayed at the top of the win-  |
               dow.                                                |

          _p_a_t_h_N_a_m_e yview moveto _f_r_a_c_t_i_o_n                        |
               |                                                   |
               Adjusts the view in the window so that the element  |
               given  by  _f_r_a_c_t_i_o_n appears at the top of the win-  |
               dow.  _F_r_a_c_t_i_o_n is a fraction between 0 and  1;   0  |
               indicates  the  first element in the listbox, 0.33  |
               indicates the element one-third  the  way  through  |
               the listbox, and so on.                             |

          _p_a_t_h_N_a_m_e yview scroll _n_u_m_b_e_r _w_h_a_t                     |
               |                                                   |
               This command adjust the view in the window  up  or  |
               down according to _n_u_m_b_e_r and _w_h_a_t.  _N_u_m_b_e_r must be  |
               an integer.  _W_h_a_t must be either units  or  pages.  |
               If  _w_h_a_t  is units, the view adjusts up or down by  |



Tk                       Last change:                           7






listbox(n)                 Tk Commands                 listbox(n)



               _n_u_m_b_e_r lines;   if  it  is  pages  then  the  view  |
               adjusts  by _n_u_m_b_e_r screenfuls.  If _n_u_m_b_e_r is nega-  |
               tive then earlier elements become visible;  if  it  |
               is positive then later elements become visible.     |


DEFAULT BINDINGS                                                   |
     Tk automatically creates class bindings for  listboxes  that  |
     give  them  Motif-like  behavior.  Much of the behavior of a  |
     listbox  is  determined  by  its  selectMode  option,  which  |
     selects one of four ways of dealing with the selection.       |

     If the selection mode is single or browse, at most one  ele-  |
     ment can be selected in the listbox at once.  In both modes,  |
     clicking button 1 on an element selects it and deselects any  |
     other  selected item.  In browse mode it is also possible to  |
     drag the selection with button 1.                             |

     If the selection mode is multiple or extended, any number of  |
     elements  may  be  selected at once, including discontiguous  |
     ranges.  In multiple mode, clicking button 1 on  an  element  |
     toggles its selection state without affecting any other ele-  |
     ments.  In extended mode, pressing button 1  on  an  element  |
     selects  it,  deselects everything else, and sets the anchor  |
     to the element under the mouse;   dragging  the  mouse  with  |
     button  1 down extends the selection to include all the ele-  |
     ments between the anchor and the element  under  the  mouse,  |
     inclusive.                                                    |

     Most people will probably want to use browse mode for single  |
     selections  and  extended  mode for multiple selections; the  |
     other modes appear to be useful only in special situations.   |

     In addition to the above behavior, the following  additional  |
     behavior is defined by the default bindings:                  |

     [1]                                                        |
          |                                                        |
          In extended mode, the selected range can be adjusted by  |
          pressing  button 1 with the Shift key down:  this modi-  |
          fies the selection to consist of the  elements  between  |
          the  anchor and the element under the mouse, inclusive.  |
          The un-anchored end of this new selection can  also  be  |
          dragged with the button down.                            |

     [2]                                                        |
          |                                                        |
          In extended mode, pressing button 1  with  the  Control  |
          key  down  starts a toggle operation: the anchor is set  |
          to the element under the mouse, and its selection state  |
          is  reversed.   The  selection  state of other elements  |
          isn't changed.  If the mouse is dragged with  button  1  |



Tk                       Last change:                           8






listbox(n)                 Tk Commands                 listbox(n)



          down,  then the selection state of all elements between  |
          the anchor and the element under the mouse  is  set  to  |
          match  that of the anchor element;  the selection state  |
          of all other elements remains what it  was  before  the  |
          toggle operation began.                                  |

     [3]                                                        |
          |                                                        |
          If the mouse leaves the listbox window  with  button  1  |
          down,  the  window  scrolls away from the mouse, making  |
          information visible that used to be off-screen  on  the  |
          side  of  the mouse.  The scrolling continues until the  |
          mouse re-enters the window, the button is released,  or  |
          the end of the listbox is reached.                       |

     [4]                                                        |
          |                                                        |
          Mouse button 2 may be used  for  scanning.   If  it  is  |
          pressed  and  dragged over the listbox, the contents of  |
          the listbox drag at high speed  in  the  direction  the  |
          mouse moves.                                             |

     [5]                                                        |
          |                                                        |
          If the Up or Down key is pressed, the  location  cursor  |
          (active  element) moves up or down one element.  If the  |
          selection mode is  browse  or  extended  then  the  new  |
          active  element is also selected and all other elements  |
          are deselected.  In extended mode the new  active  ele-  |
          ment becomes the selection anchor.                       |

     [6]                                                        |
          |                                                        |
          In extended mode,  Shift-Up  and  Shift-Down  move  the  |
          location cursor (active element) up or down one element  |
          and also extend the selection  to  that  element  in  a  |
          fashion similar to dragging with mouse button 1.         |

     [7]                                                        |
          |                                                        |
          The Left and Right keys scroll the  listbox  view  left  |
          and  right  by  the width of the character 0.  Control-  |
          Left and Control-Right scroll the listbox view left and  |
          right  by  the  width of the window.  Control-Prior and  |
          Control-Next also scroll left and right by the width of  |
          the window.                                              |

     [8]                                                        |
          |                                                        |
          The Prior and Next keys scroll the listbox view up  and  |
          down by one page (the height of the window).             |




Tk                       Last change:                           9






listbox(n)                 Tk Commands                 listbox(n)



     [9]                                                        |
          |                                                        |
          The Home and End keys scroll the  listbox  horizontally  |
          to the left and right edges, respectively.               |

     [10]                                                       |
          |                                                        |
          Control-Home sets the location cursor to the the  first  |
          element  in  the  listbox,  selects  that  element, and  |
          deselects everything else in the listbox.                |

     [11]                                                       |
          |                                                        |
          Control-End sets the location cursor to  the  the  last  |
          element  in  the  listbox,  selects  that  element, and  |
          deselects everything else in the listbox.                |

     [12]                                                       |
          |                                                        |
          In extended mode, Control-Shift-Home extends the selec-  |
          tion  to  the first element in the listbox and Control-  |
          Shift-End extends the selection to the last element.     |

     [13]                                                       |
          |                                                        |
          In multiple mode, Control-Shift-Home moves the location  |
          cursor   to  the  first  element  in  the  listbox  and  |
          Control-Shift-End moves the location cursor to the last  |
          element.                                                 |

     [14]                                                       |
          |                                                        |
          The space and Select keys make a selection at the loca-  |
          tion  cursor (active element) just as if mouse button 1  |
          had been pressed over this element.                      |

     [15]                                                       |
          |                                                        |
          In extended mode, Control-Shift-space and  Shift-Select  |
          extend  the  selection to the active element just as if  |
          button 1 had been pressed with the Shift key down.       |

     [16]                                                       |
          |                                                        |
          In extended mode,  the  Escape  key  cancels  the  most  |
          recent  selection  and restores all the elements in the  |
          selected range to their previous selection state.        |

     [17]                                                       |
          |                                                        |
          Control-slash selects everything in the widget,  except  |
          in  single  and  browse modes, in which case it selects  |



Tk                       Last change:                          10






listbox(n)                 Tk Commands                 listbox(n)



          the active element and deselects everything else.        |

     [18]                                                       |
          |                                                        |
          Control-backslash deselects everything in  the  widget,  |
          except in browse mode where it has no effect.            |

     [19]                                                       |
          |                                                        |
          The F16 key (labelled Copy on many Sun workstations) or  |
          Meta-w  copies the selection in the widget to the clip-  |
          board, if there is a selection.                          |


     The behavior of listboxes can be  changed  by  defining  new
     bindings  for  individual widgets or by redefining the class
     bindings.


KEYWORDS
     listbox, widget


































Tk                       Last change:                          11




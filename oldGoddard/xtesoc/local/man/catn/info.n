


info(n)               Tcl Built-In Commands               info(n)



_________________________________________________________________

NAME
     info - Return information about the state of the Tcl  inter-
     preter

SYNOPSIS
     info _o_p_t_i_o_n ?_a_r_g _a_r_g ...?
_________________________________________________________________


DESCRIPTION
     This command provides information about various internals of
     the  Tcl  interpreter.   The  legal  _o_p_t_i_o_n's  (which may be
     abbreviated) are:

     info args _p_r_o_c_n_a_m_e
          Returns a list containing the names of the arguments to
          procedure  _p_r_o_c_n_a_m_e,  in  order.   _P_r_o_c_n_a_m_e must be the
          name of a Tcl command procedure.

     info body _p_r_o_c_n_a_m_e
          Returns the body of procedure _p_r_o_c_n_a_m_e.  _P_r_o_c_n_a_m_e  must
          be the name of a Tcl command procedure.

     info cmdcount
          Returns a count of the total number  of  commands  that
          have been invoked in this interpreter.

     info commands ?_p_a_t_t_e_r_n?
          If _p_a_t_t_e_r_n isn't specified, returns a list of names  of
          all  the Tcl commands, including both the built-in com-
          mands written in C and the command  procedures  defined
          using  the proc command.  If _p_a_t_t_e_r_n is specified, only
          those names matching _p_a_t_t_e_r_n are returned.  Matching is
          determined using the same rules as for string match.

     info complete _c_o_m_m_a_n_d
          Returns 1 if _c_o_m_m_a_n_d is a complete Tcl command  in  the
          sense of having no unclosed quotes, braces, brackets or
          array element names, If the command doesn't  appear  to
          be  complete then 0 is returned.  This command is typi-
          cally used in line-oriented input environments to allow
          users to type in commands that span multiple lines;  if
          the  command  isn't  complete,  the  script  can  delay
          evaluating it until additional lines have been typed to
          complete the command.

     info default _p_r_o_c_n_a_m_e _a_r_g _v_a_r_n_a_m_e
          _P_r_o_c_n_a_m_e must be the name of a  Tcl  command  procedure
          and  _a_r_g  must  be the name of an argument to that pro-
          cedure.  If _a_r_g doesn't have a default value  then  the



Tcl                     Last change: 7.0                        1






info(n)               Tcl Built-In Commands               info(n)



          command  returns  0.  Otherwise it returns 1 and places
          the default value of _a_r_g into variable _v_a_r_n_a_m_e.

     info exists _v_a_r_N_a_m_e
          Returns 1 if the variable named _v_a_r_N_a_m_e exists  in  the
          current context (either as a global or local variable),
          returns 0 otherwise.

     info globals ?_p_a_t_t_e_r_n?
          If _p_a_t_t_e_r_n isn't specified, returns a list of  all  the
          names  of  currently-defined global variables.  If _p_a_t_-
          _t_e_r_n is specified, only those  names  matching  _p_a_t_t_e_r_n
          are  returned.   Matching  is determined using the same
          rules as for string match.

     info level ?_n_u_m_b_e_r?
          If _n_u_m_b_e_r is not  specified,  this  command  returns  a
          number  giving  the  stack  level  of the invoking pro-
          cedure, or 0 if the command is  invoked  at  top-level.
          If  _n_u_m_b_e_r is specified, then the result is a list con-
          sisting of the name and  arguments  for  the  procedure
          call  at level _n_u_m_b_e_r on the stack.  If _n_u_m_b_e_r is posi-
          tive then it selects a particular stack level (1 refers
          to the top-most active procedure, 2 to the procedure it
          called, and so on); otherwise it gives a level relative
          to  the  current  level  (0  refers to the current pro-
          cedure, -1 to its caller, and so on).  See the  uplevel
          command for more information on what stack levels mean.

     info library
          Returns the name of  the  library  directory  in  which
          standard Tcl scripts are stored.  The default value for
          the library is compiled into Tcl, but it may  be  over-
          ridden by setting the TCL_LIBRARY environment variable.
          If there is no TCL_LIBRARY variable and no  compiled-in
          value  then  and  error  is generated.  See the library
          manual entry for details of the facilities provided  by
          the Tcl script library.  Normally each application will
          have its own  application-specific  script  library  in
          addition  to  the  Tcl  script library;  I suggest that
          each application set a global variable with a name like
          $_a_p_p_library  (where  _a_p_p is the application's name) to
          hold the location of that application's library  direc-
          tory.

     info locals ?_p_a_t_t_e_r_n?
          If _p_a_t_t_e_r_n isn't specified, returns a list of  all  the
          names  of  currently-defined local variables, including
          arguments to the current procedure, if any.   Variables
          defined  with the global and upvar commands will not be
          returned.  If _p_a_t_t_e_r_n is specified,  only  those  names
          matching  _p_a_t_t_e_r_n are returned.  Matching is determined



Tcl                     Last change: 7.0                        2






info(n)               Tcl Built-In Commands               info(n)



          using the same rules as for string match.

     info patchlevel
          Returns a decimal  integer  giving  the  current  patch  |
          level for Tcl.  The patch level is incremented for each  |
          new release or patch, and  it  uniquely  identifies  an  |
          official version of Tcl.

     info procs ?_p_a_t_t_e_r_n?
          If _p_a_t_t_e_r_n isn't specified, returns a list of  all  the
          names  of Tcl command procedures.  If _p_a_t_t_e_r_n is speci-
          fied, only those names matching _p_a_t_t_e_r_n  are  returned.
          Matching  is  determined  using  the  same rules as for
          string match.

     info script
          If a Tcl script file is currently being evaluated (i.e.
          there  is  a call to Tcl_EvalFile active or there is an
          active invocation of the  source  command),  then  this
          command  returns  the  name of the innermost file being
          processed.  Otherwise  the  command  returns  an  empty
          string.

     info tclversion
          Returns the version number for this version of  Tcl  in
          the  form  _x._y,  where  changes  to  _x  represent major
          changes with probable incompatibilities and changes  to
          _y  represent  small  enhancements  and  bug  fixes that
          retain backward compatibility.

     info vars ?_p_a_t_t_e_r_n?
          If _p_a_t_t_e_r_n isn't specified, returns a list of  all  the
          names  of  currently-visible  variables, including both
          locals and currently-visible globals.   If  _p_a_t_t_e_r_n  is
          specified,   only  those  names  matching  _p_a_t_t_e_r_n  are
          returned.  Matching is determined using the same  rules
          as for string match.


KEYWORDS
     command, information, interpreter, level,  procedure,  vari-
     able













Tcl                     Last change: 7.0                        3




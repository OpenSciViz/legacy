


frame(n)                   Tk Commands                   frame(n)



_________________________________________________________________

NAME
     frame - Create and manipulate frame widgets

SYNOPSIS
     frame _p_a_t_h_N_a_m_e ?_o_p_t_i_o_n_s?

STANDARD OPTIONS
     borderWidth     highlightColor relief                         |
     cursor          highlightThickness

     See the ``options'' manual entry for details on the standard
     options.

WIDGET-SPECIFIC OPTIONS
     Name:           background                                    |
     Class:          Background                                    |
     Command-Line Switch:           -background                    |

                                                                |
          |                                                        |
          This option is the  same  as  the  standard  background  |
          option  except  that its value may also be specified as  |
          an empty string.  In this case, the widget will display  |
          no background or border, and no colors will be consumed  |
          from its colormap for its background and border.         |

     Name:           class                                         |
     Class:          Class                                         |
     Command-Line Switch:           -class                         |

                                                                |
          |                                                        |
          Specifies a class for the window.  This class  will  be  |
          used when querying the option database for the window's  |
          other options, and it will also be used later for other  |
          purposes such as bindings.  The class option may not be  |
          changed with the configure widget command.               |

     Name:           colormap                                      |
     Class:          Colormap                                      |
     Command-Line Switch:           -colormap                      |

                                                                |
          |                                                        |
          Specifies a colormap to use for the window.  The  value  |
          may  be  either  new,  in  which case a new colormap is  |
          created for the window and its children, or the name of  |
          another  window  (which  must be on the same screen and  |
          have the same visual as _p_a_t_h_N_a_m_e), in  which  case  the  |
          new  window  will  use  the colormap from the specified  |



Tk                      Last change: 4.0                        1






frame(n)                   Tk Commands                   frame(n)



          window.  If the colormap option is not  specified,  the  |
          new  window uses the same colormap as its parent.  This  |
          option may not be changed  with  the  configure  widget  |
          command.

     Name:           height
     Class:          Height
     Command-Line Switch:           -height

          Specifies the desired height for the window in  any  of
          the  forms  acceptable to Tk_GetPixels.  If this option
          is less than or equal to zero then the window will  not
          request any size at all.

     Name:           visual                                        |
     Class:          Visual                                        |
     Command-Line Switch:           -visual                        |

                                                                |
          |                                                        |
          Specifies visual information for the new window in  any  |
          of  the forms accepted by Tk_GetVisual.  If this option  |
          is not specified, the new  window  will  use  the  same  |
          visual  as  its  parent.   The visual option may not be  |
          modified with the configure widget command.

     Name:           width
     Class:          Width
     Command-Line Switch:           -width

          Specifies the desired width for the window  in  any  of
          the  forms  acceptable to Tk_GetPixels.  If this option
          is less than or equal to zero then the window will  not
          request any size at all.
_________________________________________________________________


DESCRIPTION
     The frame command creates a new window (given by  the  _p_a_t_h_-
     _N_a_m_e argument) and makes it into a frame widget.  Additional
     options, described above, may be specified  on  the  command
     line  or  in the option database to configure aspects of the
     frame such as its background color and  relief.   The  frame
     command returns the path name of the new window.

     A frame is a simple widget.  Its primary purpose is  to  act
     as  a  spacer  or container for complex window layouts.  The
     only features of a frame are its  background  color  and  an
     optional  3-D  border  to  make  the  frame appear raised or
     sunken.





Tk                      Last change: 4.0                        2






frame(n)                   Tk Commands                   frame(n)



WIDGET COMMAND
     The frame command creates a new Tcl command  whose  name  is
     the  same as the path name of the frame's window.  This com-
     mand may be used to invoke various operations on the widget.
     It has the following general form:

          _p_a_t_h_N_a_m_e _o_p_t_i_o_n ?_a_r_g _a_r_g ...?

     _P_a_t_h_N_a_m_e is the name of the command, which is  the  same  as
     the frame widget's path name.  _O_p_t_i_o_n and the _a_r_gs determine
     the exact behavior of the command.  The  following  commands
     are possible for frame widgets:

     _p_a_t_h_N_a_m_e cget _o_p_t_i_o_n
          Returns the current value of the  configuration  option  |
          given  by  _o_p_t_i_o_n.   _O_p_t_i_o_n  may have any of the values  |
          accepted by the frame command.

     _p_a_t_h_N_a_m_e configure ?_o_p_t_i_o_n? ?_v_a_l_u_e _o_p_t_i_o_n _v_a_l_u_e ...?
          Query  or  modify  the  configuration  options  of  the
          widget.   If  no  _o_p_t_i_o_n  is  specified, returns a list
          describing all of the available  options  for  _p_a_t_h_N_a_m_e
          (see  Tk_ConfigureInfo for information on the format of
          this list).  If _o_p_t_i_o_n is specified with no _v_a_l_u_e, then
          the  command  returns  a  list describing the one named
          option (this list will be identical to the  correspond-
          ing  sublist  of  the  value  returned  if no _o_p_t_i_o_n is
          specified).  If one or more  _o_p_t_i_o_n - _v_a_l_u_e  pairs  are
          specified,  then  the command modifies the given widget
          option(s) to have the given value(s);  in this case the
          command  returns  an empty string.  _O_p_t_i_o_n may have any
          of the values accepted by the frame command.


BINDINGS
     When a new frame is created, it has no default  event  bind-
     ings:  frames are not intended to be interactive.


KEYWORDS
     frame, widget














Tk                      Last change: 4.0                        3







menubutton(n)              Tk Commands              menubutton(n)



_________________________________________________________________

NAME
     menubutton - Create and manipulate menubutton widgets

SYNOPSIS
     menubutton _p_a_t_h_N_a_m_e ?_o_p_t_i_o_n_s?

STANDARD OPTIONS
     activeBackground               cursor          imagetextVariable|
     activeForeground               disabledForegroundjustifyunderline|
     anchor          font           padX            wrapLength     |
     background      foreground     padY                           |
     bitmap          highlightColor relief                         |
     borderWidth     highlightThickness             text

     See the ``options'' manual entry for details on the standard
     options.

WIDGET-SPECIFIC OPTIONS
     Name:           height
     Class:          Height
     Command-Line Switch:-height

          Specifies a desired height for the menubutton.   If  an
          image  or  bitmap  is being displayed in the menubutton
          then the value is in screen  units  (i.e.  any  of  the
          forms  acceptable  to  Tk_GetPixels); for text it is in
          lines of text.  If this  option  isn't  specified,  the
          menubutton's  desired  height is computed from the size
          of the image or bitmap or text being displayed in it.

     Name:           indicatorOn                                   |
     Class:          IndicatorOn                                   |
     Command-Line Switch:-indicatoron                              |

                                                                |
          |                                                        |
          The value must be a proper boolean  value.   If  it  is  |
          true then a small indicator rectangle will be displayed  |
          on the right side of the  menubutton  and  the  default  |
          menu  bindings will treat this as an option menubutton.  |
          If false then no indicator will be displayed.

     Name:           menu
     Class:          MenuName
     Command-Line Switch:-menu

          Specifies the path name of  the  menu  associated  with
          this menubutton.  The menu must be a child of the menu-
          button.




Tk                      Last change: 4.0                        1






menubutton(n)              Tk Commands              menubutton(n)



     Name:           state
     Class:          State
     Command-Line Switch:-state

          Specifies one of three states for the menubutton:  nor-
          mal, active, or disabled.  In normal state the menubut-
          ton is displayed using the  foreground  and  background
          options.   The  active state is typically used when the
          pointer is over the menubutton.  In  active  state  the
          menubutton  is displayed using the activeForeground and
          activeBackground options.  Disabled  state  means  that
          the menubutton is insensitive:  it doesn't activate and
          doesn't respond to mouse button presses.  In this state
          the disabledForeground and background options determine
          how the button is displayed.

     Name:           width
     Class:          Width
     Command-Line Switch:-width

          Specifies a desired width for the  menubutton.   If  an
          image  or  bitmap  is being displayed in the menubutton
          then the value is in screen  units  (i.e.  any  of  the
          forms  acceptable  to  Tk_GetPixels); for text it is in
          characters.   If  this  option  isn't  specified,   the
          menubutton's desired width is computed from the size of
          the image or bitmap or text being displayed in it.
_________________________________________________________________


INTRODUCTION
     The menubutton command creates a new window  (given  by  the
     _p_a_t_h_N_a_m_e  argument)  and  makes it into a menubutton widget.
     Additional options, described above, may be specified on the
     command  line or in the option database to configure aspects
     of the menubutton such as its colors, font, text,  and  ini-
     tial  relief.   The  menubutton command returns its _p_a_t_h_N_a_m_e
     argument.  At the time this command is invoked,  there  must
     not  exist  a  window  named _p_a_t_h_N_a_m_e, but _p_a_t_h_N_a_m_e's parent
     must exist.

     A menubutton is a widget that  displays  a  textual  string,  |
     bitmap,  or  image and is associated with a menu widget.  If  |
     text is displayed, it must all be in a single font,  but  it  |
     can occupy multiple lines on the screen (if it contains new-  |
     lines or  if  wrapping  occurs  because  of  the  wrapLength  |
     option)  and  one of the characters may optionally be under-  |
     lined using the underline option.  In normal usage, pressing
     mouse  button  1  over  the menubutton causes the associated
     menu to be posted just underneath the  menubutton.   If  the
     mouse is moved over the menu before releasing the mouse but-
     ton, the button release causes the underlying menu entry  to



Tk                      Last change: 4.0                        2






menubutton(n)              Tk Commands              menubutton(n)



     be  invoked.   When  the  button  is  released,  the menu is
     unposted.

     Menubuttons are typically organized into groups called  menu
     bars  that  allow  scanning:  if the mouse button is pressed
     over one menubutton (causing it to post its  menu)  and  the
     mouse  is moved over another menubutton in the same menu bar
     without releasing the mouse button, then  the  menu  of  the
     first  menubutton  is unposted and the menu of the new menu-
     button is posted instead.

     There  are  several  interactions  between  menubuttons  and  |
     menus;  see the menu manual entry for information on various  |
     menu configurations,  such  as  pulldown  menus  and  option  |
     menus.                                                        |


WIDGET COMMAND                                                     |
     The menubutton command creates a new Tcl command whose  name  |
     is  _p_a_t_h_N_a_m_e.   This  command  may be used to invoke various  |
     operations on the widget.   It  has  the  following  general  |
     form:                                                         |

          _p_a_t_h_N_a_m_e _o_p_t_i_o_n ?_a_r_g _a_r_g ...?                            |

     _O_p_t_i_o_n and the _a_r_gs determine the exact behavior of the com-  |
     mand.   The  following  commands are possible for menubutton  |
     widgets:                                                      |

     _p_a_t_h_N_a_m_e cget _o_p_t_i_o_n                                       |
          |                                                        |
          Returns the current value of the  configuration  option  |
          given  by  _o_p_t_i_o_n.   _O_p_t_i_o_n  may have any of the values  |
          accepted by the menubutton command.

     _p_a_t_h_N_a_m_e configure ?_o_p_t_i_o_n? ?_v_a_l_u_e _o_p_t_i_o_n _v_a_l_u_e ...?
          Query  or  modify  the  configuration  options  of  the
          widget.   If  no  _o_p_t_i_o_n  is  specified, returns a list
          describing all of the available  options  for  _p_a_t_h_N_a_m_e
          (see  Tk_ConfigureInfo for information on the format of
          this list).  If _o_p_t_i_o_n is specified with no _v_a_l_u_e, then
          the  command  returns  a  list describing the one named
          option (this list will be identical to the  correspond-
          ing  sublist  of  the  value  returned  if no _o_p_t_i_o_n is
          specified).  If one or more  _o_p_t_i_o_n - _v_a_l_u_e  pairs  are
          specified,  then  the command modifies the given widget
          option(s) to have the given value(s);  in this case the
          command  returns  an empty string.  _O_p_t_i_o_n may have any
          of the values accepted by the menubutton command.






Tk                      Last change: 4.0                        3






menubutton(n)              Tk Commands              menubutton(n)



DEFAULT BINDINGS
     Tk automatically creates class bindings for menubuttons that
     give them the following default behavior:

     [1]  A menubutton activates whenever the mouse  passes  over  |
          it and deactivates whenever the mouse leaves it.         |

     [2]                                                        |
          |                                                        |
          Pressing mouse button 1 over  a  menubutton  posts  the  |
          menubutton:  its relief changes to raised and its asso-  |
          ciated menu is posted under  the  menubutton.   If  the  |
          mouse  is  dragged  down  into the menu with the button  |
          still down, and if the mouse button  is  then  released  |
          over  an  entry in the menu, the menubutton is unposted  |
          and the menu entry is invoked.                           |

     [3]                                                        |
          |                                                        |
          If button 1 is  pressed  over  a  menubutton  and  then  |
          released  over  that  menubutton,  the menubutton stays  |
          posted: you can still move the mouse over the menu  and  |
          click  button  1 on an entry to invoke it.  Once a menu  |
          entry has been invoked, the menubutton unposts itself.   |

     [4]                                                        |
          |                                                        |
          If button 1 is  pressed  over  a  menubutton  and  then  |
          dragged  over some other menubutton, the original menu-  |
          button unposts itself and the new menubutton posts.      |

     [5]                                                        |
          |                                                        |
          If button 1 is pressed over a menubutton  and  released  |
          outside  any menubutton or menu, the menubutton unposts  |
          without invoking any menu entry.                         |

     [6]                                                        |
          |                                                        |
          When a menubutton is posted, its associated menu claims  |
          the input focus to allow keyboard traversal of the menu  |
          and its  submenus.   See  the  menu  manual  entry  for  |
          details on these bindings.                               |

     [7]                                                        |
          |                                                        |
          If the underline option has been specified for a  menu-  |
          button  then keyboard traversal may be used to post the  |
          menubutton:  Alt+_x, where _x is the underlined character  |
          (or  its  lower-case  or upper-case equivalent), may be  |
          typed in any window under the menubutton's toplevel  to  |
          post the menubutton.                                     |



Tk                      Last change: 4.0                        4






menubutton(n)              Tk Commands              menubutton(n)



     [8]                                                        |
          |                                                        |
          The F10 key may be typed in  any  window  to  post  the  |
          first  menubutton  under its toplevel window that isn't  |
          disabled.                                                |

     [9]                                                        |
          |                                                        |
          If a menubutton has the  input  focus,  the  space  and  |
          return keys post the menubutton.

     If the menubutton's state is disabled then none of the above
     actions occur:  the menubutton is completely non-responsive.

     The behavior of menubuttons can be changed by  defining  new
     bindings  for  individual widgets or by redefining the class
     bindings.


KEYWORDS
     menubutton, widget


































Tk                      Last change: 4.0                        5




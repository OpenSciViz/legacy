


destroy(n)                 Tk Commands                 destroy(n)



_________________________________________________________________

NAME
     destroy - Destroy one or more windows

SYNOPSIS
     destroy ?_w_i_n_d_o_w _w_i_n_d_o_w ...?
_________________________________________________________________


DESCRIPTION
     This command deletes the windows given by the  _w_i_n_d_o_w  argu-
     ments,  plus all of their descendants.  If a _w_i_n_d_o_w ``.'' is
     deleted then the entire application will be destroyed.   The
     _w_i_n_d_o_ws  are  destroyed  in order, and if an error occurs in
     destroying a window the command  aborts  without  destroying
     the remaining windows.


KEYWORDS
     application, destroy, window


































Tk                       Last change:                           1







time(n)               Tcl Built-In Commands               time(n)



_________________________________________________________________

NAME
     time - Time the execution of a script

SYNOPSIS
     time _s_c_r_i_p_t ?_c_o_u_n_t?
_________________________________________________________________


DESCRIPTION
     This command will call the Tcl interpreter  _c_o_u_n_t  times  to
     evaluate _s_c_r_i_p_t (or once if _c_o_u_n_t isn't specified).  It will
     then return a string of the form

          503 microseconds per iteration

     which indicates the average  amount  of  time  required  per
     iteration,  in  microseconds.   Time  is measured in elapsed
     time, not CPU time.


KEYWORDS
     script, time































Tcl                      Last change:                           1







while(n)              Tcl Built-In Commands              while(n)



_________________________________________________________________

NAME
     while - Execute script repeatedly as long as a condition  is
     met

SYNOPSIS
     while _t_e_s_t _b_o_d_y
_________________________________________________________________


DESCRIPTION
     The _w_h_i_l_e command evaluates _t_e_s_t as an  expression  (in  the
     same  way  that  expr evaluates its argument).  The value of
     the expression must a proper boolean value; if it is a  true
     value  then _b_o_d_y is executed by passing it to the Tcl inter-
     preter.  Once _b_o_d_y has been executed then _t_e_s_t is  evaluated
     again,  and the process repeats until eventually _t_e_s_t evalu-
     ates to a false boolean value.   Continue  commands  may  be
     executed  inside  _b_o_d_y to terminate the current iteration of
     the loop, and break commands may be executed inside _b_o_d_y  to
     cause immediate termination of the while command.  The while
     command always returns an empty string.


KEYWORDS
     boolean value, loop, test, while




























Tcl                      Last change:                           1




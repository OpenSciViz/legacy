


tk_dialog(n)               Tk Commands               tk_dialog(n)



_________________________________________________________________

NAME
     tk_dialog - Create modal dialog and wait for response

SYNOPSIS
     tk_dialog _w_i_n_d_o_w _t_i_t_l_e _t_e_x_t _b_i_t_m_a_p _d_e_f_a_u_l_t _s_t_r_i_n_g _s_t_r_i_n_g ...
_________________________________________________________________


DESCRIPTION
     This procedure is part of the Tk script library.  Its  argu-
     ments describe a dialog box:

     _w_i_n_d_o_w
          Name of top-level window to use for dialog.  Any exist-
          ing window by this name is destroyed.

     _t_i_t_l_e
          Text to appear in the window manager's  title  bar  for
          the dialog.

     _t_e_x_t Message to appear in the top portion of the dialog box.

     _b_i_t_m_a_p
          If non-empty, specifies a bitmap to display in the  top
          portion  of  the  dialog,  to the left of the text.  If
          this is an empty string then no bitmap is displayed  in
          the dialog.

     _d_e_f_a_u_l_t
          If this is an integer greater than or  equal  to  zero,
          then it gives the index of the button that is to be the
          default button for the dialog (0 for the leftmost  but-
          ton,  and so on).  If less than zero or an empty string
          then there won't be any default button.

     _s_t_r_i_n_g
          There will be one button for each of  these  arguments.
          Each  _s_t_r_i_n_g  specifies text to display in a button, in
          order from left to right.

     After creating a dialog box, tk_dialog waits for the user to
     select  one  of the buttons either by clicking on the button
     with the mouse or by typing return  to  invoke  the  default
     button  (if any).  Then it returns the index of the selected
     button:  0 for the leftmost button, 1 for the button next to
     it, and so on.

     While waiting for the user  to  respond,  tk_dialog  sets  a
     local  grab.   This  prevents the user from interacting with
     the application in any way except to invoke the dialog box.



Tk                      Last change: 7.0                        1






tk_dialog(n)               Tk Commands               tk_dialog(n)



KEYWORDS
     bitmap, dialog, modal





















































Tk                      Last change: 7.0                        2




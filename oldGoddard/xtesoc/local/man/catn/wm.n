


wm(n)                      Tk Commands                      wm(n)



_________________________________________________________________

NAME
     wm - Communicate with window manager

SYNOPSIS
     wm _o_p_t_i_o_n _w_i_n_d_o_w ?_a_r_g_s?
_________________________________________________________________


DESCRIPTION
     The wm command is used to interact with window  managers  in
     order  to control such things as the title for a window, its
     geometry, or the increments in terms  of  which  it  may  be
     resized.   The  wm  command can take any of a number of dif-
     ferent forms, depending on the _o_p_t_i_o_n argument.  All of  the
     forms expect at least one additional argument, _w_i_n_d_o_w, which
     must be the path name of a top-level window.

     The legal forms for the wm command are:

     wm aspect _w_i_n_d_o_w ?_m_i_n_N_u_m_e_r _m_i_n_D_e_n_o_m _m_a_x_N_u_m_e_r _m_a_x_D_e_n_o_m?
          If _m_i_n_N_u_m_e_r, _m_i_n_D_e_n_o_m, _m_a_x_N_u_m_e_r, and _m_a_x_D_e_n_o_m  are  all
          specified,  then  they  will  be  passed  to the window
          manager and the  window  manager  should  use  them  to
          enforce a range of acceptable aspect ratios for _w_i_n_d_o_w.
          The aspect ratio of _w_i_n_d_o_w (width/length) will be  con-
          strained   to   lie   between   _m_i_n_N_u_m_e_r/_m_i_n_D_e_n_o_m   and
          _m_a_x_N_u_m_e_r/_m_a_x_D_e_n_o_m.  If _m_i_n_N_u_m_e_r etc. are all  specified
          as  empty  strings, then any existing aspect ratio res-
          trictions are removed.  If _m_i_n_N_u_m_e_r etc. are specified,
          then  the  command returns an empty string.  Otherwise,
          it returns a Tcl list containing four  elements,  which
          are the current values of _m_i_n_N_u_m_e_r, _m_i_n_D_e_n_o_m, _m_a_x_N_u_m_e_r,
          and _m_a_x_D_e_n_o_m (if no aspect restrictions are in  effect,
          then an empty string is returned).

     wm client _w_i_n_d_o_w ?_n_a_m_e?
          If _n_a_m_e is specified, this command stores  _n_a_m_e  (which
          should be the name of the host on which the application
          is executing) in  _w_i_n_d_o_w's  WM_CLIENT_MACHINE  property
          for  use by the window manager or session manager.  The
          command returns an empty string in this case.  If  _n_a_m_e
          isn't  specified, the command returns the last name set
          in a wm client command for _w_i_n_d_o_w.  If _n_a_m_e  is  speci-
          fied  as  an  empty  string,  the  command  deletes the
          WM_CLIENT_MACHINE property from _w_i_n_d_o_w.

     wm colormapwindows _w_i_n_d_o_w ?_w_i_n_d_o_w_L_i_s_t?
          This   command    is    used    to    manipulate    the  |
          WM_COLORMAP_WINDOWS  property,  which provides informa-  |
          tion to the window managers  about  windows  that  have  |



Tk                      Last change: 4.0                        1






wm(n)                      Tk Commands                      wm(n)



          private  colormaps.  If _w_i_n_d_o_w_L_i_s_t isn't specified, the  |
          command returns a list whose elements are the names  of  |
          the  windows  in  the WM_COLORMAP_WINDOWS property.  If  |
          _w_i_n_d_o_w_L_i_s_t is specified, it consists of a list of  win-  |
          dow   path   names;    the   command   overwrites   the  |
          WM_COLORMAP_WINDOWS property with the given windows and  |
          returns  an empty string.  The WM_COLORMAP_WINDOWS pro-  |
          perty should normally contain a list  of  the  internal  |
          windows within _w_i_n_d_o_w whose colormaps differ from their  |
          parents.  The order of  the  windows  in  the  property  |
          indicates  a  priority  order:  the window manager will  |
          attempt to install as many colormaps as  possible  from  |
          the  head  of  this  list when _w_i_n_d_o_w gets the colormap  |
          focus.  If wm colormapwindows is not invoked,  Tk  will  |
          automatically  set the property for each top-level win-  |
          dow to all the internal windows whose colormaps  differ  |
          from  their  parents,  but the order is undefined.  See  |
          the ICCCM documentation for  more  information  on  the  |
          WM_COLORMAP_WINDOWS property.

     wm command _w_i_n_d_o_w ?_v_a_l_u_e?
          If _v_a_l_u_e is specified, this  command  stores  _v_a_l_u_e  in
          _w_i_n_d_o_w's  WM_COMMAND  property  for  use  by the window
          manager or session manager and returns an empty string.
          _V_a_l_u_e  must  have  proper list structure;  the elements
          should contain the words of the command used to  invoke
          the  application.   If  _v_a_l_u_e  isn't specified then the
          command returns the last value set in a wm command com-
          mand  for  _w_i_n_d_o_w.   If  _v_a_l_u_e is specified as an empty
          string, the command  deletes  the  WM_COMMAND  property
          from _w_i_n_d_o_w.

     wm deiconify _w_i_n_d_o_w
          Arrange for _w_i_n_d_o_w to  be  displayed  in  normal  (non-
          iconified)  form.   This is done by mapping the window.
          If the window has never been mapped then  this  command
          will  not  map the window, but it will ensure that when
          the window is first mapped it will be displayed in  de-
          iconified form.  Returns an empty string.

     wm focusmodel _w_i_n_d_o_w ?active|passive?
          If active or passive is supplied as an  optional  argu-
          ment  to the command, then it specifies the focus model
          for _w_i_n_d_o_w.  In this case the command returns an  empty
          string.   If  no  additional argument is supplied, then
          the command returns the current focus model for _w_i_n_d_o_w.
          An  active focus model means that _w_i_n_d_o_w will claim the
          input focus for itself  or  its  descendants,  even  at
          times  when the focus is currently in some other appli-
          cation.  Passive means that _w_i_n_d_o_w will never claim the
          focus  for  itself:  the window manager should give the
          focus to _w_i_n_d_o_w at appropriate  times.   However,  once



Tk                      Last change: 4.0                        2






wm(n)                      Tk Commands                      wm(n)



          the  focus  has been given to _w_i_n_d_o_w or one of its des-
          cendants, the application may re-assign the focus among
          _w_i_n_d_o_w's descendants.  The focus model defaults to pas-
          sive, and Tk's focus command assumes a passive model of
          focussing.

     wm frame _w_i_n_d_o_w
          If _w_i_n_d_o_w has been reparented  by  the  window  manager
          into a decorative frame, the command returns the X win-
          dow identifier for the outermost  frame  that  contains
          _w_i_n_d_o_w  (the window whose parent is the root or virtual
          root).  If _w_i_n_d_o_w hasn't been reparented by the  window
          manager  then the command returns the X window identif-
          ier for _w_i_n_d_o_w.

     wm geometry _w_i_n_d_o_w ?_n_e_w_G_e_o_m_e_t_r_y?
          If _n_e_w_G_e_o_m_e_t_r_y is specified, then the geometry of  _w_i_n_-
          _d_o_w is changed and an empty string is returned.  Other-
          wise the current geometry for _w_i_n_d_o_w is returned  (this
          is  the most recent geometry specified either by manual
          resizing or in a wm geometry command).  _N_e_w_G_e_o_m_e_t_r_y has
          the  form  =_w_i_d_t_hx_h_e_i_g_h_t +_ _x+__y, where any of =, _w_i_d_t_hx-
          _h_e_i_g_h_t, or +__x+__y may be omitted.  _W_i_d_t_h and  _h_e_i_g_h_t  are
          positive  integers specifying the desired dimensions of
          _w_i_n_d_o_w.  If _w_i_n_d_o_w is  gridded  (see  GRIDDED  GEOMETRY
          MANAGEMENT  below) then the dimensions are specified in
          grid units;  otherwise  they  are  specified  in  pixel
          units.   _X and _y specify the desired location of _w_i_n_d_o_w
          on the screen, in pixels.  If _x is preceded  by  +,  it
          specifies the number of pixels between the left edge of
          the screen and the left edge of  _w_i_n_d_o_w's  border;   if
          preceded  by  -  then  _x specifies the number of pixels
          between the right edge of the screen and the right edge
          of  _w_i_n_d_o_w's  border.   If  _y  is preceded by + then it
          specifies the number of pixels between the top  of  the
          screen  and  the  top of _w_i_n_d_o_w's border;  if _y is pre-
          ceded by - then  it  specifies  the  number  of  pixels
          between the bottom of _w_i_n_d_o_w's border and the bottom of
          the screen.  If _n_e_w_G_e_o_m_e_t_r_y is specified  as  an  empty
          string  then  any  existing user-specified geometry for
          _w_i_n_d_o_w is cancelled, and the window will revert to  the
          size requested internally by its widgets.

     wm grid _w_i_n_d_o_w ?_b_a_s_e_W_i_d_t_h _b_a_s_e_H_e_i_g_h_t _w_i_d_t_h_I_n_c _h_e_i_g_h_t_I_n_c?
          This command indicates that _w_i_n_d_o_w is to be managed  as
          a  gridded  window.  It also specifies the relationship
          between grid units  and  pixel  units.   _B_a_s_e_W_i_d_t_h  and
          _b_a_s_e_H_e_i_g_h_t specify the number of grid units correspond-
          ing to the pixel  dimensions  requested  internally  by
          _w_i_n_d_o_w    using   Tk_GeometryRequest.    _W_i_d_t_h_I_n_c   and
          _h_e_i_g_h_t_I_n_c specify the number of pixels in each horizon-
          tal   and   vertical  grid  unit.   These  four  values



Tk                      Last change: 4.0                        3






wm(n)                      Tk Commands                      wm(n)



          determine a  range  of  acceptable  sizes  for  _w_i_n_d_o_w,
          corresponding to grid-based widths and heights that are
          non-negative integers.  Tk will pass  this  information
          to  the  window  manager;   during manual resizing, the
          window manager will restrict the window's size  to  one
          of  these acceptable sizes.  Furthermore, during manual
          resizing the window manager will display  the  window's
          current size in terms of grid units rather than pixels.
          If _b_a_s_e_W_i_d_t_h etc. are all specified as  empty  strings,
          then _w_i_n_d_o_w will no longer be managed as a gridded win-
          dow.  If _b_a_s_e_W_i_d_t_h etc. are specified then  the  return
          value  is  an empty string.  Otherwise the return value
          is a Tcl list containing four elements corresponding to
          the   current   _b_a_s_e_W_i_d_t_h,  _b_a_s_e_H_e_i_g_h_t,  _w_i_d_t_h_I_n_c,  and
          _h_e_i_g_h_t_I_n_c;  if _w_i_n_d_o_w is not currently gridded, then an
          empty  string  is  returned.  Note: this command should
          not be needed very often, since the Tk_SetGrid  library
          procedure  and the setGrid option provide easier access
          to the same functionality.

     wm group _w_i_n_d_o_w ?_p_a_t_h_N_a_m_e?
          If _p_a_t_h_N_a_m_e is specified, it gives the  path  name  for
          the  leader  of a group of related windows.  The window
          manager may use this information, for example, to unmap
          all  of  the windows in a group when the group's leader
          is iconified.  _P_a_t_h_N_a_m_e may be specified  as  an  empty
          string to remove _w_i_n_d_o_w from any group association.  If
          _p_a_t_h_N_a_m_e is specified then the command returns an empty
          string;  otherwise it returns the path name of _w_i_n_d_o_w's
          current group leader, or  an  empty  string  if  _w_i_n_d_o_w
          isn't part of any group.

     wm iconbitmap _w_i_n_d_o_w ?_b_i_t_m_a_p?
          If _b_i_t_m_a_p is specified, then it names a bitmap  in  the
          standard  forms  accepted  by  Tk (see the Tk_GetBitmap
          manual entry for details).  This bitmap  is  passed  to
          the  window  manager  to be displayed in _w_i_n_d_o_w's icon,
          and the command returns an empty string.  If  an  empty
          string  is  specified for _b_i_t_m_a_p, then any current icon
          bitmap is cancelled for _w_i_n_d_o_w.  If _b_i_t_m_a_p is specified
          then the command returns an empty string.  Otherwise it
          returns the name of the current icon bitmap  associated
          with  _w_i_n_d_o_w,  or an empty string if _w_i_n_d_o_w has no icon
          bitmap.

     wm iconify _w_i_n_d_o_w
          Arrange for _w_i_n_d_o_w to be iconified.  It  _w_i_n_d_o_w  hasn't
          yet  been  mapped for the first time, this command will
          arrange for it to appear in the iconified state when it
          is eventually mapped.

     wm iconmask _w_i_n_d_o_w ?_b_i_t_m_a_p?



Tk                      Last change: 4.0                        4






wm(n)                      Tk Commands                      wm(n)



          If _b_i_t_m_a_p is specified, then it names a bitmap  in  the
          standard  forms  accepted  by  Tk (see the Tk_GetBitmap
          manual entry for details).  This bitmap  is  passed  to
          the  window manager to be used as a mask in conjunction
          with the iconbitmap option:  where the mask has  zeroes
          no icon will be displayed;  where it has ones, the bits
          from the icon bitmap will be displayed.   If  an  empty
          string  is  specified  for _b_i_t_m_a_p then any current icon
          mask is cancelled for _w_i_n_d_o_w  (this  is  equivalent  to
          specifying  a bitmap of all ones).  If _b_i_t_m_a_p is speci-
          fied then the command returns an empty string.   Other-
          wise it returns the name of the current icon mask asso-
          ciated with _w_i_n_d_o_w, or an empty string if no mask is in
          effect.

     wm iconname _w_i_n_d_o_w ?_n_e_w_N_a_m_e?
          If _n_e_w_N_a_m_e is specified, then it is passed to the  win-
          dow manager;  the window manager should display _n_e_w_N_a_m_e
          inside the icon associated with _w_i_n_d_o_w.  In  this  case
          an  empty  string  is  returned  as result.  If _n_e_w_N_a_m_e
          isn't specified then the command  returns  the  current
          icon  name  for  _w_i_n_d_o_w,  or an empty string if no icon
          name has  been  specified  (in  this  case  the  window
          manager  will  normally  display the window's title, as
          specified with the wm title command).

     wm iconposition _w_i_n_d_o_w ?_x _y?
          If _x and _y are specified, they are passed to the window
          manager  as a hint about where to position the icon for
          _w_i_n_d_o_w.  In this case an empty string is returned.   If
          _x  and _y are specified as empty strings then any exist-
          ing icon position hint is cancelled.  If neither _x  nor
          _y  is  specified,  then  the command returns a Tcl list
          containing two values, which are the current icon posi-
          tion  hints  (if  no  hints are in effect then an empty
          string is returned).

     wm iconwindow _w_i_n_d_o_w ?_p_a_t_h_N_a_m_e?
          If _p_a_t_h_N_a_m_e is specified, it is the  path  name  for  a
          window to use as icon for _w_i_n_d_o_w: when _w_i_n_d_o_w is iconi-
          fied then _p_a_t_h_N_a_m_e will be mapped to serve as icon, and
          when  _w_i_n_d_o_w  is  de-iconified  then  _p_a_t_h_N_a_m_e  will be
          unmapped again.  If _p_a_t_h_N_a_m_e is specified as  an  empty
          string  then  any  existing icon window association for
          _w_i_n_d_o_w will be cancelled.  If the _p_a_t_h_N_a_m_e argument  is
          specified  then an empty string is returned.  Otherwise
          the command returns the path name of the  current  icon
          window  for  _w_i_n_d_o_w,  or an empty string if there is no
          icon window currently specified for _w_i_n_d_o_w.  Note:  not
          all  window managers support the notion of an icon win-
          dow.




Tk                      Last change: 4.0                        5






wm(n)                      Tk Commands                      wm(n)



     wm maxsize _w_i_n_d_o_w ?_w_i_d_t_h _h_e_i_g_h_t?
          If _w_i_d_t_h and _h_e_i_g_h_t are specified, they give  the  max-  |
          imum  permissible  dimensions  for _w_i_n_d_o_w.  For gridded  |
          windows the dimensions are  specified  in  grid  units;  |
          otherwise  they are specified in pixel units.  The win-  |
          dow manager will restrict the window's dimensions to be  |
          less  than  or equal to _w_i_d_t_h and _h_e_i_g_h_t.  If _w_i_d_t_h and  |
          _h_e_i_g_h_t are specified, then the command returns an empty  |
          string.   Otherwise it returns a Tcl list with two ele-  |
          ments, which are the maximum width and height currently  |
          in  effect.   The  maximum size defaults to the size of  |
          the screen.  See the sections  on  geometry  management  |
          below for more information.                              |

     wm minsize _w_i_n_d_o_w ?_w_i_d_t_h _h_e_i_g_h_t?                           |
          |                                                        |
          If _w_i_d_t_h  and  _h_e_i_g_h_t  are  specified,  they  give  the  |
          minimum permissible dimensions for _w_i_n_d_o_w.  For gridded  |
          windows the dimensions are  specified  in  grid  units;  |
          otherwise  they are specified in pixel units.  The win-  |
          dow manager will restrict the window's dimensions to be  |
          greater  than  or  equal to _w_i_d_t_h and _h_e_i_g_h_t.  If _w_i_d_t_h  |
          and _h_e_i_g_h_t are specified, then the command  returns  an  |
          empty string.  Otherwise it returns a Tcl list with two  |
          elements,  which  are  the  minimum  width  and  height  |
          currently  in effect.  The minimum size defaults to one  |
          pixel in each dimension.  See the sections on  geometry  |
          management below for more information.

     wm overrideredirect _w_i_n_d_o_w ?_b_o_o_l_e_a_n?
          If _b_o_o_l_e_a_n is specified, it must have a proper  boolean
          form  and  the override-redirect flag for _w_i_n_d_o_w is set
          to that value.  If _b_o_o_l_e_a_n is not specified then 1 or 0
          is  returned  to  indicate whether or not the override-
          redirect flag is currently set for _w_i_n_d_o_w.  Setting the
          override-redirect  flag  for  a  window causes it to be
          ignored by the window  manager;   among  other  things,
          this  means that the window will not be reparented from
          the root window into a decorative frame  and  the  user
          will  not  be  able  to manipulate the window using the
          normal window manager mechanisms.

     wm positionfrom _w_i_n_d_o_w ?_w_h_o?
          If _w_h_o is specified, it must be either program or user,
          or  an  abbreviation of one of these two.  It indicates
          whether _w_i_n_d_o_w's current position was requested by  the
          program  or  by  the user.  Many window managers ignore
          program-requested initial positions and ask the user to
          manually  position  the  window;   if user is specified
          then the window manager should position the  window  at
          the given place without asking the user for assistance.
          If _w_h_o is  specified  as  an  empty  string,  then  the



Tk                      Last change: 4.0                        6






wm(n)                      Tk Commands                      wm(n)



          current position source is cancelled.  If _w_h_o is speci-
          fied, then the command returns an empty string.  Other-
          wise  it  returns user or window to indicate the source
          of the window's current position, or an empty string if
          no source has been specified yet.  Most window managers
          interpret ``no source'' as equivalent to  program.   Tk
          will automatically set the position source to user when
          a wm geometry command is invoked, unless the source has
          been set explicitly to program.

     wm protocol _w_i_n_d_o_w ?_n_a_m_e? ?_c_o_m_m_a_n_d?
          This command is used to manage window manager protocols
          such  as WM_DELETE_WINDOW.  _N_a_m_e is the name of an atom
          corresponding to a window  manager  protocol,  such  as
          WM_DELETE_WINDOW  or WM_SAVE_YOURSELF or WM_TAKE_FOCUS.
          If both _n_a_m_e and _c_o_m_m_a_n_d are specified, then _c_o_m_m_a_n_d is
          associated  with  the protocol specified by _n_a_m_e.  _N_a_m_e
          will be added to _w_i_n_d_o_w's WM_PROTOCOLS property to tell
          the  window manager that the application has a protocol
          handler for _n_a_m_e, and _c_o_m_m_a_n_d will be  invoked  in  the
          future  whenever  the window manager sends a message to
          the client for that protocol.  In this case the command
          returns an empty string.  If _n_a_m_e is specified but _c_o_m_-
          _m_a_n_d isn't,  then  the  current  command  for  _n_a_m_e  is
          returned,  or  an  empty  string if there is no handler
          defined for _n_a_m_e.  If _c_o_m_m_a_n_d is specified as an  empty
          string then the current handler for _n_a_m_e is deleted and
          it is removed from the WM_PROTOCOLS property on _w_i_n_d_o_w;
          an  empty  string is returned.  Lastly, if neither _n_a_m_e
          nor _c_o_m_m_a_n_d is specified, the command returns a list of
          all  the  protocols  for  which  handlers are currently
          defined for _w_i_n_d_o_w.

          Tk   always   defines   a    protocol    handler    for
          WM_DELETE_WINDOW,  even  if  you  haven't asked for one
          with  wm  protocol.   If  a  WM_DELETE_WINDOW   message
          arrives  when  you  haven't  defined a handler, then Tk
          handles the message by destroying the window for  which
          it was received.

     wm sizefrom _w_i_n_d_o_w ?_w_h_o?
          If _w_h_o is specified, it must be either program or user,
          or  an  abbreviation of one of these two.  It indicates
          whether _w_i_n_d_o_w's current size was requested by the pro-
          gram  or  by  the  user.   Some  window managers ignore
          program-requested sizes and ask the  user  to  manually
          size  the window;  if user is specified then the window
          manager should  give  the  window  its  specified  size
          without  asking  the  user  for  assistance.  If _w_h_o is
          specified as an empty string,  then  the  current  size
          source  is  cancelled.   If  _w_h_o is specified, then the
          command returns an empty string.  Otherwise it  returns



Tk                      Last change: 4.0                        7






wm(n)                      Tk Commands                      wm(n)



          user  or  window to indicate the source of the window's
          current size, or an empty string if no source has  been
          specified  yet.   Most  window  managers interpret ``no
          source'' as equivalent to program.

     wm state _w_i_n_d_o_w
          Returns the current state of  _w_i_n_d_o_w:   either  normal,
          iconic, or withdrawn.

     wm title _w_i_n_d_o_w ?_s_t_r_i_n_g?
          If _s_t_r_i_n_g is specified, then it will be passed  to  the
          window  manager  for  use  as the title for _w_i_n_d_o_w (the
          window manager should display this string  in  _w_i_n_d_o_w's
          title  bar).  In this case the command returns an empty
          string.  If _s_t_r_i_n_g isn't  specified  then  the  command
          returns  the  current  title for the _w_i_n_d_o_w.  The title
          for a window defaults to its name.

     wm transient _w_i_n_d_o_w ?_m_a_s_t_e_r?
          If _m_a_s_t_e_r is specified,  then  the  window  manager  is
          informed  that _w_i_n_d_o_w is a transient window (e.g. pull-
          down menu) working on behalf of _m_a_s_t_e_r (where _m_a_s_t_e_r is
          the  path  name  for  a top-level window).  Some window
          managers will use this  information  to  manage  _w_i_n_d_o_w
          specially.   If  _m_a_s_t_e_r is specified as an empty string
          then _w_i_n_d_o_w is marked as not being a  transient  window
          any  more.   If  _m_a_s_t_e_r  is specified, then the command
          returns an empty string.  Otherwise the command returns
          the  path  name of _w_i_n_d_o_w's current master, or an empty
          string if _w_i_n_d_o_w isn't currently a transient window.

     wm withdraw _w_i_n_d_o_w
          Arranges for _w_i_n_d_o_w to be withdrawn  from  the  screen.
          This  causes  the  window  to be unmapped and forgotten
          about by the window manager.  If the window  has  never
          been  mapped, then this command causes the window to be
          mapped in the withdrawn state.  Not all window managers
          appear to know how to handle windows that are mapped in
          the withdrawn state.  Note: it sometimes  seems  to  be
          necessary to withdraw a window and then re-map it (e.g.
          with wm deiconify) to get some window managers  to  pay
          attention  to  changes  in  window  attributes  such as
          group.


GEOMETRY MANAGEMENT
     By default a top-level window appears on the screen  in  its  |
     _n_a_t_u_r_a_l  _s_i_z_e, which is the one determined internally by its  |
     widgets and geometry managers.  If the  natural  size  of  a  |
     top-level  window changes, then the window's size changes to  |
     match.  A top-level window can be given a  size  other  than  |
     its  natural  size  in two ways.  First, the user can resize  |



Tk                      Last change: 4.0                        8






wm(n)                      Tk Commands                      wm(n)



     the window manually  using  the  facilities  of  the  window  |
     manager,  such  as  resize handles.  Second, the application  |
     can request a particular size for a top-level  window  using  |
     the wm geometry command.  These two cases are handled ident-  |
     ically by Tk;  in either case, the requested size  overrides  |
     the  natural size.  You can return the window to its natural  |
     by invoking wm geometry with an empty _g_e_o_m_e_t_r_y string.        |

     Normally a top-level window can have any size from one pixel  |
     in  each  dimension  up to the size of its screen.  However,  |
     you can use the wm minsize and wm maxsize commands to  limit  |
     the  range  of allowable sizes.  The range set by wm minsize  |
     and wm maxsize applies to all forms of  resizing,  including  |
     the  window's natural size as well as manual resizes and the  |
     wm geometry command.


GRIDDED GEOMETRY MANAGEMENT
     Gridded geometry management occurs when one of  the  widgets
     of  an  application  supports a range of useful sizes.  This
     occurs, for example, in a text editor where the  scrollbars,
     menus,  and  other adornments are fixed in size but the edit
     widget can support any number of lines of text or characters
     per  line.  In this case, it is usually desirable to let the
     user specify the number  of  lines  or  characters-per-line,
     either  with  the  wm  geometry  command or by interactively
     resizing the window.  In the case  of  text,  and  in  other
     interesting  cases  also,  only discrete sizes of the window
     make  sense,  such  as  integral  numbers   of   lines   and
     characters-per-line;  arbitrary pixel sizes are not useful.

     Gridded geometry management provides support for  this  kind
     of  application.   Tk  (and  the window manager) assume that
     there is a grid of some sort within the application and that
     the  application  should  be  resized in terms of _g_r_i_d _u_n_i_t_s
     rather than pixels.  Gridded geometry  management  is  typi-
     cally invoked by turning on the setGrid option for a widget;
     it can also be invoked with the wm grid command or  by  cal-
     ling Tk_SetGrid.  In each of these approaches the particular
     widget (or sometimes code in the  application  as  a  whole)
     specifies  the  relationship between integral grid sizes for
     the window  and  pixel  sizes.   To  return  to  non-gridded
     geometry  management,  invoke  wm  grid  with empty argument
     strings.

     When gridded geometry management is  enabled  then  all  the  |
     dimensions  specified  in  wm  minsize,  wm  maxsize, and wm  |
     geometry commands are treated  as  grid  units  rather  than  |
     pixel  units.   Interactive  resizing is also carried out in  |
     even numbers of grid units rather than pixels.





Tk                      Last change: 4.0                        9






wm(n)                      Tk Commands                      wm(n)



BUGS
     Most existing window  managers  appear  to  have  bugs  that
     affect  the  operation of the wm command.  For example, some
     changes won't take effect if the window is  already  active:
     the  window  will  have  to be withdrawn and de-iconified in
     order to make the change happen.


KEYWORDS
     aspect ratio, deiconify, focus model, geometry, grid, group,
     icon,  iconify, increments, position, size, title, top-level
     window, units, window manager











































Tk                      Last change: 4.0                       10







flush(n)              Tcl Built-In Commands              flush(n)



_________________________________________________________________

NAME
     flush - Flush buffered output for a file

SYNOPSIS
     flush _f_i_l_e_I_d
_________________________________________________________________


DESCRIPTION
     Flushes any  output  that  has  been  buffered  for  _f_i_l_e_I_d.
     _F_i_l_e_I_d  must have been the return value from a previous call
     to open, or it may be stdout or stderr to access one of  the
     standard  I/O  streams;  it  must  refer  to a file that was
     opened for writing.  The command returns an empty string.


KEYWORDS
     buffer, file, flush, output



































Tcl                      Last change:                           1




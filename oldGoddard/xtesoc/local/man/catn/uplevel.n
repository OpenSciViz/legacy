


uplevel(n)            Tcl Built-In Commands            uplevel(n)



_________________________________________________________________

NAME
     uplevel - Execute a script in a different stack frame

SYNOPSIS
     uplevel ?_l_e_v_e_l? _a_r_g ?_a_r_g ...?
_________________________________________________________________


DESCRIPTION
     All of the _a_r_g arguments are concatenated  as  if  they  had
     been  passed  to concat; the result is then evaluated in the
     variable context indicated by _l_e_v_e_l.   Uplevel  returns  the
     result of that evaluation.

     If _l_e_v_e_l is an integer then it gives a distance (up the pro-
     cedure  calling stack) to move before executing the command.
     If _l_e_v_e_l consists of # followed by a number then the  number
     gives an absolute level number.  If _l_e_v_e_l is omitted then it
     defaults to 1.  _L_e_v_e_l cannot be defaulted if the first  _c_o_m_-
     _m_a_n_d argument starts with a digit or #.

     For example, suppose that procedure a was invoked from  top-
     level,  and  that it called b, and that b called c.  Suppose
     that c invokes the uplevel command.  If _l_e_v_e_l is 1 or #2  or
     omitted,  then  the command will be executed in the variable
     context of b.  If _l_e_v_e_l is 2 or #1 then the command will  be
     executed  in the variable context of a.  If _l_e_v_e_l is 3 or #0
     then the command will be executed at top-level (only  global
     variables will be visible).

     The uplevel command causes the invoking procedure to  disap-
     pear  from  the procedure calling stack while the command is
     being executed.  In the above example, suppose c invokes the
     command

          uplevel 1 {set x 43; d}

     where d is another Tcl  procedure.   The  set  command  will
     modify  the variable x in b's context, and d will execute at
     level 3, as if called from b.  If it in  turn  executes  the
     command

          uplevel {set x 42}

     then the set command will modify the same variable x in  b's
     context:   the procedure c does not appear to be on the call
     stack when d is executing.  The command ``info  level''  may
     be used to obtain the level of the current procedure.





Tcl                      Last change:                           1






uplevel(n)            Tcl Built-In Commands            uplevel(n)



     Uplevel makes it possible  to  implement  new  control  con-
     structs  as  Tcl  procedures  (for example, uplevel could be
     used to implement the while construct as a Tcl procedure).


KEYWORDS
     context, stack frame, variables
















































Tcl                      Last change:                           2







source(n)             Tcl Built-In Commands             source(n)



_________________________________________________________________

NAME
     source - Evaluate a file as a Tcl script

SYNOPSIS
     source _f_i_l_e_N_a_m_e
_________________________________________________________________


DESCRIPTION
     Read file _f_i_l_e_N_a_m_e and pass the contents to the  Tcl  inter-
     preter  as  a script to evaluate in the normal fashion.  The
     return value from source is the return  value  of  the  last
     command  executed  from  the  file.   If  an error occurs in
     evaluating the contents of the file then the source  command
     will return that error.  If a return command is invoked from
     within the file then the  remainder  of  the  file  will  be
     skipped and the source command will return normally with the
     result from the return command.  If _f_i_l_e_N_a_m_e starts  with  a
     tilde,  then  it  is  tilde-substituted  as described in the
     Tcl_TildeSubst manual entry.


KEYWORDS
     file, script





























Tcl                      Last change:                           1







expr(n)               Tcl Built-In Commands               expr(n)



_________________________________________________________________

NAME
     expr - Evalue an expression

SYNOPSIS
     expr _a_r_g ?_a_r_g _a_r_g ...?
_________________________________________________________________


DESCRIPTION
     Concatenates _a_r_g's (adding separator spaces  between  them),  |
     evaluates  the  result  as a Tcl expression, and returns the  |
     value.  The operators permitted in  Tcl  expressions  are  a
     subset of the operators permitted in C expressions, and they
     have the same meaning and precedence as the corresponding  C
     operators.   Expressions almost always yield numeric results
     (integer  or  floating-point  values).   For  example,   the
     expression

          expr 8.2 + 6

     evaluates to 14.2.  Tcl expressions differ  from  C  expres-
     sions  in  the  way  that operands are specified.  Also, Tcl
     expressions support non-numeric  operands  and  string  com-
     parisons.

OPERANDS
     A Tcl expression consists  of  a  combination  of  operands,
     operators, and parentheses.  White space may be used between
     the operands and operators and parentheses; it is ignored by
     the  expression  processor.   Where  possible,  operands are
     interpreted as integer values.  Integer values may be speci-
     fied  in  decimal  (the normal case), in octal (if the first
     character of the operand is 0), or in  hexadecimal  (if  the
     first  two characters of the operand are 0x).  If an operand
     does not have one of the integer formats given  above,  then
     it  is  treated as a floating-point number if that is possi-
     ble.  Floating-point numbers may be specified in any of  the
     ways  accepted  by an ANSI-compliant C compiler (except that
     the ``f'', ``F'', ``l'', and ``L'' suffixes will not be per-
     mitted in most installations).  For example, all of the fol-
     lowing are valid  floating-point  numbers:   2.1,  3.,  6e4,
     7.91e+16.  If no numeric interpretation is possible, then an
     operand is left as a string  (and  only  a  limited  set  of
     operators may be applied to it).

     Operands may be specified in any of the following ways:

     [1]  As an numeric value, either integer or floating-point.

     [2]  As a Tcl variable,  using  standard  $  notation.   The



Tcl                     Last change: 7.0                        1






expr(n)               Tcl Built-In Commands               expr(n)



          variable's value will be used as the operand.

     [3]  As a string enclosed in double-quotes.  The  expression
          parser  will  perform  backslash, variable, and command
          substitutions on the information  between  the  quotes,
          and use the resulting value as the operand

     [4]  As a string enclosed in braces.  The characters between
          the open brace and matching close brace will be used as
          the operand without any substitutions.

     [5]  As a Tcl command enclosed  in  brackets.   The  command
          will  be  executed  and  its result will be used as the
          operand.

     [6]  As a mathematical function whose arguments have any  of  |
          the above forms for operands, such as ``sin($x)''.  See  |
          below for a list of defined functions.

     Where  substitutions  occur  above   (e.g.   inside   quoted
     strings),  they  are  performed by the expression processor.
     However, an additional layer  of  substitution  may  already
     have been performed by the command parser before the expres-
     sion processor was called.  As discussed below, it  is  usu-
     ally  best  to  enclose expressions in braces to prevent the
     command parser from performing  substitutions  on  the  con-
     tents.

     For some examples of simple expressions, suppose  the  vari-
     able  a  has the value 3 and the variable b has the value 6.
     Then the command on the left side of each of the lines below
     will produce the value on the right side of the line:

          expr 3.1 + $a           6.1
          expr 2 + "$a.$b"        5.6
          expr 4*[llength "6 2"]  8
          expr {{word one} < "word $a"}0


OPERATORS
     The valid operators are listed below, grouped in  decreasing
     order of precedence:

     -  +  ~  !           Unary minus, unary plus, bit-wise  NOT,  |
                         logical NOT.  None of these operands may
                         be applied to string operands, and  bit-
                         wise   NOT   may   be  applied  only  to
                         integers.

     *  /  %             Multiply, divide,  remainder.   None  of
                         these  operands may be applied to string
                         operands, and remainder may  be  applied



Tcl                     Last change: 7.0                        2






expr(n)               Tcl Built-In Commands               expr(n)



                         only  to  integers.   The remainder will  |
                         always have the same sign as the divisor  |
                         and  an  absolute value smaller than the  |
                         divisor.

     +   -                 Add  and  subtract.   Valid  for   any
                         numeric operands.

     <<  >>              Left and right shift.  Valid for integer
                         operands only.

     <  >  <=  >=        Boolean  less,  greater,  less  than  or
                         equal,  and greater than or equal.  Each
                         operator produces 1 if the condition  is
                         true,  0 otherwise.  These operators may
                         be applied to strings as well as numeric
                         operands,  in  which  case  string  com-
                         parison is used.

     ==  !=              Boolean  equal  and  not  equal.    Each
                         operator  produces  a  zero/one  result.
                         Valid for all operand types.

     &                   Bit-wise   AND.    Valid   for   integer
                         operands only.

     ^                   Bit-wise  exclusive   OR.    Valid   for
                         integer operands only.

     |                   Bit-wise OR.  Valid for integer operands
                         only.

     &&                  Logical AND.  Produces  a  1  result  if
                         both operands are non-zero, 0 otherwise.
                         Valid   for   numeric   operands    only
                         (integers or floating-point).

     ||                  Logical OR.  Produces a 0 result if both
                         operands  are  zero, 1 otherwise.  Valid
                         for numeric operands only  (integers  or
                         floating-point).

     _x?_y:_z               If-then-else, as in C.  If  _x  evaluates
                         to  non-zero,  then  the  result  is the
                         value of _y.  Otherwise the result is the
                         value  of  _z.  The _x operand must have a
                         numeric value.

     See the C manual for more details on the results produced by
     each  operator.   All of the binary operators group left-to-
     right within the same precedence level.   For  example,  the
     command



Tcl                     Last change: 7.0                        3






expr(n)               Tcl Built-In Commands               expr(n)



          expr 4*2 < 7

     returns 0.

     The &&, ||, and ?: operators have ``lazy evaluation'',  just
     as in C, which means that operands are not evaluated if they
     are not needed to determine the outcome.   For  example,  in
     the command

          expr {$v ? [a] : [b]}

     only one of [a] or [b] will actually be evaluated, depending
     on  the  value of $v.  Note, however, that this is only true
     if the entire expression is enclosed in  braces;   otherwise
     the  Tcl parser will evaluate both [a] and [b] before invok-
     ing the expr command.

MATH FUNCTIONS
     Tcl supports the following mathematical functions in expres-  |
     sions:                                                        |

          acos        cos         hypot      sinh                  |
          asin        cosh        log        sqrt                  |
          atan        exp         log10      tan                   |
          atan2       floor       pow        tanh                  |
          ceil        fmod        sin                              |

     Each of these functions invokes the math library function of  |
     the same name;  see the manual entries for the library func-  |
     tions for details on what they do.  Tcl also implements  the  |
     following  functions  for  conversion  between  integers and  |
     floating-point numbers:                                       |

     abs(_a_r_g)                                                   |
          |                                                        |
          Returns the absolute value of _a_r_g.  _A_r_g may  be  either  |
          integer  or  floating-point, and the result is returned  |
          in the same form.                                        |

     double(_a_r_g)                                                |
          |                                                        |
          If _a_r_g is a floating value, returns _a_r_g, otherwise con-  |
          verts _a_r_g to floating and returns the converted value.   |

     int(_a_r_g)                                                   |
          |                                                        |
          If _a_r_g is an integer value, returns _a_r_g, otherwise con-  |
          verts _a_r_g to integer by truncation and returns the con-  |
          verted value.                                            |

     round(_a_r_g)                                                 |
          |                                                        |



Tcl                     Last change: 7.0                        4






expr(n)               Tcl Built-In Commands               expr(n)



          If _a_r_g is an integer value, returns _a_r_g, otherwise con-  |
          verts  _a_r_g  to integer by rounding and returns the con-  |
          verted value.                                            |

     In addition to these predifined functions, applications  may  |
     define additional functions using Tcl_CreateMathFunc().

TYPES, OVERFLOW, AND PRECISION
     All internal computations involving integers are  done  with
     the  C  type  _l_o_n_g,  and all internal computations involving
     floating-point are done with the C type _d_o_u_b_l_e.   When  con-
     verting  a  string  to  floating-point, exponent overflow is
     detected and results in a  Tcl  error.   For  conversion  to
     integer  from  string,  detection of overflow depends on the
     behavior of some routines in the  local  C  library,  so  it
     should  be  regarded  as  unreliable.   In any case, integer
     overflow and underflow are generally not  detected  reliably
     for   intermediate  results.   Floating-point  overflow  and
     underflow are  detected  to  the  degree  supported  by  the
     hardware, which is generally pretty reliable.

     Conversion  among  internal  representations  for   integer,
     floating-point, and string operands is done automatically as
     needed.  For  arithmetic  computations,  integers  are  used
     until  some floating-point number is introduced, after which
     floating-point is used.  For example,

          expr 5 / 4

     returns 1, while

          expr 5 / 4.0
          expr 5 / ( [string length "abcd"] + 0.0 )

     both return 1.25.  Floating-point values are always returned  |
     with  a  ``.''   or an ``e'' so that they will not look like  |
     integer values.  For example,                                 |

          expr 20.0/5.0                                            |

     returns   ``4.0'',   not   ``4''.    The   global   variable  |
     tcl_precision  determines  the  the  number  of  significant  |
     digits that are retained when floating values are  converted  |
     to  strings  (except  that trailing zeroes are omitted).  If  |
     tcl_precision is unset then 6 digits of precision are  used.  |
     To  retain  all of the significant bits of an IEEE floating-  |
     point number set tcl_precision to 17;  if a  value  is  con-  |
     verted  to  string with 17 digits of precision and then con-  |
     verted back  to  binary  for  some  later  calculation,  the  |
     resulting  binary value is guaranteed to be identical to the  |
     original one.




Tcl                     Last change: 7.0                        5






expr(n)               Tcl Built-In Commands               expr(n)



STRING OPERATIONS
     String values may be used  as  operands  of  the  comparison
     operators,  although  the  expression  evaluator tries to do
     comparisons as integer or floating-point when  it  can.   If
     one  of  the  operands  of  a comparison is a string and the
     other has a numeric value, the numeric operand is  converted
     back to a string using the C _s_p_r_i_n_t_f format specifier %d for
     integers and %g for floating-point values.  For example, the
     commands

          expr {"0x03" > "2"}
          expr {"0y" < "0x12"}

     both return 1.  The first comparison is done  using  integer
     comparison,  and  the second is done using string comparison
     after the second operand is converted to the string ``18''.


KEYWORDS
     arithmetic, boolean, compare, expression



































Tcl                     Last change: 7.0                        6




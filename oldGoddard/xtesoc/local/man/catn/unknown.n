


unknown(n)            Tcl Built-In Commands            unknown(n)



_________________________________________________________________

NAME
     unknown - Handle attempts to use non-existent commands

SYNOPSIS
     unknown _c_m_d_N_a_m_e ?_a_r_g _a_r_g ...?
_________________________________________________________________


DESCRIPTION
     This command doesn't actually exist as part of Tcl, but  Tcl
     will  invoke  it  if  it does exist.  If the Tcl interpreter
     encounters a command name for which there is not  a  defined
     command,  then  Tcl  checks  for  the existence of a command
     named unknown.  If there is no such command, then the inter-
     preter  returns  an  error.   If the unknown command exists,
     then it is invoked with arguments consisting of  the  fully-
     substituted name and arguments for the original non-existent
     command.  The unknown command  typically  does  things  like
     searching  through  library  directories  for a command pro-
     cedure with the name _c_m_d_N_a_m_e, or expanding abbreviated  com-
     mand   names  to  full-length,  or  automatically  executing
     unknown commands as sub-processes.  In some cases  (such  as
     expanding  abbreviations)  unknown  will change the original
     command slightly and then (re-)execute it.   The  result  of
     the  unknown  command is used as the result for the original
     non-existent command.


KEYWORDS
     error, non-existent command























Tcl                      Last change:                           1




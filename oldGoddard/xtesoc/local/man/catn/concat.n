


concat(n)             Tcl Built-In Commands             concat(n)



_________________________________________________________________

NAME
     concat - Join lists together

SYNOPSIS
     concat ?_a_r_g _a_r_g ...?                                          |
_________________________________________________________________


DESCRIPTION
     This command treats each argument as a list and concatenates
     them  into  a  single  list.  It also eliminates leading and
     trailing spaces in the _a_r_g's and  adds  a  single  separator
     space  between  _a_r_g's.   It permits any number of arguments.
     For example, the command

          concat a b {c d e} {f {g h}}

     will return

          a b c d e f {g h}

     as its result.

     If no _a_r_gs are supplied, the result is an empty string.       |


KEYWORDS
     concatenate, join, lists

























Tcl                      Last change:                           1




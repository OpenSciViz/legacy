


winfo(n)                   Tk Commands                   winfo(n)



_________________________________________________________________

NAME
     winfo - Return window-related information

SYNOPSIS
     winfo _o_p_t_i_o_n ?_a_r_g _a_r_g ...?
_________________________________________________________________


DESCRIPTION
     The winfo command is used to retrieve information about win-
     dows  managed  by  Tk.   It can take any of a number of dif-
     ferent forms, depending on the _o_p_t_i_o_n argument.   The  legal
     forms are:

     winfo atom _n_a_m_e
          Returns a decimal string giving the integer  identifier
          for  the  atom  whose  name is _n_a_m_e.  If no atom exists
          with the name _n_a_m_e then a new one is created.

     winfo atomname _i_d
          Returns the textual name for  the  atom  whose  integer
          identifier  is  _i_d.  This command is the inverse of the
          winfo atom command.  Generates an error if no such atom
          exists.

     winfo cells _w_i_n_d_o_w
          Returns a decimal string giving the number of cells  in
          the color map for _w_i_n_d_o_w.

     winfo children _w_i_n_d_o_w
          Returns a list containing the path  names  of  all  the
          children  of _w_i_n_d_o_w.  Top-level windows are returned as
          children of their logical parents.

     winfo class _w_i_n_d_o_w
          Returns the class name for _w_i_n_d_o_w.

     winfo containing _r_o_o_t_X _r_o_o_t_Y
          Returns the path name for  the  window  containing  the
          point  given  by  _r_o_o_t_X and _r_o_o_t_Y.  _R_o_o_t_X and _r_o_o_t_Y are
          specified in screen units (i.e.  any form acceptable to
          Tk_GetPixels) in the coordinate system of the root win-
          dow (if a virtual-root window manager is  in  use  then
          the  coordinate  system  of  the virtual root window is
          used).  If no window in this application  contains  the
          point  then  an empty string is returned.  In selecting
          the containing window, children are given higher prior-
          ity  than parents and among siblings the highest one in
          the stacking order is chosen.




Tk                      Last change: 4.0                        1






winfo(n)                   Tk Commands                   winfo(n)



     winfo depth _w_i_n_d_o_w
          Returns a decimal string giving  the  depth  of  _w_i_n_d_o_w
          (number of bits per pixel).

     winfo exists _w_i_n_d_o_w
          Returns 1 if there exists a window named _w_i_n_d_o_w,  0  if
          no such window exists.

     winfo fpixels _w_i_n_d_o_w _n_u_m_b_e_r
          Returns a floating-point value  giving  the  number  of
          pixels in _w_i_n_d_o_w corresponding to the distance given by
          _n_u_m_b_e_r.  _N_u_m_b_e_r may be specified in any  of  the  forms
          acceptable  to  Tk_GetScreenMM,  such  as  ``2.0c''  or
          ``1i''.  The return value may be  fractional;   for  an
          integer value, use winfo pixels.

     winfo geometry _w_i_n_d_o_w
          Returns  the  geometry  for   _w_i_n_d_o_w,   in   the   form
          _w_i_d_t_hx_h_e_i_g_h_t+_x+_y.  All dimensions are in pixels.

     winfo height _w_i_n_d_o_w
          Returns a decimal string giving _w_i_n_d_o_w's height in pix-
          els.  When a window is first created its height will be
          1 pixel;  the height will eventually be  changed  by  a
          geometry manager to fulfill the window's needs.  If you
          need the  true  height  immediately  after  creating  a
          widget,  invoke update to force the geometry manager to
          arrange it, or use winfo reqheight to get the  window's
          requested height instead of its actual height.

     winfo id _w_i_n_d_o_w
          Returns a hexadecimal string indicating the X  identif-
          ier for _w_i_n_d_o_w.

     winfo interps
          Returns a list whose members are the names of  all  Tcl
          interpreters (e.g. all Tk-based applications) currently
          registered for the display of the invoking application.

     winfo ismapped _w_i_n_d_o_w
          Returns 1 if _w_i_n_d_o_w is currently mapped, 0 otherwise.

     winfo manager _w_i_n_d_o_w
          Returns the name  of  the  geometry  manager  currently  |
          responsible  for  _w_i_n_d_o_w,  or an empty string if _w_i_n_d_o_w  |
          isn't managed by any geometry  manager.   The  name  is  |
          usually  the  name  of the Tcl command for the geometry  |
          manager, such  as  pack  or  place.   If  the  geometry  |
          manager is a widget, such as canvases or text, the name  |
          is the widget's class command, such as canvas.

     winfo name _w_i_n_d_o_w



Tk                      Last change: 4.0                        2






winfo(n)                   Tk Commands                   winfo(n)



          Returns _w_i_n_d_o_w's name (i.e. its name within its parent,
          as  opposed  to its full path name).  The command winfo
          name . will return the name of the application.

     winfo parent _w_i_n_d_o_w
          Returns the path name of _w_i_n_d_o_w's parent, or  an  empty
          string if _w_i_n_d_o_w is the main window of the application.

     winfo pathname _i_d
          Returns the path name of the window whose X  identifier
          is  _i_d.   _I_d  must  be a decimal, hexadecimal, or octal
          integer and must correspond to a window in the invoking
          application.

     winfo pixels _w_i_n_d_o_w _n_u_m_b_e_r
          Returns the number of pixels in _w_i_n_d_o_w corresponding to
          the  distance given by _n_u_m_b_e_r.  _N_u_m_b_e_r may be specified
          in any of the forms acceptable to Tk_GetPixels, such as
          ``2.0c''  or  ``1i''.   The  result  is  rounded to the
          nearest integer value;  for a  fractional  result,  use
          winfo fpixels.

     winfo reqheight _w_i_n_d_o_w
          Returns a  decimal  string  giving  _w_i_n_d_o_w's  requested
          height,  in pixels.  This is the value used by _w_i_n_d_o_w's
          geometry manager to compute its geometry.

     winfo reqwidth _w_i_n_d_o_w
          Returns a  decimal  string  giving  _w_i_n_d_o_w's  requested
          width,  in  pixels.  This is the value used by _w_i_n_d_o_w's
          geometry manager to compute its geometry.

     winfo rgb _w_i_n_d_o_w _c_o_l_o_r
          Returns a list containing three decimal  values,  which
          are   the   red,   green,  and  blue  intensities  that
          correspond to _c_o_l_o_r in  the  window  given  by  _w_i_n_d_o_w.
          _C_o_l_o_r  may  be specified in any of the forms acceptable
          for a color option.

     winfo rootx _w_i_n_d_o_w
          Returns a decimal string giving  the  x-coordinate,  in
          the root window of the screen, of the upper-left corner
          of _w_i_n_d_o_w's border (or _w_i_n_d_o_w if it has no border).

     winfo rooty _w_i_n_d_o_w
          Returns a decimal string giving  the  y-coordinate,  in
          the root window of the screen, of the upper-left corner
          of _w_i_n_d_o_w's border (or _w_i_n_d_o_w if it has no border).

     winfo screen _w_i_n_d_o_w
          Returns the name of the screen associated with  _w_i_n_d_o_w,
          in the form _d_i_s_p_l_a_y_N_a_m_e._s_c_r_e_e_n_I_n_d_e_x.



Tk                      Last change: 4.0                        3






winfo(n)                   Tk Commands                   winfo(n)



     winfo screencells _w_i_n_d_o_w
          Returns a decimal string giving the number of cells  in
          the default color map for _w_i_n_d_o_w's screen.

     winfo screendepth _w_i_n_d_o_w
          Returns a decimal string giving the depth of  the  root
          window of _w_i_n_d_o_w's screen (number of bits per pixel).

     winfo screenheight _w_i_n_d_o_w
          Returns a decimal string giving the height of  _w_i_n_d_o_w's
          screen, in pixels.

     winfo screenmmheight _w_i_n_d_o_w
          Returns a decimal string giving the height of  _w_i_n_d_o_w's
          screen, in millimeters.

     winfo screenmmwidth _w_i_n_d_o_w
          Returns a decimal string giving the width  of  _w_i_n_d_o_w's
          screen, in millimeters.

     winfo screenvisual _w_i_n_d_o_w
          Returns one of the following strings  to  indicate  the
          default  visual class for _w_i_n_d_o_w's screen: directcolor,
          grayscale,  pseudocolor,  staticcolor,  staticgray,  or
          truecolor.

     winfo screenwidth _w_i_n_d_o_w
          Returns a decimal string giving the width  of  _w_i_n_d_o_w's
          screen, in pixels.

     winfo toplevel _w_i_n_d_o_w
          Returns the path name of the top-level window  contain-
          ing _w_i_n_d_o_w.

     winfo visual _w_i_n_d_o_w
          Returns one of the following strings  to  indicate  the
          visual  class for _w_i_n_d_o_w: directcolor, grayscale, pseu-
          docolor, staticcolor, staticgray, or truecolor.

     winfo visualsavailable _w_i_n_d_o_w
          Returns a list  whose  elements  describe  the  visuals  |
          available  for  _w_i_n_d_o_w's screen.  Each element consists  |
          of a visual class followed by an  integer  depth.   The  |
          class  has  the  same form as returned by winfo visual.  |
          The depth gives the number of bits  per  pixel  in  the  |
          visual.

     winfo vrootheight _w_i_n_d_o_w
          Returns the height of the virtual root  window  associ-
          ated  with  _w_i_n_d_o_w  if there is one;  otherwise returns
          the height of _w_i_n_d_o_w's screen.




Tk                      Last change: 4.0                        4






winfo(n)                   Tk Commands                   winfo(n)



     winfo vrootwidth _w_i_n_d_o_w
          Returns the width of the virtual root window associated
          with  _w_i_n_d_o_w  if  there  is one;  otherwise returns the
          width of _w_i_n_d_o_w's screen.

     winfo vrootx _w_i_n_d_o_w
          Returns the x-offset of the virtual root window associ-
          ated  with  _w_i_n_d_o_w,  relative to the root window of its
          screen.  This is  normally  either  zero  or  negative.
          Returns  0  if there is no virtual root window for _w_i_n_-
          _d_o_w.

     winfo vrooty _w_i_n_d_o_w
          Returns the y-offset of the virtual root window associ-
          ated  with  _w_i_n_d_o_w,  relative to the root window of its
          screen.  This is  normally  either  zero  or  negative.
          Returns  0  if there is no virtual root window for _w_i_n_-
          _d_o_w.

     winfo width _w_i_n_d_o_w
          Returns a decimal string giving _w_i_n_d_o_w's width in  pix-
          els.   When a window is first created its width will be
          1 pixel;  the width will eventually  be  changed  by  a
          geometry manager to fulfill the window's needs.  If you
          need  the  true  width  immediately  after  creating  a
          widget,  invoke update to force the geometry manager to
          arrange it, or use winfo reqwidth to get  the  window's
          requested width instead of its actual width.

     winfo x _w_i_n_d_o_w
          Returns a decimal string giving  the  x-coordinate,  in
          _w_i_n_d_o_w's  parent,  of the upper-left corner of _w_i_n_d_o_w's
          border (or _w_i_n_d_o_w if it has no border).

     winfo y _w_i_n_d_o_w
          Returns a decimal string giving  the  y-coordinate,  in
          _w_i_n_d_o_w's  parent,  of the upper-left corner of _w_i_n_d_o_w's
          border (or _w_i_n_d_o_w if it has no border).


KEYWORDS
     atom, children, class, geometry, height, identifier,  infor-
     mation,  interpreters,  mapped,  parent,  path name, screen,
     virtual root, width, window











Tk                      Last change: 4.0                        5




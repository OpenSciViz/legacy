


label(n)                   Tk Commands                   label(n)



_________________________________________________________________

NAME
     label - Create and manipulate label widgets

SYNOPSIS
     label _p_a_t_h_N_a_m_e ?_o_p_t_i_o_n_s?

STANDARD OPTIONS
     anchor          font           justify         textVariable   |
     background      foreground     padX            underline      |
     bitmap          highlightColor padY            wrapLength     |
     borderWidth     highlightThickness             relief         |
     cursor          image          text

     See the ``options'' manual entry for details on the standard
     options.

WIDGET-SPECIFIC OPTIONS
     Name:           height
     Class:          Height
     Command-Line Switch:           -height

          Specifies a desired height for the label.  If an  image
          or  bitmap  is  being  displayed  in the label then the
          value is in screen units (i.e. any of the forms accept-
          able to Tk_GetPixels); for text it is in lines of text.
          If this option isn't  specified,  the  label's  desired
          height is computed from the size of the image or bitmap
          or text being displayed in it.

     Name:           width
     Class:          Width
     Command-Line Switch:           -width

          Specifies a desired width for the label.  If  an  image
          or  bitmap  is  being  displayed  in the label then the
          value is in screen units (i.e. any of the forms accept-
          able  to  Tk_GetPixels);  for text it is in characters.
          If this option isn't  specified,  the  label's  desired
          width  is computed from the size of the image or bitmap
          or text being displayed in it.
_________________________________________________________________


DESCRIPTION
     The label command creates a new window (given by  the  _p_a_t_h_-
     _N_a_m_e argument) and makes it into a label widget.  Additional
     options, described above, may be specified  on  the  command
     line  or  in the option database to configure aspects of the
     label such as its colors, font, text,  and  initial  relief.
     The  label  command  returns  its _p_a_t_h_N_a_m_e argument.  At the



Tk                      Last change: 4.0                        1






label(n)                   Tk Commands                   label(n)



     time this command is invoked, there must not exist a  window
     named _p_a_t_h_N_a_m_e, but _p_a_t_h_N_a_m_e's parent must exist.

     A label is a widget that displays a textual  string,  bitmap  |
     or  image.  If text is displayed, it must all be in a single  |
     font, but it can occupy multiple lines on the screen (if  it  |
     contains  newlines or if wrapping occurs because of the wra-  |
     pLength option) and one of the characters may optionally  be  |
     underlined  using  the  underline  option.  The label can be
     manipulated in a few  simple  ways,  such  as  changing  its
     relief or text, using the commands described below.


WIDGET COMMAND
     The label command creates a new Tcl command  whose  name  is
     _p_a_t_h_N_a_m_e.  This command may be used to invoke various opera-
     tions on the widget.  It has the following general form:

          _p_a_t_h_N_a_m_e _o_p_t_i_o_n ?_a_r_g _a_r_g ...?

     _O_p_t_i_o_n and the _a_r_gs determine the exact behavior of the com-
     mand.   The  following commands are possible for label widg-
     ets:

     _p_a_t_h_N_a_m_e cget _o_p_t_i_o_n
          Returns the current value of the  configuration  option  |
          given  by  _o_p_t_i_o_n.   _O_p_t_i_o_n  may have any of the values  |
          accepted by the label command.

     _p_a_t_h_N_a_m_e configure ?_o_p_t_i_o_n? ?_v_a_l_u_e _o_p_t_i_o_n _v_a_l_u_e ...?
          Query  or  modify  the  configuration  options  of  the
          widget.   If  no  _o_p_t_i_o_n  is  specified, returns a list
          describing all of the available  options  for  _p_a_t_h_N_a_m_e
          (see  Tk_ConfigureInfo for information on the format of
          this list).  If _o_p_t_i_o_n is specified with no _v_a_l_u_e, then
          the  command  returns  a  list describing the one named
          option (this list will be identical to the  correspond-
          ing  sublist  of  the  value  returned  if no _o_p_t_i_o_n is
          specified).  If one or more  _o_p_t_i_o_n - _v_a_l_u_e  pairs  are
          specified,  then  the command modifies the given widget
          option(s) to have the given value(s);  in this case the
          command  returns  an empty string.  _O_p_t_i_o_n may have any
          of the values accepted by the label command.


BINDINGS
     When a new label is created, it has no default  event  bind-
     ings:  labels are not intended to be interactive.


KEYWORDS
     label, widget



Tk                      Last change: 4.0                        2




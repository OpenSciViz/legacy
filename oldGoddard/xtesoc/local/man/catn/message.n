


message(n)                 Tk Commands                 message(n)



_________________________________________________________________

NAME
     message - Create and manipulate message widgets

SYNOPSIS
     message _p_a_t_h_N_a_m_e ?_o_p_t_i_o_n_s?

STANDARD OPTIONS
     anchor          font           padX            textVariable
     background      foreground     padY            width
     borderWidth     highlightColor relief                         |
     cursor          highlightThickness             text

     See the ``options'' manual entry for details on the standard
     options.

WIDGET-SPECIFIC OPTIONS
     Name:           aspect
     Class:          Aspect
     Command-Line Switch:-aspect

          Specifies  a  non-negative  integer  value   indicating
          desired aspect ratio for the text.  The aspect ratio is
          specified as  100*width/height.   100  means  the  text
          should  be  as  wide  as it is tall, 200 means the text
          should be twice as wide as it is  tall,  50  means  the
          text  should be twice as tall as it is wide, and so on.
          Used to choose line length for  text  if  width  option
          isn't specified.  Defaults to 150.

     Name:           justify
     Class:          Justify
     Command-Line Switch:-justify

          Specifies how to justify lines of text.  Must be one of
          left, center, or right.  Defaults to left.  This option
          works together with the anchor, aspect, padX, padY, and
          width  options  to provide a variety of arrangements of
          the text within  the  window.   The  aspect  and  width
          options  determine the amount of screen space needed to
          display the text.  The anchor, padX, and  padY  options
          determine  where  this  rectangular  area  is displayed
          within the widget's  window,  and  the  justify  option
          determines  how each line is displayed within that rec-
          tangular region.  For example, suppose anchor is e  and
          justify  is  left,  and that the message window is much
          larger than needed for the text.   The  the  text  will
          displayed  so that the left edges of all the lines line
          up and the right edge of the longest line is padX  from
          the  right  side  of the window;  the entire text block
          will be centered in the vertical span of the window.



Tk                      Last change: 4.0                        1






message(n)                 Tk Commands                 message(n)



     Name:           width
     Class:          Width
     Command-Line Switch:-width

          Specifies the length of lines in the window.  The value
          may  have  any of the forms acceptable to Tk_GetPixels.
          If this option has a value greater than zero  then  the
          aspect  option  is  ignored and the width option deter-
          mines the line length.  If this option has a value less
          than  or  equal  to zero, then the aspect option deter-
          mines the line length.
_________________________________________________________________


DESCRIPTION
     The message command creates a new window (given by the _p_a_t_h_-
     _N_a_m_e  argument)  and  makes it into a message widget.  Addi-
     tional options, described above, may  be  specified  on  the
     command  line or in the option database to configure aspects
     of the message such as its colors, font, text,  and  initial
     relief.   The message command returns its _p_a_t_h_N_a_m_e argument.
     At the time this command is invoked, there must not exist  a
     window named _p_a_t_h_N_a_m_e, but _p_a_t_h_N_a_m_e's parent must exist.

     A message is a widget that displays  a  textual  string.   A
     message widget has three special features.  First, it breaks
     up its string into lines in order to produce a given  aspect
     ratio  for  the  window.  The line breaks are chosen at word
     boundaries wherever possible (if  not  even  a  single  word
     would  fit  on  a  line,  then the word will be split across
     lines).  Newline characters in the string  will  force  line
     breaks;  they can be used, for example, to leave blank lines
     in the display.

     The second feature of a  message  widget  is  justification.
     The  text  may be displayed left-justified (each line starts
     at the left side of the window), centered on a  line-by-line
     basis,  or right-justified (each line ends at the right side
     of the window).

     The third feature of a message widget  is  that  it  handles
     control  characters  and  non-printing characters specially.
     Tab characters are replaced with enough blank space to  line
     up  on  the  next 8-character boundary.  Newlines cause line
     breaks.  Other control  characters  (ASCII  code  less  than
     0x20)  and  characters not defined in the font are displayed
     as a four-character sequence \x_h_h where _h_h is the  two-digit
     hexadecimal  number  corresponding to the character.  In the
     unusual case where the font doesn't contain all of the char-
     acters in ``0123456789abcdef\x'' then control characters and
     undefined characters are not displayed at all.




Tk                      Last change: 4.0                        2






message(n)                 Tk Commands                 message(n)



WIDGET COMMAND
     The message command creates a new Tcl command whose name  is
     _p_a_t_h_N_a_m_e.  This command may be used to invoke various opera-
     tions on the widget.  It has the following general form:

          _p_a_t_h_N_a_m_e _o_p_t_i_o_n ?_a_r_g _a_r_g ...?

     _O_p_t_i_o_n and the _a_r_gs determine the exact behavior of the com-
     mand.  The following commands are possible for message widg-
     ets:

     _p_a_t_h_N_a_m_e cget _o_p_t_i_o_n
          Returns the current value of the  configuration  option  |
          given  by  _o_p_t_i_o_n.   _O_p_t_i_o_n  may have any of the values  |
          accepted by the message command.

     _p_a_t_h_N_a_m_e configure ?_o_p_t_i_o_n? ?_v_a_l_u_e _o_p_t_i_o_n _v_a_l_u_e ...?
          Query  or  modify  the  configuration  options  of  the
          widget.   If  no  _o_p_t_i_o_n  is  specified, returns a list
          describing all of the available  options  for  _p_a_t_h_N_a_m_e
          (see  Tk_ConfigureInfo for information on the format of
          this list).  If _o_p_t_i_o_n is specified with no _v_a_l_u_e, then
          the  command  returns  a  list describing the one named
          option (this list will be identical to the  correspond-
          ing  sublist  of  the  value  returned  if no _o_p_t_i_o_n is
          specified).  If one or more  _o_p_t_i_o_n - _v_a_l_u_e  pairs  are
          specified,  then  the command modifies the given widget
          option(s) to have the given value(s);  in this case the
          command  returns  an empty string.  _O_p_t_i_o_n may have any
          of the values accepted by the message command.


DEFAULT BINDINGS
     When a new message is created, it has no default event bind-
     ings:  messages are intended for output purposes only.


BUGS
     Tabs don't work very well with  text  that  is  centered  or
     right-justified.  The most common result is that the line is
     justified wrong.


KEYWORDS
     message, widget










Tk                      Last change: 4.0                        3




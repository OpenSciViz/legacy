


toplevel(n)                Tk Commands                toplevel(n)



_________________________________________________________________

NAME
     toplevel - Create and manipulate toplevel widgets

SYNOPSIS
     toplevel _p_a_t_h_N_a_m_e ?_o_p_t_i_o_n_s?

STANDARD OPTIONS
     borderWidth     highlightColor relief                         |
     cursor          highlightThickness

     See the ``options'' manual entry for details on the standard
     options.

WIDGET-SPECIFIC OPTIONS
     Name:           background                                    |
     Class:          Background                                    |
     Command-Line Switch:           -background                    |

                                                                |
          |                                                        |
          This option is the  same  as  the  standard  background  |
          option  except  that its value may also be specified as  |
          an empty string.  In this case, the widget will display  |
          no background or border, and no colors will be consumed  |
          from its colormap for its background and border.         |

     Name:           class                                         |
     Class:          Class                                         |
     Command-Line Switch:           -class                         |

                                                                |
          |                                                        |
          Specifies a class for the window.  This class  will  be  |
          used when querying the option database for the window's  |
          other options, and it will also be used later for other  |
          purposes such as bindings.  The class option may not be  |
          changed with the configure widget command.

     Name:           colormap
     Class:          Colormap
     Command-Line Switch:           -colormap

          Specifies a colormap to use for the window.  The  value
          may  be  either  new,  in  which case a new colormap is
          created for the window and its children, or the name of
          another  window  (which  must be on the same screen and
          have the same visual as _p_a_t_h_N_a_m_e), in  which  case  the
          new  window  will  use  the colormap from the specified
          window.  If the colormap option is not  specified,  the
          new  window  uses  the  default colormap of its screen.



Tk                      Last change: 4.0                        1






toplevel(n)                Tk Commands                toplevel(n)



          This option may  not  be  changed  with  the  configure
          widget command.

     Name:           height
     Class:          Height
     Command-Line Switch:           -height

          Specifies the desired height for the window in  any  of
          the  forms  acceptable to Tk_GetPixels.  If this option
          is less than or equal to zero then the window will  not
          request any size at all.

     Name:           _N_o_n_e                                          |
     Class:          _N_o_n_e                                          |
     Command-Line Switch:           -screen                        |

                                                                |
          |                                                        |
          Specifies the screen on which to place the new  window.  |
          Any  valid screen name may be used, even one associated  |
          with a different display.  Defaults to the same  screen  |
          as  its  parent.  This option is special in that it may  |
          not be specified via the option database,  and  it  may  |
          not be modified with the configure widget command.       |

     Name:           visual                                        |
     Class:          Visual                                        |
     Command-Line Switch:           -visual                        |

                                                                |
          |                                                        |
          Specifies visual information for the new window in  any  |
          of  the forms accepted by Tk_GetVisual.  If this option  |
          is not specified, the new window will use  the  default  |
          visual  for  its  screen.  The visual option may not be  |
          modified with the configure widget command.

     Name:           width
     Class:          Width
     Command-Line Switch:           -width

          Specifies the desired width for the window  in  any  of
          the  forms  acceptable to Tk_GetPixels.  If this option
          is less than or equal to zero then the window will  not
          request any size at all.
_________________________________________________________________


DESCRIPTION
     The toplevel command creates a new toplevel widget (given by
     the   _p_a_t_h_N_a_m_e  argument).   Additional  options,  described
     above, may be specified on the command line or in the option



Tk                      Last change: 4.0                        2






toplevel(n)                Tk Commands                toplevel(n)



     database  to  configure  aspects of the toplevel such as its
     background color and relief.  The toplevel  command  returns
     the path name of the new window.

     A toplevel is similar to a frame except that it  is  created
     as a top-level window:  its X parent is the root window of a
     screen rather than the logical parent from  its  path  name.
     The primary purpose of a toplevel is to serve as a container
     for dialog boxes and other collections of widgets.  The only
     visible  features of a toplevel are its background color and
     an optional 3-D border to make the toplevel appear raised or
     sunken.


WIDGET COMMAND
     The toplevel command creates a new Tcl command whose name is
     the  same  as  the path name of the toplevel's window.  This
     command may be used to  invoke  various  operations  on  the
     widget.  It has the following general form:

          _p_a_t_h_N_a_m_e _o_p_t_i_o_n ?_a_r_g _a_r_g ...?

     _P_a_t_h_N_a_m_e is the name of the command, which is  the  same  as
     the toplevel widget's path name.  _O_p_t_i_o_n and the _a_r_gs deter-
     mine the exact behavior of the command.  The following  com-
     mands are possible for toplevel widgets:

     _p_a_t_h_N_a_m_e cget _o_p_t_i_o_n
          Returns the current value of the  configuration  option  |
          given  by  _o_p_t_i_o_n.   _O_p_t_i_o_n  may have any of the values  |
          accepted by the toplevel command.

     _p_a_t_h_N_a_m_e configure ?_o_p_t_i_o_n? ?_v_a_l_u_e _o_p_t_i_o_n _v_a_l_u_e ...?
          Query  or  modify  the  configuration  options  of  the
          widget.   If  no  _o_p_t_i_o_n  is  specified, returns a list
          describing all of the available  options  for  _p_a_t_h_N_a_m_e
          (see  Tk_ConfigureInfo for information on the format of
          this list).  If _o_p_t_i_o_n is specified with no _v_a_l_u_e, then
          the  command  returns  a  list describing the one named
          option (this list will be identical to the  correspond-
          ing  sublist  of  the  value  returned  if no _o_p_t_i_o_n is
          specified).  If one or more  _o_p_t_i_o_n - _v_a_l_u_e  pairs  are
          specified,  then  the command modifies the given widget
          option(s) to have the given value(s);  in this case the
          command  returns  an empty string.  _O_p_t_i_o_n may have any
          of the values accepted by the toplevel command.


BINDINGS
     When a new toplevel is created,  it  has  no  default  event
     bindings:  toplevels are not intended to be interactive.




Tk                      Last change: 4.0                        3






toplevel(n)                Tk Commands                toplevel(n)



KEYWORDS
     toplevel, widget





















































Tk                      Last change: 4.0                        4







trace(n)              Tcl Built-In Commands              trace(n)



_________________________________________________________________

NAME
     trace - Monitor variable accesses

SYNOPSIS
     trace _o_p_t_i_o_n ?_a_r_g _a_r_g ...?
_________________________________________________________________


DESCRIPTION
     This command causes Tcl commands  to  be  executed  whenever
     certain  operations  are invoked.  At present, only variable
     tracing is implemented. The legal  _o_p_t_i_o_n's  (which  may  be
     abbreviated) are:

     trace variable _n_a_m_e _o_p_s _c_o_m_m_a_n_d
          Arrange for _c_o_m_m_a_n_d to be  executed  whenever  variable
          _n_a_m_e is accessed in one of the ways given by _o_p_s.  _N_a_m_e
          may refer to a normal variable, an element of an array,
          or  to  an  array as a whole (i.e. _n_a_m_e may be just the
          name of an array, with  no  parenthesized  index).   If
          _n_a_m_e  refers  to a whole array, then _c_o_m_m_a_n_d is invoked
          whenever any element of the array is manipulated.

          _O_p_s indicates which operations  are  of  interest,  and
          consists of one or more of the following letters:

               r    Invoke _c_o_m_m_a_n_d whenever the variable is read.

               w    Invoke _c_o_m_m_a_n_d whenever the variable is writ-
                    ten.

               u    Invoke  _c_o_m_m_a_n_d  whenever  the  variable   is
                    unset.   Variables  can  be  unset explicitly
                    with the unset command,  or  implicitly  when
                    procedures  return  (all of their local vari-
                    ables are unset).  Variables are  also  unset
                    when  interpreters  are  deleted,  but traces
                    will not  be  invoked  because  there  is  no
                    interpreter in which to execute them.

          When the trace triggers, three arguments  are  appended
          to _c_o_m_m_a_n_d so that the actual command is as follows:

               _c_o_m_m_a_n_d _n_a_m_e_1 _n_a_m_e_2 _o_p

          _N_a_m_e_1 and _n_a_m_e_2 give the name(s) for the variable being
          accessed:  if the variable is a scalar then _n_a_m_e_1 gives
          the variable's name and _n_a_m_e_2 is an  empty  string;  if
          the  variable  is an array element then _n_a_m_e_1 gives the
          name of the array and name2 gives the  index  into  the



Tcl                      Last change:                           1






trace(n)              Tcl Built-In Commands              trace(n)



          array;  if  an  entire  array  is being deleted and the
          trace was registered on the overall array, rather  than
          a  single  element, then _n_a_m_e_1 gives the array name and
          _n_a_m_e_2 is an empty string.  _O_p indicates what  operation
          is being performed on the variable, and is one of r, w,
          or u as defined above.

          _C_o_m_m_a_n_d executes in the same context as the  code  that
          invoked  the  traced  operation:   if  the variable was
          accessed as part of a Tcl procedure, then _c_o_m_m_a_n_d  will
          have  access to the same local variables as code in the
          procedure.  This context may be different than the con-
          text  in  which  the  trace  was  created.   If _c_o_m_m_a_n_d
          invokes a procedure (which it normally does)  then  the
          procedure  will  have  to  use  upvar  or uplevel if it
          wishes to access the traced variable.  Note  also  that
          _n_a_m_e_1  may not necessarily be the same as the name used
          to set the trace  on  the  variable;   differences  can
          occur  if the access is made through a variable defined
          with the upvar command.

          For read and write traces, _c_o_m_m_a_n_d can modify the vari-
          able  to affect the result of the traced operation.  If
          _c_o_m_m_a_n_d modifies the value of a variable during a  read
          or  write trace, then the new value will be returned as
          the result of the traced operation.  The  return  value
          from   _c_o_m_m_a_n_d  is ignored except that if it returns an
          error of  any  sort  then  the  traced  operation  also
          returns  an  error with the same error message returned  |
          by the trace command (this mechanism  can  be  used  to
          implement read-only variables, for example).  For write
          traces, _c_o_m_m_a_n_d is invoked after the  variable's  value
          has  been  changed;  it  can write a new value into the
          variable to override the original  value  specified  in
          the write operation.  To implement read-only variables,
          _c_o_m_m_a_n_d will have to restore the old value of the vari-
          able.

          While _c_o_m_m_a_n_d is  executing  during  a  read  or  write
          trace, traces on the variable are temporarily disabled.
          This means that reads and  writes  invoked  by  _c_o_m_m_a_n_d
          will  occur  directly, without invoking _c_o_m_m_a_n_d (or any
          other traces) again.  However, if  _c_o_m_m_a_n_d  unsets  the  |
          variable then unset traces will be invoked.

          When an  unset  trace  is  invoked,  the  variable  has
          already  been  deleted:  it will appear to be undefined
          with no traces.  If an unset occurs because of  a  pro-
          cedure  return,  then  the trace will be invoked in the
          variable context of the procedure  being  returned  to:
          the  stack  frame  of  the  returning procedure will no
          longer exist.  Traces are  not  disabled  during  unset



Tcl                      Last change:                           2






trace(n)              Tcl Built-In Commands              trace(n)



          traces,  so  if  an  unset  trace command creates a new
          trace and accesses the  variable,  the  trace  will  be
          invoked.  Any errors in unset traces are ignored.        |

          If there are multiple traces on  a  variable  they  are
          invoked  in  order  of creation, most-recent first.  If
          one trace returns an error, then no further traces  are
          invoked  for  the  variable.  If an array element has a
          trace set, and there is also a trace set on  the  array
          as  a  whole, the trace on the overall array is invoked
          before the one on the element.

          Once created, the trace remains in effect either  until
          the  trace  is  removed  with the trace vdelete command
          described below, until the variable is unset, or  until
          the  interpreter  is  deleted.  Unsetting an element of
          array will remove any traces on that element, but  will
          not remove traces on the overall array.

          This command returns an empty string.

     trace vdelete _n_a_m_e _o_p_s _c_o_m_m_a_n_d
          If there is a trace  set  on  variable  _n_a_m_e  with  the
          operations  and  command given by _o_p_s and _c_o_m_m_a_n_d, then
          the trace is removed, so that _c_o_m_m_a_n_d will never  again
          be invoked.  Returns an empty string.

     trace vinfo _n_a_m_e
          Returns a list containing one element  for  each  trace
          currently  set  on  variable _n_a_m_e.  Each element of the
          list is itself a list containing  two  elements,  which
          are  the _o_p_s and _c_o_m_m_a_n_d associated with the trace.  If
          _n_a_m_e doesn't exist or doesn't have any traces set, then
          the result of the command will be an empty string.


KEYWORDS
     read, variable, write, trace, unset

















Tcl                      Last change:                           3




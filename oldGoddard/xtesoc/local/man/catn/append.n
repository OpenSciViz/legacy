


append(n)             Tcl Built-In Commands             append(n)



_________________________________________________________________

NAME
     append - Append to variable

SYNOPSIS
     append _v_a_r_N_a_m_e ?_v_a_l_u_e _v_a_l_u_e _v_a_l_u_e ...?
_________________________________________________________________


DESCRIPTION
     Append all of the _v_a_l_u_e arguments to the  current  value  of
     variable  _v_a_r_N_a_m_e.   If _v_a_r_N_a_m_e doesn't exist, it is given a
     value equal to the concatenation of all the _v_a_l_u_e arguments.
     This  command  provides  an  efficient  way to build up long
     variables incrementally.  For example, ``append  a  $b''  is
     much more efficient than ``set a $a$b'' if $a is long.


KEYWORDS
     append, variable


































Tcl                      Last change:                           1







regsub(n)             Tcl Built-In Commands             regsub(n)



_________________________________________________________________

NAME
     regsub - Perform substitutions based on  regular  expression
     pattern matching

SYNOPSIS
     regsub ?_s_w_i_t_c_h_e_s? _e_x_p _s_t_r_i_n_g _s_u_b_S_p_e_c _v_a_r_N_a_m_e
_________________________________________________________________


DESCRIPTION
     This command matches  the  regular  expression  _e_x_p  against
     _s_t_r_i_n_g,  and  it copies _s_t_r_i_n_g to the variable whose name is  |
     given by _v_a_r_N_a_m_e.  If there is a match, then  while  copying  |
     _s_t_r_i_n_g  to _v_a_r_N_a_m_e the portion of _s_t_r_i_n_g that matched _e_x_p is
     replaced with _s_u_b_S_p_e_c.   If  _s_u_b_S_p_e_c  contains  a  ``&''  or
     ``\0'',  then  it  is  replaced in the substitution with the
     portion of _s_t_r_i_n_g that matched _e_x_p.  If _s_u_b_S_p_e_c  contains  a
     ``\_n'',  where  _n  is  a  digit  between 1 and 9, then it is
     replaced in the substitution with the portion of _s_t_r_i_n_g that
     matched  the _n-th parenthesized subexpression of _e_x_p.  Addi-
     tional backslashes may be used in _s_u_b_S_p_e_c to prevent special
     interpretation  of  ``&''  or ``\0'' or ``\_n'' or backslash.
     The use of backslashes in _s_u_b_S_p_e_c tends  to  interact  badly
     with  the Tcl parser's use of backslashes, so it's generally
     safest  to  enclose  _s_u_b_S_p_e_c  in  braces  if   it   includes
     backslashes.

     If the initial arguments to regexp start with  -  then  they  |
     are   treated  as  switches.   The  following  switches  are  |
     currently supported:                                          |

                                                                -                                                             |
               all                                              |
               |                                                   |
               All ranges in _s_t_r_i_n_g that match _e_x_p are found  and  |
               substitution   is  performed  for  each  of  these  |
               ranges.  Without this switch only the first match-  |
               ing  range  is  found and substituted.  If -all is  |
               specified, then ``&''  and  ``\_n''  sequences  are  |
               handled  for  each substitution using the informa-  |
               tion from the corresponding match.                  |

                                                                -                                                             |
               nocase                                           |
               |                                                   |
               Upper-case characters in _s_t_r_i_n_g will be  converted  |
               to  lower-case  before matching against _e_x_p;  how-  |
               ever, substitutions specified by _s_u_b_S_p_e_c  use  the  |
               original unconverted form of _s_t_r_i_n_g.                |




Tcl                     Last change: 7.4                        1






regsub(n)             Tcl Built-In Commands             regsub(n)



                                  -                                                             |
               -                                                |
               |                                                   |
               Marks the end of switches.  The argument following  |
               this  one will be treated as _e_x_p even if it starts  |
               with a -.

     The command returns a count of the number of matching ranges  |
     that  were  found  and  replaced.   See the manual entry for
     regexp for details on the interpretation of regular  expres-
     sions.


KEYWORDS
     match, pattern, regular expression, substitute








































Tcl                     Last change: 7.4                        2




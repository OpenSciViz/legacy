


proc(n)               Tcl Built-In Commands               proc(n)



_________________________________________________________________

NAME
     proc - Create a Tcl procedure

SYNOPSIS
     proc _n_a_m_e _a_r_g_s _b_o_d_y
_________________________________________________________________


DESCRIPTION
     The proc command creates a new  Tcl  procedure  named  _n_a_m_e,
     replacing  any  existing command or procedure there may have
     been by that name.  Whenever the new command is invoked, the
     contents  of  _b_o_d_y  will be executed by the Tcl interpreter.
     _A_r_g_s specifies the formal arguments to  the  procedure.   It
     consists  of  a list, possibly empty, each of whose elements
     specifies one argument.  Each argument specifier is  also  a
     list with either one or two fields.  If there is only a sin-
     gle field in the specifier then it is the name of the  argu-
     ment;  if  there are two fields, then the first is the argu-
     ment name and the second is its default value.

     When _n_a_m_e is invoked a local variable will  be  created  for
     each  of  the  formal  arguments to the procedure; its value
     will be the value of corresponding argument in the  invoking
     command  or  the  argument's  default value.  Arguments with
     default values need not be specified in a procedure  invoca-
     tion.   However,  there  must be enough actual arguments for
     all the formal arguments that don't have defaults, and there
     must  not  be any extra actual arguments.  There is one spe-
     cial case to permit  procedures  with  variable  numbers  of
     arguments.   If  the last formal argument has the name args,
     then a call to the procedure may contain more  actual  argu-
     ments  than the procedure has formals.  In this case, all of
     the actual arguments starting  at  the  one  that  would  be
     assigned  to  args  are combined into a list (as if the list
     command had been used); this combined value is  assigned  to
     the local variable args.

     When _b_o_d_y is being executed, variable names  normally  refer
     to  local  variables,  which  are created automatically when
     referenced and deleted  when  the  procedure  returns.   One
     local  variable  is  automatically  created  for each of the
     procedure's  arguments.   Global  variables  can   only   be
     accessed  by  invoking  the global command or the upvar com-
     mand.

     The proc command returns an empty string.  When a  procedure
     is invoked, the procedure's return value is the value speci-
     fied in a return command.  If the procedure doesn't  execute
     an  explicit  return,  then its return value is the value of



Tcl                      Last change:                           1






proc(n)               Tcl Built-In Commands               proc(n)



     the last command executed in the procedure's  body.   If  an
     error  occurs  while  executing the procedure body, then the
     procedure-as-a-whole will return that same error.


KEYWORDS
     argument, procedure
















































Tcl                      Last change:                           2




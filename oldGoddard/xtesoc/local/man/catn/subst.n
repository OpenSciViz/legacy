


subst(n)              Tcl Built-In Commands              subst(n)



_________________________________________________________________

NAME
     subst - Perform backslash, command, and  variable  substitu-
     tions

SYNOPSIS
     subst _s_t_r_i_n_g
_________________________________________________________________


DESCRIPTION
     This command performs variable substitutions,  command  sub-
     stitutions,  and  backslash subsitutions on its _s_t_r_i_n_g argu-
     ment and returns the fully-substituted result.  The  substi-
     tutions  are  performed  in  exactly the same way as for Tcl
     commands.  As a result, the _s_t_r_i_n_g argument is actually sub-
     stituted  twice, once by the Tcl parser in the usual fashion
     for Tcl commands, and again by the _s_u_b_s_t command.

     Note: when it performs its  substitutions,  _s_u_b_s_t  does  not
     give any special treatment to double quotes or curly braces.
     For example, the script

          set a 44
          subst {xyz {$a}}

     returns ``xyz {44}'', not ``xyz {$a}''.


KEYWORDS
     backslash substitution, command substitution, variable  sub-
     stitution






















Tcl                     Last change: 7.4                        1




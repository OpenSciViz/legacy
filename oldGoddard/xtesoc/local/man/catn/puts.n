


puts(n)               Tcl Built-In Commands               puts(n)



_________________________________________________________________

NAME
     puts - Write to a file

SYNOPSIS
     puts ?-nonewline? ?_f_i_l_e_I_d? _s_t_r_i_n_g
_________________________________________________________________


DESCRIPTION
     Writes the characters given by _s_t_r_i_n_g to the file  given  by
     _f_i_l_e_I_d.   _F_i_l_e_I_d must have been the return value from a pre-
     vious call to open, or it may be stdout or stderr  to  refer
     to one of the standard I/O channels; it must refer to a file
     that was opened for writing.  If no _f_i_l_e_I_d is specified then
     it  defaults  to  stdout.   Puts  normally outputs a newline
     character after _s_t_r_i_n_g, but this feature may  be  suppressed
     by  specifying  the  - nonewline switch.  Output to files is
     buffered internally by Tcl; the flush command may be used to
     force buffered characters to be output.


KEYWORDS
     file, newline, output, write






























Tcl                      Last change:                           1




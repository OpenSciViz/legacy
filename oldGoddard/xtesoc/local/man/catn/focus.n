


focus(n)                   Tk Commands                   focus(n)



_________________________________________________________________

NAME
     focus - Manage the input focus

SYNOPSIS
     focus
     focus _w_i_n_d_o_w
     focus _o_p_t_i_o_n ?_a_r_g _a_r_g ...?
_________________________________________________________________


DESCRIPTION
     The focus command is used to manage the Tk input focus.   At  |
     any  given time, one window on each display is designated as  |
     the _f_o_c_u_s _w_i_n_d_o_w;  any key press or key release  events  for  |
     the display are sent to that window.  It is up to the window  |
     manager to redirect the focus among the top-level windows of  |
     a  display.  For example, some window managers automatically  |
     set the input focus to a top-level window whenever the mouse  |
     enters  it;   others  redirect the input focus only when the  |
     user clicks on a window.  Usually the  window  manager  will  |
     set  the  focus  only to top-level windows, leaving it up to  |
     the application to redirect the focus among the children  of  |
     the top-level.                                                |

     Tk remembers one focus window for each top-level  (the  most  |
     recent  descendant  of that top-level to receive the focus);  |
     when the window manager gives the focus to a  top-level,  Tk  |
     automatically redirects it to the remembered window.  Within  |
     a top-level Tk uses an  _e_x_p_l_i_c_i_t  focus  model  by  default.  |
     Moving the mouse within a top-level does not normally change  |
     the focus;  the focus changes only  when  a  widget  decides  |
     explicitly  to  claim  the  focus (e.g., because of a button  |
     click), or when the user types a key such as Tab that  moves  |
     the focus.                                                    |

     The Tcl procedure tk_focusFollowsMouse  may  be  invoked  to  |
     create  an _i_m_p_l_i_c_i_t focus model:  it reconfigures Tk so that  |
     the focus is set to a window whenever the mouse  enters  it.  |
     The Tcl procedures tk_focusNext and tk_focusPrev implement a  |
     focus order among the windows of a top-level;  they are used  |
     in  the  default bindings for Tab and Shift-Tab, among other  |
     things.                                                       |

     The focus command can take any of the following forms:        |

     focus                                                      |
          |                                                        |
          Returns the path  name  of  the  focus  window  on  the  |
          display  containing  the application's main window,  or  |
          an empty string if no window in  this  application  has  |



Tk                      Last change: 4.0                        1






focus(n)                   Tk Commands                   focus(n)



          the  focus  on  that  display.   Note:  it is better to  |
          specify the display explicitly using  - displayof  (see  |
          below) so that the code will work in applications using  |
          multiple displays.                                       |

     focus _w_i_n_d_o_w                                               |
          |                                                        |
          Sets the input focus for _w_i_n_d_o_w's display to _w_i_n_d_o_w and  |
          returns  an  empty string.  If _w_i_n_d_o_w isn't viewable at  |
          the time this command is  invoked,  X  will  refuse  to  |
          actually  set  the  focus;  nonetheless, _w_i_n_d_o_w will be  |
          marked by Tk as the focus window for its display.        |

     focus                                                      -                                                       |
          displayof _w_i_n_d_o_w                                      |
          |                                                        |
          Returns the name of the focus  window  on  the  display  |
          containing  _w_i_n_d_o_w.   If  the focus window for _w_i_n_d_o_w's  |
          display isn't in this application, the return value  is  |
          an empty string.                                         |

     focus                                                      -                                                       |
          lastfor _w_i_n_d_o_w                                        |
          |                                                        |
          Returns the name of the most recent window to have  the  |
          input focus among all the windows in the same top-level  |
          as _w_i_n_d_o_w.  If no window in that top-level has ever had  |
          the input focus, or if the most recent focus window has  |
          been  deleted,  then  the  name  of  the  top-level  is  |
          returned.   The  return  value  is the window that will  |
          receive the  input  focus  the  next  time  the  window  |
          manager gives the focus to the top-level.                |


BUGS                                                               |
     When Tk redirects the input focus, either because of a focus  |
     command  or  because  the  window manager set the focus to a  |
     top-level and Tk is redirecting it to an internal window  in  |
     the  top-level, there may already be keyboard events queued.  |
     If this happens, the FocusIn and  FocusOut  events  for  the  |
     focus  change  will not be received until after the keyboard  |
     events;  however, the keyboard events will  be  directed  to  |
     the  new focus window.  This means that a window can receive  |
     keyboard events before receiving a FocusIn event.


KEYWORDS
     events, focus, keyboard, top-level, window manager







Tk                      Last change: 4.0                        2




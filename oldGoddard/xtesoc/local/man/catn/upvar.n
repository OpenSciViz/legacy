


upvar(n)              Tcl Built-In Commands              upvar(n)



_________________________________________________________________

NAME
     upvar - Create link to variable in a different stack frame

SYNOPSIS
     upvar ?_l_e_v_e_l? _o_t_h_e_r_V_a_r _m_y_V_a_r ?_o_t_h_e_r_V_a_r _m_y_V_a_r ...?
_________________________________________________________________


DESCRIPTION
     This command arranges for one or more local variables in the
     current procedure to refer to variables in an enclosing pro-
     cedure call or to global variables.  _L_e_v_e_l may have  any  of
     the  forms  permitted  for  the  uplevel command, and may be
     omitted if the first letter of the first _o_t_h_e_r_V_a_r isn't # or
     a  digit  (it  defaults  to 1).  For each _o_t_h_e_r_V_a_r argument,
     upvar makes the variable by that name in the procedure frame
     given by _l_e_v_e_l (or at global level, if _l_e_v_e_l is #0) accessi-
     ble in the current  procedure  by  the  name  given  in  the
     corresponding  _m_y_V_a_r argument.  The variable named by _o_t_h_e_r_-
     _V_a_r need not exist at the time of  the  call;   it  will  be
     created  the  first  time  _m_y_V_a_r is referenced, just like an
     ordinary variable.  Upvar may only be  invoked  from  within
     procedures.   _M_y_V_a_r may not refer to an element of an array,  |
     but _o_t_h_e_r_V_a_r may refer to an array element.   Upvar  returns
     an empty string.

     The upvar command simplifies the implementation of  call-by-
     name procedure calling and also makes it easier to build new
     control constructs as Tcl procedures.  For example, consider
     the following procedure:

          proc add2 name {
              upvar $name x
              set x [expr $x+2]
          }

     Add2 is invoked with an argument giving the name of a  vari-
     able,  and  it  adds  two  to  the  value  of that variable.
     Although add2 could  have  been  implemented  using  uplevel
     instead  of upvar, upvar makes it simpler for add2 to access
     the variable in the caller's procedure frame.

     If an upvar variable is unset (e.g. x in  add2  above),  the  |
     unset  operation  affects  the variable it is linked to, not  |
     the upvar variable.  There is no way to unset an upvar vari-  |
     able except by exiting the procedure in which it is defined.  |
     However, it is possible to retarget  an  upvar  variable  by  |
     executing another upvar command.





Tcl                      Last change:                           1






upvar(n)              Tcl Built-In Commands              upvar(n)



KEYWORDS
     context, frame, global, level, procedure, variable





















































Tcl                      Last change:                           2




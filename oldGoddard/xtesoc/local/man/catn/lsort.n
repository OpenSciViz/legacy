


lsort(n)              Tcl Built-In Commands              lsort(n)



_________________________________________________________________

NAME
     lsort - Sort the elements of a list

SYNOPSIS
     lsort ?_s_w_i_t_c_h_e_s? _l_i_s_t
_________________________________________________________________


DESCRIPTION
     This command sorts the elements of  _l_i_s_t,  returning  a  new
     list in sorted order.  By default ASCII sorting is used with
     the result returned in increasing order.   However,  any  of  |
     the  following switches may be specified before _l_i_s_t to con-  |
     trol  the  sorting   process   (unique   abbreviations   are  |
     accepted):                                                    |

                                                                -                                                             |
                         ascii                                  |
                         |                                         |
                         Use string comparison with ASCII  colla-  |
                         tion order.  This is the default.         |

                                                                -                                                             |
                         integer                                |
                         |                                         |
                         Convert list elements  to  integers  and  |
                         use integer comparison.                   |

                                                                -                                                             |
                         real                                   |
                         |                                         |
                         Convert list elements to  floating-point  |
                         values and use floating comparison.       |

                                                                -                                                             |
                         command _c_o_m_m_a_n_d                        |
                         |                                         |
                         Use _c_o_m_m_a_n_d as a comparison command.  To  |
                         compare  two  elements,  evaluate  a Tcl  |
                         script consisting of  _c_o_m_m_a_n_d  with  the  |
                         two   elements  appended  as  additional  |
                         arguments.  The script should return  an  |
                         integer  less than, equal to, or greater  |
                         than zero if the first element is to  be  |
                         considered   less  than,  equal  to,  or  |
                         greater than the second, respectively.    |

                              -                          increas-  |
                         ing                                    |
                         |                                         |



Tcl                     Last change: 7.0                        1






lsort(n)              Tcl Built-In Commands              lsort(n)



                         Sort  the  list  in   increasing   order  |
                         (``smallest'' items first).  This is the  |
                         default.                                  |

                              -                          decreas-  |
                         ing                                    |
                         |                                         |
                         Sort  the  list  in   decreasing   order  |
                         (``largest'' items first).


KEYWORDS
     element, list, order, sort










































Tcl                     Last change: 7.0                        2




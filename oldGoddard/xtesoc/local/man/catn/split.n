


split(n)              Tcl Built-In Commands              split(n)



_________________________________________________________________

NAME
     split - Split a string into a proper Tcl list

SYNOPSIS
     split _s_t_r_i_n_g ?_s_p_l_i_t_C_h_a_r_s?
_________________________________________________________________


DESCRIPTION
     Returns a list created by splitting _s_t_r_i_n_g at each character
     that  is  in  the  _s_p_l_i_t_C_h_a_r_s argument.  Each element of the
     result list will consist of the characters from _s_t_r_i_n_g  that
     lie  between  instances  of  the  characters  in _s_p_l_i_t_C_h_a_r_s.
     Empty list elements will be  generated  if  _s_t_r_i_n_g  contains
     adjacent  characters  in _s_p_l_i_t_C_h_a_r_s, or if the first or last
     character of _s_t_r_i_n_g is in _s_p_l_i_t_C_h_a_r_s.  If _s_p_l_i_t_C_h_a_r_s  is  an
     empty  string  then  each  character  of  _s_t_r_i_n_g  becomes  a
     separate element of the result list.  _S_p_l_i_t_C_h_a_r_s defaults to
     the standard white-space characters.  For example,

          split "comp.unix.misc" .

     returns "comp unix misc" and

          split "Hello world" {}

     returns "H e l l o { } w o r l d".


KEYWORDS
     list, split, string






















Tcl                      Last change:                           1




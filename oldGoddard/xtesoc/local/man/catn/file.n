


file(n)               Tcl Built-In Commands               file(n)



_________________________________________________________________

NAME
     file - Manipulate file names and attributes

SYNOPSIS
     file _o_p_t_i_o_n _n_a_m_e ?_a_r_g _a_r_g ...?
_________________________________________________________________


DESCRIPTION
     This command provides several operations on a file's name or
     attributes.  _N_a_m_e is the name of a file; if it starts with a
     tilde, then tilde substitution is done before executing  the
     command   (see  the  manual  entry  for  Tcl_TildeSubst  for
     details).  _O_p_t_i_o_n indicates what to do with the  file  name.
     Any unique abbreviation for _o_p_t_i_o_n is acceptable.  The valid
     options are:

     file atime _n_a_m_e
          Returns a decimal string giving the time at which  file
          _n_a_m_e  was  last  accessed.  The time is measured in the
          standard POSIX fashion as seconds from a fixed starting
          time  (often  January  1,  1970).   If the file doesn't
          exist or its access time  cannot  be  queried  then  an
          error is generated.

     file dirname _n_a_m_e
          Returns all of the characters in _n_a_m_e  up  to  but  not
          including  the  last  slash character.  If there are no
          slashes in _n_a_m_e then returns ``.''.  If the last  slash
          in _n_a_m_e is its first character, then return ``/''.

     file executable _n_a_m_e
          Returns 1 if file _n_a_m_e is  executable  by  the  current
          user, 0 otherwise.

     file exists _n_a_m_e
          Returns 1 if file _n_a_m_e exists and the current user  has
          search  privileges for the directories leading to it, 0
          otherwise.

     file extension _n_a_m_e
          Returns all of the characters in _n_a_m_e after and includ-
          ing the last dot in the last element of _n_a_m_e.  If there
          is no dot in the last element of _n_a_m_e then returns  the
          empty string.

     file isdirectory _n_a_m_e
          Returns 1 if file _n_a_m_e is a directory, 0 otherwise.

     file isfile _n_a_m_e



Tcl                      Last change:                           1






file(n)               Tcl Built-In Commands               file(n)



          Returns 1 if file _n_a_m_e is a regular file, 0 otherwise.

     file lstat _n_a_m_e _v_a_r_N_a_m_e
          Same as stat option (see below) except uses  the  _l_s_t_a_t
          kernel  call  instead of _s_t_a_t.  This means that if _n_a_m_e
          refers to a symbolic link the information  returned  in
          _v_a_r_N_a_m_e  is for the link rather than the file it refers
          to.  On systems that don't support symbolic links  this
          option behaves exactly the same as the stat option.

     file mtime _n_a_m_e
          Returns a decimal string giving the time at which  file
          _n_a_m_e  was  last  modified.  The time is measured in the
          standard POSIX fashion as seconds from a fixed starting
          time  (often  January  1,  1970).   If the file doesn't
          exist or its modified time cannot be  queried  then  an
          error is generated.

     file owned _n_a_m_e
          Returns 1 if file _n_a_m_e is owned by the current user,  0
          otherwise.

     file readable _n_a_m_e
          Returns 1 if file _n_a_m_e is readable by the current user,
          0 otherwise.

     file readlink _n_a_m_e
          Returns the value of the symbolic link  given  by  _n_a_m_e
          (i.e.  the  name  of  the  file it points to).  If _n_a_m_e
          isn't a symbolic link or its value cannot be read, then
          an  error  is  returned.  On systems that don't support
          symbolic links this option is undefined.

     file rootname _n_a_m_e
          Returns all of the characters in _n_a_m_e  up  to  but  not
          including  the  last  ``.''  character in the name.  If
          _n_a_m_e doesn't contain a dot, then returns _n_a_m_e.

     file size _n_a_m_e
          Returns a decimal string giving the size of  file  _n_a_m_e
          in bytes.  If the file doesn't exist or its size cannot
          be queried then an error is generated.

     file stat  _n_a_m_e _v_a_r_N_a_m_e
          Invokes the stat kernel call  on  _n_a_m_e,  and  uses  the
          variable  given by _v_a_r_N_a_m_e to hold information returned
          from the kernel call.  _V_a_r_N_a_m_e is treated as  an  array
          variable,  and  the following elements of that variable
          are set: atime, ctime,  dev,  gid,  ino,  mode,  mtime,
          nlink,  size, type, uid.  Each element except type is a
          decimal string with  the  value  of  the  corresponding
          field  from  the  stat return structure; see the manual



Tcl                      Last change:                           2






file(n)               Tcl Built-In Commands               file(n)



          entry for stat for  details  on  the  meanings  of  the
          values.  The type element gives the type of the file in
          the same form returned by the command file type.   This
          command returns an empty string.

     file tail _n_a_m_e
          Returns all of the characters in _n_a_m_e  after  the  last
          slash.  If _n_a_m_e contains no slashes then returns _n_a_m_e.

     file type _n_a_m_e
          Returns a string giving the type of  file  _n_a_m_e,  which
          will  be  one  of  file,  directory,  characterSpecial,
          blockSpecial, fifo, link, or socket.

     file writable _n_a_m_e
          Returns 1 if file _n_a_m_e is writable by the current user,
          0 otherwise.


KEYWORDS
     attributes, directory, file, name, stat


































Tcl                      Last change:                           3




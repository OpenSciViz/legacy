


return(n)             Tcl Built-In Commands             return(n)



_________________________________________________________________

NAME
     return - Return from a procedure

SYNOPSIS
     return ?-code _c_o_d_e? ?-errorinfo  _i_n_f_o?  ? - errorcode  _c_o_d_e?
     ?_s_t_r_i_n_g?
_________________________________________________________________


DESCRIPTION
     Return immediately from the current procedure (or  top-level
     command or source command), with _s_t_r_i_n_g as the return value.
     If _s_t_r_i_n_g is not specified then  an  empty  string  will  be
     returned as result.


EXCEPTIONAL RETURNS
     In the usual case where the -code option isn't specified the  |
     procedure  will return normally (its completion code will be  |
     TCL_OK).  However, the -code option may be used to  generate  |
     an exceptional return from the procedure.  _C_o_d_e may have any  |
     of the following values:                                      |

     ok                                                         |
               |                                                   |
               Normal return:  same as if the option is omitted.   |

     error                                                      |
               |                                                   |
               Error return: same as if the  error  command  were  |
               used  to  terminate the procedure, except for han-  |
               dling of errorInfo and  errorCode  variables  (see  |
               below).                                             |

     return                                                     |
               |                                                   |
               The current procedure will return with  a  comple-  |
               tion  code  of  TCL_RETURN,  so that the procedure  |
               that invoked it will return also.                   |

     break                                                      |
               |                                                   |
               The current procedure will return with  a  comple-  |
               tion  code  of TCL_BREAK, which will terminate the  |
               innermost nested loop in the code that invoked the  |
               current procedure.                                  |

     con-  |
               tinue                                            |
               |                                                   |



Tcl                     Last change: 7.0                        1






return(n)             Tcl Built-In Commands             return(n)



               The current procedure will return with  a  comple-  |
               tion  code  of  TCL_CONTINUE, which will terminate  |
               the current iteration of the innermost nested loop  |
               in the code that invoked the current procedure.     |

     _v_a_l_u_e                                                      |
               |                                                   |
               _V_a_l_u_e must be an integer;  it will be returned  as  |
               the completion code for the current procedure.      |

     The -code option is rarely used.  It  is  provided  so  that  |
     procedures that implement new control structures can reflect  |
     exceptional conditions back to their callers.                 |

     Two additional options, -errorinfo and - errorcode,  may  be  |
     used to provide additional information during error returns.  |
     These options are ignored unless _c_o_d_e is error.               |

     The -errorinfo option specifies an initial stack  trace  for  |
     the  errorInfo  variable;   if  it is not specified then the  |
     stack trace left in errorInfo will include the call  to  the  |
     procedure  and  higher  levels  on the stack but it will not  |
     include any information  about  the  context  of  the  error  |
     within  the procedure.  Typically the _i_n_f_o value is supplied  |
     from the value left  in  errorInfo  after  a  catch  command  |
     trapped an error within the procedure.                        |

     If the -errorcode option is specified then _c_o_d_e  provides  a  |
     value  for  the  errorCode  variable.   If the option is not  |
     specified then errorCode will default to NONE.


KEYWORDS
     break, continue, error, procedure, return





















Tcl                     Last change: 7.0                        2




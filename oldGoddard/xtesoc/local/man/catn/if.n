


if(n)                 Tcl Built-In Commands                 if(n)



_________________________________________________________________

NAME
     if - Execute scripts conditionally

SYNOPSIS
     if _e_x_p_r_1 ?then? _b_o_d_y_1 elseif _e_x_p_r_2 ?then? _b_o_d_y_2  elseif  ...
     ?else? ?_b_o_d_y_N?
_________________________________________________________________


DESCRIPTION
     The _i_f command evaluates _e_x_p_r_1 as an expression (in the same
     way  that  expr  evaluates  its argument).  The value of the
     expression must be a boolean (a numeric value,  where  0  is  |
     false  and  anything is true, or a string value such as true  |
     or yes for true and false or no for false); if  it  is  true
     then _b_o_d_y_1 is executed by passing it to the Tcl interpreter.
     Otherwise _e_x_p_r_2 is evaluated as an expression and if  it  is
     true  then  body2  is  executed,  and so on.  If none of the
     expressions evaluates to true then _b_o_d_y_N is  executed.   The
     then and else arguments are optional ``noise words'' to make
     the command easier to read.  There  may  be  any  number  of
     elseif  clauses,  including zero.  _B_o_d_y_N may also be omitted
     as long as else is omitted too.  The return value  from  the
     command  is the result of the body script that was executed,
     or an empty string if none of the expressions  was  non-zero
     and there was no _b_o_d_y_N.


KEYWORDS
     boolean, conditional, else, false, if, true























Tcl                      Last change:                           1







selection(n)               Tk Commands               selection(n)



_________________________________________________________________

NAME
     selection - Manipulate the X selection

SYNOPSIS
     selection _o_p_t_i_o_n ?_a_r_g _a_r_g ...?
_________________________________________________________________


DESCRIPTION
     This command provides a Tcl interface  to  the  X  selection
     mechanism  and  implements  the full selection functionality
     described in the X  Inter-Client  Communication  Conventions
     Manual (ICCCM).

     The first argument to selection determines the format of the
     rest  of the arguments and the behavior of the command.  The
     following forms are currently supported:

     selection clear ?       -       displayof _w_i_n_d_o_w? ?        -         |
          selection _s_e_l_e_c_t_i_o_n?         |                           |
          If _s_e_l_e_c_t_i_o_n exists anywhere on _w_i_n_d_o_w's display, clear  |
          it  so  that  no  window  owns  the  selection anymore.  |
          _S_e_l_e_c_t_i_o_n specifies the  X  selection  that  should  be  |
          cleared,  and should be an atom name such as PRIMARY or  |
          CLIPBOARD; see the Inter-Client  Communication  Conven-  |
          tions  Manual for complete details.  _S_e_l_e_c_t_i_o_n defaults  |
          to PRIMARY and _w_i_n_d_o_w defaults to  ``.''.   Returns  an  |
          empty string.                                            |

_t_y_p_e?                                                           |
|                                                                  |
     selection get ?-displayof _w_i_n_d_o_w? ?-selection _s_e_l_e_c_t_i_o_n? ? -  |
           type  |                                                 |
          Retrieves the value of _s_e_l_e_c_t_i_o_n from _w_i_n_d_o_w's  display  |
          and returns it as a result.  _S_e_l_e_c_t_i_o_n defaults to PRI-  |
          MARY and _w_i_n_d_o_w defaults to ``.''. _T_y_p_e  specifies  the
          form  in  which  the  selection  is to be returned (the
          desired ``target'' for conversion, in  ICCCM  terminol-
          ogy),  and  should  be  an  atom name such as STRING or
          FILE_NAME; see the Inter-Client  Communication  Conven-
          tions  Manual  for  complete details.  _T_y_p_e defaults to
          STRING.  The selection owner may choose to  return  the
          selection  in  any  of several different representation
          formats, such as STRING, ATOM, INTEGER, etc. (this for-
          mat is different than the selection type; see the ICCCM
          for all the confusing details).  If  the  selection  is
          returned  in  a  non-string  format, such as INTEGER or
          ATOM, the selection command converts it to string  for-
          mat  as  a  collection  of  fields separated by spaces:
          atoms  are  converted  to  their  textual  names,   and



Tk                      Last change: 4.0                        1






selection(n)               Tk Commands               selection(n)



          anything else is converted to hexadecimal integers.

_f_o_r_m_a_t? _w_i_n_d_o_w _c_o_m_m_a_n_d                                          |
|                                                                  |
     selection handle ?-selection _s_e_l_e_c_t_i_o_n? ?- type  _t_y_p_e?  ?  -  |
           format  |                                               |
          Creates a handler for  selection  requests,  such  that  |
          _c_o_m_m_a_n_d will be executed whenever _s_e_l_e_c_t_i_o_n is owned by  |
          _w_i_n_d_o_w and someone attempts to retrieve it in the  form  |
          given  by _t_y_p_e (e.g. _t_y_p_e is specified in the selection  |
          get command).   _S_e_l_e_c_t_i_o_n  defaults  to  PRIMARY,  _t_y_p_e  |
          defaults  to STRING, and _f_o_r_m_a_t defaults to STRING.  If  |
          _c_o_m_m_a_n_d is an empty string then  any  existing  handler  |
          for _w_i_n_d_o_w, _t_y_p_e, and _s_e_l_e_c_t_i_o_n is removed.              |

          When _s_e_l_e_c_t_i_o_n is requested, _w_i_n_d_o_w  is  the  selection  |
          owner,  and _t_y_p_e is the requested type, _c_o_m_m_a_n_d will be  |
          executed as a Tcl command with two  additional  numbers  |
          appended  to  it (with space separators). The two addi-
          tional numbers are _o_f_f_s_e_t and _m_a_x_B_y_t_e_s:  _o_f_f_s_e_t  speci-
          fies a starting character position in the selection and
          _m_a_x_B_y_t_e_s gives the maximum number of bytes to retrieve.
          The command should return a value consisting of at most
          _m_a_x_B_y_t_e_s of the selection, starting at position _o_f_f_s_e_t.
          For  very  large  selections (larger than _m_a_x_B_y_t_e_s) the
          selection will be retrieved using  several  invocations
          of  _c_o_m_m_a_n_d  with increasing _o_f_f_s_e_t values.  If _c_o_m_m_a_n_d
          returns a string whose length is  less  than  _m_a_x_B_y_t_e_s,
          the  return  value  is  assumed  to  include all of the
          remainder of the selection;  if the length of _c_o_m_m_a_n_d's
          result  is  equal  to  _m_a_x_B_y_t_e_s  then  _c_o_m_m_a_n_d  will be
          invoked again, until it  eventually  returns  a  result
          shorter  than  _m_a_x_B_y_t_e_s.   The  value  of _m_a_x_B_y_t_e_s will
          always be relatively large (thousands of bytes).

          If  _c_o_m_m_a_n_d  returns  an  error  then   the   selection
          retrieval  is  rejected just as if the selection didn't
          exist at all.

          The _f_o_r_m_a_t argument specifies the  representation  that
          should be used to transmit the selection to the reques-
          ter (the second column of Table 2 of  the  ICCCM),  and
          defaults to STRING.  If _f_o_r_m_a_t is STRING, the selection
          is transmitted as 8-bit ASCII characters (i.e.  just in
          the form returned by _c_o_m_m_a_n_d).  If _f_o_r_m_a_t is ATOM, then
          the return value from _c_o_m_m_a_n_d is  divided  into  fields
          separated  by  white space;  each field is converted to
          its atom value, and the 32-bit atom value is  transmit-
          ted  instead  of  the atom name.  For any other _f_o_r_m_a_t,
          the return value from _c_o_m_m_a_n_d is  divided  into  fields
          separated by white space and each field is converted to
          a 32-bit integer;  an array of integers is  transmitted



Tk                      Last change: 4.0                        2






selection(n)               Tk Commands               selection(n)



          to the selection requester.

          The _f_o_r_m_a_t argument is needed  only  for  compatibility
          with  selection requesters that don't use Tk.  If Tk is
          being used to retrieve the selection then the value  is
          converted  back  to  a string at the requesting end, so
          _f_o_r_m_a_t is irrelevant.

     selection own ?        -        displayof _w_i_n_d_o_w? ?        -          |
          selection _s_e_l_e_c_t_i_o_n?           |                         |
     selection own ?-command _c_o_m_m_a_n_d? ?-selection _s_e_l_e_c_t_i_o_n? _w_i_n_-  |
     _d_o_w                                                           |
          The first form of selection own returns the  path  name  |
          of  the  window in this application that owns _s_e_l_e_c_t_i_o_n  |
          on the display containing _w_i_n_d_o_w, or an empty string if  |
          no  window  in  this  application  owns  the selection.  |
          _S_e_l_e_c_t_i_o_n defaults to PRIMARY and  _w_i_n_d_o_w  defaults  to  |
          ``.''.                                                   |

          The second form  of  selection  own  causes  _w_i_n_d_o_w  to  |
          become  the new owner of _s_e_l_e_c_t_i_o_n on _w_i_n_d_o_w's display,  |
          returning an  empty  string  as  result.  The  existing  |
          owner,  if any, is notified that it has lost the selec-  |
          tion.  If _c_o_m_m_a_n_d is specified, it is a Tcl  script  to  |
          execute  when some other window claims ownership of the  |
          selection away from _w_i_n_d_o_w.  _S_e_l_e_c_t_i_o_n defaults to PRI-  |
          MARY.


KEYWORDS
     clear, format, handler, ICCCM, own, selection, target, type
























Tk                      Last change: 4.0                        3




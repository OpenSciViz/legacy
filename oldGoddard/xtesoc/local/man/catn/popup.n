


tk_popup(n)                Tk Commands                tk_popup(n)



_________________________________________________________________

NAME
     tk_popup - Post a popup menu

SYNOPSIS
     tk_popup _m_e_n_u _x _y ?_e_n_t_r_y?
_________________________________________________________________


DESCRIPTION
     This procedure posts a menu  at  a  given  position  on  the
     screen  and  configures Tk so that the menu and its cascaded
     children can be traversed with the mouse  or  the  keyboard.
     _M_e_n_u  is  the name of a menu widget and _x and _y are the root
     coordinates at which to display the menu.  If _e_n_t_r_y is omit-
     ted  or  an  empty  string,  the menu's upper left corner is
     positioned at the given point.  Otherwise  _e_n_t_r_y  gives  the
     index of an entry in _m_e_n_u and the menu will be positioned so
     that the entry is positioned over the given point.


KEYWORDS
     menu, popup































Tk                      Last change: 4.0                        1




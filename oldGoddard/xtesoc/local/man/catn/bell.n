


bell(n)                    Tk Commands                    bell(n)



_________________________________________________________________

NAME
     bell - Ring a display's bell

SYNOPSIS
     bell ?-displayof _w_i_n_d_o_w?
_________________________________________________________________


DESCRIPTION
     This command rings the bell on the display  for  _w_i_n_d_o_w  and
     returns  an empty string.  If the -displayof option is omit-
     ted, the display of the application's main window is used by
     default.  The command uses the current bell-related settings
     for the display, which may be modified with programs such as
     xset.


KEYWORDS
     beep, bell, ring


































Tk                      Last change: 4.0                        1




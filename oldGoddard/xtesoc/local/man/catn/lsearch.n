


lsearch(n)            Tcl Built-In Commands            lsearch(n)



_________________________________________________________________

NAME
     lsearch - See if a list contains a particular element

SYNOPSIS
     lsearch ?_m_o_d_e? _l_i_s_t _p_a_t_t_e_r_n
_________________________________________________________________


DESCRIPTION
     This command searches the elements of _l_i_s_t to see if one  of
     them  matches _p_a_t_t_e_r_n.  If so, the command returns the index
     of the first matching element.  If not, the command  returns
     - 1.   The  _m_o_d_e  argument indicates how the elements of the  |
     list are to be matched against _p_a_t_t_e_r_n and it must have  one  |
     of the following values:                                      |

                                                                -                                                             |
          exact                                                 |
          |                                                        |
          The list element must contain exactly the  same  string  |
          as _p_a_t_t_e_r_n.                                              |

                                                                -                                                             |
          glob                                                  |
          |                                                        |
          _P_a_t_t_e_r_n  is  a  glob-style  pattern  which  is  matched  |
          against  each  list element using the same rules as the  |
          string match command.                                    |

                                                                -                                                             |
          regexp                                                |
          |                                                        |
          _P_a_t_t_e_r_n is treated as a regular expression and  matched  |
          against  each  list element using the same rules as the  |
          regexp command.                                          |

     If _m_o_d_e is omitted then it defaults to -glob.


KEYWORDS
     list, match, pattern, regular expression, search, string












Tcl                     Last change: 7.0                        1




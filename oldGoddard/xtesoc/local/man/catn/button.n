


button(n)                  Tk Commands                  button(n)



_________________________________________________________________

NAME
     button - Create and manipulate button widgets

SYNOPSIS
     button _p_a_t_h_N_a_m_e ?_o_p_t_i_o_n_s?

STANDARD OPTIONS
     activeBackground               cursor          imagetextVariable|
     activeForeground               disabledForegroundjustifyunderline|
     anchor          font           padX            wrapLength     |
     background      foreground     padY                           |
     bitmap          highlightColor relief                         |
     borderWidth     highlightThickness             text

     See the ``options'' manual entry for details on the standard
     options.

WIDGET-SPECIFIC OPTIONS
     Name:           command
     Class:          Command
     Command-Line Switch:-command

          Specifies a Tcl command to associate with  the  button.
          This  command  is typically invoked when mouse button 1
          is released over the button window.

     Name:           height
     Class:          Height
     Command-Line Switch:-height

          Specifies a desired height for the button.  If an image
          or  bitmap  is  being  displayed in the button then the
          value is in screen units (i.e. any of the forms accept-
          able to Tk_GetPixels); for text it is in lines of text.
          If this option isn't specified,  the  button's  desired
          height is computed from the size of the image or bitmap
          or text being displayed in it.

     Name:           state
     Class:          State
     Command-Line Switch:-state

          Specifies one of three states for the button:   normal,
          active,  or  disabled.   In  normal state the button is
          displayed using the foreground and background  options.
          The  active state is typically used when the pointer is
          over  the  button.   In  active  state  the  button  is
          displayed  using  the  activeForeground and activeBack-
          ground options.  Disabled state means that  the  button
          is   insensitive:   it  doesn't  activate  and  doesn't



Tk                      Last change: 4.0                        1






button(n)                  Tk Commands                  button(n)



          respond to mouse button presses.   In  this  state  the
          disabledForeground and background options determine how
          the button is displayed.

     Name:           width
     Class:          Width
     Command-Line Switch:-width

          Specifies a desired width for the button.  If an  image
          or  bitmap  is  being  displayed in the button then the
          value is in screen units (i.e. any of the forms accept-
          able  to  Tk_GetPixels);  for text it is in characters.
          If this option isn't specified,  the  button's  desired
          width  is computed from the size of the image or bitmap
          or text being displayed in it.
_________________________________________________________________


DESCRIPTION
     The button command creates a new window (given by the  _p_a_t_h_-
     _N_a_m_e  argument)  and  makes  it into a button widget.  Addi-
     tional options, described above, may  be  specified  on  the
     command  line or in the option database to configure aspects
     of the button such as its colors, font,  text,  and  initial
     relief.   The  button command returns its _p_a_t_h_N_a_m_e argument.
     At the time this command is invoked, there must not exist  a
     window named _p_a_t_h_N_a_m_e, but _p_a_t_h_N_a_m_e's parent must exist.

     A button is a widget that displays a textual string,  bitmap  |
     or  image.  If text is displayed, it must all be in a single  |
     font, but it can occupy multiple lines on the screen (if  it  |
     contains  newlines or if wrapping occurs because of the wra-  |
     pLength option) and one of the characters may optionally  be  |
     underlined  using  the  underline  option.   It  can display
     itself in either of three different ways, according  to  the
     state  option;  it  can be made to appear raised, sunken, or
     flat; and it can be made to flash.  When a user invokes  the
     button  (by pressing mouse button 1 with the cursor over the
     button), then the Tcl command  specified  in  the  - command
     option is invoked.


WIDGET COMMAND
     The button command creates a new Tcl command whose  name  is
     _p_a_t_h_N_a_m_e.  This command may be used to invoke various opera-
     tions on the widget.  It has the following general form:

          _p_a_t_h_N_a_m_e _o_p_t_i_o_n ?_a_r_g _a_r_g ...?

     _O_p_t_i_o_n and the _a_r_gs determine the exact behavior of the com-
     mand.   The following commands are possible for button widg-
     ets:



Tk                      Last change: 4.0                        2






button(n)                  Tk Commands                  button(n)



     _p_a_t_h_N_a_m_e cget _o_p_t_i_o_n
          Returns the current value of the  configuration  option  |
          given  by  _o_p_t_i_o_n.   _O_p_t_i_o_n  may have any of the values  |
          accepted by the button command.

     _p_a_t_h_N_a_m_e configure ?_o_p_t_i_o_n? ?_v_a_l_u_e _o_p_t_i_o_n _v_a_l_u_e ...?
          Query  or  modify  the  configuration  options  of  the
          widget.   If  no  _o_p_t_i_o_n  is  specified, returns a list
          describing all of the available  options  for  _p_a_t_h_N_a_m_e
          (see  Tk_ConfigureInfo for information on the format of
          this list).  If _o_p_t_i_o_n is specified with no _v_a_l_u_e, then
          the  command  returns  a  list describing the one named
          option (this list will be identical to the  correspond-
          ing  sublist  of  the  value  returned  if no _o_p_t_i_o_n is
          specified).  If one or more  _o_p_t_i_o_n - _v_a_l_u_e  pairs  are
          specified,  then  the command modifies the given widget
          option(s) to have the given value(s);  in this case the
          command  returns  an empty string.  _O_p_t_i_o_n may have any
          of the values accepted by the button command.

     _p_a_t_h_N_a_m_e flash
          Flash the button.  This is accomplished by redisplaying
          the  button  several  times, alternating between active
          and normal colors.  At the end of the flash the  button
          is  left  in  the  same normal/active state as when the
          command was invoked.  This command is  ignored  if  the
          button's state is disabled.

     _p_a_t_h_N_a_m_e invoke
          Invoke the Tcl command associated with the  button,  if
          there  is  one.   The  return value is the return value
          from the Tcl command, or an empty string if there is no
          command  associated  with  the button.  This command is
          ignored if the button's state is disabled.


DEFAULT BINDINGS
     Tk automatically creates class  bindings  for  buttons  that
     give them the following default behavior:

     [1]  A button activates whenever the mouse  passes  over  it
          and deactivates whenever the mouse leaves the button.

     [2]  A button's relief is changed to sunken  whenever  mouse
          button  1 is pressed over the button, and the relief is
          restored to its original value when button 1  is  later
          released.

     [3]  If mouse button 1 is pressed over a  button  and  later
          released  over the button, the button is invoked.  How-
          ever, if the mouse is not over the button when button 1
          is released, then no invocation occurs.



Tk                      Last change: 4.0                        3






button(n)                  Tk Commands                  button(n)



     [4]  When a button has the input focus, the space key causes  |
          the button to be invoked.

     If the button's state is disabled then  none  of  the  above
     actions occur:  the button is completely non-responsive.

     The behavior of buttons can be changed by defining new bind-
     ings for individual widgets or by redefining the class bind-
     ings.


KEYWORDS
     button, widget










































Tk                      Last change: 4.0                        4




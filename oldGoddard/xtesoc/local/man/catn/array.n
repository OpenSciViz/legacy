


array(n)              Tcl Built-In Commands              array(n)



_________________________________________________________________

NAME
     array - Manipulate array variables

SYNOPSIS
     array _o_p_t_i_o_n _a_r_r_a_y_N_a_m_e ?_a_r_g _a_r_g ...?
_________________________________________________________________


DESCRIPTION
     This command performs one of several operations on the vari-
     able  given  by  _a_r_r_a_y_N_a_m_e.   Unless otherwise specified for
     individual commands below, _a_r_r_a_y_N_a_m_e must be the name of  an
     existing  array  variable.   The  _o_p_t_i_o_n argument determines
     what action is  carried  out  by  the  command.   The  legal
     _o_p_t_i_o_n_s (which may be abbreviated) are:

     array anymore _a_r_r_a_y_N_a_m_e _s_e_a_r_c_h_I_d
          Returns 1 if there are any more  elements  left  to  be
          processed  in  an  array search, 0 if all elements have
          already been returned.  _S_e_a_r_c_h_I_d indicates which search
          on  _a_r_r_a_y_N_a_m_e  to  check, and must have been the return
          value from a previous invocation of array  startsearch.
          This  option  is particularly useful if an array has an
          element with an empty name, since the return value from
          array nextelement won't indicate whether the search has
          been completed.

     array donesearch _a_r_r_a_y_N_a_m_e _s_e_a_r_c_h_I_d
          This command terminates an array  search  and  destroys
          all  the  state  associated with that search.  _S_e_a_r_c_h_I_d
          indicates which search on  _a_r_r_a_y_N_a_m_e  to  destroy,  and
          must have been the return value from a previous invoca-
          tion of array startsearch.  Returns an empty string.

     array exists _a_r_r_a_y_N_a_m_e
          Returns 1 if _a_r_r_a_y_N_a_m_e is an array variable, 0 if there  |
          is  no variable by that name or if it is a scalar vari-  |
          able.

     array get _a_r_r_a_y_N_a_m_e
          Returns a list containing pairs of elements.  The first  |
          element  in  each  pair  is  the  name of an element in  |
          _a_r_r_a_y_N_a_m_e and the second element of each  pair  is  the  |
          value  of the array element.  The order of the pairs is  |
          undefined.  If _a_r_r_a_y_N_a_m_e isn't the  name  of  an  array  |
          variable, or if the array contains no elements, then an  |
          empty list is returned.

     array names _a_r_r_a_y_N_a_m_e ?_p_a_t_t_e_r_n?
          Returns a list containing  the  names  of  all  of  the



Tcl                     Last change: 7.4                        1






array(n)              Tcl Built-In Commands              array(n)



          elements  in  the  array  that match _p_a_t_t_e_r_n (using the  |
          glob-style matching rules of string match).  If _p_a_t_t_e_r_n  |
          is  omitted then the command returns all of the element  |
          names in the array.  If there are  no  (matching)  ele-  |
          ments  in  the array, or if _a_r_r_a_y_N_a_m_e isn't the name of  |
          an array variable, then an empty string is returned.

     array nextelement _a_r_r_a_y_N_a_m_e _s_e_a_r_c_h_I_d
          Returns the name of the next element in  _a_r_r_a_y_N_a_m_e,  or
          an  empty  string  if  all  elements  of _a_r_r_a_y_N_a_m_e have
          already been returned in  this  search.   The  _s_e_a_r_c_h_I_d
          argument  identifies the search, and must have been the
          return value of an array startsearch command.  Warning:
          if  elements  are  added  to or deleted from the array,
          then all searches are automatically terminated just  as
          if  array  donesearch had been invoked; this will cause
          array  nextelement  operations  to   fail   for   those
          searches.

     array set _a_r_r_a_y_N_a_m_e _l_i_s_t
          Sets the values of one or more elements  in  _a_r_r_a_y_N_a_m_e.  |
          _l_i_s_t  must have a form like that returned by array get,  |
          consisting of an even number of  elements.   Each  odd-  |
          numbered  element in _l_i_s_t is treated as an element name  |
          within _a_r_r_a_y_N_a_m_e, and the following element in _l_i_s_t  is  |
          used as a new value for that array element.

     array size _a_r_r_a_y_N_a_m_e
          Returns a decimal string giving the number of  elements
          in  the array.  If _a_r_r_a_y_N_a_m_e isn't the name of an array  |
          then 0 is returned.

     array startsearch _a_r_r_a_y_N_a_m_e
          This command initializes an  element-by-element  search
          through the array given by _a_r_r_a_y_N_a_m_e, such that invoca-
          tions of the array nextelement command will return  the
          names  of  the  individual elements in the array.  When
          the search has been  completed,  the  array  donesearch
          command  should  be  invoked.   The  return  value is a
          search identifier that must be used in  array  nextele-
          ment  and array donesearch commands; it allows multiple
          searches to be underway  simultaneously  for  the  same
          array.


KEYWORDS
     array, element names, search








Tcl                     Last change: 7.4                        2




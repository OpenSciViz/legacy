


pack(n)                    Tk Commands                    pack(n)



_________________________________________________________________

NAME
     pack - Geometry manager that packs around edges of cavity

SYNOPSIS
     pack _o_p_t_i_o_n _a_r_g ?_a_r_g ...?
_________________________________________________________________


DESCRIPTION
     The pack command is used to communicate with the  packer,  a
     geometry  manager  that arranges the children of a parent by
     packing them in order around the edges of the  parent.   The
     pack command can have any of several forms, depending on the
     _o_p_t_i_o_n argument:

     pack _s_l_a_v_e ?_s_l_a_v_e ...? ?_o_p_t_i_o_n_s?
          If the first argument to pack is  a  window  name  (any
          value  starting  with  ``.''), then the command is pro-
          cessed in the same way as pack configure.

     pack configure _s_l_a_v_e ?_s_l_a_v_e ...? ?_o_p_t_i_o_n_s?
          The arguments consist of the names of one or more slave
          windows followed by pairs of arguments that specify how
          to manage the slaves.   See  ``THE  PACKER  ALGORITHM''
          below  for  details  on how the options are used by the
          packer.  The following options are supported:

          -after _o_t_h_e_r
               _O_t_h_e_r must the name of another  window.   Use  its
               master  as  the  master for the slaves, and insert
               the slaves just after _o_t_h_e_r in the packing order.

          -anchor _a_n_c_h_o_r
               _A_n_c_h_o_r must be a valid anchor position such  as  n
               or  sw;  it specifies where to position each slave
               in its parcel.  Defaults to center.

          -before _o_t_h_e_r
               _O_t_h_e_r must the name of another  window.   Use  its
               master  as  the  master for the slaves, and insert
               the slaves just before _o_t_h_e_r in the packing order.

          -expand _b_o_o_l_e_a_n
               Specifies whether the slaves should be expanded to
               consume  extra space in their master.  _B_o_o_l_e_a_n may
               have any proper boolean value, such as  1  or  no.
               Defaults to 0.

          -fill _s_t_y_l_e
               If a slave's parcel is larger than  its  requested



Tk                      Last change: 4.0                        1






pack(n)                    Tk Commands                    pack(n)



               dimensions, this option may be used to stretch the
               slave.  _S_t_y_l_e  must  have  one  of  the  following
               values:

               none Give the slave its requested dimensions  plus
                    any internal padding requested with -ipadx or
                    -ipady.  This is the default.

               x    Stretch the slave horizontally  to  fill  the
                    entire  width  of  its  parcel  (except leave
                    external padding as specified by -padx).

               y    Stretch the  slave  vertically  to  fill  the
                    entire  height  of  its  parcel (except leave
                    external padding as specified by -pady).

               both Stretch the slave both horizontally and vert-
                    ically.

          -in _o_t_h_e_r
               Insert the slave(s) at  the  end  of  the  packing
               order for the master window given by _o_t_h_e_r.

          -ipadx _a_m_o_u_n_t
               _A_m_o_u_n_t specifies how much horizontal internal pad-
               ding  to  leave  on  each  side  of  the slave(s).
               _A_m_o_u_n_t must be a valid screen distance, such as  2
               or .5c.  It defaults to 0.

          -ipady _a_m_o_u_n_t
               _A_m_o_u_n_t specifies how much vertical  internal  pad-
               ding  to  leave  on  each  side  of  the slave(s).
               _A_m_o_u_n_t  defaults to 0.

          -padx _a_m_o_u_n_t
               _A_m_o_u_n_t specifies how much horizontal external pad-
               ding  to  leave  on  each  side  of  the slave(s).
               _A_m_o_u_n_t defaults to 0.

          -pady _a_m_o_u_n_t
               _A_m_o_u_n_t specifies how much vertical  external  pad-
               ding  to  leave  on  each  side  of  the slave(s).
               _A_m_o_u_n_t defaults to 0.

          -side _s_i_d_e
               Specifies which side of the  master  the  slave(s)
               will be packed against.  Must be left, right, top,
               or bottom.  Defaults to top.

          If no -in, -after or -before option is  specified  then
          each  of  the slaves will be inserted at the end of the
          packing list  for  its  parent  unless  it  is  already



Tk                      Last change: 4.0                        2






pack(n)                    Tk Commands                    pack(n)



          managed  by  the  packer (in which case it will be left
          where it is).  If one of  these  options  is  specified
          then  all  the slaves will be inserted at the specified
          point.  If any of the slaves are already managed by the
          geometry  manager then any unspecified options for them
          retain their  previous  values  rather  than  receiving
          default values.

     pack forget _s_l_a_v_e ?_s_l_a_v_e ...?
          Removes each of the _s_l_a_v_es from the packing  order  for
          its  master  and unmaps their windows.  The slaves will
          no longer be managed by the packer.

     pack info _s_l_a_v_e
          Returns a list whose elements are  the  current  confi-  |
          guration  state of the slave given by _s_l_a_v_e in the same  |
          option-value form that might be specified to pack  con-  |
          figure.   The  first two elements of the list are ``-in  |
          _m_a_s_t_e_r'' where _m_a_s_t_e_r is the slave's master.

     pack propagate _m_a_s_t_e_r ?_b_o_o_l_e_a_n?
          If _b_o_o_l_e_a_n has a true boolean value such  as  1  or  on
          then propagation is enabled for _m_a_s_t_e_r, which must be a
          window name (see ``GEOMETRY PROPAGATION''  below).   If
          _b_o_o_l_e_a_n  has  a false boolean value then propagation is
          disabled for _m_a_s_t_e_r.  In either of these cases an empty
          string  is  returned.   If  _b_o_o_l_e_a_n is omitted then the
          command returns 0 or 1 to indicate whether  propagation
          is   currently  enabled  for  _m_a_s_t_e_r.   Propagation  is
          enabled by default.

     pack slaves _m_a_s_t_e_r
          Returns a list of all of  the  slaves  in  the  packing
          order  for _m_a_s_t_e_r.  The order of the slaves in the list
          is the same as their order in the  packing  order.   If
          _m_a_s_t_e_r has no slaves then an empty string is returned.


THE PACKER ALGORITHM
     For each master the packer  maintains  an  ordered  list  of
     slaves  called  the  _p_a_c_k_i_n_g  _l_i_s_t.   The -in, -after, and -
     before configuration options are used to specify the  master
     for each slave and the slave's position in the packing list.
     If none of these options is given for a slave then the slave
     is added to the end of the packing list for its parent.

     The packer arranges the slaves for a master by scanning  the
     packing list in order.  At the time it processes each slave,
     a rectangular area within the master is  still  unallocated.
     This  area  is called the _c_a_v_i_t_y;  for the first slave it is
     the entire area of the master.




Tk                      Last change: 4.0                        3






pack(n)                    Tk Commands                    pack(n)



     For each slave the packer carries out the following steps:

     [1]  The packer allocates a rectangular _p_a_r_c_e_l for the slave
          along the side of the cavity given by the slave's -side
          option.  If the side is top or bottom then the width of
          the parcel is the width of the cavity and its height is
          the requested height of the slave plus the -ipady and -
          pady options.  For the left or right side the height of
          the parcel is the height of the cavity and the width is
          the  requested width of the slave plus the -ipadx and -
          padx options.   The  parcel  may  be  enlarged  further
          because of the -expand option (see ``EXPANSION'' below)

     [2]  The packer chooses the dimensions of  the  slave.   The
          width will normally be the slave's requested width plus
          twice its -ipadx option and the height will normally be
          the  slave's  requested  height  plus  twice its -ipady
          option.  However, if the -fill option is x or both then
          the width of the slave is expanded to fill the width of
          the parcel, minus twice the -padx option.  If the -fill
          option  is  y  or  both then the height of the slave is
          expanded to fill the width of the parcel,  minus  twice
          the -pady option.

     [3]  The packer positions the slave over its parcel.  If the
          slave  is  smaller  than  the  parcel  then the -anchor
          option determines where in the parcel the slave will be
          placed.   If -padx or -pady is non-zero, then the given
          amount of external padding will always be left  between
          the slave and the edges of the parcel.

     Once a given slave has been packed, the area of  its  parcel
     is subtracted from the cavity, leaving a smaller rectangular
     cavity for the next slave.  If a slave doesn't  use  all  of
     its  parcel, the unused space in the parcel will not be used
     by subsequent slaves.  If the cavity should become too small
     to  meet  the  needs of a slave then the slave will be given
     whatever space is left in the cavity.  If the cavity shrinks
     to  zero size, then all remaining slaves on the packing list
     will be unmapped from the screen  until  the  master  window
     becomes large enough to hold them again.


EXPANSION
     If a master window is so large  that  there  will  be  extra
     space  left  over  after all of its slaves have been packed,
     then the extra space is distributed uniformly among  all  of
     the  slaves for which the -expand option is set.  Extra hor-
     izontal space is distributed  among  the  expandable  slaves
     whose  - side  is left or right, and extra vertical space is
     distributed among the expandable slaves whose -side  is  top
     or bottom.



Tk                      Last change: 4.0                        4






pack(n)                    Tk Commands                    pack(n)



GEOMETRY PROPAGATION
     The packer normally computes how large a master must  be  to
     just  exactly  meet the needs of its slaves, and it sets the
     requested width and height of the  master  to  these  dimen-
     sions.   This  causes  geometry  information to propagate up
     through a window hierarchy to a top-level window so that the
     entire  sub-tree  sizes  itself to fit the needs of the leaf
     windows.  However, the pack propagate command may be used to
     turn  off  propagation for one or more masters.  If propaga-
     tion is disabled then the packer will not set the  requested
     width  and height of the packer.  This may be useful if, for
     example, you wish for a master window to have a  fixed  size
     that you specify.


RESTRICTIONS ON MASTER WINDOWS
     The master for each slave must either be the slave's  parent
     (the  default)  or a descendant of the slave's parent.  This
     restriction is necessary to guarantee that the slave can  be
     placed  over  any part of its master that is visible without
     danger of the slave being clipped by its parent.


PACKING ORDER
     If the master for a slave is not its parent  then  you  must
     make  sure  that  the  slave is higher in the stacking order
     than the master.  Otherwise  the  master  will  obscure  the
     slave  and it will appear as if the slave hasn't been packed
     correctly.  The easiest way to make sure the slave is higher
     than  the  master is to create the master window first:  the
     most recently created window will be highest in the stacking
     order.   Or,  you  can  use  the raise and lower commands to
     change the stacking order of either the master or the slave.


KEYWORDS
     geometry manager,  location,  packer,  parcel,  propagation,
     size

















Tk                      Last change: 4.0                        5




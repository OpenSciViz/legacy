


seek(n)               Tcl Built-In Commands               seek(n)



_________________________________________________________________

NAME
     seek - Change the access position for an open file

SYNOPSIS
     seek _f_i_l_e_I_d _o_f_f_s_e_t ?_o_r_i_g_i_n?
_________________________________________________________________


DESCRIPTION
     Change the current access position for _f_i_l_e_I_d.  _F_i_l_e_I_d  must
     have  been the return value from a previous call to open, or
     it may be stdin, stdout, or stderr to refer to  one  of  the
     standard  I/O  channels.   The  _o_f_f_s_e_t  and _o_r_i_g_i_n arguments
     specify the position at which the next read  or  write  will
     occur  for  _f_i_l_e_I_d.  _O_f_f_s_e_t must be an integer (which may be
     negative) and _o_r_i_g_i_n must be one of the following:

     start
          The new access position will be _o_f_f_s_e_t bytes  from  the
          start of the file.

     current
          The new access position will be _o_f_f_s_e_t bytes  from  the
          current  access  position;  a negative _o_f_f_s_e_t moves the
          access position backwards in the file.

     end  The new access position will be _o_f_f_s_e_t bytes  from  the
          end  of  the file.  A negative _o_f_f_s_e_t places the access
          position before the end-of-file, and a positive  _o_f_f_s_e_t
          places the access position after the end-of-file.

     The _o_r_i_g_i_n argument defaults to start.  This command returns
     an empty string.


KEYWORDS
     access position, file, seek
















Tcl                      Last change:                           1




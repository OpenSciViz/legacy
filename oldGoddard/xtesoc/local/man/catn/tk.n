


tk(n)                      Tk Commands                      tk(n)



_________________________________________________________________

NAME
     tk - Manipulate Tk internal state

SYNOPSIS
     tk _o_p_t_i_o_n ?_a_r_g _a_r_g ...?
_________________________________________________________________


DESCRIPTION
     The tk command provides access to miscellaneous elements  of
     Tk's internal state.  Most of the information manipulated by
     this command pertains to the application as a whole, or to a
     screen  or display, rather than to a particular window.  The
     command can take any of a number of different forms  depend-
     ing on the _o_p_t_i_o_n argument.  The legal forms are:

     tk appname ?_n_e_w_N_a_m_e?
          If _n_e_w_N_a_m_e isn't specified, this  command  returns  the  |
          name  of  the application (the name that may be used in  |
          send commands to communicate with the application).  If  |
          _n_e_w_N_a_m_e  is specified, then the name of the application  |
          is changed to _n_e_w_N_a_m_e.  If the given name is already in  |
          use,  then  a  suffix of the form `` #2'' or `` #3'' is  |
          appended  in  order  to  make  the  name  unique.   The  |
          command's result is the name actually chosen.  If sends  |
          have been disabled by deleting the send  command,  this  |
          command  will  reenable them and recreate the send com-  |
          mand.

     tk colormodel _w_i_n_d_o_w ?_n_e_w_V_a_l_u_e?
          If _n_e_w_V_a_l_u_e isn't specified, this command  returns  the
          current  color  model in use for _w_i_n_d_o_w's screen, which
          will be either color or  monochrome.   If  _n_e_w_V_a_l_u_e  is
          specified,  then  it must be either color or monochrome
          or an abbreviation of one of them; the color model  for
          _w_i_n_d_o_w's screen is set to this value.

          The color model is used by Tk and its widgets to deter-
          mine  whether it should display in black and white only
          or use colors.  A single color model is shared  by  all
          of  the  windows  managed  by  one  process  on a given
          screen.  The color model for a screen is set  initially
          by  Tk  to  monochrome if the display has four or fewer
          bit planes and to color  otherwise.   The  color  model
          will  automatically be changed from color to monochrome
          if Tk fails to allocate a color because all entries  in
          the  colormap  were  in use.  An application can change
          its own color model at any time (e.g. it  might  change
          the  model  to monochrome in order to conserve colormap
          entries, or it might set the  model  to  color  to  use



Tk                      Last change: 4.0                        1






tk(n)                      Tk Commands                      tk(n)



          color  on a four-bit display in special circumstances),
          but an application is not allowed to change  the  color
          model  to  color unless the screen has at least two bit
          planes.


KEYWORDS
     application name, color model, internal state, send















































Tk                      Last change: 4.0                        2




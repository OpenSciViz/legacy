


foreach(n)            Tcl Built-In Commands            foreach(n)



_________________________________________________________________

NAME
     foreach - Iterate over all elements in a list

SYNOPSIS
     foreach _v_a_r_n_a_m_e _l_i_s_t _b_o_d_y
_________________________________________________________________


DESCRIPTION
     In this command _v_a_r_n_a_m_e is the name of a variable, _l_i_s_t is a
     list  of  values  to  assign  to  _v_a_r_n_a_m_e, and _b_o_d_y is a Tcl
     script.  For each element of _l_i_s_t (in  order  from  left  to
     right), foreach assigns the contents of the field to _v_a_r_n_a_m_e
     as if the lindex command had been used to extract the field,
     then  calls  the Tcl interpreter to execute _b_o_d_y.  The break
     and continue statements may be invoked inside _b_o_d_y, with the
     same effect as in the for command.  Foreach returns an empty
     string.


KEYWORDS
     foreach, iteration, list, looping































Tcl                      Last change:                           1




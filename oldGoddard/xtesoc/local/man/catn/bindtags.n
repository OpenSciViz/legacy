


bindtags(n)                Tk Commands                bindtags(n)



_________________________________________________________________

NAME
     bindtags - Determine which bindings apply to a  window,  and
     order of evaluation

SYNOPSIS
     bindtags _w_i_n_d_o_w ?_t_a_g_L_i_s_t?
_________________________________________________________________


DESCRIPTION
     When a binding is created with the bind command, it is asso-
     ciated  either  with  a  particular window such as .a.b.c, a
     class name such as Button, the keyword  all,  or  any  other
     string.   All  of these forms are called _b_i_n_d_i_n_g _t_a_g_s.  Each
     window contains a list of binding tags  that  determine  how
     events  are  processed for the window.  When an event occurs
     in a window, it is applied to each of the window's  tags  in
     order:  for each tag, the most specific binding that matches
     the given tag and event is executed.  See the  bind  command
     for more information on the matching process.

     By default, each window has four binding tags consisting  of
     the window's class name, the name of the window, the name of
     the window's nearest toplevel ancestor,  and  all,  in  that
     order.   Toplevel  windows  have only three tags by default,
     since the toplevel name is the same as that of  the  window.
     The bindtags command allows the binding tags for a window to
     be read and modified.

     If bindtags is invoked with  only  one  argument,  then  the
     current  set  of  binding  tags  for _w_i_n_d_o_w is returned as a
     list.  If the _t_a_g_L_i_s_t argument  is  specified  to  bindtags,
     then  it  must  be  a  proper  list; the tags for _w_i_n_d_o_w are
     changed to the  elements  of  the  list.   The  elements  of
     _t_a_g_L_i_s_t may be arbitrary strings;  however, any tag starting
     with a dot is treated as the name of a window;  if no window
     by  that name exists at the time an event is processed, then
     the tag is ignored for that event.  The order  of  the  ele-
     ments  in  _t_a_g_L_i_s_t  determines  the  order  in which binding
     scripts are executed in response to  events.   For  example,
     the command

          bindtags .b {all . .b Button}

     reverses  the  order  in  which  binding  scripts  will   be
     evaluated  for  a  button  named .b so that all bindings are
     invoked first,  following  by  bindings  for  .b's  toplevel
     (``.''),  followed  by  bindings  for  .b, followed by class
     bindings.




Tk                      Last change: 4.0                        1






bindtags(n)                Tk Commands                bindtags(n)



     The bindtags command may  be  used  to  introduce  arbitrary
     additional  binding tags for a window, or to remove standard
     tags.  For example, the command

          bindtags .b {TrickyButton .b . all}

     replaces the Button tag  for  .b  with  TrickyButton.   This
     means  that  the  default widget bindings for buttons, which
     are associated with the Button tag, will no longer apply  to
     .b,  but  any bindings associated with TrickyButton (perhaps
     some new button behavior) will apply.


SEE ALSO
     bind


KEYWORDS
     binding, event, tag




































Tk                      Last change: 4.0                        2




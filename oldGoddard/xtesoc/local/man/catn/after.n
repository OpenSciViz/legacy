


after(n)                   Tk Commands                   after(n)



_________________________________________________________________

NAME
     after - Execute a command after a time delay

SYNOPSIS
     after _m_s
     after _m_s ?_a_r_g _a_r_g _a_r_g ...?
     after cancel _i_d
     after cancel _c_o_m_m_a_n_d
_________________________________________________________________


DESCRIPTION
     This command is used to delay execution of the program or to
     execute  a  command  in  background  after  a delay.  It has
     several forms, depending on the first argument to  the  com-
     mand:

     after _m_s
          _M_s must be an integer giving a  time  in  milliseconds.
          The   command  sleeps  for  _m_s  milliseconds  and  then
          returns.  While the command is sleeping the application
          does not respond to X events or any other events.

     after _m_s ?_a_r_g _a_r_g _a_r_g ...?
          In this form the command returns  immediately,  but  it
          arranges  for  a  Tcl  command  to  be executed _m_s mil-
          liseconds later as a  background  event  handler.   The
          delayed  command is formed by concatenating all the _a_r_g
          arguments in the same fashion as  the  concat  command.
          The  command  will be executed at global level (outside
          the context of any Tcl procedure).  If an error  occurs
          while  executing  the  delayed command then the tkerror
          mechanism is used to report the error.  The after  com-
          mand  returns  an identifier that can be used to cancel
          the delayed command using after cancel.

     after cancel _i_d
          Cancels the execution of a  delayed  command  that  was  |
          previously   scheduled.   _I_d  indicates  which  command  |
          should be canceled;  it must have been the return value  |
          from a previous after command.  If the command given by  |
          _i_d has already been executed then the after cancel com-  |
          mand has no effect.                                      |

     after cancel _c_o_m_m_a_n_d                                       |
          |                                                        |
          This command also cancels the execution  of  a  delayed  |
          command.   The  _c_o_m_m_a_n_d  argument specifies the text of  |
          the command to be cancelled.  If there  exists  such  a  |
          command  among  those whose execution is still pending,  |



Tk                      Last change: 4.0                        1






after(n)                   Tk Commands                   after(n)



          then it is cancelled and will never be executed; if  no  |
          such command is currently pending then the after cancel  |
          command has no effect.


SEE ALSO
     tkerror


KEYWORDS
     cancel, delay, sleep, time












































Tk                      Last change: 4.0                        2




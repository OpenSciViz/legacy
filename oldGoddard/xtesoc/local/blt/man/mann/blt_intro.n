'\"
'\" Copyright 1993-1994 by AT&T Bell Laboratories.
'\"
'\" Permission to use, copy, modify, and distribute this software
'\" and its documentation for any purpose and without fee is hereby
'\" granted, provided that the above copyright notice appear in all
'\" copies and that both that the copyright notice and warranty
'\" disclaimer appear in supporting documentation, and that the
'\" names of AT&T Bell Laboratories any of their entities not be used
'\" in advertising or publicity pertaining to distribution of the
'\" software without specific, written prior permission.
'\"
'\" AT&T disclaims all warranties with regard to this software, including
'\" all implied warranties of merchantability and fitness.  In no event
'\" shall AT&T be liable for any special, indirect or consequential
'\" damages or any damages whatsoever resulting from loss of use, data
'\" or profits, whether in an action of contract, negligence or other
'\" tortuous action, arising out of or in connection with the use or
'\" performance of this software.
'\"
'\"
.\" The definitions below are for supplemental macros used in Tcl/Tk
.\" manual entries.
.\"
.\" .HS name section [date [version]]
.\"	Replacement for .TH in other man pages.  See below for valid
.\"	section names.
.\"
.\" .AP type name in/out [indent]
.\"	Start paragraph describing an argument to a library procedure.
.\"	type is type of argument (int, etc.), in/out is either "in", "out",
.\"	or "in/out" to describe whether procedure reads or modifies arg,
.\"	and indent is equivalent to second arg of .IP (shouldn't ever be
.\"	needed;  use .AS below instead)
.\"
.\" .AS [type [name]]
.\"	Give maximum sizes of arguments for setting tab stops.  Type and
.\"	name are examples of largest possible arguments that will be passed
.\"	to .AP later.  If args are omitted, default tab stops are used.
.\"
.\" .BS
.\"	Start box enclosure.  From here until next .BE, everything will be
.\"	enclosed in one large box.
.\"
.\" .BE
.\"	End of box enclosure.
.\"
.\" .VS
.\"	Begin vertical sidebar, for use in marking newly-changed parts
.\"	of man pages.
.\"
.\" .VE
.\"	End of vertical sidebar.
.\"
.\" .DS
.\"	Begin an indented unfilled display.
.\"
.\" .DE
.\"	End of indented unfilled display.
.\"
'\"	# Heading for Tcl/Tk man pages
.de HS
.if '\\$2'cmds'       .TH \\$1 1 \\$3 \\$4
.if '\\$2'lib'        .TH \\$1 3 \\$3 \\$4
.if '\\$2'tcl'        .TH \\$1 3 \\$3 \\$4
.if '\\$2'tk'         .TH \\$1 3 \\$3 \\$4
.if t .wh -1.3i ^B
.nr ^l \\n(.l
.ad b
..
'\"	# Start an argument description
.de AP
.ie !"\\$4"" .TP \\$4
.el \{\
.   ie !"\\$2"" .TP \\n()Cu
.   el          .TP 15
.\}
.ie !"\\$3"" \{\
.ta \\n()Au \\n()Bu
\&\\$1	\\fI\\$2\\fP	(\\$3)
.\".b
.\}
.el \{\
.br
.ie !"\\$2"" \{\
\&\\$1	\\fI\\$2\\fP
.\}
.el \{\
\&\\fI\\$1\\fP
.\}
.\}
..
'\"	# define tabbing values for .AP
.de AS
.nr )A 10n
.if !"\\$1"" .nr )A \\w'\\$1'u+3n
.nr )B \\n()Au+15n
.\"
.if !"\\$2"" .nr )B \\w'\\$2'u+\\n()Au+3n
.nr )C \\n()Bu+\\w'(in/out)'u+2n
..
'\"	# BS - start boxed text
'\"	# ^y = starting y location
'\"	# ^b = 1
.de BS
.br
.mk ^y
.nr ^b 1u
.if n .nf
.if n .ti 0
.if n \l'\\n(.lu\(ul'
.if n .fi
..
'\"	# BE - end boxed text (draw box now)
.de BE
.nf
.ti 0
.mk ^t
.ie n \l'\\n(^lu\(ul'
.el \{\
.\"	Draw four-sided box normally, but don't draw top of
.\"	box if the box started on an earlier page.
.ie !\\n(^b-1 \{\
\h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.el \}\
\h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.\}
.fi
.br
.nr ^b 0
..
'\"	# VS - start vertical sidebar
'\"	# ^Y = starting y location
'\"	# ^v = 1 (for troff;  for nroff this doesn't matter)
.de VS
.mk ^Y
.ie n 'mc \s12\(br\s0
.el .nr ^v 1u
..
'\"	# VE - end of vertical sidebar
.de VE
.ie n 'mc
.el \{\
.ev 2
.nf
.ti 0
.mk ^t
\h'|\\n(^lu+3n'\L'|\\n(^Yu-1v\(bv'\v'\\n(^tu+1v-\\n(^Yu'\h'-|\\n(^lu+3n'
.sp -1
.fi
.ev
.\}
.nr ^v 0
..
'\"	# Special macro to handle page bottom:  finish off current
'\"	# box/sidebar if in box/sidebar mode, then invoked standard
'\"	# page bottom macro.
.de ^B
.ev 2
'ti 0
'nf
.mk ^t
.if \\n(^b \{\
.\"	Draw three-sided box if this is the box's first page,
.\"	draw two sides but no top otherwise.
.ie !\\n(^b-1 \h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.el \h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.\}
.if \\n(^v \{\
.nr ^x \\n(^tu+1v-\\n(^Yu
\kx\h'-\\nxu'\h'|\\n(^lu+3n'\ky\L'-\\n(^xu'\v'\\n(^xu'\h'|0u'\c
.\}
.bp
'fi
.ev
.if \\n(^b \{\
.mk ^y
.nr ^b 2
.\}
.if \\n(^v \{\
.mk ^Y
.\}
..
'\"	# DS - begin display
.de DS
.RS
.nf
.sp
..
'\"	# DE - end display
.de DE
.fi
.RE
.sp .5
..
.HS blt_intro cmds BLT Commands
.BS
'\" Note:  do not modify the .SH NAME line immediately below!
.SH NAME
blt_intro \- Introduction to the BLT library
.BE
.SH DESCRIPTION
BLT is a library of extensions to the Tk library.  It adds new 
commands and variables (all prefixed by "blt_") to the application's 
interpreter.
.LP
.SH COMMANDS
The following commands are added to the interpreter from the BLT library:
.TP 15
\fBblt_drag&drop\fR 
Provides a drag-and-drop facility for Tk.  Information (represented
by a token window) can be dragged to and from any Tk window, including
those of another Tk application.  \fBblt_drag&drop\fR acts as a 
coordinator, directing Tk \fBsend\fR commands between (or within) TCL/Tk 
applications. 
.TP 15
\fBblt_htext\fR 
A simple hypertext widget. Combines text and Tk widgets into a single 
scroll-able window.  Tcl commands can be embedded into text, which are 
invoked as the text is parsed.  In addition, Tk widgets can be appended 
to the window at the current point in the text.  \fBblt_htext\fR can
be also used to create scrolled windows of Tk widgets.  
.TP 15
\fBblt_graph\fR 
A 2D plotting widget.  Plots two variable data in a window with an optional 
legend and annotations.   It has of several components; coordinate axes, 
crosshairs, a legend, and a collection of elements and tags.
.TP 15
\fBblt_barchart\fR 
A barchart widget.  Plots two-variable data as rectangular bars in a 
window.  The x-coordinate values designate the position of the bar along 
the x-axis, while the y-coordinate values designate the magnitude.
The \fBblt_barchart\fR widget has of several components; coordinate axes, 
crosshairs, a legend, and a collection of elements and tags.
.TP 15
\fBblt_table\fR 
A table geometry manager for Tk.  You specify window placements as table 
row,column positions and windows can also span multiple rows or columns. 
It also has many options for setting and/or bounding window sizes.
.TP 15
\fBblt_bitmap\fR 
Reads and writes bitmaps from Tcl.  New X bitmaps can be defined
on-the-fly from Tcl, obviating the need to copy around bitmap files.  
Other options query loaded X bitmap's dimensions and data.
.TP 15
\fBblt_bgexec\fR 
Like Tcl's \fBexec\fR command, \fBblt_bgexec\fR runs a pipeline of Unix 
commands in the background.  Unlike \fBexec\fR, the output of the last
process is collected and a global Tcl variable is set upon its completion.
\fBblt_bgexec\fR can be used with \fBtkwait\fR to wait for Unix commands
to finish while still handling expose events.  Intermediate output is
also available while the pipeline is active.
.TP 15
\fBblt_busy\fR 
Creates a "busy window" which prevents user-interaction when an
application is busy.  The busy window also provides an easy way 
to have temporary busy cursors (such as a watch or hourglass).
.TP 15
\fBblt_win\fR 
Raise, lower, map, or, unmap any window.  The raise and lower functions
are useful for stacking windows above or below "busy windows".
.TP 15
\fBblt_watch\fR 
Arranges for Tcl procedures to be called before and/or after the execution
of every Tcl command. This command
may be used in the logging, profiling, or tracing of Tcl code.
.TP 15
\fBblt_debug\fR 
A simple Tcl command tracing facility useful for debugging Tcl code.  
Displays each Tcl command before and after substitution along its level 
in the interpreter on standard error.
.TP 15
\fBblt_bell\fR 
Rings the keyboard bell.  This is useful when the application can't
write to a tty.
.TP 15
\fBblt_cutbuffer\fR 
Manipulates X cutbuffer properties. This is useful for communicating with
other X applications that don't properly use X selections.
.SH VARIABLES
.PP
The following Tcl variables are either set or used by BLT at various times
in its execution:
.TP 15
\fBblt_library\fR
This variable contains the name of a directory containing a library
of Tcl scripts and other files related to BLT.  Currently, this 
directory contains the \fBblt_drag&drop\fR protocol scripts and the 
PostScript prolog
used by \fBblt_graph\fR and \fBblt_barchart\fR.
The value of this variable is taken from the BLT_LIBRARY environment
variable, if one exists, or else from a default value compiled into
the \fBBLT\fR library.
.TP 15
\fBblt_versions\fR 
This variable is set in the interpreter for each application. It is an 
array of the current version numbers for each 
of the BLT commands in the form \fImajor\fR.\fIminor\fR.  \fIMajor\fR and
\fIminor\fR are integers.  The major version number increases in
any command that includes changes that are not backward compatible
(i.e. whenever existing applications and scripts may have to change to
work with the new release).  The minor version number increases with
each new release of a command, except that it resets to zero whenever the
major version number changes.  The array is indexed by the individual 
command name.
.SH ADDING BLT TO YOUR APPLICATIONS
It's easy to add BLT to an existing Tk application.  BLT requires no 
patches or edits to the Tcl or Tk libraries.  To add BLT, simply add the 
following code snippet to your application's tkAppInit.c file.  
.DS
\fCif (Blt_Init(interp) != TCL_OK) {
    return TCL_ERROR;
}\fR
.DE
Recompile and link with the BLT library (libBLT.a) and that's it.
.SH BUGS
Send bug reports, correspondence, etc. to george.howlett@att.com
.SH KEYWORDS
BLT

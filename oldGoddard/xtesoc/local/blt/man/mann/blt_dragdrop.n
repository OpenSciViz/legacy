'\"
'\" MAN PAGE:  drag&drop widget
'\"   AUTHOR:  Michael J. McLennan
'\"     SCCS:  %W% (%G%)
'\" ========================================================================
'\"                 Copyright (c) 1993  AT&T Bell Laboratories
'\" ========================================================================
'\" Permission to use, copy, modify, and distribute this software and its
'\" documentation for any purpose and without fee is hereby granted,
'\" provided that the above copyright notice appear in all copies and that
'\" both that the copyright notice and warranty disclaimer appear in
'\" supporting documentation, and that the names of AT&T Bell Laboratories
'\" any of their entities not be used in advertising or publicity
'\" pertaining to distribution of the software without specific, written
'\" prior permission.
'\"
'\" AT&T disclaims all warranties with regard to this software, including
'\" all implied warranties of merchantability and fitness.  In no event
'\" shall AT&T be liable for any special, indirect or consequential
'\" damages or any damages whatsoever resulting from loss of use, data or
'\" profits, whether in an action of contract, negligence or other
'\" tortuous action, arising out of or in connection with the use or
'\" performance of this software.
'\" ========================================================================
'\" 
'\" Man page formatting taken from...
'\" Copyright 1992 Regents of the University of California
'\" Permission to use, copy, modify, and distribute this
'\" documentation for any purpose and without fee is hereby
'\" granted, provided that this notice appears in all copies.
'\" The University of California makes no representations about
'\" the suitability of this material for any purpose.  It is
'\" provided "as is" without express or implied warranty.
'\"
.\" The definitions below are for supplemental macros used in Tcl/Tk
.\" manual entries.
.\"
.\" .HS name section [date [version]]
.\"	Replacement for .TH in other man pages.  See below for valid
.\"	section names.
.\"
.\" .AP type name in/out [indent]
.\"	Start paragraph describing an argument to a library procedure.
.\"	type is type of argument (int, etc.), in/out is either "in", "out",
.\"	or "in/out" to describe whether procedure reads or modifies arg,
.\"	and indent is equivalent to second arg of .IP (shouldn't ever be
.\"	needed;  use .AS below instead)
.\"
.\" .AS [type [name]]
.\"	Give maximum sizes of arguments for setting tab stops.  Type and
.\"	name are examples of largest possible arguments that will be passed
.\"	to .AP later.  If args are omitted, default tab stops are used.
.\"
.\" .BS
.\"	Start box enclosure.  From here until next .BE, everything will be
.\"	enclosed in one large box.
.\"
.\" .BE
.\"	End of box enclosure.
.\"
.\" .VS
.\"	Begin vertical sidebar, for use in marking newly-changed parts
.\"	of man pages.
.\"
.\" .VE
.\"	End of vertical sidebar.
.\"
.\" .DS
.\"	Begin an indented unfilled display.
.\"
.\" .DE
.\"	End of indented unfilled display.
.\"
'\"	# Heading for Tcl/Tk man pages
.de HS
.if '\\$2'cmds'       .TH \\$1 1 \\$3 \\$4
.if '\\$2'lib'        .TH \\$1 3 \\$3 \\$4
.if '\\$2'tcl'        .TH \\$1 3 \\$3 \\$4
.if '\\$2'tk'         .TH \\$1 3 \\$3 \\$4
.if t .wh -1.3i ^B
.nr ^l \\n(.l
.ad b
..
'\"	# Start an argument description
.de AP
.ie !"\\$4"" .TP \\$4
.el \{\
.   ie !"\\$2"" .TP \\n()Cu
.   el          .TP 15
.\}
.ie !"\\$3"" \{\
.ta \\n()Au \\n()Bu
\&\\$1	\\fI\\$2\\fP	(\\$3)
.\".b
.\}
.el \{\
.br
.ie !"\\$2"" \{\
\&\\$1	\\fI\\$2\\fP
.\}
.el \{\
\&\\fI\\$1\\fP
.\}
.\}
..
'\"	# define tabbing values for .AP
.de AS
.nr )A 10n
.if !"\\$1"" .nr )A \\w'\\$1'u+3n
.nr )B \\n()Au+15n
.\"
.if !"\\$2"" .nr )B \\w'\\$2'u+\\n()Au+3n
.nr )C \\n()Bu+\\w'(in/out)'u+2n
..
'\"	# BS - start boxed text
'\"	# ^y = starting y location
'\"	# ^b = 1
.de BS
.br
.mk ^y
.nr ^b 1u
.if n .nf
.if n .ti 0
.if n \l'\\n(.lu\(ul'
.if n .fi
..
'\"	# BE - end boxed text (draw box now)
.de BE
.nf
.ti 0
.mk ^t
.ie n \l'\\n(^lu\(ul'
.el \{\
.\"	Draw four-sided box normally, but don't draw top of
.\"	box if the box started on an earlier page.
.ie !\\n(^b-1 \{\
\h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.el \}\
\h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.\}
.fi
.br
.nr ^b 0
..
'\"	# VS - start vertical sidebar
'\"	# ^Y = starting y location
'\"	# ^v = 1 (for troff;  for nroff this doesn't matter)
.de VS
.mk ^Y
.ie n 'mc \s12\(br\s0
.el .nr ^v 1u
..
'\"	# VE - end of vertical sidebar
.de VE
.ie n 'mc
.el \{\
.ev 2
.nf
.ti 0
.mk ^t
\h'|\\n(^lu+3n'\L'|\\n(^Yu-1v\(bv'\v'\\n(^tu+1v-\\n(^Yu'\h'-|\\n(^lu+3n'
.sp -1
.fi
.ev
.\}
.nr ^v 0
..
'\"	# Special macro to handle page bottom:  finish off current
'\"	# box/sidebar if in box/sidebar mode, then invoked standard
'\"	# page bottom macro.
.de ^B
.ev 2
'ti 0
'nf
.mk ^t
.if \\n(^b \{\
.\"	Draw three-sided box if this is the box's first page,
.\"	draw two sides but no top otherwise.
.ie !\\n(^b-1 \h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.el \h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.\}
.if \\n(^v \{\
.nr ^x \\n(^tu+1v-\\n(^Yu
\kx\h'-\\nxu'\h'|\\n(^lu+3n'\ky\L'-\\n(^xu'\v'\\n(^xu'\h'|0u'\c
.\}
.bp
'fi
.ev
.if \\n(^b \{\
.mk ^y
.nr ^b 2
.\}
.if \\n(^v \{\
.mk ^Y
.\}
..
'\"	# DS - begin display
.de DS
.RS
.nf
.sp
..
'\"	# DE - end display
.de DE
.fi
.RE
.sp .5
..
.HS blt_drag&drop cmds
.BS
'\" Note:  do not modify the .SH NAME line immediately below!
.SH NAME
blt_drag&drop \- facilities for handling drag&drop data transfers
.SH SYNOPSIS
\fBblt_drag&drop source
.br
\fBblt_drag&drop source \fIwindow \fR?\fBconfig \fIoptions\fR?
.br
\fBblt_drag&drop source \fIwindow \fBhandler \fR?\fIdataType command\fR ...?
.sp
\fBblt_drag&drop target
.br
\fBblt_drag&drop target \fIwindow \fBhandler \fR?\fIdataType command\fR ...?
.sp
\fBblt_drag&drop target \fIwindow \fBhandle \fIdataType
.sp
\fBblt_drag&drop drag \fIwindow x y
.br
\fBblt_drag&drop drop \fIwindow x y
.br
\fBblt_drag&drop active
.br
\fBblt_drag&drop errors \fR?\fIproc\fR?
.br
\fBblt_drag&drop location \fR?\fIx y\fR?
.BE

.SH DESCRIPTION
.PP
The \fBblt_drag&drop\fR command provides access to a set of facilities
for managing drag-and-drop data transfers.  Any of the usual Tk widgets can
be registered to participate in the drag-and-drop process.  Widgets
registered as a drag&drop \fIsource\fP can export data to other widgets
registered as a drag&drop \fItarget\fP.  Note that a particular widget
can be registered as a source, as a target, or as both.
.PP
The drag-and-drop process begins when the user clicks and holds a mouse
button in a source window; a token window appears with an icon or message
to represent the data being transferred.  As the user moves the mouse pointer,
the token window follows along, acting as a movable packet of data.
Whenever the mouse pointer falls on a valid target window, the border of the
token window is changed to a raised (active) state.  When the mouse button is
released over the target window, a Tcl routine is invoked to send the data
to the desired application.  If this communication process fails,
a rejection symbol (a circle with a line through it) is displayed
on the token window to indicate failure.
.PP
The details of the communication process are fully configurable by the
application developer.  The general philosophy, however, is that the
sender should invoke a series of commands in the remote application to
transfer data into a buffer, and then request that the receiver "handle"
the data.  For example, the sender might invoke a command like
\fB"set DragDrop(number) 12.3"\fP in the target application.
The sender would then request that the target "handle" a "number".  This
separation between the data sent and the means of handling it is critical.
It allows the developer a simple means of having several targets that all
receive "numbers" but use them in different ways.
.PP
Both sources and targets must be registered with a list of "handlers"
defining the data types that they recognize.  As a token window is
dragged from its source to various targets, each target is checked to
see if it recognizes a "handler" offered by the source.  If it does,
it is treated as a "valid" target.  Otherwise, it is ignored.  This
scheme allows the same source to interact with many different kinds of
targets.  A source for RGB color samples, for instance, might have
"color" and "text" handlers.  This would allow it to communicate with
"color" targets (sending RGB data) as well as entry widgets (sending
strings of the form "#rrggbb").
.PP
For drag&drop to be effective across many applications, handlers for
\fIsending\fR various data types should be common.  For this reason,
a directory "demos/dd_protocols" containing a number of example handlers
is included with this distribution.  Developers are encouraged to
use this library and contribute new functions.
.PP
This introduction was presented as a brief overview of the communication
process; further details are presented below:
.TP
\fBblt_drag&drop source\fR
Returns a list of path names for widgets registered as drag&drop
sources.  Returns an empty string if no widgets have been registered.
.TP
\fBblt_drag&drop source \fIwindow \fR?\fBconfig \fIoptions\fR?
Registers a new drag&drop source window with the given options, or
modifies the options for an existing window:
.RS
.LP
.nf
Name:	\fBbuttonBinding\fR
Class:	\fBButtonBinding\fR
Switch:	\fB\-button\fR \fIn\fR
.fi
.IP
Specifies the mouse button (integer 1-5) that will invoke the drag&drop
operation on the source window.  This causes the following bindings to
be added to the widget:
.sp
.nf
.RS
\f(CWbind \fIwin\fP <ButtonPress-\fIn\fP> {blt_drag&drop drag %W %X %Y}
\f(CWbind \fIwin\fP <B\fIn\fP-Motion> {blt_drag&drop drag %W %X %Y}
\f(CWbind \fIwin\fP <ButtonRelease-\fIn\fP> {blt_drag&drop drop %W %X %Y}\fR
.RE
.fi
.sp
The default value is button 3.  If the value "0" is specified, then no
bindings are added; this enables the user to establish bindings manually.
.LP
.nf
Name:	\fBpackageCommand\fR
Class:	\fBCommand\fR
Switch:	\fB\-packagecmd \fIprefix\fR
.fi
.IP
Specifies a Tcl command used to establish the appearance of the token window
at the start of each drag&drop operation.  This command is automatically
invoked by the \fBblt_drag&drop drag\fP command whenever the token window
is about to be mapped for a drag operation.  The path name for the token
window is automatically appended to the command before it is executed:
.sp
.nf
.RS
\fIpackagecmd token\fR
.RE
.fi
.sp
Typically, this command is the name of a procedure (taking the token window
argument) which will pack a label--or update the text of a label--in the
token window.
.LP
The return value from the package command is later passed as an argument
to the send handler; thus, it is the responsibility of the package command
to determine what information will later be sent.  For complex sources,
such as a listbox containing a number of items, the package command can use
the \fBblt_drag&drop location\fR command to determine the part of a widget
that a user has selected and extract a value.  For example, the following
package routine will select an item from a listbox and configure the token
window to display the selected string:
.sp
.nf
.RS
\f(CWproc package_list_item {lbox token} {
    if {[winfo children $token] == ""} {
        label $token.value
        pack $token.value
    }
    set xy [blt_drag&drop location]
    set y  [expr [lindex $xy 1]-[winfo rooty $lbox]]

    set str [$lbox get [$lbox nearest $y]]
    $token.value config -text $str
    return $str
}\fR
.RE
.fi
.sp
If the package command returns an empty string, the drag operation is
quietly aborted.  This can be used to disallow drag&drop operations from
certain parts of a widget, if the drag position is inappropriate.
.LP
.nf
Name:	\fBrejectBackground\fR
Class:	\fBBackground\fR
Switch:	\fB\-rejectbg \fIcolor\fR
.fi
.IP
Specifies the color used to draw the background of the rejection symbol
on the token window.  The rejection symbol (a circle with a line through
it--the international "no") appears whenever communication fails.
.LP
.nf
Name:	\fBrejectForeground\fR
Class:	\fBForeground\fR
Switch:	\fB\-rejectfg \fIcolor\fR
.fi
.IP
Specifies the color used to draw the foreground of the rejection symbol
on the token window.
.LP
.nf
Name:	\fBrejectStipple\fR
Class:	\fBStipple\fR
Switch:	\fB\-rejectstipple \fIpattern\fR
.fi
.IP
Specifies a stipple pattern used to draw the foreground of the rejection
symbol on the token window.  Any of the forms acceptable to Tk_GetBitmap
can be used.
.LP
.nf
Name:	\fBselfTarget\fR
Class:	\fBSelfTarget\fR
Switch:	\fB\-selftarget \fIboolean\fR
.fi
.IP
If the \fIboolean\fR value is true, and if a source widget is also
registered as a compatible target, then the source will be able to transmit
to itself during drag&drop operations.  This is primarily useful for
complex sources such as a canvas widget, where items may be moved from
place to place within the same widget.  By default, this option is disabled.
.LP
.nf
Name:	\fBsend\fR
Class:	\fBSend\fR
Switch:	\fB\-send \fIlist\fR
.fi
.IP
Specifies a \fIlist\fR of \fIdataTypes\fR enabled for communication.  Only
\fIdataTypes\fR defined by commands of the form "\fBblt_drag&drop source
\fIwindow \fBhandler \fR?\fIdataType command\fR ...?" are allowed.
This list also determines the priority of the various \fIdataTypes\fR.
When a token window is over a potential drag&drop target, this list is
searched from start to finish for a \fIdataType\fR that is also recognized
by the target.  The first matching \fIdataType\fR found determines the
value that will be sent if the token is dropped.  If no matching \fIdataType\fR
is found, then the target is incompatible, and is ignored.  By default,
this option has the value "all", indicating that all \fIdataTypes\fR should
be considered in the order that they were defined for the source.
.LP
Note that this option makes it easy to control a drag&drop source.  Setting
the value to an empty string disables the source; setting the value back
to "all" restores communication.
.LP
.nf
Name:	\fBsiteCommand\fR
Class:	\fBCommand\fR
Switch:	\fB\-sitecmd \fIprefix\fR
.fi
.IP
Specifies a Tcl command used to update the appearance of the token window.
If specified, this command is automatically invoked by the
\fBblt_drag&drop drag\fP command whenever the token window is over a
compatible drag&drop target.  Two arguments are appended to this command:
.sp
.nf
.RS
\fIsitecmd state token\fR
.RE
.fi
.sp
\fIstate\fR is an integer with a non-zero value whenever the token
window is over a compatible target, and \fItoken\fR is the path for the
token window.  Regardless of this command, border of the token window
will become raised whenever the token is over a valid target.  This
command can be used to display other visual cues.
.LP
.nf
Name:	\fBtokenAnchor\fR
Class:	\fBAnchor\fR
Switch:	\fB\-tokenanchor \fIanchor\fR
.fi
.IP
Specifies how the token window is positioned relative to the mouse
pointer coordinates passed to the \fBblt_drag&drop drag\fP command.
Must be one of the values n, s, e, w, center, nw, ne, sw or se.
For example, "nw" means to position the token such that its upper-left
corner is at the mouse pointer.  The default value is "center".
.LP
.nf
Name:	\fBtokenBackground\fR
Class:	\fBBackground\fR
Switch:	\fB\-tokenbg \fIcolor\fR
.fi
.IP
Specifies the color used to draw the background of the token window.
.LP
.nf
Name:	\fBtokenBorderWidth\fR
Class:	\fBBorderWidth\fR
Switch:	\fB\-tokenborderwidth \fIsize\fR
.fi
.IP
Specifies the width in pixels of the border around the token window.
This border becomes raised to indicate when the token is over a compatible
drag&drop target site.  The value may have any of the forms acceptable
to Tk_GetPixels.  The default value is "3".
.LP
.nf
Name:	\fBtokenCursor\fR
Class:	\fBCursor\fR
Switch:	\fB\-tokencursor \fIcursor\fR
.fi
.IP
Specifies the cursor used when a token window is active.  The value
may have any of the forms acceptable to Tk_GetCursor.  The default
value is "center_ptr".
.RE
.TP
\fBblt_drag&drop source \fIwindow \fBhandler \fR?\fIdataType command\fR ...?
Defines one or more \fIdataTypes\fR available for communication, and the
\fIcommands\fR used to send them to remote applications.  If no \fIdataTypes\fR
are specified, then the list of previously defined \fIdataTypes\fR is returned.
.LP
Each \fIcommand\fR represents a Tcl command prefix.  Three arguments are
appended to this prefix when communication is initiated for a particular
\fIdataType\fR:
.sp
.nf
.RS
\fIcommand interp target data\fR
.RE
.fi
.sp
\fIinterp\fR is the name of the interpreter containing the target widget;
\fItarget\fR is the name of the target widget; and \fIdata\fR is the data
string returned from the "package" command invoked at the start of the
drag&drop operation.
.LP
A typical source handler contains one or more "send" commands which
transfer data to the remote application, followed by a "\fBblt_drag&drop
... handle\fR command" (described below) requesting that the target
widget handle the incoming data.  An example source handler for a "text"
data type is shown below:
.sp
.nf
.RS
\f(CWproc dd_send_text {interp ddwin data} {
    send $interp "
        global DragDrop
        set DragDrop(text) {$data}
    "
    send $interp "blt_drag&drop target $ddwin handle text"
}\fR
.RE
.fi
.sp
This handler transfers the text string to a global variable DragDrop(text)
in the target interpreter, and then requests that the target "handle" the
incoming data.  This separation between the transfer and the handling of
the data is important.  It allows the same source handler to transfer data
for many different targets, and it allows each of the targets to handle
the incoming data differently.  If an error is encountered during the
communication process, the rejection symbol is posted on the token window
to indicate failure.
.RE
.sp
.TP
\fBblt_drag&drop target\fR
Returns a list of path names for widgets registered as drag&drop
targets.  Returns an empty string if no widgets have been registered.
.TP
\fBblt_drag&drop target \fIwindow \fBhandler \fR?\fIdataType command\fR ...?
Registers a new drag&drop target window with the given handlers, or
modifies the handlers for an existing window.  If no handlers are
specified, this command returns the current list of recognized
\fIdataType\fR strings.  Each \fIdataType\fR is a symbolic name
representing a form of data, and the corresponding \fIcommand\fR is
a Tcl command that specifies how the target will make use of the data.
This command is invoked indirectly whenever a send routine transmits
data and then issues the "blt_drag&drop ... handle \fIdataType\fR" command
to make use of it.
.TP
\fBblt_drag&drop target \fIwindow \fRhandle \fIdataType
Searches for the given \fIdataType\fR name among the handlers registered
for the target \fIwindow\fR, and invokes the appropriate \fIcommand\fR.
If the \fIdataType\fR name is not recognized, this command returns an
error.  Typically, this command is invoked by one of the send handlers
for a drag&drop source.
.TP
\fBblt_drag&drop drag \fIwindow x y\fR
Marks the start of (or movement during) a drag&drop operation.  If
the token window is unmapped when this command is invoked, then the
\fB\-packagecmd\fR for the source \fIwindow\fR is executed.  If this
command is successful and returns a non-null string, the token window
is mapped.  On subsequent calls, the token window is moved to the new
\fIx y\fR location.  Unless the "\fB\-button 0\fR" option is specified for
the source, this command is automatically bound to <ButtonPress-\fIn\fR>
and <B\fIn\fR-Motion> events for "\fB\-button \fIn\fR" of the source widget.
.TP
\fBblt_drag&drop drop \fIwindow x y\fR
Marks the end of a drag&drop operation.  If the mouse pointer is
over a compatible target window, then the appropriate send handler for
the first compatible \fIdataType\fR is invoked to handle the data transfer.
If the data transfer is successful, then the token window is unmapped;
otherwise, a rejection symbol is drawn on the token window, and the window
is unmapped after a small delay.  Unless the "\fB\-button 0\fR" option is
specified for the source, this command is automatically bound to the
<ButtonRelease-\fIn\fR> event for "\fB\-button \fIn\fR" of the source widget.
.TP
\fBblt_drag&drop active\fR
Returns "1" if a drag&drop operation is in progress, and "0" otherwise.
A drag&drop operation officially starts after the package command has
been executed successfully, and ends after the send handler has been
executed (successfully or otherwise).
.TP
\fBblt_drag&drop errors \fR?\fIproc\fR?
Specifies a Tcl \fIproc\fR used to handle errors encountered during
drag&drop operations.  If a \fIproc\fR is not specified, this command
returns the current error handler.  By default, all errors are sent
to the usual \fBtkerror\fR command, and therefore appear in a dialog
box to the user.  This behavior is quite useful when debugging
communication protocols, but may not be desirable in a finished
application.  Errors can be suppressed entirely (leaving the rejection
symbol as the only error indicator) by specifying a null string in
place of the \fIproc\fR name.
.TP
\fBblt_drag&drop location \fR?\fIx y\fR?
Used to set or query the pointer location during a drag&drop operation.
The \fIx y\fR arguments specify the current location; if these arguments
are missing, then the last reported (x,y) location is returned as a list
with two elements.  This command is issued automatically within the
\fBblt_drag&drop drag\fR and \fBblt_drag&drop drop\fR commands, to
keep track of pointer movement.

.SH KEYWORDS
drag&drop, send, bind, widget

'\"
'\" Copyright 1994 by AT&T Bell Laboratories.
'\"
'\" Permission to use, copy, modify, and distribute this software
'\" and its documentation for any purpose and without fee is hereby
'\" granted, provided that the above copyright notice appear in all
'\" copies and that both that the copyright notice and warranty
'\" disclaimer appear in supporting documentation, and that the
'\" names of AT&T Bell Laboratories any of their entities not be used
'\" in advertising or publicity pertaining to distribution of the
'\" software without specific, written prior permission.
'\"
'\" AT&T disclaims all warranties with regard to this software, including
'\" all implied warranties of merchantability and fitness.  In no event
'\" shall AT&T be liable for any special, indirect or consequential
'\" damages or any damages whatsoever resulting from loss of use, data
'\" or profits, whether in an action of contract, negligence or other
'\" tortuous action, arising out of or in connection with the use or
'\" performance of this software.
'\"
'\"
.\" The definitions below are for supplemental macros used in Tcl/Tk
.\" manual entries.
.\"
.\" .HS name section [date [version]]
.\"	Replacement for .TH in other man pages.  See below for valid
.\"	section names.
.\"
.\" .AP type name in/out [indent]
.\"	Start paragraph describing an argument to a library procedure.
.\"	type is type of argument (int, etc.), in/out is either "in", "out",
.\"	or "in/out" to describe whether procedure reads or modifies arg,
.\"	and indent is equivalent to second arg of .IP (shouldn't ever be
.\"	needed;  use .AS below instead)
.\"
.\" .AS [type [name]]
.\"	Give maximum sizes of arguments for setting tab stops.  Type and
.\"	name are examples of largest possible arguments that will be passed
.\"	to .AP later.  If args are omitted, default tab stops are used.
.\"
.\" .BS
.\"	Start box enclosure.  From here until next .BE, everything will be
.\"	enclosed in one large box.
.\"
.\" .BE
.\"	End of box enclosure.
.\"
.\" .VS
.\"	Begin vertical sidebar, for use in marking newly-changed parts
.\"	of man pages.
.\"
.\" .VE
.\"	End of vertical sidebar.
.\"
.\" .DS
.\"	Begin an indented unfilled display.
.\"
.\" .DE
.\"	End of indented unfilled display.
.\"
'\"	# Heading for Tcl/Tk man pages
.de HS
.if '\\$2'cmds'       .TH \\$1 1 \\$3 \\$4
.if '\\$2'lib'        .TH \\$1 3 \\$3 \\$4
.if '\\$2'tcl'        .TH \\$1 3 \\$3 \\$4
.if '\\$2'tk'         .TH \\$1 3 \\$3 \\$4
.if t .wh -1.3i ^B
.nr ^l \\n(.l
.ad b
..
'\"	# Start an argument description
.de AP
.ie !"\\$4"" .TP \\$4
.el \{\
.   ie !"\\$2"" .TP \\n()Cu
.   el          .TP 15
.\}
.ie !"\\$3"" \{\
.ta \\n()Au \\n()Bu
\&\\$1	\\fI\\$2\\fP	(\\$3)
.\".b
.\}
.el \{\
.br
.ie !"\\$2"" \{\
\&\\$1	\\fI\\$2\\fP
.\}
.el \{\
\&\\fI\\$1\\fP
.\}
.\}
..
'\"	# define tabbing values for .AP
.de AS
.nr )A 10n
.if !"\\$1"" .nr )A \\w'\\$1'u+3n
.nr )B \\n()Au+15n
.\"
.if !"\\$2"" .nr )B \\w'\\$2'u+\\n()Au+3n
.nr )C \\n()Bu+\\w'(in/out)'u+2n
..
'\"	# BS - start boxed text
'\"	# ^y = starting y location
'\"	# ^b = 1
.de BS
.br
.mk ^y
.nr ^b 1u
.if n .nf
.if n .ti 0
.if n \l'\\n(.lu\(ul'
.if n .fi
..
'\"	# BE - end boxed text (draw box now)
.de BE
.nf
.ti 0
.mk ^t
.ie n \l'\\n(^lu\(ul'
.el \{\
.\"	Draw four-sided box normally, but don't draw top of
.\"	box if the box started on an earlier page.
.ie !\\n(^b-1 \{\
\h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.el \}\
\h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.\}
.fi
.br
.nr ^b 0
..
'\"	# VS - start vertical sidebar
'\"	# ^Y = starting y location
'\"	# ^v = 1 (for troff;  for nroff this doesn't matter)
.de VS
.mk ^Y
.ie n 'mc \s12\(br\s0
.el .nr ^v 1u
..
'\"	# VE - end of vertical sidebar
.de VE
.ie n 'mc
.el \{\
.ev 2
.nf
.ti 0
.mk ^t
\h'|\\n(^lu+3n'\L'|\\n(^Yu-1v\(bv'\v'\\n(^tu+1v-\\n(^Yu'\h'-|\\n(^lu+3n'
.sp -1
.fi
.ev
.\}
.nr ^v 0
..
'\"	# Special macro to handle page bottom:  finish off current
'\"	# box/sidebar if in box/sidebar mode, then invoked standard
'\"	# page bottom macro.
.de ^B
.ev 2
'ti 0
'nf
.mk ^t
.if \\n(^b \{\
.\"	Draw three-sided box if this is the box's first page,
.\"	draw two sides but no top otherwise.
.ie !\\n(^b-1 \h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.el \h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.\}
.if \\n(^v \{\
.nr ^x \\n(^tu+1v-\\n(^Yu
\kx\h'-\\nxu'\h'|\\n(^lu+3n'\ky\L'-\\n(^xu'\v'\\n(^xu'\h'|0u'\c
.\}
.bp
'fi
.ev
.if \\n(^b \{\
.mk ^y
.nr ^b 2
.\}
.if \\n(^v \{\
.mk ^Y
.\}
..
'\"	# DS - begin display
.de DS
.RS
.nf
.sp
..
'\"	# DE - end display
.de DE
.fi
.RE
.sp .5
..
.HS blt_watch cmds
.BS
.SH NAME
blt_watch \- call Tcl procedures before and after each command
.SH SYNOPSIS
\fBblt_watch create\fR \fIname\fR ?\fIoptions\fR?
.sp
\fBblt_watch activate\fR \fIname\fR
.sp
\fBblt_watch deactivate\fR \fIname\fR
.sp
\fBblt_watch delete\fR \fIname\fR
.sp
\fBblt_watch configure\fR \fIname\fR ?\fIoptions\fR
.sp
\fBblt_watch info\fR \fIname\fR
.sp
\fBblt_watch names\fR 
.BE
.SH DESCRIPTION
.PP
This command arranges for Tcl procedures to be called before 
and/or after each the execution of each Tcl command.  This command
may be useful in the logging, profiling, or tracing of Tcl code.
.SH "WATCH COMMANDS"
The following commands are available for the \fBblt_watch\fR:
.TP
\fBblt_watch activate \fIname\fR 
Activates the watch, causing Tcl commands the be traced to the
maximum depth selected.
.TP
\fBblt_watch create \fIname\fR ?\fIoptions\fR?...
Creates a new watch \fIname\fR. It's an error if another watch 
\fIname\fR already exists and an error message will be returned.
\fIOptions\fR may have any of the values accepted by the 
\fBblt_watch configure\fR command.
This command returns the empty string.  
.TP
\fBblt_watch configure \fIname\fR ?\fIoptions...\fR?
Queries or modifies the configuration options of the watch \fIname\fR.
\fIName\fR is the name of a watch.
\fIOptions\fR may have any of the following values:
.RS
.TP
\fB\-active \fIboolean\fR
Specifies if the watch is active.
By default, watches are active when created.
.TP
\fB\-postcmd \fIstring\fR
Specifies a Tcl procedure to be called after each
Tcl command.  \fIString\fR is name of a Tcl procedure and optionally
any extra arguments to be passed to it.  Five arguments are appended:
1) the current level 2) the command string 3) a list containing the
command after substitutions and split into words 4) the return code
of the previous Tcl command, and 5) the results of the previous command.
The return status of the postcmd procedure is always ignored.
.TP
\fB\-precmd \fIstring\fR
Specifies a Tcl procedure to be called before each
Tcl command.
\fIString\fR is name of a Tcl procedure and optionally
any extra arguments to be passed to it.  Three arguments are appended:
1) the current level 2) the command string, and  3) a list containing the
command after substitutions and split into words.
The return status of the precmd procedure is always ignored.
.TP
\fB\-maxlevel \fInumber\fR
Specifies the maximum evaluation depth to watch Tcl commands.
The default maximum level is 10000.
.TP
\fBblt_watch deactivate \fIname\fR 
Deactivates the watch, causing Tcl commands to be no longer traced.
.TP
\fBblt_watch info \fIname\fR 
Returns the configuration information associated with the 
watch \fIname\fR.  \fIName\fR is the name of a watch.
.TP
\fBblt_watch names\fR ?\fIhow\fR?
Lists the names of the watches for a given state.
\fIHow\fR may be one of the following: \fCactive\fR, \fCidle\fR, 
or \fCignore\fR.  If no \fIhow\fR argument is specified all, watches are
listed.
.RE
.PP
If no \fIlevel\fR argument is given, the current level is printed.
.SH EXAMPLE
The following example use \fBblt_watch\fR to trace Tcl commands 
(printing to standard error) both before and after they are executed. 
.DS 
\fC
proc preCmd { level command argv } {
    set name [lindex $argv 0]
    puts stderr "$level $name => $command"
}

proc postCmd { level command argv retcode results } {
    set name [lindex $argv 0]
    puts stderr "$level $name => $argv\n<= ($retcode) $results"
}

blt_watch create trace \
	-postcmd postCmd -precmd preCmd
\fR
.DE
.SH KEYWORDS
debug

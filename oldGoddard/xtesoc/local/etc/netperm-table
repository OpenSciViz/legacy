#
# Sample netperm configuration table
#
# To get a good sample working netperm-table, just globally
# substitute YOURNET for your network address (e.g.; 666.777.888)
#

# Example netacl rules:
# ---------------------
# if the next 2 lines are uncommented, people can get a login prompt
# on the firewall machine through the telnet proxy
#netacl-telnetd: permit-hosts 127.0.0.1 -exec /usr/libexec/telnetd
#netacl-telnetd: permit-hosts YOURADDRESS 198.6.73.2 -exec /usr/libexec/telnetd
#
# if the next line is uncommented, the telnet proxy is available
#netacl-telnetd: permit-hosts * -exec /usr/local/etc/tn-gw
#
# if the next 2 lines are uncommented, people can get a login prompt
# on the firewall machine through the rlogin proxy
#netacl-rlogind: permit-hosts 127.0.0.1 -exec /usr/libexec/rlogind -a
#netacl-rlogind: permit-hosts YOURADDRESS 198.6.73.2 -exec /usr/libexec/rlogind -a
#
# if the next line is uncommented, the rlogin proxy is available
#netacl-rlogind: permit-hosts * -exec /usr/local/etc/rlogin-gw

#
# to enable finger service uncomment these 2 lines
#netacl-fingerd: permit-hosts YOURNET.* -exec /usr/libexec/fingerd
#netacl-fingerd: permit-hosts * -exec /bin/cat /usr/local/etc/finger.txt

# Example smap rules:
# -------------------
smap, smapd:	userid 6
smap, smapd:	directory /var/spool/smap
smapd:		executable /usr/local/etc/smapd
smapd:		sendmail /usr/sbin/sendmail
smap:		timeout 3600

# Example ftp gateway rules:
# --------------------------
#ftp-gw:	denial-msg	/usr/local/etc/ftp-deny.txt
#ftp-gw:	welcome-msg	/usr/local/etc/ftp-welcome.txt
#ftp-gw:	help-msg	/usr/local/etc/ftp-help.txt
ftp-gw:		timeout 3600
# uncomment the following line if you want internal users to be
# able to do FTP with the internet
#ftp-gw:		permit-hosts YOURNET.*
# uncomment the following line if you want external users to be
# able to do FTP with the internal network using authentication
#ftp-gw:		permit-hosts * -authall -log { retr stor }

# Example telnet gateway rules:
# -----------------------------
#tn-gw:		denial-msg	/usr/local/etc/tn-deny.txt
#tn-gw:		welcome-msg	/usr/local/etc/tn-welcome.txt
#tn-gw:		help-msg	/usr/local/etc/tn-help.txt
tn-gw:		timeout 3600
tn-gw:		permit-hosts YOURNET.* -passok -xok
# if this line is uncommented incoming traffic is permitted WITH
# authentication required
#tn-gw:		permit-hosts * -auth

# Example rlogin gateway rules:
# -----------------------------
#rlogin-gw:	denial-msg	/usr/local/etc/rlogin-deny.txt
#rlogin-gw:	welcome-msg	/usr/local/etc/rlogin-welcome.txt
#rlogin-gw:	help-msg	/usr/local/etc/rlogin-help.txt
rlogin-gw:	timeout 3600
rlogin-gw:	permit-hosts YOURNET.* -passok -xok
# if this line is uncommented incoming traffic is permitted WITH
# authentication required
#rlogin-gw:	permit-hosts * -auth -xok


# Example auth server and client rules
# ------------------------------------
authsrv:	hosts 127.0.0.1
authsrv:	database /usr/local/etc/fw-authdb
authsrv:	badsleep 1200
authsrv:	nobogus true

# clients using the auth server
*:		authserver 127.0.0.1 7777

# X-forwarder rules
tn-gw, rlogin-gw:	xforwarder /usr/local/etc/x-gw

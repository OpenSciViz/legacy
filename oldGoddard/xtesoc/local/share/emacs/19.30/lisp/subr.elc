;ELC   
;;; compiled by rms@mole.gnu.ai.mit.edu on Sat Oct  7 00:05:15 1995
;;; from file /home/fsf/rms/e19/lisp/subr.el
;;; emacs version 19.29.15.
;;; bytecomp version FSF 2.10
;;; optimization is on.
;;; this file uses opcodes which do not exist in Emacs 18.

(if (and (boundp 'emacs-version)
	 (or (and (boundp 'epoch::version) epoch::version)
	     (string-lessp emacs-version "19.28.90")))
    (error "`subr.el' was compiled for Emacs 19.29 or later"))


#@702 Return a lambda expression.
A call of the form (lambda ARGS DOCSTRING INTERACTIVE BODY) is
self-quoting; the result of evaluating the lambda expression is the
expression itself.  The lambda expression may then be treated as a
function, i.e., stored as the function value of a symbol, passed to
funcall or mapcar, etc.

ARGS should take the same form as an argument list for a `defun'.
DOCSTRING is an optional documentation string.
 If present, it should describe how to call the function.
 But documentation strings are usually not useful in nameless functions.
INTERACTIVE should be a call to the function `interactive', which see.
It may also be omitted.
BODY should be a list of lisp expressions.
(defalias 'lambda '(macro . #[(&rest cdr) "��\nBD�" [function lambda cdr] 3 (#$ . 468)]))
(defalias 'undefined #[nil "� �" [ding] 1 nil nil])
(put (quote undefined) (quote suppress-keymap) t)
#@218 Make MAP override all normally self-inserting keys to be undefined.
Normally, as an exception, digits and minus-sign are set to make prefix args,
but optional second arg NODIGITS non-nil treats them like other chars.
(defalias 'suppress-keymap #[(map &optional nodigits) "���\f$��0 ����#����X�/ ��!�#�T�� )�" [substitute-key-definition self-insert-command undefined map global-map nodigits nil loop define-key "-" negative-argument 48 57 char-to-string digit-argument] 6 (#$ . 1368)])
#@47 Used internally by substitute-key-definition.
(defvar key-substitution-in-progress nil (#$ . 1874))
#@276 Replace OLDDEF with NEWDEF for any keys in KEYMAP now defined as OLDDEF.
In other words, OLDDEF is replaced with NEWDEF where ever it appears.
If optional fourth argument OLDMAP is specified, we redefine
in KEYMAP as NEWDEF those chars which are defined as OLDDEF in OLDMAP.
(defalias 'substitute-key-definition #[(olddef newdef keymap &optional oldmap prefix) "� �\n� ��!�\"	\f\nB\n\f:��\f@:�� \f@@\f@A\f�\fI�	G\fI�Ɖ�;�` @BA��J :�r @:�r A�9�� �!�� K��w =�� �	��#��� �!�� �	\"�?�� �!)�� \n>�� �	%�,���\f@!��\f@�G��W��H\f�\fI�	G\fI�Ɖ�;�&@BA��:�8@:�8A�9�Q�!�QK��==�h�	��#����!���	\"�?��!)��\n>���	%�,T��� +\fA�� ,�" [prefix "" oldmap keymap scan vector nil vec1 vconcat prefix1 key-substitution-in-progress defn char 0 skipped inner-def fboundp olddef define-key newdef keymapp lookup-key elt substitute-key-definition arrayp array len i] 7 (#$ . 1981)])
#@377 Add binding in KEYMAP for KEY => DEFINITION, right after AFTER's binding.
This is like `define-key' except that the binding for KEY is placed
just after the binding for the event AFTER, instead of at the beginning
of the map.
The order matters when the keymap is used as a menu.
KEY must contain just one event type--that is to say, it must be
a string or vector of length 1.
(defalias 'define-key-after #[(keymap key definition after) "�	!� ���	D\"�\fG�V� ��!�	ȉ\f�H\n\f\f?�� �� A@�\n=�C �AA��@�=�\\ A@�=�\\ A�~ A@�=�h �\f�{ \f�HBAB���A�& ,�" [keymapp keymap signal wrong-type-argument key 1 error "multi-event key specified in `define-key-after'" nil 0 first inserted done tail after t definition] 5 (#$ . 3040)])
#@147 Translate character FROM to TO at a low level.
This function creates a `keyboard-translate-table' if necessary
and then modifies one entry in it.
(defalias 'keyboard-translate #[(from to) "�	!� �	GV� \f	GV�: 	G	��Z�\"P	�W�6 	�I�T��$ 	*	\fI�" [arrayp keyboard-translate-table "" from to i make-string 256 0 table] 5 (#$ . 3805)])
#@148 Default global keymap mapping Emacs keyboard input into commands.
The value is a keymap which is usually (but not necessarily) Emacs's
global map.
(defvar global-map nil (#$ . 4156))
#@117 Default keymap for ESC (meta) commands.
The normal global definition of the character ESC indirects to this keymap.
(defvar esc-map nil (#$ . 4346))
#@110 Default keymap for C-x commands.
The normal global definition of the character C-x indirects to this keymap.
(defvar ctl-x-map nil (#$ . 4502))
#@33 Keymap for subcommands of C-x 4
(defvar ctl-x-4-map (make-sparse-keymap) (#$ . 4652))
(byte-code "��\n\"��\f��#�" [defalias ctl-x-4-prefix ctl-x-4-map define-key ctl-x-map "4"] 4)
#@28 Keymap for frame commands.
(defvar ctl-x-5-map (make-sparse-keymap) (#$ . 4838))
(byte-code "��\n\"��\f��#����!�\"\n�B��" [defalias ctl-x-5-prefix ctl-x-5-map define-key ctl-x-map "5" logior read "?\\M-\\^@" 128 listify-key-sequence-1 current-load-list] 4)
#@45 Convert a key sequence to a list of events.
(defalias 'listify-key-sequence #[(key) "�	!� �	�\"����	�\"\"�" [vectorp key append nil mapcar #[(c) "�V� �\"��" [c 127 logxor listify-key-sequence-1] 3]] 5 (#$ . 5106)])
#@42 True if the argument is an event object.
(defalias 'eventp #[(obj) "�� 9� �N� :� @9� @�N�" [obj event-symbol-elements] 2 (#$ . 5333)])
(put (quote eventp) (quote byte-optimizer) (quote byte-compile-inline-expand))
#@213 Returns a list of symbols representing the modifier keys in event EVENT.
The elements of the list may include `meta', `control',
`shift', `hyper', `super', `alt', `click', `double', `triple', `drag',
and `down'.
(defalias 'event-modifiers #[(event) "�<�\n 	@	9� 	�NA�~ ���	�\"!�% �\fB��	�\"!�7 �	�\"�Y�; �\fB��	�\"!�Q �	�\"�	�\"�U�U �\fB��	�\"!�b �\fB��	�\"!�o �\fB��	�\"!�| �\fB\f))�" [event type event-symbol-elements nil list zerop logand -134217728 meta 67108864 127 32 control 33554432 255 shift 16777216 hyper 8388608 super 4194304 alt] 5 (#$ . 5565)])
#@139 Returns the basic type of the given event (all modifiers removed).
The value is an ASCII printing character (not upper case) or a symbol.
(defalias 'event-basic-type #[(event) ":� @9� �N@�����\"S\"��W�* ��\"�, �)�" [event event-symbol-elements logand lsh 1 18 base 32 logior 64] 6 (#$ . 6142)])
#@53 Return non-nil if OBJECT is a mouse movement event.
(defalias 'mouse-movement-p #[(object) ":�	 @�=�" [object mouse-movement] 2 (#$ . 6457)])
(put (quote mouse-movement-p) (quote byte-optimizer) (quote byte-compile-inline-expand))
#@318 Return the starting position of EVENT.
If EVENT is a mouse press or a mouse click, this returns the location
of the event.
If EVENT is a drag, this returns the drag's starting position.
The return value is of the form
   (WINDOW BUFFER-POSITION (X . Y) TIMESTAMP)
The `posn-' functions access elements of such lists.
(defalias 'event-start #[(event) "A@�" [event] 1 (#$ . 6697)])
(put (quote event-start) (quote byte-optimizer) (quote byte-compile-inline-expand))
#@280 Return the ending location of EVENT.  EVENT should be a click or drag event.
If EVENT is a click event, this function is the same as `event-start'.
The return value is of the form
   (WINDOW BUFFER-POSITION (X . Y) TIMESTAMP)
The `posn-' functions access elements of such lists.
(defalias 'event-end #[(event) "�	8:� ��\f �	8�" [2 event 1] 2 (#$ . 7169)])
(put (quote event-end) (quote byte-optimizer) (quote byte-compile-inline-expand))
#@103 Return the multi-click count of EVENT, a click or drag event.
The return value is a positive integer.
(defalias 'event-click-count #[(event) "�	8�� �	8�" [2 event 1] 2 (#$ . 7615)])
(put (quote event-click-count) (quote byte-optimizer) (quote byte-compile-inline-expand))
#@176 Return the window in POSITION.
POSITION should be a list of the form
   (WINDOW BUFFER-POSITION (X . Y) TIMESTAMP)
as returned by the `event-start' and `event-end' functions.
(defalias 'posn-window #[(position) "@�" [position] 1 (#$ . 7897)])
(put (quote posn-window) (quote byte-optimizer) (quote byte-compile-inline-expand))
#@185 Return the buffer location in POSITION.
POSITION should be a list of the form
   (WINDOW BUFFER-POSITION (X . Y) TIMESTAMP)
as returned by the `event-start' and `event-end' functions.
(defalias 'posn-point #[(position) "A@:�\f A@@�A@�" [position] 1 (#$ . 8232)])
(put (quote posn-point) (quote byte-optimizer) (quote byte-compile-inline-expand))
#@189 Return the x and y coordinates in POSITION.
POSITION should be a list of the form
   (WINDOW BUFFER-POSITION (X . Y) TIMESTAMP)
as returned by the `event-start' and `event-end' functions.
(defalias 'posn-x-y #[(position) "�	8�" [2 position] 2 (#$ . 8588)])
(put (quote posn-x-y) (quote byte-optimizer) (quote byte-compile-inline-expand))
#@337 Return the column and row in POSITION, measured in characters.
POSITION should be a list of the form
   (WINDOW BUFFER-POSITION (X . Y) TIMESTAMP)
as returned by the `event-start' and `event-end' functions.
For a scroll-bar event, the result column is 0, and the row
corresponds to the vertical position of the click in the scroll bar.
(defalias 'posn-col-row #[(position) "�	8	�@)	A@:� 	A@@� 	A@�=�, ���\n!S\"B�t 	A@:�: 	A@@�= 	A@�=�M ��\n!\"�B�t �\n!�W \n�Z �\n!\f@�\f!�A�\f!�+B*�" [2 position window pair vertical-scroll-bar 0 scroll-bar-scale window-height horizontal-scroll-bar window-width framep window-frame frame frame-char-width x frame-char-height y] 6 (#$ . 8933)])
#@179 Return the timestamp of POSITION.
POSITION should be a list of the form
   (WINDOW BUFFER-POSITION (X . Y) TIMESTAMP)
as returned by the `event-start' and `event-end' functions.
(defalias 'posn-timestamp #[(position) "�	8�" [3 position] 2 (#$ . 9638)])
(byte-code "����#����\"����\"����\"����\"����\"����\"����\"����\"����\"����\"����\"����\"����\"�" [put posn-timestamp byte-optimizer byte-compile-inline-expand defalias dot point dot-marker point-marker dot-min point-min dot-max point-max window-dot window-point set-window-dot set-window-point read-input read-string send-string process-send-string send-region process-send-region show-buffer set-window-buffer buffer-flush-undo buffer-disable-undo eval-current-buffer eval-buffer compiled-function-p byte-code-function-p] 4)
#@139 Obsolete function returning the value of the `baud-rate' variable.
Please convert your programs to use the variable `baud-rate' directly.
(defalias 'baud-rate #[nil "�" [baud-rate] 1 (#$ . 10425)])
(byte-code "���\"����\"����\"����\"����\"����\"����\"����\"����\"����\"����K\"����K\"����\"����\"����\"�" [defalias string= string-equal string< string-lessp move-marker set-marker eql eq not null rplaca setcar rplacd setcdr beep ding indent-to-column indent-to backward-delete-char delete-backward-char search-forward-regexp re-search-forward search-backward-regexp re-search-backward int-to-string number-to-string set-match-data store-match-data string-to-int string-to-number] 3)
#@137 Variable by which C primitives find the function `run-hooks'.
Don't change it.  Don't use it either; use the hook running C primitives.
(defconst run-hooks (quote run-hooks) (#$ . 11115))
#@748 Make the hook HOOK local to the current buffer.
When a hook is local, its local and global values
work in concert: running the hook actually runs all the hook
functions listed in *either* the local value *or* the global value
of the hook variable.

This function works by making `t' a member of the buffer-local value,
which acts as a flag to run the hook functions in the default value as
well.  This works for all normal hooks, but does not work for most
non-normal hooks yet.  We will be changing the callers of non-normal
hooks so that they can handle localness; this has to be done one by
one.

This function does nothing if HOOK is already local in the current
buffer.

Do not use `make-local-variable' to make a hook variable buffer-local.
(defalias 'make-local-hook #[(hook) "�	!?� �	!� 	�L��	!�	�CL�" [local-variable-p hook boundp nil make-local-variable t] 2 (#$ . 11310)])
#@714 Add to the value of HOOK the function FUNCTION.
FUNCTION is not added if already present.
FUNCTION is added (if necessary) at the beginning of the hook list
unless the optional argument APPEND is non-nil, in which case
FUNCTION is added at the end.

The optional fourth argument, LOCAL, if non-nil, says to modify
the hook's buffer-local value rather than its default value.
This makes no difference if the hook is not buffer-local.
To make a hook variable buffer-local, always use
`make-local-hook', not `make-local-variable'.

HOOK should be a symbol, and FUNCTION may be any valid function.  If
HOOK is void, it is first set to nil.  If HOOK's value is a single
function, it is changed to a list of functions.
(defalias 'add-hook #[(hook function &optional append local) "�	!�\n 	�L��	!� �	�\"�	J�<�$ @�=�) 	CL�)�< �	!�i �	J>�i \n:�J \n	J��O \n	J>�� 	�b �	J\nC\"�g \n	JBL�\n:�x \n�	!��~ \n�	!>�� �	�� ��	!\nC\"�� \n�	!B\"�" [boundp hook nil default-boundp set-default old lambda local local-variable-if-set-p t function append default-value] 6 (#$ . 12203)])
#@542 Remove from the value of HOOK the function FUNCTION.
HOOK should be a symbol, and FUNCTION may be any valid function.  If
FUNCTION isn't the value of HOOK, or, if FUNCTION doesn't appear in the
list of hooks to run in HOOK, then nothing is done.  See `add-hook'.

The optional third argument, LOCAL, if non-nil, says to modify
the hook's buffer-local value rather than its default value.
This makes no difference if the hook is not buffer-local.
To make a hook variable buffer-local, always use
`make-local-hook', not `make-local-variable'.
(defalias 'remove-hook #[(hook function &optional local) "�	!?� ��!?� 	J?� ??�� \f�+ �	!�W �	J>�W 	J�:�G ��Q ��!\"�Q ��Q �	L)��	!�:�t ��~ ��!\"�~ ��~ ��	\")�" [boundp hook default-boundp function local local-variable-p t hook-value delete copy-sequence nil default-value set-default] 5 (#$ . 13292)])
#@403 Add to the value of LIST-VAR the element ELEMENT if it isn't there yet.
If you want to use `add-to-list' on a variable that is not defined
until a certain package is loaded, you should put the call to `add-to-list'
into a hook function that will be run only after loading the package.
`eval-after-load' provides one way to do this.  In some cases
other hooks, such as major mode hooks, can do the job.
(defalias 'add-to-list #[(list-var element) "	J�� 		JBL�" [element list-var] 3 (#$ . 14179)])
#@295 Arrange that, if FILE is ever loaded, FORM will be run at that time.
This makes or adds to an entry on `after-load-alist'.
If FILE is already loaded, evaluate FORM right now.
It does nothing if FORM is already on the list for FILE.
FILE should be the name of a library, with no directory name.
(defalias 'eval-after-load #[(file form) "�	\n\"�\f 	C\nB�	\n\"\fA��( \fC���	\"�( �\f!�)\f�" [assoc file after-load-alist elt form load-history eval] 3 (#$ . 14685)])
#@184 Read the following input sexp, and run it whenever FILE is loaded.
This makes or adds to an entry on `after-load-alist'.
FILE should be the name of a library, with no directory name.
(defalias 'eval-next-after-load #[(file) "�	� \"�" [eval-after-load file read] 3 (#$ . 15158)])
#@270 Like `read-char', except that if the first character read is an octal
digit, we read up to two more octal digits and return the character
represented by the octal number consisting of those digits.
Optional argument PROMPT specifies a string to use to prompt the user.
(defalias 'read-quoted-char #[(&optional prompt) "����\f�W�w \f�U��	\n�$ ��\"�� \n�/ �+\n� �\nX�_ \n�X�_ �_\n�Z\\\fT� ���\n#�!�� \f�V�o \nC։�	 \n։�	 ���\"�U�� ڂ� ���\"\",�" [nil 0 char code count message-log-max 3 "Type the special character you want to use,\nor three octal digits representing its character code." help-form help-char inhibit-quit prompt message "%s-" read-char quit-flag 48 55 8 format "%s %c" unread-command-events 259 logior logand -134217728 128 255] 6 (#$ . 15444)])
#@124 Force the mode-line of the current buffer to be redisplayed.
With optional non-nil ALL, force redisplay of all mode-lines.
(defalias 'force-mode-line-update #[(&optional all) "�\n �� q�)�� !�" [all other-buffer set-buffer-modified-p buffer-modified-p] 2 (#$ . 16248)])
#@375 Momentarily display STRING in the buffer at POS.
Display remains until next character is typed.
If the char is EXIT-CHAR (optional third arg, default is SPC) it is swallowed;
otherwise it is then available as input (as a command if nothing else).
Display MESSAGE (optional fourth arg) in the echo area.
If MESSAGE is nil, instructions to type EXIT-CHAR are displayed there.
(defalias 'momentary-string-display #[(string pos &optional exit-char message) "� ���� �	\nˎ�\fb���!�`� V�6 �� ҥ!���!�`\fV�I \fb���!�)��Q ��!\"�� �=�f C�.�" [exit-char 32 nil t buffer-modified-p buffer-file-name insert-end name modified buffer-undo-list buffer-read-only ((byte-code "�\n �	|�)\n�!�" [insert-end pos name buffer-file-name set-buffer-modified-p modified] 2)) pos insert-before-markers string window-end recenter window-height 2 move-to-window-line 0 message "Type %s to continue editing." single-key-description read-event char unread-command-events] 6 (#$ . 16525)])
(byte-code "��!� ��B" [boundp font-lock-defaults nil current-load-list] 2)
#@218 Non-nil if the visited file is a binary file.
This variable is meaningful on MS-DOG and Windows NT.
On those systems, it is automatically local in every buffer.
On other systems, this variable is normally always nil.
(defvar buffer-file-type nil (#$ . 17609))
#@93 Do nothing and return nil.
This function accepts any number of arguments, but ignores them.
(defalias 'ignore #[(&rest ignore) "��" [nil] 1 (#$ . 17875) nil])
#@72 Signal an error, making error message by passing all args to `format'.
(defalias 'error #[(&rest args) "����\f\"C\"��  " [signal error apply format args] 5 (#$ . 18040)])
(defalias (quote user-original-login-name) (quote user-login-name))
#@641 Start a program in a subprocess.  Return the process object for it.
Args are NAME BUFFER COMMAND &rest COMMAND-ARGS.
NAME is name for process.  It is modified if necessary to make it unique.
BUFFER is the buffer or (buffer-name) to associate with the process.
 Process output goes at end of that buffer, unless you specify
 an output stream or filter function to handle the output.
 BUFFER may be also nil, meaning that this process is not associated
 with any buffer
Third arg is command name, the name of a shell command.
Remaining arguments are the arguments for the command.
Wildcards and redirection are handled as usual in the shell.
(defalias 'start-process-shell-command #[(name buffer &rest args) "�=� ��\f$��\f���#%�" [system-type vax-vms apply start-process name buffer args shell-file-name shell-command-switch mapconcat identity " "] 9 (#$ . 18286)])
#@71 Execute the BODY forms, restoring the global value of the match data.
(defalias 'save-match-data '(macro . #[(&rest body) "��!�\n�DC��B�\nDEE)�" [make-symbol "match-data" original let (match-data) unwind-protect progn body store-match-data] 6 (#$ . 19168)]))
#@336 Return string of text matched by last search.
NUM specifies which parenthesized expression in the last regexp.
 Value is nil if NUMth pair didn't match, or there were less than NUM pairs.
Zero means the entire text matched by the whole regexp or whole string.
STRING should be given if the last search was by `string-match' on STRING.
(defalias 'match-string #[(num &optional string) "�� 	� 	��O���{�" [num string] 3 (#$ . 19437)])
#@72 Return the text from BEG to END, without text properties, as a string.
(defalias 'buffer-substring-no-properties #[(beg end) "	{��\nG�\n$�\n)�" [beg end string set-text-properties 0 nil] 5 (#$ . 19882)])
#@65 Quote an argument for passing as argument to an inferior shell.
(defalias 'shell-quote-argument #[(argument) "�=� \n��=� �\n�Q����	\n��\n	#�C Ɣ\n\n	O�\n�TOR\nT	� \n\n	�OP+�" [system-type ms-dos argument windows-nt "\"" "" 0 nil end start result string-match "[^-0-9a-zA-Z_./]" "\\"] 6 (#$ . 20094)])
#@167 Return a new syntax table.
It inherits all letters and control characters from the standard
syntax table; other characters are copied from the standard syntax table.
(defalias 'make-syntax-table #[(&optional oldtable) "� �!�� ����X�! \f�I�T�� ���X�5 \f�I�T��$ ���X�I \f�I�T��8 ���X�] \f�I�T��L \f*�" [oldtable copy-syntax-table nil i table 0 31 65 90 97 122 128 255] 4 (#$ . 20426)])
#@398 Give KEY a global binding as COMMAND.
COMMAND is a symbol naming an interactively-callable function.
KEY is a key sequence (a string or vector of characters or event types).
Non-ASCII characters with codes above 127 (such as ISO Latin-1)
can be included if you use a vector.
Note that if KEY has a local binding in the current buffer
that local binding will continue to shadow any global binding.
(defalias 'global-set-key #[(key command) "�	!� 	;� ���	D\"��� 	#�ȇ" [vectorp key signal wrong-type-argument arrayp define-key current-global-map command nil] 4 (#$ . 20839) "KSet key globally: \nCSet key %s to command: "])
#@404 Give KEY a local binding as COMMAND.
COMMAND is a symbol naming an interactively-callable function.
KEY is a key sequence (a string or vector of characters or event types).
Non-ASCII characters with codes above 127 (such as ISO Latin-1)
can be included if you use a vector.
The binding goes in the current buffer's local map,
which in most cases is shared with all other buffers in the same major mode.
(defalias 'local-set-key #[(key command) "� �� �� �!��!�  ;�  ���D\"��	\n#�)ˇ" [current-local-map map use-local-map make-sparse-keymap vectorp key signal wrong-type-argument arrayp define-key command nil] 5 (#$ . 21472) "KSet key locally: \nCSet key %s locally to command: "])
#@86 Remove global binding of KEY.
KEY is a string representing a sequence of keystrokes.
(defalias 'global-unset-key #[(key) "�	�\"�" [global-set-key key nil] 3 (#$ . 22167) "kUnset key globally: "])
#@85 Remove local binding of KEY.
KEY is a string representing a sequence of keystrokes.
(defalias 'local-unset-key #[(key) "� �\n �\n�\"�Ç" [current-local-map local-set-key key nil] 3 (#$ . 22369) "kUnset key locally: "])
#@145 Return non-nil if OBJECT seems to be a frame configuration.
Any list whose car is `frame-configuration' is assumed to be a frame
configuration.
(defalias 'frame-configuration-p #[(object) ":�	 @�=�" [object frame-configuration] 2 (#$ . 22595)])

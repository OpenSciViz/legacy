;ELC   
;;; compiled by rms@mole.gnu.ai.mit.edu on Fri Nov 24 22:58:24 1995
;;; from file /home/fsf/rms/e19/lisp/sort.el
;;; emacs version 19.29.91.3.
;;; bytecomp version FSF 2.10
;;; optimization is on.
;;; this file uses opcodes which do not exist in Emacs 18.

(if (and (boundp 'emacs-version)
	 (or (and (boundp 'epoch::version) epoch::version)
	     (string-lessp emacs-version "19.29")))
    (error "`sort.el' was compiled for Emacs 19.29 or later"))


#@59 *Non-nil if the buffer sort functions should ignore case.
(defvar sort-fold-case nil (#$ . -466))
#@1580 General text sorting routine to divide buffer into records and sort them.
Arguments are REVERSE NEXTRECFUN ENDRECFUN &optional STARTKEYFUN ENDKEYFUN.

We divide the accessible portion of the buffer into disjoint pieces
called sort records.  A portion of each sort record (perhaps all of
it) is designated as the sort key.  The records are rearranged in the
buffer in order by their sort keys.  The records may or may not be
contiguous.

Usually the records are rearranged in order of ascending sort key.
If REVERSE is non-nil, they are rearranged in order of descending sort key.

The next four arguments are functions to be called to move point
across a sort record.  They will be called many times from within sort-subr.

NEXTRECFUN is called with point at the end of the previous record.
It moves point to the start of the next record.
It should move point to the end of the buffer if there are no more records.
The first record is assumed to start at the position of point when sort-subr
is called.

ENDRECFUN is called with point within the record.
It should move point to the end of the record.

STARTKEYFUN moves from the start of the record to the start of the key.
It may return either a non-nil value to be used as the key, or
else the key is the substring between the values of point after
STARTKEYFUN and ENDKEYFUN are called.  If STARTKEYFUN is nil, the key
starts at the beginning of the record.

ENDKEYFUN moves from the start of the sort key to the end of the sort key.
ENDKEYFUN may be nil if STARTKEYFUN returns a value or if it would be the
same as ENDRECFUN.
(defalias 'sort-subr #[(reverse nextrecfun endrecfun &optional startkeyfun endkeyfun) "deZ�V�	� ��!��$	�	!\f	�� \n�3 	�		�; ��!���!�` �	�@@��O т\\ 	@@:�[ ҂\\ �\"�| �	�@@��n Ղ{ 	@@:�z ւ{ �\"	\n�� 	�		�� ��!��	\"�+	�� ��!�*ۇ" [50000 messages message "Finding sort keys..." sort-build-lists nextrecfun endrecfun startkeyfun endkeyfun sort-lists reverse old sort-fold-case case-fold-search "Sorting records..." fboundp sortcar < #[(a b) "���@A�\f@\fA&V�" [0 compare-buffer-substrings nil a b] 8] string< sort car-less-than-car #[(a b) "���@@@A�\f@@\f@A&V�" [0 compare-buffer-substrings nil a b] 8] #[(a b) "@	@��" [a b] 2] "Reordering buffer..." sort-reorder-buffer "Reordering buffer... Done" nil] 5 (#$ . 572)])
(defalias 'sort-build-lists #[(nextrecfun endrecfun startkeyfun endkeyfun) "����m�f `��ō\n��1 �&  ��1 �1  ��	�V 	:�N 	@��N 	A`��N 	�B�S 	`BB\fB\n� �  �� \f,�" [nil key done start-rec sort-lists (byte-code "�	  � `\n� � �	`)B�" [startkeyfun start endkeyfun endrecfun t done] 2) endrecfun nextrecfun t] 4])
(defalias 'sort-reorder-buffer #[(sort-lists old) "�eeddb���!�\ndS}��J db��p	@A@#�db��p@A@@AA#�	@AAA	A	� db��p	#�\n	|�db�\n`T}�``T|,�" [t max min last inhibit-quit insert-before-markers " " sort-lists insert-buffer-substring old] 4])
#@190 Sort lines in region alphabetically; argument means descending order.
Called from a program, there are three arguments:
REVERSE (non-nil means reverse order), BEG and END (region to sort).
(defalias 'sort-lines #[(reverse beg end) "��	}�eb����#*�" [beg end sort-subr reverse forward-line end-of-line] 4 (#$ . 3507) "P\nr"])
#@195 Sort paragraphs in region alphabetically; argument means descending order.
Called from a program, there are three arguments:
REVERSE (non-nil means reverse order), BEG and END (region to sort).
(defalias 'sort-paragraphs #[(reverse beg end) "��	}�eb����#*�" [beg end sort-subr reverse #[nil "m?� �	!� �y��  �" [looking-at paragraph-separate 1] 2] forward-paragraph] 4 (#$ . 3840) "P\nr"])
#@190 Sort pages in region alphabetically; argument means descending order.
Called from a program, there are three arguments:
REVERSE (non-nil means reverse order), BEG and END (region to sort).
(defalias 'sort-pages #[(reverse beg end) "��	}�eb����#*�" [beg end sort-subr reverse #[nil "��w�" ["\n" nil] 2] forward-page] 4 (#$ . 4240) "P\nr"])
(byte-code "��!� ��B	�O � ��W�/ ��#�T�� ���#����#����#����#�*" [boundp sort-fields-syntax-table nil current-load-list make-syntax-table 0 i table 256 modify-syntax-entry "w" 32 " " 9 10 46 "_"] 5)
#@369 Sort lines in region numerically by the ARGth field of each line.
Fields are separated by whitespace and numbered from 1 up.
Specified field must contain a number in each line of the region.
With a negative arg, sorts by the ARGth field counted from the right.
Called from a program, there are three arguments:
FIELD, BEG and END.  BEG and END specify region to sort.
(defalias 'sort-numeric-fields #[(field beg end) "�	\n��%�" [sort-fields-1 field beg end #[nil "�	!��`���!�`){!�" [sort-skip-fields field string-to-number forward-sexp 1] 4] nil] 6 (#$ . 4817) "p\nr"])
#@309 Sort lines in region lexicographically by the ARGth field of each line.
Fields are separated by whitespace and numbered from 1 up.
With a negative arg, sorts by the ARGth field counted from the right.
Called from a program, there are three arguments:
FIELD, BEG and END.  BEG and END specify region to sort.
(defalias 'sort-fields #[(field beg end) "�	\n��%�" [sort-fields-1 field beg end #[nil "�	!�" [sort-skip-fields field nil] 2] #[nil "��w�" ["^ 	\n" nil] 2]] 6 (#$ . 5395) "p\nr"])
(defalias 'sort-fields-1 #[(field beg end startkeyfun endkeyfun) "� \n�U� �Ŏ��}�eb��	!�����%,�" [syntax-table tbl field 0 1 ((set-syntax-table tbl)) beg end set-syntax-table sort-fields-syntax-table sort-subr nil forward-line end-of-line startkeyfun endkeyfun] 6])
(defalias 'sort-skip-fields #[(n) "�V�8 S��V� ��w���w�\nS��\n ��w�l�6 �Ǌ�y�`)���`){\")���[S��V�T ��x���x�\nS��@ ��x�)n�n �Ǌ�y�`)���`){\"���x�" [n 0 i " 	" nil "^ 	\n" error "Line has too few fields: %s"] 5])
(byte-code "�	B�	B��" [sort-regexp-fields-regexp current-load-list sort-regexp-record-end] 2)
(defalias 'sort-regexp-fields-next-record #[nil "`�\n��#�1 ŕ��1 U�* �u��\n��#�ŕ��+ ȅ1 Ŕb)�" [oldpos re-search-forward sort-regexp-fields-regexp nil move 0 sort-regexp-record-end 1 t] 4])
#@817 Sort the region lexicographically as specified by RECORD-REGEXP and KEY.
RECORD-REGEXP specifies the textual units which should be sorted.
  For example, to sort lines RECORD-REGEXP would be "^.*$"
KEY specifies the part of each record (ie each match for RECORD-REGEXP)
  is to be used for sorting.
  If it is "\\digit" then the digit'th "\\(...\\)" match field from
  RECORD-REGEXP is used.
  If it is "\\&" then the whole record is used.
  Otherwise, it is a regular-expression for which to search within the record.
If a match for KEY is not found within a record then that record is ignored.

With a negative prefix arg sorts in reverse order.

For example: to sort lines in the region by the first word on each line
 starting with the letter "f",
 RECORD-REGEXP would be "^.*$" and KEY would be "\\=\<f\\w*\\>"
(defalias 'sort-regexp-fields #[(reverse record-regexp key-regexp beg end) "���\f � �� ��\"� �H�Z��	}�eb��\f�\f!�`Ôb�����$,�" [key-regexp "" "\\&" 0 string-match "\\`\\\\[1-9]\\'" 1 48 beg end nil record-regexp sort-regexp-fields-regexp sort-regexp-record-end re-search-forward sort-subr reverse sort-regexp-fields-next-record #[nil "b�" [sort-regexp-record-end] 1] #[nil "�\n��\f \n� �\n\f�#� �� ���\"���ʏ)�" [0 n key-regexp re-search-forward sort-regexp-record-end t throw key nil (byte-code "��!�\f \n�\n�B�\n�\n�{�" [fboundp buffer-substring-lessp n] 2) ((error (byte-code "���\"�" [throw key nil] 3)))] 4]] 5 (#$ . 6687) "P\nsRegexp specifying records to sort: \nsRegexp specifying key within record: \nr"])
(byte-code "��!� ��B��" [boundp sort-columns-subprocess t current-load-list] 2)
#@620 Sort lines in region alphabetically by a certain range of columns.
For the purpose of this command, the region includes
the entire line that point is in and the entire line the mark is in.
The column positions of point and mark bound the range of columns to sort on.
A prefix argument means sort into reverse order.

Note that `sort-columns' rejects text that contains tabs,
because tabs could be split across the specified columns
and it doesn't know how to handle that.  Also, when possible,
it uses the `sort' utility program, which doesn't understand tabs.
Use \[untabify] to convert tabs to spaces before sorting.
(defalias 'sort-columns #[(reverse &optional beg end) "�������^b�i�y�`]b�i�y�`\f^\f]���#�@ ��!��=�d ��̉��X ԂY ��\nP�	P&	�x ��}�b������%*.�" [nil col-end col-start col-end1 col-beg1 end1 beg1 beg end 0 search-backward "	" t error "sort-columns does not work with tabs.  Use M-x untabify." system-type vax-vms call-process-region "sort" reverse "-rt\n" "-t\n" "+0." "-0." sort-subr forward-line end-of-line #[nil "�	!�" [move-to-column col-start nil] 2] #[nil "�	!�" [move-to-column col-end nil] 2]] 11 (#$ . 8340) "P\nr"])
#@106 Reverse the order of lines in a region.
From a program takes two point or marker arguments, BEG and END.
(defalias 'reverse-region #[(beg end) "	V� �	)�b�n� �y�`	b�l�' n�- �y���� ��	�` b�`��`{	B	`	U?�Z `T�[ `|��6 	A�v 	@ʱ�	A�	�b 	@c+�" [beg end nil mid 1 -1 point-marker t do ll "\n"] 3 (#$ . 9535) "r"])
(provide (quote sort))

;ELC   
;;; compiled by rms@mole.gnu.ai.mit.edu on Fri Nov 24 22:57:29 1995
;;; from file /home/fsf/rms/e19/lisp/inf-lisp.el
;;; emacs version 19.29.91.3.
;;; bytecomp version FSF 2.10
;;; optimization is on.
;;; this file uses opcodes which do not exist in Emacs 18.

(if (and (boundp 'emacs-version)
	 (or (and (boundp 'epoch::version) epoch::version)
	     (string-lessp emacs-version "19.29")))
    (error "`inf-lisp.el' was compiled for Emacs 19.29 or later"))


(byte-code "��!���!�" [require comint lisp-mode] 2)
#@229 *What not to save on inferior Lisp's input history.
Input matching this regexp is not saved on the input history in Inferior Lisp
mode.  Default is whitespace followed by 0 or 1 single-letter colon-keyword 
(as in :a, :c, etc.)
(defvar inferior-lisp-filter-regexp "\\`\\s *\\(:\\(\\w\\|\\s_\\)\\)?\\s *\\'" (#$ . -527))
(byte-code "��!� ��B	�C �!���	��#��	��#��	��#��	��#��	��#��	��#��	��#����#����#����#����#����#����#����#����#����#����#����#����#�" [boundp inferior-lisp-mode-map nil current-load-list copy-keymap comint-mode-map shared-lisp-mode-map define-key "" lisp-eval-last-sexp "\f" lisp-load-file "" lisp-compile-file "" lisp-show-arglist "" lisp-describe-sym "" lisp-show-function-documentation "" lisp-show-variable-documentation lisp-mode-map "�" lisp-eval-defun "" "" lisp-eval-region "" lisp-compile-defun "" switch-to-lisp] 5)
(defalias 'inferior-lisp-install-letter-bindings #[nil "�	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#����#����#����#����#����#����#�" [define-key lisp-mode-map "e" lisp-eval-defun-and-go "r" lisp-eval-region-and-go "c" lisp-compile-defun-and-go "z" switch-to-lisp "l" lisp-load-file "k" lisp-compile-file "a" lisp-show-arglist "d" lisp-describe-sym "f" lisp-show-function-documentation "v" lisp-show-variable-documentation inferior-lisp-mode-map] 4])
#@74 *Program name for invoking an inferior Lisp with for Inferior Lisp mode.
(defvar inferior-lisp-program "lisp" (#$ . -1918))
#@425 *Format-string for building a Lisp expression to load a file.
This format string should use `%s' to substitute a file name
and should result in a Lisp expression that will command the inferior Lisp
to load that file.  The default works acceptably on most Lisps.
The string "(progn (load \"%s\" :verbose nil :print t) (values))\n"
produces cosmetically superior output for this application,
but it works only in Common Lisp.
(defvar inferior-lisp-load-command "(load \"%s\")\n" (#$ . -2049))
#@391 Regexp to recognise prompts in the Inferior Lisp mode.
Defaults to "^[^> \n]*>+:? *", which works pretty good for Lucid, kcl,
and franz.  This variable is used to initialize `comint-prompt-regexp' in the 
Inferior Lisp buffer.

More precise choices:
Lucid Common Lisp: "^\\(>\\|\\(->\\)+\\) *"
franz: "^\\(->\\|<[0-9]*>:\\) *"
kcl: "^>+ *"

This is a fine thing to set in your .emacs file.
(defvar inferior-lisp-prompt "^[^> \n]*>+:? *" (#$ . 2547))
#@1668 *The current inferior-lisp process buffer.

MULTIPLE PROCESS SUPPORT
===========================================================================
To run multiple Lisp processes, you start the first up
with \[inferior-lisp].  It will be in a buffer named `*inferior-lisp*'.
Rename this buffer with \[rename-buffer].  You may now start up a new
process with another \[inferior-lisp].  It will be in a new buffer,
named `*inferior-lisp*'.  You can switch between the different process
buffers with \[switch-to-buffer].

Commands that send text from source buffers to Lisp processes --
like `lisp-eval-defun' or `lisp-show-arglist' -- have to choose a process
to send to, when you have more than one Lisp process around.  This
is determined by the global variable `inferior-lisp-buffer'.  Suppose you
have three inferior Lisps running:
    Buffer              Process
    foo                 inferior-lisp
    bar                 inferior-lisp<2>
    *inferior-lisp*     inferior-lisp<3>
If you do a \[lisp-eval-defun] command on some Lisp source code, 
what process do you send it to?

- If you're in a process buffer (foo, bar, or *inferior-lisp*), 
  you send it to that process.
- If you're in some other buffer (e.g., a source file), you
  send it to the process attached to buffer `inferior-lisp-buffer'.
This process selection is performed by function `inferior-lisp-proc'.

Whenever \[inferior-lisp] fires up a new process, it resets
`inferior-lisp-buffer' to be the new process's buffer.  If you only run
one process, this does the right thing.  If you run multiple
processes, you can change `inferior-lisp-buffer' to another process
buffer with \[set-variable].
(defvar inferior-lisp-buffer nil (#$ . -3005))
#@43 *Hook for customising Inferior Lisp mode.
(defvar inferior-lisp-mode-hook nil (#$ . -4725))
#@1829 Major mode for interacting with an inferior Lisp process.  
Runs a Lisp interpreter as a subprocess of Emacs, with Lisp I/O through an
Emacs buffer.  Variable `inferior-lisp-program' controls which Lisp interpreter
is run.  Variables `inferior-lisp-prompt', `inferior-lisp-filter-regexp' and
`inferior-lisp-load-command' can customize this mode for different Lisp
interpreters.

For information on running multiple processes in multiple buffers, see
documentation for variable `inferior-lisp-buffer'.

\{inferior-lisp-mode-map}

Customisation: Entry to this mode runs the hooks on `comint-mode-hook' and
`inferior-lisp-mode-hook' (in that order).

You can send text to the inferior Lisp process from other buffers containing
Lisp source.  
    switch-to-lisp switches the current buffer to the Lisp process buffer.
    lisp-eval-defun sends the current defun to the Lisp process.
    lisp-compile-defun compiles the current defun.
    lisp-eval-region sends the current region to the Lisp process.
    lisp-compile-region compiles the current region.

    Prefixing the lisp-eval/compile-defun/region commands with
    a \[universal-argument] causes a switch to the Lisp process buffer after sending
    the text.

Commands:
Return after the end of the process' output sends the text from the 
    end of process to point.
Return before the end of the process' output copies the sexp ending at point
    to the end of the process' output, and sends it.
Delete converts tabs to spaces as it moves back.
Tab indents for Lisp; with argument, shifts rest
    of expression rigidly with the current line.
C-M-q does Tab on each line starting within following expression.
Paragraphs are separated only by blank lines.  Semicolons start comments.
If you accidentally suspend your process, use \[comint-continue-subjob]
to continue it.
(defalias 'inferior-lisp-mode #[nil "� �	�����!��\f!������!�" [comint-mode inferior-lisp-prompt comint-prompt-regexp inferior-lisp-mode major-mode "Inferior Lisp" mode-name (":%s") mode-line-process lisp-mode-variables t use-local-map inferior-lisp-mode-map lisp-get-old-input comint-get-old-input lisp-input-filter comint-input-filter ignore comint-input-sentinel run-hooks inferior-lisp-mode-hook] 2 (#$ . 4825) nil])
#@54 Return a string containing the sexp ending at point.
(defalias 'lisp-get-old-input #[nil "�`� �`{*�" [end backward-sexp] 2 (#$ . 7092)])
#@56 t if STR does not match `inferior-lisp-filter-regexp'.
(defalias 'lisp-input-filter #[(str) "�	\n\"?�" [string-match inferior-lisp-filter-regexp str] 3 (#$ . 7237)])
#@420 Run an inferior Lisp process, input and output via buffer `*inferior-lisp*'.
If there is a process already running in `*inferior-lisp*', just switch
to that buffer.
With argument, allows you to edit the command line (default is value
of `inferior-lisp-program').  Runs the hooks from
`inferior-lisp-mode-hook' (after the `comint-mode-hook' is run).
(Type \[describe-mode] in the process buffer for a list of commands.)
(defalias 'inferior-lisp #[(cmd) "��!� �!���\f@�\fA%q�� �)�\n��!�" [comint-check-proc "*inferior-lisp*" inferior-lisp-args-to-list cmd cmdlist apply make-comint "inferior-lisp" nil inferior-lisp-mode inferior-lisp-buffer pop-to-buffer] 6 (#$ . 7410) (list (if current-prefix-arg (read-string "Run lisp: " inferior-lisp-program) inferior-lisp-program))])
(defalias (quote run-lisp) (quote inferior-lisp))
(defalias 'inferior-lisp-args-to-list #[(string) "��\n\"�� \nC�8 �U�% \n�O�\n�\\\nGO!B�8 ��\n\"��7 �\n\nGO!))�" [string-match "[ 	]" string where 0 inferior-lisp-args-to-list 1 "[^ 	]" pos] 6])
#@115 Send the current region to the inferior Lisp process.
Prefix argument means switch to the Lisp buffer afterwards.
(defalias 'lisp-eval-region #[(start end &optional and-go) "�� \n#��� �\"�� ��!�" [comint-send-region inferior-lisp-proc start end comint-send-string "\n" and-go switch-to-lisp t] 4 (#$ . 8447) "r\nP"])
#@114 Send the current defun to the inferior Lisp process.
Prefix argument means switch to the Lisp buffer afterwards.
(defalias 'lisp-eval-defun #[(&optional and-go) "�� ���x�`� ��`\"�*� ��!�" [end-of-defun " 	\n\f" nil end beginning-of-defun lisp-eval-region and-go switch-to-lisp t] 3 (#$ . 8775) "P"])
#@114 Send the previous sexp to the inferior Lisp process.
Prefix argument means switch to the Lisp buffer afterwards.
(defalias 'lisp-eval-last-sexp #[(&optional and-go) "��� �`)`\n#�" [lisp-eval-region backward-sexp and-go] 4 (#$ . 9088) "P"])
#@118 Compile the current region in the inferior Lisp process.
Prefix argument means switch to the Lisp buffer afterwards.
(defalias 'lisp-compile-region #[(start end &optional and-go) "�� ��\f{\"\"�� ��!�" [comint-send-string inferior-lisp-proc format "(funcall (compile nil `(lambda () (progn 'compile %s))))\n" start end and-go switch-to-lisp t] 6 (#$ . 9335) "r\nP"])
#@117 Compile the current defun in the inferior Lisp process.
Prefix argument means switch to the Lisp buffer afterwards.
(defalias 'lisp-compile-defun #[(&optional and-go) "�� ���x�`� ��`\"�*� ��!�" [end-of-defun " 	\n\f" nil e beginning-of-defun lisp-compile-region and-go switch-to-lisp t] 3 (#$ . 9712) "P"])
#@95 Switch to the inferior Lisp process buffer.
With argument, positions cursor at end of buffer.
(defalias 'switch-to-lisp #[(eob-p) "�	!� �	!�� ��!�� � �db�" [get-buffer inferior-lisp-buffer pop-to-buffer error "No current inferior Lisp buffer" eob-p push-mark] 2 (#$ . 10031) "P"])
#@73 Send the current region to the inferior Lisp, and switch to its buffer.
(defalias 'lisp-eval-region-and-go #[(start end) "�	\n�#�" [lisp-eval-region start end t] 4 (#$ . 10322) "r"])
#@72 Send the current defun to the inferior Lisp, and switch to its buffer.
(defalias 'lisp-eval-defun-and-go #[nil "��!�" [lisp-eval-defun t] 2 (#$ . 10511) nil])
#@76 Compile the current region in the inferior Lisp, and switch to its buffer.
(defalias 'lisp-compile-region-and-go #[(start end) "�	\n�#�" [lisp-compile-region start end t] 4 (#$ . 10676) "r"])
#@75 Compile the current defun in the inferior Lisp, and switch to its buffer.
(defalias 'lisp-compile-defun-and-go #[nil "��!�" [lisp-compile-defun t] 2 (#$ . 10874) nil])
#@187 Record last directory and file used in loading or compiling.
This holds a cons cell of the form `(DIRECTORY . FILE)'
describing the last `lisp-load-file' or `lisp-compile-file' command.
(defvar lisp-prev-l/c-dir/file nil (#$ . 11049))
#@251 *Used to determine if a buffer contains Lisp source code.
If it's loaded into a buffer that is in one of these major modes, it's
considered a Lisp source file by `lisp-load-file' and `lisp-compile-file'.
Used by these commands to determine defaults.
(defvar lisp-source-modes (quote (lisp-mode)) (#$ . -11291))
#@50 Load a Lisp file into the inferior Lisp process.
(defalias 'lisp-load-file #[(file-name) "�	!��	!�	!B�� �	\"\"���!�" [comint-check-source file-name file-name-directory file-name-nondirectory lisp-prev-l/c-dir/file comint-send-string inferior-lisp-proc format inferior-lisp-load-command switch-to-lisp t] 5 (#$ . 11608) (comint-get-source "Load Lisp file: " lisp-prev-l/c-dir/file lisp-source-modes nil)])
#@51 Compile a Lisp file in the inferior Lisp process.
(defalias 'lisp-compile-file #[(file-name) "�	!��	!�	!B�� �	�Q\"���!�" [comint-check-source file-name file-name-directory file-name-nondirectory lisp-prev-l/c-dir/file comint-send-string inferior-lisp-proc "(compile-file \"" "\")\n" switch-to-lisp t] 5 (#$ . 12022) (comint-get-source "Compile Lisp file: " lisp-prev-l/c-dir/file lisp-source-modes nil)])
#@64 Command to query inferior Lisp for a function's documentation.
(defvar lisp-function-doc-command "(let ((fn '%s))\n     (format t \"Documentation for ~a:~&~a\"\n	     fn (documentation fn 'function))\n     (values))\n" (#$ . 12434))
#@64 Command to query inferior Lisp for a variable's documentation.
(defvar lisp-var-doc-command "(let ((v '%s))\n     (format t \"Documentation for ~a:~&~a\"\n	     v (documentation v 'variable))\n     (values))\n" (#$ . 12673))
#@58 Command to query inferior Lisp for a function's arglist.
(defvar lisp-arglist-command "(let ((fn '%s))\n     (format t \"Arglist for ~a: ~a\" fn (arglist fn))\n     (values))\n" (#$ . 12904))
#@64 Command to query inferior Lisp for a variable's documentation.
(defvar lisp-describe-sym-command "(describe '%s)\n" (#$ . 13102))
(defalias 'lisp-symprompt #[(prompt default) "�\f ��#� �P�!�G�U�  �\" *C�" [default format "%s (default %s): " prompt ": " read-string ans 0] 5])
#@101 Returns the name of the function called in the current call.
The value is nil if it can't find one.
(defalias 'lisp-fn-called-at-pt #[nil "���" [nil (byte-code "��e`�Z]d}���!��u��p!�9� \f+�" [1000 backward-up-list 1 read obj] 4) ((error))] 3 (#$ . 13397)])
(defalias 'lisp-var-at-pt #[nil "���" [nil (byte-code "���!���w��p!�9� *�" [forward-sexp -1 "'" nil read obj] 3) ((error))] 3])
#@118 Send a command to the inferior Lisp to give documentation for function FN.
See variable `lisp-function-doc-command'.
(defalias 'lisp-show-function-documentation #[(fn) "�� �\f\"\"�" [comint-proc-query inferior-lisp-proc format lisp-function-doc-command fn] 5 (#$ . 13798) (byte-code "��� \"�" [lisp-symprompt "Function doc" lisp-fn-called-at-pt] 3)])
#@113 Send a command to the inferior Lisp to give documentation for function FN.
See variable `lisp-var-doc-command'.
(defalias 'lisp-show-variable-documentation #[(var) "�� �\f\"\"�" [comint-proc-query inferior-lisp-proc format lisp-var-doc-command var] 5 (#$ . 14157) (byte-code "��� \"�" [lisp-symprompt "Variable doc" lisp-var-at-pt] 3)])
#@105 Send a query to the inferior Lisp for the arglist for function FN.
See variable `lisp-arglist-command'.
(defalias 'lisp-show-arglist #[(fn) "�� �\f\"\"�" [comint-proc-query inferior-lisp-proc format lisp-arglist-command fn] 5 (#$ . 14502) (byte-code "��� \"�" [lisp-symprompt "Arglist" lisp-fn-called-at-pt] 3)])
#@103 Send a command to the inferior Lisp to describe symbol SYM.
See variable `lisp-describe-sym-command'.
(defalias 'lisp-describe-sym #[(sym) "�� �\f\"\"�" [comint-proc-query inferior-lisp-proc format lisp-describe-sym-command sym] 5 (#$ . 14823) (byte-code "��� \"�" [lisp-symprompt "Describe" lisp-var-at-pt] 3)])
(defalias 'inferior-lisp-proc #[nil "�	�=� p�\f !�� ��!)�" [get-buffer-process major-mode inferior-lisp-mode inferior-lisp-buffer proc error "No Lisp subprocess; see variable `inferior-lisp-buffer'"] 4])
#@98 This hook is run when the library `inf-lisp' is loaded.
This is a good place to put keybindings.
(defvar inferior-lisp-load-hook nil (#$ . 15351))
(byte-code "��!���!�" [run-hooks inferior-lisp-load-hook provide inf-lisp] 2)

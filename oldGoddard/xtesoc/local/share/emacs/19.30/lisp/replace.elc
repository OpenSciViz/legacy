;ELC   
;;; compiled by roland@whiz-bang.gnu.ai.mit.edu on Sat Sep  9 03:22:11 1995
;;; from file /gd/gnu/emacs/19.0/lisp/replace.el
;;; emacs version 19.29.1.
;;; bytecomp version FSF 2.10
;;; optimization is on.
;;; this file uses opcodes which do not exist in Emacs 18.

(if (and (boundp 'emacs-version)
	 (or (and (boundp 'epoch::version) epoch::version)
	     (string-lessp emacs-version "19.29")))
    (error "`replace.el' was compiled for Emacs 19.29 or later"))


#@68 *Non-nil means query-replace should preserve case in replacements.
(defconst case-replace t (#$ . -478))
(byte-code "��!� ��B" [boundp query-replace-history nil current-load-list] 2)
#@98 Non-nil means `query-replace' uses the last search string.
That becomes the "string to replace".
(defvar query-replace-interactive nil (#$ . 673))
(defalias 'query-replace-read-args #[(string regexp-flag) "��� \f� � @�# ���\n\"����%���\n\n#����%\n	E*�" [nil to from query-replace-interactive regexp-flag regexp-search-ring search-ring read-from-minibuffer format "%s: " string query-replace-history "%s %s with: " current-prefix-arg] 6])
#@803 Replace some occurrences of FROM-STRING with TO-STRING.
As each match is found, the user must type a character saying
what to do with it.  For directions, type \[help-command] at that time.

If `query-replace-interactive' is non-nil, the last incremental search
string is used as FROM-STRING--you don't have to specify it with the
minibuffer.

Preserves case in each replacement if `case-replace' and `case-fold-search'
are non-nil and FROM-STRING has no uppercase letters.
(Preserving case means that if the string matched is all caps, or capitalized,
then its replacement is upcased or capitalized.)

Third arg DELIMITED (prefix arg if interactive), if non-nil, means replace
only matches surrounded by word boundaries.

To customize possible responses, change the "bindings" in `query-replace-map'.
(defalias 'query-replace #[(from-string to-string &optional arg) "�	\n��%�" [perform-replace from-string to-string t nil arg] 6 (#$ . 1134) (query-replace-read-args "Query replace" nil)])
(define-key esc-map "%" (quote query-replace))
#@755 Replace some things after point matching REGEXP with TO-STRING.
As each match is found, the user must type a character saying
what to do with it.  For directions, type \[help-command] at that time.

If `query-replace-interactive' is non-nil, the last incremental search
regexp is used as REGEXP--you don't have to specify it with the
minibuffer.

Preserves case in each replacement if `case-replace' and `case-fold-search'
are non-nil and REGEXP has no uppercase letters.
Third arg DELIMITED (prefix arg if interactive), if non-nil, means replace
only matches surrounded by word boundaries.
In TO-STRING, `\&' stands for whatever matched the whole of REGEXP,
and `\=\N' (where N is a digit) stands for
 whatever what matched the Nth `\(...\)' in REGEXP.
(defalias 'query-replace-regexp #[(regexp to-string &optional arg) "�	\nÉ\f%�" [perform-replace regexp to-string t arg] 6 (#$ . 2179) (query-replace-read-args "Query replace regexp" t)])
#@654 Replace some matches for REGEXP with various strings, in rotation.
The second argument TO-STRINGS contains the replacement strings, separated
by spaces.  This command works like `query-replace-regexp' except
that each successive replacement uses the next successive replacement string,
wrapping around from the last such string to the first.

Non-interactively, TO-STRINGS may be a list of replacement strings.

If `query-replace-interactive' is non-nil, the last incremental search
regexp is used as REGEXP--you don't have to specify it with the minibuffer.

A prefix argument N says to use each replacement string N times
before rotating to the next.
(defalias 'map-query-replace-regexp #[(regexp to-strings &optional arg) "�\n<�\f \n�? \nG�U�? ��\n\"�3 �	\n���\n\"OC\"\n��\n\"T�O�� �	\nC\"ǉ� �		ʉ�&)�" [nil replacements to-strings 0 string-match " " append "" perform-replace regexp t arg] 8 (#$ . 3128) (byte-code "��� \f@� ������%���\n\"����%\n	\nE*�" [nil to from query-replace-interactive regexp-search-ring read-from-minibuffer "Map query replace (regexp): " query-replace-history format "Query replace %s with (space-separated strings): " current-prefix-arg] 6)])
#@836 Replace occurrences of FROM-STRING with TO-STRING.
Preserve case in each match if `case-replace' and `case-fold-search'
are non-nil and FROM-STRING has no uppercase letters.
(Preserving case means that if the string matched is all caps, or capitalized,
then its replacement is upcased or capitalized.)

Third arg DELIMITED (prefix arg if interactive), if non-nil, means replace
only matches surrounded by word boundaries.

If `query-replace-interactive' is non-nil, the last incremental search
string is used as FROM-STRING--you don't have to specify it with the
minibuffer.

This function is usually the wrong thing to use in a Lisp program.
What you probably want is a loop like this:
  (while (search-forward FROM-STRING nil t)
    (replace-match TO-STRING nil t))
which will run faster and will not set the mark or print anything.
(defalias 'replace-string #[(from-string to-string &optional delimited) "�	\nÉ\f%�" [perform-replace from-string to-string nil delimited] 6 (#$ . 4330) (query-replace-read-args "Replace string" nil)])
#@868 Replace things after point matching REGEXP with TO-STRING.
Preserve case in each match if `case-replace' and `case-fold-search'
are non-nil and REGEXP has no uppercase letters.
Third arg DELIMITED (prefix arg if interactive), if non-nil, means replace
only matches surrounded by word boundaries.
In TO-STRING, `\&' stands for whatever matched the whole of REGEXP,
and `\=\N' (where N is a digit) stands for
 whatever what matched the Nth `\(...\)' in REGEXP.

If `query-replace-interactive' is non-nil, the last incremental search
regexp is used as REGEXP--you don't have to specify it with the minibuffer.

This function is usually the wrong thing to use in a Lisp program.
What you probably want is a loop like this:
  (while (re-search-forward REGEXP nil t)
    (replace-match TO-STRING nil nil))
which will run faster and will not set the mark or print anything.
(defalias 'replace-regexp #[(regexp to-string &optional delimited) "�	\n��%�" [perform-replace regexp to-string nil t delimited] 6 (#$ . 5374) (query-replace-read-args "Replace regexp" t)])
#@63 History list for some commands that read regular expressions.
(defvar regexp-history nil (#$ . 6438))
(defalias (quote delete-non-matching-lines) (quote keep-lines))
#@159 Delete all lines except those containing matches for REGEXP.
A match split across lines preserves all the lines it lies in.
Applies to all lines after point.
(defalias 'keep-lines #[(regexp) "�n� �y�`m?�P ���#� 	d|��7 �Ɣb��y�`)	W�6 	|�)��y�`)m�\n ƔƕU�\n �u��\n *�" [1 start re-search-forward regexp nil move 0 end] 4 (#$ . 6611) (list (read-from-minibuffer "Keep lines (containing match for regexp): " nil nil nil (quote regexp-history)))])
(defalias (quote delete-matching-lines) (quote flush-lines))
#@147 Delete lines containing matches for REGEXP.
If a match is split across lines, all the lines it lies in are deleted.
Applies to lines after point.
(defalias 'flush-lines #[(regexp) "�m?�! �	��#�! �Ĕb��y�`)�y�`|�� )�" [re-search-forward regexp nil t 0 1] 4 (#$ . 7134) (list (read-from-minibuffer "Flush lines (containing match for regexp): " nil nil nil (quote regexp-history)))])
(defalias (quote count-matches) (quote how-many))
#@53 Print number of matches for REGEXP following point.
(defalias 'how-many #[(regexp) "���m�% `���#�% \n`U� �u�� T� ��\"+�" [0 nil opoint count re-search-forward regexp t 1 message "%d occurrences"] 4 (#$ . 7572) (list (read-from-minibuffer "How many matches for (regexp): " nil nil nil (quote regexp-history)))])
(byte-code "��!� ��B	�% � �	��#��	��#��	��#���!�. ��B��!�; �\f�B��!�H ��B" [boundp occur-mode-map nil current-load-list make-sparse-keymap define-key [mouse-2] occur-mode-mouse-goto "" occur-mode-goto-occurrence "" occur-buffer occur-nlines occur-pos-list] 4)
#@279 Major mode for output from \[occur].
\<occur-mode-map>Move point to one of the items in this buffer, then use
\[occur-mode-goto-occurrence] to go to the occurrence that the item refers to.
Alternatively, click \[occur-mode-mouse-goto] on an item to go to it.

\{occur-mode-map}
(defalias 'occur-mode #[nil "� ��\n!�����!���!���!���!�" [kill-all-local-variables use-local-map occur-mode-map occur-mode major-mode "Occur" mode-name make-local-variable occur-buffer occur-nlines occur-pos-list run-hooks occur-mode-hook] 2 (#$ . 8181)])
#@62 In Occur mode, go to the occurrence whose line you click on.
(defalias 'occur-mode-mouse-goto #[(event) "����\f�\f8:� ł �\f8)�@)!q��\f�\f8:�- ł. �\f8)�A@:�B A@@�F A@)b�� 	*�\n!��	!b*�" [nil pos buffer window-buffer event 2 1 position occur-mode-find-occurrence occur-buffer pop-to-buffer marker-position] 4 (#$ . 8724) "e"])
(defalias 'occur-mode-find-occurrence #[nil "�\n �!� ����!��e��y�`)\"��y�S	�W�2 �	Z�C 	�V�B 	�\\�\\�C ˥)�\f8�W�W ��!��` ��!�+�" [occur-buffer buffer-name nil occur-pos-list error "Buffer in which occurrences were found is deleted" count-lines 0 line-count occur-nlines 2 1 occur-number pos "No occurrence on this line"] 4])
#@50 Go to the occurrence the current line describes.
(defalias 'occur-mode-goto-occurrence #[nil "� �!��	!b)�" [occur-mode-find-occurrence pos pop-to-buffer occur-buffer marker-position] 2 (#$ . 9422) nil])
#@227 *Default number of context lines to include around a `list-matching-lines'
match.  A negative number means to include that many lines before the match.
A positive number means to include that many lines both before and after.
(defvar list-matching-lines-default-context-lines 0 (#$ . -9634))
(defalias (quote list-matching-lines) (quote occur))
#@507 Show all lines in the current buffer containing a match for REGEXP.

If a match spreads across multiple lines, all those lines are shown.

Each line is displayed with NLINES lines before and after, or -NLINES
before if NLINES is negative.
NLINES defaults to `list-matching-lines-default-context-lines'.
Interactively it is the prefix arg.

The lines are shown in a buffer named `*Occur*'.
It serves as a menu to find any of the occurrences in this buffer.
\[describe-mode] in that buffer will explain how.
(defalias 'occur #[(regexp &optional nlines) "�\n �!� \n�p\f�e� 	\n\f͐�q�\n�c���!�)��!�ֱ�� ��)=�X db��� �dU�����#��ޔb��y��  �	�`\"\\	*`ޕb��ޔb��W�� �� [y�`)#�ޕb��V�� Ty��� �y�`)$��	\"'�'G�\"*�+�� �+`ړ�q�+B\f�� �!�� �c��\f�#$#�`$ޕZZړ��$#Z!��+�V�+*���y�+S�+�	1`W�g'�B��1\"''����y�`)���`)��$��y��'1T1�/`X�*���y�1T1�g)+W��*���y�+T�+��db�)�y�-�\\ q��eb�G�U���c�����G\"c�t����G\")�.�" [nlines prefix-numeric-value list-matching-lines-default-context-lines t default-directory 1 make-marker final-context-start prevpos linenum dir buffer first "*Occur*" standard-output " matching " print-escape-newlines prin1 regexp " in buffer " buffer-name 46 10 occur-mode occur-buffer occur-nlines nil occur-pos-list beginning-of-buffer re-search-forward 0 match-data match-data ((store-match-data match-data)) count-lines start end format "%5d" tag make-string 32 empty tem zerop "--------\n" insert-buffer-substring backward-char 58 this-linenum put-text-property mouse-face highlight "1 line" "%d lines" message "%d matching lines."] 7 (#$ . 9986) (list (let* ((default (car regexp-history)) (input (read-from-minibuffer (if default (format "List lines matching regexp (default `%s'): " default) "List lines matching regexp: ") nil nil nil (quote regexp-history)))) (if (> (length input) 0) input (setcar regexp-history default))) current-prefix-arg)])
#@37 Help message while in query-replace
(defconst query-replace-help "Type Space or `y' to replace one match, Delete or `n' to skip to next,\nRET or `q' to exit, Period to replace one match and exit,\nComma to replace but not move point immediately,\nC-r to enter recursive edit (\\[exit-recursive-edit] to get out again),\nC-w to delete match and recursive edit,\nC-l to clear the screen, redisplay, and offer same replacement again,\n! to replace all remaining matches with no more questions,\n^ to move point back to previous match." (#$ . 11990))
#@301 Keymap that defines the responses to questions in `query-replace'.
The "bindings" in this map are not commands; they are answers.
The valid answers include `act', `skip', `act-and-show',
`exit', `act-and-exit', `edit', `delete-and-edit', `recenter',
`automatic', `backup', `exit-prefix', and `help'.
(defvar query-replace-map (make-sparse-keymap) (#$ . 12544))
(byte-code "�	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#��	��#�" [define-key query-replace-map " " act "" skip [delete] [backspace] "y" "n" "Y" "N" "," act-and-show "q" exit "" [return] "." act-and-exit "" edit "" delete-and-edit "\f" recenter "!" automatic "^" backup "" help [f1] [help] "?" "" quit "" "" exit-prefix [escape]] 4)
#@373 Subroutine of `query-replace'.  Its complexity handles interactive queries.
Don't use this in your own program unless you want to query and set the mark
just as `query-replace' does.  Instead, write a simple loop like this:
  (while (re-search-forward "foo[ 	]+bar" nil t)
    (replace-match "foobar" nil nil))
which will run faster and probably do exactly what you want.
(defalias 'perform-replace #[(from-string replacements query-flag regexp-flag delimited-flag &optional repeat-count map) "� 	\n� � \f���??� Ƃ �\fȉ���ɉ���0 ��!;�Y �a �a ��y ���r \f�u �\f!�Q� �� ����m����#��`=�� �� ɔ=�� �� m���u���#�� ʃ�� �� �!<�� U�� \\8TG���!��#�T��� �ȉ��)*+,,���!��ɔɕ\"��.�\f#�)� *�*!*�*\"�)�=�y����^��_�\f���:!�!��;q�� )���)�=���ʉ,� )�=����@�?@b�?A�=�+���?A!�A)�΁@ !��A �B !��C �!��)�D =��+���#��,�+�)�E =�+��#���,�+�)�F =�0+��#��+�)�G =�S+�G�#��,��+�)�H =�cʉ,� )�I =�u�I �!��)�J =���� ��K  �)!���!�)�L =��ɔɕ|��� ��K  �)!��+��M N��O �P *!Q\"Qʉ,�#`+���R �S � \"BB+��T,`�� )Q�΁T ��U����U #��.�" [map query-replace-map case-fold-search case-replace from-string regexp-flag re-search-forward search-forward nil 0 t query-flag substitute-command-keys "Query replacing %s with %s: (\\<query-replace-map>\\[help] for help) " message match-again lastrepl replace-count next-rotate-count stack keep-going replacement-index next-replacement real-match-data search-string search-function literal nocasify replacements repeat-count 1 delimited-flag "\\b" regexp-quote push-mark undo-boundary ((replace-dehighlight)) match-data looking-at store-match-data replace-match def key replaced done replace-highlight message-log-max read-event vector lookup-key help "*Help*" princ "Query replacing " "regexp " "" " with " ".\n\n" query-replace-help standard-output help-mode exit backup elt "No previous match" ding no-terminate sit-for act act-and-exit act-and-show automatic skip recenter edit recursive-edit delete-and-edit mode-exited this-command append listify-key-sequence unread-command-events mapcar #[(elt) "�\f �!���" [elt marker-position nil] 4] "Replaced %d occurrence%s" "s"] 16 (#$ . 13357)])
#@61 *Non-nil means to highlight words during query replacement.
(defvar query-replace-highlight nil (#$ . -15772))
(byte-code "��!� ��B" [boundp replace-overlay nil current-load-list] 2)
(defalias 'replace-dehighlight #[nil "� �!��" [replace-overlay delete-overlay nil] 2])
(defalias 'replace-highlight #[(start end) "�# 	� �\f\"�	���!� Ȃ �#��	\fp$�" [query-replace-highlight replace-overlay make-overlay start end overlay-put face internal-find-face query-replace region move-overlay] 5])

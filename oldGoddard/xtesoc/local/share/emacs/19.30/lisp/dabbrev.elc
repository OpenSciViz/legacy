;ELC   
;;; compiled by rms@mole.gnu.ai.mit.edu on Sat Nov 11 00:44:54 1995
;;; from file /home/fsf/rms/e19/lisp/dabbrev.el
;;; emacs version 19.29.1.1.
;;; bytecomp version FSF 2.10
;;; optimization is on.
;;; this file uses opcodes which do not exist in Emacs 18.

(if (and (boundp 'emacs-version)
	 (or (and (boundp 'epoch::version) epoch::version)
	     (string-lessp emacs-version "19.28.90")))
    (error "`dabbrev.el' was compiled for Emacs 19.29 or later"))


#@53 *If non-nil, `dabbrev-expand' only looks backwards.
(defvar dabbrev-backward-only nil (#$ . -474))
#@70 *Limits region searched by `dabbrev-expand' to this many chars away.
(defvar dabbrev-limit nil (#$ . -579))
#@263 *Regexp for skipping leading characters of an abbreviation.

Example: Set this to "\\$" for programming languages
in which variable names may appear with or without a leading `$'.
(For example, in Makefiles.)

Set this to nil if no characters should be skipped.
(defvar dabbrev-abbrev-skip-leading-regexp nil (#$ . -694))
#@313 *Non-nil if dabbrev searches should ignore case.
A value of nil means case is significant.

The value of this variable is an expression; it is evaluated
and the resulting value determines the decision.
For example: setting this to `case-fold-search' means evaluate that
variable to see whether its value is nil.
(defvar dabbrev-case-fold-search (quote case-fold-search) (#$ . -1023))
#@225 *The significance of an uppercase character in an abbreviation.
nil means case fold search, non-nil means case sensitive search.

This variable has an effect only when the value of
`dabbrev-case-fold-search' evaluates to t.
(defvar dabbrev-upcase-means-case-search nil (#$ . -1414))
#@536 *Non-nil means dabbrev should preserve case when expanding the abbreviation.
More precisely, it preserves the case pattern of the abbreviation as you
typed it--as opposed to the case pattern of the expansion that is copied.
The value of this variable is an expression; it is evaluated
and the resulting value determines the decision.
For example, setting this to `case-replace' means evaluate that
variable to see if its value is t or nil.

This variable has an effect only when the value of
`dabbrev-case-fold-search' evaluates to t.
(defvar dabbrev-case-replace (quote case-replace) (#$ . -1704))
#@1012 *Regexp to recognize a character in an abbreviation or expansion.
This regexp will be surrounded with \\( ... \\) when actually used.

Set this variable to "\\sw" if you want ordinary words or
"\\sw\\|\\s_" if you want symbols (including characters whose
syntax is "symbol" as well as those whose syntax is "word".

The value nil has a special meaning: the abbreviation is from point to
previous word-start, but the search is for symbols.

For instance, if you are programming in Lisp, `yes-or-no-p' is a symbol,
while `yes', `or', `no' and `p' are considered words.  If this
variable is nil, then expanding `yes-or-no-' looks for a symbol
starting with or containing `no-'.  If you set this variable to
"\\sw\\|\\s_", that expansion looks for a symbol starting with
`yes-or-no-'.  Finally, if you set this variable to "\\sw", then
expanding `yes-or-no-' signals an error because `-' is not part of a word;
but expanding `yes-or-no' looks for a word starting with `no'.

The recommended value is "\\sw\\|\\s_".
(defvar dabbrev-abbrev-char-regexp nil (#$ . -2311))
#@324 *Non-nil means dabbrev package should search *all* buffers.

Dabbrev always searches the current buffer first.  Then, if
`dabbrev-check-other-buffers' says so, it searches the buffers
designated by `dabbrev-select-buffers-function'.

Then, if `dabbrev-check-all-buffers' is non-nil, dabbrev searches
all the other buffers.
(defvar dabbrev-check-all-buffers t (#$ . -3382))
#@338 *Should \[dabbrev-expand] look in other buffers?
nil: Don't look in other buffers.
t: Also look for expansions in the buffers pointed out by
   `dabbrev-select-buffers-function'.
Anything else: When we can't find any more expansions in
the current buffer, then ask the user whether to look in other
buffers too.

The default value is t.
(defvar dabbrev-check-other-buffers t (#$ . -3762))
#@279 A function that selects buffers that should be searched by dabbrev.
The function should take no arguments and return a list of buffers to
search for expansions.  Have a look at `dabbrev--select-buffers' for
an example.

A mode setting this variable should make it buffer local.
(defvar dabbrev-select-buffers-function (quote dabbrev--select-buffers) (#$ . 4158))
#@493 *A function to decide whether dabbrev should search OTHER-BUFFER.
The function should take one argument, OTHER-BUFFER, and return
non-nil if that buffer should be searched.  Have a look at
`dabbrev--same-major-mode-p' for an example.

The value of `dabbrev-friend-buffer-function' has an effect only if
the value of `dabbrev-select-buffers-function' uses it.  The function
`dabbrev--select-buffers' is one function you can use here.

A mode setting this variable should make it buffer local.
(defvar dabbrev-friend-buffer-function (quote dabbrev--same-major-mode-p) (#$ . -4528))
#@210 If non-nil, a list of buffers which dabbrev should search.
If this variable is non-nil, dabbrev will only look in these buffers.
It will not even look in the current buffer if it is not a member of
this list.
(defvar dabbrev-search-these-buffers-only nil (#$ . 5115))
(byte-code "��!� ��B��!� ��B��!�  ��B��!�- ��B��!�: ��B��!�G �	�B��!�T �\n�B��!�a ��B��!�n �\f�B��!�{ ��B��!�� ��B��!�� �B��!�� ��B" [boundp dabbrev--last-obarray nil current-load-list dabbrev--last-table dabbrev--last-abbreviation dabbrev--last-abbrev-location dabbrev--last-direction 0 dabbrev--last-expansion dabbrev--last-expansion-location dabbrev--friend-buffer-list dabbrev--last-buffer dabbrev--last-buffer-found dabbrev--last-completion-buffer dabbrev--check-other-buffers dabbrev-check-other-buffers dabbrev--abbrev-char-regexp] 2)
(defalias 'dabbrev--minibuffer-origin #[nil "� A@�" [buffer-list] 1])
(put (quote dabbrev--minibuffer-origin) (quote byte-optimizer) (quote byte-compile-inline-expand))
(defalias 'dabbrev-filter-elements '(macro . #[(element list condition) "���E��E����BB�	����BBEE�BBBB�BBBB�" [let dabbrev-result dabbrev-tail element setq list while ((car dabbrev-tail)) if condition cons (dabbrev-result) ((setq dabbrev-tail (cdr dabbrev-tail))) ((nreverse dabbrev-result))] 13]))
(byte-code "�	��#��	��#�" [define-key esc-map "/" dabbrev-expand [67108911] dabbrev-completion] 4)
#@472 Completion on current word.
Like \[dabbrev-expand] but finds all expansions in the current buffer
and presents suggestions for completion.

With a prefix argument, it searches all buffers accepted by the
function pointed out by `dabbrev-friend-buffer-function' to find the
completions.

If the prefix argument is 16 (which comes from C-u C-u),
then it searches *all* buffers.

With no prefix argument, it reuses an old completion list
if there is a suitable one already.
(defalias 'dabbrev-completion #[(&optional arg) "� �	� �	� �	!�U� �\n!�* ?�* ���\f��	�} �} p=�U �� !�} � A@=�} �} GGY�} �GO��} �\"��� �\f\"�G�\"�G�V�� ���� ނ� �#�\f��  �� ��\"��� ����� ��\"��� ��O��O���� ��\"��� ��\"�p�\"));�ޘ�<����<�\"G�V�-���� !\"��1��!���#��P��!����\"!����!��� !�Z��!.�" [dabbrev--reset-global-variables arg t dabbrev-check-other-buffers prefix-numeric-value 16 dabbrev-check-all-buffers dabbrev--abbrev-at-point abbrev eval dabbrev-case-fold-search dabbrev-upcase-means-case-search ignore-case-p dabbrev--last-obarray my-obarray nil init dabbrev--last-completion-buffer window-minibuffer-p selected-window buffer-list dabbrev--last-abbreviation 0 try-completion dabbrev--find-all-expansions completion-list make-vector error "No dynamic expansion for \"%s\" found%s" dabbrev--check-other-buffers "" " in this-buffer" dabbrev-case-replace mapcar #[(string) "�	\n\"�" [intern string my-obarray] 3] #[(string) "�	�\n\"�" [intern string my-obarray] 3] 1 #[(string) "��\n!\"�" [intern capitalize string my-obarray] 3] #[(string) "�	�\n\"�" [intern string my-obarray] 3] all-completions message "Repeat `%s' to see all completions" key-description this-command-keys "The only possible completion" dabbrev--substitute-expansion "Making completion list..." " *Completions*" display-completion-list "Making completion list...done"] 6 (#$ . 6555) "*P"])
#@817 Expand previous word "dynamically".

Expands to the most recent, preceding word for which this is a prefix.
If no suitable preceding word is found, words following point are
considered.  If still no suitable word is found, then look in the
buffers accepted by the function pointed out by variable
`dabbrev-friend-buffer-function'.

A positive prefix argument, N, says to take the Nth backward *distinct*
possibility.  A negative argument says search forward.

If the cursor has not moved from the end of the previous expansion and
no argument is given, replace the previously-made expansion
with the next possible expansion not yet tried.

The variable `dabbrev-backward-only' may be used to limit the
direction of search to backward if set non-nil.

See also `dabbrev-abbrev-char-regexp' and \[dabbrev-completion].
(defalias 'dabbrev-expand #[(arg) "����`��@ �!�@ �!�@ \n=�4 �� !�@ `U�@ �� h�=�� �!�� �!�� `TU�� ���h q��=�} �=�� `W�� G\\b����Q!�`{`)��� � ��� �� Ԃ� Ղ� �!� �\f�� �\n�\"!�� #?�� ���#)\f�� ��� �	$B$�!�c�``G\\|�)�����#�\\*=�\"���!\"�*�&��!�p=�2�E��E`V�E�!	$B$�\f#�\f� �-�" [nil orig-point direction old expansion abbrev arg markerp dabbrev--last-abbrev-location marker-position last-command this-command window-minibuffer-p selected-window dabbrev--last-abbreviation dabbrev--last-expansion dabbrev--last-direction 32 " " dabbrev--last-buffer 1 0 dabbrev--last-expansion-location re-search-forward "\\(\\(" dabbrev--abbrev-char-regexp "\\)+\\)" -1 dabbrev--reset-global-variables dabbrev-backward-only prefix-numeric-value dabbrev--abbrev-at-point dabbrev--find-expansion eval dabbrev-case-fold-search dabbrev-upcase-means-case-search buffer-undo-list search-backward error "No%s dynamic expansion for `%s' found" " further" "" dabbrev--last-buffer-found message "Expansion found in '%s'" buffer-name copy-marker dabbrev--substitute-expansion point-marker] 5 (#$ . 8523) "*P"])
(defalias 'dabbrev--same-major-mode-p #[(other-buffer) "�	q�)=�" [major-mode other-buffer] 2])
(defalias 'dabbrev--goto-start-of-abbrev #[nil "� o�% �u��!� o� �u��\f �!�% �u��7 �!�7 �u��* *�" [match-data match-data ((store-match-data match-data)) -1 looking-at dabbrev--abbrev-char-regexp 1 dabbrev-abbrev-skip-leading-regexp] 2])
(defalias 'dabbrev--abbrev-at-point #[nil "o� ��!��`� Ŏ��u���	� ��Q!)�: �	�* ���#�6 �u��: ��!�*� �\n`{)�" [error "No possible abbreviation preceding point" dabbrev--last-abbrev-location match-data match-data ((store-match-data match-data)) -1 looking-at "\\(" dabbrev-abbrev-char-regexp "\\sw\\|\\s_" "\\)+" re-search-backward nil t 1 dabbrev--goto-start-of-abbrev] 4])
(defalias 'dabbrev--reset-global-variables #[nil "���������	\n� �\f��" [nil dabbrev--last-table dabbrev--last-abbreviation dabbrev--last-abbrev-location dabbrev--last-direction dabbrev--last-expansion dabbrev--last-expansion-location dabbrev--friend-buffer-list dabbrev--last-buffer dabbrev--last-buffer-found dabbrev-abbrev-char-regexp "\\sw\\|\\s_" dabbrev--abbrev-char-regexp dabbrev-check-other-buffers dabbrev--check-other-buffers] 2])
(defalias 'dabbrev--select-buffers #[nil "��� !� � A@q�pĉ�� ��F @=�= ��!�= 	!�= BA��  �-�" [window-minibuffer-p selected-window buffer-list orig-buffer nil buffer dabbrev-tail dabbrev-result boundp dabbrev-friend-buffer-function] 4])
(defalias 'dabbrev--try-find #[(abbrev reverse n ignore-case) "��~��\n� \nb�\f�V�+ �	\n#��+ S�� *	�2 `	+�" [nil expansion dabbrev--last-expansion-location ignore-case n count case-fold-search 0 dabbrev--search abbrev reverse] 5])
(defalias 'dabbrev--find-all-expansions #[(abbrev ignore-case) "���eb��\f�#�� 	\nB� )\n*�" [nil expansion all-expansions dabbrev--find-expansion abbrev -1 ignore-case] 4])
(defalias 'dabbrev--scanning-message #[nil "���p!\"�" [message "Scanning `%s'" buffer-name] 4])
(defalias 'dabbrev--find-expansion #[(abbrev direction ignore-case) "��\n� \nq�� ��* \f�* �� !�* � A@q�eb��	� �\f�S 	�Y�S �	^�\f�S ���	]$��S 	�K\f�[ \n�~ 	�X�~ ȉ\f�~ ���	[]$��~ 	�K�\f�� \n�� ��\f\"��� �� �=�� ��!��� �� �� ���� ��� @�>�� BA��� �+��\"*�\"�� !� � =�!� !�� !\"B� !� �)	�J�J@A\nq�� �e#���$��&	*�" [nil expansion dabbrev--last-buffer dabbrev--scanning-message dabbrev-search-these-buffers-only window-minibuffer-p selected-window buffer-list -1 direction 0 1 dabbrev--last-direction dabbrev--try-find abbrev t ignore-case dabbrev--friend-buffer-list mapcar get-buffer dabbrev--check-other-buffers y-or-n-p "Scan other buffers also? " friend-buffer-list non-friend-buffer-list dabbrev-select-buffers-function dabbrev-check-all-buffers buffer dabbrev-tail dabbrev-result append next-window w window-buffer delq dabbrev--last-expansion-location] 6])
(defalias 'dabbrev--safe-replace-match #[(string &optional fixedcase literal) "�=�\f �\f#��\f#�" [major-mode picture-mode picture-replace-match string fixedcase literal replace-match] 4])
(defalias 'dabbrev--substitute-expansion #[(old abbrev expansion) "�	!� \n� ���� �\f!�$ ��!�)�( �!��	?�#)�" [eval dabbrev-case-fold-search dabbrev-upcase-means-case-search abbrev dabbrev-case-replace use-case-replace old search-backward dabbrev--safe-replace-match expansion t] 4])
(defalias 'dabbrev--search #[(abbrev reverse ignore-case) "� �\f!��R�\f!��R�\f��5 `�0 [�2 \\}��� �H ���#�N ���#�� Ԕb�� ��!�� �\f!�הו{�u �ʉ���� @��� BA��� �+�� ��� Ԕb��5 ԕb��5 �� Ԕԕ{B�� �!�� ��� ).�" [match-data match-data ((store-match-data match-data)) regexp-quote abbrev "\\(" dabbrev--abbrev-char-regexp "\\)" "\\(\\(" "\\)+\\)" nil found-string pattern2 pattern1 dabbrev-limit dabbrev--last-expansion-location reverse re-search-backward t re-search-forward 0 dabbrev--goto-start-of-abbrev looking-at 1 ignore-case table-string dabbrev-tail dabbrev-result dabbrev--last-table result eval dabbrev-case-replace] 6])
(provide (quote dabbrev))

;ELC   
;;; compiled by rms@mole.gnu.ai.mit.edu on Fri Nov 17 14:15:28 1995
;;; from file /home/fsf/rms/e19/lisp/startup.el
;;; emacs version 19.29.91.3.
;;; bytecomp version FSF 2.10
;;; optimization is on.
;;; this file uses opcodes which do not exist in Emacs 18.

(if (and (boundp 'emacs-version)
	 (or (and (boundp 'epoch::version) epoch::version)
	     (string-lessp emacs-version "19.29")))
    (error "`startup.el' was compiled for Emacs 19.29 or later"))


(byte-code "�" [(normal-top-level) top-level nil] 1)
#@40 t once command line has been processed
(defvar command-line-processed nil (#$ . 528))
#@156 *Non-nil inhibits the initial startup message.
This is for use in your personal init file, once you are familiar
with the contents of the startup message.
(defconst inhibit-startup-message nil (#$ . -621))
#@461 *Non-nil inhibits the initial startup echo area message.
Inhibition takes effect only if your `.emacs' file contains
a line of this form:
 (setq inhibit-startup-echo-area-message "YOUR-USER-NAME")
If your `.emacs' file is byte-compiled, use the following form instead:
 (eval '(setq inhibit-startup-echo-area-message "YOUR-USER-NAME"))
Thus, someone else using a copy of your `.emacs' file will see
the startup message unless he personally acts to inhibit it.
(defconst inhibit-startup-echo-area-message nil (#$ . -834))
#@50 *Non-nil inhibits loading the `default' library.
(defconst inhibit-default-init nil (#$ . -1361))
#@213 Alist of command-line switches.
Elements look like (SWITCH-STRING . HANDLER-FUNCTION).
HANDLER-FUNCTION receives switch name as sole arg;
remaining command-line args are in the variable `command-line-args-left'.
(defconst command-switch-alist nil (#$ . 1466))
#@46 List of command-line args not yet processed.
(defvar command-line-args-left nil (#$ . 1732))
#@398 List of functions to process unrecognized command-line arguments.
Each function should access the dynamically bound variables
`argi' (the current argument) and `command-line-args-left' (the remaining
arguments).  The function should return non-nil only if it recognizes and
processes `argi'.  If it does so, it may consume successive arguments by
altering `command-line-args-left' to remove them.
(defvar command-line-functions nil (#$ . 1832))
#@122 Default directory to use for command line arguments.
This is normally copied from `default-directory' when Emacs starts.
(defvar command-line-default-directory nil (#$ . 2284))
#@184 Functions to call after handling urgent options but before init files.
The frame system uses this to open frames to display messages while
Emacs loads the user's initialization file.
(defvar before-init-hook nil (#$ . 2468))
#@203 Functions to call after loading the init file (`~/.emacs').
The call is not protected by a condition-case, so you can set `debug-on-error'
in `.emacs', and put all the actual code on `after-init-hook'.
(defvar after-init-hook nil (#$ . 2700))
#@233 Functions to be called after loading terminal-specific Lisp code.
See `run-hooks'.  This variable exists for users to set,
so as to override the definitions made by the terminal-specific file.
Emacs never sets this variable itself.
(defvar term-setup-hook nil (#$ . 2950))
#@189 The brand of keyboard you are using.
This variable is used to define
the proper function and keypad keys for use under X.  It is used in a
fashion analogous to the environment value TERM.
(defvar keyboard-type nil (#$ . 3230))
#@153 Normal hook run to initialize window system display.
Emacs runs this hook after processing the command line arguments and loading
the user's init file.
(defvar window-setup-hook nil (#$ . 3464))
#@68 Major mode command symbol to use for the initial *scratch* buffer.
(defconst initial-major-mode (quote lisp-interaction-mode) (#$ . 3665))
#@550 Identity of user whose `.emacs' file is or was read.
The value is nil if no init file is being used; otherwise, it may be either
the null string, meaning that the init file was taken from the user that
originally logged in, or it may be a string containing a user's name.

In either of the latter cases, `(concat "~" init-file-user "/")'
evaluates to the name of the directory where the `.emacs' file was
looked for.

Setting `init-file-user' does not prevent Emacs from loading
`site-start.el'.  The only way to do that is to use `--no-site-file'.
(defvar init-file-user nil (#$ . 3811))
#@745 File containing site-wide run-time initializations.
This file is loaded at run-time before `~/.emacs'.  It contains inits
that need to be in place for the entire site, but which, due to their
higher incidence of change, don't make sense to load into emacs'
dumped image.  Thus, the run-time load order is: 1. file described in
this variable, if non-nil; 2. `~/.emacs'; 3. `default.el'.

Don't use the `site-start.el' file for things some users may not like.
Put them in `default.el' instead, so that users can more easily
override them.  Users can prevent loading `default.el' with the `-q'
option or by setting `inhibit-default-init' in their own init files,
but inhibiting `site-start.el' requires `--no-site-file', which
is less convenient.
(defvar site-run-file "site-start" (#$ . 4407))
#@184 Regexp that specifies when to enable the ISO 8859-1 character set.
We do that if this regexp matches the locale name
specified by the LC_ALL, LC_CTYPE and LANG environment variables.
(defconst iso-8859-1-locale-regexp "8859[-_]?1" (#$ . 5206))
#@54 *Name of this machine, for purposes of naming users.
(defvar mail-host-address nil (#$ . -5456))
#@154 *Full mailing address of this user.
This is initialized based on `mail-host-address',
after your init file is read, in case it sets `mail-host-address'.
(defvar user-mail-address nil (#$ . -5560))
#@143 Prefix for generating auto-save-list-file-name.
Emacs's pid and the system name will be appended to
this prefix to create a unique file name.
(defvar auto-save-list-file-prefix "~/.saves-" (#$ . 5764))
(byte-code "��!� ��B��!� ��B" [boundp init-file-debug nil current-load-list init-file-had-error] 2)
(defalias 'normal-top-level-add-to-load-path #[(dirs) "	�����\"\nA\"�)�" [default-directory load-path tail append mapcar expand-file-name dirs] 6])
(defalias 'normal-top-level #[nil "� ��!��\f���!q�*�\n��B @\nB\n@���@\"É�$�)A��  *�=�r ��!�;�q ��!�P!��\f!�P!��q ��P\")�\f!�܎� *�" [command-line-processed message "Back to top level." t default-directory dir get-buffer "*Messages*" load-path nil new tail load expand-file-name "subdirs.el" system-type vax-vms getenv "PWD" pwd file-attributes file-name-as-directory "." delete "PWD=" process-environment abbreviate-file-name menubar-bindings-done ((byte-code "�	!���� � $!��!�� ��!���!�& � ���!�/ � ��8 ��!��N �=�K �=�N � ���" [abbreviate-file-name default-directory expand-file-name format "%s%d-%s" auto-save-list-file-prefix emacs-pid system-name auto-save-list-file-name run-hooks emacs-startup-hook term-setup-hook fboundp frame-notice-user-settings font-menu-add-default window-setup-hook menubar-bindings-done window-system x win32 precompute-menubar-bindings] 6)) command-line] 6])
(defalias 'precompute-menubar-bindings #[nil "�	�\"��> @:�7 @@9�7 @A�;�7 �@AA!�7 ��@AA\"��7 � �A��	 )ɉ\n�" [lookup-key global-map [menu-bar] submap keymapp x-popup-menu nil purify-flag garbage-collect t define-key-rebound-commands] 4])
(defalias 'command-line #[nil "��!��=�? \fƘ� \fǘ� �	�? \fʘ�* \f˘�0 �	�? \f̘�< \f͘�? �	)��!�њ?�N )�s ��!�њ?�` )�s ��!�њ?�r )�� �\")�� ��!���!���!���ݏ��A !?�� � �#�� #� ��� �# �����@�&'(��'\"�� ��'\"�� '��O&'뉔O'�'(\"�-�=�'��O'�)-;�&�-(\"�0���'\"�0@��O')�)�&)'�8'�C�#A��'���Q'���n&�_A�@&&#�&A��'�����7A��'�����9A��'�����<B<A��'����'����'�����@ ABAA��� &��ہB '\"�+�� ����*�C �D !���D  ��C �E !�\n�E  �F�G =�F�H =�*�I �J  �A�V�0�K �!��L �M !�7�F�N 7ȉ#��Oŉ9�=�X�P �Z9QRSQ�T UV9�uU ��~ہW �X ��VQ=���RVS*R��SV+Y��� �Z [���\\  QY�L �] !��^ �_ !����_ q�`�a =��b �)c� !� F� d !�e�f��N cfPȉ#�Ձg f\"�e�f�eO�f��ŉf��*�h A!�!�2�i �!�" [default-directory command-line-default-directory getenv "VERSION_CONTROL" vc nil "t" "numbered" t version-control "nil" "existing" "never" "simple" never "LC_ALL" string "" "LC_CTYPE" "LANG" ctype string-match iso-8859-1-locale-regexp require disp-table standard-display-european iso-syntax error (byte-code "� 	� ��!�Q��#�Ƈ" [window-system noninteractive load term-file-prefix symbol-name "-win" nil t] 4) ((error (byte-code "�	@�=� ��	A\"�6 �	@�N>�) ��	A@��	AA�##�6 ��	@�N��	A�##�\"��� �" [princ error apply concat file-error error-conditions format "%s: %s" mapconcat #[(obj) "�	�\"�" [prin1-to-string obj t] 3] ", " error-message #[(obj) "�	�\"�" [prin1-to-string obj t] 3] external-debugging-output nil window-system kill-emacs] 8))) command-line-args args done noninteractive user-login-name init-file-user user-real-login-name (("--no-init-file") ("--no-site-file") ("--user") ("--debug-init") ("--iconic") ("--icon-type")) argval argi longopts "\\`--" "=" 0 try-completion completion 1 assoc elt "Option `%s' is ambiguous" "-q" "-no-init-file" "-u" "-user" "-no-site-file" site-run-file "-debug-init" init-file-debug "-iconic" (visibility . icon) initial-frame-alist "-icon-type" "-i" "-itype" (icon-type . t) default-frame-alist "Option `%s' doesn't allow an argument" fboundp face-initialize frame-initialize window-system x win32 menu-bar-lines frame-parameters menu-bar-mode run-hooks before-init-hook load inhibit-startup-message startup debug-on-error-initial debug-on-error-should-be-set debug-on-error-from-init-file #[nil "�A 	�=� ��Q�) 	�=� Ƃ) 	�=�% Ȃ) ��Q\n�\n̉�$��A ���̉#)�" [init-file-user system-type ms-dos "~" "/_emacs" windows-nt "~/_emacs" vax-vms "sys$login:.emacs" "/.emacs" user-init-file load t inhibit-default-init nil inhibit-startup-message "default"] 5] inner debug-on-error (byte-code " ����" [inner nil init-file-had-error] 1) ((error (byte-code "��\n@�N\nA� Ă ���\nA�#$��\n��" [message "Error in init file: %s%s%s" error error-message ": " "" mapconcat prin1-to-string ", " t init-file-had-error] 8))) user-mail-address "@" mail-host-address system-name after-init-hook get-buffer "*scratch*" major-mode fundamental-mode initial-major-mode term-file-prefix "TERM" hyphend term "[-_][^-_]+$" command-line-1 kill-emacs] 5])
(defalias 'command-line-1 #[(command-line-args-left) "�5 � �5 \n�5 �# ��!��ɏ�!�)�5 ���!�=�0 ς3 ��!!��Z?�Y?�Y� ՘�Y� ?�X�[ ��!����!�g � ��p ��!���=�� �=�� � �� �� ����!�=�� ��!�=�� ��!�=�� ��!�=�� ��!�=�� �c��� �������#�2�� �2!�� �)\"!c��� ��� ��!�=�� �c��� ��!c�;�>�� ��c���ǁ@ �$��A �B �C ��́D !�E =�D́F !�G =�D́H !�I =�D�J c��KЁK !c��L �!��M �N !)�\n�f�M �O !�P�Q ǁQ RSTU�9@�VW�X �Y �Z �[ \\\"\"]�^�_�`�abcA�d �e V\"��V�Q ��O_V�Q ��SOV�f V]\"�`�=��V�g �OV�`;��h `]\"�i��j �k V\"�i@�g �OV)��_WV�h V\\\"�^�J_�@_B^AV!�)�4^AV!��4V�l ��eV�m ��eV�n ���_�u�o _!^���o @!^A�p ^K!���q ^!��4^ ��4V�r ���_��_^��@^A�s �t ^!!��4V�u ���V�v ��\f_��_^��@^A�w ^!aBa�X a�c\"b�4V�x ��V�y ��Z_�*_^�4@^A^z�{ �w z!!�M�w z!z�| z��#�)�4V�} ���_�o_^�y@^A^;���j �~ !�� ^!��4V�� ����� �!��4�d �� V\"���� V!R�4��������@ �����A������3�d �� V\"���j �� V\"�TT�T�g U��� �w VU\"!S��� �w VU\"!��� R!�.�� R!��Q R*.�wT�O V�W�� S!�W�� �g !���  ,�" [noninteractive input-pending-p init-file-had-error inhibit-startup-echo-area-message get-buffer-create " *temp*" buffer nil (byte-code "�q��\n!������ɘ� � � !!�R��#)�" [buffer insert-file-contents user-init-file re-search-forward "([ 	\n]*setq[ 	\n]+" "inhibit-startup-echo-area-message[ 	\n]+" regexp-quote prin1-to-string init-file-user "" user-login-name "[ 	\n]*)" nil t] 7) ((error)) kill-buffer message key-binding "" describe-project "For information about the GNU Project and its goals, type C-h C-p." substitute-command-keys "For information about the GNU Project and its goals, type \\[describe-project]." command-line-args-left inhibit-startup-message buffer-name "*scratch*" term-setup-hook run-hooks fboundp frame-notice-user-settings window-setup-hook window-system x win32 precompute-menubar-bindings t menubar-bindings-done ((byte-code "���!q�� ���!�)ć" [get-buffer "*scratch*" erase-buffer set-buffer-modified-p nil] 2)) emacs-version "\nCopyright (C) 1995 Free Software Foundation, Inc." "" help-command "u" advertised-undo "" save-buffers-kill-emacs "t" help-with-tutorial "i" info "\n\nType C-h for help; C-x u to undo changes.  (`C-' means use CTRL key.)\nTo kill the Emacs job, type C-x C-c.\nType C-h t for a tutorial on using Emacs.\nType C-h i to enter Info, which you can use to read GNU documentation." format "\n\nType %s for help; \\[advertised-undo] to undo changes.  (`C-' means use CTRL key.)\nTo kill the Emacs job, type \\[save-buffers-kill-emacs].\nType \\[help-with-tutorial] for a tutorial on using Emacs.\nType \\[info] to enter Info, which you can use to read GNU documentation." where-is-internal where key-description "M-x help" display frame-parameters "�" tmm-menubar "\n\nType F10, ESC ` or Meta-` to use the menu bar." "\n\nType \\[tmm-menubar] to use the menu bar." system-type (msdos windowsnt) "\n\nC-mouse-3 (third mouse button, with Control) gets a mode-specific menu." directory-files "~/" "\\`\\.saves-" "\n\nIf an Emacs session crashed recently,\n" "type M-x recover-session RET to recover" " the files you were editing." "" describe-copying "" describe-distribution "" describe-no-warranty "\n\nGNU Emacs comes with ABSOLUTELY NO WARRANTY; type C-h C-w for full details.\nYou may give out copies of Emacs; type C-h C-c to see the conditions.\nType C-h C-d for information on getting the latest version." "\n\nGNU Emacs comes with ABSOLUTELY NO WARRANTY; type \\[describe-no-warranty] for full details.\nYou may give out copies of Emacs; type \\[describe-copying] to see the conditions.\nType \\[describe-distribution] for information on getting the latest version." set-buffer-modified-p sit-for 120 2 command-line-default-directory 0 line first-file-buffer file-count dir argi orig-argi append (("--funcall") ("--load") ("--insert") ("--kill") ("--directory") ("--eval")) mapcar #[(elt) "�	@PC�" ["-" elt] 2] command-switch-alist longopts tem argval completion extra-load-path load-path initial-load-path string-match "^--[^=]*=" try-completion 1 assoc elt error "Option `%s' is ambiguous" "-f" "-funcall" "-e" intern arrayp command-execute "-eval" eval read "-L" "-directory" expand-file-name "-l" "-load" file file-exists-p load "-insert" "File name omitted from `-insert' option" insert-file-contents "-kill" kill-emacs "^\\+[0-9]+\\'" string-to-int command-line-functions did-hook hooks "\\`-" "Unknown option `%s'" find-file find-file-other-window zerop goto-line get-buffer-window other-window buffer-menu] 8])

;ELC   
;;; compiled by rms@mole.gnu.ai.mit.edu on Fri Nov 24 18:18:29 1995
;;; from file /home/fsf/rms/e19/lisp/view.el
;;; emacs version 19.29.91.3.
;;; bytecomp version FSF 2.10
;;; optimization is on.
;;; this file uses opcodes which do not exist in Emacs 18.

(if (and (boundp 'emacs-version)
	 (or (and (boundp 'epoch::version) epoch::version)
	     (string-lessp emacs-version "19.29")))
    (error "`view.el' was compiled for Emacs 19.29 or later"))


#@78 *The overlay face used for highlighting the match found by View mode search.
(defvar view-highlight-face (quote highlight) (#$ . -466))
#@34 Non-nil if View mode is enabled.
(defvar view-mode nil (#$ . 608))
(make-variable-buffer-local (quote view-mode))
#@65 Non-nil means scrolling past the end of buffer exits View mode.
(defvar view-mode-auto-exit nil (#$ . 728))
(byte-code "��!���!�\f ��B��!��B��!���!�% ��B��!���!�6 ��B��!���!�G �	�B��!���!�X �\n�B��!���!�i ��B��!�" [make-variable-buffer-local view-mode-auto-exit boundp view-old-buffer-read-only nil current-load-list view-old-Helper-return-blurb view-scroll-size view-last-regexp view-exit-action view-return-here view-exit-position] 2)
#@114 Overlay used to display where a search operation found its match.
This is local in each buffer, once it is used.
(defvar view-overlay nil (#$ . 1195))
(byte-code "��!���� �B��!� ��B�� � �!����#����#����#����#����#����#����#����#����#����#����#����#����#����#����#����#����#����#����#����#����#����#����#����#����#����#��9��� �B9B9Ǉ" [make-variable-buffer-local view-overlay view-mode minor-mode-alist (view-mode " View") boundp view-mode-map nil current-load-list make-keymap suppress-keymap define-key "q" view-exit "<" beginning-of-buffer ">" end-of-buffer "v" View-scroll-lines-backward "" View-scroll-lines-forward " " "" "\n" View-scroll-one-more-line "" "z" View-scroll-lines-forward-set-scroll-size "g" View-goto-line "=" what-line "." set-mark-command "'" View-back-to-mark "@" "x" exchange-point-and-mark "h" describe-mode "?" "s" isearch-forward "r" isearch-backward "/" View-search-regexp-forward "\\" View-search-regexp-backward "" "" "n" View-search-last-regexp-forward "p" View-search-last-regexp-backward minor-mode-map-alist] 4)
#@381 View FILE in View mode, returning to previous buffer when done.
The usual Emacs commands are not available; instead,
a special set of commands (mostly letters and punctuation)
are defined for moving around in the buffer.
Space scrolls forward, Delete scrolls backward.
For list of all View commands, type ? or h while viewing.

This command runs the normal hook `view-mode-hook'.
(defalias 'view-file #[(file-name) "p�	!�	!�\f\"��\f?� �!?� �\"+�" [get-file-buffer file-name find-file-noselect buf-to-view had-a-buf old-buf switch-to-buffer view-mode-enter buffer-modified-p kill-buffer] 4 (#$ . 2330) "fView file: "])
#@394 View FILE in View mode in other window.
Return to previous buffer when done.
The usual Emacs commands are not available; instead,
a special set of commands (mostly letters and punctuation)
are defined for moving around in the buffer.
Space scrolls forward, Delete scrolls backward.
For list of all View commands, type ? or h while viewing.

This command runs the normal hook `view-mode-hook'.
(defalias 'view-file-other-window #[(file-name) "� �\n!�\n!�\f!��?�  �\f!?�  �\"+�" [current-window-configuration get-file-buffer file-name find-file-noselect buf-to-view had-a-buf old-arrangement switch-to-buffer-other-window view-mode-enter buffer-modified-p kill-buffer] 4 (#$ . 2963) "fView file: "])
#@383 View BUFFER in View mode, returning to previous buffer when done.
The usual Emacs commands are not available; instead,
a special set of commands (mostly letters and punctuation)
are defined for moving around in the buffer.
Space scrolls forward, Delete scrolls backward.
For list of all View commands, type ? or h while viewing.

This command runs the normal hook `view-mode-hook'.
(defalias 'view-buffer #[(buffer-name) "p�\n�\"���\")�" [old-buf switch-to-buffer buffer-name t view-mode-enter nil] 3 (#$ . 3675) "bView buffer: "])
#@442 View BUFFER in View mode in another window.
Return to previous buffer when done, unless NOT-RETURN is non-nil.

The usual Emacs commands are not available in View mode; instead,
a special set of commands (mostly letters and punctuation)
are defined for moving around in the buffer.
Space scrolls forward, Delete scrolls backward.
For list of all View commands, type ? or h while viewing.

This command runs the normal hook `view-mode-hook'.
(defalias 'view-buffer-other-window #[(buffer-name not-return) "� � �\f!��\n!)�" [not-return current-window-configuration return-to switch-to-buffer-other-window buffer-name view-mode-enter] 2 (#$ . 4216) "bView buffer:\nP"])
#@1690 Toggle View mode.
If you use this function to turn on View mode,
"exiting" View mode does nothing except turn View mode off.
The other way to turn View mode on is by calling
`view-mode-enter'.

Letters do not insert themselves.  Instead these commands are provided.
Most commands take prefix arguments.  Commands dealing with lines
default to "scroll size" lines (initially size of window).
Search commands default to a repeat count of one.

M-< or <	move to beginning of buffer.
M-> or >	move to end of buffer.
C-v or Space	scroll forward lines.
M-v or DEL	scroll backward lines.
CR or LF	scroll forward one line (backward with prefix argument).
z		like Space except set number of lines for further
		   scrolling commands to scroll by.
C-u and Digits	provide prefix arguments.  `-' denotes negative argument.
=		prints the current line number.
g		goes to line given by prefix argument.
/ or M-C-s	searches forward for regular expression
\ or M-C-r	searches backward for regular expression.
n		searches forward for last regular expression.
p		searches backward for last regular expression.
C-@ or .	set the mark.
x		exchanges point and mark.
C-s or s	do forward incremental search.
C-r or r	do reverse incremental search.
@ or '		return to mark and pops mark ring.
		  Mark ring is pushed at start of every
		  successful search and when jump to line to occurs.
		  The mark is set on jump to buffer start or end.
? or h		provide help message (list of commands).
\[help-command]		provides help (list of commands or description of a command).
C-n		moves down lines vertically.
C-p		moves upward lines vertically.
C-l		recenters the screen.
q		exit view-mode and return to previous buffer.
(defalias 'view-mode #[(&optional arg) "�	 	?� �!�V� �" [arg view-mode prefix-numeric-value 0 force-mode-line-update] 2 (#$ . 4894) "P"])
#@187 Enter View mode, a Minor mode for viewing text but not editing it.
See the function `view-mode' for more details.

This function runs the normal hook `view-mode-hook'.

\{view-mode-map}
(defalias 'view-mode-enter #[(&optional prev-buffer action) "��!�	 ������ �  �� !�\" � \"� �y����!����!!�" [buffer-read-only view-old-buffer-read-only boundp Helper-return-blurb view-old-Helper-return-blurb t view-mode-auto-exit view-mode format "continue viewing %s" buffer-file-name file-name-nondirectory buffer-name action view-exit-action prev-buffer view-return-here point-marker view-exit-position 0 nil goal-column run-hooks view-mode-hook message substitute-command-keys "Type \\[help-command] for help, \\[describe-mode] for commands, \\[view-exit] to quit."] 4 (#$ . 6732)])
#@172 Exit from view-mode.
If you viewed an existing buffer, that buffer returns to its previous mode.
If you viewed a file that was not present in Emacs, its buffer is killed.
(defalias 'view-exit #[nil "�\n�\n �\n!�� ��N �b�����p	\n�!�7 �!��C �!�C �!�\n�M \n!*�" [nil view-mode view-overlay delete-overlay force-mode-line-update view-mode-auto-exit view-old-buffer-read-only buffer-read-only view-exit-position view-exit-action action viewed-buffer bufferp view-return-here switch-to-buffer window-configuration-p set-window-configuration] 3 (#$ . 7533) nil])
(defalias 'view-window-size #[nil "� S�" [window-height] 1])
(defalias 'view-scroll-size #[nil "� 	� � ^�" [view-window-size view-scroll-size] 2])
#@57 Normal hook run when starting to view a buffer or file.
(defvar view-mode-hook nil (#$ . 8270))
#@116 Move to line LINE in View mode.
Display is centered at LINE.  Sets mark at starting position and pushes
mark ring.
(defalias 'View-goto-line #[(line) "� ��\n!��� ť!�" [push-mark goto-line line recenter view-window-size 2] 3 (#$ . 8373) "p"])
#@203 Scroll forward in View mode, or exit if end of text is visible.
No arg means whole window full, or number of lines set by \[View-scroll-lines-forward-set-scroll-size].
Arg is number of lines to scroll.
(defalias 'View-scroll-lines-forward #[(&optional lines) "�\n �!�\f � �d!�  �V�  �  � �� Y�. ��!��A [� Y�= ��!��A �!��d!�P db����!!���!��y�ɇ" [lines prefix-numeric-value view-scroll-size pos-visible-in-window-p 0 view-mode-auto-exit view-exit view-window-size scroll-up nil scroll-down message substitute-command-keys "End.  Type \\[view-exit] to quit viewing." move-to-window-line -1] 3 (#$ . 8623) "P"])
#@487 Scroll forward LINES lines in View mode, setting the "scroll size".
This is the number of lines which \[View-scroll-lines-forward] and \[View-scroll-lines-backward] scroll by default.
The absolute value of LINES is used, so this command can be used to scroll
backwards (but "scroll size" is always positive).  If LINES is greater than
window height or omitted, then window height is assumed.  If LINES is less
than window height then scrolling context is provided from previous screen.
(defalias 'View-scroll-lines-forward-set-scroll-size #[(&optional lines) "�\n � � �!��V� � [� ^�!�" [lines view-window-size view-scroll-size prefix-numeric-value 0 View-scroll-lines-forward] 3 (#$ . 9247) "P"])
#@70 Scroll one more line up in View mode.
With ARG scroll one line down.
(defalias 'View-scroll-one-more-line #[(&optional arg) "�	�	 \n �!�" [View-scroll-lines-forward arg 1 -1] 2 (#$ . 9961) "P"])
#@169 Scroll backward in View mode.
No arg means whole window full, or number of lines set by \[View-scroll-lines-forward-set-scroll-size].
Arg is number of lines to scroll.
(defalias 'View-scroll-lines-backward #[(&optional lines) "�	�\f �	![� � [!�" [View-scroll-lines-forward lines prefix-numeric-value view-scroll-size] 3 (#$ . 10165) "P"])
#@367 Search forward for Nth occurrence of REGEXP.
Displays line found at center of window.  REGEXP is remembered for
searching with \[View-search-last-regexp-forward] and \[View-search-last-regexp-backward].  Sets mark at starting position and pushes mark ring.

The variable `view-highlight-face' controls the face that is used
for highlighting the match that is found.
(defalias 'View-search-regexp-forward #[(n regexp) "�	\nÚ�\f \f� \n\"�" [view-search n regexp "" view-last-regexp] 4 (#$ . 10512) "p\nsSearch forward (regexp): "])
#@384 Search backward from window start for Nth instance of REGEXP.
Displays line found at center of window.  REGEXP is remembered for
searching with \[View-search-last-regexp-forward] and \[View-search-last-regexp-backward].  Sets mark at starting position and pushes mark ring.

The variable `view-highlight-face' controls the face that is used
for highlighting the match that is found.
(defalias 'View-search-regexp-backward #[(n regexp) "�	[\nÚ� \f� \n\"�" [View-search-regexp-forward n regexp "" view-last-regexp] 4 (#$ . 11051) "p\nsSearch backward (regexp): "])
#@269 Search forward from window end for Nth instance of last regexp.
Displays line found at center of window.  Sets mark at starting position
and pushes mark ring.

The variable `view-highlight-face' controls the face that is used
for highlighting the match that is found.
(defalias 'View-search-last-regexp-forward #[(n) "�	\n\"�" [View-search-regexp-forward n view-last-regexp] 3 (#$ . 11624) "p"])
#@272 Search backward from window start for Nth instance of last regexp.
Displays line found at center of window.  Sets mark at starting position and
pushes mark ring.

The variable `view-highlight-face' controls the face that is used
for highlighting the match that is found.
(defalias 'View-search-last-regexp-backward #[(n) "�	\n\"�" [View-search-regexp-backward n view-last-regexp] 3 (#$ . 12027) "p"])
#@165 Return to last mark set in View mode, else beginning of file.
Displays line at center of window.  Pops mark ring so successive
invocations return to earlier marks.
(defalias 'View-back-to-mark #[(&optional ignore) "��!� eb�� ��� ť!�" [mark t pop-mark recenter view-window-size 2] 3 (#$ . 12435) nil])
(defalias 'view-search #[(times regexp) "����W� Ƃ �!����$� `)�V � �b��: �Ɣƕ#��B �Ɣƕ\"��#��y��� ӥ!�_ ��#���!)�" [regexp view-last-regexp nil where move-to-window-line times 0 -1 re-search-forward t push-mark view-overlay move-overlay make-overlay overlay-put face view-highlight-face recenter view-window-size 2 message "Can't find occurrence %d of %s" sit-for 4] 5])
(provide (quote view))

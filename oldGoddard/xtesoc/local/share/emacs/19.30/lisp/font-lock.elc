;ELC   
;;; compiled by rms@mole.gnu.ai.mit.edu on Fri Nov 24 22:56:23 1995
;;; from file /home/fsf/rms/e19/lisp/font-lock.el
;;; emacs version 19.29.91.3.
;;; bytecomp version FSF 2.10
;;; optimization is on.
;;; this file uses opcodes which do not exist in Emacs 18.

(if (and (boundp 'emacs-version)
	 (or (and (boundp 'epoch::version) epoch::version)
	     (string-lessp emacs-version "19.29")))
    (error "`font-lock.el' was compiled for Emacs 19.29 or later"))


#@58 *If non-nil, means show status messages when fontifying.
(defvar font-lock-verbose t (#$ . -476))
#@588 *If non-nil, the maximum decoration level for fontifying.
If nil, use the default decoration (typically the minimum available).
If t, use the maximum decoration available.
If a number, use that level of decoration (or if not available the maximum).
If a list, each element should be a cons pair of the form (MAJOR-MODE . LEVEL),
where MAJOR-MODE is a symbol or t (meaning the default).  For example:
 ((c++-mode . 2) (c-mode . t) (t . 1))
means use level 2 decoration for buffers in `c++-mode', the maximum decoration
available for buffers in `c-mode', and level 1 decoration otherwise.
(defvar font-lock-maximum-decoration nil (#$ . -581))
#@537 *If non-nil, the maximum size for buffers for fontifying.
Only buffers less than this can be fontified when Font Lock mode is turned on.
If nil, means size is irrelevant.
If a list, each element should be a cons pair of the form (MAJOR-MODE . SIZE),
where MAJOR-MODE is a symbol or t (meaning the default).  For example:
 ((c++-mode . 256000) (c-mode . 256000) (rmail-mode . 1048576))
means that the maximum size is 250K for buffers in `c++-mode' or `c-mode', one
megabyte for buffers in `rmail-mode', and size is irrelevant otherwise.
(defvar font-lock-maximum-size 256000 (#$ . -1229))
#@27 Face to use for comments.
(defvar font-lock-comment-face (quote font-lock-comment-face) (#$ . 1823))
#@26 Face to use for strings.
(defvar font-lock-string-face (quote font-lock-string-face) (#$ . 1930))
#@27 Face to use for keywords.
(defvar font-lock-keyword-face (quote font-lock-keyword-face) (#$ . 2034))
#@33 Face to use for function names.
(defvar font-lock-function-name-face (quote font-lock-function-name-face) (#$ . 2141))
#@33 Face to use for variable names.
(defvar font-lock-variable-name-face (quote font-lock-variable-name-face) (#$ . 2266))
#@29 Face to use for type names.
(defvar font-lock-type-face (quote font-lock-type-face) (#$ . 2391))
#@34 Face to use for reference names.
(defvar font-lock-reference-face (quote font-lock-reference-face) (#$ . 2494))
#@3550 *A list of the keywords to highlight.
Each element should be of the form:

 MATCHER
 (MATCHER . MATCH)
 (MATCHER . FACENAME)
 (MATCHER . HIGHLIGHT)
 (MATCHER HIGHLIGHT ...)

where HIGHLIGHT should be either MATCH-HIGHLIGHT or MATCH-ANCHORED.

For highlighting single items, typically only MATCH-HIGHLIGHT is required.
However, if an item or (typically) items is to be hightlighted following the
instance of another item (the anchor) then MATCH-ANCHORED may be required.

MATCH-HIGHLIGHT should be of the form:

 (MATCH FACENAME OVERRIDE LAXMATCH)

Where MATCHER can be either the regexp to search for, or the function name to
call to make the search (called with one argument, the limit of the search).
MATCH is the subexpression of MATCHER to be highlighted.  FACENAME is an
expression whose value is the face name to use.  FACENAME's default attributes
may be defined in `font-lock-face-attributes'.

OVERRIDE and LAXMATCH are flags.  If OVERRIDE is t, existing fontification may
be overwritten.  If `keep', only parts not already fontified are highlighted.
If `prepend' or `append', existing fontification is merged with the new, in
which the new or existing fontification, respectively, takes precedence.
If LAXMATCH is non-nil, no error is signalled if there is no MATCH in MATCHER.

For example, an element of the form highlights (if not already highlighted):

 "\\\=<foo\\\=>"		Discrete occurrences of "foo" in the value of the
			variable `font-lock-keyword-face'.
 ("fu\\(bar\\)" . 1)	Substring "bar" within all occurrences of "fubar" in
			the value of `font-lock-keyword-face'.
 ("fubar" . fubar-face)	Occurrences of "fubar" in the value of `fubar-face'.
 ("foo\\|bar" 0 foo-bar-face t)
			Occurrences of either "foo" or "bar" in the value
			of `foo-bar-face', even if already highlighted.

MATCH-ANCHORED should be of the form:

 (MATCHER PRE-MATCH-FORM POST-MATCH-FORM MATCH-HIGHLIGHT ...)

Where MATCHER is as for MATCH-HIGHLIGHT with one exception.  The limit of the
search is currently guaranteed to be (no greater than) the end of the line.
PRE-MATCH-FORM and POST-MATCH-FORM are evaluated before the first, and after
the last, instance MATCH-ANCHORED's MATCHER is used.  Therefore they can be
used to initialise before, and cleanup after, MATCHER is used.  Typically,
PRE-MATCH-FORM is used to move to some position relative to the original
MATCHER, before starting with MATCH-ANCHORED's MATCHER.  POST-MATCH-FORM might
be used to move, before resuming with MATCH-ANCHORED's parent's MATCHER.

For example, an element of the form highlights (if not already highlighted):

 ("\\\=<anchor\\\=>" (0 anchor-face) ("\\\=<item\\\=>" nil nil (0 item-face)))

 Discrete occurrences of "anchor" in the value of `anchor-face', and subsequent
 discrete occurrences of "item" (on the same line) in the value of `item-face'.
 (Here PRE-MATCH-FORM and POST-MATCH-FORM are nil.  Therefore "item" is
 initially searched for starting from the end of the match of "anchor", and
 searching for subsequent instance of "anchor" resumes from where searching
 for "item" concluded.)

Note that the MATCH-ANCHORED feature is experimental; in the future, we may
replace it with other ways of providing this functionality.

These regular expressions should not match text which spans lines.  While
\[font-lock-fontify-buffer] handles multi-line patterns correctly, updating
when you edit the buffer does not, since it considers text one line at a time.

Be very careful composing regexps for this list;
the wrong pattern can dramatically slow things down!
(defvar font-lock-keywords nil (#$ . -2614))
(make-variable-buffer-local (quote font-lock-keywords))
#@145 If set by a major mode, should be the defaults for Font Lock mode.
The value should be like the `cdr' of an item in `font-lock-defaults-alist'.
(defvar font-lock-defaults nil (#$ . 6271))
#@1493 Alist of default major mode and Font Lock defaults.
Each item should be a list of the form:

 (MAJOR-MODE . (KEYWORDS KEYWORDS-ONLY CASE-FOLD SYNTAX-ALIST SYNTAX-BEGIN))

where MAJOR-MODE is a symbol.  KEYWORDS may be a symbol (a variable or function
whose value is the keywords to use for fontification) or a list of symbols.
If KEYWORDS-ONLY is non-nil, syntactic fontification (strings and comments) is
not performed.  If CASE-FOLD is non-nil, the case of the keywords is ignored
when fontifying.  If SYNTAX-ALIST is non-nil, it should be a list of cons pairs
of the form (CHAR . STRING) used to set the local Font Lock syntax table, for
keyword and syntactic fontification (see `modify-syntax-entry').

If SYNTAX-BEGIN is non-nil, it should be a function with no args used to move
backwards outside any enclosing syntactic block, for syntactic fontification.
Typical values are `beginning-of-line' (i.e., the start of the line is known to
be outside a syntactic block), or `beginning-of-defun' for programming modes or
`backward-paragraph' for textual modes (i.e., the mode-dependent function is
known to move outside a syntactic block).  If nil, the beginning of the buffer
is used as a position outside of a syntactic block, in the worst case.

These item elements are used by Font Lock mode to set the variables
`font-lock-keywords', `font-lock-keywords-only',
`font-lock-keywords-case-fold-search', `font-lock-syntax-table' and
`font-lock-beginning-of-syntax-function', respectively.
(defvar font-lock-defaults-alist (byte-code "�����	�B�	B�B�	B�	B�B�B�B�B�B�B�B�B�B�B�-�" [((c-font-lock-keywords c-font-lock-keywords-1 c-font-lock-keywords-2 c-font-lock-keywords-3) nil nil ((95 . "w")) beginning-of-defun) ((c++-font-lock-keywords c++-font-lock-keywords-1 c++-font-lock-keywords-2 c++-font-lock-keywords-3) nil nil ((95 . "w") (126 . "w")) beginning-of-defun) ((lisp-font-lock-keywords lisp-font-lock-keywords-1 lisp-font-lock-keywords-2) nil nil ((58 . "w") (45 . "w") (42 . "w") (43 . "w") (46 . "w") (60 . "w") (62 . "w") (61 . "w") (33 . "w") (63 . "w") (36 . "w") (37 . "w") (95 . "w") (38 . "w") (126 . "w") (94 . "w") (47 . "w")) beginning-of-defun) (scheme-font-lock-keywords nil t ((58 . "w") (45 . "w") (42 . "w") (43 . "w") (46 . "w") (60 . "w") (62 . "w") (61 . "w") (33 . "w") (63 . "w") (36 . "w") (37 . "w") (95 . "w") (38 . "w") (126 . "w") (94 . "w") (47 . "w")) beginning-of-defun) (tex-font-lock-keywords nil nil ((36 . "\""))) tex-mode-defaults scheme-mode-defaults lisp-mode-defaults c++-mode-defaults c-mode-defaults bibtex-mode c++-c-mode c++-mode c-mode elec-c-mode emacs-lisp-mode inferior-scheme-mode latex-mode lisp-mode lisp-interaction-mode plain-tex-mode scheme-mode scheme-interaction-mode slitex-mode tex-mode] 16) (#$ . 6467))
#@113 *Non-nil means Font Lock should not fontify comments or strings.
This is normally set via `font-lock-defaults'.
(defvar font-lock-keywords-only nil (#$ . -9275))
#@122 *Non-nil means the patterns in `font-lock-keywords' are case-insensitive.
This is normally set via `font-lock-defaults'.
(defvar font-lock-keywords-case-fold-search nil (#$ . -9444))
#@155 Non-nil means use this syntax table for fontifying.
If this is nil, the major mode's syntax table is used.
This is normally set via `font-lock-defaults'.
(defvar font-lock-syntax-table nil (#$ . 9634))
#@197 *Non-nil means use this function to move back outside of a syntactic block.
If this is nil, the beginning of the buffer is used (in the worst case).
This is normally set via `font-lock-defaults'.
(defvar font-lock-beginning-of-syntax-function nil (#$ . -9843))
(byte-code "��!� ��B��!� ��B��!���!���!�) ��B��!�6 ��B����#�" [boundp font-lock-cache-position nil current-load-list font-lock-cache-state make-variable-buffer-local font-lock-mode font-lock-fontified put permanent-local t] 4)
#@58 Function or functions to run on entry to Font Lock mode.
(defvar font-lock-mode-hook nil (#$ . 10353))
#@1448 Toggle Font Lock mode.
With arg, turn Font Lock mode on if and only if arg is positive.

When Font Lock mode is enabled, text is fontified as you type it:

 - Comments are displayed in `font-lock-comment-face';
 - Strings are displayed in `font-lock-string-face';
 - Certain other expressions are displayed in other faces according to the
   value of the variable `font-lock-keywords'.

You can enable Font Lock mode in any major mode automatically by turning on in
the major mode's hook.  For example, put in your ~/.emacs:

 (add-hook 'c-mode-hook 'turn-on-font-lock)

Or for any visited file with the following in your ~/.emacs:

 (add-hook 'find-file-hooks 'turn-on-font-lock)

The default Font Lock mode faces and their attributes are defined in the
variable `font-lock-face-attributes', and Font Lock mode default settings in
the variable `font-lock-defaults-alist'.  You can set your own default settings
for some mode, by setting a buffer local value for `font-lock-defaults', via
its mode hook.

Where modes support different levels of fontification, you can use the variable
`font-lock-maximum-decoration' to specify which level you generally prefer.
When you turn Font Lock mode on/off the buffer is fontified/defontified, though
fontification occurs only if the buffer is less than `font-lock-maximum-size'.
To fontify a buffer without turning on Font Lock mode, and regardless of buffer
size, you can use \[font-lock-fontify-buffer].
(defalias 'font-lock-mode #[(&optional arg) "�\f �!�V� ?\f:� \f�! \f��  �\f�A� ʚ�/ ��= ����#��H ��!������$���!L��� � ���!���!������$������$���!��� �� � X�� � ��� �� ��� \"��� �� �����#�����#��ed\"�� ��� ����#�����#�� �� *�" [arg prefix-numeric-value 0 font-lock-mode font-lock-maximum-size major-mode t maximum-size on-p buffer-name " *Compiler Input*" nil remove-hook after-change-functions font-lock-after-change-function make-local-hook add-hook make-local-variable font-lock-set-defaults before-revert-hook after-revert-hook font-lock-revert-setup font-lock-revert-cleanup run-hooks font-lock-mode-hook font-lock-fontified buffer-size font-lock-fontify-buffer font-lock-verbose message "Fontifying %s... buffer too big." font-lock-unfontify-region font-lock-thing-lock-cleanup force-mode-line-update] 5 (#$ . 10464) "P"])
#@41 Unconditionally turn on Font Lock mode.
(defalias 'turn-on-font-lock #[nil "��!�" [font-lock-mode 1] 2 (#$ . 12774)])
#@60 Fontify the current buffer the way `font-lock-mode' would.
(defalias 'font-lock-fontify-buffer #[nil "� t� � �U?��!�L�� ��� \"�\n�& � ���͏��> ��� �; ς< �#�� )�" [font-lock-verbose buffer-size 0 verbose make-local-variable font-lock-fontified nil message "Fontifying %s..." buffer-name font-lock-mode font-lock-set-defaults (byte-code "�� �ed\f#��+��" [match-data match-data ((store-match-data match-data)) font-lock-fontify-region verbose t font-lock-fontified] 4) ((quit (byte-code "�ed\"�" [font-lock-unfontify-region] 3))) "Fontifying %s... %s." "done" "aborted" font-lock-after-fontify-buffer] 4 (#$ . 12898) nil])
(defalias 'font-lock-fontify-region #[(beg end &optional loudly) "� ��� É	ʎ� �!��- �\"��6 �#��#.�" [buffer-modified-p t syntax-table nil buffer-file-truename buffer-file-name old-syntax-table inhibit-read-only buffer-undo-list modified ((byte-code "�	!�\n� � � ��!�Ň" [set-syntax-table old-syntax-table modified buffer-modified-p set-buffer-modified-p nil] 2)) font-lock-syntax-table set-syntax-table font-lock-keywords-only font-lock-unfontify-region beg end font-lock-fontify-syntactically-region loudly font-lock-fontify-keywords-region] 6])
(defalias 'font-lock-unfontify-region #[(beg end) "� ���	\n�#�?�# � �# ��!-�" [buffer-modified-p t nil buffer-file-truename buffer-file-name inhibit-read-only buffer-undo-list modified remove-text-properties beg end (face nil) set-buffer-modified-p] 5])
(defalias 'font-lock-after-change-function #[(beg end old-len) "�� �\fb��y�`b��y�`\"+�" [match-data match-data ((store-match-data match-data)) font-lock-fontify-region beg 0 end 1] 3])
#@111 Put proper face on each string and comment between START and END.
START should be at the beginning of a line.
(defalias 'font-lock-fontify-syntactically-region #[(start end &optional loudly) "�\n �P� �� �P� �ŉ�	\n�. ��� \"��~�b�=�D �� �o �V W�` �e\"�z �ŉ%�z  ��`\"�8�� `���#�� ��`ŉ%8�� �`�$��`ŉ%)�8�� �8�� `�e}���ߏ�)�`� $��`ŉ%)`W��`����\n�#���`�#��`ŉ%���`$�8�-�8�X唉b��e}���珈)�`� $��$`ŉ%)���8������#�z��$`ŉ%8�c�`�$��$`ŉ%))��� )����#-�" [comment-start-skip "\\s\"\\|" "\\s\"" "\\s<\\|" "\\s<" nil prevstate prev state comstart synstart loudly message "Fontifying %s... (syntactically...)" buffer-name start font-lock-cache-position font-lock-cache-state font-lock-beginning-of-syntax-function parse-partial-sexp 3 beg re-search-forward end move put-text-property face font-lock-string-face 4 7 (byte-code "�	e�#���!���x�" [re-search-backward comstart move forward-comment 1 " 	" nil] 4) ((error (goto-char end))) font-lock-comment-face t remove-text-properties (face nil) here 0 (byte-code "��!���x�" [forward-comment 1 " 	" nil] 2) ((error (goto-char end))) (face nil)] 8 (#$ . 14580)])
#@276 Fill in one property of the text from START to END.
Arguments PROP and VALUE specify the property and value to put where none are
already in place.  Therefore existing property values are not overwritten.
Optional argument OBJECT is the string or buffer containing the text.
(defalias 'font-lock-fillin-text-property #[(start end prop value &optional object) "�	\n�%���/ �	\n$�		%��\n�%�� �*�" [text-property-any start end prop nil object next next-single-property-change put-text-property value] 7 (#$ . 15859)])
(put (quote font-lock-fillin-text-property) (quote byte-optimizer) (quote byte-compile-inline-expand))
#@121 Uniquify LIST, deleting elements using `delq'.
Return the list with subsequent duplicate items removed by side effects.
(defalias 'font-lock-unique #[(list) "�� �@A\"��� )�" [list delq] 5 (#$ . 16503)])
(put (quote font-lock-unique) (quote byte-optimizer) (quote byte-compile-inline-expand))
#@285 Prepend to one property of the text from START to END.
Arguments PROP and VALUE specify the property and value to prepend to the value
already in place.  The resulting property values are always lists, and unique.
Optional argument OBJECT is the string or buffer containing the text.
(defalias 'font-lock-prepend-text-property #[(start end prop value &optional object) "<�	 � C��U?�e �	$�	#��\f\n<�: \n�< \nC\"���W �@A\"���F ))	%��� +�" [value nil prev next val start end next-single-property-change prop object get-text-property put-text-property append list delq] 10 (#$ . 16811)])
(put (quote font-lock-prepend-text-property) (quote byte-optimizer) (quote byte-compile-inline-expand))
#@283 Append to one property of the text from START to END.
Arguments PROP and VALUE specify the property and value to append to the value
already in place.  The resulting property values are always lists, and unique.
Optional argument OBJECT is the string or buffer containing the text.
(defalias 'font-lock-append-text-property #[(start end prop value &optional object) "<�	 � C��U?�e �	$�	#��\n<�9 \n�; \nC\f\"���W �@A\"���F ))	%��� +�" [value nil prev next val start end next-single-property-change prop object get-text-property put-text-property append list delq] 10 (#$ . 17550)])
(put (quote font-lock-append-text-property) (quote byte-optimizer) (quote byte-compile-inline-expand))
#@111 Apply HIGHLIGHT following a match.
HIGHLIGHT should be of the form MATCH-HIGHLIGHT, see `font-lock-keywords'.
(defalias 'font-lock-apply-highlight #[(highlight) "@��	��8\n� �8����	#���9 �\n��$���\n��A@!$���=�L �\n��A@!$���=�� \n��A@!��\n�%���� �\n$�\n%���%��t �.���=�,\n��A@!�<�� �� Cˉ\nU?�'�\n$�\n#�\n�<�� �� C\"����@A\"���))%���� .���=��\n��A@!�<�N�QCˉ\nU?���\n$�\n#�\n�<����C\"�����@A\"����))%���Z.,�" [highlight match start end 2 override 3 error "No match %d in highlight %S" text-property-not-all face nil put-text-property eval t keep object value prop text-property-any next next-single-property-change prepend prev val get-text-property append list delq] 11 (#$ . 18285)])
(put (quote font-lock-apply-highlight) (quote byte-optimizer) (quote byte-compile-inline-expand))
#@117 Fontify according to KEYWORDS until LIMIT.
KEYWORDS should be of the form MATCH-ANCHORED, see `font-lock-keywords'.
(defalias 'font-lock-fontify-anchored-keywords #[(keywords limit) "@������`^)�A@!�� 	ʎ;�. ��#�2 !�;\f��  @�@����8�h �8�1��#�1�� ���$�1���A@!$�1�=�� ���A@!$�1�=� ��A@!���%���� �$�%���%��� �.�1�=����A@!�<�(�+C !U?���$�# ��! <�g �j C\"�$�$��$�$@$A\"��$�t)$)%���5.�1�=�1��A@!�<����C !U?�/�$�# �� <�� � C!\"�$�$�$�$@$A\"��$�)$)%����.,�)A��8 *��8!+�" [keywords 3 nil highlights lowdarks matcher limit eval match-data match-data ((store-match-data match-data)) re-search-forward t highlight match start end 2 override error "No match %d in highlight %S" text-property-not-all face put-text-property keep object value prop text-property-any next next-single-property-change prepend prev val get-text-property append list delq] 12 (#$ . 19293)])
(put (quote font-lock-fontify-anchored-keywords) (quote byte-optimizer) (quote byte-compile-inline-expand))
#@110 Fontify according to `font-lock-keywords' between START and END.
START should be at the beginning of a line.
(defalias 'font-lock-fontify-keywords-region #[(start end &optional loudly) "	��=�\f 	� � A� �Ɖ�	\n\f\f���< ���\nT�\n�\"#�\f@�	@b�;�Y ��#�^ !��	A��I @@��o@�@����8�� �8�i��#�i�� ���$�i���A@!$�i�=�� ���A@!$�i�=�8��A@!�\"#$�$�\"%�&��3�$\"$&�&$#\"%��&$�\"%���.�i�=����A@!�\"#$#<�`#�c#CƉ)&*U?���$\"$&�$\"#)�&$�*)<��)��)C\"�-�-��-�-@-A\"��-��)-)\"%�&��m.�i�=�i��A@!�\"#$#<��#��#CƉ)&*U?�g�$\"$&�$\"#)�&$�)<�7)�:)C*\"�-�-�W-�-@-A\"��-�F)-)\"%�&��.,�)��@/�\f@�\f��0���/`^/)�\fA@!�� 2�;���/�#��/!��0���@�@����8���8����#������$�����A@!$���=�\"���A@!$���=����A@!�\"#$�$�\"%�&��~�$\"$&�&$#\"%��&$�\"%��S�.���=���A@!�\"#$#<��#��#CƉ)&*U?��$\"$&�$\"#)�&$�*)<��)��)C\"�-�-�-�-@-A\"��-��)-)\"%�&���.���=����A@!�\"#$#<�E#�H#CƉ)&*U?���$\"$&�$\"#)�&$�)<��)��)C*\"�-�-��-�-@-A\"��-��)-)\"%�&��R.,�)A���*��\f8!+�*A��g \fA�\f�( �.�" [font-lock-keywords-case-fold-search font-lock-keywords t font-lock-compile-keywords buffer-name 0 nil highlights matcher keyword count bufname keywords case-fold-search loudly message "Fontifying %s... (regexps..%s)" make-string 46 start re-search-forward end highlight match 2 override 3 error "No match %d in highlight %S" text-property-not-all face put-text-property eval keep object value prop text-property-any next next-single-property-change prepend prev val get-text-property append list delq limit lowdarks match-data match-data ((store-match-data match-data))] 13 (#$ . 20569)])
(defalias 'font-lock-thing-lock-cleanup #[nil "��!� 	� ��!���!� � ��!�" [boundp fast-lock-mode -1 lazy-lock-mode] 2])
(defalias 'font-lock-after-fontify-buffer #[nil "��!� 	� � ���!� � � �" [boundp fast-lock-mode fast-lock-after-fontify-buffer lazy-lock-mode lazy-lock-after-fontify-buffer] 2])
(defalias 'font-lock-revert-setup #[nil "���" [nil font-lock-fontified] 2])
(defalias (quote font-lock-revert-cleanup) (quote turn-on-font-lock))
(defalias 'font-lock-compile-keywords #[(&optional keywords) "� 	���=� � ���\"B�)�" [keywords font-lock-keywords t mapcar #[(item) "<�	 �D�A�� @A�DD�A9�% @�ADD�A@<�2 @AD��" [item (0 font-lock-keyword-face) font-lock-keyword-face 0] 3]] 5])
(defalias 'font-lock-choose-keywords #[(keywords level) ":�	 � 	�� ��A9� �> ��/ 8�> �!@�> �=�< �!@�> @)�" [level major-mode t keywords reverse] 2])
#@330 Set fontification defaults appropriately for this mode.
Sets `font-lock-keywords', `font-lock-keywords-only', `font-lock-syntax-table',
`font-lock-beginning-of-syntax-function' and
`font-lock-keywords-case-fold-search' using `font-lock-defaults' (or, if nil,
using `font-lock-defaults-alist') and `font-lock-maximum-decoration'.
(defalias 'font-lock-set-defaults #[nil "� �	�� \n� \f�A�@\"�!�%  �) �!A@�6 ��!�L��8�B ��!�L��8�r �8��!�� !L��q �@@@A#�A��[ )�8� ��!�8L*�" [font-lock-make-faces font-lock-keywords font-lock-defaults major-mode font-lock-defaults-alist defaults font-lock-choose-keywords font-lock-maximum-decoration keywords fboundp eval make-local-variable font-lock-keywords-only t 2 font-lock-keywords-case-fold-search 3 slist font-lock-syntax-table copy-syntax-table syntax-table modify-syntax-entry 4 font-lock-beginning-of-syntax-function] 5 (#$ . 23529)])
#@344 A symbol indicating the display Emacs is running under.
The symbol should be one of `color', `grayscale' or `mono'.
If Emacs guesses this display attribute wrongly, either set this variable in
your `~/.emacs' or set the resource `Emacs.displayType' in your `~/.Xdefaults'.
See also `font-lock-background-mode' and `font-lock-face-attributes'.
(defvar font-lock-display-type nil (#$ . 24447))
#@326 A symbol indicating the Emacs background brightness.
The symbol should be one of `light' or `dark'.
If Emacs guesses this frame attribute wrongly, either set this variable in
your `~/.emacs' or set the resource `Emacs.backgroundMode' in your
`~/.Xdefaults'.
See also `font-lock-display-type' and `font-lock-face-attributes'.
(defvar font-lock-background-mode nil (#$ . 24846))
#@1229 A list of default attributes to use for face attributes.
Each element of the list should be of the form

 (FACE FOREGROUND BACKGROUND BOLD-P ITALIC-P UNDERLINE-P)

where FACE should be one of the face symbols `font-lock-comment-face',
`font-lock-string-face', `font-lock-keyword-face', `font-lock-type-face',
`font-lock-function-name-face', `font-lock-variable-name-face', and
`font-lock-reference-face'.  A form for each of these face symbols should be
provided in the list, but other face symbols and attributes may be given and
used in highlighting.  See `font-lock-keywords'.

Subsequent element items should be the attributes for the corresponding
Font Lock mode faces.  Attributes FOREGROUND and BACKGROUND should be strings
(default if nil), while BOLD-P, ITALIC-P, and UNDERLINE-P should specify the
corresponding face attributes (yes if non-nil).

Emacs uses default attributes based on display type and background brightness.
See variables `font-lock-display-type' and `font-lock-background-mode'.

Resources can be used to over-ride these face attributes.  For example, the
resource `Emacs.font-lock-comment-face.attributeUnderline' can be used to
specify the UNDERLINE-P attribute for face `font-lock-comment-face'.
(defvar font-lock-face-attributes nil (#$ . 25231))
#@206 Make faces from `font-lock-face-attributes'.
A default list is used if this is nil.
If optional OVERRIDE is non-nil, faces that already exist are reset.
See `font-lock-make-face' and `list-faces-display'.
(defalias 'font-lock-make-faces #[(&optional override) "�) ���\"�� �\f�!�' � � ǂ' � �& ɂ' �)�| ���\"� ��D ��!�y �=�^ ����A\"�Z ւy ׂy �����A!\"����!\"ܥW�x ׂy �*�6�=�>�� ������ �A�� �A�扯����3�>�)���� 삺 ������� �� �������� �� ��扯��� �A�� �A�扯���� �� ����������������������3�2��3�)��\"�" [font-lock-display-type x-get-resource ".displayType" "DisplayType" display-resource intern x-display-color-p color x-display-grayscale-p grayscale mono font-lock-background-mode ".backgroundMode" "BackgroundMode" frame-parameters params bg-resource system-type ms-dos string-match "light" background-color light dark apply + x-color-values "white" 3 font-lock-face-attributes light-bg (mono monochrome) (font-lock-comment-face nil nil t t nil) (font-lock-string-face nil nil nil t nil) (font-lock-keyword-face nil nil t nil nil) font-lock-function-name-face foreground-color t nil (font-lock-variable-name-face nil nil t t nil) (font-lock-type-face nil nil t nil t) (font-lock-reference-face nil nil t nil t) (grayscale greyscale grayshade greyshade) font-lock-comment-face "Gray80" "DimGray" font-lock-string-face "Gray50" "LightGray" font-lock-keyword-face "Gray90" font-lock-variable-name-face font-lock-type-face font-lock-reference-face ((font-lock-comment-face "Firebrick") (font-lock-string-face "RosyBrown") (font-lock-keyword-face "Purple") (font-lock-function-name-face "Blue") (font-lock-variable-name-face "DarkGoldenrod") (font-lock-type-face "DarkOliveGreen") (font-lock-reference-face "CadetBlue")) ((font-lock-comment-face "OrangeRed") (font-lock-string-face "LightSalmon") (font-lock-keyword-face "LightSteelBlue") (font-lock-function-name-face "LightSkyBlue") (font-lock-variable-name-face "LightGoldenrod") (font-lock-type-face "PaleGreen") (font-lock-reference-face "Aquamarine")) mapcar #[(face-attributes) "@\n� �!�- �	!� �	J!� Ƃ- �	!�* 	�L�- �!)�" [face-attributes face override font-lock-make-face boundp facep nil] 2]] 13 (#$ . 26518)])
#@243 Make a face from FACE-ATTRIBUTES.
FACE-ATTRIBUTES should be like an element `font-lock-face-attributes', so that
the face name is the first item in the list.  A variable with the same name as
the face is also set; its value is the face name.
(defalias 'font-lock-make-face #[(face-attributes) "@�	!���	!���	\"��\"�! ��Ώ��\"�- ��я��\"�E �\"�Q �	��#��Q �8�Q �	��#��\"�i �\"�u �	��#��u �8�u �	��#��\"�� �	�8\"�	�L,�" [face-attributes face symbol-name face-name #[(face-name resource) "�	�Q�P\"�" [x-get-resource face-name ".attribute" resource "Face.Attribute"] 4] set-p #[(face-name resource) "	\n\"�� �ĝ)�" [set-p face-name resource set ("on" "true")] 4] on-p make-face add-to-list facemenu-unlisted-faces "Foreground" nil (byte-code "�	\nA@\"�" [set-face-foreground face face-attributes] 3) ((error)) "Background" (byte-code "�	�8\"�" [set-face-background face 2 face-attributes] 4) ((error)) "Bold" make-face-bold t 3 "Italic" make-face-italic 4 "Underline" set-face-underline-p 5] 4 (#$ . 28782)])
#@40 Subdued level highlighting Lisp modes.
(defconst lisp-font-lock-keywords-1 (byte-code "������ư��EC�" ["^(\\(def\\(" "\\(const\\(\\|ant\\)\\|ine-key\\(\\|-after\\)\\|var\\)\\|" "\\(class\\|struct\\|type\\)\\|" "\\([^ 	\n()]+\\)" "\\)\\)\\>" "[ 	'(]*" "\\([^ 	\n)]+\\)?" (1 font-lock-keyword-face) (8 (cond ((match-beginning 3) font-lock-variable-name-face) ((match-beginning 6) font-lock-type-face) (t font-lock-function-name-face)) nil t)] 7) (#$ . 29823))
#@42 Gaudy level highlighting for Lisp modes.
(defconst lisp-font-lock-keywords-2 (byte-code "�	�������ɰ�B���ί\"�" [append lisp-font-lock-keywords-1 "(\\(" "\\(c\\(atch\\|ond\\(\\|ition-case\\)\\)\\|do\\|" "eval-\\(a\\(fter-load\\|nd-compile\\)\\|when-compile\\)\\|flet\\|" "if\\|l\\(abels\\|et\\*?\\)\\|prog[nv12*]?\\|return\\(\\|-from\\)\\|" "save-\\(excursion\\|match-data\\|restriction\\|selected-window\\|" "window-excursion\\)\\|t\\(hrow\\|rack-mouse\\)\\|" "un\\(less\\|wind-protect\\)\\|wh\\(en\\|ile\\)\\)" "\\)\\>" 1 ("\\\\\\\\\\[\\(\\sw+\\)]" 1 font-lock-reference-face prepend) ("`\\(\\sw\\sw+\\)'" 1 font-lock-reference-face prepend) ("\\<:\\sw+\\>" 0 font-lock-reference-face prepend) ("\\<\\&\\(optional\\|rest\\|whole\\)\\>" . font-lock-type-face)] 10) (#$ . 30288))
#@49 Default expressions to highlight in Lisp modes.
(defvar lisp-font-lock-keywords lisp-font-lock-keywords-1 (#$ . 31076))
#@51 Default expressions to highlight in Scheme modes.
(defvar scheme-font-lock-keywords (quote (("(\\(define\\(\\(\\|-\\(generic\\(\\|-procedure\\)\\|method\\)\\)\\|\\(-syntax\\)\\|\\(-class\\)\\)\\)\\>[ 	]*(?\\(\\sw+\\)?" (1 font-lock-keyword-face) (8 (cond ((match-beginning 3) font-lock-function-name-face) ((match-beginning 6) font-lock-variable-name-face) (t font-lock-type-face)) nil t)) ("(\\(and\\|begin\\|c\\(a\\(ll\\(-with-\\(current-continuation\\|input-file\\|output-file\\)\\|/cc\\)\\|se\\)\\|ond\\)\\|d\\(elay\\|o\\)\\|else\\|for-each\\|if\\|l\\(ambda\\|et\\(-syntax\\|\\*?\\|rec\\(\\|-syntax\\)\\)\\)\\|map\\|or\\|syntax\\(\\|-rules\\)\\)\\>" . 1) ("\\<<\\sw+>\\>" . font-lock-type-face) ("\\<:\\sw+\\>" . font-lock-reference-face))) (#$ . 31202))
#@41 Subdued level highlighting for C modes.
(defconst c-font-lock-keywords-1 nil (#$ . 31967))
#@40 Medium level highlighting for C modes.
(defconst c-font-lock-keywords-2 nil (#$ . 32064))
#@39 Gaudy level highlighting for C modes.
(defconst c-font-lock-keywords-3 nil (#$ . 32160))
#@43 Subdued level highlighting for C++ modes.
(defconst c++-font-lock-keywords-1 nil (#$ . 32255))
#@42 Medium level highlighting for C++ modes.
(defconst c++-font-lock-keywords-2 nil (#$ . 32356))
#@41 Gaudy level highlighting for C++ modes.
(defconst c++-font-lock-keywords-3 nil (#$ . 32456))
(defalias 'font-lock-match-c++-style-declaration-item-and-skip-to-next #[(limit) "��!� � Ď��Ǐ*�" [looking-at "[ 	*&]*\\(\\sw+\\)\\(::\\(\\sw+\\)\\)?[ 	]*\\((\\)?" match-data match-data ((store-match-data match-data)) nil (byte-code "�e}���b���!� �`�\"� db��	 ŕb)�" [limit 1 looking-at "[ 	]*\\([,;]\\|$\\)" scan-sexps 0] 3) ((error t))] 3])
(byte-code "�����R����R�����ΰӰ��E���ٯ���Q�B��Q�B��F\"\"�\"���R�D��E\"'�A�C\")�)��Q�B�����Q�B�\"-�-���R�D��E\"1,ԇ" ["break\\|continue\\|do\\|else\\|for\\|if\\|return\\|switch\\|while" "auto\\|c\\(har\\|onst\\)\\|double\\|e\\(num\\|xtern\\)\\|" "float\\|int\\|long\\|register\\|" "s\\(hort\\|igned\\|t\\(atic\\|ruct\\)\\)\\|typedef\\|" "un\\(ion\\|signed\\)\\|vo\\(id\\|latile\\)" "asm\\|break\\|c\\(atch\\|ontinue\\)\\|d\\(elete\\|o\\)\\|" "else\\|for\\|if\\|new\\|" "p\\(r\\(ivate\\|otected\\)\\|ublic\\)\\|return\\|" "s\\(izeof\\|witch\\)\\|t\\(h\\(is\\|row\\)\\|ry\\)\\|while" "auto\\|bool\\|c\\(har\\|lass\\|o\\(mplex\\|nst\\)\\)\\|" "double\\|e\\(num\\|xtern\\)\\|f\\(loat\\|riend\\)\\|" "in\\(line\\|t\\)\\|long\\|register\\|" "s\\(hort\\|igned\\|t\\(atic\\|ruct\\)\\)\\|" "t\\(emplate\\|ypedef\\)\\|un\\(ion\\|signed\\)\\|" "v\\(irtual\\|o\\(id\\|latile\\)\\)" c++-type-types c++-keywords c-type-types c-keywords "^\\(\\sw+\\)[ 	]*(" 1 font-lock-function-name-face ("^#[ 	]*include[ 	]+\\(<[^>\"\n]+>\\)" 1 font-lock-string-face) ("^#[ 	]*define[ 	]+\\(\\(\\sw+\\)(\\)" 2 font-lock-function-name-face) ("^#[ 	]*if\\>" ("\\<\\(defined\\)\\>[ 	]*(?\\(\\sw+\\)?" nil nil (1 font-lock-reference-face) (2 font-lock-variable-name-face nil t))) ("^\\(#[ 	]*[a-z]+\\)\\>[ 	]*\\(\\sw+\\)?" (1 font-lock-reference-face) (2 font-lock-variable-name-face nil t)) c-font-lock-keywords-1 append "\\<\\(" "\\)\\>" font-lock-type-face font-lock-keyword-face ("\\<\\(case\\|goto\\)\\>[ 	]*\\([^ 	\n:;]+\\)?" (1 font-lock-keyword-face) (2 font-lock-reference-face nil t)) ("^[ 	]*\\(\\sw+\\)[ 	]*:" 1 font-lock-reference-face) c-font-lock-keywords-2 "\\([ 	*&]+\\sw+\\>\\)*" (font-lock-match-c++-style-declaration-item-and-skip-to-next (goto-char (or (match-beginning 8) (match-end 1))) (goto-char (match-end 1)) (1 (if (match-beginning 4) font-lock-function-name-face font-lock-variable-name-face))) ("\\(}\\)[ 	*]*\\sw" (font-lock-match-c++-style-declaration-item-and-skip-to-next (goto-char (match-end 1)) nil (1 (if (match-beginning 4) font-lock-function-name-face font-lock-variable-name-face)))) ("^\\(\\sw+\\)\\>\\([ 	*]+\\sw+\\>\\)*" (1 font-lock-type-face) (font-lock-match-c++-style-declaration-item-and-skip-to-next (goto-char (or (match-beginning 2) (match-end 1))) nil (1 (if (match-beginning 4) font-lock-function-name-face font-lock-variable-name-face)))) c-font-lock-keywords-3 ("^\\(\\sw+\\)\\(::\\(\\sw+\\)\\)?[ 	]*(" (1 (if (match-beginning 2) font-lock-type-face font-lock-function-name-face)) (3 (if (match-beginning 2) font-lock-function-name-face) nil t)) c++-font-lock-keywords-1 ("\\<\\(operator\\)\\>[ 	]*\\([][)(><!=+-][][)(><!=+-]?\\)?" (1 font-lock-keyword-face) (2 font-lock-function-name-face nil t)) ("\\<\\(case\\|goto\\)\\>[ 	]*\\([^ 	\n:;]+\\)?" (1 font-lock-keyword-face) (2 font-lock-reference-face nil t)) ("^[ 	]*\\(\\sw+\\)[ 	]*:[^:]" 1 font-lock-reference-face) c++-font-lock-keywords-2 (font-lock-match-c++-style-declaration-item-and-skip-to-next (goto-char (or (match-beginning 13) (match-end 1))) (goto-char (match-end 1)) (1 (cond ((match-beginning 2) font-lock-type-face) ((match-beginning 4) font-lock-function-name-face) (t font-lock-variable-name-face))) (3 (if (match-beginning 4) font-lock-function-name-face font-lock-variable-name-face) nil t)) ("\\(}\\)[ 	*]*\\sw" (font-lock-match-c++-style-declaration-item-and-skip-to-next (goto-char (match-end 1)) nil (1 (if (match-beginning 4) font-lock-function-name-face font-lock-variable-name-face)))) ("^\\(\\sw+\\)\\>\\([ 	*]+\\sw+\\>\\)*" (1 font-lock-type-face) (font-lock-match-c++-style-declaration-item-and-skip-to-next (goto-char (or (match-beginning 2) (match-end 1))) nil (1 (cond ((match-beginning 2) font-lock-type-face) ((match-beginning 4) font-lock-function-name-face) (t font-lock-variable-name-face))) (3 (if (match-beginning 4) font-lock-function-name-face font-lock-variable-name-face) nil t))) c++-font-lock-keywords-3] 9)
#@45 Default expressions to highlight in C mode.
(defvar c-font-lock-keywords c-font-lock-keywords-1 (#$ . 36873))
#@47 Default expressions to highlight in C++ mode.
(defvar c++-font-lock-keywords c++-font-lock-keywords-1 (#$ . 36989))
#@48 Default expressions to highlight in TeX modes.
(defvar tex-font-lock-keywords (quote (("\\\\\\(begin\\|end\\|newcommand\\){\\([a-zA-Z0-9\\*]+\\)}" 2 font-lock-function-name-face) ("\\\\\\(cite\\|label\\|pageref\\|ref\\){\\([^} 	\n]+\\)}" 2 font-lock-reference-face) ("^[ 	]*\\\\def\\\\\\(\\(\\w\\|@\\)+\\)" 1 font-lock-function-name-face) "\\\\\\([a-zA-Z@]+\\|.\\)" ("\\\\emph{\\([^}]+\\)}" 1 (quote italic) keep) ("\\\\text\\(\\(bf\\)\\|it\\|sl\\){\\([^}]+\\)}" 3 (if (match-beginning 2) (quote bold) (quote italic)) keep) ("\\\\\\(\\(bf\\)\\|em\\|it\\|sl\\)\\>\\(\\([^}&\\]\\|\\\\[^\\]\\)+\\)" 3 (if (match-beginning 2) (quote bold) (quote italic)) keep))) (#$ . 37111))
(byte-code "�	��\n �	B��!�" [font-lock-mode minor-mode-alist (font-lock-mode " Font") provide font-lock] 2)

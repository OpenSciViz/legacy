CODECENTER PLATFORM GUIDE FOR SUN USERS

SUN SYSTEM REQUIREMENTS

     For CodeCenter's memory, swap space, and disk space requirements, 
     please see the Release Bulletin that accompanies this release.
     
   Supported platforms
     CodeCenter Version 4.2 supports SPARCstations running
     SunOS 4.1.1 - 4.1.4 (Solaris 1.0) or Solaris 2.3 - 2.6.
	
     NOTE: Please refer to your Release Bulletin for information about Sun
           patches that should be installed before you use CodeCenter.

   The process debugging mode (pdm) used in this version of CodeCenter 
     is based on GNU gdb, Version 4.12 on Solaris 2.x platforms and
     Version 4.13 on SunOS 4.x platforms. 
	
     To license its software, this version of CodeCenter uses FLEXlm, 
     Version 5.0a, except that CenterLine-C (clcc) uses FLEXlm 2.40c.
	
  Supported compilers 
     CodeCenter supports the following compilers on Solaris 1 and Solaris 2:

     o	CenterLine-C compiler (clcc) 

     o	Sun K&R C compiler, Version 1.0 and 2.0.1 (cc)

     o	Sun ANSI C compiler, Version 1.0 and 2.0.1 (acc)

     o	SPARCompiler C (ANSI C), Version 3.0.1 (acc or cc)

     CodeCenter supports the following compilers but with limitations to 
     browsing and source level debugging. CenterLine-C is link
     compatible with them. 

     o	GNU C compiler, Version 2.5.8 (gcc) 

     o	FORTRAN


  Supported windowing systems 
     CodeCenter supports both the Motif and OPEN LOOK windowing systems. 
     OPEN LOOK is the default on the Sun platform. You can choose Motif at 
     startup with the -motif switch on the codecenter command line.


SUN C LIBRARY FUNCTIONS REPLACED

     To do its run-time error checking and to make its environment behave 
     like a standard UNIX process, CodeCenter replaces many C library 
     functions and system calls with its own version of them. For some of 
     these functions, you can substitute your own version. However, 
     CodeCenter cannot provide run-time error checking on your 
     substituted function.
     
     To use your own version of a function, load the function in a source or 
     object file before linking your program. If your program has already 
     been linked, you must quit, then start a new CodeCenter session to 
     substitute your function for one of the CodeCenter replacements.
     
     The following three tables list functions that CodeCenter replaces. 
     The three tables apply to

     o	Solaris 1 in libc

     o	Solaris 2 in libc 

     o	Solaris 2 in libucb 


     Table 1 CodeCenter replaces these C Library functions on Solaris 1    


     Name of                 Can You         Name of         Can You      
     Function                Substitute      Function        Substitute   
                             Your Own                        Your Own     
                             Function?                       Function?    
                                                                          
     __builtin_alloc()       no              shmdt()         no           
                                                                          
     _exit()                 no              shmget()        no           
                                                                          
     _longjmp()              no              shmctl()        no           
                                                                          
     _setjmp()               no              sigaction()     no           
                                                                          
     alloca()                no              sigaddset()     yes          
                                                                          
     bcopy()                 yes             sigblock()      no           
                                                                          
     brk()                   no              sigdelset()     yes          
                                                                          
     bzero()                 yes             sigemptyset()   yes          
                                                                          
     close()                 no              sigfillset()    no           
                                                                          
     dup2()                  no              sigismember()   yes          
                                                                          
     fork()                  no              siglongjmp()    no           
                                                                          
     free()                  yes             signal()        no           
                                                                          
     getdtablesize()         no              sigpause()      no           
                                                                          
     getrlimit()             no              sigpending()    no           
                                                                          
     longjmp()               no              sigprocmask()   no           
                                                                          
     malloc()                yes             sigsetjmp()     no           
                                                                          
     mallopt()               yes             sigsetmask()    no           
                                                                          
     mallinfo()              yes             sigstack()      no           
                                                                          
     memalign()              yes             sigsuspend()    no           
                                                                          
     memcpy()                yes             sigvec()        no           
                                                                          
     memccpy()               yes             strcat()        yes          
                                                                          
     memset()                yes             strcmp()        yes          
                                                                          
     mmap()                  no              strcpy()        yes          
                                                                          
     munmap()                no              strncat()       yes          
                                                                          
     pause()                 no              strncmp()       yes          
                                                                          
     realloc()               yes             strncpy         yes          
                                                                          
     sbrk()                  no              syscall()       no           
                                                                          
     setjmp()                no              system()        yes          
                                                                          
     setrlimit()             no              valloc()        yes          
                                                                          
     shmat()                 no              vfork()         no           
                                                                          
                                                                          
                                                                          
     Table 2 CodeCenter replaces these C Library functions on 
     Solaris 2 in libc
                                                                          
                                                                          
     Name of                 Can You         Name of         Can You      
     Function                Substitute      Function        Substitute   
                             Your Own                        Your Own     
                             Function?                       Function?    
                                                                          
     __builtin_alloc()       no              shmget()        no           
                                                                          
     _exit()                 no              shmctl()        no           
                                                                          
     alloca()                no              sigaction()     no           
                                                                          
     atexit()                no              sigaddset()     yes          
                                                                          
     brk()                   no              sigdelset()     yes          
                                                                          
     close()                 no              sigemptyset()   yes          
                                                                          
     dup2()                  no              sigfillset()    no           
                                                                          
     exit()                  no              sigismember()   yes          
                                                                          
     fork()                  no              siglongjmp()    no           
                                                                          
     free()                  yes             signal()        no           
                                                                          
     getrlimit()             no              sigpause()      no           
                                                                          
     longjmp()               no              sigpending()    no           
                                                                          
     malloc()                yes             sigprocmask()   no           
                                                                          
     memalign()              yes             sigsetjmp()     no           
                                                                          
     memcpy()                yes             sigsuspend()    no           
                                                                          
     memccpy()               yes             strcat()        yes          
                                                                          
     memove()                no              strcmp()        yes          
                                                                          
     memset()                yes             strcpy()        yes          
                                                                          
     mmap()                  no              strncat()       yes          
                                                                          
     munmap()                no              strncmp()       yes          
                                                                          
     pause()                 no              strncpy()       yes          
                                                                          
     realloc()               yes             syscall()       no           
                                                                          
     sbrk()                  no              sysconf()       no           
                                                                          
     setjmp()                no              system()        yes          
                                                                          
     setrlimit()             no              valloc()        yes          
                                                                          
     shmat()                 no              vfork()         no           
                                                                          
     shmdt()                 no                                           
                                                                          
                                                                          
                                                                          
     Table 3 CodeCenter replaces these C Library functions on 
     Solaris 2 in libucb
                                                                          
                                                                          
     Name of                 Can You         Name of         Can You      
     Function                Substitute      Function        Substitute   
                             Your Own                        Your Own     
                             Function?                       Function?    
                                                                          
     _longjmp()              no              signal()        no           
                                                                          
     _setjmp()               no              sigpause()      no           
                                                                          
     bcopy()                 yes             sigsetmask()    no           
                                                                          
     bzero()                 yes             sigstack()      no           
                                                                          
     getdtablesize()         no              sigvec()        no           
                                                                          
     getrlimit()             no              syscall()       no           
                                                                          
     longjmp()               no              ucbsignal()     no           
                                                                          
     setjmp()                no              ucbsigpause()   no           
                                                                          
     sigblock()              no              ucbsigblock()   no           
                                                                          
                                                                          
                                                                          
SUN SHARED LIBRARIES

     This section describes the support for shared libraries within the 
     CodeCenter environment. For general information about Sun shared 
     libraries, see the Sun manual Programming Utilities & Libraries  
     on Solaris 1 and Linker and Libraries Manual SunOS 5.0 on Solaris 2.
     
     Reading debugging information on shared libraries is not supported
     in component debugging mode. Without debugging information on a file, 
     you are unable to perform certain debugging activities, such as 
     stepping through functions. For information about what debugging 
     techniques are possible on code without debugging information, see 
     the debugging entry in the Man Browser or CodeCenter Reference 
     Manual.

     CodeCenter supports full source-level debugging of shared libraries
     in process debugging mode. For example, you can step into functions
     in shared libraries that were compiled with -g.  

     Once you load a shared library into CodeCenter, its functions and 
     any of its data that you have exported are available to your program.
     CodeCenter mimics the behavior of the system's link editors, ld
     and ld.so, with regard to loading shared libraries and with regard
     to binding functions and data.

   Search rules for loading libraries
     When you load a library using the load command's -l switch, 
     CodeCenter searches for libraries in its search path in the same 
     way that the Sun ld command does. CodeCenter stops searching as 
     soon as it finds either the shared or static version of the library. 
     If it finds both versions in the same directory, CodeCenter uses the 
     shared version (.so file) by default. You can, however, override this 
     default behavior by specifying the binding mode:

     On Solaris 1:

	-> load -lX11 
	Attaching: /s/apps/openwin3.0/lib/libX11.sa.4.3 
	Attaching: /s/apps/openwin3.0/lib/libX11.so.4.3 
	-> unload -lX11 
	Detaching: /s/apps/openwin3.0/lib/libX11.sa.4.3 
	Detaching: /s/apps/openwin3.0/lib/libX11.so.4.3 
	-> load -Bstatic -lX11 
	Attaching: /s/apps/openwin3.0/lib/libX11.a 
	-> 

     On Solaris 2:

       -> load -lX11
       Attaching: /usr/openwin/lib/libX11.so.4
       Attaching: /usr/lib/libsocket.so.1
       Attaching: /usr/lib/libnsl.so.1
       Attaching: /usr/lib/libintl.so.1
       Attaching: /usr/lib/libw.so.1
       -> unload -X11
       Detaching: /usr/openwin/lib/libX11.so.4
       -> load -Bstatic -lX11
       Attaching: /usr/openwin/lib/libX11.a
       ->

     Note that CodeCenter may load more libraries than just the X11
     library. When you load a library into CodeCenter, CodeCenter also
     loads any other library referenced by the library you loaded
     explicitly.  

     CodeCenter unloads only the X11 library as a result of unload -X11. 
     The unload command unloads only those libraries that you explicitly 
     unload, regardless of whether CodeCenter loaded them as a result 
     of a reference or an explicit load command.  

     CodeCenter allows you to load both the static and shared versions 
     of a library, but we do not recommend that you do so.

     On Solaris 1, when CodeCenter loads a shared object (an .so
     file), it looks in the same directory for a data interface
     description file (an .sa file) with the same root name and version
     number. If it finds such an .sa file, it loads it so it can
     generate the necessary data references.  CodeCenter does not
     report an error if it fails to find such an .sa file.

     NOTE	CodeCenter does not load an .sa file with the 
		same root name but different version number 
		as an .so file. This situation is treated the same 
		as a missing .sa file.


   Using environment variables to modify loading libraries
     One way to affect how CodeCenter loads shared libraries is by 
     setting environment variables before starting CodeCenter. Like ld, 
     CodeCenter takes into account the following environment 
     variables:

     LD_LIBRARY_PATH	A colon-separated list of directories to 
			search for libraries specified with the -l 
			switch. Like ld, CodeCenter looks in 
			directories specified in this environment 
			variable after looking in libraries specified 
			with the -L switch on the command line.

     LD_OPTIONS		A default set of options to pass to ld. The 
			options specified in LD_OPTIONS are 
			passed to load just as if they were entered 
			first on the command line.

			CodeCenter ignores other environment variables 
			used by ld.

    NOTE	You must set environment variables before you 
		start CodeCenter.


   Using switches to modify the loading of libraries
     Another way to affect how CodeCenter loads shared libraries is to 
     use ld's command-line switches with CodeCenter's load command. 
     The following command-line switches affect how CodeCenter 
     loads libraries:

     -Bdynamic,	Specifies binding mode. -Bdynamic is the 
     -Bstatic	default.


 		-Bdynamic enables dynamic binding; that 
		is, it uses the shared version of a library if 
		one exists.

 		-Bstatic forces static binding; that is, it loads 
		the static version of the library.

     -lx[.v]	Loads a library with the name libx.so or 
		libx.a. If -Bdynamic is in effect at that point 
		on the command line, loads the latest 
		version of the shared library in the first 
		directory found that contains the library. If 
		no shared version is found, loads the static 
		version.

		If you supply a .v suffix, only the version 
		specified will be loaded. If that version is not 
		found, CodeCenter reports an error. If you 
		specify a .v suffix to -l when -Bstatic is in 
		effect, CodeCenter reports a load-time 
		error.

     -Ldir	Adds dir to the directories searched for 
		libraries. 

     CodeCenter ignores other ld command-line switches.

   Specifying the binding mode
     If you want to load the static version of a library instead of the 
     shared version, you can specify the binding mode with the load 
     command.
     
     Just as with ld, you specify the binding mode using the -B switch. The 
     binding mode you specify is in effect until the end of the command 
     line or until you specify another binding mode. Because you can 
     specify binding mode more than once with the load command, you 
     can use the shared version of some libraries and the static version of 
     others.
     
     In the following example, the static library One (libOne.a) is loaded, 
     and the shared library Two (libTwo.so) is loaded:

	-> load -Bstatic -lOne -Bdynamic -lTwo

   Loading the static version of the C library
     CodeCenter automatically loads the C library when starting. By
     default, it loads the shared version. You can force CodeCenter to load
     the static version (libc.a) by setting the environment variable
     LD_OPTIONS to the value -Bstatic before starting CodeCenter. That
     causes CodeCenter to load the static versions of all libraries by
     default. Alternatively, you can unload the shared libraries (using
     unload), then explicitly load the static versions by specifying their
     pathnames with the load command.

     When you load a library, CodeCenter automatically loads any
     other libraries that the library references. Unloading the first
     library unloads only that first library, not any of the libraries
     that were referenced by it. To unload the other libraries, you
     must unload them explicitly.
  
   Setting breakpoints in shared libraries while in pdm
     While you are in pdm, you can set a breakpoint in a C library 
     function, such as printf(). To do so, you first set a breakpoint in 
     main(), issue the run command, set the breakpoint in printf(), and 
     issue the cont command: 

     pdm -> stop in printf
     Function "printf" not defined.
     pdm -> stop in main
     stop (1) set at "main.c":8, main().
     pdm -> run
     Resetting to top level.
     Executing: /test_dir/a.out

     Breakpoint 1, main (argc=1, argv=0xf7fff824) at main.c:8
     pdm (break 1) -> stop in printf
     stop (2) set at 0xf76ed904, printf().
     pdm (break 1) -> cont

     Breakpoint 2, 0xf76ed904 in printf ()
     pdm (break 1) ->

     You have to set the breakpoint in this fashion only once; 
     subsequent runs of the program retain the breakpoint in printf():

     pdm 14 -> run
     Resetting to top level.
     Executing: /test_dir/a.out

     Breakpoint 1, main (argc=1, argv=0xf7fff824) at main.c:8
     pdm (break 1) 15 -> cont
     Breakpoint 2, 0xf76ed904 in printf ()

  Binding of functions and data
     Like the Sun dynamic linker/loader, ld.so, CodeCenter binds 
     functions from shared libraries at run time when the functions are 
     called.
     
     Note that, if you have loaded the definition of a function in your own 
     source or object file, it takes precedence over a definition of that 
     function in a shared library. 

     On Solaris 1, CodeCenter binds a library's exported initialized data 
     (usually found in the .sa file) at link time, not at run time (similar 
     to ld's statically linking an .sa file at link time).

   Unloading a specific function
     If you specify a function in a shared library with unload, 
     CodeCenter unloads the entire .so file.

	-> unload printf 
	Detaching: /usr/lib/libc.so.1

    On Solaris 1, CodeCenter does not unload the .sa file.

   Other Solaris 1 differences

     NOTE: The rest of the section on shared libraries applies only
	   to Solaris 1.
    
   Unloading shared objects
     You can unload and unlink an entire shared library at once. 

	-> unload -lX11	 
	Detaching: /usr/openwin/lib/libX11.sa.4.3 
	Detaching: /usr/openwin/lib/libX11.so.4.3 

   Unloading a specific module
     You can also unload specific modules in a data interface description 
     file (.sa file) by specifying the module in parentheses following the 
     library name:

	-> unload /lib/libc.sa.1.6(errlst.o)
	Unloading: /lib/libc.sa.1.6(errlst.o)


   Using initialized global data
     If you declare initialized global data in a program using Solaris 1
     shared libraries, you should include its initialization in your .sa 
     archive, as well as in your .so file. If you don't, your program might 
     not use the correct initialization values.
     
     The problem results from the behavior of Sun's linker, ld, which can 
     only find initializations of data in the user's program or in an archive 
     (.a or .sa file)-not in .so files. When ld can't find the initialization 
     for a variable, the system initializes the variable to 0 (zero).
     
     This issue can come up if you use common variables in C programs. 
     Common variables are global variables that are defined in more than 
     one module without using the extern qualifier. Consider this simple C 
     example with two files, a.c and b.c:

     a.c					 b.c
     int Dogs;					 int Dogs = 12;
     main() 
     {printf("Dogs is %d\n", Dogs);}

     In this example, Dogs is a common variable. It is declared in two 
     modules, namely a.c and b.c, without using the extern qualifier.

     Here's what happens when we compile the example statically:

	% cc -c a.c 
	% cc -c -pic b.c 
	% cc -o static a.o b.o 
	% static 
	Dogs is 12

     The program runs fine. But suppose the code from b.c is in a shared 
     library without an .sa archive. When we link the code, it runs 
     incorrectly:

	% ld -o libb.so.1.0 b.o -assert pure-text 
	% cc -o shared1 a.o -L. -lb 
	% shared1 
	Dogs is 0

     The linker erroneously initialized the variable Dogs to 0 (zero).

     The solution is to create an .sa file containing the static data in b.c:

	% ar r libb.sa.1.0 b.o 
	ar: creating libb.sa.1.0 
	% ranlib libb.sa.1.0 
	% cc -o shared2 a.o -L. -lb 
	% shared2 
	Dogs is 12

     Now the program runs correctly. Notice that in this situation you must 
     initialize data in the .sa file; otherwise the statically linked program 
     runs differently from the dynamically linked one.
     
     As good practice, you should probably always include initializations 
     in your .sa files, even if the static variable is initialized to zero. 
     This results in better shared-library performance. 
     
POTENTIAL SUN ANOMALIES

     In most cases, your programs will run the same within the 
     CodeCenter environment as they do outside the environment. 
     However, there are some platform-specific features that 
     CodeCenter may not support fully, so you may see unexpected 
     behavior. 
     
     This section attempts to call your attention to these potential 
     anomalies. Unless otherwise specified, these anomalies apply to both 
     Solaris 1 and Solaris 2.
     
   Default parser configuration
     The default C parser configuration for the CodeCenter interpreter 
     is K&R. 
     
     Invoke the config_parser command to display the current setting. 
     For more information about specifying a different parser 
     configuration, see the config_parser entry in the CodeCenter 
     Reference.
       
   Sun C compilers and pdm
     On Solaris 2, when you try to debug an executable compiled with
     the SunPro C compiler Version 2.0.1 or with SPARCompiler C
     Version 3.0.1, pdm may report that no debugging symbols were
     found. This is because the Sun compiler puts the debugging
     information that pdm uses in the .stab.excl section of the
     executable, which is stripped by the static linker during final
     execution. 
  
     To work around this problem, compile with the -xs switch. This
     causes the assembler to place debugging information in the .stabs
     section, which is not stripped by the linker.
   
   Switches and variables ignored by load (Solaris 1)
     In Solaris 1, CodeCenter's load command ignores some 
     command-line switches that are accepted by Sun cc, but many of 
     these switches do not change the meaning of a program. The 
     following Sun cc command-line switches ignored by CodeCenter 
     do change the meaning of a program.

     o	-align_block

     o	-fnonstd

     o	-ffloat_option 

     o	-misalign 

     The load command also ignores the FLOAT_OPTION environment 
     variable that is accepted by Sun cc, which does change the meaning 
     of a program.

     CodeCenter does not support object files created using the Sun cc 
     compiler -dalign switch. This switch causes more stringent 
     alignment rules for storing double-precision values in memory. 
     Calling object code functions compiled with -dalign from source code 
     could result in a segmentation violation. However, you can load 
     your whole application as object code without problems.

   Instrumented object code
     On Sun workstations, CodeCenter fails to generate warnings for 
     bad pointer dereferences inside switch statements in instrumented 
     object code compiled without debugging information. You can 
     avoid this limitation by compiling with the -g switch to add 
     debugging information.


   Setting LD_LIBRARY_PATH
     To use the CodeCenter tutorial on Solaris 2 platforms, you must set
     the following environment variables:

     setenv OPENWINHOME /usr/openwin
     setenv LD_LIBRARY_PATH /usr/openwin/lib:/usr/lib:$LD_LIBRARY_PATH

     On Solaris 2, the linker only checks for major numbers. Some
     components of CodeCenter, for example the  vi integration,
     require the library libX11.so.4. If CodeCenter issues an error
     message such as "cannot find libX11.so.4", make sure that
     /usr/openwin/lib contains a symbolic link called libX11.so.4
     pointing to a libX11.so.4.x or libX11.so.5.x library.      

  Locating X11 header files
     The tutorial assumes the X11 header files are installed in
     /usr/include.  If they are not, contact your system administrator to
     put a copy or symbolic link to the location of the X11 header files
     into /usr/include, or add -Ipathname to the CL_INCS line in the
     tutorial Makefile, where pathname is the path to the directory
     containing the X11 header files.  

     If you use the X11R5 libraries instead of the openwin libraries,
     you must explicitly load -lnsl and -lsocket into the Workspace to
     run the tutorial. These dependencies are not automatically
     included in the X11R5 libraries, whereas they are included in the
     openwin libraries.





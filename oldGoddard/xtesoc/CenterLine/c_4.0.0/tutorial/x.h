#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <stdio.h>
#include "bounce.h"

#define Plot(x,y)	XDrawPoint(display,win,gc,(x),(y))
#define Line(x1,y1,x2,y2)	XDrawLine(display,win,gc,(x1),(y1),(x2),(y2))
#define IMARGIN 30

Display *display;
Window win; 
GC gc;
int screen;


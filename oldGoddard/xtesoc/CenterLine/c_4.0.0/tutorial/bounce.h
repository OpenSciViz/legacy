
/*  X version */

/* initial position of window */
#define WX	100
#define WY	100

/* size of window */
#define WIDTH 600
#define HEIGHT 600
#define MARGIN 0

/* number of coordinates */
#define LENGTH 500 
/* was 328 */

/* size of icon */
#define IWIDTH 64

#define IHEIGHT 20

#define INDEX(x) ((x)+100)

#define DECR (100)

#if defined(sparc) || defined(mips)

#ifdef CODECENTER
#define SDELAY 0
#else
#define SDELAY 5000
#endif

#else

#ifdef CODECENTER
#define SDELAY 0
#else
#define SDELAY 1000
#endif

#endif


extern short col_table[], row_table[];

void open_window(), close_window(), draw_circle(), /* draw_inverted(), */ bclear();
void create_table(), do_wait();
int  do_bounce();

void store_shape(), draw_rectangle(), draw_circle();














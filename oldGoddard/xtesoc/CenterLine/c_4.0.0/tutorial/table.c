/*SUPPRESS 590*/

#include "bounce.h"


short col_table[LENGTH] ;
short row_table[LENGTH] ;
short Xmax = WIDTH-IWIDTH-MARGIN;
short Ymax = HEIGHT-IHEIGHT-MARGIN;
short *x_coord, *y_coord;

void create_table()
{

	short *last;
	short x = 0, y = 0;	/* work variables */
	short vx = 4, vy = 0;

	x_coord = col_table;
	y_coord = row_table;
	last = x_coord + LENGTH;
	while (x_coord < last)
	{
		
		*x_coord = x;
		*y_coord = y;

		x = x + vx;
		if (x > Xmax) 
		{
			x = 2 * Xmax - x;
			vx = -vx;
		} 
		else if (x < 0) 
		{
			x = -x;
			vx = -vx;
		}
 		vy = vy + 1;
		y = y + vy;

		if (y >= Ymax) 
		{
			y = 2 * Ymax - y;
			vy = 2 - vy;
		}
		x_coord++;
		y_coord++;
	}
}

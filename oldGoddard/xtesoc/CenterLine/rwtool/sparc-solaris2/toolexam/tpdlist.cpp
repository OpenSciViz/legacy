/*
 * RWTPtrDlist<T> example, from the manual.
 *
 * $Id: tpdlist.cpp,v 1.1 1994/12/05 22:27:16 ramnarin Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * (c) Copyright 1989, 1990, 1991, 1992, 1993, 1994 Rogue Wave Software, Inc.
 * ALL RIGHTS RESERVED
 *
 * The software and information contained herein are proprietary to, and
 * comprise valuable trade secrets of, Rogue Wave Software, Inc., which
 * intends to preserve as trade secrets such software and information.
 * This software is furnished pursuant to a written license agreement and
 * may be used, copied, transmitted, and stored only in accordance with
 * the terms of such license and with the inclusion of the above copyright
 * notice.  This software and information or any other copies thereof may
 * not be provided or otherwise made available to any other person.
 *
 * Notwithstanding any other lease or license that may pertain to, or
 * accompany the delivery of, this computer software and information, the
 * rights of the Government regarding its use, reproduction and disclosure
 * are as set forth in Section 52.227-19 of the FARS Computer
 * Software-Restricted Rights clause.
 * 
 * Use, duplication, or disclosure by the Government is subject to
 * restrictions as set forth in subparagraph (c)(1)(ii) of the Rights in
 * Technical Data and Computer Software clause at DFARS 52.227-7013.
 * 
 * This computer software and information is distributed with "restricted
 * rights."  Use, duplication or disclosure is subject to restrictions as
 * set forth in NASA FAR SUP 18-52.227-79 (April 1985) "Commercial
 * Computer Software-Restricted Rights (April 1985)."  If the Clause at
 * 18-52.227-74 "Rights in Data General" is specified in the contract,
 * then the "Alternate III" clause applies.
 *
 ***************************************************************************
 *
 *
 ***************************************************************************
 *
 * $Log: tpdlist.cpp,v $
# Revision 1.1  1994/12/05  22:27:16  ramnarin
# Initial revision
#
 * Revision 6.2  1994/07/12  20:40:30  vriezen
 * Updated Copyright
 *
 * Revision 6.1  1994/04/15  19:08:49  vriezen
 * Move all files to 6.1
 *
 * Revision 2.3  1993/06/16  01:11:04  keffer
 * Added RCS keywords.
 *
 *
 */

#include <rw/tpdlist.h>
#include <rw/rstream.h>
#include <string.h>

class Dog {
  char* name;
public:
  Dog(){name=0;}
  Dog( const char* c) {
    name = new char[strlen(c)+1];
    strcpy(name, c);
  }

  ~Dog() { delete name; }

  // Define a copy constructor:
  Dog(const Dog& dog) {
    name = new char[strlen(dog.name)+1];
    strcpy(name, dog.name);
  }

  // Define an assignment operator:
  void operator=(const Dog& dog) {
    if (this!=&dog) {
      delete name;
      name = new char[strlen(dog.name)+1];
      strcpy(name, dog.name);
    }
  }

  // Define an equality test operator:
  int operator==(const Dog& dog) const {
    return strcmp(name, dog.name)==0;
  }

  // Define a less-than operator
  int operator<(const Dog& dog) const {
	return strcmp(name, dog.name) < 0;
  }

  friend ostream& operator<<(ostream& str, const Dog& dog){
    str << dog.name;
    return str;
  }
};

main()
{
  RWTPtrDlist<Dog> terriers;
  terriers.insert(new Dog("Cairn Terrier"));
  terriers.insert(new Dog("Irish Terrier"));
  terriers.insert(new Dog("Schnauzer"));

  Dog key1("Schnauzer");
  cout << "The list " <<
    (terriers.contains(&key1) ? "does " : "does not ") <<
    "contain a Schnauzer\n";

  Dog key2("Irish Terrier");
  terriers.insertAt(
      terriers.index(&key2),
      new Dog("Fox Terrier")
    );

  Dog* d;
  while (!terriers.isEmpty()) {
    d = terriers.get();
    cout << *d << endl;
    delete d;
  }

  return 0;
}

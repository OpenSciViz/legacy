/*
 * Example 2: Using the RWCString class
 *
 * $Id: example2.cpp,v 1.1 1994/12/05 22:27:19 ramnarin Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * (c) Copyright 1989, 1990, 1991, 1992, 1993, 1994 Rogue Wave Software, Inc.
 * ALL RIGHTS RESERVED
 *
 * The software and information contained herein are proprietary to, and
 * comprise valuable trade secrets of, Rogue Wave Software, Inc., which
 * intends to preserve as trade secrets such software and information.
 * This software is furnished pursuant to a written license agreement and
 * may be used, copied, transmitted, and stored only in accordance with
 * the terms of such license and with the inclusion of the above copyright
 * notice.  This software and information or any other copies thereof may
 * not be provided or otherwise made available to any other person.
 *
 * Notwithstanding any other lease or license that may pertain to, or
 * accompany the delivery of, this computer software and information, the
 * rights of the Government regarding its use, reproduction and disclosure
 * are as set forth in Section 52.227-19 of the FARS Computer
 * Software-Restricted Rights clause.
 * 
 * Use, duplication, or disclosure by the Government is subject to
 * restrictions as set forth in subparagraph (c)(1)(ii) of the Rights in
 * Technical Data and Computer Software clause at DFARS 52.227-7013.
 * 
 * This computer software and information is distributed with "restricted
 * rights."  Use, duplication or disclosure is subject to restrictions as
 * set forth in NASA FAR SUP 18-52.227-79 (April 1985) "Commercial
 * Computer Software-Restricted Rights (April 1985)."  If the Clause at
 * 18-52.227-74 "Rights in Data General" is specified in the contract,
 * then the "Alternate III" clause applies.
 *
 ***************************************************************************
 *
 ***************************************************************************
 *
 * $Log: example2.cpp,v $
# Revision 1.1  1994/12/05  22:27:19  ramnarin
# Initial revision
#
 * Revision 6.4  1994/07/12  20:40:30  vriezen
 * Updated Copyright
 *
 * Revision 6.3  1994/06/02  20:55:54  foote
 * Added RW_END_PROMPT macro
 *
 * Revision 6.2  1994/06/02  19:20:59  foote
 * Port to Symantec 7.0 on Macintosh
 *
 * Revision 6.1  1994/04/15  19:08:24  vriezen
 * Move all files to 6.1
 *
 * Revision 2.3  1993/09/16  06:20:07  randall
 * port to xlC
 *
 * Revision 2.2  1993/08/07  17:33:01  jims
 * Flush cout before requesting input from cin (MS C8 bug: streams not tied)
 *
 * Revision 2.1  1992/11/27  22:24:47  myersn
 * eliminate NL
 *
 * Revision 2.0  1992/10/23  03:34:26  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.1   07 Jun 1992 17:19:42   KEFFER
 * Tools.h++ V5.1
 * 
 */


// Include the header file for the class RWCString:
#include "rw/cstring.h"
#include "rw/rstream.h"

void exerciseSubString();	// Defined below

#ifdef applec
#  define RW_END_PROMPT   endl << flush 
#else
#  define RW_END_PROMPT   flush
#endif

main()
{
  // Construct an empty string:
  RWCString  x;

  // Read a word from the terminal:
  cout << "Type a word: " << RW_END_PROMPT;
  cin >> x;
  cout << "You typed: " << x << endl;
  
  // Print the number of characters:
  cout << "The length of the word is:\t" << x.length() << endl;

  // Assignment to a char*: 
  RWCString a;
  a = "test string";
  cout << a << endl;
  
  // Access to elements with bounds checking:
#if defined(_MSC_VER) || defined(__xlC__) || defined(__IBMCPP__) && (__IBMCPP__ <= 200)
  // can't resolve operator[] on RWCStrings; claims ambiguity.
  cout << "a(3): " << a(3) << endl;
#else
  cout << "a[3]: " << a[3] << endl;
#endif

  // Construct two RWCStrings:
  RWCString one = "one";
  RWCString two = "two";

  // Concatenate them:
  cout << "one + two :\t" << one + two << endl;
  // Append a char* to one:
  one += "plus";
  cout << "one += plus :\t" << one << endl;

  // Convert a RWCString to upper case:;
  RWCString Case("Test Of toUpper() And toLower()");
  cout << "Original string:   " << Case << endl;
  cout << "toUpper() version: " << toUpper(Case) <<endl;
  cout << "toLower() version: " << toLower(Case) <<endl;

  // Illustration of pattern search:
  RWCString Pattern = "And";
  cout << "Search for pattern using an RWCString containing \"" << Pattern << "\"\n";
  cout << " Index of first match : " 
       << Case.index(Pattern) << "\n\n";

  // Find a char* pattern:
  char* charPattern = "to";
  cout << "Search for pattern using a char* containing \"" << charPattern << "\"\n";
  int iMatch = Case.index(charPattern);
  cout << "Index of first match  : " << iMatch << endl;
  cout << "Index of second match : " 
       << Case.index(charPattern, iMatch+1) << endl;

  exerciseSubString();

  // The following line should not be necessary, but a bug in Sun C++
  // requires it:
  cout.flush();

  return 0;
}

/*
 * Exercise the RWSubString class.
 */

void
exerciseSubString()
{
  // Construct a RWCString and print it out:
  RWCString Y("History is bunk.");
  cout << "\nTest of Substrings.\nOriginal string \"Y\":\n" << Y << endl;

  // Now print out a substring of Y, using a conversion to a RWCString:
  cout << "Y(8,2): " << RWCString(Y(8,2)) << endl;

  // Here is an example of using a RWSubString as an lvalue:
  // This changes the RWCString Y.
  Y(8,2) = "was";
  cout << "Use a substring as an l-value:\n";
  cout << "Y(8,2) = \"was\":\n" << Y << endl;
}

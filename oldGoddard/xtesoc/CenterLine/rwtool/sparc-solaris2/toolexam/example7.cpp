/*
 * Example 7: FileManager.  Saves a linked list of ints.
 * This example appears in Section 11.2 of the manual.
 *
 * $Id: example7.cpp,v 1.1 1994/12/05 22:27:22 ramnarin Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * (c) Copyright 1989, 1990, 1991, 1992, 1993, 1994 Rogue Wave Software, Inc.
 * ALL RIGHTS RESERVED
 *
 * The software and information contained herein are proprietary to, and
 * comprise valuable trade secrets of, Rogue Wave Software, Inc., which
 * intends to preserve as trade secrets such software and information.
 * This software is furnished pursuant to a written license agreement and
 * may be used, copied, transmitted, and stored only in accordance with
 * the terms of such license and with the inclusion of the above copyright
 * notice.  This software and information or any other copies thereof may
 * not be provided or otherwise made available to any other person.
 *
 * Notwithstanding any other lease or license that may pertain to, or
 * accompany the delivery of, this computer software and information, the
 * rights of the Government regarding its use, reproduction and disclosure
 * are as set forth in Section 52.227-19 of the FARS Computer
 * Software-Restricted Rights clause.
 * 
 * Use, duplication, or disclosure by the Government is subject to
 * restrictions as set forth in subparagraph (c)(1)(ii) of the Rights in
 * Technical Data and Computer Software clause at DFARS 52.227-7013.
 * 
 * This computer software and information is distributed with "restricted
 * rights."  Use, duplication or disclosure is subject to restrictions as
 * set forth in NASA FAR SUP 18-52.227-79 (April 1985) "Commercial
 * Computer Software-Restricted Rights (April 1985)."  If the Clause at
 * 18-52.227-74 "Rights in Data General" is specified in the contract,
 * then the "Alternate III" clause applies.
 *
 ***************************************************************************
 *
 ***************************************************************************
 *
 * $Log: example7.cpp,v $
# Revision 1.1  1994/12/05  22:27:22  ramnarin
# Initial revision
#
 * Revision 6.4  1994/07/12  20:40:30  vriezen
 * Updated Copyright
 *
 * Revision 6.3  1994/06/02  21:14:38  foote
 * Added RW_END_PROMPT macro
 *
 * Revision 6.2  1994/06/02  20:40:19  foote
 * Port to Symantec 7.0 on Macintosh
 *
 * Revision 6.1  1994/04/15  19:08:31  vriezen
 * Move all files to 6.1
 *
 * Revision 2.2  1993/08/07  17:00:59  keffer
 * Now flushes output before read to work around Visual C++ problem.
 *
 * Revision 2.1  1992/12/04  05:07:27  myersn
 * update for tools.h++ 6.0
 *
 * Revision 2.0  1992/10/23  03:34:26  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.1   07 Jun 1992 17:11:10   KEFFER
 * Tools.h++ V5.1
 * 
 */

#include "rw/filemgr.h"
#include "rw/rstream.h"

#ifdef applec                          
#  define RW_END_PROMPT   endl << flush
#else                                  
#  define RW_END_PROMPT   flush        
#endif                                 

struct DiskNode {
  int      data;
  RWoffset nextNode;
};

main()
{
  cout << "Test of a linked-list on disk, using the RWFileManager.\n";

  RWFileManager fm("linklist.dat");
  
  // Allocate space for offset to start of the linked list:
  fm.allocate(sizeof(RWoffset));
  // Allocate space for the first link:
  RWoffset thisNode = fm.allocate(sizeof(DiskNode));
  
  fm.SeekTo(fm.start());
  fm.Write(thisNode);
  
  DiskNode n;
  int temp;
  RWoffset lastNode;
  cout << "Input a series of integers, then EOF to end:\n" << RW_END_PROMPT;

  /* Borland bug necessitates explicit test for good. */  
  while ( (cin >> temp).good() ){
    n.data = temp;
    n.nextNode = fm.allocate(sizeof(DiskNode));
    fm.SeekTo(thisNode);
    fm.Write(n.data);
    fm.Write(n.nextNode);
    lastNode = thisNode;
    thisNode = n.nextNode;
  }
  
  fm.deallocate(n.nextNode);
  n.nextNode = RWNIL;
  fm.SeekTo(lastNode);
  fm.Write(n.data);	
  fm.Write(n.nextNode);
  cout << "Now run example8 to read them in.\n" << flush;
  return 0;
}

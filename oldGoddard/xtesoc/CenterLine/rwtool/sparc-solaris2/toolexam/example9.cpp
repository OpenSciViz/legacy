/*
 * Example 9: RWDiskPageHeap. 
 *
 * $Id: example9.cpp,v 1.1 1994/12/05 22:27:24 ramnarin Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * (c) Copyright 1989, 1990, 1991, 1992, 1993, 1994 Rogue Wave Software, Inc.
 * ALL RIGHTS RESERVED
 *
 * The software and information contained herein are proprietary to, and
 * comprise valuable trade secrets of, Rogue Wave Software, Inc., which
 * intends to preserve as trade secrets such software and information.
 * This software is furnished pursuant to a written license agreement and
 * may be used, copied, transmitted, and stored only in accordance with
 * the terms of such license and with the inclusion of the above copyright
 * notice.  This software and information or any other copies thereof may
 * not be provided or otherwise made available to any other person.
 *
 * Notwithstanding any other lease or license that may pertain to, or
 * accompany the delivery of, this computer software and information, the
 * rights of the Government regarding its use, reproduction and disclosure
 * are as set forth in Section 52.227-19 of the FARS Computer
 * Software-Restricted Rights clause.
 * 
 * Use, duplication, or disclosure by the Government is subject to
 * restrictions as set forth in subparagraph (c)(1)(ii) of the Rights in
 * Technical Data and Computer Software clause at DFARS 52.227-7013.
 * 
 * This computer software and information is distributed with "restricted
 * rights."  Use, duplication or disclosure is subject to restrictions as
 * set forth in NASA FAR SUP 18-52.227-79 (April 1985) "Commercial
 * Computer Software-Restricted Rights (April 1985)."  If the Clause at
 * 18-52.227-74 "Rights in Data General" is specified in the contract,
 * then the "Alternate III" clause applies.
 *
 ***************************************************************************
 *
 *
 ***************************************************************************
 *
 * $Log: example9.cpp,v $
# Revision 1.1  1994/12/05  22:27:24  ramnarin
# Initial revision
#
 * Revision 6.3  1994/07/12  20:40:30  vriezen
 * Updated Copyright
 *
 * Revision 6.2  1994/06/02  20:42:31  foote
 * Port to Symantec 7.0 on Macintosh
 *
 * Revision 6.1  1994/04/15  19:08:33  vriezen
 * Move all files to 6.1
 *
 * Revision 2.2  1993/08/07  17:03:51  keffer
 * Now checks to see whether swap file can be opened.
 *
 * Revision 2.1  1993/06/16  01:02:34  keffer
 * Added RCS keywords.
 *
 *
 */

#include "rw/diskpage.h"
#include "rw/rstream.h"

struct Node {
  int	key;
  RWHandle	next;
};

RWHandle head = 0;

const int N = 100;	// Exercise 100 Nodes

main() {

  // Construct a disk-based page heap with page size equal
  // to the size of Node and with 10 buffers:
  RWDiskPageHeap heap(0, 10, sizeof(Node));

  if (!heap.isValid())
  {
    cerr << "Unable to open temporary swap file.\n";
    cerr << "Do you have write privileges?\n" << flush;
    return 0;
  }

  // Build the linked list:
  for (int i=0; i<N; i++){
    RWHandle h = heap.allocate();
    Node* newNode = (Node*)heap.lock(h);
    newNode->key  = i;
    newNode->next = head;
    head = h;
    heap.dirty(h);
    heap.unlock(h);
  }

  // Now walk the list:
  unsigned count = 0;
  RWHandle nodeHandle = head;
  while(nodeHandle){
    Node* node = (Node*)heap.lock(nodeHandle);
    RWHandle nextHandle = node->next;
    heap.unlock(nodeHandle);
    heap.deallocate(nodeHandle);
    nodeHandle = nextHandle;
    count++;
  }

  cout << "List with " << count << " nodes walked.\n";

  // The following line should not be necessary, but a bug in Sun C++
  // requires it:
  cout.flush();

  return 0;
}


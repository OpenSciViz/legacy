#ifndef __BUS_H__
#define __BUS_H__

/*
 * Declarations used for the Bus example in the Tools.h++ manual
 *
 * $Id: bus.h,v 1.1 1994/12/05 22:27:18 ramnarin Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * (c) Copyright 1989, 1990, 1991, 1992, 1993, 1994 Rogue Wave Software, Inc.
 * ALL RIGHTS RESERVED
 *
 * The software and information contained herein are proprietary to, and
 * comprise valuable trade secrets of, Rogue Wave Software, Inc., which
 * intends to preserve as trade secrets such software and information.
 * This software is furnished pursuant to a written license agreement and
 * may be used, copied, transmitted, and stored only in accordance with
 * the terms of such license and with the inclusion of the above copyright
 * notice.  This software and information or any other copies thereof may
 * not be provided or otherwise made available to any other person.
 *
 * Notwithstanding any other lease or license that may pertain to, or
 * accompany the delivery of, this computer software and information, the
 * rights of the Government regarding its use, reproduction and disclosure
 * are as set forth in Section 52.227-19 of the FARS Computer
 * Software-Restricted Rights clause.
 * 
 * Use, duplication, or disclosure by the Government is subject to
 * restrictions as set forth in subparagraph (c)(1)(ii) of the Rights in
 * Technical Data and Computer Software clause at DFARS 52.227-7013.
 * 
 * This computer software and information is distributed with "restricted
 * rights."  Use, duplication or disclosure is subject to restrictions as
 * set forth in NASA FAR SUP 18-52.227-79 (April 1985) "Commercial
 * Computer Software-Restricted Rights (April 1985)."  If the Clause at
 * 18-52.227-74 "Rights in Data General" is specified in the contract,
 * then the "Alternate III" clause applies.
 *
 ***************************************************************************
 *
 *
 ***************************************************************************
 *
 * $Log: bus.h,v $
 * Revision 1.1  1994/12/05  22:27:18  ramnarin
 * Initial revision
 *
 * Revision 6.2  1994/07/12  20:40:30  vriezen
 * Updated Copyright
 *
 * Revision 6.1  1994/04/15  19:08:19  vriezen
 * Move all files to 6.1
 *
 * Revision 2.5  1993/06/21  18:00:55  keffer
 * Added RCS keywords
 *
 *
 */

#include "rw/rwset.h"
#include "rw/cstring.h"
#include "rw/collstr.h"

class Bus : public RWCollectable
{

	RWDECLARE_COLLECTABLE(Bus)

public:

	Bus();
	Bus(int busno, const RWCString& driver);
	~Bus();

	// Inherited from class "RWCollectable":
	RWspace		binaryStoreSize() const;
	int		compareTo(const RWCollectable*) const;
	RWBoolean	isEqual(const RWCollectable*) const;
	unsigned	hash() const;
	void		restoreGuts(RWFile&);
	void		restoreGuts(RWvistream&);
	void		saveGuts(RWFile&) const;
	void		saveGuts(RWvostream&) const;

	void		addPassenger(const char* name);
	void		addCustomer(const char* name);
	size_t		customers() const;
	size_t		passengers() const;
	RWCString	driver() const		{return driver_;}
	int		number() const		{return busNumber_;}

private:

	RWSet		customers_;
	RWSet*		passengers_;
	int		busNumber_;
	RWCString	driver_;
};

#endif


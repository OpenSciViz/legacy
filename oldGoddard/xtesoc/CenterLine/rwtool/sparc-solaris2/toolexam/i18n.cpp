/*
 * Internationalization (I18N) example.
 *
 * $Id: i18n.cpp,v 1.1 1994/12/05 22:27:24 ramnarin Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * (c) Copyright 1989, 1990, 1991, 1992, 1993, 1994 Rogue Wave Software, Inc.
 * ALL RIGHTS RESERVED
 *
 * The software and information contained herein are proprietary to, and
 * comprise valuable trade secrets of, Rogue Wave Software, Inc., which
 * intends to preserve as trade secrets such software and information.
 * This software is furnished pursuant to a written license agreement and
 * may be used, copied, transmitted, and stored only in accordance with
 * the terms of such license and with the inclusion of the above copyright
 * notice.  This software and information or any other copies thereof may
 * not be provided or otherwise made available to any other person.
 *
 * Notwithstanding any other lease or license that may pertain to, or
 * accompany the delivery of, this computer software and information, the
 * rights of the Government regarding its use, reproduction and disclosure
 * are as set forth in Section 52.227-19 of the FARS Computer
 * Software-Restricted Rights clause.
 * 
 * Use, duplication, or disclosure by the Government is subject to
 * restrictions as set forth in subparagraph (c)(1)(ii) of the Rights in
 * Technical Data and Computer Software clause at DFARS 52.227-7013.
 * 
 * This computer software and information is distributed with "restricted
 * rights."  Use, duplication or disclosure is subject to restrictions as
 * set forth in NASA FAR SUP 18-52.227-79 (April 1985) "Commercial
 * Computer Software-Restricted Rights (April 1985)."  If the Clause at
 * 18-52.227-74 "Rights in Data General" is specified in the contract,
 * then the "Alternate III" clause applies.
 *
 ***************************************************************************
 *
 *
 ***************************************************************************
 *
 * $Log: i18n.cpp,v $
# Revision 1.1  1994/12/05  22:27:24  ramnarin
# Initial revision
#
 * Revision 6.3  1994/09/08  17:54:28  jims
 * Add code to guard against highly-unlikely but possible time zone problem
 *
 * Revision 6.2  1994/07/12  20:40:30  vriezen
 * Updated Copyright
 *
 * Revision 6.1  1994/04/15  19:08:35  vriezen
 * Move all files to 6.1
 *
 * Revision 1.8  1994/02/24  00:00:31  jims
 * Remove redefinitions of "fr" and "de"
 *
 * Revision 1.7  1994/02/22  20:03:17  jims
 * Port to DEC C++ under OSF
 *
 * Revision 1.6  1993/09/07  04:13:34  jims
 * Port to release version of Windows NT SDK
 *
 * Revision 1.5  1993/08/07  17:05:04  keffer
 * Clarified some comments.
 *
 * Revision 1.4  1993/08/07  05:03:44  jims
 * Change name of RWZoneCommon to "RWZoneSimple"
 *
 */

#include <rw/rstream.h>
#include <rw/cstring.h>
#include <rw/locale.h>
#include <rw/rwdate.h>
#include <rw/rwtime.h>
STARTWRAP
#include <assert.h>
ENDWRAP

#ifdef _MSC_VER  /* Microsoft */
  const char *const fr = "french";
  const char *const de = "german";
#else
#if defined(__DECCXX) && defined(__osf__)  /* DEC C++ under OSF */
  const char *const fr = "fr_FR.88591";
  const char *const de = "de_DE.88591";
#else
  const char *const fr = "fr";
  const char *const de = "de";
#endif
#endif

int main()
{
  RWDate today = RWDate::now();
  cout << "Today's date in the default locale:            "
       << today << endl;

  RWLocale& here = *new RWLocaleSnapshot("");
  cout << "Today's date using your environment:           "
       << today.asString('x', here) << endl;

  RWLocale::global(&here);
  cout << "The same, but installed as the default locale: "
       << today << endl;

  RWLocale& german = *new RWLocaleSnapshot(de); // or, "de_DE"

  cout << "In German (if available):                      "
       << today.asString('x', german) << endl;

  RWCString str;
  cout << "Now enter a date in German: " << flush;
  str.readLine(cin);
  today = RWDate(str, german);
  if (today.isValid())
    cout << "Printed using the default locale: " << today << endl;

#ifndef RW_IOS_XALLOC_BROKEN
  german.imbue(cin);
  cout << "Enter another date in German: " << flush;
  cin >> today;  // read a German date!
  if (today.isValid())
    cout << today << endl;
#endif

  RWZoneSimple newYorkZone(RWZone::USEastern, RWZone::NoAm);
  RWZoneSimple parisZone  (RWZone::Europe,    RWZone::WeEu);

  /* 
   * Build RWTimes relative to other time zones.  Note that
   * first setting the local time zone is a work-around to
   * guard against a rarely exercised bug in the library in
   * which an RWTime is created as "invalid".  The bug obtains
   * only for particular date/time boundary conditions when
   * the RWTime is built for a time zone with different DST rules
   * than those of the currently set local time zone.
   */
  const RWZone* saveLocal = RWZone::local(&newYorkZone);  
  RWTime leaveNewYork(RWDate(20, 12, 1993), 23,00,00, newYorkZone);

  RWZone::local(&parisZone);
  RWTime leaveParis  (RWDate(30,  3, 1994), 05,00,00, parisZone);

  RWZone::local(saveLocal);  // restore local time zone to original value

  RWTime arriveParis(leaveNewYork + long(7 * 3600));
  RWTime arriveNewYork(leaveParis + long(7 * 3600));

  RWLocaleSnapshot french(fr);
  cout << "Arrive' au Paris a` "
       << arriveParis.asString('c', parisZone, french)
       << ", heure local." << endl;

  cout << "Arrive in New York at "
       << arriveNewYork.asString('c', newYorkZone)
       << ", local time." << endl;

  static RWDaylightRule sudAmerica =
     { 0, 0, TRUE, {8, 4, 0, 120}, {2, 0, 0, 120}};

  RWZoneSimple  ciudadSud( RWZone::Atlantic, &sudAmerica );

  // RWZone::local(new RWZoneSimple(RWZone::Europe, RWZone::WeEu));

  {
  RWTime now = RWTime::now();
  cout << now.hour() << ":" << now.minute() << endl;
  }

  {
  RWTime now = RWTime::now();
  cout << now.asString('H') << ":" << now.asString('M') << endl;
  }

  {
  RWTime now = RWTime::now();
  struct tm tmbuf;
  now.extract(&tmbuf);
  const RWLocale& here = RWLocale::global();  // the default global locale
  cout << here.asString(&tmbuf, 'H') << ":"
       << here.asString(&tmbuf, 'M') << endl;
  }

  {
  RWLocaleSnapshot french(fr);
  double f = 1234567.89;
  long i = 987654;
  RWCString fs = french.asString(f, 2);
  RWCString is = french.asString(i);
  if (french.stringToNum(fs, &f) &&
      french.stringToNum(is, &i))  // verify the conversion
    cout << f << "\t" << i << endl
         << fs << "\t" << is << endl;
  }

  double sawbuck = 1000.;

  double price = 999.;  // $9.99
  double penny = 1.;    //  $.01
  assert(price + penny == sawbuck);

  {
  const RWLocale& here = RWLocale::global();
  double sawbuck = 1000.;
  RWCString tenNone  = here.moneyAsString(sawbuck, RWLocale::NONE);
  RWCString tenLocal = here.moneyAsString(sawbuck, RWLocale::LOCAL);
  RWCString tenIntl  = here.moneyAsString(sawbuck, RWLocale::INTL);
  if (here.stringToMoney(tenNone,  &sawbuck) &&
      here.stringToMoney(tenLocal, &sawbuck) &&
      here.stringToMoney(tenIntl,  &sawbuck))  // verify conversion
    cout << sawbuck << "  " << tenNone << "  "
         << tenLocal << "  " << tenIntl << "  " << endl;
  }

  return 0;
}

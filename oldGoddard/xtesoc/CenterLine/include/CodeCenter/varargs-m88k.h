/* Special varargs.h for use within Codecenter on the Motorola platform */

#ifdef __CENTERLINE__

#ifndef _VARARGS_H              /* SYSV4 sources expect same definitions */
				/* in varargs.h                          */
#define _VARARGS_H
#include <cl_va-m88k.h>


#define va_alist __va_1st_arg

#define va_dcl int va_alist;

#ifndef _STDARG_H
#define va_start(__p) 	(((__p).__va_stk = &va_alist), ((__p).__va_arg = 0))
#endif /* _STDARG_H */
#endif  /* #ifndef _VARARGS_H */

#else

    @@@ The Centerline version of varargs.h should only be used
         within CODECENTER or OBJECTCENTER. @@@

#endif /* !__CENTERLINE__ */

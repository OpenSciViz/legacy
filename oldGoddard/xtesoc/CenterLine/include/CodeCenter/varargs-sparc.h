#ifdef __CENTERLINE__
#ifndef _CENTERLINE_SPARC_VARARGS
#define _CENTERLINE_SPARC_VARARGS

typedef char __vararg_char;
typedef __vararg_char *va_list;

#ifdef _CENTERLINE_SPARC_STDARG
/* use ANSI versions of va_start macro */
#   define __va_start(ap,parmN) (ap=(__vararg_char *)&parmN + sizeof(parmN))
#   define va_start __va_start

#else
/* use K&R versions of va_start macro */
#define va_start(list) list = (char *) &va_alist
#endif /* !__CENTERLINE_SPARC_STDARG */
/* common macros */
#define va_dcl int va_alist;
#define va_arg(list,mode) ((mode *)(list += sizeof(mode)))[-1]
#define va_end(ap)
#endif /* __CENTERLINE_SPARC_VARARGS */
#else
/*
 * Do not include this version of varargs-sparc.h unless you loading your
 * code into Codecenter/Objectcenter as source.
 */
@@@ The Centerline version of varargs-sparc.h should only @@@
@@@ be used within Codecenter/Objectcenter. @@@
#endif /* !__CENTERLINE__ */

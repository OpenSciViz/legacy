/* Special stdarg.h for use in Codecenter/Objectcenter on intel platforms */

#ifdef __CENTERLINE__
/* just use the system version */
#include "/usr/include/stdarg.h"
#else

    @@@ The CENTERLINE version of stdarg.h should only be used within @@@
    @@@ CODECENTER or OBJECTCENTER. @@@

#endif /* __CENTERLINE__ */

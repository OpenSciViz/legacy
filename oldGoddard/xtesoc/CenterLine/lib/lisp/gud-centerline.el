;; $Id: gud-centerline.el,v 1.19 1995/06/19 19:46:26 douglas Exp $
;; ======================================================================
;; Code to make CenterLine work in gud mode.
;; ======================================================================


;; This is an extension to the gud-objectcenter file that was available
;; originally.
;; It attemps to provide support for CodeCenter and ObjectCenter.
;; This is a first pass at this attempt, and some of the names might still
;; refer to ObjectCenter.


(require 'gud)
(require 'ehelp)
(provide 'gud-centerline)

;; ----------------------------------------------------------------------
;; Some utility functions that will be useful later.
;; ----------------------------------------------------------------------

(defun cl-forward-c-name (arg)
  "Move forward ARG number of words - but temporarily change syntax table so
that normal C and C++ variable names are treated as one word."
  (let ((underscore-syntax (char-syntax ?_)))
    (modify-syntax-entry ?_ "w")
    (forward-word arg)
    (modify-syntax-entry ?_ (char-to-string underscore-syntax))))

(defun cl-get-user-selection-at-point (&optional text)
  (interactive (list (read-string "Select item: " (cl-get-selection-at-point))))
  text)

(defun cl-get-selection-at-point ()
  "Get the C or C++ name nearest to point.
If unsuccessful, return empty string."
  (interactive)
  (save-excursion
    ;; Go forward 1 so that if we are on the first character we don't back up to
    ;; the start of the name before this one.
    (forward-char)
    ;; Move to the start of this name.
    (cl-forward-c-name -1)
    (let ((start (point)))
      (cl-forward-c-name 1)
      (buffer-substring start (point)))))

(defun cl-filter-whitespace (text)
  "Return string TEXT with beginning and trailing whitespace removed."
  (interactive "s")
  (save-excursion
    (set-buffer (get-buffer-create " *temp-centerline*"))
    (erase-buffer)
    (insert text)
    (goto-char (point-min))
    (skip-chars-forward " \n\t")
    (let ((start (point))
          string)
      (goto-char (point-max))
      (skip-chars-backward " \t\n" start)
      (setq string (buffer-substring start (point)))
      (kill-buffer nil)
      string)))

(defun uname (&optional silent)
  "Run unix command `uname -mrs' and return a list of three strings
formatted as `os-name' `os-version' `machine-hardware-name'.
With PREFIX, suppress message to minibuffer."
  (interactive "P")
  (save-excursion
    (let
      ( (uname-buf (generate-new-buffer "**UNAME-WORK-BUFFER**"))
        (result-list ())
        (beg-str 0)
        (end-str 0)
        (result-string "")
        (current-buf (current-buffer)))
      (set-buffer uname-buf)
      (call-process "uname" nil t nil "-mrs")
      (if (not silent)
        (setq result-string (buffer-substring (point-min) (1- (point-max)))))
      (goto-char (point-max))
      (forward-char -1)
      (setq end-str (point))
      (skip-chars-backward "^ ")
      (setq result-list (cons (buffer-substring (point) end-str) result-list))
      (skip-chars-backward " ")
      (setq end-str (point))
      (skip-chars-backward "^ ")
      (setq result-list (cons (buffer-substring (point) end-str) result-list))
      (skip-chars-backward " ")
      (setq end-str (point))
      (skip-chars-backward "^ ")
      (setq result-list (cons (buffer-substring (point) end-str) result-list))
      (set-buffer current-buf)
      (kill-buffer uname-buf)
      (if (not silent) (message result-string))
      result-list)))

;;; ----------------------------------------------------------------------
;;; Some things that must be implemented to use gud-mode for objectcenter.
;;; This is similar to the implementation for dbx - but there are differences.
;;; ----------------------------------------------------------------------

;;; History of argument lists passed to objectcenter.
;;; This is kept global to the emacs session.
(defvar gud-centerline-history nil)

(defun gud-centerline-massage-args (file args)
  "For CenterLine's Emacs Main Window support.  Add command-line arguments
and switches to the invocation of ObjectCenter or CodeCenter."
  (setq args (cons "-no_main_window" args))

  (if current-prefix-arg
      (setq args (cons "-ascii" args)))

  (setq args (cons "-emacs" args))	; Tell CX it's running in an Emacs buffer.

  (if (equal "" file)
      args
    (cons "-fullname" (cons file args))))

;; There's no guarantee that Emacs will hand the filter the entire
;; marker at once; it could be broken up across several strings.  We
;; might even receive a big chunk with several markers in it.  If we
;; receive a chunk of text which looks like it might contain the
;; beginning of a marker, we save it here between calls to the
;; filter.

(defvar gud-centerline-marker-acc "")

(defun remove-control-m (string)
  "Return a string which is the same as STRING but with all control-M's
removed."
  (let ((temp-string string)
        new-string)
    (while (not (string= "" temp-string))
      (if (not (equal ?\C-M (elt temp-string 0)))
          (setq new-string (concat new-string
                                   (char-to-string (elt temp-string 0)))))
      (setq temp-string (substring temp-string 1)))
    new-string))

(defun gud-centerline-marker-filter (string)
  "For CenterLine, used by the filter for the ObjectCenter/CodeCenter
process to decide on what text to output to the workspace window."
  (remove-control-m string))

(defun gud-centerline-find-file (f)
  (find-file-noselect f))

;;; Some functions needed in start-up (cl-init).

(defun cl-pdm-filename (command-line-words)
  "Given a list of the command line words, figure out if ObjectCenter or
CodeCenter is being started in PDM mode on a specific file.  If so, return
the name of the file."
  "")

(defun cl-find-colon (str)
  "Find the first colon in a STRING and return the location as an integer,
treating the first character as location 0.  If none, return length of string."
  (let ((end 0)
        (temp str)
        (len (length str)))
    ;; find location in string of colon.
    (while (not (equal "" temp))
      (if (equal ?: (aref temp 0))
          (setq temp "")
        (setq end (1+ end))
        (setq temp (substring temp 1 (length temp)))))
    end))
      
(defun cl-get-host-name (clms-session colon-loc)
  "For CenterLine, given clms session ID as string and LOCATION of colon in
string, return the host-name."
  (if (< colon-loc (length clms-session))
      (substring clms-session 0 colon-loc)
    ""))

(defun cl-get-proc-id (clms-session colon-loc)
  "For CenterLine, given clms session ID as string and LOCATION of colon in
string, return the proc id."
  (if (< colon-loc (length clms-session))
      (string-to-int (substring clms-session (1+ colon-loc) (length clms-session)))
    0))

(defun cl-get-clms-session-string ()
  (goto-char (point-min))
  (end-of-line)
  (buffer-substring 1 (point)))

(defvar cl-clms-proc nil
"For CenterLine, lisp object representing clms process.")

(defvar cl-project-filename "centerline.proj"
  "For ObjectCenter, name of filename of project or default if never saved.")

(defun cl-start-clms ()
  "For CenterLine, start the clms process and return the session id as a
string."
  (save-excursion
    (let* ((temp-buff (set-buffer (get-buffer-create " *temp-clms-startup*")))
	   (clms-session-id-string nil)
	   (clms-pathname nil)
	   (centerline-dir (centerline-get-install-dir))
	   (platform-script (concat centerline-dir "admin/platform"))
	   (arch-os nil)
	   (start nil))
      (if (not (file-executable-p platform-script))
	  (error "Cannot execute %s!" platform-script))
      (setq arch-os (centerline-command-output "" platform-script))
      (if (or (not (stringp arch-os))
	      (string= "" arch-os))
	  (error "Cannot determine host architecture type using %s!"
		 platform-script))
      (setq clms-pathname (concat centerline-dir arch-os "/bin/clms"))

      ;; Start clms...
      ;;
      (setq cl-clms-proc
	    (start-process "clms-gud" temp-buff clms-pathname "-v" "-n"))

      (if cl-clms-proc
          ;; get the clms session id.
          (progn
            (sit-for 30)
            (setq clms-session-id-string (cl-get-clms-session-string)))
	(error "Could not start CenterLine Message Server (clms)!"))
      (set-process-buffer cl-clms-proc nil)
      (kill-buffer temp-buff)
      clms-session-id-string)))

(defvar centerline-product nil 
  "For CenterLine, dictates if CodeCenter or ObjectCenter needs to be started")

(defvar cl-start-pdm nil
  "Dictates if CenterLine should be started in the pdm mode or not")

(defvar cl-current-workspace-buffer nil
  "The workspace buffer of the current CenterLine session.  This is set
whenever one runs the elisp function 'objectcenter'.")
  
;;; cl-init is based on gud-common-init.

(defun cl-init (command-line)
  "Perform initializations for a new session."
  (let* ((words (gud-chop-words command-line))
	 (program (car words))
	 (file-word (cl-pdm-filename words))
	 (args (delq file-word (cdr words)))
	 (file (expand-file-name file-word))
	 (filepart (file-name-nondirectory file))
         (buffername (if (equal "" file-word)
                         "cl-workspace"
                       (concat "cl-workspace-" filepart))))
    (if (string= "-pdm" (car args))
	(setq cl-start-pdm 'start-pdm))

    ;; Switch to the workspace buffer of this session - creating one if none
    ;; exists.
    (setq cl-current-workspace-buffer
          (switch-to-buffer (concat "*" buffername "*")))

    ;; Start the product if there is no process connected to the workspace
    ;; buffer.
    ;;
    (if (comint-check-proc cl-current-workspace-buffer)
	nil
      (goto-char (point-max))
      (if (not (bolp))
	  (insert "\n"))
      (if (not (= (point-min) (point-max)))
	  (progn
	    (insert "\n\n")
	    (narrow-to-page)
	    (insert "o Buffer is narrowed to current "
		    (symbol-name centerline-product)
		    " session.\n  Type \""
		    (substitute-command-keys "\\[widen]")
		    "\" to widen it and reveal previous "
		    "session(s).\n\n")))

      (if (and (null (getenv "DISPLAY"))
	       (null current-prefix-arg))
	  (insert "o There is no DISPLAY variable in the environment.\n"
		  "  Starting " (symbol-name centerline-product) " in ASCII mode.\n\n"))

      (insert "o Current directory is "
	      (expand-file-name (substring default-directory 0 -1))
	      ".\n\n")
      (insert "o Starting " (symbol-name centerline-product) " ...\n\n")
			  
      ;; Start clms and get the CLMS_SESSION environment variable info.
      (let* ((clms-session (cl-start-clms))
             ;; Parse the clms session string.
             (colon-location (cl-find-colon clms-session))
             (clms-host (cl-get-host-name clms-session colon-location))
             (clms-proc-id (cl-get-proc-id clms-session colon-location))
             existing-proc
             clws-dir
             os-info)
        (if (equal "" clms-session)
            ;; Could not start clms.  Insert a message and stop.
            (insert "\nERROR!\n"
		    "Could not start CenterLine Message Server (clms).\n"
		    "Please try again.\n"
		    "If this condition persists, please contact CenterLine "
		    "Customer Support\n")

	  (setq comint-scroll-show-maximum-output nil)
	  (setq comint-scroll-to-bottom-on-input 'this)

          ;; Almost all centerline-specific variables other than
          ;; cl-current-workspace-buffer should be made to have a local value
          ;; for the workspace buffer of the ObjectCenter session to which they
          ;; belong.  This way multiple sessions can co-exist.

	  (make-local-variable 'cl-clms-proc)
	  (put 'cl-clms-proc 'permanent-local t)

          (setq cl-status-record nil)
          (make-local-variable 'cl-status-record)
          (put 'cl-status-record 'permanent-local t)

          (setq cl-source-window-buffer nil)
          (setq cl-source-window-keymap-copy nil)

          (make-local-variable 'cl-project-filename)
          (put 'cl-project-filename 'permanent-local t)

	  (make-local-variable 'cl-source-window-buffer)
	  (put 'cl-source-window-buffer 'permanent-local t)

	  (make-local-variable 'cl-source-window-keymap-copy)
	  (put 'cl-source-window-keymap-copy 'permanent-local t)

          (setq cl-overlay-list nil)
          (make-local-variable 'cl-overlay-list)
          (put 'cl-overlay-list 'permanent-local t)

          ;; Set the default directory to that of the file indicated in the
          ;; ObjectCenter command line if there was one.

          (if (equal "" file-word)
              (setq file "")
            (setq default-directory (file-name-directory file)))

          ;; Add clms info to the command line to start ObjectCenter.

          (setq args (cons "-clms_session" (cons clms-session args)))

	  ;; Start ObjectCenter or CodeCenter.  Make sure it gets a
	  ;; pseudo-terminal and has TERM=emacs in its environment.
	  ;; The combination of the value of the TERM variable and the
	  ;; "-emacs" switch that gud-massage-args adds will tell
	  ;; OC/CC to shut off line editting.  This fixes CLSoc09578.
	  ;;
	  (let ((process-connection-type t))
	    (apply 'make-comint buffername program nil
		   (gud-massage-args file args)))

	  (gud-mode)			; major mode.
	  (set-process-filter (get-buffer-process (current-buffer)) 'gud-filter)
	  (set-process-sentinel (get-buffer-process (current-buffer)) 'cl-sentinel)
	  (gud-set-buffer)

	  ;; Connect to the clms process.

	  (setq clipc-handler-callback-list nil)
	  (setq clipc-listener-callback-list nil)
	  (cl-connect clms-host clms-proc-id)
	  (cl-setup))))))

(defun cl-sentinel (proc msg)
  "For CenterLine, do clean-up to close down an ObjectCenter or CodeCenter
session."
  (cond ((null (buffer-name (process-buffer proc)))
	 ;; buffer killed - assume that clms process is global value of
         ;; cl-clms-proc.
         (delete-process cl-clms-proc)
         (cl-quit))
        (t
         (save-excursion
           (set-buffer (process-buffer proc))
           (cl-dehighlight-all-breakpoints)

	   (widen)	; Widen the workspace buffer.

           ;; Return source window to original state.
	   ;;
           (save-excursion
             (if (and cl-source-window-buffer
                      (not (eq nil (buffer-name cl-source-window-buffer))))
                 ;; Buffer still exists.
                 (progn
                   (set-buffer cl-source-window-buffer)
                   (use-local-map cl-source-window-keymap-copy))))

           ;; Return menu-bar to original state.
	   ;;
           (cl-modify-menu-bar t)
           (delete-process cl-clms-proc)
           (cl-quit))))
  ;; Now do generic gud clean-up.
  (gud-sentinel proc msg))

;; ----------------------------------------------------------------------
;; Code for mouse and key bindings.
;; ----------------------------------------------------------------------

(defun cl-print-at-mouse (event)
  "Display print info on X selection (or item at mouse if no X selection)."
  (interactive "e")
  (let ((item (cl-filter-whitespace (x-get-selection))))
    (if (equal "" item)
        (let* ((loc-info (event-end event))
               (loc (posn-point loc-info))
               (window (posn-window loc-info)))
          (save-excursion
            (select-window window)
            (goto-char loc)
            (setq item (cl-filter-whitespace (cl-get-selection-at-point))))))
    (clipc-request-send "cxcmd_print"
                        (list (list "expr" item)
                              '("typeDepth" 1)
                              '("cxcmdEchoCommand" 0)))))

(defun cl-whatis-at-mouse (event)
  "Display whatis info on X selection (or item at mouse if no X selection)."
  (interactive "e")
  (let ((item (cl-filter-whitespace (x-get-selection))))
    (if (equal "" item)
        (let* ((loc-info (event-end event))
               (loc (posn-point loc-info))
               (window (posn-window loc-info)))
          (save-excursion
            (select-window window)
            (goto-char loc)
            (setq item (cl-filter-whitespace (cl-get-selection-at-point))))))
    (clipc-request-send "cxcmd_whatis"
                        (list (list "idName" item)
                              '("echoOutput" 0)
                              '("cxcmdEchoCommand" 0)))))

(defun cl-matching-status-item-number (file-name line-number)
  "Number of item in status record matching FILENAME and LINENUMBER.
If none, nil."
  (save-excursion
    (set-buffer cl-current-workspace-buffer)
    (let ((temp-record cl-status-record)
          status-item
          found
          (item-number 0))
      (setq file-name (file-truename file-name))
      (while temp-record
        (setq status-item (car temp-record))
        (setq temp-record (cdr temp-record))
        (if (and (equal line-number (cl-status-line status-item))
                 (equal file-name
                        (file-truename (cl-status-file status-item))))
            (progn
              (setq found t)
              (setq temp-record nil)))
        (setq item-number (1+ item-number)))
      (if found
          item-number
        nil))))

(defun cl-debug-delete-breakpoint (item-number)
  "For CenterLine, delete a breakpoint."
  (clipc-request-send "cxcmd_delete"
                      (list (list "itemNumber" item-number)
                            '("cxcmdEchoCommand" 0))))

(defun cl-toggle-break-at-mouse (event)
  "Add or delete break at line mouse points to."
  (interactive "e")
  (let* ((loc-info (event-end event))
         (pos (posn-point loc-info))
         (buffer (window-buffer (car loc-info)))
         (file-name (buffer-file-name buffer))
         line-number)
    (if file-name
        (progn
          (save-excursion
            (set-buffer buffer)
            (setq line-number (count-lines 1 pos)))
          ;; Check if there is a match to this file and line number.
          ;; If so, delete it.  If not, set a break.
          (let ((item-number
                 (cl-matching-status-item-number file-name line-number)))
            (if item-number
                (cl-debug-delete-breakpoint item-number)
              (cl-debug-set-file-breakpoint file-name line-number)))))))

(defvar cl-source-window-hooks nil
  "For CenterLine, list of forms to evaluate when visiting a file as a source
window.")

(defvar cl-source-window-exit-hooks nil
  "For CenterLine, list of forms to evaluate when a buffer ceases to be the
source window.")

(defun cl-do-nothing ()
  (interactive))

(defun cl-default-source-window-hook (&optional remove)
  "Initial hook run when ObjectCenter or CodeCenter load a buffer in the source
window.  If optional arg REMOVE is non-nil, remove CenterLine definitions from
local map."
  (interactive)
  (if remove
      (progn
        (local-unset-key [C-down-mouse-1])
        (local-unset-key [C-mouse-1])
        (local-unset-key [S-mouse-1])
        (local-unset-key [S-mouse-2])
        (local-unset-key "\C-c\C-d")
        (local-unset-key "\C-c\C-n")
        (local-unset-key "\C-c\C-r")
        (local-unset-key "\C-c\C-x")
        (local-unset-key "\C-x ")
        (local-unset-key [S-down-mouse-3])
        (cl-modify-menu-bar t))
    (local-set-key [C-down-mouse-1] 'cl-do-nothing) ; over-ride any global def.
    (local-set-key [C-mouse-1]      'cl-toggle-break-at-mouse)
    (local-set-key [S-mouse-1]      'cl-print-at-mouse)
    (local-set-key [S-mouse-2]      'cl-whatis-at-mouse)
    (local-set-key "\C-c\C-d"       'cl-display-point)
    (local-set-key "\C-c\C-n"       'cl-next)
    (local-set-key "\C-c\C-r"       'cl-run)
    (local-set-key "\C-c\C-x"       'cl-gen-cmd)
    (local-set-key "\C-x "          'cl-stop-loc)
    (cl-add-objectcenter-identifier-popup-menu [S-down-mouse-3])
    (cl-modify-menu-bar)))

(defun cl-add-objectcenter-identifier-popup-menu (key)
  "For CenterLine, attaches the identifier popup menu to KEY in the local
keymap.  Key must be a mouse event."
  (local-set-key key 'cl-build-identifier-menu))

(defvar cl-identifier-map "" "")
(defvar main-map "" "")

;;; In the following code, we use x-popup-menu. Just after that we make an
;;; assumption that none of the items in the menu bring up other menus.
;;; If one is ever added which does, the code making this assumption should be
;;; modified to not make such an assumption.

(defun cl-build-identifier-menu (event)
  (interactive "e")
  (let ((selected-text (cl-filter-whitespace (x-get-selection)))
        callback key)
    (if (equal "" selected-text)
        (setq selected-text
              (cl-filter-whitespace (call-interactively
                                     'cl-get-user-selection-at-point))))
    (if (not (equal "" selected-text))
        (progn
          (setq main-map (list 'keymap selected-text))
          (setq cl-identifier-map (make-sparse-keymap selected-text))
          (define-key cl-identifier-map [cl-display-contents]
            '("display *" . cl-identify-menu-display-contents))
          (define-key cl-identifier-map [cl-display]
            '("display" . cl-identify-menu-display))
          (define-key cl-identifier-map [cl-swap]
            '("swap" . cl-identify-menu-swap))
          (define-key cl-identifier-map [cl-xref]
            '("xref" . cl-identify-menu-xref))
          (define-key cl-identifier-map [cl-examine-class]
            '("examine class" . cl-identify-menu-examine-class))
          (define-key cl-identifier-map [cl-expand]
            '("expand" . cl-identify-menu-expand))
          (define-key cl-identifier-map [cl-whereis]
            '("whereis" . cl-identify-menu-whereis))
          (define-key cl-identifier-map [cl-whatis]
            '("whatis" . cl-identify-menu-whatis))
          (define-key cl-identifier-map [cl-dump-variable]
            '("dump variable" . cl-identify-menu-dump-variable))
          (define-key cl-identifier-map [cl-dump-function]
            '("dump function" . cl-identify-menu-dump-function))
          (define-key cl-identifier-map [cl-print-contents]
            '("print *" . cl-identify-menu-print-contents))
          (define-key cl-identifier-map [cl-print]
            '("print" . cl-identify-menu-print))
          (define-key cl-identifier-map [cl-trace]
            '("trace" . cl-identify-menu-trace))
          (define-key cl-identifier-map [cl-stop-on-expression]
            '("stop on expression" . cl-identify-menu-stop-on-expression))
          (define-key cl-identifier-map [cl-stop-in-function]
            '("stop in function" . cl-identify-menu-stop-in-function))
          (define-key cl-identifier-map [cl-list]
            '("list" . cl-identify-menu-list))
          (define-key cl-identifier-map [cl-edit]
            '("edit" . cl-identify-menu-edit))
          (define-key main-map [cl-identify]
            (cons (concat "@" selected-text) cl-identifier-map))
          ;; Note: x-popup-menu takes over the command stream.
          ;; When you select an item in such a menu, it does not execute the
          ;; item bound to it.  Instead, it returns a list of the items defining
          ;; the menu-item in the keymap.  This is why we do what is done below.
          (setq key (x-popup-menu event main-map))
          ;; We will assume that key is a list of two items.
          (setq key (vector (car-safe key) (car-safe (cdr-safe key))))
          (setq callback (lookup-key main-map key))
          (if callback
              (funcall callback selected-text))))))

;;; The callbacks defined below for the pop-up menu defined above all take one
;;; argument, the selected text.  We don't check that the selected text is ok
;;; because that's done in the code above.  (Though, not much is checked for.)

(defun cl-identify-menu-edit (identifier)
  "For CenterLine, send clipc message to edit IDENTIFIER."
  (interactive)
  (clipc-request-send "cxcmd_edit"
                      (list (list "idName" identifier)
                            '("cxcmdEchoCommand" 0))))

(defun cl-identify-menu-list (identifier)
  "For CenterLine, send clipc message to list IDENTIFIER."
  (interactive)
  (clipc-request-send "cxcmd_list"
                      (list (list "idName" identifier)
                            '("cxcmdEchoCommand" 0))))

(defun cl-identify-menu-stop-in-function (identifier)
  "For CenterLine, send clipc message to stop in function IDENTIFIER."
  (interactive)
  (clipc-request-send "cxcmd_action" (list
                                      '("locType" "funcName")
                                      (list "funcName" identifier)
                                      '("cxcmdEchoCommand" 0))))

(defun cl-identify-menu-stop-on-expression (identifier)
  "For CenterLine, send clipc message to stop on IDENTIFIER."
  (interactive)
  (clipc-request-send "cxcmd_watchpoint"
                      (list (list "expression" identifier)
                            '("cxcmdEchoCommand" 0))))

(defun cl-identify-menu-trace (identifier)
  "For CenterLine, send clipc message to trace IDENTIFIER."
  (interactive)
  (clipc-request-send "cxcmd_trace"
                      (list (list "funcName" identifier)
                            '("cxcmdEchoCommand" 0))))

(defun cl-identify-menu-print (identifier)
  "For CenterLine, send clipc message to print IDENTIFIER."
  (interactive)
  (clipc-request-send "cxcmd_print"
                      (list (list "expr" identifier)
                              '("typeDepth" 1)
                              '("cxcmdEchoCommand" 0))))

(defun cl-identify-menu-print-contents (identifier)
  "For CenterLine, send clipc message to print *IDENTIFIER."
  (interactive)
  (clipc-request-send "cxcmd_print"
                      (list (list "expr" (concat "*" identifier))
                              '("typeDepth" 1)
                              '("cxcmdEchoCommand" 0))))

(defun cl-identify-menu-dump-function (identifier)
  "For CenterLine, send clipc message to do 'dump function' on IDENTIFIER."
  (interactive)
  (clipc-request-send "cxcmd_dump"
                      (list '("echoOutput" 1)
                            (list "func" identifier)
                            '("cxcmdEchoCommand" 0))))

(defun cl-identify-menu-dump-variable (identifier)
  "For CenterLine, send clipc message to do 'dump variable' on IDENTIFIER."
  (interactive)
  (clipc-request-send "cxcmd_dump"
                      (list '("echoOutput" 1)
                            (list "text" identifier)
                            '("cxcmdEchoCommand" 0))))

(defun cl-identify-menu-whatis (identifier)
  "For CenterLine, send clipc message to ask whatis IDENTIFIER."
  (interactive)
  (send-clipc-cmd (concat "whatis " identifier)))

(defun cl-identify-menu-whereis (identifier)
  "For CenterLine, send clipc message to ask whereis IDENTIFIER."
  (interactive)
  (send-clipc-cmd (concat "whereis " identifier)))

(defun cl-identify-menu-expand (identifier)
  "For CenterLine, send clipc message to expand IDENTIFIER."
  (interactive)
  (send-clipc-cmd (concat "expand " identifier)))

(defun cl-identify-menu-examine-class (identifier)
  "For CenterLine, send clipc message to examine class IDENTIFIER."
  (interactive)
  (clipc-request-send "cxcmd_browse_class"
                      (list (list "classIdent" identifier)
                            '("isIncludeInherited" 0)
                            '("isShowInheritance" 1)
                            '("cxcmdEchoCommand" 0))))

(defun cl-identify-menu-xref (identifier)
  "For CenterLine, send clipc message to xref IDENTIFIER."
  (interactive)
  (clipc-request-send "cxcmd_xref_query"
                      (list (list "symbolName" identifier)
                            '("cxcmdEchoCommand" 0))))

(defun cl-identify-menu-swap (identifier)
  "For CenterLine, send clipc message to swap IDENTIFIER."
  (interactive)
  (clipc-request-send "cxcmd_swap"
                      (list (list "swapItems"
                                  (list
                                   (list "itemName"
                                         '("nameIs" 1)
                                         (list "name" identifier))))
                            '("cxcmdEchoCommand" 0))))

(defun cl-identify-menu-display (identifier)
  "For CenterLine, send clipc message to display IDENTIFIER."
  (interactive)
  (clipc-request-send "cxcmd_display"
                      (list (list "dispText" identifier)
                            (list "userText" identifier)
                            (list "displayOffset" identifier)
                            '("cxcmdEchoCommand" 0))))

(defun cl-identify-menu-display-contents (identifier)
  "For CenterLine, send clipc message to display contents of structure pointed
to by IDENTIFIER."
  (interactive)
  (setq identifier (concat "*" identifier))
  (clipc-request-send "cxcmd_display"
                      (list (list "dispText" identifier)
                            (list "userText" identifier)
                            (list "displayOffset" identifier)
                            '("cxcmdEchoCommand" 0))))

;; ----------------------------------------------------------------------
     
(add-hook 'cl-source-window-hooks 'cl-default-source-window-hook)

(defun cl-default-source-window-exit-hook ()
  (cl-default-source-window-hook t))

(add-hook 'cl-source-window-exit-hooks 'cl-default-source-window-exit-hook)

;; ----------------------------------------------------------------------
;; Code for menubar.
;; ----------------------------------------------------------------------

(defun add-objectcenter-objectcenter-menubar-menu (&optional remove)
  "Add a menubar menu to the local-map for ObjectCenter objectcenter commands.
With prefix REMOVE, remove this menu from the menubar."
  (interactive "P")
  (if remove
      (local-set-key [menu-bar cl-objectcenter] nil)
    (local-set-key [menu-bar cl-objectcenter]
      (cons "ObjectCenter" (make-sparse-keymap "cl-objectcenter")))
    (local-set-key [menu-bar cl-objectcenter quit]
      '("Quit ObjectCenter..." . cl-objectcenter-quit))
    (local-set-key [menu-bar cl-objectcenter save]
      '("Save ObjectCenter..." . cl-objectcenter-save))
    (local-set-key [menu-bar cl-objectcenter send]
      '("Send Email..." . cl-objectcenter-send))
;;;    (local-set-key [menu-bar cl-objectcenter restart]
;;;      '("Restart Session..." . cl-objectcenter-restart))
    (local-set-key [menu-bar cl-objectcenter options]
      '("ObjectCenter Options..." . cl-objectcenter-options))
    (local-set-key [menu-bar cl-objectcenter about]
      '("About ObjectCenter..." . cl-objectcenter-about))))

;;; for CenterLine-C++
(defun add-objectcenter-centerline-c++-menubar-menu (&optional remove)
  "Add a menubar menu to the local-map for CenterLine-C++ CenterLine-C++ commands.
With prefix REMOVE, remove this menu from the menubar."
  (interactive "P")
  (if remove
      (local-set-key [menu-bar cl-objectcenter] nil)
    (local-set-key [menu-bar cl-objectcenter]
      (cons "CenterLine-C++" (make-sparse-keymap "cl-objectcenter")))
    (local-set-key [menu-bar cl-objectcenter quit]
      '("Quit CenterLine-C++..." . cl-objectcenter-quit))
    (local-set-key [menu-bar cl-objectcenter send]
      '("Send Email..." . cl-objectcenter-send))
    (local-set-key [menu-bar cl-objectcenter about]
      '("About CenterLine-C++..." . cl-centerline-c++-about))))

;;; cl-objectcenter-quit, cl-quit-exit, and cl-quit-undefined were created from
;;; functions in ehelp.el.

(defun cl-quit-undefined ()
  (interactive)
  (error "%s is undefined -- Press %s to exit"
	 (mapconcat 'single-key-description (this-command-keys) " ")
	 (if (eq (key-binding "") 'cl-quit-exit)
	     ""
	   (substitute-command-keys "\\[cl-quit-exit]"))))

(defun cl-quit-exit ()
  (interactive)
  (throw 'exit t))

(defun cl-objectcenter-quit ()
  "Quit ObjectCenter."
  (interactive)
  ;; Make a special keymap for a minibuffer interaction.
  (let ((map (make-keymap)))
    (put 'cl-quit-undefined 'suppress-keymap t)
    (fillarray (car (cdr map)) 'cl-quit-undefined)
    (define-key map (char-to-string meta-prefix-char) (copy-keymap map))
    (define-key map "\C-g" 'cl-quit-exit)
    (define-key map "q" 'self-insert-command)
    (define-key map "p" 'self-insert-command)
    (define-key map "c" 'self-insert-command)
    (define-key map "" 'backward-delete-char)
    (define-key map "" 'exit-minibuffer)
    (let ((really (read-from-minibuffer
                   "Really quit (q), cancel (c), or save session as project (p)? "
                   "q"
                   map)))
      (cond ((string= "q" really)
             (send-clipc-cmd "quit force"))
            ((string= "p" really)
             ;; Query user for project name.
             (let ((project-filename
                    (read-file-name
                     (concat "Save to file: ") ; (" cl-project-filename "): ")
                     ;; The "cond" that follows is based on the idea that if
                     ;; cl-project-filename starts with a "/", then the default
                     ;; directory should be the directory of
                     ;; cl-project-filename and otherwise should be
                     ;; default-directory.
                     ;; Make sure we use the buffer-local
                     ;; version of cl-project-filename.
                     (cond ((string= ""
                                     (save-excursion
                                       (set-buffer cl-current-workspace-buffer)
                                       cl-project-filename))
                            (expand-file-name default-directory))
                           ((equal ?/ (elt
                                       (save-excursion
                                         (set-buffer cl-current-workspace-buffer)
                                         cl-project-filename)
                                       0))
                            (file-name-directory
                             (expand-file-name
                              (save-excursion
                                (set-buffer cl-current-workspace-buffer)
                                cl-project-filename))))
                           (t
                            (expand-file-name default-directory)))
                     (file-name-nondirectory
                      (save-excursion
                        (set-buffer cl-current-workspace-buffer)
                        cl-project-filename))
                     nil
                     (file-name-nondirectory
                      (save-excursion
                        (set-buffer cl-current-workspace-buffer)
                        cl-project-filename)))))
               (setq cl-project-filename filename)
               (send-clipc-cmd (concat "quit project " project-filename))))))))

(defun cl-objectcenter-save (filename)
  "Save ObjectCenter."
  (interactive (list (read-file-name
                      (concat "Save to file: ") ; (" cl-project-filename "): ")
                      ;; The "cond" that follows is based on the idea that if
                      ;; cl-project-filename starts with a "/", then the default
                      ;; directory should be the directory of
                      ;; cl-project-filename and otherwise should be
                      ;; default-directory.
                      ;; Make sure we use the buffer-local
                      ;; version of cl-project-filename.
                      (cond ((string= ""
                                      (save-excursion
                                        (set-buffer cl-current-workspace-buffer)
                                        cl-project-filename))
                             default-directory)
                            ((equal ?/ (elt
                                        (save-excursion
                                          (set-buffer cl-current-workspace-buffer)
                                          cl-project-filename)
                                        0))
                             (file-name-directory
                              (save-excursion
                                (set-buffer cl-current-workspace-buffer)
                                cl-project-filename)))
                            (t
                             default-directory))
                      (file-name-nondirectory
                       (save-excursion
                         (set-buffer cl-current-workspace-buffer)
                         cl-project-filename))
                      nil
                      (file-name-nondirectory
                       (save-excursion
                         (set-buffer cl-current-workspace-buffer)
                         cl-project-filename)))))
  (save-excursion
    (set-buffer cl-current-workspace-buffer)
    ;; Set the buffer-local version of cl-project-filename.
    ;; This will also make cl-project-filename start with a "/".
    (setq cl-project-filename filename))
  (clipc-request-send "cxcmd_save_project"
                      (list (list "projectFile" filename)
                            '("saveAs" 3)
                            '("cxcmdEchoCommand" 0))))

(defvar cl-email-address nil
"Email address for contacting CenterLine Software")

(defun cl-objectcenter-send ()
  "Send email to ObjectCenter support."
  (interactive)
  ;; The "nil" in the next line should be changed to "t" if mail is ever fixed
  ;; by fsf.

  (if (eq centerline-product 'CodeCenter)
      (setq cl-email-address "codecenter-support@centerline.com"))

  (if (eq centerline-product 'ObjectCenter)
      (setq cl-email-address "objectcenter-support@centerline.com"))

  (if (eq centerline-product 'CenterLine-C++)
      (setq cl-email-address "centerline-c++-support@centerline.com"))

  (mail nil cl-email-address))

(defun cl-objectcenter-restart ()
  "Restart session in ObjectCenter - NOT YET IMPLEMENTED."
  (interactive)
  ;;  (send-clipc-cmd "restart")
  )

(defun cl-objectcenter-options ()
  "Edit ObjectCenter options."
  (interactive)
  (clipc-request-send "centerline_options_browser_winctl"
                      '(("browserClass" "options_browser") ("operation" 1)
                        ("cxcmdEchoCommand" 0))))

(defun cl-objectcenter-about ()
  "About ObjectCenter."
  (interactive)
  (save-excursion
    (if (get-buffer " *About ObjectCenter*")
        (set-buffer (get-buffer-create " *About ObjectCenter*"))
      (set-buffer (get-buffer-create " *About ObjectCenter*"))
      (insert "\nThis is the emacs interface to ObjectCenter."))
    (with-electric-help 'ignore (current-buffer) t)))

(defun cl-centerline-c++-about ()
  "About CenterLine-C++."
  (interactive)
  (save-excursion
    (if (get-buffer " *About CenterLine-C++*")
        (set-buffer (get-buffer-create " *About CenterLine-C++*"))
      (set-buffer (get-buffer-create " *About CenterLine-C++*"))
      (insert "\nThis is the emacs interface to CenterLine-C++."))
    (with-electric-help 'ignore (current-buffer) t)))

(defun add-objectcenter-file-menubar-menu (&optional remove)
  "Add a menubar menu to the local-map for CenterLine file commands.
With prefix REMOVE, remove this menu from the menubar."
  (interactive "P")
  (if remove
      (local-set-key [menu-bar cl-file] nil)
    (local-set-key [menu-bar cl-file]
      (cons "File" (make-sparse-keymap "cl-file")))
    (local-set-key [menu-bar cl-file edit]
      '("Edit File" . cl-file-edit))
    (local-set-key [menu-bar cl-file swap]
      '("Swap File" . cl-file-swap))
    (local-set-key [menu-bar cl-file reload]
      '("Reload File" . cl-file-reload))
    (local-set-key [menu-bar cl-file unload]
      '("Unload File" . cl-file-unload))
    (local-set-key [menu-bar cl-file load]
      '("Load File" . cl-file-load))))

(defun cl-file-edit (filename)
  "Run 'edit' on FILE in CenterLine."
  (interactive "fEdit file: ")
  (if (not (equal "" filename))
      (find-file-other-window filename)))

(defun cl-file-swap (filename)
  "Run 'swap' on FILE in ObjectCenter or CodeCenter."
  (interactive "fSwap file: ")
  (if (not (equal "" filename))
      (send-clipc-cmd (concat "swap " filename))))

(defun cl-file-reload (filename)
  "Run 'reload' on FILE in ObjectCenter or CodeCenter."
  (interactive "fReload file: ")
  (if (not (equal "" filename))
      (send-clipc-cmd (concat "reload " filename))))

(defun cl-file-unload (filename)
  "Run 'unload' on FILE in ObjectCenter or CodeCenter."
  (interactive "fUnload file: ")
  (if (not (equal "" filename))
      (send-clipc-cmd (concat "unload " filename))))

(defun cl-file-load (filename)
  "Run 'load' on FILE in ObjectCenter or CodeCenter."
  (interactive "fLoad file: ")
  (if (not (equal "" filename))
      (send-clipc-cmd (concat "load " filename))))

(defun add-objectcenter-session-menubar-menu (&optional remove)
  "Add a menubar menu to the local-map for CenterLine session commands.
With prefix REMOVE, remove this menu from the menubar."
  (interactive "P")
  (if remove
      (local-set-key [menu-bar cl-session] nil)
    (local-set-key [menu-bar cl-session]
      (cons "Session" (make-sparse-keymap "cl-session")))
    (local-set-key [menu-bar cl-session run]
      '("Run" . cl-session-run))
    (local-set-key [menu-bar cl-session show]
      '("Show Unresolved Symbols" . cl-session-show))
    (local-set-key [menu-bar cl-session link]
      '("Link Project" . cl-session-link))
    (local-set-key [menu-bar cl-session build]
      '("Build Project" . cl-session-build))))

(defun cl-session-run ()
  "Execute session run in ObjectCenter or CodeCenter."
  (interactive)
  (send-clipc-cmd "run"))

(defun cl-session-show ()
  "Show unresolved symbols in ObjectCenter or CodeCenter."
  (interactive)
  (send-clipc-cmd "unres"))

(defun cl-session-link ()
  "Link project in ObjectCenter or CodeCenter."
  (interactive)
  (send-clipc-cmd "link"))

(defun cl-session-build ()
  "Build project in ObjectCenter or CodeCenter."
  (interactive)
  (send-clipc-cmd "build"))

(defun add-objectcenter-execution-menubar-menu (&optional remove)
  "Add a menubar menu to the local-map for ObjectCenter or CodeCenter execution
commands.  With prefix REMOVE, remove this menu from the menubar."
  (interactive "P")
  (if remove
      (local-set-key [menu-bar cl-execution] nil)
    (local-set-key [menu-bar cl-execution]
      (cons "Execution" (make-sparse-keymap "cl-execution")))
    (local-set-key [menu-bar cl-execution whereami]
      '("Whereami" . cl-execution-whereami))
    (local-set-key [menu-bar cl-execution down]
      '("Down" . cl-execution-down))
    (local-set-key [menu-bar cl-execution up]
      '("Up" . cl-execution-up))
    (local-set-key [menu-bar cl-execution next]
      '("Next" . cl-execution-next))
    (local-set-key [menu-bar cl-execution step]
      '("Step" . cl-execution-step))
    (local-set-key [menu-bar cl-execution continue]
      '("Continue" . cl-execution-continue))
    (local-set-key [menu-bar cl-execution interrupt]
      '("Interrupt" . cl-execution-interrupt))
    (local-set-key [menu-bar cl-execution run]
      '("Run" . cl-execution-run))))

(defun cl-execution-whereami ()
  "Execute whereami in ObjectCenter or CodeCenter."
  (interactive)
  (send-clipc-cmd "whereami"))

(defun cl-execution-down ()
  "Execute down in ObjectCenter or CodeCenter."
  (interactive)
  (send-clipc-cmd "down"))

(defun cl-execution-up ()
  "Execute up in ObjectCenter or CodeCenter."
  (interactive)
  (send-clipc-cmd "up"))

(defun cl-execution-next ()
  "Execute next in ObjectCenter or CodeCenter."
  (interactive)
  (send-clipc-cmd "next"))

(defun cl-execution-step ()
  "Execute step in ObjectCenter or CodeCenter."
  (interactive)
  (send-clipc-cmd "step"))

(defun cl-execution-continue ()
  "Execute continue in ObjectCenter or CodeCenter."
  (interactive)
  (send-clipc-cmd "cont"))

(defun cl-execution-interrupt ()
  "Execute interrupt in ObjectCenter or CodeCenter."
  (interactive)
  (comint-interrupt-subjob))

(defun cl-execution-run ()
  "Execute run in ObjectCenter or CodeCenter."
  (interactive)
  (send-clipc-cmd "run"))

(defun add-objectcenter-examine-menubar-menu (&optional remove)
  "Add a menubar menu to the local-map for ObjectCenter or CodeCenter examining
commands.  With prefix REMOVE, remove this menu from the menubar."
  (interactive "P")
  (if remove
      (local-set-key [menu-bar cl-examine] nil)
    (local-set-key [menu-bar cl-examine]
      (cons "Examine" (make-sparse-keymap "cl-examine")))
    (local-set-key [menu-bar cl-examine dump]
      '("Dump" . cl-examine-dump))
    (local-set-key [menu-bar cl-examine where]
      '("Where" . cl-examine-where))
    (local-set-key [menu-bar cl-examine whereami]
      '("Whereami" . cl-examine-whereami))
    (local-set-key [menu-bar cl-examine status]
      '("Status" . cl-examine-status))
    (if (and (not cl-start-pdm) (eq centerline-product 'ObjectCenter))
	(local-set-key [menu-bar cl-examine expand] 
		       '("Expand <Selection>" . cl-examine-expand)))
    (local-set-key [menu-bar cl-examine whereis]
      '("Whereis <Selection>" . cl-examine-whereis))
    (local-set-key [menu-bar cl-examine whatis]
      '("Whatis <Selection>" . cl-examine-whatis))
    (local-set-key [menu-bar cl-examine print]
      '("Print <Selection>" . cl-examine-print))
    (local-set-key [menu-bar cl-examine edit]
      '("Edit <Selection>" . cl-examine-edit))
    (local-set-key [menu-bar cl-examine list]
      '("List <Selection>" . cl-examine-list))
    (if (and (not cl-start-pdm) (eq centerline-product 'ObjectCenter))
	(local-set-key [menu-bar cl-examine class]
		       '("Examine Class <Selection>" . cl-examine-class)))
    (if (not cl-start-pdm)
	(progn
          (if (not (eq centerline-product 'CenterLine-C++))
	      (local-set-key [menu-bar cl-examine xref]
			 '("Xref <Selection>" . cl-examine-xref)))
	  (local-set-key [menu-bar cl-examine display]
			 '("Display <Selection>" . cl-examine-display))))))

(defun cl-examine-dump ()
  "Do a dump in ObjectCenter or CodeCenter."
  (interactive)
  (send-clipc-cmd "dump"))

(defun cl-examine-where ()
  "Issue where command in ObjectCenter or CodeCenter."
  (interactive)
  (send-clipc-cmd "where"))

(defun cl-examine-whereami ()
  "Issue whereami command in ObjectCenter or CodeCenter."
  (interactive)
  (send-clipc-cmd "whereami"))

(defun cl-examine-status ()
  "Issue status command in ObjectCenter or CodeCenter."
  (interactive)
  (send-clipc-cmd "status"))

(defun cl-examine-expand ()
  "Issue expand command on selected region of code in ObjectCenter or CodeCenter."
  (interactive)
  (if (not (equal "" (x-get-selection)))
      (send-clipc-cmd (concat "expand " (x-get-selection)))))

(defun cl-examine-whereis ()
  "Issue whereis command on selected item in ObjectCenter or CodeCenter."
  (interactive)
  (let ((selected-text (cl-filter-whitespace (x-get-selection))))
    (if (equal "" selected-text)
        (setq selected-text (cl-filter-whitespace
                             (call-interactively
                              'cl-get-user-selection-at-point))))
    (if (not (equal "" selected-text))
        (send-clipc-cmd (concat "whereis " selected-text)))))

(defun cl-examine-whatis ()
  "Issue whatis command on selected item in ObjectCenter or CodeCenter."
  (interactive)
  (let ((selected-text (cl-filter-whitespace (x-get-selection))))
    (if (equal "" selected-text)
        (setq selected-text (cl-filter-whitespace
                             (call-interactively
                              'cl-get-user-selection-at-point))))
    (if (not (equal "" selected-text))
        (send-clipc-cmd (concat "whatis " selected-text)))))

(defun cl-examine-print ()
  "Issue print command on selected item in ObjectCenter or CodeCenter."
  (interactive)
  (let ((selected-text (cl-filter-whitespace (x-get-selection))))
    (if (equal "" selected-text)
        (setq selected-text (cl-filter-whitespace
                             (call-interactively
                              'cl-get-user-selection-at-point))))
    (if (not (equal "" selected-text))
        (send-clipc-cmd (concat "print " selected-text)))))

(defun cl-examine-edit ()
  "Issue edit command on selected item in ObjectCenter or CodeCenter."
  (interactive)
  (let ((selected-text (cl-filter-whitespace (x-get-selection))))
    (if (equal "" selected-text)
        (setq selected-text (cl-filter-whitespace
                             (call-interactively
                              'cl-get-user-selection-at-point))))
    (if (not (equal "" selected-text))
        (clipc-request-send "cxcmd_edit"
                            (list (list "idName" selected-text)
                                  '("cxcmdEchoCommand" 0))))))

(defun cl-examine-list ()
  "Issue list command on selected item in ObjectCenter or CodeCenter."
  (interactive)
  (let ((selected-text (cl-filter-whitespace (x-get-selection))))
    (if (equal "" selected-text)
        (setq selected-text (cl-filter-whitespace
                             (call-interactively
                              'cl-get-user-selection-at-point))))
    (if (not (equal "" selected-text))
        (clipc-request-send "cxcmd_list"
                            (list (list "idName" selected-text)
                                  '("cxcmdEchoCommand" 0))))))

(defun cl-examine-class ()
  "Issue command to examine selected class in ObjectCenter or CodeCenter."
  (interactive)
  (let ((selected-text (cl-filter-whitespace (x-get-selection))))
    (if (equal "" selected-text)
        (setq selected-text (cl-filter-whitespace
                             (call-interactively
                              'cl-get-user-selection-at-point))))
    (if (not (equal "" selected-text))
        (clipc-request-send "cxcmd_browse_class"
                            (list (list "classIdent" selected-text)
                                  '("isIncludeInherited" 0)
                                  '("isShowInheritance" 1)
                                  '("cxcmdEchoCommand" 0))))))

(defun cl-examine-xref ()
  "Issue command to xref selected item in ObjectCenter or CodeCenter."
  (interactive)
  (let ((selected-text (cl-filter-whitespace (x-get-selection))))
    (if (equal "" selected-text)
        (setq selected-text (cl-filter-whitespace
                             (call-interactively
                              'cl-get-user-selection-at-point))))
    (if (not (equal "" selected-text))
        (clipc-request-send "cxcmd_xref_query"
                            (list (list "symbolName" selected-text)
                                  '("cxcmdEchoCommand" 0))))))

(defun cl-examine-display ()
  "Issue command to display selected item in ObjectCenter or CodeCenter."
  (interactive)
  (let ((selected-text (cl-filter-whitespace (x-get-selection))))
    (if (equal "" selected-text)
        (setq selected-text (cl-filter-whitespace
                             (call-interactively
                              'cl-get-user-selection-at-point))))
    (if (not (equal "" selected-text))
        (clipc-request-send "cxcmd_display"
                            (list (list "dispText" selected-text)
                                  (list "userText" selected-text)
                                  (list "displayOffset" selected-text)
                                  '("cxcmdEchoCommand" 0))))))

;;; ----------------------------------------------------------------------------
;;; Debug menu

;;; This is a bit trickier.  The delete menu item brings up another menu.  That
;;; menu is dynamically modified as breakpoints, actions, etc. are set and
;;; deleted.

;;; The breakpoint and action items bring up menus with a choice of setting them
;;; in a file or a function.  In addition, the action one puts the user into a
;;; buffer with a special mode in which the user can insert commands defining
;;; the action.
;;; ----------------------------------------------------------------------------

(defun add-objectcenter-debug-menubar-menu (&optional remove)
  "Add a menubar menu to the local-map for ObjectCenter or CodeCenter debugging
commands.  With prefix REMOVE, remove this menu from the menubar."
  (interactive "P")
  (if remove
      (local-set-key [menu-bar cl-debug] nil)
    (local-set-key [menu-bar cl-debug]
      (cons "Debug" (make-sparse-keymap "cl-debug")))
    (local-set-key [menu-bar cl-debug delete]
      (cons "Delete..." (make-sparse-keymap "cl-delete")))
    (local-set-key [menu-bar cl-debug delete none]
      '("No Debugging Items" . cl-debug-delete-none))
    (if (not (eq centerline-product 'CenterLine-C++))
     (local-set-key [menu-bar cl-debug set-tracepoint]
      (cons "Set Tracepoint..." (make-sparse-keymap "cl-tracepoint"))))
    (if (not (eq centerline-product 'CenterLine-C++))
     (local-set-key [menu-bar cl-debug set-tracepoint function]
      '("in Function" . cl-debug-set-tracepoint-in-function)))
    (if (not (eq centerline-product 'CenterLine-C++))
     (local-set-key [menu-bar cl-debug set-tracepoint everywhere]
      '("Everywhere" . cl-debug-set-tracepoint-everywhere)))
    (if (not (eq centerline-product 'CenterLine-C++))
     (local-set-key [menu-bar cl-debug set-action]
      (cons "Set Action..." (make-sparse-keymap "cl-action"))))
    (if (not (eq centerline-product 'CenterLine-C++))
     (local-set-key [menu-bar cl-debug set-action function]
      '("Set Action In Function..." . cl-debug-set-function-action)))
    (if (not (eq centerline-product 'CenterLine-C++))
     (local-set-key [menu-bar cl-debug set-action file]
      '("Set Action In File..." . cl-debug-set-file-action)))
    (local-set-key [menu-bar cl-debug set-breakpoint]
      (cons "Set Breakpoint..." (make-sparse-keymap "cl-breakpoint")))
    (local-set-key [menu-bar cl-debug set-breakpoint function]
      '("Set Breakpoint In Function..." . cl-debug-set-function-breakpoint))
    (local-set-key [menu-bar cl-debug set-breakpoint file]
      '("Set Breakpoint In File..." . cl-debug-set-file-breakpoint))
    (local-set-key [menu-bar cl-debug stop-in-main]
      '("Stop in main()" . cl-debug-stop-in-main))))

(defun cl-debug-delete-none ()
  "Callback for 'No Debugging Items' item of 'Delete item of 'Debug menu.
Does nothing."
  (interactive))

(defun cl-debug-set-tracepoint-in-function (function-name)
  "Set a tracepoint in a function in ObjectCenter or CodeCenter."
  (interactive "sSelect function: ")
  (if (not (equal "" function-name))
      (clipc-request-send "cxcmd_trace"
                          (list (list "funcName" function-name)
                                '("cxcmdEchoCommand" 0)))))

(defun cl-debug-set-tracepoint-everywhere (function-name)
  "Set a tracepoint everywhere in ObjectCenter or CodeCenter."
  (interactive)
  (clipc-request-send "cxcmd_trace"
                      '(("everywhere" 1)
                        ("cxcmdEchoCommand" 0))))

;;; Stuff for setting actions.
;;; We define some global variables in which to hold data defining the action.
;;; This is because the info won't be sent to clms until after the callback for
;;; the menu item (either cl-debug-set-file-action or
;;; cl-debug-set-function-action) has returned.  Those callbacks put the user in
;;; a buffer to define the action body.  The user ends the session by issuing
;;; the appropriate key (cl-action-defined-key).  This initiates the command
;;; cl-set-action which gives the info defining the action to clms.

(defvar cl-action-buffer-name "*cl-actions*"
  "Name of CenterLine buffer holding latest action defined by the user.")

(defvar cl-action-function-name ""
  "For defining an action in ObjectCenter or CodeCenter, the name of the
function for which the action is intended.")

(defvar cl-action-file-name ""
  "For defining an action in ObjectCenter or CodeCenter, the name of the file
for which the action is intended.")

(defvar cl-action-line-number 1
  "For defining an action in ObjectCenter or CodeCenter, the line-number of the
file for which the action is intended.")

(defvar cl-action-type "function"
  "In ObjectCenter or CodeCenter, toggle indicating whether current action being
defined is for a function or a file.")

(defvar cl-action-defined-key "\C-c\C-c"
  "For CenterLine, string representing key used to indicate that user has
finished defining action in cl action buffer.")

(defvar cl-saved-local-binding nil "")

(defun cl-action-mode ()
  "A minor mode for CenterLine.  It sets up some key-bindings specific for
the cl action buffer."
  ;; for now just do this kludge:
  (setq cl-saved-local-binding (local-key-binding cl-action-defined-key))
  (local-set-key cl-action-defined-key 'cl-set-action))

(defvar cl-saved-buffer nil "")

(defun cl-query-user-action ()
  "For CenterLine, query user for action to perform."
  (interactive)
  (setq cl-saved-buffer (window-buffer (selected-window)))
  (switch-to-buffer (get-buffer-create cl-action-buffer-name))
  (cl-action-mode)     ; a minor mode that adds some special key-bindings.
  (message "Type %s to send off action when done." cl-action-defined-key))

(defun cl-read-action-buffer ()
  "For CenterLine, read from action buffer the user-inserted text and return
action(s) as string enclosed by starting and ending brackets."
  (save-excursion
    (set-buffer (get-buffer cl-action-buffer-name))
    (let ((action (buffer-substring 1 (point-max))))
      (concat "{\n" action "\n}\n"))))

(defun cl-debug-set-file-action (file-name line)
  "Initiate query for user to set an action in FILE at line NUMBER in
ObjectCenter or CodeCenter."
  (interactive "fSelect file: \nnLine number: ")
  (setq cl-action-file-name file-name)
  (setq cl-action-line-number line)
  (setq cl-action-type "file")
  (cl-query-user-action))

(defun cl-debug-set-function-action (function-name)
  "Initiate query for user to set an action in FUNCTION in ObjectCenter or
CodeCenter."
  (interactive "sSelect function: ")
  (if (not (equal "" function-name))
      (progn
        (setq cl-action-function-name function-name)
        (setq cl-action-type "function")
        (cl-query-user-action))))

(defun cl-set-action ()
  "For CenterLine, send clipc message to set an action.  It is assumed that
this was invoked in the appropriate manner."
  (interactive)
  ;; Restore key-binding for cl-action-defined-key.
  (local-set-key cl-action-defined-key cl-saved-local-binding)
  ;; Build and send request to clms.
  ;; Need to read action buffer contents and package them in a clipc request.
  (let ((action (cl-read-action-buffer)) req)
    ;; Save the action commands in a global variable so that we can access it
    ;; when cl-status-record-add gets a clipc event_status notification.
    (setq cl-last-action-body action)
    ;; Set up the text for the request.
    (if (equal cl-action-type "function")
        (setq req (list
                   (list "where"
                         (list '("locType" "funcName")
                               (list "funcName" cl-action-function-name)))
                   '("isAction" 1)
                   (list "actionText" action)
                   '("cxcmdEchoCommand" 0)))
      (setq req (list
                 (list "where"
                       (list '("locType" "fileLoc")
                             (list "fileLoc"
                                   (list (list "pathName" cl-action-file-name)
                                         (list "lineNumber"
                                               cl-action-line-number)))))
                 '("isAction" 1)
                 (list "actionText" action)
                 '("cxcmdEchoCommand" 0))))
    ;; Send the request to clms.
    (clipc-request-send "cxcmd_action" req))
  ;; Restore buffer.
  (bury-buffer (get-buffer cl-action-buffer-name))
  (switch-to-buffer cl-saved-buffer))

(defun cl-debug-set-file-breakpoint (file-name line)
  "Set a breakpoint in FILE at line NUMBER in ObjectCenter or CodeCenter."
  (interactive "fSelect file: \nnLine number: ")
  (clipc-request-send "cxcmd_stop" (list (list "lineNumber" line)
                                         (list "pathName" file-name))))

(defun cl-debug-set-function-breakpoint (function-name)
  "Set a breakpoint in FUNCTION in ObjectCenter or CodeCenter."
  (interactive "sSelect function: ")
  (if (not (equal "" function-name))
      (clipc-request-send "cxcmd_action" (list
                                          '("locType" "funcName")
                                          (list "funcName" function-name)
                                          '("cxcmdEchoCommand" 0)))))

(defun cl-debug-stop-in-main ()
  "Set breakpoint for main in ObjectCenter or CodeCenter."
  (interactive)
  (send-clipc-cmd "stop in main"))

(defun add-objectcenter-windows-menubar-menu (&optional remove)
  "Add a menubar menu to the local-map from which one or another ObjectCenter or
CodeCenter browser may be started.  With prefix REMOVE, remove this menu from
the menubar."
  (interactive "P")
  (if remove
      (local-set-key [menu-bar cl-windows] nil)
    (local-set-key [menu-bar cl-windows]
      (cons "Browsers" (make-sparse-keymap "cl-windows")))
    (local-set-key [menu-bar cl-windows button-panel]
		   '("Button Panel" . cl-windows-button-panel))
    (local-set-key [menu-bar cl-windows run]
		   '("Run Window" . cl-windows-window))
    (local-set-key [menu-bar cl-windows man]
		   '("Manual Browser" . cl-windows-man-browser))
    (if (not (eq centerline-product 'CenterLine-C++))
      (if (not cl-start-pdm)
	(progn
	  (local-set-key [menu-bar cl-windows options]
			 '("Options Browser" . cl-windows-options-browser))
	  (local-set-key [menu-bar cl-windows cross-reference]
			 '("Cross-reference Browser" .
			   cl-windows-cross-reference-browser)))))
    (if (not (eq centerline-product 'CenterLine-C++))
      (if (and (not cl-start-pdm) (eq centerline-product 'ObjectCenter))
	(local-set-key [menu-bar cl-windows inheritance]
		       '("Inheritance Browser" . cl-windows-inheritance-browser))))
    (local-set-key [menu-bar cl-windows data]
		   '("Data Browser" . cl-windows-data-browser))
    (local-set-key [menu-bar cl-windows error]
		   '("Error Browser" . cl-windows-error-browser))
    (if (not (eq centerline-product 'CenterLine-C++))
      (if (not cl-start-pdm)
	(local-set-key [menu-bar cl-windows project]
		       '("Project Browser" . cl-windows-project-browser))))))

(defun cl-windows-button-panel ()
  "Map the GUI's Button Panel."
  (interactive)
  (clipc-request-send "display_button_panel" nil))

(defun cl-windows-project-browser ()
  "Run the ObjectCenter or CodeCenter project browser."
  (interactive)
  (clipc-request-send "centerline_project_browser_winctl"
                      '(("browserClass" "project_browser") ("operation" 1)
                        ("cxcmdEchoCommand" 0)))
  (message "Launching the Project Browser..."))

(defun cl-windows-error-browser ()
  "Run the ObjectCenter or CodeCenter error browser."
  (interactive)
  (clipc-request-send "centerline_error_browser_winctl"
                      '(("browserClass" "error_browser") ("operation" 1)
                       ("cxcmdEchoCommand" 0)))
  (clipc-request-send "cxcmd_cwd"
                      '(("cxcmdEchoCommand" 0)))
  (message "Launching the Error Browser..."))

(defun cl-windows-data-browser ()
  "Run the ObjectCenter or CodeCenter data browser."
  (interactive)
  (clipc-request-send "centerline_data_browser_winctl"
                      '(("browserClass" "data_browser") ("operation" 1)
                        ("cxcmdEchoCommand" 0)))
  (clipc-request-send "cxcmd_enable_display"
                      '(("cxcmdEchoCommand" 0)))
  (message "Launching the Data Browser..."))

(defun cl-windows-inheritance-browser ()
  "Run the ObjectCenter inheritance browser."
  (interactive)
  (clipc-request-send "centerline_class_browser_winctl"
                      '(("browserClass" "class_browser") ("operation" 1)
                        ("cxcmdEchoCommand" 0)))
  (message "Launching the Inheritance Browser..."))

(defun cl-windows-cross-reference-browser ()
  "Run the ObjectCenter or CodeCenter cross-reference browser."
  (interactive)
  (clipc-request-send "centerline_xref_browser_winctl"
                      '(("browserClass" "xref_browser") ("operation" 1)
                        ("cxcmdEchoCommand" 0)))
  (message "Launching the Cross Reference Browser..."))

(defun cl-windows-options-browser ()
  "Run the ObjectCenter options browser."
  (interactive)
  (clipc-request-send "centerline_options_browser_winctl"
                      '(("browserClass" "options_browser") ("operation" 1)
                        ("cxcmdEchoCommand" 0)))
  (message "Launching the Options Browser..."))

(defun cl-windows-man-browser ()
  "Run the ObjectCenter or CodeCenter Manual browser."
  (interactive)
  (clipc-request-send "centerline_man_browser_winctl"
                      '(("browserClass" "man_browser") ("operation" 1)
                        ("cxcmdEchoCommand" 0)))
  (message "Launching the Manual Browser..."))

(defun cl-windows-window ()
  "Run the ObjectCenter or CodeCenter window."
  (interactive)
  (clipc-request-send "centerline_run_window_winctl"
                      '(("browserClass" "run_window") ("operation" 1)
                        ("cxcmdEchoCommand" 0))))

;;; The next function adds menus to the menu bar.
;;; The menus need to be listed in order from right to left.

(defun cl-add-objectcenter-menus (&optional remove)
  "Add menus for CenterLine to local keymap.  If optional arg REMOVE is non-nil,
remove CenterLine definitions from local map."
  (interactive)
  (if remove
      (cl-remove-objectcenter-menus)
    (add-objectcenter-windows-menubar-menu)
    (add-objectcenter-debug-menubar-menu)
    (add-objectcenter-examine-menubar-menu)
    (add-objectcenter-execution-menubar-menu)
    (if (not (eq centerline-product 'CenterLine-C++))
        (add-objectcenter-session-menubar-menu))
    (if (not (eq centerline-product 'CenterLine-C++))
        (add-objectcenter-file-menubar-menu))
    (if (not (eq centerline-product 'CenterLine-C++))
        (add-objectcenter-objectcenter-menubar-menu)
        (add-objectcenter-centerline-c++-menubar-menu))))

(defun cl-remove-objectcenter-menus ()
  "Remove menus for CenterLine from local keymap."
  (interactive)
  (add-objectcenter-windows-menubar-menu t)
  (add-objectcenter-debug-menubar-menu t)
  (add-objectcenter-examine-menubar-menu t)
  (add-objectcenter-execution-menubar-menu t)
  (if (not (eq centerline-product 'CenterLine-C++))
      (add-objectcenter-session-menubar-menu t))
  (if (not (eq centerline-product 'CenterLine-C++))
      (add-objectcenter-file-menubar-menu t))
  (if (not (eq centerline-product 'CenterLine-C++))
      (add-objectcenter-objectcenter-menubar-menu t)
      (add-objectcenter-centerline-c++-menubar-menu)))

(defun cl-remove-global-emacs-menus (&optional remove)
  "For CenterLine, changes the local keymap so that certain menus defined in the
global map disappear from the menu-bar.  If optional arg REMOVE is non-nil,
remove CenterLine definitions from local map."
  (interactive)
  (if remove
      (progn
        (local-unset-key [menu-bar completion])
        (local-unset-key [menu-bar input])
        (local-unset-key [menu-bar signals])
        (local-unset-key [menu-bar edit])
        (local-unset-key [menu-bar file])
        (local-unset-key [menu-bar buffer]))
    ;; The completion, input, and signals menus are added to the local keymap by
    ;; comint-mode.  We don't want them and can remove them from the local map
    ;; without affecting other comint-mode buffers because gud-mode makes the
    ;; local keymap be a copy.
    (local-set-key [menu-bar completion] nil)
    (local-set-key [menu-bar input] nil)
    (local-set-key [menu-bar signals] nil)
    ;; The edit, file, and buffer menus are in the global map.  By setting them to
    ;; 'undefined in the local map, we override the definition without affecting
    ;; other buffers.
    (local-set-key [menu-bar edit] 'undefined)
    (local-set-key [menu-bar file] 'undefined)
    (local-set-key [menu-bar buffer] 'undefined)))

(defun cl-modify-menu-bar (&optional remove)
  "For CenterLine, modify the menu-bar for the current buffer to have the
CenterLine set up.  If optional arg REMOVE is non-nil, remove CenterLine
definitions from local map."
  (if remove
      (progn
        (cl-add-objectcenter-menus t)
        (cl-remove-global-emacs-menus t))
    (cl-add-objectcenter-menus)
    (cl-remove-global-emacs-menus)))

;;; cl-send-input is used to make sure that a <CR> sends the text on the last
;;; line of the buffer even if the cursor is somewhere else.

(defun cl-send-input ()
  "Move point to end of file and call comint-send-input."
  (interactive)
  (goto-char (point-max))
  (comint-send-input))

(defvar objectcenter-mode-hook nil
  "List of forms to execute when starting up ObjectCenter in emacs.")

(defun cl-objectcenter-default-bindings ()
  "Default key bindings for ObjectCenter."
  (interactive)
  (local-set-key "\t"       'comint-dynamic-complete)
  (local-set-key "\M-?"     'comint-dynamic-list-completions)
  (local-set-key "\C-c\C-b" 'cl-build)
  (local-set-key "\C-c\C-d" 'cl-display-point)
  (local-set-key "\C-c\C-n" 'cl-next)
  (local-set-key "\C-c\C-r" 'cl-run)
  (local-set-key "\C-c\C-x" 'cl-gen-cmd)
  (local-set-key "\C-m"     'cl-send-input))

(add-hook 'objectcenter-mode-hook 'cl-objectcenter-default-bindings t)

;; (defvar cl-default-objectcenter-command-line "/usr/local/CenterLine/bin/objectcenter"
;;   "The default command-line string for ObjectCenter.")
;; 
;; (defvar cl-default-codecenter-command-line "/usr/local/CenterLine/bin/codecenter"
;;   "The default command-line string for CodeCenter.")

(defun centerline-command-output (inputstr commandstr &rest args)
  "Given INPUTSTR as stdinput, COMMANDSTR is executed with ARGS as
optional string arguments.  Returns stdoutput as a string."
  (let (p1 p2)
    (save-excursion
      (set-buffer (get-buffer-create " *centerline-command output*"))
      (setq p1 (point))
      (insert inputstr)
      (apply (function call-process-region)
	     p1 (setq p2 (point-max))
	     commandstr
	     nil	; Don't delete input.
	     t		; Output goes to current buffer.
	     nil	; Don't redisplay as you go.
	     args)
      (prog1
	  (buffer-substring p2 (1- (point-max)))
	(kill-buffer (current-buffer))))))

(defun centerline-get-install-dir ()
  "..."
  (save-match-data
    (let ((centerline-lisp-dir nil))
      (mapcar (function
	       (lambda (load-path-element)
		 (if (string-match "^\\(.*/CenterLine/\\)lib/lisp/?$" load-path-element)
		     (setq centerline-lisp-dir load-path-element))))
	       load-path)

      (if (null centerline-lisp-dir)
	  (error "There is no CenterLine install directory anywhere \
in the value of load-path!"))
      (substring centerline-lisp-dir (match-beginning 1) (match-end 1)))))
      
  
;;; The defun "objectcenter" is based on the defun "dbx" in gud.el.

(defun objectcenter (command-line)
  "Run ObjectCenter.  The user is prompted for command line to send to start
program.  This allows the specification of a FILE to run ObjectCenter on.
The buffer for the session is named *cl-workspace* if there is no FILE and
*cl-workspace-FILE* if there is.  The directory containing FILE becomes the
initial working directory and source-file directory for your debugger.  If there
is no FILE, these things default to the default-directory of the current-buffer
when 'objectcenter' is issued."
  (interactive
   (list (read-from-minibuffer "Run ObjectCenter like this: "
			       (if (consp gud-centerline-history)
				   (car gud-centerline-history)
				 (concat (centerline-get-install-dir)
					 "bin/objectcenter"))
			       nil nil
			       '(gud-centerline-history . 1))))
  (gud-overload-functions '((gud-massage-args . gud-centerline-massage-args)
			    (gud-marker-filter . gud-centerline-marker-filter)
			    (gud-find-file . gud-centerline-find-file)
			    ))

  ;; If this is a new session, start ObjectCenter and clms; create workspace
  ;; buffer.

  (setq centerline-product 'ObjectCenter)
  (cl-init command-line)

  ;; The gud-def stuff is directly copied from "dbx" in gud.el of emacs19.22.

  (gud-def gud-break  "file \"%d%f\"\nstop at %l"
	   			  "\C-b" "Set breakpoint at current line.")
;;  (gud-def gud-break  "stop at \"%f\":%l"
;;	   			  "\C-b" "Set breakpoint at current line.")
  (gud-def gud-remove "clear %l"  "\C-d" "Remove breakpoint at current line")
  (gud-def gud-step   "step %p"	  "\C-s" "Step one line with display.")
  (gud-def gud-stepi  "stepi %p"  "\C-i" "Step one instruction with display.")
  (gud-def gud-next   "next %p"	  "\C-n" "Step one line (skip functions).")
  (gud-def gud-cont   "cont"	  "\C-r" "Continue with display.")
  (gud-def gud-up     "up %p"	  "<" "Up (numeric arg) stack frames.")
  (gud-def gud-down   "down %p"	  ">" "Down (numeric arg) stack frames.")
  (gud-def gud-print  "print %e"  "\C-p" "Evaluate C expression at point.")

  ;; Modify the menu-bar.
  (cl-modify-menu-bar)

  (setq comint-prompt-regexp  "^[^)]*objectcenter) *")
  (run-hooks 'objectcenter-mode-hook))


(defun codecenter (command-line)
  "Run CodeCenter.  The user is prompted for command line to send to start
program.  This allows the specification of a FILE to run CodeCenter on.
The buffer for the session is named *cl-workspace* if there is no FILE and
*cl-workspace-FILE* if there is.  The directory containing FILE becomes the
initial working directory and source-file directory for your debugger.  If there
is no FILE, these things default to the default-directory of the current-buffer
when 'codecenter' is issued."
  (interactive
   (list (read-from-minibuffer "Run CodeCenter like this: "
			       (if (consp gud-centerline-history)
				   (car gud-centerline-history)
				 (concat (centerline-get-install-dir)
					 "bin/codecenter"))
			       nil nil
			       '(gud-centerline-history . 1))))
  (gud-overload-functions '((gud-massage-args . gud-centerline-massage-args)
			    (gud-marker-filter . gud-centerline-marker-filter)
			    (gud-find-file . gud-centerline-find-file)
			    ))

  ;; If this is a new session, start ObjectCenter and clms; create workspace
  ;; buffer.

  (setq centerline-product 'CodeCenter)
  (cl-init command-line)

  ;; The gud-def stuff is directly copied from "dbx" in gud.el of emacs19.22.

  (gud-def gud-break  "file \"%d%f\"\nstop at %l"
	   			  "\C-b" "Set breakpoint at current line.")
;;  (gud-def gud-break  "stop at \"%f\":%l"
;;	   			  "\C-b" "Set breakpoint at current line.")
  (gud-def gud-remove "clear %l"  "\C-d" "Remove breakpoint at current line")
  (gud-def gud-step   "step %p"	  "\C-s" "Step one line with display.")
  (gud-def gud-stepi  "stepi %p"  "\C-i" "Step one instruction with display.")
  (gud-def gud-next   "next %p"	  "\C-n" "Step one line (skip functions).")
  (gud-def gud-cont   "cont"	  "\C-r" "Continue with display.")
  (gud-def gud-up     "up %p"	  "<" "Up (numeric arg) stack frames.")
  (gud-def gud-down   "down %p"	  ">" "Down (numeric arg) stack frames.")
  (gud-def gud-print  "print %e"  "\C-p" "Evaluate C expression at point.")

  ;; Modify the menu-bar.
  (cl-modify-menu-bar)

  (setq comint-prompt-regexp  "^[^)]*objectcenter) *")
  (run-hooks 'objectcenter-mode-hook))

;; for CenterLine-C++
(defun centerline-c++ (command-line)
  "Run CenterLine-C++.  The user is prompted for command line to send to start
program.  This allows the specification of a FILE to run CenterLine-C++ on.
The buffer for the session is named *cl-workspace* if there is no FILE and
*cl-workspace-FILE* if there is.  The directory containing FILE becomes the
initial working directory and source-file directory for your debugger.  If there
is no FILE, these things default to the default-directory of the current-buffer
when 'centerline-c++' is issued."
  (interactive
   (list (read-from-minibuffer "Run CenterLine-C++ like this: "
			       (if (consp gud-centerline-history)
				   (car gud-centerline-history)
				 (concat (centerline-get-install-dir)
					 "bin/centerline-c++"))
			       nil nil
			       '(gud-centerline-history . 1))))
  (gud-overload-functions '((gud-massage-args . gud-centerline-massage-args)
			    (gud-marker-filter . gud-centerline-marker-filter)
			    (gud-find-file . gud-centerline-find-file)
			    ))

  ;; If this is a new session, start CenteLine-C++ and clms; create workspace
  ;; buffer.

  (setq centerline-product 'CenterLine-C++)
  (cl-init command-line)

  ;; The gud-def stuff is directly copied from "dbx" in gud.el of emacs19.22.

  (gud-def gud-break  "file \"%d%f\"\nstop at %l"
	   			  "\C-b" "Set breakpoint at current line.")
;;  (gud-def gud-break  "stop at \"%f\":%l"
;;	   			  "\C-b" "Set breakpoint at current line.")
  (gud-def gud-remove "clear %l"  "\C-d" "Remove breakpoint at current line")
  (gud-def gud-step   "step %p"	  "\C-s" "Step one line with display.")
  (gud-def gud-stepi  "stepi %p"  "\C-i" "Step one instruction with display.")
  (gud-def gud-next   "next %p"	  "\C-n" "Step one line (skip functions).")
  (gud-def gud-cont   "cont"	  "\C-r" "Continue with display.")
  (gud-def gud-up     "up %p"	  "<" "Up (numeric arg) stack frames.")
  (gud-def gud-down   "down %p"	  ">" "Down (numeric arg) stack frames.")
  (gud-def gud-print  "print %e"  "\C-p" "Evaluate C expression at point.")

  ;; Modify the menu-bar.
  (cl-modify-menu-bar)

  (setq comint-prompt-regexp  "^[^)]*objectcenter) *")
  (run-hooks 'objectcenter-mode-hook))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; ATTENTION:
;;
;; I have commented out the below chunk of code because it is not acceptable to
;; simply redefine internal Emacs functions so that they behave differently --
;; especially in a package that is intended for others to use.  This code
;; redefines the functions mouse-drag-region and mouse-yank-at-click, and as a
;; result double- and triple-clicking no longer work (as of Emacs 19.22) and the
;; selection behavior is just plain wrong in several ways (i.e., point is not left
;; at one end of the selected text).
;;
;; Rumor has it that the original author (a contractor who no longer works at
;; CenterLine) was trying to fix a selection bug in a pre-19.22 version of Emacs.
;; Sri Pushpavanam (sridhar@centerline.com) thinks the bug was that dragging over
;; a word selected all but the last character of the word.  This is definitely not
;; a problem as of 19.22.
;;
;; Even if it was a problem at 19.22, this is such a bad way to fix it, I would
;; still have commented out this code.  DO NOT UNCOMMENT THIS CODE WITHOUT TALKING
;; TO FRAN LITTERIO OR ANDY SUDDUTH ABOUT IT.
;; --
;; Fran Litterio (franl)
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; ;;; ----------------------------------------------------------------------------
;; ;;; Make some changes to X selection to make it easier to use.
;; ;;; This is optional.  It is turned off by setting cl-x-selection-enhancement to
;; ;;; nil.
;; ;;; ----------------------------------------------------------------------------
;; 
;; (defvar cl-x-selection-enhancement t
;;   "Determines if CenterLine enhancement to X selection mechanism is executed.
;; Equals t if yes.  It should be set appropriately before loading this file.")
;; 
;; (defun cl-improve-x-selection ()
;;   "A simple enhancement to the mouse stuffing mechanism when running in X."
;;   (if (equal window-system 'x)
;;       (progn
;; 
;; ;;; What follows is a modified version of mouse-drag-region, which is found in
;; ;;; mouse.el in the standard distribution from fsf.  The one in mouse.el only
;; ;;; changes the current region - it does not make it the X selection.  The
;; ;;; changes below also make it the X selection, if the up-mouse event is not at
;; ;;; the same location as the down-mouse one.  If they are the same location, the
;; ;;; location of the cursor is changed instead to this location (which does not
;; ;;; happen in the other case).  A modified version of mouse-yank-at-click
;; ;;; follows.  It has been modified so that if the interprogram-paste-function is
;; ;;; not set, it will insert the X selection at the click without putting it on
;; ;;; the kill-ring.
;; 
;; (defun mouse-drag-region (start-event)
;;   "Set the region to the text that the mouse is dragged over.
;; Highlight the drag area as you move the mouse.
;; This must be bound to a button-down mouse event.
;; In Transient Mark mode, the highlighting remains once you
;; release the mouse button.  Otherwise, it does not."
;;   (interactive "e")
;;   (let* ((start-posn (event-start start-event))
;; 	 (start-point (posn-point start-posn))
;; 	 (start-window (posn-window start-posn))
;; 	 (start-buffer (window-buffer start-window))
;;          (orig-buffer (current-buffer))
;;          (orig-window (selected-window))
;; 	 (start-frame (window-frame start-window))
;; 	 (bounds (window-edges start-window))
;; 	 (top (nth 1 bounds))
;; 	 (bottom (if (window-minibuffer-p start-window)
;; 		     (nth 3 bounds)
;; 		   ;; Don't count the mode line.
;; 		   (1- (nth 3 bounds)))))
;;     (set-buffer start-buffer)
;;     (select-window start-window)
;; ;    (mouse-set-point start-event)
;;     (move-overlay mouse-drag-overlay
;; 		  start-point start-point
;; 		  (window-buffer start-window))
;;     (deactivate-mark)
;;     (let (event end end-point)
;;       (save-excursion
;;         (track-mouse
;;           (while (progn
;;                    (setq event (read-event))
;;                    (or (mouse-movement-p event)
;;                        (eq (car-safe event) 'switch-frame)))
;; 
;;             (if (eq (car-safe event) 'switch-frame)
;;                 nil
;;               (setq end (event-end event)
;;                     end-point (posn-point end))
;; 
;;               (cond
;; 
;;                ;; Ignore switch-frame events.
;;                ((eq (car-safe event) 'switch-frame))
;; 
;;                ;; Are we moving within the original window?
;;                ((and (eq (posn-window end) start-window)
;;                      (integer-or-marker-p end-point))
;;                 (goto-char end-point)
;;                 (move-overlay mouse-drag-overlay
;;                               start-point (1+ (point))))
;;                
;;                ;; Are we moving on a different window on the same frame?
;;                ((and (windowp (posn-window end))
;;                      (eq (window-frame (posn-window end)) start-frame))
;;                 (let ((mouse-row
;;                        (+ (nth 1 (window-edges (posn-window end)))
;;                           (cdr (posn-col-row end)))))
;;                   (cond
;;                    ((< mouse-row top)
;;                     (mouse-scroll-subr
;;                      (- mouse-row top) mouse-drag-overlay start-point))
;;                    ((and (not (eobp))
;;                          (>= mouse-row bottom))
;;                     (mouse-scroll-subr (1+ (- mouse-row bottom))
;;                                        mouse-drag-overlay start-point)))))
;; 
;;                (t
;;                 (let ((mouse-y (cdr (cdr (mouse-position))))
;;                       (menu-bar-lines (or (cdr (assq 'menu-bar-lines
;;                                                      (frame-parameters)))
;;                                           0)))
;; 
;;                   ;; Are we on the menu bar?
;;                   (and (integerp mouse-y) (< mouse-y menu-bar-lines)
;;                        (mouse-scroll-subr (- mouse-y menu-bar-lines)
;;                                           mouse-drag-overlay start-point))))))))
;;         )
;;       (if (and (eq (get (event-basic-type event) 'event-kind) 'mouse-click)
;; 	       (eq (posn-window (event-end event)) start-window)
;; 	       (numberp (posn-point (event-end event))))
;; 	  (let (selection)
;; ;	    (mouse-set-point event)
;; 	    (if (= (posn-point (event-end event)) start-point)
;;                 (progn
;;                   (mouse-set-point start-event)
;;                   (deactivate-mark))
;; 	      (set-mark start-point)
;;               (setq selection (buffer-substring start-point
;;                                                 (1+ (posn-point (event-end event)))))
;; ;             Replace (x-own-selection-internal 'PRIMARY selection)
;; ;             with following three statements for backward compatiblity
;;               (x-set-cut-buffer selection)
;;               (x-set-selection 'CLIPBOARD selection)
;;               (x-set-selection 'PRIMARY selection)
;;               ;; Reset the current buffer.
;;               (set-buffer orig-buffer)
;;               (select-window orig-window)
;;               )))))
;; )
;; 
;; (defun mouse-yank-at-click (click arg)
;;   "Insert the last stretch of killed text at the position clicked on.
;; Prefix arguments are interpreted as with \\[yank]."
;;   (interactive "e\nP")
;;   (mouse-set-point click)
;;   ;; If interprogram-paste-function is not nil, assume that the user wants any
;;   ;; unprocessed X selection to be put on the kill-ring as yank will do.
;;   ;; If it is nil, then if there is an X selection, insert it without doing a
;;   ;; yank.  Otherwise, do a yank.
;;   (if (eq nil interprogram-paste-function)
;;       (let ((x-paste (x-cut-buffer-or-selection-value)))
;;         (if x-paste
;;             (insert x-paste)
;;           (setq this-command 'yank)
;;           (yank arg)))
;;     (setq this-command 'yank)
;;     (yank arg))
;; )
;; 
;; ))
;; )
;; 
;; (if (eq t cl-x-selection-enhancement)
;;     (cl-improve-x-selection))

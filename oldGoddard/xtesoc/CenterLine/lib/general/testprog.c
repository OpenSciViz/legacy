#include <stdio.h>

main() {
    int a;

    printf("Executing program line 6\n");
    printf("Executing program line 7\n");
    {
	int a;

	printf("Executing program line 11\n");
    }
    subprogram();
    printf("Executing program line 14\n");
}

subprogram() {
    int a;

    printf("Executing program line 20\n");
    printf("Executing program line 21\n");
}

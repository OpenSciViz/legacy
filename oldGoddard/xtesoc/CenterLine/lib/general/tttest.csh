# Emit one test command at a time
stty -echo
foreach command ( \
    Debug_Transaction_Type \
    Do_Command_Transaction_Type \
    Set_Trap_Transaction_Type \
    Stop_Debug_Transaction_Type \
    Execute_Debug_Transaction_Type \
    Resume_Debug_Transaction_Type \
    RunUntil_Debug_Transaction_Type \
    Delete_Trap_Transaction_Type \
    Execute_Debug_Transaction_Type \
    StepOver_Debug_Transaction_Type \
    StepOver_Debug_Transaction_Type \
    StepOver_Debug_Transaction_Type \
    StepOver_Debug_Transaction_Type \
    Execute_Debug_Transaction_Type \
    StepOver_Debug_Transaction_Type \
    StepInto_Debug_Transaction_Type \
    StepOut_Debug_Transaction_Type \
    Assign_DebugExpression_Transaction_Type \
    Eval_DebugExpression_Transaction_Type \
    Execute_Debug_Transaction_Type \
    StepOver_Debug_Transaction_Type \
    StepInto_Debug_Transaction_Type \
    Get_DebugContext_Transaction_Type \
    Set_DebugContext_Transaction_Type \
    Get_DebugContext_Transaction_Type \
    Unload_Debug_Transaction_Type \
    Fail \
    ) 
    set x = $<
    echo $command
end



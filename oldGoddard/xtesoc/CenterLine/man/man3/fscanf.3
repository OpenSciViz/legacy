.TH FSCANF 3 "June 1993" "CenterLine-C" "Reference Pages"
.SH NAME
fscanf
.LP
ANSI           Read values from a file.
.SH SYNOPSIS
.LP
.B #include <stdio.h>     /* Required. */
.br
\fBint fscanf(FILE *\fIstream\fB,const char *\fIformat\fB, ...);\fR
.SH DESCRIPTION
.LP
Reads from the file associated with
.I stream
according to the format string
.IR format ,
assigning values from the file into objects pointed to
by subsequent arguments to
.BR fscanf .
.LP
.I format
is an arbitrary sequence of three types of elements:
conversion specifications, whitespace characters, and
non-whitespace characters. A conversion specification causes
characters in the input stream to be read and converted to a
specified type, in the manner described below. In most cases there
should be a corresponding argument to
.B fscanf
that is a pointer to an object of that type. Any whitespace character in
.I format
that is not a
part of a conversion specification causes input to be consumed up to
the next non-whitespace character. A whitespace directive fails if no
whitespace character can be found. Any non-whitespace character in
.I format
that is not a part of a conversion specification must match the
next character in the input stream. If such a character does not
match the next input character,
.B fscanf
returns.
.LP
In most cases a conversion specification in
.I format
is associated with a subsequent argument to
.B fscanf
The correspondence between
conversion specifications and arguments is simple: the first
conversion specification that expects an argument corresponds to
the first argument following
.IR format ,
the second corresponds to the
second, etc. If the number of actual arguments is less than the
number of arguments specified in
.IR format ,
.B fscanf
traipses merrily
through memory assuming that whatever it finds is a pointer to the
type of object required by the specification in format, and assigning
into that object. If the number of arguments is greater than the
number specified by
.IR format ,
the excess arguments are evaluated by
the standard function call mechanism, but are otherwise ignored.
.LP
Conversion specifications are of the following form:
.LP
.RS
 '%' '*'?  Field_width? Size? C_char
.RE
.LP
where:
.LP
.RS
Field_width -> Digits;
.br
Size        -> 'h' | 'l' | 'L';
.br
C_char      -> 'd' | 'i' | 'o' | 'u' | 'x' | 'X' |
.br
		'f' | 'e' | 'E' | 'g' | 'G' | 'c' |
.br
		's' | 'p' | 'n' | '%' | '[';
.RE
.TP
 '%'
Sets off a conversion specification from ordinary characters.
.TP
 '*'
Causes the next value in the input stream to be
converted, but the assignment of the resulting value
is suppressed. All the computation (and concomitant
reading of input) is done, but the result is ignored.
No argument corresponds to a conversion specification containing '*.'
.br
.br
The input value to be converted generally extends from
the next input character to the first character in the
input stream that cannot be a part of that value. Except
for the 'c' and '[' conversions, leading whitespace is
skipped. For example, if format specifies that a decimal
integer is to be found in the input stream, first all
whitespace is skipped, then all characters that can be
part of a decimal integer (all Digits) are read. The
conversion ends with the first character that cannot be
part of a decimal integer (including whitespace
characters), and that character remains unread,
available as the first character of the next input
value. Field_width can be used to impose a maximum on
the number of characters that make up an input value.
.br
.br
It is possible for an input value to consist of zero
characters for some conversions; for instance, a decimal
integer is expected and the next character in the input is
 'k'. In this case, no value is assigned, the conflicting
character remains in the input stream, and fscanf returns.
.TP
Size
Specifies the size of the receiving object for an
argument that points to a type that comes in more than
one size; for example, int versus long int. Size is
mentioned below for each conversion it affects. If Size
is specified for a conversion that it cannot affect, it is ignored.
.TP
C_char
Specifies the type of value expected in the input
stream (implicitly specifying the type of the object
pointed to by the corresponding argument).
.LP
The meanings of the conversion characters follow. The Regular Expressions
section of the CenterLine-C Programmer's Guide and Reference
explains the notation used to explain the
meanings of the conversion characters.
.TP
 'd'
A decimal integer of the form:
.br
.br
	('+' | '-')? Digits
.br
.br   
is expected. The corresponding argument A should be a
pointer to int. The 'h' size specification means A
is a pointer to short int and 'l' means it is a pointer
to long int. If the next character in the input following
the sign (if any) is not a decimal digit, no value is
assigned and the conflicting character remains in the
input stream.
.TP
 'i'
An integer of the form:
.br
.br
	('+' | '-')? ('0' ('x' | 'X')? )?Hexdigits
.br
.br
is expected. The corresponding argument A should be a
pointer to int. Following the optional sign, if the
input begins with 0x or 0X, the value is taken to be
a hexadecimal number; otherwise, if the input begins
with '0', the value is taken to be an octal number;
otherwise, it is taken to be a decimal number. The
 'h' size specification means A is a pointer to short
int and 'l' means it is a pointer to long int. Following
the sign (if any) and the prefix (0, 0x, or 0X) if
present, if the next character in the input is not a
digit of the appropriate base, no value is assigned
and the conflicting character remains in the input
stream.
.TP
 'o'
An octal integer of the form:
.br
.br
	Odigits1
.br
.br
is expected. The corresponding argument A should be a
pointer to int. The 'h' size specification means A is
a pointer to short int and 'l' means it is a pointer
to long int. If the next character in the input is not
an octal digit, no value is assigned and the conflicting
character remains in the input stream.
.TP
 'u'
An unsigned decimal integer of the form:
.br
.br
	Digits
.br
.br
is expected. The corresponding argument A should be
a pointer to unsigned int. The 'h' size specification
means A is a pointer to unsigned short int and 'l'
means it is a pointer to unsigned long int. If the next
character in the input is not a decimal digit, no value
is assigned and the conflicting character remains in
the input stream.
.TP
 'x', 'X'
A hexadecimal integer of the form:
.br
.br
	Hexdigitsb
.br
.br
is expected. The corresponding argument A should be a
pointer to int. The 'h' size specification means A
is a pointer to short int and 'l' means it is a
pointer to long int. If the next character in the input
is not a hexadecimal digit, no value is assigned and
the conflicting character remains in the input stream.
.TP
 'f', 'e', 'E', 'g', 'G'
A floating-point number of the form:
.br
.br
	Sign? ((Digits ('.') Digits) | ('.' Digits))
	(('E'|'e') Sign? Digits)?
.br
.br
is expected. The corresponding argument A should be a
pointer to float. If the 'l' size specification is used,
A should be pointer to double. If the 'L' size
specification is used, A should be a pointer to long double.
If a conflicting character appears in the input before
a floating point value is found, no value is assigned and
the conflicting character remains in the input stream.
.TP
 'c'
One character is expected. The corresponding argument A
should be a pointer to char. Whitespace is not skipped
in this case (to read the next non-whitespace character,
use %1s). If a Field_width is given, Field_width characters
are read; A should point to a character array large enough
to hold them. No NUL is appended.
.TP
 's'
A character string is expected. The corresponding argument
should be a pointer to a character array large enough to
accept the string and a NUL that is automatically appended
to the result. The string is terminated in the input by
any whitespace character. It is advisable to use Field_width
with this specification, as otherwise the array may be
overrun for some input. Field_width does not include the
automatically appended NUL.
.TP
 'p'
A pointer is expected. The corresponding argument should
be a pointer to a pointer to void. See the SYSTEM
DEPENDENCIES section of this manual page, below.
.TP
 'n'
The argument is a pointer to int into which is written the
number of characters read thus far by the current call to
fscanf. No input is consumed.
.TP
 '%'
A % is expected. No assignment occurs.
.TP
 '['
A string that is not delimited by whitespace is expected.
The corresponding argument A should be a pointer to a
character array. The left bracket is followed by a sequence
of characters, then a right bracket. If the first character
in the sequence is not a caret (^), input characters are
assigned to the array pointed to by A as long as they are
in the sequence. If the first character is a caret,
characters are assigned until a character from the sequence
is encountered. A NUL is appended to the string. It is
advisable to use Field_width with this specification, as
otherwise the array may be overrun for some input. If the
first character in the sequence is a right bracket (']'),
fscanf will return only a string that begins with a right
bracket.
.LP
If conversion terminates on a conflicting input character, that
character is left unread in the input. Trailing whitespace is left
unread unless matched in the format string.
.LP
If a conversion specification is invalid, that is, if the conversion
character is not one of those listed above,
.B errno
is set and that
conversion is ignored--no input is read and no argument is assigned
into.
.LP
If the arguments passed do not match those specified by format, the
behavior is undefined.
.LP
.B fscanf
returns the number of input values assigned, or
.B EOF
if end-of file is encountered before the first conflict or conversion. If no
conflict occurs,
.B fscanf
returns when the end of the format string is encountered.
.SH CAUTIONS
.LP
If fewer arguments are passed to
.B fscanf
than specified by
.I format,
the behavior is undefined.
.LP
.B fscanf
interprets each argument that follows
.I format
as a pointer to
the type specified by the corresponding conversion specification. If
an argument is not the expected pointer, it is nonetheless treated as
if it were, potentially causing some arbitrary place in memory to be
overwritten with the result of the conversion. If the incorrect
argument is not the same size as a pointer, subsequent arguments
may be misinterpreted as well.
.LP
In most systems, the pointer is expected to be a hexadecimal number.
.SH SYSTEM DEPENDENCIES
.LP
For a
.B %p
conversion, a pointer is expected. The corresponding argument
.I A
should be a pointer to a pointer to void. The address
read is assigned into the pointer pointed to by
.IR A .
This conversion is
only guaranteed to work on a value that is output from one of the
printf function's conversions of a pointer (also
.BR %p )
during the same run of a program.
.SH EXAMPLE
.LP
.RS
.nf
#include <stdio.h>
.br
#define  FAILURE  (-1)
.br
.br
main() {
.br
FILE *FP;
.br
char  s1[11], s2[19] = "but missed eleven.";
.br
int   i, j;
.br
.br
if ((FP = fopen("test.dat", "r")) == NULL) {
.br
	perror("Cannot open test.dat.");
.br
	return FAILURE;
.br
	}
.br
fscanf(FP,"%11c%d %%%d %[^z]z", s1, &i, &j, s2);
.br
printf("11s%d, %s%s", s1, i,
.br
	j == 11 ? "11, " : "", s2);
.br
}
.fi
.RE
.LP
If the file
.I test.dat
contains:
.LP
.RS
.nf
scanf read 10 %11 and then quit.z and garbage?
.fi
.RE
.LP
This program prints the following to standard output:
.LP
.RS
.nf
scanf read 10, 11, and then quit.
.fi
.RE
.LP
But if
.I test.dat
contains this (that is, 11, not %11):
.LP
.RS
.nf
scanf read 10 11 who cares what is out here
.fi
.RE
.LP
This program prints the following instead:
.LP
.RS
.nf
scanf read 10, but missed eleven.
.fi
.RE
.SH SEE ALSO
.LP
.BR fprintf(3) ,
.BR printf(3) ,
.BR scanf(3) ,
.BR sprintf(3) ,
.BR sscanf(3) ,
.BR vfprintf(3) ,
.BR vprintf(3) ,
.B vsprintf(3)

.TH provabuild 1 " December 1996" "CenterLine" "CenterLine Manual Pages"
.SH NAME
provabuild \- C and C++ error-checking compilation system
.SH SYNOPSIS
.PP
\fBprovabuild \fR [ \fIprovabuild options\fR ] \fIcompile_command\fR [ \fIoptions\fR ] \fIfile\fR ...
.br
\fBprovabuild \fR [ \fIprovabuild options\fR ] \fIlink_command\fR [ \fIoptions\fR ] \fIfile\fR ...
.br
\fBprovabuild\fR [ \fB\-usage\fR | \fB\-help\fR | \fB\-flags\fR | \fB\-man\fR ]
.SH DESCRIPTION
The \fBprovabuild\fR command invokes CenterLine's C and C++ error-checking compilation system.
.PP
Use the \fBprovabuild\fR command to compile or link your
application.  Diagnostic messages will appear in the
AcquaProva GUI if one is running, or on standard
error.
.PP
Compile, link, and run-time diagnostic messages can also be
recorded in results files in the directory specified
by the environment variable PROVA_RESFILE_DIR.  If
PROVA_RESFILE_DIR is a relative path name, then 
the directory used is relative to the directory in
which provabuild is invoked.  In a multi-directory
build, several results file directories might
be created.
Setting the PROVA_SILENT environment variable
will disable any generation of diagnostic messages,
except for those in results files.
.PP
When you invoke the \fBprovabuild\fR command with \fIcompile_command\fR,
it invokes the compiler driver. You can use the full pathname to the
location of the \fIcompile_command\fR. If you use the name of a
compiler command, such as \fBCC\fR or \fBcc\fR, AcquaProva uses the
first command of that name it finds defined in your PATH environment
variable. 
.PP
For example, use
.PP
 \fBprovabuild CC \-c \-g file1.C file2.C\fR
.PP
to use the \fBCC\fR command in your path, and
.PP
 \fBprovabuild /bin/cc \-o test test.c\fR
.PP
to use the \fB/bin/cc\fR compiler.
.PP
If you invoke the \fBprovabuild\fR command with a compiler command and
the \fB\-c\fR option, you must also link your application with the
\fBprovabuild\fR command, for example:
.PP
.nf
  \fBprovabuild CC -o my_exec file1.o file2.o\fR
.fi
.PP
The \fBprovabuild\fR command should also be used with \fBld\fR when
combining instrumented object files into a single object file, for example
.PP
 \fBprovabuild /usr/ccs/bin/ld -r -o ab.o a.o b.o\fR
.PP
In the first
phase, \fBprovabuild\fR translates your C++ code into a new
C++ source module, or your C code into a new C source module. As it translates,
it performs compile-time checks. The translator also emits extra information
about types and other program attributes. This information will be
used by the prelinker and at run time. The translator removes 
definitions of unreferenced types and extern declarations.
.PP
In the second phase, the driver invokes the host compiler to convert
the translated, instrumented program into an object file and adds the
extra information about types and other program attributes emitted in
phase one. 
.PP
In the third phase, the driver invokes AcquaProva prelinker to check
type violations and collect type information. Error messages can be
sent to a GUI, results file, or standard error.
.PP
The system linker then produces an instrumented executable file.
.PP
When you run the instrumented executable, the instrumented code
performs run-time checks.
.PP
The \fBprovabuild\fR command takes arguments ending in
.BR .C ,
.BR .cc ,
.BR .cpp ,
.BR .CPP ,
.BR .cxx ,
or
.B .CXX
to be C++ source files. \fBprovabuild\fR takes 
arguments ending in \fB.c\fR to be C or C++ source files. 
Both
.B \.s
and
.B \.o
files are also accepted by the 
\fBprovabuild\fR command and passed to the assembler and linker.
.PP
.SH OPTIONS
In addition to the options described below,
\fBprovabuild\fR accepts other options after the compiler or linker
command and passes them on to the host
compiler and linker. 
.PP
.TP 10
\fB\-adv \fR(default)
Enable advisory messages.
.TP
\fB\-allc \fR(default)
Perform all checks.
.TP
\fB\-compiler \fIcompiler_name\fR
Preempt automatic detection of the type of host compiler or linker.
See "HOST COMPILERS AND LINKERS" below for more information.
.TP
\fB\-ctc \fR(default)
Perform compile-time checks at the default check level of normal (errors,
warnings, and advisories).
.TP
.B \-ctc=[low|medium|normal|high]
Set the compile time check level to low, medium, normal, or high.  A check
level of low reports only errors; that of medium reports errors and
warnings; that of normal reports errors, warnings, and advisories;
and that of high reports errors, warnings, advisories, and cautions.
.TP
.B \-dd=[on|off]
Turn demand-driven code generation on or off and control generation of
debugging symbols. With \fB\-dd=on\fR (the default setting), generate only
the code that is actually used in the module that is being compiled,
and reduce the number of debugging symbols in the object code,
resulting in smaller code and faster linking.
 
In the case of functions, suppress generation of unused inline
function bodies and unused static functions; generate code for any
definitions that might be used externally, even if they are not used
in the particular module being loaded or compiled.

In the case of classes, generate code only if the class is used in the
file in which it is defined. If only pointers to that class are used
but never indirected, generate a forward reference only.
.TP
.B \-dryrun
Show but do not execute the commands constructed by the compilation driver.
.TP 
.B \-provafile
Before linking, run the command
.B "provafile \-noprovabuilt"
on all of the object files and libraries involved in the link.
This has the effect of identifying all the files not built with
\fBprovabuild\fP.
Run-time checking catches more errors if more object files are compiled
using \fBprovabuild\fP;
you can use this option to identify parts of your program
that were not compiled with \fBprovabuild\fP, but should be.

Lines of output that end with an exclamation mark "!" indicate an
error in your build procedure.
See the \fIprovafile\fR(1) manual page for more information.

Note that the output from the \fB\-provafile\fP option goes to the standard
output stream, not to a GUI or a results file.
.TP
\fB\-force \fR(default)
Run the host compiler on the original source, without adding link-time
or run-time error checking instrumentation,
 in spite of compile-time errors found
by \fBprovabuild\fP.
.TP 
.B \-help
Display a help message.
.TP
\fB\-host \fIflag\fR
Pass the specified \fIflag\fR to the host compiler. Used for
collisions on argument names such as \fB\-usage\fR. Unrecognized
options are passed to the host compiler or linker by default.
.TP
\fB\-ltc \fR(Default)
Enable link time checking.  The default link time checking level is
high.
.TP
.B \-ltc=[none,low,medium,high]
Enable link time checking at the specified level.  A check level of
none disables link time checking and is equivalent to \fB-noltc\fP.
Check levels of low and medium are equivalent and enable the
reporting of Severe ODR violations only.  A check level of
high enables the reporting of both Mild and Severe ODR violations.
.TP
.B \-keeptmp
Do not delete the intermediate source file generated by AcquaProva.
.TP
.B \-man
Show the manual page for the compiler driver.
.TP
.B \-mt
Link in the thread-safe error-checking library. Use this option when
building applications that use threads.  This option is only supported
on the Solaris release of AcquaProva.
.TP
.B \-noadv
Disable the reporting of advisory messages.
.TP
.B \-noctc
Do not perform compile-time checks and disable advisories.
.TP
\fB\-noforce \fR
Abort if \fBprovabuild\fP finds a compile-time error.  Without this
option, whenever \fBprovabuild\fP finds a compile-time error, it will
go ahead and run the host compiler on the original source, without
adding link-time or run-time error checking instrumentation.
.TP
.B \-noltc
Do not perform link-time checks.
.TP
.B \-noretry
Abort compilation when translation phase fails.
.TP
.B \-nortc
Do not perform run-time checks. Modules compiled with \fB\-nortc\fR do
not contain any run-time checking code. Use this option only if you do
not require any run-time checks on specific modules. Use the \fB\-rtc_dormant\fR 
option instead if you wish to instrument a program with run-time
checking code that is activated only when needed.
.TP
.B \-no_suppression_files
Do not read any suppression files unless specifically requested by the
user. 
.TP
.B \-pass
Invoke the host compiler immediately without running the translator.
.TP
\fB\-retry \fR(default)
If the translation phase fails or generates code that is not
acceptable to the host compiler, attempt to compile by passing the
source code directly to the host compiler.
.TP
\fB\-rtc\fR (default)
Instrument source code to enable run-time checking and link in
run-time checking library.
.TP
\fB\-rtc_dormant
Instrument the source code to enable run-time checks but do not issue
run-time diagnostic messages. To generate run-time diagnostics from
programs linked with \fB\-rtc_dormant\fR, set the
PROVA_RUNTIME_CHECKING environment variable. This option is useful if
you wish to distribute an internal or Beta version of a program that
runs relatively fast but contains run-time checking code that can be
activated without recompiling or relinking.
.TP
.BI \-rulelib \ libfile
Perform additional advisory checks defined in the user-defined
advisory library \fIlibfile\fR.
.TP
.BI \-ruledebug \ scriptfile
Generate a debug script in \fIscriptfile\fR to use for debugging a
user-defined advisory library.
.TP
.BI \-suppression_file \ file
Read suppressions from \fIfile\fR instead of from the user's default
suppression file. If the PROVA_SUPPRESSION_FILE environment variable is
set, it overrides this option.
.TP 
.B \-usage
Displays a help message.
.TP
.B \-v
Verbose mode. Causes the name of and arguments for each subprocess to
be printed as it begins to execute. 
.SH "HOST COMPILERS AND LINKERS"
.P
Normally \fBprovabuild\fP attempts to automatically detect what kind of
host compiler or linker it is using, but you can preempt that detection
by using the option
.PP
.nf
	\fB\-compiler \fIcompiler_name\fR
.fi
.PP
The \fIcompiler_name\fPs of supported compilers are shown in the
following list, along with an indication of how to tell whether
you are using each given compiler.
.PP
.TP 10
.B "ncr_cc"
NCR's High Performance C/C++ Compiler release 3.01.
This is normally called \fB/bin/cc\fP.
.TP
.B clcc
The CenterLine C compiler.  The command is \fBclcc\fP.
.TP
.B "clc++"
The CenterLine C++ compiler.  The command is \fBCC\fP.
When run with the \fB\-V\fP option, it prints
nothing.
It normally resides in a directory \fBCenterLine/bin\fP.
.TP
.B Sun_C_v3
The Sun C compiler version 3.  The command is \fBacc\fP on SunOS 4
and \fBcc\fP on Solaris 2.  When run with the \fB\-V\fP option, it prints
nothing.
.TP
.B Sun_C_v4
The Sun C compiler version 4.  The command is \fBacc\fP on SunOS 4
and \fBcc\fP on Solaris 2.  When run with the \fB\-V\fP option, it prints
a line beginning with "cc: SC3.0" or "cc: SC4.0".
.TP
.B Sun_C++_v3
The Sun C++ compiler version 3.  The command is \fBCC\fP.
When run with the \fB\-V\fP option, it prints nothing.
.TP
.B Sun_C++_v4
The Sun C++ compiler version 4.  The command is \fBCC\fP.
When run with the \fB\-V\fP option, it prints
a line beginning with "CC: SC3.0" or "CC: SC4.0".
.TP
.B ld
The link-editor.
The command is \fBld\fP or \fBild\fP.
.SH ENVIRONMENT VARIABLES
.P
The following list shows environment variables used by AcquaProva and their
default settings.
.TP
.B CL_TEMPDIR
The directory in which CenterLine temporary files are stored.  Overrides TMPDIR if set before the compiler is invoked. Otherwise, defaults to \fBTMPDIR\fR.
.TP 
.B TMPDIR
The directory to be used for temporary files if CL_TEMPDIR is
unset. Defaults to \fB/tmp\fR.
.TP
.B PROVA_RUNTIME_CHECKING
Boolean. If set, run-time checking is enabled. If set to 0, run-time
checking is disabled. Use this at run time to override the link-time
\fB\-rtc_dormant\fR option.
.TP
.B PROVA_RUNTIME_CHECK_LEVEL
Set to none, low, medium, or high: 
none suppresses all run-time diagnostic messages;
low suppresses warnings and cautions;
medium  suppresses cautions;
high provides full run-time checking.  
The default is medium. The settings for this variable can be in upper, lower, or mixed case. 
.TP
.B PROVA_RESFILE_DIR	
If the environment variable PROVA_RESFILE_DIR is set, AcquaProva
saves results for compile-time, link-time, and run-time diagnostics
in that directory. 
NOTE: This environment variable is in effect only if the GUI
is not running, or if the environment variable PROVA_NO_GUI is set. 
You may subsequently view the results in the GUI by selecting File->Open
Results. You can also view results by using the following command: 
.nf
   prova -report file 
.fi
where file is the name of a results file.  
You can find the name of the results file by listing the files
saved in the directory pointed to by PROVA_RESFILE_DIR.
.TP
.B PROVA_NO_GUI
If set, then diagnostic messages are not to sent to the GUI,
even if it is running.  Diagnostic messages are sent to
standard error instead, unless PROVA_SILENT is set.
.TP
.B PROVA_SILENT
If set, then diagnostic messages are not sent to the GUI or
to standard error.  This is typically used in conjunction with
PROVA_RESFILE_DIR.
.TP
.B PROVA_SUPPRESSION_FILE
Path to user's  default suppressions file. If unset, defaults to 
\fB.prova.sup\fR in the user's home directory. If set, this environment variable overrides the provabuild and prova -suppression_file option.
.TP
.B PROVA_NO_SUPPRESSION_FILES
Boolean. If set, no suppression files are read automatically: only
suppression files specified with the PROVA_SUPPRESSION_FILE environment
variable or the -suppression_file option, if any, are read.
.TP
.B PROVA_UNSET_VALUE
By default, AcquaProva uses 0xbf as the value of each unset byte, that
is, each byte of uninitialized automatic or dynamic storage. You
can override this default by setting PROVA_UNSET_VALUE. The environment
variable PROVA_UNSET_VALUE is parsed as a C/C++ integer constant
(for example, 0, 17, 0xfa, 0707) and the low-order eight bits are used
as the unset value. (Note: You can also use the builtin function
prova_choose_unset_value())
.SH FILES
.TP
.B .prova.sup
User suppression file. This file is located in the user's home
directory or in the location specified by
PROVA_SUPPRESSION_FILE. Contains suppressions set by the user to
be used by default. 
.TP
.B prova.sup
Directory-specific suppression file for compile-time suppressions. If
the user requests that compile-time suppressions created in the GUI be saved,
AcquaProva saves them in a file called \fBprova.sup\fR in the directory
where the source file resides.
.TP
.B prog.prova.sup
Directory-specific suppression file for link- and run-time
suppressions. If the user requests that link- or run-time suppressions
created in the GUI be saved, AcquaProva saves them in a file called
\fBprog.prova.sup\fR in the directory where the executable program
resides.
.TP
.B prova/<arch>/bin/prova.sup
Platform-specific suppression file. This file is located in a
platform-specific directory. It contains suppressions for spurious
diagnostic messages caused by problems in system libraries.
.TP
.B Prova
Contains instrumented source files.
.TP
.B a.out
executable output file
.PD 0
.TP
.IB file .a
library of object files
.TP
.IB file .c
C or C++ source file
.TP
.IB file .{C|cpp|CPP|cxx|CXX|cc}
C++ source file
.TP
.IB file .i
source file after preprocessing
.TP
.IB file .o
object file
.TP
.IB file .s
assembler source file
.PP
.SH "SEE ALSO"
.BR prova (1),
.BR provafile (1),
.BR provarun (1)
.PP
.br
.I "AcquaProva User's Guide and Reference"

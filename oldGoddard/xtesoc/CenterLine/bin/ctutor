#! /bin/sh
# Copyright (c) 1991, 1995 by CenterLine Software, Inc.
#  C Tutorial Setup Script for CodeCenter
########
#@d.19950731 # This is an age stamp, do not remove.

# Get path to this script.  dir=`dirname $0`, but portable:
dir=`expr \( x"$0" : x'\(/\)$' \) \| \( x"$0" : x'\(.*\)/[^/]*$' \) \| .`
DIR=`cd $dir/.. ; pwd`

PATH=/bin:/usr/bin:/etc:$PATH:/usr/ucb ; export PATH

# clean up the "standard"  autounount point out of the path name if possible,
case $DIR in
 /tmp_mnt/* ) alt_DIR=`echo "$DIR" | sed 's/^\/tmp_mnt\//\//'`
              if [ -f ${alt_DIR}/admin/RUN_ME ]; then DIR=$alt_DIR ; fi ;;
esac


# set ARCH_OS (architecture and os type that we are running on)
ARCH_OS=`${DIR}/admin/platform 2> /dev/null`
status="$?"
case ${status} in
 0 ) # Do nothing
     ;;
 * ) CODECENTER_VERSION="4.0.0"
     ;;
esac

# Set CODECENTER_VERSION, if unset
case ${CODECENTER_VERSION-unset} in
 # find the latest installed version for the determined platform.
 unset | '' ) # determine if Rel. 4 version installed for platform
     if [ -f ${DIR}/c_4.0.0/${ARCH_OS}/bin/cxui ]; then
         TUTOR_DIR="${DIR}/c_4.0.0/tutorial"
     else
          case ${ARCH_OS-unset} in 
           sparc* ) ARCH_OS="sun4-40" ;;
          esac
          CODECENTER_VERSION=`cd $DIR ; /bin/ls c_3.?.?-??.?/${ARCH_OS}/bin/executive 2> /dev/null | tr " \11" "\12" | tail -1`
          CODECENTER_VERSION=`echo ${CODECENTER_VERSION} | awk -F'/' '{ print $1 }' | sed 's/^c_//'`
          case ${CODECENTER_VERSION-unset} in
            unset | '' )
              TUTOR_DIR="${DIR}/c_4.0.0/tutorial"
              ;;
            *          )
              TUTOR_DIR="${DIR}/c_${CODECENTER_VERSION}/common/tutorial/bounce"
              ;;
          esac
     fi
     ;;
 4* )
     TUTOR_DIR="${DIR}/c_4.0.0/tutorial"
     ;;
 *     )
     TUTOR_DIR="${DIR}/c_${CODECENTER_VERSION}/common/tutorial/bounce"
     ;;
esac

# check for correctness of determined location (i.e. are the files available?)
if [ ! -f ${TUTOR_DIR}/Makefile ]; then
   >&2 echo "ERROR: Unable to determine the location of the tutorial files to copy."
   >&2 echo "       The files should be in ${TUTOR_DIR}."
   >&2 echo
   exit 1
fi


# Set up the bounce tutorial
echo
echo "          CodeCenter Tutorial Setup Program"
echo

WORKING_DIR=`pwd`

if [ -d ctutor_dir ];  then
    echo "Directory 'ctutor_dir' exists ...not creating"
    echo 
    cd ctutor_dir
    /bin/rm -rf `find . -type f -print -depth` 2> /dev/null
    if [ "$?" != "0" ]; then
       >&2 echo "ERROR: You are unable to write in the existing ctutor_dir directory."
       >&2 echo "       Aborting tutorial setup."
       >&2 echo
       exit 2
    fi
    cd ..
else
    echo "Creating directory: ctutor_dir"
    echo
    /bin/mkdir ctutor_dir
    if [ $? != 0 ]; then
       >&2 echo "ERROR: Unable create directory: ctutor_dir"
       >&2 echo "       Aborting tutorial setup."
       >&2 echo
       exit 2
    fi
fi

echo "Copying tutorial files into 'ctutor_dir'"
echo

/bin/cp  ${TUTOR_DIR}/* ${WORKING_DIR}/ctutor_dir/.
if [ $? != 0 ]; then
   >&2 echo "ERROR: The process of copying the contents of ${TUTOR_DIR}"
   >&2 echo "       in to ctutor_dir failed. Perhaps you are out of space?"
   >&2 echo "       Please try again."
   >&2 echo
   exit 2
fi

#######
# massage the Makefile to pickup platform specific changes
QUOTE=`echo '"'`
BACKSLASH='\'
SLASH='/'

fixed_DIR=`echo "${DIR}" | eval "/bin/sed 's${SLASH}${BACKSLASH}${SLASH}${SLASH}${BACKSLASH}${BACKSLASH}${BACKSLASH}${SLASH}${SLASH}g'`

# clcc is not available for all supported platforms, see if it is available.
if [ -f ${DIR}/clcc/${ARCH_OS}/bin/clcc ]; then
    massage_SCRIPT="-e \"s/^CCLINE/CC = ${fixed_DIR}\/bin\/clcc/\""
else
    massage_SCRIPT="-e \"s/^CCLINE/CC = \/bin\/cc/\""
fi

# pickup platform specific Makefile changes here
case $ARCH_OS in
 i486-unixware )
    massage_SCRIPT="${massage_SCRIPT} -e \"s/^XLIBSLINE/XLIBS = -lX11 -lsocket -lnsl/\""
    massage_SCRIPT="${massage_SCRIPT} -e \"s/LD_DASH_W//\"" ;;
 * )
    massage_SCRIPT="${massage_SCRIPT} -e \"s/^XLIBSLINE/XLIBS = -lX11/\""
    massage_SCRIPT="${massage_SCRIPT} -e \"s/LD_DASH_W/-w/\"" ;;
esac

/bin/rm -f ${WORKING_DIR}/ctutor_dir/Makefile
eval "/bin/cat ${TUTOR_DIR}/Makefile | /bin/sed $massage_SCRIPT > ${WORKING_DIR}/ctutor_dir/Makefile"

#######
# massage shape.c.dump to pickup correct dereference value
/bin/rm -f ${WORKING_DIR}/ctutor_dir/shape.c.dump

if [ "${ARCH_OS}" = "powerpc-aix" ]; then
    eval "/bin/cat ${TUTOR_DIR}/shape.c.dump | /bin/sed 's/INITVAL/0x30000000/' > ${WORKING_DIR}/ctutor_dir/shape.c.dump"
else
    eval "/bin/cat ${TUTOR_DIR}/shape.c.dump | /bin/sed 's/INITVAL/43/' > ${WORKING_DIR}/ctutor_dir/shape.c.dump"
fi

########
# make certain the user has no problems using the files.
chmod ugo+w ctutor_dir/*

echo 
echo "Listing of files:"
/bin/ls -C ctutor_dir

echo
echo "To change to the tutorial directory, issue the following command:"
echo
echo "     cd ctutor_dir"
echo

exit 0

#ctutor

/*
  File:   advisories.hxx
  Author: Mark Immel
  Date:   04/04/96

  Copyright:

  (c) 1996, 1997 CenterLine Software, Inc., Cambridge, MA. 
  All rights reserved.  Use, duplication, or disclosure is subject to
  restrictions described by license agreements with CenterLine  
  Software, Inc.

  Contents:

  Interface to AcquaProva Advisory subsystem.

  */

#ifndef ADVISORIES_HXX
#define ADVISORIES_HXX

/****************************************************************************
  Compiler Configuration
****************************************************************************/

// Set manifest constants to indicate certain language features of the
// compiler being used.  These constants may be set explicitly before
// this file is included if the configuration done here is incorrect.
//
// The following constants are used:
//
// Compilers
// ---------
//
// Each of the following is non-zero if the indicated compiler is in
// use, and zero otherwise.
// 
// ADV_IS_SUN_CPP_401 
// ADV_IS_SUN_CPP_410
// ADV_IS_SUN_CPP_420
// ADV_IS_MICROSOFT_VISUAL_CPP_40 
//
// Language Features
// -----------------
//
// Each of the following is non-zero if the indicated language feature
// is supported by the compiler, and zero otherwise.
// 
// ADV_ARE_NAMESPACES_SUPPORTED
// ADV_IS_OSTREAM_IN_NAMEPSACE_STD

#ifndef ADV_IS_SUN_CPP_401
  #if defined(__SUNPRO_CC) && __SUNPRO_CC == 0x401
    #define ADV_IS_SUN_CPP_401 1
  #else
    #define ADV_IS_SUN_CPP_401 0
  #endif
#endif

#ifndef ADV_IS_SUN_CPP_410
  #if defined(__SUNPRO_CC) && __SUNPRO_CC == 0x410
    #define ADV_IS_SUN_CPP_410 1
  #else
    #define ADV_IS_SUN_CPP_410 0
  #endif
#endif

#ifndef ADV_IS_SUN_CPP_420
  #if defined(__SUNPRO_CC) && __SUNPRO_CC == 0x420
    #define ADV_IS_SUN_CPP_420 1
  #else
    #define ADV_IS_SUN_CPP_420 0
  #endif
#endif

#ifndef ADV_IS_MICROSOFT_VISUAL_CPP_40
  #if defined(_MSC_VER) && _MSC_VER == 1000
    #define ADV_IS_MICROSOFT_VISUAL_CPP_40 1
  #else
    #define ADV_IS_MICROSOFT_VISUAL_CPP_40 0
  #endif
#endif

#ifndef ADV_IS_GCC_27
  #if defined(__GNUG__) && __GNUG__ == 2 &&  __GNUC_MINOR__ == 7
    #define ADV_IS_GCC_27 1
  #else
    #define ADV_IS_GCC_27 0
  #endif 
#endif

#ifndef ADV_IS_CENTERLINE_CLPP
  #if defined(CENTERLINE_CLPP)
    #define ADV_IS_CENTERLINE_CLPP 1
  #else
    #define ADV_IS_CENTERLINE_CLPP 0
  #endif
#endif 

#ifndef ADV_IS_XLC
  #if TARGET_OS_AIX && !defined(__GNUG__)
    #define ADV_IS_XLC 1
  #else
    #define ADV_IS_XLC 0
  #endif
#endif

#ifndef ADV_ARE_NAMESPACES_SUPPORTED
  #if ADV_IS_SUN_CPP_401 || ADV_IS_SUN_CPP_410 || \
      ADV_IS_SUN_CPP_420 || ADV_IS_GCC_27 || \
      ADV_IS_CENTERLINE_CLPP || ADV_IS_XLC
    #define ADV_ARE_NAMESPACES_SUPPORTED 0
  #elif ADV_IS_MICROSOFT_VISUAL_CPP_40
    #define ADV_ARE_NAMESPACES_SUPPORTED 1
  #else
    #error "You must set ADV_ARE_NAMESPACES_SUPPORTED."
  #endif
#endif

#ifndef ADV_IS_OSTREAM_IN_NAMESPACE_STD
  #if ADV_ARE_NAMESPACES_SUPPORTED
    #if ADV_IS_MICROSOFT_VISUAL_CPP_40
      #define ADV_IS_OSTREAM_IN_NAMESPACE_STD 0
    #else
      #error "You must set ADV_IS_OSTREAM_IN_NAMESPACE_STD." 
    #endif
  #else
    #define ADV_IS_OSTREAM_IN_NAMESPACE_STD 0
  #endif
#endif

/****************************************************************************
  Forward Declarations
****************************************************************************/

#if ADV_IS_OSTREAM_IN_NAMESPACE_STD
namespace std {
#endif
  class ostream;
#if ADV_IS_OSTREAM_IN_NAMESPACE_STD
};
#endif

#if ADV_ARE_NAMESPACES_SUPPORTED
namespace CenterLine {

#if ADV_IS_OSTREAM_IN_NAMESPACE_STD
using std::ostream;
#endif // ADV_IS_OSTREAM_IN_NAMESPACE_STD

#endif // ADV_ARE_NAMESPACES_SUPPORTED

class advArrayType;
class advBlockStatement;
class advBreakStatement;
class advClassStructOrUnionType;
class advConstructor;
class advContinueStatement;
class advDataMember;
class advDoStatement;
class advDiagnostic;
class advEntity;
class advEnumerationConstant;
class advEnumerationType;
class advExpression;
class advExpressionStatement;
class advFloatType;
class advForStatement;
class advFunction;
class advFunctionMember;
class advFunctionType;
class advGotoStatement;
class advHandler;
class advIfStatement;
class advInitializer;
class advIntegerType;
class advLabelStatement;
class advLoopStatement;
class advMacro;
class advMember;
class advNamedEntity;
class advNamedTypedEntity;
class advParameter;
class advPointerToMemberType;
class advPointerType;
class advReferenceType;
class advReturnStatement;
class advStatement;
class advSwitchStatement;
class advTryStatement;
class advType;
class advTypedEntity;
class advTypedefType;
class advVariable;
class advVoidType;
class advWhileStatement;

/****************************************************************************
  Type Definitions
****************************************************************************/

// A simple boolean type.
typedef int advBool;
#define adv_false 0
#define adv_true  1

/****************************************************************************
  Debugging Functions
****************************************************************************/

// Increments the trace counter.  If the trace counter is greater than
// zero trace output is provided on standard error.
extern void advTrace();

// Decrements the trace counter.
extern void advUntrace();

// Returns a version string for the Advisory subsystem in use by the
// AcquaProva that has loaded this rule library.  This version string
// will be different in each release of the Advisory system.
extern const char* advVersion();

/****************************************************************************
  Class advManagedByProva
****************************************************************************/

// Classes derived from advManagedByProva are created, destroyed, and
// copied only by AcquaProva.  Clients cannot create, destroy, copy, or
// take the address of such objects.

class advManagedByProva
{
protected:
  inline advManagedByProva();
  advManagedByProva(const advManagedByProva&);
  advManagedByProva& operator=(const advManagedByProva&);
  virtual inline ~advManagedByProva();
};

/****************************************************************************
  Class advForEachFunction<T>
****************************************************************************/

template <class T>
class advForEachFunction
{
public:
   virtual inline ~advForEachFunction();

   // Sets terminate_loop to adv_true if the loop should terminate.
   // (If iteration should continue, terminate_loop need not be set;
   // it will be initialized to adv_false when this function is called).
   virtual void operator()(const T& t,
			   advBool& terminate_loop) = 0;
};

/****************************************************************************
  Class advCount<T>
****************************************************************************/

template <class T>
class advCount : 
  public advForEachFunction<T>
{
public:
  inline advCount();
  
  // Returns the number of times operator() has been called.
  inline unsigned int Count() const;

  // Bumps the counter.
  virtual inline void operator()(const T&, advBool&);
  
private:
  // The number of times operator() has been called.
  unsigned int count_;
};


template<class T>
inline advCount<T>::advCount() 
  : count_(0)
{
}

template <class T>
inline unsigned int advCount<T>::Count() const
{
  return count_;
}

template <class T>
inline void advCount<T>::operator()(const T&, advBool&)
{
  count_++;
}

/****************************************************************************
  Class advIterator<T, U>        
****************************************************************************/

template <class T, class U>
class advIterator
{
public:
  // Construct an interator, iterating through the elements given by 
  // (enity.*function).
  advIterator(const U& entity,
	      advBool (U::*function)(advForEachFunction<T>&) const);

  // Advance the iterator, returning the value prior to 
  // advancement.  If the iterator has already iterated through all
  // the elements, NULL is returned, but no error occurs.  (Note: Cfront
  // does not allow this function to be called.  Cfront users must use
  // Advance() instead.)
  inline const T* operator++(int);

  // Advance the iterator, returning the value prior to advancement.
  // If the iterator has already iterated through all the elements,
  // NULL is returned, but no error occurs.
  inline const T* Advance();

  // Returns the value the iterator is currently iterating over, or
  // NULL if the iterator has already iterated through all the
  // elements. 
  inline const T* Current() const;  

  // Returns Current().
  inline operator const T*() const;

  // Resets the iterator to the beginning of its iteration.
  inline void Reset();

  inline ~advIterator();

private:
  advIterator(const advIterator& i);            // Not defined.
  advIterator& operator=(const advIterator& i); // Not defined.

  // The array of elements to iterate through.
  const T** ts_;

  // The current element in the array.
  unsigned int elem_;
};

/****************************************************************************
  Class advDiagnostic
****************************************************************************/

class advDiagnostic : public advManagedByProva
{
public:
  virtual ~advDiagnostic();

  // Add a string supplement to the diagnostic message.  The
  // string_supplement will be copied; thus storage for the string
  // passed in remains the reponsibility of the caller.
  virtual advDiagnostic& operator<<(const char string_supplement[]) = 0;
  
  // Add an entity supplement to the diagnostic message.
  virtual advDiagnostic& operator<<(const advEntity& entity_supplement) = 0;
};

/****************************************************************************
  Class advRuleBase
****************************************************************************/

// This class should never be used directly by clients.  Clients
// should always derive rules from advRule<Entity>, defined below.

class advRuleBase
{
public:
  advRuleBase();
  virtual ~advRuleBase();

  // Returns a short tag-name for the rule.  Users can use this
  // tag name to suppress the rule.  The tag name should be formed of
  // uppercase and lowercase letters, and the first letter should be
  // uppercase, e.g., "ANameLikeThis".
  virtual const char* Name() const = 0;

  // Returns a short (one-or-two-sentence) description of the rule.
  virtual const char* Description() const = 0;

  // Returns a category for the rule.  (For example, rules deriving
  // from the XYZ corporate guidelines might have the category "XYZ
  // Rule".
  virtual const char* Category() const = 0;

  // Returns a URL containing documentation about this rule, or NULL
  // if there is no documentation available.
  virtual const char* DocumentationURL() const;

  // Registers the rule.
  void Register();
  
  // Unregisters the rule.
  void Unregister();

  // Returns adv_true if the entity violates the rule, adv_false
  // otherwise.  Adds information to diagnostic to describe the error
  // in detail.
  virtual advBool 
  DoesEntityViolateRule_(const advEntity& entity,
			 advDiagnostic& diagnostic) const = 0;

private:
  // The number of times the rule has been registered.
  unsigned int registration_count_;
};

/****************************************************************************
  Class advRule<Entity>
****************************************************************************/

template <class Entity>
class advRule : public advRuleBase
{
public:
  // Returns adv_true if the entity violates the rule; adv_false
  // otherwise.  Adds information to diagnostic to describe the error
  // in detail.
  virtual advBool 
  DoesEntityViolateRule(const Entity&  entity,
	                advDiagnostic& diagnostic) const = 0;

private:
  virtual advBool
  DoesEntityViolateRule_(const advEntity& entity,
			 advDiagnostic& diagnostic) const;
};

/****************************************************************************
  Class advLocation
****************************************************************************/

class advLocation : virtual public advManagedByProva
{
public:
  // Returns the name of the file or 0 if no filename is available.
  virtual const char* File() const = 0; 

  // Returns the line number within the file, or 0 if no line number
  // is available.
  virtual unsigned long Line() const = 0; 

  // Returns the column number within the line, or 0 if no column
  // number is available.
  virtual unsigned int Column() const = 0;

  // Returns the innermost function within which this location occurs, or
  // NULL if this location does not occur within a function.
  virtual const advFunction* Function() const = 0;

  // Returns the innermost type in which this location occurs, or NULL
  // if this location does not occur within a type.
  virtual const advType* Type() const = 0;
};

/****************************************************************************
  C++ Language Entities
****************************************************************************/

class advEntity : virtual public advManagedByProva
{
public:
  enum EntityKind {
    ek_ArrayType,
    ek_BlockStatement,
    ek_BreakStatement,
    ek_ClassStructOrUnionType,
    ek_Constructor,
    ek_ContinueStatement,
    ek_DataMember,
    ek_DoStatement,
    ek_Entity,
    ek_EnumerationConstant,
    ek_EnumerationType,
    ek_Expression,
    ek_ExpressionStatement,
    ek_FloatType,
    ek_ForStatement,
    ek_Function,
    ek_FunctionMember,
    ek_FunctionType,
    ek_GotoStatement,
    ek_Handler,
    ek_IfStatement,
    ek_Initializer,
    ek_IntegerType,
    ek_LabelStatement,
    ek_LoopStatement,
    ek_Macro,
    ek_Member,
    ek_NamedEntity,
    ek_NamedTypedEntity,
    ek_Parameter,
    ek_PointerToMemberType,
    ek_PointerType,
    ek_ReferenceType,
    ek_ReturnStatement,
    ek_Statement,
    ek_SwitchStatement,
    ek_TryStatement,
    ek_Type,
    ek_TypedEntity,
    ek_TypedefType,
    ek_Variable,
    ek_VoidType,
    ek_WhileStatement,
    ek_Implementation /* Not to be used by end users. */
  };

  // Returns either the location at which the entity was declared or the
  // location at which the entity was defined, or NULL if no location
  // information is not available.
  virtual const advLocation* Location() const = 0;

  // Returns a pointer to the entity as an instance of the type
  // indicated, or NULL if the entity is not of that type.  This
  // routine should never be called by client code directly; instead
  // the macro ADV_ENTITY_AS_TYPE should be used.  This function will
  // be removed in a later version, once RTTI is available
  // with all supported compilers.
  virtual const void* AsType(EntityKind ek) const = 0;

  // Returns a pointer to the entity as an instance of the type
  // indicated, or NULL if the entity is not of that type.  Should be
  // called with the name of a type descended from advEntity, without
  // the "adv" prefix.  For example,
  //   advEntity& e = FunctionReturingReferenceToStatement();
  //   advStatement* s = ADV_ENTITY_AS_TYPE(e, Statement);
  #define ADV_ENTITY_AS_TYPE(entity, type) \
    ((const adv##type*) (entity).AsType(advEntity::ek_##type))

  // Returns adv_true iff the entity is of the indicated type.
  #define ADV_IS_ENTITY_OF_TYPE(entity, type) \
    (ADV_ENTITY_AS_TYPE(entity, type) != 0)

  // Prints a description of the entity on the indicated stream.
  // Often, this will be the name of the entity.
  virtual void Describe(ostream& o) const = 0;

  virtual inline ~advEntity();
};

// Print the entity on the indicated stream, using the
// entity.Describe() function.
inline ostream& operator<<(ostream& o, const advEntity& entity);

class advNamedEntity : virtual public advEntity
{
public:
  // Returns the name of the entity.  Note that some entities which
  // could be named are not; in this case the the value returned will
  // be NULL.
  virtual const char* Name() const = 0;

  virtual inline ~advNamedEntity();
};


class advTypedEntity : virtual public advEntity
{
public:
  // Returns the type of the entity.
  virtual const advType& Type() const = 0;

  virtual inline ~advTypedEntity();
};


class advNamedTypedEntity : virtual public advNamedEntity,
                            virtual public advTypedEntity
{
public:
  virtual inline ~advNamedTypedEntity();
};


class advFunction : virtual public advNamedTypedEntity
{
public:
  enum OperatorKind {
    ok_none,                // Not an operator          
    ok_new,           	    // operator new             
    ok_delete,        	    // operator delete          
    ok_array_new,     	    // Array operator new       
    ok_array_delete,  	    // Array operator delete    
    ok_plus,          	    // operator+                
    ok_minus,         	    // operator-                
    ok_times,         	    // operator*                
    ok_divide,        	    // operator/                
    ok_remainder,     	    // operator%                
    ok_exclusive_or,  	    // operator^                
    ok_address,       	    // operator&                
    ok_or,            	    // operator|                
    ok_complement,    	    // operator~                
    ok_not,           	    // operator!                
    ok_assign,        	    // operator=                
    ok_less_than,           // operator<                
    ok_greater_than,        // operator>                
    ok_plus_assign,         // operator+=               
    ok_minus_assign,        // operator-=               
    ok_times_assign,        // operator*=
    ok_divide_assign,       // operator/=               
    ok_remainder_assign,    // operator%=               
    ok_exclusive_or_assign, // operator^=
    ok_and_assign,          // operator&=
    ok_or_assign,           // operator|=
    ok_shift_left,          // operator<<
    ok_shift_right,         // operator>>
    ok_shift_left_assign,   // operator<<=
    ok_shift_right_assign,  // operator>>=
    ok_equal,               // operator==
    ok_not_equal,           // operator!=
    ok_less_equal,          // operator<=
    ok_greater_equal,       // operator>=
    ok_logical_and,         // operator&&
    ok_logical_or,          // operator||
    ok_increment,           // operator++
    ok_decrement,           // operator--
    ok_comma,               // operator,
    ok_arrow_star,          // operator->*
    ok_arrow,               // operator->
    ok_call,                // operator()
    ok_subscript            // operator[]
  };

  // Returns the return type of the function.  In the case of a
  // constructor or destructor, this will be NULL since constructors
  // and destructors have no return type.
  const advType* ReturnType() const;

  // Calls f with each parameter.  Returns true iff f requested
  // early termination of the iteration.
  virtual advBool
  ForEachParameter(advForEachFunction<advParameter>& f) const = 0;

  // Returns the number of parameters to the function, not including
  // any optional variable arguments.
  virtual unsigned int Parameters() const;

  // Returns adv_true if a definition for the function has been seen;
  // adv_false otherwise.
  virtual advBool IsDefined() const = 0;

  // Returns adv_true if the function has been generated by the
  // compiler rather than written explicitly by a user; adv_false
  // otherwise.
  virtual advBool IsCompilerGenerated() const = 0;

  // Calls f with each function that overloads this one.  Returns adv_true
  // iff f requested early termination of the iteration.
  virtual advBool
  ForEachOverloadingFunction(advForEachFunction<advFunction>& f) const = 0;

  // Returns adv_true if the function is an overloaded operator.
  advBool IsOperator() const;

  // Returns the kind of overloaded operator this function is.
  virtual OperatorKind KindOfOperator() const = 0;

  // Returns adv_true if the function is an operator new; adv_false
  // otherwise. 
  advBool IsOperatorNew() const;

  // Returns adv_true if the function is an operator delete; adv_false
  // otherwise. 
  advBool IsOperatorDelete() const;

  // Returns adv_true if the function is a simple (i.e., operator=) 
  // assignment operator.
  advBool IsAssignmentOperator() const;

  // Returns adv_true iff the function takes a variable number of
  // arugments.
  virtual advBool HasVariableArguments() const = 0;

  virtual ~advFunction();
};


class advVariable : virtual public advNamedTypedEntity
{
};


class advMember : virtual public advNamedEntity
{
public:
  // Returns the class of which this entity is a member.
  virtual const advClassStructOrUnionType& Class() const = 0;

  enum AccessKind { 
    ac_public, 
    ac_protected,
    ac_private
  };
 
  // Returns the access of this member within the class of which 
  // it is a member.
  virtual AccessKind KindOfAccess() const = 0;

  // Returns adv_true if the member has public access within the
  // class of which it is a member; adv_false otherwise.
  advBool IsPublic() const;

  // Returns adv_true if the member has protected access within the
  // class of which it is a member; adv_false otherwise.
  advBool IsProtected() const;

  // Returns adv_true if the member has pruvate access within the
  // class of which it is a member; adv_false otherwise.
  advBool IsPrivate() const;

  // Returns adv_true if the member is static.
  virtual advBool IsStatic() const = 0;
};


class advFunctionMember : virtual public advFunction,
                          virtual public advMember
{
public:
  // Returns adv_true if the function is a constructor; adv_false
  // otherwise.
  virtual advBool IsConstructor() const = 0;
 
  // Returns adv_true if the function is a default constructor, i.e.,
  // a constructor taking no arguments; adv_false otherwise.
  virtual advBool IsDefaultConstructor() const = 0;

  // Returns adv_true if the function is a copy constructor, i.e.,
  // a constructor taking no arguments; adv_false otherwise.
  virtual advBool IsCopyConstructor() const = 0;

  // Returns adv_true if the function is a destructor; adv_false
  // otherwise.
  virtual advBool IsDestructor() const = 0;

  // Returns adv_true if the function is virtual; adv_false otherwise.
  virtual advBool IsVirtual() const = 0;

  // Calls f for each function that is overridden by this function.
  // Returns true iff f requested early termination of the iteration.
  virtual advBool
  ForEachOverriddenFunction(advForEachFunction<advFunctionMember>& f) 
       const = 0;

  // Returns the "this" variable, if the function is defined, or NULL
  // otherwise. 
  virtual const advVariable* ThisVariable() const = 0;

  // Returns the type of "this", or NULL if the function is static.
  virtual const advType* ThisType() const = 0;

  // Provided only to allow a pointer-to-function-member to
  // IsAssignmentOperator().  (This is impossible because advFunction
  // is a *virtual* base class.)
  advBool advFunctionMember::IsAssignmentOperator_() const;
};


class advConstructor : virtual public advFunctionMember
{
public:
  // Calls f for each member or base-class initializer, in the order
  // they were declared.  Returns adv_true iff f requested early
  // termination of the iteration.
  virtual advBool ForEachInitializerInDeclarationOrder
  (advForEachFunction<advInitializer>& f)
    const = 0;

  // Calls f for each member or base-class initializer, in the order
  // they will be executed.  Returns adv_true iff f requested early
  // termination of the iteration.
  virtual advBool ForEachInitializerInExecutionOrder
  (advForEachFunction<advInitializer>& f) const = 0;
};


class advInitializer : virtual public advEntity
{
public:
  enum InitializerKind {
    ik_base_class,
    ik_data_member
  };
  
  // Returns the kind of initializer this is.
  virtual InitializerKind Kind() const = 0;

  // Returns the member initialized, or NULL if this is not a member
  // initializer. 
  virtual const advDataMember* MemberInitialized() const = 0;
  
  // Returns the class initialized, or NULL if this is not a base
  // class initializer.
  virtual const advClassStructOrUnionType* ClassInitialized() const = 0;
};


class advDataMember : virtual public advMember,
		      virtual public advTypedEntity
{
};
  

class advMacro : virtual public advNamedEntity
{
public:
  // Returns adv_true if the macro takes arguments; false otherwise.
  virtual advBool TakesArguments() const = 0;
};


class advStatement : virtual public advEntity
{
};


class advExpressionStatement : virtual public advStatement
{
};


class advGotoStatement : virtual public advStatement
{
public:
  // Returns the label statement to which control will be transferred.
  virtual const advLabelStatement& LabelStatement() const = 0;
};


class advLabelStatement : virtual public advStatement
{
public:
  // Returns the label attached to this statement.
  virtual const char* Label() const = 0;
};


class advReturnStatement : virtual public advStatement
{
};


class advIfStatement : virtual public advStatement
{
public:	
  // Returns the statement in the then-clause of the if, or
  // NULL if there is no such statement.
  virtual const advStatement* ThenStatement() const = 0;

  // Returns the statement in the else-clause of the if, or
  // NULL if there is no such statement.
  virtual const advStatement* ElseStatement() const = 0;
};


class advBlockStatement : virtual public advStatement
{
public:
  // Calls f for each substatement of the block.  Returns true iff f
  // requested early termination of the iteration.
  virtual advBool
  ForEachSubstatement(advForEachFunction<advStatement>& f) const = 0; 
};


class advTryStatement : virtual public advStatement
{
public:
  // Returns the statement tried.
  virtual const advStatement& TriedStatement() const = 0;
  
  // Calls f for each handler for the try block.  Returns true iff f
  // requested early termination of the iteration.
  virtual advBool 
  ForEachHandler(advForEachFunction<advHandler>& f) const = 0;
};


class advLoopStatement : virtual public advStatement
{
public:
  // Returns the statement which will be iterated over throughout the
  // loop.
 virtual const advStatement* IterationStatement() const = 0;
};


class advWhileStatement : virtual public advLoopStatement
{
};


class advDoStatement : virtual public advLoopStatement
{
};


class advForStatement : virtual public advLoopStatement
{
public:
  // Returns the statement which is executed the first time through the
  // loop before the test, or NULL if there is no such statement.
  virtual const advStatement* InitializationStatement() const = 0;
};


class advSwitchStatement : virtual public advStatement
{
public:
  // Calls f for each substatement of the switch.  Returns true iff f
  // requested early termination of the iteration.
  virtual advBool
  ForEachSubstatement(advForEachFunction<advStatement>& f) const = 0; 
};


class advBreakStatement : virtual public advStatement
{
};


class advContinueStatement : virtual public advStatement
{
};


class advType : virtual public advNamedEntity
{
public:
  // Returns adv_true if the type is const-qualified; adv_false
  // otherwise. 
  virtual advBool IsConstQualified() const = 0;

  // Returns adv_true if the type is volatile-qualified; adv_false
  // otherwise. 
  virtual advBool IsVolatileQualified() const = 0;  

  // Returns adv_true if the class is an aggregate, as defined in the
  // forthcoming ANSI/ISO C++ standard, section 8.5.1, i.e.:
  // 
  //   8.5.1  Aggregates                                      [dcl.init.aggr]
  //
  // 1 An  aggregate  is  an array or a class (_class_) with no user-declared
  //   constructors (_class.ctor_), no private or protected  non-static  data
  //   members  (_class.access_),  no  base classes (_class.derived_), and no
  //   virtual functions (_class.virtual_).
  virtual advBool IsAggregate() const;

  // Returns the underlying type for this type once all top-level
  // const and volatile qualifiers are stripped away.
  virtual const advType& WithoutQualifiers() const = 0;

  // Return adv_true iff two types are exactly equivalent.
  virtual advBool operator==(const advType& t) const = 0;

  // Return adv_true iff two types are not exactly equivalent.
  advBool operator!=(const advType& t) const;

  virtual ~advType();
};


class advIntegerType : virtual public advType
{
public:
  enum IntegerKind {
    ik_plain_char,
    ik_signed_char,
    ik_unsigned_char,
    ik_signed_short,
    ik_unsigned_short,
    ik_signed_int,
    ik_unsigned_int,
    ik_signed_long,
    ik_unsigned_long,
    ik_signed_long_long,
    ik_unsigned_long_long
  };

  // Returns the kind of integer type this is.
  virtual IntegerKind KindOfInteger() const = 0;
};


class advFloatType : virtual public advType
{
public:
  enum FloatKind {
    fk_float,
    fk_double,
    fk_long_double
  };

  // Returns the kind of floating-point type this is.
  virtual FloatKind KindOfFloat() const = 0;
};


class advEnumerationType : virtual public advType
{
public:
  // Calls f for each enumeration constant in the enumeration.
  // Returns true iff f requested early termination of the iteration.
  virtual advBool
  ForEachEnumerationConstant(advForEachFunction<advEnumerationConstant>& f) 
       const = 0; 
};


class advReferenceType : virtual public advType
{
public:
  // Returns the type referred to be references of this type.
  virtual const advType& TypeReferredTo() const = 0;
};


class advPointerType : virtual public advType
{
public:
  // Returns the type pointed to by pointers of this type.
  virtual const advType& TypePointedTo() const = 0;
};


class advArrayType : virtual public advType
{
public:
  // Returns the element type of the array.
  virtual const advType& ElementType() const = 0;

  // Returns the number of elements in the array, or 0 if the number
  // of elements has not been specified.  For example, in the case of
  //   extern int array_of_ints[];
  // the type of array_of_ints is an array type, but the number of
  // elements may be unknown.
  virtual unsigned int NumberOfElements() const = 0;

  // Returns adv_true if the number of elements in the array is known;
  // adv_false otherwise.
  advBool IsNumberOfElementsKnown() const;
};


class advTypedefType : virtual public advType
{
public:
  // Returns the type for which this type is another name.
  virtual const advType& NamedType() const = 0;
};


class advVoidType : virtual public advType
{
};


class advFunctionType : virtual public advType
{
public:
  // Returns the return type of the routine, or NULL if the function
  // is a constructor or destructor.
  virtual const advType* ReturnType() const = 0;

  // Calls f for each parameter type of the routine.  Returns true iff
  // f requested early termination of the iteration.
  virtual advBool ForEachParameterType(advForEachFunction<advType>& f)
       const = 0;
};


class advClassStructOrUnionType : virtual public advType
{
public:
  enum ClassStructOrUnionTypeKind {
    csu_class,
    csu_struct,
    csu_union
  };

  // Returns the kind of class, struct, or union type this is.
  virtual ClassStructOrUnionTypeKind 
  KindOfClassStructOrUnionType() const = 0;

  // Returns adv_true if the class is a POD as defined in the 
  // forthcoming ANSI/ISO C++ standard, section 9, i.e.:
  //
  // 4 A  structure is a class defined with the class-key struct; its members
  //   and   base   classes   (_class.derived_)   are   public   by   default
  //   (_class.access_).   A  union  is  a  class  defined with the class-key
  //   union; its members are public by default and it holds  only  one  data
  //   member at a time (_class.union_).  [Note: aggregates of class type are
  //   described in _dcl.init.aggr_.  ] A POD-struct is an aggregate  class
  //   that  has  no  non-static data members of type pointer to member, non-
  //   POD-struct, non-POD-union (or array of such types) or  reference,  and
  //   has  no  user-defined  copy  assignment  operator  and no user-defined
  //   destructor.  Similarly, a POD-union is an aggregate union that has  no
  //   non-static  data  members  of  type pointer to member, non-POD-struct,
  //   non-POD-union (or array of such types) or reference, and has no  user-
  //   defined copy assignment operator and no user-defined destructor.
  virtual advBool IsPOD() const;

  // Returns adv_true if the class is incomplete; adv_false otherwise.
  virtual advBool IsComplete() const = 0;

  // Calls f for each function member.  Note that there will only be
  // function members if IsComplete() returns adv_true, but that no 
  // error will occur if this function is called when IsComplete()
  // returns adv_false.  Returns true iff f requested early
  // termination of the iteration.
  virtual advBool
  ForEachFunctionMember(advForEachFunction<advFunctionMember>& f)
       const = 0;

  // Calls f for each data member.  Note that there will only be
  // function members if IsComplete() returns adv_true, but that no 
  // error will occur if this function is called when IsComplete()
  // returns adv_false.  Returns true iff f requested early
  // termination of the iteration.
  virtual advBool
  ForEachDataMember(advForEachFunction<advDataMember>& f) const = 0;

  // Returns adv_true if the class has a default constructor.  If
  // user_only is adv_true, only user-defined functions will be considered.
  virtual advBool HasDefaultConstructor(advBool user_only = adv_false)    
    const;
  
  // Returns adv_true if the class has a copy constructor.  If
  // user_only is adv_true, only user-defined functions will be
  // considered. 
  virtual advBool HasCopyConstructor(advBool user_only = adv_false)
    const; 

  // Returns adv_true if the class has an assignment operator. If
  // user_only is adv_true, only user-defined functions will be
  // considered.  
  virtual advBool HasAssignmentOperator(advBool user_only = adv_false)
    const; 

  // Returns adv_true if the class has a destructor.  If
  // user_only is adv_true, only user-defined functions will be
  // considered. 
  virtual advBool HasDestructor(advBool user_only = adv_false) const; 

  // Returns the destructor for the class, or NULL if there is none.
  virtual const advFunctionMember* Destructor() const = 0;

  // Returns adv_true if the class has any virtual functions, or if
  // any base class has virtual functions.
  virtual advBool HasVirtualFunction() const;

  struct BaseClassInfo {
    BaseClassInfo(advBool is_virtual, 
		  advBool is_direct, 
		  const advClassStructOrUnionType& base_class)
      : is_virtual_(is_virtual), is_direct_(is_direct), 
	base_class_(base_class) {}

    advBool is_virtual_; // adv_true if the base class is a virtual
			 // base class; adv_false otherwise.
    advBool is_direct_;  // adv_true if the base class is a direct
			 // base class; adv_false otherwise.
    const advClassStructOrUnionType& base_class_; // The base class itself.
  }; 

  // Calls f for each class that is a base class of this class.
  // Returns true iff f requested early termination of the iteration.
  virtual advBool 
  ForEachBaseClass(advForEachFunction<BaseClassInfo>& f) const = 0;

  // Returns adv_true if the class has base classes.
  virtual advBool HasBaseClasses() const;

  virtual ~advClassStructOrUnionType();

private:
  // Returns true if there is a member function f such that f->pm()
  // return true.  If user_only is true, only user-defined functions
  // are considered.
  advBool HasMemberFunction(advBool (advFunctionMember::*pm)() const,
			    advBool user_only) const;
};


class advPointerToMemberType : virtual public advType
{
public:
  // Returns the class type to which the member belongs.
  virtual const advClassStructOrUnionType& ClassType() const = 0;

  // Returns the type of the member pointed to.
  virtual const advType& MemberType() const = 0;
};


class advHandler : virtual public advEntity
{
public:
  // Returns the variable initialized by the catch clause, or NULL if
  // the catch clause is of the form catch (...).
  virtual const advVariable* Parameter() const = 0;

  // Returns the statement executed if an exception is caught
  // by this handler.
  virtual const advStatement& HandlerStatement() const = 0;
};


class advEnumerationConstant : virtual public advNamedEntity
{
public:
  // Returns the value of the enumeration constant.
  virtual long Value() const = 0;
};


class advExpression : virtual public advTypedEntity
{
public:
  // Returns adv_true if expr might have the same value as *this.  (It
  // is provably impossible to compute whether or not two expressions
  // have the same value.)  If this function returns adv_false, it is
  // virtually guaranteed that the two expressions do not have the
  // same value; however, a return value of adv_true does not in any
  // way guarantee that the two expressions do have the same value.
  virtual advBool MaybeEquals(const advExpression& expr) const = 0;
};


class advParameter : virtual public advNamedTypedEntity
{
public:
  // Returns the default argument, if any.
  virtual const advExpression* DefaultValue() const = 0;

  // Returns adv_true iff the parameter has a default argument.
  advBool HasDefaultValue() const;
};

/****************************************************************************
  Inline Functions and Templates
****************************************************************************/

inline advType::~advType()
{
}

inline advNamedEntity::~advNamedEntity()
{
}

inline advEntity::~advEntity()
{
}

inline advFunction::~advFunction()
{
}

inline advClassStructOrUnionType::~advClassStructOrUnionType()
{
}

inline advTypedEntity::~advTypedEntity()
{
}

inline advNamedTypedEntity::~advNamedTypedEntity()
{
}

inline advManagedByProva::advManagedByProva()
{
}

inline advManagedByProva::~advManagedByProva()
{
}

inline advDiagnostic::~advDiagnostic()
{
}

inline advRuleBase::advRuleBase()
  : registration_count_(0)
{
}

inline advRuleBase::~advRuleBase()
{
}

template <class T>
inline advForEachFunction<T>::~advForEachFunction()
{
}


inline ostream& operator<<(ostream& o, const advEntity& e)
{
  e.Describe(o);
  return o;
}


inline advBool advType::operator!=(const advType& t) const
{
  return !operator==(t);
}

#define ADVTYPE(kind)							\
inline advBool								\
advRule<adv##kind>::DoesEntityViolateRule_(const advEntity& entity,	\
					   advDiagnostic& diagnostic)	\
  const									\
{									\
  return ADV_ENTITY_AS_TYPE(entity, kind) ?				\
    DoesEntityViolateRule(*ADV_ENTITY_AS_TYPE(entity, kind),		\
			  diagnostic)					\
    : adv_false;							\
}
#include "advisory_types.hxx"
#undef ADVTYPE


template <class T>
class advIteratorHelper : public advForEachFunction<T>
{
public:
  advIteratorHelper(const T** ts) : ts_(ts), i_(0) {}
  
  virtual void operator()(const T& t, 
			  advBool&) {
    ts_[i_++] = &t;
  }
  
private:
  const T** ts_;
  unsigned int i_;
};


template <class T, class U>
advIterator<T, U>::advIterator(const U& entity,
			       advBool (U::*function)
			       (advForEachFunction<T>&) const)
  : elem_(0)
{
  advCount<T> count;
  (void) (entity.*function)(count);
  ts_ = new const T*[count.Count() + 1];
  
  advIteratorHelper<T> fill(ts_);
  (void) (entity.*function)(fill);
  ts_[count.Count()] = 0;
}


template <class T, class U>
inline const T* advIterator<T, U>::operator++(int)
{
  const T* result = Current();
  if (result) 
    elem_++;
  return result;
}


template <class T, class U>
inline const T* advIterator<T, U>::Advance()
{
  return operator++(0);
}



template <class T, class U>
inline const T* advIterator<T, U>::Current() const
{
  return ts_[elem_];
}


template <class T, class U>
inline advIterator<T, U>::operator const T*() const
{
  return Current();
}


template <class T, class U>
inline void advIterator<T, U>::Reset() 
{
  elem_ = 0;
}


template <class T, class U>
inline advIterator<T, U>::~advIterator()
{
  delete[] ts_;
}


#if ADV_ARE_NAMESPACES_SUPPORTED
};
#endif

/****************************************************************************
  Macro Removal
****************************************************************************/

// Some macros are defined in this header file.  They are
// automatically removed here, so that client code will not 
// suffer namespace pollution.  The following macros are not removed
// because they are part of the interface to the advisory subsytem:
//
// ADV_ENTITY_AS_TYPE
// ADV_IS_ENTITY_OF_TYPE

#ifndef ADV_KEEP_MACRO_NAMES
  #undef ADV_IS_SUN_CPP_401
  #undef ADV_IS_SUN_CPP_410
  #undef ADV_IS_SUN_CPP_420
  #undef ADV_IS_MICROSOFT_VISUAL_CPP_40
  #undef ADV_IS_GCC_27
  #undef ADV_IS_CENTERLINE_CLPP
  #undef ADV_ARE_NAMESPACES_SUPPORTED
  #undef ADV_IS_OSTREAM_IN_NAMESPACE_STD
#endif // ADV_KEEP_MACRO_NAMES

#endif // ADVISORIES_HXX

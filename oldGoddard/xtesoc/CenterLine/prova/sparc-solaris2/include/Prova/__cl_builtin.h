/*
  File:   __cl_builtin.h
  Author: Glenn Allin
  Date:   Unknown
  
  Copyright:
  
  (c) 1996, 1997 CenterLine Software, Inc., Cambridge, MA. 
  All rights reserved.  Use, duplication, or disclosure is subject to
  restrictions described by license agreements with CenterLine  
  Software, Inc.
  
  Contents:
  
  Declarations automatically included by every compilation unit,
  whether or not run-time checking is in use.
  */

/****************************************************************************
  Note
****************************************************************************/

/*
  All preprocessor directives *must* begin with a '#' in the leftmost
  column.  Sun's C compiler, in -Xs mode, uses a cpp that requires
  this.
  */

#ifdef __cplusplus
extern "C" 
{
#endif /* __cplusplus */

/****************************************************************************
  Type and Function Declarations
****************************************************************************/

/* Type parameter and return types of alloca vary considerably,
   especially on SunOS. */
    
#if defined(__PROVA_TARGET_OS_SUNOS4)
  /* Sunos 4 */
# if defined(__PROVA_COMPILER_CLCC) || defined(__PROVA_COMPILER_CLCPP)
    /* A CenterLine compiler */
    /* Alloca returns char* on SunOS4 for clcc and clc++ compilers, and */
    /* takes an int arg for C++. */
    typedef char* __Exp_builtin_alloca_t;
#   if defined(__PROVA_COMPILER_CLCPP)
      /* CenterLine C++ */
      typedef int __Exp_builtin_alloca_arg_t;
#   else /* defined(__PROVA_COMPILER_CLCPP) */
       /* CenterLine C */
       /* __Exp_builtin_alloca_arg_t is not relevant */
#   endif /* defined(__PROVA_COMPILER_CLCPP) */
# elif defined(__PROVA_COMPILER_SUNC) || defined(__PROVA_COMPILER_SUNCPP)
    /* Modern Sunpro C (3.0 or later) or C++ (4.0 or later) */
    typedef unsigned int __Exp_builtin_alloca_arg_t;
    typedef void* __Exp_builtin_alloca_t;
# else /* defined(__PROVA_COMPILER_SUNC) ||
	   defined(__PROVA_COMPILER_SUNCPP) */
    /* By elimination: Older Sunpro C or C++ */
    /* Alloca returns char* and takes an int arg on SunOS4 for */
    /* old sunpro compilers */
    typedef char* __Exp_builtin_alloca_t;
    typedef int __Exp_builtin_alloca_arg_t;
# endif /* defined(__PROVA_COMPILER_SUNC) ||
	    defined(__PROVA_COMPILER_SUNCPP) */
#else /* defined(__PROVA_TARGET_OS_SUNOS4) */
   /* Solaris and other systems. */
   typedef unsigned int __Exp_builtin_alloca_arg_t;
   typedef void* __Exp_builtin_alloca_t;
#endif /* defined(__PROVA_TARGET_OS_SUNOS4) */

/* The __builtin_va_arg_incr() function, used for handling variable
   argument code on many platforms, must be prototyped to avoid
   warnings when it is called. */

#ifdef __cplusplus
  void* __builtin_va_arg_incr(...);
  __Exp_builtin_alloca_t __builtin_alloca(__Exp_builtin_alloca_arg_t);
#else /* __cplusplus */
  void* __builtin_va_arg_incr();
  __Exp_builtin_alloca_t __builtin_alloca();
#endif /* __cplusplus */

/* Some systems make use of a builtin _wchar_t type */

#if !defined(__cplusplus) && defined(__PROVA_TARGET_OS_SUNOS4)
typedef unsigned short _wchar_t;
#endif /* !defined(__cplusplus) && defined(__PROVA_TARGET_OS_SUNOS4) */

#ifdef __hpux__
/* this is the hp vararg hack */
#define va_alist __builtin_va_alist
/* #define __builtin_va_start(a,b) (a) = (va_list)((long *)&va_alist + 1) */
/* this makes the hp vararg variable look like the Sparc one 
   this makes declartor.c work for hp also. */
#endif /*__hpux__*/

#ifdef __PROVA_TARGET_OS_NCR43
#assert machine(i386)
#assert cpu(i386)
#assert system(unix)
#assert machine(_nst)
#endif /* __PROVA_TARGET_OS_NCR43 */

#ifdef __cplusplus
} /* extern "C" */
#endif /* __cplusplus */


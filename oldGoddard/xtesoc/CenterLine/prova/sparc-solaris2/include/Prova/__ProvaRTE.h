
#ifndef __EXPRTE_H
#define __EXPRTE_H 1



#if defined(__cplusplus)
extern "C" {
#endif


 
typedef struct __ExpZtfpe {
  struct ecc_td_tag ** the_type;
  int count;
  int offset;
  char * name; 
  char is_base_class;
  
  int * true_dims;
} __ExpZtfpe;



typedef struct __ExpZel {
  unsigned source_file_name_index;
  int line_number;
  
  char * source_file_name;
  
  
} __ExpZel;



typedef struct __ExpZhfd {
  unsigned declaration_site_index;
  unsigned type_name_index;
  
  struct __ExpZel * declaration_site;
  char * type_name;
} __ExpZhfd;


enum pl_how_strict {
    hs_char = 0,		
    hs_bitwise_copyable_no_virtual = 1,
				
				
    hs_bitwise_copyable_virtual_functions = 2,
				
				
    hs_not_bitwise_copyable = 3	
};


enum pl_other_class_attr {

   oca_empty = 0, 
   oca_has_destructor = 1, 
                  
   
   oca_has_virtual_destructor = 2, 
   oca_all_elements = 3 
};


typedef char how_strict_kind;
typedef char other_class_attr_kind;



typedef unsigned short __ExpTfpb;

struct __ExpZtfp; 


extern void
__ExpUnpa(struct __ExpZtfp *type_tbl,
	   
	   __ExpZtfpe type_elements[],
	   __ExpZhfd handle_for_debugger_tbl[],
	   __ExpZel location_tbl[],
	   char *string_tbl[]);

typedef struct __ExpZtfp {
  char tag;			

  int size;			
  struct ecc_td_tag ** symbol_address; 
  unsigned int sloppy_typehash;	
  __ExpTfpb bits; 
  short int field_count;	
  unsigned int fields_index;	
	    
  

  char future_tag;
  short topsort_counter;
  struct __ExpZhfd * handle;
  struct ecc_td_tag * translated_type;
  struct ecc_td_tag * depends_on_this_type;
  char initialized; 

  struct __ExpZtfpe * fields;

  char is_incomplete;
	 

  char first_field_is_inexact_type;
         

  

  how_strict_kind how_strict;
         

  other_class_attr_kind other_class_attr;

  struct __ExpZtfp * next;

} __ExpZtfp;

enum __ExpZtdt {
  ECC_TD_PRELINKER_INFO=0,
  ECC_TD_STRUCT=1,
  ECC_TD_INT_ARRAY=2,    
  ECC_TD_STRUCT_ARRAY=3, 

  ECC_TD_UNION_CONTENTS=4, 
  ECC_TD_EXT_ARRAY=5,
  ECC_TD_UNTYPED=6,
  ECC_TD_PADDING=7,
  ECC_TD_DEPENDENT_TYPE=8,


  ECC_TD_OVERLAY=9,      

  

  ECC_TD_STRUCT_UNION=10, 
  ECC_TD_ARRAY_UNION=11,  

  

  ECC_TD_ONE_OF_MANY=16,  
  ECC_TD_IGNORE=17,
  ECC_TD_ZERO_SIZED_STRUCT=18 
};

extern void 
__ExpRiat1(struct __ExpZtfp * tpl);

extern int __cl_ecc_stop_trapped(); 

extern void (*__cl_ecc_stop_function)(void);

        

extern char * __cl_ecc_search_path(char * execfile, char* extra_dir);


extern void  __ExpRslp(char* path[]);

extern char ** __cl_ecc_the_logger_path;

#if defined(__cplusplus)
}
#endif

#endif 

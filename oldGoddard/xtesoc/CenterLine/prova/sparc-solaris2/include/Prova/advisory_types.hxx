ADVTYPE(ArrayType)
ADVTYPE(BlockStatement)
ADVTYPE(BreakStatement)
ADVTYPE(ClassStructOrUnionType)
ADVTYPE(Constructor)
ADVTYPE(ContinueStatement)
ADVTYPE(DataMember)
ADVTYPE(DoStatement)
ADVTYPE(Entity)
ADVTYPE(EnumerationConstant)
ADVTYPE(EnumerationType)
ADVTYPE(Expression)
ADVTYPE(ExpressionStatement)
ADVTYPE(FloatType)
ADVTYPE(ForStatement)
ADVTYPE(Function)
ADVTYPE(FunctionMember)
ADVTYPE(FunctionType)
ADVTYPE(GotoStatement)
ADVTYPE(Handler)
ADVTYPE(IfStatement)
ADVTYPE(Initializer)
ADVTYPE(IntegerType)
ADVTYPE(LabelStatement)
ADVTYPE(LoopStatement)
ADVTYPE(Macro)
ADVTYPE(Member)
ADVTYPE(NamedEntity)
ADVTYPE(NamedTypedEntity)
ADVTYPE(Parameter)
ADVTYPE(PointerToMemberType)
ADVTYPE(PointerType)
ADVTYPE(ReferenceType)
ADVTYPE(ReturnStatement)
ADVTYPE(Statement)
ADVTYPE(SwitchStatement)
ADVTYPE(TryStatement)
ADVTYPE(Type)
ADVTYPE(TypedEntity)
ADVTYPE(TypedefType)
ADVTYPE(Variable)
ADVTYPE(VoidType)
ADVTYPE(WhileStatement)

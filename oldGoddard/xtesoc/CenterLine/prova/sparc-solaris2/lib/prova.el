; File:   expert.el
; Author: Mark Mitchell
; Date:   11/14/96
;
; Contents:
;   Lisp file loaded when "emacs" is used as an editor.

(server-start)
(add-hook 'server-switch-hook 
	  '(lambda () 
	     (progn
	       (let ((b (current-buffer)))
		 (server-edit)
		 (switch-to-buffer b)))))

	     

#! /bin/sh
set -u

  NAME='
	provafile' # classify files for AcquaProva
#
# IMPLEMENTATION NOTES
#	My convention for boolean variables is that '' means false and
#	anything else means true.  Here is how I usually test:
#
#		case $boolean_variable in
#		'' )	# the false case
#			;;
#		?* )	# the true case
#			;;
#		esac
#
#	Throughout this file we manipulate one-character statuses.
#	These codes are:
#
#		o	not built with provabuild
#		w	object or shared library built with provabuild
#		W	executable built with provabuild
#		!	compiled with, but linked without, provabuild
#		b	some objects compiled with, some without, provabuild

PATH=/bin:/usr/bin
MYTMPDIR=${CL_TEMPDIR-/tmp}/provafile$$

# This must be a global symbol.  It must be "U" in instrumented
# objects and shared libraries, and "T" or "D" in instrumented
# executables.
SYMBOL_FOUND_IN_ALL_PROVABUILT_OBJECTS=__ExpRrg

# 
# Configure for a particular system
# 
UNAME=/bin/uname
if [ ! -x $UNAME ]; then
	>&2 echo $NAME: "error: $UNAME not present; cannot determine system type"
fi
case `$UNAME -sr` in
SunOS\ 4.1.* )
	# SunOS 4.1.x.
	echo_n() { /bin/echo -n "$*"; }
	# use /bin/ar
	nm_for_global_symbols() { /bin/nm -gpu "$@"; }
	file_following_symlinks() { /bin/file -L "$@" ; }
	DEFAULT_IS_STRIPPED=yes
	USE_ELF=0
	DUMP=echo
	;;
SunOS\ 5.* )
	# Solaris 2.x = SunOS 5.x.
	echo_n() { /bin/echo "$*\c"; }
	ar() { /usr/ccs/bin/ar "$@"; }
	nm_for_global_symbols() { /usr/ccs/bin/nm -p "$@"; }
	file_following_symlinks() { /bin/file "$@"; }
	DEFAULT_IS_STRIPPED=
	USE_ELF=1
	DUMP=/usr/ccs/bin/dump
	;;
*\ 4.* )
       # System V release 4, I think.
	echo_n() { /bin/echo "$*\c"; }
	ar() { /usr/ccs/bin/ar "$@"; }
	nm_for_global_symbols() { /usr/ccs/bin/nm -p "$@"; }
	file_following_symlinks() { /bin/file "$@"; }
	DEFAULT_IS_STRIPPED=
	USE_ELF=1
	DUMP=/usr/ccs/bin/dump
        ;;
* )	>&2 echo $NAME: "error: unrecognized system type '"`$UNAME -sr`"'"
	exit 2
	;;
esac


# 
# Utility functions.
# 

PrintUsage() {
    echo "provafile [ -provabuilt | -noprovabuilt ] file ..."
    echo "provafile [ -usage | -help | -flags | -man ]"
}

UsageError() {
	>&2 echo "Incorrect usage."
	>&2 echo
	PrintUsage
	exit 2
}

InternalError() {
	>&2 echo $NAME: internal error: "$*"
	exit 3
}

Debug() {
	>&2 echo $NAME: Debug: "$*"
}

# FUNCTION
#	ConcatPathname -- form a composite pathname from two
ConcatPathname() {
	case "$2" in
	/* )	echo "$2" ;;
	* )	echo "$1/$2" ;;
	esac
}

CheckForInstrumentation() {
	# We throw away nm's stderr so that object files for other
	# architectures, which 'file' often recognizes, don't cause nm
	# to spit up to the user.  Instead, we just don't find the
	# symbol we're looking for, which is the right thing.
	nm_out="`nm_for_global_symbols $1 2>/dev/null |
		 fgrep $SYMBOL_FOUND_IN_ALL_PROVABUILT_OBJECTS`"
	case "$nm_out" in
	'' )	echo o ;;	# without provabuild
	*\ [TD]\ * )
		echo W ;;	# executable with provabuild
	*\ [U]\ * )
		echo w ;;	# object with provabuild
	* )	InternalError CheckForInstrumentation ;;
	esac
}

# This is a very lame implementation.
# this will echo one of:
#    w    means has prelinker info
#    o    means has no prelinker info
#    X    means has old, but should have New, prelinker info
CheckForObjextra() {
        if [ ${USE_ELF} -eq 1 ] ; then
	    ${DUMP} -h $1 | /bin/fgrep ".centerline.types" > /dev/null
	    case $? in
	    0 )	echo w ;;
	    1 )	
		# see if there is an old version
		# tst_objextra is magic found in non-elf, or old
		# versions of embed-objextra.c.
		/bin/fgrep  tst_objextra $1	> /dev/null 
		case $? in
		0 )	echo X ;;
		1 )	echo o ;;
		* )	InternalError CheckForObjextra ;;
		esac

		;;
	    * )	InternalError CheckForObjextra ;;
	    esac
	    
	else
		# tst_objextra is magic found in non-elf, or old
		# versions of embed-objextra.c.
	    /bin/fgrep  tst_objextra $1	> /dev/null 
	    case $? in
	    0 )	echo w ;;
	    1 )	echo o ;;
	    * )	InternalError CheckForObjextra ;;
	    esac
	fi
}

# FUNCTION
#	Check -- get provabuild status for a file
Check() {
	case `CheckForObjextra $1` in
	w )	echo w ;;
	o )	case `CheckForInstrumentation $1` in
		o )	echo o ;;
		w )	echo ! ;;
		W )	# It's an executable, so it's proper that it
			# lacks objextra.
			echo w
			;;
		* )	InternalError Check 1 ;;
		esac
		;;
	X )     echo X ;;
	* )     InternalError Check 2 ;;
	esac
}

CWD=`pwd`

verbose=yes
show_provabuilt=yes

while [ $# -gt 0 ]
do
	case $1 in
	-help | -usage | -flags )
		PrintUsage
		exit
		;;
        -man) # There's no /bin/man on SunOS4.
                if [ -x /bin/man ]; then
                  MAN=/bin/man;
                elif [ -x /usr/ucb/man ]; then
                  MAN=/usr/ucb/man;
                else
                  MAN=man
                fi

                (cd `/bin/dirname $0`/../../../man && \
                  ${MAN} -M . provafile)
		exit
                ;;
	-provabuilt )
		show_provabuilt=yes ;;
	-noprovabuilt )
		show_provabuilt= ;;
	-* )	UsageError ;;
	* )	break ;;
	esac

	shift
done

case $# in
0 )	UsageError ;;
esac

for f
do
	file_out=`file_following_symlinks $f`

	is_known=yes
	case $file_out in
	*"shell script"* )
		# Prevent us from thinking an "executable shell script"
		# is something we can run nm on.
		type="unknown file type; try the 'file' command"
		is_known=
		;;
	*"archive"* )
		type="ar archive" ;;
	*executable* | *relocatable* )
		type="object or executable" ;;
	*"dynamic lib"* | *"shared lib"* )
		type="dynamic lib" ;;
	*"cannot open"* )
		type="cannot open"
		is_known=
		;;
	* )	type="unknown file type; try the 'file' command"
		is_known=
		;;
	esac

	case $is_known in
	'' )	echo "$f: $type"
		continue
		;;
	esac

	case $file_out in
	*"not stripped"* | *"relocatable"* )
		# SunOS 4.x nm says "relocatable" rather than "not
		# stripped" for .o files; but otherwise the default is
		# "stripped".
		is_stripped= ;;
	*"stripped"* )
		is_stripped=yes ;;
	* )	is_stripped=$DEFAULT_IS_STRIPPED ;;
	esac

	case $is_stripped in
	?* )	echo "$f: $type, stripped"
		continue
		;;
	esac

	case $type in
	"ar archive" )
		trap 'status=$?; rm -rf $MYTMPDIR; exit $status' 1 2 3 15
		mkdir $MYTMPDIR

		archive_status=
		bang_elements=
		obsolete_elements=
		absolute_pathname_for_f=`ConcatPathname $CWD $f`
		for e in `ar t $f | uniq`
		do
			case $e in
			__.SYMDEF )
				# Special archive member under SunOS
				# 4.x, not nm-able.
				continue
				;;
			esac

			e_status=`cd $MYTMPDIR
				  ar x $absolute_pathname_for_f $e
				  Check $e
				  rm -f $e`
			case $e_status in
			! )	bang_elements="$bang_elements $e"
				;;
			X )	obsolete_elements="$obsolete_elements $e"
				;;
			esac
			case $archive_status,$e_status in
			# If the archive status has not been filled in,
			# fill it in.  If the element status is "!" or "X"m
			# that takes precedence.
			'',* | *,! | *,X )
				archive_status=$e_status
				;;
			w,w | o,o )
				;;
			w,o | o,w | b,[wo] )
				archive_status=b
				;;
			* )	rm -rf $MYTMPDIR
				InternalError archive 1
				;;
			esac
		done

		rm -rf $MYTMPDIR
		trap 1 2 3 15

		case $archive_status in
		w )	case $show_provabuilt in
			?* )	echo "$f: $type, all objects built with provabuild" ;;
			esac
			;;
		o )	echo "$f: $type, no objects built with provabuild"
			;;
		! )	echo "$f: $type, some objects compiled with provabuild but linked without it!"
			case $verbose in
			?* )	echo "	$bang_elements"
				;;
			esac
			;;
		X )	echo "$f: $type, some objects built with obsolete version of provabuild!"
			case $verbose in
			?* )	echo "	$obsolete_elements"
				;;
			esac
			;;
		b )	echo "$f: $type, some objects built with, some without provabuild"
			;;
		* )	echo_n "$f: $type, "
			InternalError archive 2 ;;
		esac
		;;
	* )	status=`Check $f`
		case $status in
		w )	case $show_provabuilt in
			?* )	echo "$f: $type, built with provabuild" ;;
			esac
			;;
		o )	echo "$f: $type, not built with provabuild" ;;
		! )	echo "$f: $type, compiled with provabuild but linked without it!" ;;
		X )     echo "$f: $type, built with obsolete version of provabuild!" ;;
		* )	echo_n "$f: $type, "
			InternalError non-archive
			;;
		esac
		;;
	esac
done

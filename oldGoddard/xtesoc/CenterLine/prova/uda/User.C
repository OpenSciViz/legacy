/*
  File:   user.C
  Author: Mark Mitchell
  Date:   9/3/96

  Copyright:

  (c) CenterLine Software, 1996.

  Contents:

  Implementation of a rule that checks that all classes have names
  beginning with the prefix "Cl".

  */

// Include the C++Expert advisory API.
#include <Expert/advisories.hxx>

// Include basic string manipulation routines.
#include <string.h>

class clPrefixCheck : public advRule<advClassStructOrUnionType>
{
public:
  clPrefixCheck()
  { Register(); }

  ~clPrefixCheck()
  { Unregister(); }

  // The name of the error.
  virtual const char* Name() const
  { return "BadPrefix"; }

  // The description of the error.
  virtual const char* Description() const
  { return "Class names should begin with \"Cl\""; }

  // The category of the error.
  virtual const char* Category() const
  { return "CenterLineDemo"; }

  // Check to see if the rule is violated.
  virtual advBool
  DoesEntityViolateRule(const advClassStructOrUnionType& c,
			advDiagnostic&                   d) const;
};


advBool 
clPrefixCheck::DoesEntityViolateRule(const advClassStructOrUnionType& c,
				     advDiagnostic& d) const
{
  // Assume that the class does not violate the rule.
  advBool result = adv_false;
  
  if (strlen(c.Name()) < 2 || strncmp(c.Name(), "Cl", 2) != 0) {
    // Either the name is not at least two characters long, or its
    // first two characters are not "Cl".  In either case, it is
    // misnamed!

    // Indicate that the error ocurred.
    result = adv_true;

    // Add the class to the diagnostic list so that the user will know
    // what class violated the rule.
    d << "Class:" << c;
  } // if

  return result;
}


// C++Expert will look for a function named "adv<Name>Init" where
// "<Name>" is the name of the rule library, and run that function
// when it loads the library.  This function *must* be declared
// "extern C".

extern "C" void advUserInit()
{
  // Create one (and only one) instance of the clPrefixCheck rule.

static clPrefixCheck cpc;
}


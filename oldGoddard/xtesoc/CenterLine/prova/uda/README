
Compiling And Debugging User-Defined Advisory Libraries
-------------------------------------------------------

For a basic introducation to writing User-Defined Advisories, please
read the online AcquaProva User's Guide and Reference, Chapter 3,
"Tasks", beginning with the subsection titled "Understanding
user-defined rules and advisories."  The User Guide can be accessed by
starting the AcquaProva GUI with the "prova" command, and then selecting
"AcquaProva User's Guide" from the Help menu.

This README file contains some corrections and additional information
on debugging User-Defined Advisories that is not available in the
AcquaProva User's Guide and Reference.

Creating a rule library:
 
To generate a rule library, you need to have access to the Free
Software Foundation compiler g++.  To debug a rule library, you need
gdb, the GNU debugger. You can obtain both by
downloading them from the from the following FTP site:
 
ftp://prep.ai.mit.edu/pub/gnu/gcc-2.7.2.2.tar.gz  
ftp://prep.ai.mit.edu/pub/gnu/gdb-4.1.6.tar.gz

To create a rule library from the C++ code you wrote to define the rules,
you need to compile the source code and put the resulting object files
into a shared library.
 
Be sure to use the -pic option when you compile, and use the -G option when you create the library.
 
Here are command lines to use for the example rules in User.C (make
sure that g++ is in your path)
 
  g++ -fpic -c -I/ path_to /CenterLine/prova/<arch_os>/include User.C
  g++ -fpic -shared -o User.so User.o 


Once the User.so user-defined rule library has been created, it can be
used by provabuild.  For example, usertest.C is a sample C++ program
against which the rules can be tested.  The command line to
enable the user-defined checking in User.so for usertest.C is as
follows:
 
  provabuild -rulelib User.so CC -c usertest.C 

When you build the usertest.C application with the User.so rule
library, AcquaProva generates the following advisory message:
 
"usertest.C", line 8: advisory: [BadPrefix] 
Class names should begin with "Cl" 
  
Class: 
Bad 


The example shows us that AcquaProva loaded the rule library and used
it to check for the "Cl" prefix in all classes defined in usertest.C.
The ClGood class did not generate an advisory, but the Bad class
violated the rule and caused AcquaProva to generate an advisory with
the BadPrefix tag.


Debugging a User-Defined Advisory Rule Library
----------------------------------------------

Once you have written a rule library, it may become necessary to run a
debugger to fix problems with the C++ code you have written.  Since
you used the Free Software Foundation g++ compiler to write your
rules, you will need to use gdb (the GNU debugger) to debug them.

To invoke gdb on your rule library, we provide a special flag to the
provabuild command, -ruledebug.  With -ruledebug, you specify the name of
a file into which provabuild will write a script that can be used to
invoke gdb with the correct arguments and start up commands.

For example, to invoke gdb to debug the rules in User.so when applied
to the test case usertest.C, you would perform the following steps:

  provabuild -ruledebug scriptfile -rulelib User.so CC -c usertest.C

which specifies that a gdb command script file should be generated in the
file "scriptfile".  The above command will write the following information
to standard output:

  Invoke gdb as follows (make sure gdb is in your path):
 
  gdb /usr/local/CenterLine/prova/sparc-solaris2/bin/edgcppfe -command=/u1/glenn/prova/demos/uda/scriptfile


If you issue the suggested gdb command, then gdb will be invoked on
the file edgcppfe, which is AcquaProva's source code parser and
compile-time checker process.  The script also contains a command to
set a breakpoint in advBreakpoint(), which is a function that is
called just after your user-defined rule library is loaded.
Therefore, issuing the suggested command starts gdb and leaves you at
a breakpoint where you can issue debugger commands to control
execution.

% gdb /auto/prova/nightly-builds/sparc-solaris2/ndebug/CenterLine/prova/sparc-solaris2/bin/edgcppfe -command=/u1/glenn/prova/demos/uda/scriptfile
GDB is free software and you are welcome to distribute copies of it
 under certain conditions; type "show copying" to see the conditions.
There is absolutely no warranty for GDB; type "show warranty" for details.
GDB 4.16 (sparc-sun-solaris2.4), 
Copyright 1996 Free Software Foundation, Inc...(no debugging symbols found)...
Breakpoint 1 at 0x14d93c
(no debugging symbols found)...(no debugging symbols found)...
(no debugging symbols found)...(no debugging symbols found)...
(no debugging symbols found)...(no debugging symbols found)...
(no debugging symbols found)...(no debugging symbols found)...
Breakpoint 1, 0x14d93c in advBreakpoint ()
(gdb) 


Now set a breakpoint in the DoesEntityViolateRule member of the
rule class you want to debug, and issue the cont command to continue execution:

(gdb) break clPrefixCheck::DoesEntityViolateRule
Breakpoint 2 at 0xef644254: file User.C, line 56.
(gdb) cont
Continuing.
 
Breakpoint 2, clPrefixCheck::DoesEntityViolateRule (this=0xef6588e8, 
    c=@0x3526d8, d=@0xefffef18) at User.C:56
56        advBool result = adv_false;
Current language:  auto; currently c++
(gdb)


At this point you can inspect variables, single step and otherwise debug
your rule library.  For example, here is how to single step in gdb:

(gdb) next
58        if (strlen(c.Name()) < 2 || strncmp(c.Name(), "Cl", 2) != 0) {
(gdb) next
71        return result;
(gdb) 

Warning: a bug in gdb may cause it do die with a segmentation
violation if you try to print out the value of the parameter c.  We
have reported this problem to the Free Software Foundation.

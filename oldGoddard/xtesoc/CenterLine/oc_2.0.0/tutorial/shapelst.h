#ifndef __SHAPELST_H__
#define __SHAPELST_H__

#include "link.h"
#include "shapes.h"

class ShapeList: public Link {
private:
  DrawableShape *shape;
public:
  ShapeList(DrawableShape *value=NULL);
  void setValue(DrawableShape *value);
  DrawableShape *value();
  DrawableShape *next();
 
};

#endif
 

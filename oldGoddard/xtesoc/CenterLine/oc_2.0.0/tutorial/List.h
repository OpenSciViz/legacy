//
// List.h
//
#ifndef __LIST_H__
#define __LIST_H__

#include <stdio.h>
#include <iostream.h>

template <class T> class List 
{
private:
  T *data;
  List *next;
public:
  List();			// conversion of a T to a List
 
  List(T& type);		// constructor of a List, given a T
 
  List *nextLink();		// return a pointer to the next
				// item in the linked list

  void add(T &newData);		// add an item containing 
				// newData to the list

  T *thisData();		// return pointer to this List`s data

  int operator==(T type);	// return 0 if *(this->data) not equal to type

/*  void display()		// this works only if T defines display
{ 
 cout << "the list is \n";
 List *temp=this; 
 while (temp && data) 
 { 
  temp->data->display(); 
  cout << "\n"; 
  temp=temp->next; 
 }
}*/


};  

#endif

#include "table.h"

#if defined(__STDC__) || defined(__CPLUSPLUS)
void  makeTable(short *rows,
		  short *cols,
		  int length,
		  short width,
		  short height,
		  short iwidth,
		  short iheight,
		  short margin
		  )
#else
void makeTable(rows, cols, length, width, height, iwidth, iheight, margin)
     short *rows,
       *cols,
       width,
       height,
       iwidth,
       iheight,
       margin;
     int length;
#endif
{
  short *last; /* Pointer to last element in table */

  short x = 0,   /* work X variable */
        y = 0;   /* work Y variable */
  
  short vx = 4,
        vy = 0;

  short Xmax,
        Ymax;

  short *xCoord;
  short *yCoord;

  xCoord = cols;
  yCoord = rows;
  last =   cols + length;
  Xmax = width  - iwidth  - margin;
  Ymax = height - iheight - margin;

  while (xCoord < last) {
    *xCoord = x;
    *yCoord = y;
    x += vx;

    if (x > Xmax) {
      x = 2 * Xmax - x;
      vx = -vx;
    }
    else if (x < 0) {
      x = -x;
      vx = -vx;
    }
    ++vy;
    y += vy;

    if (y >= Ymax) {
      y = 2 * Ymax - y;
      vy = 2 - vy;
    }
    xCoord++;
    yCoord++;
  }
}

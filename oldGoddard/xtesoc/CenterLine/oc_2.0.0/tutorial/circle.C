#include "circle.h"
#include "x.h"

Point DefaultPt(0,0);

Circle::Circle(Point &origin, int itsRadius):
        DrawableShape(origin) ,radius(itsRadius)
{
}

Circle::Circle(Circle& c) : DrawableShape(c), radius(c.radius)
{
}

void Circle::draw()
{
  drawCircle(origin.getX(), origin.getY(), radius, filled);
}

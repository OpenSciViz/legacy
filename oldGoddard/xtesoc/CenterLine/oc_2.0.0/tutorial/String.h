// definition of primitive String class
// used in examples in the Using ObjectCentermanual

class String{
public:
	String (char*);
	String(int);
	String();
	String(const String&);
	~String();
	String &operator=(const String&);
	String &operator==(const char*);
	friend String operator+(const String&, const String&);
	void getText();
	int getLength();
private:
	int len;
	char *text;
};

----------------------------------------------------------------
The following is "Not Implemented Messages", which is Appendix C
of the C++ Language System Release Notes for Release 3.0.
See the READMEcfront3.0.1 file for additional information.
-----------------------------------------------------------------

``Not Implemented'' Messages
     This appendix contains the  text  and  explanation  for  all
     ``not  implemented''  messages  produced by the C++ Language
     System Release 3.0.2.  They are listed  here  in  alphabetical
     order.

     Each message is preceded by a file  name  and  line  number.
     The  line  number is usually the line on which a problem has
     been diagnosed.

     A ``not implemented'' message is  issued  when  Release  3.0.2
     encounters  a  legal  construct for which it cannot generate
     code.  Because code is not  generated,  ``not  implemented''
     messages  cause  the command to fail, and the program is not
     linked.  Release 3.0 does, however, attempt to  examine  the
     rest of your program for other errors.

   o & of op

     This message should not be produced.

   o 1st operand of .* too complicated       
       
     The first operand of a function call expression
     involves a pointer to a member function and is an
     expression that may have side effects or may require a
     temporary.

            struct S { virtual int f(); };
            int (S::*pmf)() = &S::f;
            S *f();
            int i = (f()->*pmf)();

    "file", line 5: not implemented: 1st operand of .* too
    complicated

   o 2nd operand of .* too complicated

     The second operand of a pointer to member operator is
     an expression that has side effects.

             struct S { int f(); };
             int (S::*pmf)() = &S::f;
             S *sp = new S;
             int i = 5;
             int j = (sp->*(i+=5, pmf))();

     "file", line 5: not implemented: 2nd operand of .* too
     complicated

   o actual parameter expression of type string literal

     A template is instantiated with a sting literal actual argu-
     ment:

     template <char* s>  struct  S  {/*...*/};  
     S<"hello  world"> svar;

     "file", line 3: not implemented: actual parameter expression
     of type string literal

   o address of bound member as actual template argument

     A template is instantiated  with  the  address  of  a  class
     member bound to an actual class object:


          template <int *pi> class x {};
          class y { public: int i; } b;

          x< &b.i > xi;

     "file", line 4: not implemented: address of bound member  (&
     ::b . y::i) as actual template argument

 
   o call of virtual  function  before  class  has  been  completely
     declared

          class x {
          public:
              virtual x& f();
              int foo(x t = pt->f());
          private:
              static x* pt;
              int i;
          };


     "file", line 6: not implemented:  call of  virtual  function
     x::f()  before  class  x  has been completely declared - try
     moving call from argument list into function  body  or  make
     function non-virtual

   o cannot expand inline function  with  for  statement  

     A for statement appears in the definition of an inline function.

          struct S {
                 int s[100];
                 S() { for (int i = 0; i < 100; i++) s[i] = i; }
          };


     "file", line 1: not implemented: cannot expand inline  func-
     tion S::S() with for statement in inline

   o cannot expand inline function with return statement

     A void function contains a return statement.
          
          inline void f()
          {
                  return;
          }
          main()
          {
                   f();
          }


   o cannot expand inline function  with  statement  after ``return''

     A value-returning inline function contains a statement  fol-
     lowing a return statement.

          inline int f(int i) {
                 if (i) return i;
                 return 0;
          }


     "file", line 4: not implemented: cannot expand inline  func-
     tion f() with statement after "return"

   o cannot expand inline function function with two local variables
     with the same name 

     Two variables with the same name  and  different  types  are
     declared  within  the body of a value-returning inline func-
     tion.

          inline int f(int i) {
                 { int x = i; }
                 { double x = i; }
                 return 0;
          }


     "file", line 5: not implemented: cannot expand inline  func-
     tion f() with two local variables with the same name (x)

   o cannot expand inline function  needing  temporary  variable  of
     array type

     An inline function that contains a local declaration  of  an
     array object is called.

          inline int f(int i) {
                 int a[1];
                 a[0] = i;
                 return i;
          }
          int v = f(0);


     "file", line 6: not implemented: cannot expand inline  func-
     tion needing temporary variable of array type

   o cannot expand inline function with return in if statement

     This message should not be produced.

   o cannot expand inline function with static 

     An inline function contains  the  declaration  of  a  static
     object.

          inline void f() {
                 static int i = 5;
          }


     "file", line 2: not implemented: cannot expand inline  func-
     tion with static i

   o cast of non-integer constant

     A cast of a non-integer constant as an actual parameter to a
     template class.

          template <int i> class x;
          int yy;

          x< (int)&yy > xi;


     "file", line 4: not implemented: cast  of  non-integer  con-
     stant

   o cannot expand inline void function called in comma expression

     A call of an inline function that  cannot  be  translated  into  an
     expression (that is, one that includes a loop, a goto or a 
     switch statement) appears as the first operand of a comma operator.

          int i;
          inline void f() { for (;;) ; }
          void g() { for (f(), i = 0; i < 10; i++) ; }


     "file", line 3: not implemented: cannot expand  inline  void
     f() called in comma expression

   o cannot expand inline void function called in for expression

     A call of an inline void function that  cannot  be  translated  into  an
     expression (that is, one that includes a loop, a goto or a 
     switch statement) appears in the second expression of a for statement.

          void inline f() { for (;;) ; }
          void g() { for (;; f()) ; }


     "file", line 2: not implemented: cannot expand  inline  void
     f() called in for expression

   o cannot expand value-returning inline function with call of ...

     A value-returning inline function is defined,  and  it  con-
     tains  a  call to another inline function that is not value-
     returning.

          inline void f() { for(;;) ; }
          inline int g() { f(); return 0; }


     "file",  line  2:  not  implemented:  cannot  expand  value-
     returning inline g() with call of non-value-returning inline
     f()

   o cannot merge lists of conversion functions

     A derived class with multiple bases is  declared  and  there
     are  conversion  operators  declared in more than one of the
     base classes.

          struct B1 {
                 operator int();
          };
          struct B2 {
                 operator float();
          };
          struct D : public B1, public B2 { };


     "file", line 7:  not  implemented:  cannot  merge  lists  of
     conversion functions

   o catch

     The keyword appears; is reserved for future use.

          int catch;


     "file", line 1: not implemented: catch 
     "file", line 1: warning: name expected in declaration list

   o class hierarchy too complicated
     
     This message should not be produced.


   o conditional expression with type

     The second and third operands of  a  conditional  expression
     are member functions or pointers to members.

          struct S { int i, j; };
          void f(int i) {
                 int S::*pmi = i ? &S::i : &S::j;
          }


     "file", line 3: not implemented: conditional expression with
     int S::*

   o constructor needed for argument initializer

     The default value for an argument is a constructor or is  an
     expression that invokes a constructor.

          struct S { S(int); };
          int f(S = S(1));
          int g(S = 5);


     "file", line 2:  not  implemented:  constructor  as  default
     argument 
     "file", line 3: not implemented: constructor needed
     for argument initializer

   o copy of member[], no memberwise copy for class

     An implementation-generated copy operation for  a  class X is
     required,  but the operation cannot be generated because X has
     an array member whose type is a class with either a  virtual
     base  class  or  its  own  defined copy operation.  The wor-
     karound is to add a memberwise copy operator to X.

          struct S1 {};
          struct S2 : S1 { S2& operator=(const S2&); };
          struct X { S2 m[1]; };
          X var1;
          X var2 = var1;


     "file", line 5: not implemented: copy of S2[], no memberwise
     copy for S2

   o default argument too complicated

     A default argument  in  a  declaration  not  at  file  scope
     requires the generation of a temporary.

          struct S {
                 S();
                 int f(const int &r = 1);
          };


     "file",  line  3:  not  implemented:  default  argument  too
     complicated 
     "file", line 3: not implemented: needs temporary
     variable to evaluate argument initializer

   o ellipsis (...) in argument list of template function name

     An ellipsis is used in a template function declaration:

          template <class T> f(T, ...);


     "file", line 1: not implemented: ellipsis (...) in  argument
     list of template function f()

   o explicit template parameter list for destructor of  specialized
     template class name

     Explicit template parameters are included in declaration  of
     a specialized class' destructor:

          template <class T> struct S { /*...*/ };

          struct S<int> {
                 ~S<int>();
          };


     "file", line 4: not implemented: explicit template parameter
     list  for  destructor of specialized template class  S <> --
     please drop the parameter list.

     Instead, declare the specialized destructor as follows:

          template <class T> struct S { /*...*/ };

          struct S<int> {
                 ~S();
          };


   o formal type parameter name used as base class of template

     The formal type parameter is used as the  base  class  of  a
     template class:

          template <class T> struct S : public T {/*...*/};


     "file", line 1: not implemented:  formal  type  parameter  T
     used as base class of template

   o forward declaration of a specialized version of template name

     A forward declaration of a specialized, rather than general-
     ized template:

          template <class T> struct S;
          struct S<int>;


     "file", line 2: not implemented: forward  declaration  of  a
     specialized version of template  S <int >

   o general initializer in initializer list

     The initializer list in a declaration contains an expression
     that  cannot  easily  be  evaluated  at compile time or that
     requires runtime evaluation.

          int f();
          int i[1] = { f() };


     "file", line 2: not implemented: general initializer in ini-
     tializer list

   o initialization of name (automatic aggregate)

     An aggregate at local scope is initialized.  This message is
     not  issued  if the +a1 option (produces declarations acceptable
     to an ANSI C compiler) is specified.

          void f() {
                 int i[1] = {1};
          }


     "file",  line  2:  not  implemented:  initialization  of   i
     (automatic aggregate)

   o initialization of union with initializer list

     An object of union type is initialized with  an  initializer
     list.   This  message  is not issued if the +a1 option (produces
     declarations acceptable to an ANSI C compiler) is specified.

          union U { int i; float f; };
          U u = {1};


     "file", line 2: not  implemented:  initialization  of  union
     with initializer list

   o initializer for class member array with constructor

     This message should always be accompanied by an  error  mes-
     sage.   The ``not implemented'' message is inappropriate and
     should not be reported.

  o initializer for local static too complicated

     This message should not be produced.

   o initializer for multi-dimensional array  of  objects  of  class
     class with constructor name

     A multi-dimensional array of a class with a constructor  has
     an explicit initializer.

          struct S { S(int); };
          S s[2][2] = {1,2,3,4};


     "file", line 2:  not  implemented:  initializer  for  multi-
     dimensional array of objects of class S with constructor ::s

   o implicit static  initializer  for  multi-dimensional  array  of
     objects of class with constructor

          class x {
          public:
                  x() ;
          };

          main() {
          static x xx[10][20];
          }


     "file", line 7: not implemented: implicit static initializer
     for  multi-dimensional array of objects of class x with con-
     structor

   o initializer list for local variable name

     This message should not be produced.

   o label in block with destructors

     A labeled statement appears in a block in  which  an  object
     with a destructor exists.

          struct S { S(int);  S(); };
                             ~
          void f() {
                 S s(5);
          xyz:   ;
          }


     "file", line 5: not implemented: label in  block  with  des-
     tructors

   o local class class (local to function) as parameter to template
     class class

     A local class is defined and is used as a template actual argument.

          template <class T> class A {};

          void f()
          {
           class B {};
           A<B> a;
          }

    "file", line 2: not implemented: local  class B   (local  to
     f()) as parameter type to template class A  

   o local class name within template function

     A local class is defined inside a template funtion.  A simi-
     lar  message  is  issued  for  local enums and local typefefs
     defined inside a template function:

          template <class T> f() {
                 class l {/*...*/};
                 enum E {/*...*/};
                 typedef int* ip;
          };


     "file", line 2: not implemented: local  class  l  (local  to
     f())  within  template  function  
     "file", line 3: not implemented: local enum E(local to f()) within 
     template  function
     "file",  line  4:  not  implemented: local typedef ip within
     template function

   o local static name has class::~class() but no constructor
     (add class::class() )

     A static class object with a destructor, but no constructor, 
     appears at local scope.

          struct S { ~S(); };
          void f() { static S s;
          }


     "file", line 1: warning: S has S::~S() but no constructor
     "file", line 2: not implemented: local static s has S::~S() 
     but no constructor (add S::S())


   o lvalue op too complicated

     This message should not be produced.

   o needs temporary variable to evaluate argument initializer

     A default argument requires a temporary variable.

          void f() {
                 int g(const int& = 5);
          }

     "file", line 2: not implemented: needs temporary variable to
     evaluate argument initializer

   o nested class type as parameter type to template class name

     A nested class is used as the actual parameter  for  a  tem-
     plate class instantiation:

          template <class T> struct S;

          struct outer {
                 struct inner {};
          };

          S<outer::inner> svar;


     "file", line 7: not implemented: nested  class  outer::inner
     as parameter type to template class S


   o nested depth class beyond 9 unsupported

     Classes are nested more than nine levels deep.

          struct S1 {
           struct S2 {
            struct S3 {
             struct S4 {
              struct S5 {
               struct S6 {
                struct S7 {
                 struct S8 {
                  struct S9 {
                   struct S10 { enum { e }; };
          };};};};};};};};};

     "file", line 20: not implemented: nested depth class  beyond
     9 unsupported

   o overly complex op of op

     This message should not be produced.

   o parameter expression of type float, double or long double

     A template taking a non-type argument is declared  taking  a
     float, double or long double argument:

          template <double d> struct S { /*...*/};

     "file", line 1: not implemented:   parameter  expression  of
     type float, double, or long double

   o postfix template function operator ++(): please  make  a  class
     member function

     The postfix implementation of a template increment or decre-
     ment operator must be a member function.

          template <class t> struct x {
                 int operator++(int); // ok
          };

          template <class t>
               int operator++(x<t>&,int); // sorry

          x<int> xi;


     "file", "", line 6: not implemented: postfix template  func-
     tion operator ++(): please make a class member function

   o pointer to member function type too complicated

     This message should not be produced.

   o public specification of overloaded function

     The base class member in an access declaration refers to  an
     overloaded  function.   A  similar message is issued for private
     and protected access declarations.

          struct B { int f(); int f(int); };
          class D : private B {
          public:
                 B::f;
          };

     "file", line 2: not  implemented:  public  specification  of
     overloaded B::f()

   o reuse of formal template parameter name

     A template formal parameter name is reused within  the  tem-
     plate declaration:

          template <class T> struct S {
                 int T;
          };

     "file", line 2: not implemented: reuse  of  formal  template
     parameter T

   o specialized template name not at global scope

     A specialized template is  declared  at  other  than  global
     scope:

          template <class T> struct S {
                 T var;
          };

          void f() {
                 struct S <int > {
                 int var;
                 };

          };


     "file", line 6: not implemented: specialized template S  not
     at global scope

   o static member anonymous union

     A static class member is declared as an anonymous union.

          class C {
                 static union {
                 int i;
                 double d;
                 } ;
          };


     "file, line 5:  not  implemented:  static  member  anonymous
     union

   o struct name member name

     This message should not be produced.

     "file", line 17: not implemented: template function operator
     <<(): actuals too complicated (please simplify)

   o template function instantiated with local class name

          template <class T> int f(T);

          f2() {
                 struct local {/*...*/};
                 local lvar;
                 f(lvar);
          }


     "file", line 6:  not  implemented:   template  function  f()
     instantiated with local class local


   o too few initializers for name

     The initializer list for an array of class objects has fewer
     initializers than the number of elements in the array.

          struct S { S(int); S(); };
          S a[2] = {1};


     "file", line 2: not implemented: too  few  initializers  for
     ::a


   o type1 assigned to type2 (too complicated)

     A pointer is initialized  or  assigned  with  an  expression
     whose type is too complicated.

          struct S1 {};
          struct S2 { int i; };
          struct S3 : S1, S2 {};
          int S3::*pmi = &S2::i;


     "file", line 4: not implemented: int S2::* assigned  to  int
     S3::* (too complicated)


   o use of member with formal template parameter

     An attempt to use a member of a formal parameter type,  such
     as T::type, is not currently supported.  For example,

     template <class T> class U {  typedef T TU;
            // ...  };

     template <class Type> class V {   Type::TU t;        //
     ...  };


     "file", line 9: not implemented: use of  Type::TU  with for-
     mal  template  type parameter 
     "file", line 9: cannot recover from earlier errors

   o visibility declaration for conversion operator
     An access declaration is specified for a  conversion  opera-
     tor.

          struct B { operator int(); };
          class D : private B {
          public:
                 B::operator int;
          };

     "file", line 1: not implemented: visibility declaration  for
     conversion operator

   o volatile functions

     A member function is specified as

          struct S {
                 int f() volatile;
          };

     "file", line 2: not implemented: volatile functions

   o wide character constant

   o wide character string

     A wide character constant or  a  wide  character  string  is
     used.

          int wc = L'ab';
          char *ws = L"abcd";


     "file", line 1: not  implemented:  wide  character  constant
     "file", line 2: not implemented: wide character string









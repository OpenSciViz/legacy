
Known Problems with AT&T C++ Release 3.0 
--------------------------------------------------------------------------
(Appendix A of the AT&T C++ Language System Release 3.0 Release Notes)


NOTE	Most of the following information is directly reprinted from the 
	AT&T Release Notes. Any information we added is called out in a 
	NOTE such as this. 


This section is a reprint of the "Known Problems" (Appendix A) of 
the AT&T Release Notes for Release 3.0. It describes specific 
problem areas that remain in the C++ Language System. Where 
appropriate, the related sections of the Reference Manual are noted. 


Multiple Definitions (Section 3.3)

o In K&R C and in the ANSI C standard, implementations are free to 
  decide how to treat multiple, uninitialized definitions of objects with 
  external linkage at global scope.

  In C++ exactly one definition, initialized or uninitialized, may occur 
  in a single program. In order to enforce this rule, the C++ Language 
  System initializes most global variables to 0. However, in order to 
  reduce object file space, no initialization is done for global arrays. 
  Similarly, since most K&R C compilers reject such code, no 
  initialization is done for unions or for classes or arrays of classes 
  whose first element is a union.

  Users should be aware that invalid multiple definitions for these 
  cases may go undetected. 

o For compatibility with previous releases of the C++ Language System, 
  static data members of non-template classes are implicitly defined. 
  This means that multiple definitions of the same static member in 
  multiple files will result in multiple calls to the constructor.

  For example, suppose that the header file a.h defines a class with a 
  static member:

	struct A { A(); };
	struct B { static A ab; };

  and file a.c contains the definition of the static data member:

	#include "a.h"
	A B::ab;

  as does file main.c:

	#include "a.h"
	A B::ab;
	main() { /* ... */ }

  When these files are compiled and linked together, the duplicate 
  definitions of B::ab will not be reported and the constructor for B::ab 
  will be called twice.


Global Inline Functions Are Static (Sections 7.1.2, 7.1.1, and 3.3)

The Release 2.0 Reference Manual allowed a non-member inline 
function to have external linkage. The Release 3.0 Reference Manual 
specifies, however, that a name of global scope that is declared 
inline is local to its file.

Release 2.1 and Release 3.0 do not conform to these rules.For 
example, the following code is accepted by Releases 2.0, 2.1 and 3.0:  
f() is treated as a static function, and a static definition of f() is laid 
down. 

	extern int f(int);
	inline int f(int i) { return i; } // error, not reported
	int i = f(0);
	int (*pf)(int) = &f;

Instead, the C++ Language System should report an error that f() 
cannot be redeclared as inline after being declared extern.


Reuse of a Class Name by its Members (Section 9.2)

The Release 3.0 Reference Manual limits the ways in which a class 
name can be reused by members of the class. The rule is that a static 
data member, enumerator, member of an anonymous union, or 
nested type may not have the same name as its class. 

Release 3.0 does not enforce these restrictions completely. An error 
is reported if an enumerator or nested type has the same name as its 
enclosing class, but a static data member or member of an 
anonymous union are not caught.

	struct S1 {
		static int S1;                   // illegal, no error
	};

	struct S2 {
		union { int i; float S2; };	// illegal, no error
	};


Nested Types (Section 9.5)

This release completes the introduction of true nested types. There 
is one known problem in the new implementation:

o  Protection has not yet been implemented for nested types:

	   class A {
		enum E {/*...*/};  // private
		//...
	   };

	   A::E evar;			// undetected error,
					// A::E should not be accessible


Pure Virtual Functions (Section 10.3)

The C++ Language System fails to detect the use of a pure virtual 
function inside the class's own destructor. Other invalid uses of a 
pure virtual function are correctly detected:

	struct Base {
		Base();
		Base();
		virtual void f() =0;
	};

	Base::~Base() {
		f();			// undetected error
	};

	Base::Base() {
		f();			// correctly detected
	};

	Base f();			// correctly detected
	f(Base);			// correctly detected

The following errors are correctly reported

	line 13: error: call of pure virtual function Base::f() in 
		constructor Base::Base()
	line 15: error: abstract class Base cannot be used as a function 
		return type
	line 15:     Base::f() is a pure virtual function of class Base
	line 16: error: abstract class Base cannot be used as an argument type
	line 16:     Base::f() is a pure virtual function of class Base

but there should be a similar error reported on for the case involving 
the destructor.


Friendship (Section 11.4)

The C++ Language System invalidly extends friendship throughout 
the class hierarchy in a multiple inheritance lattice:

	class base1 {
		friend void foo();
	protected:
		int i;
	};

	class base2 {
	protected:
		int j;
	};

	class derived : public base1, public base2 {
	protected:
		int k;
	public:
		derived();
	};

	void foo() {
		derived der;
		der.i = 1;		// ok, foo is friend of base1
		der.j = 2;		// undetected error
		der.k = 3;		// detected error
	};

The following correct error is reported:

	line 23: error:  foo() cannot access derived::k: protected  member

but there should be a similar error for the assignment to der.j:

	line 22: error:  foo() cannot access derived::j: protected  member


Static Members (Section 11.5)

o  Release 3.0 is too restrictive in its treatment of protected static 
   members of a base class when they are accessed by friends of a derived 
   class. The following example should compile without complaint:

	class S1 {
	protected:
		static int s;
	};

	struct S2 : public S1 {
		friend int f1() { return S1::s; }	// legal
		friend int f2() { return S2::s; }	// legal
	};

   Instead, the following errors are incorrectly reported:

	error:  f2() cannot access S1::s: protected  member
	error:  f1() cannot access S1::s: protected  member

   This problem can be circumvented by referring to the base class's 
   static member through an object of the derived class:

	friend int f3(const S2& s2) { return s2.s; }

o  Section 11.5 of the reference manual states that "a friend or a member 
   function of a derived class can access a protected static member of a 
   base class" and section 12.5 specifies that "An X::operator new() 
   [delete()] for a class X is a static member". The C++ Language System 
   fails to allow the implied access to static members new and delete:

	typedef unsigned int size_t;

	class base {
	protected:
		void * operator new(size_t);
		void operator delete(void *);
		void static_memf();
	};

	class derived : public base {
	public:
		void f() {
			base *b = new base();		// invalidly rejected
		 	delete b;			// invalidly rejected
			static_memf();			// correctly allowed
		};
	};


   Produces the following invalid errors:

	line 10: error:  derived::f() cannot access base::operator delete(): 
		protected  member
	line 10: error:  derived::f() cannot access base::operator new(): 
		protected  member


Access control and constructors and destructors (Section 12.3)

o The reference manual stipulates that normal access control is applied 
   to constructors and destructors. This implies that making a destructor 
   private or protected disallows automatic and static allocation of such 
   objects since they could never be destroyed. The C++ Language 
   System correctly enforces this rule in most situations. However, it 
   invalidly creates temporaries of such types when passing arguments 
   as const references and then invalidly calls the private destructor:

	class A;

	struct B {
		B();
		~B();
		void foo (A const&);
	};

	class A {
	private:
		~A();
		void operator=(A&);
		A(A&);
	public:
		A(B);
	};

	main() {
		B b;

		A a(b);				// correctly detected
		A a1 = A(b);			// correctly detected

		b.foo(b);			// undetected error
		b.foo(A(b));			// undetected error
	};

   The following correct errors are reported:

	line 21: error:  main() cannot access A::~A(): private  member
	line 22: error:  main() cannot access A::~A(): private  member

   but there should be similar errors for the calls to b.foo.

o  The C++ Language System also fails to detect invalid calls to operator 
   delete for classes with a private destructor:

	class B {
		~B();			// private
	};

	main() {
		B b;			// correctly detected

		B* bp = new B;	// legal

		delete bp;		// undetected error 
	};


Protection and Destructors (Section 12.4)

o  If a base class has a private destructor, only member and friend 
   functions of that class may destroy objects of that class. However, 
   Release 3.0 fails to enforce this protection for derived classes that do 
   not redefine the destructor at the same protection level. Thus, 
   protection can be overridden by a derived class that simply fails to 
   declare a destructor or by a derived class that declares a destructor 
   with less restrictive protection.

   For example, the following code compiles without complaint:

	class B {
	private:
		~B();
	};

	class D: public B {};

	class D2: public B {
	public:
		~D2() { }
	};

	void f() {
		D d;	// undetected error
		D2 d2;	// undetected error

	}

   Instead, f() should not be able to create objects of type D or D2.

	
Template Classes (Section 14.2)

In processing templates, the C++ Language System builds up internal 
representations of template classes and functions but does not type 
check or otherwise validate user code until a template is instantiated. 
For example, in the code below, the definition of the non-existent static 
member y is not detected until a variable of the template type A is 
declared:

	template <class T> struct A {
		static int x;
	};

	template <class T> int A<T>::y = 37; 	// error not 
						// detected until
					        // a variable of type A<...> 
					        // is declared

It is a good idea, therefore, when developing code that defines 
template classes or functions to include simple references to the 
template type to force instantiation time type checking and other 
semantic checking. For example, if the above code had been 
compiled with a use of template A, the error would have been 
correctly reported:

	template <class T> struct A {
		static int x;
	};

	template <class T> int A<T>::y = 37;

	A<int> _dummy;

Produces the following correct error messages:

	line 8: error:  y: only static data members can be parameterized


Template Declarations (Section 14.5)

If two class templates refer to each other, one referring to the other 
only via a pointer or reference, and the other referring to the first in a 
way that requires the full definition to be known, the C++ compiler 
may produce errors depending on the order in which uses of the 
templates appear in user code. For example:

	template <class T> class B;

	template <class T> class A {
		B<T>* ptr;
	};

	template <class T> class B {
		A<T> not_a_ptr;
	};
	A<int> something; //Causes error

produces the following invalid errors:

	line 9: error:  A undefined, size not known
	line 9: error detected during the instantiation of B <int >
	line 9:     the instantiation path was:
	line 3:       template: B <int >
	line 12:     template: A <int >

If a use of A<int> is seen before a use of B<int>, the instantiation 
will either fail, or produce invalid C code. If a reference to B<int> is 
seen first, there are no errors.

The workaround is to add a dummy reference to B<int> before the 
first reference to A<int>:

	typedef B<int> dummy;
	A<int> something;


Member Function Templates (Section 14.6)

In processing templates, the C++ Language System builds up an 
internal representation for the template, but does not actually process 
instantiations until the end of the file. This is to allow for correct 
processing of template specializations. However, this approach has 
several side effects with respect to processing inline member function 
templates. To be inlined, member functions must be defined inside the 
class definition. For example, the Vector constructor in the following 
code will be laid down out of line in each file rather than being inlined:

	template<class T> class Vector {
	public:
		inline Vector(int size);
		T& operator[](int i) {return vec[i];}
	private:
    		int size;
		 T* vec;
	};

	template<class T> 
	inline Vector<T>::Vector(int sz) : vec(new T[size=sz]) 
	{}

	main()
	{
		typedef char *String;

		String a = "foo_bar";
		Vector<String> str_vec(2);

		str_vec[0] = a;

	}

Similarly, errors will be reported if a member function is not 
declared to be inline in the class template but is but subsequently 
defined as inline. For example: 

	template <class T> class A {
	public:
		void f(T t);
	};

	template <class T> inline void A<T>::f(T t)
	{
	}

	main()
	{
		A<int> a;

		a.f(0);

	}

produces the following errors:

	line 7: error: A <int>::f()\|declared with external\|linkage and 
		called before defined\|as\|inline
	line 7:          error detected during the instantiation of A <int >
	line 24:        is the site of the instantiation


Preprocessing (Section 16)

The C++ Language System does not include a version of cpp but 
instead uses the cpp resident on the host machine. Many cpps do not 
recognize C++ comments. This can sometimes lead to surprising 
results.

For example, if your preprocessor has not been modified for C++, 
C++-style comments (//) in a macro definition will not be ignored:

	#include <stream.h>
	#define A 5 // define something

	main() {
		cout << A;
	}

The comment in the macro definition results in the following error 
message:

	line 6: error: `;' missing after statement

Similarly, use of a macro name within a // comment

	#include <generic.h>

	main() 	{
		int a;	// declare variables
		float f;
	}

sends many preprocessors into an infinite loop expanding the macro 
declare, which is defined in generic.h. Note that generic.h may be 
included by other files as well. For example, stream.h indirectly 
includes generic.h.

Note also that the use of contractions, such as "can't" or "isn't"
can confuse some preprocessors. The solution is to change the 
contractions to full words (e.g., cannot, is not).

Finally, interactions between C comments and C++ comments 
should be noted. For example,

	#include <stddef.h>
	main() {
		//** this looks like a C-style block comment to cpp
		char *c = NULL;
		/* this is a C-style block comment */
	}

results in the following error message if cpp does not properly 
handle C++-style comments:

        line 1: error: syntax error
	line 4: error:  NULL undefined


Incompatibilities with the ANSI C Standard

Release 3.0 fails to accept C-conforming declarations for functions 
taking function arguments. For example,

	void f(int());

produces the following error:

	line 1: error:  bad base type: void f

If a function pointer is specified as a parameter, like this,

	void f(int(*)());

the code is accepted.


Missing or Extraneous Warnings

o  The C++ Language System is sometimes too cautious in deciding 
   when it is necessary to generate code to invoke a destructor. As a 
   result, unreachable code containing destructor invocations is 
   sometimes generated, and some C compilers warn about this 
   unreachable code. For example:

	struct A {
		A();
		~A();
	};
	void f(int i) {
		switch(i) {
		case 0: {
			A a;
			break;
			};
		}
	}

   This code may result in the C compiler warning:
 
	line 6: warning: statement not reached 

   which can be safely ignored.

o  C++ allows conversions that may involve loss of information. Because 
   such conversions are likely to introduce errors in the user's code, the 
   C++ Language System should warn about shortening conversions. In 
   general, such conversions are diagnosed only when assigning a float, 
   double, or long value to one of the smaller integral types. The 
   following shortening conversions are accepted without complaint:

	extern char c;
	extern short s;
	extern unsigned char uc;
	extern unsigned short us;
	extern int i;
	extern long l;
	extern float f;
	extern double d;

	void x() {
		f = d;
		c = i;
		l = d;
		l = f;
		c = s;
		s = i;
		uc = i;
		us = i;
	}

o  Some instances of ``used before set'' warnings are invalid. For 
   example, the code below causes the C++ Language System to warn 
   incorrectly that s is used before set.

	struct S {
		short a;
		short b;
	};
	void f() {
		S s;
		s.a = s.b = 0;	// invalid ``used before set'' warning
	}

o  Use of the sizeof operator also leads to invalid ``used but not set'' 
   warnings, as in the following code:

	void g() {
		char *p;
		int i = sizeof(p);	//invalid ``used but not set''  
                       //warning
	}


Other Problems with Compiling and Linking 

o  Single files compiled directly to an a.out which contain specializations 
   of templates will occasionally fail. For example:

	extern "C" void printf(...);
	template <class T> int foo(T) { return 1; }

	main()
	{
		int  i = 3;
		printf("%d\en", foo(i));  // should print 0
	}
	int foo(int) { return 0; }
	
   Invalidly produces the following error from the c compiler:
	line 11: redeclaration of foo__Fi

   This is because the single file case is optimized to avoid automated 
   instantiation support and the system invalidly instantiates the printf 
   call to create the template version of foo(int). When the subsequent 
   specialization is seen, the template instance has already been 
   created. This problem can be avoided either by ensuring that all 
   specializations precede any use:

	extern "C" void printf(...);
	template <class T> int foo(T) { return 1; }

	int foo(int) { return 0; }

	main()
	{
		int  i = 3;
		printf("%d\en", foo(i));  // should print 0
	}

   or by compiling CC -ptn which ensures that full automated 
   instantiation support is invoked. 

o  At present, function templates declared in header files have only the 
   raw name extracted and added to the name mapping files. So for:

	template <class T, class U> void f(T, int, U);

   f will be extracted. This works for many simple cases, but fails in 
   some cases where two different headers declare a function template 
   with the same or different formal arguments.

o  For compatibility with previous releases, the argument type of 
   __vec_new and vec_delete under the +a1 ANSI option are incorrect. 
   Strict ANSI compliance would declare these functions as:

	__vec_new(void*, int, int, void(*)());
	__vec_delete(void *, int, int, void(*)(), int, int);

   However, doing so would lead to bootstrapping problems when 
   building the compiler using libC compiled with earlier releases.

o  For compatibility with previous releases, static class members and 
   static template class members created from a template specialization 
   are not initialized to 0. This may lead to problems with linkers that do 
   not pull in object files from an archive if there are no initialized 
   external references. If a file exists whose only external dependency is 
   an uninitialized static data member or an uninitialized global array, 
   these linkers will fail to include the object file and a runtime error will 
   occur.

   For example, suppose ab.h defines a class with a static data member:

	// file "ab.h"
	struct A {
		int i;
		A() { i = 3; }
	};

	struct B {
		static A a;
	};

   and file ab.c defines the static member

	// file "ab.c"
	#include "ab.h"
		A B::a;

   and file main.c refers to the static member

	// file "main.c"
	#include <stdio.h>
	#include "ab.h"

	main() {
	printf ("%d\en", B::a.i);
	// ...
	return 0;
	}

   If these files are directly compiled and linked, the expected output 
   of 3 is printed on the standard output. However, if the file ab.c is 
   compiled and stored in a library and later linked with main.c, then 
   the program prints 0 if a linker that does not resolve uninitialized 
   data is used.

o  When two files are compiled separately in separate directories, but 
   contain identically named objects of the same class, problems will 
   occur when an attempt is made to link the two object files.

   For example, suppose you have a header file x.h in your current 
   directory, and you have two subdirectories a and b, each of which 
   contains a file named x.c.

	// file "x.h"
	struct X {
		virtual void f() {};
	};

	// file "a/x.c"
	#include "../x.h"

	void f() {
		X x;
	}

	// file "b/x.c"
	#include "../x.h"

	main() {
		X x;
		// ...
		return 0;
	}

   If these files are compiled separately and an attempt is made to link 
   them together,

	cd a
	CC -c x.c
	cd ../b
	CC -c x.c
	cd ..
	CC a/x.o b/x.o

   they will fail to link, and messages similar to the following will be 
   generated by the linker.

   These errors occur because the names of the virtual tables and 
   associated housekeeping information for the X objects in files a/x.c 
   and b/x.c are encoded identically, so the symbols are multiply 
   defined.

   A workaround for this problem is to rename one of the files or to 
   use a longer pathname when compiling these files.


Library Problems

NOTE:  CenterLine does NOT currently supply the task library.

The implementation of the task library limits the number of levels of 
derivation from class task to one. That is, a class derived from class 
task may not have derived classes. However, use of multi-level 
inheritance is not detected and usually results in an unexpected 
runtime core dump.

One possible workaround for this limitation is to put the required 
complex structures in a class not derived from task. Then derive a 
trivial class from task whose constructor executes the coroutine in 
the complex task. For example:

	class Task_base {
		virtual int Main();
	};

	class Runner : public task {
		Task_base*	p;
	public:
		Runner(Task_base*);
	};

	Runner::Runner(Task_base* fp) : p(fp)
	{
		resultis(p->Main());
	}

Class Task_base is the base class from which the user should derive 
whatever additional classes and structures are needed.


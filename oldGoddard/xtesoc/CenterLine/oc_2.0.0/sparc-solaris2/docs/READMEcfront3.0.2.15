This file contains code fragments which highlight each of the bugs
fixed by this patch release.

====================================================================
 * 3.0.2.01
 * Summary: illegal enum value uncaught

enum {
	VAL_NEG    = 0x80000000,          	// error
	VAL_NEG2   = (int)0x80000001,   
	VAL_POS    = 0X7FFFFFFF,       
	VAL_POS2   = (int)0x7FFFFFFE  
};				

====================================================================
 * 3.0.2.02
 * Summary: cfront goes into infinite recursion loop

class TypedValue {
public:
        TypedValue(TypedValue &);
};

class TypedAttribute : public TypedValue {
public:
	void foo(const TypedAttribute &)  const;

};

void TypedAttribute::foo(const TypedAttribute &a) const {
   (const TypedValue)(*this); //Causes infinite recursion in compiler
   (const TypedValue)(*this); //Causes infinite recursion in compiler
   (const TypedValue)(*this); //Causes infinite recursion in compiler
   (TypedValue)(*this); //Causes infinite recursion in compiler
   (TypedValue)(*this); //Causes infinite recursion in compiler
   (TypedValue)(*this); //Causes infinite recursion in compiler
}

====================================================================
 * 3.0.2.03
 * Summary: Typedef of arrays based on a nested class don't work

struct CORBA {
	class any {};
};

typedef short  (T1) [3];
typedef CORBA::any (T2) [3];

--------------------------------------------------------------------
 * 3.0.2.03 (con't)
 * Summary: incorrect code generated for virtual base class

struct Terminal {
    virtual void ComputeCurrents(void);
    virtual void what(void);
};
 
struct PowerSource: virtual public Terminal {};
struct Charger : public PowerSource {
    void ComputeCurrents(void);
};

void Charger::ComputeCurrents() {what();}
void Terminal::ComputeCurrents() {}
void Terminal::what() {}
 
int main() {
    Charger c;
    PowerSource *t;
    t = &c;
    t -> ComputeCurrents();
    return 0;
}
 
====================================================================
 * 3.0.2.04
 * Summary: inline expansion uses wrong formal parm name

extern "C" {
class _iobuf {
	unsigned char *_ptr;      
	short          _cnt;      
  public:
	friend inline int getc(_iobuf *p);
	};
extern _iobuf _iob[];  
extern int _filbuf(_iobuf *stream_p);

// inline defined with "p" formal parameter, and use of "p" within body
inline int getc(
	_iobuf *p
	)
{	return (--(p)->_cnt < 0 ? _filbuf(p) : (int) *(p)->_ptr++);
	}

typedef _iobuf FILE;

// (supposedly) benign re-declaration of above, identical via a typedef
// note: still within extern "C" { ... block
extern int getc(
		FILE *stream_p
		);
} /* "C" linkage */

static int saved, saved2;	//// allow warning.*not used
extern FILE *in_file;

int func() {
	saved= saved?(getc(in_file)):(saved2= getc(in_file), 0);
	return getc(in_file);
}

--------------------------------------------------------------------
 * 3.0.2.04 (con't)
 * Summary: this.member incorrectly generated when this->member needed

class Base {};
struct Base_object : public Base { };
struct Student :  public virtual Base_object { };
struct Teacher : public virtual Base_object { };
struct Student_Teacher : public Teacher, public Student { };
struct Volunteer : public virtual Base_object { };

struct Volunteer_Student_Teacher : public Student_Teacher, public Volunteer {
	Base_object* useGeneratedCopyCtor() const; 
};
Base_object* Volunteer_Student_Teacher::useGeneratedCopyCtor() const	{
	return new Volunteer_Student_Teacher(*this);
}

--------------------------------------------------------------------
 * 3.0.2.04 (con't)
 * Summary: compiler-gen'd temporary incorrectly declared const

class BV;
class TC  {
public:
        TC();
        ~TC();
        const unsigned long as_uint() const;
        const unsigned long x(BV& bv) const;
};

class BV {
public:
        TC    TwosComplement() const;
};

const unsigned long TC::x(BV& bv) const {
    return bv.TwosComplement().as_uint();
}

--------------------------------------------------------------------
 * 3.0.2.04 (con't)
 * Summary: improper "const" generated for temporary

class String {
public:
	String(const char *);
	~String();
};
class some_class {
public:
	const int uses_string(const String &);
	void foo();
};

void some_class::foo() {
	if (uses_string("some string"))
        	return;
};

--------------------------------------------------------------------
 * 3.0.2.04 (con't)
 * Summary: bad ANSI C - const-pointer temporary & assignment to it

class some_object { public: };

class list : public virtual some_object {
 public:
	int compare(const some_object & obj) const;
};

class foo : public virtual some_object {
 public:
	static const foo & castdown(const some_object &);
	int compare(const some_object & obj) const;
 private:
	list * d_args;
};

int foo::compare(const some_object & obj) const {
	return d_args->compare(*((castdown(obj)).d_args));
}

====================================================================
 * 3.0.2.07
 * Summary: cfront dumps core on small program

struct String {
	operator char *(void);
	operator char *(void) const;
};

struct ostream {
	ostream& operator<< (const char*);
	ostream& operator<< (void*);
};

void operator<<(ostream& o, String *str) {
	o << *str;
}

====================================================================
 * 3.0.2.08
 * Summary: Reference constructor called with wrong source address.

#include <stdlib.h>

struct X
{
  int xdummy[10];
  X() { printf("X::X() this = 0x%.8x # ",this);}
  X(const X &x) { printf("X::X(const X &x) this = 0x%.8x x = 0x%.8x\n",this,&x);}
};

struct Y 
{
  int ydummy[20];
  class X yx;
  Y(int) {printf("Y::Y() this = 0x%.8x\n",this);}
  Y(const Y &y);
};

Y::Y(const Y &y) : yx(y.yx)
{
  printf("X::X(const X &x) this = 0x%.8x x = 0x%.8x\n",&yx,&y.yx);
  printf("Y::Y(const Y &y) this = 0x%.8x y = 0x%.8x\n",this,&y);
}

main()
{
  // output from printf() should be in pairs of matching lines
  class Y y1(5);
  printf("X::X() this = 0x%.8x # ",&y1.yx);
  printf("Y::Y() this = 0x%.8x\n",&y1);
  class Y y2(y1);
  printf("Y::Y(const Y &y) this = 0x%.8x y = 0x%.8x\n",&y2,&y1);
  return 0;
}

====================================================================
 * 3.0.2.09
 * Summary: Nested type not handled as template argument

template <class T> class A {};
class B {
public:
	class C {};
};

A<B::C> a;

====================================================================
 * 3.0.2.10
 * Summary: compiler error from *const variable assignment

class First {
	virtual	void dummy() ;
public:
	int 	var1;
};

struct Second { First	var2; };
struct Third  { First 	var3; };
struct Fourth { Third	var4; };

void func1(Fourth* const param) {
    Second *var5= new Second;
    param -> var4.var3 = var5 -> var2; // this is the critical assignment
    param -> var4.var3.var1 = var5 -> var2.var1;
}

====================================================================
 * 3.0.2.11
 * Summary: Problem with virtual functions

struct base {
  virtual int g() const =0;
  virtual int f() const =0;
};

class lchild : public virtual base { };

struct rchild : public virtual base {
  int f() const { return g();};
};

struct rchild_helper: public rchild {
  int g() const { return 23;};
};

class grandchild : public lchild, public rchild_helper { };

void main() {
  grandchild *gptr=new grandchild;
  lchild *lptr=gptr;
  gptr->f();
  lptr->f();
}

====================================================================
 * 3.0.2.12
 * Summary: cfront rejects use of T::*

template <class T, class U> struct MP {
	const T k;
	U v;
	MP(const T& t) : k(t) {}
};

template <class T, class U> struct M {
	M(const MP<T,U>*);
	U& operator[](const T&);
};

template <class T> struct KM {
	KM();
	M<int, void (T::*)(int)> m;
	void f(int, void (T::*)(int));
};

template <class T> void KM<T>::f(int i, void (T::*p)(int)) {
	m[i] = p;
}

struct A {
	static KM<A>* km;
};

struct B {
	static KM<B>* km;
};

====================================================================
 * 3.0.2.13 (2 bugs fixed)
 * Summary 1: Making all functions inline causes internal error

class Base {
public:
   Base () {}
   virtual ~Base () {}
};

class Derived : public Base {
public:
   Derived () : Base () {}
   ~Derived () {}
   void* operator new (size_t);         
   void operator delete (void*);
};

void* Derived::operator new (size_t size) {
   size = size;
   Derived* ptr = ::new Derived ();
   return ptr;
}

void Derived::operator delete (void* node) {
   ::delete node;
}

void main () {
   Derived* ptr = new Derived ();
   delete ptr;
}

 * Summary 2: latest cfront patch generates wrong code for cast

extern "C" int printf(const char*, ...);

void main(void) {
    double y=1.9, y2=0.9;
    int i=int(1.9 + 0.9);
    int j=int(y + y2);

    printf("i=2\ni=%d\n", i);
    printf("j=2\nj=%d\n", j);
}

====================================================================
 * 3.0.2.14
 * Summary: cfront issues internal error

class	Date {
  public:
        long    yyyymmdd;
	Date    operator = (Date d) { yyyymmdd=d.Getyyyymmdd();
				      return *this; }
	long	Getyyyymmdd(){ return yyyymmdd; };
} ;

class	Deal {
public:
  Date		maturity;
};

class SmallDeal : public Deal
{
public:
  int *Copy()
    {
      SmallDeal *objptr = new SmallDeal;
      *objptr = *this;
      return (int*)objptr;
    }
};

====================================================================
 * 3.0.2.15
 * Summary: cfront generates inconsistent function declaration/definition

struct X {
	void prepend() {
		headLink();
	}
	void* headLink() const {
		return 0;
	}
};

struct Y { 
	void foo(); 
	X* container();
};

void Y::foo() { 
	container()->headLink();
}

====================================================================

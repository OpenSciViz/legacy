# $Revision: 1.6 $
Name: action
action -  set action to execute everywhere
action [at] line - set action on line
action [at] "file":line - set action on line in "file"
action [in] function - set action on first line of func
action [on] variable - set action on variable
action [on] address - set action on address
action [on] lvalue - set action on lvalue
Name: alias
alias - show all command aliases
alias name - show text of the alias
alias name text - set alias to text
alias name text alias_args - set alias to text with arguments
Name: assign
assign var = exp - evaluate expression and assign to variable
Name: build
build - update by loading all files that %s knows about
Name: catch
catch - show all trapped signals
catch num - trap signal num
catch name - trap signal name
Name: cd
cd path - change the working directory to the specified path
cd - change the working directory to the home directory
Name: config_c_parser
config_c_parser config - change the parser configuration
config_c_parser - show the current and possible parser configurations
Name: cont
cont - continue execution from a break level
cont value - substitute value and continue
Name: contents
contents - list loaded files
contents -ascii - list loaded files in the Workspace
contents all - list all files linked from libraries
contents file - list all symbols defined in file
Name: delete
delete - delete debugging items at break location
delete all - delete all debugging items
delete "file":line - delete debugging item at specified location
delete n - delete debugging item n
delete n1 n2 ... - delete debugging items n1 n2 ...
Name: display
display expression - display value of expression
display variable - display value of variable 
Name: down
down - move down the execution stack
down n - move down n levels
Name: dump
dump - print local variables at current location
dump func - print variables local to func
dump text - print all variables in text
Name: edit
edit - edit file at current location
edit identifier - edit file defining identifier
edit file - edit at top of file
edit "file":line - edit at specified location
edit func - edit file containg func
edit line - edit current file at line
edit workspace - append Workspace definitions to file and invoke editor
Name: email
email - send e-mail to CenterLine Software
email file - send file to CenterLine Software
Name: english
english expr - describe type of expression in english
english var - describe type of variable in english
Name: fg
fg - return to ObjectCenter after suspend
Name: file
file - display the current location
file filename - set the current location
Name: help
help - display usage information about all commands
help command - display usage information about command
Name: history
history - list previously entered input
history n - list n previous lines of input
Name: ignore
ignore - show all ignored signals
ignore num - ignore signal num
ignore name - ignore signal name
Name: info
info var - display type, size of variable
info addr - display type, size of address
info lvalue - display type, size of lvalue
Name: instrument
instrument - list names of instrumented files
instrument all - add run-time checking for all object code files
instrument file.o - add run-time checking for file.o
Name: keybind
keybind - display current key bindings
keybind key - display binding for key
keybind key key-function - set binding to function
keybind key key-command arguments  - set binding to command
Name: link
link - link symbols from libraries
link -list - echoes library link order to Workspace
link func - link symbols used by func
link var - link symbols used by var
Name: list
list - display source code lines
list file - display from top of file
list "file":line - display specified line
list func - display from top of function
list identifier - display defining location
list line - display from line
list -n - display n lines before current location
list line line - display between lines
Name: load
load [switches] file - load source, object, library, or project file
 +k[=<filename>] - save and restore header files from repository <filename>
 -C - parse specified file as C code
 -CXX - parse specified file as C++ code
 -dd=off | -dd=on - turn demand-driven generation off/on (default is on)
 -D<name>[=<def>] - define <name> as 1 or as <def> if supplied
 -G - ignore debugging information produced by -g
 -hdrepos=<dir> - look in <dir> for filename supplied with +k
 -I<dir> - add <dir> to list to search for include files
 -L<dir> - add <dir> to list to search for libraries 
 -l<x> - find and load lib<x>.a
 -U<name> - undefine macro <name>
 -w - suppress warnings but report errors
Name: load_header
load_header [switches] file - load header file(s) in source
Name: make
make - call make with default target
make target - call make with specified target
Name: man
man - display summary manual page
man item - display man page on command, function, or topic
Name: next
next - step execution by line, do not enter functions
next n - step n lines
Name: print
print var - display the value of the variable
print expr - display the value of the expression
Name: printenv
printenv - list all environment variables
printenv variable - display variable and its value
Name: printopt
printopt - list all options
printopt var - display option, its value, and description
Name: proto
proto all - send prototypes for all loaded files to output
proto user - send prototypes for user files to output
proto file - send prototypes for the named file to output
Name: quit
quit - quit with session-saving prompts
quit force - quit immediately without session-saving
quit project - quit, saving into project file ocenter.proj
quit project file - quit, saving into named project file
Name: reinit
reinit - reinitialize all variables
reinit variable - reinitialize variable
Name: rename
rename - list all renamed functions
rename old new - rename function from old to new
Name: rerun
rerun - execute program, clear old arguments
rerun args - execute program with new arguments
Name: reset
reset - return to a previous break level
reset n - return to break level n
reset -n - return to break level n levels up from current break level
Name: run
run - execute program, reuse old arguments
run args - execute program with arguments
Name: save
save - save session into project file ocenter.proj
save file - save session into named project file
Name: set
set var=exp - evaluates expression and assigns it to variable
Name: setenv
setenv - list all environment variables
setenv variable - set variable to empty string or TRUE
setenv variable value - set variable to value
Name: setopt
setopt - list all options that are set
setopt option - set option to TRUE, 1, or empty string
setopt option value - set option to value
Name: sh
sh - execute a Bourne subshell
sh args - execute Bourne shell with -c and arguments
Name: shell
shell - execute shell specified by "shell" option
shell args - execute subshell with arguments
Name: source
source file - read commands from file
Name: start
start - execute without initializing variables
start args - execute with arguments
Name: status
status - display all breakpoints and actions
Name: step
step - step execution by statement, enter functions
step n - step n statement, enter functions
Name: stepout
stepout - continue execution until function returns
Name: stop
stop - stop execution immediately
stop [at] line - set breakpoint at line
stop [at] "file":line - set breakpoint at specified location
stop [in] func - set breakpoint on first line of function
stop [on] var - set breakpoint on variable
stop [on] addr - set breakpoint on address
stop [on] lvalue - set breakpoint on lvalue
Name: suppress
suppress - show all suppressed violations
suppress n - suppress reporting everywhere
suppress n [at] line - suppress at line
suppress n [at] "file":line - suppress at specified location
suppress n [in] directory - suppress in directory
suppress n [in] file - suppress while in file
suppress n [on] func - suppress on function call
suppress n in func - suppress while in function
suppress n [in] lib - suppress while in lib(module)
suppress n [on] identifier - suppress on indentifier
suppress save - save all suppressions to a file
suppress save filename - save all suppressions to a filename
Name: suspend
suspend - suspend ObjectCenter and return to shell
Name: swap
swap func - replace function with its source/object counterpart
swap file - replace file with its source/object counterpart
Name: touch
touch var - mark variable as initialized and valid
touch addr - mark address as initialized and valid
touch expr - mark value of expression as initialized and valid
touch size at var - mark variable for size bytes
touch size at addr - mark address for size bytes
touch size at expr - mark value of expression for size bytes
Name: trace
trace - trace execution by line
trace [in] func - trace execution in func
Name: unalias
unalias name - delete alias
Name: uninstrument
uninstrument - prompts you to remove instrument info for one file at a time
uninstrument all - remove run-time checking for all object code files
uninstrument file.o - remove run-time checking for file.o
Name: unload
unload - prompts you to interactively unload all files and libraries
unload file...  - unload file
unload func - unload file containing func
unload library - detaches library and unloads linked individual modules
unload library(module) - unload module of library
unload templates - unload template instantiation modules
unload user - unload source and object code files currently loaded 
unload all - unload all files, including libraries and modules linked from libraries
unload workspace - unload definitions entered in the Workspace.
Name: unres
unres - list all undefined symbols
unres func - list undefined symbols used by func
unres var - list undefined symbols used by var
Name: unsetenv
unsetenv var - unset environment variable
Name: unsetopt
unsetopt - list all unset options
unsetopt option - unset option
Name: unsuppress
unsuppress - unsuppress violations one by one
unsuppress n - unsuppress everywhere
unsuppress n [at] line - unsuppress at line
unsuppress n [at] "file":line - unsuppress at specified location
unsuppress n [in] directory - unsuppress while in directory
unsuppress n [in] file - unsuppress while in file
unsuppress n [on] function - unsuppress on function call
unsuppress n in function - unsuppress while in function
unsuppress [in] lib(module) - unsuppress in specified module of specified library
unsuppress n [on] identifier - unsuppress on identifier
Name: up
up - move up the execution stack
up n - move up n levels
Name: use
use - display directories in search list
use path - set search list to path
Name: whatis
whatis name - display all uses of name
Name: where
where - display the execution stack
where n - display top n levels of stack
Name: whereami
whereami - display execution and scope locations
Name: whereis
whereis name - display where name is declared or defined
Name: xref
xref func - cross-reference function
xref var - cross-reference variable
#ifdef CPLUSPLUS
Name: cxxmode
cxxmode - switches to C++ mode
Name: cmode
cmode - switches to C mode
Name: browse_base
browse_base class - displays base classes
Name: browse_member_functions
browse_member_functions class - displays member functions of class
Name: browse_data_members
browse_data_members class - displays data members of class
Name: browse_derived
browse_derived class - displays derived classes of class
Name: browse_class
browse_class class - displays class
Name: classinfo
classinfo class - displays class information
classinfo       - displays information about all loaded classes
Name: browse_friends
browse_friends class - displays friends of class
Name: list_classes
list_classes - lists defined classes
Name: construct
construct - calls constructors for static class instances
construct file - calls constructors for file
Name: destruct
destruct - calls destructors for static class instances
destruct file - calls destructors for static class instances of specified file
Name: expand
expand simple_statement - displays what functions could be called
expand compound_statement - displays what functions could be called 
#endif

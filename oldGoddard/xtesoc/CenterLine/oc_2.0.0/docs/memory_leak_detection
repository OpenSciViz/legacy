NAME

	memory leak detection

	Memory leak detection identifies potential memory leaks by 
	reporting on the memory that the program allocates while running 
	and fails to free before exiting.
	
	The memory leak detection report lists leaks by the size of the 
	memory allocated and identifies the stack trace, indicating where 
	the program allocated the memory. In addition, it shows the number 
	of times the leak occurred there.

	Memory leak detection may include pointers to memory that were not 
	freed because the program was exiting. A rule of thumb is that if 
	an allocation is reported more than once, it probably is worth 
	looking at since it may be a *real* leak.


How to use memory leak detection

	To use memory leak detection, follow these instructions:

	1. Enter the following in the Workspace in the Main Window of 
	   ObjectCenter: 

		setopt mem_trace n

	    The letter n represents the maximum number of stack trace 
            levels to report. 

	2.  Run the program in ObjectCenter. Running the program in 
	    ObjectCenter creates a file with memory leak detection 
	    information when the program exits.
     
File with memory leak information

     For each possible leak, the report file contains two or more lines. 
     The first line has this format:
	
     	nbytes [size, count]

     where nbytes is the total number of bytes, size is the memory
     size allocated each time, and count is the number of times
     the potential leak occurs. Each remaining line contains one
     level of stack trace in this format:

     	<tab> function <tab> file <tab> line number


Example of file from a simple test

	Here is a file from a simple test. The file includes a detailed 
	explanation of the contents of the report.

	# This file contains a listing of possible memory
	# leaks : Wed Jan 19 19:00:00 1993
	#
	# There are 7 possible memory leaks, totaling 19 bytes.
	# The format of this report is as follows:
	# For each possible leak there are two or more lines. The first
	# has the format:
	# nBytes [size, count]
	# where 'nBytes' is the total number of bytes, 'size' is the
	# size allocated each time, and 'count' is how many times it was done.
	# Each remaining line for the leak contains one level of stack trace.
	# with the format:
	# <tab> Function <tab> file <tab> line #
	# (for as many levels of stack trace as requested).
	#
	
	2       [1, 2]
          	main /s/users/smith/Temp/mem2.c 13

	2       [2, 1]
          	main /s/users/smith/Temp/mem2.c 13

	6       [3, 2]
          	main /s/users/smith/Temp/mem2.c 13

	4       [4, 1]
          	main /s/users/smith/Temp/mem2.c 13

	5       [5, 1]
          	main /s/users/smith/Temp/mem2.c 13


Naming your memory detection file

	By default, the memory detection file is called mem.leak and 
	appears in ObjectCenter's current directory. You can use a 
	different fllename by setting the environment variable 
	CENTERLINE_LEAK_FILE to the name you want to use. You must 
	do this before invoking ObjectCenter.


You must use ObjectCenter's version of certain functions

	To use ObjectCenter's memory leak detection, you must use 
	ObjectCenter's version of these functions: malloc(), calloc(), 
	realloc(), and free(). You cannot substitute your own versions 
	of them.


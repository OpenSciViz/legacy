<!-- CenterLine rev.pri Print Stylesheet for Revelatory Elements

   cldoc/collection/styles/rev.pri -->


<sheet >



<?INSTED COMMENT: UNGROUPED STYLES FOLLOW>

<style name="#ANNOT">
	<right-indent>	+=10	</>
	<break-before>	None	</>
	<break-after>	None	</>
</style>

<style name="#DEFAULT">
	<font-family>	helvetica	</>
	<font-size>	10	</>
	<break-before>	Line	</>
	<icon-type>	raster	</>
</style>

<style name="#FOOTER">
	<font-family>	times	</>
	<font-size>	10	</>
	<left-indent>	60	</>
	<justification>	Center	</>
	<hrule>	Before	</>
	<text-before>   Page pagenum()</>
</style>

<style name="#HEADER">
	<font-family>	times	</>
	<font-size>	10	</>
	<left-indent>	60	</>
	<justification>	Center	</>
	<break-before>	Line	</>
	<text-before>  content (typechild(TITLE,typechild(BOOK,root())))</>
</style>

<style name="#ROOT">
	<break-before>	Line	</>
</style>

<style name="#SDATA">
	<font-family>	attr(font)	</>
	<font-weight>	Medium	</>
	<font-slant>	Roman	</>
	<character-set>	attr(charset)	</>
	<break-before>	None	</>
	<text-before>char(attr(code))</>
</style>

<style name="#TAGS">
	<font-weight>	Bold	</>
</style>

<style name="ANCHOR">
	<break-before>	None	</>
</style>

<style name="BKTITLE">
	<font-slant>	Italics	</>
	<break-before>	None	</>
	<break-after>	None	</>
</style>

<style name="BOOK,TITLE">
	<font-weight>	Bold	</>
	<font-size>	16	</>
	<left-indent>	-=10	</>
	<line-spacing>	20	</>
	<space-before>	6	</>
	<justification>	Left	</>
</style>

<style name="BOOKTITLE">
	<font-slant>	Italics	</>
	<break-before>	None	</>
	<break-after>	None	</>
</style>

<style name="BULLIST">
	<font-size>	10	</>
	<left-indent>	+=20	</>
	<break-before>	Line	</>
</style>

<style name="BULLIST,LISTITEM">
	<first-indent>	-20	</>
	<line-spacing>	14	</>
	<space-before>	6	</>
	<text-before>�</>
</style>

<style name="BULLIST,LISTITEM,#TEXT-BEFORE">
	<font-family>	symbol	</>
	<font-size>	10	</>
	<foreground>	DarkGreen	</>
</style>

<style name="CAUTION">
	<space-before>	16	</>
	<break-before>	Line	</>
	<break-after>	None	</>
</style>

<style name="CAUTION,BULLIST,LISTITEM">
	<font-size>	10	</>
	<left-indent>	+=35	</>
	<first-indent>	-=15	</>
	<space-before>	4	</>
	<space-after>	if(islast(), 12,	</>
	<text-before>�</>
</style>

<style name="CAUTION,PARA">
	<left-indent>	+=60	</>
	<first-indent>	if(isfirst(), -=40,  )	</>
	<line-spacing>	14	</>
	<space-after>	if(islast(), 12, 6)	</>
	<hrule>	None	</>
	<break-before>	None	</>
	<break-after>	Line	</>
	<text-before>if(isfirst(), CAUTION,  )</>
</style>

<style name="CAUTION,PARA,#TEXT-BEFORE">
	<font-weight>	Bold	</>
	<foreground>	DarkGreen	</>
	<justification>	Left	</>
</style>

<style name="CHAPTER">
	<break-before>	page	</>
</style>

<style name="CHAPTER,TITLE">
	<font-family>	helvetica	</>
	<font-weight>	Bold	</>
	<font-size>	16	</>
	<left-indent>	-=10	</>
	<space-before>	20	</>
	<space-after>	8	</>
	<justification>	Left	</>
	<hide>	Off	</>
	<text-before>Chapter gcnum(ancestor(chapter)):  </>
</style>

<style name="CODE">
	<break-before>	None	</>
	<break-after>	None	</>
</style>

<style name="COPYRT">
	<icon-position>	Right	</>
	<hide>	Children	</>
	<script>	ebt-reveal stylesheet="rev.pri" title="Copyright Information"	</>
	<icon-type>	copyrt	</>
</style>

<style name="COPYRT,TITLE,#TEXT-BEFORE">
	<font-family>	helvetica	</>
	<font-weight>	Bold	</>
	<font-size>	12	</>
	<justification>	Center	</>
</style>

<style name="DESC,TITLE">
	<font-weight>	Bold	</>
	<font-size>	12	</>
	<left-indent>	-=10	</>
	<space-before>	6	</>
	<justification>	Left	</>
	<text-before>Description</>
</style>

<style name="DESC,TITLE,#TEXT-BEFORE">
	<font-family>	helvetica	</>
	<font-weight>	Bold	</>
	<font-size>	12	</>
	<left-indent>	-=10	</>
	<space-before>	16	</>
	<space-after>	6	</>
	<justification>	Left	</>
</style>

<style name="EMPHASIS">
	<font-weight>	Medium	</>
	<font-slant>	Italics	</>
	<break-before>	None	</>
</style>

<style name="ENTRY">
	<left-indent>	+=mult(tableinfo(cals,left-indent),.8)	</>
	<width>	mult(tableinfo(cals,width,10),0.8)	</>
	<justification>	tableinfo(cals, justification)	</>
	<column>	True	</>
</style>

<style name="EXAMPLE">
	<font-family>	courier	</>
	<left-indent>	+=10	</>
	<line-spacing>	14	</>
	<space-before>	4	</>
	<justification>	Verbatim	</>
	<break-before>	None	</>
	<break-after>	None	</>
</style>

<style name="EXREF">
	<break-before>	None	</>
	<break-after>	None	</>
</style>

<style name="EXSECT2,TITLE">
	<font-family>	helvetica	</>
	<font-weight>	Bold	</>
	<font-size>	12	</>
	<left-indent>	-=10	</>
	<space-before>	16	</>
	<space-after>	6	</>
	<justification>	Left	</>
	<text-before>Example</>
</style>

<style name="FIGURE">
	<justification>	Right	</>
	<icon-position>	Off	</>
	<hide>	Off	</>
	<text-before>Figure gcnum()</>
</style>

<style name="FTABLE">
	<font-family>	times	</>
	<left-indent>	60	</>
	<right-indent>	60	</>
	<space-before>	10	</>
	<space-after>	10	</>
	<break-before>	line	</>
</style>

<style name="FTABLE,#TEXT-BEFORE">
	<font-family>	helvetica	</>
	<font-weight>	Bold	</>
	<font-size>	10	</>
</style>

<style name="FTABLE,TITLE">
	<font-family>	helvetica	</>
	<font-weight>	Bold	</>
	<font-size>	10	</>
	<justification>	Left	</>
	<text-before>Table gcnum(ancestor()):  </>
</style>

<style name="GRAPHIC">
	<right-indent>	20	</>
	<icon-position>	Right	</>
	<script>	ebt-raster filename=attr(FILENAME) title="attr(FIGNAME)"	</>
	<icon-type>	raster	</>
</style>

<style name="HARDCOPY">
	<hide>	All	</>
	<break-before>	None	</>
</style>

<style name="INPUT">
	<font-weight>	Bold	</>
	<break-before>	None	</>
	<break-after>	None	</>
</style>

<style name="LISTITEM">
	<left-indent>	+=10	</>
	<first-indent>	-10	</>
	<space-before>	6	</>
</style>

<style name="LNGEXAMP">
	<left-indent>	+=10	</>
	<space-before>	6	</>
	<break-before>	Line	</>
</style>

<style name="LNGEXAMP,EXMPITEM">
	<font-family>	courier	</>
	<line-spacing>	14	</>
	<justification>	Verbatim	</>
</style>

<style name="NAME">
	<font-weight>	Bold	</>
	<break-before>	None	</>
</style>

<style name="NOTE">
	<space-before>	16	</>
	<break-before>	Line	</>
	<break-after>	None	</>
</style>

<style name="NOTE,BULLIST,LISTITEM">
	<font-size>	10	</>
	<left-indent>	+=35	</>
	<first-indent>	-=15	</>
	<space-before>	4	</>
	<space-after>	if(islast(), 12,	</>
	<text-before>�</>
</style>

<style name="NOTE,PARA">
	<left-indent>	+=40	</>
	<first-indent>	if(isfirst(), -=40,  )	</>
	<line-spacing>	14	</>
	<space-after>	if(islast(), 12, 6)	</>
	<hrule>	None	</>
	<break-before>	None	</>
	<break-after>	Line	</>
	<text-before>if(isfirst(), NOTE,  )</>
</style>

<style name="NOTE,PARA,#TEXT-BEFORE">
	<font-family>	helvetica	</>
	<font-weight>	Bold	</>
	<foreground>	DarkGreen	</>
	<justification>	Left	</>
</style>

<style name="OPTIONS,TITLE">
	<font-family>	helvetica	</>
	<font-weight>	Bold	</>
	<font-size>	12	</>
	<left-indent>	-=10	</>
	<space-before>	16	</>
	<space-after>	6	</>
	<justification>	Left	</>
	<text-before>Options</>
</style>

<style name="ORDLIST">
	<font-size>	10	</>
	<left-indent>	+=20	</>
	<break-before>	Line	</>
</style>

<style name="ORDLIST,LISTITEM">
	<first-indent>	-20	</>
	<line-spacing>	14	</>
	<space-before>	6	</>
	<text-before>cnum()   </>
</style>

<style name="ORDLIST,LISTITEM,#TEXT-BEFORE">
	<font-weight>	Bold	</>
	<foreground>	DarkGreen	</>
</style>

<style name="OUTPUT">
	<font-family>	courier	</>
	<font-weight>	Medium	</>
	<break-before>	None	</>
	<break-after>	None	</>
</style>

<style name="OVERVIEW,TITLE">
	<font-family>	helvetica	</>
	<font-weight>	Bold	</>
	<font-size>	12	</>
	<left-indent>	-=10	</>
	<space-before>	16	</>
	<space-after>	6	</>
	<justification>	Left	</>
	<hide>	Off	</>
	<text-before>Overview of steps</>
</style>

<style name="PARA">
	<font-family>	times	</>
	<font-size>	10	</>
	<line-spacing>	14	</>
	<space-before>	6	</>
	<hrule>	None	</>
</style>

<style name="PART">
	<break-before>	page	</>
</style>

<style name="PART,CHAPTER,TITLE">
	<font-family>	helvetica	</>
	<font-weight>	Bold	</>
	<font-size>	14	</>
	<left-indent>	-=10	</>
	<space-before>	12	</>
	<space-after>	6	</>
</style>

<style name="PART,TITLE">
	<font-family>	helvetica	</>
	<font-weight>	Bold	</>
	<font-size>	16	</>
	<left-indent>	-=10	</>
	<space-before>	24	</>
	<space-after>	24	</>
	<text-before>Part format (cnum(parent(PART)), ROMAN):   </>
</style>

<style name="PARTINTR">
	<left-indent>	-=10	</>
</style>

<style name="PREFACE,TITLE">
	<font-weight>	Bold	</>
	<font-size>	12	</>
	<left-indent>	-=10	</>
	<space-before>	6	</>
	<justification>	Left	</>
	<hide>	Text	</>
</style>

<style name="PROCED">
	<left-indent>	+=20	</>
	<break-before>	Line	</>
</style>

<style name="PROCED,BULLIST">
	<font-size>	10	</>
	<left-indent>	+=20	</>
	<break-before>	Line	</>
</style>

<style name="PROCED,STEP">
	<left-indent>	20	</>
	<first-indent>	-20	</>
	<space-before>	5	</>
	<text-before>cnum()</>
</style>

<style name="PROCED,STEP,#TEXT-BEFORE">
	<font-weight>	Bold	</>
	<foreground>	DarkGreen	</>
</style>

<style name="REFSECT1">
	<break-after>	page	</>
</style>

<style name="REFSECT1,SECTION2,SYNOPSIS">
	<font-weight>	Demibold	</>
	<left-indent>	0	</>
	<space-before>	4	</>
	<text-before>Synopsis:  </>
</style>

<style name="REFSECT1,SECTION2,SYNOPSIS,#TEXT-BEFORE">
	<font-weight>	Demibold	</>
</style>

<style name="REFSECT1,TITLE">
	<font-family>	helvetica	</>
	<font-weight>	Bold	</>
	<font-size>	14	</>
	<left-indent>	-=10	</>
	<space-before>	18	</>
	<space-after>	6	</>
	<justification>	Left	</>
	<hrule>	After	</>
</style>

<style name="RESTR2,TITLE">
	<font-family>	helvetica	</>
	<font-weight>	Bold	</>
	<font-size>	12	</>
	<left-indent>	-=10	</>
	<space-before>	16	</>
	<space-after>	6	</>
	<justification>	Left	</>
	<text-before>Restrictions</>
</style>

<style name="ROW">
	<space-before>	9	</>
	<break-before>	Line	</>
</style>

<style name="SECTION1">
	<hrule>	None	</>
	<break-after>	page	</>
</style>

<style name="SECTION1,TITLE">
	<font-family>	helvetica	</>
	<font-weight>	Bold	</>
	<font-size>	14	</>
	<left-indent>	-=10	</>
	<space-before>	18	</>
	<space-after>	6	</>
	<justification>	Left	</>
	<hrule>	After	</>
</style>

<style name="SECTION1,TITLE,HRULE">
	<font-weight>	Bold	</>
	<foreground>	DarkGreen	</>
</style>

<style name="SECTION2,TITLE">
	<font-family>	helvetica	</>
	<font-weight>	Bold	</>
	<font-size>	12	</>
	<left-indent>	-=10	</>
	<space-before>	16	</>
	<space-after>	6	</>
	<justification>	Left	</>
</style>

<style name="SECTION3,TITLE">
	<font-family>	helvetica	</>
	<font-weight>	Demibold	</>
	<font-size>	10	</>
	<space-before>	14	</>
	<justification>	Left	</>
	<hrule>	None	</>
</style>

<style name="SECTION4,TITLE">
	<font-family>	helvetica	</>
	<font-weight>	Demibold	</>
	<font-size>	10	</>
	<space-before>	14	</>
	<justification>	Left	</>
	<hrule>	None	</>
</style>

<style name="SEEALSO2,TITLE">
	<font-family>	helvetica	</>
	<font-weight>	Bold	</>
	<font-size>	12	</>
	<left-indent>	-=10	</>
	<space-before>	18	</>
	<space-after>	6	</>
	<justification>	Left	</>
	<text-before>See also</>
</style>

<style name="SPACE">
	<font-size>	14	</>
	<break-before>	Line	</>
	<break-after>	Line	</>
</style>

<style name="SUP">
	<font-size>	7	</>
	<break-after> 	None	</>
	<break-before>	None	</>
	<vertical-offset> 4	</>
</style>

<style name="SWITCH2,TITLE">
	<font-family>	helvetica	</>
	<font-weight>	Bold	</>
	<font-size>	12	</>
	<left-indent>	-=10	</>
	<space-before>	6	</>
	<justification>	Left	</>
	<text-before>Switches</>
</style>

<style name="SYMBOL">
	<break-before>	None	</>
	<break-after>	None	</>
</style>

<style name="SYNOPSIS">
	<font-weight>	Demibold	</>
	<left-indent>	+=10	</>
	<space-before>	4	</>
</style>

<style name="SYNTAX,TITLE">
	<font-family>	helvetica	</>
	<font-weight>	Bold	</>
	<font-size>	12	</>
	<left-indent>	-=10	</>
	<space-before>	6	</>
	<justification>	Left	</>
	<text-before>Syntax</>
</style>

<style name="TABLE">
	<left-indent>	60	</>
	<right-indent>	60	</>
	<hide>	Off	</>
</style>

<style name="TBODY,ROW,ENTRY,ORDLIST,LISTITEM">
	<space-before>	6	</>
	<text-before>cnum() </>
</style>

<style name="TBODY,ROW,ENTRY,PARA,ORDLIST,LISTITEM">
	<space-before>	6	</>
	<text-before>cnum() </>
</style>

<style name="THEAD">
	<font-weight>	Bold	</>
	<space-before>	6	</>
	<space-after>	6	</>
</style>

<style name="THEAD,ROW,ENTRY,PARA">
	<font-family>	helvetica	</>
	<font-weight>	Bold	</>
	<font-size>	10	</>
	<space-before>	6	</>
	<space-after>	6	</>
	<hrule>	None	</>
</style>

<style name="TIP">
	<font-family>	times	</>
	<font-weight>	Medium	</>
	<left-indent>	60	</>
	<right-indent>	60	</>
	<space-before>	6	</>
	<space-after>	6	</>
	<justification>	Left	</>
	<break-before>	Line	</>
	<text-before>TIP:  content(child(TITLE))</>
</style>

<style name="TIP,#TEXT-BEFORE">
	<font-family>	helvetica	</>
	<font-weight>	Bold	</>
	<font-size>	10	</>
	<space-before>	6	</>
	<space-after>	6	</>
	<justification>	Left	</>
</style>

<style name="TIP,TITLE">
	<font-family>	helvetica	</>
	<font-weight>	Bold	</>
	<font-size>	10	</>
	<space-before>	6	</>
	<justification>	Left	</>
	<hide>	Children	</>
</style>

<style name="TITLE">
	<font-family>	helvetica	</>
	<font-weight>	Bold	</>
	<font-size>	14	</>
	<left-indent>	-=10	</>
	<space-before>	6	</>
	<justification>	Left	</>
	<hide>	Text	</>
	<text-before>Copyright Notice</>
</style>

<style name="TSKSECT1">
	<break-after>	page	</>
</style>

<style name="TSKSECT1,TITLE">
	<font-family>	helvetica	</>
	<font-weight>	Bold	</>
	<font-size>	14	</>
	<left-indent>	-=10	</>
	<line-spacing>	14	</>
	<space-before>	18	</>
	<space-after>	6	</>
	<justification>	Left	</>
	<hrule>	After	</>
	<text-before>Task:  </>
</style>

<style name="USAGE2,TITLE">
	<font-family>	helvetica	</>
	<font-weight>	Bold	</>
	<font-size>	12	</>
	<left-indent>	-=10	</>
	<space-before>	16	</>
	<space-after>	6	</>
	<justification>	Left	</>
	<text-before>Usage</>
</style>

<style name="VARIABLE">
	<font-weight>	Medium	</>
	<font-slant>	Italics	</>
	<break-before>	None	</>
	<break-after>	None	</>
</style>

<style name="VEFIGURE">
	<justification>	Right	</>
	<icon-position>	Off	</>
	<hide>	Off	</>
	<text-before>Figure gcnum(ancestor(chapter))-gcnum()</>
</style>

<style name="VEFIGURE,TITLE">
	<font-family>	helvetica	</>
	<font-weight>	Bold	</>
	<font-size>	12	</>
	<space-before>	6	</>
	<justification>	Left	</>
	<hide>	Text	</>
</style>

<style name="VEFTABLE">
	<font-family>	times	</>
	<font-size>	10	</>
	<left-indent>	+=15	</>
	<space-before>	10	</>
	<space-after>	10	</>
	<icon-position>	Inline	</>
	<hide>	Children	</>
	<break-before>	line	</>
	<script>	ebt-reveal stylesheet="rev.pri" hscroll="true"	</>
	<icon-type>	table	</>
	<text-before>Table gcnum(ancestor(chapter))-gcnum()</>
</style>

<style name="VEFTABLE,TITLE">
	<font-family>	helvetica	</>
	<font-weight>	Bold	</>
	<font-size>	12	</>
	<space-before>	6	</>
	<justification>	Left	</>
	<hide>	Off	</>
</style>

<style name="XREF">
	<break-before>	None	</>
	<break-after>	None	</>
</style>



</sheet>

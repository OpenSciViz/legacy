<!-- Template for new revelatory Stylesheets

  Copyright 1994, Electronic Book Technologies.  All rights reserved.
-->


<sheet >



<?INSTED COMMENT: UNGROUPED STYLES FOLLOW>

<style name="#ROOT">
	<break-before>	Line	</>
	<font-size>	14	</>
</style>

<style name="#SDATA">
	<font-family>	attr(font)	</>
	<font-weight>	Medium	</>
	<font-slant>	Roman	</>
	<character-set>	attr(charset)	</>
	<break-before>	None	</>
	<text-before>char(attr(code))</>
</style>

<style name="#TAGS">
	<font-weight>	Bold	</>
</style>

<style name="BOOKTITLE">
	<break-before>	Line	</>
	<font-family>	attr(font)	</>
	<font-weight>	Bold</>
	<font-slant>	Roman	</>
	<font-size>	24	</>
	<character-set>	attr(charset)	</>
	<space-before>	10	</>
	<space-after>	32	</>
	<foreground>	blue	</>
</style>


<style name="ENT">
	<font-size>	14	</>	
	<break-before>	Line	</>
	<foreground>	black	</>
</style>

<style name="ENTLIST">
	<font-family>	Times New Roman	</>
</style>

<style name="P">
        <font-family>   MS Sans Serif        </>
	<break-before>  line        </>
        <font-weight>   Medium </>
        <font-slant>    Roman</>
	<space-before>  10</>
</style>

<style name="ENTSET">
	<space-before>	10	</>
	<foreground>	blue	</>
</style>

<style name="GLYPH">
	<left-indent>	80	</>
</style>

<style name="TITLE">
	<font-weight>	Bold	</>
	<font-size>	16	</>
	<space-after>	5	</>
</style>




</sheet>

























































<!--Start Entity Definitions
    This is the fthead.unx file for the imi dtd.
    It contains entities for the fulltext.v and popup.rev 
    stylesheets on unix.  This file
    should be concatenated with ftbody.all to create
    the fulltext.v file on the unix platform

End Entity Definitions 
Start StyleSheet Body  -->

<!ENTITY	address.font	CDATA	"helvetica"	>
<!ENTITY	art.color	CDATA	"red"	>
<!ENTITY	booktitle.color	CDATA	"medium sea green"	>
<!ENTITY	booktitle.font	CDATA	"lucida"	>
<!ENTITY	booktitle.font-size	CDATA	"24"	>
<!ENTITY	booktitle.font-slant	CDATA	"roman"	>
<!ENTITY	booktitle.font-weight	CDATA	"medium"	>
<!ENTITY	booktitle.line-spacing	CDATA	"26"	>
<!ENTITY	booktitle.space-after	CDATA	"4"	>
<!ENTITY	booktitle.space-before	CDATA	"12"	>
<!ENTITY	frontmatter.font	CDATA	"helvetica"	>
<!ENTITY	genlist.first-indent	CDATA	"-=10"	>
<!ENTITY	genlist.left-indent	CDATA	"+=50"	>
<!ENTITY	genlist2.first-indent	CDATA	"-=10"	>
<!ENTITY	genlist2.left-indent	CDATA	"+=80"	>
<!ENTITY	heading1.color	CDATA	"medium sea green"	>
<!ENTITY	heading1.font	CDATA	"lucida"	>
<!ENTITY	heading1.font-size	CDATA	"21"	>
<!ENTITY	heading1.font-weight	CDATA	"BOLD"	>
<!ENTITY	heading1.line-spacing	CDATA	"22"	>
<!ENTITY	heading1.space-after	CDATA	"4"	>
<!ENTITY	heading1.space-before	CDATA	"12"	>
<!ENTITY	heading2.color	CDATA	"blue"	>
<!ENTITY	heading2.font	CDATA	"lucida"	>
<!ENTITY	heading2.font-size	CDATA	"18"	>
<!ENTITY	heading2.font-weight	CDATA	"bold"	>
<!ENTITY	heading2.line-spacing	CDATA	"20"	>
<!ENTITY	heading2.space-after	CDATA	"4"	>
<!ENTITY	heading2.space-before	CDATA	"10"	>
<!ENTITY	heading3.color	CDATA	"blue violet"	>
<!ENTITY	heading3.font	CDATA	"lucida"	>
<!ENTITY	heading3.font-size	CDATA	"14"	>
<!ENTITY	heading3.font-slant	CDATA	"italics"	>
<!ENTITY	heading3.font-weight	CDATA	"BOLD"	>
<!ENTITY	heading3.line-spacing	CDATA	"16"	>
<!ENTITY	heading3.space-after	CDATA	"4"	>
<!ENTITY	heading3.space-before	CDATA	"6"	>
<!ENTITY	heading4.color	CDATA	"red"	>
<!ENTITY	heading4.font	CDATA	"lucida"	>
<!ENTITY	heading4.font-size	CDATA	"14"	>
<!ENTITY	heading4.font-weight	CDATA	"bold"	>
<!ENTITY	heading4.line-spacing	CDATA	"16"	>
<!ENTITY	heading4.space-before	CDATA	"6"	>
<!ENTITY	heading5.color	CDATA	"blue"	>
<!ENTITY	heading5.font	CDATA	"lucida"	>
<!ENTITY	heading5.font-size	CDATA	"14"	>
<!ENTITY	heading5.font-weight	CDATA	"bold"	>
<!ENTITY	heading5.line-spacing	CDATA	"16"	>
<!ENTITY	heading5.space-before	CDATA	"6"	>
<!ENTITY	heading6.color	CDATA	"black"	>
<!ENTITY	heading6.font	CDATA	"lucida"	>
<!ENTITY	heading6.font-size	CDATA	"12"	>
<!ENTITY	heading6.font-weight	CDATA	"bold"	>
<!ENTITY	heading6.left-indent	CDATA	"0"	>
<!ENTITY	heading6.line-spacing	CDATA	"14"	>
<!ENTITY	heading6.space-after	CDATA	"2"	>
<!ENTITY	heading6.space-before	CDATA	"4"	>
<!ENTITY	hottext.fontfam	CDATA	"new century schoolbook"	>
<!ENTITY	hottext.foreground	CDATA	"medium sea green"	>
<!ENTITY	hottext.slant	CDATA	"Roman"	>
<!ENTITY	hottext.weight	CDATA	"bold"	>
<!ENTITY	revchgbw	CDATA	"if(attr(revision),under,)"	>
<!ENTITY	revchgcol	CDATA	"if(attr(revision),blueviolet,)"	>
<!ENTITY	std.font	CDATA	"new century schoolbook"	>
<!ENTITY	std.font-size	CDATA	"14"	>
<!ENTITY	std.indent	CDATA	"10"	>
<!ENTITY	std.leftindent	CDATA	"10"	>
<!ENTITY	std.line-spacing	CDATA	"16"	>
<!ENTITY	std.para.left-indent	CDATA	"+=30"	>
<!ENTITY	std.para.space-before	CDATA	"6"	>
<!ENTITY	std.rightindent	CDATA	"12"	>
<!ENTITY	subscript	CDATA	"-3"	>
<!ENTITY	subtitle.color	CDATA	"black"	>
<!ENTITY	subtitle.font	CDATA	"lucida"	>
<!ENTITY	subtitle.font-size	CDATA	"14"	>
<!ENTITY	subtitle.leftindent	CDATA	"10"	>
<!ENTITY	subtitle.line-spacing	CDATA	"16"	>
<!ENTITY	tag.color	CDATA	"purple"	>
<!ENTITY	title.color	CDATA	"blue"	>
<!ENTITY	title.font	CDATA	"lucida"	>
<!ENTITY	verbatim.font	CDATA	"courier "	>
<!ENTITY	verbatim.font-size	CDATA	"12"	>
<!ENTITY	verbatim.line-spacing	CDATA	"14"	>
<!ENTITY	verbatim.small-font-size	CDATA	"10"	>

<sheet >



<?INSTED COMMENT: GROUP asistext>

<!--This is a set of group styles that can be used to
   get started on stylesheets.  To use this file, insert
   the file, using a text editor into your stylesheet file
   (for example, fulltext.v), then add styles to the various
   groups.  For example, a level-2 title could go into the
   heading2 group.
-->
<group name="asistext">
	<font-family>	&verbatim.font	</>
	<font-size>	&verbatim.font-size	</>
	<left-indent>	+=30	</>
	<line-spacing>	&verbatim.line-spacing	</>
	<space-after>	if(islast(),10,0)	</>
	<justification>	verbatim	</>
	<break-before>	Line	</>
</group>

<style name="SECTION,XMP,TEMPLATE" group="asistext">
	<select>	TEMPLATE.attr(inline, ancestor('XMP'))	</>
</style>

<style name="TEMPLATE" group="asistext">
	<left-indent>	if(eq(tag(parent()),examples),, +=10)	</>
</style>

<style name="TEMPLATE.NO" group="asistext">
	<left-indent>	if (eq(tag(parent()),examples),+=20, +=30)	</>
	<space-before>	5	</>
	<icon-position>	if(eq(attr(inline, ancestor('XMP')), no), right, off)	</>
	<hide>	if (eq(attr(inline, ancestor('XMP')), no), Children, off)	</>
	<script>	switch(attr(inline, ancestor('XMP')), yes, , no, ebt-reveal stylesheet = fulltext.v target = me() title = "Example Callback Program width=600 height = 500, DEFAULT, )	</>
	<icon-type>	if(eq(attr(inline, ancestor('XMP')), yes), ,alink)	</>
</style>

<style name="TEMPLATE.YES" group="asistext">
	<left-indent>	if (eq(tag(parent()),examples),+=20, +=30)	</>
	<space-before>	5	</>
	<break-before>	Line	</>
</style>



<?INSTED COMMENT: GROUP booktitle>

<group name="booktitle">
	<font-family>	&booktitle.font	</>
	<font-weight>	&booktitle.font-weight	</>
	<font-size>	&booktitle.font-size	</>
	<foreground>	&booktitle.color	</>
	<line-spacing>	&booktitle.line-spacing	</>
</group>

<style name="TITLEPG,TITLE" group="booktitle">
</style>



<?INSTED COMMENT: GROUP frontmatter>

<group name="frontmatter">
	<font-family>	&frontmatter.font	</>
	<break-before>	line	</>
</group>

<style name="AUTHOR" group="frontmatter">
</style>

<style name="COPYRTPG" group="frontmatter">
	<right-indent>	12	</>
	<icon-position>	Right	</>
	<hide>	Children	</>
	<script>	ebt-reveal title="Copyright Notice" stylesheet= "fulltext.v"	</>
	<icon-type>	copyrt	</>
</style>

<style name="DATE" group="frontmatter">
</style>

<style name="NOTICE" group="frontmatter">
</style>

<style name="OWNER" group="frontmatter">
</style>



<?INSTED COMMENT: GROUP genlist>

<!-- This is a first level list  -->
<group name="genlist">
	<left-indent>	&genlist.left-indent	</>
	<first-indent>	&genlist.first-indent	</>
	<space-before>	&std.para.space-before	</>
	<break-before>	Line	</>
</group>

<style name="LIST" group="genlist">
</style>



<?INSTED COMMENT: GROUP genlist2>

<!-- This is a second level list  -->
<group name="genlist2">
	<left-indent>	&genlist2.left-indent	</>
	<first-indent>	&genlist2.first-indent	</>
	<break-before>	Line	</>
</group>



<?INSTED COMMENT: GROUP heading1>

<group name="heading1">
	<font-family>	&heading1.font	</>
	<font-weight>	&heading1.font-weight	</>
	<font-size>	&heading1.font-size	</>
	<foreground>	&heading1.color	</>
	<line-spacing>	&heading1.line-spacing	</>
	<space-before>	&heading1.space-before	</>
</group>

<style name="MODULE,NAME" group="heading1">
</style>



<?INSTED COMMENT: GROUP heading2>

<group name="heading2">
	<font-family>	&heading2.font	</>
	<font-weight>	&heading2.font-weight	</>
	<font-size>	&heading2.font-size	</>
	<foreground>	&heading2.color	</>
	<line-spacing>	&heading2.line-spacing	</>
	<space-before>	&heading2.space-before	</>
</group>

<style name="TITLE" group="heading2">
</style>



<?INSTED COMMENT: GROUP heading3>

<group name="heading3">
	<font-family>	&heading3.font	</>
	<font-weight>	&heading3.font-weight	</>
	<font-slant>	&heading3.font-slant	</>
	<font-size>	&heading3.font-size	</>
	<foreground>	&heading3.color	</>
	<line-spacing>	&heading3.line-spacing	</>
	<space-before>	&heading3.space-before	</>
	<space-after>	&heading3.space-after	</>
	<hrule>	None	</>
</group>

<style name="REF,BODY,MODULE,OVERVIEW,SECTION,SUB,TITLE" group="heading3">
</style>



<?INSTED COMMENT: GROUP heading4>

<group name="heading4">
	<font-family>	&heading4.font	</>
	<font-weight>	&heading4.font-weight	</>
	<font-size>	&heading4.font-size	</>
	<foreground>	&heading4.color	</>
	<line-spacing>	&heading4.line-spacing	</>
	<space-before>	&heading4.space-before	</>
	<hrule>	Before	</>
</group>



<?INSTED COMMENT: GROUP heading5>

<group name="heading5">
	<font-family>	&heading5.font	</>
	<font-weight>	&heading5.font-weight	</>
	<font-size>	&heading5.font-size	</>
	<foreground>	&heading5.color	</>
	<line-spacing>	&heading5.line-spacing	</>
	<space-before>	&heading5.space-before	</>
	<hrule>	Before	</>
</group>

<style name="FCT,NAME" group="heading5">
</style>

<style name="REL" group="heading5">
	<space-before>	0	</>
	<hrule>	none	</>
</style>



<?INSTED COMMENT: GROUP heading6>

<group name="heading6">
	<font-family>	&heading6.font	</>
	<font-weight>	&heading6.font-weight	</>
	<font-size>	&heading6.font-size	</>
	<foreground>	&heading6.color	</>
	<first-indent>	&heading6.left-indent	</>
	<line-spacing>	&heading6.line-spacing	</>
	<space-before>	&heading6.space-before	</>
	<space-after>	&heading6.space-after	</>
</group>

<style name="ART,#TEXT-BEFORE" group="heading6">
	<foreground>	&art.color	</>
</style>

<style name="AVAIL,#TEXT-BEFORE" group="heading6">
</style>

<style name="COMM,#TEXT-BEFORE" group="heading6">
</style>

<style name="DESC,#TEXT-BEFORE" group="heading6">
</style>

<style name="EXAMPLES,#TEXT-BEFORE" group="heading6">
</style>

<style name="FIELDLIST,#TEXT-BEFORE" group="heading6">
</style>

<style name="INTERNAL,#TEXT-BEFORE" group="heading6">
</style>

<style name="PARMLIST,#TEXT-BEFORE" group="heading6">
</style>

<style name="RET,#TEXT-BEFORE" group="heading6">
</style>

<style name="SEE,#TEXT-BEFORE" group="heading6">
</style>

<style name="SYNTAX,#TEXT-BEFORE" group="heading6">
</style>



<?INSTED COMMENT: GROUP hottext>

<group name="hottext">
	<font-weight>	&hottext.weight	</>
	<foreground>	&hottext.foreground	</>
	<script>	ebt-link root=idmatch(ID,attr(rid)) stylesheet=popup.rev window=new	</>
</group>

<style name="EXTREF" group="hottext">
	<script>	ebt-link window=new book=attr(book) target=idmatch(ID, attr(RID)) stylesheet=fulltext.v	</>
</style>

<style name="P,XREF" group="hottext">
</style>

<style name="PLENTRY,PDEFN,P,XREF" group="hottext">
	<left-indent>	+=1	</>
</style>

<style name="SEE,P,XREF" group="hottext">
	<break-before>	Line	</>
</style>

<style name="SEE,XREF" group="hottext">
	<break-before>	Line	</>
</style>

<style name="SXREF" group="hottext">
	<script>	ebt-link target=idmatch(ID,attr(rid)) view=fulltext	</>
</style>

<style name="XREF" group="hottext">
</style>



<?INSTED COMMENT: GROUP paragraph>

<group name="paragraph">
	<left-indent>	&std.para.left-indent	</>
	<space-before>	&std.para.space-before	</>
	<space-after>	if (islast(), 10, 0)	</>
	<break-before>	Line	</>
</group>

<style name="P" group="paragraph">
</style>

<style name="XMP,P" group="paragraph">
</style>



<?INSTED COMMENT: GROUP subtitle>

<group name="subtitle">
	<font-family>	&subtitle.font	</>
	<font-size>	&subtitle.font-size	</>
	<foreground>	&subtitle.color	</>
	<line-spacing>	&subtitle.line-spacing	</>
	<space-before>	8	</>
	<space-after>	10	</>
</group>

<style name="SUBTITLE" group="subtitle">
</style>



<?INSTED COMMENT: UNGROUPED STYLES FOLLOW>

<!-- Default settings for printing of annotations -->
<style name="#ANNOT">
	<line-spacing>	24	</>
	<break-before>	line	</>
	<break-after>	line	</>
</style>

<style name="#FOOTER">
	<font-family>	&title.font	</>
	<font-weight>	Bold	</>
	<font-slant>	Roman	</>
	<font-size>	10	</>
	<left-indent>	0	</>
	<right-indent>	0	</>
	<width>	500	</>
	<justification>	right	</>
	<text-before>if(gt(cnum(ancestor(MODULE)),0), 'SIT Reference (Beta1)    Page 'pagenum(),)</>
</style>

<style name="#HEADER">
	<font-family>	&title.font	</>
	<font-weight>	Bold	</>
	<font-size>	10	</>
	<left-indent>	0	</>
	<right-indent>	0	</>
	<width>	500	</>
	<justification>	Right	</>
	<text-before>if(gt(cnum(ancestor(MODULE)),0), content(typechild(NAME,parent(FCT))),)</>
</style>

<style name="#ROOT">
	<break-before>	Line	</>
</style>

<style name="#SDATA">
	<font-family>	attr(font)	</>
	<font-weight>	Medium	</>
	<character-set>	attr(charset)	</>
	<text-before>char(attr(code)) </>
</style>

<style name="#TAGS">
	<font-weight>	Bold	</>
	<foreground>	&tag.color	</>
</style>

<style name="ART">
	<space-before>	12	</>
	<break-before>	line	</>
	<script>	ebt-raster filename=attr(filename)	</>
	<inline>	raster filename=switch(platform(),'DOS',var(fig_dir)\\clicons\\strip(attr(filename),right(attr(filename),'1'),'R'),'UNIX',var(fig_dir)/clicons/strip(attr(filename),right(attr(filename),'1'),'R'),'MAC',var(fig_dir):clicons:strip(attr(filename),right(attr(filename),'1'),'R'),'DEFAULT',)	</>
	<text-before>Figure gcnum(). attr(TITLE)</>
</style>

<style name="AVAIL">
	<text-before>Availability:</>
</style>

<style name="C">
	<left-indent>	+=35	</>
	<width>	50	</>
	<select>	C_cnum()	</>
	<column>	True	</>
</style>

<style name="CLBCK">
	<font-size>	12	</>
	<left-indent>	+=10	</>
	<space-before>	4	</>
	<space-after>	4	</>
	<break-before>	Line	</>
</style>

<style name="COMM">
	<text-before>Comments:</>
</style>

<style name="C_1">
	<width>	40	</>
	<justification>	Center	</>
	<column>	True	</>
</style>

<style name="C_2">
	<left-indent>	+=45	</>
	<width>	40	</>
	<justification>	Center	</>
	<column>	True	</>
</style>

<style name="C_3">
	<left-indent>	+=90	</>
	<width>	40	</>
	<justification>	Center	</>
	<column>	True	</>
</style>

<style name="C_4">
	<left-indent>	+=135	</>
	<width>	40	</>
	<justification>	Center	</>
	<column>	True	</>
</style>

<style name="DATATYPE">
	<space-before>	20	</>
</style>

<style name="DESC">
	<text-before>Description:</>
</style>

<style name="EBTDOC">
	<font-family>	&std.font	</>
	<font-size>	&std.font-size	</>
	<left-indent>	&std.leftindent	</>
	<right-indent>	&std.rightindent	</>
	<line-spacing>	&std.line-spacing	</>
	<break-before>	Line	</>
	<title-tag>	FRONT,TITLEPG,TITLE	</>
</style>

<style name="EMPH">
	<select>	EMPH.attr(type)	</>
</style>

<style name="EMPH.BOLD">
	<font-weight>	Bold	</>
</style>

<style name="EMPH.FILE">
	<font-slant>	Italic	</>
</style>

<style name="EMPH.OBJECT">
</style>

<style name="EMPH.PARM">
	<font-family>	&verbatim.font	</>
	<font-size>	&verbatim.font-size	</>
</style>

<style name="EMPY">
	<break-before>	on	</>
</style>

<style name="EXAMPLES">
	<text-before>Examples:</>
</style>

<style name="FCTS">
	<hide>	Off	</>
</style>

<style name="FIELDLIST">
	<left-indent>	20	</>
	<first-indent>	-10	</>
	<space-before>	10	</>
	<break-before>	on	</>
	<text-before>Fields:</>
</style>

<style name="I">
	<font-slant>	Italics	</>
</style>

<style name="INTERNAL">
	<text-before>Internal:</>
</style>

<style name="ITEM,P">
	<left-indent>	+=15	</>
	<space-before>	if (isfirst(), 0,std.para.space-before)	</>
	<break-before>	if (isfirst(), none, line)	</>
</style>

<style name="MARKER">
	<font-family>	symbol	</>
	<font-weight>	Medium	</>
	<break-before>	True	</>
</style>

<style name="MEANING">
	<font-size>	&heading6.font-size	</>
	<left-indent>	160	</>
</style>

<style name="MEANING,P">
	<font-size>	&std.font-size	</>
	<break-before>	if (and(isfirst(), le(length(content(lsibling(parent()))), 15)), None, Line)	</>
</style>

<style name="NAME">
	<font-family>	&address.font	</>
	<font-weight>	Bold	</>
	<font-size>	14	</>
	<line-spacing>	16	</>
	<break-before>	line	</>
</style>

<style name="OVERVIEW">
	<break-before>	line	</>
</style>

<style name="PARMLIST">
	<text-before>Parameters:</>
</style>

<style name="PDEFN">
	<left-indent>	160	</>
</style>

<style name="PDEFN,LIST">
</style>

<style name="PDEFN,P">
	<break-before>	if (and(isfirst(), le( length(content(lsibling(parent()))), 25)), None, Line)	</>
</style>

<style name="PDEFN,VALLIST">
	<left-indent>	50	</>
	<space-before>	4	</>
	<break-before>	Line	</>
</style>

<style name="PLENTRY">
	<left-indent>	&std.para.left-indent	</>
	<space-before>	4	</>
	<break-before>	Line	</>
</style>

<style name="PNAME">
	<font-family>	&verbatim.font	</>
	<font-size>	&verbatim.font-size	</>
</style>

<style name="REF">
	<font-family>	&std.font	</>
	<font-size>	&std.font-size	</>
	<left-indent>	&std.leftindent	</>
	<right-indent>	&std.rightindent	</>
	<line-spacing>	&std.line-spacing	</>
	<break-before>	Line	</>
</style>

<style name="REF,BODY,MODULE,OVERVIEW,SECTION,SUB,XMP,TEXT-BEFORE">
	<font-weight>	if(eq(attr(inline), no), none, bold)	</>
	<score>	if(eq(attr(inline), no), underline, none)	</>
</style>

<style name="RET">
	<text-before>Return value:</>
</style>

<style name="ROW">
	<font-weight>	if(isfirst(),bold,medium)	</>
	<hrule>	Surround	</>
	<vrule>	Children	</>
	<break-before>	Line	</>
</style>

<style name="ROWRULE">
	<hrule>	Before	</>
	<break-before>	Line	</>
</style>

<style name="SECTION">
	<break-before>	Line	</>
</style>

<style name="SEE">
	<text-before>See Also:  </>
</style>

<style name="SYNTAX">
	<space-before>	6	</>
	<break-before>	Line	</>
	<text-before>Syntax:</>
</style>

<style name="TABLE">
	<space-before>	12	</>
	<icon-position>	Right	</>
	<hide>	Children	</>
	<break-before>	Line	</>
	<script>	ebt-reveal stylesheet=popup.rev hscroll=yes width=600 height=380	</>
	<icon-type>	table	</>
</style>

<style name="TABLECELL">
	<left-indent>	+=div(tableinfo(arbor, left-indent,2),2 )	</>
	<width>	sub(div(tableinfo(arbor,width,5),2),15)	</>
	<justification>	tableinfo(arbor,justification)	</>
	<column>	True	</>
</style>

<style name="TABLEROW">
	<font-weight>	if(eq(cnum(),1),Bold,Medium)	</>
	<font-slant>	if(eq(cnum(),1),Italics,Roman)	</>
	<space-before>	4	</>
	<space-after>	4	</>
	<break-before>	Line	</>
</style>

<style name="TBL">
	<font-size>	-=2	</>
	<left-indent>	&std.para.left-indent	</>
	<width>	135	</>
	<space-before>	&std.para.space-before	</>
	<break-before>	Line	</>
</style>

<style name="TOCPG">
	<hide>	Children	</>
</style>

<style name="VALLIST">
	<left-indent>	&std.para.left-indent	</>
	<space-before>	4	</>
	<break-before>	Line	</>
</style>

<style name="VALUE">
	<font-size>	&heading6.font-size	</>
	<space-before>	4	</>
</style>

<style name="VERBATIM">
	<font-family>	&verbatim.font	</>
	<font-size>	&verbatim.font-size	</>
	<left-indent>	100	</>
	<line-spacing>	&verbatim.line-spacing	</>
	<space-before>	5	</>
	<break-before>	Line	</>
	<break-after>	Line	</>
</style>

<style name="VLENTRY">
	<space-before>	8	</>
	<break-before>	Line	</>
</style>

<style name="VLENTRY,VALUE">
	<font-family>	&verbatim.font	</>
	<font-size>	&verbatim.font-size	</>
	<space-before>	4	</>
</style>

<style name="VLHEAD">
	<font-weight>	Bold	</>
</style>

<style name="VLHEAD,MEANING">
	<font-size>	&heading6.font-size	</>
	<left-indent>	160	</>
</style>

<style name="XMP">
	<left-indent>	+=20	</>
	<space-before>	10	</>
	<space-after>	10	</>
	<break-before>	line	</>
	<text-before>if(eq(attr(inline), yes), Example:, )</>
</style>



</sheet>

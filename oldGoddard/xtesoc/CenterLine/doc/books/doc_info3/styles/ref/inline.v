 <!--Start Entity Definitions
    This is the fthead.unx file for the imi dtd.
    It contains entities for the fulltext.v and popup.rev 
    stylesheets on unix.  This file
    should be concatenated with ftbody.all to create
    the fulltext.v file on the unix platform
-->             

<!ENTITY	address.font	CDATA	"helvetica"	>
<!ENTITY	art.color	CDATA	"dark green"	>
<!ENTITY	booktitle.color	CDATA	"medium sea green"	>
<!ENTITY	booktitle.font	CDATA	"lucida"	>
<!ENTITY	booktitle.font-size	CDATA	"24"	>
<!ENTITY	booktitle.font-slant	CDATA	"roman"	>
<!ENTITY	booktitle.font-weight	CDATA	"bold"	>
<!ENTITY	booktitle.line-spacing	CDATA	"28"	>
<!ENTITY	booktitle.space-after	CDATA	"4"	>
<!ENTITY	booktitle.space-before	CDATA	"12"	>
<!ENTITY	frontmatter.font	CDATA	"lucida"	>
<!ENTITY	genlist.first-indent	CDATA	"-=10"	>
<!ENTITY	genlist.left-indent	CDATA	"+=50"	>
<!ENTITY	genlist2.first-indent	CDATA	"-=10"	>
<!ENTITY	genlist2.left-indent	CDATA	"+=80"	>
<!ENTITY	heading1.color	CDATA	"medium sea green"	>
<!ENTITY	heading1.font	CDATA	"lucida"	>
<!ENTITY	heading1.font-size	CDATA	"21"	>
<!ENTITY	heading1.font-weight	CDATA	"BOLD"	>
<!ENTITY	heading1.line-spacing	CDATA	"22"	>
<!ENTITY	heading1.space-after	CDATA	"4"	>
<!ENTITY	heading1.space-before	CDATA	"12"	>
<!ENTITY	heading2.color	CDATA	"blue"		>
<!ENTITY	heading2.font	CDATA	"lucida"	>
<!ENTITY	heading2.font-size	CDATA	"18"	>
<!ENTITY	heading2.font-weight	CDATA	"bold"	>
<!ENTITY	heading2.line-spacing	CDATA	"20"	>
<!ENTITY	heading2.space-after	CDATA	"4"	>
<!ENTITY	heading2.space-before	CDATA	"10"	>
<!ENTITY	heading3.color	CDATA	"purple"	>
<!ENTITY	heading3.font	CDATA	"lucida"	>
<!ENTITY	heading3.font-size	CDATA	"16"	>
<!ENTITY	heading3.font-slant	CDATA	"italics"	>
<!ENTITY	heading3.font-weight	CDATA	"BOLD"	>
<!ENTITY	heading3.line-spacing	CDATA	"16"	>
<!ENTITY	heading3.space-after	CDATA	"4"	>
<!ENTITY	heading3.space-before	CDATA	"6"	>
<!ENTITY	heading4.color	CDATA	"black"	>
<!ENTITY	heading4.font	CDATA	"lucida"	>
<!ENTITY	heading4.font-size	CDATA	"14"	>
<!ENTITY	heading4.font-weight	CDATA	"bold"	>
<!ENTITY	heading4.line-spacing	CDATA	"16"	>
<!ENTITY	heading4.space-before	CDATA	"6"	>
<!ENTITY	heading5.color	CDATA	"blue"	>
<!ENTITY	heading5.font	CDATA	"lucida"	>
<!ENTITY	heading5.font-size	CDATA	"14"	>
<!ENTITY	heading5.font-weight	CDATA	"bold"	>
<!ENTITY	heading5.left-indent	CDATA	"5"	>
<!ENTITY	heading5.line-spacing	CDATA	"16"	>
<!ENTITY	heading5.space-before	CDATA	"6"	>
<!ENTITY	heading6.color	CDATA	"black"	>
<!ENTITY	heading6.font	CDATA	"lucida"	>
<!ENTITY	heading6.font-size	CDATA	"12"	>
<!ENTITY	heading6.font-weight	CDATA	"bold"	>
<!ENTITY	heading6.left-indent	CDATA	"0"	>
<!ENTITY	heading6.line-spacing	CDATA	"14"	>
<!ENTITY	heading6.space-after	CDATA	"2"	>
<!ENTITY	heading6.space-before	CDATA	"4"	>
<!ENTITY	heading6.line-after	CDATA	"line"	>
<!ENTITY	heading6.line-before	CDATA	"line"	>
<!ENTITY	hottext.fontfam	CDATA	"new century schoolbook"	>
<!ENTITY	hottext.foreground	CDATA	"medium sea green"	>
<!ENTITY	hottext.slant	CDATA	"Roman"	>
<!ENTITY	hottext.weight	CDATA	"medium"	>
<!ENTITY	hottext.score	CDATA	"under"	>
<!ENTITY	revchgbw	CDATA	"if(attr(revision),under,)"	>
<!ENTITY	revchgcol	CDATA	"if(attr(revision),blueviolet,)"	>
<!ENTITY	std.font	CDATA	"new century schoolbook"	>
<!ENTITY	std.font-size	CDATA	"14"	>
<!ENTITY	std.font.color	CDATA	"black"	>
<!ENTITY	std.indent	CDATA	"10"	>
<!ENTITY	std.leftindent	CDATA	"10"	>
<!ENTITY	std.line-spacing	CDATA	"16"	>
<!ENTITY	std.para.left-indent	CDATA	"+=30"	>
<!ENTITY	std.para.space-before	CDATA	"6"	>
<!ENTITY	std.rightindent	CDATA	"12"	>
<!ENTITY        subtitle.font   CDATA   "lucida"  >
<!ENTITY        subtitle.font-size   CDATA   "14"  >
<!ENTITY        subtitle.line-spacing   CDATA   "16"  >
<!ENTITY        subtitle.color   CDATA   "black"  >
<!ENTITY        subtitle.leftindent   CDATA   "10"  >
<!ENTITY        subtitle.indent   CDATA   "10"  >
<!ENTITY	subscript	CDATA	"-3"	>
<!ENTITY	tag.color	CDATA	"purple"	>
<!ENTITY	title.color	CDATA	"blue"	>
<!ENTITY	title.font	CDATA	"lucida"	>
<!ENTITY	verbatim.font	CDATA	"courier new"	>
<!ENTITY	verbatim.font-size	CDATA	"10"	>
<!ENTITY	verbatim.line-spacing	CDATA	"14"	>
<!ENTITY	verbatim.small-font-size	CDATA	"8"	>
<!--End Entity Definitions -->


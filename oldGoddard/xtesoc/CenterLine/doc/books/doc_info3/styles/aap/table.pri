<!-- Style sheet for AAP tables.

  This style sheet is used when the user selects a table icon in a
  full-text view.  In the full-text view, the table element is defined
  to put up an icon, and to specify a script which will cause the table
  to appear in its own window when the icon is clicked; this style sheet
  controls the formatting of that resulting window.

  Copyright 1990, Electronic Book Technologies.  All rights reserved.
-->


<sheet AUTHOR="sjd" DATE="10/31/90" VERSION="1.5">



<?INSTED COMMENT: UNGROUPED STYLES FOLLOW>

<!-- The '#ROOT' style is the starting point for the document.
-->
<style name="#ROOT">
	<font-family>	helvetica	</>
	<font-weight>	medium	</>
	<font-slant>	roman	</>
	<font-size>	12	</>
	<foreground>	black	</>
	<left-indent>	10	</>
	<right-indent>	12	</>
	<line-spacing>	15	</>
</style>

<style name="#TAGS">
	<font-weight>	Bold	</>
	<foreground>	purple	</>
</style>

<!-- ***************************************************************** -->
<style name="BOOK">
	<font-family>	helvetica	</>
	<font-weight>	medium	</>
	<font-slant>	roman	</>
	<font-size>	12	</>
	<foreground>	black	</>
	<left-indent>	10	</>
	<right-indent>	12	</>
	<line-spacing>	15	</>
</style>

<style name="C">
	<!-- Properties common to all columns -->
	<width>	180	</>
	<break-before>	off	</>
	<select>	C,C#cnum()	</>
	<column>	on	</>
</style>

<!-- Following styles are definitions for each column of the table.
     They can be generated by hand, or more conveniently using the
     'genTable' utility program.
-->
<style name="C#1">
	<!-- First column -->
	<left-indent>	10	</>
	<justification>	l	</>
</style>

<style name="C#2">
	<!-- Second column -->
	<left-indent>	200	</>
	<justification>	l	</>
</style>

<style name="C#3">
	<!-- Third column -->
	<left-indent>	390	</>
	<justification>	l	</>
</style>

<style name="C#4">
	<!-- Fourth column -->
	<left-indent>	580	</>
	<justification>	l	</>
</style>

<style name="C#5">
	<!-- Fifth column -->
	<left-indent>	800	</>
	<justification>	l	</>
</style>

<!-- Emphasis should be defined, since it is likely to appear
     within table cells (for example, in the header row).
-->
<style name="E1">
	<font-weight>	b	</>
</style>

<style name="E2">
	<font-slant>	i	</>
</style>

<style name="E3">
	<font-weight>	b	</>
	<font-slant>	i	</>
</style>

<!-- Since AAP doesn't provide table-column-headers, one way to get headers
     is to make each cell in the first row emphatic (using E1 or similar);
     could also get headers by defining a style for ROW#(1), etc., but this
     would not be appropriate for all AAP-conforming files.
-->
<style name="ROW">
	<font-weight>	m	</>
	<left-indent>	10	</>
	<space-before>	8	</>
	<hrules>	After	</>
	<break-before>	on	</>
</style>

<style name="TT">
	<!-- Table title -->
	<font-weight>	bold	</>
	<font-size>	14	</>
	<left-indent>	25	</>
	<justification>	c	</>
	<break-before>	on	</>
</style>



</sheet>

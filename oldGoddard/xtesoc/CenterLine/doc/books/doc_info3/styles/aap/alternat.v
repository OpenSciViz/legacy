<!-- alternate style sheet.

  Differs from fulltext.view in colors fonts, centering of titles,....

  This uses the ANSI AAP tag set, but has a few additions for
  supporting hypertext elements.

  Copyright 1990, Electronic Book Technologies.  All rights reserved.
-->

<!ENTITY	link.indent	CDATA	"12"	>
<!ENTITY	link.side	CDATA	"right"	>
<!ENTITY	std.indent	CDATA	"24"	>
<!ENTITY	std.vert	CDATA	"12"	>
<!ENTITY	text.leading	CDATA	"14"	>
<!ENTITY	text.smallest	CDATA	"12"	>
<!ENTITY	title.color	CDATA	"blue"	>
<!ENTITY	title.font	CDATA	"helvetica"	>
<!ENTITY	title.justify	CDATA	"center"	>

<sheet AUTHOR="sjd" DATE="11/19/90" VERSION="1.5">



<?INSTED COMMENT: UNGROUPED STYLES FOLLOW>

<!-- ************************************************************* 
 Following entities are used to provide names for common values,
     which can then be changed throughout the style sheet just by
     changing their definitions here.

 ************************************************************* 
  '#ROOT' is the starting point for the document.
-->
<style name="#ROOT">
	<font-family>	new century schoolbook	</>
	<font-weight>	m	</>
	<font-slant>	i	</>
	<font-size>	12	</>
	<foreground>	black	</>
	<left-indent>	10	</>
	<right-indent>	12	</>
	<line-spacing>	14	</>
</style>

<style name="#TAGS">
	<font-weight>	Bold	</>
</style>

<style name="ABS">
</style>

<style name="ACK">
</style>

<!-- ************************************************************* 
 Styles for elements beyond the AAP set, for media/hypertext:
-->
<style name="ANCHOR">
	<font-slant>	roman	</>
	<foreground>	red	</>
	<right-indent>	12	</>
	<icon-position>	right	</>
	<break-before>	on	</>
	<script>	ebt-link tname=RID tvalue=@(ID)	</>
	<!-- Destination of hypertext link -->
	<icon-type>	inlink	</>
</style>

<style name="APP">
</style>

<style name="APPM">
</style>

<!-- ************************************************************* 
 Styles for hypertextual elements that are cross-references:
-->
<style name="APR">
	<right-indent>	12	</>
	<icon-position>	right	</>
	<script>	ebt-link tname=ID tvalue=@RID	</>
	<!-- Reference to appendix -->
	<icon-type>	inlink	</>
</style>

<style name="APT">
	<!-- Appendix (APP) title -->
	<font-family>	helvetica	</>
	<font-weight>	bold	</>
	<font-size>	24	</>
	<foreground>	blue	</>
	<line-spacing>	28	</>
	<justification>	center	</>
	<break-before>	on	</>
</style>

<!-- ************************************************************* 
 Styles for hypertextual elements other than cross-references:
-->
<style name="ART">
	<right-indent>	12	</>
	<icon-position>	right	</>
	<break-before>	on	</>
	<script>	ebt-raster filename=@(ID)	</>
	<!-- Actual artwork -->
	<icon-type>	raster	</>
</style>

<style name="ARTR">
	<font-weight>	bold	</>
	<foreground>	red	</>
	<right-indent>	12	</>
	<icon-position>	right	</>
	<script>	ebt-link tname=ID tvalue=@RID	</>
	<!-- Reference to artwork (or something else) -->
	<icon-type>	inlink	</>
</style>

<style name="AU">
	<font-family>	helvetica	</>
	<font-size>	18	</>
	<line-spacing>	20	</>
	<space-before>	18	</>
	<justification>	center	</>
	<break-before>	on	</>
</style>

<style name="B">
	<font-weight>	b	</>
</style>

<style name="BI">
	<font-weight>	b	</>
	<font-slant>	i	</>
</style>

<style name="BM">
</style>

<!-- ************************************************************* 
 Styles for main body elements (most have no formatting effect):
-->
<style name="BOOK">
	<font-family>	new century schoolbook	</>
	<font-weight>	m	</>
	<font-slant>	i	</>
	<font-size>	12	</>
	<foreground>	black	</>
	<left-indent>	10	</>
	<right-indent>	12	</>
	<line-spacing>	17	</>
</style>

<style name="BQ">
	<left-indent>	+= 18	</>
	<break-before>	on	</>
</style>

<style name="C,P">
	<!-- paragraphs within table cells -->
	<break-before>	off	</>
</style>

<style name="CAU">
	<font-family>	helvetica	</>
	<font-size>	18	</>
	<line-spacing>	20	</>
	<space-before>	18	</>
	<justification>	center	</>
	<break-before>	on	</>
</style>

<style name="CCI">
</style>

<style name="CRD">
</style>

<style name="CRN">
</style>

<style name="CRT">
</style>

<style name="CT">
	<!-- Chapter (CHP) title -->
	<font-family>	helvetica	</>
	<font-weight>	bold	</>
	<font-size>	18	</>
	<foreground>	blue	</>
	<line-spacing>	24	</>
	<space-before>	36	</>
	<justification>	center	</>
	<break-before>	on	</>
</style>

<style name="CYR">
	<!-- If not available, #(roots) -->
	<font-family>	cyrillic	</>
</style>

<style name="DD">
	<!-- Definition description -->
	<space-before>	0	</>
	<break-before>	off	</>
</style>

<style name="DD,P">
	<space-before>	5	</>
	<break-before>	on	</>
</style>

<style name="DDHD">
	<!-- Definition description header -->
	<font-weight>	b	</>
	<space-before>	5	</>
	<break-before>	on	</>
</style>

<style name="DED">
</style>

<style name="DEG">
</style>

<!-- ****************** Definition lists ****************** -->
<style name="DL">
	<left-indent>	24	</>
	<first-indent>	0	</>
	<space-before>	0	</>
	<break-before>	on	</>
</style>

<style name="DOCLINK">
	<font-slant>	roman	</>
	<foreground>	red	</>
	<right-indent>	12	</>
	<icon-position>	right	</>
	<break-before>	on	</>
	<script>	ebt-link book="@DOC" tname=ID tvalue=@(RID)	</>
	<!-- link to other book -->
	<icon-type>	exlink	</>
</style>

<style name="DT">
	<!-- Definition term -->
	<font-weight>	b	</>
	<space-before>	8	</>
	<break-before>	on	</>
</style>

<style name="DTHD">
	<!-- Definition term header -->
	<font-weight>	b	</>
	<space-before>	8	</>
	<break-before>	on	</>
</style>

<!-- ************************************************************* 
 Styles for sub-paragraph elements, such as emphasis:
-->
<style name="E1">
	<font-weight>	b	</>
</style>

<style name="E2">
	<font-slant>	r	</>
</style>

<style name="E3">
	<font-weight>	b	</>
	<font-slant>	r	</>
</style>

<style name="EMQ">
	<left-indent>	+= 18	</>
	<break-before>	on	</>
</style>

<style name="FIGR">
	<right-indent>	12	</>
	<icon-position>	right	</>
	<script>	ebt-link tname=ID tvalue=@RID	</>
	<!-- Reference to figure -->
	<icon-type>	inlink	</>
</style>

<style name="FN">
	<right-indent>	12	</>
	<icon-position>	right	</>
	<hide>	text	</>
	<script>	ebt-footnote	</>
	<!-- Footnote -->
	<icon-type>	footnote	</>
</style>

<style name="FNM">
</style>

<style name="FNR">
	<font-weight>	b	</>
	<!-- Foonote reference mark (e.g. *) -->
	<vertical-offset>	4	</>
</style>

<style name="FWD">
</style>

<style name="GK">
	<!-- If not available, #(roots) -->
	<font-family>	greek	</>
</style>

<!-- ****************** Glossary lists ****************** -->
<style name="GL">
	<left-indent>	24	</>
	<first-indent>	0	</>
	<space-before>	0	</>
	<break-before>	on	</>
</style>

<style name="H">
	<font-family>	helvetica	</>
	<font-weight>	bold	</>
	<font-size>	12	</>
	<foreground>	blue	</>
	<line-spacing>	14	</>
	<justification>	center	</>
	<break-before>	on	</>
</style>

<style name="H1">
	<font-family>	helvetica	</>
	<font-weight>	bold	</>
	<font-size>	12	</>
	<foreground>	blue	</>
	<line-spacing>	14	</>
	<justification>	center	</>
	<break-before>	on	</>
</style>

<style name="H2">
	<font-family>	helvetica	</>
	<font-weight>	bold	</>
	<font-size>	12	</>
	<foreground>	blue	</>
	<line-spacing>	14	</>
	<justification>	center	</>
	<break-before>	on	</>
</style>

<style name="H3">
	<font-family>	helvetica	</>
	<font-weight>	bold	</>
	<font-size>	12	</>
	<foreground>	blue	</>
	<line-spacing>	14	</>
	<justification>	center	</>
	<break-before>	on	</>
</style>

<style name="H4">
	<font-family>	helvetica	</>
	<font-weight>	bold	</>
	<font-size>	12	</>
	<foreground>	blue	</>
	<line-spacing>	14	</>
	<justification>	center	</>
	<break-before>	on	</>
</style>

<style name="IDX">
	<!-- Manual index -->
	<hide>	text	</>
</style>

<style name="IT">
	<font-slant>	i	</>
</style>

<style name="ITM">
	<space-before>	0	</>
	<break-before>	off	</>
</style>

<!-- ****************** Item lists ****************** -->
<style name="ITML">
	<font-family>	courier	</>
	<left-indent>	24	</>
	<first-indent>	0	</>
	<line-spacing>	12	</>
	<space-before>	0	</>
	<break-before>	off	</>
</style>

<!-- ****************** Types of lists ****************** -->
<style name="L">
	<foreground>	blue	</>
	<left-indent>	24	</>
	<first-indent>	0	</>
	<space-before>	0	</>
	<break-before>	on	</>
	<break-after>	on	</>
</style>

<style name="L1">
	<left-indent>	24	</>
	<first-indent>	0	</>
	<space-before>	0	</>
	<break-before>	on	</>
	<break-after>	on	</>
</style>

<style name="L2">
	<left-indent>	62	</>
	<first-indent>	0	</>
	<space-before>	0	</>
	<break-before>	on	</>
	<break-after>	on	</>
</style>

<style name="L3">
	<left-indent>	80	</>
	<first-indent>	0	</>
	<space-before>	0	</>
	<break-before>	on	</>
	<break-after>	on	</>
</style>

<style name="L4">
	<left-indent>	85	</>
	<first-indent>	0	</>
	<space-before>	0	</>
	<break-before>	on	</>
	<break-after>	on	</>
</style>

<style name="L5">
	<left-indent>	90	</>
	<first-indent>	0	</>
	<space-before>	0	</>
	<break-before>	on	</>
	<break-after>	on	</>
</style>

<style name="LH">
	<font-weight>	b	</>
	<break-before>	on	</>
</style>

<!-- ****************** Items within lists ****************** -->
<style name="LI">
	<space-before>	5	</>
	<break-before>	on	</>
</style>

<style name="LI,P">
	<space-before>	0	</>
	<break-before>	on	</>
</style>

<style name="LINE">
	<space-before>	0	</>
	<break-before>	on	</>
</style>

<style name="NIT">
	<right-indent>	12	</>
	<icon-position>	right	</>
	<hide>	text	</>
	<script>	ebt-footnote title=Note	</>
	<!-- Note-in-text -->
	<icon-type>	footnote	</>
</style>

<!-- ************************************************************* 
 Styles for title elements (except book title, see above):
-->
<style name="NO">
</style>

<style name="NOTES">
</style>

<style name="NTR">
	<right-indent>	12	</>
	<icon-position>	right	</>
	<script>	ebt-link tname=ID tvalue=@RID	</>
	<!-- Reference to note-in-text -->
	<icon-type>	inlink	</>
</style>

<style name="ODV">
</style>

<style name="ONM">
</style>

<!-- ************************************************************* 
 Styles for paragraph/text-block level elements:
-->
<style name="P">
	<space-before>	12	</>
	<break-before>	on	</>
</style>

<style name="PART">
</style>

<style name="PDT">
	<font-family>	helvetica	</>
	<font-size>	12	</>
	<line-spacing>	14	</>
	<justification>	center	</>
	<break-before>	on	</>
</style>

<style name="PF">
</style>

<style name="PT">
	<!-- Part (PT) title -->
	<font-family>	helvetica	</>
	<font-weight>	bold	</>
	<font-size>	24	</>
	<foreground>	blue	</>
	<line-spacing>	28	</>
	<space-before>	36	</>
	<justification>	center	</>
	<break-before>	on	</>
</style>

<style name="PUBFM">
</style>

<style name="Q">
</style>

<style name="RB">
	<foreground>	red	</>
</style>

<style name="RM">
	<font-weight>	m	</>
	<font-slant>	r	</>
</style>

<style name="ROLE">
</style>

<style name="SBR">
	<right-indent>	12	</>
	<icon-position>	right	</>
	<hide>	text	</>
	<script>	ebt-footnote title=Sidebar	</>
	<!-- Sidebar -->
	<icon-type>	footnote	</>
</style>

<style name="SBT">
	<font-family>	helvetica	</>
	<font-size>	12	</>
	<line-spacing>	14	</>
	<justification>	center	</>
	<break-before>	on	</>
</style>

<style name="SCP">
	<!-- Use smaller type for now. -->
	<font-size>	-=2	</>
</style>

<style name="SIT1">
	<left-indent>	+=12	</>
	<space-before>	5	</>
	<break-before>	on	</>
</style>

<style name="SIT2">
	<left-indent>	+=12	</>
	<space-before>	5	</>
	<break-before>	on	</>
</style>

<style name="SIT3">
	<left-indent>	+=12	</>
	<space-before>	5	</>
	<break-before>	on	</>
</style>

<style name="SMTL">
</style>

<style name="SNM">
</style>

<style name="SOUND">
	<right-indent>	12	</>
	<icon-position>	right	</>
	<script>	ebt-launch cmd="sound_script var(fig_dir)/@(ID)"	</>
	<!-- Digitized sound -->
	<icon-type>	sound	</>
</style>

<style name="SS1,ST">
	<!-- Subsection (SS1) title -->
	<font-family>	helvetica	</>
	<font-weight>	bold	</>
	<font-size>	14	</>
	<foreground>	blue	</>
	<line-spacing>	17	</>
	<space-before>	12	</>
	<justification>	center	</>
	<break-before>	on	</>
</style>

<style name="SS2">
</style>

<style name="SS2,ST">
	<!-- Subsection (SS2) title -->
	<font-family>	helvetica	</>
	<font-weight>	bold	</>
	<font-size>	12	</>
	<foreground>	blue	</>
	<line-spacing>	14	</>
	<space-before>	12	</>
	<justification>	center	</>
	<break-before>	on	</>
</style>

<style name="SS3">
</style>

<style name="SS3,ST">
	<!-- Subsection (SS3) title -->
	<font-family>	helvetica	</>
	<font-weight>	bold	</>
	<font-size>	12	</>
	<foreground>	blue	</>
	<line-spacing>	14	</>
	<space-before>	12	</>
	<justification>	center	</>
	<break-before>	on	</>
</style>

<style name="ST">
	<!-- Section (SEC) title (see following) -->
	<font-family>	helvetica	</>
	<font-weight>	bold	</>
	<font-size>	18	</>
	<foreground>	blue	</>
	<line-spacing>	20	</>
	<space-before>	24	</>
	<justification>	center	</>
	<break-before>	on	</>
</style>

<!-- ************************************************************* 
 Style for tables:  hide them behind an icon; when clicked, they
     will appear in a separate window controlled by the".table"
     style sheet, or any one specified for the particular table instance
     (for example, by a 'table-type' attribute).
-->
<style name="TABLE">
	<icon-position>	r	</>
	<hide>	text	</>
	<script>	ebt-table	</>
	<icon-type>	table	</>
</style>

<style name="TBL">
	<icon-position>	r	</>
	<hide>	text	</>
	<script>	ebt-table	</>
	<icon-type>	table	</>
</style>

<style name="TI">
	<font-family>	helvetica	</>
	<font-weight>	bold	</>
	<font-size>	24	</>
	<foreground>	blue	</>
	<line-spacing>	28	</>
	<justification>	center	</>
	<break-before>	on	</>
</style>

<style name="TOC">
	<!-- Manual table of contents -->
	<hide>	text	</>
</style>

<style name="VECTOR">
	<right-indent>	12	</>
	<icon-position>	right	</>
	<script>	ebt-launch cmd="cgm_script var(fig_dir)/@(ID)"	</>
	<!-- CGM-format vector graphic -->
	<icon-type>	vector	</>
</style>

<style name="XREF">
	<font-slant>	roman	</>
	<foreground>	red	</>
	<right-indent>	12	</>
	<icon-position>	right	</>
	<break-before>	on	</>
	<script>	ebt-link tname=ID tvalue=@(RID)	</>
	<!-- Origin of hypertext link -->
	<icon-type>	inlink	</>
</style>



</sheet>

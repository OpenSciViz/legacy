<!ENTITY	art.color	CDATA	"dark green"	>
<!ENTITY	hottext.fontfam	CDATA	"lucida"	>
<!ENTITY	hottext.foreground	CDATA	"red"	>
<!ENTITY	hottext.slant	CDATA	"Roman"	>
<!ENTITY	hottext.weight	CDATA	"Bold"	>
<!ENTITY	std.font	CDATA	"New Century Schoolbook"	>
<!ENTITY	std.font-size	CDATA	"14"	>
<!ENTITY	std.leftindent	CDATA	"10"	>
<!ENTITY	std.line-spacing	CDATA	"16"	>
<!ENTITY	std.rightindent	CDATA	"12"	>
<!ENTITY	subscript	CDATA	"-3"	>
<!ENTITY	title.color	CDATA	"blue"	>
<!ENTITY	title.font	CDATA	"lucida"	>
<!ENTITY	verbatim.font	CDATA	"courier"	>
<!ENTITY	verbatim.font-size	CDATA	"10"	>
<!ENTITY	verbatim.line-spacing	CDATA	"12"	>

<sheet >



<?INSTED COMMENT: GROUP titles>

<group name="titles">
	<font-family>	times	</>
	<font-weight>	Bold	</>
	<left-indent>	-=25	</>
	<break-before>	True	</>
</group>

<style name="CT" group="titles">
	<font-size>	24	</>
	<line-spacing>	26	</>
	<space-before>	15	</>
</style>

<style name="SEC,ST" group="titles">
	<font-size>	18	</>
	<line-spacing>	20	</>
	<space-before>	10	</>
</style>

<style name="SS1,ST" group="titles">
	<font-size>	14	</>
	<left-indent>	+=0	</>
	<line-spacing>	16	</>
	<space-before>	8	</>
</style>

<style name="TITLE" group="titles">
	<font-size>	24	</>
	<foreground>	&title.color	</>
	<line-spacing>	26	</>
	<space-after>	30	</>
	<justification>	Center	</>
</style>



<?INSTED COMMENT: UNGROUPED STYLES FOLLOW>

<style name="#ROOT">
	<break-before>	Line	</>
</style>

<style name="#TAGS">
	<font-weight>	Bold	</>
</style>

<style name="BOOK">
	<font-family>	helvetica	</>
	<left-indent>	15	</>
</style>

<style name="P">
	<break-before>	True	</>
</style>



</sheet>

<!ENTITY	art.color	CDATA	"dark green"	>
<!ENTITY	hottext.fontfam	CDATA	"lucida"	>
<!ENTITY	hottext.foreground	CDATA	"red"	>
<!ENTITY	hottext.slant	CDATA	"Roman"	>
<!ENTITY	hottext.weight	CDATA	"Bold"	>
<!ENTITY	std.font	CDATA	"New Century Schoolbook"	>
<!ENTITY	std.font-size	CDATA	"14"	>
<!ENTITY	std.indent	CDATA	"10"	>
<!ENTITY	std.leftindent	CDATA	"10"	>
<!ENTITY	std.line-spacing	CDATA	"16"	>
<!ENTITY	std.rightindent	CDATA	"12"	>
<!ENTITY	subscript	CDATA	"-3"	>
<!ENTITY	title.color	CDATA	"blue"	>
<!ENTITY	title.font	CDATA	"lucida"	>
<!ENTITY	verbatim.font	CDATA	"courier"	>
<!ENTITY	verbatim.font-size	CDATA	"10"	>
<!ENTITY	verbatim.line-spacing	CDATA	"12"	>

<sheet LEVELS="1">



<?INSTED COMMENT: UNGROUPED STYLES FOLLOW>

<style name="#ROOT">
	<break-before>	Line	</>
</style>

<style name="#SDATA">
	<font-family>	attr(font)	</>
	<font-weight>	Medium	</>
	<character-set>	attr(charset)	</>
	<text-before>char(attr(code)) </>
</style>

<style name="#TAGS">
	<font-weight>	Bold	</>
	<foreground>	purple	</>
</style>

<style name="AUTHOR">
	<font-family>	helvetica	</>
	<font-size>	14	</>
	<line-spacing>	16	</>
	<space-before>	18	</>
	<justification>	Verbatim	</>
	<break-before>	Line	</>
</style>

<style name="AVAIL">
	<left-indent>	35	</>
	<first-indent>	-25	</>
	<space-before>	10	</>
	<space-after>	10	</>
	<break-before>	Line	</>
	<text-before>Availability:</>
</style>

<style name="AVAIL,#TEXT-BEFORE">
	<font-weight>	Bold	</>
	<space-after>	10	</>
</style>

<style name="C">
	<left-indent>	+=35	</>
	<width>	50	</>
	<select>	C_cnum()	</>
	<column>	True	</>
</style>

<style name="COMM">
	<left-indent>	+=25	</>
	<first-indent>	-25	</>
	<space-before>	10	</>
	<break-before>	on	</>
	<text-before>Comments:</>
</style>

<style name="COMM,#TEXT-BEFORE">
	<font-weight>	Bold	</>
</style>

<style name="COPYRTPG">
	<right-indent>	12	</>
	<icon-position>	Right	</>
	<hide>	Children	</>
	<script>	ebt-footnote title="Copyright Notice"	</>
	<icon-type>	copyrt	</>
</style>

<style name="C_1">
	<width>	40	</>
	<justification>	Center	</>
	<column>	True	</>
</style>

<style name="C_2">
	<left-indent>	+=45	</>
	<width>	40	</>
	<justification>	Center	</>
	<column>	True	</>
</style>

<style name="C_3">
	<left-indent>	+=90	</>
	<width>	40	</>
	<justification>	Center	</>
	<column>	True	</>
</style>

<style name="C_4">
	<left-indent>	+=135	</>
	<width>	40	</>
	<justification>	Center	</>
	<column>	True	</>
</style>

<style name="DATATYPE">
	<space-before>	20	</>
</style>

<style name="DATE">
	<font-family>	helvetica	</>
	<font-slant>	Roman	</>
	<font-size>	14	</>
	<line-spacing>	20	</>
	<space-before>	18	</>
	<justification>	Left	</>
	<break-before>	True	</>
</style>

<style name="DESC">
	<left-indent>	+=25	</>
	<first-indent>	-25	</>
	<space-before>	10	</>
	<break-before>	on	</>
	<text-before>Description:</>
</style>

<style name="DESC,#TEXT-BEFORE">
	<font-weight>	Bold	</>
</style>

<style name="EBTDOC">
	<font-family>	times	</>
	<font-size>	12	</>
	<left-indent>	10	</>
	<line-spacing>	12	</>
	<break-before>	Line	</>
	<title-tag>	FRONT,TITLEPG,TITLE	</>
</style>

<style name="EMPH">
	<font-slant>	Italics	</>
</style>

<style name="EMPY">
	<break-before>	on	</>
</style>

<style name="EXTREF">
	<font-family>	&std.font	</>
	<foreground>	&hottext.foreground	</>
</style>

<style name="FCT">
	<space-before>	16	</>
	<hrule>	top	</>
	<break-before>	on	</>
</style>

<style name="FCT,NAME">
	<font-weight>	Bold	</>
	<font-size>	14	</>
	<line-spacing>	16	</>
	<space-before>	8	</>
	<break-before>	on	</>
</style>

<style name="I">
	<font-slant>	Italics	</>
</style>

<style name="ITEM">
	<left-indent>	+=16	</>
	<space-after>	0	</>
	<break-before>	False	</>
	<column>	True	</>
</style>

<style name="ITEM,P">
	<font-family>	&std.font	</>
	<break-before>	None	</>
</style>

<style name="LIST">
	<first-indent>	0	</>
	<space-before>	5	</>
	<space-after>	1	</>
	<break-before>	True	</>
</style>

<style name="MARKER">
	<font-family>	symbol	</>
	<font-weight>	Medium	</>
	<space-before>	5	</>
	<break-before>	True	</>
	<break-after>	None	</>
</style>

<style name="MEANING">
	<left-indent>	180	</>
	<first-indent>	180	</>
</style>

<style name="MODULE,NAME">
	<font-family>	&title.font	</>
	<font-weight>	Bold	</>
	<font-size>	14	</>
	<foreground>	&title.color	</>
	<line-spacing>	14	</>
	<space-before>	10	</>
	<hrule>	None	</>
</style>

<style name="NAME">
	<font-family>	helvetica	</>
	<font-weight>	Bold	</>
	<font-size>	14	</>
	<line-spacing>	16	</>
	<break-before>	on	</>
</style>

<style name="NOTICE">
	<font-family>	helvetica	</>
	<font-size>	14	</>
	<line-spacing>	16	</>
	<space-before>	20	</>
	<break-before>	True	</>
</style>

<style name="OVERVIEW">
	<break-before>	on	</>
</style>

<style name="OWNER">
	<font-family>	helvetica	</>
	<font-size>	18	</>
	<line-spacing>	20	</>
	<space-before>	18	</>
	<justification>	Left	</>
	<break-before>	True	</>
</style>

<style name="P">
	<font-family>	&std.font	</>
	<font-size>	&std.font-size	</>
	<line-spacing>	&std.line-spacing	</>
	<space-before>	if(or(if(contains(content(typechild(EMPH,lsibling(P))),ts:),TRUE,),if(contains(content(typechild(EMPH,lsibling(P))),ns:),TRUE,FALSE)),0,5)	</>
	<break-before>	if(or(if(contains(content(typechild(EMPH,lsibling(P))),ts:),TRUE,),if(contains(content(typechild(EMPH,lsibling(P))),ns:),TRUE,FALSE)),off,on)	</>
</style>

<style name="PARMLIST">
	<space-before>	10	</>
	<break-before>	on	</>
	<text-before>Parameters:</>
</style>

<style name="PARMLIST,#TEXT-BEFORE">
	<font-weight>	Bold	</>
</style>

<style name="PDEFN">
	<left-indent>	60	</>
</style>

<style name="PLENTRY">
	<space-before>	4	</>
	<break-before>	Line	</>
</style>

<style name="RET">
	<left-indent>	+=25	</>
	<first-indent>	-25	</>
	<space-before>	10	</>
	<break-before>	Line	</>
	<text-before>Return value:</>
</style>

<style name="RET,#TEXT-BEFORE">
	<font-weight>	Bold	</>
	<first-indent>	-25	</>
</style>

<style name="ROW">
	<font-weight>	if(isfirst(),bold,medium)	</>
	<space-before>	4	</>
	<space-after>	4	</>
	<hrule>	Surround	</>
	<vrule>	Children	</>
	<break-before>	Line	</>
</style>

<style name="SECTION">
	<break-before>	on	</>
</style>

<style name="SEE">
	<left-indent>	+=25	</>
	<first-indent>	-25	</>
	<space-before>	10	</>
	<break-before>	on	</>
	<text-before>See Also:</>
</style>

<style name="SEE,#TEXT-BEFORE">
	<font-weight>	Bold	</>
</style>

<style name="SUBTITLE">
	<font-family>	helvetica	</>
	<font-size>	14	</>
	<line-spacing>	20	</>
	<space-before>	8	</>
	<space-after>	10	</>
	<justification>	Left	</>
	<break-before>	True	</>
</style>

<style name="SYNTAX">
	<space-before>	6	</>
	<break-before>	Line	</>
	<text-before>Syntax:</>
</style>

<style name="SYNTAX,#TEXT-BEFORE">
	<font-weight>	Bold	</>
	<first-indent>	-25	</>
</style>

<style name="TBL">
	<width>	180	</>
	<break-before>	Line	</>
</style>

<style name="TEMPLATE">
	<font-family>	courier	</>
	<font-size>	9	</>
	<left-indent>	+=25	</>
	<line-spacing>	12	</>
	<space-before>	5	</>
	<space-after>	5	</>
	<justification>	verbatim	</>
	<break-before>	on	</>
</style>

<style name="TITLE">
	<font-family>	&title.font	</>
	<font-weight>	Bold	</>
	<font-size>	12	</>
	<foreground>	&title.color	</>
	<space-before>	10	</>
</style>

<style name="TITLEPG,TITLE">
	<font-family>	&title.font	</>
	<font-weight>	Bold	</>
	<font-size>	18	</>
	<foreground>	&title.color	</>
	<line-spacing>	16	</>
	<space-before>	10	</>
</style>

<style name="TOCPG">
	<hide>	Children	</>
</style>

<style name="VALLIST">
	<space-before>	4	</>
	<break-before>	Line	</>
</style>

<style name="VERBATIM">
	<font-family>	courier	</>
	<font-size>	-=2	</>
	<line-spacing>	-=2	</>
	<space-before>	5	</>
	<break-before>	Line	</>
	<break-after>	Line	</>
</style>

<style name="VLENTRY">
	<space-before>	4	</>
	<break-before>	Line	</>
</style>

<style name="VLHEAD">
	<font-weight>	Bold	</>
</style>

<style name="XREF">
	<font-family>	&std.font	</>
	<font-slant>	Roman	</>
	<font-size>	&std.font-size	</>
	<foreground>	&hottext.foreground	</>
	<score>	Under	</>
	<script>	ebt-link target=idmatch(ID,attr(rid)) stylesheet=popup window=specified view=fulltext root=1	</>
</style>



</sheet>

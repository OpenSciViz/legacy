<!ENTITY	art.color	CDATA	"dark green"	>
<!ENTITY	hottext.fontfam	CDATA	"lucida"	>
<!ENTITY	hottext.foreground	CDATA	"red"	>
<!ENTITY	hottext.slant	CDATA	"Roman"	>
<!ENTITY	hottext.weight	CDATA	"Bold"	>
<!ENTITY	std.font	CDATA	"New Century Schoolbook"	>
<!ENTITY	std.font-size	CDATA	"14"	>
<!ENTITY	std.indent	CDATA	"10"	>
<!ENTITY	std.leftindent	CDATA	"10"	>
<!ENTITY	std.line-spacing	CDATA	"16"	>
<!ENTITY	std.rightindent	CDATA	"12"	>
<!ENTITY	subscript	CDATA	"-3"	>
<!ENTITY	title.color	CDATA	"blue"	>
<!ENTITY	title.font	CDATA	"lucida"	>
<!ENTITY	verbatim.font	CDATA	"courier"	>
<!ENTITY	verbatim.font-size	CDATA	"10"	>
<!ENTITY	verbatim.line-spacing	CDATA	"12"	>

<sheet >



<?INSTED COMMENT: GROUP debug>

<!-- Groups -->
<group name="debug">
	<font-family>	helvetica	</>
	<font-weight>	medium	</>
	<font-size>	10	</>
	<line-spacing>	12	</>
	<text-before><tag()> </>
	<text-after> <\/tag()> </>
</group>

<style name="AUTHOR" group="debug">
	<font-family>	Helvetica	</>
	<font-size>	14	</>
	<line-spacing>	20	</>
	<space-before>	18	</>
	<justification>	Left	</>
	<break-before>	True	</>
</style>

<style name="COPYRTPG" group="debug">
	<icon-position>	Right	</>
	<hide>	Children	</>
	<script>	ebt-footnote title="Copyright Notice"	</>
	<icon-type>	copyrt	</>
</style>

<style name="DATE" group="debug">
	<font-family>	Helvetica	</>
	<font-size>	18	</>
	<line-spacing>	20	</>
	<space-before>	18	</>
	<justification>	Left	</>
	<break-before>	True	</>
</style>

<style name="DESC" group="debug">
	<space-before>	5	</>
	<break-before>	on	</>
</style>

<style name="EBTDOC" group="debug">
	<font-family>	times	</>
	<font-weight>	Medium	</>
	<font-size>	14	</>
	<left-indent>	10	</>
	<line-spacing>	12	</>
	<title-tag>	FRONT,TITLEPG,TITLE	</>
</style>

<style name="EMPH" group="debug">
	<font-slant>	Italics	</>
</style>

<style name="EMPY" group="debug">
	<break-before>	on	</>
</style>

<style name="FCT" group="debug">
	<hrule>	top	</>
	<break-before>	on	</>
</style>

<style name="FCT,NAME" group="debug">
	<font-weight>	Bold	</>
	<space-before>	8	</>
	<break-before>	on	</>
</style>

<style name="I" group="debug">
	<font-family>	&std.font	</>
	<font-slant>	Italics	</>
	<font-size>	&std.font-size	</>
	<line-spacing>	&std.line-spacing	</>
</style>

<style name="ITEM" group="debug">
	<left-indent>	+=16	</>
	<space-after>	0	</>
	<break-before>	False	</>
	<column>	True	</>
</style>

<style name="ITEM,P" group="debug">
</style>

<style name="LIST" group="debug">
	<first-indent>	0	</>
	<space-before>	5	</>
	<space-after>	1	</>
	<break-before>	True	</>
</style>

<style name="MARKER" group="debug">
	<font-family>	symbol	</>
	<font-weight>	Medium	</>
	<space-before>	5	</>
	<break-before>	True	</>
</style>

<style name="MODULE" group="debug">
	<break-before>	True	</>
	<title-tag>	NAME	</>
</style>

<style name="MODULE,NAME" group="debug">
	<font-family>	&title.font	</>
	<font-weight>	Bold	</>
	<font-size>	14	</>
	<foreground>	&title.color	</>
	<line-spacing>	14	</>
	<space-after>	10	</>
	<break-before>	on	</>
</style>

<style name="NAME" group="debug">
	<break-before>	on	</>
</style>

<style name="NOTICE" group="debug">
	<font-family>	Helvetica	</>
	<font-size>	14	</>
	<line-spacing>	16	</>
	<space-before>	20	</>
	<break-before>	True	</>
</style>

<style name="OVERVIEW" group="debug">
	<break-before>	on	</>
</style>

<style name="OWNER" group="debug">
	<font-family>	Helvetica	</>
	<font-size>	18	</>
	<line-spacing>	20	</>
	<space-before>	18	</>
	<justification>	Left	</>
	<break-before>	True	</>
</style>

<style name="P" group="debug">
	<font-family>	&std.font	</>
	<font-size>	&std.font-size	</>
	<line-spacing>	&std.line-spacing	</>
	<space-before>	5	</>
	<break-before>	on	</>
</style>

<style name="SECTION" group="debug">
	<break-before>	on	</>
</style>

<style name="SUBTITLE" group="debug">
	<font-family>	Helvetica	</>
	<font-size>	18	</>
	<line-spacing>	20	</>
	<space-before>	18	</>
	<justification>	Left	</>
	<break-before>	True	</>
</style>

<style name="TEMPLATE" group="debug">
	<font-family>	courier	</>
	<font-size>	10	</>
	<line-spacing>	9	</>
	<space-before>	5	</>
	<space-after>	5	</>
	<justification>	verbatim	</>
	<break-before>	on	</>
</style>

<style name="TITLE" group="debug">
	<font-family>	&title.font	</>
	<font-weight>	Bold	</>
	<font-size>	12	</>
	<foreground>	&title.color	</>
	<space-before>	10	</>
	<break-before>	True	</>
</style>

<style name="TOCPG" group="debug">
	<hide>	Children	</>
</style>

<style name="XREF" group="debug">
	<font-family>	&std.font	</>
	<font-size>	&std.font-size	</>
	<foreground>	&hottext.foreground	</>
	<line-spacing>	&std.line-spacing	</>
	<break-before>	on	</>
</style>



<?INSTED COMMENT: UNGROUPED STYLES FOLLOW>

<style name="#ROOT">
	<break-before>	Line	</>
</style>

<style name="#TAGS">
	<font-weight>	Bold	</>
</style>

<style name="EXTREF">
	<font-family>	&std.font	</>
	<font-size>	&std.font-size	</>
	<foreground>	&hottext.foreground	</>
	<line-spacing>	&std.line-spacing	</>
</style>

<style name="TITLEPG,TITLE" group="title">
	<font-family>	&title.font	</>
	<font-size>	24	</>
	<foreground>	&title.color	</>
	<line-spacing>	28	</>
</style>

<style name="VERBATIM">
	<font-family>	courier	</>
	<font-size>	&std.font-size	</>
	<line-spacing>	&std.line-spacing	</>
</style>



</sheet>

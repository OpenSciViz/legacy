<!-- Print Style sheet for the default browser view of AAP documents.

  This uses the ANSI AAP tag set, but has a few additions for
  supporting hypertext elements.

  Copyright 1990-1994, Electronic Book Technologies.  All rights reserved.

 *************************************************************      
 Following entities are used to provide names for common values,
     which can then be changed throughout the style sheet just by
     changing their definitions here.                                   
 *************************************************************      -->

<!ENTITY	link.indent	CDATA	"12"	>
<!ENTITY	link.side	CDATA	"right"	>
<!ENTITY	min.left	CDATA	"35"	>
<!ENTITY	sample.side	CDATA	"left"	>
<!ENTITY	std.color	CDATA	"black"	>
<!ENTITY	std.font	CDATA	"Times New Roman"	>
<!ENTITY	std.justify	CDATA	"left"	>
<!ENTITY	std.leading	CDATA	"12"	>
<!ENTITY	std.left	CDATA	"35"	>
<!ENTITY	std.right	CDATA	"35"	>
<!ENTITY	std.size	CDATA	"10"	>
<!ENTITY	std.slant	CDATA	"roman"	>
<!ENTITY	std.vert	CDATA	"12"	>
<!ENTITY	std.weight	CDATA	"medium"	>
<!ENTITY	title.color	CDATA	"black"	>
<!ENTITY	title.font	CDATA	"Arial"	>
<!ENTITY	title.justify	CDATA	"left"	>
<!ENTITY	title.leading	CDATA	"16"	>
<!ENTITY	title.size	CDATA	"14"	>

<sheet >



<?INSTED COMMENT: GROUP equations>

<group name="equations">
	<hide>	Children	</>
	<inline>	equation target=me()	</>
</group>

<style name="F" group="equations">
</style>

<style name="FD" group="equations">
	<font-size>	12	</>
	<left-indent>	+=20	</>
	<space-before>	10	</>
	<justification>	Left	</>
	<break-before>	Line	</>
</style>



<?INSTED COMMENT: GROUP titles>

<group name="titles">
	<font-family>	&title.font	</>
	<justification>	Left	</>
	<break-before>	True	</>
</group>

<!-- Chapter (CHP) title -->
<style name="CT" group="titles">
	<font-size>	18	</>
	<line-spacing>	24	</>
	<space-before>	36	</>
</style>

<style name="SS1,ST" group="titles">
	<font-slant>	italics	</>
	<!-- Subsection (SS1) title -->
	<font-size>	12	</>
	<line-spacing>	14	</>
	<space-before>	12	</>
</style>

<style name="SS2,ST" group="titles">
	<!-- Subsection (SS2) title -->
	<font-size>	10	</>
	<line-spacing>	12	</>
	<space-before>	12	</>
</style>

<style name="SS3,ST" group="titles">
	<!-- Subsection (SS3) title -->
	<font-size>	10	</>
	<line-spacing>	12	</>
	<space-before>	12	</>
</style>

<!-- Section titles not otherwise specified by qualified styles -->
<style name="ST" group="titles">
	<font-weight>	bold	</>
	<font-size>	14	</>
	<line-spacing>	20	</>
	<space-before>	24	</>
</style>

<!-- Book's main title -->
<style name="TI" group="titles">
	<font-size>	24	</>
	<line-spacing>	28	</>
</style>



<?INSTED COMMENT: UNGROUPED STYLES FOLLOW>

<!-- Default settings for printing of annotations -->
<style name="#ANNOT">
	<font-family>	&std.font	</>
	<font-weight>	medium	</>
	<font-slant>	roman	</>
	<font-video>	regular	</>
	<font-size>	12	</>
	<character-set>	iso8859-1	</>
	<score>	none	</>
	<line-spacing>	24	</>
	<break-before>	line	</>
	<break-after>	line	</>
</style>

<style name="#FOOTER">
	<font-family>	&std.font	</>
	<font-weight>	&std.weight	</>
	<font-slant>	&std.slant	</>
	<font-size>	&std.size	</>
	<score>	None	</>
	<left-indent>	&std.left	</>
	<right-indent>	&std.right	</>
	<line-spacing>	&std.vert	</>
	<justification>	&std.justify	</>
	<text-before>Shuttle Press Kit:  Page pagenum()</>
</style>

<style name="#ROOT">
	<break-before>	Line	</>
</style>

<!-- Default settings for SDATA entities from our entity sets -->
<style name="#SDATA">
	<font-family>	attr(font)	</>
	<font-weight>	Medium	</>
	<font-slant>	Roman	</>
	<character-set>	attr(charset)	</>
	<break-before>	None	</>
	<text-before>char(attr(code))</>
</style>

<style name="#TAGS">
	<font-weight>	Bold	</>
</style>

<!-- ANCHOR element added as destination for hypertext links.  -->
<style name="ANCHOR">
	<font-slant>	Italics	</>
	<foreground>	red	</>
	<right-indent>	12	</>
	<icon-position>	right	</>
	<break-before>	True	</>
	<icon-type>	inlink	</>
</style>

<style name="APR">
	<font-slant>	italics	</>
	<right-indent>	12	</>
	<icon-position>	right	</>
	<icon-type>	inlink	</>
</style>

<style name="APT">
	<!-- Appendix (APP) title -->
	<font-family>	&title.font	</>
	<font-weight>	Bold	</>
	<font-size>	24	</>
	<foreground>	black	</>
	<line-spacing>	28	</>
	<justification>	left	</>
	<break-before>	True	</>
</style>

<style name="ART">
	<break-before>	True	</>
	<inline>	raster filename=@(ID)	</>
</style>

<style name="ART,#TEXT-BEFORE">
	<font-family>	&title.font	</>
	<font-weight>	bold	</>
	<font-size>	12	</>
	<foreground>		</>
	<line-spacing>	14	</>
	<space-before>	4	</>
	<space-after>	12	</>
	<justification>	left	</>
</style>

<style name="ARTR">
	<font-weight>	bold	</>
	<font-slant>	italics	</>
	<right-indent>	12	</>
	<icon-position>	right	</>
	<icon-type>	inlink	</>
</style>

<style name="AU">
	<font-family>	&title.font	</>
	<font-size>	18	</>
	<line-spacing>	20	</>
	<space-before>	18	</>
	<justification>	left	</>
	<break-before>	True	</>
</style>

<style name="B">
	<font-weight>	Bold	</>
</style>

<style name="BI">
	<font-weight>	Bold	</>
	<font-slant>	Italics	</>
</style>

<!-- ************************************************************* 
 Styles for main book element.
-->
<style name="BOOK">
	<font-family>	&std.font	</>
	<font-weight>	medium	</>
	<font-slant>	roman	</>
	<font-size>	10	</>
	<foreground>	black	</>
	<left-indent>	35	</>
	<right-indent>	35	</>
	<line-spacing>	12	</>
</style>

<style name="BQ">
	<left-indent>	+= 18	</>
	<break-before>	True	</>
</style>

<!-- paragraphs within table cells -->
<style name="C,P">
	<break-before>	False	</>
</style>

<style name="CAU">
	<font-family>	&title.font	</>
	<font-size>	18	</>
	<line-spacing>	20	</>
	<space-before>	18	</>
	<justification>	left	</>
	<break-before>	True	</>
</style>

<style name="CRT">
	<break-before>	True	</>
</style>

<style name="CYR">
	<font-family>	cyrillic	</>
</style>

<!-- Definition description -->
<style name="DD">
	<space-before>	0	</>
	<break-before>	False	</>
</style>

<style name="DD,P">
	<space-before>	5	</>
	<break-before>	True	</>
</style>

<!-- Definition description header -->
<style name="DDHD">
	<font-weight>	Bold	</>
	<space-before>	5	</>
	<break-before>	True	</>
</style>

<!-- Definition list -->
<style name="DL">
	<left-indent>	35	</>
	<first-indent>	0	</>
	<space-before>	0	</>
	<break-before>	True	</>
</style>

<!-- link to other book -->
<style name="DOCLINK">
	<font-slant>	Italics	</>
	<foreground>	red	</>
	<right-indent>	12	</>
	<icon-position>	right	</>
	<break-before>	True	</>
	<icon-type>	exlink	</>
</style>

<!-- Definition term -->
<style name="DT">
	<font-weight>	Bold	</>
	<space-before>	8	</>
	<break-before>	True	</>
</style>

<!-- Definition term header -->
<style name="DTHD">
	<font-weight>	Bold	</>
	<space-before>	8	</>
	<break-before>	True	</>
</style>

<style name="E1">
	<font-weight>	Bold	</>
</style>

<style name="E2">
	<font-slant>	Italics	</>
</style>

<style name="E3">
	<font-weight>	Bold	</>
	<font-slant>	Italics	</>
</style>

<style name="EMQ">
	<left-indent>	+= 18	</>
	<break-before>	True	</>
</style>

<style name="FIGR">
	<right-indent>	12	</>
	<icon-position>	right	</>
	<!-- Reference to figure -->
	<icon-type>	inlink	</>
</style>

<style name="FN">
	<right-indent>	12	</>
	<icon-position>	right	</>
	<hide>	Text	</>
	<script>	ebt-footnote	</>
	<icon-type>	footnote	</>
</style>

<!-- Foonote reference mark (e.g. *) -->
<style name="FNR">
	<font-weight>	Bold	</>
	<vertical-offset>	4	</>
</style>

<style name="GK">
	<font-family>	greek	</>
</style>

<!-- Glossary list -->
<style name="GL">
	<left-indent>	35	</>
	<first-indent>	0	</>
	<space-before>	0	</>
	<break-before>	True	</>
</style>

<style name="H">
	<font-family>	&title.font	</>
	<font-weight>	Bold	</>
	<font-size>	10	</>
	<foreground>	black	</>
	<line-spacing>	12	</>
	<justification>	left	</>
	<break-before>	True	</>
</style>

<style name="H1">
	<font-family>	&title.font	</>
	<font-weight>	Bold	</>
	<font-size>	10	</>
	<foreground>	black	</>
	<line-spacing>	12	</>
	<justification>	left	</>
	<break-before>	True	</>
</style>

<style name="H2">
	<font-family>	&title.font	</>
	<font-weight>	Bold	</>
	<font-size>	10	</>
	<foreground>	black	</>
	<line-spacing>	12	</>
	<justification>	left	</>
	<break-before>	True	</>
</style>

<style name="H3">
	<font-family>	&title.font	</>
	<font-weight>	Bold	</>
	<font-size>	10	</>
	<foreground>	black	</>
	<line-spacing>	12	</>
	<justification>	left	</>
	<break-before>	True	</>
</style>

<style name="H4">
	<font-family>	&title.font	</>
	<font-weight>	Bold	</>
	<font-size>	10	</>
	<foreground>	black	</>
	<line-spacing>	12	</>
	<justification>	left	</>
	<break-before>	True	</>
</style>

<style name="IDX">
	<hide>	Text	</>
</style>

<style name="IT">
	<font-slant>	Italics	</>
</style>

<style name="ITM">
	<space-before>	0	</>
	<break-before>	False	</>
</style>

<!-- Item list -->
<style name="ITML">
	<font-family>	courier	</>
	<left-indent>	35	</>
	<first-indent>	0	</>
	<line-spacing>	12	</>
	<space-before>	0	</>
	<break-before>	False	</>
</style>

<style name="L">
	<left-indent>	35	</>
	<first-indent>	0	</>
	<space-before>	0	</>
	<break-before>	True	</>
	<break-after>	True	</>
</style>

<style name="L1">
	<left-indent>	35	</>
	<first-indent>	0	</>
	<space-before>	0	</>
	<break-before>	True	</>
	<break-after>	True	</>
</style>

<style name="L2">
	<left-indent>	62	</>
	<first-indent>	0	</>
	<space-before>	0	</>
	<break-before>	True	</>
	<break-after>	True	</>
</style>

<style name="L3">
	<left-indent>	80	</>
	<first-indent>	0	</>
	<space-before>	0	</>
	<break-before>	True	</>
	<break-after>	True	</>
</style>

<style name="L4">
	<left-indent>	85	</>
	<first-indent>	0	</>
	<space-before>	0	</>
	<break-before>	True	</>
	<break-after>	True	</>
</style>

<style name="L5">
	<left-indent>	90	</>
	<first-indent>	0	</>
	<space-before>	0	</>
	<break-before>	True	</>
	<break-after>	True	</>
</style>

<style name="LH">
	<font-weight>	Bold	</>
	<break-before>	True	</>
</style>

<style name="LI">
	<space-before>	5	</>
	<break-before>	False	</>
</style>

<style name="LI,P">
	<space-before>	0	</>
	<break-before>	True	</>
</style>

<style name="LINE">
	<space-before>	0	</>
	<break-before>	True	</>
</style>

<style name="NIT">
	<right-indent>	12	</>
	<icon-position>	right	</>
	<hide>	Text	</>
	<script>	ebt-footnote title=Note	</>
	<icon-type>	footnote	</>
</style>

<style name="NTR">
	<right-indent>	12	</>
	<icon-position>	right	</>
	<icon-type>	inlink	</>
</style>

<style name="ONM">
	<break-after>	True	</>
</style>

<style name="P">
	<font-weight>	medium	</>
	<space-before>	12	</>
	<break-before>	True	</>
</style>

<style name="PDT">
	<font-family>	&title.font	</>
	<font-size>	10	</>
	<line-spacing>	12	</>
	<justification>	left	</>
	<break-before>	True	</>
</style>

<style name="PT">
	<!-- Part (PT) title -->
	<font-family>	&title.font	</>
	<font-weight>	Bold	</>
	<font-size>	24	</>
	<foreground>	black	</>
	<line-spacing>	28	</>
	<space-before>	36	</>
	<justification>	left	</>
	<break-before>	True	</>
</style>

<style name="Q">
	<text-before>   " </>
</style>

<style name="RLINK">
	<font-slant>	Italics	</>
	<foreground>	red	</>
	<right-indent>	12	</>
	<icon-position>	right	</>
	<break-before>	True	</>
	<icon-type>	inlink	</>
</style>

<style name="RM">
	<font-weight>	Medium	</>
	<font-slant>	Roman	</>
</style>

<style name="SBR">
	<right-indent>	12	</>
	<icon-position>	right	</>
	<hide>	Text	</>
	<script>	ebt-footnote title=Sidebar	</>
	<icon-type>	footnote	</>
</style>

<style name="SBT">
	<font-family>	&title.font	</>
	<font-size>	10	</>
	<line-spacing>	12	</>
	<justification>	left	</>
	<break-before>	True	</>
</style>

<style name="SCP">
	<font-size>	-=2	</>
</style>

<style name="SIT1">
	<left-indent>	+=12	</>
	<space-before>	5	</>
	<break-before>	True	</>
</style>

<style name="SIT2">
	<left-indent>	+=12	</>
	<space-before>	5	</>
	<break-before>	True	</>
</style>

<style name="SIT3">
	<left-indent>	+=12	</>
	<space-before>	5	</>
	<break-before>	True	</>
</style>

<style name="SOUND">
	<right-indent>	12	</>
	<icon-position>	right	</>
	<script>	ebt-launch cmd="sound_script var(fig_dir)/@(ID)"	</>
	<icon-type>	sound	</>
</style>

<style name="TABLE">
	<icon-position>	Right	</>
	<hide>	Text	</>
	<script>	ebt-table stylesheet=table.pri	</>
	<icon-type>	table	</>
</style>

<style name="TBL">
	<icon-position>	Right	</>
	<hide>	Text	</>
	<script>	ebt-table stylesheet=table.pri	</>
	<icon-type>	table	</>
</style>

<style name="TOC">
	<hide>	Text	</>
</style>

<style name="VECTOR">
</style>

<style name="XREF">
	<font-slant>	Italics	</>
	<foreground>	red	</>
	<right-indent>	12	</>
	<icon-position>	right	</>
	<break-before>	True	</>
	<icon-type>	inlink	</>
</style>



</sheet>

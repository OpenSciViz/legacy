<!-- Style sheet for the default browser view of AAP documents.

  This uses the ANSI AAP tag set, but has a few additions for
  supporting hypertext elements.

  Differs by showing cross-reference links inline.

  Copyright 1990, Electronic Book Technologies.  All rights reserved.
-->

<!ENTITY	art.color	CDATA	"dark green"	>
<!ENTITY	hottext.fontfam	CDATA	"lucida"	>
<!ENTITY	hottext.foreground	CDATA	"red"	>
<!ENTITY	hottext.slant	CDATA	"Roman"	>
<!ENTITY	hottext.weight	CDATA	"Bold"	>
<!ENTITY	link.indent	CDATA	"12"	>
<!ENTITY	link.side	CDATA	"right"	>
<!ENTITY	std.font	CDATA	"New Century Schoolbook"	>
<!ENTITY	std.font-size	CDATA	"14"	>
<!ENTITY	std.indent	CDATA	"24"	>
<!ENTITY	std.leftindent	CDATA	"10"	>
<!ENTITY	std.line-spacing	CDATA	"16"	>
<!ENTITY	std.rightindent	CDATA	"12"	>
<!ENTITY	std.vert	CDATA	"12"	>
<!ENTITY	subscript	CDATA	"-3"	>
<!ENTITY	text.leading	CDATA	"14"	>
<!ENTITY	text.smallest	CDATA	"12"	>
<!ENTITY	title.color	CDATA	"blue"	>
<!ENTITY	title.font	CDATA	"lucida"	>
<!ENTITY	title.justify	CDATA	"left"	>
<!ENTITY	verbatim.font	CDATA	"courier"	>
<!ENTITY	verbatim.font-size	CDATA	"10"	>
<!ENTITY	verbatim.line-spacing	CDATA	"12"	>

<sheet AUTHOR="sjd" DATE="11/19/90" VERSION="1.5">



<?INSTED COMMENT: GROUP equations>

<group name="equations">
	<hide>	Children	</>
	<break-before>	False	</>
	<break-after>	False	</>
	<script>	ebt-reveal stylesheet="eqnpopup.rev"	</>
	<inline>	equation target = me()	</>
</group>

<style name="F" group="equations">
</style>

<style name="FD" group="equations">
	<font-size>	18	</>
	<left-indent>	+=25	</>
	<space-before>	10	</>
	<space-after>	10	</>
	<break-before>	True	</>
	<break-after>	True	</>
</style>



<?INSTED COMMENT: UNGROUPED STYLES FOLLOW>

<!-- ************************************************************* 
 Following entities are used to provide names for common values,
     which can then be changed throughout the style sheet just by
     changing their definitions here.

 link.side is used for all but ARTR links. 
 ************************************************************* 
  '#ROOT' is the starting point for the document.
-->
<style name="#ROOT">
	<font-family>	helvetica	</>
	<font-weight>	m	</>
	<font-slant>	r	</>
	<font-size>	12	</>
	<foreground>	black	</>
	<left-indent>	10	</>
	<right-indent>	12	</>
	<line-spacing>	14	</>
</style>

<style name="#SDATA">
	<font-family>	attr(font)	</>
	<font-weight>	attr(weight)	</>
	<font-size>	12	</>
	<character-set>	attr(charset)	</>
	<line-spacing>	14	</>
	<text-before> char(attr(code)) </>
</style>

<style name="#TAGS">
	<font-weight>	Bold	</>
	<foreground>	purple	</>
</style>

<!-- ************************************************************* 
 Styles for elements beyond the AAP set, for media/hypertext:
-->
<style name="ANCHOR">
	<font-slant>	italic	</>
	<foreground>	red	</>
	<right-indent>	12	</>
	<icon-position>	right	</>
	<break-before>	on	</>
	<script>	ebt-link tname=RID tvalue=@(ID)	</>
	<!-- Destination of hypertext link -->
	<icon-type>	inlink	</>
</style>

<!-- ************************************************************* 
 Styles for hypertextual elements that are cross-references:
-->
<style name="APR">
	<right-indent>	12	</>
	<icon-position>	right	</>
	<script>	ebt-link tname=ID tvalue=@(RID)	</>
	<!-- Reference to appendix -->
	<icon-type>	inlink	</>
</style>

<style name="APT">
	<!-- Appendix (APP) title -->
	<font-family>	helvetica	</>
	<font-weight>	bold	</>
	<font-size>	24	</>
	<foreground>	blue	</>
	<line-spacing>	28	</>
	<justification>	left	</>
	<break-before>	on	</>
</style>

<!-- ************************************************************* 
 Styles for hypertextual elements other than cross-references:
-->
<style name="ART">
	<select>	ART,ART.toupper(attr(type))	</>
</style>

<style name="ART,#TEXT-BEFORE">
	<font-weight>	bold	</>
	<foreground>	&art.color	</>
	<space-before>	10	</>
	<space-after>	gamut(toupper(attr(type)),'DRAWING RASTER VECTOR', '10 0 0','10')	</>
	<justification>	left	</>
	<break-before>	line	</>
	<break-after>	gamut(toupper(attr(type)),'DRAWING RASTER VECTOR','line off off','off')	</>
</style>

<style name="ART.DRAWING">
	<break-before>	line	</>
	<script>	ebt-raster filename=@(ID) title="@(title)"	</>
	<!-- Actual artwork -->
	<inline>	raster filename=@(ID)	</>
	<text-before>Figure gcnum(): @(title)</>
</style>

<style name="ART.RASTER">
	<right-indent>	12	</>
	<space-before>	8	</>
	<space-after>	8	</>
	<icon-position>	right	</>
	<script>	ebt-raster filename=@(ID) title="@(title)"	</>
	<!-- Actual artwork -->
	<icon-type>	raster	</>
	<text-before>Double-click on the icon to view Figure gcnum(): attr(title)  </>
</style>

<style name="ART.VECTOR">
	<right-indent>	12	</>
	<space-before>	8	</>
	<space-after>	8	</>
	<icon-position>	right	</>
	<script>	ebt-raster filename=@(ID) title="@(title)"	</>
	<!-- CGM-format vector graphic -->
	<icon-type>	raster	</>
	<text-before>Double-click on the icon to view Figure gcnum(): attr(title)  </>
</style>

<style name="ARTR">
	<font-weight>	bold	</>
	<foreground>	&hottext.foreground	</>
	<right-indent>	12	</>
	<icon-position>	inline	</>
	<!-- Cross reference to another chapter  -->
	<break-before>	line	</>
	<script>	ebt-link tname=ID tvalue=@(RID)	</>
	<icon-type>	inlink	</>
</style>

<style name="AU">
	<font-family>	helvetica	</>
	<font-size>	18	</>
	<line-spacing>	20	</>
	<space-before>	18	</>
	<justification>	left	</>
	<break-before>	on	</>
</style>

<style name="B">
	<font-weight>	b	</>
</style>

<style name="BI">
	<font-weight>	b	</>
	<font-slant>	i	</>
</style>

<!-- ************************************************************* 
 Styles for main body elements (most have no formatting effect):
-->
<style name="BOOK">
	<font-family>	helvetica	</>
	<font-weight>	m	</>
	<font-slant>	r	</>
	<font-size>	12	</>
	<foreground>	black	</>
	<left-indent>	10	</>
	<right-indent>	12	</>
	<line-spacing>	14	</>
</style>

<style name="BQ">
	<left-indent>	+= 18	</>
	<break-before>	on	</>
</style>

<style name="C">
	<!-- Properties common to all columns -->
	<width>	180	</>
	<vrule>	None	</>
	<break-before>	off	</>
	<select>	C,C#cnum()	</>
	<column>	on	</>
</style>

<!-- Following styles are definitions for each column of the table.
     They can be generated by hand, or more conveniently using the
     'genTable' utility program.
-->
<style name="C#1">
	<!-- First column -->
	<left-indent>	10	</>
	<justification>	l	</>
</style>

<style name="C#2">
	<!-- Second column -->
	<left-indent>	200	</>
	<justification>	l	</>
</style>

<style name="C#3">
	<!-- Third column -->
	<left-indent>	390	</>
	<justification>	l	</>
</style>

<style name="C#4">
	<!-- Fourth column -->
	<left-indent>	580	</>
	<justification>	l	</>
</style>

<style name="C#5">
	<!-- Fifth column -->
	<left-indent>	800	</>
	<justification>	l	</>
</style>

<!-- end of TABLE*INLINE styles -->
<style name="C,P">
	<!-- paragraphs within table cells -->
	<break-before>	off	</>
</style>

<style name="CAU">
	<font-family>	helvetica	</>
	<font-size>	18	</>
	<line-spacing>	20	</>
	<space-before>	18	</>
	<justification>	left	</>
	<break-before>	on	</>
</style>

<style name="CT">
	<!-- Chapter (CHP) title -->
	<font-family>	helvetica	</>
	<font-weight>	bold	</>
	<font-size>	18	</>
	<foreground>	blue	</>
	<line-spacing>	24	</>
	<space-before>	36	</>
	<justification>	left	</>
	<break-before>	on	</>
</style>

<style name="CYR">
	<!-- If not available, #(roots) -->
	<font-family>	cyrillic	</>
</style>

<style name="DD">
	<!-- Definition description -->
	<space-before>	0	</>
	<break-before>	off	</>
</style>

<style name="DD,P">
	<space-before>	5	</>
	<break-before>	on	</>
</style>

<style name="DDHD">
	<!-- Definition description header -->
	<font-weight>	b	</>
	<space-before>	5	</>
	<break-before>	on	</>
</style>

<!-- ****************** Definition lists ****************** -->
<style name="DL">
	<left-indent>	24	</>
	<first-indent>	0	</>
	<space-before>	0	</>
	<break-before>	on	</>
</style>

<style name="DOCLINK">
	<font-slant>	italic	</>
	<foreground>	red	</>
	<right-indent>	12	</>
	<icon-position>	right	</>
	<break-before>	on	</>
	<script>	ebt-link book="@DOC" tname=ID tvalue=@(RID)	</>
	<!-- link to other book -->
	<icon-type>	exlink	</>
</style>

<style name="DT">
	<!-- Definition term -->
	<font-weight>	b	</>
	<space-before>	8	</>
	<break-before>	on	</>
</style>

<style name="DTHD">
	<!-- Definition term header -->
	<font-weight>	b	</>
	<space-before>	8	</>
	<break-before>	on	</>
</style>

<!-- ************************************************************* 
 Styles for sub-paragraph elements, such as emphasis:
-->
<style name="E1">
	<font-weight>	b	</>
	<vrule>	None	</>
</style>

<style name="E2">
	<font-slant>	i	</>
</style>

<style name="E3">
	<font-weight>	b	</>
	<font-slant>	i	</>
</style>

<style name="EMQ">
	<left-indent>	+= 18	</>
	<break-before>	on	</>
</style>

<style name="FIGR">
	<right-indent>	12	</>
	<icon-position>	right	</>
	<script>	ebt-link tname=ID tvalue=@(RID)	</>
	<!-- Reference to figure -->
	<icon-type>	inlink	</>
</style>

<style name="FN">
	<right-indent>	12	</>
	<icon-position>	right	</>
	<hide>	text	</>
	<script>	ebt-footnote	</>
	<!-- Footnote -->
	<icon-type>	footnote	</>
</style>

<style name="FNR">
	<font-weight>	b	</>
	<!-- Foonote reference mark (e.g. *) -->
	<vertical-offset>	4	</>
</style>

<style name="GK">
	<!-- If not available, #(roots) -->
	<font-family>	greek	</>
</style>

<!-- ****************** Glossary lists ****************** -->
<style name="GL">
	<left-indent>	24	</>
	<first-indent>	0	</>
	<space-before>	0	</>
	<break-before>	on	</>
</style>

<style name="H">
	<font-family>	helvetica	</>
	<font-weight>	bold	</>
	<font-size>	12	</>
	<foreground>	blue	</>
	<line-spacing>	14	</>
	<justification>	left	</>
	<break-before>	on	</>
</style>

<style name="H1">
	<font-family>	helvetica	</>
	<font-weight>	bold	</>
	<font-size>	12	</>
	<foreground>	blue	</>
	<line-spacing>	14	</>
	<justification>	left	</>
	<break-before>	on	</>
</style>

<style name="H2">
	<font-family>	helvetica	</>
	<font-weight>	bold	</>
	<font-size>	12	</>
	<foreground>	blue	</>
	<line-spacing>	14	</>
	<justification>	left	</>
	<break-before>	on	</>
</style>

<style name="H3">
	<font-family>	helvetica	</>
	<font-weight>	bold	</>
	<font-size>	12	</>
	<foreground>	blue	</>
	<line-spacing>	14	</>
	<justification>	left	</>
	<break-before>	on	</>
</style>

<style name="H4">
	<font-family>	helvetica	</>
	<font-weight>	bold	</>
	<font-size>	12	</>
	<foreground>	blue	</>
	<line-spacing>	14	</>
	<justification>	left	</>
	<break-before>	on	</>
</style>

<style name="IDX">
	<!-- Manual index -->
	<hide>	text	</>
</style>

<style name="IT">
	<font-slant>	i	</>
</style>

<style name="ITM">
	<space-before>	0	</>
	<break-before>	off	</>
</style>

<!-- ****************** Item lists ****************** -->
<style name="ITML">
	<font-family>	courier	</>
	<left-indent>	24	</>
	<first-indent>	0	</>
	<line-spacing>	12	</>
	<space-before>	0	</>
	<break-before>	off	</>
</style>

<!-- ****************** Types of lists ****************** -->
<style name="L">
	<left-indent>	24	</>
	<first-indent>	0	</>
	<space-before>	0	</>
	<break-before>	on	</>
	<break-after>	on	</>
</style>

<style name="L1">
	<left-indent>	24	</>
	<first-indent>	0	</>
	<space-before>	0	</>
	<break-before>	on	</>
	<break-after>	on	</>
</style>

<style name="L2">
	<left-indent>	62	</>
	<first-indent>	0	</>
	<space-before>	0	</>
	<break-before>	on	</>
	<break-after>	on	</>
</style>

<style name="L3">
	<left-indent>	80	</>
	<first-indent>	0	</>
	<space-before>	0	</>
	<break-before>	on	</>
	<break-after>	on	</>
</style>

<style name="L4">
	<left-indent>	85	</>
	<first-indent>	0	</>
	<space-before>	0	</>
	<break-before>	on	</>
	<break-after>	on	</>
</style>

<style name="L5">
	<left-indent>	90	</>
	<first-indent>	0	</>
	<space-before>	0	</>
	<break-before>	on	</>
	<break-after>	on	</>
</style>

<style name="LH">
	<font-weight>	b	</>
	<break-before>	on	</>
</style>

<!-- ****************** Items within lists ****************** -->
<style name="LI">
	<space-before>	5	</>
	<break-before>	off	</>
</style>

<style name="LI,P">
	<space-before>	0	</>
	<break-before>	on	</>
</style>

<style name="LINE">
	<space-before>	0	</>
	<break-before>	on	</>
</style>

<style name="NIT">
	<right-indent>	12	</>
	<icon-position>	right	</>
	<hide>	text	</>
	<script>	ebt-footnote title=Note	</>
	<!-- Note-in-text -->
	<icon-type>	footnote	</>
</style>

<style name="NTR">
	<right-indent>	12	</>
	<icon-position>	right	</>
	<script>	ebt-link tname=ID tvalue=@(RID)	</>
	<!-- Reference to note-in-text -->
	<icon-type>	inlink	</>
</style>

<!-- ************************************************************* 
 Styles for paragraph/text-block level elements:
-->
<style name="P">
	<space-before>	12	</>
	<break-before>	on	</>
</style>

<style name="PDT">
	<font-family>	helvetica	</>
	<font-size>	12	</>
	<line-spacing>	14	</>
	<justification>	left	</>
	<break-before>	on	</>
</style>

<style name="PT">
	<!-- Part (PT) title -->
	<font-family>	helvetica	</>
	<font-weight>	bold	</>
	<font-size>	24	</>
	<foreground>	blue	</>
	<line-spacing>	28	</>
	<space-before>	36	</>
	<justification>	left	</>
	<break-before>	on	</>
</style>

<style name="RB">
	<font-weight>	bold	</>
	<foreground>	&hottext.foreground	</>
	<score>	under	</>
	<right-indent>	12	</>
	<!-- Cross reference to another chapter  -->
	<script>	ebt-link tname=ID tvalue=@(RID)	</>
</style>

<style name="RM">
	<font-weight>	m	</>
	<font-slant>	r	</>
</style>

<!-- Since AAP doesn't provide table-column-headers, one way to get headers
     is to make each cell in the first row emphatic (using E1 or similar);
     could also get headers by defining a style for ROW#(1), etc., but this
     would not be appropriate for all AAP-conforming files.
-->
<style name="ROW">
	<font-weight>	m	</>
	<left-indent>	10	</>
	<space-before>	8	</>
	<hrule>	After	</>
	<break-before>	on	</>
</style>

<style name="SBR">
	<right-indent>	12	</>
	<icon-position>	right	</>
	<hide>	text	</>
	<script>	ebt-footnote title=Sidebar	</>
	<!-- Sidebar -->
	<icon-type>	footnote	</>
</style>

<style name="SBT">
	<font-family>	helvetica	</>
	<font-size>	12	</>
	<line-spacing>	14	</>
	<justification>	left	</>
	<break-before>	on	</>
</style>

<style name="SCP">
	<!-- Use smaller type for now. -->
	<font-size>	-=2	</>
</style>

<style name="SIT1">
	<left-indent>	+=12	</>
	<space-before>	5	</>
	<break-before>	on	</>
</style>

<style name="SIT2">
	<left-indent>	+=12	</>
	<space-before>	5	</>
	<break-before>	on	</>
</style>

<style name="SIT3">
	<left-indent>	+=12	</>
	<space-before>	5	</>
	<break-before>	on	</>
</style>

<style name="SOUND">
	<right-indent>	12	</>
	<icon-position>	right	</>
	<script>	ebt-launch cmd="sound_script var(fig_dir)/@(ID)"	</>
	<!-- Digitized sound -->
	<icon-type>	sound	</>
</style>

<style name="SS1,ST">
	<!-- Subsection (SS1) title -->
	<font-family>	helvetica	</>
	<font-weight>	bold	</>
	<font-size>	14	</>
	<foreground>	blue	</>
	<line-spacing>	17	</>
	<space-before>	12	</>
	<justification>	left	</>
	<break-before>	on	</>
</style>

<style name="SS2,ST">
	<!-- Subsection (SS2) title -->
	<font-family>	helvetica	</>
	<font-weight>	bold	</>
	<font-size>	12	</>
	<foreground>	blue	</>
	<line-spacing>	14	</>
	<space-before>	12	</>
	<justification>	left	</>
	<break-before>	on	</>
</style>

<style name="SS3,ST">
	<!-- Subsection (SS3) title -->
	<font-family>	helvetica	</>
	<font-weight>	bold	</>
	<font-size>	12	</>
	<foreground>	blue	</>
	<line-spacing>	14	</>
	<space-before>	12	</>
	<justification>	left	</>
	<break-before>	on	</>
</style>

<style name="ST">
	<!-- Section (SEC) title (see following) -->
	<font-family>	helvetica	</>
	<font-weight>	bold	</>
	<font-size>	18	</>
	<foreground>	blue	</>
	<line-spacing>	20	</>
	<space-before>	24	</>
	<justification>	left	</>
	<break-before>	on	</>
</style>

<!-- ************************************************************* 
 Style for tables:  hide them behind an icon; when clicked, they
     will appear in a separate window controlled by the".table"
     style sheet, or any one specified for the particular table instance
     (for example, by a 'table-type' attribute).
-->
<style name="TABLE">
	<icon-position>	r	</>
	<hide>	text	</>
	<script>	ebt-table	</>
	<icon-type>	table	</>
</style>

<style name="TBL">
	<select>	TBL,if(eq(attr(type),'inline'),TBL*INLINE,TBL*ICON)	</>
</style>

<style name="TBL*ICON">
	<icon-position>	right	</>
	<hide>	children	</>
	<script>	ebt-reveal stylesheet="table.rev" title="Shuttle Press Kit"	</>
	<icon-type>	table	</>
</style>

<style name="TBL*INLINE">
	<space-before>	8	</>
	<space-after>	10	</>
	<!-- styles for TBL*INLINE follow TBL*ICON style -->
	<break-before>	line	</>
</style>

<style name="TI">
	<font-family>	helvetica	</>
	<font-weight>	bold	</>
	<font-size>	24	</>
	<foreground>	blue	</>
	<line-spacing>	28	</>
	<justification>	Left	</>
	<break-before>	on	</>
</style>

<style name="TOC">
	<!-- Manual table of contents -->
	<hide>	text	</>
</style>

<style name="TT">
	<!-- Table title -->
	<font-weight>	bold	</>
	<font-size>	14	</>
	<left-indent>	25	</>
	<justification>	c	</>
	<break-before>	on	</>
</style>

<style name="XREF">
	<font-slant>	italic	</>
	<foreground>	red	</>
	<right-indent>	12	</>
	<icon-position>	right	</>
	<break-before>	on	</>
	<script>	ebt-link tname=ID tvalue=@(RID)	</>
	<!-- Origin of hypertext link -->
	<icon-type>	inlink	</>
</style>



</sheet>

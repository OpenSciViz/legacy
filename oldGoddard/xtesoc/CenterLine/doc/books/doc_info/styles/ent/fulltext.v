<!-- Template for new main (*.v) Stylesheets

  Copyright 1992, Electronic Book Technologies.  All rights reserved.
-->

<!ENTITY	art.color	CDATA	"dark green"	>
<!ENTITY	hottext.fontfam	CDATA	"lucida"	>
<!ENTITY	hottext.foreground	CDATA	"red"	>
<!ENTITY	hottext.slant	CDATA	"Roman"	>
<!ENTITY	hottext.weight	CDATA	"Bold"	>
<!ENTITY	revchgbw	CDATA	"if(attr(revision),under,)"	>
<!ENTITY	revchgcol	CDATA	"if(attr(revision),blueviolet,)"	>
<!ENTITY	std.font	CDATA	"New Century Schoolbook"	>
<!ENTITY	std.font-size	CDATA	"14"	>
<!ENTITY	std.leftindent	CDATA	"10"	>
<!ENTITY	std.line-spacing	CDATA	"16"	>
<!ENTITY	std.rightindent	CDATA	"12"	>
<!ENTITY	subscript	CDATA	"-3"	>
<!ENTITY	title.color	CDATA	"blue"	>
<!ENTITY	title.font	CDATA	"lucida"	>
<!ENTITY	verbatim.font	CDATA	"courier"	>
<!ENTITY	verbatim.font-size	CDATA	"10"	>
<!ENTITY	verbatim.line-spacing	CDATA	"12"	>

<sheet >



<?INSTED COMMENT: GROUP titles>

<group name="titles">
	<font-family>	lucida	</>
	<font-weight>	Bold	</>
	<break-before>	Line	</>
</group>

<style name="BOOK,TITLE" group="titles">
	<font-family>	&title.font	</>
	<font-size>	18	</>
	<foreground>	&title.color	</>
	<line-spacing>	18	</>
	<space-before>	10	</>
</style>

<style name="CHAP,TITLE" group="titles">
	<font-family>	&title.font	</>
	<font-size>	14	</>
	<foreground>	&title.color	</>
	<line-spacing>	14	</>
</style>



<?INSTED COMMENT: UNGROUPED STYLES FOLLOW>

<style name="#ROOT">
	<break-before>	Line	</>
</style>

<style name="#SDATA">
	<font-family>	attr(font)	</>
	<character-set>	attr(charset)	</>
	<text-before>char(attr(code))</>
</style>

<style name="#TAGS">
	<font-weight>	Bold	</>
	<foreground>	purple	</>
</style>

<style name="BOOK">
	<font-family>	New Century Schoolbook	</>
	<font-size>	14	</>
	<line-spacing>	16	</>
	<break-before>	Line	</>
</style>

<style name="CHAP">
	<space-before>	20	</>
</style>

<style name="CHARACTER">
	<width>	100	</>
	<column>	True	</>
</style>

<style name="CHARSET">
	<hide>	All	</>
</style>

<style name="CODE">
	<hide>	All	</>
</style>

<style name="DESC">
	<left-indent>	+=200	</>
	<column>	True	</>
</style>

<style name="HDROW">
	<font-weight>	Bold	</>
</style>

<style name="NAME">
	<left-indent>	+=100	</>
	<width>	90	</>
	<column>	True	</>
</style>

<style name="P">
	<font-family>	&std.font	</>
	<space-before>	10	</>
	<break-before>	Line	</>
</style>

<style name="ROW">
	<line-spacing>	18	</>
	<space-before>	4	</>
	<break-before>	Line	</>
</style>

<style name="TABLE">
	<space-before>	10	</>
</style>

<style name="VERBATIM">
	<font-family>	courier	</>
	<font-size>	10	</>
	<space-before>	10	</>
	<justification>	verbatim	</>
</style>



</sheet>

<!-- Style sheet for the default browser view of IMI documents.
  This uses the ANSI AAP tag set, but has a few additions for
  supporting hypertext elements.

  Copyright 1990, Electronic Book Technologies.  All rights reserved.
-->

<!ENTITY	big.leftindent	CDATA	"36"	>
<!ENTITY	big1.spacebef	CDATA	"20"	>
<!ENTITY	big2.spacebef	CDATA	"30"	>
<!ENTITY	big3.spacebef	CDATA	"40"	>
<!ENTITY	big4.spacebef	CDATA	"45"	>
<!ENTITY	big5.spacebef	CDATA	"60"	>
<!ENTITY	bigtitle.fontsize	CDATA	"24"	>
<!ENTITY	bigtitle.linespace	CDATA	"26"	>
<!ENTITY	hottext.fontfam	CDATA	"helvetica"	>
<!ENTITY	hottext.foreground	CDATA	"red"	>
<!ENTITY	hottext.slant	CDATA	"Italics"	>
<!ENTITY	small.fontsize	CDATA	"10"	>
<!ENTITY	small.linespace	CDATA	"12"	>
<!ENTITY	small.spacebef	CDATA	"5"	>
<!ENTITY	std.font	CDATA	"times"	>
<!ENTITY	std.fontsize	CDATA	"14"	>
<!ENTITY	std.leftindent	CDATA	"10"	>
<!ENTITY	std.linespace	CDATA	"16"	>
<!ENTITY	std.rightindent	CDATA	"12"	>
<!ENTITY	std.spacebef	CDATA	"10"	>
<!ENTITY	title.font	CDATA	"helvetica"	>
<!ENTITY	title.fontsize	CDATA	"18"	>
<!ENTITY	title.linespace	CDATA	"20"	>

<sheet >



<?INSTED COMMENT: GROUP frontstuff>

<group name="frontstuff">
	<font-family>	&title.font	</>
	<font-size>	&std.fontsize	</>
	<left-indent>	+=100	</>
	<first-indent>	-100	</>
	<line-spacing>	&std.linespace	</>
	<text-before>tag(): </>
</group>

<style name="AUTHOR" group="frontstuff">
	<hide>	All	</>
	<break-before>	True	</>
</style>

<style name="DATE" group="frontstuff">
	<space-before>	&big1.spacebef	</>
	<hide>	Text	</>
	<break-before>	True	</>
	<text-before>tag(): </>
	<text-after>switch(substr(content(),5,2),01,'January',02,'February',03,'March',04,'April',05,'May',06,'June','07','July',08,'August',09,'September',10,'October',11,'November',12,'December',DEFAULT,'') right(content(),2), left(content(),4)</>
</style>

<style name="HARDWARE" group="frontstuff">
	<break-before>	True	</>
</style>

<style name="OS" group="frontstuff">
	<break-before>	True	</>
</style>

<style name="PRODUCT" group="frontstuff">
	<break-before>	True	</>
</style>

<style name="STATUS" group="frontstuff">
	<hide>	All	</>
	<break-before>	True	</>
</style>

<style name="TOPIC" group="frontstuff">
	<hide>	All	</>
	<break-before>	True	</>
	<text-before>tag() cnum():   </>
</style>

<style name="VERSION" group="frontstuff">
	<text-before>  </>
</style>



<?INSTED COMMENT: GROUP title>

<group name="title">
	<font-family>	&title.font	</>
	<font-weight>	Bold	</>
	<foreground>	blue	</>
	<left-indent>	&std.leftindent	</>
	<break-before>	True	</>
</group>

<style name="BLOCK,TITLE" group="title">
	<font-size>	&std.fontsize	</>
	<line-spacing>	&std.linespace	</>
	<space-before>	&std.spacebef	</>
</style>

<style name="FRONT,TITLE" group="title">
	<font-size>	&bigtitle.fontsize	</>
	<line-spacing>	&bigtitle.linespace	</>
	<space-before>	&big4.spacebef	</>
	<text-before>attr(id,parent(techbull)):   </>
</style>

<style name="LABEL" group="title">
	<font-size>	&std.fontsize	</>
	<line-spacing>	&std.linespace	</>
	<space-before>	&std.spacebef	</>
</style>

<style name="MAP,TITLE" group="title">
	<font-size>	&title.fontsize	</>
	<line-spacing>	&title.linespace	</>
	<space-before>	&big1.spacebef	</>
</style>

<style name="TECHBULL,TITLE" group="title">
	<font-size>	&bigtitle.fontsize	</>
	<line-spacing>	&bigtitle.linespace	</>
	<text-before>attr(id,parent(TECHBULL)):    </>
</style>

<style name="TITLEPG,TITLE" group="title">
	<font-size>	&bigtitle.fontsize	</>
	<line-spacing>	&bigtitle.linespace	</>
	<space-before>	&big4.spacebef	</>
	<hide>	Text	</>
	<text-before>toupper(content())  </>
</style>



<?INSTED COMMENT: UNGROUPED STYLES FOLLOW>

<style name="#SDATA">
	<font-family>	attr(font>	</>
	<character-set>	attr(charset)	</>
	<text-before>char(attr(code))</>
</style>

<style name="#TAGS">
	<foreground>	purple	</>
</style>

<style name="ART">
	<!-- done for Tables TOC -->
	<icon-position>	Right	</>
	<break-before>	True	</>
	<script>	ebt-raster filename="@(FILE).ras" title="@(TITLE)"	</>
	<icon-type>	raster	</>
</style>

<style name="B">
	<font-weight>	Bold	</>
</style>

<style name="CONTMAPT">
	<hide>	All	</>
</style>

<style name="COPYRTPG">
	<icon-position>	Right	</>
	<hide>	Children	</>
	<script>	ebt-footnote title="Copyright Notice"	</>
	<icon-type>	copyrt	</>
</style>

<style name="DOCLINK">
	<font-slant>	Italics	</>
	<foreground>	red	</>
	<icon-position>	Right	</>
	<break-before>	True	</>
	<script>	ebt-link book="@(DOC)" tname=ID tvalue="@(RID)"	</>
	<icon-type>	exlink	</>
</style>

<style name="DOS">
	<break-before>	False	</>
	<select>	if(eq(index(env(PATH),'/'),0),ME.SHOW,ME.HIDE)	</>
</style>

<style name="EMPH">
	<font-family>	&title.font	</>
	<select>	EMPH.@(TYPE)	</>
</style>

<style name="EMPH.BUTTON">
	<font-family>	courier	</>
</style>

<style name="EMPH.KEY">
	<font-family>	courier	</>
</style>

<style name="EMPH.MENU">
	<font-slant>	Italics	</>
</style>

<style name="EMPH.WINDOW">
	<font-family>	&title.font	</>
</style>

<!-- ************************************************************
 Following tag is included for handling special examples, mainly
     in the DynaText Publisher Guide, that show specific formatting
     properties in isolation.  Each instance of the element has its
     own special style sheet, which is named on its SS attribute.
-->
<style name="EXAMPLE">
	<icon-position>	Right	</>
	<hide>	Children	</>
	<script>	ebt-footnote title="Example" stylesheet=@(SS)	</>
	<icon-type>	footnote	</>
</style>

<style name="FOOTNOTE">
	<icon-position>	Right	</>
	<hide>	Children	</>
	<script>	ebt-reveal stylesheet=footnote	</>
	<icon-type>	footnote	</>
</style>

<style name="I">
	<font-slant>	Italics	</>
</style>

<style name="IMIDOC">
	<font-family>	&std.font	</>
	<font-size>	&std.fontsize	</>
	<foreground>	black	</>
	<left-indent>	&big.leftindent	</>
	<right-indent>	&std.rightindent	</>
	<line-spacing>	&std.linespace	</>
</style>

<style name="INREF">
	<font-slant>	Italics	</>
	<foreground>	red	</>
	<script>	ebt-link tname=ID tvalue=attr(RID)	</>
</style>

<style name="ITEM">
	<select>	Item*attr(type,parent(list))	</>
</style>

<style name="ITEM*">
	<left-indent>	+=16	</>
</style>

<style name="ITEM*DEFN">
	<left-indent>	+=100	</>
</style>

<style name="ITEM*MARKED">
	<left-indent>	+=16	</>
</style>

<style name="ITEM*ORDERED">
	<left-indent>	+=16	</>
</style>

<style name="ITEM*PLAIN">
	<left-indent>	+=16	</>
	<break-before>	True	</>
</style>

<style name="ITEM,P">
	<space-before>	if(isfirst(), 0, &small.spacebef)	</>
	<break-before>	if(isfirst(), None, Line)	</>
</style>

<style name="LIST">
	<first-indent>	0	</>
	<space-before>	&small.spacebef	</>
	<break-before>	True	</>
</style>

<style name="LIST,LIST">
	<left-indent>	+=16	</>
	<first-indent>	0	</>
	<space-before>	&small.spacebef	</>
	<break-before>	True	</>
</style>

<style name="MARKER">
	<select>	Marker*attr(type,parent(list))	</>
</style>

<style name="MARKER*">
	<space-before>	&small.spacebef	</>
	<break-before>	True	</>
</style>

<style name="MARKER*DEFN">
	<font-weight>	Bold	</>
	<space-before>	&small.spacebef	</>
	<break-before>	True	</>
</style>

<style name="MARKER*MARKED">
	<space-before>	&small.spacebef	</>
	<break-before>	True	</>
</style>

<style name="MARKER*ORDERED">
	<space-before>	&small.spacebef	</>
	<break-before>	True	</>
</style>

<style name="ME.HIDE">
	<hide>	All	</>
</style>

<style name="ME.SHOW">
	<hide>	Off	</>
</style>

<style name="NOTICE">
	<font-family>	&title.font	</>
	<font-size>	&std.fontsize	</>
	<line-spacing>	&std.linespace	</>
	<space-before>	&big1.spacebef	</>
	<break-before>	True	</>
</style>

<style name="OWNER">
	<font-family>	&title.font	</>
	<font-size>	&title.fontsize	</>
	<line-spacing>	&title.linespace	</>
	<space-before>	&big1.spacebef	</>
	<break-before>	True	</>
</style>

<style name="P">
	<space-before>	&std.spacebef	</>
	<break-before>	Line	</>
</style>

<style name="PART">
	<space-before>	&big5.spacebef	</>
</style>

<style name="RM">
	<font-weight>	Medium	</>
	<font-slant>	Roman	</>
</style>

<style name="SECTION">
	<space-before>	&big3.spacebef	</>
</style>

<style name="SHOW">
	<space-before>	&std.spacebef	</>
	<icon-position>	Left	</>
	<break-before>	True	</>
	<script>	ebt-raster filename="@(TYPE)" title="@(Type) Icon"	</>
	<icon-type>	@(TYPE)	</>
</style>

<style name="SIDEBAR">
	<icon-position>	Right	</>
	<hide>	Children	</>
	<script>	ebt-footnote title=" " stylesheet="sidebar"	</>
	<icon-type>	footnote	</>
</style>

<style name="SIDEBAR,LABEL">
	<hide>	Children	</>
</style>

<style name="SOUND">
	<icon-position>	Right	</>
	<script>	ebt-launch cmd="sound_script var(fig_dir)/@(ID)"	</>
	<icon-type>	sound	</>
</style>

<style name="SP.ICONSHOW">
	<left-indent>	&big.leftindent	</>
	<space-before>	&std.spacebef	</>
	<icon-position>	Inline	</>
	<break-before>	True	</>
	<script>	ebt-reveal stylesheet=fulltext.v title=@(title)	</>
	<icon-type>	@(TITLE)	</>
</style>

<style name="SPECIAL">
	<font-family>	&title.font	</>
	<select>	SP.@(type)	</>
</style>

<style name="SUBBLOCK,LABEL">
	<font-weight>	Bold	</>
	<font-slant>	Italics	</>
	<foreground>	blue	</>
	<space-before>	&std.spacebef	</>
	<break-before>	True	</>
</style>

<style name="SUBBLOCK,P">
	<font-family>	&title.font	</>
	<select>	SUBBLOCK_P_first.eq(1,cnum())	</>
</style>

<style name="SUBBLOCK_P_FIRST.FALSE">
	<space-before>	&std.spacebef	</>
	<break-before>	True	</>
</style>

<style name="SUBBLOCK_P_FIRST.TRUE">
	<break-before>	False	</>
</style>

<style name="SUBTITLE">
	<font-family>	&title.font	</>
	<font-size>	&title.fontsize	</>
	<line-spacing>	&title.linespace	</>
	<space-before>	&big1.spacebef	</>
	<break-before>	True	</>
</style>

<style name="SYMBOL">
	<font-family>	symbol	</>
</style>

<style name="TABLE">
	<!-- done for Tables TOC -->
	<foreground>	black	</>
	<left-indent>	&big.leftindent	</>
	<break-before>	True	</>
	<select>	TABLE.gamut(@(type,parent()),'proc2 other2','inline inline','HIDDEN')	</>
</style>

<style name="TABLE.HIDDEN">
	<icon-position>	Right	</>
	<hide>	Children	</>
	<script>	ebt-reveal stylesheet=tolower(@(type,parent())).rev </>
	<icon-type>	table	</>
</style>

<style name="TABLECELL">
	<select>	TABLECELL.@(type,parent(tblock)).cnum(),TABLECELL	</>
	<column>	True	</>
</style>

<style name="TABLECELL,P">
</style>

<style name="TABLECELL.OTHER2.1">
	<width>	150	</>
	<column>	True	</>
</style>

<style name="TABLECELL.OTHER2.2">
	<left-indent>	+=155	</>
	<right-indent>	&std.rightindent	</>
	<column>	True	</>
</style>

<!-- Following styles are definitions for each column of the table.
     They can be generated by hand, or more conveniently using the
     genTable utility program. -->
<style name="TABLECELL.PROC2.1">
	<width>	65	</>
	<justification>	Center	</>
	<column>	True	</>
</style>

<style name="TABLECELL.PROC2.2">
	<left-indent>	+=70	</>
	<right-indent>	&std.rightindent	</>
	<column>	True	</>
</style>

<style name="TABLEROW">
	<space-before>	&std.spacebef	</>
	<break-before>	True	</>
	<select>	TABLEROW.@(hdr),TABLEROW	</>
</style>

<!-- header of a table -->
<style name="TABLEROW.1">
	<font-weight>	Bold	</>
	<font-slant>	Italics	</>
</style>

<!-- Destination of hypertext link -->
<style name="TARGET">
	<font-slant>	Italics	</>
	<foreground>	red	</>
	<icon-position>	Right	</>
	<break-before>	True	</>
	<script>	ebt-link tname=RID tvalue=@(ID)	</>
	<icon-type>	inlink	</>
</style>

<style name="TECHBULL">
	<left-indent>	&std.leftindent	</>
	<space-before>	&big2.spacebef	</>
	<break-before>	True	</>
</style>

<style name="TITLE">
	<font-size>	&bigtitle.fontsize	</>
	<line-spacing>	&bigtitle.linespace	</>
	<space-before>	&big4.spacebef	</>
</style>

<style name="TOCPG">
	<hide>	Children	</>
</style>

<style name="UL">
	<score>	Under	</>
</style>

<style name="UNIX">
	<select>	if(gt(index(env(PATH),'/'),0),ME.SHOW,ME.HIDE)	</>
</style>

<style name="VBLOCK">
	<font-family>	courier	</>
	<font-size>	&small.fontsize	</>
	<left-indent>	+=16	</>
	<line-spacing>	&small.linespace	</>
	<space-before>	&small.spacebef	</>
	<break-before>	Line	</>
	<justification>	verbatim	</>
</style>

<style name="VECTOR">
	<icon-position>	Right	</>
	<script>	ebt-launch cmd="cgm_script var(fig_dir)/@(ID)"	</>
	<icon-type>	vector	</>
</style>

<style name="VERBATIM">
	<break-before>	Line	</>
</style>

<style name="XREF">
	<font-family>	&hottext.fontfam	</>
	<font-slant>	&hottext.slant	</>
	<foreground>	&hottext.foreground	</>
	<!--script> 	ebt-link rname=ID rvalue=@(RID) window=NEW view=popup -->
	<script>	ebt-link root=idmatch(ID, ATTR(RID))    window=new	</>
</style>



</sheet>

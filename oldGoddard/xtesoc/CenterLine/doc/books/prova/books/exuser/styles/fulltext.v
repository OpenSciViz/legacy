<!---->


<sheet >



<?INSTED COMMENT: UNGROUPED STYLES FOLLOW>

<style name="#ANNOT">
	<right-indent>	+=10	</>
	<break-before>	None	</>
	<break-after>	None	</>
</style>

<style name="#DEFAULT">
	<font-family>	helvetica	</>
	<font-size>	12	</>
	<break-before>	Line	</>
	<icon-type>	raster	</>
</style>

<style name="#ROOT">
	<font-family>	lucida	</>
	<font-size>	12	</>
	<line-spacing>	14	</>
	<space-after>	6	</>
	<break-before>	Line	</>
</style>

<style name="#SDATA">
	<font-family>	attr(font)	</>
	<font-weight>	Medium	</>
	<font-slant>	Roman	</>
	<character-set>	attr(charset)	</>
	<break-before>	None	</>
	<text-before>char(attr(code))</>
</style>

<style name="#TAGS">
	<font-weight>	Bold	</>
</style>

<style name="ANCHOR">
	<break-before>	None	</>
</style>

<style name="APPENDIX,PARA">
	<font-family>	lucida	</>
	<font-weight>	Medium	</>
	<font-size>	12	</>
	<left-indent>	+10	</>
	<line-spacing>	12	</>
	<first-indent>	0	</>
	<space-before>	3	</>
	<space-after>	3	</>
	<hrule>	None	</>
</style>

<style name="APPENDIX,TITLE">
	<font-family>	lucida	</>
	<font-weight>	Bold	</>
	<font-size>	18	</>
	<left-indent>	-=10	</>
	<line-spacing>	21	</>
	<space-before>	24	</>
	<space-after>	24	</>
	<text-before>Appendix format (cnum(parent(APPENDIX)), LETTER):   </>
</style>

<style name="BKTITLE">
	<font-slant>	Italics	</>
	<break-before>	None	</>
	<break-after>	None	</>
</style>

<style name="BOOK">
	<font-slant>	Roman	</>
</style>

<style name="BOOK,TITLE">
	<font-weight>	Bold	</>
	<font-size>	18	</>
	<left-indent>	-=10	</>
	<line-spacing>	20	</>
	<justification>	Left	</>
	<space-before>	6	</>
</style>

<style name="BOOKTITLE">
	<font-slant>	Italics	</>
	<break-before>	None	</>
	<break-after>	None	</>
</style>

<style name="BULLIST">
	<font-size>	12	</>
	<left-indent>	+=20	</>
	<break-before>	Line	</>
</style>

<style name="BULLIST,LISTITEM">
	<font-family>	lucida	</>
	<line-spacing>	14	</>
	<first-indent>	-20	</>
	<space-before>	3	</>
	<space-after>	3	</>
	<break-before>	None	</>
	<text-before>�</>
</style>

<style name="BULLIST,LISTITEM,#TEXT-BEFORE">
	<font-family>	symbol	</>
	<font-size>	18	</>
	<foreground>	DarkGreen	</>
</style>

<style name="CAUTION">
	<space-before>	16	</>
	<break-before>	Line	</>
	<break-after>	None	</>
</style>

<style name="CAUTION,BULLIST,LISTITEM">
	<font-size>	12	</>
	<left-indent>	+=35	</>
	<first-indent>	-=15	</>
	<space-before>	4	</>
	<space-after>	if(islast(), 12,	</>
	<text-before>�</>
</style>

<style name="CAUTION,PARA">
	<left-indent>	+=60	</>
	<line-spacing>	14	</>
	<first-indent>	if(isfirst(), -=40,  )	</>
	<space-after>	if(islast(), 12, 6)	</>
	<hrule>	None	</>
	<break-before>	None	</>
	<break-after>	Line	</>
	<text-before>if(isfirst(), CAUTION,  )</>
</style>

<style name="CAUTION,PARA,#TEXT-BEFORE">
	<font-weight>	Bold	</>
	<foreground>	DarkGreen	</>
	<justification>	Left	</>
</style>

<style name="CHAPTER,TITLE">
	<font-weight>	Bold	</>
	<font-size>	18	</>
	<left-indent>	-=10	</>
	<line-spacing>	21	</>
	<justification>	Left	</>
	<hide>	Off	</>
	<space-before>	20	</>
	<space-after>	8	</>
	<text-before>Chapter gcnum(ancestor(chapter)):  </>
</style>

<style name="CLBOOK">
	<font-family>	helvetica	</>
	<left-indent>	+=10	</>
	<break-before>	Line	</>
</style>

<style name="CLBOOK,PART">
</style>

<style name="CODE">
	<break-before>	None	</>
	<break-after>	None	</>
</style>

<style name="COL1">
</style>

<style name="COL2">
	<left-indent>	+=150	</>
	<break-before>	None	</>
</style>

<style name="COLLIST">
	<break-before>	Line	</>
</style>

<style name="COLLIST,COL1">
	<space-before>	6	</>
	<break-before>	Line	</>
	<break-after>	None	</>
</style>

<style name="COLLIST,LISTITEM">
	<left-indent>	+=10	</>
	<column>	False	</>
	<justification>	Verbatim	</>
	<first-indent>	-=50	</>
	<space-before>	6	</>
</style>

<style name="COLLIST,LISTITEM,NAME">
	<font-weight>	Bold	</>
	<left-indent>	0	</>
	<first-indent>	-=30	</>
	<break-before>	None	</>
	<text-after>     </>
</style>

<style name="COPYRT">
	<left-indent>	-=20	</>
	<right-indent>	+=20	</>
	<hide>	Children	</>
	<first-indent>	-=20	</>
	<icon-position>	Right	</>
	<script>	ebt-reveal stylesheet="fulltext" title="Copyright Information"	</>
	<icon-type>	copyrt	</>
</style>

<style name="COPYRT,TITLE">
	<font-weight>	Bold	</>
	<font-size>	14	</>
	<justification>	Left	</>
	<hide>	Text	</>
	<space-before>	6	</>
	<text-before>Copyright Notice</>
</style>

<style name="DEF">
	<space-before>	6	</>
</style>

<style name="DESC,TITLE">
	<font-weight>	Bold	</>
	<font-size>	14	</>
	<left-indent>	-=10	</>
	<justification>	Left	</>
	<space-before>	6	</>
	<text-before>Description</>
</style>

<style name="DESC,TITLE,#TEXT-BEFORE">
	<font-family>	lucida	</>
	<font-weight>	Bold	</>
	<font-size>	14	</>
	<left-indent>	-=10	</>
	<justification>	Left	</>
	<space-before>	18	</>
	<space-after>	6	</>
</style>

<style name="DESC2,TITLE">
	<font-weight>	Bold	</>
	<font-size>	14	</>
	<first-indent>	-=10	</>
	<justification>	Left	</>
	<space-before>	6	</>
	<text-before>Description</>
</style>

<style name="DESC2,TITLE,#TEXT-BEFORE">
	<font-family>	lucida	</>
	<font-weight>	Bold	</>
	<font-size>	14	</>
	<left-indent>	-=10	</>
	<justification>	Left	</>
	<space-before>	18	</>
	<space-after>	6	</>
</style>

<style name="DTAG-ERR">
	<hide>	Text	</>
</style>

<style name="ECOMMENT">
	<font-family>	times	</>
	<font-slant>	Italics	</>
	<font-size>	12	</>
	<score>	Under	</>
	<text-before>WRITER NOTE:  </>
</style>

<style name="EFFECT,XREF">
	<score>	Under	</>
	<break-before>	None	</>
	<break-after>	None	</>
	<script>	ebt-link target=idmatch(ID,attr(REFID)) book="effect" window="specified" view="fulltext.v"  showtoc="true"	</>
</style>

<style name="EMPH">
</style>

<style name="EMPHASIS">
	<font-weight>	Medium	</>
	<font-slant>	Italics	</>
	<break-before>	None	</>
</style>

<style name="ENTRY">
	<left-indent>	+=tableinfo(cals,left-indent,10)	</>
	<width>	tableinfo(cals,width,10)	</>
	<column>	True	</>
	<justification>	tableinfo(cals, justification)	</>
</style>

<style name="ENTRY,BULLIST,LISTITEM">
	<font-family>	lucida	</>
	<line-spacing>	11	</>
	<first-indent>	-20	</>
	<space-before>	0	</>
	<break-before>	Line	</>
	<break-after>	Line	</>
	<text-before>�</>
</style>

<style name="ENTRY,EXAMPLE">
	<font-family>	courier	</>
	<left-indent>	+=10	</>
	<line-spacing>	12	</>
	<justification>	Verbatim	</>
	<space-before>	3	</>
	<break-before>	Line	</>
	<break-after>	None	</>
</style>

<style name="ENTRY,PARA">
	<line-spacing>	13	</>
	<space-before>	2	</>
	<break-before>	None	</>
</style>

<style name="EXAMPLE">
	<font-family>	courier	</>
	<left-indent>	+=10	</>
	<line-spacing>	12	</>
	<justification>	Left	</>
	<space-before>	3	</>
	<break-before>	Line	</>
	<break-after>	None	</>
</style>

<style name="EXAMPLE,VARIABLE">
	<font-family>	courier	</>
	<font-weight>	Medium	</>
	<font-slant>	Italics	</>
	<break-before>	None	</>
	<break-after>	None	</>
</style>

<style name="EXMPITEM">
</style>

<style name="EXREF">
	<score>	Under	</>
	<break-before>	None	</>
	<break-after>	None	</>
	<script>	ebt-link book=attr(book) window="specified" showtoc="true" target=idmatch(ID,attr(REFID)) stylesheet=fulltext.v	</>
</style>

<style name="EXSECT2,TITLE">
	<font-family>	lucida	</>
	<font-weight>	Bold	</>
	<font-size>	14	</>
	<left-indent>	-=10	</>
	<justification>	Left	</>
	<space-before>	18	</>
	<space-after>	6	</>
	<text-before>Example</>
</style>

<style name="FIGURE">
	<font-weight>	bold	</>
	<justification>	Left	</>
</style>

<style name="FIGURE,GRAPHIC">
	<space-before>	8	</>
	<space-after>	12	</>
	<icon-position>	Inline	</>
	<script>	ebt-raster filename=attr(FILENAME) title="content(rsibling(TITLE))"	</>
	<icon-type>	raster	</>
	<text-before>Figure gcnum(ancestor(FIGURE)): content(rsibling(title))</>
</style>

<style name="FIGURE,NOTE,PARA">
	<font-weight>	Medium	</>
	<left-indent>	+=40	</>
	<line-spacing>	14	</>
	<first-indent>	if(isfirst(), -=40,  )	</>
	<space-after>	if(islast(), 12, 6)	</>
	<hrule>	None	</>
	<break-before>	None	</>
	<break-after>	Line	</>
	<text-before>if(isfirst(), NOTE,  )</>
</style>

<style name="FIGURE,PARA">
	<font-family>	lucida	</>
	<font-weight>	Medium	</>
	<font-size>	12	</>
	<space-before>	6	</>
	<space-after>	6	</>
	<hrule>	None	</>
</style>

<style name="FIGURE,PARA,GRAPHIC">
	<space-before>	8	</>
	<space-after>	12	</>
	<icon-position>	Inline	</>
	<script>	ebt-raster filename=attr(FILENAME) title="content(ancestor(FIGURE))"	</>
	<icon-type>	raster	</>
	<text-before>Figure gcnum(ancestor(FIGURE)): content(ancestor(FIGURE))</>
</style>

<style name="FTABLE">
	<hide>	Children	</>
	<space-after>	8	</>
	<icon-position>	Inline	</>
	<script>	ebt-reveal stylesheet="fulltext" title="Table gcnum()"	</>
	<icon-type>	table	</>
	<text-before>Table gcnum(): content(child(title))</>
</style>

<style name="FTABLE,#TEXT-BEFORE">
	<font-family>	lucida	</>
	<font-weight>	Bold	</>
	<font-size>	12	</>
	<left-indent>	-=10	</>
	<justification>	Left	</>
	<space-before>	6	</>
	<space-after>	6	</>
</style>

<style name="FTABLE,TITLE">
	<font-weight>	Bold	</>
	<font-size>	14	</>
	<justification>	Left	</>
	<hide>	Children	</>
	<text-before>  </>
</style>

<style name="GLOSSARY,TITLE">
	<font-weight>	Bold	</>
	<font-size>	18	</>
	<left-indent>	-=10	</>
	<line-spacing>	20	</>
	<justification>	Left	</>
	<hide>	Off	</>
	<space-before>	20	</>
	<space-after>	8	</>
	<icon-type>	None	</>
</style>

<style name="GRAPHIC">
	<space-before>	8	</>
	<space-after>	12	</>
	<inline>	raster filename=attr(FILENAME)	</>
</style>

<style name="GRAPHIC,#TEXT-BEFORE">
	<font-family>	lucida	</>
	<font-weight>	Bold	</>
	<font-size>	12	</>
	<left-indent>	-=10	</>
	<justification>	Left	</>
	<space-before>	6	</>
	<space-after>	6	</>
</style>

<style name="HARDCOPY">
	<hide>	All	</>
	<break-before>	None	</>
</style>

<style name="HOTTEXT">
	<score>	Under	</>
	<break-before>	None	</>
	<break-after>	None	</>
</style>

<style name="HRULE">
</style>

<style name="ICONEX">
	<left-indent>	5	</>
	<hide>	Children	</>
	<space-before>	8	</>
	<space-after>	8	</>
	<icon-position>	Inline	</>
	<break-before>	Line	</>
	<script>	ebt-reveal stylesheet="fulltext" title="Code Example"	</>
	<icon-type>	table	</>
</style>

<style name="ICONEX,LNGEXAMP">
	<left-indent>	+=10	</>
	<width>	500	</>
	<column>	False	</>
	<space-before>	6	</>
	<break-before>	None	</>
</style>

<style name="ICONEX,LNGEXAMP,EXMPITEM">
	<font-family>	lucidatypewriter	</>
	<line-spacing>	14	</>
	<justification>	Verbatim	</>
	<break-before>	Line	</>
	<break-after>	None	</>
</style>

<style name="INPUT">
	<font-family>	courier	</>
	<font-weight>	Bold	</>
	<break-before>	None	</>
	<break-after>	None	</>
</style>

<style name="LISTITEM">
	<left-indent>	+=10	</>
	<first-indent>	-10	</>
	<space-before>	6	</>
	<space-after>	6	</>
</style>

<style name="LNGEXAMP">
	<left-indent>	+=10	</>
	<space-before>	6	</>
	<break-before>	Line	</>
</style>

<style name="LNGEXAMP,EXMPITEM">
	<font-family>	lucidatypewriter	</>
	<line-spacing>	14	</>
	<justification>	Verbatim	</>
</style>

<style name="MEFFECT,XREF">
	<score>	Under	</>
	<break-before>	None	</>
	<break-after>	None	</>
	<script>	ebt-link target=idmatch(ID,attr(REFID))  book="meffect" window="specified" view="fulltext.v"  showtoc="true"	</>
</style>

<style name="MISCPART,TITLE">
	<font-weight>	Bold	</>
	<font-size>	18	</>
	<left-indent>	-=10	</>
	<line-spacing>	20	</>
	<justification>	Left	</>
	<hide>	Off	</>
	<space-before>	24	</>
	<space-after>	24	</>
</style>

<style name="NAME">
	<font-weight>	Bold	</>
	<break-before>	None	</>
</style>

<style name="NOTE">
	<space-before>	16	</>
	<break-before>	Line	</>
	<break-after>	None	</>
</style>

<style name="NOTE,BULLIST,LISTITEM">
	<font-size>	12	</>
	<left-indent>	+=35	</>
	<first-indent>	-=15	</>
	<space-before>	4	</>
	<space-after>	if(islast(), 12,	</>
	<text-before>�</>
</style>

<style name="NOTE,PARA">
	<font-weight>	Medium	</>
	<left-indent>	40	</>
	<line-spacing>	14	</>
	<first-indent>	-=10	</>
	<space-after>	if(islast(), 12, 6)	</>
	<vrule>	None	</>
	<break-before>	None	</>
	<break-after>	Line	</>
	<text-before>if(isfirst(), NOTE,  )  </>
</style>

<style name="NOTE,PARA,#TEXT-BEFORE">
	<font-weight>	Bold	</>
	<foreground>	DarkGreen	</>
	<justification>	Left	</>
</style>

<style name="NUMLIST,LISTITEM">
	<left-indent>	20	</>
	<first-indent>	-20	</>
	<space-before>	6	</>
	<text-before>cnum()   </>
</style>

<style name="NUMLIST,LISTITEM,#TEXT-BEFORE">
	<font-weight>	Bold	</>
	<foreground>	DarkGreen	</>
</style>

<style name="OPTION2,TITLE">
	<font-weight>	Bold	</>
	<font-size>	14	</>
	<left-indent>	-=10	</>
	<justification>	Left	</>
	<space-before>	18	</>
	<space-after>	6	</>
	<text-before>Options</>
</style>

<style name="OPTIONS,TITLE">
	<font-weight>	Bold	</>
	<font-size>	14	</>
	<left-indent>	-=10	</>
	<justification>	Left	</>
	<space-before>	18	</>
	<space-after>	6	</>
	<text-before>Options</>
</style>

<style name="ORDLIST">
	<left-indent>	+=20	</>
</style>

<style name="ORDLIST,LISTITEM">
	<left-indent>	+=5	</>
	<first-indent>	-=25	</>
	<space-before>	6	</>
	<text-before>cnum()   </>
</style>

<style name="ORDLIST,LISTITEM,#TEXT-BEFORE">
	<font-family>	helvetica	</>
	<font-weight>	Bold	</>
	<foreground>	DarkGreen	</>
</style>

<style name="OUTPUT">
	<font-family>	courier	</>
	<break-before>	None	</>
</style>

<style name="OVERVIEW,TITLE">
	<font-family>	helvetica	</>
	<font-weight>	Bold	</>
	<font-size>	14	</>
	<left-indent>	-=10	</>
	<justification>	Left	</>
	<hide>	Off	</>
	<space-before>	18	</>
	<space-after>	6	</>
	<text-before>Overview of steps</>
</style>

<style name="PARA">
	<font-family>	lucida	</>
	<font-weight>	Medium	</>
	<font-size>	12	</>
	<space-before>	4	</>
	<space-after>	4	</>
	<hrule>	None	</>
</style>

<style name="PART,CHAPTER,TITLE">
	<font-weight>	Bold	</>
	<font-size>	18	</>
	<left-indent>	-=10	</>
	<justification>	Left	</>
	<hide>	Off	</>
	<space-before>	20	</>
	<space-after>	8	</>
	<text-before>Chapter gcnum(ancestor(chapter)):  </>
</style>

<style name="PART,TITLE">
	<font-family>	lucida	</>
	<font-weight>	Bold	</>
	<font-size>	18	</>
	<left-indent>	-=10	</>
	<space-before>	24	</>
	<space-after>	24	</>
	<text-before>Part format (cnum(parent(PART)), ROMAN):   </>
</style>

<style name="PARTINTR">
	<left-indent>	-=10	</>
</style>

<style name="PREFACE,TITLE">
	<font-weight>	Bold	</>
	<font-size>	18	</>
	<left-indent>	-=10	</>
	<justification>	Left	</>
	<hide>	Off	</>
	<space-before>	24	</>
	<space-after>	8	</>
	<text-before>Using this book</>
</style>

<style name="PROCED">
	<left-indent>	+=20	</>
	<break-before>	Line	</>
</style>

<style name="PROCED,BULLIST">
	<font-size>	12	</>
	<left-indent>	+=20	</>
	<break-before>	Line	</>
</style>

<style name="PROCED,STEP">
	<left-indent>	20	</>
	<first-indent>	-20	</>
	<space-before>	5	</>
	<text-before>cnum()</>
</style>

<style name="PROCED,STEP,#TEXT-BEFORE">
	<font-family>	helvetica	</>
	<font-weight>	Bold	</>
	<foreground>	DarkGreen	</>
</style>

<style name="REFSECT1,SECTION2,SYNOPSIS">
	<font-weight>	Demibold	</>
	<left-indent>	0	</>
	<space-before>	4	</>
</style>

<style name="REFSECT1,SECTION2,SYNOPSIS,#TEXT-BEFORE">
	<font-weight>	Medium	</>
</style>

<style name="REFSECT1,TITLE">
	<font-weight>	Bold	</>
	<font-size>	18	</>
	<left-indent>	-=10	</>
	<justification>	Left	</>
	<space-before>	24	</>
	<space-after>	6	</>
	<hrule>	After	</>
</style>

<style name="RESTR2,TITLE">
	<font-weight>	Bold	</>
	<font-size>	14	</>
	<left-indent>	-=10	</>
	<justification>	Left	</>
	<space-before>	18	</>
	<space-after>	6	</>
	<text-before>Restrictions</>
</style>

<style name="ROW">
	<space-before>	9	</>
	<break-before>	Line	</>
</style>

<style name="SECTION1">
	<hrule>	None	</>
</style>

<style name="SECTION1,TITLE">
	<font-weight>	Bold	</>
	<font-size>	18	</>
	<left-indent>	-=10	</>
	<line-spacing>	21	</>
	<justification>	Left	</>
	<space-before>	20	</>
	<space-after>	6	</>
	<hrule>	After	</>
</style>

<style name="SECTION1,TITLE,HRULE">
	<font-weight>	Bold	</>
	<foreground>	DarkGreen	</>
</style>

<style name="SECTION2,TITLE">
	<font-weight>	Bold	</>
	<font-size>	14	</>
	<left-indent>	-=10	</>
	<justification>	Left	</>
	<space-before>	18	</>
	<space-after>	6	</>
</style>

<style name="SECTION3,TITLE">
	<font-weight>	Demibold	</>
	<font-size>	12	</>
	<justification>	Left	</>
	<space-before>	14	</>
	<hrule>	None	</>
</style>

<style name="SECTION4,TITLE">
	<font-weight>	Demibold	</>
	<font-size>	12	</>
	<justification>	Left	</>
	<space-before>	14	</>
	<hrule>	None	</>
</style>

<style name="SEEALSO2,TITLE">
	<font-weight>	Bold	</>
	<font-size>	14	</>
	<left-indent>	-=10	</>
	<justification>	Left	</>
	<space-before>	18	</>
	<space-after>	6	</>
	<text-before>See also</>
</style>

<style name="SPACE">
	<font-size>	14	</>
	<break-before>	Line	</>
	<break-after>	Line	</>
</style>

<style name="STEP">
</style>

<style name="SUBSTEP">
	<left-indent>	+=20	</>
	<first-indent>	-=20	</>
	<space-before>	2	</>
	<space-after>	2	</>
	<text-before>format(cnum(),letter)</>
</style>

<style name="SUBSTEP,#TEXT-BEFORE">
	<font-family>	helvetica	</>
	<font-weight>	Bold	</>
</style>

<style name="SUP">
	<font-size>	10	</>
	<vertical-offset>	4	</>
	<break-before>	None	</>
	<break-after>	None	</>
</style>

<style name="SWITCH2,TITLE">
	<font-weight>	Bold	</>
	<font-size>	14	</>
	<left-indent>	-=10	</>
	<justification>	Left	</>
	<space-before>	6	</>
	<text-before>Switches</>
</style>

<style name="SYMBOL">
	<break-before>	None	</>
	<break-after>	None	</>
</style>

<style name="SYNOPSIS">
	<font-weight>	Demibold	</>
	<left-indent>	+=10	</>
	<space-before>	4	</>
</style>

<style name="SYNTAX,TITLE">
	<font-weight>	Bold	</>
	<font-size>	14	</>
	<left-indent>	-=10	</>
	<justification>	Left	</>
	<space-before>	6	</>
	<text-before>Syntax</>
</style>

<style name="SYNTAX2,TITLE">
	<font-weight>	Bold	</>
	<font-size>	14	</>
	<left-indent>	-=10	</>
	<justification>	Left	</>
	<space-before>	6	</>
	<text-before>Syntax</>
</style>

<style name="TABLE">
	<hide>	Off	</>
	<space-after>	12	</>
</style>

<style name="TBODY,ROW,ENTRY,ORDLIST,LISTITEM">
	<space-before>	6	</>
	<text-before>cnum() </>
</style>

<style name="TBODY,ROW,ENTRY,PARA,ORDLIST,LISTITEM">
	<space-before>	6	</>
	<text-before>cnum() </>
</style>

<style name="TERM">
	<font-weight>	Bold	</>
	<left-indent>	-=10	</>
	<space-before>	10	</>
</style>

<style name="THEAD">
	<font-family>	lucida	</>
	<font-weight>	Bold	</>
	<space-before>	10	</>
	<space-after>	10	</>
</style>

<style name="THEAD,ROW,ENTRY,PARA">
	<font-family>	lucida	</>
	<font-weight>	Bold	</>
	<font-size>	12	</>
	<space-before>	6	</>
	<space-after>	6	</>
	<hrule>	None	</>
</style>

<style name="TIP">
	<font-weight>	Medium	</>
	<justification>	Left	</>
	<hide>	Children	</>
	<space-before>	8	</>
	<space-after>	8	</>
	<icon-position>	Inline	</>
	<hrule>	None	</>
	<break-before>	Line	</>
	<script>	ebt-reveal stylesheet="fulltext" width="400"	</>
	<icon-type>	default	</>
	<text-before>TIP:  content(child(TITLE))</>
</style>

<style name="TIP,#TEXT-BEFORE">
	<font-family>	lucida	</>
	<font-weight>	Bold	</>
	<font-size>	12	</>
	<left-indent>	-=10	</>
	<justification>	Left	</>
	<space-before>	6	</>
	<space-after>	6	</>
</style>

<style name="TIP,TITLE">
	<font-weight>	Bold	</>
	<font-size>	14	</>
	<justification>	Left	</>
	<hide>	Children	</>
	<space-before>	6	</>
</style>

<style name="TIP,TITLE,#TEXT-BEFORE">
	<font-family>	helvetica	</>
	<font-weight>	Bold	</>
	<font-size>	12	</>
	<left-indent>	-=10	</>
	<justification>	Left	</>
	<space-before>	6	</>
	<space-after>	6	</>
</style>

<style name="TITLE">
	<font-weight>	Bold	</>
	<font-size>	14	</>
	<left-indent>	-=10	</>
	<justification>	Left	</>
	<hide>	Off 		</>
	<space-before>	6	</>
</style>

<style name="TSKSECT1,TITLE">
	<font-family>	helvetica	</>
	<font-weight>	Bold	</>
	<font-size>	18	</>
	<left-indent>	-=10	</>
	<line-spacing>	14	</>
	<justification>	Left	</>
	<space-before>	20	</>
	<space-after>	6	</>
	<hrule>	After	</>
	<text-before>Task:  </>
</style>

<style name="USAGE2,TITLE">
	<font-weight>	Bold	</>
	<font-size>	14	</>
	<left-indent>	-=10	</>
	<justification>	Left	</>
	<space-before>	18	</>
	<space-after>	6	</>
	<text-before>Usage</>
</style>

<style name="VARIABLE">
	<font-weight>	Medium	</>
	<font-slant>	Italics	</>
	<break-before>	None	</>
	<break-after>	None	</>
</style>

<style name="VEFIGURE">
	<justification>	Right	</>
	<hide>	Off	</>
	<icon-position>	Off	</>
	<text-before>Figure gcnum(ancestor(chapter))-gcnum()</>
</style>

<style name="VEFIGURE,TITLE">
	<font-weight>	Bold	</>
	<font-size>	14	</>
	<justification>	Left	</>
	<hide>	Text	</>
	<space-before>	6	</>
</style>

<style name="VEFTABLE">
	<hide>	Children	</>
	<icon-position>	Inline	</>
	<script>	ebt-reveal stylesheet="fulltext"	</>
	<icon-type>	table	</>
	<text-before>Table gcnum(ancestor(chapter))-gcnum()</>
</style>

<style name="VEFTABLE,TITLE">
	<font-weight>	Bold	</>
	<font-size>	14	</>
	<justification>	Left	</>
	<hide>	Off	</>
	<space-before>	6	</>
</style>

<style name="XREF">
	<score>	Under	</>
	<break-before>	None	</>
	<break-after>	None	</>
	<script>	ebt-link target=idmatch(ID,attr(REFID))	</>
</style>

<style name="XREF,EMPH">
	<font-family>	helvetica	</>
	<font-weight>	Bold	</>
	<font-size>	12	</>
	<break-before>	None	</>
	<select>	EMPH_attr(TYPE)	</>
</style>



</sheet>

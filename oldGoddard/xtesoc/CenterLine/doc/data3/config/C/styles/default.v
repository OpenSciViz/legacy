<!-- Template for new main (*.v) Stylesheets

  Copyright 1996, Electronic Book Technologies.  All rights reserved.
-->

<sheet>

<style name ="#DEFAULT">
	<break-before>  Line		</>
</style>

<style name ="#ROOT">
	<break-before>  Line		</>
</style>

<style name="#SDATA">
        <font-family>   attr(font)      </>
        <font-weight>   Medium  </>
        <font-slant>    Roman   </>
        <character-set> attr(charset)   </>
        <break-before>  None    </>
        <text-before>char(attr(code))</>
</style>

<style name ="#TAGS">
        <font-family>   Courier         </>
	<font-weight>   Bold		</>
</style>


</sheet>

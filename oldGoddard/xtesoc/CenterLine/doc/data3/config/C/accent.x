#name accents

# This table specifies a default stripping for taking accents
# of characters in a standard font and taking off the vowels...
# All of the values, for simplicity, will be specified in 
# decimal.
224 'a'
225 'a'
226 'a'
227 'a'
228 'a'
229 'a'

231 'c'

232 'e'
233 'e'
234 'e'
235 'e'

236 'i'
237 'i'
238 'i'
239 'i'

241 'n'

242 'o'
243 'o'
244 'o'
245 'o'
246 'o'

249 'u'
250 'u'
251 'u'
252 'u'

253 'y'
255 'y'




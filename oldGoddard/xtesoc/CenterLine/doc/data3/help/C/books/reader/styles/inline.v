 <!--Start Entity Definitions
    This is the fthead.unx file for the imi dtd.
    It contains entities for the fulltext.v and popup.rev 
    stylesheets on unix.  This file
    should be concatenated with ftbody.all to create
    the fulltext.v file on the unix platform
-->             

<!ENTITY	address.font	CDATA	"helvetica"	>
<!ENTITY	art.color	CDATA	"dark green"	>
<!ENTITY	booktitle.color	CDATA	"medium sea green"	>
<!ENTITY	booktitle.font	CDATA	"lucida"	>
<!ENTITY	booktitle.font-size	CDATA	"24"	>
<!ENTITY	booktitle.font-slant	CDATA	"roman"	>
<!ENTITY	booktitle.font-weight	CDATA	"bold"	>
<!ENTITY	booktitle.line-spacing	CDATA	"28"	>
<!ENTITY	booktitle.space-after	CDATA	"4"	>
<!ENTITY	booktitle.space-before	CDATA	"12"	>
<!ENTITY	frontmatter.font	CDATA	"lucida"	>
<!ENTITY	genlist.first-indent	CDATA	"-=10"	>
<!ENTITY	genlist.left-indent	CDATA	"+=50"	>
<!ENTITY	genlist2.first-indent	CDATA	"-=10"	>
<!ENTITY	genlist2.left-indent	CDATA	"+=80"	>
<!ENTITY	heading1.color	CDATA	"medium sea green"	>
<!ENTITY	heading1.font	CDATA	"lucida"	>
<!ENTITY	heading1.font-size	CDATA	"21"	>
<!ENTITY	heading1.font-weight	CDATA	"BOLD"	>
<!ENTITY	heading1.line-spacing	CDATA	"22"	>
<!ENTITY	heading1.space-after	CDATA	"4"	>
<!ENTITY	heading1.space-before	CDATA	"12"	>
<!ENTITY	heading2.color	CDATA	"blue"		>
<!ENTITY	heading2.font	CDATA	"lucida"	>
<!ENTITY	heading2.font-size	CDATA	"18"	>
<!ENTITY	heading2.font-weight	CDATA	"bold"	>
<!ENTITY	heading2.line-spacing	CDATA	"20"	>
<!ENTITY	heading2.space-after	CDATA	"4"	>
<!ENTITY	heading2.space-before	CDATA	"10"	>
<!ENTITY	heading3.color	CDATA	"purple"	>
<!ENTITY	heading3.font	CDATA	"lucida"	>
<!ENTITY	heading3.font-size	CDATA	"16"	>
<!ENTITY	heading3.font-slant	CDATA	"italics"	>
<!ENTITY	heading3.font-weight	CDATA	"BOLD"	>
<!ENTITY	heading3.line-spacing	CDATA	"16"	>
<!ENTITY	heading3.space-after	CDATA	"4"	>
<!ENTITY	heading3.space-before	CDATA	"6"	>
<!ENTITY	heading4.color	CDATA	"black"	>
<!ENTITY	heading4.font	CDATA	"lucida"	>
<!ENTITY	heading4.font-size	CDATA	"14"	>
<!ENTITY	heading4.font-weight	CDATA	"bold"	>
<!ENTITY	heading4.line-spacing	CDATA	"16"	>
<!ENTITY	heading4.space-before	CDATA	"6"	>
<!ENTITY	heading5.color	CDATA	"blue"	>
<!ENTITY	heading5.font	CDATA	"lucida"	>
<!ENTITY	heading5.font-size	CDATA	"14"	>
<!ENTITY	heading5.font-weight	CDATA	"bold"	>
<!ENTITY	heading5.left-indent	CDATA	"5"	>
<!ENTITY	heading5.line-spacing	CDATA	"16"	>
<!ENTITY	heading5.space-before	CDATA	"6"	>
<!ENTITY	heading6.color	CDATA	"black"	>
<!ENTITY	heading6.font	CDATA	"lucida"	>
<!ENTITY	heading6.font-size	CDATA	"12"	>
<!ENTITY	heading6.font-weight	CDATA	"bold"	>
<!ENTITY	heading6.left-indent	CDATA	"0"	>
<!ENTITY	heading6.line-spacing	CDATA	"14"	>
<!ENTITY	heading6.space-after	CDATA	"2"	>
<!ENTITY	heading6.space-before	CDATA	"4"	>
<!ENTITY	heading6.line-after	CDATA	"line"	>
<!ENTITY	heading6.line-before	CDATA	"line"	>
<!ENTITY	hottext.fontfam	CDATA	"new century schoolbook"	>
<!ENTITY	hottext.foreground	CDATA	"medium sea green"	>
<!ENTITY	hottext.slant	CDATA	"Roman"	>
<!ENTITY	hottext.weight	CDATA	"medium"	>
<!ENTITY	hottext.score	CDATA	"under"	>
<!ENTITY	revchgbw	CDATA	"if(attr(revision),under,)"	>
<!ENTITY	revchgcol	CDATA	"if(attr(revision),blueviolet,)"	>
<!ENTITY	std.font	CDATA	"new century schoolbook"	>
<!ENTITY	std.font-size	CDATA	"14"	>
<!ENTITY	std.font.color	CDATA	"black"	>
<!ENTITY	std.indent	CDATA	"10"	>
<!ENTITY	std.leftindent	CDATA	"10"	>
<!ENTITY	std.line-spacing	CDATA	"16"	>
<!ENTITY	std.para.left-indent	CDATA	"+=30"	>
<!ENTITY	std.para.space-before	CDATA	"6"	>
<!ENTITY	std.rightindent	CDATA	"12"	>
<!ENTITY        subtitle.font   CDATA   "lucida"  >
<!ENTITY        subtitle.font-size   CDATA   "14"  >
<!ENTITY        subtitle.line-spacing   CDATA   "16"  >
<!ENTITY        subtitle.color   CDATA   "black"  >
<!ENTITY        subtitle.leftindent   CDATA   "10"  >
<!ENTITY        subtitle.indent   CDATA   "10"  >
<!ENTITY	subscript	CDATA	"-3"	>
<!ENTITY	tag.color	CDATA	"purple"	>
<!ENTITY	title.color	CDATA	"blue"	>
<!ENTITY	title.font	CDATA	"lucida"	>
<!ENTITY	verbatim.font	CDATA	"courier new"	>
<!ENTITY	verbatim.font-size	CDATA	"10"	>
<!ENTITY	verbatim.line-spacing	CDATA	"14"	>
<!ENTITY	verbatim.small-font-size	CDATA	"8"	>
<!--End Entity Definitions -->

<sheet left-indents="column-relative">

<?INSTED COMMENT: GROUP asistext>

<group name="asistext">
	<font-family>	&verbatim.font	</>
	<font-size>	&verbatim.font-size	</>
	<left-indent>	+=30	</>
	<line-spacing>	&verbatim.line-spacing	</>
	<space-after>	if(islast(),10,0)	</>
	<justification>	verbatim	</>
	<break-before>	Line	</>
</group>

<style name="VERBATIM" group="asistext">
	<space-before>	if(eq(tag(lsibling()),'VERBATIM'),2,8)	</>
	<space-after>	if(eq(tag(rsibling()),'VERBATIM'),2,8)	</>
</style>

<?INSTED COMMENT: GROUP booktitle>

<group name="booktitle">
	<font-family>	&booktitle.font	</>
	<font-weight>	&booktitle.font-weight	</>
	<font-size>	&booktitle.font-size	</>
	<foreground>	&booktitle.color	</>
	<line-spacing>	&booktitle.line-spacing	</>
</group>

<style name="TITLEPG,TITLE" group="booktitle">
</style>

<?INSTED COMMENT: GROUP emphs>

<group name="emphs">
	<font-slant>	Italic	</>
</group>

<style name="CMD" group="emphs">
	<font-weight>	Bold	</>
	<font-slant>	Roman	</>
</style>

<style name="FILE" group="emphs">
</style>

<style name="FUNC" group="emphs">
	<font-slant>	Roman	</>
	<script>	ebt-link root=idmatch(id, attr(rid)) window="new"	</>
</style>

<style name="INLINE" group="emphs">
	<font-slant>	Roman	</>
</style>

<style name="PROP" group="emphs">
	<script>	ebt-link root=idmatch(id, attr(rid)) window="new"	</>
</style>

<style name="RESV" group="emphs">
	<font-slant>	Roman	</>
	<script>	ebt-link root=idmatch(id, attr(rid)) window="new"	</>
</style>

<style name="SCRIPT" group="emphs">
	<script>	ebt-link root=idmatch(id, attr(rid)) window="new"	</>
</style>

<?INSTED COMMENT: GROUP equations>

<group name="equations">
	<hide>	Children	</>
	<break-before>	None	</>
	<break-after>	None	</>
	<inline>	equation target=me()	</>
</group>

<style name="F" group="equations">
</style>

<style name="FD" group="equations">
	<font-size>	18	</>
	<space-before>	10	</>
	<space-after>	10	</>
	<break-before>	line	</>
	<break-after>	line	</>
</style>



<?INSTED COMMENT: GROUP frontmatter>

<group name="frontmatter">
	<font-family>	&frontmatter.font	</>
	<left-indent>	&std.leftindent	</>
	<break-before>	Line	</>
</group>

<style name="AUTHOR" group="frontmatter">
</style>

<style name="AUTHOR,P" group="frontmatter">
</style>

<style name="COPYRTPG" group="frontmatter">
	<icon-position>	Right	</>
	<hide>	Children	</>
	<script>	ebt-reveal title="Copyright Notice" stylesheet="arbor.rev"	</>
	<icon-type>	copyrt	</>
</style>

<style name="DATE" group="frontmatter">
	<hide>	Children	</>
</style>

<style name="SUBTITLE" group="frontmatter">
	<break-before>	Line	</>
</style>



<?INSTED COMMENT: GROUP genlist>

<!-- This is a first level list  -->
<group name="genlist">
	<left-indent>	&genlist.left-indent	</>
	<first-indent>	&genlist.first-indent	</>
	<space-before>	&std.para.space-before	</>
	<break-before>	Line	</>
</group>

<style name="ITEM,LIST" group="genlist">
	<break-before>	line	</>
	<left-indent>	90	</>
</style>

<style name="LIST" group="genlist">
</style>



<?INSTED COMMENT: GROUP genlist2>

<!-- This is a second level list  -->
<group name="genlist2">
	<left-indent>	&genlist2.left-indent	</>
	<first-indent>	&genlist2.first-indent	</>
	<break-before>	Line	</>
</group>



<?INSTED COMMENT: GROUP heading1>

<group name="heading1">
	<font-family>	&heading1.font	</>
	<font-weight>	&heading1.font-weight	</>
	<font-size>	&heading1.font-size	</>
	<foreground>	&heading1.color	</>
	<line-spacing>	&heading1.line-spacing	</>
	<space-before>	&heading1.space-before	</>
</group>

<style name="APPENDIX,TITLE" group="heading1">
	<text-before>Appendix format(cnum(parent()),LETTER):  </>
</style>

<style name="CHAPTER,TITLE" group="heading1">
	<text-before>if(eq(cnum(ancestor()),1),'',join(sub(cnum(parent()),1),'. '))</>
</style>



<?INSTED COMMENT: GROUP heading2>

<group name="heading2">
	<font-family>	&heading2.font	</>
	<font-weight>	&heading2.font-weight	</>
	<font-size>	&heading2.font-size	</>
	<foreground>	&heading2.color	</>
	<line-spacing>	&heading2.line-spacing	</>
	<space-before>	&heading2.space-before	</>
</group>

<style name="SECTION,TITLE" group="heading2">
</style>


<?INSTED COMMENT: GROUP heading3>

<group name="heading3">
	<font-family>	&heading3.font	</>
	<font-weight>	&heading3.font-weight	</>
	<font-slant>	&heading3.font-slant	</>
	<font-size>	&heading3.font-size	</>
	<foreground>	&heading3.color	</>
	<line-spacing>	&heading3.line-spacing	</>
	<space-before>	&heading3.space-before	</>
	<space-after>	&heading3.space-after	</>
	<hrule>	None	</>
</group>

<style name="MAP,TITLE" group="heading3">
</style>


<?INSTED COMMENT: GROUP heading4>

<group name="heading4">
	<font-family>	&heading4.font	</>
	<font-weight>	&heading4.font-weight	</>
	<font-size>	&heading4.font-size	</>
	<foreground>	&heading4.color	</>
	<line-spacing>	&heading4.line-spacing	</>
	<space-before>	&heading4.space-before	</>
</group>

<style name="BLOCK,LABEL" group="heading4">
</style>


<?INSTED COMMENT: GROUP heading5>

<group name="heading5">
	<font-family>	&heading5.font	</>
	<font-weight>	&heading5.font-weight	</>
	<font-size>	&heading5.font-size	</>
	<foreground>	&heading5.color	</>
	<left-indent>	&heading5.left-indent	</>	
	<line-spacing>	&heading5.line-spacing	</>
	<space-before>	&heading5.space-before	</>
</group>


<style name="SUBBLOCK,LABEL" group="heading5">
</style>


<?INSTED COMMENT: GROUP heading6>

<group name="heading6">
	<font-family>	&heading6.font	</>
	<font-weight>	&heading6.font-weight	</>
	<font-size>	&heading6.font-size	</>
	<foreground>	&heading6.color	</>
	<first-indent>	&heading6.left-indent	</>
	<line-spacing>	&heading6.line-spacing	</>
	<space-before>	&heading6.space-before	</>
	<space-after>	&heading6.space-after	</>
</group>


<?INSTED COMMENT: GROUP hotregion>


<group name="hotregion">
	<script> ebt-link root=idmatch(attr(idref)) stylesheet=popup.rev window=new 	</>
</group>

<style name="RECT" group="hotregion">
</style>

<style name="POLYLINE" group="hotregion">
</style>

<style name="ELLIPSE" group="hotregion">
</style>


<?INSTED COMMENT: GROUP hottext>


<group name="hottext">
	<font-weight>	&hottext.weight	</>
	<foreground>	&hottext.foreground	</>
	<score>		&hottext.score	</>
	<script>	ebt-link root=idmatch(ID,attr(rid))    stylesheet=popup.rev window=new	</>
</group>

<style name="EXTREF" group="hottext">
	<select>	if(attr(url),urllink,loclink)	</>
</style>

<style name="LOCLINK" group="hottext">
	<script>	ebt-link root=idmatch(ID, ATTR(RID)) book=attr(book)    window=new stylesheet="popup.rev"	</>
</style>

<style name="URLLINK" group="hottext">
	<script>	x-html-link url="attr(book)attr(rid)"	</>
</style>

<style name="XREF" group="hottext">
	<font-family>	&hottext.fontfam	</>
	<font-slant>	&hottext.slant	</>
	<script>	ebt-link root=idmatch(ID, ATTR(RID)) window=new stylesheet=popup.rev	</>
</style>




<?INSTED COMMENT: GROUP paragraph>

<group name="paragraph">
	<left-indent>	&std.para.left-indent	</>
	<space-before>	&std.para.space-before	</>
	<space-after>	if (islast(), 10, 0)	</>
	<break-before>	Line	</>
</group>


<style name="P" group="paragraph">
</style>

<style name="SUBBLOCK,P" group="paragraph">
	<left-indent>	&std.para.left-indent	</>
	<break-before>	if (isfirst(), none, line)	</>
</style>



<!--<?INSTED COMMENT: GROUP title>

<group name="title">
	<font-family>	&title.font	</>
	<font-weight>	Bold	</>
	<foreground>	&title.color	</>
	<justification>	Left	</>
	<break-before>	line	</>
</group>


<style name="LABEL" group="title">
	<font-size>	14	</>
	<line-spacing>	17	</>
	<space-before>	12	</>
</style>-->



<?INSTED COMMENT: UNGROUPED STYLES FOLLOW>

<style name="#ROOT">
	<foreground>	black	</>
	<break-before>	Line	</>
</style>

<style name="#SDATA">
	<font-family>	attr(font)	</>
	<font-weight>	attr(weight)	</>
	<character-set>	attr(charset)	</>
	<text-before>char(attr(code))</>
</style>

<style name="#TAGS">
	<font-family>	fixed	</>
	<font-weight>	Medium	</>
	<font-size>	12	</>
	<score>	Under	</>
</style>

<style name="ABLOCK">
	<hide>	All	</>
</style>

<style name="ABLOCK,TABLE">
	<hide>	All	</>
</style>

<style name="APPENDIX">
	<break-before>	Line	</>
</style>

<style name="ART">
	<space-before>	12	</>
	<break-before>	line	</>
	<script>	ebt-raster filename="attr(FILE).tif" title="attr(TITLE)"	</>
	<inline>	raster filename="attr(FILE).tif"	</>
</style>

<style name="ARTCAP,#TEXT-BEFORE">
	<font-family>	&title.font	</>
	<font-weight>	bold	</>
	<font-size>	12	</>
	<foreground>	&art.color	</>
	<line-spacing>	14	</>
	<space-before>	4	</>
	<justification>	left	</>
</style>

<style name="ARTCAP">
	<font-family>	&title.font	</>
	<font-weight>	bold	</>
	<font-size>	12	</>
	<foreground>	&art.color	</>
	<text-before>Figure if(gt(cnum(ancestor(CHAPTER)),1),join(sub(cnum(ancestor(CHAPTER)),1),-gcnum():),gcnum():) attr(TITLE)</>
</style>


<style name="B">
	<font-weight>	Bold	</>
</style>

<style name="BI">
	<font-weight>	Bold	</>
	<font-slant>	Italic	</>
</style>

<style name="CELLRULE">
	<hide>	All	</>
	<column>	True	</>
</style>

<style name="CHAPTER">
	<break-before>	Line	</>
</style>

<style name="DOCLINK">
	<font-slant>	Italics	</>
	<foreground>	red	</>
	<icon-position>	Right	</>
	<break-before>	line	</>
	<script>	ebt-link book="attr(DOC)" tname=ID tvalue="attr(RID)"	</>
	<icon-type>	exlink	</>
</style>

<style name="DOS">
	<break-before>	False	</>
	<select>	if(eq(index(env(PATH),'/'),0),ME.SHOW,ME.HIDE)	</>
</style>

<style name="EMPH">
	<select>	EMPH.attr(type)	</>
</style>

<style name="EMPH.ATTR">
	<font-slant>	Roman	</>
</style>

<style name="EMPH.BUTTON">
	<font-slant>	Italics	</>
</style>

<style name="EMPH.CMD">
	<font-weight>	Bold	</>
	<font-slant>	Roman	</>
</style>

<style name="EMPH.FILE">
	<font-slant>	Italic	</>
</style>

<style name="EMPH.FUNC">
	<font-slant>	Roman	</>
</style>

<style name="EMPH.INPUT">
	<font-family>	courier new	</>
</style>

<style name="EMPH.MENU">
	<font-slant>	Roman	</>
</style>

<style name="EMPH.OUTPUT">
	<font-family>	courier new	</>
</style>

<style name="EMPH.PROP">
	<font-slant>	Italic	</>
</style>

<style name="EMPH.PVAL">
	<font-slant>	Italic	</>
</style>

<style name="EMPH.RESV">
	<font-slant>	Roman	</>
</style>

<style name="EXTREF,I">
	<font-slant>	Italics	</>
	<script>	ebt-link root=idmatch(ID, ATTR(RID,ancestor()))  book=attr(book,ancestor())  window=new stylesheet="popup.rev"	</>
</style>

<style name="HOTSPOTGRAPHIC">
	<space-before>	12		</>
	<break-before>	Line		</>
	<inline>	raster filename="attr(filename)" hotinfo=me()  </>
</style>

<style name="I">
	<font-slant>	Italics	</>
</style>

<style name="IMIDOC">
	<font-family>	&std.font	</>
	<font-size>	14	</>
	<foreground>	black	</>
	<left-indent>	10	</>
	<right-indent>	&std.rightindent	</>
	<line-spacing>	16	</>
</style>

<style name="ITEM">
	<left-indent>	+=20	</>
	<break-before>	False	</>
</style>

<style name="ITEM,P">
	<space-after>	2	</>
</style>

<style name="MAP">
	<break-before>	Line	</>
</style>

<style name="MARKER">
	<font-family>	if(isempty(attr(type)),symbol,)	</>
	<font-weight>	Medium	</>
	<hide>	text	</>
	<break-before>	line	</>
	<text-before>switch(attr(type,parent(LIST)),'triple', format(cnum(),word('LETTER decimal roman',countword(qtag(), x,'eq(var(x),'LIST')',',')))., 'num', cnum()., 'default', content())</>
</style>

<style name="ME.HIDE">
	<hide>	All	</>
</style>

<style name="ME.SHOW">
	<hide>	Off	</>
</style>

<style name="NAME">
	<width>	100	</>
	<column>	True	</>
</style>

<style name="NOTICE">
	<font-family>	&title.font	</>
	<font-size>	14	</>
	<line-spacing>	16	</>
	<space-before>	20	</>
	<break-before>	line	</>
</style>

<style name="OWNER">
	<font-family>	&title.font	</>
	<font-size>	18	</>
	<line-spacing>	20	</>
	<space-before>	18	</>
	<justification>	Left	</>
	<break-before>	line	</>
</style>

<style name="PART">
	<space-before>	60	</>
</style>

<style name="PROPERTY">
	<break-before>	Line	</>
</style>

<style name="RM">
	<font-weight>	Medium	</>
	<font-slant>	Roman	</>
</style>

<!--<style name="ROWRULE">
	<hrule>	Before	</>
</style>-->

<style name="SECTION">
	<space-before>	40	</>
</style>

<style name="SIDEBAR">
	<icon-position>	Right	</>
	<hide>	Children	</>
	<script>	ebt-reveal title=" " stylesheet="fulltext.v"	</>
	<icon-type>	footnote	</>
</style>

<style name="SIDEBAR,LABEL">
	<hide>	Children	</>
</style>

<style name="SP.FN">
	<icon-position>	Right	</>
	<hide>	Children	</>
	<script>	ebt-reveal title="Footnote" stylesheet="fulltext.v"	</>
	<icon-type>	footnote	</>
</style>

<style name="SP.ICONSHOW">
	<left-indent>	36	</>
	<space-before>	12	</>
	<icon-position>	Inline	</>
	<break-before>	line	</>
	<icon-type>	attr(TITLE)	</>
</style>

<style name="SP.SUB">
	<vertical-offset>	&subscript	</>
</style>

<style name="SP.TEX">
	<break-before>	Line	</>
</style>

<style name="SP.TEXEQN">
	<break-before>	Line	</>
</style>

<style name="SPECIAL">
	<font-family>	&title.font	</>
	<select>	SP.toupper(attr(type))	</>
</style>

<style name="STYLE">
	<left-indent>	&std.para.left-indent	</>
	<space-before>	6	</>
	<space-after>	6	</>
	<break-before>	Line	</>
	<text-before>if(attr(NAME), Property setting\(s\) for style attr(NAME):, Property setting:)</>
</style>

<style name="STYLE,#TEXT-BEFORE">
	<font-weight>	Bold	</>
</style>

<style name="SUBBLOCK">
	<break-before>	Line	</>
</style>

<style name="SYMBOL">
	<font-family>	symbol	</>
	<font-weight>	Medium	</>
</style>

<style name="TABLE">
	<space-before>	20	</>
	<space-after>	20	</>
	<break-before>	Line	</>
	<make>	table
		total-width=add(mapword(tableinfo(arbor,column-widths),x,'int(add(mult(var(x),var(hsize),.001),0.5))'))
		column-widths=\"mapword(tableinfo(arbor,column-widths),x,'int(add(mult(var(x),var(hsize),.001),0.5))')\"
		row-border="s"
		column-border="s"</>
</style>

<style name="TABLECELL">
	<left-indent>	10	</>
	<right-indent>	10	</>
	<justification>	tableinfo(arbor,justification)	</>
	<make>	table-cell	column-number=tableinfo(arbor,column-number)
		n-columns-spanned=tableinfo(arbor,n-columns-spanned)
		n-rows-spanned=tableinfo(arbor,n-rows-spanned)
		cell-row-alignment=tableinfo(arbor,alignment)
		row-border="s"
		column-border="s"</>
</style>

<style name="TABLECELL,LIST">
	<space-before>	4	</>
	<break-before>	line	</>
</style>

<style name="TABLECELL,P">
	<left-indent>	if(eq(cnum(ancestor()),1), +=10, +=0)	</>
	<break-before>	if(eq(cnum(),1), None, Line)	</>
</style>

<style name="TABLEROW">
	<font-weight>	if(gt(cnum(),1),Medium,Bold)	</>
	<space-before>	4	</>
	<space-after>	4	</>
	<make>	table-row	</>
</style>


<style name="TBLOCK">
	<break-before>	Line	</>
</style>

<style name="TEX">
	<font-family>	&std.font	</>
	<font-size>	18	</>
	<space-before>	10	</>
	<space-after>	10	</>
	<hide>	Children	</>
	<break-before>	Line	</>
	<break-after>	Line	</>
	<inline>	equation target=me()	</>
</style>

<style name="TEXEQN">
	<icon-position>	Right	</>
	<hide>	Children	</>
	<break-after>	Line	</>
	<script>	ebt-reveal root=me() stylesheet="texeqn.rev"	</>
	<icon-type>	default	</>
</style>

<style name="TOCPG">
	<hide>	All	</>
</style>

<style name="UL">
	<font-weight>	Bold	</>
	<score>	None	</>
</style>

<style name="UNIX">
	<select>	if(gt(index(env(PATH),'/'),0),ME.SHOW,ME.HIDE)	</>
</style>

<style name="VALUE">
	<left-indent>	+=110	</>
	<column>	True	</>
</style>


</sheet>


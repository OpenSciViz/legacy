
/*
 * Copyright (c) 1994 by CenterLine Software, Inc., Cambridge, MA.
 * All rights reserved. Use, duplication, or disclosure is subject 
 * to restrictions described by license agreements with CenterLine
 * Software, Inc. 
 */

#if !defined(_testcenter_h_)
#define _testcenter_h_ 1

#if defined(__cplusplus)
extern "C" {
#endif
    
/*
 * Make it possible to use this include file with or without 
 * prototypes.
 */

#if !defined(__A)
# define __A_DEFINED_IN_FILE 1
# if defined(__STDC__) || defined(__cplusplus)    
#  define __A(arglist) arglist
# else
#  define __A(arglist) ()
# endif
#endif /* !defined(__A) */

#define TC_MAJOR_VERSION 2
#define TC_MINOR_VERSION 0



/*
 * Event simulation support ...
 */

#define TC_TEST_COUNT(A) \
  sizeof(A)/sizeof(A[0])

  
typedef struct tc_errsim_test
  tc_errsim_test_t;

typedef struct tc_errsim_tmpl
  tc_errsim_tmpl_t;

/* opaque type used to identify a simulation */
typedef struct tc_errsim
  tc_errsim_t;

typedef int 
  (*tc_func_t)();


#define TC_LOCKED_RETVAL  1
#define TC_LOCKED_ERRNO   2

struct tc_errsim_test {
  unsigned int         test_number;
  char               * test_name;
  char               * format;
  unsigned int         flags;
  unsigned int         default_errno;
  char *               default_return_value;
  unsigned int         reserved_1;
  unsigned int         reserved_2;
};

struct tc_errsim_tmpl {
  char               * function_name;
  char               * replacement_function;
  int                  test_count;
  tc_errsim_test_t   * test;
  unsigned             reserved_1;
  unsigned             reserved_2;
};


/*
 * This function retrieves all tests that matches the
 * specifed function. These tests have to be freed with
 * the tc_free_errsims routine.
 */
extern tc_errsim_t **
tc_get_errsims __A((tc_errsim_tmpl_t * tmpl, int *count));

/*
 * This function frees up the tests that were retrieved by
 * calling tc_get_errsims
 */
extern void
tc_free_errsims __A((tc_errsim_t ** sim, int count));
  
/*
 * This function returns the test id, the test name and
 * the supplied arguments for a test.
 */
extern void 
tc_get_test_info __A((tc_errsim_t *sim, unsigned int * test_number, \
		      char **test_name, int *num_args));

/*
 * The function returns the nth argument.
 */
extern void *
tc_get_arg __A((tc_errsim_t * sim, int nth));

/*
 * This function returns the selected return value
 */
extern char *
tc_get_return_value __A((tc_errsim_t *sim));

/*
 * This function returns the selected return value
 */
extern int
tc_get_int_return_value __A((tc_errsim_t *sim));

/*
 * This function returns the selected error number to set.
 */
extern int
tc_get_errno __A((tc_errsim_t *sim));

/*
 * This function assigns the error number to specified value.
 */
extern void
tc_set_errno __A((tc_errsim_t *sim, int errno));

/*
 * Returns the address of the real function to be invoked.
 */
extern tc_func_t
tc_get_func_addr __A((tc_errsim_tmpl_t * tmpl));


/*
 * Reports that simulation point was taken.
 */
extern void
tc_report_simulation __A((tc_errsim_t * sim, const char * message));



/*
 * Misc.. utility functions ...
 */


/*
 * Reports total amount of memory allocated.
 */
extern int
testcenter_total_alloc();


/*
 * Subtracts padding frmo size
 */
extern int
testcenter_real_alloc_size(int);


#if defined(__cplusplus)
}      /* matches extern "C" { at start of file */
#endif

#if defined(__A_DEFINED_IN_FILE)
#undef __A
#undef __A_DEFINED_IN_FILE
#endif

#endif

//
// account.h
//
#ifndef __account_h__
#define __account_h__

#include <string.h>
#include <iostream.h>

class account 

{
private:
	char* name;
protected:
	double balance;
	double rate;
public:
	account(char *c="none") ; 
	~account (); 
	void deposit (double amount);
	void withdraw (double amount ); 
	void display();
	virtual void update_balance();
};

#endif

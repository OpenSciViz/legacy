//
// banking.C
//

#include "account.h"
#include "List.h"
#include <iostream.h>

int
main ()
{
account *my_checking = new account("my account");
my_checking->deposit(1000);
my_checking->update_balance();

account *your_checking = new account("your account");

List<account> A_Bank(*my_checking);
A_Bank.add(*your_checking);
cout << "The bank accounts are as follows: ";
A_Bank.display();

delete my_checking;
delete your_checking;
}

//
// List.c   
//
//#include List.h

template <class T> List<T>::List() // conversion of T to a List
{
data = NULL;
next= NULL;
}

template <class T> List<T>::List(T& type) // conversion of T to a List, given a T
{
data = &type;
next= NULL;
}

template <class T> List<T>* List<T>::nextLink() // returns pointer to next item in list
{
  return next;
}

template <class T> void List<T>::add(T &newData) // adds newData to list
{
List<T>* newNext = new (List<T>);  
newNext->data=&newData;				// construct a List that contains newData
newNext->next=NULL;
List<T> *temp = this; 				// create a temporary List to walk through list
while (temp->next) temp = temp->next;
if (temp->next == NULL) temp->next = newNext;  	// set list to point to new List
}
   
template <class T> T* List<T>::thisData() // returns pointer to data in this item
{
  return data;
}

template <class T> int List<T>::operator==(T type) // return 0 if *(this->data) not equal to type
{
if (*(this->data)==type) return 1; 
else return 0;
}

template <class T> void  List<T>::display() // works only if T defines display

{ 
 cout << "\n";
 List<T> *temp=this; 
 while (temp && data) 
 { 
  temp->data->display(); 
  cout << "\n"; 
  temp=temp->next; 
 }
}


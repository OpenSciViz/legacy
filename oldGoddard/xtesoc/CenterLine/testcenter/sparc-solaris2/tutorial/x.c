#include "x.h"
/* x stuff */

void open_window()
{
	char *window_name = "Rain Window";
	char *display_name = NULL;
	XSizeHints size_hints;
	XWindowAttributes attr;
	int depth;

	if ((display = XOpenDisplay(display_name)) == NULL)
	{
		(void)fprintf(stderr, "Cannot connect to X server %s\n", 
					XDisplayName(display_name));
		exit(-1);
	}

	screen = DefaultScreen(display);
	
	win = XCreateSimpleWindow(display, RootWindow(display, screen), 
		WX, WY, WIDTH, HEIGHT, 0, 
		BlackPixel(display, screen), 
		WhitePixel(display, screen));

	XGetWindowAttributes(display, win, &attr);
	depth = attr.depth;

	size_hints.flags = USPosition|USSize|PMaxSize|PMinSize;
	size_hints.x = WX; size_hints.y = WY;
	size_hints.width = WIDTH; size_hints.height = HEIGHT;
	size_hints.min_width = WIDTH; size_hints.min_height = HEIGHT;

	XSetStandardProperties(display, win, window_name, NULL, 
		NULL, NULL, NULL, &size_hints);

	gc = XCreateGC(display, win, 0, NULL);
	ngc = XCreateGC(display, win, 0, NULL);
	XSetForeground(display, ngc, WhitePixel(display, screen));
	XSetBackground(display, ngc, BlackPixel(display, screen));

	XSetForeground(display, gc, BlackPixel(display, screen));
	XSetBackground(display, gc, WhitePixel(display, screen));
	XSetPlaneMask(display, gc, 
		BlackPixel(display,screen)^WhitePixel(display,screen));

	XMapWindow(display, win);

	XFlush(display);
	XSync(display, 0);
	do_wait();
}

void close_window()
{
	XFreeGC(display, gc);
	XCloseDisplay(display);
	do_wait();
}

void bclear()
{
	XFillRectangle(display, win, gc, 0, 0, WIDTH, HEIGHT);
}

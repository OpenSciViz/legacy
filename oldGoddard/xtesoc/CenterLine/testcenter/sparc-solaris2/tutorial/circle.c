#include "x.h"


void draw_circle(xOrigin, yOrigin)
    short xOrigin,
          yOrigin;
{
  int filled = 0;
  short radius = 30;

  if (!filled) {
    XDrawArc(display, win, gc, xOrigin, yOrigin+IMARGIN, radius, radius, 0, 360*64);
  } else {
    XFillArc(display, win, gc, xOrigin, yOrigin+IMARGIN, radius, radius, 0, 360*64);
  }
  XFlush(display);
}


void undraw_circle(xOrigin, yOrigin)
    short xOrigin,
          yOrigin;
{
  int filled = 0;
  short radius = 30;

  if (!filled) {
    XDrawArc(display, win, ngc, xOrigin, yOrigin+IMARGIN, radius, radius, 0, 360*64);
  } else {
    XFillArc(display, win, ngc, xOrigin, yOrigin+IMARGIN, radius, radius, 0, 360*64);
  }
  XFlush(display);
}



#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#include "drops.h"
#include "x.h"

#define LOGNAME "/tmp/rain.logfile"

char *logfile;
FILE *logfp;


main(argc, argv)
     int argc; 
     char *argv[];
{
  int *drop_array, *collect_drops();
  int count;
  int drop_hit = 0;
  char buf[1024];
  int i;
  char shape[100];
  int logfile_len;
  
  if (argc == 2)
    strcpy(shape, argv[1]);
  else
    strcpy(shape, "circle");
  
 retry:  
  open_window();
  create_table();
  draw_it(shape);
  sleep(2);
  bclear();
  close_window();
  
  /* count the number of drops that fell all the way to the ground */
  drop_array = collect_drops(&count);
  
  /* if no drops fall, start again */
  if (drop_array == 0) {
    printf("No drops fell to the ground; starting again\n");
    goto retry;
  }
  /* set up condition to rerun these functions with the same data */
  /* note that this condition could create an infinite loop */
  
  if (count == 0) goto retry;
  
  for (i = 0; i <= count; i++)
    {
      if (drop_array[i] >= HEIGHT)
	drop_hit++;
    }
	printf("%d shapes hit the ground.\n", drop_hit);
  
  /* create a temporary logfile name */
  sprintf(buf, "%s.%d", LOGNAME, getpid());
  
  logfile_len = strlen(buf);
  logfile = (char *)malloc(logfile_len); 
  if (logfile == 0 ) {
    printf("Not enough memory, exiting\n");
    exit(1);
  }
  
  memcpy(logfile, buf, logfile_len);
  
  /* open the logfile */
  logfp = fopen(logfile, "w");

  /* handle error if file pointer is null */
  if (logfp == NULL) {
    fprintf(stderr, "could not open %s",logfile);
    logfp = stdout;
  }
  
  
  fprintf(logfp, "Process %d starting\n", getpid());
  fprintf(logfp, "%d shapes hit the ground.\n", drop_hit);
  fprintf(logfp, "Process %d exiting\n", getpid());
  fclose(logfp);

  exit(0);
}


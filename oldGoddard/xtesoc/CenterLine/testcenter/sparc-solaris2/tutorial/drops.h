
/*  X version */

/* initial position of window */
#define WX	100
#define WY	100

/* size of window */
#define WIDTH 600
#define HEIGHT 300
#define MARGIN 0

/* number of coordinates */
#define LENGTH 500 
/* was 328 */

/* size of icon */
#define IWIDTH 40

#define IHEIGHT 20

#define INDEX(x) ((x)+100)

#define DECR (100)


extern void open_window();
extern void close_window();
extern void draw_circle();
extern void undraw_circle();
extern void create_table();
extern void do_wait();
extern int draw_it();

extern void store_shape();
extern void draw_rectangle();
extern void undraw_rectangle();














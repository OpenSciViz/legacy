
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>

#include "drops.h"

int shape_status[WIDTH];
#define INC 10

draw_it(shape)
char *shape;
{
    int count ;
    int i;
    int j;
    int max;

    srand(time(0));
    max = (rand() % 300) + 300;

    /* do the drawing */
    for (count = 0; count < max; count ++)
    {
	int xpos = rand() % WIDTH;

	if (shape_status[xpos] == 0)
	{
	    int max;
	    j = (xpos <= (IWIDTH/2 - 1)) ? 0 : (xpos - IWIDTH/2 - 1);
	    max = (xpos + IWIDTH/2) > WIDTH ? WIDTH : (xpos+IWIDTH/2) ;

	    for ( ; j < max ; j++)
		if (shape_status[j]) 
		    break;
	    
	    if (j != xpos + IWIDTH/2) continue;

	    if (strcmp(shape, "rectangle") == 0)
		draw_rectangle(xpos, shape_status[xpos]);
	    else
		draw_circle(xpos, shape_status[xpos]);
	    shape_status[xpos] += INC;
	}

	remember_shape_name(shape);

	for (i = 0 ; i < WIDTH; i ++)
	{
	    if (shape_status[i])
	    {
		if (strcmp(shape, "rectangle") == 0)
		{
		    undraw_rectangle(i, shape_status[i]-INC);
		    draw_rectangle(i, shape_status[i]);
		}
		else
		{
		    undraw_circle(i, shape_status[i]-INC);
		    draw_circle(i, shape_status[i]);
		}
		shape_status[i] += INC;
	    }
	}
    }
    return 0;
}



int *collect_drops(count)
int *count;
{
    int i;
    int *ret;

    int total = 0;
    for (i = 0; i < WIDTH; i++)
	if (shape_status[i]) 
		total++;

    ret = (int *)calloc(total, sizeof(int));

    total = 0;
    for (i = 0; i < WIDTH; i++)
	if (shape_status[i]) 
		ret[total++] = shape_status[i];

    *count = total;
    return ret;
}

char *shape_name;

/*
 * Called to remember in the global variable 'shape_name' the
 * name of the shape that we are drawing.
 */
remember_shape_name(name)
char *name;
{
    shape_name = (char *)malloc(strlen(name) + 1);

    if (shape_name)
    {
	strcpy(shape_name, name);
    }
    else {
        printf("Not enough memory; exiting"); 
        exit(1);
        }
}

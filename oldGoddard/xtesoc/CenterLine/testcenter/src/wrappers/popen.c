
/*
 * Copyright (c) 1994 by CenterLine Software, Inc., Cambridge, MA.
 * All rights reserved. Use, duplication, or disclosure is subject 
 * to restrictions described by license agreements with CenterLine
 * Software, Inc. 
 */


#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>

#include "testcenter.h"
#include "util.h"

FILE *
tc_popen_wrap(const char *pathname, const char *type);

/*
 * Test number definitions
 */
#define TEST_GENERIC        1
#define TEST_COMMAND        2
#define TEST_REPLACE        3
#define TEST_CMD_AND_TYPE   4

/*
 * Definitions for our tests.
 */
static tc_errsim_test_t
popen_tests[] =
{
  {
    TEST_GENERIC,
    "Failure (return NULL)",
    NULL,
    TC_LOCKED_RETVAL,
    ENOENT,
    "0",
  },
  {
    TEST_COMMAND,
    "Command Match",
    "Command matches %s",
    TC_LOCKED_RETVAL,
    0,
    "0",
  },
  {
    TEST_REPLACE,
    "Command Replace",
    "Replace command %s with %s",
    TC_LOCKED_RETVAL|TC_LOCKED_ERRNO,
    0,
    NULL,
  },
  {
    TEST_CMD_AND_TYPE,
    "Command and Type Match",
    "Command matches %s and type matches %s",
    TC_LOCKED_RETVAL,
    0,
    "0",
  }
};


/*
 * Simulation wrapping specification.
 */
tc_errsim_tmpl_t
tc_errsim_popen = 
{
  "popen",			/* name of func errsimulated */
  "tc_popen_wrap",		/* repl func */
  TC_TEST_COUNT(popen_tests),	/* test count */
  popen_tests,			/* tests */
};


FILE *
tc_popen_wrap(const char *command, const char *type)
{
  int count, i;
  tc_errsim_t **sims;
  unsigned int test_number;
  char *test_name;
  int num_args;
  int s_errno;
  tc_func_t fp;
  char * cmd;
  const char * old_command;
  char * s_type;
  char msgbuf[1000];
  char * msg;

  /*
   * Get a pointer to an array of 
   * error simulations that matched 
   */
  sims = tc_get_errsims(&tc_errsim_popen, &count);
    
  /*
   * Process each error simulation setting 
   */
  for (i = 0; i < count; i++) {
    /*
     * For each error simulation, get the test that was
     * set and the number of args specified for that test
     */
    tc_get_test_info(sims[i], &test_number, &test_name, &num_args);
	
    switch (test_number) {
    case TEST_COMMAND:
      cmd = (char *) tc_get_arg(sims[i], 0);
      if (cw_compare(cmd, command)) {
	sprintf(msg, "Command %s", command);
	goto take_sim;
      }
      break;

    case TEST_REPLACE:
      cmd = (char *) tc_get_arg(sims[i], 0);
      if (cw_compare(cmd, command)) {
	old_command = command;
	command = (const char *) tc_get_arg(sims[i], 1);
	sprintf(msg, "Command %s replaced with %s", old_command, command);
	tc_report_simulation(sims[i], msg);
      }
      break;

    case TEST_CMD_AND_TYPE:
      cmd = (char *) tc_get_arg(sims[i], 0);
      s_type = (char *) tc_get_arg(sims[i], 1);
      if (cw_compare(cmd, command) && cw_compare(s_type, type)) {
	sprintf(msg, "Command %s (type %s)", command, type);
	goto take_sim;
      }
      break;

    case TEST_GENERIC:
      msg = NULL;
      /* fall thru */
      
    take_sim:
      tc_report_simulation(sims[i], msg);
      s_errno = tc_get_errno(sims[i]);
      if (s_errno)
	tc_set_errno(sims[i], s_errno);
      tc_free_errsims(sims, count);
      return NULL;
    }
  }
  
  tc_free_errsims(sims, count);
  fp = tc_get_func_addr(&tc_errsim_popen);
  return (FILE *) (*fp)(command, type);
}

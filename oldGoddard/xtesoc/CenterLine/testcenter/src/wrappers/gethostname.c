
/*
 * Copyright (c) 1994 by CenterLine Software, Inc., Cambridge, MA.
 * All rights reserved. Use, duplication, or disclosure is subject 
 * to restrictions described by license agreements with CenterLine
 * Software, Inc. 
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

#include "testcenter.h"

int
tc_gethostname_wrap(char *, size_t);

/*
 * Test number definitions
 */
#define TEST_GENERIC        1
#define TEST_REPLACE        2

/*
 * Definitions for our tests.
 */
static tc_errsim_test_t
gethostname_tests[] =
{
  {
    TEST_GENERIC,
    "Failure (return -1)",
    NULL,
    TC_LOCKED_RETVAL,
    ENOENT,
    "-1",
  },
  {
    TEST_REPLACE,
    "Replace",
    "Replace host name with %s",
    TC_LOCKED_RETVAL|TC_LOCKED_ERRNO,
    0,
    NULL,
  }
};


/*
 * Simulation wrapping specification.
 */
tc_errsim_tmpl_t
tc_errsim_gethostname = 
{
  "gethostname",			/* name of func errsimulated */
  "tc_gethostname_wrap",        	/* repl func */
  TC_TEST_COUNT(gethostname_tests),	/* test count */
  gethostname_tests,			/* tests */
};


/*
 * The wrapper function. This function will be called instead of
 * gethostname when the location and count specification matches.
 */
int
tc_gethostname_wrap(char *hostname, size_t size)
{
  tc_errsim_t **sims;
  unsigned int test_number;
  char *test_name;
  int num_args;
  int count, i;
  char * node;
  int simulate = 0;
  char msgbuf[1000];
  char * msg = msgbuf;
  int s_errno;
  tc_func_t fp;
  int l;
  int retv;

  /*
   * Get a pointer to an array of error simulations that 
   * matched this wrapping.
   */
  sims = tc_get_errsims(&tc_errsim_gethostname, &count);
    
  /*
   * Process each error simulation setting 
   */
  for (i = 0; i < count; i++) {
    /*
     * For each error simulation, get the test that was
     * set and the number of args specified for that test
     */
    tc_get_test_info(sims[i], &test_number, &test_name, &num_args);
	
    switch (test_number) {

    case TEST_REPLACE:
      node = (char *) tc_get_arg(sims[i], 0);
      l = strlen(node)+1;
      if (l < size) l = size;
      strncpy(hostname, node, l);
      sprintf(msg, "Hostname %s simulated", node);
      tc_report_simulation(sims[i], msg);
      tc_free_errsims(sims, count);
      return 0;

    case TEST_GENERIC:
      msg = NULL;
      tc_report_simulation(sims[i], msg);
      s_errno = tc_get_errno(sims[i]);
      if (s_errno)
	tc_set_errno(sims[i], s_errno);
      retv = tc_get_int_return_value(sims[i]);
      tc_free_errsims(sims, count);
      return retv;
    }
  }

  tc_free_errsims(sims, count);
  fp = tc_get_func_addr(&tc_errsim_gethostname);
  return (*fp)(hostname, size);
}

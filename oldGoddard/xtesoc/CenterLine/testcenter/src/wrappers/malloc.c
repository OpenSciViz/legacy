
/*
 * Copyright (c) 1994 by CenterLine Software, Inc., Cambridge, MA.
 * All rights reserved. Use, duplication, or disclosure is subject 
 * to restrictions described by license agreements with CenterLine
 * Software, Inc. 
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include "testcenter.h"

void *
tc_malloc_wrap(size_t size);

/*
 * Test number definitions
 */
#define TEST_GENERIC        1
#define TEST_ALLOCATED      2
#define TEST_SIZE_EQ        3
#define TEST_SIZE_GT        4
#define TEST_SIZE_LT        5

/*
 * Definitions for our tests.
 */
static tc_errsim_test_t
malloc_tests[] =
{
  {
    TEST_GENERIC,
    "Failure (return NULL)",
    NULL,
    TC_LOCKED_RETVAL,
    0,
    "0",
  },
  {
    TEST_ALLOCATED,
    "Total Allocated",
    "Total amount allocated >= %d",
    TC_LOCKED_RETVAL,
    ENOMEM,
    "0",
  },
  {
    TEST_SIZE_EQ,
    "Size (eq)",
    "Allocation size == %d",
    TC_LOCKED_RETVAL,
    0,
    "0",
  },
  {
    TEST_SIZE_GT,
    "Size (gt)",
    "Allocation size > %d",
    TC_LOCKED_RETVAL,
    0,
    "0",
  },
  {
    TEST_SIZE_LT,
    "Size (lt)",
    "Allocation size < %d",
    TC_LOCKED_RETVAL,
    0,
    "0",
  }
};


/*
 * Simulation wrapping specification.
 */
tc_errsim_tmpl_t
tc_errsim_malloc = 
{
  "malloc",			/* name of func errsimulated */
  "tc_malloc_wrap",		/* repl func */
  TC_TEST_COUNT(malloc_tests),	/* test count */
  malloc_tests,			/* tests */
};


/*
 * Our wrapper.
 */
void *
tc_malloc_wrap(size_t size)
{
  tc_errsim_t **sims;
  int n, i;
  unsigned int test_number;
  char *test_name;
  int num_args;
  char msgbuf[1000];
  char * msg = msgbuf;
  tc_func_t fp;
  int s_errno;
  int talloc;

  /* remove the padding that TC added */
  size = testcenter_real_alloc_size(size);

  /*
   * Get a pointer to an array of 
   * error simulations that matched 
   */
  sims = tc_get_errsims(&tc_errsim_malloc, &n);
    
  /*
   * Process each error simulation setting 
   */
  for (i = 0; i < n; i++) {
    /*
     * For each error simulation, get the test that was
     * set and the number of args specified for that test
     */
    tc_get_test_info(sims[i], &test_number, &test_name, &num_args);
	
    switch (test_number) {

    case TEST_ALLOCATED:
      talloc = (int) tc_get_arg(sims[i], 0);
      if (testcenter_total_alloc() >= (int) talloc) {
	sprintf(msg, "Total allocation > %d", talloc);
	goto take_sim;
      }
      break;

    case TEST_SIZE_EQ:
      if (size == (int) tc_get_arg(sims[i], 0)) {
	sprintf(msg, "Size == %d", size);
	goto take_sim;
      }
      break;

    case TEST_SIZE_GT:
      if (size > (int) tc_get_arg(sims[i], 0)) {
	sprintf(msg, "Size > %d", size);
	goto take_sim;
      }
      break;

    case TEST_SIZE_LT:
      if (size < (int) tc_get_arg(sims[i], 0)) {
	sprintf(msg, "Size < %d", size);
	goto take_sim;
      }
      break;

    case TEST_GENERIC:
      msg = NULL;
      /* fall thru */

    take_sim:
      tc_report_simulation(sims[i], msg);
      s_errno = tc_get_errno(sims[i]);
      if (s_errno)
	tc_set_errno(sims[i], s_errno);
      tc_free_errsims(sims, n);
      return NULL;
    }
  }

  tc_free_errsims(sims, n);
  fp = tc_get_func_addr(&tc_errsim_malloc);
  return (void *) (*fp)(size);
}

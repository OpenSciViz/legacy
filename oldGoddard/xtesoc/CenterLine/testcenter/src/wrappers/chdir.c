
/*
 * Copyright (c) 1994 by CenterLine Software, Inc., Cambridge, MA.
 * All rights reserved. Use, duplication, or disclosure is subject 
 * to restrictions described by license agreements with CenterLine
 * Software, Inc. 
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>

#include "testcenter.h"
#include "util.h"

int
tc_chdir_wrap(const char *);

/*
 * Test number definitions
 */
#define TEST_GENERIC        1
#define TEST_DIR            2
#define TEST_REPLACE        3

/*
 * Definitions for our tests.
 */
static tc_errsim_test_t
chdir_tests[] =
{
  {
    TEST_GENERIC,
    "Failure (return -1)",
    NULL,
    TC_LOCKED_RETVAL,
    ENOENT,
    "-1",
  },
  {
    TEST_DIR,
    "Directory Match",
    "Directory matches %s",
    TC_LOCKED_RETVAL,
    ENOENT,
    "-1",
  },
  {
    TEST_REPLACE,
    "Directory Replace",
    "Replace directory %s with %s",
    TC_LOCKED_RETVAL|TC_LOCKED_ERRNO,
    0,
    0,
  }
};


/*
 * Simulation wrapping specification.
 */
tc_errsim_tmpl_t
tc_errsim_chdir = 
{
  "chdir",			/* name of func errsimulated */
  "tc_chdir_wrap",		/* repl func */
  TC_TEST_COUNT(chdir_tests),	/* test count */
  chdir_tests,			/* tests */
};


/*
 * The wrapper function. This function will be called instead of
 * chdir when the location and count specification matches.
 */
int
tc_chdir_wrap(const char *pathname)
{
  tc_errsim_t **sims;
  unsigned int test_number;
  char *test_name;
  int num_args;
  int count, i;
  char * dir;
  int simulate = 0;
  char msgbuf[1000];
  char * msg = msgbuf;
  const char * old_pathname;
  int s_errno;
  tc_func_t fp;
  int retv;

  /*
   * Get a pointer to an array of error simulations that 
   * matched this wrapping.
   */
  sims = tc_get_errsims(&tc_errsim_chdir, &count);
    
  /*
   * Process each error simulation setting 
   */
  for (i = 0; i < count; i++) {
    /*
     * For each error simulation, get the test that was
     * set and the number of args specified for that test
     */
    tc_get_test_info(sims[i], &test_number, &test_name, &num_args);
	
    switch (test_number) {

    case TEST_DIR:
      dir = (char *) tc_get_arg(sims[i], 0);
      if (cw_compare(dir, pathname)) {
	sprintf(msg, "Directory=%s", pathname);
	goto take_simulation;
      }
      break;

    case TEST_REPLACE:
      dir = (char *) tc_get_arg(sims[i], 0);
      if (cw_compare(dir, pathname)) {
	old_pathname = pathname;
	pathname = (const char *) tc_get_arg(sims[i], 1);
	sprintf(msg, "Directory %s replaced with %s", old_pathname, pathname);
	tc_report_simulation(sims[i], msg);
      }
      break;

    case TEST_GENERIC:
      msg = NULL;
      /* fall thru */

    take_simulation:
      tc_report_simulation(sims[i], msg);
      s_errno = tc_get_errno(sims[i]);
      if (s_errno)
	tc_set_errno(sims[i], s_errno);
      retv = tc_get_int_return_value(sims[i]);
      tc_free_errsims(sims, count);
      return retv;
    }
  }

  tc_free_errsims(sims, count);
  fp = tc_get_func_addr(&tc_errsim_chdir);
  return (*fp)(pathname);
}

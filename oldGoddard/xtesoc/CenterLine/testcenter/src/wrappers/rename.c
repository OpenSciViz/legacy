
/*
 * Copyright (c) 1994 by CenterLine Software, Inc., Cambridge, MA.
 * All rights reserved. Use, duplication, or disclosure is subject 
 * to restrictions described by license agreements with CenterLine
 * Software, Inc. 
 */


#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>

#include "testcenter.h"
#include "util.h"

int
tc_rename_wrap(const char *source, const char * target);

/*
 * Test number definitions
 */
#define TEST_GENERIC         1
#define TEST_REPLACE_SRC     2
#define TEST_REPLACE_TGT     3
#define TEST_SOURCE          4
#define TEST_TARGET          5

/*
 * Definitions for our tests.
 */
static tc_errsim_test_t
rename_tests[] =
{
  {
    TEST_GENERIC,
    "Failure (return -1)",
    NULL,
    TC_LOCKED_RETVAL,
    ENOENT,
    "-1",
  },
  {
    TEST_REPLACE_SRC,
    "Replace Source",
    "Replace source %s with %s",
    TC_LOCKED_ERRNO|TC_LOCKED_RETVAL,
    0,
    NULL,
  },
  {
    TEST_REPLACE_TGT,
    "Replace Target",
    "Replace target %s with %s",
    TC_LOCKED_ERRNO|TC_LOCKED_RETVAL,
    0,
    NULL,
  },
  {
    TEST_SOURCE,
    "Match Source",
    "Fail when source is %s",
    TC_LOCKED_RETVAL,
    ENOENT,
    "-1",
  },
  {
    TEST_TARGET,
    "Match Target",
    "Fail when target is %s",
    TC_LOCKED_RETVAL,
    EACCES,
    "-1",
  }
};


/*
 * Simulation wrapping specification.
 */
tc_errsim_tmpl_t
tc_errsim_rename = 
{
  "rename",			/* name of func errsimulated */
  "tc_rename_wrap",		/* repl func */
  TC_TEST_COUNT(rename_tests),	/* test count */
  rename_tests,			/* tests */
};


int
tc_rename_wrap(const char *source, const char * target)
{
  int count, i;
  tc_errsim_t **sims;
  unsigned int test_number;
  char *test_name;
  int num_args;
  int s_errno;
  tc_func_t fp;
  char * src;
  const char * old_source;
  char * tgt;
  const char * old_target;
  char msgbuf[1000];
  char * msg = msgbuf;
  int retv;

  /*
   * Get a pointer to an array of 
   * error simulations that matched 
   */
  sims = tc_get_errsims(&tc_errsim_rename, &count);
    
  /*
   * Process each error simulation setting 
   */
  for (i = 0; i < count; i++) {
    /*
     * For each error simulation, get the test that was
     * set and the number of args specified for that test
     */
    tc_get_test_info(sims[i], &test_number, &test_name, &num_args);
	
    switch (test_number) {
    case TEST_REPLACE_SRC:
      src = (char *) tc_get_arg(sims[i], 0);
      if (cw_compare(src, source)) {
	old_source = source;
	source = (const char *) tc_get_arg(sims[i], 1);
	sprintf(msg, "Source %s replaced with %s", old_source, source);
	tc_report_simulation(sims[i], msg);
      }
      break;

    case TEST_REPLACE_TGT:
      tgt = (char *) tc_get_arg(sims[i], 0);
      if (cw_compare(tgt, target)) {
	old_target = target;
	target = (const char *) tc_get_arg(sims[i], 1);
	sprintf(msg, "Target %s replaced with %s", old_target, target);
	tc_report_simulation(sims[i], msg);
      }
      break;

    case TEST_SOURCE:
      src = (char *) tc_get_arg(sims[i], 0);
      if (cw_compare(src, source)) {
	sprintf(msg, "Source %s", source);
	goto take_sim;
      }
      break;

    case TEST_TARGET:
      tgt = (char *) tc_get_arg(sims[i], 0);
      if (cw_compare(tgt, target)) {
	sprintf(msg, "Target %s", target);
	goto take_sim;
      }
      break;

    case TEST_GENERIC:
      msg = NULL;
      /* fall thru */
      
    take_sim:
      tc_report_simulation(sims[i], msg);
      s_errno = tc_get_errno(sims[i]);
      if (s_errno)
	tc_set_errno(sims[i], s_errno);
      retv = tc_get_int_return_value(sims[i]);
      tc_free_errsims(sims, count);
      return retv;
    }
  }
  
  tc_free_errsims(sims, count);
  fp = tc_get_func_addr(&tc_errsim_rename);
  return (*fp)(source, target);
}

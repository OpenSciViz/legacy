
/*
 * Copyright (c) 1994 by CenterLine Software, Inc., Cambridge, MA.
 * All rights reserved. Use, duplication, or disclosure is subject 
 * to restrictions described by license agreements with CenterLine
 * Software, Inc. 
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <ctype.h>
#include <string.h>

#include "testcenter.h"
#include "util.h"

int
tc_access_wrap(const char *, int);

/*
 * Test number definitions
 */
#define TEST_GENERIC        1
#define TEST_FILE           2
#define TEST_REPLACE        3
#define TEST_FILE_AND_MODE  4

/*
 * Definitions for our tests.
 */
static tc_errsim_test_t
access_tests[] =
{
  {
    TEST_GENERIC,
    "Failure (return -1)",
    NULL,
    TC_LOCKED_RETVAL,
    ENOENT,
    "-1",
  },
  {
    TEST_FILE,
    "File Match",
    "File name matches %s",
    TC_LOCKED_RETVAL,
    ENOENT,
    "-1",
  },
  {
    TEST_REPLACE,
    "File Replace",
    "Replace file name %s with %s",
    TC_LOCKED_RETVAL|TC_LOCKED_ERRNO,
    0,
    NULL,
  },
  {
    TEST_FILE_AND_MODE,
    "File and Mode Match",
    "File name matches %s with flag %s",
    TC_LOCKED_RETVAL,
    EACCES,
    "-1",
  }
};


/*
 * Simulation wrapping specification.
 */
tc_errsim_tmpl_t
tc_errsim_access = 
{
  "access",			/* name of func errsimulated */
  "tc_access_wrap",		/* repl func */
  TC_TEST_COUNT(access_tests),	/* test count */
  access_tests,			/* tests */
};


/*
 * Compare the s2 string to the beginning of s1 withoit
 * consideration to casing.
 */
static int 
cmp(char * s1, char * s2)
{
  int l = strlen(s2);

  while (l-- > 0) {
    if ((*s1 == *s2) || (tolower(*s1) == *s2))
      continue;
    return 0;
  }
  return 1;
}


/*
 * match the mode string against the actual flags value. 
 */
static int
mode_match(tc_errsim_t * sim, char * mode, int flags)
{
  char msg[1000];
  char * pe;
  char * p;
  int val;

  pe = NULL;
  val = strtol(mode, &pe, 0);
  if (pe == NULL) 
    return flags == val;

  p = mode;
  val = 0;

  for (;;) {
    while (*p == ' ') p++;

    if (*p == '\0')
      break;

    if (cmp(p, "r_ok")) {
      val |= R_OK;
      p += 4;
    }
    else if (cmp(p, "w_ok")) {
      val |= W_OK;
      p += 4;
    }
    else if (cmp(p, "x_ok")) {
      val |= X_OK;
      p += 4;
    }
    else if (cmp(p, "f_ok")) {
      val |= F_OK;
      p += 4;
    } else 
      goto error;

    while (*p == ' ') p++;
    if (*p == '|') {
      p++;
      continue;
    }

    if (*p == '\0')
      break;
    goto error;
  }

  return val == flags;

 error:
  sprintf(msg, "Bad mode: %s", mode);
  tc_report_simulation(sim, msg);
  return 0;
}


/*
 * Make some understandable string out of the flags specification.
 */
static char *
mode_string(flags)
{
  if (flags == F_OK)
    return "F_OK";

  switch (flags) {
  case R_OK:
    return "R_OK";
  case R_OK|W_OK:
    return "R_OK|W_OK";
  case R_OK|W_OK|X_OK:
    return "R_OK|W_OK|X_OK";
  case W_OK|X_OK:
    return "W_OK|X_OK";
  case R_OK|X_OK:
    return "R_OK|X_OK";
  case W_OK:
    return "W_OK";
  case X_OK:
    return "X_OK";
  }

  return "BAD MODE";
}


/*
 * The wrapper function. This function will be called instead of
 * access when the location and count specification matches.
 */
int
tc_access_wrap(const char *pathname, int flags)
{
  tc_errsim_t **sims;
  unsigned int test_number;
  char *test_name;
  int num_args;
  int count, i;
  char * file;
  int simulate = 0;
  char msgbuf[1000];
  char * msg = msgbuf;
  char * mode;
  const char * old_pathname;
  int s_errno;
  tc_func_t fp;
  int retv;

  /*
   * Get a pointer to an array of error simulations that 
   * matched this wrapping.
   */
  sims = tc_get_errsims(&tc_errsim_access, &count);
    
  /*
   * Process each error simulation setting 
   */
  for (i = 0; i < count; i++) {
    /*
     * For each error simulation, get the test that was
     * set and the number of args specified for that test
     */
    tc_get_test_info(sims[i], &test_number, &test_name, &num_args);
	
    switch (test_number) {

    case TEST_FILE:
      file = (char *) tc_get_arg(sims[i], 0);
       if (cw_compare(file, pathname)) {
	sprintf(msg, "File=%s", pathname);
	goto take_simulation;
      }
      break;

    case TEST_REPLACE:
      file = (char *) tc_get_arg(sims[i], 0);
      if (cw_compare(file, pathname)) {
	old_pathname = pathname;
	pathname = (const char *) tc_get_arg(sims[i], 1);
	sprintf(msg, "File %s replaced with %s", old_pathname, pathname);
	tc_report_simulation(sims[i], msg);
      }
      break;

    case TEST_FILE_AND_MODE:
      file = tc_get_arg(sims[i], 0);
      mode = tc_get_arg(sims[i], 1);
      if (cw_compare(file, pathname) && mode_match(sims[i], mode, flags)) {
	sprintf(msg, "File=%s mode=%s", pathname, mode_string(flags));
	goto take_simulation;
      }
      break;


    case TEST_GENERIC:
      msg = NULL;
      /* fall thru */

    take_simulation:
      tc_report_simulation(sims[i], msg);
      s_errno = tc_get_errno(sims[i]);
      if (s_errno)
	tc_set_errno(sims[i], s_errno);
      retv = tc_get_int_return_value(sims[i]);
      tc_free_errsims(sims, count);
      return retv;
    }
  }

  tc_free_errsims(sims, count);
  fp = tc_get_func_addr(&tc_errsim_access);
  return (*fp)(pathname, flags);
}


/*
 * Copyright (c) 1994 by CenterLine Software, Inc., Cambridge, MA.
 * All rights reserved. Use, duplication, or disclosure is subject 
 * to restrictions described by license agreements with CenterLine
 * Software, Inc. 
 */


#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>

#include "testcenter.h"
#include "util.h"

int
tc_unlink_wrap(const char *path);

/*
 * Test number definitions
 */
#define TEST_GENERIC         1
#define TEST_FILE            2
#define TEST_KEEP_FILE       3

/*
 * Definitions for our tests.
 */
static tc_errsim_test_t
unlink_tests[] =
{
  {
    TEST_GENERIC,
    "Failure (return -1)",
    NULL,
    TC_LOCKED_RETVAL,
    ENOENT,
    "-1",
  },
  {
    TEST_FILE,
    "File Match",
    "Fail when file is %s",
    TC_LOCKED_RETVAL,
    EACCES,
    "-1",
  },
  {
    TEST_KEEP_FILE,
    "Keep File",
    "Keep file %s",
    TC_LOCKED_ERRNO|TC_LOCKED_RETVAL,
    0,
    NULL,
  }
};


/*
 * Simulation wrapping specification.
 */
tc_errsim_tmpl_t
tc_errsim_unlink = 
{
  "unlink",			/* name of func errsimulated */
  "tc_unlink_wrap",		/* repl func */
  TC_TEST_COUNT(unlink_tests),	/* test count */
  unlink_tests,			/* tests */
};


int
tc_unlink_wrap(const char *path)
{
  int count, i;
  tc_errsim_t **sims;
  unsigned int test_number;
  char *test_name;
  int num_args;
  int s_errno;
  tc_func_t fp;
  char * file;
  char msgbuf[1000];
  char * msg = msgbuf;
  int retv;

  /*
   * Get a pointer to an array of 
   * error simulations that matched 
   */
  sims = tc_get_errsims(&tc_errsim_unlink, &count);
    
  /*
   * Process each error simulation setting 
   */
  for (i = 0; i < count; i++) {
    /*
     * For each error simulation, get the test that was
     * set and the number of args specified for that test
     */
    tc_get_test_info(sims[i], &test_number, &test_name, &num_args);
	
    switch (test_number) {
    case TEST_FILE:
      file = (char *) tc_get_arg(sims[i], 0);
      if (cw_compare(file, path)) {
	sprintf(msg, "Path = %s", path);
	goto take_sim;
      }
      break;

    case TEST_KEEP_FILE:
      file = (char *) tc_get_arg(sims[i], 0);
      if (cw_compare(file, path)) {
	sprintf(msg, "Kept file : %s", path);
	tc_report_simulation(sims[i], msg);
	tc_free_errsims(sims, count);
	return 0;
      }
      break;

    case TEST_GENERIC:
      msg = NULL;
      /* fall thru */
      
    take_sim:
      tc_report_simulation(sims[i], msg);
      s_errno = tc_get_errno(sims[i]);
      if (s_errno)
	tc_set_errno(sims[i], s_errno);

      retv = tc_get_int_return_value(sims[i]);
      tc_free_errsims(sims, count);
      return retv;
    }
  }
  
  tc_free_errsims(sims, count);
  fp = tc_get_func_addr(&tc_errsim_unlink);
  return (*fp)(path);
}

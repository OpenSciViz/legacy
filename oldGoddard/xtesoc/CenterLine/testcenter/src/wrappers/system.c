
/*
 * Copyright (c) 1994 by CenterLine Software, Inc., Cambridge, MA.
 * All rights reserved. Use, duplication, or disclosure is subject 
 * to restrictions described by license agreements with CenterLine
 * Software, Inc. 
 */


#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

#include "testcenter.h"
#include "util.h"

int
tc_system_wrap(const char *pathname);

/*
 * Test number definitions
 */
#define TEST_GENERIC        1
#define TEST_COMMAND        2
#define TEST_REPLACE        3
#define TEST_CMD_AND_TYPE   4

/*
 * Definitions for our tests.
 */
static tc_errsim_test_t
system_tests[] =
{
  {
    TEST_GENERIC,
    "Failure (return -1)",
    NULL,
    TC_LOCKED_RETVAL,
    ENOENT,
    "-1",
  },
  {
    TEST_COMMAND,
    "Command Match",
    "Command matches %s",
    TC_LOCKED_RETVAL,
    0,
    "-1",
  },
  {
    TEST_REPLACE,
    "Command Replace",
    "Replace command %s with %s",
    TC_LOCKED_RETVAL|TC_LOCKED_ERRNO,
    0,
    NULL,
  }
};


/*
 * Simulation wrapping specification.
 */
tc_errsim_tmpl_t
tc_errsim_system = 
{
  "system",			/* name of func errsimulated */
  "tc_system_wrap",		/* repl func */
  TC_TEST_COUNT(system_tests),	/* test count */
  system_tests,			/* tests */
};


int
tc_system_wrap(const char *command)
{
  int count, i;
  tc_errsim_t **sims;
  unsigned int test_number;
  char *test_name;
  int num_args;
  int s_errno;
  tc_func_t fp;
  char * cmd;
  const char * old_command;
  char msg[1000];
  int retv;

  /*
   * Get a pointer to an array of 
   * error simulations that matched 
   */
  sims = tc_get_errsims(&tc_errsim_system, &count);
    
  /*
   * Process each error simulation setting 
   */
  for (i = 0; i < count; i++) {
    /*
     * For each error simulation, get the test that was
     * set and the number of args specified for that test
     */
    tc_get_test_info(sims[i], &test_number, &test_name, &num_args);
	
    switch (test_number) {
    case TEST_COMMAND:
      cmd = (char *) tc_get_arg(sims[i], 0);
      if (cw_compare(cmd, command)) {
	sprintf(msg, "command = %s", command);
	goto take_sim;
      }
      break;

    case TEST_REPLACE:
      cmd = (char *) tc_get_arg(sims[i], 0);
      if (cw_compare(cmd, command)) {
	old_command = command;
	command = (const char *) tc_get_arg(sims[i], 1);
	sprintf(msg, "command %s replaced with %s", old_command, command);
	tc_report_simulation(sims[i], msg);
      }
      break;

    case TEST_GENERIC:
      sprintf(msg, "generic");
      /* fall thru */
      
    take_sim:
      tc_report_simulation(sims[i], msg);
      s_errno = tc_get_errno(sims[i]);
      if (s_errno)
	tc_set_errno(sims[i], s_errno);
      retv = tc_get_int_return_value(sims[i]);
      tc_free_errsims(sims, count);
      return retv;
    }
  }
  
  tc_free_errsims(sims, count);
  fp = tc_get_func_addr(&tc_errsim_system);
  return (*fp)(command);
}

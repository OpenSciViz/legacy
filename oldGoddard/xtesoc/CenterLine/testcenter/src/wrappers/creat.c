
/*
 * Copyright (c) 1994 by CenterLine Software, Inc., Cambridge, MA.
 * All rights reserved. Use, duplication, or disclosure is subject 
 * to restrictions described by license agreements with CenterLine
 * Software, Inc. 
 */


#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>

#include "testcenter.h"
#include "util.h"

int
tc_creat_wrap(const char *pathname, int oflag);

/*
 * Test number definitions
 */
#define TEST_GENERIC        1
#define TEST_FILE           2
#define TEST_FILE_AND_MODE  3
#define TEST_REPLACE        4

/*
 * Definitions for our tests.
 */
static tc_errsim_test_t
creat_tests[] =
{
  {
    TEST_GENERIC,
    "Failure (return -1)",
    NULL,
    TC_LOCKED_RETVAL,
    EACCES,
    "-1",
  },
  {
    TEST_FILE,
    "File Match",
    "File name matches %s",
    TC_LOCKED_RETVAL,
    EACCES,
    "-1",
  },
  {
    TEST_REPLACE,
    "File Replace",
    "Replace file name %s with %s",
    TC_LOCKED_RETVAL|TC_LOCKED_ERRNO,
    0,
    0,
  },
  {
    TEST_FILE_AND_MODE,
    "File and Flag Match",
    "File name matches %s with flag %d",
    TC_LOCKED_RETVAL,
    EACCES,
    "-1",
  }
};


/*
 * Simulation wrapping specification.
 */
tc_errsim_tmpl_t
tc_errsim_creat = 
{
  "creat",			/* name of func errsimulated */
  "tc_creat_wrap",		/* repl func */
  TC_TEST_COUNT(creat_tests),	/* test count */
  creat_tests,			/* tests */
};


int
tc_creat_wrap(const char *pathname, int oflag)
{
  int count, i;
  tc_errsim_t **sims;
  unsigned int test_number;
  char *test_name;
  int num_args;
  int s_errno;
  tc_func_t fp;
  char *file;
  const char *old_pathname;
  char msg[1000];
  int s_flag;
  int retv;

  /*
   * Get a pointer to an array of 
   * error simulations that matched 
   */
  sims = tc_get_errsims(&tc_errsim_creat, &count);
    
  /*
   * Process each error simulation setting 
   */
  for (i = 0; i < count; i++) {
    /*
     * For each error simulation, get the test that was
     * set and the number of args specified for that test
     */
    tc_get_test_info(sims[i], &test_number, &test_name, &num_args);
	
    switch (test_number) {
    case TEST_FILE:
      file = (char *) tc_get_arg(sims[i], 0);
      if (cw_compare(file, pathname)) {
	sprintf(msg, "file = %s", pathname);
	goto take_sim;
      }
      break;

    case TEST_REPLACE:
      file = (char *) tc_get_arg(sims[i], 0);
      if (cw_compare(file, pathname)) {
	old_pathname = pathname;
	pathname = (const char *) tc_get_arg(sims[i], 1);
	sprintf(msg, "file %s replaced with %s", old_pathname, pathname);
	tc_report_simulation(sims[i], msg);
      }
      break;

    case TEST_FILE_AND_MODE:
      file = (char *) tc_get_arg(sims[i], 0);
      s_flag = (int) tc_get_arg(sims[i], 1);
      if (cw_compare(file, pathname) && (s_flag == oflag)) {
	sprintf(msg, "file = %s (flag = 0%o)", pathname, s_flag);
	goto take_sim;
      }
      break;

    case TEST_GENERIC:
      sprintf(msg, "generic");
      /* fall thru */
      
    take_sim:
      tc_report_simulation(sims[i], msg);
      s_errno = tc_get_errno(sims[i]);
      if (s_errno)
	tc_set_errno(sims[i], s_errno);
      retv = tc_get_int_return_value(sims[i]);
      tc_free_errsims(sims, count);
      return retv;
    }
  }
  
  tc_free_errsims(sims, count);
  fp = tc_get_func_addr(&tc_errsim_creat);
  return (*fp)(pathname, oflag);
}

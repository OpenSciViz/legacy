
/*
 * Copyright (c) 1994 by CenterLine Software, Inc., Cambridge, MA.
 * All rights reserved. Use, duplication, or disclosure is subject 
 * to restrictions described by license agreements with CenterLine
 * Software, Inc. 
 */


#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>

#include "testcenter.h"
#include "util.h"

int
tc_chmod_wrap(const char *pathname, int mode);

/*
 * Test number definitions
 */
#define TEST_GENERIC        1
#define TEST_FILE           2
#define TEST_REPLACE        3
#define TEST_FILE_AND_MODE  4

/*
 * Definitions for our tests.
 */
static tc_errsim_test_t
chmod_tests[] =
{
  {
    TEST_GENERIC,
    "Failure (return -1)",
    NULL,
    TC_LOCKED_RETVAL,
    ENOENT,
    "-1",
  },
  {
    TEST_FILE,
    "File Match",
    "File name matches %s",
    TC_LOCKED_RETVAL,
    ENOENT,
    "-1",
  },
  {
    TEST_REPLACE,
    "File Replace",
    "Replace file name %s with %s",
    TC_LOCKED_RETVAL,
    0,
    NULL,
  },
  {
    TEST_FILE_AND_MODE,
    "File and Mode Match",
    "File name matches %s with mode %d",
    TC_LOCKED_RETVAL,
    EACCES,
    "-1",
  }
};


/*
 * Simulation wrapping specification.
 */
tc_errsim_tmpl_t
tc_errsim_chmod = 
{
  "chmod",			/* name of func errsimulated */
  "tc_chmod_wrap",		/* repl func */
  TC_TEST_COUNT(chmod_tests),	/* test count */
  chmod_tests,			/* tests */
};


int
tc_chmod_wrap(const char *pathname, int mode)
{
  int count, i;
  tc_errsim_t **sims;
  unsigned int test_number;
  char *test_name;
  int num_args;
  int s_errno;
  tc_func_t fp;
  char *file;
  char msg[1000];
  int s_mode;
  const char * old_pathname;
  int retv;

  /*
   * Get a pointer to an array of 
   * error simulations that matched 
   */
  sims = tc_get_errsims(&tc_errsim_chmod, &count);
    
  /*
   * Process each error simulation setting 
   */
  for (i = 0; i < count; i++) {
    /*
     * For each error simulation, get the test that was
     * set and the number of args specified for that test
     */
    tc_get_test_info(sims[i], &test_number, &test_name, &num_args);
	
    switch (test_number) {
    case TEST_FILE:
      file = (char *) tc_get_arg(sims[i], 0);
      if (cw_compare(file, pathname)) {
	sprintf(msg, "file = %s", pathname);
	goto take_sim;
      }
      break;

    case TEST_REPLACE:
      file = (char *) tc_get_arg(sims[i], 0);
      if (cw_compare(file, pathname)) {
	old_pathname = pathname;
	pathname = (const char *) tc_get_arg(sims[i], 1);
	sprintf(msg, "file %s replaced with %s", old_pathname, pathname);
	tc_report_simulation(sims[i], msg);
      }
      break;

    case TEST_FILE_AND_MODE:
      file = (char *) tc_get_arg(sims[i], 0);
      s_mode = (int) tc_get_arg(sims[i], 1);
      if (cw_compare(file, pathname) && (s_mode == mode)) {
	sprintf(msg, "file = %s (mode = 0%o)", pathname, s_mode);
	goto take_sim;
      }
      break;

    case TEST_GENERIC:
      sprintf(msg, "generic");
      /* fall thru */

    take_sim:
      tc_report_simulation(sims[i], msg);
      s_errno = tc_get_errno(sims[i]);
      if (s_errno)
	tc_set_errno(sims[i], s_errno);
      retv = tc_get_int_return_value(sims[i]);
      tc_free_errsims(sims, count);
      return retv;
    }
  }
  
  tc_free_errsims(sims, count);
  fp = tc_get_func_addr(&tc_errsim_chmod);
  return (*fp)(pathname, mode );
}


/*
 * Copyright (c) 1994 by CenterLine Software, Inc., Cambridge, MA.
 * All rights reserved. Use, duplication, or disclosure is subject 
 * to restrictions described by license agreements with CenterLine
 * Software, Inc. 
 */

#include <stdio.h>
#include <stdlib.h>

#include "cl_regexp.h"
#include "util.h"

extern cl_regexp * tc_cl_cshregcomp(const char *);
extern int tc_cl_regexec(cl_regexp *, const char *);
extern void tc_cl_regfree(cl_regexp *);

int
cw_compare(const char * pat, const char * str)
{
  int ret;
  cl_regexp * pat_regx;

  pat_regx = tc_cl_cshregcomp(pat);

  ret = tc_cl_regexec(pat_regx, str);

  tc_cl_regfree(pat_regx);
  return ret;
}

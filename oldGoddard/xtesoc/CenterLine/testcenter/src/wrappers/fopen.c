
/*
 * Copyright (c) 1994 by CenterLine Software, Inc., Cambridge, MA.
 * All rights reserved. Use, duplication, or disclosure is subject 
 * to restrictions described by license agreements with CenterLine
 * Software, Inc. 
 */


#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>

#include "testcenter.h"
#include "util.h"

FILE *
tc_fopen_wrap(const char *pathname, const char *type);

/*
 * Test number definitions
 */
#define TEST_GENERIC        1
#define TEST_FILE           2
#define TEST_REPLACE        3
#define TEST_FILE_AND_TYPE  4

/*
 * Definitions for our tests.
 */
static tc_errsim_test_t
fopen_tests[] =
{
  {
    TEST_GENERIC,
    "Failure (return NULL)",
    NULL,
    TC_LOCKED_RETVAL,
    ENOENT,
    "0",
  },
  {
    TEST_FILE,
    "File Match",
    "File name matches %s",
    TC_LOCKED_RETVAL,
    ENOENT,
    "0",
  },
  {
    TEST_REPLACE,
    "File Replace",
    "Replace file name %s with %s",
    TC_LOCKED_RETVAL|TC_LOCKED_ERRNO,
    0,
    NULL,
  },
  {
    TEST_FILE_AND_TYPE,
    "File and Type Match",
    "File name matches %s and type matches %s",
    TC_LOCKED_RETVAL,
    EACCES,
    "0",
  }
};


/*
 * Simulation wrapping specification.
 */
tc_errsim_tmpl_t
tc_errsim_fopen = 
{
  "fopen",			/* name of func errsimulated */
  "tc_fopen_wrap",		/* repl func */
  TC_TEST_COUNT(fopen_tests),	/* test count */
  fopen_tests,			/* tests */
};


FILE *
tc_fopen_wrap(const char *pathname, const char *type)
{
  int count, i;
  tc_errsim_t **sims;
  unsigned int test_number;
  char *test_name;
  int num_args;
  int s_errno;
  tc_func_t fp;
  char * file;
  const char * old_pathname;
  char * s_type;
  char msgbuf[1000];
  char * msg = msgbuf;

  /*
   * Get a pointer to an array of 
   * error simulations that matched 
   */
  sims = tc_get_errsims(&tc_errsim_fopen, &count);
    
  /*
   * Process each error simulation setting 
   */
  for (i = 0; i < count; i++) {
    /*
     * For each error simulation, get the test that was
     * set and the number of args specified for that test
     */
    tc_get_test_info(sims[i], &test_number, &test_name, &num_args);
	
    switch (test_number) {
    case TEST_FILE:
      file = (char *) tc_get_arg(sims[i], 0);
      if (cw_compare(file, pathname)) {
	sprintf(msg, "File = '%s'", pathname);
	goto take_sim;
      }
      break;

    case TEST_REPLACE:
      file = (char *) tc_get_arg(sims[i], 0);
      if (cw_compare(file, pathname)) {
	old_pathname = pathname;
	pathname = (const char *) tc_get_arg(sims[i], 1);
	sprintf(msg, "File '%s' replaced with '%s'", old_pathname, pathname);
	tc_report_simulation(sims[i], msg);
      }
      break;

    case TEST_FILE_AND_TYPE:
      file = (char *) tc_get_arg(sims[i], 0);
      s_type = (char *) tc_get_arg(sims[i], 1);
      if (cw_compare(file, pathname) && cw_compare(s_type, type)) {
	sprintf(msg, "File = '%s' (type = '%s')", pathname, type);
	goto take_sim;
      }
      break;

    case TEST_GENERIC:
      msg = NULL;
      /* fall thru */
      
    take_sim:
      tc_report_simulation(sims[i], msg);
      s_errno = tc_get_errno(sims[i]);
      if (s_errno)
	tc_set_errno(sims[i], s_errno);
      tc_free_errsims(sims, count);
      return NULL;
    }
  }
  
  tc_free_errsims(sims, count);
  fp = tc_get_func_addr(&tc_errsim_fopen);
  return (FILE *) (*fp)(pathname, type);
}

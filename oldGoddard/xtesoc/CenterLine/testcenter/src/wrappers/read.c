
/*
 * Copyright (c) 1994 by CenterLine Software, Inc., Cambridge, MA.
 * All rights reserved. Use, duplication, or disclosure is subject 
 * to restrictions described by license agreements with CenterLine
 * Software, Inc. 
 */


#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>

#include "testcenter.h"

size_t
tc_read_wrap(int, void *, size_t);

/*
 * Test number definitions
 */
#define TEST_GENERIC         1
#define TEST_INTERRUPTED     2

/*
 * Definitions for our tests.
 */
static tc_errsim_test_t
read_tests[] =
{
  {
    TEST_GENERIC,
    "Failure (return -1)",
    NULL,
    TC_LOCKED_RETVAL,
    EIO,
    "-1",
  },
  {
    TEST_INTERRUPTED,
    "Interrupt",
    "Read interrupted after reading %d bytes",
    TC_LOCKED_RETVAL|TC_LOCKED_ERRNO,
    EINTR,
    NULL,
  }
};


/*
 * Simulation wrapping specification.
 */
tc_errsim_tmpl_t
tc_errsim_read = 
{
  "read",			/* name of func errsimulated */
  "tc_read_wrap",		/* repl func */
  TC_TEST_COUNT(read_tests),	/* test count */
  read_tests,			/* tests */
};


size_t
tc_read_wrap(int fildes, void * buf, size_t nbyte)
{
  int count, i;
  tc_errsim_t **sims;
  unsigned int test_number;
  char *test_name;
  int num_args;
  int rcnt; 
  int rval; 
  int s_errno;
  tc_func_t fp;
  char msgbuf[1000];
  char * msg = msgbuf;
  int retv;

  /*
   * Get a pointer to an array of 
   * error simulations that matched 
   */
  sims = tc_get_errsims(&tc_errsim_read, &count);
    
  /*
   * Process each error simulation setting 
   */
  for (i = 0; i < count; i++) {
    /*
     * For each error simulation, get the test that was
     * set and the number of args specified for that test
     */
    tc_get_test_info(sims[i], &test_number, &test_name, &num_args);
	
    switch (test_number) {
    case TEST_INTERRUPTED:
      rcnt = (int) tc_get_arg(sims[i], 0);
      if (rcnt < nbyte) {
	fp = tc_get_func_addr(&tc_errsim_read);
	rval = (*fp)(fildes, buf, rcnt);
	if (rval == rcnt) {
	  sprintf(msg, "Interrupted read after %d bytes", rval);
	  tc_report_simulation(sims[i], msg);
	  s_errno = tc_get_errno(sims[i]);
	  if (s_errno)
	    tc_set_errno(sims[i], s_errno);
	} 
	tc_free_errsims(sims, count);
	return rval;
      }
      break;

    case TEST_GENERIC:
      msg = NULL;
      tc_report_simulation(sims[i], msg);

      s_errno = tc_get_errno(sims[i]);
      if (s_errno)
	tc_set_errno(sims[i], s_errno);
      retv = tc_get_int_return_value(sims[i]);
      tc_free_errsims(sims, count);
      return retv;
    }
  }
  
  tc_free_errsims(sims, count);
  fp = tc_get_func_addr(&tc_errsim_read);
  return (*fp)(fildes, buf, nbyte);
}

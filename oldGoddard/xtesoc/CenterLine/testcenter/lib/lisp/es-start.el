;; $Revision: 1.2 $
(let ((core (or (getenv "CENTERLINE_CORE")
		"/usr/local/CenterLine/testcenter/sparc-sunos4")))
  (string-match "^\\(.*\\)/\\([^/]\\)+/\\([^/]\\)+$" core)
  (let ((top (substring core (match-beginning 1) (match-end 1)))
	(prod (substring core (match-beginning 2) (match-end 2)))
	(arch (substring core (match-beginning 3) (match-end 3))))
    (setq load-path (cons (concat top "/testcenter/lib/lisp") load-path))
    (setq exec-path (cons (concat top "/" arch "/bin") exec-path))))

(require 'clipc)
(cl-edit)

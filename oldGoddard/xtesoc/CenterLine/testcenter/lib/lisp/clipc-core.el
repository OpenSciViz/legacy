;; $Id: clipc-core.el,v 1.1 1994/02/16 14:41:40 ramgopal Exp $
;; emacs lisp interface to clipc and the message server
;;
;;

(provide 'clipc-core)

(defvar clms-buffer nil "The buffer that holds message server output")

(defvar clms-proc nil "The CL environment connection process")

(defvar clipc-handler-callback-list nil
  "The list of handled messages and their callbacks")

(defvar clipc-listener-callback-list nil
  "The list of listened to messages, and their callbacks")

(defun cl-connect (&optional host port) "" 
  (interactive)
  (let ((stat (init-clipc-connection host port)))
    (message stat)
    (sit-for 2)
    stat))

(defun cl-edit (&optional host port) "" 
  (interactive)
  (let ((stat (init-clipc-connection host port)))
    (add-clipc-handler "cx_edit_location" 'cx-edit-callback)
    (message stat)
    (sit-for 2)
    stat))

(defun cl-quit () ""
  (interactive)
  (close-clipc-connection)
  (message "CLIPC connection closed"))

(defun init-clipc-connection (&optional host port prompt) 
  ""
  (let ((host (get-default-host host))
	(port (get-default-port port))
	(hp-list))

    (if (or (string= host "") (= port 0))
	(progn
	  (setq hp-list (make-ms-query))
	  (setq host (car hp-list))
	  (setq port (car (cdr hp-list)))))

    (if (and prompt (string= host ""))
	(setq host (read-string "host name? ")))
    
    (if (and prompt (or (null port) (= port 0)))
	(setq port (string-to-int (read-string
				   (format "port number? ")))))
    
    (cond ((or (or (null port) (= port 0)) (string= host ""))
	   (format "No connection made"))
	  (t (if (and (not (null clms-buffer)) (bufferp clms-buffer))
		 (kill-buffer clms-buffer))
	     (setq clms-buffer (get-buffer-create " *clms-input-buffer*"))
	     (setq clms-proc (open-network-stream "clms" clms-buffer host port))
	     (register-clipc-callbacks)
	     (format "connected to port %d on host %s" port host)))))

(defun make-ms-query () ""
  (let ((tmp-buf (get-buffer-create " *clms-tmp-buffer*"))
	(host)
	(port)
	(rval))
    (call-process "clms_query" nil tmp-buf nil "-b")
    (save-excursion
      (set-buffer tmp-buf)
      (goto-char (point-min))
      (setq rval 
	    (cond ((re-search-forward "\\([^:]*\\):\\([0-9]+\\)" 
				      (point-max) 1)
		   (list (buffer-substring (match-beginning 1)
					   (match-end 1))
			 (string-to-int (buffer-substring (match-beginning 2)
							  (match-end 2)))))
		  (t (list "" 0)))))
    (kill-buffer tmp-buf)
    rval))

(defun get-default-port (port) ""
  (let* ((env-session (getenv "CLMS_SESSION"))
	 (valid 
	  (and (not (null env-session))
	       (string-match "\\([^:]*\\):\\([0-9]+\\)" env-session)))
	 (port
	  (cond ((and (not (null port)) (not (= port 0))) port)
		((and (not (null valid)) (string-to-int 
					  (substring env-session
						     (match-beginning 2)
						     (match-end 2)))))
		(t 0))))
    (cond ((null port) 0)
	  (t port))))

(defun get-default-host (host) ""
  (let* ((env-session (getenv "CLMS_SESSION"))
	 (valid 
	  (and (not (null env-session))
	       (string-match "\\([^:]*\\):\\([0-9]+\\)" env-session)))
	 (host
	  (cond ((and (not (null host))) host)
		((and (not (null valid)) (substring env-session 
						    (match-beginning 1) 
						    (match-end 1))))
		(t ""))))
    (cond ((null host) "")
	  (t host))))

;;
;; Close down a clipc connection
;;
(defun close-clipc-connection ()
  ""
  (interactive)
  (let ((proc (get-buffer-process clms-buffer)))
    (if (not (null proc)) (delete-process proc)))
  (save-excursion
    (if (not (null clms-buffer)) (kill-buffer clms-buffer))
    (setq clms-buffer nil)
    (setq clms-proc nil)))

(defun add-clipc-handler (msgname callback) 
  "add a callback as a handler for a given message name"
  (setq clipc-handler-callback-list (cons (list msgname callback) 
					  clipc-handler-callback-list))
  (register-clipc-handler msgname))

(defun add-clipc-listener (msgname callback) 
  "add a callback as a handler for a given message name"
  (setq clipc-listener-callback-list (cons (list msgname callback) 
					  clipc-listener-callback-list))
  (register-clipc-listener msgname 1))

(defun register-clipc-handler (msgname)
  ""
  (let ((args nil))
    (setq args (clipc-dict-addstring args "mgr_type" "handler"))
    (setq args (clipc-dict-addstring args "msg_name" msgname))
    (clipc-cmscpreq-send "register" args)))

(defun register-clipc-listener (msgname type)
  ""
  (let ((args nil))
    (setq args (clipc-dict-addstring args "mgr_type" "listener"))
    (setq args (clipc-dict-addstring args "msg_name" msgname))
    (clipc-cmscpreq-send "register" args)))

    
;;
;; build up message parts
;; 
(defun clipc-dict-addval (dict val) "" (cons val dict))

(defun clipc-dict-addstring (dict name val)
 ""
 (clipc-dict-addval dict (list name val)))

(defun clipc-dict-addint (dict name val)
 ""
 (clipc-dict-addval dict (list name val)))

(setq notify-envlp '(("type" 1)))
(setq request-envlp '(("type" 2)("flags" 0)))
(setq cmscpreq-envlp '(("type" 4)("flags" 0)))

;;
;; send a notify packet
;;

(defun clipc-notify-send (name args)
  "send a clipc notification to proc"
  (clipc-msg-send name notify-envlp args))

(defun clipc-reply-send (name args envlp)
  "send a clipc notification to proc"
  (clipc-msg-send  name envlp args))

(defun clipc-cmscpreq-send (name args)
  "send a clipc cmscpreq to proc"
  (clipc-msg-send name cmscpreq-envlp args))

(defun clipc-request-send (name args)
  "send a clipc request to proc"
  (clipc-msg-send name request-envlp args))

(defun clipc-msg-send (name envlp args)
  "send a clipc notification to proc"

  (let ((msg-str (format "(M_DCT %s %s %s)" name 
			 (print-dict-string envlp)
			 (print-dict-string args))))
    (if (null clms-proc)
	nil
      (process-send-string clms-proc (format "%09d %s" 
					    (length msg-str) msg-str)))))

(defun print-msg-string (msg &optional indent) 
  ""
  (let* ((msg-name (car msg))
	 (msg-envlp (car (cdr msg)))
	 (msg-args (car (cdr (cdr msg)))))
    (format "(M_DCT %s %s %s %s)" 
	    msg-name (print-dict-string msg-envlp) 
	    (print-dict-string msg-args))))
	    
;; 
;; print out a dictionary value
;;
(defun print-dict-string (dict &optional indent)
  ""
  (rprint-dict-string dict "(T_DCT " indent))
       
(defun rprint-dict-string (dict string &optional indent)
  ""
  (let ((nv (car dict)))
    (if (not (null nv))
	(let ((name (car nv))
	      (value (car (cdr nv)))
	      (str ""))
	  (setq str (print-value value))
	  (concat string (rprint-dict-string (cdr dict) str)))
      (concat string ")"))))


(defun rprint-array-string (arr string &optional indent)
  ""
  (let ((value (car arr))
	(str ""))
    (if (null value)
	(concat string ")")
      (setq str (print-value value))
      (concat string (rprint-array-string (cdr arr) str)))))




(defun print-value (value) ""
  (cond ((stringp value) (format "(%s (T_STR %d %s))"
				 name (length value) value))
	((integerp value)  (format "(%s (T_INT %d))"
				   name value))
	((is-dict-p value) 
	 (rprint-dict-string value "(T_DCT"))
	(t (rprint-array-string value "(T_ARR"))))
  
(defun is-dict-p (v) "" 
  (or (null v) 
      (and (listp  v) (listp (car v)) (stringp (car (car v))))))

       
;;
;; set up the sentinel process
;;

(defun register-clipc-callbacks () ""
  (cond ((null clms-proc) nil)
	(t 
	 (set-process-filter clms-proc 'clipc-filter)
	 (set-process-sentinel clms-proc 'clipc-sentinel)
	 (let ((handlers clipc-handler-callback-list)
	       (listeners clipc-listener-callback-list))
	   (while (not (null handlers))
	     (register-clipc-handler (car (car handlers)))
	     (setq handlers (cdr handlers)))
	   
	   (while (not (null listeners))
	     (register-clipc-listener (car (car listeners)) 1)
	     (setq listeners (cdr listeners)))))))


(defun clipc-sentinel (proc change)
  ""
  (close-clipc-connection))

(defun clipc-filter (proc output)
  ""
  (let ((msg nil))
    (set-buffer clms-buffer)
    (goto-char (point-max))
    (insert output)
    (while (setq msg (parse-clms-buffer))
      (msg-callback msg))))

;;
;; private callback functions that should be in another file
;;

(defun msg-callback (msg) ""
  (let ((msg-name (car msg))
	(msg-envlp (car (cdr msg)))
	(handlers clipc-handler-callback-list)
	(listeners clipc-listener-callback-list))
    (cond 
     ((null msg-envlp) nil)
     ((= (clipc-dict-getval msg-envlp "type") 1)
      (while (and (not (null listeners))
		  (not (string= msg-name (car (car listeners)))))
	(setq listeners (cdr listeners)))
      (if (not (null listeners))
	  (eval (list (car (cdr (car listeners))) msg-name 'msg))))
     
     ((= (clipc-dict-getval msg-envlp "type") 2)
      (while (and (not (null handlers))
		  (not (string= msg-name (car (car handlers)))))
	(setq handlers (cdr handlers)))
      (if (not (null handlers))
	  (eval (list (car (cdr (car handlers))) msg-name 'msg))))
     (t nil))))

;;	


(defun clipc-msg-getval (msg field) ""
  (let ((args (car (cdr (cdr msg)))))
    (clipc-dict-getval args field)))

(defun clipc-dict-getval (dict field) ""
  (car (cdr (assoc field dict))))

(defun make-reply-envlp (msg) ""
  (let ((envlp (car (cdr msg)))
	(refid 0))
    (setq refid (clipc-dict-getval envlp "msg_id"))
    (list (list "ref_id" refid) '("type" 1) '("flags" 16))))

(defun parse-clms-buffer () ""
    (let ((rval nil)
	  (size))
      (save-excursion
	(set-buffer clms-buffer)
	(goto-char 1)
	(if (null (re-search-forward "\\([0-9]+\\) " (point-max) t))
	    nil
	  (setq size (+ (string-to-int (buffer-substring (match-beginning 1)
							 (match-end 1)))
			(match-end 0)))
	  (if (> size (point-max))
	      nil
	    (setq rval (parse-clipc-region 1 size))))
	rval)))

;;
;; parser for CLIPC messages in a buffer
;; 
;; parse the region in the buffer
(defun parse-clipc-region (start end)
  "" 
  (let ((envlp nil)
	(body nil)
	(name "")
	(msglen 0))
    (goto-char start)
    (re-search-forward "\\([0-9]+\\) " end 1)
    (catch 'end
      (progn
	(if (not (< (point) end)) (throw 'end 1))
	(goto-char (match-beginning 1))
	(setq msglen (string-to-int (buffer-substring 
				     (point) (match-end 1))))
	(goto-char (match-end 0))	
	(if (> (+ (point) msglen) end) (throw 'end 2))

	(setq end (+ (point) msglen))
	(if (not (looking-at "(M_DCT \\([a-zA-Z_]+\\)[ 	]*"))
	    (throw 'end 3))
	(setq name (buffer-substring (match-beginning 1) (match-end 1)))
	(goto-char (match-end 0))

	(setq envlp (parse-dict))
	(setq body  (parse-dict))))
    (delete-region start end)
    (list name envlp body)))

;; parse a dictionary starting at (point)
;;
(defun parse-dict () ""
  (let ((rval nil))
    (cond ((looking-at "[ 	]*(T_DCT[ 	]+")
	   (goto-char (match-end 0))
	   (while (looking-at "\\([ 	]*\\)(")
	     (goto-char (match-end 1))
	     (setq rval (cons (parse-dict-ent) rval)))
	   (re-search-forward ")" (point-max) 1)
	   (goto-char (match-end 0)))
	  (t nil))
    rval))

;; parse an array of values starting at (point)
;;
(defun parse-array () ""
  (let ((rval nil))
    (cond ((looking-at "[ 	]*(T_ARR[ 	]+")
	   (goto-char (match-end 0))
	   (while (looking-at "\\([ 	]*\\)(")
	     (goto-char (match-end 1))
	     (setq rval (append rval (list (parse-val)))))
	   (re-search-forward ")" (point-max) 1)
	   (goto-char (match-end 0)))
	  (t nil))
    rval))

;;
;; parse a name-value pair in a dictionary
;;
(defun parse-dict-ent () ""
  (let ((rval nil))
    (cond ((looking-at "[ 	]*(\\([a-zA-Z_]+\\)[ 	]*")
	   (goto-char (match-end 0))
	   (setq rval (list (buffer-substring (match-beginning 1)
					      (match-end 1))
			    (parse-val)))
	   (re-search-forward ")" (point-max) 1)
	   (goto-char (match-end 0)))
	  (t nil))
    rval))

;;
;; parse a generic value
;;
(defun parse-val () ""
  (cond ((looking-at "[ 	]*(T_STR[ 	]+\\([0-9]+\\) ")
	 (let* ((nchars (string-to-int (buffer-substring (match-beginning 1)
							 (match-end 1))))
		(end (+ (match-end 0) nchars))
		(string (buffer-substring (match-end 0) end)))
	   
	   (goto-char end)
	   (while (not (looking-at ")")) (forward-char 1))
	   (forward-char 1)
	   string))

	((looking-at "[ 	]*(T_MEM[ 	]+\\([0-9]+\\) ")
	 (let* ((nchars (* 2 (string-to-int (buffer-substring
					     (match-beginning 1)
					     (match-end 1)))))
		(end (+ (match-end 0) nchars))
		(rval (string-to-hexint (buffer-substring (match-end 0) end))))
	   
	   (goto-char end)
	   (while (not (looking-at ")")) (forward-char 1))
	   (forward-char 1)
	   rval))

	((looking-at "[ 	]*(T_INT[ 	]+\\([0-9]+\\)[^)]*)")
	 (goto-char (match-end 0))
	 (string-to-int (buffer-substring (match-beginning 1)
					  (match-end 1))))

	((looking-at "[ 	]*(T_DCT")
	 (parse-dict))
	((looking-at "[ 	]*(T_ARR")
	 (parse-array))

	(t nil)))

;;
;; callback functions
;;

;;
;; called when an editing request comes in
;;

(defun cx-edit-callback (msg-name msg)
  (let ((curbuf (current-buffer))
	(place (make-marker))
	(reply-envlp (make-reply-envlp msg))
	(reply-args nil)
	(file (clipc-msg-getval msg "filename")) 
	(lineno (clipc-msg-getval msg "linenum")))
    (setq reply-args (list '("status" 1)
			   (list "linenum" lineno) 
			   (list "filename" file)))
    (clipc-reply-send msg-name reply-args reply-envlp)
    (set-marker place (point) curbuf)
    (find-file file)
    (goto-line lineno)))

;;
;; error browser hacks
;;

(defun compile-error-callback (msg-name msg)
  (let* ((buffer (get-buffer-create "*cx-error*"))
	 (window (display-buffer buffer nil))
	 (file (clipc-msg-getval msg "filePath")) 
	 (line (clipc-msg-getval msg "fileLine"))
	 (text (clipc-msg-getval msg "msgText"))
	 (pos))
    (save-excursion
      (set-buffer buffer )
      (goto-char (point-max))
      (insert (format "%s:%d: %s\n" file line text)))))

(defun stop-loc-callback (msg-name msg)
  (let ((file (clipc-msg-getval msg "pathName")) 
	(line (clipc-msg-getval msg "lineNumber")))
    (cx-display-line file line)))

(defun cxcmd-display-callback (msg-name msg)
  ""
  (let* ((buffer (get-buffer-create "*cx-display*"))
	 (window (display-buffer buffer nil))
	 (text (clipc-msg-getval msg "msgText"))
	 (pos))
    (save-excursion
      (set-buffer buffer )
      (goto-char (point-max))
      (insert (format "\n%s:\n%s\n" msg-name (print-msg-string msg 4))))))


;; straight from gdb.el
(defun cx-display-line (true-file line)
  (let* ((buffer (find-file-noselect true-file))
	 (window (display-buffer buffer nil))
	 (pos))
    (save-excursion
      (set-buffer buffer)
      (save-restriction
	(widen)
	(goto-line line)
	(setq pos (point))
	(setq overlay-arrow-string "=>")
	(or overlay-arrow-position
	    (setq overlay-arrow-position (make-marker)))
	(set-marker overlay-arrow-position (point) (current-buffer)))
      (cond ((or (< pos (point-min)) (> pos (point-max)))
	     (widen)
	     (goto-char pos))))
    (set-window-point window overlay-arrow-position)))

;; Return a default tag to search for, based on the text at point.
(defun find-tag-default ()
  (save-excursion
    (while (looking-at "\\sw\\|\\s_")
      (forward-char 1))
    (if (re-search-backward "\\sw\\|\\s_" nil t)
	(progn (forward-char 1)
	       (buffer-substring (point)
				 (progn (forward-sexp -1)
					(while (looking-at "\\s'")
					  (forward-char 1))
					(point))))
      nil)))

(defun find-tag-tag ()
  ""
  (let* ((default (find-tag-default))
	 (spec (read-string
		(format "display (default %s) " default))))
    (if (equal spec "")
	default
      spec)))





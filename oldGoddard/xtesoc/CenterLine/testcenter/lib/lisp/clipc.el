;; emacs lisp interface to clipc and the message server
;;

(require 'clipc-core)
(provide 'clipc)

(global-set-key "\C-c\C-b" 'cl-build)
(global-set-key "\C-c\C-d" 'cl-display-point)
(global-set-key "\C-c\C-l" 'cl-load-file)
(global-set-key "\C-c\C-n" 'cl-next)
(global-set-key "\C-c\C-r" 'cl-run)
(global-set-key "\C-c\C-x" 'cl-gen-cmd)
(global-set-key "\C-x "    'cl-stop-loc)

(defun cl-next () "" (interactive) (send-clipc-cmd "next"))
(defun cl-run () ""(interactive) (send-clipc-cmd "run"))
(defun cl-build () "" (interactive)(send-clipc-cmd "build"))

(defun cl-setup () ""
  (interactive)
  (add-clipc-handler "cx_edit_location" 'cx-edit-callback)
  (add-clipc-listener "cx_list_location" 'cx-list-callback)
  (add-clipc-listener "mgr_list"         'cx-list-callback)
  (add-clipc-listener "compile_error"    'compile-error-callback)
  (add-clipc-listener "cxcmd_display"    'cxcmd-display-callback)
  (add-clipc-listener "cxcmd_xref"       'cxcmd-xref-callback)
  (add-clipc-listener "event_stop_loc"   'stop-loc-callback)
  (add-clipc-listener "event_status"   'event-status-callback))


(defun cl-gen-cmd (cmd)
  "prompt for and send a generic CX command"
  (interactive "sCX command: ")
  (send-clipc-cmd cmd))

(defun cl-stop-loc ()
  "Set CX breakpoint at this source line."
  (interactive)
  (let ((file-name buffer-file-name)
	(line (save-restriction
		(widen)
		(1+ (count-lines 1 (point))))))
    (clipc-request-send "cxcmd_stop" (list (list "lineNumber" line)
					   (list "pathName" file-name)))))

(defun cl-display-point ()
  "Set CX display on variable at point"
  (interactive)
  (let ((disp (find-tag-tag)))
    (send-clipc-cmd (format "display %s" disp))))

(defun cl-load-file ()
  "Load the current file into CenterLine"
  (interactive)
  (let ((file-name buffer-file-name))
    (send-clipc-cmd (format "load %s" file-name))))



;;
;; error browser hacks
;;

(defun compile-error-callback (msg-name msg)
  (let* ((buffer (get-buffer-create "*cx-error*"))
	 (window (display-buffer buffer nil))
	 (file (clipc-msg-getval msg "filePath")) 
	 (line (clipc-msg-getval msg "fileLine"))
	 (text (clipc-msg-getval msg "msgText"))
	 (pos))
    (save-excursion
      (set-buffer buffer )
      (goto-char (point-max))
      (insert (format "%s:%d: %s\n" file line text)))))

(defun stop-loc-callback (msg-name msg)
  (let ((file (clipc-msg-getval msg "pathName")) 
	(line (clipc-msg-getval msg "lineNumber"))
	(func (clipc-msg-getval msg "funcName")))
    (or (string= file "workspace") (cx-display-line file line))))

(defun cxcmd-display-callback (msg-name msg)
  ""
  (let* ((number     (clipc-msg-getval msg "dispNumber"))
	 (expr       (clipc-msg-getval msg "dispExpr"))
	 (expr-value (clipc-dict-getval expr "exprValue"))
	 (expr-name  (clipc-dict-getval expr "exprName"))
	 (expr-type  (clipc-dict-getval expr "exprType"))
	 (expr-lval  (clipc-dict-getval expr "exprLval"))
	 (name       (clipc-dict-getval expr-name "simpleName"))
	 (display-string (format "%s = %s\n" name 
				 (make-typeval-string expr-type expr-value
						      (+ (length name) 3)))))
    (add-display-string number display-string)))
    
(defun make-typeval-string (type value indent) ""
  (let* ((type-before (clipc-dict-getval type "typeBefore"))
	 (type-after  (clipc-dict-getval type "typeAfter"))
	 (value-string (make-value-string value)))
    (format "(%s%s) %s" type-before type-after value-string)))

(defun event-status-callback (msg-name msg)
  ""
  (let* ((op (clipc-msg-getval msg "operation"))
	 (status-item (clipc-msg-getval msg "statusItem"))
	 (item-number (clipc-dict-getval status-item "itemNumber"))
	 (item-type   (clipc-dict-getval status-item "itemType")))
    (cond ((string= item-type "display")
	   (cond ((string= op "delete")
		  (delete-display-item item-number))
		 (t nil)))
	  (t nil))))

(defvar display-list nil "List of displayed items (n mark size)")

(defun delete-display-item (n)
  ""
  (let* ((buffer (get-buffer-create "*cx-display*"))
	 (window (display-buffer buffer nil))
	 (prev-display (assoc n display-list))
	 (new-list nil)
	 (old-loc (assoc n display-list))
	 (m (car (cdr old-loc)))
	 (size (car (cdr (cdr old-loc)))))
    (if (null old-loc)
	nil
      (while (not (null display-list))
	(setq new-list (if (or (null (car (car display-list)))
			       (= (car (car display-list)) n))
			   new-list
			 (append new-list (list (car display-list)))))
	(setq display-list (cdr display-list)))
      (setq display-list new-list)
      
      (save-excursion
	(set-buffer buffer)
	(delete-region (marker-position m)
		       (+ (marker-position m) size))
	(set-marker m nil)))))
  
(defun add-display-string (n s) 
  "add display string S numbered N" 
  (let* ((buffer (get-buffer-create "*cx-display*"))
	 (window (display-buffer buffer nil))
	 (prev-display (assoc n display-list))
	 (m (car (cdr prev-display)))
	 (len (car (cdr (cdr prev-display))))
	 (start))
    (save-excursion
      (set-buffer buffer)
      (if (null prev-display)
	  (progn
	    (goto-char (point-max))
	    (setq start (point))
	    (insert-before-markers (format "(%d) %s\n" n s))
	    (setq m (make-marker))
	    (set-marker m start)
	    (setq prev-display (list n m (- (point) start)))
	    (setq display-list (cons prev-display display-list)))
	(setq start (marker-position m))
	(goto-char start)
	(insert (format "(%d) %s" n s))
	(delete-region (point) (+ (point) len))

	(rplacd (cdr prev-display) (list (- (point) start)))
	(set-marker m start)))))


	

(defun make-value-string (val) ""
  (let* ((disc-name  (clipc-dict-getval val "valueInfoKey"))
	 (disc-value (clipc-dict-getval val disc-name)))
    (cond ((string= disc-name "simpleValue")
	   (format "%d" (clipc-dict-getval disc-value "value")))
	  ((string= disc-name "pointerValue")
	   (let ((string (clipc-dict-getval disc-value "stringValue"))
		 (is-valid (clipc-dict-getval disc-value "isValid")))
	     (if (and (stringp string) (not (null is-valid)))
		 (format "0x%x \"%s\""
			 (clipc-dict-getval disc-value "pointerValue") string)
	       (format "0x%x" (clipc-dict-getval disc-value "pointerValue")))))
	  (t (format "<%s>" disc-name)))))

(defun cxcmd-xref-callback (msg-name msg)
  ""
  (let* ((buffer (get-buffer-create "*cx-xref*"))
	 (window (display-buffer buffer nil))
	 (symbol (clipc-msg-getval msg "referenceSymbol"))
	 (to-list (clipc-msg-getval msg "referencesTo"))
	 (from-list (clipc-msg-getval msg "referencesFrom")))
    (save-excursion
      (set-buffer buffer )
      (goto-char (point-max))
      (insert (format "Symbol %s:\n%s\n%s\n"
		      symbol
		      (print-ref-list from-list "Referenced by:\n")
		      (print-ref-list to-list "References to:\n"))))))

(defun print-ref-list (ref-list string) ""
  (if (null ref-list)
      string
    (concat string (print-ref (car ref-list)) 
	    (print-ref-list (cdr ref-list) ""))))
  
(defun print-ref (ref) ""
  (let* ((type (clipc-dict-getval ref "symbolType"))
	 (name (clipc-dict-getval ref "symbolName"))
	 (unDef (clipc-dict-getval ref "isUndef"))
	 (unRes (clipc-dict-getval ref "isUnres"))
	 (type-before (clipc-dict-getval type "typeBefore"))
	 (type-after (clipc-dict-getval type "typeAfter")))
    (if (and (stringp type-before)
	     (not (string= "*" (substring type-before -1 nil))))
	(setq type-before (concat type-before " ")))
    (concat "\t" (if (null type-before) "<extern>" type-before)
	    name
	    (if (null type-after) "" type-after)
	    (if (not (null unDef)) " (undefined)")
	    (if (not (null unRes)) " (unresolved)")
	    "\n")))

(defun send-clipc-cmd (cmd) ""
  (clipc-request-send "cxcmd_exec" (list (list "commandLine" cmd))))

;; straight from gdb.el
(defun cx-display-line (true-file line)
  (let* ((buffer (and true-file (find-file-noselect true-file)))
	 (window (and buffer (display-buffer buffer nil)))
	 (pos))
    (save-excursion
      (set-buffer buffer)
      (save-restriction
	(widen)
	(goto-line line)
	(setq pos (point))
	(setq overlay-arrow-string "=>")
	(or overlay-arrow-position
	    (setq overlay-arrow-position (make-marker)))
	(set-marker overlay-arrow-position (point) (current-buffer)))
      (cond ((or (< pos (point-min)) (> pos (point-max)))
	     (widen)
	     (goto-char pos))))
    (set-window-point window overlay-arrow-position)))

;; Return a default tag to search for, based on the text at point.
(defun find-tag-default ()
  (save-excursion
    (while (looking-at "\\sw\\|\\s_")
      (forward-char 1))
    (if (re-search-backward "\\sw\\|\\s_" nil t)
	(progn (forward-char 1)
	       (buffer-substring (point)
				 (progn (forward-sexp -1)
					(while (looking-at "\\s'")
					  (forward-char 1))
					(point))))
      nil)))

(defun find-tag-tag ()
  ""
  (let* ((default (find-tag-default))
	 (spec (read-string
		(format "display (default %s) " default))))
    (if (equal spec "")
	default
      spec)))


;; parse a hex string
(defun string-to-hexint (string) ""
  (let ((char)
	(zero (string-to-char "0"))
	(nine (string-to-char "9"))
	(small-a (string-to-char "a"))
	(small-f (string-to-char "f"))
	(big-a (string-to-char "A"))
	(big-f (string-to-char "F"))
	(rval 0))
    (catch 'non-digit
      (while (and (not (null string)) (not (string= string "")))
	(setq char (string-to-char string))
	(setq string (substring string 1 nil))
	(setq rval (+ (* 16 rval)
		      (cond ((and (>= char zero) (<= char nine))
			     (- char zero))
			    ((and (>= char small-a) (<= char small-f))
			     (+ 10 (- char small-a)))
			    ((and (>= char big-a) (<= char big-f))
			     (+ 10 (- char big-a)))
			    (t (throw 'non-digit rval))))))
      rval)))

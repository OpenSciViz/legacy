
/*
 * add a typedef capability to CLIPC types
 */
#if !defined(TRUE)
#define TRUE 1
#define FALSE 0
#endif

typedef struct clipc_typedef *CLIPC_TYPEDEF;
typedef struct clipc_msgdef *CLIPC_MSGDEF;

enum clipc_typedef_type {
    CLIPC_TYPEDEF_NIL,    
    CLIPC_TYPEDEF_BASIC,
    CLIPC_TYPEDEF_DICT,
    CLIPC_TYPEDEF_ARRAY,
    CLIPC_TYPEDEF_ENUM,
    CLIPC_TYPEDEF_DISC
};
typedef enum clipc_typedef_type CLIPC_TYPEDEF_TYPE;

struct clipc_typedef {
    char *typedef_name;
    CLIPC_TYPEDEF_TYPE typedef_type;
    int refcount;
    CLIPC_TYPEDEF next;
    union {
	CLIPC_TYPE basic_type;
	CLIPC_TYPEDEF array_elmt;
	struct clipc_dict_fldlist *dict_fields;
	struct clipc_enum_list *enum_list;
	struct clipc_disc_union {
	    char *disc_name;
	    struct clipc_dict_fldlist *disc_fields; 
	} disc;
    } un;
};

struct clipc_enum_list {
    char *name;
    int value;
    int is_explicit;
    struct clipc_enum_list *next;
};

struct clipc_dict_fldlist {
    char *keyname;
    CLIPC_TYPEDEF def;
    int is_opt;
    struct clipc_dict_fldlist *next;
};

struct clipc_msgdef {
    char *msg_name;
    int refcount;
    CLIPC_TYPEDEF request_def;
    CLIPC_TYPEDEF notify_def;
    CLIPC_MSGDEF next;
};

#!/bin/sh

PATH=/usr/ucb:/bin:/usr/bin
/bin/sync; /bin/sync

touch version

date=`date +"%m/%d/%y @ %H:%M:%S"`
host=`uname -n`
dir=`pwd`
version=`cat version`
if [ "x$version" = "x" ]; then
    version=1
fi

version=`expr $version + 1`

echo $version > version

echo "char Build_string[] = \"Build #$version $host:$dir $date\";" 

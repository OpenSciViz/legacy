#include <stdio.h>

extern int cltype_parse_verbose;
extern int cltype_msg_names;
extern int cltype_lex_only;
extern do_parse_doc_directives;
extern do_nest_appl_svc;
extern int yydebug;

main(int argc, char *argv[])
{
    int rval = 0;
    int print_messages = 0;
    int print_typedefs = 0;    
    extern char *optarg;
    extern int optind, opterr;
    int c;
    int i;

    /* Parse only- no need to check for doc header syntax. */
    do_parse_doc_directives = 0;
    do_nest_appl_svc = 0;
    
    /*
     * open up a socket
     * print out the port number
     * wait for messages
     */

    while ((c = getopt(argc, argv, "dlmtvn")) != -1) {
	switch (c) {
	case 'd':
	    yydebug = 1;
	    break;
	    
	case 'l':
	    cltype_lex_only = 1;
	    break;
	case 'm':
	    print_messages = 1;
	    break;
	    
	case 't':
	    print_typedefs = 1;
	    break;

	case 'v':
	    cltype_parse_verbose = 1;
	    break;

	case 'n':
	    cltype_msg_names = 1;
	    break;
	} /* switch */
    } /* while */
    argc -= optind;
    argv += optind;

    if (argc == 0) {
	rval = cltype_parse_file(0);
    }
    else {
	for (i = 0; i < argc; i++) {
	    rval += cltype_parse_file(argv[i]);
	}
    }
    if (print_typedefs)
      cltype_print_typdef_list(stdout);
    if (print_messages)
      cltype_print_msgdef_list(stdout);
    
    exit(rval);
}


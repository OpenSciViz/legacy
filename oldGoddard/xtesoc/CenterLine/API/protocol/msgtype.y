%{
#include <stdio.h>
#include <stdlib.h>    
#include "cltype.h"
#include "docdir.h"

char *clipc_strdup();    
    
#define YYDEBUG 1
#if defined(NeXT)
#define strdup(s) clipc_strdup(0, (s))
#endif

#if defined(SYSV)
#  define bcopy(A,B,L) memcpy((B),(A),(L))
#endif /* SYSV */

extern cltype_parse_verbose;
extern cltype_msg_names;
extern do_parse_doc_directives;
extern do_nest_appl_svc;

static char *input_filename;
static int input_linenum;

%}

%union {
    int intval;
    char *strval;
    CLTYPE_MSGDEF    msgdef;
    CLTYPE_TYPDEF    typdef;
    CLTYPE_MTYPENT   mtypent;
    CLTYPE_DICTENT   dictent;
    CLTYPE_ENUMENT   enument;
}

%token MSGNAME NAME INT T_DCT T_ARY T_ENU T_DSC DESC
%token APPL_DOCTOK END_APPL_DOCTOK
%token SECTION_DOCTOK END_SECTION_DOCTOK MSGDEF_DOCTOK END_MSGDEF_DOCTOK
%token DEFDEF_DOCTOK END_DEFDEF_DOCTOK DESCRIPTION_DOCTOK PROTOCOL_DOCTOK
%token SIDE_EFFECT_MSGS_DOCTOK GENERIC_DOCTOK
%token MAKES_REQUESTS_DOCTOK LISTENS_FOR_DOCTOK

%start start

%%

start : appl_doc_list
       {
	   marker_fixup_refs();
	   docgen_close();
       }

      | def_list {}
      ;

appl_doc_list : appl_doc {}
              | appl_doc appl_doc_list {}
              ;

appl_doc : appl_start appl_cont sections appl_end {}
         | appl_start appl_cont appl_end {}
         ;

appl_start : APPL_DOCTOK
             { 
		 enum DDTYPE current_scope;

		 current_scope = getset_ddstack(DDSTACK_OP_NOOP, DDTYPE_NONE);
		 if ((DDTYPE_NONE != current_scope) &&
		     (DDTYPE_APPLICATION_SERVICE != current_scope)) {
		     char buf[80];

		     sprintf(buf, "Attempting to start %s before %s end.",
			     ddtype_string[DDTYPE_APPLICATION_SERVICE],
			     ddtype_string[current_scope]);
		     yyerror(buf);
		 }

		 (void)getset_ddstack(DDSTACK_OP_PUSH,
				      DDTYPE_APPLICATION_SERVICE);
		 tbuf_clear(APPL_BUFFER);
		 tbuf_clear(DESC_BUFFER);
		 tbuf_clear(PROTO_BUFFER);
		 tbuf_clear(REQUESTS_BUFFER);
		 tbuf_clear(LISTENS_BUFFER);
	     }
           ;

appl_cont : msg_desc other_msg_doc_list
           {
	       tbuf_set(APPL_BUFFER,  $<strval>1);
	       docgen_generate_appl_svc();
	   }
          ;

appl_end : END_APPL_DOCTOK
             {
	     enum DDTYPE current_scope;

	     current_scope = getset_ddstack(DDSTACK_OP_NOOP, DDTYPE_NONE);
	     if (DDTYPE_APPLICATION_SERVICE != current_scope) {
		 char buf[80];

		 sprintf(buf, "Attempting to end %s before %s end.",
			 ddtype_string[DDTYPE_APPLICATION_SERVICE],
			 ddtype_string[current_scope]);
		 yyerror(buf);
	     }

	     (void)getset_ddstack(DDSTACK_OP_POP, DDTYPE_NONE);
	     }
         ;

sections : defs_doc_list {}
         | section_doc_list {}
         ;

section_doc_list : section_doc {}
                 | section_doc section_doc_list {}
                 ;

section_doc : section_start section_cont defs_doc_list section_end {}
            | section_start section_cont section_doc_list section_end {}
            ;

section_start : SECTION_DOCTOK
                {
		    enum DDTYPE current_scope;

		    current_scope = getset_ddstack(DDSTACK_OP_NOOP,
						   DDTYPE_NONE);
		    if ((DDTYPE_NONE != current_scope) &&
			(DDTYPE_APPLICATION_SERVICE != current_scope) &&
			(DDTYPE_SECTION != current_scope)) {
			char buf[80];

			sprintf(buf, "Attempting to start %s before %s end.",
				ddtype_string[DDTYPE_SECTION],
				ddtype_string[current_scope]);
			yyerror(buf);
		    }

		    (void)getset_ddstack(DDSTACK_OP_PUSH, DDTYPE_SECTION);
		    tbuf_clear(SECT_BUFFER);
		    tbuf_clear(DESC_BUFFER);
		    tbuf_clear(PROTO_BUFFER);
	        }
              ;

section_cont : msg_desc other_msg_doc_list
                {
		    tbuf_set(SECT_BUFFER, $<strval>1);
		    docgen_generate_section();
		}
              ;

section_end : END_SECTION_DOCTOK
              {
		  enum DDTYPE current_scope;

		  current_scope = getset_ddstack(DDSTACK_OP_NOOP, DDTYPE_NONE);
		  if (DDTYPE_SECTION != current_scope) {
		      char buf[80];

		      sprintf(buf, "Attempting to end %s before %s end.",
			      ddtype_string[DDTYPE_SECTION],
			      ddtype_string[current_scope]);
		      yyerror(buf);
		  }

		  (void)getset_ddstack(DDSTACK_OP_POP, DDTYPE_NONE);
	      }
            ;

defs_doc_list : defs_doc {}
              | defs_doc defs_doc_list {}
              ;

defs_doc : msgdef_doc {}
         | defdef_doc {}
         ;

msgdef_doc : msgdef_start other_msg_doc_list mdef_no_desc msgdef_end
            {
	    cltype_msgdef_add($<msgdef>3);
	    docgen_finish_message();
	    }
           | msgdef_start other_msg_doc_list mdef_no_desc tdef_list
             msgdef_end
            {
	    cltype_msgdef_add($<msgdef>3);
	    docgen_finish_message();
	    }
           | mdef_no_desc
            {
	    cltype_msgdef_add($<msgdef>1);
	    docgen_finish_message();
	    }
           ;

msgdef_start : MSGDEF_DOCTOK msg_desc
                {
		    enum DDTYPE current_scope;

		    current_scope = getset_ddstack(DDSTACK_OP_NOOP,
						   DDTYPE_NONE);
		    if ((DDTYPE_NONE != current_scope) &&
			(DDTYPE_APPLICATION_SERVICE != current_scope) &&
			(DDTYPE_SECTION != current_scope)) {
			char buf[80];

			sprintf(buf, "Attempting to start %s before %s end.",
				ddtype_string[DDTYPE_MSG_DEF],
				ddtype_string[current_scope]);
			yyerror(buf);
		    }

		    (void)getset_ddstack(DDSTACK_OP_PUSH, DDTYPE_MSG_DEF);
		    tbuf_clear(MSG_BUFFER);
		    tbuf_clear(DESC_BUFFER);
		    tbuf_clear(PROTO_BUFFER);
		    tbuf_clear(SIDE_BUFFER);
		    tbuf_set(MSG_BUFFER, $<strval>2);

		    reflist_init();
	        }
             ;

msgdef_end : END_MSGDEF_DOCTOK
              {
		  enum DDTYPE current_scope;
		  char *ref;

		  current_scope = getset_ddstack(DDSTACK_OP_NOOP, DDTYPE_NONE);
		  if (DDTYPE_MSG_DEF != current_scope) {
		      char buf[80];

		      sprintf(buf, "Attempting to end %s before %s end.",
			      ddtype_string[DDTYPE_MSG_DEF],
			      ddtype_string[current_scope]);
		      yyerror(buf);
		  }

		  (void)getset_ddstack(DDSTACK_OP_POP, DDTYPE_NONE);
	      }
           ;

other_msg_doc_list : /* empty */ {}
                   | other_msg_docs {}
                   ;

other_msg_docs : other_msg_doc {}
               | other_msg_doc other_msg_docs {}
               ;

other_msg_doc : DESCRIPTION_DOCTOK msg_desc
               {
		   tbuf_clear(DESC_BUFFER);
		   tbuf_set(DESC_BUFFER, $<strval>2);
               }

              | MAKES_REQUESTS_DOCTOK msg_desc
               {
		   tbuf_clear(REQUESTS_BUFFER);
		   tbuf_set(REQUESTS_BUFFER, $<strval>2);
               }

              | LISTENS_FOR_DOCTOK msg_desc
               {
		   tbuf_clear(LISTENS_BUFFER);
		   tbuf_set(LISTENS_BUFFER, $<strval>2);
               }

              | PROTOCOL_DOCTOK msg_desc
               {
		   tbuf_clear(PROTO_BUFFER);
		   tbuf_set(PROTO_BUFFER, $<strval>2);
               }

              | SIDE_EFFECT_MSGS_DOCTOK msg_desc
               {
		   tbuf_clear(SIDE_BUFFER);
		   tbuf_set(SIDE_BUFFER, $<strval>2);
               }

              | GENERIC_DOCTOK msg_desc {}
              ;

tdef_list : tdef_no_desc
           {
            cltype_typdef_add($<typdef>1); 
	    /* This is a local defdef.  Remove it from the reflist. */
	    reflist_remove_ref(cltype_typdef_getname($<typdef>1));
	   }
          | tdef_no_desc tdef_list
           {
	    /* This is a local defdef.  Remove it from the reflist. */
            cltype_typdef_add($<typdef>1); 
	    reflist_remove_ref(cltype_typdef_getname($<typdef>1));
	   }
          ;

defdef_doc : defdef_start other_def_doc_list tdef_no_desc defdef_end
            {
             cltype_typdef_add($<typdef>3); 
	     docgen_finish_def();
	    }
           | defdef_start other_def_doc_list tdef_no_desc tdef_list
             defdef_end
            {
             cltype_typdef_add($<typdef>3); 
	     docgen_finish_def();
	    }
           ;

defdef_start : DEFDEF_DOCTOK msg_desc
                {
		    enum DDTYPE current_scope;

		    current_scope = getset_ddstack(DDSTACK_OP_NOOP,
						   DDTYPE_NONE);
		    if ((DDTYPE_NONE != current_scope) &&
			(DDTYPE_APPLICATION_SERVICE != current_scope) &&
			(DDTYPE_SECTION != current_scope)) {
			char buf[80];

			sprintf(buf, "Attempting to start %s before %s end.",
				ddtype_string[DDTYPE_DEF_DEF],
				ddtype_string[current_scope]);
			yyerror(buf);
		    }

		    (void)getset_ddstack(DDSTACK_OP_PUSH, DDTYPE_DEF_DEF);
		    tbuf_clear(DEF_BUFFER);
		    tbuf_clear(DESC_BUFFER);
		    tbuf_clear(PROTO_BUFFER);
		    tbuf_clear(SIDE_BUFFER);
		    tbuf_set(DEF_BUFFER, $<strval>2);
		    docgen_reset_defcount();
		    reflist_init();
	        }
             ;

defdef_end : END_DEFDEF_DOCTOK
              {
		  enum DDTYPE current_scope;
		  char *ref;

		  current_scope = getset_ddstack(DDSTACK_OP_NOOP, DDTYPE_NONE);
		  if (DDTYPE_DEF_DEF != current_scope) {
		      char buf[80];

		      sprintf(buf, "Attempting to end %s before %s end.",
			      ddtype_string[DDTYPE_DEF_DEF],
			      ddtype_string[current_scope]);
		      yyerror(buf);
		  }

		  (void)getset_ddstack(DDSTACK_OP_POP, DDTYPE_NONE);
	      }
           ;

other_def_doc_list : /* empty */ {}
                   | other_def_docs {}
                   ;

other_def_docs : other_def_doc {}
               | other_def_doc other_def_docs {}
               ;

other_def_doc : DESCRIPTION_DOCTOK msg_desc
               {
		   tbuf_clear(DESC_BUFFER);
		   tbuf_set(DESC_BUFFER, $<strval>2);
	       }

              | PROTOCOL_DOCTOK msg_desc
               {
		   tbuf_clear(PROTO_BUFFER);
		   tbuf_set(PROTO_BUFFER, $<strval>2);
               }

              | GENERIC_DOCTOK msg_desc {}
              ;

def_list : msg_desc {}
         | mdef def_list  { cltype_msgdef_add($<msgdef>1); }
         | tdef def_list  { cltype_typdef_add($<typdef>1); }
         | error def_list 
         ;

mdef   : msg_desc MSGNAME msgtype_list
        {
	    if (cltype_parse_verbose)
	      printf("read message '%s'\n", $<strval>2);
	    if (cltype_msg_names)
	      printf("\"%s\"\n", $<strval>2);
	    $<msgdef>$ = cltype_msgdef_alloc($<strval>2, $<mtypent>3,
					     $<strval>1);
	}
       | msg_desc NAME msgtype_list    
        {
	    if (cltype_parse_verbose)
	      printf("read message '%s'\n", $<strval>2);
	    if (cltype_msg_names)
	      printf("\"%s\"\n", $<strval>2);
	    $<msgdef>$ = cltype_msgdef_alloc($<strval>2, $<mtypent>3,
					     $<strval>1); 
	}
       ;

mdef_no_desc   : MSGNAME msgtype_list
                 {
		     if (cltype_parse_verbose)
			 printf("read message '%s'\n", $<strval>1);
		     if (cltype_msg_names)
			 printf("\"%s\"\n", $<strval>2);
		     $<msgdef>$ = cltype_msgdef_alloc($<strval>1, $<mtypent>2,
						      NULL);
		     docgen_generate_message($<msgdef>$);
	         }
               | NAME msgtype_list    
                 {
		     if (cltype_parse_verbose)
			 printf("read message '%s'\n", $<strval>1);
		     if (cltype_msg_names)
			 printf("\"%s\"\n", $<strval>2);
		     $<msgdef>$ = cltype_msgdef_alloc($<strval>1, $<mtypent>2,
						      NULL); 
		     docgen_generate_message($<msgdef>$);
	         }
               ;

msg_desc : /* empty */ {$<strval>$ = 0;}
         | msg_desc DESC
            {
		/* not the best string handling routine around, but the
		 * inputs will be relatively short.
		 */
		if ($<strval>1 == 0) {
		    $<strval>$ = $<strval>2;
		}
		else {
		    char *tmp;
		    tmp = malloc(strlen($<strval>1) + strlen($<strval>2) + 4);
		    strcpy(tmp, $<strval>1);
		    strcat(tmp, $<strval>2);
		    free($<strval>1);
		    free($<strval>2);
		    $<strval>$ = tmp;
		}
	    }
         ;

tdef   : msg_desc NAME ':' typedef
        {
	    if (cltype_parse_verbose)
	      printf("read typedef '%s'\n", $<strval>2);
	    $<typdef>$ = cltype_typdef_setname($<typdef>4, $<strval>2,
					       $<strval>1);

	    if (cltype_patch_typedef_list($<typdef>$)) {
		yyerror("redefining type def");
	    }
	}	    
       | msg_desc NAME ':' error
           {
	       printf("error in tdef '%s'\n", $<strval>2); 
	       $<typdef>$ = 0;
	   }    
       ;

tdef_no_desc : NAME ':' typedef
               {
		   if (cltype_parse_verbose)
		       printf("read typedef '%s'\n", $<strval>1);
		   $<typdef>$ = cltype_typdef_setname($<typdef>3, $<strval>1,
						      NULL);
		   if (cltype_patch_typedef_list($<typdef>$)) {
		       yyerror("redefining type def");
		   }
		   
		   docgen_generate_def($<typdef>$);
	       }	    
             | NAME ':' error
               {
		   printf("error in tdef_no_desc '%s'\n", $<strval>1); 
		   $<typdef>$ = 0;
	       }    
             ;

msgtype_list : msgtype
              {
		  $<mtypent>$ = cltype_mtyplist_add($<mtypent>1, 0);
	      }
             | msgtype msgtype_list
              {
		  $<mtypent>$ = cltype_mtyplist_add($<mtypent>1,
						       $<mtypent>2);
	      }
             ;

msgtype :  '-' NAME ':' field_list
         {
	     fprintf(stderr, "%s:%d '':' after '%s' is obsolete'\n",
		     input_filename, input_linenum, $<strval>2);
		     
	     $<mtypent>$ = cltype_mtypent_alloc($<strval>2, $<dictent>4);
	 }
        |  '-' NAME field_list
         {
	     $<mtypent>$ = cltype_mtypent_alloc($<strval>2, $<dictent>3);
	 }
        |  '-' NAME ':' NAME
         {
	     $<mtypent>$ = cltype_mtypent_defer_alloc($<strval>2, $<strval>4,
						      input_filename,
						      input_linenum);
	 }
        |  '-' NAME NAME
         {
	     $<mtypent>$ = cltype_mtypent_defer_alloc($<strval>2, $<strval>3,
						      input_filename,
						      input_linenum);
	 }
        |  '-' NAME error
          {
	      printf("error in parsing msgtype '%s'\n", $<strval>1);
	      $<mtypent>$ = 0;	      
	  }
        |  '-' NAME ':' error
          {
	      printf("error in parsing msgtype '%s'\n", $<strval>1);
	      $<mtypent>$ = 0;
	  }
        ;

field_list : '(' dict_list ')'
                { $<dictent>$ = $<dictent>2 }
           | '(' T_DCT dict_list ')'
                {
		    $<dictent>$ = $<dictent>3;
		    yyerror("message field list shouldn't have T_DCT");
		}
           ;
     
typeref : NAME
         {
	     $<typdef>$ = cltype_typdef_findbyname($<strval>1, TRUE);
	     if (cltype_typdef_is_user($<typdef>$)) {
		 reflist_add_ref($<strval>1);
	     }
	 }
        | typedef
        ;

typedef : '(' T_DCT dict_list ')'
         {
	     $<typdef>$ = cltype_make_dicttype($<dictent>3);
	 }
        | '(' T_ARY typeref ')'
         {
	     $<typdef>$ = cltype_make_arraytype($<typdef>3);
	 }
        | '(' T_DSC NAME dict_list ')'
         {
	     $<typdef>$ = cltype_make_disctype($<strval>3, $<dictent>4);
	 }
        | '(' T_ENU enum_list ')'
         {
	     $<typdef>$ = cltype_make_enumtype($<enument>3);
	     cltype_reset_enumcount();
	 }
        ;

dict_list : msg_desc
           {
	       $<dictent>$ = 0;
	   }
          | msg_desc dict_entry
           {
	       $<dictent>$ = cltype_make_dictlist($<dictent>2, 0, $<strval>1);
	   }
          | msg_desc dict_entry ',' dict_list
           {
	       $<dictent>$ = cltype_make_dictlist($<dictent>2, $<dictent>4,
						  $<strval>1);
	   }
          ;

dict_entry : NAME ':' typeref
            {
		$<dictent>$ = cltype_dictent_alloc($<strval>1, $<typdef>3, 0);
	    }
           | NAME ':' error
             {
		 printf("error in parsing dict_entry '%s'\n", $<strval>1);
		 $<dictent>$ = 0;
	     }
           | '&' NAME ':' typeref
            {
		$<dictent>$ = cltype_dictent_alloc($<strval>2, $<typdef>4, 1);
	    }
  
           | '&' NAME ':' error
             {
		 printf("error in parsing dict_entry '%s'\n", $<strval>2);
		 $<dictent>$ = 0;		 
	     }  
           ;

enum_list : /* empty */     
          | enum_entry
           {
	       $<enument>$ = cltype_enument_add($<enument>1, 0);
	   }
          | enum_entry ',' enum_list
           {
	       $<enument>$ = cltype_enument_add($<enument>1, $<enument>3);
	   }
          ;

					     
enum_entry : MSGNAME
            {
		$<enument>$ = cltype_enument_alloc($<strval>1, 0, 0);
	    }
           | NAME
            {
		$<enument>$ = cltype_enument_alloc($<strval>1, 0, 0);
	    }

           | MSGNAME '=' INT
            {
		$<enument>$ = cltype_enument_alloc($<strval>1, $<intval>3, 1);
	    }

           | NAME '=' INT
            {
		$<enument>$ = cltype_enument_alloc($<strval>1, $<intval>3, 1);
	    }
           | DESC enum_entry
             {
		 $<enument>$ = cltype_enument_adddesc($<enument>2,
						      $<strval>1);
	     }

           ;


%%
     
#include <ctype.h>

yyerror(s)
     char *s;
{
    fprintf(stderr, "%s:%d '%s'\n", input_filename, input_linenum, s);
}

static FILE *ifp = stdin;

yysetup(fp, name)
     FILE *fp;
     char *name;
{
    if (fp) {
	ifp = fp;
    }
    if (input_filename) {
	clipc_free(input_filename);
    }
    if (name == 0) {
	input_filename = clipc_strdup(0, "stdin");
    }
    else {
	input_filename = clipc_strdup(0, name);
    }
    input_linenum = 1;
    getset_incl_stack(INCL_STACK_OP_PUSH, &ifp, &input_linenum,
		      &input_filename);

    cltype_init_basic_types();

    tbuf_create(APPL_BUFFER);
    tbuf_create(SECT_BUFFER);
    tbuf_create(MSG_BUFFER);
    tbuf_create(DEF_BUFFER);
    tbuf_create(DESC_BUFFER);
    tbuf_create(PROTO_BUFFER);
    tbuf_create(SIDE_BUFFER);
    tbuf_create(LISTENS_BUFFER);
    tbuf_create(REQUESTS_BUFFER);

    reflist_init();

} /* END yysetup */



#define CLTYPE_ISNAMECHAR(c) (isalpha(c) || (c) == '_' || isdigit(c))

char *lex_name(FILE *ifp, int *is_msgname)
{
    static char rbuf[1024];
    char *cp;
    int c;
    
    c = getc(ifp);
    for (cp = rbuf; CLTYPE_ISNAMECHAR(c); c = getc(ifp)) {
	if ((0 == *is_msgname) && ('_' != c) && (!isalpha(c) || isdigit(c)))
	  *is_msgname = 1;

	*cp++ = c;
    }
    *cp = 0;
    
    if (c != EOF) {
	ungetc(c, ifp);
    } else {
	c = pop_include();
	ungetc(c, ifp);
    }
    return rbuf;
}

yylex ()
{
    int c;
    char *name;
    static char *comment_buf;
    static comment_buflen;
    char *comment_cp = comment_buf;
    int is_minus = 0;
    char *strdup();

  rescan:
    /* skip white space  */
    while ((c = getc(ifp)) == ' ' || c == '\t' || c == '\n')
      if (c == '\n')
	input_linenum++;
    
    switch (c) {
      case ';':
	while ((c = getc(ifp)) != EOF && c != '\n')
	  continue;
	if (c == EOF) {
	  c = pop_include();
	  if (c == EOF) {
	      return 0;
	  }
	  ungetc(c, ifp);
	  goto rescan;
	}
	input_linenum++;
	goto rescan;

      case '/':
	c = getc(ifp);
	if (c != '/') {
	    ungetc (c, ifp);
	    c = ';';
	    break;
	}

	/* got "//" */
	c = getc(ifp);
	if (do_parse_doc_directives && (c == '-')) {
	    /* probably got a documentation directive */
	    char line[1024];
	    char *cp = line;

	    for (c = getc(ifp); ((c != EOF) && (c != '\n')); cp++) {
		*cp = c;
		c = getc(ifp);
	    }
	    *cp = '\0';
	    if ('\n' == c) {
		input_linenum++;
	    }

	    if (0 == strncmp("APPLICATION SERVICE", line, 4)) {
		return APPL_DOCTOK;
	    } else if (0 == strncmp("END APPLICATION SERVICE", line, 8)) {
		return END_APPL_DOCTOK;
	    } else if (0 == strncmp("SECTION", line, 7)) {
		return SECTION_DOCTOK;
	    } else if (0 == strncmp("END SECTION", line, 11)) {
		return END_SECTION_DOCTOK;
	    } else if (0 == strncmp("MESSAGE DEFINITION", line, 7)) {
		return MSGDEF_DOCTOK;
	    } else if (0 == strncmp("END MESSAGE DEFINITION", line, 11)) {
		return END_MSGDEF_DOCTOK;
	    } else if (0 == strncmp("DEFINITION", line, 10)) {
		return DEFDEF_DOCTOK;
	    } else if (0 == strncmp("END DEFINITION", line, 14)) {
		return END_DEFDEF_DOCTOK;
	    } else if (0 == strncmp("DESCRIPTION", line, 4)) {
		return DESCRIPTION_DOCTOK;
	    } else if (0 == strncmp("PROTOCOL", line, 8)) {
		return PROTOCOL_DOCTOK;
	    } else if (0 == strncmp("SIDE EFFECT MESSAGES", line, 11)) {
		return SIDE_EFFECT_MSGS_DOCTOK;
	    } else if (0 == strncmp("MAKES REQUESTS", line, 5)) {
		return MAKES_REQUESTS_DOCTOK;
	    } else if (0 == strncmp("LISTENS FOR", line, 7)) {
		return LISTENS_FOR_DOCTOK;
	    } else {
		/* check to see if it is a generic heading */
		for (cp = line; '\0' != *cp; cp++) {
		    if ((!isupper(*cp)) && (' ' != *cp)) {
			return 0;
		    }
		}
		yylval.strval = strdup(line);
		return GENERIC_DOCTOK;
	    }
	} /* if possible documentation directive */

	if (c != ' ')
	  ungetc (c, ifp);

	while ((c = getc(ifp)) != EOF && c != '\n') {
	    if (comment_cp+4 >= comment_buf + comment_buflen) {
		char *tmp;
		tmp = malloc(comment_buflen+1024);

		if (comment_buf)
		  free(comment_buf);
		comment_buf = tmp;
		comment_cp = comment_buf + comment_buflen;
		comment_buflen += 1024;
	    }
	    *comment_cp++ = c;
	}

	if (c == EOF) {
	  return pop_include();
        }
	input_linenum++;
	if (comment_buf) {
	    *comment_cp++ = '\n';
	    *comment_cp = 0;
	}
	    
	yylval.strval = comment_buf ? strdup(comment_buf) : 0;
	return DESC;

      case '(':
      case ')':
      case ',':
      case ':':
      case '&':
      case '=':			
	return c;

      case '-': {
	  int cc;

	  cc = getc(ifp);
	  if (isdigit(cc)) {
	      is_minus = 1;
	      c = cc;
	      break;
	  }
	  ungetc(cc, ifp);
	  return c;
      }

      case EOF:
	c = pop_include();
	if (c == EOF) {
	    return 0;
	}
	ungetc(c, ifp);
	goto rescan;
	
      default:
	break;
    }
    
    if (isdigit(c)) {
	int rval = 0;
	
	while (isdigit(c)) {
	    rval = 10 * rval + (c - '0');
	    c = getc(ifp);
	}
	if (c != EOF) {
	  ungetc(c, ifp);
        } else {
	    c = pop_include();
	    ungetc(c, ifp);
	}

	if (is_minus) {
	    rval = -rval;
	}
	yylval.intval = rval;
	return INT;
    }

    if (CLTYPE_ISNAMECHAR(c)) {
	int is_msgname = 0;
	ungetc(c, ifp);
	name = lex_name(ifp, &is_msgname);
	if (name == 0)
	  return 0;
	if (name[0] == 'T' && name[1] == '_') {
	    if (strcmp(name, "T_DCT") == 0) 
	      return T_DCT;
	    
	    if (strcmp(name, "T_DSC") == 0) 
	      return T_DSC;
	    
	    if (strcmp(name, "T_ARY") == 0) 
	      return T_ARY;
	    
	    if (strcmp(name, "T_ENU") == 0) 
	      return T_ENU;
	}

	if (0 == strcmp(name, "INCLUDE")) {
	    /* Process the include right here. */
	    char filename[1024];
	    FILE *fp;
	    char *fnp = filename;

	    /* Eat leading whitespace. */
	    while (('\n' != (c = getc(ifp))) && isspace(c)) {
		/* NULL body. */
		;
	    }
	    /* Get filename. */
	    if ('\n' == c) {
		return 0;
	    }
	    for (; !isspace(c); c = getc(ifp)) {
		*fnp = c;
		fnp++;
	    }
	    *fnp = '\0';

	    /* And eat to EOL. */
	    while ('\n' != c) {
		c = getc(ifp);
	    }

	    /* Try to open the file. */
	    fp = fopen(filename, "r");
	    if (fp == NULL) {
		fprintf(stderr, "can't open '%s' for reading\n", filename);
		perror("fopen");
		return 0;
	    }
	    getset_incl_stack(INCL_STACK_OP_PUSH, &fp, &input_linenum,
			      &input_filename);
	    input_linenum = 1;
	    input_filename = strdup(filename);
	    ifp = fp;
	    goto rescan;
	}
	yylval.strval = clipc_strdup(0, name);
	if (is_msgname)
	  return MSGNAME;
	else
	  return NAME;
    }
    goto rescan;
}

char * token_name(t)
     int t;
{
    switch (t) {
      case NAME:
	return "NAME";

      case INT:
	return "INT";

      case MSGNAME:
	return "MSGNAME";

      case T_DCT:
	return "T_DCT";
	
      case T_ARY:
	return "T_ARY";
	
      case T_DSC:
	return "T_DSC";
	
      case T_ENU:
	return "T_ENU";

      case '(':
	return "(";
	
      case ')':
	return ")";
	
      case ':':
	return ":";
	
      case '&':
	return "&";
      case '=':
	return "=";
	
      case ',':
	return ",";
	
      case '-':
	return "-";
	
      case 0:
	return "EOF";
    }	
    return "UNKNOWN";
}

print_lex_value(int rval)
{
    switch (rval) {
      case NAME:
      case MSGNAME:
	printf("(%s)\n", yylval.strval);
	break;

      case DESC:
	printf("DESCRIPTION: '%s'\n", yylval.strval);
	break;

      case INT:
	printf("(%d)\n", yylval.intval);	
	break;

      case T_DCT:
	printf("(T_DCT)\n");
	break;

      case T_ARY:
	printf("(T_ARY)\n");
	break;

      case T_ENU:
	printf("(T_ENU)\n");
	break;

      case T_DSC:
	printf("(T_DSC)\n");
	break;

      case '(': case ')':
      case ',': case ':':
      case '-': case '&': case '=':			
	printf("('%c')\n", rval);
	break;
	
      case 0:
	return 0;
	
      default:
	printf("%d: (%d)\n", rval, yylval.intval);
	break;
    }
}

    int
pop_include()
{
    FILE *old_fp;
    int old_linenum;
    char *old_filename;
    int c;

    getset_incl_stack(INCL_STACK_OP_POP, &old_fp, &old_linenum, &old_filename);
    if (0 > old_linenum) {
	/* There was nothing to pop.  We're done. */
	return EOF;
    }

    fclose(ifp);
    free(input_filename);

    ifp = old_fp;
    input_linenum = old_linenum;
    input_filename = old_filename;
    c = getc(ifp);
    return c;
}

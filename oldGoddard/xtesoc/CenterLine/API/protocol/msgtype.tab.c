
/*  A Bison parser, made from msgtype.y
 by  GNU Bison version 1.25
  */

#define YYBISON 1  /* Identify Bison output.  */

#define	MSGNAME	258
#define	NAME	259
#define	INT	260
#define	T_DCT	261
#define	T_ARY	262
#define	T_ENU	263
#define	T_DSC	264
#define	DESC	265
#define	APPL_DOCTOK	266
#define	END_APPL_DOCTOK	267
#define	SECTION_DOCTOK	268
#define	END_SECTION_DOCTOK	269
#define	MSGDEF_DOCTOK	270
#define	END_MSGDEF_DOCTOK	271
#define	DEFDEF_DOCTOK	272
#define	END_DEFDEF_DOCTOK	273
#define	DESCRIPTION_DOCTOK	274
#define	PROTOCOL_DOCTOK	275
#define	SIDE_EFFECT_MSGS_DOCTOK	276
#define	GENERIC_DOCTOK	277
#define	MAKES_REQUESTS_DOCTOK	278
#define	LISTENS_FOR_DOCTOK	279

#line 1 "msgtype.y"

#include <stdio.h>
#include <stdlib.h>    
#include "cltype.h"
#include "docdir.h"

char *clipc_strdup();    
    
#define YYDEBUG 1
#if defined(NeXT)
#define strdup(s) clipc_strdup(0, (s))
#endif

#if defined(SYSV)
#  define bcopy(A,B,L) memcpy((B),(A),(L))
#endif /* SYSV */

extern cltype_parse_verbose;
extern cltype_msg_names;
extern do_parse_doc_directives;
extern do_nest_appl_svc;

static char *input_filename;
static int input_linenum;


#line 28 "msgtype.y"
typedef union {
    int intval;
    char *strval;
    CLTYPE_MSGDEF    msgdef;
    CLTYPE_TYPDEF    typdef;
    CLTYPE_MTYPENT   mtypent;
    CLTYPE_DICTENT   dictent;
    CLTYPE_ENUMENT   enument;
} YYSTYPE;
#include <stdio.h>

#ifndef __cplusplus
#ifndef __STDC__
#define const
#endif
#endif



#define	YYFINAL		159
#define	YYFLAG		-32768
#define	YYNTBASE	32

#define YYTRANSLATE(x) ((unsigned)(x) <= 279 ? yytranslate[x] : 74)

static const char yytranslate[] = {     0,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,    30,     2,    27,
    28,     2,     2,    29,    26,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,    25,     2,     2,
    31,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     1,     2,     3,     4,     5,
     6,     7,     8,     9,    10,    11,    12,    13,    14,    15,
    16,    17,    18,    19,    20,    21,    22,    23,    24
};

#if YYDEBUG != 0
static const short yyprhs[] = {     0,
     0,     2,     4,     6,     9,    14,    18,    20,    23,    25,
    27,    29,    31,    34,    39,    44,    46,    49,    51,    53,
    56,    58,    60,    65,    71,    73,    76,    78,    79,    81,
    83,    86,    89,    92,    95,    98,   101,   104,   106,   109,
   114,   120,   123,   125,   126,   128,   130,   133,   136,   139,
   142,   144,   147,   150,   153,   157,   161,   164,   167,   168,
   171,   176,   181,   185,   189,   191,   194,   199,   203,   208,
   212,   216,   221,   225,   230,   232,   234,   239,   244,   250,
   255,   257,   260,   265,   269,   273,   278,   283,   284,   286,
   290,   292,   294,   298,   302
};

static const short yyrhs[] = {    33,
     0,    59,     0,    34,     0,    34,    33,     0,    35,    36,
    38,    37,     0,    35,    36,    37,     0,    11,     0,    62,
    49,     0,    12,     0,    44,     0,    39,     0,    40,     0,
    40,    39,     0,    41,    42,    44,    43,     0,    41,    42,
    39,    43,     0,    13,     0,    62,    49,     0,    14,     0,
    45,     0,    45,    44,     0,    46,     0,    53,     0,    47,
    49,    61,    48,     0,    47,    49,    61,    52,    48,     0,
    61,     0,    15,    62,     0,    16,     0,     0,    50,     0,
    51,     0,    51,    50,     0,    19,    62,     0,    23,    62,
     0,    24,    62,     0,    20,    62,     0,    21,    62,     0,
    22,    62,     0,    64,     0,    64,    52,     0,    54,    56,
    64,    55,     0,    54,    56,    64,    52,    55,     0,    17,
    62,     0,    18,     0,     0,    57,     0,    58,     0,    58,
    57,     0,    19,    62,     0,    20,    62,     0,    22,    62,
     0,    62,     0,    60,    59,     0,    63,    59,     0,     1,
    59,     0,    62,     3,    65,     0,    62,     4,    65,     0,
     3,    65,     0,     4,    65,     0,     0,    62,    10,     0,
    62,     4,    25,    69,     0,    62,     4,    25,     1,     0,
     4,    25,    69,     0,     4,    25,     1,     0,    66,     0,
    66,    65,     0,    26,     4,    25,    67,     0,    26,     4,
    67,     0,    26,     4,    25,     4,     0,    26,     4,     4,
     0,    26,     4,     1,     0,    26,     4,    25,     1,     0,
    27,    70,    28,     0,    27,     6,    70,    28,     0,     4,
     0,    69,     0,    27,     6,    70,    28,     0,    27,     7,
    68,    28,     0,    27,     9,     4,    70,    28,     0,    27,
     8,    72,    28,     0,    62,     0,    62,    71,     0,    62,
    71,    29,    70,     0,     4,    25,    68,     0,     4,    25,
     1,     0,    30,     4,    25,    68,     0,    30,     4,    25,
     1,     0,     0,    73,     0,    73,    29,    72,     0,     3,
     0,     4,     0,     3,    31,     5,     0,     4,    31,     5,
     0,    10,    73,     0
};

#endif

#if YYDEBUG != 0
static const short yyrline[] = { 0,
    49,    55,    58,    59,    62,    63,    66,    91,    98,   116,
   117,   120,   121,   124,   125,   128,   152,   159,   177,   178,
   181,   182,   185,   190,   196,   203,   231,   250,   251,   254,
   255,   258,   264,   270,   276,   282,   288,   291,   297,   305,
   310,   318,   346,   365,   366,   369,   370,   373,   379,   385,
   388,   389,   390,   391,   394,   403,   414,   424,   436,   437,
   457,   468,   475,   487,   494,   498,   505,   512,   516,   522,
   528,   533,   540,   542,   549,   556,   559,   563,   567,   571,
   578,   582,   586,   593,   597,   602,   607,   614,   615,   619,
   626,   630,   635,   640,   644
};
#endif


#if YYDEBUG != 0 || defined (YYERROR_VERBOSE)

static const char * const yytname[] = {   "$","error","$undefined.","MSGNAME",
"NAME","INT","T_DCT","T_ARY","T_ENU","T_DSC","DESC","APPL_DOCTOK","END_APPL_DOCTOK",
"SECTION_DOCTOK","END_SECTION_DOCTOK","MSGDEF_DOCTOK","END_MSGDEF_DOCTOK","DEFDEF_DOCTOK",
"END_DEFDEF_DOCTOK","DESCRIPTION_DOCTOK","PROTOCOL_DOCTOK","SIDE_EFFECT_MSGS_DOCTOK",
"GENERIC_DOCTOK","MAKES_REQUESTS_DOCTOK","LISTENS_FOR_DOCTOK","':'","'-'","'('",
"')'","','","'&'","'='","start","appl_doc_list","appl_doc","appl_start","appl_cont",
"appl_end","sections","section_doc_list","section_doc","section_start","section_cont",
"section_end","defs_doc_list","defs_doc","msgdef_doc","msgdef_start","msgdef_end",
"other_msg_doc_list","other_msg_docs","other_msg_doc","tdef_list","defdef_doc",
"defdef_start","defdef_end","other_def_doc_list","other_def_docs","other_def_doc",
"def_list","mdef","mdef_no_desc","msg_desc","tdef","tdef_no_desc","msgtype_list",
"msgtype","field_list","typeref","typedef","dict_list","dict_entry","enum_list",
"enum_entry", NULL
};
#endif

static const short yyr1[] = {     0,
    32,    32,    33,    33,    34,    34,    35,    36,    37,    38,
    38,    39,    39,    40,    40,    41,    42,    43,    44,    44,
    45,    45,    46,    46,    46,    47,    48,    49,    49,    50,
    50,    51,    51,    51,    51,    51,    51,    52,    52,    53,
    53,    54,    55,    56,    56,    57,    57,    58,    58,    58,
    59,    59,    59,    59,    60,    60,    61,    61,    62,    62,
    63,    63,    64,    64,    65,    65,    66,    66,    66,    66,
    66,    66,    67,    67,    68,    68,    69,    69,    69,    69,
    70,    70,    70,    71,    71,    71,    71,    72,    72,    72,
    73,    73,    73,    73,    73
};

static const short yyr2[] = {     0,
     1,     1,     1,     2,     4,     3,     1,     2,     1,     1,
     1,     1,     2,     4,     4,     1,     2,     1,     1,     2,
     1,     1,     4,     5,     1,     2,     1,     0,     1,     1,
     2,     2,     2,     2,     2,     2,     2,     1,     2,     4,
     5,     2,     1,     0,     1,     1,     2,     2,     2,     2,
     1,     2,     2,     2,     3,     3,     2,     2,     0,     2,
     4,     4,     3,     3,     1,     2,     4,     3,     4,     3,
     3,     4,     3,     4,     1,     1,     4,     4,     5,     4,
     1,     2,     4,     3,     3,     4,     4,     0,     1,     3,
     1,     1,     3,     3,     2
};

static const short yydefact[] = {     0,
     0,     7,     1,     3,    59,     2,     0,    51,     0,    54,
     4,     0,    28,    52,     0,     0,    60,    53,     0,     0,
     9,    16,    59,    59,     6,     0,    11,    12,    59,    10,
    19,    21,    28,    22,    44,    25,    59,    59,    59,    59,
    59,    59,     8,    29,    30,     0,    55,    65,     0,    56,
    57,    58,    26,    42,     5,    13,     0,    28,    20,     0,
    59,    59,    59,     0,    45,    46,    32,    35,    36,    37,
    33,    34,    31,     0,    66,    62,     0,    61,     0,     0,
    17,     0,    48,    49,    50,     0,     0,    47,    71,    70,
     0,    59,    68,    59,     0,    88,     0,    18,    15,    14,
    27,    23,     0,    38,     0,    43,     0,    40,    72,    69,
    67,    59,    81,     0,     0,    75,     0,    76,    91,    92,
     0,     0,    89,    59,    24,    39,    64,    63,    41,     0,
     0,     0,    82,    73,    77,    78,     0,     0,    95,    80,
    88,     0,    74,     0,     0,    59,    93,    94,    90,    79,
    85,    84,     0,    83,    87,    86,     0,     0,     0
};

static const short yydefgoto[] = {   157,
     3,     4,     5,    12,    25,    26,    27,    28,    29,    57,
    99,    30,    31,    32,    33,   102,    43,    44,    45,   103,
    34,    35,   108,    64,    65,    66,     6,     7,    36,   113,
     9,   104,    47,    48,    93,   117,   118,   114,   133,   122,
   123
};

static const short yypact[] = {    98,
   110,-32768,-32768,    -7,-32768,-32768,   110,    93,   110,-32768,
-32768,    70,    46,-32768,   -14,    34,-32768,-32768,   -14,   -14,
-32768,-32768,-32768,-32768,-32768,     4,-32768,    41,-32768,-32768,
    90,-32768,   107,-32768,   117,-32768,-32768,-32768,-32768,-32768,
-32768,-32768,-32768,-32768,   107,    73,-32768,   -14,    17,-32768,
-32768,-32768,    74,    74,-32768,-32768,    75,    46,-32768,    77,
-32768,-32768,-32768,    82,-32768,   117,    74,    74,    74,    74,
    74,    74,-32768,     9,-32768,-32768,   126,-32768,    81,    81,
-32768,    48,    74,    74,    74,    66,    71,-32768,-32768,-32768,
    18,    94,-32768,-32768,    44,   115,   100,-32768,-32768,-32768,
-32768,-32768,    96,    82,    30,-32768,    88,-32768,-32768,-32768,
-32768,-32768,    16,    89,   112,-32768,   113,-32768,    85,    91,
   115,   116,    95,-32768,-32768,-32768,-32768,-32768,-32768,   118,
   120,   134,   119,-32768,-32768,-32768,   137,   138,-32768,-32768,
   115,   121,-32768,    24,   122,-32768,-32768,-32768,-32768,-32768,
-32768,-32768,    26,-32768,-32768,-32768,   150,   151,-32768
};

static const short yypgoto[] = {-32768,
   148,-32768,-32768,-32768,   127,-32768,   -22,-32768,-32768,-32768,
    76,    19,-32768,-32768,-32768,    51,    14,   123,-32768,   -55,
-32768,-32768,    50,-32768,    92,-32768,   114,-32768,    99,     0,
-32768,    97,    -5,-32768,    64,  -136,   -47,   -91,-32768,    21,
    39
};


#define	YYLAST		168


static const short yytable[] = {     8,
     8,    78,   115,     2,    13,    56,     8,   152,     8,    89,
    50,    46,    90,    51,    52,    21,   156,    76,   109,   131,
   130,   110,    53,    54,   151,    17,   155,   116,    58,   116,
   127,   107,   142,    91,    79,    92,    67,    68,    69,    70,
    71,    72,    75,    77,    92,   132,    60,   116,   126,    59,
    77,    86,    77,    22,   154,    17,    77,   128,    49,    46,
    83,    84,    85,   101,    37,    38,    39,    40,    41,    42,
    77,    81,    19,    20,    86,    80,    74,    19,    20,    19,
    20,    21,    22,    17,    23,    86,    24,    22,   106,    23,
   105,    24,    19,    20,    98,    15,    16,   -59,     1,   112,
   -59,   -59,    17,   124,    23,   106,    24,   -59,     2,   -59,
     1,   101,   -59,   -59,    10,   137,   134,   119,   120,   -59,
    14,   138,    18,   141,   121,    37,    38,    39,    40,    41,
    42,    94,    95,    96,    97,    61,    62,   145,    63,   135,
   136,   147,   148,   140,   144,   143,   153,   146,   150,   158,
   159,    11,    55,   125,   111,   100,   129,    88,    82,   139,
    87,   149,     0,     0,     0,     0,     0,    73
};

static const short yycheck[] = {     0,
     1,    49,    94,    11,     5,    28,     7,   144,     9,     1,
    16,    26,     4,    19,    20,    12,   153,     1,     1,     4,
   112,     4,    23,    24,     1,    10,     1,     4,    29,     4,
     1,    87,   124,    25,    57,    27,    37,    38,    39,    40,
    41,    42,    48,    27,    27,    30,    33,     4,   104,    31,
    27,     4,    27,    13,   146,    10,    27,   105,    25,    26,
    61,    62,    63,    16,    19,    20,    21,    22,    23,    24,
    27,    58,     3,     4,     4,    57,     4,     3,     4,     3,
     4,    12,    13,    10,    15,     4,    17,    13,    18,    15,
    25,    17,     3,     4,    14,     3,     4,     0,     1,     6,
     3,     4,    10,     4,    15,    18,    17,    10,    11,     0,
     1,    16,     3,     4,     1,    31,    28,     3,     4,    10,
     7,    31,     9,    29,    10,    19,    20,    21,    22,    23,
    24,     6,     7,     8,     9,    19,    20,     4,    22,    28,
    28,     5,     5,    28,    25,    28,    25,    29,    28,     0,
     0,     4,    26,   103,    91,    80,   107,    66,    60,   121,
    64,   141,    -1,    -1,    -1,    -1,    -1,    45
};
/* -*-C-*-  Note some compilers choke on comments on `#line' lines.  */
#line 3 "/s/apps/gnu/sparc-solaris2/share/bison.simple"

/* Skeleton output parser for bison,
   Copyright (C) 1984, 1989, 1990 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

#ifndef alloca
#ifdef __GNUC__
#define alloca __builtin_alloca
#else /* not GNU C.  */
#if (!defined (__STDC__) && defined (sparc)) || defined (__sparc__) || defined (__sparc) || defined (__sgi)
#include <alloca.h>
#else /* not sparc */
#if defined (MSDOS) && !defined (__TURBOC__)
#include <malloc.h>
#else /* not MSDOS, or __TURBOC__ */
#if defined(_AIX)
#include <malloc.h>
 #pragma alloca
#else /* not MSDOS, __TURBOC__, or _AIX */
#ifdef __hpux
#ifdef __cplusplus
extern "C" {
void *alloca (unsigned int);
};
#else /* not __cplusplus */
void *alloca ();
#endif /* not __cplusplus */
#endif /* __hpux */
#endif /* not _AIX */
#endif /* not MSDOS, or __TURBOC__ */
#endif /* not sparc.  */
#endif /* not GNU C.  */
#endif /* alloca not defined.  */

/* This is the parser code that is written into each bison parser
  when the %semantic_parser declaration is not specified in the grammar.
  It was written by Richard Stallman by simplifying the hairy parser
  used when %semantic_parser is specified.  */

/* Note: there must be only one dollar sign in this file.
   It is replaced by the list of actions, each action
   as one case of the switch.  */

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		-2
#define YYEOF		0
#define YYACCEPT	return(0)
#define YYABORT 	return(1)
#define YYERROR		goto yyerrlab1
/* Like YYERROR except do call yyerror.
   This remains here temporarily to ease the
   transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */
#define YYFAIL		goto yyerrlab
#define YYRECOVERING()  (!!yyerrstatus)
#define YYBACKUP(token, value) \
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    { yychar = (token), yylval = (value);			\
      yychar1 = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    { yyerror ("syntax error: cannot back up"); YYERROR; }	\
while (0)

#define YYTERROR	1
#define YYERRCODE	256

#ifndef YYPURE
#define YYLEX		yylex()
#endif

#ifdef YYPURE
#ifdef YYLSP_NEEDED
#ifdef YYLEX_PARAM
#define YYLEX		yylex(&yylval, &yylloc, YYLEX_PARAM)
#else
#define YYLEX		yylex(&yylval, &yylloc)
#endif
#else /* not YYLSP_NEEDED */
#ifdef YYLEX_PARAM
#define YYLEX		yylex(&yylval, YYLEX_PARAM)
#else
#define YYLEX		yylex(&yylval)
#endif
#endif /* not YYLSP_NEEDED */
#endif

/* If nonreentrant, generate the variables here */

#ifndef YYPURE

int	yychar;			/*  the lookahead symbol		*/
YYSTYPE	yylval;			/*  the semantic value of the		*/
				/*  lookahead symbol			*/

#ifdef YYLSP_NEEDED
YYLTYPE yylloc;			/*  location data for the lookahead	*/
				/*  symbol				*/
#endif

int yynerrs;			/*  number of parse errors so far       */
#endif  /* not YYPURE */

#if YYDEBUG != 0
int yydebug;			/*  nonzero means print parse trace	*/
/* Since this is uninitialized, it does not stop multiple parsers
   from coexisting.  */
#endif

/*  YYINITDEPTH indicates the initial size of the parser's stacks	*/

#ifndef	YYINITDEPTH
#define YYINITDEPTH 200
#endif

/*  YYMAXDEPTH is the maximum size the stacks can grow to
    (effective only if the built-in stack extension method is used).  */

#if YYMAXDEPTH == 0
#undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
#define YYMAXDEPTH 10000
#endif

/* Prevent warning if -Wstrict-prototypes.  */
#ifdef __GNUC__
int yyparse (void);
#endif

#if __GNUC__ > 1		/* GNU C and GNU C++ define this.  */
#define __yy_memcpy(TO,FROM,COUNT)	__builtin_memcpy(TO,FROM,COUNT)
#else				/* not GNU C or C++ */
#ifndef __cplusplus

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_memcpy (to, from, count)
     char *to;
     char *from;
     int count;
{
  register char *f = from;
  register char *t = to;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#else /* __cplusplus */

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_memcpy (char *to, char *from, int count)
{
  register char *f = from;
  register char *t = to;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#endif
#endif

#line 196 "/s/apps/gnu/sparc-solaris2/share/bison.simple"

/* The user can define YYPARSE_PARAM as the name of an argument to be passed
   into yyparse.  The argument should have type void *.
   It should actually point to an object.
   Grammar actions can access the variable by casting it
   to the proper pointer type.  */

#ifdef YYPARSE_PARAM
#ifdef __cplusplus
#define YYPARSE_PARAM_ARG void *YYPARSE_PARAM
#define YYPARSE_PARAM_DECL
#else /* not __cplusplus */
#define YYPARSE_PARAM_ARG YYPARSE_PARAM
#define YYPARSE_PARAM_DECL void *YYPARSE_PARAM;
#endif /* not __cplusplus */
#else /* not YYPARSE_PARAM */
#define YYPARSE_PARAM_ARG
#define YYPARSE_PARAM_DECL
#endif /* not YYPARSE_PARAM */

int
yyparse(YYPARSE_PARAM_ARG)
     YYPARSE_PARAM_DECL
{
  register int yystate;
  register int yyn;
  register short *yyssp;
  register YYSTYPE *yyvsp;
  int yyerrstatus;	/*  number of tokens to shift before error messages enabled */
  int yychar1 = 0;		/*  lookahead token as an internal (translated) token number */

  short	yyssa[YYINITDEPTH];	/*  the state stack			*/
  YYSTYPE yyvsa[YYINITDEPTH];	/*  the semantic value stack		*/

  short *yyss = yyssa;		/*  refer to the stacks thru separate pointers */
  YYSTYPE *yyvs = yyvsa;	/*  to allow yyoverflow to reallocate them elsewhere */

#ifdef YYLSP_NEEDED
  YYLTYPE yylsa[YYINITDEPTH];	/*  the location stack			*/
  YYLTYPE *yyls = yylsa;
  YYLTYPE *yylsp;

#define YYPOPSTACK   (yyvsp--, yyssp--, yylsp--)
#else
#define YYPOPSTACK   (yyvsp--, yyssp--)
#endif

  int yystacksize = YYINITDEPTH;

#ifdef YYPURE
  int yychar;
  YYSTYPE yylval;
  int yynerrs;
#ifdef YYLSP_NEEDED
  YYLTYPE yylloc;
#endif
#endif

  YYSTYPE yyval;		/*  the variable used to return		*/
				/*  semantic values from the action	*/
				/*  routines				*/

  int yylen;

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Starting parse\n");
#endif

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss - 1;
  yyvsp = yyvs;
#ifdef YYLSP_NEEDED
  yylsp = yyls;
#endif

/* Push a new state, which is found in  yystate  .  */
/* In all cases, when you get here, the value and location stacks
   have just been pushed. so pushing a state here evens the stacks.  */
yynewstate:

  *++yyssp = yystate;

  if (yyssp >= yyss + yystacksize - 1)
    {
      /* Give user a chance to reallocate the stack */
      /* Use copies of these so that the &'s don't force the real ones into memory. */
      YYSTYPE *yyvs1 = yyvs;
      short *yyss1 = yyss;
#ifdef YYLSP_NEEDED
      YYLTYPE *yyls1 = yyls;
#endif

      /* Get the current used size of the three stacks, in elements.  */
      int size = yyssp - yyss + 1;

#ifdef yyoverflow
      /* Each stack pointer address is followed by the size of
	 the data in use in that stack, in bytes.  */
#ifdef YYLSP_NEEDED
      /* This used to be a conditional around just the two extra args,
	 but that might be undefined if yyoverflow is a macro.  */
      yyoverflow("parser stack overflow",
		 &yyss1, size * sizeof (*yyssp),
		 &yyvs1, size * sizeof (*yyvsp),
		 &yyls1, size * sizeof (*yylsp),
		 &yystacksize);
#else
      yyoverflow("parser stack overflow",
		 &yyss1, size * sizeof (*yyssp),
		 &yyvs1, size * sizeof (*yyvsp),
		 &yystacksize);
#endif

      yyss = yyss1; yyvs = yyvs1;
#ifdef YYLSP_NEEDED
      yyls = yyls1;
#endif
#else /* no yyoverflow */
      /* Extend the stack our own way.  */
      if (yystacksize >= YYMAXDEPTH)
	{
	  yyerror("parser stack overflow");
	  return 2;
	}
      yystacksize *= 2;
      if (yystacksize > YYMAXDEPTH)
	yystacksize = YYMAXDEPTH;
      yyss = (short *) alloca (yystacksize * sizeof (*yyssp));
      __yy_memcpy ((char *)yyss, (char *)yyss1, size * sizeof (*yyssp));
      yyvs = (YYSTYPE *) alloca (yystacksize * sizeof (*yyvsp));
      __yy_memcpy ((char *)yyvs, (char *)yyvs1, size * sizeof (*yyvsp));
#ifdef YYLSP_NEEDED
      yyls = (YYLTYPE *) alloca (yystacksize * sizeof (*yylsp));
      __yy_memcpy ((char *)yyls, (char *)yyls1, size * sizeof (*yylsp));
#endif
#endif /* no yyoverflow */

      yyssp = yyss + size - 1;
      yyvsp = yyvs + size - 1;
#ifdef YYLSP_NEEDED
      yylsp = yyls + size - 1;
#endif

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Stack size increased to %d\n", yystacksize);
#endif

      if (yyssp >= yyss + yystacksize - 1)
	YYABORT;
    }

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Entering state %d\n", yystate);
#endif

  goto yybackup;
 yybackup:

/* Do appropriate processing given the current state.  */
/* Read a lookahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to lookahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* yychar is either YYEMPTY or YYEOF
     or a valid token in external form.  */

  if (yychar == YYEMPTY)
    {
#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Reading a token: ");
#endif
      yychar = YYLEX;
    }

  /* Convert token to internal form (in yychar1) for indexing tables with */

  if (yychar <= 0)		/* This means end of input. */
    {
      yychar1 = 0;
      yychar = YYEOF;		/* Don't call YYLEX any more */

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Now at end of input.\n");
#endif
    }
  else
    {
      yychar1 = YYTRANSLATE(yychar);

#if YYDEBUG != 0
      if (yydebug)
	{
	  fprintf (stderr, "Next token is %d (%s", yychar, yytname[yychar1]);
	  /* Give the individual parser a way to print the precise meaning
	     of a token, for further debugging info.  */
#ifdef YYPRINT
	  YYPRINT (stderr, yychar, yylval);
#endif
	  fprintf (stderr, ")\n");
	}
#endif
    }

  yyn += yychar1;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != yychar1)
    goto yydefault;

  yyn = yytable[yyn];

  /* yyn is what to do for this token type in this state.
     Negative => reduce, -yyn is rule number.
     Positive => shift, yyn is new state.
       New state is final state => don't bother to shift,
       just return success.
     0, or most negative number => error.  */

  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrlab;

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the lookahead token.  */

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Shifting token %d (%s), ", yychar, yytname[yychar1]);
#endif

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;
#ifdef YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  /* count tokens shifted since error; after three, turn off error status.  */
  if (yyerrstatus) yyerrstatus--;

  yystate = yyn;
  goto yynewstate;

/* Do the default action for the current state.  */
yydefault:

  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;

/* Do a reduction.  yyn is the number of a rule to reduce with.  */
yyreduce:
  yylen = yyr2[yyn];
  if (yylen > 0)
    yyval = yyvsp[1-yylen]; /* implement default value of the action */

#if YYDEBUG != 0
  if (yydebug)
    {
      int i;

      fprintf (stderr, "Reducing via rule %d (line %d), ",
	       yyn, yyrline[yyn]);

      /* Print the symbols being reduced, and their result.  */
      for (i = yyprhs[yyn]; yyrhs[i] > 0; i++)
	fprintf (stderr, "%s ", yytname[yyrhs[i]]);
      fprintf (stderr, " -> %s\n", yytname[yyr1[yyn]]);
    }
#endif


  switch (yyn) {

case 1:
#line 50 "msgtype.y"
{
	   marker_fixup_refs();
	   docgen_close();
       ;
    break;}
case 2:
#line 55 "msgtype.y"
{;
    break;}
case 3:
#line 58 "msgtype.y"
{;
    break;}
case 4:
#line 59 "msgtype.y"
{;
    break;}
case 5:
#line 62 "msgtype.y"
{;
    break;}
case 6:
#line 63 "msgtype.y"
{;
    break;}
case 7:
#line 67 "msgtype.y"
{ 
		 enum DDTYPE current_scope;

		 current_scope = getset_ddstack(DDSTACK_OP_NOOP, DDTYPE_NONE);
		 if ((DDTYPE_NONE != current_scope) &&
		     (DDTYPE_APPLICATION_SERVICE != current_scope)) {
		     char buf[80];

		     sprintf(buf, "Attempting to start %s before %s end.",
			     ddtype_string[DDTYPE_APPLICATION_SERVICE],
			     ddtype_string[current_scope]);
		     yyerror(buf);
		 }

		 (void)getset_ddstack(DDSTACK_OP_PUSH,
				      DDTYPE_APPLICATION_SERVICE);
		 tbuf_clear(APPL_BUFFER);
		 tbuf_clear(DESC_BUFFER);
		 tbuf_clear(PROTO_BUFFER);
		 tbuf_clear(REQUESTS_BUFFER);
		 tbuf_clear(LISTENS_BUFFER);
	     ;
    break;}
case 8:
#line 92 "msgtype.y"
{
	       tbuf_set(APPL_BUFFER,  yyvsp[-1].strval);
	       docgen_generate_appl_svc();
	   ;
    break;}
case 9:
#line 99 "msgtype.y"
{
	     enum DDTYPE current_scope;

	     current_scope = getset_ddstack(DDSTACK_OP_NOOP, DDTYPE_NONE);
	     if (DDTYPE_APPLICATION_SERVICE != current_scope) {
		 char buf[80];

		 sprintf(buf, "Attempting to end %s before %s end.",
			 ddtype_string[DDTYPE_APPLICATION_SERVICE],
			 ddtype_string[current_scope]);
		 yyerror(buf);
	     }

	     (void)getset_ddstack(DDSTACK_OP_POP, DDTYPE_NONE);
	     ;
    break;}
case 10:
#line 116 "msgtype.y"
{;
    break;}
case 11:
#line 117 "msgtype.y"
{;
    break;}
case 12:
#line 120 "msgtype.y"
{;
    break;}
case 13:
#line 121 "msgtype.y"
{;
    break;}
case 14:
#line 124 "msgtype.y"
{;
    break;}
case 15:
#line 125 "msgtype.y"
{;
    break;}
case 16:
#line 129 "msgtype.y"
{
		    enum DDTYPE current_scope;

		    current_scope = getset_ddstack(DDSTACK_OP_NOOP,
						   DDTYPE_NONE);
		    if ((DDTYPE_NONE != current_scope) &&
			(DDTYPE_APPLICATION_SERVICE != current_scope) &&
			(DDTYPE_SECTION != current_scope)) {
			char buf[80];

			sprintf(buf, "Attempting to start %s before %s end.",
				ddtype_string[DDTYPE_SECTION],
				ddtype_string[current_scope]);
			yyerror(buf);
		    }

		    (void)getset_ddstack(DDSTACK_OP_PUSH, DDTYPE_SECTION);
		    tbuf_clear(SECT_BUFFER);
		    tbuf_clear(DESC_BUFFER);
		    tbuf_clear(PROTO_BUFFER);
	        ;
    break;}
case 17:
#line 153 "msgtype.y"
{
		    tbuf_set(SECT_BUFFER, yyvsp[-1].strval);
		    docgen_generate_section();
		;
    break;}
case 18:
#line 160 "msgtype.y"
{
		  enum DDTYPE current_scope;

		  current_scope = getset_ddstack(DDSTACK_OP_NOOP, DDTYPE_NONE);
		  if (DDTYPE_SECTION != current_scope) {
		      char buf[80];

		      sprintf(buf, "Attempting to end %s before %s end.",
			      ddtype_string[DDTYPE_SECTION],
			      ddtype_string[current_scope]);
		      yyerror(buf);
		  }

		  (void)getset_ddstack(DDSTACK_OP_POP, DDTYPE_NONE);
	      ;
    break;}
case 19:
#line 177 "msgtype.y"
{;
    break;}
case 20:
#line 178 "msgtype.y"
{;
    break;}
case 21:
#line 181 "msgtype.y"
{;
    break;}
case 22:
#line 182 "msgtype.y"
{;
    break;}
case 23:
#line 186 "msgtype.y"
{
	    cltype_msgdef_add(yyvsp[-1].msgdef);
	    docgen_finish_message();
	    ;
    break;}
case 24:
#line 192 "msgtype.y"
{
	    cltype_msgdef_add(yyvsp[-2].msgdef);
	    docgen_finish_message();
	    ;
    break;}
case 25:
#line 197 "msgtype.y"
{
	    cltype_msgdef_add(yyvsp[0].msgdef);
	    docgen_finish_message();
	    ;
    break;}
case 26:
#line 204 "msgtype.y"
{
		    enum DDTYPE current_scope;

		    current_scope = getset_ddstack(DDSTACK_OP_NOOP,
						   DDTYPE_NONE);
		    if ((DDTYPE_NONE != current_scope) &&
			(DDTYPE_APPLICATION_SERVICE != current_scope) &&
			(DDTYPE_SECTION != current_scope)) {
			char buf[80];

			sprintf(buf, "Attempting to start %s before %s end.",
				ddtype_string[DDTYPE_MSG_DEF],
				ddtype_string[current_scope]);
			yyerror(buf);
		    }

		    (void)getset_ddstack(DDSTACK_OP_PUSH, DDTYPE_MSG_DEF);
		    tbuf_clear(MSG_BUFFER);
		    tbuf_clear(DESC_BUFFER);
		    tbuf_clear(PROTO_BUFFER);
		    tbuf_clear(SIDE_BUFFER);
		    tbuf_set(MSG_BUFFER, yyvsp[0].strval);

		    reflist_init();
	        ;
    break;}
case 27:
#line 232 "msgtype.y"
{
		  enum DDTYPE current_scope;
		  char *ref;

		  current_scope = getset_ddstack(DDSTACK_OP_NOOP, DDTYPE_NONE);
		  if (DDTYPE_MSG_DEF != current_scope) {
		      char buf[80];

		      sprintf(buf, "Attempting to end %s before %s end.",
			      ddtype_string[DDTYPE_MSG_DEF],
			      ddtype_string[current_scope]);
		      yyerror(buf);
		  }

		  (void)getset_ddstack(DDSTACK_OP_POP, DDTYPE_NONE);
	      ;
    break;}
case 28:
#line 250 "msgtype.y"
{;
    break;}
case 29:
#line 251 "msgtype.y"
{;
    break;}
case 30:
#line 254 "msgtype.y"
{;
    break;}
case 31:
#line 255 "msgtype.y"
{;
    break;}
case 32:
#line 259 "msgtype.y"
{
		   tbuf_clear(DESC_BUFFER);
		   tbuf_set(DESC_BUFFER, yyvsp[0].strval);
               ;
    break;}
case 33:
#line 265 "msgtype.y"
{
		   tbuf_clear(REQUESTS_BUFFER);
		   tbuf_set(REQUESTS_BUFFER, yyvsp[0].strval);
               ;
    break;}
case 34:
#line 271 "msgtype.y"
{
		   tbuf_clear(LISTENS_BUFFER);
		   tbuf_set(LISTENS_BUFFER, yyvsp[0].strval);
               ;
    break;}
case 35:
#line 277 "msgtype.y"
{
		   tbuf_clear(PROTO_BUFFER);
		   tbuf_set(PROTO_BUFFER, yyvsp[0].strval);
               ;
    break;}
case 36:
#line 283 "msgtype.y"
{
		   tbuf_clear(SIDE_BUFFER);
		   tbuf_set(SIDE_BUFFER, yyvsp[0].strval);
               ;
    break;}
case 37:
#line 288 "msgtype.y"
{;
    break;}
case 38:
#line 292 "msgtype.y"
{
            cltype_typdef_add(yyvsp[0].typdef); 
	    /* This is a local defdef.  Remove it from the reflist. */
	    reflist_remove_ref(cltype_typdef_getname(yyvsp[0].typdef));
	   ;
    break;}
case 39:
#line 298 "msgtype.y"
{
	    /* This is a local defdef.  Remove it from the reflist. */
            cltype_typdef_add(yyvsp[-1].typdef); 
	    reflist_remove_ref(cltype_typdef_getname(yyvsp[-1].typdef));
	   ;
    break;}
case 40:
#line 306 "msgtype.y"
{
             cltype_typdef_add(yyvsp[-1].typdef); 
	     docgen_finish_def();
	    ;
    break;}
case 41:
#line 312 "msgtype.y"
{
             cltype_typdef_add(yyvsp[-2].typdef); 
	     docgen_finish_def();
	    ;
    break;}
case 42:
#line 319 "msgtype.y"
{
		    enum DDTYPE current_scope;

		    current_scope = getset_ddstack(DDSTACK_OP_NOOP,
						   DDTYPE_NONE);
		    if ((DDTYPE_NONE != current_scope) &&
			(DDTYPE_APPLICATION_SERVICE != current_scope) &&
			(DDTYPE_SECTION != current_scope)) {
			char buf[80];

			sprintf(buf, "Attempting to start %s before %s end.",
				ddtype_string[DDTYPE_DEF_DEF],
				ddtype_string[current_scope]);
			yyerror(buf);
		    }

		    (void)getset_ddstack(DDSTACK_OP_PUSH, DDTYPE_DEF_DEF);
		    tbuf_clear(DEF_BUFFER);
		    tbuf_clear(DESC_BUFFER);
		    tbuf_clear(PROTO_BUFFER);
		    tbuf_clear(SIDE_BUFFER);
		    tbuf_set(DEF_BUFFER, yyvsp[0].strval);
		    docgen_reset_defcount();
		    reflist_init();
	        ;
    break;}
case 43:
#line 347 "msgtype.y"
{
		  enum DDTYPE current_scope;
		  char *ref;

		  current_scope = getset_ddstack(DDSTACK_OP_NOOP, DDTYPE_NONE);
		  if (DDTYPE_DEF_DEF != current_scope) {
		      char buf[80];

		      sprintf(buf, "Attempting to end %s before %s end.",
			      ddtype_string[DDTYPE_DEF_DEF],
			      ddtype_string[current_scope]);
		      yyerror(buf);
		  }

		  (void)getset_ddstack(DDSTACK_OP_POP, DDTYPE_NONE);
	      ;
    break;}
case 44:
#line 365 "msgtype.y"
{;
    break;}
case 45:
#line 366 "msgtype.y"
{;
    break;}
case 46:
#line 369 "msgtype.y"
{;
    break;}
case 47:
#line 370 "msgtype.y"
{;
    break;}
case 48:
#line 374 "msgtype.y"
{
		   tbuf_clear(DESC_BUFFER);
		   tbuf_set(DESC_BUFFER, yyvsp[0].strval);
	       ;
    break;}
case 49:
#line 380 "msgtype.y"
{
		   tbuf_clear(PROTO_BUFFER);
		   tbuf_set(PROTO_BUFFER, yyvsp[0].strval);
               ;
    break;}
case 50:
#line 385 "msgtype.y"
{;
    break;}
case 51:
#line 388 "msgtype.y"
{;
    break;}
case 52:
#line 389 "msgtype.y"
{ cltype_msgdef_add(yyvsp[-1].msgdef); ;
    break;}
case 53:
#line 390 "msgtype.y"
{ cltype_typdef_add(yyvsp[-1].typdef); ;
    break;}
case 55:
#line 395 "msgtype.y"
{
	    if (cltype_parse_verbose)
	      printf("read message '%s'\n", yyvsp[-1].strval);
	    if (cltype_msg_names)
	      printf("\"%s\"\n", yyvsp[-1].strval);
	    yyval.msgdef = cltype_msgdef_alloc(yyvsp[-1].strval, yyvsp[0].mtypent,
					     yyvsp[-2].strval);
	;
    break;}
case 56:
#line 404 "msgtype.y"
{
	    if (cltype_parse_verbose)
	      printf("read message '%s'\n", yyvsp[-1].strval);
	    if (cltype_msg_names)
	      printf("\"%s\"\n", yyvsp[-1].strval);
	    yyval.msgdef = cltype_msgdef_alloc(yyvsp[-1].strval, yyvsp[0].mtypent,
					     yyvsp[-2].strval); 
	;
    break;}
case 57:
#line 415 "msgtype.y"
{
		     if (cltype_parse_verbose)
			 printf("read message '%s'\n", yyvsp[-1].strval);
		     if (cltype_msg_names)
			 printf("\"%s\"\n", yyvsp[0].strval);
		     yyval.msgdef = cltype_msgdef_alloc(yyvsp[-1].strval, yyvsp[0].mtypent,
						      NULL);
		     docgen_generate_message(yyval.msgdef);
	         ;
    break;}
case 58:
#line 425 "msgtype.y"
{
		     if (cltype_parse_verbose)
			 printf("read message '%s'\n", yyvsp[-1].strval);
		     if (cltype_msg_names)
			 printf("\"%s\"\n", yyvsp[0].strval);
		     yyval.msgdef = cltype_msgdef_alloc(yyvsp[-1].strval, yyvsp[0].mtypent,
						      NULL); 
		     docgen_generate_message(yyval.msgdef);
	         ;
    break;}
case 59:
#line 436 "msgtype.y"
{yyval.strval = 0;;
    break;}
case 60:
#line 438 "msgtype.y"
{
		/* not the best string handling routine around, but the
		 * inputs will be relatively short.
		 */
		if (yyvsp[-1].strval == 0) {
		    yyval.strval = yyvsp[0].strval;
		}
		else {
		    char *tmp;
		    tmp = malloc(strlen(yyvsp[-1].strval) + strlen(yyvsp[0].strval) + 4);
		    strcpy(tmp, yyvsp[-1].strval);
		    strcat(tmp, yyvsp[0].strval);
		    free(yyvsp[-1].strval);
		    free(yyvsp[0].strval);
		    yyval.strval = tmp;
		}
	    ;
    break;}
case 61:
#line 458 "msgtype.y"
{
	    if (cltype_parse_verbose)
	      printf("read typedef '%s'\n", yyvsp[-2].strval);
	    yyval.typdef = cltype_typdef_setname(yyvsp[0].typdef, yyvsp[-2].strval,
					       yyvsp[-3].strval);

	    if (cltype_patch_typedef_list(yyval.typdef)) {
		yyerror("redefining type def");
	    }
	;
    break;}
case 62:
#line 469 "msgtype.y"
{
	       printf("error in tdef '%s'\n", yyvsp[-2].strval); 
	       yyval.typdef = 0;
	   ;
    break;}
case 63:
#line 476 "msgtype.y"
{
		   if (cltype_parse_verbose)
		       printf("read typedef '%s'\n", yyvsp[-2].strval);
		   yyval.typdef = cltype_typdef_setname(yyvsp[0].typdef, yyvsp[-2].strval,
						      NULL);
		   if (cltype_patch_typedef_list(yyval.typdef)) {
		       yyerror("redefining type def");
		   }
		   
		   docgen_generate_def(yyval.typdef);
	       ;
    break;}
case 64:
#line 488 "msgtype.y"
{
		   printf("error in tdef_no_desc '%s'\n", yyvsp[-2].strval); 
		   yyval.typdef = 0;
	       ;
    break;}
case 65:
#line 495 "msgtype.y"
{
		  yyval.mtypent = cltype_mtyplist_add(yyvsp[0].mtypent, 0);
	      ;
    break;}
case 66:
#line 499 "msgtype.y"
{
		  yyval.mtypent = cltype_mtyplist_add(yyvsp[-1].mtypent,
						       yyvsp[0].mtypent);
	      ;
    break;}
case 67:
#line 506 "msgtype.y"
{
	     fprintf(stderr, "%s:%d '':' after '%s' is obsolete'\n",
		     input_filename, input_linenum, yyvsp[-2].strval);
		     
	     yyval.mtypent = cltype_mtypent_alloc(yyvsp[-2].strval, yyvsp[0].dictent);
	 ;
    break;}
case 68:
#line 513 "msgtype.y"
{
	     yyval.mtypent = cltype_mtypent_alloc(yyvsp[-1].strval, yyvsp[0].dictent);
	 ;
    break;}
case 69:
#line 517 "msgtype.y"
{
	     yyval.mtypent = cltype_mtypent_defer_alloc(yyvsp[-2].strval, yyvsp[0].strval,
						      input_filename,
						      input_linenum);
	 ;
    break;}
case 70:
#line 523 "msgtype.y"
{
	     yyval.mtypent = cltype_mtypent_defer_alloc(yyvsp[-1].strval, yyvsp[0].strval,
						      input_filename,
						      input_linenum);
	 ;
    break;}
case 71:
#line 529 "msgtype.y"
{
	      printf("error in parsing msgtype '%s'\n", yyvsp[-2].strval);
	      yyval.mtypent = 0;	      
	  ;
    break;}
case 72:
#line 534 "msgtype.y"
{
	      printf("error in parsing msgtype '%s'\n", yyvsp[-3].strval);
	      yyval.mtypent = 0;
	  ;
    break;}
case 73:
#line 541 "msgtype.y"
{ yyval.dictent = yyvsp[-1].dictent ;
    break;}
case 74:
#line 543 "msgtype.y"
{
		    yyval.dictent = yyvsp[-1].dictent;
		    yyerror("message field list shouldn't have T_DCT");
		;
    break;}
case 75:
#line 550 "msgtype.y"
{
	     yyval.typdef = cltype_typdef_findbyname(yyvsp[0].strval, TRUE);
	     if (cltype_typdef_is_user(yyval.typdef)) {
		 reflist_add_ref(yyvsp[0].strval);
	     }
	 ;
    break;}
case 77:
#line 560 "msgtype.y"
{
	     yyval.typdef = cltype_make_dicttype(yyvsp[-1].dictent);
	 ;
    break;}
case 78:
#line 564 "msgtype.y"
{
	     yyval.typdef = cltype_make_arraytype(yyvsp[-1].typdef);
	 ;
    break;}
case 79:
#line 568 "msgtype.y"
{
	     yyval.typdef = cltype_make_disctype(yyvsp[-2].strval, yyvsp[-1].dictent);
	 ;
    break;}
case 80:
#line 572 "msgtype.y"
{
	     yyval.typdef = cltype_make_enumtype(yyvsp[-1].enument);
	     cltype_reset_enumcount();
	 ;
    break;}
case 81:
#line 579 "msgtype.y"
{
	       yyval.dictent = 0;
	   ;
    break;}
case 82:
#line 583 "msgtype.y"
{
	       yyval.dictent = cltype_make_dictlist(yyvsp[0].dictent, 0, yyvsp[-1].strval);
	   ;
    break;}
case 83:
#line 587 "msgtype.y"
{
	       yyval.dictent = cltype_make_dictlist(yyvsp[-2].dictent, yyvsp[0].dictent,
						  yyvsp[-3].strval);
	   ;
    break;}
case 84:
#line 594 "msgtype.y"
{
		yyval.dictent = cltype_dictent_alloc(yyvsp[-2].strval, yyvsp[0].typdef, 0);
	    ;
    break;}
case 85:
#line 598 "msgtype.y"
{
		 printf("error in parsing dict_entry '%s'\n", yyvsp[-2].strval);
		 yyval.dictent = 0;
	     ;
    break;}
case 86:
#line 603 "msgtype.y"
{
		yyval.dictent = cltype_dictent_alloc(yyvsp[-2].strval, yyvsp[0].typdef, 1);
	    ;
    break;}
case 87:
#line 608 "msgtype.y"
{
		 printf("error in parsing dict_entry '%s'\n", yyvsp[-2].strval);
		 yyval.dictent = 0;		 
	     ;
    break;}
case 89:
#line 616 "msgtype.y"
{
	       yyval.enument = cltype_enument_add(yyvsp[0].enument, 0);
	   ;
    break;}
case 90:
#line 620 "msgtype.y"
{
	       yyval.enument = cltype_enument_add(yyvsp[-2].enument, yyvsp[0].enument);
	   ;
    break;}
case 91:
#line 627 "msgtype.y"
{
		yyval.enument = cltype_enument_alloc(yyvsp[0].strval, 0, 0);
	    ;
    break;}
case 92:
#line 631 "msgtype.y"
{
		yyval.enument = cltype_enument_alloc(yyvsp[0].strval, 0, 0);
	    ;
    break;}
case 93:
#line 636 "msgtype.y"
{
		yyval.enument = cltype_enument_alloc(yyvsp[-2].strval, yyvsp[0].intval, 1);
	    ;
    break;}
case 94:
#line 641 "msgtype.y"
{
		yyval.enument = cltype_enument_alloc(yyvsp[-2].strval, yyvsp[0].intval, 1);
	    ;
    break;}
case 95:
#line 645 "msgtype.y"
{
		 yyval.enument = cltype_enument_adddesc(yyvsp[0].enument,
						      yyvsp[-1].strval);
	     ;
    break;}
}
   /* the action file gets copied in in place of this dollarsign */
#line 498 "/s/apps/gnu/sparc-solaris2/share/bison.simple"

  yyvsp -= yylen;
  yyssp -= yylen;
#ifdef YYLSP_NEEDED
  yylsp -= yylen;
#endif

#if YYDEBUG != 0
  if (yydebug)
    {
      short *ssp1 = yyss - 1;
      fprintf (stderr, "state stack now");
      while (ssp1 != yyssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

  *++yyvsp = yyval;

#ifdef YYLSP_NEEDED
  yylsp++;
  if (yylen == 0)
    {
      yylsp->first_line = yylloc.first_line;
      yylsp->first_column = yylloc.first_column;
      yylsp->last_line = (yylsp-1)->last_line;
      yylsp->last_column = (yylsp-1)->last_column;
      yylsp->text = 0;
    }
  else
    {
      yylsp->last_line = (yylsp+yylen-1)->last_line;
      yylsp->last_column = (yylsp+yylen-1)->last_column;
    }
#endif

  /* Now "shift" the result of the reduction.
     Determine what state that goes to,
     based on the state we popped back to
     and the rule number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTBASE] + *yyssp;
  if (yystate >= 0 && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTBASE];

  goto yynewstate;

yyerrlab:   /* here on detecting error */

  if (! yyerrstatus)
    /* If not already recovering from an error, report this error.  */
    {
      ++yynerrs;

#ifdef YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (yyn > YYFLAG && yyn < YYLAST)
	{
	  int size = 0;
	  char *msg;
	  int x, count;

	  count = 0;
	  /* Start X at -yyn if nec to avoid negative indexes in yycheck.  */
	  for (x = (yyn < 0 ? -yyn : 0);
	       x < (sizeof(yytname) / sizeof(char *)); x++)
	    if (yycheck[x + yyn] == x)
	      size += strlen(yytname[x]) + 15, count++;
	  msg = (char *) malloc(size + 15);
	  if (msg != 0)
	    {
	      strcpy(msg, "parse error");

	      if (count < 5)
		{
		  count = 0;
		  for (x = (yyn < 0 ? -yyn : 0);
		       x < (sizeof(yytname) / sizeof(char *)); x++)
		    if (yycheck[x + yyn] == x)
		      {
			strcat(msg, count == 0 ? ", expecting `" : " or `");
			strcat(msg, yytname[x]);
			strcat(msg, "'");
			count++;
		      }
		}
	      yyerror(msg);
	      free(msg);
	    }
	  else
	    yyerror ("parse error; also virtual memory exceeded");
	}
      else
#endif /* YYERROR_VERBOSE */
	yyerror("parse error");
    }

  goto yyerrlab1;
yyerrlab1:   /* here on error raised explicitly by an action */

  if (yyerrstatus == 3)
    {
      /* if just tried and failed to reuse lookahead token after an error, discard it.  */

      /* return failure if at end of input */
      if (yychar == YYEOF)
	YYABORT;

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Discarding token %d (%s).\n", yychar, yytname[yychar1]);
#endif

      yychar = YYEMPTY;
    }

  /* Else will try to reuse lookahead token
     after shifting the error token.  */

  yyerrstatus = 3;		/* Each real token shifted decrements this */

  goto yyerrhandle;

yyerrdefault:  /* current state does not do anything special for the error token. */

#if 0
  /* This is wrong; only states that explicitly want error tokens
     should shift them.  */
  yyn = yydefact[yystate];  /* If its default is to accept any token, ok.  Otherwise pop it.*/
  if (yyn) goto yydefault;
#endif

yyerrpop:   /* pop the current state because it cannot handle the error token */

  if (yyssp == yyss) YYABORT;
  yyvsp--;
  yystate = *--yyssp;
#ifdef YYLSP_NEEDED
  yylsp--;
#endif

#if YYDEBUG != 0
  if (yydebug)
    {
      short *ssp1 = yyss - 1;
      fprintf (stderr, "Error: state stack now");
      while (ssp1 != yyssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

yyerrhandle:

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yyerrdefault;

  yyn += YYTERROR;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != YYTERROR)
    goto yyerrdefault;

  yyn = yytable[yyn];
  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrpop;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrpop;

  if (yyn == YYFINAL)
    YYACCEPT;

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Shifting error token, ");
#endif

  *++yyvsp = yylval;
#ifdef YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  yystate = yyn;
  goto yynewstate;
}
#line 653 "msgtype.y"

     
#include <ctype.h>

yyerror(s)
     char *s;
{
    fprintf(stderr, "%s:%d '%s'\n", input_filename, input_linenum, s);
}

static FILE *ifp = stdin;

yysetup(fp, name)
     FILE *fp;
     char *name;
{
    if (fp) {
	ifp = fp;
    }
    if (input_filename) {
	clipc_free(input_filename);
    }
    if (name == 0) {
	input_filename = clipc_strdup(0, "stdin");
    }
    else {
	input_filename = clipc_strdup(0, name);
    }
    input_linenum = 1;
    getset_incl_stack(INCL_STACK_OP_PUSH, &ifp, &input_linenum,
		      &input_filename);

    cltype_init_basic_types();

    tbuf_create(APPL_BUFFER);
    tbuf_create(SECT_BUFFER);
    tbuf_create(MSG_BUFFER);
    tbuf_create(DEF_BUFFER);
    tbuf_create(DESC_BUFFER);
    tbuf_create(PROTO_BUFFER);
    tbuf_create(SIDE_BUFFER);
    tbuf_create(LISTENS_BUFFER);
    tbuf_create(REQUESTS_BUFFER);

    reflist_init();

} /* END yysetup */



#define CLTYPE_ISNAMECHAR(c) (isalpha(c) || (c) == '_' || isdigit(c))

char *lex_name(FILE *ifp, int *is_msgname)
{
    static char rbuf[1024];
    char *cp;
    int c;
    
    c = getc(ifp);
    for (cp = rbuf; CLTYPE_ISNAMECHAR(c); c = getc(ifp)) {
	if ((0 == *is_msgname) && ('_' != c) && (!isalpha(c) || isdigit(c)))
	  *is_msgname = 1;

	*cp++ = c;
    }
    *cp = 0;
    
    if (c != EOF) {
	ungetc(c, ifp);
    } else {
	c = pop_include();
	ungetc(c, ifp);
    }
    return rbuf;
}

yylex ()
{
    int c;
    char *name;
    static char *comment_buf;
    static comment_buflen;
    char *comment_cp = comment_buf;
    int is_minus = 0;
    char *strdup();

  rescan:
    /* skip white space  */
    while ((c = getc(ifp)) == ' ' || c == '\t' || c == '\n')
      if (c == '\n')
	input_linenum++;
    
    switch (c) {
      case ';':
	while ((c = getc(ifp)) != EOF && c != '\n')
	  continue;
	if (c == EOF) {
	  c = pop_include();
	  if (c == EOF) {
	      return 0;
	  }
	  ungetc(c, ifp);
	  goto rescan;
	}
	input_linenum++;
	goto rescan;

      case '/':
	c = getc(ifp);
	if (c != '/') {
	    ungetc (c, ifp);
	    c = ';';
	    break;
	}

	/* got "//" */
	c = getc(ifp);
	if (do_parse_doc_directives && (c == '-')) {
	    /* probably got a documentation directive */
	    char line[1024];
	    char *cp = line;

	    for (c = getc(ifp); ((c != EOF) && (c != '\n')); cp++) {
		*cp = c;
		c = getc(ifp);
	    }
	    *cp = '\0';
	    if ('\n' == c) {
		input_linenum++;
	    }

	    if (0 == strncmp("APPLICATION SERVICE", line, 4)) {
		return APPL_DOCTOK;
	    } else if (0 == strncmp("END APPLICATION SERVICE", line, 8)) {
		return END_APPL_DOCTOK;
	    } else if (0 == strncmp("SECTION", line, 7)) {
		return SECTION_DOCTOK;
	    } else if (0 == strncmp("END SECTION", line, 11)) {
		return END_SECTION_DOCTOK;
	    } else if (0 == strncmp("MESSAGE DEFINITION", line, 7)) {
		return MSGDEF_DOCTOK;
	    } else if (0 == strncmp("END MESSAGE DEFINITION", line, 11)) {
		return END_MSGDEF_DOCTOK;
	    } else if (0 == strncmp("DEFINITION", line, 10)) {
		return DEFDEF_DOCTOK;
	    } else if (0 == strncmp("END DEFINITION", line, 14)) {
		return END_DEFDEF_DOCTOK;
	    } else if (0 == strncmp("DESCRIPTION", line, 4)) {
		return DESCRIPTION_DOCTOK;
	    } else if (0 == strncmp("PROTOCOL", line, 8)) {
		return PROTOCOL_DOCTOK;
	    } else if (0 == strncmp("SIDE EFFECT MESSAGES", line, 11)) {
		return SIDE_EFFECT_MSGS_DOCTOK;
	    } else if (0 == strncmp("MAKES REQUESTS", line, 5)) {
		return MAKES_REQUESTS_DOCTOK;
	    } else if (0 == strncmp("LISTENS FOR", line, 7)) {
		return LISTENS_FOR_DOCTOK;
	    } else {
		/* check to see if it is a generic heading */
		for (cp = line; '\0' != *cp; cp++) {
		    if ((!isupper(*cp)) && (' ' != *cp)) {
			return 0;
		    }
		}
		yylval.strval = strdup(line);
		return GENERIC_DOCTOK;
	    }
	} /* if possible documentation directive */

	if (c != ' ')
	  ungetc (c, ifp);

	while ((c = getc(ifp)) != EOF && c != '\n') {
	    if (comment_cp+4 >= comment_buf + comment_buflen) {
		char *tmp;
		tmp = malloc(comment_buflen+1024);

		if (comment_buf)
		  free(comment_buf);
		comment_buf = tmp;
		comment_cp = comment_buf + comment_buflen;
		comment_buflen += 1024;
	    }
	    *comment_cp++ = c;
	}

	if (c == EOF) {
	  return pop_include();
        }
	input_linenum++;
	if (comment_buf) {
	    *comment_cp++ = '\n';
	    *comment_cp = 0;
	}
	    
	yylval.strval = comment_buf ? strdup(comment_buf) : 0;
	return DESC;

      case '(':
      case ')':
      case ',':
      case ':':
      case '&':
      case '=':			
	return c;

      case '-': {
	  int cc;

	  cc = getc(ifp);
	  if (isdigit(cc)) {
	      is_minus = 1;
	      c = cc;
	      break;
	  }
	  ungetc(cc, ifp);
	  return c;
      }

      case EOF:
	c = pop_include();
	if (c == EOF) {
	    return 0;
	}
	ungetc(c, ifp);
	goto rescan;
	
      default:
	break;
    }
    
    if (isdigit(c)) {
	int rval = 0;
	
	while (isdigit(c)) {
	    rval = 10 * rval + (c - '0');
	    c = getc(ifp);
	}
	if (c != EOF) {
	  ungetc(c, ifp);
        } else {
	    c = pop_include();
	    ungetc(c, ifp);
	}

	if (is_minus) {
	    rval = -rval;
	}
	yylval.intval = rval;
	return INT;
    }

    if (CLTYPE_ISNAMECHAR(c)) {
	int is_msgname = 0;
	ungetc(c, ifp);
	name = lex_name(ifp, &is_msgname);
	if (name == 0)
	  return 0;
	if (name[0] == 'T' && name[1] == '_') {
	    if (strcmp(name, "T_DCT") == 0) 
	      return T_DCT;
	    
	    if (strcmp(name, "T_DSC") == 0) 
	      return T_DSC;
	    
	    if (strcmp(name, "T_ARY") == 0) 
	      return T_ARY;
	    
	    if (strcmp(name, "T_ENU") == 0) 
	      return T_ENU;
	}

	if (0 == strcmp(name, "INCLUDE")) {
	    /* Process the include right here. */
	    char filename[1024];
	    FILE *fp;
	    char *fnp = filename;

	    /* Eat leading whitespace. */
	    while (('\n' != (c = getc(ifp))) && isspace(c)) {
		/* NULL body. */
		;
	    }
	    /* Get filename. */
	    if ('\n' == c) {
		return 0;
	    }
	    for (; !isspace(c); c = getc(ifp)) {
		*fnp = c;
		fnp++;
	    }
	    *fnp = '\0';

	    /* And eat to EOL. */
	    while ('\n' != c) {
		c = getc(ifp);
	    }

	    /* Try to open the file. */
	    fp = fopen(filename, "r");
	    if (fp == NULL) {
		fprintf(stderr, "can't open '%s' for reading\n", filename);
		perror("fopen");
		return 0;
	    }
	    getset_incl_stack(INCL_STACK_OP_PUSH, &fp, &input_linenum,
			      &input_filename);
	    input_linenum = 1;
	    input_filename = strdup(filename);
	    ifp = fp;
	    goto rescan;
	}
	yylval.strval = clipc_strdup(0, name);
	if (is_msgname)
	  return MSGNAME;
	else
	  return NAME;
    }
    goto rescan;
}

char * token_name(t)
     int t;
{
    switch (t) {
      case NAME:
	return "NAME";

      case INT:
	return "INT";

      case MSGNAME:
	return "MSGNAME";

      case T_DCT:
	return "T_DCT";
	
      case T_ARY:
	return "T_ARY";
	
      case T_DSC:
	return "T_DSC";
	
      case T_ENU:
	return "T_ENU";

      case '(':
	return "(";
	
      case ')':
	return ")";
	
      case ':':
	return ":";
	
      case '&':
	return "&";
      case '=':
	return "=";
	
      case ',':
	return ",";
	
      case '-':
	return "-";
	
      case 0:
	return "EOF";
    }	
    return "UNKNOWN";
}

print_lex_value(int rval)
{
    switch (rval) {
      case NAME:
      case MSGNAME:
	printf("(%s)\n", yylval.strval);
	break;

      case DESC:
	printf("DESCRIPTION: '%s'\n", yylval.strval);
	break;

      case INT:
	printf("(%d)\n", yylval.intval);	
	break;

      case T_DCT:
	printf("(T_DCT)\n");
	break;

      case T_ARY:
	printf("(T_ARY)\n");
	break;

      case T_ENU:
	printf("(T_ENU)\n");
	break;

      case T_DSC:
	printf("(T_DSC)\n");
	break;

      case '(': case ')':
      case ',': case ':':
      case '-': case '&': case '=':			
	printf("('%c')\n", rval);
	break;
	
      case 0:
	return 0;
	
      default:
	printf("%d: (%d)\n", rval, yylval.intval);
	break;
    }
}

    int
pop_include()
{
    FILE *old_fp;
    int old_linenum;
    char *old_filename;
    int c;

    getset_incl_stack(INCL_STACK_OP_POP, &old_fp, &old_linenum, &old_filename);
    if (0 > old_linenum) {
	/* There was nothing to pop.  We're done. */
	return EOF;
    }

    fclose(ifp);
    free(input_filename);

    ifp = old_fp;
    input_linenum = old_linenum;
    input_filename = old_filename;
    c = getc(ifp);
    return c;
}

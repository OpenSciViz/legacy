#include <stdio.h>
#include "clipc.h"
#include "cltype-private.h"

void add_defer_mtype(CLTYPE_MTYPENT mtype, char *name, char *file, int line);
int verify_deferred_types(void);
void print_defer_mtype(FILE *ofp, CLTYPE_MTYPENT mtype);

int cltype_check_verbose = 0;
int cltype_parse_verbose = 0;
int cltype_msg_names = 0;
  
void
cltype_init_basic_types()
{
    CLTYPE_TYPDEF tdp;
    struct basic_type *btp;
    static basic_type_init_done = 0;

    if (basic_type_init_done)
      return;
    
    basic_type_init_done = 1;
    for (btp = basic_types; btp->name; btp++) {
	tdp = cltype_typdef_alloc(btp->name, CLTYPE_TYPE_BASIC);
	tdp->un.basic_type = btp->type;
	cltype_typdef_add(tdp);
    }
}

int cltype_lex_only = 0;
int do_parse_doc_directives = 0;
int do_nest_appl_svc = 0;

cltype_parse_file(char *fname)
{
    FILE *ifp = 0;
    int rval;
    char *token_name();

    if (fname) {
	ifp = fopen(fname, "r");
	if (ifp == NULL) {
	    fprintf(stderr, "can't open '%s' for reading\n", fname);
	    perror("fopen");
	    return 1;
	}
    }
    yysetup(ifp, fname);
    
    if (cltype_lex_only) {
	while (1) {
	    rval = yylex();
	    if (cltype_parse_verbose)
	      printf("rval is (%d) '%s' ", rval, token_name(rval));
	    print_lex_value(rval);
	}
    }
    else {
	rval = yyparse();
	if (cltype_parse_verbose)
	  printf("rval is %d\n", rval);
    }
    verify_deferred_types();
    
    if (ifp)
      fclose(ifp);
    
    return rval;
}

CLTYPE_MSGDEF cltype_msgdef_alloc(char *msg_name, CLTYPE_MTYPENT mtypent,
				  char *comment)
{
    CLTYPE_MSGDEF rval;
    rval = (CLTYPE_MSGDEF)malloc(sizeof *rval);
    if (msg_name)
      rval->msg_name = clipc_strdup(0, msg_name);
    else
      rval->msg_name = 0;
    
    rval->refcount = 1;
    rval->mtypent = mtypent;
    rval->description = clipc_strdup(0, comment);
    return rval;
}

CLTYPE_TYPDEF cltype_typdef_alloc(char *typ_name, CLTYPE_TYPDEF_TYPE type)
{
    CLTYPE_TYPDEF rval;
    rval = (CLTYPE_TYPDEF)malloc(sizeof *rval);
    if (typ_name)
      rval->typ_name = clipc_strdup(0, typ_name);
    else
      rval->typ_name = 0;
      
    rval->type = type;
    rval->description = 0;
    memset((char *)&rval->un, 0, sizeof(rval->un));
    rval->refcount = 1;
    return rval;
}

CLTYPE_TYPDEF cltype_typdef_setname(CLTYPE_TYPDEF tdef, char *type_name,
				    char *comment)
{
    if (tdef)  {
	tdef->typ_name = clipc_strdup(0, type_name);
	tdef->description = clipc_strdup(0, comment);
    }
    return tdef;
}

char *cltype_typdef_getname(CLTYPE_TYPDEF tdef)
{
    if (tdef)  {
	return tdef->typ_name;
    }
    return NULL;
}

int cltype_typdef_is_user(CLTYPE_TYPDEF tdef)
{
    if ((tdef) && (tdef->type == CLTYPE_TYPE_BASIC)) {
	return FALSE;
    }
    return TRUE;
}

CLTYPE_MTYPENT cltype_mtypent_alloc(char *name, CLTYPE_DICTENT dictent)
{
    CLTYPE_MTYPENT mtype;

    mtype = (CLTYPE_MTYPENT) malloc(sizeof *mtype);
    mtype->next = 0;
    mtype->mtyp_name = name;
    mtype->mtyp_typdef = cltype_make_dicttype(dictent);
    return mtype;
}

CLTYPE_MTYPENT cltype_mtypent_defer_alloc(char *name, char *typename,
					  char *file, int line)
{
    CLTYPE_MTYPENT mtype;
    
    mtype = (CLTYPE_MTYPENT) malloc(sizeof *mtype);
    mtype->next = 0;
    mtype->mtyp_name = name;
    mtype->mtyp_typdef = 0;
    add_defer_mtype(mtype, typename, file, line);
    return mtype;
}

CLTYPE_MTYPENT cltype_mtyplist_add(CLTYPE_MTYPENT mtype,
				   CLTYPE_MTYPENT list)
{
    if (mtype)
      mtype->next = list;
    return mtype;
}

CLTYPE_DICTENT cltype_dictent_alloc(char *name, CLTYPE_TYPDEF typdef,
				    int is_opt)
{
    CLTYPE_DICTENT rval;
    rval = (CLTYPE_DICTENT)malloc(sizeof *rval);
    rval->key_name = name;
    rval->is_opt = is_opt;
    rval->key_typdef = typdef;
    rval->next = 0;
    rval->description = 0;    
    return rval;
}

CLTYPE_TYPDEF cltype_make_dicttype(CLTYPE_DICTENT dictent)
{
    CLTYPE_TYPDEF rval;
    rval = cltype_typdef_alloc(0, CLTYPE_TYPE_DICT);
    rval->un.dict_fields = dictent;
    return rval;    
}

CLTYPE_TYPDEF cltype_make_arraytype(CLTYPE_TYPDEF typdef)
{
    CLTYPE_TYPDEF rval;
    rval = cltype_typdef_alloc(0, CLTYPE_TYPE_ARRAY);
    rval->un.array_elmt = typdef;
    return rval;
}

CLTYPE_TYPDEF cltype_make_disctype(char *disc, CLTYPE_DICTENT dictent)
{
    CLTYPE_TYPDEF rval;
    rval = cltype_typdef_alloc(0, CLTYPE_TYPE_DISC);
    rval->un.disc.disc_fields = dictent;
    rval->un.disc.disc_name = disc;
    return rval;    
}
CLTYPE_TYPDEF cltype_make_enumtype(CLTYPE_ENUMENT list)
{
    CLTYPE_TYPDEF rval;
    rval = cltype_typdef_alloc(0, CLTYPE_TYPE_ENUM);
    rval->un.enum_fields = list;
    return rval;    
}

CLTYPE_DICTENT cltype_make_dictlist(CLTYPE_DICTENT dictent,
				     CLTYPE_DICTENT list, char *comment)
{
    if (dictent) {
	dictent->next = list;
	dictent->description = clipc_strdup(0, comment);
    }
    return dictent;
}

static enum_count = 0;
CLTYPE_ENUMENT cltype_enument_alloc(char *name, int value, int is_explicit) {
    CLTYPE_ENUMENT rval;

    rval = (CLTYPE_ENUMENT)malloc(sizeof *rval);
    rval->next = 0;
    rval->enum_name = name;
    if (is_explicit)
      enum_count = value;
    rval->enum_value = enum_count;
    rval->is_explicit = is_explicit;
    rval->description = 0;
    enum_count++;
    return rval;
}

void cltype_reset_enumcount()
{
    enum_count = 0;
}


CLTYPE_ENUMENT cltype_enument_add(CLTYPE_ENUMENT enument, CLTYPE_ENUMENT list)
{
    if (enument)
      enument->next = list;
    return enument;
}

CLTYPE_ENUMENT cltype_enument_adddesc(CLTYPE_ENUMENT enument, char *desc)
{
    if (enument)
      enument->description = clipc_strdup(0, desc);
    return enument;
}

static struct cltype_msgdef_list *msg_list = 0;
static struct cltype_typdef_list *typ_list = 0;


void cltype_msgdef_add(CLTYPE_MSGDEF msgdef)
{
    struct cltype_msgdef_list *tmp, *mlp;
    if (msgdef == 0)
      return;
    
    tmp = (struct cltype_msgdef_list *)malloc(sizeof *tmp);
    tmp->next = 0;
    msgdef->refcount++;
    tmp->msgdef = msgdef;

    if (msg_list == 0) {
	msg_list = tmp;
    }
    else {
	for (mlp = msg_list; mlp->next; mlp = mlp->next)
	  continue;
	mlp->next = tmp;
    }
}

void cltype_typdef_add(CLTYPE_TYPDEF typdef)
{
    CLTYPE_TYPDEF_LIST tmp, tlp;
    if (typdef == 0)
      return;
    
    tmp = (CLTYPE_TYPDEF_LIST)malloc(sizeof *tmp);
    tmp->next = 0;
    typdef->refcount++;
    tmp->typdef = typdef;

    if (typ_list == 0) {
	typ_list = tmp;
    }
    else {
	for (tlp = typ_list; tlp->next; tlp = tlp->next) {
	    if (tlp->typdef && tlp->typdef->typ_name
		&& !strcmp(tlp->typdef->typ_name, typdef->typ_name))
	    {
		if (tlp->typdef->type == CLTYPE_TYPE_NIL) {
		    tlp->typdef->type = typdef->type;
		    memcpy((char *)&tlp->typdef->un,
			   (char *)&typdef->un,
			   sizeof(tlp->typdef->un));
		}
		typdef->refcount--;
		free((char *)tmp);
		return;
	    }
	}
	tlp->next = tmp;
    }
}

CLTYPE_MSGDEF cltype_msgdef_findbyname(char *name)
{
    CLTYPE_MSGDEF_LIST rval;
    
    /*
     * first check for builtins, then look for user defined types, then
     * if add is set add the new type name with a nil type.
     */
    if (name == 0 || *name == 0) {
	return 0;
    }
    
    for (rval = msg_list; rval; rval = rval->next) {
	if (rval->msgdef && rval->msgdef->msg_name
	    && strcmp(rval->msgdef->msg_name, name) == 0)
	{
	    return rval->msgdef;
	}
    }
    return 0;
}

CLTYPE_TYPDEF cltype_typdef_findbyname(char *name, int add)
{
    CLTYPE_TYPDEF_LIST rval;
    CLTYPE_TYPDEF tmp;
    
    /*
     * first check for builtins, then look for user defined types, then
     * if add is set add the new type name with a nil type.
     */
    if (name == 0 || *name == 0) {
	if (add)
	  return cltype_typdef_alloc(name, CLTYPE_TYPE_NIL);
	else
	  return 0;
    }
    
    for (rval = typ_list; rval; rval = rval->next) {
	if (rval->typdef && rval->typdef->typ_name
	    && strcmp(rval->typdef->typ_name, name) == 0)
	{
	    return rval->typdef;
	}
    }
    
    if (add) {
	tmp = cltype_typdef_alloc(name, CLTYPE_TYPE_NIL);
	cltype_typdef_add(tmp);
	return tmp;
    }
    return 0;
}

/*
 * hack for defined after referenced problem with typedef list
 */

int cltype_patch_typedef_list(CLTYPE_TYPDEF tdef)
{
    CLTYPE_TYPDEF_LIST tdlp;
    CLTYPE_TYPDEF tmp;
    char *name;

    name = tdef->typ_name;
    if (name == 0 || *name == 0) {
	return 0;
    }
    
    for (tdlp = typ_list; tdlp; tdlp = tdlp->next) {
	if (tdlp->typdef && tdlp->typdef->typ_name
	    && strcmp(tdlp->typdef->typ_name, name) == 0)
	{
	    /* should assert that typdef */
	    if (tdlp->typdef->type != CLTYPE_TYPE_NIL) {
		fprintf(stderr, "ERROR: redefining '%s'\n", name);
		return 1;
	    }
	    tdlp->typdef = tdef;
	    return 0;	    
	}
    }
    return 0;
}
			      
cltype_print_typdef_list(FILE *ofp)
{
    CLTYPE_TYPDEF_LIST rval;    

    for (rval = typ_list; rval; rval = rval->next) {
	if (rval->typdef == 0
	    || rval->typdef->type == CLTYPE_TYPE_NIL
	    || rval->typdef->type == CLTYPE_TYPE_BASIC)
	{
	    continue;
	}
	cltype_print_typdef(ofp, rval->typdef);
    }
}

cltype_print_typdef(FILE *ofp, CLTYPE_TYPDEF def)
{
    char newindent[1024];
    if (def == 0) {
	fprintf(ofp, "()");
	return;
    }

    if (def->description)
      print_desc(ofp, def->description);
    
    fprintf(ofp, "%s: ",  def->typ_name);
    sprintf(newindent, "%*c  ", strlen(def->typ_name), ' ');
    cltype_print_typeref(ofp, def, newindent, 0);
    fprintf(ofp, "\n");
}

cltype_print_typeref(FILE *ofp, CLTYPE_TYPDEF def,
		    char *indent, int indent_first)
{
    char newindent[1024];
    CLTYPE_TYPDEF array_elmt;
    CLTYPE_ENUMENT enument;

    if (def == 0) {
	fprintf(ofp, "()");
	return;
    }
    
    switch (def->type) {
      case CLTYPE_TYPE_DICT:
	fprintf(ofp, "%s(T_DCT%s",
		indent_first ? indent : "",
		def->un.dict_fields ? "\n" : "");

	print_dict_fields(ofp, def->un.dict_fields, indent);
	fprintf(ofp, ")");
	break;

      case CLTYPE_TYPE_DISC:
	fprintf(ofp, "(T_DSC %s\n", def->un.disc.disc_name);
	print_dict_fields(ofp, def->un.disc.disc_fields, indent);
	fprintf(ofp, ")");
	break;
	
      case CLTYPE_TYPE_ARRAY:
	fprintf(ofp, "(T_ARY ");
	array_elmt = def->un.array_elmt;
	if (array_elmt) {
	    sprintf(newindent, "%s      ", indent);
	    if (array_elmt->typ_name && *array_elmt->typ_name) {
		fprintf(ofp, "%s", array_elmt->typ_name);
	    }
	    else {
		fprintf(ofp, "\n");
		cltype_print_typeref(ofp, array_elmt, newindent, 1);
	    }
	}
	else {
	    fprintf(ofp, "NIL");
	}
	fprintf(ofp, ")");
	break;

      case CLTYPE_TYPE_ENUM:
	fprintf(ofp, "%s(T_ENU\n", indent_first ? indent: "");
	for (enument = def->un.enum_fields; enument; enument = enument->next) {
	    fprintf(ofp, "%s     %s", indent, enument->enum_name);
	    if (enument->is_explicit)
	      fprintf(ofp, " = %d", enument->enum_value);
	    if (enument->next)
	      fprintf(ofp, ",\n");
	}
	fprintf(ofp, ")");
	break;
      default:
	fprintf(ofp, "%s", def->typ_name);
	break;
    }
}

cltype_print_msgbody(FILE *ofp, CLTYPE_TYPDEF def,
		     char *indent, int indent_first)
{
    char newindent[1024];
    CLTYPE_TYPDEF array_elmt;
    CLTYPE_ENUMENT enument;

    if (def == 0) {
	fprintf(ofp, "()");
	return;
    }
    
    switch (def->type) {
      case CLTYPE_TYPE_DICT:
	fprintf(ofp, "%s(%s",
		indent_first ? indent : "",
		def->un.dict_fields ? "\n" : "");

	print_dict_fields(ofp, def->un.dict_fields, indent);
	fprintf(ofp, "\n )");
	break;
    }
}

print_dict_fields(FILE *ofp, CLTYPE_DICTENT typfld, char *indent)
{
    char newindent[1024];
    
    for ( ; typfld; typfld = typfld->next) {
	if (typfld->description)
	  print_desc(ofp, typfld->description);

	fprintf(ofp, "%s    %c%s: ", indent,
		typfld->is_opt ? '&' : ' ', typfld->key_name);
	
	if (typfld->key_typdef) {
	    sprintf(newindent, "%s     %*c  ", indent,
		    strlen(typfld->key_name), ' ');
	    
	    if (typfld->key_typdef->typ_name && *typfld->key_typdef->typ_name)
	      fprintf(ofp, "%s", typfld->key_typdef->typ_name);
	    else
	      cltype_print_typeref(ofp, typfld->key_typdef, newindent, 0);
	}
	else {
	    fprintf(ofp, "NIL");
	}
	if (typfld->next)
	  fprintf(ofp, ",\n");
    }
}      

cltype_print_msgdef_list(FILE *ofp)
{
    CLTYPE_MSGDEF_LIST rval;    
    for (rval = msg_list; rval; rval = rval->next) {
	if (rval->msgdef == 0)
	  continue;
	
	cltype_print_msgdef(ofp, rval->msgdef);
    }
}

cltype_register_msgdef_list(CLIPC_CONN conn)
{
    CLTYPE_MSGDEF_LIST rval;    
    for (rval = msg_list; rval; rval = rval->next) {
	if (rval->msgdef == 0)
	  continue;
	
	register_clipc_listener(conn, rval->msgdef->msg_name, 1);
    }
}

cltype_print_msgdef(FILE *ofp, CLTYPE_MSGDEF mdp)
{
    CLTYPE_MTYPENT mtp;
    if (mdp == 0)
      return;


    if (mdp->description)
      print_desc(ofp, mdp->description);
    
    fprintf(ofp, "%s\n", mdp->msg_name);
    
    for (mtp = mdp->mtypent; mtp; mtp = mtp->next) {
	fprintf(ofp, " -%s ", mtp->mtyp_name);
	if (0 == mtp->mtyp_typdef) {
	    print_defer_mtype(ofp, mtp);
	} else {
	    cltype_print_msgbody(ofp, mtp->mtyp_typdef, "          ", 0);
	}
	fprintf(ofp, "\n");
    } /* for */
}

char *
name_of_msgdef(CLTYPE_MSGDEF mdp)
{
    return mdp->msg_name;
}

print_desc(FILE *ofp, char *msg)
{
    char *cp;
    
    if (msg == 0)
      return;
    fputs("// ", ofp);
    for (cp = msg; *cp; cp++) {
	fputc(*cp, ofp);
	if (*cp == '\n' && *(cp+1))
	  fputs("// ", ofp);
    }
}


/*
 * Message comparison stuff
 */

/*
 * Return TRUE if msg is valid, FALSE if it doesn't match the known
 * template.
 * Return value of !exact if template doesn't exist
 *
 * If req_opt is true, require messages to contain optional fields
 */
cltype_msg_format_ok(CLIPC_MSG msg, FILE *ofp, int exact, int req_opt)
{
    CLTYPE_MSGDEF msg_def;
    CLTYPE_MTYPENT mtp;    
    CLIPC_DICT args;
    CLIPC_MSG_DELCLASS msg_type;

    char *msg_name;
    if (msg == 0)
      return TRUE;
    
    msg_name = clipc_msg_getname(msg, 0);
    msg_type = clipc_msg_gettype(msg, 0);
    
    msg_def = cltype_msgdef_findbyname(msg_name);
    if (msg_def == 0) {
	if (ofp)
	  fprintf(ofp, "no def. for message '%s', returning !exact (%d)\n",
		  msg_name, exact);
	return !exact;
    }

    args = clipc_msg_getargs(msg, 0);
    if (args == 0) {
	if (ofp)
	  fprintf(ofp, "no args for message '%s', returning FALSE\n",
		  msg_name);
	return FALSE;
    }

    for (mtp = msg_def->mtypent; mtp; mtp = mtp->next) {
	if (msg_type == CLIPC_DELCLASS_REQUEST
	    && strcmp(mtp->mtyp_name, "request") == 0)
	{
	    return cltype_typdef_match((CLIPC_VALUE)args,
				       mtp->mtyp_typdef, ofp, req_opt);
	}
	else if (msg_type == CLIPC_DELCLASS_NOTIFY
		 && strcmp(mtp->mtyp_name, "notify") == 0)
	{
	    return cltype_typdef_match((CLIPC_VALUE)args,
				       mtp->mtyp_typdef, ofp, req_opt); 
	}
    }
    if (ofp)
      fprintf(ofp, "couldn't find message type desc. matching %d for message '%s'\n",
	      msg_type, msg_name);
    
    return FALSE;
}

cltype_typdef_match(CLIPC_VALUE value, CLTYPE_TYPDEF def,
		    FILE *ofp, int req_opt)
{
    CLIPC_TYPE valtype;
    CLIPC_ARRAY array;
    CLIPC_DICT dict;
    CLIPC_VALUE dvalue;
    CLTYPE_DICTENT tdfld;
    int i;

    if (def == 0 || def->type == CLTYPE_TYPE_NIL)
      return TRUE;
    
    valtype = clipc_val_gettype(value, 0);
    switch (valtype) {
      case CLIPC_TYPE_DICT:
	dict = clipc_val_getdict(value, 0);
	/*
	 * simple check for fields.  Don't yet worry about mutex 
	 */
	if (def->type == CLTYPE_TYPE_DICT) {
	    for (tdfld = def->un.dict_fields; tdfld; tdfld = tdfld->next) {
		dvalue = clipc_dict_getval(dict, tdfld->key_name, 0);
		if (dvalue == 0) {
		    if (req_opt || !tdfld->is_opt) {
			if (ofp)
			  fprintf(ofp, "required field name '%s' missing\n",
				  tdfld->key_name);
			return FALSE;
		    }
		    else {
			continue;
		    }
		}
		
		if (!cltype_typdef_match(dvalue, tdfld->key_typdef,
					 ofp, req_opt))
		{
		    return FALSE;
		}
	    }
	    return TRUE;
	}
	
	if (def->type == CLTYPE_TYPE_DISC) {
	    char *disc_name;
	    CLTYPE_DICTENT dep;
	    CLIPC_STATUS status;
	    
	    disc_name = clipc_dict_getstring(dict,
					     def->un.disc.disc_name, &status);
	    if (status != CLIPC_STATUS_OK) {
		if (ofp)
		  fprintf(ofp, "couldn't get disc name '%s'\n",
			  def->un.disc.disc_name);
			  
		return FALSE;
	    }

	    for (dep = def->un.disc.disc_fields; dep; dep = dep->next) {
		if (strcmp(disc_name, dep->key_name) == 0) {
		    CLIPC_DICT disc_elmt;
		    disc_elmt = clipc_dict_getdict(dict, disc_name, &status);
		    if (status != CLIPC_STATUS_OK) {
			if (ofp)
			  fprintf(ofp, "field '%s' missing from disc union.\n",
				  disc_name);
			return FALSE;
		    }
		    else {
			if (cltype_typdef_match((CLIPC_VALUE)disc_elmt,
						dep->key_typdef, ofp, req_opt))
			{
			    return TRUE;
			}
			else {
			    if (ofp)
			      fprintf(ofp, "key '%s' has bad value\n",
				      dep->key_name);
			    return FALSE;
			}
		    }
		}
	    }
	    if (ofp)
	      fprintf(ofp, "bad discriminant '%s'\n", disc_name);
	    return FALSE;
	}
	
	if (ofp)
	  fprintf(ofp, "bad type: %d, expecting DICT(%d) or DISC(%d)\n",
		  def->type, CLTYPE_TYPE_DICT, CLTYPE_TYPE_DISC);
	return FALSE;
	
      case CLIPC_TYPE_ARRAY:
	if (def->type != CLTYPE_TYPE_ARRAY) {
	    if (ofp)
	      fprintf(ofp, "key '%s' not an array\n", def->typ_name);
	    return FALSE;
	}
	
	array = clipc_val_getarray(value, 0);
	for (i = 0; i < clipc_array_size(array); i++) {
	    if (!cltype_typdef_match(clipc_array_getval(array, i, 0),
				     def->un.array_elmt, ofp, req_opt))
	    {
		return FALSE;
	    }
	}
	return TRUE;

      case CLIPC_TYPE_STRING:
      case CLIPC_TYPE_INT:	
	if (def->type == CLTYPE_TYPE_ENUM) {
	    struct cltype_enument *efld;
	    int intval;
	    char *strval;
	    CLIPC_STATUS status;
	    
	    if (valtype == CLIPC_TYPE_INT) {
		intval = clipc_val_getint(value, &status);
		if (status != CLIPC_STATUS_OK) {
		    if (ofp)
		      fprintf(ofp, "can't get intval for enumerator\n");
		    return FALSE;
		}
		
		for (efld = def->un.enum_fields; efld; efld = efld->next) {
		    if (efld->enum_value == intval)
		      return TRUE;
		}
		if (ofp)
		  fprintf(ofp, "bad int enumerator %d\n", intval);
		return FALSE;
	    }
	    else if (valtype == CLIPC_TYPE_STRING) {
		strval = clipc_val_getstring(value, &status);
		if (status != CLIPC_STATUS_OK || strval == 0) {
		    if (ofp)
		      fprintf(ofp, "can't get strval for enumerator\n");
		    return FALSE;
		}
		
		for (efld = def->un.enum_fields; efld; efld = efld->next) {
		    if (!strcmp(efld->enum_name, strval))
		      return TRUE;
		}
		if (ofp)
		  fprintf(ofp, "bad string enumerator '%s'\n", strval);
		
		return FALSE;		
	    }
	    else {
		clipc_error("neither INT nor STRING");
		return FALSE;
	    }
	}
	/* FALLTHRU */
	
      default:
	if (def->type == CLTYPE_TYPE_BASIC
	    && valtype == def->un.basic_type) {
	    return TRUE;
	}
	else {
	    if (ofp)
	      fprintf(ofp, "basic type mismatch for field '%s'\n",
		      def->typ_name);
	    return FALSE;
	}
    }
}


/*
 * deferred message type creation
 */

struct mtype_defer {
    CLTYPE_MTYPENT mtype;
    char *name;
    int line;
    char *file;
    struct mtype_defer *next;
};

static struct mtype_defer *defer_list;

void
add_defer_mtype(CLTYPE_MTYPENT mtype, char *name, char *file, int line)
{
    struct mtype_defer *tmp;

    tmp = (struct mtype_defer *)malloc(sizeof *tmp);

    tmp->next = defer_list;
    tmp->name = name ? clipc_strdup(0, name) : 0;
    tmp->mtype = mtype;
    tmp->line = line;
    tmp->file = file ? clipc_strdup(0, file) : 0;
    defer_list = tmp;
}

void
print_defer_mtype(FILE *ofp, CLTYPE_MTYPENT mtype)
{
    struct mtype_defer *tmp;

    for (tmp = defer_list; NULL != tmp; tmp = tmp->next) {
	if (tmp->mtype == mtype) {
	    if (tmp->name) {
		fprintf(ofp, tmp->name);
	    } else {
		fprintf(ofp, "()");
	    }
	    return;
	}
    } /* for */
    fprintf(ofp, "()");
}

int
verify_deferred_types(void)
{
    struct mtype_defer *tmp;
    CLTYPE_TYPDEF type;
    int errcount = 0;
    
    for (tmp = defer_list; tmp; ) {
	struct mtype_defer *very_tmp;
	
	if (tmp->mtype) {
	    type = cltype_typdef_findbyname(tmp->name, FALSE);
	    if (type == 0) {
		fprintf(stderr, "%s:%d missing type '%s'\n",
			tmp->file, tmp->line, tmp->name);
		errcount++;
	    }
	    else if (type->type != CLTYPE_TYPE_DICT) {
		fprintf(stderr, "%s:%d type '%s' not a T_DCT\n",
			tmp->file, tmp->line, tmp->name); 
		errcount++;
	    }
	    else {
		tmp->mtype->mtyp_typdef = type;
	    }
	}
	if (tmp->name)
	  free(tmp->name);
	if (tmp->file)
	  free(tmp->file);
	very_tmp = tmp->next;
	free((char *)tmp);
	tmp = very_tmp;
    }
    return errcount;
}

//-SECTION
//    *.+ CX Specific Request Messages
//
//-DESCRIPTION
// Request messages providing interfaces to functions provided only
// by the CX application service are described in this section.




//-SECTION
//    .+ Controlling the CX
//
//-DESCRIPTION
// This section contains definitions for messages which request
// operations which control the CX itself.  These include modifications
// to the internal working environment of the CX, and queries on
// the state of that environment.
//
// Functions in this section are inherently synchronous, and return
// the requested data via the reply notification messages.




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Ask CX for the value of an option.

cxcmd_printopt
 -request (
// The option name, if none given, print all options.
	& optName: STRING
;;	& optList: (T_ARY STRING) Not yet implemented!
 )		

 -notify  (
// The list of options.
	optList: (T_ARY OptNameValue),

// Execution status of the operation.
	status: (T_ENU OK=0, ERR=1),

// Human-readable string indicating error status.  Field appears
// only if status != OK.
	& errorMsg: STRING
 )	

//-END MESSAGE DEFINITION cxcmd_printopt




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Request CX to unregister for all messages it handles and quit.

cxcmd_quit
 -request (
// the value to return in process terminating message.
	& quitVal: INT
  )

 -notify (
// Not determined at time of publication.
	status: INT,

// Human-readable string indicating error status.  Field appears
// only if error status.
	& errorMsg: STRING
  )

//-END MESSAGE DEFINITION cxcmd_quit




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Ask CX to set an option.  Semantics here are exactly the same as those
// of the *Center "setopt" command with respect to setting of currently
// non-existant options and setting of existing options with values of
// a new "type."

cxcmd_setopt
 -request (
// The name of the option.
	optName: STRING,

// The value to set the specified option to.
	optValue: STRING
 )

 -notify  (
// Execution status of the operation.
	status: (T_ENU OK=0, ERR=1),

// Human-readable string indicating error status.  Field appears
// only if status != OK.
	& errorMsg: STRING
)

//-END MESSAGE DEFINITION cxcmd_setopt




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Sent when a warning or error should be suppressed or
// unsuppressed.  The possible scopes of a suppression are: Everywhere,
// File (not compilation unit), Line, Procedure, and in reference to a Name.
//
// Suppressions refer to warnings and errors by enumerator value.  The
// enumeration is not documented here.
//
// An event_suppress message is emitted for each suppression to let
// application services other than the requestor know of the new
// suppression state.
//
//-SIDE EFFECT MESSAGES
// event_suppress

cxcmd_suppress
 -request (
// The warning or error number to be suppressed or unsuppressed.
	errNumber:  INT,

// The action on the suppress: add or remove.
	suppAction: (T_ENU REMOVE=0, ADD=1),

// The actual scope of this suppression.  Redundant with the existence
// of the other fields, but this redundancy stabilizes the protocol.
       suppScope: (T_ENU GLOB=0, FILE=1, LINE=2, PROC=3, NAME=4),

// The identifier name, if a warning is to be suppressed on a name,
// or missing if not.
	& idName: STRING,

// The function name for procedure scope, or empty
// if not.
	& funcName: STRING,

// The file  for file or line scope.
        & filePath: STRING,

// The line number for line scope.
        & fileLine: INT
  )

 -notify  (
// Boolean status of command.
	status: INT,

// Human-readable string indicating error status.  Field appears
// only if status != 0.
	& errorMsg: STRING
  )

//-END MESSAGE DEFINITION cxcmd_suppress




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Request the complete list of suppressions.

cxcmd_query_suppress
 -request ()

 -notify (
// Boolean status of command.
	status: INT,

// Human-readable string indicating error status.  Field appears
// only if status != OK.
	& errorMsg: STRING,

// List of currently active suppressions.
        suppList: (T_ARY SuppItem)
  )

//-END MESSAGE DEFINITION cxcmd_query_suppress

//-END SECTION Controlling the CX




//-SECTION
//    + Loading and Building
//
//-DESCRIPTION
// This section contains definitions for messages which request
// loading and building of debugged programs in *Center.
//
// Procedures in this section are generally inherently asynchronous.
// Procedures  often announce that the side effect they were called
// to perform has been done via notification messages.  Since loads
// can take a considerable and variable amount of time, reply messages
// usually contain an indication that the request is "sensible"
// and that the CX is working on it.




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Make sure the currently loaded copies of all loaded files are
// up-to-date.  If not, re-build or re-load them as appropriate.
//
//-SIDE EFFECT MESSAGES
//    event_load_start
//    event_load_end
//    event_reload_start
//    event_reload_end
//    event_swap_start
//    event_swap_end
//    event_compile_start
//    event_compile_end
//    event_load_tick
//    event_notready
//    event_ready
//
//-PROTOCOL
//    UI requests build of CX with clipc_request_send() of
//    cxcmd_build message;
//
//    If (no files loaded) {
//
//        CX sends response notification with status, build is complete;
//
//    }
//
//    If (auto_compile is not set) {
//
//        Each loaded file is checked to see if it needs to be recompiled;
//
//        If (at least one file needs to be recompiled)
//
//            CX send response notification with a list of the
//            files which need recompilation, build is complete;
//
//        }
//
//    }
//
//    For (each item to be unloaded or loaded for the build) {
//
//        See the load command for details about what happens here.
//        The cxcmd_load_* messages are not emitted, but everything
//        else that happens in the body of the load happens here
//        as well;
//
//    } For
//
//    Upon completion of the build, CX sends response notification;
//

cxcmd_build
  -request (
// Override auto_compile not set and force the recompilation of
// all out of date object files.
	& isForceAllCompiles: INT
)

  -notify  (
	status:	(T_ENU
// Should never happen.
		UNKNOWN = 0,

// Build was successful.
		OK = 1,

// There are no loaded files, so build didn't happen.
		NOTHING_LOADED = 2,

// All loaded files are up to date, so build didn't happen.
// Currently unsupported.
		ALL_UP_TO_DATE = 3,

// At least one of the files to reload needs to
// be recompiled and auto_compile is not set.
// See the outOfDateItems array for the applicable
// files.  Build didn't happen.
		COMPILE_VERIFICATION_REQUIRED = 4,

// An error occurred during the reloading of one or more files.
		ERROR = 5
	),

// Human readable error message, if status != OK.
	& errorMsg: STRING,

// An array of files requiring recompilation verification.
// This is returned only if status == COMPILE_VERIFICATION_REQUIRED.

	& outOfDateItems: (T_ARY LoadItemStatus)
 )

//-END MESSAGE DEFINITION cxcmd_build




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Load a file or library into *Center.
//
//-SIDE EFFECT MESSAGES
//    event_load_start
//    event_load_end
//    event_reload_start
//    event_reload_end
//    event_swap_start
//    event_swap_end
//    event_compile_start
//    event_compile_end
//    event_load_tick
//    event_notready
//    event_ready
//
//-PROTOCOL
//    UI requests load of CX with clipc_request_send() of
//    cxcmd_load message;
//
//    If (there is a detectable error) {
//        CX sends response notification with status indicating the error;
//
//    } Else {
//
//        For (each item to be loaded) {
//
//            If (the item is already loaded) {
//
//                CX sends an event_reload_start notification;
//
//            } Else If (the item is already loaded and will be
//                       loaded in its other form) {
//
//                CX sends an event_swap_start notification;
//
//            } Else {
//
//                CX sends an event_load_start notification;
//            }
//
//            If (the item is out of date) {
//
//                If (a makefile exists) {
//
//                    an event_make_start notification is sent;
//                    a make is started using the makefile;
//                    make errors are reported by sending event_make_error
//                    notifications;
//                    an event_make_end notification is sent;
//
//                } Else {
//
//                    no makefile exists so use the compiler.
//                    an event_compile_start notification is sent;
//                    compilation of the item is started;
//                    compilation errors are reported by sending
//                    event_compile_error notifications;
//                    compilation warnings are reported by sending
//                    event_compile_warning notifications;
//                    an event_compile_end notification is sent;
//
//                }
//
//            } if the item is out of date
//
//            If (this is a reload context) {
//
//                CX sends an event_reload_end notification;
//
//            } Else If (this is a swap context) {
//
//                CX sends an event_swap_end notification;
//
//            } Else {
//
//                CX sends an event_load_end notification;
//            }
//
//        } for each file to be loaded
//
//        Upon completion of the loads, CX notifies with response;
//
//    } if CX is working on the request

cxcmd_load
  -request CxcmdLoadRequest

  -notify (
// An array of status values for each of the LoadItemSpecs
// given in the command request.
	itemStatus: (T_ARY LoadItemResponse),	

// Command status enumeration.
	status: CxcmdLoadResponseStatus,

// Human-readable status if status is !OK.
// Not returned if status = OK
	& errorMsg: STRING,			

// Original command request packet, in case you have
// to reissue the command.  Not returned if status = OK.
	& request: CxcmdLoadRequest
 )		

CxcmdLoadRequest: (T_DCT
// Names and other info on items to load.
	& loadItems: (T_ARY LoadItemSpec)
)

//-END MESSAGE DEFINITION cxcmd_load




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Cxcmd_load_project loads a project file; it's just like 
// sending cxcmd_load with a project file name.
// It may generate many, many notifications as side 
// effects as files are loaded, suppressions are established,
// options are set, etc.
//
//-SIDE EFFECT MESSAGES
//    event_load_project
//    event_load_start
//    event_load_end
//    event_reload_start
//    event_reload_end
//    event_swap_start
//    event_swap_end
//    event_compile_start
//    event_compile_end
//    event_load_tick
//    event_notready
//    event_ready

cxcmd_load_project
  -request (
// Name of the project file.
	projectFile: STRING
 )

  -notify  (
// Inidicates if the request is reasonable.  The notification
// returns BEFORE the entire project is loaded.
	status:	(T_ENU
		ok = 0,
		not_found = 1,
		error = 2
	),

// Human-readable error status.  Field appears only if status != ok.
	& errorMsg: STRING
 )

//-END MESSAGE DEFINITION cxcmd_load_project




//-MESSAGE DEFINITION
//
//-DESCRIPTION
//    This command is identical to cxcmd_load except that it checks
//    if any of the argument fields have changed and effects a reload
//    of the file if appropriate.  For example, if the file to be
//    reloaded is an object file and there is a change in the state
//    of includeDebugInfo, then the file will be reloaded, otherwise
//    it will not.
//
//-SIDE EFFECT MESSAGES
//    event_load_start
//    event_load_end
//    event_reload_start
//    event_reload_end
//    event_swap_start
//    event_swap_end
//    event_compile_start
//    event_compile_end
//    event_load_tick
//    event_notready
//    event_ready

cxcmd_reload
  -request CxcmdReloadRequest

  -notify (
// An array of status values for each of the LoadItemSpecs
// given in the command request.
	itemStatus: (T_ARY LoadItemResponse),	

// Command status enumeration.
	status: CxcmdLoadResponseStatus,

// Human-readable status if status is !OK.
// Not returned if status = OK.
	& errorMsg: STRING,			

// Original command request packet, in case you have
// to reissue the command.  Not returned if status = OK.
	& request: CxcmdReloadRequest
 )

CxcmdReloadRequest: (T_DCT
// Names and other info on items to load.
	& loadItems: (T_ARY LoadItemSpec)
)

//-END MESSAGE DEFINITION cxcmd_reload




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Runs the program under debug, possibly re-building it first.
// The reply message is sent back immediately if the run cannot
// be done for some reason.  If it will be attempted, the reply
// is sent back just before the run actually starts.  The reply
// does not bracket the run, since a run could take a very long
// time, particularly in the presence of breakpoints, and may
// never end.
//
//-SIDE EFFECT MESSAGES
//    event_run_start
//    event_run_end
//    event_load_start
//    event_load_end
//    event_reload_start
//    event_reload_end
//    event_swap_start
//    event_swap_end
//    event_compile_start
//    event_compile_end
//    event_load_tick
//    event_notready
//    event_ready
//
//-PROTOCOL
//    UI requests run from CX with cxcmd_run request;
//
//    If (the auto_reload option is set) {
//
//        Do the same things as the build command does;
//
//    } if a reload
//
//    if (pre-run checks fail) {
//
//        CX sends reply message with appropriate status;
//
//    } else {
//
//        CX notifies with reply message;
//
//        attempt to run at main();
//
//    }

cxcmd_run
 -request (
// Arguments to pass to program.  If the string is
// *missing*, run with existing args.  If the string
// is present but *empty* ("" as opposed to NULL),
// run with no args.
	& args: STRING,

// Iff set, Force reload of out-of-date files, no matter what
// the state of auto_reload.
	& isForceReload: INT,

// Iff set and if auto_reload, force all recompilations to happen-
// do not query if auto_compile is not set.
	& isForceAllCompiles: INT
 )

 -notify (
// Reply status for the request.
	status: (T_ENU
		
// Should never happen.
		UNKNOWN = 0,

// (Possibly built and) starting to run.
		STARTING_RUN = 1,

// At least one of the files to reload needs to
// be recompiled and auto_compile is not set.
// See the outOfDateItems array for the applicable
// files.  Neither reloaded nor run.
		COMPILE_VERIFICATION_REQUIRED = 2,

// A reload was attempted, and it failed
// for reasons other than compile verification.
// Did not run.
		RELOAD_FAILED = 3,

// No main() found, did not run.
		MAIN_NOT_FOUND = 4,

// I/O redirection of main()'s output was
// specified, and I/O can't be redirected as
// specified.  Did not run.
		CANT_REDIRECT_IO = 5
	),

// Human readable error message, if status != STARTING_RUN.
	& errorMsg: STRING,

// An array of files requiring recompilation verification.
// This is returned only if status == COMPILE_VERIFICATION_REQUIRED.
	& outOfDateItems: (T_ARY LoadItemStatus)
 )

//-END MESSAGE DEFINITION cxcmd_run




//-MESSAGE DEFINITION
//
//-DESCRIPTION
//    Cxcmd_save_project saves a project file as either an image
//    file or a project file.
//
//-SIDE EFFECT MESSAGES
//    event_save_project

cxcmd_save_project
 -request (
// Nmae of the project file.
	projectFile: STRING,

// Which format should this be saved in?
// Defaults to "project" if not defined.
	& saveAs: ProjectFileFormat
)

 -notify (
// Execution status.
	status: (T_ENU
		ok = 0,
		cannot_write = 1,
		error = 2
	),

// Human-readable error message.  Appears only if status != ok.
	& errorMsg: STRING
 )

//-END MESSAGE DEFINITION cxcmd_save_project




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Swaps a loaded source or object file, or library member for the
// other form, building it if it does not exist or is out-of-date.
//
//-SIDE EFFECT MESSAGES
//    event_run_start
//    event_load_start
//    event_load_end
//    event_reload_start
//    event_reload_end
//    event_swap_start
//    event_swap_end
//    event_compile_start
//    event_compile_end
//    event_load_tick
//    event_notready
//    event_ready
//
//-PROTOCOL
//    UI requests swap from CX with clipc_request_send() of cxcmd_swap message;
//
//    If (there is a detectable error) {
//        CX sends response notification with status indicating the error;
//
//    } Else {
//
//        For (each item to be swapped) {
//
//            CX sends an event_swap_start notification;
//
//            See the load command for details about what happens here.
//            The cxcmd_load_* messages are not emitted, but everything
//            else that happens in the body of the load happens here
//            as well;
//
//            CX sends an event_swap_end notification;
//
//        } for
//
//        Upon completion of the swap, CX notifies with response;
//
//    } if CX is working on the request

cxcmd_swap
 -request CxcmdSwapRequest

 -notify (
// An array of status values for each of the LoadItemSpecs
// given in the command request.
	itemStatus: (T_ARY LoadItemResponse),	

// Command status enumeration.
	status: CxcmdLoadResponseStatus,

// Human-readable status if status is !OK.
// Not returned if status = OK.
	& errorMsg: STRING,			

// Original command request packet, in case you have
// to reissue the command.  Not returned if status = OK.
	& request: CxcmdSwapRequest
 )		

CxcmdSwapRequest: (T_DCT
// Names and other info on items to swap.
	& swapItems: (T_ARY LoadItemSpec)
)

//-END MESSAGE DEFINITION cxcmd_swap




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Unload currently loaded files.
//
//-SIDE EFFECT MESSAGES
//    event_unload_start
//    event_unload_end
//
//-PROTOCOL
//    UI requests unload of CX with clipc_request_send() of
//    cxcmd_unload message;
//
//    If (there is a detectable error) {
//        CX sends response notification with status indicating the error;
//
//    } Else {
//
//        For (each item to be unloaded) {
//
//            CX notifies with an event_unload_start message;
//
//            After the unload of a item is complete, CX notifies with a
//            event_unload_end message;
//        }
//
//        Upon completion of the unload, CX notifies with response;
//
//    } if CX is working on the request

cxcmd_unload
 -request CxcmdUnloadRequest
					
 -notify (
// An array of status values for each of the LoadItemSpecs
// given in the command request.
	itemStatus: (T_ARY LoadItemResponse),	

// Command status enumeration.
	status: CxcmdLoadResponseStatus,

// Human-readable status if status is !OK.
// Not returned if status = OK.
	& errorMsg: STRING,			

// Original command request packet, in case you have
// to reissue the command.  Not returned if status = OK.
	& request: CxcmdUnloadRequest
 )

CxcmdUnloadRequest: (T_DCT
// Names and other info on items to unload.
	& unloadItems: (T_ARY LoadItemSpec),	

// The category of the unload.  Can be set to unload all files.
	category: (T_ENU
// When set, specifies that unloadItems specifies the
// items to unload.
		USE_UNLOAD_ITEMS = 0,

// When set, specifies that unloadFiles should be
// ignored, and all files unloaded.
		ALL = 1,
			 	
// When set, specifies that unloadFiles should be
// ignored, and user files unloaded.
		USER = 2,
			 
// When set, specifies that unloadFiles should be
// ignored, and workspace files unloaded.
		WORKSPACE = 3
	)
)

//-END MESSAGE DEFINITION cxcmd_unload

//-END SECTION Loading and Building




//-SECTION
//    + Linking
//
//-DESCRIPTION
// This section contains definitions for messages which request
// linking and operations on symbol references.
//
// Functions in this section are inherently synchronous, and return
// the requested data via the reply notification messages.




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Ask about unresolved symbols in the current CX session.
cxcmd_unres

 -request (
// Specify a function or variable to check for unresolved references.
	idName: STRING,	
 )	

  -notify (
// Identifier name used in original unres or link request.
// If empty, the original request was to link completely,
// or to list all unresolved symbols.
	symbol: IdentifierName,

// Status of the command.  0 implies success, any other
// value implies failure.
	status: INT,

// Error message, present only if status != 0.
	& errorMsg: STRING,

// List of unresolved symbols.  Appears only if there were
// some unresolved symbols.
	& unresSyms: (T_ARY UnresolvedSymbol)
  )

//-END MESSAGE DEFINITION cxcmd_unres




//-MESSAGE DEFINITION
//
//-DESCRIPTION
//    Resolve references from the loaded modules.

cxcmd_link
 -request (
// Specify a function or variable to resolve.
	idName: STRING,	

// Should the output be echoed in the workspace.  Non-zero if it should,
// non-existent, or zero if it should not.
	& echoOutput: INT  
 )

  -notify (
// Identifier name used in original unres or link request.
// If empty, the original request was to link completely,
// or to list all unresolved symbols.
	symbol: IdentifierName,

// Status of the command.  0 implies success, any other
// value implies failure.
	status: INT,

// Error message, present only if status != 0.
	& errorMsg: STRING,

// List of unresolved symbols.  Appears only if there were
// some unresolved symbols.
	& unresSyms: (T_ARY UnresolvedSymbol)
  )

//-END MESSAGE DEFINITION cxcmd_link

//-END SECTION Linking




//-SECTION
//    + CX Load State
//
//-DESCRIPTION
// This section contains definitions for messages which request
// information on, and changes to, CX's load state.  This includes
// the source, object, and library files it has loaded, and information
// about the contents of them.
//
// Functions in this section are inherently synchronous, and return
// the requested data via the reply notification messages.




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Returns information about which source, object, library, and
// (possibly) library member modules are currently loaded.

cxcmd_env_contents
 - request (
// If set, send library member modules.  Usually they aren't very interesting,
// and just take up a lot of space and bandwidth.
	& showLibMembers: Bool
)

 - notify (
// The array of module headers, if a single module was asked for, the
// length of modHeaderList is one, and there will be more information.
	modHeaderList: (T_ARY ModHeader)
)

//-END MESSAGE DEFINITION cxcmd_env_contents




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Returns information on the "project".

cxcmd_get_projname
 -request (
// Boolean: echo this command to the workspace?
	cxcmdEchoCommand: INT
  )
 
 -notify (
// Name of the project file if it is known, or "<unknown>"
// if it is not.
	projectFile: STRING,

// Enumeration.  Values not determined at time of publication.
	loadAs: INT,

// Boolean: Is the project known to the CX?
	projectIsKnown: INT,

// Command execution status.  0 = OK.
	status: INT
  )




//-END MESSAGE DEFINITION cxcmd_mod_contents

//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Returns information on the contents of a loaded module.

cxcmd_mod_contents
 -request (
// Either a file name or libfoo.a(bar.o) or -lfoo(bar.o).  In the case
// of a file name, this may be a C++ template instantiation file, in
// which case either the visible or the internal name should be provided
// in modName.
// 
// To query for the list of loaded modules (i.e., the environment contents), 
// use the cxcmd_env_contents request instead of cxcmd_mod_contents.
	modName: STRING
  )
 
 -notify (
// Same value(s) as modName field in request.
	modPath: FileName,

// Module number CX knows this module by.
	modNumber: INT,

// The module number of the archive that contains this module.  Missing if 
// this module is not contained in an archive.
	& archiveModNumber: INT,

// Status of the request.
	status: (T_ENU ok = 0, not_loaded = 1, error = 2),

// Human-readable string indicating error status.  Field appears
// only if status != ok.
	& errorMsg: STRING,

// If this is a library, then this field will appear, which contains
// a list of all of its member modules.
// Either libMembers will be provided, or all of the subsequent optional
// fields will be provided.
	& libMembers: (T_ARY ModLibMember),

// If this is not a library then this field will appear.  It contains
// a list of all files #included (directly or indirectly) by this module.
	& includeFiles:	(T_ARY ModIncludeFile),

// If this is not a library then this field will appear.  It contains
// a list of all data items defined by this module.
	& dataItems: (T_ARY ModContentsData),

// If this is not a library then this field will appear.  It contains
// a list of all functions defined by this module.
	& functions: (T_ARY ModContentsFunction),

;; It may make more sense to have a single list of types, with some 
;; of them distinguished with a typedef name, rather than two lists,
;; but then again, if a module typedefs an existing type, it should not
;; show up as a type defined in the module.
;;
// If this is not a library then this field will appear.  It contains
// a list of all types defined in  this module.
	& types: (T_ARY ModContentsType),

// If this is not a library then this field will appear.  It contains
// a list of all explicit typedefs in this module.
	& typedefs: (T_ARY ModContentsTypedef)
  )

//-END MESSAGE DEFINITION cxcmd_mod_contents

//-END SECTION CX Load State




//-SECTION
//    + Breakpoint Functions
//
//-DESCRIPTION
// This section contains definitions for messages which request
// CX-specific breakpointing functions.
//
// Functions in this section are inherently synchronous, and return
// the requested data via the reply notification messages.




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Ask CX to trace everywhere or in a function.
cxcmd_trace
 -request ( 
// Trace everywhere?
	& everywhere: Bool,

// If everywhere True, ignore funcName (if passed in).
// If everywhere False, trace in function.
	& funcName: STRING
 )

 -notify  (
// Boolean command status.
	& isError: INT,

// Human-readable error message.  Field appears only if isError == TRUE.
        & errorMsg: STRING
 )

//-END MESSAGE DEFINITION cxcmd_trace




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Ask CX to set a breakpoint/action at a given location.
// The first part of the request specifies the location, 
// the second part is what to do.
//
// The requester will fill in lineNumber/pathName OR funcName.
// If all are filled in CX may choose whichever it likes.
// If none are filled in it is an error that should be returned
// in the reply.
//
// If isStop is False and no actionText is specified, a no-op
// action is set.

cxcmd_action
 -request ( 
// Where to set the breakpoint.
	where: ActionLocation,

// The next boolean says whether it is an action.  If this
// field is ommitted or FALSE then it is a stop.
	& isAction: Bool,

// If the isStop is False, this next field is the body of the action.
        & actionText : STRING
 )

 -notify  ( 
// Boolean command status.
	& isError: INT,

// Human-readable error message.  Field appears only if isError == TRUE.
        & errorMsg: STRING
 )

ActionLocation: (T_DSC locType
// A Function name, OR
	funcName: STRING,

// A line in a source file.
	fileLoc: (T_DCT

// The file name.
		pathName: STRING,

// The line number.
		lineNumber: INT
	)
)

//-END MESSAGE DEFINITION cxcmd_action




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Ask CX to set a breakpoint/action at a memory location.
// The first part of the request specifies the location, 
// the second part is what to do.
//
// The requester will fill in either an expression OR a lowMem/highMem pair.
// If all are filled in CX may choose whichever it likes.
// If none are filled in it is an error that should be returned
// in the reply.
//
// If isStop is False and no actionText is specified, a no-op
// action is set.

cxcmd_watchpoint
 -request ( 
// Where to stop.
	where: WatchLocation,

// The next boolean says whether it is a stop or action.
	isStop: Bool,

// If the isStop is False, this next field is the body of the action.
        & actionText: STRING,
 )

 -notify  ( 
// Boolean command status.
	& isError: INT,

// Human-readable error message.  Field appears only if isError == TRUE.
        & errorMsg: STRING
 )

WatchLocation: (T_DSC locType
// Not determined at time of publication.
	expression: STRING,

// Not determined at time of publication.
        addr: INT,

// Not determined at time of publication.
        range: (T_DCT
		low: INT,
		high : INT
	)
)

//-END MESSAGE DEFINITION cxcmd_watchpoint

//-END SECTION Breakpoint Functions




//-SECTION
//     + Miscellaneous query functions
//
//-DESCRIPTION
// This section contains definitions for messages wich request
// miscellaneous information about CX.




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// A request to the CX to edit an object.

cxcmd_edit
-request (
// Name of the object to edit.
	idName: STRING
)

-notify (
// Status indication for the command.  0 = OK.
	status: INT,

// Human-readable status if status is !OK.
// Not returned if status = OK.
	& errMsg: STRING
)

//-END MESSAGE DEFINITION cxcmd_edit




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// A request to the CX to list an object.

cxcmd_list
-request (
// Name of the object to list.
	idName: STRING
)

-notify (
// Status indication for the command.  0 = OK.
	status: INT,

// Human-readable status if status is !OK.
// Not returned if status = OK.
	& errMsg: STRING
)

//-END MESSAGE DEFINITION cxcmd_list




//-MESSAGE DEFINITION
//
//-DESCRIPTION
//

cxcmd_xref_query
-request (
// The name of the symbol for which xref information is being requested.
  symbolName: STRING
)

-notify (
// Status indication for the command.  0 = OK.
	status: INT,

// Human-readable status if status is !OK.
// Not returned if status = OK.
	& errorMsg: STRING,

// The name and type of the symbol, as given in the request
	symbol: RefSym,

// List of symbols that refer to this symbol.
	referencesFrom: (T_ARY XrefSymDesc),

// List of symbols that are referred to by this symbol.
	referencesTo: (T_ARY XrefSymDesc)
)

XrefSymDesc: (T_DCT
// Type of the symbol.
	symbolType: ExprType,

// Name of the symbol.
	symbol: IdentifierName,

// Boolean: set if the symbol is static.
	& isStatic: INT,

// Boolean: set if the symbol is currently undefined.
	& isUndef: INT,

// Boolean: set if the symbol is currently unresolved.
	& isUnres: INT,

// Boolean: set if the symbol is reference is implied.
	& isImplied: INT
)

//-END MESSAGE DEFINITION cxcmd_xref_query




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// This requests the hostname on which CX is running and the pid of
// CX.  This is intended to be used for signal management.

executive_pid
-request (
)

-notify (
// hostname on which CX is running
	hostname : STRING,

// process-id of CX
	pid : INT
)

//-END MESSAGE DEFINITION executive_pid

//-END SECTION Miscellaneous query functions


//-SECTION
//    + C++  Functions
//
//-DESCRIPTION
// This section contains definitions for messages which request
// operations to support C++ in the CX.
//
// Functions in this section are inherently synchronous, and return
// the requested data via the reply notification messages.




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Implements the "which" command.  Given a string which specifies
// a function, and an indication of file scope in which the function
// is referenced, returns a string containing the definition for the
// function which would actually be invoked by the reference.
// Returns an error indication if a single specification can't be found,
// or if the function specification is ambiguous.

cxcmd_which
 -request (
// Specifies name of function we are interested in.
	selectedText: STRING,

// The name of the file in which the function reference appears.
// If this string is empty, ignore the fileLine field too and
// assume that selectedText contains enough information.
        filePath: STRING,

// The number of the line in the file on which the reference appears.
// Ignored if filePath is empty.
        fileLine: INT
 )

 -notify (
// Indicates the definition of the function that would be invoked.
// This field is not valid if status != OK.
	name: IdentifierName,

// Status indication for the command.
	status: (T_ENU OK = 0, NOT_FOUND = 1),

// Human-readable status if status is !OK.
// Not returned if status = OK.
	& errorMsg: STRING
 )
      
//-END MESSAGE DEFINITION cxcmd_which




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Request an expansion of some C++ text.  Since the expansion 
// is dependant on context in the file, the filename/line-char 
// start and line-char end are needed.

cxcmd_expand
 -request (
// The text to be expanded.
	selectedText: STRING,

// The name of the file in which the text appears.  If this string
// is empty, ignore the fileLine field too and assume that selectedText
// contains enough information.
        filePath: STRING,

// The number of the line in the file representing the first line
// of the text.  Ignored if filePath is empty.
        fileLine: INT
 )

 -notify (
// C++ CX sends back what the code above expands to.  If there 
// are multiple lines, they are delimited with \n.
	expandedCode: STRING,

// Status indication for the command.
	status: (T_ENU OK = 0, NOT_FOUND = 1),

// Human-readable status if status is !OK.
// Not returned if status = OK.
	& errorMsg: STRING
 )
      
//-END MESSAGE DEFINITION cxcmd_expand




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Return a list of all classes, for browser selection.

cxcmd_list_classes
 -request ()
 
 -notify (
// Class names contain both the name to be displayed, and the name which
// the executive will recognize.
	classIdList: (T_ARY
		(T_DCT
			class: IdentifierName,

			baseClasses: (T_ARY IdentifierName),

			derivedClasses: (T_ARY IdentifierName)
		)
	)
 )

//-END MESSAGE DEFINITION cxcmd_list_classes




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Return information on a specified class.

cxcmd_browse_class
 -request (
// Class on which to return info.
	classIdent: IdentifierName,

// Include inherited members?
        isIncludeInherited: Bool,

// Show the inheritance path?
        isShowInheritance: Bool
 )

 -notify (
// Class on which to return info.
	classIdent: IdentifierName,

// Are inherited members included in the list?
        isIncludeInherited: Bool,

// Are inheritance paths included in the list?
        isShowInheritance: Bool,

// File Name of the Class Definition The combination of filename/line
// will be used for edit/list. They will not otherwise be visible to the
// user.
        file: FileName,

// Starting line Number of the Class Definition.
 	line: INT,

// List of base classess for this class.
        baseClassList: (T_ARY QualifiedName),

// List of derived classess for this class.
        derivedClassList: (T_ARY QualifiedName),

// List of data members.
        dataMemberList: (T_ARY QualifiedName),

// List of all member functions.
        memberFuncList: (T_ARY
		(T_DCT
// The printable name here will include the return type as well as the
// argument types.
			qualifiedName: QualifiedName,

// Source file Name of the Function Definition.
// The combination of filename/line will be used for edit/list.
// They will not otherwise be visible to the user.
			file: FileName,

// Starting line Number of the Function Definition.
			line: INT
		)
	)
)

QualifiedName: (T_DCT
// Name.
	name: IdentifierName,

// Is it class static?
	isClassStatic: Bool,
	
// is it a virtual function?
	isVirtual: Bool,

// Is it inline?
	isInline: Bool,

// Is it a constant?
	isConstant: Bool,

// What is its protection level?
	protection: ProtectionLevel
)

//-END MESSAGE DEFINITION cxcmd_browse_class




; cxcmd_browse_base
; -request (
;	    )
; -notify  (
;	    )
; cxcmd_browse_data_members
; -request (
;	    )
; -notify  (
;	    )
; cxcmd_browse_derived
; -request (
;	    )
; -notify  (
;	    )
; cxcmd_browse_friends
; -request (
;	    )
; -notify  (
;	    )
; cxcmd_browse_member_functions
; -request (
;	    )
; -notify  (
;	    )
; cxcmd_classinfo
; -request (
;	    )
; -notify  (
;	    )
; cxcmd_option_expl
; -request (
;	    )
; -notify  (
;	    )

//-END SECTION C++ Functions

//-END SECTION CX Specific Request Messages

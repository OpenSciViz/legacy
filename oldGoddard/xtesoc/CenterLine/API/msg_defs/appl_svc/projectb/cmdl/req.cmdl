//-SECTION
//    *.+ Project Browser Request Messages
//
//-DESCRIPTION
// The Browser application services don't define much of a clipc message
// based interface as they are user interfaces which generally consume
// interfaces exported by the active components of a session.  They do
// respond to the standard application service requests, as well as the
// following specific messages.




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// The project browser handles this request to show to the user
// the contents of a specific module.  This request is generally
// made by the CX to the project browser, to handle the case where
// a contents commnad is typed in the workspace but the module's
// contents should be displayed by the project browser.

show_mod_contents
 -request (
	modName   :	FileName,
	modNumber :     INT,
// The module number of the archive that contains this module.  Missing if 
// this module is not contained in an archive
        &archiveModNumber : INT,

// either libMembers will be provided, or all of the other optional
// fields will be provided.

	&libMembers:	(T_ARY ModLibMember),

	&includeFiles:	(T_ARY ModIncludeFile),
	&dataItems:	(T_ARY ModContentsData),
	&functions:	(T_ARY ModContentsFunction),
;;
;; It may make more sense to have a single list of types, with some 
;; of them distinguished with a typedef name, rather than two lists,
;; but then again, if a module typedefs an existing type, it should not
;; show up as a type defined in the module.
;;
// The types defined in this module
	&types:		(T_ARY ModContentsType),
// Explicit typedefs in this module
	&typedefs:	(T_ARY ModContentsTypedef)
  )

 -notify (
	status    :     (T_ENU ok = 0, not_loaded = 1, error = 2, 
			       unknown_request = 127, malformed_request = 128),
	&errorMsg  :	STRING,
  )

//-END MESSAGE DEFINITION show_mod_contents




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// The project browser handles this request to show to the user
// the contents of a specific module.  This request is generally
// made by the CX to the project browser, to handle the case where
// a contents commnad is typed in the workspace but the module's
// contents should be displayed by the project browser.

show_env_contents
 -request (
// An array of information on each of the modules currently
// loaded.  This should be included each time the CX needs
// to make sure the Project Browser's view of what is loaded
// is in synch with what it has loaded.  As an optimization,
// this can be omitted.  In this case, the request will make
// the project browser just redisplay information it already has.
	& modHeaderList: (T_ARY ModHeader)
  )

 -notify (
// Status of the request.
	status: (T_ENU ok = 0, not_loaded = 1, error = 2, 
		       unknown_request = 127, malformed_request = 128),

// Human-readable error message, returned only if status != OK.
	& errorMsg: STRING
  )


//-END MESSAGE DEFINITION show_mod_contents




//-MESSAGE DEFINITION
//
//-DESCRIPTION
// Open or close the project browser.


centerline_project_browser_winctl
  -request (
// The class of project browser to open or close.  Currently there
// is only one class, and one value for this, "project_browser".
	browserClass: STRING,

// Operation enumeration: Open = 1, Close = 0.
        operation: INT,

// Boolean: echo this command to the workspace?
        cxcmdEchoCommand: INT
  )

  -notify ()

//-END MESSAGE DEFINITION centerline_project_browser_winctl

//-END SECTION Project Browser Request Messages

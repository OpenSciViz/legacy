/*
MODULE whatis_client_demo IS (
    main
)

OVERVIEW
    This is a CLIPC demonstration program, used to show how a
    typical client for a CenterLine Executive (CX) works.
    The demo repeatedly queries for a name to check.  Once a name
    is typed in, the demo formats and sends a cxcmd_whatis request
    message to the default CLIPC session, i.e., the running
    CodeCenter instance.  CodeCenter replies with a message telling
    what it knows about the name, same as if the "whatis" command was
    typed at the workspace.

    How to Run This Program

    Bring up a CodeCenter or ObjectCenter session.  Once it is up,
    run this program in a separate window.  You may also want to
    bring up clms_monitor (also in a separate window) so you can
    watch the CLIPC message traffic.  Load some program into the
    *Center session- anything with variables or functions will do.
*/


/*
 * Copyright (c) 1992, 1993 by CenterLine Software, Inc., Cambridge, MA.  All
 * rights reserved.   Use, duplication, or disclosure is subject to
 * restrictions described by license agreements with CenterLine Software, Inc.
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "clipc.h"
#include "clipc-machdep.h"


/* External Function Implementations */

main()
{
    CLIPC_CONN clipc_conn = 0;
    char id_name[1024];
    CLIPC_MSG request_msg;
    CLIPC_STATUS status;
    CLIPC_MSG reply_msg;
    int status_field;
    CLIPC_ARRAY use_list;
    int use_list_length;
    int count;

    /* Cnnect to the default CLIPC session. */
    clipc_conn = clipc_connection_init(0, 0, 0);
    if (clipc_conn == 0) {
	/* There is no session so ... */
	printf("couldn't get CLIPC connection\n");
	exit(1);
    }

    /* Request loop. */
    while (1) {
	/* Prompt for the name to check out. */
	printf("\nWhatis? ");
	gets(id_name);

	/* Build the request message. */
	request_msg = clipc_request_alloc("cxcmd_whatis", 0, &status);
	if (CLIPC_STATUS_OK != status) {
	    printf("Clipc internal allocation error.\n");
	    exit(1);
	}
	status = clipc_msg_addstring(request_msg, "idName", id_name);
	if (CLIPC_STATUS_OK != status) {
	    printf("Clipc internal allocation error.\n");
	    exit(1);
	}

	/*
	 * Send the message.  This function is synchronous- it does not
	 * return until a reply message is received.  Generally it is
	 * not a good idea to use such functions, as this program will
	 * hang up for as long as it takes the reply to happen.
	 * this will suffice for this simple demo, though.  The reply
	 * message is implicitly allocated by this call.
	 */
	reply_msg = clipc_request_send(clipc_conn, request_msg, &status);

	/*
	 * Extract info from the reply message and report
	 * the results to the user.  First check on the status
	 * of the request.  There is a message format convention
	 * with the cxcmd_* messages, that they will always
	 * contain a "status" field, and that it will have a
	 * value of 0 iff the request was successful.
	 */
	status_field = clipc_msg_getint(reply_msg, "status", &status);
	if (0 != status_field) {
	    /* There's an error.  Tell the user what it is. */
	    char *error_msg;

	    error_msg = clipc_msg_getstring(reply_msg, "errorMsg", &status);
	    printf("%s\n", error_msg);

	} else {
	    /* The reply returned OK.  Extract info from the reply. */
	    use_list = clipc_msg_getarray(reply_msg, "useList", &status);
	    if (CLIPC_STATUS_OK != status) {
		printf("Badly formed cxcmd_whatis reply msg received.\n");
		exit(1);
	    }
	    use_list_length = clipc_array_size(use_list);
	    for (count = 0; count < use_list_length; count++) {
		/* For each element in the list ... */
		CLIPC_DICT element;
		CLIPC_DICT symbol;
		char *visible_name;
		char *symbol_storage;
		CLIPC_DICT symbol_type;
		char *type_before;
		char *type_after;
		
		element = clipc_array_getdict(use_list, count, &status);
		if (CLIPC_STATUS_OK != status) {
		    printf("Badly formed cxcmd_whatis reply msg received.\n");
		    exit(1);
		}
		symbol = clipc_dict_getdict(element, "symbol", &status);
		if (CLIPC_STATUS_OK != status) {
		    printf("Badly formed cxcmd_whatis reply msg received.\n");
		    exit(1);
		}
		visible_name = clipc_dict_getstring(symbol, "visibleName",
						    &status);
		symbol_storage = clipc_dict_getstring(element, "symbolStorage",
						      &status);
		symbol_type = clipc_dict_getdict(element, "symbolType",
						 &status);
		if (CLIPC_STATUS_OK != status) {
		    printf("Badly formed cxcmd_whatis reply msg received.\n");
		    exit(1);
		}
		type_before = clipc_dict_getstring(symbol_type, "typeBefore",
						   &status);
		type_after = clipc_dict_getstring(symbol_type, "typeAfter",
						  &status);
		printf("%s %s %s %s\n", symbol_storage, type_before,
		       visible_name, type_after);
	    } /* for each element */
	}

	/*
	 * Now we're done with the incore messages so
	 * free them up.
	 */
	clipc_msg_free(request_msg);
	clipc_msg_free(reply_msg);

    } /* forever */

} /* END main */



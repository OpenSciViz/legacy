/*
MODULE clms_monitor IS (
    main
)

OVERVIEW
    The clms message monitor.  See clms_monitor.1 for documentation.
*/


/*
 * Copyright (c) 1992, 1993 by CenterLine Software, Inc., Cambridge, MA.  All
 * rights reserved.   Use, duplication, or disclosure is subject to
 * restrictions described by license agreements with CenterLine Software, Inc.
 */


#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include "clipc.h"


/* Local variables. */
static struct timeval start_time;
int terse = 0;


/* Local Function declarations. */
void output_msg();
void init_start_time();
char *rel_time_now();


/* External Function Implementations. */

main(int argc, char *argv[])
{
    CLIPC_CONN conn;
    CLIPC_MSG msg;
    CLIPC_STATUS status;
    extern char *optarg;
    extern int optind, opterr;
    int c;
    
    while ((c = getopt(argc, argv, "dvt")) != -1) {
	extern unsigned clipc_verbose, clipc_debug;
	
	switch (c) {
	  case 'd':
	    break;
	  case 'v':
	    break;
	  case 't':
	    terse++;
	    break;
	  default:
	    break;
	}
    }
    argc -= optind;
    argv += optind;

    /* Connect to the default CLIPC session. */
    conn = clipc_connection_init(0,0,0);

    if (conn == 0) {
	/* There is no session so ... */
	printf("couldn't get CLIPC connection\n");
	exit(1);
    }

    /* Register as a listener for messages. */
    if (argc == 0) {
	/* No command line args, so register for ALL message types. */
	clipc_register_listener(conn, "*", 1);
    }
    else {
	/* Only register for specified message types. */
	for (c = 0; c < argc; c++) {
	    clipc_register_listener(conn, argv[c], 1); 
	}

    }

    /* Display the terse header, if terse display. */
    if (terse) {
	printf("\n\n");
	printf(" Arrived   Delclass  Message Type Name          Sender\n");
	printf(" ------------------------------------------------------\n");
    }
    
    /* Display dump of each message input. */
    while (1) {
	msg = clipc_read_msg(conn, &status);
	if (status != CLIPC_STATUS_OK || msg == 0) {
	    continue;
	}
	output_msg(msg);
	clipc_msg_free(msg);
    } /* while */

} /* END main */

char *sender_name(CLIPC_MSG msg)
{
    char *name = "<unknown>";
    CLIPC_DICT envlp;
    CLIPC_STATUS status;

    envlp = clipc_msg_getenvlp(msg, &status);
    if (status != CLIPC_STATUS_OK)
      return "<unknown>";    
    name = clipc_dict_getstring(envlp, "mgr_name", &status);
    if (status != CLIPC_STATUS_OK)
      return "<unknown>";
    return name;
}

char *delclass_name(CLIPC_MSG msg)
{
    char *name = "UNKNOWN";
    
    switch (clipc_msg_getdelclass(msg, 0)) {
      default:
	name = "UNKNOWN ";
	break;
      case CLIPC_DELCLASS_NONE:
	name = "NONE    ";
	break;
      case CLIPC_DELCLASS_NOTIFY:	
	name = "NOTIFY  ";
	break;
      case CLIPC_DELCLASS_REQUEST:
	name = "REQUEST ";
	break;
      case CLIPC_DELCLASS_CMSCPREQ:
	name = "CMSCPREQ";
	break;
      case CLIPC_DELCLASS_CMSCPRPL:
	name = "CMSCPRPL";
	break;
      case CLIPC_DELCLASS_QUERY:
	name = "QUERY   ";
	break;
      case CLIPC_DELCLASS_PING:
	name = "PING    ";
	break;
    } /* switch */
    return name;
}

void
output_msg(CLIPC_MSG msg)
{
    if (terse) {
	printf("%s:  %s  %-25s\t%s\n", rel_time_now(), delclass_name(msg),
	       clipc_msg_getname(msg, 0), sender_name(msg));
    }
    else {
	clipc_msg_print(msg);
	printf("\n");
    }

}


void
init_start_time()
{
    gettimeofday(&start_time, 0);
}


char *
rel_time_now()
{
    static char buf[24];
    struct timeval now;
    int sec;
    int msec;
    
    if (start_time.tv_sec == 0)
      gettimeofday(&start_time, 0);
    gettimeofday(&now, 0);

    sec = now.tv_sec - start_time.tv_sec;
    msec = (now.tv_usec - start_time.tv_usec)/1000;
    if (msec < 0) {
	msec += 1000;
	sec -= 1;
    }
    sprintf(buf, "%4d.%03d", sec, msec);

    return buf;
}


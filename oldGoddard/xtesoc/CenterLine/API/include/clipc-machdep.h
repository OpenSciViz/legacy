/*
 * Machine independent interface to data types
 */
#ifndef WIN32
 /* UNPORTED: Unused dirent stuff */
#if NEED_STRUCT_DIRECT
#include <sys/types.h>
#include <sys/dir.h>
typedef struct direct * CLIPC_DIR;
#define CLIPC_DIR_GETNAME(d) ((d)->d_name)
#else /* !NEED_STRUCT_DIRECT */
#include <dirent.h>
typedef struct dirent * CLIPC_DIR;
#define CLIPC_DIR_GETNAME(d) ((d)->d_name)
#endif

#if NEED_UNION_WAIT
typedef union wait CLIPC_WAIT;
#define WEXITSTATUS(w) (((w).w_status>>8)&0xff)
#else /* !NEED_UNION_WAIT */
typedef int CLIPC_WAIT;
#endif /* !NEED_UNION_WAIT */
#endif /* WIN32 */
/*
 * Interface to machine dependent routines
 */

/*
 * System V has utsname rather than gethostname.
 * Return a pointer to a static buffer containing this hosts name.
 */
char *_clipc_gethostname(void);

/*
 * System V has getcwd rather than gethostname.
 * Return a pointer to a static buffer containing the process's
 * current directory.
 */
char *_clipc_getwd(void);

int _clipc_setnbio(int fd);
int _clipc_unsetnbio(int fd);
void _clipc_set_fdflags(int fd, int flags);

/*
 * Only win32 has a closesocket function
 */
#ifndef WIN32
#define closesocket(x) close(x)
#endif

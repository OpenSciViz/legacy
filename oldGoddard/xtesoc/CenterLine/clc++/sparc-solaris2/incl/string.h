/*ident	"@(#)cls4:incl-master/proto-headers/string.h	1.1" */

#ifndef __STRING_H
#define __STRING_H
extern "C++" {
#define strcpy ______strcpy
#define strncpy ______strncpy
#define strcat ______strcat
#define strncat ______strncat
#define strchr ______strchr
#define strrchr ______strrchr
#define strpbrk ______strpbrk
#define strtok ______strtok
#define index ______index
#define rindex ______rindex
#define strdup ______strdup
#define strcmp ______strcmp
#define strncmp ______strncmp
#define strlen ______strlen
#define strspn ______strspn
#define strcspn ______strcspn
#define strcoll ______strcoll
#define strxfrm ______strxfrm
#define strcasecmp ______strcasecmp
#define strncasecmp ______strncasecmp
#define strstr ______strstr

#define memccpy ______memccpy
#define memchr ______memchr
#define memcpy ______memcpy
#define memset ______memset
#define memcmp ______memcmp


#include <cc/string.h> 

#undef rindex
#undef strcat
#undef strchr
#undef strlen
#undef strcmp
#undef strncat
#undef strdup
#undef strncmp
#undef strpbrk
#undef strrchr
#undef strcpy
#undef strtok
#undef strcspn
#undef strspn
#undef index
#undef strncpy
#undef strcoll
#undef strxfrm
#undef strcasecmp
#undef strncasecmp
#undef strstr

extern "C" {
	char *strncpy(char*, const char*, int);
	char *strncat(char*, const char*, int);
	int strncmp(const char*, const char*, int);
	int strlen(const char*);
	char *strcpy(char*, const char*);
	char *strcat(char*, const char*);
	int strcmp(const char*, const char*);
	char *strchr(const char*, int);
	char *strpbrk(const char*, const char*);
	char *strrchr(const char*, int);
	char *strtok(char*, const char*);
	int strspn(const char*, const char*);
	int strcspn(const char*, const char*);
	char *index(const char*, char);
	char *rindex(const char*, char);
	char *strdup(const char*);
	int strcoll(const char *, const char *);
	size_t strxfrm(char *, const char *, size_t);
	int strcasecmp(const char *, const char *);
	int strncasecmp(const char *, const char *, int);
	char *strstr(const char *, const char *);
}

#undef memccpy
#undef memset
#undef memcpy
#undef memchr
#undef memcmp

extern "C" {
    void* memcpy(void*, const void*, int);
    void* memccpy(void*, const void*, int, int);
    void* memchr(const void*, int, int);
    int   memcmp(const void*, const void*, int);
    void* memset(void*, int, int);
}

}
#endif

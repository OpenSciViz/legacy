/*ident	"@(#)cls4:incl-master/proto-headers/stdarg.h	1.1" */

/* stdarg.h */
/* ADAPTED FROM: */
/*	@(#)varargs.h	1.2	*/

#ifndef STDARGH
#define STDARGH
extern "C++" {

/*
	USAGE:
		f( arg-declarations ... ) {
			va_list ap;
			va_start(ap, parmN);	// parmN == last named arg
			// ...
			type arg = va_arg(ap, type);
			// ...
			va_end(ap);
		}
*/

#ifndef va_start
#if defined(__CLCC__) && defined(sparc) && !defined(__OBJECTCENTER__)
#   if !defined(_SPARC)
#     define _SPARC 1
#     define _SPARC_DEFINED_FOR_CLCC 1
#   endif
#   include <clccstdarg.h>
#   if defined(_SPARC_DEFINED_FOR_CLCC)
#     undef _SPARC
#     undef _SPARC_DEFINED_FOR_CLCC
#   endif
#else
#ifdef pyr
typedef int     va_buf[3];
typedef va_buf  *va_list;

extern "C"
{
	extern void     _vastart(va_list, char*);
	extern char     *_vaarg(va_list, int);
}

#define         va_start(ap, parmN)     {\
        va_buf  _va;\
        _vastart(ap = (va_list)_va, (char *)&parmN + sizeof parmN)
#define         va_end(ap)      }
#define         va_arg(ap, mode)        *((mode *)_vaarg(ap, sizeof (mode)))
#else
typedef char *va_list;
#define va_end(ap)
#ifdef u370
#define va_start(ap, parmN) ap =\
	(char *) ((int)&parmN + 2*sizeof(parmN) - 1 & -sizeof(parmN))
#define va_arg(ap, mode) ((mode *)(ap = \
	(char *) ((int)ap + 2*sizeof(mode) - 1 & -sizeof(mode))))[-1]
#else
#ifdef hp9000s800
#define va_start(ap, parmN) (ap = (char *)&parmN + sizeof(parmN),(void)va_arg(ap,int))
#define va_arg(ap, mode) ((mode *)(ap = (char *)((int)(ap-sizeof(mode)) & ~(sizeof(mode)-1))))[0]
#else
#if !defined(__CLCC__) && defined(sparc) && !defined(__OBJECTCENTER__)
/* This is the Sun version, if we ever return to it from clcc */
extern __builtin_va_alist;
#define va_start(ap, parmN) ap = ((char *)( &__builtin_va_alist ))
extern "C" __builtin_va_arg_incr(...);
#define va_arg(ap, mode) ((mode *)__builtin_va_arg_incr((mode *)ap))[0]
#else
#define va_start(ap, parmN) ap = (char *)( &parmN+1 )
#define va_arg(ap, mode) ((mode *)(ap += sizeof(mode)))[-1]
#endif
#endif
#endif
#endif
#endif

#endif

}
#endif

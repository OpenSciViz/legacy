#ifndef __SYS_TYPES_H
#define __SYS_TYPES_H

#define longlong_t ______longlong_t
#define u_longlong_t ______u_longlong_t
#define offset_t ______offset_t
#define diskaddr_t ______diskaddr_t

#include <cc/sys/types.h>

#undef longlong_t
#undef u_longlong_t
#undef offset_t
#undef diskaddr_t

/* used to reserve space and generate alignment */
typedef	union {
	long	l[2];
	double	d;
} longlong_t;
typedef	union {
	unsigned long	l[2];
	double		d;
} u_longlong_t;

typedef	longlong_t	offset_t;
typedef	longlong_t	diskaddr_t;

#endif

#ifndef SYSTIMEH
#define SYSTIMEH
extern "C++" {
#include <cc/sys/time.h>

extern "C" {
    int gettimeofday(struct timeval *, void *);
    int settimeofday(struct timeval *, void *);
}
}
#endif /* SYSTIMEH */

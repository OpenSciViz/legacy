#!/bin/sh 
# Copyright (c) 1986, 1993 by CenterLine Software, Inc.
set -u

  NAME='
    get_x11' # type in password data
#
  SYNOPSIS='
    get_x11'
#
# DESCRIPTION
#   Gets paths for X11 and Xm directories, and allows users to input
#   values.
#
# DIAGNOSTICS
#   Exits with nonzero status on error.
#
########

########
# Check invocaton
#
dirname=`dirname $0`
case ${INSTALL_CENTERLINE-unset} in
unset | '' )
    >&2 echo Error: $NAME "must be run by 'cladmin', not independently."
    exit 2
    ;;
 3 )
    configs_check=`/bin/ls ${CENTERLINE_TOP}/configs 2> /dev/null | tail -1`
    case ${configs_check-unset} in
     unset | '' )
        make_configs || exit
        ;;
     *          )
        writable ${CENTERLINE_TOP}/configs/.test || \
          {
          out "Unable to write to ${CENTERLINE_TOP}/configs."
          out " Please try again as superuser."
          exit 1
          }
        /bin/rm -f ${CENTERLINE_TOP}/configs/.test 2> /dev/null
        ;;
    esac
    ;;
 * )
    ;;
esac

case $# in
1 ) ;;
* ) >&2 echo Usage: $SYNOPSIS
    exit 2
    ;;
esac
########
 
########
# Initialize values and configs
#
archs_string=" "
case ${INSTALL_CENTERLINE-unset} in
 1 | 2 )
  for archos in `/bin/ls ${CENTERLINE_TOP}/vc?r?/*.19??????            \
                         ${CENTERLINE_TOP}/vc?r? 2> /dev/null |        \
                 sed 's/.*\/sprc4\.........$/sparc-sunos4/g' |         \
                 sed 's/.*\/sprc2\.........$/sparc-solaris2/g' |       \
                 sed 's/.*\/pahp8\.........$/pa-hpux8/g' |             \
                 sed 's/.*\/m88sv\.........$/m88k-svr4/g' |            \
                 sed 's/.*\/ncr3k\.........$/i486-svr4/g' |            \
                 sed '/doc/d' | sed '/bin/d' | sed '/.*\.19....../d' | \
                 sed '/:/d' | sed '/^\./d' | sort -ur`
  do
      archs_string="${archos} ${archs_string}"
  done ;;
 3 )
  for archos in `/bin/ls ${CENTERLINE_TOP}/vcpro                \
                         ${CENTERLINE_TOP}/vctrm 2> /dev/null | \
                sed '/doc/d' | sed '/bin/d'| sed '/:/d' |       \
                sed '/^\./d' | sort -ur`
  do
      archs_string="${archos} ${archs_string}"
  done ;;
esac

if [ -f /tmp/cl_vctrm_${1} ]; then
     /bin/mv /tmp/cl_vctrm_${1} /tmp/cl_vctrm_hold_${1}
     echo "${archs_string}" > /tmp/cl_vctrm_${1}
     /bin/sed '1q' /tmp/cl_vctrm_hold_${1} >> /tmp/cl_vctrm_${1}
     /bin/rm -f /tmp/cl_vctrm_hold_${1}
else 
     echo "${archs_string}" > /tmp/cl_vctrm_${1}
fi

# Reminder that these paths are for all systems that will use this installation.
echo 
echo "     You are about to be prompted for the paths of your site's X11R5"
echo "     and Motif 1.2 installations."
echo 
echo "     Please give responses that are correct for all nodes that will"
echo "     be using this installation."
echo 

#
for archos in `/bin/sed '1q' /tmp/cl_vctrm_${1}`
do
    echo

    case $archos in
     sparc-sunos4   ) ARCH_STRING="SPARC, SunOS 4.1.x"
                      X11INCDIR="/usr/include"
                      X11LIBDIR="/usr/lib"
                      XMINCDIR="/usr/include"
                      XMLIBDIR="/usr/lib"
                      RGB_TXT="/usr/lib/X11/rgb.txt"
                      UIL="/usr/bin/X11/uil"
                      ;;
     sparc-solaris2 ) ARCH_STRING="SPARC, Solaris 2.3"
                      X11INCDIR="/usr/openwin/include"
                      X11LIBDIR="/usr/openwin/lib"
                      XMINCDIR="/opt/SUNWmotif/include"
                      XMLIBDIR="/opt/SUNWmotif/lib"
                      RGB_TXT="/usr/openwin/lib/rgb.txt"
                      UIL="/opt/SUNWmotif/bin/uil"
                      ;;
     pa-hpux8       ) ARCH_STRING="HP9000s700, HP-UX 9.x"
                      X11INCDIR="/usr/include/X11R5"
                      X11LIBDIR="/usr/lib/X11R5"
                      XMINCDIR="/usr/include/Motif1.2"
                      XMLIBDIR="/usr/lib/Motif1.2"
                      RGB_TXT="/usr/lib/X11/rgb.txt"
                      UIL="/usr/bin/X11/uil"
                      ;;
     m88k-svr4      ) ARCH_STRING="Motorola Mc88000 SVR4" 
                      X11INCDIR="/usr/include"
                      X11LIBDIR="/usr/lib"
                      XMINCDIR="/usr/include"
                      XMLIBDIR="/usr/lib"
                      RGB_TXT="/usr/lib/X11/rgb.txt"
                      UIL="/usr/bin/X11/uil"
                      ;;
    esac

    X11INCDIR=`ask_string                                                  \
               "   Path to the X11R5 include directory for ${ARCH_STRING}" \
               "$X11INCDIR"`
    echo "${archos}:X11INCDIR $X11INCDIR" >> /tmp/cl_vctrm_${1}
    echo

    case $X11INCDIR in
     */include ) X11LIBDIR=`echo "$X11INCDIR" | /bin/sed 's/include$/lib/'` ;;
    esac
    X11LIBDIR=`ask_string                                              \
               "   Path to the X11R5 lib directory for ${ARCH_STRING}" \
               "$X11LIBDIR"`
    echo "${archos}:X11LIBDIR $X11LIBDIR" >> /tmp/cl_vctrm_${1}
    echo

    XMINCDIR=`ask_string                                                      \
         "   Path to the Motif 1.2 (Xm) include directory for ${ARCH_STRING}" \
         "$XMINCDIR"`
    echo "${archos}:XMINCDIR $XMINCDIR" >> /tmp/cl_vctrm_${1}
    echo

    case $XMINCDIR in
     */include ) XMLIBDIR=`echo "$XMINCDIR" | /bin/sed 's/include$/lib/'` ;;
    esac
    XMLIBDIR=`ask_string                                                       \
              "   Path to the Motif 1.2 (Xm) lib directory for ${ARCH_STRING}" \
              "$XMLIBDIR"`
    echo "${archos}:XMLIBDIR $XMLIBDIR" >> /tmp/cl_vctrm_${1}
    echo

    case $XMLIBDIR in
     */lib ) UIL=`echo "$XMLIBDIR" | /bin/sed 's/\/lib$/\/bin\/X11\/uil/'` ;;
    esac
    UIL=`ask_string "   Path of the uil binary for ${ARCH_STRING}" "${UIL}"`
    echo "${archos}:UIL $UIL" >> /tmp/cl_vctrm_${1}
    echo


#    RGB_TXT=`ask_string                                                    \
#             "   Path of the rgb.txt X11 resource file for ${ARCH_STRING}" \
#            "${X11LIBDIR}/X11/rgb.txt"`
    echo "${archos}:RGB_TXT $RGB_TXT" >> /tmp/cl_vctrm_${1}
#
#    echo

done

BASE_DIR=`/bin/sed -n '/^INSTALL/p' /tmp/cl_vctrm_${1} | sed '1q' 2> /dev/null`
case ${BASE_DIR-unset} in
 unset | '' ) BASE_DIR=`ask_string "   Path to this installation directory" \
                        "$TOP"`
              echo "INSTALL ${BASE_DIR}" >> /tmp/cl_vctrm_${1} 
              ;;
 /*         ) BASE_DIR=`/bin/sed -n '/^INSTALL/p' /tmp/cl_vctrm_${1} |      \
                        /bin/sed '1q' | awk -F':' '{ print $2 }'`
              BASE_DIR=`ask_string "   Path to this installation directory" \
                        "$BASEDIR"`
              echo "INSTALL ${BASE_DIR}" >> /tmp/cl_vctrm_${1} 
              ;;
 *          ) BASE_DIR=`ask_string "   Path to this installation directory" \
                        "$TOP"`
              echo "INSTALL ${BASE_DIR}" >> /tmp/cl_vctrm_${1} 
              ;;
esac

if [ -f ${CENTERLINE_TOP}/configs/vcenterinit ]; then
    /bin/mv ${CENTERLINE_TOP}/configs/vcenterinit \
            ${CENTERLINE_TOP}/configs/vcenterinit.old 2> /dev/null
   echo
   echo "============"
   echo
   echo "NOTICE: The existing 'CenterLine/configs/vcenterinit' file, is"
   echo "        being moved to 'CenterLine/configs/vcenterinit.old'."
   echo
   echo "============"
   echo
   sleep 5
fi

echo "${archs_string}" > ${CENTERLINE_TOP}/configs/vcenterinit 2> /dev/null
/bin/sed -n '/X11LIBDIR/p' /tmp/cl_vctrm_${1}                \
         >> ${CENTERLINE_TOP}/configs/vcenterinit 2> /dev/null
/bin/sed -n '/X11INCDIR/p' /tmp/cl_vctrm_${1}                \
         >> ${CENTERLINE_TOP}/configs/vcenterinit 2> /dev/null
/bin/sed -n '/XMLIBDIR/p' /tmp/cl_vctrm_${1}                 \
         >> ${CENTERLINE_TOP}/configs/vcenterinit 2> /dev/null
/bin/sed -n '/XMINCDIR/p' /tmp/cl_vctrm_${1}                 \
         >> ${CENTERLINE_TOP}/configs/vcenterinit 2> /dev/null
/bin/sed -n '/UIL/p' /tmp/cl_vctrm_${1}                      \
         >> ${CENTERLINE_TOP}/configs/vcenterinit 2> /dev/null
/bin/sed -n '/RGB_TXT/p' /tmp/cl_vctrm_${1}                  \
         >> ${CENTERLINE_TOP}/configs/vcenterinit 2> /dev/null
/bin/sed -n '/^INSTALL/p' /tmp/cl_vctrm_${1} | /bin/sed '1q' \
         >> ${CENTERLINE_TOP}/configs/vcenterinit 2> /dev/null

exit 0

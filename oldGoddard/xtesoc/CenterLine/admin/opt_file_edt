#!/bin/sh

PATH=/bin:/etc:/usr/bin:/usr/etc:/usr/ucb:${CENTERLINE_TOP}/admin; export PATH
. ${CENTERLINE_TOP}/admin/this_cpu

# Determine the options file's path...
optfile=${1-${CENTERLINE_TOP}/configs/license.opt}
newoptfile="${optfile}.new"
oldoptfile="${optfile}.old"
cat ${optfile} > ${newoptfile}

# Determine the license file's path...
licfile=${2-${CENTERLINE_TOP}/configs/license.dat}

quote='"'
modified=false

echo
echo
echo "   ********   CenterLine User License Reservation Editor   ********"
echo
echo "     Reservation entries added to the file named"
echo
echo "       ${optfile},"
echo
echo "     for the license file named"
echo
echo "       ${licfile}."
echo
echo "     Enter the word ${quote}quit${quote} at any prompt to quit the"
echo "     editor without modifying the license.opt file."
echo
echo " ====================================================================="
echo
if ask_yn "   Do you wish to enter reservations for User licenses now" y ; then
    echo
else
    /bin/rm -f ${newoptfile} 2> /dev/null
    exit 0
fi

for combo in `sed -n '/_u centerline/p' ${licfile} | awk '{print $2":"$6}'`
do
     product=`echo "$combo" | awk -F':' '{print $1}'`
     seats=`echo "$combo" | awk -F':' '{print $2}'`

     case $combo in
      CLC++*                ) prodname="CenterLine-C++ compiler"    ;;
      CLCC*                 ) prodname="CenterLine-C compiler"      ;;
      CenterLine-C++*       ) prodname="CenterLine-C++ environment" ;;
      CodeCenter*           ) prodname="CodeCenter"                 ;;
      ObjectCenter*         ) prodname="ObjectCenter"               ;;
      TestCenter*           ) prodname="TestCenter"                 ;;
      VCPRO*                ) prodname="ViewCenter PRO"             ;;
      VCTRM*                ) prodname="ViewCenter"                 ;;
      ReplayMultiProcess*   ) prodname="ReplayMultiProcess"         ;;          
      ReplaySingleProcess*  ) prodname="ReplaySingleProcess"        ;;
      Coverage-C+++*        ) prodname="Coverage-C++"               ;;
      Coverage-C+*          ) prodname="Coverage-C"                 ;;
     esac
     case $combo in
      *+alphaosf_u:*   ) platname="AlphaOSF"  ;;
      *+m88k_u:*       ) platname="M88k"      ;;
      *+ncr3000_u:*    ) platname="NCR"       ;;
      *+pa_u:*         ) platname="HP"        ;;
      *+powerpc_u:*    ) platname="PowerPC"   ;;
      *+solarispc_u:*  ) platname="SolarisPC" ;;
      *+sparc_u:*      ) platname="SPARC"     ;;
      *+unixware_u:*   ) platname="UNIXWARE"  ;;
      *+sgimips_u:*    ) platname="SGI"  ;;
     esac

     seatsr=0
     for rseats in `sed '/^#/d' ${optfile} | eval "sed -n '/$product/p'" \
        | awk '{print $2}'`
     do
        seatsr=`expr $seatsr + $rseats`
     done

     echo
     echo "   -------------"
     echo

     if [ $seatsr -lt $seats ]; then
         lseats=`expr $seats - $seatsr`
         echo
         echo
         echo "   $prodname:"
         echo
         if [ $lseats -eq 1 ]; then
             echo "     1 $platname User license is unreserved."
         else
             echo "     $lseats $platname User licenses are unreserved."
         fi
         echo
         if [ $seatsr -ne 0 ]; then
           echo "     Existing reservations for the $prodname,"
           echo "         $platname User licenses:"
         fi
         for reservation in \
           `sed '/^#/d' ${optfile} | eval "sed -n '/$product/p'" \
            | awk '{print $2":"$5}'`
         do
              rseats=`echo "$reservation" | awk -F':' '{print $1}'`
              ruser=`echo "$reservation" | awk -F':' '{print $2}'`
              if [ $rseats -eq 1 ]; then
                  echo "                             $ruser has 1 seat reserved."
              else
                  echo "                             $ruser has $rseats seats reserved."
              fi
         done

         echo
         echo "     RESERVATIONS to add for the $prodname,"
         echo "         $platname User licenses:"
         while [ $lseats -ne 0 ]
         do
            echo
            name=`ask_string "                       User's login name"`
            qname=`echo "$name" | tr "[A-Z]" "[a-z]" | sed 's/ //g' | sed 's/	//g'`
            if [ "$qname" = "quit" -o "$qname" = "q" ]; then
               echo
               if ask_yn "   Do you wish to terminate reserving licenses" ; then
                 /bin/rm -f ${newoptfile} 2> /dev/null
                 echo
                 echo " NOTICE: No modifications were made to the options file."
                 echo
                 exit 0
               fi
            fi
            if [ $lseats -gt 1 ]; then
                number=`ask_string "           Number of licenses to reserve"`
            else
                number=1
            fi
            qnumber=`echo "$number" | tr "[A-Z]" "[a-z]" | sed 's/ //g' | sed 's/	//g'`
            if [ "$qnumber" = "quit" -o "$qnumber" = "q" ]; then
               /bin/rm -f ${newoptfile} 2> /dev/null
               echo
               echo "   Terminating user license reservation at user's request..."
               echo
               echo
               echo " NOTICE: No modifications were made to the options file."
               echo
               exit 0
            fi
            ntest=`echo "$number" | /bin/sed -e "s/[0-9]*/0/g"`
              while [ "$ntest" != "0" ]
              do
                  echo "Please enter a number."
                  echo
                  number=`ask_string "       Number of licenses to reserve"`
                  qnumber=`echo "$number" | tr "[A-Z]" "[a-z]" | sed 's/ //g' | sed 's/	//g'`
                  if [ "$qnumber" = "quit" -o "$qnumber" = "q" ]; then
                     /bin/rm -f ${newoptfile} 2> /dev/null
                     echo
                     echo "   Terminating user license reservation at user's request..."
                     echo
                     echo
                     echo " NOTICE: No modifications were made to the options file."
                     echo
                     exit 0
                  fi
                  ntest=`echo "$number" | sed -e "s/[0-9]*/0/g"`
              done
              if [ $number -eq 0 ]; then
                echo "       No reservation made."
              elif [ $number -gt $lseats ]; then
                   echo 
                   echo "            ** No reservation made."
                   if [ $lseats -eq 1 ]; then
                       echo "            ** There is only 1 unreserved license,"
                   else
                       lseatsplus1=`expr $lseats + 1`
                       echo "            ** Enter a number less than $lseatsplus1,"
                   fi
                   echo "            ** $number excedes the number of unrserved licenses available."
              else
                   modified=true
                   lseats=`expr $lseats - $number`
                   echo "RESERVE $number $product USER $name" >> ${newoptfile}
              fi
              echo
         done

         echo
         echo
     elif [ $seatsr -gt $seats ]; then
         echo
         if [ $seats -eq 1 ]; then
             echo " WARNING: The existing reservations for the $prodname,"
             echo "          $platname User license excedes the possible 1 reservation."
         else
             echo " WARNING: The existing reservations for the $prodname,"
             echo "          $platname User licenses excedes the possible number of $seats reservations."
         fi
         echo
         sleep 2
     else
         echo
         echo "  All of the $prodname, $platname User licenses are reserved."
         echo
         sleep 2
     fi
done

if $modified ; then
    echo
    echo "  Done."
    echo
    query="
  Do you wish to write the file

    ${optfile}

  reflecting the new reservations"
    if ask_yn "${query}" y ; then
        oldreservations=`sed '/^#/d' ${optfile} | wc -l | awk '{print $1}'`
        if [ $oldreservations != 0 ]; then
          echo
          echo " =========="
          echo
          echo " NOTICE: Moving the original options file named"
          echo "         ${optfile}"
          echo "          to"
          echo "         ${oldoptfile}."
          echo
          echo " =========="
          echo
          sleep 3

          # "Insurance" against clever symlinks and permissions settings!
          exists=`/bin/ls ${oldoptfile} 2> /dev/null`
          case ${exists-unset} in
           unset | '' ) ;;  # do nothing
           /*         ) /bin/mv ${oldoptfile} /tmp/old_cl_opt_file$$ ;;
           esac
           /bin/mv ${optfile} ${oldoptfile}
        fi
        /bin/mv ${newoptfile} ${optfile}
        echo
        echo "  The new options file,"
        echo
        echo "    ${optfile}"
        echo
        echo "  has been written."
        echo
        sleep 2
    else
         /bin/rm -f ${newoptfile} 2> /dev/null
         echo
         echo " =========="
         echo
         echo " NOTICE: No modifications were made to the options file."
         echo
         echo " =========="
         echo
         sleep 2
    fi
else
      /bin/rm -f ${newoptfile} 2> /dev/null
      echo
      echo " =========="
      echo
      echo " NOTICE: No modifications were made to the options file."
      echo
      echo " =========="
      echo
      sleep 2
fi

exit 0

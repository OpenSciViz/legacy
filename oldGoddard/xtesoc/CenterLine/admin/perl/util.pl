sub dprint {
    local($arg)=@_;
    $val = eval "\$$arg";
    print "$arg = $val\n" if ( $debug ); 
};

sub adprint {
    local($arg)=@_;
    @val = eval "\@$arg";
    print "$arg = @val\n" if ( $debug );
};

sub pdebug {
    print "@_\n" if ( $debug );
};

sub aadp {
    local($arg,$index) = @_;
    $i = eval "\$$index";
    $i = $index unless ( $i ne "" );
    $val = eval "\$$arg{$i}" ;
    print "$arg\{$index} = $val\n" if ( $debug );
};

sub dp {
    local($arg)=@_;
    @val = eval "\@$arg";
    if ( scalar(@val) > 1 ) {
	print "$arg = @val\n" if ( $debug );
    } else {
	$val = eval "\$$arg";
	print "$arg = $val\n" if ( $debug );
    };
};

1

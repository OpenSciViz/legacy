
# Name: product.pl
#
# Copyright (c) 1996 CenterLine Software, Inc., Cambridge, MA. All
# rights reserved. Use, duplication, or disclosure is subject to
# restrictions described by license agreements with CenterLine Software,
# Inc.
#

#########################################################################
# This file is maintained for the sole purpose of isolating
# all hardcoded product information into one file.
#########################################################################

%global_product_dirs=(
			"octr", "oc_2.0.0",
			"cctr", "c_4.0.0",
	  		"clcxx", "clc++",
			"tctr", "testcenter",
			"replay","replay",
			"cov","coverage",
			"adv","advantage",
		        "acq","acqua",
			"acqpro","acquaprova",
		);

%global_product_name=(
                        "octr", "ObjectCenter",
                        "cctr", "CodeCenter",
                        "clcxx", "CenterLine-C++",
                        "tctr", "TestCenter",
                        "replay", "QC/Replay",
                        "cov", "QC/Coverage",
			"adv","QC/Advantage",
                        "acq","Acqua",
			"acqpro","AcquaProva",
		);

%global_product_size=(
                        "octr", 65,
                        "cctr", 55,
                        "clcxx", 50,
                        "tctr", 45,
                        "replay", 40,
                        "cov", 35,
			"adv",50,
			"acq",50,
			"acqpro",50,
		);



@global_product_arches=(
                        "sparc-sunos4",
                        "sparc-solaris2",
                        "pa-hpux8",
                        "powerpc-aix",
                        "i86pc-solaris2",
                        "i486-svr4",
                        "i486-unixware",
                        "decalpha-osf1",
                        "decalpha-unix",
                        "sgimips-irix"
                );


###########################################################################
# Converts sparc-sunos4 to sunos4
###########################################################################

sub archos_to_os {
    local($os) = @_;

    $os =~ s/sparc-sunos4/sunos4/;
    $os =~ s/sparc-solaris2/solaris2/;
    $os =~ s/i86pc-solaris2/solintel/;
    $os =~ s/i486-unixware/unixware/;
    $os =~ s/i486-svr4/ncrsvr4/;
    $os =~ s/pa-hpux8/hpux/;
    $os =~ s/powerpc-aix/poweraix/;
    $os =~ s/sgimips-irix/sgiirix/;
    $os =~ s/decalpha-osf1/decosf1/;
    $os =~ s/decalpha-unix/decunix/;
    return($os);

}

###########################################################################
# Converts sunos4 to sparc-sunos4
###########################################################################
sub os_to_archos {
    local($archos) = @_;

    $archos =~ s/sunos4/sparc-sunos4/;
    $archos =~ s/solaris2/sparc-solaris2/;
    $archos =~ s/solintel/i86pc-solaris2/;
    $archos =~ s/unixware/i486-unixware/;
    $archos =~ s/ncrsvr4/i486-svr4/;
    $archos =~ s/hpux/pa-hpux8/;
    $archos =~ s/poweraix/powerpc-aix/;
    $archos =~ s/sgiirix/sgimips-irix/;
    $archos =~ s/decosf1/decalpha-osf1/;
    $archos =~ s/decunix/decalpha-unix/;
    return($archos);

}


1;

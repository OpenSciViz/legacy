#! /bin/csh -f

# This post-install script for AcquaProva looks for system include
# files that need patching (Don't worry, the patched files are installed
# into the CenterLine install tree, not into /usr/include!)

if ( $#argv != 1 ) then
    echo "Usage: $0 /path-to-CenterLine"
    exit(1)
endif

if ( ! -d "$1" ) then
    echo "$1 is not a CenterLine install directory"
    exit(1)
endif

set topdir = "$1"
cd $topdir
set topdir = `/bin/pwd`
if ( ! -x $topdir/admin/provaplatform ) then
    echo "$topdir/admin/provaplatform is not executable"
    exit(1)
endif

set platform = `$topdir/admin/provaplatform`
set patch =    /bin/patch
set patchdir = $topdir/prova/$platform/include/CenterLine/$platform
set patchdir = "$patchdir"-"`/bin/uname -r`"

# The patchdirectory contains files of the form
#    <filename>.patch          -- contains context diff for system include file
#    <filename>.sourcepath     -- contains pathname of system include file
#   where <filename> is the name we will use for the patched output file.
#   e.g.,   time.h.patch   contains context diff for time.h
#           time.h.sourcepath  contains "/usr/include/time.h"
#           patched file will go into time.h, in $patchdir

# The following code finds all the files to patch (knowing they are named
#   as described above), and performs the patches.
if ( -d $patchdir) then
    cd $patchdir
    set nonomatch
    set patchfiles = ( *.patch )
    if ( "$patchfiles" != '*.patch' ) then
	foreach patchfile (*.patch)
	    set outputfile = $patchfile:r
	    set inputfile = "`/bin/cat $outputfile.sourcepath`"
	    /bin/rm -f $outputfile
	    /bin/cp $inputfile $outputfile
	    /bin/chmod +w $outputfile
	    $patch -i $patchfile $outputfile 
	    if ($status) then
		echo "patch failed for $patchfile"
		exit(1)
	    endif
	    /bin/chmod 444 $outputfile
	end
    endif
endif

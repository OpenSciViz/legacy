#!/bin/sh
# Copyright (c) 1991, 1995 by CenterLine Software, Inc.
# PLATFORM
########

# tr is in a different place on different systems...
if [ -f /bin/tr ]; then
     TR="/bin/tr"
elif [ -f /usr/bin/tr ]; then
     TR="/usr/bin/tr"
else
     TR="tr"
fi

##### First, find ARCH
# /bin/arch is a program that exists only on Suns.
if   [ -f /bin/uname ]; then
     case `/bin/uname -s | ${TR} "[A-Z]" "[a-z]"` in
      irix* ) echo "sgimips-irix"
              exit 0;;
      *     ) ARCH=`/bin/uname -m | ${TR} "[A-Z]" "[a-z]"` ;;
     esac
elif [ -f /bin/uname ]; then
     ARCH=`/bin/uname -m | ${TR} "[A-Z]" "[a-z]"`
elif [ -f /bin/arch ]; then
     ARCH=`/bin/arch | ${TR} "[A-Z]" "[a-z]"`
else
     >&2 echo "Unable to determine platform/operating system type."
     >&2 echo 
     >&2 echo "Aborting..."
     exit 2
fi
case ${ARCH-unset} in
 unset | ''  ) if [ -f /bin/arch ]; then ARCH=`/bin/arch` ; fi ;;
 mips | risc ) if [ -f /bin/machine ] ; then ARCH="mips-ultrix" ; fi ;;
esac

case ${ARCH-unset} in

 unset | '' )
  >&2 echo "Unable to determine system type."
  >&2 echo "Output is unavailable from either 'uname' or 'arch'."
  >&2 echo "Contact your Systems Administrator and CenterLine Support."
  exit 2
  ;;

 sun3* ) 
  echo "sun3"
  ;;

 alpha ) 
  OS=`/bin/uname -s 2> /dev/null` 
  case ${OS-unset} in
   OSF1* ) echo "decalpha-osf1"
        ;;
   unset | '' | * )
        >&2 echo "Unsupported system type: `/bin/uname -mrs`. "
        >&2 echo "Supported OS type for an DEC/Alpha is OSF1 V3.x"
        exit 2
        ;;
  esac
  ;;
 i86pc ) 
  OS=`/bin/uname -r 2> /dev/null` 
  case ${OS-unset} in
   5* ) echo "i86pc-solaris2"
        ;;
   unset | '' | * )
        >&2 echo "Unsupported system type: `/bin/uname -mrs`. "
        >&2 echo "Supported OS type for an 386/486 PC is Solaris 2.x"
        exit 2
        ;;
  esac
  ;;
 sun4* | sparc* | tp_s* ) 
  OS=`/bin/uname -r 2> /dev/null` 
  case ${OS-unset} in
   5* ) echo "sparc-solaris2"
        ;;
   4* ) echo "sparc-sunos4"
        ;;
   unset | '' | * )
        >&2 echo "Unsupported system type: `/bin/uname -mrs`. "
        >&2 echo "Supported OS type for SPARC workstations is SunOS 4.x"
        exit 2
        ;;
  esac
  ;;
    
 9000/2* | 9000/3* | 9000/4* )
  OS=`/bin/uname -r | ${TR} -d "[A-Z][a-z]" | sed 's/\.//g' | sed 's/^0//'`
  case ${OS-unset} in
   7* | 8* )
    echo "hp9000s300-70"
    ;;
   unset | '' | * )
    >&2 echo "Unsupported system type: `/bin/uname -mrs`. "
    exit 2
    ;;
  esac
  ;;

 9000/7* | 9000/8* )
  OS=`/bin/uname -r | ${TR} -d "[A-Z][a-z]" | sed 's/\.//g' | sed 's/^0//'`
  case ${OS-unset} in
   8* | 9* | 10* ) 
    echo "pa-hpux8"
    ;;
   unset | '' | * )
    >&2 echo "Unsupported system type: `/bin/uname -mrs`. "
    exit 2
    ;;
  esac
  ;;

 m88* ) 
  OS=`/bin/uname -r 2> /dev/null` 
  case ${OS-unset} in
   4* ) echo "m88k-svr4"
        ;;
   unset | '' | * )
        >&2 echo "Unsupported system type: `/bin/uname -mrs`. "
        >&2 echo "Supported OS type for SPARC workstations is SunOS 4.x"
        exit 2
        ;;
  esac
  ;;
    
 risc | mips-ultrix* )
  OS=`/bin/uname -r 2> /dev/null` 
  case ${OS-unset} in
   3* ) echo "mips-ultrix3x"
        ;;
   4* ) echo "mips-ultrix4x"
        ;;
   unset | '' | * )
        >&2 echo "Unsupported system type."
        >&2 echo "Supported OS types for MIPS based DECstations are:"
        >&2 echo "    Ultrix 3.x, Ultrix 4.x"
        exit 2
        ;;
  esac
  ;;
    
 sei | sumi | mips )
  # Sumitomo Sumistation/MIPS MIPSstation
  # currently backward compatable with UMIPS 4_51, subject to
  # change with future development, remove if incompatable.

  OS=`/bin/uname -r | ${TR} -d '.' | ${TR} -d '_' | sed 's/\(..\).*/\1/'`
  case ${OS-unset} in
   # backward compatability for UMIPS 4_51, not maintained, remove
   # if future development results in incompatability.
   451* )
    >&2 echo "Unsupported system type."
    >&2 echo "Compatable OS types for MIPS based workstations are:"
    >&2 echo "      SEIUX3.2, UMIPS4_51 and later"
    exit 2
    ;;
   32* | 45* )
    echo "mips-"
    ;;
   * )
    >&2 echo "Unsupported system type: `/bin/uname -mrs`. "
    >&2 echo "Supported OS types for this architecture are:"
    >&2 echo "      SEIUX3.2, UMIPS4_51 and later"
    exit 2
    ;;
  esac
  ;;
    
 aviion )
  OStocheck=`/bin/uname -r | ${TR} -d '.' |  sed 's/\(...\).*/\1/'`
  OS=`echo $OStocheck |  sed 's/\(..\).*/\1/'`
  case ${OS-unset} in
   431* )
    >&2 echo "Unsupported system type: `/bin/uname -mrs`. "
    >&2 echo "Supported OS types for MC88000 based Workstations are:"
    >&2 echo "	DGUX 4.31 and greater and DGUX 5.4X," 
    exit 2
    ;;
   43* | 54* )
    echo "aviion-43"
    ;;
   unset | '' | * )
    >&2 echo "Unsupported system type: `/bin/uname -mrs`. "
    >&2 echo "Supported OS types for MC88000 based Workstations are:"
    >&2 echo "	DGUX 4.31 and greater and DGUX 5.4X," 
    exit 2
    ;;
  esac
  ;;

 3* | 48* | 56* )
  PLATFORM=`/bin/uname -p | awk -F'/' '{ print $2 }'`
  case ${PLATFORM-/} in
   *486* ) echo "i486-svr4" ;;
   *     ) >&2 echo "Unsupported system type: `/bin/uname -mrs`. "
           >&2 echo "Supported OS type for 486 workstations is 4.0"
           exit 2 ;;
  esac
  ;;

 i386 | i486 )
      echo "i486-unixware" 
  ;;

 00*00 )
  ARCH=`/bin/uname -s | ${TR} "[A-Z]" "[a-z]"`
  case ${ARCH-unset} in
   aix* )
      OS=`/bin/uname -vr | awk '{ print $2$1 }'`
      case ${OS-unset} in
       32* | 41* | 42* )
        echo "powerpc-aix"
        ;;
       unset | '' | * )
        >&2 echo ""
        >&2 echo "Unsupported system type: `/bin/uname -mrs`. "
        >&2 echo "Supported OS versions for IBM RS/6000's are:"
        >&2 echo ""
        >&2 echo "          AIX 3.2.5, 4.1 and 4.2"
        >&2 echo ""
        exit 2
        ;;
      esac
      ;;
     unset | '' | * )
      >&2 echo ""
      >&2 echo "Unsupported system type: `/bin/uname -mrs`. "
      >&2 echo ""
      exit 2
      ;;
   * )
    >&2 echo ""
    >&2 echo "Unsupported system type: `/bin/uname -mrs`. "
    >&2 echo ""
    exit 2
    ;;
  esac
  ;;

 * )
  >&2 echo ""
  >&2 echo "Unsupported system type: `/bin/uname -mrs`. "
  >&2 echo ""
  exit 2
  ;;
esac
exit 0
#
########

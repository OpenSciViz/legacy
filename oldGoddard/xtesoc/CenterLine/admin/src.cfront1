#!/bin/sh
#ident	"@(#)CF.src	1.0"

#@d.19921117 # This is an age stamp, do not remove.

#
# This script merely finds the highest released BASE_NAME file for this
# particular architecture and execs it.  If someone is interested in customizing
# CC, s/he should customized the CC in the 
# CENTERLINE_DIR/CENTERLINE_PROD_VERS/CENTERLINE_ARCH_OS/bin directory.
#
# This file is derived from the install/CF.src, BASE_NAME is derived at the
# time of installation.

#####
# These are set at installation time
#
UI=-
OUI=-
CENTERLINE_PROD=c++
BASE_NAME=cfront
OBASE_NAME=cfront
PATH_TO_CENTERLINE=CenterLine/c++_1.1.0-r1.0
#
#####

#####
# Get path to this startup script.  dir=`dirname $0`, but portable:

dir=`expr \( x"$0" : x'\(/\)$' \) \| \( x"$0" : x'\(.*\)/[^/]*$' \) \| .`
DIR=`cd $dir/.. ; pwd`
#
#####

#####
# Facilitate name change (if script exists)
# This section is an abbreviated version of what happens in the objectcenter
# startup script.  It is abbreviated, to speed up the CC script.
# It only checks for old variables used in determining the path to the core
# binaries.
#
case ${CENTERLINE_SKIP_NAME_CHANGE-unset} in
    unset ) ;;

    * )     
	case ${SABER_DIR-unset} in
	unset )	;;
	* )	CENTERLINE_DIR=${CENTERLINE_DIR=${DIR}}
		export CENTERLINE_DIR 
		;;
	esac

	case ${SABERCXX_VERSION-unset} in
	unset )	;;
	* )	OBJECTCENTER_VERSION=${OBJECTCENTER_VERSION=$SABERCXX_VERSION}
		export OBJECTCENTER_VERSION
		;;
	esac

	case ${SABER_ARCH_OS-unset} in
	unset )	;;
	* )	CENTERLINE_ARCH_OS=${CENTERLINE_ARCH_OS=$SABER_ARCH_OS}
		export CENTERLINE_ARCH_OS
		;;
	esac
	;;
esac
#
###########

# Set CENTERLINE_DIR if it is not already set in the environment.

case ${CENTERLINE_DIR-unset} in
unset|"" )
    CENTERLINE_DIR_SET=
    CENTERLINE_DIR=$dir/.. ; export CENTERLINE_DIR ;;
* ) CENTERLINE_DIR_SET=1 ;;
esac

case ${CENTERLINE_ARCH_OS-unset} in
unset|"" ) # If unset, or null, then find it.
    CENTERLINE_ARCH_OS_SET=
    CENTERLINE_ARCH_OS="unknown" ; export CENTERLINE_ARCH_OS
    CENTERLINE_ARCH="unknown"
    CENTERLINE_OS="unknown"
    SUN='sun3 sun4'               # Supported sun arch ( sun386 obsolete)
    ALL_ARCH="$SUN"               # Supported archs

    ##### First, find CENTERLINE_ARCH
    # /bin/arch is a program that exists only on Suns.
    if [ -f /bin/arch ]; then CENTERLINE_ARCH=`/bin/arch` ; export CENTERLINE_ARCH
    else
        >&2 echo "Unsupported cpu type.  Supported cpu types are:"
        >&2 echo "    "$ALL_ARCH
        exit 2
    fi
    #
    #####
    #^L
    ##### Find CENTERLINE_ARCH_OS
    #  Based on what the architecture is, find out the os type
    case $CENTERLINE_ARCH in

        sun3|sun4)
            if [ -f /usr/lib/ld.so ]; then
                CENTERLINE_ARCH_OS="${CENTERLINE_ARCH}-40" ; export CENTERLINE_ARCH_OS
            else
                if [ -r /vmunix ]; then
                    >&2 echo "Unsupported OS type:"
                    >&2 strings /vmunix | grep ' Release' | awk '{ print $3 }' \
                        | tr -d '.' | sed 's/\(..\).*/\1/'
                else
                    >&2 echo "Unsupported OS type."
                fi
                >&2 echo "Supported OS types for this architecture <$CENTERLINE_ARCH> are:"
                >&2 echo '    SunOS 4.x'
                exit 2
            fi
            ;;

        *)  >&2 echo "Unsupported cpu type <$CENTERLINE_ARCH>. Supported cpu types are:"
            >&2 echo "    "$ALL_ARCH
            exit 2
            ;;
    esac
    ;;


# CENTERLINE_ARCH_OS is set
* ) CENTERLINE_ARCH_OS_SET=1 ;;
esac

#^L
########## Find OBJECTCENTER_VERSION
#
CENTERLINE_PROD_VERS=""
case ${OBJECTCENTER_VERSION-unset} in
    #####
    # This section handles default invocation
    unset|"" )
        OBJECTCENTER_VERSION=
        OBJECTCENTER_VERSION_SET=
        # The following 'for' loop does a sort (via the shell wildcard) to find
        # the highest release of a product.
        case ${CENTERLINE_DIR}/c++_*/$CENTERLINE_ARCH_OS in
                "" ) ;;
                * )
                    for prod in ${CENTERLINE_DIR}/c++_*/${CENTERLINE_ARCH_OS}
                    do
                        i=i
                    done

                    if [ -d $prod ] ; then
    			# latest=`dirname $prod`, but portable:
    			latest=`expr \( x"$prod" : x'\(/\)$' \) \| \( x"$prod" : x'\(.*\)/[^/]*$' \) \| .`
                        CENTERLINE_PROD_VERS=`basename $latest`
                        export CENTERLINE_PROD_VERS
                    fi
                    ;;
        esac
        ;;

    #####
    # If OBJECTCENTER_VERSION is set, then export it.
    * ) OBJECTCENTER_VERSION_SET=1
        CENTERLINE_PROD_VERS=c++_${OBJECTCENTER_VERSION}
        export CENTERLINE_PROD_VERS
        ;;
esac
#
##########

#####
# Don't depend on user's PATH.  Leave PATH unexported, so that the user
# program gets it unmodified.
savedPATH=${PATH}
#####
#^L

D=$CENTERLINE_DIR/$CENTERLINE_PROD_VERS/$CENTERLINE_ARCH_OS

# for backwards compatability the $D/bin/CC script depends on these...

SABER_DIR=$CENTERLINE_DIR               ; export SABER_DIR
SABER_PROD_VERS=$CENTERLINE_PROD_VERS   ; export SABER_PROD_VERS
SABER_ARCH_OS=$CENTERLINE_ARCH_OS       ; export SABER_ARCH_OS

EXEC_CMD=exec

if [ -d $D -a -d $D/bin -a -f $D/bin/$BASE_NAME ]; then
    case $# in
        0 )   $EXEC_CMD $D/bin/$BASE_NAME ;;
        * )   $EXEC_CMD $D/bin/$BASE_NAME "$@" ;;
    esac
    # Exit with the status returned from the $BASE_NAME script
    exit
fi

#^L
##########
# Test error conditions.

>&2 echo \
"
*
* Failure in startup script '$0'.
*
* NOTE:  There may be several probable reasons and solutions for this error.
*        You may wish to make a hard copy of the following errors/solutions.
*        In order to resolve your problem as quickly as possible, please try to
*        act on the solutions in the order that they are listed.

"
>&2 echo "Please hit return for more information: " ; read dummy

#####
# initialize some common messages
#####
message0="# A probable reason for this failure:
# The file you are trying to execute is a symbolic link, or copy of the
# installed startup script $BASE_NAME.
# You cannot invoke $BASE_NAME with links or copies of the $BASE_NAME 
# startup script.
# You must execute the file that was installed in the 'Saber/bin' directory.
#
# The 'host:path' where this startup script was originally installed is:
#             ${PATH_TO_CODECENTER}/bin
# Solutions:
# Include the path to the 'Saber/bin' in your PATH variable.
# Ask your administrator to execute the 'global_path' script, located in the
# ${PATH_TO_CODECENTER}/${CENTERLINE_PROD}_*/install directory.
"

message1="#\
# A probable reason for this failure:
# The variables 'CENTERLINE_DIR', 'OBJECTCENTER_VERSION' and 'CENTERLINE_ARCH_OS' in the `
# start up script '$0' are set to:
#     '$CENTERLINE_DIR'
#     '$OBJECTCENTER_VERSION'
# and
#     '$CENTERLINE_ARCH_OS'
# respectively.
# The derived directory '$D'
# does not exist.
# The architecture selected by the startup script is: $CENTERLINE_ARCH_OS
# The binaries for this machine's architecture '$CENTERLINE_ARCH_OS' may not
# be installed, or the startup script '$0'
# may have been incorrectly installed.
#
# Solution:
# Your system administator should make sure that the binaries for this machine's
# architecture '$CENTERLINE_ARCH_OS' are installed.
# Your system administator should reinstall this startup script by rexecuting
# the installation script 'RUN_ME' and selecting the menu item (3):
#    \"Extract files from distribution media and install commands\"
# Your system administrator should carefully specify a universal path that is
# good on all clients of the file server.
# Please try to execute '$0'
# after reinstalling.
"

message2="# A probable reason for this failure:
# Following, are environment variables used in the script '$BASE_NAME':
#
# Variable          Where Set            Currently Set To
# ----------------- -------------------- --------------------------------------"

message3="#
# The path to the $BASE_NAME binaries is composed of the following variables:
#                         CENTERLINE_DIR/c++_CODECENTERCXX_VERSION/CENTERLINE_ARCH_OS
# The derived directory:
#                         $D
# does not exist.
#
# You may have incorrectly set a variable in your environment.
# The 'host:path' where the Saber directory was originally installed is:
#                         ${PATH_TO_CODECENTER}
#
# Solution:
# Please respecify or unset any variables that were set in your environment
# and try again."

message4="# A probable reason for this failure:
# The directory '$D' exists,
# but is not set up correctly.
#
# It should contain 'bin/$BASE_NAME' but it doesn't.
#
# It may have been incorrectly installed.
#
# Solution:
# Your system administator should reinstall this directory by rexecuting
# the installation script 'RUN_ME' and selecting the menu item (1):
#
#    \"Extract files from media distribution and install commands\"
#
# Please try to execute '$0'
# after reinstalling."

#^L
# A more portable way to check to see if the file is a link
# test -h does not work on decstations
/bin/ls -l $0 | egrep "^l|^L" 2> /dev/null
status=$?
if [ x"$status" = x"0" ]; then
   >&2 echo "$message0"
   exit 2
fi

case $0 in
  */$BASE_NAME* ) ;;
  */bin/$BASE_NAME* ) ;;
  */Saber/bin/$BASE_NAME* ) ;;
  *) >&2 echo "$message0"
     >&2 echo "Please hit return for more information: " ; read dummy ;;
esac

if [ ! -d $D ]; then
    # if any variables are set by the user, then ask the user to retry.

    case "${CENTERLINE_DIR_SET}${OBJECTCENTER_VERSION_SET}${CENTERLINE_ARCH_OS_SET}" in

    "" ) >&2 echo "$message1" ;;
    * )  >&2 echo "$message2"
        case $CENTERLINE_DIR_SET in
        "") >&2 echo "# CENTERLINE_DIR         at run time          $CENTERLINE_DIR";;
        *)  >&2 echo "# CENTERLINE_DIR         in your environment  $CENTERLINE_DIR";;
        esac
        case $OBJECTCENTER_VERSION_SET in
        "") >&2 echo "# OBJECTCENTER_VERSION   at run time          $OBJECTCENTER_VERSI
ON";;
        *)  >&2 echo "# OBJECTCENTER_VERSION   in your environment  $OBJECTCENTER_VERSI
ON";;
        esac
        case $CENTERLINE_ARCH_OS_SET in
        "") >&2 echo "# CENTERLINE_ARCH_OS     at run time          $CENTERLINE_ARCH_OS";;
        *)  >&2 echo "# CENTERLINE_ARCH_OS     in your environment  $CENTERLINE_ARCH_OS";;
        esac
        >&2 echo "$message3"
        ;;
    esac
else
    #####
    # install error
    #####
    >&2 echo "$message4"
fi
exit 2

#
#####
#cfront1

#!/bin/sh
# Copyright (c) 1991, 1995 by CenterLine Software, Inc.
# PLATFORM
########

# tr is in a different place on different systems...
if [ -f /bin/tr ]; then
     TR="/bin/tr"
elif [ -f /usr/bin/tr ]; then
     TR="/usr/bin/tr"
else
     TR="tr"
fi

##### First, find ARCH
# /bin/arch is a program that exists only on Suns.
if   [ -f /bin/uname ]; then
     case `/bin/uname -s | ${TR} "[A-Z]" "[a-z]"` in
      irix* ) echo "sgimips-irix"
              exit 0;;
      *     ) ARCH=`/bin/uname -m | ${TR} "[A-Z]" "[a-z]"` ;;
     esac
elif [ -f /bin/uname ]; then
     ARCH=`/bin/uname -m | ${TR} "[A-Z]" "[a-z]"`
elif [ -f /bin/arch ]; then
     ARCH=`/bin/arch | ${TR} "[A-Z]" "[a-z]"`
else
     >&2 echo "Unable to determine platform/operating system type."
     >&2 echo 
     >&2 echo "Aborting..."
     exit 2
fi
case ${ARCH-unset} in
 unset | ''  ) if [ -f /bin/arch ]; then ARCH=`/bin/arch` ; fi ;;
 mips | risc ) if [ -f /bin/machine ] ; then ARCH="mips-ultrix" ; fi ;;
esac

case ${ARCH-unset} in

 unset | '' )
  >&2 echo "Unable to determine system type."
  >&2 echo "Output is unavailable from either 'uname' or 'arch'."
  >&2 echo "Contact your Systems Administrator and CenterLine Support."
  exit 2
  ;;

 alpha ) 
  OS=`/bin/uname -s 2> /dev/null` 
  case ${OS-unset} in
   OSF1* ) echo "decalpha-osf1"
        ;;
   unset | '' | * )
        >&2 echo "Unsupported system type: `/bin/uname -mrs`. "
        >&2 echo "Supported OS type for an DEC/Alpha is OSF1"
        exit 2
        ;;
  esac
  ;;
 i86pc ) 
  OS=`/bin/uname -r 2> /dev/null` 
  case ${OS-unset} in
   5* ) echo "i86pc-solaris2"
        ;;
   unset | '' | * )
        >&2 echo "Unsupported system type: `/bin/uname -mrs`. "
        >&2 echo "Supported OS type for an 386/486 PC is Solaris 2.x"
        exit 2
        ;;
  esac
  ;;
 sun4* | sparc* | tp_s* ) 
  OS=`/bin/uname -r 2> /dev/null` 
  case ${OS-unset} in
   5* ) echo "sparc-solaris2"
        ;;
   4* ) echo "sparc-sunos4"
        ;;
   unset | '' | * )
        >&2 echo "Unsupported system type: `/bin/uname -mrs`. "
        >&2 echo "Supported OS type for SPARC workstations is SunOS 4.x or Solaris 2.x"
        exit 2
        ;;
  esac
  ;;
    
 9000/7* | 9000/8* )
  OS=`/bin/uname -s | ${TR} "[A-Z]" "[a-z]"`
  case ${OS-unset} in
   hp-ux ) 
    echo "pa-hpux8"
    ;;
   unset | '' | * )
    >&2 echo "Unsupported system type: `/bin/uname -mrs`. "
    exit 2
    ;;
  esac
  ;;


 i386 | i486 )
  OS=`/bin/uname -s | ${TR} "[A-Z]" "[a-z]"`
  case ${OS-unset} in
   unix_sv ) 
    echo "i486-unixware"
    ;;
   unset | '' | * )
    >&2 echo "Unsupported system type: `/bin/uname -mrs`. "
    exit 2
    ;;
  esac
  ;;

 3* | 48* | 56* )
  OS_COMPAT=UNIXWARE
  export OS_COMPAT
  PLATFORM=`/bin/uname -p` 
  unset OS_COMPAT
  case ${PLATFORM-/} in
   *486* ) echo "i486-svr4" ;;
   *386* ) echo "i486-svr4" ;;
   *     ) >&2 echo "Unsupported system type: `/bin/uname -mrs`. "
           exit 2 ;;
  esac
  ;;

 00*00 )
  OS=`/bin/uname -s | ${TR} "[A-Z]" "[a-z]"`
  case ${OS-unset} in
   aix* )
    echo "powerpc-aix"
    ;;
   * )
    >&2 echo ""
    >&2 echo "Unsupported system type: `/bin/uname -mrs`. "
    >&2 echo ""
    exit 2
    ;;
  esac
  ;;

 * )
  >&2 echo ""
  >&2 echo "Unsupported system type: `/bin/uname -mrs`. "
  >&2 echo ""
  exit 2
  ;;
esac
exit 0
#
########

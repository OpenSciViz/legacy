/*
 * Example programs: manipulate matrices
 *
 * From the LAPACK.h++ manual
 *
 ****************************************************************
 *
 * Rogue Wave Software
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ****************************************************************
 *
 * $Log: matrix.cpp,v $
 * Revision 1.2  1993/10/15  07:47:21  alv
 * fixed incomplete last line
 *
 * Revision 1.1  1993/10/15  07:36:51  alv
 * Initial revision
 *
 *
 */

#include <rw/ftrdgmat.h>
#include <iostream.h>

main()
{
  FloatTriDiagMat T(6,6);
  T.zero();
  T.diagonal(0) = 2;
  T.diagonal(-1) = 1;
  T.diagonal(1) = 3;
  T(1,2) = 4;
  FloatVec x = "[ 2 3 9 8 3 0 ]";
  FloatVec Tx = product(T,x);
  cout << T << endl << x << endl << Tx << endl;

  return 0;
}

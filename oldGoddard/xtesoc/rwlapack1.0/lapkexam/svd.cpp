/*
 * Example program: construct singular value decomposition
 *
 * From the LAPACK.h++ manual
 *
 ****************************************************************
 *
 * Rogue Wave Software
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ****************************************************************
 *
 * $Log: svd.cpp,v $
 * Revision 1.1  1993/10/15  07:36:51  alv
 * Initial revision
 *
 *
 */

#include <iostream.h>
#include <rw/dgenmat.h>
#include <rw/dsv.h>

main()
{
  DoubleGenMat A;
  cin >> A;

  DoubleSVDecomp svd(A);

  cout << "Input matrix is " << A << endl;
  cout << "Singular values are " << svd.singularValues() << endl;
  cout << "Left singular vectors are columns of " << svd.leftVectors() << endl;
  cout << "Right singular vectors are columns of " << svd.rightVectors() << endl;

  return 0;
}


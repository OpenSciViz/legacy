/*
 * Example programs: manipulate vectors
 *
 * From the LAPACK.h++ manual
 *
 ****************************************************************
 *
 * Rogue Wave Software
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ****************************************************************
 *
 * $Log: vector.cpp,v $
 * Revision 1.1  1993/10/15  07:36:51  alv
 * Initial revision
 *
 *
 */

#include <iostream.h>
#include <rw/dvec.h>

main()
{
  DoubleVec x = "[ 2 3 8 9 3 2 1 ]";
  DoubleVec y(7,0);
  y("2:5") = 4;
  DoubleVec z = x+y;
  cout << z;

  return 0;
}

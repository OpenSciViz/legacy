/*
 * Example program: solve a system of equations in the least squares sense
 *
 * From the LAPACK.h++ manual
 *
 ****************************************************************
 *
 * This program reads a matrix A and a vector b from standard
 * input, and then solves the linear system of equations Ax=b
 * for x.  If the system is overdetermined it computes a least
 * squares solution.  If the system is exactly determined it
 * computes the solution.  If the system is underdetermined it
 * computes the minimum norm solution.
 *
 ****************************************************************
 *
 * Rogue Wave Software
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ****************************************************************
 *
 * $Log: lsfact.cpp,v $
 * Revision 1.1  1993/10/15  07:36:51  alv
 * Initial revision
 *
 *
 */

#include <iostream.h>
#include <rw/dlsqr.h>

main()
{
  DoubleGenMat A;                 // Read in A and b
  DoubleVec b;
  cin >> A >> b;

  DoubleLeastSqQR ls(A);          // Construct the factorization

  DoubleVec x = solve(ls,b);      // Compute and print solution
  cout << "Solution is x=" << x << endl;

  DoubleVec r = residual(ls,b);   // Compute and print residual
  cout << "Residual is r=" << r << endl;

  return 0;
}


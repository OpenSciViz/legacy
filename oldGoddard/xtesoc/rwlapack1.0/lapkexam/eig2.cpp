/*
 * Example program: compute first 2 right eigenvectors of a matrix
 *
 * From the LAPACK.h++ manual
 *
 ****************************************************************
 *
 * Rogue Wave Software
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ****************************************************************
 *
 * $Log: eig2.cpp,v $
 * Revision 1.1  1993/10/15  07:36:51  alv
 * Initial revision
 *
 *
 */

#include <iostream.h>
#include <rw/deigsrv.h>

main()
{
  DoubleGenMat A;
  cin >> A;

  DoubleHessEigServer server;
  server.computeLeftEigenVectors(FALSE);
  server.selectEigenVectors(RWRange(0,1));
  DoubleEigDecomp decomp = server(A);

  cout << "Eigenvalues are " << decomp.eigenValues() << endl;
  cout << "right eigenvectors are columns of " << decomp.rightEigenVectors() << endl;

  return 0;
}


/*
 * Example program: direct access to data
 *
 * From the LAPACK.h++ manual
 *
 ****************************************************************
 *
 * Rogue Wave Software
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ****************************************************************
 *
 * $Log: matrix4.cpp,v $
 * Revision 1.1  1993/10/15  07:36:51  alv
 * Initial revision
 *
 *
 */

#include <rw/dtrdgmat.h>
#include <iostream.h>

main()
{
  DoubleTriDiagMat A(4,4);
  DoubleVec Adata = A.dataVec();
  Adata = DoubleVec(Adata.length(),1,1);  // set storage to 1,2,3,...
  cout << A << endl;

  return 0;
}


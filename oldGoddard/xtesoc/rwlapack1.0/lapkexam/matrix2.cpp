/*
 * Example program: demonstation of references versus copies
 *
 * From the LAPACK.h++ manual
 *
 ****************************************************************
 *
 * Rogue Wave Software
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ****************************************************************
 *
 * $Log: matrix2.cpp,v $
 * Revision 1.1  1993/10/15  07:36:51  alv
 * Initial revision
 *
 *
 */

#include <rw/dskewmat.h>
#include <iostream.h>

main()
{
  DoubleSkewMat S(3,3);
  S.zero();
  DoubleSkewMat R = S;          // R and S share storage
  DoubleSkewMat T = S.copy();   // T and S do not share storage
  R(1,2) = 5;                   // affects both R and S, but not T
  T(2,1) = 3;                   // affects T, but not R nor S
  cout << "R= " << R << endl;
  cout << "S= " << S << endl;
  cout << "T= " << T << endl;

  return 0;
}


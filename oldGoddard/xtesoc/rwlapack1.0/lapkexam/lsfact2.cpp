/*
 * Example program: solve a least squares system using tolerance
 *
 * From the LAPACK.h++ manual
 *
 ****************************************************************
 *
 * Here we solve the same linear least squares problem twice, once
 * with a tolerance parameter and once without.  Without the
 * use of a tolerance, the system of equations is very ill conditioned
 * and we get a useless solution.
 *
 ****************************************************************
 *
 * Rogue Wave Software
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ****************************************************************
 *
 * $Log: lsfact2.cpp,v $
 * Revision 1.1  1993/10/15  07:36:51  alv
 * Initial revision
 *
 *
 */

#include <iostream.h>
#include <rw/dlsqr.h>

main()
{
  DoubleGenMat A = "3x2 [ 4.1 9.9   41 99   20.5 49.5 ]";
  DoubleVec b = "[ 3 30 14 ]";
  double tol = 1e-6;
  cout << "System matrix is A=" << A << endl;
  cout << "right hand side is b=" << b << endl;
  cout << "Tolerance of entries in A is " << tol << endl << endl;

  DoubleLeastSqQR notol(A);        // Construct a factorization with no tolerance
  DoubleLeastSqQR usetol(A,tol);   // and with a tolerance of 0.000001

  cout << "Without tolerance:\n";
  DoubleVec xnotol = solve(notol,b);
  cout << "\tsolution is x=" << xnotol << endl;
  
  cout << "With a tolerance of " << tol << "\n";
  DoubleVec xwithtol = solve(usetol,b);
  cout << "\tsolution is x=" << xwithtol << endl;

  return 0;
}


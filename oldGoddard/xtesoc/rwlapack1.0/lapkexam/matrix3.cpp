/*
 * Example program: changing elements with operator()()
 *
 * From the LAPACK.h++ manual
 *
 ****************************************************************
 *
 * Rogue Wave Software
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ****************************************************************
 *
 * $Log: matrix3.cpp,v $
 * Revision 1.1  1993/10/15  07:36:51  alv
 * Initial revision
 *
 *
 */

#include <rw/fskewmat.h>
#include <iostream.h>

main()
{
  FloatSkewMat A(2,2);
  A.zero();
  A(0,1) = 1;                   // also sets A(1,0) to -1
  A(1,0) = 1;                   // also sets A(0,1) to -1
  cout << A << endl;

  return 0;
}


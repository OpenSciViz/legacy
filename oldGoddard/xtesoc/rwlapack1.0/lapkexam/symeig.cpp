/*
 * Example program: construct eigenvalue decomposition for symmetric matrix
 *
 * From the LAPACK.h++ manual
 *
 ****************************************************************
 *
 * Rogue Wave Software
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ****************************************************************
 *
 * $Log: symeig.cpp,v $
 * Revision 1.1  1993/10/15  07:36:51  alv
 * Initial revision
 *
 *
 */

#include <iostream.h>
#include <rw/dsymmat.h>
#include <rw/dsymeig.h>

main()
{
  DoubleSymMat A;
  cin >> A;

  DoubleSymEigDecomp decomp(A);

  cout << "Input matrix is " << A << endl;
  cout << "Eigenvalues are " << decomp.eigenValues() << endl;
  cout << "Eigenvectors are columns of " << decomp.eigenVectors() << endl;

  return 0;
}


/*
 * Example program: compute first 2 eigenvalues of a symmetric matrix
 *
 * From the LAPACK.h++ manual
 *
 ****************************************************************
 *
 * Rogue Wave Software
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ****************************************************************
 *
 * $Log: symeig2.cpp,v $
 * Revision 1.1  1993/10/15  07:36:51  alv
 * Initial revision
 *
 *
 */

#include <iostream.h>
#include <rw/dsymmat.h>
#include <rw/dseigsrv.h>

main()
{
  DoubleSymMat A;
  cin >> A;

  DoubleSymSomeEigServer server;
  server.computeEigenVectors(FALSE);
  server.setRange("0:1");             // set range using RWSlice
  DoubleSymEigDecomp decomp = server(A);

  cout << "Eigenvalues are " << decomp.eigenValues() << endl;

  return 0;
}


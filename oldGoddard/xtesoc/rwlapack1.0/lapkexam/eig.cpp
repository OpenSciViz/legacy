/*
 * Example program: construct eigenvalue decomposition
 *
 * From the LAPACK.h++ manual
 *
 ****************************************************************
 *
 * Rogue Wave Software
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ****************************************************************
 *
 * $Log: eig.cpp,v $
 * Revision 1.1  1993/10/15  07:36:51  alv
 * Initial revision
 *
 *
 */

#include <iostream.h>
#include <rw/deig.h>

main()
{
  DoubleGenMat A;
  cin >> A;

  DoubleEigDecomp decomp(A);

  cout << "Input matrix is " << A << endl;
  cout << "Eigenvalues are " << decomp.eigenValues() << endl;
  cout << "Left eigenvectors are columns of " << decomp.leftEigenVectors() << endl;
  cout << "Right eigenvectors are columns of " << decomp.rightEigenVectors() << endl;

  return 0;
}


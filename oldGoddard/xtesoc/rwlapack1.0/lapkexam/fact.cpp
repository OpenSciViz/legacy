/*
 * Example program: solve a system of equations
 *
 * From the LAPACK.h++ manual
 *
 ****************************************************************
 *
 * This program reads a matrix A and a vector b from standard
 * input, and then solves the linear system of equations Ax=b
 * for x.
 *
 ****************************************************************
 *
 * Rogue Wave Software
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ****************************************************************
 *
 * $Log: fact.cpp,v $
 * Revision 1.1  1993/10/15  07:36:51  alv
 * Initial revision
 *
 *
 */

#include <iostream.h>
#include <rw/fgenfct.h>

main()
{
  FloatGenMat A;                 // Read in A and b
  FloatVec b;
  cin >> A >> b;

  // DoubleVec x = solve(A,b);   // This approach doesn't do any error checks!

  FloatGenFact LU(A);            // Construct and check factorization
  if (LU.fail()) {
    cout << "Could not solve system, most likely A is singular" << endl;
    return 1;
  }

  FloatVec x = solve(LU,b);      // Compute and print solution
  cout << "Solution is x=" << x << endl;

  return 0;
}


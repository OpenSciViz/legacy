> ###############################################################
> # The math member functions                                   #
> #                                                             #
> # $Header: /users/rcs/lapksrc/xmth.cpp,v 1.2 1993/10/11 21:48:17 alv Exp $
> #                                                             #
> # Copyright (1991) by Rogue Wave Software, Inc.  This software#
> # is subject to copyright protection under the laws of the    #
> # United States and other countries.                          #
> #                                                             #
> # Written by Al Vermeulen.                                    #
> #                                                             #
> # Limited License.                                            #
> ###############################################################
> #
> # $Log: xmth.cpp,v $
> # Revision 1.2  1993/10/11  21:48:17  alv
> # synch'ed with Math.h++ v5.1
> #
> # Revision 1.1  1993/06/23  18:28:10  alv
> # Initial revision
> #
> # Revision 1.3  1993/03/12  21:24:24  alv
> # NO_INLINED_TEMP_DESTRUCTORS -> RW_NO_INLINED_TEMP_DESTRUCTORS
> #
> # Revision 1.2  1993/01/26  21:05:42  alv
> # removed DOS EOF
> #
> # Revision 1.1  1993/01/23  00:01:12  alv
> # Initial revision
> #
> # 
> #    Rev 1.5   17 Oct 1991 09:22:36   keffer
> # Changed include path to <rw/xxx.h>
> # 
> #    Rev 1.4   02 Sep 1991 14:33:06   keffer
> # Now includes defs.h to avoid Glockenspiel preprocessor nesting limits.
> # Explicitly uses member data "vec" instead of dataVec() where possible.
> # 
> #    Rev 1.3   02 Sep 1991 12:08:02   keffer
> # NO_INLINED_TEMP_DESTRUCTORS compilers now have out-of-lined versions
> # 
> #    Rev 1.2   27 Aug 1991 11:47:14   keffer
> # Removed single argument constructors to avoid type conversion problems
> # 
> #    Rev 1.1   22 Aug 1991 09:38:26   keffer
> # Ported to cfront
> # 
>
> include macros 
>
#if 0
/*
 * Generated from template $Id: xmth.cpp,v 1.2 1993/10/11 21:48:17 alv Exp $
 *
 * Math member functions for the matrix types
 *
> copyright
 */
#endif

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/<tLetter><shapeAbbrev>mat.h"
> if <R/C>==Complex 
>  if <M>==HermMat || <M>==SkewMat
#include "rw/<pLetter>symmat.h"
#include "rw/<pLetter>skewmat.h"
>  elif <M>==HermBandMat
#include "rw/<pLetter>sbndmat.h"
>  else
#include "rw/<pLetter><shapeAbbrev>mat.h"
>  endif
> endif
>
> if <M>==SkewMat
#include "rw/<tLetter>symmat.h"
> endif
>
> if <M>==LowerTriMat
#include "rw/<tLetter>utrimat.h"
> elif <M>==UpperTriMat
#include "rw/<tLetter>ltrimat.h"
> endif

/*
 * This function returns a new matrix the same shape as the one 
 * passed in but with the data passed in.  This way we restrict
 * all the >if statements to a couple lines
 */
inline <T><M> sameShapeMatrix( <T>Vec& vec, const <T><M>& A )
{
> if <M>==BandMat
  return <T><M>(vec,A.rows(),A.cols(),A.lowerBandwidth(),A.upperBandwidth());
> elif <M>==SymBandMat || <M>==HermBandMat
  return <T><M>(vec,A.rows(),A.cols(),A.lowerBandwidth());
> else
  return <T><M>(vec,A.rows(),A.cols());
> endif
}

> if <R/C>==Complex && <M>!=HermMat && <M>!=HermBandMat
inline <P><M> sameShapeRealMatrix( <P>Vec& vec, const <T><M>& A )
{
> if <M>==BandMat
  return <P><M>(vec,A.rows(),A.cols(),A.lowerBandwidth(),A.upperBandwidth());
> elif <M>==SymBandMat || <M>==HermBandMat
  return <P><M>(vec,A.rows(),A.cols(),A.lowerBandwidth());
> else
  return <P><M>(vec,A.rows(),A.cols());
> endif
}
> endif

static void verifyMatch(const <T><M>& A, const <T><M>& B)
{
    if (A.rows()!=B.rows() || A.cols()!=B.cols())
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATSIZE,A.rows(),A.cols(),B.rows(),B.cols())));
> if <M>==BandMat || <M>==SymBandMat || <M>==HermBandMat
    if (A.bandwidth()!=B.bandwidth() || A.upperBandwidth()!=B.upperBandwidth())
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_BANDSIZE,A.bandwidth(),B.bandwidth(),
			A.upperBandwidth(), B.upperBandwidth())));
> endif
}

RWBoolean <T><M>::operator==(const <T><M>& X)
{
> if <M>==GenMat
    if (n!=X.n || m!=X.m) return FALSE;
> elif <M>==SymMat || <M>==SkewMat || <M>==HermMat || <M>==TriDiagMat || <M>==UpperTriMat || <M>==LowerTriMat
    if (n!=X.n) return FALSE;
> elif <M>==BandMat
    if (n!=X.n || band!=X.band || bandu!=X.bandu) return FALSE;
> elif <M>==SymBandMat || <M>==HermBandMat
    if (n!=X.n || bandu!=X.bandu) return FALSE;
> else 
> err No op== for shape <M>
> endif
>
> # Only test the vector entries that are actually used!
> if <M>==TriDiagMat
    // The following two statements cannot be combined into one
    // because of a cfront limitation:
    if ( n<1 ) return TRUE;
    return vec.slice(1,3*n-2)==X.vec.slice(1,3*n-2);
> elif <M>==BandMat
    int b = bandwidth();
    int lb = lowerBandwidth();
    int ub = upperBandwidth();
    /*
     * This is a little tricky cause some of the data vector is unused.
     * First check the middle part of the vector.  Next check the bits
     * at the front that are part of the matrix, and finally check the 
     * bits at the end
     */
    int midstart = (ub==0) ? 0 : (ub-1)*b+1;
    int fixup = (lb==0) ? ((ub==0)?0:1) : ((ub==0)?1:2);
    int midlen = (int(n)-ub-lb+fixup)*b-fixup;
    if (midlen>0)
	if(vec.slice(midstart,midlen)!=X.vec.slice(midstart,midlen)) return FALSE;
    int el,len;
    for( el=ub,len=b-ub ; el<n*b && len<(b-1); el+=b-1,len++ ) {
      if (vec.slice(el,len)!=X.vec.slice(el,len)) return FALSE;
    }
    for( el=b*(n-lb+1),len=b-2 ; el<n*b; el+=b,len-- ) {
      if (vec.slice(el,len)!=X.vec.slice(el,len)) return FALSE;
    }
    return TRUE;
> elif <M>==SymBandMat || <M>==HermBandMat
    int hb = halfBandwidth();
    if (hb==0) return ( vec==X.vec );	// A diagonal matrix
    /*
     * This is a little tricky cause some of the data vector is unused.
     * First check the last part of the data vector, then check the bits
     * at the beginning that are used.
     */
    int lastStart = hb*hb;
    int lastLen = (n-hb+1)*(hb+1)-1;
    if (n>=hb)
	if ( vec.slice(lastStart,lastLen)!=X.vec.slice(lastStart,lastLen) ) return FALSE;
    int el,len;
    for( el=hb,len=1 ; el<vec.length() && len<hb ; el+=hb,len++ ) {    
      if (vec.slice(el,len)!=X.vec.slice(el,len)) return FALSE;
    }
    return TRUE;
> else
    return vec==X.vec;	// All elements in vec used in the matrix
> endif
}

> # We can only do plus and minus operators on matrices with no
> # explicit zero elements
>
> if ( <M>==GenMat || <M>==SymMat ) && <T>!=UChar
>
<T><M>& <T><M>::operator+=(<T> x)
{
  vec += x;
  return *this;
}

<T><M>& <T><M>::operator-=(<T> x)
{
  vec -= x;
  return *this;
}
>
> endif

<T><M>& <T><M>::operator+=(const <T><M>& m)
{
  verifyMatch(*this,m);
  vec += m.vec;
  return *this;
}

> if <T>!=UChar
>
<T><M>& <T><M>::operator-=(const <T><M>& m)
{
  verifyMatch(*this,m);
  vec -= m.vec;
  return *this;
}

<T><M>& <T><M>::operator*=(const <T><M>& m)
{
  verifyMatch(*this,m);
  vec *= m.vec;
  return *this;
}

<T><M>& <T><M>::operator*=(<T> x)
{
  vec *= x;
  return *this;
}

<T><M>& <T><M>::operator/=(const <T><M>& m)
{
  verifyMatch(*this,m);
  vec /= m.vec;
  return *this;
}

<T><M>& <T><M>::operator/=(<T> x)
{
  vec /= x;
  return *this;
}
>
> endif


> if <R/C>==Real && ( <M>==GenMat || <M>==SymMat )
<T><M>& <T><M>::operator++()
{
  ++vec; return *this;
}

#ifndef RW_NO_POSTFIX
void <T><M>::operator++(int)	/* Postfix operator++ */
{
  vec++;
}
#endif

>   if <T>!=UChar
>
<T><M>& <T><M>::operator--()
{
  --vec; return *this;
}

#ifndef RW_NO_POSTFIX
void <T><M>::operator--(int)	/* Postfix operator-- */
{
  vec--;
}
#endif

>   endif
> endif

> if ( <M>==GenMat || <M>==SymMat ) && <T>!=Int && <T>!=UChar && <T>!=SChar
>  if <R/C>==Real
<T><M> <T><M>::apply(mathFunTy f) const
{
    <T>Vec temp(vec.apply(f));
    return sameShapeMatrix(temp,*this);
}

>  elif <R/C>==Complex
>
<T><M> <T><M>::apply(CmathFunTy f) const
{
    <T>Vec temp(vec.apply(f));
    return sameShapeMatrix(temp,*this);
}

<P><M> <T><M>::apply2(CmathFunTy2 f) const
{
    <P>Vec temp(vec.apply2(f));
    return sameShapeRealMatrix(temp,*this);
}

>  else
>  err <R/C> is not either Real or Complex
>  endif
> endif

>
> if <T>!=UChar
>
<T><M> operator-(const <T><M>& A)
{
  <T>Vec temp(-(A.dataVec()));
  return sameShapeMatrix(temp,A);
}
>
> endif
  
<T><M> operator+(const <T><M>& A)
{
  return A;
}

>
<T><M> operator*(const <T><M>& A, const <T><M>& B) {
	verifyMatch(A,B);
	<T>Vec temp(A.dataVec()*B.dataVec());
	return sameShapeMatrix(temp,A);
}

<T><M> operator/(const <T><M>& A, const <T><M>& B) {
	verifyMatch(A,B);
	<T>Vec temp(A.dataVec()/B.dataVec());
	return sameShapeMatrix(temp,A);
}

<T><M> operator+(const <T><M>& A, const <T><M>& B) {
	verifyMatch(A,B);
	<T>Vec temp(A.dataVec()+B.dataVec());
	return sameShapeMatrix(temp,A);
}

> if <T>!=UChar
>
<T><M> operator-(const <T><M>& A, const <T><M>& B) {
	verifyMatch(A,B);
	<T>Vec temp(A.dataVec()-B.dataVec());
	return sameShapeMatrix(temp,A);
}
>
> endif

> if <T>!=UChar
<T><M> operator*(const <T><M>& A, <T> x) {
	<T>Vec temp(A.dataVec()*x);
	return sameShapeMatrix(temp,A);
}

/*
 * Out-of-line version for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
  <T><M> operator*(<T> x, const <T><M>& A) { return A*x; }
#endif

<T><M> operator/(const <T><M>& A, <T> x) {
	<T>Vec temp(A.dataVec()/x);
	return sameShapeMatrix(temp,A);
}

> endif

> if <T>!=UChar && ( <M>==GenMat || <M>==SymMat || <M>==SkewMat || <M>==HermMat )
<T><M> operator/(<T> x, const <T><M>& A) {
	<T>Vec temp(x/A.dataVec());
	return sameShapeMatrix(temp,A);
}
> endif

> if <T>!=UChar && ( <M>==GenMat || <M>==SymMat )
<T><M> operator+(const <T><M>& A, <T> x) {
	<T>Vec temp(A.dataVec()+x);
	return sameShapeMatrix(temp,A);
}
/*
 * Out-of-line versions for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
<T><M> operator+(<T> x, const <T><M>& A) { return A+x; }
<T><M> operator-(const <T><M>& A, <T> x) { return A+(-x); }
#endif
<T><M> operator-(<T> x, const <T><M>& A) {
	<T>Vec temp(x-A.dataVec());
	return sameShapeMatrix(temp,A);
}
> endif

> # Note that the following if statement disallows int types
> if ( <T>==Float || <T>==Double ) && ( <M>==GenMat || <M>==SymMat )
<T><M> atan2(const <T><M>& A, const <T><M>& B) {
	<T>Vec temp(atan2(A.dataVec(),B.dataVec()));
	return sameShapeMatrix(temp,A);
}
> endif

>
> if <R/C>==Real
>   if <T>==UChar
/* No abs() function for unsigned types */
>   elif <M>==SkewMat
<T>SymMat abs(const <T><M>& A) {
	<T>Vec temp(abs(A.dataVec()));
	return <T>SymMat(temp,A.rows(),A.cols());
}
>
>   else
>
<T><M> abs(const <T><M>& A) {
	<T>Vec temp(abs(A.dataVec()));
	return sameShapeMatrix(temp,A);
}
>
>   endif
>
> elif <R/C>==Complex
>
>   if <M>==HermMat
<P>SymMat abs(const <T><M>& A) {
	<P>Vec temp(abs(A.dataVec()));
	return <P>SymMat(temp,A.rows(),A.cols());
}
>   elif <M>==SkewMat
<P>SymMat abs(const <T><M>& A) {
	<P>Vec temp(abs(A.dataVec()));
	return <P>SymMat(temp,A.rows(),A.cols());
}
>   elif <M>==HermBandMat
<P>SymBandMat abs(const <T><M>& A) {
	<P>Vec temp(abs(A.dataVec()));
	return <P>SymBandMat(temp,A.rows(),A.cols(),A.halfBandwidth());
}
>   else
<P><M> abs(const <T><M>& A) {
	<P>Vec temp(abs(A.dataVec()));
	return sameShapeRealMatrix(temp,A);
}
>   endif
>
> else
> err <R/C> neither Real nor Complex
> endif


> if <R/C>==Complex
>
>  if <M>==HermBandMat

   /* Sorry, no imag, arg, functions for <M> */

<T><M> conj(const <T><M>& A) {
	<T>Vec temp(conj(A.dataVec()));
	return sameShapeMatrix(temp,A);
}

<P>SymBandMat real(const <T><M>& A) {
	<P>Vec temp(real(A.dataVec()));
	return <P>SymBandMat(temp,A.rows(),A.cols(),A.halfBandwidth());
}

<P>SymBandMat norm(const <T><M>& A) {
	<P>Vec temp(norm(A.dataVec()));
	return <P>SymBandMat(temp,A.rows(),A.cols(),A.halfBandwidth());
}
>
>  elif <M>==SkewMat
>

/* Sorry, no conj or arg function for SkewMat */

<P><M> real(const <T><M>& A) {
	<P>Vec temp(real(A.dataVec()));
	return sameShapeRealMatrix(temp,A);
}

<P><M> imag(const <T><M>& A) {
	<P>Vec temp(imag(A.dataVec()));
	return sameShapeRealMatrix(temp,A);
}

<P>SymMat norm(const <T><M>& A) {
	<P>Vec temp(norm(A.dataVec()));
	return <P>SymMat(temp,A.rows(),A.cols());
}

>
>  elif <M>==HermMat
>
<T><M> conj(const <T><M>& A) {
	<T>Vec temp(conj(A.dataVec()));
	return sameShapeMatrix(temp,A);
}

<P>SymMat real(const <T><M>& A) {
	<P>Vec temp(real(A.dataVec()));
	return <P>SymMat(temp,A.rows(),A.cols());
}

<P>SkewMat imag(const <T><M>& A) {
	<P>Vec temp(imag(A.dataVec()));
	return <P>SkewMat(temp,A.rows(),A.cols());
}

<P>SymMat norm(const <T><M>& A) {
	<P>Vec temp(norm(A.dataVec()));
	return <P>SymMat(temp,A.rows(),A.cols());
}

<P>SkewMat arg(const <T><M>& A) {
	<P>Vec temp(arg(A.dataVec()));
	return <P>SkewMat(temp,A.rows(),A.cols());
}

>
>  else
>
<T><M> conj(const <T><M>& A) {
	<T>Vec temp(conj(A.dataVec()));
	return sameShapeMatrix(temp,A);
}

<P><M> real(const <T><M>& A) {
	<P>Vec temp(real(A.dataVec()));
	return sameShapeRealMatrix(temp,A);
}

<P><M> imag(const <T><M>& A) {
	<P>Vec temp(imag(A.dataVec()));
	return sameShapeRealMatrix(temp,A);
}

<P><M> norm(const <T><M>& A) {
	<P>Vec temp(norm(A.dataVec()));
	return sameShapeRealMatrix(temp,A);
}

<P><M> arg(const <T><M>& A) {
	<P>Vec temp(arg(A.dataVec()));
	return sameShapeRealMatrix(temp,A);
}


>  endif
> endif

>
>
> if <M>==GenMat
>
<T><M> transpose(const <T><M>& A)
{
  <T><M> T(A.cols(),A.rows());
  for( int i=0; i<A.cols(); i++ ) {
    T.row(i) = A.col(i);
  }
  return T;
}
>
> elif <M>==SymMat || <M>==SymBandMat
>
<T><M> transpose(const <T><M>& A) { return A; }
>
> elif <M>==SkewMat
>
<T><M> transpose(const <T><M>& A)
{
  <T><M> T(-A);
  <T>* index = T.data();		// Fix up the diagonal
  for( int k=1; k<=T.rows(); index+=(++k) ) {
> if <R/C>==Complex
    (*index) *= <T>(-1,0);	// Explicit constructor required for Zortech
> else
    (*index) *= -1;
> endif
  }
  return T;
}
>
> elif <M>==HermMat
>
<T><M> transpose(const <T><M>& A)
{
  <T><M> T(conj(A));
  <T>* index = T.data();		// Fix up the diagonal
  for( int k=1; k<=T.rows(); index+=(++k) ) {
    (*index) = conj(*index);
  }
  return T;
}
>
> elif <M>==HermBandMat
>
<T><M> transpose(const <T><M>& A)
{
  <T><M> T(conj(A));
  int n = A.rows();
  int m = A.halfBandwidth();
  <T>Vec diag = T.dataVec().slice(m,n,m+1);
  diag = conj(diag);
  return T;
}
> 
> elif <M>==BandMat
>
<T><M> transpose(const <T><M>& A)
{
  <T><M> T(A.rows(),A.cols(),A.upperBandwidth(),A.lowerBandwidth());
  int l = A.lowerBandwidth();	// Getting these as ints avoids
  int u = A.upperBandwidth();	// conversion problems
  for( int i=(-l); i<=u; i++ ) {
    T.diagonal(-i) = A.diagonal(i);
  }
  return T;
}
>
> elif <M>==TriDiagMat
>
<T><M> transpose(const <T><M>& A)
{
  <T><M> T(A.rows(),A.cols());
  T.diagonal(-1) = A.diagonal(1);
  T.diagonal(0) = A.diagonal(0);
  T.diagonal(1) = A.diagonal(-1);
  return T;
}
>
> elif <M>==LowerTriMat
>
<T>UpperTriMat transpose(const <T><M>& A)
{
return <T>UpperTriMat(A.dataVec(),A.rows(),A.cols());
}
>
> elif <M>==UpperTriMat
>
<T>LowerTriMat transpose(const <T><M>& A)
{
return <T>LowerTriMat(A.dataVec(),A.rows(),A.cols());
}
>
> else
> err No transpose function for matrix shape <M>
> endif

<T>Vec product(const <T><M>& A, const <T>Vec& x)
{
    if (A.cols()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATVECPROD,A.className,A.rows(),A.cols(),x.length())));
    }
    <T>Vec y(A.rows(),rwUninitialized);
>
> if <M>==GenMat
    for( int i=0; i<A.rows(); i++ ) {
      y(i) = dot(A.row(i),x);
    }
>
> elif <M>==SymMat 
    const <T>Vec& v = A.dataVec();
    int n=A.rows();
    for( int i=0; i<n; i++ ) {
      y(i) = dot( v.slice(i*(i+1)/2,i+1,1), x.slice(0,i+1,1) );
      <T>* index = (<T>*)v.data() + ((i+2)*(i+3)/2-2);
      for( int k=i+1; k<n; index+=(++k) ) {
	y(i) += (*index)*x(k);
      }
    }
>
> elif <M>==SkewMat
    const <T>Vec& v = A.dataVec();
    int n=A.rows();
    y(0) = <zero>;
    for( int i=0; i<n; i++ ) {
      if (i>0) {
        y(i) = -dot( v.slice(i*(i+1)/2,i,1), x.slice(0,i,1) );
      }
      <T>* index = (<T>*)v.data() + ((i+1)*(i+2)/2-1);
      for( int k=i; k<n; index+=(++k) ) {
	y(i) += (*index)*x(k);
      }
    }
>
> elif <M>==HermMat
    const <T>Vec& v = A.dataVec();
    int n=A.rows();
    y(0) = <zero>;
    for( int i=0; i<n; i++ ) {
      if (i>0) {
        y(i) = conjDot( v.slice(i*(i+1)/2,i,1), x.slice(0,i,1) );
      }
      <T>* index = (<T>*)v.data() + ((i+1)*(i+2)/2-1);
      for( int k=i; k<n; index+=(++k) ) {
	y(i) += (*index)*x(k);
      }
    }
>
> elif <M>==BandMat || <M>==SymBandMat
    y = <zero>;
    int l = A.lowerBandwidth();	// Getting these as ints avoids
    int u = A.upperBandwidth();	// conversion problems
    for( int i=(-l); i<=u; i++ ) {
      int iabs = (i>=0)?i:(-i);
      <T>Vec yslice = y.slice( (i<0)?iabs:0, x.length()-iabs );
      <T>Vec xslice = x.slice( (i>0)?iabs:0, x.length()-iabs );
      /* The * in the next line is element by element multiplication */
      yslice += A.diagonal(i) * xslice;
    }
>
> elif <M>==HermBandMat
    int n = A.rows();
    int m = A.lowerBandwidth();
    y = A.dataVec().slice(m,n,m+1);
    y *= x;
    for( int i=1; i<=m; i++ ) {
      <T>Vec diag = A.dataVec().slice( m+i*m, n-i, m+1 );
      <T>Vec yslice = y.slice( 0, x.length()-i );
      <T>Vec xslice = x.slice( i, x.length()-i );
      yslice += diag * xslice;
      <T>Vec yslice2 = y.slice( i, x.length()-i );
      <T>Vec xslice2 = x.slice( 0, x.length()-i );
      yslice2 += conj(diag) * xslice2;
    }
>
> elif <M>==UpperTriMat
    const <T>Vec& v = A.dataVec();
    int n=A.rows();
    for( int i=0; i<n; i++ ) {
      <T>* index = (<T>*)v.data() + ((i+1)*(i+2)/2-1);
      y(i) = (*index)*x(i);
      index += i+1;
      for( int k=i+1; k<n; index+=(++k) ) {
      y(i) += (*index)*x(k);
      }
    }
>
> elif <M>==LowerTriMat
    const <T>Vec& v = A.dataVec();
    int n=A.rows();
    for( int i=0; i<n; i++ ) {
      y(i) = dot( v.slice(i*(i+1)/2,i+1,1), x.slice(0,i+1,1) );
    }
>
> elif <M>==TriDiagMat
    y = A.diagonal(0) * x;
    y.slice(1,x.length()-1) += A.diagonal(-1) * x.slice(0,x.length()-1);
    y.slice(0,x.length()-1) += A.diagonal(1) * x.slice(1,x.length()-1);
>
> else
> err No matrix-product mult routine for shape <M>
> endif
    return y;
}

<T>Vec product(const <T>Vec& x, const <T><M>& A)
{
    if (A.rows()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECMATPROD,x.length(),A.className,A.rows(),A.cols())));
    }
>
> if <M>==GenMat
    <T>Vec y(A.cols(),rwUninitialized);
    for( int i=0; i<A.cols(); i++ ) {
      y(i) = dot(A.col(i),x);
    }
    return y;
>
> elif <M>==SymMat || <M>==SymBandMat
    return product(A,x);
>
> elif <M>==SkewMat
    <T>Vec y(A.cols(),<zero>);
    const <T>Vec& v = A.dataVec();
    int n=A.rows();
    for( int i=0; i<n; i++ ) {
      y(i) = dot( v.slice(i*(i+1)/2,i+1,1), x.slice(0,i+1,1) );
      <T>* index = (<T>*)v.data() + ((i+2)*(i+3)/2-2);
      for( int k=i+1; k<n; index+=(++k) ) {
	y(i) -= (*index)*x(k);
      }
    }
    return y;
>
> elif <M>==HermMat
    <T>Vec y(A.cols(),<zero>);
    const <T>Vec& v = A.dataVec();
    int n=A.rows();
    for( int i=0; i<n; i++ ) {
      y(i) = dot( v.slice(i*(i+1)/2,i+1,1), x.slice(0,i+1,1) );
      <T>* index = (<T>*)v.data() + ((i+2)*(i+3)/2-2);
      for( int k=i+1; k<n; index+=(++k) ) {
	y(i) += conj(*index)*x(k);
      }
    }
    return y;
>
> elif <M>==HermBandMat
    int n = A.rows();
    int m = A.lowerBandwidth();
    <T>Vec y(A.dataVec().slice(m,n,m+1));
    y.deepenShallowCopy();
    y *= x;
    for( int i=1; i<=m; i++ ) {
      <T>Vec diag = A.dataVec().slice( m+i*m, n-i, m+1 );
      <T>Vec yslice = y.slice( 0, x.length()-i );
      <T>Vec xslice = x.slice( i, x.length()-i );
      yslice += conj(diag) * xslice;
      <T>Vec yslice2 = y.slice( i, x.length()-i );
      <T>Vec xslice2 = x.slice( 0, x.length()-i );
      yslice2 += diag * xslice2;
    }
    return y;
>
> elif <M>==BandMat
    <T>Vec y(A.cols(),<zero>);
    int l = A.lowerBandwidth();	// Getting these as ints avoids
    int u = A.upperBandwidth();	// conversion problems
    for( int i=(-l); i<=u; i++ ) {
      int iabs = (i>=0)?i:(-i);
      <T>Vec yslice = y.slice( (i>0)?iabs:0, x.length()-iabs );
      <T>Vec xslice = x.slice( (i<0)?iabs:0, x.length()-iabs );
      /* The * in the next line is element by element multiplication */
      yslice += A.diagonal(i) * xslice;
    }
    return y;
>
> elif <M>==UpperTriMat || <M>==LowerTriMat
    return product(transpose(A),x);
>
> elif <M>==TriDiagMat
    <T>Vec y(A.cols(),rwUninitialized);
    y = A.diagonal(0) * x;
    y.slice(1,x.length()-1) += A.diagonal(1) * x.slice(0,x.length()-1);
    y.slice(0,x.length()-1) += A.diagonal(-1) * x.slice(1,x.length()-1);
    return y;
>
> else
> err No vec-LAPK prod routine for shape <M>
> endif
}

> if <M>==GenMat
>
<T><M> product( const <T><M>& A, const <T><M>& B )
{
    register int i,j;
    if (A.cols() != B.rows()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATMATPROD,A.className,A.rows(),A.cols(),B.rows(),B.cols())));
    }
> if <M>==GenMat
    <T><M> AB(A.rows(),B.cols());
> else
    <T><M> AB(A.rows());
> endif
    for( i=0; i<A.rows(); i++ ) {
      <T>Vec Arow = A.row(i);
      Arow.deepenShallowCopy();	// Consolidate memory for cacheing purposes
      for( j=0; j<B.cols(); j++ ) {
        AB(i,j) = dot(Arow,B.col(j));
      }
    }
    return AB;
}

<T><M> transposeProduct( const <T><M>& A, const <T><M>& B )
{
    register int i,j;
    if (A.rows()!=B.rows()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATMATPROD,A.className,A.cols(),A.rows(),B.rows(),B.cols())));
    }
> if <M>==GenMat
    <T><M> ATB(A.cols(),B.cols());
> else
    <T><M> ATB(A.rows());
> endif
    for( j=0; j<B.cols(); j++ ) {
      <T>Vec Bcol = B.col(j);
      for( i=0; i<A.cols(); i++ ) {
	ATB(i,j) = dot(Bcol,A.col(i));
      }
    }
    return ATB;
}
>

> if <R/C>==Complex
<T><M> conjTransposeProduct( const <T><M>& A, const <T><M>& B )
{
    register int i,j;
    if (A.rows()!=B.rows()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATMATPROD,A.className,A.cols(),A.rows(),B.rows(),B.cols())));
    }
> if <M>==GenMat
    <T><M> ATB(A.cols(),B.cols());
> else
    <T><M> ATB(A.rows());
> endif
    /*
     * The following could better be implemented by using a conjDot
     * type vector member function
     */
    <T><M> Aconj = conj(A);
    for( j=0; j<B.cols(); j++ ) {
      <T>Vec Bcol = B.col(j);
      for( i=0; i<A.cols(); i++ ) {
	ATB(i,j) = dot(Bcol,Aconj.col(i));
      }
    }
    return ATB;
}
> endif    if Complex
>
> endif	   if GenMat

> if <M>==BandMat
>
/*
 * The product of two banded matrices is a banded matrix.  Here's the details:
 *
 * Say we have want C=AB, then Cij = sum z=0,...,n ( AizBzj )
 * Say A has lower bandwidth al and upper au, B's bandwidths are bl and bu
 * so that Aij=0 unless -al<=j-i<=au and similiar for B.
 *
 * Back to Cij = sum z=0,...,n AizBzj.  Define j=i+k, now we have
 *         Cij = sum z=0,...,n Aiz Bz,i+k
 *
 * Now replace z by i+w:
 *         Ci,i+k = sum w=-i,...,n-i Ai,i+w Bi+w,i+k
 *
 * Bi+w,i+k is zero unless -bl<=k-w<=bu ==> k-bu<=w<=k+bl
 * Ai,i+w is zero unless                     -al<=w<=au
 * so we can reduce the limits on the summation:
 *
 *    Ci,i+k = sum w=max(-i,k-bu,-al),...,min(n-i,k+bl,au) Ai,i+w Bi+w,i+k
 *
 * That's the formula we'll implement.  Now consider the bandwidth of C.
 * For i in the middle of the matrix (-i is small and n-i is big) then
 * the sums are reduced to one term in two cases:
 *   k-bu=au  ==>  k=au+bu
 *   k+bl=-al ==>  k=-al-bl
 * so the lower bandwidth of C is al+bl and the upper au+bu.
 *
 * Rather than code this up with slices and dot() functions, I simply
 * wrote the loops directly.  Since (hopefully) the bandwidth is 
 * generally not that large, avoiding the overhead of function calls 
 * may be worthwhile.  (plus it is easier this way)
 */
<T><M> product(const <T><M>& A, const <T><M>& B)
{
    int n = A.cols();
    if (B.rows()!=n) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATMATPROD,A.className,n,n,B.rows(),B.cols())));
    }
    int au = A.upperBandwidth();
    int al = A.lowerBandwidth();
    int bu = B.upperBandwidth();
    int bl = B.lowerBandwidth();
    <T><M> AB(n,n,al+bl,au+bu);
    for( int i=0; i<n; i++ ) {
      int mink = (i>al+bl) ? (-al-bl) : (-i);
      int maxk = (i<n-au-bu) ? (au+bu) : (n-i-1);
      for( int k=mink; k<=maxk; k++ ) {
        int minw = (-i >= k-bu) ? (-i) : (k-bu);
	if (-al > minw) minw = -al;
        int maxw = (au <= k+bl) ? au : (k+bl);
        if (n-i-1 < maxw) maxw = n-i-1;
	<T> entry = <zero>;
        for( int w=minw; w<=maxw; w++ ) {
          entry += A.val(i,i+w) * B.val(i+w,i+k);
        }
	AB.set(i,i+k,entry);
      }
    }
    return AB;
}

<T><M> transposeProduct(const <T><M>& A, const <T><M>& B)
{
    return product(transpose(A),B);
}

> if <R/C>==Complex
<T><M> conjTransposeProduct(const <T><M>& A, const <T><M>& B)
{
    return product(conj(transpose(A)),B);
}
> endif

> endif

> ###############################################################
> # Functions for converting from one shape to another, except  #
> # for the functions which convert from/to general matrices.   #
> #                                                             #
> # $Header: /users/rcs/lapksrc/xcs.cpp,v 1.2 1993/10/11 21:48:07 alv Exp $
> #                                                             #
> # Copyright (1991) by Rogue Wave Software, Inc.  This software#
> # is subject to copyright protection under the laws of the    #
> # United States and other countries.                          #
> #                                                             #
> # Written by Al Vermeulen.                                    #
> #                                                             #
> # Limited License.                                            #
> ###############################################################
> #
> # $Log: xcs.cpp,v $
> # Revision 1.2  1993/10/11  21:48:07  alv
> # synch'ed with Math.h++ v5.1
> #
> # Revision 1.1  1993/06/23  18:28:05  alv
> # Initial revision
> #
> # Revision 1.2  1993/01/26  21:05:32  alv
> # removed DOS EOF
> #
> # Revision 1.1  1993/01/23  00:01:10  alv
> # Initial revision
> #
> # 
> #    Rev 1.4   17 Oct 1991 09:22:50   keffer
> # Changed include path to <rw/xxx.h>
> # 
> #    Rev 1.3   25 Sep 1991 14:59:24   keffer
> # Ported to Zortech V3.0
> # 
> #    Rev 1.2   01 Sep 1991 09:10:38   keffer
> # #include "rw/defs.h" to avoid Glock preprocessor nesting limits
> # 
> #    Rev 1.1   27 Aug 1991 11:47:16   keffer
> # Removed single argument constructors to avoid type conversion problems
> # 
>
>
> include macros 
>
/*
 * Functions for shape conversion for <T><M>, except
 * conversion to/from square matrices.
 *
 * Generated from template $Id: xcs.cpp,v 1.2 1993/10/11 21:48:07 alv Exp $
 *
> copyright
 */

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/lapkerr.h"
#include "rw/rwerr.h"
#include "rw/<tLetter><shapeAbbrev>mat.h"
>
> if <M>==TriDiagMat
#include "rw/<tLetter>bandmat.h"

<T>BandMat::<T>BandMat( const <T>TriDiagMat& A )
  : vec(A.dataVec())
{
  n=A.rows();
  band=3;
  bandu=1;
}

<T>TriDiagMat toTriDiagMat( const <T>BandMat& A )
{
  if (A.bandwidth()!=3 || A.upperBandwidth()!=1) {
    RWTHROW(RWInternalErr(RWMessage(RWLAPK_BANDSIZE,A.bandwidth(),3,A.upperBandwidth(),1)));
  }
  return <T><M>(A.dataVec(),A.rows(),A.cols());
}

>
> elif <M>==SymBandMat
>
#include "rw/<tLetter>bandmat.h"

<T>BandMat::<T>BandMat( const <T>SymBandMat& A )
  : vec(A.bandwidth()*A.rows(),rwUninitialized)
{
  n=A.rows();
  band =A.bandwidth();
  bandu=A.upperBandwidth();
  for(int i=0; i<=bandu; i++) {
    <T>Vec d = A.diagonal(i);
    diagonal(i) = d;
    if (i>0) diagonal(-i) = d;
  }
}

<T><M> toSymBandMat( const <T>BandMat& S )
{
  int n = S.rows();
  unsigned l=S.lowerBandwidth();
  unsigned u=S.upperBandwidth();
  if (l!=u) RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTSYM)));
  <T>SymBandMat A( n, n, u );
  if (n>=1) A.diagonal(0) = S.diagonal(0);
  for( int i=(u<n)?u:n; i>=1; i-- ) {
    <T>Vec Adiag(A.diagonal(i));
    Adiag = S.diagonal(i);		// Avoid generating temporaries
    Adiag += S.diagonal(-i);
> if <R/C>==Complex
    Adiag /= <T>(2,0);		// Explicit constructor necessary for Zortech
> else
    Adiag /= 2;
> endif
  }
  return A;
}

> elif <M>==HermBandMat
>
#include "rw/<tLetter>bandmat.h"

<T>BandMat::<T>BandMat( const <T>HermBandMat& A )
  : vec(A.bandwidth()*A.rows(),rwUninitialized)
{
  n=A.rows();
  band =A.bandwidth();
  bandu=A.upperBandwidth();
  for(int i=0; i<=bandu; i++) {
    <T>Vec d = A.dataVec().slice(bandu+i*bandu, n-i, bandu+1);
    diagonal(i) = d;
    if (i>0) diagonal(-i) = conj(d);
  }
}

<T><M> toHermBandMat( const <T>BandMat& S )
{
  int n = S.rows();
  unsigned l=S.lowerBandwidth();
  unsigned u=S.upperBandwidth();
  if (l!=u) RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTHERM)));
  <T>HermBandMat A( n, n, u );
  if (n>=1) A.dataVec().slice(u,n,u+1) = S.diagonal(0);
  for( int i=(u<n)?u:n; i>=1; i-- ) {
    <T>Vec Adiag(A.dataVec().slice(u+i*u, n-i, u+1));
    Adiag = S.diagonal(i);
    Adiag += conj(S.diagonal(-i));
> if <R/C>==Complex
    Adiag /= <T>(2,0);		// Explicit constructor necessary for Zortech
> else
    Adiag /= 2;
> endif
  }
  return A;
}
>
>
> else
> err No shape conversions for <M> other than to/from GenMat
> endif

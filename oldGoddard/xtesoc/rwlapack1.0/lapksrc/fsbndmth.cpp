#if 0
/*
 * Generated from template $Id: xmth.cpp,v 1.2 1993/10/11 21:48:17 alv Exp $
 *
 * Math member functions for the matrix types
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */
#endif

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/fsbndmat.h"

/*
 * This function returns a new matrix the same shape as the one 
 * passed in but with the data passed in.  This way we restrict
 * all the >if statements to a couple lines
 */
inline FloatSymBandMat sameShapeMatrix( FloatVec& vec, const FloatSymBandMat& A )
{
  return FloatSymBandMat(vec,A.rows(),A.cols(),A.lowerBandwidth());
}


static void verifyMatch(const FloatSymBandMat& A, const FloatSymBandMat& B)
{
    if (A.rows()!=B.rows() || A.cols()!=B.cols())
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATSIZE,A.rows(),A.cols(),B.rows(),B.cols())));
    if (A.bandwidth()!=B.bandwidth() || A.upperBandwidth()!=B.upperBandwidth())
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_BANDSIZE,A.bandwidth(),B.bandwidth(),
			A.upperBandwidth(), B.upperBandwidth())));
}

RWBoolean FloatSymBandMat::operator==(const FloatSymBandMat& X)
{
    if (n!=X.n || bandu!=X.bandu) return FALSE;
    int hb = halfBandwidth();
    if (hb==0) return ( vec==X.vec );	// A diagonal matrix
    /*
     * This is a little tricky cause some of the data vector is unused.
     * First check the last part of the data vector, then check the bits
     * at the beginning that are used.
     */
    int lastStart = hb*hb;
    int lastLen = (n-hb+1)*(hb+1)-1;
    if (n>=hb)
	if ( vec.slice(lastStart,lastLen)!=X.vec.slice(lastStart,lastLen) ) return FALSE;
    int el,len;
    for( el=hb,len=1 ; el<vec.length() && len<hb ; el+=hb,len++ ) {    
      if (vec.slice(el,len)!=X.vec.slice(el,len)) return FALSE;
    }
    return TRUE;
}


FloatSymBandMat& FloatSymBandMat::operator+=(const FloatSymBandMat& m)
{
  verifyMatch(*this,m);
  vec += m.vec;
  return *this;
}

FloatSymBandMat& FloatSymBandMat::operator-=(const FloatSymBandMat& m)
{
  verifyMatch(*this,m);
  vec -= m.vec;
  return *this;
}

FloatSymBandMat& FloatSymBandMat::operator*=(const FloatSymBandMat& m)
{
  verifyMatch(*this,m);
  vec *= m.vec;
  return *this;
}

FloatSymBandMat& FloatSymBandMat::operator*=(Float x)
{
  vec *= x;
  return *this;
}

FloatSymBandMat& FloatSymBandMat::operator/=(const FloatSymBandMat& m)
{
  verifyMatch(*this,m);
  vec /= m.vec;
  return *this;
}

FloatSymBandMat& FloatSymBandMat::operator/=(Float x)
{
  vec /= x;
  return *this;
}




FloatSymBandMat operator-(const FloatSymBandMat& A)
{
  FloatVec temp(-(A.dataVec()));
  return sameShapeMatrix(temp,A);
}
  
FloatSymBandMat operator+(const FloatSymBandMat& A)
{
  return A;
}

FloatSymBandMat operator*(const FloatSymBandMat& A, const FloatSymBandMat& B) {
	verifyMatch(A,B);
	FloatVec temp(A.dataVec()*B.dataVec());
	return sameShapeMatrix(temp,A);
}

FloatSymBandMat operator/(const FloatSymBandMat& A, const FloatSymBandMat& B) {
	verifyMatch(A,B);
	FloatVec temp(A.dataVec()/B.dataVec());
	return sameShapeMatrix(temp,A);
}

FloatSymBandMat operator+(const FloatSymBandMat& A, const FloatSymBandMat& B) {
	verifyMatch(A,B);
	FloatVec temp(A.dataVec()+B.dataVec());
	return sameShapeMatrix(temp,A);
}

FloatSymBandMat operator-(const FloatSymBandMat& A, const FloatSymBandMat& B) {
	verifyMatch(A,B);
	FloatVec temp(A.dataVec()-B.dataVec());
	return sameShapeMatrix(temp,A);
}

FloatSymBandMat operator*(const FloatSymBandMat& A, Float x) {
	FloatVec temp(A.dataVec()*x);
	return sameShapeMatrix(temp,A);
}

/*
 * Out-of-line version for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
  FloatSymBandMat operator*(Float x, const FloatSymBandMat& A) { return A*x; }
#endif

FloatSymBandMat operator/(const FloatSymBandMat& A, Float x) {
	FloatVec temp(A.dataVec()/x);
	return sameShapeMatrix(temp,A);
}





FloatSymBandMat abs(const FloatSymBandMat& A) {
	FloatVec temp(abs(A.dataVec()));
	return sameShapeMatrix(temp,A);
}



FloatSymBandMat transpose(const FloatSymBandMat& A) { return A; }

FloatVec product(const FloatSymBandMat& A, const FloatVec& x)
{
    if (A.cols()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATVECPROD,A.className,A.rows(),A.cols(),x.length())));
    }
    FloatVec y(A.rows(),rwUninitialized);
    y = (Float)(0);
    int l = A.lowerBandwidth();	// Getting these as ints avoids
    int u = A.upperBandwidth();	// conversion problems
    for( int i=(-l); i<=u; i++ ) {
      int iabs = (i>=0)?i:(-i);
      FloatVec yslice = y.slice( (i<0)?iabs:0, x.length()-iabs );
      FloatVec xslice = x.slice( (i>0)?iabs:0, x.length()-iabs );
      /* The * in the next line is element by element multiplication */
      yslice += A.diagonal(i) * xslice;
    }
    return y;
}

FloatVec product(const FloatVec& x, const FloatSymBandMat& A)
{
    if (A.rows()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECMATPROD,x.length(),A.className,A.rows(),A.cols())));
    }
    return product(A,x);
}



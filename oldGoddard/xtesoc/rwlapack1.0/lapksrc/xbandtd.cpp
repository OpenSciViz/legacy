> # $Header: /users/rcs/lapksrc/xbandtd.cpp,v 1.4 1993/10/11 21:48:05 alv Exp $
>  
> # $Log: xbandtd.cpp,v $
> # Revision 1.4  1993/10/11  21:48:05  alv
> # synch'ed with Math.h++ v5.1
> #
> # Revision 1.3  1993/07/05  19:19:33  alv
> # fixed up for complex numbers
> #
> # Revision 1.2  1993/04/06  20:45:04  alv
> # ported to sun C++ v3.0.1
> #
> # Revision 1.1  1993/04/06  15:44:41  alv
> # Initial revision
> #
>  
> define <T>=RW_T_RW
> if <T>==Double
> define <P>=Double <tLetter>=d <sLetter>=s <Sym>=Sym <sym>=sym <trd>=dsbtrd
> elif <T>==Float
> define <P>=Float  <tLetter>=f <sLetter>=s <Sym>=Sym <sym>=sym <trd>=ssbtrd
> elif <T>==DComplex
> define <P>=Double <tLetter>=c <sLetter>=h <Sym>=Herm <sym>=herm <trd>=zhbtrd
> else
> err Tri-diagonal decomposition: not defined for '<T>'
> endif
>  
/*
 * Implementation of <T><Sym>BandTriDiagDecomp
 *
 * Generated from template $Id: xbandtd.cpp,v 1.4 1993/10/11 21:48:05 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */ 

#include "rw/<tLetter>td.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/lapack.h"
#include "rw/<tLetter><sLetter>bndmat.h"

<T><Sym>BandTriDiagDecomp::<T><Sym>BandTriDiagDecomp(const <T><Sym>BandMat& A, RWBoolean keepQ)
{
  if (A.rows()>0 && A.bandwidth()>0) {
    char vect = (keepQ) ? 'V' : 'N';
    char uplo = 'U';   // Indicates to lapack that upper triangle is stored
    long n = A.rows();
    long kd = A.upperBandwidth();
    <T><Sym>BandMat Acopy = A.copy();
    long ldab = kd+1;
    <P>Vec D(A.rows(),rwUninitialized);
    <P>Vec E(A.rows()-1,rwUninitialized);
    if (keepQ) {Q.reshape(A.rows(),A.rows());}
    <T> *work = new <T> [n];
    long info;
    <trd>(vect,uplo,n,kd,(<T>*)Acopy.data(),ldab,(<P>*)D.data(),(<P>*)E.data(),(<T>*)Q.data(),n,work,info);
    RWPOSTCONDITION(info==0);
    delete work;
    set(D,E);
  }
}

<T>Vec <T><Sym>BandTriDiagDecomp::transform(const <P>Vec& V) const
{
  return <T><Sym>TriDiagDecomp::transform(V);
}

<T>GenMat <T><Sym>BandTriDiagDecomp::transform(const <P>GenMat& C) const
{
  if (C.cols()==0 || C.rows()==0) {
    return C;
  } else {
    if (C.rows()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,C.rows(),rows())));
    if (Q.rows()==0) RWTHROW(RWInternalErr(RWMessage(RWLAPK_QNOTCOMPUTED)));
    return product(Q,C);
  }
}

/*
 * The conversion functions to/from general matrices.
 *
 * Generated from template $Id: xcg.cpp,v 1.2 1993/07/05 19:19:33 alv Exp $
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/cbandmat.h"
#include "rw/cgenmat.h"

DComplexGenMat::DComplexGenMat( const DComplexBandMat& A )
  : RWMatView(A.rows(),A.cols(),sizeof(DComplex))
{
  zero();
  for( int k= -(int)A.lowerBandwidth(); k<=(int)A.upperBandwidth(); k++ ) {
    diagonal(k) = A.diagonal(k);
  }
}

DComplexBandMat toBandMat( const DComplexGenMat& S, unsigned bandl, unsigned bandu )
{
  int n = S.cols();
  DComplexBandMat A( S.rows(), n, bandl, bandu );
  for( int k= -(int)bandl; k<=(int)bandu; k++ ) {
    A.diagonal(k) = S.diagonal(k);
  }
  return A;
}


> #################################################################
> # The template file for tri-diagonal decompositions             #
> #                                                               #
> # $Header: /users/rcs/lapksrc/xtd.h,v 1.2 1993/04/06 20:45:04 alv Exp $
> #                                                               #
> # Copyright (1993) by Rogue Wave Software, Inc.  This           #
> # software is subject to copyright protection under the laws    #
> # of the United States and other countries.                     #
> #                                                               #
> # Written by Al Vermeulen.                                      #
> #                                                               #
> # Limited License.                                              #
> #################################################################
> #
> # $Log: xtd.h,v $
> # Revision 1.2  1993/04/06  20:45:04  alv
> # ported to sun C++ v3.0.1
> #
> # Revision 1.1  1993/04/06  15:44:44  alv
> # Initial revision
> #
>
> # Macros that we need
> 
> define <T>=RW_T_RW
> 
> if <T>==Double
> define <P>=Double <tLetter>=d <Sym>=Sym <sym>=sym
> elif <T>==Float
> define <P>=Float <tLetter>=f <Sym>=Sym <sym>=sym
> elif <T>==DComplex
> define <P>=Double <tLetter>=c <Sym>=Herm <sym>=herm
> else
> err Tri-diagonal decomposition: not defined for '<T>'
> endif
>
> beginwrapper <tLetter>td

/*
 * <T><Sym>TriDiagDecomp      - Abstract base class for tri-diagonal decompositions
 * <T><Sym>DenseTriDiagDecomp - Decomposition of a symmetric/Hermitian matrix
 * <T><Sym>BandTriDiagDecomp  - Decomposition of a symmetric/Hermitian banded matrix
 *
 * Generated from template $Id: xtd.h,v 1.2 1993/04/06 20:45:04 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * This class represents a decomposition of the form A=QTQ', where A is
 * symmetric/Hermitian, Q is orthogonal/unitary, and T is real tridiagonal.
 * This is usually done as a precursor to computing eigenvalues, since
 * the eigenvalues of A and T are the same, and those of T are easier to
 * compute.
 *
 * This class is used internally in the eigen-decomposition and
 * eigen-server classes.  The only reason a user should be interested
 * in this class is to implement his own routines to compute
 * eigenvalues/vectors of a tri-diagonal matrix as part of a new
 * eigen-server class.
 *
 * Each derived class must supply an implementation of the routine to
 * compute QC for a matrix C.  The routine to compute Qx for a vector x
 * by default calls the QC routine.
 *
 * The banded class can optionally not bother storing Q.  This can save
 * a lot of memory if the eigenvectors are not needed, or if you are
 * content to obtain the eigenvectors by inverse iteration later.
 */ 

#include "rw/lapkdefs.h"
#include "rw/<tLetter>vec.h"
#include "rw/<tLetter>genmat.h"

class <T>SymMat;
class <T>SymBand;

class <T><Sym>TriDiagDecomp {
private:
  <P>Vec diag;    // diagonal of T
  <P>Vec offdiag; // sub/super diagonal of T
public:
  void set(const <P>Vec& diag, const <P>Vec& offdiag);
  virtual <T>Vec transform(const <P>Vec&) const;        // multiply by Q
  virtual <T>GenMat transform(const <P>GenMat&) const =0;  // multiply by Q
  const <P>Vec diagonal()    const {return diag;}
  const <P>Vec offDiagonal() const {return offdiag;}
  unsigned     rows()        const {return diag.length();}
  unsigned     cols()        const {return diag.length();}
};

class <T><Sym>DenseTriDiagDecomp : public <T><Sym>TriDiagDecomp {
private:
  <T>Vec Qdata;   // Data necessary to generate Q (if keepQ==TRUE during construction)
  <T>Vec tau;     // Scalar factors of the elementary reflectors
public:
  <T><Sym>DenseTriDiagDecomp(const <T><Sym>Mat&);
  virtual <T>Vec transform(const <P>Vec&) const;        // multiply by Q
  virtual <T>GenMat transform(const <P>GenMat&) const;     // multiply by Q
};

class <T><Sym>BandTriDiagDecomp : public <T><Sym>TriDiagDecomp {
private:
  <T>GenMat Q;       // The matrix Q (if keepQ was TRUE during construction)
public:
  <T><Sym>BandTriDiagDecomp(const <T><Sym>BandMat&, RWBoolean keepQ=TRUE);
  virtual <T>GenMat transform(const <P>GenMat&) const;     // multiply by Q
  virtual <T>Vec transform(const <P>Vec&) const;        // multiply by Q
};
  
> endwrapper

 

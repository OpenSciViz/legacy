/*
 * defs for FloatLeastSqSV
 *
 * Generated from template $Id: xlssv.cpp,v 1.3 1993/10/11 21:48:15 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 */

#include "rw/flssv.h"
#include "rw/lapkerr.h"
#include "rw/rwerr.h"

FloatLeastSqSV::FloatLeastSqSV()                                 : FloatSVDecomp() {}
FloatLeastSqSV::FloatLeastSqSV(const FloatGenMat& A, Float tol)      : FloatSVDecomp(A,tol) {}
FloatLeastSqSV::FloatLeastSqSV(const FloatSVDecomp& A, Float tol)    : FloatSVDecomp(A) {truncate(tol);}

static FloatGenMat U(const FloatLeastSqSV& x)
{
  if (x.numLeftVectors()<x.rank()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOSV,x.rank())));
  return x.leftVectors()(RWAll,RWSlice(0,x.rank()));
}

static FloatGenMat V(const FloatLeastSqSV& x)
{
  if (x.numRightVectors()<x.rank()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOSV,x.rank())));
  return x.rightVectors()(RWAll,RWSlice(0,x.rank()));
}

FloatVec FloatLeastSqSV::residual(const FloatVec& data) const
{
  if (data.length()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,data.length(),rows())));
  FloatVec x = solve(data);
  FloatVec VTx = product(adjoint(V(*this)),x);
  VTx *= singularValues();
  FloatVec Ax = product(U(*this),VTx);
  return data-Ax;
}

Float FloatLeastSqSV::residualNorm(const FloatVec& data) const
{
  if (data.length()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,data.length(),rows())));
  FloatVec r = residual(data);
  return l2Norm(r);
}

FloatVec FloatLeastSqSV::solve(const FloatVec& data) const
{
  if (data.length()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,data.length(),rows())));
  FloatVec x = product(adjoint(U(*this)),data);
  x /= singularValues();
  return product(V(*this),x);
} 

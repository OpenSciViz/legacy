/*
 * Implementation of DComplexEigDecomp
 *
 * Generated from template $Id: xeig.cpp,v 1.3 1993/10/11 21:48:08 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 */ 

#include "rw/ceig.h"
#include "rw/ceigsrv.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"

typedef DComplexSchurEigServer DefaultServer;

DComplexEigDecomp::DComplexEigDecomp()
{
  n=0;
  computedAll = accurate = TRUE;
}

DComplexEigDecomp::DComplexEigDecomp(const DoubleEigDecomp& x)
: n(x.n),
  lambda(x.lambda),
  P(x.P),
  Q(x.Q),
  computedAll(x.computedAll),
  accurate(x.accurate)
{
}

DComplexEigDecomp::DComplexEigDecomp(const DComplexGenMat& A, RWBoolean computeVecs)
{
  factor(A,computeVecs);
}

void DComplexEigDecomp::factor(const DComplexGenMat& A, RWBoolean computeVecs)
{
  DefaultServer server;
  server.computeLeftEigenVectors(computeVecs);
  server.computeRightEigenVectors(computeVecs);
  *this = server(A);
}
  
void DComplexEigDecomp::operator=(const DComplexEigDecomp& A)
{
  n = A.n;
  computedAll = A.computedAll;
  accurate = A.accurate;
  lambda.reference(A.lambda);
  P.reference(A.P);
  Q.reference(A.Q);
}

DComplex       DComplexEigDecomp::eigenValue(int i)  const
{
  if (i<0 || i>=lambda.length()) RWTHROW(RWBoundsErr(RWMessage(RWLAPK_NOEIG,i)));
  return lambda(i);
}

const DComplexVec    DComplexEigDecomp::leftEigenVector(int i) const
{
  if (i<0 || i>=P.cols()) RWTHROW(RWBoundsErr(RWMessage(RWLAPK_NOEIG,i)));
  RWPOSTCONDITION(lambda.length()>i); // if this isn't true something went wrong
  return P.col(i);
}

const DComplexGenMat DComplexEigDecomp::leftEigenVectors() const
{
  return P;
}

const DComplexVec    DComplexEigDecomp::rightEigenVector(int i) const
{
  if (i<0 || i>=Q.cols()) RWTHROW(RWBoundsErr(RWMessage(RWLAPK_NOEIG,i)));
  RWPOSTCONDITION(lambda.length()>i); // if this isn't true something went wrong
  return Q.col(i);
}

const DComplexGenMat DComplexEigDecomp::rightEigenVectors() const
{
  return Q;
}

RWBoolean DComplexEigDecomp::good()          const
{
  return computedAll && accurate;
}

RWBoolean DComplexEigDecomp::inaccurate()    const
{
  return computedAll && !accurate;
}

RWBoolean DComplexEigDecomp::fail()          const
{
  return !computedAll;
}

/*
 * defs for DoubleLeastSqSV
 *
 * Generated from template $Id: xlssv.cpp,v 1.3 1993/10/11 21:48:15 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 */

#include "rw/dlssv.h"
#include "rw/lapkerr.h"
#include "rw/rwerr.h"

DoubleLeastSqSV::DoubleLeastSqSV()                                 : DoubleSVDecomp() {}
DoubleLeastSqSV::DoubleLeastSqSV(const DoubleGenMat& A, Double tol)      : DoubleSVDecomp(A,tol) {}
DoubleLeastSqSV::DoubleLeastSqSV(const DoubleSVDecomp& A, Double tol)    : DoubleSVDecomp(A) {truncate(tol);}

static DoubleGenMat U(const DoubleLeastSqSV& x)
{
  if (x.numLeftVectors()<x.rank()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOSV,x.rank())));
  return x.leftVectors()(RWAll,RWSlice(0,x.rank()));
}

static DoubleGenMat V(const DoubleLeastSqSV& x)
{
  if (x.numRightVectors()<x.rank()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOSV,x.rank())));
  return x.rightVectors()(RWAll,RWSlice(0,x.rank()));
}

DoubleVec DoubleLeastSqSV::residual(const DoubleVec& data) const
{
  if (data.length()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,data.length(),rows())));
  DoubleVec x = solve(data);
  DoubleVec VTx = product(adjoint(V(*this)),x);
  VTx *= singularValues();
  DoubleVec Ax = product(U(*this),VTx);
  return data-Ax;
}

Double DoubleLeastSqSV::residualNorm(const DoubleVec& data) const
{
  if (data.length()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,data.length(),rows())));
  DoubleVec r = residual(data);
  return l2Norm(r);
}

DoubleVec DoubleLeastSqSV::solve(const DoubleVec& data) const
{
  if (data.length()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,data.length(),rows())));
  DoubleVec x = product(adjoint(U(*this)),data);
  x /= singularValues();
  return product(V(*this),x);
} 

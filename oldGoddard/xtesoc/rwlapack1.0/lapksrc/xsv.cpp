> #################################################################
> # Template for singular value decomposition implementation      #
> #                                                               #
> # $Header: /users/rcs/lapksrc/xsv.cpp,v 1.4 1993/10/11 21:48:24 alv Exp $
> #                                                               #
> # Copyright (1993) by Rogue Wave Software, Inc.  This           #
> # software is subject to copyright protection under the laws    #
> # of the United States and other countries.                     #
> #                                                               #
> # Written by Al Vermeulen.                                      #
> #                                                               #
> # Limited License.                                              #
> #################################################################
> #
> # $Log: xsv.cpp,v $
> # Revision 1.4  1993/10/11  21:48:24  alv
> # synch'ed with Math.h++ v5.1
> #
> # Revision 1.3  1993/07/08  23:26:13  alv
> # ported to SUN CC v3
> #
> # Revision 1.2  1993/07/05  19:19:33  alv
> # fixed up for complex numbers
> #
> # Revision 1.1  1993/05/18  17:00:02  alv
> # Initial revision
> #
>
> # Macros that we need
> 
> define <T>=RW_T_RW
> 
> if <T>==Double
> define <P>=Double <tLetter>=d <lLetter>=d <ormbr>=dormbr
> elif <T>==Float
> define <P>=Float  <tLetter>=f <lLetter>=s <ormbr>=sormbr
> elif <T>==DComplex
> define <P>=Double <tLetter>=c <lLetter>=z <ormbr>=zunmbr
> else
> err SVD: not defined for '<T>'
> endif
>          
/*
 * Defs for <T>SVDecomp
 *
 * Generated from template $Id: xsv.cpp,v 1.4 1993/10/11 21:48:24 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 */ 

#include "rw/lapkdefs.h"
#include "rw/lapack.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/<tLetter>sv.h"
#include "rw/<tLetter>genmat.h"
#include "rw/<tLetter>vec.h"

<T>SVDecomp::<T>SVDecomp()
{
  computedAll_ = TRUE;
}

<T>SVDecomp::<T>SVDecomp(const <T>SVDecomp& x)
{
  *this = x;
}

<T>SVDecomp::<T>SVDecomp(const <T>GenMat& X, <P> tol)
{
  factor(X,tol);
}

void <T>SVDecomp::factor(const <T>GenMat& X, <P> tol)
{
  <T>SVServer s;
  s.setTolerance(tol);
  *this = s(X);
}
  
void <T>SVDecomp::operator=(const <T>SVDecomp& x)
{
  sigma_.reference(x.sigma_);
  U_.reference(x.U_);
  VT_.reference(x.VT_);
  computedAll_ = x.computedAll_;
}

<P>             <T>SVDecomp::singularValue(int i) const
{
  if (i>=0 && i<sigma_.length()) return sigma_(i);
  else return 0;
}

const <T>Vec    <T>SVDecomp::leftVector(int i)    const
{                                                     
  if (i<0 || i>=U_.cols()) RWTHROW(RWBoundsErr(RWMessage(RWLAPK_NOSV,i)));
  return U_.col(i);
}

const <T>Vec    <T>SVDecomp::rightVector(int i)   const
{                                                     
  if (i<0 || i>=VT_.rows()) RWTHROW(RWBoundsErr(RWMessage(RWLAPK_NOSV,i)));
> if <T>==<P>
  return VT_.row(i);
> else
  return conj(VT_.row(i));
> endif
}

void            <T>SVDecomp::truncate(<P> tol)
{
  RWPRECONDITION(tol>=0);
  int r = sigma_.length();
  while(r>0 && sigma_(r-1)<=tol) --r;
  sigma_.resize(r);
}

<T>SVServer::<T>SVServer()
{
  computeAllLeftVecs_ = FALSE;       
  computeAllRightVecs_ = FALSE;    
  computeDefaultLeftVecs_ = TRUE; 
  computeDefaultRightVecs_ = TRUE;
  numLeftVecs_ = 0;
  numRightVecs_ = 0;           
  tol_ = 0;
}

void        <T>SVServer::computeAllVectors()
{
  computeAllLeftVectors();
  computeAllRightVectors();
}

void        <T>SVServer::computeAllLeftVectors()
{
  computeAllLeftVecs_ = TRUE;
}

void        <T>SVServer::computeAllRightVectors()
{
  computeAllRightVecs_ = TRUE;
}

void        <T>SVServer::computeDefaultVectors()
{
  computeDefaultLeftVectors();
  computeDefaultRightVectors();
}

void        <T>SVServer::computeDefaultLeftVectors()
{
  computeAllLeftVecs_ = FALSE;
  computeDefaultLeftVecs_ = TRUE;
}

void        <T>SVServer::computeDefaultRightVectors()
{
  computeAllRightVecs_ = FALSE;
  computeDefaultRightVecs_ = TRUE;
}

void        <T>SVServer::computeVectors(unsigned n)
{
  computeLeftVectors(n);
  computeRightVectors(n);
}

void        <T>SVServer::computeLeftVectors(unsigned n)
{
  computeAllLeftVecs_ = computeDefaultLeftVecs_ = FALSE;
  numLeftVecs_ = n;
}

void        <T>SVServer::computeRightVectors(unsigned n)
{
  computeAllRightVecs_ = computeDefaultRightVecs_ = FALSE;
  numRightVecs_ = n;
}
       
void        <T>SVServer::setTolerance(<P> x)
{
  tol_ = x;
}

<T>SVDecomp <T>SVServer::operator()(const <T>GenMat& A) const
{
  <T>SVDecomp decomp;   // We'll return this someday

  // First set up parameters for and then call sgebrd to generate
  // bidiagonal reduction of A
  long m = A.rows();
  long n = A.cols();
  <T>GenMat B = A.copy();
  long lda = B.colStride();
  unsigned maxrank = (A.rows()<A.cols()) ? A.rows() : A.cols();
  <P>Vec d(maxrank,rwUninitialized);
  <P>Vec e(maxrank,rwUninitialized);   // one longer than necessary to avoid <0 checks
  <T>Vec tauq(maxrank,rwUninitialized);
  <T>Vec taup(maxrank,rwUninitialized);
  long nb = ilaenv( 1, "DGEBRD", " ", m, n, -1, -1 );
  long lwork = (m+n)*nb + 3*n;   // ensure work is big enough for sbdsqr() too
  <T> *work = new <T> [lwork];
  long info;
  <lLetter>gebrd(m,n,B.data(),lda,d.data(),e.data(),tauq.data(),taup.data(),work,lwork,info);
  RWPOSTCONDITION(info==0);
  RWPOSTCONDITION(lwork>=work[0]);

  // Compute the SVD of B.  If we need any left singular vectors,
  // compute Q, the left vecs of B, if we need any right vectors,
  // compute PT, the right vecs of B.
  char uplo = (m>=n) ? 'U' : 'L';
  long npt = 0;
  long nrq = 0;
  long ldpt = 1;
  long ldq = 1;
  if (computeAllLeftVecs_ || computeDefaultLeftVecs_ || numLeftVecs_>0) {
    decomp.U_.reshape(maxrank,maxrank);
    decomp.U_ = (<T>)0;
    decomp.U_.diagonal() = (<T>)1;
    nrq = maxrank;
    ldq = maxrank;
  }
  if (computeAllRightVecs_ || computeDefaultRightVecs_ || numRightVecs_>0) {
    decomp.VT_.reshape(maxrank,maxrank);
    decomp.VT_ = (<T>)0;
    decomp.VT_.diagonal() = (<T>)1;
    npt = maxrank;
    ldpt = maxrank;
  }
  // In the complex case, work needs to be a real array here.  I just
  // put a cast in, since it should always be OK to case a pointer to
  // a (C-style) array of complex to an array of reals
  <lLetter>bdsqr(uplo,maxrank,npt,nrq,0,d.data(),e.data(),decomp.VT_.data(),ldpt,decomp.U_.data(),ldq,(<T>*)0,1,(<P>*)work,info);
  RWPOSTCONDITION(info>=0);
  if (info!=0) {
    decomp.computedAll_ = FALSE;
    decomp.U_.reshape(B.rows(),0);
    decomp.VT_.reshape(0,B.cols());
  } else {
    
    // fill in the singular values
    decomp.sigma_.reference(d);
    decomp.truncate(tol_);
    int r = decomp.rank();

    // compute the left singular vectors, if required
    int p = numLeftVecs_;
    if (computeDefaultLeftVecs_) p = r;
    if (p>A.rows() || computeAllLeftVecs_) p = A.rows();
    if (p>0) {
      decomp.U_.resize(A.rows(),p);
      for(int i=maxrank; i<p; i++) decomp.U_(i,i) = 1; // set the new part of decomp.U_ to identity
      <ormbr>( 'Q','L','N',
               decomp.U_.rows(),
               decomp.U_.cols(),
               B.cols(),
               B.data(),
               B.colStride(),
               tauq.data(),
               decomp.U_.data(),
               decomp.U_.colStride(),
               work,lwork,info);
      RWPOSTCONDITION(info==0);
    } else {
      decomp.U_.reshape(B.rows(),0);   // don't want any left vecs
    }

    // compute the right singular vectors, if required
    int q = numRightVecs_;
    if (computeDefaultRightVecs_) q = r;
    if (q>A.cols() || computeAllRightVecs_) q = A.cols();
    if (q>0) {
      decomp.VT_.resize(q,B.cols());
      for(int i=maxrank; i<q; i++) decomp.VT_(i,i) = 1; // set the new part of decomp.VT_ to identity
      <ormbr>( 'P','R',
> if <T>!=<P>
               'C',
> else
               'T',
> endif
               decomp.VT_.rows(),
               decomp.VT_.cols(),
               B.rows(),
               B.data(),
               B.colStride(),
               taup.data(),
               decomp.VT_.data(),
               decomp.VT_.colStride(),
               work,lwork,info);
      RWPOSTCONDITION(info==0);
    } else {
      decomp.VT_.reshape(0,B.cols());  // don't want any right vecs
    }
  }

  delete work;
  return decomp;
}

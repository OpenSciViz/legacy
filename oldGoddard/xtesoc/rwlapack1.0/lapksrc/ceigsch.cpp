/*
 * Implementation of DComplexEigSchurServer
 *
 * Generated from template $Id: xeigsch.cpp,v 1.4 1993/10/11 21:48:10 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
  * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */ 

#include "rw/ceigsrv.h"
#include "rw/cschur.h"
#include "rw/lapack.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"

DComplexSchurEigServer::DComplexSchurEigServer(RWBoolean left, RWBoolean right, RWBoolean sc, RWBoolean perm)
  : selectRange_((const RWSlice&)RWAll)   // Explicit cast prevents SUN error
{
  computeLeftVecs_  = left;
  computeRightVecs_ = right;
  scale_            = sc;
  permute_          = perm;
}

RWBoolean DComplexSchurEigServer::computeLeftEigenVectors() const       {return computeLeftVecs_;}
void      DComplexSchurEigServer::computeLeftEigenVectors(RWBoolean x)  {computeLeftVecs_ = x;}
RWBoolean DComplexSchurEigServer::computeRightEigenVectors() const      {return computeRightVecs_;}
void      DComplexSchurEigServer::computeRightEigenVectors(RWBoolean x) {computeRightVecs_ = x;}
RWBoolean DComplexSchurEigServer::scale()                    const      {return scale_;}
void      DComplexSchurEigServer::scale(RWBoolean x)                    {scale_ = x;}
RWBoolean DComplexSchurEigServer::permute()                  const      {return permute_;}
void      DComplexSchurEigServer::permute(RWBoolean x)                  {permute_ = x;}
void      DComplexSchurEigServer::balance(RWBoolean x)                  {scale_ = permute_ = x;}

void DComplexSchurEigServer::selectEigenVectors(const IntVec& s)
{
  selectVec_.reference(s.copy());
  selectRange_ = RWAll;
}

void DComplexSchurEigServer::selectEigenVectors(const RWSlice& s)
{
  selectRange_ = s;
  selectVec_.reshape(0);
}

RWBoolean DComplexSchurEigServer::computeAllEigenVectors() const
{
  // The next line needs to determine if the RWSlice object selectRange_
  // is a copy of RWAll.  This means it will have a begin of zero, a stride
  // of one, and a length which matches whatever length the argument vector
  // is.  Test for this by asking for two lengths, if the slice gives back
  // the two lengths, then it must be an RWAll.
  RWBoolean isRWAll = (selectRange_.begin()==0 && selectRange_.stride()==1 && selectRange_.len(5)==5 && selectRange_.len(6)==6);
  return (isRWAll && selectVec_.length()==0);
}

DComplexEigDecomp DComplexSchurEigServer::operator()(const DComplexGenMat& A)
{
  DComplexSchurDecomp x(A,permute_,scale_);
  return (*this)(x);
}

DComplexEigDecomp DComplexSchurEigServer::operator()(const DComplexBalanceDecomp& A)
{
  DComplexSchurDecomp x(A);
  return (*this)(x);
}

DComplexEigDecomp DComplexSchurEigServer::operator()(const DComplexHessenbergDecomp& A)
{    
  DComplexSchurDecomp x(A);
  return (*this)(x);
}

DComplexEigDecomp DComplexSchurEigServer::operator()(const DComplexSchurDecomp& A)
{
  // First build a container and set up the eigenvalues
  DComplexEigDecomp eig;
  n(eig) = A.rows();
  lambda(eig).reference(A.eigenvalues());

  if (computeAllEigenVectors()) {
    DComplexGenMat vl;
    DComplexGenMat vr;
    if (computeLeftVecs_)  { vl.reshape(A.rows(),A.rows()); }
    if (computeRightVecs_) { vr.reshape(A.rows(),A.rows()); }
    if (computeLeftVecs_ || computeRightVecs_) {
      char job = (computeLeftVecs_ ? (computeRightVecs_ ? 'B' : 'L') : 'R');
      DComplexGenMat T = A.T();
      long ldvl = rwmax(1,vl.colStride());   // max to cover the case of colStride zero
      long ldvr = rwmax(1,vr.colStride());
      long m;
      long info;
      DComplex *work = new DComplex [3*A.rows()];
      Double *rwork = new Double [A.rows()];
      ztrevc(job,'A',0,A.rows(),T.data(),T.colStride(),vl.data(),ldvl,
                            vr.data(),ldvr,A.rows(),m,work,rwork,info);
      delete [] rwork;
      RWPOSTCONDITION(info==0);
      delete work;
      // Now transform and de-balance the eigenvectors
      if (computeLeftVecs_) { P(eig).reference(A.BInvTX(A.ZX(vl))); }
      if (computeRightVecs_){ Q(eig).reference(A.BX(A.ZX(vr))); }
    }
  } else {   

    // Compute only selected eigenvectors
    IntVec select(n(eig),0);
    if (selectVec_.length()!=0) {
      for(int i=selectVec_.length(); i--;) {
        int x = selectVec_(i);
        if (x<0 || x>=n(eig)) RWTHROW(RWBoundsErr(RWMessage(RWLAPK_NOEIG,x)));
        select(x) = 1;
      }
    } else {  // which to compute is indicated by the RWSlice
      selectRange_.boundsCheck(n(eig));
      select(selectRange_) = 1;
    }

    long mm=0;        // compute number of eigenvectors desired
    for(int i=select.length(); i--;) { if (select(i)) mm++; }
    DComplexGenMat vl;
    DComplexGenMat vr;
    if (computeLeftVecs_)  { vl.reshape(A.rows(),(unsigned)mm); }
    if (computeRightVecs_) { vr.reshape(A.rows(),(unsigned)mm); }
    if (mm>0 && (computeLeftVecs_ || computeRightVecs_)) {
      char job = (computeLeftVecs_ ? (computeRightVecs_ ? 'B' : 'L') : 'R');
      DComplexGenMat T = A.T();
      long ldvl = rwmax(1,vl.colStride());   // max to cover the case of colStride zero
      long ldvr = rwmax(1,vr.colStride());
      long m;
      long info;
      IntVec selectcopy = select.copy();     // Because dtrevc modifies select
      DComplex *work = new DComplex [3*A.rows()];
      Double *rwork = new Double [A.rows()];
      ztrevc(job,'S',selectcopy.data(),A.rows(),T.data(),T.colStride(),vl.data(),ldvl,
                            vr.data(),ldvr,mm,m,work,rwork,info);
      delete rwork;
      RWPOSTCONDITION(info==0);
      RWPOSTCONDITION(m==mm);
      delete work;
      // Now transform and de-balance the eigenvectors
      if (computeLeftVecs_) { P(eig).reference(A.BInvTX(A.ZX(vl))); }
      if (computeRightVecs_){ Q(eig).reference(A.BX(A.ZX(vr))); }

      // Finally, move the eigenvalues corresponding the the chosen
      // eigenvectors to the front so the positions match up.  Do this
      // in two passes.
      DComplexVec temp = lambda(eig);
      lambda(eig).deepenShallowCopy();
      int index=0;
      for(int i=0; i<temp.length(); i++) {
        if (select(i)) lambda(eig)(index++) = temp(i);
      }
      for(i=0; i<temp.length(); i++) {
        if (!select(i)) lambda(eig)(index++) = temp(i);
      }
      RWPOSTCONDITION(index==lambda(eig).length());
    }
  }
  return eig;
}

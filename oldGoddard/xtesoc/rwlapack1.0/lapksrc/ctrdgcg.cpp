/*
 * The conversion functions to/from general matrices.
 *
 * Generated from template $Id: xcg.cpp,v 1.2 1993/07/05 19:19:33 alv Exp $
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/ctrdgmat.h"
#include "rw/cgenmat.h"

DComplexGenMat::DComplexGenMat( const DComplexTriDiagMat& A )
  : RWMatView(A.rows(),A.cols(),sizeof(DComplex))
{
  zero();
  if (nrows>0) { diagonal(0) = A.diagonal(0); }
  if (nrows>1) {
    diagonal(-1) = A.diagonal(-1);
    diagonal(1) = A.diagonal(1);
  }
}

DComplexTriDiagMat toTriDiagMat( const DComplexGenMat& S )
{
  int n = S.cols();
  DComplexTriDiagMat A(S.rows(),n);
  if (n>0) { A.diagonal(0) = S.diagonal(0); }
  if (n>1) {
    A.diagonal(-1) = S.diagonal(-1);
    A.diagonal(1) = S.diagonal(1);
  }
  return A;
}


/*
 * Definitions for FloatCODecomp
 *
 * Generated from template $Id: xco.cpp,v 1.4 1993/10/11 21:48:06 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 */

#include "rw/lapack.h"
#include "rw/fco.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/futrimat.h"

FloatCODecomp::FloatCODecomp()
{
}

FloatCODecomp::FloatCODecomp(const FloatCODecomp& x)
{
  *this = x;
}

void FloatCODecomp::operator=(const FloatCODecomp& x)
{
  QRdecomp_ = x.QRdecomp_;
  Ztau_.reference(x.Ztau_);
  TZ_.reference(x.TZ_);
}

void FloatCODecomp::dofactor(double tol)
{
  long n = cols();
  int m = (rows()<cols()) ? rows() : cols();
  while (m>0 && fabs(QRdecomp_.QR_(m-1,m-1))<tol) m--;
  TZ_.reference(QRdecomp_.QR_(RWSlice(0,m),RWAll));
  Ztau_.reshape(m);
  long info;
  stzrqf(m,n,TZ_.data(),TZ_.colStride(),Ztau_.data(),info);
  RWPOSTCONDITION(info==0);
}

FloatCODecomp::FloatCODecomp(const FloatGenMat& A, double tol)
  : QRdecomp_(A)
{
  dofactor(tol);   
}

FloatCODecomp::FloatCODecomp(const FloatQRDecomp& A, double tol)
  : QRdecomp_(A)
{
  QRdecomp_.QR_.deepenShallowCopy();
  dofactor(tol);   
}

void FloatCODecomp::factor(const FloatGenMat& A, double tol)
{
  QRdecomp_.factor(A);
  dofactor(tol);   
}

void FloatCODecomp::factor(const FloatQRDecomp& A, double tol)
{
  QRdecomp_ = A;
  QRdecomp_.QR_.deepenShallowCopy();
  dofactor(tol);   
}

FloatCODecomp::~FloatCODecomp()
{
}

FloatUpperTriMat FloatCODecomp::T() const
{
  return toUpperTriMat(TZ_.operator()(RWAll,RWSlice(0,rank())));  // explicit op() to shut up sun 3.0.1 warning
}

// Generate Z as Z = I P1 P2 P3 ... Pn
//
FloatGenMat      FloatCODecomp::Z() const
{
  FloatGenMat Zmat(cols(),cols(),(Float)0);  // initialize Z to identity matrix
  Zmat.diagonal() = (Float)1;
  if (cols()>rank()) {
    long m = cols();
    long n = m-rank()+1;
    Float *work = new Float [m];
    FloatGenMat ZmatRight = Zmat(RWAll,RWToEnd(rank()));
    FloatGenMat TZRight = TZ_.operator()(RWAll,RWToEnd(rank()));  // explicit op() to shut up sun 3.0.1 warning
    Float *c2 = ZmatRight.data();
    long ldc = Zmat.colStride();
    long incv = TZRight.colStride();
    for(int i=rank(); i--;) {
      Float *v = TZRight.row(i).data();
      Float *c1 = Zmat.col(i).data();
      slatzm('R',m,n,v,incv,Ztau_(i),c1,c2,ldc,work);
    }
    delete [] work;
  }
  return Zmat;   
}

FloatVec FloatCODecomp::Tx(const FloatVec& x) const
{
  if (x.length()!=rank()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),rank())));
  if (cols()>rank()) {
    FloatUpperTriMat T = toUpperTriMat(TZ_.operator()(RWAll,RWSlice(0,rank())));  // explicit op() to shut up sun 3.0.1 warning
    return product(T,x);
  } else {    // T==R
    return QRdecomp_.Rx(x);
  }
}

FloatVec FloatCODecomp::TTx(const FloatVec& x) const
{
  if (x.length()!=rank()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),rank())));
  if (cols()>rank()) {
    FloatUpperTriMat T = toUpperTriMat(TZ_.operator()(RWAll,RWSlice(0,rank())));  // explicit op() to shut up sun 3.0.1 warning
    return product(x,T);
  } else {    // T==R
    return QRdecomp_.RTx(x);
  }
}

FloatVec FloatCODecomp::Tinvx(const FloatVec& x) const
{
  if (x.length()!=rank()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),rank())));
  FloatVec y = x.copy();
  long n = rank();
  long lda = TZ_.colStride();
  long one = 1;
  long info;
  strtrs('U','N','N',n,one,(Float*)TZ_.data(),lda,y.data(),n,info);
  RWPOSTCONDITION(info==0);
  return y;
}

FloatVec FloatCODecomp::TTinvx(const FloatVec& x) const
{
  if (x.length()!=rank()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),rank())));
  FloatVec y = x.copy();
  long n = rank();
  long lda = TZ_.colStride();
  long one = 1;
  long info;
  strtrs('U','T','N',n,one,(Float*)TZ_.data(),lda,y.data(),n,info);
  RWPOSTCONDITION(info==0);
  return y;
}

FloatVec FloatCODecomp::Zx(const FloatVec& x) const
{
  if (x.length()!=cols()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),cols())));  
  FloatVec y = x.copy();
  if (cols()>rank()) {
    long n = 1;
    long m = cols()-rank()+1;
    Float work[1];
    FloatGenMat TZRight = TZ_.operator()(RWAll,RWToEnd(rank()));  // explicit op() to shut up sun 3.0.1 warning
    Float *c2 = y.data() + rank();
    long ldc = y.length();
    long incv = TZRight.colStride();
    for(int i=0; i<rank(); i++) {
      Float *c1 = &(y(i));
      Float *v = TZRight.row(i).data();
      slatzm('L',m,n,v,incv,Ztau_(i),c1,c2,ldc,work);
    }
  }
  return y;   
}

FloatVec FloatCODecomp::ZTx(const FloatVec& x) const
{
  if (x.length()!=cols()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),cols())));  
  FloatVec y = x.copy();
  if (cols()>rank()) {
    long m = 1;
    long n = cols()-rank()+1;
    Float work[1];
    FloatGenMat TZRight = TZ_.operator()(RWAll,RWToEnd(rank()));  // explicit op() to shut up sun 3.0.1 warning
    Float *c2 = y.data() + rank();
    long ldc = 1;
    long incv = TZRight.colStride();
    for(int i=rank(); i--;) {
      Float *c1 = &(y(i));
      Float *v = TZRight.row(i).data();
      slatzm('R',m,n,v,incv,Ztau_(i),c1,c2,ldc,work);
    }
  }
  return y;
}


> #################################################################
> # The template file for Schur decomposition class               #
> #                                                               #
> # $Header: /users/rcs/lapksrc/xschur.h,v 1.1 1993/05/18 17:00:02 alv Exp $
> #                                                               #
> # Copyright (1993) by Rogue Wave Software, Inc.  This           #
> # software is subject to copyright protection under the laws    #
> # of the United States and other countries.                     #
> #                                                               #
> # Written by Al Vermeulen.                                      #
> #                                                               #
> # Limited License.                                              #
> #################################################################
> #
> # $Log: xschur.h,v $
> # Revision 1.1  1993/05/18  17:00:02  alv
> # Initial revision
> #
>
> # Macros that we need
> 
> define <T>=RW_T_RW
> 
> if <T>==Double
> define <P>=Double <tLetter>=d <Z>=DComplex <zLetter>=c
> elif <T>==Float
> define <P>=Float  <tLetter>=f <Z>=FComplex <zLetter>=b
> elif <T>==DComplex
> define <P>=Double <tLetter>=c <Z>=DComplex <zLetter>=c
> else
> err Schur decomposition: not defined for '<T>'
> endif
>
> beginwrapper <tLetter>schur
>
/*
 * <T>SchurDecomp - Schur decomposition of a matrix
 *
 * Generated from template $Id: xschur.h,v 1.1 1993/05/18 17:00:02 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 * The Schur decomposition is often more useful than a full
 * eigendecomposition.  It provides the eigenvalues, and orthogonal
 * bases for invariant subspaces --- what more could you want?
 *
 * This class includes an optional balancing transformation in addition
 * to the standard Schur decomposition.
 */ 

#include "rw/lapkdefs.h"
#include "rw/<tLetter>vec.h"
#include "rw/<zLetter>vec.h"
#include "rw/<tLetter>genmat.h"
#include "rw/<tLetter>bal.h"
class <T>HessenbergDecomp;

class <T>SchurDecomp {
private:
  <T>BalanceTransform B_;  // Balancing transformation
  <T>GenMat           T_;  // Schur matrix
  <T>GenMat           Z_;  // Schur vectors
  <Z>Vec              w_;  // Eigenvalues

public:                                          
  <T>SchurDecomp();
  <T>SchurDecomp(const <T>GenMat&, RWBoolean permute=TRUE, RWBoolean scale=TRUE);
  <T>SchurDecomp(const <T>BalanceDecomp&);
  <T>SchurDecomp(const <T>HessenbergDecomp&);
  void factor(const <T>GenMat&, RWBoolean permute=TRUE, RWBoolean scale=TRUE);
  void factor(const <T>BalanceDecomp&);
  void factor(const <T>HessenbergDecomp&);
  unsigned            cols()                  const {return T_.cols();}
  unsigned            rows()                  const {return T_.rows();}
  RWBoolean           good()                  const;// did this decomposition get computed properly
  <T>GenMat           B()                     const;
  <T>GenMat           Z()                     const;
  <T>GenMat           T()                     const;
  <Z>                 eigenvalue(int)         const;
  <Z>Vec              eigenvalues()           const;
  <T>Vec              Bx(const <T>Vec&)       const;
  <T>Vec              BInvTx(const <T>Vec&)   const;
  <T>Vec              Zx(const <T>Vec&)       const;
  <T>Vec              ZTx(const <T>Vec&)      const;
  <T>GenMat           BX(const <T>GenMat&)    const;
  <T>GenMat           BInvTX(const <T>GenMat&)const;
  <T>GenMat           ZX(const <T>GenMat&)    const;
  <T>GenMat           ZTX(const <T>GenMat&)   const;
  int                 move(int,int);              // Move an eigenvalue, returns where it actually ended up
  RWBoolean           moveToFront(const IntVec&); // Move the listed eigenvalues to the front (returns TRUE if all went well)
};

> endwrapper

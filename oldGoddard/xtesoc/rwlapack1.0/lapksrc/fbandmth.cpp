#if 0
/*
 * Generated from template $Id: xmth.cpp,v 1.2 1993/10/11 21:48:17 alv Exp $
 *
 * Math member functions for the matrix types
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */
#endif

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/fbandmat.h"

/*
 * This function returns a new matrix the same shape as the one 
 * passed in but with the data passed in.  This way we restrict
 * all the >if statements to a couple lines
 */
inline FloatBandMat sameShapeMatrix( FloatVec& vec, const FloatBandMat& A )
{
  return FloatBandMat(vec,A.rows(),A.cols(),A.lowerBandwidth(),A.upperBandwidth());
}


static void verifyMatch(const FloatBandMat& A, const FloatBandMat& B)
{
    if (A.rows()!=B.rows() || A.cols()!=B.cols())
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATSIZE,A.rows(),A.cols(),B.rows(),B.cols())));
    if (A.bandwidth()!=B.bandwidth() || A.upperBandwidth()!=B.upperBandwidth())
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_BANDSIZE,A.bandwidth(),B.bandwidth(),
			A.upperBandwidth(), B.upperBandwidth())));
}

RWBoolean FloatBandMat::operator==(const FloatBandMat& X)
{
    if (n!=X.n || band!=X.band || bandu!=X.bandu) return FALSE;
    int b = bandwidth();
    int lb = lowerBandwidth();
    int ub = upperBandwidth();
    /*
     * This is a little tricky cause some of the data vector is unused.
     * First check the middle part of the vector.  Next check the bits
     * at the front that are part of the matrix, and finally check the 
     * bits at the end
     */
    int midstart = (ub==0) ? 0 : (ub-1)*b+1;
    int fixup = (lb==0) ? ((ub==0)?0:1) : ((ub==0)?1:2);
    int midlen = (int(n)-ub-lb+fixup)*b-fixup;
    if (midlen>0)
	if(vec.slice(midstart,midlen)!=X.vec.slice(midstart,midlen)) return FALSE;
    int el,len;
    for( el=ub,len=b-ub ; el<n*b && len<(b-1); el+=b-1,len++ ) {
      if (vec.slice(el,len)!=X.vec.slice(el,len)) return FALSE;
    }
    for( el=b*(n-lb+1),len=b-2 ; el<n*b; el+=b,len-- ) {
      if (vec.slice(el,len)!=X.vec.slice(el,len)) return FALSE;
    }
    return TRUE;
}


FloatBandMat& FloatBandMat::operator+=(const FloatBandMat& m)
{
  verifyMatch(*this,m);
  vec += m.vec;
  return *this;
}

FloatBandMat& FloatBandMat::operator-=(const FloatBandMat& m)
{
  verifyMatch(*this,m);
  vec -= m.vec;
  return *this;
}

FloatBandMat& FloatBandMat::operator*=(const FloatBandMat& m)
{
  verifyMatch(*this,m);
  vec *= m.vec;
  return *this;
}

FloatBandMat& FloatBandMat::operator*=(Float x)
{
  vec *= x;
  return *this;
}

FloatBandMat& FloatBandMat::operator/=(const FloatBandMat& m)
{
  verifyMatch(*this,m);
  vec /= m.vec;
  return *this;
}

FloatBandMat& FloatBandMat::operator/=(Float x)
{
  vec /= x;
  return *this;
}




FloatBandMat operator-(const FloatBandMat& A)
{
  FloatVec temp(-(A.dataVec()));
  return sameShapeMatrix(temp,A);
}
  
FloatBandMat operator+(const FloatBandMat& A)
{
  return A;
}

FloatBandMat operator*(const FloatBandMat& A, const FloatBandMat& B) {
	verifyMatch(A,B);
	FloatVec temp(A.dataVec()*B.dataVec());
	return sameShapeMatrix(temp,A);
}

FloatBandMat operator/(const FloatBandMat& A, const FloatBandMat& B) {
	verifyMatch(A,B);
	FloatVec temp(A.dataVec()/B.dataVec());
	return sameShapeMatrix(temp,A);
}

FloatBandMat operator+(const FloatBandMat& A, const FloatBandMat& B) {
	verifyMatch(A,B);
	FloatVec temp(A.dataVec()+B.dataVec());
	return sameShapeMatrix(temp,A);
}

FloatBandMat operator-(const FloatBandMat& A, const FloatBandMat& B) {
	verifyMatch(A,B);
	FloatVec temp(A.dataVec()-B.dataVec());
	return sameShapeMatrix(temp,A);
}

FloatBandMat operator*(const FloatBandMat& A, Float x) {
	FloatVec temp(A.dataVec()*x);
	return sameShapeMatrix(temp,A);
}

/*
 * Out-of-line version for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
  FloatBandMat operator*(Float x, const FloatBandMat& A) { return A*x; }
#endif

FloatBandMat operator/(const FloatBandMat& A, Float x) {
	FloatVec temp(A.dataVec()/x);
	return sameShapeMatrix(temp,A);
}





FloatBandMat abs(const FloatBandMat& A) {
	FloatVec temp(abs(A.dataVec()));
	return sameShapeMatrix(temp,A);
}



FloatBandMat transpose(const FloatBandMat& A)
{
  FloatBandMat T(A.rows(),A.cols(),A.upperBandwidth(),A.lowerBandwidth());
  int l = A.lowerBandwidth();	// Getting these as ints avoids
  int u = A.upperBandwidth();	// conversion problems
  for( int i=(-l); i<=u; i++ ) {
    T.diagonal(-i) = A.diagonal(i);
  }
  return T;
}

FloatVec product(const FloatBandMat& A, const FloatVec& x)
{
    if (A.cols()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATVECPROD,A.className,A.rows(),A.cols(),x.length())));
    }
    FloatVec y(A.rows(),rwUninitialized);
    y = (Float)(0);
    int l = A.lowerBandwidth();	// Getting these as ints avoids
    int u = A.upperBandwidth();	// conversion problems
    for( int i=(-l); i<=u; i++ ) {
      int iabs = (i>=0)?i:(-i);
      FloatVec yslice = y.slice( (i<0)?iabs:0, x.length()-iabs );
      FloatVec xslice = x.slice( (i>0)?iabs:0, x.length()-iabs );
      /* The * in the next line is element by element multiplication */
      yslice += A.diagonal(i) * xslice;
    }
    return y;
}

FloatVec product(const FloatVec& x, const FloatBandMat& A)
{
    if (A.rows()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECMATPROD,x.length(),A.className,A.rows(),A.cols())));
    }
    FloatVec y(A.cols(),(Float)(0));
    int l = A.lowerBandwidth();	// Getting these as ints avoids
    int u = A.upperBandwidth();	// conversion problems
    for( int i=(-l); i<=u; i++ ) {
      int iabs = (i>=0)?i:(-i);
      FloatVec yslice = y.slice( (i>0)?iabs:0, x.length()-iabs );
      FloatVec xslice = x.slice( (i<0)?iabs:0, x.length()-iabs );
      /* The * in the next line is element by element multiplication */
      yslice += A.diagonal(i) * xslice;
    }
    return y;
}


/*
 * The product of two banded matrices is a banded matrix.  Here's the details:
 *
 * Say we have want C=AB, then Cij = sum z=0,...,n ( AizBzj )
 * Say A has lower bandwidth al and upper au, B's bandwidths are bl and bu
 * so that Aij=0 unless -al<=j-i<=au and similiar for B.
 *
 * Back to Cij = sum z=0,...,n AizBzj.  Define j=i+k, now we have
 *         Cij = sum z=0,...,n Aiz Bz,i+k
 *
 * Now replace z by i+w:
 *         Ci,i+k = sum w=-i,...,n-i Ai,i+w Bi+w,i+k
 *
 * Bi+w,i+k is zero unless -bl<=k-w<=bu ==> k-bu<=w<=k+bl
 * Ai,i+w is zero unless                     -al<=w<=au
 * so we can reduce the limits on the summation:
 *
 *    Ci,i+k = sum w=max(-i,k-bu,-al),...,min(n-i,k+bl,au) Ai,i+w Bi+w,i+k
 *
 * That's the formula we'll implement.  Now consider the bandwidth of C.
 * For i in the middle of the matrix (-i is small and n-i is big) then
 * the sums are reduced to one term in two cases:
 *   k-bu=au  ==>  k=au+bu
 *   k+bl=-al ==>  k=-al-bl
 * so the lower bandwidth of C is al+bl and the upper au+bu.
 *
 * Rather than code this up with slices and dot() functions, I simply
 * wrote the loops directly.  Since (hopefully) the bandwidth is 
 * generally not that large, avoiding the overhead of function calls 
 * may be worthwhile.  (plus it is easier this way)
 */
FloatBandMat product(const FloatBandMat& A, const FloatBandMat& B)
{
    int n = A.cols();
    if (B.rows()!=n) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATMATPROD,A.className,n,n,B.rows(),B.cols())));
    }
    int au = A.upperBandwidth();
    int al = A.lowerBandwidth();
    int bu = B.upperBandwidth();
    int bl = B.lowerBandwidth();
    FloatBandMat AB(n,n,al+bl,au+bu);
    for( int i=0; i<n; i++ ) {
      int mink = (i>al+bl) ? (-al-bl) : (-i);
      int maxk = (i<n-au-bu) ? (au+bu) : (n-i-1);
      for( int k=mink; k<=maxk; k++ ) {
        int minw = (-i >= k-bu) ? (-i) : (k-bu);
	if (-al > minw) minw = -al;
        int maxw = (au <= k+bl) ? au : (k+bl);
        if (n-i-1 < maxw) maxw = n-i-1;
	Float entry = (Float)(0);
        for( int w=minw; w<=maxw; w++ ) {
          entry += A.val(i,i+w) * B.val(i+w,i+k);
        }
	AB.set(i,i+k,entry);
      }
    }
    return AB;
}

FloatBandMat transposeProduct(const FloatBandMat& A, const FloatBandMat& B)
{
    return product(transpose(A),B);
}



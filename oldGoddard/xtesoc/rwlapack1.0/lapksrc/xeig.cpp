> # $Header: /users/rcs/lapksrc/xeig.cpp,v 1.3 1993/10/11 21:48:08 alv Exp $
>  
> # $Log: xeig.cpp,v $
> # Revision 1.3  1993/10/11  21:48:08  alv
> # synch'ed with Math.h++ v5.1
> #
> # Revision 1.2  1993/07/05  19:19:33  alv
> # fixed up for complex numbers
> #
> # Revision 1.1  1993/05/18  17:00:02  alv
> # Initial revision
> #
>  
> define <T>=RW_T_RW
> if <T>==Double
> define <P>=Double <tLetter>=d <Z>=DComplex <zLetter>=c
> elif <T>==Float
> define <P>=Float  <tLetter>=f <Z>=FComplex <zLetter>=b
> elif <T>==DComplex
> define <P>=Double <tLetter>=c <Z>=DComplex <zLetter>=c
> else
> err Eigen-decomposition: not defined for '<T>'
> endif
>  
>  
/*
 * Implementation of <T>EigDecomp
 *
 * Generated from template $Id: xeig.cpp,v 1.3 1993/10/11 21:48:08 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 */ 

#include "rw/<tLetter>eig.h"
#include "rw/<tLetter>eigsrv.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"

typedef <T>SchurEigServer DefaultServer;

<T>EigDecomp::<T>EigDecomp()
{
  n=0;
  computedAll = accurate = TRUE;
}

<T>EigDecomp::<T>EigDecomp(const <P>EigDecomp& x)
: n(x.n),
  lambda(x.lambda),
  P(x.P),
  Q(x.Q),
  computedAll(x.computedAll),
  accurate(x.accurate)
{
}

<T>EigDecomp::<T>EigDecomp(const <T>GenMat& A, RWBoolean computeVecs)
{
  factor(A,computeVecs);
}

void <T>EigDecomp::factor(const <T>GenMat& A, RWBoolean computeVecs)
{
  DefaultServer server;
  server.computeLeftEigenVectors(computeVecs);
  server.computeRightEigenVectors(computeVecs);
  *this = server(A);
}
  
void <T>EigDecomp::operator=(const <T>EigDecomp& A)
{
  n = A.n;
  computedAll = A.computedAll;
  accurate = A.accurate;
  lambda.reference(A.lambda);
  P.reference(A.P);
  Q.reference(A.Q);
}

<Z>       <T>EigDecomp::eigenValue(int i)  const
{
  if (i<0 || i>=lambda.length()) RWTHROW(RWBoundsErr(RWMessage(RWLAPK_NOEIG,i)));
  return lambda(i);
}

const <Z>Vec    <T>EigDecomp::leftEigenVector(int i) const
{
  if (i<0 || i>=P.cols()) RWTHROW(RWBoundsErr(RWMessage(RWLAPK_NOEIG,i)));
  RWPOSTCONDITION(lambda.length()>i); // if this isn't true something went wrong
> if <T>!=<P>
  return P.col(i);
> else
  <Z> eval = lambda(i);
  if (imag(eval)==0) {
    return P.col(i);       // A real eigenvalue
  } else {                 // A complex eigenvalue
    if (i>0 && lambda(i-1)==conj(eval)) {
      return <Z>Vec(P.col(i-1),-P.col(i));
    } else {
      RWPOSTCONDITION(i<lambda.length() && lambda(i+1)==conj(eval));  // if this fails the evals aren't a pair
      return <Z>Vec(P.col(i),P.col(i+1));
    }
  }  
> endif
}

const <Z>GenMat <T>EigDecomp::leftEigenVectors() const
{
> if <T>!=<P>
  return P;
> else
  <Z>GenMat vecs(P);
  for(int i=vecs.cols(); i--;) {  // fix up complex conjugate pairs of vals
    if (imag(lambda(i))!=0) {
      real(vecs.col(i)) = P.col(i-1);
      imag(vecs.col(i)) = -P.col(i);
      i--;
      imag(vecs.col(i)) = P.col(i+1);
    }
  }
  return vecs;
> endif
}

const <Z>Vec    <T>EigDecomp::rightEigenVector(int i) const
{
  if (i<0 || i>=Q.cols()) RWTHROW(RWBoundsErr(RWMessage(RWLAPK_NOEIG,i)));
  RWPOSTCONDITION(lambda.length()>i); // if this isn't true something went wrong
> if <T>!=<P>
  return Q.col(i);
> else
  <Z> eval = lambda(i);
  if (imag(eval)==0) {
    return Q.col(i);       // A real eigenvalue
  } else {                 // A complex eigenvalue
    if (i>0 && lambda(i-1)==conj(eval)) {
      return <Z>Vec(Q.col(i-1),-Q.col(i));
    } else {
      RWPOSTCONDITION(i<lambda.length() && lambda(i+1)==conj(eval));  // if this fails the evals aren't a pair
      return <Z>Vec(Q.col(i),Q.col(i+1));
    }
  }  
> endif
}

const <Z>GenMat <T>EigDecomp::rightEigenVectors() const
{
> if <T>!=<P>
  return Q;
> else
  <Z>GenMat vecs(Q);
  for(int i=vecs.cols(); i--;) {  // fix up complex conjugate pairs of vals
    if (imag(lambda(i))!=0) {
      real(vecs.col(i)) = Q.col(i-1);
      imag(vecs.col(i)) = -Q.col(i);
      i--;
      imag(vecs.col(i)) = Q.col(i+1);
    }
  }
  return vecs;
> endif
}

RWBoolean <T>EigDecomp::good()          const
{
  return computedAll && accurate;
}

RWBoolean <T>EigDecomp::inaccurate()    const
{
  return computedAll && !accurate;
}

RWBoolean <T>EigDecomp::fail()          const
{
  return !computedAll;
}

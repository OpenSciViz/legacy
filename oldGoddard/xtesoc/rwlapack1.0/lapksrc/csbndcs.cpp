/*
 * Functions for shape conversion for DComplexSymBandMat, except
 * conversion to/from square matrices.
 *
 * Generated from template $Id: xcs.cpp,v 1.2 1993/10/11 21:48:07 alv Exp $
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/lapkerr.h"
#include "rw/rwerr.h"
#include "rw/csbndmat.h"
#include "rw/cbandmat.h"

DComplexBandMat::DComplexBandMat( const DComplexSymBandMat& A )
  : vec(A.bandwidth()*A.rows(),rwUninitialized)
{
  n=A.rows();
  band =A.bandwidth();
  bandu=A.upperBandwidth();
  for(int i=0; i<=bandu; i++) {
    DComplexVec d = A.diagonal(i);
    diagonal(i) = d;
    if (i>0) diagonal(-i) = d;
  }
}

DComplexSymBandMat toSymBandMat( const DComplexBandMat& S )
{
  int n = S.rows();
  unsigned l=S.lowerBandwidth();
  unsigned u=S.upperBandwidth();
  if (l!=u) RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTSYM)));
  DComplexSymBandMat A( n, n, u );
  if (n>=1) A.diagonal(0) = S.diagonal(0);
  for( int i=(u<n)?u:n; i>=1; i-- ) {
    DComplexVec Adiag(A.diagonal(i));
    Adiag = S.diagonal(i);		// Avoid generating temporaries
    Adiag += S.diagonal(-i);
    Adiag /= DComplex(2,0);		// Explicit constructor necessary for Zortech
  }
  return A;
}


#if 0
/*
 * Generated from template $Id: xmth.cpp,v 1.2 1993/10/11 21:48:17 alv Exp $
 *
 * Math member functions for the matrix types
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */
#endif

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/cutrimat.h"
#include "rw/dutrimat.h"
#include "rw/cltrimat.h"

/*
 * This function returns a new matrix the same shape as the one 
 * passed in but with the data passed in.  This way we restrict
 * all the >if statements to a couple lines
 */
inline DComplexUpperTriMat sameShapeMatrix( DComplexVec& vec, const DComplexUpperTriMat& A )
{
  return DComplexUpperTriMat(vec,A.rows(),A.cols());
}

inline DoubleUpperTriMat sameShapeRealMatrix( DoubleVec& vec, const DComplexUpperTriMat& A )
{
  return DoubleUpperTriMat(vec,A.rows(),A.cols());
}

static void verifyMatch(const DComplexUpperTriMat& A, const DComplexUpperTriMat& B)
{
    if (A.rows()!=B.rows() || A.cols()!=B.cols())
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATSIZE,A.rows(),A.cols(),B.rows(),B.cols())));
}

RWBoolean DComplexUpperTriMat::operator==(const DComplexUpperTriMat& X)
{
    if (n!=X.n) return FALSE;
    return vec==X.vec;	// All elements in vec used in the matrix
}


DComplexUpperTriMat& DComplexUpperTriMat::operator+=(const DComplexUpperTriMat& m)
{
  verifyMatch(*this,m);
  vec += m.vec;
  return *this;
}

DComplexUpperTriMat& DComplexUpperTriMat::operator-=(const DComplexUpperTriMat& m)
{
  verifyMatch(*this,m);
  vec -= m.vec;
  return *this;
}

DComplexUpperTriMat& DComplexUpperTriMat::operator*=(const DComplexUpperTriMat& m)
{
  verifyMatch(*this,m);
  vec *= m.vec;
  return *this;
}

DComplexUpperTriMat& DComplexUpperTriMat::operator*=(DComplex x)
{
  vec *= x;
  return *this;
}

DComplexUpperTriMat& DComplexUpperTriMat::operator/=(const DComplexUpperTriMat& m)
{
  verifyMatch(*this,m);
  vec /= m.vec;
  return *this;
}

DComplexUpperTriMat& DComplexUpperTriMat::operator/=(DComplex x)
{
  vec /= x;
  return *this;
}




DComplexUpperTriMat operator-(const DComplexUpperTriMat& A)
{
  DComplexVec temp(-(A.dataVec()));
  return sameShapeMatrix(temp,A);
}
  
DComplexUpperTriMat operator+(const DComplexUpperTriMat& A)
{
  return A;
}

DComplexUpperTriMat operator*(const DComplexUpperTriMat& A, const DComplexUpperTriMat& B) {
	verifyMatch(A,B);
	DComplexVec temp(A.dataVec()*B.dataVec());
	return sameShapeMatrix(temp,A);
}

DComplexUpperTriMat operator/(const DComplexUpperTriMat& A, const DComplexUpperTriMat& B) {
	verifyMatch(A,B);
	DComplexVec temp(A.dataVec()/B.dataVec());
	return sameShapeMatrix(temp,A);
}

DComplexUpperTriMat operator+(const DComplexUpperTriMat& A, const DComplexUpperTriMat& B) {
	verifyMatch(A,B);
	DComplexVec temp(A.dataVec()+B.dataVec());
	return sameShapeMatrix(temp,A);
}

DComplexUpperTriMat operator-(const DComplexUpperTriMat& A, const DComplexUpperTriMat& B) {
	verifyMatch(A,B);
	DComplexVec temp(A.dataVec()-B.dataVec());
	return sameShapeMatrix(temp,A);
}

DComplexUpperTriMat operator*(const DComplexUpperTriMat& A, DComplex x) {
	DComplexVec temp(A.dataVec()*x);
	return sameShapeMatrix(temp,A);
}

/*
 * Out-of-line version for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
  DComplexUpperTriMat operator*(DComplex x, const DComplexUpperTriMat& A) { return A*x; }
#endif

DComplexUpperTriMat operator/(const DComplexUpperTriMat& A, DComplex x) {
	DComplexVec temp(A.dataVec()/x);
	return sameShapeMatrix(temp,A);
}





DoubleUpperTriMat abs(const DComplexUpperTriMat& A) {
	DoubleVec temp(abs(A.dataVec()));
	return sameShapeRealMatrix(temp,A);
}


DComplexUpperTriMat conj(const DComplexUpperTriMat& A) {
	DComplexVec temp(conj(A.dataVec()));
	return sameShapeMatrix(temp,A);
}

DoubleUpperTriMat real(const DComplexUpperTriMat& A) {
	DoubleVec temp(real(A.dataVec()));
	return sameShapeRealMatrix(temp,A);
}

DoubleUpperTriMat imag(const DComplexUpperTriMat& A) {
	DoubleVec temp(imag(A.dataVec()));
	return sameShapeRealMatrix(temp,A);
}

DoubleUpperTriMat norm(const DComplexUpperTriMat& A) {
	DoubleVec temp(norm(A.dataVec()));
	return sameShapeRealMatrix(temp,A);
}

DoubleUpperTriMat arg(const DComplexUpperTriMat& A) {
	DoubleVec temp(arg(A.dataVec()));
	return sameShapeRealMatrix(temp,A);
}



DComplexLowerTriMat transpose(const DComplexUpperTriMat& A)
{
return DComplexLowerTriMat(A.dataVec(),A.rows(),A.cols());
}

DComplexVec product(const DComplexUpperTriMat& A, const DComplexVec& x)
{
    if (A.cols()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATVECPROD,A.className,A.rows(),A.cols(),x.length())));
    }
    DComplexVec y(A.rows(),rwUninitialized);
    const DComplexVec& v = A.dataVec();
    int n=A.rows();
    for( int i=0; i<n; i++ ) {
      DComplex* index = (DComplex*)v.data() + ((i+1)*(i+2)/2-1);
      y(i) = (*index)*x(i);
      index += i+1;
      for( int k=i+1; k<n; index+=(++k) ) {
      y(i) += (*index)*x(k);
      }
    }
    return y;
}

DComplexVec product(const DComplexVec& x, const DComplexUpperTriMat& A)
{
    if (A.rows()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECMATPROD,x.length(),A.className,A.rows(),A.cols())));
    }
    return product(transpose(A),x);
}



> ###############################################################
> # Inline functions giving the size of each matrix shape       #
> #                                                             #
> # $Header: /users/rcs/lapksrc/sizes,v 1.1 1993/06/23 18:28:01 alv Exp $
> #                                                             #
> # A set of inline functions named <M>Size that tells how many #
> # elements need to be explicitly stored for a matrix of the   #
> # given shape with parameters given by the arguments.         #
> #                                                             #
> # This is included by the ctm files in some constructors, for #
> # example.                                                    #
> #                                                             #
> # Copyright (1991) by Rogue Wave Software, Inc.  This software#
> # is subject to copyright protection under the laws of the    #
> # United States and other countries.                          #
> #                                                             #
> # Written by Al Vermeulen.                                    #
> #                                                             #
> # Limited License.                                            #
> ###############################################################
> #
> # $Log: sizes,v $
> # Revision 1.1  1993/06/23  18:28:01  alv
> # Initial revision
> #
> # Revision 1.2  1993/01/26  21:06:01  alv
> # removed DOS EOF
> #
> # Revision 1.1  1993/01/23  00:01:14  alv
> # Initial revision
> #
> # 
> #    Rev 1.1   17 Oct 1991 09:22:44   keffer
> # Changed include path to <rw/xxx.h>
> # 
>
>
/*
 * These inline functions
 * give the number of elements that need be stored to represent
 * the different types of matrices
 */

inline unsigned GenMatSize(unsigned n, unsigned m)  { return m*n; }
inline unsigned BandMatSize(unsigned n, unsigned b) { return n*b; }
inline unsigned SymBandMatSize(unsigned n, unsigned hb) { return n*(hb+1); }
inline unsigned HermBandMatSize(unsigned n, unsigned hb) { return n*(hb+1); }
inline unsigned SymMatSize(unsigned n)		    { return n*(n+1)/2; }
inline unsigned SkewMatSize(unsigned n)		    { return n*(n+1)/2; }
inline unsigned HermMatSize(unsigned n)		    { return n*(n+1)/2; }
inline unsigned UpperTriMatSize(unsigned n)  	    { return n*(n+1)/2; }
inline unsigned LowerTriMatSize(unsigned n)  	    { return n*(n+1)/2; }
inline unsigned TriDiagMatSize(unsigned n)	    { return 3*n; }

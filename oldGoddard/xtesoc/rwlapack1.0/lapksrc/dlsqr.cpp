/*
 * defs for DoubleLeastSqQR
 *
 * Generated from template $Id: xlsqr.cpp,v 1.4 1993/10/11 21:48:14 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 */

#include "rw/dlsqr.h"
#include "rw/lapkerr.h"
#include "rw/rwerr.h"

DoubleLeastSqQR::DoubleLeastSqQR()                                 : DoubleCODecomp() {}
DoubleLeastSqQR::DoubleLeastSqQR(const DoubleGenMat& A, double tol)   : DoubleCODecomp(A,tol) {}
DoubleLeastSqQR::DoubleLeastSqQR(const DoubleQRDecomp& A, double tol) : DoubleCODecomp(A,tol) {}
DoubleLeastSqQR::DoubleLeastSqQR(const DoubleCODecomp& A)             : DoubleCODecomp(A) {}

DoubleVec DoubleLeastSqQR::residual(const DoubleVec& data) const
{
  if (data.length()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,data.length(),rows())));
  DoubleVec d = QTx(data);
  d(RWSlice(0,rank())) = (Double)0;
  return Qx(d);
}

Double DoubleLeastSqQR::residualNorm(const DoubleVec& data) const
{
  if (data.length()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,data.length(),rows())));
  DoubleVec d = QTx(data);
  DoubleVec d2 = d(RWToEnd(rank()));
  return l2Norm(d2);
}

DoubleVec DoubleLeastSqQR::solve(const DoubleVec& data) const
{
  if (data.length()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,data.length(),rows())));
  DoubleVec d = QTx(data);
  DoubleVec d1 = d(RWSlice(0,rank()));
  DoubleVec c(cols(),rwUninitialized);
  c(RWSlice(0,rank())) = Tinvx(d1);
  c(RWToEnd(rank())) = (Double)0;
  return Px(Zx(c));
} 

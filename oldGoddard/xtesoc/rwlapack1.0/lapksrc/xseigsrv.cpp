> # $Header: /users/rcs/lapksrc/xseigsrv.cpp,v 1.2 1993/07/05 19:19:33 alv Exp $
> #                                                               #
> # $Log: xseigsrv.cpp,v $
> # Revision 1.2  1993/07/05  19:19:33  alv
> # fixed up for complex numbers
> #
> # Revision 1.1  1993/04/06  15:44:43  alv
> # Initial revision
> #
>
> define <T>=RW_T_RW
> 
> if <T>==Double
> define <P>=Double <tLetter>=d <Sym>=Sym <sym>=sym <sLetter>=s
> elif <T>==Float
> define <P>=Float <tLetter>=f <Sym>=Sym <sym>=sym <sLetter>=s
> elif <T>==DComplex
> define <P>=Double <tLetter>=c <Sym>=Herm <sym>=herm <sLetter>=h <pLetter>=d
> else
> err Symmetric Eigen-decomposition: not defined for '<T>'
> endif
>
/*
 * Implementation of <T><Sym>EigServer
 *
 * Generated from template $Id: xseigsrv.cpp,v 1.2 1993/07/05 19:19:33 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */ 

#include "rw/<tLetter><sLetter>eigsrv.h"
#include "rw/<tLetter>td.h"
> if <P>!=<T>
#include "rw/<pLetter>seigsrv.h"
> endif

<T><Sym>EigDecomp <T><Sym>EigServer::operator()(const <T><Sym>Mat& A)
{
  <T><Sym>DenseTriDiagDecomp td(A);         // Build a tri-diagonal decomposition
  <P>SymEigDecomp tdeig = decompose(td);  // Get eigenvals/vecs for tri-diagonal problem
  <T><Sym>EigDecomp eig = tdeig;            // Needs to be done this way for complex numbers
  P(eig) = td.transform(P(tdeig));
  return eig;
}

<T><Sym>EigDecomp <T><Sym>EigServer::operator()(const <T><Sym>BandMat& A)
{
  <T><Sym>BandTriDiagDecomp td(A);          // Build a tri-diagonal decomposition
  <P>SymEigDecomp tdeig = decompose(td);  // Get eigenvals/vecs for tri-diagonal problem
  <T><Sym>EigDecomp eig = tdeig;            // Needs to be done this way for complex numbers
  P(eig) = td.transform(P(tdeig));
  return eig;
}

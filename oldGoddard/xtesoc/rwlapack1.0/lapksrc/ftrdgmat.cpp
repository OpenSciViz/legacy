#if 0
/*
 * Generated from template file $Id: xmat.cpp,v 1.4 1993/10/11 21:48:16 alv Exp $
 *
 * The main code file for FloatTriDiagMat
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */
#endif

#include "rw/ftrdgmat.h"

#include "rw/rstream.h"
#include "rw/rwfile.h"
#include "rw/vstream.h"
#include "rw/lapkerr.h"
#include "rw/rwerr.h"
#  ifdef __ZTC__
#    include <iomanip.hpp>
#  else
#    ifdef __GLOCK__
#      include <iomanip.hxx>
#    else
#      include <iomanip.h>
#    endif
#  endif
STARTWRAP
#include <stdio.h>
#include <ctype.h>
ENDWRAP

const char* FloatTriDiagMat::className = "FloatTriDiagMat";

/*
 * These inline functions
 * give the number of elements that need be stored to represent
 * the different types of matrices
 */

inline unsigned GenMatSize(unsigned n, unsigned m)  { return m*n; }
inline unsigned BandMatSize(unsigned n, unsigned b) { return n*b; }
inline unsigned SymBandMatSize(unsigned n, unsigned hb) { return n*(hb+1); }
inline unsigned HermBandMatSize(unsigned n, unsigned hb) { return n*(hb+1); }
inline unsigned SymMatSize(unsigned n)		    { return n*(n+1)/2; }
inline unsigned SkewMatSize(unsigned n)		    { return n*(n+1)/2; }
inline unsigned HermMatSize(unsigned n)		    { return n*(n+1)/2; }
inline unsigned UpperTriMatSize(unsigned n)  	    { return n*(n+1)/2; }
inline unsigned LowerTriMatSize(unsigned n)  	    { return n*(n+1)/2; }
inline unsigned TriDiagMatSize(unsigned n)	    { return 3*n; }

FloatTriDiagMat::~FloatTriDiagMat()
{
}

FloatTriDiagMat::FloatTriDiagMat()
  : vec()
{
    n=0;
}

FloatTriDiagMat::FloatTriDiagMat( const FloatTriDiagMat& A )
  : vec(A.vec)
{
    n=A.n;
}

FloatTriDiagMat::FloatTriDiagMat(unsigned M, unsigned N)
  : vec( TriDiagMatSize(N),rwUninitialized )
{
  if (M!=N) { RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTSQUARE,int(M),int(N)))); }
  n=N;
  zeroUnusedEntries();
}


FloatTriDiagMat::FloatTriDiagMat(const FloatVec& data, unsigned M, unsigned N)
  : vec( data )
{
  if (M!=N) { RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTSQUARE,int(M),int(N)))); }
  n=N;
  if (vec.length()!=TriDiagMatSize(N))
    RWTHROW(RWInternalErr(RWMessage(RWLAPK_NUMBERPOINTS,vec.length(),n,n,className)));
}

/*
 * Set entries in the data vector which are not used to zero.  This
 * avoids purify warnings and also ensures we won't have troubles
 * adding and subtracting these matrices
 */
void FloatTriDiagMat::zeroUnusedEntries()
{
    if (n>0) { vec(3*n-1) = (Float)(0); vec(0) = (Float)(0); }
}

Float FloatTriDiagMat::bcval(int i, int j) const
{
    if ( i<0 || i>=rows() || j<0 || j>=cols() ) {
      RWTHROW(RWBoundsErr(RWMessage(RWLAPK_OUTOFBOUNDS,i,j,rows(),cols())));
    }
    return ( (i-j>1) || (i-j<(-1)) ) ? (Float)(0) : vec(i+1+j*2);
}

Float FloatTriDiagMat::bcset(int i, int j, Float x)
{
    if ( i<0 || i>=rows() || j<0 || j>=cols() ) {
      RWTHROW(RWBoundsErr(RWMessage(RWLAPK_OUTOFBOUNDS,i,j,rows(),cols())));
    }
    if (i-j<(-1) || i-j>1) RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTSETABLE,i,j,className)));
    return vec(i+1+j*2)=x;
}

ROFloatRef FloatTriDiagMat::bcref(int i, int j)
{
    if ( i<0 || i>=rows() || j<0 || j>=cols() ) {
      RWTHROW(RWBoundsErr(RWMessage(RWLAPK_OUTOFBOUNDS,i,j,rows(),cols())));
    }
    return ( (i-j>1) || (i-j<(-1)) ) ? ROFloatRef(rwFloatZero,TRUE)
				     : ROFloatRef(vec(i+1+j*2),FALSE);
}

FloatVec FloatTriDiagMat::bcdiagonal(int i) const
{
  if ( i>=int(cols()) || (-i)>=int(rows()) ||
		i>int(upperBandwidth()) || (-i)>int(lowerBandwidth())) {
    RWTHROW(RWBoundsErr(RWMessage(RWLAPK_DIAGOUTOFBOUNDS,-i,className,rows(),cols())));
  }
    int iabs = (i>=0) ? i : -i;
    return vec.slice(i>0 ? upperBandwidth()+i*(bandwidth()-1)
			 : upperBandwidth()+iabs
			 , n-iabs, bandwidth() );
}

/*
 * Here are non-inline versions of the row(), col(), and diagonal()
 * routines.  Normally these would be inlined, but due to a bug
 * in the Borland C++ compiler it is best not to inline them with
 * Borland C++.
 *
 * These routines just call the bounds checking routines.
 */

/*
 * The leadingSubmatrix function
 */

FloatTriDiagMat FloatTriDiagMat::leadingSubmatrix(int k)
{
  if (k<0 || k>n) RWTHROW(RWBoundsErr(RWMessage(RWLAPK_LEADINGSUBMATRIXORDER,k,n)));
  return FloatTriDiagMat( vec.slice(0,TriDiagMatSize(k)), k, k );
}

FloatTriDiagMat& FloatTriDiagMat::operator=(const FloatTriDiagMat& M)
{
    if (rows()!=M.rows() || cols()!=M.cols()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATSIZE,M.rows(),M.cols(),rows(),cols())));
    }
    vec = M.vec;
    return *this;
}

FloatTriDiagMat& FloatTriDiagMat::reference(FloatTriDiagMat& A)
{
    vec.reference(A.vec);
    n=A.n;
    return *this;
}

FloatTriDiagMat FloatTriDiagMat::copy() const
{
  FloatTriDiagMat A( *((FloatTriDiagMat*)this) );	// cast this to non-const to make cfront happy
  A.deepenShallowCopy();
  return A;
}

void FloatTriDiagMat::resize(unsigned M, unsigned N)
{
    if (M!=N) { RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTSQUARE,int(M),int(N)))); }
    // Zero out elements past the bottom of the matrix if need be so
    // they don't enter in to the new matrix
    zeroUnusedEntries();
    n=N;
    vec.resize(TriDiagMatSize(n));

  // Finally, zero any unused entries just to keep things tidy
  zeroUnusedEntries();
}



/*
 * printOn,scanFrom:  printOn prints all non-numeric stuff first
 * then prints the numbers defining the shape and size of the matrix,
 * then prints the matrix itself.  The format used by printOn can be
 * used to read in a matrix with scanFrom.
 *
 * scanFrom first eats all the non-numeric characters it encounters.
 * This takes care of the stuff printOn spews before the data.  Next
 * it reads the numbers which define the matrix shape and then the
 * data is read in.
 */

void FloatTriDiagMat::printOn(ostream& outs) const
{
    int w = outs.width(0);
    int m=n;
      outs << className << ", ";
    outs << rows() << "x" << cols() << " [\n";
    for( int i=0; i<m; i++ ) {
      for( int j=0; j<n; j++ ) {
	outs << setw(w) << val(i,j) << " ";
      }
      outs << "\n";
    }
    outs << "]";
}

void FloatTriDiagMat::scanFrom(istream& s)
{
  char c;
  unsigned numRows, numCols;

  /* Skip through leading non-digits */
  do { s.get(c); } while (!s.fail() && !isdigit(c));
  s.putback(c);


  s >> numRows;
  do { s.get(c); } while (isspace(c));	// Skip optional x character
  if (isdigit(c)) s.putback(c);	
  s >> numCols;

  FloatVec v;			// Read the data
  v.scanFrom(s);

  if (!s.fail()) { 		// Now put the data into the matrix
    if (v.length()!=numRows*numCols) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_WRONGNUMPOINTS,v.length(),className,numRows,numCols)));
    } else {
      resize(numRows,numCols);
      int index = 0;
      for(int i=0; i<numRows; i++) {
        for(int j=0; j<numCols; j++) {
	  if (val(i,j)!=v[index]) bcset(i,j,v[index]);
	  index++;
	}
      }
    }
  }
}

void FloatTriDiagMat::saveOn(RWFile& file) const
{
    /* First write the numbers which define the shape of the matrix */
    file.Write(n);
    vec.saveOn(file);
}

void FloatTriDiagMat::saveOn(RWvostream& s) const
{
    /* First write the numbers which define the shape of the matrix */
    s << n;
    vec.saveOn(s);
}

void FloatTriDiagMat::restoreFrom(RWFile& file)
{
    /* First read the numbers which define the shape of the matrix */
    file.Read(n);
    int size = TriDiagMatSize(n);
    vec.restoreFrom(file);
    if (vec.length() != size) RWTHROW(RWInternalErr(RWMessage(RWLAPK_RESTORESIZE,className,vec.length(),size)));
}

void FloatTriDiagMat::restoreFrom(RWvistream& s)
{
    /* First read the numbers which define the shape of the matrix */
    s >> n;
    int size = TriDiagMatSize(n);
    vec.restoreFrom(s);
    if (vec.length() != size) RWTHROW(RWInternalErr(RWMessage(RWLAPK_RESTORESIZE,className,vec.length(),size)));
}

unsigned FloatTriDiagMat::binaryStoreSize() const
{
    /* First determine the size of the stuff
       which determines the matrix shape.    */
    unsigned size = sizeof(unsigned);    // n
    return size + vec.binaryStoreSize();
}

> # $Header: /users/rcs/lapksrc/xsymeig.cpp,v 1.4 1993/10/11 21:48:25 alv Exp $
>  
> # $Log: xsymeig.cpp,v $
> # Revision 1.4  1993/10/11  21:48:25  alv
> # synch'ed with Math.h++ v5.1
> #
> # Revision 1.3  1993/07/05  19:19:33  alv
> # fixed up for complex numbers
> #
> # Revision 1.2  1993/04/06  20:45:04  alv
> # ported to sun C++ v3.0.1
> #
> # Revision 1.1  1993/04/06  15:44:43  alv
> # Initial revision
> #
>  
> define <T>=RW_T_RW
> if <T>==Double
> define <P>=Double <tLetter>=d <Sym>=Sym <sym>=sym <sLetter>=s
> elif <T>==Float
> define <P>=Float <tLetter>=f <Sym>=Sym <sym>=sym <sLetter>=s
> elif <T>==DComplex
> define <P>=Double <tLetter>=c <Sym>=Herm <sym>=herm <sLetter>=h
> else
> err Symmetric Eigen-decomposition: not defined for '<T>'
> endif
>  
>  
/*
 * Implementation of <T><Sym>EigDecomp
 *
 * Generated from template $Id: xsymeig.cpp,v 1.4 1993/10/11 21:48:25 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */ 

#include "rw/<tLetter><sym>eig.h"
#include "rw/<tLetter><sLetter>eigsrv.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"

typedef <T><Sym>QREigServer DefaultServer;

<T><Sym>EigDecomp::<T><Sym>EigDecomp()
{
  n=0;
  computedAll = accurate = TRUE;
}

<T><Sym>EigDecomp::<T><Sym>EigDecomp(const <P>SymEigDecomp& x)
: n(x.n),
  lambda(x.lambda),
  P(x.P),
  computedAll(x.computedAll),
  accurate(x.accurate)
{
}

<T><Sym>EigDecomp::<T><Sym>EigDecomp(const <T><Sym>Mat& A, RWBoolean computeVecs)
{
  factor(A,computeVecs);
}

<T><Sym>EigDecomp::<T><Sym>EigDecomp(const <T><Sym>BandMat& A, RWBoolean computeVecs)
{
  factor(A,computeVecs);
}

void <T><Sym>EigDecomp::factor(const <T><Sym>Mat& A, RWBoolean computeVecs)
{
  DefaultServer server;
  server.computeEigenVectors(computeVecs);
  *this = server(A);
}
  
void <T><Sym>EigDecomp::factor(const <T><Sym>BandMat& A, RWBoolean computeVecs)
{
  DefaultServer server;
  server.computeEigenVectors(computeVecs);
  *this = server(A);
}

void <T><Sym>EigDecomp::operator=(const <T><Sym>EigDecomp& A)
{
  n = A.n;
  computedAll = A.computedAll;
  accurate = A.accurate;
  lambda.reference(A.lambda);
  P.reference(A.P);
}

<P>       <T><Sym>EigDecomp::eigenValue(int i)  const
{
  if (i<0 || i>=lambda.length()) RWTHROW(RWBoundsErr(RWMessage(RWLAPK_NOEIG,i)));
  return lambda(i);
}

const <T>Vec    <T><Sym>EigDecomp::eigenVector(int i) const
{
  if (i<0 || i>=P.cols()) RWTHROW(RWBoundsErr(RWMessage(RWLAPK_NOEIG,i)));
  return P.col(i);
}

RWBoolean <T><Sym>EigDecomp::good()          const
{
  return computedAll && accurate;
}

RWBoolean <T><Sym>EigDecomp::inaccurate()    const
{
  return computedAll && !accurate;
}

RWBoolean <T><Sym>EigDecomp::fail()          const
{
  return !computedAll;
}

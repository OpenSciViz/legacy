#if 0
/*
 * Generated from template file $Id: xmat.cpp,v 1.4 1993/10/11 21:48:16 alv Exp $
 *
 * The main code file for DoubleBandMat
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */
#endif

#include "rw/dbandmat.h"

#include "rw/rstream.h"
#include "rw/rwfile.h"
#include "rw/vstream.h"
#include "rw/lapkerr.h"
#include "rw/rwerr.h"
#  ifdef __ZTC__
#    include <iomanip.hpp>
#  else
#    ifdef __GLOCK__
#      include <iomanip.hxx>
#    else
#      include <iomanip.h>
#    endif
#  endif
STARTWRAP
#include <stdio.h>
#include <ctype.h>
ENDWRAP

const char* DoubleBandMat::className = "DoubleBandMat";

/*
 * These inline functions
 * give the number of elements that need be stored to represent
 * the different types of matrices
 */

inline unsigned GenMatSize(unsigned n, unsigned m)  { return m*n; }
inline unsigned BandMatSize(unsigned n, unsigned b) { return n*b; }
inline unsigned SymBandMatSize(unsigned n, unsigned hb) { return n*(hb+1); }
inline unsigned HermBandMatSize(unsigned n, unsigned hb) { return n*(hb+1); }
inline unsigned SymMatSize(unsigned n)		    { return n*(n+1)/2; }
inline unsigned SkewMatSize(unsigned n)		    { return n*(n+1)/2; }
inline unsigned HermMatSize(unsigned n)		    { return n*(n+1)/2; }
inline unsigned UpperTriMatSize(unsigned n)  	    { return n*(n+1)/2; }
inline unsigned LowerTriMatSize(unsigned n)  	    { return n*(n+1)/2; }
inline unsigned TriDiagMatSize(unsigned n)	    { return 3*n; }

DoubleBandMat::~DoubleBandMat()
{
}

DoubleBandMat::DoubleBandMat()
  : vec()
{
    n=0;
    band=1;
    bandu=0;
}

DoubleBandMat::DoubleBandMat( const DoubleBandMat& A )
  : vec(A.vec)
{
    n=A.n;
    band=A.band;
    bandu=A.bandu;
}

DoubleBandMat::DoubleBandMat(unsigned M, unsigned N, unsigned lowerWidth, unsigned upperWidth)
  : vec(BandMatSize(N,lowerWidth+upperWidth+1),rwUninitialized)
{
  if (M!=N) { RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTSQUARE,int(M),int(N)))); }
  n=N;
  band=lowerWidth+upperWidth+1;
  bandu=upperWidth;
  zeroUnusedEntries();
}

DoubleBandMat::DoubleBandMat(const DoubleVec& data, unsigned M, unsigned N, unsigned lowerWidth, unsigned upperWidth)
  : vec(data)
{
  if (M!=N) { RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTSQUARE,int(M),int(N)))); }
  n=N;
  band=lowerWidth+upperWidth+1;
  bandu=upperWidth;
  if (vec.length()!=BandMatSize(N,band))
    RWTHROW(RWInternalErr(RWMessage(RWLAPK_NUMBERPOINTSBAND,vec.length(),n,n,className,band)));
}

/*
 * Set entries in the data vector which are not used to zero.  This
 * avoids purify warnings and also ensures we won't have troubles
 * adding and subtracting these matrices
 */
void DoubleBandMat::zeroUnusedEntries()
{
    int i=lowerBandwidth();	   // Start i at min(n-1,upperBandwidth()-1)
    if (i>n-1) i=n-1;
    for( ; i>=0; i-- ) {
      vec.slice( i*bandwidth(), upperBandwidth()-i ) = (Double)(0);
    }
    i=lowerBandwidth()-1;	   // Start i at min(n-1,lowerBandwidth()-1)
    if (i>n-1) i=n-1;
    for( ; i>=0; i-- ) {
      vec.slice( (n-i)*bandwidth()-lowerBandwidth()+i, lowerBandwidth()-i ) = (Double)(0);
    }
}

Double DoubleBandMat::bcval(int i, int j) const
{
    if ( i<0 || i>=rows() || j<0 || j>=cols() ) {
      RWTHROW(RWBoundsErr(RWMessage(RWLAPK_OUTOFBOUNDS,i,j,rows(),cols())));
    }
    return ( (i<=j) ? ((j-i)>bandu) : ((i-j)>=(band-bandu)))
			? (Double)(0) : vec(i-j+bandu + j*band);
}

Double DoubleBandMat::bcset(int i, int j, Double x)
{
    if ( i<0 || i>=rows() || j<0 || j>=cols() ) {
      RWTHROW(RWBoundsErr(RWMessage(RWLAPK_OUTOFBOUNDS,i,j,rows(),cols())));
    }
    if ( (j>=i) ? ((j-i)>bandu) : ((i-j)>=(band-bandu)) )
                            RWTHROW(RWBoundsErr(RWMessage(RWLAPK_NOTSETABLE,i,j,className))); 
    return vec(i-j+bandu + j*band)=x;
}

RODoubleRef DoubleBandMat::bcref(int i, int j)
{
    if ( i<0 || i>=rows() || j<0 || j>=cols() ) {
      RWTHROW(RWBoundsErr(RWMessage(RWLAPK_OUTOFBOUNDS,i,j,rows(),cols())));
    }
    return ( (i<=j) ? ((j-i)>bandu) : ((i-j)>=(band-bandu)))
	    		? RODoubleRef(rwDoubleZero,TRUE)
			: RODoubleRef(vec(i-j+bandu + j*band),FALSE);
}

DoubleVec DoubleBandMat::bcdiagonal(int i) const
{
  if ( i>=int(cols()) || (-i)>=int(rows()) ||
		i>int(upperBandwidth()) || (-i)>int(lowerBandwidth())) {
    RWTHROW(RWBoundsErr(RWMessage(RWLAPK_DIAGOUTOFBOUNDS,-i,className,rows(),cols())));
  }
    int iabs = (i>=0) ? i : -i;
    return vec.slice(i>0 ? upperBandwidth()+i*(bandwidth()-1)
			 : upperBandwidth()+iabs
			 , n-iabs, bandwidth() );
}

/*
 * Here are non-inline versions of the row(), col(), and diagonal()
 * routines.  Normally these would be inlined, but due to a bug
 * in the Borland C++ compiler it is best not to inline them with
 * Borland C++.
 *
 * These routines just call the bounds checking routines.
 */

/*
 * The leadingSubmatrix function
 */

DoubleBandMat DoubleBandMat::leadingSubmatrix(int k)
{
  if (k<0 || k>n) RWTHROW(RWBoundsErr(RWMessage(RWLAPK_LEADINGSUBMATRIXORDER,k,n)));
  return DoubleBandMat( vec.slice(0,BandMatSize(k,bandwidth())), k, k,lowerBandwidth(),upperBandwidth());
}

DoubleBandMat& DoubleBandMat::operator=(const DoubleBandMat& M)
{
    if (rows()!=M.rows() || cols()!=M.cols()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATSIZE,M.rows(),M.cols(),rows(),cols())));
    }
    if (lowerBandwidth()<M.lowerBandwidth() ||
	upperBandwidth()<M.upperBandwidth()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_BANDSIZE,M.lowerBandwidth(),lowerBandwidth(),
				upperBandwidth(), M.upperBandwidth() )));
    }
    if (bandwidth()==M.bandwidth()) {
      vec = M.vec;
    } else {
      int u = upperBandwidth();
      int Mu = M.upperBandwidth();
      int l = lowerBandwidth();
      int Ml = M.lowerBandwidth();
      for( int i= -l; i<=u; i++ ) {
	if ( i<-Ml || i>Mu ) diagonal(i)=(Double)(0);
	else diagonal(i)=M.diagonal(i);
      }
    }
    return *this;
}

DoubleBandMat& DoubleBandMat::reference(DoubleBandMat& A)
{
    vec.reference(A.vec);
    n=A.n;
    band=A.band;
    bandu=A.bandu;
    return *this;
}

DoubleBandMat DoubleBandMat::copy() const
{
  DoubleBandMat A( *((DoubleBandMat*)this) );	// cast this to non-const to make cfront happy
  A.deepenShallowCopy();
  return A;
}

void DoubleBandMat::resize(unsigned M, unsigned N)
{
    if (M!=N) { RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTSQUARE,int(M),int(N)))); }
    // Zero out elements past the bottom of the matrix if need be so
    // they don't enter in to the new matrix
    zeroUnusedEntries();
    n=N;
    vec.resize(BandMatSize(n,bandwidth()));

  // Finally, zero any unused entries just to keep things tidy
  zeroUnusedEntries();
}

void DoubleBandMat::resize(unsigned M, unsigned N, unsigned lowWidth, unsigned upWidth)
{
  resize(M,N);	// Set the dimension - all that's left is to change bandwidth
  int l = lowerBandwidth();  // Convert all unsigneds to ints to avoid nasty
  int u = upperBandwidth();  // surprises when trying stuff like i= -lowWidth
  int lw = lowWidth;
  int uw = upWidth;
  if (l!=lowWidth || u!=upWidth) {
    DoubleBandMat newMat(M,N,lowWidth,upWidth);
    for( int i= -lw; i<=uw; i++ ) {
      DoubleVec d = newMat.diagonal(i);
      if ( i<(-l) || i>u ) { d = (Double)(0); }
      else                 { d = this->diagonal(i); }
    }
    this->reference(newMat);
  }
}


/*
 * printOn,scanFrom:  printOn prints all non-numeric stuff first
 * then prints the numbers defining the shape and size of the matrix,
 * then prints the matrix itself.  The format used by printOn can be
 * used to read in a matrix with scanFrom.
 *
 * scanFrom first eats all the non-numeric characters it encounters.
 * This takes care of the stuff printOn spews before the data.  Next
 * it reads the numbers which define the matrix shape and then the
 * data is read in.
 */

void DoubleBandMat::printOn(ostream& outs) const
{
    int w = outs.width(0);
    int m=n;
      outs << className << ", ";
    outs << "lower and upper bandwidths: " << lowerBandwidth() << " ";
    outs << upperBandwidth() << " ";
    outs << rows() << "x" << cols() << " [\n";
    for( int i=0; i<m; i++ ) {
      for( int j=0; j<n; j++ ) {
	outs << setw(w) << val(i,j) << " ";
      }
      outs << "\n";
    }
    outs << "]";
}

void DoubleBandMat::scanFrom(istream& s)
{
  char c;
  unsigned numRows, numCols;
  unsigned lwidth, uwidth;

  /* Skip through leading non-digits */
  do { s.get(c); } while (!s.fail() && !isdigit(c));
  s.putback(c);

  s >> lwidth >> uwidth;

  s >> numRows;
  do { s.get(c); } while (isspace(c));	// Skip optional x character
  if (isdigit(c)) s.putback(c);	
  s >> numCols;

  DoubleVec v;			// Read the data
  v.scanFrom(s);

  if (!s.fail()) { 		// Now put the data into the matrix
    if (v.length()!=numRows*numCols) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_WRONGNUMPOINTS,v.length(),className,numRows,numCols)));
    } else {
      resize(numRows,numCols,lwidth,uwidth);
      int index = 0;
      for(int i=0; i<numRows; i++) {
        for(int j=0; j<numCols; j++) {
	  if (val(i,j)!=v[index]) bcset(i,j,v[index]);
	  index++;
	}
      }
    }
  }
}

void DoubleBandMat::saveOn(RWFile& file) const
{
    /* First write the numbers which define the shape of the matrix */
    file.Write(n);
    file.Write(band);
    file.Write(bandu);
    vec.saveOn(file);
}

void DoubleBandMat::saveOn(RWvostream& s) const
{
    /* First write the numbers which define the shape of the matrix */
    s << n;
    s << band;
    s << bandu;
    vec.saveOn(s);
}

void DoubleBandMat::restoreFrom(RWFile& file)
{
    /* First read the numbers which define the shape of the matrix */
    file.Read(n);
    file.Read(band);
    file.Read(bandu);
    int size = BandMatSize(n,band);
    vec.restoreFrom(file);
    if (vec.length() != size) RWTHROW(RWInternalErr(RWMessage(RWLAPK_RESTORESIZE,className,vec.length(),size)));
}

void DoubleBandMat::restoreFrom(RWvistream& s)
{
    /* First read the numbers which define the shape of the matrix */
    s >> n;
    s >> band;
    s >> bandu;
    int size = BandMatSize(n,band);
    vec.restoreFrom(s);
    if (vec.length() != size) RWTHROW(RWInternalErr(RWMessage(RWLAPK_RESTORESIZE,className,vec.length(),size)));
}

unsigned DoubleBandMat::binaryStoreSize() const
{
    /* First determine the size of the stuff
       which determines the matrix shape.    */
    unsigned size = 3*sizeof(unsigned);  // n and band and bandu
    return size + vec.binaryStoreSize();
}

> #################################################################
> # The master template file for symmetric eigen-servers          #
> #                                                               #
> # $Header: /users/rcs/lapksrc/xseigsrv.h,v 1.2 1993/07/05 19:19:33 alv Exp $
> #                                                               #
> # Copyright (1993) by Rogue Wave Software, Inc.  This           #
> # software is subject to copyright protection under the laws    #
> # of the United States and other countries.                     #
> #                                                               #
> # Written by Al Vermeulen.                                      #
> #                                                               #
> # Limited License.                                              #
> #################################################################
> #
> # $Log: xseigsrv.h,v $
> # Revision 1.2  1993/07/05  19:19:33  alv
> # fixed up for complex numbers
> #
> # Revision 1.1  1993/04/06  15:44:43  alv
> # Initial revision
> #
>
> # Macros that we need
> 
> define <T>=RW_T_RW
> 
> if <T>==Double
> define <P>=Double <tLetter>=d <Sym>=Sym <sym>=sym <sLetter>=s
> elif <T>==Float
> define <P>=Float <tLetter>=f <Sym>=Sym <sym>=sym <sLetter>=s
> elif <T>==DComplex
> define <P>=Double <tLetter>=c <Sym>=Herm <sym>=herm <sLetter>=h <pLetter>=d
> else
> err Symmetric Eigen-decomposition: not defined for '<T>'
> endif
>
> beginwrapper <tLetter><sLetter>eigsrv

/*
 * <T><Sym>EigServer     - Abstract base for symmetric/Hermitian eigenservers
 * <T><Sym>QREigServer   - QR method, this is the default server
 * <T><Sym>PDQREigServer - QR method for positive definite matrices
 * <T><Sym>RFQREigServer - Root free variant of QR (can't compute eigenvectors)
 * <T><Sym>BisEigServer  - Bisection method
 *
 * Generated from template $Id: xseigsrv.h,v 1.2 1993/07/05 19:19:33 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * Symmetric/Hermitian eigen-decomposition servers.  This classes
 * represent various algorithms for computing spectral
 * factorizations.  They are useful for people who demand more
 * control over the computation of eigen values/vectors than
 * is provided by the constructors in <T><Sym>EigDecomp.
 *
 */ 

#include "rw/lapkdefs.h"
#include "rw/<tLetter>genmat.h"
#include "rw/<tLetter>vec.h"
#include "rw/<tLetter><sym>eig.h"  
> if <P>!=<T>
#include "rw/<pLetter>symeig.h"
> endif
class <T><Sym>TriDiagDecomp;

class <T><Sym>EigServer {
protected:
  static unsigned&  n(<T><Sym>EigDecomp& x)           {return x.n;}
  static <P>Vec&    lambda(<T><Sym>EigDecomp& x)      {return x.lambda;}
  static <T>GenMat& P(<T><Sym>EigDecomp& x)           {return x.P;}
  static RWBoolean& computedAll(<T><Sym>EigDecomp& x) {return x.computedAll;}
  static RWBoolean& accurate(<T><Sym>EigDecomp& x)    {return x.accurate;}
> if <P>!=<T>
  static unsigned&  n(<P>SymEigDecomp& x)           {return x.n;}
  static <P>Vec&    lambda(<P>SymEigDecomp& x)      {return x.lambda;}
  static <P>GenMat& P(<P>SymEigDecomp& x)           {return x.P;}
  static RWBoolean& computedAll(<P>SymEigDecomp& x) {return x.computedAll;}
  static RWBoolean& accurate(<P>SymEigDecomp& x)    {return x.accurate;}
> endif
    // These functions provide access to the guts of the EigDecomp
    // object for the server.  This way we can declare more servers
    // without having to make them all friends of EigDecomp.

public:
  virtual <T><Sym>EigDecomp operator()(const <T><Sym>Mat&);
  virtual <T><Sym>EigDecomp operator()(const <T><Sym>BandMat&);
  virtual <P>SymEigDecomp   decompose(const <T><Sym>TriDiagDecomp&) =0;
    // The op() which take matrices work by first building a 
    // tri-diagonal decomposition, computing its eigen-decomposition,
    // and then transforming to the original matrix.  The function
    // to decompose a tri diagonal matrix is called decompose() rather
    // than op() to allow its redefinition in subclasses without hiding
    // the op() functions.

  virtual RWBoolean computeEigenVectors() const =0;
    // Is this server configured to compute eigenvectors as well as
    // eigenvalues?  This is nice to know when forming the tri-diagonal
    // decomposition of a matrix, so we can decide whether or not
    // to keep the orthogonal matrix part.
};

class <T><Sym>QREigServer : public <T><Sym>EigServer {
private:
  RWBoolean computeVecs_;
public:
  <T><Sym>QREigServer(RWBoolean computeVecs=TRUE);
  virtual <P>SymEigDecomp   decompose(const <T><Sym>TriDiagDecomp&);
  virtual RWBoolean computeEigenVectors() const;
  void              computeEigenVectors(RWBoolean);
};
  
class <T><Sym>PDQREigServer : public <T><Sym>EigServer {
private:
  RWBoolean computeVecs_;
public:
  <T><Sym>PDQREigServer(RWBoolean computeVecs=TRUE);
  virtual <P>SymEigDecomp   decompose(const <T><Sym>TriDiagDecomp&);
  virtual RWBoolean computeEigenVectors() const;
  void              computeEigenVectors(RWBoolean);
};
    
class <T><Sym>RFQREigServer : public <T><Sym>EigServer {
public:
  <T><Sym>RFQREigServer();
  virtual <P>SymEigDecomp   decompose(const <T><Sym>TriDiagDecomp&);
  virtual RWBoolean computeEigenVectors() const;
};
                
class <T><Sym>SomeEigServer  : public <T><Sym>EigServer {
private:
  RWBoolean computeVecs_;
  <P>       tolerance_;
  RWSlice   range;  
public:
  <T><Sym>SomeEigServer(RWBoolean computeVecs=TRUE);
  virtual <P>SymEigDecomp   decompose(const <T><Sym>TriDiagDecomp&);
  <P>               setTolerance(<P>);
  RWSlice           setRange(const RWSlice&);
  virtual RWBoolean computeEigenVectors() const;
  void              computeEigenVectors(RWBoolean);
};

class <T><Sym>RangeEigServer  : public <T><Sym>EigServer {
private:
  RWBoolean computeVecs_;
  <P>       tolerance_;
  <P>       small_;
  <P>       large_;
public:
  <T><Sym>RangeEigServer(RWBoolean computeVecs=TRUE);
  virtual <P>SymEigDecomp   decompose(const <T><Sym>TriDiagDecomp&);
  <P>               setTolerance(<P>);
  void              setRange(<P>,<P>);
  <P>               setSmall(<P>);
  <P>               setLarge(<P>);
  virtual RWBoolean computeEigenVectors() const;
  void              computeEigenVectors(RWBoolean);
};

> endwrapper
 

/*
 * Implementation of FloatSymQREigServer
 *
 * Generated from template $Id: xseigqr.cpp,v 1.4 1993/10/11 21:48:22 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * Uses Lapack's QR/QL algorithm to compute eigenvalues/vectors of
 * a tridiagonal matrix.
 */ 

#include "rw/fseigsrv.h"
#include "rw/ftd.h"
#include "rw/lapack.h"


FloatSymQREigServer::FloatSymQREigServer(RWBoolean vecs)
{
  computeVecs_ = vecs;
}

FloatSymEigDecomp FloatSymQREigServer::decompose(const FloatSymTriDiagDecomp& decomp)
{
  char compz = computeVecs_ ? 'I' : 'N';
  long n = decomp.rows();
  FloatVec d = decomp.diagonal().copy();
  FloatVec e(decomp.rows(),rwUninitialized);   // lapack needs a length n (not n-1) off diagonal
  if (n>0) e(RWSlice(0,decomp.rows()-1)) = decomp.offDiagonal();
  int zsize = computeVecs_ ? decomp.rows() : 0;
  FloatGenMat Z(zsize,zsize,rwUninitialized);
  long ldz = computeVecs_ ? n : 1;
  Float *work = computeVecs_ ? new Float [2*n+1] : 0;  // Needs only 2*n-2, but this way we avoid <0 checks
  long info;
  ssteqr(compz,n,d.data(),e.data(),Z.data(),ldz,work,info);
  delete work;
  RWPOSTCONDITION(info>=0);

  FloatSymEigDecomp eig;
  FloatSymQREigServer::n(eig) = decomp.rows();
  lambda(eig).reference(d);
  P(eig).reference(Z);
  if (info>0) {
    unsigned numComputed = (unsigned)info;   // Make an explicit unsigned to avoid warnings on SUN
    lambda(eig).resize(numComputed);
    P(eig).resize(decomp.rows(),numComputed);
    computedAll(eig) = FALSE;
  }
  return eig;
}

RWBoolean FloatSymQREigServer::computeEigenVectors() const {return computeVecs_;}

void FloatSymQREigServer::computeEigenVectors(RWBoolean x) {computeVecs_=x;}


  

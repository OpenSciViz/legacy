/*
 * Implementation of DoubleSymFact
 *
 * Generated from template $Id: xfct.cpp,v 1.6 1993/10/11 21:48:11 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/lapkdefs.h"
#include "rw/dsymfct.h"
#include "rw/lapack.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"  
#include "rw/dgenmat.h"

const char* DoubleSymFact::className = "DoubleSymFact";

DoubleSymFact::~DoubleSymFact()
{
  delete [] pvts;
}

DoubleSymFact::DoubleSymFact()
{
  info = -1;
  pvts=0;
}

DoubleSymFact::DoubleSymFact( const DoubleSymFact& A )
  : factorization(A.factorization)
{
  info = A.info;
  Anorm = A.Anorm;
  unsigned n = A.rows();
  pvts = (n>0) ? new long [n] : 0;
  while(n--) {
    pvts[n] = A.pvts[n];
  }
}

DoubleSymFact::DoubleSymFact(const DoubleSymMat& A, RWBoolean estimateCondition)
  : factorization(A)  // Use reference semantics, followed by
                      // deepenShallowCopy(), to work around a BCC V3.0 bug
{
    factorization.deepenShallowCopy();
    pvts = 0;
    dofactor(estimateCondition);
}

void DoubleSymFact::factor( const DoubleSymMat& A, RWBoolean estimateCondition )
{
    DoubleSymMat Acopy = A.copy();
    factorization.reference(Acopy);
    dofactor(estimateCondition);
}

void DoubleSymFact::dofactor(RWBoolean estimateCondition)
{
   info=0;   // for size zero matrices, need to set this explicitly
   long n = rows();	

    // Error (sanity) checking
    RWPRECONDITION(factorization.dataVec().stride()==1);

    delete [] pvts;
    pvts = (n>0) ? new long [n] : 0;

    // If we care to ever find the condition number, we need to know
    // the 1-norm of the matrix A.  Compute it while the member data
    // factorization is still a copy of A, and not yet it's factorization.
    Anorm = -1; 
    if (n>0 && estimateCondition) {
      Double *work = new Double [n];
      Anorm = dlansp('1','U',n,factorization.data(),work);
      delete [] work;
    }

    if (n>0) {
      dsptrf('U',n,factorization.data(),pvts,info);
      RWPOSTCONDITION(info>=0);
    }
}

RWBoolean DoubleSymFact::fail() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}


RWBoolean DoubleSymFact::isSingular() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}

Double DoubleSymFact::condition() const
{
    Double Anormcopy = Anorm;      // Copy to stop warnings about ref parm
    long info;
    long n = rows();
    Double rcond;
    Double *work = new Double[4*n];   // Some routines only require 2*n, but what the hell
    long *iwork = new long [n];
    dspcon('U',n,(Double*)factorization.data(),pvts,Anormcopy,rcond,work,iwork,info);
    delete [] iwork;
    delete [] work;
    RWPOSTCONDITION(info==0);
    return rcond;
}

DoubleVec DoubleSymFact::solve( const DoubleVec& b ) const
{
  DoubleGenMat B(b,b.length(),1);
  DoubleGenMat X = solve(B);
  RWPOSTCONDITION(X.cols()==1);
  return X.col(0);
}

DoubleGenMat DoubleSymFact::solve( const DoubleGenMat& B ) const
{
    DoubleGenMat X(B.copy());
    long n = rows();
    if (B.rows()!=n) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,B.rows(),n)));
    if (fail()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTSOLVE)));
    long nrhs = X.cols();
    long info;
    if (n>0) {
    dsptrs('U',n,nrhs,(Double*)factorization.data(),pvts,X.data(),n,info);
      RWPOSTCONDITION(info==0);
    }
    return X;    
}

Double DoubleSymFact::determinant() const
{
    // see linpack manual pg 5.16 for description of algorithm
    Double det = 1.0;
    for(int i=rows(); i--;) {
      RWPRECONDITION(pvts[i]!=0);
      if (pvts[i]>0) {
        det *= factorization.val(i,i);  // one by one block
      } else {                          // two by two block
        RWPRECONDITION(i>0 && pvts[i-1]==pvts[i]);
        Double a = factorization.val(i,i);  // .val stops sun warning
        Double b = factorization.val(i-1,i);
        Double c = factorization.val(i-1,i-1);
        Double blockdet = a*c-b*b;     // see linpack manual for a better way that avoids overflow and (as much) roundoff
        det *= blockdet;
        i--;
      }
    }
    return det;
}
              
DoubleSymMat DoubleSymFact::inverse( ) const
{
    if (rows()==0) { return factorization; }
    if (fail()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTSOLVE)));
    DoubleSymMat soln(factorization);
    soln.deepenShallowCopy();
    long n = rows();
    long info;
    Double *work = new Double [n];  
    dsptri('U',n,soln.data(),pvts,work,info);
    delete [] work;
    RWPOSTCONDITION(info==0);
    return soln;
}


/*
 * Global functions which provide alternate ways to call member functions.
 * These used to be inline, but this requires that you include more header
 * files on some compilers, and that is just not worth it.
 */
DoubleVec solve(const DoubleSymFact& A, const DoubleVec& b) { return A.solve(b); }
DoubleGenMat solve(const DoubleSymFact& A, const DoubleGenMat& b) { return A.solve(b); }
Double    determinant (const DoubleSymFact& A)	      { return A.determinant(); }

DoubleSymMat inverse(const DoubleSymFact& A)      	      { return A.inverse(); }

Double condition(const DoubleSymFact& A) { return A.condition(); }



> # $Header: /users/rcs/lapksrc/xseigbis.cpp,v 1.6 1993/10/13 22:08:28 alv Exp $
> #                                                               #
> # $Log: xseigbis.cpp,v $
> # Revision 1.6  1993/10/13  22:08:28  alv
> # ported to float precision
> #
> # Revision 1.5  1993/10/11  21:48:21  alv
> # synch'ed with Math.h++ v5.1
> #
> # Revision 1.4  1993/07/08  23:26:13  alv
> # ported to SUN CC v3
> #
> # Revision 1.3  1993/07/05  19:19:33  alv
> # fixed up for complex numbers
> #
> # Revision 1.2  1993/04/06  20:45:04  alv
> # ported to sun C++ v3.0.1
> #
> # Revision 1.1  1993/04/06  15:44:42  alv
> # Initial revision
> #
>
> define <T>=RW_T_RW
> 
> if <T>==Double
> define <P>=Double <tLetter>=d <Sym>=Sym <sym>=sym <sLetter>=s <stebz>=dstebz <stein>=dstein
> elif <T>==Float
> define <P>=Float <tLetter>=f <Sym>=Sym <sym>=sym <sLetter>=s <stebz>=sstebz <stein>=sstein
> elif <T>==DComplex
> define <P>=Double <tLetter>=c <Sym>=Herm <sym>=herm <sLetter>=h <stebz>=dstebz <stein>=dstein
> else
> err Symmetric Eigen-decomposition: not defined for '<T>'
> endif
>
/*
 * Implementation of <T><Sym>SomeEigServer and <T><Sym>RangeEigServer
 *
 * Generated from template $Id: xseigbis.cpp,v 1.6 1993/10/13 22:08:28 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */ 

#include "rw/<tLetter><sLetter>eigsrv.h"
#include "rw/<tLetter>td.h"
#include "rw/lapack.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include <float.h>  /* looking for max and min values to initialize RangeEigServers */

<T><Sym>SomeEigServer::<T><Sym>SomeEigServer(RWBoolean computeVecs)
 : range((const RWSlice&)RWAll)  // Sun seems to need the explicit cast
{
  computeVecs_ = computeVecs;
  tolerance_ = 0;
}

<P>               <T><Sym>SomeEigServer::setTolerance(<P> t) 
{
  <P> temp = tolerance_;
  tolerance_ = t;
  return temp;
}
  
RWSlice <T><Sym>SomeEigServer::setRange(const RWSlice& x)
{
  RWSlice temp = range;
  range = x;
  return temp;
}

RWBoolean <T><Sym>SomeEigServer::computeEigenVectors() const
{
  return computeVecs_;
}

void              <T><Sym>SomeEigServer::computeEigenVectors(RWBoolean x)
{
  computeVecs_ = x;
}

<P>SymEigDecomp   <T><Sym>SomeEigServer::decompose(const <T><Sym>TriDiagDecomp& decomp)
{
  <P>SymEigDecomp eig;
  <T><Sym>EigServer::n(eig) = decomp.rows();

  // Put together a few things both the lapack routines can use
  long n = decomp.rows();
  <P>Vec d = decomp.diagonal().copy();
  <P>Vec e(decomp.rows(),rwUninitialized);   // lapack needs a length n (not n-1) off diagonal
  if (n>0) e(RWSlice(0,decomp.rows()-1)) = decomp.offDiagonal();
  <P>Vec w(decomp.rows(),rwUninitialized);  // eigenvalues in here
  long m;
  long *iblock = new long [n];
  long *isplit = new long [n];
  <P> *work = new <P> [5*n];
  long *iwork = new long [3*n];
  long info;

  // determine range of interest, and make sure it is sensible.
  long il = range.begin()+1;
  long iu = il+range.stride()*(range.len(decomp.rows())-1);
  if (range.stride()<0) {
    long temp = il;
    il = iu;
    iu = temp;
  }         
  if (iu>decomp.rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOEIG,iu)));
  
  // compute eigenvalues
  if (iu>=il) {   // ie skip if we don't want any eigenvalues
    long nsplit;
    <P> junk;
    <stebz>('I','E',n,junk,junk,il,iu,tolerance_,d.data(),e.data(),m,nsplit,w.data(),iblock,isplit,work,iwork,info);
    RWPOSTCONDITION(info>=0);
    RWPOSTCONDITION(m<=iu-il+1);
    if (m<il-il+1) computedAll(eig)=FALSE;
    if (info==1 || info==3) {accurate(eig)=FALSE;}
    // toss out some if some weirdo used a non-unit stride, reverse
    // for negative stride
    if (range.stride()<0) w(RWRange(w.length()-1,0)) = w;  // reverse
    int str = range.stride();
    if (str<0) {str=(-str);}
    if (str>1) {
      m = (m+str-1)/str;
      w(RWSlice(0,(unsigned)m)) = w(RWToEnd(0,str));
    }
    lambda(eig).reference(w(RWSlice(0,(unsigned)m)));

    // compute eigenvectors
    if (computeVecs_ && m>1) {
      P(eig).reshape(decomp.rows(),(unsigned)m);
      <stein>(n,d.data(),e.data(),m,w.data(),iblock,isplit,P(eig).data(),n,work,iwork,iwork+n,info);
      RWPOSTCONDITION(info>=0);
      if (info>0) accurate(eig)=FALSE;
    }
  }

  delete [] iblock;
  delete [] isplit;
  delete [] work;
  delete [] iwork;

  return eig;
}

/********************************************
 * Implementation of <T><Sym>RangeEigServer *
 ********************************************/
 
<T><Sym>RangeEigServer::<T><Sym>RangeEigServer(RWBoolean computeVecs)
{
  computeVecs_ = computeVecs;
  tolerance_ = 0;
> if <P>==Float
  small_ = -FLT_MAX;
  large_ = FLT_MAX;
> else
  small_ = -DBL_MAX;
  large_ = DBL_MAX;
> endif
}

void              <T><Sym>RangeEigServer::setRange(<P> x, <P> y)
{
  small_ = x;
  large_ = y;
}

<P>               <T><Sym>RangeEigServer::setSmall(<P> x)
{
  <P> temp = small_;
  small_ = x;
  return temp;
}

<P>               <T><Sym>RangeEigServer::setLarge(<P> x)
{
  <P> temp = large_;
  large_ = x;
  return temp;
}

<P>               <T><Sym>RangeEigServer::setTolerance(<P> t)
{
  <P> temp = tolerance_;                                   
  tolerance_ = t;
  return temp;
}

RWBoolean <T><Sym>RangeEigServer::computeEigenVectors() const
{
  return computeVecs_;
}

void              <T><Sym>RangeEigServer::computeEigenVectors(RWBoolean x)
{
  computeVecs_ = x;
}
    
<P>SymEigDecomp   <T><Sym>RangeEigServer::decompose(const <T><Sym>TriDiagDecomp& decomp)
{
  <P>SymEigDecomp eig;
  <T><Sym>EigServer::n(eig) = decomp.rows();

  // Put together a few things both the lapack routines can use
  long n = decomp.rows();
  <P>Vec d = decomp.diagonal().copy();
  <P>Vec e(decomp.rows(),rwUninitialized);   // lapack needs a length n (not n-1) off diagonal
  if (n>0) e(RWSlice(0,decomp.rows()-1)) = decomp.offDiagonal();
  <P>Vec w(decomp.rows(),rwUninitialized);  // eigenvalues in here
  long m;
  long *iblock = new long [n];
  long *isplit = new long [n];
  <P> *work = new <P> [5*n];
  long *iwork = new long [3*n];
  long info;

  // compute eigenvalues
  if (large_>small_) {   // ie skip if we don't want any eigenvalues
    long nsplit;
    long junk;
    <stebz>('V','E',n,small_,large_,junk,junk,tolerance_,d.data(),e.data(),m,nsplit,w.data(),iblock,isplit,work,iwork,info);
    RWPOSTCONDITION(info>=0);
    if (info==4) computedAll(eig)=FALSE;
    if (info==1 || info==3) {accurate(eig)=FALSE;}
    lambda(eig).reference(w(RWSlice(0,(unsigned)m)));

    // compute eigenvectors
    if (computeVecs_ && m>1) {
      P(eig).reshape(decomp.rows(),(unsigned)m);
      <stein>(n,d.data(),e.data(),m,w.data(),iblock,isplit,P(eig).data(),n,work,iwork,iwork+n,info);
      RWPOSTCONDITION(info>=0);
      if (info>0) accurate(eig)=FALSE;
    }
  }

  delete [] iblock;
  delete [] isplit;
  delete [] work;
  delete [] iwork;

  return eig;
}
   

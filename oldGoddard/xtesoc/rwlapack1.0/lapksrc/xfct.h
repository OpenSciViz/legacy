> #################################################################
> # The master template file for the factorization types          #
> #                                                               #
> # $Header: /users/rcs/lapksrc/xfct.h,v 1.3 1993/07/05 19:19:33 alv Exp $
> #                                                               #
> # Copyright (1991-1993) by Rogue Wave Software, Inc.  This      #
> # software is subject to copyright protection under the laws    #
> # of the United States and other countries.                     #
> #                                                               #
> # Written by Al Vermeulen.                                      #
> #                                                               #
> # Limited License.                                              #
> #################################################################
> #
> # $Log: xfct.h,v $
> # Revision 1.3  1993/07/05  19:19:33  alv
> # fixed up for complex numbers
> #
> # Revision 1.2  1993/04/06  20:45:04  alv
> # ported to sun C++ v3.0.1
> #
> # Revision 1.1  1993/04/06  15:44:42  alv
> # Initial revision
> #
> #
> # Ported from Linpack.h++ to Lapack.h++
>
> include fct.mac
> 
> # The traditional top of the file stuff
>
> beginwrapper <tLetter><factAbbrev>fct

/*
> if <F>==GenFact
 * <T>GenFact - An LU factorization of a square matrix
> elif <F>==SymFact
 * <T><F> - A symmetric indefinite U^T D U factorization
> elif <F>==HermFact
 * <T><F> - A U^T D U factorization of an indefinite Hermetian matrix
> elif <F>==PDFact
 * <T><F> - A Cholesky factoration of a positive definite matrix
> elif <F>==BandFact
 * <T><F> - An LU decomposition of a band matrix
> elif <F>==PDBandFact
 * <T><F> - A factorization of a positive definite band matrix
> elif <F>==TriDiagFact
 * <T><F> - A factorization of a tri-diagonal matrix
> elif <F>==PDTriDiagFact
 * <T><F> - A factorization of a positive definite tri-diagonal matrix
> else
> err  unknown factorization '<F>'
> endif
 *
 * Generated from template $Id: xfct.h,v 1.3 1993/07/05 19:19:33 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1988-1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * This header file will work both with lapack.h++ and linpack.h++.
 */

#include "rw/<tLetter><shapeAbbrev>mat.h"
> if <F>!=PDFact && <F>!=PDBandFact && <F>!=PDTriDiagFact
#include "rw/ivec.h"
> endif

>
> # The class declaration and the implementation
>
class <T><F> {
private:
> if <F>==TriDiagFact
    <T>Vec d;      // diagonal
    <T>Vec dl;     // subdiagonal
    <T>Vec du;     // superdiagonal
    <T>Vec d2;     // second superdiagonal of U
> elif <F>==PDTriDiagFact
    <P>Vec d;      // diagonal
    <T>Vec e;      // off-diagonal
> else
  <T><M> factorization;
> endif
> if <F>!=PDFact && <F>!=PDBandFact && <F>!=PDTriDiagFact
    long *pvts;            // used by Lapack.h++
    IntVec pivots;         // used by Linpack.h++
> endif
    // The factorization and the pivot vector.
    // This cannot be a union because IntVec has a constructor

union {
    long   info;           // used by Lapack.h++
> if <F>==PDFact || <F>==PDBandFact || <F>==PDTriDiagFact
    int pdinfo;            // used by Linpack.h++
> endif
};
  // If info is non-zero, something went wrong in the factorization.
  
union { 
    <P>    Anorm;          // used by Lapack.h++
    <P>    cond;           // used by Linpack.h++
};
    // If Anorm is non-negative, then it is the infinity norm of the
    // original matrix.  This is used when computing the condition
    // number.
    // cond is the reciprocal condition number if calculated.  If
    // condition number not calculated contains 0 for singularity
    // else -1.

protected:
           
    static const char *className;
    // For use in error messages

    void dofactor(RWBoolean estimateCondition);
    // Do the factorization.  Call this after the matrix 
    // variable factorization is set up.

    void calldi(const <T><M>& A, int job, <T>* det, int *inert) const;
    // call the linpack xxxdi routine with the job number indicated
    // and with the 2 byte array det.

public:
    <T><F>();
    <T><F>( const <T><F>& );
    <T><F>( const <T><M>& A, RWBoolean estimateCondition=TRUE );
    void factor( const <T><M>& A, RWBoolean estimateCondition=TRUE );
    ~<T><F>();
    // The constructors.
    // You can save a little bit of time by not estimating the
    // condition of the factorization, and thus giving up any 
    // knowledge of how accurate solutions obtained using this
    // factorization will be.

    RWBoolean good() const { return !fail(); }
    RWBoolean fail() const;
    RWBoolean isSingular() const;
> if <M>==TriDiagMat
    int       rows() const { return d.length(); }
    int       cols() const { return d.length(); }
> else
    int       rows() const { return factorization.rows(); }
    int       cols() const { return factorization.cols(); }
> endif
    <P> condition() const;
> if <F>==PDFact || <F>==PDBandFact || <F>==PDTriDiagFact
    RWBoolean isPD() const;
> endif
    // If fail() returns true calling solve() or inverse() or ... may
    // fail.  condition() returns an approximation to the reciprocal
    // condition number of the factorization.

    <T>Vec solve(const <T>Vec& b) const;
    <T>GenMat solve(const <T>GenMat& b) const;
    <T> determinant() const;
> if <F>==GenFact || <F>==SymFact || <F>==HermFact || <F>==PDFact
    <T><M> inverse() const;
> endif
    // solve and inverse functions
};

<T>Vec solve(const <T><F>& A, const <T>Vec& b);
<T>GenMat solve(const <T><F>& A, const <T>GenMat& b);
<T>    determinant (const <T><F>& A);

> if <F>==GenFact || <F>==SymFact || <F>==HermFact || <F>==PDFact
<T><M> inverse(const <T><F>& A);
> endif

<P> condition(const <T><F>& A);

> endwrapper

#if 0
/*
 * Generated from template file $Id: xmat.cpp,v 1.4 1993/10/11 21:48:16 alv Exp $
 *
 * The main code file for DoubleSymBandMat
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */
#endif

#include "rw/dsbndmat.h"

#include "rw/rstream.h"
#include "rw/rwfile.h"
#include "rw/vstream.h"
#include "rw/lapkerr.h"
#include "rw/rwerr.h"
#  ifdef __ZTC__
#    include <iomanip.hpp>
#  else
#    ifdef __GLOCK__
#      include <iomanip.hxx>
#    else
#      include <iomanip.h>
#    endif
#  endif
STARTWRAP
#include <stdio.h>
#include <ctype.h>
ENDWRAP

const char* DoubleSymBandMat::className = "DoubleSymBandMat";

/*
 * These inline functions
 * give the number of elements that need be stored to represent
 * the different types of matrices
 */

inline unsigned GenMatSize(unsigned n, unsigned m)  { return m*n; }
inline unsigned BandMatSize(unsigned n, unsigned b) { return n*b; }
inline unsigned SymBandMatSize(unsigned n, unsigned hb) { return n*(hb+1); }
inline unsigned HermBandMatSize(unsigned n, unsigned hb) { return n*(hb+1); }
inline unsigned SymMatSize(unsigned n)		    { return n*(n+1)/2; }
inline unsigned SkewMatSize(unsigned n)		    { return n*(n+1)/2; }
inline unsigned HermMatSize(unsigned n)		    { return n*(n+1)/2; }
inline unsigned UpperTriMatSize(unsigned n)  	    { return n*(n+1)/2; }
inline unsigned LowerTriMatSize(unsigned n)  	    { return n*(n+1)/2; }
inline unsigned TriDiagMatSize(unsigned n)	    { return 3*n; }

DoubleSymBandMat::~DoubleSymBandMat()
{
}

DoubleSymBandMat::DoubleSymBandMat()
  : vec()
{
    n=0;
    bandu=0;
}

DoubleSymBandMat::DoubleSymBandMat( const DoubleSymBandMat& A )
  : vec(A.vec)
{
    n=A.n;
    bandu=A.bandu;
}

DoubleSymBandMat::DoubleSymBandMat(unsigned M, unsigned N, unsigned halfWidth)
  : vec(SymBandMatSize(N,halfWidth),rwUninitialized)
{
  if (M!=N) { RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTSQUARE,int(M),int(N)))); }
  n=N;
  bandu=halfWidth;
  zeroUnusedEntries();
}

DoubleSymBandMat::DoubleSymBandMat(const DoubleVec& data, unsigned M, unsigned N, unsigned halfWidth )
  : vec(data)
{
  if (M!=N) { RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTSQUARE,int(M),int(N)))); }
  n=N;
  bandu=halfWidth;
  if (vec.length()!=SymBandMatSize(N,bandu))
    RWTHROW(RWInternalErr(RWMessage(RWLAPK_NUMBERPOINTSBAND,vec.length(),n,n,className,bandwidth())));
}

/*
 * Set entries in the data vector which are not used to zero.  This
 * avoids purify warnings and also ensures we won't have troubles
 * adding and subtracting these matrices
 */
void DoubleSymBandMat::zeroUnusedEntries()
{
    int i=halfBandwidth()-1;       // Start i at min(n-1,halfBandwidth()-1)
    if (i>n-1) i=n-1;
    for( ; i>=0; i-- ) {
      vec.slice( i*(halfBandwidth()+1), halfBandwidth()-i ) = (Double)(0);
    }
}

Double DoubleSymBandMat::bcval(int i, int j) const
{
    if ( i<0 || i>=rows() || j<0 || j>=cols() ) {
      RWTHROW(RWBoundsErr(RWMessage(RWLAPK_OUTOFBOUNDS,i,j,rows(),cols())));
    }
    return ( (i<=j) ? ( ((j-i)>bandu) ? (Double)(0) : vec(i+bandu+j*bandu) )
		    : ( ((i-j)>bandu) ? (Double)(0) : vec(j+bandu+i*bandu) ) );
}

Double DoubleSymBandMat::bcset(int i, int j, Double x)
{
    if ( i<0 || i>=rows() || j<0 || j>=cols() ) {
      RWTHROW(RWBoundsErr(RWMessage(RWLAPK_OUTOFBOUNDS,i,j,rows(),cols())));
    }
    if ( (j>=i) ? ((j-i)>bandu) : ((i-j)>bandu) )
                            RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTSETABLE,i,j,className))); 
    if (i<=j) { vec(i+bandu+j*bandu)=x; return x; }
    else      { vec(j+bandu+i*bandu)=x; return x; }
    // return ((i<=j) ? (vec(i+bandu+j*bandu)=x) : (vec(j+bandu+i*bandu)=x));
    // The above kills the cc optimizer on the DEC 5400 (complex case)
}

RODoubleRef DoubleSymBandMat::bcref(int i, int j)
{
    if ( i<0 || i>=rows() || j<0 || j>=cols() ) {
      RWTHROW(RWBoundsErr(RWMessage(RWLAPK_OUTOFBOUNDS,i,j,rows(),cols())));
    }
    return (i<=j) ? ( ((j-i)>bandu) ? RODoubleRef(rwDoubleZero,TRUE)
				      : RODoubleRef(vec(i+bandu+j*bandu),FALSE))
	   	  : ( ((i-j)>bandu) ? RODoubleRef(rwDoubleZero,TRUE)
				      : RODoubleRef(vec(j+bandu+i*bandu),FALSE));
}

DoubleVec DoubleSymBandMat::bcdiagonal(int i) const
{
  if ( i>=int(cols()) || (-i)>=int(rows()) ||
		i>int(upperBandwidth()) || (-i)>int(lowerBandwidth())) {
    RWTHROW(RWBoundsErr(RWMessage(RWLAPK_DIAGOUTOFBOUNDS,-i,className,rows(),cols())));
  }
    int iabs = (i>=0) ? i : -i;
    return vec.slice( (iabs+1)*(bandu), n-iabs, bandu+1 );
}

/*
 * Here are non-inline versions of the row(), col(), and diagonal()
 * routines.  Normally these would be inlined, but due to a bug
 * in the Borland C++ compiler it is best not to inline them with
 * Borland C++.
 *
 * These routines just call the bounds checking routines.
 */

/*
 * The leadingSubmatrix function
 */

DoubleSymBandMat DoubleSymBandMat::leadingSubmatrix(int k)
{
  if (k<0 || k>n) RWTHROW(RWBoundsErr(RWMessage(RWLAPK_LEADINGSUBMATRIXORDER,k,n)));
  return DoubleSymBandMat( vec.slice(0,SymBandMatSize(k,halfBandwidth())), k, k, halfBandwidth() );
}

DoubleSymBandMat& DoubleSymBandMat::operator=(const DoubleSymBandMat& M)
{
    if (rows()!=M.rows() || cols()!=M.cols()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATSIZE,M.rows(),M.cols(),rows(),cols())));
    }
    if (lowerBandwidth()<M.lowerBandwidth() ||
	upperBandwidth()<M.upperBandwidth()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_BANDSIZE,M.lowerBandwidth(),lowerBandwidth(),
				upperBandwidth(), M.upperBandwidth() )));
    }
    if (bandwidth()==M.bandwidth()) {
      vec = M.vec;
    } else {
      int u = upperBandwidth();
      int Mu = M.upperBandwidth();
      for( int i= 0; i<=u; i++ ) {
	DoubleVec d = vec.slice((i+1)*bandu,n-i,bandu+1);
	if ( i>Mu ) d=(Double)(0);
	else d=M.vec.slice((i+1)*M.bandu,n-i,M.bandu+1);
      }
    }
    return *this;
}

DoubleSymBandMat& DoubleSymBandMat::reference(DoubleSymBandMat& A)
{
    vec.reference(A.vec);
    n=A.n;
    bandu=A.bandu;
    return *this;
}

DoubleSymBandMat DoubleSymBandMat::copy() const
{
  DoubleSymBandMat A( *((DoubleSymBandMat*)this) );	// cast this to non-const to make cfront happy
  A.deepenShallowCopy();
  return A;
}

void DoubleSymBandMat::resize(unsigned M, unsigned N)
{
    if (M!=N) { RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTSQUARE,int(M),int(N)))); }
    n=N;
    vec.resize(SymBandMatSize(n,upperBandwidth()));

  // Finally, zero any unused entries just to keep things tidy
  zeroUnusedEntries();
}

void DoubleSymBandMat::resize(unsigned M, unsigned N, unsigned halfWidth)
{
  resize(M,N);	// Set the dimension - all that's left is to change bandwidth
  int h = halfBandwidth();
  if (h!=halfWidth) {
    DoubleSymBandMat newMat(M,N,halfWidth);
    for( int i=0; i<=halfWidth; i++ ) {
      DoubleVec d = newMat.vec.slice((i+1)*newMat.bandu,newMat.n-i,newMat.bandu+1);
      if ( i<(-h) || i>h ) d = (Double)(0);
      else                 d = vec.slice((i+1)*bandu,n-i,bandu+1);
    }
    this->reference(newMat);
  }
}


/*
 * printOn,scanFrom:  printOn prints all non-numeric stuff first
 * then prints the numbers defining the shape and size of the matrix,
 * then prints the matrix itself.  The format used by printOn can be
 * used to read in a matrix with scanFrom.
 *
 * scanFrom first eats all the non-numeric characters it encounters.
 * This takes care of the stuff printOn spews before the data.  Next
 * it reads the numbers which define the matrix shape and then the
 * data is read in.
 */

void DoubleSymBandMat::printOn(ostream& outs) const
{
    int w = outs.width(0);
    int m=n;
      outs << className << ", ";
    outs << "half bandwidth: " << halfBandwidth() << " ";
    outs << rows() << "x" << cols() << " [\n";
    for( int i=0; i<m; i++ ) {
      for( int j=0; j<n; j++ ) {
	outs << setw(w) << val(i,j) << " ";
      }
      outs << "\n";
    }
    outs << "]";
}

void DoubleSymBandMat::scanFrom(istream& s)
{
  char c;
  unsigned numRows, numCols;
  unsigned hwidth;

  /* Skip through leading non-digits */
  do { s.get(c); } while (!s.fail() && !isdigit(c));
  s.putback(c);

  s >> hwidth;

  s >> numRows;
  do { s.get(c); } while (isspace(c));	// Skip optional x character
  if (isdigit(c)) s.putback(c);	
  s >> numCols;

  DoubleVec v;			// Read the data
  v.scanFrom(s);

  if (!s.fail()) { 		// Now put the data into the matrix
    if (v.length()!=numRows*numCols) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_WRONGNUMPOINTS,v.length(),className,numRows,numCols)));
    } else {
      resize(numRows,numCols,hwidth);
      int index = 0;
      for(int i=0; i<numRows; i++) {
        for(int j=0; j<numCols; j++) {
	  if (val(i,j)!=v[index]) bcset(i,j,v[index]);
	  index++;
	}
      }
    }
  }
}

void DoubleSymBandMat::saveOn(RWFile& file) const
{
    /* First write the numbers which define the shape of the matrix */
    file.Write(n);
    file.Write(bandu);
    vec.saveOn(file);
}

void DoubleSymBandMat::saveOn(RWvostream& s) const
{
    /* First write the numbers which define the shape of the matrix */
    s << n;
    s << bandu;
    vec.saveOn(s);
}

void DoubleSymBandMat::restoreFrom(RWFile& file)
{
    /* First read the numbers which define the shape of the matrix */
    file.Read(n);
    file.Read(bandu);
    int size = SymBandMatSize(n,bandu);
    vec.restoreFrom(file);
    if (vec.length() != size) RWTHROW(RWInternalErr(RWMessage(RWLAPK_RESTORESIZE,className,vec.length(),size)));
}

void DoubleSymBandMat::restoreFrom(RWvistream& s)
{
    /* First read the numbers which define the shape of the matrix */
    s >> n;
    s >> bandu;
    int size = SymBandMatSize(n,bandu);
    vec.restoreFrom(s);
    if (vec.length() != size) RWTHROW(RWInternalErr(RWMessage(RWLAPK_RESTORESIZE,className,vec.length(),size)));
}

unsigned DoubleSymBandMat::binaryStoreSize() const
{
    /* First determine the size of the stuff
       which determines the matrix shape.    */
    unsigned size = 3*sizeof(unsigned);  // n and bandu
    return size + vec.binaryStoreSize();
}

> #################################################################
> # The master template file for symmetric eigen-decompositions   #
> #                                                               #
> # $Header: /users/rcs/lapksrc/xsymeig.h,v 1.2 1993/07/05 19:19:33 alv Exp $
> #                                                               #
> # Copyright (1993) by Rogue Wave Software, Inc.  This           #
> # software is subject to copyright protection under the laws    #
> # of the United States and other countries.                     #
> #                                                               #
> # Written by Al Vermeulen.                                      #
> #                                                               #
> # Limited License.                                              #
> #################################################################
> #
> # $Log: xsymeig.h,v $
> # Revision 1.2  1993/07/05  19:19:33  alv
> # fixed up for complex numbers
> #
> # Revision 1.1  1993/04/06  15:44:43  alv
> # Initial revision
> #
>
> # Macros that we need
> 
> define <T>=RW_T_RW
> 
> if <T>==Double
> define <P>=Double <tLetter>=d <Sym>=Sym <sym>=sym
> elif <T>==Float
> define <P>=Float <tLetter>=f <Sym>=Sym <sym>=sym
> elif <T>==DComplex
> define <P>=Double <tLetter>=c <Sym>=Herm <sym>=herm
> else
> err Symmetric Eigen-decomposition: not defined for '<T>'
> endif
> beginwrapper <tLetter><sym>eig

/*
 * <T><Sym>EigDecomp - Spectral factorization of a symmetric/Hermitian matrix
 *
 * Generated from template $Id: xsymeig.h,v 1.2 1993/07/05 19:19:33 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * This class represents a spectral factorization of a symmetric/Hermitian
 * matrix.  In addition, it has constructors for constructing a
 * factorization from a symmetric/Hermitian or banded symmetric/Hermitian,
 * with an option of whether or not to compute eigenvectors.  The
 * actual computation is done by a subclass of <T><Sym>EigServer.
 * If you want to control which server gets used, or need more
 * control over the computation, then you need to use an eigenserver
 * class directly.
 *
 */ 

#include "rw/lapkdefs.h"
#include "rw/<tLetter>genmat.h"
#include "rw/<tLetter>vec.h"
class <P>SymEigDecomp;   // declaration here for complex types

class <T><Sym>EigDecomp {
private:
  unsigned  n;           // dimension of the matrix
  <P>Vec    lambda;      // computed eigenvalues (length may be < n)
  <T>GenMat P;           // columns contain eigenvectors
  RWBoolean computedAll; // Did I compute everything the server tried to compute?
  RWBoolean accurate;    // Were all results as accurate as possible?

public:
  <T><Sym>EigDecomp();
  <T><Sym>EigDecomp(const <P>SymEigDecomp&);
  <T><Sym>EigDecomp(const <T><Sym>Mat&, RWBoolean computeVecs=TRUE);
  <T><Sym>EigDecomp(const <T><Sym>BandMat&, RWBoolean computeVecs=TRUE);
  void factor(const <T><Sym>Mat&, RWBoolean computeVecs=TRUE);
  void factor(const <T><Sym>BandMat&, RWBoolean computeVecs=TRUE);

  void      operator=(const <T><Sym>EigDecomp&);

  unsigned        cols()           const {return n;}
  <P>             eigenValue(int)  const;
  const <P>Vec    eigenValues()    const {return lambda;}
  const <T>Vec    eigenVector(int) const;
  const <T>GenMat eigenVectors()   const {return P;}
  RWBoolean       good()           const; // True if everything went perfectly
  RWBoolean       inaccurate()     const; // Either failure, or some things are not fully accurate
  RWBoolean       fail()           const; // An eigenvalue or vector wasn't computed
  unsigned        numEigenValues() const {return lambda.length();}
  unsigned        numEigenVectors()const {return P.cols();}
  unsigned        rows()           const {return n;}
               
  friend class <T><Sym>EigServer;
> if <T>==Double
  friend class DComplexHermEigServer;
  friend class DComplexHermEigDecomp;
> endif
};

> endwrapper


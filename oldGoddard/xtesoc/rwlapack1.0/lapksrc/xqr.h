> #################################################################
> # The template file for the QR decompositions                   #
> #                                                               #
> # $Header: /users/rcs/lapksrc/xqr.h,v 1.1 1993/05/18 17:00:02 alv Exp $
> #                                                               #
> # Copyright (1993) by Rogue Wave Software, Inc.  This software  #
> # is subject to copyright protection under the laws of the      #
> # United States and other countries.                            #
> #                                                               #
> # Written by Al Vermeulen.                                      #
> #                                                               #
> # Limited License.                                              #
> #################################################################
> #
> # $Log: xqr.h,v $
> # Revision 1.1  1993/05/18  17:00:02  alv
> # Initial revision
> #
>
> define <T>=RW_T_RW
> if <T>==Double
> define <P>=Double <tLetter>=d
> elif <T>==Float
> define <P>=Float <tLetter>=f
> elif <T>==DComplex
> define <P>=Double <tLetter>=c
> else
> err orthogonal decomposition: not defined for '<T>'
> endif
> 
> # The traditional top of the file stuff
>
> beginwrapper <tLetter>qr
>
/*
 * <T>QRDecomp:       representation of a QR decomposition
 * <T>QRDecompServer: produce QR decompositions, using possibly non-default options
 *
 * Generated from template $Id: xqr.h,v 1.1 1993/05/18 17:00:02 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 * A QR decomposition decomposes a matrix A like this:
 *
 *                    [ R ]
 *            A P = Q [   ]
 *                    [ 0 ]
 *
 * where P is a permutation matrix, Q is orthogonal, and R is upper
 * triangular if A is taller than wide, and upper trapazoidal is  A
 * is wider than tall.
 */

#include "rw/<tLetter>genmat.h"
#include "rw/<tLetter>vec.h"
#include "rw/ivec.h"

class <T>QRDecomp {
private:
  long      *pivots_;    // nil if no pivoting was done
  <T>Vec    tau_;        // scalars needed to recover Q
  <T>GenMat QR_;         // upper triangular part is R, rest is used to compute Q

public:
  <T>QRDecomp();                      // Constructors
  <T>QRDecomp(const <T>QRDecomp&);
  <T>QRDecomp(const <T>GenMat&);
  void factor(const <T>GenMat&);
  ~<T>QRDecomp();
  void operator=(const <T>QRDecomp&);

  unsigned       rows() const         {return QR_.rows();}
  unsigned       cols() const         {return QR_.cols();}
  <T>GenMat      P() const;           // Extract components of the decomposition
  <T>GenMat      R() const;
  <T>Vec         Rdiagonal() const;
  <T>GenMat      Q() const;

  <T>Vec         Px(const <T>Vec& x) const;    // Compute using the decomposition
  <T>Vec         PTx(const <T>Vec& x) const;
  <T>Vec         Rx(const <T>Vec& x) const;
  <T>Vec         RTx(const <T>Vec& x) const;
  <T>Vec         Rinvx(const <T>Vec& x) const;
  <T>Vec         RTinvx(const <T>Vec& x) const;
  <T>Vec         Qx(const <T>Vec& x) const;
  <T>Vec         QTx(const <T>Vec& x) const;

  friend class <T>QRDecompServer;
  friend class <T>CODecomp;
};

class <T>QRDecompServer {
private:
  RWBoolean pivot_;       // Do we pivot?
  IntVec    initial_;     // Indices of columns to be moved to initial position

public:
  <T>QRDecompServer();
  void setPivoting(RWBoolean);   // turn pivoting option on or off
  void setInitialIndex(int i);   // i moves to the begin of the decomposition
  void setFreeIndex(int i);      // i is removed from initial and final lists

  <T>QRDecomp operator()(const <T>GenMat&) const;   // build a QR decomposition
};
>
> endwrapper

/*
 * Implementation of FloatGenFact
 *
 * Generated from template $Id: xfct.cpp,v 1.6 1993/10/11 21:48:11 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/lapkdefs.h"
#include "rw/fgenfct.h"
#include "rw/lapack.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"  
#include "rw/fgenmat.h"

const char* FloatGenFact::className = "FloatGenFact";

FloatGenFact::~FloatGenFact()
{
  delete [] pvts;
}

FloatGenFact::FloatGenFact()
{
  info = -1;
  pvts=0;
}

FloatGenFact::FloatGenFact( const FloatGenFact& A )
  : factorization(A.factorization)
{
  info = A.info;
  Anorm = A.Anorm;
  unsigned n = A.rows();
  pvts = (n>0) ? new long [n] : 0;
  while(n--) {
    pvts[n] = A.pvts[n];
  }
}

FloatGenFact::FloatGenFact(const FloatGenMat& A, RWBoolean estimateCondition)
  : factorization(A)  // Use reference semantics, followed by
                      // deepenShallowCopy(), to work around a BCC V3.0 bug
{
    factorization.deepenShallowCopy();
    pvts = 0;
    dofactor(estimateCondition);
}

void FloatGenFact::factor( const FloatGenMat& A, RWBoolean estimateCondition )
{
    FloatGenMat Acopy = A.copy();
    factorization.reference(Acopy);
    dofactor(estimateCondition);
}

void FloatGenFact::dofactor(RWBoolean estimateCondition)
{
   info=0;   // for size zero matrices, need to set this explicitly
   long n = rows();	

    // Error (sanity) checking
    RWPRECONDITION(n==factorization.cols());
    RWPRECONDITION(factorization.rowStride()==1 && factorization.colStride()==factorization.rows());

    delete [] pvts;
    pvts = (n>0) ? new long [n] : 0;

    // If we care to ever find the condition number, we need to know
    // the 1-norm of the matrix A.  Compute it while the member data
    // factorization is still a copy of A, and not yet it's factorization.
    Anorm = -1; 
    if (n>0 && estimateCondition) {
      Anorm = slange('1',n,n,factorization.data(),n,0);
    }

    if (n>0) {
#if defined(RW_USE_UNBLOCKED)
      sgetf2(n,n,factorization.data(),n,pvts,info);
#else
      sgetrf(n,n,factorization.data(),n,pvts,info);
#endif
      RWPOSTCONDITION(info>=0);
    }
}

RWBoolean FloatGenFact::fail() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}


RWBoolean FloatGenFact::isSingular() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}

Float FloatGenFact::condition() const
{
    Float Anormcopy = Anorm;      // Copy to stop warnings about ref parm
    long info;
    long n = rows();
    Float rcond;
    Float *work = new Float[4*n];   // Some routines only require 2*n, but what the hell
    long *work2 = new long [n];
    sgecon('1',n,(Float*)factorization.data(),n,Anormcopy,rcond,work,work2,info);
    delete [] work2;
    delete [] work;
    RWPOSTCONDITION(info==0);
    return rcond;
}

FloatVec FloatGenFact::solve( const FloatVec& b ) const
{
  FloatGenMat B(b,b.length(),1);
  FloatGenMat X = solve(B);
  RWPOSTCONDITION(X.cols()==1);
  return X.col(0);
}

FloatGenMat FloatGenFact::solve( const FloatGenMat& B ) const
{
    FloatGenMat X(B.copy());
    long n = rows();
    if (B.rows()!=n) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,B.rows(),n)));
    if (fail()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTSOLVE)));
    long nrhs = X.cols();
    long info;
    if (n>0) {
    sgetrs('N',n,nrhs,(Float*)factorization.data(),n,pvts,X.data(),n,info);
      RWPOSTCONDITION(info==0);
    }
    return X;    
}

Float FloatGenFact::determinant() const
{
    int numExchanges = 0;
    for(int i=rows(); i--;) {
      if (pvts[i]!=i+1) numExchanges++;
    }
    Float detLU = prod(factorization.diagonal());
    return (numExchanges%2) ? -detLU : detLU;
}
              
FloatGenMat FloatGenFact::inverse( ) const
{
    if (rows()==0) { return factorization; }
    if (fail()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTSOLVE)));
    FloatGenMat soln(factorization);
    soln.deepenShallowCopy();
    long n = rows();
    long info;
    long nb = ilaenv( 1, "sgetri", " ", n, -1, -1, -1 );
    long lwork = n*nb;
    Float *work = new Float [lwork];
    sgetri(n,soln.data(),n,pvts,work,lwork,info);
    RWPOSTCONDITION(work[0]==lwork);
    delete [] work;
    RWPOSTCONDITION(info==0);
    return soln;
}


/*
 * Global functions which provide alternate ways to call member functions.
 * These used to be inline, but this requires that you include more header
 * files on some compilers, and that is just not worth it.
 */
FloatVec solve(const FloatGenFact& A, const FloatVec& b) { return A.solve(b); }
FloatGenMat solve(const FloatGenFact& A, const FloatGenMat& b) { return A.solve(b); }
Float    determinant (const FloatGenFact& A)	      { return A.determinant(); }

FloatGenMat inverse(const FloatGenFact& A)      	      { return A.inverse(); }

Float condition(const FloatGenFact& A) { return A.condition(); }



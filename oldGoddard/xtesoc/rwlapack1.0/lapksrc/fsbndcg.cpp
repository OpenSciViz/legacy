/*
 * The conversion functions to/from general matrices.
 *
 * Generated from template $Id: xcg.cpp,v 1.2 1993/07/05 19:19:33 alv Exp $
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/fsbndmat.h"
#include "rw/fgenmat.h"

FloatGenMat::FloatGenMat( const FloatSymBandMat& A )
  : RWMatView(A.rows(),A.cols(),sizeof(Float))
{
  zero();
  for( int k=A.halfBandwidth(); k>=0; k-- ) {
    FloatVec d = A.diagonal(k);
    diagonal(-k) = d;
    diagonal(k) = d;
  }
}

FloatSymBandMat toSymBandMat( const FloatGenMat& S, unsigned bandu )
{
  int n = S.cols();
  FloatSymBandMat A( S.rows(), n, bandu );
  A.diagonal(0) = S.diagonal(0);
  for( int k=1; k<=bandu; k++ ) {
    FloatVec Adiag = A.diagonal(k);
    Adiag = S.diagonal(k);
    Adiag += S.diagonal(-k);
    Adiag /= 2;
  }
  return A;
}


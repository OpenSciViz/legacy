#if 0
/*
 * Generated from template $Id: xmth.cpp,v 1.2 1993/10/11 21:48:17 alv Exp $
 *
 * Math member functions for the matrix types
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */
#endif

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/futrimat.h"
#include "rw/fltrimat.h"

/*
 * This function returns a new matrix the same shape as the one 
 * passed in but with the data passed in.  This way we restrict
 * all the >if statements to a couple lines
 */
inline FloatUpperTriMat sameShapeMatrix( FloatVec& vec, const FloatUpperTriMat& A )
{
  return FloatUpperTriMat(vec,A.rows(),A.cols());
}


static void verifyMatch(const FloatUpperTriMat& A, const FloatUpperTriMat& B)
{
    if (A.rows()!=B.rows() || A.cols()!=B.cols())
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATSIZE,A.rows(),A.cols(),B.rows(),B.cols())));
}

RWBoolean FloatUpperTriMat::operator==(const FloatUpperTriMat& X)
{
    if (n!=X.n) return FALSE;
    return vec==X.vec;	// All elements in vec used in the matrix
}


FloatUpperTriMat& FloatUpperTriMat::operator+=(const FloatUpperTriMat& m)
{
  verifyMatch(*this,m);
  vec += m.vec;
  return *this;
}

FloatUpperTriMat& FloatUpperTriMat::operator-=(const FloatUpperTriMat& m)
{
  verifyMatch(*this,m);
  vec -= m.vec;
  return *this;
}

FloatUpperTriMat& FloatUpperTriMat::operator*=(const FloatUpperTriMat& m)
{
  verifyMatch(*this,m);
  vec *= m.vec;
  return *this;
}

FloatUpperTriMat& FloatUpperTriMat::operator*=(Float x)
{
  vec *= x;
  return *this;
}

FloatUpperTriMat& FloatUpperTriMat::operator/=(const FloatUpperTriMat& m)
{
  verifyMatch(*this,m);
  vec /= m.vec;
  return *this;
}

FloatUpperTriMat& FloatUpperTriMat::operator/=(Float x)
{
  vec /= x;
  return *this;
}




FloatUpperTriMat operator-(const FloatUpperTriMat& A)
{
  FloatVec temp(-(A.dataVec()));
  return sameShapeMatrix(temp,A);
}
  
FloatUpperTriMat operator+(const FloatUpperTriMat& A)
{
  return A;
}

FloatUpperTriMat operator*(const FloatUpperTriMat& A, const FloatUpperTriMat& B) {
	verifyMatch(A,B);
	FloatVec temp(A.dataVec()*B.dataVec());
	return sameShapeMatrix(temp,A);
}

FloatUpperTriMat operator/(const FloatUpperTriMat& A, const FloatUpperTriMat& B) {
	verifyMatch(A,B);
	FloatVec temp(A.dataVec()/B.dataVec());
	return sameShapeMatrix(temp,A);
}

FloatUpperTriMat operator+(const FloatUpperTriMat& A, const FloatUpperTriMat& B) {
	verifyMatch(A,B);
	FloatVec temp(A.dataVec()+B.dataVec());
	return sameShapeMatrix(temp,A);
}

FloatUpperTriMat operator-(const FloatUpperTriMat& A, const FloatUpperTriMat& B) {
	verifyMatch(A,B);
	FloatVec temp(A.dataVec()-B.dataVec());
	return sameShapeMatrix(temp,A);
}

FloatUpperTriMat operator*(const FloatUpperTriMat& A, Float x) {
	FloatVec temp(A.dataVec()*x);
	return sameShapeMatrix(temp,A);
}

/*
 * Out-of-line version for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
  FloatUpperTriMat operator*(Float x, const FloatUpperTriMat& A) { return A*x; }
#endif

FloatUpperTriMat operator/(const FloatUpperTriMat& A, Float x) {
	FloatVec temp(A.dataVec()/x);
	return sameShapeMatrix(temp,A);
}





FloatUpperTriMat abs(const FloatUpperTriMat& A) {
	FloatVec temp(abs(A.dataVec()));
	return sameShapeMatrix(temp,A);
}



FloatLowerTriMat transpose(const FloatUpperTriMat& A)
{
return FloatLowerTriMat(A.dataVec(),A.rows(),A.cols());
}

FloatVec product(const FloatUpperTriMat& A, const FloatVec& x)
{
    if (A.cols()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATVECPROD,A.className,A.rows(),A.cols(),x.length())));
    }
    FloatVec y(A.rows(),rwUninitialized);
    const FloatVec& v = A.dataVec();
    int n=A.rows();
    for( int i=0; i<n; i++ ) {
      Float* index = (Float*)v.data() + ((i+1)*(i+2)/2-1);
      y(i) = (*index)*x(i);
      index += i+1;
      for( int k=i+1; k<n; index+=(++k) ) {
      y(i) += (*index)*x(k);
      }
    }
    return y;
}

FloatVec product(const FloatVec& x, const FloatUpperTriMat& A)
{
    if (A.rows()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECMATPROD,x.length(),A.className,A.rows(),A.cols())));
    }
    return product(transpose(A),x);
}



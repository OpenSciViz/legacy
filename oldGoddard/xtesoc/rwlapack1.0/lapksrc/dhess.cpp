/*
 * DoubleHessenbergDecomp - Hessenberg decomposition of a matrix
 *
 * Generated from template $Id: xhess.cpp,v 1.3 1993/07/08 23:26:13 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 * This decomposition is usually formed as a percusor to computing
 * the eigenvalues and/or Schur decomposition of a non-symmetric matrix.
 *
 * This class includes an optional balancing transformation in addition
 * to the standard Hessenberg decomposition.
 */ 

#include "rw/dhess.h"

DoubleHessenbergDecomp::DoubleHessenbergDecomp()
{
}

DoubleHessenbergDecomp::DoubleHessenbergDecomp(const DoubleGenMat& A, RWBoolean permute, RWBoolean scale)
{
  factor(A,permute,scale);
}

DoubleHessenbergDecomp::DoubleHessenbergDecomp(const DoubleBalanceDecomp& A)
{
  factor(A);
}

void DoubleHessenbergDecomp::factor(const DoubleGenMat& A, RWBoolean permute, RWBoolean scale)
{
  DoubleBalanceDecomp BCB(A,permute,scale);
  factor(BCB);
}
  
void DoubleHessenbergDecomp::factor(const DoubleBalanceDecomp& decomp)
{
  B_ = decomp.B();
  H_.reference(decomp.C());
  H_.deepenShallowCopy();
  tau_.reshape(decomp.rows());
  long info;
  long n = decomp.rows();
  long blockSize = ilaenv( 1, "DGEHRD", " ", n, B_.lowIndex(), B_.highIndex(), -1 );
  long lwork = n*blockSize;
  Double *work = new Double [lwork];
  dgehrd(n,B_.lowIndex(),B_.highIndex(),H_.data(),n,tau_.data(),work,lwork,info);
  RWPOSTCONDITION(info==0);
  RWPOSTCONDITION(lwork>=work[0]);
  delete work;
}

DoubleGenMat   DoubleHessenbergDecomp::B()                     const
{
  DoubleGenMat I(rows(),cols(),(Double)0);
  I.diagonal() = (Double)1;
  return B_.transform(I);
}

DoubleGenMat   DoubleHessenbergDecomp::Q()                     const
{
  DoubleGenMat theQ = H_.copy();
  long n = rows();
  long info;
  long blockSize = ilaenv( 1, "DORGHR", " ", n, B_.lowIndex(), B_.highIndex(), -1 );
  long lwork = n*blockSize;
  Double *work = new Double [lwork];
  dorghr(n,B_.lowIndex(),B_.highIndex(),theQ.data(),rows(),(Double*)tau_.data(),work,lwork,info);
  RWPOSTCONDITION(info==0);
  RWPOSTCONDITION(lwork>=work[0]);
  delete work;
  return theQ;
}

DoubleGenMat   DoubleHessenbergDecomp::H()                     const
{
  DoubleGenMat A = H_.copy();
  for(int i=2; i<rows(); i++) {
    A.diagonal(-i) = (Double)0;
  }
  return A;
}

DoubleVec      DoubleHessenbergDecomp::Bx(const DoubleVec& x)       const
{
  return B_.transform(x);
}

DoubleVec      DoubleHessenbergDecomp::BInvTx(const DoubleVec& x)   const
{
  return B_.invTransform(x);
}

DoubleVec      DoubleHessenbergDecomp::Qx(const DoubleVec& x)      const
{
  DoubleGenMat X(x,x.length(),1);
  DoubleGenMat theQX = QX(X);
  RWPOSTCONDITION(theQX.cols()==1);
  return theQX.col(0);
}

DoubleVec      DoubleHessenbergDecomp::QTx(const DoubleVec& x)      const
{
  DoubleGenMat X(x,x.length(),1);
  DoubleGenMat theQTX = QTX(X);
  RWPOSTCONDITION(theQTX.cols()==1);
  return theQTX.col(0);
}

DoubleGenMat   DoubleHessenbergDecomp::BX(const DoubleGenMat& x)    const
{
  return B_.transform(x);
}

DoubleGenMat   DoubleHessenbergDecomp::BInvTX(const DoubleGenMat& x)const
{
  return B_.invTransform(x);
}

DoubleGenMat   DoubleHessenbergDecomp::QX(const DoubleGenMat& X)   const
{
  DoubleGenMat C = X.copy();
  long n = rows();
  long info;
  long blockSize = ilaenv( 1, "DORMHR", " ", n, B_.lowIndex(), B_.highIndex(), -1 );
  long lwork = n*blockSize;
  Double *work = new Double [lwork];
  dormhr('L','N',C.rows(),C.cols(),B_.lowIndex(),B_.highIndex(),(Double*)H_.data(),
            H_.rows(),(Double*)tau_.data(),C.data(),C.rows(),work,lwork,info);
  RWPOSTCONDITION(info==0);
  RWPOSTCONDITION(lwork>=work[0]);
  delete work;
  return C;
}

DoubleGenMat   DoubleHessenbergDecomp::QTX(const DoubleGenMat& X)   const
{
  DoubleGenMat C = X.copy();
  long n = rows();
  long info;
  long blockSize = ilaenv( 1, "DORMHR", " ", n, B_.lowIndex(), B_.highIndex(), -1 );
  long lwork = rows()*blockSize;
  Double *work = new Double [lwork];
  char trans = 'T';
  dormhr('L',trans,C.rows(),C.cols(),B_.lowIndex(),B_.highIndex(),(Double*)H_.data(),H_.rows(),
                                (Double*)tau_.data(),C.data(),C.rows(),work,lwork,info);
  RWPOSTCONDITION(info==0);
  RWPOSTCONDITION(lwork>=work[0]);
  delete work;
  return C;
}

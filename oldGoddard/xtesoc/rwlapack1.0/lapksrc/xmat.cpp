> ###############################################################
> # The template for the main code file for the matrix types    #
> #                                                             #
> # $Header: /users/rcs/lapksrc/xmat.cpp,v 1.4 1993/10/11 21:48:16 alv Exp $
> #                                                             #
> # This file includes the core member functions considered     #
> # for each of the matrix types.  These functions are the ones #
> # considered most used, and also the ones that least reference#
> # symbols outside of this file.  Therefore, for example, the  #
> # conversion constructors are in a seperate file.             #
> #                                                             #
> # Copyright (1991) by Rogue Wave Software, Inc.  This software#
> # is subject to copyright protection under the laws of the    #
> # United States and other countries.                          #
> #                                                             #
> # Written by Al Vermeulen.                                    #
> #                                                             #
> # Limited License.                                            #
> ###############################################################
> #
> # $Log: xmat.cpp,v $
> # Revision 1.4  1993/10/11  21:48:16  alv
> # synch'ed with Math.h++ v5.1
> #
> # Revision 1.3  1993/07/08  23:26:13  alv
> # ported to SUN CC v3
> #
> # Revision 1.2  1993/07/05  19:19:33  alv
> # fixed up for complex numbers
> #
> # Revision 1.1  1993/06/23  18:28:08  alv
> # Initial revision
> #
> # Revision 1.2  1993/01/26  21:05:44  alv
> # removed DOS EOF
> #
> # Revision 1.1  1993/01/23  00:01:12  alv
> # Initial revision
> #
> # 
> #    Rev 1.3   15 Nov 1991 09:37:56   keffer
> # Removed RWMATXERR macro --- Static constructors now startup error facility.
> # 
> #    Rev 1.2   17 Oct 1991 09:22:38   keffer
> # Changed include path to <rw/xxx.h>
> # 
> #    Rev 1.1   24 Sep 1991 13:35:08   keffer
> # <iomanip.hpp> for Zortech; <iomanip.hxx> for Glock
> # 
>
>
> include macros
>
#if 0
/*
 * Generated from template file $Id: xmat.cpp,v 1.4 1993/10/11 21:48:16 alv Exp $
 *
 * The main code file for <T><M>
 *
> copyright
 */
#endif

#include "rw/<tLetter><shapeAbbrev>mat.h"

#include "rw/rstream.h"
#include "rw/rwfile.h"
#include "rw/vstream.h"
#include "rw/lapkerr.h"
#include "rw/rwerr.h"
#  ifdef __ZTC__
#    include <iomanip.hpp>
#  else
#    ifdef __GLOCK__
#      include <iomanip.hxx>
#    else
#      include <iomanip.h>
#    endif
#  endif
STARTWRAP
#include <stdio.h>
#include <ctype.h>
ENDWRAP

const char* <T><M>::className = "<T><M>";

> # sizes.ctm defines a bunch of inline functions named
> # <M>Size(...) which tells how many elements each shape
> # of matrix needs to store.
>
> include sizes

>
> # The destructor is pretty simple
>
<T><M>::~<T><M>()
{
}

> # The common constructors.  Other constructors are in pick.ctm,
> # convtype.ctm and convshp.ctm
>
<T><M>::<T><M>()
  : vec()
{
    n=0;
> if <M>==GenMat
    m=0;
> endif
>
> if <M>==BandMat
    band=1;
    bandu=0;
> endif
>
> if <M>==SymBandMat || <M>==HermBandMat
    bandu=0;
> endif
}

<T><M>::<T><M>( const <T><M>& A )
  : vec(A.vec)
{
    n=A.n;
> if <M>==GenMat
    m=A.m;
> endif
>
> if <M>==BandMat
    band=A.band;
    bandu=A.bandu;
> endif
>
> if <M>==SymBandMat || <M>==HermBandMat
    bandu=A.bandu;
> endif
}

> # The constructors which explicitly set the shape of the matrix
> # go here.
> # GenMat, BandMat, SymBandMat and HermBandMat get special treatment, they
> # are followed by routines which suffice for the rest.
>
> if <M>==GenMat
<T><M>::<T><M>(unsigned M, unsigned N) : vec(M*N,rwUninitialized)
{
  m=M;
  n=N;
}

<T><M>::<T><M>(unsigned M, unsigned N, <T> initval) : vec(M*N,initval)
{
  m=M;
  n=N;
}

<T><M>::<T><M>(const <T>Vec& data, unsigned M, unsigned N) : vec(data)
{
  m=M;
  n=N;
  if (vec.length()!=m*n)
    RWTHROW(RWInternalErr(RWMessage(RWLAPK_NUMBERPOINTS,vec.length(),m,n,className)));
}

<T><M>::<T><M>(const <T>* v, unsigned M, unsigned N) : vec(v,M*N), m(M), n(N)
{
}
>
> elif <M>==BandMat
>
<T><M>::<T><M>(unsigned M, unsigned N, unsigned lowerWidth, unsigned upperWidth)
  : vec(<M>Size(N,lowerWidth+upperWidth+1),rwUninitialized)
{
  if (M!=N) { RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTSQUARE,int(M),int(N)))); }
  n=N;
  band=lowerWidth+upperWidth+1;
  bandu=upperWidth;
  zeroUnusedEntries();
}

<T><M>::<T><M>(const <T>Vec& data, unsigned M, unsigned N, unsigned lowerWidth, unsigned upperWidth)
  : vec(data)
{
  if (M!=N) { RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTSQUARE,int(M),int(N)))); }
  n=N;
  band=lowerWidth+upperWidth+1;
  bandu=upperWidth;
  if (vec.length()!=<M>Size(N,band))
    RWTHROW(RWInternalErr(RWMessage(RWLAPK_NUMBERPOINTSBAND,vec.length(),n,n,className,band)));
}
>
> elif <M>==SymBandMat || <M>==HermBandMat
>
<T><M>::<T><M>(unsigned M, unsigned N, unsigned halfWidth)
  : vec(<M>Size(N,halfWidth),rwUninitialized)
{
  if (M!=N) { RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTSQUARE,int(M),int(N)))); }
  n=N;
  bandu=halfWidth;
  zeroUnusedEntries();
}

<T><M>::<T><M>(const <T>Vec& data, unsigned M, unsigned N, unsigned halfWidth )
  : vec(data)
{
  if (M!=N) { RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTSQUARE,int(M),int(N)))); }
  n=N;
  bandu=halfWidth;
  if (vec.length()!=<M>Size(N,bandu))
    RWTHROW(RWInternalErr(RWMessage(RWLAPK_NUMBERPOINTSBAND,vec.length(),n,n,className,bandwidth())));
}
>
> elif <M>==SymMat || <M>==SkewMat || <M>==HermMat || <M>==UpperTriMat || <M>==LowerTriMat || <M>==TriDiagMat
>
<T><M>::<T><M>(unsigned M, unsigned N)
  : vec( <M>Size(N),rwUninitialized )
{
  if (M!=N) { RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTSQUARE,int(M),int(N)))); }
  n=N;
> if <M>==TriDiagMat
  zeroUnusedEntries();
> endif
}

>   if <M>==SymMat
<T><M>::<T><M>(unsigned M, unsigned N, <T> initval )
  : vec( <M>Size(N), initval )
{
  if (M!=N) { RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTSQUARE,int(M),int(N)))); }
  n=N;
}
>   endif

<T><M>::<T><M>(const <T>Vec& data, unsigned M, unsigned N)
  : vec( data )
{
  if (M!=N) { RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTSQUARE,int(M),int(N)))); }
  n=N;
  if (vec.length()!=<M>Size(N))
    RWTHROW(RWInternalErr(RWMessage(RWLAPK_NUMBERPOINTS,vec.length(),n,n,className)));
}
>
> else
> err No shape constructors defined for shape <M>
> endif

> if <M>==BandMat || <M>==SymBandMat || <M>==HermBandMat || <M>==TriDiagMat
/*
 * Set entries in the data vector which are not used to zero.  This
 * avoids purify warnings and also ensures we won't have troubles
 * adding and subtracting these matrices
 */
void <T><M>::zeroUnusedEntries()
{
> if <M>==BandMat
    int i=lowerBandwidth();	   // Start i at min(n-1,upperBandwidth()-1)
    if (i>n-1) i=n-1;
    for( ; i>=0; i-- ) {
      vec.slice( i*bandwidth(), upperBandwidth()-i ) = <zero>;
    }
    i=lowerBandwidth()-1;	   // Start i at min(n-1,lowerBandwidth()-1)
    if (i>n-1) i=n-1;
    for( ; i>=0; i-- ) {
      vec.slice( (n-i)*bandwidth()-lowerBandwidth()+i, lowerBandwidth()-i ) = <zero>;
    }
> elif <M>==SymBandMat || <M>==HermBandMat
    int i=halfBandwidth()-1;       // Start i at min(n-1,halfBandwidth()-1)
    if (i>n-1) i=n-1;
    for( ; i>=0; i-- ) {
      vec.slice( i*(halfBandwidth()+1), halfBandwidth()-i ) = <zero>;
    }
> elif <M>==TriDiagMat
    if (n>0) { vec(3*n-1) = <zero>; vec(0) = <zero>; }
> else
> err No implementation of zeroUnusedEntries for mat type <M>
> endif
}
> endif

> # The out of line copies of the access functions
>
>
> # First the return value accessor.
> # The declaration and first bit of each function are common, 
> # then comes one function body for each different type of matrix.
>
<T> <T><M>::bcval(int i, int j) const
{
    if ( i<0 || i>=rows() || j<0 || j>=cols() ) {
      RWTHROW(RWBoundsErr(RWMessage(RWLAPK_OUTOFBOUNDS,i,j,rows(),cols())));
    }
> if <M>==SymMat
    if (i<=j) return vec(j*(j+1)/2+i); else return vec(i*(i+1)/2+j);
> elif <M>==SkewMat
    if (i<=j) return vec(j*(j+1)/2+i); else return -vec(i*(i+1)/2+j);
> elif <M>==HermMat
    if (i<=j) return vec(j*(j+1)/2+i); else return conj(vec(i*(i+1)/2+j));
> elif <M>==BandMat
    return ( (i<=j) ? ((j-i)>bandu) : ((i-j)>=(band-bandu)))
			? <zero> : vec(i-j+bandu + j*band);
> elif <M>==SymBandMat
    return ( (i<=j) ? ( ((j-i)>bandu) ? <zero> : vec(i+bandu+j*bandu) )
		    : ( ((i-j)>bandu) ? <zero> : vec(j+bandu+i*bandu) ) );
> elif <M>==HermBandMat
    return ( (i<=j) ? ( ((j-i)>bandu) ? <zero> : vec(i+bandu+j*bandu) )
		    : ( ((i-j)>bandu) ? <zero> : conj(vec(j+bandu+i*bandu)) ) );
> elif <M>==UpperTriMat
    return (j<i) ? <zero> : vec(j*(j+1)/2+i);
> elif <M>==LowerTriMat
    return (i<j) ? <zero> : vec(i*(i+1)/2+j);
> elif <M>==TriDiagMat
    return ( (i-j>1) || (i-j<(-1)) ) ? <zero> : vec(i+1+j*2);
> else
>   err No elementValue method defined for matrix type <M>
> endif
}

>
>
> # Now the set() function.
>
<T> <T><M>::bcset(int i, int j, <T> x)
{
    if ( i<0 || i>=rows() || j<0 || j>=cols() ) {
      RWTHROW(RWBoundsErr(RWMessage(RWLAPK_OUTOFBOUNDS,i,j,rows(),cols())));
    }
>
> # Now check to make sure we're allowed to change this element
>
> if <M>==SymMat || <M>==SkewMat || <M>==HermMat
>   # All elements of these types of matrix can be changed
> elif <M>==BandMat
    if ( (j>=i) ? ((j-i)>bandu) : ((i-j)>=(band-bandu)) )
                            RWTHROW(RWBoundsErr(RWMessage(RWLAPK_NOTSETABLE,i,j,className))); 
> elif <M>==SymBandMat || <M>==HermBandMat
    if ( (j>=i) ? ((j-i)>bandu) : ((i-j)>bandu) )
                            RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTSETABLE,i,j,className))); 
> elif <M>==UpperTriMat
    if (j<i) RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTSETABLE,i,j,className)));
> elif <M>==LowerTriMat
    if (i<j) RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTSETABLE,i,j,className)));
> elif <M>==TriDiagMat
    if (i-j<(-1) || i-j>1) RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTSETABLE,i,j,className)));
> else
>   err No setable check for matrix type <M>
> endif
>
> if <M>==SymMat
    if (i<=j) { vec(j*(j+1)/2+i)=x; return x; }
    else      { vec(i*(i+1)/2+j)=x; return x; }
    // return ((i<=j) ? (vec(j*(j+1)/2+i)=x) : (vec(i*(i+1)/2+j)=x));
    // The above kills the cc optimizer on the DEC 5400 (complex case)
> elif <M>==SkewMat
    if (i<=j) { vec(j*(j+1)/2+i)=x; return x; }
    else      { vec(i*(i+1)/2+j)=(-x); return -x; }
    // return (i<=j) ? (vec(j*(j+1)/2+i)=x) : (vec(i*(i+1)/2+j)=(-x));
    // The above kills the cc optimizer on the DEC 5400 (complex case)
> elif <M>==HermMat
    if (i<=j) { vec(j*(j+1)/2+i)=x; return x; }
    else      { vec(i*(i+1)/2+j)=conj(x); return conj(x); }
    // return (i<=j) ? (vec(j*(j+1)/2+i)=x) : (vec(i*(i+1)/2+j)=conj(x));
    // The above kills the cc optimizer on the DEC 5400 (complex case)
> elif <M>==BandMat
    return vec(i-j+bandu + j*band)=x;
> elif <M>==SymBandMat
    if (i<=j) { vec(i+bandu+j*bandu)=x; return x; }
    else      { vec(j+bandu+i*bandu)=x; return x; }
    // return ((i<=j) ? (vec(i+bandu+j*bandu)=x) : (vec(j+bandu+i*bandu)=x));
    // The above kills the cc optimizer on the DEC 5400 (complex case)
> elif <M>==HermBandMat
    if (i<=j) { vec(i+bandu+j*bandu)=x; return x; }
    else      { vec(j+bandu+i*bandu)=conj(x); return conj(x); }
    // return (i<=j) ? (vec(i+bandu+j*bandu)=x) : (vec(j+bandu+i*bandu)=conj(x));
    // The above kills the cc optimizer on the DEC 5400 (complex case)
> elif <M>==UpperTriMat
    return vec(j*(j+1)/2+i)=x; 
> elif <M>==LowerTriMat
    return vec(i*(i+1)/2+j)=x; 
> elif <M>==TriDiagMat
    return vec(i+1+j*2)=x;
> else
>   err No elementSet function defined for matrix type <M>
> endif
}

>
> # Here is the bcref function.  This returns an intermediate
> # reference class for many types of matrices.
>
>
> if <M>==SymMat
<T>& <T><M>::bcref(int i, int j) 
> elif <M>==BandMat || <M>==UpperTriMat || <M>==LowerTriMat || <M>==TriDiagMat || <M>==SymBandMat
RO<T>Ref <T><M>::bcref(int i, int j)
> elif <M>==SkewMat
NG<T>Ref <T><M>::bcref(int i, int j)
> elif <M>==HermMat
CJ<T>Ref <T><M>::bcref(int i, int j)
> elif <M>==HermBandMat
ROCJ<T>Ref <T><M>::bcref(int i, int j)
> else
> err No access method declared for ref() for type <M>
> endif
>
{
    if ( i<0 || i>=rows() || j<0 || j>=cols() ) {
      RWTHROW(RWBoundsErr(RWMessage(RWLAPK_OUTOFBOUNDS,i,j,rows(),cols())));
    }
> if <M>==SymMat
    if (i<=j) return vec(j*(j+1)/2+i); else return vec(i*(i+1)/2+j);
> elif <M>==SkewMat
    if (i<=j) return NG<T>Ref(vec(j*(j+1)/2+i),FALSE);
	      else return NG<T>Ref(vec(i*(i+1)/2+j),TRUE);
> elif <M>==HermMat
    if (i<=j) return CJ<T>Ref(vec(j*(j+1)/2+i),FALSE);
	      else return CJ<T>Ref(vec(i*(i+1)/2+j),TRUE);
> elif <M>==BandMat
    return ( (i<=j) ? ((j-i)>bandu) : ((i-j)>=(band-bandu)))
	    		? RO<T>Ref(rw<T>Zero,TRUE)
			: RO<T>Ref(vec(i-j+bandu + j*band),FALSE);
> elif <M>==SymBandMat
    return (i<=j) ? ( ((j-i)>bandu) ? RO<T>Ref(rw<T>Zero,TRUE)
				      : RO<T>Ref(vec(i+bandu+j*bandu),FALSE))
	   	  : ( ((i-j)>bandu) ? RO<T>Ref(rw<T>Zero,TRUE)
				      : RO<T>Ref(vec(j+bandu+i*bandu),FALSE));
> elif <M>==HermBandMat
    return (i<=j) ? ( ((j-i)>bandu) ? ROCJ<T>Ref(rw<T>Zero,TRUE)
				      : ROCJ<T>Ref(vec(i+bandu+j*bandu),FALSE))
	   	  : ( ((i-j)>bandu) ? ROCJ<T>Ref(rw<T>Zero,TRUE)
			      : ROCJ<T>Ref(vec(j+bandu+i*bandu),FALSE,TRUE));
> elif <M>==UpperTriMat
    return (j<i) ? RO<T>Ref(rw<T>Zero,TRUE)
		 : RO<T>Ref(vec(j*(j+1)/2+i),FALSE);
> elif <M>==LowerTriMat
    return (i<j) ? RO<T>Ref(rw<T>Zero,TRUE)
		 : RO<T>Ref(vec(i*(i+1)/2+j),FALSE);
> elif <M>==TriDiagMat
    return ( (i-j>1) || (i-j<(-1)) ) ? RO<T>Ref(rw<T>Zero,TRUE)
				     : RO<T>Ref(vec(i+1+j*2),FALSE);
> else
>   err No ref() method defined for matrix type <M>
> endif
}

>
> # Now the row(), col(), and diagonal() functions
>
> if <M>==TriDiagMat || <M>==BandMat || <M>==SymBandMat
<T>Vec <T><M>::bcdiagonal(int i) const
{
> # First the bounds checking section, then the code to actually
> # return the diagonal.
> 
> if <M>==SymBandMat || <M>==BandMat || <M>==TriDiagMat
  if ( i>=int(cols()) || (-i)>=int(rows()) ||
		i>int(upperBandwidth()) || (-i)>int(lowerBandwidth())) {
> else
> err Shouldnt get here, matrix shape <M>
> endif
    RWTHROW(RWBoundsErr(RWMessage(RWLAPK_DIAGOUTOFBOUNDS,-i,className,rows(),cols())));
  }
>
> # As promised, here's the code which actually returns the diagonal
>
    int iabs = (i>=0) ? i : -i;
>  if <M>==SymBandMat
    return vec.slice( (iabs+1)*(bandu), n-iabs, bandu+1 );
>  elif <M>==BandMat || <M>==TriDiagMat
    return vec.slice(i>0 ? upperBandwidth()+i*(bandwidth()-1)
			 : upperBandwidth()+iabs
			 , n-iabs, bandwidth() );
>  else
>  err Hey, man, how did I get here with shape <M>?
>  endif
}
> endif

/*
 * Here are non-inline versions of the row(), col(), and diagonal()
 * routines.  Normally these would be inlined, but due to a bug
 * in the Borland C++ compiler it is best not to inline them with
 * Borland C++.
 *
 * These routines just call the bounds checking routines.
 */

/*
 * The leadingSubmatrix function
 */

> if <M>==SymMat || <M>==SkewMat || <M>==HermMat || <M>==TriDiagMat || <M>==UpperTriMat || <M>==LowerTriMat
>
<T><M> <T><M>::leadingSubmatrix(int k)
{
  if (k<0 || k>n) RWTHROW(RWBoundsErr(RWMessage(RWLAPK_LEADINGSUBMATRIXORDER,k,n)));
  return <T><M>( vec.slice(0,<M>Size(k)), k, k );
}
> elif <M>==BandMat
>
<T><M> <T><M>::leadingSubmatrix(int k)
{
  if (k<0 || k>n) RWTHROW(RWBoundsErr(RWMessage(RWLAPK_LEADINGSUBMATRIXORDER,k,n)));
  return <T><M>( vec.slice(0,<M>Size(k,bandwidth())), k, k,lowerBandwidth(),upperBandwidth());
}
> elif <M>==SymBandMat || <M>==HermBandMat
>
<T><M> <T><M>::leadingSubmatrix(int k)
{
  if (k<0 || k>n) RWTHROW(RWBoundsErr(RWMessage(RWLAPK_LEADINGSUBMATRIXORDER,k,n)));
  return <T><M>( vec.slice(0,<M>Size(k,halfBandwidth())), k, k, halfBandwidth() );
}
> else
> err No leadingSubmatrix function for shape <M>
> endif

> # misc includes assignment, copying and resizing routines
>
>
> # The assignment operators
>
<T><M>& <T><M>::operator=(const <T><M>& M)
{
    if (rows()!=M.rows() || cols()!=M.cols()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATSIZE,M.rows(),M.cols(),rows(),cols())));
    }
> if <M>==BandMat || <M>==SymBandMat || <M>==HermBandMat
    if (lowerBandwidth()<M.lowerBandwidth() ||
	upperBandwidth()<M.upperBandwidth()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_BANDSIZE,M.lowerBandwidth(),lowerBandwidth(),
				upperBandwidth(), M.upperBandwidth() )));
    }
> endif
>
> # Band matrices get special treatment
> if <M>==BandMat || <M>==SymBandMat || <M>==HermBandMat
    if (bandwidth()==M.bandwidth()) {
      vec = M.vec;
    } else {
      int u = upperBandwidth();
      int Mu = M.upperBandwidth();
>  if <M>==BandMat
      int l = lowerBandwidth();
      int Ml = M.lowerBandwidth();
      for( int i= -l; i<=u; i++ ) {
	if ( i<-Ml || i>Mu ) diagonal(i)=<zero>;
	else diagonal(i)=M.diagonal(i);
      }
    }
>  elif <M>==SymBandMat || <M>==HermBandMat
      for( int i= 0; i<=u; i++ ) {
	<T>Vec d = vec.slice((i+1)*bandu,n-i,bandu+1);
	if ( i>Mu ) d=<zero>;
	else d=M.vec.slice((i+1)*M.bandu,n-i,M.bandu+1);
      }
    }
>  else
>  err Internal error
>  endif
> else
    vec = M.vec;
> endif
    return *this;
}

<T><M>& <T><M>::reference(<T><M>& A)
{
    vec.reference(A.vec);
    n=A.n;
> if <M>==GenMat
    m=A.m;
> elif <M>==BandMat
    band=A.band;
    bandu=A.bandu;
> elif <M>==SymBandMat || <M>==HermBandMat
    bandu=A.bandu;
> endif
    return *this;
}

>
> # The copy function
>
<T><M> <T><M>::copy() const
{
  <T><M> A( *((<T><M>*)this) );	// cast this to non-const to make cfront happy
  A.deepenShallowCopy();
  return A;
}

>
> # The resize function
>
void <T><M>::resize(unsigned M, unsigned N)
{
> if <M>==GenMat
    <T><M> olddata(*this);
> endif
>
> if <M>!=GenMat
    if (M!=N) { RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTSQUARE,int(M),int(N)))); }
> endif
>
> if <M>==BandMat || <M>==TriDiagMat
    // Zero out elements past the bottom of the matrix if need be so
    // they don't enter in to the new matrix
    zeroUnusedEntries();
> endif
>
> # Reset the size of the matrix
    n=N;
> if <M>==GenMat
    m=M;
> endif
>
> # Now set the size of the data vector
> if <M>==GenMat
    vec.resize(<M>Size(n,m));
> elif <M>==BandMat
    vec.resize(<M>Size(n,bandwidth()));
> elif <M>==SymBandMat || <M>==HermBandMat
    vec.resize(<M>Size(n,upperBandwidth()));
> else
    vec.resize(<M>Size(n));
> endif
>
> # Now, copy over the old data (only necessary for general matrices)
> if <M>==GenMat
    if (rows()!=olddata.rows()) {
      unsigned copyNumCols = cols() < olddata.cols() ? cols() : olddata.cols();
      unsigned copyColLen  = rows() < olddata.rows() ? rows() : olddata.rows();
      vec.resize(rows()*cols());
      deepenShallowCopy();	// Make sure we're not sharing the data
      (*this).zero();
      for( int i=0; i<copyNumCols; i++ ) { // Copy over column by column
        vec.slice(rows()*i,copyColLen) = olddata.col(i).slice(0,copyColLen);
      }
    }
> endif

> if <M>==BandMat || <M>==SymBandMat || <M>==HermBandMat || <M>==TriDiagMat
  // Finally, zero any unused entries just to keep things tidy
  zeroUnusedEntries();
> endif
}

> if <M>==BandMat
>
void <T><M>::resize(unsigned M, unsigned N, unsigned lowWidth, unsigned upWidth)
{
  resize(M,N);	// Set the dimension - all that's left is to change bandwidth
  int l = lowerBandwidth();  // Convert all unsigneds to ints to avoid nasty
  int u = upperBandwidth();  // surprises when trying stuff like i= -lowWidth
  int lw = lowWidth;
  int uw = upWidth;
  if (l!=lowWidth || u!=upWidth) {
    <T><M> newMat(M,N,lowWidth,upWidth);
    for( int i= -lw; i<=uw; i++ ) {
      <T>Vec d = newMat.diagonal(i);
      if ( i<(-l) || i>u ) { d = <zero>; }
      else                 { d = this->diagonal(i); }
    }
    this->reference(newMat);
  }
}
>
> elif <M>==SymBandMat || <M>==HermBandMat
>
void <T><M>::resize(unsigned M, unsigned N, unsigned halfWidth)
{
  resize(M,N);	// Set the dimension - all that's left is to change bandwidth
  int h = halfBandwidth();
  if (h!=halfWidth) {
    <T><M> newMat(M,N,halfWidth);
    for( int i=0; i<=halfWidth; i++ ) {
      <T>Vec d = newMat.vec.slice((i+1)*newMat.bandu,newMat.n-i,newMat.bandu+1);
      if ( i<(-h) || i>h ) d = <zero>;
      else                 d = vec.slice((i+1)*bandu,n-i,bandu+1);
    }
    this->reference(newMat);
  }
}
>
> endif

> # the makeDiagonalReal() function
> 
> if <M>==HermMat || <M>==HermBandMat
>
void <T><M>::makeDiagonalReal()
{
  for(int i=rows(); i--;) {
    <T> entry = val(i,i);
    // imag(entry) = 0;   <- this fails because some compilers don't have imag return an lvalue
    set(i,i,<T>(real(entry),0));
  }
}
> endif

> # the input/output routines
>
/*
 * printOn,scanFrom:  printOn prints all non-numeric stuff first
 * then prints the numbers defining the shape and size of the matrix,
 * then prints the matrix itself.  The format used by printOn can be
 * used to read in a matrix with scanFrom.
 *
 * scanFrom first eats all the non-numeric characters it encounters.
 * This takes care of the stuff printOn spews before the data.  Next
 * it reads the numbers which define the matrix shape and then the
 * data is read in.
 */

void <T><M>::printOn(ostream& outs) const
{
    int w = outs.width(0);
> if <M>!=GenMat
    int m=n;
> endif
>
>
> # Special things to print out for special matrices
>
> if <M>!=GenMat
      outs << className << ", ";
> endif
> if <M>==BandMat
    outs << "lower and upper bandwidths: " << lowerBandwidth() << " ";
    outs << upperBandwidth() << " ";
> endif
> if <M>==SymBandMat || <M>==HermBandMat
    outs << "half bandwidth: " << halfBandwidth() << " ";
> endif
>
    outs << rows() << "x" << cols() << " [\n";
    for( int i=0; i<m; i++ ) {
      for( int j=0; j<n; j++ ) {
> if <T>==UChar || <T>==SChar
	// Write out characters as ints to avoid weirdness when
	// writing unprintable characters
	outs << setw(w) << int(val(i,j)) << " ";
> else
	outs << setw(w) << val(i,j) << " ";
> endif
      }
      outs << "\n";
    }
    outs << "]";
}

void <T><M>::scanFrom(istream& s)
{
  char c;
  unsigned numRows, numCols;
> if <M>==BandMat
  unsigned lwidth, uwidth;
> elif <M>==SymBandMat || <M>==HermBandMat
  unsigned hwidth;
> endif

  /* Skip through leading non-digits */
  do { s.get(c); } while (!s.fail() && !isdigit(c));
  s.putback(c);

> if <M>==BandMat
  s >> lwidth >> uwidth;
> endif
> if <M>==SymBandMat || <M>==HermBandMat
  s >> hwidth;
> endif

  s >> numRows;
  do { s.get(c); } while (isspace(c));	// Skip optional x character
  if (isdigit(c)) s.putback(c);	
  s >> numCols;

  <T>Vec v;			// Read the data
  v.scanFrom(s);

  if (!s.fail()) { 		// Now put the data into the matrix
    if (v.length()!=numRows*numCols) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_WRONGNUMPOINTS,v.length(),className,numRows,numCols)));
    } else {
> if <M>==BandMat
      resize(numRows,numCols,lwidth,uwidth);
> elif <M>==SymBandMat || <M>==HermBandMat
      resize(numRows,numCols,hwidth);
> else
      resize(numRows,numCols);
> endif
      int index = 0;
      for(int i=0; i<numRows; i++) {
        for(int j=0; j<numCols; j++) {
	  if (val(i,j)!=v[index]) bcset(i,j,v[index]);
	  index++;
	}
      }
    }
  }
}

void <T><M>::saveOn(RWFile& file) const
{
    /* First write the numbers which define the shape of the matrix */
> if <M>==GenMat
    file.Write(m);
    file.Write(n);
> elif <M>==SymMat || <M>==SkewMat || <M>==HermMat || <M>==TriDiagMat || <M>==UpperTriMat || <M>==LowerTriMat
    file.Write(n);
> elif <M>==BandMat
    file.Write(n);
    file.Write(band);
    file.Write(bandu);
> elif <M>==SymBandMat || <M>==HermBandMat
    file.Write(n);
    file.Write(bandu);
> else
> err No saveOn function defined for shape <M>
> endif
    vec.saveOn(file);
}

void <T><M>::saveOn(RWvostream& s) const
{
    /* First write the numbers which define the shape of the matrix */
> if <M>==GenMat
    s << m;
    s << n;
> elif <M>==SymMat || <M>==SkewMat || <M>==HermMat || <M>==TriDiagMat || <M>==UpperTriMat || <M>==LowerTriMat
    s << n;
> elif <M>==BandMat
    s << n;
    s << band;
    s << bandu;
> elif <M>==SymBandMat || <M>==HermBandMat
    s << n;
    s << bandu;
> else
> err No saveOn function defined for shape <M>
> endif
    vec.saveOn(s);
}

void <T><M>::restoreFrom(RWFile& file)
{
    /* First read the numbers which define the shape of the matrix */
> if <M>==GenMat
    file.Read(m);
    file.Read(n);
    int size = <M>Size(m,n);
> elif <M>==SymMat || <M>==SkewMat || <M>==HermMat || <M>==TriDiagMat || <M>==UpperTriMat || <M>==LowerTriMat
    file.Read(n);
    int size = <M>Size(n);
> elif <M>==BandMat
    file.Read(n);
    file.Read(band);
    file.Read(bandu);
    int size = <M>Size(n,band);
> elif <M>==SymBandMat || <M>==HermBandMat
    file.Read(n);
    file.Read(bandu);
    int size = <M>Size(n,bandu);
> else
> err No restoreFrom function defined for shape <M>
> endif
    vec.restoreFrom(file);
    if (vec.length() != size) RWTHROW(RWInternalErr(RWMessage(RWLAPK_RESTORESIZE,className,vec.length(),size)));
}

void <T><M>::restoreFrom(RWvistream& s)
{
    /* First read the numbers which define the shape of the matrix */
> if <M>==GenMat
    s >> m;
    s >> n;
    int size = <M>Size(m,n);
> elif <M>==SymMat || <M>==SkewMat || <M>==HermMat || <M>==TriDiagMat || <M>==UpperTriMat || <M>==LowerTriMat
    s >> n;
    int size = <M>Size(n);
> elif <M>==BandMat
    s >> n;
    s >> band;
    s >> bandu;
    int size = <M>Size(n,band);
> elif <M>==SymBandMat || <M>==HermBandMat
    s >> n;
    s >> bandu;
    int size = <M>Size(n,bandu);
> else
> err No restoreFrom function defined for shape <M>
> endif
    vec.restoreFrom(s);
    if (vec.length() != size) RWTHROW(RWInternalErr(RWMessage(RWLAPK_RESTORESIZE,className,vec.length(),size)));
}

unsigned <T><M>::binaryStoreSize() const
{
    /* First determine the size of the stuff
       which determines the matrix shape.    */
> if <M>==GenMat
    unsigned size = 2*sizeof(unsigned);	// m and n
> elif <M>==SymMat || <M>==SkewMat || <M>==HermMat || <M>==TriDiagMat || <M>==UpperTriMat || <M>==LowerTriMat
    unsigned size = sizeof(unsigned);    // n
> elif <M>==BandMat
    unsigned size = 3*sizeof(unsigned);  // n and band and bandu
> elif <M>==SymBandMat || <M>==HermBandMat
    unsigned size = 3*sizeof(unsigned);  // n and bandu
> else
> err No binaryStoreSize function defined for shape <M>
> endif
    return size + vec.binaryStoreSize();
}

/*
 * Implementation of DoubleEigHessServer
 *
 * Generated from template $Id: xeighess.cpp,v 1.4 1993/10/11 21:48:09 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
  * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */ 

#include "rw/deigsrv.h"
#include "rw/dhess.h"
#include "rw/lapack.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"

DoubleHessEigServer::DoubleHessEigServer(RWBoolean left, RWBoolean right, RWBoolean sc, RWBoolean perm)
  : selectRange_((const RWSlice&)RWAll)
{
  computeLeftVecs_  = left;
  computeRightVecs_ = right;
  scale_            = sc;
  permute_          = perm;
}

RWBoolean DoubleHessEigServer::computeLeftEigenVectors() const       {return computeLeftVecs_;}
void      DoubleHessEigServer::computeLeftEigenVectors(RWBoolean x)  {computeLeftVecs_ = x;}
RWBoolean DoubleHessEigServer::computeRightEigenVectors() const      {return computeRightVecs_;}
void      DoubleHessEigServer::computeRightEigenVectors(RWBoolean x) {computeRightVecs_ = x;}
RWBoolean DoubleHessEigServer::scale()                    const      {return scale_;}
void      DoubleHessEigServer::scale(RWBoolean x)                    {scale_ = x;}
RWBoolean DoubleHessEigServer::permute()                  const      {return permute_;}
void      DoubleHessEigServer::permute(RWBoolean x)                  {permute_ = x;}
void      DoubleHessEigServer::balance(RWBoolean x)                  {scale_ = permute_ = x;}

void DoubleHessEigServer::selectEigenVectors(const IntVec& s)
{
  selectVec_.reference(s.copy());
  selectRange_ = RWAll;
}

void DoubleHessEigServer::selectEigenVectors(const RWSlice& s)
{
  selectRange_ = s;
  selectVec_.reshape(0);
}

RWBoolean DoubleHessEigServer::computeAllEigenVectors() const
{
  // The next line needs to determine if the RWSlice object selectRange_
  // is a copy of RWAll.  This means it will have a begin of zero, a stride
  // of one, and a length which matches whatever length the argument vector
  // is.  Test for this by asking for two lengths, if the slice gives back
  // the two lengths, then it must be an RWAll.
  RWBoolean isRWAll = (selectRange_.begin()==0 && selectRange_.stride()==1 && selectRange_.len(5)==5 && selectRange_.len(6)==6);
  return (isRWAll && selectVec_.length()==0);
}

DoubleEigDecomp DoubleHessEigServer::operator()(const DoubleGenMat& A)
{
  DoubleHessenbergDecomp x(A,permute_,scale_);
  return (*this)(x);
}

DoubleEigDecomp DoubleHessEigServer::operator()(const DoubleBalanceDecomp& A)
{
  DoubleHessenbergDecomp x(A);
  return (*this)(x);
}

DoubleEigDecomp DoubleHessEigServer::setEigenVals(const DoubleHessenbergDecomp& A, DoubleVec *wr, DoubleVec *wi)
{
  DoubleEigDecomp eig;
  n(eig) = A.rows();
  long n = A.rows();
  DoubleGenMat H = A.H();
  H.deepenShallowCopy();   // just in case the interface changes
  lambda(eig).reshape(A.rows());
  wr->reshape(A.rows());
  wi->reshape(A.rows());
  long ilo = A.balanceTransform().lowIndex();
  long ihi = A.balanceTransform().highIndex();
  Double *work = new Double [n];
  long info;
  dhseqr('E','N',n,ilo,ihi,H.data(),H.colStride(),wr->data(),wi->data(),0,1,work,0,info);
  RWPOSTCONDITION(info>=0);
  if (info>0) {
    RWPOSTCONDITION(ilo-1+n-info<n);
    lambda(eig).reshape((int)(ilo-1+n-info));
    (*wr)(RWSlice((int)ilo,(int)(n-info))) = (*wr)(RWToEnd((int)info));
    wr->reshape((int)(ilo-1+n-info));
    (*wi)(RWSlice((int)ilo,(int)(n-info))) = (*wi)(RWToEnd((int)info));
    wi->reshape((int)(ilo-1+n-info));
    computedAll(eig) = FALSE;
  }
  real(lambda(eig)) = *wr;
  imag(lambda(eig)) = *wi;
  delete work;
  return eig;
}

DoubleEigDecomp DoubleHessEigServer::operator()(const DoubleHessenbergDecomp& A)
{    
  // First build a container and set up the eigenvalues
  DoubleVec wr,wi;
  DoubleEigDecomp eig = setEigenVals(A,&wr,&wi);
  if (!eig.good()) return eig;   // in case we didn't get the eigenvalues

  // Now determine which eigenvectors we want
  IntVec select(n(eig),0);
  if (computeAllEigenVectors()) {
    select=1;
  } else {
    if (selectVec_.length()!=0) {
      for(int i=selectVec_.length(); i--;) {
        int x = selectVec_(i);
        if (x<0 || x>=n(eig)) RWTHROW(RWBoundsErr(RWMessage(RWLAPK_NOEIG,x)));
        select(x) = 1;
      }
    } else {  // which to compute is indicated by the RWSlice
      selectRange_.boundsCheck(n(eig));
      select(selectRange_) = 1;
    }

    // Make sure that both halves of complex conjugate eigenvals are chosen
    for(int j=select.length(); j--;) {
      if (select(j)) {
        DComplex eval = eig.eigenValue(j);
        if (imag(eval)!=0) {
          if (j>0 && real(eval)==real(eig.eigenValue(j-1))) {
            select(j-1) = 1;
          } else {
            RWPOSTCONDITION(j+1<eig.rows() && real(eval)==real(eig.eigenValue(j+1)));  // complex must be in a pair
            select(j+1) = 1;
          }
        }
      }
    }

  }

  long mm=0;        // compute number of eigenvectors desired
  for(int i=select.length(); i--;) { if (select(i)) mm++; }
  DoubleGenMat vl;
  DoubleGenMat vr;
  if (computeLeftVecs_)  { vl.reshape(A.rows(),(unsigned)mm); }
  if (computeRightVecs_) { vr.reshape(A.rows(),(unsigned)mm); }
  if (mm>0 && (computeLeftVecs_ || computeRightVecs_)) {
    long n = A.rows();
    char job = (computeLeftVecs_ ? (computeRightVecs_ ? 'B' : 'L') : 'R');
    DoubleGenMat H = A.H();
    long ldh = H.colStride();
    long ldvl = rwmax(1,vl.colStride());   // max to cover the case of colStride zero
    long ldvr = rwmax(1,vr.colStride());
    long m;
    long info;
    IntVec selectcopy = select.copy();     // Because dtrevc modifies select
    Double *work = new Double [(n+2)*n];
    long *ifaill = new long [mm];
    long *ifailr = new long [mm];
    dhsein(job,'Q','N',selectcopy.data(),n,H.data(),ldh,
                          wr.data(),wi.data(),vl.data(),ldvl,
                          vr.data(),ldvr,mm,m,work,ifaill,ifailr,info) ;
    RWPOSTCONDITION(info>=0);
    if (info>0) {
      accurate(eig) = FALSE;
    }
    RWPOSTCONDITION(m==mm);
    delete work;
    // Now de-balance the eigenvectors
    if (computeLeftVecs_) { P(eig).reference(A.BInvTX(A.QX(vl))); }
    if (computeRightVecs_){ Q(eig).reference(A.BX(A.QX(vr))); }

    // Finally, move the eigenvalues corresponding the the chosen
    // eigenvectors to the front so the positions match up.  Do this
    // in two passes.
    DComplexVec temp = lambda(eig);
    lambda(eig).deepenShallowCopy();
    int index=0;
    for(int i=0; i<temp.length(); i++) {
      if (select(i)) lambda(eig)(index++) = temp(i);
    }
    for(i=0; i<temp.length(); i++) {
      if (!select(i)) lambda(eig)(index++) = temp(i);
    }
    RWPOSTCONDITION(index==lambda(eig).length());
  }
  return eig;
}

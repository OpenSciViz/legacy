#if 0
/*
 * Generated from template $Id: xmth.cpp,v 1.2 1993/10/11 21:48:17 alv Exp $
 *
 * Math member functions for the matrix types
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */
#endif

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/cltrimat.h"
#include "rw/dltrimat.h"
#include "rw/cutrimat.h"

/*
 * This function returns a new matrix the same shape as the one 
 * passed in but with the data passed in.  This way we restrict
 * all the >if statements to a couple lines
 */
inline DComplexLowerTriMat sameShapeMatrix( DComplexVec& vec, const DComplexLowerTriMat& A )
{
  return DComplexLowerTriMat(vec,A.rows(),A.cols());
}

inline DoubleLowerTriMat sameShapeRealMatrix( DoubleVec& vec, const DComplexLowerTriMat& A )
{
  return DoubleLowerTriMat(vec,A.rows(),A.cols());
}

static void verifyMatch(const DComplexLowerTriMat& A, const DComplexLowerTriMat& B)
{
    if (A.rows()!=B.rows() || A.cols()!=B.cols())
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATSIZE,A.rows(),A.cols(),B.rows(),B.cols())));
}

RWBoolean DComplexLowerTriMat::operator==(const DComplexLowerTriMat& X)
{
    if (n!=X.n) return FALSE;
    return vec==X.vec;	// All elements in vec used in the matrix
}


DComplexLowerTriMat& DComplexLowerTriMat::operator+=(const DComplexLowerTriMat& m)
{
  verifyMatch(*this,m);
  vec += m.vec;
  return *this;
}

DComplexLowerTriMat& DComplexLowerTriMat::operator-=(const DComplexLowerTriMat& m)
{
  verifyMatch(*this,m);
  vec -= m.vec;
  return *this;
}

DComplexLowerTriMat& DComplexLowerTriMat::operator*=(const DComplexLowerTriMat& m)
{
  verifyMatch(*this,m);
  vec *= m.vec;
  return *this;
}

DComplexLowerTriMat& DComplexLowerTriMat::operator*=(DComplex x)
{
  vec *= x;
  return *this;
}

DComplexLowerTriMat& DComplexLowerTriMat::operator/=(const DComplexLowerTriMat& m)
{
  verifyMatch(*this,m);
  vec /= m.vec;
  return *this;
}

DComplexLowerTriMat& DComplexLowerTriMat::operator/=(DComplex x)
{
  vec /= x;
  return *this;
}




DComplexLowerTriMat operator-(const DComplexLowerTriMat& A)
{
  DComplexVec temp(-(A.dataVec()));
  return sameShapeMatrix(temp,A);
}
  
DComplexLowerTriMat operator+(const DComplexLowerTriMat& A)
{
  return A;
}

DComplexLowerTriMat operator*(const DComplexLowerTriMat& A, const DComplexLowerTriMat& B) {
	verifyMatch(A,B);
	DComplexVec temp(A.dataVec()*B.dataVec());
	return sameShapeMatrix(temp,A);
}

DComplexLowerTriMat operator/(const DComplexLowerTriMat& A, const DComplexLowerTriMat& B) {
	verifyMatch(A,B);
	DComplexVec temp(A.dataVec()/B.dataVec());
	return sameShapeMatrix(temp,A);
}

DComplexLowerTriMat operator+(const DComplexLowerTriMat& A, const DComplexLowerTriMat& B) {
	verifyMatch(A,B);
	DComplexVec temp(A.dataVec()+B.dataVec());
	return sameShapeMatrix(temp,A);
}

DComplexLowerTriMat operator-(const DComplexLowerTriMat& A, const DComplexLowerTriMat& B) {
	verifyMatch(A,B);
	DComplexVec temp(A.dataVec()-B.dataVec());
	return sameShapeMatrix(temp,A);
}

DComplexLowerTriMat operator*(const DComplexLowerTriMat& A, DComplex x) {
	DComplexVec temp(A.dataVec()*x);
	return sameShapeMatrix(temp,A);
}

/*
 * Out-of-line version for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
  DComplexLowerTriMat operator*(DComplex x, const DComplexLowerTriMat& A) { return A*x; }
#endif

DComplexLowerTriMat operator/(const DComplexLowerTriMat& A, DComplex x) {
	DComplexVec temp(A.dataVec()/x);
	return sameShapeMatrix(temp,A);
}





DoubleLowerTriMat abs(const DComplexLowerTriMat& A) {
	DoubleVec temp(abs(A.dataVec()));
	return sameShapeRealMatrix(temp,A);
}


DComplexLowerTriMat conj(const DComplexLowerTriMat& A) {
	DComplexVec temp(conj(A.dataVec()));
	return sameShapeMatrix(temp,A);
}

DoubleLowerTriMat real(const DComplexLowerTriMat& A) {
	DoubleVec temp(real(A.dataVec()));
	return sameShapeRealMatrix(temp,A);
}

DoubleLowerTriMat imag(const DComplexLowerTriMat& A) {
	DoubleVec temp(imag(A.dataVec()));
	return sameShapeRealMatrix(temp,A);
}

DoubleLowerTriMat norm(const DComplexLowerTriMat& A) {
	DoubleVec temp(norm(A.dataVec()));
	return sameShapeRealMatrix(temp,A);
}

DoubleLowerTriMat arg(const DComplexLowerTriMat& A) {
	DoubleVec temp(arg(A.dataVec()));
	return sameShapeRealMatrix(temp,A);
}



DComplexUpperTriMat transpose(const DComplexLowerTriMat& A)
{
return DComplexUpperTriMat(A.dataVec(),A.rows(),A.cols());
}

DComplexVec product(const DComplexLowerTriMat& A, const DComplexVec& x)
{
    if (A.cols()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATVECPROD,A.className,A.rows(),A.cols(),x.length())));
    }
    DComplexVec y(A.rows(),rwUninitialized);
    const DComplexVec& v = A.dataVec();
    int n=A.rows();
    for( int i=0; i<n; i++ ) {
      y(i) = dot( v.slice(i*(i+1)/2,i+1,1), x.slice(0,i+1,1) );
    }
    return y;
}

DComplexVec product(const DComplexVec& x, const DComplexLowerTriMat& A)
{
    if (A.rows()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECMATPROD,x.length(),A.className,A.rows(),A.cols())));
    }
    return product(transpose(A),x);
}



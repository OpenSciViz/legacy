> #################################################################
> # Least squares fitting using Cholesky decomposition            #
> #                                                               #
> # $Header: /users/rcs/lapksrc/xlsch.h,v 1.3 1993/07/05 19:19:33 alv Exp $
> #                                                               #
> # Copyright (1991) by Rogue Wave Software, Inc.  This software  #
> # is subject to copyright protection under the laws of the      #
> # United States and other countries.                            #
> #                                                               #
> # Written by Al Vermeulen.                                      #
> #                                                               #
> # Limited License.                                              #
> #################################################################
> #
> # $Log: xlsch.h,v $
> # Revision 1.3  1993/07/05  19:19:33  alv
> # fixed up for complex numbers
> #
> # Revision 1.2  1993/06/01  15:09:39  alv
> # added global fn residualNorm
> #
> # Revision 1.1  1993/05/18  17:00:02  alv
> # Initial revision
> #
>
> define <T>=RW_T_RW
> if <T>==Double
> define <P>=Double <tLetter>=d
> elif <T>==Float
> define <P>=Float <tLetter>=f
> elif <T>==DComplex
> define <P>=Double <tLetter>=c
> else
> err least square class: not defined for '<T>'
> endif
> 
> # The traditional top of the file stuff
>
> beginwrapper <tLetter>lsch
>
/*
 * <T>LeastSqCh:  solve least square Ch problem with orthogonal decomposition
 *
 * Generated from template $Id: xlsch.h,v 1.3 1993/07/05 19:19:33 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 * This least squares class only works for full rank problems.  For
 * more accurate, robust least squares classes, use the QR or SV
 * least squares factorizations.  This class has the advantage of
 * being the fastest of the three.
 */

#include "rw/<tLetter>pdfct.h"
#include "rw/<tLetter>genmat.h"

class <T>LeastSqCh {
private:
  <T>GenMat A_;
  <T>PDFact decomp_;   // Cholesky decomposition of the normal equations
public:                                          
  <T>LeastSqCh();
  <T>LeastSqCh(const <T>GenMat& A);
  void      factor(const <T>GenMat& A);
  RWBoolean good() const {return decomp_.good();}
  RWBoolean fail() const {return decomp_.fail();}
  unsigned  rows() const {return A_.rows();}
  unsigned  cols() const {return A_.cols();}
  unsigned  rank() const {return A_.cols();}
  <T>Vec    residual(const <T>Vec& data) const;
  <P>       residualNorm(const <T>Vec& data) const;
  <T>Vec    solve(const <T>Vec& data) const;
};

inline <T>Vec solve(   const <T>LeastSqCh& A, const <T>Vec& b) {return A.solve(b);}
inline <T>Vec residual(const <T>LeastSqCh& A, const <T>Vec& b) {return A.residual(b);}
inline <T>    residualNorm(const <T>LeastSqCh& A, const <T>Vec& b) {return A.residualNorm(b);}

> endwrapper

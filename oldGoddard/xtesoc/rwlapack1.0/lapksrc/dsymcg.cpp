/*
 * The conversion functions to/from general matrices.
 *
 * Generated from template $Id: xcg.cpp,v 1.2 1993/07/05 19:19:33 alv Exp $
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/dsymmat.h"
#include "rw/dgenmat.h"

DoubleGenMat::DoubleGenMat( const DoubleSymMat& A )
  : RWMatView(A.rows(),A.cols(),sizeof(Double))
{
  for( int j=0; j<nrows; j++ ) {
    for( register int i=0; i<=j; i++ ) {
      Double x = A.val(i,j);
      set(i,j,x);
      set(j,i,x);
    }
  }
}

DoubleSymMat toSymMat( const DoubleGenMat& S )
{
  unsigned n = S.cols();
  DoubleSymMat A(n,n);
  for( int j=0; j<n; j++ ) {
    for( int i=0; i<=j; i++ ) {                   // This loop should be in a bla call for nonsmall j
#ifdef RW_HIGHC_INLINE_BUG
      Double temp1 = S.val(i,j);      // High C++ needs these temporaries 
      Double temp2 = S.val(j,i);
      A(i,j) = (temp1+temp2)/2;
#else
      A(i,j) = (S.val(i,j)+S.val(j,i))/2;
#endif
    }
  }
  return A;
}

DoubleSymMat upperToSymMat( const DoubleGenMat& S )
{
  unsigned n = S.cols();
  DoubleSymMat A(n,n);
  for(int j=0; j<n; j++) {
    for(int i=0; i<=j; i++) {                     // This loop should be in a bla call for nonsmall j
      A(i,j) = S.val(i,j);
    }
  }
  return A;
}

DoubleSymMat lowerToSymMat( const DoubleGenMat& S )
{
  unsigned n = S.cols();
  DoubleSymMat A(n,n);
  for(int j=0; j<n; j++) {
    for(int i=0; i<=j; i++) {                     // This loop should be in a bla call for nonsmall j
      A(i,j) = S.val(j,i);
    }
  }
  return A;
}


/*
 * Implementation of FloatSymEigServer
 *
 * Generated from template $Id: xseigsrv.cpp,v 1.2 1993/07/05 19:19:33 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */ 

#include "rw/fseigsrv.h"
#include "rw/ftd.h"

FloatSymEigDecomp FloatSymEigServer::operator()(const FloatSymMat& A)
{
  FloatSymDenseTriDiagDecomp td(A);         // Build a tri-diagonal decomposition
  FloatSymEigDecomp tdeig = decompose(td);  // Get eigenvals/vecs for tri-diagonal problem
  FloatSymEigDecomp eig = tdeig;            // Needs to be done this way for complex numbers
  P(eig) = td.transform(P(tdeig));
  return eig;
}

FloatSymEigDecomp FloatSymEigServer::operator()(const FloatSymBandMat& A)
{
  FloatSymBandTriDiagDecomp td(A);          // Build a tri-diagonal decomposition
  FloatSymEigDecomp tdeig = decompose(td);  // Get eigenvals/vecs for tri-diagonal problem
  FloatSymEigDecomp eig = tdeig;            // Needs to be done this way for complex numbers
  P(eig) = td.transform(P(tdeig));
  return eig;
}

/*
 * Implementation of FloatPDFact
 *
 * Generated from template $Id: xfct.cpp,v 1.6 1993/10/11 21:48:11 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/lapkdefs.h"
#include "rw/fpdfct.h"
#include "rw/lapack.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"  
#include "rw/fgenmat.h"

const char* FloatPDFact::className = "FloatPDFact";

FloatPDFact::~FloatPDFact()
{
}

FloatPDFact::FloatPDFact()
{
  info = -1;
}

FloatPDFact::FloatPDFact( const FloatPDFact& A )
  : factorization(A.factorization)
{
  info = A.info;
  Anorm = A.Anorm;
}

FloatPDFact::FloatPDFact(const FloatSymMat& A, RWBoolean estimateCondition)
  : factorization(A)  // Use reference semantics, followed by
                      // deepenShallowCopy(), to work around a BCC V3.0 bug
{
    factorization.deepenShallowCopy();
    dofactor(estimateCondition);
}

void FloatPDFact::factor( const FloatSymMat& A, RWBoolean estimateCondition )
{
    FloatSymMat Acopy = A.copy();
    factorization.reference(Acopy);
    dofactor(estimateCondition);
}

void FloatPDFact::dofactor(RWBoolean estimateCondition)
{
   info=0;   // for size zero matrices, need to set this explicitly
   long n = rows();	

    // Error (sanity) checking
    RWPRECONDITION(factorization.dataVec().stride()==1);


    // If we care to ever find the condition number, we need to know
    // the 1-norm of the matrix A.  Compute it while the member data
    // factorization is still a copy of A, and not yet it's factorization.
    Anorm = -1; 
    if (n>0 && estimateCondition) {
      Float *work = new Float [n];
      Anorm = slansp('1','U',n,factorization.data(),work);
      delete [] work;
    }

    if (n>0) {
      spptrf('U',n,factorization.data(),info);
      RWPOSTCONDITION(info>=0);
    }
}

RWBoolean FloatPDFact::fail() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}

RWBoolean FloatPDFact::isPD() const
{
    return (rows()>0 && info!=0) ? FALSE : TRUE;
}  

RWBoolean FloatPDFact::isSingular() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}

Float FloatPDFact::condition() const
{
    Float Anormcopy = Anorm;      // Copy to stop warnings about ref parm
    long info;
    long n = rows();
    Float rcond;
    Float *work = new Float[4*n];   // Some routines only require 2*n, but what the hell
    long *work2 = new long [n];
    sppcon('U',n,(Float*)factorization.data(),Anormcopy,rcond,work,work2,info);
    delete [] work2;    
    delete [] work;
    RWPOSTCONDITION(info==0);
    return rcond;
}

FloatVec FloatPDFact::solve( const FloatVec& b ) const
{
  FloatGenMat B(b,b.length(),1);
  FloatGenMat X = solve(B);
  RWPOSTCONDITION(X.cols()==1);
  return X.col(0);
}

FloatGenMat FloatPDFact::solve( const FloatGenMat& B ) const
{
    FloatGenMat X(B.copy());
    long n = rows();
    if (B.rows()!=n) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,B.rows(),n)));
    if (fail()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTSOLVE)));
    long nrhs = X.cols();
    long info;
    if (n>0) {
    spptrs('U',n,nrhs,(Float*)factorization.data(),X.data(),n,info);
      RWPOSTCONDITION(info==0);
    }
    return X;    
}

Float FloatPDFact::determinant() const
{
    Float rootdet = 1.0;
    for(int i=rows(); i--;) { rootdet *= factorization.val(i,i); }  // using val() rather than just op()() shuts up a SUN warning
    return rootdet*rootdet;
}
              
FloatSymMat FloatPDFact::inverse( ) const
{
    if (rows()==0) { return factorization; }
    if (fail()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTSOLVE)));
    FloatSymMat soln(factorization);
    soln.deepenShallowCopy();
    long n = rows();
    long info;
    spptri('U',n,soln.data(),info);
    RWPOSTCONDITION(info==0);
    return soln;
}


/*
 * Global functions which provide alternate ways to call member functions.
 * These used to be inline, but this requires that you include more header
 * files on some compilers, and that is just not worth it.
 */
FloatVec solve(const FloatPDFact& A, const FloatVec& b) { return A.solve(b); }
FloatGenMat solve(const FloatPDFact& A, const FloatGenMat& b) { return A.solve(b); }
Float    determinant (const FloatPDFact& A)	      { return A.determinant(); }

FloatSymMat inverse(const FloatPDFact& A)      	      { return A.inverse(); }

Float condition(const FloatPDFact& A) { return A.condition(); }



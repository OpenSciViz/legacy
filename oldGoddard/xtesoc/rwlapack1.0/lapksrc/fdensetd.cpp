/*
 * Implementation of FloatSymDenseTriDiagDecomp
 *
 * Generated from template $Id: xdensetd.cpp,v 1.4 1993/10/11 21:48:08 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */ 

#include "rw/ftd.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/lapack.h"
#include "rw/fsymmat.h"

FloatSymDenseTriDiagDecomp::FloatSymDenseTriDiagDecomp(const FloatSymMat& A)
{
  if (A.rows()>0) {
    char uplo = 'U';   // Indicates to lapack that upper triangle is stored
    long n = A.rows();
    Qdata.reference(A.dataVec());
    Qdata.deepenShallowCopy();
    FloatVec D(A.rows(),rwUninitialized);
    FloatVec E(A.rows()-1,rwUninitialized);
    tau.reshape(A.rows());
    long info;
    ssptrd(uplo,n,(Float*)Qdata.data(),(Float*)D.data(),(Float*)E.data(),(Float*)tau.data(),info);
    RWPOSTCONDITION(info==0);
    set(D,E);
  }
}

FloatVec FloatSymDenseTriDiagDecomp::transform(const FloatVec& V) const
{
  return FloatSymTriDiagDecomp::transform(V);
}

FloatGenMat FloatSymDenseTriDiagDecomp::transform(const FloatGenMat& Cinput) const
{
  FloatGenMat C = Cinput.copy();
  C.deepenShallowCopy();
  if (C.rows()>0) {
    if (C.rows()!=rows()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,C.rows(),rows())));
    }
    char side = 'L';
    char uplo = 'U';
    char trans = 'N';
    long m = C.rows();
    long n = C.cols();
    Float *work = new Float[n];
    long info;
    sopmtr(side,uplo,trans,m,n,(Float*)Qdata.data(),(Float*)tau.data(),(Float*)C.data(),m,work,info);
    RWPOSTCONDITION(info==0);
    delete [] work;
  }
  return C;
}


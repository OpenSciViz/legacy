/*
 * Implementation of DComplexHermFact
 *
 * Generated from template $Id: xfct.cpp,v 1.6 1993/10/11 21:48:11 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/lapkdefs.h"
#include "rw/chermfct.h"
#include "rw/lapack.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"  
#include "rw/cgenmat.h"

const char* DComplexHermFact::className = "DComplexHermFact";

DComplexHermFact::~DComplexHermFact()
{
  delete [] pvts;
}

DComplexHermFact::DComplexHermFact()
{
  info = -1;
  pvts=0;
}

DComplexHermFact::DComplexHermFact( const DComplexHermFact& A )
  : factorization(A.factorization)
{
  info = A.info;
  Anorm = A.Anorm;
  unsigned n = A.rows();
  pvts = (n>0) ? new long [n] : 0;
  while(n--) {
    pvts[n] = A.pvts[n];
  }
}

DComplexHermFact::DComplexHermFact(const DComplexHermMat& A, RWBoolean estimateCondition)
  : factorization(A)  // Use reference semantics, followed by
                      // deepenShallowCopy(), to work around a BCC V3.0 bug
{
    factorization.deepenShallowCopy();
    pvts = 0;
    dofactor(estimateCondition);
}

void DComplexHermFact::factor( const DComplexHermMat& A, RWBoolean estimateCondition )
{
    DComplexHermMat Acopy = A.copy();
    factorization.reference(Acopy);
    dofactor(estimateCondition);
}

void DComplexHermFact::dofactor(RWBoolean estimateCondition)
{
   info=0;   // for size zero matrices, need to set this explicitly
   long n = rows();	

    // Error (sanity) checking
    RWPRECONDITION(factorization.dataVec().stride()==1);
    for(int r=rows(); r--;) {
      if (imag(factorization.val(r,r))!=0.0) {
	RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTHERM)));
      }
    }

    delete [] pvts;
    pvts = (n>0) ? new long [n] : 0;

    // If we care to ever find the condition number, we need to know
    // the 1-norm of the matrix A.  Compute it while the member data
    // factorization is still a copy of A, and not yet it's factorization.
    Anorm = -1; 
    if (n>0 && estimateCondition) {
      Double *work = new Double [n];
      Anorm = zlanhp('1','U',n,factorization.data(),work);
      delete [] work;
    }

    if (n>0) {
      zhptrf('U',n,factorization.data(),pvts,info);
      RWPOSTCONDITION(info>=0);
    }
}

RWBoolean DComplexHermFact::fail() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}


RWBoolean DComplexHermFact::isSingular() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}

Double DComplexHermFact::condition() const
{
    Double Anormcopy = Anorm;      // Copy to stop warnings about ref parm
    long info;
    long n = rows();
    Double rcond;
    DComplex *work = new DComplex[4*n];   // Some routines only require 2*n, but what the hell
    zhpcon('U',n,(DComplex*)factorization.data(),pvts,Anormcopy,rcond,work,info);
    delete [] work;
    RWPOSTCONDITION(info==0);
    return rcond;
}

DComplexVec DComplexHermFact::solve( const DComplexVec& b ) const
{
  DComplexGenMat B(b,b.length(),1);
  DComplexGenMat X = solve(B);
  RWPOSTCONDITION(X.cols()==1);
  return X.col(0);
}

DComplexGenMat DComplexHermFact::solve( const DComplexGenMat& B ) const
{
    DComplexGenMat X(B.copy());
    long n = rows();
    if (B.rows()!=n) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,B.rows(),n)));
    if (fail()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTSOLVE)));
    long nrhs = X.cols();
    long info;
    if (n>0) {
    zhptrs('U',n,nrhs,(DComplex*)factorization.data(),pvts,X.data(),n,info);
      RWPOSTCONDITION(info==0);
    }
    return X;    
}

DComplex DComplexHermFact::determinant() const
{
    // see linpack manual pg 5.16 for description of algorithm
    DComplex det = 1.0;
    for(int i=rows(); i--;) {
      RWPRECONDITION(pvts[i]!=0);
      if (pvts[i]>0) {
        det *= factorization.val(i,i);  // one by one block
      } else {                          // two by two block
        RWPRECONDITION(i>0 && pvts[i-1]==pvts[i]);
        DComplex a = factorization.val(i,i);  // .val stops sun warning
        DComplex b = factorization.val(i-1,i);
        DComplex c = factorization.val(i-1,i-1);
        DComplex blockdet = a*c-DComplex(norm(b),0); // see linpack manual for a better way that avoids overflow and (as much) roundoff
        det *= blockdet;
        i--;
      }
    }
    return det;
}
              
DComplexHermMat DComplexHermFact::inverse( ) const
{
    if (rows()==0) { return factorization; }
    if (fail()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTSOLVE)));
    DComplexHermMat soln(factorization);
    soln.deepenShallowCopy();
    long n = rows();
    long info;
    DComplex *work = new DComplex [n];  
    zhptri('U',n,soln.data(),pvts,work,info);
    delete [] work;
    RWPOSTCONDITION(info==0);
    return soln;
}


/*
 * Global functions which provide alternate ways to call member functions.
 * These used to be inline, but this requires that you include more header
 * files on some compilers, and that is just not worth it.
 */
DComplexVec solve(const DComplexHermFact& A, const DComplexVec& b) { return A.solve(b); }
DComplexGenMat solve(const DComplexHermFact& A, const DComplexGenMat& b) { return A.solve(b); }
DComplex    determinant (const DComplexHermFact& A)	      { return A.determinant(); }

DComplexHermMat inverse(const DComplexHermFact& A)      	      { return A.inverse(); }

Double condition(const DComplexHermFact& A) { return A.condition(); }



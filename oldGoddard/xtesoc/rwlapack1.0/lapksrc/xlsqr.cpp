> #################################################################
> # Least squares fitting using QR (actually QTZ) decomposition   #
> #                                                               #
> # $Header: /users/rcs/lapksrc/xlsqr.cpp,v 1.4 1993/10/11 21:48:14 alv Exp $
> #                                                               #
> # Copyright (1991) by Rogue Wave Software, Inc.  This software  #
> # is subject to copyright protection under the laws of the      #
> # United States and other countries.                            #
> #                                                               #
> # Written by Al Vermeulen.                                      #
> #                                                               #
> # Limited License.                                              #
> #################################################################
> #
> # $Log: xlsqr.cpp,v $
> # Revision 1.4  1993/10/11  21:48:14  alv
> # synch'ed with Math.h++ v5.1
> #
> # Revision 1.3  1993/07/08  23:26:13  alv
> # ported to SUN CC v3
> #
> # Revision 1.2  1993/07/05  19:19:33  alv
> # fixed up for complex numbers
> #
> # Revision 1.1  1993/05/18  17:00:02  alv
> # Initial revision
> #
>
> define <T>=RW_T_RW
> if <T>==Double
> define <P>=Double <tLetter>=d
> elif <T>==Float
> define <P>=Float <tLetter>=f
> elif <T>==DComplex
> define <P>=Double <tLetter>=c
> else
> err least square class: not defined for '<T>'
> endif
> 
/*
 * defs for <T>LeastSqQR
 *
 * Generated from template $Id: xlsqr.cpp,v 1.4 1993/10/11 21:48:14 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 */

#include "rw/<tLetter>lsqr.h"
#include "rw/lapkerr.h"
#include "rw/rwerr.h"

<T>LeastSqQR::<T>LeastSqQR()                                 : <T>CODecomp() {}
<T>LeastSqQR::<T>LeastSqQR(const <T>GenMat& A, double tol)   : <T>CODecomp(A,tol) {}
<T>LeastSqQR::<T>LeastSqQR(const <T>QRDecomp& A, double tol) : <T>CODecomp(A,tol) {}
<T>LeastSqQR::<T>LeastSqQR(const <T>CODecomp& A)             : <T>CODecomp(A) {}

<T>Vec <T>LeastSqQR::residual(const <T>Vec& data) const
{
  if (data.length()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,data.length(),rows())));
  <T>Vec d = QTx(data);
  d(RWSlice(0,rank())) = (<T>)0;
  return Qx(d);
}

<P> <T>LeastSqQR::residualNorm(const <T>Vec& data) const
{
  if (data.length()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,data.length(),rows())));
  <T>Vec d = QTx(data);
  <T>Vec d2 = d(RWToEnd(rank()));
  return l2Norm(d2);
}

<T>Vec <T>LeastSqQR::solve(const <T>Vec& data) const
{
  if (data.length()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,data.length(),rows())));
  <T>Vec d = QTx(data);
  <T>Vec d1 = d(RWSlice(0,rank()));
  <T>Vec c(cols(),rwUninitialized);
  c(RWSlice(0,rank())) = Tinvx(d1);
  c(RWToEnd(rank())) = (<T>)0;
  return Px(Zx(c));
} 

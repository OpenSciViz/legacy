/*
 * Implementation of DoublePDFact
 *
 * Generated from template $Id: xfct.cpp,v 1.6 1993/10/11 21:48:11 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/lapkdefs.h"
#include "rw/dpdfct.h"
#include "rw/lapack.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"  
#include "rw/dgenmat.h"

const char* DoublePDFact::className = "DoublePDFact";

DoublePDFact::~DoublePDFact()
{
}

DoublePDFact::DoublePDFact()
{
  info = -1;
}

DoublePDFact::DoublePDFact( const DoublePDFact& A )
  : factorization(A.factorization)
{
  info = A.info;
  Anorm = A.Anorm;
}

DoublePDFact::DoublePDFact(const DoubleSymMat& A, RWBoolean estimateCondition)
  : factorization(A)  // Use reference semantics, followed by
                      // deepenShallowCopy(), to work around a BCC V3.0 bug
{
    factorization.deepenShallowCopy();
    dofactor(estimateCondition);
}

void DoublePDFact::factor( const DoubleSymMat& A, RWBoolean estimateCondition )
{
    DoubleSymMat Acopy = A.copy();
    factorization.reference(Acopy);
    dofactor(estimateCondition);
}

void DoublePDFact::dofactor(RWBoolean estimateCondition)
{
   info=0;   // for size zero matrices, need to set this explicitly
   long n = rows();	

    // Error (sanity) checking
    RWPRECONDITION(factorization.dataVec().stride()==1);


    // If we care to ever find the condition number, we need to know
    // the 1-norm of the matrix A.  Compute it while the member data
    // factorization is still a copy of A, and not yet it's factorization.
    Anorm = -1; 
    if (n>0 && estimateCondition) {
      Double *work = new Double [n];
      Anorm = dlansp('1','U',n,factorization.data(),work);
      delete [] work;
    }

    if (n>0) {
      dpptrf('U',n,factorization.data(),info);
      RWPOSTCONDITION(info>=0);
    }
}

RWBoolean DoublePDFact::fail() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}

RWBoolean DoublePDFact::isPD() const
{
    return (rows()>0 && info!=0) ? FALSE : TRUE;
}  

RWBoolean DoublePDFact::isSingular() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}

Double DoublePDFact::condition() const
{
    Double Anormcopy = Anorm;      // Copy to stop warnings about ref parm
    long info;
    long n = rows();
    Double rcond;
    Double *work = new Double[4*n];   // Some routines only require 2*n, but what the hell
    long *work2 = new long [n];
    dppcon('U',n,(Double*)factorization.data(),Anormcopy,rcond,work,work2,info);
    delete [] work2;    
    delete [] work;
    RWPOSTCONDITION(info==0);
    return rcond;
}

DoubleVec DoublePDFact::solve( const DoubleVec& b ) const
{
  DoubleGenMat B(b,b.length(),1);
  DoubleGenMat X = solve(B);
  RWPOSTCONDITION(X.cols()==1);
  return X.col(0);
}

DoubleGenMat DoublePDFact::solve( const DoubleGenMat& B ) const
{
    DoubleGenMat X(B.copy());
    long n = rows();
    if (B.rows()!=n) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,B.rows(),n)));
    if (fail()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTSOLVE)));
    long nrhs = X.cols();
    long info;
    if (n>0) {
    dpptrs('U',n,nrhs,(Double*)factorization.data(),X.data(),n,info);
      RWPOSTCONDITION(info==0);
    }
    return X;    
}

Double DoublePDFact::determinant() const
{
    Double rootdet = 1.0;
    for(int i=rows(); i--;) { rootdet *= factorization.val(i,i); }  // using val() rather than just op()() shuts up a SUN warning
    return rootdet*rootdet;
}
              
DoubleSymMat DoublePDFact::inverse( ) const
{
    if (rows()==0) { return factorization; }
    if (fail()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTSOLVE)));
    DoubleSymMat soln(factorization);
    soln.deepenShallowCopy();
    long n = rows();
    long info;
    dpptri('U',n,soln.data(),info);
    RWPOSTCONDITION(info==0);
    return soln;
}


/*
 * Global functions which provide alternate ways to call member functions.
 * These used to be inline, but this requires that you include more header
 * files on some compilers, and that is just not worth it.
 */
DoubleVec solve(const DoublePDFact& A, const DoubleVec& b) { return A.solve(b); }
DoubleGenMat solve(const DoublePDFact& A, const DoubleGenMat& b) { return A.solve(b); }
Double    determinant (const DoublePDFact& A)	      { return A.determinant(); }

DoubleSymMat inverse(const DoublePDFact& A)      	      { return A.inverse(); }

Double condition(const DoublePDFact& A) { return A.condition(); }



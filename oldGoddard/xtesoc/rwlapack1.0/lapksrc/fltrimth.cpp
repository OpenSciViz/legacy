#if 0
/*
 * Generated from template $Id: xmth.cpp,v 1.2 1993/10/11 21:48:17 alv Exp $
 *
 * Math member functions for the matrix types
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */
#endif

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/fltrimat.h"
#include "rw/futrimat.h"

/*
 * This function returns a new matrix the same shape as the one 
 * passed in but with the data passed in.  This way we restrict
 * all the >if statements to a couple lines
 */
inline FloatLowerTriMat sameShapeMatrix( FloatVec& vec, const FloatLowerTriMat& A )
{
  return FloatLowerTriMat(vec,A.rows(),A.cols());
}


static void verifyMatch(const FloatLowerTriMat& A, const FloatLowerTriMat& B)
{
    if (A.rows()!=B.rows() || A.cols()!=B.cols())
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATSIZE,A.rows(),A.cols(),B.rows(),B.cols())));
}

RWBoolean FloatLowerTriMat::operator==(const FloatLowerTriMat& X)
{
    if (n!=X.n) return FALSE;
    return vec==X.vec;	// All elements in vec used in the matrix
}


FloatLowerTriMat& FloatLowerTriMat::operator+=(const FloatLowerTriMat& m)
{
  verifyMatch(*this,m);
  vec += m.vec;
  return *this;
}

FloatLowerTriMat& FloatLowerTriMat::operator-=(const FloatLowerTriMat& m)
{
  verifyMatch(*this,m);
  vec -= m.vec;
  return *this;
}

FloatLowerTriMat& FloatLowerTriMat::operator*=(const FloatLowerTriMat& m)
{
  verifyMatch(*this,m);
  vec *= m.vec;
  return *this;
}

FloatLowerTriMat& FloatLowerTriMat::operator*=(Float x)
{
  vec *= x;
  return *this;
}

FloatLowerTriMat& FloatLowerTriMat::operator/=(const FloatLowerTriMat& m)
{
  verifyMatch(*this,m);
  vec /= m.vec;
  return *this;
}

FloatLowerTriMat& FloatLowerTriMat::operator/=(Float x)
{
  vec /= x;
  return *this;
}




FloatLowerTriMat operator-(const FloatLowerTriMat& A)
{
  FloatVec temp(-(A.dataVec()));
  return sameShapeMatrix(temp,A);
}
  
FloatLowerTriMat operator+(const FloatLowerTriMat& A)
{
  return A;
}

FloatLowerTriMat operator*(const FloatLowerTriMat& A, const FloatLowerTriMat& B) {
	verifyMatch(A,B);
	FloatVec temp(A.dataVec()*B.dataVec());
	return sameShapeMatrix(temp,A);
}

FloatLowerTriMat operator/(const FloatLowerTriMat& A, const FloatLowerTriMat& B) {
	verifyMatch(A,B);
	FloatVec temp(A.dataVec()/B.dataVec());
	return sameShapeMatrix(temp,A);
}

FloatLowerTriMat operator+(const FloatLowerTriMat& A, const FloatLowerTriMat& B) {
	verifyMatch(A,B);
	FloatVec temp(A.dataVec()+B.dataVec());
	return sameShapeMatrix(temp,A);
}

FloatLowerTriMat operator-(const FloatLowerTriMat& A, const FloatLowerTriMat& B) {
	verifyMatch(A,B);
	FloatVec temp(A.dataVec()-B.dataVec());
	return sameShapeMatrix(temp,A);
}

FloatLowerTriMat operator*(const FloatLowerTriMat& A, Float x) {
	FloatVec temp(A.dataVec()*x);
	return sameShapeMatrix(temp,A);
}

/*
 * Out-of-line version for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
  FloatLowerTriMat operator*(Float x, const FloatLowerTriMat& A) { return A*x; }
#endif

FloatLowerTriMat operator/(const FloatLowerTriMat& A, Float x) {
	FloatVec temp(A.dataVec()/x);
	return sameShapeMatrix(temp,A);
}





FloatLowerTriMat abs(const FloatLowerTriMat& A) {
	FloatVec temp(abs(A.dataVec()));
	return sameShapeMatrix(temp,A);
}



FloatUpperTriMat transpose(const FloatLowerTriMat& A)
{
return FloatUpperTriMat(A.dataVec(),A.rows(),A.cols());
}

FloatVec product(const FloatLowerTriMat& A, const FloatVec& x)
{
    if (A.cols()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATVECPROD,A.className,A.rows(),A.cols(),x.length())));
    }
    FloatVec y(A.rows(),rwUninitialized);
    const FloatVec& v = A.dataVec();
    int n=A.rows();
    for( int i=0; i<n; i++ ) {
      y(i) = dot( v.slice(i*(i+1)/2,i+1,1), x.slice(0,i+1,1) );
    }
    return y;
}

FloatVec product(const FloatVec& x, const FloatLowerTriMat& A)
{
    if (A.rows()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECMATPROD,x.length(),A.className,A.rows(),A.cols())));
    }
    return product(transpose(A),x);
}



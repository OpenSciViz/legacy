#if 0
/*
 * Generated from template file $Id: xmat.cpp,v 1.4 1993/10/11 21:48:16 alv Exp $
 *
 * The main code file for FloatLowerTriMat
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */
#endif

#include "rw/fltrimat.h"

#include "rw/rstream.h"
#include "rw/rwfile.h"
#include "rw/vstream.h"
#include "rw/lapkerr.h"
#include "rw/rwerr.h"
#  ifdef __ZTC__
#    include <iomanip.hpp>
#  else
#    ifdef __GLOCK__
#      include <iomanip.hxx>
#    else
#      include <iomanip.h>
#    endif
#  endif
STARTWRAP
#include <stdio.h>
#include <ctype.h>
ENDWRAP

const char* FloatLowerTriMat::className = "FloatLowerTriMat";

/*
 * These inline functions
 * give the number of elements that need be stored to represent
 * the different types of matrices
 */

inline unsigned GenMatSize(unsigned n, unsigned m)  { return m*n; }
inline unsigned BandMatSize(unsigned n, unsigned b) { return n*b; }
inline unsigned SymBandMatSize(unsigned n, unsigned hb) { return n*(hb+1); }
inline unsigned HermBandMatSize(unsigned n, unsigned hb) { return n*(hb+1); }
inline unsigned SymMatSize(unsigned n)		    { return n*(n+1)/2; }
inline unsigned SkewMatSize(unsigned n)		    { return n*(n+1)/2; }
inline unsigned HermMatSize(unsigned n)		    { return n*(n+1)/2; }
inline unsigned UpperTriMatSize(unsigned n)  	    { return n*(n+1)/2; }
inline unsigned LowerTriMatSize(unsigned n)  	    { return n*(n+1)/2; }
inline unsigned TriDiagMatSize(unsigned n)	    { return 3*n; }

FloatLowerTriMat::~FloatLowerTriMat()
{
}

FloatLowerTriMat::FloatLowerTriMat()
  : vec()
{
    n=0;
}

FloatLowerTriMat::FloatLowerTriMat( const FloatLowerTriMat& A )
  : vec(A.vec)
{
    n=A.n;
}

FloatLowerTriMat::FloatLowerTriMat(unsigned M, unsigned N)
  : vec( LowerTriMatSize(N),rwUninitialized )
{
  if (M!=N) { RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTSQUARE,int(M),int(N)))); }
  n=N;
}


FloatLowerTriMat::FloatLowerTriMat(const FloatVec& data, unsigned M, unsigned N)
  : vec( data )
{
  if (M!=N) { RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTSQUARE,int(M),int(N)))); }
  n=N;
  if (vec.length()!=LowerTriMatSize(N))
    RWTHROW(RWInternalErr(RWMessage(RWLAPK_NUMBERPOINTS,vec.length(),n,n,className)));
}


Float FloatLowerTriMat::bcval(int i, int j) const
{
    if ( i<0 || i>=rows() || j<0 || j>=cols() ) {
      RWTHROW(RWBoundsErr(RWMessage(RWLAPK_OUTOFBOUNDS,i,j,rows(),cols())));
    }
    return (i<j) ? (Float)(0) : vec(i*(i+1)/2+j);
}

Float FloatLowerTriMat::bcset(int i, int j, Float x)
{
    if ( i<0 || i>=rows() || j<0 || j>=cols() ) {
      RWTHROW(RWBoundsErr(RWMessage(RWLAPK_OUTOFBOUNDS,i,j,rows(),cols())));
    }
    if (i<j) RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTSETABLE,i,j,className)));
    return vec(i*(i+1)/2+j)=x; 
}

ROFloatRef FloatLowerTriMat::bcref(int i, int j)
{
    if ( i<0 || i>=rows() || j<0 || j>=cols() ) {
      RWTHROW(RWBoundsErr(RWMessage(RWLAPK_OUTOFBOUNDS,i,j,rows(),cols())));
    }
    return (i<j) ? ROFloatRef(rwFloatZero,TRUE)
		 : ROFloatRef(vec(i*(i+1)/2+j),FALSE);
}


/*
 * Here are non-inline versions of the row(), col(), and diagonal()
 * routines.  Normally these would be inlined, but due to a bug
 * in the Borland C++ compiler it is best not to inline them with
 * Borland C++.
 *
 * These routines just call the bounds checking routines.
 */

/*
 * The leadingSubmatrix function
 */

FloatLowerTriMat FloatLowerTriMat::leadingSubmatrix(int k)
{
  if (k<0 || k>n) RWTHROW(RWBoundsErr(RWMessage(RWLAPK_LEADINGSUBMATRIXORDER,k,n)));
  return FloatLowerTriMat( vec.slice(0,LowerTriMatSize(k)), k, k );
}

FloatLowerTriMat& FloatLowerTriMat::operator=(const FloatLowerTriMat& M)
{
    if (rows()!=M.rows() || cols()!=M.cols()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATSIZE,M.rows(),M.cols(),rows(),cols())));
    }
    vec = M.vec;
    return *this;
}

FloatLowerTriMat& FloatLowerTriMat::reference(FloatLowerTriMat& A)
{
    vec.reference(A.vec);
    n=A.n;
    return *this;
}

FloatLowerTriMat FloatLowerTriMat::copy() const
{
  FloatLowerTriMat A( *((FloatLowerTriMat*)this) );	// cast this to non-const to make cfront happy
  A.deepenShallowCopy();
  return A;
}

void FloatLowerTriMat::resize(unsigned M, unsigned N)
{
    if (M!=N) { RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTSQUARE,int(M),int(N)))); }
    n=N;
    vec.resize(LowerTriMatSize(n));

}



/*
 * printOn,scanFrom:  printOn prints all non-numeric stuff first
 * then prints the numbers defining the shape and size of the matrix,
 * then prints the matrix itself.  The format used by printOn can be
 * used to read in a matrix with scanFrom.
 *
 * scanFrom first eats all the non-numeric characters it encounters.
 * This takes care of the stuff printOn spews before the data.  Next
 * it reads the numbers which define the matrix shape and then the
 * data is read in.
 */

void FloatLowerTriMat::printOn(ostream& outs) const
{
    int w = outs.width(0);
    int m=n;
      outs << className << ", ";
    outs << rows() << "x" << cols() << " [\n";
    for( int i=0; i<m; i++ ) {
      for( int j=0; j<n; j++ ) {
	outs << setw(w) << val(i,j) << " ";
      }
      outs << "\n";
    }
    outs << "]";
}

void FloatLowerTriMat::scanFrom(istream& s)
{
  char c;
  unsigned numRows, numCols;

  /* Skip through leading non-digits */
  do { s.get(c); } while (!s.fail() && !isdigit(c));
  s.putback(c);


  s >> numRows;
  do { s.get(c); } while (isspace(c));	// Skip optional x character
  if (isdigit(c)) s.putback(c);	
  s >> numCols;

  FloatVec v;			// Read the data
  v.scanFrom(s);

  if (!s.fail()) { 		// Now put the data into the matrix
    if (v.length()!=numRows*numCols) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_WRONGNUMPOINTS,v.length(),className,numRows,numCols)));
    } else {
      resize(numRows,numCols);
      int index = 0;
      for(int i=0; i<numRows; i++) {
        for(int j=0; j<numCols; j++) {
	  if (val(i,j)!=v[index]) bcset(i,j,v[index]);
	  index++;
	}
      }
    }
  }
}

void FloatLowerTriMat::saveOn(RWFile& file) const
{
    /* First write the numbers which define the shape of the matrix */
    file.Write(n);
    vec.saveOn(file);
}

void FloatLowerTriMat::saveOn(RWvostream& s) const
{
    /* First write the numbers which define the shape of the matrix */
    s << n;
    vec.saveOn(s);
}

void FloatLowerTriMat::restoreFrom(RWFile& file)
{
    /* First read the numbers which define the shape of the matrix */
    file.Read(n);
    int size = LowerTriMatSize(n);
    vec.restoreFrom(file);
    if (vec.length() != size) RWTHROW(RWInternalErr(RWMessage(RWLAPK_RESTORESIZE,className,vec.length(),size)));
}

void FloatLowerTriMat::restoreFrom(RWvistream& s)
{
    /* First read the numbers which define the shape of the matrix */
    s >> n;
    int size = LowerTriMatSize(n);
    vec.restoreFrom(s);
    if (vec.length() != size) RWTHROW(RWInternalErr(RWMessage(RWLAPK_RESTORESIZE,className,vec.length(),size)));
}

unsigned FloatLowerTriMat::binaryStoreSize() const
{
    /* First determine the size of the stuff
       which determines the matrix shape.    */
    unsigned size = sizeof(unsigned);    // n
    return size + vec.binaryStoreSize();
}

/*
 * Implementation of DoublePDTriDiagFact
 *
 * Generated from template $Id: xfct.cpp,v 1.6 1993/10/11 21:48:11 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/lapkdefs.h"
#include "rw/dpdtdfct.h"
#include "rw/lapack.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"  
#include "rw/dgenmat.h"

const char* DoublePDTriDiagFact::className = "DoublePDTriDiagFact";

DoublePDTriDiagFact::~DoublePDTriDiagFact()
{
}

DoublePDTriDiagFact::DoublePDTriDiagFact()
{
  info = -1;
}

DoublePDTriDiagFact::DoublePDTriDiagFact( const DoublePDTriDiagFact& A )
  : d(A.d), e(A.e)
{
  info = A.info;
  Anorm = A.Anorm;
}

DoublePDTriDiagFact::DoublePDTriDiagFact(const DoubleTriDiagMat& A, RWBoolean estimateCondition)
  // Initialize in the body so we can be careful of negative lengths
{
    RWPRECONDITION(A.diagonal(-1)==A.diagonal(1));
    d.reference(A.diagonal().copy());
    if (A.rows()>1) {
      e.reference(A.diagonal(1).copy());
    } 
    dofactor(estimateCondition);
}

void DoublePDTriDiagFact::factor( const DoubleTriDiagMat& A, RWBoolean estimateCondition )
{
    RWPRECONDITION(A.diagonal(-1)==A.diagonal(1));
    d.reference(A.diagonal().copy());
    if (A.rows()>1) {
      e.reference(A.diagonal(1).copy());
    } else {
      e.reshape(0);
    }
    dofactor(estimateCondition);
}

void DoublePDTriDiagFact::dofactor(RWBoolean estimateCondition)
{
   info=0;   // for size zero matrices, need to set this explicitly
   long n = rows();	

    // Error (sanity) checking
    RWPRECONDITION(factorization.dataVec().stride()==1);


    // If we care to ever find the condition number, we need to know
    // the 1-norm of the matrix A.  Compute it while the member data
    // factorization is still a copy of A, and not yet it's factorization.
    Anorm = -1; 
    if (n>0 && estimateCondition) {
      Anorm = dlanst('1',n,d.data(),e.data());
    }

    if (n>0) {
      dpttrf(n,d.data(),e.data(),info);
      RWPOSTCONDITION(info>=0);
    }
}

RWBoolean DoublePDTriDiagFact::fail() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}

RWBoolean DoublePDTriDiagFact::isPD() const
{
    return (rows()>0 && info!=0) ? FALSE : TRUE;
}  

RWBoolean DoublePDTriDiagFact::isSingular() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}

Double DoublePDTriDiagFact::condition() const
{
    Double Anormcopy = Anorm;      // Copy to stop warnings about ref parm
    long info;
    long n = rows();
    Double rcond;
    Double *work = new Double[4*n];   // Some routines only require 2*n, but what the hell
      dptcon(n,(Double*)d.data(),(Double*)e.data(),Anormcopy,rcond,work,info);
    delete [] work;
    RWPOSTCONDITION(info==0);
    return rcond;
}

DoubleVec DoublePDTriDiagFact::solve( const DoubleVec& b ) const
{
  DoubleGenMat B(b,b.length(),1);
  DoubleGenMat X = solve(B);
  RWPOSTCONDITION(X.cols()==1);
  return X.col(0);
}

DoubleGenMat DoublePDTriDiagFact::solve( const DoubleGenMat& B ) const
{
    DoubleGenMat X(B.copy());
    long n = rows();
    if (B.rows()!=n) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,B.rows(),n)));
    if (fail()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTSOLVE)));
    long nrhs = X.cols();
    long info;
    if (n>0) {
    dpttrs(n,nrhs,(Double*)d.data(),(Double*)e.data(),X.data(),n,info);  
      RWPOSTCONDITION(info==0);
    }
    return X;    
}

Double DoublePDTriDiagFact::determinant() const
{
    return prod(d);
}
              

/*
 * Global functions which provide alternate ways to call member functions.
 * These used to be inline, but this requires that you include more header
 * files on some compilers, and that is just not worth it.
 */
DoubleVec solve(const DoublePDTriDiagFact& A, const DoubleVec& b) { return A.solve(b); }
DoubleGenMat solve(const DoublePDTriDiagFact& A, const DoubleGenMat& b) { return A.solve(b); }
Double    determinant (const DoublePDTriDiagFact& A)	      { return A.determinant(); }


Double condition(const DoublePDTriDiagFact& A) { return A.condition(); }



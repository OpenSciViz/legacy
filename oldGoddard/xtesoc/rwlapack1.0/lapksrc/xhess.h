> #################################################################
> # The template file for Hessenberg decomposition class          #
> #                                                               #
> # $Header: /users/rcs/lapksrc/xhess.h,v 1.1 1993/05/18 17:00:02 alv Exp $
> #                                                               #
> # Copyright (1993) by Rogue Wave Software, Inc.  This           #
> # software is subject to copyright protection under the laws    #
> # of the United States and other countries.                     #
> #                                                               #
> # Written by Al Vermeulen.                                      #
> #                                                               #
> # Limited License.                                              #
> #################################################################
> #
> # $Log: xhess.h,v $
> # Revision 1.1  1993/05/18  17:00:02  alv
> # Initial revision
> #
>
> # Macros that we need
> 
> define <T>=RW_T_RW
> 
> if <T>==Double
> define <P>=Double <tLetter>=d
> elif <T>==Float
> define <P>=Float <tLetter>=f 
> elif <T>==DComplex
> define <P>=Double <tLetter>=c
> else
> err Hessenberg decomposition: not defined for '<T>'
> endif
>
> beginwrapper <tLetter>hess
>
/*
 * <T>HessenbergDecomp - Hessenberg decomposition of a matrix
 *
 * Generated from template $Id: xhess.h,v 1.1 1993/05/18 17:00:02 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 * This decomposition is usually formed as a percusor to computing
 * the eigenvalues and/or Schur decomposition of a non-symmetric matrix.
 *
 * This class includes an optional balancing transformation in addition
 * to the standard Hessenberg decomposition.
 */ 

#include "rw/lapkdefs.h"
#include "rw/<tLetter>vec.h"
#include "rw/<tLetter>genmat.h"
#include "rw/<tLetter>bal.h"
#include "rw/lapack.h"

class <T>HessenbergDecomp {
private:
  <T>BalanceTransform B_;  // Balancing transformation
  <T>GenMat           H_;  // Hessenberg matrix and Q info
  <T>Vec              tau_;// scalar reflectors for generating Q

public:
  <T>HessenbergDecomp();
  <T>HessenbergDecomp(const <T>GenMat&, RWBoolean permute=TRUE, RWBoolean scale=TRUE);
  <T>HessenbergDecomp(const <T>BalanceDecomp&);
  void factor(const <T>GenMat&, RWBoolean permute=TRUE, RWBoolean scale=TRUE);
  void factor(const <T>BalanceDecomp&);
  unsigned            cols()                  const {return H_.cols();}
  unsigned            rows()                  const {return H_.rows();}
  <T>GenMat           B()                     const;
  <T>BalanceTransform balanceTransform()      const {return B_;}  // An alternate representation of B
  <T>GenMat           Q()                     const;
  <T>GenMat           H()                     const;
  <T>Vec              Bx(const <T>Vec&)       const;
  <T>Vec              BInvTx(const <T>Vec&)   const;
  <T>Vec              Qx(const <T>Vec&)       const;
  <T>Vec              QTx(const <T>Vec&)      const;
  <T>GenMat           BX(const <T>GenMat&)    const;
  <T>GenMat           BInvTX(const <T>GenMat&)const;
  <T>GenMat           QX(const <T>GenMat&)    const;
  <T>GenMat           QTX(const <T>GenMat&)   const;
};

> endwrapper

#!/bin/sh
#
#	A Korn shell script to generate the template expansion rules for
#	the lapack.h++ makefile template.
#
###########################################################################
#
#	$Header: /users/rcs/lapksrc/tempexp.ksh,v 1.3 1993/07/05 19:19:33 alv Exp $
#
#	Copyright (1993) by Rogue Wave Software, Inc.  This software
#	is subject to copyright protection under the laws of the
#	United States and other countries.
#
#
###########################################################################
#
#	$Log: tempexp.ksh,v $
# Revision 1.3  1993/07/05  19:19:33  alv
# fixed up for complex numbers
#
# Revision 1.2  1993/06/23  18:36:03  alv
# added matrix classes
#
#
f=tempexp.tm		# Output file

dtype=Double		# Use type=`eval echo \$${i}type` to translate
ftype=Float		# d,c,f to Double, DComplex, Float
ctype=DComplex
itype=Int
uctype=UChar
sctype=SChar

gen=Gen
sym=Sym
herm=Herm
band=Band
pd=PD
pdbd=PDBand
trdg=TriDiag
pdtd=PDTriDiag
skew=Skew
sbnd=SymBand
hbnd=HermBand
utri=UpperTri
ltri=LowerTri

rm -f $f

echo '> #   D O   N O T   E D I T   T H I S   F I L E'            >> $f
echo '>'                                                          >> $f
echo '> # This file is created by running the script tempexp.ksh' >> $f
echo '>'                                                          >> $f
echo                                                              >> $f

# The ref.h files
#
for i in d f c ; do
	type=`eval echo \\\$${i}type`
	echo "\$(RWDIR)/${i}ref.h: xref.h macros" >> $f
	echo '	$(MATHPP) xref.h '"\$(RWDIR)/${i}ref.h RW_C_RW=${type}SymMat" >> $f
done

# The rules to build header and source files for all the matrix
# types.
#
for shape in gen sym herm skew band sbnd hbnd utri ltri trdg ; do
    SHAPE=`eval echo \\\$${shape}`	# Convert from (eg) sbnd to SymBand
    if   test $shape = "gen"  ; then prefixes="i sc uc d f c"; srcdir='$(MATHSRC)/'
    elif test $shape = "herm" ; then prefixes="c";             srcdir=''
    elif test $shape = "hbnd" ; then prefixes="c";             srcdir=''
    else                             prefixes="d f c";         srcdir=''
    fi
    for i in $prefixes ; do
	type=`eval echo \\\$${i}type`	# Convert from (eg) f to Float

        echo "Writing lines for ${type}${SHAPE}Mat"

	# Header files
	#
	echo "\$(RWDIR)/${i}${shape}mat.h: xmat.h macros" >> $f
	echo "	\$(MATHPP) xmat.h \$@ RW_C_RW=${type}${SHAPE}Mat" >> $f
	#
	# Code files (mat.cpp, mth.cpp, pik.cpp, ct.cpp, cs.cpp, cg.cpp)
	#
	echo "${srcdir}${i}${shape}mat.cpp: xmat.cpp macros" >> $f
	echo "	\$(MATHPP) xmat.cpp \$@ RW_C_RW=${type}${SHAPE}Mat" >> $f
	#
	echo "${srcdir}${i}${shape}mth.cpp: xmth.cpp macros" >> $f
	echo "	\$(MATHPP) xmth.cpp \$@ RW_C_RW=${type}${SHAPE}Mat" >> $f
	#
	echo "${srcdir}${i}${shape}ct.cpp: xct.cpp macros" >> $f
	echo "	\$(MATHPP) xct.cpp \$@ RW_C_RW=${type}${SHAPE}Mat" >> $f
	#
	echo "${srcdir}${i}${shape}cs.cpp: xcs.cpp macros" >> $f
	echo "	\$(MATHPP) xcs.cpp \$@ RW_C_RW=${type}${SHAPE}Mat" >> $f
	#
	echo "${srcdir}${i}${shape}cg.cpp: xcg.cpp macros" >> $f
	echo "	\$(MATHPP) xcg.cpp \$@ RW_C_RW=${type}${SHAPE}Mat" >> $f
		
    done;
done;

# The rules to build header and source files for all the factorization
# types.
#
for shape in gen sym herm pd band pdbd trdg pdtd; do
    SHAPE=`eval echo \\\$${shape}`	# Convert from (eg) sbnd to SymBand
    if test $shape = "herm" ; then prefixes="c";
    else                           prefixes="d f c";
    fi
    for i in $prefixes ; do
	type=`eval echo \\\$${i}type`	# Convert from (eg) f to Float

        echo "Writing lines for ${type}${SHAPE}Fact"

	# Header files
	#
	echo "\$(RWDIR)/${i}${shape}fct.h: xfct.h fct.mac" >> $f
	echo "	\$(MATHPP) xfct.h \$@ RW_C_RW=${type}${SHAPE}Fact" >> $f

	# Code files (fct.cpp)
	#
	echo "${srcdir}${i}${shape}fct.cpp: xfct.cpp fct.mac" >> $f
	echo "	\$(MATHPP) xfct.cpp \$@ RW_C_RW=${type}${SHAPE}Fact" >> $f

    done;
done;

# Eigenvalue and decomposition rules (real case)
#
for i in d f c; do
    type=`eval echo \\\$${i}type`	# Convert from (eg) f to Float
    echo "Writing lines for $type classes"

    # Header files
    for j in symeig td seigsrv qr co sv lsqr lssv lsch bal hess schur eig eigsrv; do
	echo "\$(RWDIR)/${i}${j}.h: x${j}.h" >> $f
	echo "	\$(MATHPP) x${j}.h \$@ RW_T_RW=${type}" >> $f
    done

    # Code files
    for j in symeig seigsrv seigqr seigpd seigrf seigbis td densetd bandtd qr co sv lsqr lssv lsch bal hess schur eig eigsch eighess; do
        echo "${i}${j}.cpp: x${j}.cpp" >> $f
        echo "	\$(MATHPP) x${j}.cpp \$@ RW_T_RW=${type}" >> $f
    done
done

exit 0;

> ###############################################################
> # The conversion functions to and from general matrices       #
> #                                                             #
> # $Header: /users/rcs/lapksrc/xcg.cpp,v 1.2 1993/07/05 19:19:33 alv Exp $
> #                                                             #
> # Every shape of matrix can be converted to and from a general#
> # matrix.  These functions are in this file.  The functions   #
> # converting to a general matrix are constructors of GenMat.  #
> # The ones converting a general matrix to other forms are     #
> # the toXxxx functions.                                       #
> #                                                             #
> # Copyright (1991) by Rogue Wave Software, Inc.  This software#
> # is subject to copyright protection under the laws of the    #
> # United States and other countries.                          #
> #                                                             #
> # Written by Al Vermeulen.                                    #
> #                                                             #
> # Limited License.                                            #
> ###############################################################
> #
> # $Log: xcg.cpp,v $
> # Revision 1.2  1993/07/05  19:19:33  alv
> # fixed up for complex numbers
> #
> # Revision 1.1  1993/06/23  18:28:03  alv
> # Initial revision
> #
> # Revision 1.4  1993/03/15  16:04:44  alv
> # rewrote toSymMat and toHermMat for Metaware High C++
> #
> # Revision 1.3  1993/03/11  18:51:15  alv
> # fixed bug in toHermMat
> #
> # Revision 1.2  1993/03/04  20:22:14  alv
> # fixed bug in HermBand -> general conversion
> #
> # Revision 1.1  1993/01/23  00:01:09  alv
> # Initial revision
> #
> # 
> #    Rev 1.5   17 Oct 1991 09:22:26   keffer
> # Changed include path to <rw/xxx.h>
> # 
> #    Rev 1.4   25 Sep 1991 14:59:24   keffer
> # Ported to Zortech V3.0
> # 
> #    Rev 1.3   01 Sep 1991 09:16:24   keffer
> # #include "rw/defs.h" to avoid Glock preprocessor nesting limits
> # 
> #    Rev 1.2   27 Aug 1991 11:47:20   keffer
> # Removed single argument constructors to avoid type conversion problems
> # 
> #    Rev 1.1   22 Aug 1991 09:38:20   keffer
> # Ported to cfront
> # 
>
> include macros
>
> if <M>==GenMat
> err No GenMat <--> GenMat conversion functions!
> endif
>
/*
 * The conversion functions to/from general matrices.
 *
 * Generated from template $Id: xcg.cpp,v 1.2 1993/07/05 19:19:33 alv Exp $
 *
> copyright
 */

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/<tLetter><shapeAbbrev>mat.h"
#include "rw/<tLetter>genmat.h"

> if <M>==SymMat
>
<T>GenMat::<T>GenMat( const <T>SymMat& A )
  : RWMatView(A.rows(),A.cols(),sizeof(<T>))
{
  for( int j=0; j<nrows; j++ ) {
    for( register int i=0; i<=j; i++ ) {
      <T> x = A.val(i,j);
      set(i,j,x);
      set(j,i,x);
    }
  }
}

<T>SymMat toSymMat( const <T>GenMat& S )
{
  unsigned n = S.cols();
  <T>SymMat A(n,n);
  for( int j=0; j<n; j++ ) {
    for( int i=0; i<=j; i++ ) {                   // This loop should be in a bla call for nonsmall j
#ifdef RW_HIGHC_INLINE_BUG
      <T> temp1 = S.val(i,j);      // High C++ needs these temporaries 
      <T> temp2 = S.val(j,i);
> if <R/C>==Complex
      A(i,j) = (temp1+temp2)/<T>(2,0);	// Explicit constructor necessary for ZTC
> else
      A(i,j) = (temp1+temp2)/2;
> endif
#else
> if <R/C>==Complex
      A(i,j) = (S.val(i,j)+S.val(j,i))/<T>(2,0);	// Explicit constructor necessary for ZTC
> else
      A(i,j) = (S.val(i,j)+S.val(j,i))/2;
> endif
#endif
    }
  }
  return A;
}

<T>SymMat upperToSymMat( const <T>GenMat& S )
{
  unsigned n = S.cols();
  <T>SymMat A(n,n);
  for(int j=0; j<n; j++) {
    for(int i=0; i<=j; i++) {                     // This loop should be in a bla call for nonsmall j
      A(i,j) = S.val(i,j);
    }
  }
  return A;
}

<T>SymMat lowerToSymMat( const <T>GenMat& S )
{
  unsigned n = S.cols();
  <T>SymMat A(n,n);
  for(int j=0; j<n; j++) {
    for(int i=0; i<=j; i++) {                     // This loop should be in a bla call for nonsmall j
      A(i,j) = S.val(j,i);
    }
  }
  return A;
}

>
> elif <M>==SkewMat
>
<T>GenMat::<T>GenMat( const <T>SkewMat& A )
  : RWMatView(A.rows(),A.cols(),sizeof(<T>))
{
  for( int j=0; j<A.rows(); j++ ) {
    for( register int i=0; i<=j; i++ ) {
      <T> x = A.val(i,j);
      set(j,i,-x);
      set(i,j,x);
    }
  }
}

<T>SkewMat toSkewMat( const <T>GenMat& S, RWBoolean keepMainDiag)
{
  int n = S.cols();
  <T>SkewMat A(S.rows(),n);
> if <R/C>==Complex
   // Use bcset rather than set in the following.  set generates
   // inlines which cause cfront to spew things too complicated for
   // the DEC MIPS C compiler
   for( int j=0; j<n; j++ ) {
     for( int i=0; i<j; i++ ) {
       A.bcset(i,j,(S.val(i,j)-S.val(j,i))/<T>(2,0));	// Explicit constructor necessary for Zortech
     }
     if (keepMainDiag) A.bcset(j,j,S.val(j,j));
     else              A.bcset(j,j,<zero>);		// Explicit constructor necessary for Zortech
   }
> elif <R/C>==Real
   for( int j=0; j<n; j++ ) {
     for( int i=0; i<j; i++ ) {
       A.set(i,j,(S.val(i,j)-S.val(j,i))/2);
     }
     if (keepMainDiag) A.set(j,j,S.val(j,j));
     else              A.set(j,j,0);
   }
> else 
>  err <R/C> not Complex or Real
> endif
   return A;
}

>
> elif <M>==HermMat
>
<T>GenMat::<T>GenMat( const <T>HermMat& A )
  : RWMatView(A.rows(),A.cols(),sizeof(<T>))
{
  for( int j=0; j<A.rows(); j++ ) {
    for( register int i=0; i<=j; i++ ) {
      <T> x = A.val(i,j);
      set(j,i,conj(x));
      set(i,j,x);
    }
  }
}

<T>HermMat toHermMat( const <T>GenMat& S )
{
  int n = S.cols();
  <T>HermMat A(S.rows(),n);
  for( int j=0; j<n; j++ ) {
    for( int i=0; i<j; i++ ) {
      // Using set in the following generates an inline too complicated
      // for the C compiler on the DEC (ie cfront spits out something
      // too hard to understand).
      // A.set(i,j,(S.val(i,j)+conj(S.val(j,i)))/(<T>)2);
#ifdef RW_HIGHC_INLINE_BUG
      <T> temp1 = S.val(i,j);
      <T> temp1 = S.val(j,i);
      A.bcset(i,j,(temp1+conj(temp2))/<T>(2,0));
#else
      A.bcset(i,j,(S.val(i,j)+conj(S.val(j,i)))/<T>(2,0));
#endif
    }
    A.bcset(j,j,S.val(j,j));   // We don't force imaginary part to zero
  }
  return A;
}

<T>HermMat upperToHermMat( const <T>GenMat& S )
{
  unsigned n = S.cols();
  <T>HermMat A(n,n);
  for(int j=0; j<n; j++) {
    for(int i=0; i<=j; i++) {                     // This loop should be in a bla call for nonsmall j
      A.set(i,j,S.val(i,j));
    }
  }
  return A;
}

<T>HermMat lowerToHermMat( const <T>GenMat& S )
{
  unsigned n = S.cols();
  <T>HermMat A(n,n);
  for(int j=0; j<n; j++) {
    for(int i=j; i<n; i++) {                     // This loop should be in a bla call for nonsmall j
      A.set(i,j,S.val(i,j));
    }
  }
  return A;
}

>
> elif <M>==BandMat
>
<T>GenMat::<T>GenMat( const <T>BandMat& A )
  : RWMatView(A.rows(),A.cols(),sizeof(<T>))
{
  zero();
  for( int k= -(int)A.lowerBandwidth(); k<=(int)A.upperBandwidth(); k++ ) {
    diagonal(k) = A.diagonal(k);
  }
}

<T>BandMat toBandMat( const <T>GenMat& S, unsigned bandl, unsigned bandu )
{
  int n = S.cols();
  <T>BandMat A( S.rows(), n, bandl, bandu );
  for( int k= -(int)bandl; k<=(int)bandu; k++ ) {
    A.diagonal(k) = S.diagonal(k);
  }
  return A;
}

>
> elif <M>==SymBandMat
>
<T>GenMat::<T>GenMat( const <T>SymBandMat& A )
  : RWMatView(A.rows(),A.cols(),sizeof(<T>))
{
  zero();
  for( int k=A.halfBandwidth(); k>=0; k-- ) {
    <T>Vec d = A.diagonal(k);
    diagonal(-k) = d;
    diagonal(k) = d;
  }
}

<T>SymBandMat toSymBandMat( const <T>GenMat& S, unsigned bandu )
{
  int n = S.cols();
  <T>SymBandMat A( S.rows(), n, bandu );
  A.diagonal(0) = S.diagonal(0);
  for( int k=1; k<=bandu; k++ ) {
    <T>Vec Adiag = A.diagonal(k);
    Adiag = S.diagonal(k);
    Adiag += S.diagonal(-k);
> if <R/C>==Complex
    Adiag /= <T>(2,0);		// Explicit constructor necessary for Zortech
> else
    Adiag /= 2;
> endif
  }
  return A;
}

> elif <M>==HermBandMat
>
<T>GenMat::<T>GenMat( const <T>HermBandMat& A )
  : RWMatView(A.rows(),A.cols(),sizeof(<T>))
{
  zero();
  int hb = A.halfBandwidth();
  for( int k= hb; k>=0; k-- ) {
    <T>Vec d = A.dataVec().slice((k+1)*hb,nrows-k,hb+1);
    if (k>0) diagonal(-k) = conj(d);
    diagonal(k) = d;
  }
}

<T><M> toHermBandMat( const <T>GenMat& S, unsigned bandu )
{
  int n = S.cols();
  <T>HermBandMat A( S.rows(), n, bandu );
  A.dataVec().slice(bandu,n,bandu+1) = S.diagonal(0);
  for( int k=1; k<=bandu; k++ ) {
    <T>Vec Adiag = A.dataVec().slice(bandu+k*bandu,n-k,bandu+1);
    Adiag = S.diagonal(k);
    Adiag += conj(S.diagonal(-k));
> if <R/C>==Complex
    Adiag /= <T>(2,0);
> else
    Adiag /= 2;
> endif
  }
  return A;
}

> 
> elif <M>==UpperTriMat
>
<T>GenMat::<T>GenMat( const <T>UpperTriMat& A )
  : RWMatView(A.rows(),A.cols(),sizeof(<T>))
{
  zero();
  for( int j=0; j<nrows; j++ ) {
    for( int i=0; i<=j; i++ ) {
      set(i,j,A.val(i,j));
    }
  }
}

<T>UpperTriMat toUpperTriMat( const <T>GenMat& S )
{
  int n = S.cols();
  <T>UpperTriMat A(S.rows(),n);
  for( int j=0; j<n; j++ ) {
    for( int i=0; i<=j; i++ ) {
      A.set(i,j,S.val(i,j));
    }
  }
  return A;
}

> 
> elif <M>==LowerTriMat
>
<T>GenMat::<T>GenMat( const <T>LowerTriMat& A )
  : RWMatView(A.rows(),A.cols(),sizeof(<T>))
{
  /* We can't win.  If we go through A in order of element storage we
   * have to skip around through *this, and vice versa.  We'll go 
   * through A in order and skip around through *this so the code looks
   * as much as possible like the UpperTriMat code.
   * Hmmm, now with version 5 of math.h++ we could win by making the
   * general matrix in row major order.  Maybe I'll change the code
   * someday.
   */
  zero();
  for( int i=0; i<nrows; i++ ) {
    for( int j=0; j<=i; j++ ) {
      set(i,j,A.val(i,j));
    }
  }
}

<T>LowerTriMat toLowerTriMat( const <T>GenMat& S )
{
  int n = S.cols();
  <T>LowerTriMat A(S.rows(),n);
  for( int i=0; i<n; i++ ) {
    for( int j=0; j<=i; j++ ) {
      A.set(i,j,S.val(i,j));
    }
  }
  return A;
}

>
> elif <M>==TriDiagMat
>
<T>GenMat::<T>GenMat( const <T>TriDiagMat& A )
  : RWMatView(A.rows(),A.cols(),sizeof(<T>))
{
  zero();
  if (nrows>0) { diagonal(0) = A.diagonal(0); }
  if (nrows>1) {
    diagonal(-1) = A.diagonal(-1);
    diagonal(1) = A.diagonal(1);
  }
}

<T>TriDiagMat toTriDiagMat( const <T>GenMat& S )
{
  int n = S.cols();
  <T>TriDiagMat A(S.rows(),n);
  if (n>0) { A.diagonal(0) = S.diagonal(0); }
  if (n>1) {
    A.diagonal(-1) = S.diagonal(-1);
    A.diagonal(1) = S.diagonal(1);
  }
  return A;
}

> else
> err No conversion to general for shape <M>
> endif

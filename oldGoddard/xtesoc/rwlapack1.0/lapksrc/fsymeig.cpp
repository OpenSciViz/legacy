/*
 * Implementation of FloatSymEigDecomp
 *
 * Generated from template $Id: xsymeig.cpp,v 1.4 1993/10/11 21:48:25 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */ 

#include "rw/fsymeig.h"
#include "rw/fseigsrv.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"

typedef FloatSymQREigServer DefaultServer;

FloatSymEigDecomp::FloatSymEigDecomp()
{
  n=0;
  computedAll = accurate = TRUE;
}

FloatSymEigDecomp::FloatSymEigDecomp(const FloatSymEigDecomp& x)
: n(x.n),
  lambda(x.lambda),
  P(x.P),
  computedAll(x.computedAll),
  accurate(x.accurate)
{
}

FloatSymEigDecomp::FloatSymEigDecomp(const FloatSymMat& A, RWBoolean computeVecs)
{
  factor(A,computeVecs);
}

FloatSymEigDecomp::FloatSymEigDecomp(const FloatSymBandMat& A, RWBoolean computeVecs)
{
  factor(A,computeVecs);
}

void FloatSymEigDecomp::factor(const FloatSymMat& A, RWBoolean computeVecs)
{
  DefaultServer server;
  server.computeEigenVectors(computeVecs);
  *this = server(A);
}
  
void FloatSymEigDecomp::factor(const FloatSymBandMat& A, RWBoolean computeVecs)
{
  DefaultServer server;
  server.computeEigenVectors(computeVecs);
  *this = server(A);
}

void FloatSymEigDecomp::operator=(const FloatSymEigDecomp& A)
{
  n = A.n;
  computedAll = A.computedAll;
  accurate = A.accurate;
  lambda.reference(A.lambda);
  P.reference(A.P);
}

Float       FloatSymEigDecomp::eigenValue(int i)  const
{
  if (i<0 || i>=lambda.length()) RWTHROW(RWBoundsErr(RWMessage(RWLAPK_NOEIG,i)));
  return lambda(i);
}

const FloatVec    FloatSymEigDecomp::eigenVector(int i) const
{
  if (i<0 || i>=P.cols()) RWTHROW(RWBoundsErr(RWMessage(RWLAPK_NOEIG,i)));
  return P.col(i);
}

RWBoolean FloatSymEigDecomp::good()          const
{
  return computedAll && accurate;
}

RWBoolean FloatSymEigDecomp::inaccurate()    const
{
  return computedAll && !accurate;
}

RWBoolean FloatSymEigDecomp::fail()          const
{
  return !computedAll;
}

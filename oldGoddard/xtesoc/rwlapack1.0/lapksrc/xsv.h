> #################################################################
> # Template for singular value decomposition header files        #
> #                                                               #
> # $Header: /users/rcs/lapksrc/xsv.h,v 1.2 1993/07/05 19:19:33 alv Exp $
> #                                                               #
> # Copyright (1993) by Rogue Wave Software, Inc.  This           #
> # software is subject to copyright protection under the laws    #
> # of the United States and other countries.                     #
> #                                                               #
> # Written by Al Vermeulen.                                      #
> #                                                               #
> # Limited License.                                              #
> #################################################################
> #
> # $Log: xsv.h,v $
> # Revision 1.2  1993/07/05  19:19:33  alv
> # fixed up for complex numbers
> #
> # Revision 1.1  1993/05/18  17:00:02  alv
> # Initial revision
> #
>
> # Macros that we need
> 
> define <T>=RW_T_RW
> 
> if <T>==Double
> define <P>=Double <tLetter>=d
> elif <T>==Float
> define <P>=Float <tLetter>=f 
> elif <T>==DComplex
> define <P>=Double <tLetter>=c
> else
> err SVD: not defined for '<T>'
> endif
> beginwrapper <tLetter>sv
>          
/*
 * <T>SVDecomp:   Singular value decomposition
 * <T>SVServer:   SVD server using bidiagonal reduction
 * <T>SVQRServer: SVD server using QR and then bidiagonal reduction
 *
 * Generated from template $Id: xsv.h,v 1.2 1993/07/05 19:19:33 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 * This class represents the singular value decomposition of an
 * mxn matrix A:
 *
 *                          [ s1                 ] 
 *                          [    .               ]  
 *            [           ] [      .             ] [   v1'   ]
 *            [           ] [        .           ] [    .    ]
 *        A = [ u1 ... um ] [          sr        ] [    .    ]
 *            [           ] [             0      ] [    .    ]
 *            [           ] [               .    ] [   vn'   ]
 *                          [                 .  ]
 *                          [                   .]
 *
 * where s1,...,sr are real positive numbers called the singular values
 * of A; u1,...,um are the left singular vectors of A; and v1,...,vn are
 * the right singular vectors of A.  The left singular vectors for an
 * orthonormal basis for Rm, the right singular vectors are an orthonormal
 * basis for Rn.  The singular values are ordered so s1>=s2>=...>=sr.
 * Often, only some of the singular vectors are needed; the default
 * server computes only the first r.  The number of singular values, r,
 * is the rank of A.  Often we truncate the singular value decomposition
 * and ignore any singular values less than some threshold, then r becomes
 * the numberical rank of A.  Doing this can yield more meaningful
 * computational results, since otherwise the insignificant singular
 * values can dominate the computation.
 */ 

#include "rw/lapkdefs.h"
#include "rw/<tLetter>genmat.h"
#include "rw/<tLetter>vec.h"

class <T>SVDecomp {
private:
  <P>Vec    sigma_;       // the singular values
  <T>GenMat U_;           // columns are the computed left singular values
  <T>GenMat VT_;          // rows are the computed right singular values
  RWBoolean computedAll_; // Did I compute everything the server tried to compute?

public:
  <T>SVDecomp();
  <T>SVDecomp(const <T>SVDecomp&);
  <T>SVDecomp(const <T>GenMat&, <P> tol=0);  // compute SVD using default server
  void factor(const <T>GenMat&, <P> tol=0);  // compute SVD using default server
  void operator=(const <T>SVDecomp& x);

  unsigned        cols()             const {return VT_.cols();}
  <P>             singularValue(int) const;
  const <P>Vec    singularValues()   const {return sigma_;}
  const <T>Vec    leftVector(int)    const;
  const <T>GenMat leftVectors()      const {return U_;}
  RWBoolean       good()             const {return computedAll_;}
  RWBoolean       fail()             const {return !computedAll_;}
  unsigned        numLeftVectors()   const {return U_.cols();}
  unsigned        numRightVectors()  const {return VT_.rows();}
  unsigned        rank()             const {return sigma_.length();}
  const <T>Vec    rightVector(int)   const;
> if <T>==<P>
  const <T>GenMat rightVectors()     const {return transpose(VT_);}
> else
  const <T>GenMat rightVectors()     const {return conj(transpose(VT_));}
> endif
  unsigned        rows()             const {return U_.rows();}
  void            truncate(<P> tol);    // truncate singular values less than tol
               
  friend class <T>SVServer;
  friend class <T>SVQRServer;
};

class <T>SVServer {
protected:
  RWBoolean computeAllLeftVecs_;      // compute all m left vecs if true
  RWBoolean computeAllRightVecs_;     // compute all n right vecs if true
  RWBoolean computeDefaultLeftVecs_;  // compute first min(m,n) left vecs if true
  RWBoolean computeDefaultRightVecs_; // compute first min(m,n) right vecs if true
  unsigned  numLeftVecs_;             // number of left vectors to compute (if switches all off)
  unsigned  numRightVecs_;            // number of right vectors to compute (if switches all off)
  <P>       tol_;                     // when to disregard singular values

public:
  <T>SVServer();
  virtual <T>SVDecomp operator()(const <T>GenMat&) const;
  void        computeAllVectors();
  void        computeAllLeftVectors();
  void        computeAllRightVectors();
  void        computeDefaultVectors();
  void        computeDefaultLeftVectors();
  void        computeDefaultRightVectors();
  void        computeVectors(unsigned);
  void        computeLeftVectors(unsigned);
  void        computeRightVectors(unsigned);
  void        setTolerance(<P>);
};

class <T>SVQRServer : public <T>SVServer
{
  <T>SVQRServer();
  virtual <T>SVDecomp operator()(const <T>GenMat&) const;
};

> endwrapper


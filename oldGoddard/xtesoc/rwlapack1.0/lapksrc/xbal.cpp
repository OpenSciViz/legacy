> # Macros that we need
> 
> define <T>=RW_T_RW
> 
> if <T>==Double
> define <P>=Double <tLetter>=d <lLetter>=d
> elif <T>==Float
> define <P>=Float  <tLetter>=f <lLetter>=s 
> elif <T>==DComplex
> define <P>=Double <tLetter>=c <lLetter>=z
> else
> err balance decomposition: not defined for '<T>'
> endif

/*
 * defs for balancing transformation and decomposition classes
 *
 * Generated from template $Id: xbal.cpp,v 1.2 1993/10/11 21:48:04 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 */ 

#include "rw/lapkdefs.h"
#include "rw/lapack.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/<tLetter>bal.h"

<T>BalanceTransform::<T>BalanceTransform()
{
  job_ = 'N';
  ilo_ = ihi_ = 0;
}

void <T>BalanceTransform::operator=(const <T>BalanceTransform& A)
{
  job_ = A.job_;
  ilo_ = A.ilo_;
  ihi_ = A.ihi_;
  scale_.reference(A.scale_);
}
 
void <T>BalanceTransform::init(<T>GenMat *A, RWBoolean permute, RWBoolean scale)
{
  if (A->rows()!=A->cols()) {
    RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTSQUARE,A->rows(),A->cols())));
  }
  RWPRECONDITION(A->rowStride()==1);
  RWPRECONDITION(A->colStride()==A->rows());
  scale_.reshape(A->rows());
  job_ = ( permute ? (scale ? 'B' : 'P') : (scale ? 'S' : 'N') );
  long info;
  <lLetter>gebal(job_,A->rows(),A->data(),A->rows(),ilo_,ihi_,scale_.data(),info);
  RWPOSTCONDITION(info==0);
}

<T>GenMat <T>BalanceTransform::transform(const <T>GenMat& X) const
{
  if (X.rows()!=rows()) {
    RWTHROW(RWInternalErr(RWMessage(RWLAPK_BALANCE,X.rows(),rows())));
  }
  <T>GenMat Y = X.copy();
  if (rows()>0) {
    long info;
    <lLetter>gebak(job_,'R',rows(),ilo_,ihi_,(<P>*)scale_.data(),Y.cols(),Y.data(),Y.rows(),info);
    RWPOSTCONDITION(info==0);
  }
  return Y;
}

<T>GenMat <T>BalanceTransform::invTransform(const <T>GenMat& X) const
{
  if (X.rows()!=rows()) {
    RWTHROW(RWInternalErr(RWMessage(RWLAPK_BALANCE,X.rows(),rows())));
  }
  <T>GenMat Y = X.copy();
  if (rows()>0) {
    long info;
    <lLetter>gebak(job_,'L',rows(),ilo_,ihi_,(<P>*)scale_.data(),Y.cols(),Y.data(),Y.rows(),info);
    RWPOSTCONDITION(info==0);
  }
  return Y;
}

<T>Vec <T>BalanceTransform::transform(const <T>Vec& x) const
{
  <T>GenMat Y = transform(<T>GenMat(x,x.length(),1));
  RWPOSTCONDITION(Y.cols()==1);
  return Y.col(0);
}

<T>Vec <T>BalanceTransform::invTransform(const <T>Vec& x) const
{
  <T>GenMat Y = invTransform(<T>GenMat(x,x.length(),1));
  RWPOSTCONDITION(Y.cols()==1);
  return Y.col(0);
}

<T>BalanceDecomp::<T>BalanceDecomp()
{
} 

<T>BalanceDecomp::<T>BalanceDecomp(const <T>GenMat& A, RWBoolean permute, RWBoolean scale)
{
  factor(A,permute,scale);
}

void <T>BalanceDecomp::factor(const <T>GenMat& A, RWBoolean permute, RWBoolean scale)
{
  C_.reference(A.copy());
  B_.init(&C_,permute,scale);
  RWPOSTCONDITION(rows()==A.rows());
  RWPOSTCONDITION(B_.rows()==A.rows());
  RWPOSTCONDITION(C_.rows()==A.rows());
}

/*
 * The conversion functions to/from general matrices.
 *
 * Generated from template $Id: xcg.cpp,v 1.2 1993/07/05 19:19:33 alv Exp $
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/fltrimat.h"
#include "rw/fgenmat.h"

FloatGenMat::FloatGenMat( const FloatLowerTriMat& A )
  : RWMatView(A.rows(),A.cols(),sizeof(Float))
{
  /* We can't win.  If we go through A in order of element storage we
   * have to skip around through *this, and vice versa.  We'll go 
   * through A in order and skip around through *this so the code looks
   * as much as possible like the UpperTriMat code.
   * Hmmm, now with version 5 of math.h++ we could win by making the
   * general matrix in row major order.  Maybe I'll change the code
   * someday.
   */
  zero();
  for( int i=0; i<nrows; i++ ) {
    for( int j=0; j<=i; j++ ) {
      set(i,j,A.val(i,j));
    }
  }
}

FloatLowerTriMat toLowerTriMat( const FloatGenMat& S )
{
  int n = S.cols();
  FloatLowerTriMat A(S.rows(),n);
  for( int i=0; i<n; i++ ) {
    for( int j=0; j<=i; j++ ) {
      A.set(i,j,S.val(i,j));
    }
  }
  return A;
}


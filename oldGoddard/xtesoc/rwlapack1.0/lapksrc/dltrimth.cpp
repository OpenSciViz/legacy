#if 0
/*
 * Generated from template $Id: xmth.cpp,v 1.2 1993/10/11 21:48:17 alv Exp $
 *
 * Math member functions for the matrix types
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */
#endif

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/dltrimat.h"
#include "rw/dutrimat.h"

/*
 * This function returns a new matrix the same shape as the one 
 * passed in but with the data passed in.  This way we restrict
 * all the >if statements to a couple lines
 */
inline DoubleLowerTriMat sameShapeMatrix( DoubleVec& vec, const DoubleLowerTriMat& A )
{
  return DoubleLowerTriMat(vec,A.rows(),A.cols());
}


static void verifyMatch(const DoubleLowerTriMat& A, const DoubleLowerTriMat& B)
{
    if (A.rows()!=B.rows() || A.cols()!=B.cols())
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATSIZE,A.rows(),A.cols(),B.rows(),B.cols())));
}

RWBoolean DoubleLowerTriMat::operator==(const DoubleLowerTriMat& X)
{
    if (n!=X.n) return FALSE;
    return vec==X.vec;	// All elements in vec used in the matrix
}


DoubleLowerTriMat& DoubleLowerTriMat::operator+=(const DoubleLowerTriMat& m)
{
  verifyMatch(*this,m);
  vec += m.vec;
  return *this;
}

DoubleLowerTriMat& DoubleLowerTriMat::operator-=(const DoubleLowerTriMat& m)
{
  verifyMatch(*this,m);
  vec -= m.vec;
  return *this;
}

DoubleLowerTriMat& DoubleLowerTriMat::operator*=(const DoubleLowerTriMat& m)
{
  verifyMatch(*this,m);
  vec *= m.vec;
  return *this;
}

DoubleLowerTriMat& DoubleLowerTriMat::operator*=(Double x)
{
  vec *= x;
  return *this;
}

DoubleLowerTriMat& DoubleLowerTriMat::operator/=(const DoubleLowerTriMat& m)
{
  verifyMatch(*this,m);
  vec /= m.vec;
  return *this;
}

DoubleLowerTriMat& DoubleLowerTriMat::operator/=(Double x)
{
  vec /= x;
  return *this;
}




DoubleLowerTriMat operator-(const DoubleLowerTriMat& A)
{
  DoubleVec temp(-(A.dataVec()));
  return sameShapeMatrix(temp,A);
}
  
DoubleLowerTriMat operator+(const DoubleLowerTriMat& A)
{
  return A;
}

DoubleLowerTriMat operator*(const DoubleLowerTriMat& A, const DoubleLowerTriMat& B) {
	verifyMatch(A,B);
	DoubleVec temp(A.dataVec()*B.dataVec());
	return sameShapeMatrix(temp,A);
}

DoubleLowerTriMat operator/(const DoubleLowerTriMat& A, const DoubleLowerTriMat& B) {
	verifyMatch(A,B);
	DoubleVec temp(A.dataVec()/B.dataVec());
	return sameShapeMatrix(temp,A);
}

DoubleLowerTriMat operator+(const DoubleLowerTriMat& A, const DoubleLowerTriMat& B) {
	verifyMatch(A,B);
	DoubleVec temp(A.dataVec()+B.dataVec());
	return sameShapeMatrix(temp,A);
}

DoubleLowerTriMat operator-(const DoubleLowerTriMat& A, const DoubleLowerTriMat& B) {
	verifyMatch(A,B);
	DoubleVec temp(A.dataVec()-B.dataVec());
	return sameShapeMatrix(temp,A);
}

DoubleLowerTriMat operator*(const DoubleLowerTriMat& A, Double x) {
	DoubleVec temp(A.dataVec()*x);
	return sameShapeMatrix(temp,A);
}

/*
 * Out-of-line version for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
  DoubleLowerTriMat operator*(Double x, const DoubleLowerTriMat& A) { return A*x; }
#endif

DoubleLowerTriMat operator/(const DoubleLowerTriMat& A, Double x) {
	DoubleVec temp(A.dataVec()/x);
	return sameShapeMatrix(temp,A);
}





DoubleLowerTriMat abs(const DoubleLowerTriMat& A) {
	DoubleVec temp(abs(A.dataVec()));
	return sameShapeMatrix(temp,A);
}



DoubleUpperTriMat transpose(const DoubleLowerTriMat& A)
{
return DoubleUpperTriMat(A.dataVec(),A.rows(),A.cols());
}

DoubleVec product(const DoubleLowerTriMat& A, const DoubleVec& x)
{
    if (A.cols()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATVECPROD,A.className,A.rows(),A.cols(),x.length())));
    }
    DoubleVec y(A.rows(),rwUninitialized);
    const DoubleVec& v = A.dataVec();
    int n=A.rows();
    for( int i=0; i<n; i++ ) {
      y(i) = dot( v.slice(i*(i+1)/2,i+1,1), x.slice(0,i+1,1) );
    }
    return y;
}

DoubleVec product(const DoubleVec& x, const DoubleLowerTriMat& A)
{
    if (A.rows()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECMATPROD,x.length(),A.className,A.rows(),A.cols())));
    }
    return product(transpose(A),x);
}



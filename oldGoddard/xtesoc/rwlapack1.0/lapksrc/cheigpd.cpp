/*
 * Implementation of DComplexHermPDQREigServer
 *
 * Generated from template $Id: xseigpd.cpp,v 1.4 1993/10/11 21:48:21 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * Compute eigenvalues/vectors of a positive definite tridiagonal matrix.
 */ 

#include "rw/cheigsrv.h"
#include "rw/ctd.h"
#include "rw/lapack.h"


DComplexHermPDQREigServer::DComplexHermPDQREigServer(RWBoolean vecs)
{
  computeVecs_ = vecs;
}

DoubleSymEigDecomp DComplexHermPDQREigServer::decompose(const DComplexHermTriDiagDecomp& decomp)
{
  char compz = computeVecs_ ? 'I' : 'N';
  long n = decomp.rows();
  DoubleVec d = decomp.diagonal().copy();
  DoubleVec e = decomp.offDiagonal().copy();
  int zsize = computeVecs_ ? decomp.rows() : 0;
  DoubleGenMat Z(zsize,zsize,rwUninitialized);
  long ldz = computeVecs_ ? n : 1;
  Double *work = computeVecs_ ? new Double [4*n+1] : 0;  // Needs only 4*n-4, but this way we avoid <0 checks
  long info;
  dpteqr(compz,n,d.data(),e.data(),Z.data(),ldz,work,info);
  delete work;
  RWPOSTCONDITION(info>=0);

  DoubleSymEigDecomp eig;
  if (info>0) {
    computedAll(eig) = FALSE;
  }
  if (info==0) {
  DComplexHermPDQREigServer::n(eig) = decomp.rows();
  lambda(eig).reference(d);
  P(eig).reference(Z);
  }
  return eig;
}

RWBoolean DComplexHermPDQREigServer::computeEigenVectors() const {return computeVecs_;}

void DComplexHermPDQREigServer::computeEigenVectors(RWBoolean x) {computeVecs_=x;}


  


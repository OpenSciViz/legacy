/*
 * Implementation of DComplexPDFact
 *
 * Generated from template $Id: xfct.cpp,v 1.6 1993/10/11 21:48:11 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/lapkdefs.h"
#include "rw/cpdfct.h"
#include "rw/lapack.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"  
#include "rw/cgenmat.h"

const char* DComplexPDFact::className = "DComplexPDFact";

DComplexPDFact::~DComplexPDFact()
{
}

DComplexPDFact::DComplexPDFact()
{
  info = -1;
}

DComplexPDFact::DComplexPDFact( const DComplexPDFact& A )
  : factorization(A.factorization)
{
  info = A.info;
  Anorm = A.Anorm;
}

DComplexPDFact::DComplexPDFact(const DComplexHermMat& A, RWBoolean estimateCondition)
  : factorization(A)  // Use reference semantics, followed by
                      // deepenShallowCopy(), to work around a BCC V3.0 bug
{
    factorization.deepenShallowCopy();
    dofactor(estimateCondition);
}

void DComplexPDFact::factor( const DComplexHermMat& A, RWBoolean estimateCondition )
{
    DComplexHermMat Acopy = A.copy();
    factorization.reference(Acopy);
    dofactor(estimateCondition);
}

void DComplexPDFact::dofactor(RWBoolean estimateCondition)
{
   info=0;   // for size zero matrices, need to set this explicitly
   long n = rows();	

    // Error (sanity) checking
    RWPRECONDITION(factorization.dataVec().stride()==1);
    for(int r=rows(); r--;) {
      if (imag(factorization.val(r,r))!=0.0) {
	RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTHERM)));
      }
    }


    // If we care to ever find the condition number, we need to know
    // the 1-norm of the matrix A.  Compute it while the member data
    // factorization is still a copy of A, and not yet it's factorization.
    Anorm = -1; 
    if (n>0 && estimateCondition) {
      Double *work = new Double [n];
      Anorm = zlansp('1','U',n,factorization.data(),work);
      delete [] work;
    }

    if (n>0) {
      zpptrf('U',n,factorization.data(),info);
      RWPOSTCONDITION(info>=0);
    }
}

RWBoolean DComplexPDFact::fail() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}

RWBoolean DComplexPDFact::isPD() const
{
    return (rows()>0 && info!=0) ? FALSE : TRUE;
}  

RWBoolean DComplexPDFact::isSingular() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}

Double DComplexPDFact::condition() const
{
    Double Anormcopy = Anorm;      // Copy to stop warnings about ref parm
    long info;
    long n = rows();
    Double rcond;
    DComplex *work = new DComplex[4*n];   // Some routines only require 2*n, but what the hell
    Double *work2 = new Double [n];
    zppcon('U',n,(DComplex*)factorization.data(),Anormcopy,rcond,work,work2,info);
    delete [] work2;    
    delete [] work;
    RWPOSTCONDITION(info==0);
    return rcond;
}

DComplexVec DComplexPDFact::solve( const DComplexVec& b ) const
{
  DComplexGenMat B(b,b.length(),1);
  DComplexGenMat X = solve(B);
  RWPOSTCONDITION(X.cols()==1);
  return X.col(0);
}

DComplexGenMat DComplexPDFact::solve( const DComplexGenMat& B ) const
{
    DComplexGenMat X(B.copy());
    long n = rows();
    if (B.rows()!=n) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,B.rows(),n)));
    if (fail()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTSOLVE)));
    long nrhs = X.cols();
    long info;
    if (n>0) {
    zpptrs('U',n,nrhs,(DComplex*)factorization.data(),X.data(),n,info);
      RWPOSTCONDITION(info==0);
    }
    return X;    
}

DComplex DComplexPDFact::determinant() const
{
    DComplex rootdet = 1.0;
    for(int i=rows(); i--;) { rootdet *= factorization.val(i,i); }  // using val() rather than just op()() shuts up a SUN warning
    return rootdet*rootdet;
}
              
DComplexHermMat DComplexPDFact::inverse( ) const
{
    if (rows()==0) { return factorization; }
    if (fail()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTSOLVE)));
    DComplexHermMat soln(factorization);
    soln.deepenShallowCopy();
    long n = rows();
    long info;
    zpptri('U',n,soln.data(),info);
    RWPOSTCONDITION(info==0);
    return soln;
}


/*
 * Global functions which provide alternate ways to call member functions.
 * These used to be inline, but this requires that you include more header
 * files on some compilers, and that is just not worth it.
 */
DComplexVec solve(const DComplexPDFact& A, const DComplexVec& b) { return A.solve(b); }
DComplexGenMat solve(const DComplexPDFact& A, const DComplexGenMat& b) { return A.solve(b); }
DComplex    determinant (const DComplexPDFact& A)	      { return A.determinant(); }

DComplexHermMat inverse(const DComplexPDFact& A)      	      { return A.inverse(); }

Double condition(const DComplexPDFact& A) { return A.condition(); }



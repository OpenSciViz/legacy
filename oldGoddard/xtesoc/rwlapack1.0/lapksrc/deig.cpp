/*
 * Implementation of DoubleEigDecomp
 *
 * Generated from template $Id: xeig.cpp,v 1.3 1993/10/11 21:48:08 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 */ 

#include "rw/deig.h"
#include "rw/deigsrv.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"

typedef DoubleSchurEigServer DefaultServer;

DoubleEigDecomp::DoubleEigDecomp()
{
  n=0;
  computedAll = accurate = TRUE;
}

DoubleEigDecomp::DoubleEigDecomp(const DoubleEigDecomp& x)
: n(x.n),
  lambda(x.lambda),
  P(x.P),
  Q(x.Q),
  computedAll(x.computedAll),
  accurate(x.accurate)
{
}

DoubleEigDecomp::DoubleEigDecomp(const DoubleGenMat& A, RWBoolean computeVecs)
{
  factor(A,computeVecs);
}

void DoubleEigDecomp::factor(const DoubleGenMat& A, RWBoolean computeVecs)
{
  DefaultServer server;
  server.computeLeftEigenVectors(computeVecs);
  server.computeRightEigenVectors(computeVecs);
  *this = server(A);
}
  
void DoubleEigDecomp::operator=(const DoubleEigDecomp& A)
{
  n = A.n;
  computedAll = A.computedAll;
  accurate = A.accurate;
  lambda.reference(A.lambda);
  P.reference(A.P);
  Q.reference(A.Q);
}

DComplex       DoubleEigDecomp::eigenValue(int i)  const
{
  if (i<0 || i>=lambda.length()) RWTHROW(RWBoundsErr(RWMessage(RWLAPK_NOEIG,i)));
  return lambda(i);
}

const DComplexVec    DoubleEigDecomp::leftEigenVector(int i) const
{
  if (i<0 || i>=P.cols()) RWTHROW(RWBoundsErr(RWMessage(RWLAPK_NOEIG,i)));
  RWPOSTCONDITION(lambda.length()>i); // if this isn't true something went wrong
  DComplex eval = lambda(i);
  if (imag(eval)==0) {
    return P.col(i);       // A real eigenvalue
  } else {                 // A complex eigenvalue
    if (i>0 && lambda(i-1)==conj(eval)) {
      return DComplexVec(P.col(i-1),-P.col(i));
    } else {
      RWPOSTCONDITION(i<lambda.length() && lambda(i+1)==conj(eval));  // if this fails the evals aren't a pair
      return DComplexVec(P.col(i),P.col(i+1));
    }
  }  
}

const DComplexGenMat DoubleEigDecomp::leftEigenVectors() const
{
  DComplexGenMat vecs(P);
  for(int i=vecs.cols(); i--;) {  // fix up complex conjugate pairs of vals
    if (imag(lambda(i))!=0) {
      real(vecs.col(i)) = P.col(i-1);
      imag(vecs.col(i)) = -P.col(i);
      i--;
      imag(vecs.col(i)) = P.col(i+1);
    }
  }
  return vecs;
}

const DComplexVec    DoubleEigDecomp::rightEigenVector(int i) const
{
  if (i<0 || i>=Q.cols()) RWTHROW(RWBoundsErr(RWMessage(RWLAPK_NOEIG,i)));
  RWPOSTCONDITION(lambda.length()>i); // if this isn't true something went wrong
  DComplex eval = lambda(i);
  if (imag(eval)==0) {
    return Q.col(i);       // A real eigenvalue
  } else {                 // A complex eigenvalue
    if (i>0 && lambda(i-1)==conj(eval)) {
      return DComplexVec(Q.col(i-1),-Q.col(i));
    } else {
      RWPOSTCONDITION(i<lambda.length() && lambda(i+1)==conj(eval));  // if this fails the evals aren't a pair
      return DComplexVec(Q.col(i),Q.col(i+1));
    }
  }  
}

const DComplexGenMat DoubleEigDecomp::rightEigenVectors() const
{
  DComplexGenMat vecs(Q);
  for(int i=vecs.cols(); i--;) {  // fix up complex conjugate pairs of vals
    if (imag(lambda(i))!=0) {
      real(vecs.col(i)) = Q.col(i-1);
      imag(vecs.col(i)) = -Q.col(i);
      i--;
      imag(vecs.col(i)) = Q.col(i+1);
    }
  }
  return vecs;
}

RWBoolean DoubleEigDecomp::good()          const
{
  return computedAll && accurate;
}

RWBoolean DoubleEigDecomp::inaccurate()    const
{
  return computedAll && !accurate;
}

RWBoolean DoubleEigDecomp::fail()          const
{
  return !computedAll;
}

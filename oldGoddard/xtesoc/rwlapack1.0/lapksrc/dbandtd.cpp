/*
 * Implementation of DoubleSymBandTriDiagDecomp
 *
 * Generated from template $Id: xbandtd.cpp,v 1.4 1993/10/11 21:48:05 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */ 

#include "rw/dtd.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/lapack.h"
#include "rw/dsbndmat.h"

DoubleSymBandTriDiagDecomp::DoubleSymBandTriDiagDecomp(const DoubleSymBandMat& A, RWBoolean keepQ)
{
  if (A.rows()>0 && A.bandwidth()>0) {
    char vect = (keepQ) ? 'V' : 'N';
    char uplo = 'U';   // Indicates to lapack that upper triangle is stored
    long n = A.rows();
    long kd = A.upperBandwidth();
    DoubleSymBandMat Acopy = A.copy();
    long ldab = kd+1;
    DoubleVec D(A.rows(),rwUninitialized);
    DoubleVec E(A.rows()-1,rwUninitialized);
    if (keepQ) {Q.reshape(A.rows(),A.rows());}
    Double *work = new Double [n];
    long info;
    dsbtrd(vect,uplo,n,kd,(Double*)Acopy.data(),ldab,(Double*)D.data(),(Double*)E.data(),(Double*)Q.data(),n,work,info);
    RWPOSTCONDITION(info==0);
    delete work;
    set(D,E);
  }
}

DoubleVec DoubleSymBandTriDiagDecomp::transform(const DoubleVec& V) const
{
  return DoubleSymTriDiagDecomp::transform(V);
}

DoubleGenMat DoubleSymBandTriDiagDecomp::transform(const DoubleGenMat& C) const
{
  if (C.cols()==0 || C.rows()==0) {
    return C;
  } else {
    if (C.rows()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,C.rows(),rows())));
    if (Q.rows()==0) RWTHROW(RWInternalErr(RWMessage(RWLAPK_QNOTCOMPUTED)));
    return product(Q,C);
  }
}

> #################################################################
> # The code for the QR decomposition routines                    #
> #                                                               #
> # $Header: /users/rcs/lapksrc/xqr.cpp,v 1.5 1993/10/15 06:37:58 alv Exp $
> #                                                               #
> # Copyright (1991-1993) by Rogue Wave Software, Inc.  This      #
> # software is subject to copyright protection under the laws of #
> # the United States and other countries.                        #
> #                                                               #
> # Written by Al Vermeulen.                                      #
> #                                                               #
> # Limited License.                                              #
> #################################################################
> #
> # $Log: xqr.cpp,v $
> # Revision 1.5  1993/10/15  06:37:58  alv
> # fixed bug caused by typo uncovered by helpful xlC warning
> #
> # Revision 1.4  1993/10/11  21:48:18  alv
> # synch'ed with Math.h++ v5.1
> #
> # Revision 1.3  1993/07/08  23:26:13  alv
> # ported to SUN CC v3
> #
> # Revision 1.2  1993/07/05  19:19:33  alv
> # fixed up for complex numbers
> #
> # Revision 1.1  1993/05/18  17:00:02  alv
> # Initial revision
> #
>
> define <T>=RW_T_RW
> if <T>==Double
> define <P>=Double <tLetter>=d <gqr>=dorgqr <mqr>=dormqr <lLetter>=d
> elif <T>==Float 
> define <P>=Float  <tLetter>=f <gqr>=sorgqr <mqr>=sormqr <lLetter>=s
> elif <T>==DComplex 
> define <P>=Double <tLetter>=c <gqr>=zungqr <mqr>=zunmqr <lLetter>=z
> else
> err orthogonal decomposition: not defined for '<T>'
> endif
>
/*
 * Definitions for <T>QRDecomp and <T>QRDecompServer
 *
 * Generated from template $Id: xqr.cpp,v 1.5 1993/10/15 06:37:58 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 */

#include "rw/lapack.h"
#include "rw/<tLetter>qr.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/<tLetter>utrimat.h"

<T>QRDecomp::<T>QRDecomp()
{
  pivots_ = 0;
}

<T>QRDecomp::<T>QRDecomp(const <T>QRDecomp& x)
{
  pivots_ = 0;
  *this = x;
}

void <T>QRDecomp::operator=(const <T>QRDecomp& x)
{
  tau_.reference(x.tau_);
  QR_.reference(x.QR_);
  delete [] pivots_;
  pivots_ = 0;
  if (x.pivots_) {
    pivots_ = new long [cols()];
    for(int i=cols(); i--;) {
      pivots_[i] = x.pivots_[i];
    }
  }
}

<T>QRDecomp::<T>QRDecomp(const <T>GenMat& A) 
{
  pivots_=0;
  factor(A);
}

void <T>QRDecomp::factor(const <T>GenMat& A)
{
  <T>QRDecompServer server;
  *this = server(A);
}

<T>QRDecomp::~<T>QRDecomp()
{
  delete [] pivots_;
}


<T>GenMat      <T>QRDecomp::P() const
{
  <T>GenMat Pmat(cols(),cols(),(<T>)0);
  if (pivots_) {
    for(int j=cols(); j--;) {
      Pmat((int)(pivots_[j]-1),j) = 1;
    }
  } else {
    Pmat.diagonal() = (<T>)1;
  }
  return Pmat;
}

<T>GenMat <T>QRDecomp::R() const
{
  int m = (cols()<rows()) ? cols() : rows();
  // Obfuscate the following line, in order to shut up a spurious
  // sun CC 3.0.1 warning
  // <T>GenMat Rmat = QR_(RWSlice(0,m),RWAll);
  <T>GenMat Rmat = QR_.operator()(RWSlice(0,m),RWAll);
  Rmat.deepenShallowCopy();
  for(int i=m; --i>0;) {  // Loop indexes i=m-1,...,1
    Rmat.diagonal(-i) = (<T>)0;
  }
  return Rmat;
}

<T>Vec         <T>QRDecomp::Rdiagonal() const
{
  return QR_.diagonal().copy();
}

<T>GenMat      <T>QRDecomp::Q() const
{
  long m = rows();
  long n = cols();
  long k = (m<n) ? m : n;
  <T>GenMat Qmat(QR_);
  Qmat.resize((int)m,(int)m);
  Qmat.deepenShallowCopy();
  long lwork = n*ilaenv( 1, "<gqr>", " ", m, m, k, -1 );
  <T> *work = new <T> [lwork];
  long info;
  <gqr>(m,m,k,Qmat.data(),m,(<T>*)tau_.data(),work,lwork,info); 
  RWPOSTCONDITION(info==0);
  RWPOSTCONDITION(work[0]<=lwork);
  delete work;
  return Qmat;   
}

<T>Vec <T>QRDecomp::Px(const <T>Vec& x) const
{
  if (x.length()!=cols()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),cols())));
  if (pivots_) {
    <T>Vec y(x.length(),rwUninitialized);
    for(int i=x.length(); i--;) { y((int)pivots_[i]-1) = x(i); }
    return y;
  } else {
    return x.copy();
  }
}

<T>Vec <T>QRDecomp::PTx(const <T>Vec& x) const
{
  if (x.length()!=cols()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),cols())));
  if (pivots_) {
    <T>Vec y(x.length(),rwUninitialized);
    for(int i=x.length(); i--;) { y(i) = x((int)pivots_[i]-1); }
    return y;
  } else {
    return x.copy();
  }
}

<T>Vec <T>QRDecomp::Rx(const <T>Vec& x) const
{
  if (x.length()!=cols()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),cols())));
  int m = (cols()<rows()) ? cols() : rows();
  // explicit call to operator() in next line shuts up sun v3.0.1 warning
  <T>GenMat R = QR_.operator()(RWSlice(0,m),RWAll);     // Also has entries for Q
  RWPRECONDITION(R.rows()<=R.cols());
  if (R.cols()>R.rows()) {
    <T>UpperTriMat R1 = toUpperTriMat(R(RWAll,RWSlice(0,m)));
    <T>GenMat R2 = R(RWAll,RWToEnd(m));
    return product(R1,x(RWSlice(0,m))) + product(R2,x(RWToEnd(m)));
  } else {
    <T>UpperTriMat R1 = toUpperTriMat(R);
    return product(R1,x);
  }
}

<T>Vec <T>QRDecomp::RTx(const <T>Vec& x) const
{
  int m = (cols()<rows()) ? cols() : rows();
  if (x.length()!=m) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),m)));
  // explicit call to operator() in next line shuts up sun v3.0.1 warning
  <T>GenMat R = QR_.operator()(RWSlice(0,m),RWAll);     // Also has entries for Q
  RWPRECONDITION(R.rows()<=R.cols());
  if (R.cols()>R.rows()) {
    <T>Vec y(cols(),rwUninitialized);
    <T>UpperTriMat R1 = toUpperTriMat(R(RWAll,RWSlice(0,m)));
    <T>GenMat R2 = R(RWAll,RWToEnd(m));
> if <T>==<P>
    y(RWSlice(0,m)) = product(x,R1);
    y(RWToEnd(m)) = product(x,R2);
> else
    y(RWSlice(0,m)) = product(x,conj(R1));
    y(RWToEnd(m)) = product(x,conj(R2));
> endif
    return y;
  } else {
> if <T>==<P>
    <T>UpperTriMat R1 = toUpperTriMat(R);
> else
    <T>UpperTriMat R1 = conj(toUpperTriMat(R));
> endif
    return product(x,R1);
  }
}

<T>Vec <T>QRDecomp::Rinvx(const <T>Vec& x) const
{
  if (x.length()!=cols()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),cols())));
  if (cols()>rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_RSINGULAR)));
  <T>Vec y = x.copy();
  long n = cols();
  long m = rows();
  long one = 1;
  long info;
  <lLetter>trtrs('U','N','N',n,one,(<T>*)QR_.data(),m,y.data(),n,info);
  RWPOSTCONDITION(info>=0);
  if (info>0) RWTHROW(RWInternalErr(RWMessage(RWLAPK_RSINGULAR)));
  return y;
}

<T>Vec <T>QRDecomp::RTinvx(const <T>Vec& x) const
{
  if (x.length()!=cols()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),cols())));
  if (cols()>rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_RSINGULAR)));
  <T>Vec y = x.copy();
  long n = cols();
  long m = rows();
  long one = 1;
  long info;
> if <T>!=<P>
  <lLetter>trtrs('U','C','N',n,one,(<T>*)QR_.data(),m,y.data(),n,info);
> else
  <lLetter>trtrs('U','T','N',n,one,(<T>*)QR_.data(),m,y.data(),n,info);
> endif
  RWPOSTCONDITION(info>=0);
  if (info>0) RWTHROW(RWInternalErr(RWMessage(RWLAPK_RSINGULAR)));
  return y;
}

<T>Vec <T>QRDecomp::Qx(const <T>Vec& x) const
{
  if (x.length()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),rows())));
  long m = rows();
  long n = cols();
  long k = (m<n) ? m : n;
  <T>Vec y = x.copy();
  long one = 1;
  long lwork = ilaenv( 1, "<mqr>", "LN", m, one, k, -1 );
  <T> *work = new <T> [lwork];
  long info;
  <mqr>('L','N',m,one,k,(<T>*)QR_.data(),m,(<T>*)tau_.data(),y.data(),m,work,lwork,info);
  RWPOSTCONDITION(info==0);
  RWPOSTCONDITION(lwork>=work[0]);
  delete [] work;
  return y;
}

<T>Vec <T>QRDecomp::QTx(const <T>Vec& x) const
{
  if (x.length()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),rows())));
  long m = rows();
  long n = cols();
  long k = (m<n) ? m : n;
  <T>Vec y = x.copy();
  long one = 1;
> if <T>!=<P>
  long lwork = ilaenv( 1, "<mqr>", "LT", m, one, k, -1 );
> else
  long lwork = ilaenv( 1, "<mqr>", "LC", m, one, k, -1 );
> endif
  <T> *work = new <T> [lwork];
  long info;
> if <T>!=<P>
  <mqr>('L','C',m,one,k,(<T>*)QR_.data(),m,(<T>*)tau_.data(),y.data(),m,work,lwork,info);
> else
  <mqr>('L','T',m,one,k,(<T>*)QR_.data(),m,(<T>*)tau_.data(),y.data(),m,work,lwork,info);
> endif
  RWPOSTCONDITION(info==0);
  RWPOSTCONDITION(lwork>=work[0]);
  delete [] work;
  return y;
}

<T>QRDecompServer::<T>QRDecompServer()
{
  pivot_ = TRUE;
}

void <T>QRDecompServer::setPivoting(RWBoolean x)
{
  pivot_ = x;
}

void <T>QRDecompServer::setInitialIndex(int i)
{
  if (i<0) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTFREEZE,i)));
  for(int j=initial_.length(); j--;) {
    if (initial_(j)==i) return;   // already frozen
  }
  initial_.resize(initial_.length()+1);
  initial_(initial_.length()-1) = i;
}

void <T>QRDecompServer::setFreeIndex(int i)
{
  if (i<0) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTFREE,i)));
  for(int j=initial_.length(); j--;) {
    if (initial_(j)==i) {
      int l = initial_.length()-j-1;
      if (l>0) {
        initial_(RWSlice(j,l)) = initial_(RWSlice(j+1,l));
      }
      initial_.resize(initial_.length()-1);
    }
  }
}

<T>QRDecomp <T>QRDecompServer::operator()(const <T>GenMat& A) const
{
  long m = A.rows();
  long n = A.cols();
  long info;
  <T>QRDecomp decomp;
  decomp.QR_.reference(A.copy());
  decomp.tau_.reshape((m<n) ? (int)m : (int)n);
  delete [] decomp.pivots_;
  decomp.pivots_ = 0;
  if (pivot_) {
    decomp.pivots_ = new long [n];
    for(int i=(int)n; i--;) decomp.pivots_[i]=0;
    for(i=initial_.length(); i--;) {
      long index = initial_(i);
      RWPRECONDITION(index>=0);
      if (index>=n) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTFREEZE,index)));
      decomp.pivots_[index] = 1;
    }
    <T> *work = new <T> [3*n];
> if <T>!=<P>
    <P> *rwork = new <P> [2*n];
    <lLetter>geqpf(m,n,decomp.QR_.data(),m,decomp.pivots_,decomp.tau_.data(),work,rwork,info);
    delete [] rwork;
> else
    <lLetter>geqpf(m,n,decomp.QR_.data(),m,decomp.pivots_,decomp.tau_.data(),work,info);
> endif
    RWPOSTCONDITION(info==0);
    delete [] work;
  } else {
    long lwork = n*ilaenv( 1, "<lLetter>geqrf", " ", m, n, -1, -1 );
    <T> *work = new <T> [lwork];
    <lLetter>geqrf(m,n,decomp.QR_.data(),m,decomp.tau_.data(),work,lwork,info);
    RWPOSTCONDITION(info==0);
    RWPOSTCONDITION(lwork>=work[0]);
    delete [] work;
  }
  return decomp;
}           

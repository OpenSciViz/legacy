> # $Header: /users/rcs/lapksrc/xeighess.cpp,v 1.4 1993/10/11 21:48:09 alv Exp $
> #
> # $Log: xeighess.cpp,v $
> # Revision 1.4  1993/10/11  21:48:09  alv
> # synch'ed with Math.h++ v5.1
> #
> # Revision 1.3  1993/07/08  23:26:13  alv
> # ported to SUN CC v3
> #
> # Revision 1.2  1993/07/05  19:19:33  alv
> # fixed up for complex numbers
> #
> # Revision 1.1  1993/05/18  17:00:02  alv
> # Initial revision
> #
>
> define <T>=RW_T_RW
> 
> if <T>==Double
> define <P>=Double <tLetter>=d <Z>=DComplex <zLetter>=c <lLetter>=d
> elif <T>==Float
> define <P>=Float  <tLetter>=f <Z>=FComplex <zLetter>=b <lLetter>=s
> elif <T>==DComplex
> define <P>=Double <tLetter>=c <Z>=DComplex <zLetter>=c <lLetter>=z 
> else
> err eigen-decomposition: not defined for '<T>'
> endif
>
/*
 * Implementation of <T>EigHessServer
 *
 * Generated from template $Id: xeighess.cpp,v 1.4 1993/10/11 21:48:09 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
  * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */ 

#include "rw/<tLetter>eigsrv.h"
#include "rw/<tLetter>hess.h"
#include "rw/lapack.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"

<T>HessEigServer::<T>HessEigServer(RWBoolean left, RWBoolean right, RWBoolean sc, RWBoolean perm)
  : selectRange_((const RWSlice&)RWAll)
{
  computeLeftVecs_  = left;
  computeRightVecs_ = right;
  scale_            = sc;
  permute_          = perm;
}

RWBoolean <T>HessEigServer::computeLeftEigenVectors() const       {return computeLeftVecs_;}
void      <T>HessEigServer::computeLeftEigenVectors(RWBoolean x)  {computeLeftVecs_ = x;}
RWBoolean <T>HessEigServer::computeRightEigenVectors() const      {return computeRightVecs_;}
void      <T>HessEigServer::computeRightEigenVectors(RWBoolean x) {computeRightVecs_ = x;}
RWBoolean <T>HessEigServer::scale()                    const      {return scale_;}
void      <T>HessEigServer::scale(RWBoolean x)                    {scale_ = x;}
RWBoolean <T>HessEigServer::permute()                  const      {return permute_;}
void      <T>HessEigServer::permute(RWBoolean x)                  {permute_ = x;}
void      <T>HessEigServer::balance(RWBoolean x)                  {scale_ = permute_ = x;}

void <T>HessEigServer::selectEigenVectors(const IntVec& s)
{
  selectVec_.reference(s.copy());
  selectRange_ = RWAll;
}

void <T>HessEigServer::selectEigenVectors(const RWSlice& s)
{
  selectRange_ = s;
  selectVec_.reshape(0);
}

RWBoolean <T>HessEigServer::computeAllEigenVectors() const
{
  // The next line needs to determine if the RWSlice object selectRange_
  // is a copy of RWAll.  This means it will have a begin of zero, a stride
  // of one, and a length which matches whatever length the argument vector
  // is.  Test for this by asking for two lengths, if the slice gives back
  // the two lengths, then it must be an RWAll.
  RWBoolean isRWAll = (selectRange_.begin()==0 && selectRange_.stride()==1 && selectRange_.len(5)==5 && selectRange_.len(6)==6);
  return (isRWAll && selectVec_.length()==0);
}

<T>EigDecomp <T>HessEigServer::operator()(const <T>GenMat& A)
{
  <T>HessenbergDecomp x(A,permute_,scale_);
  return (*this)(x);
}

<T>EigDecomp <T>HessEigServer::operator()(const <T>BalanceDecomp& A)
{
  <T>HessenbergDecomp x(A);
  return (*this)(x);
}

> if <T>==<Z>
<T>EigDecomp <T>HessEigServer::setEigenVals(const <T>HessenbergDecomp& A)
> else
<T>EigDecomp <T>HessEigServer::setEigenVals(const <T>HessenbergDecomp& A, <T>Vec *wr, <T>Vec *wi)
> endif
{
  <T>EigDecomp eig;
  n(eig) = A.rows();
  long n = A.rows();
  <T>GenMat H = A.H();
  H.deepenShallowCopy();   // just in case the interface changes
  lambda(eig).reshape(A.rows());
> if <T>==<P>
  wr->reshape(A.rows());
  wi->reshape(A.rows());
> endif
  long ilo = A.balanceTransform().lowIndex();
  long ihi = A.balanceTransform().highIndex();
  <T> *work = new <T> [n];
  long info;
> if <T>==<P>
  <lLetter>hseqr('E','N',n,ilo,ihi,H.data(),H.colStride(),wr->data(),wi->data(),0,1,work,0,info);
> else
  <lLetter>hseqr('E','N',n,ilo,ihi,H.data(),H.colStride(),lambda(eig).data(),0,1,work,0,info);
> endif
  RWPOSTCONDITION(info>=0);
  if (info>0) {
    RWPOSTCONDITION(ilo-1+n-info<n);
> if <T>==<P>
    lambda(eig).reshape((int)(ilo-1+n-info));
    (*wr)(RWSlice((int)ilo,(int)(n-info))) = (*wr)(RWToEnd((int)info));
    wr->reshape((int)(ilo-1+n-info));
    (*wi)(RWSlice((int)ilo,(int)(n-info))) = (*wi)(RWToEnd((int)info));
    wi->reshape((int)(ilo-1+n-info));
    computedAll(eig) = FALSE;
> else
    lambda(eig)(RWSlice(ilo,n-info)) = lambda(eig)(RWToEnd(info));
    lambda(eig).reshape(ilo-1+n-info);
> endif
  }
> if <T>==<P>
  real(lambda(eig)) = *wr;
  imag(lambda(eig)) = *wi;
> endif
  delete work;
  return eig;
}

<T>EigDecomp <T>HessEigServer::operator()(const <T>HessenbergDecomp& A)
{    
  // First build a container and set up the eigenvalues
> if <T>==<P>
  DoubleVec wr,wi;
  <T>EigDecomp eig = setEigenVals(A,&wr,&wi);
> else
  <T>EigDecomp eig = setEigenVals(A);
> endif
  if (!eig.good()) return eig;   // in case we didn't get the eigenvalues

  // Now determine which eigenvectors we want
  IntVec select(n(eig),0);
  if (computeAllEigenVectors()) {
    select=1;
  } else {
    if (selectVec_.length()!=0) {
      for(int i=selectVec_.length(); i--;) {
        int x = selectVec_(i);
        if (x<0 || x>=n(eig)) RWTHROW(RWBoundsErr(RWMessage(RWLAPK_NOEIG,x)));
        select(x) = 1;
      }
    } else {  // which to compute is indicated by the RWSlice
      selectRange_.boundsCheck(n(eig));
      select(selectRange_) = 1;
    }

> if <Z>!=<T>
    // Make sure that both halves of complex conjugate eigenvals are chosen
    for(int j=select.length(); j--;) {
      if (select(j)) {
        <Z> eval = eig.eigenValue(j);
        if (imag(eval)!=0) {
          if (j>0 && real(eval)==real(eig.eigenValue(j-1))) {
            select(j-1) = 1;
          } else {
            RWPOSTCONDITION(j+1<eig.rows() && real(eval)==real(eig.eigenValue(j+1)));  // complex must be in a pair
            select(j+1) = 1;
          }
        }
      }
    }
> endif

  }

  long mm=0;        // compute number of eigenvectors desired
  for(int i=select.length(); i--;) { if (select(i)) mm++; }
  <T>GenMat vl;
  <T>GenMat vr;
  if (computeLeftVecs_)  { vl.reshape(A.rows(),(unsigned)mm); }
  if (computeRightVecs_) { vr.reshape(A.rows(),(unsigned)mm); }
  if (mm>0 && (computeLeftVecs_ || computeRightVecs_)) {
    long n = A.rows();
    char job = (computeLeftVecs_ ? (computeRightVecs_ ? 'B' : 'L') : 'R');
    <T>GenMat H = A.H();
    long ldh = H.colStride();
    long ldvl = rwmax(1,vl.colStride());   // max to cover the case of colStride zero
    long ldvr = rwmax(1,vr.colStride());
    long m;
    long info;
    IntVec selectcopy = select.copy();     // Because dtrevc modifies select
    <T> *work = new <T> [(n+2)*n];
    long *ifaill = new long [mm];
    long *ifailr = new long [mm];
> if <T>==<P>
    <lLetter>hsein(job,'Q','N',selectcopy.data(),n,H.data(),ldh,
                          wr.data(),wi.data(),vl.data(),ldvl,
                          vr.data(),ldvr,mm,m,work,ifaill,ifailr,info) ;
> else
    <P> *rwork = new <P> [n];
    <lLetter>hsein(job,'Q','N',selectcopy.data(),n,H.data(),ldh,
                          lambda(eig).data(),vl.data(),ldvl,
                          vr.data(),ldvr,mm,m,work,rwork,ifaill,ifailr,info) ;
    delete rwork;
> endif
    RWPOSTCONDITION(info>=0);
    if (info>0) {
      accurate(eig) = FALSE;
    }
    RWPOSTCONDITION(m==mm);
    delete work;
    // Now de-balance the eigenvectors
    if (computeLeftVecs_) { P(eig).reference(A.BInvTX(A.QX(vl))); }
    if (computeRightVecs_){ Q(eig).reference(A.BX(A.QX(vr))); }

    // Finally, move the eigenvalues corresponding the the chosen
    // eigenvectors to the front so the positions match up.  Do this
    // in two passes.
    <Z>Vec temp = lambda(eig);
    lambda(eig).deepenShallowCopy();
    int index=0;
    for(int i=0; i<temp.length(); i++) {
      if (select(i)) lambda(eig)(index++) = temp(i);
    }
    for(i=0; i<temp.length(); i++) {
      if (!select(i)) lambda(eig)(index++) = temp(i);
    }
    RWPOSTCONDITION(index==lambda(eig).length());
  }
  return eig;
}

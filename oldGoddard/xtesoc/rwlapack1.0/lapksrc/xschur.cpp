> #################################################################
> # The template file for Schur decomposition class               #
> #                                                               #
> # $Header: /users/rcs/lapksrc/xschur.cpp,v 1.4 1993/10/11 21:48:20 alv Exp $
> #                                                               #
> # Copyright (1993) by Rogue Wave Software, Inc.  This           #
> # software is subject to copyright protection under the laws    #
> # of the United States and other countries.                     #
> #                                                               #
> # Written by Al Vermeulen.                                      #
> #                                                               #
> # Limited License.                                              #
> #################################################################
> #
> # $Log: xschur.cpp,v $
> # Revision 1.4  1993/10/11  21:48:20  alv
> # synch'ed with Math.h++ v5.1
> #
> # Revision 1.3  1993/07/08  23:26:13  alv
> # ported to SUN CC v3
> #
> # Revision 1.2  1993/07/05  19:19:33  alv
> # fixed up for complex numbers
> #
> # Revision 1.1  1993/05/18  17:00:02  alv
> # Initial revision
> #
>
> # Macros that we need
> 
> define <T>=RW_T_RW
> 
> if <T>==Double
> define <P>=Double <tLetter>=d <Z>=DComplex <zLetter>=c <lLetter>=d
> elif <T>==Float
> define <P>=Float  <tLetter>=f <Z>=FComplex <zLetter>=b <lLetter>=s
> elif <T>==DComplex
> define <P>=Double <tLetter>=c <Z>=DComplex <zLetter>=c <lLetter>=z
> else
> err Schur decomposition: not defined for '<T>'
> endif
>
/*
 * defs for <T>SchurDecomp
 *
 * Generated from template $Id: xschur.cpp,v 1.4 1993/10/11 21:48:20 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 */ 

#include "rw/<tLetter>schur.h"
#include "rw/<tLetter>hess.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/lapack.h"
#include "rw/ivec.h"
#include "rw/ivecpik.h"

<T>SchurDecomp::<T>SchurDecomp()
{
}

<T>SchurDecomp::<T>SchurDecomp(const <T>GenMat& A, RWBoolean permute, RWBoolean scale)
{
  factor(A,permute,scale);
}

<T>SchurDecomp::<T>SchurDecomp(const <T>BalanceDecomp& A)
{
  factor(A);
}

<T>SchurDecomp::<T>SchurDecomp(const <T>HessenbergDecomp& A)
{
  factor(A);
}

void <T>SchurDecomp::factor(const <T>GenMat& A, RWBoolean permute, RWBoolean scale)
{
  <T>BalanceDecomp B(A,permute,scale);
  factor(B);
}

void <T>SchurDecomp::factor(const <T>BalanceDecomp& A)
{
  <T>HessenbergDecomp B(A);
  factor(B);
}

RWBoolean <T>SchurDecomp::good() const
{
  return (w_.length()==rows());
}

void <T>SchurDecomp::factor(const <T>HessenbergDecomp& A)
{
  long n = A.rows();
  B_ = A.balanceTransform();
  T_.reference(A.H());
  Z_.reference(A.Q());
  w_.reshape(A.rows());
> if <T>==<P>
  <T>Vec wr(rows(),rwUninitialized);
  <T>Vec wi(rows(),rwUninitialized);
> endif
  long ilo = B_.lowIndex();
  long ihi = B_.highIndex();
  <T> *work = new <T> [n];
  long info;
> if <T>==<P>
  <lLetter>hseqr('S','V',n,ilo,ihi,T_.data(),T_.colStride(),wr.data(),wi.data(),Z_.data(),Z_.colStride(),work,0,info);
> else
  <lLetter>hseqr('S','V',n,ilo,ihi,T_.data(),T_.colStride(),w_.data(),Z_.data(),Z_.colStride(),work,0,info);
> endif
  RWPOSTCONDITION(info>=0);
  if (info>0) {
    RWPOSTCONDITION(ilo-1+n-info<n);
> if <P>==<T>
    // The casts in the following prevent "long passed as int" warnings
    w_.reshape((int)(ilo-1+n-info));
    wr(RWSlice((int)(ilo),(int)(n-info))) = wr(RWToEnd((int)info));
    wr.resize((int)(ilo-1+n-info));
    wi(RWSlice((int)(ilo),(int)(n-info))) = wi(RWToEnd((int)info));
    wi.resize((int)(ilo-1+n-info));
> else
    w_(RWSlice((int)(ilo),(int)(n-info))) = w_(RWToEnd((int)info));
    w_.resize((int)(ilo-1+n-info));
> endif
  }
> if <T>==<P>
  real(w_) = wr;
  imag(w_) = wi;
> endif
  delete work;
}

<T>GenMat <T>SchurDecomp::B() const
{
  <T>GenMat I(rows(),cols(),(<T>)0);
  I.diagonal() = (<T>)1;
  return B_.transform(I);
}

<T>GenMat <T>SchurDecomp::Z() const {return Z_;}
<T>GenMat <T>SchurDecomp::T() const {return T_;}

<Z> <T>SchurDecomp::eigenvalue(int i) const
{
  if (i<0 || i>=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOEIG,i)));
  return w_(i);
}

<Z>Vec <T>SchurDecomp::eigenvalues() const
{
  return w_;
}

<T>Vec <T>SchurDecomp::Bx(const <T>Vec& x) const
{                                         
  return B_.transform(x);
}

<T>Vec <T>SchurDecomp::BInvTx(const <T>Vec& x) const
{
  return B_.invTransform(x);
}

<T>Vec <T>SchurDecomp::Zx(const <T>Vec& x) const
{
  return product(Z_,x);
}

<T>Vec <T>SchurDecomp::ZTx(const <T>Vec& x) const
{
> if <T>==<P>
  return product(x,Z_);
> else
  return conj(product(conj(x),Z_));
> endif
}

<T>GenMat <T>SchurDecomp::BX(const <T>GenMat& X) const
{                                         
  return B_.transform(X);
}
 
<T>GenMat <T>SchurDecomp::BInvTX(const <T>GenMat& X)const
{                                         
  return B_.invTransform(X);
}

<T>GenMat <T>SchurDecomp::ZX(const <T>GenMat& X) const
{
  return product(Z_,X);
}

<T>GenMat <T>SchurDecomp::ZTX(const <T>GenMat& X) const
{
  return transposeProduct(Z_,X);
}

int <T>SchurDecomp::move(int from, int to)
{
  if (!good()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_SCHURREORDER)));
  long n = rows();
  long ifst = from;
  long ilst = to;
  long info;
> if <T>!=<P>
  <lLetter>trexc('V',n,T_.data(),T_.colStride(),Z_.data(),Z_.colStride(),ifst,ilst,info);
> else
  <T> *work = new <T> [n];
  <lLetter>trexc('V',n,T_.data(),T_.colStride(),Z_.data(),Z_.colStride(),ifst,ilst,work,info);
  delete work;
> endif
  RWPOSTCONDITION(info==0 || info==1);
  return (int) ilst;
}

RWBoolean <T>SchurDecomp::moveToFront(const IntVec& indices)
{
  if (!good()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_SCHURREORDER)));
  IntVec select(rows(),0);
  select.pick(indices) = 1;
> if <T>==<P>
  <T>Vec wr(rows(),rwUninitialized);
  <T>Vec wi(rows(),rwUninitialized);
> endif
  long m;
  <P> s,sep;
  long n = rows();
  long lwork = n*(n+1)/2;
  <T> *work = new <T> [lwork];
  long ldt = T_.colStride();
  long ldz = Z_.colStride();
  long info;
> if <P>==<T>
  long *iwork = new long [lwork];
  <lLetter>trsen('N','V',select.data(),n,T_.data(),ldt,Z_.data(),ldz,
                 wr.data(),wi.data(),m,s,sep,work,lwork,iwork,lwork,info);
  delete iwork;
> else
  <lLetter>trsen('N','V',select.data(),n,T_.data(),ldt,Z_.data(),ldz,
                 w_.data(),m,s,sep,work,lwork,info);
> endif
  RWPOSTCONDITION(info==0 || info==1);
  delete work;
> if <T>==<P>
  real(w_) = wr;
  imag(w_) = wi;
> endif
  return info==0;
}

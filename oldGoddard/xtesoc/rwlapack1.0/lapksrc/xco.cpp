> #################################################################
> # The code for the complete orthogonal decomposition routines   #
> #                                                               #
> # $Header: /users/rcs/lapksrc/xco.cpp,v 1.4 1993/10/11 21:48:06 alv Exp $
> #                                                               #
> # Copyright (1991-1993) by Rogue Wave Software, Inc.  This      #
> # software is subject to copyright protection under the laws of #
> # the United States and other countries.                        #
> #                                                               #
> # Written by Al Vermeulen.                                      #
> #                                                               #
> # Limited License.                                              #
> #################################################################
> #
> # $Log: xco.cpp,v $
> # Revision 1.4  1993/10/11  21:48:06  alv
> # synch'ed with Math.h++ v5.1
> #
> # Revision 1.3  1993/07/08  23:26:13  alv
> # ported to SUN CC v3
> #
> # Revision 1.2  1993/07/05  19:19:33  alv
> # fixed up for complex numbers
> #
> # Revision 1.1  1993/05/18  17:00:02  alv
> # Initial revision
> #
>
> define <T>=RW_T_RW
> if <T>==Double
> define <P>=Double <tLetter>=d  <lLetter>=d
> elif <T>==Float 
> define <P>=Float  <tLetter>=f  <lLetter>=s
> elif <T>==DComplex
> define <P>=Double <tLetter>=c  <lLetter>=z
> else
> err orthogonal decomposition: not defined for '<T>'
> endif
>
/*
 * Definitions for <T>CODecomp
 *
 * Generated from template $Id: xco.cpp,v 1.4 1993/10/11 21:48:06 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 */

#include "rw/lapack.h"
#include "rw/<tLetter>co.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/<tLetter>utrimat.h"

<T>CODecomp::<T>CODecomp()
{
}

<T>CODecomp::<T>CODecomp(const <T>CODecomp& x)
{
  *this = x;
}

void <T>CODecomp::operator=(const <T>CODecomp& x)
{
  QRdecomp_ = x.QRdecomp_;
  Ztau_.reference(x.Ztau_);
  TZ_.reference(x.TZ_);
}

void <T>CODecomp::dofactor(double tol)
{
  long n = cols();
  int m = (rows()<cols()) ? rows() : cols();
> if <T>==<P>
  while (m>0 && fabs(QRdecomp_.QR_(m-1,m-1))<tol) m--;
> else
  while (m>0 && norm(QRdecomp_.QR_(m-1,m-1))<tol) m--;
> endif
  TZ_.reference(QRdecomp_.QR_(RWSlice(0,m),RWAll));
  Ztau_.reshape(m);
  long info;
  <lLetter>tzrqf(m,n,TZ_.data(),TZ_.colStride(),Ztau_.data(),info);
  RWPOSTCONDITION(info==0);
}

<T>CODecomp::<T>CODecomp(const <T>GenMat& A, double tol)
  : QRdecomp_(A)
{
  dofactor(tol);   
}

<T>CODecomp::<T>CODecomp(const <T>QRDecomp& A, double tol)
  : QRdecomp_(A)
{
  QRdecomp_.QR_.deepenShallowCopy();
  dofactor(tol);   
}

void <T>CODecomp::factor(const <T>GenMat& A, double tol)
{
  QRdecomp_.factor(A);
  dofactor(tol);   
}

void <T>CODecomp::factor(const <T>QRDecomp& A, double tol)
{
  QRdecomp_ = A;
  QRdecomp_.QR_.deepenShallowCopy();
  dofactor(tol);   
}

<T>CODecomp::~<T>CODecomp()
{
}

<T>UpperTriMat <T>CODecomp::T() const
{
  return toUpperTriMat(TZ_.operator()(RWAll,RWSlice(0,rank())));  // explicit op() to shut up sun 3.0.1 warning
}

// Generate Z as Z = I P1 P2 P3 ... Pn
//
<T>GenMat      <T>CODecomp::Z() const
{
  <T>GenMat Zmat(cols(),cols(),(<T>)0);  // initialize Z to identity matrix
  Zmat.diagonal() = (<T>)1;
  if (cols()>rank()) {
    long m = cols();
    long n = m-rank()+1;
    <T> *work = new <T> [m];
    <T>GenMat ZmatRight = Zmat(RWAll,RWToEnd(rank()));
    <T>GenMat TZRight = TZ_.operator()(RWAll,RWToEnd(rank()));  // explicit op() to shut up sun 3.0.1 warning
    <T> *c2 = ZmatRight.data();
    long ldc = Zmat.colStride();
    long incv = TZRight.colStride();
    for(int i=rank(); i--;) {
      <T> *v = TZRight.row(i).data();
      <T> *c1 = Zmat.col(i).data();
      <lLetter>latzm('R',m,n,v,incv,Ztau_(i),c1,c2,ldc,work);
    }
    delete [] work;
  }
  return Zmat;   
}

<T>Vec <T>CODecomp::Tx(const <T>Vec& x) const
{
  if (x.length()!=rank()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),rank())));
  if (cols()>rank()) {
    <T>UpperTriMat T = toUpperTriMat(TZ_.operator()(RWAll,RWSlice(0,rank())));  // explicit op() to shut up sun 3.0.1 warning
    return product(T,x);
  } else {    // T==R
    return QRdecomp_.Rx(x);
  }
}

<T>Vec <T>CODecomp::TTx(const <T>Vec& x) const
{
  if (x.length()!=rank()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),rank())));
  if (cols()>rank()) {
    <T>UpperTriMat T = toUpperTriMat(TZ_.operator()(RWAll,RWSlice(0,rank())));  // explicit op() to shut up sun 3.0.1 warning
> if <T>==<P>
    return product(x,T);
> else
    return product(x,conj(T));
> endif
  } else {    // T==R
    return QRdecomp_.RTx(x);
  }
}

<T>Vec <T>CODecomp::Tinvx(const <T>Vec& x) const
{
  if (x.length()!=rank()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),rank())));
  <T>Vec y = x.copy();
  long n = rank();
  long lda = TZ_.colStride();
  long one = 1;
  long info;
  <lLetter>trtrs('U','N','N',n,one,(<T>*)TZ_.data(),lda,y.data(),n,info);
  RWPOSTCONDITION(info==0);
  return y;
}

<T>Vec <T>CODecomp::TTinvx(const <T>Vec& x) const
{
  if (x.length()!=rank()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),rank())));
  <T>Vec y = x.copy();
  long n = rank();
  long lda = TZ_.colStride();
  long one = 1;
  long info;
> if <T>!=<P>
  <lLetter>trtrs('U','C','N',n,one,(<T>*)TZ_.data(),lda,y.data(),n,info);
> else
  <lLetter>trtrs('U','T','N',n,one,(<T>*)TZ_.data(),lda,y.data(),n,info);
> endif
  RWPOSTCONDITION(info==0);
  return y;
}

<T>Vec <T>CODecomp::Zx(const <T>Vec& x) const
{
  if (x.length()!=cols()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),cols())));  
  <T>Vec y = x.copy();
  if (cols()>rank()) {
    long n = 1;
    long m = cols()-rank()+1;
    <T> work[1];
    <T>GenMat TZRight = TZ_.operator()(RWAll,RWToEnd(rank()));  // explicit op() to shut up sun 3.0.1 warning
    <T> *c2 = y.data() + rank();
    long ldc = y.length();
    long incv = TZRight.colStride();
    for(int i=0; i<rank(); i++) {
      <T> *c1 = &(y(i));
      <T> *v = TZRight.row(i).data();
      <lLetter>latzm('L',m,n,v,incv,Ztau_(i),c1,c2,ldc,work);
    }
  }
  return y;   
}

<T>Vec <T>CODecomp::ZTx(const <T>Vec& x) const
{
  if (x.length()!=cols()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),cols())));  
> if <T>==<P>
  <T>Vec y = x.copy();
> else
  // To compute Z'x, use relationship y' = x'Z
  <T>Vec y = conj(x);
  y.deepenShallowCopy();
> endif
  if (cols()>rank()) {
    long m = 1;
    long n = cols()-rank()+1;
    <T> work[1];
    <T>GenMat TZRight = TZ_.operator()(RWAll,RWToEnd(rank()));  // explicit op() to shut up sun 3.0.1 warning
    <T> *c2 = y.data() + rank();
    long ldc = 1;
    long incv = TZRight.colStride();
    for(int i=rank(); i--;) {
      <T> *c1 = &(y(i));
      <T> *v = TZRight.row(i).data();
      <lLetter>latzm('R',m,n,v,incv,Ztau_(i),c1,c2,ldc,work);
    }
  }
> if <T>==<P>
  return y;
> else
  return conj(y);   
> endif
}


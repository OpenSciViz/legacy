/*
 * The conversion functions to/from general matrices.
 *
 * Generated from template $Id: xcg.cpp,v 1.2 1993/07/05 19:19:33 alv Exp $
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/cutrimat.h"
#include "rw/cgenmat.h"

DComplexGenMat::DComplexGenMat( const DComplexUpperTriMat& A )
  : RWMatView(A.rows(),A.cols(),sizeof(DComplex))
{
  zero();
  for( int j=0; j<nrows; j++ ) {
    for( int i=0; i<=j; i++ ) {
      set(i,j,A.val(i,j));
    }
  }
}

DComplexUpperTriMat toUpperTriMat( const DComplexGenMat& S )
{
  int n = S.cols();
  DComplexUpperTriMat A(S.rows(),n);
  for( int j=0; j<n; j++ ) {
    for( int i=0; i<=j; i++ ) {
      A.set(i,j,S.val(i,j));
    }
  }
  return A;
}


> # $Header: /users/rcs/lapksrc/xeigsch.cpp,v 1.4 1993/10/11 21:48:10 alv Exp $
> #
> # $Log: xeigsch.cpp,v $
> # Revision 1.4  1993/10/11  21:48:10  alv
> # synch'ed with Math.h++ v5.1
> #
> # Revision 1.3  1993/07/08  23:26:13  alv
> # ported to SUN CC v3
> #
> # Revision 1.2  1993/07/05  19:19:33  alv
> # fixed up for complex numbers
> #
> # Revision 1.1  1993/05/18  17:00:02  alv
> # Initial revision
> #
>
> define <T>=RW_T_RW
> 
> if <T>==Double
> define <P>=Double <tLetter>=d <Z>=DComplex <zLetter>=c <lLetter>=d
> elif <T>==Float
> define <P>=Float  <tLetter>=f <Z>=FComplex <zLetter>=b <lLetter>=s
> elif <T>==DComplex
> define <P>=Double <tLetter>=c <Z>=DComplex <zLetter>=c <lLetter>=z 
> else
> err eigen-decomposition: not defined for '<T>'
> endif
>
/*
 * Implementation of <T>EigSchurServer
 *
 * Generated from template $Id: xeigsch.cpp,v 1.4 1993/10/11 21:48:10 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
  * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */ 

#include "rw/<tLetter>eigsrv.h"
#include "rw/<tLetter>schur.h"
#include "rw/lapack.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"

<T>SchurEigServer::<T>SchurEigServer(RWBoolean left, RWBoolean right, RWBoolean sc, RWBoolean perm)
  : selectRange_((const RWSlice&)RWAll)   // Explicit cast prevents SUN error
{
  computeLeftVecs_  = left;
  computeRightVecs_ = right;
  scale_            = sc;
  permute_          = perm;
}

RWBoolean <T>SchurEigServer::computeLeftEigenVectors() const       {return computeLeftVecs_;}
void      <T>SchurEigServer::computeLeftEigenVectors(RWBoolean x)  {computeLeftVecs_ = x;}
RWBoolean <T>SchurEigServer::computeRightEigenVectors() const      {return computeRightVecs_;}
void      <T>SchurEigServer::computeRightEigenVectors(RWBoolean x) {computeRightVecs_ = x;}
RWBoolean <T>SchurEigServer::scale()                    const      {return scale_;}
void      <T>SchurEigServer::scale(RWBoolean x)                    {scale_ = x;}
RWBoolean <T>SchurEigServer::permute()                  const      {return permute_;}
void      <T>SchurEigServer::permute(RWBoolean x)                  {permute_ = x;}
void      <T>SchurEigServer::balance(RWBoolean x)                  {scale_ = permute_ = x;}

void <T>SchurEigServer::selectEigenVectors(const IntVec& s)
{
  selectVec_.reference(s.copy());
  selectRange_ = RWAll;
}

void <T>SchurEigServer::selectEigenVectors(const RWSlice& s)
{
  selectRange_ = s;
  selectVec_.reshape(0);
}

RWBoolean <T>SchurEigServer::computeAllEigenVectors() const
{
  // The next line needs to determine if the RWSlice object selectRange_
  // is a copy of RWAll.  This means it will have a begin of zero, a stride
  // of one, and a length which matches whatever length the argument vector
  // is.  Test for this by asking for two lengths, if the slice gives back
  // the two lengths, then it must be an RWAll.
  RWBoolean isRWAll = (selectRange_.begin()==0 && selectRange_.stride()==1 && selectRange_.len(5)==5 && selectRange_.len(6)==6);
  return (isRWAll && selectVec_.length()==0);
}

<T>EigDecomp <T>SchurEigServer::operator()(const <T>GenMat& A)
{
  <T>SchurDecomp x(A,permute_,scale_);
  return (*this)(x);
}

<T>EigDecomp <T>SchurEigServer::operator()(const <T>BalanceDecomp& A)
{
  <T>SchurDecomp x(A);
  return (*this)(x);
}

<T>EigDecomp <T>SchurEigServer::operator()(const <T>HessenbergDecomp& A)
{    
  <T>SchurDecomp x(A);
  return (*this)(x);
}

<T>EigDecomp <T>SchurEigServer::operator()(const <T>SchurDecomp& A)
{
  // First build a container and set up the eigenvalues
  <T>EigDecomp eig;
  n(eig) = A.rows();
  lambda(eig).reference(A.eigenvalues());

  if (computeAllEigenVectors()) {
    <T>GenMat vl;
    <T>GenMat vr;
    if (computeLeftVecs_)  { vl.reshape(A.rows(),A.rows()); }
    if (computeRightVecs_) { vr.reshape(A.rows(),A.rows()); }
    if (computeLeftVecs_ || computeRightVecs_) {
      char job = (computeLeftVecs_ ? (computeRightVecs_ ? 'B' : 'L') : 'R');
      <T>GenMat T = A.T();
      long ldvl = rwmax(1,vl.colStride());   // max to cover the case of colStride zero
      long ldvr = rwmax(1,vr.colStride());
      long m;
      long info;
      <T> *work = new <T> [3*A.rows()];
> if <T>==<P>
      <lLetter>trevc(job,'A',0,A.rows(),T.data(),T.colStride(),vl.data(),ldvl,
                            vr.data(),ldvr,A.rows(),m,work,info);
> else
      <P> *rwork = new <P> [A.rows()];
      <lLetter>trevc(job,'A',0,A.rows(),T.data(),T.colStride(),vl.data(),ldvl,
                            vr.data(),ldvr,A.rows(),m,work,rwork,info);
      delete [] rwork;
> endif
      RWPOSTCONDITION(info==0);
      delete work;
      // Now transform and de-balance the eigenvectors
      if (computeLeftVecs_) { P(eig).reference(A.BInvTX(A.ZX(vl))); }
      if (computeRightVecs_){ Q(eig).reference(A.BX(A.ZX(vr))); }
    }
  } else {   

    // Compute only selected eigenvectors
    IntVec select(n(eig),0);
    if (selectVec_.length()!=0) {
      for(int i=selectVec_.length(); i--;) {
        int x = selectVec_(i);
        if (x<0 || x>=n(eig)) RWTHROW(RWBoundsErr(RWMessage(RWLAPK_NOEIG,x)));
        select(x) = 1;
      }
    } else {  // which to compute is indicated by the RWSlice
      selectRange_.boundsCheck(n(eig));
      select(selectRange_) = 1;
    }

> if <Z>!=<T>
    // Make sure that both halves of complex conjugate eigenvals are chosen
    for(int j=select.length(); j--;) {
      if (select(j)) {
        <Z> eval = eig.eigenValue(j);
        if (imag(eval)!=0) {
          if (j>0 && real(eval)==real(eig.eigenValue(j-1))) {
            select(j-1) = 1;
          } else {
            RWPOSTCONDITION(j+1<eig.rows() && real(eval)==real(eig.eigenValue(j+1)));  // complex must be in a pair
            select(j+1) = 1;
          }
        }
      }
    }

> endif
    long mm=0;        // compute number of eigenvectors desired
    for(int i=select.length(); i--;) { if (select(i)) mm++; }
    <T>GenMat vl;
    <T>GenMat vr;
    if (computeLeftVecs_)  { vl.reshape(A.rows(),(unsigned)mm); }
    if (computeRightVecs_) { vr.reshape(A.rows(),(unsigned)mm); }
    if (mm>0 && (computeLeftVecs_ || computeRightVecs_)) {
      char job = (computeLeftVecs_ ? (computeRightVecs_ ? 'B' : 'L') : 'R');
      <T>GenMat T = A.T();
      long ldvl = rwmax(1,vl.colStride());   // max to cover the case of colStride zero
      long ldvr = rwmax(1,vr.colStride());
      long m;
      long info;
      IntVec selectcopy = select.copy();     // Because dtrevc modifies select
      <T> *work = new <T> [3*A.rows()];
> if <T>==<P>
      <lLetter>trevc(job,'S',selectcopy.data(),A.rows(),T.data(),T.colStride(),vl.data(),ldvl,
                            vr.data(),ldvr,mm,m,work,info);
> else
      <P> *rwork = new <P> [A.rows()];
      <lLetter>trevc(job,'S',selectcopy.data(),A.rows(),T.data(),T.colStride(),vl.data(),ldvl,
                            vr.data(),ldvr,mm,m,work,rwork,info);
      delete rwork;
> endif
      RWPOSTCONDITION(info==0);
      RWPOSTCONDITION(m==mm);
      delete work;
      // Now transform and de-balance the eigenvectors
      if (computeLeftVecs_) { P(eig).reference(A.BInvTX(A.ZX(vl))); }
      if (computeRightVecs_){ Q(eig).reference(A.BX(A.ZX(vr))); }

      // Finally, move the eigenvalues corresponding the the chosen
      // eigenvectors to the front so the positions match up.  Do this
      // in two passes.
      <Z>Vec temp = lambda(eig);
      lambda(eig).deepenShallowCopy();
      int index=0;
      for(int i=0; i<temp.length(); i++) {
        if (select(i)) lambda(eig)(index++) = temp(i);
      }
      for(i=0; i<temp.length(); i++) {
        if (!select(i)) lambda(eig)(index++) = temp(i);
      }
      RWPOSTCONDITION(index==lambda(eig).length());
    }
  }
  return eig;
}

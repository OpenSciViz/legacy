/*
 * Implementation of FloatSymTriDiagDecomp
 *
 * Generated from template $Id: xtd.cpp,v 1.2 1993/10/11 21:48:26 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */ 

#include "rw/ftd.h"
#include "rw/lapkerr.h"
#include "rw/rwerr.h"

void FloatSymTriDiagDecomp::set(const FloatVec& diag_, const FloatVec& offdiag_)
{
  if (offdiag_.length()+1 != diag_.length()) {
    RWTHROW(RWInternalErr(RWMessage(RWLAPK_OFFDIAG,offdiag_.length(),diag_.length()-1)));
  }
  diag.reference(diag_);
  offdiag.reference(offdiag_);
}

FloatVec FloatSymTriDiagDecomp::transform(const FloatVec& x) const
{
  FloatGenMat C(x,x.length(),1);
  FloatGenMat QC = transform(C);
  RWPOSTCONDITION(QC.cols()==1);
  return QC.col(0);
}
  


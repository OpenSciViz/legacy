> #   D O   N O T   E D I T   T H I S   F I L E
>
> # This file is created by running the script tempexp.ksh
>

$(RWDIR)/dref.h: xref.h macros
	$(MATHPP) xref.h $(RWDIR)/dref.h RW_C_RW=DoubleSymMat
$(RWDIR)/fref.h: xref.h macros
	$(MATHPP) xref.h $(RWDIR)/fref.h RW_C_RW=FloatSymMat
$(RWDIR)/cref.h: xref.h macros
	$(MATHPP) xref.h $(RWDIR)/cref.h RW_C_RW=DComplexSymMat
$(RWDIR)/igenmat.h: xmat.h macros
	$(MATHPP) xmat.h $@ RW_C_RW=IntGenMat
$(MATHSRC)/igenmat.cpp: xmat.cpp macros
	$(MATHPP) xmat.cpp $@ RW_C_RW=IntGenMat
$(MATHSRC)/igenmth.cpp: xmth.cpp macros
	$(MATHPP) xmth.cpp $@ RW_C_RW=IntGenMat
$(MATHSRC)/igenct.cpp: xct.cpp macros
	$(MATHPP) xct.cpp $@ RW_C_RW=IntGenMat
$(MATHSRC)/igencs.cpp: xcs.cpp macros
	$(MATHPP) xcs.cpp $@ RW_C_RW=IntGenMat
$(MATHSRC)/igencg.cpp: xcg.cpp macros
	$(MATHPP) xcg.cpp $@ RW_C_RW=IntGenMat
$(RWDIR)/scgenmat.h: xmat.h macros
	$(MATHPP) xmat.h $@ RW_C_RW=SCharGenMat
$(MATHSRC)/scgenmat.cpp: xmat.cpp macros
	$(MATHPP) xmat.cpp $@ RW_C_RW=SCharGenMat
$(MATHSRC)/scgenmth.cpp: xmth.cpp macros
	$(MATHPP) xmth.cpp $@ RW_C_RW=SCharGenMat
$(MATHSRC)/scgenct.cpp: xct.cpp macros
	$(MATHPP) xct.cpp $@ RW_C_RW=SCharGenMat
$(MATHSRC)/scgencs.cpp: xcs.cpp macros
	$(MATHPP) xcs.cpp $@ RW_C_RW=SCharGenMat
$(MATHSRC)/scgencg.cpp: xcg.cpp macros
	$(MATHPP) xcg.cpp $@ RW_C_RW=SCharGenMat
$(RWDIR)/ucgenmat.h: xmat.h macros
	$(MATHPP) xmat.h $@ RW_C_RW=UCharGenMat
$(MATHSRC)/ucgenmat.cpp: xmat.cpp macros
	$(MATHPP) xmat.cpp $@ RW_C_RW=UCharGenMat
$(MATHSRC)/ucgenmth.cpp: xmth.cpp macros
	$(MATHPP) xmth.cpp $@ RW_C_RW=UCharGenMat
$(MATHSRC)/ucgenct.cpp: xct.cpp macros
	$(MATHPP) xct.cpp $@ RW_C_RW=UCharGenMat
$(MATHSRC)/ucgencs.cpp: xcs.cpp macros
	$(MATHPP) xcs.cpp $@ RW_C_RW=UCharGenMat
$(MATHSRC)/ucgencg.cpp: xcg.cpp macros
	$(MATHPP) xcg.cpp $@ RW_C_RW=UCharGenMat
$(RWDIR)/dgenmat.h: xmat.h macros
	$(MATHPP) xmat.h $@ RW_C_RW=DoubleGenMat
$(MATHSRC)/dgenmat.cpp: xmat.cpp macros
	$(MATHPP) xmat.cpp $@ RW_C_RW=DoubleGenMat
$(MATHSRC)/dgenmth.cpp: xmth.cpp macros
	$(MATHPP) xmth.cpp $@ RW_C_RW=DoubleGenMat
$(MATHSRC)/dgenct.cpp: xct.cpp macros
	$(MATHPP) xct.cpp $@ RW_C_RW=DoubleGenMat
$(MATHSRC)/dgencs.cpp: xcs.cpp macros
	$(MATHPP) xcs.cpp $@ RW_C_RW=DoubleGenMat
$(MATHSRC)/dgencg.cpp: xcg.cpp macros
	$(MATHPP) xcg.cpp $@ RW_C_RW=DoubleGenMat
$(RWDIR)/fgenmat.h: xmat.h macros
	$(MATHPP) xmat.h $@ RW_C_RW=FloatGenMat
$(MATHSRC)/fgenmat.cpp: xmat.cpp macros
	$(MATHPP) xmat.cpp $@ RW_C_RW=FloatGenMat
$(MATHSRC)/fgenmth.cpp: xmth.cpp macros
	$(MATHPP) xmth.cpp $@ RW_C_RW=FloatGenMat
$(MATHSRC)/fgenct.cpp: xct.cpp macros
	$(MATHPP) xct.cpp $@ RW_C_RW=FloatGenMat
$(MATHSRC)/fgencs.cpp: xcs.cpp macros
	$(MATHPP) xcs.cpp $@ RW_C_RW=FloatGenMat
$(MATHSRC)/fgencg.cpp: xcg.cpp macros
	$(MATHPP) xcg.cpp $@ RW_C_RW=FloatGenMat
$(RWDIR)/cgenmat.h: xmat.h macros
	$(MATHPP) xmat.h $@ RW_C_RW=DComplexGenMat
$(MATHSRC)/cgenmat.cpp: xmat.cpp macros
	$(MATHPP) xmat.cpp $@ RW_C_RW=DComplexGenMat
$(MATHSRC)/cgenmth.cpp: xmth.cpp macros
	$(MATHPP) xmth.cpp $@ RW_C_RW=DComplexGenMat
$(MATHSRC)/cgenct.cpp: xct.cpp macros
	$(MATHPP) xct.cpp $@ RW_C_RW=DComplexGenMat
$(MATHSRC)/cgencs.cpp: xcs.cpp macros
	$(MATHPP) xcs.cpp $@ RW_C_RW=DComplexGenMat
$(MATHSRC)/cgencg.cpp: xcg.cpp macros
	$(MATHPP) xcg.cpp $@ RW_C_RW=DComplexGenMat
$(RWDIR)/dsymmat.h: xmat.h macros
	$(MATHPP) xmat.h $@ RW_C_RW=DoubleSymMat
dsymmat.cpp: xmat.cpp macros
	$(MATHPP) xmat.cpp $@ RW_C_RW=DoubleSymMat
dsymmth.cpp: xmth.cpp macros
	$(MATHPP) xmth.cpp $@ RW_C_RW=DoubleSymMat
dsymct.cpp: xct.cpp macros
	$(MATHPP) xct.cpp $@ RW_C_RW=DoubleSymMat
dsymcs.cpp: xcs.cpp macros
	$(MATHPP) xcs.cpp $@ RW_C_RW=DoubleSymMat
dsymcg.cpp: xcg.cpp macros
	$(MATHPP) xcg.cpp $@ RW_C_RW=DoubleSymMat
$(RWDIR)/fsymmat.h: xmat.h macros
	$(MATHPP) xmat.h $@ RW_C_RW=FloatSymMat
fsymmat.cpp: xmat.cpp macros
	$(MATHPP) xmat.cpp $@ RW_C_RW=FloatSymMat
fsymmth.cpp: xmth.cpp macros
	$(MATHPP) xmth.cpp $@ RW_C_RW=FloatSymMat
fsymct.cpp: xct.cpp macros
	$(MATHPP) xct.cpp $@ RW_C_RW=FloatSymMat
fsymcs.cpp: xcs.cpp macros
	$(MATHPP) xcs.cpp $@ RW_C_RW=FloatSymMat
fsymcg.cpp: xcg.cpp macros
	$(MATHPP) xcg.cpp $@ RW_C_RW=FloatSymMat
$(RWDIR)/csymmat.h: xmat.h macros
	$(MATHPP) xmat.h $@ RW_C_RW=DComplexSymMat
csymmat.cpp: xmat.cpp macros
	$(MATHPP) xmat.cpp $@ RW_C_RW=DComplexSymMat
csymmth.cpp: xmth.cpp macros
	$(MATHPP) xmth.cpp $@ RW_C_RW=DComplexSymMat
csymct.cpp: xct.cpp macros
	$(MATHPP) xct.cpp $@ RW_C_RW=DComplexSymMat
csymcs.cpp: xcs.cpp macros
	$(MATHPP) xcs.cpp $@ RW_C_RW=DComplexSymMat
csymcg.cpp: xcg.cpp macros
	$(MATHPP) xcg.cpp $@ RW_C_RW=DComplexSymMat
$(RWDIR)/chermmat.h: xmat.h macros
	$(MATHPP) xmat.h $@ RW_C_RW=DComplexHermMat
chermmat.cpp: xmat.cpp macros
	$(MATHPP) xmat.cpp $@ RW_C_RW=DComplexHermMat
chermmth.cpp: xmth.cpp macros
	$(MATHPP) xmth.cpp $@ RW_C_RW=DComplexHermMat
chermct.cpp: xct.cpp macros
	$(MATHPP) xct.cpp $@ RW_C_RW=DComplexHermMat
chermcs.cpp: xcs.cpp macros
	$(MATHPP) xcs.cpp $@ RW_C_RW=DComplexHermMat
chermcg.cpp: xcg.cpp macros
	$(MATHPP) xcg.cpp $@ RW_C_RW=DComplexHermMat
$(RWDIR)/dskewmat.h: xmat.h macros
	$(MATHPP) xmat.h $@ RW_C_RW=DoubleSkewMat
dskewmat.cpp: xmat.cpp macros
	$(MATHPP) xmat.cpp $@ RW_C_RW=DoubleSkewMat
dskewmth.cpp: xmth.cpp macros
	$(MATHPP) xmth.cpp $@ RW_C_RW=DoubleSkewMat
dskewct.cpp: xct.cpp macros
	$(MATHPP) xct.cpp $@ RW_C_RW=DoubleSkewMat
dskewcs.cpp: xcs.cpp macros
	$(MATHPP) xcs.cpp $@ RW_C_RW=DoubleSkewMat
dskewcg.cpp: xcg.cpp macros
	$(MATHPP) xcg.cpp $@ RW_C_RW=DoubleSkewMat
$(RWDIR)/fskewmat.h: xmat.h macros
	$(MATHPP) xmat.h $@ RW_C_RW=FloatSkewMat
fskewmat.cpp: xmat.cpp macros
	$(MATHPP) xmat.cpp $@ RW_C_RW=FloatSkewMat
fskewmth.cpp: xmth.cpp macros
	$(MATHPP) xmth.cpp $@ RW_C_RW=FloatSkewMat
fskewct.cpp: xct.cpp macros
	$(MATHPP) xct.cpp $@ RW_C_RW=FloatSkewMat
fskewcs.cpp: xcs.cpp macros
	$(MATHPP) xcs.cpp $@ RW_C_RW=FloatSkewMat
fskewcg.cpp: xcg.cpp macros
	$(MATHPP) xcg.cpp $@ RW_C_RW=FloatSkewMat
$(RWDIR)/cskewmat.h: xmat.h macros
	$(MATHPP) xmat.h $@ RW_C_RW=DComplexSkewMat
cskewmat.cpp: xmat.cpp macros
	$(MATHPP) xmat.cpp $@ RW_C_RW=DComplexSkewMat
cskewmth.cpp: xmth.cpp macros
	$(MATHPP) xmth.cpp $@ RW_C_RW=DComplexSkewMat
cskewct.cpp: xct.cpp macros
	$(MATHPP) xct.cpp $@ RW_C_RW=DComplexSkewMat
cskewcs.cpp: xcs.cpp macros
	$(MATHPP) xcs.cpp $@ RW_C_RW=DComplexSkewMat
cskewcg.cpp: xcg.cpp macros
	$(MATHPP) xcg.cpp $@ RW_C_RW=DComplexSkewMat
$(RWDIR)/dbandmat.h: xmat.h macros
	$(MATHPP) xmat.h $@ RW_C_RW=DoubleBandMat
dbandmat.cpp: xmat.cpp macros
	$(MATHPP) xmat.cpp $@ RW_C_RW=DoubleBandMat
dbandmth.cpp: xmth.cpp macros
	$(MATHPP) xmth.cpp $@ RW_C_RW=DoubleBandMat
dbandct.cpp: xct.cpp macros
	$(MATHPP) xct.cpp $@ RW_C_RW=DoubleBandMat
dbandcs.cpp: xcs.cpp macros
	$(MATHPP) xcs.cpp $@ RW_C_RW=DoubleBandMat
dbandcg.cpp: xcg.cpp macros
	$(MATHPP) xcg.cpp $@ RW_C_RW=DoubleBandMat
$(RWDIR)/fbandmat.h: xmat.h macros
	$(MATHPP) xmat.h $@ RW_C_RW=FloatBandMat
fbandmat.cpp: xmat.cpp macros
	$(MATHPP) xmat.cpp $@ RW_C_RW=FloatBandMat
fbandmth.cpp: xmth.cpp macros
	$(MATHPP) xmth.cpp $@ RW_C_RW=FloatBandMat
fbandct.cpp: xct.cpp macros
	$(MATHPP) xct.cpp $@ RW_C_RW=FloatBandMat
fbandcs.cpp: xcs.cpp macros
	$(MATHPP) xcs.cpp $@ RW_C_RW=FloatBandMat
fbandcg.cpp: xcg.cpp macros
	$(MATHPP) xcg.cpp $@ RW_C_RW=FloatBandMat
$(RWDIR)/cbandmat.h: xmat.h macros
	$(MATHPP) xmat.h $@ RW_C_RW=DComplexBandMat
cbandmat.cpp: xmat.cpp macros
	$(MATHPP) xmat.cpp $@ RW_C_RW=DComplexBandMat
cbandmth.cpp: xmth.cpp macros
	$(MATHPP) xmth.cpp $@ RW_C_RW=DComplexBandMat
cbandct.cpp: xct.cpp macros
	$(MATHPP) xct.cpp $@ RW_C_RW=DComplexBandMat
cbandcs.cpp: xcs.cpp macros
	$(MATHPP) xcs.cpp $@ RW_C_RW=DComplexBandMat
cbandcg.cpp: xcg.cpp macros
	$(MATHPP) xcg.cpp $@ RW_C_RW=DComplexBandMat
$(RWDIR)/dsbndmat.h: xmat.h macros
	$(MATHPP) xmat.h $@ RW_C_RW=DoubleSymBandMat
dsbndmat.cpp: xmat.cpp macros
	$(MATHPP) xmat.cpp $@ RW_C_RW=DoubleSymBandMat
dsbndmth.cpp: xmth.cpp macros
	$(MATHPP) xmth.cpp $@ RW_C_RW=DoubleSymBandMat
dsbndct.cpp: xct.cpp macros
	$(MATHPP) xct.cpp $@ RW_C_RW=DoubleSymBandMat
dsbndcs.cpp: xcs.cpp macros
	$(MATHPP) xcs.cpp $@ RW_C_RW=DoubleSymBandMat
dsbndcg.cpp: xcg.cpp macros
	$(MATHPP) xcg.cpp $@ RW_C_RW=DoubleSymBandMat
$(RWDIR)/fsbndmat.h: xmat.h macros
	$(MATHPP) xmat.h $@ RW_C_RW=FloatSymBandMat
fsbndmat.cpp: xmat.cpp macros
	$(MATHPP) xmat.cpp $@ RW_C_RW=FloatSymBandMat
fsbndmth.cpp: xmth.cpp macros
	$(MATHPP) xmth.cpp $@ RW_C_RW=FloatSymBandMat
fsbndct.cpp: xct.cpp macros
	$(MATHPP) xct.cpp $@ RW_C_RW=FloatSymBandMat
fsbndcs.cpp: xcs.cpp macros
	$(MATHPP) xcs.cpp $@ RW_C_RW=FloatSymBandMat
fsbndcg.cpp: xcg.cpp macros
	$(MATHPP) xcg.cpp $@ RW_C_RW=FloatSymBandMat
$(RWDIR)/csbndmat.h: xmat.h macros
	$(MATHPP) xmat.h $@ RW_C_RW=DComplexSymBandMat
csbndmat.cpp: xmat.cpp macros
	$(MATHPP) xmat.cpp $@ RW_C_RW=DComplexSymBandMat
csbndmth.cpp: xmth.cpp macros
	$(MATHPP) xmth.cpp $@ RW_C_RW=DComplexSymBandMat
csbndct.cpp: xct.cpp macros
	$(MATHPP) xct.cpp $@ RW_C_RW=DComplexSymBandMat
csbndcs.cpp: xcs.cpp macros
	$(MATHPP) xcs.cpp $@ RW_C_RW=DComplexSymBandMat
csbndcg.cpp: xcg.cpp macros
	$(MATHPP) xcg.cpp $@ RW_C_RW=DComplexSymBandMat
$(RWDIR)/chbndmat.h: xmat.h macros
	$(MATHPP) xmat.h $@ RW_C_RW=DComplexHermBandMat
chbndmat.cpp: xmat.cpp macros
	$(MATHPP) xmat.cpp $@ RW_C_RW=DComplexHermBandMat
chbndmth.cpp: xmth.cpp macros
	$(MATHPP) xmth.cpp $@ RW_C_RW=DComplexHermBandMat
chbndct.cpp: xct.cpp macros
	$(MATHPP) xct.cpp $@ RW_C_RW=DComplexHermBandMat
chbndcs.cpp: xcs.cpp macros
	$(MATHPP) xcs.cpp $@ RW_C_RW=DComplexHermBandMat
chbndcg.cpp: xcg.cpp macros
	$(MATHPP) xcg.cpp $@ RW_C_RW=DComplexHermBandMat
$(RWDIR)/dutrimat.h: xmat.h macros
	$(MATHPP) xmat.h $@ RW_C_RW=DoubleUpperTriMat
dutrimat.cpp: xmat.cpp macros
	$(MATHPP) xmat.cpp $@ RW_C_RW=DoubleUpperTriMat
dutrimth.cpp: xmth.cpp macros
	$(MATHPP) xmth.cpp $@ RW_C_RW=DoubleUpperTriMat
dutrict.cpp: xct.cpp macros
	$(MATHPP) xct.cpp $@ RW_C_RW=DoubleUpperTriMat
dutrics.cpp: xcs.cpp macros
	$(MATHPP) xcs.cpp $@ RW_C_RW=DoubleUpperTriMat
dutricg.cpp: xcg.cpp macros
	$(MATHPP) xcg.cpp $@ RW_C_RW=DoubleUpperTriMat
$(RWDIR)/futrimat.h: xmat.h macros
	$(MATHPP) xmat.h $@ RW_C_RW=FloatUpperTriMat
futrimat.cpp: xmat.cpp macros
	$(MATHPP) xmat.cpp $@ RW_C_RW=FloatUpperTriMat
futrimth.cpp: xmth.cpp macros
	$(MATHPP) xmth.cpp $@ RW_C_RW=FloatUpperTriMat
futrict.cpp: xct.cpp macros
	$(MATHPP) xct.cpp $@ RW_C_RW=FloatUpperTriMat
futrics.cpp: xcs.cpp macros
	$(MATHPP) xcs.cpp $@ RW_C_RW=FloatUpperTriMat
futricg.cpp: xcg.cpp macros
	$(MATHPP) xcg.cpp $@ RW_C_RW=FloatUpperTriMat
$(RWDIR)/cutrimat.h: xmat.h macros
	$(MATHPP) xmat.h $@ RW_C_RW=DComplexUpperTriMat
cutrimat.cpp: xmat.cpp macros
	$(MATHPP) xmat.cpp $@ RW_C_RW=DComplexUpperTriMat
cutrimth.cpp: xmth.cpp macros
	$(MATHPP) xmth.cpp $@ RW_C_RW=DComplexUpperTriMat
cutrict.cpp: xct.cpp macros
	$(MATHPP) xct.cpp $@ RW_C_RW=DComplexUpperTriMat
cutrics.cpp: xcs.cpp macros
	$(MATHPP) xcs.cpp $@ RW_C_RW=DComplexUpperTriMat
cutricg.cpp: xcg.cpp macros
	$(MATHPP) xcg.cpp $@ RW_C_RW=DComplexUpperTriMat
$(RWDIR)/dltrimat.h: xmat.h macros
	$(MATHPP) xmat.h $@ RW_C_RW=DoubleLowerTriMat
dltrimat.cpp: xmat.cpp macros
	$(MATHPP) xmat.cpp $@ RW_C_RW=DoubleLowerTriMat
dltrimth.cpp: xmth.cpp macros
	$(MATHPP) xmth.cpp $@ RW_C_RW=DoubleLowerTriMat
dltrict.cpp: xct.cpp macros
	$(MATHPP) xct.cpp $@ RW_C_RW=DoubleLowerTriMat
dltrics.cpp: xcs.cpp macros
	$(MATHPP) xcs.cpp $@ RW_C_RW=DoubleLowerTriMat
dltricg.cpp: xcg.cpp macros
	$(MATHPP) xcg.cpp $@ RW_C_RW=DoubleLowerTriMat
$(RWDIR)/fltrimat.h: xmat.h macros
	$(MATHPP) xmat.h $@ RW_C_RW=FloatLowerTriMat
fltrimat.cpp: xmat.cpp macros
	$(MATHPP) xmat.cpp $@ RW_C_RW=FloatLowerTriMat
fltrimth.cpp: xmth.cpp macros
	$(MATHPP) xmth.cpp $@ RW_C_RW=FloatLowerTriMat
fltrict.cpp: xct.cpp macros
	$(MATHPP) xct.cpp $@ RW_C_RW=FloatLowerTriMat
fltrics.cpp: xcs.cpp macros
	$(MATHPP) xcs.cpp $@ RW_C_RW=FloatLowerTriMat
fltricg.cpp: xcg.cpp macros
	$(MATHPP) xcg.cpp $@ RW_C_RW=FloatLowerTriMat
$(RWDIR)/cltrimat.h: xmat.h macros
	$(MATHPP) xmat.h $@ RW_C_RW=DComplexLowerTriMat
cltrimat.cpp: xmat.cpp macros
	$(MATHPP) xmat.cpp $@ RW_C_RW=DComplexLowerTriMat
cltrimth.cpp: xmth.cpp macros
	$(MATHPP) xmth.cpp $@ RW_C_RW=DComplexLowerTriMat
cltrict.cpp: xct.cpp macros
	$(MATHPP) xct.cpp $@ RW_C_RW=DComplexLowerTriMat
cltrics.cpp: xcs.cpp macros
	$(MATHPP) xcs.cpp $@ RW_C_RW=DComplexLowerTriMat
cltricg.cpp: xcg.cpp macros
	$(MATHPP) xcg.cpp $@ RW_C_RW=DComplexLowerTriMat
$(RWDIR)/dtrdgmat.h: xmat.h macros
	$(MATHPP) xmat.h $@ RW_C_RW=DoubleTriDiagMat
dtrdgmat.cpp: xmat.cpp macros
	$(MATHPP) xmat.cpp $@ RW_C_RW=DoubleTriDiagMat
dtrdgmth.cpp: xmth.cpp macros
	$(MATHPP) xmth.cpp $@ RW_C_RW=DoubleTriDiagMat
dtrdgct.cpp: xct.cpp macros
	$(MATHPP) xct.cpp $@ RW_C_RW=DoubleTriDiagMat
dtrdgcs.cpp: xcs.cpp macros
	$(MATHPP) xcs.cpp $@ RW_C_RW=DoubleTriDiagMat
dtrdgcg.cpp: xcg.cpp macros
	$(MATHPP) xcg.cpp $@ RW_C_RW=DoubleTriDiagMat
$(RWDIR)/ftrdgmat.h: xmat.h macros
	$(MATHPP) xmat.h $@ RW_C_RW=FloatTriDiagMat
ftrdgmat.cpp: xmat.cpp macros
	$(MATHPP) xmat.cpp $@ RW_C_RW=FloatTriDiagMat
ftrdgmth.cpp: xmth.cpp macros
	$(MATHPP) xmth.cpp $@ RW_C_RW=FloatTriDiagMat
ftrdgct.cpp: xct.cpp macros
	$(MATHPP) xct.cpp $@ RW_C_RW=FloatTriDiagMat
ftrdgcs.cpp: xcs.cpp macros
	$(MATHPP) xcs.cpp $@ RW_C_RW=FloatTriDiagMat
ftrdgcg.cpp: xcg.cpp macros
	$(MATHPP) xcg.cpp $@ RW_C_RW=FloatTriDiagMat
$(RWDIR)/ctrdgmat.h: xmat.h macros
	$(MATHPP) xmat.h $@ RW_C_RW=DComplexTriDiagMat
ctrdgmat.cpp: xmat.cpp macros
	$(MATHPP) xmat.cpp $@ RW_C_RW=DComplexTriDiagMat
ctrdgmth.cpp: xmth.cpp macros
	$(MATHPP) xmth.cpp $@ RW_C_RW=DComplexTriDiagMat
ctrdgct.cpp: xct.cpp macros
	$(MATHPP) xct.cpp $@ RW_C_RW=DComplexTriDiagMat
ctrdgcs.cpp: xcs.cpp macros
	$(MATHPP) xcs.cpp $@ RW_C_RW=DComplexTriDiagMat
ctrdgcg.cpp: xcg.cpp macros
	$(MATHPP) xcg.cpp $@ RW_C_RW=DComplexTriDiagMat
$(RWDIR)/dgenfct.h: xfct.h fct.mac
	$(MATHPP) xfct.h $@ RW_C_RW=DoubleGenFact
dgenfct.cpp: xfct.cpp fct.mac
	$(MATHPP) xfct.cpp $@ RW_C_RW=DoubleGenFact
$(RWDIR)/fgenfct.h: xfct.h fct.mac
	$(MATHPP) xfct.h $@ RW_C_RW=FloatGenFact
fgenfct.cpp: xfct.cpp fct.mac
	$(MATHPP) xfct.cpp $@ RW_C_RW=FloatGenFact
$(RWDIR)/cgenfct.h: xfct.h fct.mac
	$(MATHPP) xfct.h $@ RW_C_RW=DComplexGenFact
cgenfct.cpp: xfct.cpp fct.mac
	$(MATHPP) xfct.cpp $@ RW_C_RW=DComplexGenFact
$(RWDIR)/dsymfct.h: xfct.h fct.mac
	$(MATHPP) xfct.h $@ RW_C_RW=DoubleSymFact
dsymfct.cpp: xfct.cpp fct.mac
	$(MATHPP) xfct.cpp $@ RW_C_RW=DoubleSymFact
$(RWDIR)/fsymfct.h: xfct.h fct.mac
	$(MATHPP) xfct.h $@ RW_C_RW=FloatSymFact
fsymfct.cpp: xfct.cpp fct.mac
	$(MATHPP) xfct.cpp $@ RW_C_RW=FloatSymFact
$(RWDIR)/csymfct.h: xfct.h fct.mac
	$(MATHPP) xfct.h $@ RW_C_RW=DComplexSymFact
csymfct.cpp: xfct.cpp fct.mac
	$(MATHPP) xfct.cpp $@ RW_C_RW=DComplexSymFact
$(RWDIR)/chermfct.h: xfct.h fct.mac
	$(MATHPP) xfct.h $@ RW_C_RW=DComplexHermFact
chermfct.cpp: xfct.cpp fct.mac
	$(MATHPP) xfct.cpp $@ RW_C_RW=DComplexHermFact
$(RWDIR)/dpdfct.h: xfct.h fct.mac
	$(MATHPP) xfct.h $@ RW_C_RW=DoublePDFact
dpdfct.cpp: xfct.cpp fct.mac
	$(MATHPP) xfct.cpp $@ RW_C_RW=DoublePDFact
$(RWDIR)/fpdfct.h: xfct.h fct.mac
	$(MATHPP) xfct.h $@ RW_C_RW=FloatPDFact
fpdfct.cpp: xfct.cpp fct.mac
	$(MATHPP) xfct.cpp $@ RW_C_RW=FloatPDFact
$(RWDIR)/cpdfct.h: xfct.h fct.mac
	$(MATHPP) xfct.h $@ RW_C_RW=DComplexPDFact
cpdfct.cpp: xfct.cpp fct.mac
	$(MATHPP) xfct.cpp $@ RW_C_RW=DComplexPDFact
$(RWDIR)/dbandfct.h: xfct.h fct.mac
	$(MATHPP) xfct.h $@ RW_C_RW=DoubleBandFact
dbandfct.cpp: xfct.cpp fct.mac
	$(MATHPP) xfct.cpp $@ RW_C_RW=DoubleBandFact
$(RWDIR)/fbandfct.h: xfct.h fct.mac
	$(MATHPP) xfct.h $@ RW_C_RW=FloatBandFact
fbandfct.cpp: xfct.cpp fct.mac
	$(MATHPP) xfct.cpp $@ RW_C_RW=FloatBandFact
$(RWDIR)/cbandfct.h: xfct.h fct.mac
	$(MATHPP) xfct.h $@ RW_C_RW=DComplexBandFact
cbandfct.cpp: xfct.cpp fct.mac
	$(MATHPP) xfct.cpp $@ RW_C_RW=DComplexBandFact
$(RWDIR)/dpdbdfct.h: xfct.h fct.mac
	$(MATHPP) xfct.h $@ RW_C_RW=DoublePDBandFact
dpdbdfct.cpp: xfct.cpp fct.mac
	$(MATHPP) xfct.cpp $@ RW_C_RW=DoublePDBandFact
$(RWDIR)/fpdbdfct.h: xfct.h fct.mac
	$(MATHPP) xfct.h $@ RW_C_RW=FloatPDBandFact
fpdbdfct.cpp: xfct.cpp fct.mac
	$(MATHPP) xfct.cpp $@ RW_C_RW=FloatPDBandFact
$(RWDIR)/cpdbdfct.h: xfct.h fct.mac
	$(MATHPP) xfct.h $@ RW_C_RW=DComplexPDBandFact
cpdbdfct.cpp: xfct.cpp fct.mac
	$(MATHPP) xfct.cpp $@ RW_C_RW=DComplexPDBandFact
$(RWDIR)/dtrdgfct.h: xfct.h fct.mac
	$(MATHPP) xfct.h $@ RW_C_RW=DoubleTriDiagFact
dtrdgfct.cpp: xfct.cpp fct.mac
	$(MATHPP) xfct.cpp $@ RW_C_RW=DoubleTriDiagFact
$(RWDIR)/ftrdgfct.h: xfct.h fct.mac
	$(MATHPP) xfct.h $@ RW_C_RW=FloatTriDiagFact
ftrdgfct.cpp: xfct.cpp fct.mac
	$(MATHPP) xfct.cpp $@ RW_C_RW=FloatTriDiagFact
$(RWDIR)/ctrdgfct.h: xfct.h fct.mac
	$(MATHPP) xfct.h $@ RW_C_RW=DComplexTriDiagFact
ctrdgfct.cpp: xfct.cpp fct.mac
	$(MATHPP) xfct.cpp $@ RW_C_RW=DComplexTriDiagFact
$(RWDIR)/dpdtdfct.h: xfct.h fct.mac
	$(MATHPP) xfct.h $@ RW_C_RW=DoublePDTriDiagFact
dpdtdfct.cpp: xfct.cpp fct.mac
	$(MATHPP) xfct.cpp $@ RW_C_RW=DoublePDTriDiagFact
$(RWDIR)/fpdtdfct.h: xfct.h fct.mac
	$(MATHPP) xfct.h $@ RW_C_RW=FloatPDTriDiagFact
fpdtdfct.cpp: xfct.cpp fct.mac
	$(MATHPP) xfct.cpp $@ RW_C_RW=FloatPDTriDiagFact
$(RWDIR)/cpdtdfct.h: xfct.h fct.mac
	$(MATHPP) xfct.h $@ RW_C_RW=DComplexPDTriDiagFact
cpdtdfct.cpp: xfct.cpp fct.mac
	$(MATHPP) xfct.cpp $@ RW_C_RW=DComplexPDTriDiagFact
$(RWDIR)/dsymeig.h: xsymeig.h
	$(MATHPP) xsymeig.h $@ RW_T_RW=Double
$(RWDIR)/dtd.h: xtd.h
	$(MATHPP) xtd.h $@ RW_T_RW=Double
$(RWDIR)/dseigsrv.h: xseigsrv.h
	$(MATHPP) xseigsrv.h $@ RW_T_RW=Double
$(RWDIR)/dqr.h: xqr.h
	$(MATHPP) xqr.h $@ RW_T_RW=Double
$(RWDIR)/dco.h: xco.h
	$(MATHPP) xco.h $@ RW_T_RW=Double
$(RWDIR)/dsv.h: xsv.h
	$(MATHPP) xsv.h $@ RW_T_RW=Double
$(RWDIR)/dlsqr.h: xlsqr.h
	$(MATHPP) xlsqr.h $@ RW_T_RW=Double
$(RWDIR)/dlssv.h: xlssv.h
	$(MATHPP) xlssv.h $@ RW_T_RW=Double
$(RWDIR)/dlsch.h: xlsch.h
	$(MATHPP) xlsch.h $@ RW_T_RW=Double
$(RWDIR)/dbal.h: xbal.h
	$(MATHPP) xbal.h $@ RW_T_RW=Double
$(RWDIR)/dhess.h: xhess.h
	$(MATHPP) xhess.h $@ RW_T_RW=Double
$(RWDIR)/dschur.h: xschur.h
	$(MATHPP) xschur.h $@ RW_T_RW=Double
$(RWDIR)/deig.h: xeig.h
	$(MATHPP) xeig.h $@ RW_T_RW=Double
$(RWDIR)/deigsrv.h: xeigsrv.h
	$(MATHPP) xeigsrv.h $@ RW_T_RW=Double
dsymeig.cpp: xsymeig.cpp
	$(MATHPP) xsymeig.cpp $@ RW_T_RW=Double
dseigsrv.cpp: xseigsrv.cpp
	$(MATHPP) xseigsrv.cpp $@ RW_T_RW=Double
dseigqr.cpp: xseigqr.cpp
	$(MATHPP) xseigqr.cpp $@ RW_T_RW=Double
dseigpd.cpp: xseigpd.cpp
	$(MATHPP) xseigpd.cpp $@ RW_T_RW=Double
dseigrf.cpp: xseigrf.cpp
	$(MATHPP) xseigrf.cpp $@ RW_T_RW=Double
dseigbis.cpp: xseigbis.cpp
	$(MATHPP) xseigbis.cpp $@ RW_T_RW=Double
dtd.cpp: xtd.cpp
	$(MATHPP) xtd.cpp $@ RW_T_RW=Double
ddensetd.cpp: xdensetd.cpp
	$(MATHPP) xdensetd.cpp $@ RW_T_RW=Double
dbandtd.cpp: xbandtd.cpp
	$(MATHPP) xbandtd.cpp $@ RW_T_RW=Double
dqr.cpp: xqr.cpp
	$(MATHPP) xqr.cpp $@ RW_T_RW=Double
dco.cpp: xco.cpp
	$(MATHPP) xco.cpp $@ RW_T_RW=Double
dsv.cpp: xsv.cpp
	$(MATHPP) xsv.cpp $@ RW_T_RW=Double
dlsqr.cpp: xlsqr.cpp
	$(MATHPP) xlsqr.cpp $@ RW_T_RW=Double
dlssv.cpp: xlssv.cpp
	$(MATHPP) xlssv.cpp $@ RW_T_RW=Double
dlsch.cpp: xlsch.cpp
	$(MATHPP) xlsch.cpp $@ RW_T_RW=Double
dbal.cpp: xbal.cpp
	$(MATHPP) xbal.cpp $@ RW_T_RW=Double
dhess.cpp: xhess.cpp
	$(MATHPP) xhess.cpp $@ RW_T_RW=Double
dschur.cpp: xschur.cpp
	$(MATHPP) xschur.cpp $@ RW_T_RW=Double
deig.cpp: xeig.cpp
	$(MATHPP) xeig.cpp $@ RW_T_RW=Double
deigsch.cpp: xeigsch.cpp
	$(MATHPP) xeigsch.cpp $@ RW_T_RW=Double
deighess.cpp: xeighess.cpp
	$(MATHPP) xeighess.cpp $@ RW_T_RW=Double
$(RWDIR)/fsymeig.h: xsymeig.h
	$(MATHPP) xsymeig.h $@ RW_T_RW=Float
$(RWDIR)/ftd.h: xtd.h
	$(MATHPP) xtd.h $@ RW_T_RW=Float
$(RWDIR)/fseigsrv.h: xseigsrv.h
	$(MATHPP) xseigsrv.h $@ RW_T_RW=Float
$(RWDIR)/fqr.h: xqr.h
	$(MATHPP) xqr.h $@ RW_T_RW=Float
$(RWDIR)/fco.h: xco.h
	$(MATHPP) xco.h $@ RW_T_RW=Float
$(RWDIR)/fsv.h: xsv.h
	$(MATHPP) xsv.h $@ RW_T_RW=Float
$(RWDIR)/flsqr.h: xlsqr.h
	$(MATHPP) xlsqr.h $@ RW_T_RW=Float
$(RWDIR)/flssv.h: xlssv.h
	$(MATHPP) xlssv.h $@ RW_T_RW=Float
$(RWDIR)/flsch.h: xlsch.h
	$(MATHPP) xlsch.h $@ RW_T_RW=Float
$(RWDIR)/fbal.h: xbal.h
	$(MATHPP) xbal.h $@ RW_T_RW=Float
$(RWDIR)/fhess.h: xhess.h
	$(MATHPP) xhess.h $@ RW_T_RW=Float
$(RWDIR)/fschur.h: xschur.h
	$(MATHPP) xschur.h $@ RW_T_RW=Float
$(RWDIR)/feig.h: xeig.h
	$(MATHPP) xeig.h $@ RW_T_RW=Float
$(RWDIR)/feigsrv.h: xeigsrv.h
	$(MATHPP) xeigsrv.h $@ RW_T_RW=Float
fsymeig.cpp: xsymeig.cpp
	$(MATHPP) xsymeig.cpp $@ RW_T_RW=Float
fseigsrv.cpp: xseigsrv.cpp
	$(MATHPP) xseigsrv.cpp $@ RW_T_RW=Float
fseigqr.cpp: xseigqr.cpp
	$(MATHPP) xseigqr.cpp $@ RW_T_RW=Float
fseigpd.cpp: xseigpd.cpp
	$(MATHPP) xseigpd.cpp $@ RW_T_RW=Float
fseigrf.cpp: xseigrf.cpp
	$(MATHPP) xseigrf.cpp $@ RW_T_RW=Float
fseigbis.cpp: xseigbis.cpp
	$(MATHPP) xseigbis.cpp $@ RW_T_RW=Float
ftd.cpp: xtd.cpp
	$(MATHPP) xtd.cpp $@ RW_T_RW=Float
fdensetd.cpp: xdensetd.cpp
	$(MATHPP) xdensetd.cpp $@ RW_T_RW=Float
fbandtd.cpp: xbandtd.cpp
	$(MATHPP) xbandtd.cpp $@ RW_T_RW=Float
fqr.cpp: xqr.cpp
	$(MATHPP) xqr.cpp $@ RW_T_RW=Float
fco.cpp: xco.cpp
	$(MATHPP) xco.cpp $@ RW_T_RW=Float
fsv.cpp: xsv.cpp
	$(MATHPP) xsv.cpp $@ RW_T_RW=Float
flsqr.cpp: xlsqr.cpp
	$(MATHPP) xlsqr.cpp $@ RW_T_RW=Float
flssv.cpp: xlssv.cpp
	$(MATHPP) xlssv.cpp $@ RW_T_RW=Float
flsch.cpp: xlsch.cpp
	$(MATHPP) xlsch.cpp $@ RW_T_RW=Float
fbal.cpp: xbal.cpp
	$(MATHPP) xbal.cpp $@ RW_T_RW=Float
fhess.cpp: xhess.cpp
	$(MATHPP) xhess.cpp $@ RW_T_RW=Float
fschur.cpp: xschur.cpp
	$(MATHPP) xschur.cpp $@ RW_T_RW=Float
feig.cpp: xeig.cpp
	$(MATHPP) xeig.cpp $@ RW_T_RW=Float
feigsch.cpp: xeigsch.cpp
	$(MATHPP) xeigsch.cpp $@ RW_T_RW=Float
feighess.cpp: xeighess.cpp
	$(MATHPP) xeighess.cpp $@ RW_T_RW=Float
$(RWDIR)/csymeig.h: xsymeig.h
	$(MATHPP) xsymeig.h $@ RW_T_RW=DComplex
$(RWDIR)/ctd.h: xtd.h
	$(MATHPP) xtd.h $@ RW_T_RW=DComplex
$(RWDIR)/cseigsrv.h: xseigsrv.h
	$(MATHPP) xseigsrv.h $@ RW_T_RW=DComplex
$(RWDIR)/cqr.h: xqr.h
	$(MATHPP) xqr.h $@ RW_T_RW=DComplex
$(RWDIR)/cco.h: xco.h
	$(MATHPP) xco.h $@ RW_T_RW=DComplex
$(RWDIR)/csv.h: xsv.h
	$(MATHPP) xsv.h $@ RW_T_RW=DComplex
$(RWDIR)/clsqr.h: xlsqr.h
	$(MATHPP) xlsqr.h $@ RW_T_RW=DComplex
$(RWDIR)/clssv.h: xlssv.h
	$(MATHPP) xlssv.h $@ RW_T_RW=DComplex
$(RWDIR)/clsch.h: xlsch.h
	$(MATHPP) xlsch.h $@ RW_T_RW=DComplex
$(RWDIR)/cbal.h: xbal.h
	$(MATHPP) xbal.h $@ RW_T_RW=DComplex
$(RWDIR)/chess.h: xhess.h
	$(MATHPP) xhess.h $@ RW_T_RW=DComplex
$(RWDIR)/cschur.h: xschur.h
	$(MATHPP) xschur.h $@ RW_T_RW=DComplex
$(RWDIR)/ceig.h: xeig.h
	$(MATHPP) xeig.h $@ RW_T_RW=DComplex
$(RWDIR)/ceigsrv.h: xeigsrv.h
	$(MATHPP) xeigsrv.h $@ RW_T_RW=DComplex
csymeig.cpp: xsymeig.cpp
	$(MATHPP) xsymeig.cpp $@ RW_T_RW=DComplex
cseigsrv.cpp: xseigsrv.cpp
	$(MATHPP) xseigsrv.cpp $@ RW_T_RW=DComplex
cseigqr.cpp: xseigqr.cpp
	$(MATHPP) xseigqr.cpp $@ RW_T_RW=DComplex
cseigpd.cpp: xseigpd.cpp
	$(MATHPP) xseigpd.cpp $@ RW_T_RW=DComplex
cseigrf.cpp: xseigrf.cpp
	$(MATHPP) xseigrf.cpp $@ RW_T_RW=DComplex
cseigbis.cpp: xseigbis.cpp
	$(MATHPP) xseigbis.cpp $@ RW_T_RW=DComplex
ctd.cpp: xtd.cpp
	$(MATHPP) xtd.cpp $@ RW_T_RW=DComplex
cdensetd.cpp: xdensetd.cpp
	$(MATHPP) xdensetd.cpp $@ RW_T_RW=DComplex
cbandtd.cpp: xbandtd.cpp
	$(MATHPP) xbandtd.cpp $@ RW_T_RW=DComplex
cqr.cpp: xqr.cpp
	$(MATHPP) xqr.cpp $@ RW_T_RW=DComplex
cco.cpp: xco.cpp
	$(MATHPP) xco.cpp $@ RW_T_RW=DComplex
csv.cpp: xsv.cpp
	$(MATHPP) xsv.cpp $@ RW_T_RW=DComplex
clsqr.cpp: xlsqr.cpp
	$(MATHPP) xlsqr.cpp $@ RW_T_RW=DComplex
clssv.cpp: xlssv.cpp
	$(MATHPP) xlssv.cpp $@ RW_T_RW=DComplex
clsch.cpp: xlsch.cpp
	$(MATHPP) xlsch.cpp $@ RW_T_RW=DComplex
cbal.cpp: xbal.cpp
	$(MATHPP) xbal.cpp $@ RW_T_RW=DComplex
chess.cpp: xhess.cpp
	$(MATHPP) xhess.cpp $@ RW_T_RW=DComplex
cschur.cpp: xschur.cpp
	$(MATHPP) xschur.cpp $@ RW_T_RW=DComplex
ceig.cpp: xeig.cpp
	$(MATHPP) xeig.cpp $@ RW_T_RW=DComplex
ceigsch.cpp: xeigsch.cpp
	$(MATHPP) xeigsch.cpp $@ RW_T_RW=DComplex
ceighess.cpp: xeighess.cpp
	$(MATHPP) xeighess.cpp $@ RW_T_RW=DComplex

/*
 * Implementation of DComplexPDBandFact
 *
 * Generated from template $Id: xfct.cpp,v 1.6 1993/10/11 21:48:11 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/lapkdefs.h"
#include "rw/cpdbdfct.h"
#include "rw/lapack.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"  
#include "rw/cgenmat.h"

const char* DComplexPDBandFact::className = "DComplexPDBandFact";

DComplexPDBandFact::~DComplexPDBandFact()
{
}

DComplexPDBandFact::DComplexPDBandFact()
{
  info = -1;
}

DComplexPDBandFact::DComplexPDBandFact( const DComplexPDBandFact& A )
  : factorization(A.factorization)
{
  info = A.info;
  Anorm = A.Anorm;
}

DComplexPDBandFact::DComplexPDBandFact(const DComplexHermBandMat& A, RWBoolean estimateCondition)
  : factorization(A)  // Use reference semantics, followed by
                      // deepenShallowCopy(), to work around a BCC V3.0 bug
{
    factorization.deepenShallowCopy();
    dofactor(estimateCondition);
}

void DComplexPDBandFact::factor( const DComplexHermBandMat& A, RWBoolean estimateCondition )
{
    DComplexHermBandMat Acopy = A.copy();
    factorization.reference(Acopy);
    dofactor(estimateCondition);
}

void DComplexPDBandFact::dofactor(RWBoolean estimateCondition)
{
   info=0;   // for size zero matrices, need to set this explicitly
   long n = rows();	

    // Error (sanity) checking
    RWPRECONDITION(factorization.dataVec().stride()==1);
    for(int r=rows(); r--;) {
      if (imag(factorization.val(r,r))!=0.0) {
	RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTHERM)));
      }
    }


    // If we care to ever find the condition number, we need to know
    // the 1-norm of the matrix A.  Compute it while the member data
    // factorization is still a copy of A, and not yet it's factorization.
    Anorm = -1; 
    if (n>0 && estimateCondition) {
      long kd = factorization.halfBandwidth();
      long ldab = kd+1;
      Double *work = new Double [n];
      Anorm = zlansb('1','U',n,kd,factorization.data(),ldab,work);
      delete [] work;
    }

    if (n>0) {
      long kd = factorization.halfBandwidth();
      long ldab = kd+1;
      zpbtrf('U',n,kd,factorization.data(),ldab,info);
      RWPOSTCONDITION(info>=0);
    }
}

RWBoolean DComplexPDBandFact::fail() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}

RWBoolean DComplexPDBandFact::isPD() const
{
    return (rows()>0 && info!=0) ? FALSE : TRUE;
}  

RWBoolean DComplexPDBandFact::isSingular() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}

Double DComplexPDBandFact::condition() const
{
    Double Anormcopy = Anorm;      // Copy to stop warnings about ref parm
    long info;
    long n = rows();
    Double rcond;
    DComplex *work = new DComplex[4*n];   // Some routines only require 2*n, but what the hell
    long kd = factorization.halfBandwidth();
    long ldab = kd+1;
    Double *work2 = new Double [n];
    zpbcon('U',n,kd,(DComplex*)factorization.data(),ldab,Anormcopy,rcond,work,work2,info);
    delete [] work2;
    delete [] work;
    RWPOSTCONDITION(info==0);
    return rcond;
}

DComplexVec DComplexPDBandFact::solve( const DComplexVec& b ) const
{
  DComplexGenMat B(b,b.length(),1);
  DComplexGenMat X = solve(B);
  RWPOSTCONDITION(X.cols()==1);
  return X.col(0);
}

DComplexGenMat DComplexPDBandFact::solve( const DComplexGenMat& B ) const
{
    DComplexGenMat X(B.copy());
    long n = rows();
    if (B.rows()!=n) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,B.rows(),n)));
    if (fail()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTSOLVE)));
    long nrhs = X.cols();
    long info;
    if (n>0) {
    long kd = factorization.halfBandwidth();
    long ldab = kd+1;
    zpbtrs('U',n,kd,nrhs,(DComplex*)factorization.data(),ldab,X.data(),n,info);
      RWPOSTCONDITION(info==0);
    }
    return X;    
}

DComplex DComplexPDBandFact::determinant() const
{
    DComplex rootdet = 1.0;
    for(int i=rows(); i--;) { rootdet *= factorization.val(i,i); }  // using val() rather than just op()() shuts up a SUN warning
    return rootdet*rootdet;
}
              

/*
 * Global functions which provide alternate ways to call member functions.
 * These used to be inline, but this requires that you include more header
 * files on some compilers, and that is just not worth it.
 */
DComplexVec solve(const DComplexPDBandFact& A, const DComplexVec& b) { return A.solve(b); }
DComplexGenMat solve(const DComplexPDBandFact& A, const DComplexGenMat& b) { return A.solve(b); }
DComplex    determinant (const DComplexPDBandFact& A)	      { return A.determinant(); }


Double condition(const DComplexPDBandFact& A) { return A.condition(); }



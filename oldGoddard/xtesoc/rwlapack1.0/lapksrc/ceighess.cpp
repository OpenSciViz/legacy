/*
 * Implementation of DComplexEigHessServer
 *
 * Generated from template $Id: xeighess.cpp,v 1.4 1993/10/11 21:48:09 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
  * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */ 

#include "rw/ceigsrv.h"
#include "rw/chess.h"
#include "rw/lapack.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"

DComplexHessEigServer::DComplexHessEigServer(RWBoolean left, RWBoolean right, RWBoolean sc, RWBoolean perm)
  : selectRange_((const RWSlice&)RWAll)
{
  computeLeftVecs_  = left;
  computeRightVecs_ = right;
  scale_            = sc;
  permute_          = perm;
}

RWBoolean DComplexHessEigServer::computeLeftEigenVectors() const       {return computeLeftVecs_;}
void      DComplexHessEigServer::computeLeftEigenVectors(RWBoolean x)  {computeLeftVecs_ = x;}
RWBoolean DComplexHessEigServer::computeRightEigenVectors() const      {return computeRightVecs_;}
void      DComplexHessEigServer::computeRightEigenVectors(RWBoolean x) {computeRightVecs_ = x;}
RWBoolean DComplexHessEigServer::scale()                    const      {return scale_;}
void      DComplexHessEigServer::scale(RWBoolean x)                    {scale_ = x;}
RWBoolean DComplexHessEigServer::permute()                  const      {return permute_;}
void      DComplexHessEigServer::permute(RWBoolean x)                  {permute_ = x;}
void      DComplexHessEigServer::balance(RWBoolean x)                  {scale_ = permute_ = x;}

void DComplexHessEigServer::selectEigenVectors(const IntVec& s)
{
  selectVec_.reference(s.copy());
  selectRange_ = RWAll;
}

void DComplexHessEigServer::selectEigenVectors(const RWSlice& s)
{
  selectRange_ = s;
  selectVec_.reshape(0);
}

RWBoolean DComplexHessEigServer::computeAllEigenVectors() const
{
  // The next line needs to determine if the RWSlice object selectRange_
  // is a copy of RWAll.  This means it will have a begin of zero, a stride
  // of one, and a length which matches whatever length the argument vector
  // is.  Test for this by asking for two lengths, if the slice gives back
  // the two lengths, then it must be an RWAll.
  RWBoolean isRWAll = (selectRange_.begin()==0 && selectRange_.stride()==1 && selectRange_.len(5)==5 && selectRange_.len(6)==6);
  return (isRWAll && selectVec_.length()==0);
}

DComplexEigDecomp DComplexHessEigServer::operator()(const DComplexGenMat& A)
{
  DComplexHessenbergDecomp x(A,permute_,scale_);
  return (*this)(x);
}

DComplexEigDecomp DComplexHessEigServer::operator()(const DComplexBalanceDecomp& A)
{
  DComplexHessenbergDecomp x(A);
  return (*this)(x);
}

DComplexEigDecomp DComplexHessEigServer::setEigenVals(const DComplexHessenbergDecomp& A)
{
  DComplexEigDecomp eig;
  n(eig) = A.rows();
  long n = A.rows();
  DComplexGenMat H = A.H();
  H.deepenShallowCopy();   // just in case the interface changes
  lambda(eig).reshape(A.rows());
  long ilo = A.balanceTransform().lowIndex();
  long ihi = A.balanceTransform().highIndex();
  DComplex *work = new DComplex [n];
  long info;
  zhseqr('E','N',n,ilo,ihi,H.data(),H.colStride(),lambda(eig).data(),0,1,work,0,info);
  RWPOSTCONDITION(info>=0);
  if (info>0) {
    RWPOSTCONDITION(ilo-1+n-info<n);
    lambda(eig)(RWSlice(ilo,n-info)) = lambda(eig)(RWToEnd(info));
    lambda(eig).reshape(ilo-1+n-info);
  }
  delete work;
  return eig;
}

DComplexEigDecomp DComplexHessEigServer::operator()(const DComplexHessenbergDecomp& A)
{    
  // First build a container and set up the eigenvalues
  DComplexEigDecomp eig = setEigenVals(A);
  if (!eig.good()) return eig;   // in case we didn't get the eigenvalues

  // Now determine which eigenvectors we want
  IntVec select(n(eig),0);
  if (computeAllEigenVectors()) {
    select=1;
  } else {
    if (selectVec_.length()!=0) {
      for(int i=selectVec_.length(); i--;) {
        int x = selectVec_(i);
        if (x<0 || x>=n(eig)) RWTHROW(RWBoundsErr(RWMessage(RWLAPK_NOEIG,x)));
        select(x) = 1;
      }
    } else {  // which to compute is indicated by the RWSlice
      selectRange_.boundsCheck(n(eig));
      select(selectRange_) = 1;
    }


  }

  long mm=0;        // compute number of eigenvectors desired
  for(int i=select.length(); i--;) { if (select(i)) mm++; }
  DComplexGenMat vl;
  DComplexGenMat vr;
  if (computeLeftVecs_)  { vl.reshape(A.rows(),(unsigned)mm); }
  if (computeRightVecs_) { vr.reshape(A.rows(),(unsigned)mm); }
  if (mm>0 && (computeLeftVecs_ || computeRightVecs_)) {
    long n = A.rows();
    char job = (computeLeftVecs_ ? (computeRightVecs_ ? 'B' : 'L') : 'R');
    DComplexGenMat H = A.H();
    long ldh = H.colStride();
    long ldvl = rwmax(1,vl.colStride());   // max to cover the case of colStride zero
    long ldvr = rwmax(1,vr.colStride());
    long m;
    long info;
    IntVec selectcopy = select.copy();     // Because dtrevc modifies select
    DComplex *work = new DComplex [(n+2)*n];
    long *ifaill = new long [mm];
    long *ifailr = new long [mm];
    Double *rwork = new Double [n];
    zhsein(job,'Q','N',selectcopy.data(),n,H.data(),ldh,
                          lambda(eig).data(),vl.data(),ldvl,
                          vr.data(),ldvr,mm,m,work,rwork,ifaill,ifailr,info) ;
    delete rwork;
    RWPOSTCONDITION(info>=0);
    if (info>0) {
      accurate(eig) = FALSE;
    }
    RWPOSTCONDITION(m==mm);
    delete work;
    // Now de-balance the eigenvectors
    if (computeLeftVecs_) { P(eig).reference(A.BInvTX(A.QX(vl))); }
    if (computeRightVecs_){ Q(eig).reference(A.BX(A.QX(vr))); }

    // Finally, move the eigenvalues corresponding the the chosen
    // eigenvectors to the front so the positions match up.  Do this
    // in two passes.
    DComplexVec temp = lambda(eig);
    lambda(eig).deepenShallowCopy();
    int index=0;
    for(int i=0; i<temp.length(); i++) {
      if (select(i)) lambda(eig)(index++) = temp(i);
    }
    for(i=0; i<temp.length(); i++) {
      if (!select(i)) lambda(eig)(index++) = temp(i);
    }
    RWPOSTCONDITION(index==lambda(eig).length());
  }
  return eig;
}

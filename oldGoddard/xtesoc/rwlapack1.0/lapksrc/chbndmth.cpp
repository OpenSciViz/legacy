#if 0
/*
 * Generated from template $Id: xmth.cpp,v 1.2 1993/10/11 21:48:17 alv Exp $
 *
 * Math member functions for the matrix types
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */
#endif

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/chbndmat.h"
#include "rw/dsbndmat.h"

/*
 * This function returns a new matrix the same shape as the one 
 * passed in but with the data passed in.  This way we restrict
 * all the >if statements to a couple lines
 */
inline DComplexHermBandMat sameShapeMatrix( DComplexVec& vec, const DComplexHermBandMat& A )
{
  return DComplexHermBandMat(vec,A.rows(),A.cols(),A.lowerBandwidth());
}


static void verifyMatch(const DComplexHermBandMat& A, const DComplexHermBandMat& B)
{
    if (A.rows()!=B.rows() || A.cols()!=B.cols())
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATSIZE,A.rows(),A.cols(),B.rows(),B.cols())));
    if (A.bandwidth()!=B.bandwidth() || A.upperBandwidth()!=B.upperBandwidth())
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_BANDSIZE,A.bandwidth(),B.bandwidth(),
			A.upperBandwidth(), B.upperBandwidth())));
}

RWBoolean DComplexHermBandMat::operator==(const DComplexHermBandMat& X)
{
    if (n!=X.n || bandu!=X.bandu) return FALSE;
    int hb = halfBandwidth();
    if (hb==0) return ( vec==X.vec );	// A diagonal matrix
    /*
     * This is a little tricky cause some of the data vector is unused.
     * First check the last part of the data vector, then check the bits
     * at the beginning that are used.
     */
    int lastStart = hb*hb;
    int lastLen = (n-hb+1)*(hb+1)-1;
    if (n>=hb)
	if ( vec.slice(lastStart,lastLen)!=X.vec.slice(lastStart,lastLen) ) return FALSE;
    int el,len;
    for( el=hb,len=1 ; el<vec.length() && len<hb ; el+=hb,len++ ) {    
      if (vec.slice(el,len)!=X.vec.slice(el,len)) return FALSE;
    }
    return TRUE;
}


DComplexHermBandMat& DComplexHermBandMat::operator+=(const DComplexHermBandMat& m)
{
  verifyMatch(*this,m);
  vec += m.vec;
  return *this;
}

DComplexHermBandMat& DComplexHermBandMat::operator-=(const DComplexHermBandMat& m)
{
  verifyMatch(*this,m);
  vec -= m.vec;
  return *this;
}

DComplexHermBandMat& DComplexHermBandMat::operator*=(const DComplexHermBandMat& m)
{
  verifyMatch(*this,m);
  vec *= m.vec;
  return *this;
}

DComplexHermBandMat& DComplexHermBandMat::operator*=(DComplex x)
{
  vec *= x;
  return *this;
}

DComplexHermBandMat& DComplexHermBandMat::operator/=(const DComplexHermBandMat& m)
{
  verifyMatch(*this,m);
  vec /= m.vec;
  return *this;
}

DComplexHermBandMat& DComplexHermBandMat::operator/=(DComplex x)
{
  vec /= x;
  return *this;
}




DComplexHermBandMat operator-(const DComplexHermBandMat& A)
{
  DComplexVec temp(-(A.dataVec()));
  return sameShapeMatrix(temp,A);
}
  
DComplexHermBandMat operator+(const DComplexHermBandMat& A)
{
  return A;
}

DComplexHermBandMat operator*(const DComplexHermBandMat& A, const DComplexHermBandMat& B) {
	verifyMatch(A,B);
	DComplexVec temp(A.dataVec()*B.dataVec());
	return sameShapeMatrix(temp,A);
}

DComplexHermBandMat operator/(const DComplexHermBandMat& A, const DComplexHermBandMat& B) {
	verifyMatch(A,B);
	DComplexVec temp(A.dataVec()/B.dataVec());
	return sameShapeMatrix(temp,A);
}

DComplexHermBandMat operator+(const DComplexHermBandMat& A, const DComplexHermBandMat& B) {
	verifyMatch(A,B);
	DComplexVec temp(A.dataVec()+B.dataVec());
	return sameShapeMatrix(temp,A);
}

DComplexHermBandMat operator-(const DComplexHermBandMat& A, const DComplexHermBandMat& B) {
	verifyMatch(A,B);
	DComplexVec temp(A.dataVec()-B.dataVec());
	return sameShapeMatrix(temp,A);
}

DComplexHermBandMat operator*(const DComplexHermBandMat& A, DComplex x) {
	DComplexVec temp(A.dataVec()*x);
	return sameShapeMatrix(temp,A);
}

/*
 * Out-of-line version for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
  DComplexHermBandMat operator*(DComplex x, const DComplexHermBandMat& A) { return A*x; }
#endif

DComplexHermBandMat operator/(const DComplexHermBandMat& A, DComplex x) {
	DComplexVec temp(A.dataVec()/x);
	return sameShapeMatrix(temp,A);
}





DoubleSymBandMat abs(const DComplexHermBandMat& A) {
	DoubleVec temp(abs(A.dataVec()));
	return DoubleSymBandMat(temp,A.rows(),A.cols(),A.halfBandwidth());
}



   /* Sorry, no imag, arg, functions for HermBandMat */

DComplexHermBandMat conj(const DComplexHermBandMat& A) {
	DComplexVec temp(conj(A.dataVec()));
	return sameShapeMatrix(temp,A);
}

DoubleSymBandMat real(const DComplexHermBandMat& A) {
	DoubleVec temp(real(A.dataVec()));
	return DoubleSymBandMat(temp,A.rows(),A.cols(),A.halfBandwidth());
}

DoubleSymBandMat norm(const DComplexHermBandMat& A) {
	DoubleVec temp(norm(A.dataVec()));
	return DoubleSymBandMat(temp,A.rows(),A.cols(),A.halfBandwidth());
}

DComplexHermBandMat transpose(const DComplexHermBandMat& A)
{
  DComplexHermBandMat T(conj(A));
  int n = A.rows();
  int m = A.halfBandwidth();
  DComplexVec diag = T.dataVec().slice(m,n,m+1);
  diag = conj(diag);
  return T;
}

DComplexVec product(const DComplexHermBandMat& A, const DComplexVec& x)
{
    if (A.cols()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATVECPROD,A.className,A.rows(),A.cols(),x.length())));
    }
    DComplexVec y(A.rows(),rwUninitialized);
    int n = A.rows();
    int m = A.lowerBandwidth();
    y = A.dataVec().slice(m,n,m+1);
    y *= x;
    for( int i=1; i<=m; i++ ) {
      DComplexVec diag = A.dataVec().slice( m+i*m, n-i, m+1 );
      DComplexVec yslice = y.slice( 0, x.length()-i );
      DComplexVec xslice = x.slice( i, x.length()-i );
      yslice += diag * xslice;
      DComplexVec yslice2 = y.slice( i, x.length()-i );
      DComplexVec xslice2 = x.slice( 0, x.length()-i );
      yslice2 += conj(diag) * xslice2;
    }
    return y;
}

DComplexVec product(const DComplexVec& x, const DComplexHermBandMat& A)
{
    if (A.rows()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECMATPROD,x.length(),A.className,A.rows(),A.cols())));
    }
    int n = A.rows();
    int m = A.lowerBandwidth();
    DComplexVec y(A.dataVec().slice(m,n,m+1));
    y.deepenShallowCopy();
    y *= x;
    for( int i=1; i<=m; i++ ) {
      DComplexVec diag = A.dataVec().slice( m+i*m, n-i, m+1 );
      DComplexVec yslice = y.slice( 0, x.length()-i );
      DComplexVec xslice = x.slice( i, x.length()-i );
      yslice += conj(diag) * xslice;
      DComplexVec yslice2 = y.slice( i, x.length()-i );
      DComplexVec xslice2 = x.slice( 0, x.length()-i );
      yslice2 += diag * xslice2;
    }
    return y;
}



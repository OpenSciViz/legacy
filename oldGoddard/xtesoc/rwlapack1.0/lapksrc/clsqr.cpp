/*
 * defs for DComplexLeastSqQR
 *
 * Generated from template $Id: xlsqr.cpp,v 1.4 1993/10/11 21:48:14 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 */

#include "rw/clsqr.h"
#include "rw/lapkerr.h"
#include "rw/rwerr.h"

DComplexLeastSqQR::DComplexLeastSqQR()                                 : DComplexCODecomp() {}
DComplexLeastSqQR::DComplexLeastSqQR(const DComplexGenMat& A, double tol)   : DComplexCODecomp(A,tol) {}
DComplexLeastSqQR::DComplexLeastSqQR(const DComplexQRDecomp& A, double tol) : DComplexCODecomp(A,tol) {}
DComplexLeastSqQR::DComplexLeastSqQR(const DComplexCODecomp& A)             : DComplexCODecomp(A) {}

DComplexVec DComplexLeastSqQR::residual(const DComplexVec& data) const
{
  if (data.length()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,data.length(),rows())));
  DComplexVec d = QTx(data);
  d(RWSlice(0,rank())) = (DComplex)0;
  return Qx(d);
}

Double DComplexLeastSqQR::residualNorm(const DComplexVec& data) const
{
  if (data.length()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,data.length(),rows())));
  DComplexVec d = QTx(data);
  DComplexVec d2 = d(RWToEnd(rank()));
  return l2Norm(d2);
}

DComplexVec DComplexLeastSqQR::solve(const DComplexVec& data) const
{
  if (data.length()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,data.length(),rows())));
  DComplexVec d = QTx(data);
  DComplexVec d1 = d(RWSlice(0,rank()));
  DComplexVec c(cols(),rwUninitialized);
  c(RWSlice(0,rank())) = Tinvx(d1);
  c(RWToEnd(rank())) = (DComplex)0;
  return Px(Zx(c));
} 

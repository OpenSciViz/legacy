#if 0
/*
 * Generated from template $Id: xmth.cpp,v 1.2 1993/10/11 21:48:17 alv Exp $
 *
 * Math member functions for the matrix types
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */
#endif

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/fskewmat.h"
#include "rw/fsymmat.h"

/*
 * This function returns a new matrix the same shape as the one 
 * passed in but with the data passed in.  This way we restrict
 * all the >if statements to a couple lines
 */
inline FloatSkewMat sameShapeMatrix( FloatVec& vec, const FloatSkewMat& A )
{
  return FloatSkewMat(vec,A.rows(),A.cols());
}


static void verifyMatch(const FloatSkewMat& A, const FloatSkewMat& B)
{
    if (A.rows()!=B.rows() || A.cols()!=B.cols())
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATSIZE,A.rows(),A.cols(),B.rows(),B.cols())));
}

RWBoolean FloatSkewMat::operator==(const FloatSkewMat& X)
{
    if (n!=X.n) return FALSE;
    return vec==X.vec;	// All elements in vec used in the matrix
}


FloatSkewMat& FloatSkewMat::operator+=(const FloatSkewMat& m)
{
  verifyMatch(*this,m);
  vec += m.vec;
  return *this;
}

FloatSkewMat& FloatSkewMat::operator-=(const FloatSkewMat& m)
{
  verifyMatch(*this,m);
  vec -= m.vec;
  return *this;
}

FloatSkewMat& FloatSkewMat::operator*=(const FloatSkewMat& m)
{
  verifyMatch(*this,m);
  vec *= m.vec;
  return *this;
}

FloatSkewMat& FloatSkewMat::operator*=(Float x)
{
  vec *= x;
  return *this;
}

FloatSkewMat& FloatSkewMat::operator/=(const FloatSkewMat& m)
{
  verifyMatch(*this,m);
  vec /= m.vec;
  return *this;
}

FloatSkewMat& FloatSkewMat::operator/=(Float x)
{
  vec /= x;
  return *this;
}




FloatSkewMat operator-(const FloatSkewMat& A)
{
  FloatVec temp(-(A.dataVec()));
  return sameShapeMatrix(temp,A);
}
  
FloatSkewMat operator+(const FloatSkewMat& A)
{
  return A;
}

FloatSkewMat operator*(const FloatSkewMat& A, const FloatSkewMat& B) {
	verifyMatch(A,B);
	FloatVec temp(A.dataVec()*B.dataVec());
	return sameShapeMatrix(temp,A);
}

FloatSkewMat operator/(const FloatSkewMat& A, const FloatSkewMat& B) {
	verifyMatch(A,B);
	FloatVec temp(A.dataVec()/B.dataVec());
	return sameShapeMatrix(temp,A);
}

FloatSkewMat operator+(const FloatSkewMat& A, const FloatSkewMat& B) {
	verifyMatch(A,B);
	FloatVec temp(A.dataVec()+B.dataVec());
	return sameShapeMatrix(temp,A);
}

FloatSkewMat operator-(const FloatSkewMat& A, const FloatSkewMat& B) {
	verifyMatch(A,B);
	FloatVec temp(A.dataVec()-B.dataVec());
	return sameShapeMatrix(temp,A);
}

FloatSkewMat operator*(const FloatSkewMat& A, Float x) {
	FloatVec temp(A.dataVec()*x);
	return sameShapeMatrix(temp,A);
}

/*
 * Out-of-line version for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
  FloatSkewMat operator*(Float x, const FloatSkewMat& A) { return A*x; }
#endif

FloatSkewMat operator/(const FloatSkewMat& A, Float x) {
	FloatVec temp(A.dataVec()/x);
	return sameShapeMatrix(temp,A);
}


FloatSkewMat operator/(Float x, const FloatSkewMat& A) {
	FloatVec temp(x/A.dataVec());
	return sameShapeMatrix(temp,A);
}



FloatSymMat abs(const FloatSkewMat& A) {
	FloatVec temp(abs(A.dataVec()));
	return FloatSymMat(temp,A.rows(),A.cols());
}



FloatSkewMat transpose(const FloatSkewMat& A)
{
  FloatSkewMat T(-A);
  Float* index = T.data();		// Fix up the diagonal
  for( int k=1; k<=T.rows(); index+=(++k) ) {
    (*index) *= -1;
  }
  return T;
}

FloatVec product(const FloatSkewMat& A, const FloatVec& x)
{
    if (A.cols()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATVECPROD,A.className,A.rows(),A.cols(),x.length())));
    }
    FloatVec y(A.rows(),rwUninitialized);
    const FloatVec& v = A.dataVec();
    int n=A.rows();
    y(0) = (Float)(0);
    for( int i=0; i<n; i++ ) {
      if (i>0) {
        y(i) = -dot( v.slice(i*(i+1)/2,i,1), x.slice(0,i,1) );
      }
      Float* index = (Float*)v.data() + ((i+1)*(i+2)/2-1);
      for( int k=i; k<n; index+=(++k) ) {
	y(i) += (*index)*x(k);
      }
    }
    return y;
}

FloatVec product(const FloatVec& x, const FloatSkewMat& A)
{
    if (A.rows()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECMATPROD,x.length(),A.className,A.rows(),A.cols())));
    }
    FloatVec y(A.cols(),(Float)(0));
    const FloatVec& v = A.dataVec();
    int n=A.rows();
    for( int i=0; i<n; i++ ) {
      y(i) = dot( v.slice(i*(i+1)/2,i+1,1), x.slice(0,i+1,1) );
      Float* index = (Float*)v.data() + ((i+2)*(i+3)/2-2);
      for( int k=i+1; k<n; index+=(++k) ) {
	y(i) -= (*index)*x(k);
      }
    }
    return y;
}



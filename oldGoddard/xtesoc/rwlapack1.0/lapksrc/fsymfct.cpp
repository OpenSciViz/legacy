/*
 * Implementation of FloatSymFact
 *
 * Generated from template $Id: xfct.cpp,v 1.6 1993/10/11 21:48:11 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/lapkdefs.h"
#include "rw/fsymfct.h"
#include "rw/lapack.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"  
#include "rw/fgenmat.h"

const char* FloatSymFact::className = "FloatSymFact";

FloatSymFact::~FloatSymFact()
{
  delete [] pvts;
}

FloatSymFact::FloatSymFact()
{
  info = -1;
  pvts=0;
}

FloatSymFact::FloatSymFact( const FloatSymFact& A )
  : factorization(A.factorization)
{
  info = A.info;
  Anorm = A.Anorm;
  unsigned n = A.rows();
  pvts = (n>0) ? new long [n] : 0;
  while(n--) {
    pvts[n] = A.pvts[n];
  }
}

FloatSymFact::FloatSymFact(const FloatSymMat& A, RWBoolean estimateCondition)
  : factorization(A)  // Use reference semantics, followed by
                      // deepenShallowCopy(), to work around a BCC V3.0 bug
{
    factorization.deepenShallowCopy();
    pvts = 0;
    dofactor(estimateCondition);
}

void FloatSymFact::factor( const FloatSymMat& A, RWBoolean estimateCondition )
{
    FloatSymMat Acopy = A.copy();
    factorization.reference(Acopy);
    dofactor(estimateCondition);
}

void FloatSymFact::dofactor(RWBoolean estimateCondition)
{
   info=0;   // for size zero matrices, need to set this explicitly
   long n = rows();	

    // Error (sanity) checking
    RWPRECONDITION(factorization.dataVec().stride()==1);

    delete [] pvts;
    pvts = (n>0) ? new long [n] : 0;

    // If we care to ever find the condition number, we need to know
    // the 1-norm of the matrix A.  Compute it while the member data
    // factorization is still a copy of A, and not yet it's factorization.
    Anorm = -1; 
    if (n>0 && estimateCondition) {
      Float *work = new Float [n];
      Anorm = slansp('1','U',n,factorization.data(),work);
      delete [] work;
    }

    if (n>0) {
      ssptrf('U',n,factorization.data(),pvts,info);
      RWPOSTCONDITION(info>=0);
    }
}

RWBoolean FloatSymFact::fail() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}


RWBoolean FloatSymFact::isSingular() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}

Float FloatSymFact::condition() const
{
    Float Anormcopy = Anorm;      // Copy to stop warnings about ref parm
    long info;
    long n = rows();
    Float rcond;
    Float *work = new Float[4*n];   // Some routines only require 2*n, but what the hell
    long *iwork = new long [n];
    sspcon('U',n,(Float*)factorization.data(),pvts,Anormcopy,rcond,work,iwork,info);
    delete [] iwork;
    delete [] work;
    RWPOSTCONDITION(info==0);
    return rcond;
}

FloatVec FloatSymFact::solve( const FloatVec& b ) const
{
  FloatGenMat B(b,b.length(),1);
  FloatGenMat X = solve(B);
  RWPOSTCONDITION(X.cols()==1);
  return X.col(0);
}

FloatGenMat FloatSymFact::solve( const FloatGenMat& B ) const
{
    FloatGenMat X(B.copy());
    long n = rows();
    if (B.rows()!=n) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,B.rows(),n)));
    if (fail()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTSOLVE)));
    long nrhs = X.cols();
    long info;
    if (n>0) {
    ssptrs('U',n,nrhs,(Float*)factorization.data(),pvts,X.data(),n,info);
      RWPOSTCONDITION(info==0);
    }
    return X;    
}

Float FloatSymFact::determinant() const
{
    // see linpack manual pg 5.16 for description of algorithm
    Float det = 1.0;
    for(int i=rows(); i--;) {
      RWPRECONDITION(pvts[i]!=0);
      if (pvts[i]>0) {
        det *= factorization.val(i,i);  // one by one block
      } else {                          // two by two block
        RWPRECONDITION(i>0 && pvts[i-1]==pvts[i]);
        Float a = factorization.val(i,i);  // .val stops sun warning
        Float b = factorization.val(i-1,i);
        Float c = factorization.val(i-1,i-1);
        Float blockdet = a*c-b*b;     // see linpack manual for a better way that avoids overflow and (as much) roundoff
        det *= blockdet;
        i--;
      }
    }
    return det;
}
              
FloatSymMat FloatSymFact::inverse( ) const
{
    if (rows()==0) { return factorization; }
    if (fail()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTSOLVE)));
    FloatSymMat soln(factorization);
    soln.deepenShallowCopy();
    long n = rows();
    long info;
    Float *work = new Float [n];  
    ssptri('U',n,soln.data(),pvts,work,info);
    delete [] work;
    RWPOSTCONDITION(info==0);
    return soln;
}


/*
 * Global functions which provide alternate ways to call member functions.
 * These used to be inline, but this requires that you include more header
 * files on some compilers, and that is just not worth it.
 */
FloatVec solve(const FloatSymFact& A, const FloatVec& b) { return A.solve(b); }
FloatGenMat solve(const FloatSymFact& A, const FloatGenMat& b) { return A.solve(b); }
Float    determinant (const FloatSymFact& A)	      { return A.determinant(); }

FloatSymMat inverse(const FloatSymFact& A)      	      { return A.inverse(); }

Float condition(const FloatSymFact& A) { return A.condition(); }



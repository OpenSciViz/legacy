/*
 * defs for DoubleSchurDecomp
 *
 * Generated from template $Id: xschur.cpp,v 1.4 1993/10/11 21:48:20 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 */ 

#include "rw/dschur.h"
#include "rw/dhess.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/lapack.h"
#include "rw/ivec.h"
#include "rw/ivecpik.h"

DoubleSchurDecomp::DoubleSchurDecomp()
{
}

DoubleSchurDecomp::DoubleSchurDecomp(const DoubleGenMat& A, RWBoolean permute, RWBoolean scale)
{
  factor(A,permute,scale);
}

DoubleSchurDecomp::DoubleSchurDecomp(const DoubleBalanceDecomp& A)
{
  factor(A);
}

DoubleSchurDecomp::DoubleSchurDecomp(const DoubleHessenbergDecomp& A)
{
  factor(A);
}

void DoubleSchurDecomp::factor(const DoubleGenMat& A, RWBoolean permute, RWBoolean scale)
{
  DoubleBalanceDecomp B(A,permute,scale);
  factor(B);
}

void DoubleSchurDecomp::factor(const DoubleBalanceDecomp& A)
{
  DoubleHessenbergDecomp B(A);
  factor(B);
}

RWBoolean DoubleSchurDecomp::good() const
{
  return (w_.length()==rows());
}

void DoubleSchurDecomp::factor(const DoubleHessenbergDecomp& A)
{
  long n = A.rows();
  B_ = A.balanceTransform();
  T_.reference(A.H());
  Z_.reference(A.Q());
  w_.reshape(A.rows());
  DoubleVec wr(rows(),rwUninitialized);
  DoubleVec wi(rows(),rwUninitialized);
  long ilo = B_.lowIndex();
  long ihi = B_.highIndex();
  Double *work = new Double [n];
  long info;
  dhseqr('S','V',n,ilo,ihi,T_.data(),T_.colStride(),wr.data(),wi.data(),Z_.data(),Z_.colStride(),work,0,info);
  RWPOSTCONDITION(info>=0);
  if (info>0) {
    RWPOSTCONDITION(ilo-1+n-info<n);
    // The casts in the following prevent "long passed as int" warnings
    w_.reshape((int)(ilo-1+n-info));
    wr(RWSlice((int)(ilo),(int)(n-info))) = wr(RWToEnd((int)info));
    wr.resize((int)(ilo-1+n-info));
    wi(RWSlice((int)(ilo),(int)(n-info))) = wi(RWToEnd((int)info));
    wi.resize((int)(ilo-1+n-info));
  }
  real(w_) = wr;
  imag(w_) = wi;
  delete work;
}

DoubleGenMat DoubleSchurDecomp::B() const
{
  DoubleGenMat I(rows(),cols(),(Double)0);
  I.diagonal() = (Double)1;
  return B_.transform(I);
}

DoubleGenMat DoubleSchurDecomp::Z() const {return Z_;}
DoubleGenMat DoubleSchurDecomp::T() const {return T_;}

DComplex DoubleSchurDecomp::eigenvalue(int i) const
{
  if (i<0 || i>=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOEIG,i)));
  return w_(i);
}

DComplexVec DoubleSchurDecomp::eigenvalues() const
{
  return w_;
}

DoubleVec DoubleSchurDecomp::Bx(const DoubleVec& x) const
{                                         
  return B_.transform(x);
}

DoubleVec DoubleSchurDecomp::BInvTx(const DoubleVec& x) const
{
  return B_.invTransform(x);
}

DoubleVec DoubleSchurDecomp::Zx(const DoubleVec& x) const
{
  return product(Z_,x);
}

DoubleVec DoubleSchurDecomp::ZTx(const DoubleVec& x) const
{
  return product(x,Z_);
}

DoubleGenMat DoubleSchurDecomp::BX(const DoubleGenMat& X) const
{                                         
  return B_.transform(X);
}
 
DoubleGenMat DoubleSchurDecomp::BInvTX(const DoubleGenMat& X)const
{                                         
  return B_.invTransform(X);
}

DoubleGenMat DoubleSchurDecomp::ZX(const DoubleGenMat& X) const
{
  return product(Z_,X);
}

DoubleGenMat DoubleSchurDecomp::ZTX(const DoubleGenMat& X) const
{
  return transposeProduct(Z_,X);
}

int DoubleSchurDecomp::move(int from, int to)
{
  if (!good()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_SCHURREORDER)));
  long n = rows();
  long ifst = from;
  long ilst = to;
  long info;
  Double *work = new Double [n];
  dtrexc('V',n,T_.data(),T_.colStride(),Z_.data(),Z_.colStride(),ifst,ilst,work,info);
  delete work;
  RWPOSTCONDITION(info==0 || info==1);
  return (int) ilst;
}

RWBoolean DoubleSchurDecomp::moveToFront(const IntVec& indices)
{
  if (!good()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_SCHURREORDER)));
  IntVec select(rows(),0);
  select.pick(indices) = 1;
  DoubleVec wr(rows(),rwUninitialized);
  DoubleVec wi(rows(),rwUninitialized);
  long m;
  Double s,sep;
  long n = rows();
  long lwork = n*(n+1)/2;
  Double *work = new Double [lwork];
  long ldt = T_.colStride();
  long ldz = Z_.colStride();
  long info;
  long *iwork = new long [lwork];
  dtrsen('N','V',select.data(),n,T_.data(),ldt,Z_.data(),ldz,
                 wr.data(),wi.data(),m,s,sep,work,lwork,iwork,lwork,info);
  delete iwork;
  RWPOSTCONDITION(info==0 || info==1);
  delete work;
  real(w_) = wr;
  imag(w_) = wi;
  return info==0;
}

/*
 * The conversion functions to/from general matrices.
 *
 * Generated from template $Id: xcg.cpp,v 1.2 1993/07/05 19:19:33 alv Exp $
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/fskewmat.h"
#include "rw/fgenmat.h"

FloatGenMat::FloatGenMat( const FloatSkewMat& A )
  : RWMatView(A.rows(),A.cols(),sizeof(Float))
{
  for( int j=0; j<A.rows(); j++ ) {
    for( register int i=0; i<=j; i++ ) {
      Float x = A.val(i,j);
      set(j,i,-x);
      set(i,j,x);
    }
  }
}

FloatSkewMat toSkewMat( const FloatGenMat& S, RWBoolean keepMainDiag)
{
  int n = S.cols();
  FloatSkewMat A(S.rows(),n);
   for( int j=0; j<n; j++ ) {
     for( int i=0; i<j; i++ ) {
       A.set(i,j,(S.val(i,j)-S.val(j,i))/2);
     }
     if (keepMainDiag) A.set(j,j,S.val(j,j));
     else              A.set(j,j,0);
   }
   return A;
}


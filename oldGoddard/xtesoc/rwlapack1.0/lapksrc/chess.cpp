/*
 * DComplexHessenbergDecomp - Hessenberg decomposition of a matrix
 *
 * Generated from template $Id: xhess.cpp,v 1.3 1993/07/08 23:26:13 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 * This decomposition is usually formed as a percusor to computing
 * the eigenvalues and/or Schur decomposition of a non-symmetric matrix.
 *
 * This class includes an optional balancing transformation in addition
 * to the standard Hessenberg decomposition.
 */ 

#include "rw/chess.h"

DComplexHessenbergDecomp::DComplexHessenbergDecomp()
{
}

DComplexHessenbergDecomp::DComplexHessenbergDecomp(const DComplexGenMat& A, RWBoolean permute, RWBoolean scale)
{
  factor(A,permute,scale);
}

DComplexHessenbergDecomp::DComplexHessenbergDecomp(const DComplexBalanceDecomp& A)
{
  factor(A);
}

void DComplexHessenbergDecomp::factor(const DComplexGenMat& A, RWBoolean permute, RWBoolean scale)
{
  DComplexBalanceDecomp BCB(A,permute,scale);
  factor(BCB);
}
  
void DComplexHessenbergDecomp::factor(const DComplexBalanceDecomp& decomp)
{
  B_ = decomp.B();
  H_.reference(decomp.C());
  H_.deepenShallowCopy();
  tau_.reshape(decomp.rows());
  long info;
  long n = decomp.rows();
  long blockSize = ilaenv( 1, "DGEHRD", " ", n, B_.lowIndex(), B_.highIndex(), -1 );
  long lwork = n*blockSize;
  DComplex *work = new DComplex [lwork];
  zgehrd(n,B_.lowIndex(),B_.highIndex(),H_.data(),n,tau_.data(),work,lwork,info);
  RWPOSTCONDITION(info==0);
  RWPOSTCONDITION(lwork>=work[0]);
  delete work;
}

DComplexGenMat   DComplexHessenbergDecomp::B()                     const
{
  DComplexGenMat I(rows(),cols(),(DComplex)0);
  I.diagonal() = (DComplex)1;
  return B_.transform(I);
}

DComplexGenMat   DComplexHessenbergDecomp::Q()                     const
{
  DComplexGenMat theQ = H_.copy();
  long n = rows();
  long info;
  long blockSize = ilaenv( 1, "DORGHR", " ", n, B_.lowIndex(), B_.highIndex(), -1 );
  long lwork = n*blockSize;
  DComplex *work = new DComplex [lwork];
  zunghr(n,B_.lowIndex(),B_.highIndex(),theQ.data(),rows(),(DComplex*)tau_.data(),work,lwork,info);
  RWPOSTCONDITION(info==0);
  RWPOSTCONDITION(lwork>=work[0]);
  delete work;
  return theQ;
}

DComplexGenMat   DComplexHessenbergDecomp::H()                     const
{
  DComplexGenMat A = H_.copy();
  for(int i=2; i<rows(); i++) {
    A.diagonal(-i) = (DComplex)0;
  }
  return A;
}

DComplexVec      DComplexHessenbergDecomp::Bx(const DComplexVec& x)       const
{
  return B_.transform(x);
}

DComplexVec      DComplexHessenbergDecomp::BInvTx(const DComplexVec& x)   const
{
  return B_.invTransform(x);
}

DComplexVec      DComplexHessenbergDecomp::Qx(const DComplexVec& x)      const
{
  DComplexGenMat X(x,x.length(),1);
  DComplexGenMat theQX = QX(X);
  RWPOSTCONDITION(theQX.cols()==1);
  return theQX.col(0);
}

DComplexVec      DComplexHessenbergDecomp::QTx(const DComplexVec& x)      const
{
  DComplexGenMat X(x,x.length(),1);
  DComplexGenMat theQTX = QTX(X);
  RWPOSTCONDITION(theQTX.cols()==1);
  return theQTX.col(0);
}

DComplexGenMat   DComplexHessenbergDecomp::BX(const DComplexGenMat& x)    const
{
  return B_.transform(x);
}

DComplexGenMat   DComplexHessenbergDecomp::BInvTX(const DComplexGenMat& x)const
{
  return B_.invTransform(x);
}

DComplexGenMat   DComplexHessenbergDecomp::QX(const DComplexGenMat& X)   const
{
  DComplexGenMat C = X.copy();
  long n = rows();
  long info;
  long blockSize = ilaenv( 1, "DORMHR", " ", n, B_.lowIndex(), B_.highIndex(), -1 );
  long lwork = n*blockSize;
  DComplex *work = new DComplex [lwork];
  zunmhr('L','N',C.rows(),C.cols(),B_.lowIndex(),B_.highIndex(),(DComplex*)H_.data(),
            H_.rows(),(DComplex*)tau_.data(),C.data(),C.rows(),work,lwork,info);
  RWPOSTCONDITION(info==0);
  RWPOSTCONDITION(lwork>=work[0]);
  delete work;
  return C;
}

DComplexGenMat   DComplexHessenbergDecomp::QTX(const DComplexGenMat& X)   const
{
  DComplexGenMat C = X.copy();
  long n = rows();
  long info;
  long blockSize = ilaenv( 1, "DORMHR", " ", n, B_.lowIndex(), B_.highIndex(), -1 );
  long lwork = rows()*blockSize;
  DComplex *work = new DComplex [lwork];
  char trans = 'C';
  zunmhr('L',trans,C.rows(),C.cols(),B_.lowIndex(),B_.highIndex(),(DComplex*)H_.data(),H_.rows(),
                                (DComplex*)tau_.data(),C.data(),C.rows(),work,lwork,info);
  RWPOSTCONDITION(info==0);
  RWPOSTCONDITION(lwork>=work[0]);
  delete work;
  return C;
}

/*
 * defs for DComplexLeastSqCh
 *
 * Generated from template $Id: xlsch.cpp,v 1.5 1993/10/11 21:48:13 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 */

#include "rw/clsch.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"

DComplexLeastSqCh::DComplexLeastSqCh() {}

DComplexLeastSqCh::DComplexLeastSqCh(const DComplexGenMat& A)
{
  factor(A);
}

void      DComplexLeastSqCh::factor(const DComplexGenMat& A)
{
  A_.reference(A);
  DComplexHermMat ATA = upperToHermMat(conjTransposeProduct(A,A));
  ATA.makeDiagonalReal();
  decomp_.factor(ATA);
}

DComplexVec    DComplexLeastSqCh::residual(const DComplexVec& data) const
{
  DComplexVec x = solve(data);
  return data - product(A_,x);
}

Double       DComplexLeastSqCh::residualNorm(const DComplexVec& data) const
{
  if (data.length()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,data.length(),rows())));
  return l2Norm(residual(data));
}

DComplexVec    DComplexLeastSqCh::solve(const DComplexVec& data) const
{
  if (decomp_.fail()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTSOLVELS)));
  DComplexVec ATb = product(conj(transpose(A_)),data);
  return ::solve(decomp_,ATb);
}

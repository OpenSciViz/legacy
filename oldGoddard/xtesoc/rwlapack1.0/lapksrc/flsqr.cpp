/*
 * defs for FloatLeastSqQR
 *
 * Generated from template $Id: xlsqr.cpp,v 1.4 1993/10/11 21:48:14 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 */

#include "rw/flsqr.h"
#include "rw/lapkerr.h"
#include "rw/rwerr.h"

FloatLeastSqQR::FloatLeastSqQR()                                 : FloatCODecomp() {}
FloatLeastSqQR::FloatLeastSqQR(const FloatGenMat& A, double tol)   : FloatCODecomp(A,tol) {}
FloatLeastSqQR::FloatLeastSqQR(const FloatQRDecomp& A, double tol) : FloatCODecomp(A,tol) {}
FloatLeastSqQR::FloatLeastSqQR(const FloatCODecomp& A)             : FloatCODecomp(A) {}

FloatVec FloatLeastSqQR::residual(const FloatVec& data) const
{
  if (data.length()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,data.length(),rows())));
  FloatVec d = QTx(data);
  d(RWSlice(0,rank())) = (Float)0;
  return Qx(d);
}

Float FloatLeastSqQR::residualNorm(const FloatVec& data) const
{
  if (data.length()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,data.length(),rows())));
  FloatVec d = QTx(data);
  FloatVec d2 = d(RWToEnd(rank()));
  return l2Norm(d2);
}

FloatVec FloatLeastSqQR::solve(const FloatVec& data) const
{
  if (data.length()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,data.length(),rows())));
  FloatVec d = QTx(data);
  FloatVec d1 = d(RWSlice(0,rank()));
  FloatVec c(cols(),rwUninitialized);
  c(RWSlice(0,rank())) = Tinvx(d1);
  c(RWToEnd(rank())) = (Float)0;
  return Px(Zx(c));
} 

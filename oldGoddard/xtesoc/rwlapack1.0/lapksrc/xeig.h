> #################################################################
> # The master template file for non-symmetric eigen-decomposition#
> #                                                               #
> # $Header: /users/rcs/lapksrc/xeig.h,v 1.2 1993/07/05 19:19:33 alv Exp $
> #                                                               #
> # Copyright (1993) by Rogue Wave Software, Inc.  This           #
> # software is subject to copyright protection under the laws    #
> # of the United States and other countries.                     #
> #                                                               #
> # Written by Al Vermeulen.                                      #
> #                                                               #
> # Limited License.                                              #
> #################################################################
> #
> # $Log: xeig.h,v $
> # Revision 1.2  1993/07/05  19:19:33  alv
> # fixed up for complex numbers
> #
> # Revision 1.1  1993/05/18  17:00:02  alv
> # Initial revision
> #
>
> # Macros that we need
> 
> define <T>=RW_T_RW
> 
> if <T>==Double
> define <P>=Double <tLetter>=d <Z>=DComplex <zLetter>=c
> elif <T>==Float
> define <P>=Float  <tLetter>=f <Z>=FComplex <zLetter>=b
> elif <T>==DComplex
> define <P>=Double <tLetter>=c <Z>=DComplex <zLetter>=c <pLetter>=d
> else
> err eigen-decomposition: not defined for '<T>'
> endif
> beginwrapper <tLetter>eig

/*
 * <T>EigDecomp - Spectral factorization of a symmetric/Hermitian matrix
 *
 * Generated from template $Id: xeig.h,v 1.2 1993/07/05 19:19:33 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * This class represents the eigenvalues/vectors of a non-symmetric
 * matrix.
 *
 */ 

#include "rw/lapkdefs.h"
#include "rw/<tLetter>genmat.h"
#include "rw/<tLetter>vec.h"
#include "rw/<zLetter>genmat.h"
#include "rw/<zLetter>vec.h"
> if <T>!=<P>
#include "rw/<pLetter>eig.h"
> endif

class <T>EigDecomp {
private:
  unsigned  n;           // dimension of the matrix
  <Z>Vec    lambda;      // computed eigenvalues (length may be < n)
  <T>GenMat P;           // columns contain left eigenvectors
  <T>GenMat Q;           // columns contain right eigenvectors
  RWBoolean computedAll; // Did I compute everything the server tried to compute?
  RWBoolean accurate;    // Were all results as accurate as possible?

public:
  <T>EigDecomp();
  <T>EigDecomp(const <P>EigDecomp&);
  <T>EigDecomp(const <T>GenMat&, RWBoolean computeVecs=TRUE);
  void factor(const <T>GenMat&, RWBoolean computeVecs=TRUE);

  void      operator=(const <T>EigDecomp&);

  unsigned        cols()           const {return n;}
  <Z>             eigenValue(int)  const;
  const <Z>Vec    eigenValues()    const {return lambda;}
  const <Z>Vec    leftEigenVector(int)  const;
  const <Z>GenMat leftEigenVectors()    const;
  const <Z>Vec    rightEigenVector(int) const;
  const <Z>GenMat rightEigenVectors()   const;
  RWBoolean       good()           const; // True if everything went perfectly
  RWBoolean       inaccurate()     const; // Either failure, or some things are not fully accurate
  RWBoolean       fail()           const; // An eigenvalue or vector wasn't computed
  unsigned        numEigenValues() const {return lambda.length();}
  unsigned        numLeftEigenVectors()  const {return P.cols();}
  unsigned        numRightEigenVectors() const {return Q.cols();}
  unsigned        rows()           const {return n;}
               
  friend class <T>EigServer;
> if <T>==Double
  friend class DComplexEigServer;
  friend class DComplexEigDecomp;
> endif
};

> endwrapper


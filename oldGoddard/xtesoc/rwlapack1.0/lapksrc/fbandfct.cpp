/*
 * Implementation of FloatBandFact
 *
 * Generated from template $Id: xfct.cpp,v 1.6 1993/10/11 21:48:11 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/lapkdefs.h"
#include "rw/fbandfct.h"
#include "rw/lapack.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"  
#include "rw/fgenmat.h"

const char* FloatBandFact::className = "FloatBandFact";

FloatBandFact::~FloatBandFact()
{
  delete [] pvts;
}

FloatBandFact::FloatBandFact()
{
  info = -1;
  pvts=0;
}

FloatBandFact::FloatBandFact( const FloatBandFact& A )
  : factorization(A.factorization)
{
  info = A.info;
  Anorm = A.Anorm;
  unsigned n = A.rows();
  pvts = (n>0) ? new long [n] : 0;
  while(n--) {
    pvts[n] = A.pvts[n];
  }
}

FloatBandFact::FloatBandFact(const FloatBandMat& A, RWBoolean estimateCondition)
  : factorization(A.rows(),A.cols(),A.lowerBandwidth(),A.lowerBandwidth()+A.upperBandwidth() )
{
    factorization = A;
    pvts = 0;
    dofactor(estimateCondition);
}

void FloatBandFact::factor( const FloatBandMat& A, RWBoolean estimateCondition )
{
    FloatBandMat F(A.rows(),A.cols(),A.lowerBandwidth(),A.lowerBandwidth()+A.upperBandwidth());
    F = A;
    factorization.reference(F);
    dofactor(estimateCondition);
}

void FloatBandFact::dofactor(RWBoolean estimateCondition)
{
   info=0;   // for size zero matrices, need to set this explicitly
   long n = rows();	

    // Error (sanity) checking
    RWPRECONDITION(factorization.dataVec().stride()==1);

    delete [] pvts;
    pvts = (n>0) ? new long [n] : 0;

    // If we care to ever find the condition number, we need to know
    // the 1-norm of the matrix A.  Compute it while the member data
    // factorization is still a copy of A, and not yet it's factorization.
    Anorm = -1; 
    if (n>0 && estimateCondition) {
      long kl = factorization.lowerBandwidth();
      long ku = factorization.upperBandwidth();
      long b = factorization.bandwidth();
      Anorm = slangb('1',n,kl,ku,factorization.data(),b,0);
    }

    if (n>0) {
      long b = factorization.bandwidth();
      long l = factorization.lowerBandwidth();
      long u = factorization.upperBandwidth()-factorization.lowerBandwidth();
      sgbtrf(n,n,l,u,factorization.data(),b,pvts,info);
      RWPOSTCONDITION(info>=0);
    }
}

RWBoolean FloatBandFact::fail() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}


RWBoolean FloatBandFact::isSingular() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}

Float FloatBandFact::condition() const
{
    Float Anormcopy = Anorm;      // Copy to stop warnings about ref parm
    long info;
    long n = rows();
    Float rcond;
    Float *work = new Float[4*n];   // Some routines only require 2*n, but what the hell
    long b = factorization.bandwidth();
    long l = factorization.lowerBandwidth();
    long u = factorization.upperBandwidth()-factorization.lowerBandwidth();
    long *work2 = new long [n];
    sgbcon('1',n,l,u,(Float*)factorization.data(),b,pvts,Anormcopy,rcond,work,work2,info);
    delete [] work2;
    delete [] work;
    RWPOSTCONDITION(info==0);
    return rcond;
}

FloatVec FloatBandFact::solve( const FloatVec& b ) const
{
  FloatGenMat B(b,b.length(),1);
  FloatGenMat X = solve(B);
  RWPOSTCONDITION(X.cols()==1);
  return X.col(0);
}

FloatGenMat FloatBandFact::solve( const FloatGenMat& B ) const
{
    FloatGenMat X(B.copy());
    long n = rows();
    if (B.rows()!=n) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,B.rows(),n)));
    if (fail()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTSOLVE)));
    long nrhs = X.cols();
    long info;
    if (n>0) {
    long b = factorization.bandwidth();
    long l = factorization.lowerBandwidth();
    long u = factorization.upperBandwidth()-factorization.lowerBandwidth();
    sgbtrs('N',n,l,u,nrhs,(Float*)factorization.data(),b,pvts,X.data(),n,info);
      RWPOSTCONDITION(info==0);
    }
    return X;    
}

Float FloatBandFact::determinant() const
{
    int numExchanges = 0;
    for(int i=rows(); i--;) {
      if (pvts[i]!=i+1) numExchanges++;
    }
    Float detLU = prod(factorization.diagonal());
    return (numExchanges%2) ? -detLU : detLU;
}
              

/*
 * Global functions which provide alternate ways to call member functions.
 * These used to be inline, but this requires that you include more header
 * files on some compilers, and that is just not worth it.
 */
FloatVec solve(const FloatBandFact& A, const FloatVec& b) { return A.solve(b); }
FloatGenMat solve(const FloatBandFact& A, const FloatGenMat& b) { return A.solve(b); }
Float    determinant (const FloatBandFact& A)	      { return A.determinant(); }


Float condition(const FloatBandFact& A) { return A.condition(); }



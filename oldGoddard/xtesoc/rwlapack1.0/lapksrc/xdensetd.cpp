> # $Header: /users/rcs/lapksrc/xdensetd.cpp,v 1.4 1993/10/11 21:48:08 alv Exp $
>  
> # $Log: xdensetd.cpp,v $
> # Revision 1.4  1993/10/11  21:48:08  alv
> # synch'ed with Math.h++ v5.1
> #
> # Revision 1.3  1993/07/05  19:19:33  alv
> # fixed up for complex numbers
> #
> # Revision 1.2  1993/04/06  20:45:04  alv
> # ported to sun C++ v3.0.1
> #
> # Revision 1.1  1993/04/06  15:44:41  alv
> # Initial revision
> #
>  
> define <T>=RW_T_RW
> if <T>==Double
> define <P>=Double <tLetter>=d <Sym>=Sym <sym>=sym <trd>=dsptrd <mtr>=dopmtr
> elif <T>==Float
> define <P>=Float <tLetter>=f <Sym>=Sym <sym>=sym <trd>=ssptrd <mtr>=sopmtr
> elif <T>==DComplex
> define <P>=Double <tLetter>=c <Sym>=Herm <sym>=herm <trd>=zhptrd <mtr>=zupmtr
> else
> err Tri-diagonal decomposition: not defined for '<T>'
> endif
>  
/*
 * Implementation of <T><Sym>DenseTriDiagDecomp
 *
 * Generated from template $Id: xdensetd.cpp,v 1.4 1993/10/11 21:48:08 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */ 

#include "rw/<tLetter>td.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/lapack.h"
#include "rw/<tLetter><sym>mat.h"

<T><Sym>DenseTriDiagDecomp::<T><Sym>DenseTriDiagDecomp(const <T><Sym>Mat& A)
{
  if (A.rows()>0) {
    char uplo = 'U';   // Indicates to lapack that upper triangle is stored
    long n = A.rows();
    Qdata.reference(A.dataVec());
    Qdata.deepenShallowCopy();
    <P>Vec D(A.rows(),rwUninitialized);
    <P>Vec E(A.rows()-1,rwUninitialized);
    tau.reshape(A.rows());
    long info;
    <trd>(uplo,n,(<T>*)Qdata.data(),(<P>*)D.data(),(<P>*)E.data(),(<T>*)tau.data(),info);
    RWPOSTCONDITION(info==0);
    set(D,E);
  }
}

<T>Vec <T><Sym>DenseTriDiagDecomp::transform(const <P>Vec& V) const
{
  return <T><Sym>TriDiagDecomp::transform(V);
}

<T>GenMat <T><Sym>DenseTriDiagDecomp::transform(const <P>GenMat& Cinput) const
{
  <T>GenMat C = Cinput.copy();
  C.deepenShallowCopy();
  if (C.rows()>0) {
    if (C.rows()!=rows()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,C.rows(),rows())));
    }
    char side = 'L';
    char uplo = 'U';
    char trans = 'N';
    long m = C.rows();
    long n = C.cols();
    <T> *work = new <T>[n];
    long info;
    <mtr>(side,uplo,trans,m,n,(<T>*)Qdata.data(),(<T>*)tau.data(),(<T>*)C.data(),m,work,info);
    RWPOSTCONDITION(info==0);
    delete [] work;
  }
  return C;
}


/*
 * Implementation of DoubleSymSomeEigServer and DoubleSymRangeEigServer
 *
 * Generated from template $Id: xseigbis.cpp,v 1.6 1993/10/13 22:08:28 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */ 

#include "rw/dseigsrv.h"
#include "rw/dtd.h"
#include "rw/lapack.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include <float.h>  /* looking for max and min values to initialize RangeEigServers */

DoubleSymSomeEigServer::DoubleSymSomeEigServer(RWBoolean computeVecs)
 : range((const RWSlice&)RWAll)  // Sun seems to need the explicit cast
{
  computeVecs_ = computeVecs;
  tolerance_ = 0;
}

Double               DoubleSymSomeEigServer::setTolerance(Double t) 
{
  Double temp = tolerance_;
  tolerance_ = t;
  return temp;
}
  
RWSlice DoubleSymSomeEigServer::setRange(const RWSlice& x)
{
  RWSlice temp = range;
  range = x;
  return temp;
}

RWBoolean DoubleSymSomeEigServer::computeEigenVectors() const
{
  return computeVecs_;
}

void              DoubleSymSomeEigServer::computeEigenVectors(RWBoolean x)
{
  computeVecs_ = x;
}

DoubleSymEigDecomp   DoubleSymSomeEigServer::decompose(const DoubleSymTriDiagDecomp& decomp)
{
  DoubleSymEigDecomp eig;
  DoubleSymEigServer::n(eig) = decomp.rows();

  // Put together a few things both the lapack routines can use
  long n = decomp.rows();
  DoubleVec d = decomp.diagonal().copy();
  DoubleVec e(decomp.rows(),rwUninitialized);   // lapack needs a length n (not n-1) off diagonal
  if (n>0) e(RWSlice(0,decomp.rows()-1)) = decomp.offDiagonal();
  DoubleVec w(decomp.rows(),rwUninitialized);  // eigenvalues in here
  long m;
  long *iblock = new long [n];
  long *isplit = new long [n];
  Double *work = new Double [5*n];
  long *iwork = new long [3*n];
  long info;

  // determine range of interest, and make sure it is sensible.
  long il = range.begin()+1;
  long iu = il+range.stride()*(range.len(decomp.rows())-1);
  if (range.stride()<0) {
    long temp = il;
    il = iu;
    iu = temp;
  }         
  if (iu>decomp.rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOEIG,iu)));
  
  // compute eigenvalues
  if (iu>=il) {   // ie skip if we don't want any eigenvalues
    long nsplit;
    Double junk;
    dstebz('I','E',n,junk,junk,il,iu,tolerance_,d.data(),e.data(),m,nsplit,w.data(),iblock,isplit,work,iwork,info);
    RWPOSTCONDITION(info>=0);
    RWPOSTCONDITION(m<=iu-il+1);
    if (m<il-il+1) computedAll(eig)=FALSE;
    if (info==1 || info==3) {accurate(eig)=FALSE;}
    // toss out some if some weirdo used a non-unit stride, reverse
    // for negative stride
    if (range.stride()<0) w(RWRange(w.length()-1,0)) = w;  // reverse
    int str = range.stride();
    if (str<0) {str=(-str);}
    if (str>1) {
      m = (m+str-1)/str;
      w(RWSlice(0,(unsigned)m)) = w(RWToEnd(0,str));
    }
    lambda(eig).reference(w(RWSlice(0,(unsigned)m)));

    // compute eigenvectors
    if (computeVecs_ && m>1) {
      P(eig).reshape(decomp.rows(),(unsigned)m);
      dstein(n,d.data(),e.data(),m,w.data(),iblock,isplit,P(eig).data(),n,work,iwork,iwork+n,info);
      RWPOSTCONDITION(info>=0);
      if (info>0) accurate(eig)=FALSE;
    }
  }

  delete [] iblock;
  delete [] isplit;
  delete [] work;
  delete [] iwork;

  return eig;
}

/********************************************
 * Implementation of DoubleSymRangeEigServer *
 ********************************************/
 
DoubleSymRangeEigServer::DoubleSymRangeEigServer(RWBoolean computeVecs)
{
  computeVecs_ = computeVecs;
  tolerance_ = 0;
  small_ = -DBL_MAX;
  large_ = DBL_MAX;
}

void              DoubleSymRangeEigServer::setRange(Double x, Double y)
{
  small_ = x;
  large_ = y;
}

Double               DoubleSymRangeEigServer::setSmall(Double x)
{
  Double temp = small_;
  small_ = x;
  return temp;
}

Double               DoubleSymRangeEigServer::setLarge(Double x)
{
  Double temp = large_;
  large_ = x;
  return temp;
}

Double               DoubleSymRangeEigServer::setTolerance(Double t)
{
  Double temp = tolerance_;                                   
  tolerance_ = t;
  return temp;
}

RWBoolean DoubleSymRangeEigServer::computeEigenVectors() const
{
  return computeVecs_;
}

void              DoubleSymRangeEigServer::computeEigenVectors(RWBoolean x)
{
  computeVecs_ = x;
}
    
DoubleSymEigDecomp   DoubleSymRangeEigServer::decompose(const DoubleSymTriDiagDecomp& decomp)
{
  DoubleSymEigDecomp eig;
  DoubleSymEigServer::n(eig) = decomp.rows();

  // Put together a few things both the lapack routines can use
  long n = decomp.rows();
  DoubleVec d = decomp.diagonal().copy();
  DoubleVec e(decomp.rows(),rwUninitialized);   // lapack needs a length n (not n-1) off diagonal
  if (n>0) e(RWSlice(0,decomp.rows()-1)) = decomp.offDiagonal();
  DoubleVec w(decomp.rows(),rwUninitialized);  // eigenvalues in here
  long m;
  long *iblock = new long [n];
  long *isplit = new long [n];
  Double *work = new Double [5*n];
  long *iwork = new long [3*n];
  long info;

  // compute eigenvalues
  if (large_>small_) {   // ie skip if we don't want any eigenvalues
    long nsplit;
    long junk;
    dstebz('V','E',n,small_,large_,junk,junk,tolerance_,d.data(),e.data(),m,nsplit,w.data(),iblock,isplit,work,iwork,info);
    RWPOSTCONDITION(info>=0);
    if (info==4) computedAll(eig)=FALSE;
    if (info==1 || info==3) {accurate(eig)=FALSE;}
    lambda(eig).reference(w(RWSlice(0,(unsigned)m)));

    // compute eigenvectors
    if (computeVecs_ && m>1) {
      P(eig).reshape(decomp.rows(),(unsigned)m);
      dstein(n,d.data(),e.data(),m,w.data(),iblock,isplit,P(eig).data(),n,work,iwork,iwork+n,info);
      RWPOSTCONDITION(info>=0);
      if (info>0) accurate(eig)=FALSE;
    }
  }

  delete [] iblock;
  delete [] isplit;
  delete [] work;
  delete [] iwork;

  return eig;
}
   

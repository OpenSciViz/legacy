#if 0
/*
 * Generated from template $Id: xmth.cpp,v 1.2 1993/10/11 21:48:17 alv Exp $
 *
 * Math member functions for the matrix types
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */
#endif

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/dutrimat.h"
#include "rw/dltrimat.h"

/*
 * This function returns a new matrix the same shape as the one 
 * passed in but with the data passed in.  This way we restrict
 * all the >if statements to a couple lines
 */
inline DoubleUpperTriMat sameShapeMatrix( DoubleVec& vec, const DoubleUpperTriMat& A )
{
  return DoubleUpperTriMat(vec,A.rows(),A.cols());
}


static void verifyMatch(const DoubleUpperTriMat& A, const DoubleUpperTriMat& B)
{
    if (A.rows()!=B.rows() || A.cols()!=B.cols())
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATSIZE,A.rows(),A.cols(),B.rows(),B.cols())));
}

RWBoolean DoubleUpperTriMat::operator==(const DoubleUpperTriMat& X)
{
    if (n!=X.n) return FALSE;
    return vec==X.vec;	// All elements in vec used in the matrix
}


DoubleUpperTriMat& DoubleUpperTriMat::operator+=(const DoubleUpperTriMat& m)
{
  verifyMatch(*this,m);
  vec += m.vec;
  return *this;
}

DoubleUpperTriMat& DoubleUpperTriMat::operator-=(const DoubleUpperTriMat& m)
{
  verifyMatch(*this,m);
  vec -= m.vec;
  return *this;
}

DoubleUpperTriMat& DoubleUpperTriMat::operator*=(const DoubleUpperTriMat& m)
{
  verifyMatch(*this,m);
  vec *= m.vec;
  return *this;
}

DoubleUpperTriMat& DoubleUpperTriMat::operator*=(Double x)
{
  vec *= x;
  return *this;
}

DoubleUpperTriMat& DoubleUpperTriMat::operator/=(const DoubleUpperTriMat& m)
{
  verifyMatch(*this,m);
  vec /= m.vec;
  return *this;
}

DoubleUpperTriMat& DoubleUpperTriMat::operator/=(Double x)
{
  vec /= x;
  return *this;
}




DoubleUpperTriMat operator-(const DoubleUpperTriMat& A)
{
  DoubleVec temp(-(A.dataVec()));
  return sameShapeMatrix(temp,A);
}
  
DoubleUpperTriMat operator+(const DoubleUpperTriMat& A)
{
  return A;
}

DoubleUpperTriMat operator*(const DoubleUpperTriMat& A, const DoubleUpperTriMat& B) {
	verifyMatch(A,B);
	DoubleVec temp(A.dataVec()*B.dataVec());
	return sameShapeMatrix(temp,A);
}

DoubleUpperTriMat operator/(const DoubleUpperTriMat& A, const DoubleUpperTriMat& B) {
	verifyMatch(A,B);
	DoubleVec temp(A.dataVec()/B.dataVec());
	return sameShapeMatrix(temp,A);
}

DoubleUpperTriMat operator+(const DoubleUpperTriMat& A, const DoubleUpperTriMat& B) {
	verifyMatch(A,B);
	DoubleVec temp(A.dataVec()+B.dataVec());
	return sameShapeMatrix(temp,A);
}

DoubleUpperTriMat operator-(const DoubleUpperTriMat& A, const DoubleUpperTriMat& B) {
	verifyMatch(A,B);
	DoubleVec temp(A.dataVec()-B.dataVec());
	return sameShapeMatrix(temp,A);
}

DoubleUpperTriMat operator*(const DoubleUpperTriMat& A, Double x) {
	DoubleVec temp(A.dataVec()*x);
	return sameShapeMatrix(temp,A);
}

/*
 * Out-of-line version for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
  DoubleUpperTriMat operator*(Double x, const DoubleUpperTriMat& A) { return A*x; }
#endif

DoubleUpperTriMat operator/(const DoubleUpperTriMat& A, Double x) {
	DoubleVec temp(A.dataVec()/x);
	return sameShapeMatrix(temp,A);
}





DoubleUpperTriMat abs(const DoubleUpperTriMat& A) {
	DoubleVec temp(abs(A.dataVec()));
	return sameShapeMatrix(temp,A);
}



DoubleLowerTriMat transpose(const DoubleUpperTriMat& A)
{
return DoubleLowerTriMat(A.dataVec(),A.rows(),A.cols());
}

DoubleVec product(const DoubleUpperTriMat& A, const DoubleVec& x)
{
    if (A.cols()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATVECPROD,A.className,A.rows(),A.cols(),x.length())));
    }
    DoubleVec y(A.rows(),rwUninitialized);
    const DoubleVec& v = A.dataVec();
    int n=A.rows();
    for( int i=0; i<n; i++ ) {
      Double* index = (Double*)v.data() + ((i+1)*(i+2)/2-1);
      y(i) = (*index)*x(i);
      index += i+1;
      for( int k=i+1; k<n; index+=(++k) ) {
      y(i) += (*index)*x(k);
      }
    }
    return y;
}

DoubleVec product(const DoubleVec& x, const DoubleUpperTriMat& A)
{
    if (A.rows()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECMATPROD,x.length(),A.className,A.rows(),A.cols())));
    }
    return product(transpose(A),x);
}



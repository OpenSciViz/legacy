/*
 * Implementation of FloatPDTriDiagFact
 *
 * Generated from template $Id: xfct.cpp,v 1.6 1993/10/11 21:48:11 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/lapkdefs.h"
#include "rw/fpdtdfct.h"
#include "rw/lapack.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"  
#include "rw/fgenmat.h"

const char* FloatPDTriDiagFact::className = "FloatPDTriDiagFact";

FloatPDTriDiagFact::~FloatPDTriDiagFact()
{
}

FloatPDTriDiagFact::FloatPDTriDiagFact()
{
  info = -1;
}

FloatPDTriDiagFact::FloatPDTriDiagFact( const FloatPDTriDiagFact& A )
  : d(A.d), e(A.e)
{
  info = A.info;
  Anorm = A.Anorm;
}

FloatPDTriDiagFact::FloatPDTriDiagFact(const FloatTriDiagMat& A, RWBoolean estimateCondition)
  // Initialize in the body so we can be careful of negative lengths
{
    RWPRECONDITION(A.diagonal(-1)==A.diagonal(1));
    d.reference(A.diagonal().copy());
    if (A.rows()>1) {
      e.reference(A.diagonal(1).copy());
    } 
    dofactor(estimateCondition);
}

void FloatPDTriDiagFact::factor( const FloatTriDiagMat& A, RWBoolean estimateCondition )
{
    RWPRECONDITION(A.diagonal(-1)==A.diagonal(1));
    d.reference(A.diagonal().copy());
    if (A.rows()>1) {
      e.reference(A.diagonal(1).copy());
    } else {
      e.reshape(0);
    }
    dofactor(estimateCondition);
}

void FloatPDTriDiagFact::dofactor(RWBoolean estimateCondition)
{
   info=0;   // for size zero matrices, need to set this explicitly
   long n = rows();	

    // Error (sanity) checking
    RWPRECONDITION(factorization.dataVec().stride()==1);


    // If we care to ever find the condition number, we need to know
    // the 1-norm of the matrix A.  Compute it while the member data
    // factorization is still a copy of A, and not yet it's factorization.
    Anorm = -1; 
    if (n>0 && estimateCondition) {
      Anorm = slanst('1',n,d.data(),e.data());
    }

    if (n>0) {
      spttrf(n,d.data(),e.data(),info);
      RWPOSTCONDITION(info>=0);
    }
}

RWBoolean FloatPDTriDiagFact::fail() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}

RWBoolean FloatPDTriDiagFact::isPD() const
{
    return (rows()>0 && info!=0) ? FALSE : TRUE;
}  

RWBoolean FloatPDTriDiagFact::isSingular() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}

Float FloatPDTriDiagFact::condition() const
{
    Float Anormcopy = Anorm;      // Copy to stop warnings about ref parm
    long info;
    long n = rows();
    Float rcond;
    Float *work = new Float[4*n];   // Some routines only require 2*n, but what the hell
      sptcon(n,(Float*)d.data(),(Float*)e.data(),Anormcopy,rcond,work,info);
    delete [] work;
    RWPOSTCONDITION(info==0);
    return rcond;
}

FloatVec FloatPDTriDiagFact::solve( const FloatVec& b ) const
{
  FloatGenMat B(b,b.length(),1);
  FloatGenMat X = solve(B);
  RWPOSTCONDITION(X.cols()==1);
  return X.col(0);
}

FloatGenMat FloatPDTriDiagFact::solve( const FloatGenMat& B ) const
{
    FloatGenMat X(B.copy());
    long n = rows();
    if (B.rows()!=n) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,B.rows(),n)));
    if (fail()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTSOLVE)));
    long nrhs = X.cols();
    long info;
    if (n>0) {
    spttrs(n,nrhs,(Float*)d.data(),(Float*)e.data(),X.data(),n,info);  
      RWPOSTCONDITION(info==0);
    }
    return X;    
}

Float FloatPDTriDiagFact::determinant() const
{
    return prod(d);
}
              

/*
 * Global functions which provide alternate ways to call member functions.
 * These used to be inline, but this requires that you include more header
 * files on some compilers, and that is just not worth it.
 */
FloatVec solve(const FloatPDTriDiagFact& A, const FloatVec& b) { return A.solve(b); }
FloatGenMat solve(const FloatPDTriDiagFact& A, const FloatGenMat& b) { return A.solve(b); }
Float    determinant (const FloatPDTriDiagFact& A)	      { return A.determinant(); }


Float condition(const FloatPDTriDiagFact& A) { return A.condition(); }



> # $Header: /users/rcs/lapksrc/xtd.cpp,v 1.2 1993/10/11 21:48:26 alv Exp $
>  
> # $Log: xtd.cpp,v $
> # Revision 1.2  1993/10/11  21:48:26  alv
> # synch'ed with Math.h++ v5.1
> #
> # Revision 1.1  1993/04/06  15:44:43  alv
> # Initial revision
> #
>  
> define <T>=RW_T_RW
> if <T>==Double
> define <P>=Double <tLetter>=d <Sym>=Sym <sym>=sym
> elif <T>==Float
> define <P>=Float <tLetter>=f <Sym>=Sym <sym>=sym
> elif <T>==DComplex
> define <P>=Double <tLetter>=c <Sym>=Herm <sym>=herm
> else
> err Tri-diagonal decomposition: not defined for '<T>'
> endif
>  
/*
 * Implementation of <T><Sym>TriDiagDecomp
 *
 * Generated from template $Id: xtd.cpp,v 1.2 1993/10/11 21:48:26 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */ 

#include "rw/<tLetter>td.h"
#include "rw/lapkerr.h"
#include "rw/rwerr.h"

void <T><Sym>TriDiagDecomp::set(const <P>Vec& diag_, const <P>Vec& offdiag_)
{
  if (offdiag_.length()+1 != diag_.length()) {
    RWTHROW(RWInternalErr(RWMessage(RWLAPK_OFFDIAG,offdiag_.length(),diag_.length()-1)));
  }
  diag.reference(diag_);
  offdiag.reference(offdiag_);
}

<T>Vec <T><Sym>TriDiagDecomp::transform(const <P>Vec& x) const
{
  <P>GenMat C(x,x.length(),1);
  <T>GenMat QC = transform(C);
  RWPOSTCONDITION(QC.cols()==1);
  return QC.col(0);
}
  


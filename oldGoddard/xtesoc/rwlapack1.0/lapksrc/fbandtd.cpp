/*
 * Implementation of FloatSymBandTriDiagDecomp
 *
 * Generated from template $Id: xbandtd.cpp,v 1.4 1993/10/11 21:48:05 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */ 

#include "rw/ftd.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/lapack.h"
#include "rw/fsbndmat.h"

FloatSymBandTriDiagDecomp::FloatSymBandTriDiagDecomp(const FloatSymBandMat& A, RWBoolean keepQ)
{
  if (A.rows()>0 && A.bandwidth()>0) {
    char vect = (keepQ) ? 'V' : 'N';
    char uplo = 'U';   // Indicates to lapack that upper triangle is stored
    long n = A.rows();
    long kd = A.upperBandwidth();
    FloatSymBandMat Acopy = A.copy();
    long ldab = kd+1;
    FloatVec D(A.rows(),rwUninitialized);
    FloatVec E(A.rows()-1,rwUninitialized);
    if (keepQ) {Q.reshape(A.rows(),A.rows());}
    Float *work = new Float [n];
    long info;
    ssbtrd(vect,uplo,n,kd,(Float*)Acopy.data(),ldab,(Float*)D.data(),(Float*)E.data(),(Float*)Q.data(),n,work,info);
    RWPOSTCONDITION(info==0);
    delete work;
    set(D,E);
  }
}

FloatVec FloatSymBandTriDiagDecomp::transform(const FloatVec& V) const
{
  return FloatSymTriDiagDecomp::transform(V);
}

FloatGenMat FloatSymBandTriDiagDecomp::transform(const FloatGenMat& C) const
{
  if (C.cols()==0 || C.rows()==0) {
    return C;
  } else {
    if (C.rows()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,C.rows(),rows())));
    if (Q.rows()==0) RWTHROW(RWInternalErr(RWMessage(RWLAPK_QNOTCOMPUTED)));
    return product(Q,C);
  }
}

/*
 * The conversion functions to/from general matrices.
 *
 * Generated from template $Id: xcg.cpp,v 1.2 1993/07/05 19:19:33 alv Exp $
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/chbndmat.h"
#include "rw/cgenmat.h"

DComplexGenMat::DComplexGenMat( const DComplexHermBandMat& A )
  : RWMatView(A.rows(),A.cols(),sizeof(DComplex))
{
  zero();
  int hb = A.halfBandwidth();
  for( int k= hb; k>=0; k-- ) {
    DComplexVec d = A.dataVec().slice((k+1)*hb,nrows-k,hb+1);
    if (k>0) diagonal(-k) = conj(d);
    diagonal(k) = d;
  }
}

DComplexHermBandMat toHermBandMat( const DComplexGenMat& S, unsigned bandu )
{
  int n = S.cols();
  DComplexHermBandMat A( S.rows(), n, bandu );
  A.dataVec().slice(bandu,n,bandu+1) = S.diagonal(0);
  for( int k=1; k<=bandu; k++ ) {
    DComplexVec Adiag = A.dataVec().slice(bandu+k*bandu,n-k,bandu+1);
    Adiag = S.diagonal(k);
    Adiag += conj(S.diagonal(-k));
    Adiag /= DComplex(2,0);
  }
  return A;
}


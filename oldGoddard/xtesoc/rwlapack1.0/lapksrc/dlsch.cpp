/*
 * defs for DoubleLeastSqCh
 *
 * Generated from template $Id: xlsch.cpp,v 1.5 1993/10/11 21:48:13 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 */

#include "rw/dlsch.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"

DoubleLeastSqCh::DoubleLeastSqCh() {}

DoubleLeastSqCh::DoubleLeastSqCh(const DoubleGenMat& A)
{
  factor(A);
}

void      DoubleLeastSqCh::factor(const DoubleGenMat& A)
{
  A_.reference(A);
  DoubleSymMat ATA = upperToSymMat(transposeProduct(A,A));
  decomp_.factor(ATA);
}

DoubleVec    DoubleLeastSqCh::residual(const DoubleVec& data) const
{
  DoubleVec x = solve(data);
  return data - product(A_,x);
}

Double       DoubleLeastSqCh::residualNorm(const DoubleVec& data) const
{
  if (data.length()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,data.length(),rows())));
  return l2Norm(residual(data));
}

DoubleVec    DoubleLeastSqCh::solve(const DoubleVec& data) const
{
  if (decomp_.fail()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTSOLVELS)));
  DoubleVec ATb = product(transpose(A_),data);
  return ::solve(decomp_,ATb);
}

/*
 * Implementation of DoubleGenFact
 *
 * Generated from template $Id: xfct.cpp,v 1.6 1993/10/11 21:48:11 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/lapkdefs.h"
#include "rw/dgenfct.h"
#include "rw/lapack.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"  
#include "rw/dgenmat.h"

const char* DoubleGenFact::className = "DoubleGenFact";

DoubleGenFact::~DoubleGenFact()
{
  delete [] pvts;
}

DoubleGenFact::DoubleGenFact()
{
  info = -1;
  pvts=0;
}

DoubleGenFact::DoubleGenFact( const DoubleGenFact& A )
  : factorization(A.factorization)
{
  info = A.info;
  Anorm = A.Anorm;
  unsigned n = A.rows();
  pvts = (n>0) ? new long [n] : 0;
  while(n--) {
    pvts[n] = A.pvts[n];
  }
}

DoubleGenFact::DoubleGenFact(const DoubleGenMat& A, RWBoolean estimateCondition)
  : factorization(A)  // Use reference semantics, followed by
                      // deepenShallowCopy(), to work around a BCC V3.0 bug
{
    factorization.deepenShallowCopy();
    pvts = 0;
    dofactor(estimateCondition);
}

void DoubleGenFact::factor( const DoubleGenMat& A, RWBoolean estimateCondition )
{
    DoubleGenMat Acopy = A.copy();
    factorization.reference(Acopy);
    dofactor(estimateCondition);
}

void DoubleGenFact::dofactor(RWBoolean estimateCondition)
{
   info=0;   // for size zero matrices, need to set this explicitly
   long n = rows();	

    // Error (sanity) checking
    RWPRECONDITION(n==factorization.cols());
    RWPRECONDITION(factorization.rowStride()==1 && factorization.colStride()==factorization.rows());

    delete [] pvts;
    pvts = (n>0) ? new long [n] : 0;

    // If we care to ever find the condition number, we need to know
    // the 1-norm of the matrix A.  Compute it while the member data
    // factorization is still a copy of A, and not yet it's factorization.
    Anorm = -1; 
    if (n>0 && estimateCondition) {
      Anorm = dlange('1',n,n,factorization.data(),n,0);
    }

    if (n>0) {
#if defined(RW_USE_UNBLOCKED)
      dgetf2(n,n,factorization.data(),n,pvts,info);
#else
      dgetrf(n,n,factorization.data(),n,pvts,info);
#endif
      RWPOSTCONDITION(info>=0);
    }
}

RWBoolean DoubleGenFact::fail() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}


RWBoolean DoubleGenFact::isSingular() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}

Double DoubleGenFact::condition() const
{
    Double Anormcopy = Anorm;      // Copy to stop warnings about ref parm
    long info;
    long n = rows();
    Double rcond;
    Double *work = new Double[4*n];   // Some routines only require 2*n, but what the hell
    long *work2 = new long [n];
    dgecon('1',n,(Double*)factorization.data(),n,Anormcopy,rcond,work,work2,info);
    delete [] work2;
    delete [] work;
    RWPOSTCONDITION(info==0);
    return rcond;
}

DoubleVec DoubleGenFact::solve( const DoubleVec& b ) const
{
  DoubleGenMat B(b,b.length(),1);
  DoubleGenMat X = solve(B);
  RWPOSTCONDITION(X.cols()==1);
  return X.col(0);
}

DoubleGenMat DoubleGenFact::solve( const DoubleGenMat& B ) const
{
    DoubleGenMat X(B.copy());
    long n = rows();
    if (B.rows()!=n) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,B.rows(),n)));
    if (fail()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTSOLVE)));
    long nrhs = X.cols();
    long info;
    if (n>0) {
    dgetrs('N',n,nrhs,(Double*)factorization.data(),n,pvts,X.data(),n,info);
      RWPOSTCONDITION(info==0);
    }
    return X;    
}

Double DoubleGenFact::determinant() const
{
    int numExchanges = 0;
    for(int i=rows(); i--;) {
      if (pvts[i]!=i+1) numExchanges++;
    }
    Double detLU = prod(factorization.diagonal());
    return (numExchanges%2) ? -detLU : detLU;
}
              
DoubleGenMat DoubleGenFact::inverse( ) const
{
    if (rows()==0) { return factorization; }
    if (fail()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTSOLVE)));
    DoubleGenMat soln(factorization);
    soln.deepenShallowCopy();
    long n = rows();
    long info;
    long nb = ilaenv( 1, "dgetri", " ", n, -1, -1, -1 );
    long lwork = n*nb;
    Double *work = new Double [lwork];
    dgetri(n,soln.data(),n,pvts,work,lwork,info);
    RWPOSTCONDITION(work[0]==lwork);
    delete [] work;
    RWPOSTCONDITION(info==0);
    return soln;
}


/*
 * Global functions which provide alternate ways to call member functions.
 * These used to be inline, but this requires that you include more header
 * files on some compilers, and that is just not worth it.
 */
DoubleVec solve(const DoubleGenFact& A, const DoubleVec& b) { return A.solve(b); }
DoubleGenMat solve(const DoubleGenFact& A, const DoubleGenMat& b) { return A.solve(b); }
Double    determinant (const DoubleGenFact& A)	      { return A.determinant(); }

DoubleGenMat inverse(const DoubleGenFact& A)      	      { return A.inverse(); }

Double condition(const DoubleGenFact& A) { return A.condition(); }



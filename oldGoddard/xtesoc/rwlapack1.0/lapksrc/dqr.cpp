/*
 * Definitions for DoubleQRDecomp and DoubleQRDecompServer
 *
 * Generated from template $Id: xqr.cpp,v 1.5 1993/10/15 06:37:58 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 */

#include "rw/lapack.h"
#include "rw/dqr.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/dutrimat.h"

DoubleQRDecomp::DoubleQRDecomp()
{
  pivots_ = 0;
}

DoubleQRDecomp::DoubleQRDecomp(const DoubleQRDecomp& x)
{
  pivots_ = 0;
  *this = x;
}

void DoubleQRDecomp::operator=(const DoubleQRDecomp& x)
{
  tau_.reference(x.tau_);
  QR_.reference(x.QR_);
  delete [] pivots_;
  pivots_ = 0;
  if (x.pivots_) {
    pivots_ = new long [cols()];
    for(int i=cols(); i--;) {
      pivots_[i] = x.pivots_[i];
    }
  }
}

DoubleQRDecomp::DoubleQRDecomp(const DoubleGenMat& A) 
{
  pivots_=0;
  factor(A);
}

void DoubleQRDecomp::factor(const DoubleGenMat& A)
{
  DoubleQRDecompServer server;
  *this = server(A);
}

DoubleQRDecomp::~DoubleQRDecomp()
{
  delete [] pivots_;
}


DoubleGenMat      DoubleQRDecomp::P() const
{
  DoubleGenMat Pmat(cols(),cols(),(Double)0);
  if (pivots_) {
    for(int j=cols(); j--;) {
      Pmat((int)(pivots_[j]-1),j) = 1;
    }
  } else {
    Pmat.diagonal() = (Double)1;
  }
  return Pmat;
}

DoubleGenMat DoubleQRDecomp::R() const
{
  int m = (cols()<rows()) ? cols() : rows();
  // Obfuscate the following line, in order to shut up a spurious
  // sun CC 3.0.1 warning
  // DoubleGenMat Rmat = QR_(RWSlice(0,m),RWAll);
  DoubleGenMat Rmat = QR_.operator()(RWSlice(0,m),RWAll);
  Rmat.deepenShallowCopy();
  for(int i=m; --i>0;) {  // Loop indexes i=m-1,...,1
    Rmat.diagonal(-i) = (Double)0;
  }
  return Rmat;
}

DoubleVec         DoubleQRDecomp::Rdiagonal() const
{
  return QR_.diagonal().copy();
}

DoubleGenMat      DoubleQRDecomp::Q() const
{
  long m = rows();
  long n = cols();
  long k = (m<n) ? m : n;
  DoubleGenMat Qmat(QR_);
  Qmat.resize((int)m,(int)m);
  Qmat.deepenShallowCopy();
  long lwork = n*ilaenv( 1, "dorgqr", " ", m, m, k, -1 );
  Double *work = new Double [lwork];
  long info;
  dorgqr(m,m,k,Qmat.data(),m,(Double*)tau_.data(),work,lwork,info); 
  RWPOSTCONDITION(info==0);
  RWPOSTCONDITION(work[0]<=lwork);
  delete work;
  return Qmat;   
}

DoubleVec DoubleQRDecomp::Px(const DoubleVec& x) const
{
  if (x.length()!=cols()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),cols())));
  if (pivots_) {
    DoubleVec y(x.length(),rwUninitialized);
    for(int i=x.length(); i--;) { y((int)pivots_[i]-1) = x(i); }
    return y;
  } else {
    return x.copy();
  }
}

DoubleVec DoubleQRDecomp::PTx(const DoubleVec& x) const
{
  if (x.length()!=cols()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),cols())));
  if (pivots_) {
    DoubleVec y(x.length(),rwUninitialized);
    for(int i=x.length(); i--;) { y(i) = x((int)pivots_[i]-1); }
    return y;
  } else {
    return x.copy();
  }
}

DoubleVec DoubleQRDecomp::Rx(const DoubleVec& x) const
{
  if (x.length()!=cols()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),cols())));
  int m = (cols()<rows()) ? cols() : rows();
  // explicit call to operator() in next line shuts up sun v3.0.1 warning
  DoubleGenMat R = QR_.operator()(RWSlice(0,m),RWAll);     // Also has entries for Q
  RWPRECONDITION(R.rows()<=R.cols());
  if (R.cols()>R.rows()) {
    DoubleUpperTriMat R1 = toUpperTriMat(R(RWAll,RWSlice(0,m)));
    DoubleGenMat R2 = R(RWAll,RWToEnd(m));
    return product(R1,x(RWSlice(0,m))) + product(R2,x(RWToEnd(m)));
  } else {
    DoubleUpperTriMat R1 = toUpperTriMat(R);
    return product(R1,x);
  }
}

DoubleVec DoubleQRDecomp::RTx(const DoubleVec& x) const
{
  int m = (cols()<rows()) ? cols() : rows();
  if (x.length()!=m) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),m)));
  // explicit call to operator() in next line shuts up sun v3.0.1 warning
  DoubleGenMat R = QR_.operator()(RWSlice(0,m),RWAll);     // Also has entries for Q
  RWPRECONDITION(R.rows()<=R.cols());
  if (R.cols()>R.rows()) {
    DoubleVec y(cols(),rwUninitialized);
    DoubleUpperTriMat R1 = toUpperTriMat(R(RWAll,RWSlice(0,m)));
    DoubleGenMat R2 = R(RWAll,RWToEnd(m));
    y(RWSlice(0,m)) = product(x,R1);
    y(RWToEnd(m)) = product(x,R2);
    return y;
  } else {
    DoubleUpperTriMat R1 = toUpperTriMat(R);
    return product(x,R1);
  }
}

DoubleVec DoubleQRDecomp::Rinvx(const DoubleVec& x) const
{
  if (x.length()!=cols()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),cols())));
  if (cols()>rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_RSINGULAR)));
  DoubleVec y = x.copy();
  long n = cols();
  long m = rows();
  long one = 1;
  long info;
  dtrtrs('U','N','N',n,one,(Double*)QR_.data(),m,y.data(),n,info);
  RWPOSTCONDITION(info>=0);
  if (info>0) RWTHROW(RWInternalErr(RWMessage(RWLAPK_RSINGULAR)));
  return y;
}

DoubleVec DoubleQRDecomp::RTinvx(const DoubleVec& x) const
{
  if (x.length()!=cols()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),cols())));
  if (cols()>rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_RSINGULAR)));
  DoubleVec y = x.copy();
  long n = cols();
  long m = rows();
  long one = 1;
  long info;
  dtrtrs('U','T','N',n,one,(Double*)QR_.data(),m,y.data(),n,info);
  RWPOSTCONDITION(info>=0);
  if (info>0) RWTHROW(RWInternalErr(RWMessage(RWLAPK_RSINGULAR)));
  return y;
}

DoubleVec DoubleQRDecomp::Qx(const DoubleVec& x) const
{
  if (x.length()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),rows())));
  long m = rows();
  long n = cols();
  long k = (m<n) ? m : n;
  DoubleVec y = x.copy();
  long one = 1;
  long lwork = ilaenv( 1, "dormqr", "LN", m, one, k, -1 );
  Double *work = new Double [lwork];
  long info;
  dormqr('L','N',m,one,k,(Double*)QR_.data(),m,(Double*)tau_.data(),y.data(),m,work,lwork,info);
  RWPOSTCONDITION(info==0);
  RWPOSTCONDITION(lwork>=work[0]);
  delete [] work;
  return y;
}

DoubleVec DoubleQRDecomp::QTx(const DoubleVec& x) const
{
  if (x.length()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),rows())));
  long m = rows();
  long n = cols();
  long k = (m<n) ? m : n;
  DoubleVec y = x.copy();
  long one = 1;
  long lwork = ilaenv( 1, "dormqr", "LC", m, one, k, -1 );
  Double *work = new Double [lwork];
  long info;
  dormqr('L','T',m,one,k,(Double*)QR_.data(),m,(Double*)tau_.data(),y.data(),m,work,lwork,info);
  RWPOSTCONDITION(info==0);
  RWPOSTCONDITION(lwork>=work[0]);
  delete [] work;
  return y;
}

DoubleQRDecompServer::DoubleQRDecompServer()
{
  pivot_ = TRUE;
}

void DoubleQRDecompServer::setPivoting(RWBoolean x)
{
  pivot_ = x;
}

void DoubleQRDecompServer::setInitialIndex(int i)
{
  if (i<0) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTFREEZE,i)));
  for(int j=initial_.length(); j--;) {
    if (initial_(j)==i) return;   // already frozen
  }
  initial_.resize(initial_.length()+1);
  initial_(initial_.length()-1) = i;
}

void DoubleQRDecompServer::setFreeIndex(int i)
{
  if (i<0) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTFREE,i)));
  for(int j=initial_.length(); j--;) {
    if (initial_(j)==i) {
      int l = initial_.length()-j-1;
      if (l>0) {
        initial_(RWSlice(j,l)) = initial_(RWSlice(j+1,l));
      }
      initial_.resize(initial_.length()-1);
    }
  }
}

DoubleQRDecomp DoubleQRDecompServer::operator()(const DoubleGenMat& A) const
{
  long m = A.rows();
  long n = A.cols();
  long info;
  DoubleQRDecomp decomp;
  decomp.QR_.reference(A.copy());
  decomp.tau_.reshape((m<n) ? (int)m : (int)n);
  delete [] decomp.pivots_;
  decomp.pivots_ = 0;
  if (pivot_) {
    decomp.pivots_ = new long [n];
    for(int i=(int)n; i--;) decomp.pivots_[i]=0;
    for(i=initial_.length(); i--;) {
      long index = initial_(i);
      RWPRECONDITION(index>=0);
      if (index>=n) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTFREEZE,index)));
      decomp.pivots_[index] = 1;
    }
    Double *work = new Double [3*n];
    dgeqpf(m,n,decomp.QR_.data(),m,decomp.pivots_,decomp.tau_.data(),work,info);
    RWPOSTCONDITION(info==0);
    delete [] work;
  } else {
    long lwork = n*ilaenv( 1, "dgeqrf", " ", m, n, -1, -1 );
    Double *work = new Double [lwork];
    dgeqrf(m,n,decomp.QR_.data(),m,decomp.tau_.data(),work,lwork,info);
    RWPOSTCONDITION(info==0);
    RWPOSTCONDITION(lwork>=work[0]);
    delete [] work;
  }
  return decomp;
}           

/*
 * Implementation of DoubleTriDiagFact
 *
 * Generated from template $Id: xfct.cpp,v 1.6 1993/10/11 21:48:11 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/lapkdefs.h"
#include "rw/dtrdgfct.h"
#include "rw/lapack.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"  
#include "rw/dgenmat.h"

const char* DoubleTriDiagFact::className = "DoubleTriDiagFact";

DoubleTriDiagFact::~DoubleTriDiagFact()
{
  delete [] pvts;
}

DoubleTriDiagFact::DoubleTriDiagFact()
{
  info = -1;
  pvts=0;
}

DoubleTriDiagFact::DoubleTriDiagFact( const DoubleTriDiagFact& A )
  : dl(A.dl), d(A.d), du(A.du), d2(A.d2)
{
  info = A.info;
  Anorm = A.Anorm;
  unsigned n = A.rows();
  pvts = (n>0) ? new long [n] : 0;
  while(n--) {
    pvts[n] = A.pvts[n];
  }
}

DoubleTriDiagFact::DoubleTriDiagFact(const DoubleTriDiagMat& A, RWBoolean estimateCondition)
  // Initialize in the body so we can be careful of negative lengths
{
    d.reference(A.diagonal().copy());
    if (A.rows()>1) {
      dl.reference(A.diagonal(-1).copy());
      du.reference(A.diagonal(1).copy());
      d2.reshape(A.rows()-1);    // Only use n-2 elements, but this simplifies the code by eliminating if statements
    }
    pvts = 0;
    dofactor(estimateCondition);
}

void DoubleTriDiagFact::factor( const DoubleTriDiagMat& A, RWBoolean estimateCondition )
{
    d.reference(A.diagonal().copy());
    if (A.rows()>1) {
      dl.reference(A.diagonal(-1).copy());
      du.reference(A.diagonal(1).copy());
      d2.reshape(A.rows()-1);    // Only use n-2 elements, but this simplifies the code by eliminating if statements
    } else {
      dl.reshape(0);
      du.reshape(0);
      d2.reshape(0);
    }
    d2.reshape(A.rows()-2);
    dofactor(estimateCondition);
}

void DoubleTriDiagFact::dofactor(RWBoolean estimateCondition)
{
   info=0;   // for size zero matrices, need to set this explicitly
   long n = rows();	

    // Error (sanity) checking
    RWPRECONDITION(factorization.dataVec().stride()==1);

    delete [] pvts;
    pvts = (n>0) ? new long [n] : 0;

    // If we care to ever find the condition number, we need to know
    // the 1-norm of the matrix A.  Compute it while the member data
    // factorization is still a copy of A, and not yet it's factorization.
    Anorm = -1; 
    if (n>0 && estimateCondition) {
      Anorm = dlangt('1',n,dl.data(),d.data(),du.data());
    }

    if (n>0) {
      dgttrf(n,dl.data(),d.data(),du.data(),d2.data(),pvts,info);
      RWPOSTCONDITION(info>=0);
    }
}

RWBoolean DoubleTriDiagFact::fail() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}


RWBoolean DoubleTriDiagFact::isSingular() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}

Double DoubleTriDiagFact::condition() const
{
    Double Anormcopy = Anorm;      // Copy to stop warnings about ref parm
    long info;
    long n = rows();
    Double rcond;
    Double *work = new Double[4*n];   // Some routines only require 2*n, but what the hell
      long *iwork = new long [n];
      dgtcon('1',n,(Double*)dl.data(),(Double*)d.data(),(Double*)du.data(),(Double*)d2.data(),pvts,Anormcopy,rcond,work,iwork,info);
      delete iwork;
    delete [] work;
    RWPOSTCONDITION(info==0);
    return rcond;
}

DoubleVec DoubleTriDiagFact::solve( const DoubleVec& b ) const
{
  DoubleGenMat B(b,b.length(),1);
  DoubleGenMat X = solve(B);
  RWPOSTCONDITION(X.cols()==1);
  return X.col(0);
}

DoubleGenMat DoubleTriDiagFact::solve( const DoubleGenMat& B ) const
{
    DoubleGenMat X(B.copy());
    long n = rows();
    if (B.rows()!=n) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,B.rows(),n)));
    if (fail()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTSOLVE)));
    long nrhs = X.cols();
    long info;
    if (n>0) {
    dgttrs('N',n,nrhs,(Double*)dl.data(),(Double*)d.data(),(Double*)du.data(),(Double*)d2.data(),pvts,X.data(),n,info);
      RWPOSTCONDITION(info==0);
    }
    return X;    
}

Double DoubleTriDiagFact::determinant() const
{
    int numExchanges = 0;
    for(int i=rows(); i--;) {
      if (pvts[i]!=i+1) numExchanges++;
    }
    Double detLU = prod(d);
    return (numExchanges%2) ? -detLU : detLU;
}
              

/*
 * Global functions which provide alternate ways to call member functions.
 * These used to be inline, but this requires that you include more header
 * files on some compilers, and that is just not worth it.
 */
DoubleVec solve(const DoubleTriDiagFact& A, const DoubleVec& b) { return A.solve(b); }
DoubleGenMat solve(const DoubleTriDiagFact& A, const DoubleGenMat& b) { return A.solve(b); }
Double    determinant (const DoubleTriDiagFact& A)	      { return A.determinant(); }


Double condition(const DoubleTriDiagFact& A) { return A.condition(); }



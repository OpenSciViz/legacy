> ###############################################################
> # The header file for the reference intermediate classes      #
> #                                                             #
> # $Header: /users/rcs/lapksrc/xref.h,v 1.1 1993/06/23 18:28:11 alv Exp $
> #                                                             #
> # Instances of these classes are returned by the operator()   #
> # function for several matrices.  Specifically,               #
> # RO<T>Ref is returned by BandMat,TriMat,TriDiagMat           #
> # NG<T>Ref is returned by SkewMat                             #
> # CJ<T>Ref is returned by HermMat                             #
> # ROCJ<T>Ref is returned by HermBandMat                       #
> #                                                             #
> # Copyright (1991) by Rogue Wave Software, Inc.  This software#
> # is subject to copyright protection under the laws of the    #
> # United States and other countries.                          #
> #                                                             #
> # Written by Al Vermeulen.                                    #
> #                                                             #
> # Limited License.                                            #
> ###############################################################
> #
> # $Log: xref.h,v $
> # Revision 1.1  1993/06/23  18:28:11  alv
> # Initial revision
> #
> # Revision 1.1  1993/01/23  00:01:14  alv
> # Initial revision
> #
> # 
> #    Rev 1.1   17 Oct 1991 09:22:42   keffer
> # Changed include path to <rw/xxx.h>
> # 
>
> include macros
>
> beginwrapper <tLetter>Ref
>
/*
 * Generated from template $Id: xref.h,v 1.1 1993/06/23 18:28:11 alv Exp $
 *
 * Reference classes.
 *
 * Changed having a reference to "data" to using a pointer to "data"
 * because the bcc 3.0 compiler generates incorrect code sometimes for
 * classes containing a reference and no explicit copy constructor.
 * This is really too bad; the reference semantics make more sense here.
 * 
 * The operator<<() output routines were added because the 
 * Borland C++ compiler can't figure out enough to call the
 * conversion routine and then call the proper op<< routine.
 * For some bizarre reason it thinks there is a conflict with
 * converting the ref to a char*.  Hey, I don't pretend to understand.
 *
> copyright
 */

#include "rw/rstream.h"

class RO<T>Ref {
  private:
    <T>*        data;
    RWBoolean   readonly;
    <T>         error(<T>);   // Called when op=() invoked for a readonly
  public:
    RO<T>Ref(<T>& x, RWBoolean ro=FALSE) : data(&x) { readonly=ro; }
    RO<T>Ref& operator=(<T> x) { *data=(readonly?error(x):x); return *this; }
    operator <T>()             { return *data; }
    friend ostream& operator<<(ostream& s,RO<T>Ref& r) { return s<<*r.data; }
};

class NG<T>Ref {
  private:
    <T>*        data;
    RWBoolean   negate;
  public:
    NG<T>Ref(<T>& x, RWBoolean ng=FALSE) : data(&x) { negate=ng; }
    NG<T>Ref& operator=(<T> x)         { *data=(negate?-x:x); return *this; }
    operator <T>()                     { return negate?-*data:*data; }
    friend ostream& operator<<(ostream& s,NG<T>Ref& r) { return s<<(r.negate?-*r.data:*r.data); }
};

> if <R/C>==Complex
>
class CJ<T>Ref {
  private:
    <T>*        data;
    RWBoolean   conjugate;
  public:
    CJ<T>Ref(<T>& x, RWBoolean cj=FALSE) : data(&x) { conjugate=cj; }
    CJ<T>Ref& operator=(<T> x)    { *data=(conjugate?conj(x):x); return *this; }
    operator <T>()                { return conjugate?conj(*data):*data; }
    friend ostream& operator<<(ostream& s,CJ<T>Ref& r) { return s<<<T>(r); }
};

class ROCJ<T>Ref {
  private:
    <T>*                data;
    unsigned char       rocj;         // 0:readable, 1:readonly, 2:conj
    <T>                 error(<T>);   // Called when op=() invoked for a readonly
  public:
    ROCJ<T>Ref(<T>& x, RWBoolean ro=FALSE, RWBoolean cj=FALSE) : data(&x)
                { rocj=(ro ? 1 : (cj ? 2 : 0 )); }
    ROCJ<T>Ref& operator=(<T> x)
        { *data=( (rocj==0) ? x : ((rocj==2) ? conj(x) : error(x)) );
          return *this;}
    operator <T>()      { return (rocj==2) ? conj(*data) : *data; }
    friend ostream& operator<<(ostream& s,ROCJ<T>Ref& r) { return s<<<T>(r); }
};

> endif
>
> endwrapper

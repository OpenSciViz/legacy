> #################################################################
> # Least squares fitting using SVD                               #
> #                                                               #
> # $Header: /users/rcs/lapksrc/xlssv.h,v 1.3 1993/07/05 19:19:33 alv Exp $
> #                                                               #
> # Copyright (1991) by Rogue Wave Software, Inc.  This software  #
> # is subject to copyright protection under the laws of the      #
> # United States and other countries.                            #
> #                                                               #
> # Written by Al Vermeulen.                                      #
> #                                                               #
> # Limited License.                                              #
> #################################################################
> #
> # $Log: xlssv.h,v $
> # Revision 1.3  1993/07/05  19:19:33  alv
> # fixed up for complex numbers
> #
> # Revision 1.2  1993/06/01  15:09:39  alv
> # added global fn residualNorm
> #
> # Revision 1.1  1993/05/18  17:00:02  alv
> # Initial revision
> #
>
> define <T>=RW_T_RW
> if <T>==Double
> define <P>=Double <tLetter>=d
> elif <T>==Float
> define <P>=Float <tLetter>=f
> elif <T>==DComplex
> define <P>=Double <tLetter>=c
> else
> err least square class: not defined for '<T>'
> endif
> 
> # The traditional top of the file stuff
>
> beginwrapper <tLetter>lssv
>
/*
 * <T>LeastSqSV:  solve least square SV problem with orthogonal decomposition
 *
 * Generated from template $Id: xlssv.h,v 1.3 1993/07/05 19:19:33 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 * Uses a singular value decomposition to solve the linear
 * least squares problem.  The solution is the minimum norm solution
 * which minimizes the residual.
 *
 * When building a factorization using either a constructor or one
 * of the factor functions, you can supply a tolerance parameter.
 * Entries along the diagonal of the R factor of the SV decomposition
 * less than this tolerance are treated as zero.  Using this can
 * prevent insignificant entries of R from corrupting your solution.
 *
 * The factor functions and constructors taking a SV decomposition
 * are useful if you'd like to examine the singular values
 * for yourself and decide what tolerance to use.
 */

#include "rw/<tLetter>sv.h"

class <T>LeastSqSV : public <T>SVDecomp {
public:
  <T>LeastSqSV();
  <T>LeastSqSV(const <T>GenMat& A, <P> tol=0);
  <T>LeastSqSV(const <T>SVDecomp& A, <P> tol=0);
//void      factor(const <T>GenMat& A, <P> tol=0);
//RWBoolean good() const;
//RWBoolean fail() const;
//unsigned  rows() const {return decomp_.rows();}
//unsigned  cols() const {return decomp_.cols();}
//unsigned  rank() const {return decomp_.rank();}
//void      truncate(<P> tol);
  <T>Vec    residual(const <T>Vec& data) const;
  <P>       residualNorm(const <T>Vec& data) const;
  <T>Vec    solve(const <T>Vec& data) const;
};

inline <T>Vec solve(   const <T>LeastSqSV& A, const <T>Vec& b) {return A.solve(b);}
inline <T>Vec residual(const <T>LeastSqSV& A, const <T>Vec& b) {return A.residual(b);}
inline <T>    residualNorm(const <T>LeastSqSV& A, const <T>Vec& b) {return A.residualNorm(b);}

> endwrapper

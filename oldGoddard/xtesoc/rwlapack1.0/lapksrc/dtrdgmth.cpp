#if 0
/*
 * Generated from template $Id: xmth.cpp,v 1.2 1993/10/11 21:48:17 alv Exp $
 *
 * Math member functions for the matrix types
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */
#endif

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/dtrdgmat.h"

/*
 * This function returns a new matrix the same shape as the one 
 * passed in but with the data passed in.  This way we restrict
 * all the >if statements to a couple lines
 */
inline DoubleTriDiagMat sameShapeMatrix( DoubleVec& vec, const DoubleTriDiagMat& A )
{
  return DoubleTriDiagMat(vec,A.rows(),A.cols());
}


static void verifyMatch(const DoubleTriDiagMat& A, const DoubleTriDiagMat& B)
{
    if (A.rows()!=B.rows() || A.cols()!=B.cols())
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATSIZE,A.rows(),A.cols(),B.rows(),B.cols())));
}

RWBoolean DoubleTriDiagMat::operator==(const DoubleTriDiagMat& X)
{
    if (n!=X.n) return FALSE;
    // The following two statements cannot be combined into one
    // because of a cfront limitation:
    if ( n<1 ) return TRUE;
    return vec.slice(1,3*n-2)==X.vec.slice(1,3*n-2);
}


DoubleTriDiagMat& DoubleTriDiagMat::operator+=(const DoubleTriDiagMat& m)
{
  verifyMatch(*this,m);
  vec += m.vec;
  return *this;
}

DoubleTriDiagMat& DoubleTriDiagMat::operator-=(const DoubleTriDiagMat& m)
{
  verifyMatch(*this,m);
  vec -= m.vec;
  return *this;
}

DoubleTriDiagMat& DoubleTriDiagMat::operator*=(const DoubleTriDiagMat& m)
{
  verifyMatch(*this,m);
  vec *= m.vec;
  return *this;
}

DoubleTriDiagMat& DoubleTriDiagMat::operator*=(Double x)
{
  vec *= x;
  return *this;
}

DoubleTriDiagMat& DoubleTriDiagMat::operator/=(const DoubleTriDiagMat& m)
{
  verifyMatch(*this,m);
  vec /= m.vec;
  return *this;
}

DoubleTriDiagMat& DoubleTriDiagMat::operator/=(Double x)
{
  vec /= x;
  return *this;
}




DoubleTriDiagMat operator-(const DoubleTriDiagMat& A)
{
  DoubleVec temp(-(A.dataVec()));
  return sameShapeMatrix(temp,A);
}
  
DoubleTriDiagMat operator+(const DoubleTriDiagMat& A)
{
  return A;
}

DoubleTriDiagMat operator*(const DoubleTriDiagMat& A, const DoubleTriDiagMat& B) {
	verifyMatch(A,B);
	DoubleVec temp(A.dataVec()*B.dataVec());
	return sameShapeMatrix(temp,A);
}

DoubleTriDiagMat operator/(const DoubleTriDiagMat& A, const DoubleTriDiagMat& B) {
	verifyMatch(A,B);
	DoubleVec temp(A.dataVec()/B.dataVec());
	return sameShapeMatrix(temp,A);
}

DoubleTriDiagMat operator+(const DoubleTriDiagMat& A, const DoubleTriDiagMat& B) {
	verifyMatch(A,B);
	DoubleVec temp(A.dataVec()+B.dataVec());
	return sameShapeMatrix(temp,A);
}

DoubleTriDiagMat operator-(const DoubleTriDiagMat& A, const DoubleTriDiagMat& B) {
	verifyMatch(A,B);
	DoubleVec temp(A.dataVec()-B.dataVec());
	return sameShapeMatrix(temp,A);
}

DoubleTriDiagMat operator*(const DoubleTriDiagMat& A, Double x) {
	DoubleVec temp(A.dataVec()*x);
	return sameShapeMatrix(temp,A);
}

/*
 * Out-of-line version for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
  DoubleTriDiagMat operator*(Double x, const DoubleTriDiagMat& A) { return A*x; }
#endif

DoubleTriDiagMat operator/(const DoubleTriDiagMat& A, Double x) {
	DoubleVec temp(A.dataVec()/x);
	return sameShapeMatrix(temp,A);
}





DoubleTriDiagMat abs(const DoubleTriDiagMat& A) {
	DoubleVec temp(abs(A.dataVec()));
	return sameShapeMatrix(temp,A);
}



DoubleTriDiagMat transpose(const DoubleTriDiagMat& A)
{
  DoubleTriDiagMat T(A.rows(),A.cols());
  T.diagonal(-1) = A.diagonal(1);
  T.diagonal(0) = A.diagonal(0);
  T.diagonal(1) = A.diagonal(-1);
  return T;
}

DoubleVec product(const DoubleTriDiagMat& A, const DoubleVec& x)
{
    if (A.cols()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATVECPROD,A.className,A.rows(),A.cols(),x.length())));
    }
    DoubleVec y(A.rows(),rwUninitialized);
    y = A.diagonal(0) * x;
    y.slice(1,x.length()-1) += A.diagonal(-1) * x.slice(0,x.length()-1);
    y.slice(0,x.length()-1) += A.diagonal(1) * x.slice(1,x.length()-1);
    return y;
}

DoubleVec product(const DoubleVec& x, const DoubleTriDiagMat& A)
{
    if (A.rows()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECMATPROD,x.length(),A.className,A.rows(),A.cols())));
    }
    DoubleVec y(A.cols(),rwUninitialized);
    y = A.diagonal(0) * x;
    y.slice(1,x.length()-1) += A.diagonal(1) * x.slice(0,x.length()-1);
    y.slice(0,x.length()-1) += A.diagonal(-1) * x.slice(1,x.length()-1);
    return y;
}



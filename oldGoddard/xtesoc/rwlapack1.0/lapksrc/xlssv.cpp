> #################################################################
> # Least squares fitting using singular value decomposition      #
> #                                                               #
> # $Header: /users/rcs/lapksrc/xlssv.cpp,v 1.3 1993/10/11 21:48:15 alv Exp $
> #                                                               #
> # Copyright (1991) by Rogue Wave Software, Inc.  This software  #
> # is subject to copyright protection under the laws of the      #
> # United States and other countries.                            #
> #                                                               #
> # Written by Al Vermeulen.                                      #
> #                                                               #
> # Limited License.                                              #
> #################################################################
> #
> # $Log: xlssv.cpp,v $
> # Revision 1.3  1993/10/11  21:48:15  alv
> # synch'ed with Math.h++ v5.1
> #
> # Revision 1.2  1993/07/05  19:19:33  alv
> # fixed up for complex numbers
> #
> # Revision 1.1  1993/05/18  17:00:02  alv
> # Initial revision
> #
>
> define <T>=RW_T_RW
> if <T>==Double
> define <P>=Double <tLetter>=d
> elif <T>==Float
> define <P>=Float <tLetter>=f
> elif <T>==DComplex
> define <P>=Double <tLetter>=c
> else
> err least square class: not defined for '<T>'
> endif
> 
/*
 * defs for <T>LeastSqSV
 *
 * Generated from template $Id: xlssv.cpp,v 1.3 1993/10/11 21:48:15 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 */

#include "rw/<tLetter>lssv.h"
#include "rw/lapkerr.h"
#include "rw/rwerr.h"

<T>LeastSqSV::<T>LeastSqSV()                                 : <T>SVDecomp() {}
<T>LeastSqSV::<T>LeastSqSV(const <T>GenMat& A, <P> tol)      : <T>SVDecomp(A,tol) {}
<T>LeastSqSV::<T>LeastSqSV(const <T>SVDecomp& A, <P> tol)    : <T>SVDecomp(A) {truncate(tol);}

static <T>GenMat U(const <T>LeastSqSV& x)
{
  if (x.numLeftVectors()<x.rank()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOSV,x.rank())));
  return x.leftVectors()(RWAll,RWSlice(0,x.rank()));
}

static <T>GenMat V(const <T>LeastSqSV& x)
{
  if (x.numRightVectors()<x.rank()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOSV,x.rank())));
  return x.rightVectors()(RWAll,RWSlice(0,x.rank()));
}

<T>Vec <T>LeastSqSV::residual(const <T>Vec& data) const
{
  if (data.length()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,data.length(),rows())));
  <T>Vec x = solve(data);
  <T>Vec VTx = product(adjoint(V(*this)),x);
  VTx *= singularValues();
  <T>Vec Ax = product(U(*this),VTx);
  return data-Ax;
}

<P> <T>LeastSqSV::residualNorm(const <T>Vec& data) const
{
  if (data.length()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,data.length(),rows())));
  <T>Vec r = residual(data);
  return l2Norm(r);
}

<T>Vec <T>LeastSqSV::solve(const <T>Vec& data) const
{
  if (data.length()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,data.length(),rows())));
  <T>Vec x = product(adjoint(U(*this)),data);
  x /= singularValues();
  return product(V(*this),x);
} 

/*
 * Definitions for DComplexCODecomp
 *
 * Generated from template $Id: xco.cpp,v 1.4 1993/10/11 21:48:06 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 */

#include "rw/lapack.h"
#include "rw/cco.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/cutrimat.h"

DComplexCODecomp::DComplexCODecomp()
{
}

DComplexCODecomp::DComplexCODecomp(const DComplexCODecomp& x)
{
  *this = x;
}

void DComplexCODecomp::operator=(const DComplexCODecomp& x)
{
  QRdecomp_ = x.QRdecomp_;
  Ztau_.reference(x.Ztau_);
  TZ_.reference(x.TZ_);
}

void DComplexCODecomp::dofactor(double tol)
{
  long n = cols();
  int m = (rows()<cols()) ? rows() : cols();
  while (m>0 && norm(QRdecomp_.QR_(m-1,m-1))<tol) m--;
  TZ_.reference(QRdecomp_.QR_(RWSlice(0,m),RWAll));
  Ztau_.reshape(m);
  long info;
  ztzrqf(m,n,TZ_.data(),TZ_.colStride(),Ztau_.data(),info);
  RWPOSTCONDITION(info==0);
}

DComplexCODecomp::DComplexCODecomp(const DComplexGenMat& A, double tol)
  : QRdecomp_(A)
{
  dofactor(tol);   
}

DComplexCODecomp::DComplexCODecomp(const DComplexQRDecomp& A, double tol)
  : QRdecomp_(A)
{
  QRdecomp_.QR_.deepenShallowCopy();
  dofactor(tol);   
}

void DComplexCODecomp::factor(const DComplexGenMat& A, double tol)
{
  QRdecomp_.factor(A);
  dofactor(tol);   
}

void DComplexCODecomp::factor(const DComplexQRDecomp& A, double tol)
{
  QRdecomp_ = A;
  QRdecomp_.QR_.deepenShallowCopy();
  dofactor(tol);   
}

DComplexCODecomp::~DComplexCODecomp()
{
}

DComplexUpperTriMat DComplexCODecomp::T() const
{
  return toUpperTriMat(TZ_.operator()(RWAll,RWSlice(0,rank())));  // explicit op() to shut up sun 3.0.1 warning
}

// Generate Z as Z = I P1 P2 P3 ... Pn
//
DComplexGenMat      DComplexCODecomp::Z() const
{
  DComplexGenMat Zmat(cols(),cols(),(DComplex)0);  // initialize Z to identity matrix
  Zmat.diagonal() = (DComplex)1;
  if (cols()>rank()) {
    long m = cols();
    long n = m-rank()+1;
    DComplex *work = new DComplex [m];
    DComplexGenMat ZmatRight = Zmat(RWAll,RWToEnd(rank()));
    DComplexGenMat TZRight = TZ_.operator()(RWAll,RWToEnd(rank()));  // explicit op() to shut up sun 3.0.1 warning
    DComplex *c2 = ZmatRight.data();
    long ldc = Zmat.colStride();
    long incv = TZRight.colStride();
    for(int i=rank(); i--;) {
      DComplex *v = TZRight.row(i).data();
      DComplex *c1 = Zmat.col(i).data();
      zlatzm('R',m,n,v,incv,Ztau_(i),c1,c2,ldc,work);
    }
    delete [] work;
  }
  return Zmat;   
}

DComplexVec DComplexCODecomp::Tx(const DComplexVec& x) const
{
  if (x.length()!=rank()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),rank())));
  if (cols()>rank()) {
    DComplexUpperTriMat T = toUpperTriMat(TZ_.operator()(RWAll,RWSlice(0,rank())));  // explicit op() to shut up sun 3.0.1 warning
    return product(T,x);
  } else {    // T==R
    return QRdecomp_.Rx(x);
  }
}

DComplexVec DComplexCODecomp::TTx(const DComplexVec& x) const
{
  if (x.length()!=rank()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),rank())));
  if (cols()>rank()) {
    DComplexUpperTriMat T = toUpperTriMat(TZ_.operator()(RWAll,RWSlice(0,rank())));  // explicit op() to shut up sun 3.0.1 warning
    return product(x,conj(T));
  } else {    // T==R
    return QRdecomp_.RTx(x);
  }
}

DComplexVec DComplexCODecomp::Tinvx(const DComplexVec& x) const
{
  if (x.length()!=rank()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),rank())));
  DComplexVec y = x.copy();
  long n = rank();
  long lda = TZ_.colStride();
  long one = 1;
  long info;
  ztrtrs('U','N','N',n,one,(DComplex*)TZ_.data(),lda,y.data(),n,info);
  RWPOSTCONDITION(info==0);
  return y;
}

DComplexVec DComplexCODecomp::TTinvx(const DComplexVec& x) const
{
  if (x.length()!=rank()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),rank())));
  DComplexVec y = x.copy();
  long n = rank();
  long lda = TZ_.colStride();
  long one = 1;
  long info;
  ztrtrs('U','C','N',n,one,(DComplex*)TZ_.data(),lda,y.data(),n,info);
  RWPOSTCONDITION(info==0);
  return y;
}

DComplexVec DComplexCODecomp::Zx(const DComplexVec& x) const
{
  if (x.length()!=cols()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),cols())));  
  DComplexVec y = x.copy();
  if (cols()>rank()) {
    long n = 1;
    long m = cols()-rank()+1;
    DComplex work[1];
    DComplexGenMat TZRight = TZ_.operator()(RWAll,RWToEnd(rank()));  // explicit op() to shut up sun 3.0.1 warning
    DComplex *c2 = y.data() + rank();
    long ldc = y.length();
    long incv = TZRight.colStride();
    for(int i=0; i<rank(); i++) {
      DComplex *c1 = &(y(i));
      DComplex *v = TZRight.row(i).data();
      zlatzm('L',m,n,v,incv,Ztau_(i),c1,c2,ldc,work);
    }
  }
  return y;   
}

DComplexVec DComplexCODecomp::ZTx(const DComplexVec& x) const
{
  if (x.length()!=cols()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),cols())));  
  // To compute Z'x, use relationship y' = x'Z
  DComplexVec y = conj(x);
  y.deepenShallowCopy();
  if (cols()>rank()) {
    long m = 1;
    long n = cols()-rank()+1;
    DComplex work[1];
    DComplexGenMat TZRight = TZ_.operator()(RWAll,RWToEnd(rank()));  // explicit op() to shut up sun 3.0.1 warning
    DComplex *c2 = y.data() + rank();
    long ldc = 1;
    long incv = TZRight.colStride();
    for(int i=rank(); i--;) {
      DComplex *c1 = &(y(i));
      DComplex *v = TZRight.row(i).data();
      zlatzm('R',m,n,v,incv,Ztau_(i),c1,c2,ldc,work);
    }
  }
  return conj(y);   
}


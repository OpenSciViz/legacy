/*
 * defs for DComplexSchurDecomp
 *
 * Generated from template $Id: xschur.cpp,v 1.4 1993/10/11 21:48:20 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 */ 

#include "rw/cschur.h"
#include "rw/chess.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/lapack.h"
#include "rw/ivec.h"
#include "rw/ivecpik.h"

DComplexSchurDecomp::DComplexSchurDecomp()
{
}

DComplexSchurDecomp::DComplexSchurDecomp(const DComplexGenMat& A, RWBoolean permute, RWBoolean scale)
{
  factor(A,permute,scale);
}

DComplexSchurDecomp::DComplexSchurDecomp(const DComplexBalanceDecomp& A)
{
  factor(A);
}

DComplexSchurDecomp::DComplexSchurDecomp(const DComplexHessenbergDecomp& A)
{
  factor(A);
}

void DComplexSchurDecomp::factor(const DComplexGenMat& A, RWBoolean permute, RWBoolean scale)
{
  DComplexBalanceDecomp B(A,permute,scale);
  factor(B);
}

void DComplexSchurDecomp::factor(const DComplexBalanceDecomp& A)
{
  DComplexHessenbergDecomp B(A);
  factor(B);
}

RWBoolean DComplexSchurDecomp::good() const
{
  return (w_.length()==rows());
}

void DComplexSchurDecomp::factor(const DComplexHessenbergDecomp& A)
{
  long n = A.rows();
  B_ = A.balanceTransform();
  T_.reference(A.H());
  Z_.reference(A.Q());
  w_.reshape(A.rows());
  long ilo = B_.lowIndex();
  long ihi = B_.highIndex();
  DComplex *work = new DComplex [n];
  long info;
  zhseqr('S','V',n,ilo,ihi,T_.data(),T_.colStride(),w_.data(),Z_.data(),Z_.colStride(),work,0,info);
  RWPOSTCONDITION(info>=0);
  if (info>0) {
    RWPOSTCONDITION(ilo-1+n-info<n);
    w_(RWSlice((int)(ilo),(int)(n-info))) = w_(RWToEnd((int)info));
    w_.resize((int)(ilo-1+n-info));
  }
  delete work;
}

DComplexGenMat DComplexSchurDecomp::B() const
{
  DComplexGenMat I(rows(),cols(),(DComplex)0);
  I.diagonal() = (DComplex)1;
  return B_.transform(I);
}

DComplexGenMat DComplexSchurDecomp::Z() const {return Z_;}
DComplexGenMat DComplexSchurDecomp::T() const {return T_;}

DComplex DComplexSchurDecomp::eigenvalue(int i) const
{
  if (i<0 || i>=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOEIG,i)));
  return w_(i);
}

DComplexVec DComplexSchurDecomp::eigenvalues() const
{
  return w_;
}

DComplexVec DComplexSchurDecomp::Bx(const DComplexVec& x) const
{                                         
  return B_.transform(x);
}

DComplexVec DComplexSchurDecomp::BInvTx(const DComplexVec& x) const
{
  return B_.invTransform(x);
}

DComplexVec DComplexSchurDecomp::Zx(const DComplexVec& x) const
{
  return product(Z_,x);
}

DComplexVec DComplexSchurDecomp::ZTx(const DComplexVec& x) const
{
  return conj(product(conj(x),Z_));
}

DComplexGenMat DComplexSchurDecomp::BX(const DComplexGenMat& X) const
{                                         
  return B_.transform(X);
}
 
DComplexGenMat DComplexSchurDecomp::BInvTX(const DComplexGenMat& X)const
{                                         
  return B_.invTransform(X);
}

DComplexGenMat DComplexSchurDecomp::ZX(const DComplexGenMat& X) const
{
  return product(Z_,X);
}

DComplexGenMat DComplexSchurDecomp::ZTX(const DComplexGenMat& X) const
{
  return transposeProduct(Z_,X);
}

int DComplexSchurDecomp::move(int from, int to)
{
  if (!good()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_SCHURREORDER)));
  long n = rows();
  long ifst = from;
  long ilst = to;
  long info;
  ztrexc('V',n,T_.data(),T_.colStride(),Z_.data(),Z_.colStride(),ifst,ilst,info);
  RWPOSTCONDITION(info==0 || info==1);
  return (int) ilst;
}

RWBoolean DComplexSchurDecomp::moveToFront(const IntVec& indices)
{
  if (!good()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_SCHURREORDER)));
  IntVec select(rows(),0);
  select.pick(indices) = 1;
  long m;
  Double s,sep;
  long n = rows();
  long lwork = n*(n+1)/2;
  DComplex *work = new DComplex [lwork];
  long ldt = T_.colStride();
  long ldz = Z_.colStride();
  long info;
  ztrsen('N','V',select.data(),n,T_.data(),ldt,Z_.data(),ldz,
                 w_.data(),m,s,sep,work,lwork,info);
  RWPOSTCONDITION(info==0 || info==1);
  delete work;
  return info==0;
}

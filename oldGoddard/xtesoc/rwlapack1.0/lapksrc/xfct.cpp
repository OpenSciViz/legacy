> #################################################################
> # The template for the main code file for the factorizations    #
> #                                                               #
> # $Header: /users/rcs/lapksrc/xfct.cpp,v 1.6 1993/10/11 21:48:11 alv Exp $
> #                                                               #
> # Copyright (1993) by Rogue Wave Software, Inc.  This software  #
> # is subject to copyright protection under the laws of the      #
> # United States and other countries.                            #
> #                                                               #
> # Written by Al Vermeulen.                                      #
> #                                                               #
> # Limited License.                                              #
> #################################################################
> #
> # $Log: xfct.cpp,v $
> # Revision 1.6  1993/10/11  21:48:11  alv
> # synch'ed with Math.h++ v5.1
> #
> # Revision 1.5  1993/07/08  23:26:13  alv
> # ported to SUN CC v3
> #
> # Revision 1.4  1993/07/05  19:19:33  alv
> # fixed up for complex numbers
> #
> # Revision 1.3  1993/04/07  17:23:51  alv
> # added some POSTCONDITION checks
> #
> # Revision 1.2  1993/04/06  20:45:04  alv
> # ported to sun C++ v3.0.1
> #
> #
> # Rewrote Linpack.h++'s fact.ctm for use in Lapack.h++
>
> include fct.mac
>
/*
 * Implementation of <C>
 *
 * Generated from template $Id: xfct.cpp,v 1.6 1993/10/11 21:48:11 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/lapkdefs.h"
#include "rw/<tLetter><factAbbrev>fct.h"
#include "rw/lapack.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"  
#include "rw/<tLetter>genmat.h"

const char* <T><F>::className = "<T><F>";

<T><F>::~<T><F>()
{
> if <F>!=PDFact && <F>!=PDBandFact && <F>!=PDTriDiagFact
  delete [] pvts;
> endif
}

<T><F>::<T><F>()
{
  info = -1;
> if <F>!=PDFact && <F>!=PDBandFact && <F>!=PDTriDiagFact
  pvts=0;
> endif
}

<T><F>::<T><F>( const <T><F>& A )
> if <F>==TriDiagFact
  : dl(A.dl), d(A.d), du(A.du), d2(A.d2)
> elif <F>==PDTriDiagFact
  : d(A.d), e(A.e)
> else
  : factorization(A.factorization)
> endif
{
  info = A.info;
  Anorm = A.Anorm;
> if <F>!=PDFact && <F>!=PDBandFact && <F>!=PDTriDiagFact
  unsigned n = A.rows();
  pvts = (n>0) ? new long [n] : 0;
  while(n--) {
    pvts[n] = A.pvts[n];
  }
> endif
}

<T><F>::<T><F>(const <T><M>& A, RWBoolean estimateCondition)
> if <F>==BandFact
  : factorization(A.rows(),A.cols(),A.lowerBandwidth(),A.lowerBandwidth()+A.upperBandwidth() )
> elif <F>==TriDiagFact || <F>==PDTriDiagFact
  // Initialize in the body so we can be careful of negative lengths
> else
  : factorization(A)  // Use reference semantics, followed by
                      // deepenShallowCopy(), to work around a BCC V3.0 bug
> endif
{
> if <F>==BandFact
    factorization = A;
> elif <F>==TriDiagFact
    d.reference(A.diagonal().copy());
    if (A.rows()>1) {
      dl.reference(A.diagonal(-1).copy());
      du.reference(A.diagonal(1).copy());
      d2.reshape(A.rows()-1);    // Only use n-2 elements, but this simplifies the code by eliminating if statements
    }
> elif <F>==PDTriDiagFact
>  if <R/C>==Complex
    RWPRECONDITION(A.diagonal(-1)==conj(A.diagonal(1)));
    d.reference(real(A.diagonal()).copy());
>  else
    RWPRECONDITION(A.diagonal(-1)==A.diagonal(1));
    d.reference(A.diagonal().copy());
>  endif
    if (A.rows()>1) {
      e.reference(A.diagonal(1).copy());
    } 
> else
    factorization.deepenShallowCopy();
> endif
> if <F>!=PDFact && <F>!=PDBandFact && <F>!=PDTriDiagFact
    pvts = 0;
> endif
    dofactor(estimateCondition);
}

void <T><F>::factor( const <T><M>& A, RWBoolean estimateCondition )
{
> if <F>==BandFact
    <T><M> F(A.rows(),A.cols(),A.lowerBandwidth(),A.lowerBandwidth()+A.upperBandwidth());
    F = A;
    factorization.reference(F);
> elif <F>==TriDiagFact
    d.reference(A.diagonal().copy());
    if (A.rows()>1) {
      dl.reference(A.diagonal(-1).copy());
      du.reference(A.diagonal(1).copy());
      d2.reshape(A.rows()-1);    // Only use n-2 elements, but this simplifies the code by eliminating if statements
    } else {
      dl.reshape(0);
      du.reshape(0);
      d2.reshape(0);
    }
> elif <F>==PDTriDiagFact
>  if <R/C>==Complex
    RWPRECONDITION(A.diagonal(-1)==conj(A.diagonal(1)));
    d.reference(real(A.diagonal()).copy());
>  else
    RWPRECONDITION(A.diagonal(-1)==A.diagonal(1));
    d.reference(A.diagonal().copy());
>  endif
    if (A.rows()>1) {
      e.reference(A.diagonal(1).copy());
    } else {
      e.reshape(0);
    }
> else
    <T><M> Acopy = A.copy();
    factorization.reference(Acopy);
> endif
> if <F>==TriDiagFact
    d2.reshape(A.rows()-2);
> endif
    dofactor(estimateCondition);
}

void <T><F>::dofactor(RWBoolean estimateCondition)
{
   info=0;   // for size zero matrices, need to set this explicitly
   long n = rows();	

    // Error (sanity) checking
> if <M>==GenMat
    RWPRECONDITION(n==factorization.cols());
> endif
> if <M>==GenMat
    RWPRECONDITION(factorization.rowStride()==1 && factorization.colStride()==factorization.rows());
> elif <M>==PDTriDiagFact
> # positive definite tri-diag does not store a factorization matrix
> else
    RWPRECONDITION(factorization.dataVec().stride()==1);
> endif     
>
> if <M>==HermMat || <M>==HermBandMat
    for(int r=rows(); r--;) {
      if (imag(factorization.val(r,r))!=0.0) {
	RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTHERM)));
      }
    }
> endif

> if <F>!=PDFact && <F>!=PDBandFact && <F>!=PDTriDiagFact
    delete [] pvts;
    pvts = (n>0) ? new long [n] : 0;
> endif

    // If we care to ever find the condition number, we need to know
    // the 1-norm of the matrix A.  Compute it while the member data
    // factorization is still a copy of A, and not yet it's factorization.
    Anorm = -1; 
    if (n>0 && estimateCondition) {
> if <F>==GenFact
      Anorm = <linletter>lan<linshape>('1',n,n,factorization.data(),n,0);
> elif <F>==SymFact || <F>==PDFact
      <P> *work = new <P> [n];
      Anorm = <linletter>lansp('1','U',n,factorization.data(),work);
      delete [] work;
> elif <F>==HermFact
      <P> *work = new <P> [n];
      Anorm = <linletter>lanhp('1','U',n,factorization.data(),work);
      delete [] work;
> elif <F>==BandFact
      long kl = factorization.lowerBandwidth();
      long ku = factorization.upperBandwidth();
      long b = factorization.bandwidth();
      Anorm = <linletter>langb('1',n,kl,ku,factorization.data(),b,0);
> elif <F>==PDBandFact
      long kd = factorization.halfBandwidth();
      long ldab = kd+1;
      <P> *work = new <P> [n];
      Anorm = <linletter>lansb('1','U',n,kd,factorization.data(),ldab,work);
      delete [] work;
> elif <F>==TriDiagFact
      Anorm = <linletter>langt('1',n,dl.data(),d.data(),du.data());
> elif <F>==PDTriDiagFact
>   if <R/C>==Complex
      // For some reason, zlanst() is the name of the routine
      // used to compute complex Hermitian tri-diagonal norms (as
      // opposed to the more logical zlanht()).  So the following
      // is not a typo.
>   endif
      Anorm = <linletter>lanst('1',n,d.data(),e.data());
> else 
> err norm finding not yet defined for factorization type <F>
> endif
    }

    if (n>0) {
>
> # call the appropriate lapack routine
>
> if <F>==GenFact
#if defined(RW_USE_UNBLOCKED)
      <lintype>tf2(n,n,factorization.data(),n,pvts,info);
#else
      <lintype>trf(n,n,factorization.data(),n,pvts,info);
#endif
> elif <F>==SymFact || <F>==HermFact
      <lintype>trf('U',n,factorization.data(),pvts,info);
> elif <F>==PDFact
      <lintype>trf('U',n,factorization.data(),info);
> elif <F>==BandFact
      long b = factorization.bandwidth();
      long l = factorization.lowerBandwidth();
      long u = factorization.upperBandwidth()-factorization.lowerBandwidth();
      <lintype>trf(n,n,l,u,factorization.data(),b,pvts,info);
> elif <F>==PDBandFact
      long kd = factorization.halfBandwidth();
      long ldab = kd+1;
      <lintype>trf('U',n,kd,factorization.data(),ldab,info);
> elif <F>==TriDiagFact
      <lintype>trf(n,dl.data(),d.data(),du.data(),d2.data(),pvts,info);
> elif <F>==PDTriDiagFact
      <lintype>trf(n,d.data(),e.data(),info);
> else 
> err factor not yet defined for factorization type <F>
> endif
      RWPOSTCONDITION(info>=0);
    }
}

RWBoolean <T><F>::fail() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}

> if <F>==PDFact || <F>==PDBandFact || <F>==PDTriDiagFact
RWBoolean <T><F>::isPD() const
{
    return (rows()>0 && info!=0) ? FALSE : TRUE;
}  
> endif

RWBoolean <T><F>::isSingular() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}

<P> <T><F>::condition() const
{
    <P> Anormcopy = Anorm;      // Copy to stop warnings about ref parm
    long info;
    long n = rows();
    <P> rcond;
    <T> *work = new <T>[4*n];   // Some routines only require 2*n, but what the hell
> if <F>==GenFact
>  if <R/C>==Real
    long *work2 = new long [n];
>  else
    <P> *work2 = new <P> [2*n];
>  endif
    <lintype>con('1',n,(<T>*)factorization.data(),n,Anormcopy,rcond,work,work2,info);
    delete [] work2;
>
> elif <F>==SymFact || <F>==HermFact
>  if <R/C>==Real
    long *iwork = new long [n];
    <lintype>con('U',n,(<T>*)factorization.data(),pvts,Anormcopy,rcond,work,iwork,info);
    delete [] iwork;
>  else
    <lintype>con('U',n,(<T>*)factorization.data(),pvts,Anormcopy,rcond,work,info);
>  endif
>
> elif <F>==PDFact
>  if <R/C>==Real
    long *work2 = new long [n];
>  else
    <P> *work2 = new <P> [n];
>  endif
    <lintype>con('U',n,(<T>*)factorization.data(),Anormcopy,rcond,work,work2,info);
    delete [] work2;    
>
> elif <F>==BandFact
    long b = factorization.bandwidth();
    long l = factorization.lowerBandwidth();
    long u = factorization.upperBandwidth()-factorization.lowerBandwidth();
>  if <R/C>==Real
    long *work2 = new long [n];
>  else
    <P> *work2 = new <P> [n];
>  endif
    <lintype>con('1',n,l,u,(<T>*)factorization.data(),b,pvts,Anormcopy,rcond,work,work2,info);
    delete [] work2;
>
> elif <F>==PDBandFact
    long kd = factorization.halfBandwidth();
    long ldab = kd+1;
>  if <R/C>==Real
    long *work2 = new long [n];
>  else
    <P> *work2 = new <P> [n];
>  endif
    <lintype>con('U',n,kd,(<T>*)factorization.data(),ldab,Anormcopy,rcond,work,work2,info);
    delete [] work2;
>
> elif <F>==TriDiagFact
>   if <R/C>==Real
      long *iwork = new long [n];
      <lintype>con('1',n,(<T>*)dl.data(),(<T>*)d.data(),(<T>*)du.data(),(<T>*)d2.data(),pvts,Anormcopy,rcond,work,iwork,info);
      delete iwork;
>   else
      <lintype>con('1',n,(<T>*)dl.data(),(<T>*)d.data(),(<T>*)du.data(),(<T>*)d2.data(),pvts,Anormcopy,rcond,work,info);
>   endif
> 
> elif <F>==PDTriDiagFact
>  if <R/C>==Complex
      <P> *work2 = new <P> [n];
      <lintype>con(n,(<P>*)d.data(),(<T>*)e.data(),Anormcopy,rcond,work2,info);
      delete work2;
>  else
      <lintype>con(n,(<T>*)d.data(),(<T>*)e.data(),Anormcopy,rcond,work,info);
>  endif
>
> else
> err No condition() routine for <F> yet
> endif
    delete [] work;
    RWPOSTCONDITION(info==0);
    return rcond;
}

<T>Vec <T><F>::solve( const <T>Vec& b ) const
{
  <T>GenMat B(b,b.length(),1);
  <T>GenMat X = solve(B);
  RWPOSTCONDITION(X.cols()==1);
  return X.col(0);
}

<T>GenMat <T><F>::solve( const <T>GenMat& B ) const
{
    <T>GenMat X(B.copy());
    long n = rows();
    if (B.rows()!=n) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,B.rows(),n)));
    if (fail()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTSOLVE)));
    long nrhs = X.cols();
    long info;
    if (n>0) {
>
> # Call the appropriate solve routine
>
> if <F>==GenFact
    <lintype>trs('N',n,nrhs,(<T>*)factorization.data(),n,pvts,X.data(),n,info);
> elif <F>==SymFact || <F>==HermFact
    <lintype>trs('U',n,nrhs,(<T>*)factorization.data(),pvts,X.data(),n,info);
> elif <F>==PDFact
    <lintype>trs('U',n,nrhs,(<T>*)factorization.data(),X.data(),n,info);
> elif <F>==BandFact
    long b = factorization.bandwidth();
    long l = factorization.lowerBandwidth();
    long u = factorization.upperBandwidth()-factorization.lowerBandwidth();
    <lintype>trs('N',n,l,u,nrhs,(<T>*)factorization.data(),b,pvts,X.data(),n,info);
> elif <F>==PDBandFact
    long kd = factorization.halfBandwidth();
    long ldab = kd+1;
    <lintype>trs('U',n,kd,nrhs,(<T>*)factorization.data(),ldab,X.data(),n,info);
> elif <F>==TriDiagFact
    <lintype>trs('N',n,nrhs,(<T>*)dl.data(),(<T>*)d.data(),(<T>*)du.data(),(<T>*)d2.data(),pvts,X.data(),n,info);
> elif <F>==PDTriDiagFact
>  if <R/C>==Complex
    <lintype>trs('U',n,nrhs,(<P>*)d.data(),(<T>*)e.data(),X.data(),n,info);  
>  else
    <lintype>trs(n,nrhs,(<T>*)d.data(),(<T>*)e.data(),X.data(),n,info);  
>  endif
> else
> err no solve routine for factorization <F>
> endif
      RWPOSTCONDITION(info==0);
    }
    return X;    
}

<T> <T><F>::determinant() const
{
> if <F>==GenFact || <F>==BandFact || <F>==TriDiagFact
    int numExchanges = 0;
    for(int i=rows(); i--;) {
      if (pvts[i]!=i+1) numExchanges++;
    }
>  if <F>==TriDiagFact
    <T> detLU = prod(d);
>  else
    <T> detLU = prod(factorization.diagonal());
>  endif
    return (numExchanges%2) ? -detLU : detLU;
>
> elif <F>==SymFact || <F>==HermFact
    // see linpack manual pg 5.16 for description of algorithm
    <T> det = 1.0;
    for(int i=rows(); i--;) {
      RWPRECONDITION(pvts[i]!=0);
      if (pvts[i]>0) {
        det *= factorization.val(i,i);  // one by one block
      } else {                          // two by two block
        RWPRECONDITION(i>0 && pvts[i-1]==pvts[i]);
        <T> a = factorization.val(i,i);  // .val stops sun warning
        <T> b = factorization.val(i-1,i);
        <T> c = factorization.val(i-1,i-1);
>   if <F>==HermFact
        <T> blockdet = a*c-<T>(norm(b),0); // see linpack manual for a better way that avoids overflow and (as much) roundoff
>   else
        <T> blockdet = a*c-b*b;     // see linpack manual for a better way that avoids overflow and (as much) roundoff
>   endif
        det *= blockdet;
        i--;
      }
    }
    return det;
>
> elif <F>==PDTriDiagFact
    return prod(d);
> elif <F>==PDFact || <F>==PDBandFact
    <T> rootdet = 1.0;
    for(int i=rows(); i--;) { rootdet *= factorization.val(i,i); }  // using val() rather than just op()() shuts up a SUN warning
    return rootdet*rootdet;
>
> else
> err no determinant routine for <F>
> endif
}
              
> # Inverse function does not exist for banded matrices
>
> if <F>==GenFact || <F>==SymFact || <F>==HermFact || <F>==PDFact
>
>
<T><M> <T><F>::inverse( ) const
{
    if (rows()==0) { return factorization; }
    if (fail()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTSOLVE)));
    <T><M> soln(factorization);
    soln.deepenShallowCopy();
    long n = rows();
    long info;
> if <F>==GenFact
    long nb = ilaenv( 1, "<lintype>tri", " ", n, -1, -1, -1 );
    long lwork = n*nb;
    <T> *work = new <T> [lwork];
    <lintype>tri(n,soln.data(),n,pvts,work,lwork,info);
    RWPOSTCONDITION(work[0]==lwork);
    delete [] work;
> elif <F>==SymFact || <F>==HermFact
    <T> *work = new <T> [n];  
    <lintype>tri('U',n,soln.data(),pvts,work,info);
    delete [] work;
> elif <F>==PDFact
    <lintype>tri('U',n,soln.data(),info);
> else
> err No such factorization <F>
> endif
    RWPOSTCONDITION(info==0);
    return soln;
}

> endif

/*
 * Global functions which provide alternate ways to call member functions.
 * These used to be inline, but this requires that you include more header
 * files on some compilers, and that is just not worth it.
 */
<T>Vec solve(const <T><F>& A, const <T>Vec& b) { return A.solve(b); }
<T>GenMat solve(const <T><F>& A, const <T>GenMat& b) { return A.solve(b); }
<T>    determinant (const <T><F>& A)	      { return A.determinant(); }

> if <F>==GenFact || <F>==SymFact || <F>==HermFact || <F>==PDFact
<T><M> inverse(const <T><F>& A)      	      { return A.inverse(); }
> endif

<P> condition(const <T><F>& A) { return A.condition(); }



/*
 * Definitions for LAPACK.h++ error messages
 *
 * $Id: lapkerr.cpp,v 1.2 1993/10/15 05:56:10 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 * Limited license.
 *
 ***************************************************************************
 *
 * $Log: lapkerr.cpp,v $
// Revision 1.2  1993/10/15  05:56:10  alv
// fixed incomplete last line
//
// Revision 1.1  1993/10/15  05:53:19  alv
// Initial revision
//
 */

#include "rw/lapkerr.h"
#include "rw/generic.h"

#undef  DECLARE_MSG
#define DECLARE_MSG(NAME, VALUE, MSG) \
        const RWCatMsg name2(RWLAPKMSG_,NAME) = {RWLAPK, VALUE, MSG};    \
        const RWMsgId  name2(RWLAPK_,NAME)    = &name2(RWLAPKMSG_,NAME);

const char RWLAPK[] = "rwlapk";

DECLARE_MSG(BALANCE,               0x3200, "[LAPK_BALANCE] Matrix to be balanced has wrong number of rows (%u!=%u)");
DECLARE_MSG(BANDSIZE,              0x3201, "[LAPK_BANDSIZE] Bandwidths don't match: lower: %u & %u, upper: %u & %u");
DECLARE_MSG(CANTFREE,              0x3202, "[LAPK_CANTFREE] Can't free index %u");
DECLARE_MSG(CANTFREEZE,            0x3203, "[LAPK_CANTFREEZE] Can't freeze index %u");
DECLARE_MSG(CANTSOLVE,             0x3204, "[LAPK_CANTSOLVE] Can't solve system of equations");
DECLARE_MSG(CANTSOLVELS,           0x3205, "[LAPK_CANTSOLVELS] Can't solve least squares system");
DECLARE_MSG(CHANGEDCOMPLEXCONSTANT,0x3206, "[LAPK_CHANGEDCOMPLEXCONSTANT] Complex constant reference was changed from %g+i%g to %g+i%g");
DECLARE_MSG(CHANGEDCONSTANT,       0x3207, "[LAPK_CHANGEDCONSTANT] Constant reference was changed from %g to %g");
DECLARE_MSG(CONDITION,             0x3208, "[LAPK_CONDITION] Condition number not computed");
DECLARE_MSG(DIAGOUTOFBOUNDS,       0x3209, "[LAPK_DIAGOUTOFBOUNDS] Diagonal %d not in %s matrix of size (%ux%u)");
DECLARE_MSG(LEADINGSUBMATRIXORDER, 0x320a, "[LAPK_LEADINGSUBMATRIXORDER] cant build order %u leading submatrix of order %u matrix");
DECLARE_MSG(MATMATPROD,            0x320b, "[LAPK_MATMATPROD] inner product not possible %s matrices (%u,%u) and (%ux%u)");
DECLARE_MSG(MATSIZE,               0x320c, "[LAPK_MATSIZE] Matrix sizes don't match (%dx%d) vs (%dx%d)");
DECLARE_MSG(MATVECPROD,            0x320d, "[LAPK_MATVECPROD] inner product not possible between %s (%ux%u) and vector (%u)");
DECLARE_MSG(NOEIG,                 0x320e, "[LAPK_NOEIG] %u th eigenvalue not computed");
DECLARE_MSG(NOSV,                  0x320f, "[LAPK_NOSV] %u th singular value not computed");
DECLARE_MSG(NOTHERM,               0x3210, "[LAPK_NOTHERM] matrix not hermetian");
DECLARE_MSG(NOTSETABLE,            0x3211, "[LAPK_NOTSETABLE] Can't change element (%d,%d) of %s");
DECLARE_MSG(NOTSQUARE,             0x3212, "[LAPK_NOTSQUARE] %dx%d matrix not square");
DECLARE_MSG(NOTSYM,                0x3213, "[LAPK_NOTSYM] matrix not symmetric");
DECLARE_MSG(NUMBERPOINTS,          0x3214, "[LAPK_NUMBERPOINTS] Wrong number of points (%u) for %ux%u %s");
DECLARE_MSG(NUMBERPOINTSBAND,      0x3215, "[LAPK_NUMBERPOINTSBAND] Wrong number of points (%u) for %ux%u %s with bandwidth %u");
DECLARE_MSG(OFFDIAG,               0x3216, "[LAPK_OFFDIAG] Off diagonal length of tridiagonal decomposition wrong (%u!=%u)")
DECLARE_MSG(OUTOFBOUNDS,           0x3217, "[LAPK_OUTOFBOUNDS] Element (%dx%d) not in matrix of size (%ux%u)");
DECLARE_MSG(QNOTCOMPUTED,          0x3218, "[LAPK_QNOTCOMPUTED] Q not computed, can't do transform");
DECLARE_MSG(RESTORESIZE,           0x3219, "[LAPK_RESTORESIZE] Restoring %s, data vector len %d, expected %u");
DECLARE_MSG(RSINGULAR,             0x321a, "[LAPK_RSINGULAR] Can't invert R in QR decomposition");
DECLARE_MSG(SCHURREORDER,          0x321b, "[LAPK_SCHURREORDER] Can't reorder eigenvalues in incomplete Schur decomposition");
DECLARE_MSG(VECLENGTH,             0x321c, "[LAPK_VECLENGTH] vector length wrong (%u!=%u)");
DECLARE_MSG(VECMATPROD,            0x321d, "[LAPK_VECMATPROD] inner product not possible vector(%u) and %s (%ux%u)");
DECLARE_MSG(WRONGNUMPOINTS,        0x321e, "[LAPK_WRONGNUMPOINTS] wrong number of elements (%u) for %s (%ux%u)");

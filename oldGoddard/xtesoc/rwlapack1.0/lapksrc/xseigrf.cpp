> # $Header: /users/rcs/lapksrc/xseigrf.cpp,v 1.3 1993/10/11 21:48:22 alv Exp $
> #                                                               #
> # $Log: xseigrf.cpp,v $
> # Revision 1.3  1993/10/11  21:48:22  alv
> # synch'ed with Math.h++ v5.1
> #
> # Revision 1.2  1993/04/06  20:45:04  alv
> # ported to sun C++ v3.0.1
> #
> # Revision 1.1  1993/04/06  15:44:42  alv
> # Initial revision
> #
>
> define <T>=RW_T_RW
> 
> if <T>==Double
> define <P>=Double <tLetter>=d <Sym>=Sym <sym>=sym <sLetter>=s <sterf>=dsterf
> elif <T>==Float
> define <P>=Float <tLetter>=f <Sym>=Sym <sym>=sym <sLetter>=s <sterf>=ssterf
> elif <T>==DComplex
> define <P>=Double <tLetter>=c <Sym>=Herm <sym>=herm <sLetter>=h <sterf>=dsterf
> else
> err Symmetric Eigen-decomposition: not defined for '<T>'
> endif
>
/*
 * Implementation of <T><Sym>RFQREigServer
 *
 * Generated from template $Id: xseigrf.cpp,v 1.3 1993/10/11 21:48:22 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * Uses Lapack's root free QR algorithm to compute eigenvalues of
 * a symmetric matrix.
 */ 

#include "rw/<tLetter><sLetter>eigsrv.h"
#include "rw/<tLetter>td.h"
#include "rw/lapack.h"


<T><Sym>RFQREigServer::<T><Sym>RFQREigServer()
{
}

<P>SymEigDecomp <T><Sym>RFQREigServer::decompose(const <T><Sym>TriDiagDecomp& decomp)
{
  long n = decomp.rows();
  <P>Vec d = decomp.diagonal().copy();
  <P>Vec e(decomp.rows(),rwUninitialized);   // lapack needs a length n (not n-1) off diagonal
  if (n>0) e(RWSlice(0,decomp.rows()-1)) = decomp.offDiagonal();
  long info;
  <sterf>(n,d.data(),e.data(),info);
  RWPOSTCONDITION(info>=0);

  <P>SymEigDecomp eig;
  <T><Sym>EigServer::n(eig) = decomp.rows();
  lambda(eig).reference(d);
  if (info>0) {
    lambda(eig).resize((unsigned)info);  // explicit cast to avoid warnings on SUN
    computedAll(eig) = FALSE;
  }
  return eig;
}

RWBoolean <T><Sym>RFQREigServer::computeEigenVectors() const {return FALSE;}


  


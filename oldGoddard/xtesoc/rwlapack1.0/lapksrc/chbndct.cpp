#if 0
/*
 * Functions for conversion between matrix types with the same shape.
 *
 * Generated from template $Id: xct.cpp,v 1.2 1993/10/11 21:48:07 alv Exp $
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */
#endif

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/lapkerr.h"
#include "rw/rwerr.h"
#include "rw/chbndmat.h"
#include "rw/dsbndmat.h"

DComplexHermBandMat::DComplexHermBandMat(const DoubleSymBandMat& re) : vec(re.dataVec())
{
  n = re.cols();
  bandu = re.halfBandwidth();
}

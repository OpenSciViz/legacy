#if 0
/*
 * Functions for conversion between matrix types with the same shape.
 *
 * Generated from template $Id: xct.cpp,v 1.2 1993/10/11 21:48:07 alv Exp $
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */
#endif

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/lapkerr.h"
#include "rw/rwerr.h"
#include "rw/cbandmat.h"
#include "rw/dbandmat.h"

DComplexBandMat::DComplexBandMat(const DoubleBandMat& re)
  : vec(re.dataVec())
{
    n=re.cols();
    band=re.bandwidth();
    bandu=re.upperBandwidth();
}

DComplexBandMat::DComplexBandMat(const DoubleBandMat& re, const DoubleBandMat& im)
  : vec(re.dataVec(),im.dataVec())
{
    // The number of points in re and im must agree at this point
    // or else the constructor for vec above would have failed.

    n=re.cols();
    band=re.bandwidth();
    bandu=re.upperBandwidth();
    if (bandwidth()!=im.bandwidth() || bandu!=im.upperBandwidth()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_BANDSIZE,upperBandwidth(),lowerBandwidth(),im.upperBandwidth(),im.lowerBandwidth())));
    }
}

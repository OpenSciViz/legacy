/*
 * Implementation of DoubleSymEigServer
 *
 * Generated from template $Id: xseigsrv.cpp,v 1.2 1993/07/05 19:19:33 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */ 

#include "rw/dseigsrv.h"
#include "rw/dtd.h"

DoubleSymEigDecomp DoubleSymEigServer::operator()(const DoubleSymMat& A)
{
  DoubleSymDenseTriDiagDecomp td(A);         // Build a tri-diagonal decomposition
  DoubleSymEigDecomp tdeig = decompose(td);  // Get eigenvals/vecs for tri-diagonal problem
  DoubleSymEigDecomp eig = tdeig;            // Needs to be done this way for complex numbers
  P(eig) = td.transform(P(tdeig));
  return eig;
}

DoubleSymEigDecomp DoubleSymEigServer::operator()(const DoubleSymBandMat& A)
{
  DoubleSymBandTriDiagDecomp td(A);          // Build a tri-diagonal decomposition
  DoubleSymEigDecomp tdeig = decompose(td);  // Get eigenvals/vecs for tri-diagonal problem
  DoubleSymEigDecomp eig = tdeig;            // Needs to be done this way for complex numbers
  P(eig) = td.transform(P(tdeig));
  return eig;
}

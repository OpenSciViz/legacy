> ###############################################################
> # The master template for the matrix types                    #
> #                                                             #
> # $Header: /users/rcs/lapksrc/xmat.h,v 1.4 1993/10/11 21:48:17 alv Exp $
> #                                                             #
> # All the matrix header files are obtained using this         #
> # template file as fodder for mathpp.                         #
> #                                                             #
> # Copyright (1991,1992,1993) by Rogue Wave Software, Inc.     #
> # This software                                               #
> # is subject to copyright protection under the laws of the    #
> # United States and other countries.                          #
> #                                                             #
> # Written by Al Vermeulen.                                    #
> #                                                             #
> # Limited License.                                            #
> ###############################################################
> #
> # Moved from Matrix.h++ into Lapack.h++
> #
> # $Log: xmat.h,v $
> # Revision 1.4  1993/10/11  21:48:17  alv
> # synch'ed with Math.h++ v5.1
> #
> # Revision 1.3  1993/07/08  23:26:13  alv
> # ported to SUN CC v3
> #
> # Revision 1.2  1993/07/05  19:19:33  alv
> # fixed up for complex numbers
> #
> # Revision 1.1  1993/06/23  18:28:09  alv
> # Initial revision
> #
> # Revision 1.2  1993/01/26  21:05:37  alv
> # removed DOS EOF
> #
> # Revision 1.1  1993/01/23  00:01:12  alv
> # Initial revision
> #
> # 
> #    Rev 1.1   17 Oct 1991 09:22:32   keffer
> # Changed include path to <rw/xxx.h>
> # 
>
>
> # Set up the <T>,<M>,... macros
>
> include macros
>
>
> beginwrapper <tLetter><shapeAbbrev>mat
>
/*
>
> if <M>==GenMat
>
 * <T>GenMat - A general rectangular matrix of <T>s
 *
 * The matrix is stored in column major order.  This
 * is what Fortran likes, but not what C likes.
>
> elif <M>==SymMat
>
 * <T>SymMat - A symmetric matrix of <T>s
 *
 * Stores a symmetric matrix.  Only half the matrix is actually
 * stored.  This means if you change, say, entry (2,3) you are also
 * implicitly changing entry (3,2).
 *
 * The lower left triangle of the matrix is stored in column major
 * order.
>
> elif <M>==SkewMat
>
 * <T>SkewMat - An skew symmetric matrix of <T>s
 *
 * Stores a skew symmetric matrix.  Note that only half the matrix is actually
 * stored.  This means if you change, say, entry (2,3) you are also
 * implicitly changing entry (3,2).
 *
 * The lower left triangle of the matrix is stored in column major
 * order.
>
> elif <M>==HermMat
>
 * <T>HermMat - A hermetian matrix of <T>s
 *
 * Stores an Hermitian matrix.  Note that only the lower left 
 * half of the matrix is actually stored.
 *
 * The lower left triangle of the matrix is stored in column major
 * order.
>
> elif <M>==BandMat
>
 * <T>BandMat - A banded matrix of <T>s
 *
 * Stores a banded matrix.  All entries farther above the diagonal
 * that the upper bandwidth, or farther below than the lower bandwidth
 * are defined to be zero.  The total bandwidth is the sum of the 
 * upper and lower bandwidths, plus one for the diagonal.  Only
 * the elements which may be non-zero are stored.
 *
 * The matrix is stored column by column.  Each column takes up 
 * <bandwidth> entries, some of these entries near the corners
 * are not used.
>
> elif <M>==SymBandMat
>
 * <T>SymBandMat - A symmetric banded matrix of <T>s
 *
 * Stores a symmetric banded matrix.  All entries farther above or below    
 * the diagonal that the upper bandwidth
 * are defined to be zero.  The total bandwidth is twice the 
 * upperBandwidth, plus one for the diagonal.  Only
 * the elements which may be non-zero are stored.
 *
 * The top half of the matrix is stored column by column.  Each
 * column takes up 
 * <upperBandwidth+1> entries, some of these entries near the top left
 * corner are not used.
> 
> elif <M>==HermBandMat
>
 * <T>HermBandMat - An Hermitian banded matrix of <T>s
 *
 * Stores an Hermitian banded matrix.  All entries farther above or below    
 * the diagonal than the upper bandwidth
 * are defined to be zero.  The total bandwidth is twice the 
 * upperBandwidth, plus one for the diagonal.  Only
 * the elements which may be non-zero are stored.  
 *
 * The top half of the matrix is stored column by column.  Each
 * column takes up 
 * <upperBandwidth+1> entries, some of these entries near the top
 * left corner are not used.
> 
> elif <M>==UpperTriMat
>
 * <T>TriMat - An upper triangular matrix of <T>s
 *
 * Stores an upper triangular matrix.
 *
 * The matrix is stored in column major order.
>
> elif <M>==LowerTriMat
>
 * <T>TriMat - A lower triangular matrix of <T>s
 *
 * Stores a lower triangular matrix.
 *
 * The matrix is stored in row major order.  This way the vector
 * of data is exactly the same as the matrix's transpose (an UpperTriMat)
>
> elif <M>==TriDiagMat
>
 * <T>TriDiagMat - A tridiagonal matrix of <T>s
 *
 * Stores a tridiagonal matrix.
 *
 * The matrix is stored in the same format as a banded matrix with
 * upper and lower bandwidth one.
 * .fi
>
> else
>
> err  unknown matrix shape '<M>'
>
> endif
>
>
> # Now the end of the comments --- this is the same in all 
> # headers.
>
 *
 * Generated from template $Id: xmat.h,v 1.4 1993/10/11 21:48:17 alv Exp $
 *
 * Bounds checking is enabled by setting the preprocessor
 * symbol RWBOUNDS_CHECK.  This is strongly encouraged.
 *
> copyright
 */

> # Now, declare anything we need to know about to process this file
>
#include "rw/<tLetter>vec.h"
>
> if <M>!=GenMat && <M>!=SymMat
#include "rw/<tLetter>ref.h"
> endif

> if <M>==BandMat || <M>==SymBandMat || <M>==HermBandMat || <M>==UpperTriMat || <M>==LowerTriMat || <M>==TriDiagMat
extern <T> rw<T>Zero;	// This constant is sometimes returned by reference
> endif

> # Declare classes that are used in the class declaration.
> # First declare the pick class.
> # Next declare all other classes with the same shape, then
> # classes of different shapes to which we can convert.
>
> if <M>==GenMat
class <T><M>Pick;	// Definition in <tLetter><shapeAbbrev>pik.h
> endif
>
> if <T>==Int
class Float<M>;
class Double<M>;
class UChar<M>;
class SChar<M>;
> elif <T>==UChar || <T>==SChar
class Int<M>;
> elif <T>==Float
class Double<M>;
>  if <M>==GenMat
class Int<M>;
>  endif
> elif <T>==Double
class Float<M>;
>  if <M>==GenMat
class Int<M>;
>  endif
> elif <T>==DComplex
>  if <M>==HermMat
class <P>SymMat;
class <P>SkewMat;
>  elif <M>==HermBandMat
class <P>SymBandMat;
>  else
class <P><M>;
>  endif
> else
> err No such type <T>
> endif
>
> if <M>==GenMat && ( <T>==Int || <T>==UChar || <T>==SChar )
> elif <M>==GenMat
class <T>SymMat;
class <T>SkewMat;
class <T>BandMat;
class <T>SymBandMat;
class <T>UpperTriMat;
class <T>LowerTriMat;
class <T>TriDiagMat;
>  if <R/C>==Complex
class <T>HermMat;
class <T>HermBandMat;
>  endif
> elif <M>==SymMat
class <T>GenMat;
> elif <M>==SkewMat
class <T>GenMat;
>  if <R/C>==Complex
class <P>SymMat;
>  else
class <T>SymMat;
>  endif
> elif <M>==HermMat
class <T>GenMat;
> elif <M>==BandMat
class <T>GenMat;
class <T>TriDiagMat;
class <T>SymBandMat;
>  if <R/C>==Complex
class <T>HermBandMat;
>  endif
> elif <M>==SymBandMat
class <T>GenMat;
class <T>BandMat;
> elif <M>==HermBandMat
class <T>GenMat;
class <T>BandMat;
class <P>SymBandMat;
> elif <M>==UpperTriMat
class <T>GenMat;
class <T>LowerTriMat;
> elif <M>==LowerTriMat
class <T>GenMat;
class <T>UpperTriMat;
> elif <M>==TriDiagMat
class <T>GenMat;
class <T>BandMat;
> else
> err No such shape <M>
> endif

>
> # The class itself
>
class <T><M> {
private:

<T>Vec vec;
>
> # The numbers which define the shape of the matrix.  These
> # depend on the type of matrix.
>
> if <M>==GenMat
unsigned n,m;
> else
unsigned n;
> endif
>
> if <M>==BandMat
unsigned band, bandu;	// bandwidth and upper bandwidth
> endif
> if <M>==SymBandMat || <M>==HermBandMat
unsigned bandu;		// The upper bandwidth
> endif
  // The data which define the matrix

> if <M>==TriDiagMat || <M>==BandMat || <M>==SymBandMat || <M>==HermBandMat
  void zeroUnusedEntries();  // Initialize entries in the data vector which are not used to zero
> endif

public:
<T><M>();
<T><M>( const <T><M>& );
> if <M>==GenMat
<T><M>( const <T><M>Pick& );
> endif
>
>
> # The constructors which explicitly set the shape of the matrix
> # Different versions depending on what integers are needed to set
> # the shape.
>
> if <M>==GenMat
<T><M>(unsigned m, unsigned n);
<T><M>(unsigned m, unsigned n, <T> initval);
<T><M>(const <T>Vec& data, unsigned m, unsigned n);
<T><M>(const <T>* v, unsigned m, unsigned n);	// Copy of v will be made
> elif <M>==BandMat
<T><M>(unsigned n, unsigned nAgain, unsigned lowerWidth, unsigned upperWidth);
<T><M>(const <T>Vec& data, unsigned n, unsigned nAgain, unsigned lowerWidth, unsigned upperWdth);
> elif <M>==SymBandMat || <M>==HermBandMat
<T><M>(unsigned n, unsigned nAgain, unsigned halfWidth);
<T><M>(const <T>Vec& data, unsigned n, unsigned nAgain, unsigned halfWidth);
> elif <M>==SymMat
<T><M>(unsigned n, unsigned nAgain);
<T><M>(unsigned n, unsigned nAgain, <T> initval);
<T><M>(const <T>Vec& data, unsigned n, unsigned nAgain);
> elif <M>==SkewMat || <M>==HermMat || <M>==UpperTriMat || <M>==LowerTriMat || <M>==TriDiagMat
<T><M>(unsigned n, unsigned nAgain);
<T><M>(const <T>Vec& data, unsigned n, unsigned nAgain);
> else
> err No simple constructors for matrix shape <M>
> endif
>
>
> # Constructors used only for complex types
> # HermBandMat only gets the first one, since
> # there is no SkewBandMat class.
>
> if <R/C>==Complex
>   if <M>==HermMat
<T><M>(const <P>SymMat& re);
<T><M>(const <P>SymMat& re, const <P>SkewMat& im);
>   elif <M>==HermBandMat
<T><M>(const <P>SymBandMat& re);
>   else
<T><M>(const <P><M>& re);
<T><M>(const <P><M>& re, const <P><M>& im);
>   endif
> endif
>
> 
> # Conversion constructors
> # Float --> Double, Int --> Float, Int --> Double, [US]Char --> Int
> # Conversions the other way are done by toXXX functions
>
> if <T>==Double
<T><M>(const Float<M>&);
>   if <M>==GenMat
<T><M>(const Int<M>&);
>   endif
> endif
>
> if <T>==Float
>   if <M>==GenMat
<T><M>(const Int<M>&);
>   endif
> endif
>
> if <T>==Int
<T><M>(const UChar<M>&);
<T><M>(const SChar<M>&);
> endif
>
>
> # Conversion between matrix types except to general matrices
> 
> if <M>==BandMat
<T><M>( const <T>TriDiagMat& A );
<T><M>( const <T>SymBandMat& A );
> if <R/C>==Complex
<T><M>( const <T>HermBandMat& A );
> endif
> endif
>
>
> # Conversion constructors for general matrices
>
> if <M>==GenMat && <T>!=Int && <T>!=SChar && <T>!=UChar
<T><M>(const <T>SymMat& A);
<T><M>(const <T>SkewMat& A);
>    if <R/C>==Complex
<T><M>(const <T>HermMat& A);
<T><M>(const <T>HermBandMat& A);
>    endif
<T><M>(const <T>BandMat& A);
<T><M>(const <T>SymBandMat& A);
<T><M>(const <T>UpperTriMat& A);
<T><M>(const <T>LowerTriMat& A);
<T><M>(const <T>TriDiagMat& A);
> endif
>
>
> # And, of course, the destructor
~<T><M>();
  // Constructors, conversion functions, and a destructor.

static const char *className;
  // For use in error messages

> # Access the shape of the matrix.  These are all defined inline.
>
inline unsigned rows() const	   { return n; }
inline unsigned cols() const	   { return n; }
>
> if <M>==BandMat
inline unsigned bandwidth() const      { return band; }
inline unsigned lowerBandwidth() const { return band-bandu-1; }
inline unsigned upperBandwidth() const { return bandu; }
> endif
> if <M>==SymBandMat || <M>==HermBandMat
inline unsigned bandwidth() const      { return 2*bandu+1; }
inline unsigned halfBandwidth() const  { return bandu; }
inline unsigned lowerBandwidth() const { return bandu; }
inline unsigned upperBandwidth() const { return bandu; }
> endif
>
> if <M>==TriDiagMat
inline unsigned bandwidth() const      { return 3; }
inline unsigned lowerBandwidth() const { return 1; }
inline unsigned upperBandwidth() const { return 1; }
inline unsigned halfBandwidth() const  { return 1; }
> endif
>
  // Member functions to access the shape of the matrix

>
> # The member functions to access and change elements of the matrix
>
inline <T>  val(int i, int j) const;
       <T>  bcval(int i, int j) const;
inline <T>  set(int i, int j, <T> x);
       <T>  bcset(int i, int j, <T> x);
>
> # Pick the correct definition for ref()
>
> if <M>==SymMat
inline <T>& operator()(int i, int j);
inline <T>& ref(int i, int j);
       <T>& bcref(int i, int j);
> elif <M>==BandMat || <M>==UpperTriMat || <M>==LowerTriMat || <M>==TriDiagMat || <M>==SymBandMat
inline RO<T>Ref operator()(int i, int j);
inline RO<T>Ref ref(int i, int j);
       RO<T>Ref bcref(int i, int j);
> elif <M>==SkewMat
inline NG<T>Ref operator()(int i, int j);
inline NG<T>Ref ref(int i, int j);
       NG<T>Ref bcref(int i, int j);
> elif <M>==HermMat
inline CJ<T>Ref operator()(int i, int j);
inline CJ<T>Ref ref(int i, int j);
       CJ<T>Ref bcref(int i, int j);
> elif <M>==HermBandMat
inline ROCJ<T>Ref operator()(int i, int j);
inline ROCJ<T>Ref ref(int i, int j);
       ROCJ<T>Ref bcref(int i, int j);
> else
> err No ref defined for type <M>
> endif
>
>
inline <T>  operator()(int i, int j) const; // { return val(i,j); }
  // Access and/or change elements of the matrix.
  // The versions that begin with the letters bc do bounds
  // checking.  The others do bounds checking only if the
  // preprocessor symbol RWBOUNDS_CHECK is defined.

>
<T><M> leadingSubmatrix(int order);
  // Access to submatrices.
  // The leading submatrix of order k is the kxk matrix in the upper
  // left.
>
> if <M>==BandMat || <M>==TriDiagMat || <M>==SymBandMat
inline <T>Vec diagonal(int =0) const;
       <T>Vec bcdiagonal(int =0) const;
> endif
  // Access to diagonals.  The same conventions as for
  // element access apply (with respect to bcxxx and xxx names).

>
> # Assignment type stuff
>
<T><M>& operator=(const <T><M>& m);
<T><M>& reference(<T><M>& m);
  // reference() makes an alias, operator= makes a copy.

> if <M>==SymMat
<T><M>& operator=(<T> x)   { vec=<T>(x); return *this; }
  // Sets all elements of the matrix equal to x
> endif

> if <R/C>==Complex
void zero()        { vec=<T>(0,0); }
> else
void zero()        { vec=<T>(0); }
> endif
  // Sets all elements of the matrix equal to zero.


<T><M> copy() const;
<T><M> deepCopy() const		{ return copy(); }
void deepenShallowCopy()        { vec.deepenShallowCopy(); }
  // copy() returns a <T><M> with its own copy of the data.
  // deepenShallowCopy makes sure the data is not shared by another
  // vector or matrix.

>
> # Assorted member functions
>
> # I tried changing dataVec from having two prototypes to having only
> # the following prototype:
> # <T>Vec& dataVec() const
> # but then borland C++ complained about not allowing inline functions
> # which contain temporaries with destructors in the mean function.
>
<T>Vec dataVec() 		{ return vec; }
const <T>Vec& dataVec() const	{ return vec; }
<T>* data() 			{ return vec.data(); }
const <T>* data() const 	{ return vec.data(); }
  // Access to the raw data.

void resize(unsigned m, unsigned n);
> if <M>==BandMat
void resize(unsigned n, unsigned nAgain, unsigned lowWidth, unsigned upWidth);
> endif
> if <M>==SymBandMat || <M>==HermBandMat
void resize(unsigned n, unsigned nAgain, unsigned halfWidth);
> endif
  // Resize the matrix.  New elements are set to zero.

>
>
> if <M>==SymMat
>  if <T>==Int || <T>==UChar || <T>==SChar
>  # No apply functions for integer types, although  
>  # there really should be.
>  elif <R/C>==Real
<T><M> apply(mathFunTy) const;
>  elif <R/C>==Complex
<T><M> apply(CmathFunTy) const;
<P><M> apply2(CmathFunTy2) const;
>  else
>  err <R/C> is not either Real or Complex
>  endif
  // Each element in the matrix returned by apply is
  // the result of calling f() on the corresponding element
  // in this matrix.  
> endif
>
> if <M>==HermMat || <M>==HermBandMat
void makeDiagonalReal();            // Set imaginary part of diagonal to zero
> endif

void scanFrom(istream&);      		// Human readable
void printOn(ostream&) const;
void restoreFrom(RWvistream&);     	// Internal ASCII or binary code
void saveOn(RWvostream&) const;
void restoreFrom(RWFile&);
void saveOn(RWFile&) const;
unsigned binaryStoreSize() const;
  // Input/output member functions
  // Whether ASCII or binary formatting is used with readFrom and
  // storeOn is determined by the type of rwstream passed.

/*
 * The operator()() member functions are declared with ref(),
 * since the declarations look the same.
 *
 * The operator=() assignment operators are declared with reference(),
 * since the functions are so closely related.
 */

RWBoolean operator==(const <T><M>& X);
RWBoolean operator!=(const <T><M>& X)  { return !(operator==(X)); }
  // Two matrices are considered equal if their shapes are identical and
  // all their components are identical.

>
> if ( <M>==GenMat || <M>==SymMat ) && <T>!=UChar
<T><M>& operator+=(<T>);
<T><M>& operator-=(<T>);
> endif
<T><M>& operator+=(const <T><M>& m);
> if <T>!=UChar
<T><M>& operator-=(const <T><M>& m);
<T><M>& operator*=(const <T><M>& m);
<T><M>& operator*=(<T>);
<T><M>& operator/=(const <T><M>& m);
<T><M>& operator/=(<T>);
> endif
  // assignment operators.  self must be same size as m.

>
> if <R/C>==Real && ( <M>==GenMat || <M>==SymMat )
<T><M>& operator++();	// Prefix operator
#ifndef RW_NO_POSTFIX
void operator++(int);	// Postfix operator
#endif
>    if <T>!=UChar
<T><M>& operator--();	// Prefix operator
#ifndef RW_NO_POSTFIX
void operator--(int);	// Postfix operator
#endif
>    endif
  // Increment/Decrement operators

> endif

};

<T><M> operator-(const <T><M>&);	// Unary minus
<T><M> operator+(const <T><M>&);	// Unary plus
>
<T><M> operator*(const <T><M>&, const <T><M>&);
<T><M> operator/(const <T><M>&, const <T><M>&);
<T><M> operator+(const <T><M>&, const <T><M>&);
> if <T>!=UChar
<T><M> operator-(const <T><M>&, const <T><M>&);
<T><M> operator*(const <T><M>&, <T>);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
inline <T><M> operator*(<T> x, const <T><M>& A) { return A*x; }
#else
       <T><M> operator*(<T> x, const <T><M>& A);
#endif
<T><M> operator/(const <T><M>& A, <T> x);
>   if <M>==GenMat || <M>==SymMat || <M>==SkewMat || <M>==HermMat
<T><M> operator/(<T> x, const <T><M>& A);
>   endif
>   if <M>==GenMat || <M>==SymMat
<T><M> operator+(const <T><M>&, <T>);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
inline <T><M> operator+(<T> x, const <T><M>& A) { return A+x; }
inline <T><M> operator-(const <T><M>& A, <T> x) { return A+(-x); }
#else
       <T><M> operator+(<T> x, const <T><M>& A);
       <T><M> operator-(const <T><M>& A, <T> x);
#endif
<T><M> operator-(<T> x, const <T><M>& A);
>   endif
> endif
  // Arithmetic operators; Notice that operator* is an
  // element by element multiply, NOT a matrix multiply.

inline ostream& operator<<(ostream& s, const <T><M>& m)
		{m.printOn(s); return s;}
inline istream& operator>>(istream& s, <T><M>& m)
		{m.scanFrom(s); return s;}
  // I/O for humans

> if <M>==UpperTriMat
<T>LowerTriMat transpose(const <T><M>&);
> elif <M>==LowerTriMat
<T>UpperTriMat transpose(const <T><M>&);
> else
<T><M> transpose(const <T><M>&);
>  endif
  // The transpose of the matrix. 

<T>Vec product(const <T><M>& A, const <T>Vec& x);
<T>Vec product(const <T>Vec& x, const <T><M>& A);
> if <M>==GenMat || <M>==BandMat
<T><M> product(const <T><M>& A, const <T><M>& B);
<T><M> transposeProduct(const <T><M>& A, const <T><M>& B);
> if <R/C>==Complex
<T><M> conjTransposeProduct(const <T><M>& A, const <T><M>& B);
  // conjTransposeProduct calculates conj(A^T)B
> endif
  // transposeProduct calculates A^TB
> endif
  // inner products

>
> # Conversion routines toXxxx, the routines which go the other way
> # are defined as constructors in ctors.htm.  The distinction is that
> # these may lose information, the
> # constructors never fail.
>
> if <T>==Float
<T><M> to<T>(const Double<M>& A);
> endif
> if <T>==Int
<T><M> to<T>(const Double<M>& A);
<T><M> to<T>(const Float<M>& A);
> endif
> if <T>==SChar || <T>==UChar
// <T><M> to<T>(const Int<M>& A); no Int->Char things in math.h++, so none here
> endif
>
> if <M>==GenMat
> elif <M>==SymMat
<T><M> toSymMat( const <T>GenMat& A );
<T><M> upperToSymMat( const <T>GenMat& A );
<T><M> lowerToSymMat( const <T>GenMat& A );
> elif <M>==SkewMat
<T><M> toSkewMat( const <T>GenMat& A, RWBoolean keepMainDiag = TRUE );
> elif <M>==HermMat
<T><M> toHermMat( const <T>GenMat& A );
<T><M> upperToHermMat( const <T>GenMat& A );
<T><M> lowerToHermMat( const <T>GenMat& A );
> elif <M>==BandMat
<T><M> toBandMat( const <T>GenMat& A, unsigned bandl, unsigned bandu );
> elif <M>==SymBandMat
<T><M> toSymBandMat( const <T>GenMat& A, unsigned halfband );
<T><M> toSymBandMat( const <T>BandMat& A );
> elif <M>==HermBandMat
<T><M> toHermBandMat( const <T>GenMat& A, unsigned halfband );
<T><M> toHermBandMat( const <T>BandMat& A );
> elif <M>==UpperTriMat
<T><M> toUpperTriMat( const <T>GenMat& A );
> elif <M>==LowerTriMat
<T><M> toLowerTriMat( const <T>GenMat& A );
> elif <M>==TriDiagMat
<T><M> toTriDiagMat( const <T>GenMat& A );
<T><M> toTriDiagMat( const <T>BandMat& A );
> else
> err No toXXX function section for matrix shape <M>
> endif

> # Functions that exist for all
> # matrix types
>
> if <R/C>==Real
>   if <T>==UChar
/* No abs() function for unsigned types */
>   elif <M>==SkewMat
<T>SymMat abs(const <T><M>& A);
>   else
<T><M> abs(const <T><M>& A);
>   endif
> elif <R/C>==Complex
>   if <M>==HermMat
<P>SymMat abs(const <T><M>& A);
>   elif <M>==HermBandMat
<P>SymBandMat abs(const <T><M>& A);
>   elif <M>==SkewMat
<P>SymMat abs(const <T><M>& A);
>   else
<P><M> abs(const <T><M>& A);
>   endif
> else
> err <R/C> not Real or Complex
> endif
> 
> if <R/C>==Complex
>
>  if <M>==SkewMat
<T><M> conj(const <T><M>& A);
<P><M> real(const <T><M>& A);
<P><M> imag(const <T><M>& A);
<P>SymMat norm(const <T><M>& A);
/* Sorry, no arg function for SkewMat */
>  elif <M>==HermMat
<T><M> conj(const <T><M>& A);
<P>SymMat real(const <T><M>& A);
<P>SkewMat imag(const <T><M>& A);
<P>SymMat norm(const <T><M>& A);
<P>SkewMat arg(const <T><M>& A);
>  elif <M>==HermBandMat
<T><M> conj(const <T><M>& A);
<P>SymBandMat real(const <T><M>& A);
<P>SymBandMat norm(const <T><M>& A);
 /* imag and arg not defined for HermBandMat */
>  else
<T><M> conj(const <T><M>& A);
<P><M> real(const <T><M>& A);
<P><M> imag(const <T><M>& A);
<P><M> norm(const <T><M>& A);
<P><M> arg(const <T><M>& A);
>  endif
>
> endif

> if <R/C>==Real && <M>!=SkewMat
inline <T> minValue(const <T><M>& A) { return minValue(A.dataVec()); }
inline <T> maxValue(const <T><M>& A) { return maxValue(A.dataVec()); }
> endif

>
> # The rest of the functions are only valid for the matrix types
> # for which apply is implemented.
> 
> if <M>==GenMat || <M>==SymMat
>  if <T>!=Int && <T>!=UChar && <T>!=SChar
inline <T><M> cos(const <T><M>& A)  { return A.apply(::cos); }
inline <T><M> cosh(const <T><M>& A) { return A.apply(::cosh); }
inline <T><M> exp(const <T><M>& A)  { return A.apply(::exp); } 
inline <T><M> log(const <T><M>& A)  { return A.apply(::log); }
inline <T><M> sin(const <T><M>& A)  { return A.apply(::sin); }
inline <T><M> sinh(const <T><M>& A) { return A.apply(::sinh); }
inline <T><M> sqrt(const <T><M>& A) { return A.apply(::sqrt); }
>  endif
>  if <M>==GenMat
>   if <T>!=Int && <T>!=UChar && <T>!=SChar
inline <T>    mean(const <T><M>& A) { return mean(A.dataVec()); }
inline double variance(const <T><M>& A) { return variance(A.dataVec()); }
>   else
inline <T>    prod(const <T><M>& A) { return prod(A.dataVec()); }
inline <T>    sum(const <T><M>& A)  { return sum(A.dataVec()); }
>   endif
>  endif
  // Math functions applicable to both real and complex types.

>  if <R/C>==Real && <T>!=Int && <T>!=UChar && <T>!=SChar
inline <T><M> acos(const <T><M>& A) { return A.apply(::acos); }
inline <T><M> asin(const <T><M>& A) { return A.apply(::asin); }
inline <T><M> atan(const <T><M>& A) { return A.apply(::atan); }
<T><M> atan2(const <T><M>&,const <T><M>&);
inline <T><M> ceil(const <T><M>& A) { return A.apply(::ceil); }
inline <T><M> floor(const <T><M>& A){ return A.apply(::floor); }
inline <T><M> log10(const <T><M>& A){ return A.apply(::log10); }
inline <T><M> tan(const <T><M>& A)  { return A.apply(::tan); }
inline <T><M> tanh(const <T><M>& A) { return A.apply(::tanh); }
>  endif
>
> endif

/*
 * Inline functions
 */
>
>
> #############################
> # Now the element access functions
> # 
> # Note that all of the function bodies here are repeated in the
> # bcref, bcset, ... functions.
> # 
> #############################

> # First the return value accessor.
> # The declaration and first bit of each function are common, 
> # then comes one function body for each different type of matrix.
>
inline <T> <T><M>::val(int i, int j) const {
>
> # The inline versions of these are just too tough for some compilers
> # to handle, so we'll just de-inline them.
> #
> if <M>==BandMat || <M>==SymBandMat || <M>==HermBandMat
    return bcval(i,j);
> else
>
#ifdef RWBOUNDS_CHECK
return bcval(i,j);
#else
> 
> if <M>==SymMat
    if (i>j) {int temp=i; i=j; j=temp;}
    return vec(j*(j+1)/2+i);
> elif <M>==SkewMat
    int index = (i<=j) ?  (j*(j+1)/2+i) : (i*(i+1)/2+j);
    <T> val = vec(index);
    return (i<=j) ?  val : -val;
> elif <M>==HermMat
    int index = (i<=j) ?  (j*(j+1)/2+i) : (i*(i+1)/2+j);
    <T> val = vec(index);
    return (i<=j) ?  val : conj(val);
> elif <M>==UpperTriMat
    return (j<i) ? <zero> : vec(j*(j+1)/2+i);
> elif <M>==LowerTriMat
    return (i<j) ? <zero> : vec(i*(i+1)/2+j);
> elif <M>==TriDiagMat
    return ( (i-j>1) || (i-j<(-1)) ) ? <zero> : vec(i+1+j*2);
> else
>   err No elementValue method defined for matrix type <M>
> endif
#endif
> endif
}

>
>
> # Now the set() function.
>
inline <T> <T><M>::set(int i, int j, <T> x) {
>
> # The inline versions of these are just too tough for some compilers
> # to handle, so we'll just de-inline them.
> #
> if <M>==SymMat || <M>==SkewMat || <M>==HermMat || <M>==SymBandMat || <M>==HermBandMat
    return bcset(i,j,x);
> else
>
#ifdef RWBOUNDS_CHECK
return bcset(i,j,x);
#else
>
> if <M>==BandMat
    return vec(i-j+bandu + j*band)=x;
> elif <M>==UpperTriMat
    return vec(j*(j+1)/2+i)=x; 
> elif <M>==LowerTriMat
    return vec(i*(i+1)/2+j)=x; 
> elif <M>==TriDiagMat
    return vec(i+1+j*2)=x;
> else
>   err No elementSet function defined for matrix type <M>
> endif
#endif
> endif
}

>
>
> # Here is the operator() function.  This returns an intermediate
> # reference class for many types of matrices.
>
>
> # Pick the correct definition for ref()
>
> if <M>==SymMat
inline <T>& <T><M>::ref(int i, int j) {
> elif <M>==BandMat || <M>==UpperTriMat || <M>==LowerTriMat || <M>==TriDiagMat || <M>==SymBandMat
inline RO<T>Ref <T><M>::ref(int i, int j) {
> elif <M>==SkewMat
inline NG<T>Ref <T><M>::ref(int i, int j) {
> elif <M>==HermMat
inline CJ<T>Ref <T><M>::ref(int i, int j) {
> elif <M>==HermBandMat
inline ROCJ<T>Ref <T><M>::ref(int i, int j) {
> else
> err No access method declared for ref() for type <M>
> endif
>
> # The inline versions of these are just too tough for some compilers
> # to handle, so we'll just de-inline them.
> #
> if <M>!=SymMat
    return bcref(i,j);
> else
#ifdef RWBOUNDS_CHECK
return bcref(i,j);
#else
>
>
> if <M>==SymMat
    if (i>j) {int temp=i; i=j; j=temp;}
    return vec(j*(j+1)/2+i);
> else
>   err No ref() method defined for matrix type <M>
> endif
#endif
> endif
}

> if <M>==SymMat
inline <T>& <T><M>::operator()(int i, int j) { return ref(i,j); }
> elif <M>==BandMat || <M>==UpperTriMat || <M>==LowerTriMat || <M>==TriDiagMat || <M>==SymBandMat
inline RO<T>Ref <T><M>::operator()(int i, int j) { return ref(i,j); }
> elif <M>==SkewMat
inline NG<T>Ref <T><M>::operator()(int i, int j) { return ref(i,j); }
> elif <M>==HermMat
inline CJ<T>Ref <T><M>::operator()(int i, int j) { return ref(i,j); }
> elif <M>==HermBandMat
inline ROCJ<T>Ref <T><M>::operator()(int i, int j) { return ref(i,j); }
> else
> err No op() method declared for type <M>
> endif
>
inline <T> <T><M>::operator()(int i, int j) const { return val(i,j); }

> if <M>==TriDiagMat || <M>==BandMat || <M>==SymBandMat
inline <T>Vec <T><M>::diagonal(int i) const {
#if defined(RWBOUNDS_CHECK)
    return bcdiagonal(i);
#else
    int iabs = (i>=0) ? i : -i;
>  if <M>==SymBandMat
    return vec.slice( (iabs+1)*(bandu), n-iabs, bandu+1 );
>  elif <M>==BandMat
    return vec.slice(i>0 ? upperBandwidth()+i*(bandwidth()-1)
			 : upperBandwidth()+iabs
			 , n-iabs, bandwidth() );
>  elif <M>==TriDiagMat
    return vec.slice(i>0 ? 1 + i*2 : 1 + iabs, n-iabs, 3 );
>  else
>  err Hey, man, how did I get here with shape <M>?
>  endif
#endif
}
> endif

> endwrapper

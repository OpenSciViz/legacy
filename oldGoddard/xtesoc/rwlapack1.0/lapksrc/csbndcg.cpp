/*
 * The conversion functions to/from general matrices.
 *
 * Generated from template $Id: xcg.cpp,v 1.2 1993/07/05 19:19:33 alv Exp $
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/csbndmat.h"
#include "rw/cgenmat.h"

DComplexGenMat::DComplexGenMat( const DComplexSymBandMat& A )
  : RWMatView(A.rows(),A.cols(),sizeof(DComplex))
{
  zero();
  for( int k=A.halfBandwidth(); k>=0; k-- ) {
    DComplexVec d = A.diagonal(k);
    diagonal(-k) = d;
    diagonal(k) = d;
  }
}

DComplexSymBandMat toSymBandMat( const DComplexGenMat& S, unsigned bandu )
{
  int n = S.cols();
  DComplexSymBandMat A( S.rows(), n, bandu );
  A.diagonal(0) = S.diagonal(0);
  for( int k=1; k<=bandu; k++ ) {
    DComplexVec Adiag = A.diagonal(k);
    Adiag = S.diagonal(k);
    Adiag += S.diagonal(-k);
    Adiag /= DComplex(2,0);		// Explicit constructor necessary for Zortech
  }
  return A;
}


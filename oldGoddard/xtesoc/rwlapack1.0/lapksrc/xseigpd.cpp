> # $Header: /users/rcs/lapksrc/xseigpd.cpp,v 1.4 1993/10/11 21:48:21 alv Exp $
> #                                                               #
> # $Log: xseigpd.cpp,v $
> # Revision 1.4  1993/10/11  21:48:21  alv
> # synch'ed with Math.h++ v5.1
> #
> # Revision 1.3  1993/07/09  21:12:46  alv
> # correct typo discovered by IBM's xlC
> #
> # Revision 1.2  1993/07/05  19:19:33  alv
> # fixed up for complex numbers
> #
> # Revision 1.1  1993/04/06  15:44:42  alv
> # Initial revision
> #
>
> define <T>=RW_T_RW
> 
> if <T>==Double
> define <P>=Double <tLetter>=d <Sym>=Sym <sym>=sym <sLetter>=s <pteqr>=dpteqr
> elif <T>==Float
> define <P>=Float <tLetter>=f <Sym>=Sym <sym>=sym <sLetter>=s <pteqr>=spteqr
> elif <T>==DComplex                                                      
> define <P>=Double <tLetter>=c <Sym>=Herm <sym>=herm <sLetter>=h <pteqr>=dpteqr
> else
> err Symmetric Eigen-decomposition: not defined for '<T>'
> endif
>
/*
 * Implementation of <T><Sym>PDQREigServer
 *
 * Generated from template $Id: xseigpd.cpp,v 1.4 1993/10/11 21:48:21 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * Compute eigenvalues/vectors of a positive definite tridiagonal matrix.
 */ 

#include "rw/<tLetter><sLetter>eigsrv.h"
#include "rw/<tLetter>td.h"
#include "rw/lapack.h"


<T><Sym>PDQREigServer::<T><Sym>PDQREigServer(RWBoolean vecs)
{
  computeVecs_ = vecs;
}

<P>SymEigDecomp <T><Sym>PDQREigServer::decompose(const <T><Sym>TriDiagDecomp& decomp)
{
  char compz = computeVecs_ ? 'I' : 'N';
  long n = decomp.rows();
  <P>Vec d = decomp.diagonal().copy();
  <P>Vec e = decomp.offDiagonal().copy();
  int zsize = computeVecs_ ? decomp.rows() : 0;
  <P>GenMat Z(zsize,zsize,rwUninitialized);
  long ldz = computeVecs_ ? n : 1;
  <P> *work = computeVecs_ ? new <P> [4*n+1] : 0;  // Needs only 4*n-4, but this way we avoid <0 checks
  long info;
  <pteqr>(compz,n,d.data(),e.data(),Z.data(),ldz,work,info);
  delete work;
  RWPOSTCONDITION(info>=0);

  <P>SymEigDecomp eig;
  if (info>0) {
    computedAll(eig) = FALSE;
  }
  if (info==0) {
  <T><Sym>PDQREigServer::n(eig) = decomp.rows();
  lambda(eig).reference(d);
  P(eig).reference(Z);
  }
  return eig;
}

RWBoolean <T><Sym>PDQREigServer::computeEigenVectors() const {return computeVecs_;}

void <T><Sym>PDQREigServer::computeEigenVectors(RWBoolean x) {computeVecs_=x;}


  


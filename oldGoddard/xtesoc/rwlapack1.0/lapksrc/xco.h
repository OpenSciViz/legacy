> #################################################################
> # The template file for the complete orthogonal decompositions  #
> #                                                               #
> # $Header: /users/rcs/lapksrc/xco.h,v 1.1 1993/05/18 17:00:02 alv Exp $
> #                                                               #
> # Copyright (1993) by Rogue Wave Software, Inc.  This software  #
> # is subject to copyright protection under the laws of the      #
> # United States and other countries.                            #
> #                                                               #
> # Written by Al Vermeulen.                                      #
> #                                                               #
> # Limited License.                                              #
> #################################################################
> #
> # $Log: xco.h,v $
> # Revision 1.1  1993/05/18  17:00:02  alv
> # Initial revision
> #
>
> define <T>=RW_T_RW
> if <T>==Double
> define <P>=Double <tLetter>=d
> elif <T>==Float
> define <P>=Float <tLetter>=f
> elif <T>==DComplex
> define <P>=Double <tLetter>=c
> else
> err orthogonal decomposition: not defined for '<T>'
> endif
> 
> # The traditional top of the file stuff
>
> beginwrapper <tLetter>co
>
/*
 * <T>CODecomp: representation of a complete orthogonal decomposition
 *
 * Generated from template $Id: xco.h,v 1.1 1993/05/18 17:00:02 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 * A complete orthogonal decomposition decomposes a matrix A like this:
 *
 *                    [ T 0 ]
 *            A P = Q [     ] Z'
 *                    [ 0 0 ]
 *
 * where P is a permutation matrix, Q and Z are orthogonal, and T is upper
 * triangular.
 *
 * Implementation details:
 * The Q and P parts of the decomposition are represented using a QRDecomp
 * object.  In order to save copying the R part, CODecomp is made a friend
 * or QRDecomp and we invalidate the R portion of the decomposition,
 * writing T and information needed to compute Z over top of it.  Note
 * that the TZ_ object refers to the same data as QRdecomp_.QR_.
 */

#include "rw/<tLetter>qr.h"
#include "rw/<tLetter>utrimat.h"

class <T>CODecomp {
private:
  <T>QRDecomp QRdecomp_;           // used to represent Q and P.  The R part of this is invalid.
  <T>Vec      Ztau_;               // scalars needed to recover Z
  <T>GenMat   TZ_;                 // upper triangular part is T, rest is used to compute Z
  void        dofactor(double tol);// build Ztau_ and TZ_ from QRdecomp_

public:
  <T>CODecomp();                      // Constructors
  <T>CODecomp(const <T>CODecomp&);
  <T>CODecomp(const <T>QRDecomp&, double tol=0);  // tol indicates when to treat diagonal entries as zero
  <T>CODecomp(const <T>GenMat&, double tol=0);    // tol indicates when to treat diagonal entries as zero
  void factor(const <T>QRDecomp&, double tol=0);  // tol indicates when to treat diagonal entries as zero
  void factor(const <T>GenMat&, double tol=0);    // tol indicates when to treat diagonal entries as zero
  ~<T>CODecomp();
  void operator=(const <T>CODecomp&);

  unsigned       rows() const         {return QRdecomp_.rows();}
  unsigned       cols() const         {return QRdecomp_.cols();}
  unsigned       rank() const         {return TZ_.rows();}
  <T>GenMat      P() const            {return QRdecomp_.P();}
  <T>UpperTriMat T() const;
  <T>GenMat      Q() const            {return QRdecomp_.Q();}
  <T>GenMat      Z() const;

  <T>Vec         Px(const <T>Vec& x) const       {return QRdecomp_.Px(x);}
  <T>Vec         PTx(const <T>Vec& x) const      {return QRdecomp_.PTx(x);}
  <T>Vec         Tx(const <T>Vec& x) const;
  <T>Vec         TTx(const <T>Vec& x) const;
  <T>Vec         Tinvx(const <T>Vec& x) const;
  <T>Vec         TTinvx(const <T>Vec& x) const;
  <T>Vec         Qx(const <T>Vec& x) const       {return QRdecomp_.Qx(x);}
  <T>Vec         QTx(const <T>Vec& x) const      {return QRdecomp_.QTx(x);}
  <T>Vec         Zx(const <T>Vec& x) const;
  <T>Vec         ZTx(const <T>Vec& x) const;
};

> endwrapper

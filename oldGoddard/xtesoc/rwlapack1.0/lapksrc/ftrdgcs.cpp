/*
 * Functions for shape conversion for FloatTriDiagMat, except
 * conversion to/from square matrices.
 *
 * Generated from template $Id: xcs.cpp,v 1.2 1993/10/11 21:48:07 alv Exp $
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/lapkerr.h"
#include "rw/rwerr.h"
#include "rw/ftrdgmat.h"
#include "rw/fbandmat.h"

FloatBandMat::FloatBandMat( const FloatTriDiagMat& A )
  : vec(A.dataVec())
{
  n=A.rows();
  band=3;
  bandu=1;
}

FloatTriDiagMat toTriDiagMat( const FloatBandMat& A )
{
  if (A.bandwidth()!=3 || A.upperBandwidth()!=1) {
    RWTHROW(RWInternalErr(RWMessage(RWLAPK_BANDSIZE,A.bandwidth(),3,A.upperBandwidth(),1)));
  }
  return FloatTriDiagMat(A.dataVec(),A.rows(),A.cols());
}


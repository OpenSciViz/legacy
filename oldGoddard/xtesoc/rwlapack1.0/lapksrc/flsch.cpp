/*
 * defs for FloatLeastSqCh
 *
 * Generated from template $Id: xlsch.cpp,v 1.5 1993/10/11 21:48:13 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 */

#include "rw/flsch.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"

FloatLeastSqCh::FloatLeastSqCh() {}

FloatLeastSqCh::FloatLeastSqCh(const FloatGenMat& A)
{
  factor(A);
}

void      FloatLeastSqCh::factor(const FloatGenMat& A)
{
  A_.reference(A);
  FloatSymMat ATA = upperToSymMat(transposeProduct(A,A));
  decomp_.factor(ATA);
}

FloatVec    FloatLeastSqCh::residual(const FloatVec& data) const
{
  FloatVec x = solve(data);
  return data - product(A_,x);
}

Float       FloatLeastSqCh::residualNorm(const FloatVec& data) const
{
  if (data.length()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,data.length(),rows())));
  return l2Norm(residual(data));
}

FloatVec    FloatLeastSqCh::solve(const FloatVec& data) const
{
  if (decomp_.fail()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTSOLVELS)));
  FloatVec ATb = product(transpose(A_),data);
  return ::solve(decomp_,ATb);
}

> #################################################################
> # The template file for Hessenburg decomposition class          #
> #                                                               #
> # $Header: /users/rcs/lapksrc/xhess.cpp,v 1.3 1993/07/08 23:26:13 alv Exp $
> #                                                               #
> # Copyright (1993) by Rogue Wave Software, Inc.  This           #
> # software is subject to copyright protection under the laws    #
> # of the United States and other countries.                     #
> #                                                               #
> # Written by Al Vermeulen.                                      #
> #                                                               #
> # Limited License.                                              #
> #################################################################
> #
> # $Log: xhess.cpp,v $
> # Revision 1.3  1993/07/08  23:26:13  alv
> # ported to SUN CC v3
> #
> # Revision 1.2  1993/07/05  19:19:33  alv
> # fixed up for complex numbers
> #
> # Revision 1.1  1993/05/18  17:00:02  alv
> # Initial revision
> #
>
> # Macros that we need
> 
> define <T>=RW_T_RW
> 
> if <T>==Double
> define <P>=Double <tLetter>=d <lLetter>=d <or>=or
> elif <T>==Float
> define <P>=Float  <tLetter>=f <lLetter>=s <or>=or
> elif <T>==DComplex
> define <P>=Double <tLetter>=c <lLetter>=z <or>=un
> else
> err Hessenberg decomposition: not defined for '<T>'
> endif
>
/*
 * <T>HessenbergDecomp - Hessenberg decomposition of a matrix
 *
 * Generated from template $Id: xhess.cpp,v 1.3 1993/07/08 23:26:13 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 * This decomposition is usually formed as a percusor to computing
 * the eigenvalues and/or Schur decomposition of a non-symmetric matrix.
 *
 * This class includes an optional balancing transformation in addition
 * to the standard Hessenberg decomposition.
 */ 

#include "rw/<tLetter>hess.h"

<T>HessenbergDecomp::<T>HessenbergDecomp()
{
}

<T>HessenbergDecomp::<T>HessenbergDecomp(const <T>GenMat& A, RWBoolean permute, RWBoolean scale)
{
  factor(A,permute,scale);
}

<T>HessenbergDecomp::<T>HessenbergDecomp(const <T>BalanceDecomp& A)
{
  factor(A);
}

void <T>HessenbergDecomp::factor(const <T>GenMat& A, RWBoolean permute, RWBoolean scale)
{
  <T>BalanceDecomp BCB(A,permute,scale);
  factor(BCB);
}
  
void <T>HessenbergDecomp::factor(const <T>BalanceDecomp& decomp)
{
  B_ = decomp.B();
  H_.reference(decomp.C());
  H_.deepenShallowCopy();
  tau_.reshape(decomp.rows());
  long info;
  long n = decomp.rows();
  long blockSize = ilaenv( 1, "DGEHRD", " ", n, B_.lowIndex(), B_.highIndex(), -1 );
  long lwork = n*blockSize;
  <T> *work = new <T> [lwork];
  <lLetter>gehrd(n,B_.lowIndex(),B_.highIndex(),H_.data(),n,tau_.data(),work,lwork,info);
  RWPOSTCONDITION(info==0);
  RWPOSTCONDITION(lwork>=work[0]);
  delete work;
}

<T>GenMat   <T>HessenbergDecomp::B()                     const
{
  <T>GenMat I(rows(),cols(),(<T>)0);
  I.diagonal() = (<T>)1;
  return B_.transform(I);
}

<T>GenMat   <T>HessenbergDecomp::Q()                     const
{
  <T>GenMat theQ = H_.copy();
  long n = rows();
  long info;
  long blockSize = ilaenv( 1, "DORGHR", " ", n, B_.lowIndex(), B_.highIndex(), -1 );
  long lwork = n*blockSize;
  <T> *work = new <T> [lwork];
  <lLetter><or>ghr(n,B_.lowIndex(),B_.highIndex(),theQ.data(),rows(),(<T>*)tau_.data(),work,lwork,info);
  RWPOSTCONDITION(info==0);
  RWPOSTCONDITION(lwork>=work[0]);
  delete work;
  return theQ;
}

<T>GenMat   <T>HessenbergDecomp::H()                     const
{
  <T>GenMat A = H_.copy();
  for(int i=2; i<rows(); i++) {
    A.diagonal(-i) = (<T>)0;
  }
  return A;
}

<T>Vec      <T>HessenbergDecomp::Bx(const <T>Vec& x)       const
{
  return B_.transform(x);
}

<T>Vec      <T>HessenbergDecomp::BInvTx(const <T>Vec& x)   const
{
  return B_.invTransform(x);
}

<T>Vec      <T>HessenbergDecomp::Qx(const <T>Vec& x)      const
{
  <T>GenMat X(x,x.length(),1);
  <T>GenMat theQX = QX(X);
  RWPOSTCONDITION(theQX.cols()==1);
  return theQX.col(0);
}

<T>Vec      <T>HessenbergDecomp::QTx(const <T>Vec& x)      const
{
  <T>GenMat X(x,x.length(),1);
  <T>GenMat theQTX = QTX(X);
  RWPOSTCONDITION(theQTX.cols()==1);
  return theQTX.col(0);
}

<T>GenMat   <T>HessenbergDecomp::BX(const <T>GenMat& x)    const
{
  return B_.transform(x);
}

<T>GenMat   <T>HessenbergDecomp::BInvTX(const <T>GenMat& x)const
{
  return B_.invTransform(x);
}

<T>GenMat   <T>HessenbergDecomp::QX(const <T>GenMat& X)   const
{
  <T>GenMat C = X.copy();
  long n = rows();
  long info;
  long blockSize = ilaenv( 1, "DORMHR", " ", n, B_.lowIndex(), B_.highIndex(), -1 );
  long lwork = n*blockSize;
  <T> *work = new <T> [lwork];
  <lLetter><or>mhr('L','N',C.rows(),C.cols(),B_.lowIndex(),B_.highIndex(),(<T>*)H_.data(),
            H_.rows(),(<T>*)tau_.data(),C.data(),C.rows(),work,lwork,info);
  RWPOSTCONDITION(info==0);
  RWPOSTCONDITION(lwork>=work[0]);
  delete work;
  return C;
}

<T>GenMat   <T>HessenbergDecomp::QTX(const <T>GenMat& X)   const
{
  <T>GenMat C = X.copy();
  long n = rows();
  long info;
  long blockSize = ilaenv( 1, "DORMHR", " ", n, B_.lowIndex(), B_.highIndex(), -1 );
  long lwork = rows()*blockSize;
  <T> *work = new <T> [lwork];
> if <T>==DComplex || <T>==FComplex
  char trans = 'C';
> else
  char trans = 'T';
> endif
  <lLetter><or>mhr('L',trans,C.rows(),C.cols(),B_.lowIndex(),B_.highIndex(),(<T>*)H_.data(),H_.rows(),
                                (<T>*)tau_.data(),C.data(),C.rows(),work,lwork,info);
  RWPOSTCONDITION(info==0);
  RWPOSTCONDITION(lwork>=work[0]);
  delete work;
  return C;
}

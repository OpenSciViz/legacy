#if 0
/*
 * Generated from template $Id: xmth.cpp,v 1.2 1993/10/11 21:48:17 alv Exp $
 *
 * Math member functions for the matrix types
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */
#endif

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/dskewmat.h"
#include "rw/dsymmat.h"

/*
 * This function returns a new matrix the same shape as the one 
 * passed in but with the data passed in.  This way we restrict
 * all the >if statements to a couple lines
 */
inline DoubleSkewMat sameShapeMatrix( DoubleVec& vec, const DoubleSkewMat& A )
{
  return DoubleSkewMat(vec,A.rows(),A.cols());
}


static void verifyMatch(const DoubleSkewMat& A, const DoubleSkewMat& B)
{
    if (A.rows()!=B.rows() || A.cols()!=B.cols())
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATSIZE,A.rows(),A.cols(),B.rows(),B.cols())));
}

RWBoolean DoubleSkewMat::operator==(const DoubleSkewMat& X)
{
    if (n!=X.n) return FALSE;
    return vec==X.vec;	// All elements in vec used in the matrix
}


DoubleSkewMat& DoubleSkewMat::operator+=(const DoubleSkewMat& m)
{
  verifyMatch(*this,m);
  vec += m.vec;
  return *this;
}

DoubleSkewMat& DoubleSkewMat::operator-=(const DoubleSkewMat& m)
{
  verifyMatch(*this,m);
  vec -= m.vec;
  return *this;
}

DoubleSkewMat& DoubleSkewMat::operator*=(const DoubleSkewMat& m)
{
  verifyMatch(*this,m);
  vec *= m.vec;
  return *this;
}

DoubleSkewMat& DoubleSkewMat::operator*=(Double x)
{
  vec *= x;
  return *this;
}

DoubleSkewMat& DoubleSkewMat::operator/=(const DoubleSkewMat& m)
{
  verifyMatch(*this,m);
  vec /= m.vec;
  return *this;
}

DoubleSkewMat& DoubleSkewMat::operator/=(Double x)
{
  vec /= x;
  return *this;
}




DoubleSkewMat operator-(const DoubleSkewMat& A)
{
  DoubleVec temp(-(A.dataVec()));
  return sameShapeMatrix(temp,A);
}
  
DoubleSkewMat operator+(const DoubleSkewMat& A)
{
  return A;
}

DoubleSkewMat operator*(const DoubleSkewMat& A, const DoubleSkewMat& B) {
	verifyMatch(A,B);
	DoubleVec temp(A.dataVec()*B.dataVec());
	return sameShapeMatrix(temp,A);
}

DoubleSkewMat operator/(const DoubleSkewMat& A, const DoubleSkewMat& B) {
	verifyMatch(A,B);
	DoubleVec temp(A.dataVec()/B.dataVec());
	return sameShapeMatrix(temp,A);
}

DoubleSkewMat operator+(const DoubleSkewMat& A, const DoubleSkewMat& B) {
	verifyMatch(A,B);
	DoubleVec temp(A.dataVec()+B.dataVec());
	return sameShapeMatrix(temp,A);
}

DoubleSkewMat operator-(const DoubleSkewMat& A, const DoubleSkewMat& B) {
	verifyMatch(A,B);
	DoubleVec temp(A.dataVec()-B.dataVec());
	return sameShapeMatrix(temp,A);
}

DoubleSkewMat operator*(const DoubleSkewMat& A, Double x) {
	DoubleVec temp(A.dataVec()*x);
	return sameShapeMatrix(temp,A);
}

/*
 * Out-of-line version for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
  DoubleSkewMat operator*(Double x, const DoubleSkewMat& A) { return A*x; }
#endif

DoubleSkewMat operator/(const DoubleSkewMat& A, Double x) {
	DoubleVec temp(A.dataVec()/x);
	return sameShapeMatrix(temp,A);
}


DoubleSkewMat operator/(Double x, const DoubleSkewMat& A) {
	DoubleVec temp(x/A.dataVec());
	return sameShapeMatrix(temp,A);
}



DoubleSymMat abs(const DoubleSkewMat& A) {
	DoubleVec temp(abs(A.dataVec()));
	return DoubleSymMat(temp,A.rows(),A.cols());
}



DoubleSkewMat transpose(const DoubleSkewMat& A)
{
  DoubleSkewMat T(-A);
  Double* index = T.data();		// Fix up the diagonal
  for( int k=1; k<=T.rows(); index+=(++k) ) {
    (*index) *= -1;
  }
  return T;
}

DoubleVec product(const DoubleSkewMat& A, const DoubleVec& x)
{
    if (A.cols()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATVECPROD,A.className,A.rows(),A.cols(),x.length())));
    }
    DoubleVec y(A.rows(),rwUninitialized);
    const DoubleVec& v = A.dataVec();
    int n=A.rows();
    y(0) = (Double)(0);
    for( int i=0; i<n; i++ ) {
      if (i>0) {
        y(i) = -dot( v.slice(i*(i+1)/2,i,1), x.slice(0,i,1) );
      }
      Double* index = (Double*)v.data() + ((i+1)*(i+2)/2-1);
      for( int k=i; k<n; index+=(++k) ) {
	y(i) += (*index)*x(k);
      }
    }
    return y;
}

DoubleVec product(const DoubleVec& x, const DoubleSkewMat& A)
{
    if (A.rows()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECMATPROD,x.length(),A.className,A.rows(),A.cols())));
    }
    DoubleVec y(A.cols(),(Double)(0));
    const DoubleVec& v = A.dataVec();
    int n=A.rows();
    for( int i=0; i<n; i++ ) {
      y(i) = dot( v.slice(i*(i+1)/2,i+1,1), x.slice(0,i+1,1) );
      Double* index = (Double*)v.data() + ((i+2)*(i+3)/2-2);
      for( int k=i+1; k<n; index+=(++k) ) {
	y(i) -= (*index)*x(k);
      }
    }
    return y;
}



/*
 * Definitions for DComplexQRDecomp and DComplexQRDecompServer
 *
 * Generated from template $Id: xqr.cpp,v 1.5 1993/10/15 06:37:58 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 */

#include "rw/lapack.h"
#include "rw/cqr.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/cutrimat.h"

DComplexQRDecomp::DComplexQRDecomp()
{
  pivots_ = 0;
}

DComplexQRDecomp::DComplexQRDecomp(const DComplexQRDecomp& x)
{
  pivots_ = 0;
  *this = x;
}

void DComplexQRDecomp::operator=(const DComplexQRDecomp& x)
{
  tau_.reference(x.tau_);
  QR_.reference(x.QR_);
  delete [] pivots_;
  pivots_ = 0;
  if (x.pivots_) {
    pivots_ = new long [cols()];
    for(int i=cols(); i--;) {
      pivots_[i] = x.pivots_[i];
    }
  }
}

DComplexQRDecomp::DComplexQRDecomp(const DComplexGenMat& A) 
{
  pivots_=0;
  factor(A);
}

void DComplexQRDecomp::factor(const DComplexGenMat& A)
{
  DComplexQRDecompServer server;
  *this = server(A);
}

DComplexQRDecomp::~DComplexQRDecomp()
{
  delete [] pivots_;
}


DComplexGenMat      DComplexQRDecomp::P() const
{
  DComplexGenMat Pmat(cols(),cols(),(DComplex)0);
  if (pivots_) {
    for(int j=cols(); j--;) {
      Pmat((int)(pivots_[j]-1),j) = 1;
    }
  } else {
    Pmat.diagonal() = (DComplex)1;
  }
  return Pmat;
}

DComplexGenMat DComplexQRDecomp::R() const
{
  int m = (cols()<rows()) ? cols() : rows();
  // Obfuscate the following line, in order to shut up a spurious
  // sun CC 3.0.1 warning
  // DComplexGenMat Rmat = QR_(RWSlice(0,m),RWAll);
  DComplexGenMat Rmat = QR_.operator()(RWSlice(0,m),RWAll);
  Rmat.deepenShallowCopy();
  for(int i=m; --i>0;) {  // Loop indexes i=m-1,...,1
    Rmat.diagonal(-i) = (DComplex)0;
  }
  return Rmat;
}

DComplexVec         DComplexQRDecomp::Rdiagonal() const
{
  return QR_.diagonal().copy();
}

DComplexGenMat      DComplexQRDecomp::Q() const
{
  long m = rows();
  long n = cols();
  long k = (m<n) ? m : n;
  DComplexGenMat Qmat(QR_);
  Qmat.resize((int)m,(int)m);
  Qmat.deepenShallowCopy();
  long lwork = n*ilaenv( 1, "zungqr", " ", m, m, k, -1 );
  DComplex *work = new DComplex [lwork];
  long info;
  zungqr(m,m,k,Qmat.data(),m,(DComplex*)tau_.data(),work,lwork,info); 
  RWPOSTCONDITION(info==0);
  RWPOSTCONDITION(work[0]<=lwork);
  delete work;
  return Qmat;   
}

DComplexVec DComplexQRDecomp::Px(const DComplexVec& x) const
{
  if (x.length()!=cols()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),cols())));
  if (pivots_) {
    DComplexVec y(x.length(),rwUninitialized);
    for(int i=x.length(); i--;) { y((int)pivots_[i]-1) = x(i); }
    return y;
  } else {
    return x.copy();
  }
}

DComplexVec DComplexQRDecomp::PTx(const DComplexVec& x) const
{
  if (x.length()!=cols()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),cols())));
  if (pivots_) {
    DComplexVec y(x.length(),rwUninitialized);
    for(int i=x.length(); i--;) { y(i) = x((int)pivots_[i]-1); }
    return y;
  } else {
    return x.copy();
  }
}

DComplexVec DComplexQRDecomp::Rx(const DComplexVec& x) const
{
  if (x.length()!=cols()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),cols())));
  int m = (cols()<rows()) ? cols() : rows();
  // explicit call to operator() in next line shuts up sun v3.0.1 warning
  DComplexGenMat R = QR_.operator()(RWSlice(0,m),RWAll);     // Also has entries for Q
  RWPRECONDITION(R.rows()<=R.cols());
  if (R.cols()>R.rows()) {
    DComplexUpperTriMat R1 = toUpperTriMat(R(RWAll,RWSlice(0,m)));
    DComplexGenMat R2 = R(RWAll,RWToEnd(m));
    return product(R1,x(RWSlice(0,m))) + product(R2,x(RWToEnd(m)));
  } else {
    DComplexUpperTriMat R1 = toUpperTriMat(R);
    return product(R1,x);
  }
}

DComplexVec DComplexQRDecomp::RTx(const DComplexVec& x) const
{
  int m = (cols()<rows()) ? cols() : rows();
  if (x.length()!=m) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),m)));
  // explicit call to operator() in next line shuts up sun v3.0.1 warning
  DComplexGenMat R = QR_.operator()(RWSlice(0,m),RWAll);     // Also has entries for Q
  RWPRECONDITION(R.rows()<=R.cols());
  if (R.cols()>R.rows()) {
    DComplexVec y(cols(),rwUninitialized);
    DComplexUpperTriMat R1 = toUpperTriMat(R(RWAll,RWSlice(0,m)));
    DComplexGenMat R2 = R(RWAll,RWToEnd(m));
    y(RWSlice(0,m)) = product(x,conj(R1));
    y(RWToEnd(m)) = product(x,conj(R2));
    return y;
  } else {
    DComplexUpperTriMat R1 = conj(toUpperTriMat(R));
    return product(x,R1);
  }
}

DComplexVec DComplexQRDecomp::Rinvx(const DComplexVec& x) const
{
  if (x.length()!=cols()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),cols())));
  if (cols()>rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_RSINGULAR)));
  DComplexVec y = x.copy();
  long n = cols();
  long m = rows();
  long one = 1;
  long info;
  ztrtrs('U','N','N',n,one,(DComplex*)QR_.data(),m,y.data(),n,info);
  RWPOSTCONDITION(info>=0);
  if (info>0) RWTHROW(RWInternalErr(RWMessage(RWLAPK_RSINGULAR)));
  return y;
}

DComplexVec DComplexQRDecomp::RTinvx(const DComplexVec& x) const
{
  if (x.length()!=cols()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),cols())));
  if (cols()>rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_RSINGULAR)));
  DComplexVec y = x.copy();
  long n = cols();
  long m = rows();
  long one = 1;
  long info;
  ztrtrs('U','C','N',n,one,(DComplex*)QR_.data(),m,y.data(),n,info);
  RWPOSTCONDITION(info>=0);
  if (info>0) RWTHROW(RWInternalErr(RWMessage(RWLAPK_RSINGULAR)));
  return y;
}

DComplexVec DComplexQRDecomp::Qx(const DComplexVec& x) const
{
  if (x.length()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),rows())));
  long m = rows();
  long n = cols();
  long k = (m<n) ? m : n;
  DComplexVec y = x.copy();
  long one = 1;
  long lwork = ilaenv( 1, "zunmqr", "LN", m, one, k, -1 );
  DComplex *work = new DComplex [lwork];
  long info;
  zunmqr('L','N',m,one,k,(DComplex*)QR_.data(),m,(DComplex*)tau_.data(),y.data(),m,work,lwork,info);
  RWPOSTCONDITION(info==0);
  RWPOSTCONDITION(lwork>=work[0]);
  delete [] work;
  return y;
}

DComplexVec DComplexQRDecomp::QTx(const DComplexVec& x) const
{
  if (x.length()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),rows())));
  long m = rows();
  long n = cols();
  long k = (m<n) ? m : n;
  DComplexVec y = x.copy();
  long one = 1;
  long lwork = ilaenv( 1, "zunmqr", "LT", m, one, k, -1 );
  DComplex *work = new DComplex [lwork];
  long info;
  zunmqr('L','C',m,one,k,(DComplex*)QR_.data(),m,(DComplex*)tau_.data(),y.data(),m,work,lwork,info);
  RWPOSTCONDITION(info==0);
  RWPOSTCONDITION(lwork>=work[0]);
  delete [] work;
  return y;
}

DComplexQRDecompServer::DComplexQRDecompServer()
{
  pivot_ = TRUE;
}

void DComplexQRDecompServer::setPivoting(RWBoolean x)
{
  pivot_ = x;
}

void DComplexQRDecompServer::setInitialIndex(int i)
{
  if (i<0) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTFREEZE,i)));
  for(int j=initial_.length(); j--;) {
    if (initial_(j)==i) return;   // already frozen
  }
  initial_.resize(initial_.length()+1);
  initial_(initial_.length()-1) = i;
}

void DComplexQRDecompServer::setFreeIndex(int i)
{
  if (i<0) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTFREE,i)));
  for(int j=initial_.length(); j--;) {
    if (initial_(j)==i) {
      int l = initial_.length()-j-1;
      if (l>0) {
        initial_(RWSlice(j,l)) = initial_(RWSlice(j+1,l));
      }
      initial_.resize(initial_.length()-1);
    }
  }
}

DComplexQRDecomp DComplexQRDecompServer::operator()(const DComplexGenMat& A) const
{
  long m = A.rows();
  long n = A.cols();
  long info;
  DComplexQRDecomp decomp;
  decomp.QR_.reference(A.copy());
  decomp.tau_.reshape((m<n) ? (int)m : (int)n);
  delete [] decomp.pivots_;
  decomp.pivots_ = 0;
  if (pivot_) {
    decomp.pivots_ = new long [n];
    for(int i=(int)n; i--;) decomp.pivots_[i]=0;
    for(i=initial_.length(); i--;) {
      long index = initial_(i);
      RWPRECONDITION(index>=0);
      if (index>=n) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTFREEZE,index)));
      decomp.pivots_[index] = 1;
    }
    DComplex *work = new DComplex [3*n];
    Double *rwork = new Double [2*n];
    zgeqpf(m,n,decomp.QR_.data(),m,decomp.pivots_,decomp.tau_.data(),work,rwork,info);
    delete [] rwork;
    RWPOSTCONDITION(info==0);
    delete [] work;
  } else {
    long lwork = n*ilaenv( 1, "zgeqrf", " ", m, n, -1, -1 );
    DComplex *work = new DComplex [lwork];
    zgeqrf(m,n,decomp.QR_.data(),m,decomp.tau_.data(),work,lwork,info);
    RWPOSTCONDITION(info==0);
    RWPOSTCONDITION(lwork>=work[0]);
    delete [] work;
  }
  return decomp;
}           

/*
 * Defs for DComplexSVDecomp
 *
 * Generated from template $Id: xsv.cpp,v 1.4 1993/10/11 21:48:24 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 */ 

#include "rw/lapkdefs.h"
#include "rw/lapack.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/csv.h"
#include "rw/cgenmat.h"
#include "rw/cvec.h"

DComplexSVDecomp::DComplexSVDecomp()
{
  computedAll_ = TRUE;
}

DComplexSVDecomp::DComplexSVDecomp(const DComplexSVDecomp& x)
{
  *this = x;
}

DComplexSVDecomp::DComplexSVDecomp(const DComplexGenMat& X, Double tol)
{
  factor(X,tol);
}

void DComplexSVDecomp::factor(const DComplexGenMat& X, Double tol)
{
  DComplexSVServer s;
  s.setTolerance(tol);
  *this = s(X);
}
  
void DComplexSVDecomp::operator=(const DComplexSVDecomp& x)
{
  sigma_.reference(x.sigma_);
  U_.reference(x.U_);
  VT_.reference(x.VT_);
  computedAll_ = x.computedAll_;
}

Double             DComplexSVDecomp::singularValue(int i) const
{
  if (i>=0 && i<sigma_.length()) return sigma_(i);
  else return 0;
}

const DComplexVec    DComplexSVDecomp::leftVector(int i)    const
{                                                     
  if (i<0 || i>=U_.cols()) RWTHROW(RWBoundsErr(RWMessage(RWLAPK_NOSV,i)));
  return U_.col(i);
}

const DComplexVec    DComplexSVDecomp::rightVector(int i)   const
{                                                     
  if (i<0 || i>=VT_.rows()) RWTHROW(RWBoundsErr(RWMessage(RWLAPK_NOSV,i)));
  return conj(VT_.row(i));
}

void            DComplexSVDecomp::truncate(Double tol)
{
  RWPRECONDITION(tol>=0);
  int r = sigma_.length();
  while(r>0 && sigma_(r-1)<=tol) --r;
  sigma_.resize(r);
}

DComplexSVServer::DComplexSVServer()
{
  computeAllLeftVecs_ = FALSE;       
  computeAllRightVecs_ = FALSE;    
  computeDefaultLeftVecs_ = TRUE; 
  computeDefaultRightVecs_ = TRUE;
  numLeftVecs_ = 0;
  numRightVecs_ = 0;           
  tol_ = 0;
}

void        DComplexSVServer::computeAllVectors()
{
  computeAllLeftVectors();
  computeAllRightVectors();
}

void        DComplexSVServer::computeAllLeftVectors()
{
  computeAllLeftVecs_ = TRUE;
}

void        DComplexSVServer::computeAllRightVectors()
{
  computeAllRightVecs_ = TRUE;
}

void        DComplexSVServer::computeDefaultVectors()
{
  computeDefaultLeftVectors();
  computeDefaultRightVectors();
}

void        DComplexSVServer::computeDefaultLeftVectors()
{
  computeAllLeftVecs_ = FALSE;
  computeDefaultLeftVecs_ = TRUE;
}

void        DComplexSVServer::computeDefaultRightVectors()
{
  computeAllRightVecs_ = FALSE;
  computeDefaultRightVecs_ = TRUE;
}

void        DComplexSVServer::computeVectors(unsigned n)
{
  computeLeftVectors(n);
  computeRightVectors(n);
}

void        DComplexSVServer::computeLeftVectors(unsigned n)
{
  computeAllLeftVecs_ = computeDefaultLeftVecs_ = FALSE;
  numLeftVecs_ = n;
}

void        DComplexSVServer::computeRightVectors(unsigned n)
{
  computeAllRightVecs_ = computeDefaultRightVecs_ = FALSE;
  numRightVecs_ = n;
}
       
void        DComplexSVServer::setTolerance(Double x)
{
  tol_ = x;
}

DComplexSVDecomp DComplexSVServer::operator()(const DComplexGenMat& A) const
{
  DComplexSVDecomp decomp;   // We'll return this someday

  // First set up parameters for and then call sgebrd to generate
  // bidiagonal reduction of A
  long m = A.rows();
  long n = A.cols();
  DComplexGenMat B = A.copy();
  long lda = B.colStride();
  unsigned maxrank = (A.rows()<A.cols()) ? A.rows() : A.cols();
  DoubleVec d(maxrank,rwUninitialized);
  DoubleVec e(maxrank,rwUninitialized);   // one longer than necessary to avoid <0 checks
  DComplexVec tauq(maxrank,rwUninitialized);
  DComplexVec taup(maxrank,rwUninitialized);
  long nb = ilaenv( 1, "DGEBRD", " ", m, n, -1, -1 );
  long lwork = (m+n)*nb + 3*n;   // ensure work is big enough for sbdsqr() too
  DComplex *work = new DComplex [lwork];
  long info;
  zgebrd(m,n,B.data(),lda,d.data(),e.data(),tauq.data(),taup.data(),work,lwork,info);
  RWPOSTCONDITION(info==0);
  RWPOSTCONDITION(lwork>=work[0]);

  // Compute the SVD of B.  If we need any left singular vectors,
  // compute Q, the left vecs of B, if we need any right vectors,
  // compute PT, the right vecs of B.
  char uplo = (m>=n) ? 'U' : 'L';
  long npt = 0;
  long nrq = 0;
  long ldpt = 1;
  long ldq = 1;
  if (computeAllLeftVecs_ || computeDefaultLeftVecs_ || numLeftVecs_>0) {
    decomp.U_.reshape(maxrank,maxrank);
    decomp.U_ = (DComplex)0;
    decomp.U_.diagonal() = (DComplex)1;
    nrq = maxrank;
    ldq = maxrank;
  }
  if (computeAllRightVecs_ || computeDefaultRightVecs_ || numRightVecs_>0) {
    decomp.VT_.reshape(maxrank,maxrank);
    decomp.VT_ = (DComplex)0;
    decomp.VT_.diagonal() = (DComplex)1;
    npt = maxrank;
    ldpt = maxrank;
  }
  // In the complex case, work needs to be a real array here.  I just
  // put a cast in, since it should always be OK to case a pointer to
  // a (C-style) array of complex to an array of reals
  zbdsqr(uplo,maxrank,npt,nrq,0,d.data(),e.data(),decomp.VT_.data(),ldpt,decomp.U_.data(),ldq,(DComplex*)0,1,(Double*)work,info);
  RWPOSTCONDITION(info>=0);
  if (info!=0) {
    decomp.computedAll_ = FALSE;
    decomp.U_.reshape(B.rows(),0);
    decomp.VT_.reshape(0,B.cols());
  } else {
    
    // fill in the singular values
    decomp.sigma_.reference(d);
    decomp.truncate(tol_);
    int r = decomp.rank();

    // compute the left singular vectors, if required
    int p = numLeftVecs_;
    if (computeDefaultLeftVecs_) p = r;
    if (p>A.rows() || computeAllLeftVecs_) p = A.rows();
    if (p>0) {
      decomp.U_.resize(A.rows(),p);
      for(int i=maxrank; i<p; i++) decomp.U_(i,i) = 1; // set the new part of decomp.U_ to identity
      zunmbr( 'Q','L','N',
               decomp.U_.rows(),
               decomp.U_.cols(),
               B.cols(),
               B.data(),
               B.colStride(),
               tauq.data(),
               decomp.U_.data(),
               decomp.U_.colStride(),
               work,lwork,info);
      RWPOSTCONDITION(info==0);
    } else {
      decomp.U_.reshape(B.rows(),0);   // don't want any left vecs
    }

    // compute the right singular vectors, if required
    int q = numRightVecs_;
    if (computeDefaultRightVecs_) q = r;
    if (q>A.cols() || computeAllRightVecs_) q = A.cols();
    if (q>0) {
      decomp.VT_.resize(q,B.cols());
      for(int i=maxrank; i<q; i++) decomp.VT_(i,i) = 1; // set the new part of decomp.VT_ to identity
      zunmbr( 'P','R',
               'C',
               decomp.VT_.rows(),
               decomp.VT_.cols(),
               B.rows(),
               B.data(),
               B.colStride(),
               taup.data(),
               decomp.VT_.data(),
               decomp.VT_.colStride(),
               work,lwork,info);
      RWPOSTCONDITION(info==0);
    } else {
      decomp.VT_.reshape(0,B.cols());  // don't want any right vecs
    }
  }

  delete work;
  return decomp;
}

/*
 * The conversion functions to/from general matrices.
 *
 * Generated from template $Id: xcg.cpp,v 1.2 1993/07/05 19:19:33 alv Exp $
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/cskewmat.h"
#include "rw/cgenmat.h"

DComplexGenMat::DComplexGenMat( const DComplexSkewMat& A )
  : RWMatView(A.rows(),A.cols(),sizeof(DComplex))
{
  for( int j=0; j<A.rows(); j++ ) {
    for( register int i=0; i<=j; i++ ) {
      DComplex x = A.val(i,j);
      set(j,i,-x);
      set(i,j,x);
    }
  }
}

DComplexSkewMat toSkewMat( const DComplexGenMat& S, RWBoolean keepMainDiag)
{
  int n = S.cols();
  DComplexSkewMat A(S.rows(),n);
   // Use bcset rather than set in the following.  set generates
   // inlines which cause cfront to spew things too complicated for
   // the DEC MIPS C compiler
   for( int j=0; j<n; j++ ) {
     for( int i=0; i<j; i++ ) {
       A.bcset(i,j,(S.val(i,j)-S.val(j,i))/DComplex(2,0));	// Explicit constructor necessary for Zortech
     }
     if (keepMainDiag) A.bcset(j,j,S.val(j,j));
     else              A.bcset(j,j,DComplex(0,0));		// Explicit constructor necessary for Zortech
   }
   return A;
}


/*
 * Implementation of FloatTriDiagFact
 *
 * Generated from template $Id: xfct.cpp,v 1.6 1993/10/11 21:48:11 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/lapkdefs.h"
#include "rw/ftrdgfct.h"
#include "rw/lapack.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"  
#include "rw/fgenmat.h"

const char* FloatTriDiagFact::className = "FloatTriDiagFact";

FloatTriDiagFact::~FloatTriDiagFact()
{
  delete [] pvts;
}

FloatTriDiagFact::FloatTriDiagFact()
{
  info = -1;
  pvts=0;
}

FloatTriDiagFact::FloatTriDiagFact( const FloatTriDiagFact& A )
  : dl(A.dl), d(A.d), du(A.du), d2(A.d2)
{
  info = A.info;
  Anorm = A.Anorm;
  unsigned n = A.rows();
  pvts = (n>0) ? new long [n] : 0;
  while(n--) {
    pvts[n] = A.pvts[n];
  }
}

FloatTriDiagFact::FloatTriDiagFact(const FloatTriDiagMat& A, RWBoolean estimateCondition)
  // Initialize in the body so we can be careful of negative lengths
{
    d.reference(A.diagonal().copy());
    if (A.rows()>1) {
      dl.reference(A.diagonal(-1).copy());
      du.reference(A.diagonal(1).copy());
      d2.reshape(A.rows()-1);    // Only use n-2 elements, but this simplifies the code by eliminating if statements
    }
    pvts = 0;
    dofactor(estimateCondition);
}

void FloatTriDiagFact::factor( const FloatTriDiagMat& A, RWBoolean estimateCondition )
{
    d.reference(A.diagonal().copy());
    if (A.rows()>1) {
      dl.reference(A.diagonal(-1).copy());
      du.reference(A.diagonal(1).copy());
      d2.reshape(A.rows()-1);    // Only use n-2 elements, but this simplifies the code by eliminating if statements
    } else {
      dl.reshape(0);
      du.reshape(0);
      d2.reshape(0);
    }
    d2.reshape(A.rows()-2);
    dofactor(estimateCondition);
}

void FloatTriDiagFact::dofactor(RWBoolean estimateCondition)
{
   info=0;   // for size zero matrices, need to set this explicitly
   long n = rows();	

    // Error (sanity) checking
    RWPRECONDITION(factorization.dataVec().stride()==1);

    delete [] pvts;
    pvts = (n>0) ? new long [n] : 0;

    // If we care to ever find the condition number, we need to know
    // the 1-norm of the matrix A.  Compute it while the member data
    // factorization is still a copy of A, and not yet it's factorization.
    Anorm = -1; 
    if (n>0 && estimateCondition) {
      Anorm = slangt('1',n,dl.data(),d.data(),du.data());
    }

    if (n>0) {
      sgttrf(n,dl.data(),d.data(),du.data(),d2.data(),pvts,info);
      RWPOSTCONDITION(info>=0);
    }
}

RWBoolean FloatTriDiagFact::fail() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}


RWBoolean FloatTriDiagFact::isSingular() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}

Float FloatTriDiagFact::condition() const
{
    Float Anormcopy = Anorm;      // Copy to stop warnings about ref parm
    long info;
    long n = rows();
    Float rcond;
    Float *work = new Float[4*n];   // Some routines only require 2*n, but what the hell
      long *iwork = new long [n];
      sgtcon('1',n,(Float*)dl.data(),(Float*)d.data(),(Float*)du.data(),(Float*)d2.data(),pvts,Anormcopy,rcond,work,iwork,info);
      delete iwork;
    delete [] work;
    RWPOSTCONDITION(info==0);
    return rcond;
}

FloatVec FloatTriDiagFact::solve( const FloatVec& b ) const
{
  FloatGenMat B(b,b.length(),1);
  FloatGenMat X = solve(B);
  RWPOSTCONDITION(X.cols()==1);
  return X.col(0);
}

FloatGenMat FloatTriDiagFact::solve( const FloatGenMat& B ) const
{
    FloatGenMat X(B.copy());
    long n = rows();
    if (B.rows()!=n) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,B.rows(),n)));
    if (fail()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTSOLVE)));
    long nrhs = X.cols();
    long info;
    if (n>0) {
    sgttrs('N',n,nrhs,(Float*)dl.data(),(Float*)d.data(),(Float*)du.data(),(Float*)d2.data(),pvts,X.data(),n,info);
      RWPOSTCONDITION(info==0);
    }
    return X;    
}

Float FloatTriDiagFact::determinant() const
{
    int numExchanges = 0;
    for(int i=rows(); i--;) {
      if (pvts[i]!=i+1) numExchanges++;
    }
    Float detLU = prod(d);
    return (numExchanges%2) ? -detLU : detLU;
}
              

/*
 * Global functions which provide alternate ways to call member functions.
 * These used to be inline, but this requires that you include more header
 * files on some compilers, and that is just not worth it.
 */
FloatVec solve(const FloatTriDiagFact& A, const FloatVec& b) { return A.solve(b); }
FloatGenMat solve(const FloatTriDiagFact& A, const FloatGenMat& b) { return A.solve(b); }
Float    determinant (const FloatTriDiagFact& A)	      { return A.determinant(); }


Float condition(const FloatTriDiagFact& A) { return A.condition(); }



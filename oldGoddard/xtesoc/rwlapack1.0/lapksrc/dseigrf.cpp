/*
 * Implementation of DoubleSymRFQREigServer
 *
 * Generated from template $Id: xseigrf.cpp,v 1.3 1993/10/11 21:48:22 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * Uses Lapack's root free QR algorithm to compute eigenvalues of
 * a symmetric matrix.
 */ 

#include "rw/dseigsrv.h"
#include "rw/dtd.h"
#include "rw/lapack.h"


DoubleSymRFQREigServer::DoubleSymRFQREigServer()
{
}

DoubleSymEigDecomp DoubleSymRFQREigServer::decompose(const DoubleSymTriDiagDecomp& decomp)
{
  long n = decomp.rows();
  DoubleVec d = decomp.diagonal().copy();
  DoubleVec e(decomp.rows(),rwUninitialized);   // lapack needs a length n (not n-1) off diagonal
  if (n>0) e(RWSlice(0,decomp.rows()-1)) = decomp.offDiagonal();
  long info;
  dsterf(n,d.data(),e.data(),info);
  RWPOSTCONDITION(info>=0);

  DoubleSymEigDecomp eig;
  DoubleSymEigServer::n(eig) = decomp.rows();
  lambda(eig).reference(d);
  if (info>0) {
    lambda(eig).resize((unsigned)info);  // explicit cast to avoid warnings on SUN
    computedAll(eig) = FALSE;
  }
  return eig;
}

RWBoolean DoubleSymRFQREigServer::computeEigenVectors() const {return FALSE;}


  


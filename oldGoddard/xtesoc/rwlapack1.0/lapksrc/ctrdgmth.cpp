#if 0
/*
 * Generated from template $Id: xmth.cpp,v 1.2 1993/10/11 21:48:17 alv Exp $
 *
 * Math member functions for the matrix types
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */
#endif

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/ctrdgmat.h"
#include "rw/dtrdgmat.h"

/*
 * This function returns a new matrix the same shape as the one 
 * passed in but with the data passed in.  This way we restrict
 * all the >if statements to a couple lines
 */
inline DComplexTriDiagMat sameShapeMatrix( DComplexVec& vec, const DComplexTriDiagMat& A )
{
  return DComplexTriDiagMat(vec,A.rows(),A.cols());
}

inline DoubleTriDiagMat sameShapeRealMatrix( DoubleVec& vec, const DComplexTriDiagMat& A )
{
  return DoubleTriDiagMat(vec,A.rows(),A.cols());
}

static void verifyMatch(const DComplexTriDiagMat& A, const DComplexTriDiagMat& B)
{
    if (A.rows()!=B.rows() || A.cols()!=B.cols())
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATSIZE,A.rows(),A.cols(),B.rows(),B.cols())));
}

RWBoolean DComplexTriDiagMat::operator==(const DComplexTriDiagMat& X)
{
    if (n!=X.n) return FALSE;
    // The following two statements cannot be combined into one
    // because of a cfront limitation:
    if ( n<1 ) return TRUE;
    return vec.slice(1,3*n-2)==X.vec.slice(1,3*n-2);
}


DComplexTriDiagMat& DComplexTriDiagMat::operator+=(const DComplexTriDiagMat& m)
{
  verifyMatch(*this,m);
  vec += m.vec;
  return *this;
}

DComplexTriDiagMat& DComplexTriDiagMat::operator-=(const DComplexTriDiagMat& m)
{
  verifyMatch(*this,m);
  vec -= m.vec;
  return *this;
}

DComplexTriDiagMat& DComplexTriDiagMat::operator*=(const DComplexTriDiagMat& m)
{
  verifyMatch(*this,m);
  vec *= m.vec;
  return *this;
}

DComplexTriDiagMat& DComplexTriDiagMat::operator*=(DComplex x)
{
  vec *= x;
  return *this;
}

DComplexTriDiagMat& DComplexTriDiagMat::operator/=(const DComplexTriDiagMat& m)
{
  verifyMatch(*this,m);
  vec /= m.vec;
  return *this;
}

DComplexTriDiagMat& DComplexTriDiagMat::operator/=(DComplex x)
{
  vec /= x;
  return *this;
}




DComplexTriDiagMat operator-(const DComplexTriDiagMat& A)
{
  DComplexVec temp(-(A.dataVec()));
  return sameShapeMatrix(temp,A);
}
  
DComplexTriDiagMat operator+(const DComplexTriDiagMat& A)
{
  return A;
}

DComplexTriDiagMat operator*(const DComplexTriDiagMat& A, const DComplexTriDiagMat& B) {
	verifyMatch(A,B);
	DComplexVec temp(A.dataVec()*B.dataVec());
	return sameShapeMatrix(temp,A);
}

DComplexTriDiagMat operator/(const DComplexTriDiagMat& A, const DComplexTriDiagMat& B) {
	verifyMatch(A,B);
	DComplexVec temp(A.dataVec()/B.dataVec());
	return sameShapeMatrix(temp,A);
}

DComplexTriDiagMat operator+(const DComplexTriDiagMat& A, const DComplexTriDiagMat& B) {
	verifyMatch(A,B);
	DComplexVec temp(A.dataVec()+B.dataVec());
	return sameShapeMatrix(temp,A);
}

DComplexTriDiagMat operator-(const DComplexTriDiagMat& A, const DComplexTriDiagMat& B) {
	verifyMatch(A,B);
	DComplexVec temp(A.dataVec()-B.dataVec());
	return sameShapeMatrix(temp,A);
}

DComplexTriDiagMat operator*(const DComplexTriDiagMat& A, DComplex x) {
	DComplexVec temp(A.dataVec()*x);
	return sameShapeMatrix(temp,A);
}

/*
 * Out-of-line version for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
  DComplexTriDiagMat operator*(DComplex x, const DComplexTriDiagMat& A) { return A*x; }
#endif

DComplexTriDiagMat operator/(const DComplexTriDiagMat& A, DComplex x) {
	DComplexVec temp(A.dataVec()/x);
	return sameShapeMatrix(temp,A);
}





DoubleTriDiagMat abs(const DComplexTriDiagMat& A) {
	DoubleVec temp(abs(A.dataVec()));
	return sameShapeRealMatrix(temp,A);
}


DComplexTriDiagMat conj(const DComplexTriDiagMat& A) {
	DComplexVec temp(conj(A.dataVec()));
	return sameShapeMatrix(temp,A);
}

DoubleTriDiagMat real(const DComplexTriDiagMat& A) {
	DoubleVec temp(real(A.dataVec()));
	return sameShapeRealMatrix(temp,A);
}

DoubleTriDiagMat imag(const DComplexTriDiagMat& A) {
	DoubleVec temp(imag(A.dataVec()));
	return sameShapeRealMatrix(temp,A);
}

DoubleTriDiagMat norm(const DComplexTriDiagMat& A) {
	DoubleVec temp(norm(A.dataVec()));
	return sameShapeRealMatrix(temp,A);
}

DoubleTriDiagMat arg(const DComplexTriDiagMat& A) {
	DoubleVec temp(arg(A.dataVec()));
	return sameShapeRealMatrix(temp,A);
}



DComplexTriDiagMat transpose(const DComplexTriDiagMat& A)
{
  DComplexTriDiagMat T(A.rows(),A.cols());
  T.diagonal(-1) = A.diagonal(1);
  T.diagonal(0) = A.diagonal(0);
  T.diagonal(1) = A.diagonal(-1);
  return T;
}

DComplexVec product(const DComplexTriDiagMat& A, const DComplexVec& x)
{
    if (A.cols()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATVECPROD,A.className,A.rows(),A.cols(),x.length())));
    }
    DComplexVec y(A.rows(),rwUninitialized);
    y = A.diagonal(0) * x;
    y.slice(1,x.length()-1) += A.diagonal(-1) * x.slice(0,x.length()-1);
    y.slice(0,x.length()-1) += A.diagonal(1) * x.slice(1,x.length()-1);
    return y;
}

DComplexVec product(const DComplexVec& x, const DComplexTriDiagMat& A)
{
    if (A.rows()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECMATPROD,x.length(),A.className,A.rows(),A.cols())));
    }
    DComplexVec y(A.cols(),rwUninitialized);
    y = A.diagonal(0) * x;
    y.slice(1,x.length()-1) += A.diagonal(1) * x.slice(0,x.length()-1);
    y.slice(0,x.length()-1) += A.diagonal(-1) * x.slice(1,x.length()-1);
    return y;
}



/*
 * Implementation of DoubleSymTriDiagDecomp
 *
 * Generated from template $Id: xtd.cpp,v 1.2 1993/10/11 21:48:26 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */ 

#include "rw/dtd.h"
#include "rw/lapkerr.h"
#include "rw/rwerr.h"

void DoubleSymTriDiagDecomp::set(const DoubleVec& diag_, const DoubleVec& offdiag_)
{
  if (offdiag_.length()+1 != diag_.length()) {
    RWTHROW(RWInternalErr(RWMessage(RWLAPK_OFFDIAG,offdiag_.length(),diag_.length()-1)));
  }
  diag.reference(diag_);
  offdiag.reference(offdiag_);
}

DoubleVec DoubleSymTriDiagDecomp::transform(const DoubleVec& x) const
{
  DoubleGenMat C(x,x.length(),1);
  DoubleGenMat QC = transform(C);
  RWPOSTCONDITION(QC.cols()==1);
  return QC.col(0);
}
  


/*
 * Implementation of DComplexTriDiagFact
 *
 * Generated from template $Id: xfct.cpp,v 1.6 1993/10/11 21:48:11 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/lapkdefs.h"
#include "rw/ctrdgfct.h"
#include "rw/lapack.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"  
#include "rw/cgenmat.h"

const char* DComplexTriDiagFact::className = "DComplexTriDiagFact";

DComplexTriDiagFact::~DComplexTriDiagFact()
{
  delete [] pvts;
}

DComplexTriDiagFact::DComplexTriDiagFact()
{
  info = -1;
  pvts=0;
}

DComplexTriDiagFact::DComplexTriDiagFact( const DComplexTriDiagFact& A )
  : dl(A.dl), d(A.d), du(A.du), d2(A.d2)
{
  info = A.info;
  Anorm = A.Anorm;
  unsigned n = A.rows();
  pvts = (n>0) ? new long [n] : 0;
  while(n--) {
    pvts[n] = A.pvts[n];
  }
}

DComplexTriDiagFact::DComplexTriDiagFact(const DComplexTriDiagMat& A, RWBoolean estimateCondition)
  // Initialize in the body so we can be careful of negative lengths
{
    d.reference(A.diagonal().copy());
    if (A.rows()>1) {
      dl.reference(A.diagonal(-1).copy());
      du.reference(A.diagonal(1).copy());
      d2.reshape(A.rows()-1);    // Only use n-2 elements, but this simplifies the code by eliminating if statements
    }
    pvts = 0;
    dofactor(estimateCondition);
}

void DComplexTriDiagFact::factor( const DComplexTriDiagMat& A, RWBoolean estimateCondition )
{
    d.reference(A.diagonal().copy());
    if (A.rows()>1) {
      dl.reference(A.diagonal(-1).copy());
      du.reference(A.diagonal(1).copy());
      d2.reshape(A.rows()-1);    // Only use n-2 elements, but this simplifies the code by eliminating if statements
    } else {
      dl.reshape(0);
      du.reshape(0);
      d2.reshape(0);
    }
    d2.reshape(A.rows()-2);
    dofactor(estimateCondition);
}

void DComplexTriDiagFact::dofactor(RWBoolean estimateCondition)
{
   info=0;   // for size zero matrices, need to set this explicitly
   long n = rows();	

    // Error (sanity) checking
    RWPRECONDITION(factorization.dataVec().stride()==1);

    delete [] pvts;
    pvts = (n>0) ? new long [n] : 0;

    // If we care to ever find the condition number, we need to know
    // the 1-norm of the matrix A.  Compute it while the member data
    // factorization is still a copy of A, and not yet it's factorization.
    Anorm = -1; 
    if (n>0 && estimateCondition) {
      Anorm = zlangt('1',n,dl.data(),d.data(),du.data());
    }

    if (n>0) {
      zgttrf(n,dl.data(),d.data(),du.data(),d2.data(),pvts,info);
      RWPOSTCONDITION(info>=0);
    }
}

RWBoolean DComplexTriDiagFact::fail() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}


RWBoolean DComplexTriDiagFact::isSingular() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}

Double DComplexTriDiagFact::condition() const
{
    Double Anormcopy = Anorm;      // Copy to stop warnings about ref parm
    long info;
    long n = rows();
    Double rcond;
    DComplex *work = new DComplex[4*n];   // Some routines only require 2*n, but what the hell
      zgtcon('1',n,(DComplex*)dl.data(),(DComplex*)d.data(),(DComplex*)du.data(),(DComplex*)d2.data(),pvts,Anormcopy,rcond,work,info);
    delete [] work;
    RWPOSTCONDITION(info==0);
    return rcond;
}

DComplexVec DComplexTriDiagFact::solve( const DComplexVec& b ) const
{
  DComplexGenMat B(b,b.length(),1);
  DComplexGenMat X = solve(B);
  RWPOSTCONDITION(X.cols()==1);
  return X.col(0);
}

DComplexGenMat DComplexTriDiagFact::solve( const DComplexGenMat& B ) const
{
    DComplexGenMat X(B.copy());
    long n = rows();
    if (B.rows()!=n) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,B.rows(),n)));
    if (fail()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTSOLVE)));
    long nrhs = X.cols();
    long info;
    if (n>0) {
    zgttrs('N',n,nrhs,(DComplex*)dl.data(),(DComplex*)d.data(),(DComplex*)du.data(),(DComplex*)d2.data(),pvts,X.data(),n,info);
      RWPOSTCONDITION(info==0);
    }
    return X;    
}

DComplex DComplexTriDiagFact::determinant() const
{
    int numExchanges = 0;
    for(int i=rows(); i--;) {
      if (pvts[i]!=i+1) numExchanges++;
    }
    DComplex detLU = prod(d);
    return (numExchanges%2) ? -detLU : detLU;
}
              

/*
 * Global functions which provide alternate ways to call member functions.
 * These used to be inline, but this requires that you include more header
 * files on some compilers, and that is just not worth it.
 */
DComplexVec solve(const DComplexTriDiagFact& A, const DComplexVec& b) { return A.solve(b); }
DComplexGenMat solve(const DComplexTriDiagFact& A, const DComplexGenMat& b) { return A.solve(b); }
DComplex    determinant (const DComplexTriDiagFact& A)	      { return A.determinant(); }


Double condition(const DComplexTriDiagFact& A) { return A.condition(); }



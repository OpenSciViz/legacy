#if 0
/*
 * Generated from template $Id: xmth.cpp,v 1.2 1993/10/11 21:48:17 alv Exp $
 *
 * Math member functions for the matrix types
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */
#endif

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/ftrdgmat.h"

/*
 * This function returns a new matrix the same shape as the one 
 * passed in but with the data passed in.  This way we restrict
 * all the >if statements to a couple lines
 */
inline FloatTriDiagMat sameShapeMatrix( FloatVec& vec, const FloatTriDiagMat& A )
{
  return FloatTriDiagMat(vec,A.rows(),A.cols());
}


static void verifyMatch(const FloatTriDiagMat& A, const FloatTriDiagMat& B)
{
    if (A.rows()!=B.rows() || A.cols()!=B.cols())
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATSIZE,A.rows(),A.cols(),B.rows(),B.cols())));
}

RWBoolean FloatTriDiagMat::operator==(const FloatTriDiagMat& X)
{
    if (n!=X.n) return FALSE;
    // The following two statements cannot be combined into one
    // because of a cfront limitation:
    if ( n<1 ) return TRUE;
    return vec.slice(1,3*n-2)==X.vec.slice(1,3*n-2);
}


FloatTriDiagMat& FloatTriDiagMat::operator+=(const FloatTriDiagMat& m)
{
  verifyMatch(*this,m);
  vec += m.vec;
  return *this;
}

FloatTriDiagMat& FloatTriDiagMat::operator-=(const FloatTriDiagMat& m)
{
  verifyMatch(*this,m);
  vec -= m.vec;
  return *this;
}

FloatTriDiagMat& FloatTriDiagMat::operator*=(const FloatTriDiagMat& m)
{
  verifyMatch(*this,m);
  vec *= m.vec;
  return *this;
}

FloatTriDiagMat& FloatTriDiagMat::operator*=(Float x)
{
  vec *= x;
  return *this;
}

FloatTriDiagMat& FloatTriDiagMat::operator/=(const FloatTriDiagMat& m)
{
  verifyMatch(*this,m);
  vec /= m.vec;
  return *this;
}

FloatTriDiagMat& FloatTriDiagMat::operator/=(Float x)
{
  vec /= x;
  return *this;
}




FloatTriDiagMat operator-(const FloatTriDiagMat& A)
{
  FloatVec temp(-(A.dataVec()));
  return sameShapeMatrix(temp,A);
}
  
FloatTriDiagMat operator+(const FloatTriDiagMat& A)
{
  return A;
}

FloatTriDiagMat operator*(const FloatTriDiagMat& A, const FloatTriDiagMat& B) {
	verifyMatch(A,B);
	FloatVec temp(A.dataVec()*B.dataVec());
	return sameShapeMatrix(temp,A);
}

FloatTriDiagMat operator/(const FloatTriDiagMat& A, const FloatTriDiagMat& B) {
	verifyMatch(A,B);
	FloatVec temp(A.dataVec()/B.dataVec());
	return sameShapeMatrix(temp,A);
}

FloatTriDiagMat operator+(const FloatTriDiagMat& A, const FloatTriDiagMat& B) {
	verifyMatch(A,B);
	FloatVec temp(A.dataVec()+B.dataVec());
	return sameShapeMatrix(temp,A);
}

FloatTriDiagMat operator-(const FloatTriDiagMat& A, const FloatTriDiagMat& B) {
	verifyMatch(A,B);
	FloatVec temp(A.dataVec()-B.dataVec());
	return sameShapeMatrix(temp,A);
}

FloatTriDiagMat operator*(const FloatTriDiagMat& A, Float x) {
	FloatVec temp(A.dataVec()*x);
	return sameShapeMatrix(temp,A);
}

/*
 * Out-of-line version for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
  FloatTriDiagMat operator*(Float x, const FloatTriDiagMat& A) { return A*x; }
#endif

FloatTriDiagMat operator/(const FloatTriDiagMat& A, Float x) {
	FloatVec temp(A.dataVec()/x);
	return sameShapeMatrix(temp,A);
}





FloatTriDiagMat abs(const FloatTriDiagMat& A) {
	FloatVec temp(abs(A.dataVec()));
	return sameShapeMatrix(temp,A);
}



FloatTriDiagMat transpose(const FloatTriDiagMat& A)
{
  FloatTriDiagMat T(A.rows(),A.cols());
  T.diagonal(-1) = A.diagonal(1);
  T.diagonal(0) = A.diagonal(0);
  T.diagonal(1) = A.diagonal(-1);
  return T;
}

FloatVec product(const FloatTriDiagMat& A, const FloatVec& x)
{
    if (A.cols()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATVECPROD,A.className,A.rows(),A.cols(),x.length())));
    }
    FloatVec y(A.rows(),rwUninitialized);
    y = A.diagonal(0) * x;
    y.slice(1,x.length()-1) += A.diagonal(-1) * x.slice(0,x.length()-1);
    y.slice(0,x.length()-1) += A.diagonal(1) * x.slice(1,x.length()-1);
    return y;
}

FloatVec product(const FloatVec& x, const FloatTriDiagMat& A)
{
    if (A.rows()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECMATPROD,x.length(),A.className,A.rows(),A.cols())));
    }
    FloatVec y(A.cols(),rwUninitialized);
    y = A.diagonal(0) * x;
    y.slice(1,x.length()-1) += A.diagonal(1) * x.slice(0,x.length()-1);
    y.slice(0,x.length()-1) += A.diagonal(-1) * x.slice(1,x.length()-1);
    return y;
}



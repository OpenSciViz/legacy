/*
 * The conversion functions to/from general matrices.
 *
 * Generated from template $Id: xcg.cpp,v 1.2 1993/07/05 19:19:33 alv Exp $
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/chermmat.h"
#include "rw/cgenmat.h"

DComplexGenMat::DComplexGenMat( const DComplexHermMat& A )
  : RWMatView(A.rows(),A.cols(),sizeof(DComplex))
{
  for( int j=0; j<A.rows(); j++ ) {
    for( register int i=0; i<=j; i++ ) {
      DComplex x = A.val(i,j);
      set(j,i,conj(x));
      set(i,j,x);
    }
  }
}

DComplexHermMat toHermMat( const DComplexGenMat& S )
{
  int n = S.cols();
  DComplexHermMat A(S.rows(),n);
  for( int j=0; j<n; j++ ) {
    for( int i=0; i<j; i++ ) {
      // Using set in the following generates an inline too complicated
      // for the C compiler on the DEC (ie cfront spits out something
      // too hard to understand).
      // A.set(i,j,(S.val(i,j)+conj(S.val(j,i)))/(DComplex)2);
#ifdef RW_HIGHC_INLINE_BUG
      DComplex temp1 = S.val(i,j);
      DComplex temp1 = S.val(j,i);
      A.bcset(i,j,(temp1+conj(temp2))/DComplex(2,0));
#else
      A.bcset(i,j,(S.val(i,j)+conj(S.val(j,i)))/DComplex(2,0));
#endif
    }
    A.bcset(j,j,S.val(j,j));   // We don't force imaginary part to zero
  }
  return A;
}

DComplexHermMat upperToHermMat( const DComplexGenMat& S )
{
  unsigned n = S.cols();
  DComplexHermMat A(n,n);
  for(int j=0; j<n; j++) {
    for(int i=0; i<=j; i++) {                     // This loop should be in a bla call for nonsmall j
      A.set(i,j,S.val(i,j));
    }
  }
  return A;
}

DComplexHermMat lowerToHermMat( const DComplexGenMat& S )
{
  unsigned n = S.cols();
  DComplexHermMat A(n,n);
  for(int j=0; j<n; j++) {
    for(int i=j; i<n; i++) {                     // This loop should be in a bla call for nonsmall j
      A.set(i,j,S.val(i,j));
    }
  }
  return A;
}


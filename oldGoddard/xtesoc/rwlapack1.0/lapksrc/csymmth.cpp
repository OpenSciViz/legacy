#if 0
/*
 * Generated from template $Id: xmth.cpp,v 1.2 1993/10/11 21:48:17 alv Exp $
 *
 * Math member functions for the matrix types
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */
#endif

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/csymmat.h"
#include "rw/dsymmat.h"

/*
 * This function returns a new matrix the same shape as the one 
 * passed in but with the data passed in.  This way we restrict
 * all the >if statements to a couple lines
 */
inline DComplexSymMat sameShapeMatrix( DComplexVec& vec, const DComplexSymMat& A )
{
  return DComplexSymMat(vec,A.rows(),A.cols());
}

inline DoubleSymMat sameShapeRealMatrix( DoubleVec& vec, const DComplexSymMat& A )
{
  return DoubleSymMat(vec,A.rows(),A.cols());
}

static void verifyMatch(const DComplexSymMat& A, const DComplexSymMat& B)
{
    if (A.rows()!=B.rows() || A.cols()!=B.cols())
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATSIZE,A.rows(),A.cols(),B.rows(),B.cols())));
}

RWBoolean DComplexSymMat::operator==(const DComplexSymMat& X)
{
    if (n!=X.n) return FALSE;
    return vec==X.vec;	// All elements in vec used in the matrix
}

DComplexSymMat& DComplexSymMat::operator+=(DComplex x)
{
  vec += x;
  return *this;
}

DComplexSymMat& DComplexSymMat::operator-=(DComplex x)
{
  vec -= x;
  return *this;
}

DComplexSymMat& DComplexSymMat::operator+=(const DComplexSymMat& m)
{
  verifyMatch(*this,m);
  vec += m.vec;
  return *this;
}

DComplexSymMat& DComplexSymMat::operator-=(const DComplexSymMat& m)
{
  verifyMatch(*this,m);
  vec -= m.vec;
  return *this;
}

DComplexSymMat& DComplexSymMat::operator*=(const DComplexSymMat& m)
{
  verifyMatch(*this,m);
  vec *= m.vec;
  return *this;
}

DComplexSymMat& DComplexSymMat::operator*=(DComplex x)
{
  vec *= x;
  return *this;
}

DComplexSymMat& DComplexSymMat::operator/=(const DComplexSymMat& m)
{
  verifyMatch(*this,m);
  vec /= m.vec;
  return *this;
}

DComplexSymMat& DComplexSymMat::operator/=(DComplex x)
{
  vec /= x;
  return *this;
}



DComplexSymMat DComplexSymMat::apply(CmathFunTy f) const
{
    DComplexVec temp(vec.apply(f));
    return sameShapeMatrix(temp,*this);
}

DoubleSymMat DComplexSymMat::apply2(CmathFunTy2 f) const
{
    DoubleVec temp(vec.apply2(f));
    return sameShapeRealMatrix(temp,*this);
}


DComplexSymMat operator-(const DComplexSymMat& A)
{
  DComplexVec temp(-(A.dataVec()));
  return sameShapeMatrix(temp,A);
}
  
DComplexSymMat operator+(const DComplexSymMat& A)
{
  return A;
}

DComplexSymMat operator*(const DComplexSymMat& A, const DComplexSymMat& B) {
	verifyMatch(A,B);
	DComplexVec temp(A.dataVec()*B.dataVec());
	return sameShapeMatrix(temp,A);
}

DComplexSymMat operator/(const DComplexSymMat& A, const DComplexSymMat& B) {
	verifyMatch(A,B);
	DComplexVec temp(A.dataVec()/B.dataVec());
	return sameShapeMatrix(temp,A);
}

DComplexSymMat operator+(const DComplexSymMat& A, const DComplexSymMat& B) {
	verifyMatch(A,B);
	DComplexVec temp(A.dataVec()+B.dataVec());
	return sameShapeMatrix(temp,A);
}

DComplexSymMat operator-(const DComplexSymMat& A, const DComplexSymMat& B) {
	verifyMatch(A,B);
	DComplexVec temp(A.dataVec()-B.dataVec());
	return sameShapeMatrix(temp,A);
}

DComplexSymMat operator*(const DComplexSymMat& A, DComplex x) {
	DComplexVec temp(A.dataVec()*x);
	return sameShapeMatrix(temp,A);
}

/*
 * Out-of-line version for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
  DComplexSymMat operator*(DComplex x, const DComplexSymMat& A) { return A*x; }
#endif

DComplexSymMat operator/(const DComplexSymMat& A, DComplex x) {
	DComplexVec temp(A.dataVec()/x);
	return sameShapeMatrix(temp,A);
}


DComplexSymMat operator/(DComplex x, const DComplexSymMat& A) {
	DComplexVec temp(x/A.dataVec());
	return sameShapeMatrix(temp,A);
}

DComplexSymMat operator+(const DComplexSymMat& A, DComplex x) {
	DComplexVec temp(A.dataVec()+x);
	return sameShapeMatrix(temp,A);
}
/*
 * Out-of-line versions for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
DComplexSymMat operator+(DComplex x, const DComplexSymMat& A) { return A+x; }
DComplexSymMat operator-(const DComplexSymMat& A, DComplex x) { return A+(-x); }
#endif
DComplexSymMat operator-(DComplex x, const DComplexSymMat& A) {
	DComplexVec temp(x-A.dataVec());
	return sameShapeMatrix(temp,A);
}


DoubleSymMat abs(const DComplexSymMat& A) {
	DoubleVec temp(abs(A.dataVec()));
	return sameShapeRealMatrix(temp,A);
}


DComplexSymMat conj(const DComplexSymMat& A) {
	DComplexVec temp(conj(A.dataVec()));
	return sameShapeMatrix(temp,A);
}

DoubleSymMat real(const DComplexSymMat& A) {
	DoubleVec temp(real(A.dataVec()));
	return sameShapeRealMatrix(temp,A);
}

DoubleSymMat imag(const DComplexSymMat& A) {
	DoubleVec temp(imag(A.dataVec()));
	return sameShapeRealMatrix(temp,A);
}

DoubleSymMat norm(const DComplexSymMat& A) {
	DoubleVec temp(norm(A.dataVec()));
	return sameShapeRealMatrix(temp,A);
}

DoubleSymMat arg(const DComplexSymMat& A) {
	DoubleVec temp(arg(A.dataVec()));
	return sameShapeRealMatrix(temp,A);
}



DComplexSymMat transpose(const DComplexSymMat& A) { return A; }

DComplexVec product(const DComplexSymMat& A, const DComplexVec& x)
{
    if (A.cols()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATVECPROD,A.className,A.rows(),A.cols(),x.length())));
    }
    DComplexVec y(A.rows(),rwUninitialized);
    const DComplexVec& v = A.dataVec();
    int n=A.rows();
    for( int i=0; i<n; i++ ) {
      y(i) = dot( v.slice(i*(i+1)/2,i+1,1), x.slice(0,i+1,1) );
      DComplex* index = (DComplex*)v.data() + ((i+2)*(i+3)/2-2);
      for( int k=i+1; k<n; index+=(++k) ) {
	y(i) += (*index)*x(k);
      }
    }
    return y;
}

DComplexVec product(const DComplexVec& x, const DComplexSymMat& A)
{
    if (A.rows()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECMATPROD,x.length(),A.className,A.rows(),A.cols())));
    }
    return product(A,x);
}



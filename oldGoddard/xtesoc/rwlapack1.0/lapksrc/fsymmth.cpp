#if 0
/*
 * Generated from template $Id: xmth.cpp,v 1.2 1993/10/11 21:48:17 alv Exp $
 *
 * Math member functions for the matrix types
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */
#endif

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/fsymmat.h"

/*
 * This function returns a new matrix the same shape as the one 
 * passed in but with the data passed in.  This way we restrict
 * all the >if statements to a couple lines
 */
inline FloatSymMat sameShapeMatrix( FloatVec& vec, const FloatSymMat& A )
{
  return FloatSymMat(vec,A.rows(),A.cols());
}


static void verifyMatch(const FloatSymMat& A, const FloatSymMat& B)
{
    if (A.rows()!=B.rows() || A.cols()!=B.cols())
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATSIZE,A.rows(),A.cols(),B.rows(),B.cols())));
}

RWBoolean FloatSymMat::operator==(const FloatSymMat& X)
{
    if (n!=X.n) return FALSE;
    return vec==X.vec;	// All elements in vec used in the matrix
}

FloatSymMat& FloatSymMat::operator+=(Float x)
{
  vec += x;
  return *this;
}

FloatSymMat& FloatSymMat::operator-=(Float x)
{
  vec -= x;
  return *this;
}

FloatSymMat& FloatSymMat::operator+=(const FloatSymMat& m)
{
  verifyMatch(*this,m);
  vec += m.vec;
  return *this;
}

FloatSymMat& FloatSymMat::operator-=(const FloatSymMat& m)
{
  verifyMatch(*this,m);
  vec -= m.vec;
  return *this;
}

FloatSymMat& FloatSymMat::operator*=(const FloatSymMat& m)
{
  verifyMatch(*this,m);
  vec *= m.vec;
  return *this;
}

FloatSymMat& FloatSymMat::operator*=(Float x)
{
  vec *= x;
  return *this;
}

FloatSymMat& FloatSymMat::operator/=(const FloatSymMat& m)
{
  verifyMatch(*this,m);
  vec /= m.vec;
  return *this;
}

FloatSymMat& FloatSymMat::operator/=(Float x)
{
  vec /= x;
  return *this;
}


FloatSymMat& FloatSymMat::operator++()
{
  ++vec; return *this;
}

#ifndef RW_NO_POSTFIX
void FloatSymMat::operator++(int)	/* Postfix operator++ */
{
  vec++;
}
#endif

FloatSymMat& FloatSymMat::operator--()
{
  --vec; return *this;
}

#ifndef RW_NO_POSTFIX
void FloatSymMat::operator--(int)	/* Postfix operator-- */
{
  vec--;
}
#endif


FloatSymMat FloatSymMat::apply(mathFunTy f) const
{
    FloatVec temp(vec.apply(f));
    return sameShapeMatrix(temp,*this);
}


FloatSymMat operator-(const FloatSymMat& A)
{
  FloatVec temp(-(A.dataVec()));
  return sameShapeMatrix(temp,A);
}
  
FloatSymMat operator+(const FloatSymMat& A)
{
  return A;
}

FloatSymMat operator*(const FloatSymMat& A, const FloatSymMat& B) {
	verifyMatch(A,B);
	FloatVec temp(A.dataVec()*B.dataVec());
	return sameShapeMatrix(temp,A);
}

FloatSymMat operator/(const FloatSymMat& A, const FloatSymMat& B) {
	verifyMatch(A,B);
	FloatVec temp(A.dataVec()/B.dataVec());
	return sameShapeMatrix(temp,A);
}

FloatSymMat operator+(const FloatSymMat& A, const FloatSymMat& B) {
	verifyMatch(A,B);
	FloatVec temp(A.dataVec()+B.dataVec());
	return sameShapeMatrix(temp,A);
}

FloatSymMat operator-(const FloatSymMat& A, const FloatSymMat& B) {
	verifyMatch(A,B);
	FloatVec temp(A.dataVec()-B.dataVec());
	return sameShapeMatrix(temp,A);
}

FloatSymMat operator*(const FloatSymMat& A, Float x) {
	FloatVec temp(A.dataVec()*x);
	return sameShapeMatrix(temp,A);
}

/*
 * Out-of-line version for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
  FloatSymMat operator*(Float x, const FloatSymMat& A) { return A*x; }
#endif

FloatSymMat operator/(const FloatSymMat& A, Float x) {
	FloatVec temp(A.dataVec()/x);
	return sameShapeMatrix(temp,A);
}


FloatSymMat operator/(Float x, const FloatSymMat& A) {
	FloatVec temp(x/A.dataVec());
	return sameShapeMatrix(temp,A);
}

FloatSymMat operator+(const FloatSymMat& A, Float x) {
	FloatVec temp(A.dataVec()+x);
	return sameShapeMatrix(temp,A);
}
/*
 * Out-of-line versions for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
FloatSymMat operator+(Float x, const FloatSymMat& A) { return A+x; }
FloatSymMat operator-(const FloatSymMat& A, Float x) { return A+(-x); }
#endif
FloatSymMat operator-(Float x, const FloatSymMat& A) {
	FloatVec temp(x-A.dataVec());
	return sameShapeMatrix(temp,A);
}

FloatSymMat atan2(const FloatSymMat& A, const FloatSymMat& B) {
	FloatVec temp(atan2(A.dataVec(),B.dataVec()));
	return sameShapeMatrix(temp,A);
}

FloatSymMat abs(const FloatSymMat& A) {
	FloatVec temp(abs(A.dataVec()));
	return sameShapeMatrix(temp,A);
}



FloatSymMat transpose(const FloatSymMat& A) { return A; }

FloatVec product(const FloatSymMat& A, const FloatVec& x)
{
    if (A.cols()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATVECPROD,A.className,A.rows(),A.cols(),x.length())));
    }
    FloatVec y(A.rows(),rwUninitialized);
    const FloatVec& v = A.dataVec();
    int n=A.rows();
    for( int i=0; i<n; i++ ) {
      y(i) = dot( v.slice(i*(i+1)/2,i+1,1), x.slice(0,i+1,1) );
      Float* index = (Float*)v.data() + ((i+2)*(i+3)/2-2);
      for( int k=i+1; k<n; index+=(++k) ) {
	y(i) += (*index)*x(k);
      }
    }
    return y;
}

FloatVec product(const FloatVec& x, const FloatSymMat& A)
{
    if (A.rows()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECMATPROD,x.length(),A.className,A.rows(),A.cols())));
    }
    return product(A,x);
}



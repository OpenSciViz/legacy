/*
 * Implementation of DComplexHermDenseTriDiagDecomp
 *
 * Generated from template $Id: xdensetd.cpp,v 1.4 1993/10/11 21:48:08 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */ 

#include "rw/ctd.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/lapack.h"
#include "rw/chermmat.h"

DComplexHermDenseTriDiagDecomp::DComplexHermDenseTriDiagDecomp(const DComplexHermMat& A)
{
  if (A.rows()>0) {
    char uplo = 'U';   // Indicates to lapack that upper triangle is stored
    long n = A.rows();
    Qdata.reference(A.dataVec());
    Qdata.deepenShallowCopy();
    DoubleVec D(A.rows(),rwUninitialized);
    DoubleVec E(A.rows()-1,rwUninitialized);
    tau.reshape(A.rows());
    long info;
    zhptrd(uplo,n,(DComplex*)Qdata.data(),(Double*)D.data(),(Double*)E.data(),(DComplex*)tau.data(),info);
    RWPOSTCONDITION(info==0);
    set(D,E);
  }
}

DComplexVec DComplexHermDenseTriDiagDecomp::transform(const DoubleVec& V) const
{
  return DComplexHermTriDiagDecomp::transform(V);
}

DComplexGenMat DComplexHermDenseTriDiagDecomp::transform(const DoubleGenMat& Cinput) const
{
  DComplexGenMat C = Cinput.copy();
  C.deepenShallowCopy();
  if (C.rows()>0) {
    if (C.rows()!=rows()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,C.rows(),rows())));
    }
    char side = 'L';
    char uplo = 'U';
    char trans = 'N';
    long m = C.rows();
    long n = C.cols();
    DComplex *work = new DComplex[n];
    long info;
    zupmtr(side,uplo,trans,m,n,(DComplex*)Qdata.data(),(DComplex*)tau.data(),(DComplex*)C.data(),m,work,info);
    RWPOSTCONDITION(info==0);
    delete [] work;
  }
  return C;
}


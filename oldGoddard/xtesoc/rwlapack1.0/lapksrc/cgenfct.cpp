/*
 * Implementation of DComplexGenFact
 *
 * Generated from template $Id: xfct.cpp,v 1.6 1993/10/11 21:48:11 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/lapkdefs.h"
#include "rw/cgenfct.h"
#include "rw/lapack.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"  
#include "rw/cgenmat.h"

const char* DComplexGenFact::className = "DComplexGenFact";

DComplexGenFact::~DComplexGenFact()
{
  delete [] pvts;
}

DComplexGenFact::DComplexGenFact()
{
  info = -1;
  pvts=0;
}

DComplexGenFact::DComplexGenFact( const DComplexGenFact& A )
  : factorization(A.factorization)
{
  info = A.info;
  Anorm = A.Anorm;
  unsigned n = A.rows();
  pvts = (n>0) ? new long [n] : 0;
  while(n--) {
    pvts[n] = A.pvts[n];
  }
}

DComplexGenFact::DComplexGenFact(const DComplexGenMat& A, RWBoolean estimateCondition)
  : factorization(A)  // Use reference semantics, followed by
                      // deepenShallowCopy(), to work around a BCC V3.0 bug
{
    factorization.deepenShallowCopy();
    pvts = 0;
    dofactor(estimateCondition);
}

void DComplexGenFact::factor( const DComplexGenMat& A, RWBoolean estimateCondition )
{
    DComplexGenMat Acopy = A.copy();
    factorization.reference(Acopy);
    dofactor(estimateCondition);
}

void DComplexGenFact::dofactor(RWBoolean estimateCondition)
{
   info=0;   // for size zero matrices, need to set this explicitly
   long n = rows();	

    // Error (sanity) checking
    RWPRECONDITION(n==factorization.cols());
    RWPRECONDITION(factorization.rowStride()==1 && factorization.colStride()==factorization.rows());

    delete [] pvts;
    pvts = (n>0) ? new long [n] : 0;

    // If we care to ever find the condition number, we need to know
    // the 1-norm of the matrix A.  Compute it while the member data
    // factorization is still a copy of A, and not yet it's factorization.
    Anorm = -1; 
    if (n>0 && estimateCondition) {
      Anorm = zlange('1',n,n,factorization.data(),n,0);
    }

    if (n>0) {
#if defined(RW_USE_UNBLOCKED)
      zgetf2(n,n,factorization.data(),n,pvts,info);
#else
      zgetrf(n,n,factorization.data(),n,pvts,info);
#endif
      RWPOSTCONDITION(info>=0);
    }
}

RWBoolean DComplexGenFact::fail() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}


RWBoolean DComplexGenFact::isSingular() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}

Double DComplexGenFact::condition() const
{
    Double Anormcopy = Anorm;      // Copy to stop warnings about ref parm
    long info;
    long n = rows();
    Double rcond;
    DComplex *work = new DComplex[4*n];   // Some routines only require 2*n, but what the hell
    Double *work2 = new Double [2*n];
    zgecon('1',n,(DComplex*)factorization.data(),n,Anormcopy,rcond,work,work2,info);
    delete [] work2;
    delete [] work;
    RWPOSTCONDITION(info==0);
    return rcond;
}

DComplexVec DComplexGenFact::solve( const DComplexVec& b ) const
{
  DComplexGenMat B(b,b.length(),1);
  DComplexGenMat X = solve(B);
  RWPOSTCONDITION(X.cols()==1);
  return X.col(0);
}

DComplexGenMat DComplexGenFact::solve( const DComplexGenMat& B ) const
{
    DComplexGenMat X(B.copy());
    long n = rows();
    if (B.rows()!=n) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,B.rows(),n)));
    if (fail()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTSOLVE)));
    long nrhs = X.cols();
    long info;
    if (n>0) {
    zgetrs('N',n,nrhs,(DComplex*)factorization.data(),n,pvts,X.data(),n,info);
      RWPOSTCONDITION(info==0);
    }
    return X;    
}

DComplex DComplexGenFact::determinant() const
{
    int numExchanges = 0;
    for(int i=rows(); i--;) {
      if (pvts[i]!=i+1) numExchanges++;
    }
    DComplex detLU = prod(factorization.diagonal());
    return (numExchanges%2) ? -detLU : detLU;
}
              
DComplexGenMat DComplexGenFact::inverse( ) const
{
    if (rows()==0) { return factorization; }
    if (fail()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTSOLVE)));
    DComplexGenMat soln(factorization);
    soln.deepenShallowCopy();
    long n = rows();
    long info;
    long nb = ilaenv( 1, "zgetri", " ", n, -1, -1, -1 );
    long lwork = n*nb;
    DComplex *work = new DComplex [lwork];
    zgetri(n,soln.data(),n,pvts,work,lwork,info);
    RWPOSTCONDITION(work[0]==lwork);
    delete [] work;
    RWPOSTCONDITION(info==0);
    return soln;
}


/*
 * Global functions which provide alternate ways to call member functions.
 * These used to be inline, but this requires that you include more header
 * files on some compilers, and that is just not worth it.
 */
DComplexVec solve(const DComplexGenFact& A, const DComplexVec& b) { return A.solve(b); }
DComplexGenMat solve(const DComplexGenFact& A, const DComplexGenMat& b) { return A.solve(b); }
DComplex    determinant (const DComplexGenFact& A)	      { return A.determinant(); }

DComplexGenMat inverse(const DComplexGenFact& A)      	      { return A.inverse(); }

Double condition(const DComplexGenFact& A) { return A.condition(); }



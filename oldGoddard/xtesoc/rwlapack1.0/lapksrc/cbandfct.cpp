/*
 * Implementation of DComplexBandFact
 *
 * Generated from template $Id: xfct.cpp,v 1.6 1993/10/11 21:48:11 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/lapkdefs.h"
#include "rw/cbandfct.h"
#include "rw/lapack.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"  
#include "rw/cgenmat.h"

const char* DComplexBandFact::className = "DComplexBandFact";

DComplexBandFact::~DComplexBandFact()
{
  delete [] pvts;
}

DComplexBandFact::DComplexBandFact()
{
  info = -1;
  pvts=0;
}

DComplexBandFact::DComplexBandFact( const DComplexBandFact& A )
  : factorization(A.factorization)
{
  info = A.info;
  Anorm = A.Anorm;
  unsigned n = A.rows();
  pvts = (n>0) ? new long [n] : 0;
  while(n--) {
    pvts[n] = A.pvts[n];
  }
}

DComplexBandFact::DComplexBandFact(const DComplexBandMat& A, RWBoolean estimateCondition)
  : factorization(A.rows(),A.cols(),A.lowerBandwidth(),A.lowerBandwidth()+A.upperBandwidth() )
{
    factorization = A;
    pvts = 0;
    dofactor(estimateCondition);
}

void DComplexBandFact::factor( const DComplexBandMat& A, RWBoolean estimateCondition )
{
    DComplexBandMat F(A.rows(),A.cols(),A.lowerBandwidth(),A.lowerBandwidth()+A.upperBandwidth());
    F = A;
    factorization.reference(F);
    dofactor(estimateCondition);
}

void DComplexBandFact::dofactor(RWBoolean estimateCondition)
{
   info=0;   // for size zero matrices, need to set this explicitly
   long n = rows();	

    // Error (sanity) checking
    RWPRECONDITION(factorization.dataVec().stride()==1);

    delete [] pvts;
    pvts = (n>0) ? new long [n] : 0;

    // If we care to ever find the condition number, we need to know
    // the 1-norm of the matrix A.  Compute it while the member data
    // factorization is still a copy of A, and not yet it's factorization.
    Anorm = -1; 
    if (n>0 && estimateCondition) {
      long kl = factorization.lowerBandwidth();
      long ku = factorization.upperBandwidth();
      long b = factorization.bandwidth();
      Anorm = zlangb('1',n,kl,ku,factorization.data(),b,0);
    }

    if (n>0) {
      long b = factorization.bandwidth();
      long l = factorization.lowerBandwidth();
      long u = factorization.upperBandwidth()-factorization.lowerBandwidth();
      zgbtrf(n,n,l,u,factorization.data(),b,pvts,info);
      RWPOSTCONDITION(info>=0);
    }
}

RWBoolean DComplexBandFact::fail() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}


RWBoolean DComplexBandFact::isSingular() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}

Double DComplexBandFact::condition() const
{
    Double Anormcopy = Anorm;      // Copy to stop warnings about ref parm
    long info;
    long n = rows();
    Double rcond;
    DComplex *work = new DComplex[4*n];   // Some routines only require 2*n, but what the hell
    long b = factorization.bandwidth();
    long l = factorization.lowerBandwidth();
    long u = factorization.upperBandwidth()-factorization.lowerBandwidth();
    Double *work2 = new Double [n];
    zgbcon('1',n,l,u,(DComplex*)factorization.data(),b,pvts,Anormcopy,rcond,work,work2,info);
    delete [] work2;
    delete [] work;
    RWPOSTCONDITION(info==0);
    return rcond;
}

DComplexVec DComplexBandFact::solve( const DComplexVec& b ) const
{
  DComplexGenMat B(b,b.length(),1);
  DComplexGenMat X = solve(B);
  RWPOSTCONDITION(X.cols()==1);
  return X.col(0);
}

DComplexGenMat DComplexBandFact::solve( const DComplexGenMat& B ) const
{
    DComplexGenMat X(B.copy());
    long n = rows();
    if (B.rows()!=n) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,B.rows(),n)));
    if (fail()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTSOLVE)));
    long nrhs = X.cols();
    long info;
    if (n>0) {
    long b = factorization.bandwidth();
    long l = factorization.lowerBandwidth();
    long u = factorization.upperBandwidth()-factorization.lowerBandwidth();
    zgbtrs('N',n,l,u,nrhs,(DComplex*)factorization.data(),b,pvts,X.data(),n,info);
      RWPOSTCONDITION(info==0);
    }
    return X;    
}

DComplex DComplexBandFact::determinant() const
{
    int numExchanges = 0;
    for(int i=rows(); i--;) {
      if (pvts[i]!=i+1) numExchanges++;
    }
    DComplex detLU = prod(factorization.diagonal());
    return (numExchanges%2) ? -detLU : detLU;
}
              

/*
 * Global functions which provide alternate ways to call member functions.
 * These used to be inline, but this requires that you include more header
 * files on some compilers, and that is just not worth it.
 */
DComplexVec solve(const DComplexBandFact& A, const DComplexVec& b) { return A.solve(b); }
DComplexGenMat solve(const DComplexBandFact& A, const DComplexGenMat& b) { return A.solve(b); }
DComplex    determinant (const DComplexBandFact& A)	      { return A.determinant(); }


Double condition(const DComplexBandFact& A) { return A.condition(); }



#if 0
/*
 * Generated from template $Id: xmth.cpp,v 1.2 1993/10/11 21:48:17 alv Exp $
 *
 * Math member functions for the matrix types
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */
#endif

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/cskewmat.h"
#include "rw/dsymmat.h"
#include "rw/dskewmat.h"
#include "rw/csymmat.h"

/*
 * This function returns a new matrix the same shape as the one 
 * passed in but with the data passed in.  This way we restrict
 * all the >if statements to a couple lines
 */
inline DComplexSkewMat sameShapeMatrix( DComplexVec& vec, const DComplexSkewMat& A )
{
  return DComplexSkewMat(vec,A.rows(),A.cols());
}

inline DoubleSkewMat sameShapeRealMatrix( DoubleVec& vec, const DComplexSkewMat& A )
{
  return DoubleSkewMat(vec,A.rows(),A.cols());
}

static void verifyMatch(const DComplexSkewMat& A, const DComplexSkewMat& B)
{
    if (A.rows()!=B.rows() || A.cols()!=B.cols())
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATSIZE,A.rows(),A.cols(),B.rows(),B.cols())));
}

RWBoolean DComplexSkewMat::operator==(const DComplexSkewMat& X)
{
    if (n!=X.n) return FALSE;
    return vec==X.vec;	// All elements in vec used in the matrix
}


DComplexSkewMat& DComplexSkewMat::operator+=(const DComplexSkewMat& m)
{
  verifyMatch(*this,m);
  vec += m.vec;
  return *this;
}

DComplexSkewMat& DComplexSkewMat::operator-=(const DComplexSkewMat& m)
{
  verifyMatch(*this,m);
  vec -= m.vec;
  return *this;
}

DComplexSkewMat& DComplexSkewMat::operator*=(const DComplexSkewMat& m)
{
  verifyMatch(*this,m);
  vec *= m.vec;
  return *this;
}

DComplexSkewMat& DComplexSkewMat::operator*=(DComplex x)
{
  vec *= x;
  return *this;
}

DComplexSkewMat& DComplexSkewMat::operator/=(const DComplexSkewMat& m)
{
  verifyMatch(*this,m);
  vec /= m.vec;
  return *this;
}

DComplexSkewMat& DComplexSkewMat::operator/=(DComplex x)
{
  vec /= x;
  return *this;
}




DComplexSkewMat operator-(const DComplexSkewMat& A)
{
  DComplexVec temp(-(A.dataVec()));
  return sameShapeMatrix(temp,A);
}
  
DComplexSkewMat operator+(const DComplexSkewMat& A)
{
  return A;
}

DComplexSkewMat operator*(const DComplexSkewMat& A, const DComplexSkewMat& B) {
	verifyMatch(A,B);
	DComplexVec temp(A.dataVec()*B.dataVec());
	return sameShapeMatrix(temp,A);
}

DComplexSkewMat operator/(const DComplexSkewMat& A, const DComplexSkewMat& B) {
	verifyMatch(A,B);
	DComplexVec temp(A.dataVec()/B.dataVec());
	return sameShapeMatrix(temp,A);
}

DComplexSkewMat operator+(const DComplexSkewMat& A, const DComplexSkewMat& B) {
	verifyMatch(A,B);
	DComplexVec temp(A.dataVec()+B.dataVec());
	return sameShapeMatrix(temp,A);
}

DComplexSkewMat operator-(const DComplexSkewMat& A, const DComplexSkewMat& B) {
	verifyMatch(A,B);
	DComplexVec temp(A.dataVec()-B.dataVec());
	return sameShapeMatrix(temp,A);
}

DComplexSkewMat operator*(const DComplexSkewMat& A, DComplex x) {
	DComplexVec temp(A.dataVec()*x);
	return sameShapeMatrix(temp,A);
}

/*
 * Out-of-line version for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
  DComplexSkewMat operator*(DComplex x, const DComplexSkewMat& A) { return A*x; }
#endif

DComplexSkewMat operator/(const DComplexSkewMat& A, DComplex x) {
	DComplexVec temp(A.dataVec()/x);
	return sameShapeMatrix(temp,A);
}


DComplexSkewMat operator/(DComplex x, const DComplexSkewMat& A) {
	DComplexVec temp(x/A.dataVec());
	return sameShapeMatrix(temp,A);
}



DoubleSymMat abs(const DComplexSkewMat& A) {
	DoubleVec temp(abs(A.dataVec()));
	return DoubleSymMat(temp,A.rows(),A.cols());
}



/* Sorry, no conj or arg function for SkewMat */

DoubleSkewMat real(const DComplexSkewMat& A) {
	DoubleVec temp(real(A.dataVec()));
	return sameShapeRealMatrix(temp,A);
}

DoubleSkewMat imag(const DComplexSkewMat& A) {
	DoubleVec temp(imag(A.dataVec()));
	return sameShapeRealMatrix(temp,A);
}

DoubleSymMat norm(const DComplexSkewMat& A) {
	DoubleVec temp(norm(A.dataVec()));
	return DoubleSymMat(temp,A.rows(),A.cols());
}


DComplexSkewMat transpose(const DComplexSkewMat& A)
{
  DComplexSkewMat T(-A);
  DComplex* index = T.data();		// Fix up the diagonal
  for( int k=1; k<=T.rows(); index+=(++k) ) {
    (*index) *= DComplex(-1,0);	// Explicit constructor required for Zortech
  }
  return T;
}

DComplexVec product(const DComplexSkewMat& A, const DComplexVec& x)
{
    if (A.cols()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATVECPROD,A.className,A.rows(),A.cols(),x.length())));
    }
    DComplexVec y(A.rows(),rwUninitialized);
    const DComplexVec& v = A.dataVec();
    int n=A.rows();
    y(0) = DComplex(0,0);
    for( int i=0; i<n; i++ ) {
      if (i>0) {
        y(i) = -dot( v.slice(i*(i+1)/2,i,1), x.slice(0,i,1) );
      }
      DComplex* index = (DComplex*)v.data() + ((i+1)*(i+2)/2-1);
      for( int k=i; k<n; index+=(++k) ) {
	y(i) += (*index)*x(k);
      }
    }
    return y;
}

DComplexVec product(const DComplexVec& x, const DComplexSkewMat& A)
{
    if (A.rows()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECMATPROD,x.length(),A.className,A.rows(),A.cols())));
    }
    DComplexVec y(A.cols(),DComplex(0,0));
    const DComplexVec& v = A.dataVec();
    int n=A.rows();
    for( int i=0; i<n; i++ ) {
      y(i) = dot( v.slice(i*(i+1)/2,i+1,1), x.slice(0,i+1,1) );
      DComplex* index = (DComplex*)v.data() + ((i+2)*(i+3)/2-2);
      for( int k=i+1; k<n; index+=(++k) ) {
	y(i) -= (*index)*x(k);
      }
    }
    return y;
}



/*
 * Definitions for DoubleCODecomp
 *
 * Generated from template $Id: xco.cpp,v 1.4 1993/10/11 21:48:06 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 */

#include "rw/lapack.h"
#include "rw/dco.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/dutrimat.h"

DoubleCODecomp::DoubleCODecomp()
{
}

DoubleCODecomp::DoubleCODecomp(const DoubleCODecomp& x)
{
  *this = x;
}

void DoubleCODecomp::operator=(const DoubleCODecomp& x)
{
  QRdecomp_ = x.QRdecomp_;
  Ztau_.reference(x.Ztau_);
  TZ_.reference(x.TZ_);
}

void DoubleCODecomp::dofactor(double tol)
{
  long n = cols();
  int m = (rows()<cols()) ? rows() : cols();
  while (m>0 && fabs(QRdecomp_.QR_(m-1,m-1))<tol) m--;
  TZ_.reference(QRdecomp_.QR_(RWSlice(0,m),RWAll));
  Ztau_.reshape(m);
  long info;
  dtzrqf(m,n,TZ_.data(),TZ_.colStride(),Ztau_.data(),info);
  RWPOSTCONDITION(info==0);
}

DoubleCODecomp::DoubleCODecomp(const DoubleGenMat& A, double tol)
  : QRdecomp_(A)
{
  dofactor(tol);   
}

DoubleCODecomp::DoubleCODecomp(const DoubleQRDecomp& A, double tol)
  : QRdecomp_(A)
{
  QRdecomp_.QR_.deepenShallowCopy();
  dofactor(tol);   
}

void DoubleCODecomp::factor(const DoubleGenMat& A, double tol)
{
  QRdecomp_.factor(A);
  dofactor(tol);   
}

void DoubleCODecomp::factor(const DoubleQRDecomp& A, double tol)
{
  QRdecomp_ = A;
  QRdecomp_.QR_.deepenShallowCopy();
  dofactor(tol);   
}

DoubleCODecomp::~DoubleCODecomp()
{
}

DoubleUpperTriMat DoubleCODecomp::T() const
{
  return toUpperTriMat(TZ_.operator()(RWAll,RWSlice(0,rank())));  // explicit op() to shut up sun 3.0.1 warning
}

// Generate Z as Z = I P1 P2 P3 ... Pn
//
DoubleGenMat      DoubleCODecomp::Z() const
{
  DoubleGenMat Zmat(cols(),cols(),(Double)0);  // initialize Z to identity matrix
  Zmat.diagonal() = (Double)1;
  if (cols()>rank()) {
    long m = cols();
    long n = m-rank()+1;
    Double *work = new Double [m];
    DoubleGenMat ZmatRight = Zmat(RWAll,RWToEnd(rank()));
    DoubleGenMat TZRight = TZ_.operator()(RWAll,RWToEnd(rank()));  // explicit op() to shut up sun 3.0.1 warning
    Double *c2 = ZmatRight.data();
    long ldc = Zmat.colStride();
    long incv = TZRight.colStride();
    for(int i=rank(); i--;) {
      Double *v = TZRight.row(i).data();
      Double *c1 = Zmat.col(i).data();
      dlatzm('R',m,n,v,incv,Ztau_(i),c1,c2,ldc,work);
    }
    delete [] work;
  }
  return Zmat;   
}

DoubleVec DoubleCODecomp::Tx(const DoubleVec& x) const
{
  if (x.length()!=rank()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),rank())));
  if (cols()>rank()) {
    DoubleUpperTriMat T = toUpperTriMat(TZ_.operator()(RWAll,RWSlice(0,rank())));  // explicit op() to shut up sun 3.0.1 warning
    return product(T,x);
  } else {    // T==R
    return QRdecomp_.Rx(x);
  }
}

DoubleVec DoubleCODecomp::TTx(const DoubleVec& x) const
{
  if (x.length()!=rank()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),rank())));
  if (cols()>rank()) {
    DoubleUpperTriMat T = toUpperTriMat(TZ_.operator()(RWAll,RWSlice(0,rank())));  // explicit op() to shut up sun 3.0.1 warning
    return product(x,T);
  } else {    // T==R
    return QRdecomp_.RTx(x);
  }
}

DoubleVec DoubleCODecomp::Tinvx(const DoubleVec& x) const
{
  if (x.length()!=rank()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),rank())));
  DoubleVec y = x.copy();
  long n = rank();
  long lda = TZ_.colStride();
  long one = 1;
  long info;
  dtrtrs('U','N','N',n,one,(Double*)TZ_.data(),lda,y.data(),n,info);
  RWPOSTCONDITION(info==0);
  return y;
}

DoubleVec DoubleCODecomp::TTinvx(const DoubleVec& x) const
{
  if (x.length()!=rank()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),rank())));
  DoubleVec y = x.copy();
  long n = rank();
  long lda = TZ_.colStride();
  long one = 1;
  long info;
  dtrtrs('U','T','N',n,one,(Double*)TZ_.data(),lda,y.data(),n,info);
  RWPOSTCONDITION(info==0);
  return y;
}

DoubleVec DoubleCODecomp::Zx(const DoubleVec& x) const
{
  if (x.length()!=cols()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),cols())));  
  DoubleVec y = x.copy();
  if (cols()>rank()) {
    long n = 1;
    long m = cols()-rank()+1;
    Double work[1];
    DoubleGenMat TZRight = TZ_.operator()(RWAll,RWToEnd(rank()));  // explicit op() to shut up sun 3.0.1 warning
    Double *c2 = y.data() + rank();
    long ldc = y.length();
    long incv = TZRight.colStride();
    for(int i=0; i<rank(); i++) {
      Double *c1 = &(y(i));
      Double *v = TZRight.row(i).data();
      dlatzm('L',m,n,v,incv,Ztau_(i),c1,c2,ldc,work);
    }
  }
  return y;   
}

DoubleVec DoubleCODecomp::ZTx(const DoubleVec& x) const
{
  if (x.length()!=cols()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,x.length(),cols())));  
  DoubleVec y = x.copy();
  if (cols()>rank()) {
    long m = 1;
    long n = cols()-rank()+1;
    Double work[1];
    DoubleGenMat TZRight = TZ_.operator()(RWAll,RWToEnd(rank()));  // explicit op() to shut up sun 3.0.1 warning
    Double *c2 = y.data() + rank();
    long ldc = 1;
    long incv = TZRight.colStride();
    for(int i=rank(); i--;) {
      Double *c1 = &(y(i));
      Double *v = TZRight.row(i).data();
      dlatzm('R',m,n,v,incv,Ztau_(i),c1,c2,ldc,work);
    }
  }
  return y;
}


> # $Header: /users/rcs/lapksrc/xseigqr.cpp,v 1.4 1993/10/11 21:48:22 alv Exp $
> #                                                               #
> # $Log: xseigqr.cpp,v $
> # Revision 1.4  1993/10/11  21:48:22  alv
> # synch'ed with Math.h++ v5.1
> #
> # Revision 1.3  1993/07/05  19:19:33  alv
> # fixed up for complex numbers
> #
> # Revision 1.2  1993/04/06  20:45:04  alv
> # ported to sun C++ v3.0.1
> #
> # Revision 1.1  1993/04/06  15:44:42  alv
> # Initial revision
> #
>
> define <T>=RW_T_RW
> 
> if <T>==Double
> define <P>=Double <tLetter>=d <Sym>=Sym <sym>=sym <sLetter>=s <steqr>=dsteqr
> elif <T>==Float
> define <P>=Float <tLetter>=f <Sym>=Sym <sym>=sym <sLetter>=s <steqr>=ssteqr
> elif <T>==DComplex
> define <P>=Double <tLetter>=c <Sym>=Herm <sym>=herm <sLetter>=h <steqr>=dsteqr <pLetter>=d
> else
> err Symmetric Eigen-decomposition: not defined for '<T>'
> endif
>
/*
 * Implementation of <T><Sym>QREigServer
 *
 * Generated from template $Id: xseigqr.cpp,v 1.4 1993/10/11 21:48:22 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * Uses Lapack's QR/QL algorithm to compute eigenvalues/vectors of
 * a tridiagonal matrix.
 */ 

#include "rw/<tLetter><sLetter>eigsrv.h"
#include "rw/<tLetter>td.h"
#include "rw/lapack.h"
> if <P>!=<T>
#include "rw/<pLetter>seigsrv.h"
> endif


<T><Sym>QREigServer::<T><Sym>QREigServer(RWBoolean vecs)
{
  computeVecs_ = vecs;
}

<P>SymEigDecomp <T><Sym>QREigServer::decompose(const <T><Sym>TriDiagDecomp& decomp)
{
  char compz = computeVecs_ ? 'I' : 'N';
  long n = decomp.rows();
  <P>Vec d = decomp.diagonal().copy();
  <P>Vec e(decomp.rows(),rwUninitialized);   // lapack needs a length n (not n-1) off diagonal
  if (n>0) e(RWSlice(0,decomp.rows()-1)) = decomp.offDiagonal();
  int zsize = computeVecs_ ? decomp.rows() : 0;
  <P>GenMat Z(zsize,zsize,rwUninitialized);
  long ldz = computeVecs_ ? n : 1;
  <P> *work = computeVecs_ ? new <P> [2*n+1] : 0;  // Needs only 2*n-2, but this way we avoid <0 checks
  long info;
  <steqr>(compz,n,d.data(),e.data(),Z.data(),ldz,work,info);
  delete work;
  RWPOSTCONDITION(info>=0);

  <P>SymEigDecomp eig;
  <T><Sym>QREigServer::n(eig) = decomp.rows();
  lambda(eig).reference(d);
  P(eig).reference(Z);
  if (info>0) {
    unsigned numComputed = (unsigned)info;   // Make an explicit unsigned to avoid warnings on SUN
    lambda(eig).resize(numComputed);
    P(eig).resize(decomp.rows(),numComputed);
    computedAll(eig) = FALSE;
  }
  return eig;
}

RWBoolean <T><Sym>QREigServer::computeEigenVectors() const {return computeVecs_;}

void <T><Sym>QREigServer::computeEigenVectors(RWBoolean x) {computeVecs_=x;}


  

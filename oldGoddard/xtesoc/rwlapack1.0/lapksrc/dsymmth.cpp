#if 0
/*
 * Generated from template $Id: xmth.cpp,v 1.2 1993/10/11 21:48:17 alv Exp $
 *
 * Math member functions for the matrix types
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */
#endif

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/dsymmat.h"

/*
 * This function returns a new matrix the same shape as the one 
 * passed in but with the data passed in.  This way we restrict
 * all the >if statements to a couple lines
 */
inline DoubleSymMat sameShapeMatrix( DoubleVec& vec, const DoubleSymMat& A )
{
  return DoubleSymMat(vec,A.rows(),A.cols());
}


static void verifyMatch(const DoubleSymMat& A, const DoubleSymMat& B)
{
    if (A.rows()!=B.rows() || A.cols()!=B.cols())
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATSIZE,A.rows(),A.cols(),B.rows(),B.cols())));
}

RWBoolean DoubleSymMat::operator==(const DoubleSymMat& X)
{
    if (n!=X.n) return FALSE;
    return vec==X.vec;	// All elements in vec used in the matrix
}

DoubleSymMat& DoubleSymMat::operator+=(Double x)
{
  vec += x;
  return *this;
}

DoubleSymMat& DoubleSymMat::operator-=(Double x)
{
  vec -= x;
  return *this;
}

DoubleSymMat& DoubleSymMat::operator+=(const DoubleSymMat& m)
{
  verifyMatch(*this,m);
  vec += m.vec;
  return *this;
}

DoubleSymMat& DoubleSymMat::operator-=(const DoubleSymMat& m)
{
  verifyMatch(*this,m);
  vec -= m.vec;
  return *this;
}

DoubleSymMat& DoubleSymMat::operator*=(const DoubleSymMat& m)
{
  verifyMatch(*this,m);
  vec *= m.vec;
  return *this;
}

DoubleSymMat& DoubleSymMat::operator*=(Double x)
{
  vec *= x;
  return *this;
}

DoubleSymMat& DoubleSymMat::operator/=(const DoubleSymMat& m)
{
  verifyMatch(*this,m);
  vec /= m.vec;
  return *this;
}

DoubleSymMat& DoubleSymMat::operator/=(Double x)
{
  vec /= x;
  return *this;
}


DoubleSymMat& DoubleSymMat::operator++()
{
  ++vec; return *this;
}

#ifndef RW_NO_POSTFIX
void DoubleSymMat::operator++(int)	/* Postfix operator++ */
{
  vec++;
}
#endif

DoubleSymMat& DoubleSymMat::operator--()
{
  --vec; return *this;
}

#ifndef RW_NO_POSTFIX
void DoubleSymMat::operator--(int)	/* Postfix operator-- */
{
  vec--;
}
#endif


DoubleSymMat DoubleSymMat::apply(mathFunTy f) const
{
    DoubleVec temp(vec.apply(f));
    return sameShapeMatrix(temp,*this);
}


DoubleSymMat operator-(const DoubleSymMat& A)
{
  DoubleVec temp(-(A.dataVec()));
  return sameShapeMatrix(temp,A);
}
  
DoubleSymMat operator+(const DoubleSymMat& A)
{
  return A;
}

DoubleSymMat operator*(const DoubleSymMat& A, const DoubleSymMat& B) {
	verifyMatch(A,B);
	DoubleVec temp(A.dataVec()*B.dataVec());
	return sameShapeMatrix(temp,A);
}

DoubleSymMat operator/(const DoubleSymMat& A, const DoubleSymMat& B) {
	verifyMatch(A,B);
	DoubleVec temp(A.dataVec()/B.dataVec());
	return sameShapeMatrix(temp,A);
}

DoubleSymMat operator+(const DoubleSymMat& A, const DoubleSymMat& B) {
	verifyMatch(A,B);
	DoubleVec temp(A.dataVec()+B.dataVec());
	return sameShapeMatrix(temp,A);
}

DoubleSymMat operator-(const DoubleSymMat& A, const DoubleSymMat& B) {
	verifyMatch(A,B);
	DoubleVec temp(A.dataVec()-B.dataVec());
	return sameShapeMatrix(temp,A);
}

DoubleSymMat operator*(const DoubleSymMat& A, Double x) {
	DoubleVec temp(A.dataVec()*x);
	return sameShapeMatrix(temp,A);
}

/*
 * Out-of-line version for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
  DoubleSymMat operator*(Double x, const DoubleSymMat& A) { return A*x; }
#endif

DoubleSymMat operator/(const DoubleSymMat& A, Double x) {
	DoubleVec temp(A.dataVec()/x);
	return sameShapeMatrix(temp,A);
}


DoubleSymMat operator/(Double x, const DoubleSymMat& A) {
	DoubleVec temp(x/A.dataVec());
	return sameShapeMatrix(temp,A);
}

DoubleSymMat operator+(const DoubleSymMat& A, Double x) {
	DoubleVec temp(A.dataVec()+x);
	return sameShapeMatrix(temp,A);
}
/*
 * Out-of-line versions for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
DoubleSymMat operator+(Double x, const DoubleSymMat& A) { return A+x; }
DoubleSymMat operator-(const DoubleSymMat& A, Double x) { return A+(-x); }
#endif
DoubleSymMat operator-(Double x, const DoubleSymMat& A) {
	DoubleVec temp(x-A.dataVec());
	return sameShapeMatrix(temp,A);
}

DoubleSymMat atan2(const DoubleSymMat& A, const DoubleSymMat& B) {
	DoubleVec temp(atan2(A.dataVec(),B.dataVec()));
	return sameShapeMatrix(temp,A);
}

DoubleSymMat abs(const DoubleSymMat& A) {
	DoubleVec temp(abs(A.dataVec()));
	return sameShapeMatrix(temp,A);
}



DoubleSymMat transpose(const DoubleSymMat& A) { return A; }

DoubleVec product(const DoubleSymMat& A, const DoubleVec& x)
{
    if (A.cols()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATVECPROD,A.className,A.rows(),A.cols(),x.length())));
    }
    DoubleVec y(A.rows(),rwUninitialized);
    const DoubleVec& v = A.dataVec();
    int n=A.rows();
    for( int i=0; i<n; i++ ) {
      y(i) = dot( v.slice(i*(i+1)/2,i+1,1), x.slice(0,i+1,1) );
      Double* index = (Double*)v.data() + ((i+2)*(i+3)/2-2);
      for( int k=i+1; k<n; index+=(++k) ) {
	y(i) += (*index)*x(k);
      }
    }
    return y;
}

DoubleVec product(const DoubleVec& x, const DoubleSymMat& A)
{
    if (A.rows()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECMATPROD,x.length(),A.className,A.rows(),A.cols())));
    }
    return product(A,x);
}



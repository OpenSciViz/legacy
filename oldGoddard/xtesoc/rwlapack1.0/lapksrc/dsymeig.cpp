/*
 * Implementation of DoubleSymEigDecomp
 *
 * Generated from template $Id: xsymeig.cpp,v 1.4 1993/10/11 21:48:25 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */ 

#include "rw/dsymeig.h"
#include "rw/dseigsrv.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"

typedef DoubleSymQREigServer DefaultServer;

DoubleSymEigDecomp::DoubleSymEigDecomp()
{
  n=0;
  computedAll = accurate = TRUE;
}

DoubleSymEigDecomp::DoubleSymEigDecomp(const DoubleSymEigDecomp& x)
: n(x.n),
  lambda(x.lambda),
  P(x.P),
  computedAll(x.computedAll),
  accurate(x.accurate)
{
}

DoubleSymEigDecomp::DoubleSymEigDecomp(const DoubleSymMat& A, RWBoolean computeVecs)
{
  factor(A,computeVecs);
}

DoubleSymEigDecomp::DoubleSymEigDecomp(const DoubleSymBandMat& A, RWBoolean computeVecs)
{
  factor(A,computeVecs);
}

void DoubleSymEigDecomp::factor(const DoubleSymMat& A, RWBoolean computeVecs)
{
  DefaultServer server;
  server.computeEigenVectors(computeVecs);
  *this = server(A);
}
  
void DoubleSymEigDecomp::factor(const DoubleSymBandMat& A, RWBoolean computeVecs)
{
  DefaultServer server;
  server.computeEigenVectors(computeVecs);
  *this = server(A);
}

void DoubleSymEigDecomp::operator=(const DoubleSymEigDecomp& A)
{
  n = A.n;
  computedAll = A.computedAll;
  accurate = A.accurate;
  lambda.reference(A.lambda);
  P.reference(A.P);
}

Double       DoubleSymEigDecomp::eigenValue(int i)  const
{
  if (i<0 || i>=lambda.length()) RWTHROW(RWBoundsErr(RWMessage(RWLAPK_NOEIG,i)));
  return lambda(i);
}

const DoubleVec    DoubleSymEigDecomp::eigenVector(int i) const
{
  if (i<0 || i>=P.cols()) RWTHROW(RWBoundsErr(RWMessage(RWLAPK_NOEIG,i)));
  return P.col(i);
}

RWBoolean DoubleSymEigDecomp::good()          const
{
  return computedAll && accurate;
}

RWBoolean DoubleSymEigDecomp::inaccurate()    const
{
  return computedAll && !accurate;
}

RWBoolean DoubleSymEigDecomp::fail()          const
{
  return !computedAll;
}

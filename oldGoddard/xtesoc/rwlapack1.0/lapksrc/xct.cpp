> ###############################################################
> # Functions for converting from one type to another - Part 1  #
> #                                                             #
> # $Header: /users/rcs/lapksrc/xct.cpp,v 1.2 1993/10/11 21:48:07 alv Exp $
> #                                                             #
> # This is the functions to convert between types of the same  #
> # shape of matrix.                                            #
> #                                                             #
> # The following type of conversions go in the following files:#
> # Float <->Double  : fxxxxct.cpp                              #
> # Int   <->Double  : dxxxxct.cpp                              #
> # Int   <->Float   : ixxxxct.cpp                              #
> # SChar <->Int     : scxxxxct.cpp                             #
> # UChar <->Int     : ucxxxxct.cpp                             #
> # Double<->DComplex: xxxxct.cpp                               #
> # This works out so that each particular conversion sits in   #
> # its own file.  This minimizes the number of object files    #
> # that need to be drawn in to an executable.                  #
> #                                                             #
> # Copyright (1991) by Rogue Wave Software, Inc.  This software#
> # is subject to copyright protection under the laws of the    #
> # United States and other countries.                          #
> #                                                             #
> # Written by Al Vermeulen.                                    #
> #                                                             #
> # Limited License.                                            #
> ###############################################################
> #
> # $Log: xct.cpp,v $
> # Revision 1.2  1993/10/11  21:48:07  alv
> # synch'ed with Math.h++ v5.1
> #
> # Revision 1.1  1993/06/23  18:28:06  alv
> # Initial revision
> #
> # Revision 1.2  1993/01/26  21:05:35  alv
> # removed DOS EOF
> #
> # Revision 1.1  1993/01/23  00:01:10  alv
> # Initial revision
> #
> # 
> #    Rev 1.3   17 Oct 1991 09:22:28   keffer
> # Changed include path to <rw/xxx.h>
> # 
> #    Rev 1.2   01 Sep 1991 09:23:08   keffer
> # #include "rw/defs.h" to avoid Glock preprocessor nesting limits
> # 
> #    Rev 1.1   27 Aug 1991 11:47:22   keffer
> # Removed single argument constructors to avoid type conversion problems
> # 
> #    Rev 1.0   18 Aug 1991 10:28:30   keffer
> # Math 4.0 alpha5
>
>
> include macros 
>
#if 0
/*
 * Functions for conversion between matrix types with the same shape.
 *
 * Generated from template $Id: xct.cpp,v 1.2 1993/10/11 21:48:07 alv Exp $
 *
> copyright
 */
#endif

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/lapkerr.h"
#include "rw/rwerr.h"
#include "rw/<tLetter><shapeAbbrev>mat.h"
> if <R/C>==Complex
>   if <M>==HermMat
#include "rw/<pLetter>symmat.h"
#include "rw/<pLetter>skewmat.h"
>   elif <M>==HermBandMat
#include "rw/<pLetter>sbndmat.h"
>   else
#include "rw/<pLetter><shapeAbbrev>mat.h"
>   endif
> else /* Real */
>
> if <T>==Float
>   define <From>=Float <To>=Double
#include "rw/d<shapeAbbrev>mat.h"
> elif <T>==Double
>  if <M>!=GenMat
>  err dxxxct.cpp files only needed for GenMat, not for <M>
>  endif
>   define <From>=Int <To>=Double
#include "rw/i<shapeAbbrev>mat.h"
> elif <T>==Int
>   define <From>=Int <To>=Float
#include "rw/f<shapeAbbrev>mat.h"
> elif <T>==SChar
>   define <From>=SChar <To>=Int
#include "rw/i<shapeAbbrev>mat.h"
> elif <T>==UChar
>   define <From>=UChar <To>=Int
#include "rw/i<shapeAbbrev>mat.h"
> else
> err No type (not shape) conversions defined for <T>
> endif
>
> endif /* Real or Complex */
>

> # Constructors used only for complex types
>
> if <R/C>==Complex
>   if <M>==HermMat
>
<T><M>::<T><M>(const <P>SymMat& re)
  : vec(re.dataVec())
{
  n=re.cols();
}

<T><M>::<T><M>(const <P>SymMat& re, const <P>SkewMat& im)
  : vec(re.dataVec(), im.dataVec())
{
    // The number of points in re and im must agree at this point
    // or else the constructor for vec above would have failed.
    n = re.cols();
}
>
>   elif <M>==HermBandMat
>
<T><M>::<T><M>(const <P>SymBandMat& re) : vec(re.dataVec())
{
  n = re.cols();
  bandu = re.halfBandwidth();
}
>
>   else    # matrix is not one of the Hermetian types
>
<T><M>::<T><M>(const <P><M>& re)
  : vec(re.dataVec())
{
    n=re.cols();
> if <M>==GenMat
    m=re.rows();
> endif
>
> if <M>==BandMat
    band=re.bandwidth();
    bandu=re.upperBandwidth();
> endif
>
> if <M>==SymBandMat || <M>==HermBandMat
    bandu=re.upperBandwidth();
> endif
}

<T><M>::<T><M>(const <P><M>& re, const <P><M>& im)
  : vec(re.dataVec(),im.dataVec())
{
    // The number of points in re and im must agree at this point
    // or else the constructor for vec above would have failed.

    n=re.cols();
> if <M>==GenMat
    m=re.rows();
    if (re.rows() != im.rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATSIZE,re.rows(),re.cols(),im.rows(),im.cols())));
> endif
>
> if <M>==BandMat || <M>==SymBandMat || <M>==HermBandMat
>  if <M>==BandMat
    band=re.bandwidth();
>  endif
    bandu=re.upperBandwidth();
    if (bandwidth()!=im.bandwidth() || bandu!=im.upperBandwidth()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_BANDSIZE,upperBandwidth(),lowerBandwidth(),im.upperBandwidth(),im.lowerBandwidth())));
    }
> endif
}
>   endif    /* If <M>==HermMat else ... */
>
> else  /* <R/C> != Complex */
>
>
> # There is no way to convert an IntVec to a FloatVec (this is
> # a deficiency in math.h++) so don't try to generate this function
> # in that case.
>
> if <From>!=Int || <To>!=Float
>
<To><M>::<To><M>(const <From><M>& A)
  : vec(A.dataVec())
{
    n=A.cols();
>  if <M>==GenMat
    m=A.rows();
>  endif
>
>  if <M>==BandMat
    band=A.bandwidth();
    bandu=A.upperBandwidth();
>  endif
>
>  if <M>==SymBandMat || <M>==HermBandMat
    bandu=A.upperBandwidth();
>  endif
}
>
> endif

> # There is no way to convert things to characters (this is a
> # deficiency in math.h++)

> if <From>!=UChar && <From>!=SChar

<From><M> to<From>(const <To><M>& A)
{
> if <M>==BandMat
    return <From><M>( to<From>(A.dataVec()),A.rows(),A.cols(),
		     A.lowerBandwidth(),A.upperBandwidth() );
> elif <M>==SymBandMat || <M>==HermBandMat
    return <From><M>( to<From>(A.dataVec()),A.rows(),A.cols(),A.lowerBandwidth() );
> else
    return <From><M>( to<From>(A.dataVec()), A.rows(), A.cols() );
> endif
}
> endif

> endif	/* If <R/C>==Complex ... else */

/*
 * Functions for shape conversion for DoubleSymBandMat, except
 * conversion to/from square matrices.
 *
 * Generated from template $Id: xcs.cpp,v 1.2 1993/10/11 21:48:07 alv Exp $
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/lapkerr.h"
#include "rw/rwerr.h"
#include "rw/dsbndmat.h"
#include "rw/dbandmat.h"

DoubleBandMat::DoubleBandMat( const DoubleSymBandMat& A )
  : vec(A.bandwidth()*A.rows(),rwUninitialized)
{
  n=A.rows();
  band =A.bandwidth();
  bandu=A.upperBandwidth();
  for(int i=0; i<=bandu; i++) {
    DoubleVec d = A.diagonal(i);
    diagonal(i) = d;
    if (i>0) diagonal(-i) = d;
  }
}

DoubleSymBandMat toSymBandMat( const DoubleBandMat& S )
{
  int n = S.rows();
  unsigned l=S.lowerBandwidth();
  unsigned u=S.upperBandwidth();
  if (l!=u) RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTSYM)));
  DoubleSymBandMat A( n, n, u );
  if (n>=1) A.diagonal(0) = S.diagonal(0);
  for( int i=(u<n)?u:n; i>=1; i-- ) {
    DoubleVec Adiag(A.diagonal(i));
    Adiag = S.diagonal(i);		// Avoid generating temporaries
    Adiag += S.diagonal(-i);
    Adiag /= 2;
  }
  return A;
}


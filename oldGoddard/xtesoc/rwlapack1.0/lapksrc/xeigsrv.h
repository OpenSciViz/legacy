> #################################################################
> # The master template file for non-symmetric eigen-servers      #
> #                                                               #
> # $Header: /users/rcs/lapksrc/xeigsrv.h,v 1.2 1993/07/05 19:19:33 alv Exp $
> #                                                               #
> # Copyright (1993) by Rogue Wave Software, Inc.  This           #
> # software is subject to copyright protection under the laws    #
> # of the United States and other countries.                     #
> #                                                               #
> # Written by Al Vermeulen.                                      #
> #                                                               #
> # Limited License.                                              #
> #################################################################
> #
> # $Log: xeigsrv.h,v $
> # Revision 1.2  1993/07/05  19:19:33  alv
> # fixed up for complex numbers
> #
> # Revision 1.1  1993/05/18  17:00:02  alv
> # Initial revision
> #
>
> # Macros that we need
> 
> define <T>=RW_T_RW
> 
> if <T>==Double
> define <P>=Double <tLetter>=d <Z>=DComplex <zLetter>=c
> elif <T>==Float
> define <P>=Float  <tLetter>=f <Z>=FComplex <zLetter>=b
> elif <T>==DComplex
> define <P>=Double <tLetter>=c <Z>=DComplex <zLetter>=c <pLetter>=d
> else
> err Symmetric Eigen-decomposition: not defined for '<T>'
> endif
>
> beginwrapper <tLetter>eigsrv

/*
 * <T>EigServer     - Abstract base for eigenservers
 * <T>SchurEigServer- Schur decomposition method, this is the default server
 * <T>HessEigServer - Don't generate Schur decomp - get eigenvectors by inverse iteration
 *
 * Generated from template $Id: xeigsrv.h,v 1.2 1993/07/05 19:19:33 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */ 

#include "rw/lapkdefs.h"
#include "rw/<tLetter>genmat.h"
#include "rw/<tLetter>vec.h"
#include "rw/ivec.h"
#include "rw/<tLetter>eig.h"  
> if <T>!=<P>
#include "rw/<pLetter>eig.h"
> endif
class <T>BalanceDecomp;
class <T>HessenbergDecomp;
class <T>SchurDecomp;

class <T>EigServer {
protected:
  static unsigned&  n(<T>EigDecomp& x)           {return x.n;}
  static <Z>Vec&    lambda(<T>EigDecomp& x)      {return x.lambda;}
  static <T>GenMat& P(<T>EigDecomp& x)           {return x.P;}
  static <T>GenMat& Q(<T>EigDecomp& x)           {return x.Q;}
  static RWBoolean& computedAll(<T>EigDecomp& x) {return x.computedAll;}
  static RWBoolean& accurate(<T>EigDecomp& x)    {return x.accurate;}
> if <T>!=<P>
  static unsigned&  n(<P>EigDecomp& x)           {return x.n;}
  static <Z>Vec&    lambda(<P>EigDecomp& x)      {return x.lambda;}
  static <P>GenMat& P(<P>EigDecomp& x)           {return x.P;}
  static <P>GenMat& Q(<P>EigDecomp& x)           {return x.Q;}
  static RWBoolean& computedAll(<P>EigDecomp& x) {return x.computedAll;}
  static RWBoolean& accurate(<P>EigDecomp& x)    {return x.accurate;}
> endif
    // These functions provide access to the guts of the EigDecomp
    // object for the server.  This way we can declare more servers
    // without having to make them all friends of EigDecomp.

public:
  virtual <T>EigDecomp operator()(const <T>GenMat&) =0;
    // Compute a decomposition.  The subclasses have operator()
    // functions for intermediate forms, like Hessenberg and Schur
    // decompositions.
};

class <T>SchurEigServer : public <T>EigServer {
private:
  RWBoolean computeLeftVecs_;
  RWBoolean computeRightVecs_;
  RWBoolean scale_;
  RWBoolean permute_;
  IntVec    selectVec_;       // Which eigenvectors to compute
  RWSlice   selectRange_;     // (either Vec==nil, or Range==RWAll, or both)
public:
  <T>SchurEigServer(RWBoolean computeLeftVecs=TRUE, RWBoolean computeRightVecs=TRUE, RWBoolean scale=TRUE, RWBoolean permute=TRUE);
  virtual <T>EigDecomp operator()(const <T>GenMat& A);
  virtual <T>EigDecomp operator()(const <T>BalanceDecomp& A);
  virtual <T>EigDecomp operator()(const <T>HessenbergDecomp& A);
  virtual <T>EigDecomp operator()(const <T>SchurDecomp& A);
  RWBoolean computeLeftEigenVectors() const;
  void      computeLeftEigenVectors(RWBoolean);
  RWBoolean computeRightEigenVectors() const;
  void      computeRightEigenVectors(RWBoolean);
  RWBoolean computeAllEigenVectors()   const;
  RWBoolean scale() const;
  void      scale(RWBoolean);
  void      selectEigenVectors(const IntVec&);     // Compute only selected eigenvectors
  void      selectEigenVectors(const RWSlice&);
  RWBoolean permute() const;
  void      permute(RWBoolean);
  void      balance(RWBoolean);   // sets both scale and permute
};
  
class <T>HessEigServer : public <T>EigServer {
private:
  RWBoolean computeLeftVecs_;
  RWBoolean computeRightVecs_;
  RWBoolean scale_;
  RWBoolean permute_;
  IntVec    selectVec_;       // Which eigenvectors to compute
  RWSlice   selectRange_;     // (either Vec==nil, or Range==RWAll, or both)]
> if <T>==<Z>
  <T>EigDecomp setEigenVals(const <T>HessenbergDecomp& H);   // Compute eigenvalues
> else
  <T>EigDecomp setEigenVals(const <T>HessenbergDecomp& H, <T>Vec* wr, <T>Vec* wi);   // Compute eigenvalues
> endif
public:
  <T>HessEigServer(RWBoolean computeLeftVecs=TRUE, RWBoolean computeRightVecs=TRUE, RWBoolean scale=TRUE, RWBoolean permute=TRUE);
  virtual <T>EigDecomp operator()(const <T>GenMat& A);
  virtual <T>EigDecomp operator()(const <T>BalanceDecomp& A);
  virtual <T>EigDecomp operator()(const <T>HessenbergDecomp& A);
  RWBoolean computeLeftEigenVectors() const;
  void      computeLeftEigenVectors(RWBoolean);
  RWBoolean computeRightEigenVectors() const;
  void      computeRightEigenVectors(RWBoolean);
  RWBoolean computeAllEigenVectors()   const;
  RWBoolean scale() const;
  void      scale(RWBoolean);
  void      selectEigenVectors(const IntVec&);     // Compute only selected eigenvectors
  void      selectEigenVectors(const RWSlice&);
  RWBoolean permute() const;
  void      permute(RWBoolean);
  void      balance(RWBoolean);   // sets both scale and permute
};

> endwrapper

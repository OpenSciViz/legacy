
/*
 * defs for balancing transformation and decomposition classes
 *
 * Generated from template $Id: xbal.cpp,v 1.2 1993/10/11 21:48:04 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 */ 

#include "rw/lapkdefs.h"
#include "rw/lapack.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/cbal.h"

DComplexBalanceTransform::DComplexBalanceTransform()
{
  job_ = 'N';
  ilo_ = ihi_ = 0;
}

void DComplexBalanceTransform::operator=(const DComplexBalanceTransform& A)
{
  job_ = A.job_;
  ilo_ = A.ilo_;
  ihi_ = A.ihi_;
  scale_.reference(A.scale_);
}
 
void DComplexBalanceTransform::init(DComplexGenMat *A, RWBoolean permute, RWBoolean scale)
{
  if (A->rows()!=A->cols()) {
    RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOTSQUARE,A->rows(),A->cols())));
  }
  RWPRECONDITION(A->rowStride()==1);
  RWPRECONDITION(A->colStride()==A->rows());
  scale_.reshape(A->rows());
  job_ = ( permute ? (scale ? 'B' : 'P') : (scale ? 'S' : 'N') );
  long info;
  zgebal(job_,A->rows(),A->data(),A->rows(),ilo_,ihi_,scale_.data(),info);
  RWPOSTCONDITION(info==0);
}

DComplexGenMat DComplexBalanceTransform::transform(const DComplexGenMat& X) const
{
  if (X.rows()!=rows()) {
    RWTHROW(RWInternalErr(RWMessage(RWLAPK_BALANCE,X.rows(),rows())));
  }
  DComplexGenMat Y = X.copy();
  if (rows()>0) {
    long info;
    zgebak(job_,'R',rows(),ilo_,ihi_,(Double*)scale_.data(),Y.cols(),Y.data(),Y.rows(),info);
    RWPOSTCONDITION(info==0);
  }
  return Y;
}

DComplexGenMat DComplexBalanceTransform::invTransform(const DComplexGenMat& X) const
{
  if (X.rows()!=rows()) {
    RWTHROW(RWInternalErr(RWMessage(RWLAPK_BALANCE,X.rows(),rows())));
  }
  DComplexGenMat Y = X.copy();
  if (rows()>0) {
    long info;
    zgebak(job_,'L',rows(),ilo_,ihi_,(Double*)scale_.data(),Y.cols(),Y.data(),Y.rows(),info);
    RWPOSTCONDITION(info==0);
  }
  return Y;
}

DComplexVec DComplexBalanceTransform::transform(const DComplexVec& x) const
{
  DComplexGenMat Y = transform(DComplexGenMat(x,x.length(),1));
  RWPOSTCONDITION(Y.cols()==1);
  return Y.col(0);
}

DComplexVec DComplexBalanceTransform::invTransform(const DComplexVec& x) const
{
  DComplexGenMat Y = invTransform(DComplexGenMat(x,x.length(),1));
  RWPOSTCONDITION(Y.cols()==1);
  return Y.col(0);
}

DComplexBalanceDecomp::DComplexBalanceDecomp()
{
} 

DComplexBalanceDecomp::DComplexBalanceDecomp(const DComplexGenMat& A, RWBoolean permute, RWBoolean scale)
{
  factor(A,permute,scale);
}

void DComplexBalanceDecomp::factor(const DComplexGenMat& A, RWBoolean permute, RWBoolean scale)
{
  C_.reference(A.copy());
  B_.init(&C_,permute,scale);
  RWPOSTCONDITION(rows()==A.rows());
  RWPOSTCONDITION(B_.rows()==A.rows());
  RWPOSTCONDITION(C_.rows()==A.rows());
}

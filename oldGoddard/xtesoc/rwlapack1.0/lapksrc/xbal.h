> #################################################################
> # The template file for eigenvalue balancing transformation     #
> #                                                               #
> # $Header: /users/rcs/lapksrc/xbal.h,v 1.1 1993/05/18 17:00:02 alv Exp $
> #                                                               #
> # Copyright (1993) by Rogue Wave Software, Inc.  This           #
> # software is subject to copyright protection under the laws    #
> # of the United States and other countries.                     #
> #                                                               #
> # Written by Al Vermeulen.                                      #
> #                                                               #
> # Limited License.                                              #
> #################################################################
> #
> # $Log: xbal.h,v $
> # Revision 1.1  1993/05/18  17:00:02  alv
> # Initial revision
> #
>
> # Macros that we need
> 
> define <T>=RW_T_RW
> 
> if <T>==Double
> define <P>=Double <tLetter>=d
> elif <T>==Float
> define <P>=Float <tLetter>=f 
> elif <T>==DComplex
> define <P>=Double <tLetter>=c
> else
> err balance decomposition: not defined for '<T>'
> endif
>
> beginwrapper <tLetter>bal

/*
 * <T>BalanceTransform - combined permutation and diagonal balancing transformation
 * <T>BalanceDecomp    - balance decomposition of a matrix
 *
 * Generated from template $Id: xbal.h,v 1.1 1993/05/18 17:00:02 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 * This is the representation of a balancing transformation.  Sometimes you
 * can improve the quality of eigenvectors and eigenvalues by first scaling
 * the matrix so the rows and columns have approximately the same norm.  This
 * is done by the transformation encapsulated in these classes.
 *
 * The Transform class encapsulates just the transform itself.  The Decomp
 * class encapsulates a decomposition of a particular matrix.  The only way
 * to set up a Transform is via the Decomp class.  This is true because the
 * transform's only constructor (excepting the copy ctor) is private, and
 * Decomp is a friend class.
 */ 

#include "rw/lapkdefs.h"
#include "rw/<tLetter>vec.h"
#include "rw/<tLetter>genmat.h"

class <T>BalanceTransform {
friend class <T>BalanceDecomp;
private:
  char    job_;      // Encodes whether this does scaling, permuting, or both
  long    ilo_,ihi_; // breakup of the matrix into disconnected pieces, if possible
  <P>Vec  scale_;    // details of the permutations and scaling factors
  void init(<T>GenMat *A, RWBoolean permute, RWBoolean scale);
     // A is overwritten with the balanced matrix.
public:
  <T>BalanceTransform();
  void         operator=(const <T>BalanceTransform&);
  unsigned     cols()                        const {return scale_.length();}
  unsigned     rows()                        const {return scale_.length();}
  unsigned     lowIndex()                    const {return ilo_;}
  unsigned     highIndex()                   const {return ihi_;}
  <T>GenMat    transform(const <T>GenMat&)   const;// Compute BX
  <T>GenMat    invTransform(const <T>GenMat&)const;// Compute inv(B)'X
  <T>Vec       transform(const <T>Vec&)      const;// Compute BX
  <T>Vec       invTransform(const <T>Vec&)   const;// Compute inv(B)'X
};

class <T>BalanceDecomp {
private:
  <T>GenMat           C_;
  <T>BalanceTransform B_;
    // C_ must be declared before B_ because B_ uses C_ in its constructor
public:
  <T>BalanceDecomp();
  <T>BalanceDecomp(const <T>GenMat& A, RWBoolean permute=TRUE, RWBoolean scale=TRUE);
  void factor(const <T>GenMat& A, RWBoolean permute=TRUE, RWBoolean scale=TRUE);
  unsigned                   cols()          const {return C_.cols();}
  unsigned                   rows()          const {return C_.rows();}
  const <T>GenMat            C()             const {return C_;}
  const <T>BalanceTransform& B()             const {return B_;}
};

> endwrapper

#if 0
/*
 * Generated from template $Id: xmth.cpp,v 1.2 1993/10/11 21:48:17 alv Exp $
 *
 * Math member functions for the matrix types
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */
#endif

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/chermmat.h"
#include "rw/dsymmat.h"
#include "rw/dskewmat.h"

/*
 * This function returns a new matrix the same shape as the one 
 * passed in but with the data passed in.  This way we restrict
 * all the >if statements to a couple lines
 */
inline DComplexHermMat sameShapeMatrix( DComplexVec& vec, const DComplexHermMat& A )
{
  return DComplexHermMat(vec,A.rows(),A.cols());
}


static void verifyMatch(const DComplexHermMat& A, const DComplexHermMat& B)
{
    if (A.rows()!=B.rows() || A.cols()!=B.cols())
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATSIZE,A.rows(),A.cols(),B.rows(),B.cols())));
}

RWBoolean DComplexHermMat::operator==(const DComplexHermMat& X)
{
    if (n!=X.n) return FALSE;
    return vec==X.vec;	// All elements in vec used in the matrix
}


DComplexHermMat& DComplexHermMat::operator+=(const DComplexHermMat& m)
{
  verifyMatch(*this,m);
  vec += m.vec;
  return *this;
}

DComplexHermMat& DComplexHermMat::operator-=(const DComplexHermMat& m)
{
  verifyMatch(*this,m);
  vec -= m.vec;
  return *this;
}

DComplexHermMat& DComplexHermMat::operator*=(const DComplexHermMat& m)
{
  verifyMatch(*this,m);
  vec *= m.vec;
  return *this;
}

DComplexHermMat& DComplexHermMat::operator*=(DComplex x)
{
  vec *= x;
  return *this;
}

DComplexHermMat& DComplexHermMat::operator/=(const DComplexHermMat& m)
{
  verifyMatch(*this,m);
  vec /= m.vec;
  return *this;
}

DComplexHermMat& DComplexHermMat::operator/=(DComplex x)
{
  vec /= x;
  return *this;
}




DComplexHermMat operator-(const DComplexHermMat& A)
{
  DComplexVec temp(-(A.dataVec()));
  return sameShapeMatrix(temp,A);
}
  
DComplexHermMat operator+(const DComplexHermMat& A)
{
  return A;
}

DComplexHermMat operator*(const DComplexHermMat& A, const DComplexHermMat& B) {
	verifyMatch(A,B);
	DComplexVec temp(A.dataVec()*B.dataVec());
	return sameShapeMatrix(temp,A);
}

DComplexHermMat operator/(const DComplexHermMat& A, const DComplexHermMat& B) {
	verifyMatch(A,B);
	DComplexVec temp(A.dataVec()/B.dataVec());
	return sameShapeMatrix(temp,A);
}

DComplexHermMat operator+(const DComplexHermMat& A, const DComplexHermMat& B) {
	verifyMatch(A,B);
	DComplexVec temp(A.dataVec()+B.dataVec());
	return sameShapeMatrix(temp,A);
}

DComplexHermMat operator-(const DComplexHermMat& A, const DComplexHermMat& B) {
	verifyMatch(A,B);
	DComplexVec temp(A.dataVec()-B.dataVec());
	return sameShapeMatrix(temp,A);
}

DComplexHermMat operator*(const DComplexHermMat& A, DComplex x) {
	DComplexVec temp(A.dataVec()*x);
	return sameShapeMatrix(temp,A);
}

/*
 * Out-of-line version for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
  DComplexHermMat operator*(DComplex x, const DComplexHermMat& A) { return A*x; }
#endif

DComplexHermMat operator/(const DComplexHermMat& A, DComplex x) {
	DComplexVec temp(A.dataVec()/x);
	return sameShapeMatrix(temp,A);
}


DComplexHermMat operator/(DComplex x, const DComplexHermMat& A) {
	DComplexVec temp(x/A.dataVec());
	return sameShapeMatrix(temp,A);
}



DoubleSymMat abs(const DComplexHermMat& A) {
	DoubleVec temp(abs(A.dataVec()));
	return DoubleSymMat(temp,A.rows(),A.cols());
}


DComplexHermMat conj(const DComplexHermMat& A) {
	DComplexVec temp(conj(A.dataVec()));
	return sameShapeMatrix(temp,A);
}

DoubleSymMat real(const DComplexHermMat& A) {
	DoubleVec temp(real(A.dataVec()));
	return DoubleSymMat(temp,A.rows(),A.cols());
}

DoubleSkewMat imag(const DComplexHermMat& A) {
	DoubleVec temp(imag(A.dataVec()));
	return DoubleSkewMat(temp,A.rows(),A.cols());
}

DoubleSymMat norm(const DComplexHermMat& A) {
	DoubleVec temp(norm(A.dataVec()));
	return DoubleSymMat(temp,A.rows(),A.cols());
}

DoubleSkewMat arg(const DComplexHermMat& A) {
	DoubleVec temp(arg(A.dataVec()));
	return DoubleSkewMat(temp,A.rows(),A.cols());
}


DComplexHermMat transpose(const DComplexHermMat& A)
{
  DComplexHermMat T(conj(A));
  DComplex* index = T.data();		// Fix up the diagonal
  for( int k=1; k<=T.rows(); index+=(++k) ) {
    (*index) = conj(*index);
  }
  return T;
}

DComplexVec product(const DComplexHermMat& A, const DComplexVec& x)
{
    if (A.cols()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATVECPROD,A.className,A.rows(),A.cols(),x.length())));
    }
    DComplexVec y(A.rows(),rwUninitialized);
    const DComplexVec& v = A.dataVec();
    int n=A.rows();
    y(0) = DComplex(0,0);
    for( int i=0; i<n; i++ ) {
      if (i>0) {
        y(i) = conjDot( v.slice(i*(i+1)/2,i,1), x.slice(0,i,1) );
      }
      DComplex* index = (DComplex*)v.data() + ((i+1)*(i+2)/2-1);
      for( int k=i; k<n; index+=(++k) ) {
	y(i) += (*index)*x(k);
      }
    }
    return y;
}

DComplexVec product(const DComplexVec& x, const DComplexHermMat& A)
{
    if (A.rows()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECMATPROD,x.length(),A.className,A.rows(),A.cols())));
    }
    DComplexVec y(A.cols(),DComplex(0,0));
    const DComplexVec& v = A.dataVec();
    int n=A.rows();
    for( int i=0; i<n; i++ ) {
      y(i) = dot( v.slice(i*(i+1)/2,i+1,1), x.slice(0,i+1,1) );
      DComplex* index = (DComplex*)v.data() + ((i+2)*(i+3)/2-2);
      for( int k=i+1; k<n; index+=(++k) ) {
	y(i) += conj(*index)*x(k);
      }
    }
    return y;
}



> #################################################################
> # Least squares fitting using Cholesky decomposition            #
> #                                                               #
> # $Header: /users/rcs/lapksrc/xlsch.cpp,v 1.5 1993/10/11 21:48:13 alv Exp $
> #                                                               #
> # Copyright (1991) by Rogue Wave Software, Inc.  This software  #
> # is subject to copyright protection under the laws of the      #
> # United States and other countries.                            #
> #                                                               #
> # Written by Al Vermeulen.                                      #
> #                                                               #
> # Limited License.                                              #
> #################################################################
> #
> # $Log: xlsch.cpp,v $
> # Revision 1.5  1993/10/11  21:48:13  alv
> # synch'ed with Math.h++ v5.1
> #
> # Revision 1.4  1993/07/19  19:32:21  alv
> # somehow, this revision was not checked in last time
> #
> # Revision 1.2  1993/07/05  19:19:33  alv
> # fixed up for complex numbers
> #
> # Revision 1.1  1993/05/18  17:00:02  alv
> # Initial revision
> #
>
> define <T>=RW_T_RW
> if <T>==Double
> define <P>=Double <tLetter>=d <Sym>=Sym
> elif <T>==Float
> define <P>=Float <tLetter>=f  <Sym>=Sym
> elif <T>==DComplex
> define <P>=Double <tLetter>=c <Sym>=Herm
> else
> err least square class: not defined for '<T>'
> endif
> 
>
/*
 * defs for <T>LeastSqCh
 *
 * Generated from template $Id: xlsch.cpp,v 1.5 1993/10/11 21:48:13 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 */

#include "rw/<tLetter>lsch.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"

<T>LeastSqCh::<T>LeastSqCh() {}

<T>LeastSqCh::<T>LeastSqCh(const <T>GenMat& A)
{
  factor(A);
}

void      <T>LeastSqCh::factor(const <T>GenMat& A)
{
  A_.reference(A);
> if <T>!=<P>
  <T><Sym>Mat ATA = upperTo<Sym>Mat(conjTransposeProduct(A,A));
  ATA.makeDiagonalReal();
> else
  <T><Sym>Mat ATA = upperTo<Sym>Mat(transposeProduct(A,A));
> endif
  decomp_.factor(ATA);
}

<T>Vec    <T>LeastSqCh::residual(const <T>Vec& data) const
{
  <T>Vec x = solve(data);
  return data - product(A_,x);
}

<P>       <T>LeastSqCh::residualNorm(const <T>Vec& data) const
{
  if (data.length()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,data.length(),rows())));
  return l2Norm(residual(data));
}

<T>Vec    <T>LeastSqCh::solve(const <T>Vec& data) const
{
  if (decomp_.fail()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTSOLVELS)));
> if <T>==<P>
  <T>Vec ATb = product(transpose(A_),data);
> else
  <T>Vec ATb = product(conj(transpose(A_)),data);
> endif
  return ::solve(decomp_,ATb);
}

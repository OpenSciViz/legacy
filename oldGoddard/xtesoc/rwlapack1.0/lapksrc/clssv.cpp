/*
 * defs for DComplexLeastSqSV
 *
 * Generated from template $Id: xlssv.cpp,v 1.3 1993/10/11 21:48:15 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 */

#include "rw/clssv.h"
#include "rw/lapkerr.h"
#include "rw/rwerr.h"

DComplexLeastSqSV::DComplexLeastSqSV()                                 : DComplexSVDecomp() {}
DComplexLeastSqSV::DComplexLeastSqSV(const DComplexGenMat& A, Double tol)      : DComplexSVDecomp(A,tol) {}
DComplexLeastSqSV::DComplexLeastSqSV(const DComplexSVDecomp& A, Double tol)    : DComplexSVDecomp(A) {truncate(tol);}

static DComplexGenMat U(const DComplexLeastSqSV& x)
{
  if (x.numLeftVectors()<x.rank()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOSV,x.rank())));
  return x.leftVectors()(RWAll,RWSlice(0,x.rank()));
}

static DComplexGenMat V(const DComplexLeastSqSV& x)
{
  if (x.numRightVectors()<x.rank()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_NOSV,x.rank())));
  return x.rightVectors()(RWAll,RWSlice(0,x.rank()));
}

DComplexVec DComplexLeastSqSV::residual(const DComplexVec& data) const
{
  if (data.length()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,data.length(),rows())));
  DComplexVec x = solve(data);
  DComplexVec VTx = product(adjoint(V(*this)),x);
  VTx *= singularValues();
  DComplexVec Ax = product(U(*this),VTx);
  return data-Ax;
}

Double DComplexLeastSqSV::residualNorm(const DComplexVec& data) const
{
  if (data.length()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,data.length(),rows())));
  DComplexVec r = residual(data);
  return l2Norm(r);
}

DComplexVec DComplexLeastSqSV::solve(const DComplexVec& data) const
{
  if (data.length()!=rows()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,data.length(),rows())));
  DComplexVec x = product(adjoint(U(*this)),data);
  x /= singularValues();
  return product(V(*this),x);
} 

/*
 * Implementation of FloatPDBandFact
 *
 * Generated from template $Id: xfct.cpp,v 1.6 1993/10/11 21:48:11 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/lapkdefs.h"
#include "rw/fpdbdfct.h"
#include "rw/lapack.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"  
#include "rw/fgenmat.h"

const char* FloatPDBandFact::className = "FloatPDBandFact";

FloatPDBandFact::~FloatPDBandFact()
{
}

FloatPDBandFact::FloatPDBandFact()
{
  info = -1;
}

FloatPDBandFact::FloatPDBandFact( const FloatPDBandFact& A )
  : factorization(A.factorization)
{
  info = A.info;
  Anorm = A.Anorm;
}

FloatPDBandFact::FloatPDBandFact(const FloatSymBandMat& A, RWBoolean estimateCondition)
  : factorization(A)  // Use reference semantics, followed by
                      // deepenShallowCopy(), to work around a BCC V3.0 bug
{
    factorization.deepenShallowCopy();
    dofactor(estimateCondition);
}

void FloatPDBandFact::factor( const FloatSymBandMat& A, RWBoolean estimateCondition )
{
    FloatSymBandMat Acopy = A.copy();
    factorization.reference(Acopy);
    dofactor(estimateCondition);
}

void FloatPDBandFact::dofactor(RWBoolean estimateCondition)
{
   info=0;   // for size zero matrices, need to set this explicitly
   long n = rows();	

    // Error (sanity) checking
    RWPRECONDITION(factorization.dataVec().stride()==1);


    // If we care to ever find the condition number, we need to know
    // the 1-norm of the matrix A.  Compute it while the member data
    // factorization is still a copy of A, and not yet it's factorization.
    Anorm = -1; 
    if (n>0 && estimateCondition) {
      long kd = factorization.halfBandwidth();
      long ldab = kd+1;
      Float *work = new Float [n];
      Anorm = slansb('1','U',n,kd,factorization.data(),ldab,work);
      delete [] work;
    }

    if (n>0) {
      long kd = factorization.halfBandwidth();
      long ldab = kd+1;
      spbtrf('U',n,kd,factorization.data(),ldab,info);
      RWPOSTCONDITION(info>=0);
    }
}

RWBoolean FloatPDBandFact::fail() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}

RWBoolean FloatPDBandFact::isPD() const
{
    return (rows()>0 && info!=0) ? FALSE : TRUE;
}  

RWBoolean FloatPDBandFact::isSingular() const
{
    return (rows()>0 && info!=0) ? TRUE : FALSE;
}

Float FloatPDBandFact::condition() const
{
    Float Anormcopy = Anorm;      // Copy to stop warnings about ref parm
    long info;
    long n = rows();
    Float rcond;
    Float *work = new Float[4*n];   // Some routines only require 2*n, but what the hell
    long kd = factorization.halfBandwidth();
    long ldab = kd+1;
    long *work2 = new long [n];
    spbcon('U',n,kd,(Float*)factorization.data(),ldab,Anormcopy,rcond,work,work2,info);
    delete [] work2;
    delete [] work;
    RWPOSTCONDITION(info==0);
    return rcond;
}

FloatVec FloatPDBandFact::solve( const FloatVec& b ) const
{
  FloatGenMat B(b,b.length(),1);
  FloatGenMat X = solve(B);
  RWPOSTCONDITION(X.cols()==1);
  return X.col(0);
}

FloatGenMat FloatPDBandFact::solve( const FloatGenMat& B ) const
{
    FloatGenMat X(B.copy());
    long n = rows();
    if (B.rows()!=n) RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECLENGTH,B.rows(),n)));
    if (fail()) RWTHROW(RWInternalErr(RWMessage(RWLAPK_CANTSOLVE)));
    long nrhs = X.cols();
    long info;
    if (n>0) {
    long kd = factorization.halfBandwidth();
    long ldab = kd+1;
    spbtrs('U',n,kd,nrhs,(Float*)factorization.data(),ldab,X.data(),n,info);
      RWPOSTCONDITION(info==0);
    }
    return X;    
}

Float FloatPDBandFact::determinant() const
{
    Float rootdet = 1.0;
    for(int i=rows(); i--;) { rootdet *= factorization.val(i,i); }  // using val() rather than just op()() shuts up a SUN warning
    return rootdet*rootdet;
}
              

/*
 * Global functions which provide alternate ways to call member functions.
 * These used to be inline, but this requires that you include more header
 * files on some compilers, and that is just not worth it.
 */
FloatVec solve(const FloatPDBandFact& A, const FloatVec& b) { return A.solve(b); }
FloatGenMat solve(const FloatPDBandFact& A, const FloatGenMat& b) { return A.solve(b); }
Float    determinant (const FloatPDBandFact& A)	      { return A.determinant(); }


Float condition(const FloatPDBandFact& A) { return A.condition(); }



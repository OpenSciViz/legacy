/*
 *  Global functions used by the matrix classes.
 *
 *  $Header: /users/rcs/lapksrc/mat.cpp,v 1.2 1993/10/11 21:48:01 alv Exp $
 * 
 * Copyright (1991) by Rogue Wave Software, Inc.  This software
 * is subject to copyright protection under the laws of the
 * United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 *
 * $Log: mat.cpp,v $
 * Revision 1.2  1993/10/11  21:48:01  alv
 * synch'ed with Math.h++ v5.1
 *
 * Revision 1.1  1993/06/23  18:27:59  alv
 * Initial revision
 *
 * Revision 1.2  1993/01/26  21:05:40  alv
 * removed DOS EOF
 *
 * Revision 1.1  1993/01/23  00:02:09  alv
 * Initial revision
 *
 * 
 *    Rev 1.1   17 Oct 1991 09:22:34   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.0   18 Aug 1991 10:30:34   keffer
 * Math 4.0 alpha5
 */

#include "rw/rstream.h"
#include "rw/dcomplex.h"
#include "rw/dref.h"
#include "rw/fref.h"
#include "rw/cref.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"

double rwDoubleZero = 0;		// Initialize the zero constants
float rwFloatZero = 0;
DComplex rwDComplexZero = DComplex(0,0);

Float ROFloatRef::error(Float x)
{
    RWTHROW( RWInternalErr(RWMessage(RWLAPK_CHANGEDCONSTANT, double(*data), double(x)) ));
    return *data;
}

Double RODoubleRef::error(Double x)
{
    RWTHROW( RWInternalErr(RWMessage(RWLAPK_CHANGEDCONSTANT, double(*data), double(x) )));
    return *data;
}

DComplex RODComplexRef::error(DComplex x)
{
    RWTHROW( RWInternalErr(RWMessage( RWLAPK_CHANGEDCOMPLEXCONSTANT, real(*data), imag(*data), real(x), imag(x) )));
    return *data;
}

DComplex ROCJDComplexRef::error(DComplex x)
{
    RWTHROW( RWInternalErr( RWMessage(RWLAPK_CHANGEDCOMPLEXCONSTANT, real(*data), imag(*data), real(x), imag(x) )));
    return *data;
}

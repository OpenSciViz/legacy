/*
 * Implementation of DComplexHermEigDecomp
 *
 * Generated from template $Id: xsymeig.cpp,v 1.4 1993/10/11 21:48:25 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */ 

#include "rw/chermeig.h"
#include "rw/cheigsrv.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"

typedef DComplexHermQREigServer DefaultServer;

DComplexHermEigDecomp::DComplexHermEigDecomp()
{
  n=0;
  computedAll = accurate = TRUE;
}

DComplexHermEigDecomp::DComplexHermEigDecomp(const DoubleSymEigDecomp& x)
: n(x.n),
  lambda(x.lambda),
  P(x.P),
  computedAll(x.computedAll),
  accurate(x.accurate)
{
}

DComplexHermEigDecomp::DComplexHermEigDecomp(const DComplexHermMat& A, RWBoolean computeVecs)
{
  factor(A,computeVecs);
}

DComplexHermEigDecomp::DComplexHermEigDecomp(const DComplexHermBandMat& A, RWBoolean computeVecs)
{
  factor(A,computeVecs);
}

void DComplexHermEigDecomp::factor(const DComplexHermMat& A, RWBoolean computeVecs)
{
  DefaultServer server;
  server.computeEigenVectors(computeVecs);
  *this = server(A);
}
  
void DComplexHermEigDecomp::factor(const DComplexHermBandMat& A, RWBoolean computeVecs)
{
  DefaultServer server;
  server.computeEigenVectors(computeVecs);
  *this = server(A);
}

void DComplexHermEigDecomp::operator=(const DComplexHermEigDecomp& A)
{
  n = A.n;
  computedAll = A.computedAll;
  accurate = A.accurate;
  lambda.reference(A.lambda);
  P.reference(A.P);
}

Double       DComplexHermEigDecomp::eigenValue(int i)  const
{
  if (i<0 || i>=lambda.length()) RWTHROW(RWBoundsErr(RWMessage(RWLAPK_NOEIG,i)));
  return lambda(i);
}

const DComplexVec    DComplexHermEigDecomp::eigenVector(int i) const
{
  if (i<0 || i>=P.cols()) RWTHROW(RWBoundsErr(RWMessage(RWLAPK_NOEIG,i)));
  return P.col(i);
}

RWBoolean DComplexHermEigDecomp::good()          const
{
  return computedAll && accurate;
}

RWBoolean DComplexHermEigDecomp::inaccurate()    const
{
  return computedAll && !accurate;
}

RWBoolean DComplexHermEigDecomp::fail()          const
{
  return !computedAll;
}

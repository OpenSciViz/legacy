#if 0
/*
 * Generated from template $Id: xmth.cpp,v 1.2 1993/10/11 21:48:17 alv Exp $
 *
 * Math member functions for the matrix types
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */
#endif

/* This include is to avoid nesting limits in the Glock precompiler: */
#include "rw/defs.h"
#include "rw/rwerr.h"
#include "rw/lapkerr.h"
#include "rw/csbndmat.h"
#include "rw/dsbndmat.h"

/*
 * This function returns a new matrix the same shape as the one 
 * passed in but with the data passed in.  This way we restrict
 * all the >if statements to a couple lines
 */
inline DComplexSymBandMat sameShapeMatrix( DComplexVec& vec, const DComplexSymBandMat& A )
{
  return DComplexSymBandMat(vec,A.rows(),A.cols(),A.lowerBandwidth());
}

inline DoubleSymBandMat sameShapeRealMatrix( DoubleVec& vec, const DComplexSymBandMat& A )
{
  return DoubleSymBandMat(vec,A.rows(),A.cols(),A.lowerBandwidth());
}

static void verifyMatch(const DComplexSymBandMat& A, const DComplexSymBandMat& B)
{
    if (A.rows()!=B.rows() || A.cols()!=B.cols())
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATSIZE,A.rows(),A.cols(),B.rows(),B.cols())));
    if (A.bandwidth()!=B.bandwidth() || A.upperBandwidth()!=B.upperBandwidth())
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_BANDSIZE,A.bandwidth(),B.bandwidth(),
			A.upperBandwidth(), B.upperBandwidth())));
}

RWBoolean DComplexSymBandMat::operator==(const DComplexSymBandMat& X)
{
    if (n!=X.n || bandu!=X.bandu) return FALSE;
    int hb = halfBandwidth();
    if (hb==0) return ( vec==X.vec );	// A diagonal matrix
    /*
     * This is a little tricky cause some of the data vector is unused.
     * First check the last part of the data vector, then check the bits
     * at the beginning that are used.
     */
    int lastStart = hb*hb;
    int lastLen = (n-hb+1)*(hb+1)-1;
    if (n>=hb)
	if ( vec.slice(lastStart,lastLen)!=X.vec.slice(lastStart,lastLen) ) return FALSE;
    int el,len;
    for( el=hb,len=1 ; el<vec.length() && len<hb ; el+=hb,len++ ) {    
      if (vec.slice(el,len)!=X.vec.slice(el,len)) return FALSE;
    }
    return TRUE;
}


DComplexSymBandMat& DComplexSymBandMat::operator+=(const DComplexSymBandMat& m)
{
  verifyMatch(*this,m);
  vec += m.vec;
  return *this;
}

DComplexSymBandMat& DComplexSymBandMat::operator-=(const DComplexSymBandMat& m)
{
  verifyMatch(*this,m);
  vec -= m.vec;
  return *this;
}

DComplexSymBandMat& DComplexSymBandMat::operator*=(const DComplexSymBandMat& m)
{
  verifyMatch(*this,m);
  vec *= m.vec;
  return *this;
}

DComplexSymBandMat& DComplexSymBandMat::operator*=(DComplex x)
{
  vec *= x;
  return *this;
}

DComplexSymBandMat& DComplexSymBandMat::operator/=(const DComplexSymBandMat& m)
{
  verifyMatch(*this,m);
  vec /= m.vec;
  return *this;
}

DComplexSymBandMat& DComplexSymBandMat::operator/=(DComplex x)
{
  vec /= x;
  return *this;
}




DComplexSymBandMat operator-(const DComplexSymBandMat& A)
{
  DComplexVec temp(-(A.dataVec()));
  return sameShapeMatrix(temp,A);
}
  
DComplexSymBandMat operator+(const DComplexSymBandMat& A)
{
  return A;
}

DComplexSymBandMat operator*(const DComplexSymBandMat& A, const DComplexSymBandMat& B) {
	verifyMatch(A,B);
	DComplexVec temp(A.dataVec()*B.dataVec());
	return sameShapeMatrix(temp,A);
}

DComplexSymBandMat operator/(const DComplexSymBandMat& A, const DComplexSymBandMat& B) {
	verifyMatch(A,B);
	DComplexVec temp(A.dataVec()/B.dataVec());
	return sameShapeMatrix(temp,A);
}

DComplexSymBandMat operator+(const DComplexSymBandMat& A, const DComplexSymBandMat& B) {
	verifyMatch(A,B);
	DComplexVec temp(A.dataVec()+B.dataVec());
	return sameShapeMatrix(temp,A);
}

DComplexSymBandMat operator-(const DComplexSymBandMat& A, const DComplexSymBandMat& B) {
	verifyMatch(A,B);
	DComplexVec temp(A.dataVec()-B.dataVec());
	return sameShapeMatrix(temp,A);
}

DComplexSymBandMat operator*(const DComplexSymBandMat& A, DComplex x) {
	DComplexVec temp(A.dataVec()*x);
	return sameShapeMatrix(temp,A);
}

/*
 * Out-of-line version for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
  DComplexSymBandMat operator*(DComplex x, const DComplexSymBandMat& A) { return A*x; }
#endif

DComplexSymBandMat operator/(const DComplexSymBandMat& A, DComplex x) {
	DComplexVec temp(A.dataVec()/x);
	return sameShapeMatrix(temp,A);
}





DoubleSymBandMat abs(const DComplexSymBandMat& A) {
	DoubleVec temp(abs(A.dataVec()));
	return sameShapeRealMatrix(temp,A);
}


DComplexSymBandMat conj(const DComplexSymBandMat& A) {
	DComplexVec temp(conj(A.dataVec()));
	return sameShapeMatrix(temp,A);
}

DoubleSymBandMat real(const DComplexSymBandMat& A) {
	DoubleVec temp(real(A.dataVec()));
	return sameShapeRealMatrix(temp,A);
}

DoubleSymBandMat imag(const DComplexSymBandMat& A) {
	DoubleVec temp(imag(A.dataVec()));
	return sameShapeRealMatrix(temp,A);
}

DoubleSymBandMat norm(const DComplexSymBandMat& A) {
	DoubleVec temp(norm(A.dataVec()));
	return sameShapeRealMatrix(temp,A);
}

DoubleSymBandMat arg(const DComplexSymBandMat& A) {
	DoubleVec temp(arg(A.dataVec()));
	return sameShapeRealMatrix(temp,A);
}



DComplexSymBandMat transpose(const DComplexSymBandMat& A) { return A; }

DComplexVec product(const DComplexSymBandMat& A, const DComplexVec& x)
{
    if (A.cols()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_MATVECPROD,A.className,A.rows(),A.cols(),x.length())));
    }
    DComplexVec y(A.rows(),rwUninitialized);
    y = DComplex(0,0);
    int l = A.lowerBandwidth();	// Getting these as ints avoids
    int u = A.upperBandwidth();	// conversion problems
    for( int i=(-l); i<=u; i++ ) {
      int iabs = (i>=0)?i:(-i);
      DComplexVec yslice = y.slice( (i<0)?iabs:0, x.length()-iabs );
      DComplexVec xslice = x.slice( (i>0)?iabs:0, x.length()-iabs );
      /* The * in the next line is element by element multiplication */
      yslice += A.diagonal(i) * xslice;
    }
    return y;
}

DComplexVec product(const DComplexVec& x, const DComplexSymBandMat& A)
{
    if (A.rows()!=x.length()) {
      RWTHROW(RWInternalErr(RWMessage(RWLAPK_VECMATPROD,x.length(),A.className,A.rows(),A.cols())));
    }
    return product(A,x);
}



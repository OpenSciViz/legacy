#if 0
/*
 * The if expression evaluator implementation for pp
 *
 * $Header: /users/rcs/pp/ifexpr.cpp,v 1.1 1993/01/23 00:04:53 alv Exp $
 *
 * There is a main() test program at the end of this file.
 * To compile test program:
 * CC -c -D__ATT2__ -I../../rw string.cc
 * CC -DTEST -D__ATT2__ -I../../rw ifexpr.cc string.o
 *
 * Copyright (1991) by Rogue Wave Software, Inc.  This software
 * is subject to copyright protection under the laws of the
 * United States and other countries.
 * 
 * Written by Al Vermeulen.
 * 
 * Limited license.
 *
 * $Log: ifexpr.cpp,v $
 * Revision 1.1  1993/01/23  00:04:53  alv
 * Initial revision
 *
 * 
 *    Rev 1.3   25 Oct 1991 10:30:00   keffer
 * No change.
 * 
 *    Rev 1.2   22 Aug 1991 09:25:18   keffer
 * Ported to cfront
 * 
 *    Rev 1.1   16 Aug 1991 08:43:02   keffer
 * Added PVCS keywords
 */
#endif

#include "pp.h"

String IfExpr::space(" ");
String IfExpr::tab("\t");
String IfExpr::newline("\n");
String IfExpr::orToken("||");
String IfExpr::andToken("&&");
String IfExpr::lparenToken("(");
String IfExpr::rparenToken(")");
String IfExpr::equalToken("==");
String IfExpr::nequalToken("!=");

IfExpr::IfExpr(const String& s)
  : token(""), theRest(s)
{
};

IfExpr::~IfExpr() {}

int IfExpr::evaluate()
{
  nextToken();
  int value = expr();
  if (token.length()>0) err("Tokens left after parsing if expr");
  return value;
}

void IfExpr::nextToken()
{
  token = theRest.nextWord();
  DBG("Grabbed this token: >"+token+"<");
}

int IfExpr::expr()
{
  DBG("In expr");
  int value = term();
  while(token==orToken) {
    nextToken();
    int rhs = term();
    value = (value==1 || rhs==1) ? 1 : 0;
  }
  DBG("expr returns "+String(value?"true":"false"));
  return value;
}

int IfExpr::term()
{
  DBG("In term");
  int value = factor();
  while(token==andToken) {
    nextToken();
    int rhs = factor();
    value = (value==1 && rhs==1) ? 1 : 0;
  }
  DBG("term returns "+String(value?"true":"false"));
  return value;
}

int IfExpr::factor()
{
  DBG("In factor");
  int where;
  int value = 0;
  if ((where=token.find(equalToken))>=0) {  // A token like xx==yy
    value = (token.substr(0,where)==token.substr(where+2)) ? 1 : 0;
    nextToken();
  }
  else if ((where=token.find(nequalToken))>=0) {  // A token like xx!=yy
    value = (token.substr(0,where)==token.substr(where+2)) ? 0 : 1;
    nextToken();
  }
  else if (token==lparenToken) {
    nextToken();
    value = expr();
    if (token==rparenToken) {
      nextToken();
    } else {
      err("Didn't find expected right paren in if-expr");
    }
  } else {
    err("Expected x==y or x!=y or left paren in if-expr");
  }
  DBG("factor returns "+String(value?"true":"false"));
  return value;
}

#ifdef TEST
main()
{
  String expr("");
  int val;

  expr = "A==A";
  val = IfExpr(expr).evaluate();
  cout << expr << ": " << val << "\n"; cout.flush();

  expr = "A==B";
  val = IfExpr(expr).evaluate();
  cout << expr << ": " << val << "\n"; cout.flush();

  expr = "A!=A";
  val = IfExpr(expr).evaluate();
  cout << expr << ": " << val << "\n"; cout.flush();

  expr = "A!=B";
  val = IfExpr(expr).evaluate();
  cout << expr << ": " << val << "\n"; cout.flush();

  expr = "( A!=B )";
  val = IfExpr(expr).evaluate();
  cout << expr << ": " << val << "\n"; cout.flush();

  expr = "A!=B && B==B";
  val = IfExpr(expr).evaluate();
  cout << expr << ": " << val << "\n"; cout.flush();

  expr = "A==B && B==B";
  val = IfExpr(expr).evaluate();
  cout << expr << ": " << val << "\n"; cout.flush();

  expr = "A!=B || B==B";
  val = IfExpr(expr).evaluate();
  cout << expr << ": " << val << "\n"; cout.flush();

  expr = "A==B || B!=B";
  val = IfExpr(expr).evaluate();
  cout << expr << ": " << val << "\n"; cout.flush();

  expr = "( A==B || A!=B ) && C==C";
  val = IfExpr(expr).evaluate();
  cout << expr << ": " << val << "\n"; cout.flush();

  return 0;
}
#endif

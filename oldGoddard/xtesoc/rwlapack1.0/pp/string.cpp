#if 0
/*
 * The string class implementation for pp
 *
 * $Header: /users/rcs/pp/string.cpp,v 1.4 1993/07/05 17:54:15 alv Exp $
 *
 * There is a main() test program at the end of this file.
 * To compile the test program: CC -DTEST -D__ATT2__ -I../../rw string.cc
 *
 * Copyright (1991) by Rogue Wave Software, Inc.  This software
 * is subject to copyright protection under the laws of the
 * United States and other countries.
 * 
 * Written by Al Vermeulen.
 * 
 * Limited license.
 *
 * $Log: string.cpp,v $
 * Revision 1.4  1993/07/05  17:54:15  alv
 * removed dependence on rw include files
 * RWVECTOR_DELEETE -> delete []
 *
 * Revision 1.3  1993/01/29  17:40:00  alv
 * delete [n] -> RWVECTOR_DELETE(n)
 *
 * Revision 1.2  1993/01/23  00:56:51  alv
 * removed DOS EOF
 *
 * Revision 1.1  1993/01/23  00:04:54  alv
 * Initial revision
 *
 * 
 *    Rev 1.3   25 Oct 1991 10:31:32   keffer
 * No change.
 * 
 *    Rev 1.2   22 Aug 1991 09:25:16   keffer
 * Ported to cfront
 * 
 *    Rev 1.1   16 Aug 1991 08:43:08   keffer
 * Added PVCS keywords
 */
#endif

#include "pp.h"

#ifdef HAS_MEMCPY
#  if defined (BSD)
#    include <mem.h>
#  else
#    include <string.h>
#  endif
#endif

#ifdef HAS_STRSTR
#include <string.h>
#endif

char& String::operator[](int i) const
{
  if (i<0 || i>n) err("String[] - out of bounds");
  return str[i];
}

String::String(unsigned l)
{
  str = new char [l+1];
  str[l] = '\0';
  n = l;
}

String::String(const char *s)
{
  for( register int l=0; s[l]!='\0'; l++ ) { }
  str = new char[l+1];
  n = l;
#ifdef HAS_MEMCPY
	memcpy( str, s, l+1 );
#else
	for( ; l>=0; l-- ) { str[l]=s[l]; }
#endif
}

String::String(const String& s)
{
  n = s.n;
  str = new char[n+1];
#ifdef HAS_MEMCPY
	memcpy( str, s.str, n+1 );
#else
	for( register int l=n; l>=0; l-- ) { str[l]=s.str[l]; }
#endif
}

String::~String()
{
  delete [] str;
}

void String::replace(int start, unsigned len, const String& s )
{
  register int i;
  if (start<0 || start+len>n) err("Internal error in String::replace");
  char* oldstr = str;
  unsigned oldn = n;
  n += s.length() - len;
  str = new char [n+1];
#ifdef HAS_MEMCPY
	memcpy( str, oldstr, start );
	memcpy( str+start, s.str, s.length() );
	memcpy( str+start+s.length(), oldstr+start+len, oldn-start-len+1 );
#else
  for(i=0; i<start; i++) { str[i]=oldstr[i]; }
  for(i=0; i<s.length(); i++) { str[start+i]=s.str[i]; }
  for(i=start+len; i<=oldn; i++) { str[s.length()-len+i]=oldstr[i]; }
#endif
  delete [] oldstr;
}

int String::find(const String& s) const
{
#ifdef HAS_STRSTR
	char *index = strstr(str,s.str);
	if (!index) return -1;
	else return index-str;
#else
  register int i,j;
  if (s.length()<1) return 0;
  if (s.length()>n) return -1;
  for(i=0; i<n-s.length()+1; i++) {
    if (str[i]==s.str[0]) {
      int matches=1;	// A flag indicating so far the match is good
      for(j=1; matches==1 && j<s.length(); j++) {
	if (str[i+j]!=s.str[j]) matches=0;
      }
      if (matches) return i;
    }
  }
  return -1;
#endif
}

int String::gsub(String& s1, String& s2)
{
  int maxsubs = n;
  int counter=0;
  int where;
  while ( (where=find(s1))>=0 ) {
    replace(where,s1.length(),s2);
    counter++;
    if (counter>maxsubs) err("recursion in String::gsub");
  }
  return counter;
}

void String::toUpper()
{
  for( register int i=0; i<n; i++ )
    if(str[i]<='z' && str[i]>='a') str[i] += 'A'-'a';
}

String String::substr(int start, int len) const
{
  if (len<0 && start<=n) len=n-start;
  if (start<0 || start+len>n) err("Internal error in String::substr");
  String s(len);
  for(register int i=0; i<len; i++) s.str[i]=str[start+i];
  return s;
}

int String::operator==(const String& s) const
{
  if (n!=s.n) return 0;
  for(register int i=n-1; i>=0; i--) {
    if (str[i] != s.str[i]) return 0;
  }
  return 1;
}

String operator+(const String& s1, const String& s2)
{
  String s(s1);
  s.add(s2);
  return s;
}

String operator+(char* s1, const String& s2) { return String(s1)+s2; }
String operator+(const String& s1, char* s2) { return s1+String(s2); }

String& String::operator=(const char* s)
{
  register int l;
  delete str;
  for( l=0; s[l]!='\0'; l++ )  {}
  str = new char[l+1];
  n = l;
#ifdef HAS_MEMCPY
	memcpy( str, s, n+1 );
#else
  for( ; l>=0; l-- ) { str[l]=s[l]; }
#endif
  return *this;
}

String String::nextWord()
{
  register int a=0;	// The next word runs from str[a] through str[b]
  register int b;
  while (a<n && (str[a]==' ' || str[a]=='\t' || str[a]=='\n')) a++;
  b = a;
  while (b<n && (str[b]!=' ' && str[b]!='\t' && str[b]!='\n')) b++;
  String word = substr(a,b-a);
  remove(0,b);
  return word;
}

ostream& operator<<(ostream& s, const String& string)
{
  return s << (const char*)string;
}

#ifdef TEST
main()
{
  String s1("Hi there Bob.");
  String s2 = s1;
  cout << "s1=" << s1 << "\ns2=" << s2 << "\n";

  cout << "s1+s2=" << s1+s2 << "\n";

  cout << "s1==s1? " << ((s1==s2)?"True":"False") << "\n";

  cout << "Substituting Fred for Bob in 1 " << s1.gsub("Bob","Fred");
  cout << " times\n";
  cout << "s1=" << s1 << "\ns2=" << s2 << "\n";
  cout << "Substituting a for re in 1 " << s1.gsub("re","a");
  cout << " times\n";
  cout << "s1=" << s1 << "\ns2=" << s2 << "\n";

  cout << "s1==s1? " << ((s1==s2)?"True":"False") << "\n";

  cout << "s2.substr(3,5)=" << s2.substr(3,5) << "\n";
  cout << "s2.substr(3)=" << s2.substr(3) << "\n";

  cout << "Splicing s1 and s2 together\n"; s1.add(s2);
  cout << "s1=" << s1 << "\ns2=" << s2 << "\n";

  cout << "Removing the th in there in s2\n"; s2.remove(3,2);
  cout << "s1=" << s1 << "\ns2=" << s2 << "\n";

  cout << "Converting s1 to upper case\n"; s1.toUpper();
  cout << "s1=" << s1 << "\ns2=" << s2 << "\n";

  cout << "Length of s1 = " << s1.length() << "\n";
  cout << "Length of s2 = " << s2.length() << "\n";

  String s3 = "  Hello. 	word2 word3 and x then \n word5  ";
  cout << "s3=" << s3 << "\n";
  String word("");
  while( (word=s3.nextWord()).length()>0 ) {
    cout << "word: >" << word << "<\n";
  }

  return 0;
}
#endif


#if 0
/*
 * The implementation of mathpp
 *
 * $Header: /users/rcs/pp/mathpp.cpp,v 1.2 1993/03/15 19:40:27 alv Exp $
 *
 * Copyright (1991) by Rogue Wave Software, Inc.  This software
 * is subject to copyright protection under the laws of the
 * United States and other countries.
 * 
 * Written by Al Vermeulen.
 * 
 * Limited license.
 *
 * $Log: mathpp.cpp,v $
 * Revision 1.2  1993/03/15  19:40:27  alv
 * removed macros OPEN[IO]STREAM
 *
 * Revision 1.1  1993/01/23  00:04:53  alv
 * Initial revision
 *
 * 
 *    Rev 1.5   15 Nov 1991 09:13:18   keffer
 *  
 * 
 *    Rev 1.4   25 Oct 1991 10:31:28   keffer
 * No change.
 * 
 *    Rev 1.3   24 Sep 1991 14:04:12   keffer
 * Ported to Zortech V3.0
 * 
 *    Rev 1.2   22 Aug 1991 09:25:02   keffer
 * Ported to cfront
 * 
 *    Rev 1.1   16 Aug 1991 08:37:40   keffer
 * Added copyright notices.
 */
#endif

char cright[] = "(C) Copyright (1991) Rogue Wave Software, Inc.";

#include "mathpp.h"
#ifdef __ZTC__
#  include <fstream.hpp>
#else
#  ifdef __GLOCK__
#    include <fstream.hxx>
#  else
#    include <fstream.h>
#  endif
#endif

extern "C" { void exit(int); }

void err(const char *s) { err(String(s)); }
void warn(const char *s) { warn(String(s)); }
void debug(const char *s) { debug(String(s)); }

void err(const String& s)
{
  cerr << "pp: error: line " << PpFile::currentLine;
  cerr << " of file: " << PpFile::currentFile << "\n";
  cerr << "pp: " << s << "\n";
  exit(-1);
}

void warn(const String& s)
{
  cerr << "pp: warning: line " << PpFile::currentLine;
  cerr << " of file: " << PpFile::currentFile << "\n";
  cerr << "pp: " << s << "\n";
}

// void debug(const String& s)
void debug(const String&)
{
  // cerr << "pp: debug: " << s << "\n";
}

main(int argc, char **argv)
{
  if (argc<3) {
    err("usage: mathpp template target [-Ipath] [<C>=Classname] ...");
  }
  ofstream outs(argv[2]);

  if (outs.fail()) err(String("Problem opening file: ")+argv[2]);

  LinppFile ppfile(String(argv[1]),&outs,argc,argv);
	ppfile.process();

  return 0;
}

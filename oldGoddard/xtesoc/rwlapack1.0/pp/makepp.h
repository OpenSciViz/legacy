#if 0
/*
 * makepp:  A generalization of pp for building makefiles
 *
 * $Header: /users/rcs/pp/makepp.h,v 1.3 1993/10/14 15:47:25 alv Exp $
 *
 * This uses a class derived from PpFile to implement a few
 * additional commands.
 *
 *
 * Copyright (1991) by Rogue Wave Software, Inc.  This software
 * is subject to copyright protection under the laws of the
 * United States and other countries.
 * 
 * Written by Al Vermeulen.
 * 
 * Limited license.
 *
 * $Log: makepp.h,v $
 * Revision 1.3  1993/10/14  15:47:25  alv
 * increased MAXNUMOBJLINES for lapack source
 *
 * Revision 1.2  1993/01/23  00:58:17  alv
 * removed DOS EOF
 *
 * Revision 1.1  1993/01/23  00:04:54  alv
 * Initial revision
 *
 * 
 *    Rev 1.5   25 Oct 1991 10:31:28   keffer
 * No change.
 * 
 *    Rev 1.4   02 Sep 1991 15:14:54   keffer
 * Now supports Glockenspiel
 * 
 *    Rev 1.3   22 Aug 1991 09:25:18   keffer
 * Ported to cfront
 * 
 *    Rev 1.2   16 Aug 1991 08:43:08   keffer
 * Added PVCS keywords
 * 
 *    Rev 1.1   16 Aug 1991 08:29:48   keffer
 * 15 Aug 1991 revisions from Al
 * 
 *    Rev 1.0   03 Aug 1991 10:38:04   keffer
 * Initial revision.
*/
#endif

const int MAXNUMOBJLINES = 200;

#include "pp.h"

class MakeppFile : public PpFile {
protected:
  static String* objFileList[MAXNUMOBJLINES];
  static String* definename[MAXNUMOBJLINES];
  static int     numObjLines;
  static String  objToken;
  static String  cppToken;
  static String  libListToken;
  static String  executableToken;
  static String  someLibListToken;
  static String  otherLibListToken;
  
public:
  MakeppFile(const String& file, istream*, ostream*);
  virtual void processargs(int argc, char **argv);  // Process argv[3] and on
  virtual void processCommand();
  virtual void include();
  virtual void obj();
  virtual void cpp();
  virtual void libList();
  virtual void executable();
  virtual void someLibList();
  virtual void otherLibList();
  virtual void subs();
};


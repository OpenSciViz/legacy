#if 0
/*
 * linpp:  A generalization of pp for the linpack.h++ project
 *
 * $Header: /users/rcs/pp/mathpp.h,v 1.1 1993/01/23 00:04:54 alv Exp $
 *
 * This uses a class derived from PpFile to implement a few
 * additional commands.
 *
 * Copyright (1991) by Rogue Wave Software, Inc.  This software
 * is subject to copyright protection under the laws of the
 * United States and other countries.
 * 
 * Written by Al Vermeulen.
 * 
 * Limited license.
 *
 * $Log: mathpp.h,v $
 * Revision 1.1  1993/01/23  00:04:54  alv
 * Initial revision
 *
 * 
 *    Rev 1.3   25 Oct 1991 10:31:28   keffer
 * No change.
 * 
 *    Rev 1.2   22 Aug 1991 09:25:20   keffer
 * Ported to cfront
 * 
 *    Rev 1.1   16 Aug 1991 08:43:10   keffer
 * Added PVCS keywords
 * 
 *    Rev 1.0   03 Aug 1991 10:38:04   keffer
 * Initial revision.
*/
#endif

#include "pp.h"

const int MAXDIRS = 5;	// Maximum number -I flags

class LinppFile : public PpFile {
protected:
  static String beginwrapperToken;
  static String endwrapperToken;
  static String copyrightToken;
  static String mathincludeToken;
	static String colsToken;
	static String nocolsToken;
	static String *dirs[MAXDIRS];	// Directories in which to look for files
	static int    col1,col2,col3; // Column widths (or 0 if not columnizing)

public:
  LinppFile(const String& file, ostream*);
  LinppFile(const String& file, ostream*, int argc, char **argv);
	virtual void processargs(int argc, char **argv);  // Process argv[3] and on
  virtual void processCommand();
	virtual int  processLine();    // Take over this function for cols benefit
	virtual void cols();
	virtual void nocols();
	virtual void columnize();
	virtual void deTab();
	virtual void setColumn(int,int);
  virtual void beginwrapper();
  virtual void endwrapper();
  virtual void copyright();
  virtual void mathinclude();
  virtual void include();
	virtual int  getLine();
	virtual istream *openFile(const String&);
};

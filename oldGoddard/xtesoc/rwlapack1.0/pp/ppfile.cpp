#if 0
/*
 * The ppFile class implementation for pp
 *
 * $Header: /users/rcs/pp/ppfile.cpp,v 1.4 1993/09/14 18:47:33 alv Exp $
 *
 * Copyright (1991) by Rogue Wave Software, Inc.  This software
 * is subject to copyright protection under the laws of the
 * United States and other countries.
 * 
 * Written by Al Vermeulen.
 * 
 * Limited license.
 *
 * $Log: ppfile.cpp,v $
 * Revision 1.4  1993/09/14  18:47:33  alv
 * checks if input is over using !good() rather than fail() which
 * fixes strange bug
 *
 * Revision 1.3  1993/03/31  19:09:08  alv
 * fixed typo
 *
 * Revision 1.2  1993/03/15  19:40:27  alv
 * removed macros OPEN[IO]STREAM
 *
 * Revision 1.1  1993/01/23  00:04:54  alv
 * Initial revision
 *
 * 
 *    Rev 1.5   15 Nov 1991 09:13:20   keffer
 *  
 * 
 *    Rev 1.4   25 Oct 1991 10:31:30   keffer
 * No change.
 * 
 *    Rev 1.3   24 Sep 1991 14:04:10   keffer
 * Ported to Zortech V3.0
 * 
 *    Rev 1.2   22 Aug 1991 09:25:10   keffer
 * Ported to cfront
 * 
 *    Rev 1.1   16 Aug 1991 08:43:06   keffer
 * Added PVCS keywords
 */
#endif

#include "pp.h"
#  ifdef __ZTC__
#    include <fstream.hpp>
#  else
#    ifdef __GLOCK__
#      include <fstream.hxx>
#    else
#      include <fstream.h>
#    endif
#  endif

char 	PpFile::linebuf[MAXLINELENGTH];
String *PpFile::macros[MAXNUMMACROS];
String *PpFile::replacements[MAXNUMMACROS];
int 	PpFile::numMacros = 0;;
PpFile *PpFile::openFiles[MAXNUMPPFILES];
int 	PpFile::numOpenFiles = 0;
String 	PpFile::currentFile = "";
int 	PpFile::currentLine = 0;

String PpFile::defineToken     = "define";
String PpFile::ifToken      = "if";
String PpFile::elseToken    = "else";
String PpFile::elifToken    = "elif";
String PpFile::endifToken   = "endif";
String PpFile::includeToken = "include";
String PpFile::errToken     = "err";
String PpFile::warnToken     = "warn";
String PpFile::commentToken = "#";

PpFile::PpFile(const String& fname, istream *i, ostream *o)
  : filename(fname), line(""), command("")
{
  linenumber=0;
  ins = i;
  outs = o;
  iflevel = 0;
  leveltrue = 0;
  seenelse[0] = 1;

  if (numOpenFiles>=MAXNUMPPFILES) err("Too many levels of include");
  openFiles[numOpenFiles++] = this;
  currentFile = filename;
  currentLine = 0;
}

PpFile::~PpFile()  {
  if (openFiles[numOpenFiles-1]!=this) {
    err("Internal error: includes ending out of synch");
  }
  if (iflevel!=0) {
    warn("endif missing");
  }
  numOpenFiles--;
}

int PpFile::process()
{
  currentFile = filename;
  currentLine = 0;
  while (processLine()==0)  {}
  return outs->eof();
}

int PpFile::processLine()
{
  if (getLine()==0) {
    currentLine = linenumber;
    subs();
    if (parseCommand()) {
      processCommand();
    } else {
      if (leveltrue<iflevel) {
	/* Do nothing */
      } else {
	       *outs << line << "\n";
      }
    }
    return 0;
  }
  return 1;
}

int PpFile::getLine()
{
  ins->getline(linebuf,MAXLINELENGTH);
  if (!ins->good()) {
    line = "";
    if (!ins->eof()) warn("File read failure, but not end of file");
    return 1;
  } else {
    linenumber++;
#ifdef __ZTC__
    line.operator=(linebuf);
#else
    line = linebuf;
#endif
    if (line.length() > MAXLINELENGTH-2) {
      warn("Max line length exceeded");
    }
    return 0;
  }
}

void PpFile::subs()
{
	for( int i=0; i<numMacros; i++ ) {
    line.gsub( *macros[i], *replacements[i] );
  }
}

int PpFile::parseCommand()
{
  if (line.length()>0 && line[0]=='>') {
    line.remove(0,1);	// Eliminate the leading >
    command = line.nextWord();
    return 1;
  }
  return 0;
}

void PpFile::processCommand()
{
  if (leveltrue<iflevel) {	// Then we're in a false block
         if (command==ifToken)      ifCommand();
    else if (command==elseToken)    elseCommand();
    else if (command==elifToken)    elif();
    else if (command==endifToken)   endif();
    else if (command==defineToken)  {}
    else if (command==includeToken) {}
    else if (command==errToken)     {}
    else if (command==warnToken)    {}
    else if (command==commentToken) {}
    else if (command.length()==0)   {}
    else if (command[0]=='#')       {}
    else err("Unknown command: "+command);
  } else {			// We are in a true block
	 if (command==ifToken)      ifCommand();
    else if (command==elseToken)    elseCommand();
    else if (command==elifToken)    elif();
    else if (command==endifToken)   endif();
    else if (command==defineToken)  define();
    else if (command==includeToken) include();
    else if (command==errToken)     errCommand();
    else if (command==warnToken)    warnCommand();
    else if (command==commentToken) {}
    else if (command.length()==0)   {}
    else if (command[0]=='#')       {}
    else err("Unknown command: "+command);
  }
}

void PpFile::define()
{
  String eqToken("=");
  String word("");
  while ( (word=line.nextWord()).length()!=0 ) {
    int where = word.find(eqToken);
    if (where<0) {
      warn("No equal sign in set argument: "+word);
    } else {
      addMacro(word.substr(0,where),word.substr(where+1));
    }
  }
}

void PpFile::ifCommand()
{
  if (++iflevel > MAXIFLEVEL) err("Too many levels of if");
  iseliflevel[iflevel] = 0;
  seenelse[iflevel] = 0;
  IfExpr expr(line);
  if ( expr.evaluate() && leveltrue==iflevel-1) leveltrue++;
}

void PpFile::elseCommand()
{
  if (seenelse[iflevel]) {
    err("else or elif with no matching if");
  }
  seenelse[iflevel]=1;
  if (leveltrue==iflevel) {
    leveltrue--;
  } else if (leveltrue==iflevel-1) {
    leveltrue++;
  }
}

void PpFile::elif()
{
  elseCommand();
  ifCommand();
  iseliflevel[iflevel]=1;
}

void PpFile::endif()
{
  if ( --iflevel < 0 ) {
    err("endif with no matching if");
    iflevel=0;
  }
  if (leveltrue>iflevel) leveltrue=iflevel;
  if (iseliflevel[iflevel+1]==1) {
    endif();
  }
}

void PpFile::include()
{
  String file = line.nextWord();
  ifstream infilestream((const char*)file);
  if (infilestream.fail()) {
    warn("Problem opening file >"+file+"<");
  } else {
    PpFile ppfile(file, &infilestream, outs);
    ppfile.process();
    currentLine = linenumber;
    currentFile = filename;
  }
}

void PpFile::errCommand()
{
  err("err command hit: "+line);
}

void PpFile::warnCommand()
{
  warn("warn command: "+line);
}

void PpFile::addMacro(const String& mac, const String& rep)
{
	debug("addMacro: "+mac);
  int duplicate=0;
  for( int i=0; duplicate==0 && i<numMacros; i++ ) {
    if (*macros[i]==mac) duplicate=i;
  }
  if (duplicate) {
    warn("Redefining macro "+mac);
    *replacements[duplicate]=rep;
  } else {
    if (numMacros>=MAXNUMMACROS) err("Out of space for macros");
    macros[numMacros] = new String(mac);
    replacements[numMacros] = new String(rep);
    numMacros++;
  }
}

#if 0
/*
 * The implementation of makepp
 *
 * $Header: /users/rcs/pp/makepp.cpp,v 1.11 1993/10/04 21:46:13 alv Exp $
 *
 * Copyright (1991) by Rogue Wave Software, Inc.  This software
 * is subject to copyright protection under the laws of the
 * United States and other countries.
 * 
 * Written by Al Vermeulen.
 * 
 * Limited license.
 *
 * $Log: makepp.cpp,v $
 * Revision 1.11  1993/10/04  21:46:13  alv
 * ported to Windows NT
 *
 * Revision 1.10  1993/07/22  21:31:35  alv
 * Microsoft executable stack size bumped up to 16K for LAPACK.h++ tests
 *
 * Revision 1.9  1993/07/22  20:01:05  alv
 * added /noe option to link line for Microsoft, increased
 * number segments for Microsoft
 *
 * Revision 1.8  1993/03/26  19:22:06  alv
 * uncommented out libList so executables depend on libraries with Borland.
 * I wonder why libList was commented out before.
 *
 * Revision 1.7  1993/03/15  19:40:27  alv
 * removed macros OPEN[IO]STREAM
 *
 * Revision 1.6  1993/03/04  21:40:09  alv
 * now just one Borland makefile
 *
 * Revision 1.5  1993/03/01  18:06:59  alv
 * ported to Metaware High C++
 *
 * Revision 1.4  1993/02/01  20:19:43  alv
 * added options when building Microsoft executables:
 *    /F:1000 to increase stack size due to problems in matxtest/chermt1 eg
 *    /seg:192 to increase num segments due to probs in lpaktest/cchtst eg
 *
 * Revision 1.3  1993/01/27  20:25:24  alv
 * compiler for unix now unix, not ATT
 *
 * Revision 1.2  1993/01/25  17:45:42  alv
 * Fixed so include file paths always contain forward slashes
 *
 * Revision 1.1  1993/01/23  00:04:53  alv
 * Initial revision
 *
 * 
 *    Rev 1.15   12 Dec 1991 16:39:46   keffer
 * Ported to Borland C++ V3.0
 * 
 *    Rev 1.13   13 Nov 1991 11:00:44   keffer
 * Ported to Glock on an RS6000.
 * 
 *    Rev 1.9   24 Sep 1991 14:04:10   keffer
 * Ported to Zortech V3.0
 * 
 *    Rev 1.8   07 Sep 1991 10:59:48   keffer
 * Ported again to cfront.
 * 
 *    Rev 1.7   07 Sep 1991 10:56:04   keffer
 * Applied IRIS patches
 * 
 *    Rev 1.6   02 Sep 1991 15:14:54   keffer
 * Now supports Glockenspiel
 * 
 */
#endif

const char cright[] = "(C) Copyright (1991-1993) Rogue Wave Software, Inc.";

#include "makepp.h"
#  ifdef __ZTC__
#    include <fstream.hpp>
#  else
#    ifdef __GLOCK__
#      include <fstream.hxx>
#    else
#      include <fstream.h>
#    endif
#  endif

extern "C" { void exit(int); }

void err(const char *s) { err(String(s)); }
void warn(const char *s) { warn(String(s)); }
void debug(const char *s) { debug(String(s)); }

String *MakeppFile::objFileList[MAXNUMOBJLINES];
String *MakeppFile::definename[MAXNUMOBJLINES];
int    MakeppFile::numObjLines = 0;

String MakeppFile::cppToken          = "cpp";
String MakeppFile::objToken          = "obj";
String MakeppFile::libListToken      = "libList";
String MakeppFile::executableToken   = "executable";
String MakeppFile::someLibListToken  = "someLibList";
String MakeppFile::otherLibListToken = "otherLibList";

void err(const String& s)
{
  cerr << "pp: error: line " << PpFile::currentLine;
  cerr << " of file: " << PpFile::currentFile << "\n";
  cerr << "pp: " << s << "\n";
  exit(-1);
}

void warn(const String& s)
{
  cerr << "pp: warning: line " << PpFile::currentLine;
  cerr << " of file: " << PpFile::currentFile << "\n";
  cerr << "pp: " << s << "\n";
}

// void debug(const String& s)
void debug(const String&)
{
  // cerr << "pp: debug: " << s << "\n";
}

main(int argc, char **argv)
{
  if (argc<3) {
    err("usage: makepp template target [RW_COMPILER_RW=Compiler] ...");
  }
  
  ifstream ins(argv[1]);
  ofstream outs(argv[2]);
  
  if (ins.fail()) err(String("Problem opening file: ")+argv[1]);
  if (outs.fail()) err(String("Problem opening file: ")+argv[2]);
  
  MakeppFile ppfile(String(argv[1]),&ins,&outs);
  ppfile.processargs(argc,argv);
  ppfile.process();
  
  return 0;
}

MakeppFile::MakeppFile(const String& fname, istream *i, ostream *o)
  : PpFile(fname,i,o)
{
}

void MakeppFile::processargs(int argc, char **argv)
{
  for( int i=3; i<argc; i++ ) {
    line = String(argv[i]);
    define();			// Interpret argument as a define, ie XXX=YYY
  }
}

void MakeppFile::processCommand()
{
  if (leveltrue<iflevel) {	// Then we're in a false block
    if (command==objToken)                  {}
    else if (command==cppToken)             {}
    else if (command==libListToken)         {}
    else if (command==executableToken)      {}
    else if (command==someLibListToken)     {}
    else if (command==otherLibListToken)    {}
    else PpFile::processCommand();
  } else {			// We are in a true block
    if (command==objToken)                  obj();
    else if (command==cppToken)             cpp();
    else if (command==libListToken)         libList();
    else if (command==executableToken)      executable();
    else if (command==someLibListToken)     someLibList();
    else if (command==otherLibListToken)    otherLibList();
    else PpFile::processCommand();
  }
}

void MakeppFile::obj()
{
  // Check for overflow:
  if (numObjLines>=MAXNUMOBJLINES) {
    err("Too many obj or cpp lines, increase MAXNUMOBJLINES and recompile");
  }

  definename[numObjLines]  = new String(line.nextWord());
  objFileList[numObjLines] = new String(line);
  *outs << ((const char *)*definename[numObjLines]) << "=" << ((const char *)line) << "\n";
  numObjLines++;
}

void MakeppFile::cpp()
{
  String CPPString("CPP");
  String cppString("cpp");
  String OBJString("OBJ");
  String objString("obj");
  String linecopy = line;               // Relies on value (not ref) semantics
  String definename = line.nextWord();
  *outs << ((const char *)definename) << "=" << ((const char *)line) << "\n";
  line = linecopy;
  line.gsub(CPPString,OBJString);
  line.gsub(cppString,objString);
  subs();
  obj();
}

void MakeppFile::libList()
{
  String compiler = line.nextWord();

  /********************  BORLAND  ********************/

  if (compiler==String("Borland")) {
    for(int i=0; i<numObjLines; i++) {
      String line = String(" ") + *(objFileList[i]);
      for(int j=0; j<(line.length()-1); j++) {
        if (line[j]==' ' && line[j+1]!=' ') line[j]='+';
      }
      *outs << "\t" << line;
      if (i+1<numObjLines) {    // continuation for all but last line
        *outs << " & \n";
      } else {
        *outs << "\n";
      }
    }
  } 

  /************************  ZORTECH *****************/

  else if (compiler==String("Zortech")) {
    for(int i=0; i<numObjLines; i++) {
      *outs << "\t@echo $(" << ((const char*)*definename[i]) << ")   $(AMP) >> $@\n";
    }
  }

  else {
    err("No libList for compiler "+compiler);
  }
}

static String doto = ".o";

void MakeppFile::executable()
{
  String compiler= line.nextWord();
  String exename = line.nextWord();
  String objList("");
  String libList("");
  String word("");
  while ( (word=line.nextWord()).length()!=0 ) {
    if (word.find(doto)>=0) {
      objList = objList + " " + word;
    } else {
      libList = libList + " " + word;
    }
  }
  
  // These are needed to avoid a cfront "sorry not implemented" error:
  int isZortech  = compiler==String("Zortech");
  int isMicrosoft= compiler==String("Microsoft");
  int isBorland  = compiler==String("Borland");
  int isHighC   = compiler==String("HiC");

  if (isBorland) {
    *outs << exename << ".exe: " << objList << libList << "\n";
    *outs << "\t$(CPP) $(CPPFLAGS) -e" << exename << " " << objList << libList << "\n";
  } else if (isZortech) {
    *outs << exename << ".exe:\t" << objList << "\n";
    *outs << "\t$(CPP) $(CPPFLAGS) " << objList << libList << "\n";
  } else if (isMicrosoft) {             
    *outs << "!if \"$(OS)\" == \"Windows_NT\"\n";
    *outs << exename << ".exe:\t" << objList << "\n";
    *outs << "\t$(CPP) $(CPPFLAGS) -F 4000 " << objList << libList << " /link libc.lib kernel32.lib user32.lib\n";
    *outs << "!else\n";
    *outs << exename << ".exe:\t" << objList << "\n";
    *outs << "\t$(CPP) $(CPPFLAGS) -F 4000 " << objList << libList << " /link /seg:512 /noe\n";
    *outs << "!endif\n";
  } else if (compiler==String("unix")) {
    *outs << exename << ": " << objList << "\n";
    *outs << "\t$(CPP) $(CPPFLAGS) -o " << exename << " " 
      << objList << libList << " -lcomplex -lm\n";
  } else if (isHighC) {
    *outs << exename << ".exp:\t" << objList << "\n";
    *outs << "\t$(CPP) $(CPPFLAGS) -o " << exename << ".exp " << objList << libList << "\n";
  } else {
    err("No executable for compiler "+compiler);
  }
}

void MakeppFile::include()
{
  String file = line.nextWord();
  String backslash = "\\";            // Ensure all seperators in path name
  String forwardslash = "/";          // are forward slashes so that this will
  file.gsub(backslash,forwardslash);  // work under both DOS and UNIX
  ifstream infilestream((const char*)file);
  if (infilestream.fail()) {
    warn("Problem opening file >"+file+"<");
  } else {
    MakeppFile ppfile(file, &infilestream, outs);
    ppfile.process();
    currentLine = linenumber;
    currentFile = filename;
  }
}

static String slashToken = "<slash>";
static String slash = "/";
/*
 * This version of subs substitutes a / for <slash> after
 * regular macro substitutions have happened.  This lets us use
 * slashes in dos makefiles where / has been macroed to \
 */
void MakeppFile::subs()
{
  for( int i=0; i<numMacros; i++ ) {
    line.gsub( *macros[i], *replacements[i] );
  }
  line.gsub( slashToken, slash );
}

/*
 * SomeList only includes lines whose first (non space) character
 * is equal to the command's argument.  This is here for the matx
 * library, which is too big to put in one file.  We split it into
 * a complex and non-complex part, and the complex part just happens
 * to contain all lines starting with a c
 */

void MakeppFile::someLibList()
{
  String compiler = line.nextWord();
  String thechars = line.nextWord();

  if (compiler==String("Borland")) {
    for(int i=0; i<numObjLines; i++) {
      String line = String(" ") + *(objFileList[i]);
      String linecopy = line;		// Relies on value semantics
      String firstword = linecopy.nextWord();
      int useThisLine = 0;
      for(int j=0; j<thechars.length(); j++) {
        if (firstword[0]==thechars[j]) useThisLine=1;
      }
      if (useThisLine) {
        for(int i=0; i<line.length()-1; i++) {
          if (line[i]==' ' && line[i+1]!=' ') line[i]='+';
        }
        *outs << "\t" << line << " & \n";
      }
    }
  } else {
    err("No someLibList for compiler "+compiler);
  }
}

void MakeppFile::otherLibList()
{
  String compiler = line.nextWord();
  String thechars = line.nextWord();

  if (compiler==String("Borland")) {
    for(int i=0; i<numObjLines; i++) {
      String line = String(" ") + *(objFileList[i]);
      String linecopy = line;			// Relies on value semantics
      String firstword = linecopy.nextWord();
      int useThisLine = 1;
      for(int j=0; j<thechars.length(); j++) {
        if (firstword[0]==thechars[j]) useThisLine=0;
      }
      if (useThisLine) {
        for(int i=0; i<line.length()-1; i++) {
          if (line[i]==' ' && line[i+1]!=' ') line[i]='+';
        }
        *outs << "\t" << line << " & \n";
      }
    }
  } else if (compiler==String("Zortech")) {
    err("No otherlibList for Zortech yet");
  } else {
    err("No otherLibList for compiler "+compiler);
  }
}


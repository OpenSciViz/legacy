#if 0
/*
 * pp:  A simple text-based preprocessor
 *
 * $Header: /users/rcs/pp/pp.h,v 1.4 1993/07/05 17:54:56 alv Exp $
 *
 * This was originally an awk program, but that was too slow,
 * so I wrote this C++ version.  pp is a basic preprocessor, 
 * see also linpp which has some added commands specifically
 * for the linpack.h++ project.
 *
 * This header file contains definitions of 3 classes:
 *   String: a simple string manipulation class
 *   IfExpr: a class to parse if expressions
 *   PpFile: holds the state of a pre-processor input file being read
 *
 * Copyright (1991) by Rogue Wave Software, Inc.  This software
 * is subject to copyright protection under the laws of the
 * United States and other countries.
 * 
 * Written by Al Vermeulen.
 * 
 * Limited license.
 *
 * $Log: pp.h,v $
 * Revision 1.4  1993/07/05  17:54:56  alv
 * #include <rw/rstream.h>  ->  #include <iostream.h>
 * avoids dependence on rw header files
 *
 * Revision 1.3  1993/03/18  19:38:46  alv
 * made PPFile destructor virtual so g++ wouldn't complain
 *
 * Revision 1.2  1993/01/23  00:56:51  alv
 * removed DOS EOF
 *
 * Revision 1.1  1993/01/23  00:04:55  alv
 * Initial revision
 *
 * 
 *    Rev 1.3   25 Oct 1991 10:31:30   keffer
 * 
 *    Rev 1.2   22 Aug 1991 09:25:22   keffer
 * Ported to cfront
 * 
 *    Rev 1.1   16 Aug 1991 08:43:12   keffer
 * Added PVCS keywords
 * 
 *    Rev 1.0   03 Aug 1991 10:38:04   keffer
 * Initial revision.
*/
#endif

#include <iostream.h>

extern "C" { void exit(int); }

class String;

void err(const String&);	// Die die die
void warn(const String&);	// Spew a warning to standard error
void debug(const String&);
/*
 * Borland C++ seems to have a rough time automatically converting
 * char* to String, so I'll put in these explicit functions which
 * are defined in pp.cpp.  They would be inline, but Borland can't
 * inline anything with a local destructor.
 */
void err(const char*);
void warn(const char*);
void debug(const char*);

/*
 * Sorry about this, but the easiest way to code this was to use
 * fixed buffer sizes.  These things determine the size of arrays
 * used in the PpFile class.  Using linked lists or something could
 * avoid these size restrictions if it ever becomes a problem.
 */

const int MAXLINELENGTH = 1000;	// Largest line length
const int MAXNUMMACROS  = 1000; // Largest number macros
const int MAXNUMPPFILES = 5;	// Maximum include depth nesting
const int MAXIFLEVEL = 50;	// Maximum depth of if statements


#ifdef DEBUG
#define DBG(X) debug(X)
#else
#define DBG(X) /**/
#endif

/*
 * A strings class that does all the awk things I need to do
 */

class String {
private:
  char *str;	// Pointer to null terminated string on free store
  unsigned n;	// Length of the string (not counting null terminator)

protected:
  String( unsigned len );

public:
  String( const char* );
  String( const String& );
  ~String();

  unsigned length() const		{ return n; }
  operator const char* () const		{ return str; }
  char& operator[](int i) const;
  String substr(int start, int len= -1) const;  // Returns a substring

  void replace(int start, unsigned len, const String&);
  void add(const String& s)		{ replace(n,0,s); }
  inline void remove(int start, unsigned len);
  String& operator=(const String& s)	{ replace(0,n,s); return *this; }
  String& operator=(const char* s);

  int operator==(const String& s) const;

  int  find(const String&) const;	// Returns -1 if nothing found
  int  gsub(String&,String&);		// Returns number substitutions made
  void toUpper();			// Convert to upper case

  String nextWord();	// Provide the next word and eliminate it from this
			// Words are seperated by white space, like in awk.
};

static String null("");

inline void String::remove(int start, unsigned len)  { replace(start,len,null); }

ostream& operator<<(ostream&,const String&);

String operator+(const String& s1, const String& s2);
String operator+(char* s1, const String& s2);
String operator+(const String& s1, char* s2);

/*
 * Now, a class to parse if statements
 *
 * This is done similiar to code in the awk book, section 6.6.
 *
 * Grammar:
 *
 * expr: term
 *       expr || term
 *
 * term: factor
 *       term && factor
 *
 * factor: x==y
 *         x!=y
 *         ( factor )
 */

class IfExpr {
private:
  String token;		// The current token
  String theRest;	// The rest of the expression

public:
  IfExpr(const String&);	// String is: ... if <expr>
  ~IfExpr();

  int evaluate();	// Call me!  call me!

protected:
  void nextToken();
  int  expr();
  int  term();
  int  factor();

  static String space;
  static String tab;
  static String newline;
  static String orToken;
  static String andToken;
  static String lparenToken;
  static String rparenToken;
  static String equalToken;
  static String nequalToken;
};

class PpFile {
protected:
  static char 	linebuf[MAXLINELENGTH];
  static String	*macros[MAXNUMMACROS];
  static String	*replacements[MAXNUMMACROS];
  static int 	numMacros;
  static PpFile	*openFiles[MAXNUMPPFILES];
  static int 	numOpenFiles;

  static String defineToken;	// These are just strings
  static String ifToken;
  static String elseToken;
  static String elifToken;
  static String endifToken;
  static String includeToken;
  static String errToken;
  static String warnToken;
  static String commentToken;

  String filename;
  int    linenumber;
  istream *ins;
  ostream *outs;

  int iflevel;
  int leveltrue;
  int iseliflevel[MAXIFLEVEL];
  int seenelse[MAXIFLEVEL];
  String line;
  String command;

public:
  static String currentFile;
  static int	currentLine;

  PpFile(const String& fname, istream *i, ostream *o);
  virtual ~PpFile();

  virtual int  process();	// Process this file
  virtual int  processLine();	// Returns zero if more lines are waiting
  virtual int  getLine();	// Read next line into line, zero return good
  virtual void subs();		// Do macro substitutions
  virtual int  parseCommand();	// Returns 1 if this is a command
  virtual void processCommand();
  virtual void define();
  virtual void ifCommand();
  virtual void elseCommand();
  virtual void elif();
  virtual void endif();
  virtual void include();
  virtual void errCommand();
  virtual void warnCommand();
  virtual void addMacro(const String&,const String&);
};

    

#ifdef TEST

void err(const char* s) { err(String(s); }
void warn(const char* s) { warn(String(s); }
void debug(const char* s) { debug(String(s); }

void err(String& s)
{
  cerr << "err" << s;
  exit(-1);
}

void debug(String& s)
{
  cout << "debug: " << s << "\n";
}
#endif

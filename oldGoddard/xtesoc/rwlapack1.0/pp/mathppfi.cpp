#if 0
/*
 * The ppFile class implementation for pp
 *
 * $Header: /users/rcs/pp/mathppfi.cpp,v 1.4 1993/08/23 17:00:12 alv Exp $
 *
 * Copyright (1991) by Rogue Wave Software, Inc.  This software
 * is subject to copyright protection under the laws of the
 * United States and other countries.
 * 
 * Written by Al Vermeulen.
 * 
 * Limited license.
 *
 * $Log: mathppfi.cpp,v $
 * Revision 1.4  1993/08/23  17:00:12  alv
 * rewrote getLine() for more efficiency (no longer uses
 * recursion) and changed a call to ios::fail to !ios::good
 * which seemed to fix a strange problem
 *
 * Revision 1.3  1993/02/25  19:46:21  alv
 * ported to ESIX
 * updated copyright notice for lapack.h++
 *
 * Revision 1.1  1993/01/23  00:04:53  alv
 * Initial revision
 *
 * 
 *    Rev 1.4   25 Oct 1991 10:31:28   keffer
 * No change.
 * 
 *    Rev 1.3   24 Sep 1991 14:04:12   keffer
 * Ported to Zortech V3.0
 * 
 *    Rev 1.2   22 Aug 1991 09:25:04   keffer
 * Ported to cfront
 * 
 *    Rev 1.1   16 Aug 1991 08:43:02   keffer
 * Added PVCS keywords
 */
#endif

#include "mathpp.h"
#  ifdef __ZTC__
#    include <fstream.hpp>
#  else
#    include <fstream.h>
#  endif
#include <stdio.h>

#ifdef HAS_STRCHR
#include <string.h>
#endif

String LinppFile::beginwrapperToken = "beginwrapper";
String LinppFile::endwrapperToken   = "endwrapper";
String LinppFile::copyrightToken    = "copyright";
String LinppFile::mathincludeToken  = "math.h++include";
String LinppFile::colsToken         = "cols";
String LinppFile::nocolsToken       = "nocols";
String *LinppFile::dirs[MAXDIRS];
int    LinppFile::col1=0;
int    LinppFile::col2=0;
int    LinppFile::col3=0;

LinppFile::LinppFile(const String& fname, ostream *o)
  : PpFile(fname,(istream*)0,o)
{
	ins = openFile(fname);
}

LinppFile::LinppFile(const String& fname, ostream *o, int argc, char **argv)
  : PpFile(fname,(istream*)0,o)
{
	processargs(argc,argv);
	ins = openFile(fname);
}


void LinppFile::processargs(int argc, char **argv)
{
	dirs[0] = 0;
  int dirsIndex = 0;
  for( int i=3; i<argc; i++ ) {
    line = String(argv[i]);
		if (line[0]=='-') {
			switch (line[1]) {
				case 'I':
					{
					  String dir = line.substr(2);
					  dirs[dirsIndex++] = new String(dir);
					  if (dirsIndex>=MAXDIRS) {
						  err("Too many -I flags");
					  }
					  dirs[dirsIndex] = 0;
					}
					break;
				default:
					err(String("Unknown flag: ")+line);
				  break;
			}
		} else {
      define();	// Interpret argument as a define, ie XXX=YYY
		}
  }
}

void LinppFile::processCommand()
{
  if (leveltrue<iflevel) {	// Then we're in a false block
         if (command==beginwrapperToken)      {}
    else if (command==endwrapperToken)        {}
    else if (command==copyrightToken)         {}
    else if (command==mathincludeToken)       {}
    else if (command==colsToken)		  				{}
    else if (command==nocolsToken)  					{}
    else PpFile::processCommand();
  } else {			// We are in a true block
         if (command==beginwrapperToken)      beginwrapper();
    else if (command==endwrapperToken)        endwrapper();
    else if (command==copyrightToken)         copyright();
    else if (command==mathincludeToken)       mathinclude();
    else if (command==colsToken)							cols();
    else if (command==nocolsToken)						nocols();
    else PpFile::processCommand();
  }
}

void LinppFile::beginwrapper()
{
  String name = line.nextWord();
  if (name.length()==0) err("No wrapper name specified");
  name.toUpper();
  *outs << "#ifndef __RW" << name << "_H__" << "\n";
  *outs << "#define __RW" << name << "_H__" << "\n";
}

void LinppFile::endwrapper()
{
  *outs << "#endif\n";
}

void LinppFile::copyright()
{
  *outs << " * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.\n";
  *outs << " * This software is subject to copyright protection under the\n";
  *outs << " * laws of the United States and other countries.\n";
  *outs << " *\n";
  *outs << " * Written by Al Vermeulen.\n";
  *outs << " *\n";
  *outs << " * Limited license.\n";
}

void LinppFile::mathinclude()
{
  String word = line.nextWord();
  if (word.length()==0) warn("Empty mathinclude");
  *outs << "#include <" << word << ".h>\n";
}

void LinppFile::include()
{
  LinppFile ppfile(line.nextWord(), outs);
  ppfile.process();
  currentLine = linenumber;
  currentFile = filename;
}

istream* LinppFile::openFile(const String& s)
{
	String path = s;
	ifstream *strm;
	strm = new ifstream((const char*)s);
	String **dir = dirs;
	while (strm->fail()) {
		delete strm;
		if (*dir==0) {
			err("Can't open file >"+s+"<");
		} else {
			path = **dir++ + "/" + s;
			strm = new ifstream((const char*)path);
		}
	}
	return strm;
}
	
/*
 * This is a version of getLine() slightly modified from the PpFile
 * version.  In mathpp, we make the restriction that only lines
 * containing the character ">" even get looked at.  This is possible
 * because all macros happen to have a > in them.
 *
 * This used to use recursion heavily, but I've rewritten it as a loop
 * to avoid stack overflow problems.  
 */

int LinppFile::getLine()
{
  // keep looping and printing (or skipping) lines until we
  // hit a line that requires processing
  for(;;) {
    ins->getline(linebuf,MAXLINELENGTH);
    if (!ins->good()) {
      line = "";
      if (!ins->eof()) warn("File read failure, but not end of file");
      return 1;
    } else {
  
      linenumber++;
  
      // Skip ">\0" lines and "> #" lines
      if (linebuf[0]!='>' || (linebuf[1]!='\0' && (linebuf[1]!=' ' || linebuf[2]!='#'))) {
    
        // Continue processing only if either this is a command or
        // this is a true block
        if (linebuf[0]=='>' || leveltrue>=iflevel) {
      
          // If this line has a > in it, do processing, otherwise just
          // print this line.  Always bother with the line if columnizing is on.
          int botherwith = (col1==0) ? 0 : 1;
#ifdef HAS_STRCHR
          botherwith = botherwith || strchr(linebuf,'>');
#else
          for(int i=0; botherwith==0 && linebuf[i]!=0; i++) {
            if (linebuf[i]=='>') botherwith=1;
          }
#endif
          if (botherwith) {
            line=linebuf;
            if (line.length()>MAXLINELENGTH-2) {
              warn("Max line length exceeded");
            }
            return 0;
          } else {
            *outs << linebuf << "\n";
          } /* if (has a '>') else */
        } /* if (got line successfully) */
      }
    }
  }
}

/*
 * This is the same as pp's processLine, except that it calls columnize()
 * if col1 is not zero
 */

int LinppFile::processLine()
{
  if (getLine()==0) {
    currentLine = linenumber;
    subs();
    if (parseCommand()) {
      processCommand();
    } else {
      if (leveltrue<iflevel) {
	/* Do nothing */
      } else {
				 if (col1!=0 || col2!=0 || col3!=0) { columnize(); }
	       *outs << line << "\n";
      }
    }
    return 0;
  }
  return 1;
}

void LinppFile::cols()
{
	String word1 = line.nextWord();
	String word2 = line.nextWord();
	String word3 = line.nextWord();
	if ( sscanf((const char *)word1,"%d",&col1)!=1 ||
	     sscanf((const char *)word2,"%d",&col2)!=1 ||
	     sscanf((const char *)word3,"%d",&col3)!=1 ) {
		        err("cols needs 3 integer arguments for the 3 column widths");
	}
	if (col1<0 || col2<0 || col3<0) {
		err("cols must be positive");
	}
}

void LinppFile::nocols()
{
	col1=0;
	col2=0;
	col3=0;
}

/*
 * The heuristic for columnizing is as follows.  The first column
 * consists of everything up to the first word that contains a "(".
 * This word begins the second column.  Everything up to the ")"
 * character is put in this column.  If there is a semicolon after ")"
 * it goes there too.  The third column is meant for the word "const",
 * it holds everything up to the "/" or "{" character.
 *
 * The presence of tab characters will screw up this simple algorithm.
 */

	static String leftParen = "(";
	static String rightParen = ")";
void LinppFile::columnize()
{
	if (col1>99 || col2>99 || col3>99||
		  col1<0  || col2<0  || col3<0 ) {
		err("cols can't be set less than 0 or more than 99 spaces wide");
	}

	deTab();  // Replace tabs by spaces

	// Widen up the first column
	int begincol2 = line.find(leftParen);
	while(begincol2>0 && line[begincol2-1]!=' ') begincol2--;
	if (begincol2>=0) { setColumn(begincol2,col1); }

	// Widen up the second column
	int begincol3 = line.find(rightParen);
	if (begincol3<0) { begincol3=line.length(); }
	while(begincol3<line.length() && line[begincol3]!=' ') begincol3++;
	while(begincol3<line.length() && line[begincol3]==' ') begincol3++;
	if (begincol3<line.length()) { setColumn(begincol3,col2); }

	// Widen up the third column
  // Try changing the heuristic so it looks for the // or { from the beginning
  // int begincol4 = line.find(rightParen);
	// if (begincol4<0) { begincol4=line.length(); }
  int begincol4 = 1;   // Just leave it alone if // is at the very beginning of the line.
	while(begincol4+1<line.length() && (line[begincol4]!='/' || line[begincol4+1]!='/') && line[begincol4]!='{') begincol4++;
	if (begincol4+1<line.length()) { setColumn(begincol4,col3); }
}

/*
 * setColumn() arranges so that line[begin] begins at character target,
 * or at least as close as possible, by modifying the number spaces
 * just before the character line[begin].
 */

void LinppFile::setColumn(int begin, int target)
{
	static char spaces[100];
	if (begin<0 || begin>line.length()) { err("Internal error: bad begin in setColumn"); }

	// Determine number of spaces just before line[begin]
	int numspaces = 0;
	while(begin-numspaces>0 && line[begin-numspaces-1]==' ') numspaces++;

	int numSpacesDesired = target-begin+numspaces;
	if (numSpacesDesired<0) numSpacesDesired=0;    // We do what we can!
	if (numspaces>0 && numSpacesDesired==0) numSpacesDesired=1;

	if (numspaces!=numSpacesDesired) {
		int i = numSpacesDesired;     // Build a string of spaces
		if (i<0 || i>=100) {
			err("Internal error in setColumn");
		}
		spaces[i] = '\0';
		while( --i>=0 ) { spaces[i]=' '; }
		String spaceString(spaces);
		line.replace(begin-numspaces,numspaces,spaceString);
	}
}

void LinppFile::deTab()
{
	static char spaces[9];
	for(int i=0; i<line.length(); i++) {
		if (line[i]=='\t') {
			int numSpaces = 8-i%8;
			spaces[numSpaces] = '\0';
			while( --numSpaces>=0 ) { spaces[numSpaces]=' '; }
			String spString(spaces);
			line.replace(i,1,spString);
		}
	}
}

#if 0
/*
 * The implementation of pp
 *
 * $Header: /users/rcs/pp/pp.cpp,v 1.1 1993/01/23 00:04:54 alv Exp $
 *
 * Copyright (1991) by Rogue Wave Software, Inc.  This software
 * is subject to copyright protection under the laws of the
 * United States and other countries.
 * 
 * Written by Al Vermeulen.
 * 
 * Limited license.
 *
 * $Log: pp.cpp,v $
 * Revision 1.1  1993/01/23  00:04:54  alv
 * Initial revision
 *
 * 
 *    Rev 1.4   15 Nov 1991 09:13:18   keffer
 *  
 * 
 *    Rev 1.3   25 Oct 1991 10:31:30   keffer
 * No change.
 * 
 *    Rev 1.2   22 Aug 1991 09:25:12   keffer
 * Ported to cfront
 * 
 *    Rev 1.1   16 Aug 1991 08:43:04   keffer
 * Added PVCS keywords
 */
#endif

#include "pp.h"
#  ifdef __ZTC__
#    include <fstream.hpp>
#  else
#    ifdef __GLOCK__
#      include <fstream.hxx>
#    else
#      include <fstream.h>
#    endif
#  endif

extern "C" { void exit(int); }

void err(const char* s) { err(String(s)); }
void warn(const char* s) { warn(String(s)); }
void debug(const char* s) { debug(String(s)); }

void err(const String& s)
{
  cerr << "pp: error: line " << PpFile::currentLine;
  cerr << " of file: " << PpFile::currentFile << "\n";
  cerr << "pp: " << s << "\n";
  exit(-1);
}

void warn(const String& s)
{
  cerr << "pp: warning: line " << PpFile::currentLine;
  cerr << " of file: " << PpFile::currentFile << "\n";
  cerr << "pp: " << s << "\n";
}

void debug(const String& s)
{
  cerr << "pp: debug: " << s << "\n";
}

main(int, char **)
{
  PpFile ppfile("-",&cin,&cout);
  ppfile.process();

  return 0;
}

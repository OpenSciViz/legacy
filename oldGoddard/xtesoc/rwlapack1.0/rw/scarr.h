#ifndef __RWSCARR_H__
#define __RWSCARR_H__

/*
 * Declarations for SChar precision arrays
 *
 * Generated from template $Id: array.htm,v 1.13 1993/09/19 15:18:58 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311        FAX: (503) 757-7350
 *
 * Copyright (C) 1988, 1989, 1990, 1991.  This software is subject to copyright
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * Right now, functions are only provided for easy indexing up to 4D.
 * Beyond this, you need to use the general functions (ie with IntVecs).
 * It would be easy to add 5D or higher functions if need be, except that
 * the op() functions would go through a combinatorial explosion.  As 
 * long as you are content to always get back an Array, and not explicitly
 * a Vec or GenMat as the case may be, then this explosion can be avoided.
 */

/*
 * Defining the preprocessor directive RWBOUNDS_CHECK will
 * cause bounds checking on the slice function, subscripting operators
 * and inlined math functions.
 *
 * Bounds checking is always done on subscripting operators returning
 * anything other than a SChar or SChar&.
 */

#include "rw/igenmat.h"
#include "rw/array.h"

class RWRand;

class SCharArray : public RWArrayView {
private:
    SCharArray(const RWDataView&, SChar*, const IntVec&, const IntVec&); // For real() and imag() and slices

public:

    /****************
     * Constructors *
     ****************/

    SCharArray();                               // Declares a scalar (0D array)
    SCharArray(const IntVec&, RWUninitialized, Storage=COLUMN_MAJOR);
    SCharArray(unsigned,unsigned,unsigned,RWUninitialized);
    SCharArray(unsigned,unsigned,unsigned,unsigned,RWUninitialized);
    SCharArray(const IntVec&, RWRand&, Storage=COLUMN_MAJOR);
    SCharArray(unsigned,unsigned,unsigned,RWRand&);
    SCharArray(unsigned,unsigned,unsigned,unsigned,RWRand&);
    SCharArray(const IntVec& n, SChar val);
    SCharArray(unsigned,unsigned,unsigned, SChar val);
    SCharArray(unsigned,unsigned,unsigned,unsigned, SChar val);
    SCharArray(const char *);
    SCharArray(const SCharArray& a);
    SCharArray(const SChar* dat, const IntVec& n); // Copy of dat will be made
    SCharArray(const SChar* dat, unsigned,unsigned,unsigned);
    SCharArray(const SChar* dat, unsigned,unsigned,unsigned,unsigned);
    SCharArray(const SCharVec& vec, const IntVec& n); // View of dat will be taken
    SCharArray(const SCharVec& vec, unsigned,unsigned,unsigned);
    SCharArray(const SCharVec& vec, unsigned,unsigned,unsigned,unsigned);
    SCharArray(RWBlock *block, const IntVec& n); // Use a custom block for the data
    SCharArray(RWBlock *block, unsigned,unsigned,unsigned);
    SCharArray(RWBlock *block, unsigned,unsigned,unsigned,unsigned);
    SCharArray(const SCharVec&);                // View will be taken
    SCharArray(const SCharGenMat&);             // View will be taken

    operator IntArray() const;                  // Convert to IntArray

    /********************
     * Member functions *
     ********************/

    SCharArray    apply(mathFunTy)      const;
#ifdef RW_NATIVE_EXTENDED
    SCharArray    apply(XmathFunTy)     const;
#endif  
    unsigned      binaryStoreSize()     const;   // Storage requirements
    SCharArray    copy()                const;   // Copy with distinct instance variables
    SChar*        data()                         {return (SChar*)begin;}
    const SChar*  data()                const    {return (SChar*)begin;}
    SCharArray    deepCopy()            const;   // Copy with distinct instance variables 
    void          deepenShallowCopy();           // Insures only 1 ref and data is compact
//  unsigned      dimension()           const    {return npts.length();}
//  const IntVec& length()              const    {return npts;}
//  int           length(int i)         const    {return npts(i);}
    void          printOn(ostream& s)   const;   // Pretty print
    SCharArray&   reference(const SCharArray& v); // Reference self to v
    void          resize(const IntVec&);         // Will pad with zeroes if necessary
    void          resize(unsigned,unsigned,unsigned);
    void          resize(unsigned,unsigned,unsigned,unsigned);
    void          reshape(const IntVec&);        // Contents after reshape are garbage
    void          reshape(unsigned,unsigned,unsigned);
    void          reshape(unsigned,unsigned,unsigned,unsigned);
    void          restoreFrom(RWFile&);          // Restore from binary
    void          restoreFrom(RWvistream&);      // Restore from virtual stream
    void          saveOn(RWFile&)       const;   // Store using binary
    void          saveOn(RWvostream&)   const;   // Store to virtual stream
    void          scanFrom(istream& s);          // Read to eof or delimit with []
    SCharArray    slice(const IntVec& start, const IntVec& lgt, const IntGenMat& strider) const;
//  const IntVec& stride()              const    {return step;}
//  int           stride(int i)         const    {return step(i);}

    /********************
     * Member operators *
     ********************/

    SChar&          operator[](const IntVec& i); // With bounds checking
    SChar           operator[](const IntVec& i) const;
    SChar&          operator()(const IntVec& i); // With optional bounds checking
    SChar           operator()(const IntVec& i) const;
    SChar&          operator()(int,int,int);
    SChar           operator()(int,int,int) const;
    SChar&          operator()(int,int,int,int);
    SChar           operator()(int,int,int,int) const;
    SCharVec        operator()(const RWSlice&,int,int);
    const SCharVec  operator()(const RWSlice&,int,int) const;
    SCharVec        operator()(int,const RWSlice&,int);
    const SCharVec  operator()(int,const RWSlice&,int) const;
    SCharVec        operator()(int,int,const RWSlice&);
    const SCharVec  operator()(int,int,const RWSlice&) const;
    SCharVec        operator()(const RWSlice&,int,int,int);
    const SCharVec  operator()(const RWSlice&,int,int,int) const;
    SCharVec        operator()(int,const RWSlice&,int,int);
    const SCharVec  operator()(int,const RWSlice&,int,int) const;
    SCharVec        operator()(int,int,const RWSlice&,int);
    const SCharVec  operator()(int,int,const RWSlice&,int) const;
    SCharVec        operator()(int,int,int,const RWSlice&);
    const SCharVec  operator()(int,int,int,const RWSlice&) const;
    SCharGenMat     operator()(int,const RWSlice&,const RWSlice&);
    const SCharGenMat operator()(int,const RWSlice&,const RWSlice&) const;
    SCharGenMat     operator()(const RWSlice&,int,const RWSlice&);
    const SCharGenMat operator()(const RWSlice&,int,const RWSlice&) const;
    SCharGenMat     operator()(const RWSlice&,const RWSlice&,int);
    const SCharGenMat operator()(const RWSlice&,const RWSlice&,int) const;
    SCharGenMat     operator()(int,int,const RWSlice&,const RWSlice&);
    const SCharGenMat operator()(int,int,const RWSlice&,const RWSlice&) const;
    SCharGenMat     operator()(int,const RWSlice&,int,const RWSlice&);
    const SCharGenMat operator()(int,const RWSlice&,int,const RWSlice&) const;
    SCharGenMat     operator()(int,const RWSlice&,const RWSlice&,int);
    const SCharGenMat operator()(int,const RWSlice&,const RWSlice&,int) const;
    SCharGenMat     operator()(const RWSlice&,int,int,const RWSlice&);
    const SCharGenMat operator()(const RWSlice&,int,int,const RWSlice&) const;
    SCharGenMat     operator()(const RWSlice&,int,const RWSlice&,int);
    const SCharGenMat operator()(const RWSlice&,int,const RWSlice&,int) const;
    SCharGenMat     operator()(const RWSlice&,const RWSlice&,int,int);
    const SCharGenMat operator()(const RWSlice&,const RWSlice&,int,int) const;
    SCharArray      operator()(const RWSlice&,const RWSlice&,const RWSlice&);
    const SCharArray operator()(const RWSlice&,const RWSlice&,const RWSlice&) const;
    SCharArray      operator()(int,const RWSlice&,const RWSlice&,const RWSlice&);
    const SCharArray operator()(int,const RWSlice&,const RWSlice&,const RWSlice&) const;
    SCharArray      operator()(const RWSlice&,int,const RWSlice&,const RWSlice&);
    const SCharArray operator()(const RWSlice&,int,const RWSlice&,const RWSlice&) const;
    SCharArray      operator()(const RWSlice&,const RWSlice&,int,const RWSlice&);
    const SCharArray operator()(const RWSlice&,const RWSlice&,int,const RWSlice&) const;
    SCharArray      operator()(const RWSlice&,const RWSlice&,const RWSlice&,int);
    const SCharArray operator()(const RWSlice&,const RWSlice&,const RWSlice&,int) const;
    SCharArray      operator()(const RWSlice&,const RWSlice&,const RWSlice&,const RWSlice&);
    const SCharArray operator()(const RWSlice&,const RWSlice&,const RWSlice&,const RWSlice&) const;
    SCharArray& operator=(const SCharArray& v); // Must be same length as v
    SCharArray& operator=(SChar);
    RWBoolean   operator==(const SCharArray&) const;
    RWBoolean   operator!=(const SCharArray& v) const;
    SCharArray& operator+=(SChar);
    SCharArray& operator-=(SChar s)             { return operator+=(-s); }
    SCharArray& operator*=(SChar);
    SCharArray& operator/=(SChar s);
    SCharArray& operator+=(const SCharArray&);
    SCharArray& operator-=(const SCharArray&);
    SCharArray& operator*=(const SCharArray&);
    SCharArray& operator/=(const SCharArray&);
    SCharArray& operator++();                   // Prefix operator
    SCharArray& operator--();                   // Prefix operator
#if             !defined(RW_NO_POSTFIX)
    void        operator++(int)                 { ++(*this); }  // Postfix operator
    void        operator--(int)                 { --(*this); }  // Postfix operator
#endif

    friend SChar       toScalar(const SCharArray&);    // For 0-D arrays
    friend SCharVec    toVec(const SCharArray&);       // For 1-D arrays
    friend SCharGenMat toGenMat(const SCharArray&);    // For 2-D arrays
};
  
    /********************
     * Global Operators *
     ********************/

           SCharArray  operator-(const SCharArray&);
    inline SCharArray  operator+(const SCharArray& v) { return v; }
           SCharArray  operator+(const SCharArray&, const SCharArray&);
           SCharArray  operator-(const SCharArray&, const SCharArray&);
           SCharArray  operator*(const SCharArray&, const SCharArray&);
           SCharArray  operator/(const SCharArray&, const SCharArray&);
           SCharArray  operator+(const SCharArray&,SChar);
           SCharArray  operator+(SChar, const SCharArray&);
           SCharArray  operator-(const SCharArray&,SChar);
           SCharArray  operator-(SChar, const SCharArray&);
           SCharArray  operator*(const SCharArray&,SChar);
           SCharArray  operator*(SChar, const SCharArray&);
           SCharArray  operator/(const SCharArray&,SChar);
           SCharArray  operator/(SChar, const SCharArray&);
           ostream&    operator<<(ostream& s, const SCharArray& v);
           istream&    operator>>(istream& s, SCharArray& v);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
    inline SCharArray  operator*(SChar s, const SCharArray& V) {return V*s;}
    inline SCharArray  operator+(SChar s, const SCharArray& V) {return V+s;}
    inline SCharArray  operator-(const SCharArray& V, SChar s) {return V+(SChar)(-s);}
#endif


    /********************
     * Global Functions *
     ********************/

    /**** Miscellaneous Functions ****/

           SCharArray  abs(const SCharArray& V);
           SChar       dot(const SCharArray&,const SCharArray&); // Return sum_ij...k (Aij...k * Bij...k)
           SCharArray  dot(const SCharVec&,const SCharArray&); // Return sum_i (Vi * Aij...k)
           SCharArray  dot(const SCharArray&,const SCharVec&); // Return sum_k (Ai...jk * Vk)
           IntVec      maxIndex(const SCharArray&); // Returns *index* of max value
           SChar       maxValue(const SCharArray&);
           IntVec      minIndex(const SCharArray&); // Returns *index* of min value
           SChar       minValue(const SCharArray&);
           SChar       prod(const SCharArray&);
           SChar       sum(const SCharArray&);


    /**** Functions for type conversion ****/

           SCharArray  toChar(const IntArray&);

    /**** Norm functions ****/

    /***************************
     * Inline Access Functions *
     ***************************/


  inline SChar& SCharArray::operator[](const IntVec& i) {
    boundsCheck(i);
    return data()[dot(i,step)];
  }

  inline SChar SCharArray::operator[](const IntVec& i) const {
    boundsCheck(i);
    return data()[dot(i,step)];
  }

  inline SChar& SCharArray::operator()(const IntVec& i) {
#  ifdef RWBOUNDS_CHECK    
    boundsCheck(i);
#  endif
    return data()[dot(i,step)];
  }

  inline SChar SCharArray::operator()(const IntVec& i) const {
#  ifdef RWBOUNDS_CHECK    
    boundsCheck(i);
#  endif
    return data()[dot(i,step)];
  }

  inline SChar& SCharArray::operator()(int i, int j, int k) {
#  ifdef RWBOUNDS_CHECK
    boundsCheck(i,j,k);
#  endif
    return data()[i*step((int)0)+j*step(1)+k*step(2)];
  }

  inline SChar SCharArray::operator()(int i, int j, int k) const {
#  ifdef RWBOUNDS_CHECK
    boundsCheck(i,j,k);
#  endif
    return data()[i*step((int)0)+j*step(1)+k*step(2)];
  }

  inline SChar& SCharArray::operator()(int i, int j, int k, int l) {
#  ifdef RWBOUNDS_CHECK
    boundsCheck(i,j,k,l);
#  endif
    return data()[i*step((int)0)+j*step(1)+k*step(2)+l*step(3)];
  }

  inline SChar SCharArray::operator()(int i, int j, int k, int l) const {
#  ifdef RWBOUNDS_CHECK
    boundsCheck(i,j,k,l);
#  endif
    return data()[i*step((int)0)+j*step(1)+k*step(2)+l*step(3)];
  }

#endif /*__RWSCARR_H__*/

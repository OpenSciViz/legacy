#ifndef __RWLINPACK_H__
#define __RWLINPACK_H__

/*
 * The declaration of the linpack subroutines.
 * There are two parts... first there are a set of #defines which
 * determine the actual names of the routines.  By default, a
 * routine named xxxx is called as xxxx_.  You may wish to change
 * this, for example to use a customized set of blas with a
 * different naming convention.
 *
 * After this section of defines comes the second section... the
 * declaration of the default linpack routines.
 *
 * Copyright (1991) by Rogue Wave Associates.  This software
 * is subject to copyright protection under the laws of the
 * United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

#include "rw/mathdefs.h"		/* Looking for "FLinkage" and "FDecl" */
#include "rw/dcomplex.h"		/* Get declarations for DComplex */
#include "rw/fcomplex.h"		/* Get declarations for FComplex */

#if defined(__cplusplus)
extern FLinkage {
# define P_(s) s
#else
#  ifdef RW_KR_ONLY
#    define P_(s) ()
#  else
#    define P_(s) s
#  endif
#endif

#define sgeco sgeco_
#define sgefa sgefa_
#define sgesl sgesl_
#define sgedi sgedi_
#define dgeco dgeco_
#define dgefa dgefa_
#define dgesl dgesl_
#define dgedi dgedi_
#define cgeco cgeco_
#define cgefa cgefa_
#define cgesl cgesl_
#define cgedi cgedi_
#define zgeco zgeco_
#define zgefa zgefa_
#define zgesl zgesl_
#define zgedi zgedi_
#define sgbco sgbco_
#define sgbfa sgbfa_
#define sgbsl sgbsl_
#define sgbdi sgbdi_
#define dgbco dgbco_
#define dgbfa dgbfa_
#define dgbsl dgbsl_
#define dgbdi dgbdi_
#define cgbco cgbco_
#define cgbfa cgbfa_
#define cgbsl cgbsl_
#define cgbdi cgbdi_
#define zgbco zgbco_
#define zgbfa zgbfa_
#define zgbsl zgbsl_
#define zgbdi zgbdi_
#define spoco spoco_
#define spofa spofa_
#define sposl sposl_
#define spodi spodi_
#define dpoco dpoco_
#define dpofa dpofa_
#define dposl dposl_
#define dpodi dpodi_
#define cpoco cpoco_
#define cpofa cpofa_
#define cposl cposl_
#define cpodi cpodi_
#define zpoco zpoco_
#define zpofa zpofa_
#define zposl zposl_
#define zpodi zpodi_
#define sppco sppco_
#define sppfa sppfa_
#define sppsl sppsl_
#define sppdi sppdi_
#define dppco dppco_
#define dppfa dppfa_
#define dppsl dppsl_
#define dppdi dppdi_
#define cppco cppco_
#define cppfa cppfa_
#define cppsl cppsl_
#define cppdi cppdi_
#define zppco zppco_
#define zppfa zppfa_
#define zppsl zppsl_
#define zppdi zppdi_
#define spbco spbco_
#define spbfa spbfa_
#define spbsl spbsl_
#define spbdi spbdi_
#define dpbco dpbco_
#define dpbfa dpbfa_
#define dpbsl dpbsl_
#define dpbdi dpbdi_
#define cpbco cpbco_
#define cpbfa cpbfa_
#define cpbsl cpbsl_
#define cpbdi cpbdi_
#define zpbco zpbco_
#define zpbfa zpbfa_
#define zpbsl zpbsl_
#define zpbdi zpbdi_
#define ssico ssico_
#define ssifa ssifa_
#define ssisl ssisl_
#define ssidi ssidi_
#define dsico dsico_
#define dsifa dsifa_
#define dsisl dsisl_
#define dsidi dsidi_
#define csico csico_
#define csifa csifa_
#define csisl csisl_
#define csidi csidi_
#define zsico zsico_
#define zsifa zsifa_
#define zsisl zsisl_
#define zsidi zsidi_
#define sspco sspco_
#define sspfa sspfa_
#define sspsl sspsl_
#define sspdi sspdi_
#define dspco dspco_
#define dspfa dspfa_
#define dspsl dspsl_
#define dspdi dspdi_
#define cspco cspco_
#define cspfa cspfa_
#define cspsl cspsl_
#define cspdi cspdi_
#define zspco zspco_
#define zspfa zspfa_
#define zspsl zspsl_
#define zspdi zspdi_
#define chico chico_
#define chifa chifa_
#define chisl chisl_
#define chidi chidi_
#define zhico zhico_
#define zhifa zhifa_
#define zhisl zhisl_
#define zhidi zhidi_
#define chpco chpco_
#define chpfa chpfa_
#define chpsl chpsl_
#define chpdi chpdi_
#define zhpco zhpco_
#define zhpfa zhpfa_
#define zhpsl zhpsl_
#define zhpdi zhpdi_
#define strco strco_
#define strsl strsl_
#define strdi strdi_
#define dtrco dtrco_
#define dtrsl dtrsl_
#define dtrdi dtrdi_
#define ctrco ctrco_
#define ctrsl ctrsl_
#define ctrdi ctrdi_
#define ztrco ztrco_
#define ztrsl ztrsl_
#define ztrdi ztrdi_
#define sgtsl sgtsl_
#define dgtsl dgtsl_
#define cgtsl cgtsl_
#define zgtsl zgtsl_
#define sptsl sptsl_
#define dptsl dptsl_
#define cptsl cptsl_
#define zptsl zptsl_
#define schdc schdc_
#define schud schud_
#define schdd schdd_
#define schex schex_
#define dchdc dchdc_
#define dchud dchud_
#define dchdd dchdd_
#define dchex dchex_
#define cchdc cchdc_
#define cchud cchud_
#define cchdd cchdd_
#define cchex cchex_
#define zchdc zchdc_
#define zchud zchud_
#define zchdd zchdd_
#define zchex zchex_
#define sqrdc sqrdc_
#define sqrsl sqrsl_
#define dqrdc dqrdc_
#define dqrsl dqrsl_
#define cqrdc cqrdc_
#define cqrsl cqrsl_
#define zqrdc zqrdc_
#define zqrsl zqrsl_
#define ssvdc ssvdc_
#define dsvdc dsvdc_
#define csvdc csvdc_
#define zsvdc zsvdc_

int FDecl sgeco P_((float *a, int *lda, int *n, int *ipvt, float *rcond, float *z));
int FDecl sgefa P_((float *a, int *lda, int *n, int *ipvt, int *info));
int FDecl sgesl P_((float *a, int *lda, int *n, int *ipvt, float *b, int *job));
int FDecl sgedi P_((float *a, int *lda, int *n, int *ipvt, float *det, float *work, int *job));
int FDecl dgeco P_((double *a, int *lda, int *n, int *ipvt, double *rcond, double *z));
int FDecl dgefa P_((double *a, int *lda, int *n, int *ipvt, int *info));
int FDecl dgesl P_((double *a, int *lda, int *n, int *ipvt, double *b, int *job));
int FDecl dgedi P_((double *a, int *lda, int *n, int *ipvt, double *det, double *work, int *job));

int FDecl cgeco P_((FComplex *a, int *lda, int *n, int *ipvt, float *rcond, FComplex *z));
int FDecl cgefa P_((FComplex *a, int *lda, int *n, int *ipvt, int *info));
int FDecl cgesl P_((FComplex *a, int *lda, int *n, int *ipvt, FComplex *b, int *job));
int FDecl cgedi P_((FComplex *a, int *lda, int *n, int *ipvt, FComplex *det, FComplex *work, int *job));
int FDecl zgeco P_((DComplex *a, int *lda, int *n, int *ipvt, double *rcond, DComplex *z));
int FDecl zgefa P_((DComplex *a, int *lda, int *n, int *ipvt, int *info));
int FDecl zgesl P_((DComplex *a, int *lda, int *n, int *ipvt, DComplex *b, int *job));
int FDecl zgedi P_((DComplex *a, int *lda, int *n, int *ipvt, DComplex *det, DComplex *work, int *job));
int FDecl sgbco P_((float *abd, int *lda, int *n, int *ml, int *mu, int *ipvt, float *rcond, float *z));
int FDecl sgbfa P_((float *abd, int *lda, int *n, int *ml, int *mu, int *ipvt, int *info));
int FDecl sgbsl P_((float *abd, int *lda, int *n, int *ml, int *mu, int *ipvt, float *b, int *job));
int FDecl sgbdi P_((float *abd, int *lda, int *n, int *ml, int *mu, int *ipvt, float *det));
int FDecl dgbco P_((double *abd, int *lda, int *n, int *ml, int *mu, int *ipvt, double *rcond, double *z));
int FDecl dgbfa P_((double *abd, int *lda, int *n, int *ml, int *mu, int *ipvt, int *info));
int FDecl dgbsl P_((double *abd, int *lda, int *n, int *ml, int *mu, int *ipvt, double *b, int *job));
int FDecl dgbdi P_((double *abd, int *lda, int *n, int *ml, int *mu, int *ipvt, double *det));
int FDecl cgbco P_((FComplex *abd, int *lda, int *n, int *ml, int *mu, int *ipvt, float *rcond, FComplex *z));
int FDecl cgbfa P_((FComplex *abd, int *lda, int *n, int *ml, int *mu, int *ipvt, int *info));
int FDecl cgbsl P_((FComplex *abd, int *lda, int *n, int *ml, int *mu, int *ipvt, FComplex *b, int *job));
int FDecl cgbdi P_((FComplex *abd, int *lda, int *n, int *ml, int *mu, int *ipvt, FComplex *det));
int FDecl zgbco P_((DComplex *abd, int *lda, int *n, int *ml, int *mu, int *ipvt, double *rcond, DComplex *z));
int FDecl zgbfa P_((DComplex *abd, int *lda, int *n, int *ml, int *mu, int *ipvt, int *info));
int FDecl zgbsl P_((DComplex *abd, int *lda, int *n, int *ml, int *mu, int *ipvt, DComplex *b, int *job));
int FDecl zgbdi P_((DComplex *abd, int *lda, int *n, int *ml, int *mu, int *ipvt, DComplex *det));
int FDecl spoco P_((float *a, int *lda, int *n, float *rcond, float *z, int *info));
int FDecl spofa P_((float *a, int *lda, int *n, int *info));
int FDecl sposl P_((float *a, int *lda, int *n, float *b));
int FDecl spodi P_((float *a, int *lda, int *n, float *det, int *job));
int FDecl dpoco P_((double *a, int *lda, int *n, double *rcond, double *z, int *info));
int FDecl dpofa P_((double *a, int *lda, int *n, int *info));
int FDecl dposl P_((double *a, int *lda, int *n, double *b));
int FDecl dpodi P_((double *a, int *lda, int *n, double *det, int *job));
int FDecl cpoco P_((FComplex *a, int *lda, int *n, float *rcond, FComplex *z, int *info));
int FDecl cpofa P_((FComplex *a, int *lda, int *n, int *info));
int FDecl cposl P_((FComplex *a, int *lda, int *n, FComplex *b));
int FDecl cpodi P_((FComplex *a, int *lda, int *n, float *det, int *job));
int FDecl zpoco P_((DComplex *a, int *lda, int *n, double *rcond, DComplex *z, int *info));
int FDecl zpofa P_((DComplex *a, int *lda, int *n, int *info));
int FDecl zposl P_((DComplex *a, int *lda, int *n, DComplex *b));
int FDecl zpodi P_((DComplex *a, int *lda, int *n, double *det, int *job));
int FDecl sppco P_((float *ap, int *n, float *rcond, float *z, int *info));
int FDecl sppfa P_((float *ap, int *n, int *info));
int FDecl sppsl P_((float *ap, int *n, float *b));
int FDecl sppdi P_((float *ap, int *n, float *det, int *job));
int FDecl dppco P_((double *ap, int *n, double *rcond, double *z, int *info));
int FDecl dppfa P_((double *ap, int *n, int *info));
int FDecl dppsl P_((double *ap, int *n, double *b));
int FDecl dppdi P_((double *ap, int *n, double *det, int *job));
int FDecl cppco P_((FComplex *ap, int *n, float *rcond, FComplex *z, int *info));
int FDecl cppfa P_((FComplex *ap, int *n, int *info));
int FDecl cppsl P_((FComplex *ap, int *n, FComplex *b));
int FDecl cppdi P_((FComplex *ap, int *n, float *det, int *job));
int FDecl zppco P_((DComplex *ap, int *n, double *rcond, DComplex *z, int *info));
int FDecl zppfa P_((DComplex *ap, int *n, int *info));
int FDecl zppsl P_((DComplex *ap, int *n, DComplex *b));
int FDecl zppdi P_((DComplex *ap, int *n, double *det, int *job));
int FDecl spbco P_((float *abd, int *lda, int *n, int *m, float *rcond, float *z, int *info));
int FDecl spbfa P_((float *abd, int *lda, int *n, int *m, int *info));
int FDecl spbsl P_((float *abd, int *lda, int *n, int *m, float *b));
int FDecl spbdi P_((float *abd, int *lda, int *n, int *m, float *det));
int FDecl dpbco P_((double *abd, int *lda, int *n, int *m, double *rcond, double *z, int *info));
int FDecl dpbfa P_((double *abd, int *lda, int *n, int *m, int *info));
int FDecl dpbsl P_((double *abd, int *lda, int *n, int *m, double *b));
int FDecl dpbdi P_((double *abd, int *lda, int *n, int *m, double *det));
int FDecl cpbco P_((FComplex *abd, int *lda, int *n, int *m, float *rcond, FComplex *z, int *info));
int FDecl cpbfa P_((FComplex *abd, int *lda, int *n, int *m, int *info));
int FDecl cpbsl P_((FComplex *abd, int *lda, int *n, int *m, FComplex *b));
int FDecl cpbdi P_((FComplex *abd, int *lda, int *n, int *m, float *det));
int FDecl zpbco P_((DComplex *abd, int *lda, int *n, int *m, double *rcond, DComplex *z, int *info));
int FDecl zpbfa P_((DComplex *abd, int *lda, int *n, int *m, int *info));
int FDecl zpbsl P_((DComplex *abd, int *lda, int *n, int *m, DComplex *b));
int FDecl zpbdi P_((DComplex *abd, int *lda, int *n, int *m, double *det));
int FDecl ssico P_((float *a, int *lda, int *n, int *kpvt, float *rcond, float *z));
int FDecl ssifa P_((float *a, int *lda, int *n, int *kpvt, int *info));
int FDecl ssisl P_((float *a, int *lda, int *n, int *kpvt, float *b));
int FDecl ssidi P_((float *a, int *lda, int *n, int *kpvt, float *det, int *inert, float *work, int *job));
int FDecl dsico P_((double *a, int *lda, int *n, int *kpvt, double *rcond, double *z));
int FDecl dsifa P_((double *a, int *lda, int *n, int *kpvt, int *info));
int FDecl dsisl P_((double *a, int *lda, int *n, int *kpvt, double *b));
int FDecl dsidi P_((double *a, int *lda, int *n, int *kpvt, double *det, int *inert, double *work, int *job));
int FDecl csico P_((FComplex *a, int *lda, int *n, int *kpvt, float *rcond, FComplex *z));
int FDecl csifa P_((FComplex *a, int *lda, int *n, int *kpvt, int *info));
int FDecl csisl P_((FComplex *a, int *lda, int *n, int *kpvt, FComplex *b));
int FDecl csidi P_((FComplex *a, int *lda, int *n, int *kpvt, FComplex *det, FComplex *work, int *job));
int FDecl zsico P_((DComplex *a, int *lda, int *n, int *kpvt, double *rcond, DComplex *z));
int FDecl zsifa P_((DComplex *a, int *lda, int *n, int *kpvt, int *info));
int FDecl zsisl P_((DComplex *a, int *lda, int *n, int *kpvt, DComplex *b));
int FDecl zsidi P_((DComplex *a, int *lda, int *n, int *kpvt, DComplex *det, DComplex *work, int *job));
int FDecl sspco P_((float *ap, int *n, int *kpvt, float *rcond, float *z));
int FDecl sspfa P_((float *ap, int *n, int *kpvt, int *info));
int FDecl sspsl P_((float *ap, int *n, int *kpvt, float *b));
int FDecl sspdi P_((float *ap, int *n, int *kpvt, float *det, int *inert, float *work, int *job));
int FDecl dspco P_((double *ap, int *n, int *kpvt, double *rcond, double *z));
int FDecl dspfa P_((double *ap, int *n, int *kpvt, int *info));
int FDecl dspsl P_((double *ap, int *n, int *kpvt, double *b));
int FDecl dspdi P_((double *ap, int *n, int *kpvt, double *det, int *inert, double *work, int *job));
int FDecl cspco P_((FComplex *ap, int *n, int *kpvt, float *rcond, FComplex *z));
int FDecl cspfa P_((FComplex *ap, int *n, int *kpvt, int *info));
int FDecl cspsl P_((FComplex *ap, int *n, int *kpvt, FComplex *b));
int FDecl cspdi P_((FComplex *ap, int *n, int *kpvt, FComplex *det, FComplex *work, int *job));
int FDecl zspco P_((DComplex *ap, int *n, int *kpvt, double *rcond, DComplex *z));
int FDecl zspfa P_((DComplex *ap, int *n, int *kpvt, int *info));
int FDecl zspsl P_((DComplex *ap, int *n, int *kpvt, DComplex *b));
int FDecl zspdi P_((DComplex *ap, int *n, int *kpvt, DComplex *det, DComplex *work, int *job));
int FDecl chico P_((FComplex *a, int *lda, int *n, int *kpvt, float *rcond, FComplex *z));
int FDecl chifa P_((FComplex *a, int *lda, int *n, int *kpvt, int *info));
int FDecl chisl P_((FComplex *a, int *lda, int *n, int *kpvt, FComplex *b));
int FDecl chidi P_((FComplex *a, int *lda, int *n, int *kpvt, float *det, int *inert, FComplex *work, int *job));
int FDecl zhico P_((DComplex *a, int *lda, int *n, int *kpvt, double *rcond, DComplex *z));
int FDecl zhifa P_((DComplex *a, int *lda, int *n, int *kpvt, int *info));
int FDecl zhisl P_((DComplex *a, int *lda, int *n, int *kpvt, DComplex *b));
int FDecl zhidi P_((DComplex *a, int *lda, int *n, int *kpvt, double *det, int *inert, DComplex *work, int *job));
int FDecl chpco P_((FComplex *ap, int *n, int *kpvt, float *rcond, FComplex *z));
int FDecl chpfa P_((FComplex *ap, int *n, int *kpvt, int *info));
int FDecl chpsl P_((FComplex *ap, int *n, int *kpvt, FComplex *b));
int FDecl chpdi P_((FComplex *ap, int *n, int *kpvt, float *det, int *inert, FComplex *work, int *job));
int FDecl zhpco P_((DComplex *ap, int *n, int *kpvt, double *rcond, DComplex *z));
int FDecl zhpfa P_((DComplex *ap, int *n, int *kpvt, int *info));
int FDecl zhpsl P_((DComplex *ap, int *n, int *kpvt, DComplex *b));
int FDecl zhpdi P_((DComplex *ap, int *n, int *kpvt, double *det, int *inert, DComplex *work, int *job));
int FDecl strco P_((float *t, int *ldt, int *n, float *rcond, float *z, int *job));
int FDecl strsl P_((float *t, int *ldt, int *n, float *b, int *job, int *info));
int FDecl strdi P_((float *t, int *ldt, int *n, float *det, int *job, int *info));
int FDecl dtrco P_((double *t, int *ldt, int *n, double *rcond, double *z, int *job));
int FDecl dtrsl P_((double *t, int *ldt, int *n, double *b, int *job, int *info));
int FDecl dtrdi P_((double *t, int *ldt, int *n, double *det, int *job, int *info));
int FDecl ctrco P_((FComplex *t, int *ldt, int *n, float *rcond, FComplex *z, int *job));
int FDecl ctrsl P_((FComplex *t, int *ldt, int *n, FComplex *b, int *job, int *info));
int FDecl ctrdi P_((FComplex *t, int *ldt, int *n, FComplex *det, int *job, int *info));
int FDecl ztrco P_((DComplex *t, int *ldt, int *n, double *rcond, DComplex *z, int *job));
int FDecl ztrsl P_((DComplex *t, int *ldt, int *n, DComplex *b, int *job, int *info));
int FDecl ztrdi P_((DComplex *t, int *ldt, int *n, DComplex *det, int *job, int *info));
int FDecl sgtsl P_((int *n, float *c, float *d, float *e, float *b, int *info));
int FDecl dgtsl P_((int *n, double *c, double *d, double *e, double *b, int *info));
int FDecl cgtsl P_((int *n, FComplex *c, FComplex *d, FComplex *e, FComplex *b, int *info));
int FDecl zgtsl P_((int *n, DComplex *c, DComplex *d, DComplex *e, DComplex *b, int *info));
int FDecl sptsl P_((int *n, float *d, float *e, float *b));
int FDecl dptsl P_((int *n, double *d, double *e, double *b));
int FDecl cptsl P_((int *n, FComplex *d, FComplex *e, FComplex *b));
int FDecl zptsl P_((int *n, DComplex *d, DComplex *e, DComplex *b));
int FDecl schdc P_((float *a, int *lda, int *p, float *work, int *jpvt, int *job, int *info));
int FDecl schud P_((float *r, int *ldr, int *p, float *x, float *z, int *ldz, int *nz, float *y, float *rho, float *c, float *s));
int FDecl schdd P_((float *r, int *ldr, int *p, float *x, float *z, int *ldz, int *nz, float *y, float *rho, float *c, float *s, int *info));
int FDecl schex P_((float *r, int *ldr, int *p, int *k, int *l, float *z, int *ldz, int *nz, float *c, float *s, int *job));
int FDecl dchdc P_((double *a, int *lda, int *p, double *work, int *jpvt, int *job, int *info));
int FDecl dchud P_((double *r, int *ldr, int *p, double *x, double *z, int *ldz, int *nz, double *y, double *rho, double *c, double *s));
int FDecl dchdd P_((double *r, int *ldr, int *p, double *x, double *z, int *ldz, int *nz, double *y, double *rho, double *c, double *s, int *info));
int FDecl dchex P_((double *r, int *ldr, int *p, int *k, int *l, double *z, int *ldz, int *nz, double *c, double *s, int *job));
int FDecl cchdc P_((FComplex *a, int *lda, int *p, FComplex *work, int *jpvt, int *job, int *info));
int FDecl cchud P_((FComplex *r, int *ldr, int *p, FComplex *x, FComplex *z, int *ldz, int *nz, FComplex *y, float *rho, float *c, FComplex *s));
int FDecl cchdd P_((FComplex *r, int *ldr, int *p, FComplex *x, FComplex *z, int *ldz, int *nz, FComplex *y, float *rho, float *c, FComplex *s, int *info));
int FDecl cchex P_((FComplex *r, int *ldr, int *p, int *k, int *l, FComplex *z, int *ldz, int *nz, float *c, FComplex *s, int *job));
int FDecl zchdc P_((DComplex *a, int *lda, int *p, DComplex *work, int *jpvt, int *job, int *info));
int FDecl zchud P_((DComplex *r, int *ldr, int *p, DComplex *x, DComplex *z, int *ldz, int *nz, DComplex *y, double *rho, double *c, DComplex *s));
int FDecl zchdd P_((DComplex *r, int *ldr, int *p, DComplex *x, DComplex *z, int *ldz, int *nz, DComplex *y, double *rho, double *c, DComplex *s, int *info));
int FDecl zchex P_((DComplex *r, int *ldr, int *p, int *k, int *l, DComplex *z, int *ldz, int *nz, double *c, DComplex *s, int *job));
int FDecl sqrdc P_((float *x, int *ldx, int *n, int *p, float *qraux, int *jpvt, float *work, int *job));
int FDecl sqrsl P_((float *x, int *ldx, int *n, int *k, float *qraux, float *y, float *qy, float *qty, float *b, float *rsd, float *xb, int *job, int *info));
int FDecl dqrdc P_((double *x, int *ldx, int *n, int *p, double *qraux, int *jpvt, double *work, int *job));
int FDecl dqrsl P_((double *x, int *ldx, int *n, int *k, double *qraux, double *y, double *qy, double *qty, double *b, double *rsd, double *xb, int *job, int *info));
int FDecl cqrdc P_((FComplex *x, int *ldx, int *n, int *p, FComplex *qraux, int *jpvt, FComplex *work, int *job));
int FDecl cqrsl P_((FComplex *x, int *ldx, int *n, int *k, FComplex *qraux, FComplex *y, FComplex *qy, FComplex *qty, FComplex *b, FComplex *rsd, FComplex *xb, int *job, int *info));
int FDecl zqrdc P_((DComplex *x, int *ldx, int *n, int *p, DComplex *qraux, int *jpvt, DComplex *work, int *job));
int FDecl zqrsl P_((DComplex *x, int *ldx, int *n, int *k, DComplex *qraux, DComplex *y, DComplex *qy, DComplex *qty, DComplex *b, DComplex *rsd, DComplex *xb, int *job, int *info));
int FDecl ssvdc P_((float *x, int *ldx, int *n, int *p, float *s, float *e, float *u, int *ldu, float *v, int *ldv, float *work, int *job, int *info));
int FDecl dsvdc P_((double *x, int *ldx, int *n, int *p, double *s, double *e, double *u, int *ldu, double *v, int *ldv, double *work, int *job, int *info));
int FDecl csvdc P_((FComplex *x, int *ldx, int *n, int *p, FComplex *s, FComplex *e, FComplex *u, int *ldu, FComplex *v, int *ldv, FComplex *work, int *job, int *info));
int FDecl zsvdc P_((DComplex *x, int *ldx, int *n, int *p, DComplex *s, DComplex *e, DComplex *u, int *ldu, DComplex *v, int *ldv, DComplex *work, int *job, int *info));

#undef P_

#if defined(__cplusplus)
}
#endif
#endif

#ifndef __RWDTRDGMAT_H__
#define __RWDTRDGMAT_H__
/*
 * DoubleTriDiagMat - A tridiagonal matrix of Doubles
 *
 * Stores a tridiagonal matrix.
 *
 * The matrix is stored in the same format as a banded matrix with
 * upper and lower bandwidth one.
 * .fi
 *
 * Generated from template $Id: xmat.h,v 1.4 1993/10/11 21:48:17 alv Exp $
 *
 * Bounds checking is enabled by setting the preprocessor
 * symbol RWBOUNDS_CHECK.  This is strongly encouraged.
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

#include "rw/dvec.h"
#include "rw/dref.h"

extern Double rwDoubleZero;	// This constant is sometimes returned by reference

class FloatTriDiagMat;
class DoubleGenMat;
class DoubleBandMat;

class DoubleTriDiagMat {
private:

DoubleVec vec;
unsigned n;
  // The data which define the matrix

  void zeroUnusedEntries();  // Initialize entries in the data vector which are not used to zero

public:
DoubleTriDiagMat();
DoubleTriDiagMat( const DoubleTriDiagMat& );
DoubleTriDiagMat(unsigned n, unsigned nAgain);
DoubleTriDiagMat(const DoubleVec& data, unsigned n, unsigned nAgain);
DoubleTriDiagMat(const FloatTriDiagMat&);
~DoubleTriDiagMat();
  // Constructors, conversion functions, and a destructor.

static const char *className;
  // For use in error messages

inline unsigned rows() const	   { return n; }
inline unsigned cols() const	   { return n; }
inline unsigned bandwidth() const      { return 3; }
inline unsigned lowerBandwidth() const { return 1; }
inline unsigned upperBandwidth() const { return 1; }
inline unsigned halfBandwidth() const  { return 1; }
  // Member functions to access the shape of the matrix

inline Double  val(int i, int j) const;
       Double  bcval(int i, int j) const;
inline Double  set(int i, int j, Double x);
       Double  bcset(int i, int j, Double x);
inline RODoubleRef operator()(int i, int j);
inline RODoubleRef ref(int i, int j);
       RODoubleRef bcref(int i, int j);
inline Double  operator()(int i, int j) const; // { return val(i,j); }
  // Access and/or change elements of the matrix.
  // The versions that begin with the letters bc do bounds
  // checking.  The others do bounds checking only if the
  // preprocessor symbol RWBOUNDS_CHECK is defined.

DoubleTriDiagMat leadingSubmatrix(int order);
  // Access to submatrices.
  // The leading submatrix of order k is the kxk matrix in the upper
  // left.
inline DoubleVec diagonal(int =0) const;
       DoubleVec bcdiagonal(int =0) const;
  // Access to diagonals.  The same conventions as for
  // element access apply (with respect to bcxxx and xxx names).

DoubleTriDiagMat& operator=(const DoubleTriDiagMat& m);
DoubleTriDiagMat& reference(DoubleTriDiagMat& m);
  // reference() makes an alias, operator= makes a copy.


void zero()        { vec=Double(0); }
  // Sets all elements of the matrix equal to zero.


DoubleTriDiagMat copy() const;
DoubleTriDiagMat deepCopy() const		{ return copy(); }
void deepenShallowCopy()        { vec.deepenShallowCopy(); }
  // copy() returns a DoubleTriDiagMat with its own copy of the data.
  // deepenShallowCopy makes sure the data is not shared by another
  // vector or matrix.

DoubleVec dataVec() 		{ return vec; }
const DoubleVec& dataVec() const	{ return vec; }
Double* data() 			{ return vec.data(); }
const Double* data() const 	{ return vec.data(); }
  // Access to the raw data.

void resize(unsigned m, unsigned n);
  // Resize the matrix.  New elements are set to zero.


void scanFrom(istream&);      		// Human readable
void printOn(ostream&) const;
void restoreFrom(RWvistream&);     	// Internal ASCII or binary code
void saveOn(RWvostream&) const;
void restoreFrom(RWFile&);
void saveOn(RWFile&) const;
unsigned binaryStoreSize() const;
  // Input/output member functions
  // Whether ASCII or binary formatting is used with readFrom and
  // storeOn is determined by the type of rwstream passed.

/*
 * The operator()() member functions are declared with ref(),
 * since the declarations look the same.
 *
 * The operator=() assignment operators are declared with reference(),
 * since the functions are so closely related.
 */

RWBoolean operator==(const DoubleTriDiagMat& X);
RWBoolean operator!=(const DoubleTriDiagMat& X)  { return !(operator==(X)); }
  // Two matrices are considered equal if their shapes are identical and
  // all their components are identical.

DoubleTriDiagMat& operator+=(const DoubleTriDiagMat& m);
DoubleTriDiagMat& operator-=(const DoubleTriDiagMat& m);
DoubleTriDiagMat& operator*=(const DoubleTriDiagMat& m);
DoubleTriDiagMat& operator*=(Double);
DoubleTriDiagMat& operator/=(const DoubleTriDiagMat& m);
DoubleTriDiagMat& operator/=(Double);
  // assignment operators.  self must be same size as m.


};

DoubleTriDiagMat operator-(const DoubleTriDiagMat&);	// Unary minus
DoubleTriDiagMat operator+(const DoubleTriDiagMat&);	// Unary plus
DoubleTriDiagMat operator*(const DoubleTriDiagMat&, const DoubleTriDiagMat&);
DoubleTriDiagMat operator/(const DoubleTriDiagMat&, const DoubleTriDiagMat&);
DoubleTriDiagMat operator+(const DoubleTriDiagMat&, const DoubleTriDiagMat&);
DoubleTriDiagMat operator-(const DoubleTriDiagMat&, const DoubleTriDiagMat&);
DoubleTriDiagMat operator*(const DoubleTriDiagMat&, Double);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
inline DoubleTriDiagMat operator*(Double x, const DoubleTriDiagMat& A) { return A*x; }
#else
       DoubleTriDiagMat operator*(Double x, const DoubleTriDiagMat& A);
#endif
DoubleTriDiagMat operator/(const DoubleTriDiagMat& A, Double x);
  // Arithmetic operators; Notice that operator* is an
  // element by element multiply, NOT a matrix multiply.

inline ostream& operator<<(ostream& s, const DoubleTriDiagMat& m)
		{m.printOn(s); return s;}
inline istream& operator>>(istream& s, DoubleTriDiagMat& m)
		{m.scanFrom(s); return s;}
  // I/O for humans

DoubleTriDiagMat transpose(const DoubleTriDiagMat&);
  // The transpose of the matrix. 

DoubleVec product(const DoubleTriDiagMat& A, const DoubleVec& x);
DoubleVec product(const DoubleVec& x, const DoubleTriDiagMat& A);
  // inner products

DoubleTriDiagMat toTriDiagMat( const DoubleGenMat& A );
DoubleTriDiagMat toTriDiagMat( const DoubleBandMat& A );

DoubleTriDiagMat abs(const DoubleTriDiagMat& A);

inline Double minValue(const DoubleTriDiagMat& A) { return minValue(A.dataVec()); }
inline Double maxValue(const DoubleTriDiagMat& A) { return maxValue(A.dataVec()); }


/*
 * Inline functions
 */

inline Double DoubleTriDiagMat::val(int i, int j) const {
#ifdef RWBOUNDS_CHECK
return bcval(i,j);
#else
    return ( (i-j>1) || (i-j<(-1)) ) ? (Double)(0) : vec(i+1+j*2);
#endif
}

inline Double DoubleTriDiagMat::set(int i, int j, Double x) {
#ifdef RWBOUNDS_CHECK
return bcset(i,j,x);
#else
    return vec(i+1+j*2)=x;
#endif
}

inline RODoubleRef DoubleTriDiagMat::ref(int i, int j) {
    return bcref(i,j);
}

inline RODoubleRef DoubleTriDiagMat::operator()(int i, int j) { return ref(i,j); }
inline Double DoubleTriDiagMat::operator()(int i, int j) const { return val(i,j); }

inline DoubleVec DoubleTriDiagMat::diagonal(int i) const {
#if defined(RWBOUNDS_CHECK)
    return bcdiagonal(i);
#else
    int iabs = (i>=0) ? i : -i;
    return vec.slice(i>0 ? 1 + i*2 : 1 + iabs, n-iabs, 3 );
#endif
}

#endif

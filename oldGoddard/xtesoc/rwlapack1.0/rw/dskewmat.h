#ifndef __RWDSKEWMAT_H__
#define __RWDSKEWMAT_H__
/*
 * DoubleSkewMat - An skew symmetric matrix of Doubles
 *
 * Stores a skew symmetric matrix.  Note that only half the matrix is actually
 * stored.  This means if you change, say, entry (2,3) you are also
 * implicitly changing entry (3,2).
 *
 * The lower left triangle of the matrix is stored in column major
 * order.
 *
 * Generated from template $Id: xmat.h,v 1.4 1993/10/11 21:48:17 alv Exp $
 *
 * Bounds checking is enabled by setting the preprocessor
 * symbol RWBOUNDS_CHECK.  This is strongly encouraged.
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

#include "rw/dvec.h"
#include "rw/dref.h"


class FloatSkewMat;
class DoubleGenMat;
class DoubleSymMat;

class DoubleSkewMat {
private:

DoubleVec vec;
unsigned n;
  // The data which define the matrix


public:
DoubleSkewMat();
DoubleSkewMat( const DoubleSkewMat& );
DoubleSkewMat(unsigned n, unsigned nAgain);
DoubleSkewMat(const DoubleVec& data, unsigned n, unsigned nAgain);
DoubleSkewMat(const FloatSkewMat&);
~DoubleSkewMat();
  // Constructors, conversion functions, and a destructor.

static const char *className;
  // For use in error messages

inline unsigned rows() const	   { return n; }
inline unsigned cols() const	   { return n; }
  // Member functions to access the shape of the matrix

inline Double  val(int i, int j) const;
       Double  bcval(int i, int j) const;
inline Double  set(int i, int j, Double x);
       Double  bcset(int i, int j, Double x);
inline NGDoubleRef operator()(int i, int j);
inline NGDoubleRef ref(int i, int j);
       NGDoubleRef bcref(int i, int j);
inline Double  operator()(int i, int j) const; // { return val(i,j); }
  // Access and/or change elements of the matrix.
  // The versions that begin with the letters bc do bounds
  // checking.  The others do bounds checking only if the
  // preprocessor symbol RWBOUNDS_CHECK is defined.

DoubleSkewMat leadingSubmatrix(int order);
  // Access to submatrices.
  // The leading submatrix of order k is the kxk matrix in the upper
  // left.
  // Access to diagonals.  The same conventions as for
  // element access apply (with respect to bcxxx and xxx names).

DoubleSkewMat& operator=(const DoubleSkewMat& m);
DoubleSkewMat& reference(DoubleSkewMat& m);
  // reference() makes an alias, operator= makes a copy.


void zero()        { vec=Double(0); }
  // Sets all elements of the matrix equal to zero.


DoubleSkewMat copy() const;
DoubleSkewMat deepCopy() const		{ return copy(); }
void deepenShallowCopy()        { vec.deepenShallowCopy(); }
  // copy() returns a DoubleSkewMat with its own copy of the data.
  // deepenShallowCopy makes sure the data is not shared by another
  // vector or matrix.

DoubleVec dataVec() 		{ return vec; }
const DoubleVec& dataVec() const	{ return vec; }
Double* data() 			{ return vec.data(); }
const Double* data() const 	{ return vec.data(); }
  // Access to the raw data.

void resize(unsigned m, unsigned n);
  // Resize the matrix.  New elements are set to zero.


void scanFrom(istream&);      		// Human readable
void printOn(ostream&) const;
void restoreFrom(RWvistream&);     	// Internal ASCII or binary code
void saveOn(RWvostream&) const;
void restoreFrom(RWFile&);
void saveOn(RWFile&) const;
unsigned binaryStoreSize() const;
  // Input/output member functions
  // Whether ASCII or binary formatting is used with readFrom and
  // storeOn is determined by the type of rwstream passed.

/*
 * The operator()() member functions are declared with ref(),
 * since the declarations look the same.
 *
 * The operator=() assignment operators are declared with reference(),
 * since the functions are so closely related.
 */

RWBoolean operator==(const DoubleSkewMat& X);
RWBoolean operator!=(const DoubleSkewMat& X)  { return !(operator==(X)); }
  // Two matrices are considered equal if their shapes are identical and
  // all their components are identical.

DoubleSkewMat& operator+=(const DoubleSkewMat& m);
DoubleSkewMat& operator-=(const DoubleSkewMat& m);
DoubleSkewMat& operator*=(const DoubleSkewMat& m);
DoubleSkewMat& operator*=(Double);
DoubleSkewMat& operator/=(const DoubleSkewMat& m);
DoubleSkewMat& operator/=(Double);
  // assignment operators.  self must be same size as m.


};

DoubleSkewMat operator-(const DoubleSkewMat&);	// Unary minus
DoubleSkewMat operator+(const DoubleSkewMat&);	// Unary plus
DoubleSkewMat operator*(const DoubleSkewMat&, const DoubleSkewMat&);
DoubleSkewMat operator/(const DoubleSkewMat&, const DoubleSkewMat&);
DoubleSkewMat operator+(const DoubleSkewMat&, const DoubleSkewMat&);
DoubleSkewMat operator-(const DoubleSkewMat&, const DoubleSkewMat&);
DoubleSkewMat operator*(const DoubleSkewMat&, Double);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
inline DoubleSkewMat operator*(Double x, const DoubleSkewMat& A) { return A*x; }
#else
       DoubleSkewMat operator*(Double x, const DoubleSkewMat& A);
#endif
DoubleSkewMat operator/(const DoubleSkewMat& A, Double x);
DoubleSkewMat operator/(Double x, const DoubleSkewMat& A);
  // Arithmetic operators; Notice that operator* is an
  // element by element multiply, NOT a matrix multiply.

inline ostream& operator<<(ostream& s, const DoubleSkewMat& m)
		{m.printOn(s); return s;}
inline istream& operator>>(istream& s, DoubleSkewMat& m)
		{m.scanFrom(s); return s;}
  // I/O for humans

DoubleSkewMat transpose(const DoubleSkewMat&);
  // The transpose of the matrix. 

DoubleVec product(const DoubleSkewMat& A, const DoubleVec& x);
DoubleVec product(const DoubleVec& x, const DoubleSkewMat& A);
  // inner products

DoubleSkewMat toSkewMat( const DoubleGenMat& A, RWBoolean keepMainDiag = TRUE );

DoubleSymMat abs(const DoubleSkewMat& A);



/*
 * Inline functions
 */

inline Double DoubleSkewMat::val(int i, int j) const {
#ifdef RWBOUNDS_CHECK
return bcval(i,j);
#else
    int index = (i<=j) ?  (j*(j+1)/2+i) : (i*(i+1)/2+j);
    Double val = vec(index);
    return (i<=j) ?  val : -val;
#endif
}

inline Double DoubleSkewMat::set(int i, int j, Double x) {
    return bcset(i,j,x);
}

inline NGDoubleRef DoubleSkewMat::ref(int i, int j) {
    return bcref(i,j);
}

inline NGDoubleRef DoubleSkewMat::operator()(int i, int j) { return ref(i,j); }
inline Double DoubleSkewMat::operator()(int i, int j) const { return val(i,j); }


#endif

#ifndef __RWPSTREAM_H__
#define __RWPSTREAM_H__

/*
 * rwp[io]stream ---	Portable I/O streams (use escape sequences for 
 * 			writing special characters.
 *
 * $Id: pstream.h,v 2.11 1993/11/17 21:42:02 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1989 - 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: pstream.h,v $
 * Revision 2.11  1993/11/17  21:42:02  keffer
 * [io]stream is now member data instead of multiply inheriting, thus
 * avoiding ambiguous shift operations.
 *
 * Revision 2.10  1993/11/17  04:12:46  keffer
 * Added function putString(), to allow support of embedded nulls
 *
 * Revision 2.9  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 2.8  1993/05/18  00:41:46  keffer
 * Now uses new exception handling facility
 *
 * Revision 2.7  1993/04/15  02:45:18  keffer
 * Changed inheritance order to avoid cfront bug
 *
 * Revision 2.6  1993/04/14  21:03:52  keffer
 * Changed inheritance hierarchy to allow XDR streams, which cannot use
 * streambuf model.
 *
 * Revision 2.5  1993/04/09  19:35:45  keffer
 * Indexing is now done using size_t
 *
 * Revision 2.3  1992/11/26  21:04:33  myersn
 * add members to operate on wchar_t values and strings.
 *
 * Revision 2.3  1992/11/26  21:04:33  myersn
 * add members to operate on wchar_t values and strings.
 *
 * Revision 2.2  1992/11/19  05:45:01  keffer
 * Introduced new <rw/compiler.h> macro directives
 *
 * Revision 2.1  1992/11/15  22:12:09  keffer
 * Removed use of macro NL
 *
 * Revision 2.0  1992/10/23  03:27:32  keffer
 * RCS Baseline version
 *
 *    Rev 1.8   17 Mar 1992 12:26:48   KEFFER
 * Removed RWTV3_COMPATIBLE macro hooks.
 * 
 *    Rev 1.7   18 Feb 1992 09:54:32   KEFFER
 * 
 *    Rev 1.6   13 Nov 1991 11:09:16   keffer
 * Removed "near" qualifiers.  Both putwrap()'s are now out of line.
 * 
 *    Rev 1.5   05 Nov 1991 13:52:50   keffer
 * Can now live in the DLL
 * 
 *    Rev 1.4   17 Oct 1991 09:12:46   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.2   29 Jul 1991 11:33:32   keffer
 * Macro RW_NO_OVERLOAD_UCHAR checks for overloaded unsigned char
 * 
 */

#include "rw/vstream.h"

/************************************************
 *						*
 *		class RWpistream		*
 *						*
 ************************************************/

class RWExport RWpistream : public RWvistream
{

public:

  RWpistream(istream& str);
  RWpistream(streambuf* s);

  virtual int		eof()          {return istr_.eof();    }
  virtual int		fail()         {return istr_.fail();   }
  virtual int		bad()          {return istr_.bad();    }
  virtual int		good()         {return istr_.good();   }
  virtual int		rdstate()      {return istr_.rdstate();}
  virtual void		clear(int v=0) {       istr_.clear(v); }
  streambuf*		rdbuf()        {return istr_.rdbuf();  }

  virtual int		get();
  virtual RWvistream&	get(char&);
#ifndef RW_NO_OVERLOAD_WCHAR
  virtual RWvistream&	get(wchar_t&);
#endif
#ifndef RW_NO_OVERLOAD_UCHAR
  virtual RWvistream&	get(unsigned char&);
#endif
  virtual RWvistream&	get(char*,   size_t N);
  virtual RWvistream&	get(double*, size_t N);
  virtual RWvistream&	get(float*,  size_t N);
  virtual RWvistream&	get(int*,    size_t N);
  virtual RWvistream&	get(long*,   size_t N);
  virtual RWvistream&	get(short*,  size_t N);
#ifndef RW_NO_OVERLOAD_WCHAR
  virtual RWvistream&	get(wchar_t*,size_t N);
#endif
#ifndef RW_NO_OVERLOAD_UCHAR
  virtual RWvistream&	get(unsigned char*,  size_t N);
#endif
  virtual RWvistream&	get(unsigned short*, size_t N);
  virtual RWvistream&	get(unsigned int*,   size_t N);
  virtual RWvistream&	get(unsigned long*,  size_t N);
  virtual RWvistream&	getString(char* s,   size_t maxlen);
  virtual RWvistream&	operator>>(char&);
  virtual RWvistream&	operator>>(double&);
  virtual RWvistream&	operator>>(float&);
  virtual RWvistream&	operator>>(int&);
  virtual RWvistream&	operator>>(long&);
  virtual RWvistream&	operator>>(short&);
#ifndef RW_NO_OVERLOAD_WCHAR
  virtual RWvistream&	operator>>(wchar_t&);
#endif
#ifndef RW_NO_OVERLOAD_UCHAR
  virtual RWvistream&	operator>>(unsigned char&);
#endif
  virtual RWvistream&	operator>>(unsigned int&);
  virtual RWvistream&	operator>>(unsigned long&);
  virtual RWvistream&	operator>>(unsigned short&);
protected:
  void			syntaxErr(const char* expect, char was);
  char			getCChar();
  istream		istr_;
};

/************************************************
 *						*
 *		class RWpostream		*
 *						*
 ************************************************/

class RWExport RWpostream : public RWvostream
{

public:

  RWpostream(ostream& str);
  RWpostream(streambuf* s);

  virtual int		eof()          {return ostr_.eof();    }
  virtual int		fail()         {return ostr_.fail();   }
  virtual int		bad()          {return ostr_.bad();    }
  virtual int		good()         {return ostr_.good();   }
  virtual int		rdstate()      {return ostr_.rdstate();}
  virtual void		clear(int v=0) {       ostr_.clear(v); }
  streambuf*		rdbuf()        {return ostr_.rdbuf();  }

  virtual RWvostream&	putString(const char*, size_t);
  virtual RWvostream&	operator<<(const char*);
  virtual RWvostream&	operator<<(char);
#ifndef RW_NO_OVERLOAD_WCHAR
  virtual RWvostream&	operator<<(wchar_t);
#endif
#ifndef RW_NO_OVERLOAD_UCHAR
  virtual RWvostream&	operator<<(unsigned char);
#endif
  virtual RWvostream&	operator<<(double);
  virtual RWvostream&	operator<<(float);
  virtual RWvostream&	operator<<(int);
  virtual RWvostream&	operator<<(unsigned int);
  virtual RWvostream&	operator<<(long);
  virtual RWvostream&	operator<<(unsigned long);
  virtual RWvostream&	operator<<(short);
  virtual RWvostream&	operator<<(unsigned short);

  virtual RWvostream&	put(char);
  virtual RWvostream&	put(const char* p, size_t N);
#ifndef RW_NO_OVERLOAD_WCHAR
  virtual RWvostream&	put(wchar_t);
  virtual RWvostream&	put(const wchar_t* p, size_t N);
#endif
#ifndef RW_NO_OVERLOAD_UCHAR
  virtual RWvostream&	put(unsigned char);
  virtual RWvostream&	put(const unsigned char* p,  size_t N);
#endif
  virtual RWvostream&	put(const short* p,          size_t N);
  virtual RWvostream&	put(const unsigned short* p, size_t N);
  virtual RWvostream&	put(const int* p,            size_t N);
  virtual RWvostream&	put(const unsigned int* p,   size_t N);
  virtual RWvostream&	put(const long* p,           size_t N);
  virtual RWvostream&	put(const unsigned long* p,  size_t N);
  virtual RWvostream&	put(const float* p,          size_t N);
  virtual RWvostream&	put(const double* p,         size_t N);
protected:
  static const unsigned	MAXCOL;
  void			putwrap(char);
#ifndef RW_NO_OVERLOAD_WCHAR
  void			putwrap(wchar_t c);
#endif
#ifndef RW_NO_OVERLOAD_UCHAR
  void			putwrap(unsigned char c);
#endif
  void			putwrap(const char* s, size_t len);
  void			putCChars(const char* s, size_t len);
  ostream		ostr_;
  unsigned		column_;
};

#endif /* __RWPSTREAM_H__ */

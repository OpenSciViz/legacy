#ifndef __RWRWSLICE_H__
#define __RWRWSLICE_H__

/*
 * RWSlice:   Base subscripting class - subscript using a string or a slice
 *  RWRange:  Subscript a range of indices
 *  RWToEnd:  Subscript everything from a given index to the end
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited licence.
 *
 ***************************************************************************
 *
 * It is required that the stride in an RWRange and an RWToEnd be positive.
 * Reasons:  The semantics of negative strides in an RWRange are unclear.
 * Constructing an RWToEnd with a negative stride, RWToEnd(b,-2) eg,
 * has the same semantics as the clearer RWRange(b,0,2).  Prohibiting 
 * a negative stride in this case also makes the inline function
 * RWSlice::len() considerably simpler.
 *
 * $Log: rwslice.h,v $
 * Revision 1.7  1993/10/12  14:46:52  alv
 * added non-inline copy ctor - this fixes bcc for OS/2 bug
 *
 * Revision 1.6  1993/09/19  22:50:26  alv
 * ported to Metaware
 *
 * Revision 1.5  1993/09/19  21:47:46  alv
 * ported to Microsoft
 *
 * Revision 1.4  1993/09/14  17:16:35  alv
 * Fixed so RWAll is inited statically
 *
 * Revision 1.3  1993/04/06  18:02:34  alv
 * added public default constructor, RWAll is now an RWSlice, not an RWToEnd
 *
 */

/*
 * The slice data is kept in a seperate struct so that we can
 * initialize RWAll at compile time using an initializer.
 */

struct RWSliceData {
  int      theBegin;
  int      theLen;    // A negative value means as long as possible
  int      theStride; // Stride of zero means collapse this dimension
};

#if defined(_MSC_VER) || defined(__HIGHC__)
class RWSlice : public RWSliceData {   // Microsoft screws up friend access to private bases
#else
class RWSlice : private RWSliceData {
#endif
friend class RWRange;
friend class RWToEnd;

  RWSlice(double){}   // For use by the friend classes.  The argument is
                      // a dummy to distinguish this constructor.
public:
  RWSlice();                    // Defaults to all entries
  RWSlice(const RWSlice&);      // Copy ctor needs to be non-inline to avoid bcc for OS/2 bug
  RWSlice(int i);               // Extracts one index entry
  RWSlice(int b, int l);        // Length l, starting at b
  RWSlice(int b, int l, int s); // Length l, start at b, stride of s
  RWSlice(const char *);        // Parse a string of form "b:e:s"
  static void boundsCheck(int l, int s);  // Check l>=0, s!=0
    // The int constructor is meant for automatic conversion type from
    // an integer.  This situation is marked by setting the stride to
    // zero, it is checked for using the collapse() function.

  void  boundsCheck(int vecLen) const; // Check that all indicated elements in vector
  int   collapse()      const { return theStride==0; }
  int   begin()         const { return theBegin; }
  int   len(int vecLen) const { return (theLen>=0) ? theLen : (vecLen-theBegin-1)/theStride+1; }
  int   stride()        const { return theStride; }
    // boundsCheck() differs from the earlier overloaded boundsCheck() in
    // that this one is used by the vector at subscripting time, and the
    // other is used when constructing an RWSlice.
    // collapse() indicates that this RWSlice was constructed via type
    // conversion from an integer.  This implies the user is subscripting
    // with an integer, so this dimension should be collapsed.
    // len() requires the vector length to be passed in because some 
    // calculation will occur if this is actually a RWToEnd object (flagged
    // by len<0).
};

class RWRange : public RWSlice {
public:
  RWRange(int b, int e);         // Extract everything from b to e inclusive
  RWRange(int b, int e, int s);  // Extracts every sth index from b to e
  static void boundsCheck(int s);// Ensure s>0
};

class RWToEnd : public RWSlice {
public:
  RWToEnd(int b);                // Everything from b to the end
  RWToEnd(int b, int s);         // Every sth entry from b to the  end
  static void boundsCheck(int s);// Ensure s>0
};

extern const RWSlice& RWAll;     // Definition in rwslice.cpp


/*
 * Inline definitions for the constructors.
 */

inline RWSlice::RWSlice()
{
  theBegin = 0;
  theLen = -1;
  theStride = 1;
}

inline RWSlice::RWSlice(int i)
{
  theBegin=i; theLen=0; theStride=0;
#ifdef BOUNDS_CHECK
  boundsCheck(theLen,1);  // This is the one instance where stride==0 is ok
#endif
}

inline RWSlice::RWSlice(int b, int l)       
{
  theBegin=b; theLen=l; theStride=1;
#ifdef BOUNDS_CHECK
  boundsCheck(theLen,theStride);
#endif
}

inline RWSlice::RWSlice(int b, int l, int s)
{
  theBegin=b; theLen=l; theStride=s;
#ifdef BOUNDS_CHECK
  boundsCheck(theLen,theStride);
#endif
}

inline RWRange::RWRange(int b, int e) : RWSlice((double)0)
{
  theBegin = b;
  theLen = ((b>e)?b-e:e-b)+1;
  theStride = (b>e)?(-1):1;
}

inline RWRange::RWRange(int b, int e, int s) : RWSlice((double)0)
{
#ifdef BOUNDS_CHECK
  boundsCheck(s);
#endif
  theBegin = b;
  theLen = ((b>e)?b-e:e-b)/s+1;
  theStride = (b>e)?(-s):s;
}

inline RWToEnd::RWToEnd(int b) : RWSlice((double)0)
{
  theBegin = b;
  theLen = -1;
  theStride = 1;
}

inline RWToEnd::RWToEnd(int b, int s) : RWSlice((double)0)
{
#ifdef BOUNDS_CHECK
  boundsCheck(s);
#endif
  theBegin = b;
  theLen = -1;
  theStride = s;
}

#endif /*__RWRWSLICE_H__*/

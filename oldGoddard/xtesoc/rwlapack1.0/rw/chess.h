#ifndef __RWCHESS_H__
#define __RWCHESS_H__
/*
 * DComplexHessenbergDecomp - Hessenberg decomposition of a matrix
 *
 * Generated from template $Id: xhess.h,v 1.1 1993/05/18 17:00:02 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 * This decomposition is usually formed as a percusor to computing
 * the eigenvalues and/or Schur decomposition of a non-symmetric matrix.
 *
 * This class includes an optional balancing transformation in addition
 * to the standard Hessenberg decomposition.
 */ 

#include "rw/lapkdefs.h"
#include "rw/cvec.h"
#include "rw/cgenmat.h"
#include "rw/cbal.h"
#include "rw/lapack.h"

class DComplexHessenbergDecomp {
private:
  DComplexBalanceTransform B_;  // Balancing transformation
  DComplexGenMat           H_;  // Hessenberg matrix and Q info
  DComplexVec              tau_;// scalar reflectors for generating Q

public:
  DComplexHessenbergDecomp();
  DComplexHessenbergDecomp(const DComplexGenMat&, RWBoolean permute=TRUE, RWBoolean scale=TRUE);
  DComplexHessenbergDecomp(const DComplexBalanceDecomp&);
  void factor(const DComplexGenMat&, RWBoolean permute=TRUE, RWBoolean scale=TRUE);
  void factor(const DComplexBalanceDecomp&);
  unsigned            cols()                  const {return H_.cols();}
  unsigned            rows()                  const {return H_.rows();}
  DComplexGenMat           B()                     const;
  DComplexBalanceTransform balanceTransform()      const {return B_;}  // An alternate representation of B
  DComplexGenMat           Q()                     const;
  DComplexGenMat           H()                     const;
  DComplexVec              Bx(const DComplexVec&)       const;
  DComplexVec              BInvTx(const DComplexVec&)   const;
  DComplexVec              Qx(const DComplexVec&)       const;
  DComplexVec              QTx(const DComplexVec&)      const;
  DComplexGenMat           BX(const DComplexGenMat&)    const;
  DComplexGenMat           BInvTX(const DComplexGenMat&)const;
  DComplexGenMat           QX(const DComplexGenMat&)    const;
  DComplexGenMat           QTX(const DComplexGenMat&)   const;
};

#endif

#ifndef __FLUDECOMP_H__
#define __FLUDECOMP_H__

/*
 * Stub for V3.X compatibility.
 *
 * $Header: /users/rcs/mathrw/fludecmp.h,v 1.2 1993/01/26 21:04:14 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * Copyright (C) 1988, 1989, 1990, 1991.  This software is subject to copyright
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: fludecmp.h,v $
 * Revision 1.2  1993/01/26  21:04:14  alv
 * removed DOS EOF
 *
 * Revision 1.1  1993/01/23  00:08:35  alv
 * Initial revision
 *
 * 
 *    Rev 1.2   17 Oct 1991 09:12:46   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.0   04 Sep 1991 09:44:30   keffer
 * Initial revision.
 *
 */

#include "rw/fgenfct.h"

typedef FloatGenFact FLUDecomp;

#endif /* __FLUDECOMP_H__ */

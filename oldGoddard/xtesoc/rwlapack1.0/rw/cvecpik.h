#ifndef __RWCVECPIK_H__
#define __RWCVECPIK_H__

/*
 * Declarations for DComplexVecPick --- picks elements out of a DComplexVec
 *
 * Generated from template $Id: vecpik.htm,v 1.2 1993/03/22 15:51:31 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * Copyright (C) 1988, 1989, 1990, 1991.  This software is subject to copyright
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/ivec.h"

/*
 * The DComplexVecPick class allows selected elements to be addressed.
 * There are no public constructors.
 */

class DComplexVecPick {
private:
  DComplexVec		V;
  const IntVec		pick;
  DComplexVecPick(DComplexVec& v, const IntVec& x);	// Constructor is private
  friend		DComplexVec;
protected:
  void			assertElements() const;
  void			lengthCheck(unsigned) const;
public:
  void			operator=(const DComplexVec&);
  void			operator=(const DComplexVecPick&);
  void			operator=(DComplex);

  inline DComplex&	operator()(int i);
  inline DComplex		operator()(int i) const;
  unsigned		length() const	{ return pick.length(); }
};

/************************************************
 *						*
 *		I N L I N E S			*
 *						*
 ************************************************/

inline
DComplexVecPick::DComplexVecPick(DComplexVec& v, const IntVec& x) :
  V(v), pick(x)
{
#ifdef RWBOUNDS_CHECK
  assertElements();
#endif
}

inline DComplex& DComplexVecPick::operator()(int i)       { return V( pick(i) ); }
inline DComplex  DComplexVecPick::operator()(int i) const { return V( pick(i) ); }

#endif /*__CVECPIK_H__*/

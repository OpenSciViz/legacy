#ifndef __RWFBANDMAT_H__
#define __RWFBANDMAT_H__
/*
 * FloatBandMat - A banded matrix of Floats
 *
 * Stores a banded matrix.  All entries farther above the diagonal
 * that the upper bandwidth, or farther below than the lower bandwidth
 * are defined to be zero.  The total bandwidth is the sum of the 
 * upper and lower bandwidths, plus one for the diagonal.  Only
 * the elements which may be non-zero are stored.
 *
 * The matrix is stored column by column.  Each column takes up 
 * <bandwidth> entries, some of these entries near the corners
 * are not used.
 *
 * Generated from template $Id: xmat.h,v 1.4 1993/10/11 21:48:17 alv Exp $
 *
 * Bounds checking is enabled by setting the preprocessor
 * symbol RWBOUNDS_CHECK.  This is strongly encouraged.
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

#include "rw/fvec.h"
#include "rw/fref.h"

extern Float rwFloatZero;	// This constant is sometimes returned by reference

class DoubleBandMat;
class FloatGenMat;
class FloatTriDiagMat;
class FloatSymBandMat;

class FloatBandMat {
private:

FloatVec vec;
unsigned n;
unsigned band, bandu;	// bandwidth and upper bandwidth
  // The data which define the matrix

  void zeroUnusedEntries();  // Initialize entries in the data vector which are not used to zero

public:
FloatBandMat();
FloatBandMat( const FloatBandMat& );
FloatBandMat(unsigned n, unsigned nAgain, unsigned lowerWidth, unsigned upperWidth);
FloatBandMat(const FloatVec& data, unsigned n, unsigned nAgain, unsigned lowerWidth, unsigned upperWdth);
FloatBandMat( const FloatTriDiagMat& A );
FloatBandMat( const FloatSymBandMat& A );
~FloatBandMat();
  // Constructors, conversion functions, and a destructor.

static const char *className;
  // For use in error messages

inline unsigned rows() const	   { return n; }
inline unsigned cols() const	   { return n; }
inline unsigned bandwidth() const      { return band; }
inline unsigned lowerBandwidth() const { return band-bandu-1; }
inline unsigned upperBandwidth() const { return bandu; }
  // Member functions to access the shape of the matrix

inline Float  val(int i, int j) const;
       Float  bcval(int i, int j) const;
inline Float  set(int i, int j, Float x);
       Float  bcset(int i, int j, Float x);
inline ROFloatRef operator()(int i, int j);
inline ROFloatRef ref(int i, int j);
       ROFloatRef bcref(int i, int j);
inline Float  operator()(int i, int j) const; // { return val(i,j); }
  // Access and/or change elements of the matrix.
  // The versions that begin with the letters bc do bounds
  // checking.  The others do bounds checking only if the
  // preprocessor symbol RWBOUNDS_CHECK is defined.

FloatBandMat leadingSubmatrix(int order);
  // Access to submatrices.
  // The leading submatrix of order k is the kxk matrix in the upper
  // left.
inline FloatVec diagonal(int =0) const;
       FloatVec bcdiagonal(int =0) const;
  // Access to diagonals.  The same conventions as for
  // element access apply (with respect to bcxxx and xxx names).

FloatBandMat& operator=(const FloatBandMat& m);
FloatBandMat& reference(FloatBandMat& m);
  // reference() makes an alias, operator= makes a copy.


void zero()        { vec=Float(0); }
  // Sets all elements of the matrix equal to zero.


FloatBandMat copy() const;
FloatBandMat deepCopy() const		{ return copy(); }
void deepenShallowCopy()        { vec.deepenShallowCopy(); }
  // copy() returns a FloatBandMat with its own copy of the data.
  // deepenShallowCopy makes sure the data is not shared by another
  // vector or matrix.

FloatVec dataVec() 		{ return vec; }
const FloatVec& dataVec() const	{ return vec; }
Float* data() 			{ return vec.data(); }
const Float* data() const 	{ return vec.data(); }
  // Access to the raw data.

void resize(unsigned m, unsigned n);
void resize(unsigned n, unsigned nAgain, unsigned lowWidth, unsigned upWidth);
  // Resize the matrix.  New elements are set to zero.


void scanFrom(istream&);      		// Human readable
void printOn(ostream&) const;
void restoreFrom(RWvistream&);     	// Internal ASCII or binary code
void saveOn(RWvostream&) const;
void restoreFrom(RWFile&);
void saveOn(RWFile&) const;
unsigned binaryStoreSize() const;
  // Input/output member functions
  // Whether ASCII or binary formatting is used with readFrom and
  // storeOn is determined by the type of rwstream passed.

/*
 * The operator()() member functions are declared with ref(),
 * since the declarations look the same.
 *
 * The operator=() assignment operators are declared with reference(),
 * since the functions are so closely related.
 */

RWBoolean operator==(const FloatBandMat& X);
RWBoolean operator!=(const FloatBandMat& X)  { return !(operator==(X)); }
  // Two matrices are considered equal if their shapes are identical and
  // all their components are identical.

FloatBandMat& operator+=(const FloatBandMat& m);
FloatBandMat& operator-=(const FloatBandMat& m);
FloatBandMat& operator*=(const FloatBandMat& m);
FloatBandMat& operator*=(Float);
FloatBandMat& operator/=(const FloatBandMat& m);
FloatBandMat& operator/=(Float);
  // assignment operators.  self must be same size as m.


};

FloatBandMat operator-(const FloatBandMat&);	// Unary minus
FloatBandMat operator+(const FloatBandMat&);	// Unary plus
FloatBandMat operator*(const FloatBandMat&, const FloatBandMat&);
FloatBandMat operator/(const FloatBandMat&, const FloatBandMat&);
FloatBandMat operator+(const FloatBandMat&, const FloatBandMat&);
FloatBandMat operator-(const FloatBandMat&, const FloatBandMat&);
FloatBandMat operator*(const FloatBandMat&, Float);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
inline FloatBandMat operator*(Float x, const FloatBandMat& A) { return A*x; }
#else
       FloatBandMat operator*(Float x, const FloatBandMat& A);
#endif
FloatBandMat operator/(const FloatBandMat& A, Float x);
  // Arithmetic operators; Notice that operator* is an
  // element by element multiply, NOT a matrix multiply.

inline ostream& operator<<(ostream& s, const FloatBandMat& m)
		{m.printOn(s); return s;}
inline istream& operator>>(istream& s, FloatBandMat& m)
		{m.scanFrom(s); return s;}
  // I/O for humans

FloatBandMat transpose(const FloatBandMat&);
  // The transpose of the matrix. 

FloatVec product(const FloatBandMat& A, const FloatVec& x);
FloatVec product(const FloatVec& x, const FloatBandMat& A);
FloatBandMat product(const FloatBandMat& A, const FloatBandMat& B);
FloatBandMat transposeProduct(const FloatBandMat& A, const FloatBandMat& B);
  // transposeProduct calculates A^TB
  // inner products

FloatBandMat toFloat(const DoubleBandMat& A);
FloatBandMat toBandMat( const FloatGenMat& A, unsigned bandl, unsigned bandu );

FloatBandMat abs(const FloatBandMat& A);

inline Float minValue(const FloatBandMat& A) { return minValue(A.dataVec()); }
inline Float maxValue(const FloatBandMat& A) { return maxValue(A.dataVec()); }


/*
 * Inline functions
 */

inline Float FloatBandMat::val(int i, int j) const {
    return bcval(i,j);
}

inline Float FloatBandMat::set(int i, int j, Float x) {
#ifdef RWBOUNDS_CHECK
return bcset(i,j,x);
#else
    return vec(i-j+bandu + j*band)=x;
#endif
}

inline ROFloatRef FloatBandMat::ref(int i, int j) {
    return bcref(i,j);
}

inline ROFloatRef FloatBandMat::operator()(int i, int j) { return ref(i,j); }
inline Float FloatBandMat::operator()(int i, int j) const { return val(i,j); }

inline FloatVec FloatBandMat::diagonal(int i) const {
#if defined(RWBOUNDS_CHECK)
    return bcdiagonal(i);
#else
    int iabs = (i>=0) ? i : -i;
    return vec.slice(i>0 ? upperBandwidth()+i*(bandwidth()-1)
			 : upperBandwidth()+iabs
			 , n-iabs, bandwidth() );
#endif
}

#endif

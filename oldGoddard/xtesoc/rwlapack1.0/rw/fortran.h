#ifndef __RWFORTRAN_H__
#define __RWFORTRAN_H__

/*
 * Functions needed by lapack.h++ code translated by FOR_C++
 *
 * $Id: fortran.h,v 1.5 1993/07/21 21:00:40 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * This is a small subset of the full FOR_C++ run time library.
 * I extracted only the small portion needed to compile lapack.h++.
 *
 * $Log: fortran.h,v $
 * Revision 1.5  1993/07/21  21:00:40  alv
 * ported to HP
 *
 * Revision 1.4  1993/07/09  23:07:24  alv
 * ported to xlC
 *
 * Revision 1.3  1993/07/09  16:54:36  alv
 * #ifdefed out defn of abs for xlC which already has it in math.h
 *
 * Revision 1.2  1993/03/09  15:38:17  alv
 * removed abs(unsigned) function
 *
 * Revision 1.1  1993/03/03  01:05:15  alv
 * Initial revision
 *
 */

#include "rw/mathdefs.h"

/*
 * max and min are defined as macros rather than as inline functions
 * for two reasons: 1) to avoid the plethora of inline functions needed
 * to hit every combination of precisions (this is needed to avoid
 * ambiguities) and 2) to finesse the case where some compilers might
 * already have defined inline functions for max and min.
 * Same story for sign
 */
  
#ifndef max
# define max(a,b) ( (a)>(b) ? (a) : (b) )
#endif

#ifndef min
# define min(a,b) ( (a)<(b) ? (a) : (b) )
#endif

#ifndef sign
# define sign(a,b) ( (b)<0 ? ((a)<0?(a):(-(a))) : ((a)<0?(-(a)):(a)) )
#endif

/*
 * Can't use a simple #define for abs() because that approach doesn't
 * work for complex numbers.
 */

inline long abs(long a)         { return a<0 ? -a : a; }
inline short abs(short a)       { return a<0 ? -a : a; }
inline float abs(float a)       { return a<0 ? -a : a; }
inline double abs(double a)     { return a<0 ? -a : a; }

/*
 * Trust automatic conversion of complex numbers by disabling the
 * ctocf (convert from complex*16 to complex) intrinsic.  
 */

#define ctocf(x) (((DComplex)x))

/*
 * memerr() is called if an attempt to allocate a large array  off the
 * heap fails
 */
void memerr(char*);

/*
 * pow functions
 */
double pow(double,long);
long   pow(long,long);
inline double pow(float x, long r)  {return pow(double(x),long(r));}
#if !defined(hpux)    /* These already provide pow */
inline double pow(double x, int r)  {return pow(double(x),long(r));}
#endif
inline double pow(float x, int r)   {return pow(double(x),long(r));}

/*
 * Variable number of arguments min and max
 */
const long IEND = 2147483646;
const float FEND = 1.e+38-1.;
long   vmax(long,long,long,long);
long   vmax(long,long,long,long,long);
long   vmax(long,long,long,long,long,long);
double vmax(double,double,double,double);
double vmax(double,double,double,double,double);
double vmax(double,double,double,double,double,double);
long   vmin(long,long,long,long);
long   vmin(long,long,long,long,long);
long   vmin(long,long,long,long,long,long);
double vmin(double,double,double,double);
double vmin(double,double,double,double,double);
double vmin(double,double,double,double,double,double);

/*
 * support for DO loops
 * formula:  DOCNT(ini,tst,inc) = max( (long)((tst-ini+inc)/inc), 0 ) 
 */
inline long docnt(double e1,double e2,double e3)
		{ return e3==0. ? 0 : max( (long)((e2-e1+e3)/e3), 0 ); }

/*
 * The string support needed (primarily for calls to ilaenv)
 *
 * STR1 converts character literal to single character string.
 */
struct CHRTMP {
  unsigned siz; // current size assoc. w/string 's' 
  char *s;      // ptr to string space 
};
inline char *STR1(char *t, char c) { t[0]=c; t[1]='\0'; return t; }
void ini_chrtmp(CHRTMP*,int);   // initialize the chrtmp array 
void rel_chrtmp(CHRTMP*,int);   // release the space assoc. w/chrtmp array 
char *f_concat(CHRTMP*,char*,char*,long);

/* 
 * nearest number
 */
inline int nint( double f )   { return (int)( f < 0 ? f-0.5 : f+0.5 ); }

#endif 

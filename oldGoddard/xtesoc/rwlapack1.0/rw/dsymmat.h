#ifndef __RWDSYMMAT_H__
#define __RWDSYMMAT_H__
/*
 * DoubleSymMat - A symmetric matrix of Doubles
 *
 * Stores a symmetric matrix.  Only half the matrix is actually
 * stored.  This means if you change, say, entry (2,3) you are also
 * implicitly changing entry (3,2).
 *
 * The lower left triangle of the matrix is stored in column major
 * order.
 *
 * Generated from template $Id: xmat.h,v 1.4 1993/10/11 21:48:17 alv Exp $
 *
 * Bounds checking is enabled by setting the preprocessor
 * symbol RWBOUNDS_CHECK.  This is strongly encouraged.
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

#include "rw/dvec.h"


class FloatSymMat;
class DoubleGenMat;

class DoubleSymMat {
private:

DoubleVec vec;
unsigned n;
  // The data which define the matrix


public:
DoubleSymMat();
DoubleSymMat( const DoubleSymMat& );
DoubleSymMat(unsigned n, unsigned nAgain);
DoubleSymMat(unsigned n, unsigned nAgain, Double initval);
DoubleSymMat(const DoubleVec& data, unsigned n, unsigned nAgain);
DoubleSymMat(const FloatSymMat&);
~DoubleSymMat();
  // Constructors, conversion functions, and a destructor.

static const char *className;
  // For use in error messages

inline unsigned rows() const	   { return n; }
inline unsigned cols() const	   { return n; }
  // Member functions to access the shape of the matrix

inline Double  val(int i, int j) const;
       Double  bcval(int i, int j) const;
inline Double  set(int i, int j, Double x);
       Double  bcset(int i, int j, Double x);
inline Double& operator()(int i, int j);
inline Double& ref(int i, int j);
       Double& bcref(int i, int j);
inline Double  operator()(int i, int j) const; // { return val(i,j); }
  // Access and/or change elements of the matrix.
  // The versions that begin with the letters bc do bounds
  // checking.  The others do bounds checking only if the
  // preprocessor symbol RWBOUNDS_CHECK is defined.

DoubleSymMat leadingSubmatrix(int order);
  // Access to submatrices.
  // The leading submatrix of order k is the kxk matrix in the upper
  // left.
  // Access to diagonals.  The same conventions as for
  // element access apply (with respect to bcxxx and xxx names).

DoubleSymMat& operator=(const DoubleSymMat& m);
DoubleSymMat& reference(DoubleSymMat& m);
  // reference() makes an alias, operator= makes a copy.

DoubleSymMat& operator=(Double x)   { vec=Double(x); return *this; }
  // Sets all elements of the matrix equal to x

void zero()        { vec=Double(0); }
  // Sets all elements of the matrix equal to zero.


DoubleSymMat copy() const;
DoubleSymMat deepCopy() const		{ return copy(); }
void deepenShallowCopy()        { vec.deepenShallowCopy(); }
  // copy() returns a DoubleSymMat with its own copy of the data.
  // deepenShallowCopy makes sure the data is not shared by another
  // vector or matrix.

DoubleVec dataVec() 		{ return vec; }
const DoubleVec& dataVec() const	{ return vec; }
Double* data() 			{ return vec.data(); }
const Double* data() const 	{ return vec.data(); }
  // Access to the raw data.

void resize(unsigned m, unsigned n);
  // Resize the matrix.  New elements are set to zero.

DoubleSymMat apply(mathFunTy) const;
  // Each element in the matrix returned by apply is
  // the result of calling f() on the corresponding element
  // in this matrix.  

void scanFrom(istream&);      		// Human readable
void printOn(ostream&) const;
void restoreFrom(RWvistream&);     	// Internal ASCII or binary code
void saveOn(RWvostream&) const;
void restoreFrom(RWFile&);
void saveOn(RWFile&) const;
unsigned binaryStoreSize() const;
  // Input/output member functions
  // Whether ASCII or binary formatting is used with readFrom and
  // storeOn is determined by the type of rwstream passed.

/*
 * The operator()() member functions are declared with ref(),
 * since the declarations look the same.
 *
 * The operator=() assignment operators are declared with reference(),
 * since the functions are so closely related.
 */

RWBoolean operator==(const DoubleSymMat& X);
RWBoolean operator!=(const DoubleSymMat& X)  { return !(operator==(X)); }
  // Two matrices are considered equal if their shapes are identical and
  // all their components are identical.

DoubleSymMat& operator+=(Double);
DoubleSymMat& operator-=(Double);
DoubleSymMat& operator+=(const DoubleSymMat& m);
DoubleSymMat& operator-=(const DoubleSymMat& m);
DoubleSymMat& operator*=(const DoubleSymMat& m);
DoubleSymMat& operator*=(Double);
DoubleSymMat& operator/=(const DoubleSymMat& m);
DoubleSymMat& operator/=(Double);
  // assignment operators.  self must be same size as m.

DoubleSymMat& operator++();	// Prefix operator
#ifndef RW_NO_POSTFIX
void operator++(int);	// Postfix operator
#endif
DoubleSymMat& operator--();	// Prefix operator
#ifndef RW_NO_POSTFIX
void operator--(int);	// Postfix operator
#endif
  // Increment/Decrement operators


};

DoubleSymMat operator-(const DoubleSymMat&);	// Unary minus
DoubleSymMat operator+(const DoubleSymMat&);	// Unary plus
DoubleSymMat operator*(const DoubleSymMat&, const DoubleSymMat&);
DoubleSymMat operator/(const DoubleSymMat&, const DoubleSymMat&);
DoubleSymMat operator+(const DoubleSymMat&, const DoubleSymMat&);
DoubleSymMat operator-(const DoubleSymMat&, const DoubleSymMat&);
DoubleSymMat operator*(const DoubleSymMat&, Double);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
inline DoubleSymMat operator*(Double x, const DoubleSymMat& A) { return A*x; }
#else
       DoubleSymMat operator*(Double x, const DoubleSymMat& A);
#endif
DoubleSymMat operator/(const DoubleSymMat& A, Double x);
DoubleSymMat operator/(Double x, const DoubleSymMat& A);
DoubleSymMat operator+(const DoubleSymMat&, Double);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
inline DoubleSymMat operator+(Double x, const DoubleSymMat& A) { return A+x; }
inline DoubleSymMat operator-(const DoubleSymMat& A, Double x) { return A+(-x); }
#else
       DoubleSymMat operator+(Double x, const DoubleSymMat& A);
       DoubleSymMat operator-(const DoubleSymMat& A, Double x);
#endif
DoubleSymMat operator-(Double x, const DoubleSymMat& A);
  // Arithmetic operators; Notice that operator* is an
  // element by element multiply, NOT a matrix multiply.

inline ostream& operator<<(ostream& s, const DoubleSymMat& m)
		{m.printOn(s); return s;}
inline istream& operator>>(istream& s, DoubleSymMat& m)
		{m.scanFrom(s); return s;}
  // I/O for humans

DoubleSymMat transpose(const DoubleSymMat&);
  // The transpose of the matrix. 

DoubleVec product(const DoubleSymMat& A, const DoubleVec& x);
DoubleVec product(const DoubleVec& x, const DoubleSymMat& A);
  // inner products

DoubleSymMat toSymMat( const DoubleGenMat& A );
DoubleSymMat upperToSymMat( const DoubleGenMat& A );
DoubleSymMat lowerToSymMat( const DoubleGenMat& A );

DoubleSymMat abs(const DoubleSymMat& A);

inline Double minValue(const DoubleSymMat& A) { return minValue(A.dataVec()); }
inline Double maxValue(const DoubleSymMat& A) { return maxValue(A.dataVec()); }

inline DoubleSymMat cos(const DoubleSymMat& A)  { return A.apply(::cos); }
inline DoubleSymMat cosh(const DoubleSymMat& A) { return A.apply(::cosh); }
inline DoubleSymMat exp(const DoubleSymMat& A)  { return A.apply(::exp); } 
inline DoubleSymMat log(const DoubleSymMat& A)  { return A.apply(::log); }
inline DoubleSymMat sin(const DoubleSymMat& A)  { return A.apply(::sin); }
inline DoubleSymMat sinh(const DoubleSymMat& A) { return A.apply(::sinh); }
inline DoubleSymMat sqrt(const DoubleSymMat& A) { return A.apply(::sqrt); }
  // Math functions applicable to both real and complex types.

inline DoubleSymMat acos(const DoubleSymMat& A) { return A.apply(::acos); }
inline DoubleSymMat asin(const DoubleSymMat& A) { return A.apply(::asin); }
inline DoubleSymMat atan(const DoubleSymMat& A) { return A.apply(::atan); }
DoubleSymMat atan2(const DoubleSymMat&,const DoubleSymMat&);
inline DoubleSymMat ceil(const DoubleSymMat& A) { return A.apply(::ceil); }
inline DoubleSymMat floor(const DoubleSymMat& A){ return A.apply(::floor); }
inline DoubleSymMat log10(const DoubleSymMat& A){ return A.apply(::log10); }
inline DoubleSymMat tan(const DoubleSymMat& A)  { return A.apply(::tan); }
inline DoubleSymMat tanh(const DoubleSymMat& A) { return A.apply(::tanh); }

/*
 * Inline functions
 */

inline Double DoubleSymMat::val(int i, int j) const {
#ifdef RWBOUNDS_CHECK
return bcval(i,j);
#else
    if (i>j) {int temp=i; i=j; j=temp;}
    return vec(j*(j+1)/2+i);
#endif
}

inline Double DoubleSymMat::set(int i, int j, Double x) {
    return bcset(i,j,x);
}

inline Double& DoubleSymMat::ref(int i, int j) {
#ifdef RWBOUNDS_CHECK
return bcref(i,j);
#else
    if (i>j) {int temp=i; i=j; j=temp;}
    return vec(j*(j+1)/2+i);
#endif
}

inline Double& DoubleSymMat::operator()(int i, int j) { return ref(i,j); }
inline Double DoubleSymMat::operator()(int i, int j) const { return val(i,j); }


#endif

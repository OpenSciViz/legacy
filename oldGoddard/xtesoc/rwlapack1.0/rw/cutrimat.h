#ifndef __RWCUTRIMAT_H__
#define __RWCUTRIMAT_H__
/*
 * DComplexTriMat - An upper triangular matrix of DComplexs
 *
 * Stores an upper triangular matrix.
 *
 * The matrix is stored in column major order.
 *
 * Generated from template $Id: xmat.h,v 1.4 1993/10/11 21:48:17 alv Exp $
 *
 * Bounds checking is enabled by setting the preprocessor
 * symbol RWBOUNDS_CHECK.  This is strongly encouraged.
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

#include "rw/cvec.h"
#include "rw/cref.h"

extern DComplex rwDComplexZero;	// This constant is sometimes returned by reference

class DoubleUpperTriMat;
class DComplexGenMat;
class DComplexLowerTriMat;

class DComplexUpperTriMat {
private:

DComplexVec vec;
unsigned n;
  // The data which define the matrix


public:
DComplexUpperTriMat();
DComplexUpperTriMat( const DComplexUpperTriMat& );
DComplexUpperTriMat(unsigned n, unsigned nAgain);
DComplexUpperTriMat(const DComplexVec& data, unsigned n, unsigned nAgain);
DComplexUpperTriMat(const DoubleUpperTriMat& re);
DComplexUpperTriMat(const DoubleUpperTriMat& re, const DoubleUpperTriMat& im);
~DComplexUpperTriMat();
  // Constructors, conversion functions, and a destructor.

static const char *className;
  // For use in error messages

inline unsigned rows() const	   { return n; }
inline unsigned cols() const	   { return n; }
  // Member functions to access the shape of the matrix

inline DComplex  val(int i, int j) const;
       DComplex  bcval(int i, int j) const;
inline DComplex  set(int i, int j, DComplex x);
       DComplex  bcset(int i, int j, DComplex x);
inline RODComplexRef operator()(int i, int j);
inline RODComplexRef ref(int i, int j);
       RODComplexRef bcref(int i, int j);
inline DComplex  operator()(int i, int j) const; // { return val(i,j); }
  // Access and/or change elements of the matrix.
  // The versions that begin with the letters bc do bounds
  // checking.  The others do bounds checking only if the
  // preprocessor symbol RWBOUNDS_CHECK is defined.

DComplexUpperTriMat leadingSubmatrix(int order);
  // Access to submatrices.
  // The leading submatrix of order k is the kxk matrix in the upper
  // left.
  // Access to diagonals.  The same conventions as for
  // element access apply (with respect to bcxxx and xxx names).

DComplexUpperTriMat& operator=(const DComplexUpperTriMat& m);
DComplexUpperTriMat& reference(DComplexUpperTriMat& m);
  // reference() makes an alias, operator= makes a copy.


void zero()        { vec=DComplex(0,0); }
  // Sets all elements of the matrix equal to zero.


DComplexUpperTriMat copy() const;
DComplexUpperTriMat deepCopy() const		{ return copy(); }
void deepenShallowCopy()        { vec.deepenShallowCopy(); }
  // copy() returns a DComplexUpperTriMat with its own copy of the data.
  // deepenShallowCopy makes sure the data is not shared by another
  // vector or matrix.

DComplexVec dataVec() 		{ return vec; }
const DComplexVec& dataVec() const	{ return vec; }
DComplex* data() 			{ return vec.data(); }
const DComplex* data() const 	{ return vec.data(); }
  // Access to the raw data.

void resize(unsigned m, unsigned n);
  // Resize the matrix.  New elements are set to zero.


void scanFrom(istream&);      		// Human readable
void printOn(ostream&) const;
void restoreFrom(RWvistream&);     	// Internal ASCII or binary code
void saveOn(RWvostream&) const;
void restoreFrom(RWFile&);
void saveOn(RWFile&) const;
unsigned binaryStoreSize() const;
  // Input/output member functions
  // Whether ASCII or binary formatting is used with readFrom and
  // storeOn is determined by the type of rwstream passed.

/*
 * The operator()() member functions are declared with ref(),
 * since the declarations look the same.
 *
 * The operator=() assignment operators are declared with reference(),
 * since the functions are so closely related.
 */

RWBoolean operator==(const DComplexUpperTriMat& X);
RWBoolean operator!=(const DComplexUpperTriMat& X)  { return !(operator==(X)); }
  // Two matrices are considered equal if their shapes are identical and
  // all their components are identical.

DComplexUpperTriMat& operator+=(const DComplexUpperTriMat& m);
DComplexUpperTriMat& operator-=(const DComplexUpperTriMat& m);
DComplexUpperTriMat& operator*=(const DComplexUpperTriMat& m);
DComplexUpperTriMat& operator*=(DComplex);
DComplexUpperTriMat& operator/=(const DComplexUpperTriMat& m);
DComplexUpperTriMat& operator/=(DComplex);
  // assignment operators.  self must be same size as m.


};

DComplexUpperTriMat operator-(const DComplexUpperTriMat&);	// Unary minus
DComplexUpperTriMat operator+(const DComplexUpperTriMat&);	// Unary plus
DComplexUpperTriMat operator*(const DComplexUpperTriMat&, const DComplexUpperTriMat&);
DComplexUpperTriMat operator/(const DComplexUpperTriMat&, const DComplexUpperTriMat&);
DComplexUpperTriMat operator+(const DComplexUpperTriMat&, const DComplexUpperTriMat&);
DComplexUpperTriMat operator-(const DComplexUpperTriMat&, const DComplexUpperTriMat&);
DComplexUpperTriMat operator*(const DComplexUpperTriMat&, DComplex);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
inline DComplexUpperTriMat operator*(DComplex x, const DComplexUpperTriMat& A) { return A*x; }
#else
       DComplexUpperTriMat operator*(DComplex x, const DComplexUpperTriMat& A);
#endif
DComplexUpperTriMat operator/(const DComplexUpperTriMat& A, DComplex x);
  // Arithmetic operators; Notice that operator* is an
  // element by element multiply, NOT a matrix multiply.

inline ostream& operator<<(ostream& s, const DComplexUpperTriMat& m)
		{m.printOn(s); return s;}
inline istream& operator>>(istream& s, DComplexUpperTriMat& m)
		{m.scanFrom(s); return s;}
  // I/O for humans

DComplexLowerTriMat transpose(const DComplexUpperTriMat&);
  // The transpose of the matrix. 

DComplexVec product(const DComplexUpperTriMat& A, const DComplexVec& x);
DComplexVec product(const DComplexVec& x, const DComplexUpperTriMat& A);
  // inner products

DComplexUpperTriMat toUpperTriMat( const DComplexGenMat& A );

DoubleUpperTriMat abs(const DComplexUpperTriMat& A);
DComplexUpperTriMat conj(const DComplexUpperTriMat& A);
DoubleUpperTriMat real(const DComplexUpperTriMat& A);
DoubleUpperTriMat imag(const DComplexUpperTriMat& A);
DoubleUpperTriMat norm(const DComplexUpperTriMat& A);
DoubleUpperTriMat arg(const DComplexUpperTriMat& A);



/*
 * Inline functions
 */

inline DComplex DComplexUpperTriMat::val(int i, int j) const {
#ifdef RWBOUNDS_CHECK
return bcval(i,j);
#else
    return (j<i) ? DComplex(0,0) : vec(j*(j+1)/2+i);
#endif
}

inline DComplex DComplexUpperTriMat::set(int i, int j, DComplex x) {
#ifdef RWBOUNDS_CHECK
return bcset(i,j,x);
#else
    return vec(j*(j+1)/2+i)=x; 
#endif
}

inline RODComplexRef DComplexUpperTriMat::ref(int i, int j) {
    return bcref(i,j);
}

inline RODComplexRef DComplexUpperTriMat::operator()(int i, int j) { return ref(i,j); }
inline DComplex DComplexUpperTriMat::operator()(int i, int j) const { return val(i,j); }


#endif

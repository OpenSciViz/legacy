#ifndef __RWCBLA_H__
#define __RWCBLA_H__

/*
 * FComplex precision (float complex) Basic Linear Algebra prototypes.
 *
 * $Header: /users/rcs/mathrw/rwcbla.h,v 1.1 1993/01/23 00:08:43 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * Copyright (C) 1991.  This software is subject to copyright
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: rwcbla.h,v $
 * Revision 1.1  1993/01/23  00:08:43  alv
 * Initial revision
 *
 * 
 *    Rev 1.2   17 Oct 1991 09:12:58   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.0   27 Sep 1991 11:49:48   keffer
 * Initial revision.
 */

#ifndef __MATHDEFS_H__
#  include "rw/mathdefs.h"		/* Looking for "FDecl" and "RW_KR_ONLY" */
#endif
#include "rw/fcomplex.h"		/* Get declarations for FComplex */

#ifdef __cplusplus
extern "C" {
# define P_(s) s
#else
#  ifdef RW_KR_ONLY
#    define P_(s) ()
#  else
#    define P_(s) s
#  endif
#endif

void   FDecl rwccopy P_((unsigned n, FComplex* restrict x, int incx, const FComplex* restrict y, int incy));
void   FDecl rwcset  P_((unsigned n, FComplex* restrict x, int incx, const FComplex* restrict scalar));
int    FDecl rwcsame P_((unsigned n, const FComplex* restrict x, int incx, const FComplex* restrict y, int incy));

void   FDecl rwcdot P_((unsigned n, const FComplex* restrict x, int incx, const FComplex* restrict y, int incy, FComplex* result));
void   FDecl rwccdot P_((unsigned n, const FComplex* restrict x, int incx, const FComplex* restrict y, int incy, FComplex* result));

/* For implementing "Vector& Vector::operator+=(const Vector&)" functionality: */
void   FDecl rwc_aplvv P_((unsigned n, FComplex* restrict x, int incx, const FComplex* restrict y, int incy));
void   FDecl rwc_amivv P_((unsigned n, FComplex* restrict x, int incx, const FComplex* restrict y, int incy));
void   FDecl rwc_amuvv P_((unsigned n, FComplex* restrict x, int incx, const FComplex* restrict y, int incy));
void   FDecl rwc_advvv P_((unsigned n, FComplex* restrict x, int incx, const FComplex* restrict y, int incy));

/* For implementing "Vector& Vector::operator+=(type)" functionality: */
void   FDecl rwc_aplvs P_((unsigned n, FComplex* restrict x, int incx, const FComplex* restrict scalar));
void   FDecl rwc_amuvs P_((unsigned n, FComplex* restrict x, int incx, const FComplex* restrict scalar));
void   FDecl rwc_advvs P_((unsigned n, FComplex* restrict x, int incx, const FComplex* restrict scalar));

/* For implementing "Vector operator+(const Vector&, const Vector&)" functionality: */
void   FDecl rwc_plvv P_((unsigned n, FComplex* restrict z, const FComplex* restrict x, int incx, const FComplex* restrict y, int incy));
void   FDecl rwc_mivv P_((unsigned n, FComplex* restrict z, const FComplex* restrict x, int incx, const FComplex* restrict y, int incy));
void   FDecl rwc_muvv P_((unsigned n, FComplex* restrict z, const FComplex* restrict x, int incx, const FComplex* restrict y, int incy));
void   FDecl rwc_dvvv P_((unsigned n, FComplex* restrict z, const FComplex* restrict x, int incx, const FComplex* restrict y, int incy));

/* For implementing "Vector operator+(const Vector&, type)" functionality: */
void   FDecl rwc_plvs P_((unsigned n, FComplex* restrict z, const FComplex* restrict x, int incx, const FComplex* restrict scalar));
void   FDecl rwc_muvs P_((unsigned n, FComplex* restrict z, const FComplex* restrict x, int incx, const FComplex* restrict scalar));
void   FDecl rwc_dvvs P_((unsigned n, FComplex* restrict z, const FComplex* restrict x, int incx, const FComplex* restrict scalar));

/* For implementing "Vector operator+(type, const Vector&)" functionality: */
void   FDecl rwc_misv P_((unsigned n, FComplex* restrict z, const FComplex* scalar, const FComplex* restrict x, int incx));
void   FDecl rwc_dvsv P_((unsigned n, FComplex* restrict z, const FComplex* scalar, const FComplex* restrict x, int incx));

#undef P_

#ifdef __cplusplus
}
#endif

#endif


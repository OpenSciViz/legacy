#ifndef __RWCREF_H__
#define __RWCREF_H__
/*
 * Generated from template $Id: xref.h,v 1.1 1993/06/23 18:28:11 alv Exp $
 *
 * Reference classes.
 *
 * Changed having a reference to "data" to using a pointer to "data"
 * because the bcc 3.0 compiler generates incorrect code sometimes for
 * classes containing a reference and no explicit copy constructor.
 * This is really too bad; the reference semantics make more sense here.
 * 
 * The operator<<() output routines were added because the 
 * Borland C++ compiler can't figure out enough to call the
 * conversion routine and then call the proper op<< routine.
 * For some bizarre reason it thinks there is a conflict with
 * converting the ref to a char*.  Hey, I don't pretend to understand.
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

#include "rw/rstream.h"

class RODComplexRef {
  private:
    DComplex*        data;
    RWBoolean   readonly;
    DComplex         error(DComplex);   // Called when op=() invoked for a readonly
  public:
    RODComplexRef(DComplex& x, RWBoolean ro=FALSE) : data(&x) { readonly=ro; }
    RODComplexRef& operator=(DComplex x) { *data=(readonly?error(x):x); return *this; }
    operator DComplex()             { return *data; }
    friend ostream& operator<<(ostream& s,RODComplexRef& r) { return s<<*r.data; }
};

class NGDComplexRef {
  private:
    DComplex*        data;
    RWBoolean   negate;
  public:
    NGDComplexRef(DComplex& x, RWBoolean ng=FALSE) : data(&x) { negate=ng; }
    NGDComplexRef& operator=(DComplex x)         { *data=(negate?-x:x); return *this; }
    operator DComplex()                     { return negate?-*data:*data; }
    friend ostream& operator<<(ostream& s,NGDComplexRef& r) { return s<<(r.negate?-*r.data:*r.data); }
};

class CJDComplexRef {
  private:
    DComplex*        data;
    RWBoolean   conjugate;
  public:
    CJDComplexRef(DComplex& x, RWBoolean cj=FALSE) : data(&x) { conjugate=cj; }
    CJDComplexRef& operator=(DComplex x)    { *data=(conjugate?conj(x):x); return *this; }
    operator DComplex()                { return conjugate?conj(*data):*data; }
    friend ostream& operator<<(ostream& s,CJDComplexRef& r) { return s<<DComplex(r); }
};

class ROCJDComplexRef {
  private:
    DComplex*                data;
    unsigned char       rocj;         // 0:readable, 1:readonly, 2:conj
    DComplex                 error(DComplex);   // Called when op=() invoked for a readonly
  public:
    ROCJDComplexRef(DComplex& x, RWBoolean ro=FALSE, RWBoolean cj=FALSE) : data(&x)
                { rocj=(ro ? 1 : (cj ? 2 : 0 )); }
    ROCJDComplexRef& operator=(DComplex x)
        { *data=( (rocj==0) ? x : ((rocj==2) ? conj(x) : error(x)) );
          return *this;}
    operator DComplex()      { return (rocj==2) ? conj(*data) : *data; }
    friend ostream& operator<<(ostream& s,ROCJDComplexRef& r) { return s<<DComplex(r); }
};

#endif

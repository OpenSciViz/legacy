#ifndef __RWDBANDMAT_H__
#define __RWDBANDMAT_H__
/*
 * DoubleBandMat - A banded matrix of Doubles
 *
 * Stores a banded matrix.  All entries farther above the diagonal
 * that the upper bandwidth, or farther below than the lower bandwidth
 * are defined to be zero.  The total bandwidth is the sum of the 
 * upper and lower bandwidths, plus one for the diagonal.  Only
 * the elements which may be non-zero are stored.
 *
 * The matrix is stored column by column.  Each column takes up 
 * <bandwidth> entries, some of these entries near the corners
 * are not used.
 *
 * Generated from template $Id: xmat.h,v 1.4 1993/10/11 21:48:17 alv Exp $
 *
 * Bounds checking is enabled by setting the preprocessor
 * symbol RWBOUNDS_CHECK.  This is strongly encouraged.
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

#include "rw/dvec.h"
#include "rw/dref.h"

extern Double rwDoubleZero;	// This constant is sometimes returned by reference

class FloatBandMat;
class DoubleGenMat;
class DoubleTriDiagMat;
class DoubleSymBandMat;

class DoubleBandMat {
private:

DoubleVec vec;
unsigned n;
unsigned band, bandu;	// bandwidth and upper bandwidth
  // The data which define the matrix

  void zeroUnusedEntries();  // Initialize entries in the data vector which are not used to zero

public:
DoubleBandMat();
DoubleBandMat( const DoubleBandMat& );
DoubleBandMat(unsigned n, unsigned nAgain, unsigned lowerWidth, unsigned upperWidth);
DoubleBandMat(const DoubleVec& data, unsigned n, unsigned nAgain, unsigned lowerWidth, unsigned upperWdth);
DoubleBandMat(const FloatBandMat&);
DoubleBandMat( const DoubleTriDiagMat& A );
DoubleBandMat( const DoubleSymBandMat& A );
~DoubleBandMat();
  // Constructors, conversion functions, and a destructor.

static const char *className;
  // For use in error messages

inline unsigned rows() const	   { return n; }
inline unsigned cols() const	   { return n; }
inline unsigned bandwidth() const      { return band; }
inline unsigned lowerBandwidth() const { return band-bandu-1; }
inline unsigned upperBandwidth() const { return bandu; }
  // Member functions to access the shape of the matrix

inline Double  val(int i, int j) const;
       Double  bcval(int i, int j) const;
inline Double  set(int i, int j, Double x);
       Double  bcset(int i, int j, Double x);
inline RODoubleRef operator()(int i, int j);
inline RODoubleRef ref(int i, int j);
       RODoubleRef bcref(int i, int j);
inline Double  operator()(int i, int j) const; // { return val(i,j); }
  // Access and/or change elements of the matrix.
  // The versions that begin with the letters bc do bounds
  // checking.  The others do bounds checking only if the
  // preprocessor symbol RWBOUNDS_CHECK is defined.

DoubleBandMat leadingSubmatrix(int order);
  // Access to submatrices.
  // The leading submatrix of order k is the kxk matrix in the upper
  // left.
inline DoubleVec diagonal(int =0) const;
       DoubleVec bcdiagonal(int =0) const;
  // Access to diagonals.  The same conventions as for
  // element access apply (with respect to bcxxx and xxx names).

DoubleBandMat& operator=(const DoubleBandMat& m);
DoubleBandMat& reference(DoubleBandMat& m);
  // reference() makes an alias, operator= makes a copy.


void zero()        { vec=Double(0); }
  // Sets all elements of the matrix equal to zero.


DoubleBandMat copy() const;
DoubleBandMat deepCopy() const		{ return copy(); }
void deepenShallowCopy()        { vec.deepenShallowCopy(); }
  // copy() returns a DoubleBandMat with its own copy of the data.
  // deepenShallowCopy makes sure the data is not shared by another
  // vector or matrix.

DoubleVec dataVec() 		{ return vec; }
const DoubleVec& dataVec() const	{ return vec; }
Double* data() 			{ return vec.data(); }
const Double* data() const 	{ return vec.data(); }
  // Access to the raw data.

void resize(unsigned m, unsigned n);
void resize(unsigned n, unsigned nAgain, unsigned lowWidth, unsigned upWidth);
  // Resize the matrix.  New elements are set to zero.


void scanFrom(istream&);      		// Human readable
void printOn(ostream&) const;
void restoreFrom(RWvistream&);     	// Internal ASCII or binary code
void saveOn(RWvostream&) const;
void restoreFrom(RWFile&);
void saveOn(RWFile&) const;
unsigned binaryStoreSize() const;
  // Input/output member functions
  // Whether ASCII or binary formatting is used with readFrom and
  // storeOn is determined by the type of rwstream passed.

/*
 * The operator()() member functions are declared with ref(),
 * since the declarations look the same.
 *
 * The operator=() assignment operators are declared with reference(),
 * since the functions are so closely related.
 */

RWBoolean operator==(const DoubleBandMat& X);
RWBoolean operator!=(const DoubleBandMat& X)  { return !(operator==(X)); }
  // Two matrices are considered equal if their shapes are identical and
  // all their components are identical.

DoubleBandMat& operator+=(const DoubleBandMat& m);
DoubleBandMat& operator-=(const DoubleBandMat& m);
DoubleBandMat& operator*=(const DoubleBandMat& m);
DoubleBandMat& operator*=(Double);
DoubleBandMat& operator/=(const DoubleBandMat& m);
DoubleBandMat& operator/=(Double);
  // assignment operators.  self must be same size as m.


};

DoubleBandMat operator-(const DoubleBandMat&);	// Unary minus
DoubleBandMat operator+(const DoubleBandMat&);	// Unary plus
DoubleBandMat operator*(const DoubleBandMat&, const DoubleBandMat&);
DoubleBandMat operator/(const DoubleBandMat&, const DoubleBandMat&);
DoubleBandMat operator+(const DoubleBandMat&, const DoubleBandMat&);
DoubleBandMat operator-(const DoubleBandMat&, const DoubleBandMat&);
DoubleBandMat operator*(const DoubleBandMat&, Double);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
inline DoubleBandMat operator*(Double x, const DoubleBandMat& A) { return A*x; }
#else
       DoubleBandMat operator*(Double x, const DoubleBandMat& A);
#endif
DoubleBandMat operator/(const DoubleBandMat& A, Double x);
  // Arithmetic operators; Notice that operator* is an
  // element by element multiply, NOT a matrix multiply.

inline ostream& operator<<(ostream& s, const DoubleBandMat& m)
		{m.printOn(s); return s;}
inline istream& operator>>(istream& s, DoubleBandMat& m)
		{m.scanFrom(s); return s;}
  // I/O for humans

DoubleBandMat transpose(const DoubleBandMat&);
  // The transpose of the matrix. 

DoubleVec product(const DoubleBandMat& A, const DoubleVec& x);
DoubleVec product(const DoubleVec& x, const DoubleBandMat& A);
DoubleBandMat product(const DoubleBandMat& A, const DoubleBandMat& B);
DoubleBandMat transposeProduct(const DoubleBandMat& A, const DoubleBandMat& B);
  // transposeProduct calculates A^TB
  // inner products

DoubleBandMat toBandMat( const DoubleGenMat& A, unsigned bandl, unsigned bandu );

DoubleBandMat abs(const DoubleBandMat& A);

inline Double minValue(const DoubleBandMat& A) { return minValue(A.dataVec()); }
inline Double maxValue(const DoubleBandMat& A) { return maxValue(A.dataVec()); }


/*
 * Inline functions
 */

inline Double DoubleBandMat::val(int i, int j) const {
    return bcval(i,j);
}

inline Double DoubleBandMat::set(int i, int j, Double x) {
#ifdef RWBOUNDS_CHECK
return bcset(i,j,x);
#else
    return vec(i-j+bandu + j*band)=x;
#endif
}

inline RODoubleRef DoubleBandMat::ref(int i, int j) {
    return bcref(i,j);
}

inline RODoubleRef DoubleBandMat::operator()(int i, int j) { return ref(i,j); }
inline Double DoubleBandMat::operator()(int i, int j) const { return val(i,j); }

inline DoubleVec DoubleBandMat::diagonal(int i) const {
#if defined(RWBOUNDS_CHECK)
    return bcdiagonal(i);
#else
    int iabs = (i>=0) ? i : -i;
    return vec.slice(i>0 ? upperBandwidth()+i*(bandwidth()-1)
			 : upperBandwidth()+iabs
			 , n-iabs, bandwidth() );
#endif
}

#endif

#ifndef __RWRWBLA_H__
#define __RWRWBLA_H__

/*
 * RW BLA-LIKE ROUTINES
 * --------------------
 * 
 * These are low level routines used to implement the vector, matrix,
 * and array classes.  They can be implemented in assembly if you like.
 *
 * In routines which take more than one vector argument, it is assumed
 * that no aliasing takes place.
 * 
 * 
 * The generic format of the prototypes are given below.  See
 * the individual header files for the exact prototype.
 *
 * The functions fall into 6 general classes: utility, vector-vector assignments,
 * vector-scalar assignments, vector-vector binary, vector-scalar binary,
 * and scalar-vector binary.
 *
 * Names of the routines are given by:
 *
 *	rwq_opxy
 * 
 * 	- q is the precision (d=double, s=float, z=dcomplex, i=int, a=SChar, b=UChar
 * 	- op is the operation
 *		apl = arithmetic plus
 *		ami = arithmetic minus
 *		amu = arithmetic multiply
 *		adv = arithmetic divide
 *		pl  = binary plus
 *		mi  = binary minus
 *		mu  = binary multiply
 *		dv  = binary divide
 *	- xy is the type of the operands: v=vector, s=scalar.
 *
 *	Hence, for example "rwz_amuvs" is DComplex precision,
 *	arithmetic multiply, for vector-scalar operands:  x[i] *= scalar;
 *	

			UTILITY
void   rwqcopy (unsigned n, TYPE* x, int incx, const TYPE* y, int incy);	// Copy y into x
void   rwqset  (unsigned n, TYPE* x, int incx, TYPE* scalar);			// Set x equal to a scalar
int    rwqsame (unsigned n, const TYPE* x, int incx, const TYPE* y, int incy);	// Test whether x==y
void   rwqdot (unsigned n, const TYPE* x, int incx, const TYPE* y, int incy, TYPE* result);	// Calculate dot product
void   rwqcdot(unsigned n, const TYPE* x, int incx, const TYPE* y, int incy, TYPE* result);	// Calculate conjugate dot product (complex only)
int    rwqmax (unsigned n, const TYPE* x, int incx);					// Return index of max element
int    rwqmin (unsigned n, const TYPE* x, int incx);					// Return index of min element

			VECTOR-VECTOR assignments
void   rwq_aplvv (unsigned n, TYPE* x, int incx, const TYPE* y, int incy);	// x[i] += y[i]
void   rwq_amivv (unsigned n, TYPE* x, int incx, const TYPE* y, int incy);	// x[i] -= y[i]
void   rwq_amuvv (unsigned n, TYPE* x, int incx, const TYPE* y, int incy);	// x[i] *= y[i]
void   rwq_advvv (unsigned n, TYPE* x, int incx, const TYPE* y, int incy);	// x[i] /= y[i]

			VECTOR-SCALAR assignments
void   rwq_aplvs (unsigned n, TYPE* x, int incx, TYPE* scalar);			// x[i] += *scalar
void   rwq_amuvs (unsigned n, TYPE* x, int incx, TYPE* scalar);			// x[i] *= *scalar
void   rwq_advvs (unsigned n, TYPE* x, int incx, TYPE* scalar);			// x[i] /= *scalar

			VECTOR-VECTOR binary
void   rwq_plvv (unsigned n, TYPE* z, const TYPE* x, int incx, const TYPE* y, int incy);	// z[i] = x[i] + y[i]
void   rwq_mivv (unsigned n, TYPE* z, const TYPE* x, int incx, const TYPE* y, int incy);	// z[i] = x[i] - y[i]
void   rwq_muvv (unsigned n, TYPE* z, const TYPE* x, int incx, const TYPE* y, int incy);	// z[i] = x[i] * y[i]
void   rwq_dvvv (unsigned n, TYPE* z, const TYPE* x, int incx, const TYPE* y, int incy);	// z[i] = x[i] / y[i]

			VECTOR-SCALAR binary
void   rwq_plvs (unsigned n, TYPE* z, const TYPE* x, int incx, TYPE* scalar);			// z[i] = x[i] + *scalar
void   rwq_muvs (unsigned n, TYPE* z, const TYPE* x, int incx, TYPE* scalar);			// z[i] = x[i] * *scalar
void   rwq_dvvs (unsigned n, TYPE* z, const TYPE* x, int incx, TYPE* scalar);			// z[i] = x[i] / *scalar

			SCALAR-VECTOR binary
void   rwq_misv (unsigned n, TYPE* z, TYPE* scalar, const TYPE* x, int incx);			// z[i] = *scalar - x[i]
void   rwq_dvsv (unsigned n, TYPE* z, TYPE* scalar, const TYPE* x, int incx);			// z[i] = *scalar / x[i]

 */

#include "rw/rwabla.h"
#include "rw/rwbbla.h"
#include "rw/rwcbla.h"
#include "rw/rwdbla.h"
#include "rw/rwibla.h"
#include "rw/rwsbla.h"
#include "rw/rwzbla.h"

#endif /* __RWBLA_H__ */


#ifndef __RWDSYMEIG_H__
#define __RWDSYMEIG_H__

/*
 * DoubleSymEigDecomp - Spectral factorization of a symmetric/Hermitian matrix
 *
 * Generated from template $Id: xsymeig.h,v 1.2 1993/07/05 19:19:33 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * This class represents a spectral factorization of a symmetric/Hermitian
 * matrix.  In addition, it has constructors for constructing a
 * factorization from a symmetric/Hermitian or banded symmetric/Hermitian,
 * with an option of whether or not to compute eigenvectors.  The
 * actual computation is done by a subclass of DoubleSymEigServer.
 * If you want to control which server gets used, or need more
 * control over the computation, then you need to use an eigenserver
 * class directly.
 *
 */ 

#include "rw/lapkdefs.h"
#include "rw/dgenmat.h"
#include "rw/dvec.h"
class DoubleSymEigDecomp;   // declaration here for complex types

class DoubleSymEigDecomp {
private:
  unsigned  n;           // dimension of the matrix
  DoubleVec    lambda;      // computed eigenvalues (length may be < n)
  DoubleGenMat P;           // columns contain eigenvectors
  RWBoolean computedAll; // Did I compute everything the server tried to compute?
  RWBoolean accurate;    // Were all results as accurate as possible?

public:
  DoubleSymEigDecomp();
  DoubleSymEigDecomp(const DoubleSymEigDecomp&);
  DoubleSymEigDecomp(const DoubleSymMat&, RWBoolean computeVecs=TRUE);
  DoubleSymEigDecomp(const DoubleSymBandMat&, RWBoolean computeVecs=TRUE);
  void factor(const DoubleSymMat&, RWBoolean computeVecs=TRUE);
  void factor(const DoubleSymBandMat&, RWBoolean computeVecs=TRUE);

  void      operator=(const DoubleSymEigDecomp&);

  unsigned        cols()           const {return n;}
  Double             eigenValue(int)  const;
  const DoubleVec    eigenValues()    const {return lambda;}
  const DoubleVec    eigenVector(int) const;
  const DoubleGenMat eigenVectors()   const {return P;}
  RWBoolean       good()           const; // True if everything went perfectly
  RWBoolean       inaccurate()     const; // Either failure, or some things are not fully accurate
  RWBoolean       fail()           const; // An eigenvalue or vector wasn't computed
  unsigned        numEigenValues() const {return lambda.length();}
  unsigned        numEigenVectors()const {return P.cols();}
  unsigned        rows()           const {return n;}
               
  friend class DoubleSymEigServer;
  friend class DComplexHermEigServer;
  friend class DComplexHermEigDecomp;
};

#endif


#ifndef __RWCARR_H__
#define __RWCARR_H__

/*
 * Declarations for DComplex precision arrays
 *
 * Generated from template $Id: array.htm,v 1.13 1993/09/19 15:18:58 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311        FAX: (503) 757-7350
 *
 * Copyright (C) 1988, 1989, 1990, 1991.  This software is subject to copyright
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * Right now, functions are only provided for easy indexing up to 4D.
 * Beyond this, you need to use the general functions (ie with IntVecs).
 * It would be easy to add 5D or higher functions if need be, except that
 * the op() functions would go through a combinatorial explosion.  As 
 * long as you are content to always get back an Array, and not explicitly
 * a Vec or GenMat as the case may be, then this explosion can be avoided.
 */

/*
 * Defining the preprocessor directive RWBOUNDS_CHECK will
 * cause bounds checking on the slice function, subscripting operators
 * and inlined math functions.
 *
 * Bounds checking is always done on subscripting operators returning
 * anything other than a DComplex or DComplex&.
 */

#include "rw/darr.h"
#include "rw/dcomplex.h"
#include "rw/igenmat.h"
#include "rw/array.h"

class RWRand;

class DComplexArray : public RWArrayView {
private:
    DComplexArray(const RWDataView&, DComplex*, const IntVec&, const IntVec&); // For real() and imag() and slices

public:

    /****************
     * Constructors *
     ****************/

    DComplexArray();                            // Declares a scalar (0D array)
    DComplexArray(const IntVec&, RWUninitialized, Storage=COLUMN_MAJOR);
    DComplexArray(unsigned,unsigned,unsigned,RWUninitialized);
    DComplexArray(unsigned,unsigned,unsigned,unsigned,RWUninitialized);
    DComplexArray(const IntVec&, RWRand&, Storage=COLUMN_MAJOR);
    DComplexArray(unsigned,unsigned,unsigned,RWRand&);
    DComplexArray(unsigned,unsigned,unsigned,unsigned,RWRand&);
    DComplexArray(const IntVec& n, DComplex val);
    DComplexArray(unsigned,unsigned,unsigned, DComplex val);
    DComplexArray(unsigned,unsigned,unsigned,unsigned, DComplex val);
    DComplexArray(const char *);
    DComplexArray(const DComplexArray& a);
    DComplexArray(const DComplex* dat, const IntVec& n); // Copy of dat will be made
    DComplexArray(const DComplex* dat, unsigned,unsigned,unsigned);
    DComplexArray(const DComplex* dat, unsigned,unsigned,unsigned,unsigned);
    DComplexArray(const DComplexVec& vec, const IntVec& n); // View of dat will be taken
    DComplexArray(const DComplexVec& vec, unsigned,unsigned,unsigned);
    DComplexArray(const DComplexVec& vec, unsigned,unsigned,unsigned,unsigned);
    DComplexArray(RWBlock *block, const IntVec& n); // Use a custom block for the data
    DComplexArray(RWBlock *block, unsigned,unsigned,unsigned);
    DComplexArray(RWBlock *block, unsigned,unsigned,unsigned,unsigned);
    DComplexArray(const DComplexVec&);          // View will be taken
    DComplexArray(const DComplexGenMat&);       // View will be taken
    DComplexArray(const DoubleArray& re);       // Conversion from DoubleArray
    DComplexArray(const DoubleArray& re, const DoubleArray& im);


    /********************
     * Member functions *
     ********************/

    DComplexArray apply(CmathFunTy)     const;
    DoubleArray   apply2(CmathFunTy2)   const;
    unsigned      binaryStoreSize()     const;   // Storage requirements
    DComplexArray copy()                const;   // Copy with distinct instance variables
    DComplex*     data()                         {return (DComplex*)begin;}
    const DComplex* data()              const    {return (DComplex*)begin;}
    DComplexArray deepCopy()            const;   // Copy with distinct instance variables 
    void          deepenShallowCopy();           // Insures only 1 ref and data is compact
//  unsigned      dimension()           const    {return npts.length();}
//  const IntVec& length()              const    {return npts;}
//  int           length(int i)         const    {return npts(i);}
    void          printOn(ostream& s)   const;   // Pretty print
    DComplexArray& reference(const DComplexArray& v); // Reference self to v
    void          resize(const IntVec&);         // Will pad with zeroes if necessary
    void          resize(unsigned,unsigned,unsigned);
    void          resize(unsigned,unsigned,unsigned,unsigned);
    void          reshape(const IntVec&);        // Contents after reshape are garbage
    void          reshape(unsigned,unsigned,unsigned);
    void          reshape(unsigned,unsigned,unsigned,unsigned);
    void          restoreFrom(RWFile&);          // Restore from binary
    void          restoreFrom(RWvistream&);      // Restore from virtual stream
    void          saveOn(RWFile&)       const;   // Store using binary
    void          saveOn(RWvostream&)   const;   // Store to virtual stream
    void          scanFrom(istream& s);          // Read to eof or delimit with []
    DComplexArray slice(const IntVec& start, const IntVec& lgt, const IntGenMat& strider) const;
//  const IntVec& stride()              const    {return step;}
//  int           stride(int i)         const    {return step(i);}

    /********************
     * Member operators *
     ********************/

    DComplex&       operator[](const IntVec& i); // With bounds checking
    DComplex        operator[](const IntVec& i) const;
    DComplex&       operator()(const IntVec& i); // With optional bounds checking
    DComplex        operator()(const IntVec& i) const;
    DComplex&       operator()(int,int,int);
    DComplex        operator()(int,int,int) const;
    DComplex&       operator()(int,int,int,int);
    DComplex        operator()(int,int,int,int) const;
    DComplexVec     operator()(const RWSlice&,int,int);
    const DComplexVec operator()(const RWSlice&,int,int) const;
    DComplexVec     operator()(int,const RWSlice&,int);
    const DComplexVec operator()(int,const RWSlice&,int) const;
    DComplexVec     operator()(int,int,const RWSlice&);
    const DComplexVec operator()(int,int,const RWSlice&) const;
    DComplexVec     operator()(const RWSlice&,int,int,int);
    const DComplexVec operator()(const RWSlice&,int,int,int) const;
    DComplexVec     operator()(int,const RWSlice&,int,int);
    const DComplexVec operator()(int,const RWSlice&,int,int) const;
    DComplexVec     operator()(int,int,const RWSlice&,int);
    const DComplexVec operator()(int,int,const RWSlice&,int) const;
    DComplexVec     operator()(int,int,int,const RWSlice&);
    const DComplexVec operator()(int,int,int,const RWSlice&) const;
    DComplexGenMat  operator()(int,const RWSlice&,const RWSlice&);
    const DComplexGenMat operator()(int,const RWSlice&,const RWSlice&) const;
    DComplexGenMat  operator()(const RWSlice&,int,const RWSlice&);
    const DComplexGenMat operator()(const RWSlice&,int,const RWSlice&) const;
    DComplexGenMat  operator()(const RWSlice&,const RWSlice&,int);
    const DComplexGenMat operator()(const RWSlice&,const RWSlice&,int) const;
    DComplexGenMat  operator()(int,int,const RWSlice&,const RWSlice&);
    const DComplexGenMat operator()(int,int,const RWSlice&,const RWSlice&) const;
    DComplexGenMat  operator()(int,const RWSlice&,int,const RWSlice&);
    const DComplexGenMat operator()(int,const RWSlice&,int,const RWSlice&) const;
    DComplexGenMat  operator()(int,const RWSlice&,const RWSlice&,int);
    const DComplexGenMat operator()(int,const RWSlice&,const RWSlice&,int) const;
    DComplexGenMat  operator()(const RWSlice&,int,int,const RWSlice&);
    const DComplexGenMat operator()(const RWSlice&,int,int,const RWSlice&) const;
    DComplexGenMat  operator()(const RWSlice&,int,const RWSlice&,int);
    const DComplexGenMat operator()(const RWSlice&,int,const RWSlice&,int) const;
    DComplexGenMat  operator()(const RWSlice&,const RWSlice&,int,int);
    const DComplexGenMat operator()(const RWSlice&,const RWSlice&,int,int) const;
    DComplexArray   operator()(const RWSlice&,const RWSlice&,const RWSlice&);
    const DComplexArray operator()(const RWSlice&,const RWSlice&,const RWSlice&) const;
    DComplexArray   operator()(int,const RWSlice&,const RWSlice&,const RWSlice&);
    const DComplexArray operator()(int,const RWSlice&,const RWSlice&,const RWSlice&) const;
    DComplexArray   operator()(const RWSlice&,int,const RWSlice&,const RWSlice&);
    const DComplexArray operator()(const RWSlice&,int,const RWSlice&,const RWSlice&) const;
    DComplexArray   operator()(const RWSlice&,const RWSlice&,int,const RWSlice&);
    const DComplexArray operator()(const RWSlice&,const RWSlice&,int,const RWSlice&) const;
    DComplexArray   operator()(const RWSlice&,const RWSlice&,const RWSlice&,int);
    const DComplexArray operator()(const RWSlice&,const RWSlice&,const RWSlice&,int) const;
    DComplexArray   operator()(const RWSlice&,const RWSlice&,const RWSlice&,const RWSlice&);
    const DComplexArray operator()(const RWSlice&,const RWSlice&,const RWSlice&,const RWSlice&) const;
    DComplexArray& operator=(const DComplexArray& v); // Must be same length as v
    DComplexArray& operator=(DComplex);
    RWBoolean   operator==(const DComplexArray&) const;
    RWBoolean   operator!=(const DComplexArray& v) const;
    DComplexArray& operator+=(DComplex);
    DComplexArray& operator-=(DComplex s)       { return operator+=(-s); }
    DComplexArray& operator*=(DComplex);
    DComplexArray& operator/=(DComplex s);
    DComplexArray& operator+=(const DComplexArray&);
    DComplexArray& operator-=(const DComplexArray&);
    DComplexArray& operator*=(const DComplexArray&);
    DComplexArray& operator/=(const DComplexArray&);

    friend DoubleArray real(const DComplexArray&);    // real and imag need access to the
    friend DoubleArray imag(const DComplexArray&);    // RWBlock, so they must be friends
    friend DComplex       toScalar(const DComplexArray&);    // For 0-D arrays
    friend DComplexVec    toVec(const DComplexArray&);       // For 1-D arrays
    friend DComplexGenMat toGenMat(const DComplexArray&);    // For 2-D arrays
};
  
    /********************
     * Global Operators *
     ********************/

           DComplexArray operator-(const DComplexArray&);
    inline DComplexArray operator+(const DComplexArray& v) { return v; }
           DComplexArray operator+(const DComplexArray&, const DComplexArray&);
           DComplexArray operator-(const DComplexArray&, const DComplexArray&);
           DComplexArray operator*(const DComplexArray&, const DComplexArray&);
           DComplexArray operator/(const DComplexArray&, const DComplexArray&);
           DComplexArray operator+(const DComplexArray&,DComplex);
           DComplexArray operator+(DComplex, const DComplexArray&);
           DComplexArray operator-(const DComplexArray&,DComplex);
           DComplexArray operator-(DComplex, const DComplexArray&);
           DComplexArray operator*(const DComplexArray&,DComplex);
           DComplexArray operator*(DComplex, const DComplexArray&);
           DComplexArray operator/(const DComplexArray&,DComplex);
           DComplexArray operator/(DComplex, const DComplexArray&);
           ostream&    operator<<(ostream& s, const DComplexArray& v);
           istream&    operator>>(istream& s, DComplexArray& v);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
    inline DComplexArray operator*(DComplex s, const DComplexArray& V) {return V*s;}
    inline DComplexArray operator+(DComplex s, const DComplexArray& V) {return V+s;}
    inline DComplexArray operator-(const DComplexArray& V, DComplex s) {return V+(DComplex)(-s);}
#endif


    /********************
     * Global Functions *
     ********************/

    /**** Miscellaneous Functions ****/

           DoubleArray abs(const DComplexArray& V);
           DComplex    dot(const DComplexArray&,const DComplexArray&); // Return sum_ij...k (Aij...k * Bij...k)
           DComplexArray dot(const DComplexVec&,const DComplexArray&); // Return sum_i (Vi * Aij...k)
           DComplexArray dot(const DComplexArray&,const DComplexVec&); // Return sum_k (Ai...jk * Vk)
           DComplex    prod(const DComplexArray&);
           DComplex    sum(const DComplexArray&);


    /**** Functions for both real and complex ****/

           DComplexArray cos(const DComplexArray& V);
           DComplexArray cosh(const DComplexArray& V);
           DComplexArray exp(const DComplexArray& V);
           DComplex    mean(const DComplexArray& V);
           DComplexArray pow(const DComplexArray&,const DComplexArray&);
           DComplexArray sin(const DComplexArray& V);
           DComplexArray sinh(const DComplexArray& V);
           DComplexArray sqrt(const DComplexArray& V);
           double      variance(const DComplexArray&);


    /**** Functions for complex arrays ****/

           DoubleArray arg(const DComplexArray& V);
           DComplexArray conj(const DComplexArray& V);
           DoubleArray imag(const DComplexArray&); // Returns reference to data
           DoubleArray norm(const DComplexArray& V);
           DoubleArray real(const DComplexArray&); // Returns reference to data

    /**** Functions for type conversion ****/

                                                  // No type conversion functions for DComplexArray

    /**** Norm functions ****/
           Double      frobNorm(const DComplexArray&); // Root of sum of squares
           Double      maxNorm(const DComplexArray&); // Largest absolute value, not really a norm

    /***************************
     * Inline Access Functions *
     ***************************/


  inline DComplex& DComplexArray::operator[](const IntVec& i) {
    boundsCheck(i);
    return data()[dot(i,step)];
  }

  inline DComplex DComplexArray::operator[](const IntVec& i) const {
    boundsCheck(i);
    return data()[dot(i,step)];
  }

  inline DComplex& DComplexArray::operator()(const IntVec& i) {
#  ifdef RWBOUNDS_CHECK    
    boundsCheck(i);
#  endif
    return data()[dot(i,step)];
  }

  inline DComplex DComplexArray::operator()(const IntVec& i) const {
#  ifdef RWBOUNDS_CHECK    
    boundsCheck(i);
#  endif
    return data()[dot(i,step)];
  }

  inline DComplex& DComplexArray::operator()(int i, int j, int k) {
#  ifdef RWBOUNDS_CHECK
    boundsCheck(i,j,k);
#  endif
    return data()[i*step((int)0)+j*step(1)+k*step(2)];
  }

  inline DComplex DComplexArray::operator()(int i, int j, int k) const {
#  ifdef RWBOUNDS_CHECK
    boundsCheck(i,j,k);
#  endif
    return data()[i*step((int)0)+j*step(1)+k*step(2)];
  }

  inline DComplex& DComplexArray::operator()(int i, int j, int k, int l) {
#  ifdef RWBOUNDS_CHECK
    boundsCheck(i,j,k,l);
#  endif
    return data()[i*step((int)0)+j*step(1)+k*step(2)+l*step(3)];
  }

  inline DComplex DComplexArray::operator()(int i, int j, int k, int l) const {
#  ifdef RWBOUNDS_CHECK
    boundsCheck(i,j,k,l);
#  endif
    return data()[i*step((int)0)+j*step(1)+k*step(2)+l*step(3)];
  }

#endif /*__RWCARR_H__*/

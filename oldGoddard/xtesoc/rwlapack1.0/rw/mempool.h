#ifndef __RWMEMPOOL_H__
#define __RWMEMPOOL_H__

/*
 * RWMemoryPool: Manages small object pool via inheritance
 *
 * $Id: mempool.h,v 2.7 1993/11/08 09:11:13 jims Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1989, 1990, 1991.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: mempool.h,v $
 * Revision 2.7  1993/11/08  09:11:13  jims
 * Port to ObjectStore
 *
 * Revision 2.6  1993/09/03  02:08:13  keffer
 * Macro _CLASSDLL is now named _RWTOOLSDLL
 *
 * Revision 2.5  1993/08/05  11:49:12  jims
 * Distinguish between using a WIN16 DLL from a WIN32 DLL by
 * checking for __WIN16__
 *
 * Revision 2.4  1993/03/17  18:41:25  keffer
 * TCC_DELETE_SIZE_BUG -> RW_TCC_DELETE_SIZE_BUG
 *
 * Revision 2.3  1993/02/18  17:00:47  keffer
 * Constants no longer declared static.
 *
 * Revision 2.2  1993/01/28  01:54:00  myersn
 * define macro RWMemoryPool_OPTION for MT-safe use
 *
 * Revision 2.1  1993/01/15  22:23:49  keffer
 * Tools.h++ V5.2 alpha
 *
 * Revision 2.0  1992/10/23  03:27:32  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.9   22 Jun 1992 17:52:28   KEFFER
 * Static data no longer visible when compiling or using the Tools.h++ DLL.
 * 
 *    Rev 1.8   28 May 1992 10:38:10   KEFFER
 * Now compatible with RWMEMCK.
 * 
 *    Rev 1.5   17 Oct 1991 09:13:00   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.3   31 Aug 1991 20:40:28   keffer
 * Now includes <stddef.h>, with a STARTWRAP wrapper
 * 
 *    Rev 1.1   24 Jul 1991 13:06:44   keffer
 * Added pvcs keywords
 *
 */

/*
 * This class maintains a pool of free memory allocations for small objects.
 * Objects inheriting from it will be managed by it.  It can greatly improve
 * the performance of objects that are rapidly allocated and deleted, such
 * as nodes in a linked list or binary tree.  It can also help memory
 * fragmentation.
 *
 * Two static variables affect its performance:
 *
 *  RWMAXPOOLS:		The number of different sizes managed.  The
 *			default (5) will manage objects with sizes
 *			from 1 to 10 bytes big.  Objects bigger than this
 * 			will be handled by the operating system.
 *
 *  RWPOOLSIZE:		The maximum number of objects retained within a size class.
 *			Excess objects are returned to the operating system.
 */

#ifndef __RWDEFS_H__
#  include "rw/defs.h"
#endif
STARTWRAP
#include <stddef.h>	/* Looking for size_t*/
ENDWRAP

const int RWPOOLSIZE = 5;
const int RWMAXPOOLS = 5;

class RWExport RWMemoryPool {
#if !defined(_RWTOOLSDLL) || !defined(__WIN16__)
  static void*		stash[RWMAXPOOLS][RWPOOLSIZE];	// The private stash
  static short		nstash[RWMAXPOOLS];		// Number of free objects in each size class.
#endif
public:
#ifdef RW_TCC_DELETE_SIZE_BUG
  ~RWMemoryPool() { }	// Superfluous destructor required for Borland bug
#endif
  void			operator delete(void*, size_t);
  void*			operator new(size_t);
#ifdef RWMEMCK
  void*			operator new(size_t size, const char* filename, int line);
#endif
};

// Multi-threaded programs can't afford conflicts in the static
// RWMemoryPool free-block pool.  We inherit differently because
// otherwise every derived object would grow by (up to) 4 bytes,
// for no reason.

#ifdef RW_MULTI_THREAD
#define RWMemoryPool_OPTION
#else
#define RWMemoryPool_OPTION   : public RWMemoryPool
#endif

#endif	/* __RWMEMPOOL_H__ */

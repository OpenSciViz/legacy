#ifndef __RWDLTRIMAT_H__
#define __RWDLTRIMAT_H__
/*
 * DoubleTriMat - A lower triangular matrix of Doubles
 *
 * Stores a lower triangular matrix.
 *
 * The matrix is stored in row major order.  This way the vector
 * of data is exactly the same as the matrix's transpose (an UpperTriMat)
 *
 * Generated from template $Id: xmat.h,v 1.4 1993/10/11 21:48:17 alv Exp $
 *
 * Bounds checking is enabled by setting the preprocessor
 * symbol RWBOUNDS_CHECK.  This is strongly encouraged.
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

#include "rw/dvec.h"
#include "rw/dref.h"

extern Double rwDoubleZero;	// This constant is sometimes returned by reference

class FloatLowerTriMat;
class DoubleGenMat;
class DoubleUpperTriMat;

class DoubleLowerTriMat {
private:

DoubleVec vec;
unsigned n;
  // The data which define the matrix


public:
DoubleLowerTriMat();
DoubleLowerTriMat( const DoubleLowerTriMat& );
DoubleLowerTriMat(unsigned n, unsigned nAgain);
DoubleLowerTriMat(const DoubleVec& data, unsigned n, unsigned nAgain);
DoubleLowerTriMat(const FloatLowerTriMat&);
~DoubleLowerTriMat();
  // Constructors, conversion functions, and a destructor.

static const char *className;
  // For use in error messages

inline unsigned rows() const	   { return n; }
inline unsigned cols() const	   { return n; }
  // Member functions to access the shape of the matrix

inline Double  val(int i, int j) const;
       Double  bcval(int i, int j) const;
inline Double  set(int i, int j, Double x);
       Double  bcset(int i, int j, Double x);
inline RODoubleRef operator()(int i, int j);
inline RODoubleRef ref(int i, int j);
       RODoubleRef bcref(int i, int j);
inline Double  operator()(int i, int j) const; // { return val(i,j); }
  // Access and/or change elements of the matrix.
  // The versions that begin with the letters bc do bounds
  // checking.  The others do bounds checking only if the
  // preprocessor symbol RWBOUNDS_CHECK is defined.

DoubleLowerTriMat leadingSubmatrix(int order);
  // Access to submatrices.
  // The leading submatrix of order k is the kxk matrix in the upper
  // left.
  // Access to diagonals.  The same conventions as for
  // element access apply (with respect to bcxxx and xxx names).

DoubleLowerTriMat& operator=(const DoubleLowerTriMat& m);
DoubleLowerTriMat& reference(DoubleLowerTriMat& m);
  // reference() makes an alias, operator= makes a copy.


void zero()        { vec=Double(0); }
  // Sets all elements of the matrix equal to zero.


DoubleLowerTriMat copy() const;
DoubleLowerTriMat deepCopy() const		{ return copy(); }
void deepenShallowCopy()        { vec.deepenShallowCopy(); }
  // copy() returns a DoubleLowerTriMat with its own copy of the data.
  // deepenShallowCopy makes sure the data is not shared by another
  // vector or matrix.

DoubleVec dataVec() 		{ return vec; }
const DoubleVec& dataVec() const	{ return vec; }
Double* data() 			{ return vec.data(); }
const Double* data() const 	{ return vec.data(); }
  // Access to the raw data.

void resize(unsigned m, unsigned n);
  // Resize the matrix.  New elements are set to zero.


void scanFrom(istream&);      		// Human readable
void printOn(ostream&) const;
void restoreFrom(RWvistream&);     	// Internal ASCII or binary code
void saveOn(RWvostream&) const;
void restoreFrom(RWFile&);
void saveOn(RWFile&) const;
unsigned binaryStoreSize() const;
  // Input/output member functions
  // Whether ASCII or binary formatting is used with readFrom and
  // storeOn is determined by the type of rwstream passed.

/*
 * The operator()() member functions are declared with ref(),
 * since the declarations look the same.
 *
 * The operator=() assignment operators are declared with reference(),
 * since the functions are so closely related.
 */

RWBoolean operator==(const DoubleLowerTriMat& X);
RWBoolean operator!=(const DoubleLowerTriMat& X)  { return !(operator==(X)); }
  // Two matrices are considered equal if their shapes are identical and
  // all their components are identical.

DoubleLowerTriMat& operator+=(const DoubleLowerTriMat& m);
DoubleLowerTriMat& operator-=(const DoubleLowerTriMat& m);
DoubleLowerTriMat& operator*=(const DoubleLowerTriMat& m);
DoubleLowerTriMat& operator*=(Double);
DoubleLowerTriMat& operator/=(const DoubleLowerTriMat& m);
DoubleLowerTriMat& operator/=(Double);
  // assignment operators.  self must be same size as m.


};

DoubleLowerTriMat operator-(const DoubleLowerTriMat&);	// Unary minus
DoubleLowerTriMat operator+(const DoubleLowerTriMat&);	// Unary plus
DoubleLowerTriMat operator*(const DoubleLowerTriMat&, const DoubleLowerTriMat&);
DoubleLowerTriMat operator/(const DoubleLowerTriMat&, const DoubleLowerTriMat&);
DoubleLowerTriMat operator+(const DoubleLowerTriMat&, const DoubleLowerTriMat&);
DoubleLowerTriMat operator-(const DoubleLowerTriMat&, const DoubleLowerTriMat&);
DoubleLowerTriMat operator*(const DoubleLowerTriMat&, Double);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
inline DoubleLowerTriMat operator*(Double x, const DoubleLowerTriMat& A) { return A*x; }
#else
       DoubleLowerTriMat operator*(Double x, const DoubleLowerTriMat& A);
#endif
DoubleLowerTriMat operator/(const DoubleLowerTriMat& A, Double x);
  // Arithmetic operators; Notice that operator* is an
  // element by element multiply, NOT a matrix multiply.

inline ostream& operator<<(ostream& s, const DoubleLowerTriMat& m)
		{m.printOn(s); return s;}
inline istream& operator>>(istream& s, DoubleLowerTriMat& m)
		{m.scanFrom(s); return s;}
  // I/O for humans

DoubleUpperTriMat transpose(const DoubleLowerTriMat&);
  // The transpose of the matrix. 

DoubleVec product(const DoubleLowerTriMat& A, const DoubleVec& x);
DoubleVec product(const DoubleVec& x, const DoubleLowerTriMat& A);
  // inner products

DoubleLowerTriMat toLowerTriMat( const DoubleGenMat& A );

DoubleLowerTriMat abs(const DoubleLowerTriMat& A);

inline Double minValue(const DoubleLowerTriMat& A) { return minValue(A.dataVec()); }
inline Double maxValue(const DoubleLowerTriMat& A) { return maxValue(A.dataVec()); }


/*
 * Inline functions
 */

inline Double DoubleLowerTriMat::val(int i, int j) const {
#ifdef RWBOUNDS_CHECK
return bcval(i,j);
#else
    return (i<j) ? (Double)(0) : vec(i*(i+1)/2+j);
#endif
}

inline Double DoubleLowerTriMat::set(int i, int j, Double x) {
#ifdef RWBOUNDS_CHECK
return bcset(i,j,x);
#else
    return vec(i*(i+1)/2+j)=x; 
#endif
}

inline RODoubleRef DoubleLowerTriMat::ref(int i, int j) {
    return bcref(i,j);
}

inline RODoubleRef DoubleLowerTriMat::operator()(int i, int j) { return ref(i,j); }
inline Double DoubleLowerTriMat::operator()(int i, int j) const { return val(i,j); }


#endif

#ifndef __RWFUTRIMAT_H__
#define __RWFUTRIMAT_H__
/*
 * FloatTriMat - An upper triangular matrix of Floats
 *
 * Stores an upper triangular matrix.
 *
 * The matrix is stored in column major order.
 *
 * Generated from template $Id: xmat.h,v 1.4 1993/10/11 21:48:17 alv Exp $
 *
 * Bounds checking is enabled by setting the preprocessor
 * symbol RWBOUNDS_CHECK.  This is strongly encouraged.
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

#include "rw/fvec.h"
#include "rw/fref.h"

extern Float rwFloatZero;	// This constant is sometimes returned by reference

class DoubleUpperTriMat;
class FloatGenMat;
class FloatLowerTriMat;

class FloatUpperTriMat {
private:

FloatVec vec;
unsigned n;
  // The data which define the matrix


public:
FloatUpperTriMat();
FloatUpperTriMat( const FloatUpperTriMat& );
FloatUpperTriMat(unsigned n, unsigned nAgain);
FloatUpperTriMat(const FloatVec& data, unsigned n, unsigned nAgain);
~FloatUpperTriMat();
  // Constructors, conversion functions, and a destructor.

static const char *className;
  // For use in error messages

inline unsigned rows() const	   { return n; }
inline unsigned cols() const	   { return n; }
  // Member functions to access the shape of the matrix

inline Float  val(int i, int j) const;
       Float  bcval(int i, int j) const;
inline Float  set(int i, int j, Float x);
       Float  bcset(int i, int j, Float x);
inline ROFloatRef operator()(int i, int j);
inline ROFloatRef ref(int i, int j);
       ROFloatRef bcref(int i, int j);
inline Float  operator()(int i, int j) const; // { return val(i,j); }
  // Access and/or change elements of the matrix.
  // The versions that begin with the letters bc do bounds
  // checking.  The others do bounds checking only if the
  // preprocessor symbol RWBOUNDS_CHECK is defined.

FloatUpperTriMat leadingSubmatrix(int order);
  // Access to submatrices.
  // The leading submatrix of order k is the kxk matrix in the upper
  // left.
  // Access to diagonals.  The same conventions as for
  // element access apply (with respect to bcxxx and xxx names).

FloatUpperTriMat& operator=(const FloatUpperTriMat& m);
FloatUpperTriMat& reference(FloatUpperTriMat& m);
  // reference() makes an alias, operator= makes a copy.


void zero()        { vec=Float(0); }
  // Sets all elements of the matrix equal to zero.


FloatUpperTriMat copy() const;
FloatUpperTriMat deepCopy() const		{ return copy(); }
void deepenShallowCopy()        { vec.deepenShallowCopy(); }
  // copy() returns a FloatUpperTriMat with its own copy of the data.
  // deepenShallowCopy makes sure the data is not shared by another
  // vector or matrix.

FloatVec dataVec() 		{ return vec; }
const FloatVec& dataVec() const	{ return vec; }
Float* data() 			{ return vec.data(); }
const Float* data() const 	{ return vec.data(); }
  // Access to the raw data.

void resize(unsigned m, unsigned n);
  // Resize the matrix.  New elements are set to zero.


void scanFrom(istream&);      		// Human readable
void printOn(ostream&) const;
void restoreFrom(RWvistream&);     	// Internal ASCII or binary code
void saveOn(RWvostream&) const;
void restoreFrom(RWFile&);
void saveOn(RWFile&) const;
unsigned binaryStoreSize() const;
  // Input/output member functions
  // Whether ASCII or binary formatting is used with readFrom and
  // storeOn is determined by the type of rwstream passed.

/*
 * The operator()() member functions are declared with ref(),
 * since the declarations look the same.
 *
 * The operator=() assignment operators are declared with reference(),
 * since the functions are so closely related.
 */

RWBoolean operator==(const FloatUpperTriMat& X);
RWBoolean operator!=(const FloatUpperTriMat& X)  { return !(operator==(X)); }
  // Two matrices are considered equal if their shapes are identical and
  // all their components are identical.

FloatUpperTriMat& operator+=(const FloatUpperTriMat& m);
FloatUpperTriMat& operator-=(const FloatUpperTriMat& m);
FloatUpperTriMat& operator*=(const FloatUpperTriMat& m);
FloatUpperTriMat& operator*=(Float);
FloatUpperTriMat& operator/=(const FloatUpperTriMat& m);
FloatUpperTriMat& operator/=(Float);
  // assignment operators.  self must be same size as m.


};

FloatUpperTriMat operator-(const FloatUpperTriMat&);	// Unary minus
FloatUpperTriMat operator+(const FloatUpperTriMat&);	// Unary plus
FloatUpperTriMat operator*(const FloatUpperTriMat&, const FloatUpperTriMat&);
FloatUpperTriMat operator/(const FloatUpperTriMat&, const FloatUpperTriMat&);
FloatUpperTriMat operator+(const FloatUpperTriMat&, const FloatUpperTriMat&);
FloatUpperTriMat operator-(const FloatUpperTriMat&, const FloatUpperTriMat&);
FloatUpperTriMat operator*(const FloatUpperTriMat&, Float);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
inline FloatUpperTriMat operator*(Float x, const FloatUpperTriMat& A) { return A*x; }
#else
       FloatUpperTriMat operator*(Float x, const FloatUpperTriMat& A);
#endif
FloatUpperTriMat operator/(const FloatUpperTriMat& A, Float x);
  // Arithmetic operators; Notice that operator* is an
  // element by element multiply, NOT a matrix multiply.

inline ostream& operator<<(ostream& s, const FloatUpperTriMat& m)
		{m.printOn(s); return s;}
inline istream& operator>>(istream& s, FloatUpperTriMat& m)
		{m.scanFrom(s); return s;}
  // I/O for humans

FloatLowerTriMat transpose(const FloatUpperTriMat&);
  // The transpose of the matrix. 

FloatVec product(const FloatUpperTriMat& A, const FloatVec& x);
FloatVec product(const FloatVec& x, const FloatUpperTriMat& A);
  // inner products

FloatUpperTriMat toFloat(const DoubleUpperTriMat& A);
FloatUpperTriMat toUpperTriMat( const FloatGenMat& A );

FloatUpperTriMat abs(const FloatUpperTriMat& A);

inline Float minValue(const FloatUpperTriMat& A) { return minValue(A.dataVec()); }
inline Float maxValue(const FloatUpperTriMat& A) { return maxValue(A.dataVec()); }


/*
 * Inline functions
 */

inline Float FloatUpperTriMat::val(int i, int j) const {
#ifdef RWBOUNDS_CHECK
return bcval(i,j);
#else
    return (j<i) ? (Float)(0) : vec(j*(j+1)/2+i);
#endif
}

inline Float FloatUpperTriMat::set(int i, int j, Float x) {
#ifdef RWBOUNDS_CHECK
return bcset(i,j,x);
#else
    return vec(j*(j+1)/2+i)=x; 
#endif
}

inline ROFloatRef FloatUpperTriMat::ref(int i, int j) {
    return bcref(i,j);
}

inline ROFloatRef FloatUpperTriMat::operator()(int i, int j) { return ref(i,j); }
inline Float FloatUpperTriMat::operator()(int i, int j) const { return val(i,j); }


#endif

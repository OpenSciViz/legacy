#ifndef __RWSBLA_H__
#define __RWSBLA_H__

/*
 * float precision Basic Linear Algebra prototypes.
 *
 * $Header: /users/rcs/mathrw/rwsbla.h,v 1.1 1993/01/23 00:08:44 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * Copyright (C) 1991.  This software is subject to copyright
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: rwsbla.h,v $
 * Revision 1.1  1993/01/23  00:08:44  alv
 * Initial revision
 *
 * 
 *    Rev 1.2   17 Oct 1991 09:13:04   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.0   27 Sep 1991 11:49:50   keffer
 * Initial revision.
 */

#ifndef __MATHDEFS_H__
#  include "rw/mathdefs.h"		/* Looking for "FDecl" and "RW_KR_ONLY" */
#endif

#ifdef __cplusplus
extern "C" {
# define P_(s) s
#else
#  ifdef RW_KR_ONLY
#    define P_(s) ()
#  else
#    define P_(s) s
#  endif
#endif

void   FDecl rwscopy P_((unsigned n, float* restrict x, int incx, const float* restrict y, int incy));
void   FDecl rwsset  P_((unsigned n, float* restrict x, int incx, const float* restrict scalar));
int    FDecl rwssame P_((unsigned n, const float* restrict x, int incx, const float* restrict y, int incy));
int    FDecl rwsmax P_((unsigned n, const float* restrict x, int incx));
int    FDecl rwsmin P_((unsigned n, const float* restrict x, int incx));

void   FDecl rwsdot P_((unsigned n, const float* restrict x, int incx, const float* restrict y, int incy, float* result));
void   FDecl rwscdot P_((unsigned n, const float* restrict x, int incx, const float* restrict y, int incy, float* result));

/* For implementing "Vector& Vector::operator+=(const Vector&)" functionality: */
void   FDecl rws_aplvv P_((unsigned n, float* restrict x, int incx, const float* restrict y, int incy));
void   FDecl rws_amivv P_((unsigned n, float* restrict x, int incx, const float* restrict y, int incy));
void   FDecl rws_amuvv P_((unsigned n, float* restrict x, int incx, const float* restrict y, int incy));
void   FDecl rws_advvv P_((unsigned n, float* restrict x, int incx, const float* restrict y, int incy));

/* For implementing "Vector& Vector::operator+=(type)" functionality: */
void   FDecl rws_aplvs P_((unsigned n, float* restrict x, int incx, const float* restrict scalar));
void   FDecl rws_amuvs P_((unsigned n, float* restrict x, int incx, const float* restrict scalar));

/* For implementing "Vector operator+(const Vector&, const Vector&)" functionality: */
void   FDecl rws_plvv P_((unsigned n, float* restrict z, const float* restrict x, int incx, const float* restrict y, int incy));
void   FDecl rws_mivv P_((unsigned n, float* restrict z, const float* restrict x, int incx, const float* restrict y, int incy));
void   FDecl rws_muvv P_((unsigned n, float* restrict z, const float* restrict x, int incx, const float* restrict y, int incy));
void   FDecl rws_dvvv P_((unsigned n, float* restrict z, const float* restrict x, int incx, const float* restrict y, int incy));

/* For implementing "Vector operator+(const Vector&, type)" functionality: */
void   FDecl rws_plvs P_((unsigned n, float* restrict z, const float* restrict x, int incx, const float* restrict scalar));
void   FDecl rws_muvs P_((unsigned n, float* restrict z, const float* restrict x, int incx, const float* restrict scalar));
void   FDecl rws_dvvs P_((unsigned n, float* restrict z, const float* restrict x, int incx, const float* restrict scalar));

/* For implementing "Vector operator+(type, const Vector&)" functionality: */
void   FDecl rws_misv P_((unsigned n, float* restrict z, const float* scalar, const float* restrict x, int incx));
void   FDecl rws_dvsv P_((unsigned n, float* restrict z, const float* scalar, const float* restrict x, int incx));

#undef P_

#ifdef __cplusplus
}
#endif

#endif


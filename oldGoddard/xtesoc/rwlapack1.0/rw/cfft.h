#ifndef __RWCFFT_H__
#define __RWCFFT_H__

/*
 * Double Precision Complex FFT server
 *
 * $Header: /users/rcs/mathrw/cfft.h,v 1.1 1993/01/23 00:08:32 alv Exp $
 *
 ****************************************************************************
 * 
 * Source code available!  Call or write:
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: cfft.h,v $
 * Revision 1.1  1993/01/23  00:08:32  alv
 * Initial revision
 *
 * 
 *    Rev 1.3   17 Oct 1991 09:12:40   keffer
 * Changed include path to <rw/xxx.h>
 * 
 */

/*
 * This class does complex fourier transforms of a series of a specified
 * length.  It can serve as the nucleus by which to do a wide variety of
 * transforms and, indeed, is inherited by several other servers.  The
 * NCAR FFT routines are used to perform the actual calculations.  This
 * can easily be replaced by specialized hardware or another algorithm.
 * 
 * The DFT calculated is:
 * 
 *                N-1
 *         A(n) = sum X(j) exp(-2 * pi * n * j * I / N);      n=0,...,N-1
 *                j=0
 * 
 * The IDFT calculated is similar:
 * 
 *                N-1
 *         X(j) = sum A(n) exp( 2 * pi * n * j * I / N);      j=0,...,N-1
 *                n=0
 * 
 * where A and X are complex.  Note that the sum is NOT normalized: a
 * call to fourier(), followed by a call to ifourier() will result in
 * the original series multiplied by N.
 * 
 * It precalculates the weights necessary to do the transform.  If it is
 * handed a series of a different length, it will reconfigure, an
 * expensive calculation.  Hence, it is most efficient to call it
 * repeatedly for series of the same length.  Construct several servers
 * to handle a variety of lengths.
 * 
 */

#include "rw/dcomplex.h"	/* Include here to reduce #include nesting depth */
#include "rw/cvec.h"

class DComplexFFTServer {
  unsigned		server_N;
  DComplexVec		the_weights;
  void			fourTrans(DComplex*, unsigned, DComplex*);
  unsigned		radix(unsigned);
  void			split(DComplex*, unsigned, unsigned, DComplex*);
  void			join(DComplex*, unsigned, unsigned, DComplex*);
public:
  DComplexFFTServer();
  DComplexFFTServer(unsigned oforder);
  DComplexFFTServer(const DComplexFFTServer&);

  void			operator=(const DComplexFFTServer&);

  unsigned		order() const {return server_N;}
  void			setOrder(unsigned); // Set new N

  /***********  TRANSFORMS ***********/

  // Returns DFT of a complex sequence:
  DComplexVec		fourier(const DComplexVec&);

  // Returns IDFT of a complex sequence:
  DComplexVec		ifourier(const DComplexVec&);
};

// Other useful functions:
double			spectralVariance(const DComplexVec&);
#endif /* __RWCFFT_H__ */

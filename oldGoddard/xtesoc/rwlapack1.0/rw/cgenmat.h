#ifndef __RWCGENMAT_H__
#define __RWCGENMAT_H__

/*
 * Declarations for DComplex precision matrices
 *
 * Generated from template $Id: matrix.htm,v 1.17 1993/09/17 18:13:14 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010
 *
 * Copyright (C) 1988, 1989, 1990, 1991, 1993.
 * This software is subject to copyright protection under the laws
 * of the United States and other countries.
 *
 ***************************************************************************
 *
 */

/*
 * Defining the preprocessor directive RWBOUNDS_CHECK will
 * cause bounds checking on the subscripting operators
 * and inlined math functions.
 */

#include "rw/dgenmat.h"
#include "rw/dcomplex.h"
#include "rw/matrix.h"
#include "rw/cvec.h"

class DComplexGenMatPick;
class RWRand;
class DComplexSymMat;           // So that the conversion constructors 
class DComplexSkewMat;          // to matrix.h++ classes parse successfully
class DComplexHermMat;
class DComplexHermBandMat;
class DComplexBandMat;
class DComplexSymBandMat;
class DComplexUpperTriMat;
class DComplexLowerTriMat;
class DComplexTriDiagMat;

class DComplexGenMat : public RWMatView {
private:
    DComplexGenMat(const RWDataView&, DComplex*, unsigned,unsigned, int,int); // For internal use
    friend DComplexGenMat toGenMat(const DComplexArray&);
    friend class DComplexArray;                 // For conversion constructor

    /*****************************
     * Routines for internal use *
     *****************************/

    /* These slice functions dispense with bounds checking for speed */
    DComplexVec fastSlice(int starti, int startj, unsigned n, int rowstride, int colstride) const;
    DComplexGenMat fastSlice(int starti, int startj, unsigned m, unsigned n, int rowstr1, int colstr1, int rowstr2, int colstr2) const;

public:

    /****************
     * Constructors *
     ****************/

    DComplexGenMat() : RWMatView()              {}
#ifdef RW_ALLOW_OLD_ARRAY_CONSTRUCTORS
    DComplexGenMat(unsigned m, unsigned n, Storage s=COLUMN_MAJOR) : RWMatView(m,n,sizeof(DComplex),s) {}
#endif
    DComplexGenMat(unsigned m, unsigned n, RWUninitialized, Storage s=COLUMN_MAJOR) : RWMatView(m,n,sizeof(DComplex),s) {}
    DComplexGenMat(unsigned m, unsigned n, RWRand&);
    DComplexGenMat(unsigned m, unsigned n, DComplex val);
    DComplexGenMat(const char *);
    DComplexGenMat(const DComplexGenMat& a) : RWMatView((const RWMatView&)a) {}  // cast needed by Zortech 3.1
    DComplexGenMat(const DComplexGenMatPick& p);
    DComplexGenMat(const DComplex* dat, unsigned,unsigned,Storage=COLUMN_MAJOR); // Copy of dat will be made
    DComplexGenMat(const DComplexVec& vec, unsigned,unsigned,Storage=COLUMN_MAJOR);// View of vec will be taken
    DComplexGenMat(RWBlock *block,    unsigned,unsigned,Storage=COLUMN_MAJOR);// Use a custom block for the data
    DComplexGenMat(const DoubleGenMat& re);     // Conversion from DoubleGenMat
    DComplexGenMat(const DoubleGenMat& re, const DoubleGenMat& im);

    DComplexGenMat(const DComplexSymMat& A);    // Conversion to the matrix
    DComplexGenMat(const DComplexSkewMat& A);   // types in matrix.h++.
    DComplexGenMat(const DComplexHermMat& A);
    DComplexGenMat(const DComplexHermBandMat& A);
    DComplexGenMat(const DComplexBandMat& A);
    DComplexGenMat(const DComplexSymBandMat& A);
    DComplexGenMat(const DComplexUpperTriMat& A);
    DComplexGenMat(const DComplexLowerTriMat& A);
    DComplexGenMat(const DComplexTriDiagMat& A);



    /********************
     * Member functions *
     ********************/

    DComplexGenMat apply(CmathFunTy) const;
    DoubleGenMat apply2(CmathFunTy2) const;
    DComplex&  bcref(int i, int j);             // ref() with bounds checking: for matrix.h++ compatability
    void       bcset(int i, int j, DComplex x); // set() with bounds checking: for matrix.h++ compatability
    DComplex   bcval(int i, int j)   const;     // val() with bounds checking: for matrix.h++ compatability
    unsigned   binaryStoreSize()     const;     // Storage requirements
    const DComplexVec col(int)       const;     // Returns a view of a column
    DComplexVec col(int);                       // Returns a view of a column
//  unsigned   cols()                const      // Number of columns
//  int        colStride()           const      // Step from one column to the next
    DComplexGenMat copy(Storage s=COLUMN_MAJOR)const;//Copy with distinct instance variables
    DComplex*  data()                           {return (DComplex*)begin;}
    const DComplex* data()           const      {return (DComplex*)begin;}
    DComplexGenMat deepCopy(Storage s=COLUMN_MAJOR)const;//Copy with distinct instance variables
    void       deepenShallowCopy(Storage s=COLUMN_MAJOR); // Ensures only 1 reference to data
    const DComplexVec diagonal(int =0) const;   // Returns a view of a diagonal
    DComplexVec diagonal(int =0);               // Returns a view of a diagonal
    DComplexGenMatPick pick(const IntVec&, const IntVec&); // Return the "picked" elements
    const DComplexGenMatPick pick(const IntVec&, const IntVec&) const; // Return the "picked" elements
    void       printOn(ostream& s)   const;     // Pretty print
    DComplex&  ref(int i, int j);               // for matrix.h++ compatability
    DComplexGenMat& reference(const DComplexGenMat& v); // Reference self to v
    void       resize(unsigned,unsigned);       // Will pad with zeroes if necessary
    void       reshape(unsigned,unsigned,Storage s=COLUMN_MAJOR); // Contents after reshape are garbage
    void       restoreFrom(RWFile&);            // Restore from binary
    void       restoreFrom(RWvistream&);        // Restore from virtual stream
    const DComplexVec row(int)       const;     // Returns a view of a row
    DComplexVec row(int);                       // Returns a view of a row
//  unsigned   rows()                const      // Number of rows
//  int        rowStride()           const      // Step from one row to the next
    void       saveOn(RWFile&)       const;     // Store using binary
    void       saveOn(RWvostream&)   const;     // Store to virtual stream
    void       scanFrom(istream& s);            // Read to eof or delimit with []
    void       set(int i, int j, DComplex x);   // for matrix.h++ compatability 
    DComplexVec slice(int starti, int startj, unsigned n, int rowstride, int colstride) const;
    DComplexGenMat slice(int starti, int startj, unsigned m, unsigned n, int rowstr1, int colstr1, int rowstr2, int colstr2) const;
    DComplex   val(int i, int j)     const;     // for matrix.h++ compatability
    void       zero()                           {(*this)=DComplex(0,0);}      // for matrix.h++ compatability


    /********************
     * Member operators *
     ********************/

    DComplex&            operator()(int,int);           
    DComplex             operator()(int,int) const;
    DComplexVec          operator()(int, const RWSlice&);
    const DComplexVec    operator()(int, const RWSlice&)            const;
    DComplexVec          operator()(const RWSlice&, int);
    const DComplexVec    operator()(const RWSlice&, int)            const;
    DComplexGenMat       operator()(const RWSlice&, const RWSlice&);
    const DComplexGenMat operator()(const RWSlice&, const RWSlice&) const;
    DComplexGenMat& operator=(const DComplexGenMat& v); // Must be same size as v
    DComplexGenMat& operator=(const DComplexGenMatPick&);
    DComplexGenMat& operator=(DComplex);
    RWBoolean   operator==(const DComplexGenMat&) const;
    RWBoolean   operator!=(const DComplexGenMat& v) const;
    DComplexGenMat& operator+=(DComplex);
    DComplexGenMat& operator-=(DComplex s)      { return operator+=(-s); }
    DComplexGenMat& operator*=(DComplex);
    DComplexGenMat& operator/=(DComplex);
    DComplexGenMat& operator+=(const DComplexGenMat&);
    DComplexGenMat& operator-=(const DComplexGenMat&);
    DComplexGenMat& operator*=(const DComplexGenMat&);
    DComplexGenMat& operator/=(const DComplexGenMat&);

    friend DoubleGenMat real(const DComplexGenMat&);    // real and imag need access to the
    friend DoubleGenMat imag(const DComplexGenMat&);    // RWBlock, so they must be friends
};
  
    /********************
     * Global Operators *
     ********************/

           DComplexGenMat operator-(const DComplexGenMat&);
    inline DComplexGenMat operator+(const DComplexGenMat& v) { return v; }
           DComplexGenMat operator+(const DComplexGenMat&, const DComplexGenMat&);
           DComplexGenMat operator-(const DComplexGenMat&, const DComplexGenMat&);
           DComplexGenMat operator*(const DComplexGenMat&, const DComplexGenMat&);
           DComplexGenMat operator/(const DComplexGenMat&, const DComplexGenMat&);
// The % operator used for inner products.  These definitions are
// postponed until after product() has been declared
//  inline DComplexGenMat operator%(const DComplexGenMat& A, const DComplexGenMat& B) {return product(A,B);}
//  inline DComplexVec operator%(const DComplexGenMat& A, const DComplexVec& x) {return product(A,x);}
//  inline DComplexVec operator%(const DComplexVec& x, const DComplexGenMat& A) {return product(x,A);}
           DComplexGenMat operator+(const DComplexGenMat&,DComplex);
           DComplexGenMat operator+(DComplex, const DComplexGenMat&);
           DComplexGenMat operator-(const DComplexGenMat&,DComplex);
           DComplexGenMat operator-(DComplex, const DComplexGenMat&);
           DComplexGenMat operator*(const DComplexGenMat&,DComplex);
           DComplexGenMat operator*(DComplex, const DComplexGenMat&);
           DComplexGenMat operator/(const DComplexGenMat&,DComplex);
           DComplexGenMat operator/(DComplex, const DComplexGenMat&);
           ostream&    operator<<(ostream& s, const DComplexGenMat& v);
           istream&    operator>>(istream& s, DComplexGenMat& v);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
    inline DComplexGenMat operator*(DComplex s, const DComplexGenMat& V) {return V*s;}
    inline DComplexGenMat operator+(DComplex s, const DComplexGenMat& V) {return V+s;}
    inline DComplexGenMat operator-(const DComplexGenMat& V, DComplex s) {return V+(DComplex)(-s);}// cast makes microsoft happy when compiling sarr.cpp
#endif

    /********************
     * Global Functions *
     ********************/

    /**** Miscellaneous Functions ****/

           DoubleGenMat abs(const DComplexGenMat& V);
           DComplex    dot(const DComplexGenMat&,const DComplexGenMat&); // Return sum_ij (Aij*Bij)
           DComplex    prod(const DComplexGenMat&);
           DComplexVec product(const DComplexGenMat&, const DComplexVec&);
           DComplexVec product(const DComplexVec&, const DComplexGenMat&);
           DComplexGenMat product(const DComplexGenMat&, const DComplexGenMat&);
           DComplex    sum(const DComplexGenMat&);
           DComplexGenMat transpose(const DComplexGenMat&); // Transpose references this's data
           DComplexGenMat transposeProduct(const DComplexGenMat&,const DComplexGenMat&); // For matrix.h++ compatability
           DComplexGenMat conjTransposeProduct(const DComplexGenMat&,const DComplexGenMat&); // For matrix.h++ compatibility



    /**** Functions for both real and complex ****/

           DComplexGenMat cos(const DComplexGenMat& V);
           DComplexGenMat cosh(const DComplexGenMat& V);
           DComplexGenMat exp(const DComplexGenMat& V);
           DComplex    mean(const DComplexGenMat& V);
           DComplexGenMat pow(const DComplexGenMat&,const DComplexGenMat&);
           DComplexGenMat sin(const DComplexGenMat& V);
           DComplexGenMat sinh(const DComplexGenMat& V);
           DComplexGenMat sqrt(const DComplexGenMat& V);
           double      variance(const DComplexGenMat&);


    /**** Functions for complex matrices ****/

           DoubleGenMat arg(const DComplexGenMat& V);
           DComplexGenMat conj(const DComplexGenMat& V);
           DoubleGenMat imag(const DComplexGenMat&); // Returns reference to data
           DoubleGenMat norm(const DComplexGenMat& V);
           DoubleGenMat real(const DComplexGenMat&); // Returns reference to data
    inline DComplexGenMat adjoint(const DComplexGenMat& A) {return conj(transpose(A));}


    /**** Functions for type conversion ****/

                                                  // No type conversion functions for DComplexGenMat

    /**** Norm functions ****/
           Double      l1Norm(const DComplexGenMat&); // Largest column sum
           Double      linfNorm(const DComplexGenMat&); // Largest row sum
           Double      frobNorm(const DComplexGenMat&); // Root of sum of squares
           Double      maxNorm(const DComplexGenMat&); // Largest absolute value, not really a norm

    /***************************
     * Inline Access Functions *
     ***************************/


  inline DComplex& DComplexGenMat::operator()(int i, int j) {
#  ifdef RWBOUNDS_CHECK    
    boundsCheck(i,j);
#  endif
    return data()[i*rowstep+j*colstep];
  }

  inline DComplex DComplexGenMat::operator()(int i, int j) const {
#  ifdef RWBOUNDS_CHECK    
    boundsCheck(i,j);
#  endif
    return data()[i*rowstep+j*colstep];
  }

  inline DComplexVec DComplexGenMat::operator()(int i, const RWSlice& j) {
#  ifdef RWBOUNDS_CHECK
    rowBoundsCheck(i);
    j.boundsCheck(ncols);
#  endif
    return fastSlice(i,j.begin(),j.len(ncols),0,j.stride());
}

  inline const DComplexVec DComplexGenMat::operator()(int i, const RWSlice& j) const {
#  ifdef RWBOUNDS_CHECK
    rowBoundsCheck(i);
    j.boundsCheck(ncols);
#  endif
    return fastSlice(i,j.begin(),j.len(ncols),0,j.stride());
}

  inline DComplexVec DComplexGenMat::operator()(const RWSlice& i, int j) {
#  ifdef RWBOUNDS_CHECK
    colBoundsCheck(j);
    i.boundsCheck(nrows);
#  endif
    return fastSlice(i.begin(),j,i.len(nrows),i.stride(),0);
}

  inline const DComplexVec DComplexGenMat::operator()(const RWSlice& i, int j) const {
#  ifdef RWBOUNDS_CHECK
    colBoundsCheck(j);
    i.boundsCheck(nrows);
#  endif
    return fastSlice(i.begin(),j,i.len(nrows),i.stride(),0);
}

  inline DComplexGenMat DComplexGenMat::operator()(const RWSlice& i, const RWSlice& j) {
#  ifdef RWBOUNDS_CHECK
    i.boundsCheck(nrows);
    j.boundsCheck(ncols);
#  endif
    return fastSlice(i.begin(),j.begin(),i.len(nrows),j.len(ncols),i.stride(),0,0,j.stride());
}

  inline const DComplexGenMat DComplexGenMat::operator()(const RWSlice& i, const RWSlice& j) const {
#  ifdef RWBOUNDS_CHECK
    i.boundsCheck(nrows);
    j.boundsCheck(ncols);
#  endif
    return fastSlice(i.begin(),j.begin(),i.len(nrows),j.len(ncols),i.stride(),0,0,j.stride());
}


// These next functions predate the nice subscripting mechanisms 
// available with the current math.h++

inline const DComplexVec DComplexGenMat::col(int j) const { return (*this)(RWAll,j); }
inline       DComplexVec DComplexGenMat::col(int j)       { return (*this)(RWAll,j); }
inline const DComplexVec DComplexGenMat::row(int i) const { return (*this)(i,RWAll); }
inline       DComplexVec DComplexGenMat::row(int i)       { return (*this)(i,RWAll); }


// The following functions are for compatability with the other matrix
// types provided by matrix.h++

  inline DComplex& DComplexGenMat::ref(int i, int j) {
    return (*this)(i,j);
  }

  inline void DComplexGenMat::set(int i, int j, DComplex x) {
    (*this)(i,j)=x;
  }

  inline DComplex DComplexGenMat::val(int i, int j) const {
    return (*this)(i,j);
  }

  inline DComplex& DComplexGenMat::bcref(int i, int j) {
    boundsCheck(i,j);
    return data()[i*rowstep+j*colstep];
  }

  inline void DComplexGenMat::bcset(int i, int j, DComplex x) {
    boundsCheck(i,j);
    data()[i*rowstep+j*colstep]=x;
  }

  inline DComplex DComplexGenMat::bcval(int i, int j) const {
    boundsCheck(i,j);
    return data()[i*rowstep+j*colstep];
  }


// Here are the definitions of operator% for inner products.  We
// had to postpone them because in the operator section product()
// was not yet declared.
inline DComplexGenMat         operator%(const DComplexGenMat& A, const DComplexGenMat& B) {return product(A,B);}
inline DComplexVec      operator%(const DComplexGenMat& A, const DComplexVec& x) {return product(A,x);}
inline DComplexVec      operator%(const DComplexVec& x, const DComplexGenMat& A) {return product(x,A);}

#endif /* __RWCGENMAT_H__ */

#ifndef __RWRANDBINO_H__
#define __RWRANDBINO_H__

/*
 * Declarations for RandBinomial: Binomially distributed random numbers.
 *
 * $Header: /users/rcs/mathrw/randbino.h,v 1.1 1993/01/23 00:08:39 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * Copyright (C) 1988, 1989, 1990, 1991.  This software is subject to copyright
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: randbino.h,v $
 * Revision 1.1  1993/01/23  00:08:39  alv
 * Initial revision
 *
 * 
 *    Rev 1.3   17 Oct 1991 09:12:52   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.1   24 Jul 1991 13:01:06   keffer
 * Added pvcs keywords
 *
 */

#include "rw/randunif.h"

class RandBinomial : public RandUniform {
private:		// Private data used in the algorithms.
  double 		prb, g, am, pc, plog, pclog, sq;
protected:
  unsigned 		tries;
  double   		prob;
public:
  RandBinomial(unsigned Ntries, double probability);            
  RandBinomial(unsigned n, unsigned Ntries, double probability);            
 
  unsigned  		trials()  const		{return tries;}
  double    		probability() const	{return prob;}
  void      		setTrials(unsigned Ntries);
  void      		setProbability(double probability);

  double    		randValue();			// Return a random number.
  void     		randValue(double*, unsigned n);	// Return an array of random numbers.
  DoubleVec 		randValue(unsigned n);		// Return a vector of n random numbers.
  DoubleGenMat 		randValue(unsigned nr, unsigned nc); // Return a matrix of random numbers

};

#endif /*__RWRANDBINO_H__*/

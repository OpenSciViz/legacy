#ifndef __RWLSQFIT_H__
#define __RWLSQFIT_H__

/*
 * Declarations for class LeastSqFit: linear least squares fit.
 *
 * $Header: /users/rcs/mathrw/lsqfit.h,v 1.1 1993/01/23 00:08:36 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * Copyright (C) 1988, 1989, 1990, 1991.  This software is subject to copyright
 * protection under the laws of the United States and other countries.
 * 
 ***************************************************************************
 *
 * $Log: lsqfit.h,v $
 * Revision 1.1  1993/01/23  00:08:36  alv
 * Initial revision
 *
 * 
 *    Rev 1.3   17 Oct 1991 09:12:50   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.1   24 Jul 1991 13:01:00   keffer
 * Added pvcs keywords
 *
 */

#include "rw/dvec.h"

class LeastSqFit {
private:
   unsigned 		npts;
protected:
   double 		LSq_slope;
   double 		LSq_intercept;
   double 		SD_slope;
   double 		SD_intercept;
   double 		correlation_coeff;
public:
   // construct least squares fit without weights:
   LeastSqFit(const DoubleVec& x, const DoubleVec& y);
   // construct least squares fit with weights:
   LeastSqFit(const DoubleVec& x, const DoubleVec& y, const DoubleVec& sigmay);   

   double 		correlationCoeff() 	const	{return correlation_coeff;}
   double 		intercept()        	const	{return LSq_intercept;}
   double 		interceptStandardDev()  const	{return SD_intercept;}
   double 		slope()	 		const	{return LSq_slope;}
   double 		slopeStandardDev() 	const	{return SD_slope;}
   double 		xPosition(double y)  	const	{return (y-LSq_intercept)/LSq_slope;}
   double 		yPosition(double x) 	const	{return x*LSq_slope+LSq_intercept;} 
   unsigned		binaryStoreSize() 		const;	// Storage requirements.
  
friend ostream&		operator<<(ostream&, const LeastSqFit&);  // prints a summary
};

#endif /*__RWLSQFIT_H__*/

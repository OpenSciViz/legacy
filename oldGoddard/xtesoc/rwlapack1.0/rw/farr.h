#ifndef __RWFARR_H__
#define __RWFARR_H__

/*
 * Declarations for Float precision arrays
 *
 * Generated from template $Id: array.htm,v 1.13 1993/09/19 15:18:58 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311        FAX: (503) 757-7350
 *
 * Copyright (C) 1988, 1989, 1990, 1991.  This software is subject to copyright
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * Right now, functions are only provided for easy indexing up to 4D.
 * Beyond this, you need to use the general functions (ie with IntVecs).
 * It would be easy to add 5D or higher functions if need be, except that
 * the op() functions would go through a combinatorial explosion.  As 
 * long as you are content to always get back an Array, and not explicitly
 * a Vec or GenMat as the case may be, then this explosion can be avoided.
 */

/*
 * Defining the preprocessor directive RWBOUNDS_CHECK will
 * cause bounds checking on the slice function, subscripting operators
 * and inlined math functions.
 *
 * Bounds checking is always done on subscripting operators returning
 * anything other than a float or float&.
 */

#include "rw/igenmat.h"
#include "rw/array.h"

class RWRand;

class FloatArray : public RWArrayView {
private:
    FloatArray(const RWDataView&, float*, const IntVec&, const IntVec&); // For real() and imag() and slices

public:

    /****************
     * Constructors *
     ****************/

    FloatArray();                               // Declares a scalar (0D array)
    FloatArray(const IntVec&, RWUninitialized, Storage=COLUMN_MAJOR);
    FloatArray(unsigned,unsigned,unsigned,RWUninitialized);
    FloatArray(unsigned,unsigned,unsigned,unsigned,RWUninitialized);
    FloatArray(const IntVec&, RWRand&, Storage=COLUMN_MAJOR);
    FloatArray(unsigned,unsigned,unsigned,RWRand&);
    FloatArray(unsigned,unsigned,unsigned,unsigned,RWRand&);
    FloatArray(const IntVec& n, float val);
    FloatArray(unsigned,unsigned,unsigned, float val);
    FloatArray(unsigned,unsigned,unsigned,unsigned, float val);
    FloatArray(const char *);
    FloatArray(const FloatArray& a);
    FloatArray(const float* dat, const IntVec& n); // Copy of dat will be made
    FloatArray(const float* dat, unsigned,unsigned,unsigned);
    FloatArray(const float* dat, unsigned,unsigned,unsigned,unsigned);
    FloatArray(const FloatVec& vec, const IntVec& n); // View of dat will be taken
    FloatArray(const FloatVec& vec, unsigned,unsigned,unsigned);
    FloatArray(const FloatVec& vec, unsigned,unsigned,unsigned,unsigned);
    FloatArray(RWBlock *block, const IntVec& n); // Use a custom block for the data
    FloatArray(RWBlock *block, unsigned,unsigned,unsigned);
    FloatArray(RWBlock *block, unsigned,unsigned,unsigned,unsigned);
    FloatArray(const FloatVec&);                // View will be taken
    FloatArray(const FloatGenMat&);             // View will be taken

    operator DoubleArray() const;               // Conversion to DoubleArray

    /********************
     * Member functions *
     ********************/

    FloatArray    apply(mathFunTy)      const;
#ifdef RW_NATIVE_EXTENDED
    FloatArray    apply(XmathFunTy)     const;
#endif  
    unsigned      binaryStoreSize()     const;   // Storage requirements
    FloatArray    copy()                const;   // Copy with distinct instance variables
    float*        data()                         {return (float*)begin;}
    const float*  data()                const    {return (float*)begin;}
    FloatArray    deepCopy()            const;   // Copy with distinct instance variables 
    void          deepenShallowCopy();           // Insures only 1 ref and data is compact
//  unsigned      dimension()           const    {return npts.length();}
//  const IntVec& length()              const    {return npts;}
//  int           length(int i)         const    {return npts(i);}
    void          printOn(ostream& s)   const;   // Pretty print
    FloatArray&   reference(const FloatArray& v); // Reference self to v
    void          resize(const IntVec&);         // Will pad with zeroes if necessary
    void          resize(unsigned,unsigned,unsigned);
    void          resize(unsigned,unsigned,unsigned,unsigned);
    void          reshape(const IntVec&);        // Contents after reshape are garbage
    void          reshape(unsigned,unsigned,unsigned);
    void          reshape(unsigned,unsigned,unsigned,unsigned);
    void          restoreFrom(RWFile&);          // Restore from binary
    void          restoreFrom(RWvistream&);      // Restore from virtual stream
    void          saveOn(RWFile&)       const;   // Store using binary
    void          saveOn(RWvostream&)   const;   // Store to virtual stream
    void          scanFrom(istream& s);          // Read to eof or delimit with []
    FloatArray    slice(const IntVec& start, const IntVec& lgt, const IntGenMat& strider) const;
//  const IntVec& stride()              const    {return step;}
//  int           stride(int i)         const    {return step(i);}

    /********************
     * Member operators *
     ********************/

    float&          operator[](const IntVec& i); // With bounds checking
    float           operator[](const IntVec& i) const;
    float&          operator()(const IntVec& i); // With optional bounds checking
    float           operator()(const IntVec& i) const;
    float&          operator()(int,int,int);
    float           operator()(int,int,int) const;
    float&          operator()(int,int,int,int);
    float           operator()(int,int,int,int) const;
    FloatVec        operator()(const RWSlice&,int,int);
    const FloatVec  operator()(const RWSlice&,int,int) const;
    FloatVec        operator()(int,const RWSlice&,int);
    const FloatVec  operator()(int,const RWSlice&,int) const;
    FloatVec        operator()(int,int,const RWSlice&);
    const FloatVec  operator()(int,int,const RWSlice&) const;
    FloatVec        operator()(const RWSlice&,int,int,int);
    const FloatVec  operator()(const RWSlice&,int,int,int) const;
    FloatVec        operator()(int,const RWSlice&,int,int);
    const FloatVec  operator()(int,const RWSlice&,int,int) const;
    FloatVec        operator()(int,int,const RWSlice&,int);
    const FloatVec  operator()(int,int,const RWSlice&,int) const;
    FloatVec        operator()(int,int,int,const RWSlice&);
    const FloatVec  operator()(int,int,int,const RWSlice&) const;
    FloatGenMat     operator()(int,const RWSlice&,const RWSlice&);
    const FloatGenMat operator()(int,const RWSlice&,const RWSlice&) const;
    FloatGenMat     operator()(const RWSlice&,int,const RWSlice&);
    const FloatGenMat operator()(const RWSlice&,int,const RWSlice&) const;
    FloatGenMat     operator()(const RWSlice&,const RWSlice&,int);
    const FloatGenMat operator()(const RWSlice&,const RWSlice&,int) const;
    FloatGenMat     operator()(int,int,const RWSlice&,const RWSlice&);
    const FloatGenMat operator()(int,int,const RWSlice&,const RWSlice&) const;
    FloatGenMat     operator()(int,const RWSlice&,int,const RWSlice&);
    const FloatGenMat operator()(int,const RWSlice&,int,const RWSlice&) const;
    FloatGenMat     operator()(int,const RWSlice&,const RWSlice&,int);
    const FloatGenMat operator()(int,const RWSlice&,const RWSlice&,int) const;
    FloatGenMat     operator()(const RWSlice&,int,int,const RWSlice&);
    const FloatGenMat operator()(const RWSlice&,int,int,const RWSlice&) const;
    FloatGenMat     operator()(const RWSlice&,int,const RWSlice&,int);
    const FloatGenMat operator()(const RWSlice&,int,const RWSlice&,int) const;
    FloatGenMat     operator()(const RWSlice&,const RWSlice&,int,int);
    const FloatGenMat operator()(const RWSlice&,const RWSlice&,int,int) const;
    FloatArray      operator()(const RWSlice&,const RWSlice&,const RWSlice&);
    const FloatArray operator()(const RWSlice&,const RWSlice&,const RWSlice&) const;
    FloatArray      operator()(int,const RWSlice&,const RWSlice&,const RWSlice&);
    const FloatArray operator()(int,const RWSlice&,const RWSlice&,const RWSlice&) const;
    FloatArray      operator()(const RWSlice&,int,const RWSlice&,const RWSlice&);
    const FloatArray operator()(const RWSlice&,int,const RWSlice&,const RWSlice&) const;
    FloatArray      operator()(const RWSlice&,const RWSlice&,int,const RWSlice&);
    const FloatArray operator()(const RWSlice&,const RWSlice&,int,const RWSlice&) const;
    FloatArray      operator()(const RWSlice&,const RWSlice&,const RWSlice&,int);
    const FloatArray operator()(const RWSlice&,const RWSlice&,const RWSlice&,int) const;
    FloatArray      operator()(const RWSlice&,const RWSlice&,const RWSlice&,const RWSlice&);
    const FloatArray operator()(const RWSlice&,const RWSlice&,const RWSlice&,const RWSlice&) const;
    FloatArray& operator=(const FloatArray& v); // Must be same length as v
    FloatArray& operator=(float);
    RWBoolean   operator==(const FloatArray&) const;
    RWBoolean   operator!=(const FloatArray& v) const;
    FloatArray& operator+=(float);
    FloatArray& operator-=(float s)             { return operator+=(-s); }
    FloatArray& operator*=(float);
    FloatArray& operator/=(float s)             { return operator*=(1.0/s); }
    FloatArray& operator+=(const FloatArray&);
    FloatArray& operator-=(const FloatArray&);
    FloatArray& operator*=(const FloatArray&);
    FloatArray& operator/=(const FloatArray&);
    FloatArray& operator++();                   // Prefix operator
    FloatArray& operator--();                   // Prefix operator
#if             !defined(RW_NO_POSTFIX)
    void        operator++(int)                 { ++(*this); }  // Postfix operator
    void        operator--(int)                 { --(*this); }  // Postfix operator
#endif

    friend float       toScalar(const FloatArray&);    // For 0-D arrays
    friend FloatVec    toVec(const FloatArray&);       // For 1-D arrays
    friend FloatGenMat toGenMat(const FloatArray&);    // For 2-D arrays
};
  
    /********************
     * Global Operators *
     ********************/

           FloatArray  operator-(const FloatArray&);
    inline FloatArray  operator+(const FloatArray& v) { return v; }
           FloatArray  operator+(const FloatArray&, const FloatArray&);
           FloatArray  operator-(const FloatArray&, const FloatArray&);
           FloatArray  operator*(const FloatArray&, const FloatArray&);
           FloatArray  operator/(const FloatArray&, const FloatArray&);
           FloatArray  operator+(const FloatArray&,float);
           FloatArray  operator+(float, const FloatArray&);
           FloatArray  operator-(const FloatArray&,float);
           FloatArray  operator-(float, const FloatArray&);
           FloatArray  operator*(const FloatArray&,float);
           FloatArray  operator*(float, const FloatArray&);
           FloatArray  operator/(const FloatArray&,float);
           FloatArray  operator/(float, const FloatArray&);
           ostream&    operator<<(ostream& s, const FloatArray& v);
           istream&    operator>>(istream& s, FloatArray& v);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
    inline FloatArray  operator*(float s, const FloatArray& V) {return V*s;}
    inline FloatArray  operator+(float s, const FloatArray& V) {return V+s;}
    inline FloatArray  operator-(const FloatArray& V, float s) {return V+(float)(-s);}
    inline FloatArray  operator/(const FloatArray& V, float s) {return V*(1/s);}
#endif


    /********************
     * Global Functions *
     ********************/

    /**** Miscellaneous Functions ****/

           FloatArray  abs(const FloatArray& V);
           float       dot(const FloatArray&,const FloatArray&); // Return sum_ij...k (Aij...k * Bij...k)
           FloatArray  dot(const FloatVec&,const FloatArray&); // Return sum_i (Vi * Aij...k)
           FloatArray  dot(const FloatArray&,const FloatVec&); // Return sum_k (Ai...jk * Vk)
           IntVec      maxIndex(const FloatArray&); // Returns *index* of max value
           float       maxValue(const FloatArray&);
           IntVec      minIndex(const FloatArray&); // Returns *index* of min value
           float       minValue(const FloatArray&);
           float       prod(const FloatArray&);
           float       sum(const FloatArray&);


    /**** Functions for both real and complex ****/

           FloatArray  cos(const FloatArray& V);
           FloatArray  cosh(const FloatArray& V);
           FloatArray  exp(const FloatArray& V);
           float       mean(const FloatArray& V);
           FloatArray  pow(const FloatArray&,const FloatArray&);
           FloatArray  sin(const FloatArray& V);
           FloatArray  sinh(const FloatArray& V);
           FloatArray  sqrt(const FloatArray& V);
           float       variance(const FloatArray&);


    /**** Functions for real arrays ****/

           FloatArray  acos(const FloatArray& V);
           FloatArray  asin(const FloatArray& V);
           FloatArray  atan(const FloatArray& V);
           FloatArray  atan2(const FloatArray&,const FloatArray&);
           FloatArray  ceil(const FloatArray& V);
           FloatArray  floor(const FloatArray& V);
           FloatArray  log(const FloatArray& V);
           FloatArray  log10(const FloatArray& V);
           FloatArray  tan(const FloatArray& V);
           FloatArray  tanh(const FloatArray& V);


    /**** Functions for type conversion ****/

           FloatArray  toFloat(const DoubleArray&);

    /**** Norm functions ****/
           Float       frobNorm(const FloatArray&); // Root of sum of squares
           Float       maxNorm(const FloatArray&); // Largest absolute value, not really a norm

    /***************************
     * Inline Access Functions *
     ***************************/


  inline float& FloatArray::operator[](const IntVec& i) {
    boundsCheck(i);
    return data()[dot(i,step)];
  }

  inline float FloatArray::operator[](const IntVec& i) const {
    boundsCheck(i);
    return data()[dot(i,step)];
  }

  inline float& FloatArray::operator()(const IntVec& i) {
#  ifdef RWBOUNDS_CHECK    
    boundsCheck(i);
#  endif
    return data()[dot(i,step)];
  }

  inline float FloatArray::operator()(const IntVec& i) const {
#  ifdef RWBOUNDS_CHECK    
    boundsCheck(i);
#  endif
    return data()[dot(i,step)];
  }

  inline float& FloatArray::operator()(int i, int j, int k) {
#  ifdef RWBOUNDS_CHECK
    boundsCheck(i,j,k);
#  endif
    return data()[i*step((int)0)+j*step(1)+k*step(2)];
  }

  inline float FloatArray::operator()(int i, int j, int k) const {
#  ifdef RWBOUNDS_CHECK
    boundsCheck(i,j,k);
#  endif
    return data()[i*step((int)0)+j*step(1)+k*step(2)];
  }

  inline float& FloatArray::operator()(int i, int j, int k, int l) {
#  ifdef RWBOUNDS_CHECK
    boundsCheck(i,j,k,l);
#  endif
    return data()[i*step((int)0)+j*step(1)+k*step(2)+l*step(3)];
  }

  inline float FloatArray::operator()(int i, int j, int k, int l) const {
#  ifdef RWBOUNDS_CHECK
    boundsCheck(i,j,k,l);
#  endif
    return data()[i*step((int)0)+j*step(1)+k*step(2)+l*step(3)];
  }

#endif /*__RWFARR_H__*/

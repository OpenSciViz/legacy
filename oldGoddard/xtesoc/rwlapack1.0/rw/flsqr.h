#ifndef __RWFLSQR_H__
#define __RWFLSQR_H__
/*
 * FloatLeastSqQR:  solve least square QR problem with orthogonal decomposition
 *
 * Generated from template $Id: xlsqr.h,v 1.3 1993/07/05 19:19:33 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 * Uses a complete orthogonal decomposition to solve the linear
 * least squares problem.  The solution is the minimum norm solution
 * which minimizes the residual.
 *
 * When building a factorization using either a constructor or one
 * of the factor functions, you can supply a tolerance parameter.
 * Entries along the diagonal of the R factor of the QR decomposition
 * less than this tolerance are treated as zero.  Using this can
 * prevent insignificant entries of R from corrupting your solution.
 *
 * The factor functions and constructors taking a QR decomposition
 * are useful if you'd like to examine the diagonal entries of R
 * for yourself and decide what tolerance to use.
 */

#include "rw/fco.h"

class FloatLeastSqQR : public FloatCODecomp {
public:
  FloatLeastSqQR();
  FloatLeastSqQR(const FloatGenMat& A, double tol=0);
  FloatLeastSqQR(const FloatQRDecomp& A, double tol=0);
  FloatLeastSqQR(const FloatCODecomp& A);
//void     factor(const FloatGenMat& A, double tol=0);
//void     factor(const FloatQRDecomp& A, double tol=0);
//void     factor(const FloatCODecomp& A);
//unsigned rows() const {return decomp_.rows();}
//unsigned cols() const {return decomp_.cols();}
//unsigned rank() const {return decomp_.rank();}
  FloatVec   residual(const FloatVec& data) const;
  Float      residualNorm(const FloatVec& data) const;
  FloatVec   solve(const FloatVec& data) const;
};

inline FloatVec solve(   const FloatLeastSqQR& A, const FloatVec& b) {return A.solve(b);}
inline FloatVec residual(const FloatLeastSqQR& A, const FloatVec& b) {return A.residual(b);}
inline Float    residualNorm(const FloatLeastSqQR& A, const FloatVec& b) {return A.residualNorm(b);}

#endif

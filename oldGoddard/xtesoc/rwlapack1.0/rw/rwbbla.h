#ifndef __RWBBLA_H__
#define __RWBBLA_H__

/*
 * UChar precision Basic Linear Algebra prototypes.
 *
 * $Header: /users/rcs/mathrw/rwbbla.h,v 1.1 1993/01/23 00:08:42 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * Copyright (C) 1991.  This software is subject to copyright
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: rwbbla.h,v $
 * Revision 1.1  1993/01/23  00:08:42  alv
 * Initial revision
 *
 * 
 *    Rev 1.2   17 Oct 1991 09:12:50   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.0   27 Sep 1991 11:49:48   keffer
 * Initial revision.
 */

#ifndef __MATHDEFS_H__
#  include "rw/mathdefs.h"		/* Looking for "FDecl" and "RW_KR_ONLY" */
#endif

#ifdef __cplusplus
extern "C" {
# define P_(s) s
#else
#  ifdef RW_KR_ONLY
#    define P_(s) ()
#  else
#    define P_(s) s
#  endif
#endif

void   FDecl rwbcopy P_((unsigned n, UChar* restrict x, int incx, const UChar* restrict y, int incy));
void   FDecl rwbset  P_((unsigned n, UChar* restrict x, int incx, const UChar* restrict scalar));
int    FDecl rwbsame P_((unsigned n, const UChar* restrict x, int incx, const UChar* restrict y, int incy));
int    FDecl rwbmax P_((unsigned n, const UChar* restrict x, int incx));
int    FDecl rwbmin P_((unsigned n, const UChar* restrict x, int incx));

void   FDecl rwbdot P_((unsigned n, const UChar* restrict x, int incx, const UChar* restrict y, int incy, UChar* result));
void   FDecl rwbcdot P_((unsigned n, const UChar* restrict x, int incx, const UChar* restrict y, int incy, UChar* result));

/* For implementing "Vector& Vector::operator+=(const Vector&)" functionality: */
void   FDecl rwb_aplvv P_((unsigned n, UChar* restrict x, int incx, const UChar* restrict y, int incy));
void   FDecl rwb_amivv P_((unsigned n, UChar* restrict x, int incx, const UChar* restrict y, int incy));
void   FDecl rwb_amuvv P_((unsigned n, UChar* restrict x, int incx, const UChar* restrict y, int incy));
void   FDecl rwb_advvv P_((unsigned n, UChar* restrict x, int incx, const UChar* restrict y, int incy));

/* For implementing "Vector& Vector::operator+=(type)" functionality: */
void   FDecl rwb_aplvs P_((unsigned n, UChar* restrict x, int incx, const UChar* restrict scalar));
void   FDecl rwb_amuvs P_((unsigned n, UChar* restrict x, int incx, const UChar* restrict scalar));
void   FDecl rwb_advvs P_((unsigned n, UChar* restrict x, int incx, const UChar* restrict scalar));

/* For implementing "Vector operator+(const Vector&, const Vector&)" functionality: */
void   FDecl rwb_plvv P_((unsigned n, UChar* restrict z, const UChar* restrict x, int incx, const UChar* restrict y, int incy));
void   FDecl rwb_mivv P_((unsigned n, UChar* restrict z, const UChar* restrict x, int incx, const UChar* restrict y, int incy));
void   FDecl rwb_muvv P_((unsigned n, UChar* restrict z, const UChar* restrict x, int incx, const UChar* restrict y, int incy));
void   FDecl rwb_dvvv P_((unsigned n, UChar* restrict z, const UChar* restrict x, int incx, const UChar* restrict y, int incy));

/* For implementing "Vector operator+(const Vector&, type)" functionality: */
void   FDecl rwb_plvs P_((unsigned n, UChar* restrict z, const UChar* restrict x, int incx, const UChar* restrict scalar));
void   FDecl rwb_muvs P_((unsigned n, UChar* restrict z, const UChar* restrict x, int incx, const UChar* restrict scalar));
void   FDecl rwb_dvvs P_((unsigned n, UChar* restrict z, const UChar* restrict x, int incx, const UChar* restrict scalar));

/* For implementing "Vector operator+(type, const Vector&)" functionality: */
void   FDecl rwb_misv P_((unsigned n, UChar* restrict z, const UChar* scalar, const UChar* restrict x, int incx));
void   FDecl rwb_dvsv P_((unsigned n, UChar* restrict z, const UChar* scalar, const UChar* restrict x, int incx));

#undef P_

#ifdef __cplusplus
}
#endif

#endif


#ifndef __RWSCVECPIK_H__
#define __RWSCVECPIK_H__

/*
 * Declarations for SCharVecPick --- picks elements out of a SCharVec
 *
 * Generated from template $Id: vecpik.htm,v 1.2 1993/03/22 15:51:31 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * Copyright (C) 1988, 1989, 1990, 1991.  This software is subject to copyright
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/ivec.h"

/*
 * The SCharVecPick class allows selected elements to be addressed.
 * There are no public constructors.
 */

class SCharVecPick {
private:
  SCharVec		V;
  const IntVec		pick;
  SCharVecPick(SCharVec& v, const IntVec& x);	// Constructor is private
  friend		SCharVec;
protected:
  void			assertElements() const;
  void			lengthCheck(unsigned) const;
public:
  void			operator=(const SCharVec&);
  void			operator=(const SCharVecPick&);
  void			operator=(SChar);

  inline SChar&	operator()(int i);
  inline SChar		operator()(int i) const;
  unsigned		length() const	{ return pick.length(); }
};

/************************************************
 *						*
 *		I N L I N E S			*
 *						*
 ************************************************/

inline
SCharVecPick::SCharVecPick(SCharVec& v, const IntVec& x) :
  V(v), pick(x)
{
#ifdef RWBOUNDS_CHECK
  assertElements();
#endif
}

inline SChar& SCharVecPick::operator()(int i)       { return V( pick(i) ); }
inline SChar  SCharVecPick::operator()(int i) const { return V( pick(i) ); }

#endif /*__SCVECPIK_H__*/

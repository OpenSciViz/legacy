#ifndef __RWDHESS_H__
#define __RWDHESS_H__
/*
 * DoubleHessenbergDecomp - Hessenberg decomposition of a matrix
 *
 * Generated from template $Id: xhess.h,v 1.1 1993/05/18 17:00:02 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 * This decomposition is usually formed as a percusor to computing
 * the eigenvalues and/or Schur decomposition of a non-symmetric matrix.
 *
 * This class includes an optional balancing transformation in addition
 * to the standard Hessenberg decomposition.
 */ 

#include "rw/lapkdefs.h"
#include "rw/dvec.h"
#include "rw/dgenmat.h"
#include "rw/dbal.h"
#include "rw/lapack.h"

class DoubleHessenbergDecomp {
private:
  DoubleBalanceTransform B_;  // Balancing transformation
  DoubleGenMat           H_;  // Hessenberg matrix and Q info
  DoubleVec              tau_;// scalar reflectors for generating Q

public:
  DoubleHessenbergDecomp();
  DoubleHessenbergDecomp(const DoubleGenMat&, RWBoolean permute=TRUE, RWBoolean scale=TRUE);
  DoubleHessenbergDecomp(const DoubleBalanceDecomp&);
  void factor(const DoubleGenMat&, RWBoolean permute=TRUE, RWBoolean scale=TRUE);
  void factor(const DoubleBalanceDecomp&);
  unsigned            cols()                  const {return H_.cols();}
  unsigned            rows()                  const {return H_.rows();}
  DoubleGenMat           B()                     const;
  DoubleBalanceTransform balanceTransform()      const {return B_;}  // An alternate representation of B
  DoubleGenMat           Q()                     const;
  DoubleGenMat           H()                     const;
  DoubleVec              Bx(const DoubleVec&)       const;
  DoubleVec              BInvTx(const DoubleVec&)   const;
  DoubleVec              Qx(const DoubleVec&)       const;
  DoubleVec              QTx(const DoubleVec&)      const;
  DoubleGenMat           BX(const DoubleGenMat&)    const;
  DoubleGenMat           BInvTX(const DoubleGenMat&)const;
  DoubleGenMat           QX(const DoubleGenMat&)    const;
  DoubleGenMat           QTX(const DoubleGenMat&)   const;
};

#endif

#ifndef __RWMATRIX_H
#define __RWMATRIX_H
/* 
 * Base class for general matrix views.
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311        FAX: (503) 757-7350
 *
 * Copyright (C) 1988, 1989, 1990, 1991, 1992.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 */

#include "rw/dataview.h"

class RWMatView : public RWDataView {
protected:
  unsigned   nrows,ncols;                     // Size of the matrix
  int        rowstep,colstep;                 // Stride length of the matrix

  RWMatView();
  RWMatView(unsigned m, unsigned n, size_t s, Storage=COLUMN_MAJOR);
  RWMatView(RWBlock *block, unsigned m, unsigned n, Storage=COLUMN_MAJOR);
  RWMatView(const RWMatView&);
  RWMatView(const RWDataView& v, void *b, unsigned m, unsigned n, int r, int s)
    : RWDataView(v,b) {nrows=m; ncols=n; rowstep=r; colstep=s;}

  void      reference(const RWMatView&);

public:  // These functions need to be public so that they can be called by global functions
  void boundsCheck(int,int)            const; // Ensure 0<=i,j<nrows,ncols
  void rowBoundsCheck(int i)           const; // Ensure 0<=i<nrows
  void colBoundsCheck(int j)           const; // Ensure 0<=j<ncols
  void lengthCheck(unsigned,unsigned)  const; // Check that size of self is m,n
  void colCheck(unsigned)              const; // Ensure n=ncols
  void rowCheck(unsigned)              const; // Ensure m=nrows
  void sliceCheck(int,int,unsigned,int,int) const;     // Check that slice is valid
  void sliceCheck(int,int,unsigned,unsigned,int,int,int,int) const; // Check that slice is valid
  void numPointsCheck(const char*,int) const; // Check that there is at least n points        
//static void  versionErr(int,int);           // Error: version number wrong in restoreFrom

public:  // These functions really are for public consumption
  unsigned   cols()                const      {return ncols;}
  int        colStride()           const      {return colstep;}
  unsigned   rows()                const      {return nrows;}
  int        rowStride()           const      {return rowstep;}
};

/*
 * The MatrixLooper class is used internally by the matrix code.
 * This class is not documented in the manual, and may dissappear or
 * change in a future release, so I wouldn't use it if I were you!
 *
 * The MatrixLooper and DoubleMatrixLooper are exactly analogous to the
 * ArrayLooper classes, except for the special case of general matrices.
 *
 * The matrix looper loops through a column/row at a time.  If the columns
 * are contiguous, then it strings them together into one big mega-column.
 *
 * Whether we loop through columns or rows is determined by the GenMatStorage
 * enum.  The default is to loop through columns for matrices stored in
 * column major order and to loop through rows for matrices stored in row
 * major order.
 */

class MatrixLooper {
  int    ncolsLeft;
  int    colstep;   // How far to step from one column to the next

public:
	int      start;		// begin of the current column
	unsigned length;  // length of the column
	int      stride;	// the inter-column stride (the rowstride of the matrix)

#ifdef RW_GLOBAL_ENUMS
/* Our ancient 2.0 Cfront can\'t find RWEITHER. Use ugly hack == 2 */
        MatrixLooper(unsigned,unsigned, int,int, Storage dummyArg   =(enum Storage)2);
#else       
	MatrixLooper(unsigned,unsigned, int,int, RWDataView::Storage=RWDataView::RWEITHER);

#endif

	void operator++(){ start+=colstep; --ncolsLeft; }
	operator void*() { return (ncolsLeft<=0) ? 0 : (void*)1; }
};

class DoubleMatrixLooper {
  int    ncolsLeft;
	int    colstep1;
	int    colstep2;

public:
	unsigned length;
	int      start1;
	int      start2;
	int      stride1;
	int      stride2;

#ifdef RW_GLOBAL_ENUMS
/* Our ancient 2.0 Cfront can\'t find RWEITHER. Use ugly hack == 2 */
        DoubleMatrixLooper(unsigned,unsigned, int,int, int,int,             Storage dummyArg = (enum Storage)2);
#else
	DoubleMatrixLooper(unsigned,unsigned, int,int, int,int, RWDataView::Storage=RWDataView::RWEITHER);
#endif
 
	void operator++(){ start1+=colstep1; start2+=colstep2; --ncolsLeft; }
	operator void*() { return (ncolsLeft<=0) ? 0 : (void*)1; }
};

#endif /*__RWMATRIX_H__*/


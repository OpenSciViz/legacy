#ifndef __RWCBANDMAT_H__
#define __RWCBANDMAT_H__
/*
 * DComplexBandMat - A banded matrix of DComplexs
 *
 * Stores a banded matrix.  All entries farther above the diagonal
 * that the upper bandwidth, or farther below than the lower bandwidth
 * are defined to be zero.  The total bandwidth is the sum of the 
 * upper and lower bandwidths, plus one for the diagonal.  Only
 * the elements which may be non-zero are stored.
 *
 * The matrix is stored column by column.  Each column takes up 
 * <bandwidth> entries, some of these entries near the corners
 * are not used.
 *
 * Generated from template $Id: xmat.h,v 1.4 1993/10/11 21:48:17 alv Exp $
 *
 * Bounds checking is enabled by setting the preprocessor
 * symbol RWBOUNDS_CHECK.  This is strongly encouraged.
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

#include "rw/cvec.h"
#include "rw/cref.h"

extern DComplex rwDComplexZero;	// This constant is sometimes returned by reference

class DoubleBandMat;
class DComplexGenMat;
class DComplexTriDiagMat;
class DComplexSymBandMat;
class DComplexHermBandMat;

class DComplexBandMat {
private:

DComplexVec vec;
unsigned n;
unsigned band, bandu;	// bandwidth and upper bandwidth
  // The data which define the matrix

  void zeroUnusedEntries();  // Initialize entries in the data vector which are not used to zero

public:
DComplexBandMat();
DComplexBandMat( const DComplexBandMat& );
DComplexBandMat(unsigned n, unsigned nAgain, unsigned lowerWidth, unsigned upperWidth);
DComplexBandMat(const DComplexVec& data, unsigned n, unsigned nAgain, unsigned lowerWidth, unsigned upperWdth);
DComplexBandMat(const DoubleBandMat& re);
DComplexBandMat(const DoubleBandMat& re, const DoubleBandMat& im);
DComplexBandMat( const DComplexTriDiagMat& A );
DComplexBandMat( const DComplexSymBandMat& A );
DComplexBandMat( const DComplexHermBandMat& A );
~DComplexBandMat();
  // Constructors, conversion functions, and a destructor.

static const char *className;
  // For use in error messages

inline unsigned rows() const	   { return n; }
inline unsigned cols() const	   { return n; }
inline unsigned bandwidth() const      { return band; }
inline unsigned lowerBandwidth() const { return band-bandu-1; }
inline unsigned upperBandwidth() const { return bandu; }
  // Member functions to access the shape of the matrix

inline DComplex  val(int i, int j) const;
       DComplex  bcval(int i, int j) const;
inline DComplex  set(int i, int j, DComplex x);
       DComplex  bcset(int i, int j, DComplex x);
inline RODComplexRef operator()(int i, int j);
inline RODComplexRef ref(int i, int j);
       RODComplexRef bcref(int i, int j);
inline DComplex  operator()(int i, int j) const; // { return val(i,j); }
  // Access and/or change elements of the matrix.
  // The versions that begin with the letters bc do bounds
  // checking.  The others do bounds checking only if the
  // preprocessor symbol RWBOUNDS_CHECK is defined.

DComplexBandMat leadingSubmatrix(int order);
  // Access to submatrices.
  // The leading submatrix of order k is the kxk matrix in the upper
  // left.
inline DComplexVec diagonal(int =0) const;
       DComplexVec bcdiagonal(int =0) const;
  // Access to diagonals.  The same conventions as for
  // element access apply (with respect to bcxxx and xxx names).

DComplexBandMat& operator=(const DComplexBandMat& m);
DComplexBandMat& reference(DComplexBandMat& m);
  // reference() makes an alias, operator= makes a copy.


void zero()        { vec=DComplex(0,0); }
  // Sets all elements of the matrix equal to zero.


DComplexBandMat copy() const;
DComplexBandMat deepCopy() const		{ return copy(); }
void deepenShallowCopy()        { vec.deepenShallowCopy(); }
  // copy() returns a DComplexBandMat with its own copy of the data.
  // deepenShallowCopy makes sure the data is not shared by another
  // vector or matrix.

DComplexVec dataVec() 		{ return vec; }
const DComplexVec& dataVec() const	{ return vec; }
DComplex* data() 			{ return vec.data(); }
const DComplex* data() const 	{ return vec.data(); }
  // Access to the raw data.

void resize(unsigned m, unsigned n);
void resize(unsigned n, unsigned nAgain, unsigned lowWidth, unsigned upWidth);
  // Resize the matrix.  New elements are set to zero.


void scanFrom(istream&);      		// Human readable
void printOn(ostream&) const;
void restoreFrom(RWvistream&);     	// Internal ASCII or binary code
void saveOn(RWvostream&) const;
void restoreFrom(RWFile&);
void saveOn(RWFile&) const;
unsigned binaryStoreSize() const;
  // Input/output member functions
  // Whether ASCII or binary formatting is used with readFrom and
  // storeOn is determined by the type of rwstream passed.

/*
 * The operator()() member functions are declared with ref(),
 * since the declarations look the same.
 *
 * The operator=() assignment operators are declared with reference(),
 * since the functions are so closely related.
 */

RWBoolean operator==(const DComplexBandMat& X);
RWBoolean operator!=(const DComplexBandMat& X)  { return !(operator==(X)); }
  // Two matrices are considered equal if their shapes are identical and
  // all their components are identical.

DComplexBandMat& operator+=(const DComplexBandMat& m);
DComplexBandMat& operator-=(const DComplexBandMat& m);
DComplexBandMat& operator*=(const DComplexBandMat& m);
DComplexBandMat& operator*=(DComplex);
DComplexBandMat& operator/=(const DComplexBandMat& m);
DComplexBandMat& operator/=(DComplex);
  // assignment operators.  self must be same size as m.


};

DComplexBandMat operator-(const DComplexBandMat&);	// Unary minus
DComplexBandMat operator+(const DComplexBandMat&);	// Unary plus
DComplexBandMat operator*(const DComplexBandMat&, const DComplexBandMat&);
DComplexBandMat operator/(const DComplexBandMat&, const DComplexBandMat&);
DComplexBandMat operator+(const DComplexBandMat&, const DComplexBandMat&);
DComplexBandMat operator-(const DComplexBandMat&, const DComplexBandMat&);
DComplexBandMat operator*(const DComplexBandMat&, DComplex);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
inline DComplexBandMat operator*(DComplex x, const DComplexBandMat& A) { return A*x; }
#else
       DComplexBandMat operator*(DComplex x, const DComplexBandMat& A);
#endif
DComplexBandMat operator/(const DComplexBandMat& A, DComplex x);
  // Arithmetic operators; Notice that operator* is an
  // element by element multiply, NOT a matrix multiply.

inline ostream& operator<<(ostream& s, const DComplexBandMat& m)
		{m.printOn(s); return s;}
inline istream& operator>>(istream& s, DComplexBandMat& m)
		{m.scanFrom(s); return s;}
  // I/O for humans

DComplexBandMat transpose(const DComplexBandMat&);
  // The transpose of the matrix. 

DComplexVec product(const DComplexBandMat& A, const DComplexVec& x);
DComplexVec product(const DComplexVec& x, const DComplexBandMat& A);
DComplexBandMat product(const DComplexBandMat& A, const DComplexBandMat& B);
DComplexBandMat transposeProduct(const DComplexBandMat& A, const DComplexBandMat& B);
DComplexBandMat conjTransposeProduct(const DComplexBandMat& A, const DComplexBandMat& B);
  // conjTransposeProduct calculates conj(A^T)B
  // transposeProduct calculates A^TB
  // inner products

DComplexBandMat toBandMat( const DComplexGenMat& A, unsigned bandl, unsigned bandu );

DoubleBandMat abs(const DComplexBandMat& A);
DComplexBandMat conj(const DComplexBandMat& A);
DoubleBandMat real(const DComplexBandMat& A);
DoubleBandMat imag(const DComplexBandMat& A);
DoubleBandMat norm(const DComplexBandMat& A);
DoubleBandMat arg(const DComplexBandMat& A);



/*
 * Inline functions
 */

inline DComplex DComplexBandMat::val(int i, int j) const {
    return bcval(i,j);
}

inline DComplex DComplexBandMat::set(int i, int j, DComplex x) {
#ifdef RWBOUNDS_CHECK
return bcset(i,j,x);
#else
    return vec(i-j+bandu + j*band)=x;
#endif
}

inline RODComplexRef DComplexBandMat::ref(int i, int j) {
    return bcref(i,j);
}

inline RODComplexRef DComplexBandMat::operator()(int i, int j) { return ref(i,j); }
inline DComplex DComplexBandMat::operator()(int i, int j) const { return val(i,j); }

inline DComplexVec DComplexBandMat::diagonal(int i) const {
#if defined(RWBOUNDS_CHECK)
    return bcdiagonal(i);
#else
    int iabs = (i>=0) ? i : -i;
    return vec.slice(i>0 ? upperBandwidth()+i*(bandwidth()-1)
			 : upperBandwidth()+iabs
			 , n-iabs, bandwidth() );
#endif
}

#endif

#ifndef __RWUCGENMAT_H__
#define __RWUCGENMAT_H__

/*
 * Declarations for UChar precision matrices
 *
 * Generated from template $Id: matrix.htm,v 1.17 1993/09/17 18:13:14 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010
 *
 * Copyright (C) 1988, 1989, 1990, 1991, 1993.
 * This software is subject to copyright protection under the laws
 * of the United States and other countries.
 *
 ***************************************************************************
 *
 */

/*
 * Defining the preprocessor directive RWBOUNDS_CHECK will
 * cause bounds checking on the subscripting operators
 * and inlined math functions.
 */

#include "rw/matrix.h"
#include "rw/ucvec.h"

class UCharGenMatPick;
class RWRand;

class UCharGenMat : public RWMatView {
private:
    UCharGenMat(const RWDataView&, UChar*, unsigned,unsigned, int,int); // For internal use
    friend UCharGenMat toGenMat(const UCharArray&);
    friend class UCharArray;                    // For conversion constructor

    /*****************************
     * Routines for internal use *
     *****************************/

    /* These slice functions dispense with bounds checking for speed */
    UCharVec fastSlice(int starti, int startj, unsigned n, int rowstride, int colstride) const;
    UCharGenMat fastSlice(int starti, int startj, unsigned m, unsigned n, int rowstr1, int colstr1, int rowstr2, int colstr2) const;

public:

    /****************
     * Constructors *
     ****************/

    UCharGenMat() : RWMatView()                 {}
#ifdef RW_ALLOW_OLD_ARRAY_CONSTRUCTORS
    UCharGenMat(unsigned m, unsigned n, Storage s=COLUMN_MAJOR) : RWMatView(m,n,sizeof(UChar),s) {}
#endif
    UCharGenMat(unsigned m, unsigned n, RWUninitialized, Storage s=COLUMN_MAJOR) : RWMatView(m,n,sizeof(UChar),s) {}
    UCharGenMat(unsigned m, unsigned n, RWRand&);
    UCharGenMat(unsigned m, unsigned n, UChar val);
    UCharGenMat(const char *);
    UCharGenMat(const UCharGenMat& a) : RWMatView((const RWMatView&)a) {}  // cast needed by Zortech 3.1
    UCharGenMat(const UCharGenMatPick& p);
    UCharGenMat(const UChar* dat, unsigned,unsigned,Storage=COLUMN_MAJOR); // Copy of dat will be made
    UCharGenMat(const UCharVec& vec, unsigned,unsigned,Storage=COLUMN_MAJOR);// View of vec will be taken
    UCharGenMat(RWBlock *block,    unsigned,unsigned,Storage=COLUMN_MAJOR);// Use a custom block for the data



    operator IntGenMat() const;                 // Convert to IntGenMat

    /********************
     * Member functions *
     ********************/

    UCharGenMat apply(mathFunTy)     const;
#ifdef RW_NATIVE_EXTENDED
    UCharGenMat apply(XmathFunTy)    const;
#endif
    UChar&     bcref(int i, int j);             // ref() with bounds checking: for matrix.h++ compatability
    void       bcset(int i, int j, UChar x);    // set() with bounds checking: for matrix.h++ compatability
    UChar      bcval(int i, int j)   const;     // val() with bounds checking: for matrix.h++ compatability
    unsigned   binaryStoreSize()     const;     // Storage requirements
    const UCharVec col(int)          const;     // Returns a view of a column
    UCharVec   col(int);                        // Returns a view of a column
//  unsigned   cols()                const      // Number of columns
//  int        colStride()           const      // Step from one column to the next
    UCharGenMat copy(Storage s=COLUMN_MAJOR)const;//Copy with distinct instance variables
    UChar*     data()                           {return (UChar*)begin;}
    const UChar* data()              const      {return (UChar*)begin;}
    UCharGenMat deepCopy(Storage s=COLUMN_MAJOR)const;//Copy with distinct instance variables
    void       deepenShallowCopy(Storage s=COLUMN_MAJOR); // Ensures only 1 reference to data
    const UCharVec diagonal(int =0)  const;     // Returns a view of a diagonal
    UCharVec   diagonal(int =0);                // Returns a view of a diagonal
    UCharGenMatPick pick(const IntVec&, const IntVec&); // Return the "picked" elements
    const UCharGenMatPick pick(const IntVec&, const IntVec&) const; // Return the "picked" elements
    void       printOn(ostream& s)   const;     // Pretty print
    UChar&     ref(int i, int j);               // for matrix.h++ compatability
    UCharGenMat& reference(const UCharGenMat& v); // Reference self to v
    void       resize(unsigned,unsigned);       // Will pad with zeroes if necessary
    void       reshape(unsigned,unsigned,Storage s=COLUMN_MAJOR); // Contents after reshape are garbage
    void       restoreFrom(RWFile&);            // Restore from binary
    void       restoreFrom(RWvistream&);        // Restore from virtual stream
    const UCharVec row(int)          const;     // Returns a view of a row
    UCharVec   row(int);                        // Returns a view of a row
//  unsigned   rows()                const      // Number of rows
//  int        rowStride()           const      // Step from one row to the next
    void       saveOn(RWFile&)       const;     // Store using binary
    void       saveOn(RWvostream&)   const;     // Store to virtual stream
    void       scanFrom(istream& s);            // Read to eof or delimit with []
    void       set(int i, int j, UChar x);      // for matrix.h++ compatability 
    UCharVec   slice(int starti, int startj, unsigned n, int rowstride, int colstride) const;
    UCharGenMat slice(int starti, int startj, unsigned m, unsigned n, int rowstr1, int colstr1, int rowstr2, int colstr2) const;
    UChar      val(int i, int j)     const;     // for matrix.h++ compatability
    void       zero()                           {(*this)=(UChar)0;}      // for matrix.h++ compatability


    /********************
     * Member operators *
     ********************/

    UChar&               operator()(int,int);           
    UChar                operator()(int,int) const;
    UCharVec             operator()(int, const RWSlice&);
    const UCharVec       operator()(int, const RWSlice&)            const;
    UCharVec             operator()(const RWSlice&, int);
    const UCharVec       operator()(const RWSlice&, int)            const;
    UCharGenMat          operator()(const RWSlice&, const RWSlice&);
    const UCharGenMat    operator()(const RWSlice&, const RWSlice&) const;
    UCharGenMat& operator=(const UCharGenMat& v); // Must be same size as v
    UCharGenMat& operator=(const UCharGenMatPick&);
    UCharGenMat& operator=(UChar);
    RWBoolean   operator==(const UCharGenMat&) const;
    RWBoolean   operator!=(const UCharGenMat& v) const;
    UCharGenMat& operator+=(UChar);
    UCharGenMat& operator-=(UChar s);
    UCharGenMat& operator*=(UChar);
    UCharGenMat& operator/=(UChar);
    UCharGenMat& operator+=(const UCharGenMat&);
    UCharGenMat& operator-=(const UCharGenMat&);
    UCharGenMat& operator*=(const UCharGenMat&);
    UCharGenMat& operator/=(const UCharGenMat&);
    UCharGenMat& operator++();                  // Prefix operator
    UCharGenMat& operator--();                  // Prefix operator
#if             !defined(RW_NO_POSTFIX)
    void        operator++(int)                 { ++(*this); }  // Postfix operator
    void        operator--(int)                 { --(*this); }  // Postfix operator
#endif

};
  
    /********************
     * Global Operators *
     ********************/

    inline UCharGenMat operator+(const UCharGenMat& v) { return v; }
           UCharGenMat operator+(const UCharGenMat&, const UCharGenMat&);
           UCharGenMat operator-(const UCharGenMat&, const UCharGenMat&);
           UCharGenMat operator*(const UCharGenMat&, const UCharGenMat&);
           UCharGenMat operator/(const UCharGenMat&, const UCharGenMat&);
// The % operator used for inner products.  These definitions are
// postponed until after product() has been declared
//  inline UCharGenMat operator%(const UCharGenMat& A, const UCharGenMat& B) {return product(A,B);}
//  inline UCharVec    operator%(const UCharGenMat& A, const UCharVec& x) {return product(A,x);}
//  inline UCharVec    operator%(const UCharVec& x, const UCharGenMat& A) {return product(x,A);}
           UCharGenMat operator+(const UCharGenMat&,UChar);
           UCharGenMat operator+(UChar, const UCharGenMat&);
           UCharGenMat operator-(const UCharGenMat&,UChar);
           UCharGenMat operator-(UChar, const UCharGenMat&);
           UCharGenMat operator*(const UCharGenMat&,UChar);
           UCharGenMat operator*(UChar, const UCharGenMat&);
           UCharGenMat operator/(const UCharGenMat&,UChar);
           UCharGenMat operator/(UChar, const UCharGenMat&);
           ostream&    operator<<(ostream& s, const UCharGenMat& v);
           istream&    operator>>(istream& s, UCharGenMat& v);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
    inline UCharGenMat operator*(UChar s, const UCharGenMat& V) {return V*s;}
    inline UCharGenMat operator+(UChar s, const UCharGenMat& V) {return V+s;}
#endif

    /********************
     * Global Functions *
     ********************/

    /**** Miscellaneous Functions ****/

                                                  // No       abs() for UCharGenMat
           UChar       dot(const UCharGenMat&,const UCharGenMat&); // Return sum_ij (Aij*Bij)
           void        maxIndex(const UCharGenMat&, int *i, int *j);
           void        minIndex(const UCharGenMat&, int *i, int *j);
           UChar       maxValue(const UCharGenMat&);
           UChar       minValue(const UCharGenMat&);
           UChar       prod(const UCharGenMat&);
           UCharVec    product(const UCharGenMat&, const UCharVec&);
           UCharVec    product(const UCharVec&, const UCharGenMat&);
           UCharGenMat product(const UCharGenMat&, const UCharGenMat&);
           UChar       sum(const UCharGenMat&);
           UCharGenMat transpose(const UCharGenMat&); // Transpose references this's data
    inline UCharGenMat adjoint(const UCharGenMat& A) {return transpose(A);}
           UCharGenMat transposeProduct(const UCharGenMat&,const UCharGenMat&); // For matrix.h++ compatability



    /**** Functions for type conversion ****/

                                                  // No type conversion functions for UCharGenMat

    /**** Norm functions ****/

    /***************************
     * Inline Access Functions *
     ***************************/


  inline UChar& UCharGenMat::operator()(int i, int j) {
#  ifdef RWBOUNDS_CHECK    
    boundsCheck(i,j);
#  endif
    return data()[i*rowstep+j*colstep];
  }

  inline UChar UCharGenMat::operator()(int i, int j) const {
#  ifdef RWBOUNDS_CHECK    
    boundsCheck(i,j);
#  endif
    return data()[i*rowstep+j*colstep];
  }

  inline UCharVec UCharGenMat::operator()(int i, const RWSlice& j) {
#  ifdef RWBOUNDS_CHECK
    rowBoundsCheck(i);
    j.boundsCheck(ncols);
#  endif
    return fastSlice(i,j.begin(),j.len(ncols),0,j.stride());
}

  inline const UCharVec UCharGenMat::operator()(int i, const RWSlice& j) const {
#  ifdef RWBOUNDS_CHECK
    rowBoundsCheck(i);
    j.boundsCheck(ncols);
#  endif
    return fastSlice(i,j.begin(),j.len(ncols),0,j.stride());
}

  inline UCharVec UCharGenMat::operator()(const RWSlice& i, int j) {
#  ifdef RWBOUNDS_CHECK
    colBoundsCheck(j);
    i.boundsCheck(nrows);
#  endif
    return fastSlice(i.begin(),j,i.len(nrows),i.stride(),0);
}

  inline const UCharVec UCharGenMat::operator()(const RWSlice& i, int j) const {
#  ifdef RWBOUNDS_CHECK
    colBoundsCheck(j);
    i.boundsCheck(nrows);
#  endif
    return fastSlice(i.begin(),j,i.len(nrows),i.stride(),0);
}

  inline UCharGenMat UCharGenMat::operator()(const RWSlice& i, const RWSlice& j) {
#  ifdef RWBOUNDS_CHECK
    i.boundsCheck(nrows);
    j.boundsCheck(ncols);
#  endif
    return fastSlice(i.begin(),j.begin(),i.len(nrows),j.len(ncols),i.stride(),0,0,j.stride());
}

  inline const UCharGenMat UCharGenMat::operator()(const RWSlice& i, const RWSlice& j) const {
#  ifdef RWBOUNDS_CHECK
    i.boundsCheck(nrows);
    j.boundsCheck(ncols);
#  endif
    return fastSlice(i.begin(),j.begin(),i.len(nrows),j.len(ncols),i.stride(),0,0,j.stride());
}


// These next functions predate the nice subscripting mechanisms 
// available with the current math.h++

inline const UCharVec UCharGenMat::col(int j) const { return (*this)(RWAll,j); }
inline       UCharVec UCharGenMat::col(int j)       { return (*this)(RWAll,j); }
inline const UCharVec UCharGenMat::row(int i) const { return (*this)(i,RWAll); }
inline       UCharVec UCharGenMat::row(int i)       { return (*this)(i,RWAll); }


// The following functions are for compatability with the other matrix
// types provided by matrix.h++

  inline UChar& UCharGenMat::ref(int i, int j) {
    return (*this)(i,j);
  }

  inline void UCharGenMat::set(int i, int j, UChar x) {
    (*this)(i,j)=x;
  }

  inline UChar UCharGenMat::val(int i, int j) const {
    return (*this)(i,j);
  }

  inline UChar& UCharGenMat::bcref(int i, int j) {
    boundsCheck(i,j);
    return data()[i*rowstep+j*colstep];
  }

  inline void UCharGenMat::bcset(int i, int j, UChar x) {
    boundsCheck(i,j);
    data()[i*rowstep+j*colstep]=x;
  }

  inline UChar UCharGenMat::bcval(int i, int j) const {
    boundsCheck(i,j);
    return data()[i*rowstep+j*colstep];
  }


// Here are the definitions of operator% for inner products.  We
// had to postpone them because in the operator section product()
// was not yet declared.
inline UCharGenMat         operator%(const UCharGenMat& A, const UCharGenMat& B) {return product(A,B);}
inline UCharVec      operator%(const UCharGenMat& A, const UCharVec& x) {return product(A,x);}
inline UCharVec      operator%(const UCharVec& x, const UCharGenMat& A) {return product(x,A);}

#endif /* __RWUCGENMAT_H__ */

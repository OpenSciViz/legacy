#ifndef __RWRSTREAM_H__
#define __RWRSTREAM_H__

/*
 * Includes either stream.h or iostream.h, depending
 * on the compiler.
 *
 * $Id: rstream.h,v 2.5 1993/09/10 02:56:53 keffer Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010	FAX: (503) 757-6650
 *
 * Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: rstream.h,v $
 * Revision 2.5  1993/09/10  02:56:53  keffer
 * Switched RCS idents to avoid spurious diffs
 *
 * Revision 2.4  1993/03/13  01:53:16  keffer
 * Moved OPEN[IO]STREAM to backward.h
 *
 * Revision 2.2  1992/11/15  22:12:09  keffer
 * Removed use of macro NL
 *
 * Revision 2.1  1992/11/04  23:03:15  myersn
 * *** empty log message ***
 *
 * Revision 2.0  1992/10/23  03:27:32  keffer
 * RCS Baseline version
 *
 * 
 *    Rev 1.8   04 Mar 1992 10:20:22   KEFFER
 * Includes "rw/defs.h" instead of "rw/compiler.h"
 * 
 *    Rev 1.7   18 Feb 1992 09:54:36   KEFFER
 * 
 *    Rev 1.6   27 Oct 1991 17:39:06   keffer
 * Corrected Glock inclusion of iostream.hxx
 * 
 *    Rev 1.5   17 Oct 1991 09:12:56   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.3   24 Sep 1991 11:08:10   keffer
 * Zortech now includes iostream.hpp; Glock iostream.hxx.
 * 
 *    Rev 1.2   08 Sep 1991 19:10:08   keffer
 * Changed name of eatwhite() to rwEatwhite()
 * 
 *    Rev 1.1   24 Jul 1991 13:06:46   keffer
 * Added pvcs keywords
 *
 */

#include "rw/defs.h"

#ifdef __ZTC__
#  include <iostream.hpp>
#else
#  ifdef __GLOCK__
#    include <iostream.hxx>
#  else
#    include <iostream.h>
#  endif
#endif

inline istream& rwEatwhite(istream& s) {return s >> ws;}

#endif /* __RWRSTREAM_H__ */

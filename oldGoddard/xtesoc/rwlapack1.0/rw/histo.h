#ifndef __RWHISTO_H__
#define __RWHISTO_H__

/*
 * Declarations for Histogram Class.
 *
 * $Header: /users/rcs/mathrw/histo.h,v 1.1 1993/01/23 00:08:35 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * Copyright (C) 1988, 1989, 1990, 1991.  This software is subject to copyright
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: histo.h,v $
 * Revision 1.1  1993/01/23  00:08:35  alv
 * Initial revision
 *
 * 
 *    Rev 1.3   17 Oct 1991 09:12:58   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.1   24 Jul 1991 13:01:00   keffer
 * Added pvcs keywords
 *
 */

#include "rw/ivec.h"
#include "rw/dvec.h"
#include "rw/rstream.h"

class Histogram : public IntVec {
private:
  unsigned   		totalCounts;
  unsigned   		nbins;
  DoubleVec  		binValues;
  unsigned   		largerValues;
  unsigned   		smallerValues;
public:
  // constructors:

  // Histogram (with n bins), scaled with the min and max of v;
  // Data in v is added to Histogram.
  Histogram( unsigned n, const DoubleVec& v );

  // Histogram with boundaries given by b:
  Histogram( const DoubleVec& b );

  // Histogram with n bins, scaled with bmin and bmax:
  Histogram( unsigned n, double bmin, double bmax);

  unsigned  		larger() const		{return largerValues;}
  unsigned  		smaller() const		{return smallerValues;}
  unsigned  		bins() const		{return nbins;}
  DoubleVec 		binBoundaries() const	{return binValues.copy();}

  void      		addCount(double);
  void      		addCount(const DoubleVec& v);
  unsigned		binaryStoreSize() const;		// Storage requirements.

friend ostream& 	operator<<(ostream& s, const Histogram& h);

};

#endif /*__RWHISTO_H__*/

#ifndef __RWDLSSV_H__
#define __RWDLSSV_H__
/*
 * DoubleLeastSqSV:  solve least square SV problem with orthogonal decomposition
 *
 * Generated from template $Id: xlssv.h,v 1.3 1993/07/05 19:19:33 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 * Uses a singular value decomposition to solve the linear
 * least squares problem.  The solution is the minimum norm solution
 * which minimizes the residual.
 *
 * When building a factorization using either a constructor or one
 * of the factor functions, you can supply a tolerance parameter.
 * Entries along the diagonal of the R factor of the SV decomposition
 * less than this tolerance are treated as zero.  Using this can
 * prevent insignificant entries of R from corrupting your solution.
 *
 * The factor functions and constructors taking a SV decomposition
 * are useful if you'd like to examine the singular values
 * for yourself and decide what tolerance to use.
 */

#include "rw/dsv.h"

class DoubleLeastSqSV : public DoubleSVDecomp {
public:
  DoubleLeastSqSV();
  DoubleLeastSqSV(const DoubleGenMat& A, Double tol=0);
  DoubleLeastSqSV(const DoubleSVDecomp& A, Double tol=0);
//void      factor(const DoubleGenMat& A, Double tol=0);
//RWBoolean good() const;
//RWBoolean fail() const;
//unsigned  rows() const {return decomp_.rows();}
//unsigned  cols() const {return decomp_.cols();}
//unsigned  rank() const {return decomp_.rank();}
//void      truncate(Double tol);
  DoubleVec    residual(const DoubleVec& data) const;
  Double       residualNorm(const DoubleVec& data) const;
  DoubleVec    solve(const DoubleVec& data) const;
};

inline DoubleVec solve(   const DoubleLeastSqSV& A, const DoubleVec& b) {return A.solve(b);}
inline DoubleVec residual(const DoubleLeastSqSV& A, const DoubleVec& b) {return A.residual(b);}
inline Double    residualNorm(const DoubleLeastSqSV& A, const DoubleVec& b) {return A.residualNorm(b);}

#endif

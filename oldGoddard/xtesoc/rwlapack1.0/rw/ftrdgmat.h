#ifndef __RWFTRDGMAT_H__
#define __RWFTRDGMAT_H__
/*
 * FloatTriDiagMat - A tridiagonal matrix of Floats
 *
 * Stores a tridiagonal matrix.
 *
 * The matrix is stored in the same format as a banded matrix with
 * upper and lower bandwidth one.
 * .fi
 *
 * Generated from template $Id: xmat.h,v 1.4 1993/10/11 21:48:17 alv Exp $
 *
 * Bounds checking is enabled by setting the preprocessor
 * symbol RWBOUNDS_CHECK.  This is strongly encouraged.
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

#include "rw/fvec.h"
#include "rw/fref.h"

extern Float rwFloatZero;	// This constant is sometimes returned by reference

class DoubleTriDiagMat;
class FloatGenMat;
class FloatBandMat;

class FloatTriDiagMat {
private:

FloatVec vec;
unsigned n;
  // The data which define the matrix

  void zeroUnusedEntries();  // Initialize entries in the data vector which are not used to zero

public:
FloatTriDiagMat();
FloatTriDiagMat( const FloatTriDiagMat& );
FloatTriDiagMat(unsigned n, unsigned nAgain);
FloatTriDiagMat(const FloatVec& data, unsigned n, unsigned nAgain);
~FloatTriDiagMat();
  // Constructors, conversion functions, and a destructor.

static const char *className;
  // For use in error messages

inline unsigned rows() const	   { return n; }
inline unsigned cols() const	   { return n; }
inline unsigned bandwidth() const      { return 3; }
inline unsigned lowerBandwidth() const { return 1; }
inline unsigned upperBandwidth() const { return 1; }
inline unsigned halfBandwidth() const  { return 1; }
  // Member functions to access the shape of the matrix

inline Float  val(int i, int j) const;
       Float  bcval(int i, int j) const;
inline Float  set(int i, int j, Float x);
       Float  bcset(int i, int j, Float x);
inline ROFloatRef operator()(int i, int j);
inline ROFloatRef ref(int i, int j);
       ROFloatRef bcref(int i, int j);
inline Float  operator()(int i, int j) const; // { return val(i,j); }
  // Access and/or change elements of the matrix.
  // The versions that begin with the letters bc do bounds
  // checking.  The others do bounds checking only if the
  // preprocessor symbol RWBOUNDS_CHECK is defined.

FloatTriDiagMat leadingSubmatrix(int order);
  // Access to submatrices.
  // The leading submatrix of order k is the kxk matrix in the upper
  // left.
inline FloatVec diagonal(int =0) const;
       FloatVec bcdiagonal(int =0) const;
  // Access to diagonals.  The same conventions as for
  // element access apply (with respect to bcxxx and xxx names).

FloatTriDiagMat& operator=(const FloatTriDiagMat& m);
FloatTriDiagMat& reference(FloatTriDiagMat& m);
  // reference() makes an alias, operator= makes a copy.


void zero()        { vec=Float(0); }
  // Sets all elements of the matrix equal to zero.


FloatTriDiagMat copy() const;
FloatTriDiagMat deepCopy() const		{ return copy(); }
void deepenShallowCopy()        { vec.deepenShallowCopy(); }
  // copy() returns a FloatTriDiagMat with its own copy of the data.
  // deepenShallowCopy makes sure the data is not shared by another
  // vector or matrix.

FloatVec dataVec() 		{ return vec; }
const FloatVec& dataVec() const	{ return vec; }
Float* data() 			{ return vec.data(); }
const Float* data() const 	{ return vec.data(); }
  // Access to the raw data.

void resize(unsigned m, unsigned n);
  // Resize the matrix.  New elements are set to zero.


void scanFrom(istream&);      		// Human readable
void printOn(ostream&) const;
void restoreFrom(RWvistream&);     	// Internal ASCII or binary code
void saveOn(RWvostream&) const;
void restoreFrom(RWFile&);
void saveOn(RWFile&) const;
unsigned binaryStoreSize() const;
  // Input/output member functions
  // Whether ASCII or binary formatting is used with readFrom and
  // storeOn is determined by the type of rwstream passed.

/*
 * The operator()() member functions are declared with ref(),
 * since the declarations look the same.
 *
 * The operator=() assignment operators are declared with reference(),
 * since the functions are so closely related.
 */

RWBoolean operator==(const FloatTriDiagMat& X);
RWBoolean operator!=(const FloatTriDiagMat& X)  { return !(operator==(X)); }
  // Two matrices are considered equal if their shapes are identical and
  // all their components are identical.

FloatTriDiagMat& operator+=(const FloatTriDiagMat& m);
FloatTriDiagMat& operator-=(const FloatTriDiagMat& m);
FloatTriDiagMat& operator*=(const FloatTriDiagMat& m);
FloatTriDiagMat& operator*=(Float);
FloatTriDiagMat& operator/=(const FloatTriDiagMat& m);
FloatTriDiagMat& operator/=(Float);
  // assignment operators.  self must be same size as m.


};

FloatTriDiagMat operator-(const FloatTriDiagMat&);	// Unary minus
FloatTriDiagMat operator+(const FloatTriDiagMat&);	// Unary plus
FloatTriDiagMat operator*(const FloatTriDiagMat&, const FloatTriDiagMat&);
FloatTriDiagMat operator/(const FloatTriDiagMat&, const FloatTriDiagMat&);
FloatTriDiagMat operator+(const FloatTriDiagMat&, const FloatTriDiagMat&);
FloatTriDiagMat operator-(const FloatTriDiagMat&, const FloatTriDiagMat&);
FloatTriDiagMat operator*(const FloatTriDiagMat&, Float);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
inline FloatTriDiagMat operator*(Float x, const FloatTriDiagMat& A) { return A*x; }
#else
       FloatTriDiagMat operator*(Float x, const FloatTriDiagMat& A);
#endif
FloatTriDiagMat operator/(const FloatTriDiagMat& A, Float x);
  // Arithmetic operators; Notice that operator* is an
  // element by element multiply, NOT a matrix multiply.

inline ostream& operator<<(ostream& s, const FloatTriDiagMat& m)
		{m.printOn(s); return s;}
inline istream& operator>>(istream& s, FloatTriDiagMat& m)
		{m.scanFrom(s); return s;}
  // I/O for humans

FloatTriDiagMat transpose(const FloatTriDiagMat&);
  // The transpose of the matrix. 

FloatVec product(const FloatTriDiagMat& A, const FloatVec& x);
FloatVec product(const FloatVec& x, const FloatTriDiagMat& A);
  // inner products

FloatTriDiagMat toFloat(const DoubleTriDiagMat& A);
FloatTriDiagMat toTriDiagMat( const FloatGenMat& A );
FloatTriDiagMat toTriDiagMat( const FloatBandMat& A );

FloatTriDiagMat abs(const FloatTriDiagMat& A);

inline Float minValue(const FloatTriDiagMat& A) { return minValue(A.dataVec()); }
inline Float maxValue(const FloatTriDiagMat& A) { return maxValue(A.dataVec()); }


/*
 * Inline functions
 */

inline Float FloatTriDiagMat::val(int i, int j) const {
#ifdef RWBOUNDS_CHECK
return bcval(i,j);
#else
    return ( (i-j>1) || (i-j<(-1)) ) ? (Float)(0) : vec(i+1+j*2);
#endif
}

inline Float FloatTriDiagMat::set(int i, int j, Float x) {
#ifdef RWBOUNDS_CHECK
return bcset(i,j,x);
#else
    return vec(i+1+j*2)=x;
#endif
}

inline ROFloatRef FloatTriDiagMat::ref(int i, int j) {
    return bcref(i,j);
}

inline ROFloatRef FloatTriDiagMat::operator()(int i, int j) { return ref(i,j); }
inline Float FloatTriDiagMat::operator()(int i, int j) const { return val(i,j); }

inline FloatVec FloatTriDiagMat::diagonal(int i) const {
#if defined(RWBOUNDS_CHECK)
    return bcdiagonal(i);
#else
    int iabs = (i>=0) ? i : -i;
    return vec.slice(i>0 ? 1 + i*2 : 1 + iabs, n-iabs, 3 );
#endif
}

#endif

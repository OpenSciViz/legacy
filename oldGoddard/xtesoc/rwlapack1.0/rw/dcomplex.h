#ifndef __RWDCOMPLEX_H__
#define __RWDCOMPLEX_H__

/*
 * Standard header file for double precision complex.
 *
 * $Header: /users/rcs/mathrw/dcomplex.h,v 1.10 1993/10/04 21:45:42 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1988-1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: dcomplex.h,v $
 * Revision 1.10  1993/10/04  21:45:42  alv
 * ported to Windows NT
 *
 * Revision 1.9  1993/09/19  16:40:27  alv
 * ported to lucid
 *
 * Revision 1.8  1993/08/17  17:11:05  alv
 * added ability to use custom RWBlocks
 *
 * Revision 1.6  1993/08/05  21:32:01  dealys
 * ported to MPW C++ 3.3
 *
 * Revision 1.5  1993/03/19  22:40:49  alv
 * moved complex tunables into mathdefs.h
 *
 * Revision 1.4  1993/03/17  17:25:36  alv
 * added clause __xlC__ for RS6000
 *
 * Revision 1.3  1993/03/03  01:05:15  alv
 * updated for lapack.h++
 *
 * Revision 1.1  1993/01/23  00:08:33  alv
 * Initial revision
 *
 * 
 *    Rev 1.7   16 Apr 1992 15:06:12   KEFFER
 * Ported to DEC C++.
 * 
 *    Rev 1.6   17 Oct 1991 09:12:44   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.4   24 Sep 1991 18:56:28   keffer
 * Ported to Zortech V3.0.  Moved the inclusion of "rw/mathdefs.h" to the C++ 
 * section.
 * 
 *    Rev 1.3   02 Sep 1991 12:56:42   keffer
 * Finesses the inclusion of iostream.h by the Borland compilers in complex.h
 * 
 *    Rev 1.2   26 Jul 1991 13:46:14   keffer
 * Now assumes that real leads unless the macro IMAG_LEADS is defined.
 * This is the more usual case
 * 
 *    Rev 1.1   24 Jul 1991 13:00:52   keffer
 * Added pvcs keywords
 *
 */

/*
 * Because there are so many variations of "Complex" types, this 
 * header file attempts to standardize what the user sees.  If you
 * include this header file and use type "DComplex" (for double
 * precision complex) you generally need not worry about what
 * compiler you are using.
 *
 * If you are using C++, then it will select the proper header file
 * and typedef DComplex to double precision complex.
 *
 * If you are using C, then it will declare a struct DComplex.
 */

/************************************************************************ 
 *                                                                      *
 *              From here on, it's pretty much boilerplate              *
 *              and rarely requires any tuning.                         *
 *                                                                      *
 ************************************************************************/

#ifdef __cplusplus

/****************************************************************
 *                                                              *
 *                              C++                             *
 *                                                              *
 ****************************************************************/

#include "rw/mathdefs.h"

#if defined(RW_USE_RW_COMPLEX)
#  include "rw/rcomplex.h"
   typedef DComplex complex;
   typedef DComplex (*CmathFunTy)(const DComplex&);
   typedef double  (*CmathFunTy2)(const DComplex&);

/***********************  MPW  **************************/

#elif defined(applec)
#  include "rw/rcomplex.h"
   typedef DComplex (*CmathFunTy)(DComplex);
   typedef double (*CmathFunTy2)(DComplex);

/***********************  GNU  **************************/

/*
 * The GNU library uses a capital "C", and passes by
 * reference.
 */

#elif defined(__GNUG__)
#  include <Complex.h>
   typedef Complex DComplex;
   typedef DComplex (*CmathFunTy)(const DComplex&);
   typedef double (*CmathFunTy2)(const DComplex&);

/**********************  AT&T  ***************************/

/*
 * The AT&T compilers uses a small "c", and passes by
 * value, EXCEPT for real and imag.
 */

/*
 * The !defined(applec) in the following should be irrelevant,
 * but it helps the Apple compiler
 */
#elif (defined(__ATT__) || defined(__xlC__) || defined(__lucid)) && !defined(applec)
#  ifdef __GLOCK__
     /* Glock for some silly reason uses ".hxx" suffixes: */
#    include <complex.hxx>
#  else
#    include <complex.h>
#  endif
   typedef complex DComplex;
   typedef DComplex (*CmathFunTy)(DComplex);
   typedef double  (*CmathFunTy2)(DComplex);

/**********************  Turbo C++  **********************/

/*
 * Turbo C++ uses a small "c" and passes by reference.
 */

#elif defined(__TURBOC__)

/*
 * For some inexplicable reason, Turbo and Borland C++ #include <iostream.h>
 * within complex.h, thus bogging down compile times and severely straining 
 * the compiler's symbol table.  This can be avoided by 
 * defining __IOSTREAM_H.  However, in case the user has already 
 * #included <iostream.h>, we must remember the old setting.
 */
#  ifndef __IOSTREAM_H
#    define __IOSTREAM_H
#    define RWTCCFIX 1  /* Remember the old setting */
#  endif
#  include <complex.h>
#  ifdef RWTCCFIX
#    undef __IOSTREAM_H /* Put things back the way they were */
#    undef RWTCCFIX
#  endif
   typedef complex DComplex;
   typedef DComplex (*CmathFunTy)(DComplex&);
   typedef double (*CmathFunTy2)(DComplex&);

/**********************  Early Zortech or Oregon C++  ********************/

/*
 * Zortech V2.06 and Oregon Software do not supply a complex class.
 * Zortech V2.1 does supply a complex class, but it has some
 * errors in it (type conversion does not work, pow() and other
 * transcendentals are not implemented correctly).
 *
 * Use the Rogue Wave supplied complex class
 */

#elif defined(__OREGON__) || (defined(__ZTC__) && (__ZTC__ < 0x300) )
#  include "rw/rcomplex.h"
   typedef DComplex complex;
   typedef DComplex (*CmathFunTy)(DComplex);
   typedef double  (*CmathFunTy2)(DComplex);

/************************  Late Zortech  ****************************/

/*
 * Later Zortech does supply a complex class.
 */
#elif defined(__ZTC__) && (__ZTC__ >= 0x300)
#  include <complex.hpp>
   typedef complex DComplex;
   typedef DComplex (*CmathFunTy)(const DComplex&);
   typedef double (*CmathFunTy2)(const DComplex&);

/*********************** Metaware High C++ ****************************/

/*
 * Metaware's complex is called complex and passes by non-const reference
 */
#elif defined(__HIGHC__)
#  include <complex.h>
   typedef complex DComplex;
   typedef DComplex (*CmathFunTy)(DComplex&);
   typedef double (*CmathFunTy2)(DComplex&);

/************************  Microsoft C/C++  ****************************/

/*
 * Microsoft C/C++ V7.0 does not supply a complex class.
 *
 * Use the Rogue Wave supplied complex class
 */

#elif defined(_MSC_VER) 
#  include "rw/rcomplex.h"
/*
 * If this is NT, omit the typedef because the nice folks at Microsoft
 * supply a "struct complex { double re,im; }".  Isn't that sweet of them?
 */
#if !defined(__WIN32__)
   typedef DComplex complex;
#endif
   typedef DComplex (*CmathFunTy)(DComplex);
   typedef double  (*CmathFunTy2)(DComplex);

/**************************  DEC C++  ****************************/

/*
 * Digital's complex is similar to late Zortech's, but has a different
 * header file name.
 */
#elif defined(__DECCXX)
#  include <complex.h>
   typedef complex DComplex;
   typedef DComplex (*CmathFunTy)(const DComplex&);
   typedef double (*CmathFunTy2)(const DComplex&);

/********************* Oh-oh, not a known compiler ****************/

#else
               /* The sun C compiler objects to #error */
Error error;   /* #error dcomplex.h has not been set up for this compiler! */
#endif

/***********************************************************************/

#else /* Not C++ */

/****************************************************************
 *                                                              *
 *                              C                               *
 *                                                              *
 ****************************************************************/

typedef struct { double r, i; } DComplex;

#endif /* __cplusplus */

#endif /* __RWDCOMPLEX_H__ */

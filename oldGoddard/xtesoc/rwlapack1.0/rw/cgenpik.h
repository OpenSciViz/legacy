#ifndef __CGENPIK_H__
#define __CGENPIK_H__

/*
 * Declarations for DComplexGenMatPick --- picks elements out of a DComplexGenMat
 *
 * Generated from template $Id: genpik.htm,v 1.3 1993/03/22 15:51:31 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311        FAX: (503) 757-7350
 *
 * Copyright (C) 1988, 1989, 1990, 1991.  This software is subject to copyright
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/ivec.h"

/*
 * The DComplexGenMatPick class allows selected elements to be addressed.
 * There are no public constructors.
 */

class DComplexGenMatPick {
friend class DComplexGenMat;
private:
                        DComplexGenMatPick(DComplexGenMat&,const IntVec&,const IntVec&);
  DComplexGenMat&             V;
  const IntVec     rowpick;
  const IntVec     colpick;
protected:         
  void                  assertElements() const;
  void                  lengthCheck(unsigned,unsigned) const;
public:            
  void                  operator=(const DComplexGenMat&);
  void                  operator=(const DComplexGenMatPick&);
  void                  operator=(DComplex);
                   
  unsigned              rows() const    { return rowpick.length(); }
  unsigned              cols() const    { return colpick.length(); }
};

/************************************************
 *                                              *
 *              I N L I N E S                   *
 *                                              *
 ************************************************/

inline
DComplexGenMatPick::DComplexGenMatPick(DComplexGenMat& v, const IntVec& x, const IntVec& y) :
  V(v), rowpick(x), colpick(y)
{
#ifdef RWBOUNDS_CHECK
  assertElements();
#endif
}

#endif /*__CGENPIK_H__*/

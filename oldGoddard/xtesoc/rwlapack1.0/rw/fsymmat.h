#ifndef __RWFSYMMAT_H__
#define __RWFSYMMAT_H__
/*
 * FloatSymMat - A symmetric matrix of Floats
 *
 * Stores a symmetric matrix.  Only half the matrix is actually
 * stored.  This means if you change, say, entry (2,3) you are also
 * implicitly changing entry (3,2).
 *
 * The lower left triangle of the matrix is stored in column major
 * order.
 *
 * Generated from template $Id: xmat.h,v 1.4 1993/10/11 21:48:17 alv Exp $
 *
 * Bounds checking is enabled by setting the preprocessor
 * symbol RWBOUNDS_CHECK.  This is strongly encouraged.
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

#include "rw/fvec.h"


class DoubleSymMat;
class FloatGenMat;

class FloatSymMat {
private:

FloatVec vec;
unsigned n;
  // The data which define the matrix


public:
FloatSymMat();
FloatSymMat( const FloatSymMat& );
FloatSymMat(unsigned n, unsigned nAgain);
FloatSymMat(unsigned n, unsigned nAgain, Float initval);
FloatSymMat(const FloatVec& data, unsigned n, unsigned nAgain);
~FloatSymMat();
  // Constructors, conversion functions, and a destructor.

static const char *className;
  // For use in error messages

inline unsigned rows() const	   { return n; }
inline unsigned cols() const	   { return n; }
  // Member functions to access the shape of the matrix

inline Float  val(int i, int j) const;
       Float  bcval(int i, int j) const;
inline Float  set(int i, int j, Float x);
       Float  bcset(int i, int j, Float x);
inline Float& operator()(int i, int j);
inline Float& ref(int i, int j);
       Float& bcref(int i, int j);
inline Float  operator()(int i, int j) const; // { return val(i,j); }
  // Access and/or change elements of the matrix.
  // The versions that begin with the letters bc do bounds
  // checking.  The others do bounds checking only if the
  // preprocessor symbol RWBOUNDS_CHECK is defined.

FloatSymMat leadingSubmatrix(int order);
  // Access to submatrices.
  // The leading submatrix of order k is the kxk matrix in the upper
  // left.
  // Access to diagonals.  The same conventions as for
  // element access apply (with respect to bcxxx and xxx names).

FloatSymMat& operator=(const FloatSymMat& m);
FloatSymMat& reference(FloatSymMat& m);
  // reference() makes an alias, operator= makes a copy.

FloatSymMat& operator=(Float x)   { vec=Float(x); return *this; }
  // Sets all elements of the matrix equal to x

void zero()        { vec=Float(0); }
  // Sets all elements of the matrix equal to zero.


FloatSymMat copy() const;
FloatSymMat deepCopy() const		{ return copy(); }
void deepenShallowCopy()        { vec.deepenShallowCopy(); }
  // copy() returns a FloatSymMat with its own copy of the data.
  // deepenShallowCopy makes sure the data is not shared by another
  // vector or matrix.

FloatVec dataVec() 		{ return vec; }
const FloatVec& dataVec() const	{ return vec; }
Float* data() 			{ return vec.data(); }
const Float* data() const 	{ return vec.data(); }
  // Access to the raw data.

void resize(unsigned m, unsigned n);
  // Resize the matrix.  New elements are set to zero.

FloatSymMat apply(mathFunTy) const;
  // Each element in the matrix returned by apply is
  // the result of calling f() on the corresponding element
  // in this matrix.  

void scanFrom(istream&);      		// Human readable
void printOn(ostream&) const;
void restoreFrom(RWvistream&);     	// Internal ASCII or binary code
void saveOn(RWvostream&) const;
void restoreFrom(RWFile&);
void saveOn(RWFile&) const;
unsigned binaryStoreSize() const;
  // Input/output member functions
  // Whether ASCII or binary formatting is used with readFrom and
  // storeOn is determined by the type of rwstream passed.

/*
 * The operator()() member functions are declared with ref(),
 * since the declarations look the same.
 *
 * The operator=() assignment operators are declared with reference(),
 * since the functions are so closely related.
 */

RWBoolean operator==(const FloatSymMat& X);
RWBoolean operator!=(const FloatSymMat& X)  { return !(operator==(X)); }
  // Two matrices are considered equal if their shapes are identical and
  // all their components are identical.

FloatSymMat& operator+=(Float);
FloatSymMat& operator-=(Float);
FloatSymMat& operator+=(const FloatSymMat& m);
FloatSymMat& operator-=(const FloatSymMat& m);
FloatSymMat& operator*=(const FloatSymMat& m);
FloatSymMat& operator*=(Float);
FloatSymMat& operator/=(const FloatSymMat& m);
FloatSymMat& operator/=(Float);
  // assignment operators.  self must be same size as m.

FloatSymMat& operator++();	// Prefix operator
#ifndef RW_NO_POSTFIX
void operator++(int);	// Postfix operator
#endif
FloatSymMat& operator--();	// Prefix operator
#ifndef RW_NO_POSTFIX
void operator--(int);	// Postfix operator
#endif
  // Increment/Decrement operators


};

FloatSymMat operator-(const FloatSymMat&);	// Unary minus
FloatSymMat operator+(const FloatSymMat&);	// Unary plus
FloatSymMat operator*(const FloatSymMat&, const FloatSymMat&);
FloatSymMat operator/(const FloatSymMat&, const FloatSymMat&);
FloatSymMat operator+(const FloatSymMat&, const FloatSymMat&);
FloatSymMat operator-(const FloatSymMat&, const FloatSymMat&);
FloatSymMat operator*(const FloatSymMat&, Float);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
inline FloatSymMat operator*(Float x, const FloatSymMat& A) { return A*x; }
#else
       FloatSymMat operator*(Float x, const FloatSymMat& A);
#endif
FloatSymMat operator/(const FloatSymMat& A, Float x);
FloatSymMat operator/(Float x, const FloatSymMat& A);
FloatSymMat operator+(const FloatSymMat&, Float);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
inline FloatSymMat operator+(Float x, const FloatSymMat& A) { return A+x; }
inline FloatSymMat operator-(const FloatSymMat& A, Float x) { return A+(-x); }
#else
       FloatSymMat operator+(Float x, const FloatSymMat& A);
       FloatSymMat operator-(const FloatSymMat& A, Float x);
#endif
FloatSymMat operator-(Float x, const FloatSymMat& A);
  // Arithmetic operators; Notice that operator* is an
  // element by element multiply, NOT a matrix multiply.

inline ostream& operator<<(ostream& s, const FloatSymMat& m)
		{m.printOn(s); return s;}
inline istream& operator>>(istream& s, FloatSymMat& m)
		{m.scanFrom(s); return s;}
  // I/O for humans

FloatSymMat transpose(const FloatSymMat&);
  // The transpose of the matrix. 

FloatVec product(const FloatSymMat& A, const FloatVec& x);
FloatVec product(const FloatVec& x, const FloatSymMat& A);
  // inner products

FloatSymMat toFloat(const DoubleSymMat& A);
FloatSymMat toSymMat( const FloatGenMat& A );
FloatSymMat upperToSymMat( const FloatGenMat& A );
FloatSymMat lowerToSymMat( const FloatGenMat& A );

FloatSymMat abs(const FloatSymMat& A);

inline Float minValue(const FloatSymMat& A) { return minValue(A.dataVec()); }
inline Float maxValue(const FloatSymMat& A) { return maxValue(A.dataVec()); }

inline FloatSymMat cos(const FloatSymMat& A)  { return A.apply(::cos); }
inline FloatSymMat cosh(const FloatSymMat& A) { return A.apply(::cosh); }
inline FloatSymMat exp(const FloatSymMat& A)  { return A.apply(::exp); } 
inline FloatSymMat log(const FloatSymMat& A)  { return A.apply(::log); }
inline FloatSymMat sin(const FloatSymMat& A)  { return A.apply(::sin); }
inline FloatSymMat sinh(const FloatSymMat& A) { return A.apply(::sinh); }
inline FloatSymMat sqrt(const FloatSymMat& A) { return A.apply(::sqrt); }
  // Math functions applicable to both real and complex types.

inline FloatSymMat acos(const FloatSymMat& A) { return A.apply(::acos); }
inline FloatSymMat asin(const FloatSymMat& A) { return A.apply(::asin); }
inline FloatSymMat atan(const FloatSymMat& A) { return A.apply(::atan); }
FloatSymMat atan2(const FloatSymMat&,const FloatSymMat&);
inline FloatSymMat ceil(const FloatSymMat& A) { return A.apply(::ceil); }
inline FloatSymMat floor(const FloatSymMat& A){ return A.apply(::floor); }
inline FloatSymMat log10(const FloatSymMat& A){ return A.apply(::log10); }
inline FloatSymMat tan(const FloatSymMat& A)  { return A.apply(::tan); }
inline FloatSymMat tanh(const FloatSymMat& A) { return A.apply(::tanh); }

/*
 * Inline functions
 */

inline Float FloatSymMat::val(int i, int j) const {
#ifdef RWBOUNDS_CHECK
return bcval(i,j);
#else
    if (i>j) {int temp=i; i=j; j=temp;}
    return vec(j*(j+1)/2+i);
#endif
}

inline Float FloatSymMat::set(int i, int j, Float x) {
    return bcset(i,j,x);
}

inline Float& FloatSymMat::ref(int i, int j) {
#ifdef RWBOUNDS_CHECK
return bcref(i,j);
#else
    if (i>j) {int temp=i; i=j; j=temp;}
    return vec(j*(j+1)/2+i);
#endif
}

inline Float& FloatSymMat::operator()(int i, int j) { return ref(i,j); }
inline Float FloatSymMat::operator()(int i, int j) const { return val(i,j); }


#endif

#ifndef __RWMATH_ERROR_H__
#define __RWMATH_ERROR_H__

/*
 * Error messages for Math.h++
 *
 * $Id: matherr.h,v 1.3 1993/08/17 17:11:05 alv Exp $
 *
 ****************************************************************************
 *
 *  Rogue Wave Software, Inc.
 *  P.O. Box 2328
 *  Corvallis, OR 97339
 *
 *  Copyright (C) 1990, 1991. This software is subject to copyright protection
 *  under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: matherr.h,v $
 * Revision 1.3  1993/08/17  17:11:05  alv
 * added ability to use custom RWBlocks
 *
 */

#include "rw/message.h"

extern const RWMsgId RWMATH_ARRDIM;
extern const RWMsgId RWMATH_BADPARITY;
extern const RWMsgId RWMATH_BADVERNO;
extern const RWMsgId RWMATH_CANTSOLVE;
extern const RWMsgId RWMATH_INDEX;
extern const RWMsgId RWMATH_LENPOS;
extern const RWMsgId RWMATH_MNMATCH;
extern const RWMsgId RWMATH_NMATCH;
extern const RWMsgId RWMATH_NOCONDITION;
extern const RWMsgId RWMATH_NPOINTS;
extern const RWMsgId RWMATH_NSQUARE;
extern const RWMsgId RWMATH_RWBDEALL;
extern const RWMsgId RWMATH_SHORTBLOCK;
extern const RWMsgId RWMATH_SLICEBEG;
extern const RWMsgId RWMATH_SLICEEND;
extern const RWMsgId RWMATH_STORAGE;
extern const RWMsgId RWMATH_STRCTOR;
extern const RWMsgId RWMATH_STRIDEPOS;
extern const RWMsgId RWMATH_STRIDENZ;
extern const RWMsgId RWMATH_STRIDEMAT;
extern const RWMsgId RWMATH_VNMATCH;
extern const RWMsgId RWMATH_ZEROSD;
extern const RWMsgId RWMATH_ZEROVEC;
extern const RWMsgId RWMATH_ZROOT;

#endif /* __RWMATH_ERROR_H__ */

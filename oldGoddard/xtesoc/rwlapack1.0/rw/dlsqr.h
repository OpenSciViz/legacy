#ifndef __RWDLSQR_H__
#define __RWDLSQR_H__
/*
 * DoubleLeastSqQR:  solve least square QR problem with orthogonal decomposition
 *
 * Generated from template $Id: xlsqr.h,v 1.3 1993/07/05 19:19:33 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 * Uses a complete orthogonal decomposition to solve the linear
 * least squares problem.  The solution is the minimum norm solution
 * which minimizes the residual.
 *
 * When building a factorization using either a constructor or one
 * of the factor functions, you can supply a tolerance parameter.
 * Entries along the diagonal of the R factor of the QR decomposition
 * less than this tolerance are treated as zero.  Using this can
 * prevent insignificant entries of R from corrupting your solution.
 *
 * The factor functions and constructors taking a QR decomposition
 * are useful if you'd like to examine the diagonal entries of R
 * for yourself and decide what tolerance to use.
 */

#include "rw/dco.h"

class DoubleLeastSqQR : public DoubleCODecomp {
public:
  DoubleLeastSqQR();
  DoubleLeastSqQR(const DoubleGenMat& A, double tol=0);
  DoubleLeastSqQR(const DoubleQRDecomp& A, double tol=0);
  DoubleLeastSqQR(const DoubleCODecomp& A);
//void     factor(const DoubleGenMat& A, double tol=0);
//void     factor(const DoubleQRDecomp& A, double tol=0);
//void     factor(const DoubleCODecomp& A);
//unsigned rows() const {return decomp_.rows();}
//unsigned cols() const {return decomp_.cols();}
//unsigned rank() const {return decomp_.rank();}
  DoubleVec   residual(const DoubleVec& data) const;
  Double      residualNorm(const DoubleVec& data) const;
  DoubleVec   solve(const DoubleVec& data) const;
};

inline DoubleVec solve(   const DoubleLeastSqQR& A, const DoubleVec& b) {return A.solve(b);}
inline DoubleVec residual(const DoubleLeastSqQR& A, const DoubleVec& b) {return A.residual(b);}
inline Double    residualNorm(const DoubleLeastSqQR& A, const DoubleVec& b) {return A.residualNorm(b);}

#endif

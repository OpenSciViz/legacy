#ifndef __RWCHEIGSRV_H__
#define __RWCHEIGSRV_H__

/*
 * DComplexHermEigServer     - Abstract base for symmetric/Hermitian eigenservers
 * DComplexHermQREigServer   - QR method, this is the default server
 * DComplexHermPDQREigServer - QR method for positive definite matrices
 * DComplexHermRFQREigServer - Root free variant of QR (can't compute eigenvectors)
 * DComplexHermBisEigServer  - Bisection method
 *
 * Generated from template $Id: xseigsrv.h,v 1.2 1993/07/05 19:19:33 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * Symmetric/Hermitian eigen-decomposition servers.  This classes
 * represent various algorithms for computing spectral
 * factorizations.  They are useful for people who demand more
 * control over the computation of eigen values/vectors than
 * is provided by the constructors in DComplexHermEigDecomp.
 *
 */ 

#include "rw/lapkdefs.h"
#include "rw/cgenmat.h"
#include "rw/cvec.h"
#include "rw/chermeig.h"  
#include "rw/dsymeig.h"
class DComplexHermTriDiagDecomp;

class DComplexHermEigServer {
protected:
  static unsigned&  n(DComplexHermEigDecomp& x)           {return x.n;}
  static DoubleVec&    lambda(DComplexHermEigDecomp& x)      {return x.lambda;}
  static DComplexGenMat& P(DComplexHermEigDecomp& x)           {return x.P;}
  static RWBoolean& computedAll(DComplexHermEigDecomp& x) {return x.computedAll;}
  static RWBoolean& accurate(DComplexHermEigDecomp& x)    {return x.accurate;}
  static unsigned&  n(DoubleSymEigDecomp& x)           {return x.n;}
  static DoubleVec&    lambda(DoubleSymEigDecomp& x)      {return x.lambda;}
  static DoubleGenMat& P(DoubleSymEigDecomp& x)           {return x.P;}
  static RWBoolean& computedAll(DoubleSymEigDecomp& x) {return x.computedAll;}
  static RWBoolean& accurate(DoubleSymEigDecomp& x)    {return x.accurate;}
    // These functions provide access to the guts of the EigDecomp
    // object for the server.  This way we can declare more servers
    // without having to make them all friends of EigDecomp.

public:
  virtual DComplexHermEigDecomp operator()(const DComplexHermMat&);
  virtual DComplexHermEigDecomp operator()(const DComplexHermBandMat&);
  virtual DoubleSymEigDecomp   decompose(const DComplexHermTriDiagDecomp&) =0;
    // The op() which take matrices work by first building a 
    // tri-diagonal decomposition, computing its eigen-decomposition,
    // and then transforming to the original matrix.  The function
    // to decompose a tri diagonal matrix is called decompose() rather
    // than op() to allow its redefinition in subclasses without hiding
    // the op() functions.

  virtual RWBoolean computeEigenVectors() const =0;
    // Is this server configured to compute eigenvectors as well as
    // eigenvalues?  This is nice to know when forming the tri-diagonal
    // decomposition of a matrix, so we can decide whether or not
    // to keep the orthogonal matrix part.
};

class DComplexHermQREigServer : public DComplexHermEigServer {
private:
  RWBoolean computeVecs_;
public:
  DComplexHermQREigServer(RWBoolean computeVecs=TRUE);
  virtual DoubleSymEigDecomp   decompose(const DComplexHermTriDiagDecomp&);
  virtual RWBoolean computeEigenVectors() const;
  void              computeEigenVectors(RWBoolean);
};
  
class DComplexHermPDQREigServer : public DComplexHermEigServer {
private:
  RWBoolean computeVecs_;
public:
  DComplexHermPDQREigServer(RWBoolean computeVecs=TRUE);
  virtual DoubleSymEigDecomp   decompose(const DComplexHermTriDiagDecomp&);
  virtual RWBoolean computeEigenVectors() const;
  void              computeEigenVectors(RWBoolean);
};
    
class DComplexHermRFQREigServer : public DComplexHermEigServer {
public:
  DComplexHermRFQREigServer();
  virtual DoubleSymEigDecomp   decompose(const DComplexHermTriDiagDecomp&);
  virtual RWBoolean computeEigenVectors() const;
};
                
class DComplexHermSomeEigServer  : public DComplexHermEigServer {
private:
  RWBoolean computeVecs_;
  Double       tolerance_;
  RWSlice   range;  
public:
  DComplexHermSomeEigServer(RWBoolean computeVecs=TRUE);
  virtual DoubleSymEigDecomp   decompose(const DComplexHermTriDiagDecomp&);
  Double               setTolerance(Double);
  RWSlice           setRange(const RWSlice&);
  virtual RWBoolean computeEigenVectors() const;
  void              computeEigenVectors(RWBoolean);
};

class DComplexHermRangeEigServer  : public DComplexHermEigServer {
private:
  RWBoolean computeVecs_;
  Double       tolerance_;
  Double       small_;
  Double       large_;
public:
  DComplexHermRangeEigServer(RWBoolean computeVecs=TRUE);
  virtual DoubleSymEigDecomp   decompose(const DComplexHermTriDiagDecomp&);
  Double               setTolerance(Double);
  void              setRange(Double,Double);
  Double               setSmall(Double);
  Double               setLarge(Double);
  virtual RWBoolean computeEigenVectors() const;
  void              computeEigenVectors(RWBoolean);
};

#endif
 

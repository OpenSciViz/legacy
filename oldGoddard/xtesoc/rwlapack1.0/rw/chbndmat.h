#ifndef __RWCHBNDMAT_H__
#define __RWCHBNDMAT_H__
/*
 * DComplexHermBandMat - An Hermitian banded matrix of DComplexs
 *
 * Stores an Hermitian banded matrix.  All entries farther above or below    
 * the diagonal than the upper bandwidth
 * are defined to be zero.  The total bandwidth is twice the 
 * upperBandwidth, plus one for the diagonal.  Only
 * the elements which may be non-zero are stored.  
 *
 * The top half of the matrix is stored column by column.  Each
 * column takes up 
 * <upperBandwidth+1> entries, some of these entries near the top
 * left corner are not used.
 *
 * Generated from template $Id: xmat.h,v 1.4 1993/10/11 21:48:17 alv Exp $
 *
 * Bounds checking is enabled by setting the preprocessor
 * symbol RWBOUNDS_CHECK.  This is strongly encouraged.
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

#include "rw/cvec.h"
#include "rw/cref.h"

extern DComplex rwDComplexZero;	// This constant is sometimes returned by reference

class DoubleSymBandMat;
class DComplexGenMat;
class DComplexBandMat;
class DoubleSymBandMat;

class DComplexHermBandMat {
private:

DComplexVec vec;
unsigned n;
unsigned bandu;		// The upper bandwidth
  // The data which define the matrix

  void zeroUnusedEntries();  // Initialize entries in the data vector which are not used to zero

public:
DComplexHermBandMat();
DComplexHermBandMat( const DComplexHermBandMat& );
DComplexHermBandMat(unsigned n, unsigned nAgain, unsigned halfWidth);
DComplexHermBandMat(const DComplexVec& data, unsigned n, unsigned nAgain, unsigned halfWidth);
DComplexHermBandMat(const DoubleSymBandMat& re);
~DComplexHermBandMat();
  // Constructors, conversion functions, and a destructor.

static const char *className;
  // For use in error messages

inline unsigned rows() const	   { return n; }
inline unsigned cols() const	   { return n; }
inline unsigned bandwidth() const      { return 2*bandu+1; }
inline unsigned halfBandwidth() const  { return bandu; }
inline unsigned lowerBandwidth() const { return bandu; }
inline unsigned upperBandwidth() const { return bandu; }
  // Member functions to access the shape of the matrix

inline DComplex  val(int i, int j) const;
       DComplex  bcval(int i, int j) const;
inline DComplex  set(int i, int j, DComplex x);
       DComplex  bcset(int i, int j, DComplex x);
inline ROCJDComplexRef operator()(int i, int j);
inline ROCJDComplexRef ref(int i, int j);
       ROCJDComplexRef bcref(int i, int j);
inline DComplex  operator()(int i, int j) const; // { return val(i,j); }
  // Access and/or change elements of the matrix.
  // The versions that begin with the letters bc do bounds
  // checking.  The others do bounds checking only if the
  // preprocessor symbol RWBOUNDS_CHECK is defined.

DComplexHermBandMat leadingSubmatrix(int order);
  // Access to submatrices.
  // The leading submatrix of order k is the kxk matrix in the upper
  // left.
  // Access to diagonals.  The same conventions as for
  // element access apply (with respect to bcxxx and xxx names).

DComplexHermBandMat& operator=(const DComplexHermBandMat& m);
DComplexHermBandMat& reference(DComplexHermBandMat& m);
  // reference() makes an alias, operator= makes a copy.


void zero()        { vec=DComplex(0,0); }
  // Sets all elements of the matrix equal to zero.


DComplexHermBandMat copy() const;
DComplexHermBandMat deepCopy() const		{ return copy(); }
void deepenShallowCopy()        { vec.deepenShallowCopy(); }
  // copy() returns a DComplexHermBandMat with its own copy of the data.
  // deepenShallowCopy makes sure the data is not shared by another
  // vector or matrix.

DComplexVec dataVec() 		{ return vec; }
const DComplexVec& dataVec() const	{ return vec; }
DComplex* data() 			{ return vec.data(); }
const DComplex* data() const 	{ return vec.data(); }
  // Access to the raw data.

void resize(unsigned m, unsigned n);
void resize(unsigned n, unsigned nAgain, unsigned halfWidth);
  // Resize the matrix.  New elements are set to zero.

void makeDiagonalReal();            // Set imaginary part of diagonal to zero

void scanFrom(istream&);      		// Human readable
void printOn(ostream&) const;
void restoreFrom(RWvistream&);     	// Internal ASCII or binary code
void saveOn(RWvostream&) const;
void restoreFrom(RWFile&);
void saveOn(RWFile&) const;
unsigned binaryStoreSize() const;
  // Input/output member functions
  // Whether ASCII or binary formatting is used with readFrom and
  // storeOn is determined by the type of rwstream passed.

/*
 * The operator()() member functions are declared with ref(),
 * since the declarations look the same.
 *
 * The operator=() assignment operators are declared with reference(),
 * since the functions are so closely related.
 */

RWBoolean operator==(const DComplexHermBandMat& X);
RWBoolean operator!=(const DComplexHermBandMat& X)  { return !(operator==(X)); }
  // Two matrices are considered equal if their shapes are identical and
  // all their components are identical.

DComplexHermBandMat& operator+=(const DComplexHermBandMat& m);
DComplexHermBandMat& operator-=(const DComplexHermBandMat& m);
DComplexHermBandMat& operator*=(const DComplexHermBandMat& m);
DComplexHermBandMat& operator*=(DComplex);
DComplexHermBandMat& operator/=(const DComplexHermBandMat& m);
DComplexHermBandMat& operator/=(DComplex);
  // assignment operators.  self must be same size as m.


};

DComplexHermBandMat operator-(const DComplexHermBandMat&);	// Unary minus
DComplexHermBandMat operator+(const DComplexHermBandMat&);	// Unary plus
DComplexHermBandMat operator*(const DComplexHermBandMat&, const DComplexHermBandMat&);
DComplexHermBandMat operator/(const DComplexHermBandMat&, const DComplexHermBandMat&);
DComplexHermBandMat operator+(const DComplexHermBandMat&, const DComplexHermBandMat&);
DComplexHermBandMat operator-(const DComplexHermBandMat&, const DComplexHermBandMat&);
DComplexHermBandMat operator*(const DComplexHermBandMat&, DComplex);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
inline DComplexHermBandMat operator*(DComplex x, const DComplexHermBandMat& A) { return A*x; }
#else
       DComplexHermBandMat operator*(DComplex x, const DComplexHermBandMat& A);
#endif
DComplexHermBandMat operator/(const DComplexHermBandMat& A, DComplex x);
  // Arithmetic operators; Notice that operator* is an
  // element by element multiply, NOT a matrix multiply.

inline ostream& operator<<(ostream& s, const DComplexHermBandMat& m)
		{m.printOn(s); return s;}
inline istream& operator>>(istream& s, DComplexHermBandMat& m)
		{m.scanFrom(s); return s;}
  // I/O for humans

DComplexHermBandMat transpose(const DComplexHermBandMat&);
  // The transpose of the matrix. 

DComplexVec product(const DComplexHermBandMat& A, const DComplexVec& x);
DComplexVec product(const DComplexVec& x, const DComplexHermBandMat& A);
  // inner products

DComplexHermBandMat toHermBandMat( const DComplexGenMat& A, unsigned halfband );
DComplexHermBandMat toHermBandMat( const DComplexBandMat& A );

DoubleSymBandMat abs(const DComplexHermBandMat& A);
DComplexHermBandMat conj(const DComplexHermBandMat& A);
DoubleSymBandMat real(const DComplexHermBandMat& A);
DoubleSymBandMat norm(const DComplexHermBandMat& A);
 /* imag and arg not defined for HermBandMat */



/*
 * Inline functions
 */

inline DComplex DComplexHermBandMat::val(int i, int j) const {
    return bcval(i,j);
}

inline DComplex DComplexHermBandMat::set(int i, int j, DComplex x) {
    return bcset(i,j,x);
}

inline ROCJDComplexRef DComplexHermBandMat::ref(int i, int j) {
    return bcref(i,j);
}

inline ROCJDComplexRef DComplexHermBandMat::operator()(int i, int j) { return ref(i,j); }
inline DComplex DComplexHermBandMat::operator()(int i, int j) const { return val(i,j); }


#endif

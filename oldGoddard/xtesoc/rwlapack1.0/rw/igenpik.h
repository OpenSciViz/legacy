#ifndef __IGENPIK_H__
#define __IGENPIK_H__

/*
 * Declarations for IntGenMatPick --- picks elements out of a IntGenMat
 *
 * Generated from template $Id: genpik.htm,v 1.3 1993/03/22 15:51:31 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311        FAX: (503) 757-7350
 *
 * Copyright (C) 1988, 1989, 1990, 1991.  This software is subject to copyright
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/ivec.h"

/*
 * The IntGenMatPick class allows selected elements to be addressed.
 * There are no public constructors.
 */

class IntGenMatPick {
friend class IntGenMat;
private:
                        IntGenMatPick(IntGenMat&,const IntVec&,const IntVec&);
  IntGenMat&             V;
  const IntVec     rowpick;
  const IntVec     colpick;
protected:         
  void                  assertElements() const;
  void                  lengthCheck(unsigned,unsigned) const;
public:            
  void                  operator=(const IntGenMat&);
  void                  operator=(const IntGenMatPick&);
  void                  operator=(int);
                   
  unsigned              rows() const    { return rowpick.length(); }
  unsigned              cols() const    { return colpick.length(); }
};

/************************************************
 *                                              *
 *              I N L I N E S                   *
 *                                              *
 ************************************************/

inline
IntGenMatPick::IntGenMatPick(IntGenMat& v, const IntVec& x, const IntVec& y) :
  V(v), rowpick(x), colpick(y)
{
#ifdef RWBOUNDS_CHECK
  assertElements();
#endif
}

#endif /*__IGENPIK_H__*/

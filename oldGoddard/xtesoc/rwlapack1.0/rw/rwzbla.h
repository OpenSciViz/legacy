#ifndef __RWZBLA_H__
#define __RWZBLA_H__

/*
 * DComplex precision (double complex) Basic Linear Algebra prototypes.
 *
 * $Header: /users/rcs/mathrw/rwzbla.h,v 1.1 1993/01/23 00:08:44 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * Copyright (C) 1991.  This software is subject to copyright
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: rwzbla.h,v $
 * Revision 1.1  1993/01/23  00:08:44  alv
 * Initial revision
 *
 * 
 *    Rev 1.2   17 Oct 1991 09:13:04   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.0   27 Sep 1991 11:49:50   keffer
 * Initial revision.
 */

#ifndef __MATHDEFS_H__
#  include "rw/mathdefs.h"		/* Looking for "FDecl" and "RW_KR_ONLY" */
#endif
#include "rw/dcomplex.h"		/* Get declarations for DComplex */

#ifdef __cplusplus
extern "C" {
# define P_(s) s
#else
#  ifdef RW_KR_ONLY
#    define P_(s) ()
#  else
#    define P_(s) s
#  endif
#endif

void   FDecl rwzcopy P_((unsigned n, DComplex* restrict x, int incx, const DComplex* restrict y, int incy));
void   FDecl rwzset  P_((unsigned n, DComplex* restrict x, int incx, const DComplex* restrict scalar));
int    FDecl rwzsame P_((unsigned n, const DComplex* restrict x, int incx, const DComplex* restrict y, int incy));

void   FDecl rwzdot P_((unsigned n, const DComplex* restrict x, int incx, const DComplex* restrict y, int incy, DComplex* result));
void   FDecl rwzcdot P_((unsigned n, const DComplex* restrict x, int incx, const DComplex* restrict y, int incy, DComplex* result));

/* For implementing "Vector& Vector::operator+=(const Vector&)" functionality: */
void   FDecl rwz_aplvv P_((unsigned n, DComplex* restrict x, int incx, const DComplex* restrict y, int incy));
void   FDecl rwz_amivv P_((unsigned n, DComplex* restrict x, int incx, const DComplex* restrict y, int incy));
void   FDecl rwz_amuvv P_((unsigned n, DComplex* restrict x, int incx, const DComplex* restrict y, int incy));
void   FDecl rwz_advvv P_((unsigned n, DComplex* restrict x, int incx, const DComplex* restrict y, int incy));

/* For implementing "Vector& Vector::operator+=(type)" functionality: */
void   FDecl rwz_aplvs P_((unsigned n, DComplex* restrict x, int incx, const DComplex* restrict scalar));
void   FDecl rwz_amuvs P_((unsigned n, DComplex* restrict x, int incx, const DComplex* restrict scalar));
void   FDecl rwz_advvs P_((unsigned n, DComplex* restrict x, int incx, const DComplex* restrict scalar));

/* For implementing "Vector operator+(const Vector&, const Vector&)" functionality: */
void   FDecl rwz_plvv P_((unsigned n, DComplex* restrict z, const DComplex* restrict x, int incx, const DComplex* restrict y, int incy));
void   FDecl rwz_mivv P_((unsigned n, DComplex* restrict z, const DComplex* restrict x, int incx, const DComplex* restrict y, int incy));
void   FDecl rwz_muvv P_((unsigned n, DComplex* restrict z, const DComplex* restrict x, int incx, const DComplex* restrict y, int incy));
void   FDecl rwz_dvvv P_((unsigned n, DComplex* restrict z, const DComplex* restrict x, int incx, const DComplex* restrict y, int incy));

/* For implementing "Vector operator+(const Vector&, type)" functionality: */
void   FDecl rwz_plvs P_((unsigned n, DComplex* restrict z, const DComplex* restrict x, int incx, const DComplex* restrict scalar));
void   FDecl rwz_muvs P_((unsigned n, DComplex* restrict z, const DComplex* restrict x, int incx, const DComplex* restrict scalar));
void   FDecl rwz_dvvs P_((unsigned n, DComplex* restrict z, const DComplex* restrict x, int incx, const DComplex* restrict scalar));

/* For implementing "Vector operator+(type, const Vector&)" functionality: */
void   FDecl rwz_misv P_((unsigned n, DComplex* restrict z, const DComplex* scalar, const DComplex* restrict x, int incx));
void   FDecl rwz_dvsv P_((unsigned n, DComplex* restrict z, const DComplex* scalar, const DComplex* restrict x, int incx));

#undef P_

#ifdef __cplusplus
}
#endif

#endif


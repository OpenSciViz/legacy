#ifndef __RWLAPKDEFS_H__
#define __RWLAPKDEFS_H__

/*
 * Lapack.h++ common definitions
 *
 * $Header: /users/rcs/lapkrw/lapkdefs.h,v 1.5 1993/10/15 03:57:35 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * (503 754-3010
 *
 * (c) Copyright 1991, Rogue Wave Software, Inc.
 * ALL RIGHTS RESERVED
 *
 * The software and information contained herein are proprietary to, and
 * comprise valuable trade secrets of, Rogue Wave Software, Inc., which
 * intends to preserve as trade secrets such software and information.
 * This software is furnished pursuant to a written license agreement and
 * may be used, copied, transmitted, and stored only in accordance with
 * the terms of such license and with the inclusion of the above copyright
 * notice.  This software and information or any other copies thereof may
 * not be provided or otherwise made available to any other person.
 *
 * Notwithstanding any other lease or license that may pertain to, or
 * accompany the delivery of, this computer software and information, the
 * rights of the Government regarding its use, reproduction and disclosure
 * are as set forth in Section 52.227-19 of the FARS Computer
 * Software-Restricted Rights clause.
 * 
 * Use, duplication, or disclosure by the Government is subject to
 * restrictions as set forth in subparagraph (c)(1)(ii) of the Rights in
 * Technical Data and Computer Software clause at DFARS 52.227-7013.
 * 
 * This computer software and information is distributed with "restricted
 * rights."  Use, duplication or disclosure is subject to restrictions as
 * set forth in NASA FAR SUP 18-52.227-79 (April 1985) "Commercial
 * Computer Software-Restricted Rights (April 1985)."  If the Clause at
 * 18-52.227-74 "Rights in Data General" is specified in the contract,
 * then the "Alternate III" clause applies.
 *
 ***************************************************************************
 *
 * $Log: lapkdefs.h,v $
 * Revision 1.5  1993/10/15  03:57:35  alv
 * added copyright notice
 *
 */

#define RWLAPACK 0x0100      /* Version number */

#include "rw/mathdefs.h"

/*
 * If we are running with MSDOS, then the blocked algorithms are not
 * going to help much, and will drastically increase code size.
 */
#if defined(__MSDOS__)
#define RW_USE_UNBLOCKED 1
#endif

#endif

#ifndef __RWDSTATS_H__
#define __RWDSTATS_H__

/*
 * Declarations for global statistics functions.
 *
 * $Header: /users/rcs/mathrw/dstats.h,v 1.1 1993/01/23 00:08:34 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * Copyright (C) 1988, 1989, 1990, 1991.  This software is subject to copyright
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: dstats.h,v $
 * Revision 1.1  1993/01/23  00:08:34  alv
 * Initial revision
 *
 * 
 *    Rev 1.4   17 Oct 1991 09:13:02   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.2   01 Sep 1991 17:40:34   keffer
 * Added new global function rwEpslon(double).  Returns roundoff estimate.
 * 
 *    Rev 1.1   24 Jul 1991 13:00:58   keffer
 * Added pvcs keywords
 *
 */

#include "rw/dvec.h"

double  		beta          	(double, double);
double  		binomialPF    	(unsigned, unsigned, double);
double  		exponentialPF  	(double, double);
double  		factorial     	(unsigned);
double  		gaussianPF    	(double, double, double);
double    		kurtosis      	(const DoubleVec&);
double  		logGamma      	(double); 
double  		lorentzianPF  	(double, double, double);
double  		poissonPF     	(unsigned, double);
double			rwEpslon	(double);
DoubleVec 		sort 		(const DoubleVec&);
double    		skewness      	(const DoubleVec&);
    
#endif /*__RWDSTATS_H__*/

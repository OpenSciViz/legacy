#ifndef __RWRANDGAMA_H__
#define __RWRANDGAMA_H__

/*
 * Declarations for RandGamma: Gamma distributed random numbers.
 *
 * $Header: /users/rcs/mathrw/randgama.h,v 1.1 1993/01/23 00:08:40 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-2311	FAX: (503) 757-7350
 *
 * Copyright (C) 1988, 1989, 1990, 1991.  This software is subject to copyright
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: randgama.h,v $
 * Revision 1.1  1993/01/23  00:08:40  alv
 * Initial revision
 *
 * 
 *    Rev 1.3   17 Oct 1991 09:12:54   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.1   24 Jul 1991 13:01:08   keffer
 * Added pvcs keywords
 *
 */

#include "rw/randunif.h"

class RandGamma : public RandUniform {
protected:
  unsigned 		orderOfDistribution;
public:
  RandGamma(unsigned i)		: RandUniform()  {orderOfDistribution = i;}
  RandGamma(unsigned n, unsigned i)	: RandUniform(n) {orderOfDistribution = i;}
 
  unsigned 		order() const {return orderOfDistribution;}

  double    		randValue();           // Return a random number 
  void    		randValue(double*, unsigned n);	// Return an array of random numbers.
  DoubleVec 		randValue(unsigned n); // Return a vector of n random numbers 
  DoubleGenMat 		randValue(unsigned nr, unsigned nc);  // Return a matrix of random numbers
};
  
#endif /*__RWRANDGAMA_H__*/

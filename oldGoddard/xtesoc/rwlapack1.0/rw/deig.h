#ifndef __RWDEIG_H__
#define __RWDEIG_H__

/*
 * DoubleEigDecomp - Spectral factorization of a symmetric/Hermitian matrix
 *
 * Generated from template $Id: xeig.h,v 1.2 1993/07/05 19:19:33 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * This class represents the eigenvalues/vectors of a non-symmetric
 * matrix.
 *
 */ 

#include "rw/lapkdefs.h"
#include "rw/dgenmat.h"
#include "rw/dvec.h"
#include "rw/cgenmat.h"
#include "rw/cvec.h"

class DoubleEigDecomp {
private:
  unsigned  n;           // dimension of the matrix
  DComplexVec    lambda;      // computed eigenvalues (length may be < n)
  DoubleGenMat P;           // columns contain left eigenvectors
  DoubleGenMat Q;           // columns contain right eigenvectors
  RWBoolean computedAll; // Did I compute everything the server tried to compute?
  RWBoolean accurate;    // Were all results as accurate as possible?

public:
  DoubleEigDecomp();
  DoubleEigDecomp(const DoubleEigDecomp&);
  DoubleEigDecomp(const DoubleGenMat&, RWBoolean computeVecs=TRUE);
  void factor(const DoubleGenMat&, RWBoolean computeVecs=TRUE);

  void      operator=(const DoubleEigDecomp&);

  unsigned        cols()           const {return n;}
  DComplex             eigenValue(int)  const;
  const DComplexVec    eigenValues()    const {return lambda;}
  const DComplexVec    leftEigenVector(int)  const;
  const DComplexGenMat leftEigenVectors()    const;
  const DComplexVec    rightEigenVector(int) const;
  const DComplexGenMat rightEigenVectors()   const;
  RWBoolean       good()           const; // True if everything went perfectly
  RWBoolean       inaccurate()     const; // Either failure, or some things are not fully accurate
  RWBoolean       fail()           const; // An eigenvalue or vector wasn't computed
  unsigned        numEigenValues() const {return lambda.length();}
  unsigned        numLeftEigenVectors()  const {return P.cols();}
  unsigned        numRightEigenVectors() const {return Q.cols();}
  unsigned        rows()           const {return n;}
               
  friend class DoubleEigServer;
  friend class DComplexEigServer;
  friend class DComplexEigDecomp;
};

#endif


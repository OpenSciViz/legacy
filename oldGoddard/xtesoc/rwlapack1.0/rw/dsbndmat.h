#ifndef __RWDSBNDMAT_H__
#define __RWDSBNDMAT_H__
/*
 * DoubleSymBandMat - A symmetric banded matrix of Doubles
 *
 * Stores a symmetric banded matrix.  All entries farther above or below    
 * the diagonal that the upper bandwidth
 * are defined to be zero.  The total bandwidth is twice the 
 * upperBandwidth, plus one for the diagonal.  Only
 * the elements which may be non-zero are stored.
 *
 * The top half of the matrix is stored column by column.  Each
 * column takes up 
 * <upperBandwidth+1> entries, some of these entries near the top left
 * corner are not used.
 *
 * Generated from template $Id: xmat.h,v 1.4 1993/10/11 21:48:17 alv Exp $
 *
 * Bounds checking is enabled by setting the preprocessor
 * symbol RWBOUNDS_CHECK.  This is strongly encouraged.
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

#include "rw/dvec.h"
#include "rw/dref.h"

extern Double rwDoubleZero;	// This constant is sometimes returned by reference

class FloatSymBandMat;
class DoubleGenMat;
class DoubleBandMat;

class DoubleSymBandMat {
private:

DoubleVec vec;
unsigned n;
unsigned bandu;		// The upper bandwidth
  // The data which define the matrix

  void zeroUnusedEntries();  // Initialize entries in the data vector which are not used to zero

public:
DoubleSymBandMat();
DoubleSymBandMat( const DoubleSymBandMat& );
DoubleSymBandMat(unsigned n, unsigned nAgain, unsigned halfWidth);
DoubleSymBandMat(const DoubleVec& data, unsigned n, unsigned nAgain, unsigned halfWidth);
DoubleSymBandMat(const FloatSymBandMat&);
~DoubleSymBandMat();
  // Constructors, conversion functions, and a destructor.

static const char *className;
  // For use in error messages

inline unsigned rows() const	   { return n; }
inline unsigned cols() const	   { return n; }
inline unsigned bandwidth() const      { return 2*bandu+1; }
inline unsigned halfBandwidth() const  { return bandu; }
inline unsigned lowerBandwidth() const { return bandu; }
inline unsigned upperBandwidth() const { return bandu; }
  // Member functions to access the shape of the matrix

inline Double  val(int i, int j) const;
       Double  bcval(int i, int j) const;
inline Double  set(int i, int j, Double x);
       Double  bcset(int i, int j, Double x);
inline RODoubleRef operator()(int i, int j);
inline RODoubleRef ref(int i, int j);
       RODoubleRef bcref(int i, int j);
inline Double  operator()(int i, int j) const; // { return val(i,j); }
  // Access and/or change elements of the matrix.
  // The versions that begin with the letters bc do bounds
  // checking.  The others do bounds checking only if the
  // preprocessor symbol RWBOUNDS_CHECK is defined.

DoubleSymBandMat leadingSubmatrix(int order);
  // Access to submatrices.
  // The leading submatrix of order k is the kxk matrix in the upper
  // left.
inline DoubleVec diagonal(int =0) const;
       DoubleVec bcdiagonal(int =0) const;
  // Access to diagonals.  The same conventions as for
  // element access apply (with respect to bcxxx and xxx names).

DoubleSymBandMat& operator=(const DoubleSymBandMat& m);
DoubleSymBandMat& reference(DoubleSymBandMat& m);
  // reference() makes an alias, operator= makes a copy.


void zero()        { vec=Double(0); }
  // Sets all elements of the matrix equal to zero.


DoubleSymBandMat copy() const;
DoubleSymBandMat deepCopy() const		{ return copy(); }
void deepenShallowCopy()        { vec.deepenShallowCopy(); }
  // copy() returns a DoubleSymBandMat with its own copy of the data.
  // deepenShallowCopy makes sure the data is not shared by another
  // vector or matrix.

DoubleVec dataVec() 		{ return vec; }
const DoubleVec& dataVec() const	{ return vec; }
Double* data() 			{ return vec.data(); }
const Double* data() const 	{ return vec.data(); }
  // Access to the raw data.

void resize(unsigned m, unsigned n);
void resize(unsigned n, unsigned nAgain, unsigned halfWidth);
  // Resize the matrix.  New elements are set to zero.


void scanFrom(istream&);      		// Human readable
void printOn(ostream&) const;
void restoreFrom(RWvistream&);     	// Internal ASCII or binary code
void saveOn(RWvostream&) const;
void restoreFrom(RWFile&);
void saveOn(RWFile&) const;
unsigned binaryStoreSize() const;
  // Input/output member functions
  // Whether ASCII or binary formatting is used with readFrom and
  // storeOn is determined by the type of rwstream passed.

/*
 * The operator()() member functions are declared with ref(),
 * since the declarations look the same.
 *
 * The operator=() assignment operators are declared with reference(),
 * since the functions are so closely related.
 */

RWBoolean operator==(const DoubleSymBandMat& X);
RWBoolean operator!=(const DoubleSymBandMat& X)  { return !(operator==(X)); }
  // Two matrices are considered equal if their shapes are identical and
  // all their components are identical.

DoubleSymBandMat& operator+=(const DoubleSymBandMat& m);
DoubleSymBandMat& operator-=(const DoubleSymBandMat& m);
DoubleSymBandMat& operator*=(const DoubleSymBandMat& m);
DoubleSymBandMat& operator*=(Double);
DoubleSymBandMat& operator/=(const DoubleSymBandMat& m);
DoubleSymBandMat& operator/=(Double);
  // assignment operators.  self must be same size as m.


};

DoubleSymBandMat operator-(const DoubleSymBandMat&);	// Unary minus
DoubleSymBandMat operator+(const DoubleSymBandMat&);	// Unary plus
DoubleSymBandMat operator*(const DoubleSymBandMat&, const DoubleSymBandMat&);
DoubleSymBandMat operator/(const DoubleSymBandMat&, const DoubleSymBandMat&);
DoubleSymBandMat operator+(const DoubleSymBandMat&, const DoubleSymBandMat&);
DoubleSymBandMat operator-(const DoubleSymBandMat&, const DoubleSymBandMat&);
DoubleSymBandMat operator*(const DoubleSymBandMat&, Double);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
inline DoubleSymBandMat operator*(Double x, const DoubleSymBandMat& A) { return A*x; }
#else
       DoubleSymBandMat operator*(Double x, const DoubleSymBandMat& A);
#endif
DoubleSymBandMat operator/(const DoubleSymBandMat& A, Double x);
  // Arithmetic operators; Notice that operator* is an
  // element by element multiply, NOT a matrix multiply.

inline ostream& operator<<(ostream& s, const DoubleSymBandMat& m)
		{m.printOn(s); return s;}
inline istream& operator>>(istream& s, DoubleSymBandMat& m)
		{m.scanFrom(s); return s;}
  // I/O for humans

DoubleSymBandMat transpose(const DoubleSymBandMat&);
  // The transpose of the matrix. 

DoubleVec product(const DoubleSymBandMat& A, const DoubleVec& x);
DoubleVec product(const DoubleVec& x, const DoubleSymBandMat& A);
  // inner products

DoubleSymBandMat toSymBandMat( const DoubleGenMat& A, unsigned halfband );
DoubleSymBandMat toSymBandMat( const DoubleBandMat& A );

DoubleSymBandMat abs(const DoubleSymBandMat& A);

inline Double minValue(const DoubleSymBandMat& A) { return minValue(A.dataVec()); }
inline Double maxValue(const DoubleSymBandMat& A) { return maxValue(A.dataVec()); }


/*
 * Inline functions
 */

inline Double DoubleSymBandMat::val(int i, int j) const {
    return bcval(i,j);
}

inline Double DoubleSymBandMat::set(int i, int j, Double x) {
    return bcset(i,j,x);
}

inline RODoubleRef DoubleSymBandMat::ref(int i, int j) {
    return bcref(i,j);
}

inline RODoubleRef DoubleSymBandMat::operator()(int i, int j) { return ref(i,j); }
inline Double DoubleSymBandMat::operator()(int i, int j) const { return val(i,j); }

inline DoubleVec DoubleSymBandMat::diagonal(int i) const {
#if defined(RWBOUNDS_CHECK)
    return bcdiagonal(i);
#else
    int iabs = (i>=0) ? i : -i;
    return vec.slice( (iabs+1)*(bandu), n-iabs, bandu+1 );
#endif
}

#endif

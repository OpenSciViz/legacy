#ifndef __RWDEIGSRV_H__
#define __RWDEIGSRV_H__

/*
 * DoubleEigServer     - Abstract base for eigenservers
 * DoubleSchurEigServer- Schur decomposition method, this is the default server
 * DoubleHessEigServer - Don't generate Schur decomp - get eigenvectors by inverse iteration
 *
 * Generated from template $Id: xeigsrv.h,v 1.2 1993/07/05 19:19:33 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 */ 

#include "rw/lapkdefs.h"
#include "rw/dgenmat.h"
#include "rw/dvec.h"
#include "rw/ivec.h"
#include "rw/deig.h"  
class DoubleBalanceDecomp;
class DoubleHessenbergDecomp;
class DoubleSchurDecomp;

class DoubleEigServer {
protected:
  static unsigned&  n(DoubleEigDecomp& x)           {return x.n;}
  static DComplexVec&    lambda(DoubleEigDecomp& x)      {return x.lambda;}
  static DoubleGenMat& P(DoubleEigDecomp& x)           {return x.P;}
  static DoubleGenMat& Q(DoubleEigDecomp& x)           {return x.Q;}
  static RWBoolean& computedAll(DoubleEigDecomp& x) {return x.computedAll;}
  static RWBoolean& accurate(DoubleEigDecomp& x)    {return x.accurate;}
    // These functions provide access to the guts of the EigDecomp
    // object for the server.  This way we can declare more servers
    // without having to make them all friends of EigDecomp.

public:
  virtual DoubleEigDecomp operator()(const DoubleGenMat&) =0;
    // Compute a decomposition.  The subclasses have operator()
    // functions for intermediate forms, like Hessenberg and Schur
    // decompositions.
};

class DoubleSchurEigServer : public DoubleEigServer {
private:
  RWBoolean computeLeftVecs_;
  RWBoolean computeRightVecs_;
  RWBoolean scale_;
  RWBoolean permute_;
  IntVec    selectVec_;       // Which eigenvectors to compute
  RWSlice   selectRange_;     // (either Vec==nil, or Range==RWAll, or both)
public:
  DoubleSchurEigServer(RWBoolean computeLeftVecs=TRUE, RWBoolean computeRightVecs=TRUE, RWBoolean scale=TRUE, RWBoolean permute=TRUE);
  virtual DoubleEigDecomp operator()(const DoubleGenMat& A);
  virtual DoubleEigDecomp operator()(const DoubleBalanceDecomp& A);
  virtual DoubleEigDecomp operator()(const DoubleHessenbergDecomp& A);
  virtual DoubleEigDecomp operator()(const DoubleSchurDecomp& A);
  RWBoolean computeLeftEigenVectors() const;
  void      computeLeftEigenVectors(RWBoolean);
  RWBoolean computeRightEigenVectors() const;
  void      computeRightEigenVectors(RWBoolean);
  RWBoolean computeAllEigenVectors()   const;
  RWBoolean scale() const;
  void      scale(RWBoolean);
  void      selectEigenVectors(const IntVec&);     // Compute only selected eigenvectors
  void      selectEigenVectors(const RWSlice&);
  RWBoolean permute() const;
  void      permute(RWBoolean);
  void      balance(RWBoolean);   // sets both scale and permute
};
  
class DoubleHessEigServer : public DoubleEigServer {
private:
  RWBoolean computeLeftVecs_;
  RWBoolean computeRightVecs_;
  RWBoolean scale_;
  RWBoolean permute_;
  IntVec    selectVec_;       // Which eigenvectors to compute
  RWSlice   selectRange_;     // (either Vec==nil, or Range==RWAll, or both)]
  DoubleEigDecomp setEigenVals(const DoubleHessenbergDecomp& H, DoubleVec* wr, DoubleVec* wi);   // Compute eigenvalues
public:
  DoubleHessEigServer(RWBoolean computeLeftVecs=TRUE, RWBoolean computeRightVecs=TRUE, RWBoolean scale=TRUE, RWBoolean permute=TRUE);
  virtual DoubleEigDecomp operator()(const DoubleGenMat& A);
  virtual DoubleEigDecomp operator()(const DoubleBalanceDecomp& A);
  virtual DoubleEigDecomp operator()(const DoubleHessenbergDecomp& A);
  RWBoolean computeLeftEigenVectors() const;
  void      computeLeftEigenVectors(RWBoolean);
  RWBoolean computeRightEigenVectors() const;
  void      computeRightEigenVectors(RWBoolean);
  RWBoolean computeAllEigenVectors()   const;
  RWBoolean scale() const;
  void      scale(RWBoolean);
  void      selectEigenVectors(const IntVec&);     // Compute only selected eigenvectors
  void      selectEigenVectors(const RWSlice&);
  RWBoolean permute() const;
  void      permute(RWBoolean);
  void      balance(RWBoolean);   // sets both scale and permute
};

#endif

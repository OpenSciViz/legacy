#ifndef __RWSCVEC_H__
#define __RWSCVEC_H__

/*
 * Declarations for SChar precision vectors
 *
 * Generated from template $Id: vector.htm,v 1.17 1993/09/17 18:13:14 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010
 *                      
 * Copyright (C) 1988, 1989, 1990, 1991, 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

/*
 * Defining the preprocessor directive RWBOUNDS_CHECK will
 * cause bounds checking on the subscripting operators.
 */

#include "rw/rwslice.h"
#include "rw/vector.h"

class SCharVecPick;
class RWRand;

class SCharVec : public RWVecView {
private:
    SCharVec(const RWDataView&, SChar*, unsigned, int);// For constructing new views
    friend SCharVec toVec(const SCharArray&);
    friend class SCharGenMat;                   // For row,col,diagonal
    friend class SCharArray;                    // For conversion constructor

public:

    /****************
     * Constructors *
     ****************/

    SCharVec() : RWVecView()                    {}
#ifdef RW_ALLOW_OLD_ARRAY_CONSTRUCTORS
    SCharVec(unsigned n) : RWVecView(n,sizeof(SChar)) {}  
#endif
    SCharVec(unsigned n, RWUninitialized) : RWVecView(n,sizeof(SChar)) {}  
    SCharVec(unsigned n, RWRand&);
    SCharVec(unsigned n, SChar val);
    SCharVec(unsigned n, SChar val, SChar by);
    SCharVec(const char *);
    SCharVec(const SCharVec& a) : RWVecView((const RWVecView&)a) {}  // cast needed by Zortech 3.1
    SCharVec(const SCharVecPick& p);
    SCharVec(const SChar* dat, unsigned n);     // Copy of dat will be made
    SCharVec(RWBlock *block, unsigned n);       // Pass a custom block on the free store

    operator IntVec() const;                    // Convert to IntVec

    /********************
     * Member functions *
     ********************/

    SCharVec   apply(mathFunTy)      const;
#ifdef RW_NATIVE_EXTENDED
    SCharVec   apply(XmathFunTy)     const;
#endif
    unsigned   binaryStoreSize()     const;     // Storage requirements
    SCharVec   copy()                const;     // Synonym for deepCopy()
    SChar*     data()                           {return (SChar*)begin;}
    const SChar* data()              const      {return (const SChar*)begin;}
    SCharVec   deepCopy()            const;     // Copy with distinct instance variables 
    void       deepenShallowCopy();             // Insures only 1 reference to data
//  unsigned   length()              const;     // # elements (defined in base class RWVecView)
    SCharVecPick pick(const IntVec& x);         // Return the "picked" elements
    const SCharVecPick pick(const IntVec& x) const; // const version can only be used in vec ctor
    void       printOn(ostream& s, unsigned numberPerLine=5) const; // Pretty print
    SCharVec&  reference(const SCharVec& v);    // Reference self to v
    void       resize(unsigned);                // Will pad with zeroes if necessary
    void       reshape(unsigned);               // Contents after reshape are garbage
    void       restoreFrom(RWFile&);            // Restore from binary
    void       restoreFrom(RWvistream&);        // Restore from virtual stream
    void       saveOn(RWFile&)       const;     // Store using binary
    void       saveOn(RWvostream&)   const;     // Store to virtual stream
    void       scanFrom(istream& s);            // Read to eof or delimit with []
//  static int setFormatting(int);              // Change # items per line (defined in base class RWVecView)
    SCharVec   slice(int start, unsigned lgt, int strider=1) const;
//  int        stride()              const;     // increment between elements (defined in base class RWVecView)

    /********************
     * Member operators *
     ********************/

    SChar&      operator[](int i);              // With bounds checking
    SChar       operator[](int i) const;        // With bounds checking
    SChar&      operator()(int i);              // With optional bounds checking
    SChar       operator()(int i)        const; // With optional bounds checking
    SCharVec    operator[](const RWSlice&);     // With bounds checking
    const SCharVec operator[](const RWSlice&) const;
    SCharVec    operator()(const RWSlice&);     // With optional bounds checking
    const SCharVec operator()(const RWSlice&) const;
    SCharVec&   operator=(const SCharVec& v);   // Must be same length as v
    SCharVec&   operator=(const SCharVecPick&);
    SCharVec&   operator=(SChar);
    RWBoolean   operator==(const SCharVec&) const;
    RWBoolean   operator!=(const SCharVec& v) const;
    SCharVec&   operator+=(SChar);
    SCharVec&   operator-=(SChar s)             { return operator+=(-s); }
    SCharVec&   operator*=(SChar);
    SCharVec&   operator/=(SChar s);
    SCharVec&   operator+=(const SCharVec&);
    SCharVec&   operator-=(const SCharVec&);
    SCharVec&   operator*=(const SCharVec&);
    SCharVec&   operator/=(const SCharVec&);
    SCharVec&   operator++();                   // Prefix operator
    SCharVec&   operator--();                   // Prefix operator
#if             !defined(RW_NO_POSTFIX)
    void        operator++(int)                 { ++(*this); }  // Postfix operator
    void        operator--(int)                 { --(*this); }  // Postfix operator
#endif

};
  
    /********************
     * Global Operators *
     ********************/

           SCharVec    operator-(const SCharVec&);
    inline SCharVec    operator+(const SCharVec& v) { return v; }
           SCharVec    operator+(const SCharVec&, const SCharVec&);
           SCharVec    operator-(const SCharVec&, const SCharVec&);
           SCharVec    operator*(const SCharVec&, const SCharVec&);
           SCharVec    operator/(const SCharVec&, const SCharVec&);
           SCharVec    operator+(const SCharVec&,SChar);
           SCharVec    operator+(SChar s, const SCharVec& V);
           SCharVec    operator-(const SCharVec& V, SChar s);
           SCharVec    operator-(SChar, const SCharVec&);
           SCharVec    operator*(const SCharVec&,SChar);
           SCharVec    operator*(SChar s, const SCharVec& V);
// The % operator used for inner product.  Definition postponed until
// after the dot prototype is seen.
//  inline SChar       operator%(const SCharVec& x, const SCharVec& y) {return dot(x,y);}
           SCharVec    operator/(const SCharVec& V, SChar s);
           SCharVec    operator/(SChar, const SCharVec&);
           ostream&    operator<<(ostream& s, const SCharVec& v);
           istream&    operator>>(istream& s, SCharVec& v);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
    inline SCharVec    operator*(SChar s, const SCharVec& V) {return V*s;}
    inline SCharVec    operator+(SChar s, const SCharVec& V) {return V+s;}
    inline SCharVec    operator-(const SCharVec& V, SChar s) {return V+(SChar)(-s);} // cast makes microsoft happy when compiling sarr.cpp
#endif

    /********************
     * Global Functions *
     ********************/

    /**** Miscellaneous Functions ****/

           SCharVec    abs(const SCharVec& V);
           SCharVec    cumsum(const SCharVec&);
           SCharVec    delta(const SCharVec&);
           SChar       dot(const SCharVec&,const SCharVec&);
           int         maxIndex(const SCharVec&); // Returns *index* of max value
           SChar       maxValue(const SCharVec&);
           int         minIndex(const SCharVec&); // Returns *index* of min value
           SChar       minValue(const SCharVec&);
           SChar       prod(const SCharVec&);
           SCharVec    reverse(const SCharVec&);
           SChar       sum(const SCharVec&);


    /**** Functions for type conversion ****/

           SCharVec    toChar(const IntVec&);


    /*****************************************
     * Miscellaneous inline member functions *
     *****************************************/
/*
 * This must be put before the inline access functions because
 * subscripting uses this constructor and cfront complains
 * if a function is declared inline after it has already been
 * used.
 */

inline SCharVec::SCharVec(const RWDataView& v, SChar* b, unsigned l, int s)
  : RWVecView(v,b,l,s)
{
}

    /***************************
     * Inline Access Functions *
     ***************************/

  inline SChar& SCharVec::operator[](int i) {
    boundsCheck(i);
    return data()[i*step];
  }

  inline SChar SCharVec::operator[](int i) const {
    boundsCheck(i);
    return data()[i*step];
  }

  inline SChar& SCharVec::operator()(int i) {
#  ifdef RWBOUNDS_CHECK    
    boundsCheck(i);
#  endif
    return data()[i*step];
  }

  inline SChar SCharVec::operator()(int i) const {
#  ifdef RWBOUNDS_CHECK    
    boundsCheck(i);
#  endif
    return data()[i*step];
  }

  inline SCharVec SCharVec::operator[](const RWSlice& s) {
    s.boundsCheck(npts);
    return SCharVec(*this, data()+s.begin()*stride(), s.len(npts), stride()*s.stride());
  }

  inline const SCharVec SCharVec::operator[](const RWSlice& s) const {
    s.boundsCheck(npts);
    return SCharVec(*this, (SChar*)(data()+s.begin()*stride()), s.len(npts), stride()*s.stride());
  }

  inline SCharVec SCharVec::operator()(const RWSlice& s) {
#  ifdef RWBOUNDS_CHECK
    s.boundsCheck(npts);
#  endif
    return SCharVec(*this, data()+s.begin()*stride(), s.len(npts), stride()*s.stride());
  }

  inline const SCharVec SCharVec::operator()(const RWSlice& s) const {
#  ifdef RWBOUNDS_CHECK
    s.boundsCheck(npts);
#  endif
    return SCharVec(*this, (SChar*)(data()+s.begin()*stride()), s.len(npts), stride()*s.stride());
  }

// Here is the definition of operator% for inner products.  Had to
// be postponed because dot() was not yet declared in the operator
// section.
inline SChar         operator%(const SCharVec& x, const SCharVec& y) {return dot(x,y);}

#endif /* __RWSCVEC_H__ */

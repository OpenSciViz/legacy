#ifndef __RWFQR_H__
#define __RWFQR_H__
/*
 * FloatQRDecomp:       representation of a QR decomposition
 * FloatQRDecompServer: produce QR decompositions, using possibly non-default options
 *
 * Generated from template $Id: xqr.h,v 1.1 1993/05/18 17:00:02 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 * A QR decomposition decomposes a matrix A like this:
 *
 *                    [ R ]
 *            A P = Q [   ]
 *                    [ 0 ]
 *
 * where P is a permutation matrix, Q is orthogonal, and R is upper
 * triangular if A is taller than wide, and upper trapazoidal is  A
 * is wider than tall.
 */

#include "rw/fgenmat.h"
#include "rw/fvec.h"
#include "rw/ivec.h"

class FloatQRDecomp {
private:
  long      *pivots_;    // nil if no pivoting was done
  FloatVec    tau_;        // scalars needed to recover Q
  FloatGenMat QR_;         // upper triangular part is R, rest is used to compute Q

public:
  FloatQRDecomp();                      // Constructors
  FloatQRDecomp(const FloatQRDecomp&);
  FloatQRDecomp(const FloatGenMat&);
  void factor(const FloatGenMat&);
  ~FloatQRDecomp();
  void operator=(const FloatQRDecomp&);

  unsigned       rows() const         {return QR_.rows();}
  unsigned       cols() const         {return QR_.cols();}
  FloatGenMat      P() const;           // Extract components of the decomposition
  FloatGenMat      R() const;
  FloatVec         Rdiagonal() const;
  FloatGenMat      Q() const;

  FloatVec         Px(const FloatVec& x) const;    // Compute using the decomposition
  FloatVec         PTx(const FloatVec& x) const;
  FloatVec         Rx(const FloatVec& x) const;
  FloatVec         RTx(const FloatVec& x) const;
  FloatVec         Rinvx(const FloatVec& x) const;
  FloatVec         RTinvx(const FloatVec& x) const;
  FloatVec         Qx(const FloatVec& x) const;
  FloatVec         QTx(const FloatVec& x) const;

  friend class FloatQRDecompServer;
  friend class FloatCODecomp;
};

class FloatQRDecompServer {
private:
  RWBoolean pivot_;       // Do we pivot?
  IntVec    initial_;     // Indices of columns to be moved to initial position

public:
  FloatQRDecompServer();
  void setPivoting(RWBoolean);   // turn pivoting option on or off
  void setInitialIndex(int i);   // i moves to the begin of the decomposition
  void setFreeIndex(int i);      // i is removed from initial and final lists

  FloatQRDecomp operator()(const FloatGenMat&) const;   // build a QR decomposition
};
#endif

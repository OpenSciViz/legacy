#ifndef __RWCPDTDFCT_H__
#define __RWCPDTDFCT_H__

/*
 * DComplexPDTriDiagFact - A factorization of a positive definite tri-diagonal matrix
 *
 * Generated from template $Id: xfct.h,v 1.3 1993/07/05 19:19:33 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1988-1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 *
 * This header file will work both with lapack.h++ and linpack.h++.
 */

#include "rw/ctrdgmat.h"

class DComplexPDTriDiagFact {
private:
    DoubleVec d;      // diagonal
    DComplexVec e;      // off-diagonal
    // The factorization and the pivot vector.
    // This cannot be a union because IntVec has a constructor

union {
    long   info;           // used by Lapack.h++
    int pdinfo;            // used by Linpack.h++
};
  // If info is non-zero, something went wrong in the factorization.
  
union { 
    Double    Anorm;          // used by Lapack.h++
    Double    cond;           // used by Linpack.h++
};
    // If Anorm is non-negative, then it is the infinity norm of the
    // original matrix.  This is used when computing the condition
    // number.
    // cond is the reciprocal condition number if calculated.  If
    // condition number not calculated contains 0 for singularity
    // else -1.

protected:
           
    static const char *className;
    // For use in error messages

    void dofactor(RWBoolean estimateCondition);
    // Do the factorization.  Call this after the matrix 
    // variable factorization is set up.

    void calldi(const DComplexTriDiagMat& A, int job, DComplex* det, int *inert) const;
    // call the linpack xxxdi routine with the job number indicated
    // and with the 2 byte array det.

public:
    DComplexPDTriDiagFact();
    DComplexPDTriDiagFact( const DComplexPDTriDiagFact& );
    DComplexPDTriDiagFact( const DComplexTriDiagMat& A, RWBoolean estimateCondition=TRUE );
    void factor( const DComplexTriDiagMat& A, RWBoolean estimateCondition=TRUE );
    ~DComplexPDTriDiagFact();
    // The constructors.
    // You can save a little bit of time by not estimating the
    // condition of the factorization, and thus giving up any 
    // knowledge of how accurate solutions obtained using this
    // factorization will be.

    RWBoolean good() const { return !fail(); }
    RWBoolean fail() const;
    RWBoolean isSingular() const;
    int       rows() const { return d.length(); }
    int       cols() const { return d.length(); }
    Double condition() const;
    RWBoolean isPD() const;
    // If fail() returns true calling solve() or inverse() or ... may
    // fail.  condition() returns an approximation to the reciprocal
    // condition number of the factorization.

    DComplexVec solve(const DComplexVec& b) const;
    DComplexGenMat solve(const DComplexGenMat& b) const;
    DComplex determinant() const;
    // solve and inverse functions
};

DComplexVec solve(const DComplexPDTriDiagFact& A, const DComplexVec& b);
DComplexGenMat solve(const DComplexPDTriDiagFact& A, const DComplexGenMat& b);
DComplex    determinant (const DComplexPDTriDiagFact& A);


Double condition(const DComplexPDTriDiagFact& A);

#endif

#ifndef __RWCCO_H__
#define __RWCCO_H__
/*
 * DComplexCODecomp: representation of a complete orthogonal decomposition
 *
 * Generated from template $Id: xco.h,v 1.1 1993/05/18 17:00:02 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 * A complete orthogonal decomposition decomposes a matrix A like this:
 *
 *                    [ T 0 ]
 *            A P = Q [     ] Z'
 *                    [ 0 0 ]
 *
 * where P is a permutation matrix, Q and Z are orthogonal, and T is upper
 * triangular.
 *
 * Implementation details:
 * The Q and P parts of the decomposition are represented using a QRDecomp
 * object.  In order to save copying the R part, CODecomp is made a friend
 * or QRDecomp and we invalidate the R portion of the decomposition,
 * writing T and information needed to compute Z over top of it.  Note
 * that the TZ_ object refers to the same data as QRdecomp_.QR_.
 */

#include "rw/cqr.h"
#include "rw/cutrimat.h"

class DComplexCODecomp {
private:
  DComplexQRDecomp QRdecomp_;           // used to represent Q and P.  The R part of this is invalid.
  DComplexVec      Ztau_;               // scalars needed to recover Z
  DComplexGenMat   TZ_;                 // upper triangular part is T, rest is used to compute Z
  void        dofactor(double tol);// build Ztau_ and TZ_ from QRdecomp_

public:
  DComplexCODecomp();                      // Constructors
  DComplexCODecomp(const DComplexCODecomp&);
  DComplexCODecomp(const DComplexQRDecomp&, double tol=0);  // tol indicates when to treat diagonal entries as zero
  DComplexCODecomp(const DComplexGenMat&, double tol=0);    // tol indicates when to treat diagonal entries as zero
  void factor(const DComplexQRDecomp&, double tol=0);  // tol indicates when to treat diagonal entries as zero
  void factor(const DComplexGenMat&, double tol=0);    // tol indicates when to treat diagonal entries as zero
  ~DComplexCODecomp();
  void operator=(const DComplexCODecomp&);

  unsigned       rows() const         {return QRdecomp_.rows();}
  unsigned       cols() const         {return QRdecomp_.cols();}
  unsigned       rank() const         {return TZ_.rows();}
  DComplexGenMat      P() const            {return QRdecomp_.P();}
  DComplexUpperTriMat T() const;
  DComplexGenMat      Q() const            {return QRdecomp_.Q();}
  DComplexGenMat      Z() const;

  DComplexVec         Px(const DComplexVec& x) const       {return QRdecomp_.Px(x);}
  DComplexVec         PTx(const DComplexVec& x) const      {return QRdecomp_.PTx(x);}
  DComplexVec         Tx(const DComplexVec& x) const;
  DComplexVec         TTx(const DComplexVec& x) const;
  DComplexVec         Tinvx(const DComplexVec& x) const;
  DComplexVec         TTinvx(const DComplexVec& x) const;
  DComplexVec         Qx(const DComplexVec& x) const       {return QRdecomp_.Qx(x);}
  DComplexVec         QTx(const DComplexVec& x) const      {return QRdecomp_.QTx(x);}
  DComplexVec         Zx(const DComplexVec& x) const;
  DComplexVec         ZTx(const DComplexVec& x) const;
};

#endif

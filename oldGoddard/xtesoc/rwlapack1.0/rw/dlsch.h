#ifndef __RWDLSCH_H__
#define __RWDLSCH_H__
/*
 * DoubleLeastSqCh:  solve least square Ch problem with orthogonal decomposition
 *
 * Generated from template $Id: xlsch.h,v 1.3 1993/07/05 19:19:33 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 * This least squares class only works for full rank problems.  For
 * more accurate, robust least squares classes, use the QR or SV
 * least squares factorizations.  This class has the advantage of
 * being the fastest of the three.
 */

#include "rw/dpdfct.h"
#include "rw/dgenmat.h"

class DoubleLeastSqCh {
private:
  DoubleGenMat A_;
  DoublePDFact decomp_;   // Cholesky decomposition of the normal equations
public:                                          
  DoubleLeastSqCh();
  DoubleLeastSqCh(const DoubleGenMat& A);
  void      factor(const DoubleGenMat& A);
  RWBoolean good() const {return decomp_.good();}
  RWBoolean fail() const {return decomp_.fail();}
  unsigned  rows() const {return A_.rows();}
  unsigned  cols() const {return A_.cols();}
  unsigned  rank() const {return A_.cols();}
  DoubleVec    residual(const DoubleVec& data) const;
  Double       residualNorm(const DoubleVec& data) const;
  DoubleVec    solve(const DoubleVec& data) const;
};

inline DoubleVec solve(   const DoubleLeastSqCh& A, const DoubleVec& b) {return A.solve(b);}
inline DoubleVec residual(const DoubleLeastSqCh& A, const DoubleVec& b) {return A.residual(b);}
inline Double    residualNorm(const DoubleLeastSqCh& A, const DoubleVec& b) {return A.residualNorm(b);}

#endif

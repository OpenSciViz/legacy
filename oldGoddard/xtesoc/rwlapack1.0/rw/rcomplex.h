#ifndef __RWRCOMPLEX_H__
#define __RWRCOMPLEX_H__

/*
 * Declarations for the Rogue Wave complex class DComplex
 *
 * $Header: /users/rcs/mathrw/rcomplex.h,v 1.4 1993/09/19 21:55:16 alv Exp $
 *
 ****************************************************************************
 *
 * Source code available!
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989-1993. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: rcomplex.h,v $
 * Revision 1.4  1993/09/19  21:55:16  alv
 * changed to take non-const parms
 *
 * Revision 1.3  1993/03/09  15:36:23  alv
 * added full suite of divide operators
 *
 * Revision 1.2  1993/03/03  01:05:15  alv
 * updated for lapack.h++
 *
 * Revision 1.1  1993/01/23  00:08:42  alv
 * Initial revision
 *
 * 
 *    Rev 1.3   17 Oct 1991 09:12:56   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.1   24 Jul 1991 13:01:12   keffer
 * Added pvcs keywords
 *
 */

STARTWRAP
#include <math.h>
ENDWRAP

class istream;
class ostream;

class DComplex {
  double  re, im;
public:
  DComplex()                              {re=0; im=0;}
  DComplex(double r)                      {re=r; im=0; }
  DComplex(double r, double i)            {re=r; im=i; }

  // Following 3 lines should not be necessary, but
  // the class a bug in the Zortech optimizer requires them:
  DComplex(const DComplex& c)             {re=c.re; im=c.im;}
  DComplex& operator=(const DComplex& c)  {re=c.re; im=c.im; return *this;}
  DComplex& operator=(double x)           {re=x;    im=0;    return *this;}

  friend  double   real(const DComplex& a){return a.re;}
  friend  double   imag(const DComplex& a){return a.im;}

  friend  double   abs(DComplex);
  friend  double   norm(DComplex);
  friend  double   arg(DComplex);
  friend  DComplex conj(DComplex a)    {return DComplex(a.re, -a.im);}
  friend  DComplex cos(DComplex);
  friend  DComplex cosh(DComplex);
  friend  DComplex exp(DComplex);
  friend  DComplex log(DComplex);
  friend  DComplex pow(double, DComplex);
  friend  DComplex pow(DComplex, int);
  friend  DComplex pow(DComplex, double);
  friend  DComplex pow(DComplex, DComplex);
  friend  DComplex polar(double, double = 0);
  friend  DComplex sin(DComplex);
  friend  DComplex sinh(DComplex);
  friend  DComplex sqrt(DComplex);

  friend  DComplex operator+(DComplex a1, DComplex a2) {return DComplex(a1.re+a2.re, a1.im+a2.im);}
  friend  DComplex operator+(double s, DComplex a2)    {return DComplex(s+a2.re, a2.im);}
  friend  DComplex operator+(DComplex a1, double s)    {return DComplex(a1.re+s, a1.im);}
  friend  DComplex operator-(DComplex a)               {return DComplex(-a.re, -a.im);}
  friend  DComplex operator-(DComplex a1, DComplex a2) {return DComplex(a1.re-a2.re, a1.im-a2.im);}
  friend  DComplex operator-(double s, DComplex a2)    {return DComplex(s-a2.re, -a2.im);}
  friend  DComplex operator-(DComplex a1, double s)    {return DComplex(a1.re-s, a1.im);}
  friend  DComplex operator*(DComplex a1, DComplex a2) {return DComplex(a1.re*a2.re-a1.im*a2.im, a1.re*a2.im+a1.im*a2.re);}
  friend  DComplex operator*(DComplex a1, double s)    {return DComplex(s*a1.re, s*a1.im);}
  friend  DComplex operator*(double s, DComplex a1)    {return DComplex(s*a1.re, s*a1.im);}
  friend  DComplex operator/(DComplex a1, DComplex a2);  
  friend  DComplex operator/(DComplex a1, double s)    {return DComplex(a1.re/s, a1.im/s);}
  friend  DComplex operator/(double s, DComplex a1);
  friend  int      operator==(DComplex a, DComplex b)  {return (a.re==b.re && a.im==b.im);}
  friend  int      operator!=(DComplex a, DComplex b)  {return (a.re!=b.re || a.im!=b.im);}
  
  void    operator+=(DComplex a)  {re+=a.re; im+=a.im;}
  void    operator-=(DComplex a)  {re-=a.re; im-=a.im;}
  void    operator*=(DComplex a);
  void    operator/=(DComplex a);

  friend  ostream& operator<<(ostream&, DComplex);
  friend  istream& operator>>(istream&, DComplex&);
};

#endif /* __RWRCOMPLEX_H__ */

#ifndef __RWCSKEWMAT_H__
#define __RWCSKEWMAT_H__
/*
 * DComplexSkewMat - An skew symmetric matrix of DComplexs
 *
 * Stores a skew symmetric matrix.  Note that only half the matrix is actually
 * stored.  This means if you change, say, entry (2,3) you are also
 * implicitly changing entry (3,2).
 *
 * The lower left triangle of the matrix is stored in column major
 * order.
 *
 * Generated from template $Id: xmat.h,v 1.4 1993/10/11 21:48:17 alv Exp $
 *
 * Bounds checking is enabled by setting the preprocessor
 * symbol RWBOUNDS_CHECK.  This is strongly encouraged.
 *
 * Copyright (1991,1992,1993) by Rogue Wave Software, Inc.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 * Written by Al Vermeulen.
 *
 * Limited license.
 */

#include "rw/cvec.h"
#include "rw/cref.h"


class DoubleSkewMat;
class DComplexGenMat;
class DoubleSymMat;

class DComplexSkewMat {
private:

DComplexVec vec;
unsigned n;
  // The data which define the matrix


public:
DComplexSkewMat();
DComplexSkewMat( const DComplexSkewMat& );
DComplexSkewMat(unsigned n, unsigned nAgain);
DComplexSkewMat(const DComplexVec& data, unsigned n, unsigned nAgain);
DComplexSkewMat(const DoubleSkewMat& re);
DComplexSkewMat(const DoubleSkewMat& re, const DoubleSkewMat& im);
~DComplexSkewMat();
  // Constructors, conversion functions, and a destructor.

static const char *className;
  // For use in error messages

inline unsigned rows() const	   { return n; }
inline unsigned cols() const	   { return n; }
  // Member functions to access the shape of the matrix

inline DComplex  val(int i, int j) const;
       DComplex  bcval(int i, int j) const;
inline DComplex  set(int i, int j, DComplex x);
       DComplex  bcset(int i, int j, DComplex x);
inline NGDComplexRef operator()(int i, int j);
inline NGDComplexRef ref(int i, int j);
       NGDComplexRef bcref(int i, int j);
inline DComplex  operator()(int i, int j) const; // { return val(i,j); }
  // Access and/or change elements of the matrix.
  // The versions that begin with the letters bc do bounds
  // checking.  The others do bounds checking only if the
  // preprocessor symbol RWBOUNDS_CHECK is defined.

DComplexSkewMat leadingSubmatrix(int order);
  // Access to submatrices.
  // The leading submatrix of order k is the kxk matrix in the upper
  // left.
  // Access to diagonals.  The same conventions as for
  // element access apply (with respect to bcxxx and xxx names).

DComplexSkewMat& operator=(const DComplexSkewMat& m);
DComplexSkewMat& reference(DComplexSkewMat& m);
  // reference() makes an alias, operator= makes a copy.


void zero()        { vec=DComplex(0,0); }
  // Sets all elements of the matrix equal to zero.


DComplexSkewMat copy() const;
DComplexSkewMat deepCopy() const		{ return copy(); }
void deepenShallowCopy()        { vec.deepenShallowCopy(); }
  // copy() returns a DComplexSkewMat with its own copy of the data.
  // deepenShallowCopy makes sure the data is not shared by another
  // vector or matrix.

DComplexVec dataVec() 		{ return vec; }
const DComplexVec& dataVec() const	{ return vec; }
DComplex* data() 			{ return vec.data(); }
const DComplex* data() const 	{ return vec.data(); }
  // Access to the raw data.

void resize(unsigned m, unsigned n);
  // Resize the matrix.  New elements are set to zero.


void scanFrom(istream&);      		// Human readable
void printOn(ostream&) const;
void restoreFrom(RWvistream&);     	// Internal ASCII or binary code
void saveOn(RWvostream&) const;
void restoreFrom(RWFile&);
void saveOn(RWFile&) const;
unsigned binaryStoreSize() const;
  // Input/output member functions
  // Whether ASCII or binary formatting is used with readFrom and
  // storeOn is determined by the type of rwstream passed.

/*
 * The operator()() member functions are declared with ref(),
 * since the declarations look the same.
 *
 * The operator=() assignment operators are declared with reference(),
 * since the functions are so closely related.
 */

RWBoolean operator==(const DComplexSkewMat& X);
RWBoolean operator!=(const DComplexSkewMat& X)  { return !(operator==(X)); }
  // Two matrices are considered equal if their shapes are identical and
  // all their components are identical.

DComplexSkewMat& operator+=(const DComplexSkewMat& m);
DComplexSkewMat& operator-=(const DComplexSkewMat& m);
DComplexSkewMat& operator*=(const DComplexSkewMat& m);
DComplexSkewMat& operator*=(DComplex);
DComplexSkewMat& operator/=(const DComplexSkewMat& m);
DComplexSkewMat& operator/=(DComplex);
  // assignment operators.  self must be same size as m.


};

DComplexSkewMat operator-(const DComplexSkewMat&);	// Unary minus
DComplexSkewMat operator+(const DComplexSkewMat&);	// Unary plus
DComplexSkewMat operator*(const DComplexSkewMat&, const DComplexSkewMat&);
DComplexSkewMat operator/(const DComplexSkewMat&, const DComplexSkewMat&);
DComplexSkewMat operator+(const DComplexSkewMat&, const DComplexSkewMat&);
DComplexSkewMat operator-(const DComplexSkewMat&, const DComplexSkewMat&);
DComplexSkewMat operator*(const DComplexSkewMat&, DComplex);
#ifndef RW_NO_INLINED_TEMP_DESTRUCTORS
inline DComplexSkewMat operator*(DComplex x, const DComplexSkewMat& A) { return A*x; }
#else
       DComplexSkewMat operator*(DComplex x, const DComplexSkewMat& A);
#endif
DComplexSkewMat operator/(const DComplexSkewMat& A, DComplex x);
DComplexSkewMat operator/(DComplex x, const DComplexSkewMat& A);
  // Arithmetic operators; Notice that operator* is an
  // element by element multiply, NOT a matrix multiply.

inline ostream& operator<<(ostream& s, const DComplexSkewMat& m)
		{m.printOn(s); return s;}
inline istream& operator>>(istream& s, DComplexSkewMat& m)
		{m.scanFrom(s); return s;}
  // I/O for humans

DComplexSkewMat transpose(const DComplexSkewMat&);
  // The transpose of the matrix. 

DComplexVec product(const DComplexSkewMat& A, const DComplexVec& x);
DComplexVec product(const DComplexVec& x, const DComplexSkewMat& A);
  // inner products

DComplexSkewMat toSkewMat( const DComplexGenMat& A, RWBoolean keepMainDiag = TRUE );

DoubleSymMat abs(const DComplexSkewMat& A);
DComplexSkewMat conj(const DComplexSkewMat& A);
DoubleSkewMat real(const DComplexSkewMat& A);
DoubleSkewMat imag(const DComplexSkewMat& A);
DoubleSymMat norm(const DComplexSkewMat& A);
/* Sorry, no arg function for SkewMat */



/*
 * Inline functions
 */

inline DComplex DComplexSkewMat::val(int i, int j) const {
#ifdef RWBOUNDS_CHECK
return bcval(i,j);
#else
    int index = (i<=j) ?  (j*(j+1)/2+i) : (i*(i+1)/2+j);
    DComplex val = vec(index);
    return (i<=j) ?  val : -val;
#endif
}

inline DComplex DComplexSkewMat::set(int i, int j, DComplex x) {
    return bcset(i,j,x);
}

inline NGDComplexRef DComplexSkewMat::ref(int i, int j) {
    return bcref(i,j);
}

inline NGDComplexRef DComplexSkewMat::operator()(int i, int j) { return ref(i,j); }
inline DComplex DComplexSkewMat::operator()(int i, int j) const { return val(i,j); }


#endif

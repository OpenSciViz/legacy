#ifndef __RWFLSCH_H__
#define __RWFLSCH_H__
/*
 * FloatLeastSqCh:  solve least square Ch problem with orthogonal decomposition
 *
 * Generated from template $Id: xlsch.h,v 1.3 1993/07/05 19:19:33 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 * This least squares class only works for full rank problems.  For
 * more accurate, robust least squares classes, use the QR or SV
 * least squares factorizations.  This class has the advantage of
 * being the fastest of the three.
 */

#include "rw/fpdfct.h"
#include "rw/fgenmat.h"

class FloatLeastSqCh {
private:
  FloatGenMat A_;
  FloatPDFact decomp_;   // Cholesky decomposition of the normal equations
public:                                          
  FloatLeastSqCh();
  FloatLeastSqCh(const FloatGenMat& A);
  void      factor(const FloatGenMat& A);
  RWBoolean good() const {return decomp_.good();}
  RWBoolean fail() const {return decomp_.fail();}
  unsigned  rows() const {return A_.rows();}
  unsigned  cols() const {return A_.cols();}
  unsigned  rank() const {return A_.cols();}
  FloatVec    residual(const FloatVec& data) const;
  Float       residualNorm(const FloatVec& data) const;
  FloatVec    solve(const FloatVec& data) const;
};

inline FloatVec solve(   const FloatLeastSqCh& A, const FloatVec& b) {return A.solve(b);}
inline FloatVec residual(const FloatLeastSqCh& A, const FloatVec& b) {return A.residual(b);}
inline Float    residualNorm(const FloatLeastSqCh& A, const FloatVec& b) {return A.residualNorm(b);}

#endif

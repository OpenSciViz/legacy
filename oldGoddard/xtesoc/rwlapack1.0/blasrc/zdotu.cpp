/*
 * Default C++ implementation of zdotu
 * For optimum performance, use a machine specific bla library
 *
 * $Id: zdotu.cpp,v 1.3 1993/03/19 16:26:58 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 06:03:43
 * FOR_C++ Options SET: alloc do=rt no=p pf=zbla,dbla,xbla s=dv str=l - prototypes
 *
 * $Log: zdotu.cpp,v $
 * Revision 1.3  1993/03/19  16:26:58  alv
 * added RWBLADECL linkage specification
 *
 * Revision 1.2  1993/03/05  23:08:20  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:05:13  alv
 * Initial revision
 *
 */

#define RW_CPPBLAS 1
#include "rw/bla.h"
#include "rw/bla.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

RWBLADECL DComplex /*FUNCTION*/ zdotu(const long &n, DComplex zx[], const long &incx, 
   DComplex zy[], const long &incy)
{
  long _do0, _do1, i, i_, ix, iy;
  DComplex zdotu_v, ztemp;

  
  //     forms the dot product of two vectors.
  //     jack dongarra, 3/11/78.
  
  ztemp = DComplex(0.0e0,0.0e0);
  zdotu_v = DComplex(0.0e0,0.0e0);
  if( n <= 0 ) 
    return( zdotu_v );
  if( incx == 1 && incy == 1 ) 
    goto L_20;
  
  //        code for unequal increments or equal increments
  //          not equal to 1
  
  ix = 1;
  iy = 1;
  if( incx < 0 ) 
    ix = (-n + 1)*incx + 1;
  if( incy < 0 ) 
    iy = (-n + 1)*incy + 1;
  for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
    ztemp = ztemp + zx[ix - 1]*zy[iy - 1];
    ix = ix + incx;
    iy = iy + incy;
  }
  zdotu_v = ztemp;
  return( zdotu_v );
  
  //        code for both increments equal to 1
  
L_20:
  for( i = 1, i_ = i - 1, _do1 = n; i <= _do1; i++, i_++ ) { 
    ztemp = ztemp + zx[i_]*zy[i_];
  }
  zdotu_v = ztemp;
  return( zdotu_v );
} // end of function 


/*
 * Default C++ implementation of crotg
 * For optimum performance, use a machine specific bla library
 *
 * $Id: crotg.cpp,v 1.4 1993/03/19 16:25:49 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 06:02:50
 * FOR_C++ Options SET: alloc do=rt no=p pf=cbla,sbla,xbla s=dv str=l - prototypes
 *
 * $Log: crotg.cpp,v $
 * Revision 1.4  1993/03/19  16:25:49  alv
 * added RWBLADECL linkage specification
 *
 * Revision 1.3  1993/03/06  00:55:18  alv
 * eliminated const where info is passed back through args
 *
 * Revision 1.2  1993/03/05  23:06:57  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:04:27  alv
 * Initial revision
 *
 */

#define RW_CPPBLAS 1
#include "rw/bla.h"
#include "rw/bla.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/fcomplex.h"

RWBLADECL void /*FUNCTION*/ crotg(FComplex &ca, FComplex &cb, float &c, FComplex &s)
{
  float norm, scale;
  double tmp1, tmp2;
  FComplex alpha;

  if( abs( ca ) != 0. ) 
    goto L_10;
  c = 0.;
  s = FComplex(1.,0.);
  ca = cb;
  goto L_20;
L_10:
  ;
  scale = abs( ca ) + abs( cb );
  // norm = scale*sqrt( pow(abs( ca/scale ), 2) + pow(abs( cb/scale ), 2) );
  tmp1 = abs(ca/scale);
  tmp2 = abs(cb/scale);
  norm = scale*sqrt( tmp1*tmp1 + tmp2*tmp2 );
  alpha = ca/abs( ca );
  c = abs( ca )/norm;
  s = alpha*conj( cb )/norm;
  ca = alpha*norm;
L_20:
  ;
  return;
} // end of function 


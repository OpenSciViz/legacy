/*
 * Default C++ implementation of lsame
 * For optimum performance, use a machine specific bla library
 *
 * $Id: lsame.cpp,v 1.3 1993/03/19 16:26:58 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Rewrote this version from scratch, since this is very easy
 * in C++, and very difficult in Fortran.
 *
 * $Log: lsame.cpp,v $
 * Revision 1.3  1993/03/19  16:26:58  alv
 * added RWBLADECL linkage specification
 *
 * Revision 1.2  1993/03/05  23:07:42  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:04:51  alv
 * Initial revision
 *
 */

#define RW_CPPBLAS 1
#include "rw/bla.h"
#include "rw/bla.h"
#include "rw/fortran.h" /* Fortran run time library */
#include <ctype.h>

RWBLADECL int /*FUNCTION*/ lsame(const char &ca, const char &cb)
{
  return ( toupper(ca) == toupper(cb) );
}

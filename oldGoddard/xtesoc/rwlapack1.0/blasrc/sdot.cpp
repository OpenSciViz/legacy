/*
 * Default C++ implementation of sdot
 * For optimum performance, use a machine specific bla library
 *
 * $Id: sdot.cpp,v 1.3 1993/03/19 16:26:58 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/17/93 at 14:39:10
 * FOR_C++ Options SET: alloc do=rt no=p pf=sbla,xbla s=dv str=l - prototypes
 *
 * $Log: sdot.cpp,v $
 * Revision 1.3  1993/03/19  16:26:58  alv
 * added RWBLADECL linkage specification
 *
 * Revision 1.2  1993/03/05  23:07:47  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:04:53  alv
 * Initial revision
 *
 */

#define RW_CPPBLAS 1
#include "rw/bla.h"
#include "rw/bla.h"
#include "rw/fortran.h" /* Fortran run time library */

RWBLADECL float /*FUNCTION*/ sdot(const long &n, float sx[], const long &incx, float sy[], 
   const long &incy)
{
  long _do0, _do1, _do2, i, i_, ix, iy, m, mp1;
  float sdot_v, stemp;

  
  //     forms the dot product of two vectors.
  //     uses unrolled loops for increments equal to one.
  //     jack dongarra, linpack, 3/11/78.
  
  
  stemp = 0.0e0;
  sdot_v = 0.0e0;
  if( n <= 0 ) 
    return( sdot_v );
  if( incx == 1 && incy == 1 ) 
    goto L_20;
  
  //        code for unequal increments or equal increments
  //          not equal to 1
  
  ix = 1;
  iy = 1;
  if( incx < 0 ) 
    ix = (-n + 1)*incx + 1;
  if( incy < 0 ) 
    iy = (-n + 1)*incy + 1;
  for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
    stemp = stemp + sx[ix - 1]*sy[iy - 1];
    ix = ix + incx;
    iy = iy + incy;
  }
  sdot_v = stemp;
  return( sdot_v );
  
  //        code for both increments equal to 1
  
  
  //        clean-up loop
  
L_20:
  // mod() function massaged out of next line
  m = n % 5; //m = mod( n, 5 );
  if( m == 0 ) 
    goto L_40;
  for( i = 1, i_ = i - 1, _do1 = m; i <= _do1; i++, i_++ ) { 
    stemp = stemp + sx[i_]*sy[i_];
  }
  if( n < 5 ) 
    goto L_60;
L_40:
  mp1 = m + 1;
  for( i = mp1, i_ = i - 1, _do2 = n; i <= _do2; i += 5, i_ += 5 ) { 
    stemp = stemp + sx[i_]*sy[i_] + sx[i_ + 1]*sy[i_ + 1] + sx[i_ + 2]*
     sy[i_ + 2] + sx[i_ + 3]*sy[i_ + 3] + sx[i_ + 4]*sy[i_ + 4];
  }
L_60:
  sdot_v = stemp;
  return( sdot_v );
} // end of function 


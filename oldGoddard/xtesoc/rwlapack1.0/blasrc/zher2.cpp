/*
 * Default C++ implementation of zher2
 * For optimum performance, use a machine specific bla library
 *
 * $Id: zher2.cpp,v 1.3 1993/03/19 16:26:58 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 06:03:43
 * FOR_C++ Options SET: alloc do=rt no=p pf=zbla,dbla,xbla s=dv str=l - prototypes
 *
 * $Log: zher2.cpp,v $
 * Revision 1.3  1993/03/19  16:26:58  alv
 * added RWBLADECL linkage specification
 *
 * Revision 1.2  1993/03/05  23:08:30  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:05:17  alv
 * Initial revision
 *
 */

#define RW_CPPBLAS 1
#include "rw/bla.h"
#include "rw/bla.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const DComplex ZERO = DComplex(0.0e0,0.0e0);
// end of PARAMETER translations

RWBLADECL void /*FUNCTION*/ zher2(const char &uplo, const long &n, const DComplex &alpha, DComplex x[], 
 const long &incx, DComplex y[], const long &incy, DComplex *a, const long &lda)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  long _do0, _do1, _do2, _do3, _do4, _do5, _do6, _do7, i, i_, 
   info, ix, iy, j, j_, jx, jy, kx, ky;
  DComplex temp1, temp2;

  //     .. Scalar Arguments ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZHER2  performs the hermitian rank 2 operation
  
  //     A := alpha*x*conjg( y' ) + conjg( alpha )*y*conjg( x' ) + A,
  
  //  where alpha is a scalar, x and y are n element vectors and A is an n
  //  by n hermitian matrix.
  
  //  Parameters
  //  ==========
  
  //  UPLO   - CHARACTER*1.
  //           On entry, UPLO specifies whether the upper or lower
  //           triangular part of the array A is to be referenced as
  //           follows:
  
  //              UPLO = 'U' or 'u'   Only the upper triangular part of A
  //                                  is to be referenced.
  
  //              UPLO = 'L' or 'l'   Only the lower triangular part of A
  //                                  is to be referenced.
  
  //           Unchanged on exit.
  
  //  N      - INTEGER.
  //           On entry, N specifies the order of the matrix A.
  //           N must be at least zero.
  //           Unchanged on exit.
  
  //  ALPHA  - COMPLEX*16      .
  //           On entry, ALPHA specifies the scalar alpha.
  //           Unchanged on exit.
  
  //  X      - COMPLEX*16       array of dimension at least
  //           ( 1 + ( n - 1 )*abs( INCX ) ).
  //           Before entry, the incremented array X must contain the n
  //           element vector x.
  //           Unchanged on exit.
  
  //  INCX   - INTEGER.
  //           On entry, INCX specifies the increment for the elements of
  //           X. INCX must not be zero.
  //           Unchanged on exit.
  
  //  Y      - COMPLEX*16       array of dimension at least
  //           ( 1 + ( n - 1 )*abs( INCY ) ).
  //           Before entry, the incremented array Y must contain the n
  //           element vector y.
  //           Unchanged on exit.
  
  //  INCY   - INTEGER.
  //           On entry, INCY specifies the increment for the elements of
  //           Y. INCY must not be zero.
  //           Unchanged on exit.
  
  //  A      - COMPLEX*16       array of DIMENSION ( LDA, n ).
  //           Before entry with  UPLO = 'U' or 'u', the leading n by n
  //           upper triangular part of the array A must contain the upper
  //           triangular part of the hermitian matrix and the strictly
  //           lower triangular part of A is not referenced. On exit, the
  //           upper triangular part of the array A is overwritten by the
  //           upper triangular part of the updated matrix.
  //           Before entry with UPLO = 'L' or 'l', the leading n by n
  //           lower triangular part of the array A must contain the lower
  //           triangular part of the hermitian matrix and the strictly
  //           upper triangular part of A is not referenced. On exit, the
  //           lower triangular part of the array A is overwritten by the
  //           lower triangular part of the updated matrix.
  //           Note that the imaginary parts of the diagonal elements need
  //           not be set, they are assumed to be zero, and on exit they
  //           are set to zero.
  
  //  LDA    - INTEGER.
  //           On entry, LDA specifies the first dimension of A as declared
  //           in the calling (sub) program. LDA must be at least
  //           max( 1, n ).
  //           Unchanged on exit.
  
  
  //  Level 2 Blas routine.
  
  //  -- Written on 22-October-1986.
  //     Jack Dongarra, Argonne National Lab.
  //     Jeremy Du Croz, Nag Central Office.
  //     Sven Hammarling, Nag Central Office.
  //     Richard Hanson, Sandia National Labs.
  
  
  //     .. Parameters ..
  //     .. Local Scalars ..
  //     .. External Functions ..
  //     .. External Subroutines ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  if( !lsame( uplo, 'U' ) && !lsame( uplo, 'L' ) ) { 
    info = 1;
  }
  else if( n < 0 ) { 
    info = 2;
  }
  else if( incx == 0 ) { 
    info = 5;
  }
  else if( incy == 0 ) { 
    info = 7;
  }
  else if( lda < max( 1, n ) ) { 
    info = 9;
  }
  if( info != 0 ) { 
    xerbla( "ZHER2 ", info );
    return;
  }
  
  //     Quick return if possible.
  
  if( (n == 0) || (ctocf(alpha) == ctocf(ZERO)) ) 
    return;
  
  //     Set up the start points in X and Y if the increments are not both
  //     unity.
  
  if( (incx != 1) || (incy != 1) ) { 
    if( incx > 0 ) { 
      kx = 1;
    }
    else { 
      kx = 1 - (n - 1)*incx;
    }
    if( incy > 0 ) { 
      ky = 1;
    }
    else { 
      ky = 1 - (n - 1)*incy;
    }
    jx = kx;
    jy = ky;
  }
  
  //     Start the operations. In this version the elements of A are
  //     accessed sequentially with one pass through the triangular part
  //     of A.
  
  if( lsame( uplo, 'U' ) ) { 
    
    //        Form  A  when A is stored in the upper triangle.
    
    if( (incx == 1) && (incy == 1) ) { 
      for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
        if( (ctocf(x[j_]) != ctocf(ZERO)) || (ctocf(y[j_]) != 
         ctocf(ZERO)) ) { 
          temp1 = alpha*conj( y[j_] );
          temp2 = conj( alpha*x[j_] );
          for( i = 1, i_ = i - 1, _do1 = j - 1; i <= _do1; i++, i_++ ) { 
            A(j_,i_) = A(j_,i_) + x[i_]*temp1 + y[i_]*
             temp2;
          }
          A(j_,j_) = DComplex(real( A(j_,j_) ) + real( x[j_]*
           temp1 + y[j_]*temp2 ));
        }
        else { 
          A(j_,j_) = DComplex(real( A(j_,j_) ));
        }
      }
    }
    else { 
      for( j = 1, j_ = j - 1, _do2 = n; j <= _do2; j++, j_++ ) { 
        if( (ctocf(x[jx - 1]) != ctocf(ZERO)) || (ctocf(y[jy - 1]) != 
         ctocf(ZERO)) ) { 
          temp1 = alpha*conj( y[jy - 1] );
          temp2 = conj( alpha*x[jx - 1] );
          ix = kx;
          iy = ky;
          for( i = 1, i_ = i - 1, _do3 = j - 1; i <= _do3; i++, i_++ ) { 
            A(j_,i_) = A(j_,i_) + x[ix - 1]*temp1 + y[iy - 1]*
             temp2;
            ix = ix + incx;
            iy = iy + incy;
          }
          A(j_,j_) = DComplex(real( A(j_,j_) ) + real( x[jx - 1]*
           temp1 + y[jy - 1]*temp2 ));
        }
        else { 
          A(j_,j_) = DComplex(real( A(j_,j_) ));
        }
        jx = jx + incx;
        jy = jy + incy;
      }
    }
  }
  else { 
    
    //        Form  A  when A is stored in the lower triangle.
    
    if( (incx == 1) && (incy == 1) ) { 
      for( j = 1, j_ = j - 1, _do4 = n; j <= _do4; j++, j_++ ) { 
        if( (ctocf(x[j_]) != ctocf(ZERO)) || (ctocf(y[j_]) != 
         ctocf(ZERO)) ) { 
          temp1 = alpha*conj( y[j_] );
          temp2 = conj( alpha*x[j_] );
          A(j_,j_) = DComplex(real( A(j_,j_) ) + real( x[j_]*
           temp1 + y[j_]*temp2 ));
          for( i = j + 1, i_ = i - 1, _do5 = n; i <= _do5; i++, i_++ ) { 
            A(j_,i_) = A(j_,i_) + x[i_]*temp1 + y[i_]*
             temp2;
          }
        }
        else { 
          A(j_,j_) = DComplex(real( A(j_,j_) ));
        }
      }
    }
    else { 
      for( j = 1, j_ = j - 1, _do6 = n; j <= _do6; j++, j_++ ) { 
        if( (ctocf(x[jx - 1]) != ctocf(ZERO)) || (ctocf(y[jy - 1]) != 
         ctocf(ZERO)) ) { 
          temp1 = alpha*conj( y[jy - 1] );
          temp2 = conj( alpha*x[jx - 1] );
          A(j_,j_) = DComplex(real( A(j_,j_) ) + real( x[jx - 1]*
           temp1 + y[jy - 1]*temp2 ));
          ix = jx;
          iy = jy;
          for( i = j + 1, i_ = i - 1, _do7 = n; i <= _do7; i++, i_++ ) { 
            ix = ix + incx;
            iy = iy + incy;
            A(j_,i_) = A(j_,i_) + x[ix - 1]*temp1 + y[iy - 1]*
             temp2;
          }
        }
        else { 
          A(j_,j_) = DComplex(real( A(j_,j_) ));
        }
        jx = jx + incx;
        jy = jy + incy;
      }
    }
  }
  
  return;
  
  //     End of ZHER2 .
  
#undef  A
} // end of function 


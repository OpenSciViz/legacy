/*
 * Default C++ implementation of xerbla
 * For optimum performance, use a machine specific bla library
 *
 * $Id: xerbla.cpp,v 1.3 1993/03/19 16:26:58 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Rewritten from scratch in C++ to avoid using Fortran I/O.
 * This needs to be set up to throw an exception.
 *
 * $Log: xerbla.cpp,v $
 * Revision 1.3  1993/03/19  16:26:58  alv
 * added RWBLADECL linkage specification
 *
 * Revision 1.2  1993/03/05  23:08:17  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:05:12  alv
 * Initial revision
 *
 */

#include <stdlib.h>
#include <iostream.h>
#define RW_CPPBLAS 1
#include "rw/bla.h"
#include "rw/bla.h"
#include "rw/fortran.h" /* Fortran run time library */

RWBLADECL void /*FUNCTION*/ xerbla(char *srname, const long &info)
{
  //  -- LAPACK auxiliary routine (preliminary version) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  XERBLA  is an error handler for the LAPACK routines.
  //  It is called by an LAPACK routine if an input parameter has an
  //  invalid value.  A message is printed and execution stops.
  
  //  Installers may consider modifying the STOP statement in order to
  //  call system-specific exception-handling facilities.
  
  //  Arguments
  //  =========
  
  //  SRNAME  (input) CHARACTER*6
  //          The name of the routine which called XERBLA.
  
  //  INFO    (input) INTEGER
  //          The position of the invalid parameter in the parameter list
  //          of the calling routine.
  
  cerr <<    " ***\n";
  cerr <<    " *** xerbla called in routine '" << srname;
  cerr << "'\n *** error number is " << info;
  cerr <<  "\n *** aborting\n";
  cerr <<    " *** " << endl;

  exit(info);

} // end of function 


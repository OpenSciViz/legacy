/*
 * Default C++ implementation of dznrm2
 * For optimum performance, use a machine specific bla library
 *
 * $Id: dznrm2.cpp,v 1.3 1993/03/19 16:26:58 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/17/93 at 14:40:06
 * FOR_C++ Options SET: alloc do=rt no=p pf=dbla,xbla s=dv str=l - prototypes
 *
 * $Log: dznrm2.cpp,v $
 * Revision 1.3  1993/03/19  16:26:58  alv
 * added RWBLADECL linkage specification
 *
 * Revision 1.2  1993/03/05  23:07:38  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:04:49  alv
 * Initial revision
 *
 */

#include <rw/dcomplex.h>
#define RW_CPPBLAS 1
#include "rw/bla.h"
#include "rw/bla.h"
#include "rw/fortran.h" /* Fortran run time library */

RWBLADECL double /*FUNCTION*/ dznrm2(const long &n, DComplex zx[], const long &incx)
{
  int imag, scale;
  long _do0, i, ix, ix_, next;
  double absx, dznrm2_v, hitest, sum, xmax;
  DComplex zdumi, zdumr;
  static double zero = 0.0e0;
  static double one = 1.0e0;
  static double cutlo = 8.232e-11;
  static double cuthi = 1.304e19;

  
  //     unitary norm of the DComplex n-vector stored in zx() with storage
  //     increment incx .
  //     if    n .le. 0 return with result = 0.
  //     if n .ge. 1 then incx must be .ge. 1
  
  //           c.l.lawson , 1978 jan 08
  //     modified to correct problem with negative increment, 8/21/90.
  
  //     four phase method     using two built-in constants that are
  //     hopefully applicable to all machines.
  //         cutlo = maximum of  sqrt(u/eps)  over all known machines.
  //         cuthi = minimum of  sqrt(v)      over all known machines.
  //     where
  //         eps = smallest no. such that eps + 1. .gt. 1.
  //         u   = smallest positive no.   (underflow limit)
  //         v   = largest  no.            (overflow  limit)
  
  //     brief outline of algorithm..
  
  //     phase 1    scans zero components.
  //     move to phase 2 when a component is nonzero and .le. cutlo
  //     move to phase 3 when a component is .gt. cutlo
  //     move to phase 4 when a component is .ge. cuthi/m
  //     where m = n for x() real and m = 2*n for DComplex.
  
  //     values for cutlo and cuthi..
  //     from the environmental parameters listed in the imsl converter
  //     document the limiting values are as follows..
  //     cutlo, s.p.   u/eps = 2**(-102) for  honeywell.  close seconds are
  //                   univac and dec at 2**(-103)
  //                   thus cutlo = 2**(-51) = 4.44089e-16
  //     cuthi, s.p.   v = 2**127 for univac, honeywell, and dec.
  //                   thus cuthi = 2**(63.5) = 1.30438e19
  //     cutlo, d.p.   u/eps = 2**(-67) for honeywell and dec.
  //                   thus cutlo = 2**(-33.5) = 8.23181d-11
  //     cuthi, d.p.   same as s.p.  cuthi = 1.30438d19
  //     data cutlo, cuthi / 8.232d-11,  1.304d19 /
  //     data cutlo, cuthi / 4.441e-16,  1.304e19 /
  
  if( n > 0 ) 
    goto L_10;
  dznrm2_v = zero;
  goto L_300;
  
L_10:
  next = 30;
  sum = zero;
  i = 1;
  if( incx < 0 ) 
    i = (-n + 1)*incx + 1;
  //                                                 begin main loop
  for( ix = 1, ix_ = ix - 1, _do0 = n; ix <= _do0; ix++, ix_++ ) { 
    absx = abs( ::real( zx[i - 1] ) );
    imag = FALSE;
    switch( next ) { 
      case 30: goto L_30;
      case 50: goto L_50;
      case 70: goto L_70;
      case 90: goto L_90;
      case 110: goto L_110;
    }
L_30:
    if( absx > cutlo ) 
      goto L_85;
    next = 50;
    scale = FALSE;
    
    //                        phase 1.  sum is zero
    
L_50:
    if( absx == zero ) 
      goto L_200;
    if( absx > cutlo ) 
      goto L_85;
    
    //                                prepare for phase 2.
    next = 70;
    goto L_105;
    
    //                                prepare for phase 4.
    
L_100:
    next = 110;
    sum = (sum/absx)/absx;
L_105:
    scale = TRUE;
    xmax = absx;
    goto L_115;
    
    //                   phase 2.  sum is small.
    //                             scale to avoid destructive underflow.
    
L_70:
    if( absx > cutlo ) 
      goto L_75;
    
    //                     common code for phases 2 and 4.
    //                     in phase 4 sum is large.  scale to avoid overflow.
    
L_110:
    if( absx <= xmax ) 
      goto L_115;
    sum = one + sum*(xmax/absx)*(xmax/absx); //pow(xmax/absx, 2);
    xmax = absx;
    goto L_200;
    
L_115:
    sum = sum + (absx/xmax)*(absx/xmax); //pow(absx/xmax, 2);
    goto L_200;
    
    
    //                  prepare for phase 3.
    
L_75:
    sum = (sum*xmax)*xmax;
    
L_85:
    next = 90;
    scale = FALSE;
    
    //     for real or d.p. set hitest = cuthi/n
    //     for DComplex      set hitest = cuthi/(2*n)
    
    hitest = cuthi/(double)( 2*n );
    
    //                   phase 3.  sum is mid-range.  no scaling.
    
L_90:
    if( absx >= hitest ) 
      goto L_100;
    sum = sum + absx*absx; //pow(absx, 2);
L_200:
    ;
    //                  control selection of real and imaginary parts.
    
    if( imag ) 
      goto L_210;
    absx = abs( ::imag( zx[i - 1] ) );
    imag = TRUE;
    switch( next ) { 
      case 50: goto L_50;
      case 70: goto L_70;
      case 90: goto L_90;
      case 110: goto L_110;
    }
    
L_210:
    ;
    i = i + incx;
  }
  
  //              end of main loop.
  //              compute square root and adjust for scaling.
  
  dznrm2_v = sqrt( sum );
  if( scale ) 
    dznrm2_v = dznrm2_v*xmax;
L_300:
  ;
  return( dznrm2_v );
} // end of function 


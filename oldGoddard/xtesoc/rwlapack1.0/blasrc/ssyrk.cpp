/*
 * Default C++ implementation of ssyrk
 * For optimum performance, use a machine specific bla library
 *
 * $Id: ssyrk.cpp,v 1.3 1993/03/19 16:26:58 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/17/93 at 14:39:10
 * FOR_C++ Options SET: alloc do=rt no=p pf=sbla,xbla s=dv str=l - prototypes
 *
 * $Log: ssyrk.cpp,v $
 * Revision 1.3  1993/03/19  16:26:58  alv
 * added RWBLADECL linkage specification
 *
 * Revision 1.2  1993/03/05  23:08:00  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:05:04  alv
 * Initial revision
 *
 */

#define RW_CPPBLAS 1
#include "rw/bla.h"
#include "rw/bla.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ONE = 1.0e0;
const float ZERO = 0.0e0;
// end of PARAMETER translations

RWBLADECL void /*FUNCTION*/ ssyrk(const char &uplo, const char &trans, const long &n, const long &k, 
 const float &alpha, float *a, const long &lda, const float &beta, float *c, const long &ldc)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define C(I_,J_)  (*(c+(I_)*(ldc)+(J_)))
  int upper;
  long _do0, _do1, _do10, _do11, _do12, _do13, _do14, _do15, 
   _do16, _do17, _do18, _do19, _do2, _do20, _do21, _do22, _do23, 
   _do3, _do4, _do5, _do6, _do7, _do8, _do9, i, i_, info, j, j_, 
   l, l_, nrowa;
  float temp;

  //     .. Scalar Arguments ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SSYRK  performs one of the symmetric rank k operations
  
  //     C := alpha*A*A' + beta*C,
  
  //  or
  
  //     C := alpha*A'*A + beta*C,
  
  //  where  alpha and beta  are scalars, C is an  n by n  symmetric matrix
  //  and  A  is an  n by k  matrix in the first case and a  k by n  matrix
  //  in the second case.
  
  //  Parameters
  //  ==========
  
  //  UPLO   - CHARACTER*1.
  //           On  entry,   UPLO  specifies  whether  the  upper  or  lower
  //           triangular  part  of the  array  C  is to be  referenced  as
  //           follows:
  
  //              UPLO = 'U' or 'u'   Only the  upper triangular part of  C
  //                                  is to be referenced.
  
  //              UPLO = 'L' or 'l'   Only the  lower triangular part of  C
  //                                  is to be referenced.
  
  //           Unchanged on exit.
  
  //  TRANS  - CHARACTER*1.
  //           On entry,  TRANS  specifies the operation to be performed as
  //           follows:
  
  //              TRANS = 'N' or 'n'   C := alpha*A*A' + beta*C.
  
  //              TRANS = 'T' or 't'   C := alpha*A'*A + beta*C.
  
  //              TRANS = 'C' or 'c'   C := alpha*A'*A + beta*C.
  
  //           Unchanged on exit.
  
  //  N      - INTEGER.
  //           On entry,  N specifies the order of the matrix C.  N must be
  //           at least zero.
  //           Unchanged on exit.
  
  //  K      - INTEGER.
  //           On entry with  TRANS = 'N' or 'n',  K  specifies  the number
  //           of  columns   of  the   matrix   A,   and  on   entry   with
  //           TRANS = 'T' or 't' or 'C' or 'c',  K  specifies  the  number
  //           of rows of the matrix  A.  K must be at least zero.
  //           Unchanged on exit.
  
  //  ALPHA  - REAL            .
  //           On entry, ALPHA specifies the scalar alpha.
  //           Unchanged on exit.
  
  //  A      - REAL             array of DIMENSION ( LDA, ka ), where ka is
  //           k  when  TRANS = 'N' or 'n',  and is  n  otherwise.
  //           Before entry with  TRANS = 'N' or 'n',  the  leading  n by k
  //           part of the array  A  must contain the matrix  A,  otherwise
  //           the leading  k by n  part of the array  A  must contain  the
  //           matrix A.
  //           Unchanged on exit.
  
  //  LDA    - INTEGER.
  //           On entry, LDA specifies the first dimension of A as declared
  //           in  the  calling  (sub)  program.   When  TRANS = 'N' or 'n'
  //           then  LDA must be at least  max( 1, n ), otherwise  LDA must
  //           be at least  max( 1, k ).
  //           Unchanged on exit.
  
  //  BETA   - REAL            .
  //           On entry, BETA specifies the scalar beta.
  //           Unchanged on exit.
  
  //  C      - REAL             array of DIMENSION ( LDC, n ).
  //           Before entry  with  UPLO = 'U' or 'u',  the leading  n by n
  //           upper triangular part of the array C must contain the upper
  //           triangular part  of the  symmetric matrix  and the strictly
  //           lower triangular part of C is not referenced.  On exit, the
  //           upper triangular part of the array  C is overwritten by the
  //           upper triangular part of the updated matrix.
  //           Before entry  with  UPLO = 'L' or 'l',  the leading  n by n
  //           lower triangular part of the array C must contain the lower
  //           triangular part  of the  symmetric matrix  and the strictly
  //           upper triangular part of C is not referenced.  On exit, the
  //           lower triangular part of the array  C is overwritten by the
  //           lower triangular part of the updated matrix.
  
  //  LDC    - INTEGER.
  //           On entry, LDC specifies the first dimension of C as declared
  //           in  the  calling  (sub)  program.   LDC  must  be  at  least
  //           max( 1, n ).
  //           Unchanged on exit.
  
  
  //  Level 3 Blas routine.
  
  //  -- Written on 8-February-1989.
  //     Jack Dongarra, Argonne National Laboratory.
  //     Iain Duff, AERE Harwell.
  //     Jeremy Du Croz, Numerical Algorithms Group Ltd.
  //     Sven Hammarling, Numerical Algorithms Group Ltd.
  
  
  //     .. External Functions ..
  //     .. External Subroutines ..
  //     .. Intrinsic Functions ..
  //     .. Local Scalars ..
  //     .. Parameters ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  if( lsame( trans, 'N' ) ) { 
    nrowa = n;
  }
  else { 
    nrowa = k;
  }
  upper = lsame( uplo, 'U' );
  
  info = 0;
  if( (!upper) && (!lsame( uplo, 'L' )) ) { 
    info = 1;
  }
  else if( ((!lsame( trans, 'N' )) && (!lsame( trans, 'T' ))) && 
   (!lsame( trans, 'C' )) ) { 
    info = 2;
  }
  else if( n < 0 ) { 
    info = 3;
  }
  else if( k < 0 ) { 
    info = 4;
  }
  else if( lda < max( 1, nrowa ) ) { 
    info = 7;
  }
  else if( ldc < max( 1, n ) ) { 
    info = 10;
  }
  if( info != 0 ) { 
    xerbla( "SSYRK ", info );
    return;
  }
  
  //     Quick return if possible.
  
  if( (n == 0) || (((alpha == ZERO) || (k == 0)) && (beta == ONE)) ) 
    return;
  
  //     And when  alpha.eq.zero.
  
  if( alpha == ZERO ) { 
    if( upper ) { 
      if( beta == ZERO ) { 
        for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
          for( i = 1, i_ = i - 1, _do1 = j; i <= _do1; i++, i_++ ) { 
            C(j_,i_) = ZERO;
          }
        }
      }
      else { 
        for( j = 1, j_ = j - 1, _do2 = n; j <= _do2; j++, j_++ ) { 
          for( i = 1, i_ = i - 1, _do3 = j; i <= _do3; i++, i_++ ) { 
            C(j_,i_) = beta*C(j_,i_);
          }
        }
      }
    }
    else { 
      if( beta == ZERO ) { 
        for( j = 1, j_ = j - 1, _do4 = n; j <= _do4; j++, j_++ ) { 
          for( i = j, i_ = i - 1, _do5 = n; i <= _do5; i++, i_++ ) { 
            C(j_,i_) = ZERO;
          }
        }
      }
      else { 
        for( j = 1, j_ = j - 1, _do6 = n; j <= _do6; j++, j_++ ) { 
          for( i = j, i_ = i - 1, _do7 = n; i <= _do7; i++, i_++ ) { 
            C(j_,i_) = beta*C(j_,i_);
          }
        }
      }
    }
    return;
  }
  
  //     Start the operations.
  
  if( lsame( trans, 'N' ) ) { 
    
    //        Form  C := alpha*A*A' + beta*C.
    
    if( upper ) { 
      for( j = 1, j_ = j - 1, _do8 = n; j <= _do8; j++, j_++ ) { 
        if( beta == ZERO ) { 
          for( i = 1, i_ = i - 1, _do9 = j; i <= _do9; i++, i_++ ) { 
            C(j_,i_) = ZERO;
          }
        }
        else if( beta != ONE ) { 
          for( i = 1, i_ = i - 1, _do10 = j; i <= _do10; i++, i_++ ) { 
            C(j_,i_) = beta*C(j_,i_);
          }
        }
        for( l = 1, l_ = l - 1, _do11 = k; l <= _do11; l++, l_++ ) { 
          if( A(l_,j_) != ZERO ) { 
            temp = alpha*A(l_,j_);
            for( i = 1, i_ = i - 1, _do12 = j; i <= _do12; i++, i_++ ) { 
              C(j_,i_) = C(j_,i_) + temp*A(l_,i_);
            }
          }
        }
      }
    }
    else { 
      for( j = 1, j_ = j - 1, _do13 = n; j <= _do13; j++, j_++ ) { 
        if( beta == ZERO ) { 
          for( i = j, i_ = i - 1, _do14 = n; i <= _do14; i++, i_++ ) { 
            C(j_,i_) = ZERO;
          }
        }
        else if( beta != ONE ) { 
          for( i = j, i_ = i - 1, _do15 = n; i <= _do15; i++, i_++ ) { 
            C(j_,i_) = beta*C(j_,i_);
          }
        }
        for( l = 1, l_ = l - 1, _do16 = k; l <= _do16; l++, l_++ ) { 
          if( A(l_,j_) != ZERO ) { 
            temp = alpha*A(l_,j_);
            for( i = j, i_ = i - 1, _do17 = n; i <= _do17; i++, i_++ ) { 
              C(j_,i_) = C(j_,i_) + temp*A(l_,i_);
            }
          }
        }
      }
    }
  }
  else { 
    
    //        Form  C := alpha*A'*A + beta*C.
    
    if( upper ) { 
      for( j = 1, j_ = j - 1, _do18 = n; j <= _do18; j++, j_++ ) { 
        for( i = 1, i_ = i - 1, _do19 = j; i <= _do19; i++, i_++ ) { 
          temp = ZERO;
          for( l = 1, l_ = l - 1, _do20 = k; l <= _do20; l++, l_++ ) { 
            temp = temp + A(i_,l_)*A(j_,l_);
          }
          if( beta == ZERO ) { 
            C(j_,i_) = alpha*temp;
          }
          else { 
            C(j_,i_) = alpha*temp + beta*C(j_,i_);
          }
        }
      }
    }
    else { 
      for( j = 1, j_ = j - 1, _do21 = n; j <= _do21; j++, j_++ ) { 
        for( i = j, i_ = i - 1, _do22 = n; i <= _do22; i++, i_++ ) { 
          temp = ZERO;
          for( l = 1, l_ = l - 1, _do23 = k; l <= _do23; l++, l_++ ) { 
            temp = temp + A(i_,l_)*A(j_,l_);
          }
          if( beta == ZERO ) { 
            C(j_,i_) = alpha*temp;
          }
          else { 
            C(j_,i_) = alpha*temp + beta*C(j_,i_);
          }
        }
      }
    }
  }
  
  return;
  
  //     End of SSYRK .
  
#undef  C
#undef  A
} // end of function 


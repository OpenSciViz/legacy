/*
 * Default C++ implementation of dcabs1
 * For optimum performance, use a machine specific bla library
 *
 * $Id: dcabs1.cpp,v 1.3 1993/03/19 16:26:58 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/17/93 at 14:40:06
 * FOR_C++ Options SET: alloc do=rt no=p pf=dbla,xbla s=dv str=l - prototypes
 *
 * $Log: dcabs1.cpp,v $
 * Revision 1.3  1993/03/19  16:26:58  alv
 * added RWBLADECL linkage specification
 *
 * Revision 1.2  1993/03/05  23:07:10  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:04:34  alv
 * Initial revision
 *
 */

#include <rw/dcomplex.h>
#define RW_CPPBLAS 1
#include "rw/bla.h"
#include "rw/bla.h"
#include "rw/fortran.h" /* Fortran run time library */

RWBLADECL double /*FUNCTION*/ dcabs1(const DComplex &z)
{
  double dcabs1_v;
  // EQUIVALENCE translations
  double _e0[2];
  double *const t = (double*)_e0;
  DComplex *const zz = (DComplex*)_e0;
  // end of EQUIVALENCE translations

  *zz = z;
  dcabs1_v = abs( t[0] ) + abs( t[1] );
  return( dcabs1_v );
} // end of function 


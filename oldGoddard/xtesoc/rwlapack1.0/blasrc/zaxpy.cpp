/*
 * Default C++ implementation of zaxpy
 * For optimum performance, use a machine specific bla library
 *
 * $Id: zaxpy.cpp,v 1.3 1993/03/19 16:26:58 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 06:03:43
 * FOR_C++ Options SET: alloc do=rt no=p pf=zbla,dbla,xbla s=dv str=l - prototypes
 *
 * $Log: zaxpy.cpp,v $
 * Revision 1.3  1993/03/19  16:26:58  alv
 * added RWBLADECL linkage specification
 *
 * Revision 1.2  1993/03/05  23:08:18  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:05:12  alv
 * Initial revision
 *
 */

#define RW_CPPBLAS 1
#include "rw/bla.h"
#include "rw/bla.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

RWBLADECL void /*FUNCTION*/ zaxpy(const long &n, const DComplex &za, DComplex zx[], const long &incx, 
   DComplex zy[], const long &incy)
{
  long _do0, _do1, i, i_, ix, iy;

  
  //     constant times a vector plus a vector.
  //     jack dongarra, 3/11/78.
  
  if( n <= 0 ) 
    return;
  if( dcabs1( za ) == 0.0e0 ) 
    return;
  if( incx == 1 && incy == 1 ) 
    goto L_20;
  
  //        code for unequal increments or equal increments
  //          not equal to 1
  
  ix = 1;
  iy = 1;
  if( incx < 0 ) 
    ix = (-n + 1)*incx + 1;
  if( incy < 0 ) 
    iy = (-n + 1)*incy + 1;
  for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
    zy[iy - 1] = zy[iy - 1] + za*zx[ix - 1];
    ix = ix + incx;
    iy = iy + incy;
  }
  return;
  
  //        code for both increments equal to 1
  
L_20:
  for( i = 1, i_ = i - 1, _do1 = n; i <= _do1; i++, i_++ ) { 
    zy[i_] = zy[i_] + za*zx[i_];
  }
  return;
} // end of function 


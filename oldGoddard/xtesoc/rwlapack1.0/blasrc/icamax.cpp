/*
 * Default C++ implementation of icamax
 * For optimum performance, use a machine specific bla library
 *
 * $Id: icamax.cpp,v 1.3 1993/03/19 16:26:58 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/17/93 at 14:41:01
 * FOR_C++ Options SET: alloc do=rt no=p pf=cbla,xbla s=dv str=l - prototypes
 *
 * $Log: icamax.cpp,v $
 * Revision 1.3  1993/03/19  16:26:58  alv
 * added RWBLADECL linkage specification
 *
 * Revision 1.2  1993/03/05  23:07:40  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:04:49  alv
 * Initial revision
 *
 */

#define RW_CPPBLAS 1
#include "rw/bla.h"
#include "rw/bla.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/fcomplex.h"

inline float icamax_cabs1(FComplex zdum) { return abs( real( (zdum) ) ) + 
   abs( imag( (zdum) ) ); }
RWBLADECL long /*FUNCTION*/ icamax(const long &n, FComplex cx[], const long &incx)
{
  long _do0, _do1, i, i_, icamax_v, ix;
  float smax;
  FComplex zdum;

  
  //     finds the index of element having max. absolute value.
  //     jack dongarra, linpack, 3/11/78.
  //     modified to correct problem with negative increment, 8/21/90.
  
  
  icamax_v = 0;
  if( n < 1 ) 
    return( icamax_v );
  icamax_v = 1;
  if( n == 1 ) 
    return( icamax_v );
  if( incx == 1 ) 
    goto L_20;
  
  //        code for increment not equal to 1
  
  ix = 1;
  if( incx < 0 ) 
    ix = (-n + 1)*incx + 1;
  smax = icamax_cabs1( cx[ix - 1] );
  ix = ix + incx;
  for( i = 2, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
    if( icamax_cabs1( cx[ix - 1] ) <= smax ) 
      goto L_5;
    icamax_v = i;
    smax = icamax_cabs1( cx[ix - 1] );
L_5:
    ix = ix + incx;
  }
  return( icamax_v );
  
  //        code for increment equal to 1
  
L_20:
  smax = icamax_cabs1( cx[0] );
  for( i = 2, i_ = i - 1, _do1 = n; i <= _do1; i++, i_++ ) { 
    if( icamax_cabs1( cx[i_] ) <= smax ) 
      goto L_30;
    icamax_v = i;
    smax = icamax_cabs1( cx[i_] );
L_30:
    ;
  }
  return( icamax_v );
} // end of function 


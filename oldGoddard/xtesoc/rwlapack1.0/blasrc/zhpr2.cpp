/*
 * Default C++ implementation of zhpr2
 * For optimum performance, use a machine specific bla library
 *
 * $Id: zhpr2.cpp,v 1.3 1993/03/19 16:26:58 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 06:03:43
 * FOR_C++ Options SET: alloc do=rt no=p pf=zbla,dbla,xbla s=dv str=l - prototypes
 *
 * $Log: zhpr2.cpp,v $
 * Revision 1.3  1993/03/19  16:26:58  alv
 * added RWBLADECL linkage specification
 *
 * Revision 1.2  1993/03/05  23:08:34  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:05:19  alv
 * Initial revision
 *
 */

#define RW_CPPBLAS 1
#include "rw/bla.h"
#include "rw/bla.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const DComplex ZERO = DComplex(0.0e0,0.0e0);
// end of PARAMETER translations

RWBLADECL void /*FUNCTION*/ zhpr2(const char &uplo, const long &n, const DComplex &alpha, DComplex x[], 
 const long &incx, DComplex y[], const long &incy, DComplex ap[])
{
  long _do0, _do1, _do2, _do3, _do4, _do5, _do6, _do7, i, i_, 
   info, ix, iy, j, j_, jx, jy, k, k_, kk, kx, ky;
  DComplex temp1, temp2;

  //     .. Scalar Arguments ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZHPR2  performs the hermitian rank 2 operation
  
  //     A := alpha*x*conjg( y' ) + conjg( alpha )*y*conjg( x' ) + A,
  
  //  where alpha is a scalar, x and y are n element vectors and A is an
  //  n by n hermitian matrix, supplied in packed form.
  
  //  Parameters
  //  ==========
  
  //  UPLO   - CHARACTER*1.
  //           On entry, UPLO specifies whether the upper or lower
  //           triangular part of the matrix A is supplied in the packed
  //           array AP as follows:
  
  //              UPLO = 'U' or 'u'   The upper triangular part of A is
  //                                  supplied in AP.
  
  //              UPLO = 'L' or 'l'   The lower triangular part of A is
  //                                  supplied in AP.
  
  //           Unchanged on exit.
  
  //  N      - INTEGER.
  //           On entry, N specifies the order of the matrix A.
  //           N must be at least zero.
  //           Unchanged on exit.
  
  //  ALPHA  - COMPLEX*16      .
  //           On entry, ALPHA specifies the scalar alpha.
  //           Unchanged on exit.
  
  //  X      - COMPLEX*16       array of dimension at least
  //           ( 1 + ( n - 1 )*abs( INCX ) ).
  //           Before entry, the incremented array X must contain the n
  //           element vector x.
  //           Unchanged on exit.
  
  //  INCX   - INTEGER.
  //           On entry, INCX specifies the increment for the elements of
  //           X. INCX must not be zero.
  //           Unchanged on exit.
  
  //  Y      - COMPLEX*16       array of dimension at least
  //           ( 1 + ( n - 1 )*abs( INCY ) ).
  //           Before entry, the incremented array Y must contain the n
  //           element vector y.
  //           Unchanged on exit.
  
  //  INCY   - INTEGER.
  //           On entry, INCY specifies the increment for the elements of
  //           Y. INCY must not be zero.
  //           Unchanged on exit.
  
  //  AP     - COMPLEX*16       array of DIMENSION at least
  //           ( ( n*( n + 1 ) )/2 ).
  //           Before entry with  UPLO = 'U' or 'u', the array AP must
  //           contain the upper triangular part of the hermitian matrix
  //           packed sequentially, column by column, so that AP( 1 )
  //           contains a( 1, 1 ), AP( 2 ) and AP( 3 ) contain a( 1, 2 )
  //           and a( 2, 2 ) respectively, and so on. On exit, the array
  //           AP is overwritten by the upper triangular part of the
  //           updated matrix.
  //           Before entry with UPLO = 'L' or 'l', the array AP must
  //           contain the lower triangular part of the hermitian matrix
  //           packed sequentially, column by column, so that AP( 1 )
  //           contains a( 1, 1 ), AP( 2 ) and AP( 3 ) contain a( 2, 1 )
  //           and a( 3, 1 ) respectively, and so on. On exit, the array
  //           AP is overwritten by the lower triangular part of the
  //           updated matrix.
  //           Note that the imaginary parts of the diagonal elements need
  //           not be set, they are assumed to be zero, and on exit they
  //           are set to zero.
  
  
  //  Level 2 Blas routine.
  
  //  -- Written on 22-October-1986.
  //     Jack Dongarra, Argonne National Lab.
  //     Jeremy Du Croz, Nag Central Office.
  //     Sven Hammarling, Nag Central Office.
  //     Richard Hanson, Sandia National Labs.
  
  
  //     .. Parameters ..
  //     .. Local Scalars ..
  //     .. External Functions ..
  //     .. External Subroutines ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  if( !lsame( uplo, 'U' ) && !lsame( uplo, 'L' ) ) { 
    info = 1;
  }
  else if( n < 0 ) { 
    info = 2;
  }
  else if( incx == 0 ) { 
    info = 5;
  }
  else if( incy == 0 ) { 
    info = 7;
  }
  if( info != 0 ) { 
    xerbla( "ZHPR2 ", info );
    return;
  }
  
  //     Quick return if possible.
  
  if( (n == 0) || (ctocf(alpha) == ctocf(ZERO)) ) 
    return;
  
  //     Set up the start points in X and Y if the increments are not both
  //     unity.
  
  if( (incx != 1) || (incy != 1) ) { 
    if( incx > 0 ) { 
      kx = 1;
    }
    else { 
      kx = 1 - (n - 1)*incx;
    }
    if( incy > 0 ) { 
      ky = 1;
    }
    else { 
      ky = 1 - (n - 1)*incy;
    }
    jx = kx;
    jy = ky;
  }
  
  //     Start the operations. In this version the elements of the array AP
  //     are accessed sequentially with one pass through AP.
  
  kk = 1;
  if( lsame( uplo, 'U' ) ) { 
    
    //        Form  A  when upper triangle is stored in AP.
    
    if( (incx == 1) && (incy == 1) ) { 
      for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
        if( (ctocf(x[j_]) != ctocf(ZERO)) || (ctocf(y[j_]) != 
         ctocf(ZERO)) ) { 
          temp1 = alpha*conj( y[j_] );
          temp2 = conj( alpha*x[j_] );
          k = kk;
          for( i = 1, i_ = i - 1, _do1 = j - 1; i <= _do1; i++, i_++ ) { 
            ap[k - 1] = ap[k - 1] + x[i_]*temp1 + y[i_]*
             temp2;
            k = k + 1;
          }
          ap[kk + j_ - 1] = DComplex(real( ap[kk + j_ - 1] ) + 
           real( x[j_]*temp1 + y[j_]*temp2 ));
        }
        else { 
          ap[kk + j_ - 1] = DComplex(real( ap[kk + j_ - 1] ));
        }
        kk = kk + j;
      }
    }
    else { 
      for( j = 1, j_ = j - 1, _do2 = n; j <= _do2; j++, j_++ ) { 
        if( (ctocf(x[jx - 1]) != ctocf(ZERO)) || (ctocf(y[jy - 1]) != 
         ctocf(ZERO)) ) { 
          temp1 = alpha*conj( y[jy - 1] );
          temp2 = conj( alpha*x[jx - 1] );
          ix = kx;
          iy = ky;
          for( k = kk, k_ = k - 1, _do3 = kk + j - 2; k <= _do3; k++, k_++ ) { 
            ap[k_] = ap[k_] + x[ix - 1]*temp1 + y[iy - 1]*
             temp2;
            ix = ix + incx;
            iy = iy + incy;
          }
          ap[kk + j_ - 1] = DComplex(real( ap[kk + j_ - 1] ) + 
           real( x[jx - 1]*temp1 + y[jy - 1]*temp2 ));
        }
        else { 
          ap[kk + j_ - 1] = DComplex(real( ap[kk + j_ - 1] ));
        }
        jx = jx + incx;
        jy = jy + incy;
        kk = kk + j;
      }
    }
  }
  else { 
    
    //        Form  A  when lower triangle is stored in AP.
    
    if( (incx == 1) && (incy == 1) ) { 
      for( j = 1, j_ = j - 1, _do4 = n; j <= _do4; j++, j_++ ) { 
        if( (ctocf(x[j_]) != ctocf(ZERO)) || (ctocf(y[j_]) != 
         ctocf(ZERO)) ) { 
          temp1 = alpha*conj( y[j_] );
          temp2 = conj( alpha*x[j_] );
          ap[kk - 1] = DComplex(real( ap[kk - 1] ) + real( x[j_]*
           temp1 + y[j_]*temp2 ));
          k = kk + 1;
          for( i = j + 1, i_ = i - 1, _do5 = n; i <= _do5; i++, i_++ ) { 
            ap[k - 1] = ap[k - 1] + x[i_]*temp1 + y[i_]*
             temp2;
            k = k + 1;
          }
        }
        else { 
          ap[kk - 1] = DComplex(real( ap[kk - 1] ));
        }
        kk = kk + n - j + 1;
      }
    }
    else { 
      for( j = 1, j_ = j - 1, _do6 = n; j <= _do6; j++, j_++ ) { 
        if( (ctocf(x[jx - 1]) != ctocf(ZERO)) || (ctocf(y[jy - 1]) != 
         ctocf(ZERO)) ) { 
          temp1 = alpha*conj( y[jy - 1] );
          temp2 = conj( alpha*x[jx - 1] );
          ap[kk - 1] = DComplex(real( ap[kk - 1] ) + real( x[jx - 1]*
           temp1 + y[jy - 1]*temp2 ));
          ix = jx;
          iy = jy;
          for( k = kk + 1, k_ = k - 1, _do7 = kk + n - j; k <= _do7; k++, k_++ ) { 
            ix = ix + incx;
            iy = iy + incy;
            ap[k_] = ap[k_] + x[ix - 1]*temp1 + y[iy - 1]*
             temp2;
          }
        }
        else { 
          ap[kk - 1] = DComplex(real( ap[kk - 1] ));
        }
        jx = jx + incx;
        jy = jy + incy;
        kk = kk + n - j + 1;
      }
    }
  }
  
  return;
  
  //     End of ZHPR2 .
  
} // end of function 


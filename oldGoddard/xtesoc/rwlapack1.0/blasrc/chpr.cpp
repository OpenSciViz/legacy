/*
 * Default C++ implementation of chpr
 * For optimum performance, use a machine specific bla library
 *
 * $Id: chpr.cpp,v 1.3 1993/03/19 16:26:58 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 06:02:50
 * FOR_C++ Options SET: alloc do=rt no=p pf=cbla,sbla,xbla s=dv str=l - prototypes
 *
 * $Log: chpr.cpp,v $
 * Revision 1.3  1993/03/19  16:26:58  alv
 * added RWBLADECL linkage specification
 *
 * Revision 1.2  1993/03/05  23:06:56  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:04:26  alv
 * Initial revision
 *
 */

#define RW_CPPBLAS 1
#include "rw/bla.h"
#include "rw/bla.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/fcomplex.h"

// PARAMETER translations
const FComplex ZERO = FComplex(0.0e0,0.0e0);
// end of PARAMETER translations

RWBLADECL void /*FUNCTION*/ chpr(const char &uplo, const long &n, const float &alpha, FComplex x[], 
 const long &incx, FComplex ap[])
{
  long _do0, _do1, _do2, _do3, _do4, _do5, _do6, _do7, i, i_, 
   info, ix, j, j_, jx, k, k_, kk, kx;
  FComplex temp;

  //     .. Scalar Arguments ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  CHPR    performs the hermitian rank 1 operation
  
  //     A := alpha*x*conjg( x' ) + A,
  
  //  where alpha is a real scalar, x is an n element vector and A is an
  //  n by n hermitian matrix, supplied in packed form.
  
  //  Parameters
  //  ==========
  
  //  UPLO   - CHARACTER*1.
  //           On entry, UPLO specifies whether the upper or lower
  //           triangular part of the matrix A is supplied in the packed
  //           array AP as follows:
  
  //              UPLO = 'U' or 'u'   The upper triangular part of A is
  //                                  supplied in AP.
  
  //              UPLO = 'L' or 'l'   The lower triangular part of A is
  //                                  supplied in AP.
  
  //           Unchanged on exit.
  
  //  N      - INTEGER.
  //           On entry, N specifies the order of the matrix A.
  //           N must be at least zero.
  //           Unchanged on exit.
  
  //  ALPHA  - REAL            .
  //           On entry, ALPHA specifies the scalar alpha.
  //           Unchanged on exit.
  
  //  X      - COMPLEX          array of dimension at least
  //           ( 1 + ( n - 1 )*abs( INCX ) ).
  //           Before entry, the incremented array X must contain the n
  //           element vector x.
  //           Unchanged on exit.
  
  //  INCX   - INTEGER.
  //           On entry, INCX specifies the increment for the elements of
  //           X. INCX must not be zero.
  //           Unchanged on exit.
  
  //  AP     - COMPLEX          array of DIMENSION at least
  //           ( ( n*( n + 1 ) )/2 ).
  //           Before entry with  UPLO = 'U' or 'u', the array AP must
  //           contain the upper triangular part of the hermitian matrix
  //           packed sequentially, column by column, so that AP( 1 )
  //           contains a( 1, 1 ), AP( 2 ) and AP( 3 ) contain a( 1, 2 )
  //           and a( 2, 2 ) respectively, and so on. On exit, the array
  //           AP is overwritten by the upper triangular part of the
  //           updated matrix.
  //           Before entry with UPLO = 'L' or 'l', the array AP must
  //           contain the lower triangular part of the hermitian matrix
  //           packed sequentially, column by column, so that AP( 1 )
  //           contains a( 1, 1 ), AP( 2 ) and AP( 3 ) contain a( 2, 1 )
  //           and a( 3, 1 ) respectively, and so on. On exit, the array
  //           AP is overwritten by the lower triangular part of the
  //           updated matrix.
  //           Note that the imaginary parts of the diagonal elements need
  //           not be set, they are assumed to be zero, and on exit they
  //           are set to zero.
  
  
  //  Level 2 Blas routine.
  
  //  -- Written on 22-October-1986.
  //     Jack Dongarra, Argonne National Lab.
  //     Jeremy Du Croz, Nag Central Office.
  //     Sven Hammarling, Nag Central Office.
  //     Richard Hanson, Sandia National Labs.
  
  
  //     .. Parameters ..
  //     .. Local Scalars ..
  //     .. External Functions ..
  //     .. External Subroutines ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  if( !lsame( uplo, 'U' ) && !lsame( uplo, 'L' ) ) { 
    info = 1;
  }
  else if( n < 0 ) { 
    info = 2;
  }
  else if( incx == 0 ) { 
    info = 5;
  }
  if( info != 0 ) { 
    xerbla( "CHPR  ", info );
    return;
  }
  
  //     Quick return if possible.
  
  if( (n == 0) || (alpha == real( ZERO )) ) 
    return;
  
  //     Set the start point in X if the increment is not unity.
  
  if( incx <= 0 ) { 
    kx = 1 - (n - 1)*incx;
  }
  else if( incx != 1 ) { 
    kx = 1;
  }
  
  //     Start the operations. In this version the elements of the array AP
  //     are accessed sequentially with one pass through AP.
  
  kk = 1;
  if( lsame( uplo, 'U' ) ) { 
    
    //        Form  A  when upper triangle is stored in AP.
    
    if( incx == 1 ) { 
      for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
        if( x[j_] != ZERO ) { 
          temp = alpha*conj( x[j_] );
          k = kk;
          for( i = 1, i_ = i - 1, _do1 = j - 1; i <= _do1; i++, i_++ ) { 
            ap[k - 1] = ap[k - 1] + x[i_]*temp;
            k = k + 1;
          }
          ap[kk + j_ - 1] = FComplex(real( ap[kk + j_ - 1] ) + 
           real( x[j_]*temp ));
        }
        else { 
          ap[kk + j_ - 1] = FComplex(real( ap[kk + j_ - 1] ));
        }
        kk = kk + j;
      }
    }
    else { 
      jx = kx;
      for( j = 1, j_ = j - 1, _do2 = n; j <= _do2; j++, j_++ ) { 
        if( x[jx - 1] != ZERO ) { 
          temp = alpha*conj( x[jx - 1] );
          ix = kx;
          for( k = kk, k_ = k - 1, _do3 = kk + j - 2; k <= _do3; k++, k_++ ) { 
            ap[k_] = ap[k_] + x[ix - 1]*temp;
            ix = ix + incx;
          }
          ap[kk + j_ - 1] = FComplex(real( ap[kk + j_ - 1] ) + 
           real( x[jx - 1]*temp ));
        }
        else { 
          ap[kk + j_ - 1] = FComplex(real( ap[kk + j_ - 1] ));
        }
        jx = jx + incx;
        kk = kk + j;
      }
    }
  }
  else { 
    
    //        Form  A  when lower triangle is stored in AP.
    
    if( incx == 1 ) { 
      for( j = 1, j_ = j - 1, _do4 = n; j <= _do4; j++, j_++ ) { 
        if( x[j_] != ZERO ) { 
          temp = alpha*conj( x[j_] );
          ap[kk - 1] = FComplex(real( ap[kk - 1] ) + real( temp*
           x[j_] ));
          k = kk + 1;
          for( i = j + 1, i_ = i - 1, _do5 = n; i <= _do5; i++, i_++ ) { 
            ap[k - 1] = ap[k - 1] + x[i_]*temp;
            k = k + 1;
          }
        }
        else { 
          ap[kk - 1] = FComplex(real( ap[kk - 1] ));
        }
        kk = kk + n - j + 1;
      }
    }
    else { 
      jx = kx;
      for( j = 1, j_ = j - 1, _do6 = n; j <= _do6; j++, j_++ ) { 
        if( x[jx - 1] != ZERO ) { 
          temp = alpha*conj( x[jx - 1] );
          ap[kk - 1] = FComplex(real( ap[kk - 1] ) + real( temp*
           x[jx - 1] ));
          ix = jx;
          for( k = kk + 1, k_ = k - 1, _do7 = kk + n - j; k <= _do7; k++, k_++ ) { 
            ix = ix + incx;
            ap[k_] = ap[k_] + x[ix - 1]*temp;
          }
        }
        else { 
          ap[kk - 1] = FComplex(real( ap[kk - 1] ));
        }
        jx = jx + incx;
        kk = kk + n - j + 1;
      }
    }
  }
  
  return;
  
  //     End of CHPR  .
  
} // end of function 


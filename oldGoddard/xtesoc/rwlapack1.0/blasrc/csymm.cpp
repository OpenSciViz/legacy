/*
 * Default C++ implementation of csymm
 * For optimum performance, use a machine specific bla library
 *
 * $Id: csymm.cpp,v 1.3 1993/03/19 16:26:58 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 06:02:50
 * FOR_C++ Options SET: alloc do=rt no=p pf=cbla,sbla,xbla s=dv str=l - prototypes
 *
 * $Log: csymm.cpp,v $
 * Revision 1.3  1993/03/19  16:26:58  alv
 * added RWBLADECL linkage specification
 *
 * Revision 1.2  1993/03/05  23:07:01  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:04:29  alv
 * Initial revision
 *
 */

#define RW_CPPBLAS 1
#include "rw/bla.h"
#include "rw/bla.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/fcomplex.h"

// PARAMETER translations
const FComplex ONE = FComplex(1.0e0,0.0e0);
const FComplex ZERO = FComplex(0.0e0,0.0e0);
// end of PARAMETER translations

RWBLADECL void /*FUNCTION*/ csymm(const char &side, const char &uplo, const long &m, const long &n, 
 const FComplex &alpha, FComplex *a, const long &lda, FComplex *b, const long &ldb, 
 const FComplex &beta, FComplex *c, const long &ldc)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
#define C(I_,J_)  (*(c+(I_)*(ldc)+(J_)))
  int upper;
  long _do0, _do1, _do10, _do11, _do12, _do13, _do14, _do15, 
   _do2, _do3, _do4, _do5, _do6, _do7, _do8, _do9, i, i_, info, 
   j, j_, k, k_, nrowa;
  FComplex temp1, temp2;

  //     .. Scalar Arguments ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  CSYMM  performs one of the matrix-matrix operations
  
  //     C := alpha*A*B + beta*C,
  
  //  or
  
  //     C := alpha*B*A + beta*C,
  
  //  where  alpha and beta are scalars, A is a symmetric matrix and  B and
  //  C are m by n matrices.
  
  //  Parameters
  //  ==========
  
  //  SIDE   - CHARACTER*1.
  //           On entry,  SIDE  specifies whether  the  symmetric matrix  A
  //           appears on the  left or right  in the  operation as follows:
  
  //              SIDE = 'L' or 'l'   C := alpha*A*B + beta*C,
  
  //              SIDE = 'R' or 'r'   C := alpha*B*A + beta*C,
  
  //           Unchanged on exit.
  
  //  UPLO   - CHARACTER*1.
  //           On  entry,   UPLO  specifies  whether  the  upper  or  lower
  //           triangular  part  of  the  symmetric  matrix   A  is  to  be
  //           referenced as follows:
  
  //              UPLO = 'U' or 'u'   Only the upper triangular part of the
  //                                  symmetric matrix is to be referenced.
  
  //              UPLO = 'L' or 'l'   Only the lower triangular part of the
  //                                  symmetric matrix is to be referenced.
  
  //           Unchanged on exit.
  
  //  M      - INTEGER.
  //           On entry,  M  specifies the number of rows of the matrix  C.
  //           M  must be at least zero.
  //           Unchanged on exit.
  
  //  N      - INTEGER.
  //           On entry, N specifies the number of columns of the matrix C.
  //           N  must be at least zero.
  //           Unchanged on exit.
  
  //  ALPHA  - COMPLEX         .
  //           On entry, ALPHA specifies the scalar alpha.
  //           Unchanged on exit.
  
  //  A      - COMPLEX          array of DIMENSION ( LDA, ka ), where ka is
  //           m  when  SIDE = 'L' or 'l'  and is n  otherwise.
  //           Before entry  with  SIDE = 'L' or 'l',  the  m by m  part of
  //           the array  A  must contain the  symmetric matrix,  such that
  //           when  UPLO = 'U' or 'u', the leading m by m upper triangular
  //           part of the array  A  must contain the upper triangular part
  //           of the  symmetric matrix and the  strictly  lower triangular
  //           part of  A  is not referenced,  and when  UPLO = 'L' or 'l',
  //           the leading  m by m  lower triangular part  of the  array  A
  //           must  contain  the  lower triangular part  of the  symmetric
  //           matrix and the  strictly upper triangular part of  A  is not
  //           referenced.
  //           Before entry  with  SIDE = 'R' or 'r',  the  n by n  part of
  //           the array  A  must contain the  symmetric matrix,  such that
  //           when  UPLO = 'U' or 'u', the leading n by n upper triangular
  //           part of the array  A  must contain the upper triangular part
  //           of the  symmetric matrix and the  strictly  lower triangular
  //           part of  A  is not referenced,  and when  UPLO = 'L' or 'l',
  //           the leading  n by n  lower triangular part  of the  array  A
  //           must  contain  the  lower triangular part  of the  symmetric
  //           matrix and the  strictly upper triangular part of  A  is not
  //           referenced.
  //           Unchanged on exit.
  
  //  LDA    - INTEGER.
  //           On entry, LDA specifies the first dimension of A as declared
  //           in the  calling (sub) program. When  SIDE = 'L' or 'l'  then
  //           LDA must be at least  max( 1, m ), otherwise  LDA must be at
  //           least max( 1, n ).
  //           Unchanged on exit.
  
  //  B      - COMPLEX          array of DIMENSION ( LDB, n ).
  //           Before entry, the leading  m by n part of the array  B  must
  //           contain the matrix B.
  //           Unchanged on exit.
  
  //  LDB    - INTEGER.
  //           On entry, LDB specifies the first dimension of B as declared
  //           in  the  calling  (sub)  program.   LDB  must  be  at  least
  //           max( 1, m ).
  //           Unchanged on exit.
  
  //  BETA   - COMPLEX         .
  //           On entry,  BETA  specifies the scalar  beta.  When  BETA  is
  //           supplied as zero then C need not be set on input.
  //           Unchanged on exit.
  
  //  C      - COMPLEX          array of DIMENSION ( LDC, n ).
  //           Before entry, the leading  m by n  part of the array  C must
  //           contain the matrix  C,  except when  beta  is zero, in which
  //           case C need not be set on entry.
  //           On exit, the array  C  is overwritten by the  m by n updated
  //           matrix.
  
  //  LDC    - INTEGER.
  //           On entry, LDC specifies the first dimension of C as declared
  //           in  the  calling  (sub)  program.   LDC  must  be  at  least
  //           max( 1, m ).
  //           Unchanged on exit.
  
  
  //  Level 3 Blas routine.
  
  //  -- Written on 8-February-1989.
  //     Jack Dongarra, Argonne National Laboratory.
  //     Iain Duff, AERE Harwell.
  //     Jeremy Du Croz, Numerical Algorithms Group Ltd.
  //     Sven Hammarling, Numerical Algorithms Group Ltd.
  
  
  //     .. External Functions ..
  //     .. External Subroutines ..
  //     .. Intrinsic Functions ..
  //     .. Local Scalars ..
  //     .. Parameters ..
  //     ..
  //     .. Executable Statements ..
  
  //     Set NROWA as the number of rows of A.
  
  if( lsame( side, 'L' ) ) { 
    nrowa = m;
  }
  else { 
    nrowa = n;
  }
  upper = lsame( uplo, 'U' );
  
  //     Test the input parameters.
  
  info = 0;
  if( (!lsame( side, 'L' )) && (!lsame( side, 'R' )) ) { 
    info = 1;
  }
  else if( (!upper) && (!lsame( uplo, 'L' )) ) { 
    info = 2;
  }
  else if( m < 0 ) { 
    info = 3;
  }
  else if( n < 0 ) { 
    info = 4;
  }
  else if( lda < max( 1, nrowa ) ) { 
    info = 7;
  }
  else if( ldb < max( 1, m ) ) { 
    info = 9;
  }
  else if( ldc < max( 1, m ) ) { 
    info = 12;
  }
  if( info != 0 ) { 
    xerbla( "CSYMM ", info );
    return;
  }
  
  //     Quick return if possible.
  
  if( ((m == 0) || (n == 0)) || ((alpha == ZERO) && (beta == ONE)) ) 
    return;
  
  //     And when  alpha.eq.zero.
  
  if( alpha == ZERO ) { 
    if( beta == ZERO ) { 
      for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
        for( i = 1, i_ = i - 1, _do1 = m; i <= _do1; i++, i_++ ) { 
          C(j_,i_) = ZERO;
        }
      }
    }
    else { 
      for( j = 1, j_ = j - 1, _do2 = n; j <= _do2; j++, j_++ ) { 
        for( i = 1, i_ = i - 1, _do3 = m; i <= _do3; i++, i_++ ) { 
          C(j_,i_) = beta*C(j_,i_);
        }
      }
    }
    return;
  }
  
  //     Start the operations.
  
  if( lsame( side, 'L' ) ) { 
    
    //        Form  C := alpha*A*B + beta*C.
    
    if( upper ) { 
      for( j = 1, j_ = j - 1, _do4 = n; j <= _do4; j++, j_++ ) { 
        for( i = 1, i_ = i - 1, _do5 = m; i <= _do5; i++, i_++ ) { 
          temp1 = alpha*B(j_,i_);
          temp2 = ZERO;
          for( k = 1, k_ = k - 1, _do6 = i - 1; k <= _do6; k++, k_++ ) { 
            C(j_,k_) = C(j_,k_) + temp1*A(i_,k_);
            temp2 = temp2 + B(j_,k_)*A(i_,k_);
          }
          if( beta == ZERO ) { 
            C(j_,i_) = temp1*A(i_,i_) + alpha*temp2;
          }
          else { 
            C(j_,i_) = beta*C(j_,i_) + temp1*A(i_,i_) + 
             alpha*temp2;
          }
        }
      }
    }
    else { 
      for( j = 1, j_ = j - 1, _do7 = n; j <= _do7; j++, j_++ ) { 
        for( i = m, i_ = i - 1; i >= 1; i--, i_-- ) { 
          temp1 = alpha*B(j_,i_);
          temp2 = ZERO;
          for( k = i + 1, k_ = k - 1, _do8 = m; k <= _do8; k++, k_++ ) { 
            C(j_,k_) = C(j_,k_) + temp1*A(i_,k_);
            temp2 = temp2 + B(j_,k_)*A(i_,k_);
          }
          if( beta == ZERO ) { 
            C(j_,i_) = temp1*A(i_,i_) + alpha*temp2;
          }
          else { 
            C(j_,i_) = beta*C(j_,i_) + temp1*A(i_,i_) + 
             alpha*temp2;
          }
        }
      }
    }
  }
  else { 
    
    //        Form  C := alpha*B*A + beta*C.
    
    for( j = 1, j_ = j - 1, _do9 = n; j <= _do9; j++, j_++ ) { 
      temp1 = alpha*A(j_,j_);
      if( beta == ZERO ) { 
        for( i = 1, i_ = i - 1, _do10 = m; i <= _do10; i++, i_++ ) { 
          C(j_,i_) = temp1*B(j_,i_);
        }
      }
      else { 
        for( i = 1, i_ = i - 1, _do11 = m; i <= _do11; i++, i_++ ) { 
          C(j_,i_) = beta*C(j_,i_) + temp1*B(j_,i_);
        }
      }
      for( k = 1, k_ = k - 1, _do12 = j - 1; k <= _do12; k++, k_++ ) { 
        if( upper ) { 
          temp1 = alpha*A(j_,k_);
        }
        else { 
          temp1 = alpha*A(k_,j_);
        }
        for( i = 1, i_ = i - 1, _do13 = m; i <= _do13; i++, i_++ ) { 
          C(j_,i_) = C(j_,i_) + temp1*B(k_,i_);
        }
      }
      for( k = j + 1, k_ = k - 1, _do14 = n; k <= _do14; k++, k_++ ) { 
        if( upper ) { 
          temp1 = alpha*A(k_,j_);
        }
        else { 
          temp1 = alpha*A(j_,k_);
        }
        for( i = 1, i_ = i - 1, _do15 = m; i <= _do15; i++, i_++ ) { 
          C(j_,i_) = C(j_,i_) + temp1*B(k_,i_);
        }
      }
    }
  }
  
  return;
  
  //     End of CSYMM .
  
#undef  C
#undef  B
#undef  A
} // end of function 


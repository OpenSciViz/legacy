/*
 * Default C++ implementation of ztrmm
 * For optimum performance, use a machine specific bla library
 *
 * $Id: ztrmm.cpp,v 1.3 1993/03/19 16:26:58 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 06:03:43
 * FOR_C++ Options SET: alloc do=rt no=p pf=zbla,dbla,xbla s=dv str=l - prototypes
 *
 * $Log: ztrmm.cpp,v $
 * Revision 1.3  1993/03/19  16:26:58  alv
 * added RWBLADECL linkage specification
 *
 * Revision 1.2  1993/03/05  23:08:42  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:05:23  alv
 * Initial revision
 *
 */

#define RW_CPPBLAS 1
#include "rw/bla.h"
#include "rw/bla.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const DComplex ONE = DComplex(1.0e0,0.0e0);
const DComplex ZERO = DComplex(0.0e0,0.0e0);
// end of PARAMETER translations

RWBLADECL void /*FUNCTION*/ ztrmm(const char &side, const char &uplo, const char &transa, const char &diag, 
 const long &m, const long &n, const DComplex &alpha, DComplex *a, const long &lda, 
 DComplex *b, const long &ldb)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
  int lside, noconj, nounit, upper;
  long _do0, _do1, _do10, _do11, _do12, _do13, _do14, _do15, 
   _do16, _do17, _do18, _do19, _do2, _do20, _do21, _do22, _do23, 
   _do24, _do25, _do26, _do27, _do3, _do4, _do5, _do6, _do7, _do8, 
   _do9, i, i_, info, j, j_, k, k_, nrowa;
  DComplex temp;

  //     .. Scalar Arguments ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZTRMM  performs one of the matrix-matrix operations
  
  //     B := alpha*op( A )*B,   or   B := alpha*B*op( A )
  
  //  where  alpha  is a scalar,  B  is an m by n matrix,  A  is a unit, or
  //  non-unit,  upper or lower triangular matrix  and  op( A )  is one  of
  
  //     op( A ) = A   or   op( A ) = A'   or   op( A ) = conjg( A' ).
  
  //  Parameters
  //  ==========
  
  //  SIDE   - CHARACTER*1.
  //           On entry,  SIDE specifies whether  op( A ) multiplies B from
  //           the left or right as follows:
  
  //              SIDE = 'L' or 'l'   B := alpha*op( A )*B.
  
  //              SIDE = 'R' or 'r'   B := alpha*B*op( A ).
  
  //           Unchanged on exit.
  
  //  UPLO   - CHARACTER*1.
  //           On entry, UPLO specifies whether the matrix A is an upper or
  //           lower triangular matrix as follows:
  
  //              UPLO = 'U' or 'u'   A is an upper triangular matrix.
  
  //              UPLO = 'L' or 'l'   A is a lower triangular matrix.
  
  //           Unchanged on exit.
  
  //  TRANSA - CHARACTER*1.
  //           On entry, TRANSA specifies the form of op( A ) to be used in
  //           the matrix multiplication as follows:
  
  //              TRANSA = 'N' or 'n'   op( A ) = A.
  
  //              TRANSA = 'T' or 't'   op( A ) = A'.
  
  //              TRANSA = 'C' or 'c'   op( A ) = conjg( A' ).
  
  //           Unchanged on exit.
  
  //  DIAG   - CHARACTER*1.
  //           On entry, DIAG specifies whether or not A is unit triangular
  //           as follows:
  
  //              DIAG = 'U' or 'u'   A is assumed to be unit triangular.
  
  //              DIAG = 'N' or 'n'   A is not assumed to be unit
  //                                  triangular.
  
  //           Unchanged on exit.
  
  //  M      - INTEGER.
  //           On entry, M specifies the number of rows of B. M must be at
  //           least zero.
  //           Unchanged on exit.
  
  //  N      - INTEGER.
  //           On entry, N specifies the number of columns of B.  N must be
  //           at least zero.
  //           Unchanged on exit.
  
  //  ALPHA  - COMPLEX*16      .
  //           On entry,  ALPHA specifies the scalar  alpha. When  alpha is
  //           zero then  A is not referenced and  B need not be set before
  //           entry.
  //           Unchanged on exit.
  
  //  A      - COMPLEX*16       array of DIMENSION ( LDA, k ), where k is m
  //           when  SIDE = 'L' or 'l'  and is  n  when  SIDE = 'R' or 'r'.
  //           Before entry  with  UPLO = 'U' or 'u',  the  leading  k by k
  //           upper triangular part of the array  A must contain the upper
  //           triangular matrix  and the strictly lower triangular part of
  //           A is not referenced.
  //           Before entry  with  UPLO = 'L' or 'l',  the  leading  k by k
  //           lower triangular part of the array  A must contain the lower
  //           triangular matrix  and the strictly upper triangular part of
  //           A is not referenced.
  //           Note that when  DIAG = 'U' or 'u',  the diagonal elements of
  //           A  are not referenced either,  but are assumed to be  unity.
  //           Unchanged on exit.
  
  //  LDA    - INTEGER.
  //           On entry, LDA specifies the first dimension of A as declared
  //           in the calling (sub) program.  When  SIDE = 'L' or 'l'  then
  //           LDA  must be at least  max( 1, m ),  when  SIDE = 'R' or 'r'
  //           then LDA must be at least max( 1, n ).
  //           Unchanged on exit.
  
  //  B      - COMPLEX*16       array of DIMENSION ( LDB, n ).
  //           Before entry,  the leading  m by n part of the array  B must
  //           contain the matrix  B,  and  on exit  is overwritten  by the
  //           transformed matrix.
  
  //  LDB    - INTEGER.
  //           On entry, LDB specifies the first dimension of B as declared
  //           in  the  calling  (sub)  program.   LDB  must  be  at  least
  //           max( 1, m ).
  //           Unchanged on exit.
  
  
  //  Level 3 Blas routine.
  
  //  -- Written on 8-February-1989.
  //     Jack Dongarra, Argonne National Laboratory.
  //     Iain Duff, AERE Harwell.
  //     Jeremy Du Croz, Numerical Algorithms Group Ltd.
  //     Sven Hammarling, Numerical Algorithms Group Ltd.
  
  
  //     .. External Functions ..
  //     .. External Subroutines ..
  //     .. Intrinsic Functions ..
  //     .. Local Scalars ..
  //     .. Parameters ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  lside = lsame( side, 'L' );
  if( lside ) { 
    nrowa = m;
  }
  else { 
    nrowa = n;
  }
  noconj = lsame( transa, 'T' );
  nounit = lsame( diag, 'N' );
  upper = lsame( uplo, 'U' );
  
  info = 0;
  if( (!lside) && (!lsame( side, 'R' )) ) { 
    info = 1;
  }
  else if( (!upper) && (!lsame( uplo, 'L' )) ) { 
    info = 2;
  }
  else if( ((!lsame( transa, 'N' )) && (!lsame( transa, 'T' ))) && 
   (!lsame( transa, 'C' )) ) { 
    info = 3;
  }
  else if( (!lsame( diag, 'U' )) && (!lsame( diag, 'N' )) ) { 
    info = 4;
  }
  else if( m < 0 ) { 
    info = 5;
  }
  else if( n < 0 ) { 
    info = 6;
  }
  else if( lda < max( 1, nrowa ) ) { 
    info = 9;
  }
  else if( ldb < max( 1, m ) ) { 
    info = 11;
  }
  if( info != 0 ) { 
    xerbla( "ZTRMM ", info );
    return;
  }
  
  //     Quick return if possible.
  
  if( n == 0 ) 
    return;
  
  //     And when  alpha.eq.zero.
  
  if( ctocf(alpha) == ctocf(ZERO) ) { 
    for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
      for( i = 1, i_ = i - 1, _do1 = m; i <= _do1; i++, i_++ ) { 
        B(j_,i_) = ZERO;
      }
    }
    return;
  }
  
  //     Start the operations.
  
  if( lside ) { 
    if( lsame( transa, 'N' ) ) { 
      
      //           Form  B := alpha*A*B.
      
      if( upper ) { 
        for( j = 1, j_ = j - 1, _do2 = n; j <= _do2; j++, j_++ ) { 
          for( k = 1, k_ = k - 1, _do3 = m; k <= _do3; k++, k_++ ) { 
            if( ctocf(B(j_,k_)) != ctocf(ZERO) ) { 
              temp = alpha*B(j_,k_);
              for( i = 1, i_ = i - 1, _do4 = k - 1; i <= _do4; i++, i_++ ) { 
                B(j_,i_) = B(j_,i_) + temp*A(k_,i_);
              }
              if( nounit ) 
                temp = temp*A(k_,k_);
              B(j_,k_) = temp;
            }
          }
        }
      }
      else { 
        for( j = 1, j_ = j - 1, _do5 = n; j <= _do5; j++, j_++ ) { 
          for( k = m, k_ = k - 1; k >= 1; k--, k_-- ) { 
            if( ctocf(B(j_,k_)) != ctocf(ZERO) ) { 
              temp = alpha*B(j_,k_);
              B(j_,k_) = temp;
              if( nounit ) 
                B(j_,k_) = B(j_,k_)*A(k_,k_);
              for( i = k + 1, i_ = i - 1, _do6 = m; i <= _do6; i++, i_++ ) { 
                B(j_,i_) = B(j_,i_) + temp*A(k_,i_);
              }
            }
          }
        }
      }
    }
    else { 
      
      //           Form  B := alpha*B*A'   or   B := alpha*B*conjg( A' ).
      
      if( upper ) { 
        for( j = 1, j_ = j - 1, _do7 = n; j <= _do7; j++, j_++ ) { 
          for( i = m, i_ = i - 1; i >= 1; i--, i_-- ) { 
            temp = B(j_,i_);
            if( noconj ) { 
              if( nounit ) 
                temp = temp*A(i_,i_);
              for( k = 1, k_ = k - 1, _do8 = i - 1; k <= _do8; k++, k_++ ) { 
                temp = temp + A(i_,k_)*B(j_,k_);
              }
            }
            else { 
              if( nounit ) 
                temp = temp*conj( A(i_,i_) );
              for( k = 1, k_ = k - 1, _do9 = i - 1; k <= _do9; k++, k_++ ) { 
                temp = temp + conj( A(i_,k_) )*B(j_,k_);
              }
            }
            B(j_,i_) = alpha*temp;
          }
        }
      }
      else { 
        for( j = 1, j_ = j - 1, _do10 = n; j <= _do10; j++, j_++ ) { 
          for( i = 1, i_ = i - 1, _do11 = m; i <= _do11; i++, i_++ ) { 
            temp = B(j_,i_);
            if( noconj ) { 
              if( nounit ) 
                temp = temp*A(i_,i_);
              for( k = i + 1, k_ = k - 1, _do12 = m; k <= _do12; k++, k_++ ) { 
                temp = temp + A(i_,k_)*B(j_,k_);
              }
            }
            else { 
              if( nounit ) 
                temp = temp*conj( A(i_,i_) );
              for( k = i + 1, k_ = k - 1, _do13 = m; k <= _do13; k++, k_++ ) { 
                temp = temp + conj( A(i_,k_) )*B(j_,k_);
              }
            }
            B(j_,i_) = alpha*temp;
          }
        }
      }
    }
  }
  else { 
    if( lsame( transa, 'N' ) ) { 
      
      //           Form  B := alpha*B*A.
      
      if( upper ) { 
        for( j = n, j_ = j - 1; j >= 1; j--, j_-- ) { 
          temp = alpha;
          if( nounit ) 
            temp = temp*A(j_,j_);
          for( i = 1, i_ = i - 1, _do14 = m; i <= _do14; i++, i_++ ) { 
            B(j_,i_) = temp*B(j_,i_);
          }
          for( k = 1, k_ = k - 1, _do15 = j - 1; k <= _do15; k++, k_++ ) { 
            if( ctocf(A(j_,k_)) != ctocf(ZERO) ) { 
              temp = alpha*A(j_,k_);
              for( i = 1, i_ = i - 1, _do16 = m; i <= _do16; i++, i_++ ) { 
                B(j_,i_) = B(j_,i_) + temp*B(k_,i_);
              }
            }
          }
        }
      }
      else { 
        for( j = 1, j_ = j - 1, _do17 = n; j <= _do17; j++, j_++ ) { 
          temp = alpha;
          if( nounit ) 
            temp = temp*A(j_,j_);
          for( i = 1, i_ = i - 1, _do18 = m; i <= _do18; i++, i_++ ) { 
            B(j_,i_) = temp*B(j_,i_);
          }
          for( k = j + 1, k_ = k - 1, _do19 = n; k <= _do19; k++, k_++ ) { 
            if( ctocf(A(j_,k_)) != ctocf(ZERO) ) { 
              temp = alpha*A(j_,k_);
              for( i = 1, i_ = i - 1, _do20 = m; i <= _do20; i++, i_++ ) { 
                B(j_,i_) = B(j_,i_) + temp*B(k_,i_);
              }
            }
          }
        }
      }
    }
    else { 
      
      //           Form  B := alpha*B*A'   or   B := alpha*B*conjg( A' ).
      
      if( upper ) { 
        for( k = 1, k_ = k - 1, _do21 = n; k <= _do21; k++, k_++ ) { 
          for( j = 1, j_ = j - 1, _do22 = k - 1; j <= _do22; j++, j_++ ) { 
            if( ctocf(A(k_,j_)) != ctocf(ZERO) ) { 
              if( noconj ) { 
                temp = alpha*A(k_,j_);
              }
              else { 
                temp = alpha*conj( A(k_,j_) );
              }
              for( i = 1, i_ = i - 1, _do23 = m; i <= _do23; i++, i_++ ) { 
                B(j_,i_) = B(j_,i_) + temp*B(k_,i_);
              }
            }
          }
          temp = alpha;
          if( nounit ) { 
            if( noconj ) { 
              temp = temp*A(k_,k_);
            }
            else { 
              temp = temp*conj( A(k_,k_) );
            }
          }
          if( ctocf(temp) != ctocf(ONE) ) { 
            for( i = 1, i_ = i - 1, _do24 = m; i <= _do24; i++, i_++ ) { 
              B(k_,i_) = temp*B(k_,i_);
            }
          }
        }
      }
      else { 
        for( k = n, k_ = k - 1; k >= 1; k--, k_-- ) { 
          for( j = k + 1, j_ = j - 1, _do25 = n; j <= _do25; j++, j_++ ) { 
            if( ctocf(A(k_,j_)) != ctocf(ZERO) ) { 
              if( noconj ) { 
                temp = alpha*A(k_,j_);
              }
              else { 
                temp = alpha*conj( A(k_,j_) );
              }
              for( i = 1, i_ = i - 1, _do26 = m; i <= _do26; i++, i_++ ) { 
                B(j_,i_) = B(j_,i_) + temp*B(k_,i_);
              }
            }
          }
          temp = alpha;
          if( nounit ) { 
            if( noconj ) { 
              temp = temp*A(k_,k_);
            }
            else { 
              temp = temp*conj( A(k_,k_) );
            }
          }
          if( ctocf(temp) != ctocf(ONE) ) { 
            for( i = 1, i_ = i - 1, _do27 = m; i <= _do27; i++, i_++ ) { 
              B(k_,i_) = temp*B(k_,i_);
            }
          }
        }
      }
    }
  }
  
  return;
  
  //     End of ZTRMM .
  
#undef  B
#undef  A
} // end of function 


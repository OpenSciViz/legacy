/*
 * Default C++ implementation of cgemm
 * For optimum performance, use a machine specific bla library
 *
 * $Id: cgemm.cpp,v 1.3 1993/03/19 16:26:58 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 06:02:50
 * FOR_C++ Options SET: alloc do=rt no=p pf=cbla,sbla,xbla s=dv str=l - prototypes
 *
 * $Log: cgemm.cpp,v $
 * Revision 1.3  1993/03/19  16:26:58  alv
 * added RWBLADECL linkage specification
 *
 * Revision 1.2  1993/03/05  23:06:46  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:04:20  alv
 * Initial revision
 *
 */

#define RW_CPPBLAS 1
#include "rw/bla.h"
#include "rw/bla.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/fcomplex.h"

// PARAMETER translations
const FComplex ONE = FComplex(1.0e0,0.0e0);
const FComplex ZERO = FComplex(0.0e0,0.0e0);
// end of PARAMETER translations

RWBLADECL void /*FUNCTION*/ cgemm(const char &transa, const char &transb, const long &m, const long &n, 
 const long &k, const FComplex &alpha, FComplex *a, const long &lda, FComplex *b, 
 const long &ldb, const FComplex &beta, FComplex *c, const long &ldc)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
#define C(I_,J_)  (*(c+(I_)*(ldc)+(J_)))
  int conja, conjb, nota, notb;
  long _do0, _do1, _do10, _do11, _do12, _do13, _do14, _do15, 
   _do16, _do17, _do18, _do19, _do2, _do20, _do21, _do22, _do23, 
   _do24, _do25, _do26, _do27, _do28, _do29, _do3, _do30, _do31, 
   _do32, _do33, _do34, _do35, _do36, _do4, _do5, _do6, _do7, _do8, 
   _do9, i, i_, info, j, j_, l, l_, /*ncola,*/ nrowa, nrowb;
  FComplex temp;

  //     .. Scalar Arguments ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  CGEMM  performs one of the matrix-matrix operations
  
  //     C := alpha*op( A )*op( B ) + beta*C,
  
  //  where  op( X ) is one of
  
  //     op( X ) = X   or   op( X ) = X'   or   op( X ) = conjg( X' ),
  
  //  alpha and beta are scalars, and A, B and C are matrices, with op( A )
  //  an m by k matrix,  op( B )  a  k by n matrix and  C an m by n matrix.
  
  //  Parameters
  //  ==========
  
  //  TRANSA - CHARACTER*1.
  //           On entry, TRANSA specifies the form of op( A ) to be used in
  //           the matrix multiplication as follows:
  
  //              TRANSA = 'N' or 'n',  op( A ) = A.
  
  //              TRANSA = 'T' or 't',  op( A ) = A'.
  
  //              TRANSA = 'C' or 'c',  op( A ) = conjg( A' ).
  
  //           Unchanged on exit.
  
  //  TRANSB - CHARACTER*1.
  //           On entry, TRANSB specifies the form of op( B ) to be used in
  //           the matrix multiplication as follows:
  
  //              TRANSB = 'N' or 'n',  op( B ) = B.
  
  //              TRANSB = 'T' or 't',  op( B ) = B'.
  
  //              TRANSB = 'C' or 'c',  op( B ) = conjg( B' ).
  
  //           Unchanged on exit.
  
  //  M      - INTEGER.
  //           On entry,  M  specifies  the number  of rows  of the  matrix
  //           op( A )  and of the  matrix  C.  M  must  be at least  zero.
  //           Unchanged on exit.
  
  //  N      - INTEGER.
  //           On entry,  N  specifies the number  of columns of the matrix
  //           op( B ) and the number of columns of the matrix C. N must be
  //           at least zero.
  //           Unchanged on exit.
  
  //  K      - INTEGER.
  //           On entry,  K  specifies  the number of columns of the matrix
  //           op( A ) and the number of rows of the matrix op( B ). K must
  //           be at least  zero.
  //           Unchanged on exit.
  
  //  ALPHA  - COMPLEX         .
  //           On entry, ALPHA specifies the scalar alpha.
  //           Unchanged on exit.
  
  //  A      - COMPLEX          array of DIMENSION ( LDA, ka ), where ka is
  //           k  when  TRANSA = 'N' or 'n',  and is  m  otherwise.
  //           Before entry with  TRANSA = 'N' or 'n',  the leading  m by k
  //           part of the array  A  must contain the matrix  A,  otherwise
  //           the leading  k by m  part of the array  A  must contain  the
  //           matrix A.
  //           Unchanged on exit.
  
  //  LDA    - INTEGER.
  //           On entry, LDA specifies the first dimension of A as declared
  //           in the calling (sub) program. When  TRANSA = 'N' or 'n' then
  //           LDA must be at least  max( 1, m ), otherwise  LDA must be at
  //           least  max( 1, k ).
  //           Unchanged on exit.
  
  //  B      - COMPLEX          array of DIMENSION ( LDB, kb ), where kb is
  //           n  when  TRANSB = 'N' or 'n',  and is  k  otherwise.
  //           Before entry with  TRANSB = 'N' or 'n',  the leading  k by n
  //           part of the array  B  must contain the matrix  B,  otherwise
  //           the leading  n by k  part of the array  B  must contain  the
  //           matrix B.
  //           Unchanged on exit.
  
  //  LDB    - INTEGER.
  //           On entry, LDB specifies the first dimension of B as declared
  //           in the calling (sub) program. When  TRANSB = 'N' or 'n' then
  //           LDB must be at least  max( 1, k ), otherwise  LDB must be at
  //           least  max( 1, n ).
  //           Unchanged on exit.
  
  //  BETA   - COMPLEX         .
  //           On entry,  BETA  specifies the scalar  beta.  When  BETA  is
  //           supplied as zero then C need not be set on input.
  //           Unchanged on exit.
  
  //  C      - COMPLEX          array of DIMENSION ( LDC, n ).
  //           Before entry, the leading  m by n  part of the array  C must
  //           contain the matrix  C,  except when  beta  is zero, in which
  //           case C need not be set on entry.
  //           On exit, the array  C  is overwritten by the  m by n  matrix
  //           ( alpha*op( A )*op( B ) + beta*C ).
  
  //  LDC    - INTEGER.
  //           On entry, LDC specifies the first dimension of C as declared
  //           in  the  calling  (sub)  program.   LDC  must  be  at  least
  //           max( 1, m ).
  //           Unchanged on exit.
  
  
  //  Level 3 Blas routine.
  
  //  -- Written on 8-February-1989.
  //     Jack Dongarra, Argonne National Laboratory.
  //     Iain Duff, AERE Harwell.
  //     Jeremy Du Croz, Numerical Algorithms Group Ltd.
  //     Sven Hammarling, Numerical Algorithms Group Ltd.
  
  
  //     .. External Functions ..
  //     .. External Subroutines ..
  //     .. Intrinsic Functions ..
  //     .. Local Scalars ..
  //     .. Parameters ..
  //     ..
  //     .. Executable Statements ..
  
  //     Set  NOTA  and  NOTB  as  true if  A  and  B  respectively are not
  //     conjugated or transposed, set  CONJA and CONJB  as true if  A  and
  //     B  respectively are to be  transposed but  not conjugated  and set
  //     NROWA, NCOLA and  NROWB  as the number of rows and  columns  of  A
  //     and the number of rows of  B  respectively.
  
  nota = lsame( transa, 'N' );
  notb = lsame( transb, 'N' );
  conja = lsame( transa, 'C' );
  conjb = lsame( transb, 'C' );
  if( nota ) { 
    nrowa = m;
    //ncola = k;
  }
  else { 
    nrowa = k;
    //ncola = m;
  }
  if( notb ) { 
    nrowb = k;
  }
  else { 
    nrowb = n;
  }
  
  //     Test the input parameters.
  
  info = 0;
  if( ((!nota) && (!conja)) && (!lsame( transa, 'T' )) ) { 
    info = 1;
  }
  else if( ((!notb) && (!conjb)) && (!lsame( transb, 'T' )) ) { 
    info = 2;
  }
  else if( m < 0 ) { 
    info = 3;
  }
  else if( n < 0 ) { 
    info = 4;
  }
  else if( k < 0 ) { 
    info = 5;
  }
  else if( lda < max( 1, nrowa ) ) { 
    info = 8;
  }
  else if( ldb < max( 1, nrowb ) ) { 
    info = 10;
  }
  else if( ldc < max( 1, m ) ) { 
    info = 13;
  }
  if( info != 0 ) { 
    xerbla( "CGEMM ", info );
    return;
  }
  
  //     Quick return if possible.
  
  if( ((m == 0) || (n == 0)) || (((alpha == ZERO) || (k == 0)) && 
   (beta == ONE)) ) 
    return;
  
  //     And when  alpha.eq.zero.
  
  if( alpha == ZERO ) { 
    if( beta == ZERO ) { 
      for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
        for( i = 1, i_ = i - 1, _do1 = m; i <= _do1; i++, i_++ ) { 
          C(j_,i_) = ZERO;
        }
      }
    }
    else { 
      for( j = 1, j_ = j - 1, _do2 = n; j <= _do2; j++, j_++ ) { 
        for( i = 1, i_ = i - 1, _do3 = m; i <= _do3; i++, i_++ ) { 
          C(j_,i_) = beta*C(j_,i_);
        }
      }
    }
    return;
  }
  
  //     Start the operations.
  
  if( notb ) { 
    if( nota ) { 
      
      //           Form  C := alpha*A*B + beta*C.
      
      for( j = 1, j_ = j - 1, _do4 = n; j <= _do4; j++, j_++ ) { 
        if( beta == ZERO ) { 
          for( i = 1, i_ = i - 1, _do5 = m; i <= _do5; i++, i_++ ) { 
            C(j_,i_) = ZERO;
          }
        }
        else if( beta != ONE ) { 
          for( i = 1, i_ = i - 1, _do6 = m; i <= _do6; i++, i_++ ) { 
            C(j_,i_) = beta*C(j_,i_);
          }
        }
        for( l = 1, l_ = l - 1, _do7 = k; l <= _do7; l++, l_++ ) { 
          if( B(j_,l_) != ZERO ) { 
            temp = alpha*B(j_,l_);
            for( i = 1, i_ = i - 1, _do8 = m; i <= _do8; i++, i_++ ) { 
              C(j_,i_) = C(j_,i_) + temp*A(l_,i_);
            }
          }
        }
      }
    }
    else if( conja ) { 
      
      //           Form  C := alpha*conjg( A' )*B + beta*C.
      
      for( j = 1, j_ = j - 1, _do9 = n; j <= _do9; j++, j_++ ) { 
        for( i = 1, i_ = i - 1, _do10 = m; i <= _do10; i++, i_++ ) { 
          temp = ZERO;
          for( l = 1, l_ = l - 1, _do11 = k; l <= _do11; l++, l_++ ) { 
            temp = temp + conj( A(i_,l_) )*B(j_,l_);
          }
          if( beta == ZERO ) { 
            C(j_,i_) = alpha*temp;
          }
          else { 
            C(j_,i_) = alpha*temp + beta*C(j_,i_);
          }
        }
      }
    }
    else { 
      
      //           Form  C := alpha*A'*B + beta*C
      
      for( j = 1, j_ = j - 1, _do12 = n; j <= _do12; j++, j_++ ) { 
        for( i = 1, i_ = i - 1, _do13 = m; i <= _do13; i++, i_++ ) { 
          temp = ZERO;
          for( l = 1, l_ = l - 1, _do14 = k; l <= _do14; l++, l_++ ) { 
            temp = temp + A(i_,l_)*B(j_,l_);
          }
          if( beta == ZERO ) { 
            C(j_,i_) = alpha*temp;
          }
          else { 
            C(j_,i_) = alpha*temp + beta*C(j_,i_);
          }
        }
      }
    }
  }
  else if( nota ) { 
    if( conjb ) { 
      
      //           Form  C := alpha*A*conjg( B' ) + beta*C.
      
      for( j = 1, j_ = j - 1, _do15 = n; j <= _do15; j++, j_++ ) { 
        if( beta == ZERO ) { 
          for( i = 1, i_ = i - 1, _do16 = m; i <= _do16; i++, i_++ ) { 
            C(j_,i_) = ZERO;
          }
        }
        else if( beta != ONE ) { 
          for( i = 1, i_ = i - 1, _do17 = m; i <= _do17; i++, i_++ ) { 
            C(j_,i_) = beta*C(j_,i_);
          }
        }
        for( l = 1, l_ = l - 1, _do18 = k; l <= _do18; l++, l_++ ) { 
          if( B(l_,j_) != ZERO ) { 
            temp = alpha*conj( B(l_,j_) );
            for( i = 1, i_ = i - 1, _do19 = m; i <= _do19; i++, i_++ ) { 
              C(j_,i_) = C(j_,i_) + temp*A(l_,i_);
            }
          }
        }
      }
    }
    else { 
      
      //           Form  C := alpha*A*B'          + beta*C
      
      for( j = 1, j_ = j - 1, _do20 = n; j <= _do20; j++, j_++ ) { 
        if( beta == ZERO ) { 
          for( i = 1, i_ = i - 1, _do21 = m; i <= _do21; i++, i_++ ) { 
            C(j_,i_) = ZERO;
          }
        }
        else if( beta != ONE ) { 
          for( i = 1, i_ = i - 1, _do22 = m; i <= _do22; i++, i_++ ) { 
            C(j_,i_) = beta*C(j_,i_);
          }
        }
        for( l = 1, l_ = l - 1, _do23 = k; l <= _do23; l++, l_++ ) { 
          if( B(l_,j_) != ZERO ) { 
            temp = alpha*B(l_,j_);
            for( i = 1, i_ = i - 1, _do24 = m; i <= _do24; i++, i_++ ) { 
              C(j_,i_) = C(j_,i_) + temp*A(l_,i_);
            }
          }
        }
      }
    }
  }
  else if( conja ) { 
    if( conjb ) { 
      
      //           Form  C := alpha*conjg( A' )*conjg( B' ) + beta*C.
      
      for( j = 1, j_ = j - 1, _do25 = n; j <= _do25; j++, j_++ ) { 
        for( i = 1, i_ = i - 1, _do26 = m; i <= _do26; i++, i_++ ) { 
          temp = ZERO;
          for( l = 1, l_ = l - 1, _do27 = k; l <= _do27; l++, l_++ ) { 
            temp = temp + conj( A(i_,l_) )*conj( B(l_,j_) );
          }
          if( beta == ZERO ) { 
            C(j_,i_) = alpha*temp;
          }
          else { 
            C(j_,i_) = alpha*temp + beta*C(j_,i_);
          }
        }
      }
    }
    else { 
      
      //           Form  C := alpha*conjg( A' )*B' + beta*C
      
      for( j = 1, j_ = j - 1, _do28 = n; j <= _do28; j++, j_++ ) { 
        for( i = 1, i_ = i - 1, _do29 = m; i <= _do29; i++, i_++ ) { 
          temp = ZERO;
          for( l = 1, l_ = l - 1, _do30 = k; l <= _do30; l++, l_++ ) { 
            temp = temp + conj( A(i_,l_) )*B(l_,j_);
          }
          if( beta == ZERO ) { 
            C(j_,i_) = alpha*temp;
          }
          else { 
            C(j_,i_) = alpha*temp + beta*C(j_,i_);
          }
        }
      }
    }
  }
  else { 
    if( conjb ) { 
      
      //           Form  C := alpha*A'*conjg( B' ) + beta*C
      
      for( j = 1, j_ = j - 1, _do31 = n; j <= _do31; j++, j_++ ) { 
        for( i = 1, i_ = i - 1, _do32 = m; i <= _do32; i++, i_++ ) { 
          temp = ZERO;
          for( l = 1, l_ = l - 1, _do33 = k; l <= _do33; l++, l_++ ) { 
            temp = temp + A(i_,l_)*conj( B(l_,j_) );
          }
          if( beta == ZERO ) { 
            C(j_,i_) = alpha*temp;
          }
          else { 
            C(j_,i_) = alpha*temp + beta*C(j_,i_);
          }
        }
      }
    }
    else { 
      
      //           Form  C := alpha*A'*B' + beta*C
      
      for( j = 1, j_ = j - 1, _do34 = n; j <= _do34; j++, j_++ ) { 
        for( i = 1, i_ = i - 1, _do35 = m; i <= _do35; i++, i_++ ) { 
          temp = ZERO;
          for( l = 1, l_ = l - 1, _do36 = k; l <= _do36; l++, l_++ ) { 
            temp = temp + A(i_,l_)*B(l_,j_);
          }
          if( beta == ZERO ) { 
            C(j_,i_) = alpha*temp;
          }
          else { 
            C(j_,i_) = alpha*temp + beta*C(j_,i_);
          }
        }
      }
    }
  }
  
  return;
  
  //     End of CGEMM .
  
#undef  C
#undef  B
#undef  A
} // end of function 


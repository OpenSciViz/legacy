/*
 * Default C++ implementation of csscal
 * For optimum performance, use a machine specific bla library
 *
 * $Id: csscal.cpp,v 1.3 1993/03/19 16:26:58 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 06:02:50
 * FOR_C++ Options SET: alloc do=rt no=p pf=cbla,sbla,xbla s=dv str=l - prototypes
 *
 * $Log: csscal.cpp,v $
 * Revision 1.3  1993/03/19  16:26:58  alv
 * added RWBLADECL linkage specification
 *
 * Revision 1.2  1993/03/05  23:06:59  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:04:29  alv
 * Initial revision
 *
 */

#define RW_CPPBLAS 1
#include "rw/bla.h"
#include "rw/bla.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/fcomplex.h"

RWBLADECL void /*FUNCTION*/ csscal(const long &n, const float &sa, FComplex cx[], const long &incx)
{
  long _do0, _do1, i, i_, ix;

  
  //     scales a DComplex vector by a real constant.
  //     jack dongarra, linpack, 3/11/78.
  //     modified to correct problem with negative increment, 8/21/90.
  
  
  if( n <= 0 ) 
    return;
  if( incx == 1 ) 
    goto L_20;
  
  //        code for increment not equal to 1
  
  ix = 1;
  if( incx < 0 ) 
    ix = (-n + 1)*incx + 1;
  for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
    cx[ix - 1] = FComplex( sa*real( cx[ix - 1] ), sa*imag( cx[ix - 1] ) );
    ix = ix + incx;
  }
  return;
  
  //        code for increment equal to 1
  
L_20:
  for( i = 1, i_ = i - 1, _do1 = n; i <= _do1; i++, i_++ ) { 
    cx[i_] = FComplex( sa*real( cx[i_] ), sa*imag( cx[i_] ) );
  }
  return;
} // end of function 


/*
 * Default C++ implementation of chemv
 * For optimum performance, use a machine specific bla library
 *
 * $Id: chemv.cpp,v 1.3 1993/03/19 16:26:58 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 06:02:50
 * FOR_C++ Options SET: alloc do=rt no=p pf=cbla,sbla,xbla s=dv str=l - prototypes
 *
 * $Log: chemv.cpp,v $
 * Revision 1.3  1993/03/19  16:26:58  alv
 * added RWBLADECL linkage specification
 *
 * Revision 1.2  1993/03/05  23:06:51  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:04:22  alv
 * Initial revision
 *
 */

#define RW_CPPBLAS 1
#include "rw/bla.h"
#include "rw/bla.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/fcomplex.h"

// PARAMETER translations
const FComplex ONE = FComplex(1.0e0,0.0e0);
const FComplex ZERO = FComplex(0.0e0,0.0e0);
// end of PARAMETER translations

RWBLADECL void /*FUNCTION*/ chemv(const char &uplo, const long &n, const FComplex &alpha, 
 FComplex *a, const long &lda, FComplex x[], const long &incx, const FComplex &beta, 
 FComplex y[], const long &incy)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  long _do0, _do1, _do10, _do11, _do2, _do3, _do4, _do5, _do6, 
   _do7, _do8, _do9, i, i_, info, ix, iy, j, j_, jx, jy, kx, ky;
  FComplex temp1, temp2;

  //     .. Scalar Arguments ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  CHEMV  performs the matrix-vector  operation
  
  //     y := alpha*A*x + beta*y,
  
  //  where alpha and beta are scalars, x and y are n element vectors and
  //  A is an n by n hermitian matrix.
  
  //  Parameters
  //  ==========
  
  //  UPLO   - CHARACTER*1.
  //           On entry, UPLO specifies whether the upper or lower
  //           triangular part of the array A is to be referenced as
  //           follows:
  
  //              UPLO = 'U' or 'u'   Only the upper triangular part of A
  //                                  is to be referenced.
  
  //              UPLO = 'L' or 'l'   Only the lower triangular part of A
  //                                  is to be referenced.
  
  //           Unchanged on exit.
  
  //  N      - INTEGER.
  //           On entry, N specifies the order of the matrix A.
  //           N must be at least zero.
  //           Unchanged on exit.
  
  //  ALPHA  - COMPLEX         .
  //           On entry, ALPHA specifies the scalar alpha.
  //           Unchanged on exit.
  
  //  A      - COMPLEX          array of DIMENSION ( LDA, n ).
  //           Before entry with  UPLO = 'U' or 'u', the leading n by n
  //           upper triangular part of the array A must contain the upper
  //           triangular part of the hermitian matrix and the strictly
  //           lower triangular part of A is not referenced.
  //           Before entry with UPLO = 'L' or 'l', the leading n by n
  //           lower triangular part of the array A must contain the lower
  //           triangular part of the hermitian matrix and the strictly
  //           upper triangular part of A is not referenced.
  //           Note that the imaginary parts of the diagonal elements need
  //           not be set and are assumed to be zero.
  //           Unchanged on exit.
  
  //  LDA    - INTEGER.
  //           On entry, LDA specifies the first dimension of A as declared
  //           in the calling (sub) program. LDA must be at least
  //           max( 1, n ).
  //           Unchanged on exit.
  
  //  X      - COMPLEX          array of dimension at least
  //           ( 1 + ( n - 1 )*abs( INCX ) ).
  //           Before entry, the incremented array X must contain the n
  //           element vector x.
  //           Unchanged on exit.
  
  //  INCX   - INTEGER.
  //           On entry, INCX specifies the increment for the elements of
  //           X. INCX must not be zero.
  //           Unchanged on exit.
  
  //  BETA   - COMPLEX         .
  //           On entry, BETA specifies the scalar beta. When BETA is
  //           supplied as zero then Y need not be set on input.
  //           Unchanged on exit.
  
  //  Y      - COMPLEX          array of dimension at least
  //           ( 1 + ( n - 1 )*abs( INCY ) ).
  //           Before entry, the incremented array Y must contain the n
  //           element vector y. On exit, Y is overwritten by the updated
  //           vector y.
  
  //  INCY   - INTEGER.
  //           On entry, INCY specifies the increment for the elements of
  //           Y. INCY must not be zero.
  //           Unchanged on exit.
  
  
  //  Level 2 Blas routine.
  
  //  -- Written on 22-October-1986.
  //     Jack Dongarra, Argonne National Lab.
  //     Jeremy Du Croz, Nag Central Office.
  //     Sven Hammarling, Nag Central Office.
  //     Richard Hanson, Sandia National Labs.
  
  
  //     .. Parameters ..
  //     .. Local Scalars ..
  //     .. External Functions ..
  //     .. External Subroutines ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  if( !lsame( uplo, 'U' ) && !lsame( uplo, 'L' ) ) { 
    info = 1;
  }
  else if( n < 0 ) { 
    info = 2;
  }
  else if( lda < max( 1, n ) ) { 
    info = 5;
  }
  else if( incx == 0 ) { 
    info = 7;
  }
  else if( incy == 0 ) { 
    info = 10;
  }
  if( info != 0 ) { 
    xerbla( "CHEMV ", info );
    return;
  }
  
  //     Quick return if possible.
  
  if( (n == 0) || ((alpha == ZERO) && (beta == ONE)) ) 
    return;
  
  //     Set up the start points in  X  and  Y.
  
  if( incx > 0 ) { 
    kx = 1;
  }
  else { 
    kx = 1 - (n - 1)*incx;
  }
  if( incy > 0 ) { 
    ky = 1;
  }
  else { 
    ky = 1 - (n - 1)*incy;
  }
  
  //     Start the operations. In this version the elements of A are
  //     accessed sequentially with one pass through the triangular part
  //     of A.
  
  //     First form  y := beta*y.
  
  if( beta != ONE ) { 
    if( incy == 1 ) { 
      if( beta == ZERO ) { 
        for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
          y[i_] = ZERO;
        }
      }
      else { 
        for( i = 1, i_ = i - 1, _do1 = n; i <= _do1; i++, i_++ ) { 
          y[i_] = beta*y[i_];
        }
      }
    }
    else { 
      iy = ky;
      if( beta == ZERO ) { 
        for( i = 1, i_ = i - 1, _do2 = n; i <= _do2; i++, i_++ ) { 
          y[iy - 1] = ZERO;
          iy = iy + incy;
        }
      }
      else { 
        for( i = 1, i_ = i - 1, _do3 = n; i <= _do3; i++, i_++ ) { 
          y[iy - 1] = beta*y[iy - 1];
          iy = iy + incy;
        }
      }
    }
  }
  if( alpha == ZERO ) 
    return;
  if( lsame( uplo, 'U' ) ) { 
    
    //        Form  y  when A is stored in upper triangle.
    
    if( (incx == 1) && (incy == 1) ) { 
      for( j = 1, j_ = j - 1, _do4 = n; j <= _do4; j++, j_++ ) { 
        temp1 = alpha*x[j_];
        temp2 = ZERO;
        for( i = 1, i_ = i - 1, _do5 = j - 1; i <= _do5; i++, i_++ ) { 
          y[i_] = y[i_] + temp1*A(j_,i_);
          temp2 = temp2 + conj( A(j_,i_) )*x[i_];
        }
        y[j_] = y[j_] + temp1*real( A(j_,j_) ) + alpha*temp2;
      }
    }
    else { 
      jx = kx;
      jy = ky;
      for( j = 1, j_ = j - 1, _do6 = n; j <= _do6; j++, j_++ ) { 
        temp1 = alpha*x[jx - 1];
        temp2 = ZERO;
        ix = kx;
        iy = ky;
        for( i = 1, i_ = i - 1, _do7 = j - 1; i <= _do7; i++, i_++ ) { 
          y[iy - 1] = y[iy - 1] + temp1*A(j_,i_);
          temp2 = temp2 + conj( A(j_,i_) )*x[ix - 1];
          ix = ix + incx;
          iy = iy + incy;
        }
        y[jy - 1] = y[jy - 1] + temp1*real( A(j_,j_) ) + alpha*
         temp2;
        jx = jx + incx;
        jy = jy + incy;
      }
    }
  }
  else { 
    
    //        Form  y  when A is stored in lower triangle.
    
    if( (incx == 1) && (incy == 1) ) { 
      for( j = 1, j_ = j - 1, _do8 = n; j <= _do8; j++, j_++ ) { 
        temp1 = alpha*x[j_];
        temp2 = ZERO;
        y[j_] = y[j_] + temp1*real( A(j_,j_) );
        for( i = j + 1, i_ = i - 1, _do9 = n; i <= _do9; i++, i_++ ) { 
          y[i_] = y[i_] + temp1*A(j_,i_);
          temp2 = temp2 + conj( A(j_,i_) )*x[i_];
        }
        y[j_] = y[j_] + alpha*temp2;
      }
    }
    else { 
      jx = kx;
      jy = ky;
      for( j = 1, j_ = j - 1, _do10 = n; j <= _do10; j++, j_++ ) { 
        temp1 = alpha*x[jx - 1];
        temp2 = ZERO;
        y[jy - 1] = y[jy - 1] + temp1*real( A(j_,j_) );
        ix = jx;
        iy = jy;
        for( i = j + 1, i_ = i - 1, _do11 = n; i <= _do11; i++, i_++ ) { 
          ix = ix + incx;
          iy = iy + incy;
          y[iy - 1] = y[iy - 1] + temp1*A(j_,i_);
          temp2 = temp2 + conj( A(j_,i_) )*x[ix - 1];
        }
        y[jy - 1] = y[jy - 1] + alpha*temp2;
        jx = jx + incx;
        jy = jy + incy;
      }
    }
  }
  
  return;
  
  //     End of CHEMV .
  
#undef  A
} // end of function 


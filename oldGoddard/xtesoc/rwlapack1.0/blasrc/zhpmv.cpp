/*
 * Default C++ implementation of zhpmv
 * For optimum performance, use a machine specific bla library
 *
 * $Id: zhpmv.cpp,v 1.3 1993/03/19 16:26:58 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 06:03:43
 * FOR_C++ Options SET: alloc do=rt no=p pf=zbla,dbla,xbla s=dv str=l - prototypes
 *
 * $Log: zhpmv.cpp,v $
 * Revision 1.3  1993/03/19  16:26:58  alv
 * added RWBLADECL linkage specification
 *
 * Revision 1.2  1993/03/05  23:08:32  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:05:18  alv
 * Initial revision
 *
 */

#define RW_CPPBLAS 1
#include "rw/bla.h"
#include "rw/bla.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const DComplex ONE = DComplex(1.0e0,0.0e0);
const DComplex ZERO = DComplex(0.0e0,0.0e0);
// end of PARAMETER translations

RWBLADECL void /*FUNCTION*/ zhpmv(const char &uplo, const long &n, const DComplex &alpha, DComplex ap[], 
 DComplex x[], const long &incx, const DComplex &beta, DComplex y[], const long &incy)
{
  long _do0, _do1, _do10, _do11, _do2, _do3, _do4, _do5, _do6, 
   _do7, _do8, _do9, i, i_, info, ix, iy, j, j_, jx, jy, k, k_, 
   kk, kx, ky;
  DComplex temp1, temp2;

  //     .. Scalar Arguments ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZHPMV  performs the matrix-vector operation
  
  //     y := alpha*A*x + beta*y,
  
  //  where alpha and beta are scalars, x and y are n element vectors and
  //  A is an n by n hermitian matrix, supplied in packed form.
  
  //  Parameters
  //  ==========
  
  //  UPLO   - CHARACTER*1.
  //           On entry, UPLO specifies whether the upper or lower
  //           triangular part of the matrix A is supplied in the packed
  //           array AP as follows:
  
  //              UPLO = 'U' or 'u'   The upper triangular part of A is
  //                                  supplied in AP.
  
  //              UPLO = 'L' or 'l'   The lower triangular part of A is
  //                                  supplied in AP.
  
  //           Unchanged on exit.
  
  //  N      - INTEGER.
  //           On entry, N specifies the order of the matrix A.
  //           N must be at least zero.
  //           Unchanged on exit.
  
  //  ALPHA  - COMPLEX*16      .
  //           On entry, ALPHA specifies the scalar alpha.
  //           Unchanged on exit.
  
  //  AP     - COMPLEX*16       array of DIMENSION at least
  //           ( ( n*( n + 1 ) )/2 ).
  //           Before entry with UPLO = 'U' or 'u', the array AP must
  //           contain the upper triangular part of the hermitian matrix
  //           packed sequentially, column by column, so that AP( 1 )
  //           contains a( 1, 1 ), AP( 2 ) and AP( 3 ) contain a( 1, 2 )
  //           and a( 2, 2 ) respectively, and so on.
  //           Before entry with UPLO = 'L' or 'l', the array AP must
  //           contain the lower triangular part of the hermitian matrix
  //           packed sequentially, column by column, so that AP( 1 )
  //           contains a( 1, 1 ), AP( 2 ) and AP( 3 ) contain a( 2, 1 )
  //           and a( 3, 1 ) respectively, and so on.
  //           Note that the imaginary parts of the diagonal elements need
  //           not be set and are assumed to be zero.
  //           Unchanged on exit.
  
  //  X      - COMPLEX*16       array of dimension at least
  //           ( 1 + ( n - 1 )*abs( INCX ) ).
  //           Before entry, the incremented array X must contain the n
  //           element vector x.
  //           Unchanged on exit.
  
  //  INCX   - INTEGER.
  //           On entry, INCX specifies the increment for the elements of
  //           X. INCX must not be zero.
  //           Unchanged on exit.
  
  //  BETA   - COMPLEX*16      .
  //           On entry, BETA specifies the scalar beta. When BETA is
  //           supplied as zero then Y need not be set on input.
  //           Unchanged on exit.
  
  //  Y      - COMPLEX*16       array of dimension at least
  //           ( 1 + ( n - 1 )*abs( INCY ) ).
  //           Before entry, the incremented array Y must contain the n
  //           element vector y. On exit, Y is overwritten by the updated
  //           vector y.
  
  //  INCY   - INTEGER.
  //           On entry, INCY specifies the increment for the elements of
  //           Y. INCY must not be zero.
  //           Unchanged on exit.
  
  
  //  Level 2 Blas routine.
  
  //  -- Written on 22-October-1986.
  //     Jack Dongarra, Argonne National Lab.
  //     Jeremy Du Croz, Nag Central Office.
  //     Sven Hammarling, Nag Central Office.
  //     Richard Hanson, Sandia National Labs.
  
  
  //     .. Parameters ..
  //     .. Local Scalars ..
  //     .. External Functions ..
  //     .. External Subroutines ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  if( !lsame( uplo, 'U' ) && !lsame( uplo, 'L' ) ) { 
    info = 1;
  }
  else if( n < 0 ) { 
    info = 2;
  }
  else if( incx == 0 ) { 
    info = 6;
  }
  else if( incy == 0 ) { 
    info = 9;
  }
  if( info != 0 ) { 
    xerbla( "ZHPMV ", info );
    return;
  }
  
  //     Quick return if possible.
  
  if( (n == 0) || ((ctocf(alpha) == ctocf(ZERO)) && (ctocf(beta) == 
   ctocf(ONE))) ) 
    return;
  
  //     Set up the start points in  X  and  Y.
  
  if( incx > 0 ) { 
    kx = 1;
  }
  else { 
    kx = 1 - (n - 1)*incx;
  }
  if( incy > 0 ) { 
    ky = 1;
  }
  else { 
    ky = 1 - (n - 1)*incy;
  }
  
  //     Start the operations. In this version the elements of the array AP
  //     are accessed sequentially with one pass through AP.
  
  //     First form  y := beta*y.
  
  if( ctocf(beta) != ctocf(ONE) ) { 
    if( incy == 1 ) { 
      if( ctocf(beta) == ctocf(ZERO) ) { 
        for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
          y[i_] = ZERO;
        }
      }
      else { 
        for( i = 1, i_ = i - 1, _do1 = n; i <= _do1; i++, i_++ ) { 
          y[i_] = beta*y[i_];
        }
      }
    }
    else { 
      iy = ky;
      if( ctocf(beta) == ctocf(ZERO) ) { 
        for( i = 1, i_ = i - 1, _do2 = n; i <= _do2; i++, i_++ ) { 
          y[iy - 1] = ZERO;
          iy = iy + incy;
        }
      }
      else { 
        for( i = 1, i_ = i - 1, _do3 = n; i <= _do3; i++, i_++ ) { 
          y[iy - 1] = beta*y[iy - 1];
          iy = iy + incy;
        }
      }
    }
  }
  if( ctocf(alpha) == ctocf(ZERO) ) 
    return;
  kk = 1;
  if( lsame( uplo, 'U' ) ) { 
    
    //        Form  y  when AP contains the upper triangle.
    
    if( (incx == 1) && (incy == 1) ) { 
      for( j = 1, j_ = j - 1, _do4 = n; j <= _do4; j++, j_++ ) { 
        temp1 = alpha*x[j_];
        temp2 = ZERO;
        k = kk;
        for( i = 1, i_ = i - 1, _do5 = j - 1; i <= _do5; i++, i_++ ) { 
          y[i_] = y[i_] + temp1*ap[k - 1];
          temp2 = temp2 + conj( ap[k - 1] )*x[i_];
          k = k + 1;
        }
        y[j_] = y[j_] + temp1*real( ap[kk + j_ - 1] ) + alpha*
         temp2;
        kk = kk + j;
      }
    }
    else { 
      jx = kx;
      jy = ky;
      for( j = 1, j_ = j - 1, _do6 = n; j <= _do6; j++, j_++ ) { 
        temp1 = alpha*x[jx - 1];
        temp2 = ZERO;
        ix = kx;
        iy = ky;
        for( k = kk, k_ = k - 1, _do7 = kk + j - 2; k <= _do7; k++, k_++ ) { 
          y[iy - 1] = y[iy - 1] + temp1*ap[k_];
          temp2 = temp2 + conj( ap[k_] )*x[ix - 1];
          ix = ix + incx;
          iy = iy + incy;
        }
        y[jy - 1] = y[jy - 1] + temp1*real( ap[kk + j_ - 1] ) + 
         alpha*temp2;
        jx = jx + incx;
        jy = jy + incy;
        kk = kk + j;
      }
    }
  }
  else { 
    
    //        Form  y  when AP contains the lower triangle.
    
    if( (incx == 1) && (incy == 1) ) { 
      for( j = 1, j_ = j - 1, _do8 = n; j <= _do8; j++, j_++ ) { 
        temp1 = alpha*x[j_];
        temp2 = ZERO;
        y[j_] = y[j_] + temp1*real( ap[kk - 1] );
        k = kk + 1;
        for( i = j + 1, i_ = i - 1, _do9 = n; i <= _do9; i++, i_++ ) { 
          y[i_] = y[i_] + temp1*ap[k - 1];
          temp2 = temp2 + conj( ap[k - 1] )*x[i_];
          k = k + 1;
        }
        y[j_] = y[j_] + alpha*temp2;
        kk = kk + (n - j + 1);
      }
    }
    else { 
      jx = kx;
      jy = ky;
      for( j = 1, j_ = j - 1, _do10 = n; j <= _do10; j++, j_++ ) { 
        temp1 = alpha*x[jx - 1];
        temp2 = ZERO;
        y[jy - 1] = y[jy - 1] + temp1*real( ap[kk - 1] );
        ix = jx;
        iy = jy;
        for( k = kk + 1, k_ = k - 1, _do11 = kk + n - j; k <= _do11; k++, k_++ ) { 
          ix = ix + incx;
          iy = iy + incy;
          y[iy - 1] = y[iy - 1] + temp1*ap[k_];
          temp2 = temp2 + conj( ap[k_] )*x[ix - 1];
        }
        y[jy - 1] = y[jy - 1] + alpha*temp2;
        jx = jx + incx;
        jy = jy + incy;
        kk = kk + (n - j + 1);
      }
    }
  }
  
  return;
  
  //     End of ZHPMV .
  
} // end of function 


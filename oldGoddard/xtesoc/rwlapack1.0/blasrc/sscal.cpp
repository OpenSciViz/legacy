/*
 * Default C++ implementation of sscal
 * For optimum performance, use a machine specific bla library
 *
 * $Id: sscal.cpp,v 1.3 1993/03/19 16:26:58 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/17/93 at 14:39:10
 * FOR_C++ Options SET: alloc do=rt no=p pf=sbla,xbla s=dv str=l - prototypes
 *
 * $Log: sscal.cpp,v $
 * Revision 1.3  1993/03/19  16:26:58  alv
 * added RWBLADECL linkage specification
 *
 * Revision 1.2  1993/03/05  23:07:53  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:04:57  alv
 * Initial revision
 *
 */

#define RW_CPPBLAS 1
#include "rw/bla.h"
#include "rw/bla.h"
#include "rw/fortran.h" /* Fortran run time library */

RWBLADECL void /*FUNCTION*/ sscal(const long &n, const float &sa, float sx[], const long &incx)
{
  long _do0, _do1, _do2, i, i_, ix, m, mp1;

  
  //     scales a vector by a constant.
  //     uses unrolled loops for increment equal to 1.
  //     jack dongarra, linpack, 3/11/78.
  //     modified to correct problem with negative increment, 8/21/90.
  
  
  if( n <= 0 ) 
    return;
  if( incx == 1 ) 
    goto L_20;
  
  //        code for increment not equal to 1
  
  ix = 1;
  if( incx < 0 ) 
    ix = (-n + 1)*incx + 1;
  for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
    sx[ix - 1] = sa*sx[ix - 1];
    ix = ix + incx;
  }
  return;
  
  //        code for increment equal to 1
  
  
  //        clean-up loop
  
L_20:
  // mod() function massaged out of next line
  m = n % 5; //m = mod( n, 5 );
  if( m == 0 ) 
    goto L_40;
  for( i = 1, i_ = i - 1, _do1 = m; i <= _do1; i++, i_++ ) { 
    sx[i_] = sa*sx[i_];
  }
  if( n < 5 ) 
    return;
L_40:
  mp1 = m + 1;
  for( i = mp1, i_ = i - 1, _do2 = n; i <= _do2; i += 5, i_ += 5 ) { 
    sx[i_] = sa*sx[i_];
    sx[i_ + 1] = sa*sx[i_ + 1];
    sx[i_ + 2] = sa*sx[i_ + 2];
    sx[i_ + 3] = sa*sx[i_ + 3];
    sx[i_ + 4] = sa*sx[i_ + 4];
  }
  return;
} // end of function 


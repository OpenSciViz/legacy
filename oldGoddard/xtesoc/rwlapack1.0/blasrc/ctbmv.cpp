/*
 * Default C++ implementation of ctbmv
 * For optimum performance, use a machine specific bla library
 *
 * $Id: ctbmv.cpp,v 1.3 1993/03/19 16:26:58 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 06:02:50
 * FOR_C++ Options SET: alloc do=rt no=p pf=cbla,sbla,xbla s=dv str=l - prototypes
 *
 * $Log: ctbmv.cpp,v $
 * Revision 1.3  1993/03/19  16:26:58  alv
 * added RWBLADECL linkage specification
 *
 * Revision 1.2  1993/03/05  23:07:03  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:04:30  alv
 * Initial revision
 *
 */

#define RW_CPPBLAS 1
#include "rw/bla.h"
#include "rw/bla.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/fcomplex.h"

// PARAMETER translations
const FComplex ZERO = FComplex(0.0e0,0.0e0);
// end of PARAMETER translations

RWBLADECL void /*FUNCTION*/ ctbmv(const char &uplo, const char &trans, const char &diag, const long &n, 
 const long &k, FComplex *a, const long &lda, FComplex x[], const long &incx)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  int noconj, nounit;
  long _do0, _do1, _do10, _do11, _do12, _do13, _do14, _do15, 
   _do2, _do3, _do4, _do5, _do6, _do7, _do8, _do9, i, i_, info, 
   ix, j, j_, jx, kplus1, kx, l;
  FComplex temp;

  //     .. Scalar Arguments ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  CTBMV  performs one of the matrix-vector operations
  
  //     x := A*x,   or   x := A'*x,   or   x := conjg( A' )*x,
  
  //  where x is an n element vector and  A is an n by n unit, or non-unit,
  //  upper or lower triangular band matrix, with ( k + 1 ) diagonals.
  
  //  Parameters
  //  ==========
  
  //  UPLO   - CHARACTER*1.
  //           On entry, UPLO specifies whether the matrix is an upper or
  //           lower triangular matrix as follows:
  
  //              UPLO = 'U' or 'u'   A is an upper triangular matrix.
  
  //              UPLO = 'L' or 'l'   A is a lower triangular matrix.
  
  //           Unchanged on exit.
  
  //  TRANS  - CHARACTER*1.
  //           On entry, TRANS specifies the operation to be performed as
  //           follows:
  
  //              TRANS = 'N' or 'n'   x := A*x.
  
  //              TRANS = 'T' or 't'   x := A'*x.
  
  //              TRANS = 'C' or 'c'   x := conjg( A' )*x.
  
  //           Unchanged on exit.
  
  //  DIAG   - CHARACTER*1.
  //           On entry, DIAG specifies whether or not A is unit
  //           triangular as follows:
  
  //              DIAG = 'U' or 'u'   A is assumed to be unit triangular.
  
  //              DIAG = 'N' or 'n'   A is not assumed to be unit
  //                                  triangular.
  
  //           Unchanged on exit.
  
  //  N      - INTEGER.
  //           On entry, N specifies the order of the matrix A.
  //           N must be at least zero.
  //           Unchanged on exit.
  
  //  K      - INTEGER.
  //           On entry with UPLO = 'U' or 'u', K specifies the number of
  //           super-diagonals of the matrix A.
  //           On entry with UPLO = 'L' or 'l', K specifies the number of
  //           sub-diagonals of the matrix A.
  //           K must satisfy  0 .le. K.
  //           Unchanged on exit.
  
  //  A      - COMPLEX          array of DIMENSION ( LDA, n ).
  //           Before entry with UPLO = 'U' or 'u', the leading ( k + 1 )
  //           by n part of the array A must contain the upper triangular
  //           band part of the matrix of coefficients, supplied column by
  //           column, with the leading diagonal of the matrix in row
  //           ( k + 1 ) of the array, the first super-diagonal starting at
  //           position 2 in row k, and so on. The top left k by k triangle
  //           of the array A is not referenced.
  //           The following program segment will transfer an upper
  //           triangular band matrix from conventional full matrix storage
  //           to band storage:
  
  //                 DO 20, J = 1, N
  //                    M = K + 1 - J
  //                    DO 10, I = MAX( 1, J - K ), J
  //                       A( M + I, J ) = matrix( I, J )
  //              10    CONTINUE
  //              20 CONTINUE
  
  //           Before entry with UPLO = 'L' or 'l', the leading ( k + 1 )
  //           by n part of the array A must contain the lower triangular
  //           band part of the matrix of coefficients, supplied column by
  //           column, with the leading diagonal of the matrix in row 1 of
  //           the array, the first sub-diagonal starting at position 1 in
  //           row 2, and so on. The bottom right k by k triangle of the
  //           array A is not referenced.
  //           The following program segment will transfer a lower
  //           triangular band matrix from conventional full matrix storage
  //           to band storage:
  
  //                 DO 20, J = 1, N
  //                    M = 1 - J
  //                    DO 10, I = J, MIN( N, J + K )
  //                       A( M + I, J ) = matrix( I, J )
  //              10    CONTINUE
  //              20 CONTINUE
  
  //           Note that when DIAG = 'U' or 'u' the elements of the array A
  //           corresponding to the diagonal elements of the matrix are not
  //           referenced, but are assumed to be unity.
  //           Unchanged on exit.
  
  //  LDA    - INTEGER.
  //           On entry, LDA specifies the first dimension of A as declared
  //           in the calling (sub) program. LDA must be at least
  //           ( k + 1 ).
  //           Unchanged on exit.
  
  //  X      - COMPLEX          array of dimension at least
  //           ( 1 + ( n - 1 )*abs( INCX ) ).
  //           Before entry, the incremented array X must contain the n
  //           element vector x. On exit, X is overwritten with the
  //           tranformed vector x.
  
  //  INCX   - INTEGER.
  //           On entry, INCX specifies the increment for the elements of
  //           X. INCX must not be zero.
  //           Unchanged on exit.
  
  
  //  Level 2 Blas routine.
  
  //  -- Written on 22-October-1986.
  //     Jack Dongarra, Argonne National Lab.
  //     Jeremy Du Croz, Nag Central Office.
  //     Sven Hammarling, Nag Central Office.
  //     Richard Hanson, Sandia National Labs.
  
  
  //     .. Parameters ..
  //     .. Local Scalars ..
  //     .. External Functions ..
  //     .. External Subroutines ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  if( !lsame( uplo, 'U' ) && !lsame( uplo, 'L' ) ) { 
    info = 1;
  }
  else if( (!lsame( trans, 'N' ) && !lsame( trans, 'T' )) && !lsame( trans, 
   'C' ) ) { 
    info = 2;
  }
  else if( !lsame( diag, 'U' ) && !lsame( diag, 'N' ) ) { 
    info = 3;
  }
  else if( n < 0 ) { 
    info = 4;
  }
  else if( k < 0 ) { 
    info = 5;
  }
  else if( lda < (k + 1) ) { 
    info = 7;
  }
  else if( incx == 0 ) { 
    info = 9;
  }
  if( info != 0 ) { 
    xerbla( "CTBMV ", info );
    return;
  }
  
  //     Quick return if possible.
  
  if( n == 0 ) 
    return;
  
  noconj = lsame( trans, 'T' );
  nounit = lsame( diag, 'N' );
  
  //     Set up the start point in X if the increment is not unity. This
  //     will be  ( N - 1 )*INCX   too small for descending loops.
  
  if( incx <= 0 ) { 
    kx = 1 - (n - 1)*incx;
  }
  else if( incx != 1 ) { 
    kx = 1;
  }
  
  //     Start the operations. In this version the elements of A are
  //     accessed sequentially with one pass through A.
  
  if( lsame( trans, 'N' ) ) { 
    
    //         Form  x := A*x.
    
    if( lsame( uplo, 'U' ) ) { 
      kplus1 = k + 1;
      if( incx == 1 ) { 
        for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
          if( x[j_] != ZERO ) { 
            temp = x[j_];
            l = kplus1 - j;
            for( i = max( 1, j - k ), i_ = i - 1, _do1 = j - 
             1; i <= _do1; i++, i_++ ) { 
              x[i_] = x[i_] + temp*A(j_,l + i_);
            }
            if( nounit ) 
              x[j_] = x[j_]*A(j_,kplus1 - 1);
          }
        }
      }
      else { 
        jx = kx;
        for( j = 1, j_ = j - 1, _do2 = n; j <= _do2; j++, j_++ ) { 
          if( x[jx - 1] != ZERO ) { 
            temp = x[jx - 1];
            ix = kx;
            l = kplus1 - j;
            for( i = max( 1, j - k ), i_ = i - 1, _do3 = j - 
             1; i <= _do3; i++, i_++ ) { 
              x[ix - 1] = x[ix - 1] + temp*A(j_,l + i_);
              ix = ix + incx;
            }
            if( nounit ) 
              x[jx - 1] = x[jx - 1]*A(j_,kplus1 - 1);
          }
          jx = jx + incx;
          if( j > k ) 
            kx = kx + incx;
        }
      }
    }
    else { 
      if( incx == 1 ) { 
        for( j = n, j_ = j - 1; j >= 1; j--, j_-- ) { 
          if( x[j_] != ZERO ) { 
            temp = x[j_];
            l = 1 - j;
            for( i = min( n, j + k ), i_ = i - 1, _do4 = j + 
             1; i >= _do4; i--, i_-- ) { 
              x[i_] = x[i_] + temp*A(j_,l + i_);
            }
            if( nounit ) 
              x[j_] = x[j_]*A(j_,0);
          }
        }
      }
      else { 
        kx = kx + (n - 1)*incx;
        jx = kx;
        for( j = n, j_ = j - 1; j >= 1; j--, j_-- ) { 
          if( x[jx - 1] != ZERO ) { 
            temp = x[jx - 1];
            ix = kx;
            l = 1 - j;
            for( i = min( n, j + k ), i_ = i - 1, _do5 = j + 
             1; i >= _do5; i--, i_-- ) { 
              x[ix - 1] = x[ix - 1] + temp*A(j_,l + i_);
              ix = ix - incx;
            }
            if( nounit ) 
              x[jx - 1] = x[jx - 1]*A(j_,0);
          }
          jx = jx - incx;
          if( (n - j) >= k ) 
            kx = kx - incx;
        }
      }
    }
  }
  else { 
    
    //        Form  x := A'*x  or  x := conjg( A' )*x.
    
    if( lsame( uplo, 'U' ) ) { 
      kplus1 = k + 1;
      if( incx == 1 ) { 
        for( j = n, j_ = j - 1; j >= 1; j--, j_-- ) { 
          temp = x[j_];
          l = kplus1 - j;
          if( noconj ) { 
            if( nounit ) 
              temp = temp*A(j_,kplus1 - 1);
            for( i = j - 1, i_ = i - 1, _do6 = max( 1, 
             j - k ); i >= _do6; i--, i_-- ) { 
              temp = temp + A(j_,l + i_)*x[i_];
            }
          }
          else { 
            if( nounit ) 
              temp = temp*conj( A(j_,kplus1 - 1) );
            for( i = j - 1, i_ = i - 1, _do7 = max( 1, 
             j - k ); i >= _do7; i--, i_-- ) { 
              temp = temp + conj( A(j_,l + i_) )*x[i_];
            }
          }
          x[j_] = temp;
        }
      }
      else { 
        kx = kx + (n - 1)*incx;
        jx = kx;
        for( j = n, j_ = j - 1; j >= 1; j--, j_-- ) { 
          temp = x[jx - 1];
          kx = kx - incx;
          ix = kx;
          l = kplus1 - j;
          if( noconj ) { 
            if( nounit ) 
              temp = temp*A(j_,kplus1 - 1);
            for( i = j - 1, i_ = i - 1, _do8 = max( 1, 
             j - k ); i >= _do8; i--, i_-- ) { 
              temp = temp + A(j_,l + i_)*x[ix - 1];
              ix = ix - incx;
            }
          }
          else { 
            if( nounit ) 
              temp = temp*conj( A(j_,kplus1 - 1) );
            for( i = j - 1, i_ = i - 1, _do9 = max( 1, 
             j - k ); i >= _do9; i--, i_-- ) { 
              temp = temp + conj( A(j_,l + i_) )*x[ix - 1];
              ix = ix - incx;
            }
          }
          x[jx - 1] = temp;
          jx = jx - incx;
        }
      }
    }
    else { 
      if( incx == 1 ) { 
        for( j = 1, j_ = j - 1, _do10 = n; j <= _do10; j++, j_++ ) { 
          temp = x[j_];
          l = 1 - j;
          if( noconj ) { 
            if( nounit ) 
              temp = temp*A(j_,0);
            for( i = j + 1, i_ = i - 1, _do11 = min( n, 
             j + k ); i <= _do11; i++, i_++ ) { 
              temp = temp + A(j_,l + i_)*x[i_];
            }
          }
          else { 
            if( nounit ) 
              temp = temp*conj( A(j_,0) );
            for( i = j + 1, i_ = i - 1, _do12 = min( n, 
             j + k ); i <= _do12; i++, i_++ ) { 
              temp = temp + conj( A(j_,l + i_) )*x[i_];
            }
          }
          x[j_] = temp;
        }
      }
      else { 
        jx = kx;
        for( j = 1, j_ = j - 1, _do13 = n; j <= _do13; j++, j_++ ) { 
          temp = x[jx - 1];
          kx = kx + incx;
          ix = kx;
          l = 1 - j;
          if( noconj ) { 
            if( nounit ) 
              temp = temp*A(j_,0);
            for( i = j + 1, i_ = i - 1, _do14 = min( n, 
             j + k ); i <= _do14; i++, i_++ ) { 
              temp = temp + A(j_,l + i_)*x[ix - 1];
              ix = ix + incx;
            }
          }
          else { 
            if( nounit ) 
              temp = temp*conj( A(j_,0) );
            for( i = j + 1, i_ = i - 1, _do15 = min( n, 
             j + k ); i <= _do15; i++, i_++ ) { 
              temp = temp + conj( A(j_,l + i_) )*x[ix - 1];
              ix = ix + incx;
            }
          }
          x[jx - 1] = temp;
          jx = jx + incx;
        }
      }
    }
  }
  
  return;
  
  //     End of CTBMV .
  
#undef  A
} // end of function 


/*
 * Default C++ implementation of zhbmv
 * For optimum performance, use a machine specific bla library
 *
 * $Id: zhbmv.cpp,v 1.3 1993/03/19 16:26:58 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 06:03:43
 * FOR_C++ Options SET: alloc do=rt no=p pf=zbla,dbla,xbla s=dv str=l - prototypes
 *
 * $Log: zhbmv.cpp,v $
 * Revision 1.3  1993/03/19  16:26:58  alv
 * added RWBLADECL linkage specification
 *
 * Revision 1.2  1993/03/05  23:08:26  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:05:16  alv
 * Initial revision
 *
 */

#define RW_CPPBLAS 1
#include "rw/bla.h"
#include "rw/bla.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const DComplex ONE = DComplex(1.0e0,0.0e0);
const DComplex ZERO = DComplex(0.0e0,0.0e0);
// end of PARAMETER translations

RWBLADECL void /*FUNCTION*/ zhbmv(const char &uplo, const long &n, const long &k, const DComplex &alpha, 
 DComplex *a, const long &lda, DComplex x[], const long &incx, const DComplex &beta, 
 DComplex y[], const long &incy)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  long _do0, _do1, _do10, _do11, _do2, _do3, _do4, _do5, _do6, 
   _do7, _do8, _do9, i, i_, info, ix, iy, j, j_, jx, jy, kplus1, 
   kx, ky, l;
  DComplex temp1, temp2;

  //     .. Scalar Arguments ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZHBMV  performs the matrix-vector  operation
  
  //     y := alpha*A*x + beta*y,
  
  //  where alpha and beta are scalars, x and y are n element vectors and
  //  A is an n by n hermitian band matrix, with k super-diagonals.
  
  //  Parameters
  //  ==========
  
  //  UPLO   - CHARACTER*1.
  //           On entry, UPLO specifies whether the upper or lower
  //           triangular part of the band matrix A is being supplied as
  //           follows:
  
  //              UPLO = 'U' or 'u'   The upper triangular part of A is
  //                                  being supplied.
  
  //              UPLO = 'L' or 'l'   The lower triangular part of A is
  //                                  being supplied.
  
  //           Unchanged on exit.
  
  //  N      - INTEGER.
  //           On entry, N specifies the order of the matrix A.
  //           N must be at least zero.
  //           Unchanged on exit.
  
  //  K      - INTEGER.
  //           On entry, K specifies the number of super-diagonals of the
  //           matrix A. K must satisfy  0 .le. K.
  //           Unchanged on exit.
  
  //  ALPHA  - COMPLEX*16      .
  //           On entry, ALPHA specifies the scalar alpha.
  //           Unchanged on exit.
  
  //  A      - COMPLEX*16       array of DIMENSION ( LDA, n ).
  //           Before entry with UPLO = 'U' or 'u', the leading ( k + 1 )
  //           by n part of the array A must contain the upper triangular
  //           band part of the hermitian matrix, supplied column by
  //           column, with the leading diagonal of the matrix in row
  //           ( k + 1 ) of the array, the first super-diagonal starting at
  //           position 2 in row k, and so on. The top left k by k triangle
  //           of the array A is not referenced.
  //           The following program segment will transfer the upper
  //           triangular part of a hermitian band matrix from conventional
  //           full matrix storage to band storage:
  
  //                 DO 20, J = 1, N
  //                    M = K + 1 - J
  //                    DO 10, I = MAX( 1, J - K ), J
  //                       A( M + I, J ) = matrix( I, J )
  //              10    CONTINUE
  //              20 CONTINUE
  
  //           Before entry with UPLO = 'L' or 'l', the leading ( k + 1 )
  //           by n part of the array A must contain the lower triangular
  //           band part of the hermitian matrix, supplied column by
  //           column, with the leading diagonal of the matrix in row 1 of
  //           the array, the first sub-diagonal starting at position 1 in
  //           row 2, and so on. The bottom right k by k triangle of the
  //           array A is not referenced.
  //           The following program segment will transfer the lower
  //           triangular part of a hermitian band matrix from conventional
  //           full matrix storage to band storage:
  
  //                 DO 20, J = 1, N
  //                    M = 1 - J
  //                    DO 10, I = J, MIN( N, J + K )
  //                       A( M + I, J ) = matrix( I, J )
  //              10    CONTINUE
  //              20 CONTINUE
  
  //           Note that the imaginary parts of the diagonal elements need
  //           not be set and are assumed to be zero.
  //           Unchanged on exit.
  
  //  LDA    - INTEGER.
  //           On entry, LDA specifies the first dimension of A as declared
  //           in the calling (sub) program. LDA must be at least
  //           ( k + 1 ).
  //           Unchanged on exit.
  
  //  X      - COMPLEX*16       array of DIMENSION at least
  //           ( 1 + ( n - 1 )*abs( INCX ) ).
  //           Before entry, the incremented array X must contain the
  //           vector x.
  //           Unchanged on exit.
  
  //  INCX   - INTEGER.
  //           On entry, INCX specifies the increment for the elements of
  //           X. INCX must not be zero.
  //           Unchanged on exit.
  
  //  BETA   - COMPLEX*16      .
  //           On entry, BETA specifies the scalar beta.
  //           Unchanged on exit.
  
  //  Y      - COMPLEX*16       array of DIMENSION at least
  //           ( 1 + ( n - 1 )*abs( INCY ) ).
  //           Before entry, the incremented array Y must contain the
  //           vector y. On exit, Y is overwritten by the updated vector y.
  
  //  INCY   - INTEGER.
  //           On entry, INCY specifies the increment for the elements of
  //           Y. INCY must not be zero.
  //           Unchanged on exit.
  
  
  //  Level 2 Blas routine.
  
  //  -- Written on 22-October-1986.
  //     Jack Dongarra, Argonne National Lab.
  //     Jeremy Du Croz, Nag Central Office.
  //     Sven Hammarling, Nag Central Office.
  //     Richard Hanson, Sandia National Labs.
  
  
  //     .. Parameters ..
  //     .. Local Scalars ..
  //     .. External Functions ..
  //     .. External Subroutines ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  if( !lsame( uplo, 'U' ) && !lsame( uplo, 'L' ) ) { 
    info = 1;
  }
  else if( n < 0 ) { 
    info = 2;
  }
  else if( k < 0 ) { 
    info = 3;
  }
  else if( lda < (k + 1) ) { 
    info = 6;
  }
  else if( incx == 0 ) { 
    info = 8;
  }
  else if( incy == 0 ) { 
    info = 11;
  }
  if( info != 0 ) { 
    xerbla( "ZHBMV ", info );
    return;
  }
  
  //     Quick return if possible.
  
  if( (n == 0) || ((ctocf(alpha) == ctocf(ZERO)) && (ctocf(beta) == 
   ctocf(ONE))) ) 
    return;
  
  //     Set up the start points in  X  and  Y.
  
  if( incx > 0 ) { 
    kx = 1;
  }
  else { 
    kx = 1 - (n - 1)*incx;
  }
  if( incy > 0 ) { 
    ky = 1;
  }
  else { 
    ky = 1 - (n - 1)*incy;
  }
  
  //     Start the operations. In this version the elements of the array A
  //     are accessed sequentially with one pass through A.
  
  //     First form  y := beta*y.
  
  if( ctocf(beta) != ctocf(ONE) ) { 
    if( incy == 1 ) { 
      if( ctocf(beta) == ctocf(ZERO) ) { 
        for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
          y[i_] = ZERO;
        }
      }
      else { 
        for( i = 1, i_ = i - 1, _do1 = n; i <= _do1; i++, i_++ ) { 
          y[i_] = beta*y[i_];
        }
      }
    }
    else { 
      iy = ky;
      if( ctocf(beta) == ctocf(ZERO) ) { 
        for( i = 1, i_ = i - 1, _do2 = n; i <= _do2; i++, i_++ ) { 
          y[iy - 1] = ZERO;
          iy = iy + incy;
        }
      }
      else { 
        for( i = 1, i_ = i - 1, _do3 = n; i <= _do3; i++, i_++ ) { 
          y[iy - 1] = beta*y[iy - 1];
          iy = iy + incy;
        }
      }
    }
  }
  if( ctocf(alpha) == ctocf(ZERO) ) 
    return;
  if( lsame( uplo, 'U' ) ) { 
    
    //        Form  y  when upper triangle of A is stored.
    
    kplus1 = k + 1;
    if( (incx == 1) && (incy == 1) ) { 
      for( j = 1, j_ = j - 1, _do4 = n; j <= _do4; j++, j_++ ) { 
        temp1 = alpha*x[j_];
        temp2 = ZERO;
        l = kplus1 - j;
        for( i = max( 1, j - k ), i_ = i - 1, _do5 = j - 1; i <= _do5; i++, i_++ ) { 
          y[i_] = y[i_] + temp1*A(j_,l + i_);
          temp2 = temp2 + conj( A(j_,l + i_) )*x[i_];
        }
        y[j_] = y[j_] + temp1*real( A(j_,kplus1 - 1) ) + alpha*
         temp2;
      }
    }
    else { 
      jx = kx;
      jy = ky;
      for( j = 1, j_ = j - 1, _do6 = n; j <= _do6; j++, j_++ ) { 
        temp1 = alpha*x[jx - 1];
        temp2 = ZERO;
        ix = kx;
        iy = ky;
        l = kplus1 - j;
        for( i = max( 1, j - k ), i_ = i - 1, _do7 = j - 1; i <= _do7; i++, i_++ ) { 
          y[iy - 1] = y[iy - 1] + temp1*A(j_,l + i_);
          temp2 = temp2 + conj( A(j_,l + i_) )*x[ix - 1];
          ix = ix + incx;
          iy = iy + incy;
        }
        y[jy - 1] = y[jy - 1] + temp1*real( A(j_,kplus1 - 1) ) + 
         alpha*temp2;
        jx = jx + incx;
        jy = jy + incy;
        if( j > k ) { 
          kx = kx + incx;
          ky = ky + incy;
        }
      }
    }
  }
  else { 
    
    //        Form  y  when lower triangle of A is stored.
    
    if( (incx == 1) && (incy == 1) ) { 
      for( j = 1, j_ = j - 1, _do8 = n; j <= _do8; j++, j_++ ) { 
        temp1 = alpha*x[j_];
        temp2 = ZERO;
        y[j_] = y[j_] + temp1*real( A(j_,0) );
        l = 1 - j;
        for( i = j + 1, i_ = i - 1, _do9 = min( n, j + k ); i <= _do9; i++, i_++ ) { 
          y[i_] = y[i_] + temp1*A(j_,l + i_);
          temp2 = temp2 + conj( A(j_,l + i_) )*x[i_];
        }
        y[j_] = y[j_] + alpha*temp2;
      }
    }
    else { 
      jx = kx;
      jy = ky;
      for( j = 1, j_ = j - 1, _do10 = n; j <= _do10; j++, j_++ ) { 
        temp1 = alpha*x[jx - 1];
        temp2 = ZERO;
        y[jy - 1] = y[jy - 1] + temp1*real( A(j_,0) );
        l = 1 - j;
        ix = jx;
        iy = jy;
        for( i = j + 1, i_ = i - 1, _do11 = min( n, j + k ); i <= _do11; i++, i_++ ) { 
          ix = ix + incx;
          iy = iy + incy;
          y[iy - 1] = y[iy - 1] + temp1*A(j_,l + i_);
          temp2 = temp2 + conj( A(j_,l + i_) )*x[ix - 1];
        }
        y[jy - 1] = y[jy - 1] + alpha*temp2;
        jx = jx + incx;
        jy = jy + incy;
      }
    }
  }
  
  return;
  
  //     End of ZHBMV .
  
#undef  A
} // end of function 


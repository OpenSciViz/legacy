/*
 * Default C++ implementation of dasum
 * For optimum performance, use a machine specific bla library
 *
 * $Id: dasum.cpp,v 1.3 1993/03/19 16:26:58 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/17/93 at 14:40:06
 * FOR_C++ Options SET: alloc do=rt no=p pf=dbla,xbla s=dv str=l - prototypes
 *
 * $Log: dasum.cpp,v $
 * Revision 1.3  1993/03/19  16:26:58  alv
 * added RWBLADECL linkage specification
 *
 * Revision 1.2  1993/03/05  23:07:09  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:04:33  alv
 * Initial revision
 *
 */

#define RW_CPPBLAS 1
#include "rw/bla.h"
#include "rw/bla.h"
#include "rw/fortran.h" /* Fortran run time library */

RWBLADECL double /*FUNCTION*/ dasum(const long &n, double dx[], const long &incx)
{
  long _do0, _do1, _do2, i, i_, ix, m, mp1;
  double dasum_v, dtemp;

  
  //     takes the sum of the absolute values.
  //     uses unrolled loops for increment equal to one.
  //     jack dongarra, linpack, 3/11/78.
  //     modified to correct problem with negative increment, 8/21/90.
  
  
  dasum_v = 0.0e0;
  dtemp = 0.0e0;
  if( n <= 0 ) 
    return( dasum_v );
  if( incx == 1 ) 
    goto L_20;
  
  //        code for increment not equal to 1
  
  ix = 1;
  if( incx < 0 ) 
    ix = (-n + 1)*incx + 1;
  for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
    dtemp = dtemp + abs( dx[ix - 1] );
    ix = ix + incx;
  }
  dasum_v = dtemp;
  return( dasum_v );
  
  //        code for increment equal to 1
  
  
  //        clean-up loop
  
L_20:
  // mod() function massaged out of next line
  m = n % 6; //m = mod( n, 6 );
  if( m == 0 ) 
    goto L_40;
  for( i = 1, i_ = i - 1, _do1 = m; i <= _do1; i++, i_++ ) { 
    dtemp = dtemp + abs( dx[i_] );
  }
  if( n < 6 ) 
    goto L_60;
L_40:
  mp1 = m + 1;
  for( i = mp1, i_ = i - 1, _do2 = n; i <= _do2; i += 6, i_ += 6 ) { 
    dtemp = dtemp + abs( dx[i_] ) + abs( dx[i_ + 1] ) + abs( dx[i_ + 2] ) + 
     abs( dx[i_ + 3] ) + abs( dx[i_ + 4] ) + abs( dx[i_ + 5] );
  }
L_60:
  dasum_v = dtemp;
  return( dasum_v );
} // end of function 


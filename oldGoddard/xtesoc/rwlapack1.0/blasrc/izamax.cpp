/*
 * Default C++ implementation of izamax
 * For optimum performance, use a machine specific bla library
 *
 * $Id: izamax.cpp,v 1.3 1993/03/19 16:26:58 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 06:03:43
 * FOR_C++ Options SET: alloc do=rt no=p pf=zbla,dbla,xbla s=dv str=l - prototypes
 *
 * $Log: izamax.cpp,v $
 * Revision 1.3  1993/03/19  16:26:58  alv
 * added RWBLADECL linkage specification
 *
 * Revision 1.2  1993/03/05  23:07:42  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:04:50  alv
 * Initial revision
 *
 */

#define RW_CPPBLAS 1
#include "rw/bla.h"
#include "rw/bla.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

RWBLADECL long /*FUNCTION*/ izamax(const long &n, DComplex zx[], const long &incx)
{
  long _do0, _do1, i, i_, ix, izamax_v;
  double smax;

  
  //     finds the index of element having max. absolute value.
  //     jack dongarra, 1/15/85.
  //     modified to correct problem with negative increment, 8/21/90.
  
  
  izamax_v = 0;
  if( n < 1 ) 
    return( izamax_v );
  izamax_v = 1;
  if( n == 1 ) 
    return( izamax_v );
  if( incx == 1 ) 
    goto L_20;
  
  //        code for increment not equal to 1
  
  ix = 1;
  if( incx < 0 ) 
    ix = (-n + 1)*incx + 1;
  smax = dcabs1( zx[ix - 1] );
  ix = ix + incx;
  for( i = 2, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
    if( dcabs1( zx[ix - 1] ) <= smax ) 
      goto L_5;
    izamax_v = i;
    smax = dcabs1( zx[ix - 1] );
L_5:
    ix = ix + incx;
  }
  return( izamax_v );
  
  //        code for increment equal to 1
  
L_20:
  smax = dcabs1( zx[0] );
  for( i = 2, i_ = i - 1, _do1 = n; i <= _do1; i++, i_++ ) { 
    if( dcabs1( zx[i_] ) <= smax ) 
      goto L_30;
    izamax_v = i;
    smax = dcabs1( zx[i_] );
L_30:
    ;
  }
  return( izamax_v );
} // end of function 


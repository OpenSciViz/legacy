/*
 * Default C++ implementation of srotg
 * For optimum performance, use a machine specific bla library
 *
 * $Id: srotg.cpp,v 1.4 1993/03/19 16:26:58 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/17/93 at 14:39:10
 * FOR_C++ Options SET: alloc do=rt no=p pf=sbla,xbla s=dv str=l - prototypes
 *
 * $Log: srotg.cpp,v $
 * Revision 1.4  1993/03/19  16:26:58  alv
 * added RWBLADECL linkage specification
 *
 * Revision 1.3  1993/03/06  00:55:18  alv
 * eliminated const where info is passed back through args
 *
 * Revision 1.2  1993/03/05  23:07:52  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:04:56  alv
 * Initial revision
 *
 */

#define RW_CPPBLAS 1
#include "rw/bla.h"
#include "rw/bla.h"
#include "rw/fortran.h" /* Fortran run time library */

RWBLADECL void /*FUNCTION*/ srotg(float &sa, float &sb, float &c, float &s)
{
  float r, roe, scale, z;

  
  //     construct givens plane rotation.
  //     jack dongarra, linpack, 3/11/78.
  
  
  roe = sb;
  if( abs( sa ) > abs( sb ) ) 
    roe = sa;
  scale = abs( sa ) + abs( sb );
  if( scale != 0.0 ) 
    goto L_10;
  c = 1.0;
  s = 0.0;
  r = 0.0;
  z = 0.0;
  goto L_20;
L_10:
  r = scale*sqrt( pow(sa/scale, 2) + pow(sb/scale, 2) );
  if (roe<0) { r = (-r); }  //r = sign( 1.0, roe )*r;
  c = sa/r;
  s = sb/r;
  z = 1.0;
  if( abs( sa ) > abs( sb ) ) 
    z = s;
  if( abs( sb ) >= abs( sa ) && c != 0.0 ) 
    z = 1.0/c;
L_20:
  sa = r;
  sb = z;
  return;
} // end of function 


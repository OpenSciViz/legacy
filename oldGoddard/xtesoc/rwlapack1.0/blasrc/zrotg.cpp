/*
 * Default C++ implementation of zrotg
 * For optimum performance, use a machine specific bla library
 *
 * $Id: zrotg.cpp,v 1.4 1993/03/19 16:26:58 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 06:03:43
 * FOR_C++ Options SET: alloc do=rt no=p pf=zbla,dbla,xbla s=dv str=l - prototypes
 *
 * $Log: zrotg.cpp,v $
 * Revision 1.4  1993/03/19  16:26:58  alv
 * added RWBLADECL linkage specification
 *
 * Revision 1.3  1993/03/06  00:55:18  alv
 * eliminated const where info is passed back through args
 *
 * Revision 1.2  1993/03/05  23:08:34  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:05:20  alv
 * Initial revision
 *
 */

#define RW_CPPBLAS 1
#include "rw/bla.h"
#include "rw/bla.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

RWBLADECL void /*FUNCTION*/ zrotg(DComplex &ca, DComplex &cb, double &c, DComplex &s)
{
  double norm, scale;
  DComplex alpha;
  double tmp1,tmp2;

  if( abs( ca ) != 0.0e0 ) 
    goto L_10;
  c = 0.0e0;
  s = DComplex(1.0e0,0.0e0);
  ca = cb;
  goto L_20;
L_10:
  ;
  scale = abs( ca ) + abs( cb );
  //norm = scale*sqrt( pow(abs( ca/DComplex( scale, 0.0e0 ) ), 2) + 
  // pow(abs( cb/DComplex( scale, 0.0e0 ) ), 2) );
  tmp1 = abs(ca/scale);
  tmp2 = abs(cb/scale);
  norm = scale*sqrt(tmp1*tmp1 + tmp2*tmp2);
  alpha = ca/abs( ca );
  c = abs( ca )/norm;
  s = alpha*conj( cb )/norm;
  ca = alpha*norm;
L_20:
  ;
  return;
} // end of function 


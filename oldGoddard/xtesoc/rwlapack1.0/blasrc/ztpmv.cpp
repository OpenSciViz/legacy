/*
 * Default C++ implementation of ztpmv
 * For optimum performance, use a machine specific bla library
 *
 * $Id: ztpmv.cpp,v 1.3 1993/03/19 16:26:58 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 06:03:43
 * FOR_C++ Options SET: alloc do=rt no=p pf=zbla,dbla,xbla s=dv str=l - prototypes
 *
 * $Log: ztpmv.cpp,v $
 * Revision 1.3  1993/03/19  16:26:58  alv
 * added RWBLADECL linkage specification
 *
 * Revision 1.2  1993/03/05  23:08:41  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:05:22  alv
 * Initial revision
 *
 */

#define RW_CPPBLAS 1
#include "rw/bla.h"
#include "rw/bla.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const DComplex ZERO = DComplex(0.0e0,0.0e0);
// end of PARAMETER translations

RWBLADECL void /*FUNCTION*/ ztpmv(const char &uplo, const char &trans, const char &diag, const long &n, 
 DComplex ap[], DComplex x[], const long &incx)
{
  int noconj, nounit;
  long _do0, _do1, _do10, _do11, _do12, _do13, _do2, _do3, _do4, 
   _do5, _do6, _do7, _do8, _do9, i, i_, info, ix, j, j_, jx, k, 
   k_, kk, kx;
  DComplex temp;

  //     .. Scalar Arguments ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZTPMV  performs one of the matrix-vector operations
  
  //     x := A*x,   or   x := A'*x,   or   x := conjg( A' )*x,
  
  //  where x is an n element vector and  A is an n by n unit, or non-unit,
  //  upper or lower triangular matrix, supplied in packed form.
  
  //  Parameters
  //  ==========
  
  //  UPLO   - CHARACTER*1.
  //           On entry, UPLO specifies whether the matrix is an upper or
  //           lower triangular matrix as follows:
  
  //              UPLO = 'U' or 'u'   A is an upper triangular matrix.
  
  //              UPLO = 'L' or 'l'   A is a lower triangular matrix.
  
  //           Unchanged on exit.
  
  //  TRANS  - CHARACTER*1.
  //           On entry, TRANS specifies the operation to be performed as
  //           follows:
  
  //              TRANS = 'N' or 'n'   x := A*x.
  
  //              TRANS = 'T' or 't'   x := A'*x.
  
  //              TRANS = 'C' or 'c'   x := conjg( A' )*x.
  
  //           Unchanged on exit.
  
  //  DIAG   - CHARACTER*1.
  //           On entry, DIAG specifies whether or not A is unit
  //           triangular as follows:
  
  //              DIAG = 'U' or 'u'   A is assumed to be unit triangular.
  
  //              DIAG = 'N' or 'n'   A is not assumed to be unit
  //                                  triangular.
  
  //           Unchanged on exit.
  
  //  N      - INTEGER.
  //           On entry, N specifies the order of the matrix A.
  //           N must be at least zero.
  //           Unchanged on exit.
  
  //  AP     - COMPLEX*16       array of DIMENSION at least
  //           ( ( n*( n + 1 ) )/2 ).
  //           Before entry with  UPLO = 'U' or 'u', the array AP must
  //           contain the upper triangular matrix packed sequentially,
  //           column by column, so that AP( 1 ) contains a( 1, 1 ),
  //           AP( 2 ) and AP( 3 ) contain a( 1, 2 ) and a( 2, 2 )
  //           respectively, and so on.
  //           Before entry with UPLO = 'L' or 'l', the array AP must
  //           contain the lower triangular matrix packed sequentially,
  //           column by column, so that AP( 1 ) contains a( 1, 1 ),
  //           AP( 2 ) and AP( 3 ) contain a( 2, 1 ) and a( 3, 1 )
  //           respectively, and so on.
  //           Note that when  DIAG = 'U' or 'u', the diagonal elements of
  //           A are not referenced, but are assumed to be unity.
  //           Unchanged on exit.
  
  //  X      - COMPLEX*16       array of dimension at least
  //           ( 1 + ( n - 1 )*abs( INCX ) ).
  //           Before entry, the incremented array X must contain the n
  //           element vector x. On exit, X is overwritten with the
  //           tranformed vector x.
  
  //  INCX   - INTEGER.
  //           On entry, INCX specifies the increment for the elements of
  //           X. INCX must not be zero.
  //           Unchanged on exit.
  
  
  //  Level 2 Blas routine.
  
  //  -- Written on 22-October-1986.
  //     Jack Dongarra, Argonne National Lab.
  //     Jeremy Du Croz, Nag Central Office.
  //     Sven Hammarling, Nag Central Office.
  //     Richard Hanson, Sandia National Labs.
  
  
  //     .. Parameters ..
  //     .. Local Scalars ..
  //     .. External Functions ..
  //     .. External Subroutines ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  if( !lsame( uplo, 'U' ) && !lsame( uplo, 'L' ) ) { 
    info = 1;
  }
  else if( (!lsame( trans, 'N' ) && !lsame( trans, 'T' )) && !lsame( trans, 
   'C' ) ) { 
    info = 2;
  }
  else if( !lsame( diag, 'U' ) && !lsame( diag, 'N' ) ) { 
    info = 3;
  }
  else if( n < 0 ) { 
    info = 4;
  }
  else if( incx == 0 ) { 
    info = 7;
  }
  if( info != 0 ) { 
    xerbla( "ZTPMV ", info );
    return;
  }
  
  //     Quick return if possible.
  
  if( n == 0 ) 
    return;
  
  noconj = lsame( trans, 'T' );
  nounit = lsame( diag, 'N' );
  
  //     Set up the start point in X if the increment is not unity. This
  //     will be  ( N - 1 )*INCX  too small for descending loops.
  
  if( incx <= 0 ) { 
    kx = 1 - (n - 1)*incx;
  }
  else if( incx != 1 ) { 
    kx = 1;
  }
  
  //     Start the operations. In this version the elements of AP are
  //     accessed sequentially with one pass through AP.
  
  if( lsame( trans, 'N' ) ) { 
    
    //        Form  x:= A*x.
    
    if( lsame( uplo, 'U' ) ) { 
      kk = 1;
      if( incx == 1 ) { 
        for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
          if( ctocf(x[j_]) != ctocf(ZERO) ) { 
            temp = x[j_];
            k = kk;
            for( i = 1, i_ = i - 1, _do1 = j - 1; i <= _do1; i++, i_++ ) { 
              x[i_] = x[i_] + temp*ap[k - 1];
              k = k + 1;
            }
            if( nounit ) 
              x[j_] = x[j_]*ap[kk + j_ - 1];
          }
          kk = kk + j;
        }
      }
      else { 
        jx = kx;
        for( j = 1, j_ = j - 1, _do2 = n; j <= _do2; j++, j_++ ) { 
          if( ctocf(x[jx - 1]) != ctocf(ZERO) ) { 
            temp = x[jx - 1];
            ix = kx;
            for( k = kk, k_ = k - 1, _do3 = kk + j - 2; k <= _do3; k++, k_++ ) { 
              x[ix - 1] = x[ix - 1] + temp*ap[k_];
              ix = ix + incx;
            }
            if( nounit ) 
              x[jx - 1] = x[jx - 1]*ap[kk + j_ - 1];
          }
          jx = jx + incx;
          kk = kk + j;
        }
      }
    }
    else { 
      kk = (n*(n + 1))/2;
      if( incx == 1 ) { 
        for( j = n, j_ = j - 1; j >= 1; j--, j_-- ) { 
          if( ctocf(x[j_]) != ctocf(ZERO) ) { 
            temp = x[j_];
            k = kk;
            for( i = n, i_ = i - 1, _do4 = j + 1; i >= _do4; i--, i_-- ) { 
              x[i_] = x[i_] + temp*ap[k - 1];
              k = k - 1;
            }
            if( nounit ) 
              x[j_] = x[j_]*ap[kk - n + j_];
          }
          kk = kk - (n - j + 1);
        }
      }
      else { 
        kx = kx + (n - 1)*incx;
        jx = kx;
        for( j = n, j_ = j - 1; j >= 1; j--, j_-- ) { 
          if( ctocf(x[jx - 1]) != ctocf(ZERO) ) { 
            temp = x[jx - 1];
            ix = kx;
            for( k = kk, k_ = k - 1, _do5 = kk - (n - 
             (j + 1)); k >= _do5; k--, k_-- ) { 
              x[ix - 1] = x[ix - 1] + temp*ap[k_];
              ix = ix - incx;
            }
            if( nounit ) 
              x[jx - 1] = x[jx - 1]*ap[kk - n + j_];
          }
          jx = jx - incx;
          kk = kk - (n - j + 1);
        }
      }
    }
  }
  else { 
    
    //        Form  x := A'*x  or  x := conjg( A' )*x.
    
    if( lsame( uplo, 'U' ) ) { 
      kk = (n*(n + 1))/2;
      if( incx == 1 ) { 
        for( j = n, j_ = j - 1; j >= 1; j--, j_-- ) { 
          temp = x[j_];
          k = kk - 1;
          if( noconj ) { 
            if( nounit ) 
              temp = temp*ap[kk - 1];
            for( i = j - 1, i_ = i - 1; i >= 1; i--, i_-- ) { 
              temp = temp + ap[k - 1]*x[i_];
              k = k - 1;
            }
          }
          else { 
            if( nounit ) 
              temp = temp*conj( ap[kk - 1] );
            for( i = j - 1, i_ = i - 1; i >= 1; i--, i_-- ) { 
              temp = temp + conj( ap[k - 1] )*x[i_];
              k = k - 1;
            }
          }
          x[j_] = temp;
          kk = kk - j;
        }
      }
      else { 
        jx = kx + (n - 1)*incx;
        for( j = n, j_ = j - 1; j >= 1; j--, j_-- ) { 
          temp = x[jx - 1];
          ix = jx;
          if( noconj ) { 
            if( nounit ) 
              temp = temp*ap[kk - 1];
            for( k = kk - 1, k_ = k - 1, _do6 = kk - j + 
             1; k >= _do6; k--, k_-- ) { 
              ix = ix - incx;
              temp = temp + ap[k_]*x[ix - 1];
            }
          }
          else { 
            if( nounit ) 
              temp = temp*conj( ap[kk - 1] );
            for( k = kk - 1, k_ = k - 1, _do7 = kk - j + 
             1; k >= _do7; k--, k_-- ) { 
              ix = ix - incx;
              temp = temp + conj( ap[k_] )*x[ix - 1];
            }
          }
          x[jx - 1] = temp;
          jx = jx - incx;
          kk = kk - j;
        }
      }
    }
    else { 
      kk = 1;
      if( incx == 1 ) { 
        for( j = 1, j_ = j - 1, _do8 = n; j <= _do8; j++, j_++ ) { 
          temp = x[j_];
          k = kk + 1;
          if( noconj ) { 
            if( nounit ) 
              temp = temp*ap[kk - 1];
            for( i = j + 1, i_ = i - 1, _do9 = n; i <= _do9; i++, i_++ ) { 
              temp = temp + ap[k - 1]*x[i_];
              k = k + 1;
            }
          }
          else { 
            if( nounit ) 
              temp = temp*conj( ap[kk - 1] );
            for( i = j + 1, i_ = i - 1, _do10 = n; i <= _do10; i++, i_++ ) { 
              temp = temp + conj( ap[k - 1] )*x[i_];
              k = k + 1;
            }
          }
          x[j_] = temp;
          kk = kk + (n - j + 1);
        }
      }
      else { 
        jx = kx;
        for( j = 1, j_ = j - 1, _do11 = n; j <= _do11; j++, j_++ ) { 
          temp = x[jx - 1];
          ix = jx;
          if( noconj ) { 
            if( nounit ) 
              temp = temp*ap[kk - 1];
            for( k = kk + 1, k_ = k - 1, _do12 = kk + 
             n - j; k <= _do12; k++, k_++ ) { 
              ix = ix + incx;
              temp = temp + ap[k_]*x[ix - 1];
            }
          }
          else { 
            if( nounit ) 
              temp = temp*conj( ap[kk - 1] );
            for( k = kk + 1, k_ = k - 1, _do13 = kk + 
             n - j; k <= _do13; k++, k_++ ) { 
              ix = ix + incx;
              temp = temp + conj( ap[k_] )*x[ix - 1];
            }
          }
          x[jx - 1] = temp;
          jx = jx + incx;
          kk = kk + (n - j + 1);
        }
      }
    }
  }
  
  return;
  
  //     End of ZTPMV .
  
} // end of function 


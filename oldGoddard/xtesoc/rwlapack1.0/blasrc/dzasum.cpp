/*
 * Default C++ implementation of dzasum
 * For optimum performance, use a machine specific bla library
 *
 * $Id: dzasum.cpp,v 1.3 1993/03/19 16:26:58 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/17/93 at 14:40:06
 * FOR_C++ Options SET: alloc do=rt no=p pf=dbla,xbla s=dv str=l - prototypes
 *
 * $Log: dzasum.cpp,v $
 * Revision 1.3  1993/03/19  16:26:58  alv
 * added RWBLADECL linkage specification
 *
 * Revision 1.2  1993/03/05  23:07:38  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:04:48  alv
 * Initial revision
 *
 */

#include <rw/dcomplex.h>
#define RW_CPPBLAS 1
#include "rw/bla.h"
#include "rw/bla.h"
#include "rw/fortran.h" /* Fortran run time library */

RWBLADECL double /*FUNCTION*/ dzasum(const long &n, DComplex zx[], const long &incx)
{
  long _do0, _do1, i, i_, ix;
  double dzasum_v, stemp;

  
  //     takes the sum of the absolute values.
  //     jack dongarra, 3/11/78.
  //     modified to correct problem with negative increment, 8/21/90.
  
  
  dzasum_v = 0.0e0;
  stemp = 0.0e0;
  if( n <= 0 ) 
    return( dzasum_v );
  if( incx == 1 ) 
    goto L_20;
  
  //        code for increment not equal to 1
  
  ix = 1;
  if( incx < 0 ) 
    ix = (-n + 1)*incx + 1;
  for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
    stemp = stemp + dcabs1( zx[ix - 1] );
    ix = ix + incx;
  }
  dzasum_v = stemp;
  return( dzasum_v );
  
  //        code for increment equal to 1
  
L_20:
  for( i = 1, i_ = i - 1, _do1 = n; i <= _do1; i++, i_++ ) { 
    stemp = stemp + dcabs1( zx[i_] );
  }
  dzasum_v = stemp;
  return( dzasum_v );
} // end of function 


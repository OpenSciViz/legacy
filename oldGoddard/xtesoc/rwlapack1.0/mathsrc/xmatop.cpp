> #
> # $Header: /users/rcs/mathsrc/xmatop.cpp,v 1.10 1993/09/01 16:27:57 alv Exp $
> #
> # $Log: xmatop.cpp,v $
> # Revision 1.10  1993/09/01  16:27:57  alv
> # now uses rwUninitialized form for simple constructors
> #
> # Revision 1.9  1993/07/21  22:39:04  alv
> # fixed bug where op+= was called for all assignment ops when
> # blocks were the same
> #
> # Revision 1.8  1993/03/22  15:51:31  alv
> # changed PVCS Workfile keyword to RCS ID keyword
> #
> # Revision 1.7  1993/03/12  21:09:58  alv
> # NO_INLINED_TEMP_DESTRUCTORS -> RW_NO_INLINED_TEMP_DESTRUCTORS
> #
> # Revision 1.6  1993/03/12  20:01:12  alv
> # removed refs to BCC_STRUCT_POINTER_BUG
> #
> # Revision 1.5  1993/01/29  18:52:53  alv
> # added defn for - ops for UChar
> #
> # Revision 1.4  1993/01/29  18:25:08  alv
> # fixed UChar op- ops
> #
> # Revision 1.3  1993/01/29  18:07:53  alv
> # added defns for UChar's op- ops
> #
> # Revision 1.2  1993/01/28  16:32:58  alv
> # UChar types no longer calculate v-s via v+(-s)
> #
> # Revision 1.1  1993/01/22  23:51:08  alv
> # Initial revision
> #
> #
> define <ClassType>=GenMat
> include macros
/*
 * Definitions for various arithmetic operations for <C>
 *
 * Generated from template $Id: xmatop.cpp,v 1.10 1993/09/01 16:27:57 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

> if <t>==double || <t>==int || <R/C>==Complex
/*
 * If a promotable scalar (such as a float) is passed by value, then
 * it will get promoted to, say, a double.  If the address of this is
 * then taken, we have a pointer to a double.  Unfortunately, if
 * this address is then passed to a routine that expects a pointer
 * to a float, cfront thinks it has done its job and quietly passes
 * the address of this promoted variable.
 *
 * This problem occurs when K&R compilers are used as a backend to
 * cfront.  The workaround is to create a temporary of known type
 * on the stack.  This is the "scalar2" variable found throughout
 * this file.
 */

> endif
#include "rw/<a>genmat.h"
> # Bla, bla, bla... :-)
#include "rw/rw<bla>bla.h"	

RCSID("$Header: /users/rcs/mathsrc/xmatop.cpp,v 1.10 1993/09/01 16:27:57 alv Exp $");

/************************************************
 *                                              *
 *              UNARY OPERATORS                 *
 *                                              *
 ************************************************/

> if <T>!=UChar
<C> operator-(const <C>& s)   // Unary minus
{
  <C> temp(s.rows(),s.cols(),rwUninitialized);
  <t>* dp = temp.data();
  for(MatrixLooper l(s.rows(),s.cols(),s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    int n = l.length;
    <t>* sp = (<t>*)s.data()+l.start;
    while(n--) { *dp++ = -(*sp); sp+=l.stride; }
  }
  return temp;
}
> endif

> if <R/C>!=Complex
// Unary prefix increment on a <C>; (i.e. ++a)
<C>& <C>::operator++()
{
  for(MatrixLooper l(rows(),cols(),rowStride(),colStride()); l; ++l) {
    int n = l.length;
    <t>* sp = data()+l.start;
    while(n--) { ++(*sp); sp+=l.stride; }
  }
  return *this;
}

// Unary prefix decrement on a <C> (i.e., --a)
<C>&
<C>::operator--()
{
  for(MatrixLooper l(rows(),cols(),rowStride(),colStride()); l; ++l) {
    int n = l.length;
    <t>* sp = data()+l.start;
    while(n--) { --(*sp); sp+=l.stride; }
  }
  return *this;
}
> endif

/************************************************
 *                                              *
 *              BINARY OPERATORS                *
 *               vector - vector                *
 *                                              *
 ************************************************/

<C> operator+(const <C>& u, const <C>& v)
{
  unsigned m = v.rows();
  unsigned n = v.cols();
  u.lengthCheck(m,n);
  <C> temp(m,n,rwUninitialized);
  <t>* dp = temp.data();
  for(DoubleMatrixLooper l(m,n,u.rowStride(),u.colStride(),v.rowStride(),v.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rw<bla>_plvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

<C> operator-(const <C>& u, const <C>& v)
{
  unsigned m = v.rows();
  unsigned n = v.cols();
  u.lengthCheck(m,n);
  <C> temp(m,n,rwUninitialized);
  <t>* dp = temp.data();
  for(DoubleMatrixLooper l(m,n,u.rowStride(),u.colStride(),v.rowStride(),v.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rw<bla>_mivv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

<C> operator*(const <C>& u, const <C>& v)
{
  unsigned m = v.rows();
  unsigned n = v.cols();
  u.lengthCheck(m,n);
  <C> temp(m,n,rwUninitialized);
  <t>* dp = temp.data();
  for(DoubleMatrixLooper l(m,n,u.rowStride(),u.colStride(),v.rowStride(),v.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rw<bla>_muvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

<C> operator/(const <C>& u, const <C>& v)
{
  unsigned m = v.rows();
  unsigned n = v.cols();
  u.lengthCheck(m,n);
  <C> temp(m,n,rwUninitialized);
  <t>* dp = temp.data();
  for(DoubleMatrixLooper l(m,n,u.rowStride(),u.colStride(),v.rowStride(),v.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rw<bla>_dvvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

/************************************************
 *                                              *
 *              BINARY OPERATORS                *
 *               vector - scalar                *
 *                                              *
 ************************************************/

<C> operator+(const <C>& s, <t> scalar)
{
  unsigned m = s.rows();
  unsigned n = s.cols();
  <C> temp(m,n,rwUninitialized);
  <t>* dp = temp.data();
  for(MatrixLooper l(m,n,s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
>  if <t>==double || <t>==int || <R/C>==Complex
    rw<bla>_plvs(l.length, dp, s.data()+l.start, l.stride, &scalar);
>  else
> #   Promotable types need a temporary for K&R compilers:
    <t> scalar2 = scalar;
    rw<bla>_plvs(l.length, dp, s.data()+l.start, l.stride, &scalar2);
>  endif
    dp += l.length;
  }
  return temp;
}
> if <T>==UChar
/*
 * Normally, V-s is implemented as V+(-s), but this doesn't work
 * for unsigned types, so we use this workaround.
 */
<C> operator-(const <C>& v, <t> s)
{
  <C> result = v.copy();
  result -= s;
  return result;
}
> endif

<C> operator-(<t> scalar, const <C>& s)
{
  unsigned m = s.rows();
  unsigned n = s.cols();
  <C> temp(m,n,rwUninitialized);
  <t>* dp = temp.data();
  for(MatrixLooper l(m,n,s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
>  if <t>==double || <t>==int || <R/C>==Complex
    rw<bla>_misv(l.length, dp, &scalar, s.data()+l.start, l.stride);
>  else
> #   Promotable types need a temporary for K&R compilers:
    <t> scalar2 = scalar;
    rw<bla>_misv(l.length, dp, &scalar2, s.data()+l.start, l.stride);
>  endif
    dp += l.length;
  }
  return temp;
}

<C> operator*(const <C>& s, <t> scalar)
{
  unsigned m = s.rows();
  unsigned n = s.cols();
  <C> temp(m,n,rwUninitialized);
  <t>* dp = temp.data();
  for(MatrixLooper l(m,n,s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
>  if <t>==double || <t>==int || <R/C>==Complex
    rw<bla>_muvs(l.length, dp, s.data()+l.start, l.stride, &scalar);
>  else
> #   Promotable types need a temporary for K&R compilers:
    <t> scalar2 = scalar;
    rw<bla>_muvs(l.length, dp, s.data()+l.start, l.stride, &scalar2);
>  endif
    dp += l.length;
  }
  return temp;
}

<C> operator/(<t> scalar, const <C>& s)
{
  unsigned m = s.rows();
  unsigned n = s.cols();
  <C> temp(m,n,rwUninitialized);
  <t>* dp = temp.data();
  for(MatrixLooper l(m,n,s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
>  if <t>==double || <t>==int || <R/C>==Complex
    rw<bla>_dvsv(l.length, dp, &scalar, s.data()+l.start, l.stride);
>  else
> #   Promotable types need a temporary for K&R compilers:
    <t> scalar2 = scalar;
    rw<bla>_dvsv(l.length, dp, &scalar2, s.data()+l.start, l.stride);
>  endif
    dp += l.length;
  }
  return temp;
}

> #
> # Binary divide by a scalar defined in terms of V * (1/scalar) for
> # types DoubleGenMat and FloatGenMat
> #
> if <T>!=Double && <T>!=Float
<C> operator/(const <C>& s, <t> scalar)
{
  unsigned m = s.rows();
  unsigned n = s.cols();
  <C> temp(m,n,rwUninitialized);
  <t>* dp = temp.data();
  for(MatrixLooper l(m,n,s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
>  if <t>==double || <t>==int || <R/C>==Complex
    rw<bla>_dvvs(l.length, dp, s.data()+l.start, l.stride, &scalar);
>  else
> #   Promotable types need a temporary for K&R compilers:
    <t> scalar2 = scalar;
    rw<bla>_dvvs(l.length, dp, s.data()+l.start, l.stride, &scalar2);
>  endif
    dp += l.length;
  }
  return temp;
}
> endif

/*
 * Out-of-line versions for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
<C>     operator*(<t> s, const <C>& V)  {return V*s;}
<C>     operator+(<t> s, const <C>& V)  {return V+s;}
> if <T>!=UChar
<C>     operator-(const <C>& V, <t> s)  {return V+(-s);}
> endif
> if <T>==Double || <T>==Float
<C>     operator/(const <C>& V, <t> s)  {return V*(1/s);}
>endif
#endif

/************************************************
 *                                              *
 *      ARITHMETIC ASSIGNMENT OPERATORS         *
 *        with other vectors                    *
 *                                              *
 ************************************************/

<C>& <C>::operator+=(const <C>& u)
{
  if (sameDataBlock(u)) { return operator+=(u.copy()); }  // Avoid aliasing
  u.lengthCheck(rows(),cols());
  for(DoubleMatrixLooper l(rows(),cols(),rowStride(),colStride(),u.rowStride(),u.colStride()); l; ++l) {
    rw<bla>_aplvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

<C>& <C>::operator-=(const <C>& u)
{
  if (sameDataBlock(u)) { return operator-=(u.copy()); }  // Avoid aliasing
  u.lengthCheck(rows(),cols());
  for(DoubleMatrixLooper l(rows(),cols(),rowStride(),colStride(),u.rowStride(),u.colStride()); l; ++l) {
    rw<bla>_amivv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

<C>& <C>::operator*=(const <C>& u)
{
  if (sameDataBlock(u)) { return operator*=(u.copy()); }  // Avoid aliasing
  u.lengthCheck(rows(),cols());
  for(DoubleMatrixLooper l(rows(),cols(),rowStride(),colStride(),u.rowStride(),u.colStride()); l; ++l) {
    rw<bla>_amuvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

<C>& <C>::operator/=(const <C>& u)
{
  if (sameDataBlock(u)) { return operator/=(u.copy()); }  // Avoid aliasing
  u.lengthCheck(rows(),cols());
  for(DoubleMatrixLooper l(rows(),cols(),rowStride(),colStride(),u.rowStride(),u.colStride()); l; ++l) {
    rw<bla>_advvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

/************************************************
 *                                              *
 *      ARITHMETIC ASSIGNMENT OPERATORS         *
 *                with a scalar                 *
 *                                              *
 ************************************************/

<C>& <C>::operator+=(<t> scalar)
{
  for(MatrixLooper l(rows(),cols(),rowStride(),colStride()); l; ++l) {
>  if <t>==double || <t>==int || <R/C>==Complex
    rw<bla>_aplvs(l.length, data()+l.start, l.stride, &scalar);
>  else
> #   Promotable types need a temporary for K&R compilers:
    <t> scalar2 = scalar;
    rw<bla>_aplvs(l.length, data()+l.start, l.stride, &scalar2);
>  endif
  }
  return *this;
}
> if <T>==UChar
/*
 * Most types implement V-=s using V+=(-s), but this doesn't
 * work for unsigned types, so we need an explicit function.
 * Since there is no rwbla, we'll just use a loop.
 */
<C>&
<C>::operator-=(<t> s)
{
  for(MatrixLooper l(rows(),cols(),rowStride(),colStride()); l; ++l) {
    int n = l.length;
    <t>* sp = data()+l.start;
    while(n--) { *sp -= s; sp+=l.stride; }
  }
  return *this;
}
> endif

<C>& <C>::operator*=(<t> scalar)
{
  for(MatrixLooper l(rows(),cols(),rowStride(),colStride()); l; ++l) {
>  if <t>==double || <t>==int || <R/C>==Complex
    rw<bla>_amuvs(l.length, data()+l.start, l.stride, &scalar);
>  else
> #   Promotable types need a temporary for K&R compilers:
    <t> scalar2 = scalar;
    rw<bla>_amuvs(l.length, data()+l.start, l.stride, &scalar2);
>  endif
  }
  return *this;
}

> if <t>!=double && <t>!=float
<C>& <C>::operator/=(<t> scalar)
{
  for(MatrixLooper l(rows(),cols(),rowStride(),colStride()); l; ++l) {
>  if <t>==double || <t>==int || <R/C>==Complex
    rw<bla>_advvs(l.length, data()+l.start, l.stride, &scalar);
>  else
> #   Promotable types need a temporary for K&R compilers:
    <t> scalar2 = scalar;
    rw<bla>_advvs(l.length, data()+l.start, l.stride, &scalar2);
>  endif
  }
  return *this;
}
> endif

/************************************************
 *                                              *
 *              LOGICAL OPERATORS               *
 *                                              *
 ************************************************/

RWBoolean <C>::operator==(const <C>& u) const
{
  if(rows()!=u.rows() || cols()!=u.cols()) return FALSE;  // They can't be equal if they don't have the same length.
  for(DoubleMatrixLooper l(rows(),cols(),rowStride(),colStride(),u.rowStride(),u.colStride()); l; ++l) {
    if (!rw<bla>same(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2)) {
      return FALSE;
    }
  }
  return TRUE;
}

RWBoolean <C>::operator!=(const <C>& u) const
{
  return !(*this == u);
}

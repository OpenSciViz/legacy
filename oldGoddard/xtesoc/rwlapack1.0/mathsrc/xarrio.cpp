> #
> # $Header: /users/rcs/mathsrc/xarrio.cpp,v 1.7 1993/09/17 18:42:21 alv Exp $
> #
> # $Log: xarrio.cpp,v $
> # Revision 1.7  1993/09/17  18:42:21  alv
> # (0) -> ((int)0) for benefit of DEC compiler
> #
> # Revision 1.6  1993/09/17  18:28:12  alv
> # fixed bug in stream I/O output
> #
> # Revision 1.5  1993/09/16  16:29:28  alv
> # MultiIndex -> RWMultiIndex
> #
> # Revision 1.4  1993/08/10  21:22:18  alv
> # now uses Tools v6 compatible error handling
> #
> # Revision 1.3  1993/03/22  15:51:31  alv
> # changed PVCS Workfile keyword to RCS ID keyword
> #
> # Revision 1.2  1993/01/27  20:20:01  alv
> # eliminated needless className defn to avoid warnings on sun
> #
> # Revision 1.1  1993/01/22  23:51:06  alv
> # Initial revision
> #
> # 
> define <ClassType>=Array
> include macros
/*
 * Definitions for <C> I/O
 *
 * Generated from template $Id: xarrio.cpp,v 1.7 1993/09/17 18:42:21 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/<a>arr.h"
#include "rw/rwfile.h"
#include "rw/vstream.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"
STARTWRAP
#include <stdio.h>
#include <ctype.h>
ENDWRAP

> if <T>==SChar || <T>==UChar
// read and write chars as integers:
#  define CAST Int
#  define CTYPE char
> else
#  define CAST <T>
#  define CTYPE <T>
> endif

RCSID("$Header: /users/rcs/mathsrc/xarrio.cpp,v 1.7 1993/09/17 18:42:21 alv Exp $");

const unsigned short versionID = 2;             // Formatting version number

// Print out the array in row major order
void <C>::printOn(ostream& s) const 
{
  for(int i=0; i<dimension(); ++i) {    // Print dimensions: eg, 5x3x6
    s << length(i);
    if (i<dimension()-1) { s << "x"; }
  }
  s << " [\n";
  if (prod(length())>0) {
    // Loop through the array printing a plane at a time.  The code
    // is much simpler if we don't have to worry about zero, one or
    // two dimension arrays.  To accomodate this, the looping index is
    // at least 3-D, but the index used to access the array in the 0,1,2D
    // cases is just a slice of the last entries of the index.
    int p = dimension();  // Save some typing
    IntVec index(((p>3)?p:3),0);
    IntVec accessIndex = index.slice(index.length()-p,p);
    RWBoolean isBeginningOfLine = TRUE;
    while(index((int)0)==0 || (p>2 && index((int)0)<length((int)0)) ) { // Loop over planes
      while(index(index.length()-2)==0 || (p>1 && accessIndex(p-2)<length(p-2)) ) { // Loop over lines
        while(index(index.length()-1)==0 || (p>0 && accessIndex(p-1)<length(p-1)) ) {
          if (!isBeginningOfLine) { s << " "; }
          CAST el = (*this)(accessIndex);
          s << el;
          isBeginningOfLine = FALSE;
          ++(index(index.length()-1));   // Incremement last component of index
        }
        isBeginningOfLine = TRUE;
        s << "\n";
        index(index.length()-1) = 0;
        ++(index(index.length()-2));
      }
      index(index.length()-2) = 0;       // Advance to next plane
      if (p<3) {
        ++(index((int)0));
      } else {      // The array is at least 3D
        int r=p-3;
        int numBlankLinesToPrint = 1;    // Only print these if not done
        while(++(index(r))>=npts(r) && --r>=0) {
          index(r+1)=0;
          numBlankLinesToPrint++;
        }
        if(r>=0) {
          while (--numBlankLinesToPrint >= 0) { s << "\n"; }
        }
      }
    }
  }
  s << "]";
}
        
void <C>::scanFrom(istream& s)
{
  CAST item;
  char c;

  do { s.get(c); } while(s.good() && c!='[' && !isdigit(c));
  s.putback(c);

  IntVec n;                     // Read in array dimensions
  if (c!='[') {   // if c is a left bracket, this is a 0-D array
    do {
      n.resize(n.length()+1);
      s >> n(n.length()-1);
      do { s.get(c); } while (isspace(c) && s.good()); // Ignore whitespace
    } while (s.good() && c=='x');
  }
  if (c!='[') { s.putback(c); }
  
  reshape(n);
  // Loop through all the entries and read them in.
  for(RWMultiIndex i(n); s.good() && i; ++i) {
    s >> item;
    (*this)(i) = item;
  }
  
  if (s.good()) {
    do { s.get(c); } while(s.good() && isspace(c));  // Eat the trailing ]
  }
  if (c!=']') { s.putback(c); }

  return;
}

ostream& operator<<(ostream& s, const <C>& v)
{
  v.printOn(s); return s;
}

istream& operator>>(istream& s, <C>& v)
{
  v.scanFrom(s); return s;
}

void <C>::saveOn(RWFile& file) const 
{
  file.Write(versionID);
  npts.saveOn(file);
  for(ArrayLooper l(npts,step); l; ++l) {

> if <R/C>==Complex
#  if COMPLEX_PACKS && !defined(IMAG_LEADS)

    if( l.stride == 1 ){
      // This is the normal case and things can be done very efficiently.
      // Write the N point complex vector as a 2N point real vector:
      const <P>* dataStart = (const <P>*) (data()+l.start);
      file.Write(dataStart, 2*l.length);
    }
    else {
      for(register int n = 0; n < l.length; n++) {
        <t> x = data()[l.start+n*l.stride]; 
        file.Write( real(x) );
        file.Write( imag(x) );
      }
    }

#  else

    /*
     * Complex vector, but it is not packed.
     * Have to write it out, element-by-element
     */

    for(register int n = 0; n < l.length; n++) {
        <t> x = data()[l.start+n*l.stride]; 
        file.Write( real(x) );
        file.Write( imag(x) );
    }

#  endif

> else
> # Not a complex vector

    if( l.stride == 1 ){
      const CTYPE* dataStart = (const CTYPE*)(data()+l.start);
      file.Write(dataStart, l.length);
    }
    else {
      for(register int n = 0; n < l.length; n++){
        CTYPE item = data()[l.start+n*l.stride];
        file.Write(item);
      }
    }

> endif
> # end check of complex
  }
}

void <C>::saveOn(RWvostream& s) const 
{
  s << versionID;               // Save the version number
  npts.saveOn(s);               // Save the vector length

  for(ArrayLooper l(npts,step); l; ++l) {

> if <R/C>==Complex
#  if COMPLEX_PACKS && !defined(IMAG_LEADS)

    if( l.stride == 1 ){
      // This is the normal case and things can be done very efficiently.
      // Write the N point complex vector as a 2N point real vector:
      const <P>* dataStart = (const <P>*) (data()+l.start);
      s.put(dataStart, 2*l.length);
    }
    else {
      for(register int n = 0; n < l.length; n++) {
        <t> x = data()[l.start+n*l.stride];
        s << real(x);
        s << imag(x);
      }
    }

#  else 

    /*
     * Complex vector, but either it is not packed, or real does not lead.
     * Have to write it out, element-by-element
     */
    
    for(register int n = 0; n < l.length; n++) {
      s << real( data()[l.start+n*l.stride] );
      s << imag( data()[l.start+n*l.stride] );
    }

#  endif

> else
> # Not complex

    if( l.stride == 1 ){
      const CTYPE* dataStart = (const CTYPE*)(data()+l.start);
      s.put(dataStart, l.length);
    }
    else {
      for(register int n = 0; n < l.length; n++){
        CTYPE item = data()[l.start+n*l.stride];
        s << item;
      }
    }
> endif
  }
}

void <C>::restoreFrom(RWFile& file)
{
  // Get and check the version number
  unsigned short ID;
  file.Read(ID);

  if( ID!=versionID && ID!=340 ){
    versionErr((int)versionID, (int)ID);
  }

  // Get the length and resize to it:
  IntVec lenVec;
  lenVec.restoreFrom(file);
  resize(lenVec);
  unsigned len = prod(lenVec);

> if <R/C>==Complex
> # Complex vector
#  if COMPLEX_PACKS

  // This is the normal case.  We can read the N point complex
  // vector as a 2N real vector.
  <P>* dataStart = (<P>*) data();
  file.Read(dataStart, 2*len);

#  else

  // Complex doesn't pack.  Have to read it in element-by-element
  <P> tmpReal, tmpImag;
  for(register int n=0; n<len; n++){
    file.Read(tmpReal);
    file.Read(tmpImag);
    (data())[n] = <T>(tmpReal, tmpImag);
  }

#  endif

> else
> # Not complex

  // Scoop up the whole vector in one read:
  CTYPE* dataStart = (CTYPE*) data();
  file.Read(dataStart, len);
> endif
}

void <C>::restoreFrom(RWvistream& s)
{
  // Get and check the version number
  unsigned short ID;
  s >> ID;

  if( ID!=versionID && ID!=340 ){
    versionErr((int)versionID, (int)ID);
  }

  // Get the length and resize to it:
  IntVec lenVec;
  lenVec.restoreFrom(s);
  resize(lenVec);
  unsigned len = prod(lenVec);

> if <R/C>==Complex
#  if COMPLEX_PACKS

  // This is the normal case.  We can read the N point complex
  // vector as a 2N real vector.
  <P>* dataStart = (<P>*) data();
  s.get(dataStart, 2*len);

#else

  // Complex doesn't pack.  Have to read it in element-by-element
  <P> tmpReal, tmpImag;
  for(register int n=0; n<len; n++){
    s >> tmpReal;
    s >> tmpImag;
    (data())[n] = <T>(tmpReal, tmpImag);
  }

#  endif

> else
> # Not complex

  // Scoop up the whole vector in one read:
  CTYPE* dataStart = (CTYPE*) data();
  s.get(dataStart, len);
> endif
}

unsigned <C>::binaryStoreSize() const
{
  // Total storage requirements = Number of elements times their size, 
  // plus space for the vector length and ID number:
  return prod(length())*sizeof(<T>) + npts.binaryStoreSize() + sizeof(versionID);
}

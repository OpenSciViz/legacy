/*
 * Math definitions for DoubleGenMat
 *
 * Generated from template $Id: xmatmath.cpp,v 1.8 1993/09/16 17:16:56 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/dgenmat.h"
#include "rw/dvec.h"
#include "rw/rwdbla.h"


#define REGISTER register

RCSID("$Header: /users/rcs/mathsrc/xmatmath.cpp,v 1.8 1993/09/16 17:16:56 alv Exp $");




DoubleGenMat DoubleGenMat::apply(mathFunTy f) const
// Apply function returning double to each element of vector slice
{
  DoubleGenMat temp(nrows,ncols,rwUninitialized);
  register double* dp = temp.data();
  for(MatrixLooper l(nrows,ncols,rowstep,colstep,RWDataView::COLUMN_MAJOR); l; ++l) {
    register const double* sp = data()+l.start;
    register i = l.length;
    register j = l.stride;
    while(i--) {
// Turbo C++ bug requires temporary.
#if defined (__TURBOC__)
      double dummy = (*f)(*sp);
      *dp++ = dummy;
#else
      *dp++ = (*f)(*sp);  
#endif
      sp += j; 
    }
  }
  return temp;
}

#if defined(RW_NATIVE_EXTENDED)
 DoubleGenMat DoubleGenMat::apply(XmathFunTy f) const
// Apply function for compilers using extended types
{
  DoubleGenMat temp(nrows,ncols);
  register double* dp = temp.data();
  for(MatrixLooper l(nrows,ncols,rowstep,colstep,RWDataView::COLUMN_MAJOR); l; ++l) {
    register const double* sp = data()+l.start;
    register i = l.length;
    register j = l.stride;
    while(i--) {
      *dp++ = (*f)(*sp);
      sp += j;
    }
  }
  return temp;
}
#endif


DoubleGenMat atan2(const DoubleGenMat& u,const DoubleGenMat& v)
     // Arctangent of u/v.
{
  u.lengthCheck(v.rows(),v.cols());
  DoubleGenMat temp(v.rows(),v.cols(),rwUninitialized);
  register double* dp = temp.data();
  for(DoubleMatrixLooper l(v.rows(),v.cols(),u.rowStride(),u.colStride(),v.rowStride(),v.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    register const double* up = u.data()+l.start1;
    register const double* vp = v.data()+l.start2;
    register i = l.length;
    register uj = l.stride1;
    register vj = l.stride2;
    while (i--) { *dp++ = atan2(*up,*vp);  up += uj;  vp += vj; }
  }
  return temp;
}

double dot(const DoubleGenMat& u, const DoubleGenMat& v)
      // dot product: sum_ij (u_ij * v_ij)
{
  u.lengthCheck(v.rows(),v.cols());
  double result = 0;
  for(DoubleMatrixLooper l(v.rows(),v.cols(),u.rowStride(),u.colStride(),v.rowStride(),v.colStride()); l; ++l) {
    double partialResult;
    rwddot(l.length,u.data()+l.start1,l.stride1,v.data()+l.start2,l.stride2,&partialResult);
    result += partialResult;
  }
  return result;
}

void maxIndex(const DoubleGenMat& s, int *maxi, int *maxj)
// Index of maximum element.
{
  int len = s.rows()*s.cols();
  s.numPointsCheck("max needs at least",1);
  if (s.colStride()==s.rowStride()*s.rows()) {    // Column major order
    int maxindex = rwdmax(len,s.data(),s.rowStride());
    *maxj = maxindex/s.rows();
    *maxi = maxindex%s.rows();
    return;
  }
  if (s.rowStride()==s.colStride()*s.cols()) {    // Row major order
    int maxindex = rwdmax(len,s.data(),s.colStride());
    *maxj = maxindex/s.cols();
    *maxi = maxindex%s.cols();
    return;
  }
  *maxi = *maxj = 0;			    // Matrix is not contiguous
  double themax = s(0,0);
  if (s.colStride()>s.rowStride()) {
    for( int j=0; j<s.cols(); j++ ) {  // Do a column at a time
      int i = rwdmax(len,s.data()+j*s.colStride(),s.rowStride());
      if (s(i,j)>themax) { themax=s(i,j); *maxi=i; *maxj=j; }
    }
  } else {
    for( int i=0; i<s.rows(); i++ ) {  // Do a row at a time
      int j = rwdmax(len,s.data()+i*s.rowStride(),s.colStride());
      if (s(i,j)>themax) { themax=s(i,j); *maxi=i; *maxj=j; }
    }
  }
}

double maxValue(const DoubleGenMat& s)
{
  int i,j;
  maxIndex(s,&i,&j);
  return s(i,j);
}
    
void minIndex(const DoubleGenMat& s, int *mini, int *minj)
// Index of minimum element.
{
  int len = s.rows()*s.cols();
  s.numPointsCheck("min needs at least",1);
  if (s.colStride()==s.rowStride()*s.rows()) {    // Column major order
    int minindex = rwdmin(len,s.data(),s.rowStride());
    *minj = minindex/s.rows();
    *mini = minindex%s.rows();
    return;
  }
  if (s.rowStride()==s.colStride()*s.cols()) {    // Row major order
    int minindex = rwdmin(len,s.data(),s.colStride());
    *minj = minindex/s.cols();
    *mini = minindex%s.cols();
    return;
  }
  *mini = *minj = 0;			    // Matrix is not contiguous
  double themin = s(0,0);
  if (s.colStride()>s.rowStride()) {
    for( int j=0; j<s.cols(); j++ ) {  // Do a column at a time
      int i = rwdmin(len,s.data()+j*s.colStride(),s.rowStride());
      if (s(i,j)>themin) { themin=s(i,j); *mini=i; *minj=j; }
    }
  } else {
    for( int i=0; i<s.rows(); i++ ) {  // Do a row at a time
      int j = rwdmin(len,s.data()+i*s.rowStride(),s.colStride());
      if (s(i,j)>themin) { themin=s(i,j); *mini=i; *minj=j; }
    }
  }
}
    
double minValue(const DoubleGenMat& s)
{
  int i,j;
  minIndex(s,&i,&j);
  return s(i,j);
}


double prod(const DoubleGenMat& s)
{
  REGISTER double t = 1;
  for(MatrixLooper l(s.rows(),s.cols(),s.rowStride(),s.colStride()); l; ++l) {
    register double* sp = (double*)(s.data()+l.start);
    register int i = l.length;
    register j = l.stride;
    while (i--) { t *= *sp;  sp += j; }
  }
  return (double)t;
}

DoubleGenMat pow(const DoubleGenMat& u, const DoubleGenMat& v)
// u to the v power.
{
  u.lengthCheck(v.rows(),v.cols());
  DoubleGenMat temp(v.rows(),v.cols(),rwUninitialized);
  register double* dp = temp.data();
  for(DoubleMatrixLooper l(v.rows(),v.cols(),u.rowStride(),u.colStride(),v.rowStride(),v.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    register i = l.length;
    register double* up = (double*)(u.data()+l.start1);
    register double* vp = (double*)(v.data()+l.start2);
    register uj = l.stride1;
    register vj = l.stride2;
    while (i--) { *dp++ = pow(*up,*vp); up += uj; vp += vj; }
  }
  return temp;
}

double sum(const DoubleGenMat& s)
{
  REGISTER double t = 0;
  for(MatrixLooper l(s.rows(),s.cols(),s.rowStride(),s.colStride()); l; ++l) {
    register double* sp = (double*)(s.data()+l.start);
    register int i = l.length;
    register j = l.stride;
    while (i--) { t += *sp;  sp += j; }
  }
  return (double)t;
}

double mean(const DoubleGenMat& s)
{
  double theSum = sum(s);
  double numPoints = s.rows()*s.cols();
  if (theSum==0) return 0;   // Covers the case of zero points
  return theSum/numPoints;
}

double variance(const DoubleGenMat& s)
// Variance of a DoubleGenMat
{
  s.numPointsCheck("Variance needs minimum of",2);
  register double sum2 = 0;	// Accumulate sums in double precision
  REGISTER double avg = mean(s);
  for(MatrixLooper l(s.rows(),s.cols(),s.rowStride(),s.colStride()); l; ++l) {
    register double* sp = (double*)(s.data()+l.start);
    register int i = l.length;
    register j = l.stride;
    while (i--) {
      REGISTER double temp = *sp - avg;
      sum2 += temp * temp;
      sp += j;
    }
  }
  // Use the biased estimate (easier to work with):
  return (double)(sum2/(s.rows()*s.cols()));
}

DoubleGenMat transpose(const DoubleGenMat& A)
{
  return A.slice(0,0, A.cols(),A.rows(), 0,1, 1,0);
}

DoubleVec product(const DoubleGenMat& A, const DoubleVec& x)
{
  int m = A.rows();
  int n = A.cols();
  x.lengthCheck(n);
  DoubleVec y(m,rwUninitialized);
  const double* Aptr = A.data();
  for(int i=0; i<m; i++) {
    rwddot(n,x.data(),x.stride(),Aptr,A.colStride(),&(y(i)));
    Aptr += A.rowStride();
  }
  return y;
}

DoubleGenMat transposeProduct(const DoubleGenMat& A, const DoubleGenMat& B)
{
  return product( transpose(A), B );
}


DoubleVec product(const DoubleVec& x, const DoubleGenMat& A)
{
  return product( transpose(A),x );
}

DoubleGenMat product(const DoubleGenMat& A, const DoubleGenMat& B)
{
  int m = A.rows();
  int n = B.cols();
  int r = A.cols();  // == B.rows() if A,B conform
  B.rowCheck(r);
  DoubleGenMat AB(m,n,rwUninitialized);
  DoubleVec Arow(r,rwUninitialized);
  for(int i=0; i<m; i++) {
    Arow = A.row(i);  // Compact this row to minimize page faults
    const double* Bptr = B.data();
    for(int j=0; j<n; j++) {
      rwddot(r,Arow.data(),1,Bptr,B.rowStride(),&(AB(i,j)));
      Bptr += B.colStride();
    }
  }
  return AB;
}

/*
 * Various functions with simple definitions:
 */
DoubleGenMat	acos(const DoubleGenMat& V)	{ return V.apply(::acos); }
DoubleGenMat	asin(const DoubleGenMat& V)	{ return V.apply(::asin); }
DoubleGenMat	atan(const DoubleGenMat& V)	{ return V.apply(::atan); }
DoubleGenMat	ceil(const DoubleGenMat& V)	{ return V.apply(::ceil); }
DoubleGenMat	cos(const DoubleGenMat& V)		{ return V.apply(::cos);  }
DoubleGenMat	cosh(const DoubleGenMat& V)	{ return V.apply(::cosh); }
DoubleGenMat	exp(const DoubleGenMat& V)		{ return V.apply(::exp);  }
DoubleGenMat	floor(const DoubleGenMat& V)	{ return V.apply(::floor);}
DoubleGenMat	log(const DoubleGenMat& V)		{ return V.apply(::log);  }
DoubleGenMat	log10(const DoubleGenMat& V)	{ return V.apply(::log10);}
DoubleGenMat	sin(const DoubleGenMat& V)		{ return V.apply(::sin);  }
DoubleGenMat	sinh(const DoubleGenMat& V)	{ return V.apply(::sinh); }
DoubleGenMat	tan(const DoubleGenMat& V)		{ return V.apply(::tan);  }
DoubleGenMat	tanh(const DoubleGenMat& V)	{ return V.apply(::tanh); }

#ifdef __ZTC__
/*
 * Zortech requires its own special version of abs(const DoubleGenMat&)
 * because it uses an inlined version of fabs().  Hence, apply() 
 * cannot be used.
 */
DoubleGenMat
abs(const DoubleGenMat& s)
// Absolute value of a DoubleGenMat --- Zortech version
{
  DoubleGenMat temp(s.rows(),s.cols(),rwUninitialized);
  register double* dp = temp.data();
  for(MatrixLooper l(s.rows(),s.cols(),s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    register const double* sp = s.data()+l.start;
    register i = l.length;
    register j = l.stride;
    while (i--) { *dp++ = fabs(*sp);  sp += j; }
  }
  return temp;
}
#else   /* Not Zortech */
DoubleGenMat	abs(const DoubleGenMat& V)		{ return V.apply(::fabs); }
#endif	
#ifdef __ZTC__
/*
 * Zortech requires its own special version of sqrt(const DoubleGenMat&) 
 * because it uses an inlined version of sqrt().  Hence, apply() 
 * cannot be used.
 */
DoubleGenMat
sqrt(const DoubleGenMat& s)
// Square root of a DoubleGenMat --- Zortech version
{
  DoubleGenMat temp(s.rows(),s.cols(),rwUninitialized);
  register double* dp = temp.data();
  for(MatrixLooper l(s.rows(),s.cols(),s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    register const double* sp = s.data()+l.start;
    register i = l.length;
    register j = l.stride;
    while (i--) { *dp++ = sqrt(*sp);  sp += j; }
  }
  return temp;
}
#else
DoubleGenMat	sqrt(const DoubleGenMat& V)	{ return V.apply(::sqrt); }
#endif

    /**** Norm functions ****/

Double l1Norm(const DoubleGenMat& x)
{
  DoubleVec colNorms(x.cols(),rwUninitialized);
  for(int j=x.cols(); j--;) {
    colNorms(j) = l1Norm(x(RWAll,j));
  }
  return linfNorm(colNorms);
}
	  
Double linfNorm(const DoubleGenMat& x)
{
  return l1Norm(transpose(x));
}

Double maxNorm(const DoubleGenMat& x)     // Not really a norm!
{
  DoubleVec colNorms(x.cols(),rwUninitialized);
  for(int j=x.cols(); j--;) {
    colNorms(j) = linfNorm(x(RWAll,j));
  }
  return linfNorm(colNorms);
}

Double frobNorm(const DoubleGenMat& x)
{
  DoubleVec colNorms(x.cols(),rwUninitialized);
  for(int j=x.cols(); j--;) {
    colNorms(j) = l2Norm(x(RWAll,j));
  }
  return l2Norm(colNorms);
}


/*
 * Definitions for various arithmetic operations for DComplexGenMat
 *
 * Generated from template $Id: xmatop.cpp,v 1.10 1993/09/01 16:27:57 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

/*
 * If a promotable scalar (such as a float) is passed by value, then
 * it will get promoted to, say, a double.  If the address of this is
 * then taken, we have a pointer to a double.  Unfortunately, if
 * this address is then passed to a routine that expects a pointer
 * to a float, cfront thinks it has done its job and quietly passes
 * the address of this promoted variable.
 *
 * This problem occurs when K&R compilers are used as a backend to
 * cfront.  The workaround is to create a temporary of known type
 * on the stack.  This is the "scalar2" variable found throughout
 * this file.
 */

#include "rw/cgenmat.h"
#include "rw/rwzbla.h"	

RCSID("$Header: /users/rcs/mathsrc/xmatop.cpp,v 1.10 1993/09/01 16:27:57 alv Exp $");

/************************************************
 *                                              *
 *              UNARY OPERATORS                 *
 *                                              *
 ************************************************/

DComplexGenMat operator-(const DComplexGenMat& s)   // Unary minus
{
  DComplexGenMat temp(s.rows(),s.cols(),rwUninitialized);
  DComplex* dp = temp.data();
  for(MatrixLooper l(s.rows(),s.cols(),s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    int n = l.length;
    DComplex* sp = (DComplex*)s.data()+l.start;
    while(n--) { *dp++ = -(*sp); sp+=l.stride; }
  }
  return temp;
}


/************************************************
 *                                              *
 *              BINARY OPERATORS                *
 *               vector - vector                *
 *                                              *
 ************************************************/

DComplexGenMat operator+(const DComplexGenMat& u, const DComplexGenMat& v)
{
  unsigned m = v.rows();
  unsigned n = v.cols();
  u.lengthCheck(m,n);
  DComplexGenMat temp(m,n,rwUninitialized);
  DComplex* dp = temp.data();
  for(DoubleMatrixLooper l(m,n,u.rowStride(),u.colStride(),v.rowStride(),v.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rwz_plvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

DComplexGenMat operator-(const DComplexGenMat& u, const DComplexGenMat& v)
{
  unsigned m = v.rows();
  unsigned n = v.cols();
  u.lengthCheck(m,n);
  DComplexGenMat temp(m,n,rwUninitialized);
  DComplex* dp = temp.data();
  for(DoubleMatrixLooper l(m,n,u.rowStride(),u.colStride(),v.rowStride(),v.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rwz_mivv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

DComplexGenMat operator*(const DComplexGenMat& u, const DComplexGenMat& v)
{
  unsigned m = v.rows();
  unsigned n = v.cols();
  u.lengthCheck(m,n);
  DComplexGenMat temp(m,n,rwUninitialized);
  DComplex* dp = temp.data();
  for(DoubleMatrixLooper l(m,n,u.rowStride(),u.colStride(),v.rowStride(),v.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rwz_muvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

DComplexGenMat operator/(const DComplexGenMat& u, const DComplexGenMat& v)
{
  unsigned m = v.rows();
  unsigned n = v.cols();
  u.lengthCheck(m,n);
  DComplexGenMat temp(m,n,rwUninitialized);
  DComplex* dp = temp.data();
  for(DoubleMatrixLooper l(m,n,u.rowStride(),u.colStride(),v.rowStride(),v.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rwz_dvvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

/************************************************
 *                                              *
 *              BINARY OPERATORS                *
 *               vector - scalar                *
 *                                              *
 ************************************************/

DComplexGenMat operator+(const DComplexGenMat& s, DComplex scalar)
{
  unsigned m = s.rows();
  unsigned n = s.cols();
  DComplexGenMat temp(m,n,rwUninitialized);
  DComplex* dp = temp.data();
  for(MatrixLooper l(m,n,s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rwz_plvs(l.length, dp, s.data()+l.start, l.stride, &scalar);
    dp += l.length;
  }
  return temp;
}

DComplexGenMat operator-(DComplex scalar, const DComplexGenMat& s)
{
  unsigned m = s.rows();
  unsigned n = s.cols();
  DComplexGenMat temp(m,n,rwUninitialized);
  DComplex* dp = temp.data();
  for(MatrixLooper l(m,n,s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rwz_misv(l.length, dp, &scalar, s.data()+l.start, l.stride);
    dp += l.length;
  }
  return temp;
}

DComplexGenMat operator*(const DComplexGenMat& s, DComplex scalar)
{
  unsigned m = s.rows();
  unsigned n = s.cols();
  DComplexGenMat temp(m,n,rwUninitialized);
  DComplex* dp = temp.data();
  for(MatrixLooper l(m,n,s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rwz_muvs(l.length, dp, s.data()+l.start, l.stride, &scalar);
    dp += l.length;
  }
  return temp;
}

DComplexGenMat operator/(DComplex scalar, const DComplexGenMat& s)
{
  unsigned m = s.rows();
  unsigned n = s.cols();
  DComplexGenMat temp(m,n,rwUninitialized);
  DComplex* dp = temp.data();
  for(MatrixLooper l(m,n,s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rwz_dvsv(l.length, dp, &scalar, s.data()+l.start, l.stride);
    dp += l.length;
  }
  return temp;
}

DComplexGenMat operator/(const DComplexGenMat& s, DComplex scalar)
{
  unsigned m = s.rows();
  unsigned n = s.cols();
  DComplexGenMat temp(m,n,rwUninitialized);
  DComplex* dp = temp.data();
  for(MatrixLooper l(m,n,s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rwz_dvvs(l.length, dp, s.data()+l.start, l.stride, &scalar);
    dp += l.length;
  }
  return temp;
}

/*
 * Out-of-line versions for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
DComplexGenMat     operator*(DComplex s, const DComplexGenMat& V)  {return V*s;}
DComplexGenMat     operator+(DComplex s, const DComplexGenMat& V)  {return V+s;}
DComplexGenMat     operator-(const DComplexGenMat& V, DComplex s)  {return V+(-s);}
#endif

/************************************************
 *                                              *
 *      ARITHMETIC ASSIGNMENT OPERATORS         *
 *        with other vectors                    *
 *                                              *
 ************************************************/

DComplexGenMat& DComplexGenMat::operator+=(const DComplexGenMat& u)
{
  if (sameDataBlock(u)) { return operator+=(u.copy()); }  // Avoid aliasing
  u.lengthCheck(rows(),cols());
  for(DoubleMatrixLooper l(rows(),cols(),rowStride(),colStride(),u.rowStride(),u.colStride()); l; ++l) {
    rwz_aplvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

DComplexGenMat& DComplexGenMat::operator-=(const DComplexGenMat& u)
{
  if (sameDataBlock(u)) { return operator-=(u.copy()); }  // Avoid aliasing
  u.lengthCheck(rows(),cols());
  for(DoubleMatrixLooper l(rows(),cols(),rowStride(),colStride(),u.rowStride(),u.colStride()); l; ++l) {
    rwz_amivv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

DComplexGenMat& DComplexGenMat::operator*=(const DComplexGenMat& u)
{
  if (sameDataBlock(u)) { return operator*=(u.copy()); }  // Avoid aliasing
  u.lengthCheck(rows(),cols());
  for(DoubleMatrixLooper l(rows(),cols(),rowStride(),colStride(),u.rowStride(),u.colStride()); l; ++l) {
    rwz_amuvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

DComplexGenMat& DComplexGenMat::operator/=(const DComplexGenMat& u)
{
  if (sameDataBlock(u)) { return operator/=(u.copy()); }  // Avoid aliasing
  u.lengthCheck(rows(),cols());
  for(DoubleMatrixLooper l(rows(),cols(),rowStride(),colStride(),u.rowStride(),u.colStride()); l; ++l) {
    rwz_advvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

/************************************************
 *                                              *
 *      ARITHMETIC ASSIGNMENT OPERATORS         *
 *                with a scalar                 *
 *                                              *
 ************************************************/

DComplexGenMat& DComplexGenMat::operator+=(DComplex scalar)
{
  for(MatrixLooper l(rows(),cols(),rowStride(),colStride()); l; ++l) {
    rwz_aplvs(l.length, data()+l.start, l.stride, &scalar);
  }
  return *this;
}

DComplexGenMat& DComplexGenMat::operator*=(DComplex scalar)
{
  for(MatrixLooper l(rows(),cols(),rowStride(),colStride()); l; ++l) {
    rwz_amuvs(l.length, data()+l.start, l.stride, &scalar);
  }
  return *this;
}

DComplexGenMat& DComplexGenMat::operator/=(DComplex scalar)
{
  for(MatrixLooper l(rows(),cols(),rowStride(),colStride()); l; ++l) {
    rwz_advvs(l.length, data()+l.start, l.stride, &scalar);
  }
  return *this;
}

/************************************************
 *                                              *
 *              LOGICAL OPERATORS               *
 *                                              *
 ************************************************/

RWBoolean DComplexGenMat::operator==(const DComplexGenMat& u) const
{
  if(rows()!=u.rows() || cols()!=u.cols()) return FALSE;  // They can't be equal if they don't have the same length.
  for(DoubleMatrixLooper l(rows(),cols(),rowStride(),colStride(),u.rowStride(),u.colStride()); l; ++l) {
    if (!rwzsame(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2)) {
      return FALSE;
    }
  }
  return TRUE;
}

RWBoolean DComplexGenMat::operator!=(const DComplexGenMat& u) const
{
  return !(*this == u);
}

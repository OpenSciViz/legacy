/*
 * Math definitions for FloatGenMat
 *
 * Generated from template $Id: xmatmath.cpp,v 1.8 1993/09/16 17:16:56 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/fgenmat.h"
#include "rw/fvec.h"
#include "rw/rwsbla.h"


#define REGISTER register

RCSID("$Header: /users/rcs/mathsrc/xmatmath.cpp,v 1.8 1993/09/16 17:16:56 alv Exp $");




FloatGenMat FloatGenMat::apply(mathFunTy f) const
// Apply function returning float to each element of vector slice
{
  FloatGenMat temp(nrows,ncols,rwUninitialized);
  register float* dp = temp.data();
  for(MatrixLooper l(nrows,ncols,rowstep,colstep,RWDataView::COLUMN_MAJOR); l; ++l) {
    register const float* sp = data()+l.start;
    register i = l.length;
    register j = l.stride;
    while(i--) {
// Turbo C++ bug requires temporary.
#if defined (__TURBOC__)
      float dummy = (*f)(*sp);
      *dp++ = dummy;
#else
      *dp++ = (*f)(*sp);  
#endif
      sp += j; 
    }
  }
  return temp;
}

#if defined(RW_NATIVE_EXTENDED)
 FloatGenMat FloatGenMat::apply(XmathFunTy f) const
// Apply function for compilers using extended types
{
  FloatGenMat temp(nrows,ncols);
  register float* dp = temp.data();
  for(MatrixLooper l(nrows,ncols,rowstep,colstep,RWDataView::COLUMN_MAJOR); l; ++l) {
    register const float* sp = data()+l.start;
    register i = l.length;
    register j = l.stride;
    while(i--) {
      *dp++ = (*f)(*sp);
      sp += j;
    }
  }
  return temp;
}
#endif


FloatGenMat atan2(const FloatGenMat& u,const FloatGenMat& v)
     // Arctangent of u/v.
{
  u.lengthCheck(v.rows(),v.cols());
  FloatGenMat temp(v.rows(),v.cols(),rwUninitialized);
  register float* dp = temp.data();
  for(DoubleMatrixLooper l(v.rows(),v.cols(),u.rowStride(),u.colStride(),v.rowStride(),v.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    register const float* up = u.data()+l.start1;
    register const float* vp = v.data()+l.start2;
    register i = l.length;
    register uj = l.stride1;
    register vj = l.stride2;
    while (i--) { *dp++ = atan2(*up,*vp);  up += uj;  vp += vj; }
  }
  return temp;
}

float dot(const FloatGenMat& u, const FloatGenMat& v)
      // dot product: sum_ij (u_ij * v_ij)
{
  u.lengthCheck(v.rows(),v.cols());
  float result = 0;
  for(DoubleMatrixLooper l(v.rows(),v.cols(),u.rowStride(),u.colStride(),v.rowStride(),v.colStride()); l; ++l) {
    float partialResult;
    rwsdot(l.length,u.data()+l.start1,l.stride1,v.data()+l.start2,l.stride2,&partialResult);
    result += partialResult;
  }
  return result;
}

void maxIndex(const FloatGenMat& s, int *maxi, int *maxj)
// Index of maximum element.
{
  int len = s.rows()*s.cols();
  s.numPointsCheck("max needs at least",1);
  if (s.colStride()==s.rowStride()*s.rows()) {    // Column major order
    int maxindex = rwsmax(len,s.data(),s.rowStride());
    *maxj = maxindex/s.rows();
    *maxi = maxindex%s.rows();
    return;
  }
  if (s.rowStride()==s.colStride()*s.cols()) {    // Row major order
    int maxindex = rwsmax(len,s.data(),s.colStride());
    *maxj = maxindex/s.cols();
    *maxi = maxindex%s.cols();
    return;
  }
  *maxi = *maxj = 0;			    // Matrix is not contiguous
  float themax = s(0,0);
  if (s.colStride()>s.rowStride()) {
    for( int j=0; j<s.cols(); j++ ) {  // Do a column at a time
      int i = rwsmax(len,s.data()+j*s.colStride(),s.rowStride());
      if (s(i,j)>themax) { themax=s(i,j); *maxi=i; *maxj=j; }
    }
  } else {
    for( int i=0; i<s.rows(); i++ ) {  // Do a row at a time
      int j = rwsmax(len,s.data()+i*s.rowStride(),s.colStride());
      if (s(i,j)>themax) { themax=s(i,j); *maxi=i; *maxj=j; }
    }
  }
}

float maxValue(const FloatGenMat& s)
{
  int i,j;
  maxIndex(s,&i,&j);
  return s(i,j);
}
    
void minIndex(const FloatGenMat& s, int *mini, int *minj)
// Index of minimum element.
{
  int len = s.rows()*s.cols();
  s.numPointsCheck("min needs at least",1);
  if (s.colStride()==s.rowStride()*s.rows()) {    // Column major order
    int minindex = rwsmin(len,s.data(),s.rowStride());
    *minj = minindex/s.rows();
    *mini = minindex%s.rows();
    return;
  }
  if (s.rowStride()==s.colStride()*s.cols()) {    // Row major order
    int minindex = rwsmin(len,s.data(),s.colStride());
    *minj = minindex/s.cols();
    *mini = minindex%s.cols();
    return;
  }
  *mini = *minj = 0;			    // Matrix is not contiguous
  float themin = s(0,0);
  if (s.colStride()>s.rowStride()) {
    for( int j=0; j<s.cols(); j++ ) {  // Do a column at a time
      int i = rwsmin(len,s.data()+j*s.colStride(),s.rowStride());
      if (s(i,j)>themin) { themin=s(i,j); *mini=i; *minj=j; }
    }
  } else {
    for( int i=0; i<s.rows(); i++ ) {  // Do a row at a time
      int j = rwsmin(len,s.data()+i*s.rowStride(),s.colStride());
      if (s(i,j)>themin) { themin=s(i,j); *mini=i; *minj=j; }
    }
  }
}
    
float minValue(const FloatGenMat& s)
{
  int i,j;
  minIndex(s,&i,&j);
  return s(i,j);
}


float prod(const FloatGenMat& s)
{
  register double t = 1;	// Accumulate prods in double precision
  for(MatrixLooper l(s.rows(),s.cols(),s.rowStride(),s.colStride()); l; ++l) {
    register float* sp = (float*)(s.data()+l.start);
    register int i = l.length;
    register j = l.stride;
    while (i--) { t *= *sp;  sp += j; }
  }
  return (float)t;
}

FloatGenMat pow(const FloatGenMat& u, const FloatGenMat& v)
// u to the v power.
{
  u.lengthCheck(v.rows(),v.cols());
  FloatGenMat temp(v.rows(),v.cols(),rwUninitialized);
  register float* dp = temp.data();
  for(DoubleMatrixLooper l(v.rows(),v.cols(),u.rowStride(),u.colStride(),v.rowStride(),v.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    register i = l.length;
    register float* up = (float*)(u.data()+l.start1);
    register float* vp = (float*)(v.data()+l.start2);
    register uj = l.stride1;
    register vj = l.stride2;
    while (i--) { *dp++ = pow(*up,*vp); up += uj; vp += vj; }
  }
  return temp;
}

float sum(const FloatGenMat& s)
{
  register double t = 0;	// Accumulate sums in double precision
  for(MatrixLooper l(s.rows(),s.cols(),s.rowStride(),s.colStride()); l; ++l) {
    register float* sp = (float*)(s.data()+l.start);
    register int i = l.length;
    register j = l.stride;
    while (i--) { t += *sp;  sp += j; }
  }
  return (float)t;
}

float mean(const FloatGenMat& s)
{
  float theSum = sum(s);
  float numPoints = s.rows()*s.cols();
  if (theSum==0) return 0;   // Covers the case of zero points
  return theSum/numPoints;
}

float variance(const FloatGenMat& s)
// Variance of a FloatGenMat
{
  s.numPointsCheck("Variance needs minimum of",2);
  register double sum2 = 0;	// Accumulate sums in double precision
  REGISTER float avg = mean(s);
  for(MatrixLooper l(s.rows(),s.cols(),s.rowStride(),s.colStride()); l; ++l) {
    register float* sp = (float*)(s.data()+l.start);
    register int i = l.length;
    register j = l.stride;
    while (i--) {
      REGISTER float temp = *sp - avg;
      sum2 += temp * temp;
      sp += j;
    }
  }
  // Use the biased estimate (easier to work with):
  return (float)(sum2/(s.rows()*s.cols()));
}

FloatGenMat transpose(const FloatGenMat& A)
{
  return A.slice(0,0, A.cols(),A.rows(), 0,1, 1,0);
}

FloatVec product(const FloatGenMat& A, const FloatVec& x)
{
  int m = A.rows();
  int n = A.cols();
  x.lengthCheck(n);
  FloatVec y(m,rwUninitialized);
  const float* Aptr = A.data();
  for(int i=0; i<m; i++) {
    rwsdot(n,x.data(),x.stride(),Aptr,A.colStride(),&(y(i)));
    Aptr += A.rowStride();
  }
  return y;
}

FloatGenMat transposeProduct(const FloatGenMat& A, const FloatGenMat& B)
{
  return product( transpose(A), B );
}


FloatVec product(const FloatVec& x, const FloatGenMat& A)
{
  return product( transpose(A),x );
}

FloatGenMat product(const FloatGenMat& A, const FloatGenMat& B)
{
  int m = A.rows();
  int n = B.cols();
  int r = A.cols();  // == B.rows() if A,B conform
  B.rowCheck(r);
  FloatGenMat AB(m,n,rwUninitialized);
  FloatVec Arow(r,rwUninitialized);
  for(int i=0; i<m; i++) {
    Arow = A.row(i);  // Compact this row to minimize page faults
    const float* Bptr = B.data();
    for(int j=0; j<n; j++) {
      rwsdot(r,Arow.data(),1,Bptr,B.rowStride(),&(AB(i,j)));
      Bptr += B.colStride();
    }
  }
  return AB;
}

/*
 * Various functions with simple definitions:
 */
FloatGenMat	acos(const FloatGenMat& V)	{ return V.apply(::acos); }
FloatGenMat	asin(const FloatGenMat& V)	{ return V.apply(::asin); }
FloatGenMat	atan(const FloatGenMat& V)	{ return V.apply(::atan); }
FloatGenMat	ceil(const FloatGenMat& V)	{ return V.apply(::ceil); }
FloatGenMat	cos(const FloatGenMat& V)		{ return V.apply(::cos);  }
FloatGenMat	cosh(const FloatGenMat& V)	{ return V.apply(::cosh); }
FloatGenMat	exp(const FloatGenMat& V)		{ return V.apply(::exp);  }
FloatGenMat	floor(const FloatGenMat& V)	{ return V.apply(::floor);}
FloatGenMat	log(const FloatGenMat& V)		{ return V.apply(::log);  }
FloatGenMat	log10(const FloatGenMat& V)	{ return V.apply(::log10);}
FloatGenMat	sin(const FloatGenMat& V)		{ return V.apply(::sin);  }
FloatGenMat	sinh(const FloatGenMat& V)	{ return V.apply(::sinh); }
FloatGenMat	tan(const FloatGenMat& V)		{ return V.apply(::tan);  }
FloatGenMat	tanh(const FloatGenMat& V)	{ return V.apply(::tanh); }

#ifdef __ZTC__
/*
 * Zortech requires its own special version of abs(const FloatGenMat&)
 * because it uses an inlined version of fabs().  Hence, apply() 
 * cannot be used.
 */
FloatGenMat
abs(const FloatGenMat& s)
// Absolute value of a FloatGenMat --- Zortech version
{
  FloatGenMat temp(s.rows(),s.cols(),rwUninitialized);
  register float* dp = temp.data();
  for(MatrixLooper l(s.rows(),s.cols(),s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    register const float* sp = s.data()+l.start;
    register i = l.length;
    register j = l.stride;
    while (i--) { *dp++ = fabs(*sp);  sp += j; }
  }
  return temp;
}
#else   /* Not Zortech */
FloatGenMat	abs(const FloatGenMat& V)		{ return V.apply(::fabs); }
#endif	
#ifdef __ZTC__
/*
 * Zortech requires its own special version of sqrt(const FloatGenMat&) 
 * because it uses an inlined version of sqrt().  Hence, apply() 
 * cannot be used.
 */
FloatGenMat
sqrt(const FloatGenMat& s)
// Square root of a FloatGenMat --- Zortech version
{
  FloatGenMat temp(s.rows(),s.cols(),rwUninitialized);
  register float* dp = temp.data();
  for(MatrixLooper l(s.rows(),s.cols(),s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    register const float* sp = s.data()+l.start;
    register i = l.length;
    register j = l.stride;
    while (i--) { *dp++ = sqrt(*sp);  sp += j; }
  }
  return temp;
}
#else
FloatGenMat	sqrt(const FloatGenMat& V)	{ return V.apply(::sqrt); }
#endif

    /**** Norm functions ****/

Float l1Norm(const FloatGenMat& x)
{
  FloatVec colNorms(x.cols(),rwUninitialized);
  for(int j=x.cols(); j--;) {
    colNorms(j) = l1Norm(x(RWAll,j));
  }
  return linfNorm(colNorms);
}
	  
Float linfNorm(const FloatGenMat& x)
{
  return l1Norm(transpose(x));
}

Float maxNorm(const FloatGenMat& x)     // Not really a norm!
{
  FloatVec colNorms(x.cols(),rwUninitialized);
  for(int j=x.cols(); j--;) {
    colNorms(j) = linfNorm(x(RWAll,j));
  }
  return linfNorm(colNorms);
}

Float frobNorm(const FloatGenMat& x)
{
  FloatVec colNorms(x.cols(),rwUninitialized);
  for(int j=x.cols(); j--;) {
    colNorms(j) = l2Norm(x(RWAll,j));
  }
  return l2Norm(colNorms);
}


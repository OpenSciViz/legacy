/*
 * Definitions for UCharVec
 *
 * Generated from template $Id: xvec.cpp,v 1.9 1993/10/04 21:44:01 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include <rw/strstrea.h>  /* I use istrstream to parse character string ctor */
#include "rw/ucvec.h"
#include "rw/rwbbla.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"
#include "rw/rand.h"

RCSID("$Header: /users/rcs/mathsrc/xvec.cpp,v 1.9 1993/10/04 21:44:01 alv Exp $");

UCharVec::UCharVec(const char *string)
 : RWVecView()
{
  istrstream s((char*)string);
  scanFrom(s);
  if (!s.good()) {
    RWTHROW( RWExternalErr( RWMessage(RWMATH_STRCTOR,string) ));
  }
}

UCharVec::UCharVec(unsigned n, RWRand& r)
 : RWVecView(n,sizeof(UChar))
{
  while(n--) {
    (*this)(n) = (UChar)r();
  }
}

UCharVec::UCharVec(unsigned n, UChar scalar)
 : RWVecView(n,sizeof(UChar))
{
  UChar scalar2 = scalar;	// Temporary required to finesse K&R compilers
  rwbset(n, data(), 1, &scalar2);
}

UCharVec::UCharVec(unsigned n, UChar scalar, UChar by)
 : RWVecView(n,sizeof(UChar))
{
  register int i = n;
  UChar* dp = data();
  UChar value = scalar;
  UChar byvalue = by;
  while(i--) {*dp++ = value; value += byvalue;}
}


UCharVec::UCharVec(const UChar* dat, unsigned n)
  : RWVecView(n,sizeof(UChar))
{
  rwbcopy(n, data(), 1, dat, 1);
}

UCharVec::UCharVec(RWBlock *block, unsigned n)
  : RWVecView(block,n)
{
  unsigned minlen = n*sizeof(UChar);
  if (block->length() < minlen) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_SHORTBLOCK, block->length(), minlen) ));
  }
}

UCharVec&
UCharVec::operator=(const UCharVec& rhs)
{
  lengthCheck(rhs.length());
  if (sameDataBlock(rhs)) { return (*this)=rhs.copy(); }  // avoid aliasing problems
  rwbcopy(npts, data(), step, rhs.data(), rhs.step);
  return *this;
}

// Assign to a scalar:
UCharVec&
UCharVec::operator=(UChar scalar)
{
  UChar scalar2 = scalar;	// Temporary necessary to finesse K&R compilers
  rwbset(npts, data(), step, &scalar2);
  return *this;
}

UCharVec&
UCharVec::reference(const UCharVec& v)
{
  RWVecView::reference(v);
  return *this;
}

// Return copy of self with distinct instance variables
UCharVec
UCharVec::copy() const
{
  UCharVec temp(npts,rwUninitialized);
  rwbcopy(npts, temp.data(), 1, data(), step);
  return temp;
}

// Synonym for copy().  Not made inline because some compilers
// have trouble with inlined temporaries with destructors.
UCharVec
UCharVec::deepCopy() const
{
  return copy();
}

// Guarantee that references==1 and stride==1
void
UCharVec::deepenShallowCopy()
{
  if (!isSimpleView()) {
    RWVecView::reference(copy());
  }
}

/*
 * Reset the length of the vector.  The contents of the old
 * vector are saved.  If the vector has grown, the extra storage
 * is zeroed out.  If the vector has shrunk, it is truncated.
 */
void
UCharVec::resize(unsigned N)
{
  if(N != npts){
    UCharVec newvec(N,rwUninitialized);

    // Copy over the minimum of the two lengths:
    unsigned nkeep = npts < N ? npts : N;
    rwbcopy(nkeep, newvec.data(), 1, data(), stride());
    unsigned oldLength= npts;	// Remember the old length

    RWVecView::reference(newvec);

    // If vector has grown, zero out the extra storage:
    if( N > oldLength ){
      static const UChar zero = 0;	/* "static" required for Glockenspiel */
      rwbset((unsigned)(N-oldLength), data()+oldLength, 1, &zero);
    }
  }
}

/*
 * Reset the length of the vector.  The results can be (and
 * probably will be) garbage.
 */
void
UCharVec::reshape(unsigned N)
{
  if(N != npts) {
    RWVecView::reference(UCharVec(N,rwUninitialized));
  }
}

UCharVec UCharVec::slice(int start, unsigned n, int str) const {
  return (*this)[RWSlice(start,n,str)];
}


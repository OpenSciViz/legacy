> #
> # $Header: /users/rcs/mathsrc/xgenpik.cpp,v 1.3 1993/08/10 21:22:18 alv Exp $
> #
> # $Log: xgenpik.cpp,v $
> # Revision 1.3  1993/08/10  21:22:18  alv
> # now uses Tools v6 compatible error handling
> #
> # Revision 1.2  1993/03/22  15:51:31  alv
> # changed PVCS Workfile keyword to RCS ID keyword
> #
> # Revision 1.1  1993/01/22  23:51:07  alv
> # Initial revision
> #
> # 
> define <ClassType>=GenMat
> include macros
/*
 * Definitions for <C>Pick
 *
 * Generated from template $Id: xgenpik.cpp,v 1.3 1993/08/10 21:22:18 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */
#include "rw/<a>genmat.h"
#include "rw/<a>genpik.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"
STARTWRAP
#include <stdio.h>
ENDWRAP

RCSID("$Header: /users/rcs/mathsrc/xgenpik.cpp,v 1.3 1993/08/10 21:22:18 alv Exp $");

void <C>Pick::operator=(const <C>& v)
{
  lengthCheck(v.rows(),v.cols());
  for(int j=cols(); j--;) {
    <T>* dest = V.data() + colpick(j)*V.colStride();
    const <T>* src = v.data() + j*v.colStride();
    for(int i=rows(); i--;) {
      dest[rowpick(i)*V.rowStride()] = src[i*v.rowStride()];
    }
  }
}

void <C>Pick::operator=(const <C>Pick& p)
{
  lengthCheck(p.rows(),p.cols());
  for(int j=cols(); j--;) {
    <T>* dest = V.data() + colpick(j)*V.colStride();
    const <T>* src = p.V.data() + p.colpick(j)*p.V.colStride();
    for(int i=rows(); i--;) {
      dest[rowpick(i)*V.rowStride()] = src[p.rowpick(i)*p.V.rowStride()];
    }
  }
}

void <C>Pick::operator=(<T> s)
{
  for(int j=cols(); j--;) {
    <T>* dest = V.data() + colpick(j)*V.colStride();
    for(int i=rows(); i--;) {
      dest[rowpick(i)*V.rowStride()] = s;
    }
  }
}

<C>& <C>::operator=(const <C>Pick& p)
{
  p.lengthCheck(rows(),cols());
  for(int j=cols(); j--;) {
    <T>* dest = data() + j*colStride();
    const <T>* src = p.V.data() + p.colpick(j)*p.V.colStride();
    for(int i=rows(); i--;) {
      dest[i*rowStride()] = src[p.rowpick(i)*p.V.rowStride()];
    }
  }
  return *this;
}

<C>::<C>(const <C>Pick& p)
  : RWMatView(p.rows(),p.cols(),sizeof(<t>))
{
  <T>* dest = data()+nrows*ncols;
  if (p.V.rowStride()==1 && p.rowpick.stride()==1) {
    for(int j=ncols; j--;) {
      const <T>* src = p.V.data() + p.colpick(j)*p.V.colStride();
      const int* pdat = p.rowpick.data()+nrows;
      for(int i=nrows; i--;) {
        *(--dest) = src[*(--pdat)];
      }
    }
  } else {
    for(int j=ncols; j--;) {
      const <T>* src = p.V.data() + p.colpick(j)*p.V.colStride();
      for(int i=nrows; i--;) {
        *(--dest) = src[p.rowpick(i)*p.V.rowStride()];
      }
    }
  }
}

<C>Pick <C>::pick(const IntVec& ipick, const IntVec& jpick) {
  return <C>Pick(*this, ipick, jpick);
}

const <C>Pick <C>::pick(const IntVec& ipick, const IntVec& jpick) const {
  return <C>Pick(*(<C>*)this, ipick, jpick);
}

/********************** U T I L I T I E S ***********************/

void <C>Pick::assertElements() const
{
  int i = rowpick.length();
  while (--i>=0) {
    if (rowpick(i)<0 || rowpick(i)>=V.rows()) {
      RWTHROW( RWBoundsErr( RWMessage(RWMATH_INDEX,	(int)rowpick(i), (unsigned)(V.rows()-1)) ));
    }
  }
  i = colpick.length();
  while (--i>=0) {
    if (colpick(i)<0 || colpick(i)>=V.cols()) {
      RWTHROW( RWBoundsErr( RWMessage(RWMATH_INDEX,	(int)colpick(i), (unsigned)(V.cols()-1)) ));
    }
  }
}

void <C>Pick::lengthCheck(unsigned m, unsigned n) const
{
  if(rows()!=m || cols()!=n) {
  	 RWTHROW( RWBoundsErr( RWMessage(RWMATH_MNMATCH, (unsigned)rows(), (unsigned)cols(), m, n)));
  }
}

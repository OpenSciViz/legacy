/*
 * Math definitions for UCharGenMat
 *
 * Generated from template $Id: xmatmath.cpp,v 1.8 1993/09/16 17:16:56 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/ucgenmat.h"
#include "rw/ucvec.h"
#include "rw/rwbbla.h"


#define REGISTER register

RCSID("$Header: /users/rcs/mathsrc/xmatmath.cpp,v 1.8 1993/09/16 17:16:56 alv Exp $");






UChar dot(const UCharGenMat& u, const UCharGenMat& v)
      // dot product: sum_ij (u_ij * v_ij)
{
  u.lengthCheck(v.rows(),v.cols());
  UChar result = 0;
  for(DoubleMatrixLooper l(v.rows(),v.cols(),u.rowStride(),u.colStride(),v.rowStride(),v.colStride()); l; ++l) {
    UChar partialResult;
    rwbdot(l.length,u.data()+l.start1,l.stride1,v.data()+l.start2,l.stride2,&partialResult);
    result += partialResult;
  }
  return result;
}

void maxIndex(const UCharGenMat& s, int *maxi, int *maxj)
// Index of maximum element.
{
  int len = s.rows()*s.cols();
  s.numPointsCheck("max needs at least",1);
  if (s.colStride()==s.rowStride()*s.rows()) {    // Column major order
    int maxindex = rwbmax(len,s.data(),s.rowStride());
    *maxj = maxindex/s.rows();
    *maxi = maxindex%s.rows();
    return;
  }
  if (s.rowStride()==s.colStride()*s.cols()) {    // Row major order
    int maxindex = rwbmax(len,s.data(),s.colStride());
    *maxj = maxindex/s.cols();
    *maxi = maxindex%s.cols();
    return;
  }
  *maxi = *maxj = 0;			    // Matrix is not contiguous
  UChar themax = s(0,0);
  if (s.colStride()>s.rowStride()) {
    for( int j=0; j<s.cols(); j++ ) {  // Do a column at a time
      int i = rwbmax(len,s.data()+j*s.colStride(),s.rowStride());
      if (s(i,j)>themax) { themax=s(i,j); *maxi=i; *maxj=j; }
    }
  } else {
    for( int i=0; i<s.rows(); i++ ) {  // Do a row at a time
      int j = rwbmax(len,s.data()+i*s.rowStride(),s.colStride());
      if (s(i,j)>themax) { themax=s(i,j); *maxi=i; *maxj=j; }
    }
  }
}

UChar maxValue(const UCharGenMat& s)
{
  int i,j;
  maxIndex(s,&i,&j);
  return s(i,j);
}
    
void minIndex(const UCharGenMat& s, int *mini, int *minj)
// Index of minimum element.
{
  int len = s.rows()*s.cols();
  s.numPointsCheck("min needs at least",1);
  if (s.colStride()==s.rowStride()*s.rows()) {    // Column major order
    int minindex = rwbmin(len,s.data(),s.rowStride());
    *minj = minindex/s.rows();
    *mini = minindex%s.rows();
    return;
  }
  if (s.rowStride()==s.colStride()*s.cols()) {    // Row major order
    int minindex = rwbmin(len,s.data(),s.colStride());
    *minj = minindex/s.cols();
    *mini = minindex%s.cols();
    return;
  }
  *mini = *minj = 0;			    // Matrix is not contiguous
  UChar themin = s(0,0);
  if (s.colStride()>s.rowStride()) {
    for( int j=0; j<s.cols(); j++ ) {  // Do a column at a time
      int i = rwbmin(len,s.data()+j*s.colStride(),s.rowStride());
      if (s(i,j)>themin) { themin=s(i,j); *mini=i; *minj=j; }
    }
  } else {
    for( int i=0; i<s.rows(); i++ ) {  // Do a row at a time
      int j = rwbmin(len,s.data()+i*s.rowStride(),s.colStride());
      if (s(i,j)>themin) { themin=s(i,j); *mini=i; *minj=j; }
    }
  }
}
    
UChar minValue(const UCharGenMat& s)
{
  int i,j;
  minIndex(s,&i,&j);
  return s(i,j);
}


UChar prod(const UCharGenMat& s)
{
  REGISTER UChar t = 1;
  for(MatrixLooper l(s.rows(),s.cols(),s.rowStride(),s.colStride()); l; ++l) {
    register UChar* sp = (UChar*)(s.data()+l.start);
    register int i = l.length;
    register j = l.stride;
    while (i--) { t *= *sp;  sp += j; }
  }
  return (UChar)t;
}


UChar sum(const UCharGenMat& s)
{
  REGISTER UChar t = 0;
  for(MatrixLooper l(s.rows(),s.cols(),s.rowStride(),s.colStride()); l; ++l) {
    register UChar* sp = (UChar*)(s.data()+l.start);
    register int i = l.length;
    register j = l.stride;
    while (i--) { t += *sp;  sp += j; }
  }
  return (UChar)t;
}



UCharGenMat transpose(const UCharGenMat& A)
{
  return A.slice(0,0, A.cols(),A.rows(), 0,1, 1,0);
}

UCharVec product(const UCharGenMat& A, const UCharVec& x)
{
  int m = A.rows();
  int n = A.cols();
  x.lengthCheck(n);
  UCharVec y(m,rwUninitialized);
  const UChar* Aptr = A.data();
  for(int i=0; i<m; i++) {
    rwbdot(n,x.data(),x.stride(),Aptr,A.colStride(),&(y(i)));
    Aptr += A.rowStride();
  }
  return y;
}

UCharGenMat transposeProduct(const UCharGenMat& A, const UCharGenMat& B)
{
  return product( transpose(A), B );
}


UCharVec product(const UCharVec& x, const UCharGenMat& A)
{
  return product( transpose(A),x );
}

UCharGenMat product(const UCharGenMat& A, const UCharGenMat& B)
{
  int m = A.rows();
  int n = B.cols();
  int r = A.cols();  // == B.rows() if A,B conform
  B.rowCheck(r);
  UCharGenMat AB(m,n,rwUninitialized);
  UCharVec Arow(r,rwUninitialized);
  for(int i=0; i<m; i++) {
    Arow = A.row(i);  // Compact this row to minimize page faults
    const UChar* Bptr = B.data();
    for(int j=0; j<n; j++) {
      rwbdot(r,Arow.data(),1,Bptr,B.rowStride(),&(AB(i,j)));
      Bptr += B.colStride();
    }
  }
  return AB;
}




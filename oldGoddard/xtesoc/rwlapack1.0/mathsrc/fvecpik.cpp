/*
 * Definitions for FloatPick
 *
 * Generated from template $Id: xvecpik.cpp,v 1.3 1993/08/10 21:22:18 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */
#include "rw/fvec.h"
#include "rw/fvecpik.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"

STARTWRAP
#include <stdio.h>
ENDWRAP

RCSID("$Header: /users/rcs/mathsrc/xvecpik.cpp,v 1.3 1993/08/10 21:22:18 alv Exp $");

void
FloatVecPick::operator=(const FloatVec& v)
{
  register n = v.length();
  lengthCheck(n);
  for(register int i = 0; i<n; i++) (*this)(i) = v(i);
}

void
FloatVecPick::operator=(const FloatVecPick& p)
{
  register n = p.length();
  lengthCheck(n);
  for(register int i = 0; i<n; i++) (*this)(i) = p(i);
}

void
FloatVecPick::operator=(Float s)
{
  register n = length();
  for(register int i = 0; i<n; i++) (*this)(i) = s;
}

FloatVec&
FloatVec::operator=(const FloatVecPick& p)
{
  register n = length();
  p.lengthCheck(n);
  for(register int i = 0; i<n; i++) (*this)(i) = p(i);
  return *this;
}

FloatVec::FloatVec(const FloatVecPick& p)
 : RWVecView(p.length(),sizeof(Float))
{
  register Float* dp = data();
  for(register int i = 0; i<npts; i++) *dp++ = p(i);
}

FloatVecPick
FloatVec::pick(const IntVec& ipick) {
  // Temporary necessary for cfront; will generate a warning:
  FloatVecPick temp(*this, ipick);
  return temp;
}

const FloatVecPick
FloatVec::pick(const IntVec& ipick) const {
  // Temporary necessary for cfront; will generate a warning:
  FloatVecPick temp(*(FloatVec*)this, ipick);  // without cast of this, a temporary FloatVec is constructed
  return temp;
}

/********************** U T I L I T I E S ***********************/

void
FloatVecPick::assertElements() const
{
  unsigned N  = pick.length();
  unsigned VN = V.length();
  for(int i=0; i<N; i++){
     if( pick(i) < 0 || pick(i) >= VN ){
       RWTHROW( RWBoundsErr( RWMessage(RWMATH_INDEX,(int)pick(i), (unsigned)(V.length()-1)) ));
     }
  }
}

void
FloatVecPick::lengthCheck(unsigned n) const
{
  if(length() != n){
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_VNMATCH, (unsigned)length(), n) ));
  }
}

> #	Compiler dependencies
> #
> # $Header: /users/rcs/mathsrc/depends.tm,v 1.2 1993/01/26 17:52:28 alv Exp $
> #
> # $Log: depends.tm,v $
> # Revision 1.2  1993/01/26  17:52:28  alv
> # removed DOS EOF
> #
> # Revision 1.1  1993/01/22  23:55:39  alv
> # Initial revision
> #
> # 
> #    Rev 1.4   17 Oct 1991 09:19:34   keffer
> # Changed include path to <rw/xxx.h>
> # 
> #    Rev 1.3   08 Oct 1991 13:44:34   keffer
> # Math V4.0 beta 2
> # 
> #    Rev 1.2   07 Sep 1991 10:38:46   keffer
> # 
> #	
> if <Compiler>==Borland || <Compiler>==Turbo
# No dependency list required if the .AUTODEPEND directive is used
> else
#	To be supplied
> endif


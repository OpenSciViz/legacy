/*
 * Definitions for various arithmetic operations
 *
 * Generated from template $Id: xvecop.cpp,v 1.7 1993/09/01 16:28:02 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/ucvec.h"
#include "rw/rwbbla.h"	

RCSID("$Header: /users/rcs/mathsrc/xvecop.cpp,v 1.7 1993/09/01 16:28:02 alv Exp $");

/************************************************
 *						*
 *		UNARY OPERATORS			*
 *						*
 ************************************************/


// Unary prefix increment on a UCharVec; (i.e. ++a)
UCharVec&
UCharVec::operator++()
{
  register i = length();
  register UChar* sp = data();
  register j = stride();
  while (i--) { ++(*sp); sp += j; }
  return *this;
}

// Unary prefix decrement on a UCharVec (i.e., --a)
UCharVec&
UCharVec::operator--()
{
  register i = length();
  register UChar* sp = data();
  register j = stride();
  while (i--) { --(*sp); sp += j; }
  return *this;
}

/************************************************
 *						*
 *		BINARY OPERATORS		*
 *		 vector - vector		*
 *						*
 ************************************************/

// Binary add element-by-element
UCharVec
operator+(const UCharVec& u, const UCharVec& v)
{
  unsigned i = v.length();
  u.lengthCheck(i);
  UCharVec temp(i,rwUninitialized);
  rwb_plvv(i, temp.data(), u.data(), u.stride(), v.data(), v.stride());
  return temp;
}

// Binary subtract element-by-element
UCharVec
operator-(const UCharVec& u, const UCharVec& v)
{
  unsigned i = v.length();
  u.lengthCheck(i);
  UCharVec temp(i,rwUninitialized);
  rwb_mivv(i, temp.data(), u.data(), u.stride(), v.data(), v.stride());
  return temp;
}

// Binary multiply element-by-element
UCharVec
operator*(const UCharVec& u, const UCharVec& v)
{
  unsigned i = v.length();
  u.lengthCheck(i);
  UCharVec temp(i,rwUninitialized);
  rwb_muvv(i, temp.data(), u.data(), u.stride(), v.data(), v.stride());
  return temp;
}

// Binary divide element-by-element
UCharVec
operator/(const UCharVec& u, const UCharVec& v)
{
  unsigned i = v.length();
  u.lengthCheck(i);
  UCharVec temp(i,rwUninitialized);
  rwb_dvvv(i, temp.data(), u.data(), u.stride(), v.data(), v.stride());
  return temp;
}

/************************************************
 *						*
 *		BINARY OPERATORS		*
 *		 vector - scalar		*
 *						*
 ************************************************/

// Add a scalar
UCharVec
operator+(const UCharVec& s, UChar scalar)
{
  unsigned i = s.length();
  UCharVec temp(i,rwUninitialized);
  UChar scalar2 = scalar;
  rwb_plvs(i, temp.data(), s.data(), s.stride(), &scalar2);
  return temp;
}

/*
 * Normally, V-s is implemented as V+(-s), but this doesn't work
 * for unsigned types, so we use this workaround.
 */
UCharVec operator-(const UCharVec& v, UChar s)
{
  UCharVec result = v.copy();
  result -= s;
  return result;
}

// Subtract from a scalar
UCharVec
operator-(UChar scalar, const UCharVec& s)
{
  unsigned i = s.length();
  UCharVec temp(i,rwUninitialized);
  UChar scalar2 = scalar;
  rwb_misv(i, temp.data(), &scalar2, s.data(), s.stride());
  return temp;
}

// Multiply by a scalar
UCharVec
operator*(const UCharVec& s, UChar scalar)
{
  unsigned i = s.length();
  UCharVec temp(i,rwUninitialized);
  UChar scalar2 = scalar;
  rwb_muvs(i, temp.data(), s.data(), s.stride(), &scalar2);
  return temp;
}

// Divide into a scalar
UCharVec
operator/(UChar scalar, const UCharVec& s)
{
  unsigned i = s.length();
  UCharVec temp(i,rwUninitialized);
  UChar scalar2 = scalar;
  rwb_dvsv(i, temp.data(), &scalar2, s.data(), s.stride());
  return temp;
}

// Divide by a scalar
UCharVec
operator/(const UCharVec& s, UChar scalar)
{
  unsigned i = s.length();
  UCharVec temp(i,rwUninitialized);
  UChar scalar2 = scalar;
  rwb_dvvs(i, temp.data(), s.data(), s.stride(), &scalar2);
  return temp;
}

/*
 * Out-of-line versions for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
UCharVec	operator*(UChar s, const UCharVec& V)	{return V*s;}
UCharVec	operator+(UChar s, const UCharVec& V)	{return V+s;}
#endif

/************************************************
 *						*
 *	ARITHMETIC ASSIGNMENT OPERATORS		*
 *	  with other vectors			*
 *						*
 ************************************************/

UCharVec&
UCharVec::operator+=(const UCharVec& u)
{
  if (sameDataBlock(u)) { return operator+=(u.deepCopy()); }  // Avoid aliasing
  unsigned i = u.length();
  lengthCheck(i);
  rwb_aplvv(i, data(), stride(), u.data(), u.stride());
  return *this;
}

UCharVec&
UCharVec::operator-=(const UCharVec& u)
{
  if (sameDataBlock(u)) { return operator-=(u.deepCopy()); }  // Avoid aliasing
  unsigned i = u.length();
  lengthCheck(i);
  rwb_amivv(i, data(), stride(), u.data(), u.stride());
  return *this;
}

UCharVec&
UCharVec::operator*=(const UCharVec& u)
{
  if (sameDataBlock(u)) { return operator*=(u.deepCopy()); }  // Avoid aliasing
  unsigned i = u.length();
  lengthCheck(i);
  rwb_amuvv(i, data(), stride(), u.data(), u.stride());
  return *this;
}

UCharVec&
UCharVec::operator/=(const UCharVec& u)
{
  if (sameDataBlock(u)) { return operator/=(u.deepCopy()); }  // Avoid aliasing
  unsigned i = u.length();
  lengthCheck(i);
  rwb_advvv(i, data(), stride(), u.data(), u.stride());
  return *this;
}

/************************************************
 *						*
 *	ARITHMETIC ASSIGNMENT OPERATORS		*
 *		  with a scalar			*
 *						*
 ************************************************/

UCharVec&
UCharVec::operator+=(UChar scalar)
{
  UChar scalar2 = scalar;
  rwb_aplvs(length(), data(), stride(), &scalar2);
  return *this;
}

/*
 * Most types implement V-=s using V+=(-s), but this doesn't
 * work for unsigned types, so we need an explicit function.
 * Since there is no rwbla, we'll just use a loop.
 */
UCharVec&
UCharVec::operator-=(UChar s)
{
  register i = length();
  register UChar* sp = data();
  register j = stride();
  while (i--) { *sp -= s; sp += j; }
  return *this;
}

UCharVec&
UCharVec::operator*=(UChar scalar)
{
  UChar scalar2 = scalar;
  rwb_amuvs(length(), data(), stride(), &scalar2);
  return *this;
}

UCharVec&
UCharVec::operator/=(UChar scalar)
{
  UChar scalar2 = scalar;
  rwb_advvs(length(), data(), stride(), &scalar2);
  return *this;
}

/************************************************
 *						*
 *		LOGICAL OPERATORS		*
 *						*
 ************************************************/

RWBoolean
UCharVec::operator==(const UCharVec& u) const
{
  unsigned i = length();
  if(i != u.length()) return FALSE;  // They can't be equal if they don't have the same length.
  return rwbsame(i, data(), stride(), u.data(), u.stride());
}

RWBoolean
UCharVec::operator!=(const UCharVec& u) const
{
  return !(*this == u);
}


> #
> # $Header: /users/rcs/mathsrc/xvecop.cpp,v 1.7 1993/09/01 16:28:02 alv Exp $
> #
> # $Log: xvecop.cpp,v $
> # Revision 1.7  1993/09/01  16:28:02  alv
> # now uses rwUninitialized form for simple constructors
> #
> # Revision 1.6  1993/03/22  15:51:31  alv
> # changed PVCS Workfile keyword to RCS ID keyword
> #
> # Revision 1.5  1993/03/12  21:09:58  alv
> # NO_INLINED_TEMP_DESTRUCTORS -> RW_NO_INLINED_TEMP_DESTRUCTORS
> #
> # Revision 1.4  1993/03/12  20:01:12  alv
> # removed refs to BCC_STRUCT_POINTER_BUG
> #
> # Revision 1.3  1993/01/29  18:52:53  alv
> # added defn for - ops for UChar
> #
> # Revision 1.2  1993/01/28  16:32:58  alv
> # UChar types no longer calculate v-s via v+(-s)
> #
> # Revision 1.1  1993/01/22  23:51:09  alv
> # Initial revision
> #
> # 
> #    Rev 1.9   25 Oct 1991 08:42:48   keffer
> # Temporary made for promotable types passed to BLAs
> # 
> #    Rev 1.8   17 Oct 1991 09:15:40   keffer
> # Changed include path to <rw/xxx.h>
> # 
> #    Rev 1.5   02 Sep 1991 11:58:56   keffer
> # Out-of-line versions for operator- and operator*.
> # 
> #    Rev 1.3   03 Aug 1991 14:11:56   keffer
> # Added a comment about prefix vs. postfix operator++() and operator--()
> # 
> #    Rev 1.2   24 Jul 1991 12:52:14   keffer
> # Added pvcs keywords
> #
> define <ClassType>=Vec
> include macros
/*
 * Definitions for various arithmetic operations
 *
 * Generated from template $Id: xvecop.cpp,v 1.7 1993/09/01 16:28:02 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

> if <t>==double || <t>==int || <R/C>==Complex
/*
 * If a promotable scalar (such as a float) is passed by value, then
 * it will get promoted to, say, a double.  If the address of this is
 * then taken, we have a pointer to a double.  Unfortunately, if
 * this address is then passed to a routine that expects a pointer
 * to a float, cfront thinks it has done its job and quietly passes
 * the address of this promoted variable.
 *
 * This problem occurs when K&R compilers are used as a backend to
 * cfront.  The workaround is to create a temporary of known type
 * on the stack.  This is the "scalar2" variable found throughout
 * this file.
 */

> endif
#include "rw/<a>vec.h"
> # Bla, bla, bla... :-)
#include "rw/rw<bla>bla.h"	

RCSID("$Header: /users/rcs/mathsrc/xvecop.cpp,v 1.7 1993/09/01 16:28:02 alv Exp $");

/************************************************
 *						*
 *		UNARY OPERATORS			*
 *						*
 ************************************************/

> if <T>!=UChar
// Unary minus on a <T>Vec
<T>Vec
operator-(const <T>Vec& s)
{
  unsigned i = s.length();
  <T>Vec temp(i,rwUninitialized);
  register <t>* sp = (<t>*)s.data();
  register <t>* dp = temp.data();
  register j = s.stride();
  while (i--) { *dp++ = -(*sp);  sp += j; }
  return temp;
}
> endif

> if <R/C>!=Complex
// Unary prefix increment on a <T>Vec; (i.e. ++a)
<T>Vec&
<T>Vec::operator++()
{
  register i = length();
  register <t>* sp = data();
  register j = stride();
  while (i--) { ++(*sp); sp += j; }
  return *this;
}

// Unary prefix decrement on a <T>Vec (i.e., --a)
<T>Vec&
<T>Vec::operator--()
{
  register i = length();
  register <t>* sp = data();
  register j = stride();
  while (i--) { --(*sp); sp += j; }
  return *this;
}
> endif

/************************************************
 *						*
 *		BINARY OPERATORS		*
 *		 vector - vector		*
 *						*
 ************************************************/

// Binary add element-by-element
<T>Vec
operator+(const <T>Vec& u, const <T>Vec& v)
{
  unsigned i = v.length();
  u.lengthCheck(i);
  <T>Vec temp(i,rwUninitialized);
  rw<bla>_plvv(i, temp.data(), u.data(), u.stride(), v.data(), v.stride());
  return temp;
}

// Binary subtract element-by-element
<T>Vec
operator-(const <T>Vec& u, const <T>Vec& v)
{
  unsigned i = v.length();
  u.lengthCheck(i);
  <T>Vec temp(i,rwUninitialized);
  rw<bla>_mivv(i, temp.data(), u.data(), u.stride(), v.data(), v.stride());
  return temp;
}

// Binary multiply element-by-element
<T>Vec
operator*(const <T>Vec& u, const <T>Vec& v)
{
  unsigned i = v.length();
  u.lengthCheck(i);
  <T>Vec temp(i,rwUninitialized);
  rw<bla>_muvv(i, temp.data(), u.data(), u.stride(), v.data(), v.stride());
  return temp;
}

// Binary divide element-by-element
<T>Vec
operator/(const <T>Vec& u, const <T>Vec& v)
{
  unsigned i = v.length();
  u.lengthCheck(i);
  <T>Vec temp(i,rwUninitialized);
  rw<bla>_dvvv(i, temp.data(), u.data(), u.stride(), v.data(), v.stride());
  return temp;
}

/************************************************
 *						*
 *		BINARY OPERATORS		*
 *		 vector - scalar		*
 *						*
 ************************************************/

// Add a scalar
<T>Vec
operator+(const <T>Vec& s, <t> scalar)
{
  unsigned i = s.length();
  <T>Vec temp(i,rwUninitialized);
> if <t>==double || <t>==int || <R/C>==Complex
  rw<bla>_plvs(i, temp.data(), s.data(), s.stride(), &scalar);
> else
> #   Promotable types need a temporary for K&R compilers:
  <t> scalar2 = scalar;
  rw<bla>_plvs(i, temp.data(), s.data(), s.stride(), &scalar2);
> endif
  return temp;
}

> if <T>==UChar
/*
 * Normally, V-s is implemented as V+(-s), but this doesn't work
 * for unsigned types, so we use this workaround.
 */
<T>Vec operator-(const <T>Vec& v, <t> s)
{
  <T>Vec result = v.copy();
  result -= s;
  return result;
}
> endif

// Subtract from a scalar
<T>Vec
operator-(<t> scalar, const <T>Vec& s)
{
  unsigned i = s.length();
  <T>Vec temp(i,rwUninitialized);
> if <t>==double || <t>==int || <R/C>==Complex
  rw<bla>_misv(i, temp.data(), &scalar, s.data(), s.stride());
> else
> #   Promotable types need a temporary for K&R compilers:
  <t> scalar2 = scalar;
  rw<bla>_misv(i, temp.data(), &scalar2, s.data(), s.stride());
> endif
  return temp;
}

// Multiply by a scalar
<T>Vec
operator*(const <T>Vec& s, <t> scalar)
{
  unsigned i = s.length();
  <T>Vec temp(i,rwUninitialized);
> if <t>==double || <t>==int || <R/C>==Complex
  rw<bla>_muvs(i, temp.data(), s.data(), s.stride(), &scalar);
> else
> #   Promotable types need a temporary for K&R compilers:
  <t> scalar2 = scalar;
  rw<bla>_muvs(i, temp.data(), s.data(), s.stride(), &scalar2);
> endif
  return temp;
}

// Divide into a scalar
<T>Vec
operator/(<t> scalar, const <T>Vec& s)
{
  unsigned i = s.length();
  <T>Vec temp(i,rwUninitialized);
> if <t>==double || <t>==int || <R/C>==Complex
  rw<bla>_dvsv(i, temp.data(), &scalar, s.data(), s.stride());
> else
> #   Promotable types need a temporary for K&R compilers:
  <t> scalar2 = scalar;
  rw<bla>_dvsv(i, temp.data(), &scalar2, s.data(), s.stride());
> endif
  return temp;
}

> #
> # Binary divide by a scalar defined in turns of V * (1/scalar) for
> # types DoubleVec and FloatVec
> #
> if <C>!=DoubleVec && <C>!=FloatVec
// Divide by a scalar
<T>Vec
operator/(const <T>Vec& s, <t> scalar)
{
  unsigned i = s.length();
  <T>Vec temp(i,rwUninitialized);
> if <t>==double || <t>==int || <R/C>==Complex
  rw<bla>_dvvs(i, temp.data(), s.data(), s.stride(), &scalar);
> else
> #   Promotable types need a temporary for K&R compilers:
  <t> scalar2 = scalar;
  rw<bla>_dvvs(i, temp.data(), s.data(), s.stride(), &scalar2);
> endif
  return temp;
}
> endif

/*
 * Out-of-line versions for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
<C>	operator*(<t> s, const <C>& V)	{return V*s;}
<C>	operator+(<t> s, const <C>& V)	{return V+s;}
> if <T>!=UChar
<C>	operator-(const <C>& V, <t> s)  {return V+(-s);}
> endif
> if <C>==DoubleVec || <C>==FloatVec
<C>	operator/(const <C>& V, <t> s)  {return V*(1/s);}
>endif
#endif

/************************************************
 *						*
 *	ARITHMETIC ASSIGNMENT OPERATORS		*
 *	  with other vectors			*
 *						*
 ************************************************/

<T>Vec&
<T>Vec::operator+=(const <T>Vec& u)
{
  if (sameDataBlock(u)) { return operator+=(u.deepCopy()); }  // Avoid aliasing
  unsigned i = u.length();
  lengthCheck(i);
  rw<bla>_aplvv(i, data(), stride(), u.data(), u.stride());
  return *this;
}

<T>Vec&
<T>Vec::operator-=(const <T>Vec& u)
{
  if (sameDataBlock(u)) { return operator-=(u.deepCopy()); }  // Avoid aliasing
  unsigned i = u.length();
  lengthCheck(i);
  rw<bla>_amivv(i, data(), stride(), u.data(), u.stride());
  return *this;
}

<T>Vec&
<T>Vec::operator*=(const <T>Vec& u)
{
  if (sameDataBlock(u)) { return operator*=(u.deepCopy()); }  // Avoid aliasing
  unsigned i = u.length();
  lengthCheck(i);
  rw<bla>_amuvv(i, data(), stride(), u.data(), u.stride());
  return *this;
}

<T>Vec&
<T>Vec::operator/=(const <T>Vec& u)
{
  if (sameDataBlock(u)) { return operator/=(u.deepCopy()); }  // Avoid aliasing
  unsigned i = u.length();
  lengthCheck(i);
  rw<bla>_advvv(i, data(), stride(), u.data(), u.stride());
  return *this;
}

/************************************************
 *						*
 *	ARITHMETIC ASSIGNMENT OPERATORS		*
 *		  with a scalar			*
 *						*
 ************************************************/

<T>Vec&
<T>Vec::operator+=(<t> scalar)
{
> if <t>==double || <t>==int || <R/C>==Complex
  rw<bla>_aplvs(length(), data(), stride(), &scalar);
> else
> #   Promotable types need a temporary for K&R compilers:
  <t> scalar2 = scalar;
  rw<bla>_aplvs(length(), data(), stride(), &scalar2);
> endif
  return *this;
}

> if <T>==UChar
/*
 * Most types implement V-=s using V+=(-s), but this doesn't
 * work for unsigned types, so we need an explicit function.
 * Since there is no rwbla, we'll just use a loop.
 */
<T>Vec&
<T>Vec::operator-=(<t> s)
{
  register i = length();
  register <t>* sp = data();
  register j = stride();
  while (i--) { *sp -= s; sp += j; }
  return *this;
}
> endif

<T>Vec&
<T>Vec::operator*=(<t> scalar)
{
> if <t>==double || <t>==int || <R/C>==Complex
  rw<bla>_amuvs(length(), data(), stride(), &scalar);
> else
> #   Promotable types need a temporary for K&R compilers:
  <t> scalar2 = scalar;
  rw<bla>_amuvs(length(), data(), stride(), &scalar2);
> endif
  return *this;
}

> if <t>!=double && <t>!=float
<T>Vec&
<T>Vec::operator/=(<t> scalar)
{
> if <t>==double || <t>==int || <R/C>==Complex
  rw<bla>_advvs(length(), data(), stride(), &scalar);
> else
> #   Promotable types need a temporary for K&R compilers:
  <t> scalar2 = scalar;
  rw<bla>_advvs(length(), data(), stride(), &scalar2);
> endif
  return *this;
}
> endif

/************************************************
 *						*
 *		LOGICAL OPERATORS		*
 *						*
 ************************************************/

RWBoolean
<T>Vec::operator==(const <T>Vec& u) const
{
  unsigned i = length();
  if(i != u.length()) return FALSE;  // They can't be equal if they don't have the same length.
  return rw<bla>same(i, data(), stride(), u.data(), u.stride());
}

RWBoolean
<T>Vec::operator!=(const <T>Vec& u) const
{
  return !(*this == u);
}


/*
 * Definitions for UCharPick
 *
 * Generated from template $Id: xvecpik.cpp,v 1.3 1993/08/10 21:22:18 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */
#include "rw/ucvec.h"
#include "rw/ucvecpik.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"

STARTWRAP
#include <stdio.h>
ENDWRAP

RCSID("$Header: /users/rcs/mathsrc/xvecpik.cpp,v 1.3 1993/08/10 21:22:18 alv Exp $");

void
UCharVecPick::operator=(const UCharVec& v)
{
  register n = v.length();
  lengthCheck(n);
  for(register int i = 0; i<n; i++) (*this)(i) = v(i);
}

void
UCharVecPick::operator=(const UCharVecPick& p)
{
  register n = p.length();
  lengthCheck(n);
  for(register int i = 0; i<n; i++) (*this)(i) = p(i);
}

void
UCharVecPick::operator=(UChar s)
{
  register n = length();
  for(register int i = 0; i<n; i++) (*this)(i) = s;
}

UCharVec&
UCharVec::operator=(const UCharVecPick& p)
{
  register n = length();
  p.lengthCheck(n);
  for(register int i = 0; i<n; i++) (*this)(i) = p(i);
  return *this;
}

UCharVec::UCharVec(const UCharVecPick& p)
 : RWVecView(p.length(),sizeof(UChar))
{
  register UChar* dp = data();
  for(register int i = 0; i<npts; i++) *dp++ = p(i);
}

UCharVecPick
UCharVec::pick(const IntVec& ipick) {
  // Temporary necessary for cfront; will generate a warning:
  UCharVecPick temp(*this, ipick);
  return temp;
}

const UCharVecPick
UCharVec::pick(const IntVec& ipick) const {
  // Temporary necessary for cfront; will generate a warning:
  UCharVecPick temp(*(UCharVec*)this, ipick);  // without cast of this, a temporary UCharVec is constructed
  return temp;
}

/********************** U T I L I T I E S ***********************/

void
UCharVecPick::assertElements() const
{
  unsigned N  = pick.length();
  unsigned VN = V.length();
  for(int i=0; i<N; i++){
     if( pick(i) < 0 || pick(i) >= VN ){
       RWTHROW( RWBoundsErr( RWMessage(RWMATH_INDEX,(int)pick(i), (unsigned)(V.length()-1)) ));
     }
  }
}

void
UCharVecPick::lengthCheck(unsigned n) const
{
  if(length() != n){
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_VNMATCH, (unsigned)length(), n) ));
  }
}

/*
 * Definitions for class RandUniform
 *
 * $Header: /users/rcs/mathsrc/randunif.cpp,v 1.6 1993/09/01 16:27:36 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: randunif.cpp,v $
 * Revision 1.6  1993/09/01  16:27:36  alv
 * now uses rwUninitialized form for simple constructors
 *
 * Revision 1.5  1993/03/23  21:17:41  alv
 * now seeds with clock time plus a bit to avoid problems with
 * using same seed over and over when creating RandUniforms
 * one after the other
 *
 * Revision 1.4  1993/03/12  21:32:40  alv
 * PRECONDITION -> RWPRECONDITION
 *
 * Revision 1.3  1993/02/09  23:35:04  griswolf
 * Fix dmaxul typo
 *
 * Revision 1.2  1993/01/26  17:10:26  alv
 * removed DOS EOF
 *
 * Revision 1.1  1993/01/22  23:51:03  alv
 * Initial revision
 *
 * 
 *    Rev 1.7   16 Apr 1992 15:15:16   KEFFER
 * Now checks for NON_ANSI_HEADERS before looking for <limits.h>.
 * 
 *    Rev 1.6   24 Mar 1992 16:29:54   KEFFER
 * DEBUG -> RWDEBUG
 * 
 *    Rev 1.5   17 Oct 1991 09:15:28   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.4   08 Oct 1991 13:45:14   keffer
 * Math V4.0 beta 2
 * 
 *    Rev 1.3   04 Sep 1991 14:39:42   keffer
 * Improved bootstrap for the generator.
 * 
 *    Rev 1.2   02 Sep 1991 12:05:48   keffer
 * Now uses builtin additive congruential generator instead of
 * system "rand()" function, allowing independent reseeding of multiple 
 * generators.
 * 
 *    Rev 1.1   24 Jul 1991 12:51:54   keffer
 * Added pvcs keywords
 *
 */

#include "rw/dvec.h"
#include "rw/dgenmat.h"
#include "rw/randunif.h"
STARTWRAP
#include <stdlib.h>
#include <time.h>
#include <math.h>
ENDWRAP

#ifndef RW_NON_ANSI_HEADERS
  /*
   * ANSI-style compilers should define a <limits.h> file which we can use to
   * find out what the max value of a long is.  Otherwise, we have
   * to use a heuristic.
   */
  STARTWRAP
# include <limits.h>
  ENDWRAP
  static const double dmaxl = (double)LONG_MAX;
#else
  /*
   * Use a heuristic.
   * This could fail if your compiler first converts an unsigned long
   * to a long, THEN converts to a double.  E.g., Zortech does this.
   */
  static const unsigned long zero = 0L;
  static const unsigned long maxul = ~zero;
  static const double dmaxl = (double)maxul / 2.0;
#endif

#ifdef RWDEBUG
# include "rw/dstats.h"
#endif

// Initialize a uniform random number generator with clock time.
// Add a bit to the clock time to avoid getting the same seed
// over and over when creating RandUniform objects one after
// the other.
RandUniform::RandUniform() 
{
  static long extrabit;
  long ltime = (long)time(0);	// Grab a time from the system and initialize the seeder with it.
  reseed(ltime + extrabit++);
}

/*
 * Reseed the generator with the given seed.
 */
void
RandUniform::reseed(long x)
{
  register i;
  index = 0;
  seeder = x;
  /*
   * Use a linear congruential generators to bootstrap the 
   * additive congruential generator.
   *
   * The first little loop just warms up the generator to be sure
   * that we are in the range where x is taking up all the bits in a long.
   */
  for(i=0; i<4; i++) { x=x*84589+45989; }  // Warm up the generator a little

  for(i=0; i<55; i++) {
    x = x*84589+45989;
    table[((21*i)%55)] = x;  // Shuffle order in which table is filled
  }
}

// Return a random number selected from a uniform 
// distribution between 0 and 1.
double
RandUniform::randValue() 
{
  /*
   * If the following precondition fails it's because your compiler 
   * did not do an unsigned long to double type conversion properly.
   * See the note above.
   */
  RWPRECONDITION(dmaxl>0);
  index = (index+1) % 55;
  table[index] = table[(index+23)%55] + table[(index+54)%55];
  double val = ( (double)table[index] / dmaxl + 1.0 ) * 0.5;
#ifdef RWDEBUG
  double roundErr = rwEpslon(1.0);	// Get an estimate of the roundoff error
  /*
   * If one of the following fails, there is probably a problem 
   * in unsigned long to double type conversion with your compiler.
   */
  if(val<0) assert( val+2*roundErr > 0.0 );
  else if (val > 1.0) assert( val-2*roundErr < 1.0 );
#endif
  // This is to avoid scaling round off errors:
  if( val<0   ) val = 0.0;
  else if( val>1.0 ) val = 1.0;
  return val;
}

// Return an array of random numbers selected from a uniform 
// distribution between 0 and 1, with shuffling.
void
RandUniform::randValue(double* d, unsigned n) 
{
  while (n--) { *d++ = RandUniform::randValue(); }
}

// Return a vector of n random numbers selected from a uniform 
// distribution between 0 and 1.
DoubleVec
RandUniform::randValue(unsigned n) 
{
  DoubleVec temp(n,rwUninitialized);
  while (n--) temp(n) = RandUniform::randValue();
  return temp;
}

// Return a nrow X ncol matrix of random numbers selected from a uniform 
// distribution between 0 and 1.
DoubleGenMat
RandUniform::randValue(unsigned nr, unsigned nc) 
{
  DoubleVec tempv = RandUniform::randValue(nr*nc);
  DoubleGenMat temp(tempv, nr, nc);
  return temp;
}


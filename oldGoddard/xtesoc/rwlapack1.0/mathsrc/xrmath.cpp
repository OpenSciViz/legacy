> #
> # $Header: /users/rcs/mathsrc/xrmath.cpp,v 1.4 1993/09/17 18:42:21 alv Exp $
> #
> # $Log: xrmath.cpp,v $
> # Revision 1.4  1993/09/17  18:42:21  alv
> # (0) -> ((int)0) for benefit of DEC compiler
> #
> # Revision 1.3  1993/09/01  16:27:58  alv
> # now uses rwUninitialized form for simple constructors
> #
> # Revision 1.2  1993/03/22  15:51:31  alv
> # changed PVCS Workfile keyword to RCS ID keyword
> #
> # Revision 1.1  1993/01/22  23:51:08  alv
> # Initial revision
> #
> # 
> #    Rev 1.3   17 Oct 1991 09:15:34   keffer
> # Changed include path to <rw/xxx.h>
> # 
> #    Rev 1.1   24 Jul 1991 12:52:06   keffer
> # Added pvcs keywords
> #
> include macros
/*
 * <T>Vec special math functions
 *
 * Generated from template $Id: xrmath.cpp,v 1.4 1993/09/17 18:42:21 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

/*
 *  This file contains functions peculiar to real vectors
 *  of precision <P>.
 */

#include "rw/<a>vec.h"
STARTWRAP
#include <math.h>
ENDWRAP

RCSID("$Header: /users/rcs/mathsrc/xrmath.cpp,v 1.4 1993/09/17 18:42:21 alv Exp $");

/*
 * Expand an even real series into its full series.
 */

<T>Vec
expandEven(const <T>Vec& v)
{
  unsigned N = v.length()-1;
  <T>Vec full(2*N,rwUninitialized);

  // Lower half:
  full.slice(0,N+1,1) = v;
  // Upper half:
  full.slice(N+1,N-1,1) = v.slice(N-1,N-1,-1);

  return full;
}

/*
 * Expand an odd series into its full series.
 */

<T>Vec
expandOdd(const <T>Vec& v)
{
  unsigned N = v.length()+1;
  <T>Vec full(2*N,rwUninitialized);

  // Lower half:
  full.slice(1,N-1,1) = v;
  // Upper half:
  full.slice(N+1,N-1,1) = -v.slice(N-2,N-1,-1);

  full((int)0) = 0;
  full(N) = 0;

  return full;
}


/*
 * Math definitions
 *
 * Generated from template $Id: xarrmath.cpp,v 1.10 1993/09/20 04:02:38 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/rwsbla.h"
#include "rw/farr.h"
#include "rw/fvec.h"


#define REGISTER register

RCSID("$Header: /users/rcs/mathsrc/xarrmath.cpp,v 1.10 1993/09/20 04:02:38 alv Exp $");




FloatArray FloatArray::apply(mathFunTy f) const
// Apply function returning float to each element of vector slice
{
  FloatArray temp(npts,rwUninitialized,RWDataView::COLUMN_MAJOR);
  register float* dp = temp.data();
  for(ArrayLooper l(npts,step); l; ++l) {
    register const float* sp = data()+l.start;
    register i = l.length;
    register j = l.stride;
    while(i--) {
// Turbo C++ bug requires temporary.
#if defined (__TURBOC__)
      float dummy = (*f)(*sp);
      *dp++ = dummy;
#else
      *dp++ = (*f)(*sp);  
#endif
      sp += j; 
    }
  }
  return temp;
}

#if defined(RW_NATIVE_EXTENDED)
FloatArray FloatArray::apply(XmathFunTy f) const
// Apply function for compilers using extended types
{
  FloatArray temp(npts,rwUninitialized,RWDataView::COLUMN_MAJOR);
  register float* dp = temp.data();
  for(ArrayLooper l(npts,step); l; ++l) {
    register const float* sp = data()+l.start;
    register i = l.length;
    register j = l.stride;
    while(i--) {
      *dp++ = (*f)(*sp);
      sp += j;
    }
  }
  return temp;
}
#endif


FloatArray atan2(const FloatArray& u,const FloatArray& v)
     // Arctangent of u/v.
{
  u.lengthCheck(v.length());
  FloatArray temp(v.length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  register float* dp = temp.data();
  for(DoubleArrayLooper l(v.length(),u.stride(),v.stride()); l; ++l) {
    register const float* up = u.data()+l.start1;
    register const float* vp = v.data()+l.start2;
    register i = l.length;
    register uj = l.stride1;
    register vj = l.stride2;
    while (i--) { *dp++ = atan2(*up,*vp);  up += uj;  vp += vj; }
  }
  return temp;
}

float dot(const FloatArray& u, const FloatArray& v)
      // dot product: sum_ij...k (u_ij...k * v_ij...k)
{
  u.lengthCheck(v.length());
  float result = 0;
  for(DoubleArrayLooper l(v.length(),u.stride(),v.stride()); l; ++l) {
    float partialResult;
    rwsdot(l.length,u.data()+l.start1,l.stride1,v.data()+l.start2,l.stride2,&partialResult);
    result += partialResult;
  }
  return result;
}

FloatArray dot(const FloatVec& V, const FloatArray& A)
      // dot product: Bj...k = sum_i (Vi * Aij...k)
{
  int n = V.length();
  A.dimensionLengthCheck(0,n);
  RWToEnd jk(1);   // Indexes all components of a vector but the first
  FloatArray B(A.length()(jk),rwUninitialized,RWDataView::COLUMN_MAJOR);
  float *dp = B.data();
  for(ArrayLooper l(B.length(),A.stride()(jk)); l; ++l) {
    const float *sp = A.data()+l.start;
    for(int i=l.length; i--;) {
      rwsdot(n,V.data(),V.stride(),sp,A.stride((int)0),dp);
      sp += l.stride;
      dp++;
    }
  }
  return B;
}

FloatArray dot(const FloatArray& A, const FloatVec& V)
      // dot product: Bi...j = sum_k (Ai...jk * Vk)
{
  int n = V.length();
  A.dimensionLengthCheck(0,n);
  RWSlice ij(0,n-1);   // Indexes all components of array index but the last
  FloatArray B(A.length()(ij),rwUninitialized,RWDataView::COLUMN_MAJOR);
  float *dp = B.data();
  for(ArrayLooper l(B.length(),A.stride()(ij)); l; ++l) {
    const float *sp = A.data()+l.start;
    for(int i=l.length; i--;) {
      rwsdot(n,V.data(),V.stride(),sp,A.stride((int)0),dp);
      sp += l.stride;
      dp++;
    }
  }
  return B;
}

IntVec maxIndex(const FloatArray& s)
// Index of maximum element.
{
  s.numPointsCheck("max needs at least",1);
  int p = s.dimension();
  if (p<1) { return s.length(); } //s.length() is a length 0 vector
  IntVec index(p,0);
  IntVec maxIndex(p,0);
  float max = s(maxIndex);

  // Loop through the array, using the bla routine to loop through the
  // innermost loop (first component of index vector).
  int strider = s.stride((int)0);
  int len = s.length((int)0);
  while(index(p-1)<s.length(p-1)) {
    const float* ptr = s.data() + dot(index,s.stride());
    int maxLocal = rwsmax(len,ptr,strider);
    if (ptr[maxLocal*strider]>max) {
      max=ptr[maxLocal*strider];
      index((int)0) = maxLocal;
      maxIndex = index;
    }
    index((int)0) = s.length((int)0);     // Skip over the zeroth component, then
    int r=0;                    // bump up the index.
    while( index(r)>=s.length(r) && ++r<p ) {
      index(r-1) = 0;
      ++(index(r));
    }
  }
  return maxIndex;
}

float maxValue(const FloatArray& s)
{
  s.numPointsCheck("maxValue needs at least",1);
  float max = *(s.data());
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    int localMaxIndex = rwsmax(l.length,s.data()+l.start,l.stride);
    float localMax = *(s.data()+l.start+localMaxIndex*l.stride);
    if (localMax>max) { max=localMax; }
  }
  return max;
}

IntVec minIndex(const FloatArray& s)
// Index of minimum element.
{
  s.numPointsCheck("min needs at least",1);
  int p = s.dimension();
  if (p<1) { return s.length(); } //s.length() is a length 0 vector
  IntVec index(p,0);
  IntVec minIndex(p,0);
  float min = s(minIndex);

  // Loop through the array, using the bla routine to loop through the
  // innermost loop (first component of index vector).
  int strider = s.stride((int)0);
  int len = s.length((int)0);
  while(index(p-1)<s.length(p-1)) {
    const float* ptr = s.data() + dot(index,s.stride());
    int minLocal = rwsmin(len,ptr,strider);
    if (ptr[minLocal*strider]<min) {
      min=ptr[minLocal*strider];
      index((int)0) = minLocal;
      minIndex = index;
    }
    index((int)0) = s.length((int)0);     // Skip over the zeroth component, then
    int r=0;                    // bump up the index.
    while( index(r)>=s.length(r) && ++r<p ) {
      index(r-1) = 0;
      ++(index(r));
    }
  }
  return minIndex;
}

float minValue(const FloatArray& s)
{
  s.numPointsCheck("minValue needs at least",1);
  float min = *(s.data());
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    int localMinIndex = rwsmin(l.length,s.data()+l.start,l.stride);
    float localMin = *(s.data()+l.start+localMinIndex*l.stride);
    if (localMin<min) { min=localMin; }
  }
  return min;
}


float prod(const FloatArray& s)
{
  register double t = 1;        // Accumulate sums in double precision
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    register float* sp = (float*)(s.data()+l.start);
    register int i = l.length;
    register j = l.stride;
    while (i--) { t *= *sp;  sp += j; }
  }
  return (float)t;
}

FloatArray pow(const FloatArray& u, const FloatArray& v)
// u to the v power.
{
  u.lengthCheck(v.length());
  FloatArray temp(v.length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  register float* dp = temp.data();
  for(DoubleArrayLooper l(v.length(),u.stride(),v.stride()); l; ++l) {
    register i = l.length;
    register float* up = (float*)(u.data()+l.start1);
    register float* vp = (float*)(v.data()+l.start2);
    register uj = l.stride1;
    register vj = l.stride2;
    while (i--) { *dp++ = pow(*up,*vp); up += uj; vp += vj; }
  }
  return temp;
}

float sum(const FloatArray& s)
{
  register double t = 0;        // Accumulate sums in double precision
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    register float* sp = (float*)(s.data()+l.start);
    register int i = l.length;
    register j = l.stride;
    while (i--) { t += *sp;  sp += j; }
  }
  return (float)t;
}

float mean(const FloatArray& s)
{
  float theSum = sum(s);
  float numPoints = prod(s.length());
  if (theSum==0) return 0;   // Covers the case of zero points
  return theSum/numPoints;
}

float variance(const FloatArray& s)
// Variance of a FloatArray
{
  s.numPointsCheck("Variance needs minimum of",2);
  register double sum2 = 0;     // Accumulate sums in double precision
  REGISTER float avg = mean(s);
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    register float* sp = (float*)(s.data()+l.start);
    register int i = l.length;
    register j = l.stride;
    while (i--) {
      REGISTER float temp = *sp - avg;
      sum2 += temp * temp;
      sp += j;
    }
  }
  // Use the biased estimate (easier to work with):
  return (float)(sum2/prod(s.length()));
}

/*
 * Various functions with simple definitions:
 */
FloatArray	acos(const FloatArray& V)	{ return V.apply(::acos); }
FloatArray	asin(const FloatArray& V)	{ return V.apply(::asin); }
FloatArray	atan(const FloatArray& V)	{ return V.apply(::atan); }
FloatArray	ceil(const FloatArray& V)	{ return V.apply(::ceil); }
FloatArray	cos(const FloatArray& V)		{ return V.apply(::cos);  }
FloatArray	cosh(const FloatArray& V)	{ return V.apply(::cosh); }
FloatArray	exp(const FloatArray& V)		{ return V.apply(::exp);  }
FloatArray	floor(const FloatArray& V)	{ return V.apply(::floor);}
FloatArray	log(const FloatArray& V)		{ return V.apply(::log);  }
FloatArray	log10(const FloatArray& V)	{ return V.apply(::log10);}
FloatArray	sin(const FloatArray& V)		{ return V.apply(::sin);  }
FloatArray	sinh(const FloatArray& V)	{ return V.apply(::sinh); }
FloatArray	tan(const FloatArray& V)		{ return V.apply(::tan);  }
FloatArray	tanh(const FloatArray& V)	{ return V.apply(::tanh); }

#ifdef __ZTC__
/*
 * Zortech requires its own special version of abs(const FloatArray&)
 * because it uses an inlined version of fabs().  Hence, apply() 
 * cannot be used.
 */
FloatArray
abs(const FloatArray& s)
// Absolute value of a FloatArray --- Zortech version
{
  FloatArray temp(s.length(),rwUninitialized);
  register float* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    register const float* sp = s.data()+l.start;
    register i = l.length;
    register j = l.stride;
    while (i--) { *dp++ = fabs(*sp);  sp += j; }
  }
  return temp;
}
#else   /* Not Zortech */
FloatArray	abs(const FloatArray& V)		{ return V.apply(::fabs); }
#endif	
#ifdef __ZTC__
/*
 * Zortech requires its own special version of sqrt(const FloatArray&) 
 * because it uses an inlined version of sqrt().  Hence, apply() 
 * cannot be used.
 */
FloatArray
sqrt(const FloatArray& s)
// Square root of a FloatArray --- Zortech version
{
  FloatArray temp(s.length(),rwUninitialized);
  register float* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    register const float* sp = s.data()+l.start;
    register i = l.length;
    register j = l.stride;
    while (i--) { *dp++ = sqrt(*sp);  sp += j; }
  }
  return temp;
}
#else
FloatArray	sqrt(const FloatArray& V)	{ return V.apply(::sqrt); }
#endif

    /**** Norm functions ****/

Float maxNorm(const FloatArray& x)     // Not really a norm!
{
  Float theMax = 0;
  for(ArrayLooper l(x.length(),x.stride()); l; ++l) {
    const Float *sp = x.data()+l.start;
    for(int i=l.length; i--;) {
      Float entry = sp[i*l.stride];
      if (entry>theMax) theMax=entry;
      if ((-entry)>theMax) theMax=(-entry);
    }
  }
  return theMax;
}

Float frobNorm(const FloatArray& x)
{
  Float sumSquares = 0;
  for(ArrayLooper l(x.length(),x.stride()); l; ++l) {
    const Float *sp = x.data()+l.start;
    for(int i=l.length; i--;) {
      Float entry = sp[i*l.stride];      
      sumSquares += entry*entry;
    }
  }
  return sqrt(sumSquares);
}


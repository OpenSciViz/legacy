/*	
 * DComplexVec FFT definitions
 *
 * $Header: /users/rcs/mathsrc/cfft.cpp,v 1.4 1993/09/01 16:27:06 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: cfft.cpp,v $
 * Revision 1.4  1993/09/01  16:27:06  alv
 * now uses rwUninitialized form for simple constructors
 *
 * Revision 1.3  1993/02/24  21:04:19  alv
 * fixed bug that forbid doing long FFTs due to overflowing int in
 * join() function.  In the process, moved some computation out of
 * inner loop of join() to give a 20% speedup.
 *
 * Revision 1.2  1993/02/23  23:42:11  alv
 * fixed DOS bug caused by overflowing 16 bit integer
 *
 * Revision 1.1  1993/01/22  23:50:56  alv
 * Initial revision
 *
 * 
 *    Rev 1.3   17 Oct 1991 09:15:18   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.2   08 Oct 1991 13:44:16   keffer
 * Math V4.0 beta 2
 * 
 *    Rev 1.1   24 Jul 1991 12:51:30   keffer
 * Added pvcs keywords
 *
 */

/*
 * Parts of this code are adapted from FFT code written and placed
 * into the public domain by Peter Valkenburg (valke@cs.vu.nl) on
 * comp.sources.unix.
 */

/*
 * Unfortunately, a workaround necessitated by a runtime bug 
 * in the Glockenspiel complex library gives this routine absolutely 
 * horrid performance when used on that compiler.  See the end
 * of this file for the gruesome details.  
 * Other compilers are not affected.
 */

#include "rw/cfft.h"

RCSID("$Header: /users/rcs/mathsrc/cfft.cpp,v 1.4 1993/09/01 16:27:06 alv Exp $");

DComplexFFTServer::DComplexFFTServer()
{
  server_N = 0;
}

DComplexFFTServer::DComplexFFTServer(unsigned oforder)
{
  setOrder(oforder);
}

DComplexFFTServer::DComplexFFTServer(const DComplexFFTServer& s) :
  the_weights(s.the_weights)
{
  server_N = s.server_N;
}

void
DComplexFFTServer::operator=(const DComplexFFTServer& s)
{
  server_N = s.server_N;
  the_weights.reference(s.the_weights);
}

void
DComplexFFTServer::setOrder(unsigned newN)
{
  if(server_N != newN){
    server_N = newN;
    the_weights.reference(rootsOfOne(newN));
  }
}

DComplexVec
DComplexFFTServer::fourier(const DComplexVec& v)
{
  if(server_N != v.length())setOrder(v.length());
  DComplexVec work = conj(v);
  DComplexVec results(server_N,rwUninitialized);
  fourTrans(work.data(), server_N, results.data());
  return conj(results);
}

DComplexVec
DComplexFFTServer::ifourier(const DComplexVec& v)
{
  if(server_N != v.length())setOrder(v.length());
  DComplexVec work = v.copy();
  DComplexVec results(server_N,rwUninitialized);
  fourTrans(work.data(), server_N, results.data());
  return results;
}

/*
 * Recursive (reverse) complex FFT on the n complex samples of array
 * in, with the Cooley-Tukey method.  The result is placed in out.  The
 * number of samples, n, is arbitrary.  The algorithm costs O (n * (r1 +
 * ... + rk)), where k is the number of factors in the prime-
 * decomposition of n (also the maximum depth of the recursion), and ri
 * is the i-th primefactor.
 */

void
DComplexFFTServer::fourTrans(DComplex* in, unsigned n, DComplex* out)
{
  unsigned r;

  if((r = radix(n)) < n)
	split(in, r, n / r, out);
  join(in, n / r, n, out);
}

/*
 * Give smallest possible radix for n.
 * No apologies for this algorithm --- if the radix is much greater
 * than 2, you're going to lose on the FFT anyway.
 */

unsigned
DComplexFFTServer::radix(unsigned n)
{
  unsigned r;

  if(n < 2) return 1;

  for(r = 2; r < n; r++)  if(n % r == 0) break;

  return r;
}

/*
 * Split array "in" of r * m samples into r parts of m samples,
 * such that in [i] goes to "out" [(i % r) * m + (i / r)].
 * Then call for each part of out fourTrans, so the r recursively
 * transformed parts will go back to in.
 */
void
DComplexFFTServer::split(DComplex* in, unsigned r, unsigned m, DComplex* out)
{
  register unsigned k, s, i, j;

  for(k = 0, j = 0; k < r; k++)
	for(s = 0, i = k; s < m; s++, i += r, j++)
		out[j] = in[i];

  for(k = 0; k < r; k++, out += m, in += m)
	fourTrans(out, m, in);
}

/*
 * Sum the n / m parts of each m samples of in to n samples in out.
 * For k = 0, a complex multiplication is avoided.
 */

void
DComplexFFTServer::join(DComplex* in, unsigned m, unsigned n, DComplex* out)
{
  // p is defined to be jkN/n
  unsigned s;
  register unsigned i, j, p;
  unsigned pinc;
  unsigned Nonn = server_N/n;

  for(s = 0; s < m; s++)
	for(j = s; j < n; j += m) {
		out[j] = in[s];
		pinc = j*Nonn;
		for(i = s + m, p = pinc; i < n; i += m, p += pinc) {
#ifdef __GLOCK__
	// Runtime bug in Glock complex library necessitates this 
	// explicit complex multiply and temporaries:
			DComplex temp = the_weights((jk * N / n) % N);
			double a2 = real(temp);
			double b2 = imag(temp);
			double a1 = real(in[i]);
			double b1 = imag(in[i]);
			out[j] += DComplex(a1*a2 - b1*b2, a1*b2 + a2*b1);
#else
			out[j] += in[i] * the_weights(p % server_N);
#endif
		}
	}
}

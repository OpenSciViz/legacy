/*
 * Definitions for RWSlice
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1992, 1993 .This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 * Limited license.
 *
 ***************************************************************************
 *
 * $Log: rwslice.cpp,v $
 * Revision 1.9  1993/10/12  14:50:38  alv
 * added non-inline copy ctor - this fixes bcc for OS/2 bug
 *
 * Revision 1.8  1993/10/04  21:44:01  alv
 * ported to Windows NT
 *
 * Revision 1.7  1993/09/15  23:27:31  alv
 * fixed bug in specification of RWAll
 *
 * Revision 1.6  1993/09/14  17:17:10  alv
 * Fixed so RWAll is inited statically
 *
 * Revision 1.5  1993/08/10  21:22:18  alv
 * now uses Tools v6 compatible error handling
 *
 * Revision 1.4  1993/05/10  20:22:57  alv
 * ported to Zortech 3.1
 *
 * Revision 1.3  1993/04/06  18:02:34  alv
 * added public default constructor, RWAll is now an RWSlice, not an RWToEnd
 *
 */

#include <rw/strstrea.h>
#include "rw/rwslice.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"

/*
 * Init RWAll by initializing its data part, and then casting RWAll
 * to that data.
 */
static RWSliceData RWAllData = { 0,-1, 1 };
#if __ZTC__ == 0x310
extern    /* For some reason, Zortech forgets that RWAll is extern */
#endif
const RWSlice& RWAll = *(RWSlice*)&RWAllData;

static const char* RWSliceClassName = "RWSlice";
static const char* RWRangeClassName = "RWRange";
static const char* RWToEndClassName = "RWToEnd";

RWSlice::RWSlice(const RWSlice& x)
{
  theBegin = x.theBegin;
  theLen = x.theLen;
  theStride = x.theStride;
}


// The string has the form x:y:z where any of x,y,z may be left out.
// The second : may also be left out, or both :'s can be left out.
// Also allow as a special case the string "*" which means ":", ie
// all the entries.
//
RWSlice::RWSlice(const char *string)
{
  char c;
  int x = 0;          // Set default values
  int y = -1;
  int z = 1;
  if (string[0]!='*' || string[1]!='\0') {  // check for special case of "*"
    istrstream s((char*)string);
    if (s>>c && c!=':') {  // Read in begin if not left out of string
      s.putback(c);
      s >> x;
      if (!(s>>c) || c!=':') {  // Eat the colon after the x, complain if it is not there
        RWTHROW( RWInternalErr( RWMessage(RWMATH_STRCTOR,string)));
      }
    }
    if (s>>c && c!=':') {  // Read in end if not left out of string
      s.putback(c);
      s >> y;
      if (s>>c && c!=':') {     // Either a colon or nothing follows y, if no colon here complain
        RWTHROW( RWInternalErr( RWMessage(RWMATH_STRCTOR,string)));
      }
    }
    s >> z;
    s >> c;         // This should push it past end of file, since we are done parsing
    if (!s.eof()) { // Note that if one of the >> failed, we won't have hit eof
      RWTHROW( RWInternalErr( RWMessage(RWMATH_STRCTOR,string)));
    }
  }
  if (z<=0) {
    RWTHROW( RWInternalErr( RWMessage(RWMATH_STRIDEPOS,z)));
  }
  theBegin = x;
  if (y==(-1)) {       // if end of range was omitted (so go to end)
    theLen = -1;
    theStride = z;
  } else if (y>=x) {   // if end of range is after begin of range
    theLen = (y-x)/z+1;
    theStride = z;
  } else {             // if end of range is before begin of range (going backwards)
    theLen = (x-y)/z+1;
    theStride = -z;
  }
}

void RWSlice::boundsCheck(int l, int s)
{
  if (l<0)  {RWTHROW( RWBoundsErr( RWMessage(RWMATH_LENPOS)));}
  if (s==0) {RWTHROW( RWBoundsErr( RWMessage(RWMATH_STRIDENZ)));}
}

void RWSlice::boundsCheck(int vecLen) const
{
  int theLength = len(vecLen);
  if (theLength!=0) {         // Testing for !=0 rather than >0 catches "IntVec i(2); i(RWToEnd(3));"
    int first = begin();
    int last  = first + (theLength-1)*stride();
    if (first<0 || first>=vecLen) { RWTHROW(RWBoundsErr(RWMessage(RWMATH_SLICEBEG,RWSliceClassName,first,vecLen-1))); }
    if (last<0 || last>=vecLen) { RWTHROW(RWBoundsErr(RWMessage(RWMATH_SLICEEND,RWSliceClassName,last,vecLen-1))); }
  }
}

void RWRange::boundsCheck(int s)
{
  if (s<=0) { RWTHROW(RWBoundsErr(RWMessage(RWMATH_STRIDEPOS,RWRangeClassName,s))); }
}

void RWToEnd::boundsCheck(int s)
{
  if (s<=0) { RWTHROW(RWBoundsErr(RWMessage(RWMATH_STRIDEPOS,RWToEndClassName,s))); }
}


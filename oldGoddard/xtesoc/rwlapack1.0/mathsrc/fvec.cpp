/*
 * Definitions for FloatVec
 *
 * Generated from template $Id: xvec.cpp,v 1.9 1993/10/04 21:44:01 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include <rw/strstrea.h>  /* I use istrstream to parse character string ctor */
#include "rw/fvec.h"
#include "rw/rwsbla.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"
#include "rw/rand.h"

RCSID("$Header: /users/rcs/mathsrc/xvec.cpp,v 1.9 1993/10/04 21:44:01 alv Exp $");

FloatVec::FloatVec(const char *string)
 : RWVecView()
{
  istrstream s((char*)string);
  scanFrom(s);
  if (!s.good()) {
    RWTHROW( RWExternalErr( RWMessage(RWMATH_STRCTOR,string) ));
  }
}

FloatVec::FloatVec(unsigned n, RWRand& r)
 : RWVecView(n,sizeof(Float))
{
  while(n--) {
    (*this)(n) = (float)r();
  }
}

FloatVec::FloatVec(unsigned n, float scalar)
 : RWVecView(n,sizeof(Float))
{
  float scalar2 = scalar;	// Temporary required to finesse K&R compilers
  rwsset(n, data(), 1, &scalar2);
}

FloatVec::FloatVec(unsigned n, float scalar, float by)
 : RWVecView(n,sizeof(Float))
{
  register int i = n;
  float* dp = data();
  float value = scalar;
  float byvalue = by;
  while(i--) {*dp++ = value; value += byvalue;}
}


FloatVec::FloatVec(const float* dat, unsigned n)
  : RWVecView(n,sizeof(float))
{
  rwscopy(n, data(), 1, dat, 1);
}

FloatVec::FloatVec(RWBlock *block, unsigned n)
  : RWVecView(block,n)
{
  unsigned minlen = n*sizeof(float);
  if (block->length() < minlen) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_SHORTBLOCK, block->length(), minlen) ));
  }
}

FloatVec&
FloatVec::operator=(const FloatVec& rhs)
{
  lengthCheck(rhs.length());
  if (sameDataBlock(rhs)) { return (*this)=rhs.copy(); }  // avoid aliasing problems
  rwscopy(npts, data(), step, rhs.data(), rhs.step);
  return *this;
}

// Assign to a scalar:
FloatVec&
FloatVec::operator=(float scalar)
{
  float scalar2 = scalar;	// Temporary necessary to finesse K&R compilers
  rwsset(npts, data(), step, &scalar2);
  return *this;
}

FloatVec&
FloatVec::reference(const FloatVec& v)
{
  RWVecView::reference(v);
  return *this;
}

// Return copy of self with distinct instance variables
FloatVec
FloatVec::copy() const
{
  FloatVec temp(npts,rwUninitialized);
  rwscopy(npts, temp.data(), 1, data(), step);
  return temp;
}

// Synonym for copy().  Not made inline because some compilers
// have trouble with inlined temporaries with destructors.
FloatVec
FloatVec::deepCopy() const
{
  return copy();
}

// Guarantee that references==1 and stride==1
void
FloatVec::deepenShallowCopy()
{
  if (!isSimpleView()) {
    RWVecView::reference(copy());
  }
}

/*
 * Reset the length of the vector.  The contents of the old
 * vector are saved.  If the vector has grown, the extra storage
 * is zeroed out.  If the vector has shrunk, it is truncated.
 */
void
FloatVec::resize(unsigned N)
{
  if(N != npts){
    FloatVec newvec(N,rwUninitialized);

    // Copy over the minimum of the two lengths:
    unsigned nkeep = npts < N ? npts : N;
    rwscopy(nkeep, newvec.data(), 1, data(), stride());
    unsigned oldLength= npts;	// Remember the old length

    RWVecView::reference(newvec);

    // If vector has grown, zero out the extra storage:
    if( N > oldLength ){
      static const float zero = 0;	/* "static" required for Glockenspiel */
      rwsset((unsigned)(N-oldLength), data()+oldLength, 1, &zero);
    }
  }
}

/*
 * Reset the length of the vector.  The results can be (and
 * probably will be) garbage.
 */
void
FloatVec::reshape(unsigned N)
{
  if(N != npts) {
    RWVecView::reference(FloatVec(N,rwUninitialized));
  }
}

FloatVec FloatVec::slice(int start, unsigned n, int str) const {
  return (*this)[RWSlice(start,n,str)];
}


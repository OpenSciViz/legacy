/*
 * Definitions for various arithmetic operations for SCharArray
 *
 * Generated from template $Id: xarrop.cpp,v 1.11 1993/09/20 04:08:08 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/scarr.h"
#include "rw/rwabla.h"	

RCSID("$Header: /users/rcs/mathsrc/xarrop.cpp,v 1.11 1993/09/20 04:08:08 alv Exp $");

/************************************************
 *                                              *
 *              UNARY OPERATORS                 *
 *                                              *
 ************************************************/

SCharArray operator-(const SCharArray& s)   // Unary minus
{
  SCharArray temp(s.length(),rwUninitialized);
  SChar* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    int n = l.length;
    SChar* sp = (SChar*)s.data()+l.start;
    while(n--) { *dp++ = -(*sp); sp+=l.stride; }
  }
  return temp;
}

// Unary prefix increment on a SCharArray; (i.e. ++a)
SCharArray& SCharArray::operator++()
{
  for(ArrayLooper l(length(),stride()); l; ++l) {
    int n = l.length;
    SChar* sp = data()+l.start;
    while(n--) { ++(*sp); sp+=l.stride; }
  }
  return *this;
}

// Unary prefix decrement on a SCharArray (i.e., --a)
SCharArray& SCharArray::operator--()
{
  for(ArrayLooper l(length(),stride()); l; ++l) {
    int n = l.length;
    SChar* sp = data()+l.start;
    while(n--) { --(*sp); sp+=l.stride; }
  }
  return *this;
}

/************************************************
 *                                              *
 *              BINARY OPERATORS                *
 *               vector - vector                *
 *                                              *
 ************************************************/

SCharArray operator+(const SCharArray& u, const SCharArray& v)
{
  u.lengthCheck(v.length());
  SCharArray temp(u.length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  SChar* dp = temp.data();
  for(DoubleArrayLooper l(u.length(),u.stride(),v.stride()); l; ++l) {
    rwa_plvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

SCharArray operator-(const SCharArray& u, const SCharArray& v)
{
  u.lengthCheck(v.length());
  SCharArray temp(u.length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  SChar* dp = temp.data();
  for(DoubleArrayLooper l(u.length(),u.stride(),v.stride()); l; ++l) {
    rwa_mivv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

SCharArray operator*(const SCharArray& u, const SCharArray& v)
{
  u.lengthCheck(v.length());
  SCharArray temp(u.length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  SChar* dp = temp.data();
  for(DoubleArrayLooper l(u.length(),u.stride(),v.stride()); l; ++l) {
    rwa_muvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

SCharArray operator/(const SCharArray& u, const SCharArray& v)
{
  u.lengthCheck(v.length());
  SCharArray temp(u.length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  SChar* dp = temp.data();
  for(DoubleArrayLooper l(u.length(),u.stride(),v.stride()); l; ++l) {
    rwa_dvvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

/************************************************
 *                                              *
 *              BINARY OPERATORS                *
 *               vector - scalar                *
 *                                              *
 ************************************************/

SCharArray operator+(const SCharArray& s, SChar scalar)
{
  SCharArray temp(s.length(),rwUninitialized);
  SChar* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    SChar scalar2 = scalar;
    rwa_plvs(l.length, dp, s.data()+l.start, l.stride, &scalar2);
    dp += l.length;
  }
  return temp;
}

SCharArray operator-(SChar scalar, const SCharArray& s)
{
  SCharArray temp(s.length(),rwUninitialized);
  SChar* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    SChar scalar2 = scalar;
    rwa_misv(l.length, dp, &scalar2, s.data()+l.start, l.stride);
    dp += l.length;
  }
  return temp;
}

SCharArray operator*(const SCharArray& s, SChar scalar)
{
  SCharArray temp(s.length(),rwUninitialized);
  SChar* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    SChar scalar2 = scalar;
    rwa_muvs(l.length, dp, s.data()+l.start, l.stride, &scalar2);
    dp += l.length;
  }
  return temp;
}

SCharArray operator/(SChar scalar, const SCharArray& s)
{
  SCharArray temp(s.length(),rwUninitialized);
  SChar* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    SChar scalar2 = scalar;
    rwa_dvsv(l.length, dp, &scalar2, s.data()+l.start, l.stride);
    dp += l.length;
  }
  return temp;
}

SCharArray operator/(const SCharArray& s, SChar scalar)
{
  SCharArray temp(s.length(),rwUninitialized);
  SChar* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    SChar scalar2 = scalar;
    rwa_dvvs(l.length, dp, s.data()+l.start, l.stride, &scalar2);
    dp += l.length;
  }
  return temp;
}

/*
 * Out-of-line versions for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
SCharArray     operator*(SChar s, const SCharArray& V)  {return V*s;}
SCharArray     operator+(SChar s, const SCharArray& V)  {return V+s;}
SCharArray     operator-(const SCharArray& V, SChar s)  {return V+(-s);}
#endif

/************************************************
 *                                              *
 *      ARITHMETIC ASSIGNMENT OPERATORS         *
 *        with other vectors                    *
 *                                              *
 ************************************************/

SCharArray& SCharArray::operator+=(const SCharArray& u)
{
  if (sameDataBlock(u)) { return operator+=(u.deepCopy()); }  // Avoid aliasing
  u.lengthCheck(length());
  for(DoubleArrayLooper l(length(),stride(),u.stride()); l; ++l) {
    rwa_aplvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}


SCharArray& SCharArray::operator-=(const SCharArray& u)
{
  if (sameDataBlock(u)) { return operator-=(u.deepCopy()); }  // Avoid aliasing
  u.lengthCheck(length());
  for(DoubleArrayLooper l(length(),stride(),u.stride()); l; ++l) {
    rwa_amivv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

SCharArray& SCharArray::operator*=(const SCharArray& u)
{
  if (sameDataBlock(u)) { return operator*=(u.deepCopy()); }  // Avoid aliasing
  u.lengthCheck(length());
  for(DoubleArrayLooper l(length(),stride(),u.stride()); l; ++l) {
    rwa_amuvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

SCharArray& SCharArray::operator/=(const SCharArray& u)
{
  if (sameDataBlock(u)) { return operator/=(u.deepCopy()); }  // Avoid aliasing
  u.lengthCheck(length());
  for(DoubleArrayLooper l(length(),stride(),u.stride()); l; ++l) {
    rwa_advvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

/************************************************
 *                                              *
 *      ARITHMETIC ASSIGNMENT OPERATORS         *
 *                with a scalar                 *
 *                                              *
 ************************************************/

SCharArray& SCharArray::operator+=(SChar scalar)
{
  for(ArrayLooper l(length(),stride()); l; ++l) {
    SChar scalar2 = scalar;
    rwa_aplvs(l.length, data()+l.start, l.stride, &scalar2);
  }
  return *this;
}

SCharArray& SCharArray::operator*=(SChar scalar)
{
  for(ArrayLooper l(length(),stride()); l; ++l) {
    SChar scalar2 = scalar;
    rwa_amuvs(l.length, data()+l.start, l.stride, &scalar2);
  }
  return *this;
}

SCharArray& SCharArray::operator/=(SChar scalar)
{
  for(ArrayLooper l(length(),stride()); l; ++l) {
    SChar scalar2 = scalar;
    rwa_advvs(l.length, data()+l.start, l.stride, &scalar2);
  }
  return *this;
}

/************************************************
 *                                              *
 *              LOGICAL OPERATORS               *
 *                                              *
 ************************************************/

RWBoolean SCharArray::operator==(const SCharArray& u) const
{
  if(length()!=u.length()) return FALSE;  // They can't be equal if they don't have the same length.
  for(DoubleArrayLooper l(length(),stride(),u.stride()); l; ++l) {
    if (!rwasame(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2)) {
      return FALSE;
    }
  }
  return TRUE;
}

RWBoolean SCharArray::operator!=(const SCharArray& u) const
{
  return !(*this == u);
}

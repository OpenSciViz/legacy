/*
 * Math definitions
 *
 * Generated from template $Id: xvecmath.cpp,v 1.7 1993/09/16 17:16:56 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/ucvec.h"
#include "rw/rwbbla.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"

#define REGISTER register

RCSID("$Header: /users/rcs/mathsrc/xvecmath.cpp,v 1.7 1993/09/16 17:16:56 alv Exp $");






UCharVec
cumsum(const UCharVec& s)
// Cumulative sum of UCharVec slice.
// Note: V == delta(cumsum(V)) == cumsum(delta(V))
{
  unsigned i = s.length();
  UCharVec temp(i,rwUninitialized);
  register UChar* sp = (UChar*)s.data();
  register UChar* dp = temp.data();
  register UChar c = 0;
  register j = s.stride();
  while (i--) { c += *sp;  *dp++ = c;  sp += j; }
  return temp;
}


UChar
dot(const UCharVec& u,const UCharVec& v)
     // Vector dot product.
{
  unsigned N = v.length();
  u.lengthCheck(N);
  UChar result;
  rwbdot(N, u.data(), u.stride(), v.data(), v.stride(), &result);
  return result;
}

int
maxIndex(const UCharVec& s)
// Index of maximum element.
{
  if (s.length() == 0) s.emptyErr("maxIndex");
  int result = rwbmax(s.length(), s.data(), s.stride());
  return result;
}

UChar
maxValue(const UCharVec& s)
{
  int inx = maxIndex(s);
  return s(inx);
}

int
minIndex(const UCharVec& s)
     // Index of minimum element.
{
  if (s.length() == 0) s.emptyErr("minIndex");
  int result = rwbmin(s.length(), s.data(), s.stride());
  return result;
}

UChar
minValue(const UCharVec& s)
{
  int inx = minIndex(s);
  return s(inx);
}


UChar
prod(const UCharVec& s)
     // Product of UCharVec elements.
{
  register unsigned i = s.length();
  register UChar* sp = (UChar*)s.data();
  REGISTER UChar t = 1;
  register j = s.stride();
  while (i--) { t *= *sp;  sp += j; }
  return t;
}


UCharVec
reverse(const UCharVec& s)
     // Reverse vector elements --- pretty tricky!
{
  return s.slice( s.length()-1, s.length(), -1);
}

UChar
sum(const UCharVec& s)
     // Sum of a UCharVec
{
  register unsigned i = s.length();
  register UChar* sp = (UChar*)s.data();
  register UChar t = 0;

  register j = s.stride();
  while (i--) { t += *sp;  sp += j; }
  return (UChar)t;
}





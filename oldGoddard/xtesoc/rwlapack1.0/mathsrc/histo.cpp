/*
 * Definitions for Histogram class.
 *
 * $Header: /users/rcs/mathsrc/histo.cpp,v 1.2 1993/09/01 16:27:20 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: histo.cpp,v $
 * Revision 1.2  1993/09/01  16:27:20  alv
 * now uses rwUninitialized form for simple constructors
 *
 * Revision 1.1  1993/01/22  23:50:59  alv
 * Initial revision
 *
 * 
 *    Rev 1.3   17 Oct 1991 09:15:20   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.2   08 Oct 1991 13:44:38   keffer
 * Math V4.0 beta 2
 * 
 *    Rev 1.1   24 Jul 1991 12:51:40   keffer
 * Added pvcs keywords
 *
 */

#include "rw/histo.h"
#include "rw/dstats.h"
#include "rw/rstream.h"
STARTWRAP
#include <stdio.h>
ENDWRAP

// construct a histogram (with n bins) of data in v; 
// bins are scaled with max and min of v: 

Histogram::Histogram( unsigned n, const DoubleVec& v) :
  IntVec(n, 0),
  binValues(n+1,rwUninitialized)
{
  nbins = n;          // set number of bins
  totalCounts = 0;
  unsigned ndata = v.length();
  DoubleVec sortedData = sort(v);	// Sort data.
  largerValues = 0;			// All the data will be in bins
  smallerValues = 0;
  register double width = (sortedData(ndata-1) - sortedData(0))/nbins;
  for (register i = 0; i < nbins+1; i++) 
    binValues(i) = i*width + sortedData(0); // Set bin boundaries

  addCount(v);			// This requires two sorts: could do better.
}

// construct a histogram (with n bins); initialized with no data;
// bins are scaled with bmax and bmin:
Histogram::Histogram( unsigned n, double bmin, double bmax) :
  IntVec(n, 0),
  binValues(n+1, bmin, (bmax-bmin)/n)
{
  nbins 	= n;
  totalCounts	= 0;
  smallerValues	= 0;
  largerValues	= 0;
}

// construct a histogram with ( b.length()-1 ) bins, 
// initialized with no data;  b contains bin boundaries:

Histogram::Histogram(const DoubleVec& b) :
  IntVec(b.length()-1, 0),
  binValues(b.copy())
{
  nbins 	= b.length()-1;
  totalCounts	= 0;
  smallerValues	= 0;
  largerValues	= 0;
}

void
Histogram::addCount(const DoubleVec& v)  
{
  DoubleVec sortedData = sort(v); // Sort data.

  register jdata = 0;
  register ndata = v.length();

  while ( jdata<ndata && sortedData(jdata)<=binValues(0) ) jdata++;
  smallerValues += jdata;	// Number of small outliers.

  for ( register ibin = 0; ibin < nbins; ibin++) { 
    while( jdata<ndata && sortedData(jdata)<=binValues(ibin+1) ) {
      jdata++; (*this)(ibin)++;
    }
  }
  largerValues += ndata-jdata;	// Number of large outliers.
  totalCounts += ndata;
}

void
Histogram::addCount(double a)
{
  totalCounts++;
  if(a > binValues(nbins)) {
    largerValues++;
    return;
  }
  if(a < binValues(0)) {
    smallerValues++;
    return;
  }
  
  for ( register i = 0; i < nbins; i++) { 
    if ( a <= binValues(i+1) ) {
      (*this)(i)++; 
      return;
    }
  }
}

// Storage requirements.
unsigned	    
Histogram::binaryStoreSize() const
{
  // Need space for:	unsigned nbins  (number of bins)
  //			unsigned largerValues 
  //			unsigned smallerValues 
  //			IntVec(nbins): int*nbins (the counts)
  //			DoubleVec(nbins+1) : double*(nbins+1) (bin boundaries)
  // 			unsigned short (ID number)
  return 3*sizeof(unsigned) + nbins*sizeof(int) + 
		(nbins+1)*sizeof(double) + sizeof(unsigned short);
}

ostream&
operator<<(ostream& s, const Histogram& h)
{
  s << "\nHISTOGRAM:\n\n";
  s << "Total number of Counts  =  \t" << h.totalCounts << "\n";  
  s << "Total number of bins    =  \t" << h.nbins  << "\n";  
  s << "Number of larger values =  \t" << h.largerValues  << "\n";  
  s << "Number of smaller values = \t" << h.smallerValues  << "\n";  
  s << "\n   Bin number        -- Bin boundaries --        Counts\n\n";
  for (int i = 0; i < h.nbins; i++){
    char line[80];
    sprintf(line, "%8d    %12.3g     %12.3g     %8d",
	    i, h.binValues(i), h.binValues(i+1), h(i) );
    cout << line << "\n";
  }
  return s;
}

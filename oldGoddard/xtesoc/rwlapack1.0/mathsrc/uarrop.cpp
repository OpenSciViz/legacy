/*
 * Definitions for various arithmetic operations for UCharArray
 *
 * Generated from template $Id: xarrop.cpp,v 1.11 1993/09/20 04:08:08 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/ucarr.h"
#include "rw/rwbbla.h"	

RCSID("$Header: /users/rcs/mathsrc/xarrop.cpp,v 1.11 1993/09/20 04:08:08 alv Exp $");

/************************************************
 *                                              *
 *              UNARY OPERATORS                 *
 *                                              *
 ************************************************/


// Unary prefix increment on a UCharArray; (i.e. ++a)
UCharArray& UCharArray::operator++()
{
  for(ArrayLooper l(length(),stride()); l; ++l) {
    int n = l.length;
    UChar* sp = data()+l.start;
    while(n--) { ++(*sp); sp+=l.stride; }
  }
  return *this;
}

// Unary prefix decrement on a UCharArray (i.e., --a)
UCharArray& UCharArray::operator--()
{
  for(ArrayLooper l(length(),stride()); l; ++l) {
    int n = l.length;
    UChar* sp = data()+l.start;
    while(n--) { --(*sp); sp+=l.stride; }
  }
  return *this;
}

/************************************************
 *                                              *
 *              BINARY OPERATORS                *
 *               vector - vector                *
 *                                              *
 ************************************************/

UCharArray operator+(const UCharArray& u, const UCharArray& v)
{
  u.lengthCheck(v.length());
  UCharArray temp(u.length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  UChar* dp = temp.data();
  for(DoubleArrayLooper l(u.length(),u.stride(),v.stride()); l; ++l) {
    rwb_plvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

UCharArray operator-(const UCharArray& u, const UCharArray& v)
{
  u.lengthCheck(v.length());
  UCharArray temp(u.length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  UChar* dp = temp.data();
  for(DoubleArrayLooper l(u.length(),u.stride(),v.stride()); l; ++l) {
    rwb_mivv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

UCharArray operator*(const UCharArray& u, const UCharArray& v)
{
  u.lengthCheck(v.length());
  UCharArray temp(u.length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  UChar* dp = temp.data();
  for(DoubleArrayLooper l(u.length(),u.stride(),v.stride()); l; ++l) {
    rwb_muvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

UCharArray operator/(const UCharArray& u, const UCharArray& v)
{
  u.lengthCheck(v.length());
  UCharArray temp(u.length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  UChar* dp = temp.data();
  for(DoubleArrayLooper l(u.length(),u.stride(),v.stride()); l; ++l) {
    rwb_dvvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

/************************************************
 *                                              *
 *              BINARY OPERATORS                *
 *               vector - scalar                *
 *                                              *
 ************************************************/

UCharArray operator+(const UCharArray& s, UChar scalar)
{
  UCharArray temp(s.length(),rwUninitialized);
  UChar* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    UChar scalar2 = scalar;
    rwb_plvs(l.length, dp, s.data()+l.start, l.stride, &scalar2);
    dp += l.length;
  }
  return temp;
}

/*
 * Normally, V-s is implemented as V+(-s), but this doesn't work
 * for unsigned types, so we use this workaround.
 */
UCharArray operator-(const UCharArray& v, UChar s)
{
  UCharArray result = v.copy();
  result -= s;
  return result;
}
UCharArray operator-(UChar scalar, const UCharArray& s)
{
  UCharArray temp(s.length(),rwUninitialized);
  UChar* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    UChar scalar2 = scalar;
    rwb_misv(l.length, dp, &scalar2, s.data()+l.start, l.stride);
    dp += l.length;
  }
  return temp;
}

UCharArray operator*(const UCharArray& s, UChar scalar)
{
  UCharArray temp(s.length(),rwUninitialized);
  UChar* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    UChar scalar2 = scalar;
    rwb_muvs(l.length, dp, s.data()+l.start, l.stride, &scalar2);
    dp += l.length;
  }
  return temp;
}

UCharArray operator/(UChar scalar, const UCharArray& s)
{
  UCharArray temp(s.length(),rwUninitialized);
  UChar* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    UChar scalar2 = scalar;
    rwb_dvsv(l.length, dp, &scalar2, s.data()+l.start, l.stride);
    dp += l.length;
  }
  return temp;
}

UCharArray operator/(const UCharArray& s, UChar scalar)
{
  UCharArray temp(s.length(),rwUninitialized);
  UChar* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    UChar scalar2 = scalar;
    rwb_dvvs(l.length, dp, s.data()+l.start, l.stride, &scalar2);
    dp += l.length;
  }
  return temp;
}

/*
 * Out-of-line versions for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
UCharArray     operator*(UChar s, const UCharArray& V)  {return V*s;}
UCharArray     operator+(UChar s, const UCharArray& V)  {return V+s;}
#endif

/************************************************
 *                                              *
 *      ARITHMETIC ASSIGNMENT OPERATORS         *
 *        with other vectors                    *
 *                                              *
 ************************************************/

UCharArray& UCharArray::operator+=(const UCharArray& u)
{
  if (sameDataBlock(u)) { return operator+=(u.deepCopy()); }  // Avoid aliasing
  u.lengthCheck(length());
  for(DoubleArrayLooper l(length(),stride(),u.stride()); l; ++l) {
    rwb_aplvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

/*
 * Most types implement V-=s using V+=(-s), but this doesn't
 * work for unsigned types, so we need an explicit function.
 * Since there is no rwbla, we'll just use a loop.
 */
UCharArray&
UCharArray::operator-=(UChar s)
{
  for(ArrayLooper l(length(),stride()); l; ++l) {
    int n = l.length;
    UChar* sp = data()+l.start;
    while(n--) { *sp -= s; sp+=l.stride; }
  }
  return *this;
}

UCharArray& UCharArray::operator-=(const UCharArray& u)
{
  if (sameDataBlock(u)) { return operator-=(u.deepCopy()); }  // Avoid aliasing
  u.lengthCheck(length());
  for(DoubleArrayLooper l(length(),stride(),u.stride()); l; ++l) {
    rwb_amivv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

UCharArray& UCharArray::operator*=(const UCharArray& u)
{
  if (sameDataBlock(u)) { return operator*=(u.deepCopy()); }  // Avoid aliasing
  u.lengthCheck(length());
  for(DoubleArrayLooper l(length(),stride(),u.stride()); l; ++l) {
    rwb_amuvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

UCharArray& UCharArray::operator/=(const UCharArray& u)
{
  if (sameDataBlock(u)) { return operator/=(u.deepCopy()); }  // Avoid aliasing
  u.lengthCheck(length());
  for(DoubleArrayLooper l(length(),stride(),u.stride()); l; ++l) {
    rwb_advvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

/************************************************
 *                                              *
 *      ARITHMETIC ASSIGNMENT OPERATORS         *
 *                with a scalar                 *
 *                                              *
 ************************************************/

UCharArray& UCharArray::operator+=(UChar scalar)
{
  for(ArrayLooper l(length(),stride()); l; ++l) {
    UChar scalar2 = scalar;
    rwb_aplvs(l.length, data()+l.start, l.stride, &scalar2);
  }
  return *this;
}

UCharArray& UCharArray::operator*=(UChar scalar)
{
  for(ArrayLooper l(length(),stride()); l; ++l) {
    UChar scalar2 = scalar;
    rwb_amuvs(l.length, data()+l.start, l.stride, &scalar2);
  }
  return *this;
}

UCharArray& UCharArray::operator/=(UChar scalar)
{
  for(ArrayLooper l(length(),stride()); l; ++l) {
    UChar scalar2 = scalar;
    rwb_advvs(l.length, data()+l.start, l.stride, &scalar2);
  }
  return *this;
}

/************************************************
 *                                              *
 *              LOGICAL OPERATORS               *
 *                                              *
 ************************************************/

RWBoolean UCharArray::operator==(const UCharArray& u) const
{
  if(length()!=u.length()) return FALSE;  // They can't be equal if they don't have the same length.
  for(DoubleArrayLooper l(length(),stride(),u.stride()); l; ++l) {
    if (!rwbsame(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2)) {
      return FALSE;
    }
  }
  return TRUE;
}

RWBoolean UCharArray::operator!=(const UCharArray& u) const
{
  return !(*this == u);
}

/*
 * Definitions for Math.h++ error messages
 *
 * $Id: matherr.cpp,v 1.6 1993/09/20 21:50:21 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 * Limited license.
 *
 ***************************************************************************
 *
 */

#include "rw/matherr.h"
#include "rw/generic.h"

#undef  DECLARE_MSG
#define DECLARE_MSG(NAME, VALUE, MSG) \
        const RWCatMsg name2(RWMATHMSG_,NAME) = {RWMATH, VALUE, MSG};    \
        const RWMsgId  name2(RWMATH_,NAME)    = &name2(RWMATHMSG_,NAME);

const char RWMATH[] = "rwmath";

DECLARE_MSG(ARRDIM,     0x3000, "[ARRDIM] Array index wrong dimension (%d != %d)");
DECLARE_MSG(BADPARITY,  0x3001, "[BADPARITY] %s: Length (%u) of a real series must be %s");
DECLARE_MSG(BADVERNO,   0x3002, "[BADVERNO] Bad formatting version number.  Expected %d; got %d");
DECLARE_MSG(CANTSOLVE,  0x3003, "[CANTSOLVE] Matrix not successfully factored");
DECLARE_MSG(INDEX,      0x3004, "[INDEX] Index (%d) out of range [0->%u]");
DECLARE_MSG(LENPOS,     0x3005, "[LENPOS] Length (%d) must not be negative");
DECLARE_MSG(MNMATCH,    0x3006, "[MNMATCH] Rows or columns do not match: (%u, %u) vs (%u, %u)");
DECLARE_MSG(NMATCH,     0x3007, "[NMATCH] Dimension length doesn't match %u vs %u");
DECLARE_MSG(NOCONDITION,0x3008, "[NOCONDITION] Condition number not computed");
DECLARE_MSG(NPOINTS,    0x3009, "[NPOINTS] Vector (%u) %s %d");
DECLARE_MSG(NSQUARE,    0x300a, "[NSQUARE] Matrix (%u, %u) is not square");
DECLARE_MSG(RWBDEALL,   0x300b, "[RWBDEALL] Attempt to deallocate unallocated RWBlock");
DECLARE_MSG(SHORTBLOCK, 0x300c, "[SHORTBLOCK] Custom RWBlock too short (%u<%u)");
DECLARE_MSG(SLICEBEG,   0x300d, "[SLICEBEG] slice begin (%d) out of range [0->%u]");
DECLARE_MSG(SLICEEND,   0x300e, "[SLICEEND] slice end (%d) out of range [0->%u]");
DECLARE_MSG(STORAGE,    0x300f, "[STORAGE] Storage type must be either ROW_MAJOR or COLUMN_MAJOR");
DECLARE_MSG(STRCTOR,    0x3010, "[STRCTOR] Could not parse string constructor \"%s\"");
DECLARE_MSG(STRIDEPOS,  0x3011, "[STRIDEPOS] Stride (%d) must be positive");
DECLARE_MSG(STRIDENZ,   0x3012, "[STRIDENZ] Stride must not be zero");
DECLARE_MSG(STRIDEMAT,  0x3013, "[STRIDEMAT] Stride matrix has wrong num rows (%d!=%d)");
DECLARE_MSG(VNMATCH,    0x3014, "[VNMATCH] Vector lengths do not match (%u vs %u)");
DECLARE_MSG(ZEROSD,     0x3015, "[ZEROSD] Series has zero standard deviation");
DECLARE_MSG(ZEROVEC,    0x3016, "[ZEROVEC] %s: vector is empty");
DECLARE_MSG(ZROOT,      0x3017, "[ZROOT] Zero'th root of 1 requested");

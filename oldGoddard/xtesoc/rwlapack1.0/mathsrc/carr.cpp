/*
 * Definitions for DComplexArray
 *
 * Generated from template $Id: xarr.cpp,v 1.12 1993/10/04 21:44:01 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1992. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include <rw/strstrea.h>  /* I use istrstream to parse character string ctor */
#include "rw/carr.h"
#include "rw/cgenmat.h"
#include "rw/cvec.h"
#include "rw/rwbla.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"
#include "rw/rand.h"

// Complexes won't fit in a register
#define REGISTER


RCSID("$Header");

DComplexArray::DComplexArray(const char *string)
 : RWArrayView(IntVec(),sizeof(DComplex))
{
  istrstream s((char*)string);
  scanFrom(s);
  if (!s.good()) {
    RWTHROW( RWExternalErr( RWMessage(RWMATH_STRCTOR) ));
  }
}

DComplexArray::DComplexArray()
  : RWArrayView(IntVec(),sizeof(DComplex))
{
}

DComplexArray::DComplexArray(const IntVec& n, RWUninitialized, Storage storage)
  : RWArrayView(n,sizeof(DComplex),storage)
{
}

DComplexArray::DComplexArray(unsigned nx, unsigned ny, unsigned nz, RWUninitialized)
  : RWArrayView(nx,ny,nz,sizeof(DComplex))
{
}

DComplexArray::DComplexArray(unsigned nx, unsigned ny, unsigned nz, unsigned nw, RWUninitialized)
  : RWArrayView(nx,ny,nz,nw,sizeof(DComplex))
{
}

static void randFill(DComplexArray& X, RWRand& r)
{
  for(RWMultiIndex i(X.length()); i; ++i) {
    X(i) = r.complex();
  }
}

DComplexArray::DComplexArray(const IntVec& n, RWRand& r, Storage storage)
  : RWArrayView(n,sizeof(DComplex),storage)
{
  randFill(*this,r);
}

DComplexArray::DComplexArray(unsigned nx, unsigned ny, unsigned nz, RWRand& r)
  : RWArrayView(nx,ny,nz,sizeof(DComplex))
{
  randFill(*this,r);
}

DComplexArray::DComplexArray(unsigned nx, unsigned ny, unsigned nz, unsigned nw, RWRand& r)
  : RWArrayView(nx,ny,nz,nw,sizeof(DComplex))
{
  randFill(*this,r);
}

DComplexArray::DComplexArray(const IntVec& n, DComplex scalar)
  : RWArrayView(n,sizeof(DComplex))
{
  rwzset(prod(n),data(),1,&scalar);
}

DComplexArray::DComplexArray(unsigned nx, unsigned ny, unsigned nz, DComplex scalar)
  : RWArrayView(nx,ny,nz,sizeof(DComplex))
{
  rwzset(nx*ny*nz,data(),1,&scalar);
}

DComplexArray::DComplexArray(unsigned nx, unsigned ny, unsigned nz, unsigned nw, DComplex scalar)
  : RWArrayView(nx,ny,nz,nw,sizeof(DComplex))
{
  rwzset(nx*ny*nz*nw,data(),1,&scalar);
}


// Type conversion: real->complex
DComplexArray::DComplexArray(const DoubleArray& re)
  : RWArrayView(re.length(),sizeof(DComplex))
{
  DComplex* cp = data();
  for(ArrayLooper l(re.length(),re.stride()); l; ++l) {
#ifdef COMPLEX_PACKS
//    static const double zero = 0;     /* "static" required for Glockenspiel */
    double zero = 0;     		     /* above bombs on lucid, so use this nice simple form */
#ifdef IMAG_LEADS
    rwdcopy(l.length, (double*)cp+1, 2, re.data()+l.start, l.stride);
    rwdset(l.length, (double*)cp, 2, &zero);
#else  /* Imaginary does not lead */
    rwdcopy(l.length, (double*)cp, 2, re.data()+l.start, l.stride);
    rwdset(l.length, (double*)cp+1, 2, &zero);
#endif
    cp += l.length;

#else /* Complex does not pack */

    register const double* rp = re.data()+l.start;
    register int rstride = l.stride;
    register int i = l.length;
    while (i--) {
      *cp++ = DComplex(*rp); 
      rp += rstride;
    }
#endif /* COMPLEX_PACKS */
  }
}  

DComplexArray::DComplexArray(const DoubleArray& re, const DoubleArray& im)
  : RWArrayView(re.length(),sizeof(DComplex))
{
  lengthCheck(im.length());
  DComplex* cp = data();
  for(DoubleArrayLooper l(re.length(),re.stride(),im.stride()); l; ++l) {
#ifdef COMPLEX_PACKS
#ifdef IMAG_LEADS
    rwdcopy(l.length, (double*)cp+1, 2, re.data()+l.start1, l.stride1);
    rwdcopy(l.length, (double*)cp, 2, im.data()+l.start2, l.stride2);
#else  /* Imaginary does not lead */
    rwdcopy(l.length, (double*)cp, 2, re.data()+l.start1, l.stride1);
    rwdcopy(l.length, (double*)cp+1, 2, im.data()+l.start2, l.stride2);
#endif
    cp += l.length;

#else /* Complex does not pack */
    register const Double* rp = re.data()+l.start1;
    register const Double* ip = im.data()+l.start2;
    register int rstride = l.stride1;
    register int istride = l.stride2;
    register int i = l.length;
    while(i--){
      *cp++ = DComplex(*rp, *ip);
      rp += rstride;
      ip += istride;
    }   
#endif /* COMPLEX_PACKS */
  }
}


// Copy constructor
DComplexArray::DComplexArray(const DComplexArray& a)
  : RWArrayView(a)
{
}

DComplexArray::DComplexArray(const DComplex* dat, const IntVec& n)
  : RWArrayView(n,sizeof(DComplex))
{
  rwzcopy(prod(n), data(), 1, dat, 1);
}

DComplexArray::DComplexArray(const DComplex* dat, unsigned nx, unsigned ny, unsigned nz)
  : RWArrayView(nx,ny,nz,sizeof(DComplex))
{
  rwzcopy(nx*ny*nz, data(), 1, dat, 1);
}

DComplexArray::DComplexArray(const DComplex* dat, unsigned nx, unsigned ny, unsigned nz, unsigned nw)
  : RWArrayView(nx,ny,nz,nw,sizeof(DComplex))
{
  rwzcopy(nx*ny*nz*nw, data(), 1, dat, 1);
}

DComplexArray::DComplexArray(const DComplexVec& dat, const IntVec& n)
  : RWArrayView(n,sizeof(DComplex))
{
  rwzcopy(prod(n), data(), 1, dat.data(), dat.stride());
}

DComplexArray::DComplexArray(const DComplexVec& dat, unsigned nx, unsigned ny, unsigned nz)
  : RWArrayView(nx,ny,nz,sizeof(DComplex))
{
  rwzcopy(nx*ny*nz, data(), 1, dat.data(), dat.stride());
}

DComplexArray::DComplexArray(const DComplexVec& dat, unsigned nx, unsigned ny, unsigned nz, unsigned nw)
  : RWArrayView(nx,ny,nz,nw,sizeof(DComplex))
{
  rwzcopy(nx*ny*nz*nw, data(), 1, dat.data(), dat.stride());
}

DComplexArray::DComplexArray(RWBlock *block, const IntVec& n)
  : RWArrayView(block,n)
{
  unsigned minlen = prod(length())*sizeof(DComplex);
  if (block->length() < minlen) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_SHORTBLOCK, block->length(), minlen) ));
  }
}

DComplexArray::DComplexArray(RWBlock *block, unsigned nx, unsigned ny, unsigned nz)
  : RWArrayView(block, nx, ny, nz)
{
  unsigned minlen = prod(length())*sizeof(DComplex);
  if (block->length() < minlen) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_SHORTBLOCK, block->length(), minlen) ));
  }
}

DComplexArray::DComplexArray(RWBlock *block, unsigned nx, unsigned ny, unsigned nz, unsigned nw)
  : RWArrayView(block, nx, ny, nz, nw)
{
  unsigned minlen = prod(length())*sizeof(DComplex);
  if (block->length() < minlen) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_SHORTBLOCK, block->length(), minlen) ));
  }
}

DComplexArray::DComplexArray(const DComplexVec& v)
  : RWArrayView(v,(void*)v.data(),IntVec(1,v.length()),IntVec(1,1))
{
}

DComplexArray::DComplexArray(const DComplexGenMat& m)
  : RWArrayView(m,(void*)m.data(),
       IntVec(2,m.rows(),m.cols()-m.rows()),                 // [ m.rows() m.cols() ]
       IntVec(2,m.rowStride(),m.colStride()-m.rowStride())) // [ m.rowStride() m.colStride() ]
{
}

// This constructor is used internally by real() and imag() and slices:
DComplexArray::DComplexArray(const RWDataView& b, DComplex* start, const IntVec& n, const IntVec& str)
  : RWArrayView(b,start,n,str)
{
}

DComplexArray& DComplexArray::operator=(const DComplexArray& rhs)
{
  lengthCheck(rhs.length());
  if(sameDataBlock(rhs)) {
    (*this) = rhs.deepCopy();   // Avoid aliasing problems
  } else {
    for(DoubleArrayLooper l(npts,step,rhs.step); l; ++l) {
      rwzcopy(l.length, data()+l.start1, l.stride1, rhs.data()+l.start2, l.stride2);
    }
  }
  return *this;
}

DComplexArray& DComplexArray::operator=(DComplex scalar)
{
  for(ArrayLooper l(npts,step); l; ++l) {
    rwzset(l.length,data()+l.start,l.stride,&scalar);
  }
  return *this;
}

DComplexArray& DComplexArray::reference(const DComplexArray& v)
{
  RWArrayView::reference(v);
  return *this;
}

// Return copy of self with distinct instance variables
DComplexArray DComplexArray::deepCopy() const
{
  return copy();
}

// Synonym for deepCopy().  Not made inline because some compilers
// have trouble with inlined temporaries with destructors
DComplexArray DComplexArray::copy() const
{
  DComplexArray temp(npts,rwUninitialized,COLUMN_MAJOR);
  temp = *this;
  return temp;
}

// Guarantee that references==1 and stride==1
void DComplexArray::deepenShallowCopy()
{
  RWBoolean isStrideCompact = (dimension()==0 || step((int)0)==1);
  for( int r=npts.length()-1; r>0 && isStrideCompact; --r ) {
    if (step(r) != npts(r-1)*step(r-1)) isStrideCompact=FALSE;
  }
  if (!isStrideCompact || !isSimpleView()) {
    RWArrayView::reference(copy());
  }
}

void DComplexArray::resize(const IntVec& N)
{
  if(N != npts){
    DComplexArray temp(N,DComplex(0,0));
    int p = npts.length()<N.length() ? npts.length() : N.length();  // # dimensions to copy
    if (p>0) {
      IntVec n(p,rwUninitialized);   // The size of the common parts of the arrays
      for(int i=0; i<p; i++) {
        n(i) = (N(i)<npts(i)) ? N(i) : npts(i);
      }
  
      // Build a view of the source common part
      IntGenMat sstr(dimension(),p,0);
      sstr.diagonal() = 1;
      DComplexArray sp = slice(IntVec(dimension(),0), n, sstr);
  
      // Build a view of the destination common part
      IntGenMat dstr(N.length(),p,0);
      dstr.diagonal() = 1;
      DComplexArray dp = temp.slice(IntVec(N.length(),0), n, dstr);
  
      dp = sp;
    }
    reference(temp);
  }
}

void DComplexArray::resize(unsigned m, unsigned n, unsigned o)
{
  IntVec i(3,rwUninitialized);
  i((int)0) = m;
  i(1) = n;
  i(2) = o;
  resize(i);
}

void DComplexArray::resize(unsigned m, unsigned n, unsigned o, unsigned p)
{
  IntVec i(4,rwUninitialized);
  i((int)0) = m;
  i(1) = n;
  i(2) = o;
  i(3) = p;
  resize(i);
}

void DComplexArray::reshape(const IntVec& N)
{
  if (N!=npts) {
    DComplexArray temp(N,rwUninitialized,COLUMN_MAJOR);
    reference(temp);
  }
}

void DComplexArray::reshape(unsigned m, unsigned n, unsigned o)
{
  IntVec i(3,rwUninitialized);
  i((int)0) = m;
  i(1) = n;
  i(2) = o;
  reshape(i);
}

void DComplexArray::reshape(unsigned m, unsigned n, unsigned o, unsigned p)
{
  IntVec i(4,rwUninitialized);
  i((int)0) = m;
  i(1) = n;
  i(2) = o;
  i(3) = p;
  reshape(i);
}

DComplexArray DComplexArray::slice(const IntVec& start, const IntVec& n, const IntGenMat& str) const
{
  sliceCheck(start, n, str);
  return DComplexArray(*this, (DComplex*)data()+dot(start,stride()), n, product(stride(),str));
}

DComplex toScalar(const DComplexArray& A)
{
  A.dimensionCheck(0);
  return *(A.data());
}

DComplexVec toVec(const DComplexArray& A)
{
  A.dimensionCheck(1);
  return DComplexVec(A,(DComplex*)A.data(),(unsigned)A.length((int)0),A.stride((int)0));
}

DComplexGenMat toGenMat(const DComplexArray& A)
{
  A.dimensionCheck(2);
  return DComplexGenMat(A,(DComplex*)A.data(),(unsigned)A.length((int)0),(unsigned)A.length(1),A.stride((int)0),A.stride(1));
}


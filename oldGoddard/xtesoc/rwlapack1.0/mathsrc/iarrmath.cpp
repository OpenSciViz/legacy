/*
 * Math definitions
 *
 * Generated from template $Id: xarrmath.cpp,v 1.10 1993/09/20 04:02:38 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/rwibla.h"
#include "rw/iarr.h"
#include "rw/ivec.h"


#define REGISTER register

RCSID("$Header: /users/rcs/mathsrc/xarrmath.cpp,v 1.10 1993/09/20 04:02:38 alv Exp $");

// Absolute value of a IntArray
IntArray abs(const IntArray& s)
{
  IntArray temp(s.length(),rwUninitialized);
  register Int* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    register int* sp = (int*)(s.data()+l.start); // Cast away "constness" to avoid problems with abs()
    register j = l.stride;
    register i = l.length;
    while (i--) { *dp++ = abs(*sp); sp += j; }
  }
  return temp;
}





int dot(const IntArray& u, const IntArray& v)
      // dot product: sum_ij...k (u_ij...k * v_ij...k)
{
  u.lengthCheck(v.length());
  int result = 0;
  for(DoubleArrayLooper l(v.length(),u.stride(),v.stride()); l; ++l) {
    int partialResult;
    rwidot(l.length,u.data()+l.start1,l.stride1,v.data()+l.start2,l.stride2,&partialResult);
    result += partialResult;
  }
  return result;
}

IntArray dot(const IntVec& V, const IntArray& A)
      // dot product: Bj...k = sum_i (Vi * Aij...k)
{
  int n = V.length();
  A.dimensionLengthCheck(0,n);
  RWToEnd jk(1);   // Indexes all components of a vector but the first
  IntArray B(A.length()(jk),rwUninitialized,RWDataView::COLUMN_MAJOR);
  int *dp = B.data();
  for(ArrayLooper l(B.length(),A.stride()(jk)); l; ++l) {
    const int *sp = A.data()+l.start;
    for(int i=l.length; i--;) {
      rwidot(n,V.data(),V.stride(),sp,A.stride((int)0),dp);
      sp += l.stride;
      dp++;
    }
  }
  return B;
}

IntArray dot(const IntArray& A, const IntVec& V)
      // dot product: Bi...j = sum_k (Ai...jk * Vk)
{
  int n = V.length();
  A.dimensionLengthCheck(0,n);
  RWSlice ij(0,n-1);   // Indexes all components of array index but the last
  IntArray B(A.length()(ij),rwUninitialized,RWDataView::COLUMN_MAJOR);
  int *dp = B.data();
  for(ArrayLooper l(B.length(),A.stride()(ij)); l; ++l) {
    const int *sp = A.data()+l.start;
    for(int i=l.length; i--;) {
      rwidot(n,V.data(),V.stride(),sp,A.stride((int)0),dp);
      sp += l.stride;
      dp++;
    }
  }
  return B;
}

IntVec maxIndex(const IntArray& s)
// Index of maximum element.
{
  s.numPointsCheck("max needs at least",1);
  int p = s.dimension();
  if (p<1) { return s.length(); } //s.length() is a length 0 vector
  IntVec index(p,0);
  IntVec maxIndex(p,0);
  int max = s(maxIndex);

  // Loop through the array, using the bla routine to loop through the
  // innermost loop (first component of index vector).
  int strider = s.stride((int)0);
  int len = s.length((int)0);
  while(index(p-1)<s.length(p-1)) {
    const int* ptr = s.data() + dot(index,s.stride());
    int maxLocal = rwimax(len,ptr,strider);
    if (ptr[maxLocal*strider]>max) {
      max=ptr[maxLocal*strider];
      index((int)0) = maxLocal;
      maxIndex = index;
    }
    index((int)0) = s.length((int)0);     // Skip over the zeroth component, then
    int r=0;                    // bump up the index.
    while( index(r)>=s.length(r) && ++r<p ) {
      index(r-1) = 0;
      ++(index(r));
    }
  }
  return maxIndex;
}

int maxValue(const IntArray& s)
{
  s.numPointsCheck("maxValue needs at least",1);
  int max = *(s.data());
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    int localMaxIndex = rwimax(l.length,s.data()+l.start,l.stride);
    int localMax = *(s.data()+l.start+localMaxIndex*l.stride);
    if (localMax>max) { max=localMax; }
  }
  return max;
}

IntVec minIndex(const IntArray& s)
// Index of minimum element.
{
  s.numPointsCheck("min needs at least",1);
  int p = s.dimension();
  if (p<1) { return s.length(); } //s.length() is a length 0 vector
  IntVec index(p,0);
  IntVec minIndex(p,0);
  int min = s(minIndex);

  // Loop through the array, using the bla routine to loop through the
  // innermost loop (first component of index vector).
  int strider = s.stride((int)0);
  int len = s.length((int)0);
  while(index(p-1)<s.length(p-1)) {
    const int* ptr = s.data() + dot(index,s.stride());
    int minLocal = rwimin(len,ptr,strider);
    if (ptr[minLocal*strider]<min) {
      min=ptr[minLocal*strider];
      index((int)0) = minLocal;
      minIndex = index;
    }
    index((int)0) = s.length((int)0);     // Skip over the zeroth component, then
    int r=0;                    // bump up the index.
    while( index(r)>=s.length(r) && ++r<p ) {
      index(r-1) = 0;
      ++(index(r));
    }
  }
  return minIndex;
}

int minValue(const IntArray& s)
{
  s.numPointsCheck("minValue needs at least",1);
  int min = *(s.data());
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    int localMinIndex = rwimin(l.length,s.data()+l.start,l.stride);
    int localMin = *(s.data()+l.start+localMinIndex*l.stride);
    if (localMin<min) { min=localMin; }
  }
  return min;
}


int prod(const IntArray& s)
{
  REGISTER int t = 1;
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    register int* sp = (int*)(s.data()+l.start);
    register int i = l.length;
    register j = l.stride;
    while (i--) { t *= *sp;  sp += j; }
  }
  return (int)t;
}


int sum(const IntArray& s)
{
  REGISTER int t = 0;
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    register int* sp = (int*)(s.data()+l.start);
    register int i = l.length;
    register j = l.stride;
    while (i--) { t += *sp;  sp += j; }
  }
  return (int)t;
}






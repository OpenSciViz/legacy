/*
 * Math definitions
 *
 * Generated from template $Id: xarrmath.cpp,v 1.10 1993/09/20 04:02:38 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/rwdbla.h"
#include "rw/darr.h"
#include "rw/dvec.h"


#define REGISTER register

RCSID("$Header: /users/rcs/mathsrc/xarrmath.cpp,v 1.10 1993/09/20 04:02:38 alv Exp $");




DoubleArray DoubleArray::apply(mathFunTy f) const
// Apply function returning double to each element of vector slice
{
  DoubleArray temp(npts,rwUninitialized,RWDataView::COLUMN_MAJOR);
  register double* dp = temp.data();
  for(ArrayLooper l(npts,step); l; ++l) {
    register const double* sp = data()+l.start;
    register i = l.length;
    register j = l.stride;
    while(i--) {
// Turbo C++ bug requires temporary.
#if defined (__TURBOC__)
      double dummy = (*f)(*sp);
      *dp++ = dummy;
#else
      *dp++ = (*f)(*sp);  
#endif
      sp += j; 
    }
  }
  return temp;
}

#if defined(RW_NATIVE_EXTENDED)
DoubleArray DoubleArray::apply(XmathFunTy f) const
// Apply function for compilers using extended types
{
  DoubleArray temp(npts,rwUninitialized,RWDataView::COLUMN_MAJOR);
  register double* dp = temp.data();
  for(ArrayLooper l(npts,step); l; ++l) {
    register const double* sp = data()+l.start;
    register i = l.length;
    register j = l.stride;
    while(i--) {
      *dp++ = (*f)(*sp);
      sp += j;
    }
  }
  return temp;
}
#endif


DoubleArray atan2(const DoubleArray& u,const DoubleArray& v)
     // Arctangent of u/v.
{
  u.lengthCheck(v.length());
  DoubleArray temp(v.length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  register double* dp = temp.data();
  for(DoubleArrayLooper l(v.length(),u.stride(),v.stride()); l; ++l) {
    register const double* up = u.data()+l.start1;
    register const double* vp = v.data()+l.start2;
    register i = l.length;
    register uj = l.stride1;
    register vj = l.stride2;
    while (i--) { *dp++ = atan2(*up,*vp);  up += uj;  vp += vj; }
  }
  return temp;
}

double dot(const DoubleArray& u, const DoubleArray& v)
      // dot product: sum_ij...k (u_ij...k * v_ij...k)
{
  u.lengthCheck(v.length());
  double result = 0;
  for(DoubleArrayLooper l(v.length(),u.stride(),v.stride()); l; ++l) {
    double partialResult;
    rwddot(l.length,u.data()+l.start1,l.stride1,v.data()+l.start2,l.stride2,&partialResult);
    result += partialResult;
  }
  return result;
}

DoubleArray dot(const DoubleVec& V, const DoubleArray& A)
      // dot product: Bj...k = sum_i (Vi * Aij...k)
{
  int n = V.length();
  A.dimensionLengthCheck(0,n);
  RWToEnd jk(1);   // Indexes all components of a vector but the first
  DoubleArray B(A.length()(jk),rwUninitialized,RWDataView::COLUMN_MAJOR);
  double *dp = B.data();
  for(ArrayLooper l(B.length(),A.stride()(jk)); l; ++l) {
    const double *sp = A.data()+l.start;
    for(int i=l.length; i--;) {
      rwddot(n,V.data(),V.stride(),sp,A.stride((int)0),dp);
      sp += l.stride;
      dp++;
    }
  }
  return B;
}

DoubleArray dot(const DoubleArray& A, const DoubleVec& V)
      // dot product: Bi...j = sum_k (Ai...jk * Vk)
{
  int n = V.length();
  A.dimensionLengthCheck(0,n);
  RWSlice ij(0,n-1);   // Indexes all components of array index but the last
  DoubleArray B(A.length()(ij),rwUninitialized,RWDataView::COLUMN_MAJOR);
  double *dp = B.data();
  for(ArrayLooper l(B.length(),A.stride()(ij)); l; ++l) {
    const double *sp = A.data()+l.start;
    for(int i=l.length; i--;) {
      rwddot(n,V.data(),V.stride(),sp,A.stride((int)0),dp);
      sp += l.stride;
      dp++;
    }
  }
  return B;
}

IntVec maxIndex(const DoubleArray& s)
// Index of maximum element.
{
  s.numPointsCheck("max needs at least",1);
  int p = s.dimension();
  if (p<1) { return s.length(); } //s.length() is a length 0 vector
  IntVec index(p,0);
  IntVec maxIndex(p,0);
  double max = s(maxIndex);

  // Loop through the array, using the bla routine to loop through the
  // innermost loop (first component of index vector).
  int strider = s.stride((int)0);
  int len = s.length((int)0);
  while(index(p-1)<s.length(p-1)) {
    const double* ptr = s.data() + dot(index,s.stride());
    int maxLocal = rwdmax(len,ptr,strider);
    if (ptr[maxLocal*strider]>max) {
      max=ptr[maxLocal*strider];
      index((int)0) = maxLocal;
      maxIndex = index;
    }
    index((int)0) = s.length((int)0);     // Skip over the zeroth component, then
    int r=0;                    // bump up the index.
    while( index(r)>=s.length(r) && ++r<p ) {
      index(r-1) = 0;
      ++(index(r));
    }
  }
  return maxIndex;
}

double maxValue(const DoubleArray& s)
{
  s.numPointsCheck("maxValue needs at least",1);
  double max = *(s.data());
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    int localMaxIndex = rwdmax(l.length,s.data()+l.start,l.stride);
    double localMax = *(s.data()+l.start+localMaxIndex*l.stride);
    if (localMax>max) { max=localMax; }
  }
  return max;
}

IntVec minIndex(const DoubleArray& s)
// Index of minimum element.
{
  s.numPointsCheck("min needs at least",1);
  int p = s.dimension();
  if (p<1) { return s.length(); } //s.length() is a length 0 vector
  IntVec index(p,0);
  IntVec minIndex(p,0);
  double min = s(minIndex);

  // Loop through the array, using the bla routine to loop through the
  // innermost loop (first component of index vector).
  int strider = s.stride((int)0);
  int len = s.length((int)0);
  while(index(p-1)<s.length(p-1)) {
    const double* ptr = s.data() + dot(index,s.stride());
    int minLocal = rwdmin(len,ptr,strider);
    if (ptr[minLocal*strider]<min) {
      min=ptr[minLocal*strider];
      index((int)0) = minLocal;
      minIndex = index;
    }
    index((int)0) = s.length((int)0);     // Skip over the zeroth component, then
    int r=0;                    // bump up the index.
    while( index(r)>=s.length(r) && ++r<p ) {
      index(r-1) = 0;
      ++(index(r));
    }
  }
  return minIndex;
}

double minValue(const DoubleArray& s)
{
  s.numPointsCheck("minValue needs at least",1);
  double min = *(s.data());
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    int localMinIndex = rwdmin(l.length,s.data()+l.start,l.stride);
    double localMin = *(s.data()+l.start+localMinIndex*l.stride);
    if (localMin<min) { min=localMin; }
  }
  return min;
}


double prod(const DoubleArray& s)
{
  REGISTER double t = 1;
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    register double* sp = (double*)(s.data()+l.start);
    register int i = l.length;
    register j = l.stride;
    while (i--) { t *= *sp;  sp += j; }
  }
  return (double)t;
}

DoubleArray pow(const DoubleArray& u, const DoubleArray& v)
// u to the v power.
{
  u.lengthCheck(v.length());
  DoubleArray temp(v.length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  register double* dp = temp.data();
  for(DoubleArrayLooper l(v.length(),u.stride(),v.stride()); l; ++l) {
    register i = l.length;
    register double* up = (double*)(u.data()+l.start1);
    register double* vp = (double*)(v.data()+l.start2);
    register uj = l.stride1;
    register vj = l.stride2;
    while (i--) { *dp++ = pow(*up,*vp); up += uj; vp += vj; }
  }
  return temp;
}

double sum(const DoubleArray& s)
{
  REGISTER double t = 0;
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    register double* sp = (double*)(s.data()+l.start);
    register int i = l.length;
    register j = l.stride;
    while (i--) { t += *sp;  sp += j; }
  }
  return (double)t;
}

double mean(const DoubleArray& s)
{
  double theSum = sum(s);
  double numPoints = prod(s.length());
  if (theSum==0) return 0;   // Covers the case of zero points
  return theSum/numPoints;
}

double variance(const DoubleArray& s)
// Variance of a DoubleArray
{
  s.numPointsCheck("Variance needs minimum of",2);
  register double sum2 = 0;     // Accumulate sums in double precision
  REGISTER double avg = mean(s);
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    register double* sp = (double*)(s.data()+l.start);
    register int i = l.length;
    register j = l.stride;
    while (i--) {
      REGISTER double temp = *sp - avg;
      sum2 += temp * temp;
      sp += j;
    }
  }
  // Use the biased estimate (easier to work with):
  return (double)(sum2/prod(s.length()));
}

/*
 * Various functions with simple definitions:
 */
DoubleArray	acos(const DoubleArray& V)	{ return V.apply(::acos); }
DoubleArray	asin(const DoubleArray& V)	{ return V.apply(::asin); }
DoubleArray	atan(const DoubleArray& V)	{ return V.apply(::atan); }
DoubleArray	ceil(const DoubleArray& V)	{ return V.apply(::ceil); }
DoubleArray	cos(const DoubleArray& V)		{ return V.apply(::cos);  }
DoubleArray	cosh(const DoubleArray& V)	{ return V.apply(::cosh); }
DoubleArray	exp(const DoubleArray& V)		{ return V.apply(::exp);  }
DoubleArray	floor(const DoubleArray& V)	{ return V.apply(::floor);}
DoubleArray	log(const DoubleArray& V)		{ return V.apply(::log);  }
DoubleArray	log10(const DoubleArray& V)	{ return V.apply(::log10);}
DoubleArray	sin(const DoubleArray& V)		{ return V.apply(::sin);  }
DoubleArray	sinh(const DoubleArray& V)	{ return V.apply(::sinh); }
DoubleArray	tan(const DoubleArray& V)		{ return V.apply(::tan);  }
DoubleArray	tanh(const DoubleArray& V)	{ return V.apply(::tanh); }

#ifdef __ZTC__
/*
 * Zortech requires its own special version of abs(const DoubleArray&)
 * because it uses an inlined version of fabs().  Hence, apply() 
 * cannot be used.
 */
DoubleArray
abs(const DoubleArray& s)
// Absolute value of a DoubleArray --- Zortech version
{
  DoubleArray temp(s.length(),rwUninitialized);
  register double* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    register const double* sp = s.data()+l.start;
    register i = l.length;
    register j = l.stride;
    while (i--) { *dp++ = fabs(*sp);  sp += j; }
  }
  return temp;
}
#else   /* Not Zortech */
DoubleArray	abs(const DoubleArray& V)		{ return V.apply(::fabs); }
#endif	
#ifdef __ZTC__
/*
 * Zortech requires its own special version of sqrt(const DoubleArray&) 
 * because it uses an inlined version of sqrt().  Hence, apply() 
 * cannot be used.
 */
DoubleArray
sqrt(const DoubleArray& s)
// Square root of a DoubleArray --- Zortech version
{
  DoubleArray temp(s.length(),rwUninitialized);
  register double* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    register const double* sp = s.data()+l.start;
    register i = l.length;
    register j = l.stride;
    while (i--) { *dp++ = sqrt(*sp);  sp += j; }
  }
  return temp;
}
#else
DoubleArray	sqrt(const DoubleArray& V)	{ return V.apply(::sqrt); }
#endif

    /**** Norm functions ****/

Double maxNorm(const DoubleArray& x)     // Not really a norm!
{
  Double theMax = 0;
  for(ArrayLooper l(x.length(),x.stride()); l; ++l) {
    const Double *sp = x.data()+l.start;
    for(int i=l.length; i--;) {
      Double entry = sp[i*l.stride];
      if (entry>theMax) theMax=entry;
      if ((-entry)>theMax) theMax=(-entry);
    }
  }
  return theMax;
}

Double frobNorm(const DoubleArray& x)
{
  Double sumSquares = 0;
  for(ArrayLooper l(x.length(),x.stride()); l; ++l) {
    const Double *sp = x.data()+l.start;
    for(int i=l.length; i--;) {
      Double entry = sp[i*l.stride];      
      sumSquares += entry*entry;
    }
  }
  return sqrt(sumSquares);
}


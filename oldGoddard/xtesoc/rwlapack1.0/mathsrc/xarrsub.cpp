> #
> # $Header: /users/rcs/mathsrc/xarrsub.cpp,v 1.4 1993/09/17 18:42:21 alv Exp $
> #
> # $Log: xarrsub.cpp,v $
> # Revision 1.4  1993/09/17  18:42:21  alv
> # (0) -> ((int)0) for benefit of DEC compiler
> #
> # Revision 1.3  1993/09/01  16:27:52  alv
> # now uses rwUninitialized form for simple constructors
> #
> # Revision 1.2  1993/03/22  15:51:31  alv
> # changed PVCS Workfile keyword to RCS ID keyword
> #
> # Revision 1.1  1993/01/22  23:51:07  alv
> # Initial revision
> #
> # 
> define <ClassType>=Array
> include macros
/*
 * Definitions for <C> non-inline subscripting functions
 *
 * Generated from template $Id: xarrsub.cpp,v 1.4 1993/09/17 18:42:21 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1992. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/<a>arr.h"
#include "rw/<a>genmat.h"
#include "rw/<a>vec.h"

<T>Array <C>::operator()(const RWSlice& i, const RWSlice& j, const RWSlice& k)
{
  dimensionCheck(3);
  i.boundsCheck(npts((int)0));
  j.boundsCheck(npts(1));
  k.boundsCheck(npts(2));
  int numDims = 3;        // Calculate number of dimensions in the slice
  if (i.collapse()) numDims--;
  if (j.collapse()) numDims--;
  if (k.collapse()) numDims--;
  IntVec b(3,rwUninitialized);            // Build start position for the slice
  b((int)0) = i.begin();
  b(1) = j.begin();
  b(2) = k.begin();
  IntVec n(numDims,rwUninitialized);      // Build size of slice and stride matrix
  IntGenMat str(3,numDims,0);
  int index=0;
  if (!i.collapse()) { n(index)=i.len(npts((int)0)); str(0,index)=i.stride(); index++; }
  if (!j.collapse()) { n(index)=j.len(npts(1)); str(1,index)=j.stride(); index++; }
  if (!k.collapse()) { n(index)=k.len(npts(2)); str(2,index)=k.stride(); index++; }
  return slice(b,n,str);
}

const <T>Array <C>::operator()(const RWSlice& i, const RWSlice& j, const RWSlice& k) const
{
  return ((<C>*)this)->operator()(i,j,k);  // Call the non-const version
}

<T>Array <C>::operator()(const RWSlice& i, const RWSlice& j, const RWSlice& k, const RWSlice& l)
{
  dimensionCheck(4);
  i.boundsCheck(npts((int)0));
  j.boundsCheck(npts(1));
  k.boundsCheck(npts(2));
  l.boundsCheck(npts(3));
  int numDims = 4;        // Calculate number of dimensions in the slice
  if (i.collapse()) numDims--;
  if (j.collapse()) numDims--;
  if (k.collapse()) numDims--;
  if (l.collapse()) numDims--;
  IntVec b(4,rwUninitialized);            // Build start position for the slice
  b((int)0) = i.begin();
  b(1) = j.begin();
  b(2) = k.begin();
  b(3) = l.begin();
  IntVec n(numDims,rwUninitialized);      // Build size of slice and stride matrix
  IntGenMat str(4,numDims,0);
  int index=0;
  if (!i.collapse()) { n(index)=i.len(npts((int)0)); str(0,index)=i.stride(); index++; }
  if (!j.collapse()) { n(index)=j.len(npts(1)); str(1,index)=j.stride(); index++; }
  if (!k.collapse()) { n(index)=k.len(npts(2)); str(2,index)=k.stride(); index++; }
  if (!l.collapse()) { n(index)=l.len(npts(3)); str(3,index)=l.stride(); index++; }
  return slice(b,n,str);
}

const <T>Array <C>::operator()(const RWSlice& i, const RWSlice& j, const RWSlice& k, const RWSlice& l) const
{
  return ((<C>*)this)->operator()(i,j,k,l);  // Call the non-const version
}

<T>Vec <C>::operator()(const RWSlice& i, int j, int k) {
  return toVec(operator()(i,RWSlice(j),RWSlice(k)));
}

const <T>Vec <C>::operator()(const RWSlice& i, int j, int k) const {
  return ((<C>*)this)->operator()(i,j,k);
}

<T>Vec <C>::operator()(int i, const RWSlice& j, int k) {
  return toVec(operator()(RWSlice(i),j,RWSlice(k)));
}

const <T>Vec <C>::operator()(int i, const RWSlice& j, int k) const {
  return ((<C>*)this)->operator()(i,j,k);
}

<T>Vec <C>::operator()(int i, int j, const RWSlice& k) {
  return toVec(operator()(RWSlice(i),RWSlice(j),k));
}

const <T>Vec <C>::operator()(int i, int j, const RWSlice& k) const {
  return ((<C>*)this)->operator()(i,j,k);
}

<T>Vec <C>::operator()(const RWSlice& i, int j, int k, int l) {
  return toVec(operator()(i,RWSlice(j),RWSlice(k),RWSlice(l)));
}

const <T>Vec <C>::operator()(const RWSlice& i, int j, int k, int l) const {
  return ((<C>*)this)->operator()(i,j,k,l);
}

<T>Vec <C>::operator()(int i, const RWSlice& j, int k, int l) {
  return toVec(operator()(RWSlice(i),j,RWSlice(k),RWSlice(l)));
}

const <T>Vec <C>::operator()(int i, const RWSlice& j, int k, int l) const {
  return ((<C>*)this)->operator()(i,j,k,l);
}

<T>Vec <C>::operator()(int i, int j, const RWSlice& k, int l) {
  return toVec(operator()(RWSlice(i),RWSlice(j),k,RWSlice(l)));
}

const <T>Vec <C>::operator()(int i, int j, const RWSlice& k, int l) const {
  return ((<C>*)this)->operator()(i,j,k,l);
}

<T>Vec <C>::operator()(int i, int j, int k, const RWSlice& l) {
  return toVec(operator()(RWSlice(i),RWSlice(j),RWSlice(k),l));
}

const <T>Vec <C>::operator()(int i, int j, int k, const RWSlice& l) const {
  return ((<C>*)this)->operator()(i,j,k,l);
}

<T>GenMat <C>::operator()(int i, const RWSlice& j, const RWSlice& k) {
  return toGenMat(operator()(RWSlice(i),j,k));
}

const <T>GenMat <C>::operator()(int i, const RWSlice& j, const RWSlice& k) const {
  return ((<C>*)this)->operator()(i,j,k);
}

<T>GenMat <C>::operator()(const RWSlice& i, int j, const RWSlice& k) {
  return toGenMat(operator()(i,RWSlice(j),k));
}

const <T>GenMat <C>::operator()(const RWSlice& i, int j, const RWSlice& k) const {
  return ((<C>*)this)->operator()(i,j,k);
}

<T>GenMat <C>::operator()(const RWSlice& i, const RWSlice& j, int k) {
  return toGenMat(operator()(i,j,RWSlice(k)));
}

const <T>GenMat <C>::operator()(const RWSlice& i, const RWSlice& j, int k) const {
  return ((<C>*)this)->operator()(i,j,k);
}

<T>GenMat <C>::operator()(int i, int j, const RWSlice& k, const RWSlice& l) {
  return toGenMat(operator()(RWSlice(i),RWSlice(j),k,l));
}

const <T>GenMat <C>::operator()(int i, int j, const RWSlice& k, const RWSlice& l) const {
  return ((<C>*)this)->operator()(i,j,k,l);
}

<T>GenMat <C>::operator()(int i, const RWSlice& j, int k, const RWSlice& l) {
  return toGenMat(operator()(RWSlice(i),j,RWSlice(k),l));
}

const <T>GenMat <C>::operator()(int i, const RWSlice& j, int k, const RWSlice& l) const {
  return ((<C>*)this)->operator()(i,j,k,l);
}

<T>GenMat <C>::operator()(int i, const RWSlice& j, const RWSlice& k, int l) {
  return toGenMat(operator()(RWSlice(i),j,k,RWSlice(l)));
}

const <T>GenMat <C>::operator()(int i, const RWSlice& j, const RWSlice& k, int l) const {
  return ((<C>*)this)->operator()(i,j,k,l);
}

<T>GenMat <C>::operator()(const RWSlice& i, int j, int k, const RWSlice& l) {
  return toGenMat(operator()(i,RWSlice(j),RWSlice(k),l));
}

const <T>GenMat <C>::operator()(const RWSlice& i, int j, int k, const RWSlice& l) const {
  return ((<C>*)this)->operator()(i,j,k,l);
}

<T>GenMat <C>::operator()(const RWSlice& i, int j, const RWSlice& k, int l) {
  return toGenMat(operator()(i,RWSlice(j),k,RWSlice(l)));
}

const <T>GenMat <C>::operator()(const RWSlice& i, int j, const RWSlice& k, int l) const {
  return ((<C>*)this)->operator()(i,j,k,l);
}

<T>GenMat <C>::operator()(const RWSlice& i, const RWSlice& j, int k, int l) {
  return toGenMat(operator()(i,j,RWSlice(k),RWSlice(l)));
}

const <T>GenMat <C>::operator()(const RWSlice& i, const RWSlice& j, int k, int l) const {
  return ((<C>*)this)->operator()(i,j,k,l);
}

<T>Array <C>::operator()(int i, const RWSlice& j, const RWSlice& k, const RWSlice& l) {
  return operator()(RWSlice(i),j,k,l);
}

const <T>Array <C>::operator()(int i, const RWSlice& j, const RWSlice& k, const RWSlice& l) const {
  return ((<C>*)this)->operator()(i,j,k,l);
}

<T>Array <C>::operator()(const RWSlice& i, int j, const RWSlice& k, const RWSlice& l) {
  return operator()(i,RWSlice(j),k,l);
}

const <T>Array <C>::operator()(const RWSlice& i, int j, const RWSlice& k, const RWSlice& l) const {
  return ((<C>*)this)->operator()(i,j,k,l);
}

<T>Array <C>::operator()(const RWSlice& i, const RWSlice& j, int k, const RWSlice& l) {
  return operator()(i,j,RWSlice(k),l);
}

const <T>Array <C>::operator()(const RWSlice& i, const RWSlice& j, int k, const RWSlice& l) const {
  return ((<C>*)this)->operator()(i,j,k,l);
}

<T>Array <C>::operator()(const RWSlice& i, const RWSlice& j, const RWSlice& k, int l) {
  return operator()(i,j,k,RWSlice(l));
}

const <T>Array <C>::operator()(const RWSlice& i, const RWSlice& j, const RWSlice& k, int l) const {
  return ((<C>*)this)->operator()(i,j,k,l);
}

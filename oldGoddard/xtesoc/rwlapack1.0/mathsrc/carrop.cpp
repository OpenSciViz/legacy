/*
 * Definitions for various arithmetic operations for DComplexArray
 *
 * Generated from template $Id: xarrop.cpp,v 1.11 1993/09/20 04:08:08 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

/*
 * If a promotable scalar (such as a float) is passed by value, then
 * it will get promoted to, say, a double.  If the address of this is
 * then taken, we have a pointer to a double.  Unfortunately, if
 * this address is then passed to a routine that expects a pointer
 * to a float, cfront thinks it has done its job and quietly passes
 * the address of this promoted variable.
 *
 * This problem occurs when K&R compilers are used as a backend to
 * cfront.  The workaround is to create a temporary of known type
 * on the stack.  This is the "scalar2" variable found throughout
 * this file.
 */

#include "rw/carr.h"
#include "rw/rwzbla.h"	

RCSID("$Header: /users/rcs/mathsrc/xarrop.cpp,v 1.11 1993/09/20 04:08:08 alv Exp $");

/************************************************
 *                                              *
 *              UNARY OPERATORS                 *
 *                                              *
 ************************************************/

DComplexArray operator-(const DComplexArray& s)   // Unary minus
{
  DComplexArray temp(s.length(),rwUninitialized);
  DComplex* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    int n = l.length;
    DComplex* sp = (DComplex*)s.data()+l.start;
    while(n--) { *dp++ = -(*sp); sp+=l.stride; }
  }
  return temp;
}


/************************************************
 *                                              *
 *              BINARY OPERATORS                *
 *               vector - vector                *
 *                                              *
 ************************************************/

DComplexArray operator+(const DComplexArray& u, const DComplexArray& v)
{
  u.lengthCheck(v.length());
  DComplexArray temp(u.length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  DComplex* dp = temp.data();
  for(DoubleArrayLooper l(u.length(),u.stride(),v.stride()); l; ++l) {
    rwz_plvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

DComplexArray operator-(const DComplexArray& u, const DComplexArray& v)
{
  u.lengthCheck(v.length());
  DComplexArray temp(u.length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  DComplex* dp = temp.data();
  for(DoubleArrayLooper l(u.length(),u.stride(),v.stride()); l; ++l) {
    rwz_mivv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

DComplexArray operator*(const DComplexArray& u, const DComplexArray& v)
{
  u.lengthCheck(v.length());
  DComplexArray temp(u.length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  DComplex* dp = temp.data();
  for(DoubleArrayLooper l(u.length(),u.stride(),v.stride()); l; ++l) {
    rwz_muvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

DComplexArray operator/(const DComplexArray& u, const DComplexArray& v)
{
  u.lengthCheck(v.length());
  DComplexArray temp(u.length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  DComplex* dp = temp.data();
  for(DoubleArrayLooper l(u.length(),u.stride(),v.stride()); l; ++l) {
    rwz_dvvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

/************************************************
 *                                              *
 *              BINARY OPERATORS                *
 *               vector - scalar                *
 *                                              *
 ************************************************/

DComplexArray operator+(const DComplexArray& s, DComplex scalar)
{
  DComplexArray temp(s.length(),rwUninitialized);
  DComplex* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    rwz_plvs(l.length, dp, s.data()+l.start, l.stride, &scalar);
    dp += l.length;
  }
  return temp;
}

DComplexArray operator-(DComplex scalar, const DComplexArray& s)
{
  DComplexArray temp(s.length(),rwUninitialized);
  DComplex* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    rwz_misv(l.length, dp, &scalar, s.data()+l.start, l.stride);
    dp += l.length;
  }
  return temp;
}

DComplexArray operator*(const DComplexArray& s, DComplex scalar)
{
  DComplexArray temp(s.length(),rwUninitialized);
  DComplex* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    rwz_muvs(l.length, dp, s.data()+l.start, l.stride, &scalar);
    dp += l.length;
  }
  return temp;
}

DComplexArray operator/(DComplex scalar, const DComplexArray& s)
{
  DComplexArray temp(s.length(),rwUninitialized);
  DComplex* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    rwz_dvsv(l.length, dp, &scalar, s.data()+l.start, l.stride);
    dp += l.length;
  }
  return temp;
}

DComplexArray operator/(const DComplexArray& s, DComplex scalar)
{
  DComplexArray temp(s.length(),rwUninitialized);
  DComplex* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    rwz_dvvs(l.length, dp, s.data()+l.start, l.stride, &scalar);
    dp += l.length;
  }
  return temp;
}

/*
 * Out-of-line versions for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
DComplexArray     operator*(DComplex s, const DComplexArray& V)  {return V*s;}
DComplexArray     operator+(DComplex s, const DComplexArray& V)  {return V+s;}
DComplexArray     operator-(const DComplexArray& V, DComplex s)  {return V+(-s);}
#endif

/************************************************
 *                                              *
 *      ARITHMETIC ASSIGNMENT OPERATORS         *
 *        with other vectors                    *
 *                                              *
 ************************************************/

DComplexArray& DComplexArray::operator+=(const DComplexArray& u)
{
  if (sameDataBlock(u)) { return operator+=(u.deepCopy()); }  // Avoid aliasing
  u.lengthCheck(length());
  for(DoubleArrayLooper l(length(),stride(),u.stride()); l; ++l) {
    rwz_aplvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}


DComplexArray& DComplexArray::operator-=(const DComplexArray& u)
{
  if (sameDataBlock(u)) { return operator-=(u.deepCopy()); }  // Avoid aliasing
  u.lengthCheck(length());
  for(DoubleArrayLooper l(length(),stride(),u.stride()); l; ++l) {
    rwz_amivv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

DComplexArray& DComplexArray::operator*=(const DComplexArray& u)
{
  if (sameDataBlock(u)) { return operator*=(u.deepCopy()); }  // Avoid aliasing
  u.lengthCheck(length());
  for(DoubleArrayLooper l(length(),stride(),u.stride()); l; ++l) {
    rwz_amuvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

DComplexArray& DComplexArray::operator/=(const DComplexArray& u)
{
  if (sameDataBlock(u)) { return operator/=(u.deepCopy()); }  // Avoid aliasing
  u.lengthCheck(length());
  for(DoubleArrayLooper l(length(),stride(),u.stride()); l; ++l) {
    rwz_advvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

/************************************************
 *                                              *
 *      ARITHMETIC ASSIGNMENT OPERATORS         *
 *                with a scalar                 *
 *                                              *
 ************************************************/

DComplexArray& DComplexArray::operator+=(DComplex scalar)
{
  for(ArrayLooper l(length(),stride()); l; ++l) {
    rwz_aplvs(l.length, data()+l.start, l.stride, &scalar);
  }
  return *this;
}

DComplexArray& DComplexArray::operator*=(DComplex scalar)
{
  for(ArrayLooper l(length(),stride()); l; ++l) {
    rwz_amuvs(l.length, data()+l.start, l.stride, &scalar);
  }
  return *this;
}

DComplexArray& DComplexArray::operator/=(DComplex scalar)
{
  for(ArrayLooper l(length(),stride()); l; ++l) {
    rwz_advvs(l.length, data()+l.start, l.stride, &scalar);
  }
  return *this;
}

/************************************************
 *                                              *
 *              LOGICAL OPERATORS               *
 *                                              *
 ************************************************/

RWBoolean DComplexArray::operator==(const DComplexArray& u) const
{
  if(length()!=u.length()) return FALSE;  // They can't be equal if they don't have the same length.
  for(DoubleArrayLooper l(length(),stride(),u.stride()); l; ++l) {
    if (!rwzsame(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2)) {
      return FALSE;
    }
  }
  return TRUE;
}

RWBoolean DComplexArray::operator!=(const DComplexArray& u) const
{
  return !(*this == u);
}

/*
 * Definitions for DoubleGenMatPick
 *
 * Generated from template $Id: xgenpik.cpp,v 1.3 1993/08/10 21:22:18 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */
#include "rw/dgenmat.h"
#include "rw/dgenpik.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"
STARTWRAP
#include <stdio.h>
ENDWRAP

RCSID("$Header: /users/rcs/mathsrc/xgenpik.cpp,v 1.3 1993/08/10 21:22:18 alv Exp $");

void DoubleGenMatPick::operator=(const DoubleGenMat& v)
{
  lengthCheck(v.rows(),v.cols());
  for(int j=cols(); j--;) {
    Double* dest = V.data() + colpick(j)*V.colStride();
    const Double* src = v.data() + j*v.colStride();
    for(int i=rows(); i--;) {
      dest[rowpick(i)*V.rowStride()] = src[i*v.rowStride()];
    }
  }
}

void DoubleGenMatPick::operator=(const DoubleGenMatPick& p)
{
  lengthCheck(p.rows(),p.cols());
  for(int j=cols(); j--;) {
    Double* dest = V.data() + colpick(j)*V.colStride();
    const Double* src = p.V.data() + p.colpick(j)*p.V.colStride();
    for(int i=rows(); i--;) {
      dest[rowpick(i)*V.rowStride()] = src[p.rowpick(i)*p.V.rowStride()];
    }
  }
}

void DoubleGenMatPick::operator=(Double s)
{
  for(int j=cols(); j--;) {
    Double* dest = V.data() + colpick(j)*V.colStride();
    for(int i=rows(); i--;) {
      dest[rowpick(i)*V.rowStride()] = s;
    }
  }
}

DoubleGenMat& DoubleGenMat::operator=(const DoubleGenMatPick& p)
{
  p.lengthCheck(rows(),cols());
  for(int j=cols(); j--;) {
    Double* dest = data() + j*colStride();
    const Double* src = p.V.data() + p.colpick(j)*p.V.colStride();
    for(int i=rows(); i--;) {
      dest[i*rowStride()] = src[p.rowpick(i)*p.V.rowStride()];
    }
  }
  return *this;
}

DoubleGenMat::DoubleGenMat(const DoubleGenMatPick& p)
  : RWMatView(p.rows(),p.cols(),sizeof(double))
{
  Double* dest = data()+nrows*ncols;
  if (p.V.rowStride()==1 && p.rowpick.stride()==1) {
    for(int j=ncols; j--;) {
      const Double* src = p.V.data() + p.colpick(j)*p.V.colStride();
      const int* pdat = p.rowpick.data()+nrows;
      for(int i=nrows; i--;) {
        *(--dest) = src[*(--pdat)];
      }
    }
  } else {
    for(int j=ncols; j--;) {
      const Double* src = p.V.data() + p.colpick(j)*p.V.colStride();
      for(int i=nrows; i--;) {
        *(--dest) = src[p.rowpick(i)*p.V.rowStride()];
      }
    }
  }
}

DoubleGenMatPick DoubleGenMat::pick(const IntVec& ipick, const IntVec& jpick) {
  return DoubleGenMatPick(*this, ipick, jpick);
}

const DoubleGenMatPick DoubleGenMat::pick(const IntVec& ipick, const IntVec& jpick) const {
  return DoubleGenMatPick(*(DoubleGenMat*)this, ipick, jpick);
}

/********************** U T I L I T I E S ***********************/

void DoubleGenMatPick::assertElements() const
{
  int i = rowpick.length();
  while (--i>=0) {
    if (rowpick(i)<0 || rowpick(i)>=V.rows()) {
      RWTHROW( RWBoundsErr( RWMessage(RWMATH_INDEX,	(int)rowpick(i), (unsigned)(V.rows()-1)) ));
    }
  }
  i = colpick.length();
  while (--i>=0) {
    if (colpick(i)<0 || colpick(i)>=V.cols()) {
      RWTHROW( RWBoundsErr( RWMessage(RWMATH_INDEX,	(int)colpick(i), (unsigned)(V.cols()-1)) ));
    }
  }
}

void DoubleGenMatPick::lengthCheck(unsigned m, unsigned n) const
{
  if(rows()!=m || cols()!=n) {
  	 RWTHROW( RWBoundsErr( RWMessage(RWMATH_MNMATCH, (unsigned)rows(), (unsigned)cols(), m, n)));
  }
}

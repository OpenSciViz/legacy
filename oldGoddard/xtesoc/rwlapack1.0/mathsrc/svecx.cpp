/*
 * Type conversion routines for SCharVec
 *
 * $Header: /users/rcs/mathsrc/svecx.cpp,v 1.3 1993/09/01 16:27:42 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: svecx.cpp,v $
 * Revision 1.3  1993/09/01  16:27:42  alv
 * now uses rwUninitialized form for simple constructors
 *
 * Revision 1.2  1993/01/26  17:52:28  alv
 * removed DOS EOF
 *
 * Revision 1.1  1993/01/22  23:51:04  alv
 * Initial revision
 *
 * 
 *    Rev 1.3   17 Oct 1991 09:15:38   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.2   08 Oct 1991 13:45:00   keffer
 * Math V4.0 beta 2
 * 
 *    Rev 1.1   24 Jul 1991 12:52:00   keffer
 * Added pvcs keywords
 *
 */

#include "rw/scvec.h"
#include "rw/ivec.h"

RCSID("$Header: /users/rcs/mathsrc/svecx.cpp,v 1.3 1993/09/01 16:27:42 alv Exp $");

// Convert to an IntVec
// Should be a friend of IntVec, but...
SCharVec::operator IntVec() const
{
  register int n = length();
  IntVec temp(n,rwUninitialized);
  register       int*   dp = temp.data();
  register const SChar* tp = data();
  register int ts = stride();
  while(n--) {
    *dp++ = int(*tp); tp += ts;
  }
  return temp;
}

// narrow from IntVec to signed SCharVec
SCharVec
toChar(const IntVec& v)
{
  register int n = v.length();
  SCharVec temp(n,rwUninitialized);
  register const int*   dp = v.data();
  register       SChar* ip = temp.data();
  register int    vs = v.stride();
  while(n--) {
    *ip++ = SChar(*dp); dp += vs;
  }
  return temp;
}

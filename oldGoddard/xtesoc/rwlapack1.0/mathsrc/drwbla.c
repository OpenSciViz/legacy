/*
 * BLA-like extensions for type double.
 *
 * Generated from template $Id: xrwbla.c,v 1.3 1993/10/11 18:42:24 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/rwdbla.h"
#include "rw/mathdefs.h"

#include <math.h>
#ifdef BSD
#  include <memory.h>
#else
#  include <string.h>
#endif

/****************************************************************
 *								*
 *			UTILITY					*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwdcopy(n, x, incx, y, incy )
  unsigned n;
  double *x, *y;
  int incx, incy;
#else
void FDecl rwdcopy( unsigned n, double* restrict x, int incx, const double* restrict y, int incy)
#endif
{
  if (n==0) return;

  if (incx == 1 && incy == 1) {
    memcpy(x, y, n*sizeof(double));
  }
  else {
    /*
     * Code for increments not equal to one
     */
    while (n--) {
      *x = *y;
      x += incx;
      y += incy;
    }
  }
}

#ifdef RW_KR_ONLY
void rwdset(n, x, incx, scalar)
  unsigned n;
  double *x, *scalar;
  int incx;
#else
void FDecl rwdset(unsigned n, double* restrict x, int incx, const double* restrict scalar)
#endif
{
  if(incx==1)
    while (n--) { *x++ = *scalar; }
  else
    while (n--) { *x = *scalar; x+=incx; }
}

#ifdef RW_KR_ONLY
int rwdsame(n, x, incx, y, incy)
  unsigned n;
  double *x, *y;
  int incx, incy;
#else
int FDecl rwdsame(unsigned n, const double* restrict x, int incx, const double* restrict y, int incy)
#endif
{
  while(n--){
    if( *x != *y ) return 0;
    x += incx;
    y += incy;
  }
  return 1;
}

#ifdef RW_KR_ONLY
int rwdmax(n, x, incx)
  unsigned n;
  double *x;
  int incx;
#else
int FDecl rwdmax(unsigned n, const double* restrict x, int incx)
#endif
{
  double maxv;
  int maxi, i;

  if(n==0) return -1;
  maxv = *x;
  maxi = 0;

  for(i=1; i<n; i++){
    x += incx;
    if(*x > maxv) { maxv = *x; maxi=i; }
  }
  return maxi;
}

#ifdef RW_KR_ONLY
int rwdmin(n, x, incx)
  unsigned n;
  double *x;
  int incx;
#else
int FDecl rwdmin(unsigned n, const double* restrict x, int incx)
#endif
{
  double minv;
  int mini, i;

  if(n==0) return -1;
  minv = *x;
  mini = 0;

  for(i=1; i<n; i++){
    x += incx;
    if(*x < minv) { minv = *x; mini=i; }
  }
  return mini;
}

/****************************************************************
 *								*
 *			DOT PRODUCTS				*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwddot(n, x, incx, y, incy, result)
  unsigned n;
  double *x, *y, *result;
  int incx, incy;
#else
void FDecl rwddot(unsigned n, const double* restrict x, int incx, const double* restrict y, int incy, double* result)
#endif
{

  double sum = 0;
  while(n--){
    sum += *x * *y;
    x += incx;
    y += incy;
  }
  *result = (double)sum;
}


/****************************************************************
 *								*
 *		VECTOR --- VECTOR ASSIGNMENT OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwd_aplvv(n, x, incx, y, incy)
  unsigned n;
  double *x, *y;
  int incx, incy;
#else
void FDecl rwd_aplvv(unsigned n, double* restrict x, int incx, const double* restrict y, int incy)
#endif
{
  while(n--){
    *x += *y;
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rwd_amivv(n, x, incx, y, incy)
  unsigned n;
  double *x, *y;
  int incx, incy;
#else
void FDecl rwd_amivv(unsigned n, double* restrict x, int incx, const double* restrict y, int incy)
#endif
{
  while(n--){
    *x -= *y;
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rwd_amuvv(n, x, incx, y, incy)
  unsigned n;
  double *x, *y;
  int incx, incy;
#else
void FDecl rwd_amuvv(unsigned n, double* restrict x, int incx, const double* restrict y, int incy)
#endif
{
  while(n--){
    *x *= *y;
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rwd_advvv(n, x, incx, y, incy)
  unsigned n;
  double *x, *y;
  int incx, incy;
#else
void FDecl rwd_advvv(unsigned n, double* restrict x, int incx, const double* restrict y, int incy)
#endif
{
  while(n--){
    *x /= *y;
    x += incx;
    y += incy;
  }
}

/****************************************************************
 *								*
 *		VECTOR --- SCALAR ASSIGNMENT OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwd_aplvs(n, x, incx, scalar)
  unsigned n;
  double *x, *scalar;
  int incx;
#else
void FDecl rwd_aplvs(unsigned n, double* restrict x, int incx, const double* restrict scalar)
#endif
{
  while(n--) {
    *x += *scalar;
    x += incx;
  }
}

#ifdef RW_KR_ONLY
void rwd_amuvs(n, x, incx, scalar)
  unsigned n;
  double *x, *scalar;
  int incx;
#else
void FDecl rwd_amuvs(unsigned n, double* restrict x, int incx, const double* restrict scalar)
#endif
{
  while(n--) {
    *x *= *scalar;
    x += incx;
  }
}


/****************************************************************
 *								*
 *		VECTOR --- VECTOR BINARY OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwd_plvv(n, z, x, incx, y, incy)
  unsigned n;
  double *z, *x, *y;
  int incx, incy;
#else
void FDecl rwd_plvv(unsigned n, double* restrict z, const double* restrict x, int incx, const double* restrict y, int incy)
#endif
{
  while(n--){
    *z++ = *x + *y;
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rwd_mivv(n, z, x, incx, y, incy)
  unsigned n;
  double *z, *x, *y;
  int incx, incy;
#else
void FDecl rwd_mivv(unsigned n, double* restrict z, const double* restrict x, int incx, const double* restrict y, int incy)
#endif
{
  while(n--){
    *z++ = *x - *y;
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rwd_muvv(n, z, x, incx, y, incy)
  unsigned n;
  double *z, *x, *y;
  int incx, incy;
#else
void FDecl rwd_muvv(unsigned n, double* restrict z, const double* restrict x, int incx, const double* restrict y, int incy)
#endif
{
  while(n--){
    *z++ = *x * *y;
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rwd_dvvv(n, z, x, incx, y, incy)
  unsigned n;
  double *z, *x, *y;
  int incx, incy;
#else
void FDecl rwd_dvvv(unsigned n, double* restrict z, const double* restrict x, int incx, const double* restrict y, int incy)
#endif
{
  while(n--){
    *z++ = *x / *y;
    x += incx;
    y += incy;
  }
}

/****************************************************************
 *								*
 *		VECTOR --- SCALAR BINARY OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwd_plvs(n, z, x, incx, scalar)
  unsigned n;
  double *z, *x, *scalar;
  int incx;
#else
void FDecl rwd_plvs(unsigned n, double* restrict z, const double* restrict x, int incx, const double* restrict scalar)
#endif
{
  while(n--){
    *z++ = *x + *scalar;
    x += incx;
  }
}

#ifdef RW_KR_ONLY
void rwd_muvs(n, z, x, incx, scalar)
  unsigned n;
  double *z, *x, *scalar;
  int incx;
#else
void FDecl rwd_muvs(unsigned n, double* restrict z, const double* restrict x, int incx, const double* restrict scalar)
#endif
{
  while(n--){
    *z++ = *x * *scalar;
    x += incx;
  }
}

#ifdef RW_KR_ONLY
void rwd_dvvs(n, z, x, incx, scalar)
  unsigned n;
  double *z, *x, *scalar;
  int incx;
#else
void FDecl rwd_dvvs(unsigned n, double* restrict z, const double* restrict x, int incx, const double* restrict scalar)
#endif
{
  while(n--){
    *z++ = *x / *scalar;
    x += incx;
  }
}

/****************************************************************
 *								*
 *		SCALAR --- VECTOR BINARY OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwd_misv(n, z, scalar, x, incx)
  unsigned n;
  double *z, *scalar, *x;
  int incx;
#else
void FDecl rwd_misv(unsigned n, double* restrict z, const double* scalar, const double* restrict x, int incx)
#endif
{
  while(n--){
    *z++ = *scalar - *x;
    x += incx;
  }
}

#ifdef RW_KR_ONLY
void rwd_dvsv(n, z, scalar, x, incx)
  unsigned n;
  double *z, *scalar, *x;
  int incx;
#else
void FDecl rwd_dvsv(unsigned n, double* restrict z, const double* scalar, const double* restrict x, int incx)
#endif
{
  while(n--){
    *z++ = *scalar / *x;
    x += incx;
  }
}


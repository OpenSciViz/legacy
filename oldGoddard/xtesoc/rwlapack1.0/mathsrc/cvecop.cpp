/*
 * Definitions for various arithmetic operations
 *
 * Generated from template $Id: xvecop.cpp,v 1.7 1993/09/01 16:28:02 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

/*
 * If a promotable scalar (such as a float) is passed by value, then
 * it will get promoted to, say, a double.  If the address of this is
 * then taken, we have a pointer to a double.  Unfortunately, if
 * this address is then passed to a routine that expects a pointer
 * to a float, cfront thinks it has done its job and quietly passes
 * the address of this promoted variable.
 *
 * This problem occurs when K&R compilers are used as a backend to
 * cfront.  The workaround is to create a temporary of known type
 * on the stack.  This is the "scalar2" variable found throughout
 * this file.
 */

#include "rw/cvec.h"
#include "rw/rwzbla.h"	

RCSID("$Header: /users/rcs/mathsrc/xvecop.cpp,v 1.7 1993/09/01 16:28:02 alv Exp $");

/************************************************
 *						*
 *		UNARY OPERATORS			*
 *						*
 ************************************************/

// Unary minus on a DComplexVec
DComplexVec
operator-(const DComplexVec& s)
{
  unsigned i = s.length();
  DComplexVec temp(i,rwUninitialized);
  register DComplex* sp = (DComplex*)s.data();
  register DComplex* dp = temp.data();
  register j = s.stride();
  while (i--) { *dp++ = -(*sp);  sp += j; }
  return temp;
}


/************************************************
 *						*
 *		BINARY OPERATORS		*
 *		 vector - vector		*
 *						*
 ************************************************/

// Binary add element-by-element
DComplexVec
operator+(const DComplexVec& u, const DComplexVec& v)
{
  unsigned i = v.length();
  u.lengthCheck(i);
  DComplexVec temp(i,rwUninitialized);
  rwz_plvv(i, temp.data(), u.data(), u.stride(), v.data(), v.stride());
  return temp;
}

// Binary subtract element-by-element
DComplexVec
operator-(const DComplexVec& u, const DComplexVec& v)
{
  unsigned i = v.length();
  u.lengthCheck(i);
  DComplexVec temp(i,rwUninitialized);
  rwz_mivv(i, temp.data(), u.data(), u.stride(), v.data(), v.stride());
  return temp;
}

// Binary multiply element-by-element
DComplexVec
operator*(const DComplexVec& u, const DComplexVec& v)
{
  unsigned i = v.length();
  u.lengthCheck(i);
  DComplexVec temp(i,rwUninitialized);
  rwz_muvv(i, temp.data(), u.data(), u.stride(), v.data(), v.stride());
  return temp;
}

// Binary divide element-by-element
DComplexVec
operator/(const DComplexVec& u, const DComplexVec& v)
{
  unsigned i = v.length();
  u.lengthCheck(i);
  DComplexVec temp(i,rwUninitialized);
  rwz_dvvv(i, temp.data(), u.data(), u.stride(), v.data(), v.stride());
  return temp;
}

/************************************************
 *						*
 *		BINARY OPERATORS		*
 *		 vector - scalar		*
 *						*
 ************************************************/

// Add a scalar
DComplexVec
operator+(const DComplexVec& s, DComplex scalar)
{
  unsigned i = s.length();
  DComplexVec temp(i,rwUninitialized);
  rwz_plvs(i, temp.data(), s.data(), s.stride(), &scalar);
  return temp;
}


// Subtract from a scalar
DComplexVec
operator-(DComplex scalar, const DComplexVec& s)
{
  unsigned i = s.length();
  DComplexVec temp(i,rwUninitialized);
  rwz_misv(i, temp.data(), &scalar, s.data(), s.stride());
  return temp;
}

// Multiply by a scalar
DComplexVec
operator*(const DComplexVec& s, DComplex scalar)
{
  unsigned i = s.length();
  DComplexVec temp(i,rwUninitialized);
  rwz_muvs(i, temp.data(), s.data(), s.stride(), &scalar);
  return temp;
}

// Divide into a scalar
DComplexVec
operator/(DComplex scalar, const DComplexVec& s)
{
  unsigned i = s.length();
  DComplexVec temp(i,rwUninitialized);
  rwz_dvsv(i, temp.data(), &scalar, s.data(), s.stride());
  return temp;
}

// Divide by a scalar
DComplexVec
operator/(const DComplexVec& s, DComplex scalar)
{
  unsigned i = s.length();
  DComplexVec temp(i,rwUninitialized);
  rwz_dvvs(i, temp.data(), s.data(), s.stride(), &scalar);
  return temp;
}

/*
 * Out-of-line versions for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
DComplexVec	operator*(DComplex s, const DComplexVec& V)	{return V*s;}
DComplexVec	operator+(DComplex s, const DComplexVec& V)	{return V+s;}
DComplexVec	operator-(const DComplexVec& V, DComplex s)  {return V+(-s);}
#endif

/************************************************
 *						*
 *	ARITHMETIC ASSIGNMENT OPERATORS		*
 *	  with other vectors			*
 *						*
 ************************************************/

DComplexVec&
DComplexVec::operator+=(const DComplexVec& u)
{
  if (sameDataBlock(u)) { return operator+=(u.deepCopy()); }  // Avoid aliasing
  unsigned i = u.length();
  lengthCheck(i);
  rwz_aplvv(i, data(), stride(), u.data(), u.stride());
  return *this;
}

DComplexVec&
DComplexVec::operator-=(const DComplexVec& u)
{
  if (sameDataBlock(u)) { return operator-=(u.deepCopy()); }  // Avoid aliasing
  unsigned i = u.length();
  lengthCheck(i);
  rwz_amivv(i, data(), stride(), u.data(), u.stride());
  return *this;
}

DComplexVec&
DComplexVec::operator*=(const DComplexVec& u)
{
  if (sameDataBlock(u)) { return operator*=(u.deepCopy()); }  // Avoid aliasing
  unsigned i = u.length();
  lengthCheck(i);
  rwz_amuvv(i, data(), stride(), u.data(), u.stride());
  return *this;
}

DComplexVec&
DComplexVec::operator/=(const DComplexVec& u)
{
  if (sameDataBlock(u)) { return operator/=(u.deepCopy()); }  // Avoid aliasing
  unsigned i = u.length();
  lengthCheck(i);
  rwz_advvv(i, data(), stride(), u.data(), u.stride());
  return *this;
}

/************************************************
 *						*
 *	ARITHMETIC ASSIGNMENT OPERATORS		*
 *		  with a scalar			*
 *						*
 ************************************************/

DComplexVec&
DComplexVec::operator+=(DComplex scalar)
{
  rwz_aplvs(length(), data(), stride(), &scalar);
  return *this;
}


DComplexVec&
DComplexVec::operator*=(DComplex scalar)
{
  rwz_amuvs(length(), data(), stride(), &scalar);
  return *this;
}

DComplexVec&
DComplexVec::operator/=(DComplex scalar)
{
  rwz_advvs(length(), data(), stride(), &scalar);
  return *this;
}

/************************************************
 *						*
 *		LOGICAL OPERATORS		*
 *						*
 ************************************************/

RWBoolean
DComplexVec::operator==(const DComplexVec& u) const
{
  unsigned i = length();
  if(i != u.length()) return FALSE;  // They can't be equal if they don't have the same length.
  return rwzsame(i, data(), stride(), u.data(), u.stride());
}

RWBoolean
DComplexVec::operator!=(const DComplexVec& u) const
{
  return !(*this == u);
}


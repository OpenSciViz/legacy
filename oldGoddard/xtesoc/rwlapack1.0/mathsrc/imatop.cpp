/*
 * Definitions for various arithmetic operations for IntGenMat
 *
 * Generated from template $Id: xmatop.cpp,v 1.10 1993/09/01 16:27:57 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

/*
 * If a promotable scalar (such as a float) is passed by value, then
 * it will get promoted to, say, a double.  If the address of this is
 * then taken, we have a pointer to a double.  Unfortunately, if
 * this address is then passed to a routine that expects a pointer
 * to a float, cfront thinks it has done its job and quietly passes
 * the address of this promoted variable.
 *
 * This problem occurs when K&R compilers are used as a backend to
 * cfront.  The workaround is to create a temporary of known type
 * on the stack.  This is the "scalar2" variable found throughout
 * this file.
 */

#include "rw/igenmat.h"
#include "rw/rwibla.h"	

RCSID("$Header: /users/rcs/mathsrc/xmatop.cpp,v 1.10 1993/09/01 16:27:57 alv Exp $");

/************************************************
 *                                              *
 *              UNARY OPERATORS                 *
 *                                              *
 ************************************************/

IntGenMat operator-(const IntGenMat& s)   // Unary minus
{
  IntGenMat temp(s.rows(),s.cols(),rwUninitialized);
  int* dp = temp.data();
  for(MatrixLooper l(s.rows(),s.cols(),s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    int n = l.length;
    int* sp = (int*)s.data()+l.start;
    while(n--) { *dp++ = -(*sp); sp+=l.stride; }
  }
  return temp;
}

// Unary prefix increment on a IntGenMat; (i.e. ++a)
IntGenMat& IntGenMat::operator++()
{
  for(MatrixLooper l(rows(),cols(),rowStride(),colStride()); l; ++l) {
    int n = l.length;
    int* sp = data()+l.start;
    while(n--) { ++(*sp); sp+=l.stride; }
  }
  return *this;
}

// Unary prefix decrement on a IntGenMat (i.e., --a)
IntGenMat&
IntGenMat::operator--()
{
  for(MatrixLooper l(rows(),cols(),rowStride(),colStride()); l; ++l) {
    int n = l.length;
    int* sp = data()+l.start;
    while(n--) { --(*sp); sp+=l.stride; }
  }
  return *this;
}

/************************************************
 *                                              *
 *              BINARY OPERATORS                *
 *               vector - vector                *
 *                                              *
 ************************************************/

IntGenMat operator+(const IntGenMat& u, const IntGenMat& v)
{
  unsigned m = v.rows();
  unsigned n = v.cols();
  u.lengthCheck(m,n);
  IntGenMat temp(m,n,rwUninitialized);
  int* dp = temp.data();
  for(DoubleMatrixLooper l(m,n,u.rowStride(),u.colStride(),v.rowStride(),v.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rwi_plvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

IntGenMat operator-(const IntGenMat& u, const IntGenMat& v)
{
  unsigned m = v.rows();
  unsigned n = v.cols();
  u.lengthCheck(m,n);
  IntGenMat temp(m,n,rwUninitialized);
  int* dp = temp.data();
  for(DoubleMatrixLooper l(m,n,u.rowStride(),u.colStride(),v.rowStride(),v.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rwi_mivv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

IntGenMat operator*(const IntGenMat& u, const IntGenMat& v)
{
  unsigned m = v.rows();
  unsigned n = v.cols();
  u.lengthCheck(m,n);
  IntGenMat temp(m,n,rwUninitialized);
  int* dp = temp.data();
  for(DoubleMatrixLooper l(m,n,u.rowStride(),u.colStride(),v.rowStride(),v.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rwi_muvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

IntGenMat operator/(const IntGenMat& u, const IntGenMat& v)
{
  unsigned m = v.rows();
  unsigned n = v.cols();
  u.lengthCheck(m,n);
  IntGenMat temp(m,n,rwUninitialized);
  int* dp = temp.data();
  for(DoubleMatrixLooper l(m,n,u.rowStride(),u.colStride(),v.rowStride(),v.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rwi_dvvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

/************************************************
 *                                              *
 *              BINARY OPERATORS                *
 *               vector - scalar                *
 *                                              *
 ************************************************/

IntGenMat operator+(const IntGenMat& s, int scalar)
{
  unsigned m = s.rows();
  unsigned n = s.cols();
  IntGenMat temp(m,n,rwUninitialized);
  int* dp = temp.data();
  for(MatrixLooper l(m,n,s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rwi_plvs(l.length, dp, s.data()+l.start, l.stride, &scalar);
    dp += l.length;
  }
  return temp;
}

IntGenMat operator-(int scalar, const IntGenMat& s)
{
  unsigned m = s.rows();
  unsigned n = s.cols();
  IntGenMat temp(m,n,rwUninitialized);
  int* dp = temp.data();
  for(MatrixLooper l(m,n,s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rwi_misv(l.length, dp, &scalar, s.data()+l.start, l.stride);
    dp += l.length;
  }
  return temp;
}

IntGenMat operator*(const IntGenMat& s, int scalar)
{
  unsigned m = s.rows();
  unsigned n = s.cols();
  IntGenMat temp(m,n,rwUninitialized);
  int* dp = temp.data();
  for(MatrixLooper l(m,n,s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rwi_muvs(l.length, dp, s.data()+l.start, l.stride, &scalar);
    dp += l.length;
  }
  return temp;
}

IntGenMat operator/(int scalar, const IntGenMat& s)
{
  unsigned m = s.rows();
  unsigned n = s.cols();
  IntGenMat temp(m,n,rwUninitialized);
  int* dp = temp.data();
  for(MatrixLooper l(m,n,s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rwi_dvsv(l.length, dp, &scalar, s.data()+l.start, l.stride);
    dp += l.length;
  }
  return temp;
}

IntGenMat operator/(const IntGenMat& s, int scalar)
{
  unsigned m = s.rows();
  unsigned n = s.cols();
  IntGenMat temp(m,n,rwUninitialized);
  int* dp = temp.data();
  for(MatrixLooper l(m,n,s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rwi_dvvs(l.length, dp, s.data()+l.start, l.stride, &scalar);
    dp += l.length;
  }
  return temp;
}

/*
 * Out-of-line versions for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
IntGenMat     operator*(int s, const IntGenMat& V)  {return V*s;}
IntGenMat     operator+(int s, const IntGenMat& V)  {return V+s;}
IntGenMat     operator-(const IntGenMat& V, int s)  {return V+(-s);}
#endif

/************************************************
 *                                              *
 *      ARITHMETIC ASSIGNMENT OPERATORS         *
 *        with other vectors                    *
 *                                              *
 ************************************************/

IntGenMat& IntGenMat::operator+=(const IntGenMat& u)
{
  if (sameDataBlock(u)) { return operator+=(u.copy()); }  // Avoid aliasing
  u.lengthCheck(rows(),cols());
  for(DoubleMatrixLooper l(rows(),cols(),rowStride(),colStride(),u.rowStride(),u.colStride()); l; ++l) {
    rwi_aplvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

IntGenMat& IntGenMat::operator-=(const IntGenMat& u)
{
  if (sameDataBlock(u)) { return operator-=(u.copy()); }  // Avoid aliasing
  u.lengthCheck(rows(),cols());
  for(DoubleMatrixLooper l(rows(),cols(),rowStride(),colStride(),u.rowStride(),u.colStride()); l; ++l) {
    rwi_amivv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

IntGenMat& IntGenMat::operator*=(const IntGenMat& u)
{
  if (sameDataBlock(u)) { return operator*=(u.copy()); }  // Avoid aliasing
  u.lengthCheck(rows(),cols());
  for(DoubleMatrixLooper l(rows(),cols(),rowStride(),colStride(),u.rowStride(),u.colStride()); l; ++l) {
    rwi_amuvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

IntGenMat& IntGenMat::operator/=(const IntGenMat& u)
{
  if (sameDataBlock(u)) { return operator/=(u.copy()); }  // Avoid aliasing
  u.lengthCheck(rows(),cols());
  for(DoubleMatrixLooper l(rows(),cols(),rowStride(),colStride(),u.rowStride(),u.colStride()); l; ++l) {
    rwi_advvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

/************************************************
 *                                              *
 *      ARITHMETIC ASSIGNMENT OPERATORS         *
 *                with a scalar                 *
 *                                              *
 ************************************************/

IntGenMat& IntGenMat::operator+=(int scalar)
{
  for(MatrixLooper l(rows(),cols(),rowStride(),colStride()); l; ++l) {
    rwi_aplvs(l.length, data()+l.start, l.stride, &scalar);
  }
  return *this;
}

IntGenMat& IntGenMat::operator*=(int scalar)
{
  for(MatrixLooper l(rows(),cols(),rowStride(),colStride()); l; ++l) {
    rwi_amuvs(l.length, data()+l.start, l.stride, &scalar);
  }
  return *this;
}

IntGenMat& IntGenMat::operator/=(int scalar)
{
  for(MatrixLooper l(rows(),cols(),rowStride(),colStride()); l; ++l) {
    rwi_advvs(l.length, data()+l.start, l.stride, &scalar);
  }
  return *this;
}

/************************************************
 *                                              *
 *              LOGICAL OPERATORS               *
 *                                              *
 ************************************************/

RWBoolean IntGenMat::operator==(const IntGenMat& u) const
{
  if(rows()!=u.rows() || cols()!=u.cols()) return FALSE;  // They can't be equal if they don't have the same length.
  for(DoubleMatrixLooper l(rows(),cols(),rowStride(),colStride(),u.rowStride(),u.colStride()); l; ++l) {
    if (!rwisame(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2)) {
      return FALSE;
    }
  }
  return TRUE;
}

RWBoolean IntGenMat::operator!=(const IntGenMat& u) const
{
  return !(*this == u);
}

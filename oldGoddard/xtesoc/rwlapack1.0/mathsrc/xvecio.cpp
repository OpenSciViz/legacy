> #
> # $Header: /users/rcs/mathsrc/xvecio.cpp,v 1.6 1993/08/10 21:22:18 alv Exp $
> #
> # $Log: xvecio.cpp,v $
> # Revision 1.6  1993/08/10  21:22:18  alv
> # now uses Tools v6 compatible error handling
> #
> # Revision 1.5  1993/06/18  19:23:50  alv
> # removed reference to static numPerLine for multi-thread safety
> #
> # Revision 1.4  1993/03/22  15:51:31  alv
> # changed PVCS Workfile keyword to RCS ID keyword
> #
> # Revision 1.3  1993/01/26  23:59:08  alv
> # fixed rcs comment leader problem
> #
> # Revision 1.2  1993/01/26  17:52:28  alv
> # removed DOS EOF
> #
> # Revision 1.1  1993/01/22  23:51:09  alv
> # Initial revision
> #
> # 
> #    Rev 1.8   15 Nov 1991 09:36:28   keffer
> # Removed RWMATHERR macro
> # 
> #    Rev 1.7   17 Oct 1991 09:15:14   keffer
> # Changed include path to <rw/xxx.h>
> # 
> #    Rev 1.5   20 Sep 1991 18:33:14   keffer
> # Corrected error in boundsCheck()
> # 
> #    Rev 1.4   31 Aug 1991 22:19:22   keffer
> # Ported to Glockenspiel; storeOn() now uses explicit DComplex val
> # 
> #    Rev 1.3   26 Jul 1991 13:48:26   keffer
> # Complex now tests for whether IMAG_LEADS has been defined.
> # 
> define <ClassType>=Vec
> include macros
/*
 * Definitions for <T>Vec I/O
 *
 * Generated from template $Id: xvecio.cpp,v 1.6 1993/08/10 21:22:18 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/<a>vec.h"
#include "rw/rwfile.h"
#include "rw/vstream.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"
STARTWRAP
#include <stdio.h>
#include <ctype.h>
ENDWRAP

> if <C>==SCharVec || <C>==UCharVec
// read and write chars as integers:
#  define CAST Int
#  define CTYPE char
> else
#  define CAST <T>
#  define CTYPE <T>
> endif

RCSID("$Header: /users/rcs/mathsrc/xvecio.cpp,v 1.6 1993/08/10 21:22:18 alv Exp $");

const int            default_resize        = 128;
const unsigned short versionID             = 2;		// Formatting version number

/************************* I / O   R O U T I N E S ***********************/

void
<T>Vec::printOn(ostream& s, unsigned numberPerLine) const 
{
  s << "[\n";
  for(register int n = 0; n < length(); n++){
    if (n) { if (numberPerLine==0 || n%numberPerLine) {s<<" ";} else {s<<"\n";} }
    CAST item = (*this)(n);	// Can't use s<<(*this)(n) due to cfront bug
    s << item;
  }
  s << "\n]";
}

void
<T>Vec::scanFrom(istream& s)
{
  CAST item;
  register int nextspace = 0;
  char c = 0;		// The first character read from the stream
  
  // Munch through any leading white space:
  do {
    s.get(c);
  } while (isspace(c)) ;
  
  if (c != '[') {
    // Scan input stream, resizing as necessary.
    // Keep scanning till we can't scan no mo'
    s.putback(c);
    while (s >> item) {
      if (nextspace >= length()) resize(length()+default_resize);
      (*this)(nextspace++) = item;
    }
  }
  else {  // Scan input stream, stop scanning at the matching ']' character
    
    do {
      s.get(c);			// Munch through any white space after the leading '['
    } while (isspace(c));

    while ( s && (c != ']')  ) {
       s.putback(c);
       if (s >> item) {
           if (nextspace >= length()) resize(length()+default_resize);
           (*this)(nextspace++) = item;
       }
       do {
	 s.get(c);
       } while (isspace(c)) ;

     }
    }
  // Trim to fit
  if(nextspace != length())resize(nextspace);
}

ostream&
operator<<(ostream& s, const <T>Vec& v)
{
  v.printOn(s); return s;
}
istream&
operator>>(istream& s, <T>Vec& v)
{
  v.scanFrom(s); return s;
}

void
<T>Vec::saveOn(RWFile& file) const 
{
  unsigned len = length();
  file.Write(versionID);
  file.Write(len);

> if <R/C>==Complex
#  if COMPLEX_PACKS

  if( stride() == 1 ){
    // This is the normal case and things can be done very efficiently.
    // Write the N point complex vector as a 2N point real vector:
    const <P>* dataStart = (const <P>*) data();
    file.Write(dataStart, 2*len);
  }
  else {
    for(int n = 0; n < len; n++) {
      DComplex val = (*this)(n);
      file.Write( real(val) );
      file.Write( imag(val) );
    }
  }

#  else

  /*
   * Complex vector, but it is not packed.
   * Have to write it out, element-by-element
   */

  for(register int n = 0; n < len; n++) {
    DComplex val = (*this)(n);
    file.Write( real(val) );
    file.Write( imag(val) );
  }

#  endif

> else
> # Not a complex vector

  if( stride() == 1 ){
    const CTYPE* dataStart = (const CTYPE*)data();
    file.Write(dataStart, len);
  }
  else {
    for(register int n = 0; n < len; n++){
      CTYPE item = (*this)(n);
      file.Write(item);
    }
  }

> endif
> # end check of complex
}

void
<T>Vec::saveOn(RWvostream& s) const 
{
  unsigned len = length();
  s << versionID;		// Save the version number
  s << len;			// Save the vector length

> if <R/C>==Complex
#  if COMPLEX_PACKS && !defined(IMAG_LEADS)

  if( stride() == 1 ){
    // This is the normal case and things can be done very efficiently.
    // Write the N point complex vector as a 2N point real vector:
    const <P>* dataStart = (const <P>*) data();
    s.put(dataStart, 2*len);
  }
  else {
    for(register int n = 0; n < len; n++) {
      DComplex val = (*this)(n);
      s << real( val );
      s << imag( val );
    }
  }

#  else	

  /*
   * Complex vector, but either it is not packed, or real does not lead.
   * Have to write it out, element-by-element
   */

  for(register int n = 0; n < len; n++) {
    s << real( (*this)(n) );
    s << imag( (*this)(n) );
  }

#  endif

> else
> # Not complex

  if( stride() == 1 ){
    const CTYPE* dataStart = (const CTYPE*)data();
    s.put(dataStart, len);
  }
  else {
    for(register int n = 0; n < len; n++){
      CTYPE item = (*this)(n);
      s << item;
    }
  }
> endif
}

void
<T>Vec::restoreFrom(RWFile& file)
{
  // Get and check the version number
  unsigned short ID;
  file.Read(ID);

  if( ID!=versionID && ID!=340 ){
    versionErr((int)versionID, (int)ID);
  }

  // Get the length and resize to it:
  unsigned len;
  file.Read(len);
  resize(len);

> if <R/C>==Complex
> # Complex vector
#  if COMPLEX_PACKS

  // This is the normal case.  We can read the N point complex
  // vector as a 2N real vector.
  <P>* dataStart = (<P>*) data();
  file.Read(dataStart, 2*len);

#  else

  // Complex doesn't pack.  Have to read it in element-by-element
  <P> tmpReal, tmpImag;
  for(register int n=0; n<len; n++){
    file.Read(tmpReal);
    file.Read(tmpImag);
    (*this)(n) = <T>(tmpReal, tmpImag);
  }

#  endif

> else
> # Not complex

  // Scoop up the whole vector in one read:
  CTYPE* dataStart = (CTYPE*) data();
  file.Read(dataStart, len);
> endif
}

void
<T>Vec::restoreFrom(RWvistream& s)
{
  // Get and check the version number
  unsigned short ID;
  s >> ID;

  if( ID!=versionID && ID!=340 ){
    RWTHROW( RWExternalErr( RWMessage(RWMATH_BADVERNO, (int)versionID, (int)ID) ));
  }

  // Get the length and resize to it:
  unsigned len;
  s >> len;
  resize(len);

> if <R/C>==Complex
#  if COMPLEX_PACKS

  // This is the normal case.  We can read the N point complex
  // vector as a 2N real vector.
  <P>* dataStart = (<P>*) data();
  s.get(dataStart, 2*len);

#else

  // Complex doesn't pack.  Have to read it in element-by-element
  <P> tmpReal, tmpImag;
  for(register int n=0; n<len; n++){
    s >> tmpReal;
    s >> tmpImag;
    (*this)(n) = <T>(tmpReal, tmpImag);
  }

#  endif

> else
> # Not complex

  // Scoop up the whole vector in one read:
  CTYPE* dataStart = (CTYPE*) data();
  s.get(dataStart, len);
> endif
}

unsigned
<T>Vec::binaryStoreSize() const
{
  // Total storage requirements = Number of elements times their size, 
  // plus space for the vector length and ID number:
  return length()*sizeof(<T>) + sizeof(unsigned) + sizeof(versionID);
}

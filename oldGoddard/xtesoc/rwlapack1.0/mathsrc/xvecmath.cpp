> #
> # $Header: /users/rcs/mathsrc/xvecmath.cpp,v 1.7 1993/09/16 17:16:56 alv Exp $
> #
> # $Log: xvecmath.cpp,v $
> # Revision 1.7  1993/09/16  17:16:56  alv
> # added norm functions
> #
> # Revision 1.6  1993/09/01  16:28:01  alv
> # now uses rwUninitialized form for simple constructors
> #
> # Revision 1.5  1993/08/10  21:22:18  alv
> # now uses Tools v6 compatible error handling
> #
> # Revision 1.4  1993/08/04  22:32:37  dealys
> # ported to MPW C++ 3.3
> #
> # Revision 1.3  1993/03/22  15:51:31  alv
> # changed PVCS Workfile keyword to RCS ID keyword
> #
> # Revision 1.2  1993/03/12  20:01:12  alv
> # removed refs to BCC_STRUCT_POINTER_BUG
> #
> # Revision 1.1  1993/01/22  23:51:09  alv
> # Initial revision
> #
> # 
> #    Rev 1.13   15 Nov 1991 09:36:28   keffer
> # Removed RWMATHERR macro
> # 
> #    Rev 1.12   18 Oct 1991 12:35:34   keffer
> # sqrt(const <C>&) does not use apply() for Zortech.
> # 
> #    Rev 1.11   17 Oct 1991 09:15:36   keffer
> # Changed include path to <rw/xxx.h>
> # 
> #    Rev 1.10   08 Oct 1991 18:05:56   keffer
> # Changed invocation of function "f" in apply() to (*f)().
> # 
> #    Rev 1.8   24 Sep 1991 18:54:20   keffer
> # Ported to Zortech V3.0
> # 
> #    Rev 1.7   31 Aug 1991 22:18:42   keffer
> # Changed min() to minIndex(); max to maxIndex()
> # 
> #    Rev 1.6   04 Aug 1991 10:22:52   keffer
> # Changed definition of conjDot() to return u* v, rather than u v*.
> # 
> #    Rev 1.5   03 Aug 1991 14:11:10   keffer
> # Ported to Zortech --- Now includes <stdlib.h>
> # 
> #    Rev 1.4   27 Jul 1991 13:11:58   keffer
> # Now accumulates sums in double precision for dot() sum() and variance().
> # 
> #    Rev 1.3   26 Jul 1991 13:50:50   keffer
> # Complex now tests for whether IMAG_LEADS has been defined, rather
> # than REAL_LEADS.  This protects against the more usual case.
> # 
> #    Rev 1.2   26 Jul 1991 10:22:30   keffer
> # Worked around Borland postincrement pointer to aggregate bug
> # that affected delta() and pow().  
> # 
> define <ClassType>=Vec
> include macros
/*
 * Math definitions
 *
 * Generated from template $Id: xvecmath.cpp,v 1.7 1993/09/16 17:16:56 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/<a>vec.h"
#include "rw/rw<bla>bla.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"
> if <C>==IntVec || <C>==SCharVec
STARTWRAP
#include <stdlib.h>	/* Looking for abs(int) */
ENDWRAP
> endif

> if <R/C>==Complex
// Complexes won't fit in a register
#define REGISTER
> else
#define REGISTER register
> endif

RCSID("$Header: /users/rcs/mathsrc/xvecmath.cpp,v 1.7 1993/09/16 17:16:56 alv Exp $");

> if <C>==DComplexVec || <C>==FComplexVec || <C>==IntVec
<P>Vec
abs(const <C>& s)
// Absolute value of a <C>
{
  register unsigned i = s.length();
  <P>Vec temp(i,rwUninitialized);
  register <t>* sp = (<t>*)s.data();	// Cast away "constness" to avoid problems with abs()
  register <P>* dp = temp.data();
  register j = s.stride();
  while (i--) { *dp++ = abs(*sp); sp += j; }
  return temp;
}
> endif

> if <C>==SCharVec
<C>
abs(const <C>& s)
// Absolute value of a <C>
{
  register unsigned i = s.length();
  <C> temp(i,rwUninitialized);
  register const <t>* sp = s.data();
  register <t>* dp = temp.data();
  register j = s.stride();
  while (i--) { *dp++ = abs(int(*sp));  sp += j; }
  return temp;
}
> endif

> if <R/C>==Complex
<C>
<C>::apply(CmathFunTy f) const
// Apply function returning <t> to each element of vector slice
{
  register i = length();
  <C> temp(i,rwUninitialized);
  register <t>* sp = (<t>*)data();
  register <t>* dp = temp.data();
  register j = stride();
  while(i--) {
// Turbo C++ bug requires temporary.
     <t> dummy = (*f)(*sp);
     *dp++ = dummy;
     sp += j; 
  }
  return temp;
}

<P>Vec
<C>::apply2(CmathFunTy2 f) const
// Apply function returning <P> to each element of vector slice
{
  register i = length();
  <P>Vec temp(i,rwUninitialized);
  register <t>* sp = (<t>*)data();
  register <P>* dp = temp.data();
  register j = stride();
  while (i--) { *dp++ = (*f)(*sp);  sp += j; }
  return temp;
}
> elif <C>==DoubleVec || <C>==FloatVec

<C>
<C>::apply(mathFunTy f) const
// Apply function returning <t> to each element of vector slice
{
  register i = length();
  <C> temp(i,rwUninitialized);
  register const <t>* sp = data();
  register <t>* dp = temp.data();
  register j = stride();
  while(i--) {
// Turbo C++ bug requires temporary.
#if defined (__TURBOC__)
     <t> dummy = f(*sp);
     *dp++ = dummy;
#else
     *dp++ = (*f)(*sp);
#endif
     sp += j; 
  }
  return temp;
}

#if defined(RW_NATIVE_EXTENDED)
<C>
<C>::apply(XmathFunTy f) const
// apply function for compilers using the extended type
{
  register i = length();
  <C> temp(i,rwUninitialized);
  register const <t>* sp = data();
  register <t>* dp = temp.data();
  register j = stride();
  while(i--) {
    *dp++ = (*f)(*sp);
    sp += j;
  }
  return temp;
}
#endif
> endif

> if <R/C>==Complex
#  ifdef COMPLEX_PACKS

/*
 * If we can guarantee that the complex vector goes
 * (real, imag, real, imag, real, imag, etc.) then
 * it becomes possible to implement real(<C>&) and imag(<C>&)
 * via a slice, allowing its use as an lvalue.  What
 * follows violates encapsulation (a bit), but sure is useful.
 */

<P>Vec
real(const <C>& v)
{
#ifdef IMAG_LEADS
  return <P>Vec(v, (<P>*)v.begin + 1, v.length(), 2*v.stride());
#else
  return <P>Vec(v, (<P>*)v.begin, v.length(), 2*v.stride());
#endif
}
<P>Vec
imag(const <C>& v)
{
#ifdef IMAG_LEADS
  return <P>Vec(v, (<P>*)v.begin, v.length(), 2*v.stride());
#else
  return <P>Vec(v, (<P>*)v.begin + 1, v.length(), 2*v.stride());
#endif
}

// The above allows a very compact form of conj() to be written:
<C>
conj(const <C>& V)
{
  return <C>(real(V), -imag(V));
}

#else	/* Complex does not pack */

<P>Vec
real(const <C>& v)
{
  register unsigned i = v.length();
  <P>Vec temp(i,rwUninitialized);
  register const <t>* sp = v.data();
  register <P>* dp = temp.data();
  register j = v.stride();
  while(i--) {*dp++ = real(*sp); sp += j;}
  return temp;
}

<P>Vec
imag(const <C>& v)
{
  register unsigned i = v.length();
  <P>Vec temp(i,rwUninitialized);
  register const <t>* sp = v.data();
  register <P>* dp = temp.data();
  register j = v.stride();
  while(i--) {*dp++ = imag(*sp); sp += j;}
  return temp;
}

<C>	conj(const <C>& V)	{return V.apply(::conj); }

#endif /* Not COMPLEX_PACKS */

<t>
conjDot(const <C>& u,const <C>& v)
     // Conjugate dot product: returns dot product of u star v.
{
  unsigned N = v.length();
  u.lengthCheck(N);
  <t> result;
  rw<bla>cdot(N, u.data(), u.stride(), v.data(), v.stride(), &result);
  return result;
}

> # End of complex section:
> endif

> if <C>==DoubleVec || <C>==FloatVec
<C>
atan2(const <C>& u,const <C>& v)
     // Arctangent of u/v.
{
  register unsigned i = v.length();
  u.lengthCheck(i);
  <C> temp(i,rwUninitialized);
  register const <t>* up = u.data();
  register const <t>* vp = v.data();
  register <t>* dp = temp.data();
  register uj = u.stride();
  register vj = v.stride();
  while (i--) { *dp++ = atan2(*up,*vp);  up += uj;  vp += vj; }
  return temp;
}
> endif

<C>
cumsum(const <C>& s)
// Cumulative sum of <C> slice.
// Note: V == delta(cumsum(V)) == cumsum(delta(V))
{
  unsigned i = s.length();
  <C> temp(i,rwUninitialized);
  register <t>* sp = (<t>*)s.data();
  register <t>* dp = temp.data();
> if <R/C>==Complex
  <t> c(0,0);
> else
  register <t> c = 0;
> endif
  register j = s.stride();
  while (i--) { c += *sp;  *dp++ = c;  sp += j; }
  return temp;
}

> # delta() is not defined for unsigned chars
> if <C>!=UCharVec
<C>
delta(const <C>& s)
     // Element differences of <C> slice.
     // Note: V == delta(cumsum(V)) == cumsum(delta(V))
{
  unsigned i = s.length();
  <C> temp(i,rwUninitialized);
  register <t>* sp = (<t>*)s.data();
  register <t>* dp = temp.data();
> if <R/C>==Complex
  <t> c(0,0);
> else
  register <t> c = 0;
> endif
  register j = s.stride();
  while (i--) { *dp++ = *sp - c; c = *sp; sp += j; }
  return temp;
}
> endif

<t>
dot(const <C>& u,const <C>& v)
     // Vector dot product.
{
  unsigned N = v.length();
  u.lengthCheck(N);
  <t> result;
  rw<bla>dot(N, u.data(), u.stride(), v.data(), v.stride(), &result);
  return result;
}

> if <R/C>==Real
int
maxIndex(const <C>& s)
// Index of maximum element.
{
  if (s.length() == 0) s.emptyErr("maxIndex");
  int result = rw<bla>max(s.length(), s.data(), s.stride());
  return result;
}

<t>
maxValue(const <C>& s)
{
  int inx = maxIndex(s);
  return s(inx);
}

int
minIndex(const <C>& s)
     // Index of minimum element.
{
  if (s.length() == 0) s.emptyErr("minIndex");
  int result = rw<bla>min(s.length(), s.data(), s.stride());
  return result;
}

<t>
minValue(const <C>& s)
{
  int inx = minIndex(s);
  return s(inx);
}

> endif

<t>
prod(const <C>& s)
     // Product of <C> elements.
{
  register unsigned i = s.length();
  register <t>* sp = (<t>*)s.data();
> if <R/C>==Complex
  REGISTER <t> t(1,0);
> else
  REGISTER <t> t = 1;
> endif
  register j = s.stride();
  while (i--) { t *= *sp;  sp += j; }
  return t;
}

> if <P>==Double || <P>==Float
<C>
pow(const <C>& u, const <C>& v)
// u to the v power.
{
  register unsigned i = v.length();
  u.lengthCheck(i);
  <C> temp(i,rwUninitialized);
  register <t>* up = (<t>*)u.data();
  register <t>* vp = (<t>*)v.data();
  register <t>* dp = temp.data();
  register uj = u.stride();
  register vj = v.stride();
  while (i--) { *dp++ = pow(*up,*vp); up += uj; vp += vj; }
  return temp;
}
> endif

<C>
reverse(const <C>& s)
     // Reverse vector elements --- pretty tricky!
{
  return s.slice( s.length()-1, s.length(), -1);
}

<t>
sum(const <C>& s)
     // Sum of a <C>
{
  register unsigned i = s.length();
  register <t>* sp = (<t>*)s.data();
> if <R/C>==Complex
  <t> t(0,0);
> elif <C>==FloatVec
  register double t = 0;	// Accumulate floats as double
> else
  register <t> t = 0;
> endif

  register j = s.stride();
  while (i--) { t += *sp;  sp += j; }
  return (<t>)t;
}

> if <P>==Double || <P>==Float
<p>
variance(const <C>& s)
// Variance of a <C>
{
  register unsigned i = s.length();
  if(i<2){
    RWTHROW( RWInternalErr( RWMessage( RWMATH_NPOINTS, "<C>", (unsigned)i, "Variance needs minimum of", 2) ));
  }
  register <t>* sp = (<t>*)s.data();
  register double sum2 = 0;	// Accumulate sums in double precision
  REGISTER <t> avg = mean(s);
  register j = s.stride();
  while (i--) {
    REGISTER <t> temp = *sp - avg;
> if <R/C>==Complex
    sum2 += norm(temp);
> else
    sum2 += temp * temp;
> endif
    sp += j;
  }
  // Use the biased estimate (easier to work with):
  return (<p>)(sum2/s.length());
}
> endif

> if <P>==Double || <P>==Float
/*
 * Various functions with simple definitions:
 */
> endif
> if <C>==DoubleVec || <C>==FloatVec
<C>	acos(const <C>& V)	{ return V.apply(::acos); }
<C>	asin(const <C>& V)	{ return V.apply(::asin); }
<C>	atan(const <C>& V)	{ return V.apply(::atan); }
<C>	ceil(const <C>& V)	{ return V.apply(::ceil); }
> endif
> if <P>==Double || <P>==Float
<C>	cos(const <C>& V)		{ return V.apply(::cos);  }
<C>	cosh(const <C>& V)	{ return V.apply(::cosh); }
> endif
> if <P>==Double || <P>==Float
<C>	exp(const <C>& V)		{ return V.apply(::exp);  }
> endif
> if <C>==DoubleVec || <C>==FloatVec
<C>	floor(const <C>& V)	{ return V.apply(::floor);}
> endif
> if <C>==DoubleVec || <C>==FloatVec
<C>	log(const <C>& V)		{ return V.apply(::log);  }
<C>	log10(const <C>& V)	{ return V.apply(::log10);}
> endif
> if <C>==DoubleVec || <C>==FloatVec
<t>		mean(const <C>& V)	{ return sum(V)/V.length();}
> elif <R/C>==Complex
<t>		mean(const <C>& V)	{ <T> s = sum(V); return s/<T>(<p>(V.length()),0); }
> endif
> if <P>==Double || <P>==Float
<C>	sin(const <C>& V)		{ return V.apply(::sin);  }
<C>	sinh(const <C>& V)	{ return V.apply(::sinh); }
> endif
> if <C>==DoubleVec || <C>==FloatVec
<C>	tan(const <C>& V)		{ return V.apply(::tan);  }
<C>	tanh(const <C>& V)	{ return V.apply(::tanh); }
> endif
> if <R/C>==Complex
<P>Vec	arg(const <C>& V)	{return V.apply2(::arg);  }
<P>Vec	norm(const <C>& V)	{return V.apply2(::norm); }
/* One oddball function: */
<C>	rootsOfOne(int N) {return rootsOfOne(N, (N>=0 ? N : -N) );}
> endif

> if <C>==DoubleVec || <C>==FloatVec
#ifdef __ZTC__
/*
 * Zortech requires its own special version of abs(const <C>&)
 * because it uses an inlined version of fabs().  Hence, apply() 
 * cannot be used.
 */
<C>
abs(const <C>& s)
// Absolute value of a <C> --- Zortech version
{
  register unsigned i = s.length();
  <C> temp(i,rwUninitialized);
  register <t>* sp = (<t>*)s.data();
  register <t>* dp = temp.data();
  register j = s.stride();
  while (i--) { *dp++ = fabs(*sp); sp += j; }
  return temp;
}
#else   /* Not Zortech */
<C>	abs(const <C>& V)		{ return V.apply(::fabs); }
#endif	
> endif
> 
> if <P>==Double || <P>==Float
#ifdef __ZTC__
/*
 * Zortech requires its own special version of sqrt(const <C>&) 
 * because it uses an inlined version of sqrt().  Hence, apply() 
 * cannot be used.
 */
<C>
sqrt(const <C>& s)
// Square root of a <C> --- Zortech version
{
  register unsigned i = s.length();
  <C> temp(i,rwUninitialized);
  register <t>* sp = (<t>*)s.data();
  register <t>* dp = temp.data();
  register j = s.stride();
  while (i--) { *dp++ = sqrt(*sp); sp += j; }
  return temp;
}
#else
<C>	sqrt(const <C>& V)	{ return V.apply(::sqrt); }
#endif
> endif

> if <T>==Double || <T>==Float || <T>==DComplex
    /**** Norm functions ****/
<P> l2Norm(const <C>& x)
{
> if <R/C>==Complex
  return sqrt(real(conjDot(x,x)));
> else
  return sqrt(dot(x,x));
> endif
}

<P> l1Norm(const <C>& x)
{
> if <R/C>==Complex
  return sum(sqrt(norm(x)));
> else
  return sum(abs(x));
> endif
}
	  
<P> linfNorm(const <C>& x)
{
> if <R/C>==Complex
  return sqrt(maxValue(norm(x)));
> else
  return maxValue(abs(x));
> endif
}

<P> maxNorm(const <C>& x)
{
  return linfNorm(x);
}

<P> frobNorm(const <C>& x)
{
  return l2Norm(x);
}

> endif

/*
 * Definitions for DComplexVec I/O
 *
 * Generated from template $Id: xvecio.cpp,v 1.6 1993/08/10 21:22:18 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/cvec.h"
#include "rw/rwfile.h"
#include "rw/vstream.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"
STARTWRAP
#include <stdio.h>
#include <ctype.h>
ENDWRAP

#  define CAST DComplex
#  define CTYPE DComplex

RCSID("$Header: /users/rcs/mathsrc/xvecio.cpp,v 1.6 1993/08/10 21:22:18 alv Exp $");

const int            default_resize        = 128;
const unsigned short versionID             = 2;		// Formatting version number

/************************* I / O   R O U T I N E S ***********************/

void
DComplexVec::printOn(ostream& s, unsigned numberPerLine) const 
{
  s << "[\n";
  for(register int n = 0; n < length(); n++){
    if (n) { if (numberPerLine==0 || n%numberPerLine) {s<<" ";} else {s<<"\n";} }
    CAST item = (*this)(n);	// Can't use s<<(*this)(n) due to cfront bug
    s << item;
  }
  s << "\n]";
}

void
DComplexVec::scanFrom(istream& s)
{
  CAST item;
  register int nextspace = 0;
  char c = 0;		// The first character read from the stream
  
  // Munch through any leading white space:
  do {
    s.get(c);
  } while (isspace(c)) ;
  
  if (c != '[') {
    // Scan input stream, resizing as necessary.
    // Keep scanning till we can't scan no mo'
    s.putback(c);
    while (s >> item) {
      if (nextspace >= length()) resize(length()+default_resize);
      (*this)(nextspace++) = item;
    }
  }
  else {  // Scan input stream, stop scanning at the matching ']' character
    
    do {
      s.get(c);			// Munch through any white space after the leading '['
    } while (isspace(c));

    while ( s && (c != ']')  ) {
       s.putback(c);
       if (s >> item) {
           if (nextspace >= length()) resize(length()+default_resize);
           (*this)(nextspace++) = item;
       }
       do {
	 s.get(c);
       } while (isspace(c)) ;

     }
    }
  // Trim to fit
  if(nextspace != length())resize(nextspace);
}

ostream&
operator<<(ostream& s, const DComplexVec& v)
{
  v.printOn(s); return s;
}
istream&
operator>>(istream& s, DComplexVec& v)
{
  v.scanFrom(s); return s;
}

void
DComplexVec::saveOn(RWFile& file) const 
{
  unsigned len = length();
  file.Write(versionID);
  file.Write(len);

#  if COMPLEX_PACKS

  if( stride() == 1 ){
    // This is the normal case and things can be done very efficiently.
    // Write the N point complex vector as a 2N point real vector:
    const Double* dataStart = (const Double*) data();
    file.Write(dataStart, 2*len);
  }
  else {
    for(int n = 0; n < len; n++) {
      DComplex val = (*this)(n);
      file.Write( real(val) );
      file.Write( imag(val) );
    }
  }

#  else

  /*
   * Complex vector, but it is not packed.
   * Have to write it out, element-by-element
   */

  for(register int n = 0; n < len; n++) {
    DComplex val = (*this)(n);
    file.Write( real(val) );
    file.Write( imag(val) );
  }

#  endif

}

void
DComplexVec::saveOn(RWvostream& s) const 
{
  unsigned len = length();
  s << versionID;		// Save the version number
  s << len;			// Save the vector length

#  if COMPLEX_PACKS && !defined(IMAG_LEADS)

  if( stride() == 1 ){
    // This is the normal case and things can be done very efficiently.
    // Write the N point complex vector as a 2N point real vector:
    const Double* dataStart = (const Double*) data();
    s.put(dataStart, 2*len);
  }
  else {
    for(register int n = 0; n < len; n++) {
      DComplex val = (*this)(n);
      s << real( val );
      s << imag( val );
    }
  }

#  else	

  /*
   * Complex vector, but either it is not packed, or real does not lead.
   * Have to write it out, element-by-element
   */

  for(register int n = 0; n < len; n++) {
    s << real( (*this)(n) );
    s << imag( (*this)(n) );
  }

#  endif

}

void
DComplexVec::restoreFrom(RWFile& file)
{
  // Get and check the version number
  unsigned short ID;
  file.Read(ID);

  if( ID!=versionID && ID!=340 ){
    versionErr((int)versionID, (int)ID);
  }

  // Get the length and resize to it:
  unsigned len;
  file.Read(len);
  resize(len);

#  if COMPLEX_PACKS

  // This is the normal case.  We can read the N point complex
  // vector as a 2N real vector.
  Double* dataStart = (Double*) data();
  file.Read(dataStart, 2*len);

#  else

  // Complex doesn't pack.  Have to read it in element-by-element
  Double tmpReal, tmpImag;
  for(register int n=0; n<len; n++){
    file.Read(tmpReal);
    file.Read(tmpImag);
    (*this)(n) = DComplex(tmpReal, tmpImag);
  }

#  endif

}

void
DComplexVec::restoreFrom(RWvistream& s)
{
  // Get and check the version number
  unsigned short ID;
  s >> ID;

  if( ID!=versionID && ID!=340 ){
    RWTHROW( RWExternalErr( RWMessage(RWMATH_BADVERNO, (int)versionID, (int)ID) ));
  }

  // Get the length and resize to it:
  unsigned len;
  s >> len;
  resize(len);

#  if COMPLEX_PACKS

  // This is the normal case.  We can read the N point complex
  // vector as a 2N real vector.
  Double* dataStart = (Double*) data();
  s.get(dataStart, 2*len);

#else

  // Complex doesn't pack.  Have to read it in element-by-element
  Double tmpReal, tmpImag;
  for(register int n=0; n<len; n++){
    s >> tmpReal;
    s >> tmpImag;
    (*this)(n) = DComplex(tmpReal, tmpImag);
  }

#  endif

}

unsigned
DComplexVec::binaryStoreSize() const
{
  // Total storage requirements = Number of elements times their size, 
  // plus space for the vector length and ID number:
  return length()*sizeof(DComplex) + sizeof(unsigned) + sizeof(versionID);
}

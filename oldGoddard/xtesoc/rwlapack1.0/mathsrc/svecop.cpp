/*
 * Definitions for various arithmetic operations
 *
 * Generated from template $Id: xvecop.cpp,v 1.7 1993/09/01 16:28:02 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/scvec.h"
#include "rw/rwabla.h"	

RCSID("$Header: /users/rcs/mathsrc/xvecop.cpp,v 1.7 1993/09/01 16:28:02 alv Exp $");

/************************************************
 *						*
 *		UNARY OPERATORS			*
 *						*
 ************************************************/

// Unary minus on a SCharVec
SCharVec
operator-(const SCharVec& s)
{
  unsigned i = s.length();
  SCharVec temp(i,rwUninitialized);
  register SChar* sp = (SChar*)s.data();
  register SChar* dp = temp.data();
  register j = s.stride();
  while (i--) { *dp++ = -(*sp);  sp += j; }
  return temp;
}

// Unary prefix increment on a SCharVec; (i.e. ++a)
SCharVec&
SCharVec::operator++()
{
  register i = length();
  register SChar* sp = data();
  register j = stride();
  while (i--) { ++(*sp); sp += j; }
  return *this;
}

// Unary prefix decrement on a SCharVec (i.e., --a)
SCharVec&
SCharVec::operator--()
{
  register i = length();
  register SChar* sp = data();
  register j = stride();
  while (i--) { --(*sp); sp += j; }
  return *this;
}

/************************************************
 *						*
 *		BINARY OPERATORS		*
 *		 vector - vector		*
 *						*
 ************************************************/

// Binary add element-by-element
SCharVec
operator+(const SCharVec& u, const SCharVec& v)
{
  unsigned i = v.length();
  u.lengthCheck(i);
  SCharVec temp(i,rwUninitialized);
  rwa_plvv(i, temp.data(), u.data(), u.stride(), v.data(), v.stride());
  return temp;
}

// Binary subtract element-by-element
SCharVec
operator-(const SCharVec& u, const SCharVec& v)
{
  unsigned i = v.length();
  u.lengthCheck(i);
  SCharVec temp(i,rwUninitialized);
  rwa_mivv(i, temp.data(), u.data(), u.stride(), v.data(), v.stride());
  return temp;
}

// Binary multiply element-by-element
SCharVec
operator*(const SCharVec& u, const SCharVec& v)
{
  unsigned i = v.length();
  u.lengthCheck(i);
  SCharVec temp(i,rwUninitialized);
  rwa_muvv(i, temp.data(), u.data(), u.stride(), v.data(), v.stride());
  return temp;
}

// Binary divide element-by-element
SCharVec
operator/(const SCharVec& u, const SCharVec& v)
{
  unsigned i = v.length();
  u.lengthCheck(i);
  SCharVec temp(i,rwUninitialized);
  rwa_dvvv(i, temp.data(), u.data(), u.stride(), v.data(), v.stride());
  return temp;
}

/************************************************
 *						*
 *		BINARY OPERATORS		*
 *		 vector - scalar		*
 *						*
 ************************************************/

// Add a scalar
SCharVec
operator+(const SCharVec& s, SChar scalar)
{
  unsigned i = s.length();
  SCharVec temp(i,rwUninitialized);
  SChar scalar2 = scalar;
  rwa_plvs(i, temp.data(), s.data(), s.stride(), &scalar2);
  return temp;
}


// Subtract from a scalar
SCharVec
operator-(SChar scalar, const SCharVec& s)
{
  unsigned i = s.length();
  SCharVec temp(i,rwUninitialized);
  SChar scalar2 = scalar;
  rwa_misv(i, temp.data(), &scalar2, s.data(), s.stride());
  return temp;
}

// Multiply by a scalar
SCharVec
operator*(const SCharVec& s, SChar scalar)
{
  unsigned i = s.length();
  SCharVec temp(i,rwUninitialized);
  SChar scalar2 = scalar;
  rwa_muvs(i, temp.data(), s.data(), s.stride(), &scalar2);
  return temp;
}

// Divide into a scalar
SCharVec
operator/(SChar scalar, const SCharVec& s)
{
  unsigned i = s.length();
  SCharVec temp(i,rwUninitialized);
  SChar scalar2 = scalar;
  rwa_dvsv(i, temp.data(), &scalar2, s.data(), s.stride());
  return temp;
}

// Divide by a scalar
SCharVec
operator/(const SCharVec& s, SChar scalar)
{
  unsigned i = s.length();
  SCharVec temp(i,rwUninitialized);
  SChar scalar2 = scalar;
  rwa_dvvs(i, temp.data(), s.data(), s.stride(), &scalar2);
  return temp;
}

/*
 * Out-of-line versions for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
SCharVec	operator*(SChar s, const SCharVec& V)	{return V*s;}
SCharVec	operator+(SChar s, const SCharVec& V)	{return V+s;}
SCharVec	operator-(const SCharVec& V, SChar s)  {return V+(-s);}
#endif

/************************************************
 *						*
 *	ARITHMETIC ASSIGNMENT OPERATORS		*
 *	  with other vectors			*
 *						*
 ************************************************/

SCharVec&
SCharVec::operator+=(const SCharVec& u)
{
  if (sameDataBlock(u)) { return operator+=(u.deepCopy()); }  // Avoid aliasing
  unsigned i = u.length();
  lengthCheck(i);
  rwa_aplvv(i, data(), stride(), u.data(), u.stride());
  return *this;
}

SCharVec&
SCharVec::operator-=(const SCharVec& u)
{
  if (sameDataBlock(u)) { return operator-=(u.deepCopy()); }  // Avoid aliasing
  unsigned i = u.length();
  lengthCheck(i);
  rwa_amivv(i, data(), stride(), u.data(), u.stride());
  return *this;
}

SCharVec&
SCharVec::operator*=(const SCharVec& u)
{
  if (sameDataBlock(u)) { return operator*=(u.deepCopy()); }  // Avoid aliasing
  unsigned i = u.length();
  lengthCheck(i);
  rwa_amuvv(i, data(), stride(), u.data(), u.stride());
  return *this;
}

SCharVec&
SCharVec::operator/=(const SCharVec& u)
{
  if (sameDataBlock(u)) { return operator/=(u.deepCopy()); }  // Avoid aliasing
  unsigned i = u.length();
  lengthCheck(i);
  rwa_advvv(i, data(), stride(), u.data(), u.stride());
  return *this;
}

/************************************************
 *						*
 *	ARITHMETIC ASSIGNMENT OPERATORS		*
 *		  with a scalar			*
 *						*
 ************************************************/

SCharVec&
SCharVec::operator+=(SChar scalar)
{
  SChar scalar2 = scalar;
  rwa_aplvs(length(), data(), stride(), &scalar2);
  return *this;
}


SCharVec&
SCharVec::operator*=(SChar scalar)
{
  SChar scalar2 = scalar;
  rwa_amuvs(length(), data(), stride(), &scalar2);
  return *this;
}

SCharVec&
SCharVec::operator/=(SChar scalar)
{
  SChar scalar2 = scalar;
  rwa_advvs(length(), data(), stride(), &scalar2);
  return *this;
}

/************************************************
 *						*
 *		LOGICAL OPERATORS		*
 *						*
 ************************************************/

RWBoolean
SCharVec::operator==(const SCharVec& u) const
{
  unsigned i = length();
  if(i != u.length()) return FALSE;  // They can't be equal if they don't have the same length.
  return rwasame(i, data(), stride(), u.data(), u.stride());
}

RWBoolean
SCharVec::operator!=(const SCharVec& u) const
{
  return !(*this == u);
}


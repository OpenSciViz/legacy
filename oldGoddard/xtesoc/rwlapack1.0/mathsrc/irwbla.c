/*
 * BLA-like extensions for type int.
 *
 * Generated from template $Id: xrwbla.c,v 1.3 1993/10/11 18:42:24 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/rwibla.h"
#include "rw/mathdefs.h"

#include <math.h>
#ifdef BSD
#  include <memory.h>
#else
#  include <string.h>
#endif

/****************************************************************
 *								*
 *			UTILITY					*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwicopy(n, x, incx, y, incy )
  unsigned n;
  int *x, *y;
  int incx, incy;
#else
void FDecl rwicopy( unsigned n, int* restrict x, int incx, const int* restrict y, int incy)
#endif
{
  if (n==0) return;

  if (incx == 1 && incy == 1) {
    memcpy(x, y, n*sizeof(int));
  }
  else {
    /*
     * Code for increments not equal to one
     */
    while (n--) {
      *x = *y;
      x += incx;
      y += incy;
    }
  }
}

#ifdef RW_KR_ONLY
void rwiset(n, x, incx, scalar)
  unsigned n;
  int *x, *scalar;
  int incx;
#else
void FDecl rwiset(unsigned n, int* restrict x, int incx, const int* restrict scalar)
#endif
{
  if(incx==1)
    while (n--) { *x++ = *scalar; }
  else
    while (n--) { *x = *scalar; x+=incx; }
}

#ifdef RW_KR_ONLY
int rwisame(n, x, incx, y, incy)
  unsigned n;
  int *x, *y;
  int incx, incy;
#else
int FDecl rwisame(unsigned n, const int* restrict x, int incx, const int* restrict y, int incy)
#endif
{
  while(n--){
    if( *x != *y ) return 0;
    x += incx;
    y += incy;
  }
  return 1;
}

#ifdef RW_KR_ONLY
int rwimax(n, x, incx)
  unsigned n;
  int *x;
  int incx;
#else
int FDecl rwimax(unsigned n, const int* restrict x, int incx)
#endif
{
  int maxv;
  int maxi, i;

  if(n==0) return -1;
  maxv = *x;
  maxi = 0;

  for(i=1; i<n; i++){
    x += incx;
    if(*x > maxv) { maxv = *x; maxi=i; }
  }
  return maxi;
}

#ifdef RW_KR_ONLY
int rwimin(n, x, incx)
  unsigned n;
  int *x;
  int incx;
#else
int FDecl rwimin(unsigned n, const int* restrict x, int incx)
#endif
{
  int minv;
  int mini, i;

  if(n==0) return -1;
  minv = *x;
  mini = 0;

  for(i=1; i<n; i++){
    x += incx;
    if(*x < minv) { minv = *x; mini=i; }
  }
  return mini;
}

/****************************************************************
 *								*
 *			DOT PRODUCTS				*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwidot(n, x, incx, y, incy, result)
  unsigned n;
  int *x, *y, *result;
  int incx, incy;
#else
void FDecl rwidot(unsigned n, const int* restrict x, int incx, const int* restrict y, int incy, int* result)
#endif
{

  int sum = 0;
  while(n--){
    sum += *x * *y;
    x += incx;
    y += incy;
  }
  *result = (int)sum;
}


/****************************************************************
 *								*
 *		VECTOR --- VECTOR ASSIGNMENT OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwi_aplvv(n, x, incx, y, incy)
  unsigned n;
  int *x, *y;
  int incx, incy;
#else
void FDecl rwi_aplvv(unsigned n, int* restrict x, int incx, const int* restrict y, int incy)
#endif
{
  while(n--){
    *x += *y;
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rwi_amivv(n, x, incx, y, incy)
  unsigned n;
  int *x, *y;
  int incx, incy;
#else
void FDecl rwi_amivv(unsigned n, int* restrict x, int incx, const int* restrict y, int incy)
#endif
{
  while(n--){
    *x -= *y;
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rwi_amuvv(n, x, incx, y, incy)
  unsigned n;
  int *x, *y;
  int incx, incy;
#else
void FDecl rwi_amuvv(unsigned n, int* restrict x, int incx, const int* restrict y, int incy)
#endif
{
  while(n--){
    *x *= *y;
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rwi_advvv(n, x, incx, y, incy)
  unsigned n;
  int *x, *y;
  int incx, incy;
#else
void FDecl rwi_advvv(unsigned n, int* restrict x, int incx, const int* restrict y, int incy)
#endif
{
  while(n--){
    *x /= *y;
    x += incx;
    y += incy;
  }
}

/****************************************************************
 *								*
 *		VECTOR --- SCALAR ASSIGNMENT OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwi_aplvs(n, x, incx, scalar)
  unsigned n;
  int *x, *scalar;
  int incx;
#else
void FDecl rwi_aplvs(unsigned n, int* restrict x, int incx, const int* restrict scalar)
#endif
{
  while(n--) {
    *x += *scalar;
    x += incx;
  }
}

#ifdef RW_KR_ONLY
void rwi_amuvs(n, x, incx, scalar)
  unsigned n;
  int *x, *scalar;
  int incx;
#else
void FDecl rwi_amuvs(unsigned n, int* restrict x, int incx, const int* restrict scalar)
#endif
{
  while(n--) {
    *x *= *scalar;
    x += incx;
  }
}

#ifdef RW_KR_ONLY
void rwi_advvs(n, x, incx, scalar)
  unsigned n;
  int *x, *scalar;
  int incx;
#else
void FDecl rwi_advvs(unsigned n, int* restrict x, int incx, const int* restrict scalar)
#endif
{
  while(n--) {
    *x /= *scalar;
    x += incx;
  }
}

/****************************************************************
 *								*
 *		VECTOR --- VECTOR BINARY OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwi_plvv(n, z, x, incx, y, incy)
  unsigned n;
  int *z, *x, *y;
  int incx, incy;
#else
void FDecl rwi_plvv(unsigned n, int* restrict z, const int* restrict x, int incx, const int* restrict y, int incy)
#endif
{
  while(n--){
    *z++ = *x + *y;
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rwi_mivv(n, z, x, incx, y, incy)
  unsigned n;
  int *z, *x, *y;
  int incx, incy;
#else
void FDecl rwi_mivv(unsigned n, int* restrict z, const int* restrict x, int incx, const int* restrict y, int incy)
#endif
{
  while(n--){
    *z++ = *x - *y;
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rwi_muvv(n, z, x, incx, y, incy)
  unsigned n;
  int *z, *x, *y;
  int incx, incy;
#else
void FDecl rwi_muvv(unsigned n, int* restrict z, const int* restrict x, int incx, const int* restrict y, int incy)
#endif
{
  while(n--){
    *z++ = *x * *y;
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rwi_dvvv(n, z, x, incx, y, incy)
  unsigned n;
  int *z, *x, *y;
  int incx, incy;
#else
void FDecl rwi_dvvv(unsigned n, int* restrict z, const int* restrict x, int incx, const int* restrict y, int incy)
#endif
{
  while(n--){
    *z++ = *x / *y;
    x += incx;
    y += incy;
  }
}

/****************************************************************
 *								*
 *		VECTOR --- SCALAR BINARY OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwi_plvs(n, z, x, incx, scalar)
  unsigned n;
  int *z, *x, *scalar;
  int incx;
#else
void FDecl rwi_plvs(unsigned n, int* restrict z, const int* restrict x, int incx, const int* restrict scalar)
#endif
{
  while(n--){
    *z++ = *x + *scalar;
    x += incx;
  }
}

#ifdef RW_KR_ONLY
void rwi_muvs(n, z, x, incx, scalar)
  unsigned n;
  int *z, *x, *scalar;
  int incx;
#else
void FDecl rwi_muvs(unsigned n, int* restrict z, const int* restrict x, int incx, const int* restrict scalar)
#endif
{
  while(n--){
    *z++ = *x * *scalar;
    x += incx;
  }
}

#ifdef RW_KR_ONLY
void rwi_dvvs(n, z, x, incx, scalar)
  unsigned n;
  int *z, *x, *scalar;
  int incx;
#else
void FDecl rwi_dvvs(unsigned n, int* restrict z, const int* restrict x, int incx, const int* restrict scalar)
#endif
{
  while(n--){
    *z++ = *x / *scalar;
    x += incx;
  }
}

/****************************************************************
 *								*
 *		SCALAR --- VECTOR BINARY OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwi_misv(n, z, scalar, x, incx)
  unsigned n;
  int *z, *scalar, *x;
  int incx;
#else
void FDecl rwi_misv(unsigned n, int* restrict z, const int* scalar, const int* restrict x, int incx)
#endif
{
  while(n--){
    *z++ = *scalar - *x;
    x += incx;
  }
}

#ifdef RW_KR_ONLY
void rwi_dvsv(n, z, scalar, x, incx)
  unsigned n;
  int *z, *scalar, *x;
  int incx;
#else
void FDecl rwi_dvsv(unsigned n, int* restrict z, const int* scalar, const int* restrict x, int incx)
#endif
{
  while(n--){
    *z++ = *scalar / *x;
    x += incx;
  }
}


/*
 * Definitions for IntGenMatPick
 *
 * Generated from template $Id: xgenpik.cpp,v 1.3 1993/08/10 21:22:18 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */
#include "rw/igenmat.h"
#include "rw/igenpik.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"
STARTWRAP
#include <stdio.h>
ENDWRAP

RCSID("$Header: /users/rcs/mathsrc/xgenpik.cpp,v 1.3 1993/08/10 21:22:18 alv Exp $");

void IntGenMatPick::operator=(const IntGenMat& v)
{
  lengthCheck(v.rows(),v.cols());
  for(int j=cols(); j--;) {
    Int* dest = V.data() + colpick(j)*V.colStride();
    const Int* src = v.data() + j*v.colStride();
    for(int i=rows(); i--;) {
      dest[rowpick(i)*V.rowStride()] = src[i*v.rowStride()];
    }
  }
}

void IntGenMatPick::operator=(const IntGenMatPick& p)
{
  lengthCheck(p.rows(),p.cols());
  for(int j=cols(); j--;) {
    Int* dest = V.data() + colpick(j)*V.colStride();
    const Int* src = p.V.data() + p.colpick(j)*p.V.colStride();
    for(int i=rows(); i--;) {
      dest[rowpick(i)*V.rowStride()] = src[p.rowpick(i)*p.V.rowStride()];
    }
  }
}

void IntGenMatPick::operator=(Int s)
{
  for(int j=cols(); j--;) {
    Int* dest = V.data() + colpick(j)*V.colStride();
    for(int i=rows(); i--;) {
      dest[rowpick(i)*V.rowStride()] = s;
    }
  }
}

IntGenMat& IntGenMat::operator=(const IntGenMatPick& p)
{
  p.lengthCheck(rows(),cols());
  for(int j=cols(); j--;) {
    Int* dest = data() + j*colStride();
    const Int* src = p.V.data() + p.colpick(j)*p.V.colStride();
    for(int i=rows(); i--;) {
      dest[i*rowStride()] = src[p.rowpick(i)*p.V.rowStride()];
    }
  }
  return *this;
}

IntGenMat::IntGenMat(const IntGenMatPick& p)
  : RWMatView(p.rows(),p.cols(),sizeof(int))
{
  Int* dest = data()+nrows*ncols;
  if (p.V.rowStride()==1 && p.rowpick.stride()==1) {
    for(int j=ncols; j--;) {
      const Int* src = p.V.data() + p.colpick(j)*p.V.colStride();
      const int* pdat = p.rowpick.data()+nrows;
      for(int i=nrows; i--;) {
        *(--dest) = src[*(--pdat)];
      }
    }
  } else {
    for(int j=ncols; j--;) {
      const Int* src = p.V.data() + p.colpick(j)*p.V.colStride();
      for(int i=nrows; i--;) {
        *(--dest) = src[p.rowpick(i)*p.V.rowStride()];
      }
    }
  }
}

IntGenMatPick IntGenMat::pick(const IntVec& ipick, const IntVec& jpick) {
  return IntGenMatPick(*this, ipick, jpick);
}

const IntGenMatPick IntGenMat::pick(const IntVec& ipick, const IntVec& jpick) const {
  return IntGenMatPick(*(IntGenMat*)this, ipick, jpick);
}

/********************** U T I L I T I E S ***********************/

void IntGenMatPick::assertElements() const
{
  int i = rowpick.length();
  while (--i>=0) {
    if (rowpick(i)<0 || rowpick(i)>=V.rows()) {
      RWTHROW( RWBoundsErr( RWMessage(RWMATH_INDEX,	(int)rowpick(i), (unsigned)(V.rows()-1)) ));
    }
  }
  i = colpick.length();
  while (--i>=0) {
    if (colpick(i)<0 || colpick(i)>=V.cols()) {
      RWTHROW( RWBoundsErr( RWMessage(RWMATH_INDEX,	(int)colpick(i), (unsigned)(V.cols()-1)) ));
    }
  }
}

void IntGenMatPick::lengthCheck(unsigned m, unsigned n) const
{
  if(rows()!=m || cols()!=n) {
  	 RWTHROW( RWBoundsErr( RWMessage(RWMATH_MNMATCH, (unsigned)rows(), (unsigned)cols(), m, n)));
  }
}

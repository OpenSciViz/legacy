/*
 * Definitions for various arithmetic operations for DoubleGenMat
 *
 * Generated from template $Id: xmatop.cpp,v 1.10 1993/09/01 16:27:57 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

/*
 * If a promotable scalar (such as a float) is passed by value, then
 * it will get promoted to, say, a double.  If the address of this is
 * then taken, we have a pointer to a double.  Unfortunately, if
 * this address is then passed to a routine that expects a pointer
 * to a float, cfront thinks it has done its job and quietly passes
 * the address of this promoted variable.
 *
 * This problem occurs when K&R compilers are used as a backend to
 * cfront.  The workaround is to create a temporary of known type
 * on the stack.  This is the "scalar2" variable found throughout
 * this file.
 */

#include "rw/dgenmat.h"
#include "rw/rwdbla.h"	

RCSID("$Header: /users/rcs/mathsrc/xmatop.cpp,v 1.10 1993/09/01 16:27:57 alv Exp $");

/************************************************
 *                                              *
 *              UNARY OPERATORS                 *
 *                                              *
 ************************************************/

DoubleGenMat operator-(const DoubleGenMat& s)   // Unary minus
{
  DoubleGenMat temp(s.rows(),s.cols(),rwUninitialized);
  double* dp = temp.data();
  for(MatrixLooper l(s.rows(),s.cols(),s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    int n = l.length;
    double* sp = (double*)s.data()+l.start;
    while(n--) { *dp++ = -(*sp); sp+=l.stride; }
  }
  return temp;
}

// Unary prefix increment on a DoubleGenMat; (i.e. ++a)
DoubleGenMat& DoubleGenMat::operator++()
{
  for(MatrixLooper l(rows(),cols(),rowStride(),colStride()); l; ++l) {
    int n = l.length;
    double* sp = data()+l.start;
    while(n--) { ++(*sp); sp+=l.stride; }
  }
  return *this;
}

// Unary prefix decrement on a DoubleGenMat (i.e., --a)
DoubleGenMat&
DoubleGenMat::operator--()
{
  for(MatrixLooper l(rows(),cols(),rowStride(),colStride()); l; ++l) {
    int n = l.length;
    double* sp = data()+l.start;
    while(n--) { --(*sp); sp+=l.stride; }
  }
  return *this;
}

/************************************************
 *                                              *
 *              BINARY OPERATORS                *
 *               vector - vector                *
 *                                              *
 ************************************************/

DoubleGenMat operator+(const DoubleGenMat& u, const DoubleGenMat& v)
{
  unsigned m = v.rows();
  unsigned n = v.cols();
  u.lengthCheck(m,n);
  DoubleGenMat temp(m,n,rwUninitialized);
  double* dp = temp.data();
  for(DoubleMatrixLooper l(m,n,u.rowStride(),u.colStride(),v.rowStride(),v.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rwd_plvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

DoubleGenMat operator-(const DoubleGenMat& u, const DoubleGenMat& v)
{
  unsigned m = v.rows();
  unsigned n = v.cols();
  u.lengthCheck(m,n);
  DoubleGenMat temp(m,n,rwUninitialized);
  double* dp = temp.data();
  for(DoubleMatrixLooper l(m,n,u.rowStride(),u.colStride(),v.rowStride(),v.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rwd_mivv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

DoubleGenMat operator*(const DoubleGenMat& u, const DoubleGenMat& v)
{
  unsigned m = v.rows();
  unsigned n = v.cols();
  u.lengthCheck(m,n);
  DoubleGenMat temp(m,n,rwUninitialized);
  double* dp = temp.data();
  for(DoubleMatrixLooper l(m,n,u.rowStride(),u.colStride(),v.rowStride(),v.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rwd_muvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

DoubleGenMat operator/(const DoubleGenMat& u, const DoubleGenMat& v)
{
  unsigned m = v.rows();
  unsigned n = v.cols();
  u.lengthCheck(m,n);
  DoubleGenMat temp(m,n,rwUninitialized);
  double* dp = temp.data();
  for(DoubleMatrixLooper l(m,n,u.rowStride(),u.colStride(),v.rowStride(),v.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rwd_dvvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

/************************************************
 *                                              *
 *              BINARY OPERATORS                *
 *               vector - scalar                *
 *                                              *
 ************************************************/

DoubleGenMat operator+(const DoubleGenMat& s, double scalar)
{
  unsigned m = s.rows();
  unsigned n = s.cols();
  DoubleGenMat temp(m,n,rwUninitialized);
  double* dp = temp.data();
  for(MatrixLooper l(m,n,s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rwd_plvs(l.length, dp, s.data()+l.start, l.stride, &scalar);
    dp += l.length;
  }
  return temp;
}

DoubleGenMat operator-(double scalar, const DoubleGenMat& s)
{
  unsigned m = s.rows();
  unsigned n = s.cols();
  DoubleGenMat temp(m,n,rwUninitialized);
  double* dp = temp.data();
  for(MatrixLooper l(m,n,s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rwd_misv(l.length, dp, &scalar, s.data()+l.start, l.stride);
    dp += l.length;
  }
  return temp;
}

DoubleGenMat operator*(const DoubleGenMat& s, double scalar)
{
  unsigned m = s.rows();
  unsigned n = s.cols();
  DoubleGenMat temp(m,n,rwUninitialized);
  double* dp = temp.data();
  for(MatrixLooper l(m,n,s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rwd_muvs(l.length, dp, s.data()+l.start, l.stride, &scalar);
    dp += l.length;
  }
  return temp;
}

DoubleGenMat operator/(double scalar, const DoubleGenMat& s)
{
  unsigned m = s.rows();
  unsigned n = s.cols();
  DoubleGenMat temp(m,n,rwUninitialized);
  double* dp = temp.data();
  for(MatrixLooper l(m,n,s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rwd_dvsv(l.length, dp, &scalar, s.data()+l.start, l.stride);
    dp += l.length;
  }
  return temp;
}


/*
 * Out-of-line versions for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
DoubleGenMat     operator*(double s, const DoubleGenMat& V)  {return V*s;}
DoubleGenMat     operator+(double s, const DoubleGenMat& V)  {return V+s;}
DoubleGenMat     operator-(const DoubleGenMat& V, double s)  {return V+(-s);}
DoubleGenMat     operator/(const DoubleGenMat& V, double s)  {return V*(1/s);}
#endif

/************************************************
 *                                              *
 *      ARITHMETIC ASSIGNMENT OPERATORS         *
 *        with other vectors                    *
 *                                              *
 ************************************************/

DoubleGenMat& DoubleGenMat::operator+=(const DoubleGenMat& u)
{
  if (sameDataBlock(u)) { return operator+=(u.copy()); }  // Avoid aliasing
  u.lengthCheck(rows(),cols());
  for(DoubleMatrixLooper l(rows(),cols(),rowStride(),colStride(),u.rowStride(),u.colStride()); l; ++l) {
    rwd_aplvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

DoubleGenMat& DoubleGenMat::operator-=(const DoubleGenMat& u)
{
  if (sameDataBlock(u)) { return operator-=(u.copy()); }  // Avoid aliasing
  u.lengthCheck(rows(),cols());
  for(DoubleMatrixLooper l(rows(),cols(),rowStride(),colStride(),u.rowStride(),u.colStride()); l; ++l) {
    rwd_amivv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

DoubleGenMat& DoubleGenMat::operator*=(const DoubleGenMat& u)
{
  if (sameDataBlock(u)) { return operator*=(u.copy()); }  // Avoid aliasing
  u.lengthCheck(rows(),cols());
  for(DoubleMatrixLooper l(rows(),cols(),rowStride(),colStride(),u.rowStride(),u.colStride()); l; ++l) {
    rwd_amuvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

DoubleGenMat& DoubleGenMat::operator/=(const DoubleGenMat& u)
{
  if (sameDataBlock(u)) { return operator/=(u.copy()); }  // Avoid aliasing
  u.lengthCheck(rows(),cols());
  for(DoubleMatrixLooper l(rows(),cols(),rowStride(),colStride(),u.rowStride(),u.colStride()); l; ++l) {
    rwd_advvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

/************************************************
 *                                              *
 *      ARITHMETIC ASSIGNMENT OPERATORS         *
 *                with a scalar                 *
 *                                              *
 ************************************************/

DoubleGenMat& DoubleGenMat::operator+=(double scalar)
{
  for(MatrixLooper l(rows(),cols(),rowStride(),colStride()); l; ++l) {
    rwd_aplvs(l.length, data()+l.start, l.stride, &scalar);
  }
  return *this;
}

DoubleGenMat& DoubleGenMat::operator*=(double scalar)
{
  for(MatrixLooper l(rows(),cols(),rowStride(),colStride()); l; ++l) {
    rwd_amuvs(l.length, data()+l.start, l.stride, &scalar);
  }
  return *this;
}


/************************************************
 *                                              *
 *              LOGICAL OPERATORS               *
 *                                              *
 ************************************************/

RWBoolean DoubleGenMat::operator==(const DoubleGenMat& u) const
{
  if(rows()!=u.rows() || cols()!=u.cols()) return FALSE;  // They can't be equal if they don't have the same length.
  for(DoubleMatrixLooper l(rows(),cols(),rowStride(),colStride(),u.rowStride(),u.colStride()); l; ++l) {
    if (!rwdsame(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2)) {
      return FALSE;
    }
  }
  return TRUE;
}

RWBoolean DoubleGenMat::operator!=(const DoubleGenMat& u) const
{
  return !(*this == u);
}

/*
 * Definitions for various arithmetic operations for DoubleArray
 *
 * Generated from template $Id: xarrop.cpp,v 1.11 1993/09/20 04:08:08 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

/*
 * If a promotable scalar (such as a float) is passed by value, then
 * it will get promoted to, say, a double.  If the address of this is
 * then taken, we have a pointer to a double.  Unfortunately, if
 * this address is then passed to a routine that expects a pointer
 * to a float, cfront thinks it has done its job and quietly passes
 * the address of this promoted variable.
 *
 * This problem occurs when K&R compilers are used as a backend to
 * cfront.  The workaround is to create a temporary of known type
 * on the stack.  This is the "scalar2" variable found throughout
 * this file.
 */

#include "rw/darr.h"
#include "rw/rwdbla.h"	

RCSID("$Header: /users/rcs/mathsrc/xarrop.cpp,v 1.11 1993/09/20 04:08:08 alv Exp $");

/************************************************
 *                                              *
 *              UNARY OPERATORS                 *
 *                                              *
 ************************************************/

DoubleArray operator-(const DoubleArray& s)   // Unary minus
{
  DoubleArray temp(s.length(),rwUninitialized);
  double* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    int n = l.length;
    double* sp = (double*)s.data()+l.start;
    while(n--) { *dp++ = -(*sp); sp+=l.stride; }
  }
  return temp;
}

// Unary prefix increment on a DoubleArray; (i.e. ++a)
DoubleArray& DoubleArray::operator++()
{
  for(ArrayLooper l(length(),stride()); l; ++l) {
    int n = l.length;
    double* sp = data()+l.start;
    while(n--) { ++(*sp); sp+=l.stride; }
  }
  return *this;
}

// Unary prefix decrement on a DoubleArray (i.e., --a)
DoubleArray& DoubleArray::operator--()
{
  for(ArrayLooper l(length(),stride()); l; ++l) {
    int n = l.length;
    double* sp = data()+l.start;
    while(n--) { --(*sp); sp+=l.stride; }
  }
  return *this;
}

/************************************************
 *                                              *
 *              BINARY OPERATORS                *
 *               vector - vector                *
 *                                              *
 ************************************************/

DoubleArray operator+(const DoubleArray& u, const DoubleArray& v)
{
  u.lengthCheck(v.length());
  DoubleArray temp(u.length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  double* dp = temp.data();
  for(DoubleArrayLooper l(u.length(),u.stride(),v.stride()); l; ++l) {
    rwd_plvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

DoubleArray operator-(const DoubleArray& u, const DoubleArray& v)
{
  u.lengthCheck(v.length());
  DoubleArray temp(u.length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  double* dp = temp.data();
  for(DoubleArrayLooper l(u.length(),u.stride(),v.stride()); l; ++l) {
    rwd_mivv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

DoubleArray operator*(const DoubleArray& u, const DoubleArray& v)
{
  u.lengthCheck(v.length());
  DoubleArray temp(u.length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  double* dp = temp.data();
  for(DoubleArrayLooper l(u.length(),u.stride(),v.stride()); l; ++l) {
    rwd_muvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

DoubleArray operator/(const DoubleArray& u, const DoubleArray& v)
{
  u.lengthCheck(v.length());
  DoubleArray temp(u.length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  double* dp = temp.data();
  for(DoubleArrayLooper l(u.length(),u.stride(),v.stride()); l; ++l) {
    rwd_dvvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

/************************************************
 *                                              *
 *              BINARY OPERATORS                *
 *               vector - scalar                *
 *                                              *
 ************************************************/

DoubleArray operator+(const DoubleArray& s, double scalar)
{
  DoubleArray temp(s.length(),rwUninitialized);
  double* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    rwd_plvs(l.length, dp, s.data()+l.start, l.stride, &scalar);
    dp += l.length;
  }
  return temp;
}

DoubleArray operator-(double scalar, const DoubleArray& s)
{
  DoubleArray temp(s.length(),rwUninitialized);
  double* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    rwd_misv(l.length, dp, &scalar, s.data()+l.start, l.stride);
    dp += l.length;
  }
  return temp;
}

DoubleArray operator*(const DoubleArray& s, double scalar)
{
  DoubleArray temp(s.length(),rwUninitialized);
  double* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    rwd_muvs(l.length, dp, s.data()+l.start, l.stride, &scalar);
    dp += l.length;
  }
  return temp;
}

DoubleArray operator/(double scalar, const DoubleArray& s)
{
  DoubleArray temp(s.length(),rwUninitialized);
  double* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    rwd_dvsv(l.length, dp, &scalar, s.data()+l.start, l.stride);
    dp += l.length;
  }
  return temp;
}


/*
 * Out-of-line versions for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
DoubleArray     operator*(double s, const DoubleArray& V)  {return V*s;}
DoubleArray     operator+(double s, const DoubleArray& V)  {return V+s;}
DoubleArray     operator-(const DoubleArray& V, double s)  {return V+(-s);}
DoubleArray     operator/(const DoubleArray& V, double s)  {return V*(1/s);}
#endif

/************************************************
 *                                              *
 *      ARITHMETIC ASSIGNMENT OPERATORS         *
 *        with other vectors                    *
 *                                              *
 ************************************************/

DoubleArray& DoubleArray::operator+=(const DoubleArray& u)
{
  if (sameDataBlock(u)) { return operator+=(u.deepCopy()); }  // Avoid aliasing
  u.lengthCheck(length());
  for(DoubleArrayLooper l(length(),stride(),u.stride()); l; ++l) {
    rwd_aplvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}


DoubleArray& DoubleArray::operator-=(const DoubleArray& u)
{
  if (sameDataBlock(u)) { return operator-=(u.deepCopy()); }  // Avoid aliasing
  u.lengthCheck(length());
  for(DoubleArrayLooper l(length(),stride(),u.stride()); l; ++l) {
    rwd_amivv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

DoubleArray& DoubleArray::operator*=(const DoubleArray& u)
{
  if (sameDataBlock(u)) { return operator*=(u.deepCopy()); }  // Avoid aliasing
  u.lengthCheck(length());
  for(DoubleArrayLooper l(length(),stride(),u.stride()); l; ++l) {
    rwd_amuvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

DoubleArray& DoubleArray::operator/=(const DoubleArray& u)
{
  if (sameDataBlock(u)) { return operator/=(u.deepCopy()); }  // Avoid aliasing
  u.lengthCheck(length());
  for(DoubleArrayLooper l(length(),stride(),u.stride()); l; ++l) {
    rwd_advvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

/************************************************
 *                                              *
 *      ARITHMETIC ASSIGNMENT OPERATORS         *
 *                with a scalar                 *
 *                                              *
 ************************************************/

DoubleArray& DoubleArray::operator+=(double scalar)
{
  for(ArrayLooper l(length(),stride()); l; ++l) {
    rwd_aplvs(l.length, data()+l.start, l.stride, &scalar);
  }
  return *this;
}

DoubleArray& DoubleArray::operator*=(double scalar)
{
  for(ArrayLooper l(length(),stride()); l; ++l) {
    rwd_amuvs(l.length, data()+l.start, l.stride, &scalar);
  }
  return *this;
}


/************************************************
 *                                              *
 *              LOGICAL OPERATORS               *
 *                                              *
 ************************************************/

RWBoolean DoubleArray::operator==(const DoubleArray& u) const
{
  if(length()!=u.length()) return FALSE;  // They can't be equal if they don't have the same length.
  for(DoubleArrayLooper l(length(),stride(),u.stride()); l; ++l) {
    if (!rwdsame(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2)) {
      return FALSE;
    }
  }
  return TRUE;
}

RWBoolean DoubleArray::operator!=(const DoubleArray& u) const
{
  return !(*this == u);
}

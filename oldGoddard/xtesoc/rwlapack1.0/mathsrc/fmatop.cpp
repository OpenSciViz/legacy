/*
 * Definitions for various arithmetic operations for FloatGenMat
 *
 * Generated from template $Id: xmatop.cpp,v 1.10 1993/09/01 16:27:57 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/fgenmat.h"
#include "rw/rwsbla.h"	

RCSID("$Header: /users/rcs/mathsrc/xmatop.cpp,v 1.10 1993/09/01 16:27:57 alv Exp $");

/************************************************
 *                                              *
 *              UNARY OPERATORS                 *
 *                                              *
 ************************************************/

FloatGenMat operator-(const FloatGenMat& s)   // Unary minus
{
  FloatGenMat temp(s.rows(),s.cols(),rwUninitialized);
  float* dp = temp.data();
  for(MatrixLooper l(s.rows(),s.cols(),s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    int n = l.length;
    float* sp = (float*)s.data()+l.start;
    while(n--) { *dp++ = -(*sp); sp+=l.stride; }
  }
  return temp;
}

// Unary prefix increment on a FloatGenMat; (i.e. ++a)
FloatGenMat& FloatGenMat::operator++()
{
  for(MatrixLooper l(rows(),cols(),rowStride(),colStride()); l; ++l) {
    int n = l.length;
    float* sp = data()+l.start;
    while(n--) { ++(*sp); sp+=l.stride; }
  }
  return *this;
}

// Unary prefix decrement on a FloatGenMat (i.e., --a)
FloatGenMat&
FloatGenMat::operator--()
{
  for(MatrixLooper l(rows(),cols(),rowStride(),colStride()); l; ++l) {
    int n = l.length;
    float* sp = data()+l.start;
    while(n--) { --(*sp); sp+=l.stride; }
  }
  return *this;
}

/************************************************
 *                                              *
 *              BINARY OPERATORS                *
 *               vector - vector                *
 *                                              *
 ************************************************/

FloatGenMat operator+(const FloatGenMat& u, const FloatGenMat& v)
{
  unsigned m = v.rows();
  unsigned n = v.cols();
  u.lengthCheck(m,n);
  FloatGenMat temp(m,n,rwUninitialized);
  float* dp = temp.data();
  for(DoubleMatrixLooper l(m,n,u.rowStride(),u.colStride(),v.rowStride(),v.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rws_plvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

FloatGenMat operator-(const FloatGenMat& u, const FloatGenMat& v)
{
  unsigned m = v.rows();
  unsigned n = v.cols();
  u.lengthCheck(m,n);
  FloatGenMat temp(m,n,rwUninitialized);
  float* dp = temp.data();
  for(DoubleMatrixLooper l(m,n,u.rowStride(),u.colStride(),v.rowStride(),v.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rws_mivv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

FloatGenMat operator*(const FloatGenMat& u, const FloatGenMat& v)
{
  unsigned m = v.rows();
  unsigned n = v.cols();
  u.lengthCheck(m,n);
  FloatGenMat temp(m,n,rwUninitialized);
  float* dp = temp.data();
  for(DoubleMatrixLooper l(m,n,u.rowStride(),u.colStride(),v.rowStride(),v.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rws_muvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

FloatGenMat operator/(const FloatGenMat& u, const FloatGenMat& v)
{
  unsigned m = v.rows();
  unsigned n = v.cols();
  u.lengthCheck(m,n);
  FloatGenMat temp(m,n,rwUninitialized);
  float* dp = temp.data();
  for(DoubleMatrixLooper l(m,n,u.rowStride(),u.colStride(),v.rowStride(),v.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rws_dvvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

/************************************************
 *                                              *
 *              BINARY OPERATORS                *
 *               vector - scalar                *
 *                                              *
 ************************************************/

FloatGenMat operator+(const FloatGenMat& s, float scalar)
{
  unsigned m = s.rows();
  unsigned n = s.cols();
  FloatGenMat temp(m,n,rwUninitialized);
  float* dp = temp.data();
  for(MatrixLooper l(m,n,s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    float scalar2 = scalar;
    rws_plvs(l.length, dp, s.data()+l.start, l.stride, &scalar2);
    dp += l.length;
  }
  return temp;
}

FloatGenMat operator-(float scalar, const FloatGenMat& s)
{
  unsigned m = s.rows();
  unsigned n = s.cols();
  FloatGenMat temp(m,n,rwUninitialized);
  float* dp = temp.data();
  for(MatrixLooper l(m,n,s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    float scalar2 = scalar;
    rws_misv(l.length, dp, &scalar2, s.data()+l.start, l.stride);
    dp += l.length;
  }
  return temp;
}

FloatGenMat operator*(const FloatGenMat& s, float scalar)
{
  unsigned m = s.rows();
  unsigned n = s.cols();
  FloatGenMat temp(m,n,rwUninitialized);
  float* dp = temp.data();
  for(MatrixLooper l(m,n,s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    float scalar2 = scalar;
    rws_muvs(l.length, dp, s.data()+l.start, l.stride, &scalar2);
    dp += l.length;
  }
  return temp;
}

FloatGenMat operator/(float scalar, const FloatGenMat& s)
{
  unsigned m = s.rows();
  unsigned n = s.cols();
  FloatGenMat temp(m,n,rwUninitialized);
  float* dp = temp.data();
  for(MatrixLooper l(m,n,s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    float scalar2 = scalar;
    rws_dvsv(l.length, dp, &scalar2, s.data()+l.start, l.stride);
    dp += l.length;
  }
  return temp;
}


/*
 * Out-of-line versions for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
FloatGenMat     operator*(float s, const FloatGenMat& V)  {return V*s;}
FloatGenMat     operator+(float s, const FloatGenMat& V)  {return V+s;}
FloatGenMat     operator-(const FloatGenMat& V, float s)  {return V+(-s);}
FloatGenMat     operator/(const FloatGenMat& V, float s)  {return V*(1/s);}
#endif

/************************************************
 *                                              *
 *      ARITHMETIC ASSIGNMENT OPERATORS         *
 *        with other vectors                    *
 *                                              *
 ************************************************/

FloatGenMat& FloatGenMat::operator+=(const FloatGenMat& u)
{
  if (sameDataBlock(u)) { return operator+=(u.copy()); }  // Avoid aliasing
  u.lengthCheck(rows(),cols());
  for(DoubleMatrixLooper l(rows(),cols(),rowStride(),colStride(),u.rowStride(),u.colStride()); l; ++l) {
    rws_aplvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

FloatGenMat& FloatGenMat::operator-=(const FloatGenMat& u)
{
  if (sameDataBlock(u)) { return operator-=(u.copy()); }  // Avoid aliasing
  u.lengthCheck(rows(),cols());
  for(DoubleMatrixLooper l(rows(),cols(),rowStride(),colStride(),u.rowStride(),u.colStride()); l; ++l) {
    rws_amivv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

FloatGenMat& FloatGenMat::operator*=(const FloatGenMat& u)
{
  if (sameDataBlock(u)) { return operator*=(u.copy()); }  // Avoid aliasing
  u.lengthCheck(rows(),cols());
  for(DoubleMatrixLooper l(rows(),cols(),rowStride(),colStride(),u.rowStride(),u.colStride()); l; ++l) {
    rws_amuvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

FloatGenMat& FloatGenMat::operator/=(const FloatGenMat& u)
{
  if (sameDataBlock(u)) { return operator/=(u.copy()); }  // Avoid aliasing
  u.lengthCheck(rows(),cols());
  for(DoubleMatrixLooper l(rows(),cols(),rowStride(),colStride(),u.rowStride(),u.colStride()); l; ++l) {
    rws_advvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

/************************************************
 *                                              *
 *      ARITHMETIC ASSIGNMENT OPERATORS         *
 *                with a scalar                 *
 *                                              *
 ************************************************/

FloatGenMat& FloatGenMat::operator+=(float scalar)
{
  for(MatrixLooper l(rows(),cols(),rowStride(),colStride()); l; ++l) {
    float scalar2 = scalar;
    rws_aplvs(l.length, data()+l.start, l.stride, &scalar2);
  }
  return *this;
}

FloatGenMat& FloatGenMat::operator*=(float scalar)
{
  for(MatrixLooper l(rows(),cols(),rowStride(),colStride()); l; ++l) {
    float scalar2 = scalar;
    rws_amuvs(l.length, data()+l.start, l.stride, &scalar2);
  }
  return *this;
}


/************************************************
 *                                              *
 *              LOGICAL OPERATORS               *
 *                                              *
 ************************************************/

RWBoolean FloatGenMat::operator==(const FloatGenMat& u) const
{
  if(rows()!=u.rows() || cols()!=u.cols()) return FALSE;  // They can't be equal if they don't have the same length.
  for(DoubleMatrixLooper l(rows(),cols(),rowStride(),colStride(),u.rowStride(),u.colStride()); l; ++l) {
    if (!rwssame(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2)) {
      return FALSE;
    }
  }
  return TRUE;
}

RWBoolean FloatGenMat::operator!=(const FloatGenMat& u) const
{
  return !(*this == u);
}

/*
 * Type specific routines for FloatVec
 *
 * $Header: /users/rcs/mathsrc/fvecx.cpp,v 1.3 1993/09/01 16:27:19 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: fvecx.cpp,v $
 * Revision 1.3  1993/09/01  16:27:19  alv
 * now uses rwUninitialized form for simple constructors
 *
 * Revision 1.2  1993/01/26  17:43:14  alv
 * removed DOS EOF
 *
 * Revision 1.1  1993/01/22  23:50:59  alv
 * Initial revision
 *
 * 
 *    Rev 1.3   17 Oct 1991 09:15:18   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.2   08 Oct 1991 13:44:36   keffer
 * Math V4.0 beta 2
 * 
 *    Rev 1.1   24 Jul 1991 12:51:38   keffer
 * Added pvcs keywords
 *
 */

#include "rw/fvec.h"
#include "rw/dvec.h"

RCSID("$Header: /users/rcs/mathsrc/fvecx.cpp,v 1.3 1993/09/01 16:27:19 alv Exp $");

// Convert to DoubleVec.  
// Should be a friend, but.... 
FloatVec::operator DoubleVec() const
{
  register int n = length();
  DoubleVec temp(n,rwUninitialized);
  register double* dp = temp.data();
  register const float* tp = data();
  register int ts = stride();
  while(n--) {
    *dp++ = double(*tp); tp += ts;
  }
  return temp;
}


// narrow from DoubleVec to FloatVec
FloatVec
toFloat(const DoubleVec& v)
{
  register int n = v.length();
  FloatVec temp(n,rwUninitialized);
  register const double* dp = v.data();
  register float* ip = temp.data();
  register int vs = v.stride();
  while(n--) {
    *ip++ = float(*dp); dp += vs;
  }
  return temp;
}

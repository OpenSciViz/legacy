/*
 * Definitions for the double precision FFT server.
 *
 * $Header: /users/rcs/mathsrc/dfft.cpp,v 1.4 1993/09/01 16:27:13 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: dfft.cpp,v $
 * Revision 1.4  1993/09/01  16:27:13  alv
 * now uses rwUninitialized form for simple constructors
 *
 * Revision 1.3  1993/08/10  21:22:18  alv
 * now uses Tools v6 compatible error handling
 *
 * Revision 1.2  1993/02/24  21:03:46  alv
 * fixed bug that forbid finding FFTs of vectors with stride != 1
 *
 * Revision 1.1  1993/01/22  23:50:58  alv
 * Initial revision
 *
 * 
 *    Rev 1.5   15 Nov 1991 09:36:24   keffer
 * Removed RWMATHERR macro
 * 
 *    Rev 1.4   17 Oct 1991 09:15:16   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.2   26 Jul 1991 13:52:56   keffer
 * Now checks to see whether IMAG_LEADS has been defined, rather than
 * REAL_LEADS.  This supports the more usual case.
 * 
 */

#include "rw/dfft.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"
STARTWRAP
#include <stdio.h>
ENDWRAP

RCSID("$Header: /users/rcs/mathsrc/dfft.cpp,v 1.4 1993/09/01 16:27:13 alv Exp $");

DoubleFFTServer::DoubleFFTServer()
{
  serverLength=0;
}

DoubleFFTServer::DoubleFFTServer(unsigned Nlength)
{
  setOrder(Nlength);
}

DoubleFFTServer::DoubleFFTServer(const DoubleFFTServer& s) :
    DComplexFFTServer(*(const DComplexFFTServer *)&s),
    roots_of_1(s.roots_of_1),
    conjroots_of_1(s.conjroots_of_1)
{
  serverLength = s.serverLength;
}

void
DoubleFFTServer::operator=(const DoubleFFTServer& s)
{
  DComplexFFTServer::operator=(*(const DComplexFFTServer *)&s);
  serverLength = s.serverLength;
  roots_of_1.reference(s.roots_of_1);
  conjroots_of_1.reference(s.conjroots_of_1);
}

void
DoubleFFTServer::setOrder(unsigned Nlength)
{
  serverLength = Nlength;
  DComplexFFTServer::setOrder(serverLength/2);
  roots_of_1.reference(rootsOfOne(serverLength, serverLength/4+1));
  conjroots_of_1.reference(conj(roots_of_1));
}

/*
 * Performs DFT of a real sequence.  Must have an even number of points.
 * The forward fourier transform of a real sequence is a complex
 * conjugate even sequence.  If the original sequence had 2N points, the
 * result will have N + 1 complex points, for a total of 2N+2 points.
 * The extra two points are the imaginary parts of C(0) and C(N).  Both
 * are always zero.  Reference: Cooley, et al.  (1970) J. Sound Vib.
 * (12) 315--337.  This is algorithm #4.  See the class description
 * header for more information. 
 */

DComplexVec
DoubleFFTServer::fourier(const DoubleVec& v)
{
  unsigned Ntot     = v.length();
  unsigned N        = Ntot/2;
  unsigned Nhalf    = N/2;
  unsigned Nhalfp1  = N/2+1;

  // Various things that could go wrong:
  checkEven(Ntot);
  if(!Ntot) return DComplexVec();

  if(Ntot != serverLength) setOrder(Ntot);

  DComplexVec tempcomplex(v.slice(0,N,2), v.slice(1,N,2));

  DComplexVec A = DComplexFFTServer::fourier(tempcomplex);

  DComplexVec results(N+1,rwUninitialized);
  DComplexVec A1(Nhalfp1,rwUninitialized);
  DComplexVec A2(Nhalfp1,rwUninitialized);

  A1.slice(1,Nhalf,1) = conj(A.slice(N-1,Nhalf,-1)) + A.slice(1,Nhalf,1);
  A2.slice(1,Nhalf,1) = DComplexVec(imag(A.slice(N-1,Nhalf,-1))+imag(A.slice(1,Nhalf,1)),
				    real(A.slice(N-1,Nhalf,-1))-real(A.slice(1,Nhalf,1)));

  A1(0) = DComplex(2.0*real(A(0)),0);
  A2(0) = DComplex(2.0*imag(A(0)),0);

  DComplexVec temp = A2 * conjroots_of_1;
  results.slice(0,Nhalfp1, 1) = DComplex(0.5,0) *     (A1 + temp);
  results.slice(N,Nhalfp1,-1) = DComplex(0.5,0) * conj(A1 - temp);

  return results;
}

/*
 * Take the IDFT of a complex conjugate even sequence.  V is assumed to
 * be the lower half of the complex conjugate even sequence.  See the
 * comments in the class header about the format of even sequences.  The
 * results is a real periodic sequence.  Reference: Cooley, et al.
 * (1970) J. Sound Vib.  (12) 315--337.  This is algorithm #5.
 */

DoubleVec
DoubleFFTServer::ifourier(const DComplexVec& v)
{
  unsigned N        = v.length()-1;
  unsigned Ntot     = 2*N;
  unsigned Nhalf    = N/2;
  unsigned Nhalfp1  = N/2+1;

  if(Ntot != serverLength) setOrder(Ntot);

  DComplexVec lowslice = v.slice(0,Nhalfp1,1);
  DComplexVec hislice = conj(v.slice(N,Nhalfp1,-1));

  DComplexVec A1 = lowslice + hislice;
  DComplexVec A2 = (lowslice - hislice) * roots_of_1;
  
  DComplexVec A(N,rwUninitialized);
  A.slice(0,Nhalfp1,1)  = DComplexVec(real(A1)-imag(A2), real(A2)+imag(A1));
  A.slice(N-1,Nhalf,-1) = DComplexVec(real(A1.slice(1,Nhalf,1))+imag(A2.slice(1,Nhalf,1)),
				      real(A2.slice(1,Nhalf,1))-imag(A1.slice(1,Nhalf,1)));
  
  DComplexVec X = DComplexFFTServer::ifourier(A);

#if COMPLEX_PACKS
  DoubleVec results((double*)X.data(), Ntot);
#else
  DoubleVec results(Ntot);
  results.slice(0,N,2) = real(X);
  results.slice(1,N,2) = imag(X);
#endif
  return results;
}  

void
DoubleFFTServer::checkEven(int n)
{
  if( n%2 ){
    RWTHROW( RWInternalErr( RWMessage(RWMATH_BADPARITY, "DoubleFFTServer", (unsigned)n, "even" ) ));
  }
}

/*
 * Math definitions
 *
 * Generated from template $Id: xvecmath.cpp,v 1.7 1993/09/16 17:16:56 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/dvec.h"
#include "rw/rwdbla.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"

#define REGISTER register

RCSID("$Header: /users/rcs/mathsrc/xvecmath.cpp,v 1.7 1993/09/16 17:16:56 alv Exp $");




DoubleVec
DoubleVec::apply(mathFunTy f) const
// Apply function returning double to each element of vector slice
{
  register i = length();
  DoubleVec temp(i,rwUninitialized);
  register const double* sp = data();
  register double* dp = temp.data();
  register j = stride();
  while(i--) {
// Turbo C++ bug requires temporary.
#if defined (__TURBOC__)
     double dummy = f(*sp);
     *dp++ = dummy;
#else
     *dp++ = (*f)(*sp);
#endif
     sp += j; 
  }
  return temp;
}

#if defined(RW_NATIVE_EXTENDED)
DoubleVec
DoubleVec::apply(XmathFunTy f) const
// apply function for compilers using the extended type
{
  register i = length();
  DoubleVec temp(i,rwUninitialized);
  register const double* sp = data();
  register double* dp = temp.data();
  register j = stride();
  while(i--) {
    *dp++ = (*f)(*sp);
    sp += j;
  }
  return temp;
}
#endif


DoubleVec
atan2(const DoubleVec& u,const DoubleVec& v)
     // Arctangent of u/v.
{
  register unsigned i = v.length();
  u.lengthCheck(i);
  DoubleVec temp(i,rwUninitialized);
  register const double* up = u.data();
  register const double* vp = v.data();
  register double* dp = temp.data();
  register uj = u.stride();
  register vj = v.stride();
  while (i--) { *dp++ = atan2(*up,*vp);  up += uj;  vp += vj; }
  return temp;
}

DoubleVec
cumsum(const DoubleVec& s)
// Cumulative sum of DoubleVec slice.
// Note: V == delta(cumsum(V)) == cumsum(delta(V))
{
  unsigned i = s.length();
  DoubleVec temp(i,rwUninitialized);
  register double* sp = (double*)s.data();
  register double* dp = temp.data();
  register double c = 0;
  register j = s.stride();
  while (i--) { c += *sp;  *dp++ = c;  sp += j; }
  return temp;
}

DoubleVec
delta(const DoubleVec& s)
     // Element differences of DoubleVec slice.
     // Note: V == delta(cumsum(V)) == cumsum(delta(V))
{
  unsigned i = s.length();
  DoubleVec temp(i,rwUninitialized);
  register double* sp = (double*)s.data();
  register double* dp = temp.data();
  register double c = 0;
  register j = s.stride();
  while (i--) { *dp++ = *sp - c; c = *sp; sp += j; }
  return temp;
}

double
dot(const DoubleVec& u,const DoubleVec& v)
     // Vector dot product.
{
  unsigned N = v.length();
  u.lengthCheck(N);
  double result;
  rwddot(N, u.data(), u.stride(), v.data(), v.stride(), &result);
  return result;
}

int
maxIndex(const DoubleVec& s)
// Index of maximum element.
{
  if (s.length() == 0) s.emptyErr("maxIndex");
  int result = rwdmax(s.length(), s.data(), s.stride());
  return result;
}

double
maxValue(const DoubleVec& s)
{
  int inx = maxIndex(s);
  return s(inx);
}

int
minIndex(const DoubleVec& s)
     // Index of minimum element.
{
  if (s.length() == 0) s.emptyErr("minIndex");
  int result = rwdmin(s.length(), s.data(), s.stride());
  return result;
}

double
minValue(const DoubleVec& s)
{
  int inx = minIndex(s);
  return s(inx);
}


double
prod(const DoubleVec& s)
     // Product of DoubleVec elements.
{
  register unsigned i = s.length();
  register double* sp = (double*)s.data();
  REGISTER double t = 1;
  register j = s.stride();
  while (i--) { t *= *sp;  sp += j; }
  return t;
}

DoubleVec
pow(const DoubleVec& u, const DoubleVec& v)
// u to the v power.
{
  register unsigned i = v.length();
  u.lengthCheck(i);
  DoubleVec temp(i,rwUninitialized);
  register double* up = (double*)u.data();
  register double* vp = (double*)v.data();
  register double* dp = temp.data();
  register uj = u.stride();
  register vj = v.stride();
  while (i--) { *dp++ = pow(*up,*vp); up += uj; vp += vj; }
  return temp;
}

DoubleVec
reverse(const DoubleVec& s)
     // Reverse vector elements --- pretty tricky!
{
  return s.slice( s.length()-1, s.length(), -1);
}

double
sum(const DoubleVec& s)
     // Sum of a DoubleVec
{
  register unsigned i = s.length();
  register double* sp = (double*)s.data();
  register double t = 0;

  register j = s.stride();
  while (i--) { t += *sp;  sp += j; }
  return (double)t;
}

double
variance(const DoubleVec& s)
// Variance of a DoubleVec
{
  register unsigned i = s.length();
  if(i<2){
    RWTHROW( RWInternalErr( RWMessage( RWMATH_NPOINTS, "DoubleVec", (unsigned)i, "Variance needs minimum of", 2) ));
  }
  register double* sp = (double*)s.data();
  register double sum2 = 0;	// Accumulate sums in double precision
  REGISTER double avg = mean(s);
  register j = s.stride();
  while (i--) {
    REGISTER double temp = *sp - avg;
    sum2 += temp * temp;
    sp += j;
  }
  // Use the biased estimate (easier to work with):
  return (double)(sum2/s.length());
}

/*
 * Various functions with simple definitions:
 */
DoubleVec	acos(const DoubleVec& V)	{ return V.apply(::acos); }
DoubleVec	asin(const DoubleVec& V)	{ return V.apply(::asin); }
DoubleVec	atan(const DoubleVec& V)	{ return V.apply(::atan); }
DoubleVec	ceil(const DoubleVec& V)	{ return V.apply(::ceil); }
DoubleVec	cos(const DoubleVec& V)		{ return V.apply(::cos);  }
DoubleVec	cosh(const DoubleVec& V)	{ return V.apply(::cosh); }
DoubleVec	exp(const DoubleVec& V)		{ return V.apply(::exp);  }
DoubleVec	floor(const DoubleVec& V)	{ return V.apply(::floor);}
DoubleVec	log(const DoubleVec& V)		{ return V.apply(::log);  }
DoubleVec	log10(const DoubleVec& V)	{ return V.apply(::log10);}
double		mean(const DoubleVec& V)	{ return sum(V)/V.length();}
DoubleVec	sin(const DoubleVec& V)		{ return V.apply(::sin);  }
DoubleVec	sinh(const DoubleVec& V)	{ return V.apply(::sinh); }
DoubleVec	tan(const DoubleVec& V)		{ return V.apply(::tan);  }
DoubleVec	tanh(const DoubleVec& V)	{ return V.apply(::tanh); }

#ifdef __ZTC__
/*
 * Zortech requires its own special version of abs(const DoubleVec&)
 * because it uses an inlined version of fabs().  Hence, apply() 
 * cannot be used.
 */
DoubleVec
abs(const DoubleVec& s)
// Absolute value of a DoubleVec --- Zortech version
{
  register unsigned i = s.length();
  DoubleVec temp(i,rwUninitialized);
  register double* sp = (double*)s.data();
  register double* dp = temp.data();
  register j = s.stride();
  while (i--) { *dp++ = fabs(*sp); sp += j; }
  return temp;
}
#else   /* Not Zortech */
DoubleVec	abs(const DoubleVec& V)		{ return V.apply(::fabs); }
#endif	
#ifdef __ZTC__
/*
 * Zortech requires its own special version of sqrt(const DoubleVec&) 
 * because it uses an inlined version of sqrt().  Hence, apply() 
 * cannot be used.
 */
DoubleVec
sqrt(const DoubleVec& s)
// Square root of a DoubleVec --- Zortech version
{
  register unsigned i = s.length();
  DoubleVec temp(i,rwUninitialized);
  register double* sp = (double*)s.data();
  register double* dp = temp.data();
  register j = s.stride();
  while (i--) { *dp++ = sqrt(*sp); sp += j; }
  return temp;
}
#else
DoubleVec	sqrt(const DoubleVec& V)	{ return V.apply(::sqrt); }
#endif

    /**** Norm functions ****/
Double l2Norm(const DoubleVec& x)
{
  return sqrt(dot(x,x));
}

Double l1Norm(const DoubleVec& x)
{
  return sum(abs(x));
}
	  
Double linfNorm(const DoubleVec& x)
{
  return maxValue(abs(x));
}

Double maxNorm(const DoubleVec& x)
{
  return linfNorm(x);
}

Double frobNorm(const DoubleVec& x)
{
  return l2Norm(x);
}


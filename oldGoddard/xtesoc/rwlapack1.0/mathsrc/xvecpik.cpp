> #
> # $Header: /users/rcs/mathsrc/xvecpik.cpp,v 1.3 1993/08/10 21:22:18 alv Exp $
> #
> # $Log: xvecpik.cpp,v $
> # Revision 1.3  1993/08/10  21:22:18  alv
> # now uses Tools v6 compatible error handling
> #
> # Revision 1.2  1993/03/22  15:51:31  alv
> # changed PVCS Workfile keyword to RCS ID keyword
> #
> # Revision 1.1  1993/01/22  23:51:10  alv
> # Initial revision
> #
> # 
> #    Rev 1.4   15 Nov 1991 09:36:28   keffer
> # Removed RWMATHERR macro
> # 
> #    Rev 1.3   17 Oct 1991 09:15:34   keffer
> # Changed include path to <rw/xxx.h>
> # 
> define <ClassType>=Vec
> include macros
/*
 * Definitions for <T>Pick
 *
 * Generated from template $Id: xvecpik.cpp,v 1.3 1993/08/10 21:22:18 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */
#include "rw/<a>vec.h"
#include "rw/<a>vecpik.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"

STARTWRAP
#include <stdio.h>
ENDWRAP

RCSID("$Header: /users/rcs/mathsrc/xvecpik.cpp,v 1.3 1993/08/10 21:22:18 alv Exp $");

void
<T>VecPick::operator=(const <T>Vec& v)
{
  register n = v.length();
  lengthCheck(n);
  for(register int i = 0; i<n; i++) (*this)(i) = v(i);
}

void
<T>VecPick::operator=(const <T>VecPick& p)
{
  register n = p.length();
  lengthCheck(n);
  for(register int i = 0; i<n; i++) (*this)(i) = p(i);
}

void
<T>VecPick::operator=(<T> s)
{
  register n = length();
  for(register int i = 0; i<n; i++) (*this)(i) = s;
}

<T>Vec&
<T>Vec::operator=(const <T>VecPick& p)
{
  register n = length();
  p.lengthCheck(n);
  for(register int i = 0; i<n; i++) (*this)(i) = p(i);
  return *this;
}

<T>Vec::<T>Vec(const <T>VecPick& p)
 : RWVecView(p.length(),sizeof(<T>))
{
  register <T>* dp = data();
  for(register int i = 0; i<npts; i++) *dp++ = p(i);
}

<T>VecPick
<T>Vec::pick(const IntVec& ipick) {
  // Temporary necessary for cfront; will generate a warning:
  <T>VecPick temp(*this, ipick);
  return temp;
}

const <T>VecPick
<T>Vec::pick(const IntVec& ipick) const {
  // Temporary necessary for cfront; will generate a warning:
  <T>VecPick temp(*(<T>Vec*)this, ipick);  // without cast of this, a temporary <T>Vec is constructed
  return temp;
}

/********************** U T I L I T I E S ***********************/

void
<T>VecPick::assertElements() const
{
  unsigned N  = pick.length();
  unsigned VN = V.length();
  for(int i=0; i<N; i++){
     if( pick(i) < 0 || pick(i) >= VN ){
       RWTHROW( RWBoundsErr( RWMessage(RWMATH_INDEX,(int)pick(i), (unsigned)(V.length()-1)) ));
     }
  }
}

void
<T>VecPick::lengthCheck(unsigned n) const
{
  if(length() != n){
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_VNMATCH, (unsigned)length(), n) ));
  }
}

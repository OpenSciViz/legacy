/*
 * Definitions for UCharArray
 *
 * Generated from template $Id: xarr.cpp,v 1.12 1993/10/04 21:44:01 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1992. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include <rw/strstrea.h>  /* I use istrstream to parse character string ctor */
#include "rw/ucarr.h"
#include "rw/ucgenmat.h"
#include "rw/ucvec.h"
#include "rw/rwbla.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"
#include "rw/rand.h"

#define REGISTER register


RCSID("$Header");

UCharArray::UCharArray(const char *string)
 : RWArrayView(IntVec(),sizeof(UChar))
{
  istrstream s((char*)string);
  scanFrom(s);
  if (!s.good()) {
    RWTHROW( RWExternalErr( RWMessage(RWMATH_STRCTOR) ));
  }
}

UCharArray::UCharArray()
  : RWArrayView(IntVec(),sizeof(UChar))
{
}

UCharArray::UCharArray(const IntVec& n, RWUninitialized, Storage storage)
  : RWArrayView(n,sizeof(UChar),storage)
{
}

UCharArray::UCharArray(unsigned nx, unsigned ny, unsigned nz, RWUninitialized)
  : RWArrayView(nx,ny,nz,sizeof(UChar))
{
}

UCharArray::UCharArray(unsigned nx, unsigned ny, unsigned nz, unsigned nw, RWUninitialized)
  : RWArrayView(nx,ny,nz,nw,sizeof(UChar))
{
}

static void randFill(UCharArray& X, RWRand& r)
{
  for(RWMultiIndex i(X.length()); i; ++i) {
    X(i) = (UChar)r();
  }
}

UCharArray::UCharArray(const IntVec& n, RWRand& r, Storage storage)
  : RWArrayView(n,sizeof(UChar),storage)
{
  randFill(*this,r);
}

UCharArray::UCharArray(unsigned nx, unsigned ny, unsigned nz, RWRand& r)
  : RWArrayView(nx,ny,nz,sizeof(UChar))
{
  randFill(*this,r);
}

UCharArray::UCharArray(unsigned nx, unsigned ny, unsigned nz, unsigned nw, RWRand& r)
  : RWArrayView(nx,ny,nz,nw,sizeof(UChar))
{
  randFill(*this,r);
}

UCharArray::UCharArray(const IntVec& n, UChar scalar)
  : RWArrayView(n,sizeof(UChar))
{
  UChar scalar2 = scalar;  // Temporary required to finesse K&R compilers
  rwbset(prod(n), data(), 1, &scalar2);
}

UCharArray::UCharArray(unsigned nx, unsigned ny, unsigned nz, UChar scalar)
  : RWArrayView(nx,ny,nz,sizeof(UChar))
{
  UChar scalar2 = scalar;  // Temporary required to finesse K&R compilers
  rwbset(nx*ny*nz, data(), 1, &scalar2);
}

UCharArray::UCharArray(unsigned nx, unsigned ny, unsigned nz, unsigned nw, UChar scalar)
  : RWArrayView(nx,ny,nz,nw,sizeof(UChar))
{
  UChar scalar2 = scalar;  // Temporary required to finesse K&R compilers
  rwbset(nx*ny*nz*nw, data(), 1, &scalar2);
}


// Copy constructor
UCharArray::UCharArray(const UCharArray& a)
  : RWArrayView(a)
{
}

UCharArray::UCharArray(const UChar* dat, const IntVec& n)
  : RWArrayView(n,sizeof(UChar))
{
  rwbcopy(prod(n), data(), 1, dat, 1);
}

UCharArray::UCharArray(const UChar* dat, unsigned nx, unsigned ny, unsigned nz)
  : RWArrayView(nx,ny,nz,sizeof(UChar))
{
  rwbcopy(nx*ny*nz, data(), 1, dat, 1);
}

UCharArray::UCharArray(const UChar* dat, unsigned nx, unsigned ny, unsigned nz, unsigned nw)
  : RWArrayView(nx,ny,nz,nw,sizeof(UChar))
{
  rwbcopy(nx*ny*nz*nw, data(), 1, dat, 1);
}

UCharArray::UCharArray(const UCharVec& dat, const IntVec& n)
  : RWArrayView(n,sizeof(UChar))
{
  rwbcopy(prod(n), data(), 1, dat.data(), dat.stride());
}

UCharArray::UCharArray(const UCharVec& dat, unsigned nx, unsigned ny, unsigned nz)
  : RWArrayView(nx,ny,nz,sizeof(UChar))
{
  rwbcopy(nx*ny*nz, data(), 1, dat.data(), dat.stride());
}

UCharArray::UCharArray(const UCharVec& dat, unsigned nx, unsigned ny, unsigned nz, unsigned nw)
  : RWArrayView(nx,ny,nz,nw,sizeof(UChar))
{
  rwbcopy(nx*ny*nz*nw, data(), 1, dat.data(), dat.stride());
}

UCharArray::UCharArray(RWBlock *block, const IntVec& n)
  : RWArrayView(block,n)
{
  unsigned minlen = prod(length())*sizeof(UChar);
  if (block->length() < minlen) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_SHORTBLOCK, block->length(), minlen) ));
  }
}

UCharArray::UCharArray(RWBlock *block, unsigned nx, unsigned ny, unsigned nz)
  : RWArrayView(block, nx, ny, nz)
{
  unsigned minlen = prod(length())*sizeof(UChar);
  if (block->length() < minlen) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_SHORTBLOCK, block->length(), minlen) ));
  }
}

UCharArray::UCharArray(RWBlock *block, unsigned nx, unsigned ny, unsigned nz, unsigned nw)
  : RWArrayView(block, nx, ny, nz, nw)
{
  unsigned minlen = prod(length())*sizeof(UChar);
  if (block->length() < minlen) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_SHORTBLOCK, block->length(), minlen) ));
  }
}

UCharArray::UCharArray(const UCharVec& v)
  : RWArrayView(v,(void*)v.data(),IntVec(1,v.length()),IntVec(1,1))
{
}

UCharArray::UCharArray(const UCharGenMat& m)
  : RWArrayView(m,(void*)m.data(),
       IntVec(2,m.rows(),m.cols()-m.rows()),                 // [ m.rows() m.cols() ]
       IntVec(2,m.rowStride(),m.colStride()-m.rowStride())) // [ m.rowStride() m.colStride() ]
{
}

// This constructor is used internally by real() and imag() and slices:
UCharArray::UCharArray(const RWDataView& b, UChar* start, const IntVec& n, const IntVec& str)
  : RWArrayView(b,start,n,str)
{
}

UCharArray& UCharArray::operator=(const UCharArray& rhs)
{
  lengthCheck(rhs.length());
  if(sameDataBlock(rhs)) {
    (*this) = rhs.deepCopy();   // Avoid aliasing problems
  } else {
    for(DoubleArrayLooper l(npts,step,rhs.step); l; ++l) {
      rwbcopy(l.length, data()+l.start1, l.stride1, rhs.data()+l.start2, l.stride2);
    }
  }
  return *this;
}

UCharArray& UCharArray::operator=(UChar scalar)
{
  for(ArrayLooper l(npts,step); l; ++l) {
    UChar scalar2 = scalar;  // Temporary required to finesse K&R compilers
    rwbset(l.length,data()+l.start,l.stride,&scalar2);
  }
  return *this;
}

UCharArray& UCharArray::reference(const UCharArray& v)
{
  RWArrayView::reference(v);
  return *this;
}

// Return copy of self with distinct instance variables
UCharArray UCharArray::deepCopy() const
{
  return copy();
}

// Synonym for deepCopy().  Not made inline because some compilers
// have trouble with inlined temporaries with destructors
UCharArray UCharArray::copy() const
{
  UCharArray temp(npts,rwUninitialized,COLUMN_MAJOR);
  temp = *this;
  return temp;
}

// Guarantee that references==1 and stride==1
void UCharArray::deepenShallowCopy()
{
  RWBoolean isStrideCompact = (dimension()==0 || step((int)0)==1);
  for( int r=npts.length()-1; r>0 && isStrideCompact; --r ) {
    if (step(r) != npts(r-1)*step(r-1)) isStrideCompact=FALSE;
  }
  if (!isStrideCompact || !isSimpleView()) {
    RWArrayView::reference(copy());
  }
}

void UCharArray::resize(const IntVec& N)
{
  if(N != npts){
    UCharArray temp(N,(UChar)0);
    int p = npts.length()<N.length() ? npts.length() : N.length();  // # dimensions to copy
    if (p>0) {
      IntVec n(p,rwUninitialized);   // The size of the common parts of the arrays
      for(int i=0; i<p; i++) {
        n(i) = (N(i)<npts(i)) ? N(i) : npts(i);
      }
  
      // Build a view of the source common part
      IntGenMat sstr(dimension(),p,0);
      sstr.diagonal() = 1;
      UCharArray sp = slice(IntVec(dimension(),0), n, sstr);
  
      // Build a view of the destination common part
      IntGenMat dstr(N.length(),p,0);
      dstr.diagonal() = 1;
      UCharArray dp = temp.slice(IntVec(N.length(),0), n, dstr);
  
      dp = sp;
    }
    reference(temp);
  }
}

void UCharArray::resize(unsigned m, unsigned n, unsigned o)
{
  IntVec i(3,rwUninitialized);
  i((int)0) = m;
  i(1) = n;
  i(2) = o;
  resize(i);
}

void UCharArray::resize(unsigned m, unsigned n, unsigned o, unsigned p)
{
  IntVec i(4,rwUninitialized);
  i((int)0) = m;
  i(1) = n;
  i(2) = o;
  i(3) = p;
  resize(i);
}

void UCharArray::reshape(const IntVec& N)
{
  if (N!=npts) {
    UCharArray temp(N,rwUninitialized,COLUMN_MAJOR);
    reference(temp);
  }
}

void UCharArray::reshape(unsigned m, unsigned n, unsigned o)
{
  IntVec i(3,rwUninitialized);
  i((int)0) = m;
  i(1) = n;
  i(2) = o;
  reshape(i);
}

void UCharArray::reshape(unsigned m, unsigned n, unsigned o, unsigned p)
{
  IntVec i(4,rwUninitialized);
  i((int)0) = m;
  i(1) = n;
  i(2) = o;
  i(3) = p;
  reshape(i);
}

UCharArray UCharArray::slice(const IntVec& start, const IntVec& n, const IntGenMat& str) const
{
  sliceCheck(start, n, str);
  return UCharArray(*this, (UChar*)data()+dot(start,stride()), n, product(stride(),str));
}

UChar toScalar(const UCharArray& A)
{
  A.dimensionCheck(0);
  return *(A.data());
}

UCharVec toVec(const UCharArray& A)
{
  A.dimensionCheck(1);
  return UCharVec(A,(UChar*)A.data(),(unsigned)A.length((int)0),A.stride((int)0));
}

UCharGenMat toGenMat(const UCharArray& A)
{
  A.dimensionCheck(2);
  return UCharGenMat(A,(UChar*)A.data(),(unsigned)A.length((int)0),(unsigned)A.length(1),A.stride((int)0),A.stride(1));
}


/*
 * Definitions for class RandNormal.
 *
 * $Header: /users/rcs/mathsrc/randnorm.cpp,v 1.4 1993/09/17 00:08:26 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: randnorm.cpp,v $
 * Revision 1.4  1993/09/17  00:08:26  alv
 * removed statics so multithreading and independent generators work
 *
 * Revision 1.3  1993/09/01  16:27:34  alv
 * now uses rwUninitialized form for simple constructors
 *
 * Revision 1.2  1993/01/26  17:52:28  alv
 * removed DOS EOF
 *
 * Revision 1.1  1993/01/22  23:51:02  alv
 * Initial revision
 *
 * 
 *    Rev 1.3   17 Oct 1991 09:15:26   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.2   08 Oct 1991 13:44:48   keffer
 * Math V4.0 beta 2
 * 
 *    Rev 1.1   24 Jul 1991 12:51:52   keffer
 * Added pvcs keywords
 *
 */

#include "rw/dvec.h"
#include "rw/dgenmat.h"
#include "rw/randnorm.h"
STARTWRAP
#include <math.h>
ENDWRAP

// Return a random number selected from an normal distribution.
double
RandNormal::randValue() {
  double r,v1,v2;
    do {
       v1 = 2*RandUniform::randValue() - 1.; // Pick two uniform 
       v2 = 2*RandUniform::randValue() - 1.; // numbers fron -1 to 1.
       r = v1*v1 + v2*v2;
    } while ( r >= 1.);               // Make sure they're in the unit circle.
    double f = sqrt(-2.*::log(r)/r);   // Box-Muller transformation.
    return (v2 * f);                   // Return the other number.
}

// Return an array of random numbers selected from a normal distribution.
void
RandNormal::randValue(double* d, unsigned n) 
{
  while (n--) { *d++ = RandNormal::randValue(); }
}


// Return a vector of n random numbers selected from a normal distribution
DoubleVec
RandNormal::randValue(unsigned n) {
  DoubleVec temp(n,rwUninitialized);
  while (n--) temp(n) = RandNormal::randValue();
  return temp;
}

// Return a matrix of random numbers selected from a normal distribution .
DoubleGenMat
RandNormal::randValue(unsigned nr, unsigned nc) {
  DoubleVec tempv = RandNormal::randValue(nr*nc);
  DoubleGenMat temp(tempv, nr, nc);
  return temp;
}

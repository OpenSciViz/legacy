; 
; MASM common definitions
; 
; $Header: /users/rcs/mathsrc/rwmasm.inc,v 1.1 1993/01/22 23:55:39 alv Exp $
; 
; ***************************************************************************
; 
; Rogue Wave 
; P.O. Box 2328
; Corvallis, OR 97339
; Voice: (503) 754-2311	FAX: (503) 757-7350
; 
; Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
; protection under the laws of the United States and other countries.
; 
; ***************************************************************************
; 
; $Log: rwmasm.inc,v $
; Revision 1.1  1993/01/22  23:55:39  alv
; Initial revision
;
; 
;    Rev 1.1   25 Oct 1991 11:34:16   keffer
; No change.
; 
;    Rev 1.0   08 Oct 1991 13:47:54   keffer
; Initial revision.
; 

;
; This header file depends on one of the following being defined on the
; MASM command line in order to set the memory model:
;
;	I8086S:	Small memory model
;	I8086M:	Medium memory model
;	I8086C:	Compact memory model
;	I8086L:	Large memory model
;

IFDEF I8086S
memmodel        EQU     <SMALL>
ELSEIFDEF I8086M
memmodel        EQU     <MEDIUM>
ELSEIFDEF I8086C
memmodel        EQU     <COMPACT>
ELSEIFDEF I8086L
memmodel        EQU     <LARGE>
ELSE
%err
%out    No proper memory model defined
ENDIF

;	Detect whether we are using TASM:
IFDEF ??version
;	Enable local symbols when using TASM
LOCALS
ENDIF


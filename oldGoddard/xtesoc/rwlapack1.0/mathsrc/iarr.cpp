/*
 * Definitions for IntArray
 *
 * Generated from template $Id: xarr.cpp,v 1.12 1993/10/04 21:44:01 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1992. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include <rw/strstrea.h>  /* I use istrstream to parse character string ctor */
#include "rw/iarr.h"
#include "rw/igenmat.h"
#include "rw/ivec.h"
#include "rw/rwbla.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"
#include "rw/rand.h"

#define REGISTER register


RCSID("$Header");

IntArray::IntArray(const char *string)
 : RWArrayView(IntVec(),sizeof(int))
{
  istrstream s((char*)string);
  scanFrom(s);
  if (!s.good()) {
    RWTHROW( RWExternalErr( RWMessage(RWMATH_STRCTOR) ));
  }
}

IntArray::IntArray()
  : RWArrayView(IntVec(),sizeof(int))
{
}

IntArray::IntArray(const IntVec& n, RWUninitialized, Storage storage)
  : RWArrayView(n,sizeof(int),storage)
{
}

IntArray::IntArray(unsigned nx, unsigned ny, unsigned nz, RWUninitialized)
  : RWArrayView(nx,ny,nz,sizeof(int))
{
}

IntArray::IntArray(unsigned nx, unsigned ny, unsigned nz, unsigned nw, RWUninitialized)
  : RWArrayView(nx,ny,nz,nw,sizeof(int))
{
}

static void randFill(IntArray& X, RWRand& r)
{
  for(RWMultiIndex i(X.length()); i; ++i) {
    X(i) = (int)r();
  }
}

IntArray::IntArray(const IntVec& n, RWRand& r, Storage storage)
  : RWArrayView(n,sizeof(int),storage)
{
  randFill(*this,r);
}

IntArray::IntArray(unsigned nx, unsigned ny, unsigned nz, RWRand& r)
  : RWArrayView(nx,ny,nz,sizeof(int))
{
  randFill(*this,r);
}

IntArray::IntArray(unsigned nx, unsigned ny, unsigned nz, unsigned nw, RWRand& r)
  : RWArrayView(nx,ny,nz,nw,sizeof(int))
{
  randFill(*this,r);
}

IntArray::IntArray(const IntVec& n, int scalar)
  : RWArrayView(n,sizeof(int))
{
  rwiset(prod(n),data(),1,&scalar);
}

IntArray::IntArray(unsigned nx, unsigned ny, unsigned nz, int scalar)
  : RWArrayView(nx,ny,nz,sizeof(int))
{
  rwiset(nx*ny*nz,data(),1,&scalar);
}

IntArray::IntArray(unsigned nx, unsigned ny, unsigned nz, unsigned nw, int scalar)
  : RWArrayView(nx,ny,nz,nw,sizeof(int))
{
  rwiset(nx*ny*nz*nw,data(),1,&scalar);
}


// Copy constructor
IntArray::IntArray(const IntArray& a)
  : RWArrayView(a)
{
}

IntArray::IntArray(const int* dat, const IntVec& n)
  : RWArrayView(n,sizeof(int))
{
  rwicopy(prod(n), data(), 1, dat, 1);
}

IntArray::IntArray(const int* dat, unsigned nx, unsigned ny, unsigned nz)
  : RWArrayView(nx,ny,nz,sizeof(int))
{
  rwicopy(nx*ny*nz, data(), 1, dat, 1);
}

IntArray::IntArray(const int* dat, unsigned nx, unsigned ny, unsigned nz, unsigned nw)
  : RWArrayView(nx,ny,nz,nw,sizeof(int))
{
  rwicopy(nx*ny*nz*nw, data(), 1, dat, 1);
}

IntArray::IntArray(const IntVec& dat, const IntVec& n)
  : RWArrayView(n,sizeof(int))
{
  rwicopy(prod(n), data(), 1, dat.data(), dat.stride());
}

IntArray::IntArray(const IntVec& dat, unsigned nx, unsigned ny, unsigned nz)
  : RWArrayView(nx,ny,nz,sizeof(int))
{
  rwicopy(nx*ny*nz, data(), 1, dat.data(), dat.stride());
}

IntArray::IntArray(const IntVec& dat, unsigned nx, unsigned ny, unsigned nz, unsigned nw)
  : RWArrayView(nx,ny,nz,nw,sizeof(int))
{
  rwicopy(nx*ny*nz*nw, data(), 1, dat.data(), dat.stride());
}

IntArray::IntArray(RWBlock *block, const IntVec& n)
  : RWArrayView(block,n)
{
  unsigned minlen = prod(length())*sizeof(int);
  if (block->length() < minlen) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_SHORTBLOCK, block->length(), minlen) ));
  }
}

IntArray::IntArray(RWBlock *block, unsigned nx, unsigned ny, unsigned nz)
  : RWArrayView(block, nx, ny, nz)
{
  unsigned minlen = prod(length())*sizeof(int);
  if (block->length() < minlen) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_SHORTBLOCK, block->length(), minlen) ));
  }
}

IntArray::IntArray(RWBlock *block, unsigned nx, unsigned ny, unsigned nz, unsigned nw)
  : RWArrayView(block, nx, ny, nz, nw)
{
  unsigned minlen = prod(length())*sizeof(int);
  if (block->length() < minlen) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_SHORTBLOCK, block->length(), minlen) ));
  }
}

IntArray::IntArray(const IntVec& v)
  : RWArrayView(v,(void*)v.data(),IntVec(1,v.length()),IntVec(1,1))
{
}

IntArray::IntArray(const IntGenMat& m)
  : RWArrayView(m,(void*)m.data(),
       IntVec(2,m.rows(),m.cols()-m.rows()),                 // [ m.rows() m.cols() ]
       IntVec(2,m.rowStride(),m.colStride()-m.rowStride())) // [ m.rowStride() m.colStride() ]
{
}

// This constructor is used internally by real() and imag() and slices:
IntArray::IntArray(const RWDataView& b, int* start, const IntVec& n, const IntVec& str)
  : RWArrayView(b,start,n,str)
{
}

IntArray& IntArray::operator=(const IntArray& rhs)
{
  lengthCheck(rhs.length());
  if(sameDataBlock(rhs)) {
    (*this) = rhs.deepCopy();   // Avoid aliasing problems
  } else {
    for(DoubleArrayLooper l(npts,step,rhs.step); l; ++l) {
      rwicopy(l.length, data()+l.start1, l.stride1, rhs.data()+l.start2, l.stride2);
    }
  }
  return *this;
}

IntArray& IntArray::operator=(int scalar)
{
  for(ArrayLooper l(npts,step); l; ++l) {
    rwiset(l.length,data()+l.start,l.stride,&scalar);
  }
  return *this;
}

IntArray& IntArray::reference(const IntArray& v)
{
  RWArrayView::reference(v);
  return *this;
}

// Return copy of self with distinct instance variables
IntArray IntArray::deepCopy() const
{
  return copy();
}

// Synonym for deepCopy().  Not made inline because some compilers
// have trouble with inlined temporaries with destructors
IntArray IntArray::copy() const
{
  IntArray temp(npts,rwUninitialized,COLUMN_MAJOR);
  temp = *this;
  return temp;
}

// Guarantee that references==1 and stride==1
void IntArray::deepenShallowCopy()
{
  RWBoolean isStrideCompact = (dimension()==0 || step((int)0)==1);
  for( int r=npts.length()-1; r>0 && isStrideCompact; --r ) {
    if (step(r) != npts(r-1)*step(r-1)) isStrideCompact=FALSE;
  }
  if (!isStrideCompact || !isSimpleView()) {
    RWArrayView::reference(copy());
  }
}

void IntArray::resize(const IntVec& N)
{
  if(N != npts){
    IntArray temp(N,(int)0);
    int p = npts.length()<N.length() ? npts.length() : N.length();  // # dimensions to copy
    if (p>0) {
      IntVec n(p,rwUninitialized);   // The size of the common parts of the arrays
      for(int i=0; i<p; i++) {
        n(i) = (N(i)<npts(i)) ? N(i) : npts(i);
      }
  
      // Build a view of the source common part
      IntGenMat sstr(dimension(),p,0);
      sstr.diagonal() = 1;
      IntArray sp = slice(IntVec(dimension(),0), n, sstr);
  
      // Build a view of the destination common part
      IntGenMat dstr(N.length(),p,0);
      dstr.diagonal() = 1;
      IntArray dp = temp.slice(IntVec(N.length(),0), n, dstr);
  
      dp = sp;
    }
    reference(temp);
  }
}

void IntArray::resize(unsigned m, unsigned n, unsigned o)
{
  IntVec i(3,rwUninitialized);
  i((int)0) = m;
  i(1) = n;
  i(2) = o;
  resize(i);
}

void IntArray::resize(unsigned m, unsigned n, unsigned o, unsigned p)
{
  IntVec i(4,rwUninitialized);
  i((int)0) = m;
  i(1) = n;
  i(2) = o;
  i(3) = p;
  resize(i);
}

void IntArray::reshape(const IntVec& N)
{
  if (N!=npts) {
    IntArray temp(N,rwUninitialized,COLUMN_MAJOR);
    reference(temp);
  }
}

void IntArray::reshape(unsigned m, unsigned n, unsigned o)
{
  IntVec i(3,rwUninitialized);
  i((int)0) = m;
  i(1) = n;
  i(2) = o;
  reshape(i);
}

void IntArray::reshape(unsigned m, unsigned n, unsigned o, unsigned p)
{
  IntVec i(4,rwUninitialized);
  i((int)0) = m;
  i(1) = n;
  i(2) = o;
  i(3) = p;
  reshape(i);
}

IntArray IntArray::slice(const IntVec& start, const IntVec& n, const IntGenMat& str) const
{
  sliceCheck(start, n, str);
  return IntArray(*this, (int*)data()+dot(start,stride()), n, product(stride(),str));
}

int toScalar(const IntArray& A)
{
  A.dimensionCheck(0);
  return *(A.data());
}

IntVec toVec(const IntArray& A)
{
  A.dimensionCheck(1);
  return IntVec(A,(int*)A.data(),(unsigned)A.length((int)0),A.stride((int)0));
}

IntGenMat toGenMat(const IntArray& A)
{
  A.dimensionCheck(2);
  return IntGenMat(A,(int*)A.data(),(unsigned)A.length((int)0),(unsigned)A.length(1),A.stride((int)0),A.stride(1));
}


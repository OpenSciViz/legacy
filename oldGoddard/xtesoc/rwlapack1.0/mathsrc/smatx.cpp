/*
 * Type conversion routines for SCharGenMat
 *
 * $Header: /users/rcs/mathsrc/smatx.cpp,v 1.3 1993/09/01 16:27:41 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: smatx.cpp,v $
 * Revision 1.3  1993/09/01  16:27:41  alv
 * now uses rwUninitialized form for simple constructors
 *
 * Revision 1.2  1993/01/26  21:54:57  alv
 * fixed wrong cast bug
 *
 * Revision 1.1  1993/01/22  23:51:04  alv
 * Initial revision
 *
 * 
 *    Rev 1.3   17 Oct 1991 09:15:38   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.2   08 Oct 1991 13:45:00   keffer
 * Math V4.0 beta 2
 * 
 *    Rev 1.1   24 Jul 1991 12:52:00   keffer
 * Added pvcs keywords
 *
 */

#include "rw/scgenmat.h"
#include "rw/igenmat.h"

RCSID("$Header: /users/rcs/mathsrc/smatx.cpp,v 1.3 1993/09/01 16:27:41 alv Exp $");

// Convert to an IntGenMat
// Should be a friend of IntGenMat, but...
SCharGenMat::operator IntGenMat() const
{
  IntGenMat temp(rows(),cols(),rwUninitialized);
  int *dp = temp.data();
  for(MatrixLooper l(rows(),cols(),rowStride(),colStride()); l; ++l) {
    int n = l.length;
    const SChar *tp = data()+l.start;
    while(n--) {
      *dp++ = int(*tp); tp+=l.stride;
    }
  }
  return temp;
}

// narrow from IntGenMat to signed SCharGenMat
SCharGenMat
toChar(const IntGenMat& v)
{
  SCharGenMat temp(v.rows(),v.cols(),rwUninitialized);
  SChar *dp = temp.data();
  for(MatrixLooper l(v.rows(),v.cols(),v.rowStride(),v.colStride()); l; ++l) {
    int n = l.length;
    const int *tp = v.data()+l.start;
    while(n--) {
      *dp++ = SChar(*tp); tp+=l.stride;
    }
  }
  return temp;
}

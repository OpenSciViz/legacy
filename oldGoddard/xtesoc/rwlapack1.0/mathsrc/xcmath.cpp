> #
> # $Header: /users/rcs/mathsrc/xcmath.cpp,v 1.5 1993/09/17 18:42:21 alv Exp $
> #
> # $Log: xcmath.cpp,v $
> # Revision 1.5  1993/09/17  18:42:21  alv
> # (0) -> ((int)0) for benefit of DEC compiler
> #
> # Revision 1.4  1993/09/01  16:27:53  alv
> # now uses rwUninitialized form for simple constructors
> #
> # Revision 1.3  1993/08/10  21:22:18  alv
> # now uses Tools v6 compatible error handling
> #
> # Revision 1.2  1993/03/22  15:51:31  alv
> # changed PVCS Workfile keyword to RCS ID keyword
> #
> # Revision 1.1  1993/01/22  23:51:07  alv
> # Initial revision
> #
> # 
> #    Rev 1.4   15 Nov 1991 09:36:26   keffer
> # Removed RWMATHERR macro
> # 
> #    Rev 1.3   17 Oct 1991 09:15:28   keffer
> # Changed include path to <rw/xxx.h>
> # 
> include macros
/*
 * <T>Vec special math functions
 *
 * Generated from template $Id: xcmath.cpp,v 1.5 1993/09/17 18:42:21 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

/*
 *  This file contains functions peculiar to complex vectors
 *  for precision <P>.
 */

#include "rw/<a>vec.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"
STARTWRAP
#include <math.h>
ENDWRAP

RCSID("$Header: /users/rcs/mathsrc/xcmath.cpp,v 1.5 1993/09/17 18:42:21 alv Exp $");

/*
 * Return first nterms of the N'th order roots of one.
 */

<T>Vec
rootsOfOne(int N, unsigned nterms)
{
  /* Use the recursion formulae
   *  sin(a+b) = sin(a)cos(b) + cos(a)sin(b)
   *  cos(a+b) = cos(a)cos(b) - sin(a)sin(b)
   */

  if(N==0) {
    RWTHROW( RWInternalErr( RWMessage(RWMATH_ZROOT) ));
  }

  <P> b = 2.0*M_PI/N;
  register <P> cosb = cos(b);
  register <P> sinb = sin(b);

  register <P> cosa = 1.0;		// implies a=0
  register <P> sina = 0.0;

  <T>Vec roots(nterms,rwUninitialized);

  if(nterms){
    roots((int)0) = <T>(cosa, sina);

    register <P> temp;
    for(register int i = 1; i<nterms; i++){
      temp = cosa*cosb - sina*sinb;
      sina = sina*cosb + cosa*sinb;
      cosa = temp;
      roots(i) = <T>(cosa, sina);
    }
  }

  return roots;
}

/*
 * Expand a complex conjugate even series into its full series.
 */

<T>Vec
expandConjugateEven(const <T>Vec& v)
{
  unsigned N = v.length()-1;
  <T>Vec full(2*N,rwUninitialized);

  // Lower half:
  full.slice(0,N+1,1) = v;
  // Upper half:
  full.slice(N+1,N-1,1) = conj(v.slice(N-1,N-1,-1));

  return full;
}

/*
 * Expand a complex conjugate odd series into its full series.
 */

<T>Vec
expandConjugateOdd(const <T>Vec& v)
{
  unsigned N = v.length()-1;
  <T>Vec full(2*N,rwUninitialized);

  // Lower half:
  full.slice(0,N+1,1) = v;
  // Upper half:
  full.slice(N+1,N-1,1) = conj(-v.slice(N-1,N-1,-1));

  return full;
}

<P>
spectralVariance(const <T>Vec& v)
{
  // Do not count the mean v(0):
  return sum(norm(v.slice(1,v.length()-1,1)));
}

/*
 * Definitions for SCharArray
 *
 * Generated from template $Id: xarr.cpp,v 1.12 1993/10/04 21:44:01 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1992. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include <rw/strstrea.h>  /* I use istrstream to parse character string ctor */
#include "rw/scarr.h"
#include "rw/scgenmat.h"
#include "rw/scvec.h"
#include "rw/rwbla.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"
#include "rw/rand.h"

#define REGISTER register


RCSID("$Header");

SCharArray::SCharArray(const char *string)
 : RWArrayView(IntVec(),sizeof(SChar))
{
  istrstream s((char*)string);
  scanFrom(s);
  if (!s.good()) {
    RWTHROW( RWExternalErr( RWMessage(RWMATH_STRCTOR) ));
  }
}

SCharArray::SCharArray()
  : RWArrayView(IntVec(),sizeof(SChar))
{
}

SCharArray::SCharArray(const IntVec& n, RWUninitialized, Storage storage)
  : RWArrayView(n,sizeof(SChar),storage)
{
}

SCharArray::SCharArray(unsigned nx, unsigned ny, unsigned nz, RWUninitialized)
  : RWArrayView(nx,ny,nz,sizeof(SChar))
{
}

SCharArray::SCharArray(unsigned nx, unsigned ny, unsigned nz, unsigned nw, RWUninitialized)
  : RWArrayView(nx,ny,nz,nw,sizeof(SChar))
{
}

static void randFill(SCharArray& X, RWRand& r)
{
  for(RWMultiIndex i(X.length()); i; ++i) {
    X(i) = (SChar)r();
  }
}

SCharArray::SCharArray(const IntVec& n, RWRand& r, Storage storage)
  : RWArrayView(n,sizeof(SChar),storage)
{
  randFill(*this,r);
}

SCharArray::SCharArray(unsigned nx, unsigned ny, unsigned nz, RWRand& r)
  : RWArrayView(nx,ny,nz,sizeof(SChar))
{
  randFill(*this,r);
}

SCharArray::SCharArray(unsigned nx, unsigned ny, unsigned nz, unsigned nw, RWRand& r)
  : RWArrayView(nx,ny,nz,nw,sizeof(SChar))
{
  randFill(*this,r);
}

SCharArray::SCharArray(const IntVec& n, SChar scalar)
  : RWArrayView(n,sizeof(SChar))
{
  SChar scalar2 = scalar;  // Temporary required to finesse K&R compilers
  rwaset(prod(n), data(), 1, &scalar2);
}

SCharArray::SCharArray(unsigned nx, unsigned ny, unsigned nz, SChar scalar)
  : RWArrayView(nx,ny,nz,sizeof(SChar))
{
  SChar scalar2 = scalar;  // Temporary required to finesse K&R compilers
  rwaset(nx*ny*nz, data(), 1, &scalar2);
}

SCharArray::SCharArray(unsigned nx, unsigned ny, unsigned nz, unsigned nw, SChar scalar)
  : RWArrayView(nx,ny,nz,nw,sizeof(SChar))
{
  SChar scalar2 = scalar;  // Temporary required to finesse K&R compilers
  rwaset(nx*ny*nz*nw, data(), 1, &scalar2);
}


// Copy constructor
SCharArray::SCharArray(const SCharArray& a)
  : RWArrayView(a)
{
}

SCharArray::SCharArray(const SChar* dat, const IntVec& n)
  : RWArrayView(n,sizeof(SChar))
{
  rwacopy(prod(n), data(), 1, dat, 1);
}

SCharArray::SCharArray(const SChar* dat, unsigned nx, unsigned ny, unsigned nz)
  : RWArrayView(nx,ny,nz,sizeof(SChar))
{
  rwacopy(nx*ny*nz, data(), 1, dat, 1);
}

SCharArray::SCharArray(const SChar* dat, unsigned nx, unsigned ny, unsigned nz, unsigned nw)
  : RWArrayView(nx,ny,nz,nw,sizeof(SChar))
{
  rwacopy(nx*ny*nz*nw, data(), 1, dat, 1);
}

SCharArray::SCharArray(const SCharVec& dat, const IntVec& n)
  : RWArrayView(n,sizeof(SChar))
{
  rwacopy(prod(n), data(), 1, dat.data(), dat.stride());
}

SCharArray::SCharArray(const SCharVec& dat, unsigned nx, unsigned ny, unsigned nz)
  : RWArrayView(nx,ny,nz,sizeof(SChar))
{
  rwacopy(nx*ny*nz, data(), 1, dat.data(), dat.stride());
}

SCharArray::SCharArray(const SCharVec& dat, unsigned nx, unsigned ny, unsigned nz, unsigned nw)
  : RWArrayView(nx,ny,nz,nw,sizeof(SChar))
{
  rwacopy(nx*ny*nz*nw, data(), 1, dat.data(), dat.stride());
}

SCharArray::SCharArray(RWBlock *block, const IntVec& n)
  : RWArrayView(block,n)
{
  unsigned minlen = prod(length())*sizeof(SChar);
  if (block->length() < minlen) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_SHORTBLOCK, block->length(), minlen) ));
  }
}

SCharArray::SCharArray(RWBlock *block, unsigned nx, unsigned ny, unsigned nz)
  : RWArrayView(block, nx, ny, nz)
{
  unsigned minlen = prod(length())*sizeof(SChar);
  if (block->length() < minlen) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_SHORTBLOCK, block->length(), minlen) ));
  }
}

SCharArray::SCharArray(RWBlock *block, unsigned nx, unsigned ny, unsigned nz, unsigned nw)
  : RWArrayView(block, nx, ny, nz, nw)
{
  unsigned minlen = prod(length())*sizeof(SChar);
  if (block->length() < minlen) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_SHORTBLOCK, block->length(), minlen) ));
  }
}

SCharArray::SCharArray(const SCharVec& v)
  : RWArrayView(v,(void*)v.data(),IntVec(1,v.length()),IntVec(1,1))
{
}

SCharArray::SCharArray(const SCharGenMat& m)
  : RWArrayView(m,(void*)m.data(),
       IntVec(2,m.rows(),m.cols()-m.rows()),                 // [ m.rows() m.cols() ]
       IntVec(2,m.rowStride(),m.colStride()-m.rowStride())) // [ m.rowStride() m.colStride() ]
{
}

// This constructor is used internally by real() and imag() and slices:
SCharArray::SCharArray(const RWDataView& b, SChar* start, const IntVec& n, const IntVec& str)
  : RWArrayView(b,start,n,str)
{
}

SCharArray& SCharArray::operator=(const SCharArray& rhs)
{
  lengthCheck(rhs.length());
  if(sameDataBlock(rhs)) {
    (*this) = rhs.deepCopy();   // Avoid aliasing problems
  } else {
    for(DoubleArrayLooper l(npts,step,rhs.step); l; ++l) {
      rwacopy(l.length, data()+l.start1, l.stride1, rhs.data()+l.start2, l.stride2);
    }
  }
  return *this;
}

SCharArray& SCharArray::operator=(SChar scalar)
{
  for(ArrayLooper l(npts,step); l; ++l) {
    SChar scalar2 = scalar;  // Temporary required to finesse K&R compilers
    rwaset(l.length,data()+l.start,l.stride,&scalar2);
  }
  return *this;
}

SCharArray& SCharArray::reference(const SCharArray& v)
{
  RWArrayView::reference(v);
  return *this;
}

// Return copy of self with distinct instance variables
SCharArray SCharArray::deepCopy() const
{
  return copy();
}

// Synonym for deepCopy().  Not made inline because some compilers
// have trouble with inlined temporaries with destructors
SCharArray SCharArray::copy() const
{
  SCharArray temp(npts,rwUninitialized,COLUMN_MAJOR);
  temp = *this;
  return temp;
}

// Guarantee that references==1 and stride==1
void SCharArray::deepenShallowCopy()
{
  RWBoolean isStrideCompact = (dimension()==0 || step((int)0)==1);
  for( int r=npts.length()-1; r>0 && isStrideCompact; --r ) {
    if (step(r) != npts(r-1)*step(r-1)) isStrideCompact=FALSE;
  }
  if (!isStrideCompact || !isSimpleView()) {
    RWArrayView::reference(copy());
  }
}

void SCharArray::resize(const IntVec& N)
{
  if(N != npts){
    SCharArray temp(N,(SChar)0);
    int p = npts.length()<N.length() ? npts.length() : N.length();  // # dimensions to copy
    if (p>0) {
      IntVec n(p,rwUninitialized);   // The size of the common parts of the arrays
      for(int i=0; i<p; i++) {
        n(i) = (N(i)<npts(i)) ? N(i) : npts(i);
      }
  
      // Build a view of the source common part
      IntGenMat sstr(dimension(),p,0);
      sstr.diagonal() = 1;
      SCharArray sp = slice(IntVec(dimension(),0), n, sstr);
  
      // Build a view of the destination common part
      IntGenMat dstr(N.length(),p,0);
      dstr.diagonal() = 1;
      SCharArray dp = temp.slice(IntVec(N.length(),0), n, dstr);
  
      dp = sp;
    }
    reference(temp);
  }
}

void SCharArray::resize(unsigned m, unsigned n, unsigned o)
{
  IntVec i(3,rwUninitialized);
  i((int)0) = m;
  i(1) = n;
  i(2) = o;
  resize(i);
}

void SCharArray::resize(unsigned m, unsigned n, unsigned o, unsigned p)
{
  IntVec i(4,rwUninitialized);
  i((int)0) = m;
  i(1) = n;
  i(2) = o;
  i(3) = p;
  resize(i);
}

void SCharArray::reshape(const IntVec& N)
{
  if (N!=npts) {
    SCharArray temp(N,rwUninitialized,COLUMN_MAJOR);
    reference(temp);
  }
}

void SCharArray::reshape(unsigned m, unsigned n, unsigned o)
{
  IntVec i(3,rwUninitialized);
  i((int)0) = m;
  i(1) = n;
  i(2) = o;
  reshape(i);
}

void SCharArray::reshape(unsigned m, unsigned n, unsigned o, unsigned p)
{
  IntVec i(4,rwUninitialized);
  i((int)0) = m;
  i(1) = n;
  i(2) = o;
  i(3) = p;
  reshape(i);
}

SCharArray SCharArray::slice(const IntVec& start, const IntVec& n, const IntGenMat& str) const
{
  sliceCheck(start, n, str);
  return SCharArray(*this, (SChar*)data()+dot(start,stride()), n, product(stride(),str));
}

SChar toScalar(const SCharArray& A)
{
  A.dimensionCheck(0);
  return *(A.data());
}

SCharVec toVec(const SCharArray& A)
{
  A.dimensionCheck(1);
  return SCharVec(A,(SChar*)A.data(),(unsigned)A.length((int)0),A.stride((int)0));
}

SCharGenMat toGenMat(const SCharArray& A)
{
  A.dimensionCheck(2);
  return SCharGenMat(A,(SChar*)A.data(),(unsigned)A.length((int)0),(unsigned)A.length(1),A.stride((int)0),A.stride(1));
}


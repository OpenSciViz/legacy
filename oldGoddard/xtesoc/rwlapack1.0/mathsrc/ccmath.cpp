/*
 * DComplexVec special math functions
 *
 * Generated from template $Id: xcmath.cpp,v 1.5 1993/09/17 18:42:21 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

/*
 *  This file contains functions peculiar to complex vectors
 *  for precision Double.
 */

#include "rw/cvec.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"
STARTWRAP
#include <math.h>
ENDWRAP

RCSID("$Header: /users/rcs/mathsrc/xcmath.cpp,v 1.5 1993/09/17 18:42:21 alv Exp $");

/*
 * Return first nterms of the N'th order roots of one.
 */

DComplexVec
rootsOfOne(int N, unsigned nterms)
{
  /* Use the recursion formulae
   *  sin(a+b) = sin(a)cos(b) + cos(a)sin(b)
   *  cos(a+b) = cos(a)cos(b) - sin(a)sin(b)
   */

  if(N==0) {
    RWTHROW( RWInternalErr( RWMessage(RWMATH_ZROOT) ));
  }

  Double b = 2.0*M_PI/N;
  register Double cosb = cos(b);
  register Double sinb = sin(b);

  register Double cosa = 1.0;		// implies a=0
  register Double sina = 0.0;

  DComplexVec roots(nterms,rwUninitialized);

  if(nterms){
    roots((int)0) = DComplex(cosa, sina);

    register Double temp;
    for(register int i = 1; i<nterms; i++){
      temp = cosa*cosb - sina*sinb;
      sina = sina*cosb + cosa*sinb;
      cosa = temp;
      roots(i) = DComplex(cosa, sina);
    }
  }

  return roots;
}

/*
 * Expand a complex conjugate even series into its full series.
 */

DComplexVec
expandConjugateEven(const DComplexVec& v)
{
  unsigned N = v.length()-1;
  DComplexVec full(2*N,rwUninitialized);

  // Lower half:
  full.slice(0,N+1,1) = v;
  // Upper half:
  full.slice(N+1,N-1,1) = conj(v.slice(N-1,N-1,-1));

  return full;
}

/*
 * Expand a complex conjugate odd series into its full series.
 */

DComplexVec
expandConjugateOdd(const DComplexVec& v)
{
  unsigned N = v.length()-1;
  DComplexVec full(2*N,rwUninitialized);

  // Lower half:
  full.slice(0,N+1,1) = v;
  // Upper half:
  full.slice(N+1,N-1,1) = conj(-v.slice(N-1,N-1,-1));

  return full;
}

Double
spectralVariance(const DComplexVec& v)
{
  // Do not count the mean v(0):
  return sum(norm(v.slice(1,v.length()-1,1)));
}

#ifndef __F2CINCL_H__
#define __F2CINCL_H__

/*
 * This file is for internal use by the Linpack
 * and BLA routines only.  In general, it should not be
 * seen by the user.  This is because it flagrantly
 * pollutes the global name space.
 *
 * $Log: f2cincl.h,v $
 * Revision 1.3  1993/08/10  21:22:18  alv
 * now uses Tools v6 compatible error handling
 *
 * Revision 1.2  1993/08/04  22:18:45  dealys
 * ported to MPW C++ 3.3
 *
 * Revision 1.1  1993/01/22  23:55:39  alv
 * Initial revision
 *
 */

#  include "rw/compiler.h"		/* Looking for "KR_ONLY" */
#  include "rw/dcomplex.h"		/* Get declarations for DComplex */
#  include "rw/fcomplex.h"		/* Get declarations for FComplex */

#define TRUE_ (1)
#define FALSE_ (0)

#define abs(x)    ((x) >= 0 ? (x) : -(x))
#define dabs(x)   (double)abs(x)
#define min(a,b)  ((a) <= (b) ? (a) : (b))
#define max(a,b)  ((a) >= (b) ? (a) : (b))
#define dmin(a,b) (double)min(a,b)
#define dmax(a,b) (double)max(a,b)

#if defined(__cplusplus)
extern "C" { /* closing brace seperately protected near end of file */
# if defined(RW_NATIVE_EXTENDED)
    extended sqrt(extended);
    extended fabs(extended);
#   define P_(s) s
# else
    double sqrt(double);
    double fabs(double);
#   define P_(s) s
# endif
#else  /* defined(__cplusplus) */
# ifdef RW_KR_ONLY
#   define P_(s) ()
    double sqrt();
    double fabs();
# else
#   if defined(RW_NATIVE_EXTENDED)
#     define P_(s) s
      extended sqrt(extended);
      extended fabs(extended);
#   else
#     define P_(s) s
      double sqrt(double);
      double fabs(double);
#   endif
#  endif
#endif

/*
 * These functions defined as macros for efficiency:
 */
#define c_abs(a)     cabs((a)->r, (a)->i)	/* replaces: double c_abs(); */
#define z_abs(a)     cabs((a)->r, (a)->i)	/* replaces: double z_abs(); */
#define d_cnjg(a,b)  { DComplex *zza=(a); DComplex *zzb=(b);  zza->r = zzb->r; zza->i = - zzb->i; }
#define r_cnjg(a,b)  { FComplex *zza=(a); FComplex *zzb=(b);  zza->r = zzb->r; zza->i = - zzb->i; }
#define d_imag(a)    ((a)->i)			/* replaces: double d_imag(); */
#define r_imag(a)    ((a)->i)			/* replaces: double r_imag(); */

double  cabs    P_((double,double));
void   	c_div   P_((FComplex*,FComplex*,FComplex*));
void   	z_div   P_((DComplex*,DComplex*,DComplex*));
double 	d_sign  P_((double*,double*));
float 	r_sign  P_((float*,float*));
void   	c_sqrt  P_((FComplex*,FComplex*));
void   	z_sqrt  P_((DComplex*,DComplex*));

/*
 * Formerly statement functions, reimplemented as externals:
 */

float  cabs1_  P_((FComplex*));
double zabs1_  P_((DComplex*));

#ifdef __cplusplus
}
#endif

#endif


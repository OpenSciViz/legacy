/*
 * Type specific routines for IntArray
 *
 * $Header: /users/rcs/mathsrc/iarrx.cpp,v 1.3 1993/09/19 23:52:12 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: iarrx.cpp,v $
 * Revision 1.3  1993/09/19  23:52:12  alv
 * fixed bug caught by Symantic
 *
 * Revision 1.2  1993/01/26  21:54:57  alv
 * fixed wrong cast bug
 *
 * Revision 1.1  1993/01/22  23:50:59  alv
 * Initial revision
 *
 * 
 *    Rev 1.3   17 Oct 1991 09:15:42   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.2   08 Oct 1991 13:44:38   keffer
 * Math V4.0 beta 2
 * 
 *    Rev 1.1   24 Jul 1991 12:51:42   keffer
 * Added pvcs keywords
 *
 */

#include "rw/iarr.h"
#include "rw/darr.h"
#include "rw/farr.h"

RCSID("$Header: /users/rcs/mathsrc/iarrx.cpp,v 1.3 1993/09/19 23:52:12 alv Exp $");

// Convert to a DoubleArray
// Should be a friend DoubleArray, but...
IntArray::operator DoubleArray() const
{
  DoubleArray temp(length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  double *dp = temp.data();
  for(ArrayLooper l(length(),stride()); l; ++l) {
    int n = l.length;
    const int *tp = data()+l.start;
    while(n--) {
      *dp++ = double(*tp); tp+=l.stride;
    }
  }
  return temp;
}

// narrow from DoubleArray to IntArray
IntArray
toInt(const DoubleArray& v)
{
  IntArray temp(v.length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  int *dp = temp.data();
  for(ArrayLooper l(v.length(),v.stride()); l; ++l) {
    int n = l.length;
    const double *tp = v.data()+l.start;
    while(n--) {
      *dp++ = int(*tp); tp+=l.stride;
    }
  }
  return temp;
}

// narrow from FloatArray to IntArray
IntArray
toInt(const FloatArray& v)
{
  IntArray temp(v.length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  int *dp = temp.data();
  for(ArrayLooper l(v.length(),v.stride()); l; ++l) {
    int n = l.length;
    const float *tp = v.data()+l.start;
    while(n--) {
      *dp++ = int(*tp); tp+=l.stride;
    }
  }
  return temp;
}

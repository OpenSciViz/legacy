> #
> # $Header: /users/rcs/mathsrc/xarrmath.cpp,v 1.10 1993/09/20 04:02:38 alv Exp $
> #
> # $Log: xarrmath.cpp,v $
> # Revision 1.10  1993/09/20  04:02:38  alv
> # fixed bug caught by Symantec
> #
> # Revision 1.9  1993/09/17  18:42:21  alv
> # (0) -> ((int)0) for benefit of DEC compiler
> #
> # Revision 1.8  1993/09/16  17:16:56  alv
> # added norm functions
> #
> # Revision 1.7  1993/09/01  16:27:49  alv
> # now uses rwUninitialized form for simple constructors
> #
> # Revision 1.6  1993/08/04  21:36:49  dealys
> # ported to MPW C++ 3.3
> #
> # Revision 1.5  1993/05/10  19:29:27  alv
> # ported to Zortech 3.1
> #
> # Revision 1.4  1993/03/22  15:51:31  alv
> # changed PVCS Workfile keyword to RCS ID keyword
> #
> # Revision 1.3  1993/03/12  20:01:12  alv
> # removed refs to BCC_STRUCT_POINTER_BUG
> #
> # Revision 1.2  1993/01/27  20:20:48  alv
> # eliminated mean() for integer array types
> #
> # Revision 1.1  1993/01/22  23:51:06  alv
> # Initial revision
> #
> # 
> define <ClassType>=Array
> include macros
/*
 * Math definitions
 *
 * Generated from template $Id: xarrmath.cpp,v 1.10 1993/09/20 04:02:38 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/rw<bla>bla.h"
#include "rw/<a>arr.h"
#include "rw/<a>vec.h"

> if <C>==IntVec || <C>==SCharVec
#include <stdlib.h>     /* Looking for abs(int) */
> endif

> if <R/C>==Complex
// Complexes won't fit in a register
#define REGISTER
> else
#define REGISTER register
> endif

RCSID("$Header: /users/rcs/mathsrc/xarrmath.cpp,v 1.10 1993/09/20 04:02:38 alv Exp $");

> if <C>==DComplexArray || <C>==FComplexArray || <C>==IntArray
// Absolute value of a <C>
<P>Array abs(const <C>& s)
{
  <P>Array temp(s.length(),rwUninitialized);
  register <P>* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    register <t>* sp = (<t>*)(s.data()+l.start); // Cast away "constness" to avoid problems with abs()
    register j = l.stride;
    register i = l.length;
    while (i--) { *dp++ = abs(*sp); sp += j; }
  }
  return temp;
}
> endif

> if <C>==SCharArray
<C> abs(const <C>& s)
// Absolute value of a <C>
{
  <C> temp(s.length(),rwUninitialized);
  register <t>* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    register const <t>* sp = s.data()+l.start;
    register i = l.length;
    register j = l.stride;
    while (i--) { *dp++ = abs(int(*sp));  sp += j; }
  }
  return temp;
}
> endif

> if <R/C>==Complex
<C> <C>::apply(CmathFunTy f) const
// Apply function returning <t> to each element of vector slice
{
  <C> temp(npts,rwUninitialized,RWDataView::COLUMN_MAJOR);
  register <t>* dp = temp.data();
  for(ArrayLooper l(npts,step); l; ++l) {
    register i = l.length;
    register <t>* sp = (<t>*)(data()+l.start);
    register j = l.stride;
    while(i--) {
// Turbo C++ bug requires temporary.
      <t> dummy = (*f)(*sp);
      *dp++ = dummy;
      sp += j; 
    }
  }
  return temp;
}

<P>Array <C>::apply2(CmathFunTy2 f) const
// Apply function returning <P> to each element of vector slice
{
  <P>Array temp(npts,rwUninitialized,RWDataView::COLUMN_MAJOR);
  register <P>* dp = temp.data();
  for(ArrayLooper l(npts,step); l; ++l) {
    register <t>* sp = (<t>*)(data()+l.start);
    register i = l.length;
    register j = l.stride;
    while (i--) { *dp++ = (*f)(*sp);  sp += j; }
  }
  return temp;
}
> elif <C>==DoubleArray || <C>==FloatArray

<C> <C>::apply(mathFunTy f) const
// Apply function returning <t> to each element of vector slice
{
  <C> temp(npts,rwUninitialized,RWDataView::COLUMN_MAJOR);
  register <t>* dp = temp.data();
  for(ArrayLooper l(npts,step); l; ++l) {
    register const <t>* sp = data()+l.start;
    register i = l.length;
    register j = l.stride;
    while(i--) {
// Turbo C++ bug requires temporary.
#if defined (__TURBOC__)
      <t> dummy = (*f)(*sp);
      *dp++ = dummy;
#else
      *dp++ = (*f)(*sp);  
#endif
      sp += j; 
    }
  }
  return temp;
}

#if defined(RW_NATIVE_EXTENDED)
<C> <C>::apply(XmathFunTy f) const
// Apply function for compilers using extended types
{
  <C> temp(npts,rwUninitialized,RWDataView::COLUMN_MAJOR);
  register <t>* dp = temp.data();
  for(ArrayLooper l(npts,step); l; ++l) {
    register const <t>* sp = data()+l.start;
    register i = l.length;
    register j = l.stride;
    while(i--) {
      *dp++ = (*f)(*sp);
      sp += j;
    }
  }
  return temp;
}
#endif
> endif

> if <R/C>==Complex
#  ifdef COMPLEX_PACKS

/*
 * If we can guarantee that the complex vector goes
 * (real, imag, real, imag, real, imag, etc.) then
 * it becomes possible to implement real(<C>&) and imag(<C>&)
 * via a slice, allowing its use as an lvalue.  What
 * follows violates encapsulation (a bit), but sure is useful.
 */

<P>Array real(const <C>& v)
{
#ifdef IMAG_LEADS
  return <P>Array(v, (<P>*)v.begin + 1, v.length(), 2*v.stride());
#else
  return <P>Array(v, (<P>*)v.begin, v.length(), 2*v.stride());
#endif
}
<P>Array imag(const <C>& v)
{
#ifdef IMAG_LEADS
  return <P>Array(v, (<P>*)v.begin, v.length(), 2*v.stride());
#else
  return <P>Array(v, (<P>*)v.begin + 1, v.length(), 2*v.stride());
#endif
}

// The above allows a very compact form of conj() to be written:
<C>
conj(const <C>& V)
{
  return <C>(real(V), -imag(V));
}

#else   /* Complex does not pack */

<P>Array real(const <C>& v)
{
  <P>Array temp(npts,rwUninitialized,RWDataView::COLUMN_MAJOR);
  register <P>* dp = temp.data();
  for(ArrayLooper l(npts,step); l; ++l) {
    register i = l.length;
    register const <t>* sp = v.data()+l.start;
    register j = l.stride;
    while(i--) {*dp++ = real(*sp); sp += j;}
  }
  return temp;
}

<P>Array imag(const <C>& v)
{
  <P>Array temp(npts,rwUninitialized,RWDataView::COLUMN_MAJOR);
  register <P>* dp = temp.data();
  for(ArrayLooper l(npts,step); l; ++l) {
    register i = l.length;
    register const <t>* sp = v.data()+l.start;
    register j = l.stride;
    while(i--) {*dp++ = imag(*sp); sp += j;}
  }
  return temp;
}

<C>	conj(const <C>& V)	{return V.apply(::conj); }

#endif /* Not COMPLEX_PACKS */

> # End of complex section:
> endif

> if <C>==DoubleArray || <C>==FloatArray
<C> atan2(const <C>& u,const <C>& v)
     // Arctangent of u/v.
{
  u.lengthCheck(v.length());
  <C> temp(v.length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  register <t>* dp = temp.data();
  for(DoubleArrayLooper l(v.length(),u.stride(),v.stride()); l; ++l) {
    register const <t>* up = u.data()+l.start1;
    register const <t>* vp = v.data()+l.start2;
    register i = l.length;
    register uj = l.stride1;
    register vj = l.stride2;
    while (i--) { *dp++ = atan2(*up,*vp);  up += uj;  vp += vj; }
  }
  return temp;
}
> endif

<t> dot(const <C>& u, const <C>& v)
      // dot product: sum_ij...k (u_ij...k * v_ij...k)
{
  u.lengthCheck(v.length());
> if <R/C>==Complex
  <t> result(0,0);
> else
  <t> result = 0;
> endif
  for(DoubleArrayLooper l(v.length(),u.stride(),v.stride()); l; ++l) {
    <t> partialResult;
    rw<bla>dot(l.length,u.data()+l.start1,l.stride1,v.data()+l.start2,l.stride2,&partialResult);
    result += partialResult;
  }
  return result;
}

<C> dot(const <T>Vec& V, const <C>& A)
      // dot product: Bj...k = sum_i (Vi * Aij...k)
{
  int n = V.length();
  A.dimensionLengthCheck(0,n);
  RWToEnd jk(1);   // Indexes all components of a vector but the first
  <C> B(A.length()(jk),rwUninitialized,RWDataView::COLUMN_MAJOR);
  <t> *dp = B.data();
  for(ArrayLooper l(B.length(),A.stride()(jk)); l; ++l) {
    const <t> *sp = A.data()+l.start;
    for(int i=l.length; i--;) {
      rw<bla>dot(n,V.data(),V.stride(),sp,A.stride((int)0),dp);
      sp += l.stride;
      dp++;
    }
  }
  return B;
}

<C> dot(const <C>& A, const <T>Vec& V)
      // dot product: Bi...j = sum_k (Ai...jk * Vk)
{
  int n = V.length();
  A.dimensionLengthCheck(0,n);
  RWSlice ij(0,n-1);   // Indexes all components of array index but the last
  <C> B(A.length()(ij),rwUninitialized,RWDataView::COLUMN_MAJOR);
  <t> *dp = B.data();
  for(ArrayLooper l(B.length(),A.stride()(ij)); l; ++l) {
    const <t> *sp = A.data()+l.start;
    for(int i=l.length; i--;) {
      rw<bla>dot(n,V.data(),V.stride(),sp,A.stride((int)0),dp);
      sp += l.stride;
      dp++;
    }
  }
  return B;
}

> if <R/C>==Real
IntVec maxIndex(const <C>& s)
// Index of maximum element.
{
  s.numPointsCheck("max needs at least",1);
  int p = s.dimension();
  if (p<1) { return s.length(); } //s.length() is a length 0 vector
  IntVec index(p,0);
  IntVec maxIndex(p,0);
  <t> max = s(maxIndex);

  // Loop through the array, using the bla routine to loop through the
  // innermost loop (first component of index vector).
  int strider = s.stride((int)0);
  int len = s.length((int)0);
  while(index(p-1)<s.length(p-1)) {
    const <t>* ptr = s.data() + dot(index,s.stride());
    int maxLocal = rw<bla>max(len,ptr,strider);
    if (ptr[maxLocal*strider]>max) {
      max=ptr[maxLocal*strider];
      index((int)0) = maxLocal;
      maxIndex = index;
    }
    index((int)0) = s.length((int)0);     // Skip over the zeroth component, then
    int r=0;                    // bump up the index.
    while( index(r)>=s.length(r) && ++r<p ) {
      index(r-1) = 0;
      ++(index(r));
    }
  }
  return maxIndex;
}

<t> maxValue(const <C>& s)
{
  s.numPointsCheck("maxValue needs at least",1);
  <t> max = *(s.data());
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    int localMaxIndex = rw<bla>max(l.length,s.data()+l.start,l.stride);
    <t> localMax = *(s.data()+l.start+localMaxIndex*l.stride);
    if (localMax>max) { max=localMax; }
  }
  return max;
}

IntVec minIndex(const <C>& s)
// Index of minimum element.
{
  s.numPointsCheck("min needs at least",1);
  int p = s.dimension();
  if (p<1) { return s.length(); } //s.length() is a length 0 vector
  IntVec index(p,0);
  IntVec minIndex(p,0);
  <t> min = s(minIndex);

  // Loop through the array, using the bla routine to loop through the
  // innermost loop (first component of index vector).
  int strider = s.stride((int)0);
  int len = s.length((int)0);
  while(index(p-1)<s.length(p-1)) {
    const <t>* ptr = s.data() + dot(index,s.stride());
    int minLocal = rw<bla>min(len,ptr,strider);
    if (ptr[minLocal*strider]<min) {
      min=ptr[minLocal*strider];
      index((int)0) = minLocal;
      minIndex = index;
    }
    index((int)0) = s.length((int)0);     // Skip over the zeroth component, then
    int r=0;                    // bump up the index.
    while( index(r)>=s.length(r) && ++r<p ) {
      index(r-1) = 0;
      ++(index(r));
    }
  }
  return minIndex;
}

<t> minValue(const <C>& s)
{
  s.numPointsCheck("minValue needs at least",1);
  <t> min = *(s.data());
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    int localMinIndex = rw<bla>min(l.length,s.data()+l.start,l.stride);
    <t> localMin = *(s.data()+l.start+localMinIndex*l.stride);
    if (localMin<min) { min=localMin; }
  }
  return min;
}

> endif

<t> prod(const <C>& s)
{
> if <C>==FloatArray
  register double t = 1;        // Accumulate sums in double precision
> else
> if <R/C>==Complex
  <t> t(1,0);
> else
  REGISTER <t> t = 1;
> endif
> endif
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    register <t>* sp = (<t>*)(s.data()+l.start);
    register int i = l.length;
    register j = l.stride;
    while (i--) { t *= *sp;  sp += j; }
  }
  return (<t>)t;
}

> if <P>==Double || <P>==Float
<C> pow(const <C>& u, const <C>& v)
// u to the v power.
{
  u.lengthCheck(v.length());
  <C> temp(v.length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  register <t>* dp = temp.data();
  for(DoubleArrayLooper l(v.length(),u.stride(),v.stride()); l; ++l) {
    register i = l.length;
    register <t>* up = (<t>*)(u.data()+l.start1);
    register <t>* vp = (<t>*)(v.data()+l.start2);
    register uj = l.stride1;
    register vj = l.stride2;
    while (i--) { *dp++ = pow(*up,*vp); up += uj; vp += vj; }
  }
  return temp;
}
> endif

<t> sum(const <C>& s)
{
> if <C>==FloatArray
  register double t = 0;        // Accumulate sums in double precision
> else
> if <R/C>==Complex
  REGISTER <t> t(0,0);
> else
  REGISTER <t> t = 0;
> endif
> endif
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    register <t>* sp = (<t>*)(s.data()+l.start);
    register int i = l.length;
    register j = l.stride;
    while (i--) { t += *sp;  sp += j; }
  }
  return (<t>)t;
}

> if <P>==Double || <P>==Float
<t> mean(const <C>& s)
{
  <t> theSum = sum(s);
  <p> numPoints = prod(s.length());
> if <R/C>==Complex
  if (theSum==<t>(0,0)) return <t>(0,0);   // Covers the case of zero points
  return theSum/<t>(numPoints,0);
> else
  if (theSum==0) return 0;   // Covers the case of zero points
  return theSum/numPoints;
> endif
}
> endif

> if <P>==Double || <P>==Float
<p> variance(const <C>& s)
// Variance of a <C>
{
  s.numPointsCheck("Variance needs minimum of",2);
  register double sum2 = 0;     // Accumulate sums in double precision
  REGISTER <t> avg = mean(s);
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    register <t>* sp = (<t>*)(s.data()+l.start);
    register int i = l.length;
    register j = l.stride;
    while (i--) {
      REGISTER <t> temp = *sp - avg;
> if <R/C>==Complex
      sum2 += norm(temp);
> else
      sum2 += temp * temp;
> endif
      sp += j;
    }
  }
  // Use the biased estimate (easier to work with):
  return (<p>)(sum2/prod(s.length()));
}
> endif

> if <P>==Double || <P>==Float
/*
 * Various functions with simple definitions:
 */
> endif
> if <T>==Double || <T>==Float
<C>	acos(const <C>& V)	{ return V.apply(::acos); }
<C>	asin(const <C>& V)	{ return V.apply(::asin); }
<C>	atan(const <C>& V)	{ return V.apply(::atan); }
<C>	ceil(const <C>& V)	{ return V.apply(::ceil); }
> endif
> if <P>==Double || <P>==Float
<C>	cos(const <C>& V)		{ return V.apply(::cos);  }
<C>	cosh(const <C>& V)	{ return V.apply(::cosh); }
> endif
> if <P>==Double || <P>==Float
<C>	exp(const <C>& V)		{ return V.apply(::exp);  }
> endif
> if <T>==Double || <T>==Float
<C>	floor(const <C>& V)	{ return V.apply(::floor);}
> endif
> if <T>==Double || <T>==Float
<C>	log(const <C>& V)		{ return V.apply(::log);  }
<C>	log10(const <C>& V)	{ return V.apply(::log10);}
> endif
> if <P>==Double || <P>==Float
<C>	sin(const <C>& V)		{ return V.apply(::sin);  }
<C>	sinh(const <C>& V)	{ return V.apply(::sinh); }
> endif
> if <T>==Double || <T>==Float
<C>	tan(const <C>& V)		{ return V.apply(::tan);  }
<C>	tanh(const <C>& V)	{ return V.apply(::tanh); }
> endif
> if <R/C>==Complex
<P>Array	arg(const <C>& V)	{return V.apply2(::arg);  }
<P>Array	norm(const <C>& V)	{return V.apply2(::norm); }
> endif

> if <T>==Double || <T>==Float
#ifdef __ZTC__
/*
 * Zortech requires its own special version of abs(const <C>&)
 * because it uses an inlined version of fabs().  Hence, apply() 
 * cannot be used.
 */
<C>
abs(const <C>& s)
// Absolute value of a <C> --- Zortech version
{
  <C> temp(s.length(),rwUninitialized);
  register <t>* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    register const <t>* sp = s.data()+l.start;
    register i = l.length;
    register j = l.stride;
    while (i--) { *dp++ = fabs(*sp);  sp += j; }
  }
  return temp;
}
#else   /* Not Zortech */
<C>	abs(const <C>& V)		{ return V.apply(::fabs); }
#endif	
> endif
> 
> if <P>==Double || <P>==Float
#ifdef __ZTC__
/*
 * Zortech requires its own special version of sqrt(const <C>&) 
 * because it uses an inlined version of sqrt().  Hence, apply() 
 * cannot be used.
 */
<C>
sqrt(const <C>& s)
// Square root of a <C> --- Zortech version
{
  <C> temp(s.length(),rwUninitialized);
  register <t>* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    register const <t>* sp = s.data()+l.start;
    register i = l.length;
    register j = l.stride;
    while (i--) { *dp++ = sqrt(*sp);  sp += j; }
  }
  return temp;
}
#else
<C>	sqrt(const <C>& V)	{ return V.apply(::sqrt); }
#endif
> endif

> if <T>==Double || <T>==Float || <T>==DComplex
    /**** Norm functions ****/

<P> maxNorm(const <C>& x)     // Not really a norm!
{
  <P> theMax = 0;
  for(ArrayLooper l(x.length(),x.stride()); l; ++l) {
    const <T> *sp = x.data()+l.start;
    for(int i=l.length; i--;) {
      <T> entry = sp[i*l.stride];
> if <R/C>==Complex
      if (norm(entry)>theMax) theMax=norm(entry);
> else
      if (entry>theMax) theMax=entry;
      if ((-entry)>theMax) theMax=(-entry);
> endif
    }
  }
> if <R/C>==Complex
  return sqrt(theMax);
> else
  return theMax;
> endif
}

<P> frobNorm(const <C>& x)
{
  <P> sumSquares = 0;
  for(ArrayLooper l(x.length(),x.stride()); l; ++l) {
    const <T> *sp = x.data()+l.start;
    for(int i=l.length; i--;) {
      <T> entry = sp[i*l.stride];      
> if <R/C>==Complex
      sumSquares += norm(entry);
> else
      sumSquares += entry*entry;
> endif
    }
  }
  return sqrt(sumSquares);
}

> endif

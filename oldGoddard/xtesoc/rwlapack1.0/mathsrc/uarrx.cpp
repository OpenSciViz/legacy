/*
 * Type conversion routines for UCharArray
 *
 * $Header: /users/rcs/mathsrc/uarrx.cpp,v 1.3 1993/09/19 23:49:57 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: uarrx.cpp,v $
 * Revision 1.3  1993/09/19  23:49:57  alv
 * fixed bug caught by Symantic
 *
 * Revision 1.2  1993/01/26  21:54:57  alv
 * fixed wrong cast bug
 *
 * Revision 1.1  1993/01/22  23:51:05  alv
 * Initial revision
 *
 * 
 *    Rev 1.3   17 Oct 1991 09:15:40   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.2   08 Oct 1991 13:45:00   keffer
 * Math V4.0 beta 2
 * 
 *    Rev 1.1   24 Jul 1991 12:52:02   keffer
 * Added pvcs keywords
 *
 */

#include "rw/ucarr.h"
#include "rw/iarr.h"

RCSID("$Header: /users/rcs/mathsrc/uarrx.cpp,v 1.3 1993/09/19 23:49:57 alv Exp $");


// Convert to an IntArray
// Should be a friend of IntArray, but...
UCharArray::operator IntArray() const
{
  IntArray temp(length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  int *dp = temp.data();
  for(ArrayLooper l(length(),stride()); l; ++l) {
    int n = l.length;
    const UChar *tp = data()+l.start;
    while(n--) {
      *dp++ = int(*tp); tp+=l.stride;
    }
  }
  return temp;
}

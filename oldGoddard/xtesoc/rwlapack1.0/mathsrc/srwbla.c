/*
 * BLA-like extensions for type float.
 *
 * Generated from template $Id: xrwbla.c,v 1.3 1993/10/11 18:42:24 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/rwsbla.h"
#include "rw/mathdefs.h"

#include <math.h>
#ifdef BSD
#  include <memory.h>
#else
#  include <string.h>
#endif

/****************************************************************
 *								*
 *			UTILITY					*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwscopy(n, x, incx, y, incy )
  unsigned n;
  float *x, *y;
  int incx, incy;
#else
void FDecl rwscopy( unsigned n, float* restrict x, int incx, const float* restrict y, int incy)
#endif
{
  if (n==0) return;

  if (incx == 1 && incy == 1) {
    memcpy(x, y, n*sizeof(float));
  }
  else {
    /*
     * Code for increments not equal to one
     */
    while (n--) {
      *x = *y;
      x += incx;
      y += incy;
    }
  }
}

#ifdef RW_KR_ONLY
void rwsset(n, x, incx, scalar)
  unsigned n;
  float *x, *scalar;
  int incx;
#else
void FDecl rwsset(unsigned n, float* restrict x, int incx, const float* restrict scalar)
#endif
{
  if(incx==1)
    while (n--) { *x++ = *scalar; }
  else
    while (n--) { *x = *scalar; x+=incx; }
}

#ifdef RW_KR_ONLY
int rwssame(n, x, incx, y, incy)
  unsigned n;
  float *x, *y;
  int incx, incy;
#else
int FDecl rwssame(unsigned n, const float* restrict x, int incx, const float* restrict y, int incy)
#endif
{
  while(n--){
    if( *x != *y ) return 0;
    x += incx;
    y += incy;
  }
  return 1;
}

#ifdef RW_KR_ONLY
int rwsmax(n, x, incx)
  unsigned n;
  float *x;
  int incx;
#else
int FDecl rwsmax(unsigned n, const float* restrict x, int incx)
#endif
{
  float maxv;
  int maxi, i;

  if(n==0) return -1;
  maxv = *x;
  maxi = 0;

  for(i=1; i<n; i++){
    x += incx;
    if(*x > maxv) { maxv = *x; maxi=i; }
  }
  return maxi;
}

#ifdef RW_KR_ONLY
int rwsmin(n, x, incx)
  unsigned n;
  float *x;
  int incx;
#else
int FDecl rwsmin(unsigned n, const float* restrict x, int incx)
#endif
{
  float minv;
  int mini, i;

  if(n==0) return -1;
  minv = *x;
  mini = 0;

  for(i=1; i<n; i++){
    x += incx;
    if(*x < minv) { minv = *x; mini=i; }
  }
  return mini;
}

/****************************************************************
 *								*
 *			DOT PRODUCTS				*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwsdot(n, x, incx, y, incy, result)
  unsigned n;
  float *x, *y, *result;
  int incx, incy;
#else
void FDecl rwsdot(unsigned n, const float* restrict x, int incx, const float* restrict y, int incy, float* result)
#endif
{

  double sum = 0;		/* Accumulate in double precision */
  while(n--){
    sum += *x * *y;
    x += incx;
    y += incy;
  }
  *result = (float)sum;
}


/****************************************************************
 *								*
 *		VECTOR --- VECTOR ASSIGNMENT OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rws_aplvv(n, x, incx, y, incy)
  unsigned n;
  float *x, *y;
  int incx, incy;
#else
void FDecl rws_aplvv(unsigned n, float* restrict x, int incx, const float* restrict y, int incy)
#endif
{
  while(n--){
    *x += *y;
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rws_amivv(n, x, incx, y, incy)
  unsigned n;
  float *x, *y;
  int incx, incy;
#else
void FDecl rws_amivv(unsigned n, float* restrict x, int incx, const float* restrict y, int incy)
#endif
{
  while(n--){
    *x -= *y;
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rws_amuvv(n, x, incx, y, incy)
  unsigned n;
  float *x, *y;
  int incx, incy;
#else
void FDecl rws_amuvv(unsigned n, float* restrict x, int incx, const float* restrict y, int incy)
#endif
{
  while(n--){
    *x *= *y;
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rws_advvv(n, x, incx, y, incy)
  unsigned n;
  float *x, *y;
  int incx, incy;
#else
void FDecl rws_advvv(unsigned n, float* restrict x, int incx, const float* restrict y, int incy)
#endif
{
  while(n--){
    *x /= *y;
    x += incx;
    y += incy;
  }
}

/****************************************************************
 *								*
 *		VECTOR --- SCALAR ASSIGNMENT OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rws_aplvs(n, x, incx, scalar)
  unsigned n;
  float *x, *scalar;
  int incx;
#else
void FDecl rws_aplvs(unsigned n, float* restrict x, int incx, const float* restrict scalar)
#endif
{
  while(n--) {
    *x += *scalar;
    x += incx;
  }
}

#ifdef RW_KR_ONLY
void rws_amuvs(n, x, incx, scalar)
  unsigned n;
  float *x, *scalar;
  int incx;
#else
void FDecl rws_amuvs(unsigned n, float* restrict x, int incx, const float* restrict scalar)
#endif
{
  while(n--) {
    *x *= *scalar;
    x += incx;
  }
}


/****************************************************************
 *								*
 *		VECTOR --- VECTOR BINARY OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rws_plvv(n, z, x, incx, y, incy)
  unsigned n;
  float *z, *x, *y;
  int incx, incy;
#else
void FDecl rws_plvv(unsigned n, float* restrict z, const float* restrict x, int incx, const float* restrict y, int incy)
#endif
{
  while(n--){
    *z++ = *x + *y;
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rws_mivv(n, z, x, incx, y, incy)
  unsigned n;
  float *z, *x, *y;
  int incx, incy;
#else
void FDecl rws_mivv(unsigned n, float* restrict z, const float* restrict x, int incx, const float* restrict y, int incy)
#endif
{
  while(n--){
    *z++ = *x - *y;
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rws_muvv(n, z, x, incx, y, incy)
  unsigned n;
  float *z, *x, *y;
  int incx, incy;
#else
void FDecl rws_muvv(unsigned n, float* restrict z, const float* restrict x, int incx, const float* restrict y, int incy)
#endif
{
  while(n--){
    *z++ = *x * *y;
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rws_dvvv(n, z, x, incx, y, incy)
  unsigned n;
  float *z, *x, *y;
  int incx, incy;
#else
void FDecl rws_dvvv(unsigned n, float* restrict z, const float* restrict x, int incx, const float* restrict y, int incy)
#endif
{
  while(n--){
    *z++ = *x / *y;
    x += incx;
    y += incy;
  }
}

/****************************************************************
 *								*
 *		VECTOR --- SCALAR BINARY OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rws_plvs(n, z, x, incx, scalar)
  unsigned n;
  float *z, *x, *scalar;
  int incx;
#else
void FDecl rws_plvs(unsigned n, float* restrict z, const float* restrict x, int incx, const float* restrict scalar)
#endif
{
  while(n--){
    *z++ = *x + *scalar;
    x += incx;
  }
}

#ifdef RW_KR_ONLY
void rws_muvs(n, z, x, incx, scalar)
  unsigned n;
  float *z, *x, *scalar;
  int incx;
#else
void FDecl rws_muvs(unsigned n, float* restrict z, const float* restrict x, int incx, const float* restrict scalar)
#endif
{
  while(n--){
    *z++ = *x * *scalar;
    x += incx;
  }
}

#ifdef RW_KR_ONLY
void rws_dvvs(n, z, x, incx, scalar)
  unsigned n;
  float *z, *x, *scalar;
  int incx;
#else
void FDecl rws_dvvs(unsigned n, float* restrict z, const float* restrict x, int incx, const float* restrict scalar)
#endif
{
  while(n--){
    *z++ = *x / *scalar;
    x += incx;
  }
}

/****************************************************************
 *								*
 *		SCALAR --- VECTOR BINARY OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rws_misv(n, z, scalar, x, incx)
  unsigned n;
  float *z, *scalar, *x;
  int incx;
#else
void FDecl rws_misv(unsigned n, float* restrict z, const float* scalar, const float* restrict x, int incx)
#endif
{
  while(n--){
    *z++ = *scalar - *x;
    x += incx;
  }
}

#ifdef RW_KR_ONLY
void rws_dvsv(n, z, scalar, x, incx)
  unsigned n;
  float *z, *scalar, *x;
  int incx;
#else
void FDecl rws_dvsv(unsigned n, float* restrict z, const float* scalar, const float* restrict x, int incx)
#endif
{
  while(n--){
    *z++ = *scalar / *x;
    x += incx;
  }
}


/*
 * Definitions for the class RandGamma
 *
 * $Header: /users/rcs/mathsrc/randgama.cpp,v 1.3 1993/09/01 16:27:33 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: randgama.cpp,v $
 * Revision 1.3  1993/09/01  16:27:33  alv
 * now uses rwUninitialized form for simple constructors
 *
 * Revision 1.2  1993/01/26  17:52:28  alv
 * removed DOS EOF
 *
 * Revision 1.1  1993/01/22  23:51:02  alv
 * Initial revision
 *
 * 
 *    Rev 1.3   17 Oct 1991 09:15:24   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.2   08 Oct 1991 13:44:46   keffer
 * Math V4.0 beta 2
 * 
 *    Rev 1.1   24 Jul 1991 12:51:50   keffer
 * Added pvcs keywords
 *
 */

#include "rw/dvec.h"
#include "rw/dgenmat.h"
#include "rw/randgama.h"

STARTWRAP
#include <math.h>
ENDWRAP

// Return a random number selected from an gamma distribution of order: orderOfDistribution
double
RandGamma::randValue() {
   double x;
   if(orderOfDistribution < 6) {                             // direct method
      x = 1.;
      for (int j = 0; j < orderOfDistribution; j++)
         x *= RandUniform::randValue();
      return -::log(x);
   }
   else {                                      // rejection method
      double v1, v2, y, s, e;
      int am;
      do {
        do {
          do {
            v1 = 2.* RandUniform::randValue() - 1.; // pick two uniform 
            v2 = 2.* RandUniform::randValue() - 1.; // numbers from -1 to 1
          } while ( v1*v1 + v2*v2 > 1. ); 
          y = v2/v1;
          am = orderOfDistribution - 1;
          s = ::sqrt(2.*am + 1.);
          x = s*y + am;
        } while ( x <= 0. );
        e = (1. + y*y) * ::exp( am*::log(x/am) - s*y);
      } while ( RandUniform::randValue() > e );
      return x;
   }
}


void
RandGamma::randValue(double* d, unsigned n)
{
  while (n--) { *d++ = RandGamma::randValue(); }
}

// Return a vector of n random numbers selected from a gamma distribution
DoubleVec
RandGamma::randValue(unsigned n) {
  DoubleVec temp(n,rwUninitialized);
  while (n--) temp(n) = RandGamma::randValue();
  return temp;
}

// Return a matrix of random numbers selected from a gamma distribution 
DoubleGenMat
RandGamma::randValue(unsigned nr, unsigned nc) {
  DoubleVec tempv = RandGamma::randValue(nr*nc);
  DoubleGenMat temp(tempv, nr, nc);
  return temp;
}

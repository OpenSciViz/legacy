/*
 * BLA-like extensions for type DComplex.
 *
 * Generated from template $Id: xrwbla.c,v 1.3 1993/10/11 18:42:24 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/rwzbla.h"
#include "rw/mathdefs.h"

#include <math.h>
#ifdef BSD
#  include <memory.h>
#else
#  include <string.h>
#endif

/*
 * Divide the DComplex pointed to by a1 by the DComplex pointed to
 * by a2, put the results in result.  There are no restrictions
 * on a1, a2, and result pointing to different data storage.
 */
#ifdef RW_KR_ONLY
static void z_div(result, a1, a2)
  DComplex *result, *a1, *a2;
#else
static void z_div(DComplex *result, const DComplex *a1, const DComplex *a2)
#endif
{
  double p, q, r, i;

  r = a2->r;
  i = a2->i;
  p = r < 0 ? -r : r;
  q = i < 0 ? -i : i;

  if (p <= q) {
    q = r/i;
    p = (q*q + 1) * i;
    r = a1->r;
    i = a1->i;
  }
  else {
    q = -i/r;
    p = (q*q + 1) * r;
    r = -a1->i;
    i = a1->r;
  }
  result->r = (r*q + i)/p;
  result->i = (i*q - r)/p;
}

/****************************************************************
 *								*
 *			UTILITY					*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwzcopy(n, x, incx, y, incy )
  unsigned n;
  DComplex *x, *y;
  int incx, incy;
#else
void FDecl rwzcopy( unsigned n, DComplex* restrict x, int incx, const DComplex* restrict y, int incy)
#endif
{
  if (n==0) return;

  if (incx == 1 && incy == 1) {
    memcpy(x, y, n*sizeof(DComplex));
  }
  else {
    /*
     * Code for increments not equal to one
     */
    while (n--) {
#ifdef HAS_NATIVE_COMPLEX
      *x = *y;
#else
      x->r = y->r;  x->i = y->i;
#endif
      x += incx;
      y += incy;
    }
  }
}

#ifdef RW_KR_ONLY
void rwzset(n, x, incx, scalar)
  unsigned n;
  DComplex *x, *scalar;
  int incx;
#else
void FDecl rwzset(unsigned n, DComplex* restrict x, int incx, const DComplex* restrict scalar)
#endif
{
#ifdef HAS_NATIVE_COMPLEX
  if(incx==1)
    while (n--) { *x++ = *scalar; }
  else
    while (n--) { *x = *scalar; x+=incx; }
#else
  if(incx==1)
    while (n--) { x->r = scalar->r; x->i = scalar->i; x++; }
  else
    while (n--) { x->r = scalar->r; x->i = scalar->i; x+=incx; }
#endif
}

#ifdef RW_KR_ONLY
int rwzsame(n, x, incx, y, incy)
  unsigned n;
  DComplex *x, *y;
  int incx, incy;
#else
int FDecl rwzsame(unsigned n, const DComplex* restrict x, int incx, const DComplex* restrict y, int incy)
#endif
{
  while(n--){
#ifdef HAS_NATIVE_COMPLEX
    if( *x != *y ) return 0;
#else
    if( x->r != y->r || x->i != y->i ) return 0;
#endif
    x += incx;
    y += incy;
  }
  return 1;
}


/****************************************************************
 *								*
 *			DOT PRODUCTS				*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwzdot(n, x, incx, y, incy, result)
  unsigned n;
  DComplex *x, *y, *result;
  int incx, incy;
#else
void FDecl rwzdot(unsigned n, const DComplex* restrict x, int incx, const DComplex* restrict y, int incy, DComplex* result)
#endif
{
  double sumr = 0;	/* Always accumulate in double precision */
  double sumi = 0;
  while(n--){
    sumr += x->r * y->r - x->i * y->i;
    sumi += x->r * y->i + x->i * y->r;
    x += incx;
    y += incy;
  }
  result->r = sumr;
  result->i = sumi;

}

/*
 * Conjugate dot product:
 */

#ifdef RW_KR_ONLY
void rwzcdot(n, x, incx, y, incy, result)
  unsigned n;
  DComplex *x, *y, *result;
  int incx, incy;
#else
void FDecl rwzcdot(unsigned n, const DComplex* restrict x, int incx, const DComplex* restrict y, int incy, DComplex* result)
#endif
{
  double sumr = 0;	/* Always accumulate in double precision */
  double sumi = 0;
  while(n--){
    sumr += x->r * y->r + x->i * y->i;
    sumi += x->r * y->i - x->i * y->r;
    x += incx;
    y += incy;
  }
  result->r = sumr;
  result->i = sumi;
}

/****************************************************************
 *								*
 *		VECTOR --- VECTOR ASSIGNMENT OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwz_aplvv(n, x, incx, y, incy)
  unsigned n;
  DComplex *x, *y;
  int incx, incy;
#else
void FDecl rwz_aplvv(unsigned n, DComplex* restrict x, int incx, const DComplex* restrict y, int incy)
#endif
{
#ifdef HAS_NATIVE_COMPLEX
  while(n--){
    *x += *y;
    x += incx;
    y += incy;
  }
#else
  while(n--){
    x->r += y->r;
    x->i += y->i;
    x += incx;
    y += incy;
  }
#endif
}

#ifdef RW_KR_ONLY
void rwz_amivv(n, x, incx, y, incy)
  unsigned n;
  DComplex *x, *y;
  int incx, incy;
#else
void FDecl rwz_amivv(unsigned n, DComplex* restrict x, int incx, const DComplex* restrict y, int incy)
#endif
{
#ifdef HAS_NATIVE_COMPLEX
  while(n--){
    *x -= *y;
    x += incx;
    y += incy;
  }
#else
  while(n--){
    x->r -= y->r;
    x->i -= y->i;
    x += incx;
    y += incy;
  }
#endif
}

#ifdef RW_KR_ONLY
void rwz_amuvv(n, x, incx, y, incy)
  unsigned n;
  DComplex *x, *y;
  int incx, incy;
#else
void FDecl rwz_amuvv(unsigned n, DComplex* restrict x, int incx, const DComplex* restrict y, int incy)
#endif
{
#ifdef HAS_NATIVE_COMPLEX
  while(n--){
    *x *= *y;
    x += incx;
    y += incy;
  }
#else
  register double re;
  while(n--){
    re = x->r;
    x->r = re * y->r - x->i * y->i;
    x->i = re * y->i + x->i * y->r;
    x += incx;
    y += incy;
  }
#endif
}

#ifdef RW_KR_ONLY
void rwz_advvv(n, x, incx, y, incy)
  unsigned n;
  DComplex *x, *y;
  int incx, incy;
#else
void FDecl rwz_advvv(unsigned n, DComplex* restrict x, int incx, const DComplex* restrict y, int incy)
#endif
{
#ifdef HAS_NATIVE_COMPLEX
  while(n--){
    *x /= *y;
    x += incx;
    y += incy;
  }
#else
  while(n--){
    z_div(x,x,y);
    x += incx;
    y += incy;
  }
#endif
}

/****************************************************************
 *								*
 *		VECTOR --- SCALAR ASSIGNMENT OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwz_aplvs(n, x, incx, scalar)
  unsigned n;
  DComplex *x, *scalar;
  int incx;
#else
void FDecl rwz_aplvs(unsigned n, DComplex* restrict x, int incx, const DComplex* restrict scalar)
#endif
{
#ifdef HAS_NATIVE_COMPLEX
  while(n--) {
    *x += *scalar;
    x += incx;
  }
#else
  while(n--) {
    x->r += scalar->r;
    x->i += scalar->i;
    x += incx;
  }
#endif
}

#ifdef RW_KR_ONLY
void rwz_amuvs(n, x, incx, scalar)
  unsigned n;
  DComplex *x, *scalar;
  int incx;
#else
void FDecl rwz_amuvs(unsigned n, DComplex* restrict x, int incx, const DComplex* restrict scalar)
#endif
{
#ifdef HAS_NATIVE_COMPLEX
  while(n--) {
    *x *= *scalar;
    x += incx;
  }
#else
  register double re;
  while(n--) {
    re = x->r;
    x->r = re * scalar->r - x->i * scalar->i;
    x->i = re * scalar->i + x->i * scalar->r;
    x += incx;
  }
#endif
}

#ifdef RW_KR_ONLY
void rwz_advvs(n, x, incx, scalar)
  unsigned n;
  DComplex *x, *scalar;
  int incx;
#else
void FDecl rwz_advvs(unsigned n, DComplex* restrict x, int incx, const DComplex* restrict scalar)
#endif
{
#ifdef HAS_NATIVE_COMPLEX
  while(n--) {
    *x /= *scalar;
    x += incx;
  }
#else
  while(n--) {
    z_div(x,x,scalar);
    x += incx;
  }
#endif
}

/****************************************************************
 *								*
 *		VECTOR --- VECTOR BINARY OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwz_plvv(n, z, x, incx, y, incy)
  unsigned n;
  DComplex *z, *x, *y;
  int incx, incy;
#else
void FDecl rwz_plvv(unsigned n, DComplex* restrict z, const DComplex* restrict x, int incx, const DComplex* restrict y, int incy)
#endif
{
  while(n--){
#ifdef HAS_NATIVE_COMPLEX
    *z++ = *x + *y;
#else
    z->r = x->r + y->r;
    z->i = x->i + y->i;
    z++;
#endif
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rwz_mivv(n, z, x, incx, y, incy)
  unsigned n;
  DComplex *z, *x, *y;
  int incx, incy;
#else
void FDecl rwz_mivv(unsigned n, DComplex* restrict z, const DComplex* restrict x, int incx, const DComplex* restrict y, int incy)
#endif
{
  while(n--){
#ifdef HAS_NATIVE_COMPLEX
    *z++ = *x - *y;
#else
    z->r = x->r - y->r;
    z->i = x->i - y->i;
    z++;
#endif
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rwz_muvv(n, z, x, incx, y, incy)
  unsigned n;
  DComplex *z, *x, *y;
  int incx, incy;
#else
void FDecl rwz_muvv(unsigned n, DComplex* restrict z, const DComplex* restrict x, int incx, const DComplex* restrict y, int incy)
#endif
{
  while(n--){
#ifdef HAS_NATIVE_COMPLEX
    *z++ = *x * *y;
#else
    z->r = x->r * y->r - x->i * y->i;
    z->i = x->r * y->i + x->i * y->r;
    z++;
#endif
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rwz_dvvv(n, z, x, incx, y, incy)
  unsigned n;
  DComplex *z, *x, *y;
  int incx, incy;
#else
void FDecl rwz_dvvv(unsigned n, DComplex* restrict z, const DComplex* restrict x, int incx, const DComplex* restrict y, int incy)
#endif
{
  while(n--){
#ifdef HAS_NATIVE_COMPLEX
    *z++ = *x / *y;
#else
    z_div(z++,x,y);
#endif
    x += incx;
    y += incy;
  }
}

/****************************************************************
 *								*
 *		VECTOR --- SCALAR BINARY OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwz_plvs(n, z, x, incx, scalar)
  unsigned n;
  DComplex *z, *x, *scalar;
  int incx;
#else
void FDecl rwz_plvs(unsigned n, DComplex* restrict z, const DComplex* restrict x, int incx, const DComplex* restrict scalar)
#endif
{
  while(n--){
#ifdef HAS_NATIVE_COMPLEX
    *z++ = *x + *scalar;
#else
    z->r = x->r + scalar->r;
    z->i = x->i + scalar->i;
	z++;
#endif
    x += incx;
  }
}

#ifdef RW_KR_ONLY
void rwz_muvs(n, z, x, incx, scalar)
  unsigned n;
  DComplex *z, *x, *scalar;
  int incx;
#else
void FDecl rwz_muvs(unsigned n, DComplex* restrict z, const DComplex* restrict x, int incx, const DComplex* restrict scalar)
#endif
{
  while(n--){
#ifdef HAS_NATIVE_COMPLEX
    *z++ = *x * *scalar;
#else
    z->r = x->r * scalar->r - x->i * scalar->i;
    z->i = x->r * scalar->i + x->i * scalar->r;
	z++;
#endif
    x += incx;
  }
}

#ifdef RW_KR_ONLY
void rwz_dvvs(n, z, x, incx, scalar)
  unsigned n;
  DComplex *z, *x, *scalar;
  int incx;
#else
void FDecl rwz_dvvs(unsigned n, DComplex* restrict z, const DComplex* restrict x, int incx, const DComplex* restrict scalar)
#endif
{
  while(n--){
#ifdef HAS_NATIVE_COMPLEX
    *z++ = *x / *scalar;
#else
    z_div(z++,x,scalar);
#endif
    x += incx;
  }
}

/****************************************************************
 *								*
 *		SCALAR --- VECTOR BINARY OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwz_misv(n, z, scalar, x, incx)
  unsigned n;
  DComplex *z, *scalar, *x;
  int incx;
#else
void FDecl rwz_misv(unsigned n, DComplex* restrict z, const DComplex* scalar, const DComplex* restrict x, int incx)
#endif
{
  while(n--){
#ifdef HAS_NATIVE_COMPLEX
    *z++ = *scalar - *x;
#else
    z->r = scalar->r - x->r;
    z->i = scalar->i - x->i;
	z++;
#endif
    x += incx;
  }
}

#ifdef RW_KR_ONLY
void rwz_dvsv(n, z, scalar, x, incx)
  unsigned n;
  DComplex *z, *scalar, *x;
  int incx;
#else
void FDecl rwz_dvsv(unsigned n, DComplex* restrict z, const DComplex* scalar, const DComplex* restrict x, int incx)
#endif
{
  while(n--){
#ifdef HAS_NATIVE_COMPLEX
    *z++ = *scalar / *x;
#else
    z_div(z++,scalar,x);
#endif
    x += incx;
  }
}


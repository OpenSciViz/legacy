/*
 * Definitions for DComplexGenMat
 *
 * Generated from template $Id: xmat.cpp,v 1.10 1993/10/04 21:44:01 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include <rw/strstrea.h>  /* I use istrstream to parse character string ctor */
#include "rw/cgenmat.h"
#include "rw/rwbla.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"
#include "rw/rand.h"


RCSID("$Header: /users/rcs/mathsrc/xmat.cpp,v 1.10 1993/10/04 21:44:01 alv Exp $");

DComplexGenMat::DComplexGenMat(const char *string)
 : RWMatView()
{
  istrstream s((char*)string);
  scanFrom(s);
  if (!s.good()) {
    RWTHROW( RWExternalErr( RWMessage(RWMATH_STRCTOR,string) ));
  }
}

DComplexGenMat::DComplexGenMat(unsigned m, unsigned n, RWRand& r)
 : RWMatView(m,n,sizeof(DComplex))
{
  for(int j=n; j--;) {
    for(int i=m; i--;) {
    (*this)(i,j) = r.complex();
    }
  }
}

DComplexGenMat::DComplexGenMat(unsigned m, unsigned n, DComplex scalar)
  : RWMatView(m,n,sizeof(DComplex))
{
  rwzset(n*m, data(), 1, &scalar);
}


// Type conversion: real->complex
DComplexGenMat::DComplexGenMat(const DoubleGenMat& re)
  : RWMatView(re.rows(),re.cols(),sizeof(DComplex))
{
  DComplex* cp = data();
  for(MatrixLooper l(re.rows(),re.cols(), re.rowStride(),re.colStride(), RWDataView::COLUMN_MAJOR); l; ++l) {
#ifdef COMPLEX_PACKS
//    static const double zero = 0;     /* "static" required for Glockenspiel */
    double zero = 0;     		     /* above bombs on lucid, so use this nice simple form */
#ifdef IMAG_LEADS
    rwdcopy(l.length, (double*)cp+1, 2, re.data()+l.start, l.stride);
    rwdset(l.length, (double*)cp, 2, &zero);
#else  /* Imaginary does not lead */
    rwdcopy(l.length, (double*)cp, 2, re.data()+l.start, l.stride);
    rwdset(l.length, (double*)cp+1, 2, &zero);
#endif
    cp += l.length;
  }

#else /* Complex does not pack */

  for(int j=cols(); j--;) {
    for(int i=rows(); i--;) {
      (*this)(i,j) = re(i,j);
    }
  }
#endif /* COMPLEX_PACKS */
}  

DComplexGenMat::DComplexGenMat(const DoubleGenMat& re, const DoubleGenMat& im)
  : RWMatView(re.rows(),re.cols(),sizeof(DComplex))
{
  lengthCheck(im.rows(),im.cols());
  DComplex* cp = data();
  for(DoubleMatrixLooper l(re.rows(),re.cols(),re.rowStride(),re.colStride(),
	                   im.rowStride(),im.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
#ifdef COMPLEX_PACKS
#ifdef IMAG_LEADS
    rwdcopy(l.length, (double*)cp+1, 2, re.data()+l.start1, l.stride1);
    rwdcopy(l.length, (double*)cp, 2, im.data()+l.start2, l.stride2);
#else  /* Imaginary does not lead */
    rwdcopy(l.length, (double*)cp, 2, re.data()+l.start1, l.stride1);
    rwdcopy(l.length, (double*)cp+1, 2, im.data()+l.start2, l.stride2);
#endif
    cp += l.length;
  }

#else /* Complex does not pack */
  for(int j=cols(); j--;) {
    for(int i=rows(); i--;) {
      (*this)(i,j) = DComplex(re(i,j),im(i,j));
    }
  }
#endif /* COMPLEX_PACKS */
}


DComplexGenMat::DComplexGenMat(const DComplex* dat, unsigned m, unsigned n, Storage storage)
  : RWMatView(m,n,sizeof(DComplex),storage)
{
  rwzcopy(nrows*ncols, data(), 1, dat, 1);
}

DComplexGenMat::DComplexGenMat(const DComplexVec& vec, unsigned m, unsigned n, Storage storage)
  : RWMatView(vec,vec.begin,m,n,(storage==ROW_MAJOR?n:1)*vec.stride(),
                                (storage==ROW_MAJOR?1:m)*vec.stride())
{
  vec.lengthCheck(m*n);
}

DComplexGenMat::DComplexGenMat(RWBlock *block, unsigned m, unsigned n, Storage storage)
  : RWMatView(block, m, n, storage)
{
  unsigned minlen = m*n*sizeof(DComplex);
  if (block->length() < minlen) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_SHORTBLOCK, block->length(), minlen) ));
  }
}

// This constructor is used internally by real() and imag(), the
// slice() function and the Array::op()() functions.
DComplexGenMat::DComplexGenMat(const RWDataView& b, DComplex* start, unsigned m, unsigned n, int rowstr, int colstr)
  : RWMatView(b,start,m,n,rowstr,colstr)
{
}

DComplexGenMat& DComplexGenMat::operator=(const DComplexGenMat& rhs)
{
  lengthCheck(rhs.rows(),rhs.cols());
  if(sameDataBlock(rhs))
    (*this) = rhs.copy();   // Avoid aliasing problems
  else {
    for(DoubleMatrixLooper l(nrows,ncols,rowstep,colstep,rhs.rowstep,rhs.colstep); l; ++l) {
      rwzcopy(l.length, data()+l.start1, l.stride1, rhs.data()+l.start2, l.stride2);
    }
  }
  return *this;
}

DComplexGenMat& DComplexGenMat::operator=(DComplex scalar)
{
  for(MatrixLooper l(nrows,ncols,rowstep,colstep); l; ++l) {
    rwzset(l.length,data()+l.start,l.stride,&scalar);
  }
  return *this;
}

DComplexGenMat& DComplexGenMat::reference(const DComplexGenMat& v)
{
  RWMatView::reference(v);
  return *this;
}

// Return copy of self with distinct instance variables
DComplexGenMat DComplexGenMat::copy(Storage s) const
{
  DComplexGenMat temp(nrows,ncols,rwUninitialized,s);
  temp = *this;
  return temp;
}

// Synonym for copy().  Not made inline because some compilers
// have trouble with inlined temporaries with destructors
DComplexGenMat DComplexGenMat::deepCopy(Storage s) const
{
  return copy(s);
}

// Guarantee that references==1, rowstep=1 and colstep=nrows
void DComplexGenMat::deepenShallowCopy(Storage s)
{
  RWBoolean isStrideCompact = (s==COLUMN_MAJOR) ? (rowstep==1 && colstep==nrows)
                                                : (colstep==1 && rowstep==ncols);
  if (!isStrideCompact || !isSimpleView()) {
    RWMatView::reference(copy(s));
  }
}

void DComplexGenMat::resize(unsigned m, unsigned n)
{
  if(nrows!=m || ncols!=n) {
    DComplexGenMat temp(m,n,DComplex(0,0));
    int rowsToCopy = (nrows<m) ? nrows : m;
    int colsToCopy = (ncols<n) ? ncols : n;
    if (rowsToCopy>0 && colsToCopy>0) {
      for(DoubleMatrixLooper l(rowsToCopy,colsToCopy, temp.rowstep,temp.colstep, rowstep,colstep); l; ++l) {
        rwzcopy(l.length,temp.data()+l.start1,l.stride1,data()+l.start2,l.stride2);
      }
    }
    RWMatView::reference(temp);
  }
}

void DComplexGenMat::reshape(unsigned m, unsigned n, Storage s)
{
  DComplexGenMat temp(m,n,rwUninitialized,s);
  RWMatView::reference(temp);
}

DComplexVec DComplexGenMat::fastSlice(int i, int j, unsigned n, int rowstr, int colstr) const
{
  return DComplexVec(*this,(DComplex*)begin+i*rowstep+j*colstep,n,rowstr*rowstep+colstr*colstep);
}

DComplexGenMat DComplexGenMat::fastSlice(int i, int j, unsigned m, unsigned n, int rowstr1, int colstr1, int rowstr2, int colstr2) const
{
  return DComplexGenMat(*this,(DComplex*)begin+i*rowstep+j*colstep,m,n,
                    rowstr1*rowstep+colstr1*colstep,
                    rowstr2*rowstep+colstr2*colstep);
}

DComplexVec DComplexGenMat::slice(int i, int j, unsigned n, int rowstr, int colstr) const
{
  sliceCheck(i,j,n,rowstr,colstr);
  return fastSlice(i,j,n,rowstr,colstr);
}

DComplexGenMat DComplexGenMat::slice(int i, int j, unsigned m, unsigned n, int rowstr1, int colstr1, int rowstr2, int colstr2) const
{
  sliceCheck(i,j,m,n,rowstr1,colstr1,rowstr2,colstr2);
  return fastSlice(i,j,m,n,rowstr1,colstr1,rowstr2,colstr2);
}

const DComplexVec DComplexGenMat::diagonal(int k) const
{
  // Count on the slice function to do bounds checking
  if (k>=0) {
    return slice(0,k,(nrows<ncols-k) ? nrows : (ncols-k),1,1);
  } else {
    return slice(-k,0,(ncols<nrows+k) ? ncols : (nrows+k),1,1);
  }
}

DComplexVec DComplexGenMat::diagonal(int k)
{
  // Count on the slice function to do bounds checking
  if (k>=0) {
    return slice(0,k,(nrows<ncols-k) ? nrows : (ncols-k),1,1);
  } else {
    return slice(-k,0,(ncols<nrows+k) ? ncols : (nrows+k),1,1);
  }
}

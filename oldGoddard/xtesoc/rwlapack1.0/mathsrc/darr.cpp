/*
 * Definitions for DoubleArray
 *
 * Generated from template $Id: xarr.cpp,v 1.12 1993/10/04 21:44:01 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1992. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include <rw/strstrea.h>  /* I use istrstream to parse character string ctor */
#include "rw/darr.h"
#include "rw/dgenmat.h"
#include "rw/dvec.h"
#include "rw/rwbla.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"
#include "rw/rand.h"

#define REGISTER register


RCSID("$Header");

DoubleArray::DoubleArray(const char *string)
 : RWArrayView(IntVec(),sizeof(double))
{
  istrstream s((char*)string);
  scanFrom(s);
  if (!s.good()) {
    RWTHROW( RWExternalErr( RWMessage(RWMATH_STRCTOR) ));
  }
}

DoubleArray::DoubleArray()
  : RWArrayView(IntVec(),sizeof(double))
{
}

DoubleArray::DoubleArray(const IntVec& n, RWUninitialized, Storage storage)
  : RWArrayView(n,sizeof(double),storage)
{
}

DoubleArray::DoubleArray(unsigned nx, unsigned ny, unsigned nz, RWUninitialized)
  : RWArrayView(nx,ny,nz,sizeof(double))
{
}

DoubleArray::DoubleArray(unsigned nx, unsigned ny, unsigned nz, unsigned nw, RWUninitialized)
  : RWArrayView(nx,ny,nz,nw,sizeof(double))
{
}

static void randFill(DoubleArray& X, RWRand& r)
{
  for(RWMultiIndex i(X.length()); i; ++i) {
    X(i) = (double)r();
  }
}

DoubleArray::DoubleArray(const IntVec& n, RWRand& r, Storage storage)
  : RWArrayView(n,sizeof(double),storage)
{
  randFill(*this,r);
}

DoubleArray::DoubleArray(unsigned nx, unsigned ny, unsigned nz, RWRand& r)
  : RWArrayView(nx,ny,nz,sizeof(double))
{
  randFill(*this,r);
}

DoubleArray::DoubleArray(unsigned nx, unsigned ny, unsigned nz, unsigned nw, RWRand& r)
  : RWArrayView(nx,ny,nz,nw,sizeof(double))
{
  randFill(*this,r);
}

DoubleArray::DoubleArray(const IntVec& n, double scalar)
  : RWArrayView(n,sizeof(double))
{
  rwdset(prod(n),data(),1,&scalar);
}

DoubleArray::DoubleArray(unsigned nx, unsigned ny, unsigned nz, double scalar)
  : RWArrayView(nx,ny,nz,sizeof(double))
{
  rwdset(nx*ny*nz,data(),1,&scalar);
}

DoubleArray::DoubleArray(unsigned nx, unsigned ny, unsigned nz, unsigned nw, double scalar)
  : RWArrayView(nx,ny,nz,nw,sizeof(double))
{
  rwdset(nx*ny*nz*nw,data(),1,&scalar);
}


// Copy constructor
DoubleArray::DoubleArray(const DoubleArray& a)
  : RWArrayView(a)
{
}

DoubleArray::DoubleArray(const double* dat, const IntVec& n)
  : RWArrayView(n,sizeof(double))
{
  rwdcopy(prod(n), data(), 1, dat, 1);
}

DoubleArray::DoubleArray(const double* dat, unsigned nx, unsigned ny, unsigned nz)
  : RWArrayView(nx,ny,nz,sizeof(double))
{
  rwdcopy(nx*ny*nz, data(), 1, dat, 1);
}

DoubleArray::DoubleArray(const double* dat, unsigned nx, unsigned ny, unsigned nz, unsigned nw)
  : RWArrayView(nx,ny,nz,nw,sizeof(double))
{
  rwdcopy(nx*ny*nz*nw, data(), 1, dat, 1);
}

DoubleArray::DoubleArray(const DoubleVec& dat, const IntVec& n)
  : RWArrayView(n,sizeof(double))
{
  rwdcopy(prod(n), data(), 1, dat.data(), dat.stride());
}

DoubleArray::DoubleArray(const DoubleVec& dat, unsigned nx, unsigned ny, unsigned nz)
  : RWArrayView(nx,ny,nz,sizeof(double))
{
  rwdcopy(nx*ny*nz, data(), 1, dat.data(), dat.stride());
}

DoubleArray::DoubleArray(const DoubleVec& dat, unsigned nx, unsigned ny, unsigned nz, unsigned nw)
  : RWArrayView(nx,ny,nz,nw,sizeof(double))
{
  rwdcopy(nx*ny*nz*nw, data(), 1, dat.data(), dat.stride());
}

DoubleArray::DoubleArray(RWBlock *block, const IntVec& n)
  : RWArrayView(block,n)
{
  unsigned minlen = prod(length())*sizeof(double);
  if (block->length() < minlen) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_SHORTBLOCK, block->length(), minlen) ));
  }
}

DoubleArray::DoubleArray(RWBlock *block, unsigned nx, unsigned ny, unsigned nz)
  : RWArrayView(block, nx, ny, nz)
{
  unsigned minlen = prod(length())*sizeof(double);
  if (block->length() < minlen) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_SHORTBLOCK, block->length(), minlen) ));
  }
}

DoubleArray::DoubleArray(RWBlock *block, unsigned nx, unsigned ny, unsigned nz, unsigned nw)
  : RWArrayView(block, nx, ny, nz, nw)
{
  unsigned minlen = prod(length())*sizeof(double);
  if (block->length() < minlen) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_SHORTBLOCK, block->length(), minlen) ));
  }
}

DoubleArray::DoubleArray(const DoubleVec& v)
  : RWArrayView(v,(void*)v.data(),IntVec(1,v.length()),IntVec(1,1))
{
}

DoubleArray::DoubleArray(const DoubleGenMat& m)
  : RWArrayView(m,(void*)m.data(),
       IntVec(2,m.rows(),m.cols()-m.rows()),                 // [ m.rows() m.cols() ]
       IntVec(2,m.rowStride(),m.colStride()-m.rowStride())) // [ m.rowStride() m.colStride() ]
{
}

// This constructor is used internally by real() and imag() and slices:
DoubleArray::DoubleArray(const RWDataView& b, double* start, const IntVec& n, const IntVec& str)
  : RWArrayView(b,start,n,str)
{
}

DoubleArray& DoubleArray::operator=(const DoubleArray& rhs)
{
  lengthCheck(rhs.length());
  if(sameDataBlock(rhs)) {
    (*this) = rhs.deepCopy();   // Avoid aliasing problems
  } else {
    for(DoubleArrayLooper l(npts,step,rhs.step); l; ++l) {
      rwdcopy(l.length, data()+l.start1, l.stride1, rhs.data()+l.start2, l.stride2);
    }
  }
  return *this;
}

DoubleArray& DoubleArray::operator=(double scalar)
{
  for(ArrayLooper l(npts,step); l; ++l) {
    rwdset(l.length,data()+l.start,l.stride,&scalar);
  }
  return *this;
}

DoubleArray& DoubleArray::reference(const DoubleArray& v)
{
  RWArrayView::reference(v);
  return *this;
}

// Return copy of self with distinct instance variables
DoubleArray DoubleArray::deepCopy() const
{
  return copy();
}

// Synonym for deepCopy().  Not made inline because some compilers
// have trouble with inlined temporaries with destructors
DoubleArray DoubleArray::copy() const
{
  DoubleArray temp(npts,rwUninitialized,COLUMN_MAJOR);
  temp = *this;
  return temp;
}

// Guarantee that references==1 and stride==1
void DoubleArray::deepenShallowCopy()
{
  RWBoolean isStrideCompact = (dimension()==0 || step((int)0)==1);
  for( int r=npts.length()-1; r>0 && isStrideCompact; --r ) {
    if (step(r) != npts(r-1)*step(r-1)) isStrideCompact=FALSE;
  }
  if (!isStrideCompact || !isSimpleView()) {
    RWArrayView::reference(copy());
  }
}

void DoubleArray::resize(const IntVec& N)
{
  if(N != npts){
    DoubleArray temp(N,(double)0);
    int p = npts.length()<N.length() ? npts.length() : N.length();  // # dimensions to copy
    if (p>0) {
      IntVec n(p,rwUninitialized);   // The size of the common parts of the arrays
      for(int i=0; i<p; i++) {
        n(i) = (N(i)<npts(i)) ? N(i) : npts(i);
      }
  
      // Build a view of the source common part
      IntGenMat sstr(dimension(),p,0);
      sstr.diagonal() = 1;
      DoubleArray sp = slice(IntVec(dimension(),0), n, sstr);
  
      // Build a view of the destination common part
      IntGenMat dstr(N.length(),p,0);
      dstr.diagonal() = 1;
      DoubleArray dp = temp.slice(IntVec(N.length(),0), n, dstr);
  
      dp = sp;
    }
    reference(temp);
  }
}

void DoubleArray::resize(unsigned m, unsigned n, unsigned o)
{
  IntVec i(3,rwUninitialized);
  i((int)0) = m;
  i(1) = n;
  i(2) = o;
  resize(i);
}

void DoubleArray::resize(unsigned m, unsigned n, unsigned o, unsigned p)
{
  IntVec i(4,rwUninitialized);
  i((int)0) = m;
  i(1) = n;
  i(2) = o;
  i(3) = p;
  resize(i);
}

void DoubleArray::reshape(const IntVec& N)
{
  if (N!=npts) {
    DoubleArray temp(N,rwUninitialized,COLUMN_MAJOR);
    reference(temp);
  }
}

void DoubleArray::reshape(unsigned m, unsigned n, unsigned o)
{
  IntVec i(3,rwUninitialized);
  i((int)0) = m;
  i(1) = n;
  i(2) = o;
  reshape(i);
}

void DoubleArray::reshape(unsigned m, unsigned n, unsigned o, unsigned p)
{
  IntVec i(4,rwUninitialized);
  i((int)0) = m;
  i(1) = n;
  i(2) = o;
  i(3) = p;
  reshape(i);
}

DoubleArray DoubleArray::slice(const IntVec& start, const IntVec& n, const IntGenMat& str) const
{
  sliceCheck(start, n, str);
  return DoubleArray(*this, (double*)data()+dot(start,stride()), n, product(stride(),str));
}

double toScalar(const DoubleArray& A)
{
  A.dimensionCheck(0);
  return *(A.data());
}

DoubleVec toVec(const DoubleArray& A)
{
  A.dimensionCheck(1);
  return DoubleVec(A,(double*)A.data(),(unsigned)A.length((int)0),A.stride((int)0));
}

DoubleGenMat toGenMat(const DoubleArray& A)
{
  A.dimensionCheck(2);
  return DoubleGenMat(A,(double*)A.data(),(unsigned)A.length((int)0),(unsigned)A.length(1),A.stride((int)0),A.stride(1));
}


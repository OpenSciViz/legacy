> #
> # $Header: /users/rcs/mathsrc/xvec.cpp,v 1.9 1993/10/04 21:44:01 alv Exp $
> #
> # $Log: xvec.cpp,v $
> # Revision 1.9  1993/10/04  21:44:01  alv
> # ported to Windows NT
> #
> # Revision 1.8  1993/09/19  17:30:49  alv
> # ported to lucid v3
> #
> # Revision 1.7  1993/09/17  18:14:38  alv
> # added RWRand& constructor
> #
> # Revision 1.6  1993/09/01  16:28:00  alv
> # now uses rwUninitialized form for simple constructors
> #
> # Revision 1.5  1993/08/17  19:02:00  alv
> # now allows use of custom RWBlocks
> #
> # Revision 1.3  1993/03/22  15:51:31  alv
> # changed PVCS Workfile keyword to RCS ID keyword
> #
> # Revision 1.2  1993/03/04  18:57:38  alv
> # changed char* ctor to const char*
> #
> # Revision 1.1  1993/01/22  23:51:09  alv
> # Initial revision
> #
> # 
> #    Rev 1.8   24 Mar 1992 16:07:58   KEFFER
> # Changed from BOUNDS_CHECK to RWBOUNDS_CHECK
> # 
> #    Rev 1.7   25 Oct 1991 08:42:46   keffer
> # Temporary made for promotable types passed to BLAs
> # 
> #    Rev 1.6   17 Oct 1991 09:15:14   keffer
> # Changed include path to <rw/xxx.h>
> # 
> #    Rev 1.4   24 Sep 1991 18:54:18   keffer
> # Ported to Zortech V3.0
> # 
> #    Rev 1.3   27 Jul 1991 14:11:52   keffer
> # Now based on the Rogue Wave BLA extensions.
> # Complex constructors take advantage of COMPLEX_PACKS where possible.
> # 
> #    Rev 1.2   24 Jul 1991 12:52:08   keffer
> # Added pvcs keywords
> #
> define <ClassType>=Vec
> include macros
/*
 * Definitions for <T>Vec
 *
 * Generated from template $Id: xvec.cpp,v 1.9 1993/10/04 21:44:01 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include <rw/strstrea.h>  /* I use istrstream to parse character string ctor */
#include "rw/<a>vec.h"
#include "rw/rw<bla>bla.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"
#include "rw/rand.h"
> if <R/C>==Complex
>   # Pick the proper type of copy and set for the underlying precision
>   if <p>==double
#include "rw/rwdbla.h"
>     define COPY=rwdcopy  SET=rwdset
>   elif <p>==float
#include "rw/rwsbla.h"
>     define COPY=rwscopy  SET=rwsset
>   else
>     err Unrecognized precision <p>
>   endif
> endif

RCSID("$Header: /users/rcs/mathsrc/xvec.cpp,v 1.9 1993/10/04 21:44:01 alv Exp $");

<T>Vec::<T>Vec(const char *string)
 : RWVecView()
{
  istrstream s((char*)string);
  scanFrom(s);
  if (!s.good()) {
    RWTHROW( RWExternalErr( RWMessage(RWMATH_STRCTOR,string) ));
  }
}

<T>Vec::<T>Vec(unsigned n, RWRand& r)
 : RWVecView(n,sizeof(<T>))
{
  while(n--) {
> if <R/C>==Complex    
    (*this)(n) = r.complex();
> else
    (*this)(n) = (<t>)r();
> endif
  }
}

<T>Vec::<T>Vec(unsigned n, <t> scalar)
 : RWVecView(n,sizeof(<T>))
{
> if <t>==double || <t>==int || <R/C>==Complex
  rw<bla>set(n, data(), 1, &scalar);
> else
  <t> scalar2 = scalar;	// Temporary required to finesse K&R compilers
  rw<bla>set(n, data(), 1, &scalar2);
> endif
}

<T>Vec::<T>Vec(unsigned n, <t> scalar, <t> by)
 : RWVecView(n,sizeof(<T>))
{
  register int i = n;
  <t>* dp = data();
  <t> value = scalar;
  <t> byvalue = by;
  while(i--) {*dp++ = value; value += byvalue;}
}

> # Special code for constructors of complex types:
> if <R/C>==Complex

// Type conversion: real->complex
<T>Vec::<T>Vec(const <P>Vec& re)
  : RWVecView(re.length(),sizeof(<t>))
{
#ifdef COMPLEX_PACKS
//    static const <p> zero = 0;     /* "static" required for Glockenspiel */
    <p> zero = 0;     		     /* above bombs on lucid, so use this nice simple form */
#ifdef IMAG_LEADS
  COPY(npts, (<p>*)data()+1, 2, re.data(), re.stride());
  SET(npts, (<p>*)data(), 2, &zero);
#else  /* Imaginary does not lead */
  COPY(npts, (<p>*)data(), 2, re.data(), re.stride());
  SET(npts, (<p>*)data()+1, 2, &zero);
#endif

#else /* Complex does not pack */

  for(int i=length(); i--;) {
    (*this)(i) = re(i);
  }
#endif /* COMPLEX_PACKS */
}  

<T>Vec::<T>Vec(const <P>Vec& re, const <P>Vec& im)
  : RWVecView(re.length(),sizeof(<t>))
{
  lengthCheck(im.length());
#ifdef COMPLEX_PACKS
#ifdef IMAG_LEADS
  COPY(npts, (<p>*)data()+1, 2, re.data(), re.stride());
  COPY(npts, (<p>*)data(), 2, im.data(), im.stride());
#else  /* Imaginary does not lead */
  COPY(npts, (<p>*)data(), 2, re.data(), re.stride());
  COPY(npts, (<p>*)data()+1, 2, im.data(), im.stride());
#endif

#else /* Complex does not pack */
  for(int i=length(); i--;) {
    (*this)(i) = <t>(re(i),im(i));
  }
#endif /* COMPLEX_PACKS */
}

> endif

<T>Vec::<T>Vec(const <t>* dat, unsigned n)
  : RWVecView(n,sizeof(<t>))
{
  rw<bla>copy(n, data(), 1, dat, 1);
}

<T>Vec::<T>Vec(RWBlock *block, unsigned n)
  : RWVecView(block,n)
{
  unsigned minlen = n*sizeof(<t>);
  if (block->length() < minlen) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_SHORTBLOCK, block->length(), minlen) ));
  }
}

<T>Vec&
<T>Vec::operator=(const <T>Vec& rhs)
{
  lengthCheck(rhs.length());
  if (sameDataBlock(rhs)) { return (*this)=rhs.copy(); }  // avoid aliasing problems
  rw<bla>copy(npts, data(), step, rhs.data(), rhs.step);
  return *this;
}

// Assign to a scalar:
<T>Vec&
<T>Vec::operator=(<t> scalar)
{
> if <t>==double || <t>==int || <R/C>==Complex
  rw<bla>set(npts, data(), step, &scalar);
> else
  <t> scalar2 = scalar;	// Temporary necessary to finesse K&R compilers
  rw<bla>set(npts, data(), step, &scalar2);
> endif
  return *this;
}

<T>Vec&
<T>Vec::reference(const <T>Vec& v)
{
  RWVecView::reference(v);
  return *this;
}

// Return copy of self with distinct instance variables
<T>Vec
<T>Vec::copy() const
{
  <T>Vec temp(npts,rwUninitialized);
  rw<bla>copy(npts, temp.data(), 1, data(), step);
  return temp;
}

// Synonym for copy().  Not made inline because some compilers
// have trouble with inlined temporaries with destructors.
<T>Vec
<T>Vec::deepCopy() const
{
  return copy();
}

// Guarantee that references==1 and stride==1
void
<T>Vec::deepenShallowCopy()
{
  if (!isSimpleView()) {
    RWVecView::reference(copy());
  }
}

/*
 * Reset the length of the vector.  The contents of the old
 * vector are saved.  If the vector has grown, the extra storage
 * is zeroed out.  If the vector has shrunk, it is truncated.
 */
void
<T>Vec::resize(unsigned N)
{
  if(N != npts){
    <C> newvec(N,rwUninitialized);

    // Copy over the minimum of the two lengths:
    unsigned nkeep = npts < N ? npts : N;
    rw<bla>copy(nkeep, newvec.data(), 1, data(), stride());
    unsigned oldLength= npts;	// Remember the old length

    RWVecView::reference(newvec);

    // If vector has grown, zero out the extra storage:
    if( N > oldLength ){
> if <R/C>==Complex
      const <t> zero(0,0);
> else
      static const <t> zero = 0;	/* "static" required for Glockenspiel */
> endif
      rw<bla>set((unsigned)(N-oldLength), data()+oldLength, 1, &zero);
    }
  }
}

/*
 * Reset the length of the vector.  The results can be (and
 * probably will be) garbage.
 */
void
<T>Vec::reshape(unsigned N)
{
  if(N != npts) {
    RWVecView::reference(<C>(N,rwUninitialized));
  }
}

<C> <C>::slice(int start, unsigned n, int str) const {
  return (*this)[RWSlice(start,n,str)];
}


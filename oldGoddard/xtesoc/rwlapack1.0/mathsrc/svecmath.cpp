/*
 * Math definitions
 *
 * Generated from template $Id: xvecmath.cpp,v 1.7 1993/09/16 17:16:56 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/scvec.h"
#include "rw/rwabla.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"
STARTWRAP
#include <stdlib.h>	/* Looking for abs(int) */
ENDWRAP

#define REGISTER register

RCSID("$Header: /users/rcs/mathsrc/xvecmath.cpp,v 1.7 1993/09/16 17:16:56 alv Exp $");


SCharVec
abs(const SCharVec& s)
// Absolute value of a SCharVec
{
  register unsigned i = s.length();
  SCharVec temp(i,rwUninitialized);
  register const SChar* sp = s.data();
  register SChar* dp = temp.data();
  register j = s.stride();
  while (i--) { *dp++ = abs(int(*sp));  sp += j; }
  return temp;
}




SCharVec
cumsum(const SCharVec& s)
// Cumulative sum of SCharVec slice.
// Note: V == delta(cumsum(V)) == cumsum(delta(V))
{
  unsigned i = s.length();
  SCharVec temp(i,rwUninitialized);
  register SChar* sp = (SChar*)s.data();
  register SChar* dp = temp.data();
  register SChar c = 0;
  register j = s.stride();
  while (i--) { c += *sp;  *dp++ = c;  sp += j; }
  return temp;
}

SCharVec
delta(const SCharVec& s)
     // Element differences of SCharVec slice.
     // Note: V == delta(cumsum(V)) == cumsum(delta(V))
{
  unsigned i = s.length();
  SCharVec temp(i,rwUninitialized);
  register SChar* sp = (SChar*)s.data();
  register SChar* dp = temp.data();
  register SChar c = 0;
  register j = s.stride();
  while (i--) { *dp++ = *sp - c; c = *sp; sp += j; }
  return temp;
}

SChar
dot(const SCharVec& u,const SCharVec& v)
     // Vector dot product.
{
  unsigned N = v.length();
  u.lengthCheck(N);
  SChar result;
  rwadot(N, u.data(), u.stride(), v.data(), v.stride(), &result);
  return result;
}

int
maxIndex(const SCharVec& s)
// Index of maximum element.
{
  if (s.length() == 0) s.emptyErr("maxIndex");
  int result = rwamax(s.length(), s.data(), s.stride());
  return result;
}

SChar
maxValue(const SCharVec& s)
{
  int inx = maxIndex(s);
  return s(inx);
}

int
minIndex(const SCharVec& s)
     // Index of minimum element.
{
  if (s.length() == 0) s.emptyErr("minIndex");
  int result = rwamin(s.length(), s.data(), s.stride());
  return result;
}

SChar
minValue(const SCharVec& s)
{
  int inx = minIndex(s);
  return s(inx);
}


SChar
prod(const SCharVec& s)
     // Product of SCharVec elements.
{
  register unsigned i = s.length();
  register SChar* sp = (SChar*)s.data();
  REGISTER SChar t = 1;
  register j = s.stride();
  while (i--) { t *= *sp;  sp += j; }
  return t;
}


SCharVec
reverse(const SCharVec& s)
     // Reverse vector elements --- pretty tricky!
{
  return s.slice( s.length()-1, s.length(), -1);
}

SChar
sum(const SCharVec& s)
     // Sum of a SCharVec
{
  register unsigned i = s.length();
  register SChar* sp = (SChar*)s.data();
  register SChar t = 0;

  register j = s.stride();
  while (i--) { t += *sp;  sp += j; }
  return (SChar)t;
}





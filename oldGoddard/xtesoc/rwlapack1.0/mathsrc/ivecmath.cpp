/*
 * Math definitions
 *
 * Generated from template $Id: xvecmath.cpp,v 1.7 1993/09/16 17:16:56 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/ivec.h"
#include "rw/rwibla.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"
STARTWRAP
#include <stdlib.h>	/* Looking for abs(int) */
ENDWRAP

#define REGISTER register

RCSID("$Header: /users/rcs/mathsrc/xvecmath.cpp,v 1.7 1993/09/16 17:16:56 alv Exp $");

IntVec
abs(const IntVec& s)
// Absolute value of a IntVec
{
  register unsigned i = s.length();
  IntVec temp(i,rwUninitialized);
  register int* sp = (int*)s.data();	// Cast away "constness" to avoid problems with abs()
  register Int* dp = temp.data();
  register j = s.stride();
  while (i--) { *dp++ = abs(*sp); sp += j; }
  return temp;
}





IntVec
cumsum(const IntVec& s)
// Cumulative sum of IntVec slice.
// Note: V == delta(cumsum(V)) == cumsum(delta(V))
{
  unsigned i = s.length();
  IntVec temp(i,rwUninitialized);
  register int* sp = (int*)s.data();
  register int* dp = temp.data();
  register int c = 0;
  register j = s.stride();
  while (i--) { c += *sp;  *dp++ = c;  sp += j; }
  return temp;
}

IntVec
delta(const IntVec& s)
     // Element differences of IntVec slice.
     // Note: V == delta(cumsum(V)) == cumsum(delta(V))
{
  unsigned i = s.length();
  IntVec temp(i,rwUninitialized);
  register int* sp = (int*)s.data();
  register int* dp = temp.data();
  register int c = 0;
  register j = s.stride();
  while (i--) { *dp++ = *sp - c; c = *sp; sp += j; }
  return temp;
}

int
dot(const IntVec& u,const IntVec& v)
     // Vector dot product.
{
  unsigned N = v.length();
  u.lengthCheck(N);
  int result;
  rwidot(N, u.data(), u.stride(), v.data(), v.stride(), &result);
  return result;
}

int
maxIndex(const IntVec& s)
// Index of maximum element.
{
  if (s.length() == 0) s.emptyErr("maxIndex");
  int result = rwimax(s.length(), s.data(), s.stride());
  return result;
}

int
maxValue(const IntVec& s)
{
  int inx = maxIndex(s);
  return s(inx);
}

int
minIndex(const IntVec& s)
     // Index of minimum element.
{
  if (s.length() == 0) s.emptyErr("minIndex");
  int result = rwimin(s.length(), s.data(), s.stride());
  return result;
}

int
minValue(const IntVec& s)
{
  int inx = minIndex(s);
  return s(inx);
}


int
prod(const IntVec& s)
     // Product of IntVec elements.
{
  register unsigned i = s.length();
  register int* sp = (int*)s.data();
  REGISTER int t = 1;
  register j = s.stride();
  while (i--) { t *= *sp;  sp += j; }
  return t;
}


IntVec
reverse(const IntVec& s)
     // Reverse vector elements --- pretty tricky!
{
  return s.slice( s.length()-1, s.length(), -1);
}

int
sum(const IntVec& s)
     // Sum of a IntVec
{
  register unsigned i = s.length();
  register int* sp = (int*)s.data();
  register int t = 0;

  register j = s.stride();
  while (i--) { t += *sp;  sp += j; }
  return (int)t;
}





/*
 * DoubleVec special math functions
 *
 * Generated from template $Id: xrmath.cpp,v 1.4 1993/09/17 18:42:21 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

/*
 *  This file contains functions peculiar to real vectors
 *  of precision Double.
 */

#include "rw/dvec.h"
STARTWRAP
#include <math.h>
ENDWRAP

RCSID("$Header: /users/rcs/mathsrc/xrmath.cpp,v 1.4 1993/09/17 18:42:21 alv Exp $");

/*
 * Expand an even real series into its full series.
 */

DoubleVec
expandEven(const DoubleVec& v)
{
  unsigned N = v.length()-1;
  DoubleVec full(2*N,rwUninitialized);

  // Lower half:
  full.slice(0,N+1,1) = v;
  // Upper half:
  full.slice(N+1,N-1,1) = v.slice(N-1,N-1,-1);

  return full;
}

/*
 * Expand an odd series into its full series.
 */

DoubleVec
expandOdd(const DoubleVec& v)
{
  unsigned N = v.length()+1;
  DoubleVec full(2*N,rwUninitialized);

  // Lower half:
  full.slice(1,N-1,1) = v;
  // Upper half:
  full.slice(N+1,N-1,1) = -v.slice(N-2,N-1,-1);

  full((int)0) = 0;
  full(N) = 0;

  return full;
}


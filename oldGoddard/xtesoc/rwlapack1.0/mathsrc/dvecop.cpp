/*
 * Definitions for various arithmetic operations
 *
 * Generated from template $Id: xvecop.cpp,v 1.7 1993/09/01 16:28:02 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

/*
 * If a promotable scalar (such as a float) is passed by value, then
 * it will get promoted to, say, a double.  If the address of this is
 * then taken, we have a pointer to a double.  Unfortunately, if
 * this address is then passed to a routine that expects a pointer
 * to a float, cfront thinks it has done its job and quietly passes
 * the address of this promoted variable.
 *
 * This problem occurs when K&R compilers are used as a backend to
 * cfront.  The workaround is to create a temporary of known type
 * on the stack.  This is the "scalar2" variable found throughout
 * this file.
 */

#include "rw/dvec.h"
#include "rw/rwdbla.h"	

RCSID("$Header: /users/rcs/mathsrc/xvecop.cpp,v 1.7 1993/09/01 16:28:02 alv Exp $");

/************************************************
 *						*
 *		UNARY OPERATORS			*
 *						*
 ************************************************/

// Unary minus on a DoubleVec
DoubleVec
operator-(const DoubleVec& s)
{
  unsigned i = s.length();
  DoubleVec temp(i,rwUninitialized);
  register double* sp = (double*)s.data();
  register double* dp = temp.data();
  register j = s.stride();
  while (i--) { *dp++ = -(*sp);  sp += j; }
  return temp;
}

// Unary prefix increment on a DoubleVec; (i.e. ++a)
DoubleVec&
DoubleVec::operator++()
{
  register i = length();
  register double* sp = data();
  register j = stride();
  while (i--) { ++(*sp); sp += j; }
  return *this;
}

// Unary prefix decrement on a DoubleVec (i.e., --a)
DoubleVec&
DoubleVec::operator--()
{
  register i = length();
  register double* sp = data();
  register j = stride();
  while (i--) { --(*sp); sp += j; }
  return *this;
}

/************************************************
 *						*
 *		BINARY OPERATORS		*
 *		 vector - vector		*
 *						*
 ************************************************/

// Binary add element-by-element
DoubleVec
operator+(const DoubleVec& u, const DoubleVec& v)
{
  unsigned i = v.length();
  u.lengthCheck(i);
  DoubleVec temp(i,rwUninitialized);
  rwd_plvv(i, temp.data(), u.data(), u.stride(), v.data(), v.stride());
  return temp;
}

// Binary subtract element-by-element
DoubleVec
operator-(const DoubleVec& u, const DoubleVec& v)
{
  unsigned i = v.length();
  u.lengthCheck(i);
  DoubleVec temp(i,rwUninitialized);
  rwd_mivv(i, temp.data(), u.data(), u.stride(), v.data(), v.stride());
  return temp;
}

// Binary multiply element-by-element
DoubleVec
operator*(const DoubleVec& u, const DoubleVec& v)
{
  unsigned i = v.length();
  u.lengthCheck(i);
  DoubleVec temp(i,rwUninitialized);
  rwd_muvv(i, temp.data(), u.data(), u.stride(), v.data(), v.stride());
  return temp;
}

// Binary divide element-by-element
DoubleVec
operator/(const DoubleVec& u, const DoubleVec& v)
{
  unsigned i = v.length();
  u.lengthCheck(i);
  DoubleVec temp(i,rwUninitialized);
  rwd_dvvv(i, temp.data(), u.data(), u.stride(), v.data(), v.stride());
  return temp;
}

/************************************************
 *						*
 *		BINARY OPERATORS		*
 *		 vector - scalar		*
 *						*
 ************************************************/

// Add a scalar
DoubleVec
operator+(const DoubleVec& s, double scalar)
{
  unsigned i = s.length();
  DoubleVec temp(i,rwUninitialized);
  rwd_plvs(i, temp.data(), s.data(), s.stride(), &scalar);
  return temp;
}


// Subtract from a scalar
DoubleVec
operator-(double scalar, const DoubleVec& s)
{
  unsigned i = s.length();
  DoubleVec temp(i,rwUninitialized);
  rwd_misv(i, temp.data(), &scalar, s.data(), s.stride());
  return temp;
}

// Multiply by a scalar
DoubleVec
operator*(const DoubleVec& s, double scalar)
{
  unsigned i = s.length();
  DoubleVec temp(i,rwUninitialized);
  rwd_muvs(i, temp.data(), s.data(), s.stride(), &scalar);
  return temp;
}

// Divide into a scalar
DoubleVec
operator/(double scalar, const DoubleVec& s)
{
  unsigned i = s.length();
  DoubleVec temp(i,rwUninitialized);
  rwd_dvsv(i, temp.data(), &scalar, s.data(), s.stride());
  return temp;
}


/*
 * Out-of-line versions for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
DoubleVec	operator*(double s, const DoubleVec& V)	{return V*s;}
DoubleVec	operator+(double s, const DoubleVec& V)	{return V+s;}
DoubleVec	operator-(const DoubleVec& V, double s)  {return V+(-s);}
DoubleVec	operator/(const DoubleVec& V, double s)  {return V*(1/s);}
#endif

/************************************************
 *						*
 *	ARITHMETIC ASSIGNMENT OPERATORS		*
 *	  with other vectors			*
 *						*
 ************************************************/

DoubleVec&
DoubleVec::operator+=(const DoubleVec& u)
{
  if (sameDataBlock(u)) { return operator+=(u.deepCopy()); }  // Avoid aliasing
  unsigned i = u.length();
  lengthCheck(i);
  rwd_aplvv(i, data(), stride(), u.data(), u.stride());
  return *this;
}

DoubleVec&
DoubleVec::operator-=(const DoubleVec& u)
{
  if (sameDataBlock(u)) { return operator-=(u.deepCopy()); }  // Avoid aliasing
  unsigned i = u.length();
  lengthCheck(i);
  rwd_amivv(i, data(), stride(), u.data(), u.stride());
  return *this;
}

DoubleVec&
DoubleVec::operator*=(const DoubleVec& u)
{
  if (sameDataBlock(u)) { return operator*=(u.deepCopy()); }  // Avoid aliasing
  unsigned i = u.length();
  lengthCheck(i);
  rwd_amuvv(i, data(), stride(), u.data(), u.stride());
  return *this;
}

DoubleVec&
DoubleVec::operator/=(const DoubleVec& u)
{
  if (sameDataBlock(u)) { return operator/=(u.deepCopy()); }  // Avoid aliasing
  unsigned i = u.length();
  lengthCheck(i);
  rwd_advvv(i, data(), stride(), u.data(), u.stride());
  return *this;
}

/************************************************
 *						*
 *	ARITHMETIC ASSIGNMENT OPERATORS		*
 *		  with a scalar			*
 *						*
 ************************************************/

DoubleVec&
DoubleVec::operator+=(double scalar)
{
  rwd_aplvs(length(), data(), stride(), &scalar);
  return *this;
}


DoubleVec&
DoubleVec::operator*=(double scalar)
{
  rwd_amuvs(length(), data(), stride(), &scalar);
  return *this;
}


/************************************************
 *						*
 *		LOGICAL OPERATORS		*
 *						*
 ************************************************/

RWBoolean
DoubleVec::operator==(const DoubleVec& u) const
{
  unsigned i = length();
  if(i != u.length()) return FALSE;  // They can't be equal if they don't have the same length.
  return rwdsame(i, data(), stride(), u.data(), u.stride());
}

RWBoolean
DoubleVec::operator!=(const DoubleVec& u) const
{
  return !(*this == u);
}


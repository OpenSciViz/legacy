/*
 * Definitions for various arithmetic operations for IntArray
 *
 * Generated from template $Id: xarrop.cpp,v 1.11 1993/09/20 04:08:08 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

/*
 * If a promotable scalar (such as a float) is passed by value, then
 * it will get promoted to, say, a double.  If the address of this is
 * then taken, we have a pointer to a double.  Unfortunately, if
 * this address is then passed to a routine that expects a pointer
 * to a float, cfront thinks it has done its job and quietly passes
 * the address of this promoted variable.
 *
 * This problem occurs when K&R compilers are used as a backend to
 * cfront.  The workaround is to create a temporary of known type
 * on the stack.  This is the "scalar2" variable found throughout
 * this file.
 */

#include "rw/iarr.h"
#include "rw/rwibla.h"	

RCSID("$Header: /users/rcs/mathsrc/xarrop.cpp,v 1.11 1993/09/20 04:08:08 alv Exp $");

/************************************************
 *                                              *
 *              UNARY OPERATORS                 *
 *                                              *
 ************************************************/

IntArray operator-(const IntArray& s)   // Unary minus
{
  IntArray temp(s.length(),rwUninitialized);
  int* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    int n = l.length;
    int* sp = (int*)s.data()+l.start;
    while(n--) { *dp++ = -(*sp); sp+=l.stride; }
  }
  return temp;
}

// Unary prefix increment on a IntArray; (i.e. ++a)
IntArray& IntArray::operator++()
{
  for(ArrayLooper l(length(),stride()); l; ++l) {
    int n = l.length;
    int* sp = data()+l.start;
    while(n--) { ++(*sp); sp+=l.stride; }
  }
  return *this;
}

// Unary prefix decrement on a IntArray (i.e., --a)
IntArray& IntArray::operator--()
{
  for(ArrayLooper l(length(),stride()); l; ++l) {
    int n = l.length;
    int* sp = data()+l.start;
    while(n--) { --(*sp); sp+=l.stride; }
  }
  return *this;
}

/************************************************
 *                                              *
 *              BINARY OPERATORS                *
 *               vector - vector                *
 *                                              *
 ************************************************/

IntArray operator+(const IntArray& u, const IntArray& v)
{
  u.lengthCheck(v.length());
  IntArray temp(u.length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  int* dp = temp.data();
  for(DoubleArrayLooper l(u.length(),u.stride(),v.stride()); l; ++l) {
    rwi_plvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

IntArray operator-(const IntArray& u, const IntArray& v)
{
  u.lengthCheck(v.length());
  IntArray temp(u.length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  int* dp = temp.data();
  for(DoubleArrayLooper l(u.length(),u.stride(),v.stride()); l; ++l) {
    rwi_mivv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

IntArray operator*(const IntArray& u, const IntArray& v)
{
  u.lengthCheck(v.length());
  IntArray temp(u.length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  int* dp = temp.data();
  for(DoubleArrayLooper l(u.length(),u.stride(),v.stride()); l; ++l) {
    rwi_muvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

IntArray operator/(const IntArray& u, const IntArray& v)
{
  u.lengthCheck(v.length());
  IntArray temp(u.length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  int* dp = temp.data();
  for(DoubleArrayLooper l(u.length(),u.stride(),v.stride()); l; ++l) {
    rwi_dvvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

/************************************************
 *                                              *
 *              BINARY OPERATORS                *
 *               vector - scalar                *
 *                                              *
 ************************************************/

IntArray operator+(const IntArray& s, int scalar)
{
  IntArray temp(s.length(),rwUninitialized);
  int* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    rwi_plvs(l.length, dp, s.data()+l.start, l.stride, &scalar);
    dp += l.length;
  }
  return temp;
}

IntArray operator-(int scalar, const IntArray& s)
{
  IntArray temp(s.length(),rwUninitialized);
  int* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    rwi_misv(l.length, dp, &scalar, s.data()+l.start, l.stride);
    dp += l.length;
  }
  return temp;
}

IntArray operator*(const IntArray& s, int scalar)
{
  IntArray temp(s.length(),rwUninitialized);
  int* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    rwi_muvs(l.length, dp, s.data()+l.start, l.stride, &scalar);
    dp += l.length;
  }
  return temp;
}

IntArray operator/(int scalar, const IntArray& s)
{
  IntArray temp(s.length(),rwUninitialized);
  int* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    rwi_dvsv(l.length, dp, &scalar, s.data()+l.start, l.stride);
    dp += l.length;
  }
  return temp;
}

IntArray operator/(const IntArray& s, int scalar)
{
  IntArray temp(s.length(),rwUninitialized);
  int* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    rwi_dvvs(l.length, dp, s.data()+l.start, l.stride, &scalar);
    dp += l.length;
  }
  return temp;
}

/*
 * Out-of-line versions for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
IntArray     operator*(int s, const IntArray& V)  {return V*s;}
IntArray     operator+(int s, const IntArray& V)  {return V+s;}
IntArray     operator-(const IntArray& V, int s)  {return V+(-s);}
#endif

/************************************************
 *                                              *
 *      ARITHMETIC ASSIGNMENT OPERATORS         *
 *        with other vectors                    *
 *                                              *
 ************************************************/

IntArray& IntArray::operator+=(const IntArray& u)
{
  if (sameDataBlock(u)) { return operator+=(u.deepCopy()); }  // Avoid aliasing
  u.lengthCheck(length());
  for(DoubleArrayLooper l(length(),stride(),u.stride()); l; ++l) {
    rwi_aplvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}


IntArray& IntArray::operator-=(const IntArray& u)
{
  if (sameDataBlock(u)) { return operator-=(u.deepCopy()); }  // Avoid aliasing
  u.lengthCheck(length());
  for(DoubleArrayLooper l(length(),stride(),u.stride()); l; ++l) {
    rwi_amivv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

IntArray& IntArray::operator*=(const IntArray& u)
{
  if (sameDataBlock(u)) { return operator*=(u.deepCopy()); }  // Avoid aliasing
  u.lengthCheck(length());
  for(DoubleArrayLooper l(length(),stride(),u.stride()); l; ++l) {
    rwi_amuvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

IntArray& IntArray::operator/=(const IntArray& u)
{
  if (sameDataBlock(u)) { return operator/=(u.deepCopy()); }  // Avoid aliasing
  u.lengthCheck(length());
  for(DoubleArrayLooper l(length(),stride(),u.stride()); l; ++l) {
    rwi_advvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

/************************************************
 *                                              *
 *      ARITHMETIC ASSIGNMENT OPERATORS         *
 *                with a scalar                 *
 *                                              *
 ************************************************/

IntArray& IntArray::operator+=(int scalar)
{
  for(ArrayLooper l(length(),stride()); l; ++l) {
    rwi_aplvs(l.length, data()+l.start, l.stride, &scalar);
  }
  return *this;
}

IntArray& IntArray::operator*=(int scalar)
{
  for(ArrayLooper l(length(),stride()); l; ++l) {
    rwi_amuvs(l.length, data()+l.start, l.stride, &scalar);
  }
  return *this;
}

IntArray& IntArray::operator/=(int scalar)
{
  for(ArrayLooper l(length(),stride()); l; ++l) {
    rwi_advvs(l.length, data()+l.start, l.stride, &scalar);
  }
  return *this;
}

/************************************************
 *                                              *
 *              LOGICAL OPERATORS               *
 *                                              *
 ************************************************/

RWBoolean IntArray::operator==(const IntArray& u) const
{
  if(length()!=u.length()) return FALSE;  // They can't be equal if they don't have the same length.
  for(DoubleArrayLooper l(length(),stride(),u.stride()); l; ++l) {
    if (!rwisame(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2)) {
      return FALSE;
    }
  }
  return TRUE;
}

RWBoolean IntArray::operator!=(const IntArray& u) const
{
  return !(*this == u);
}

/*
 * DComplexGenMat 2-dimensional FFT definitions
 *
 * $Header: /users/rcs/mathsrc/cfft2d.cpp,v 1.3 1993/09/01 16:27:07 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: cfft2d.cpp,v $
 * Revision 1.3  1993/09/01  16:27:07  alv
 * now uses rwUninitialized form for simple constructors
 *
 * Revision 1.2  1993/01/26  17:52:28  alv
 * removed DOS EOF
 *
 * Revision 1.1  1993/01/22  23:50:57  alv
 * Initial revision
 *
 * 
 *    Rev 1.3   17 Oct 1991 09:15:12   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.2   08 Oct 1991 13:44:16   keffer
 * Math V4.0 beta 2
 * 
 *    Rev 1.1   24 Jul 1991 12:51:32   keffer
 * Added pvcs keywords
 *
 */

#include "rw/cfft2d.h"

RCSID("$Header: /users/rcs/mathsrc/cfft2d.cpp,v 1.3 1993/09/01 16:27:07 alv Exp $");

DComplexFFT2DServer::DComplexFFT2DServer():
  FFTrow(), FFTcol() 
{
  Nrows = 0;
  Ncols = 0;
}


DComplexFFT2DServer::DComplexFFT2DServer(unsigned nrows, unsigned ncols):
  FFTrow(nrows), FFTcol(ncols) 
{
  Nrows = nrows;
  Ncols = ncols;
}


DComplexFFT2DServer::DComplexFFT2DServer(unsigned nrows):
  FFTrow(nrows), FFTcol(nrows) 
{
  Nrows = nrows;
  Ncols = nrows;
}


DComplexFFT2DServer::DComplexFFT2DServer(const DComplexFFT2DServer& s) :
  FFTrow(s.rows()), FFTcol(s.cols()) 
{
  Nrows = s.Nrows;
  Ncols = s.Ncols;
}


void
DComplexFFT2DServer::operator=(const DComplexFFT2DServer& s) 
{
  Nrows  = s.Nrows;
  Ncols  = s.Ncols;
  FFTrow = s.FFTrow;
  FFTcol = s.FFTcol;
}


void
DComplexFFT2DServer::setRows(unsigned newN) 
{
  if ( Nrows != newN ) {
    Nrows = newN;
    FFTrow.setOrder (newN);
  }
}


void
DComplexFFT2DServer::setCols(unsigned newN) 
{
  if ( Ncols != newN ) {
    Ncols = newN;
    FFTcol.setOrder (newN);
  }
}


DComplexGenMat
DComplexFFT2DServer::fourier(const DComplexGenMat& m) 
{
  if ( Nrows != m.rows() ) setRows (m.rows());
  if ( Ncols != m.cols() ) setCols (m.cols());
  register int i;
  DComplexGenMat work(Nrows, Ncols,rwUninitialized);
  for ( i = 0; i < Ncols; i++ ) 
    work.col(i) = FFTcol.fourier (m.col(i));
  for ( i = 0; i < Nrows; i++ ) 
    work.row(i) = FFTrow.fourier (work.row(i));
  return work;
}


DComplexGenMat
DComplexFFT2DServer::ifourier(const DComplexGenMat& m) 
{
  if ( Nrows != m.rows() ) setRows (m.rows());
  if ( Ncols != m.cols() ) setCols (m.cols());
  register int i;
  DComplexGenMat work(Nrows, Ncols,rwUninitialized);
  for ( i = 0; i < Ncols; i++ ) 
    work.col(i) = FFTcol.ifourier (m.col(i));
  for ( i = 0; i < Nrows; i++ ) 
    work.row(i) = FFTrow.ifourier (work.row(i));
  return work;
}

/*
 * Definitions for IntVec
 *
 * Generated from template $Id: xvec.cpp,v 1.9 1993/10/04 21:44:01 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include <rw/strstrea.h>  /* I use istrstream to parse character string ctor */
#include "rw/ivec.h"
#include "rw/rwibla.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"
#include "rw/rand.h"

RCSID("$Header: /users/rcs/mathsrc/xvec.cpp,v 1.9 1993/10/04 21:44:01 alv Exp $");

IntVec::IntVec(const char *string)
 : RWVecView()
{
  istrstream s((char*)string);
  scanFrom(s);
  if (!s.good()) {
    RWTHROW( RWExternalErr( RWMessage(RWMATH_STRCTOR,string) ));
  }
}

IntVec::IntVec(unsigned n, RWRand& r)
 : RWVecView(n,sizeof(Int))
{
  while(n--) {
    (*this)(n) = (int)r();
  }
}

IntVec::IntVec(unsigned n, int scalar)
 : RWVecView(n,sizeof(Int))
{
  rwiset(n, data(), 1, &scalar);
}

IntVec::IntVec(unsigned n, int scalar, int by)
 : RWVecView(n,sizeof(Int))
{
  register int i = n;
  int* dp = data();
  int value = scalar;
  int byvalue = by;
  while(i--) {*dp++ = value; value += byvalue;}
}


IntVec::IntVec(const int* dat, unsigned n)
  : RWVecView(n,sizeof(int))
{
  rwicopy(n, data(), 1, dat, 1);
}

IntVec::IntVec(RWBlock *block, unsigned n)
  : RWVecView(block,n)
{
  unsigned minlen = n*sizeof(int);
  if (block->length() < minlen) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_SHORTBLOCK, block->length(), minlen) ));
  }
}

IntVec&
IntVec::operator=(const IntVec& rhs)
{
  lengthCheck(rhs.length());
  if (sameDataBlock(rhs)) { return (*this)=rhs.copy(); }  // avoid aliasing problems
  rwicopy(npts, data(), step, rhs.data(), rhs.step);
  return *this;
}

// Assign to a scalar:
IntVec&
IntVec::operator=(int scalar)
{
  rwiset(npts, data(), step, &scalar);
  return *this;
}

IntVec&
IntVec::reference(const IntVec& v)
{
  RWVecView::reference(v);
  return *this;
}

// Return copy of self with distinct instance variables
IntVec
IntVec::copy() const
{
  IntVec temp(npts,rwUninitialized);
  rwicopy(npts, temp.data(), 1, data(), step);
  return temp;
}

// Synonym for copy().  Not made inline because some compilers
// have trouble with inlined temporaries with destructors.
IntVec
IntVec::deepCopy() const
{
  return copy();
}

// Guarantee that references==1 and stride==1
void
IntVec::deepenShallowCopy()
{
  if (!isSimpleView()) {
    RWVecView::reference(copy());
  }
}

/*
 * Reset the length of the vector.  The contents of the old
 * vector are saved.  If the vector has grown, the extra storage
 * is zeroed out.  If the vector has shrunk, it is truncated.
 */
void
IntVec::resize(unsigned N)
{
  if(N != npts){
    IntVec newvec(N,rwUninitialized);

    // Copy over the minimum of the two lengths:
    unsigned nkeep = npts < N ? npts : N;
    rwicopy(nkeep, newvec.data(), 1, data(), stride());
    unsigned oldLength= npts;	// Remember the old length

    RWVecView::reference(newvec);

    // If vector has grown, zero out the extra storage:
    if( N > oldLength ){
      static const int zero = 0;	/* "static" required for Glockenspiel */
      rwiset((unsigned)(N-oldLength), data()+oldLength, 1, &zero);
    }
  }
}

/*
 * Reset the length of the vector.  The results can be (and
 * probably will be) garbage.
 */
void
IntVec::reshape(unsigned N)
{
  if(N != npts) {
    RWVecView::reference(IntVec(N,rwUninitialized));
  }
}

IntVec IntVec::slice(int start, unsigned n, int str) const {
  return (*this)[RWSlice(start,n,str)];
}


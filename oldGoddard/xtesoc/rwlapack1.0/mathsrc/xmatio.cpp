> #
> # $Header: /users/rcs/mathsrc/xmatio.cpp,v 1.5 1993/09/17 18:42:21 alv Exp $
> #
> # $Log: xmatio.cpp,v $
> # Revision 1.5  1993/09/17  18:42:21  alv
> # (0) -> ((int)0) for benefit of DEC compiler
> #
> # Revision 1.4  1993/08/10  21:22:18  alv
> # now uses Tools v6 compatible error handling
> #
> # Revision 1.3  1993/03/22  15:51:31  alv
> # changed PVCS Workfile keyword to RCS ID keyword
> #
> # Revision 1.2  1993/01/28  20:52:00  alv
> # simplified an expression for the benefit of cfront
> #
> # Revision 1.1  1993/01/22  23:51:08  alv
> # Initial revision
> #
> # 
> #
> define <ClassType>=GenMat
> include macros
/*
 * Definitions for <C> I/O
 *
 * Generated from template $Id: xmatio.cpp,v 1.5 1993/09/17 18:42:21 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/<a>genmat.h"
#include "rw/rwfile.h"
#include "rw/vstream.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"
STARTWRAP
#include <stdio.h>
#include <ctype.h>
ENDWRAP
#include <iomanip.h>

> if <T>==SChar || <T>==UChar
// read and write chars as integers:
#  define CAST Int
#  define CTYPE char
> else
#  define CAST <T>
#  define CTYPE <T>
> endif

RCSID("$Header: /users/rcs/mathsrc/xmatio.cpp,v 1.5 1993/09/17 18:42:21 alv Exp $");

const unsigned short versionID = 2;		// Formatting version number


void <C>::printOn(ostream& s) const 
{
    int w = s.width((int)0);
    s << rows() << "x" << cols() << " [\n";
    for( int i=0; i<rows(); i++ ) {
      for( int j=0; j<cols(); j++ ) {
        CAST el = (*this)(i,j);
        s << setw(w) << el;
        if (j<(cols()-1)) { s << " "; }
      }
      s << "\n";
    }
    s << "]";
}

void <C>::scanFrom(istream& s)
{
  CAST item;
  char c;

  int m,n;				// Read in array dimensions
  s >> m;
  do { s.get(c); } while (isspace(c));	// Skip optional x character
  if (isdigit(c)) s.putback(c);	
  s >> n;

  do { s>>c; } while(isspace(c)); 	// Eat the leading [, if it exists
  if (c!='[') { s.putback(c); }
 
  reshape(m,n);
  for(int i=0; s.good() && i<rows(); i++) {
    for(int j=0; s.good() && j<cols(); j++) {
      s >> item;
      (*this)(i,j) = item;
    }
  }
  
  if (s.good()) {
    do { s.get(c); } while(s.good() && isspace(c));  // Eat the trailing ]
  }
  if (c!=']') { s.putback(c); }

  return;
}

ostream& operator<<(ostream& s, const <C>& m)
{
  m.printOn(s); return s;
}

istream& operator>>(istream& s, <C>& m)
{
  m.scanFrom(s); return s;
}

void <C>::saveOn(RWFile& file) const 
{
  file.Write(versionID);
  file.Write(nrows);
  file.Write(ncols);

> if <R/C>==Complex
#  if COMPLEX_PACKS && !defined(IMAG_LEADS)

  if( rowStride()==1 && colStride()==rows() ){
    // This is the normal case and things can be done very efficiently.
    // Write the N point complex vector as a 2N point real vector:
    const <P>* dataStart = (const <P>*) data();
    file.Write(dataStart, 2*rows()*cols());
  }
  else {
    for(int j=0; j<cols(); j++) {
      for(int i=0; i<rows(); i++) {
        // Do things one step at a time for the benefit of some cfronts
        DComplex c = (*this)(i,j);
        file.Write(real(c));
        file.Write(imag(c));
      }
    }
  }

#  else

  /*
   * Complex vector, but it is not packed.
   * Have to write it out, element-by-element
   */

  for(int j=0; j<cols(); j++) {
    for(int i=0; i<rows(); i++) {
      file.Write( real( (*this)(i,j) ) );
      file.Write( imag( (*this)(i,j) ) );
    }
  }

#  endif

> else
> # Not a complex vector

  if( rowStride()==1 && colStride()==rows() ){
    const CTYPE* dataStart = (const CTYPE*)data();
    file.Write(dataStart, rows()*cols());
  }
  else {
    for(int j=0; j<cols(); j++) {
      for(int i=0; i<rows(); i++) {
        CTYPE item = (*this)(i,j);
        file.Write(item);
      }
    }
  }

> endif
> # end check of complex
}

void <C>::saveOn(RWvostream& s) const 
{
  s << versionID;		// Save the version number
  s << nrows << ncols; 		// Save the vector length

> if <R/C>==Complex
#  if COMPLEX_PACKS && !defined(IMAG_LEADS)

  if( rowStride()==1 && colStride()==nrows ){
    // This is the normal case and things can be done very efficiently.
    // Write the N point complex vector as a 2N point real vector:
    const <P>* dataStart = (const <P>*) data();
    s.put(dataStart, 2*rows()*cols());
  }
  else {
    for(int j=0; j<cols(); j++) {
      for(int i=0; i<rows(); i++) {
        DComplex c = (*this)(i,j);
        s << real(c) << imag(c);
      }
    }
  }

#  else	

  /*
   * Complex vector, but either it is not packed, or real does not lead.
   * Have to write it out, element-by-element
   */

  for(int j=0; j<cols(); j++) {
    for(int i=0; i<rows(); i++) {
      s << real( (*this)(i,j) );
      s << imag( (*this)(i,j) );
    }
  }

#  endif

> else
> # Not complex

  if( rowStride()==1 && colStride()==rows() ){
    const CTYPE* dataStart = (const CTYPE*)data();
    s.put(dataStart, rows()*cols());
  }
  else {
    for(int j=0; j<cols(); j++) {
      for(int i=0; i<rows(); i++) {
        CTYPE item = (*this)(i,j);
        s << item;
      }
    }
  }
> endif
}

void <C>::restoreFrom(RWFile& file)
{
  // Get and check the version number
  unsigned short ID;
  file.Read(ID);

  if( ID!=versionID && ID!=340 ){
    versionErr((int)versionID, (int)ID);
  }

  // Get the length and resize to it:
  unsigned m,n;
  file.Read(m);
  file.Read(n);
  reshape(m,n);

> if <R/C>==Complex
> # Complex vector
#  if COMPLEX_PACKS

  // This is the normal case.  We can read the N point complex
  // vector as a 2N real vector.
  <P>* dataStart = (<P>*) data();
  file.Read(dataStart, 2*rows()*cols());

#  else

  // Complex doesn't pack.  Have to read it in element-by-element
  <P> tmpReal, tmpImag;
  for(int j=0; j<ncols(); j++) {
    for(int i=0; i<nrows(); i++) {
      file.Read(tmpReal);
      file.Read(tmpImag);
      (*this)(i,j) = <T>(tmpReal,tmpImag);
    }
  }

#  endif

> else
> # Not complex

  // Scoop up the whole vector in one read:
  CTYPE* dataStart = (CTYPE*) data();
  file.Read(dataStart, rows()*cols());
> endif
}

void <C>::restoreFrom(RWvistream& s)
{
  // Get and check the version number
  unsigned short ID;
  s >> ID;

  if( ID!=versionID && ID!=340 ){
    versionErr((int)versionID, (int)ID);
  }

  // Get the length and resize to it:
  unsigned m,n;
  s >> m >> n;
  reshape(m,n);

> if <R/C>==Complex
#  if COMPLEX_PACKS

  // This is the normal case.  We can read the N point complex
  // vector as a 2N real vector.
  <P>* dataStart = (<P>*) data();
  s.get(dataStart, 2*rows()*cols());

#else

  // Complex doesn't pack.  Have to read it in element-by-element
  <P> tmpReal, tmpImag;
  for(int j=0; j<ncols(); j++) {
    for(int i=0; i<nrows(); i++) {
      s >> tmpReal;
      s >> tmpImag;
      (*this)(i,j) = <T>(tmpReal,tmpImag);
    }
  }

#  endif

> else
> # Not complex

  // Scoop up the whole vector in one read:
  CTYPE* dataStart = (CTYPE*) data();
  s.get(dataStart, rows()*cols());
> endif
}

unsigned <C>::binaryStoreSize() const
{
  // Total storage requirements = Number of elements times their size, 
  // plus space for the vector length and ID number:
  return rows()*cols()*sizeof(<T>) + 2*sizeof(unsigned) + sizeof(versionID);
}

/*
 * Definitions for various arithmetic operations for FloatArray
 *
 * Generated from template $Id: xarrop.cpp,v 1.11 1993/09/20 04:08:08 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/farr.h"
#include "rw/rwsbla.h"	

RCSID("$Header: /users/rcs/mathsrc/xarrop.cpp,v 1.11 1993/09/20 04:08:08 alv Exp $");

/************************************************
 *                                              *
 *              UNARY OPERATORS                 *
 *                                              *
 ************************************************/

FloatArray operator-(const FloatArray& s)   // Unary minus
{
  FloatArray temp(s.length(),rwUninitialized);
  float* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    int n = l.length;
    float* sp = (float*)s.data()+l.start;
    while(n--) { *dp++ = -(*sp); sp+=l.stride; }
  }
  return temp;
}

// Unary prefix increment on a FloatArray; (i.e. ++a)
FloatArray& FloatArray::operator++()
{
  for(ArrayLooper l(length(),stride()); l; ++l) {
    int n = l.length;
    float* sp = data()+l.start;
    while(n--) { ++(*sp); sp+=l.stride; }
  }
  return *this;
}

// Unary prefix decrement on a FloatArray (i.e., --a)
FloatArray& FloatArray::operator--()
{
  for(ArrayLooper l(length(),stride()); l; ++l) {
    int n = l.length;
    float* sp = data()+l.start;
    while(n--) { --(*sp); sp+=l.stride; }
  }
  return *this;
}

/************************************************
 *                                              *
 *              BINARY OPERATORS                *
 *               vector - vector                *
 *                                              *
 ************************************************/

FloatArray operator+(const FloatArray& u, const FloatArray& v)
{
  u.lengthCheck(v.length());
  FloatArray temp(u.length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  float* dp = temp.data();
  for(DoubleArrayLooper l(u.length(),u.stride(),v.stride()); l; ++l) {
    rws_plvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

FloatArray operator-(const FloatArray& u, const FloatArray& v)
{
  u.lengthCheck(v.length());
  FloatArray temp(u.length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  float* dp = temp.data();
  for(DoubleArrayLooper l(u.length(),u.stride(),v.stride()); l; ++l) {
    rws_mivv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

FloatArray operator*(const FloatArray& u, const FloatArray& v)
{
  u.lengthCheck(v.length());
  FloatArray temp(u.length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  float* dp = temp.data();
  for(DoubleArrayLooper l(u.length(),u.stride(),v.stride()); l; ++l) {
    rws_muvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

FloatArray operator/(const FloatArray& u, const FloatArray& v)
{
  u.lengthCheck(v.length());
  FloatArray temp(u.length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  float* dp = temp.data();
  for(DoubleArrayLooper l(u.length(),u.stride(),v.stride()); l; ++l) {
    rws_dvvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

/************************************************
 *                                              *
 *              BINARY OPERATORS                *
 *               vector - scalar                *
 *                                              *
 ************************************************/

FloatArray operator+(const FloatArray& s, float scalar)
{
  FloatArray temp(s.length(),rwUninitialized);
  float* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    float scalar2 = scalar;
    rws_plvs(l.length, dp, s.data()+l.start, l.stride, &scalar2);
    dp += l.length;
  }
  return temp;
}

FloatArray operator-(float scalar, const FloatArray& s)
{
  FloatArray temp(s.length(),rwUninitialized);
  float* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    float scalar2 = scalar;
    rws_misv(l.length, dp, &scalar2, s.data()+l.start, l.stride);
    dp += l.length;
  }
  return temp;
}

FloatArray operator*(const FloatArray& s, float scalar)
{
  FloatArray temp(s.length(),rwUninitialized);
  float* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    float scalar2 = scalar;
    rws_muvs(l.length, dp, s.data()+l.start, l.stride, &scalar2);
    dp += l.length;
  }
  return temp;
}

FloatArray operator/(float scalar, const FloatArray& s)
{
  FloatArray temp(s.length(),rwUninitialized);
  float* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    float scalar2 = scalar;
    rws_dvsv(l.length, dp, &scalar2, s.data()+l.start, l.stride);
    dp += l.length;
  }
  return temp;
}


/*
 * Out-of-line versions for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
FloatArray     operator*(float s, const FloatArray& V)  {return V*s;}
FloatArray     operator+(float s, const FloatArray& V)  {return V+s;}
FloatArray     operator-(const FloatArray& V, float s)  {return V+(-s);}
FloatArray     operator/(const FloatArray& V, float s)  {return V*(1/s);}
#endif

/************************************************
 *                                              *
 *      ARITHMETIC ASSIGNMENT OPERATORS         *
 *        with other vectors                    *
 *                                              *
 ************************************************/

FloatArray& FloatArray::operator+=(const FloatArray& u)
{
  if (sameDataBlock(u)) { return operator+=(u.deepCopy()); }  // Avoid aliasing
  u.lengthCheck(length());
  for(DoubleArrayLooper l(length(),stride(),u.stride()); l; ++l) {
    rws_aplvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}


FloatArray& FloatArray::operator-=(const FloatArray& u)
{
  if (sameDataBlock(u)) { return operator-=(u.deepCopy()); }  // Avoid aliasing
  u.lengthCheck(length());
  for(DoubleArrayLooper l(length(),stride(),u.stride()); l; ++l) {
    rws_amivv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

FloatArray& FloatArray::operator*=(const FloatArray& u)
{
  if (sameDataBlock(u)) { return operator*=(u.deepCopy()); }  // Avoid aliasing
  u.lengthCheck(length());
  for(DoubleArrayLooper l(length(),stride(),u.stride()); l; ++l) {
    rws_amuvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

FloatArray& FloatArray::operator/=(const FloatArray& u)
{
  if (sameDataBlock(u)) { return operator/=(u.deepCopy()); }  // Avoid aliasing
  u.lengthCheck(length());
  for(DoubleArrayLooper l(length(),stride(),u.stride()); l; ++l) {
    rws_advvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

/************************************************
 *                                              *
 *      ARITHMETIC ASSIGNMENT OPERATORS         *
 *                with a scalar                 *
 *                                              *
 ************************************************/

FloatArray& FloatArray::operator+=(float scalar)
{
  for(ArrayLooper l(length(),stride()); l; ++l) {
    float scalar2 = scalar;
    rws_aplvs(l.length, data()+l.start, l.stride, &scalar2);
  }
  return *this;
}

FloatArray& FloatArray::operator*=(float scalar)
{
  for(ArrayLooper l(length(),stride()); l; ++l) {
    float scalar2 = scalar;
    rws_amuvs(l.length, data()+l.start, l.stride, &scalar2);
  }
  return *this;
}


/************************************************
 *                                              *
 *              LOGICAL OPERATORS               *
 *                                              *
 ************************************************/

RWBoolean FloatArray::operator==(const FloatArray& u) const
{
  if(length()!=u.length()) return FALSE;  // They can't be equal if they don't have the same length.
  for(DoubleArrayLooper l(length(),stride(),u.stride()); l; ++l) {
    if (!rwssame(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2)) {
      return FALSE;
    }
  }
  return TRUE;
}

RWBoolean FloatArray::operator!=(const FloatArray& u) const
{
  return !(*this == u);
}

/*
 * Type specific routines for FloatGenMat
 *
 * $Header: /users/rcs/mathsrc/fmatx.cpp,v 1.2 1993/09/01 16:27:18 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: fmatx.cpp,v $
 * Revision 1.2  1993/09/01  16:27:18  alv
 * now uses rwUninitialized form for simple constructors
 *
 * Revision 1.1  1993/01/22  23:50:59  alv
 * Initial revision
 *
 * 
 *    Rev 1.3   17 Oct 1991 09:15:18   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.2   08 Oct 1991 13:44:36   keffer
 * Math V4.0 beta 2
 * 
 *    Rev 1.1   24 Jul 1991 12:51:38   keffer
 * Added pvcs keywords
 *
 */

#include "rw/fgenmat.h"
#include "rw/dgenmat.h"

RCSID("$Header: /users/rcs/mathsrc/fmatx.cpp,v 1.2 1993/09/01 16:27:18 alv Exp $");

// Convert to DoubleGenMat.  
// Should be a friend, but.... 
FloatGenMat::operator DoubleGenMat() const
{
  DoubleGenMat temp(rows(),cols(),rwUninitialized);
  double *dp = temp.data();
  for(MatrixLooper l(rows(),cols(),rowStride(),colStride()); l; ++l) {
    int n = l.length;
    const float *tp = data()+l.start;
    while(n--) {
      *dp++ = double(*tp); tp+=l.stride;
    }
  }
  return temp;
}

// narrow from DoubleGenMat to FloatGenMat
FloatGenMat
toFloat(const DoubleGenMat& v)
{
  FloatGenMat temp(v.rows(),v.cols(),rwUninitialized);
  float *dp = temp.data();
  for(MatrixLooper l(v.rows(),v.cols(),v.rowStride(),v.colStride()); l; ++l) {
    int n = l.length;
    const double *tp = v.data()+l.start;
    while(n--) {
      *dp++ = float(*tp); tp+=l.stride;
    }
  }
  return temp;
}

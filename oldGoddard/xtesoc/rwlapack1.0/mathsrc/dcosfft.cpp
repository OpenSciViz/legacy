/*
 * Double Precision Fast Cosine server Definition
 *  (NOTE: sine and cosine transforms are both included)
 *
 * $Header: /users/rcs/mathsrc/dcosfft.cpp,v 1.4 1993/09/01 16:27:11 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: dcosfft.cpp,v $
 * Revision 1.4  1993/09/01  16:27:11  alv
 * now uses rwUninitialized form for simple constructors
 *
 * Revision 1.3  1993/08/10  21:22:18  alv
 * now uses Tools v6 compatible error handling
 *
 * Revision 1.2  1993/01/26  17:52:28  alv
 * removed DOS EOF
 *
 * Revision 1.1  1993/01/22  23:50:57  alv
 * Initial revision
 *
 * 
 *    Rev 1.5   15 Nov 1991 09:36:24   keffer
 * Removed RWMATHERR macro
 * 
 *    Rev 1.4   17 Oct 1991 09:15:12   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.2   24 Sep 1991 18:54:12   keffer
 * Ported to Zortech V3.0
 * 
 */
#include "rw/dcosfft.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"
STARTWRAP
#include <stdio.h>
#include <math.h>
ENDWRAP

RCSID("$Header: /users/rcs/mathsrc/dcosfft.cpp,v 1.4 1993/09/01 16:27:11 alv Exp $");

DoubleCosineServer::DoubleCosineServer()
{
  server_N=0;
}

DoubleCosineServer::DoubleCosineServer(unsigned N)
{
  setOrder(N);
}

DoubleCosineServer::DoubleCosineServer(const DoubleCosineServer& s)
     : DoubleFFTServer(s), sins(s.sins)
{
  server_N = s.server_N;
}

void
DoubleCosineServer::operator=(const DoubleCosineServer& s)
{
  DoubleFFTServer::operator=(s);
  server_N = s.server_N;
  sins.reference(s.sins);
}

void
DoubleCosineServer::setOrder(unsigned N)
{
  server_N = N;
  DoubleFFTServer::setOrder(N/2+1);
  sins.reference( 2.0*sin( DoubleVec( N-1, M_PI/N, M_PI/N) ) );
}

/* 
 * The fourier transform of a real even sequence is a cosine transform.
 * The result is also a real even sequence.  This is prodedure #6 from
 * Cooley, et al. (1970) J. Sound Vib. (12) 315--337.
 */

DoubleVec
DoubleCosineServer::cosine(const DoubleVec& v)
{
  unsigned Nstore	= v.length();
  unsigned N		= Nstore-1;
  unsigned Nm1		= N-1;
  unsigned Nhalf	= N/2;
  unsigned Nhalfp1	= Nhalf+1;
  unsigned Nhalfm1	= Nhalf-1;

  // Various things that could go wrong:
  checkOdd(Nstore);
  if( N != server_N )setOrder(N);

  /* The results will appear here, and will consist of length() points.*/
  DoubleVec results(Nstore,rwUninitialized);

  /* X will be a complex conjugate even vector */
  DComplexVec X(Nhalfp1,rwUninitialized);

  X.slice(1,Nhalfm1,1) =
    DComplexVec(v.slice(2,Nhalfm1,2),	// Real part
		v.slice(1,Nhalfm1,2) - v.slice(3,Nhalfm1,2)); // Imag part

  X(0) = DComplex(v(0), 0.);
  X(Nhalf) = DComplex(v(N), 0.);

  /* Take the IDFT. The transform of a complex conjugate even vector
    N/2+1 points long is a real vector, N points long. */
  DoubleVec A = DoubleFFTServer::ifourier(X);
  
  DoubleVec temp1 = A.slice(1,Nm1,1);
  DoubleVec temp2 = A.slice(Nm1,Nm1,-1);

  results.slice(1,Nm1,1) = 0.5*( (temp1 + temp2) - (temp1 - temp2)/sins);

  /* Special procedure required for the first and last points. */
  // Sum all of the odd points.  Count all but v(N) twice.
  double A2 = 2.0*sum(v.slice(1,Nhalf,2));

  if( N%2) A2 += v(N);		// If N is odd, include v(N)

  results(0) = A(0) + A2;
  results(N) = A(0) - A2;
  
  return results;
}                                         

/* 
 * The fourier transform of a real odd sequence is a sine transform.
 * The result is also a real odd sequence.  This is prodedure #7 from
 * Cooley, et al. (1970) J. Sound Vib. (12) 315--337.
 */

DoubleVec
DoubleCosineServer::sine(const DoubleVec& v)
{
  unsigned Nstore	= v.length();
  unsigned N		= Nstore+1;
  unsigned Nm1		= N-1;
  unsigned Nhalf	= N/2;
  unsigned Nhalfp1	= Nhalf+1;
  unsigned Nhalfm1	= Nhalf-1;

  // Various things that could go wrong:
  checkOdd(Nstore);
  if(N != server_N)setOrder(N);

  /* X will be a complex conjugate even vector */
  DComplexVec X(Nhalfp1,rwUninitialized);

  X.slice(1,Nhalfm1,1) =
    DComplexVec(v.slice(0,Nhalfm1,2) - v.slice(2,Nhalfm1,2),	// real part
		-v.slice(1,Nhalfm1,2));				// imag part

  X(0)     = DComplex(-2.0*v(0),0);
  X(Nhalf) = DComplex( 2.0*v(N-2),0);

  /* Take the IDFT. The transform of a complex conjugate even vector
    N/2+1 points long is a real vector, N points long. */
  DoubleVec A = DoubleFFTServer::ifourier(X);
  
  DoubleVec temp1 = A.slice(1,Nm1,1);
  DoubleVec temp2 = A.slice(Nm1,Nm1,-1);

  DoubleVec results = 0.5*( (temp1 - temp2) - (temp1 + temp2)/sins);

  return results;
}


void
DoubleCosineServer::checkOdd(int l)  const
{
  if( !(l%2) ) {
    RWTHROW( RWInternalErr( RWMessage(RWMATH_BADPARITY, "DoubleCosineServer", (unsigned)l, "odd") ));
  }
}

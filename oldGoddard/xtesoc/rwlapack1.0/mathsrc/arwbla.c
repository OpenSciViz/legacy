/*
 * BLA-like extensions for type SChar.
 *
 * Generated from template $Id: xrwbla.c,v 1.3 1993/10/11 18:42:24 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/rwabla.h"
#include "rw/mathdefs.h"

#include <math.h>
#ifdef BSD
#  include <memory.h>
#else
#  include <string.h>
#endif

/****************************************************************
 *								*
 *			UTILITY					*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwacopy(n, x, incx, y, incy )
  unsigned n;
  SChar *x, *y;
  int incx, incy;
#else
void FDecl rwacopy( unsigned n, SChar* restrict x, int incx, const SChar* restrict y, int incy)
#endif
{
  if (n==0) return;

  if (incx == 1 && incy == 1) {
    memcpy(x, y, n*sizeof(SChar));
  }
  else {
    /*
     * Code for increments not equal to one
     */
    while (n--) {
      *x = *y;
      x += incx;
      y += incy;
    }
  }
}

#ifdef RW_KR_ONLY
void rwaset(n, x, incx, scalar)
  unsigned n;
  SChar *x, *scalar;
  int incx;
#else
void FDecl rwaset(unsigned n, SChar* restrict x, int incx, const SChar* restrict scalar)
#endif
{
  if(incx==1)
    while (n--) { *x++ = *scalar; }
  else
    while (n--) { *x = *scalar; x+=incx; }
}

#ifdef RW_KR_ONLY
int rwasame(n, x, incx, y, incy)
  unsigned n;
  SChar *x, *y;
  int incx, incy;
#else
int FDecl rwasame(unsigned n, const SChar* restrict x, int incx, const SChar* restrict y, int incy)
#endif
{
  while(n--){
    if( *x != *y ) return 0;
    x += incx;
    y += incy;
  }
  return 1;
}

#ifdef RW_KR_ONLY
int rwamax(n, x, incx)
  unsigned n;
  SChar *x;
  int incx;
#else
int FDecl rwamax(unsigned n, const SChar* restrict x, int incx)
#endif
{
  SChar maxv;
  int maxi, i;

  if(n==0) return -1;
  maxv = *x;
  maxi = 0;

  for(i=1; i<n; i++){
    x += incx;
    if(*x > maxv) { maxv = *x; maxi=i; }
  }
  return maxi;
}

#ifdef RW_KR_ONLY
int rwamin(n, x, incx)
  unsigned n;
  SChar *x;
  int incx;
#else
int FDecl rwamin(unsigned n, const SChar* restrict x, int incx)
#endif
{
  SChar minv;
  int mini, i;

  if(n==0) return -1;
  minv = *x;
  mini = 0;

  for(i=1; i<n; i++){
    x += incx;
    if(*x < minv) { minv = *x; mini=i; }
  }
  return mini;
}

/****************************************************************
 *								*
 *			DOT PRODUCTS				*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwadot(n, x, incx, y, incy, result)
  unsigned n;
  SChar *x, *y, *result;
  int incx, incy;
#else
void FDecl rwadot(unsigned n, const SChar* restrict x, int incx, const SChar* restrict y, int incy, SChar* result)
#endif
{

  SChar sum = 0;
  while(n--){
    sum += *x * *y;
    x += incx;
    y += incy;
  }
  *result = (SChar)sum;
}


/****************************************************************
 *								*
 *		VECTOR --- VECTOR ASSIGNMENT OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwa_aplvv(n, x, incx, y, incy)
  unsigned n;
  SChar *x, *y;
  int incx, incy;
#else
void FDecl rwa_aplvv(unsigned n, SChar* restrict x, int incx, const SChar* restrict y, int incy)
#endif
{
  while(n--){
    *x += *y;
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rwa_amivv(n, x, incx, y, incy)
  unsigned n;
  SChar *x, *y;
  int incx, incy;
#else
void FDecl rwa_amivv(unsigned n, SChar* restrict x, int incx, const SChar* restrict y, int incy)
#endif
{
  while(n--){
    *x -= *y;
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rwa_amuvv(n, x, incx, y, incy)
  unsigned n;
  SChar *x, *y;
  int incx, incy;
#else
void FDecl rwa_amuvv(unsigned n, SChar* restrict x, int incx, const SChar* restrict y, int incy)
#endif
{
  while(n--){
    *x *= *y;
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rwa_advvv(n, x, incx, y, incy)
  unsigned n;
  SChar *x, *y;
  int incx, incy;
#else
void FDecl rwa_advvv(unsigned n, SChar* restrict x, int incx, const SChar* restrict y, int incy)
#endif
{
  while(n--){
    *x /= *y;
    x += incx;
    y += incy;
  }
}

/****************************************************************
 *								*
 *		VECTOR --- SCALAR ASSIGNMENT OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwa_aplvs(n, x, incx, scalar)
  unsigned n;
  SChar *x, *scalar;
  int incx;
#else
void FDecl rwa_aplvs(unsigned n, SChar* restrict x, int incx, const SChar* restrict scalar)
#endif
{
  while(n--) {
    *x += *scalar;
    x += incx;
  }
}

#ifdef RW_KR_ONLY
void rwa_amuvs(n, x, incx, scalar)
  unsigned n;
  SChar *x, *scalar;
  int incx;
#else
void FDecl rwa_amuvs(unsigned n, SChar* restrict x, int incx, const SChar* restrict scalar)
#endif
{
  while(n--) {
    *x *= *scalar;
    x += incx;
  }
}

#ifdef RW_KR_ONLY
void rwa_advvs(n, x, incx, scalar)
  unsigned n;
  SChar *x, *scalar;
  int incx;
#else
void FDecl rwa_advvs(unsigned n, SChar* restrict x, int incx, const SChar* restrict scalar)
#endif
{
  while(n--) {
    *x /= *scalar;
    x += incx;
  }
}

/****************************************************************
 *								*
 *		VECTOR --- VECTOR BINARY OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwa_plvv(n, z, x, incx, y, incy)
  unsigned n;
  SChar *z, *x, *y;
  int incx, incy;
#else
void FDecl rwa_plvv(unsigned n, SChar* restrict z, const SChar* restrict x, int incx, const SChar* restrict y, int incy)
#endif
{
  while(n--){
    *z++ = *x + *y;
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rwa_mivv(n, z, x, incx, y, incy)
  unsigned n;
  SChar *z, *x, *y;
  int incx, incy;
#else
void FDecl rwa_mivv(unsigned n, SChar* restrict z, const SChar* restrict x, int incx, const SChar* restrict y, int incy)
#endif
{
  while(n--){
    *z++ = *x - *y;
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rwa_muvv(n, z, x, incx, y, incy)
  unsigned n;
  SChar *z, *x, *y;
  int incx, incy;
#else
void FDecl rwa_muvv(unsigned n, SChar* restrict z, const SChar* restrict x, int incx, const SChar* restrict y, int incy)
#endif
{
  while(n--){
    *z++ = *x * *y;
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rwa_dvvv(n, z, x, incx, y, incy)
  unsigned n;
  SChar *z, *x, *y;
  int incx, incy;
#else
void FDecl rwa_dvvv(unsigned n, SChar* restrict z, const SChar* restrict x, int incx, const SChar* restrict y, int incy)
#endif
{
  while(n--){
    *z++ = *x / *y;
    x += incx;
    y += incy;
  }
}

/****************************************************************
 *								*
 *		VECTOR --- SCALAR BINARY OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwa_plvs(n, z, x, incx, scalar)
  unsigned n;
  SChar *z, *x, *scalar;
  int incx;
#else
void FDecl rwa_plvs(unsigned n, SChar* restrict z, const SChar* restrict x, int incx, const SChar* restrict scalar)
#endif
{
  while(n--){
    *z++ = *x + *scalar;
    x += incx;
  }
}

#ifdef RW_KR_ONLY
void rwa_muvs(n, z, x, incx, scalar)
  unsigned n;
  SChar *z, *x, *scalar;
  int incx;
#else
void FDecl rwa_muvs(unsigned n, SChar* restrict z, const SChar* restrict x, int incx, const SChar* restrict scalar)
#endif
{
  while(n--){
    *z++ = *x * *scalar;
    x += incx;
  }
}

#ifdef RW_KR_ONLY
void rwa_dvvs(n, z, x, incx, scalar)
  unsigned n;
  SChar *z, *x, *scalar;
  int incx;
#else
void FDecl rwa_dvvs(unsigned n, SChar* restrict z, const SChar* restrict x, int incx, const SChar* restrict scalar)
#endif
{
  while(n--){
    *z++ = *x / *scalar;
    x += incx;
  }
}

/****************************************************************
 *								*
 *		SCALAR --- VECTOR BINARY OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwa_misv(n, z, scalar, x, incx)
  unsigned n;
  SChar *z, *scalar, *x;
  int incx;
#else
void FDecl rwa_misv(unsigned n, SChar* restrict z, const SChar* scalar, const SChar* restrict x, int incx)
#endif
{
  while(n--){
    *z++ = *scalar - *x;
    x += incx;
  }
}

#ifdef RW_KR_ONLY
void rwa_dvsv(n, z, scalar, x, incx)
  unsigned n;
  SChar *z, *scalar, *x;
  int incx;
#else
void FDecl rwa_dvsv(unsigned n, SChar* restrict z, const SChar* scalar, const SChar* restrict x, int incx)
#endif
{
  while(n--){
    *z++ = *scalar / *x;
    x += incx;
  }
}


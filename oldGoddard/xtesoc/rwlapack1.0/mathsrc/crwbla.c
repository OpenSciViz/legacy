/*
 * BLA-like extensions for type FComplex.
 *
 * Generated from template $Id: xrwbla.c,v 1.3 1993/10/11 18:42:24 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/rwcbla.h"
#include "rw/mathdefs.h"

#include <math.h>
#ifdef BSD
#  include <memory.h>
#else
#  include <string.h>
#endif

/*
 * Divide the DComplex pointed to by a1 by the DComplex pointed to
 * by a2, put the results in result.  There are no restrictions
 * on a1, a2, and result pointing to different data storage.
 */
#ifdef RW_KR_ONLY
static void c_div(result, a1, a2)
  FComplex *result, *a1, *a2;
#else
static void c_div(FComplex *result, const FComplex *a1, const FComplex *a2)
#endif
{
  float p, q, r, i;

  r = a2->r;
  i = a2->i;
  p = r < 0 ? -r : r;
  q = i < 0 ? -i : i;

  if (p <= q) {
    q = r/i;
    p = (q*q + 1) * i;
    r = a1->r;
    i = a1->i;
  }
  else {
    q = -i/r;
    p = (q*q + 1) * r;
    r = -a1->i;
    i = a1->r;
  }
  result->r = (r*q + i)/p;
  result->i = (i*q - r)/p;
}

/****************************************************************
 *								*
 *			UTILITY					*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwccopy(n, x, incx, y, incy )
  unsigned n;
  FComplex *x, *y;
  int incx, incy;
#else
void FDecl rwccopy( unsigned n, FComplex* restrict x, int incx, const FComplex* restrict y, int incy)
#endif
{
  if (n==0) return;

  if (incx == 1 && incy == 1) {
    memcpy(x, y, n*sizeof(FComplex));
  }
  else {
    /*
     * Code for increments not equal to one
     */
    while (n--) {
#ifdef HAS_NATIVE_COMPLEX
      *x = *y;
#else
      x->r = y->r;  x->i = y->i;
#endif
      x += incx;
      y += incy;
    }
  }
}

#ifdef RW_KR_ONLY
void rwcset(n, x, incx, scalar)
  unsigned n;
  FComplex *x, *scalar;
  int incx;
#else
void FDecl rwcset(unsigned n, FComplex* restrict x, int incx, const FComplex* restrict scalar)
#endif
{
#ifdef HAS_NATIVE_COMPLEX
  if(incx==1)
    while (n--) { *x++ = *scalar; }
  else
    while (n--) { *x = *scalar; x+=incx; }
#else
  if(incx==1)
    while (n--) { x->r = scalar->r; x->i = scalar->i; x++; }
  else
    while (n--) { x->r = scalar->r; x->i = scalar->i; x+=incx; }
#endif
}

#ifdef RW_KR_ONLY
int rwcsame(n, x, incx, y, incy)
  unsigned n;
  FComplex *x, *y;
  int incx, incy;
#else
int FDecl rwcsame(unsigned n, const FComplex* restrict x, int incx, const FComplex* restrict y, int incy)
#endif
{
  while(n--){
#ifdef HAS_NATIVE_COMPLEX
    if( *x != *y ) return 0;
#else
    if( x->r != y->r || x->i != y->i ) return 0;
#endif
    x += incx;
    y += incy;
  }
  return 1;
}


/****************************************************************
 *								*
 *			DOT PRODUCTS				*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwcdot(n, x, incx, y, incy, result)
  unsigned n;
  FComplex *x, *y, *result;
  int incx, incy;
#else
void FDecl rwcdot(unsigned n, const FComplex* restrict x, int incx, const FComplex* restrict y, int incy, FComplex* result)
#endif
{
  double sumr = 0;	/* Always accumulate in double precision */
  double sumi = 0;
  while(n--){
    sumr += x->r * y->r - x->i * y->i;
    sumi += x->r * y->i + x->i * y->r;
    x += incx;
    y += incy;
  }
  result->r = sumr;
  result->i = sumi;

}

/*
 * Conjugate dot product:
 */

#ifdef RW_KR_ONLY
void rwccdot(n, x, incx, y, incy, result)
  unsigned n;
  FComplex *x, *y, *result;
  int incx, incy;
#else
void FDecl rwccdot(unsigned n, const FComplex* restrict x, int incx, const FComplex* restrict y, int incy, FComplex* result)
#endif
{
  double sumr = 0;	/* Always accumulate in double precision */
  double sumi = 0;
  while(n--){
    sumr += x->r * y->r + x->i * y->i;
    sumi += x->r * y->i - x->i * y->r;
    x += incx;
    y += incy;
  }
  result->r = sumr;
  result->i = sumi;
}

/****************************************************************
 *								*
 *		VECTOR --- VECTOR ASSIGNMENT OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwc_aplvv(n, x, incx, y, incy)
  unsigned n;
  FComplex *x, *y;
  int incx, incy;
#else
void FDecl rwc_aplvv(unsigned n, FComplex* restrict x, int incx, const FComplex* restrict y, int incy)
#endif
{
#ifdef HAS_NATIVE_COMPLEX
  while(n--){
    *x += *y;
    x += incx;
    y += incy;
  }
#else
  while(n--){
    x->r += y->r;
    x->i += y->i;
    x += incx;
    y += incy;
  }
#endif
}

#ifdef RW_KR_ONLY
void rwc_amivv(n, x, incx, y, incy)
  unsigned n;
  FComplex *x, *y;
  int incx, incy;
#else
void FDecl rwc_amivv(unsigned n, FComplex* restrict x, int incx, const FComplex* restrict y, int incy)
#endif
{
#ifdef HAS_NATIVE_COMPLEX
  while(n--){
    *x -= *y;
    x += incx;
    y += incy;
  }
#else
  while(n--){
    x->r -= y->r;
    x->i -= y->i;
    x += incx;
    y += incy;
  }
#endif
}

#ifdef RW_KR_ONLY
void rwc_amuvv(n, x, incx, y, incy)
  unsigned n;
  FComplex *x, *y;
  int incx, incy;
#else
void FDecl rwc_amuvv(unsigned n, FComplex* restrict x, int incx, const FComplex* restrict y, int incy)
#endif
{
#ifdef HAS_NATIVE_COMPLEX
  while(n--){
    *x *= *y;
    x += incx;
    y += incy;
  }
#else
  register float re;
  while(n--){
    re = x->r;
    x->r = re * y->r - x->i * y->i;
    x->i = re * y->i + x->i * y->r;
    x += incx;
    y += incy;
  }
#endif
}

#ifdef RW_KR_ONLY
void rwc_advvv(n, x, incx, y, incy)
  unsigned n;
  FComplex *x, *y;
  int incx, incy;
#else
void FDecl rwc_advvv(unsigned n, FComplex* restrict x, int incx, const FComplex* restrict y, int incy)
#endif
{
#ifdef HAS_NATIVE_COMPLEX
  while(n--){
    *x /= *y;
    x += incx;
    y += incy;
  }
#else
  while(n--){
    c_div(x,x,y);
    x += incx;
    y += incy;
  }
#endif
}

/****************************************************************
 *								*
 *		VECTOR --- SCALAR ASSIGNMENT OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwc_aplvs(n, x, incx, scalar)
  unsigned n;
  FComplex *x, *scalar;
  int incx;
#else
void FDecl rwc_aplvs(unsigned n, FComplex* restrict x, int incx, const FComplex* restrict scalar)
#endif
{
#ifdef HAS_NATIVE_COMPLEX
  while(n--) {
    *x += *scalar;
    x += incx;
  }
#else
  while(n--) {
    x->r += scalar->r;
    x->i += scalar->i;
    x += incx;
  }
#endif
}

#ifdef RW_KR_ONLY
void rwc_amuvs(n, x, incx, scalar)
  unsigned n;
  FComplex *x, *scalar;
  int incx;
#else
void FDecl rwc_amuvs(unsigned n, FComplex* restrict x, int incx, const FComplex* restrict scalar)
#endif
{
#ifdef HAS_NATIVE_COMPLEX
  while(n--) {
    *x *= *scalar;
    x += incx;
  }
#else
  register float re;
  while(n--) {
    re = x->r;
    x->r = re * scalar->r - x->i * scalar->i;
    x->i = re * scalar->i + x->i * scalar->r;
    x += incx;
  }
#endif
}

#ifdef RW_KR_ONLY
void rwc_advvs(n, x, incx, scalar)
  unsigned n;
  FComplex *x, *scalar;
  int incx;
#else
void FDecl rwc_advvs(unsigned n, FComplex* restrict x, int incx, const FComplex* restrict scalar)
#endif
{
#ifdef HAS_NATIVE_COMPLEX
  while(n--) {
    *x /= *scalar;
    x += incx;
  }
#else
  while(n--) {
    c_div(x,x,scalar);
    x += incx;
  }
#endif
}

/****************************************************************
 *								*
 *		VECTOR --- VECTOR BINARY OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwc_plvv(n, z, x, incx, y, incy)
  unsigned n;
  FComplex *z, *x, *y;
  int incx, incy;
#else
void FDecl rwc_plvv(unsigned n, FComplex* restrict z, const FComplex* restrict x, int incx, const FComplex* restrict y, int incy)
#endif
{
  while(n--){
#ifdef HAS_NATIVE_COMPLEX
    *z++ = *x + *y;
#else
    z->r = x->r + y->r;
    z->i = x->i + y->i;
    z++;
#endif
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rwc_mivv(n, z, x, incx, y, incy)
  unsigned n;
  FComplex *z, *x, *y;
  int incx, incy;
#else
void FDecl rwc_mivv(unsigned n, FComplex* restrict z, const FComplex* restrict x, int incx, const FComplex* restrict y, int incy)
#endif
{
  while(n--){
#ifdef HAS_NATIVE_COMPLEX
    *z++ = *x - *y;
#else
    z->r = x->r - y->r;
    z->i = x->i - y->i;
    z++;
#endif
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rwc_muvv(n, z, x, incx, y, incy)
  unsigned n;
  FComplex *z, *x, *y;
  int incx, incy;
#else
void FDecl rwc_muvv(unsigned n, FComplex* restrict z, const FComplex* restrict x, int incx, const FComplex* restrict y, int incy)
#endif
{
  while(n--){
#ifdef HAS_NATIVE_COMPLEX
    *z++ = *x * *y;
#else
    z->r = x->r * y->r - x->i * y->i;
    z->i = x->r * y->i + x->i * y->r;
    z++;
#endif
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rwc_dvvv(n, z, x, incx, y, incy)
  unsigned n;
  FComplex *z, *x, *y;
  int incx, incy;
#else
void FDecl rwc_dvvv(unsigned n, FComplex* restrict z, const FComplex* restrict x, int incx, const FComplex* restrict y, int incy)
#endif
{
  while(n--){
#ifdef HAS_NATIVE_COMPLEX
    *z++ = *x / *y;
#else
    c_div(z++,x,y);
#endif
    x += incx;
    y += incy;
  }
}

/****************************************************************
 *								*
 *		VECTOR --- SCALAR BINARY OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwc_plvs(n, z, x, incx, scalar)
  unsigned n;
  FComplex *z, *x, *scalar;
  int incx;
#else
void FDecl rwc_plvs(unsigned n, FComplex* restrict z, const FComplex* restrict x, int incx, const FComplex* restrict scalar)
#endif
{
  while(n--){
#ifdef HAS_NATIVE_COMPLEX
    *z++ = *x + *scalar;
#else
    z->r = x->r + scalar->r;
    z->i = x->i + scalar->i;
	z++;
#endif
    x += incx;
  }
}

#ifdef RW_KR_ONLY
void rwc_muvs(n, z, x, incx, scalar)
  unsigned n;
  FComplex *z, *x, *scalar;
  int incx;
#else
void FDecl rwc_muvs(unsigned n, FComplex* restrict z, const FComplex* restrict x, int incx, const FComplex* restrict scalar)
#endif
{
  while(n--){
#ifdef HAS_NATIVE_COMPLEX
    *z++ = *x * *scalar;
#else
    z->r = x->r * scalar->r - x->i * scalar->i;
    z->i = x->r * scalar->i + x->i * scalar->r;
	z++;
#endif
    x += incx;
  }
}

#ifdef RW_KR_ONLY
void rwc_dvvs(n, z, x, incx, scalar)
  unsigned n;
  FComplex *z, *x, *scalar;
  int incx;
#else
void FDecl rwc_dvvs(unsigned n, FComplex* restrict z, const FComplex* restrict x, int incx, const FComplex* restrict scalar)
#endif
{
  while(n--){
#ifdef HAS_NATIVE_COMPLEX
    *z++ = *x / *scalar;
#else
    c_div(z++,x,scalar);
#endif
    x += incx;
  }
}

/****************************************************************
 *								*
 *		SCALAR --- VECTOR BINARY OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwc_misv(n, z, scalar, x, incx)
  unsigned n;
  FComplex *z, *scalar, *x;
  int incx;
#else
void FDecl rwc_misv(unsigned n, FComplex* restrict z, const FComplex* scalar, const FComplex* restrict x, int incx)
#endif
{
  while(n--){
#ifdef HAS_NATIVE_COMPLEX
    *z++ = *scalar - *x;
#else
    z->r = scalar->r - x->r;
    z->i = scalar->i - x->i;
	z++;
#endif
    x += incx;
  }
}

#ifdef RW_KR_ONLY
void rwc_dvsv(n, z, scalar, x, incx)
  unsigned n;
  FComplex *z, *scalar, *x;
  int incx;
#else
void FDecl rwc_dvsv(unsigned n, FComplex* restrict z, const FComplex* scalar, const FComplex* restrict x, int incx)
#endif
{
  while(n--){
#ifdef HAS_NATIVE_COMPLEX
    *z++ = *scalar / *x;
#else
    c_div(z++,scalar,x);
#endif
    x += incx;
  }
}


/*
 * Definitions for array helper classes and functions
 */

#include "rw/array.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"
#include "rw/igenmat.h"

#ifdef RW_GLOBAL_ENUMS
static IntVec compactArrayStep(            Storage storage, const IntVec& n);
#else
static IntVec compactArrayStep(RWDataView::Storage storage, const IntVec& n);
#endif
  // Above function generates step vector for compact array

RWArrayView::RWArrayView(const IntVec& n, size_t s, Storage storage)
  : RWDataView(prod(n),s), npts(n), step(n.length(),rwUninitialized)
{
  npts.deepenShallowCopy();  // Using n.deepCopy() in npts constructor causes memory leak with bcc
  nonnegCheck(npts);
  step = compactArrayStep(storage,npts);
}

RWArrayView::RWArrayView(unsigned nx, unsigned ny, unsigned nz, size_t s, Storage storage)
  : RWDataView(nx*ny*nz,s), npts(3,rwUninitialized), step(3,rwUninitialized)
{
  npts(0) = nx;
  npts(1) = ny;
  npts(2) = nz;
  if (storage==ROW_MAJOR) {
    step(2) = 1;
    step(1) = nz;
    step(0) = ny*nz;
  } else {
    step(0) = 1;
    step(1) = nx;
    step(2) = nx*ny;
  }
}

RWArrayView::RWArrayView(unsigned nx, unsigned ny, unsigned nz, unsigned nw, size_t s, Storage storage)
  : RWDataView(nx*ny*nz*nw,s), npts(4,rwUninitialized), step(4,rwUninitialized)
{
  npts(0) = nx;
  npts(1) = ny;
  npts(2) = nz;
  npts(3) = nw;
  if (storage==ROW_MAJOR) {
    step(3) = 1;
    step(2) = nw;
    step(1) = nz*nw;
    step(0) = ny*nz*nw;
  } else {
    step(0) = 1;
    step(1) = nx;
    step(2) = nx*ny;
    step(3) = nx*ny*nz;
  }
}

RWArrayView::RWArrayView(RWBlock *block, const IntVec& n, Storage storage)
  : RWDataView(block), npts(n), step(n.length(),rwUninitialized)
{
  npts.deepenShallowCopy();  // Using n.deepCopy() in npts constructor causes memory leak with bcc
  nonnegCheck(npts);
  step = compactArrayStep(storage,npts);
}

RWArrayView::RWArrayView(RWBlock *block, unsigned nx, unsigned ny, unsigned nz, Storage storage)
  : RWDataView(block), npts(3,rwUninitialized), step(3,rwUninitialized)
{
  npts(0) = nx;
  npts(1) = ny;
  npts(2) = nz;
  if (storage==ROW_MAJOR) {
    step(2) = 1;
    step(1) = nz;
    step(0) = ny*nz;
  } else {
    step(0) = 1;
    step(1) = nx;
    step(2) = nx*ny;
  }
}

RWArrayView::RWArrayView(RWBlock *block, unsigned nx, unsigned ny, unsigned nz, unsigned nw, Storage storage)
  : RWDataView(block), npts(4,rwUninitialized), step(4,rwUninitialized)
{
  npts(0) = nx;
  npts(1) = ny;
  npts(2) = nz;
  npts(3) = nw;
  if (storage==ROW_MAJOR) {
    step(3) = 1;
    step(2) = nw;
    step(1) = nz*nw;
    step(0) = ny*nz*nw;
  } else {
    step(0) = 1;
    step(1) = nx;
    step(2) = nx*ny;
    step(3) = nx*ny*nz;
  }
}

RWArrayView::RWArrayView(const RWArrayView& x)
  : RWDataView(x), npts(x.npts), step(x.step)
{
  npts.deepenShallowCopy();  // Using n.deepCopy() in npts constructor causes memory leak with bcc
  step.deepenShallowCopy();
}

RWArrayView::RWArrayView(const RWDataView& v, void *b, const IntVec& n, const IntVec& s)
  : RWDataView(v,b), npts(n), step(s)
{
  npts.deepenShallowCopy();  // Using n.deepCopy() in npts constructor causes memory leak with bcc
  step.deepenShallowCopy();
}
  
void      RWArrayView::reference(const RWArrayView& x)
{
  RWDataView::reference(x);
  npts.reference(x.npts.copy());
  step.reference(x.step.copy());
}

void RWArrayView::boundsCheck(const IntVec& i) const
{
  dimensionCheck(i.length());
  int r = npts.length();
  while( --r>=0 ) {
    if (i(r)<0 || i(r)>=npts(r)) {
      RWTHROW( RWBoundsErr( RWMessage( RWMATH_INDEX, i(r), (unsigned)(npts(r)-1) )));
    }
  }
}

void RWArrayView::boundsCheck(int i, int j, int k) const
{
  static IntVec index(3,rwUninitialized);
  index(0) = i;
  index(1) = j;
  index(2) = k;
  boundsCheck(index);
}

void RWArrayView::boundsCheck(int i, int j, int k, int l) const
{
  static IntVec index(4,rwUninitialized);
  index(0) = i;
  index(1) = j;
  index(2) = k;
  index(3) = l;
  boundsCheck(index);
}

void RWArrayView::nonnegCheck(const IntVec& n) const
{
  int r = n.length();
  while( --r>=0 ) {
    if (n(r)<0) {
      RWTHROW( RWBoundsErr( RWMessage(RWMATH_LENPOS, n(r)) ));
    }
  }
}

void RWArrayView::lengthCheck(const IntVec& n) const 
{
  dimensionCheck(n.length());
  int r = npts.length();
  while( --r>=0 ) {
    if (n(r)!=npts(r)) {
      RWTHROW( RWBoundsErr( RWMessage(RWMATH_NMATCH, (unsigned)(n(r)), (unsigned)(npts(r))) ));
    }
  }
}

void RWArrayView::lengthCheck(int m, int n, int o) const
{
  static IntVec l(3,rwUninitialized);
  l(0) = m;
  l(1) = n;
  l(2) = o;
  lengthCheck(l);
}

void RWArrayView::lengthCheck(int m, int n, int o, int p) const
{
  static IntVec l(4,rwUninitialized);
  l(0) = m;
  l(1) = n;
  l(2) = o;
  l(2) = p;
  lengthCheck(l);
}

void RWArrayView::sliceCheck(const IntVec& start, const IntVec& n, const IntGenMat& str) const 
{
  dimensionCheck(start.length());
  dimensionCheck(str.rows());
  if (n.length()!=str.cols()) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_STRIDEMAT, str.cols(), n.length() )));
  }
  if (prod(n)>0) {      // Allow arbitrary zero length slices
    int r = npts.length();
    while (--r>=0) {
      int limit = npts(r);
      int begin = start(r);
      if (begin<0 || begin>=limit) {
        RWTHROW( RWBoundsErr( RWMessage(RWMATH_SLICEBEG, begin, limit) ));
      }
      int i = n.length();
      while(--i>=0) {
        int end = begin+(n(i)-1)*str(r,i);
        if (end<0 || end>=limit) {
          RWTHROW( RWBoundsErr( RWMessage(RWMATH_SLICEEND, end, limit) ));
        }
      }
    }
  }
}

void RWArrayView::dimensionCheck(int p) const
{
  if (npts.length()!=p) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_ARRDIM, p, npts.length() ) ));
  }
}

void RWArrayView::dimensionLengthCheck(int i,int l) const
{
  if (npts.length()<i+1 || npts(i)!=l) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_NMATCH, (unsigned)l, (unsigned)(npts.length()<i+1?0:npts(i))) ));
  }
}

void RWArrayView::numPointsCheck(const char *why, int n) const
{
  if (prod(npts)<n) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_NPOINTS,(unsigned)prod(npts),why,n) ));
  }
}

/*
 * The following function is used to set up the stride vector for
 * a compact array.
 */
#ifdef RW_GLOBAL_ENUMS
     static IntVec compactArrayStep(            Storage storage, const IntVec& n)
#else
     static IntVec compactArrayStep(RWDataView::Storage storage, const IntVec& n)
#endif     
{
  IntVec step(n.length(),rwUninitialized);
  int i;
  int prod=1;
  if (storage==RWDataView::ROW_MAJOR) {
    for(i=n.length()-1; i>=0; i--) {
      step(i)=prod;
      prod*=n(i);
    }
  } else {
    for(i=0; i<n.length(); i++) {
      step(i)=prod;
      prod*=n(i);
    }
  }
  return step;
}

/*
 * RWMultiIndex and array looper class implementations
 */

void RWMultiIndex::operator++()
{
  if (isOK) {
    for(int r=length(); r-- && ++(*this)(r)>=npts(r); ) {
      (*this)(r)=0;
    }
    if (r<0) isOK=0;
  }
}

RWMultiIndex::RWMultiIndex(const IntVec& n) : IntVec(n.length(),0), npts(n)
{
  isOK = (minValue(npts)>0);
}


/* Implementation of the Looper class */

ArrayLooper::ArrayLooper(const IntVec& N, const IntVec& S)
{
  if (prod(N)==0) {
    isOK=0;    // No elements
    return;
  }

  isOK = 1;
  start = 0;

  if (N.length()==0) {
    length = 1;
    stride = 1;
    return;
  }

  // First determine how long the initial contiguous chunks are.
  // Conglomerate as many initial indices as possible.
  stride = S(0);
  length = N(0);
  int firstIndex=1;  // First index which it is necessary to loop over
  while ( firstIndex<N.length() && S(firstIndex)==length*stride ) {
    int n = N(firstIndex++);
    length *= n;
    // length *= N(firstIndex++);
    // The Borland C++ 2.0 compiler does bizarre things with
    // the (commented) line above, probably due to some bug
    // in how it handles unsigneds.
  }

  // Now construct the state necessary to loop over the remaining indices
  if (firstIndex>=N.length()) {
    return;   // The whole array is contiguous
  }
  int stateLength = N.length()-firstIndex;
  npts.reference(N.slice(firstIndex,stateLength));
  step.reference(S.slice(firstIndex,stateLength));
  state.reference(IntVec((unsigned)stateLength,0));
}
  
void ArrayLooper::operator++()
{
  // The loop here is a little weird.  Trouble is, state.length() might be
  // of length zero, so the 1st if statement needs to be executed before
  // anything else.

  int r=0;
  while (1) {
    if (r>=state.length()) { isOK=0; return; }
    start += step(r);
    if (++state(r)<npts(r)) { return; }
    state(r)=0;
    start -= step(r)*npts(r);
    r++;
  }
}


/* Implementation of the DoubleArrayLooper class */

DoubleArrayLooper::DoubleArrayLooper(const IntVec& N,const IntVec& S1, const IntVec& S2)
{
  if (prod(N)==0) {
    isOK=0;    // No elements
    return;
  }

  isOK = 1;
  start1 = 0;
  start2 = 0;

  if (N.length()==0) {
    length  = 1;
    stride1 = 1;
    stride2 = 1;
    return;
  }

  // First determine how long the initial contiguous chunks are.
  // Conglomerate as many initial indices as possible.
  stride1 = S1(0);
  stride2 = S2(0);
  length  = N(0);
  int firstIndex=1;  // First index which it is necessary to loop over
  while ( firstIndex<N.length() && S1(firstIndex)==length*stride1
                                && S2(firstIndex)==length*stride2  ) {
    int n = N(firstIndex++);
    length *= n;
    // length *= N(firstIndex++);
    // The Borland C++ 2.0 compiler does bizarre things with the line
    // above, probably due to some bug in how it handles unsigneds.
  }

  // Now construct the state necessary to loop over the remaining indices
  if (firstIndex>=N.length()) {
    return;   // The whole array is contiguous
  }
  int stateLength = N.length()-firstIndex;
  npts.reference(N.slice(firstIndex,stateLength));
  step1.reference(S1.slice(firstIndex,stateLength));
  step2.reference(S2.slice(firstIndex,stateLength));
  state.reference(IntVec((unsigned)stateLength,0));
}
  
void DoubleArrayLooper::operator++()
{
  // The loop here is a little weird.  Trouble is, state.length() might be
  // of length zero, so the 1st if statement needs to be executed before
  // anything else.

  int r=0;
  while (1) {
    if (r>=state.length()) { isOK=0; return; }
    start1 += step1(r);
    start2 += step2(r);
    if (++state(r)<npts(r)) { return; }
    state(r)=0;
    start1 -= step1(r)*npts(r);
    start2 -= step2(r)*npts(r);
    r++;
  }
}

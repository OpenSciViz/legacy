/*
 * Definitions for global statistics functions.
 *
 * $Header: /users/rcs/mathsrc/dstats.cpp,v 1.2 1993/08/10 21:22:18 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: dstats.cpp,v $
 * Revision 1.2  1993/08/10  21:22:18  alv
 * now uses Tools v6 compatible error handling
 *
 * Revision 1.1  1993/01/22  23:50:58  alv
 * Initial revision
 *
 * 
 *    Rev 1.6   15 Nov 1991 09:36:26   keffer
 * Removed RWMATHERR macro
 * 
 *    Rev 1.5   13 Nov 1991 11:02:20   keffer
 * Ported to RS6000 which had an unusable declaration for qsort().
 * 
 *    Rev 1.4   17 Oct 1991 09:15:16   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.2   01 Sep 1991 17:40:14   keffer
 * Added new global function rwEpslon(double).  Returns roundoff estimate.
 * 
 */

#ifdef _IBMR2
#  define qsort QSORT
#endif

#include "rw/dstats.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"
STARTWRAP
#include <stdlib.h>
ENDWRAP

#ifdef _IBMR2
# undef qsort
  extern "C" void qsort(void *base, size_t N, size_t size, 
                        int(*compar)(const void*, const void*));
#endif

extern "C" int
RWcfun(const void* a1, const void* a2)
{
  register double av1 = *(const double*)a1;
  register double av2 = *(const double*)a2;

  return av1==av2 ? 0 : (av1<av2 ? -1 : 1);
}


DoubleVec 
sort(const DoubleVec& v_original)
{ 
  DoubleVec v = v_original.copy();
  qsort((char*)(v.data()), v.length(), sizeof(double), RWcfun);
  return v;
}

// statistics functions

Double
skewness(const DoubleVec& s)
// Skewness of a DoubleVec
{
  Double sigma = ::sqrt(variance(s));  // Standard Deviation.
  if(sigma==0) {
    RWTHROW( RWInternalErr( RWMessage( RWMATH_ZEROSD )));
  }
  DoubleVec temp((s-mean(s))/sigma);
  return sum(temp*temp*temp)/s.length();
}                    

Double
kurtosis(const DoubleVec& s)
// Kurtosis of a DoubleVec
{
  register Double sigma = ::sqrt(variance(s));  // Standard Deviation.
  if(sigma==0) {
    RWTHROW( RWInternalErr( RWMessage( RWMATH_ZEROSD )));
  }
  DoubleVec temp((s-mean(s))/sigma);
  return   sum(temp * temp * temp * temp)/s.length() - 3 ;
}

/*
 * Estimate unit roundoff in quantities of size x.
 * From J. Dongarra's Linpack benchmark.
 */
double
rwEpslon(double x)
{
  double a = 4.0e0/3.0e0;
backto:		/* To defeat optimizing compilers. */
  double b = a - 1.0e0;
  double c = b + b + b;
  double eps = fabs(c-1.0e0);
  if (eps == 0.0e0) goto backto;
  return eps*fabs(x);
}


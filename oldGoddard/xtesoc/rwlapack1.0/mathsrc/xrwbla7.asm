> #
> #		Assembly Rogue Wave BLA extensions for the 80x87
> #		in real mode.
> #
> # $Header: /users/rcs/mathsrc/xrwbla7.asm,v 1.3 1993/03/22 15:51:31 alv Exp $
> #
> # ***************************************************************************
> # 
> # Rogue Wave 
> # P.O. Box 2328
> # Corvallis, OR 97339
> # Voice: (503) 754-2311	FAX: (503) 757-7350
> # 
> # Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
> # protection under the laws of the United States and other countries.
> # 
> # ***************************************************************************
> #
> # $Log: xrwbla7.asm,v $
;; Revision 1.3  1993/03/22  15:51:31  alv
;; changed PVCS Workfile keyword to RCS ID keyword
;;
;; Revision 1.2  1993/01/26  17:52:28  alv
;; removed DOS EOF
;;
;; Revision 1.1  1993/01/22  23:51:18  alv
;; Initial revision
;;
> # 
> #    Rev 1.1   17 Oct 1991 09:15:56   keffer
> # Changed include path to <rw/xxx.h>
> # 
> #    Rev 1.0   08 Oct 1991 13:48:52   keffer
> # Initial revision.
> # 
> #
> include macros
> #
; <T> precision Rogue Wave BLA extensions
; using the 80x87 coprocessor in real mode
; 
; Generated from template $Id: xrwbla7.asm,v 1.3 1993/03/22 15:51:31 alv Exp $
; 
; ***************************************************************************
; 
; Rogue Wave 
; P.O. Box 2328
; Corvallis, OR 97339
; Voice: (503) 754-2311	FAX: (503) 757-7350
; 
; Copyright (C) 1989, 1990, 1991. This software is subject to copyright 
; protection under the laws of the United States and other countries.
; 
; ***************************************************************************

; This file depends on two symbols having been set on the
; command line: I8086[smcl] and FPU.
;
; The symbol "I8086[smcl]" should be set to one of
;
;	I8086S:	Small memory model
;	I8086M:	Medium memory model
;	I8086C:	Compact memory model
;	I8086L:	Large memory model
;
; The symbol "FPU" should be set to one of the following:
;
;	FPU:	Assemble for 8087 or better
;	FPU3:	Assemble for 80387 or better
;
; Hence, a sample command line (using MASM) for compact memory model,
; using 80387 capabilities would be
;
;	MASM -DI8086c -DFPU3 <bla>rwbla7.asm
;

> if <t>==double
>   define <size>=8
>   define <word>=QWORD
>   define <shlsz>=3
> else
>   define <size>=4
>   define <word>=DWORD
>   define <shlsz>=2
> endif
;
; All routines within this file restore si,di,ds, and es if they use them.
; The contents of ax, bx, cx, and dx may change.
;

include rwmasm.inc

IFNDEF memmodel
.ERR
%OUT "memmodel" must have been defined
ENDIF

%                .MODEL  memmodel,PASCAL

IFDEF FPU3
  .386
  .387
ELSE
  .8086
  .8087
ENDIF

CPU286		EQU	(@Cpu AND 0004h)
CPU386		EQU	(@Cpu AND 0008h)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;					;;
;;	MACROS used in this file	;;
;;					;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

LOADINC		MACRO	reg,inc
		mov     reg,inc
IF	CPU286
		shl     reg,<shlsz>
ELSE
		shl     reg,1
		shl     reg,1
> if <t>==double
		shl     reg,1
> endif
ENDIF
		ENDM

_LES		MACRO	reg,val
IF @DataSize
		les	reg,val
ELSE
		mov	reg,val
ENDIF
		ENDM

_LDS		MACRO	reg,val
IF @DataSize
		lds	reg,val
ELSE
		mov	reg,val
ENDIF
		ENDM

.CODE

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;						;;
;;		Utility functions		;;
;;						;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; void rw<bla>copy(unsigned n, <t> *x, int incx, <t> *y, int incy ){
;   while(n--){ *x = *y; x+=incx; y+=iny; }
;
		PUBLIC  rw<bla>copy
IF @DataSize
rw<bla>copy		PROC    uses di si ds es, n:WORD, x:PTR <word>, incx:WORD, y:PTR <word>, incy:WORD
ELSE
rw<bla>copy		PROC    uses di si,       n:WORD, x:PTR <word>, incx:WORD, y:PTR <word>, incy:WORD
ENDIF

		mov	cx,n
		jcxz	@@Fini
		_LES	di,x
		_LDS	si,y

		LOADINC	ax,incx			; Put incx into ax
		LOADINC	bx,incy			; Put incy into bx

@@Loop1:	fld	<word> ptr [si]		; Load *y
		add     si,bx			; Increment y by incy
IF @DataSize
		fstp    <word> ptr es:[di]	; Store into *x
ELSE
		fstp    <word> ptr [di]
ENDIF
		add     di,ax			; Increment x by incx
		loop    @@Loop1

@@Fini:		fwait
		ret
rw<bla>copy		ENDP

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; void rw<bla>set(unsigned n, <t> *x, int incx, const <t> *scalar){
;   while(n--){ *x = scalar; x+=incx; }
;
		PUBLIC rw<bla>set
rw<bla>set		PROC	uses di es, n:WORD, x:PTR <word>, incx:WORD, scalar:PTR <word>

		mov	cx,n
		jcxz	@@Fini
IF @DataSize
		les	di,scalar
		fld	<word> PTR es:[di]
		les	di,x
ELSE
		mov	di,scalar
		fld	<word> PTR [di]
		mov	di,x
ENDIF
		LOADINC	ax,incx

IF @DataSize
@@Loop1:	fst	<word> PTR es:[di]	; Store the scalar to *x
ELSE
@@Loop1:	fst	<word> PTR [di]
ENDIF
		add	di,ax			; Increment x
		loop	@@Loop1
		fstp	st			; Throw away the scalar

@@Fini:		fwait
		ret
rw<bla>set		ENDP

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; int rw<bla>same(unsigned n, const <t>* x, int incx, const <t> *y, int incy)
;   while(n--){ if(*x != *y)return 0; x+=incx; y+=incy; } 
;   return 1; 
; }
;
		PUBLIC  rw<bla>same
IF @DataSize
rw<bla>same		PROC    uses di si ds es, n:WORD, x:PTR <word>, incx:WORD, y:PTR <word>, incy:WORD
ELSE
rw<bla>same		PROC    uses di si,       n:WORD, x:PTR <word>, incx:WORD, y:PTR <word>, incy:WORD
ENDIF
IFE		CPU286
		LOCAL	stat87:WORD		; Used to hold the '87 control word
ENDIF
		mov     cx,n
		jcxz    Equal
		_LES	di,x			; es:[di] has x
		_LDS	si,y			; ds:[si] has y

		LOADINC	dx,incx			; Put incx into dx
		LOADINC	bx,incy			; Put incy into bx

@@Loop1:	fld     <word> ptr [si]		; Load *y
		add     si,bx			; Increment y by incy
IF @DataSize
		fcomp	<word> ptr es:[di]	; Compare with *x
ELSE
		fcomp	<word> ptr [di]
ENDIF
IF CPU286
		fstsw	ax
		fwait
ELSE
		fstsw	stat87
		fwait
		mov	ax,stat87
ENDIF
		sahf
		jne	SHORT NotEqual
		add     di,dx			; Increment x by incx
		loop	@@Loop1

Equal:		mov	ax,1			; Put TRUE in AX
		jmp	SHORT @@Fini
NotEqual:	xor	ax,ax			; Put FALSE in ax

@@Fini:		fwait
		ret
rw<bla>same		ENDP

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;	Returns index of max value in x
; int rw<bla>max(unsigned n, const <t>* x, int incx){
;   int i, maxi=0;
;   <t> maxv = *x;
; 
;   if(n==0) return -1;
; 
;   for(i=1; i<n; i++){
;     x += incx;
;     if(*x > maxv) { maxv = *x; maxi=i; }
;   }
;   return maxi;
; }
;
		PUBLIC  rw<bla>max
rw<bla>max		PROC    uses di es, n:WORD, x:PTR <word>, incx:WORD
		LOCAL	stat87:WORD		; Used to hold the '87 control word
		LOCAL	maxi:WORD		; Used to hold index of current max value

		mov     cx,n
		jcxz	@@Error			; Error if there are no elements
		xor	ax,ax			; Move zero index into dx
		cmp	cx,1			; If there is only one element, then it's the max element
		je	@@Fini

		_LES	di,x			; es:[di] has x
		LOADINC	dx,incx
IF @DataSize
		fld	<word> ptr es:[di]	; Put x[0] on top of stack
ELSE
		fld	<word> ptr [di]
ENDIF
		mov	maxi,0			; Current max index
		mov	bx,1			; Current index

@@Loop1:	add	di,dx			; Increment x
IF @DataSize
		fld	<word> ptr es:[di]
ELSE
		fld	<word> ptr [di]
ENDIF
		fcom
IF CPU286
		fstsw	ax
		fwait
ELSE
		fstsw	stat87
		fwait
		mov	ax,stat87
ENDIF
		sahf
		jbe	SHORT LessOrEq
		fst	st(1)
		mov	maxi,bx
LessOrEq:	fstp	st
		inc	bx
		cmp	bx,cx
		jl	@@Loop1

		fstp	st			; Throw away the max value, which we don't need
		mov	ax,maxi
		jmp	SHORT @@Fini

@@Error:	mov	ax,-1

@@Fini:		fwait
		ret

rw<bla>max		ENDP

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;	Returns index of min value in x
; int rw<bla>min(unsigned n, const <t>* x, int incx){
;   int i, mini=0;
;   <t> minv = *x;
; 
;   if(n==0) return -1;
; 
;   for(i=1; i<n; i++){
;     x += incx;
;     if(*x < minv) { minv = *x; mini=i; }
;   }
;   return mini;
; }
;
		PUBLIC  rw<bla>min
rw<bla>min		PROC    uses di es, n:WORD, x:PTR <word>, incx:WORD
		LOCAL	stat87:WORD		; Used to hold the '87 control word
		LOCAL	mini:WORD		; Used to hold index of current min value

		mov     cx,n
		jcxz	@@Error			; Error if there are no elements
		xor	ax,ax			; Move zero index into dx
		cmp	cx,1			; If there is only one element, then it's the min element
		je	@@Fini

		_LES	di,x			; es:[di] has x
		LOADINC	dx,incx
IF @DataSize
		fld	<word> ptr es:[di]	; Put x[0] on top of stack
ELSE
		fld	<word> ptr [di]
ENDIF
		mov	mini,0			; Current min index
		mov	bx,1			; Current index

						; Main loop
@@Loop1:	add	di,dx			; Increment x
IF @DataSize
		fld	<word> ptr es:[di]
ELSE
		fld	<word> ptr [di]
ENDIF
		fcom
IF CPU286
		fstsw	ax
		fwait
ELSE
		fstsw	stat87
		fwait
		mov	ax,stat87
ENDIF
		sahf
		jae	SHORT @@GThanOrEq
		fst	st(1)
		mov	mini,bx
@@GThanOrEq:	fstp	st
		inc	bx
		cmp	bx,cx
		jl	@@Loop1

		fstp	st			; Throw away the min value, which we don't need
		mov	ax,mini
		jmp	SHORT @@Fini

@@Error:	mov	ax,-1

@@Fini:		fwait
		ret

rw<bla>min		ENDP

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;						;;
;;		Dot Product			;;
;;						;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;	Returns dot product of x and y
; void rw<bla>dot(unsigned n, const <t>* x, int incx, const <t>* y, int incy, <t>* result)
;   <t> sum = 0;
;   while(n--){ sum += *x * *y; x += incx; y += incy; }
;   *result = (<t>)sum;
; }
;
		PUBLIC  rw<bla>dot
IF @DataSize
rw<bla>dot		PROC    uses di si ds es, n:WORD, x:PTR <word>, incx:WORD, y:PTR <word>, incy:WORD, result:PTR <word>
ELSE
rw<bla>dot		PROC    uses di si      , n:WORD, x:PTR <word>, incx:WORD, y:PTR <word>, incy:WORD, result:PTR <word>
ENDIF
		fldz				; Put 0.0 on the top of the '87 stack

		mov     cx,n
		jcxz    @@Fini
		_LES	di,x
		_LDS	si,y
		LOADINC	ax,incx			; Put incx into ax
		LOADINC	bx,incy			; Put incy into bx

@@Loop1:	fld     <word> ptr [si]		; Load *y
		add     si,bx			; Increment y by incy
IF @DataSize
		fmul    <word> ptr es:[di]	; Multiply by *x
ELSE
		fmul    <word> ptr [di]
ENDIF
		add     di,ax			; Increment x by incx
		fadd				; Add to running sum
		loop    @@Loop1

@@Fini:		_LDS	di,result
		fstp	<word> ptr [di]
		fwait
		ret

rw<bla>dot		ENDP




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;									;;
;;									;;
;;			ARITHMETIC OPERATORS				;;
;;									;;
;;									;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;




;-----------------------------------------------------------------------;
;									;
;		Hairy macro department					;
;									;
;	What follows is a few macros that are used			;
;	implement the various arithmetic functions,			;
;	thus improving maintenance.					;
;									;
;-----------------------------------------------------------------------;


;------------------------------------------------;
;-						-;
;-		Vector - Vector			-;
;-		arithmetic assignment		-;
;-						-;
;------------------------------------------------;

; void rwd_aOPvv(unsigned n, double* x, int incx, const double* y, int incy) {
;   while(n--){ *x <OP>= *y; x += incx; y += incy; }
; }
;	where OP is one of (pl, mi, mu, dv)
;	and   <OP> is one of (+, -, *, dv)
;


AOPVV		MACRO	OP,OPCODE
		PUBLIC  rw<bla>_a&OP&vv
IF @DataSize
rw<bla>_a&OP&vv	PROC    uses di si ds es, n:WORD, x:PTR <word>, incx:WORD, y:PTR <word>, incy:WORD
ELSE
rw<bla>_a&OP&vv	PROC    uses di si      , n:WORD, x:PTR <word>, incx:WORD, y:PTR <word>, incy:WORD
ENDIF

		mov	cx,n
		jcxz	@@Fini

IF @DataSize
		lds	si,x
		les	di,y
ELSE
		mov	si,x
		mov	di,y
ENDIF
		LOADINC	ax, incx
		LOADINC	bx, incy

@@Loop1:	fld	<word> ptr [si]
IF @DataSize
%		OPCODE	<word> ptr es:[di]
ELSE
%		OPCODE	<word> ptr [di]
ENDIF
		add	di,bx
		fstp	<word> ptr [si]
		add	si,ax
		loop	@@Loop1

@@Fini:		fwait
		ret

rw<bla>_a&OP&vv	ENDP
		ENDM

;------------------------------------------------;
;-						-;
;-		Vector - Scalar			-;
;-		arithmetic assignment		-;
;-						-;
;------------------------------------------------;

;
; void rw<bla>_aOPvs(unsigned n, double* x, int incx, const double* scalar){
;   while(n--) { *x <OP>= *scalar; x += incx; }
; }
;
AOPVS		MACRO	OP,OPCODE

%		PUBLIC  rw<bla>_a&OP&vs
IF @DataSize
rw<bla>_a&OP&vs	PROC    uses si ds, n:WORD, x:PTR <word>, incx:WORD, scalar:PTR <word>
ELSE
rw<bla>_a&OP&vs	PROC    uses si   , n:WORD, x:PTR <word>, incx:WORD, scalar:PTR <word>
ENDIF

		mov	cx,n
		jcxz	@@Fini
IF @DataSize
		lds	si,scalar		; ds:[si] gets scalar
		fld	<word> PTR [si]		; Load *scalar onto '87 stack
		lds	si,x			; ds:[si] gets x
ELSE
		mov	si,scalar
		fld	<word> PTR [si]		; Load *scalar onto '87 stack
		mov	si,x
ENDIF
		LOADINC	ax,incx

@@Loop1:	fld	<word> ptr [si]		; Get "*x"
%		OPCODE	st,st(1)		; Replace top of stack with "*x + scalar"
		fstp	<word> ptr [si]		; Store the results in "*x"
		add	si,ax			; Increment x
		loop	@@Loop1

		fstp	st			; Pop the scalar off the stack

@@Fini:		fwait
		ret

rw<bla>_a&OP&vs	ENDP
		ENDM

;------------------------------------------------;
;-						-;
;-		Vector - Vector			-;
;-		Binary operators		-;
;-						-;
;------------------------------------------------;

; void rw<bla>_OPvv(unsigned n, double* z, const double* x, int incx, const double* y, int incy) {
;   while(n--){ *z++ = *x <OP> *y; x += incx; y += incy; }
; }
;	where OP is one of (pl, mi, mu, dv)
;	and   <OP> is one of (+, -, *, dv)

;		CODE FOR LONG POINTERS
;
OPVVL		MACRO	OP,OPCODE

%		PUBLIC	rw<bla>_&OP&vv
rw<bla>_&OP&vv	PROC    uses di si ds es, n:WORD, z:PTR <word>, x:PTR <word>, incx:WORD, y:PTR <word>, incy:WORD
;
;		In what follows:
;			ds:[si] always holds x.  
;			ax always holds the segment of z.
;			dx always holds the segment of y.
;	The offsets of z and y are switched between di and bx.

		mov	cl,<shlsz>
		shl	word ptr incx,cl	; incx *= <size>
		shl	word ptr incy,cl	; incy *= <size>

		mov	cx,n
		jcxz	@@Fini

		lds	si,x			; ds:[si] holds x
		les	di,z			; es:[di] holds z
		mov	ax,es			; Put z's segment in ax
		mov	bx,di			; Now ax:[bx] holds a copy of z
		les	di,y			; es:[di] holds y
		mov	dx,es			; Put y's segment in dx; now dx:[di] holds y

@@Loop1:	fld	<word> ptr ds:[si]	; Load "*x"
		add	si,incx			; Increment x
%		OPCODE <word> ptr es:[di]	; Add "*y"
		add	di,incy			; Increment y
		xchg	di,bx			; Switch z's segment with y's segment
		mov	es,ax			; Move z's offset to replace y's offset.  Now es:[di] contains z
		fstp	<word> ptr es:[di]	; Store the results in "*z"
		xchg	di,bx			; Now switch the offsets back
		mov	es,dx			; Restore y's segment
		add	bx,<size>		; Increment z
		loop	@@Loop1

@@Fini:		fwait
		ret

rw<bla>_&OP&vv	ENDP

		ENDM

;		CODE FOR LONG POINTERS --- 386
;
OPVVL386	MACRO	OP,OPCODE
		PUBLIC	rw<bla>_&OP&vv
rw<bla>_&OP&vv	PROC    uses di si ds fs es, n:WORD, z:PTR <word>, x:PTR <word>, incx:WORD, y:PTR <word>, incy:WORD

		mov	cx,n
		jcxz	@@Fini

		LOADINC	ax,incx
		LOADINC	dx,incy

		lds	si,x
		lfs	di,y
		les	bx,z

@@Loop1:	fld	<word> ptr [si]		; Load *x
		add	si,ax			; Increment x
%		OPCODE	<word> ptr fs:[di]	; Add *y
		add	di,dx			; Increment y
		fstp	<word> ptr es:[bx]	; Store in *z
		add	bx,<size>		; Increment z
		loop	@@Loop1

@@Fini:		fwait
		ret

rw<bla>_&OP&vv	ENDP

		ENDM

;		CODE FOR SHORT POINTERS
;
OPVVS		MACRO	OP,OPCODE

%		PUBLIC	rw<bla>_&OP&vv
rw<bla>_&OP&vv	PROC    uses di si, n:WORD, z:PTR <word>, x:PTR <word>, incx:WORD, y:PTR <word>, incy:WORD

		mov	cx,n
		jcxz	@@Fini

		LOADINC	ax,incx
		LOADINC	dx,incy

		mov	si,x
		mov	di,y
		mov	bx,z

@@Loop1:	fld	<word> ptr [si]		; Load *x
		add	si,ax			; Increment x
%		OPCODE	<word> ptr [di]		; Add *y
		add	di,dx			; Increment y
		fstp	<word> ptr [bx]		; Store in *z
		add	bx,<size>		; Increment z
		loop	@@Loop1

@@Fini:		fwait
		ret

rw<bla>_&OP&vv	ENDP

		ENDM

;------------------------------------------------;
;-						-;
;-		Vector - Scalar			-;
;-		Binary operators		-;
;-						-;
;------------------------------------------------;

; void rw<bla>_OPvs(unsigned n, double* z, const double* x, int incx, const double* scalar) {
;   while(n--){ *z++ = *x <OP> *scalar; x += incx; }
; }
;	where OP is one of (pl, mi, mu, dv)
;	and   <OP> is one of (+, -, *, dv)
;


OPVS		MACRO	OP,OPCODE
		PUBLIC  rw<bla>_&OP&vs
IF @DataSize
rw<bla>_&OP&vs	PROC    uses di si ds es, n:WORD, z:PTR <word>, x:PTR <word>, incx:WORD, scalar:PTR <word>
ELSE
rw<bla>_&OP&vs	PROC    uses di si      , n:WORD, z:PTR <word>, x:PTR <word>, incx:WORD, scalar:PTR <word>
ENDIF

		mov	cx,n
		jcxz	@@Fini

IF @DataSize
		lds	si,scalar		; ds:[si] gets scalar
		fld	<word> PTR [si]		; Load *scalar onto '87 stack
		lds	si,x			; ds:[si] gets x
		les	di,z			; es:[di] gets z
ELSE
		mov	si,scalar
		fld	<word> PTR [si]		; Load *scalar onto '87 stack
		mov	si,x
		mov	di,z
ENDIF
		LOADINC	ax,incx

@@Loop1:	fld	<word> ptr [si]		; Get *x
		add	si,ax			; Increment x
%		OPCODE	st,st(1)		; Replace top of stack with "*x <OP> scalar"
IF @DataSize
		fstp	<word> ptr es:[di]	; Store the results in "*z"
ELSE
		fstp	<word> ptr [di]
ENDIF
		add	di,<size>		; Increment z
		loop	@@Loop1

		fstp	st			; Pop the scalar off the stack

@@Fini:		fwait
		ret

rw<bla>_&OP&vs	ENDP

		ENDM

;------------------------------------------------;
;-						-;
;-		Scalar - Vector			-;
;-		Binary operators		-;
;-						-;
;------------------------------------------------;

;	This macro is actually identical to the macro above,
;	except for the generated function name.
;
; void rw<bla>_OPsv(unsigned n, double* z, const double* x, int incx, const double* scalar) {
;   while(n--){ *z++ = *scalar <OP> *x; x += incx; }
; }
;	where OP is one of (pl, mi, mu, dv)
;	and   <OP> is one of (+, -, *, dv)
;


OPSV		MACRO	OP,OPCODE
		PUBLIC  rw<bla>_&OP&sv
IF @DataSize
rw<bla>_&OP&sv	PROC    uses di si ds es, n:WORD, z:PTR <word>, scalar:PTR <word>, x:PTR <word>, incx:WORD
ELSE
rw<bla>_&OP&sv	PROC    uses di si      , n:WORD, z:PTR <word>, scalar:PTR <word>, x:PTR <word>, incx:WORD
ENDIF

		mov	cx,n
		jcxz	@@Fini

IF @DataSize
		lds	si,scalar		; ds:[si] gets scalar
		fld	<word> PTR [si]		; Load *scalar onto '87 stack
		lds	si,x			; ds:[si] gets x
		les	di,z			; es:[di] gets z
ELSE
		mov	si,scalar
		fld	<word> PTR [si]		; Load *scalar onto '87 stack
		mov	si,x
		mov	di,z
ENDIF
		LOADINC	ax,incx

@@Loop1:	fld	<word> ptr [si]		; Get *x
		add	si,ax			; Increment x
%		OPCODE	st,st(1)		; Replace top of stack with "*x <OP> scalar"
IF @DataSize
		fstp	<word> ptr es:[di]	; Store the results in "*z"
ELSE
		fstp	<word> ptr [di]
ENDIF
		add	di,<size>		; Increment z
		loop	@@Loop1

		fstp	st			; Pop the scalar off the stack

@@Fini:		fwait
		ret

rw<bla>_&OP&sv	ENDP

		ENDM

		
		
		
;-----------------------------------------------------------------------;
;									;
;		End Hairy macro department				;
;									;
;-----------------------------------------------------------------------;






;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;						;;
;;		Vector - Vector			;;
;;		arithmetic assignment		;;
;;						;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; void rw<bla>_aplvv(unsigned n, <t>* x, int incx, const <t>* y, int incy) {
;   while(n--){ *x += *y; x += incx; y += incy; }
; }
		AOPVV	pl,fadd


; void rw<bla>_amivv(unsigned n, <t>* x, int incx, const <t>* y, int incy) {
;   while(n--){ *x -= *y; x += incx; y += incy; }
; }
		AOPVV	mi,fsub


; void rw<bla>_amuvv(unsigned n, <t>* x, int incx, const <t>* y, int incy) {
;   while(n--){ *x *= *y; x += incx; y += incy; }
; }
		AOPVV	mu,fmul


; void rw<bla>_advvv(unsigned n, <t>* x, int incx, const <t>* y, int incy) {
;   while(n--){ *x /= *y; x += incx; y += incy; }
; }
		AOPVV	dv,fdiv


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;						;;
;;		Vector - Scalar			;;
;;		arithmetic assignment		;;
;;						;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; void rw<bla>_aplvs(unsigned n, <t>* x, int incx, const <t>* scalar){
;   while(n--) { *x += *scalar; x += incx; }
; }
		AOPVS	pl,fadd


; void rw<bla>_amuvs(unsigned n, <t>* x, int incx, const <t>* scalar){
;   while(n--) { *x *= *scalar; x += incx; }
; }
		AOPVS	mu,fmul


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;						;;
;;		Vector - Vector			;;
;;		Binary operators		;;
;;						;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; void rw<bla>_plvv(unsigned n, <t>* z, const <t>* x, int incx, const <t>* y, int incy) {
;   while(n--){ *z++ = *x + *y; x += incx; y += incy; }
; }
IF @DataSize
  IF CPU386
		OPVVL386	pl,fadd
  ELSE
		OPVVL		pl,fadd
  ENDIF
ELSE
		OPVVS		pl,fadd
ENDIF


; void rw<bla>_mivv(unsigned n, <t>* z, const <t>* x, int incx, const <t>* y, int incy) {
;   while(n--){ *z++ = *x - *y; x += incx; y += incy; }
; }
IF @DataSize
  IF CPU386
		OPVVL386	mi,fsub
  ELSE
		OPVVL		mi,fsub
  ENDIF
ELSE
		OPVVS		mi,fsub
ENDIF


; void rw<bla>_muvv(unsigned n, <t>* z, const <t>* x, int incx, const <t>* y, int incy) {
;   while(n--){ *z++ = *x * *y; x += incx; y += incy; }
; }
IF @DataSize
  IF CPU386
  		OPVVL386	mu,fmul
  ELSE
		OPVVL		mu,fmul
  ENDIF
ELSE
		OPVVS		mu,fmul
ENDIF


; void rw<bla>_dvvv(unsigned n, <t>* z, const <t>* x, int incx, const <t>* y, int incy) {
;   while(n--){ *z++ = *x / *y; x += incx; y += incy; }
; }
IF @DataSize
  IF CPU386
		OPVVL386	dv,fdiv
  ELSE
  		OPVVL		dv,fdiv
  ENDIF
ELSE
		OPVVS		dv,fdiv
ENDIF


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;						;;
;;		Vector - Scalar			;;
;;		Binary operators		;;
;;						;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; void rw<bla>_plvs(unsigned n, <t>* z, const <t>* x, int incx, const <t>* scalar) {
;   while(n--){ *z++ = *x + *scalar; x += incx; }
; }
		OPVS	pl,fadd


; void rw<bla>_muvs(unsigned n, <t>* z, const <t>* x, int incx, const <t>* scalar) {
;   while(n--){ *z++ = *x * *scalar; x += incx; }
; }
		OPVS	mu,fmul


; void rw<bla>_dvvs(unsigned n, <t>* z, const <t>* x, int incx, const <t>* scalar) {
;   while(n--){ *z++ = *x / *scalar; x += incx; }
; }
		OPVS	dv,fdiv


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;						;;
;;		Scalar - Vector			;;
;;		Binary operators		;;
;;						;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


; void rw<bla>_misv(unsigned n, <t>* z, const <t>* scalar, const <t>* x, int incx) {
;   while(n--){ *z++ = *scalar - *x; x += incx; }
; }
		OPSV	mi,fsubr


; void rw<bla>_dvsv(unsigned n, <t>* z, const <t>* scalar, const <t>* x, int incx) {
;   while(n--){ *z++ = *scalar / *x; x += incx; }
; }
		OPSV	dv,fdivr




		END

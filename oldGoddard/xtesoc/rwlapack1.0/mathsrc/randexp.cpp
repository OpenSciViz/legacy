/*
 * Definitions for class RandExp
 *
 * $Header: /users/rcs/mathsrc/randexp.cpp,v 1.2 1993/01/26 17:52:28 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: randexp.cpp,v $
 * Revision 1.2  1993/01/26  17:52:28  alv
 * removed DOS EOF
 *
 * Revision 1.1  1993/01/22  23:51:02  alv
 * Initial revision
 *
 * 
 *    Rev 1.3   17 Oct 1991 09:15:22   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.2   08 Oct 1991 13:44:44   keffer
 * Math V4.0 beta 2
 * 
 *    Rev 1.1   24 Jul 1991 12:51:46   keffer
 * Added pvcs keywords
 *
 */

/* Definitions for exponential Random Number Class  */

#include "rw/dvec.h"
#include "rw/dgenmat.h"
#include "rw/randexp.h"

void
RandExp::randValue(double* d, unsigned n)
{
  while (n--) { *d++ = RandExp::randValue(); }
}

DoubleVec 		
RandExp::randValue(unsigned n) 
{
   return -(::log(RandUniform::randValue(n)));
}

DoubleGenMat
RandExp::randValue(unsigned nr, unsigned nc) 
{
   return -(::log(RandUniform::randValue(nr,nc)));
}

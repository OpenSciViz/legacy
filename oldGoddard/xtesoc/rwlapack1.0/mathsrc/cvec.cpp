/*
 * Definitions for DComplexVec
 *
 * Generated from template $Id: xvec.cpp,v 1.9 1993/10/04 21:44:01 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include <rw/strstrea.h>  /* I use istrstream to parse character string ctor */
#include "rw/cvec.h"
#include "rw/rwzbla.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"
#include "rw/rand.h"
#include "rw/rwdbla.h"

RCSID("$Header: /users/rcs/mathsrc/xvec.cpp,v 1.9 1993/10/04 21:44:01 alv Exp $");

DComplexVec::DComplexVec(const char *string)
 : RWVecView()
{
  istrstream s((char*)string);
  scanFrom(s);
  if (!s.good()) {
    RWTHROW( RWExternalErr( RWMessage(RWMATH_STRCTOR,string) ));
  }
}

DComplexVec::DComplexVec(unsigned n, RWRand& r)
 : RWVecView(n,sizeof(DComplex))
{
  while(n--) {
    (*this)(n) = r.complex();
  }
}

DComplexVec::DComplexVec(unsigned n, DComplex scalar)
 : RWVecView(n,sizeof(DComplex))
{
  rwzset(n, data(), 1, &scalar);
}

DComplexVec::DComplexVec(unsigned n, DComplex scalar, DComplex by)
 : RWVecView(n,sizeof(DComplex))
{
  register int i = n;
  DComplex* dp = data();
  DComplex value = scalar;
  DComplex byvalue = by;
  while(i--) {*dp++ = value; value += byvalue;}
}


// Type conversion: real->complex
DComplexVec::DComplexVec(const DoubleVec& re)
  : RWVecView(re.length(),sizeof(DComplex))
{
#ifdef COMPLEX_PACKS
//    static const double zero = 0;     /* "static" required for Glockenspiel */
    double zero = 0;     		     /* above bombs on lucid, so use this nice simple form */
#ifdef IMAG_LEADS
  rwdcopy(npts, (double*)data()+1, 2, re.data(), re.stride());
  rwdset(npts, (double*)data(), 2, &zero);
#else  /* Imaginary does not lead */
  rwdcopy(npts, (double*)data(), 2, re.data(), re.stride());
  rwdset(npts, (double*)data()+1, 2, &zero);
#endif

#else /* Complex does not pack */

  for(int i=length(); i--;) {
    (*this)(i) = re(i);
  }
#endif /* COMPLEX_PACKS */
}  

DComplexVec::DComplexVec(const DoubleVec& re, const DoubleVec& im)
  : RWVecView(re.length(),sizeof(DComplex))
{
  lengthCheck(im.length());
#ifdef COMPLEX_PACKS
#ifdef IMAG_LEADS
  rwdcopy(npts, (double*)data()+1, 2, re.data(), re.stride());
  rwdcopy(npts, (double*)data(), 2, im.data(), im.stride());
#else  /* Imaginary does not lead */
  rwdcopy(npts, (double*)data(), 2, re.data(), re.stride());
  rwdcopy(npts, (double*)data()+1, 2, im.data(), im.stride());
#endif

#else /* Complex does not pack */
  for(int i=length(); i--;) {
    (*this)(i) = DComplex(re(i),im(i));
  }
#endif /* COMPLEX_PACKS */
}


DComplexVec::DComplexVec(const DComplex* dat, unsigned n)
  : RWVecView(n,sizeof(DComplex))
{
  rwzcopy(n, data(), 1, dat, 1);
}

DComplexVec::DComplexVec(RWBlock *block, unsigned n)
  : RWVecView(block,n)
{
  unsigned minlen = n*sizeof(DComplex);
  if (block->length() < minlen) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_SHORTBLOCK, block->length(), minlen) ));
  }
}

DComplexVec&
DComplexVec::operator=(const DComplexVec& rhs)
{
  lengthCheck(rhs.length());
  if (sameDataBlock(rhs)) { return (*this)=rhs.copy(); }  // avoid aliasing problems
  rwzcopy(npts, data(), step, rhs.data(), rhs.step);
  return *this;
}

// Assign to a scalar:
DComplexVec&
DComplexVec::operator=(DComplex scalar)
{
  rwzset(npts, data(), step, &scalar);
  return *this;
}

DComplexVec&
DComplexVec::reference(const DComplexVec& v)
{
  RWVecView::reference(v);
  return *this;
}

// Return copy of self with distinct instance variables
DComplexVec
DComplexVec::copy() const
{
  DComplexVec temp(npts,rwUninitialized);
  rwzcopy(npts, temp.data(), 1, data(), step);
  return temp;
}

// Synonym for copy().  Not made inline because some compilers
// have trouble with inlined temporaries with destructors.
DComplexVec
DComplexVec::deepCopy() const
{
  return copy();
}

// Guarantee that references==1 and stride==1
void
DComplexVec::deepenShallowCopy()
{
  if (!isSimpleView()) {
    RWVecView::reference(copy());
  }
}

/*
 * Reset the length of the vector.  The contents of the old
 * vector are saved.  If the vector has grown, the extra storage
 * is zeroed out.  If the vector has shrunk, it is truncated.
 */
void
DComplexVec::resize(unsigned N)
{
  if(N != npts){
    DComplexVec newvec(N,rwUninitialized);

    // Copy over the minimum of the two lengths:
    unsigned nkeep = npts < N ? npts : N;
    rwzcopy(nkeep, newvec.data(), 1, data(), stride());
    unsigned oldLength= npts;	// Remember the old length

    RWVecView::reference(newvec);

    // If vector has grown, zero out the extra storage:
    if( N > oldLength ){
      const DComplex zero(0,0);
      rwzset((unsigned)(N-oldLength), data()+oldLength, 1, &zero);
    }
  }
}

/*
 * Reset the length of the vector.  The results can be (and
 * probably will be) garbage.
 */
void
DComplexVec::reshape(unsigned N)
{
  if(N != npts) {
    RWVecView::reference(DComplexVec(N,rwUninitialized));
  }
}

DComplexVec DComplexVec::slice(int start, unsigned n, int str) const {
  return (*this)[RWSlice(start,n,str)];
}


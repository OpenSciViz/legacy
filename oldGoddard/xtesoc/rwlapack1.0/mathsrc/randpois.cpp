/*
 * Definitions for class RandPoisson.
 *
 * $Header: /users/rcs/mathsrc/randpois.cpp,v 1.4 1993/09/23 17:15:23 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: randpois.cpp,v $
 * Revision 1.4  1993/09/23  17:15:23  alv
 * ported to NCR
 *
 * Revision 1.3  1993/09/01  16:27:35  alv
 * now uses rwUninitialized form for simple constructors
 *
 * Revision 1.2  1993/01/26  17:52:28  alv
 * removed DOS EOF
 *
 * Revision 1.1  1993/01/22  23:51:03  alv
 * Initial revision
 *
 * 
 *    Rev 1.3   17 Oct 1991 09:15:26   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.2   08 Oct 1991 13:44:48   keffer
 * Math V4.0 beta 2
 * 
 *    Rev 1.1   24 Jul 1991 12:51:54   keffer
 * Added pvcs keywords
 *
 */

/* Definitions for the Poisson Random Number Class: RandPois */

#include "rw/dcomplex.h"  /* So NCR can find M_PI */
#include "rw/dvec.h"
#include "rw/dgenmat.h"
#include "rw/dstats.h"
#include "rw/randpois.h"
STARTWRAP
#include <math.h>
ENDWRAP

RandPoisson::RandPoisson(double m) :
  RandUniform()  
{
  p_mean = m;
  if(p_mean < 12.) { 
     g     = ::exp(-p_mean);
     sq    = 0.;
     lmean = 0.;
  }
  else {
     sq    = ::sqrt(2.*p_mean);
     lmean = ::log(p_mean); 
     g     = p_mean*lmean - logGamma(p_mean + 1.);
  }
}


RandPoisson::RandPoisson(unsigned n, double m) :
	RandUniform(n)  
{
  p_mean = m;
  if(p_mean < 12.) {
     g     = ::exp(-p_mean);
     sq    = 0.;
     lmean = 0.;
  }
  else {
     sq    = ::sqrt(2.*p_mean);
     lmean = ::log(p_mean); 
     g     = p_mean*lmean - logGamma(p_mean + 1.);
  }
}


void
RandPoisson::setPoissonMean(double m)
{
  p_mean = m;
  if(p_mean < 12.) { 
     g     = ::exp(-p_mean);
     sq    = 0.;
     lmean = 0.;
  }
  else {
     sq    = ::sqrt(2.*p_mean);
     lmean = ::log(p_mean); 
     g     = p_mean*lmean - logGamma(p_mean + 1.);
  }
}


// Return a random number selected from an poisson distribution 
// with mean: p_mean

double
RandPoisson::randValue() {
  double em, t;
  if(p_mean < 12.) {
     em = -1.;
     t  = 1.;
     do {
        em += 1.;
        t  *= RandUniform::randValue();
     } while (t > g);
  }
  else {
     double y;
     do {
         do {
            y  = ::tan(M_PI * RandUniform::randValue());
            em = sq*y + p_mean;
         } while (em < 0.);
         em = ::floor(em);
         t  = .9*(1.+y*y)*::exp(em*lmean-logGamma(em+1.)-g);
     } while ( RandUniform::randValue() > t );
  }
  return em;
}

void
RandPoisson::randValue(double* d, unsigned n) 
{
  while (n--) { *d++ = RandPoisson::randValue(); }
}

// Return a vector of n random numbers selected from a poisson distribution
DoubleVec
RandPoisson::randValue(unsigned n) {
  DoubleVec temp(n,rwUninitialized);
  while (n--) temp(n) = RandPoisson::randValue();
  return temp;
}

// Return a matrix of random numbers selected from a poisson distribution 
DoubleGenMat
RandPoisson::randValue(unsigned nr, unsigned nc) {
  DoubleVec tempv = RandPoisson::randValue(nr*nc);
  DoubleGenMat temp(tempv, nr, nc);
  return temp;
}

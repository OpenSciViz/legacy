/*
 * Definitions for DoubleVec
 *
 * Generated from template $Id: xvec.cpp,v 1.9 1993/10/04 21:44:01 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include <rw/strstrea.h>  /* I use istrstream to parse character string ctor */
#include "rw/dvec.h"
#include "rw/rwdbla.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"
#include "rw/rand.h"

RCSID("$Header: /users/rcs/mathsrc/xvec.cpp,v 1.9 1993/10/04 21:44:01 alv Exp $");

DoubleVec::DoubleVec(const char *string)
 : RWVecView()
{
  istrstream s((char*)string);
  scanFrom(s);
  if (!s.good()) {
    RWTHROW( RWExternalErr( RWMessage(RWMATH_STRCTOR,string) ));
  }
}

DoubleVec::DoubleVec(unsigned n, RWRand& r)
 : RWVecView(n,sizeof(Double))
{
  while(n--) {
    (*this)(n) = (double)r();
  }
}

DoubleVec::DoubleVec(unsigned n, double scalar)
 : RWVecView(n,sizeof(Double))
{
  rwdset(n, data(), 1, &scalar);
}

DoubleVec::DoubleVec(unsigned n, double scalar, double by)
 : RWVecView(n,sizeof(Double))
{
  register int i = n;
  double* dp = data();
  double value = scalar;
  double byvalue = by;
  while(i--) {*dp++ = value; value += byvalue;}
}


DoubleVec::DoubleVec(const double* dat, unsigned n)
  : RWVecView(n,sizeof(double))
{
  rwdcopy(n, data(), 1, dat, 1);
}

DoubleVec::DoubleVec(RWBlock *block, unsigned n)
  : RWVecView(block,n)
{
  unsigned minlen = n*sizeof(double);
  if (block->length() < minlen) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_SHORTBLOCK, block->length(), minlen) ));
  }
}

DoubleVec&
DoubleVec::operator=(const DoubleVec& rhs)
{
  lengthCheck(rhs.length());
  if (sameDataBlock(rhs)) { return (*this)=rhs.copy(); }  // avoid aliasing problems
  rwdcopy(npts, data(), step, rhs.data(), rhs.step);
  return *this;
}

// Assign to a scalar:
DoubleVec&
DoubleVec::operator=(double scalar)
{
  rwdset(npts, data(), step, &scalar);
  return *this;
}

DoubleVec&
DoubleVec::reference(const DoubleVec& v)
{
  RWVecView::reference(v);
  return *this;
}

// Return copy of self with distinct instance variables
DoubleVec
DoubleVec::copy() const
{
  DoubleVec temp(npts,rwUninitialized);
  rwdcopy(npts, temp.data(), 1, data(), step);
  return temp;
}

// Synonym for copy().  Not made inline because some compilers
// have trouble with inlined temporaries with destructors.
DoubleVec
DoubleVec::deepCopy() const
{
  return copy();
}

// Guarantee that references==1 and stride==1
void
DoubleVec::deepenShallowCopy()
{
  if (!isSimpleView()) {
    RWVecView::reference(copy());
  }
}

/*
 * Reset the length of the vector.  The contents of the old
 * vector are saved.  If the vector has grown, the extra storage
 * is zeroed out.  If the vector has shrunk, it is truncated.
 */
void
DoubleVec::resize(unsigned N)
{
  if(N != npts){
    DoubleVec newvec(N,rwUninitialized);

    // Copy over the minimum of the two lengths:
    unsigned nkeep = npts < N ? npts : N;
    rwdcopy(nkeep, newvec.data(), 1, data(), stride());
    unsigned oldLength= npts;	// Remember the old length

    RWVecView::reference(newvec);

    // If vector has grown, zero out the extra storage:
    if( N > oldLength ){
      static const double zero = 0;	/* "static" required for Glockenspiel */
      rwdset((unsigned)(N-oldLength), data()+oldLength, 1, &zero);
    }
  }
}

/*
 * Reset the length of the vector.  The results can be (and
 * probably will be) garbage.
 */
void
DoubleVec::reshape(unsigned N)
{
  if(N != npts) {
    RWVecView::reference(DoubleVec(N,rwUninitialized));
  }
}

DoubleVec DoubleVec::slice(int start, unsigned n, int str) const {
  return (*this)[RWSlice(start,n,str)];
}


/* 
 * Random number generator classes implementation
 *
 ****************************************************************************
 *
 * Source code available!  Call or write:
 *
 * Rogue Wave
 * P.O. Box 2328
 * Corvallis, OR 97339
 * Voice: (503) 754-3010
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the laws of
 * the United States and other countries.
 *
 ***************************************************************************
 * 
 * These random number classes are new to math.h++ v5.1.  The old classes
 * are available in math.h++ 5.1 in rand*.h.  They will be phased out.
 *
 *************************************************************************** 
 */

#include <time.h>     /* Use time(0) to seed the global generator */
#include <limits.h>   /* Looking for LONG_MAX */
#include "rw/rand.h"
#include "rw/ref.h"

class RWRandGeneratorState : public RWReference {
private:
  long    *table_;   // A table of length 55, filled with random numbers
  int     index_;    // Index into the table
  long    seed_;     // This was used to start it up
public:
  RWRandGeneratorState(RWReferenceFlag f); // Used to construct the global default generator
  RWRandGeneratorState(const RWRandGeneratorState&); // Makes independent copy
  RWRandGeneratorState();                  // Sets all to zero
  ~RWRandGeneratorState();  
  long   seed()       const  {return seed_;}
  void   restart();
  void   restart(long seed);
  long   operator()();                     // Get a random value
};

/*
 * The defaultGeneratorState is declared up front, because it is used in
 * the restart() function.  The constructor doesn't touch the data: this
 * way things work even if the object is used (by other static constructors)
 * before the constructor is called.
 */
static RWRandGeneratorState defaultGeneratorState(RWReference::STATIC_INIT);

RWRandGeneratorState::RWRandGeneratorState(RWReferenceFlag f) : RWReference(f)
{
}

RWRandGeneratorState::RWRandGeneratorState()
{
  table_ = 0;
  index_ = 0;
  seed_ = 0;
}

RWRandGeneratorState::RWRandGeneratorState(const RWRandGeneratorState& x)
{
  seed_ = x.seed_;
  index_ = x.index_;
  table_ = new long[55];
  for(int i=55; i--;) { table_[i]=x.table_[i]; }
}

RWRandGeneratorState::~RWRandGeneratorState()
{
  delete table_;
}

void RWRandGeneratorState::restart()
{
  if (this == &defaultGeneratorState) restart(time(0));   // avoid recursion
  else restart(defaultGeneratorState());    // don't seed with time(0) in case we start up generators < 1 sec apart
}

void RWRandGeneratorState::restart(long x)
{
  seed_ = x;
  index_ = 0;
  if (table_==0) table_ = new long[55];  // allocate table if need be

  /*
   * Use a linear congruential generator to bootstrap the 
   * additive congruential generator.
   *
   * The first little loop just warms up the generator to be sure
   * that we are in the range where x is taking up all the bits in a long.
   */
  for(int i=0; i<4; i++) { x=x*84589+45989; }  // Warm up the generator a little

  for(i=55; i--;) {
    x = x*84589+45989;
    table_[((21*i)%55)] = x;  // Shuffle order in which table is filled
  }
}

long RWRandGeneratorState::operator()()
{
  if (table_==0) restart();
  index_ = (index_+1) % 55;
  table_[index_] = table_[(index_+23)%55] + table_[(index_+54)%55];
  return table_[index_];
}


RWRandGenerator::RWRandGenerator()
{
  state_ = &defaultGeneratorState;
  defaultGeneratorState.addReference();
}
  
RWRandGenerator::RWRandGenerator(long seed)
{
  state_ = new RWRandGeneratorState;
  restart(seed);
}

RWRandGenerator::RWRandGenerator(const RWRandGenerator& x)
{
  state_ = x.state_;
  state_->addReference();
}
  
RWRandGenerator::~RWRandGenerator()
{
  if (state_->removeReference()==0) delete state_;
}

long RWRandGenerator::operator()()      {return state_->operator()();}
long RWRandGenerator::seed() const      {return state_->seed();}
void RWRandGenerator::restart()         {state_->restart();}
void RWRandGenerator::restart(long seed){state_->restart(seed);}

void RWRandGenerator::reference(const RWRandGenerator& x)
{
  if (state_->removeReference()==0) delete state_;
  state_ = x.state_;
}

void RWRandGenerator::deepenShallowCopy()
{
  if (state_->references()>1 || state_==&defaultGeneratorState) {
    state_->removeReference();
    state_ = new RWRandGeneratorState(*state_);
  }
}


RWRand::RWRand()
{
}

RWRand::RWRand(const RWRandGenerator& x)
 : generator_(x)
{
}

DComplex RWRand::complex()
{
  double arg = RWRandUniform(generator_,-M_PI,M_PI)();
  double norm = (*this)();
  return DComplex( norm*cos(arg), norm*sin(arg) );
}

RWRandGenerator RWRand::generator() const
{
  return generator_;
}

void RWRand::setGenerator(const RWRandGenerator& x)
{
  generator_.reference(x);
}

void RWRand::setGenerator(const RWRand& r)
{
  generator_.reference(r.generator_);
}


RWRandUniform::RWRandUniform()
{
  a_ = 0;
  b_ = 1;
}

RWRandUniform::RWRandUniform(double a, double b)
{
  a_ = a;
  b_ = b;
}

RWRandUniform::RWRandUniform(const RWRandUniform& x)
 : RWRand(x)
{
  a_ = x.a_;
  b_ = x.b_;
}

RWRandUniform::RWRandUniform(const RWRandGenerator& gen, double a, double b)
 : RWRand(gen)
{
  a_ = a;
  b_ = b;
}

double RWRandUniform::operator()()
{
  double r = (double)generator_()/LONG_MAX;  // -1 <= r  <= 1
  double xi = (r+1.0)/2.0;                   // 0  <= xi <= 1
  double v = xi*a_ + (1-xi)*b_;
  if (a_<b_) {                   // check for roundoff in boundary cases
    if (v<a_) v=a_;              // note this is necesssary, in case, eg a_==b_
    if (v>b_) v=b_;
  } else {
    if (v<b_) v=b_;
    if (v>a_) v=a_;
  }
  return v;
}

double RWRandUniform::lowBound() const                  {return rwmin(a_,b_);}
double RWRandUniform::highBound() const                 {return rwmax(a_,b_);}


RWRandNormal::RWRandNormal()
{
  mean_ = 0;
  variance_ = 1;
}

RWRandNormal::RWRandNormal(double mean, double variance)
{
  mean_ = mean;
  variance_ = variance;
}

RWRandNormal::RWRandNormal(const RWRandNormal& x)
 : RWRand(x)
{
  mean_ = x.mean_;
  variance_ = x.variance_;
}

RWRandNormal::RWRandNormal(const RWRandGenerator& gen, double mean, double variance)
 : RWRand(gen)
{
  mean_ = mean;
  variance_ = variance;
}

double RWRandNormal::operator()()
{
  // pick (v1,v2) in the unit circle
  double v1,v2,rsquared;
  do {
     v1 = (double)generator_()/LONG_MAX;   // -1 <= v1 <= 1
     v2 = (double)generator_()/LONG_MAX;   // -1 <= v2 <= 1
     rsquared = v1*v1 + v2*v2;
  } while ( rsquared>=1. || rsquared==0.0);
  return mean_+variance_*v1*sqrt(-2.*::log(rsquared)/rsquared);  // See numerical recipes
}

double RWRandNormal::lowBound() const   {return mean_-2.58*variance_;}
double RWRandNormal::highBound() const  {return mean_+2.58*variance_;}


RWRandExponential::RWRandExponential(double lambda)
{
  lambda_ = lambda;
}

RWRandExponential::RWRandExponential(const RWRandExponential& x)
 : RWRand(x)
{
  lambda_ = x.lambda_;
}

RWRandExponential::RWRandExponential(const RWRandGenerator& gen, double lambda)
 : RWRand(gen)
{
  lambda_ = lambda;
}

double RWRandExponential::operator()()
{
  double r = (double)generator_()/LONG_MAX;  // -1 <= r  <= 1
  double xi = (r+1.0)/2.0;                   // 0  <= xi <= 1
  return -log(xi)/lambda_;
}

double RWRandExponential::lowBound() const {return 0;}
double RWRandExponential::highBound() const{return 4.605170219/lambda_;}  // ln(0.01)=4.6...

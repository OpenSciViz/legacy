> #
> # $Header: /users/rcs/mathsrc/xrwbla.c,v 1.3 1993/10/11 18:42:24 alv Exp $
> #
> # $Log: xrwbla.c,v $
> # Revision 1.3  1993/10/11  18:42:24  alv
> # removed dependency on [cz]_math.c, which is not included in LAPACK.h++
> #
> # Revision 1.3  1993/10/11  18:28:43  alv
> # removed dependency on z_math.c, which is not included in LAPACK.h++
> #
> # Revision 1.2  1993/03/22  15:51:31  alv
> # changed PVCS Workfile keyword to RCS ID keyword
> #
> # Revision 1.1  1993/01/22  23:51:16  alv
> # Initial revision
> #
> # 
> #    Rev 1.2   17 Oct 1991 09:15:02   keffer
> # Changed include path to <rw/xxx.h>
> # 
> #    Rev 1.0   27 Sep 1991 11:52:12   keffer
> # Initial revision.
> #
> include macros
/*
 * BLA-like extensions for type <t>.
 *
 * Generated from template $Id: xrwbla.c,v 1.3 1993/10/11 18:42:24 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/rw<bla>bla.h"
#include "rw/mathdefs.h"

#include <math.h>
#ifdef BSD
#  include <memory.h>
#else
#  include <string.h>
#endif

> if <R/C>==Complex
/*
 * Divide the DComplex pointed to by a1 by the DComplex pointed to
 * by a2, put the results in result.  There are no restrictions
 * on a1, a2, and result pointing to different data storage.
 */
#ifdef RW_KR_ONLY
static void <bla>_div(result, a1, a2)
  <t> *result, *a1, *a2;
#else
static void <bla>_div(<t> *result, const <t> *a1, const <t> *a2)
#endif
{
  <p> p, q, r, i;

  r = a2->r;
  i = a2->i;
  p = r < 0 ? -r : r;
  q = i < 0 ? -i : i;

  if (p <= q) {
    q = r/i;
    p = (q*q + 1) * i;
    r = a1->r;
    i = a1->i;
  }
  else {
    q = -i/r;
    p = (q*q + 1) * r;
    r = -a1->i;
    i = a1->r;
  }
  result->r = (r*q + i)/p;
  result->i = (i*q - r)/p;
}

> endif
/****************************************************************
 *								*
 *			UTILITY					*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rw<bla>copy(n, x, incx, y, incy )
  unsigned n;
  <t> *x, *y;
  int incx, incy;
#else
void FDecl rw<bla>copy( unsigned n, <t>* restrict x, int incx, const <t>* restrict y, int incy)
#endif
{
  if (n==0) return;

  if (incx == 1 && incy == 1) {
    memcpy(x, y, n*sizeof(<t>));
  }
  else {
    /*
     * Code for increments not equal to one
     */
    while (n--) {
> if <R/C>==Complex
#ifdef HAS_NATIVE_COMPLEX
      *x = *y;
#else
      x->r = y->r;  x->i = y->i;
#endif
> else
      *x = *y;
> endif
      x += incx;
      y += incy;
    }
  }
}

#ifdef RW_KR_ONLY
void rw<bla>set(n, x, incx, scalar)
  unsigned n;
  <t> *x, *scalar;
  int incx;
#else
void FDecl rw<bla>set(unsigned n, <t>* restrict x, int incx, const <t>* restrict scalar)
#endif
{
> if <R/C>==Complex
#ifdef HAS_NATIVE_COMPLEX
  if(incx==1)
    while (n--) { *x++ = *scalar; }
  else
    while (n--) { *x = *scalar; x+=incx; }
#else
  if(incx==1)
    while (n--) { x->r = scalar->r; x->i = scalar->i; x++; }
  else
    while (n--) { x->r = scalar->r; x->i = scalar->i; x+=incx; }
#endif
> else
  if(incx==1)
    while (n--) { *x++ = *scalar; }
  else
    while (n--) { *x = *scalar; x+=incx; }
> endif
}

#ifdef RW_KR_ONLY
int rw<bla>same(n, x, incx, y, incy)
  unsigned n;
  <t> *x, *y;
  int incx, incy;
#else
int FDecl rw<bla>same(unsigned n, const <t>* restrict x, int incx, const <t>* restrict y, int incy)
#endif
{
  while(n--){
> if <R/C>==Complex
#ifdef HAS_NATIVE_COMPLEX
    if( *x != *y ) return 0;
#else
    if( x->r != y->r || x->i != y->i ) return 0;
#endif
> else
    if( *x != *y ) return 0;
> endif
    x += incx;
    y += incy;
  }
  return 1;
}

> if <R/C>!=Complex
#ifdef RW_KR_ONLY
int rw<bla>max(n, x, incx)
  unsigned n;
  <t> *x;
  int incx;
#else
int FDecl rw<bla>max(unsigned n, const <t>* restrict x, int incx)
#endif
{
  <t> maxv;
  int maxi, i;

  if(n==0) return -1;
  maxv = *x;
  maxi = 0;

  for(i=1; i<n; i++){
    x += incx;
    if(*x > maxv) { maxv = *x; maxi=i; }
  }
  return maxi;
}

#ifdef RW_KR_ONLY
int rw<bla>min(n, x, incx)
  unsigned n;
  <t> *x;
  int incx;
#else
int FDecl rw<bla>min(unsigned n, const <t>* restrict x, int incx)
#endif
{
  <t> minv;
  int mini, i;

  if(n==0) return -1;
  minv = *x;
  mini = 0;

  for(i=1; i<n; i++){
    x += incx;
    if(*x < minv) { minv = *x; mini=i; }
  }
  return mini;
}
> endif

/****************************************************************
 *								*
 *			DOT PRODUCTS				*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rw<bla>dot(n, x, incx, y, incy, result)
  unsigned n;
  <t> *x, *y, *result;
  int incx, incy;
#else
void FDecl rw<bla>dot(unsigned n, const <t>* restrict x, int incx, const <t>* restrict y, int incy, <t>* result)
#endif
{
> if <R/C>==Complex
  double sumr = 0;	/* Always accumulate in double precision */
  double sumi = 0;
  while(n--){
    sumr += x->r * y->r - x->i * y->i;
    sumi += x->r * y->i + x->i * y->r;
    x += incx;
    y += incy;
  }
  result->r = sumr;
  result->i = sumi;

> else

>   if <t>==float
  double sum = 0;		/* Accumulate in double precision */
>   else
  <t> sum = 0;
>   endif
  while(n--){
    sum += *x * *y;
    x += incx;
    y += incy;
  }
  *result = (<t>)sum;
> endif
}

> if <R/C>==Complex
/*
 * Conjugate dot product:
 */

#ifdef RW_KR_ONLY
void rw<bla>cdot(n, x, incx, y, incy, result)
  unsigned n;
  <t> *x, *y, *result;
  int incx, incy;
#else
void FDecl rw<bla>cdot(unsigned n, const <t>* restrict x, int incx, const <t>* restrict y, int incy, <t>* result)
#endif
{
  double sumr = 0;	/* Always accumulate in double precision */
  double sumi = 0;
  while(n--){
    sumr += x->r * y->r + x->i * y->i;
    sumi += x->r * y->i - x->i * y->r;
    x += incx;
    y += incy;
  }
  result->r = sumr;
  result->i = sumi;
}
> endif

/****************************************************************
 *								*
 *		VECTOR --- VECTOR ASSIGNMENT OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rw<bla>_aplvv(n, x, incx, y, incy)
  unsigned n;
  <t> *x, *y;
  int incx, incy;
#else
void FDecl rw<bla>_aplvv(unsigned n, <t>* restrict x, int incx, const <t>* restrict y, int incy)
#endif
{
> if <R/C>==Complex
#ifdef HAS_NATIVE_COMPLEX
  while(n--){
    *x += *y;
    x += incx;
    y += incy;
  }
#else
  while(n--){
    x->r += y->r;
    x->i += y->i;
    x += incx;
    y += incy;
  }
#endif
> else
  while(n--){
    *x += *y;
    x += incx;
    y += incy;
  }
> endif
}

#ifdef RW_KR_ONLY
void rw<bla>_amivv(n, x, incx, y, incy)
  unsigned n;
  <t> *x, *y;
  int incx, incy;
#else
void FDecl rw<bla>_amivv(unsigned n, <t>* restrict x, int incx, const <t>* restrict y, int incy)
#endif
{
> if <R/C>==Complex
#ifdef HAS_NATIVE_COMPLEX
  while(n--){
    *x -= *y;
    x += incx;
    y += incy;
  }
#else
  while(n--){
    x->r -= y->r;
    x->i -= y->i;
    x += incx;
    y += incy;
  }
#endif
> else
  while(n--){
    *x -= *y;
    x += incx;
    y += incy;
  }
> endif
}

#ifdef RW_KR_ONLY
void rw<bla>_amuvv(n, x, incx, y, incy)
  unsigned n;
  <t> *x, *y;
  int incx, incy;
#else
void FDecl rw<bla>_amuvv(unsigned n, <t>* restrict x, int incx, const <t>* restrict y, int incy)
#endif
{
> if <R/C>==Complex
#ifdef HAS_NATIVE_COMPLEX
  while(n--){
    *x *= *y;
    x += incx;
    y += incy;
  }
#else
  register <p> re;
  while(n--){
    re = x->r;
    x->r = re * y->r - x->i * y->i;
    x->i = re * y->i + x->i * y->r;
    x += incx;
    y += incy;
  }
#endif
> else
  while(n--){
    *x *= *y;
    x += incx;
    y += incy;
  }
> endif
}

#ifdef RW_KR_ONLY
void rw<bla>_advvv(n, x, incx, y, incy)
  unsigned n;
  <t> *x, *y;
  int incx, incy;
#else
void FDecl rw<bla>_advvv(unsigned n, <t>* restrict x, int incx, const <t>* restrict y, int incy)
#endif
{
> if <R/C>==Complex
#ifdef HAS_NATIVE_COMPLEX
  while(n--){
    *x /= *y;
    x += incx;
    y += incy;
  }
#else
  while(n--){
    <bla>_div(x,x,y);
    x += incx;
    y += incy;
  }
#endif
> else
  while(n--){
    *x /= *y;
    x += incx;
    y += incy;
  }
> endif
}

/****************************************************************
 *								*
 *		VECTOR --- SCALAR ASSIGNMENT OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rw<bla>_aplvs(n, x, incx, scalar)
  unsigned n;
  <t> *x, *scalar;
  int incx;
#else
void FDecl rw<bla>_aplvs(unsigned n, <t>* restrict x, int incx, const <t>* restrict scalar)
#endif
{
> if <R/C>==Complex
#ifdef HAS_NATIVE_COMPLEX
  while(n--) {
    *x += *scalar;
    x += incx;
  }
#else
  while(n--) {
    x->r += scalar->r;
    x->i += scalar->i;
    x += incx;
  }
#endif
> else
  while(n--) {
    *x += *scalar;
    x += incx;
  }
> endif
}

#ifdef RW_KR_ONLY
void rw<bla>_amuvs(n, x, incx, scalar)
  unsigned n;
  <t> *x, *scalar;
  int incx;
#else
void FDecl rw<bla>_amuvs(unsigned n, <t>* restrict x, int incx, const <t>* restrict scalar)
#endif
{
> if <R/C>==Complex
#ifdef HAS_NATIVE_COMPLEX
  while(n--) {
    *x *= *scalar;
    x += incx;
  }
#else
  register <p> re;
  while(n--) {
    re = x->r;
    x->r = re * scalar->r - x->i * scalar->i;
    x->i = re * scalar->i + x->i * scalar->r;
    x += incx;
  }
#endif
> else
  while(n--) {
    *x *= *scalar;
    x += incx;
  }
> endif
}

> # This operation can be done in terms of * (1/scalar) for float and double,
> # and so is not necessary for these types:
> if <t>!=float && <t>!=double
>
#ifdef RW_KR_ONLY
void rw<bla>_advvs(n, x, incx, scalar)
  unsigned n;
  <t> *x, *scalar;
  int incx;
#else
void FDecl rw<bla>_advvs(unsigned n, <t>* restrict x, int incx, const <t>* restrict scalar)
#endif
{
> if <R/C>==Complex
#ifdef HAS_NATIVE_COMPLEX
  while(n--) {
    *x /= *scalar;
    x += incx;
  }
#else
  while(n--) {
    <bla>_div(x,x,scalar);
    x += incx;
  }
#endif
> else
  while(n--) {
    *x /= *scalar;
    x += incx;
  }
> endif
}
> endif

/****************************************************************
 *								*
 *		VECTOR --- VECTOR BINARY OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rw<bla>_plvv(n, z, x, incx, y, incy)
  unsigned n;
  <t> *z, *x, *y;
  int incx, incy;
#else
void FDecl rw<bla>_plvv(unsigned n, <t>* restrict z, const <t>* restrict x, int incx, const <t>* restrict y, int incy)
#endif
{
  while(n--){
> if <R/C>==Complex
#ifdef HAS_NATIVE_COMPLEX
    *z++ = *x + *y;
#else
    z->r = x->r + y->r;
    z->i = x->i + y->i;
    z++;
#endif
> else
    *z++ = *x + *y;
> endif
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rw<bla>_mivv(n, z, x, incx, y, incy)
  unsigned n;
  <t> *z, *x, *y;
  int incx, incy;
#else
void FDecl rw<bla>_mivv(unsigned n, <t>* restrict z, const <t>* restrict x, int incx, const <t>* restrict y, int incy)
#endif
{
  while(n--){
> if <R/C>==Complex
#ifdef HAS_NATIVE_COMPLEX
    *z++ = *x - *y;
#else
    z->r = x->r - y->r;
    z->i = x->i - y->i;
    z++;
#endif
> else
    *z++ = *x - *y;
> endif
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rw<bla>_muvv(n, z, x, incx, y, incy)
  unsigned n;
  <t> *z, *x, *y;
  int incx, incy;
#else
void FDecl rw<bla>_muvv(unsigned n, <t>* restrict z, const <t>* restrict x, int incx, const <t>* restrict y, int incy)
#endif
{
  while(n--){
> if <R/C>==Complex
#ifdef HAS_NATIVE_COMPLEX
    *z++ = *x * *y;
#else
    z->r = x->r * y->r - x->i * y->i;
    z->i = x->r * y->i + x->i * y->r;
    z++;
#endif
> else
    *z++ = *x * *y;
> endif
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rw<bla>_dvvv(n, z, x, incx, y, incy)
  unsigned n;
  <t> *z, *x, *y;
  int incx, incy;
#else
void FDecl rw<bla>_dvvv(unsigned n, <t>* restrict z, const <t>* restrict x, int incx, const <t>* restrict y, int incy)
#endif
{
  while(n--){
> if <R/C>==Complex
#ifdef HAS_NATIVE_COMPLEX
    *z++ = *x / *y;
#else
    <bla>_div(z++,x,y);
#endif
> else
    *z++ = *x / *y;
> endif
    x += incx;
    y += incy;
  }
}

/****************************************************************
 *								*
 *		VECTOR --- SCALAR BINARY OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rw<bla>_plvs(n, z, x, incx, scalar)
  unsigned n;
  <t> *z, *x, *scalar;
  int incx;
#else
void FDecl rw<bla>_plvs(unsigned n, <t>* restrict z, const <t>* restrict x, int incx, const <t>* restrict scalar)
#endif
{
  while(n--){
> if <R/C>==Complex
#ifdef HAS_NATIVE_COMPLEX
    *z++ = *x + *scalar;
#else
    z->r = x->r + scalar->r;
    z->i = x->i + scalar->i;
	z++;
#endif
> else
    *z++ = *x + *scalar;
> endif
    x += incx;
  }
}

#ifdef RW_KR_ONLY
void rw<bla>_muvs(n, z, x, incx, scalar)
  unsigned n;
  <t> *z, *x, *scalar;
  int incx;
#else
void FDecl rw<bla>_muvs(unsigned n, <t>* restrict z, const <t>* restrict x, int incx, const <t>* restrict scalar)
#endif
{
  while(n--){
> if <R/C>==Complex
#ifdef HAS_NATIVE_COMPLEX
    *z++ = *x * *scalar;
#else
    z->r = x->r * scalar->r - x->i * scalar->i;
    z->i = x->r * scalar->i + x->i * scalar->r;
	z++;
#endif
> else
    *z++ = *x * *scalar;
> endif
    x += incx;
  }
}

#ifdef RW_KR_ONLY
void rw<bla>_dvvs(n, z, x, incx, scalar)
  unsigned n;
  <t> *z, *x, *scalar;
  int incx;
#else
void FDecl rw<bla>_dvvs(unsigned n, <t>* restrict z, const <t>* restrict x, int incx, const <t>* restrict scalar)
#endif
{
  while(n--){
> if <R/C>==Complex
#ifdef HAS_NATIVE_COMPLEX
    *z++ = *x / *scalar;
#else
    <bla>_div(z++,x,scalar);
#endif
> else
    *z++ = *x / *scalar;
> endif
    x += incx;
  }
}

/****************************************************************
 *								*
 *		SCALAR --- VECTOR BINARY OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rw<bla>_misv(n, z, scalar, x, incx)
  unsigned n;
  <t> *z, *scalar, *x;
  int incx;
#else
void FDecl rw<bla>_misv(unsigned n, <t>* restrict z, const <t>* scalar, const <t>* restrict x, int incx)
#endif
{
  while(n--){
> if <R/C>==Complex
#ifdef HAS_NATIVE_COMPLEX
    *z++ = *scalar - *x;
#else
    z->r = scalar->r - x->r;
    z->i = scalar->i - x->i;
	z++;
#endif
> else
    *z++ = *scalar - *x;
> endif
    x += incx;
  }
}

#ifdef RW_KR_ONLY
void rw<bla>_dvsv(n, z, scalar, x, incx)
  unsigned n;
  <t> *z, *scalar, *x;
  int incx;
#else
void FDecl rw<bla>_dvsv(unsigned n, <t>* restrict z, const <t>* scalar, const <t>* restrict x, int incx)
#endif
{
  while(n--){
> if <R/C>==Complex
#ifdef HAS_NATIVE_COMPLEX
    *z++ = *scalar / *x;
#else
    <bla>_div(z++,scalar,x);
#endif
> else
    *z++ = *scalar / *x;
> endif
    x += incx;
  }
}


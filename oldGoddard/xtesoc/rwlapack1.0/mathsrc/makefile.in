###################################################################
#
#		M A K E F I L E
#
#	Makefile template for Rogue Wave's Math.h++
#
#	This file is normally processed by the shell script
#	"config" to create the actual makefile
#
#	Copyright (C) 1989 - 1993
#
#	Rogue Wave Software, Inc.
#	P.O. Box 2328
#	Corvallis, OR 97339
#
###################################################################
#
#       Things to check:
#
#       FPFLAG:		Compiler floating point flags
#	FPNAME:		The library suffix reflecting floating point flag
#	DBG:		Uncomment to build a debug version
#       CPPOPTIONS:	Modify as necessary for production version
#	LIBINST:	Where to install finished library
#	INCLINST:	Where to install header files
#
###################################################################

#		Uncomment the next two lines if you want to build an
#		inline floating point version of the library.
#		Set FPFLAG to whatever flag your compiler uses.
#		Set FPNAME to a mnemonic for this flag.
#FPFLAG=	-f68881
#FPNAME=	81

#		Uncomment the next two lines if you want to build a
#		"debug" version of the library.
#DBG=		-DRWDEBUG
#DBGNAME=	g

#		Additional C++ compile options:
#		Options to be passed to your C++ compiler
CPPOPTIONS=	-O

#		Same as above, except for C files:
COPTIONS=	-O

#		Directory where the finished library should be installed:
#		(Make sure it exists)
LIBINST=	/usr/local/lib

#		Directory where the header files should be installed:
#		(Make sure it exists)
INCLINST=	/usr/local/include/rw

#		C++ compiler name:
CPP=		CC

##################################################################
#	From here on, it's pretty much boilerplate

#		Where the library should be built:
LIBDIR=		../lib

#		Rogue Wave header file include path:
RWINCL=		..

#		Rogue Wave header file directory:
RWDIR=		../rw

#               Base name of the library:
#		Prefix depends on the compiler:
LIBPREFIX=	librw
LIBNAME=	$(LIBPREFIX)mth$(MEMMODEL)$(WINNAME)$(DBGNAME)$(FPNAME)

#		Root names (excluding suffix) of the library:
LIBROOT=        $(LIBDIR)/$(LIBNAME)
#		Full name (including suffix) of the library:
LIBFULL=	$(LIBDIR)/$(LIBNAME).a

#		Compile flags for the library:
CPPFLAGS= 	-I$(RWINCL) $(CPPOPTIONS) $(FPFLAG) $(DBG)
CFLAGS=		-I$(RWINCL) $(COPTIONS)   $(FPFLAG) $(DBG)

#		Extension used by this compiler:
EXT=		C

#		Any special flags used by this compiler:
SPECIAL=

#
#		Preprocessor name:
MATHPP=		../bin/mathpp

#		Any programs we need:
RM=		rm -f junk
MV=		mv
COPY=		copy
SHELL=		/bin/sh
#########################  Object code ######################################
#
#	Math.h++ object files:
MATHOBJ1=	dfft.o     cfft2d.o   dcosfft.o  cfft.o  
MATHOBJ2=	dstats.o   histo.o    rand.o
MATHOBJ3=	lsqfit.o   randbino.o randexp.o  randfunc.o
MATHOBJ4=	randgama.o randnorm.o randpois.o randunif.o
MATHERR=   matherr.o coreerr.o
MATHOBJ=	$(MATHOBJ1) $(MATHOBJ2) $(MATHOBJ3) $(MATHOBJ4) $(MATHERR)

#	Core.h++ object files:
COREOBJ1=	bstream.o mempool.o    pstream.o  message.o
COREOBJ2=	rwerr.o   rwfile.o     utility.o  vstream.o
COREOBJ=	$(COREOBJ1) $(COREOBJ2)

#	Array.h++ object files:
ARRAYOBJ1=  vector.o   fvecx.o    ivecx.o    svecx.o    uvecx.o 
ARRAYOBJ2=  matrix.o   fmatx.o    imatx.o    smatx.o    umatx.o 
ARRAYOBJ3=  array.o    farrx.o    iarrx.o    sarrx.o    uarrx.o
ARRAYOBJ4=  dataview.o rwslice.o 
ARRAYOBJ=	$(ARRAYOBJ1) $(ARRAYOBJ2) $(ARRAYOBJ3) $(ARRAYOBJ4)

#	Rogue Wave BLA extensions:
RWBLAOBJ1=	arwbla.o brwbla.o crwbla.o
RWBLAOBJ2=	irwbla.o zrwbla.o
RWBLAOBJ3= drwbla.o srwbla.o
RWBLAOBJ=	$(RWBLAOBJ1) $(RWBLAOBJ2) $(RWBLAOBJ3)

#	Complex number extra functions (may be nothing in this file if
#	using compiler supplied complex)
ZOBJS=	rccfun.o

###################  Derived source code ##################################

#	Rogue Wave BLA extensions:
RWBLAC1=	arwbla.c     brwbla.c     crwbla.c      drwbla.c
RWBLAC2=	irwbla.c     srwbla.c     zrwbla.c

VECFUNCPP=   drmath.cpp   ccmath.cpp   frmath.cpp
VECFUNOBJ=   drmath.o   ccmath.o   frmath.o
VECCOMPLEXCPP= cvec.cpp cvecio.cpp cvecmath.cpp cvecop.cpp cvecpik.cpp 
VECCOMPLEXOBJ= cvec.o cvecio.o cvecmath.o cvecop.o cvecpik.o 
VECDOUBLECPP=  dvec.cpp dvecio.cpp dvecmath.cpp dvecop.cpp dvecpik.cpp 
VECDOUBLEOBJ=  dvec.o dvecio.o dvecmath.o dvecop.o dvecpik.o 
VECFLOATCPP=   fvec.cpp fvecio.cpp fvecmath.cpp fvecop.cpp fvecpik.cpp 
VECFLOATOBJ=   fvec.o fvecio.o fvecmath.o fvecop.o fvecpik.o 
VECINTCPP=     ivec.cpp ivecio.cpp ivecmath.cpp ivecop.cpp ivecpik.cpp 
VECINTOBJ=     ivec.o ivecio.o ivecmath.o ivecop.o ivecpik.o 
VECSCHARCPP=   svec.cpp svecio.cpp svecmath.cpp svecop.cpp svecpik.cpp 
VECSCHAROBJ=   svec.o svecio.o svecmath.o svecop.o svecpik.o 
VECUCHARCPP=   uvec.cpp uvecio.cpp uvecmath.cpp uvecop.cpp uvecpik.cpp 
VECUCHAROBJ=   uvec.o uvecio.o uvecmath.o uvecop.o uvecpik.o 
MATCOMPLEXCPP= cmat.cpp cmatio.cpp cmatmath.cpp cmatop.cpp cgenpik.cpp 
MATCOMPLEXOBJ= cmat.o cmatio.o cmatmath.o cmatop.o cgenpik.o 
MATDOUBLECPP=  dmat.cpp dmatio.cpp dmatmath.cpp dmatop.cpp dgenpik.cpp 
MATDOUBLEOBJ=  dmat.o dmatio.o dmatmath.o dmatop.o dgenpik.o 
MATFLOATCPP=   fmat.cpp fmatio.cpp fmatmath.cpp fmatop.cpp fgenpik.cpp 
MATFLOATOBJ=   fmat.o fmatio.o fmatmath.o fmatop.o fgenpik.o 
MATINTCPP=     imat.cpp imatio.cpp imatmath.cpp imatop.cpp igenpik.cpp 
MATINTOBJ=     imat.o imatio.o imatmath.o imatop.o igenpik.o 
MATSCHARCPP=   smat.cpp smatio.cpp smatmath.cpp smatop.cpp sgenpik.cpp 
MATSCHAROBJ=   smat.o smatio.o smatmath.o smatop.o sgenpik.o 
MATUCHARCPP=   umat.cpp umatio.cpp umatmath.cpp umatop.cpp ugenpik.cpp 
MATUCHAROBJ=   umat.o umatio.o umatmath.o umatop.o ugenpik.o 
ARRCOMPLEXCPP= carr.cpp carrio.cpp carrmath.cpp carrop.cpp carrsub.cpp 
ARRCOMPLEXOBJ= carr.o carrio.o carrmath.o carrop.o carrsub.o 
ARRDOUBLECPP=  darr.cpp darrio.cpp darrmath.cpp darrop.cpp darrsub.cpp 
ARRDOUBLEOBJ=  darr.o darrio.o darrmath.o darrop.o darrsub.o 
ARRFLOATCPP=   farr.cpp farrio.cpp farrmath.cpp farrop.cpp farrsub.cpp 
ARRFLOATOBJ=   farr.o farrio.o farrmath.o farrop.o farrsub.o 
ARRINTCPP=     iarr.cpp iarrio.cpp iarrmath.cpp iarrop.cpp iarrsub.cpp 
ARRINTOBJ=     iarr.o iarrio.o iarrmath.o iarrop.o iarrsub.o 
ARRSCHARCPP=   sarr.cpp sarrio.cpp sarrmath.cpp sarrop.cpp sarrsub.cpp 
ARRSCHAROBJ=   sarr.o sarrio.o sarrmath.o sarrop.o sarrsub.o 
ARRUCHARCPP=   uarr.cpp uarrio.cpp uarrmath.cpp uarrop.cpp uarrsub.cpp 
ARRUCHAROBJ=   uarr.o uarrio.o uarrmath.o uarrop.o uarrsub.o 

OBJECTS=        $(COREOBJ)       $(ARRAYOBJ)      $(BLAOBJ)        \
		$(RWBLAOBJ)      $(ZOBJS)                          \
		$(VECFUNOBJ)                                       \
		$(VECDOUBLEOBJ)  $(MATDOUBLEOBJ)  $(ARRDOUBLEOBJ)  \
		$(VECFLOATOBJ)   $(MATFLOATOBJ)   $(ARRFLOATOBJ)   \
		$(VECINTOBJ)     $(MATINTOBJ)     $(ARRINTOBJ)     \
		$(VECCOMPLEXOBJ) $(MATCOMPLEXOBJ) $(ARRCOMPLEXOBJ) \
		$(VECUCHAROBJ)   $(MATUCHAROBJ)   $(ARRUCHAROBJ)   \
		$(VECSCHAROBJ)   $(MATSCHAROBJ)   $(ARRSCHAROBJ)   \
		$(MATHOBJ)       $(LPAKCOBJ)      $(LPAKCPPOBJ)

SOURCES=	$(RWBLAC1)       $(RWBLAC2)       $(RWBLAASM)      \
		$(VECFUNCPP)                                       \
		$(VECCOMPLEXCPP) $(MATCOMPLEXCPP) $(ARRCOMPLEXCPP) \
		$(VECUCHARCPP)   $(MATUCHARCPP)   $(ARRUCHARCPP)   \
		$(VECSCHARCPP)   $(MATSCHARCPP)   $(ARRSCHARCPP)   \
		$(VECDOUBLECPP)  $(MATDOUBLECPP)  $(ARRDOUBLECPP)  \
		$(VECFLOATCPP)   $(MATFLOATCPP)   $(ARRFLOATCPP)   \
		$(VECINTCPP)     $(MATINTCPP)     $(ARRINTCPP)

###################  Derived Header files ##################################

HEADERS=	$(RWDIR)/dvec.h    $(RWDIR)/cvec.h     $(RWDIR)/fvec.h     \
		$(RWDIR)/ivec.h    $(RWDIR)/scvec.h    $(RWDIR)/ucvec.h    \
		$(RWDIR)/dvecpik.h $(RWDIR)/cvecpik.h  $(RWDIR)/fvecpik.h  \
		$(RWDIR)/ivecpik.h $(RWDIR)/scvecpik.h $(RWDIR)/ucvecpik.h \
		$(RWDIR)/dgenmat.h $(RWDIR)/cgenmat.h  $(RWDIR)/fgenmat.h  \
		$(RWDIR)/igenmat.h $(RWDIR)/scgenmat.h $(RWDIR)/ucgenmat.h \
		$(RWDIR)/dgenpik.h $(RWDIR)/cgenpik.h  $(RWDIR)/fgenpik.h  \
		$(RWDIR)/igenpik.h $(RWDIR)/scgenpik.h $(RWDIR)/ucgenpik.h \
		$(RWDIR)/darr.h    $(RWDIR)/carr.h     $(RWDIR)/farr.h     \
		$(RWDIR)/iarr.h    $(RWDIR)/scarr.h    $(RWDIR)/ucarr.h

##############################   Targets    #################################

all:		headers sources lib
headers:	$(HEADERS)
sources:	$(SOURCES)
objects:	$(OBJECTS)
lib:		$(LIBFULL)

$(LIBFULL):	$(OBJECTS)
	-if [ ! -d ${LIBDIR} ] ; then mkdir ${LIBDIR} ;fi
	ar rvu ${LIBFULL} $?
	if [ -f /usr/bin/ranlib -o -f /bin/ranlib ] ; \
		then ranlib ${LIBFULL} ;fi

install:	${LIBFULL}
	-if [ ! -d ${LIBINST} ] ; then mkdir ${LIBINST} ;fi
	cp ${LIBFULL} ${LIBINST}
	if [ -f /usr/bin/ranlib -o -f /bin/ranlib ] ; \
		then ranlib ${LIBINST}/${LIBNAME}.a ;fi
	-if [ ! -d ${INCLINST} ] ; then mkdir ${INCLINST} ;fi
	cp ${RWDIR}/*.h ${INCLINST}

clean:
	$(RM) *.o
	$(RM) *.C
	$(RM) *..dbx

realclean:	clean
	$(RM) $(RWBLAC1)
	$(RM) $(RWBLAC2)
	$(RM) $(VECFUNCPP)
	$(RM) $(VECCOMPLEXCPP)
	$(RM) $(VECDOUBLECPP)
	$(RM) $(VECFLOATCPP)
	$(RM) $(VECINTCPP)
	$(RM) $(VECSCHARCPP)
	$(RM) $(VECUCHARCPP)
	$(RM) $(MATCOMPLEXCPP)
	$(RM) $(MATDOUBLECPP)
	$(RM) $(MATFLOATCPP)
	$(RM) $(MATINTCPP)
	$(RM) $(MATSCHARCPP)
	$(RM) $(MATUCHARCPP)
	$(RM) $(ARRCOMPLEXCPP)
	$(RM) $(ARRDOUBLECPP)
	$(RM) $(ARRFLOATCPP)
	$(RM) $(ARRINTCPP)
	$(RM) $(ARRSCHARCPP)
	$(RM) $(ARRUCHARCPP)
	$(RM) *.msl     
	$(RM) junk*
		
cleanheaders:
	$(RM) $(RWDIR)/dvec.h    $(RWDIR)/cvec.h     $(RWDIR)/fvec.h
	$(RM) $(RWDIR)/ivec.h    $(RWDIR)/scvec.h    $(RWDIR)/ucvec.h
	$(RM) $(RWDIR)/dvecpik.h $(RWDIR)/cvecpik.h  $(RWDIR)/fvecpik.h
	$(RM) $(RWDIR)/ivecpik.h $(RWDIR)/scvecpik.h $(RWDIR)/ucvecpik.h
	$(RM) $(RWDIR)/dgenmat.h $(RWDIR)/cgenmat.h  $(RWDIR)/fgenmat.h
	$(RM) $(RWDIR)/igenmat.h $(RWDIR)/scgenmat.h $(RWDIR)/ucgenmat.h
	$(RM) $(RWDIR)/dgenpik.h $(RWDIR)/cgenpik.h  $(RWDIR)/fgenpik.h
	$(RM) $(RWDIR)/igenpik.h $(RWDIR)/scgenpik.h $(RWDIR)/ucgenpik.h
	$(RM) $(RWDIR)/darr.h    $(RWDIR)/carr.h     $(RWDIR)/farr.h
	$(RM) $(RWDIR)/iarr.h    $(RWDIR)/scarr.h    $(RWDIR)/ucarr.h
	
###################   Template Expansions --- headers   #################

$(RWDIR)/dvec.h: vector.htm
	$(MATHPP) vector.htm $@ RW_TYPE_RW=double

$(RWDIR)/fvec.h: vector.htm
	$(MATHPP) vector.htm $@ RW_TYPE_RW=float

$(RWDIR)/cvec.h: vector.htm
	$(MATHPP) vector.htm $@ RW_TYPE_RW=DComplex

$(RWDIR)/ivec.h: vector.htm
	$(MATHPP) vector.htm $@ RW_TYPE_RW=int

$(RWDIR)/scvec.h: vector.htm
	$(MATHPP) vector.htm $@ RW_TYPE_RW=SChar

$(RWDIR)/ucvec.h: vector.htm
	$(MATHPP) vector.htm $@ RW_TYPE_RW=UChar

$(RWDIR)/dvecpik.h: vecpik.htm
	$(MATHPP) vecpik.htm $@ RW_TYPE_RW=double

$(RWDIR)/fvecpik.h: vecpik.htm
	$(MATHPP) vecpik.htm $@ RW_TYPE_RW=float

$(RWDIR)/cvecpik.h: vecpik.htm
	$(MATHPP) vecpik.htm $@ RW_TYPE_RW=DComplex

$(RWDIR)/ivecpik.h: vecpik.htm
	$(MATHPP) vecpik.htm $@ RW_TYPE_RW=int

$(RWDIR)/scvecpik.h: vecpik.htm
	$(MATHPP) vecpik.htm $@ RW_TYPE_RW=SChar

$(RWDIR)/ucvecpik.h: vecpik.htm
	$(MATHPP) vecpik.htm $@ RW_TYPE_RW=UChar

$(RWDIR)/dgenmat.h: matrix.htm
	$(MATHPP) matrix.htm $@ RW_TYPE_RW=double

$(RWDIR)/fgenmat.h: matrix.htm
	$(MATHPP) matrix.htm $@ RW_TYPE_RW=float

$(RWDIR)/cgenmat.h: matrix.htm
	$(MATHPP) matrix.htm $@ RW_TYPE_RW=DComplex

$(RWDIR)/igenmat.h: matrix.htm
	$(MATHPP) matrix.htm $@ RW_TYPE_RW=int

$(RWDIR)/scgenmat.h: matrix.htm
	$(MATHPP) matrix.htm $@ RW_TYPE_RW=SChar

$(RWDIR)/ucgenmat.h: matrix.htm
	$(MATHPP) matrix.htm $@ RW_TYPE_RW=UChar

$(RWDIR)/dgenpik.h: genpik.htm
	$(MATHPP) genpik.htm $@ RW_TYPE_RW=double

$(RWDIR)/fgenpik.h: genpik.htm
	$(MATHPP) genpik.htm $@ RW_TYPE_RW=float

$(RWDIR)/cgenpik.h: genpik.htm
	$(MATHPP) genpik.htm $@ RW_TYPE_RW=DComplex

$(RWDIR)/igenpik.h: genpik.htm
	$(MATHPP) genpik.htm $@ RW_TYPE_RW=int

$(RWDIR)/scgenpik.h: genpik.htm
	$(MATHPP) genpik.htm $@ RW_TYPE_RW=SChar

$(RWDIR)/ucgenpik.h: genpik.htm
	$(MATHPP) genpik.htm $@ RW_TYPE_RW=UChar

$(RWDIR)/darr.h:    array.htm
	$(MATHPP) array.htm $@ RW_TYPE_RW=double

$(RWDIR)/farr.h:    array.htm
	$(MATHPP) array.htm $@ RW_TYPE_RW=float

$(RWDIR)/carr.h:    array.htm
	$(MATHPP) array.htm $@ RW_TYPE_RW=DComplex

$(RWDIR)/iarr.h:    array.htm
	$(MATHPP) array.htm $@ RW_TYPE_RW=int

$(RWDIR)/scarr.h:   array.htm
	$(MATHPP) array.htm $@ RW_TYPE_RW=SChar

$(RWDIR)/ucarr.h:   array.htm
	$(MATHPP) array.htm $@ RW_TYPE_RW=UChar

###################   Template Expansions --- source   #################

ccmath.cpp:      xcmath.cpp
	$(MATHPP) xcmath.cpp $@ RW_TYPE_RW=DComplex

cvecpik.cpp:	xvecpik.cpp
	$(MATHPP) xvecpik.cpp $@ RW_TYPE_RW=DComplex

cvec.cpp:	xvec.cpp
	$(MATHPP) xvec.cpp $@ RW_TYPE_RW=DComplex

cvecio.cpp:	xvecio.cpp
	$(MATHPP) xvecio.cpp $@ RW_TYPE_RW=DComplex

cvecmath.cpp:	xvecmath.cpp
	$(MATHPP) xvecmath.cpp $@ RW_TYPE_RW=DComplex

cvecop.cpp:	xvecop.cpp
	$(MATHPP) xvecop.cpp $@ RW_TYPE_RW=DComplex

cgenpik.cpp:	xgenpik.cpp
	$(MATHPP) xgenpik.cpp $@ RW_TYPE_RW=DComplex

cmat.cpp:	xmat.cpp
	$(MATHPP) xmat.cpp $@ RW_TYPE_RW=DComplex

cmatio.cpp:	xmatio.cpp
	$(MATHPP) xmatio.cpp $@ RW_TYPE_RW=DComplex

cmatmath.cpp:	xmatmath.cpp
	$(MATHPP) xmatmath.cpp $@ RW_TYPE_RW=DComplex

cmatop.cpp:	xmatop.cpp
	$(MATHPP) xmatop.cpp $@ RW_TYPE_RW=DComplex

carr.cpp:	xarr.cpp
	$(MATHPP) xarr.cpp $@ RW_TYPE_RW=DComplex

carrio.cpp:	xarrio.cpp
	$(MATHPP) xarrio.cpp $@ RW_TYPE_RW=DComplex

carrmath.cpp:	xarrmath.cpp
	$(MATHPP) xarrmath.cpp $@ RW_TYPE_RW=DComplex

carrop.cpp:	xarrop.cpp
	$(MATHPP) xarrop.cpp $@ RW_TYPE_RW=DComplex

carrsub.cpp:	xarrsub.cpp
	$(MATHPP) xarrsub.cpp $@ RW_TYPE_RW=DComplex

drmath.cpp:      xrmath.cpp
	$(MATHPP) xrmath.cpp $@ RW_TYPE_RW=double

dvecpik.cpp:	xvecpik.cpp
	$(MATHPP) xvecpik.cpp $@ RW_TYPE_RW=double

dvec.cpp:	xvec.cpp
	$(MATHPP) xvec.cpp $@ RW_TYPE_RW=double

dvecio.cpp:	xvecio.cpp
	$(MATHPP) xvecio.cpp $@ RW_TYPE_RW=double

dvecmath.cpp:	xvecmath.cpp
	$(MATHPP) xvecmath.cpp $@ RW_TYPE_RW=double

dvecop.cpp:	xvecop.cpp
	$(MATHPP) xvecop.cpp $@ RW_TYPE_RW=double

dgenpik.cpp:	xgenpik.cpp
	$(MATHPP) xgenpik.cpp $@ RW_TYPE_RW=double

dmat.cpp:	xmat.cpp
	$(MATHPP) xmat.cpp $@ RW_TYPE_RW=double

dmatio.cpp:	xmatio.cpp
	$(MATHPP) xmatio.cpp $@ RW_TYPE_RW=double

dmatmath.cpp:	xmatmath.cpp
	$(MATHPP) xmatmath.cpp $@ RW_TYPE_RW=double

dmatop.cpp:	xmatop.cpp
	$(MATHPP) xmatop.cpp $@ RW_TYPE_RW=double

darr.cpp:	xarr.cpp
	$(MATHPP) xarr.cpp $@ RW_TYPE_RW=double

darrio.cpp:	xarrio.cpp
	$(MATHPP) xarrio.cpp $@ RW_TYPE_RW=double

darrmath.cpp:	xarrmath.cpp
	$(MATHPP) xarrmath.cpp $@ RW_TYPE_RW=double

darrop.cpp:	xarrop.cpp
	$(MATHPP) xarrop.cpp $@ RW_TYPE_RW=double

darrsub.cpp:	xarrsub.cpp
	$(MATHPP) xarrsub.cpp $@ RW_TYPE_RW=double

frmath.cpp:      xrmath.cpp
	$(MATHPP) xrmath.cpp $@ RW_TYPE_RW=float

fvecpik.cpp:	xvecpik.cpp
	$(MATHPP) xvecpik.cpp $@ RW_TYPE_RW=float

fvec.cpp:	xvec.cpp
	$(MATHPP) xvec.cpp $@ RW_TYPE_RW=float

fvecio.cpp:	xvecio.cpp
	$(MATHPP) xvecio.cpp $@ RW_TYPE_RW=float

fvecmath.cpp:	xvecmath.cpp
	$(MATHPP) xvecmath.cpp $@ RW_TYPE_RW=float

fvecop.cpp:	xvecop.cpp
	$(MATHPP) xvecop.cpp $@ RW_TYPE_RW=float

fgenpik.cpp:	xgenpik.cpp
	$(MATHPP) xgenpik.cpp $@ RW_TYPE_RW=float

fmat.cpp:	xmat.cpp
	$(MATHPP) xmat.cpp $@ RW_TYPE_RW=float

fmatio.cpp:	xmatio.cpp
	$(MATHPP) xmatio.cpp $@ RW_TYPE_RW=float

fmatmath.cpp:	xmatmath.cpp
	$(MATHPP) xmatmath.cpp $@ RW_TYPE_RW=float

fmatop.cpp:	xmatop.cpp
	$(MATHPP) xmatop.cpp $@ RW_TYPE_RW=float

farr.cpp:	xarr.cpp
	$(MATHPP) xarr.cpp $@ RW_TYPE_RW=float

farrio.cpp:	xarrio.cpp
	$(MATHPP) xarrio.cpp $@ RW_TYPE_RW=float

farrmath.cpp:	xarrmath.cpp
	$(MATHPP) xarrmath.cpp $@ RW_TYPE_RW=float

farrop.cpp:	xarrop.cpp
	$(MATHPP) xarrop.cpp $@ RW_TYPE_RW=float

farrsub.cpp:	xarrsub.cpp
	$(MATHPP) xarrsub.cpp $@ RW_TYPE_RW=float

ivecpik.cpp:	xvecpik.cpp
	$(MATHPP) xvecpik.cpp $@ RW_TYPE_RW=int

ivec.cpp:	xvec.cpp
	$(MATHPP) xvec.cpp $@ RW_TYPE_RW=int

ivecio.cpp:	xvecio.cpp
	$(MATHPP) xvecio.cpp $@ RW_TYPE_RW=int

ivecmath.cpp:	xvecmath.cpp
	$(MATHPP) xvecmath.cpp $@ RW_TYPE_RW=int

ivecop.cpp:	xvecop.cpp
	$(MATHPP) xvecop.cpp $@ RW_TYPE_RW=int

igenpik.cpp:	xgenpik.cpp
	$(MATHPP) xgenpik.cpp $@ RW_TYPE_RW=int

imat.cpp:	xmat.cpp
	$(MATHPP) xmat.cpp $@ RW_TYPE_RW=int

imatio.cpp:	xmatio.cpp
	$(MATHPP) xmatio.cpp $@ RW_TYPE_RW=int

imatmath.cpp:	xmatmath.cpp
	$(MATHPP) xmatmath.cpp $@ RW_TYPE_RW=int

imatop.cpp:	xmatop.cpp
	$(MATHPP) xmatop.cpp $@ RW_TYPE_RW=int

iarr.cpp:	xarr.cpp
	$(MATHPP) xarr.cpp $@ RW_TYPE_RW=int

iarrio.cpp:	xarrio.cpp
	$(MATHPP) xarrio.cpp $@ RW_TYPE_RW=int

iarrmath.cpp:	xarrmath.cpp
	$(MATHPP) xarrmath.cpp $@ RW_TYPE_RW=int

iarrop.cpp:	xarrop.cpp
	$(MATHPP) xarrop.cpp $@ RW_TYPE_RW=int

iarrsub.cpp:	xarrsub.cpp
	$(MATHPP) xarrsub.cpp $@ RW_TYPE_RW=int

svecpik.cpp:	xvecpik.cpp
	$(MATHPP) xvecpik.cpp $@ RW_TYPE_RW=SChar

svec.cpp:	xvec.cpp
	$(MATHPP) xvec.cpp $@ RW_TYPE_RW=SChar

svecio.cpp:	xvecio.cpp
	$(MATHPP) xvecio.cpp $@ RW_TYPE_RW=SChar

svecmath.cpp:	xvecmath.cpp
	$(MATHPP) xvecmath.cpp $@ RW_TYPE_RW=SChar

svecop.cpp:	xvecop.cpp
	$(MATHPP) xvecop.cpp $@ RW_TYPE_RW=SChar

sgenpik.cpp:	xgenpik.cpp
	$(MATHPP) xgenpik.cpp $@ RW_TYPE_RW=SChar

smat.cpp:	xmat.cpp
	$(MATHPP) xmat.cpp $@ RW_TYPE_RW=SChar

smatio.cpp:	xmatio.cpp
	$(MATHPP) xmatio.cpp $@ RW_TYPE_RW=SChar

smatmath.cpp:	xmatmath.cpp
	$(MATHPP) xmatmath.cpp $@ RW_TYPE_RW=SChar

smatop.cpp:	xmatop.cpp
	$(MATHPP) xmatop.cpp $@ RW_TYPE_RW=SChar

sarr.cpp:	xarr.cpp
	$(MATHPP) xarr.cpp $@ RW_TYPE_RW=SChar

sarrio.cpp:	xarrio.cpp
	$(MATHPP) xarrio.cpp $@ RW_TYPE_RW=SChar

sarrmath.cpp:	xarrmath.cpp
	$(MATHPP) xarrmath.cpp $@ RW_TYPE_RW=SChar

sarrop.cpp:	xarrop.cpp
	$(MATHPP) xarrop.cpp $@ RW_TYPE_RW=SChar

sarrsub.cpp:	xarrsub.cpp
	$(MATHPP) xarrsub.cpp $@ RW_TYPE_RW=SChar

uvecpik.cpp:	xvecpik.cpp
	$(MATHPP) xvecpik.cpp $@ RW_TYPE_RW=UChar

uvec.cpp:	xvec.cpp
	$(MATHPP) xvec.cpp $@ RW_TYPE_RW=UChar

uvecio.cpp:	xvecio.cpp
	$(MATHPP) xvecio.cpp $@ RW_TYPE_RW=UChar

uvecmath.cpp:	xvecmath.cpp
	$(MATHPP) xvecmath.cpp $@ RW_TYPE_RW=UChar

uvecop.cpp:	xvecop.cpp
	$(MATHPP) xvecop.cpp $@ RW_TYPE_RW=UChar

ugenpik.cpp:	xgenpik.cpp
	$(MATHPP) xgenpik.cpp $@ RW_TYPE_RW=UChar

umat.cpp:	xmat.cpp
	$(MATHPP) xmat.cpp $@ RW_TYPE_RW=UChar

umatio.cpp:	xmatio.cpp
	$(MATHPP) xmatio.cpp $@ RW_TYPE_RW=UChar

umatmath.cpp:	xmatmath.cpp
	$(MATHPP) xmatmath.cpp $@ RW_TYPE_RW=UChar

umatop.cpp:	xmatop.cpp
	$(MATHPP) xmatop.cpp $@ RW_TYPE_RW=UChar

uarr.cpp:	xarr.cpp
	$(MATHPP) xarr.cpp $@ RW_TYPE_RW=UChar

uarrio.cpp:	xarrio.cpp
	$(MATHPP) xarrio.cpp $@ RW_TYPE_RW=UChar

uarrmath.cpp:	xarrmath.cpp
	$(MATHPP) xarrmath.cpp $@ RW_TYPE_RW=UChar

uarrop.cpp:	xarrop.cpp
	$(MATHPP) xarrop.cpp $@ RW_TYPE_RW=UChar

uarrsub.cpp:	xarrsub.cpp
	$(MATHPP) xarrsub.cpp $@ RW_TYPE_RW=UChar

###################   Template Expansions --- BLA   #################

arwbla.c:		xrwbla.c
	$(MATHPP) xrwbla.c $@ RW_TYPE_RW=SChar

brwbla.c:		xrwbla.c
	$(MATHPP) xrwbla.c $@ RW_TYPE_RW=UChar

crwbla.c:		xrwbla.c
	$(MATHPP) xrwbla.c $@ RW_TYPE_RW=FComplex

drwbla.c:		xrwbla.c
	$(MATHPP) xrwbla.c $@ RW_TYPE_RW=double

irwbla.c:		xrwbla.c
	$(MATHPP) xrwbla.c $@ RW_TYPE_RW=int

srwbla.c:		xrwbla.c
	$(MATHPP) xrwbla.c $@ RW_TYPE_RW=float

zrwbla.c:		xrwbla.c
	$(MATHPP) xrwbla.c $@ RW_TYPE_RW=DComplex

###########################  Conversions   ########################
#
.PRECIOUS:	${LIBFULL}

.SUFFIXES: .cpp .C .cc .cxx .o


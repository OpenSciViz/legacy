/*
 * Definitions for various arithmetic operations
 *
 * Generated from template $Id: xvecop.cpp,v 1.7 1993/09/01 16:28:02 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

/*
 * If a promotable scalar (such as a float) is passed by value, then
 * it will get promoted to, say, a double.  If the address of this is
 * then taken, we have a pointer to a double.  Unfortunately, if
 * this address is then passed to a routine that expects a pointer
 * to a float, cfront thinks it has done its job and quietly passes
 * the address of this promoted variable.
 *
 * This problem occurs when K&R compilers are used as a backend to
 * cfront.  The workaround is to create a temporary of known type
 * on the stack.  This is the "scalar2" variable found throughout
 * this file.
 */

#include "rw/ivec.h"
#include "rw/rwibla.h"	

RCSID("$Header: /users/rcs/mathsrc/xvecop.cpp,v 1.7 1993/09/01 16:28:02 alv Exp $");

/************************************************
 *						*
 *		UNARY OPERATORS			*
 *						*
 ************************************************/

// Unary minus on a IntVec
IntVec
operator-(const IntVec& s)
{
  unsigned i = s.length();
  IntVec temp(i,rwUninitialized);
  register int* sp = (int*)s.data();
  register int* dp = temp.data();
  register j = s.stride();
  while (i--) { *dp++ = -(*sp);  sp += j; }
  return temp;
}

// Unary prefix increment on a IntVec; (i.e. ++a)
IntVec&
IntVec::operator++()
{
  register i = length();
  register int* sp = data();
  register j = stride();
  while (i--) { ++(*sp); sp += j; }
  return *this;
}

// Unary prefix decrement on a IntVec (i.e., --a)
IntVec&
IntVec::operator--()
{
  register i = length();
  register int* sp = data();
  register j = stride();
  while (i--) { --(*sp); sp += j; }
  return *this;
}

/************************************************
 *						*
 *		BINARY OPERATORS		*
 *		 vector - vector		*
 *						*
 ************************************************/

// Binary add element-by-element
IntVec
operator+(const IntVec& u, const IntVec& v)
{
  unsigned i = v.length();
  u.lengthCheck(i);
  IntVec temp(i,rwUninitialized);
  rwi_plvv(i, temp.data(), u.data(), u.stride(), v.data(), v.stride());
  return temp;
}

// Binary subtract element-by-element
IntVec
operator-(const IntVec& u, const IntVec& v)
{
  unsigned i = v.length();
  u.lengthCheck(i);
  IntVec temp(i,rwUninitialized);
  rwi_mivv(i, temp.data(), u.data(), u.stride(), v.data(), v.stride());
  return temp;
}

// Binary multiply element-by-element
IntVec
operator*(const IntVec& u, const IntVec& v)
{
  unsigned i = v.length();
  u.lengthCheck(i);
  IntVec temp(i,rwUninitialized);
  rwi_muvv(i, temp.data(), u.data(), u.stride(), v.data(), v.stride());
  return temp;
}

// Binary divide element-by-element
IntVec
operator/(const IntVec& u, const IntVec& v)
{
  unsigned i = v.length();
  u.lengthCheck(i);
  IntVec temp(i,rwUninitialized);
  rwi_dvvv(i, temp.data(), u.data(), u.stride(), v.data(), v.stride());
  return temp;
}

/************************************************
 *						*
 *		BINARY OPERATORS		*
 *		 vector - scalar		*
 *						*
 ************************************************/

// Add a scalar
IntVec
operator+(const IntVec& s, int scalar)
{
  unsigned i = s.length();
  IntVec temp(i,rwUninitialized);
  rwi_plvs(i, temp.data(), s.data(), s.stride(), &scalar);
  return temp;
}


// Subtract from a scalar
IntVec
operator-(int scalar, const IntVec& s)
{
  unsigned i = s.length();
  IntVec temp(i,rwUninitialized);
  rwi_misv(i, temp.data(), &scalar, s.data(), s.stride());
  return temp;
}

// Multiply by a scalar
IntVec
operator*(const IntVec& s, int scalar)
{
  unsigned i = s.length();
  IntVec temp(i,rwUninitialized);
  rwi_muvs(i, temp.data(), s.data(), s.stride(), &scalar);
  return temp;
}

// Divide into a scalar
IntVec
operator/(int scalar, const IntVec& s)
{
  unsigned i = s.length();
  IntVec temp(i,rwUninitialized);
  rwi_dvsv(i, temp.data(), &scalar, s.data(), s.stride());
  return temp;
}

// Divide by a scalar
IntVec
operator/(const IntVec& s, int scalar)
{
  unsigned i = s.length();
  IntVec temp(i,rwUninitialized);
  rwi_dvvs(i, temp.data(), s.data(), s.stride(), &scalar);
  return temp;
}

/*
 * Out-of-line versions for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
IntVec	operator*(int s, const IntVec& V)	{return V*s;}
IntVec	operator+(int s, const IntVec& V)	{return V+s;}
IntVec	operator-(const IntVec& V, int s)  {return V+(-s);}
#endif

/************************************************
 *						*
 *	ARITHMETIC ASSIGNMENT OPERATORS		*
 *	  with other vectors			*
 *						*
 ************************************************/

IntVec&
IntVec::operator+=(const IntVec& u)
{
  if (sameDataBlock(u)) { return operator+=(u.deepCopy()); }  // Avoid aliasing
  unsigned i = u.length();
  lengthCheck(i);
  rwi_aplvv(i, data(), stride(), u.data(), u.stride());
  return *this;
}

IntVec&
IntVec::operator-=(const IntVec& u)
{
  if (sameDataBlock(u)) { return operator-=(u.deepCopy()); }  // Avoid aliasing
  unsigned i = u.length();
  lengthCheck(i);
  rwi_amivv(i, data(), stride(), u.data(), u.stride());
  return *this;
}

IntVec&
IntVec::operator*=(const IntVec& u)
{
  if (sameDataBlock(u)) { return operator*=(u.deepCopy()); }  // Avoid aliasing
  unsigned i = u.length();
  lengthCheck(i);
  rwi_amuvv(i, data(), stride(), u.data(), u.stride());
  return *this;
}

IntVec&
IntVec::operator/=(const IntVec& u)
{
  if (sameDataBlock(u)) { return operator/=(u.deepCopy()); }  // Avoid aliasing
  unsigned i = u.length();
  lengthCheck(i);
  rwi_advvv(i, data(), stride(), u.data(), u.stride());
  return *this;
}

/************************************************
 *						*
 *	ARITHMETIC ASSIGNMENT OPERATORS		*
 *		  with a scalar			*
 *						*
 ************************************************/

IntVec&
IntVec::operator+=(int scalar)
{
  rwi_aplvs(length(), data(), stride(), &scalar);
  return *this;
}


IntVec&
IntVec::operator*=(int scalar)
{
  rwi_amuvs(length(), data(), stride(), &scalar);
  return *this;
}

IntVec&
IntVec::operator/=(int scalar)
{
  rwi_advvs(length(), data(), stride(), &scalar);
  return *this;
}

/************************************************
 *						*
 *		LOGICAL OPERATORS		*
 *						*
 ************************************************/

RWBoolean
IntVec::operator==(const IntVec& u) const
{
  unsigned i = length();
  if(i != u.length()) return FALSE;  // They can't be equal if they don't have the same length.
  return rwisame(i, data(), stride(), u.data(), u.stride());
}

RWBoolean
IntVec::operator!=(const IntVec& u) const
{
  return !(*this == u);
}


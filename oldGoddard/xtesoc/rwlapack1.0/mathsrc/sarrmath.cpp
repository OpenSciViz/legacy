/*
 * Math definitions
 *
 * Generated from template $Id: xarrmath.cpp,v 1.10 1993/09/20 04:02:38 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/rwabla.h"
#include "rw/scarr.h"
#include "rw/scvec.h"


#define REGISTER register

RCSID("$Header: /users/rcs/mathsrc/xarrmath.cpp,v 1.10 1993/09/20 04:02:38 alv Exp $");


SCharArray abs(const SCharArray& s)
// Absolute value of a SCharArray
{
  SCharArray temp(s.length(),rwUninitialized);
  register SChar* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    register const SChar* sp = s.data()+l.start;
    register i = l.length;
    register j = l.stride;
    while (i--) { *dp++ = abs(int(*sp));  sp += j; }
  }
  return temp;
}




SChar dot(const SCharArray& u, const SCharArray& v)
      // dot product: sum_ij...k (u_ij...k * v_ij...k)
{
  u.lengthCheck(v.length());
  SChar result = 0;
  for(DoubleArrayLooper l(v.length(),u.stride(),v.stride()); l; ++l) {
    SChar partialResult;
    rwadot(l.length,u.data()+l.start1,l.stride1,v.data()+l.start2,l.stride2,&partialResult);
    result += partialResult;
  }
  return result;
}

SCharArray dot(const SCharVec& V, const SCharArray& A)
      // dot product: Bj...k = sum_i (Vi * Aij...k)
{
  int n = V.length();
  A.dimensionLengthCheck(0,n);
  RWToEnd jk(1);   // Indexes all components of a vector but the first
  SCharArray B(A.length()(jk),rwUninitialized,RWDataView::COLUMN_MAJOR);
  SChar *dp = B.data();
  for(ArrayLooper l(B.length(),A.stride()(jk)); l; ++l) {
    const SChar *sp = A.data()+l.start;
    for(int i=l.length; i--;) {
      rwadot(n,V.data(),V.stride(),sp,A.stride((int)0),dp);
      sp += l.stride;
      dp++;
    }
  }
  return B;
}

SCharArray dot(const SCharArray& A, const SCharVec& V)
      // dot product: Bi...j = sum_k (Ai...jk * Vk)
{
  int n = V.length();
  A.dimensionLengthCheck(0,n);
  RWSlice ij(0,n-1);   // Indexes all components of array index but the last
  SCharArray B(A.length()(ij),rwUninitialized,RWDataView::COLUMN_MAJOR);
  SChar *dp = B.data();
  for(ArrayLooper l(B.length(),A.stride()(ij)); l; ++l) {
    const SChar *sp = A.data()+l.start;
    for(int i=l.length; i--;) {
      rwadot(n,V.data(),V.stride(),sp,A.stride((int)0),dp);
      sp += l.stride;
      dp++;
    }
  }
  return B;
}

IntVec maxIndex(const SCharArray& s)
// Index of maximum element.
{
  s.numPointsCheck("max needs at least",1);
  int p = s.dimension();
  if (p<1) { return s.length(); } //s.length() is a length 0 vector
  IntVec index(p,0);
  IntVec maxIndex(p,0);
  SChar max = s(maxIndex);

  // Loop through the array, using the bla routine to loop through the
  // innermost loop (first component of index vector).
  int strider = s.stride((int)0);
  int len = s.length((int)0);
  while(index(p-1)<s.length(p-1)) {
    const SChar* ptr = s.data() + dot(index,s.stride());
    int maxLocal = rwamax(len,ptr,strider);
    if (ptr[maxLocal*strider]>max) {
      max=ptr[maxLocal*strider];
      index((int)0) = maxLocal;
      maxIndex = index;
    }
    index((int)0) = s.length((int)0);     // Skip over the zeroth component, then
    int r=0;                    // bump up the index.
    while( index(r)>=s.length(r) && ++r<p ) {
      index(r-1) = 0;
      ++(index(r));
    }
  }
  return maxIndex;
}

SChar maxValue(const SCharArray& s)
{
  s.numPointsCheck("maxValue needs at least",1);
  SChar max = *(s.data());
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    int localMaxIndex = rwamax(l.length,s.data()+l.start,l.stride);
    SChar localMax = *(s.data()+l.start+localMaxIndex*l.stride);
    if (localMax>max) { max=localMax; }
  }
  return max;
}

IntVec minIndex(const SCharArray& s)
// Index of minimum element.
{
  s.numPointsCheck("min needs at least",1);
  int p = s.dimension();
  if (p<1) { return s.length(); } //s.length() is a length 0 vector
  IntVec index(p,0);
  IntVec minIndex(p,0);
  SChar min = s(minIndex);

  // Loop through the array, using the bla routine to loop through the
  // innermost loop (first component of index vector).
  int strider = s.stride((int)0);
  int len = s.length((int)0);
  while(index(p-1)<s.length(p-1)) {
    const SChar* ptr = s.data() + dot(index,s.stride());
    int minLocal = rwamin(len,ptr,strider);
    if (ptr[minLocal*strider]<min) {
      min=ptr[minLocal*strider];
      index((int)0) = minLocal;
      minIndex = index;
    }
    index((int)0) = s.length((int)0);     // Skip over the zeroth component, then
    int r=0;                    // bump up the index.
    while( index(r)>=s.length(r) && ++r<p ) {
      index(r-1) = 0;
      ++(index(r));
    }
  }
  return minIndex;
}

SChar minValue(const SCharArray& s)
{
  s.numPointsCheck("minValue needs at least",1);
  SChar min = *(s.data());
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    int localMinIndex = rwamin(l.length,s.data()+l.start,l.stride);
    SChar localMin = *(s.data()+l.start+localMinIndex*l.stride);
    if (localMin<min) { min=localMin; }
  }
  return min;
}


SChar prod(const SCharArray& s)
{
  REGISTER SChar t = 1;
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    register SChar* sp = (SChar*)(s.data()+l.start);
    register int i = l.length;
    register j = l.stride;
    while (i--) { t *= *sp;  sp += j; }
  }
  return (SChar)t;
}


SChar sum(const SCharArray& s)
{
  REGISTER SChar t = 0;
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    register SChar* sp = (SChar*)(s.data()+l.start);
    register int i = l.length;
    register j = l.stride;
    while (i--) { t += *sp;  sp += j; }
  }
  return (SChar)t;
}






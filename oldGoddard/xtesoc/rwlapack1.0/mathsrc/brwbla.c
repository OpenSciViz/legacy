/*
 * BLA-like extensions for type UChar.
 *
 * Generated from template $Id: xrwbla.c,v 1.3 1993/10/11 18:42:24 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/rwbbla.h"
#include "rw/mathdefs.h"

#include <math.h>
#ifdef BSD
#  include <memory.h>
#else
#  include <string.h>
#endif

/****************************************************************
 *								*
 *			UTILITY					*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwbcopy(n, x, incx, y, incy )
  unsigned n;
  UChar *x, *y;
  int incx, incy;
#else
void FDecl rwbcopy( unsigned n, UChar* restrict x, int incx, const UChar* restrict y, int incy)
#endif
{
  if (n==0) return;

  if (incx == 1 && incy == 1) {
    memcpy(x, y, n*sizeof(UChar));
  }
  else {
    /*
     * Code for increments not equal to one
     */
    while (n--) {
      *x = *y;
      x += incx;
      y += incy;
    }
  }
}

#ifdef RW_KR_ONLY
void rwbset(n, x, incx, scalar)
  unsigned n;
  UChar *x, *scalar;
  int incx;
#else
void FDecl rwbset(unsigned n, UChar* restrict x, int incx, const UChar* restrict scalar)
#endif
{
  if(incx==1)
    while (n--) { *x++ = *scalar; }
  else
    while (n--) { *x = *scalar; x+=incx; }
}

#ifdef RW_KR_ONLY
int rwbsame(n, x, incx, y, incy)
  unsigned n;
  UChar *x, *y;
  int incx, incy;
#else
int FDecl rwbsame(unsigned n, const UChar* restrict x, int incx, const UChar* restrict y, int incy)
#endif
{
  while(n--){
    if( *x != *y ) return 0;
    x += incx;
    y += incy;
  }
  return 1;
}

#ifdef RW_KR_ONLY
int rwbmax(n, x, incx)
  unsigned n;
  UChar *x;
  int incx;
#else
int FDecl rwbmax(unsigned n, const UChar* restrict x, int incx)
#endif
{
  UChar maxv;
  int maxi, i;

  if(n==0) return -1;
  maxv = *x;
  maxi = 0;

  for(i=1; i<n; i++){
    x += incx;
    if(*x > maxv) { maxv = *x; maxi=i; }
  }
  return maxi;
}

#ifdef RW_KR_ONLY
int rwbmin(n, x, incx)
  unsigned n;
  UChar *x;
  int incx;
#else
int FDecl rwbmin(unsigned n, const UChar* restrict x, int incx)
#endif
{
  UChar minv;
  int mini, i;

  if(n==0) return -1;
  minv = *x;
  mini = 0;

  for(i=1; i<n; i++){
    x += incx;
    if(*x < minv) { minv = *x; mini=i; }
  }
  return mini;
}

/****************************************************************
 *								*
 *			DOT PRODUCTS				*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwbdot(n, x, incx, y, incy, result)
  unsigned n;
  UChar *x, *y, *result;
  int incx, incy;
#else
void FDecl rwbdot(unsigned n, const UChar* restrict x, int incx, const UChar* restrict y, int incy, UChar* result)
#endif
{

  UChar sum = 0;
  while(n--){
    sum += *x * *y;
    x += incx;
    y += incy;
  }
  *result = (UChar)sum;
}


/****************************************************************
 *								*
 *		VECTOR --- VECTOR ASSIGNMENT OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwb_aplvv(n, x, incx, y, incy)
  unsigned n;
  UChar *x, *y;
  int incx, incy;
#else
void FDecl rwb_aplvv(unsigned n, UChar* restrict x, int incx, const UChar* restrict y, int incy)
#endif
{
  while(n--){
    *x += *y;
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rwb_amivv(n, x, incx, y, incy)
  unsigned n;
  UChar *x, *y;
  int incx, incy;
#else
void FDecl rwb_amivv(unsigned n, UChar* restrict x, int incx, const UChar* restrict y, int incy)
#endif
{
  while(n--){
    *x -= *y;
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rwb_amuvv(n, x, incx, y, incy)
  unsigned n;
  UChar *x, *y;
  int incx, incy;
#else
void FDecl rwb_amuvv(unsigned n, UChar* restrict x, int incx, const UChar* restrict y, int incy)
#endif
{
  while(n--){
    *x *= *y;
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rwb_advvv(n, x, incx, y, incy)
  unsigned n;
  UChar *x, *y;
  int incx, incy;
#else
void FDecl rwb_advvv(unsigned n, UChar* restrict x, int incx, const UChar* restrict y, int incy)
#endif
{
  while(n--){
    *x /= *y;
    x += incx;
    y += incy;
  }
}

/****************************************************************
 *								*
 *		VECTOR --- SCALAR ASSIGNMENT OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwb_aplvs(n, x, incx, scalar)
  unsigned n;
  UChar *x, *scalar;
  int incx;
#else
void FDecl rwb_aplvs(unsigned n, UChar* restrict x, int incx, const UChar* restrict scalar)
#endif
{
  while(n--) {
    *x += *scalar;
    x += incx;
  }
}

#ifdef RW_KR_ONLY
void rwb_amuvs(n, x, incx, scalar)
  unsigned n;
  UChar *x, *scalar;
  int incx;
#else
void FDecl rwb_amuvs(unsigned n, UChar* restrict x, int incx, const UChar* restrict scalar)
#endif
{
  while(n--) {
    *x *= *scalar;
    x += incx;
  }
}

#ifdef RW_KR_ONLY
void rwb_advvs(n, x, incx, scalar)
  unsigned n;
  UChar *x, *scalar;
  int incx;
#else
void FDecl rwb_advvs(unsigned n, UChar* restrict x, int incx, const UChar* restrict scalar)
#endif
{
  while(n--) {
    *x /= *scalar;
    x += incx;
  }
}

/****************************************************************
 *								*
 *		VECTOR --- VECTOR BINARY OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwb_plvv(n, z, x, incx, y, incy)
  unsigned n;
  UChar *z, *x, *y;
  int incx, incy;
#else
void FDecl rwb_plvv(unsigned n, UChar* restrict z, const UChar* restrict x, int incx, const UChar* restrict y, int incy)
#endif
{
  while(n--){
    *z++ = *x + *y;
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rwb_mivv(n, z, x, incx, y, incy)
  unsigned n;
  UChar *z, *x, *y;
  int incx, incy;
#else
void FDecl rwb_mivv(unsigned n, UChar* restrict z, const UChar* restrict x, int incx, const UChar* restrict y, int incy)
#endif
{
  while(n--){
    *z++ = *x - *y;
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rwb_muvv(n, z, x, incx, y, incy)
  unsigned n;
  UChar *z, *x, *y;
  int incx, incy;
#else
void FDecl rwb_muvv(unsigned n, UChar* restrict z, const UChar* restrict x, int incx, const UChar* restrict y, int incy)
#endif
{
  while(n--){
    *z++ = *x * *y;
    x += incx;
    y += incy;
  }
}

#ifdef RW_KR_ONLY
void rwb_dvvv(n, z, x, incx, y, incy)
  unsigned n;
  UChar *z, *x, *y;
  int incx, incy;
#else
void FDecl rwb_dvvv(unsigned n, UChar* restrict z, const UChar* restrict x, int incx, const UChar* restrict y, int incy)
#endif
{
  while(n--){
    *z++ = *x / *y;
    x += incx;
    y += incy;
  }
}

/****************************************************************
 *								*
 *		VECTOR --- SCALAR BINARY OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwb_plvs(n, z, x, incx, scalar)
  unsigned n;
  UChar *z, *x, *scalar;
  int incx;
#else
void FDecl rwb_plvs(unsigned n, UChar* restrict z, const UChar* restrict x, int incx, const UChar* restrict scalar)
#endif
{
  while(n--){
    *z++ = *x + *scalar;
    x += incx;
  }
}

#ifdef RW_KR_ONLY
void rwb_muvs(n, z, x, incx, scalar)
  unsigned n;
  UChar *z, *x, *scalar;
  int incx;
#else
void FDecl rwb_muvs(unsigned n, UChar* restrict z, const UChar* restrict x, int incx, const UChar* restrict scalar)
#endif
{
  while(n--){
    *z++ = *x * *scalar;
    x += incx;
  }
}

#ifdef RW_KR_ONLY
void rwb_dvvs(n, z, x, incx, scalar)
  unsigned n;
  UChar *z, *x, *scalar;
  int incx;
#else
void FDecl rwb_dvvs(unsigned n, UChar* restrict z, const UChar* restrict x, int incx, const UChar* restrict scalar)
#endif
{
  while(n--){
    *z++ = *x / *scalar;
    x += incx;
  }
}

/****************************************************************
 *								*
 *		SCALAR --- VECTOR BINARY OPERATIONS		*
 *								*
 ****************************************************************/

#ifdef RW_KR_ONLY
void rwb_misv(n, z, scalar, x, incx)
  unsigned n;
  UChar *z, *scalar, *x;
  int incx;
#else
void FDecl rwb_misv(unsigned n, UChar* restrict z, const UChar* scalar, const UChar* restrict x, int incx)
#endif
{
  while(n--){
    *z++ = *scalar - *x;
    x += incx;
  }
}

#ifdef RW_KR_ONLY
void rwb_dvsv(n, z, scalar, x, incx)
  unsigned n;
  UChar *z, *scalar, *x;
  int incx;
#else
void FDecl rwb_dvsv(unsigned n, UChar* restrict z, const UChar* scalar, const UChar* restrict x, int incx)
#endif
{
  while(n--){
    *z++ = *scalar / *x;
    x += incx;
  }
}


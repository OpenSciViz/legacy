/*
 * Definitions for various arithmetic operations
 *
 * Generated from template $Id: xvecop.cpp,v 1.7 1993/09/01 16:28:02 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/fvec.h"
#include "rw/rwsbla.h"	

RCSID("$Header: /users/rcs/mathsrc/xvecop.cpp,v 1.7 1993/09/01 16:28:02 alv Exp $");

/************************************************
 *						*
 *		UNARY OPERATORS			*
 *						*
 ************************************************/

// Unary minus on a FloatVec
FloatVec
operator-(const FloatVec& s)
{
  unsigned i = s.length();
  FloatVec temp(i,rwUninitialized);
  register float* sp = (float*)s.data();
  register float* dp = temp.data();
  register j = s.stride();
  while (i--) { *dp++ = -(*sp);  sp += j; }
  return temp;
}

// Unary prefix increment on a FloatVec; (i.e. ++a)
FloatVec&
FloatVec::operator++()
{
  register i = length();
  register float* sp = data();
  register j = stride();
  while (i--) { ++(*sp); sp += j; }
  return *this;
}

// Unary prefix decrement on a FloatVec (i.e., --a)
FloatVec&
FloatVec::operator--()
{
  register i = length();
  register float* sp = data();
  register j = stride();
  while (i--) { --(*sp); sp += j; }
  return *this;
}

/************************************************
 *						*
 *		BINARY OPERATORS		*
 *		 vector - vector		*
 *						*
 ************************************************/

// Binary add element-by-element
FloatVec
operator+(const FloatVec& u, const FloatVec& v)
{
  unsigned i = v.length();
  u.lengthCheck(i);
  FloatVec temp(i,rwUninitialized);
  rws_plvv(i, temp.data(), u.data(), u.stride(), v.data(), v.stride());
  return temp;
}

// Binary subtract element-by-element
FloatVec
operator-(const FloatVec& u, const FloatVec& v)
{
  unsigned i = v.length();
  u.lengthCheck(i);
  FloatVec temp(i,rwUninitialized);
  rws_mivv(i, temp.data(), u.data(), u.stride(), v.data(), v.stride());
  return temp;
}

// Binary multiply element-by-element
FloatVec
operator*(const FloatVec& u, const FloatVec& v)
{
  unsigned i = v.length();
  u.lengthCheck(i);
  FloatVec temp(i,rwUninitialized);
  rws_muvv(i, temp.data(), u.data(), u.stride(), v.data(), v.stride());
  return temp;
}

// Binary divide element-by-element
FloatVec
operator/(const FloatVec& u, const FloatVec& v)
{
  unsigned i = v.length();
  u.lengthCheck(i);
  FloatVec temp(i,rwUninitialized);
  rws_dvvv(i, temp.data(), u.data(), u.stride(), v.data(), v.stride());
  return temp;
}

/************************************************
 *						*
 *		BINARY OPERATORS		*
 *		 vector - scalar		*
 *						*
 ************************************************/

// Add a scalar
FloatVec
operator+(const FloatVec& s, float scalar)
{
  unsigned i = s.length();
  FloatVec temp(i,rwUninitialized);
  float scalar2 = scalar;
  rws_plvs(i, temp.data(), s.data(), s.stride(), &scalar2);
  return temp;
}


// Subtract from a scalar
FloatVec
operator-(float scalar, const FloatVec& s)
{
  unsigned i = s.length();
  FloatVec temp(i,rwUninitialized);
  float scalar2 = scalar;
  rws_misv(i, temp.data(), &scalar2, s.data(), s.stride());
  return temp;
}

// Multiply by a scalar
FloatVec
operator*(const FloatVec& s, float scalar)
{
  unsigned i = s.length();
  FloatVec temp(i,rwUninitialized);
  float scalar2 = scalar;
  rws_muvs(i, temp.data(), s.data(), s.stride(), &scalar2);
  return temp;
}

// Divide into a scalar
FloatVec
operator/(float scalar, const FloatVec& s)
{
  unsigned i = s.length();
  FloatVec temp(i,rwUninitialized);
  float scalar2 = scalar;
  rws_dvsv(i, temp.data(), &scalar2, s.data(), s.stride());
  return temp;
}


/*
 * Out-of-line versions for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
FloatVec	operator*(float s, const FloatVec& V)	{return V*s;}
FloatVec	operator+(float s, const FloatVec& V)	{return V+s;}
FloatVec	operator-(const FloatVec& V, float s)  {return V+(-s);}
FloatVec	operator/(const FloatVec& V, float s)  {return V*(1/s);}
#endif

/************************************************
 *						*
 *	ARITHMETIC ASSIGNMENT OPERATORS		*
 *	  with other vectors			*
 *						*
 ************************************************/

FloatVec&
FloatVec::operator+=(const FloatVec& u)
{
  if (sameDataBlock(u)) { return operator+=(u.deepCopy()); }  // Avoid aliasing
  unsigned i = u.length();
  lengthCheck(i);
  rws_aplvv(i, data(), stride(), u.data(), u.stride());
  return *this;
}

FloatVec&
FloatVec::operator-=(const FloatVec& u)
{
  if (sameDataBlock(u)) { return operator-=(u.deepCopy()); }  // Avoid aliasing
  unsigned i = u.length();
  lengthCheck(i);
  rws_amivv(i, data(), stride(), u.data(), u.stride());
  return *this;
}

FloatVec&
FloatVec::operator*=(const FloatVec& u)
{
  if (sameDataBlock(u)) { return operator*=(u.deepCopy()); }  // Avoid aliasing
  unsigned i = u.length();
  lengthCheck(i);
  rws_amuvv(i, data(), stride(), u.data(), u.stride());
  return *this;
}

FloatVec&
FloatVec::operator/=(const FloatVec& u)
{
  if (sameDataBlock(u)) { return operator/=(u.deepCopy()); }  // Avoid aliasing
  unsigned i = u.length();
  lengthCheck(i);
  rws_advvv(i, data(), stride(), u.data(), u.stride());
  return *this;
}

/************************************************
 *						*
 *	ARITHMETIC ASSIGNMENT OPERATORS		*
 *		  with a scalar			*
 *						*
 ************************************************/

FloatVec&
FloatVec::operator+=(float scalar)
{
  float scalar2 = scalar;
  rws_aplvs(length(), data(), stride(), &scalar2);
  return *this;
}


FloatVec&
FloatVec::operator*=(float scalar)
{
  float scalar2 = scalar;
  rws_amuvs(length(), data(), stride(), &scalar2);
  return *this;
}


/************************************************
 *						*
 *		LOGICAL OPERATORS		*
 *						*
 ************************************************/

RWBoolean
FloatVec::operator==(const FloatVec& u) const
{
  unsigned i = length();
  if(i != u.length()) return FALSE;  // They can't be equal if they don't have the same length.
  return rwssame(i, data(), stride(), u.data(), u.stride());
}

RWBoolean
FloatVec::operator!=(const FloatVec& u) const
{
  return !(*this == u);
}


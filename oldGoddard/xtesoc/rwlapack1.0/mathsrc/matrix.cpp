/*
 * Definitions for RWMatView and the matrix looper class
 *
 * $Header: /users/rcs/mathsrc/matrix.cpp,v 1.4 1993/08/17 19:02:00 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * It is possible to log data block allocations/deallocations.  See
 * the comment block near the end of this file for details.
 *
 * $Log: matrix.cpp,v $
 * Revision 1.4  1993/08/17  19:02:00  alv
 * now allows use of custom RWBlocks
 *
 * Revision 1.2  1993/06/24  22:57:56  north
 * added use of RW_GLOBAL_ENUMS
 *
 * Revision 1.1  1993/01/22  23:51:01  alv
 * Initial revision
 *
 * 
 *    Rev 1.5   24 Mar 1992 12:59:48   KEFFER
 * nil -> rwnil
 * 
 *    Rev 1.4   15 Nov 1991 09:36:26   keffer
 * Removed RWMATHERR macro
 * 
 *    Rev 1.3   17 Oct 1991 09:15:30   keffer
 * Changed include path to <rw/xxx.h>
 * 
 */

#include "rw/matrix.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"


/*
 * Implementation of RWMatView
 */

RWMatView::RWMatView()
  : RWDataView()
{
  nrows=ncols=0;
  rowstep=colstep=1;
}

RWMatView::RWMatView(unsigned m, unsigned n, size_t s, Storage storage)
  : RWDataView(m*n,s)
{
  nrows=m;
  ncols=n;
  if (storage==ROW_MAJOR) {
    colstep=1;
    rowstep=ncols;
  } else {
    if (storage!=COLUMN_MAJOR) {
      RWTHROW( RWInternalErr( RWMessage(RWMATH_STORAGE) ));
    }
    rowstep=1;
    colstep=nrows;
  }
}

RWMatView::RWMatView(RWBlock *block, unsigned m, unsigned n, Storage storage)
  : RWDataView(block)
{
  nrows=m;
  ncols=n;
  if (storage==ROW_MAJOR) {
    colstep=1;
    rowstep=ncols;
  } else {
    if (storage!=COLUMN_MAJOR) {
      RWTHROW( RWInternalErr( RWMessage(RWMATH_STORAGE) ));
    }
    rowstep=1;
    colstep=nrows;
  }
}

RWMatView::RWMatView(const RWMatView& x)
  : RWDataView(x)
{
  nrows = x.nrows;
  ncols = x.ncols;
  rowstep = x.rowstep;
  colstep = x.colstep;
}

void RWMatView::reference(const RWMatView& x)
{
  RWDataView::reference(x);
  nrows = x.nrows;
  ncols = x.ncols;
  rowstep = x.rowstep;
  colstep = x.colstep;
}

void RWMatView::boundsCheck(int i, int j)            const // Ensure 0<=i,j<nrows,ncols
{
  rowBoundsCheck(i);
  colBoundsCheck(j);
}

void RWMatView::rowBoundsCheck(int i) const
{
  if(i<0 || i>=nrows) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_INDEX, i, nrows) ));
  }
}

void RWMatView::colBoundsCheck(int j) const
{
  if(j<0 || j>=ncols) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_INDEX, j, ncols) ));
  }
}

void RWMatView::lengthCheck(unsigned m, unsigned n)  const // Check that size of self is m,n
{
  if(nrows!=m || ncols!=n) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_MNMATCH, m, n, nrows, ncols) ));
  }
}

void RWMatView::colCheck(unsigned j)              const // Ensure n=ncols
{
  if(j!=ncols) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_NMATCH, j, ncols) ));
  }
}

void RWMatView::rowCheck(unsigned i)              const // Ensure m=nrows
{
  if(i!=nrows) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_NMATCH, i, nrows) ));
  }
}

void RWMatView::sliceCheck(int i, int j, unsigned n, int rowstr, int colstr) const     // Check that slice is valid
{
  // This allows for zero length slices:
  if(n>0) {
    int maxi = i+(n-1)*rowstr;
    int maxj = j+(n-1)*colstr;
    if (i<0 || i>=nrows)       { RWTHROW( RWBoundsErr( RWMessage(RWMATH_SLICEBEG, i, nrows-1)));}  
    if (maxi<0 || maxi>=nrows) { RWTHROW( RWBoundsErr( RWMessage(RWMATH_SLICEEND, maxi, nrows-1)));}
    if (j<0 || j>=ncols)       { RWTHROW( RWBoundsErr( RWMessage(RWMATH_SLICEBEG, j, ncols-1)));}
    if (maxj<0 || maxj>=ncols) { RWTHROW( RWBoundsErr( RWMessage(RWMATH_SLICEEND, maxj, ncols-1 )));}
  }
}

void RWMatView::sliceCheck(int i, int j, unsigned m, unsigned n, int rowstr1, int colstr1, int rowstr2, int colstr2) const 
{
  if (m*n>0) {
    sliceCheck(i,j,m,rowstr1,colstr1);
    sliceCheck(i,j,n,rowstr2,colstr2);
  }
}

void RWMatView::numPointsCheck(const char *why, int n) const // Check that there is at least n points
{
  if (nrows*ncols<n) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_NPOINTS,(unsigned)(nrows*ncols),why,n) ));
  }
}


/*
 * Implementation of matrix looper class.  This is used to visit
 * every element in a matrix
 */

MatrixLooper::MatrixLooper(unsigned nrows, unsigned ncols,
			   int rowstride, int colstride,
#ifdef RW_GLOBAL_ENUMS
			               Storage storage)
#else
			   RWDataView::Storage storage)
#endif
{
	start = 0;

#ifdef RW_GLOBAL_ENUMS
/* ugly hack for our old Cfront 2.0 compiler, ROW_MAJOR=1, RWEITHER=2 */
	if (storage==      (enum Storage)1 || storage==     (enum Storage)2 && colstride<rowstride )
#else
	if (storage==RWDataView::ROW_MAJOR || storage==RWDataView::RWEITHER && colstride<rowstride )
#endif	
	{
		ncolsLeft = nrows;			 // Row major order
		length = ncols;
		colstep = rowstride;
		stride = colstride;
	} else {
		ncolsLeft = ncols;       // Column major order
		length = nrows;
		colstep = colstride;
		stride = rowstride;
	}

	// If the matrix is in contiguous storage, then treat it as one 
	// long column
	if (length*stride == colstep) {
		length *= ncolsLeft;
		ncolsLeft = 1;
	}

	if (length==0) { ncolsLeft=0; }   // Nothing to do
}

DoubleMatrixLooper::DoubleMatrixLooper(unsigned nrows, unsigned ncols,
				       int rowstr1, int colstr1,
				       int rowstr2, int colstr2,
#ifdef RW_GLOBAL_ENUMS
				                   Storage storage
#else
				       RWDataView::Storage storage
#endif
				      )
{
	start1 = start2 = 0;

	// If both matrices are in row major order, then move by rows instead
	// of by columns
/*** Ugly hack for our Cfront 2.0 Compiler: ROW_MAJOR=1, RWEITHER=2 ***/
#ifdef RW_GLOBAL_ENUMS
	if (storage==        (enum Storage)1 || storage==     (enum Storage)2 && colstr1<rowstr1 && colstr2<rowstr2)
#else
	  if (storage==RWDataView::ROW_MAJOR || storage==RWDataView::RWEITHER && colstr1<rowstr1 && colstr2<rowstr2)
#endif	    
	{
		ncolsLeft = nrows;
		length = ncols;
		colstep1 = rowstr1;
		stride1 = colstr1;
		colstep2 = rowstr2;
		stride2 = colstr2;
	} else {
		ncolsLeft = ncols;
		length = nrows;
		colstep1 = colstr1;
		stride1 = rowstr1;
		colstep2 = colstr2;
		stride2 = rowstr2;
	}

	// If the matrices are in contigous storage, then treat them
	// as one long column
	if (length*stride1==colstep1 && length*stride2==colstep2) {
		length *= ncolsLeft;
		ncolsLeft = 1;
	}

	if (length==0) { ncolsLeft=0; }   // Nothing to do
}


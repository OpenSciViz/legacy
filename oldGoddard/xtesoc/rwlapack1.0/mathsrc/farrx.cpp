/*
 * Type specific routines for FloatArray
 *
 * $Header: /users/rcs/mathsrc/farrx.cpp,v 1.2 1993/09/19 23:43:24 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: farrx.cpp,v $
 * Revision 1.2  1993/09/19  23:43:24  alv
 * fixed bug caught by semantic
 *
 * Revision 1.1  1993/01/22  23:50:58  alv
 * Initial revision
 *
 * 
 *    Rev 1.3   17 Oct 1991 09:15:18   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.2   08 Oct 1991 13:44:36   keffer
 * Math V4.0 beta 2
 * 
 *    Rev 1.1   24 Jul 1991 12:51:38   keffer
 * Added pvcs keywords
 *
 */

#include "rw/farr.h"
#include "rw/darr.h"

RCSID("$Header: /users/rcs/mathsrc/farrx.cpp,v 1.2 1993/09/19 23:43:24 alv Exp $");

// Convert to DoubleArray.  
// Should be a friend, but.... 
FloatArray::operator DoubleArray() const
{
  DoubleArray temp(length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  double *dp = temp.data();
  for(ArrayLooper l(length(),stride()); l; ++l) {
    int n = l.length;
    const float *tp = data()+l.start;
    while(n--) {
      *dp++ = double(*tp); tp+=l.stride;
    }
  }
  return temp;
}


// narrow from DoubleArray to FloatArray
FloatArray
toFloat(const DoubleArray& v)
{
  FloatArray temp(v.length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  float *dp = temp.data();
  for(ArrayLooper l(v.length(),v.stride()); l; ++l) {
    int n = l.length;
    const double *tp = v.data()+l.start;
    while(n--) {
      *dp++ = float(*tp); tp+=l.stride;
    }
  }
  return temp;
}

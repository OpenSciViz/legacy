/*
 * Type conversion routines for UCharVec
 *
 * $Header: /users/rcs/mathsrc/uvecx.cpp,v 1.3 1993/09/01 16:27:45 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: uvecx.cpp,v $
 * Revision 1.3  1993/09/01  16:27:45  alv
 * now uses rwUninitialized form for simple constructors
 *
 * Revision 1.2  1993/01/26  17:52:28  alv
 * removed DOS EOF
 *
 * Revision 1.1  1993/01/22  23:51:06  alv
 * Initial revision
 *
 * 
 *    Rev 1.3   17 Oct 1991 09:15:40   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.2   08 Oct 1991 13:45:00   keffer
 * Math V4.0 beta 2
 * 
 *    Rev 1.1   24 Jul 1991 12:52:02   keffer
 * Added pvcs keywords
 *
 */

#include "rw/ucvec.h"
#include "rw/ivec.h"

RCSID("$Header: /users/rcs/mathsrc/uvecx.cpp,v 1.3 1993/09/01 16:27:45 alv Exp $");


// Convert to an IntVec
// Should be a friend of IntVec, but...
UCharVec::operator IntVec() const
{
  register int n = length();
  IntVec temp(n,rwUninitialized);
  register int* dp = temp.data();
  register const UChar* tp = data();
  register int ts = stride();
  while(n--) {
    *dp++ = int(*tp); tp += ts;
  }
  return temp;
}


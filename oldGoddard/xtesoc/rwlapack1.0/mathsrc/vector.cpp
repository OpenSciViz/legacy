/*
 * Definitions for RWVecView
 *
 * $Header: /users/rcs/mathsrc/vector.cpp,v 1.5 1993/09/19 20:52:52 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * It is possible to log data block allocations/deallocations.  See
 * the comment block near the end of this file for details.
 *
 * $Log: vector.cpp,v $
 * Revision 1.5  1993/09/19  20:52:52  alv
 * ported to SUN cafe compiler
 *
 * Revision 1.4  1993/08/17  19:02:00  alv
 * now allows use of custom RWBlocks
 *
 * Revision 1.2  1993/06/18  19:23:50  alv
 * removed reference to static numPerLine for multi-thread safety
 *
 * Revision 1.1  1993/01/22  23:51:06  alv
 * Initial revision
 *
 * 
 *    Rev 1.5   24 Mar 1992 12:59:48   KEFFER
 * nil -> rwnil
 * 
 *    Rev 1.4   15 Nov 1991 09:36:26   keffer
 * Removed RWMATHERR macro
 * 
 *    Rev 1.3   17 Oct 1991 09:15:30   keffer
 * Changed include path to <rw/xxx.h>
 * 
 */

#include "rw/vector.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"

RWVecView::RWVecView()
  : RWDataView()
{
  npts = 0;
  step = 1;
}

RWVecView::RWVecView(RWBlock *blck, unsigned n)
  : RWDataView(blck)
{
  npts = n;
  step = 1;
}

RWVecView::RWVecView(unsigned n, size_t s)
  : RWDataView(n,s)
{
  npts = n;
  step = 1;
}

RWVecView::RWVecView(const RWVecView& x)
  : RWDataView(x)
{
  npts = x.npts;
  step = x.step;
}

void      RWVecView::reference(const RWVecView& x)
{
  RWDataView::reference(x);
  npts = x.npts;
  step = x.step;
}

void RWVecView::boundsCheck(int i)             const  // Ensure 0<=i<npts
{
  if(i<0 || i>=length() )
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_INDEX,i, (unsigned)(length()-1) )));
}

void RWVecView::lengthCheck(unsigned  i)       const  // Check that length of self is i
{
  if(length() != i)
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_VNMATCH,(unsigned)length(), i)));
}

void RWVecView::emptyErr(const char* fname)    const  // Error: vector not empty
{
  RWTHROW( RWBoundsErr( RWMessage(RWMATH_ZEROVEC, fname)));
}

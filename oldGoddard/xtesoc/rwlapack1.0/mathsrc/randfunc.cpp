/*
 * Definitions for global statistics functions.
 *
 * $Header: /users/rcs/mathsrc/randfunc.cpp,v 1.2 1993/01/26 17:52:28 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: randfunc.cpp,v $
 * Revision 1.2  1993/01/26  17:52:28  alv
 * removed DOS EOF
 *
 * Revision 1.1  1993/01/22  23:51:02  alv
 * Initial revision
 *
 * 
 *    Rev 1.3   17 Oct 1991 09:15:24   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.2   08 Oct 1991 13:44:46   keffer
 * Math V4.0 beta 2
 * 
 *    Rev 1.1   24 Jul 1991 12:51:48   keffer
 * Added pvcs keywords
 *
 */

/*  Functions used by Random number classes  */

#include "rw/dstats.h"
STARTWRAP
#include <math.h>
#include <stdlib.h>
ENDWRAP

// natural log of the gamma function:

double c_data[] = { 76.18009173,     -86.50532033,   24.01409822,
                     -1.231739516,   .120858003e-2,   -.536382e-5 };
double
logGamma(double xx) 
{
   double x   = xx - 1.;
   double tmp = x + 5.5;
   tmp = (x + .5)*::log(tmp) - tmp;
   double ser = 1.;
   for (int j=0; j<6; j++) {
      x   += 1.;
      ser += c_data[j]/x;
   }
   return (tmp + ::log(2.50662827465*ser));
}


// factorial function for integers:
double 
factorial(unsigned n)
{
   register unsigned j;
   static double A[33];        // table to be filled in as required
   static unsigned top;        // initialized with 0 factorial only
   A[0] = 1.;
   if ( n <= top )        // already in table
       return A[n];
   else if (n <= 32) {
       for ( j=top+1; j<n+1; j++ )     // fill in table to desired value
          A[j] = A[j-1]*j;
       top = n;
       return A[n];
   }
   else                              // larger value than table is required
       return ::exp(logGamma(n+1.));   // beware of overflow!!
}

// beta function B(z,w) = B(w,z) = integral(0-1){t**(z-1) * (1-t)**(w-1)}dt
double
beta(double z, double w)
{
   return ( ::exp(logGamma(z)+logGamma(w)-logGamma(z+w))  );
}


// evaluate the binomial probability coefficient:
       // nobs = number of items observed
       // ntotal = total number of items
       // prob = probability of observing each item
double
binomialPF(unsigned nobs, unsigned ntotal, double prob)
{
   unsigned n = ntotal - nobs;
   double p = 1. - prob;
   return ( factorial(ntotal)/(factorial(nobs)*factorial(n))
                  * ::pow(prob,(double)nobs) * ::pow(p,(double)n)  );
}


// evaluate the poisson probability function:
       // nobs = number of items observed
       // mean = mean of distribution
double
poissonPF(unsigned nobs, double mean)
{
  return ( ::pow(mean,(double)nobs)/factorial(nobs)*::exp(-mean)  );
}


// evaluate the Gaussian probability function:
       // x = value for which probability is to be evaluated
       // mean = mean of distribution
       // sigma = standard deviation of distribution 
double
gaussianPF(double x, double mean, double sigma)
{
  double z = (x-mean)/sigma;
  return ( .3989422804/sigma * ::exp(-z*z/2.) );  
}



// evaluate the Exponential probability function:
// the quantity x/a has the probability distribution a*exp(-ax).
double
exponentialPF(double x, double a)
{
  return ( a*::exp(-a*x) );  
}



// evaluate the Lorentzian probability function:
       // x = value for which probability is to be evaluated
       // mean = mean of distribution
       // width = full width at half maximum of distribution
double
lorentzianPF(double x, double mean, double width)
{
  double z = x - mean;
  return (  .1591549431 * width / (z*z + width*width/4.)  );  
}


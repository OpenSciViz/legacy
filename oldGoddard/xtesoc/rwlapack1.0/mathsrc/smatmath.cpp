/*
 * Math definitions for SCharGenMat
 *
 * Generated from template $Id: xmatmath.cpp,v 1.8 1993/09/16 17:16:56 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/scgenmat.h"
#include "rw/scvec.h"
#include "rw/rwabla.h"


#define REGISTER register

RCSID("$Header: /users/rcs/mathsrc/xmatmath.cpp,v 1.8 1993/09/16 17:16:56 alv Exp $");


SCharGenMat abs(const SCharGenMat& s)
// Absolute value of a SCharGenMat
{
  SCharGenMat temp(s.rows(),s.cols(),rwUninitialized);
  register SChar* dp = temp.data();
  for(MatrixLooper l(s.rows(),s.cols(),s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    register const SChar* sp = s.data()+l.start;
    register i = l.length;
    register j = l.stride;
    while (i--) { *dp++ = abs(int(*sp));  sp += j; }
  }
  return temp;
}




SChar dot(const SCharGenMat& u, const SCharGenMat& v)
      // dot product: sum_ij (u_ij * v_ij)
{
  u.lengthCheck(v.rows(),v.cols());
  SChar result = 0;
  for(DoubleMatrixLooper l(v.rows(),v.cols(),u.rowStride(),u.colStride(),v.rowStride(),v.colStride()); l; ++l) {
    SChar partialResult;
    rwadot(l.length,u.data()+l.start1,l.stride1,v.data()+l.start2,l.stride2,&partialResult);
    result += partialResult;
  }
  return result;
}

void maxIndex(const SCharGenMat& s, int *maxi, int *maxj)
// Index of maximum element.
{
  int len = s.rows()*s.cols();
  s.numPointsCheck("max needs at least",1);
  if (s.colStride()==s.rowStride()*s.rows()) {    // Column major order
    int maxindex = rwamax(len,s.data(),s.rowStride());
    *maxj = maxindex/s.rows();
    *maxi = maxindex%s.rows();
    return;
  }
  if (s.rowStride()==s.colStride()*s.cols()) {    // Row major order
    int maxindex = rwamax(len,s.data(),s.colStride());
    *maxj = maxindex/s.cols();
    *maxi = maxindex%s.cols();
    return;
  }
  *maxi = *maxj = 0;			    // Matrix is not contiguous
  SChar themax = s(0,0);
  if (s.colStride()>s.rowStride()) {
    for( int j=0; j<s.cols(); j++ ) {  // Do a column at a time
      int i = rwamax(len,s.data()+j*s.colStride(),s.rowStride());
      if (s(i,j)>themax) { themax=s(i,j); *maxi=i; *maxj=j; }
    }
  } else {
    for( int i=0; i<s.rows(); i++ ) {  // Do a row at a time
      int j = rwamax(len,s.data()+i*s.rowStride(),s.colStride());
      if (s(i,j)>themax) { themax=s(i,j); *maxi=i; *maxj=j; }
    }
  }
}

SChar maxValue(const SCharGenMat& s)
{
  int i,j;
  maxIndex(s,&i,&j);
  return s(i,j);
}
    
void minIndex(const SCharGenMat& s, int *mini, int *minj)
// Index of minimum element.
{
  int len = s.rows()*s.cols();
  s.numPointsCheck("min needs at least",1);
  if (s.colStride()==s.rowStride()*s.rows()) {    // Column major order
    int minindex = rwamin(len,s.data(),s.rowStride());
    *minj = minindex/s.rows();
    *mini = minindex%s.rows();
    return;
  }
  if (s.rowStride()==s.colStride()*s.cols()) {    // Row major order
    int minindex = rwamin(len,s.data(),s.colStride());
    *minj = minindex/s.cols();
    *mini = minindex%s.cols();
    return;
  }
  *mini = *minj = 0;			    // Matrix is not contiguous
  SChar themin = s(0,0);
  if (s.colStride()>s.rowStride()) {
    for( int j=0; j<s.cols(); j++ ) {  // Do a column at a time
      int i = rwamin(len,s.data()+j*s.colStride(),s.rowStride());
      if (s(i,j)>themin) { themin=s(i,j); *mini=i; *minj=j; }
    }
  } else {
    for( int i=0; i<s.rows(); i++ ) {  // Do a row at a time
      int j = rwamin(len,s.data()+i*s.rowStride(),s.colStride());
      if (s(i,j)>themin) { themin=s(i,j); *mini=i; *minj=j; }
    }
  }
}
    
SChar minValue(const SCharGenMat& s)
{
  int i,j;
  minIndex(s,&i,&j);
  return s(i,j);
}


SChar prod(const SCharGenMat& s)
{
  REGISTER SChar t = 1;
  for(MatrixLooper l(s.rows(),s.cols(),s.rowStride(),s.colStride()); l; ++l) {
    register SChar* sp = (SChar*)(s.data()+l.start);
    register int i = l.length;
    register j = l.stride;
    while (i--) { t *= *sp;  sp += j; }
  }
  return (SChar)t;
}


SChar sum(const SCharGenMat& s)
{
  REGISTER SChar t = 0;
  for(MatrixLooper l(s.rows(),s.cols(),s.rowStride(),s.colStride()); l; ++l) {
    register SChar* sp = (SChar*)(s.data()+l.start);
    register int i = l.length;
    register j = l.stride;
    while (i--) { t += *sp;  sp += j; }
  }
  return (SChar)t;
}



SCharGenMat transpose(const SCharGenMat& A)
{
  return A.slice(0,0, A.cols(),A.rows(), 0,1, 1,0);
}

SCharVec product(const SCharGenMat& A, const SCharVec& x)
{
  int m = A.rows();
  int n = A.cols();
  x.lengthCheck(n);
  SCharVec y(m,rwUninitialized);
  const SChar* Aptr = A.data();
  for(int i=0; i<m; i++) {
    rwadot(n,x.data(),x.stride(),Aptr,A.colStride(),&(y(i)));
    Aptr += A.rowStride();
  }
  return y;
}

SCharGenMat transposeProduct(const SCharGenMat& A, const SCharGenMat& B)
{
  return product( transpose(A), B );
}


SCharVec product(const SCharVec& x, const SCharGenMat& A)
{
  return product( transpose(A),x );
}

SCharGenMat product(const SCharGenMat& A, const SCharGenMat& B)
{
  int m = A.rows();
  int n = B.cols();
  int r = A.cols();  // == B.rows() if A,B conform
  B.rowCheck(r);
  SCharGenMat AB(m,n,rwUninitialized);
  SCharVec Arow(r,rwUninitialized);
  for(int i=0; i<m; i++) {
    Arow = A.row(i);  // Compact this row to minimize page faults
    const SChar* Bptr = B.data();
    for(int j=0; j<n; j++) {
      rwadot(r,Arow.data(),1,Bptr,B.rowStride(),&(AB(i,j)));
      Bptr += B.colStride();
    }
  }
  return AB;
}




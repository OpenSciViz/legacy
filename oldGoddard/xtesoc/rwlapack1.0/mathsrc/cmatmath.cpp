/*
 * Math definitions for DComplexGenMat
 *
 * Generated from template $Id: xmatmath.cpp,v 1.8 1993/09/16 17:16:56 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/cgenmat.h"
#include "rw/cvec.h"
#include "rw/rwzbla.h"


// Complexes won't fit in a register
#define REGISTER

RCSID("$Header: /users/rcs/mathsrc/xmatmath.cpp,v 1.8 1993/09/16 17:16:56 alv Exp $");

// Absolute value of a DComplexGenMat
DoubleGenMat abs(const DComplexGenMat& s)
{
  DoubleGenMat temp(s.rows(),s.cols(),rwUninitialized);
  register Double* dp = temp.data();
  for(MatrixLooper l(s.rows(),s.cols(),s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    register DComplex* sp = (DComplex*)(s.data()+l.start); // Cast away "constness" to avoid problems with abs()
    register j = l.stride;
    register i = l.length;
    while (i--) { *dp++ = abs(*sp); sp += j; }
  }
  return temp;
}


DComplexGenMat DComplexGenMat::apply(CmathFunTy f) const
// Apply function returning DComplex to each element of vector slice
{
  DComplexGenMat temp(nrows,ncols,rwUninitialized);
  register DComplex* dp = temp.data();
  for(MatrixLooper l(nrows,ncols,rowstep,colstep,RWDataView::COLUMN_MAJOR); l; ++l) {
    register i = l.length;
    register DComplex* sp = (DComplex*)(data()+l.start);
    register j = l.stride;
    while(i--) {
// Turbo C++ bug requires temporary.
      DComplex dummy = (*f)(*sp);
      *dp++ = dummy;
      sp += j; 
    }
  }
  return temp;
}

DoubleGenMat DComplexGenMat::apply2(CmathFunTy2 f) const
// Apply function returning Double to each element of vector slice
{
  DoubleGenMat temp(nrows,ncols,rwUninitialized);
  register Double* dp = temp.data();
  for(MatrixLooper l(nrows,ncols,rowstep,colstep,RWDataView::COLUMN_MAJOR); l; ++l) {
    register DComplex* sp = (DComplex*)(data()+l.start);
    register i = l.length;
    register j = l.stride;
    while (i--) { *dp++ = (*f)(*sp);  sp += j; }
  }
  return temp;
}

#  ifdef COMPLEX_PACKS

/*
 * If we can guarantee that the complex vector goes
 * (real, imag, real, imag, real, imag, etc.) then
 * it becomes possible to implement real(DComplexGenMat&) and imag(DComplexGenMat&)
 * via a slice, allowing its use as an lvalue.  What
 * follows violates encapsulation (a bit), but sure is useful.
 */

DoubleGenMat real(const DComplexGenMat& v)
{
#ifdef IMAG_LEADS
  return DoubleGenMat(v, (Double*)v.begin + 1, v.rows(),v.cols(), 2*v.rowStride(),2*v.colStride());
#else
  return DoubleGenMat(v, (Double*)v.begin, v.rows(),v.cols(), 2*v.rowStride(),2*v.colStride());
#endif
}
DoubleGenMat imag(const DComplexGenMat& v)
{
#ifdef IMAG_LEADS
  return DoubleGenMat(v, (Double*)v.begin, v.rows(),v.cols(), 2*v.rowStride(),2*v.colStride());
#else
  return DoubleGenMat(v, (Double*)v.begin + 1, v.rows(),v.cols(), 2*v.rowStride(),2*v.colStride());
#endif
}

// The above allows a very compact form of conj() to be written:
DComplexGenMat
conj(const DComplexGenMat& V)
{
  return DComplexGenMat(real(V), -imag(V));
}

#else	/* Complex does not pack */

DoubleGenMat real(const DComplexGenMat& v)
{
  DoubleGenMat temp(nrows,ncols);
  register Double* dp = temp.data();
  for(MatrixLooper l(nrows,ncols,rowstep,colstep,RWDataView::COLUMN_MAJOR); l; ++l) {
    register i = l.length;
    register const DComplex* sp = v.data()+l.start;
    register j = l.stride;
    while(i--) {*dp++ = real(*sp); sp += j;}
  }
  return temp;
}

DoubleGenMat imag(const DComplexGenMat& v)
{
  DoubleGenMat temp(nrows,ncols);
  register Double* dp = temp.data();
  for(MatrixLooper l(nrows,ncols,rowstep,colstep,RWDataView::COLUMN_MAJOR); l; ++l) {
    register i = l.length;
    register const DComplex* sp = v.data()+l.start;
    register j = l.stride;
    while(i--) {*dp++ = imag(*sp); sp += j;}
  }
  return temp;
}

DComplexGenMat	conj(const DComplexGenMat& V)	{return V.apply(::conj); }

#endif /* Not COMPLEX_PACKS */



DComplex dot(const DComplexGenMat& u, const DComplexGenMat& v)
      // dot product: sum_ij (u_ij * v_ij)
{
  u.lengthCheck(v.rows(),v.cols());
  DComplex result(0,0);
  for(DoubleMatrixLooper l(v.rows(),v.cols(),u.rowStride(),u.colStride(),v.rowStride(),v.colStride()); l; ++l) {
    DComplex partialResult;
    rwzdot(l.length,u.data()+l.start1,l.stride1,v.data()+l.start2,l.stride2,&partialResult);
    result += partialResult;
  }
  return result;
}


DComplex prod(const DComplexGenMat& s)
{
  REGISTER DComplex t(1,0);
  for(MatrixLooper l(s.rows(),s.cols(),s.rowStride(),s.colStride()); l; ++l) {
    register DComplex* sp = (DComplex*)(s.data()+l.start);
    register int i = l.length;
    register j = l.stride;
    while (i--) { t *= *sp;  sp += j; }
  }
  return (DComplex)t;
}

DComplexGenMat pow(const DComplexGenMat& u, const DComplexGenMat& v)
// u to the v power.
{
  u.lengthCheck(v.rows(),v.cols());
  DComplexGenMat temp(v.rows(),v.cols(),rwUninitialized);
  register DComplex* dp = temp.data();
  for(DoubleMatrixLooper l(v.rows(),v.cols(),u.rowStride(),u.colStride(),v.rowStride(),v.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    register i = l.length;
    register DComplex* up = (DComplex*)(u.data()+l.start1);
    register DComplex* vp = (DComplex*)(v.data()+l.start2);
    register uj = l.stride1;
    register vj = l.stride2;
    while (i--) { *dp++ = pow(*up,*vp); up += uj; vp += vj; }
  }
  return temp;
}

DComplex sum(const DComplexGenMat& s)
{
  REGISTER DComplex t(0,0);
  for(MatrixLooper l(s.rows(),s.cols(),s.rowStride(),s.colStride()); l; ++l) {
    register DComplex* sp = (DComplex*)(s.data()+l.start);
    register int i = l.length;
    register j = l.stride;
    while (i--) { t += *sp;  sp += j; }
  }
  return (DComplex)t;
}

DComplex mean(const DComplexGenMat& s)
{
  DComplex theSum = sum(s);
  double numPoints = s.rows()*s.cols();
  if (theSum==DComplex(0,0)) return DComplex(0,0);   // Covers the case of zero points
  return theSum/DComplex(numPoints,0);
}

double variance(const DComplexGenMat& s)
// Variance of a DComplexGenMat
{
  s.numPointsCheck("Variance needs minimum of",2);
  register double sum2 = 0;	// Accumulate sums in double precision
  REGISTER DComplex avg = mean(s);
  for(MatrixLooper l(s.rows(),s.cols(),s.rowStride(),s.colStride()); l; ++l) {
    register DComplex* sp = (DComplex*)(s.data()+l.start);
    register int i = l.length;
    register j = l.stride;
    while (i--) {
      REGISTER DComplex temp = *sp - avg;
      sum2 += norm(temp);
      sp += j;
    }
  }
  // Use the biased estimate (easier to work with):
  return (double)(sum2/(s.rows()*s.cols()));
}

DComplexGenMat transpose(const DComplexGenMat& A)
{
  return A.slice(0,0, A.cols(),A.rows(), 0,1, 1,0);
}

DComplexVec product(const DComplexGenMat& A, const DComplexVec& x)
{
  int m = A.rows();
  int n = A.cols();
  x.lengthCheck(n);
  DComplexVec y(m,rwUninitialized);
  const DComplex* Aptr = A.data();
  for(int i=0; i<m; i++) {
    rwzdot(n,x.data(),x.stride(),Aptr,A.colStride(),&(y(i)));
    Aptr += A.rowStride();
  }
  return y;
}

DComplexGenMat transposeProduct(const DComplexGenMat& A, const DComplexGenMat& B)
{
  return product( transpose(A), B );
}

DComplexGenMat conjTransposeProduct(const DComplexGenMat& A, const DComplexGenMat& B)
{
  return product( conj(transpose(A)), B );
}

DComplexVec product(const DComplexVec& x, const DComplexGenMat& A)
{
  return product( transpose(A),x );
}

DComplexGenMat product(const DComplexGenMat& A, const DComplexGenMat& B)
{
  int m = A.rows();
  int n = B.cols();
  int r = A.cols();  // == B.rows() if A,B conform
  B.rowCheck(r);
  DComplexGenMat AB(m,n,rwUninitialized);
  DComplexVec Arow(r,rwUninitialized);
  for(int i=0; i<m; i++) {
    Arow = A.row(i);  // Compact this row to minimize page faults
    const DComplex* Bptr = B.data();
    for(int j=0; j<n; j++) {
      rwzdot(r,Arow.data(),1,Bptr,B.rowStride(),&(AB(i,j)));
      Bptr += B.colStride();
    }
  }
  return AB;
}

/*
 * Various functions with simple definitions:
 */
DComplexGenMat	cos(const DComplexGenMat& V)		{ return V.apply(::cos);  }
DComplexGenMat	cosh(const DComplexGenMat& V)	{ return V.apply(::cosh); }
DComplexGenMat	exp(const DComplexGenMat& V)		{ return V.apply(::exp);  }
DComplexGenMat	sin(const DComplexGenMat& V)		{ return V.apply(::sin);  }
DComplexGenMat	sinh(const DComplexGenMat& V)	{ return V.apply(::sinh); }
DoubleGenMat	arg(const DComplexGenMat& V)	{return V.apply2(::arg);  }
DoubleGenMat	norm(const DComplexGenMat& V)	{return V.apply2(::norm); }

#ifdef __ZTC__
/*
 * Zortech requires its own special version of sqrt(const DComplexGenMat&) 
 * because it uses an inlined version of sqrt().  Hence, apply() 
 * cannot be used.
 */
DComplexGenMat
sqrt(const DComplexGenMat& s)
// Square root of a DComplexGenMat --- Zortech version
{
  DComplexGenMat temp(s.rows(),s.cols(),rwUninitialized);
  register DComplex* dp = temp.data();
  for(MatrixLooper l(s.rows(),s.cols(),s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    register const DComplex* sp = s.data()+l.start;
    register i = l.length;
    register j = l.stride;
    while (i--) { *dp++ = sqrt(*sp);  sp += j; }
  }
  return temp;
}
#else
DComplexGenMat	sqrt(const DComplexGenMat& V)	{ return V.apply(::sqrt); }
#endif

    /**** Norm functions ****/

Double l1Norm(const DComplexGenMat& x)
{
  DoubleVec colNorms(x.cols(),rwUninitialized);
  for(int j=x.cols(); j--;) {
    colNorms(j) = l1Norm(x(RWAll,j));
  }
  return linfNorm(colNorms);
}
	  
Double linfNorm(const DComplexGenMat& x)
{
  return l1Norm(transpose(x));
}

Double maxNorm(const DComplexGenMat& x)     // Not really a norm!
{
  DoubleVec colNorms(x.cols(),rwUninitialized);
  for(int j=x.cols(); j--;) {
    colNorms(j) = linfNorm(x(RWAll,j));
  }
  return linfNorm(colNorms);
}

Double frobNorm(const DComplexGenMat& x)
{
  DoubleVec colNorms(x.cols(),rwUninitialized);
  for(int j=x.cols(); j--;) {
    colNorms(j) = l2Norm(x(RWAll,j));
  }
  return l2Norm(colNorms);
}


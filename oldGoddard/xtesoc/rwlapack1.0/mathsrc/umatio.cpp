/*
 * Definitions for UCharGenMat I/O
 *
 * Generated from template $Id: xmatio.cpp,v 1.5 1993/09/17 18:42:21 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/ucgenmat.h"
#include "rw/rwfile.h"
#include "rw/vstream.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"
STARTWRAP
#include <stdio.h>
#include <ctype.h>
ENDWRAP
#include <iomanip.h>

// read and write chars as integers:
#  define CAST Int
#  define CTYPE char

RCSID("$Header: /users/rcs/mathsrc/xmatio.cpp,v 1.5 1993/09/17 18:42:21 alv Exp $");

const unsigned short versionID = 2;		// Formatting version number


void UCharGenMat::printOn(ostream& s) const 
{
    int w = s.width((int)0);
    s << rows() << "x" << cols() << " [\n";
    for( int i=0; i<rows(); i++ ) {
      for( int j=0; j<cols(); j++ ) {
        CAST el = (*this)(i,j);
        s << setw(w) << el;
        if (j<(cols()-1)) { s << " "; }
      }
      s << "\n";
    }
    s << "]";
}

void UCharGenMat::scanFrom(istream& s)
{
  CAST item;
  char c;

  int m,n;				// Read in array dimensions
  s >> m;
  do { s.get(c); } while (isspace(c));	// Skip optional x character
  if (isdigit(c)) s.putback(c);	
  s >> n;

  do { s>>c; } while(isspace(c)); 	// Eat the leading [, if it exists
  if (c!='[') { s.putback(c); }
 
  reshape(m,n);
  for(int i=0; s.good() && i<rows(); i++) {
    for(int j=0; s.good() && j<cols(); j++) {
      s >> item;
      (*this)(i,j) = item;
    }
  }
  
  if (s.good()) {
    do { s.get(c); } while(s.good() && isspace(c));  // Eat the trailing ]
  }
  if (c!=']') { s.putback(c); }

  return;
}

ostream& operator<<(ostream& s, const UCharGenMat& m)
{
  m.printOn(s); return s;
}

istream& operator>>(istream& s, UCharGenMat& m)
{
  m.scanFrom(s); return s;
}

void UCharGenMat::saveOn(RWFile& file) const 
{
  file.Write(versionID);
  file.Write(nrows);
  file.Write(ncols);


  if( rowStride()==1 && colStride()==rows() ){
    const CTYPE* dataStart = (const CTYPE*)data();
    file.Write(dataStart, rows()*cols());
  }
  else {
    for(int j=0; j<cols(); j++) {
      for(int i=0; i<rows(); i++) {
        CTYPE item = (*this)(i,j);
        file.Write(item);
      }
    }
  }

}

void UCharGenMat::saveOn(RWvostream& s) const 
{
  s << versionID;		// Save the version number
  s << nrows << ncols; 		// Save the vector length


  if( rowStride()==1 && colStride()==rows() ){
    const CTYPE* dataStart = (const CTYPE*)data();
    s.put(dataStart, rows()*cols());
  }
  else {
    for(int j=0; j<cols(); j++) {
      for(int i=0; i<rows(); i++) {
        CTYPE item = (*this)(i,j);
        s << item;
      }
    }
  }
}

void UCharGenMat::restoreFrom(RWFile& file)
{
  // Get and check the version number
  unsigned short ID;
  file.Read(ID);

  if( ID!=versionID && ID!=340 ){
    versionErr((int)versionID, (int)ID);
  }

  // Get the length and resize to it:
  unsigned m,n;
  file.Read(m);
  file.Read(n);
  reshape(m,n);


  // Scoop up the whole vector in one read:
  CTYPE* dataStart = (CTYPE*) data();
  file.Read(dataStart, rows()*cols());
}

void UCharGenMat::restoreFrom(RWvistream& s)
{
  // Get and check the version number
  unsigned short ID;
  s >> ID;

  if( ID!=versionID && ID!=340 ){
    versionErr((int)versionID, (int)ID);
  }

  // Get the length and resize to it:
  unsigned m,n;
  s >> m >> n;
  reshape(m,n);


  // Scoop up the whole vector in one read:
  CTYPE* dataStart = (CTYPE*) data();
  s.get(dataStart, rows()*cols());
}

unsigned UCharGenMat::binaryStoreSize() const
{
  // Total storage requirements = Number of elements times their size, 
  // plus space for the vector length and ID number:
  return rows()*cols()*sizeof(UChar) + 2*sizeof(unsigned) + sizeof(versionID);
}

> #
> # $Header: /users/rcs/mathsrc/xmatmath.cpp,v 1.8 1993/09/16 17:16:56 alv Exp $
> #
> # $Log: xmatmath.cpp,v $
> # Revision 1.8  1993/09/16  17:16:56  alv
> # added norm functions
> #
> # Revision 1.7  1993/09/01  16:27:55  alv
> # now uses rwUninitialized form for simple constructors
> #
> # Revision 1.6  1993/08/04  22:03:20  dealys
> # ported to MPW C++ 3.3
> #
> # Revision 1.5  1993/05/10  19:29:27  alv
> # ported to Zortech 3.1
> #
> # Revision 1.4  1993/03/22  15:51:31  alv
> # changed PVCS Workfile keyword to RCS ID keyword
> #
> # Revision 1.3  1993/03/12  20:01:12  alv
> # removed refs to BCC_STRUCT_POINTER_BUG
> #
> # Revision 1.2  1993/01/27  20:20:48  alv
> # eliminated mean() for integer array types
> #
> # Revision 1.1  1993/01/22  23:51:08  alv
> # Initial revision
> #
> # 
> define <ClassType>=GenMat
> include macros
/*
 * Math definitions for <C>
 *
 * Generated from template $Id: xmatmath.cpp,v 1.8 1993/09/16 17:16:56 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/<a>genmat.h"
#include "rw/<a>vec.h"
#include "rw/rw<bla>bla.h"

> if <C>==IntVec || <C>==SCharVec
#include <stdlib.h>	/* Looking for abs(int) */
> endif

> if <R/C>==Complex
// Complexes won't fit in a register
#define REGISTER
> else
#define REGISTER register
> endif

RCSID("$Header: /users/rcs/mathsrc/xmatmath.cpp,v 1.8 1993/09/16 17:16:56 alv Exp $");

> if <C>==DComplexGenMat || <C>==FComplexGenMat || <C>==IntGenMat
// Absolute value of a <C>
<P>GenMat abs(const <C>& s)
{
  <P>GenMat temp(s.rows(),s.cols(),rwUninitialized);
  register <P>* dp = temp.data();
  for(MatrixLooper l(s.rows(),s.cols(),s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    register <t>* sp = (<t>*)(s.data()+l.start); // Cast away "constness" to avoid problems with abs()
    register j = l.stride;
    register i = l.length;
    while (i--) { *dp++ = abs(*sp); sp += j; }
  }
  return temp;
}
> endif

> if <C>==SCharGenMat
<C> abs(const <C>& s)
// Absolute value of a <C>
{
  <C> temp(s.rows(),s.cols(),rwUninitialized);
  register <t>* dp = temp.data();
  for(MatrixLooper l(s.rows(),s.cols(),s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    register const <t>* sp = s.data()+l.start;
    register i = l.length;
    register j = l.stride;
    while (i--) { *dp++ = abs(int(*sp));  sp += j; }
  }
  return temp;
}
> endif

> if <R/C>==Complex
<C> <C>::apply(CmathFunTy f) const
// Apply function returning <t> to each element of vector slice
{
  <C> temp(nrows,ncols,rwUninitialized);
  register <t>* dp = temp.data();
  for(MatrixLooper l(nrows,ncols,rowstep,colstep,RWDataView::COLUMN_MAJOR); l; ++l) {
    register i = l.length;
    register <t>* sp = (<t>*)(data()+l.start);
    register j = l.stride;
    while(i--) {
// Turbo C++ bug requires temporary.
      <t> dummy = (*f)(*sp);
      *dp++ = dummy;
      sp += j; 
    }
  }
  return temp;
}

<P>GenMat <C>::apply2(CmathFunTy2 f) const
// Apply function returning <P> to each element of vector slice
{
  <P>GenMat temp(nrows,ncols,rwUninitialized);
  register <P>* dp = temp.data();
  for(MatrixLooper l(nrows,ncols,rowstep,colstep,RWDataView::COLUMN_MAJOR); l; ++l) {
    register <t>* sp = (<t>*)(data()+l.start);
    register i = l.length;
    register j = l.stride;
    while (i--) { *dp++ = (*f)(*sp);  sp += j; }
  }
  return temp;
}
> elif <C>==DoubleGenMat || <C>==FloatGenMat

<C> <C>::apply(mathFunTy f) const
// Apply function returning <t> to each element of vector slice
{
  <C> temp(nrows,ncols,rwUninitialized);
  register <t>* dp = temp.data();
  for(MatrixLooper l(nrows,ncols,rowstep,colstep,RWDataView::COLUMN_MAJOR); l; ++l) {
    register const <t>* sp = data()+l.start;
    register i = l.length;
    register j = l.stride;
    while(i--) {
// Turbo C++ bug requires temporary.
#if defined (__TURBOC__)
      <t> dummy = (*f)(*sp);
      *dp++ = dummy;
#else
      *dp++ = (*f)(*sp);  
#endif
      sp += j; 
    }
  }
  return temp;
}

#if defined(RW_NATIVE_EXTENDED)
 <C> <C>::apply(XmathFunTy f) const
// Apply function for compilers using extended types
{
  <C> temp(nrows,ncols);
  register <t>* dp = temp.data();
  for(MatrixLooper l(nrows,ncols,rowstep,colstep,RWDataView::COLUMN_MAJOR); l; ++l) {
    register const <t>* sp = data()+l.start;
    register i = l.length;
    register j = l.stride;
    while(i--) {
      *dp++ = (*f)(*sp);
      sp += j;
    }
  }
  return temp;
}
#endif
> endif

> if <R/C>==Complex
#  ifdef COMPLEX_PACKS

/*
 * If we can guarantee that the complex vector goes
 * (real, imag, real, imag, real, imag, etc.) then
 * it becomes possible to implement real(<C>&) and imag(<C>&)
 * via a slice, allowing its use as an lvalue.  What
 * follows violates encapsulation (a bit), but sure is useful.
 */

<P>GenMat real(const <C>& v)
{
#ifdef IMAG_LEADS
  return <P>GenMat(v, (<P>*)v.begin + 1, v.rows(),v.cols(), 2*v.rowStride(),2*v.colStride());
#else
  return <P>GenMat(v, (<P>*)v.begin, v.rows(),v.cols(), 2*v.rowStride(),2*v.colStride());
#endif
}
<P>GenMat imag(const <C>& v)
{
#ifdef IMAG_LEADS
  return <P>GenMat(v, (<P>*)v.begin, v.rows(),v.cols(), 2*v.rowStride(),2*v.colStride());
#else
  return <P>GenMat(v, (<P>*)v.begin + 1, v.rows(),v.cols(), 2*v.rowStride(),2*v.colStride());
#endif
}

// The above allows a very compact form of conj() to be written:
<C>
conj(const <C>& V)
{
  return <C>(real(V), -imag(V));
}

#else	/* Complex does not pack */

<P>GenMat real(const <C>& v)
{
  <P>GenMat temp(nrows,ncols);
  register <P>* dp = temp.data();
  for(MatrixLooper l(nrows,ncols,rowstep,colstep,RWDataView::COLUMN_MAJOR); l; ++l) {
    register i = l.length;
    register const <t>* sp = v.data()+l.start;
    register j = l.stride;
    while(i--) {*dp++ = real(*sp); sp += j;}
  }
  return temp;
}

<P>GenMat imag(const <C>& v)
{
  <P>GenMat temp(nrows,ncols);
  register <P>* dp = temp.data();
  for(MatrixLooper l(nrows,ncols,rowstep,colstep,RWDataView::COLUMN_MAJOR); l; ++l) {
    register i = l.length;
    register const <t>* sp = v.data()+l.start;
    register j = l.stride;
    while(i--) {*dp++ = imag(*sp); sp += j;}
  }
  return temp;
}

<C>	conj(const <C>& V)	{return V.apply(::conj); }

#endif /* Not COMPLEX_PACKS */

> # End of complex section:
> endif

> if <C>==DoubleGenMat || <C>==FloatGenMat
<C> atan2(const <C>& u,const <C>& v)
     // Arctangent of u/v.
{
  u.lengthCheck(v.rows(),v.cols());
  <C> temp(v.rows(),v.cols(),rwUninitialized);
  register <t>* dp = temp.data();
  for(DoubleMatrixLooper l(v.rows(),v.cols(),u.rowStride(),u.colStride(),v.rowStride(),v.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    register const <t>* up = u.data()+l.start1;
    register const <t>* vp = v.data()+l.start2;
    register i = l.length;
    register uj = l.stride1;
    register vj = l.stride2;
    while (i--) { *dp++ = atan2(*up,*vp);  up += uj;  vp += vj; }
  }
  return temp;
}
> endif

<t> dot(const <C>& u, const <C>& v)
      // dot product: sum_ij (u_ij * v_ij)
{
  u.lengthCheck(v.rows(),v.cols());
> if <R/C>==Complex
  <t> result(0,0);
> else
  <t> result = 0;
> endif
  for(DoubleMatrixLooper l(v.rows(),v.cols(),u.rowStride(),u.colStride(),v.rowStride(),v.colStride()); l; ++l) {
    <t> partialResult;
    rw<bla>dot(l.length,u.data()+l.start1,l.stride1,v.data()+l.start2,l.stride2,&partialResult);
    result += partialResult;
  }
  return result;
}

> if <R/C>==Real
void maxIndex(const <C>& s, int *maxi, int *maxj)
// Index of maximum element.
{
  int len = s.rows()*s.cols();
  s.numPointsCheck("max needs at least",1);
  if (s.colStride()==s.rowStride()*s.rows()) {    // Column major order
    int maxindex = rw<bla>max(len,s.data(),s.rowStride());
    *maxj = maxindex/s.rows();
    *maxi = maxindex%s.rows();
    return;
  }
  if (s.rowStride()==s.colStride()*s.cols()) {    // Row major order
    int maxindex = rw<bla>max(len,s.data(),s.colStride());
    *maxj = maxindex/s.cols();
    *maxi = maxindex%s.cols();
    return;
  }
  *maxi = *maxj = 0;			    // Matrix is not contiguous
  <t> themax = s(0,0);
  if (s.colStride()>s.rowStride()) {
    for( int j=0; j<s.cols(); j++ ) {  // Do a column at a time
      int i = rw<bla>max(len,s.data()+j*s.colStride(),s.rowStride());
      if (s(i,j)>themax) { themax=s(i,j); *maxi=i; *maxj=j; }
    }
  } else {
    for( int i=0; i<s.rows(); i++ ) {  // Do a row at a time
      int j = rw<bla>max(len,s.data()+i*s.rowStride(),s.colStride());
      if (s(i,j)>themax) { themax=s(i,j); *maxi=i; *maxj=j; }
    }
  }
}

<t> maxValue(const <C>& s)
{
  int i,j;
  maxIndex(s,&i,&j);
  return s(i,j);
}
    
void minIndex(const <C>& s, int *mini, int *minj)
// Index of minimum element.
{
  int len = s.rows()*s.cols();
  s.numPointsCheck("min needs at least",1);
  if (s.colStride()==s.rowStride()*s.rows()) {    // Column major order
    int minindex = rw<bla>min(len,s.data(),s.rowStride());
    *minj = minindex/s.rows();
    *mini = minindex%s.rows();
    return;
  }
  if (s.rowStride()==s.colStride()*s.cols()) {    // Row major order
    int minindex = rw<bla>min(len,s.data(),s.colStride());
    *minj = minindex/s.cols();
    *mini = minindex%s.cols();
    return;
  }
  *mini = *minj = 0;			    // Matrix is not contiguous
  <t> themin = s(0,0);
  if (s.colStride()>s.rowStride()) {
    for( int j=0; j<s.cols(); j++ ) {  // Do a column at a time
      int i = rw<bla>min(len,s.data()+j*s.colStride(),s.rowStride());
      if (s(i,j)>themin) { themin=s(i,j); *mini=i; *minj=j; }
    }
  } else {
    for( int i=0; i<s.rows(); i++ ) {  // Do a row at a time
      int j = rw<bla>min(len,s.data()+i*s.rowStride(),s.colStride());
      if (s(i,j)>themin) { themin=s(i,j); *mini=i; *minj=j; }
    }
  }
}
    
<t> minValue(const <C>& s)
{
  int i,j;
  minIndex(s,&i,&j);
  return s(i,j);
}

> endif

<t> prod(const <C>& s)
{
> if <C>==FloatGenMat
  register double t = 1;	// Accumulate prods in double precision
> elif <R/C>==Complex 
  REGISTER <t> t(1,0);
> else
  REGISTER <t> t = 1;
> endif
  for(MatrixLooper l(s.rows(),s.cols(),s.rowStride(),s.colStride()); l; ++l) {
    register <t>* sp = (<t>*)(s.data()+l.start);
    register int i = l.length;
    register j = l.stride;
    while (i--) { t *= *sp;  sp += j; }
  }
  return (<t>)t;
}

> if <P>==Double || <P>==Float
<C> pow(const <C>& u, const <C>& v)
// u to the v power.
{
  u.lengthCheck(v.rows(),v.cols());
  <C> temp(v.rows(),v.cols(),rwUninitialized);
  register <t>* dp = temp.data();
  for(DoubleMatrixLooper l(v.rows(),v.cols(),u.rowStride(),u.colStride(),v.rowStride(),v.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    register i = l.length;
    register <t>* up = (<t>*)(u.data()+l.start1);
    register <t>* vp = (<t>*)(v.data()+l.start2);
    register uj = l.stride1;
    register vj = l.stride2;
    while (i--) { *dp++ = pow(*up,*vp); up += uj; vp += vj; }
  }
  return temp;
}
> endif

<t> sum(const <C>& s)
{
> if <C>==FloatGenMat
  register double t = 0;	// Accumulate sums in double precision
> else
> if <R/C>==Complex
  REGISTER <t> t(0,0);
> else
  REGISTER <t> t = 0;
> endif
> endif
  for(MatrixLooper l(s.rows(),s.cols(),s.rowStride(),s.colStride()); l; ++l) {
    register <t>* sp = (<t>*)(s.data()+l.start);
    register int i = l.length;
    register j = l.stride;
    while (i--) { t += *sp;  sp += j; }
  }
  return (<t>)t;
}

> if <P>==Double || <P>==Float
<t> mean(const <C>& s)
{
  <t> theSum = sum(s);
  <p> numPoints = s.rows()*s.cols();
> if <R/C>==Complex
  if (theSum==<t>(0,0)) return <t>(0,0);   // Covers the case of zero points
  return theSum/<t>(numPoints,0);
> else
  if (theSum==0) return 0;   // Covers the case of zero points
  return theSum/numPoints;
> endif
}
> endif

> if <P>==Double || <P>==Float
<p> variance(const <C>& s)
// Variance of a <C>
{
  s.numPointsCheck("Variance needs minimum of",2);
  register double sum2 = 0;	// Accumulate sums in double precision
  REGISTER <t> avg = mean(s);
  for(MatrixLooper l(s.rows(),s.cols(),s.rowStride(),s.colStride()); l; ++l) {
    register <t>* sp = (<t>*)(s.data()+l.start);
    register int i = l.length;
    register j = l.stride;
    while (i--) {
      REGISTER <t> temp = *sp - avg;
> if <R/C>==Complex
      sum2 += norm(temp);
> else
      sum2 += temp * temp;
> endif
      sp += j;
    }
  }
  // Use the biased estimate (easier to work with):
  return (<p>)(sum2/(s.rows()*s.cols()));
}
> endif

<C> transpose(const <C>& A)
{
  return A.slice(0,0, A.cols(),A.rows(), 0,1, 1,0);
}

<T>Vec product(const <C>& A, const <T>Vec& x)
{
  int m = A.rows();
  int n = A.cols();
  x.lengthCheck(n);
  <T>Vec y(m,rwUninitialized);
  const <t>* Aptr = A.data();
  for(int i=0; i<m; i++) {
    rw<bla>dot(n,x.data(),x.stride(),Aptr,A.colStride(),&(y(i)));
    Aptr += A.rowStride();
  }
  return y;
}

<C> transposeProduct(const <C>& A, const <C>& B)
{
  return product( transpose(A), B );
}

> if <R/C>==Complex
<C> conjTransposeProduct(const <C>& A, const <C>& B)
{
  return product( conj(transpose(A)), B );
}
> endif

<T>Vec product(const <T>Vec& x, const <C>& A)
{
  return product( transpose(A),x );
}

<C> product(const <C>& A, const <C>& B)
{
  int m = A.rows();
  int n = B.cols();
  int r = A.cols();  // == B.rows() if A,B conform
  B.rowCheck(r);
  <C> AB(m,n,rwUninitialized);
  <T>Vec Arow(r,rwUninitialized);
  for(int i=0; i<m; i++) {
    Arow = A.row(i);  // Compact this row to minimize page faults
    const <t>* Bptr = B.data();
    for(int j=0; j<n; j++) {
      rw<bla>dot(r,Arow.data(),1,Bptr,B.rowStride(),&(AB(i,j)));
      Bptr += B.colStride();
    }
  }
  return AB;
}

> if <P>==Double || <P>==Float
/*
 * Various functions with simple definitions:
 */
> endif
> if <T>==Double || <T>==Float
<C>	acos(const <C>& V)	{ return V.apply(::acos); }
<C>	asin(const <C>& V)	{ return V.apply(::asin); }
<C>	atan(const <C>& V)	{ return V.apply(::atan); }
<C>	ceil(const <C>& V)	{ return V.apply(::ceil); }
> endif
> if <P>==Double || <P>==Float
<C>	cos(const <C>& V)		{ return V.apply(::cos);  }
<C>	cosh(const <C>& V)	{ return V.apply(::cosh); }
> endif
> if <P>==Double || <P>==Float
<C>	exp(const <C>& V)		{ return V.apply(::exp);  }
> endif
> if <T>==Double || <T>==Float
<C>	floor(const <C>& V)	{ return V.apply(::floor);}
> endif
> if <T>==Double || <T>==Float
<C>	log(const <C>& V)		{ return V.apply(::log);  }
<C>	log10(const <C>& V)	{ return V.apply(::log10);}
> endif
> if <P>==Double || <P>==Float
<C>	sin(const <C>& V)		{ return V.apply(::sin);  }
<C>	sinh(const <C>& V)	{ return V.apply(::sinh); }
> endif
> if <T>==Double || <T>==Float
<C>	tan(const <C>& V)		{ return V.apply(::tan);  }
<C>	tanh(const <C>& V)	{ return V.apply(::tanh); }
> endif
> if <R/C>==Complex
<P>GenMat	arg(const <C>& V)	{return V.apply2(::arg);  }
<P>GenMat	norm(const <C>& V)	{return V.apply2(::norm); }
> endif

> if <T>==Double || <T>==Float
#ifdef __ZTC__
/*
 * Zortech requires its own special version of abs(const <C>&)
 * because it uses an inlined version of fabs().  Hence, apply() 
 * cannot be used.
 */
<C>
abs(const <C>& s)
// Absolute value of a <C> --- Zortech version
{
  <C> temp(s.rows(),s.cols(),rwUninitialized);
  register <t>* dp = temp.data();
  for(MatrixLooper l(s.rows(),s.cols(),s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    register const <t>* sp = s.data()+l.start;
    register i = l.length;
    register j = l.stride;
    while (i--) { *dp++ = fabs(*sp);  sp += j; }
  }
  return temp;
}
#else   /* Not Zortech */
<C>	abs(const <C>& V)		{ return V.apply(::fabs); }
#endif	
> endif
> 
> if <P>==Double || <P>==Float
#ifdef __ZTC__
/*
 * Zortech requires its own special version of sqrt(const <C>&) 
 * because it uses an inlined version of sqrt().  Hence, apply() 
 * cannot be used.
 */
<C>
sqrt(const <C>& s)
// Square root of a <C> --- Zortech version
{
  <C> temp(s.rows(),s.cols(),rwUninitialized);
  register <t>* dp = temp.data();
  for(MatrixLooper l(s.rows(),s.cols(),s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    register const <t>* sp = s.data()+l.start;
    register i = l.length;
    register j = l.stride;
    while (i--) { *dp++ = sqrt(*sp);  sp += j; }
  }
  return temp;
}
#else
<C>	sqrt(const <C>& V)	{ return V.apply(::sqrt); }
#endif
> endif

> if <T>==Double || <T>==Float || <T>==DComplex
    /**** Norm functions ****/

<P> l1Norm(const <C>& x)
{
  <P>Vec colNorms(x.cols(),rwUninitialized);
  for(int j=x.cols(); j--;) {
    colNorms(j) = l1Norm(x(RWAll,j));
  }
  return linfNorm(colNorms);
}
	  
<P> linfNorm(const <C>& x)
{
  return l1Norm(transpose(x));
}

<P> maxNorm(const <C>& x)     // Not really a norm!
{
  <P>Vec colNorms(x.cols(),rwUninitialized);
  for(int j=x.cols(); j--;) {
    colNorms(j) = linfNorm(x(RWAll,j));
  }
  return linfNorm(colNorms);
}

<P> frobNorm(const <C>& x)
{
  <P>Vec colNorms(x.cols(),rwUninitialized);
  for(int j=x.cols(); j--;) {
    colNorms(j) = l2Norm(x(RWAll,j));
  }
  return l2Norm(colNorms);
}

> endif

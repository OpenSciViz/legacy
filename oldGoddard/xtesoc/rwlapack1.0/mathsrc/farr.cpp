/*
 * Definitions for FloatArray
 *
 * Generated from template $Id: xarr.cpp,v 1.12 1993/10/04 21:44:01 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1992. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include <rw/strstrea.h>  /* I use istrstream to parse character string ctor */
#include "rw/farr.h"
#include "rw/fgenmat.h"
#include "rw/fvec.h"
#include "rw/rwbla.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"
#include "rw/rand.h"

#define REGISTER register


RCSID("$Header");

FloatArray::FloatArray(const char *string)
 : RWArrayView(IntVec(),sizeof(float))
{
  istrstream s((char*)string);
  scanFrom(s);
  if (!s.good()) {
    RWTHROW( RWExternalErr( RWMessage(RWMATH_STRCTOR) ));
  }
}

FloatArray::FloatArray()
  : RWArrayView(IntVec(),sizeof(float))
{
}

FloatArray::FloatArray(const IntVec& n, RWUninitialized, Storage storage)
  : RWArrayView(n,sizeof(float),storage)
{
}

FloatArray::FloatArray(unsigned nx, unsigned ny, unsigned nz, RWUninitialized)
  : RWArrayView(nx,ny,nz,sizeof(float))
{
}

FloatArray::FloatArray(unsigned nx, unsigned ny, unsigned nz, unsigned nw, RWUninitialized)
  : RWArrayView(nx,ny,nz,nw,sizeof(float))
{
}

static void randFill(FloatArray& X, RWRand& r)
{
  for(RWMultiIndex i(X.length()); i; ++i) {
    X(i) = (float)r();
  }
}

FloatArray::FloatArray(const IntVec& n, RWRand& r, Storage storage)
  : RWArrayView(n,sizeof(float),storage)
{
  randFill(*this,r);
}

FloatArray::FloatArray(unsigned nx, unsigned ny, unsigned nz, RWRand& r)
  : RWArrayView(nx,ny,nz,sizeof(float))
{
  randFill(*this,r);
}

FloatArray::FloatArray(unsigned nx, unsigned ny, unsigned nz, unsigned nw, RWRand& r)
  : RWArrayView(nx,ny,nz,nw,sizeof(float))
{
  randFill(*this,r);
}

FloatArray::FloatArray(const IntVec& n, float scalar)
  : RWArrayView(n,sizeof(float))
{
  float scalar2 = scalar;  // Temporary required to finesse K&R compilers
  rwsset(prod(n), data(), 1, &scalar2);
}

FloatArray::FloatArray(unsigned nx, unsigned ny, unsigned nz, float scalar)
  : RWArrayView(nx,ny,nz,sizeof(float))
{
  float scalar2 = scalar;  // Temporary required to finesse K&R compilers
  rwsset(nx*ny*nz, data(), 1, &scalar2);
}

FloatArray::FloatArray(unsigned nx, unsigned ny, unsigned nz, unsigned nw, float scalar)
  : RWArrayView(nx,ny,nz,nw,sizeof(float))
{
  float scalar2 = scalar;  // Temporary required to finesse K&R compilers
  rwsset(nx*ny*nz*nw, data(), 1, &scalar2);
}


// Copy constructor
FloatArray::FloatArray(const FloatArray& a)
  : RWArrayView(a)
{
}

FloatArray::FloatArray(const float* dat, const IntVec& n)
  : RWArrayView(n,sizeof(float))
{
  rwscopy(prod(n), data(), 1, dat, 1);
}

FloatArray::FloatArray(const float* dat, unsigned nx, unsigned ny, unsigned nz)
  : RWArrayView(nx,ny,nz,sizeof(float))
{
  rwscopy(nx*ny*nz, data(), 1, dat, 1);
}

FloatArray::FloatArray(const float* dat, unsigned nx, unsigned ny, unsigned nz, unsigned nw)
  : RWArrayView(nx,ny,nz,nw,sizeof(float))
{
  rwscopy(nx*ny*nz*nw, data(), 1, dat, 1);
}

FloatArray::FloatArray(const FloatVec& dat, const IntVec& n)
  : RWArrayView(n,sizeof(float))
{
  rwscopy(prod(n), data(), 1, dat.data(), dat.stride());
}

FloatArray::FloatArray(const FloatVec& dat, unsigned nx, unsigned ny, unsigned nz)
  : RWArrayView(nx,ny,nz,sizeof(float))
{
  rwscopy(nx*ny*nz, data(), 1, dat.data(), dat.stride());
}

FloatArray::FloatArray(const FloatVec& dat, unsigned nx, unsigned ny, unsigned nz, unsigned nw)
  : RWArrayView(nx,ny,nz,nw,sizeof(float))
{
  rwscopy(nx*ny*nz*nw, data(), 1, dat.data(), dat.stride());
}

FloatArray::FloatArray(RWBlock *block, const IntVec& n)
  : RWArrayView(block,n)
{
  unsigned minlen = prod(length())*sizeof(float);
  if (block->length() < minlen) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_SHORTBLOCK, block->length(), minlen) ));
  }
}

FloatArray::FloatArray(RWBlock *block, unsigned nx, unsigned ny, unsigned nz)
  : RWArrayView(block, nx, ny, nz)
{
  unsigned minlen = prod(length())*sizeof(float);
  if (block->length() < minlen) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_SHORTBLOCK, block->length(), minlen) ));
  }
}

FloatArray::FloatArray(RWBlock *block, unsigned nx, unsigned ny, unsigned nz, unsigned nw)
  : RWArrayView(block, nx, ny, nz, nw)
{
  unsigned minlen = prod(length())*sizeof(float);
  if (block->length() < minlen) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_SHORTBLOCK, block->length(), minlen) ));
  }
}

FloatArray::FloatArray(const FloatVec& v)
  : RWArrayView(v,(void*)v.data(),IntVec(1,v.length()),IntVec(1,1))
{
}

FloatArray::FloatArray(const FloatGenMat& m)
  : RWArrayView(m,(void*)m.data(),
       IntVec(2,m.rows(),m.cols()-m.rows()),                 // [ m.rows() m.cols() ]
       IntVec(2,m.rowStride(),m.colStride()-m.rowStride())) // [ m.rowStride() m.colStride() ]
{
}

// This constructor is used internally by real() and imag() and slices:
FloatArray::FloatArray(const RWDataView& b, float* start, const IntVec& n, const IntVec& str)
  : RWArrayView(b,start,n,str)
{
}

FloatArray& FloatArray::operator=(const FloatArray& rhs)
{
  lengthCheck(rhs.length());
  if(sameDataBlock(rhs)) {
    (*this) = rhs.deepCopy();   // Avoid aliasing problems
  } else {
    for(DoubleArrayLooper l(npts,step,rhs.step); l; ++l) {
      rwscopy(l.length, data()+l.start1, l.stride1, rhs.data()+l.start2, l.stride2);
    }
  }
  return *this;
}

FloatArray& FloatArray::operator=(float scalar)
{
  for(ArrayLooper l(npts,step); l; ++l) {
    float scalar2 = scalar;  // Temporary required to finesse K&R compilers
    rwsset(l.length,data()+l.start,l.stride,&scalar2);
  }
  return *this;
}

FloatArray& FloatArray::reference(const FloatArray& v)
{
  RWArrayView::reference(v);
  return *this;
}

// Return copy of self with distinct instance variables
FloatArray FloatArray::deepCopy() const
{
  return copy();
}

// Synonym for deepCopy().  Not made inline because some compilers
// have trouble with inlined temporaries with destructors
FloatArray FloatArray::copy() const
{
  FloatArray temp(npts,rwUninitialized,COLUMN_MAJOR);
  temp = *this;
  return temp;
}

// Guarantee that references==1 and stride==1
void FloatArray::deepenShallowCopy()
{
  RWBoolean isStrideCompact = (dimension()==0 || step((int)0)==1);
  for( int r=npts.length()-1; r>0 && isStrideCompact; --r ) {
    if (step(r) != npts(r-1)*step(r-1)) isStrideCompact=FALSE;
  }
  if (!isStrideCompact || !isSimpleView()) {
    RWArrayView::reference(copy());
  }
}

void FloatArray::resize(const IntVec& N)
{
  if(N != npts){
    FloatArray temp(N,(float)0);
    int p = npts.length()<N.length() ? npts.length() : N.length();  // # dimensions to copy
    if (p>0) {
      IntVec n(p,rwUninitialized);   // The size of the common parts of the arrays
      for(int i=0; i<p; i++) {
        n(i) = (N(i)<npts(i)) ? N(i) : npts(i);
      }
  
      // Build a view of the source common part
      IntGenMat sstr(dimension(),p,0);
      sstr.diagonal() = 1;
      FloatArray sp = slice(IntVec(dimension(),0), n, sstr);
  
      // Build a view of the destination common part
      IntGenMat dstr(N.length(),p,0);
      dstr.diagonal() = 1;
      FloatArray dp = temp.slice(IntVec(N.length(),0), n, dstr);
  
      dp = sp;
    }
    reference(temp);
  }
}

void FloatArray::resize(unsigned m, unsigned n, unsigned o)
{
  IntVec i(3,rwUninitialized);
  i((int)0) = m;
  i(1) = n;
  i(2) = o;
  resize(i);
}

void FloatArray::resize(unsigned m, unsigned n, unsigned o, unsigned p)
{
  IntVec i(4,rwUninitialized);
  i((int)0) = m;
  i(1) = n;
  i(2) = o;
  i(3) = p;
  resize(i);
}

void FloatArray::reshape(const IntVec& N)
{
  if (N!=npts) {
    FloatArray temp(N,rwUninitialized,COLUMN_MAJOR);
    reference(temp);
  }
}

void FloatArray::reshape(unsigned m, unsigned n, unsigned o)
{
  IntVec i(3,rwUninitialized);
  i((int)0) = m;
  i(1) = n;
  i(2) = o;
  reshape(i);
}

void FloatArray::reshape(unsigned m, unsigned n, unsigned o, unsigned p)
{
  IntVec i(4,rwUninitialized);
  i((int)0) = m;
  i(1) = n;
  i(2) = o;
  i(3) = p;
  reshape(i);
}

FloatArray FloatArray::slice(const IntVec& start, const IntVec& n, const IntGenMat& str) const
{
  sliceCheck(start, n, str);
  return FloatArray(*this, (float*)data()+dot(start,stride()), n, product(stride(),str));
}

float toScalar(const FloatArray& A)
{
  A.dimensionCheck(0);
  return *(A.data());
}

FloatVec toVec(const FloatArray& A)
{
  A.dimensionCheck(1);
  return FloatVec(A,(float*)A.data(),(unsigned)A.length((int)0),A.stride((int)0));
}

FloatGenMat toGenMat(const FloatArray& A)
{
  A.dimensionCheck(2);
  return FloatGenMat(A,(float*)A.data(),(unsigned)A.length((int)0),(unsigned)A.length(1),A.stride((int)0),A.stride(1));
}


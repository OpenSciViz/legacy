/*
 * Math definitions
 *
 * Generated from template $Id: xarrmath.cpp,v 1.10 1993/09/20 04:02:38 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/rwzbla.h"
#include "rw/carr.h"
#include "rw/cvec.h"


// Complexes won't fit in a register
#define REGISTER

RCSID("$Header: /users/rcs/mathsrc/xarrmath.cpp,v 1.10 1993/09/20 04:02:38 alv Exp $");

// Absolute value of a DComplexArray
DoubleArray abs(const DComplexArray& s)
{
  DoubleArray temp(s.length(),rwUninitialized);
  register Double* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    register DComplex* sp = (DComplex*)(s.data()+l.start); // Cast away "constness" to avoid problems with abs()
    register j = l.stride;
    register i = l.length;
    while (i--) { *dp++ = abs(*sp); sp += j; }
  }
  return temp;
}


DComplexArray DComplexArray::apply(CmathFunTy f) const
// Apply function returning DComplex to each element of vector slice
{
  DComplexArray temp(npts,rwUninitialized,RWDataView::COLUMN_MAJOR);
  register DComplex* dp = temp.data();
  for(ArrayLooper l(npts,step); l; ++l) {
    register i = l.length;
    register DComplex* sp = (DComplex*)(data()+l.start);
    register j = l.stride;
    while(i--) {
// Turbo C++ bug requires temporary.
      DComplex dummy = (*f)(*sp);
      *dp++ = dummy;
      sp += j; 
    }
  }
  return temp;
}

DoubleArray DComplexArray::apply2(CmathFunTy2 f) const
// Apply function returning Double to each element of vector slice
{
  DoubleArray temp(npts,rwUninitialized,RWDataView::COLUMN_MAJOR);
  register Double* dp = temp.data();
  for(ArrayLooper l(npts,step); l; ++l) {
    register DComplex* sp = (DComplex*)(data()+l.start);
    register i = l.length;
    register j = l.stride;
    while (i--) { *dp++ = (*f)(*sp);  sp += j; }
  }
  return temp;
}

#  ifdef COMPLEX_PACKS

/*
 * If we can guarantee that the complex vector goes
 * (real, imag, real, imag, real, imag, etc.) then
 * it becomes possible to implement real(DComplexArray&) and imag(DComplexArray&)
 * via a slice, allowing its use as an lvalue.  What
 * follows violates encapsulation (a bit), but sure is useful.
 */

DoubleArray real(const DComplexArray& v)
{
#ifdef IMAG_LEADS
  return DoubleArray(v, (Double*)v.begin + 1, v.length(), 2*v.stride());
#else
  return DoubleArray(v, (Double*)v.begin, v.length(), 2*v.stride());
#endif
}
DoubleArray imag(const DComplexArray& v)
{
#ifdef IMAG_LEADS
  return DoubleArray(v, (Double*)v.begin, v.length(), 2*v.stride());
#else
  return DoubleArray(v, (Double*)v.begin + 1, v.length(), 2*v.stride());
#endif
}

// The above allows a very compact form of conj() to be written:
DComplexArray
conj(const DComplexArray& V)
{
  return DComplexArray(real(V), -imag(V));
}

#else   /* Complex does not pack */

DoubleArray real(const DComplexArray& v)
{
  DoubleArray temp(npts,rwUninitialized,RWDataView::COLUMN_MAJOR);
  register Double* dp = temp.data();
  for(ArrayLooper l(npts,step); l; ++l) {
    register i = l.length;
    register const DComplex* sp = v.data()+l.start;
    register j = l.stride;
    while(i--) {*dp++ = real(*sp); sp += j;}
  }
  return temp;
}

DoubleArray imag(const DComplexArray& v)
{
  DoubleArray temp(npts,rwUninitialized,RWDataView::COLUMN_MAJOR);
  register Double* dp = temp.data();
  for(ArrayLooper l(npts,step); l; ++l) {
    register i = l.length;
    register const DComplex* sp = v.data()+l.start;
    register j = l.stride;
    while(i--) {*dp++ = imag(*sp); sp += j;}
  }
  return temp;
}

DComplexArray	conj(const DComplexArray& V)	{return V.apply(::conj); }

#endif /* Not COMPLEX_PACKS */



DComplex dot(const DComplexArray& u, const DComplexArray& v)
      // dot product: sum_ij...k (u_ij...k * v_ij...k)
{
  u.lengthCheck(v.length());
  DComplex result(0,0);
  for(DoubleArrayLooper l(v.length(),u.stride(),v.stride()); l; ++l) {
    DComplex partialResult;
    rwzdot(l.length,u.data()+l.start1,l.stride1,v.data()+l.start2,l.stride2,&partialResult);
    result += partialResult;
  }
  return result;
}

DComplexArray dot(const DComplexVec& V, const DComplexArray& A)
      // dot product: Bj...k = sum_i (Vi * Aij...k)
{
  int n = V.length();
  A.dimensionLengthCheck(0,n);
  RWToEnd jk(1);   // Indexes all components of a vector but the first
  DComplexArray B(A.length()(jk),rwUninitialized,RWDataView::COLUMN_MAJOR);
  DComplex *dp = B.data();
  for(ArrayLooper l(B.length(),A.stride()(jk)); l; ++l) {
    const DComplex *sp = A.data()+l.start;
    for(int i=l.length; i--;) {
      rwzdot(n,V.data(),V.stride(),sp,A.stride((int)0),dp);
      sp += l.stride;
      dp++;
    }
  }
  return B;
}

DComplexArray dot(const DComplexArray& A, const DComplexVec& V)
      // dot product: Bi...j = sum_k (Ai...jk * Vk)
{
  int n = V.length();
  A.dimensionLengthCheck(0,n);
  RWSlice ij(0,n-1);   // Indexes all components of array index but the last
  DComplexArray B(A.length()(ij),rwUninitialized,RWDataView::COLUMN_MAJOR);
  DComplex *dp = B.data();
  for(ArrayLooper l(B.length(),A.stride()(ij)); l; ++l) {
    const DComplex *sp = A.data()+l.start;
    for(int i=l.length; i--;) {
      rwzdot(n,V.data(),V.stride(),sp,A.stride((int)0),dp);
      sp += l.stride;
      dp++;
    }
  }
  return B;
}


DComplex prod(const DComplexArray& s)
{
  DComplex t(1,0);
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    register DComplex* sp = (DComplex*)(s.data()+l.start);
    register int i = l.length;
    register j = l.stride;
    while (i--) { t *= *sp;  sp += j; }
  }
  return (DComplex)t;
}

DComplexArray pow(const DComplexArray& u, const DComplexArray& v)
// u to the v power.
{
  u.lengthCheck(v.length());
  DComplexArray temp(v.length(),rwUninitialized,RWDataView::COLUMN_MAJOR);
  register DComplex* dp = temp.data();
  for(DoubleArrayLooper l(v.length(),u.stride(),v.stride()); l; ++l) {
    register i = l.length;
    register DComplex* up = (DComplex*)(u.data()+l.start1);
    register DComplex* vp = (DComplex*)(v.data()+l.start2);
    register uj = l.stride1;
    register vj = l.stride2;
    while (i--) { *dp++ = pow(*up,*vp); up += uj; vp += vj; }
  }
  return temp;
}

DComplex sum(const DComplexArray& s)
{
  REGISTER DComplex t(0,0);
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    register DComplex* sp = (DComplex*)(s.data()+l.start);
    register int i = l.length;
    register j = l.stride;
    while (i--) { t += *sp;  sp += j; }
  }
  return (DComplex)t;
}

DComplex mean(const DComplexArray& s)
{
  DComplex theSum = sum(s);
  double numPoints = prod(s.length());
  if (theSum==DComplex(0,0)) return DComplex(0,0);   // Covers the case of zero points
  return theSum/DComplex(numPoints,0);
}

double variance(const DComplexArray& s)
// Variance of a DComplexArray
{
  s.numPointsCheck("Variance needs minimum of",2);
  register double sum2 = 0;     // Accumulate sums in double precision
  REGISTER DComplex avg = mean(s);
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    register DComplex* sp = (DComplex*)(s.data()+l.start);
    register int i = l.length;
    register j = l.stride;
    while (i--) {
      REGISTER DComplex temp = *sp - avg;
      sum2 += norm(temp);
      sp += j;
    }
  }
  // Use the biased estimate (easier to work with):
  return (double)(sum2/prod(s.length()));
}

/*
 * Various functions with simple definitions:
 */
DComplexArray	cos(const DComplexArray& V)		{ return V.apply(::cos);  }
DComplexArray	cosh(const DComplexArray& V)	{ return V.apply(::cosh); }
DComplexArray	exp(const DComplexArray& V)		{ return V.apply(::exp);  }
DComplexArray	sin(const DComplexArray& V)		{ return V.apply(::sin);  }
DComplexArray	sinh(const DComplexArray& V)	{ return V.apply(::sinh); }
DoubleArray	arg(const DComplexArray& V)	{return V.apply2(::arg);  }
DoubleArray	norm(const DComplexArray& V)	{return V.apply2(::norm); }

#ifdef __ZTC__
/*
 * Zortech requires its own special version of sqrt(const DComplexArray&) 
 * because it uses an inlined version of sqrt().  Hence, apply() 
 * cannot be used.
 */
DComplexArray
sqrt(const DComplexArray& s)
// Square root of a DComplexArray --- Zortech version
{
  DComplexArray temp(s.length(),rwUninitialized);
  register DComplex* dp = temp.data();
  for(ArrayLooper l(s.length(),s.stride()); l; ++l) {
    register const DComplex* sp = s.data()+l.start;
    register i = l.length;
    register j = l.stride;
    while (i--) { *dp++ = sqrt(*sp);  sp += j; }
  }
  return temp;
}
#else
DComplexArray	sqrt(const DComplexArray& V)	{ return V.apply(::sqrt); }
#endif

    /**** Norm functions ****/

Double maxNorm(const DComplexArray& x)     // Not really a norm!
{
  Double theMax = 0;
  for(ArrayLooper l(x.length(),x.stride()); l; ++l) {
    const DComplex *sp = x.data()+l.start;
    for(int i=l.length; i--;) {
      DComplex entry = sp[i*l.stride];
      if (norm(entry)>theMax) theMax=norm(entry);
    }
  }
  return sqrt(theMax);
}

Double frobNorm(const DComplexArray& x)
{
  Double sumSquares = 0;
  for(ArrayLooper l(x.length(),x.stride()); l; ++l) {
    const DComplex *sp = x.data()+l.start;
    for(int i=l.length; i--;) {
      DComplex entry = sp[i*l.stride];      
      sumSquares += norm(entry);
    }
  }
  return sqrt(sumSquares);
}


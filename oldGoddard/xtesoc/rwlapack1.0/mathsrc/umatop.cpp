/*
 * Definitions for various arithmetic operations for UCharGenMat
 *
 * Generated from template $Id: xmatop.cpp,v 1.10 1993/09/01 16:27:57 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/ucgenmat.h"
#include "rw/rwbbla.h"	

RCSID("$Header: /users/rcs/mathsrc/xmatop.cpp,v 1.10 1993/09/01 16:27:57 alv Exp $");

/************************************************
 *                                              *
 *              UNARY OPERATORS                 *
 *                                              *
 ************************************************/


// Unary prefix increment on a UCharGenMat; (i.e. ++a)
UCharGenMat& UCharGenMat::operator++()
{
  for(MatrixLooper l(rows(),cols(),rowStride(),colStride()); l; ++l) {
    int n = l.length;
    UChar* sp = data()+l.start;
    while(n--) { ++(*sp); sp+=l.stride; }
  }
  return *this;
}

// Unary prefix decrement on a UCharGenMat (i.e., --a)
UCharGenMat&
UCharGenMat::operator--()
{
  for(MatrixLooper l(rows(),cols(),rowStride(),colStride()); l; ++l) {
    int n = l.length;
    UChar* sp = data()+l.start;
    while(n--) { --(*sp); sp+=l.stride; }
  }
  return *this;
}

/************************************************
 *                                              *
 *              BINARY OPERATORS                *
 *               vector - vector                *
 *                                              *
 ************************************************/

UCharGenMat operator+(const UCharGenMat& u, const UCharGenMat& v)
{
  unsigned m = v.rows();
  unsigned n = v.cols();
  u.lengthCheck(m,n);
  UCharGenMat temp(m,n,rwUninitialized);
  UChar* dp = temp.data();
  for(DoubleMatrixLooper l(m,n,u.rowStride(),u.colStride(),v.rowStride(),v.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rwb_plvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

UCharGenMat operator-(const UCharGenMat& u, const UCharGenMat& v)
{
  unsigned m = v.rows();
  unsigned n = v.cols();
  u.lengthCheck(m,n);
  UCharGenMat temp(m,n,rwUninitialized);
  UChar* dp = temp.data();
  for(DoubleMatrixLooper l(m,n,u.rowStride(),u.colStride(),v.rowStride(),v.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rwb_mivv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

UCharGenMat operator*(const UCharGenMat& u, const UCharGenMat& v)
{
  unsigned m = v.rows();
  unsigned n = v.cols();
  u.lengthCheck(m,n);
  UCharGenMat temp(m,n,rwUninitialized);
  UChar* dp = temp.data();
  for(DoubleMatrixLooper l(m,n,u.rowStride(),u.colStride(),v.rowStride(),v.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rwb_muvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

UCharGenMat operator/(const UCharGenMat& u, const UCharGenMat& v)
{
  unsigned m = v.rows();
  unsigned n = v.cols();
  u.lengthCheck(m,n);
  UCharGenMat temp(m,n,rwUninitialized);
  UChar* dp = temp.data();
  for(DoubleMatrixLooper l(m,n,u.rowStride(),u.colStride(),v.rowStride(),v.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rwb_dvvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

/************************************************
 *                                              *
 *              BINARY OPERATORS                *
 *               vector - scalar                *
 *                                              *
 ************************************************/

UCharGenMat operator+(const UCharGenMat& s, UChar scalar)
{
  unsigned m = s.rows();
  unsigned n = s.cols();
  UCharGenMat temp(m,n,rwUninitialized);
  UChar* dp = temp.data();
  for(MatrixLooper l(m,n,s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    UChar scalar2 = scalar;
    rwb_plvs(l.length, dp, s.data()+l.start, l.stride, &scalar2);
    dp += l.length;
  }
  return temp;
}
/*
 * Normally, V-s is implemented as V+(-s), but this doesn't work
 * for unsigned types, so we use this workaround.
 */
UCharGenMat operator-(const UCharGenMat& v, UChar s)
{
  UCharGenMat result = v.copy();
  result -= s;
  return result;
}

UCharGenMat operator-(UChar scalar, const UCharGenMat& s)
{
  unsigned m = s.rows();
  unsigned n = s.cols();
  UCharGenMat temp(m,n,rwUninitialized);
  UChar* dp = temp.data();
  for(MatrixLooper l(m,n,s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    UChar scalar2 = scalar;
    rwb_misv(l.length, dp, &scalar2, s.data()+l.start, l.stride);
    dp += l.length;
  }
  return temp;
}

UCharGenMat operator*(const UCharGenMat& s, UChar scalar)
{
  unsigned m = s.rows();
  unsigned n = s.cols();
  UCharGenMat temp(m,n,rwUninitialized);
  UChar* dp = temp.data();
  for(MatrixLooper l(m,n,s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    UChar scalar2 = scalar;
    rwb_muvs(l.length, dp, s.data()+l.start, l.stride, &scalar2);
    dp += l.length;
  }
  return temp;
}

UCharGenMat operator/(UChar scalar, const UCharGenMat& s)
{
  unsigned m = s.rows();
  unsigned n = s.cols();
  UCharGenMat temp(m,n,rwUninitialized);
  UChar* dp = temp.data();
  for(MatrixLooper l(m,n,s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    UChar scalar2 = scalar;
    rwb_dvsv(l.length, dp, &scalar2, s.data()+l.start, l.stride);
    dp += l.length;
  }
  return temp;
}

UCharGenMat operator/(const UCharGenMat& s, UChar scalar)
{
  unsigned m = s.rows();
  unsigned n = s.cols();
  UCharGenMat temp(m,n,rwUninitialized);
  UChar* dp = temp.data();
  for(MatrixLooper l(m,n,s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    UChar scalar2 = scalar;
    rwb_dvvs(l.length, dp, s.data()+l.start, l.stride, &scalar2);
    dp += l.length;
  }
  return temp;
}

/*
 * Out-of-line versions for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
UCharGenMat     operator*(UChar s, const UCharGenMat& V)  {return V*s;}
UCharGenMat     operator+(UChar s, const UCharGenMat& V)  {return V+s;}
#endif

/************************************************
 *                                              *
 *      ARITHMETIC ASSIGNMENT OPERATORS         *
 *        with other vectors                    *
 *                                              *
 ************************************************/

UCharGenMat& UCharGenMat::operator+=(const UCharGenMat& u)
{
  if (sameDataBlock(u)) { return operator+=(u.copy()); }  // Avoid aliasing
  u.lengthCheck(rows(),cols());
  for(DoubleMatrixLooper l(rows(),cols(),rowStride(),colStride(),u.rowStride(),u.colStride()); l; ++l) {
    rwb_aplvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

UCharGenMat& UCharGenMat::operator-=(const UCharGenMat& u)
{
  if (sameDataBlock(u)) { return operator-=(u.copy()); }  // Avoid aliasing
  u.lengthCheck(rows(),cols());
  for(DoubleMatrixLooper l(rows(),cols(),rowStride(),colStride(),u.rowStride(),u.colStride()); l; ++l) {
    rwb_amivv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

UCharGenMat& UCharGenMat::operator*=(const UCharGenMat& u)
{
  if (sameDataBlock(u)) { return operator*=(u.copy()); }  // Avoid aliasing
  u.lengthCheck(rows(),cols());
  for(DoubleMatrixLooper l(rows(),cols(),rowStride(),colStride(),u.rowStride(),u.colStride()); l; ++l) {
    rwb_amuvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

UCharGenMat& UCharGenMat::operator/=(const UCharGenMat& u)
{
  if (sameDataBlock(u)) { return operator/=(u.copy()); }  // Avoid aliasing
  u.lengthCheck(rows(),cols());
  for(DoubleMatrixLooper l(rows(),cols(),rowStride(),colStride(),u.rowStride(),u.colStride()); l; ++l) {
    rwb_advvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

/************************************************
 *                                              *
 *      ARITHMETIC ASSIGNMENT OPERATORS         *
 *                with a scalar                 *
 *                                              *
 ************************************************/

UCharGenMat& UCharGenMat::operator+=(UChar scalar)
{
  for(MatrixLooper l(rows(),cols(),rowStride(),colStride()); l; ++l) {
    UChar scalar2 = scalar;
    rwb_aplvs(l.length, data()+l.start, l.stride, &scalar2);
  }
  return *this;
}
/*
 * Most types implement V-=s using V+=(-s), but this doesn't
 * work for unsigned types, so we need an explicit function.
 * Since there is no rwbla, we'll just use a loop.
 */
UCharGenMat&
UCharGenMat::operator-=(UChar s)
{
  for(MatrixLooper l(rows(),cols(),rowStride(),colStride()); l; ++l) {
    int n = l.length;
    UChar* sp = data()+l.start;
    while(n--) { *sp -= s; sp+=l.stride; }
  }
  return *this;
}

UCharGenMat& UCharGenMat::operator*=(UChar scalar)
{
  for(MatrixLooper l(rows(),cols(),rowStride(),colStride()); l; ++l) {
    UChar scalar2 = scalar;
    rwb_amuvs(l.length, data()+l.start, l.stride, &scalar2);
  }
  return *this;
}

UCharGenMat& UCharGenMat::operator/=(UChar scalar)
{
  for(MatrixLooper l(rows(),cols(),rowStride(),colStride()); l; ++l) {
    UChar scalar2 = scalar;
    rwb_advvs(l.length, data()+l.start, l.stride, &scalar2);
  }
  return *this;
}

/************************************************
 *                                              *
 *              LOGICAL OPERATORS               *
 *                                              *
 ************************************************/

RWBoolean UCharGenMat::operator==(const UCharGenMat& u) const
{
  if(rows()!=u.rows() || cols()!=u.cols()) return FALSE;  // They can't be equal if they don't have the same length.
  for(DoubleMatrixLooper l(rows(),cols(),rowStride(),colStride(),u.rowStride(),u.colStride()); l; ++l) {
    if (!rwbsame(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2)) {
      return FALSE;
    }
  }
  return TRUE;
}

RWBoolean UCharGenMat::operator!=(const UCharGenMat& u) const
{
  return !(*this == u);
}

/*
 * Definitions for SCharVec
 *
 * Generated from template $Id: xvec.cpp,v 1.9 1993/10/04 21:44:01 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include <rw/strstrea.h>  /* I use istrstream to parse character string ctor */
#include "rw/scvec.h"
#include "rw/rwabla.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"
#include "rw/rand.h"

RCSID("$Header: /users/rcs/mathsrc/xvec.cpp,v 1.9 1993/10/04 21:44:01 alv Exp $");

SCharVec::SCharVec(const char *string)
 : RWVecView()
{
  istrstream s((char*)string);
  scanFrom(s);
  if (!s.good()) {
    RWTHROW( RWExternalErr( RWMessage(RWMATH_STRCTOR,string) ));
  }
}

SCharVec::SCharVec(unsigned n, RWRand& r)
 : RWVecView(n,sizeof(SChar))
{
  while(n--) {
    (*this)(n) = (SChar)r();
  }
}

SCharVec::SCharVec(unsigned n, SChar scalar)
 : RWVecView(n,sizeof(SChar))
{
  SChar scalar2 = scalar;	// Temporary required to finesse K&R compilers
  rwaset(n, data(), 1, &scalar2);
}

SCharVec::SCharVec(unsigned n, SChar scalar, SChar by)
 : RWVecView(n,sizeof(SChar))
{
  register int i = n;
  SChar* dp = data();
  SChar value = scalar;
  SChar byvalue = by;
  while(i--) {*dp++ = value; value += byvalue;}
}


SCharVec::SCharVec(const SChar* dat, unsigned n)
  : RWVecView(n,sizeof(SChar))
{
  rwacopy(n, data(), 1, dat, 1);
}

SCharVec::SCharVec(RWBlock *block, unsigned n)
  : RWVecView(block,n)
{
  unsigned minlen = n*sizeof(SChar);
  if (block->length() < minlen) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_SHORTBLOCK, block->length(), minlen) ));
  }
}

SCharVec&
SCharVec::operator=(const SCharVec& rhs)
{
  lengthCheck(rhs.length());
  if (sameDataBlock(rhs)) { return (*this)=rhs.copy(); }  // avoid aliasing problems
  rwacopy(npts, data(), step, rhs.data(), rhs.step);
  return *this;
}

// Assign to a scalar:
SCharVec&
SCharVec::operator=(SChar scalar)
{
  SChar scalar2 = scalar;	// Temporary necessary to finesse K&R compilers
  rwaset(npts, data(), step, &scalar2);
  return *this;
}

SCharVec&
SCharVec::reference(const SCharVec& v)
{
  RWVecView::reference(v);
  return *this;
}

// Return copy of self with distinct instance variables
SCharVec
SCharVec::copy() const
{
  SCharVec temp(npts,rwUninitialized);
  rwacopy(npts, temp.data(), 1, data(), step);
  return temp;
}

// Synonym for copy().  Not made inline because some compilers
// have trouble with inlined temporaries with destructors.
SCharVec
SCharVec::deepCopy() const
{
  return copy();
}

// Guarantee that references==1 and stride==1
void
SCharVec::deepenShallowCopy()
{
  if (!isSimpleView()) {
    RWVecView::reference(copy());
  }
}

/*
 * Reset the length of the vector.  The contents of the old
 * vector are saved.  If the vector has grown, the extra storage
 * is zeroed out.  If the vector has shrunk, it is truncated.
 */
void
SCharVec::resize(unsigned N)
{
  if(N != npts){
    SCharVec newvec(N,rwUninitialized);

    // Copy over the minimum of the two lengths:
    unsigned nkeep = npts < N ? npts : N;
    rwacopy(nkeep, newvec.data(), 1, data(), stride());
    unsigned oldLength= npts;	// Remember the old length

    RWVecView::reference(newvec);

    // If vector has grown, zero out the extra storage:
    if( N > oldLength ){
      static const SChar zero = 0;	/* "static" required for Glockenspiel */
      rwaset((unsigned)(N-oldLength), data()+oldLength, 1, &zero);
    }
  }
}

/*
 * Reset the length of the vector.  The results can be (and
 * probably will be) garbage.
 */
void
SCharVec::reshape(unsigned N)
{
  if(N != npts) {
    RWVecView::reference(SCharVec(N,rwUninitialized));
  }
}

SCharVec SCharVec::slice(int start, unsigned n, int str) const {
  return (*this)[RWSlice(start,n,str)];
}


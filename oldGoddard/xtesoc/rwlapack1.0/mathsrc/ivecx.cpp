/*
 * Type specific routines for IntVec
 *
 * $Header: /users/rcs/mathsrc/ivecx.cpp,v 1.3 1993/09/01 16:27:22 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: ivecx.cpp,v $
 * Revision 1.3  1993/09/01  16:27:22  alv
 * now uses rwUninitialized form for simple constructors
 *
 * Revision 1.2  1993/01/26  17:52:28  alv
 * removed DOS EOF
 *
 * Revision 1.1  1993/01/22  23:51:00  alv
 * Initial revision
 *
 * 
 *    Rev 1.3   17 Oct 1991 09:15:42   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.2   08 Oct 1991 13:44:38   keffer
 * Math V4.0 beta 2
 * 
 *    Rev 1.1   24 Jul 1991 12:51:42   keffer
 * Added pvcs keywords
 *
 */

#include "rw/ivec.h"
#include "rw/dvec.h"
#include "rw/fvec.h"

RCSID("$Header: /users/rcs/mathsrc/ivecx.cpp,v 1.3 1993/09/01 16:27:22 alv Exp $");

// Convert to a DoubleVec
// Should be a friend DoubleVec, but...
IntVec::operator DoubleVec() const
{
  register int n = length();
  DoubleVec temp(n,rwUninitialized);
  register double* dp = temp.data();
  register const int* tp = data();
  register int ts = stride();
  while(n--) {
    *dp++ = double(*tp); tp += ts;
  }
  return temp;
}

// narrow from DoubleVec to IntVec
IntVec
toInt(const DoubleVec& v)
{
  register int n = v.length();
  IntVec temp(n,rwUninitialized);
  register const double* dp = v.data();
  register int* ip = temp.data();
  register int vs = v.stride();
  while(n--) {
    *ip++ = int(*dp); dp += vs;
  }
  return temp;
}

// narrow from FloatVec to IntVec
IntVec
toInt(const FloatVec& v)
{
  register int n = v.length();
  IntVec temp(n,rwUninitialized);
  register const float* dp = v.data();
  register int* ip = temp.data();
  register int vs = v.stride();
  while(n--) {
    *ip++ = int(*dp); dp += vs;
  }
  return temp;
}


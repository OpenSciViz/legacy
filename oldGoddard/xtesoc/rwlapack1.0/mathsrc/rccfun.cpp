/*
 * DComplex class operator and math function definitions;
 * used only if the compiler manufacturer does not provide
 * a complex class.
 *
 * $Header: /users/rcs/mathsrc/rccfun.cpp,v 1.6 1993/09/19 21:52:36 alv Exp $
 ***************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 * 
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 * 
 ***************************************************************************
 *
 * $Log: rccfun.cpp,v $
 * Revision 1.6  1993/09/19  21:52:36  alv
 * fixed a typo, added missing op/(dbl,cplx)
 *
 * Revision 1.5  1993/08/05  20:46:39  dealys
 * ported to MPW C++ 3.3 - extended cast
 *
 * Revision 1.4  1993/07/19  18:11:31  alv
 * fixed so include files read before #ifdefs checked
 *
 * Revision 1.3  1993/05/13  15:19:05  alv
 * fixed so Rogue Wave complex is always included
 *
 * Revision 1.2  1993/01/28  00:26:57  alv
 * removed HAS_IOSTREAMS ifdef, must now have iostreams
 *
 * Revision 1.1  1993/01/22  23:51:03  alv
 * Initial revision
 *
 * 
 *    Rev 1.5   24 Mar 1992 13:31:30   KEFFER
 * Ported to Microsoft C/C++ V7.0
 * 
 *    Rev 1.4   17 Oct 1991 09:15:38   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.2   24 Sep 1991 18:54:16   keffer
 * Ported to Zortech V3.0
 * 
 *    Rev 1.1   24 Jul 1991 12:51:56   keffer
 * Added pvcs keywords
 *
 */

/*
 *  The Rogue Wave classes generally use the compiler-supplied version
 *  of complex.  If no version has been supplied, then this gets used.
 *  An example is early Zortech.
 */

/*     
 *		a = complex number
 *		u = real part of a
 *		v = imaginary part of a
 *		r = sqrt (u*u + v*v) = absolute value of a
 *		theta = arc tan (v/u) = argument of a (radians)
 */		

#include "rw/dcomplex.h"
#include "rw/rstream.h"
#include <ctype.h>

RCSID("$Header: /users/rcs/mathsrc/rccfun.cpp,v 1.6 1993/09/19 21:52:36 alv Exp $");

#if defined(RW_USE_RW_COMPLEX) || (defined(__ZTC__) && (__ZTC__ < 0x300)) || defined(_MSC_VER) || defined(applec)

#ifdef RW_NATIVE_EXTENDED
#  define EXTENDED (extended)
#else
#  define EXTENDED
#endif

/******************* OPERATOR DEFINITIONS *****************************/

//division of DComplex by DComplex
DComplex
operator/(DComplex a1, DComplex a2)
{
  register double denom = a2.re*a2.re + a2.im*a2.im;
  return DComplex( (a1.re*a2.re+a1.im*a2.im)/denom, 
		  (a2.re*a1.im-a1.re*a2.im)/denom );
}

DComplex
operator/(double a1, DComplex a2)
{
  register double denom = a2.re*a2.re + a2.im*a2.im;
  return DComplex( (a1*a2.re)/denom, 
		  (-a1	*a2.im)/denom );
}  

// increment type operators

void
DComplex::operator*=(DComplex a)
{
  register double r = re;
  re = r*a.re-im*a.im;
  im = r*a.im+im*a.re;
}

void
DComplex::operator/=(DComplex a)
{
  register double r = re;
  register double denom = a.re*a.re + a.im*a.im;
  re = (r*a.re+im*a.im)/denom;
  im = (a.re*im-r*a.im)/denom;
}

/************************ I/O Operators ***********************/

// operator << writes a DComplex number a in the form ( u, v)
ostream&
operator<<(ostream& s, DComplex a)
{
  return s << "( " << real(a) << ", " << imag(a) << ")";
}

// operator >> reads a DComplex number a in the form
//	u
//      (u)
//	(u, v)
istream&
operator>>(istream& s, DComplex& a)
{
  double u = 0, v = 0;
  char 	c;

  s >> c;

  if (c == '(') {
    s >> u >> c;
    if (c == ',') {s >> v >> c;}
    if (c != ')' ) {
      s.clear(ios::badbit);
    }
  }
  else {
    s.putback(c);
    s >> u;
  }

  // Assign the value if everthing is OK:
  if (s) a = DComplex(u,v);

  return s;
}
	

/******************* FUNCTION DEFINITIONS ****************************/


// absolute value of DComplex number a
// 	absolute value = sqrt ( u*u + v*v ) 
double
abs(DComplex a)
{
  return sqrt(norm(a));
}



// norm of DComplex number a
//	norm = u*u + v*v
double
norm(DComplex a)
{
  return  ( a.re*a.re + a.im*a.im );
}



// argument (radians) of DComplex number a
// 	argument = arc tangent ( v/u )
//     	     note: atan2 function returns :  -pi < arg < pi.
//                 This implies the principal value of DComplex log,sqrt etc.
double
arg(DComplex a)
{
  return atan2 (a.im, a.re);
}


// DComplex cosine of DComplex number a
//	cos (a) = cos u * cosh v - i * sin u * sinh v
DComplex
cos(DComplex a)
{
  return DComplex ( cos(a.re)*cosh(a.im), - sin(a.re)*sinh(a.im) );
}


// DComplex hyperbolic cosine of DComplex number a
//	cosh (a) = cosh u * cosv + i * sinh u * sin v
DComplex
cosh(DComplex a)
{
  return DComplex ( cosh(a.re)*cos(a.im), sinh(a.re)*sin(a.im) );
}

// DComplex sine of DComplex number a
//	sin (a) = sin u * cosh v + i * cos u * sinh v
DComplex
sin(DComplex a)
{
  return DComplex ( sin(a.re)*cosh(a.im), cos(a.re)*sinh(a.im) );
}

// DComplex hyperbolic sine of DComplex number a
//	sinh (a) = sinh u cos v + i cosh u sin v
DComplex
sinh(DComplex a)
{
  return DComplex ( sinh(a.re)*cos(a.im), cosh(a.re)*sin(a.im) );
}

// DComplex exponential of  DComplex number a
//	exp (a) = exp(u) * (cos v + i * sin v)
DComplex
exp(DComplex a)
{
  register double e = exp(a.re);
  return DComplex ( e * cos(a.im) , e * sin(a.im) );
}


// DComplex natural log of DComplex number a
//	log(a) = log(r) + i * theta 
DComplex
log(DComplex a)
{
  return DComplex( log(abs(a)), arg(a) );
}


// DComplex square root of DComplex number a
//  	sqrt(a) = sqrt(r) * ( cos (theta/2) + i sin (theta/2) )
DComplex
sqrt(DComplex a)
{
  register double r  = sqrt(abs(a));
  register double th = arg(a)/2.;
  return DComplex( r*cos(th), r*sin(th) );
}

// DComplex number a raised to an integer power n
//	a**n = r**n  *  ( cos (n theta) + i sin (n theta) ) 
DComplex
pow(DComplex a, int n)
{
  if ( n == 0 )
    return DComplex (1, 0);
  if ( n == 1 )
    return a;
  if ( a.im == 0 ) {
    if ( a.re == 0 )
      return DComplex (0,0);
    else
      return DComplex (pow(EXTENDED a.re,EXTENDED double(n)), 0);
  }

 // Zortech 2.06 doesn't handle this.
 // DComplex b = n<0 ? 1/a : a;
  DComplex b;
  if (n<0) 
	b = 1/a;
  else
	b = a;

  register double r  = pow(EXTENDED abs(a),EXTENDED double(n) );
  register double th = double(n) * arg(b);
  return DComplex ( r*cos(th), r*sin(th) ); 	
}

//DComplex number a raised to a real power s
//  a**s = exp ( s * log (a) )
DComplex
pow(DComplex a, double s)
{
  DComplex b;
  if ( a.im == 0 ) {
    if ( a.re == 0 )
      return DComplex (0,0);
    else
      return DComplex (pow(EXTENDED a.re,EXTENDED s), 0);
  }
  
  if( s < 0 )
    b = 1/a;
  else
    b = a;
  
  DComplex answer = exp ( s * log(b) );
	return answer;

}

//real number s raised to a DComplex power a
//  s**a = exp ( a * log (s) )
DComplex
pow(double s, DComplex a)
{
  if ( a.im == 0 )
    return DComplex(pow (EXTENDED s,EXTENDED a.re),0);
  if ( s == 0 ) 
    return DComplex (0,0);
  
  return DComplex ( exp ( a * log(s) ) );
}

// DComplex number a1 raised to a DComplex power a2
// a1**a2 = rho * (cos (phi) + i sin (phi)  )
//  	rho = r1 **u2   *  exp (-v2* theta1)
//	phi = v2 * log(r1) + u2 * theta1
DComplex
pow(DComplex a1, DComplex a2)
{
  double r1 = abs(a1);
  if (r1 == 0)  return DComplex (0,0);
  
  double u2 = a2.re;
  double v2 = a2.im;
  
  double th1 = arg(a1);
  double rho = pow(EXTENDED r1,EXTENDED u2) * exp(-v2*th1);
  double phi = v2 * log(r1) + u2 * th1;
  return DComplex ( rho*cos(phi), rho*sin(phi) );
}

//  for input r and theta (radians)
//  this function returns a DComplex number  in the form
//	a = u + i v
DComplex
polar(double r, double theta)
{
  return DComplex ( r*cos(theta), r*sin(theta) );
}

#endif

/*
 * Definitions for IntVec I/O
 *
 * Generated from template $Id: xvecio.cpp,v 1.6 1993/08/10 21:22:18 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/ivec.h"
#include "rw/rwfile.h"
#include "rw/vstream.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"
STARTWRAP
#include <stdio.h>
#include <ctype.h>
ENDWRAP

#  define CAST Int
#  define CTYPE Int

RCSID("$Header: /users/rcs/mathsrc/xvecio.cpp,v 1.6 1993/08/10 21:22:18 alv Exp $");

const int            default_resize        = 128;
const unsigned short versionID             = 2;		// Formatting version number

/************************* I / O   R O U T I N E S ***********************/

void
IntVec::printOn(ostream& s, unsigned numberPerLine) const 
{
  s << "[\n";
  for(register int n = 0; n < length(); n++){
    if (n) { if (numberPerLine==0 || n%numberPerLine) {s<<" ";} else {s<<"\n";} }
    CAST item = (*this)(n);	// Can't use s<<(*this)(n) due to cfront bug
    s << item;
  }
  s << "\n]";
}

void
IntVec::scanFrom(istream& s)
{
  CAST item;
  register int nextspace = 0;
  char c = 0;		// The first character read from the stream
  
  // Munch through any leading white space:
  do {
    s.get(c);
  } while (isspace(c)) ;
  
  if (c != '[') {
    // Scan input stream, resizing as necessary.
    // Keep scanning till we can't scan no mo'
    s.putback(c);
    while (s >> item) {
      if (nextspace >= length()) resize(length()+default_resize);
      (*this)(nextspace++) = item;
    }
  }
  else {  // Scan input stream, stop scanning at the matching ']' character
    
    do {
      s.get(c);			// Munch through any white space after the leading '['
    } while (isspace(c));

    while ( s && (c != ']')  ) {
       s.putback(c);
       if (s >> item) {
           if (nextspace >= length()) resize(length()+default_resize);
           (*this)(nextspace++) = item;
       }
       do {
	 s.get(c);
       } while (isspace(c)) ;

     }
    }
  // Trim to fit
  if(nextspace != length())resize(nextspace);
}

ostream&
operator<<(ostream& s, const IntVec& v)
{
  v.printOn(s); return s;
}
istream&
operator>>(istream& s, IntVec& v)
{
  v.scanFrom(s); return s;
}

void
IntVec::saveOn(RWFile& file) const 
{
  unsigned len = length();
  file.Write(versionID);
  file.Write(len);


  if( stride() == 1 ){
    const CTYPE* dataStart = (const CTYPE*)data();
    file.Write(dataStart, len);
  }
  else {
    for(register int n = 0; n < len; n++){
      CTYPE item = (*this)(n);
      file.Write(item);
    }
  }

}

void
IntVec::saveOn(RWvostream& s) const 
{
  unsigned len = length();
  s << versionID;		// Save the version number
  s << len;			// Save the vector length


  if( stride() == 1 ){
    const CTYPE* dataStart = (const CTYPE*)data();
    s.put(dataStart, len);
  }
  else {
    for(register int n = 0; n < len; n++){
      CTYPE item = (*this)(n);
      s << item;
    }
  }
}

void
IntVec::restoreFrom(RWFile& file)
{
  // Get and check the version number
  unsigned short ID;
  file.Read(ID);

  if( ID!=versionID && ID!=340 ){
    versionErr((int)versionID, (int)ID);
  }

  // Get the length and resize to it:
  unsigned len;
  file.Read(len);
  resize(len);


  // Scoop up the whole vector in one read:
  CTYPE* dataStart = (CTYPE*) data();
  file.Read(dataStart, len);
}

void
IntVec::restoreFrom(RWvistream& s)
{
  // Get and check the version number
  unsigned short ID;
  s >> ID;

  if( ID!=versionID && ID!=340 ){
    RWTHROW( RWExternalErr( RWMessage(RWMATH_BADVERNO, (int)versionID, (int)ID) ));
  }

  // Get the length and resize to it:
  unsigned len;
  s >> len;
  resize(len);


  // Scoop up the whole vector in one read:
  CTYPE* dataStart = (CTYPE*) data();
  s.get(dataStart, len);
}

unsigned
IntVec::binaryStoreSize() const
{
  // Total storage requirements = Number of elements times their size, 
  // plus space for the vector length and ID number:
  return length()*sizeof(Int) + sizeof(unsigned) + sizeof(versionID);
}

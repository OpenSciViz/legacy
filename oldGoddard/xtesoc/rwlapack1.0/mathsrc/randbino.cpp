/*
 * Definitions for class RandBinomial.
 *
 * $Header: /users/rcs/mathsrc/randbino.cpp,v 1.4 1993/09/23 17:15:23 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: randbino.cpp,v $
 * Revision 1.4  1993/09/23  17:15:23  alv
 * ported to NCR
 *
 * Revision 1.3  1993/09/01  16:27:30  alv
 * now uses rwUninitialized form for simple constructors
 *
 * Revision 1.2  1993/01/26  17:52:28  alv
 * removed DOS EOF
 *
 * Revision 1.1  1993/01/22  23:51:02  alv
 * Initial revision
 *
 * 
 *    Rev 1.3   17 Oct 1991 09:15:32   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.2   08 Oct 1991 13:44:44   keffer
 * Math V4.0 beta 2
 * 
 *    Rev 1.1   24 Jul 1991 12:51:46   keffer
 * Added pvcs keywords
 *
 */

/* Definitions for the Binomial Random Number Class: RandBinomial */

#include "rw/dcomplex.h"  /* So NCR can find M_PI, of all the silly things */
#include "rw/dvec.h"
#include "rw/dgenmat.h"
#include "rw/randbino.h"
#include "rw/dstats.h"
STARTWRAP
#include <math.h>
ENDWRAP

RandBinomial::RandBinomial(unsigned N, double probability)
{
  prob   = probability;
  tries = N;
  if (prob <= .5) 
    prb = prob;
  else
    prb = 1 - prob;
  
  if(tries < 25) {
    g     = 0.;
    am    = 0.;
    pc    = 0.;
    sq    = 0.;
    plog  = 0.;
    pclog = 0.;
  }
  else {
    am = tries*prb;
    if(am < 1.) {
      g = ::exp(-am);
      pc    = 0.;
      sq    = 0.;
      plog  = 0.;
      pclog = 0.;
    }
    else {
      g  = logGamma(double(tries+1));
      pc = 1.- prb;
      sq = ::sqrt(2.*am*pc);
      plog = ::log(prb);
      pclog = ::log(pc);
    }
  }
}


RandBinomial::RandBinomial(unsigned n, unsigned N, double probability) :
	RandUniform(n)  
{
  tries = N;
  prob   = probability;
  if (prob <= .5) 
    prb = prob;
  else
    prb = 1 - prob;
  
  if(tries < 25) {
    g     = 0.;
    am    = 0.;
    pc    = 0.;
    sq    = 0.;
    plog  = 0.;
    pclog = 0.;
  }
  else {
    am = tries*prb;
    if(am < 1.) {
      g = ::exp(-am);
      pc    = 0.;
      sq    = 0.;
      plog  = 0.;
      pclog = 0.;
    }
    else {
      g  = logGamma(double(tries+1));
      pc = 1.- prb;
      sq = ::sqrt(2.*am*pc);
      plog = ::log(prb);
      pclog = ::log(pc);
    }
  }
}

void
RandBinomial::setTrials(unsigned N)
{
  tries = N;
  if(tries < 25) {
    g     = 0.;
    am    = 0.;
    pc    = 0.;
    sq    = 0.;
    plog  = 0.;
    pclog = 0.;
  }
  else {
    am = tries*prb;
    if(am < 1.) {
      g = ::exp(-am);
      pc    = 0.;
      sq    = 0.;
      plog  = 0.;
      pclog = 0.;
    }
    else {
      g  = logGamma(double(tries+1));
      pc = 1.- prb;
      sq = ::sqrt(2.*am*pc);
      plog = ::log(prb);
      pclog = ::log(pc);
    }
  }
}


void
RandBinomial::setProbability(double probability)
{
  prob   = probability;
  if (prob <= .5) 
    prb = prob;	 
  else
    prb = 1 - prob;
  
  if(tries < 25) {
    g     = 0.;
    am    = 0.;
    pc    = 0.;
    sq    = 0.;
    plog  = 0.;
    pclog = 0.;
  }
  else {
    am = tries*prb;
    if(am < 1.) {
      g = ::exp(-am);
      pc    = 0.;
      sq    = 0.;
      plog  = 0.;
      pclog = 0.;
    } 
    else {
      g  = logGamma(double(tries+1));
      pc = 1.- prb;
      sq = ::sqrt(2.*am*pc);
      plog = ::log(prb);
      pclog = ::log(pc);
    }
  }
}



// Return a random number selected from a binomial distribution 
// of (tries) trials each of probability (prob)
double
RandBinomial::randValue() 
{
  double temp;
  double t;
  int    j;
  if (tries < 25) {
    temp = 0.;
    for ( int i = 0; i < tries; i++ ) {
      if( RandUniform::randValue() < prb ) 
	temp += 1.;
    }
  }
  else if (am < 1.) {
    t = 1.;
    for ( j = 0; j < tries+1; j++) {
      t *= RandUniform::randValue(); 
      if (t < g) break;
    }
    temp = (double)j;
  }
  else {
    double y,em;
    do {
      do {
	y = ::tan(M_PI*RandUniform::randValue());
	em = sq*y + am;
      } while ( em < 0. || em >= tries+1 );
      em = ::floor(em);
      t = 1.2*sq*(1.+y*y) * 
	::exp ( g - logGamma(em+1.) - logGamma(tries-em+1.)
	       + em*plog + (tries-em)*pclog );
    } while ( RandUniform::randValue() > t );
    temp = em;
  }
  if ( prob != prb ) 
    return (tries - temp);
  else
    return temp;
}


// Return an array of random numbers selected from a binomial distribution
// of (tries) trials each of probability (prob).
void
RandBinomial::randValue(double* d, unsigned n) 
{
  while (n--) { *d++ = RandBinomial::randValue(); }
}

// Return a vector of n random numbers selected from a binomial distribution
DoubleVec
RandBinomial::randValue(unsigned n) 
{
  DoubleVec temp(n,rwUninitialized);
  while (n--) temp(n) = RandBinomial::randValue();
  return temp;
}

// Return a matrix of random numbers selected from a binomial distribution 
DoubleGenMat
RandBinomial::randValue(unsigned nr, unsigned nc)
{
  DoubleVec tempv = RandBinomial::randValue(nr*nc);
  DoubleGenMat temp(tempv, nr, nc);
  return temp;
}

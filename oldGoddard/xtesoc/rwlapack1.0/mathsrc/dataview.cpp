/*
 * Definitions for data block handle classes and RWDataView
 *
 * $Id: dataview.cpp,v 1.7 1993/09/18 17:41:50 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990, 1993.
 * This software is subject to copyright protection under the laws
 * of the United States and other countries.
 *
 ***************************************************************************
 *          
 * It is possible to log data block allocations/deallocations.  See
 * the comment block near the end of this file for details.
 *
 * $Log: dataview.cpp,v $
 * Revision 1.7  1993/09/18  17:41:50  alv
 * fixed syntax errors in RWMappedBlock code
 *
 * Revision 1.6  1993/09/15  23:02:08  alv
 * fixed error in RWPRECONDITION
 *
 * Revision 1.5  1993/09/14  17:37:53  alv
 * uses new RWReference STATIC_INIT constructor
 *
 * Revision 1.4  1993/08/17  19:02:00  alv
 * now allows use of custom RWBlocks
 *
 * Revision 1.1  1993/01/22  23:50:57  alv
 * Initial revision
 *
 * 
 *    Rev 1.5   24 Mar 1992 12:59:48   KEFFER
 * nil -> rwnil
 * 
 *    Rev 1.4   15 Nov 1991 09:36:26   keffer
 * Removed RWMATHERR macro
 * 
 *    Rev 1.3   17 Oct 1991 09:15:30   keffer
 * Changed include path to <rw/xxx.h>
 * 
 */

#include "rw/dataview.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"

#ifdef RW_HAS_MAPPED_FILES
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#endif

/* #define RWBLOCK_LOGGING */  /*  See large block comment below */

RCSID("$Id: dataview.cpp,v 1.7 1993/09/18 17:41:50 alv Exp $");

#ifdef RWBLOCK_LOGGING
  static void logAllocation(unsigned,size_t,void*);
  static void logDeallocation(void*);
#endif

#ifdef RW_MULTI_THREAD
# include "rw/mutex.h"
  static RWMutex rwDataViewLock;
# define MULTITHREAD_LOCK (rwDataViewLock)
#else
# define MULTITHREAD_LOCK /* nothing */
#endif

/*
 * Define the nilblock.  Use the special constructor for
 * RWReference that initializes statically, rather than
 * dynamically.  Dynamic construction can cause problems if
 * user's global objects are inited first.
 */
static RWBlock nilBlock(RWReference::STATIC_INIT);  

RWBlock::RWBlock(RWReference::RWReferenceFlag)
 : RWReference(STATIC_INIT)
{
}

RWBlock::RWBlock(const RWBlock& block)
{
  data_ = block.data_;
  len_ = block.len_;
}

/*
 * Implementation of RWNewBlock.  This just allocates the block
 * off the heap using new, and then restores it using delete.
 */

RWNewBlock::RWNewBlock(unsigned nelem, size_t elemsize)
{
  void *data = (nelem==0) ? 0 : new char[nelem*elemsize];
  init(data,nelem*elemsize);
#ifdef RWBLOCK_LOGGING
  logAllocation(nelem,elemsize,data);
#endif
}

RWNewBlock::~RWNewBlock()
{
#ifdef RWBLOCK_LOGGING
  logDeallocation(array);
#endif
  delete [] data();
}

/*
 * Implementation of RWMappedBlock.  This version uses the traditional
 * unix mmap() to map the file.  The actual work of mapping the file
 * is done in the static function doTheMapping() so the same code can
 * be used by both constructors.
 */
#if defined(RW_HAS_MAPPED_FILES)

void *doTheMapping(const char *filename, size_t offset, int mode, unsigned *len)
{
  *len = 0;
  int prot = PROT_READ;           // mmap protection parameter
  if (mode&O_WRONLY || mode&O_RDWR) prot = PROT_READ | PROT_WRITE;
  int fd = open(filename, mode);
  if (fd<0) return 0;
  struct stat statbuf;
  if (fstat(fd,&statbuf)!=0) return 0;  // quit if fstat fails
  caddr_t data = mmap(0, statbuf.st_size, prot, MAP_SHARED, fd, offset);
  if (data == (caddr_t)-1) return 0;
  *len = statbuf.st_size;
  return data;         
}

RWMappedBlock::RWMappedBlock(const char *filename, size_t offset)
{
  unsigned len;
  void *data = doTheMapping(filename, offset, O_RDONLY, &len);
  init(data,len);
}

RWMappedBlock::RWMappedBlock(const char *filename, size_t offset, int mode)
{
  unsigned len;
  void *data = doTheMapping(filename, offset, mode, &len);
  init(data,len);
}

RWMappedBlock::~RWMappedBlock()
{
  munmap( (caddr_t) data(), length() );
}
#endif

/*
 * Implementation of RWDataView
 */

RWDataView::RWDataView()
{
  block = &nilBlock;
  begin = 0;
  block->addReference(MULTITHREAD_LOCK);
}

RWDataView::RWDataView(RWBlock *b)
{
  block = b;
  block->addReference(MULTITHREAD_LOCK);
  begin = block->data();
}

RWDataView::RWDataView(unsigned n, size_t s)
{
  block = new RWNewBlock(n,s);
  block->addReference(MULTITHREAD_LOCK);
  begin = block->data();
}

RWDataView::RWDataView(const RWDataView& x)
{
  block = x.block;
  begin = x.begin;
  block->addReference(MULTITHREAD_LOCK);
}

RWDataView::RWDataView(const RWDataView& v, void *b)
{
  // Preconditions check that b points to data in v's block
  RWPRECONDITION((char*)b >= (char*)(v.block->data()));
  RWPRECONDITION((char*)b - (char*)(v.block->data()) <= v.block->length() );
  block = v.block;
  begin = b;
  block->addReference(MULTITHREAD_LOCK);
}

RWDataView::~RWDataView()
{
  if (block->removeReference(MULTITHREAD_LOCK) == 0) delete block;
}

void         RWDataView::reference(const RWDataView& x)
{
  if (block->removeReference(MULTITHREAD_LOCK) == 0) delete block;
  block = x.block;
  begin = x.begin;
  block->addReference(MULTITHREAD_LOCK);
}

RWBoolean    RWDataView::isSimpleView()
{
  return (block->references()==1 && begin==block->data());
}

void RWDataView::versionErr(int shouldBe, int was)
{
  RWTHROW( RWExternalErr( RWMessage(RWMATH_BADVERNO, shouldBe, was) ));
}


#ifdef RWBLOCK_LOGGING

/*************************************************************************
 *                                                                       *
 * RWBlock logging code.  This code is enabled by setting the            *
 * preprocessor symbol RWBLOCK_LOGGING.  The logging code requires       *
 * Rogue Wave's Tools.h++ classes.  If RWBLOCK_LOGGING has been          *
 * defined when this source file is compiled, you can use the            *
 * following routines: (declared in rwblock.h)                           * 
 *                                                                       *
 *   ostream*      rwblockSetLogStream(ostream* s)                       *
 *     Sets logging on stream s.  Every time a block is allocated or     *
 *     deallocated a log line is written to the stream.  s=0 means no    *
 *     logging lines are written.  Returns previous log stream.          *
 *                                                                       *
 *   void          rwblockReport(ostream&)                               *
 *     Reports on outstanding RWBlocks to the indicated stream.          *
 *                                                                       *
 *   unsigned long rwblockBytesAllocated()                               *
 *     Reports number of bytes currently tied up in RWBlock allocations. *
 *                                                                       *
 *   unsigned      rwblockBlocksAllocated()                              *
 *     Reports the number of outstanding RWBlocks.                       *
 *                                                                       *
 * WARNING:  Enabling RWBlock logging will make your code run as much as *
 *           50 times more slowly!                                       *
 *                                                                       *
 *************************************************************************/

#include <iostream.h>
#include "rw/rwset.h"

class RWBlockRecord : public RWCollectable {
public:
  RWBlockRecord(unsigned n, size_t e, void *a) {nelem=n, elemsize=e, array=a;}
  unsigned          nelem;
  size_t            elemsize;
  void*             array;
  virtual RWBoolean isEqual(const RWCollectable* t) const {return ((RWBlockRecord*)t)->array==array;}
  virtual unsigned  hash()                          const {return (unsigned)array;}
};

static unsigned      nilBlocks=0;     // number of RWBlocks of size zero
static unsigned long totalAllocated=0;// total number of bytes allocated to RWBlocks
static RWSet         *allocated;      // set of RWBlockRecords, one for each outstanding RWBlock (see note below)
static ostream       *out=0;          // where to write reports

// The RWSet allocated is implemented as a pointer to an RWSet rather
// than an RWSet itself to avoid a situation where logAllocation is
// called by construction of a static DoubleVec, say, before the RWSet
// has been initialized.  Chaos would surely follow!  See ARM pg 20.

void logAllocation(unsigned nelem, size_t elemsize, void* array)
{
  if (array) {
    if (!allocated) allocated = new RWSet;
    allocated->insert(new RWBlockRecord(nelem,elemsize,array));
    totalAllocated += nelem*elemsize;
    if (out) {
      *out << "RWBlock: allocated " << nelem<< "*" <<elemsize << "=";
      *out << nelem*elemsize << " bytes at " << array;
    }
  } else {
    nilBlocks++;
    if (out) *out << "RWBlock: allocated nilblock";
  }
  if (out) {
    *out << ", total: " << rwblockBlocksAllocated() << " allocs, ";
    *out << rwblockBytesAllocated() << " bytes" << endl;
  }
}

void logDeallocation(void *array)
{
  if (array) {
    if (out) *out << "RWBlock: deallocated " << array << flush;
    RWBlockRecord r(0,0,array);
    RWBlockRecord *record = allocated ? (RWBlockRecord*)allocated->remove(&r) : 0;
    if (!record) {
      RWThrow( RWInternalErr( RWMessage(RWMATH_RWBDEALL) ));
    }
    totalAllocated -= record->nelem*record->elemsize;
    if (out) {
      *out << ", " << record->nelem << "*" << record->elemsize << "=";
      *out << record->nelem*record->elemsize << " bytes.";
    }
    delete record;
  } else {
    if (out) *out << "RWBlock: deallocated nilblock.";
    nilBlocks--;
  }
  if (out) {
    *out << " Left: " << rwblockBlocksAllocated() << " allocs, " << rwblockBytesAllocated();
    *out << " bytes" << endl;
  }
}

ostream* rwblockSetLogStream(ostream *o)
{
  ostream *oldout=out;
  out=o;
  return oldout;
}

void rwblockReport(ostream& s)
{
  s << "RWBlock: " << rwblockBlocksAllocated() << " allocs, " << rwblockBytesAllocated() << " bytes as follows:" << endl;
  s << "   " << nilBlocks << " nil blocks" << endl;
  if (allocated) {
    RWSetIterator i(*allocated);
    RWBlockRecord *r;
    while((r=(RWBlockRecord*)i())!=0) {
      s << "   " << r->array << ": " << r->nelem << "*" << r->elemsize << "=" << r->nelem*r->elemsize << " bytes" << endl;
    }
  }
}

unsigned rwblockBlocksAllocated()
{
  return nilBlocks + (allocated?allocated->entries():0);
}

unsigned long rwblockBytesAllocated()
{
  return totalAllocated;
}

#else    /* RWBLOCK_LOGGING is not set --- supply empty shells for routines */

ostream* rwblockSetLogStream(ostream*) {return 0;}
void     rwblockReport(ostream&)       {}

#endif   /* RWBLOCK_LOGGING */

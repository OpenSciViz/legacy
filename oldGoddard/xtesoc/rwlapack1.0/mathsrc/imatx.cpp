/*
 * Type specific routines for IntGenMat
 *
 * $Header: /users/rcs/mathsrc/imatx.cpp,v 1.3 1993/09/01 16:27:21 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: imatx.cpp,v $
 * Revision 1.3  1993/09/01  16:27:21  alv
 * now uses rwUninitialized form for simple constructors
 *
 * Revision 1.2  1993/01/26  21:54:57  alv
 * fixed wrong cast bug
 *
 * Revision 1.1  1993/01/22  23:51:00  alv
 * Initial revision
 *
 * 
 *    Rev 1.3   17 Oct 1991 09:15:42   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.2   08 Oct 1991 13:44:38   keffer
 * Math V4.0 beta 2
 * 
 *    Rev 1.1   24 Jul 1991 12:51:42   keffer
 * Added pvcs keywords
 *
 */

#include "rw/igenmat.h"
#include "rw/dgenmat.h"
#include "rw/fgenmat.h"

RCSID("$Header: /users/rcs/mathsrc/imatx.cpp,v 1.3 1993/09/01 16:27:21 alv Exp $");

// Convert to a DoubleGenMat
// Should be a friend DoubleGenMat, but...
IntGenMat::operator DoubleGenMat() const
{
  DoubleGenMat temp(rows(),cols(),rwUninitialized);
  double *dp = temp.data();
  for(MatrixLooper l(rows(),cols(),rowStride(),colStride()); l; ++l) {
    int n = l.length;
    const int *tp = data()+l.start;
    while(n--) {
      *dp++ = double(*tp); tp+=l.stride;
    }
  }
  return temp;
}

// narrow from DoubleGenMat to IntGenMat
IntGenMat
toInt(const DoubleGenMat& v)
{
  IntGenMat temp(v.rows(),v.cols(),rwUninitialized);
  int *dp = temp.data();
  for(MatrixLooper l(v.rows(),v.cols(),v.rowStride(),v.colStride()); l; ++l) {
    int n = l.length;
    const double *tp = v.data()+l.start;
    while(n--) {
      *dp++ = int(*tp); tp+=l.stride;
    }
  }
  return temp;
}

// narrow from FloatGenMat to IntGenMat
IntGenMat
toInt(const FloatGenMat& v)
{
  IntGenMat temp(v.rows(),v.cols(),rwUninitialized);
  int *dp = temp.data();
  for(MatrixLooper l(v.rows(),v.cols(),v.rowStride(),v.colStride()); l; ++l) {
    int n = l.length;
    const float *tp = v.data()+l.start;
    while(n--) {
      *dp++ = int(*tp); tp+=l.stride;
    }
  }
  return temp;
}

> #
> # $Header: /users/rcs/mathsrc/xmat.cpp,v 1.10 1993/10/04 21:44:01 alv Exp $
> #
> # $Log: xmat.cpp,v $
> # Revision 1.10  1993/10/04  21:44:01  alv
> # ported to Windows NT
> #
> # Revision 1.9  1993/09/20  04:19:44  alv
> # fixed bug caught by Symantec
> #
> # Revision 1.8  1993/09/19  17:30:49  alv
> # ported to lucid v3
> #
> # Revision 1.7  1993/09/17  18:14:38  alv
> # added RWRand& constructor
> #
> # Revision 1.6  1993/08/17  19:02:00  alv
> # now allows use of custom RWBlocks
> #
> # Revision 1.4  1993/05/10  19:29:27  alv
> # ported to Zortech 3.1
> #
> # Revision 1.3  1993/03/22  15:51:31  alv
> # changed PVCS Workfile keyword to RCS ID keyword
> #
> # Revision 1.2  1993/03/04  18:57:38  alv
> # changed char* ctor to const char*
> #
> # Revision 1.1  1993/01/22  23:51:07  alv
> # Initial revision
> #
> #
> define <ClassType>=GenMat
> include macros
/*
 * Definitions for <C>
 *
 * Generated from template $Id: xmat.cpp,v 1.10 1993/10/04 21:44:01 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include <rw/strstrea.h>  /* I use istrstream to parse character string ctor */
#include "rw/<a>genmat.h"
#include "rw/rwbla.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"
#include "rw/rand.h"

> if <R/C>==Complex
> # Pick the proper type of copy and set for the underlying precision
> # This is used to do the work in the GenMat(RealGenMat realpart) ctor.
>  if <p>==double
>   define COPY=rwdcopy  SET=rwdset
>  elif <p>==float
>   define COPY=rwscopy  SET=rwsset
>  else
>   err Unrecognized precision <p>
>  endif
> endif

RCSID("$Header: /users/rcs/mathsrc/xmat.cpp,v 1.10 1993/10/04 21:44:01 alv Exp $");

<C>::<C>(const char *string)
 : RWMatView()
{
  istrstream s((char*)string);
  scanFrom(s);
  if (!s.good()) {
    RWTHROW( RWExternalErr( RWMessage(RWMATH_STRCTOR,string) ));
  }
}

<C>::<C>(unsigned m, unsigned n, RWRand& r)
 : RWMatView(m,n,sizeof(<t>))
{
  for(int j=n; j--;) {
    for(int i=m; i--;) {
> if <R/C>==Complex    
    (*this)(i,j) = r.complex();
> else
    (*this)(i,j) = (<t>)r();
> endif
    }
  }
}

<C>::<C>(unsigned m, unsigned n, <t> scalar)
  : RWMatView(m,n,sizeof(<t>))
{
> if <t>==double || <t>==int || <R/C>==Complex
  rw<bla>set(n*m, data(), 1, &scalar);
> else
  <t> scalar2 = scalar;  // Temporary required to finesse K&R compilers
  rw<bla>set(n*m, data(), 1, &scalar2);
> endif
}

> # Special code for constructors of complex types:
> if <R/C>==Complex

// Type conversion: real->complex
<C>::<C>(const <P>GenMat& re)
  : RWMatView(re.rows(),re.cols(),sizeof(<t>))
{
  <t>* cp = data();
  for(MatrixLooper l(re.rows(),re.cols(), re.rowStride(),re.colStride(), RWDataView::COLUMN_MAJOR); l; ++l) {
#ifdef COMPLEX_PACKS
//    static const <p> zero = 0;     /* "static" required for Glockenspiel */
    <p> zero = 0;     		     /* above bombs on lucid, so use this nice simple form */
#ifdef IMAG_LEADS
    COPY(l.length, (<p>*)cp+1, 2, re.data()+l.start, l.stride);
    SET(l.length, (<p>*)cp, 2, &zero);
#else  /* Imaginary does not lead */
    COPY(l.length, (<p>*)cp, 2, re.data()+l.start, l.stride);
    SET(l.length, (<p>*)cp+1, 2, &zero);
#endif
    cp += l.length;
  }

#else /* Complex does not pack */

  for(int j=cols(); j--;) {
    for(int i=rows(); i--;) {
      (*this)(i,j) = re(i,j);
    }
  }
#endif /* COMPLEX_PACKS */
}  

<C>::<C>(const <P>GenMat& re, const <P>GenMat& im)
  : RWMatView(re.rows(),re.cols(),sizeof(<t>))
{
  lengthCheck(im.rows(),im.cols());
  <t>* cp = data();
  for(DoubleMatrixLooper l(re.rows(),re.cols(),re.rowStride(),re.colStride(),
	                   im.rowStride(),im.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
#ifdef COMPLEX_PACKS
#ifdef IMAG_LEADS
    COPY(l.length, (<p>*)cp+1, 2, re.data()+l.start1, l.stride1);
    COPY(l.length, (<p>*)cp, 2, im.data()+l.start2, l.stride2);
#else  /* Imaginary does not lead */
    COPY(l.length, (<p>*)cp, 2, re.data()+l.start1, l.stride1);
    COPY(l.length, (<p>*)cp+1, 2, im.data()+l.start2, l.stride2);
#endif
    cp += l.length;
  }

#else /* Complex does not pack */
  for(int j=cols(); j--;) {
    for(int i=rows(); i--;) {
      (*this)(i,j) = <t>(re(i,j),im(i,j));
    }
  }
#endif /* COMPLEX_PACKS */
}

> endif

<C>::<C>(const <t>* dat, unsigned m, unsigned n, Storage storage)
  : RWMatView(m,n,sizeof(<t>),storage)
{
  rw<bla>copy(nrows*ncols, data(), 1, dat, 1);
}

<C>::<C>(const <T>Vec& vec, unsigned m, unsigned n, Storage storage)
  : RWMatView(vec,vec.begin,m,n,(storage==ROW_MAJOR?n:1)*vec.stride(),
                                (storage==ROW_MAJOR?1:m)*vec.stride())
{
  vec.lengthCheck(m*n);
}

<C>::<C>(RWBlock *block, unsigned m, unsigned n, Storage storage)
  : RWMatView(block, m, n, storage)
{
  unsigned minlen = m*n*sizeof(<t>);
  if (block->length() < minlen) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_SHORTBLOCK, block->length(), minlen) ));
  }
}

// This constructor is used internally by real() and imag(), the
// slice() function and the Array::op()() functions.
<C>::<C>(const RWDataView& b, <t>* start, unsigned m, unsigned n, int rowstr, int colstr)
  : RWMatView(b,start,m,n,rowstr,colstr)
{
}

<C>& <C>::operator=(const <C>& rhs)
{
  lengthCheck(rhs.rows(),rhs.cols());
  if(sameDataBlock(rhs))
    (*this) = rhs.copy();   // Avoid aliasing problems
  else {
    for(DoubleMatrixLooper l(nrows,ncols,rowstep,colstep,rhs.rowstep,rhs.colstep); l; ++l) {
      rw<bla>copy(l.length, data()+l.start1, l.stride1, rhs.data()+l.start2, l.stride2);
    }
  }
  return *this;
}

<C>& <C>::operator=(<t> scalar)
{
  for(MatrixLooper l(nrows,ncols,rowstep,colstep); l; ++l) {
>  if <t>==double || <t>==int || <R/C>==Complex
    rw<bla>set(l.length,data()+l.start,l.stride,&scalar);
>  else
    <t> scalar2 = scalar; // Temporary needed to finesse K&R compilers
    rw<bla>set(l.length,data()+l.start,l.stride,&scalar2);
>  endif
  }
  return *this;
}

<C>& <C>::reference(const <C>& v)
{
  RWMatView::reference(v);
  return *this;
}

// Return copy of self with distinct instance variables
<C> <C>::copy(Storage s) const
{
  <C> temp(nrows,ncols,rwUninitialized,s);
  temp = *this;
  return temp;
}

// Synonym for copy().  Not made inline because some compilers
// have trouble with inlined temporaries with destructors
<C> <C>::deepCopy(Storage s) const
{
  return copy(s);
}

// Guarantee that references==1, rowstep=1 and colstep=nrows
void <C>::deepenShallowCopy(Storage s)
{
  RWBoolean isStrideCompact = (s==COLUMN_MAJOR) ? (rowstep==1 && colstep==nrows)
                                                : (colstep==1 && rowstep==ncols);
  if (!isStrideCompact || !isSimpleView()) {
    RWMatView::reference(copy(s));
  }
}

void <C>::resize(unsigned m, unsigned n)
{
  if(nrows!=m || ncols!=n) {
> if <R/C>==Complex
    <C> temp(m,n,<t>(0,0));
> else
    <C> temp(m,n,(<t>)0);
> endif
    int rowsToCopy = (nrows<m) ? nrows : m;
    int colsToCopy = (ncols<n) ? ncols : n;
    if (rowsToCopy>0 && colsToCopy>0) {
      for(DoubleMatrixLooper l(rowsToCopy,colsToCopy, temp.rowstep,temp.colstep, rowstep,colstep); l; ++l) {
        rw<bla>copy(l.length,temp.data()+l.start1,l.stride1,data()+l.start2,l.stride2);
      }
    }
    RWMatView::reference(temp);
  }
}

void <C>::reshape(unsigned m, unsigned n, Storage s)
{
  <C> temp(m,n,rwUninitialized,s);
  RWMatView::reference(temp);
}

<T>Vec <C>::fastSlice(int i, int j, unsigned n, int rowstr, int colstr) const
{
  return <T>Vec(*this,(<t>*)begin+i*rowstep+j*colstep,n,rowstr*rowstep+colstr*colstep);
}

<C> <C>::fastSlice(int i, int j, unsigned m, unsigned n, int rowstr1, int colstr1, int rowstr2, int colstr2) const
{
  return <C>(*this,(<t>*)begin+i*rowstep+j*colstep,m,n,
                    rowstr1*rowstep+colstr1*colstep,
                    rowstr2*rowstep+colstr2*colstep);
}

<T>Vec <C>::slice(int i, int j, unsigned n, int rowstr, int colstr) const
{
  sliceCheck(i,j,n,rowstr,colstr);
  return fastSlice(i,j,n,rowstr,colstr);
}

<C> <C>::slice(int i, int j, unsigned m, unsigned n, int rowstr1, int colstr1, int rowstr2, int colstr2) const
{
  sliceCheck(i,j,m,n,rowstr1,colstr1,rowstr2,colstr2);
  return fastSlice(i,j,m,n,rowstr1,colstr1,rowstr2,colstr2);
}

const <T>Vec <C>::diagonal(int k) const
{
  // Count on the slice function to do bounds checking
  if (k>=0) {
    return slice(0,k,(nrows<ncols-k) ? nrows : (ncols-k),1,1);
  } else {
    return slice(-k,0,(ncols<nrows+k) ? ncols : (nrows+k),1,1);
  }
}

<T>Vec <C>::diagonal(int k)
{
  // Count on the slice function to do bounds checking
  if (k>=0) {
    return slice(0,k,(nrows<ncols-k) ? nrows : (ncols-k),1,1);
  } else {
    return slice(-k,0,(ncols<nrows+k) ? ncols : (nrows+k),1,1);
  }
}

/*
 * Definitions for various arithmetic operations for SCharGenMat
 *
 * Generated from template $Id: xmatop.cpp,v 1.10 1993/09/01 16:27:57 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/scgenmat.h"
#include "rw/rwabla.h"	

RCSID("$Header: /users/rcs/mathsrc/xmatop.cpp,v 1.10 1993/09/01 16:27:57 alv Exp $");

/************************************************
 *                                              *
 *              UNARY OPERATORS                 *
 *                                              *
 ************************************************/

SCharGenMat operator-(const SCharGenMat& s)   // Unary minus
{
  SCharGenMat temp(s.rows(),s.cols(),rwUninitialized);
  SChar* dp = temp.data();
  for(MatrixLooper l(s.rows(),s.cols(),s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    int n = l.length;
    SChar* sp = (SChar*)s.data()+l.start;
    while(n--) { *dp++ = -(*sp); sp+=l.stride; }
  }
  return temp;
}

// Unary prefix increment on a SCharGenMat; (i.e. ++a)
SCharGenMat& SCharGenMat::operator++()
{
  for(MatrixLooper l(rows(),cols(),rowStride(),colStride()); l; ++l) {
    int n = l.length;
    SChar* sp = data()+l.start;
    while(n--) { ++(*sp); sp+=l.stride; }
  }
  return *this;
}

// Unary prefix decrement on a SCharGenMat (i.e., --a)
SCharGenMat&
SCharGenMat::operator--()
{
  for(MatrixLooper l(rows(),cols(),rowStride(),colStride()); l; ++l) {
    int n = l.length;
    SChar* sp = data()+l.start;
    while(n--) { --(*sp); sp+=l.stride; }
  }
  return *this;
}

/************************************************
 *                                              *
 *              BINARY OPERATORS                *
 *               vector - vector                *
 *                                              *
 ************************************************/

SCharGenMat operator+(const SCharGenMat& u, const SCharGenMat& v)
{
  unsigned m = v.rows();
  unsigned n = v.cols();
  u.lengthCheck(m,n);
  SCharGenMat temp(m,n,rwUninitialized);
  SChar* dp = temp.data();
  for(DoubleMatrixLooper l(m,n,u.rowStride(),u.colStride(),v.rowStride(),v.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rwa_plvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

SCharGenMat operator-(const SCharGenMat& u, const SCharGenMat& v)
{
  unsigned m = v.rows();
  unsigned n = v.cols();
  u.lengthCheck(m,n);
  SCharGenMat temp(m,n,rwUninitialized);
  SChar* dp = temp.data();
  for(DoubleMatrixLooper l(m,n,u.rowStride(),u.colStride(),v.rowStride(),v.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rwa_mivv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

SCharGenMat operator*(const SCharGenMat& u, const SCharGenMat& v)
{
  unsigned m = v.rows();
  unsigned n = v.cols();
  u.lengthCheck(m,n);
  SCharGenMat temp(m,n,rwUninitialized);
  SChar* dp = temp.data();
  for(DoubleMatrixLooper l(m,n,u.rowStride(),u.colStride(),v.rowStride(),v.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rwa_muvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

SCharGenMat operator/(const SCharGenMat& u, const SCharGenMat& v)
{
  unsigned m = v.rows();
  unsigned n = v.cols();
  u.lengthCheck(m,n);
  SCharGenMat temp(m,n,rwUninitialized);
  SChar* dp = temp.data();
  for(DoubleMatrixLooper l(m,n,u.rowStride(),u.colStride(),v.rowStride(),v.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    rwa_dvvv(l.length, dp, u.data()+l.start1, l.stride1, v.data()+l.start2, l.stride2);
    dp += l.length;
  }
  return temp;
}

/************************************************
 *                                              *
 *              BINARY OPERATORS                *
 *               vector - scalar                *
 *                                              *
 ************************************************/

SCharGenMat operator+(const SCharGenMat& s, SChar scalar)
{
  unsigned m = s.rows();
  unsigned n = s.cols();
  SCharGenMat temp(m,n,rwUninitialized);
  SChar* dp = temp.data();
  for(MatrixLooper l(m,n,s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    SChar scalar2 = scalar;
    rwa_plvs(l.length, dp, s.data()+l.start, l.stride, &scalar2);
    dp += l.length;
  }
  return temp;
}

SCharGenMat operator-(SChar scalar, const SCharGenMat& s)
{
  unsigned m = s.rows();
  unsigned n = s.cols();
  SCharGenMat temp(m,n,rwUninitialized);
  SChar* dp = temp.data();
  for(MatrixLooper l(m,n,s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    SChar scalar2 = scalar;
    rwa_misv(l.length, dp, &scalar2, s.data()+l.start, l.stride);
    dp += l.length;
  }
  return temp;
}

SCharGenMat operator*(const SCharGenMat& s, SChar scalar)
{
  unsigned m = s.rows();
  unsigned n = s.cols();
  SCharGenMat temp(m,n,rwUninitialized);
  SChar* dp = temp.data();
  for(MatrixLooper l(m,n,s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    SChar scalar2 = scalar;
    rwa_muvs(l.length, dp, s.data()+l.start, l.stride, &scalar2);
    dp += l.length;
  }
  return temp;
}

SCharGenMat operator/(SChar scalar, const SCharGenMat& s)
{
  unsigned m = s.rows();
  unsigned n = s.cols();
  SCharGenMat temp(m,n,rwUninitialized);
  SChar* dp = temp.data();
  for(MatrixLooper l(m,n,s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    SChar scalar2 = scalar;
    rwa_dvsv(l.length, dp, &scalar2, s.data()+l.start, l.stride);
    dp += l.length;
  }
  return temp;
}

SCharGenMat operator/(const SCharGenMat& s, SChar scalar)
{
  unsigned m = s.rows();
  unsigned n = s.cols();
  SCharGenMat temp(m,n,rwUninitialized);
  SChar* dp = temp.data();
  for(MatrixLooper l(m,n,s.rowStride(),s.colStride(),RWDataView::COLUMN_MAJOR); l; ++l) {
    SChar scalar2 = scalar;
    rwa_dvvs(l.length, dp, s.data()+l.start, l.stride, &scalar2);
    dp += l.length;
  }
  return temp;
}

/*
 * Out-of-line versions for compilers which can't 
 * handle inlined temporaries:
 */
#ifdef RW_NO_INLINED_TEMP_DESTRUCTORS
SCharGenMat     operator*(SChar s, const SCharGenMat& V)  {return V*s;}
SCharGenMat     operator+(SChar s, const SCharGenMat& V)  {return V+s;}
SCharGenMat     operator-(const SCharGenMat& V, SChar s)  {return V+(-s);}
#endif

/************************************************
 *                                              *
 *      ARITHMETIC ASSIGNMENT OPERATORS         *
 *        with other vectors                    *
 *                                              *
 ************************************************/

SCharGenMat& SCharGenMat::operator+=(const SCharGenMat& u)
{
  if (sameDataBlock(u)) { return operator+=(u.copy()); }  // Avoid aliasing
  u.lengthCheck(rows(),cols());
  for(DoubleMatrixLooper l(rows(),cols(),rowStride(),colStride(),u.rowStride(),u.colStride()); l; ++l) {
    rwa_aplvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

SCharGenMat& SCharGenMat::operator-=(const SCharGenMat& u)
{
  if (sameDataBlock(u)) { return operator-=(u.copy()); }  // Avoid aliasing
  u.lengthCheck(rows(),cols());
  for(DoubleMatrixLooper l(rows(),cols(),rowStride(),colStride(),u.rowStride(),u.colStride()); l; ++l) {
    rwa_amivv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

SCharGenMat& SCharGenMat::operator*=(const SCharGenMat& u)
{
  if (sameDataBlock(u)) { return operator*=(u.copy()); }  // Avoid aliasing
  u.lengthCheck(rows(),cols());
  for(DoubleMatrixLooper l(rows(),cols(),rowStride(),colStride(),u.rowStride(),u.colStride()); l; ++l) {
    rwa_amuvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

SCharGenMat& SCharGenMat::operator/=(const SCharGenMat& u)
{
  if (sameDataBlock(u)) { return operator/=(u.copy()); }  // Avoid aliasing
  u.lengthCheck(rows(),cols());
  for(DoubleMatrixLooper l(rows(),cols(),rowStride(),colStride(),u.rowStride(),u.colStride()); l; ++l) {
    rwa_advvv(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2);
  }
  return *this;
}

/************************************************
 *                                              *
 *      ARITHMETIC ASSIGNMENT OPERATORS         *
 *                with a scalar                 *
 *                                              *
 ************************************************/

SCharGenMat& SCharGenMat::operator+=(SChar scalar)
{
  for(MatrixLooper l(rows(),cols(),rowStride(),colStride()); l; ++l) {
    SChar scalar2 = scalar;
    rwa_aplvs(l.length, data()+l.start, l.stride, &scalar2);
  }
  return *this;
}

SCharGenMat& SCharGenMat::operator*=(SChar scalar)
{
  for(MatrixLooper l(rows(),cols(),rowStride(),colStride()); l; ++l) {
    SChar scalar2 = scalar;
    rwa_amuvs(l.length, data()+l.start, l.stride, &scalar2);
  }
  return *this;
}

SCharGenMat& SCharGenMat::operator/=(SChar scalar)
{
  for(MatrixLooper l(rows(),cols(),rowStride(),colStride()); l; ++l) {
    SChar scalar2 = scalar;
    rwa_advvs(l.length, data()+l.start, l.stride, &scalar2);
  }
  return *this;
}

/************************************************
 *                                              *
 *              LOGICAL OPERATORS               *
 *                                              *
 ************************************************/

RWBoolean SCharGenMat::operator==(const SCharGenMat& u) const
{
  if(rows()!=u.rows() || cols()!=u.cols()) return FALSE;  // They can't be equal if they don't have the same length.
  for(DoubleMatrixLooper l(rows(),cols(),rowStride(),colStride(),u.rowStride(),u.colStride()); l; ++l) {
    if (!rwasame(l.length, data()+l.start1, l.stride1, u.data()+l.start2, l.stride2)) {
      return FALSE;
    }
  }
  return TRUE;
}

RWBoolean SCharGenMat::operator!=(const SCharGenMat& u) const
{
  return !(*this == u);
}

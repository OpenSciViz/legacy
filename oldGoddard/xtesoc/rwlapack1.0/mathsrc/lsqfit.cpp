/*
 * Definitions for LeastSqFit (least squares fit) class.
 *
 * $Header: /users/rcs/mathsrc/lsqfit.cpp,v 1.1 1993/01/22 23:51:00 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 * $Log: lsqfit.cpp,v $
 * Revision 1.1  1993/01/22  23:51:00  alv
 * Initial revision
 *
 * 
 *    Rev 1.3   17 Oct 1991 09:15:20   keffer
 * Changed include path to <rw/xxx.h>
 * 
 *    Rev 1.2   08 Oct 1991 13:44:40   keffer
 * Math V4.0 beta 2
 * 
 *    Rev 1.1   24 Jul 1991 12:51:44   keffer
 * Added pvcs keywords
 *
 */

/* Definitions for least square fit class */

#include "rw/lsqfit.h"
#include "rw/rstream.h"
STARTWRAP
#include <math.h>
ENDWRAP

// construct a least squares fit with no weighting:

LeastSqFit::LeastSqFit(const DoubleVec& x, const DoubleVec& y)
{
   register int i;
   register double xi;
   register double yi;
   npts = x.length();
   double sum   = 0.;
   double sumx  = 0.;
   double sumx2 = 0.;
   double sumy  = 0.;
   double sumy2 = 0.;
   double sumxy = 0.;

   if(npts != y.length()) 
     npts = (npts > y.length()) ? y.length() : npts;
        
   for(i=0; i<npts; i++) {
     xi = x(i);
     yi = y(i);
     sum   += 1.;
     sumx  += xi;
     sumy  += yi;
     sumx2 += xi*xi;
     sumy2 += yi*yi;
     sumxy += xi*yi;
   }
 
   double temp = sum*sumx2 - sumx*sumx;
   LSq_intercept   = (sumx2*sumy - sumx*sumxy) / temp;
   LSq_slope       = (sumxy*sum  - sumx*sumy)  / temp;
   int c = npts - 2;
   double temp2 = (  sumy2 + LSq_intercept*LSq_intercept*sum + LSq_slope*LSq_slope*sumx2
          - 2.*(LSq_intercept*sumy +LSq_slope*sumxy - LSq_slope*LSq_intercept*sumx)  ) / c;
   SD_intercept = ::sqrt(temp2*sumx2 / temp);
   SD_slope     = ::sqrt(temp2*sum   / temp);
   correlation_coeff = (sum*sumxy - sumx*sumy) / 
                  ::sqrt(temp*(sum*sumy2 - sumy*sumy));
}



// construct a least squares fit with weights:

LeastSqFit::LeastSqFit(const DoubleVec& x, const DoubleVec& y, const DoubleVec& sigmay)
{
   register int i;
   register double xi;
   register double yi;
   register double weight;
   npts = x.length();
   double sum   = 0.;
   double sumx  = 0.;
   double sumx2 = 0.;
   double sumy  = 0.;
   double sumy2 = 0.;
   double sumxy = 0.;

   if(npts != y.length()) 
     npts = (npts > y.length()) ? y.length() : npts;
        
   for(i=0; i<npts; i++) {
     xi = x(i);
     yi = y(i);
     weight = 1./(sigmay(i)*sigmay(i));
     sum   += weight;
     sumx  += weight*xi;
     sumy  += weight*yi;
     sumx2 += weight*xi*xi;
     sumy2 += weight*yi*yi;
     sumxy += weight*xi*yi;
   }
 
   double temp = sum*sumx2 - sumx*sumx;
   LSq_intercept   = (sumx2*sumy - sumx*sumxy) / temp;
   LSq_slope       = (sumxy*sum  - sumx*sumy)  / temp;
   SD_intercept = ::sqrt(sumx2 / temp);
   SD_slope     = ::sqrt(sum   / temp);
   correlation_coeff = (sum*sumxy - sumx*sumy) / 
                  ::sqrt(temp*(sum*sumy2 - sumy*sumy));
}

unsigned
LeastSqFit::binaryStoreSize() const
{
  // Total storage requirements = 5*sizeof(double)
  // plus space for the ID number: unsigned short
  return 5*sizeof(Double) + sizeof(unsigned short);
}

ostream& 
operator<<(ostream& s, const LeastSqFit& f)
{
  s << "\nLeast Squares Linear Fit:\n\n";
  s << "slope :                   \t" << f.slope() << "\n";
  s << "intercept :               \t" << f.intercept() << "\n";
  s << "correlation coefficient : \t" << f.correlationCoeff() << "\n";
  s << "slope standard deviation: \t" << f.slopeStandardDev() << "\n";
  s << "intercept standard dev:   \t" << f.interceptStandardDev() << "\n";
  return s;
}

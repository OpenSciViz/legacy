/*
 * Definitions for IntGenMat
 *
 * Generated from template $Id: xmat.cpp,v 1.10 1993/10/04 21:44:01 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1991. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include <rw/strstrea.h>  /* I use istrstream to parse character string ctor */
#include "rw/igenmat.h"
#include "rw/rwbla.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"
#include "rw/rand.h"


RCSID("$Header: /users/rcs/mathsrc/xmat.cpp,v 1.10 1993/10/04 21:44:01 alv Exp $");

IntGenMat::IntGenMat(const char *string)
 : RWMatView()
{
  istrstream s((char*)string);
  scanFrom(s);
  if (!s.good()) {
    RWTHROW( RWExternalErr( RWMessage(RWMATH_STRCTOR,string) ));
  }
}

IntGenMat::IntGenMat(unsigned m, unsigned n, RWRand& r)
 : RWMatView(m,n,sizeof(int))
{
  for(int j=n; j--;) {
    for(int i=m; i--;) {
    (*this)(i,j) = (int)r();
    }
  }
}

IntGenMat::IntGenMat(unsigned m, unsigned n, int scalar)
  : RWMatView(m,n,sizeof(int))
{
  rwiset(n*m, data(), 1, &scalar);
}


IntGenMat::IntGenMat(const int* dat, unsigned m, unsigned n, Storage storage)
  : RWMatView(m,n,sizeof(int),storage)
{
  rwicopy(nrows*ncols, data(), 1, dat, 1);
}

IntGenMat::IntGenMat(const IntVec& vec, unsigned m, unsigned n, Storage storage)
  : RWMatView(vec,vec.begin,m,n,(storage==ROW_MAJOR?n:1)*vec.stride(),
                                (storage==ROW_MAJOR?1:m)*vec.stride())
{
  vec.lengthCheck(m*n);
}

IntGenMat::IntGenMat(RWBlock *block, unsigned m, unsigned n, Storage storage)
  : RWMatView(block, m, n, storage)
{
  unsigned minlen = m*n*sizeof(int);
  if (block->length() < minlen) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_SHORTBLOCK, block->length(), minlen) ));
  }
}

// This constructor is used internally by real() and imag(), the
// slice() function and the Array::op()() functions.
IntGenMat::IntGenMat(const RWDataView& b, int* start, unsigned m, unsigned n, int rowstr, int colstr)
  : RWMatView(b,start,m,n,rowstr,colstr)
{
}

IntGenMat& IntGenMat::operator=(const IntGenMat& rhs)
{
  lengthCheck(rhs.rows(),rhs.cols());
  if(sameDataBlock(rhs))
    (*this) = rhs.copy();   // Avoid aliasing problems
  else {
    for(DoubleMatrixLooper l(nrows,ncols,rowstep,colstep,rhs.rowstep,rhs.colstep); l; ++l) {
      rwicopy(l.length, data()+l.start1, l.stride1, rhs.data()+l.start2, l.stride2);
    }
  }
  return *this;
}

IntGenMat& IntGenMat::operator=(int scalar)
{
  for(MatrixLooper l(nrows,ncols,rowstep,colstep); l; ++l) {
    rwiset(l.length,data()+l.start,l.stride,&scalar);
  }
  return *this;
}

IntGenMat& IntGenMat::reference(const IntGenMat& v)
{
  RWMatView::reference(v);
  return *this;
}

// Return copy of self with distinct instance variables
IntGenMat IntGenMat::copy(Storage s) const
{
  IntGenMat temp(nrows,ncols,rwUninitialized,s);
  temp = *this;
  return temp;
}

// Synonym for copy().  Not made inline because some compilers
// have trouble with inlined temporaries with destructors
IntGenMat IntGenMat::deepCopy(Storage s) const
{
  return copy(s);
}

// Guarantee that references==1, rowstep=1 and colstep=nrows
void IntGenMat::deepenShallowCopy(Storage s)
{
  RWBoolean isStrideCompact = (s==COLUMN_MAJOR) ? (rowstep==1 && colstep==nrows)
                                                : (colstep==1 && rowstep==ncols);
  if (!isStrideCompact || !isSimpleView()) {
    RWMatView::reference(copy(s));
  }
}

void IntGenMat::resize(unsigned m, unsigned n)
{
  if(nrows!=m || ncols!=n) {
    IntGenMat temp(m,n,(int)0);
    int rowsToCopy = (nrows<m) ? nrows : m;
    int colsToCopy = (ncols<n) ? ncols : n;
    if (rowsToCopy>0 && colsToCopy>0) {
      for(DoubleMatrixLooper l(rowsToCopy,colsToCopy, temp.rowstep,temp.colstep, rowstep,colstep); l; ++l) {
        rwicopy(l.length,temp.data()+l.start1,l.stride1,data()+l.start2,l.stride2);
      }
    }
    RWMatView::reference(temp);
  }
}

void IntGenMat::reshape(unsigned m, unsigned n, Storage s)
{
  IntGenMat temp(m,n,rwUninitialized,s);
  RWMatView::reference(temp);
}

IntVec IntGenMat::fastSlice(int i, int j, unsigned n, int rowstr, int colstr) const
{
  return IntVec(*this,(int*)begin+i*rowstep+j*colstep,n,rowstr*rowstep+colstr*colstep);
}

IntGenMat IntGenMat::fastSlice(int i, int j, unsigned m, unsigned n, int rowstr1, int colstr1, int rowstr2, int colstr2) const
{
  return IntGenMat(*this,(int*)begin+i*rowstep+j*colstep,m,n,
                    rowstr1*rowstep+colstr1*colstep,
                    rowstr2*rowstep+colstr2*colstep);
}

IntVec IntGenMat::slice(int i, int j, unsigned n, int rowstr, int colstr) const
{
  sliceCheck(i,j,n,rowstr,colstr);
  return fastSlice(i,j,n,rowstr,colstr);
}

IntGenMat IntGenMat::slice(int i, int j, unsigned m, unsigned n, int rowstr1, int colstr1, int rowstr2, int colstr2) const
{
  sliceCheck(i,j,m,n,rowstr1,colstr1,rowstr2,colstr2);
  return fastSlice(i,j,m,n,rowstr1,colstr1,rowstr2,colstr2);
}

const IntVec IntGenMat::diagonal(int k) const
{
  // Count on the slice function to do bounds checking
  if (k>=0) {
    return slice(0,k,(nrows<ncols-k) ? nrows : (ncols-k),1,1);
  } else {
    return slice(-k,0,(ncols<nrows+k) ? ncols : (nrows+k),1,1);
  }
}

IntVec IntGenMat::diagonal(int k)
{
  // Count on the slice function to do bounds checking
  if (k>=0) {
    return slice(0,k,(nrows<ncols-k) ? nrows : (ncols-k),1,1);
  } else {
    return slice(-k,0,(ncols<nrows+k) ? ncols : (nrows+k),1,1);
  }
}

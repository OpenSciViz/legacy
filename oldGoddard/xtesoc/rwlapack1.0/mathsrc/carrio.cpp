/*
 * Definitions for DComplexArray I/O
 *
 * Generated from template $Id: xarrio.cpp,v 1.7 1993/09/17 18:42:21 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/carr.h"
#include "rw/rwfile.h"
#include "rw/vstream.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"
STARTWRAP
#include <stdio.h>
#include <ctype.h>
ENDWRAP

#  define CAST DComplex
#  define CTYPE DComplex

RCSID("$Header: /users/rcs/mathsrc/xarrio.cpp,v 1.7 1993/09/17 18:42:21 alv Exp $");

const unsigned short versionID = 2;             // Formatting version number

// Print out the array in row major order
void DComplexArray::printOn(ostream& s) const 
{
  for(int i=0; i<dimension(); ++i) {    // Print dimensions: eg, 5x3x6
    s << length(i);
    if (i<dimension()-1) { s << "x"; }
  }
  s << " [\n";
  if (prod(length())>0) {
    // Loop through the array printing a plane at a time.  The code
    // is much simpler if we don't have to worry about zero, one or
    // two dimension arrays.  To accomodate this, the looping index is
    // at least 3-D, but the index used to access the array in the 0,1,2D
    // cases is just a slice of the last entries of the index.
    int p = dimension();  // Save some typing
    IntVec index(((p>3)?p:3),0);
    IntVec accessIndex = index.slice(index.length()-p,p);
    RWBoolean isBeginningOfLine = TRUE;
    while(index((int)0)==0 || (p>2 && index((int)0)<length((int)0)) ) { // Loop over planes
      while(index(index.length()-2)==0 || (p>1 && accessIndex(p-2)<length(p-2)) ) { // Loop over lines
        while(index(index.length()-1)==0 || (p>0 && accessIndex(p-1)<length(p-1)) ) {
          if (!isBeginningOfLine) { s << " "; }
          CAST el = (*this)(accessIndex);
          s << el;
          isBeginningOfLine = FALSE;
          ++(index(index.length()-1));   // Incremement last component of index
        }
        isBeginningOfLine = TRUE;
        s << "\n";
        index(index.length()-1) = 0;
        ++(index(index.length()-2));
      }
      index(index.length()-2) = 0;       // Advance to next plane
      if (p<3) {
        ++(index((int)0));
      } else {      // The array is at least 3D
        int r=p-3;
        int numBlankLinesToPrint = 1;    // Only print these if not done
        while(++(index(r))>=npts(r) && --r>=0) {
          index(r+1)=0;
          numBlankLinesToPrint++;
        }
        if(r>=0) {
          while (--numBlankLinesToPrint >= 0) { s << "\n"; }
        }
      }
    }
  }
  s << "]";
}
        
void DComplexArray::scanFrom(istream& s)
{
  CAST item;
  char c;

  do { s.get(c); } while(s.good() && c!='[' && !isdigit(c));
  s.putback(c);

  IntVec n;                     // Read in array dimensions
  if (c!='[') {   // if c is a left bracket, this is a 0-D array
    do {
      n.resize(n.length()+1);
      s >> n(n.length()-1);
      do { s.get(c); } while (isspace(c) && s.good()); // Ignore whitespace
    } while (s.good() && c=='x');
  }
  if (c!='[') { s.putback(c); }
  
  reshape(n);
  // Loop through all the entries and read them in.
  for(RWMultiIndex i(n); s.good() && i; ++i) {
    s >> item;
    (*this)(i) = item;
  }
  
  if (s.good()) {
    do { s.get(c); } while(s.good() && isspace(c));  // Eat the trailing ]
  }
  if (c!=']') { s.putback(c); }

  return;
}

ostream& operator<<(ostream& s, const DComplexArray& v)
{
  v.printOn(s); return s;
}

istream& operator>>(istream& s, DComplexArray& v)
{
  v.scanFrom(s); return s;
}

void DComplexArray::saveOn(RWFile& file) const 
{
  file.Write(versionID);
  npts.saveOn(file);
  for(ArrayLooper l(npts,step); l; ++l) {

#  if COMPLEX_PACKS && !defined(IMAG_LEADS)

    if( l.stride == 1 ){
      // This is the normal case and things can be done very efficiently.
      // Write the N point complex vector as a 2N point real vector:
      const Double* dataStart = (const Double*) (data()+l.start);
      file.Write(dataStart, 2*l.length);
    }
    else {
      for(register int n = 0; n < l.length; n++) {
        DComplex x = data()[l.start+n*l.stride]; 
        file.Write( real(x) );
        file.Write( imag(x) );
      }
    }

#  else

    /*
     * Complex vector, but it is not packed.
     * Have to write it out, element-by-element
     */

    for(register int n = 0; n < l.length; n++) {
        DComplex x = data()[l.start+n*l.stride]; 
        file.Write( real(x) );
        file.Write( imag(x) );
    }

#  endif

  }
}

void DComplexArray::saveOn(RWvostream& s) const 
{
  s << versionID;               // Save the version number
  npts.saveOn(s);               // Save the vector length

  for(ArrayLooper l(npts,step); l; ++l) {

#  if COMPLEX_PACKS && !defined(IMAG_LEADS)

    if( l.stride == 1 ){
      // This is the normal case and things can be done very efficiently.
      // Write the N point complex vector as a 2N point real vector:
      const Double* dataStart = (const Double*) (data()+l.start);
      s.put(dataStart, 2*l.length);
    }
    else {
      for(register int n = 0; n < l.length; n++) {
        DComplex x = data()[l.start+n*l.stride];
        s << real(x);
        s << imag(x);
      }
    }

#  else 

    /*
     * Complex vector, but either it is not packed, or real does not lead.
     * Have to write it out, element-by-element
     */
    
    for(register int n = 0; n < l.length; n++) {
      s << real( data()[l.start+n*l.stride] );
      s << imag( data()[l.start+n*l.stride] );
    }

#  endif

  }
}

void DComplexArray::restoreFrom(RWFile& file)
{
  // Get and check the version number
  unsigned short ID;
  file.Read(ID);

  if( ID!=versionID && ID!=340 ){
    versionErr((int)versionID, (int)ID);
  }

  // Get the length and resize to it:
  IntVec lenVec;
  lenVec.restoreFrom(file);
  resize(lenVec);
  unsigned len = prod(lenVec);

#  if COMPLEX_PACKS

  // This is the normal case.  We can read the N point complex
  // vector as a 2N real vector.
  Double* dataStart = (Double*) data();
  file.Read(dataStart, 2*len);

#  else

  // Complex doesn't pack.  Have to read it in element-by-element
  Double tmpReal, tmpImag;
  for(register int n=0; n<len; n++){
    file.Read(tmpReal);
    file.Read(tmpImag);
    (data())[n] = DComplex(tmpReal, tmpImag);
  }

#  endif

}

void DComplexArray::restoreFrom(RWvistream& s)
{
  // Get and check the version number
  unsigned short ID;
  s >> ID;

  if( ID!=versionID && ID!=340 ){
    versionErr((int)versionID, (int)ID);
  }

  // Get the length and resize to it:
  IntVec lenVec;
  lenVec.restoreFrom(s);
  resize(lenVec);
  unsigned len = prod(lenVec);

#  if COMPLEX_PACKS

  // This is the normal case.  We can read the N point complex
  // vector as a 2N real vector.
  Double* dataStart = (Double*) data();
  s.get(dataStart, 2*len);

#else

  // Complex doesn't pack.  Have to read it in element-by-element
  Double tmpReal, tmpImag;
  for(register int n=0; n<len; n++){
    s >> tmpReal;
    s >> tmpImag;
    (data())[n] = DComplex(tmpReal, tmpImag);
  }

#  endif

}

unsigned DComplexArray::binaryStoreSize() const
{
  // Total storage requirements = Number of elements times their size, 
  // plus space for the vector length and ID number:
  return prod(length())*sizeof(DComplex) + npts.binaryStoreSize() + sizeof(versionID);
}

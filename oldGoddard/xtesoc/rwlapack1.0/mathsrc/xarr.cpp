> #
> # $Header: /users/rcs/mathsrc/xarr.cpp,v 1.12 1993/10/04 21:44:01 alv Exp $
> #
> # $Log: xarr.cpp,v $
> # Revision 1.12  1993/10/04  21:44:01  alv
> # ported to Windows NT
> #
> # Revision 1.11  1993/09/20  03:57:46  alv
> # fixed bug caught by Symantec
> #
> # Revision 1.10  1993/09/19  17:30:49  alv
> # ported to lucid v3
> #
> # Revision 1.9  1993/09/17  18:42:21  alv
> # (0) -> ((int)0) for benefit of DEC compiler
> #
> # Revision 1.8  1993/09/17  18:14:38  alv
> # added RWRand& constructor
> #
> # Revision 1.7  1993/09/01  16:27:48  alv
> # now uses rwUninitialized form for simple constructors
> #
> # Revision 1.6  1993/08/17  19:02:00  alv
> # now allows use of custom RWBlocks
> #
> # Revision 1.4  1993/05/10  19:29:27  alv
> # ported to Zortech 3.1
> #
> # Revision 1.3  1993/03/22  15:51:31  alv
> # changed PVCS Workfile keyword to RCS ID keyword
> #
> # Revision 1.2  1993/03/04  18:57:38  alv
> # changed char* ctor to const char*
> #
> # Revision 1.1  1993/01/22  23:51:06  alv
> # Initial revision
> #
> # 
> define <ClassType>=Array
> include macros
/*
 * Definitions for <C>
 *
 * Generated from template $Id: xarr.cpp,v 1.12 1993/10/04 21:44:01 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave 
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1992. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include <rw/strstrea.h>  /* I use istrstream to parse character string ctor */
#include "rw/<a>arr.h"
#include "rw/<a>genmat.h"
#include "rw/<a>vec.h"
#include "rw/rwbla.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"
#include "rw/rand.h"

> if <R/C>==Complex
// Complexes won't fit in a register
#define REGISTER
> else
#define REGISTER register
> endif

> if <R/C>==Complex
> # Pick the proper type of copy and set for the underlying precision
> # This is used to do the work in the Array(RealArray realpart) ctor.
>  if <p>==double
>   define COPY=rwdcopy  SET=rwdset
>  elif <p>==float
>   define COPY=rwscopy  SET=rwsset
>  else
>   err Unrecognized precision <p>
>  endif
> endif

RCSID("$Header");

<C>::<C>(const char *string)
 : RWArrayView(IntVec(),sizeof(<t>))
{
  istrstream s((char*)string);
  scanFrom(s);
  if (!s.good()) {
    RWTHROW( RWExternalErr( RWMessage(RWMATH_STRCTOR) ));
  }
}

<C>::<C>()
  : RWArrayView(IntVec(),sizeof(<t>))
{
}

<C>::<C>(const IntVec& n, RWUninitialized, Storage storage)
  : RWArrayView(n,sizeof(<t>),storage)
{
}

<C>::<C>(unsigned nx, unsigned ny, unsigned nz, RWUninitialized)
  : RWArrayView(nx,ny,nz,sizeof(<t>))
{
}

<C>::<C>(unsigned nx, unsigned ny, unsigned nz, unsigned nw, RWUninitialized)
  : RWArrayView(nx,ny,nz,nw,sizeof(<t>))
{
}

static void randFill(<C>& X, RWRand& r)
{
  for(RWMultiIndex i(X.length()); i; ++i) {
> if <R/C>==Complex    
    X(i) = r.complex();
> else
    X(i) = (<t>)r();
> endif
  }
}

<C>::<C>(const IntVec& n, RWRand& r, Storage storage)
  : RWArrayView(n,sizeof(<t>),storage)
{
  randFill(*this,r);
}

<C>::<C>(unsigned nx, unsigned ny, unsigned nz, RWRand& r)
  : RWArrayView(nx,ny,nz,sizeof(<t>))
{
  randFill(*this,r);
}

<C>::<C>(unsigned nx, unsigned ny, unsigned nz, unsigned nw, RWRand& r)
  : RWArrayView(nx,ny,nz,nw,sizeof(<t>))
{
  randFill(*this,r);
}

<C>::<C>(const IntVec& n, <t> scalar)
  : RWArrayView(n,sizeof(<t>))
{
> if <t>==double || <t>==int || <R/C>==Complex
  rw<bla>set(prod(n),data(),1,&scalar);
> else
  <t> scalar2 = scalar;  // Temporary required to finesse K&R compilers
  rw<bla>set(prod(n), data(), 1, &scalar2);
> endif
}

<C>::<C>(unsigned nx, unsigned ny, unsigned nz, <t> scalar)
  : RWArrayView(nx,ny,nz,sizeof(<t>))
{
> if <t>==double || <t>==int || <R/C>==Complex
  rw<bla>set(nx*ny*nz,data(),1,&scalar);
> else
  <t> scalar2 = scalar;  // Temporary required to finesse K&R compilers
  rw<bla>set(nx*ny*nz, data(), 1, &scalar2);
> endif
}

<C>::<C>(unsigned nx, unsigned ny, unsigned nz, unsigned nw, <t> scalar)
  : RWArrayView(nx,ny,nz,nw,sizeof(<t>))
{
> if <t>==double || <t>==int || <R/C>==Complex
  rw<bla>set(nx*ny*nz*nw,data(),1,&scalar);
> else
  <t> scalar2 = scalar;  // Temporary required to finesse K&R compilers
  rw<bla>set(nx*ny*nz*nw, data(), 1, &scalar2);
> endif
}

> # Special code for constructors of complex types:
> if <R/C>==Complex

// Type conversion: real->complex
<C>::<C>(const <P>Array& re)
  : RWArrayView(re.length(),sizeof(<t>))
{
  <t>* cp = data();
  for(ArrayLooper l(re.length(),re.stride()); l; ++l) {
#ifdef COMPLEX_PACKS
//    static const <p> zero = 0;     /* "static" required for Glockenspiel */
    <p> zero = 0;     		     /* above bombs on lucid, so use this nice simple form */
#ifdef IMAG_LEADS
    COPY(l.length, (<p>*)cp+1, 2, re.data()+l.start, l.stride);
    SET(l.length, (<p>*)cp, 2, &zero);
#else  /* Imaginary does not lead */
    COPY(l.length, (<p>*)cp, 2, re.data()+l.start, l.stride);
    SET(l.length, (<p>*)cp+1, 2, &zero);
#endif
    cp += l.length;

#else /* Complex does not pack */

    register const <p>* rp = re.data()+l.start;
    register int rstride = l.stride;
    register int i = l.length;
    while (i--) {
      *cp++ = <t>(*rp); 
      rp += rstride;
    }
#endif /* COMPLEX_PACKS */
  }
}  

<C>::<C>(const <P>Array& re, const <P>Array& im)
  : RWArrayView(re.length(),sizeof(<t>))
{
  lengthCheck(im.length());
  <t>* cp = data();
  for(DoubleArrayLooper l(re.length(),re.stride(),im.stride()); l; ++l) {
#ifdef COMPLEX_PACKS
#ifdef IMAG_LEADS
    COPY(l.length, (<p>*)cp+1, 2, re.data()+l.start1, l.stride1);
    COPY(l.length, (<p>*)cp, 2, im.data()+l.start2, l.stride2);
#else  /* Imaginary does not lead */
    COPY(l.length, (<p>*)cp, 2, re.data()+l.start1, l.stride1);
    COPY(l.length, (<p>*)cp+1, 2, im.data()+l.start2, l.stride2);
#endif
    cp += l.length;

#else /* Complex does not pack */
    register const <P>* rp = re.data()+l.start1;
    register const <P>* ip = im.data()+l.start2;
    register int rstride = l.stride1;
    register int istride = l.stride2;
    register int i = l.length;
    while(i--){
      *cp++ = <t>(*rp, *ip);
      rp += rstride;
      ip += istride;
    }   
#endif /* COMPLEX_PACKS */
  }
}

> endif

// Copy constructor
<C>::<C>(const <C>& a)
  : RWArrayView(a)
{
}

<C>::<C>(const <t>* dat, const IntVec& n)
  : RWArrayView(n,sizeof(<t>))
{
  rw<bla>copy(prod(n), data(), 1, dat, 1);
}

<C>::<C>(const <t>* dat, unsigned nx, unsigned ny, unsigned nz)
  : RWArrayView(nx,ny,nz,sizeof(<t>))
{
  rw<bla>copy(nx*ny*nz, data(), 1, dat, 1);
}

<C>::<C>(const <t>* dat, unsigned nx, unsigned ny, unsigned nz, unsigned nw)
  : RWArrayView(nx,ny,nz,nw,sizeof(<t>))
{
  rw<bla>copy(nx*ny*nz*nw, data(), 1, dat, 1);
}

<C>::<C>(const <T>Vec& dat, const IntVec& n)
  : RWArrayView(n,sizeof(<t>))
{
  rw<bla>copy(prod(n), data(), 1, dat.data(), dat.stride());
}

<C>::<C>(const <T>Vec& dat, unsigned nx, unsigned ny, unsigned nz)
  : RWArrayView(nx,ny,nz,sizeof(<t>))
{
  rw<bla>copy(nx*ny*nz, data(), 1, dat.data(), dat.stride());
}

<C>::<C>(const <T>Vec& dat, unsigned nx, unsigned ny, unsigned nz, unsigned nw)
  : RWArrayView(nx,ny,nz,nw,sizeof(<t>))
{
  rw<bla>copy(nx*ny*nz*nw, data(), 1, dat.data(), dat.stride());
}

<C>::<C>(RWBlock *block, const IntVec& n)
  : RWArrayView(block,n)
{
  unsigned minlen = prod(length())*sizeof(<t>);
  if (block->length() < minlen) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_SHORTBLOCK, block->length(), minlen) ));
  }
}

<C>::<C>(RWBlock *block, unsigned nx, unsigned ny, unsigned nz)
  : RWArrayView(block, nx, ny, nz)
{
  unsigned minlen = prod(length())*sizeof(<t>);
  if (block->length() < minlen) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_SHORTBLOCK, block->length(), minlen) ));
  }
}

<C>::<C>(RWBlock *block, unsigned nx, unsigned ny, unsigned nz, unsigned nw)
  : RWArrayView(block, nx, ny, nz, nw)
{
  unsigned minlen = prod(length())*sizeof(<t>);
  if (block->length() < minlen) {
    RWTHROW( RWBoundsErr( RWMessage(RWMATH_SHORTBLOCK, block->length(), minlen) ));
  }
}

<C>::<C>(const <T>Vec& v)
  : RWArrayView(v,(void*)v.data(),IntVec(1,v.length()),IntVec(1,1))
{
}

<C>::<C>(const <T>GenMat& m)
  : RWArrayView(m,(void*)m.data(),
       IntVec(2,m.rows(),m.cols()-m.rows()),                 // [ m.rows() m.cols() ]
       IntVec(2,m.rowStride(),m.colStride()-m.rowStride())) // [ m.rowStride() m.colStride() ]
{
}

// This constructor is used internally by real() and imag() and slices:
<C>::<C>(const RWDataView& b, <t>* start, const IntVec& n, const IntVec& str)
  : RWArrayView(b,start,n,str)
{
}

<C>& <C>::operator=(const <C>& rhs)
{
  lengthCheck(rhs.length());
  if(sameDataBlock(rhs)) {
    (*this) = rhs.deepCopy();   // Avoid aliasing problems
  } else {
    for(DoubleArrayLooper l(npts,step,rhs.step); l; ++l) {
      rw<bla>copy(l.length, data()+l.start1, l.stride1, rhs.data()+l.start2, l.stride2);
    }
  }
  return *this;
}

<C>& <C>::operator=(<t> scalar)
{
  for(ArrayLooper l(npts,step); l; ++l) {
>  if <t>==double || <t>==int || <R/C>==Complex
    rw<bla>set(l.length,data()+l.start,l.stride,&scalar);
>  else
    <t> scalar2 = scalar;  // Temporary required to finesse K&R compilers
    rw<bla>set(l.length,data()+l.start,l.stride,&scalar2);
>  endif
  }
  return *this;
}

<C>& <C>::reference(const <C>& v)
{
  RWArrayView::reference(v);
  return *this;
}

// Return copy of self with distinct instance variables
<C> <C>::deepCopy() const
{
  return copy();
}

// Synonym for deepCopy().  Not made inline because some compilers
// have trouble with inlined temporaries with destructors
<C> <C>::copy() const
{
  <C> temp(npts,rwUninitialized,COLUMN_MAJOR);
  temp = *this;
  return temp;
}

// Guarantee that references==1 and stride==1
void <C>::deepenShallowCopy()
{
  RWBoolean isStrideCompact = (dimension()==0 || step((int)0)==1);
  for( int r=npts.length()-1; r>0 && isStrideCompact; --r ) {
    if (step(r) != npts(r-1)*step(r-1)) isStrideCompact=FALSE;
  }
  if (!isStrideCompact || !isSimpleView()) {
    RWArrayView::reference(copy());
  }
}

void <C>::resize(const IntVec& N)
{
  if(N != npts){
> if <R/C>==Complex
    <C> temp(N,<t>(0,0));
> else
    <C> temp(N,(<t>)0);
> endif
    int p = npts.length()<N.length() ? npts.length() : N.length();  // # dimensions to copy
    if (p>0) {
      IntVec n(p,rwUninitialized);   // The size of the common parts of the arrays
      for(int i=0; i<p; i++) {
        n(i) = (N(i)<npts(i)) ? N(i) : npts(i);
      }
  
      // Build a view of the source common part
      IntGenMat sstr(dimension(),p,0);
      sstr.diagonal() = 1;
      <C> sp = slice(IntVec(dimension(),0), n, sstr);
  
      // Build a view of the destination common part
      IntGenMat dstr(N.length(),p,0);
      dstr.diagonal() = 1;
      <C> dp = temp.slice(IntVec(N.length(),0), n, dstr);
  
      dp = sp;
    }
    reference(temp);
  }
}

void <C>::resize(unsigned m, unsigned n, unsigned o)
{
  IntVec i(3,rwUninitialized);
  i((int)0) = m;
  i(1) = n;
  i(2) = o;
  resize(i);
}

void <C>::resize(unsigned m, unsigned n, unsigned o, unsigned p)
{
  IntVec i(4,rwUninitialized);
  i((int)0) = m;
  i(1) = n;
  i(2) = o;
  i(3) = p;
  resize(i);
}

void <C>::reshape(const IntVec& N)
{
  if (N!=npts) {
    <C> temp(N,rwUninitialized,COLUMN_MAJOR);
    reference(temp);
  }
}

void <C>::reshape(unsigned m, unsigned n, unsigned o)
{
  IntVec i(3,rwUninitialized);
  i((int)0) = m;
  i(1) = n;
  i(2) = o;
  reshape(i);
}

void <C>::reshape(unsigned m, unsigned n, unsigned o, unsigned p)
{
  IntVec i(4,rwUninitialized);
  i((int)0) = m;
  i(1) = n;
  i(2) = o;
  i(3) = p;
  reshape(i);
}

<C> <C>::slice(const IntVec& start, const IntVec& n, const IntGenMat& str) const
{
  sliceCheck(start, n, str);
  return <C>(*this, (<t>*)data()+dot(start,stride()), n, product(stride(),str));
}

<t> toScalar(const <C>& A)
{
  A.dimensionCheck(0);
  return *(A.data());
}

<T>Vec toVec(const <C>& A)
{
  A.dimensionCheck(1);
  return <T>Vec(A,(<t>*)A.data(),(unsigned)A.length((int)0),A.stride((int)0));
}

<T>GenMat toGenMat(const <C>& A)
{
  A.dimensionCheck(2);
  return <T>GenMat(A,(<t>*)A.data(),(unsigned)A.length((int)0),(unsigned)A.length(1),A.stride((int)0),A.stride(1));
}


/*
 * Math definitions
 *
 * Generated from template $Id: xvecmath.cpp,v 1.7 1993/09/16 17:16:56 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/fvec.h"
#include "rw/rwsbla.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"

#define REGISTER register

RCSID("$Header: /users/rcs/mathsrc/xvecmath.cpp,v 1.7 1993/09/16 17:16:56 alv Exp $");




FloatVec
FloatVec::apply(mathFunTy f) const
// Apply function returning float to each element of vector slice
{
  register i = length();
  FloatVec temp(i,rwUninitialized);
  register const float* sp = data();
  register float* dp = temp.data();
  register j = stride();
  while(i--) {
// Turbo C++ bug requires temporary.
#if defined (__TURBOC__)
     float dummy = f(*sp);
     *dp++ = dummy;
#else
     *dp++ = (*f)(*sp);
#endif
     sp += j; 
  }
  return temp;
}

#if defined(RW_NATIVE_EXTENDED)
FloatVec
FloatVec::apply(XmathFunTy f) const
// apply function for compilers using the extended type
{
  register i = length();
  FloatVec temp(i,rwUninitialized);
  register const float* sp = data();
  register float* dp = temp.data();
  register j = stride();
  while(i--) {
    *dp++ = (*f)(*sp);
    sp += j;
  }
  return temp;
}
#endif


FloatVec
atan2(const FloatVec& u,const FloatVec& v)
     // Arctangent of u/v.
{
  register unsigned i = v.length();
  u.lengthCheck(i);
  FloatVec temp(i,rwUninitialized);
  register const float* up = u.data();
  register const float* vp = v.data();
  register float* dp = temp.data();
  register uj = u.stride();
  register vj = v.stride();
  while (i--) { *dp++ = atan2(*up,*vp);  up += uj;  vp += vj; }
  return temp;
}

FloatVec
cumsum(const FloatVec& s)
// Cumulative sum of FloatVec slice.
// Note: V == delta(cumsum(V)) == cumsum(delta(V))
{
  unsigned i = s.length();
  FloatVec temp(i,rwUninitialized);
  register float* sp = (float*)s.data();
  register float* dp = temp.data();
  register float c = 0;
  register j = s.stride();
  while (i--) { c += *sp;  *dp++ = c;  sp += j; }
  return temp;
}

FloatVec
delta(const FloatVec& s)
     // Element differences of FloatVec slice.
     // Note: V == delta(cumsum(V)) == cumsum(delta(V))
{
  unsigned i = s.length();
  FloatVec temp(i,rwUninitialized);
  register float* sp = (float*)s.data();
  register float* dp = temp.data();
  register float c = 0;
  register j = s.stride();
  while (i--) { *dp++ = *sp - c; c = *sp; sp += j; }
  return temp;
}

float
dot(const FloatVec& u,const FloatVec& v)
     // Vector dot product.
{
  unsigned N = v.length();
  u.lengthCheck(N);
  float result;
  rwsdot(N, u.data(), u.stride(), v.data(), v.stride(), &result);
  return result;
}

int
maxIndex(const FloatVec& s)
// Index of maximum element.
{
  if (s.length() == 0) s.emptyErr("maxIndex");
  int result = rwsmax(s.length(), s.data(), s.stride());
  return result;
}

float
maxValue(const FloatVec& s)
{
  int inx = maxIndex(s);
  return s(inx);
}

int
minIndex(const FloatVec& s)
     // Index of minimum element.
{
  if (s.length() == 0) s.emptyErr("minIndex");
  int result = rwsmin(s.length(), s.data(), s.stride());
  return result;
}

float
minValue(const FloatVec& s)
{
  int inx = minIndex(s);
  return s(inx);
}


float
prod(const FloatVec& s)
     // Product of FloatVec elements.
{
  register unsigned i = s.length();
  register float* sp = (float*)s.data();
  REGISTER float t = 1;
  register j = s.stride();
  while (i--) { t *= *sp;  sp += j; }
  return t;
}

FloatVec
pow(const FloatVec& u, const FloatVec& v)
// u to the v power.
{
  register unsigned i = v.length();
  u.lengthCheck(i);
  FloatVec temp(i,rwUninitialized);
  register float* up = (float*)u.data();
  register float* vp = (float*)v.data();
  register float* dp = temp.data();
  register uj = u.stride();
  register vj = v.stride();
  while (i--) { *dp++ = pow(*up,*vp); up += uj; vp += vj; }
  return temp;
}

FloatVec
reverse(const FloatVec& s)
     // Reverse vector elements --- pretty tricky!
{
  return s.slice( s.length()-1, s.length(), -1);
}

float
sum(const FloatVec& s)
     // Sum of a FloatVec
{
  register unsigned i = s.length();
  register float* sp = (float*)s.data();
  register double t = 0;	// Accumulate floats as double

  register j = s.stride();
  while (i--) { t += *sp;  sp += j; }
  return (float)t;
}

float
variance(const FloatVec& s)
// Variance of a FloatVec
{
  register unsigned i = s.length();
  if(i<2){
    RWTHROW( RWInternalErr( RWMessage( RWMATH_NPOINTS, "FloatVec", (unsigned)i, "Variance needs minimum of", 2) ));
  }
  register float* sp = (float*)s.data();
  register double sum2 = 0;	// Accumulate sums in double precision
  REGISTER float avg = mean(s);
  register j = s.stride();
  while (i--) {
    REGISTER float temp = *sp - avg;
    sum2 += temp * temp;
    sp += j;
  }
  // Use the biased estimate (easier to work with):
  return (float)(sum2/s.length());
}

/*
 * Various functions with simple definitions:
 */
FloatVec	acos(const FloatVec& V)	{ return V.apply(::acos); }
FloatVec	asin(const FloatVec& V)	{ return V.apply(::asin); }
FloatVec	atan(const FloatVec& V)	{ return V.apply(::atan); }
FloatVec	ceil(const FloatVec& V)	{ return V.apply(::ceil); }
FloatVec	cos(const FloatVec& V)		{ return V.apply(::cos);  }
FloatVec	cosh(const FloatVec& V)	{ return V.apply(::cosh); }
FloatVec	exp(const FloatVec& V)		{ return V.apply(::exp);  }
FloatVec	floor(const FloatVec& V)	{ return V.apply(::floor);}
FloatVec	log(const FloatVec& V)		{ return V.apply(::log);  }
FloatVec	log10(const FloatVec& V)	{ return V.apply(::log10);}
float		mean(const FloatVec& V)	{ return sum(V)/V.length();}
FloatVec	sin(const FloatVec& V)		{ return V.apply(::sin);  }
FloatVec	sinh(const FloatVec& V)	{ return V.apply(::sinh); }
FloatVec	tan(const FloatVec& V)		{ return V.apply(::tan);  }
FloatVec	tanh(const FloatVec& V)	{ return V.apply(::tanh); }

#ifdef __ZTC__
/*
 * Zortech requires its own special version of abs(const FloatVec&)
 * because it uses an inlined version of fabs().  Hence, apply() 
 * cannot be used.
 */
FloatVec
abs(const FloatVec& s)
// Absolute value of a FloatVec --- Zortech version
{
  register unsigned i = s.length();
  FloatVec temp(i,rwUninitialized);
  register float* sp = (float*)s.data();
  register float* dp = temp.data();
  register j = s.stride();
  while (i--) { *dp++ = fabs(*sp); sp += j; }
  return temp;
}
#else   /* Not Zortech */
FloatVec	abs(const FloatVec& V)		{ return V.apply(::fabs); }
#endif	
#ifdef __ZTC__
/*
 * Zortech requires its own special version of sqrt(const FloatVec&) 
 * because it uses an inlined version of sqrt().  Hence, apply() 
 * cannot be used.
 */
FloatVec
sqrt(const FloatVec& s)
// Square root of a FloatVec --- Zortech version
{
  register unsigned i = s.length();
  FloatVec temp(i,rwUninitialized);
  register float* sp = (float*)s.data();
  register float* dp = temp.data();
  register j = s.stride();
  while (i--) { *dp++ = sqrt(*sp); sp += j; }
  return temp;
}
#else
FloatVec	sqrt(const FloatVec& V)	{ return V.apply(::sqrt); }
#endif

    /**** Norm functions ****/
Float l2Norm(const FloatVec& x)
{
  return sqrt(dot(x,x));
}

Float l1Norm(const FloatVec& x)
{
  return sum(abs(x));
}
	  
Float linfNorm(const FloatVec& x)
{
  return maxValue(abs(x));
}

Float maxNorm(const FloatVec& x)
{
  return linfNorm(x);
}

Float frobNorm(const FloatVec& x)
{
  return l2Norm(x);
}


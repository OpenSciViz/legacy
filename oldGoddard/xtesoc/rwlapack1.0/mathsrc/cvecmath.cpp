/*
 * Math definitions
 *
 * Generated from template $Id: xvecmath.cpp,v 1.7 1993/09/16 17:16:56 alv Exp $
 *
 ****************************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1989, 1990. This software is subject to copyright 
 * protection under the laws of the United States and other countries.
 *
 ***************************************************************************
 *
 */

#include "rw/cvec.h"
#include "rw/rwzbla.h"
#include "rw/rwerr.h"
#include "rw/matherr.h"

// Complexes won't fit in a register
#define REGISTER

RCSID("$Header: /users/rcs/mathsrc/xvecmath.cpp,v 1.7 1993/09/16 17:16:56 alv Exp $");

DoubleVec
abs(const DComplexVec& s)
// Absolute value of a DComplexVec
{
  register unsigned i = s.length();
  DoubleVec temp(i,rwUninitialized);
  register DComplex* sp = (DComplex*)s.data();	// Cast away "constness" to avoid problems with abs()
  register Double* dp = temp.data();
  register j = s.stride();
  while (i--) { *dp++ = abs(*sp); sp += j; }
  return temp;
}


DComplexVec
DComplexVec::apply(CmathFunTy f) const
// Apply function returning DComplex to each element of vector slice
{
  register i = length();
  DComplexVec temp(i,rwUninitialized);
  register DComplex* sp = (DComplex*)data();
  register DComplex* dp = temp.data();
  register j = stride();
  while(i--) {
// Turbo C++ bug requires temporary.
     DComplex dummy = (*f)(*sp);
     *dp++ = dummy;
     sp += j; 
  }
  return temp;
}

DoubleVec
DComplexVec::apply2(CmathFunTy2 f) const
// Apply function returning Double to each element of vector slice
{
  register i = length();
  DoubleVec temp(i,rwUninitialized);
  register DComplex* sp = (DComplex*)data();
  register Double* dp = temp.data();
  register j = stride();
  while (i--) { *dp++ = (*f)(*sp);  sp += j; }
  return temp;
}

#  ifdef COMPLEX_PACKS

/*
 * If we can guarantee that the complex vector goes
 * (real, imag, real, imag, real, imag, etc.) then
 * it becomes possible to implement real(DComplexVec&) and imag(DComplexVec&)
 * via a slice, allowing its use as an lvalue.  What
 * follows violates encapsulation (a bit), but sure is useful.
 */

DoubleVec
real(const DComplexVec& v)
{
#ifdef IMAG_LEADS
  return DoubleVec(v, (Double*)v.begin + 1, v.length(), 2*v.stride());
#else
  return DoubleVec(v, (Double*)v.begin, v.length(), 2*v.stride());
#endif
}
DoubleVec
imag(const DComplexVec& v)
{
#ifdef IMAG_LEADS
  return DoubleVec(v, (Double*)v.begin, v.length(), 2*v.stride());
#else
  return DoubleVec(v, (Double*)v.begin + 1, v.length(), 2*v.stride());
#endif
}

// The above allows a very compact form of conj() to be written:
DComplexVec
conj(const DComplexVec& V)
{
  return DComplexVec(real(V), -imag(V));
}

#else	/* Complex does not pack */

DoubleVec
real(const DComplexVec& v)
{
  register unsigned i = v.length();
  DoubleVec temp(i,rwUninitialized);
  register const DComplex* sp = v.data();
  register Double* dp = temp.data();
  register j = v.stride();
  while(i--) {*dp++ = real(*sp); sp += j;}
  return temp;
}

DoubleVec
imag(const DComplexVec& v)
{
  register unsigned i = v.length();
  DoubleVec temp(i,rwUninitialized);
  register const DComplex* sp = v.data();
  register Double* dp = temp.data();
  register j = v.stride();
  while(i--) {*dp++ = imag(*sp); sp += j;}
  return temp;
}

DComplexVec	conj(const DComplexVec& V)	{return V.apply(::conj); }

#endif /* Not COMPLEX_PACKS */

DComplex
conjDot(const DComplexVec& u,const DComplexVec& v)
     // Conjugate dot product: returns dot product of u star v.
{
  unsigned N = v.length();
  u.lengthCheck(N);
  DComplex result;
  rwzcdot(N, u.data(), u.stride(), v.data(), v.stride(), &result);
  return result;
}



DComplexVec
cumsum(const DComplexVec& s)
// Cumulative sum of DComplexVec slice.
// Note: V == delta(cumsum(V)) == cumsum(delta(V))
{
  unsigned i = s.length();
  DComplexVec temp(i,rwUninitialized);
  register DComplex* sp = (DComplex*)s.data();
  register DComplex* dp = temp.data();
  DComplex c(0,0);
  register j = s.stride();
  while (i--) { c += *sp;  *dp++ = c;  sp += j; }
  return temp;
}

DComplexVec
delta(const DComplexVec& s)
     // Element differences of DComplexVec slice.
     // Note: V == delta(cumsum(V)) == cumsum(delta(V))
{
  unsigned i = s.length();
  DComplexVec temp(i,rwUninitialized);
  register DComplex* sp = (DComplex*)s.data();
  register DComplex* dp = temp.data();
  DComplex c(0,0);
  register j = s.stride();
  while (i--) { *dp++ = *sp - c; c = *sp; sp += j; }
  return temp;
}

DComplex
dot(const DComplexVec& u,const DComplexVec& v)
     // Vector dot product.
{
  unsigned N = v.length();
  u.lengthCheck(N);
  DComplex result;
  rwzdot(N, u.data(), u.stride(), v.data(), v.stride(), &result);
  return result;
}


DComplex
prod(const DComplexVec& s)
     // Product of DComplexVec elements.
{
  register unsigned i = s.length();
  register DComplex* sp = (DComplex*)s.data();
  REGISTER DComplex t(1,0);
  register j = s.stride();
  while (i--) { t *= *sp;  sp += j; }
  return t;
}

DComplexVec
pow(const DComplexVec& u, const DComplexVec& v)
// u to the v power.
{
  register unsigned i = v.length();
  u.lengthCheck(i);
  DComplexVec temp(i,rwUninitialized);
  register DComplex* up = (DComplex*)u.data();
  register DComplex* vp = (DComplex*)v.data();
  register DComplex* dp = temp.data();
  register uj = u.stride();
  register vj = v.stride();
  while (i--) { *dp++ = pow(*up,*vp); up += uj; vp += vj; }
  return temp;
}

DComplexVec
reverse(const DComplexVec& s)
     // Reverse vector elements --- pretty tricky!
{
  return s.slice( s.length()-1, s.length(), -1);
}

DComplex
sum(const DComplexVec& s)
     // Sum of a DComplexVec
{
  register unsigned i = s.length();
  register DComplex* sp = (DComplex*)s.data();
  DComplex t(0,0);

  register j = s.stride();
  while (i--) { t += *sp;  sp += j; }
  return (DComplex)t;
}

double
variance(const DComplexVec& s)
// Variance of a DComplexVec
{
  register unsigned i = s.length();
  if(i<2){
    RWTHROW( RWInternalErr( RWMessage( RWMATH_NPOINTS, "DComplexVec", (unsigned)i, "Variance needs minimum of", 2) ));
  }
  register DComplex* sp = (DComplex*)s.data();
  register double sum2 = 0;	// Accumulate sums in double precision
  REGISTER DComplex avg = mean(s);
  register j = s.stride();
  while (i--) {
    REGISTER DComplex temp = *sp - avg;
    sum2 += norm(temp);
    sp += j;
  }
  // Use the biased estimate (easier to work with):
  return (double)(sum2/s.length());
}

/*
 * Various functions with simple definitions:
 */
DComplexVec	cos(const DComplexVec& V)		{ return V.apply(::cos);  }
DComplexVec	cosh(const DComplexVec& V)	{ return V.apply(::cosh); }
DComplexVec	exp(const DComplexVec& V)		{ return V.apply(::exp);  }
DComplex		mean(const DComplexVec& V)	{ DComplex s = sum(V); return s/DComplex(double(V.length()),0); }
DComplexVec	sin(const DComplexVec& V)		{ return V.apply(::sin);  }
DComplexVec	sinh(const DComplexVec& V)	{ return V.apply(::sinh); }
DoubleVec	arg(const DComplexVec& V)	{return V.apply2(::arg);  }
DoubleVec	norm(const DComplexVec& V)	{return V.apply2(::norm); }
/* One oddball function: */
DComplexVec	rootsOfOne(int N) {return rootsOfOne(N, (N>=0 ? N : -N) );}

#ifdef __ZTC__
/*
 * Zortech requires its own special version of sqrt(const DComplexVec&) 
 * because it uses an inlined version of sqrt().  Hence, apply() 
 * cannot be used.
 */
DComplexVec
sqrt(const DComplexVec& s)
// Square root of a DComplexVec --- Zortech version
{
  register unsigned i = s.length();
  DComplexVec temp(i,rwUninitialized);
  register DComplex* sp = (DComplex*)s.data();
  register DComplex* dp = temp.data();
  register j = s.stride();
  while (i--) { *dp++ = sqrt(*sp); sp += j; }
  return temp;
}
#else
DComplexVec	sqrt(const DComplexVec& V)	{ return V.apply(::sqrt); }
#endif

    /**** Norm functions ****/
Double l2Norm(const DComplexVec& x)
{
  return sqrt(real(conjDot(x,x)));
}

Double l1Norm(const DComplexVec& x)
{
  return sum(sqrt(norm(x)));
}
	  
Double linfNorm(const DComplexVec& x)
{
  return sqrt(maxValue(norm(x)));
}

Double maxNorm(const DComplexVec& x)
{
  return linfNorm(x);
}

Double frobNorm(const DComplexVec& x)
{
  return l2Norm(x);
}


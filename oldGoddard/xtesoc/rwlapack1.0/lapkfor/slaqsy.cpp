/*
 * C++ implementation of Lapack routine slaqsy
 *
 * $Id: slaqsy.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:00:15
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: slaqsy.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ONE = 1.0e0;
const float THRESH = 0.1e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ slaqsy(const char &uplo, long &n, float *a, long &lda, 
 float s[], float &scond, float &amax, char &equed)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  long _do0, _do1, _do2, _do3, i, i_, j, j_;
  float cj, large, small;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SLAQSY equilibrates a symmetric matrix A using the scaling factors
  //  in the vector S.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangular part of the
  //          symmetric matrix A is stored.
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  A       (input/output) REAL array, dimension (LDA,N)
  //          On entry, the symmetric matrix A.  If UPLO = 'U', the leading
  //          n by n upper triangular part of A contains the upper
  //          triangular part of the matrix A, and the strictly lower
  //          triangular part of A is not referenced.  If UPLO = 'L', the
  //          leading n by n lower triangular part of A contains the lower
  //          triangular part of the matrix A, and the strictly upper
  //          triangular part of A is not referenced.
  
  //          On exit, if EQUED = 'Y', the equilibrated matrix:
  //          diag(S) * A * diag(S).
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(N,1).
  
  //  S       (input) REAL array, dimension (N)
  //          The scale factors for A.
  
  //  SCOND   (input) REAL
  //          Ratio of the smallest S(i) to the largest S(i).
  
  //  AMAX    (input) REAL
  //          Absolute value of largest matrix entry.
  
  //  EQUED   (output) CHARACTER*1
  //          Specifies whether or not equilibration was done.
  //          = 'N':  No equilibration.
  //          = 'Y':  Equilibration was done, i.e., A has been replaced by
  //                  diag(S) * A * diag(S).
  
  //  Internal Parameters
  //  ===================
  
  //  THRESH is a threshold value used to decide if scaling should be done
  //  based on the ratio of the scaling factors.  If SCOND < THRESH,
  //  scaling is done.
  
  //  LARGE and SMALL are threshold values used to decide if scaling should
  //  be done based on the absolute size of the largest matrix element.
  //  If AMAX > LARGE or AMAX < SMALL, scaling is done.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Quick return if possible
  
  if( n <= 0 ) { 
    equed = 'N';
    return;
  }
  
  //     Initialize LARGE and SMALL.
  
  small = slamch( 'S'/*Safe minimum*/ )/slamch( 'P'/*Precision*/
    );
  large = ONE/small;
  
  if( (scond >= THRESH && amax >= small) && amax <= large ) { 
    
    //        No equilibration
    
    equed = 'N';
  }
  else { 
    
    //        Replace A by diag(S) * A * diag(S).
    
    if( lsame( uplo, 'U' ) ) { 
      
      //           Upper triangle of A is stored.
      
      for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
        cj = s[j_];
        for( i = 1, i_ = i - 1, _do1 = j; i <= _do1; i++, i_++ ) { 
          A(j_,i_) = cj*s[i_]*A(j_,i_);
        }
      }
    }
    else { 
      
      //           Lower triangle of A is stored.
      
      for( j = 1, j_ = j - 1, _do2 = n; j <= _do2; j++, j_++ ) { 
        cj = s[j_];
        for( i = j, i_ = i - 1, _do3 = n; i <= _do3; i++, i_++ ) { 
          A(j_,i_) = cj*s[i_]*A(j_,i_);
        }
      }
    }
    equed = 'Y';
  }
  
  return;
  
  //     End of SLAQSY
  
#undef  A
} // end of function 


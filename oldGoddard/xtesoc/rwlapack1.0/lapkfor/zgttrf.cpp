/*
 * C++ implementation of Lapack routine zgttrf
 *
 * $Id: zgttrf.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:47:07
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zgttrf.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const DComplex ZERO = DComplex(0.0e0);
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zgttrf(const long &n, DComplex dl[], DComplex d[], DComplex du[], 
 DComplex du2[], long ipiv[], long &info)
{
  long _do0, _do1, i, i_;
  DComplex fact, temp;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZGTTRF computes an LU factorization of a DComplex tridiagonal matrix A
  //  using elimination with partial pivoting and row interchanges.
  
  //  The factorization has the form
  //     A = L * U
  //  where L is a product of permutation and unit lower bidiagonal
  //  matrices and U is upper triangular with nonzeros in only the main
  //  diagonal and first two superdiagonals.
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The order of the matrix A.
  
  //  DL      (input/output) COMPLEX*16 array, dimension (N-1)
  //          On entry, DL must contain the (n-1) sub-diagonal elements of
  //          A.
  
  //          On exit, DL is overwritten by the (n-1) multipliers that
  //          define the matrix L from the LU factorization of A.
  
  //  D       (input/output) COMPLEX*16 array, dimension (N)
  //          On entry, D must contain the diagonal elements of A.
  
  //          On exit, D is overwritten by the n diagonal elements of the
  //          upper triangular matrix U from the LU factorization of A.
  
  //  DU      (input/output) COMPLEX*16 array, dimension (N-1)
  //          On entry, DU must contain the (n-1) super-diagonal elements
  //          of A.
  
  //          On exit, DU is overwritten by the (n-1) elements of the first
  //          super-diagonal of U.
  
  //  DU2     (output) COMPLEX*16 array, dimension (N-2)
  //          On exit, DU2 is overwritten by the (n-2) elements of the
  //          second super-diagonal of U.
  
  //  IPIV    (output) INTEGER array, dimension (N)
  //          The pivot indices; for 1 <= i <= n, row i of the matrix was
  //          interchanged with row IPIV(i).  IPIV(i) will always be either
  //          i or i+1; IPIV(i) = i indicates a row interchange was not
  //          required.
  
  //  INFO    (output)
  //          = 0:  successful exit
  //          < 0:  if INFO = -k, the k-th argument had an illegal value
  //          > 0:  if INFO = k, U(k,k) is exactly zero. The factorization
  //                has been completed, but the factor U is exactly
  //                singular, and division by zero will occur if it is used
  //                to solve a system of equations.
  
  //  =====================================================================
  
  //     .. Local Scalars ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Parameters ..
  //     ..
  //     .. Executable Statements ..
  
  info = 0;
  if( n < 0 ) { 
    info = -1;
    xerbla( "ZGTTRF", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  //     Initialize IPIV(i) = i
  
  for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
    ipiv[i_] = i;
  }
  
  for( i = 1, i_ = i - 1, _do1 = n - 1; i <= _do1; i++, i_++ ) { 
    if( ctocf(dl[i_]) == ctocf(ZERO) ) { 
      
      //           Subdiagonal is zero, no elimination is required.
      
      if( ctocf(d[i_]) == ctocf(ZERO) && info == 0 ) 
        info = i;
      if( i < n - 1 ) 
        du2[i_] = ZERO;
    }
    else if( abs( d[i_] ) >= abs( dl[i_] ) ) { 
      
      //           No row interchange required, eliminate DL(I)
      
      fact = dl[i_]/d[i_];
      dl[i_] = fact;
      d[i_ + 1] = d[i_ + 1] - fact*du[i_];
      if( i < n - 1 ) 
        du2[i_] = ZERO;
    }
    else { 
      
      //           Interchange rows I and I+1, eliminate DL(I)
      
      fact = d[i_]/dl[i_];
      d[i_] = dl[i_];
      dl[i_] = fact;
      temp = du[i_];
      du[i_] = d[i_ + 1];
      d[i_ + 1] = temp - fact*d[i_ + 1];
      if( i < n - 1 ) { 
        du2[i_] = du[i_ + 1];
        du[i_ + 1] = -(fact*du[i_ + 1]);
      }
      ipiv[i_] = ipiv[i_] + 1;
    }
  }
  if( ctocf(d[n - 1]) == ctocf(ZERO) && info == 0 ) { 
    info = n;
    return;
  }
  
  return;
  
  //     End of ZGTTRF
  
} // end of function 


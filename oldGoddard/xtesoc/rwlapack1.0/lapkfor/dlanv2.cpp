/*
 * C++ implementation of lapack routine dlanv2
 *
 * $Id: dlanv2.cpp,v 1.5 1993/04/06 20:41:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:35:39
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dlanv2.cpp,v $
 * Revision 1.5  1993/04/06  20:41:09  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:15:33  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:07:24  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ZERO = 0.0e0;
const double HALF = 0.5e0;
const double ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dlanv2(double &a, double &b, double &c, double &d, 
 double &rt1r, double &rt1i, double &rt2r, double &rt2i, double &cs, 
 double &sn)
{
  // long info;
  double aa, bb, cc, cs1, dd, p, sab, sac, sigma, sn1, tau, temp;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DLANV2 computes the Schur factorization of a real 2-by-2 nonsymmetric
  //  matrix in standard form:
  
  //       [ A  B ] = [ CS -SN ] [ AA  BB ] [ CS  SN ]
  //       [ C  D ]   [ SN  CS ] [ CC  DD ] [-SN  CS ]
  
  //  where either
  //  1) CC = 0 so that AA and DD are real eigenvalues of the matrix, or
  //  2) AA = DD and BB*CC < 0, so that AA + or - sqrt(BB*CC) are DComplex
  //  conjugate eigenvalues.
  
  //  Arguments
  //  =========
  
  //  A       (input/output) DOUBLE PRECISION
  //  B       (input/output) DOUBLE PRECISION
  //  C       (input/output) DOUBLE PRECISION
  //  D       (input/output) DOUBLE PRECISION
  //          On entry, the elements of the input matrix.
  //          On exit, they are overwritten by the elements of the
  //          standardised Schur form.
  
  //  RT1R    (output) DOUBLE PRECISION
  //  RT1I    (output) DOUBLE PRECISION
  //  RT2R    (output) DOUBLE PRECISION
  //  RT2I    (output) DOUBLE PRECISION
  //          The real and imaginary parts of the eigenvalues. If the
  //          eigenvalues are both real, abs(RT1R) >= abs(RT2R); if the
  //          eigenvalues are a DComplex conjugate pair, RT1I > 0.
  
  //  CS      (output) DOUBLE PRECISION
  //  SN      (output) DOUBLE PRECISION
  //          Parameters of the rotation matrix.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  // info = 0;
  
  //     Initialize CS and SN
  
  cs = ONE;
  sn = ZERO;
  
  if( c == ZERO ) { 
    goto L_10;
    
  }
  else if( b == ZERO ) { 
    
    //        Swap rows and columns
    
    cs = ZERO;
    sn = ONE;
    temp = d;
    d = a;
    a = temp;
    b = -c;
    c = ZERO;
    goto L_10;
  }
  else if( a == d && sign( ONE, b ) != sign( ONE, c ) ) { 
    goto L_10;
  }
  else { 
    
    //        Make diagonal elements equal
    
    temp = a - d;
    p = HALF*temp;
    sigma = b + c;
    tau = dlapy2( sigma, temp );
    cs1 = sqrt( HALF*(ONE + abs( sigma )/tau) );
    sn1 = -(p/(tau*cs1))*sign( ONE, sigma );
    
    //        Compute [ AA  BB ] = [ A  B ] [ CS1 -SN1 ]
    //                [ CC  DD ]   [ C  D ] [ SN1  CS1 ]
    
    aa = a*cs1 + b*sn1;
    bb = -a*sn1 + b*cs1;
    cc = c*cs1 + d*sn1;
    dd = -c*sn1 + d*cs1;
    
    //        Compute [ A  B ] = [ CS1  SN1 ] [ AA  BB ]
    //                [ C  D ]   [-SN1  CS1 ] [ CC  DD ]
    
    a = aa*cs1 + cc*sn1;
    b = bb*cs1 + dd*sn1;
    c = -aa*sn1 + cc*cs1;
    d = -bb*sn1 + dd*cs1;
    
    //        Accumulate transformation
    
    temp = cs*cs1 - sn*sn1;
    sn = cs*sn1 + sn*cs1;
    cs = temp;
    
    temp = HALF*(a + d);
    a = temp;
    d = temp;
    
    if( c != ZERO ) { 
      if( b != ZERO ) { 
        if( sign( ONE, b ) == sign( ONE, c ) ) { 
          
          //                 Real eigenvalues: reduce to upper triangular form
          
          sab = sqrt( abs( b ) );
          sac = sqrt( abs( c ) );
          p = sign( sab*sac, c );
          tau = ONE/sqrt( abs( b + c ) );
          a = temp + p;
          d = temp - p;
          b = b - c;
          c = ZERO;
          cs1 = sab*tau;
          sn1 = sac*tau;
          temp = cs*cs1 - sn*sn1;
          sn = cs*sn1 + sn*cs1;
          cs = temp;
        }
      }
      else { 
        b = -c;
        c = ZERO;
        temp = cs;
        cs = -sn;
        sn = temp;
      }
    }
  }
  
L_10:
  ;
  
  //     Store eigenvalues in (RT1R,RT1I) and (RT2R,RT2I).
  
  rt1r = a;
  rt2r = d;
  if( c == ZERO ) { 
    rt1i = ZERO;
    rt2i = ZERO;
  }
  else { 
    rt1i = sqrt( abs( b ) )*sqrt( abs( c ) );
    rt2i = -rt1i;
  }
  return;
  
  //     End of DLANV2
  
} // end of function 


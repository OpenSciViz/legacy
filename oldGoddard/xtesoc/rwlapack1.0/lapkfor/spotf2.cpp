/*
 * C++ implementation of Lapack routine spotf2
 *
 * $Id: spotf2.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:01:52
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: spotf2.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ONE = 1.0e0;
const float ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ spotf2(const char &uplo, const long &n, float *a, const long &lda, 
 long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  int upper;
  long _do0, _do1, j, j_;
  float ajj;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SPOTF2 computes the Cholesky factorization of a real symmetric
  //  positive definite matrix A.
  
  //  The factorization has the form
  //     A = U' * U ,  if UPLO = 'U', or
  //     A = L  * L',  if UPLO = 'L',
  //  where U is an upper triangular matrix and L is lower triangular.
  
  //  This is the unblocked version of the algorithm, calling Level 2 BLAS.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangular part of the
  //          symmetric matrix A is stored.
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  A       (input/output) REAL array, dimension (LDA,N)
  //          On entry, the symmetric matrix A.  If UPLO = 'U', the leading
  //          n by n upper triangular part of A contains the upper
  //          triangular part of the matrix A, and the strictly lower
  //          triangular part of A is not referenced.  If UPLO = 'L', the
  //          leading n by n lower triangular part of A contains the lower
  //          triangular part of the matrix A, and the strictly upper
  //          triangular part of A is not referenced.
  
  //          On exit, if INFO = 0, the factor U or L from the Cholesky
  //          factorization A = U'*U  or A = L*L'.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: if INFO = k, the leading minor of order k is not
  //               positive definite, and the factorization could not be
  //               completed.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( lda < max( 1, n ) ) { 
    info = -4;
  }
  if( info != 0 ) { 
    xerbla( "SPOTF2", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  if( upper ) { 
    
    //        Compute the Cholesky factorization A = U'*U.
    
    for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
      
      //           Compute U(J,J) and test for non-positive-definiteness.
      
      ajj = A(j_,j_) - sdot( j - 1, &A(j_,0), 1, &A(j_,0), 1 );
      if( ajj <= ZERO ) { 
        A(j_,j_) = ajj;
        goto L_30;
      }
      ajj = sqrt( ajj );
      A(j_,j_) = ajj;
      
      //           Compute elements J+1:N of row J.
      
      if( j < n ) { 
        sgemv( 'T'/*Transpose*/, j - 1, n - j, -ONE, &A(j_ + 1,0), 
         lda, &A(j_,0), 1, ONE, &A(j_ + 1,j_), lda );
        sscal( n - j, ONE/ajj, &A(j_ + 1,j_), lda );
      }
    }
  }
  else { 
    
    //        Compute the Cholesky factorization A = L*L'.
    
    for( j = 1, j_ = j - 1, _do1 = n; j <= _do1; j++, j_++ ) { 
      
      //           Compute L(J,J) and test for non-positive-definiteness.
      
      ajj = A(j_,j_) - sdot( j - 1, &A(0,j_), lda, &A(0,j_), 
       lda );
      if( ajj <= ZERO ) { 
        A(j_,j_) = ajj;
        goto L_30;
      }
      ajj = sqrt( ajj );
      A(j_,j_) = ajj;
      
      //           Compute elements J+1:N of column J.
      
      if( j < n ) { 
        sgemv( 'N'/*No transpose*/, n - j, j - 1, -ONE, &A(0,j_ + 1), 
         lda, &A(0,j_), lda, ONE, &A(j_,j_ + 1), 1 );
        sscal( n - j, ONE/ajj, &A(j_,j_ + 1), 1 );
      }
    }
  }
  goto L_40;
  
L_30:
  ;
  info = j;
  
L_40:
  ;
  return;
  
  //     End of SPOTF2
  
#undef  A
} // end of function 


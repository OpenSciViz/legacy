/*
 * C++ implementation of lapack routine dtrti2
 *
 * $Id: dtrti2.cpp,v 1.6 1993/04/06 20:42:52 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:39:14
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dtrti2.cpp,v $
 * Revision 1.6  1993/04/06  20:42:52  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.5  1993/03/19  18:41:23  alv
 * now passes chars explicitly, rather than indirection of a string, to shut up SUN warnings
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:17:56  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:09:31  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dtrti2(const char &uplo, const char &diag, const long &n, double *a, 
 const long &lda, long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  int nounit, upper;
  long _do0, j, j_;
  double ajj;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DTRTI2 computes the inverse of a real upper or lower triangular
  //  matrix.
  
  //  This is the Level 2 BLAS version of the algorithm.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the matrix A is upper or lower triangular.
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  DIAG    (input) CHARACTER*1
  //          Specifies whether or not the matrix A is unit triangular.
  //          = 'N':  Non-unit triangular
  //          = 'U':  Unit triangular
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)
  //          On entry, the triangular matrix A.  If UPLO = 'U', the
  //          leading n by n upper triangular part of the array A contains
  //          the upper triangular matrix, and the strictly lower
  //          triangular part of A is not referenced.  If UPLO = 'L', the
  //          leading n by n lower triangular part of the array A contains
  //          the lower triangular matrix, and the strictly upper
  //          triangular part of A is not referenced.  If DIAG = 'U', the
  //          diagonal elements of A are also not referenced and are
  //          assumed to be 1.
  
  //          On exit, the (triangular) inverse of the original matrix, in
  //          the same storage format.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  nounit = lsame( diag, 'N' );
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( !nounit && !lsame( diag, 'U' ) ) { 
    info = -2;
  }
  else if( n < 0 ) { 
    info = -3;
  }
  else if( lda < max( 1, n ) ) { 
    info = -5;
  }
  if( info != 0 ) { 
    xerbla( "DTRTI2", -info );
    return;
  }
  
  if( upper ) { 
    
    //        Compute inverse of upper triangular matrix.
    
    for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
      if( nounit ) { 
        A(j_,j_) = ONE/A(j_,j_);
        ajj = -A(j_,j_);
      }
      else { 
        ajj = -ONE;
      }
      
      //           Compute elements 1:j-1 of j-th column.
      
      dtrmv( 'U'/* Upper */, 'N'/* No transpose */, diag, j - 
       1, a, lda, &A(j_,0), 1 );
      dscal( j - 1, ajj, &A(j_,0), 1 );
    }
  }
  else { 
    
    //        Compute inverse of lower triangular matrix.
    
    for( j = n, j_ = j - 1; j >= 1; j--, j_-- ) { 
      if( nounit ) { 
        A(j_,j_) = ONE/A(j_,j_);
        ajj = -A(j_,j_);
      }
      else { 
        ajj = -ONE;
      }
      if( j < n ) { 
        
        //              Compute elements j+1:n of j-th column.
        
        dtrmv( 'L'/* Lower */, 'N'/* No transpose */, diag, 
         n - j, &A(j_ + 1,j_ + 1), lda, &A(j_,j_ + 1), 1 );
        dscal( n - j, ajj, &A(j_,j_ + 1), 1 );
      }
    }
  }
  
  return;
  
  //     End of DTRTI2
  
#undef  A
} // end of function 


/*
 * C++ implementation of lapack routine icmax1
 *
 * $Id: icmax1.cpp,v 1.4 1993/04/06 20:42:55 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:27:36
 * FOR_C++ Options SET: alloc do=rt no=p pf=xlapack s=dv str=l - prototypes
 *
 * $Log: icmax1.cpp,v $
 * Revision 1.4  1993/04/06  20:42:55  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.3  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.2  1993/03/05  23:18:01  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:09:37  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

inline float icmax1_cabs1(FComplex zdum) { return abs( real( (zdum) ) ); }
RWLAPKDECL long /*FUNCTION*/ icmax1(const long &n, FComplex cx[], const long &incx)
{
  long _do0, _do1, i, i_, icmax1_v, ix;
  float smax;
  FComplex zdum;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ICMAX1 finds the index of the element whose real part has maximum
  //  absolute value.
  
  //  Based on ICAMAX from Level 1 BLAS.
  //  The change is to use the 'genuine' absolute value.
  
  //  Contributed by Nick Higham for use with CLACON.
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The number of elements in the vector CX.
  
  //  CX      (input) COMPLEX array, dimension (N)
  //          The vector whose elements will be summed.
  
  //  INCX    (input) INTEGER
  //          The spacing between successive values of CX.  INCX >= 1.
  
  //     .. Local Scalars ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Statement Functions ..
  //     ..
  //     .. Statement Function definitions ..
  
  //     NEXT LINE IS THE ONLY MODIFICATION.
  //     ..
  //     .. Executable Statements ..
  
  icmax1_v = 0;
  if( n < 1 ) 
    return( icmax1_v );
  icmax1_v = 1;
  if( n == 1 ) 
    return( icmax1_v );
  if( incx == 1 ) 
    goto L_30;
  
  //     CODE FOR INCREMENT NOT EQUAL TO 1
  
  ix = 1;
  smax = icmax1_cabs1( cx[0] );
  ix = ix + incx;
  for( i = 2, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
    if( icmax1_cabs1( cx[ix - 1] ) <= smax ) 
      goto L_10;
    icmax1_v = i;
    smax = icmax1_cabs1( cx[ix - 1] );
L_10:
    ;
    ix = ix + incx;
  }
  return( icmax1_v );
  
  //     CODE FOR INCREMENT EQUAL TO 1
  
L_30:
  ;
  smax = icmax1_cabs1( cx[0] );
  for( i = 2, i_ = i - 1, _do1 = n; i <= _do1; i++, i_++ ) { 
    if( icmax1_cabs1( cx[i_] ) <= smax ) 
      goto L_40;
    icmax1_v = i;
    smax = icmax1_cabs1( cx[i_] );
L_40:
    ;
  }
  return( icmax1_v );
  
  //     End of ICMAX1
  
} // end of function 


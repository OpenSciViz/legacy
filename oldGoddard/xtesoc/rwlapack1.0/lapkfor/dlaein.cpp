/*
 * C++ implementation of lapack routine dlaein
 *
 * $Id: dlaein.cpp,v 1.7 1993/07/21 19:50:49 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:35:04
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dlaein.cpp,v $
 * Revision 1.7  1993/07/21  19:50:49  alv
 * ported to Microsoft visual C++
 *
 * Revision 1.6  1993/04/06  20:40:55  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.5  1993/03/19  18:41:23  alv
 * now passes chars explicitly, rather than indirection of a string, to shut up SUN warnings
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:15:11  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:07:08  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
const double TENTH = 1.0e-1;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dlaein(const int &rightv, const int &noinit, const long &n, 
 double *h, const long &ldh, const double &wr, const double &wi, double vr[], double vi[], 
 double *b, const long &ldb, double work[], const double &eps3, const double &smlnum, 
 const double &bignum, long &info)
{
#define H(I_,J_)  (*(h+(I_)*(ldh)+(J_)))
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
  char normin, trans;
  long _do0, _do1, _do10, _do11, _do12, _do13, _do14, _do15, 
   _do16, _do17, _do18, _do19, _do2, _do20, _do21, _do22, _do23, 
   _do24, _do25, _do3, _do4, _do5, _do6, _do7, _do8, _do9, i, i1, 
   i2, i3, i_, ierr, its, its_, j, j_;
  double absbii, absbjj, ei, ej, growto, norm, nrmsml, rec, rootn, 
   scale, temp, vcrit, vmax, vnorm, w, w1, x, xi, xr, y;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DLAEIN uses inverse iteration to find a right or left eigenvector
  //  corresponding to the eigenvalue (WR,WI) of a real upper Hessenberg
  //  matrix H.
  
  //  Arguments
  //  =========
  
  //  RIGHTV   (input) LOGICAL
  //          = .TRUE. : compute right eigenvector;
  //          = .FALSE.: compute left eigenvector.
  
  //  NOINIT   (input) LOGICAL
  //          = .TRUE. : no initial vector supplied in (VR,VI).
  //          = .FALSE.: initial vector supplied in (VR,VI).
  
  //  N       (input) INTEGER
  //          The order of the matrix H.  N >= 0.
  
  //  H       (input) DOUBLE PRECISION array, dimension (LDH,N)
  //          The upper Hessenberg matrix H.
  
  //  LDH     (input) INTEGER
  //          The leading dimension of the array H.  LDH >= max(1,N).
  
  //  WR      (input) DOUBLE PRECISION
  //  WI      (input) DOUBLE PRECISION
  //          The real and imaginary parts of the eigenvalue of H whose
  //          corresponding right or left eigenvector is to be computed.
  
  //  VR      (input/output) DOUBLE PRECISION array, dimension (N)
  //  VI      (input/output) DOUBLE PRECISION array, dimension (N)
  //          On entry, if NOINIT = .FALSE. and WI = 0.0, VR must contain
  //          a real starting vector for inverse iteration using the real
  //          eigenvalue WR; if NOINIT = .FALSE. and WI.ne.0.0, VR and VI
  //          must contain the real and imaginary parts of a DComplex
  //          starting vector for inverse iteration using the DComplex
  //          eigenvalue (WR,WI); otherwise VR and VI need not be set.
  //          On exit, if WI = 0.0 (real eigenvalue), VR contains the
  //          computed real eigenvector; if WI.ne.0.0 (DComplex eigenvalue),
  //          VR and VI contain the real and imaginary parts of the
  //          computed DComplex eigenvector. The eigenvector is normalized
  //          so that the component of largest magnitude has magnitude 1;
  //          here the magnitude of a DComplex number (x,y) is taken to be
  //          |x| + |y|.
  //          VI is not referenced if WI = 0.0.
  
  //  B       (workspace) DOUBLE PRECISION array, dimension (LDB,N)
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= N+1.
  
  //  WORK   (workspace) DOUBLE PRECISION array, dimension (N)
  
  //  EPS3    (input) DOUBLE PRECISION
  //          A small machine-dependent value which is used to perturb
  //          close eigenvalues, and to replace zero pivots.
  
  //  SMLNUM  (input) DOUBLE PRECISION
  //          A machine-dependent value close to the underflow threshold.
  
  //  BIGNUM  (input) DOUBLE PRECISION
  //          A machine-dependent value close to the overflow threshold.
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          = 1:  inverse iteration did not converge; VR is set to the
  //                last iterate, and so is VI if WI.ne.0.0.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  info = 0;
  
  //     GROWTO is the threshold used in the acceptance test for an
  //     eigenvector.
  
  rootn = sqrt( (double)( n ) );
  growto = TENTH/rootn;
  nrmsml = max( ONE, eps3*rootn )*smlnum;
  
  //     Form B = H - (WR,WI)*I (except that the subdiagonal elements and
  //     the imaginary parts of the diagonal elements are not stored).
  
  for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
    for( i = 1, i_ = i - 1, _do1 = j - 1; i <= _do1; i++, i_++ ) { 
      B(j_,i_) = H(j_,i_);
    }
    B(j_,j_) = H(j_,j_) - wr;
  }
  
  if( wi == ZERO ) { 
    
    //        Real eigenvalue.
    
    if( noinit ) { 
      
      //           Set initial vector.
      
      for( i = 1, i_ = i - 1, _do2 = n; i <= _do2; i++, i_++ ) { 
        vr[i_] = eps3;
      }
    }
    else { 
      
      //           Scale supplied initial vector.
      
      vnorm = dnrm2( n, vr, 1 );
      dscal( n, (eps3*rootn)/max( vnorm, nrmsml ), vr, 1 );
    }
    
    if( rightv ) { 
      
      //           LU decomposition with partial pivoting of B, replacing zero
      //           pivots by EPS3.
      
      for( i = 1, i_ = i - 1, _do3 = n - 1; i <= _do3; i++, i_++ ) { 
        ei = H(i_,i_ + 1);
        if( abs( B(i_,i_) ) < abs( ei ) ) { 
          
          //                 Interchange rows and eliminate.
          
          x = B(i_,i_)/ei;
          B(i_,i_) = ei;
          for( j = i + 1, j_ = j - 1, _do4 = n; j <= _do4; j++, j_++ ) { 
            temp = B(j_,i_ + 1);
            B(j_,i_ + 1) = B(j_,i_) - x*temp;
            B(j_,i_) = temp;
          }
        }
        else { 
          
          //                 Eliminate without interchange.
          
          if( B(i_,i_) == ZERO ) 
            B(i_,i_) = eps3;
          x = ei/B(i_,i_);
          if( x != ZERO ) { 
            for( j = i + 1, j_ = j - 1, _do5 = n; j <= _do5; j++, j_++ ) { 
              B(j_,i_ + 1) = B(j_,i_ + 1) - x*B(j_,i_);
            }
          }
        }
      }
      if( B(n - 1,n - 1) == ZERO ) 
        B(n - 1,n - 1) = eps3;
      
      trans = 'N';
      
    }
    else { 
      
      //           UL decomposition with partial pivoting of B, replacing zero
      //           pivots by EPS3.
      
      for( j = n, j_ = j - 1; j >= 2; j--, j_-- ) { 
        ej = H(j_ - 1,j_);
        if( abs( B(j_,j_) ) < abs( ej ) ) { 
          
          //                 Interchange columns and eliminate.
          
          x = B(j_,j_)/ej;
          B(j_,j_) = ej;
          for( i = 1, i_ = i - 1, _do6 = j - 1; i <= _do6; i++, i_++ ) { 
            temp = B(j_ - 1,i_);
            B(j_ - 1,i_) = B(j_,i_) - x*temp;
            B(j_,i_) = temp;
          }
        }
        else { 
          
          //                 Eliminate without interchange.
          
          if( B(j_,j_) == ZERO ) 
            B(j_,j_) = eps3;
          x = ej/B(j_,j_);
          if( x != ZERO ) { 
            for( i = 1, i_ = i - 1, _do7 = j - 1; i <= _do7; i++, i_++ ) { 
              B(j_ - 1,i_) = B(j_ - 1,i_) - x*B(j_,i_);
            }
          }
        }
      }
      if( B(0,0) == ZERO ) 
        B(0,0) = eps3;
      
      trans = 'T';
      
    }
    
    normin = 'N';
    for( its = 1, its_ = its - 1, _do8 = n; its <= _do8; its++, its_++ ) { 
      
      //           Solve U*x = scale*v for a right eigenvector
      //             or U'*x = scale*v for a left eigenvector,
      //           overwriting x on v.
      
      dlatrs( 'U'/* Upper */, trans, 'N'/* Nonunit */, normin, 
       n, b, ldb, vr, scale, work, ierr );
      normin = 'Y';
      
      //           Test for sufficient growth in the norm of v.
      
      vnorm = dasum( n, vr, 1 );
      if( vnorm >= growto*scale ) 
        goto L_120;
      
      //           Choose new orthogonal starting vector and try again.
      
      temp = eps3/(rootn + ONE);
      vr[0] = eps3;
      for( i = 2, i_ = i - 1, _do9 = n; i <= _do9; i++, i_++ ) { 
        vr[i_] = temp;
      }
      vr[n - its] = vr[n - its] - eps3*rootn;
    }
    
    //        Failure to find eigenvector in N iterations.
    
    info = 1;
    
L_120:
    ;
    
    //        Normalize eigenvector.
    
    i = idamax( n, vr, 1 );
    dscal( n, ONE/abs( vr[i - 1] ), vr, 1 );
  }
  else { 
    
    //        Complex eigenvalue.
    
    if( noinit ) { 
      
      //           Set initial vector.
      
      for( i = 1, i_ = i - 1, _do10 = n; i <= _do10; i++, i_++ ) { 
        vr[i_] = eps3;
        vi[i_] = ZERO;
      }
    }
    else { 
      
      //           Scale supplied initial vector.
      
      norm = dlapy2( dnrm2( n, vr, 1 ), dnrm2( n, vi, 1 ) );
      rec = (eps3*rootn)/max( norm, nrmsml );
      dscal( n, rec, vr, 1 );
      dscal( n, rec, vi, 1 );
    }
    
    if( rightv ) { 
      
      //           LU decomposition with partial pivoting of B, replacing zero
      //           pivots by EPS3.
      
      //           The imaginary part of the (i,j)-th element of U is stored in
      //           B(j+1,i).
      
      B(0,1) = -wi;
      for( i = 2, i_ = i - 1, _do11 = n; i <= _do11; i++, i_++ ) { 
        B(0,i_ + 1) = ZERO;
      }
      
      for( i = 1, i_ = i - 1, _do12 = n - 1; i <= _do12; i++, i_++ ) { 
        absbii = dlapy2( B(i_,i_), B(i_,i_ + 1) );
        ei = H(i_,i_ + 1);
        if( absbii < abs( ei ) ) { 
          
          //                 Interchange rows and eliminate.
          
          xr = B(i_,i_)/ei;
          xi = B(i_,i_ + 1)/ei;
          B(i_,i_) = ei;
          B(i_,i_ + 1) = ZERO;
          for( j = i + 1, j_ = j - 1, _do13 = n; j <= _do13; j++, j_++ ) { 
            temp = B(j_,i_ + 1);
            B(j_,i_ + 1) = B(j_,i_) - xr*temp;
            B(i_ + 1,j_ + 1) = B(i_,j_ + 1) - xi*temp;
            B(j_,i_) = temp;
            B(i_,j_ + 1) = ZERO;
          }
          B(i_,i_ + 2) = -wi;
          B(i_ + 1,i_ + 1) = B(i_ + 1,i_ + 1) - xi*wi;
          B(i_ + 1,i_ + 2) = B(i_ + 1,i_ + 2) + xr*wi;
        }
        else { 
          
          //                 Eliminate without interchanging rows.
          
          if( absbii == ZERO ) { 
            B(i_,i_) = eps3;
            B(i_,i_ + 1) = ZERO;
            absbii = eps3;
          }
          ei = (ei/absbii)/absbii;
          xr = B(i_,i_)*ei;
          xi = -B(i_,i_ + 1)*ei;
          for( j = i + 1, j_ = j - 1, _do14 = n; j <= _do14; j++, j_++ ) { 
            B(j_,i_ + 1) = B(j_,i_ + 1) - xr*B(j_,i_) + 
             xi*B(i_,j_ + 1);
	    // introduction of temp in next line is to avoid having Microsoft
	    // visual C++ crash
            double temp = -xr*B(i_,j_ + 1) - xi*B(j_,i_);
            B(i_ + 1,j_ + 1) = temp;
          }
          B(i_ + 1,i_ + 2) = B(i_ + 1,i_ + 2) - wi;
        }
        
        //              Compute 1-norm of offdiagonal elements of i-th row.
        
        work[i_] = dasum( n - i, &B(i_ + 1,i_), ldb ) + dasum( n - 
         i, &B(i_,i_ + 2), 1 );
      }
      if( B(n - 1,n - 1) == ZERO && B(n - 1,n) == ZERO ) 
        B(n - 1,n - 1) = eps3;
      work[n - 1] = ZERO;
      
      i1 = n;
      i2 = 1;
      i3 = -1;
    }
    else { 
      
      //           UL decomposition with partial pivoting of conjg(B),
      //           replacing zero pivots by EPS3.
      
      //           The imaginary part of the (i,j)-th element of U is stored in
      //           B(j+1,i).
      
      B(n - 1,n) = wi;
      for( j = 1, j_ = j - 1, _do15 = n - 1; j <= _do15; j++, j_++ ) { 
        B(j_,n) = ZERO;
      }
      
      for( j = n, j_ = j - 1; j >= 2; j--, j_-- ) { 
        ej = H(j_ - 1,j_);
        absbjj = dlapy2( B(j_,j_), B(j_,j_ + 1) );
        if( absbjj < abs( ej ) ) { 
          
          //                 Interchange columns and eliminate
          
          xr = B(j_,j_)/ej;
          xi = B(j_,j_ + 1)/ej;
          B(j_,j_) = ej;
          B(j_,j_ + 1) = ZERO;
          for( i = 1, i_ = i - 1, _do16 = j - 1; i <= _do16; i++, i_++ ) { 
            temp = B(j_ - 1,i_);
            B(j_ - 1,i_) = B(j_,i_) - xr*temp;
            B(i_,j_) = B(i_,j_ + 1) - xi*temp;
            B(j_,i_) = temp;
            B(i_,j_ + 1) = ZERO;
          }
          B(j_ - 1,j_ + 1) = wi;
          B(j_ - 1,j_ - 1) = B(j_ - 1,j_ - 1) + xi*wi;
          B(j_ - 1,j_) = B(j_ - 1,j_) - xr*wi;
        }
        else { 
          
          //                 Eliminate without interchange.
          
          if( absbjj == ZERO ) { 
            B(j_,j_) = eps3;
            B(j_,j_ + 1) = ZERO;
            absbjj = eps3;
          }
          ej = (ej/absbjj)/absbjj;
          xr = B(j_,j_)*ej;
          xi = -B(j_,j_ + 1)*ej;
          for( i = 1, i_ = i - 1, _do17 = j - 1; i <= _do17; i++, i_++ ) { 
            B(j_ - 1,i_) = B(j_ - 1,i_) - xr*B(j_,i_) + 
             xi*B(i_,j_ + 1);
            B(i_,j_) = -xr*B(i_,j_ + 1) - xi*B(j_,i_);
          }
          B(j_ - 1,j_) = B(j_ - 1,j_) + wi;
        }
        
        //              Compute 1-norm of offdiagonal elements of j-th column.
        
        work[j_] = dasum( j - 1, &B(j_,0), 1 ) + dasum( j - 
         1, &B(0,j_ + 1), ldb );
      }
      if( B(0,0) == ZERO && B(0,1) == ZERO ) 
        B(0,0) = eps3;
      work[0] = ZERO;
      
      i1 = 1;
      i2 = n;
      i3 = 1;
    }
    
    for( its = 1, its_ = its - 1, _do18 = n; its <= _do18; its++, its_++ ) { 
      scale = ONE;
      vmax = ONE;
      vcrit = bignum;
      
      //           Solve U*(xr,xi) = scale*(vr,vi) for a right eigenvector,
      //             or U'*(xr,xi) = scale*(vr,vi) for a left eigenvector,
      //           overwriting (xr,xi) on (vr,vi).
      
      for( i = i1, i_ = i - 1, _do19=docnt(i,i2,_do20 = i3); _do19 > 0; i += _do20, i_ += _do20, _do19-- ) { 
        
        if( work[i_] > vcrit ) { 
          rec = ONE/vmax;
          dscal( n, rec, vr, 1 );
          dscal( n, rec, vi, 1 );
          scale = scale*rec;
          vmax = ONE;
          vcrit = bignum;
        }
        
        xr = vr[i_];
        xi = vi[i_];
        if( rightv ) { 
          for( j = i + 1, j_ = j - 1, _do21 = n; j <= _do21; j++, j_++ ) { 
            xr = xr - B(j_,i_)*vr[j_] + B(i_,j_ + 1)*vi[j_];
            xi = xi - B(j_,i_)*vi[j_] - B(i_,j_ + 1)*vr[j_];
          }
        }
        else { 
          for( j = 1, j_ = j - 1, _do22 = i - 1; j <= _do22; j++, j_++ ) { 
            xr = xr - B(i_,j_)*vr[j_] + B(j_,i_ + 1)*vi[j_];
            xi = xi - B(i_,j_)*vi[j_] - B(j_,i_ + 1)*vr[j_];
          }
        }
        
        w = abs( B(i_,i_) ) + abs( B(i_,i_ + 1) );
        if( w > smlnum ) { 
          if( w < ONE ) { 
            w1 = abs( xr ) + abs( xi );
            if( w1 > w*bignum ) { 
              rec = ONE/w1;
              dscal( n, rec, vr, 1 );
              dscal( n, rec, vi, 1 );
              xr = vr[i_];
              xi = vi[i_];
              scale = scale*rec;
              vmax = vmax*rec;
            }
          }
          
          //                 Divide by diagonal element of B.
          
          dladiv( xr, xi, B(i_,i_), B(i_,i_ + 1), vr[i_], 
           vi[i_] );
          vmax = max( abs( vr[i_] ) + abs( vi[i_] ), vmax );
          vcrit = bignum/vmax;
        }
        else { 
          for( j = 1, j_ = j - 1, _do23 = n; j <= _do23; j++, j_++ ) { 
            vr[j_] = ZERO;
            vi[j_] = ZERO;
          }
          vr[i_] = ONE;
          vi[i_] = ONE;
          scale = ZERO;
          vmax = ONE;
          vcrit = bignum;
        }
      }
      
      //           Test for sufficient growth in the norm of (VR,VI).
      
      vnorm = dasum( n, vr, 1 ) + dasum( n, vi, 1 );
      if( vnorm >= growto*scale ) 
        goto L_280;
      
      //           Choose a new orthogonal starting vector and try again.
      
      y = eps3/(rootn + ONE);
      vr[0] = eps3;
      vi[0] = ZERO;
      
      for( i = 2, i_ = i - 1, _do24 = n; i <= _do24; i++, i_++ ) { 
        vr[i_] = y;
        vi[i_] = ZERO;
      }
      vr[n - its] = vr[n - its] - eps3*rootn;
    }
    
    //        Failure to find eigenvector in N iterations
    
    info = 1;
    
L_280:
    ;
    
    //        Normalize eigenvector.
    
    vnorm = ZERO;
    for( i = 1, i_ = i - 1, _do25 = n; i <= _do25; i++, i_++ ) { 
      vnorm = max( vnorm, abs( vr[i_] ) + abs( vi[i_] ) );
    }
    dscal( n, ONE/vnorm, vr, 1 );
    dscal( n, ONE/vnorm, vi, 1 );
    
  }
  
  return;
  
  //     End of DLAEIN
  
#undef  H
#undef  B
} // end of function 


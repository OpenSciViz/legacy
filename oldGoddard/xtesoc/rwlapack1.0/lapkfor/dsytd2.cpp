/*
 * C++ implementation of lapack routine dsytd2
 *
 * $Id: dsytd2.cpp,v 1.5 1993/04/06 20:42:39 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:38:38
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dsytd2.cpp,v $
 * Revision 1.5  1993/04/06  20:42:39  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:17:30  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:09:16  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ONE = 1.0e0;
const double ZERO = 0.0e0;
const double HALF = 1.0e0/2.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dsytd2(const char &uplo, const long &n, double *a, const long &lda, 
 double d[], double e[], double tau[], long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  int upper;
  long _do0, i, i_;
  double alpha, taui;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DSYTD2 reduces a real symmetric matrix A to symmetric tridiagonal
  //  form T by an orthogonal similarity transformation: Q' * A * Q = T.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangular part of the
  //          symmetric matrix A is stored:
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)
  //          On entry, the symmetric matrix A.  If UPLO = 'U', the leading
  //          n-by-n upper triangular part of A contains the upper
  //          triangular part of the matrix A, and the strictly lower
  //          triangular part of A is not referenced.  If UPLO = 'L', the
  //          leading n-by-n lower triangular part of A contains the lower
  //          triangular part of the matrix A, and the strictly upper
  //          triangular part of A is not referenced.
  //          On exit, if UPLO = 'U', the diagonal and first superdiagonal
  //          of A are overwritten by the corresponding elements of the
  //          tridiagonal matrix T, and the elements above the first
  //          superdiagonal, with the array TAU, represent the orthogonal
  //          matrix Q as a product of elementary reflectors; if UPLO
  //          = 'L', the diagonal and first subdiagonal of A are over-
  //          written by the corresponding elements of the tridiagonal
  //          matrix T, and the elements below the first subdiagonal, with
  //          the array TAU, represent the orthogonal matrix Q as a product
  //          of elementary reflectors. See Further Details.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  D       (output) DOUBLE PRECISION array, dimension (N)
  //          The diagonal elements of the tridiagonal matrix T:
  //          D(i) = A(i,i).
  
  //  E       (output) DOUBLE PRECISION array, dimension (N-1)
  //          The off-diagonal elements of the tridiagonal matrix T:
  //          E(i) = A(i,i+1) if UPLO = 'U', E(i) = A(i+1,i) if UPLO = 'L'.
  
  //  TAU     (output) DOUBLE PRECISION array, dimension (N)
  //          The scalar factors of the elementary reflectors (see Further
  //          Details).
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0:  if INFO = -i, the i-th argument had an illegal value.
  
  //  Further Details
  //  ===============
  
  //  If UPLO = 'U', the matrix Q is represented as a product of elementary
  //  reflectors
  
  //     Q = H(n-1) . . . H(2) H(1).
  
  //  Each H(i) has the form
  
  //     H(i) = I - tau * v * v'
  
  //  where tau is a real scalar, and v is a real vector with
  //  v(i+1:n) = 0 and v(i) = 1; v(1:i-1) is stored on exit in
  //  A(1:i-1,i+1), and tau in TAU(i).
  
  //  If UPLO = 'L', the matrix Q is represented as a product of elementary
  //  reflectors
  
  //     Q = H(1) H(2) . . . H(n-1).
  
  //  Each H(i) has the form
  
  //     H(i) = I - tau * v * v'
  
  //  where tau is a real scalar, and v is a real vector with
  //  v(1:i) = 0 and v(i+1) = 1; v(i+2:n) is stored on exit in A(i+2:n,i),
  //  and tau in TAU(i).
  
  //  The contents of A on exit are illustrated by the following examples
  //  with n = 5:
  
  //  if UPLO = 'U':                       if UPLO = 'L':
  
  //    (  d   e   v2  v3  v4 )              (  d                  )
  //    (      d   e   v3  v4 )              (  e   d              )
  //    (          d   e   v4 )              (  v1  e   d          )
  //    (              d   e  )              (  v1  v2  e   d      )
  //    (                  d  )              (  v1  v2  v3  e   d  )
  
  //  where d and e denote diagonal and off-diagonal elements of T, and vi
  //  denotes an element of the vector defining H(i).
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters
  
  info = 0;
  upper = lsame( uplo, 'U' );
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( lda < max( 1, n ) ) { 
    info = -4;
  }
  if( info != 0 ) { 
    xerbla( "DSYTD2", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n <= 0 ) 
    return;
  
  if( upper ) { 
    
    //        Reduce the upper triangle of A
    
    for( i = n - 1, i_ = i - 1; i >= 1; i--, i_-- ) { 
      
      //           Generate elementary reflector H(i) = I - tau * v * v'
      //           to annihilate A(1:i-1,i+1)
      
      dlarfg( i, A(i_ + 1,i_), &A(i_ + 1,0), 1, taui );
      e[i_] = A(i_ + 1,i_);
      
      if( taui != ZERO ) { 
        
        //              Apply H(i) from both sides to A(1:i,1:i)
        
        A(i_ + 1,i_) = ONE;
        
        //              Compute  x := tau * A * v  storing x in TAU(1:i)
        
        dsymv( uplo, i, taui, a, lda, &A(i_ + 1,0), 1, ZERO, 
         tau, 1 );
        
        //              Compute  w := x - 1/2 * tau * (x'*v) * v
        
        alpha = -HALF*taui*ddot( i, tau, 1, &A(i_ + 1,0), 
         1 );
        daxpy( i, alpha, &A(i_ + 1,0), 1, tau, 1 );
        
        //              Apply the transformation as a rank-2 update:
        //                 A := A - v * w' - w * v'
        
        dsyr2( uplo, i, -ONE, &A(i_ + 1,0), 1, tau, 1, a, 
         lda );
        
        A(i_ + 1,i_) = e[i_];
      }
      d[i_ + 1] = A(i_ + 1,i_ + 1);
      tau[i_] = taui;
    }
    d[0] = A(0,0);
  }
  else { 
    
    //        Reduce the lower triangle of A
    
    for( i = 1, i_ = i - 1, _do0 = n - 1; i <= _do0; i++, i_++ ) { 
      
      //           Generate elementary reflector H(i) = I - tau * v * v'
      //           to annihilate A(i+2:n,i)
      
      dlarfg( n - i, A(i_,i_ + 1), &A(i_,min( i + 2, n ) - 1), 
       1, taui );
      e[i_] = A(i_,i_ + 1);
      
      if( taui != ZERO ) { 
        
        //              Apply H(i) from both sides to A(i+1:n,i+1:n)
        
        A(i_,i_ + 1) = ONE;
        
        //              Compute  x := tau * A * v  storing y in TAU(i+1:n)
        
        dsymv( uplo, n - i, taui, &A(i_ + 1,i_ + 1), lda, 
         &A(i_,i_ + 1), 1, ZERO, &tau[i_ + 1], 1 );
        
        //              Compute  w := x - 1/2 * tau * (x'*v) * v
        
        alpha = -HALF*taui*ddot( n - i, &tau[i_ + 1], 1, &A(i_,i_ + 1), 
         1 );
        daxpy( n - i, alpha, &A(i_,i_ + 1), 1, &tau[i_ + 1], 
         1 );
        
        //              Apply the transformation as a rank-2 update:
        //                 A := A - v * w' - w * v'
        
        dsyr2( uplo, n - i, -ONE, &A(i_,i_ + 1), 1, &tau[i_ + 1], 
         1, &A(i_ + 1,i_ + 1), lda );
        
        A(i_,i_ + 1) = e[i_];
      }
      d[i_] = A(i_,i_);
      tau[i_] = taui;
    }
    d[n - 1] = A(n - 1,n - 1);
  }
  
  return;
  
  //     End of DSYTD2
  
#undef  A
} // end of function 


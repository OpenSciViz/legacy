/*
 * C++ implementation of Lapack routine zgbrfs
 *
 * $Id: zgbrfs.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:45:52
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zgbrfs.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const long ITMAX = 5;
const double ZERO = 0.0e0;
const DComplex ONE = DComplex(1.0e0);
const double TWO = 2.0e0;
const double THREE = 3.0e0;
// end of PARAMETER translations

inline double zgbrfs_cabs1(DComplex zdum) { return abs( real( (zdum) ) ) + 
   abs( imag( (zdum) ) ); }
RWLAPKDECL void /*FUNCTION*/ zgbrfs(const char &trans, const long &n, const long &kl, const long &ku, 
 const long &nrhs, DComplex *ab, const long &ldab, DComplex *afb, const long &ldafb, 
 long ipiv[], DComplex *b, const long &ldb, DComplex *x, const long &ldx, 
 double ferr[], double berr[], DComplex work[], double rwork[], long &info)
{
#define AB(I_,J_) (*(ab+(I_)*(ldab)+(J_)))
#define AFB(I_,J_)  (*(afb+(I_)*(ldafb)+(J_)))
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
#define X(I_,J_)  (*(x+(I_)*(ldx)+(J_)))
  int notran;
  char transn, transt;
  long _do0, _do1, _do10, _do11, _do2, _do3, _do4, _do5, _do6, 
   _do7, _do8, _do9, count, i, i_, j, j_, k, k_, kase, kk, nz;
  double eps, lstres, s, safe1, safe2, safmin, xk;
  DComplex zdum;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZGBRFS improves the computed solution to a system of linear
  //  equations when the coefficient matrix is banded and provides error
  //  bounds and backward error estimates for the solutions.
  
  //  Arguments
  //  =========
  
  //  TRANS   (input) CHARACTER*1
  //          Specifies the form of the system of equations.
  //          = 'N':  A * X = B     (No transpose)
  //          = 'T':  A**T * X = B  (Transpose)
  //          = 'C':  A**H * X = B  (Conjugate transpose)
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  KL      (input) INTEGER
  //          The number of subdiagonals within the band of A.  KL >= 0.
  
  //  KU      (input) INTEGER
  //          The number of superdiagonals within the band of A.  KU >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right hand sides, i.e., the number of columns
  //          of the matrices B and X.  NRHS >= 0.
  
  //  AB      (input) COMPLEX*16 array, dimension (LDAB,N)
  //          The original band matrix A, stored in rows 1 to KL+KU+1.
  //          The j-th column of A is stored in the j-th column of the
  //          array AB as follows:
  //          AB(ku+1+i-j,j) = A(i,j) for max(1,j-ku)<=i<=min(n,j+kl).
  
  //  LDAB    (input) INTEGER
  //          The leading dimension of the array AB.  LDAB >= KL+KU+1.
  
  //  AFB     (input) COMPLEX*16 array, dimension (LDAFB,N)
  //          Details of the LU factorization of the band matrix A, as
  //          computed by ZGBTRF.  U is stored as an upper triangular band
  //          matrix with KL+KU superdiagonals in rows 1 to KL+KU+1, and
  //          the multipliers used during the factorization are stored in
  //          rows KL+KU+2 to 2*KL+KU+1.
  
  //  LDAFB   (input) INTEGER
  //          The leading dimension of the array AFB.  LDAFB >= 2*KL*KU+1.
  
  //  IPIV    (input) INTEGER array, dimension (N)
  //          The pivot indices from ZGBTRF; for 1<=i<=N, row i of the
  //          matrix was interchanged with row IPIV(i).
  
  //  B       (input) COMPLEX*16 array, dimension (LDB,NRHS)
  //          The right hand side vectors for the system of linear
  //          equations.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  X       (input/output) COMPLEX*16 array, dimension (LDX,NRHS)
  //          On entry, the solution vectors.
  //          On exit, the improved solution vectors.
  
  //  LDX     (input) INTEGER
  //          The leading dimension of the array X.  LDX >= max(1,N).
  
  //  FERR    (output) DOUBLE PRECISION array, dimension (NRHS)
  //          The estimated forward error bounds for each solution vector
  //          X.  If XTRUE is the true solution, FERR bounds the magnitude
  //          of the largest entry in (X - XTRUE) divided by the magnitude
  //          of the largest entry in X.  The quality of the error bound
  //          depends on the quality of the estimate of norm(inv(A))
  //          computed in the code; if the estimate of norm(inv(A)) is
  //          accurate, the error bound is guaranteed.
  
  //  BERR    (output) DOUBLE PRECISION array, dimension (NRHS)
  //          The componentwise relative backward error of each solution
  //          vector (i.e., the smallest relative change in any entry of A
  //          or B that makes X an exact solution).
  
  //  WORK    (workspace) COMPLEX*16 array, dimension (2*N)
  
  //  RWORK   (workspace) DOUBLE PRECISION array, dimension (N)
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  
  //  Internal Parameters
  //  ===================
  
  //  ITMAX is the maximum number of steps of iterative refinement.
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Statement Functions ..
  //     ..
  //     .. Statement Function definitions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  notran = lsame( trans, 'N' );
  if( (!notran && !lsame( trans, 'T' )) && !lsame( trans, 'C' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( kl < 0 ) { 
    info = -3;
  }
  else if( ku < 0 ) { 
    info = -4;
  }
  else if( nrhs < 0 ) { 
    info = -5;
  }
  else if( ldab < kl + ku + 1 ) { 
    info = -7;
  }
  else if( ldafb < 2*kl + ku + 1 ) { 
    info = -9;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -12;
  }
  else if( ldx < max( 1, n ) ) { 
    info = -14;
  }
  if( info != 0 ) { 
    xerbla( "ZGBRFS", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 || nrhs == 0 ) { 
    for( j = 1, j_ = j - 1, _do0 = nrhs; j <= _do0; j++, j_++ ) { 
      ferr[j_] = ZERO;
      berr[j_] = ZERO;
    }
    return;
  }
  
  if( notran ) { 
    transn = 'N';
    transt = 'C';
  }
  else { 
    transn = 'C';
    transt = 'N';
  }
  
  //     NZ = maximum number of nonzero entries in each row of A, plus 1
  
  nz = min( kl + ku + 2, n + 1 );
  eps = dlamch( 'E'/*Epsilon*/ );
  safmin = dlamch( 'S'/*Safe minimum*/ );
  safe1 = nz*safmin;
  safe2 = safe1/eps;
  
  //     Do for each right hand side
  
  for( j = 1, j_ = j - 1, _do1 = nrhs; j <= _do1; j++, j_++ ) { 
    
    count = 1;
    lstres = THREE;
L_20:
    ;
    
    //        Loop until stopping criterion is satisfied.
    
    //        Compute residual R = B - op(A) * X,
    //        where op(A) = A, A**T, or A**H, depending on TRANS.
    
    zcopy( n, &B(j_,0), 1, work, 1 );
    zgbmv( trans, n, n, kl, ku, -(ONE), ab, ldab, &X(j_,0), 1, 
     ONE, work, 1 );
    
    //        Compute componentwise relative backward error from formula
    
    //        max(i) ( abs(R(i)) / ( abs(op(A))*abs(X) + abs(B) )(i) )
    
    //        where abs(Z) is the componentwise absolute value of the matrix
    //        or vector Z.  If the i-th component of the denominator is less
    //        than SAFE2, then SAFE1 is added to the i-th components of the
    //        numerator and denominator before dividing.
    
    for( i = 1, i_ = i - 1, _do2 = n; i <= _do2; i++, i_++ ) { 
      rwork[i_] = zgbrfs_cabs1( B(j_,i_) );
    }
    
    //        Compute abs(op(A))*abs(X) + abs(B).
    
    if( notran ) { 
      for( k = 1, k_ = k - 1, _do3 = n; k <= _do3; k++, k_++ ) { 
        kk = ku + 1 - k;
        xk = zgbrfs_cabs1( X(j_,k_) );
        for( i = max( 1, k - ku ), i_ = i - 1, _do4 = min( n, 
         k + kl ); i <= _do4; i++, i_++ ) { 
          rwork[i_] = rwork[i_] + zgbrfs_cabs1( AB(k_,kk + i_) )*
           xk;
        }
      }
    }
    else { 
      for( k = 1, k_ = k - 1, _do5 = n; k <= _do5; k++, k_++ ) { 
        s = ZERO;
        kk = ku + 1 - k;
        for( i = max( 1, k - ku ), i_ = i - 1, _do6 = min( n, 
         k + kl ); i <= _do6; i++, i_++ ) { 
          s = s + zgbrfs_cabs1( AB(k_,kk + i_) )*zgbrfs_cabs1( X(j_,i_) );
        }
        rwork[k_] = rwork[k_] + s;
      }
    }
    s = ZERO;
    for( i = 1, i_ = i - 1, _do7 = n; i <= _do7; i++, i_++ ) { 
      if( rwork[i_] > safe2 ) { 
        s = max( s, zgbrfs_cabs1( work[i_] )/rwork[i_] );
      }
      else { 
        s = max( s, (zgbrfs_cabs1( work[i_] ) + safe1)/(rwork[i_] + 
         safe1) );
      }
    }
    berr[j_] = s;
    
    //        Test stopping criterion. Continue iterating if
    //           1) The residual BERR(J) is larger than machine epsilon, and
    //           2) BERR(J) decreased by at least a factor of 2 during the
    //              last iteration, and
    //           3) At most ITMAX iterations tried.
    
    if( (berr[j_] > eps && TWO*berr[j_] <= lstres) && count <= 
     ITMAX ) { 
      
      //           Update solution and try again.
      
      zgbtrs( trans, n, kl, ku, 1, afb, ldafb, ipiv, work, n, 
       info );
      zaxpy( n, ONE, work, 1, &X(j_,0), 1 );
      lstres = berr[j_];
      count = count + 1;
      goto L_20;
    }
    
    //        Bound error from formula
    
    //        norm(X - XTRUE) / norm(X) .le. FERR =
    //        norm( abs(inv(op(A)))*
    //           ( abs(R) + NZ*EPS*( abs(op(A))*abs(X)+abs(B) ))) / norm(X)
    
    //        where
    //          norm(Z) is the magnitude of the largest component of Z
    //          inv(op(A)) is the inverse of op(A)
    //          abs(Z) is the componentwise absolute value of the matrix or
    //             vector Z
    //          NZ is the maximum number of nonzeros in any row of A, plus 1
    //          EPS is machine epsilon
    
    //        The i-th component of abs(R)+NZ*EPS*(abs(op(A))*abs(X)+abs(B))
    //        is incremented by SAFE1 if the i-th component of
    //        abs(op(A))*abs(X) + abs(B) is less than SAFE2.
    
    //        Use ZLACON to estimate the infinity-norm of the matrix
    //           inv(op(A)) * diag(W),
    //        where W = abs(R) + NZ*EPS*( abs(op(A))*abs(X)+abs(B) )))
    
    for( i = 1, i_ = i - 1, _do8 = n; i <= _do8; i++, i_++ ) { 
      if( rwork[i_] > safe2 ) { 
        rwork[i_] = zgbrfs_cabs1( work[i_] ) + nz*eps*rwork[i_];
      }
      else { 
        rwork[i_] = zgbrfs_cabs1( work[i_] ) + nz*eps*rwork[i_] + 
         safe1;
      }
    }
    
    kase = 0;
L_100:
    ;
    zlacon( n, &work[n], work, ferr[j_], kase );
    if( kase != 0 ) { 
      if( kase == 1 ) { 
        
        //              Multiply by diag(W)*inv(op(A)**H).
        
        zgbtrs( transt, n, kl, ku, 1, afb, ldafb, ipiv, work, 
         n, info );
        for( i = 1, i_ = i - 1, _do9 = n; i <= _do9; i++, i_++ ) { 
          work[i_] = rwork[i_]*work[i_];
        }
      }
      else { 
        
        //              Multiply by inv(op(A))*diag(W).
        
        for( i = 1, i_ = i - 1, _do10 = n; i <= _do10; i++, i_++ ) { 
          work[i_] = rwork[i_]*work[i_];
        }
        zgbtrs( transn, n, kl, ku, 1, afb, ldafb, ipiv, work, 
         n, info );
      }
      goto L_100;
    }
    
    //        Normalize error.
    
    lstres = ZERO;
    for( i = 1, i_ = i - 1, _do11 = n; i <= _do11; i++, i_++ ) { 
      lstres = max( lstres, zgbrfs_cabs1( X(j_,i_) ) );
    }
    if( lstres != ZERO ) 
      ferr[j_] = ferr[j_]/lstres;
    
  }
  
  return;
  
  //     End of ZGBRFS
  
#undef  X
#undef  B
#undef  AFB
#undef  AB
} // end of function 


/*
 * C++ implementation of Lapack routine spbtrf
 *
 * $Id: spbtrf.cpp,v 1.3 1993/10/15 04:51:47 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:01:43
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: spbtrf.cpp,v $
 * Revision 1.3  1993/10/15  04:51:47  alv
 * NBMAX reduced for DOS to ease memory problems
 *
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ONE = 1.0e0;
const float ZERO = 0.0e0;
#ifdef __MSDOS__
const long NBMAX = 4;
#else
const long NBMAX = 32;
#endif
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ spbtrf(const char &uplo, const long &n, const long &kd, float *ab, 
 const long &ldab, long &info)
{
#define AB(I_,J_) (*(ab+(I_)*(ldab)+(J_)))
  // PARAMETER translations
  const long LDWORK = NBMAX + 1;
  // end of PARAMETER translations

  char _c0[2];
  long _do0, _do1, _do10, _do11, _do12, _do13, _do14, _do15, 
   _do2, _do3, _do4, _do5, _do6, _do7, _do8, _do9, i, i2, i3, i_, 
   ib, ii, ii_, j, j_, jj, jj_, nb;
  float work[NBMAX][LDWORK];

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SPBTRF computes the Cholesky factorization of a real symmetric
  //  positive definite band matrix A.
  
  //  The factorization has the form
  //     A = U' * U ,  if UPLO = 'U', or
  //     A = L  * L',  if UPLO = 'L',
  //  where U is an upper triangular matrix and L is lower triangular.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangular part of the
  //          symmetric matrix A is stored:
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  KD      (input) INTEGER
  //          The number of super-diagonals of the matrix A if UPLO = 'U',
  //          or the number of sub-diagonals if UPLO = 'L'.  KD >= 0.
  
  //  AB      (input/output) REAL array, dimension (LDAB,N)
  //          On entry, the upper or lower triangle of the symmetric band
  //          matrix A, stored in the first KD+1 rows of the array.  The
  //          j-th column of A is stored in the j-th column of the array AB
  //          as follows:
  //          if UPLO = 'U', AB(kd+1+i-j,j) = A(i,j) for max(1,j-kd)<=i<=j;
  //          if UPLO = 'L', AB(1+i-j,j)    = A(i,j) for j<=i<=min(n,j+kd).
  
  //          On exit, if INFO = 0, the triangular factor U or L from the
  //          Cholesky factorization A = U'*U or A = L*L' of the band
  //          matrix A, in the same storage format as A.
  
  //  LDAB    (input) INTEGER
  //          The leading dimension of the array AB.  LDAB >= KD+1.
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: if INFO = k, the leading minor of order k is not
  //               positive definite, and the factorization could not be
  //               completed.
  
  //  Further Details
  //  ===============
  
  //  The band storage scheme is illustrated by the following example, when
  //  N = 6, KD = 2, and UPLO = 'U':
  
  //  On entry:                       On exit:
  
  //      *    *   a13  a24  a35  a46      *    *   u13  u24  u35  u46
  //      *   a12  a23  a34  a45  a56      *   u12  u23  u34  u45  u56
  //     a11  a22  a33  a44  a55  a66     u11  u22  u33  u44  u55  u66
  
  //  Similarly, if UPLO = 'L' the format of A is as follows:
  
  //  On entry:                       On exit:
  
  //     a11  a22  a33  a44  a55  a66     l11  l22  l33  l44  l55  l66
  //     a21  a32  a43  a54  a65   *      l21  l32  l43  l54  l65   *
  //     a31  a42  a53  a64   *    *      l31  l42  l53  l64   *    *
  
  //  Array elements marked * are not used by the routine.
  
  //  Contributed by
  //  Peter Mayes and Giuseppe Radicati, IBM ECSEC, Rome, March 23, 1989
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Local Arrays ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  if( (!lsame( uplo, 'U' )) && (!lsame( uplo, 'L' )) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( kd < 0 ) { 
    info = -3;
  }
  else if( ldab < kd + 1 ) { 
    info = -5;
  }
  if( info != 0 ) { 
    xerbla( "SPBTRF", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  //     Determine the block size for this environment
  
  nb = ilaenv( 1, "SPBTRF", STR1(_c0,uplo), n, kd, -1, -1 );
  
  //     The block size must not exceed the semi-bandwidth KD, and must not
  //     exceed the limit set by the size of the local array WORK.
  
  nb = min( nb, NBMAX );
  
  if( nb <= 1 || nb > kd ) { 
    
    //        Use unblocked code
    
    spbtf2( uplo, n, kd, ab, ldab, info );
  }
  else { 
    
    //        Use blocked code
    
    if( lsame( uplo, 'U' ) ) { 
      
      //           Compute the Cholesky factorization of a symmetric band
      //           matrix, given the upper triangle of the matrix in band
      //           storage.
      
      //           Zero the upper triangle of the work array.
      
      for( j = 1, j_ = j - 1, _do0 = nb; j <= _do0; j++, j_++ ) { 
        for( i = 1, i_ = i - 1, _do1 = j - 1; i <= _do1; i++, i_++ ) { 
          work[j_][i_] = ZERO;
        }
      }
      
      //           Process the band matrix one diagonal block at a time.
      
      for( i = 1, i_ = i - 1, _do2=docnt(i,n,_do3 = nb); _do2 > 0; i += _do3, i_ += _do3, _do2-- ) { 
        ib = min( nb, n - i + 1 );
        
        //              Factorize the diagonal block
        
        spotf2( uplo, ib, &AB(i_,kd), ldab - 1, ii );
        if( ii != 0 ) { 
          info = i + ii - 1;
          goto L_150;
        }
        if( i + ib <= n ) { 
          
          //                 Update the relevant part of the trailing submatrix.
          //                 If A11 denotes the diagonal block which has just been
          //                 factorized, then we need to update the remaining
          //                 blocks in the diagram:
          
          //                    A11   A12   A13
          //                          A22   A23
          //                                A33
          
          //                 The numbers of rows and columns in the partitioning
          //                 are IB, I2, I3 respectively. The blocks A12, A22 and
          //                 A23 are empty if IB = KD. The upper triangle of A13
          //                 lies outside the band.
          
          i2 = min( kd - ib, n - i - ib + 1 );
          i3 = min( ib, n - i - kd + 1 );
          
          if( i2 > 0 ) { 
            
            //                    Update A12
            
            strsm( 'L'/*Left*/, 'U'/*Upper*/, 'T'/*Transpose*/
             , 'N'/*Non-unit*/, ib, i2, ONE, &AB(i_,kd), 
             ldab - 1, &AB(i_ + ib,kd - ib), ldab - 1 );
            
            //                    Update A22
            
            ssyrk( 'U'/*Upper*/, 'T'/*Transpose*/, i2, 
             ib, -ONE, &AB(i_ + ib,kd - ib), ldab - 1, 
             ONE, &AB(i_ + ib,kd), ldab - 1 );
          }
          
          if( i3 > 0 ) { 
            
            //                    Copy the lower triangle of A13 into the work array.
            
            for( jj = 1, jj_ = jj - 1, _do4 = i3; jj <= _do4; jj++, jj_++ ) { 
              for( ii = jj, ii_ = ii - 1, _do5 = ib; ii <= _do5; ii++, ii_++ ) { 
                work[jj_][ii_] = AB(jj_ + i + kd - 1,ii_ - jj + 1);
              }
            }
            
            //                    Update A13 (in the work array).
            
            strsm( 'L'/*Left*/, 'U'/*Upper*/, 'T'/*Transpose*/
             , 'N'/*Non-unit*/, ib, i3, ONE, &AB(i_,kd), 
             ldab - 1, (float*)work, LDWORK );
            
            //                    Update A23
            
            if( i2 > 0 ) 
              sgemm( 'T'/*Transpose*/, 'N'/*No Transpose*/
               , i2, i3, ib, -ONE, &AB(i_ + ib,kd - ib), 
               ldab - 1, (float*)work, LDWORK, ONE, 
               &AB(i_ + kd,ib), ldab - 1 );
            
            //                    Update A33
            
            ssyrk( 'U'/*Upper*/, 'T'/*Transpose*/, i3, 
             ib, -ONE, (float*)work, LDWORK, ONE, &AB(i_ + kd,kd), 
             ldab - 1 );
            
            //                    Copy the lower triangle of A13 back into place.
            
            for( jj = 1, jj_ = jj - 1, _do6 = i3; jj <= _do6; jj++, jj_++ ) { 
              for( ii = jj, ii_ = ii - 1, _do7 = ib; ii <= _do7; ii++, ii_++ ) { 
                AB(jj_ + i + kd - 1,ii_ - jj + 1) = work[jj_][ii_];
              }
            }
          }
        }
      }
    }
    else { 
      
      //           Compute the Cholesky factorization of a symmetric band
      //           matrix, given the lower triangle of the matrix in band
      //           storage.
      
      //           Zero the lower triangle of the work array.
      
      for( j = 1, j_ = j - 1, _do8 = nb; j <= _do8; j++, j_++ ) { 
        for( i = j + 1, i_ = i - 1, _do9 = nb; i <= _do9; i++, i_++ ) { 
          work[j_][i_] = ZERO;
        }
      }
      
      //           Process the band matrix one diagonal block at a time.
      
      for( i = 1, i_ = i - 1, _do10=docnt(i,n,_do11 = nb); _do10 > 0; i += _do11, i_ += _do11, _do10-- ) { 
        ib = min( nb, n - i + 1 );
        
        //              Factorize the diagonal block
        
        spotf2( uplo, ib, &AB(i_,0), ldab - 1, ii );
        if( ii != 0 ) { 
          info = i + ii - 1;
          goto L_150;
        }
        if( i + ib <= n ) { 
          
          //                 Update the relevant part of the trailing submatrix.
          //                 If A11 denotes the diagonal block which has just been
          //                 factorized, then we need to update the remaining
          //                 blocks in the diagram:
          
          //                    A11
          //                    A21   A22
          //                    A31   A32   A33
          
          //                 The numbers of rows and columns in the partitioning
          //                 are IB, I2, I3 respectively. The blocks A21, A22 and
          //                 A32 are empty if IB = KD. The lower triangle of A31
          //                 lies outside the band.
          
          i2 = min( kd - ib, n - i - ib + 1 );
          i3 = min( ib, n - i - kd + 1 );
          
          if( i2 > 0 ) { 
            
            //                    Update A21
            
            strsm( 'R'/*Right*/, 'L'/*Lower*/, 'T'/*Transpose*/
             , 'N'/*Non-unit*/, i2, ib, ONE, &AB(i_,0), 
             ldab - 1, &AB(i_,ib), ldab - 1 );
            
            //                    Update A22
            
            ssyrk( 'L'/*Lower*/, 'N'/*No Transpose*/
             , i2, ib, -ONE, &AB(i_,ib), ldab - 1, ONE, 
             &AB(i_ + ib,0), ldab - 1 );
          }
          
          if( i3 > 0 ) { 
            
            //                    Copy the upper triangle of A31 into the work array.
            
            for( jj = 1, jj_ = jj - 1, _do12 = ib; jj <= _do12; jj++, jj_++ ) { 
              for( ii = 1, ii_ = ii - 1, _do13 = min( jj, 
               i3 ); ii <= _do13; ii++, ii_++ ) { 
                work[jj_][ii_] = AB(jj_ + i - 1,kd + 1 - jj + ii_);
              }
            }
            
            //                    Update A31 (in the work array).
            
            strsm( 'R'/*Right*/, 'L'/*Lower*/, 'T'/*Transpose*/
             , 'N'/*Non-unit*/, i3, ib, ONE, &AB(i_,0), 
             ldab - 1, (float*)work, LDWORK );
            
            //                    Update A32
            
            if( i2 > 0 ) 
              sgemm( 'N'/*No transpose*/, 'T'/*Transpose*/
               , i3, i2, ib, -ONE, (float*)work, LDWORK, 
               &AB(i_,ib), ldab - 1, ONE, &AB(i_ + ib,kd - ib), 
               ldab - 1 );
            
            //                    Update A33
            
            ssyrk( 'L'/*Lower*/, 'N'/*No Transpose*/
             , i3, ib, -ONE, (float*)work, LDWORK, ONE, 
             &AB(i_ + kd,0), ldab - 1 );
            
            //                    Copy the upper triangle of A31 back into place.
            
            for( jj = 1, jj_ = jj - 1, _do14 = ib; jj <= _do14; jj++, jj_++ ) { 
              for( ii = 1, ii_ = ii - 1, _do15 = min( jj, 
               i3 ); ii <= _do15; ii++, ii_++ ) { 
                AB(jj_ + i - 1,kd + 1 - jj + ii_) = work[jj_][ii_];
              }
            }
          }
        }
      }
    }
  }
  return;
  
L_150:
  ;
  return;
  
  //     End of SPBTRF
  
#undef  AB
} // end of function 


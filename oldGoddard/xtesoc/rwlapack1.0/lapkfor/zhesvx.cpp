/*
 * C++ implementation of Lapack routine zhesvx
 *
 * $Id: zhesvx.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:47:29
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zhesvx.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zhesvx(const char &fact, const char &uplo, const long &n, const long &nrhs, 
 DComplex *a, const long &lda, DComplex *af, const long &ldaf, long ipiv[], 
 DComplex *b, const long &ldb, DComplex *x, const long &ldx, double &rcond, 
 double ferr[], double berr[], DComplex work[], const long &lwork, double rwork[], 
 long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define AF(I_,J_) (*(af+(I_)*(ldaf)+(J_)))
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
#define X(I_,J_)  (*(x+(I_)*(ldx)+(J_)))
  int nofact;
  double anorm;

  
  //  -- LAPACK driver routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZHESVX uses the diagonal pivoting factorization A = U*D*U' or
  //  A = L*D*L' to compute the solution to a DComplex system of linear
  //  equations
  //     A * X = B,
  //  where A is an N by N Hermitian matrix and X and B are N by NRHS
  //  matrices.
  
  //  Error bounds on the solution and a condition estimate are also
  //  provided.
  
  //  Description
  //  ===========
  
  //  The following steps are performed by this subroutine:
  
  //  1. If FACT = 'N', the diagonal pivoting method is used to factor A as
  //        A = U * D * U',  if UPLO = 'U', or
  //        A = L * D * L',  if UPLO = 'L',
  //     where U (or L) is a product of permutation and unit upper (lower)
  //     triangular matrices, D is Hermitian and block diagonal with 1-by-1
  //     and 2-by-2 diagonal blocks, and ' indicates conjugate transpose.
  
  //  2. The factored form of A is used to estimate the condition number
  //     of the matrix A.  If the reciprocal of the condition number is
  //     less than machine precision, steps 3 and 4 are skipped.
  
  //  3. The system of equations A*X = B is solved for X using the
  //     factored form of A.
  
  //  4. Iterative refinement is applied to improve the computed solution
  //     vectors and calculate error bounds and backward error estimates
  //     for them.
  
  //  Arguments
  //  =========
  
  //  FACT    (input) CHARACTER*1
  //          Specifies whether or not the factored form of A has been
  //          supplied on entry.
  //          = 'F':  On entry, AF and IPIV contain the factored form
  //                  of A.  A, AF and IPIV will not be modified.
  //          = 'N':  The matrix A will be copied to AF and factored.
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangular part of the
  //          Hermitian matrix A is stored:
  //          = 'U':  Upper triangular;
  //          = 'L':  Lower triangular.
  
  //  N       (input) INTEGER
  //          The number of linear equations, i.e., the order of the
  //          matrix A.  N >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right-hand sides, i.e., the number of columns
  //          of the matrices B and X.  NRHS >= 0.
  
  //  A       (input) COMPLEX*16 array, dimension (LDA,N)
  //          The Hermitian matrix A.  If UPLO = 'U', the leading N by N
  //          upper triangular part of A contains the upper triangular part
  //          of the matrix A, and the strictly lower triangular part of A
  //          is not referenced.  If UPLO = 'L', the leading N by N lower
  //          triangular part of A contains the lower triangular part of
  //          the matrix A, and the strictly upper triangular part of A is
  //          not referenced.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  AF      (input or output) COMPLEX*16 array, dimension (LDAF,N)
  //          If FACT = 'F', then AF is an input argument and on entry
  //          contains the block diagonal matrix D and the multipliers used
  //          to obtain the factor U or L from the factorization A = U*D*U'
  //          or A = L*D*L' as computed by ZHETRF.
  
  //          If FACT = 'N', then AF is an output argument and on exit
  //          returns the block diagonal matrix D and the multipliers used
  //          to obtain the factor U or L from the factorization A = U*D*U'
  //          or A = L*D*L'.
  
  //  LDAF    (input) INTEGER
  //          The leading dimension of the array AF.  LDAF >= max(1,N).
  
  //  IPIV    (input or output) INTEGER array, dimension (N)
  //          If FACT = 'F', then IPIV is an input argument and on entry
  //          contains details of the interchanges and the block structure
  //          of D, as determined by ZHETRF.
  //          If IPIV(k) > 0, then rows and columns k and IPIV(k) were
  //          interchanged and D(k,k) is a 1-by-1 diagonal block.
  //          If UPLO = 'U' and IPIV(k) = IPIV(k-1) < 0, then rows and
  //          columns k-1 and -IPIV(k) were interchanged and D(k-1:k,k-1:k)
  //          is a 2-by-2 diagonal block.  If UPLO = 'L' and IPIV(k) =
  //          IPIV(k+1) < 0, then rows and columns k+1 and -IPIV(k) were
  //          interchanged and D(k:k+1,k:k+1) is a 2-by-2 diagonal block.
  
  //          If FACT = 'N', then IPIV is an output argument and on exit
  //          contains details of the interchanges and the block structure
  //          of D, as determined by ZHETRF.
  
  //  B       (input) COMPLEX*16 array, dimension (LDB,NRHS)
  //          The n-by-nrhs right-hand side matrix B.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  X       (output) COMPLEX*16 array, dimension (LDX,NRHS)
  //          If INFO = 0, the n-by-nrhs solution matrix X.
  
  //  LDX     (input) INTEGER
  //          The leading dimension of the array X.  LDX >= max(1,N).
  
  //  RCOND   (output) DOUBLE PRECISION
  //          The estimate of the reciprocal condition number of the matrix
  //          A.  If RCOND is less than the machine precision (in
  //          particular, if RCOND = 0), the matrix is singular to working
  //          precision.  This condition is indicated by a return code of
  //          INFO > 0, and the solution and error bounds are not computed.
  
  //  FERR    (output) DOUBLE PRECISION array, dimension (NRHS)
  //          The estimated forward error bounds for each solution vector
  //          X(j) (the j-th column of the solution matrix X).
  //          If XTRUE is the true solution, FERR(j) bounds the magnitude
  //          of the largest entry in (X(j) - XTRUE) divided by the
  //          magnitude of the largest entry in X(j).  The quality of the
  //          error bound depends on the quality of the estimate of
  //          norm(inv(A)) computed in the code; if the estimate of
  //          norm(inv(A)) is accurate, the error bound is guaranteed.
  
  //  BERR    (output) DOUBLE PRECISION array, dimension (NRHS)
  //          The componentwise relative backward error of each solution
  //          vector X(j) (i.e., the smallest relative change in any
  //          entry of A or B that makes X(j) an exact solution).
  
  //  WORK    (workspace) COMPLEX*16 array, dimension (LWORK)
  
  //  LWORK   (input) INTEGER
  //          The length of WORK.  LWORK must be at least 2*N, and for best
  //          performance should be at least N*NB, where NB is the optimal
  //          blocksize for ZHETRF.  On exit, if INFO = 0, WORK(1) returns
  //          N*NB, the minimum value of LWORK required to use the optimal
  //          blocksize.
  
  //  RWORK   (workspace) DOUBLE PRECISION array, dimension (N)
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0 and <= N: if INFO = k, D(k,k) is exactly zero.  The
  //               factorization has been completed, but the block diagonal
  //               matrix D is exactly singular, so the solution and error
  //               bounds could not be computed.
  //          = N+1: the block diagonal matrix D is nonsingular, but RCOND
  //               is less than machine precision.  The factorization has
  //               been completed, but the matrix is singular to working
  //               precision, so the solution and error bounds have not
  //               been computed.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  nofact = lsame( fact, 'N' );
  if( !nofact && !lsame( fact, 'F' ) ) { 
    info = -1;
  }
  else if( !lsame( uplo, 'U' ) && !lsame( uplo, 'L' ) ) { 
    info = -2;
  }
  else if( n < 0 ) { 
    info = -3;
  }
  else if( nrhs < 0 ) { 
    info = -4;
  }
  else if( lda < max( 1, n ) ) { 
    info = -6;
  }
  else if( ldaf < max( 1, n ) ) { 
    info = -8;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -11;
  }
  else if( ldx < max( 1, n ) ) { 
    info = -13;
  }
  else if( lwork < 2*n ) { 
    info = -18;
  }
  if( info != 0 ) { 
    xerbla( "ZHESVX", -info );
    return;
  }
  
  if( nofact ) { 
    
    //        Compute the factorization A = U*D*U' or A = L*D*L'.
    
    zlacpy( uplo, n, n, a, lda, af, ldaf );
    zhetrf( uplo, n, af, ldaf, ipiv, work, lwork, info );
    
    //        Return if INFO is non-zero.
    
    if( info != 0 ) { 
      if( info > 0 ) 
        rcond = ZERO;
      return;
    }
  }
  
  //     Compute the norm of the matrix A.
  
  anorm = zlansy( 'I', uplo, n, a, lda, rwork );
  
  //     Compute the reciprocal of the condition number of A.
  
  zhecon( uplo, n, af, ldaf, ipiv, anorm, rcond, work, info );
  
  //     Return if the matrix is singular to working precision.
  
  if( rcond < dlamch( 'E'/*Epsilon*/ ) ) { 
    info = n + 1;
    return;
  }
  
  //     Compute the solution vectors X.
  
  zlacpy( 'F'/*Full*/, n, nrhs, b, ldb, x, ldx );
  zhetrs( uplo, n, nrhs, af, ldaf, ipiv, x, ldx, info );
  
  //     Use iterative refinement to improve the computed solutions and
  //     compute error bounds and backward error estimates for them.
  
  zherfs( uplo, n, nrhs, a, lda, af, ldaf, ipiv, b, ldb, x, ldx, 
   ferr, berr, work, rwork, info );
  
  return;
  
  //     End of ZHESVX
  
#undef  X
#undef  B
#undef  AF
#undef  A
} // end of function 


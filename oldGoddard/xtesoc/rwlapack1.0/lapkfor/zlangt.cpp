/*
 * C++ implementation of Lapack routine zlangt
 *
 * $Id: zlangt.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:48:31
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zlangt.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ONE = 1.0e0;
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL double /*FUNCTION*/ zlangt(const char &norm, const long &n, DComplex dl[], 
 DComplex d[], DComplex du[])
{
  long _do0, _do1, _do2, i, i_;
  double anorm, scale, sum, zlangt_v;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZLANGT  returns the value of the one norm,  or the Frobenius norm, or
  //  the  infinity norm,  or the  element of  largest absolute value  of a
  //  DComplex tridiagonal matrix A.
  
  //  Description
  //  ===========
  
  //  ZLANGT returns the value
  
  //     ZLANGT = ( max(abs(A(i,j))), NORM = 'M' or 'm'
  //              (
  //              ( norm1(A),         NORM = '1', 'O' or 'o'
  //              (
  //              ( normI(A),         NORM = 'I' or 'i'
  //              (
  //              ( normF(A),         NORM = 'F', 'f', 'E' or 'e'
  
  //  where  norm1  denotes the  one norm of a matrix (maximum column sum),
  //  normI  denotes the  infinity norm  of a matrix  (maximum row sum) and
  //  normF  denotes the  Frobenius norm of a matrix (square root of sum of
  //  squares).  Note that  max(abs(A(i,j)))  is not a  matrix norm.
  
  //  Arguments
  //  =========
  
  //  NORM    (input) CHARACTER*1
  //          Specifies the value to be returned in ZLANGT as described
  //          above.
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.  When N = 0, ZLANGT is
  //          set to zero.
  
  //  DL      (input) COMPLEX*16 array, dimension (N-1)
  //          The (n-1) sub-diagonal elements of A.
  
  //  D       (input) COMPLEX*16 array, dimension (N)
  //          The diagonal elements of A.
  
  //  DU      (input) COMPLEX*16 array, dimension (N-1)
  //          The (n-1) super-diagonal elements of A.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  if( n <= 0 ) { 
    anorm = ZERO;
  }
  else if( lsame( norm, 'M' ) ) { 
    
    //        Find max(abs(A(i,j))).
    
    anorm = abs( d[n - 1] );
    for( i = 1, i_ = i - 1, _do0 = n - 1; i <= _do0; i++, i_++ ) { 
      anorm = max( anorm, abs( dl[i_] ) );
      anorm = max( anorm, abs( d[i_] ) );
      anorm = max( anorm, abs( du[i_] ) );
    }
  }
  else if( lsame( norm, 'O' ) || norm == '1' ) { 
    
    //        Find norm1(A).
    
    if( n == 1 ) { 
      anorm = abs( d[0] );
    }
    else { 
      anorm = max( abs( d[0] ) + abs( dl[0] ), abs( d[n - 1] ) + 
       abs( du[n - 2] ) );
      for( i = 2, i_ = i - 1, _do1 = n - 1; i <= _do1; i++, i_++ ) { 
        anorm = max( anorm, abs( d[i_] ) + abs( dl[i_] ) + 
         abs( du[i_ - 1] ) );
      }
    }
  }
  else if( lsame( norm, 'I' ) ) { 
    
    //        Find normI(A).
    
    if( n == 1 ) { 
      anorm = abs( d[0] );
    }
    else { 
      anorm = max( abs( d[0] ) + abs( du[0] ), abs( d[n - 1] ) + 
       abs( dl[n - 2] ) );
      for( i = 2, i_ = i - 1, _do2 = n - 1; i <= _do2; i++, i_++ ) { 
        anorm = max( anorm, abs( d[i_] ) + abs( du[i_] ) + 
         abs( dl[i_ - 1] ) );
      }
    }
  }
  else if( (lsame( norm, 'F' )) || (lsame( norm, 'E' )) ) { 
    
    //        Find normF(A).
    
    scale = ZERO;
    sum = ONE;
    zlassq( n, d, 1, scale, sum );
    if( n > 1 ) { 
      zlassq( n - 1, dl, 1, scale, sum );
      zlassq( n - 1, du, 1, scale, sum );
    }
    anorm = scale*sqrt( sum );
  }
  
  zlangt_v = anorm;
  return( zlangt_v );
  
  //     End of ZLANGT
  
} // end of function 


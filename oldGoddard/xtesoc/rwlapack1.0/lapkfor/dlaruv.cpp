/*
 * C++ implementation of lapack routine dlaruv
 *
 * $Id: dlaruv.cpp,v 1.5 1993/04/06 20:41:25 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:36:06
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dlaruv.cpp,v $
 * Revision 1.5  1993/04/06  20:41:25  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.4  1993/03/19  21:04:55  alv
 * removed unused arg
 *
 * Revision 1.3  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.2  1993/03/05  23:15:48  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:07:41  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ONE = 1.0e0;
const long LV = 128;
const long IPW2 = 4096;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dlaruv(long iseed[], const long &n, double x[])
{
  // PARAMETER translations
  const double R = ONE/IPW2;
  // end of PARAMETER translations

  long _do0, i, i1, i2, i3, i4, i_, it1, it2, it3, it4, j, /*_i, */
   _r;
  static long mm[4][LV];
  static int _aini = 1;
  if( _aini ) {  // onetime initializations
    { static long _itmp0[] = {494,322,2508,2549};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][0] = _itmp0[_r++];
    }
  }
    { static long _itmp1[] = {2637,789,3754,1145};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][1] = _itmp1[_r++];
    }
  }
    { static long _itmp2[] = {255,1440,1766,2253};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][2] = _itmp2[_r++];
    }
  }
    { static long _itmp3[] = {2008,752,3572,305};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][3] = _itmp3[_r++];
    }
  }
    { static long _itmp4[] = {1253,2859,2893,3301};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][4] = _itmp4[_r++];
    }
  }
    { static long _itmp5[] = {3344,123,307,1065};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][5] = _itmp5[_r++];
    }
  }
    { static long _itmp6[] = {4084,1848,1297,3133};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][6] = _itmp6[_r++];
    }
  }
    { static long _itmp7[] = {1739,643,3966,2913};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][7] = _itmp7[_r++];
    }
  }
    { static long _itmp8[] = {3143,2405,758,3285};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][8] = _itmp8[_r++];
    }
  }
    { static long _itmp9[] = {3468,2638,2598,1241};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][9] = _itmp9[_r++];
    }
  }
    { static long _itmp10[] = {688,2344,3406,1197};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][10] = _itmp10[_r++];
    }
  }
    { static long _itmp11[] = {1657,46,2922,3729};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][11] = _itmp11[_r++];
    }
  }
    { static long _itmp12[] = {1238,3814,1038,2501};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][12] = _itmp12[_r++];
    }
  }
    { static long _itmp13[] = {3166,913,2934,1673};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][13] = _itmp13[_r++];
    }
  }
    { static long _itmp14[] = {1292,3649,2091,541};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][14] = _itmp14[_r++];
    }
  }
    { static long _itmp15[] = {3422,339,2451,2753};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][15] = _itmp15[_r++];
    }
  }
    { static long _itmp16[] = {1270,3808,1580,949};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][16] = _itmp16[_r++];
    }
  }
    { static long _itmp17[] = {2016,822,1958,2361};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][17] = _itmp17[_r++];
    }
  }
    { static long _itmp18[] = {154,2832,2055,1165};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][18] = _itmp18[_r++];
    }
  }
    { static long _itmp19[] = {2862,3078,1507,4081};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][19] = _itmp19[_r++];
    }
  }
    { static long _itmp20[] = {697,3633,1078,2725};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][20] = _itmp20[_r++];
    }
  }
    { static long _itmp21[] = {1706,2970,3273,3305};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][21] = _itmp21[_r++];
    }
  }
    { static long _itmp22[] = {491,637,17,3069};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][22] = _itmp22[_r++];
    }
  }
    { static long _itmp23[] = {931,2249,854,3617};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][23] = _itmp23[_r++];
    }
  }
    { static long _itmp24[] = {1444,2081,2916,3733};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][24] = _itmp24[_r++];
    }
  }
    { static long _itmp25[] = {444,4019,3971,409};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][25] = _itmp25[_r++];
    }
  }
    { static long _itmp26[] = {3577,1478,2889,2157};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][26] = _itmp26[_r++];
    }
  }
    { static long _itmp27[] = {3944,242,3831,1361};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][27] = _itmp27[_r++];
    }
  }
    { static long _itmp28[] = {2184,481,2621,3973};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][28] = _itmp28[_r++];
    }
  }
    { static long _itmp29[] = {1661,2075,1541,1865};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][29] = _itmp29[_r++];
    }
  }
    { static long _itmp30[] = {3482,4058,893,2525};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][30] = _itmp30[_r++];
    }
  }
    { static long _itmp31[] = {657,622,736,1409};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][31] = _itmp31[_r++];
    }
  }
    { static long _itmp32[] = {3023,3376,3992,3445};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][32] = _itmp32[_r++];
    }
  }
    { static long _itmp33[] = {3618,812,787,3577};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][33] = _itmp33[_r++];
    }
  }
    { static long _itmp34[] = {1267,234,2125,77};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][34] = _itmp34[_r++];
    }
  }
    { static long _itmp35[] = {1828,641,2364,3761};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][35] = _itmp35[_r++];
    }
  }
    { static long _itmp36[] = {164,4005,2460,2149};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][36] = _itmp36[_r++];
    }
  }
    { static long _itmp37[] = {3798,1122,257,1449};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][37] = _itmp37[_r++];
    }
  }
    { static long _itmp38[] = {3087,3135,1574,3005};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][38] = _itmp38[_r++];
    }
  }
    { static long _itmp39[] = {2400,2640,3912,225};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][39] = _itmp39[_r++];
    }
  }
    { static long _itmp40[] = {2870,2302,1216,85};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][40] = _itmp40[_r++];
    }
  }
    { static long _itmp41[] = {3876,40,3248,3673};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][41] = _itmp41[_r++];
    }
  }
    { static long _itmp42[] = {1905,1832,3401,3117};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][42] = _itmp42[_r++];
    }
  }
    { static long _itmp43[] = {1593,2247,2124,3089};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][43] = _itmp43[_r++];
    }
  }
    { static long _itmp44[] = {1797,2034,2762,1349};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][44] = _itmp44[_r++];
    }
  }
    { static long _itmp45[] = {1234,2637,149,2057};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][45] = _itmp45[_r++];
    }
  }
    { static long _itmp46[] = {3460,1287,2245,413};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][46] = _itmp46[_r++];
    }
  }
    { static long _itmp47[] = {328,1691,166,65};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][47] = _itmp47[_r++];
    }
  }
    { static long _itmp48[] = {2861,496,466,1845};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][48] = _itmp48[_r++];
    }
  }
    { static long _itmp49[] = {1950,1597,4018,697};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][49] = _itmp49[_r++];
    }
  }
    { static long _itmp50[] = {617,2394,1399,3085};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][50] = _itmp50[_r++];
    }
  }
    { static long _itmp51[] = {2070,2584,190,3441};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][51] = _itmp51[_r++];
    }
  }
    { static long _itmp52[] = {3331,1843,2879,1573};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][52] = _itmp52[_r++];
    }
  }
    { static long _itmp53[] = {769,336,153,3689};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][53] = _itmp53[_r++];
    }
  }
    { static long _itmp54[] = {1558,1472,2320,2941};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][54] = _itmp54[_r++];
    }
  }
    { static long _itmp55[] = {2412,2407,18,929};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][55] = _itmp55[_r++];
    }
  }
    { static long _itmp56[] = {2800,433,712,533};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][56] = _itmp56[_r++];
    }
  }
    { static long _itmp57[] = {189,2096,2159,2841};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][57] = _itmp57[_r++];
    }
  }
    { static long _itmp58[] = {287,1761,2318,4077};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][58] = _itmp58[_r++];
    }
  }
    { static long _itmp59[] = {2045,2810,2091,721};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][59] = _itmp59[_r++];
    }
  }
    { static long _itmp60[] = {1227,566,3443,2821};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][60] = _itmp60[_r++];
    }
  }
    { static long _itmp61[] = {2838,442,1510,2249};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][61] = _itmp61[_r++];
    }
  }
    { static long _itmp62[] = {209,41,449,2397};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][62] = _itmp62[_r++];
    }
  }
    { static long _itmp63[] = {2770,1238,1956,2817};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][63] = _itmp63[_r++];
    }
  }
    { static long _itmp64[] = {3654,1086,2201,245};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][64] = _itmp64[_r++];
    }
  }
    { static long _itmp65[] = {3993,603,3137,1913};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][65] = _itmp65[_r++];
    }
  }
    { static long _itmp66[] = {192,840,3399,1997};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][66] = _itmp66[_r++];
    }
  }
    { static long _itmp67[] = {2253,3168,1321,3121};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][67] = _itmp67[_r++];
    }
  }
    { static long _itmp68[] = {3491,1499,2271,997};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][68] = _itmp68[_r++];
    }
  }
    { static long _itmp69[] = {2889,1084,3667,1833};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][69] = _itmp69[_r++];
    }
  }
    { static long _itmp70[] = {2857,3438,2703,2877};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][70] = _itmp70[_r++];
    }
  }
    { static long _itmp71[] = {2094,2408,629,1633};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][71] = _itmp71[_r++];
    }
  }
    { static long _itmp72[] = {1818,1589,2365,981};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][72] = _itmp72[_r++];
    }
  }
    { static long _itmp73[] = {688,2391,2431,2009};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][73] = _itmp73[_r++];
    }
  }
    { static long _itmp74[] = {1407,288,1113,941};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][74] = _itmp74[_r++];
    }
  }
    { static long _itmp75[] = {634,26,3922,2449};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][75] = _itmp75[_r++];
    }
  }
    { static long _itmp76[] = {3231,512,2554,197};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][76] = _itmp76[_r++];
    }
  }
    { static long _itmp77[] = {815,1456,184,2441};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][77] = _itmp77[_r++];
    }
  }
    { static long _itmp78[] = {3524,171,2099,285};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][78] = _itmp78[_r++];
    }
  }
    { static long _itmp79[] = {1914,1677,3228,1473};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][79] = _itmp79[_r++];
    }
  }
    { static long _itmp80[] = {516,2657,4012,2741};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][80] = _itmp80[_r++];
    }
  }
    { static long _itmp81[] = {164,2270,1921,3129};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][81] = _itmp81[_r++];
    }
  }
    { static long _itmp82[] = {303,2587,3452,909};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][82] = _itmp82[_r++];
    }
  }
    { static long _itmp83[] = {2144,2961,3901,2801};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][83] = _itmp83[_r++];
    }
  }
    { static long _itmp84[] = {3480,1970,572,421};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][84] = _itmp84[_r++];
    }
  }
    { static long _itmp85[] = {119,1817,3309,4073};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][85] = _itmp85[_r++];
    }
  }
    { static long _itmp86[] = {3357,676,3171,2813};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][86] = _itmp86[_r++];
    }
  }
    { static long _itmp87[] = {837,1410,817,2337};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][87] = _itmp87[_r++];
    }
  }
    { static long _itmp88[] = {2826,3723,3039,1429};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][88] = _itmp88[_r++];
    }
  }
    { static long _itmp89[] = {2332,2803,1696,1177};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][89] = _itmp89[_r++];
    }
  }
    { static long _itmp90[] = {2089,3185,1256,1901};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][90] = _itmp90[_r++];
    }
  }
    { static long _itmp91[] = {3780,184,3715,81};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][91] = _itmp91[_r++];
    }
  }
    { static long _itmp92[] = {1700,663,2077,1669};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][92] = _itmp92[_r++];
    }
  }
    { static long _itmp93[] = {3712,499,3019,2633};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][93] = _itmp93[_r++];
    }
  }
    { static long _itmp94[] = {150,3784,1497,2269};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][94] = _itmp94[_r++];
    }
  }
    { static long _itmp95[] = {2000,1631,1101,129};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][95] = _itmp95[_r++];
    }
  }
    { static long _itmp96[] = {3375,1925,717,1141};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][96] = _itmp96[_r++];
    }
  }
    { static long _itmp97[] = {1621,3912,51,249};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][97] = _itmp97[_r++];
    }
  }
    { static long _itmp98[] = {3090,1398,981,3917};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][98] = _itmp98[_r++];
    }
  }
    { static long _itmp99[] = {3765,1349,1978,2481};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][99] = _itmp99[_r++];
    }
  }
    { static long _itmp100[] = {1149,1441,1813,3941};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][100] = _itmp100[_r++];
    }
  }
    { static long _itmp101[] = {3146,2224,3881,2217};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][101] = _itmp101[_r++];
    }
  }
    { static long _itmp102[] = {33,2411,76,2749};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][102] = _itmp102[_r++];
    }
  }
    { static long _itmp103[] = {3082,1907,3846,3041};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][103] = _itmp103[_r++];
    }
  }
    { static long _itmp104[] = {2741,3192,3694,1877};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][104] = _itmp104[_r++];
    }
  }
    { static long _itmp105[] = {359,2786,1682,345};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][105] = _itmp105[_r++];
    }
  }
    { static long _itmp106[] = {3316,382,124,2861};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][106] = _itmp106[_r++];
    }
  }
    { static long _itmp107[] = {1749,37,1660,1809};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][107] = _itmp107[_r++];
    }
  }
    { static long _itmp108[] = {185,759,3997,3141};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][108] = _itmp108[_r++];
    }
  }
    { static long _itmp109[] = {2784,2948,479,2825};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][109] = _itmp109[_r++];
    }
  }
    { static long _itmp110[] = {2202,1862,1141,157};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][110] = _itmp110[_r++];
    }
  }
    { static long _itmp111[] = {2199,3802,886,2881};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][111] = _itmp111[_r++];
    }
  }
    { static long _itmp112[] = {1364,2423,3514,3637};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][112] = _itmp112[_r++];
    }
  }
    { static long _itmp113[] = {1244,2051,1301,1465};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][113] = _itmp113[_r++];
    }
  }
    { static long _itmp114[] = {2020,2295,3604,2829};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][114] = _itmp114[_r++];
    }
  }
    { static long _itmp115[] = {3160,1332,1888,2161};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][115] = _itmp115[_r++];
    }
  }
    { static long _itmp116[] = {2785,1832,1836,3365};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][116] = _itmp116[_r++];
    }
  }
    { static long _itmp117[] = {2772,2405,1990,361};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][117] = _itmp117[_r++];
    }
  }
    { static long _itmp118[] = {1217,3638,2058,2685};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][118] = _itmp118[_r++];
    }
  }
    { static long _itmp119[] = {1822,3661,692,3745};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][119] = _itmp119[_r++];
    }
  }
    { static long _itmp120[] = {1245,327,1194,2325};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][120] = _itmp120[_r++];
    }
  }
    { static long _itmp121[] = {2252,3660,20,3609};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][121] = _itmp121[_r++];
    }
  }
    { static long _itmp122[] = {3904,716,3285,3821};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][122] = _itmp122[_r++];
    }
  }
    { static long _itmp123[] = {2774,1842,2046,3537};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][123] = _itmp123[_r++];
    }
  }
    { static long _itmp124[] = {997,3987,2107,517};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][124] = _itmp124[_r++];
    }
  }
    { static long _itmp125[] = {2573,1368,3508,3017};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][125] = _itmp125[_r++];
    }
  }
    { static long _itmp126[] = {1148,1848,3525,2141};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][126] = _itmp126[_r++];
    }
  }
    { static long _itmp127[] = {545,2366,3801,1537};
    for( j = 1, _r = 0; j <= 4; j++ ) { 
      mm[j - 1][127] = _itmp127[_r++];
    }
  }
    _aini = 0;
  }

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DLARUV returns a vector of n random real numbers from a uniform (0,1)
  //  distribution (n <= 128).
  
  //  This is an auxiliary routine called by DLARNV and ZLARNV.
  
  //  Arguments
  //  =========
  
  //  ISEED   (input/output) INTEGER array, dimension (4)
  //          On entry, the seed of the random number generator; the array
  //          elements must be between 0 and 4095, and ISEED(4) must be
  //          odd.
  //          On exit, the seed is updated.
  
  //  N       (input) INTEGER
  //          The number of random numbers to be generated. N <= 128.
  
  //  X       (output) DOUBLE PRECISION array, dimension (N)
  //          The generated random numbers.
  
  //  Further Details
  //  ===============
  
  //  This routine uses a multiplicative congruential method with modulus
  //  2**48 and multiplier 33952834046453 (see G.S.Fishman,
  //  'Multiplicative congruential random number generators with modulus
  //  2**b: an exhaustive analysis for b = 32 and a partial analysis for
  //  b = 48', Math. Comp. 189, pp 331-344, 1990).
  
  //  48-bit integers are stored in 4 integer array elements with 12 bits
  //  per element. Hence the routine is portable across machines with
  //  integers of 32 bits or more.
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Local Arrays ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Data statements ..
  //     ..
  //     .. Executable Statements ..
  
  i1 = iseed[0];
  i2 = iseed[1];
  i3 = iseed[2];
  i4 = iseed[3];
  
  for( i = 1, i_ = i - 1, _do0 = min( n, LV ); i <= _do0; i++, i_++ ) { 
    
    //        Multiply the seed by i-th power of the multiplier modulo 2**48
    
    it4 = i4*mm[3][i_];
    it3 = it4/IPW2;
    it4 = it4 - IPW2*it3;
    it3 = it3 + i3*mm[3][i_] + i4*mm[2][i_];
    it2 = it3/IPW2;
    it3 = it3 - IPW2*it2;
    it2 = it2 + i2*mm[3][i_] + i3*mm[2][i_] + i4*mm[1][i_];
    it1 = it2/IPW2;
    it2 = it2 - IPW2*it1;
    it1 = it1 + i1*mm[3][i_] + i2*mm[2][i_] + i3*mm[1][i_] + i4*
     mm[0][i_];
    // mod() function massaged out of next line
    it1 = it1 % IPW2; //it1 = mod( it1, IPW2 );
    
    //        Convert 48-bit integer to a real number in the interval (0,1)
    
    x[i_] = R*((double)( it1 ) + R*((double)( it2 ) + R*((double)( it3 ) + 
     R*(double)( it4 ))));
  }
  
  //     Return final value of seed
  
  iseed[0] = it1;
  iseed[1] = it2;
  iseed[2] = it3;
  iseed[3] = it4;
  return;
  
  //     End of DLARUV
  
} // end of function 


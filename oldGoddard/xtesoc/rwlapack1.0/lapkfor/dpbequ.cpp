/*
 * C++ implementation of lapack routine dpbequ
 *
 * $Id: dpbequ.cpp,v 1.5 1993/04/06 20:41:50 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:37:08
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dpbequ.cpp,v $
 * Revision 1.5  1993/04/06  20:41:50  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:16:31  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:08:20  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dpbequ(const char &uplo, long &n, long &kd, double *ab, 
 long &ldab, double s[], double &scond, double &amax, long &info)
{
#define AB(I_,J_) (*(ab+(I_)*(ldab)+(J_)))
  int upper;
  long _do0, _do1, _do2, i, i_, j;
  double smin;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DPBEQU computes row and column scalings intended to equilibrate a
  //  symmetric positive definite band matrix A and reduce its condition
  //  number (with respect to the two-norm).  S contains the scale factors,
  //  S(i) = 1/sqrt(A(i,i)), chosen so that the scaled matrix B with
  //  entries B(i,j) = S(i)*A(i,j)*S(j) has ones on the diagonal.  This
  //  choice of S puts the condition number of B within a factor N of the
  //  smallest possible condition number over all possible diagonal
  //  scalings.
  
  //  SCOND returns the ratio of the smallest S(i) to the largest S(i).
  //  If SCOND >= 0.1 and AMAX is neither too large nor too small, it is
  //  not worth scaling by S.
  
  //  AMAX returns the absolute value of the largest matrix entry
  //  (which is on the diagonal if the matrix is positive definite).
  //  If AMAX is very close to overflow or very close to underflow, the
  //  matrix should be scaled.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangular part of the
  //          symmetric matrix A is stored:
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The number of rows and columns of the matrix A.  N >= 0.
  
  //  KD      (input) INTEGER
  //          The number of super-diagonals of the matrix A if UPLO = 'U',
  //          or the number of sub-diagonals if UPLO = 'L'.  KD >= 0.
  
  //  AB      (input) DOUBLE PRECISION array, dimension (LDAB,N)
  //          The upper or lower triangle of the symmetric band matrix A,
  //          stored in the first KD+1 rows of the array.  The j-th column
  //          of A is stored in the j-th column of the array AB as follows:
  //          if UPLO = 'U', AB(kd+1+i-j,j) = A(i,j) for max(1,j-kd)<=i<=j;
  //          if UPLO = 'L', AB(1+i-j,j)    = A(i,j) for j<=i<=min(n,j+kd).
  
  //  LDAB     (input) INTEGER
  //          The leading dimension of the array A.  LDAB >= KD+1.
  
  //  S       (output) DOUBLE PRECISION array, dimension (N)
  //          The scale factors for A.  Not assigned if INFO > 0.
  
  //  SCOND   (output) DOUBLE PRECISION
  //          Ratio of the smallest S(i) to the largest S(i).
  //          Not assigned if INFO > 0.
  
  //  AMAX    (output) DOUBLE PRECISION
  //          Absolute value of largest matrix entry.
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value.
  //          > 0: if INFO = k, the k-th diagonal entry is nonpositive.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( kd < 0 ) { 
    info = -3;
  }
  else if( ldab < kd + 1 ) { 
    info = -5;
  }
  if( info != 0 ) { 
    xerbla( "DPBEQU", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) { 
    scond = ONE;
    amax = ZERO;
    return;
  }
  
  if( upper ) { 
    j = kd + 1;
  }
  else { 
    j = 1;
  }
  
  //     Initialize SMIN and AMAX.
  
  s[0] = AB(0,j - 1);
  smin = s[0];
  amax = s[0];
  
  //     Find the minimum and maximum diagonal elements.
  
  for( i = 2, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
    s[i_] = AB(i_,j - 1);
    smin = min( smin, s[i_] );
    amax = max( amax, s[i_] );
  }
  
  if( smin <= ZERO ) { 
    
    //        Find the first non-positive diagonal element and return.
    
    for( i = 1, i_ = i - 1, _do1 = n; i <= _do1; i++, i_++ ) { 
      if( s[i_] <= ZERO ) { 
        info = i;
        return;
      }
    }
  }
  else { 
    
    //        Set the scale factors to the reciprocals
    //        of the diagonal elements.
    
    for( i = 1, i_ = i - 1, _do2 = n; i <= _do2; i++, i_++ ) { 
      s[i_] = ONE/sqrt( s[i_] );
    }
    
    //        Compute SCOND = min(S(I)) / max(S(I))
    
    scond = sqrt( smin )/sqrt( amax );
  }
  return;
  
  //     End of DPBEQU
  
#undef  AB
} // end of function 


/*
 * C++ implementation of Lapack routine zppequ
 *
 * $Id: zppequ.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:50:06
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zppequ.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ONE = 1.0e0;
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zppequ(const char &uplo, const long &n, DComplex ap[], double s[], 
 double &scond, double &amax, long &info)
{
  int upper;
  long _do0, _do1, _do2, _do3, i, i_, jj;
  double smin;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZPPEQU computes row and column scalings intended to equilibrate a
  //  Hermitian positive definite matrix A in packed storage and reduce
  //  its condition number (with respect to the two-norm).  S contains the
  //  scale factors, S(i)=1/sqrt(A(i,i)), chosen so that the scaled matrix
  //  B with entries B(i,j)=S(i)*A(i,j)*S(j) has ones on the diagonal. This
  //  choice of S puts the condition number of B within a factor N of the
  //  smallest possible condition number over all possible diagonal
  //  scalings.
  
  //  SCOND returns the ratio of the smallest S(i) to the largest S(i).
  //  If SCOND >= 0.1 and AMAX is neither too large nor too small, it is
  //  not worth scaling by S.
  
  //  AMAX returns the absolute value of the largest matrix entry
  //  (which is on the diagonal if the matrix is positive definite).
  //  If AMAX is very close to overflow or very close to underflow, the
  //  matrix should be scaled.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangular part of the
  //          Hermitian matrix A is stored:
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  AP      (input) COMPLEX*16 array, dimension (N*(N+1)/2)
  //          The upper or lower triangle of the Hermitian matrix A, packed
  //          columnwise in a linear array.  The j-th column of A is stored
  //          in the array AP as follows:
  //          if UPLO = 'U', AP(i + (j-1)*j/2) = A(i,j) for 1<=i<=j;
  //          if UPLO = 'L', AP(i + (j-1)*(2n-j)/2) = A(i,j) for j<=i<=n.
  
  //  S       (output) DOUBLE PRECISION array, dimension (N)
  //          The scale factors for A.  Not assigned if INFO > 0.
  
  //  SCOND   (output) DOUBLE PRECISION
  //          Ratio of the smallest S(i) to the largest S(i).
  //          Not assigned if INFO > 0.
  
  //  AMAX    (output) DOUBLE PRECISION
  //          Absolute value of largest matrix entry.
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: if INFO = k, the k-th diagonal entry is nonpositive.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  if( info != 0 ) { 
    xerbla( "ZPPEQU", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) { 
    scond = ONE;
    amax = ZERO;
    return;
  }
  
  //     Initialize SMIN and AMAX.
  
  s[0] = real( ap[0] );
  smin = s[0];
  amax = s[0];
  
  if( upper ) { 
    
    //        UPLO = 'U':  Upper triangle of A is stored.
    //        Find the minimum and maximum diagonal elements.
    
    jj = 1;
    for( i = 2, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
      jj = jj + i;
      s[i_] = real( ap[jj - 1] );
      smin = min( smin, s[i_] );
      amax = max( amax, s[i_] );
    }
    
  }
  else { 
    
    //        UPLO = 'L':  Lower triangle of A is stored.
    //        Find the minimum and maximum diagonal elements.
    
    jj = 1;
    for( i = 2, i_ = i - 1, _do1 = n; i <= _do1; i++, i_++ ) { 
      jj = jj + n - i + 2;
      s[i_] = real( ap[jj - 1] );
      smin = min( smin, s[i_] );
      amax = max( amax, s[i_] );
    }
  }
  
  if( smin <= ZERO ) { 
    
    //        Find the first non-positive diagonal element and return.
    
    for( i = 1, i_ = i - 1, _do2 = n; i <= _do2; i++, i_++ ) { 
      if( s[i_] <= ZERO ) { 
        info = i;
        return;
      }
    }
  }
  else { 
    
    //        Set the scale factors to the reciprocals
    //        of the diagonal elements.
    
    for( i = 1, i_ = i - 1, _do3 = n; i <= _do3; i++, i_++ ) { 
      s[i_] = ONE/sqrt( s[i_] );
    }
    
    //        Compute SCOND = min(S(I)) / max(S(I))
    
    scond = sqrt( smin )/sqrt( amax );
  }
  return;
  
  //     End of ZPPEQU
  
} // end of function 


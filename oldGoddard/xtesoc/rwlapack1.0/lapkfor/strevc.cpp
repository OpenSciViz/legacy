/*
 * C++ implementation of Lapack routine strevc
 *
 * $Id: strevc.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:03:29
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: strevc.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ZERO = 0.0e0;
const float ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ strevc(const char &job, const char &howmny, int select[], 
 const long &n, float *t, const long &ldt, float *vl, const long &ldvl, 
 float *vr, const long &ldvr, const long &mm, long &m, float work[], 
 long &info)
{
#define T(I_,J_)  (*(t+(I_)*(ldt)+(J_)))
#define VL(I_,J_) (*(vl+(I_)*(ldvl)+(J_)))
#define VR(I_,J_) (*(vr+(I_)*(ldvr)+(J_)))
  int allv, bothv, leftv, over, pair, rightv, somev;
  long _do0, _do1, _do10, _do11, _do12, _do13, _do14, _do15, 
   _do16, _do17, _do2, _do3, _do4, _do5, _do6, _do7, _do8, _do9, 
   i, i_, ierr, ii, ip, is, j, j1, j2, j_, jnxt, k, k_, ki, ki_, 
   n2;
  float beta, bignum, emax, ovfl, rec, remax, scale, smin, smlnum, 
   ulp, unfl, vcrit, vmax, wi, wr, x[2][2], xnorm;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  STREVC computes all or some right and/or left eigenvectors of a
  //  real upper quasi-triangular matrix T in Schur canonical form.
  
  //  The right eigenvector x and the left eigenvector y of T corresponding
  //  to an eigenvalue w are defined by:
  
  //               T*x = w*x,     y'*T = w*y'
  
  //  where y' denotes the conjugate transpose of the vector y.
  
  //  The routine may either return the matrices X and/or Y of right or
  //  left eigenvectors of T, or the products Q*X and/or Q*Y, where Q is an
  //  input orthogonal matrix. If T was obtained from the real Schur
  //  factorization of an original matrix A = Q*T*Q', then Q*X and/or Q*Y
  //  are the matrices of right or left eigenvectors of A.
  
  //  Arguments
  //  =========
  
  //  JOB     (input) CHARACTER*1
  //          = 'R': compute right eigenvectors only
  //          = 'L': compute left eigenvectors only
  //          = 'B': compute both right and left eigenvectors
  
  //  HOWMNY  (input) CHARACTER*1
  //          Specifies how many left/right eigenvectors are wanted and the
  //          form of the eigenvector matrix X or Y returned in VR or VL.
  //          = 'A': compute all right and/or left eigenvectors;
  //          = 'O': compute all right and/or left eigenvectors, multiplied
  //                 on the left by an input (generally orthogonal) matrix;
  //          = 'S': compute some right and/or left eigenvectors, specified
  //                 by the logical array SELECT.
  
  //  SELECT  (input/output) LOGICAL array, dimension (N)
  //          If HOWMNY = 'S', SELECT specifies the eigenvectors to be
  //          computed. To select the real eigenvector corresponding to a
  //          real eigenvalue w(j), SELECT(j) must be set to .TRUE.. To
  //          select the DComplex eigenvector corresponding to a DComplex
  //          conjugate pair w(j) and w(j+1), either SELECT(j) or
  //          SELECT(j+1) must be set to .TRUE.; then on exit SELECT(j) is
  //          .TRUE. and SELECT(j+1) is .FALSE..
  //          If HOWMNY = 'A' or 'O', SELECT is not referenced.
  
  //  N       (input) INTEGER
  //          The order of the matrix T. N >= 0.
  
  //  T       (input) REAL array, dimension (LDT,N)
  //          The upper quasi-triangular matrix T in Schur canonical form.
  
  //  LDT     (input) INTEGER
  //          The leading dimension of the array T. LDT >= max(1,N).
  
  //  VL      (input/output) REAL array, dimension (LDVL,MM)
  //          On entry, if JOB = 'L' or 'B' and HOWMNY = 'O', VL must
  //          contain an n-by-n matrix Q (usually the orthogonal matrix Q
  //          of Schur vectors returned by SHSEQR).
  //          On exit, if JOB = 'L' or 'B', VL contains:
  //          if HOWMNY = 'A', the matrix Y of left eigenvectors of T;
  //          if HOWMNY = 'O', the matrix Q*Y;
  //          if HOWMNY = 'S', the left eigenvectors of T specified by
  //                           SELECT, stored consecutively in the columns
  //                           of VL, in the same order as their
  //                           eigenvalues.
  //          A DComplex eigenvector corresponding to a DComplex eigenvalue
  //          is stored in two consecutive columns, the first holding the
  //          real part, and the second the imaginary part.
  //          If JOB = 'R', VL is not referenced.
  
  //  LDVL    (input) INTEGER
  //          The leading dimension of the array VL. LDVL >= max(1,N).
  
  //  VR      (input/output) REAL array, dimension (LDVR,MM)
  //          On entry, if JOB = 'R' or 'B' and HOWMNY = 'O', VR must
  //          contain an n-by-n matrix Q (usually the orthogonal matrix Q
  //          of Schur vectors returned by SHSEQR).
  //          On exit, if JOB = 'R' or 'B', VR contains:
  //          if HOWMNY = 'A', the matrix X of right eigenvectors of T;
  //          if HOWMNY = 'O', the matrix Q*X;
  //          if HOWMNY = 'S', the right eigenvectors of T specified by
  //                           SELECT, stored consecutively in the columns
  //                           of VR, in the same order as their
  //                           eigenvalues.
  //          A DComplex eigenvector corresponding to a DComplex eigenvalue
  //          is stored in two consecutive columns, the first holding the
  //          real part and the second the imaginary part.
  //          If JOB = 'L', VR is not referenced.
  
  //  LDVR    (input) INTEGER
  //          The leading dimension of the array VR. LDVR >= max(1,N).
  
  //  MM      (input) INTEGER
  //          The number of columns in the arrays VL and/or VR. MM >= M.
  
  //  M       (output) INTEGER
  //          The number of columns in the arrays VL and/or VR required to
  //          store the eigenvectors; each selected real eigenvector
  //          occupies one column and each selected DComplex eigenvector
  //          occupies two columns.  If HOWMNY = 'A' or 'O', M is set to N.
  
  //  WORK    (workspace) REAL array, dimension (3*N)
  
  //  INFO    (output) INTEGER
  //          = 0:   successful exit
  //          < 0:   if INFO = -i, the i-th argument had an illegal value
  
  //  Further Details
  //  ===============
  
  //  The algorithm used in this program is basically backward (forward)
  //  substitution, with scaling to make the the code robust against
  //  possible overflow.
  
  //  Each eigenvector is normalized so that the element of largest
  //  magnitude has magnitude 1; here the magnitude of a DComplex number
  //  (x,y) is taken to be |x| + |y|.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Local Arrays ..
  //     ..
  //     .. Executable Statements ..
  
  //     Decode and test the input parameters
  
  bothv = lsame( job, 'B' );
  rightv = lsame( job, 'R' ) || bothv;
  leftv = lsame( job, 'L' ) || bothv;
  
  allv = lsame( howmny, 'A' );
  over = lsame( howmny, 'O' );
  somev = lsame( howmny, 'S' );
  
  info = 0;
  if( !rightv && !leftv ) { 
    info = -1;
  }
  else if( (!allv && !over) && !somev ) { 
    info = -2;
  }
  else if( n < 0 ) { 
    info = -4;
  }
  else if( ldt < max( 1, n ) ) { 
    info = -6;
  }
  else if( ldvl < 1 || (leftv && ldvl < n) ) { 
    info = -8;
  }
  else if( ldvr < 1 || (rightv && ldvr < n) ) { 
    info = -10;
  }
  else { 
    
    //        Set M to the number of columns required to store the selected
    //        eigenvectors, standardize the array SELECT if necessary, and
    //        test MM.
    
    if( somev ) { 
      m = 0;
      pair = FALSE;
      for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
        if( pair ) { 
          pair = FALSE;
          select[j_] = FALSE;
        }
        else { 
          if( j < n ) { 
            if( T(j_,j_ + 1) == ZERO ) { 
              if( select[j_] ) 
                m = m + 1;
            }
            else { 
              pair = TRUE;
              if( select[j_] || select[j_ + 1] ) { 
                select[j_] = TRUE;
                m = m + 2;
              }
            }
          }
          else { 
            if( select[n - 1] ) 
              m = m + 1;
          }
        }
      }
    }
    else { 
      m = n;
    }
    
    if( mm < m ) { 
      info = -11;
    }
  }
  if( info != 0 ) { 
    xerbla( "STREVC", -info );
    return;
  }
  
  //     Quick return if possible.
  
  if( n == 0 ) 
    return;
  
  //     Set the constants to control overflow.
  
  unfl = slamch( 'S'/*Safe minimum*/ );
  ovfl = ONE/unfl;
  slabad( unfl, ovfl );
  ulp = slamch( 'P'/*Precision*/ );
  smlnum = unfl*(n/ulp);
  bignum = (ONE - ulp)/smlnum;
  
  //     Compute 1-norm of each column of strictly upper triangular
  //     part of T to control overflow in triangular solver.
  
  work[0] = ZERO;
  for( j = 2, j_ = j - 1, _do1 = n; j <= _do1; j++, j_++ ) { 
    work[j_] = ZERO;
    for( i = 1, i_ = i - 1, _do2 = j - 1; i <= _do2; i++, i_++ ) { 
      work[j_] = work[j_] + abs( T(j_,i_) );
    }
  }
  
  //     Index IP is used to specify the real or DComplex eigenvalue:
  //       IP = 0, real eigenvalue,
  //            1, first of conjugate DComplex pair: (wr,wi)
  //           -1, second of conjugate DComplex pair: (wr,wi)
  
  n2 = 2*n;
  
  if( rightv ) { 
    
    //        Compute right eigenvectors.
    
    ip = 0;
    is = m;
    for( ki = n, ki_ = ki - 1; ki >= 1; ki--, ki_-- ) { 
      
      if( ip == 1 ) 
        goto L_130;
      if( ki == 1 ) 
        goto L_40;
      if( T(ki_ - 1,ki_) == ZERO ) 
        goto L_40;
      ip = -1;
      
L_40:
      ;
      if( somev ) { 
        if( ip == 0 ) { 
          if( !select[ki_] ) 
            goto L_130;
        }
        else { 
          if( !select[ki_ - 1] ) 
            goto L_130;
        }
      }
      
      //           Compute the KI-th eigenvalue (WR,WI).
      
      wr = T(ki_,ki_);
      wi = ZERO;
      if( ip != 0 ) 
        wi = sqrt( abs( T(ki_ - 1,ki_) ) )*sqrt( abs( T(ki_,ki_ - 1) ) );
      smin = max( ulp*(abs( wr ) + abs( wi )), smlnum );
      
      if( ip == 0 ) { 
        
        //              Real right eigenvector
        
        work[ki_ + n] = ONE;
        
        //              Form right-hand side
        
        for( k = 1, k_ = k - 1, _do3 = ki - 1; k <= _do3; k++, k_++ ) { 
          work[k_ + n] = -T(ki_,k_);
        }
        
        //              Solve the upper quasi-triangular system:
        //                 (T(1:KI-1,1:KI-1) - WR)*X = SCALE*WORK.
        
        jnxt = ki - 1;
        for( j = ki - 1, j_ = j - 1; j >= 1; j--, j_-- ) { 
          if( j > jnxt ) 
            goto L_60;
          j1 = j;
          j2 = j;
          jnxt = j - 1;
          if( j > 1 ) { 
            if( T(j_ - 1,j_) != ZERO ) { 
              j1 = j - 1;
              jnxt = j - 2;
            }
          }
          
          if( j1 == j2 ) { 
            
            //                    1-by-1 diagonal block
            
            slaln2( FALSE, 1, 1, smin, ONE, &T(j_,j_), 
             ldt, ONE, ONE, &work[j_ + n], n, wr, ZERO, 
             (float*)x, 2, scale, xnorm, ierr );
            
            //                    Scale X(1,1) to avoid overflow when updating
            //                    the right-hand side.
            
            if( xnorm > ONE ) { 
              if( work[j_] > bignum/xnorm ) { 
                x[0][0] = x[0][0]/xnorm;
                scale = scale/xnorm;
              }
            }
            
            //                    Scale if necessary
            
            if( scale != ONE ) 
              sscal( ki, scale, &work[n], 1 );
            work[j_ + n] = x[0][0];
            
            //                    Update right-hand side
            
            saxpy( j - 1, -x[0][0], &T(j_,0), 1, &work[n], 
             1 );
            
          }
          else { 
            
            //                    2-by-2 diagonal block
            
            slaln2( FALSE, 2, 1, smin, ONE, &T(j_ - 1,j_ - 1), 
             ldt, ONE, ONE, &work[j_ - 1 + n], n, wr, 
             ZERO, (float*)x, 2, scale, xnorm, ierr );
            
            //                    Scale X(1,1) and X(2,1) to avoid overflow when
            //                    updating the right-hand side.
            
            if( xnorm > ONE ) { 
              beta = max( work[j_ - 1], work[j_] );
              if( beta > bignum/xnorm ) { 
                x[0][0] = x[0][0]/xnorm;
                x[0][1] = x[0][1]/xnorm;
                scale = scale/xnorm;
              }
            }
            
            //                    Scale if necessary
            
            if( scale != ONE ) 
              sscal( ki, scale, &work[n], 1 );
            work[j_ - 1 + n] = x[0][0];
            work[j_ + n] = x[0][1];
            
            //                    Update right-hand side
            
            saxpy( j - 2, -x[0][0], &T(j_ - 1,0), 1, &work[n], 
             1 );
            saxpy( j - 2, -x[0][1], &T(j_,0), 1, &work[n], 
             1 );
          }
L_60:
          ;
        }
        
        //              Copy the vector x or Q*x to VR and normalize.
        
        if( !over ) { 
          scopy( ki, &work[n], 1, &VR(is - 1,0), 1 );
          
          ii = isamax( ki, &VR(is - 1,0), 1 );
          remax = ONE/abs( VR(is - 1,ii - 1) );
          sscal( ki, remax, &VR(is - 1,0), 1 );
          
          for( k = ki + 1, k_ = k - 1, _do4 = n; k <= _do4; k++, k_++ ) { 
            VR(is - 1,k_) = ZERO;
          }
        }
        else { 
          if( ki > 1 ) 
            sgemv( 'N', n, ki - 1, ONE, vr, ldvr, &work[n], 
             1, work[ki_ + n], &VR(ki_,0), 1 );
          
          ii = isamax( n, &VR(ki_,0), 1 );
          remax = ONE/abs( VR(ki_,ii - 1) );
          sscal( n, remax, &VR(ki_,0), 1 );
        }
        
      }
      else { 
        
        //              Complex right eigenvector.
        
        //              Initial solve
        //                [ (T(KI-1,KI-1) T(KI-1,KI) ) - (WR + I* WI)]*X = 0.
        //                [ (T(KI,KI-1)   T(KI,KI)   )               ]
        
        if( abs( T(ki_,ki_ - 1) ) >= abs( T(ki_ - 1,ki_) ) ) { 
          work[ki_ - 1 + n] = ONE;
          work[ki_ + n2] = wi/T(ki_,ki_ - 1);
        }
        else { 
          work[ki_ - 1 + n] = -wi/T(ki_ - 1,ki_);
          work[ki_ + n2] = ONE;
        }
        work[ki_ + n] = ZERO;
        work[ki_ - 1 + n2] = ZERO;
        
        //              Form right-hand side
        
        for( k = 1, k_ = k - 1, _do5 = ki - 2; k <= _do5; k++, k_++ ) { 
          work[k_ + n] = -work[ki_ - 1 + n]*T(ki_ - 1,k_);
          work[k_ + n2] = -work[ki_ + n2]*T(ki_,k_);
        }
        
        //              Solve upper quasi-triangular system:
        //              (T(1:KI-2,1:KI-2) - (WR+i*WI))*X = SCALE*(WORK+i*WORK2)
        
        jnxt = ki - 2;
        for( j = ki - 2, j_ = j - 1; j >= 1; j--, j_-- ) { 
          if( j > jnxt ) 
            goto L_90;
          j1 = j;
          j2 = j;
          jnxt = j - 1;
          if( j > 1 ) { 
            if( T(j_ - 1,j_) != ZERO ) { 
              j1 = j - 1;
              jnxt = j - 2;
            }
          }
          
          if( j1 == j2 ) { 
            
            //                    1-by-1 diagonal block
            
            slaln2( FALSE, 1, 2, smin, ONE, &T(j_,j_), 
             ldt, ONE, ONE, &work[j_ + n], n, wr, wi, 
             (float*)x, 2, scale, xnorm, ierr );
            
            //                    Scale X(1,1) and X(1,2) to avoid overflow when
            //                    updating the right-hand side.
            
            if( xnorm > ONE ) { 
              if( work[j_] > bignum/xnorm ) { 
                x[0][0] = x[0][0]/xnorm;
                x[1][0] = x[1][0]/xnorm;
                scale = scale/xnorm;
              }
            }
            
            //                    Scale if necessary
            
            if( scale != ONE ) { 
              sscal( ki, scale, &work[n], 1 );
              sscal( ki, scale, &work[n2], 1 );
            }
            work[j_ + n] = x[0][0];
            work[j_ + n2] = x[1][0];
            
            //                    Update the right-hand side
            
            saxpy( j - 1, -x[0][0], &T(j_,0), 1, &work[n], 
             1 );
            saxpy( j - 1, -x[1][0], &T(j_,0), 1, &work[n2], 
             1 );
            
          }
          else { 
            
            //                    2-by-2 diagonal block
            
            slaln2( FALSE, 2, 2, smin, ONE, &T(j_ - 1,j_ - 1), 
             ldt, ONE, ONE, &work[j_ - 1 + n], n, wr, 
             wi, (float*)x, 2, scale, xnorm, ierr );
            
            //                    Scale X to avoid overflow when updating
            //                    the right-hand side.
            
            if( xnorm > ONE ) { 
              beta = max( work[j_ - 1], work[j_] );
              if( beta > bignum/xnorm ) { 
                rec = ONE/xnorm;
                x[0][0] = x[0][0]*rec;
                x[1][0] = x[1][0]*rec;
                x[0][1] = x[0][1]*rec;
                x[1][1] = x[1][1]*rec;
                scale = scale*rec;
              }
            }
            
            //                    Scale if necessary
            
            if( scale != ONE ) { 
              sscal( ki, scale, &work[n], 1 );
              sscal( ki, scale, &work[n2], 1 );
            }
            work[j_ - 1 + n] = x[0][0];
            work[j_ + n] = x[0][1];
            work[j_ - 1 + n2] = x[1][0];
            work[j_ + n2] = x[1][1];
            
            //                    Update the right-hand side
            
            saxpy( j - 2, -x[0][0], &T(j_ - 1,0), 1, &work[n], 
             1 );
            saxpy( j - 2, -x[0][1], &T(j_,0), 1, &work[n], 
             1 );
            saxpy( j - 2, -x[1][0], &T(j_ - 1,0), 1, &work[n2], 
             1 );
            saxpy( j - 2, -x[1][1], &T(j_,0), 1, &work[n2], 
             1 );
          }
L_90:
          ;
        }
        
        //              Copy the vector x or Q*x to VR and normalize.
        
        if( !over ) { 
          scopy( ki, &work[n], 1, &VR(is - 2,0), 1 );
          scopy( ki, &work[n2], 1, &VR(is - 1,0), 1 );
          
          emax = ZERO;
          for( k = 1, k_ = k - 1, _do6 = ki; k <= _do6; k++, k_++ ) { 
            emax = max( emax, abs( VR(is - 2,k_) ) + abs( VR(is - 1,k_) ) );
          }
          
          remax = ONE/emax;
          sscal( ki, remax, &VR(is - 2,0), 1 );
          sscal( ki, remax, &VR(is - 1,0), 1 );
          
          for( k = ki + 1, k_ = k - 1, _do7 = n; k <= _do7; k++, k_++ ) { 
            VR(is - 2,k_) = ZERO;
            VR(is - 1,k_) = ZERO;
          }
          
        }
        else { 
          
          if( ki > 2 ) { 
            sgemv( 'N', n, ki - 2, ONE, vr, ldvr, &work[n], 
             1, work[ki_ - 1 + n], &VR(ki_ - 1,0), 1 );
            sgemv( 'N', n, ki - 2, ONE, vr, ldvr, &work[n2], 
             1, work[ki_ + n2], &VR(ki_,0), 1 );
          }
          else { 
            sscal( n, work[ki_ - 1 + n], &VR(ki_ - 1,0), 
             1 );
            sscal( n, work[ki_ + n2], &VR(ki_,0), 1 );
          }
          
          emax = ZERO;
          for( k = 1, k_ = k - 1, _do8 = n; k <= _do8; k++, k_++ ) { 
            emax = max( emax, abs( VR(ki_ - 1,k_) ) + 
             abs( VR(ki_,k_) ) );
          }
          remax = ONE/emax;
          sscal( n, remax, &VR(ki_ - 1,0), 1 );
          sscal( n, remax, &VR(ki_,0), 1 );
        }
      }
      
      is = is - 1;
      if( ip != 0 ) 
        is = is - 1;
L_130:
      ;
      if( ip == 1 ) 
        ip = 0;
      if( ip == -1 ) 
        ip = 1;
    }
  }
  
  if( leftv ) { 
    
    //        Compute left eigenvectors.
    
    ip = 0;
    is = 1;
    for( ki = 1, ki_ = ki - 1, _do9 = n; ki <= _do9; ki++, ki_++ ) { 
      
      if( ip == -1 ) 
        goto L_250;
      if( ki == n ) 
        goto L_150;
      if( T(ki_,ki_ + 1) == ZERO ) 
        goto L_150;
      ip = 1;
      
L_150:
      ;
      if( somev ) { 
        if( !select[ki_] ) 
          goto L_250;
      }
      
      //           Compute the KI-th eigenvalue (WR,WI).
      
      wr = T(ki_,ki_);
      wi = ZERO;
      if( ip != 0 ) 
        wi = sqrt( abs( T(ki_ + 1,ki_) ) )*sqrt( abs( T(ki_,ki_ + 1) ) );
      smin = max( ulp*(abs( wr ) + abs( wi )), smlnum );
      
      if( ip == 0 ) { 
        
        //              Real left eigenvector.
        
        work[ki_ + n] = ONE;
        
        //              Form right-hand side
        
        for( k = ki + 1, k_ = k - 1, _do10 = n; k <= _do10; k++, k_++ ) { 
          work[k_ + n] = -T(k_,ki_);
        }
        
        //              Solve the quasi-triangular system:
        //                 (T(KI+1:N,KI+1:N) - WR)'*X = SCALE*WORK
        
        vmax = ONE;
        vcrit = bignum;
        
        jnxt = ki + 1;
        for( j = ki + 1, j_ = j - 1, _do11 = n; j <= _do11; j++, j_++ ) { 
          if( j < jnxt ) 
            goto L_170;
          j1 = j;
          j2 = j;
          jnxt = j + 1;
          if( j < n ) { 
            if( T(j_,j_ + 1) != ZERO ) { 
              j2 = j + 1;
              jnxt = j + 2;
            }
          }
          
          if( j1 == j2 ) { 
            
            //                    1-by-1 diagonal block
            
            //                    Scale if necessary to avoid overflow when forming
            //                    the right-hand side.
            
            if( work[j_] > vcrit ) { 
              rec = ONE/vmax;
              sscal( n - ki + 1, rec, &work[ki_ + n], 
               1 );
              vmax = ONE;
              vcrit = bignum;
            }
            
            work[j_ + n] = work[j_ + n] - sdot( j - ki - 
             1, &T(j_,ki_ + 1), 1, &work[ki_ + 1 + n], 
             1 );
            
            //                    Solve (T(J,J)-WR)'*X = WORK
            
            slaln2( FALSE, 1, 1, smin, ONE, &T(j_,j_), 
             ldt, ONE, ONE, &work[j_ + n], n, wr, ZERO, 
             (float*)x, 2, scale, xnorm, ierr );
            
            //                    Scale if necessary
            
            if( scale != ONE ) 
              sscal( n - ki + 1, scale, &work[ki_ + n], 
               1 );
            work[j_ + n] = x[0][0];
            vmax = max( abs( work[j_ + n] ), vmax );
            vcrit = bignum/vmax;
            
          }
          else { 
            
            //                    2-by-2 diagonal block
            
            //                    Scale if necessary to avoid overflow when forming
            //                    the right-hand side.
            
            beta = max( work[j_], work[j_ + 1] );
            if( beta > vcrit ) { 
              rec = ONE/vmax;
              sscal( n - ki + 1, rec, &work[ki_ + n], 
               1 );
              vmax = ONE;
              vcrit = bignum;
            }
            
            work[j_ + n] = work[j_ + n] - sdot( j - ki - 
             1, &T(j_,ki_ + 1), 1, &work[ki_ + 1 + n], 
             1 );
            
            work[j_ + 1 + n] = work[j_ + 1 + n] - sdot( j - 
             ki - 1, &T(j_ + 1,ki_ + 1), 1, &work[ki_ + 1 + n], 
             1 );
            
            //                    Solve
            //                      [T(J,J)-WR   T(J,J+1)     ]'* X = SCALE*( WORK1 )
            //                      [T(J+1,J)    T(J+1,J+1)-WR]             ( WORK2 )
            
            slaln2( TRUE, 2, 1, smin, ONE, &T(j_,j_), 
             ldt, ONE, ONE, &work[j_ + n], n, wr, ZERO, 
             (float*)x, 2, scale, xnorm, ierr );
            
            //                    Scale if necessary
            
            if( scale != ONE ) 
              sscal( n - ki + 1, scale, &work[ki_ + n], 
               1 );
            work[j_ + n] = x[0][0];
            work[j_ + 1 + n] = x[0][1];
            
            vmax = ::vmax( abs( work[j_ + n] ), abs( work[j_ + 1 + n] ), 
             vmax, FEND );
            vcrit = bignum/vmax;
            
          }
L_170:
          ;
        }
        
        //              Copy the vector x or Q*x to VL and normalize.
        
        if( !over ) { 
          scopy( n - ki + 1, &work[ki_ + n], 1, &VL(is - 1,ki_), 
           1 );
          
          ii = isamax( n - ki + 1, &VL(is - 1,ki_), 1 ) + 
           ki - 1;
          remax = ONE/abs( VL(is - 1,ii - 1) );
          sscal( n - ki + 1, remax, &VL(is - 1,ki_), 1 );
          
          for( k = 1, k_ = k - 1, _do12 = ki - 1; k <= _do12; k++, k_++ ) { 
            VL(is - 1,k_) = ZERO;
          }
          
        }
        else { 
          
          if( ki < n ) 
            sgemv( 'N', n, n - ki, ONE, &VL(ki_ + 1,0), 
             ldvl, &work[ki_ + 1 + n], 1, work[ki_ + n], 
             &VL(ki_,0), 1 );
          
          ii = isamax( n, &VL(ki_,0), 1 );
          remax = ONE/abs( VL(ki_,ii - 1) );
          sscal( n, remax, &VL(ki_,0), 1 );
          
        }
        
      }
      else { 
        
        //              Complex left eigenvector.
        
        //               Initial solve:
        //                 ((T(KI,KI)    T(KI,KI+1) )' - (WR - I* WI))*X = 0.
        //                 ((T(KI+1,KI) T(KI+1,KI+1))                )
        
        if( abs( T(ki_ + 1,ki_) ) >= abs( T(ki_,ki_ + 1) ) ) { 
          work[ki_ + n] = wi/T(ki_ + 1,ki_);
          work[ki_ + 1 + n2] = ONE;
        }
        else { 
          work[ki_ + n] = ONE;
          work[ki_ + 1 + n2] = -wi/T(ki_,ki_ + 1);
        }
        work[ki_ + 1 + n] = ZERO;
        work[ki_ + n2] = ZERO;
        
        //              Form right-hand side
        
        for( k = ki + 2, k_ = k - 1, _do13 = n; k <= _do13; k++, k_++ ) { 
          work[k_ + n] = -work[ki_ + n]*T(k_,ki_);
          work[k_ + n2] = -work[ki_ + 1 + n2]*T(k_,ki_ + 1);
        }
        
        //              Solve DComplex quasi-triangular system:
        //              ( T(KI+2,N:KI+2,N) - (WR-i*WI) )*X = WORK1+i*WORK2
        
        vmax = ONE;
        vcrit = bignum;
        
        jnxt = ki + 2;
        for( j = ki + 2, j_ = j - 1, _do14 = n; j <= _do14; j++, j_++ ) { 
          if( j < jnxt ) 
            goto L_200;
          j1 = j;
          j2 = j;
          jnxt = j + 1;
          if( j < n ) { 
            if( T(j_,j_ + 1) != ZERO ) { 
              j2 = j + 1;
              jnxt = j + 2;
            }
          }
          
          if( j1 == j2 ) { 
            
            //                    1-by-1 diagonal block
            
            //                    Scale if necessary to avoid overflow when
            //                    forming the right-hand side entries.
            
            if( work[j_] > vcrit ) { 
              rec = ONE/vmax;
              sscal( n - ki + 1, rec, &work[ki_ + n], 
               1 );
              sscal( n - ki + 1, rec, &work[ki_ + n2], 
               1 );
              vmax = ONE;
              vcrit = bignum;
            }
            
            work[j_ + n] = work[j_ + n] - sdot( j - ki - 
             2, &T(j_,ki_ + 2), 1, &work[ki_ + 2 + n], 
             1 );
            work[j_ + n2] = work[j_ + n2] - sdot( j - 
             ki - 2, &T(j_,ki_ + 2), 1, &work[ki_ + 2 + n2], 
             1 );
            
            //                    Solve (T(J,J)-(WR-i*WI))*(X11+i*X12)= WK+I*WK2
            
            slaln2( FALSE, 1, 2, smin, ONE, &T(j_,j_), 
             ldt, ONE, ONE, &work[j_ + n], n, wr, -wi, 
             (float*)x, 2, scale, xnorm, ierr );
            
            //                    Scale if necessary
            
            if( scale != ONE ) { 
              sscal( n - ki + 1, scale, &work[ki_ + n], 
               1 );
              sscal( n - ki + 1, scale, &work[ki_ + n2], 
               1 );
            }
            work[j_ + n] = x[0][0];
            work[j_ + n2] = x[1][0];
            vmax = ::vmax( abs( work[j_ + n] ), abs( work[j_ + n2] ), 
             vmax, FEND );
            vcrit = bignum/vmax;
            
          }
          else { 
            
            //                    2-by-2 diagonal block
            
            //                    Scale if necessary to avoid overflow when forming
            //                    the right-hand side entries.
            
            beta = max( work[j_], work[j_ + 1] );
            if( beta > vcrit ) { 
              rec = ONE/vmax;
              sscal( n - ki + 1, rec, &work[ki_ + n], 
               1 );
              sscal( n - ki + 1, rec, &work[ki_ + n2], 
               1 );
              vmax = ONE;
              vcrit = bignum;
            }
            
            work[j_ + n] = work[j_ + n] - sdot( j - ki - 
             2, &T(j_,ki_ + 2), 1, &work[ki_ + 2 + n], 
             1 );
            
            work[j_ + n2] = work[j_ + n2] - sdot( j - 
             ki - 2, &T(j_,ki_ + 2), 1, &work[ki_ + 2 + n2], 
             1 );
            
            work[j_ + 1 + n] = work[j_ + 1 + n] - sdot( j - 
             ki - 2, &T(j_ + 1,ki_ + 2), 1, &work[ki_ + 2 + n], 
             1 );
            
            work[j_ + 1 + n2] = work[j_ + 1 + n2] - sdot( j - 
             ki - 2, &T(j_ + 1,ki_ + 2), 1, &work[ki_ + 2 + n2], 
             1 );
            
            //                    Solve 2-by-2 DComplex linear equation
            //                      ([T(j,j)   T(j,j+1)  ]'-(wr-i*wi)*I)*X = SCALE*B
            //                      ([T(j+1,j) T(j+1,j+1)]             )
            
            slaln2( TRUE, 2, 2, smin, ONE, &T(j_,j_), 
             ldt, ONE, ONE, &work[j_ + n], n, wr, -wi, 
             (float*)x, 2, scale, xnorm, ierr );
            
            //                    Scale if necessary
            
            if( scale != ONE ) { 
              sscal( n - ki + 1, scale, &work[ki_ + n], 
               1 );
              sscal( n - ki + 1, scale, &work[ki_ + n2], 
               1 );
            }
            work[j_ + n] = x[0][0];
            work[j_ + n2] = x[1][0];
            work[j_ + 1 + n] = x[0][1];
            work[j_ + 1 + n2] = x[1][1];
            vmax = ::vmax( abs( x[0][0] ), abs( x[1][0] ), 
             abs( x[0][1] ), abs( x[1][1] ), vmax, FEND );
            vcrit = bignum/vmax;
            
          }
L_200:
          ;
        }
        
        //              Copy the vector x or Q*x to VL and normalize.
        
L_210:
        ;
        if( !over ) { 
          scopy( n - ki + 1, &work[ki_ + n], 1, &VL(is - 1,ki_), 
           1 );
          scopy( n - ki + 1, &work[ki_ + n2], 1, &VL(is,ki_), 
           1 );
          
          emax = ZERO;
          for( k = ki, k_ = k - 1, _do15 = n; k <= _do15; k++, k_++ ) { 
            emax = max( emax, abs( VL(is - 1,k_) ) + abs( VL(is,k_) ) );
          }
          remax = ONE/emax;
          sscal( n - ki + 1, remax, &VL(is - 1,ki_), 1 );
          sscal( n - ki + 1, remax, &VL(is,ki_), 1 );
          
          for( k = 1, k_ = k - 1, _do16 = ki - 1; k <= _do16; k++, k_++ ) { 
            VL(is - 1,k_) = ZERO;
            VL(is,k_) = ZERO;
          }
        }
        else { 
          if( ki < n - 1 ) { 
            sgemv( 'N', n, n - ki - 1, ONE, &VL(ki_ + 2,0), 
             ldvl, &work[ki_ + 2 + n], 1, work[ki_ + n], 
             &VL(ki_,0), 1 );
            sgemv( 'N', n, n - ki - 1, ONE, &VL(ki_ + 2,0), 
             ldvl, &work[ki_ + 2 + n2], 1, work[ki_ + 1 + n2], 
             &VL(ki_ + 1,0), 1 );
          }
          else { 
            sscal( n, work[ki_ + n], &VL(ki_,0), 1 );
            sscal( n, work[ki_ + 1 + n2], &VL(ki_ + 1,0), 
             1 );
          }
          
          emax = ZERO;
          for( k = 1, k_ = k - 1, _do17 = n; k <= _do17; k++, k_++ ) { 
            emax = max( emax, abs( VL(ki_,k_) ) + abs( VL(ki_ + 1,k_) ) );
          }
          remax = ONE/emax;
          sscal( n, remax, &VL(ki_,0), 1 );
          sscal( n, remax, &VL(ki_ + 1,0), 1 );
          
        }
        
      }
      
      is = is + 1;
      if( ip != 0 ) 
        is = is + 1;
L_250:
      ;
      if( ip == -1 ) 
        ip = 0;
      if( ip == 1 ) 
        ip = -1;
      
    }
    
  }
  
  return;
  
  //     End of STREVC
  
#undef  VR
#undef  VL
#undef  T
} // end of function 


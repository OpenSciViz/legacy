/*
 * C++ implementation of lapack routine dgetf2
 *
 * $Id: dgetf2.cpp,v 1.5 1993/04/06 20:40:42 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:34:38
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dgetf2.cpp,v $
 * Revision 1.5  1993/04/06  20:40:42  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:14:55  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:06:53  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ONE = 1.0e0;
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dgetf2(const long &m, const long &n, double *a, const long &lda, 
 long ipiv[], long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  long _do0, j, j_, jp;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DGETF2 computes an LU factorization of a general m-by-n matrix A
  //  using partial pivoting with row interchanges.
  
  //  The factorization has the form
  //     A = P * L * U
  //  where P is a permutation matrix, L is lower triangular with unit
  //  diagonal elements (lower trapezoidal if m > n), and U is upper
  //  triangular (upper trapezoidal if m < n).
  
  //  This is the right-looking Level 2 BLAS version of the algorithm.
  
  //  Arguments
  //  =========
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix A.  M >= 0.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix A.  N >= 0.
  
  //  A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)
  //          On entry, the m by n matrix to be factored.
  //          On exit, the factors L and U from the factorization
  //          A = P*L*U; the unit diagonal elements of L are not stored.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,M).
  
  //  IPIV    (output) INTEGER array, dimension (min(M,N))
  //          The pivot indices; for 1 <= i <= min(M,N), row i of the
  //          matrix was interchanged with row IPIV(i).
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: if INFO = k, U(k,k) is exactly zero. The factorization
  //               has been completed, but the factor U is exactly
  //               singular, and division by zero will occur if it is used
  //               to solve a system of equations.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  if( m < 0 ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( lda < max( 1, m ) ) { 
    info = -4;
  }
  if( info != 0 ) { 
    xerbla( "DGETF2", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( m == 0 || n == 0 ) 
    return;
  
  for( j = 1, j_ = j - 1, _do0 = min( m, n ); j <= _do0; j++, j_++ ) { 
    
    //        Find pivot and test for singularity.
    
    jp = j - 1 + idamax( m - j + 1, &A(j_,j_), 1 );
    ipiv[j_] = jp;
    if( A(j_,jp - 1) != ZERO ) { 
      
      //           Apply the interchange to columns 1:N.
      
      if( jp != j ) 
        dswap( n, &A(0,j_), lda, &A(0,jp - 1), lda );
      
      //           Compute elements J+1:M of J-th column.
      
      if( j < m ) 
        dscal( m - j, ONE/A(j_,j_), &A(j_,j_ + 1), 1 );
      
    }
    else if( info == 0 ) { 
      
      info = j;
    }
    
    if( j + 1 <= n ) { 
      
      //           Update trailing submatrix.
      
      dger( m - j, n - j, -ONE, &A(j_,j_ + 1), 1, &A(j_ + 1,j_), 
       lda, &A(j_ + 1,j_ + 1), lda );
    }
  }
  return;
  
  //     End of DGETF2
  
#undef  A
} // end of function 


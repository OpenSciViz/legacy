/*
 * C++ implementation of Lapack routine slapy3
 *
 * $Id: slapy3.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:00:09
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: slapy3.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL float /*FUNCTION*/ slapy3(const float &x, const float &y, const float &z)
{
  float slapy3_v, w, xabs, yabs, zabs;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SLAPY3 returns sqrt(x**2+y**2+z**2), taking care not to cause
  //  unnecessary overflow.
  
  //  Arguments
  //  =========
  
  //  X       (input) REAL
  //  Y       (input) REAL
  //  Z       (input) REAL
  //          X, Y and Z specify the values x, y and z.
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  xabs = abs( x );
  yabs = abs( y );
  zabs = abs( z );
  w = vmax( xabs, yabs, zabs, FEND );
  if( w == ZERO ) { 
    slapy3_v = ZERO;
  }
  else { 
    slapy3_v = w*sqrt( pow(xabs/w, 2) + pow(yabs/w, 2) + pow(zabs/
     w, 2) );
  }
  return( slapy3_v );
  
  //     End of SLAPY3
  
} // end of function 


/*
 * C++ implementation of Lapack routine ssyev
 *
 * $Id: ssyev.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:02:54
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: ssyev.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ZERO = 0.0e0;
const float ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ ssyev(const char &jobz, const char &uplo, const long &n, float *a, 
 const long &lda, float w[], float work[], const long &lwork, long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  int lower, wantz;
  long _do0, _do1, iinfo, imax, inde, indtau, indwrk, iscale, 
   j, j_, llwork, lopt;
  float anrm, bignum, eps, rmax, rmin, safmin, sigma, smlnum;

  
  //  -- LAPACK driver routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SSYEV  computes all eigenvalues and, optionally, eigenvectors of a
  //  real symmetric matrix A by calling the recommended sequence of LAPACK
  //  routines.
  
  //  Arguments
  //  =========
  
  //  JOBZ    (input) CHARACTER*1
  //          Specifies whether or not to compute the eigenvectors:
  //          = 'N':  Compute eigenvalues only.
  //          = 'V':  Compute eigenvalues and eigenvectors.
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangular part of the
  //          symmetric matrix A is stored:
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The number of rows and columns of the matrix A.  N >= 0.
  
  //  A       (input/output) REAL array, dimension (LDA, N)
  //          On entry, the symmetric matrix A.  If UPLO = 'U', only the
  //          upper triangular part of A is used to define the elements of
  //          the symmetric matrix.  If UPLO = 'L', only the lower
  //          triangular part of A is used to define the elements of the
  //          symmetric matrix.
  
  //          If JOBZ = 'V', then if INFO = 0 on exit, A contains the
  //          orthonormal eigenvectors of the matrix A.  If INFO > 0, A
  //          contains the eigenvectors associated with only the stored
  //          eigenvalues.
  //          If JOBZ = 'N', then on exit the lower triangle (if UPLO='L')
  //          or the upper triangle (if UPLO='U') of A, including the
  //          diagonal, is destroyed.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  W       (output) REAL array, dimension (N)
  //          On exit, if INFO = 0, W contains the eigenvalues in ascending
  //          order.  If INFO > 0, the eigenvalues are correct for indices
  //          1, 2, ..., INFO-1, but they are unordered and may not be the
  //          smallest eigenvalues of the matrix.
  
  //  WORK    (workspace) REAL array, dimension (LWORK)
  //          On exit, WORK(1) is set to the dimension of the work array
  //          needed to obtain optimal performance from this routine.
  //          See the description of LWORK below.
  
  //  LWORK   (input) INTEGER
  //          The length of the array WORK.  LWORK >= max(1,3*N-1).
  //          For optimal efficiency, LWORK should be at least (NB+2)*N,
  //          where NB is the blocksize for SSYTRD returned by ILAENV.
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit.
  //          < 0:  if INFO = -i, the i-th argument had an illegal value.
  //          > 0:  if INFO = +i, the algorithm terminated before finding
  //                the i-th eigenvalue.
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  wantz = lsame( jobz, 'V' );
  lower = lsame( uplo, 'L' );
  
  info = 0;
  if( !(wantz || lsame( jobz, 'N' )) ) { 
    info = -1;
  }
  else if( !(lower || lsame( uplo, 'U' )) ) { 
    info = -2;
  }
  else if( n < 0 ) { 
    info = -3;
  }
  else if( lda < max( 1, n ) ) { 
    info = -5;
  }
  else if( lwork < max( 1, 3*n - 1 ) ) { 
    info = -8;
  }
  
  if( info != 0 ) { 
    xerbla( "SSYEV ", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) { 
    work[0] = 1;
    return;
  }
  
  if( n == 1 ) { 
    w[0] = A(0,0);
    work[0] = 3;
    if( wantz ) 
      A(0,0) = ONE;
    return;
  }
  
  //     Get machine constants.
  
  safmin = slamch( 'S'/*Safe minimum*/ );
  eps = slamch( 'P'/*Precision*/ );
  smlnum = safmin/eps;
  bignum = ONE/smlnum;
  rmin = sqrt( smlnum );
  rmax = sqrt( bignum );
  
  //     Scale matrix to allowable range, if necessary.
  
  anrm = slansy( 'M', uplo, n, a, lda, work );
  iscale = 0;
  if( anrm > ZERO && anrm < rmin ) { 
    iscale = 1;
    sigma = rmin/anrm;
  }
  else if( anrm > rmax ) { 
    iscale = 1;
    sigma = rmax/anrm;
  }
  if( iscale == 1 ) { 
    if( lower ) { 
      for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
        sscal( n - j + 1, sigma, &A(j_,j_), 1 );
      }
    }
    else { 
      for( j = 1, j_ = j - 1, _do1 = n; j <= _do1; j++, j_++ ) { 
        sscal( j, sigma, &A(j_,0), 1 );
      }
    }
  }
  
  //     Call SSYTRD to reduce symmetric matrix to tridiagonal form.
  
  inde = 1;
  indtau = inde + n;
  indwrk = indtau + n;
  llwork = lwork - indwrk + 1;
  ssytrd( uplo, n, a, lda, w, &work[inde - 1], &work[indtau - 1], 
   &work[indwrk - 1], llwork, iinfo );
  lopt = 2*n + work[indwrk - 1];
  
  //     For eigenvalues only, call SSTERF.  For eigenvectors, first call
  //     SORGTR to generate the orthogonal matrix, then call SSTEQR.
  
  if( !wantz ) { 
    ssterf( n, w, &work[inde - 1], info );
  }
  else { 
    sorgtr( uplo, n, a, lda, &work[indtau - 1], &work[indwrk - 1], 
     llwork, iinfo );
    ssteqr( jobz, n, w, &work[inde - 1], a, lda, &work[indtau - 1], 
     info );
  }
  
  //     If matrix was scaled, then rescale eigenvalues appropriately.
  
  if( iscale == 1 ) { 
    if( info == 0 ) { 
      imax = n;
    }
    else { 
      imax = info - 1;
    }
    sscal( imax, ONE/sigma, w, 1 );
  }
  
  //     Set WORK(1) to optimal workspace size.
  
  work[0] = max( 3*n - 1, lopt );
  
  return;
  
  //     End of SSYEV
  
#undef  A
} // end of function 


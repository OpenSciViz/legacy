/*
 * C++ implementation of Lapack routine slartv
 *
 * $Id: slartv.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:00:33
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: slartv.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

RWLAPKDECL void /*FUNCTION*/ slartv(const long &n, float x[], const long &incx, float y[], 
   const long &incy, float c[], float s[], const long &incc)
{
  long _do0, i, i_, ic, ix, iy;
  float xi, yi;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SLARTV applies a vector of real plane rotations to elements of the
  //  real vectors x and y. For i = 1,2,...,n
  
  //     ( x(i) ) := (  c(i)  s(i) ) ( x(i) )
  //     ( y(i) )    ( -s(i)  c(i) ) ( y(i) )
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The number of plane rotations to be applied.
  
  //  X       (input/output) REAL array,
  //                         dimension (1+(N-1)*INCX)
  //          The vector x.
  
  //  INCX    (input) INTEGER
  //          The increment between elements of X. INCX > 0.
  
  //  Y       (input/output) REAL array,
  //                         dimension (1+(N-1)*INCY)
  //          The vector y.
  
  //  INCY    (input) INTEGER
  //          The increment between elements of Y. INCY > 0.
  
  //  C       (input) REAL array, dimension (1+(N-1)*INCC)
  //          The cosines of the plane rotations.
  
  //  S       (input) REAL array, dimension (1+(N-1)*INCC)
  //          The sines of the plane rotations.
  
  //  INCC    (input) INTEGER
  //          The increment between elements of C and S. INCC > 0.
  
  //  =====================================================================
  
  //     .. Local Scalars ..
  //     ..
  //     .. Executable Statements ..
  
  ix = 1;
  iy = 1;
  ic = 1;
  for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
    xi = x[ix - 1];
    yi = y[iy - 1];
    x[ix - 1] = c[ic - 1]*xi + s[ic - 1]*yi;
    y[iy - 1] = c[ic - 1]*yi - s[ic - 1]*xi;
    ix = ix + incx;
    iy = iy + incy;
    ic = ic + incc;
  }
  return;
  
  //     End of SLARTV
  
} // end of function 


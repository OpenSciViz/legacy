/*
 * C++ implementation of Lapack routine slaqsb
 *
 * $Id: slaqsb.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:00:13
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: slaqsb.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ONE = 1.0e0;
const float THRESH = 0.1e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ slaqsb(const char &uplo, long &n, long &kd, float *ab, 
 long &ldab, float s[], float &scond, float &amax, char &equed)
{
#define AB(I_,J_) (*(ab+(I_)*(ldab)+(J_)))
  long _do0, _do1, _do2, _do3, i, i_, j, j_;
  float cj, large, small;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SLAQSB equilibrates a symmetric band matrix A using the scaling
  //  factors in the vector S.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangular part of the
  //          symmetric matrix A is stored.
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  KD      (input) INTEGER
  //          The number of super-diagonals of the matrix A if UPLO = 'U',
  //          or the number of sub-diagonals if UPLO = 'L'.  KD >= 0.
  
  //  AB      (input/output) REAL array, dimension (LDAB,N)
  //          On entry, the upper or lower triangle of the symmetric band
  //          matrix A, stored in the first KD+1 rows of the array.  The
  //          j-th column of A is stored in the j-th column of the array AB
  //          as follows:
  //          if UPLO = 'U', AB(kd+1+i-j,j) = A(i,j) for max(1,j-kd)<=i<=j;
  //          if UPLO = 'L', AB(1+i-j,j)    = A(i,j) for j<=i<=min(n,j+kd).
  
  //          On exit, if INFO = 0, the triangular factor U or L from the
  //          Cholesky factorization A = U'*U or A = L*L' of the band
  //          matrix A, in the same storage format as A.
  
  //  LDAB    (input) INTEGER
  //          The leading dimension of the array AB.  LDAB >= KD+1.
  
  //  S       (output) REAL array, dimension (N)
  //          The scale factors for A.
  
  //  SCOND   (input) REAL
  //          Ratio of the smallest S(i) to the largest S(i).
  
  //  AMAX    (input) REAL
  //          Absolute value of largest matrix entry.
  
  //  EQUED   (output) CHARACTER*1
  //          Specifies whether or not equilibration was done.
  //          = 'N':  No equilibration.
  //          = 'Y':  Equilibration was done, i.e., A has been replaced by
  //                  diag(S) * A * diag(S).
  
  //  Internal Parameters
  //  ===================
  
  //  THRESH is a threshold value used to decide if scaling should be done
  //  based on the ratio of the scaling factors.  If SCOND < THRESH,
  //  scaling is done.
  
  //  LARGE and SMALL are threshold values used to decide if scaling should
  //  be done based on the absolute size of the largest matrix element.
  //  If AMAX > LARGE or AMAX < SMALL, scaling is done.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Quick return if possible
  
  if( n <= 0 ) { 
    equed = 'N';
    return;
  }
  
  //     Initialize LARGE and SMALL.
  
  small = slamch( 'S'/*Safe minimum*/ )/slamch( 'P'/*Precision*/
    );
  large = ONE/small;
  
  if( (scond >= THRESH && amax >= small) && amax <= large ) { 
    
    //        No equilibration
    
    equed = 'N';
  }
  else { 
    
    //        Replace A by diag(S) * A * diag(S).
    
    if( lsame( uplo, 'U' ) ) { 
      
      //           Upper triangle of A is stored in band format.
      
      for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
        cj = s[j_];
        for( i = max( 1, j - kd ), i_ = i - 1, _do1 = j; i <= _do1; i++, i_++ ) { 
          AB(j_,kd + 1 + i_ - j) = cj*s[i_]*AB(j_,kd + 1 + i_ - j);
        }
      }
    }
    else { 
      
      //           Lower triangle of A is stored.
      
      for( j = 1, j_ = j - 1, _do2 = n; j <= _do2; j++, j_++ ) { 
        cj = s[j_];
        for( i = j, i_ = i - 1, _do3 = min( n, j + kd ); i <= _do3; i++, i_++ ) { 
          AB(j_,1 + i_ - j) = cj*s[i_]*AB(j_,1 + i_ - j);
        }
      }
    }
    equed = 'Y';
  }
  
  return;
  
  //     End of SLAQSB
  
#undef  AB
} // end of function 


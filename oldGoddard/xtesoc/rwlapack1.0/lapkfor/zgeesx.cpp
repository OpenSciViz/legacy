/*
 * C++ implementation of Lapack routine zgeesx
 *
 * $Id: zgeesx.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:46:13
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zgeesx.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zgeesx(const char &jobvs, const char &sort, int (*select)(DComplex[]), 
 const char &sense, const long &n, DComplex *a, const long &lda, const long &sdim, 
 DComplex w[], DComplex *vs, const long &ldvs, const double &rconde, const double &rcondv, 
 DComplex work[], const long &lwork, double rwork[], int bwork[], 
 long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define VS(I_,J_) (*(vs+(I_)*(ldvs)+(J_)))
  int scalea, wantsb, wantse, wantsn, wantst, wantsv, wantvs;
  long hswork, i, ibal, icond, ierr, ieval, ihi, ilo, itau, 
   iwrk, k, maxb, maxwrk, minwrk;
  double anrm, bignum, cscale, dum[1], eps, smlnum;

  
  //  -- LAPACK driver routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  //     .. Function Arguments ..

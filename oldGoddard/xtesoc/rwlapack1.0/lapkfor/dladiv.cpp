/*
 * C++ implementation of lapack routine dladiv
 *
 * $Id: dladiv.cpp,v 1.5 1993/04/06 20:40:53 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:35:00
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dladiv.cpp,v $
 * Revision 1.5  1993/04/06  20:40:53  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:15:08  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:07:06  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

RWLAPKDECL void /*FUNCTION*/ dladiv(const double &a, const double &b, const double &c, const double &d, 
   double &p, double &q)
{
  double e, f;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DLADIV performs DComplex division in  real arithmetic
  
  //                        a + i*b
  //             p + i*q = ---------
  //                        c + i*d
  
  //  The algorithm is due to Robert L. Smith and can be found
  //  in D. Knuth, The art of Computer Programming, Vol.2, p.195
  
  //  Arguments
  //  =========
  
  //  A       (input) DOUBLE PRECISION
  //  B       (input) DOUBLE PRECISION
  //  C       (input) DOUBLE PRECISION
  //  D       (input) DOUBLE PRECISION
  //          The scalars a, b, c, and d in the above expression.
  
  //  P       (output) DOUBLE PRECISION
  //  Q       (output) DOUBLE PRECISION
  //          The scalars p and q in the above expression.
  
  //     .. Local Scalars ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  if( abs( d ) < abs( c ) ) {
    e = d/c;
    f = c + d*e;
    p = (a + b*e)/f;
    q = (b - a*e)/f;
  }
  else { 
    e = c/d;
    f = d + c*e;
    p = (b + a*e)/f;
    q = (-a + b*e)/f;
  }
  
  return;
  
  //     End of DLADIV
  
} // end of function 


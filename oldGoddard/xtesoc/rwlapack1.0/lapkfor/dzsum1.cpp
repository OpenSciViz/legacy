/*
 * C++ implementation of lapack routine dzsum1
 *
 * $Id: dzsum1.cpp,v 1.4 1993/04/06 20:42:54 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:39:19
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dzsum1.cpp,v $
 * Revision 1.4  1993/04/06  20:42:54  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.3  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.2  1993/03/05  23:17:59  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:09:34  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

RWLAPKDECL double /*FUNCTION*/ dzsum1(const long &n, DComplex cx[], const long &incx)
{
  long _do0, _do1, _do2, i, i_, nincx;
  double dzsum1_v, stemp;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DZSUM1 takes the sum of the absolute values of a DComplex
  //  vector and returns a double precision result.
  
  //  Based on DZASUM from the Level 1 BLAS.
  //  The change is to use the 'genuine' absolute value.
  
  //  Contributed by Nick Higham for use with ZLACON.
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The number of elements in the vector CX.
  
  //  CX      (input) COMPLEX*16 array, dimension (N)
  //          The vector whose elements will be summed.
  
  //  INCX    (input) INTEGER
  //          The spacing between successive values of CX.  INCX > 0.
  
  //     .. Local Scalars ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  dzsum1_v = 0.0e0;
  stemp = 0.0e0;
  if( n <= 0 ) 
    return( dzsum1_v );
  if( incx == 1 ) 
    goto L_20;
  
  //     CODE FOR INCREMENT NOT EQUAL TO 1
  
  nincx = n*incx;
  for( i = 1, i_ = i - 1, _do0=docnt(i,nincx,_do1 = incx); _do0 > 0; i += _do1, i_ += _do1, _do0-- ) { 
    
    //        NEXT LINE MODIFIED.
    
    stemp = stemp + abs( cx[i_] );
  }
  dzsum1_v = stemp;
  return( dzsum1_v );
  
  //     CODE FOR INCREMENT EQUAL TO 1
  
L_20:
  ;
  for( i = 1, i_ = i - 1, _do2 = n; i <= _do2; i++, i_++ ) { 
    
    //        NEXT LINE MODIFIED.
    
    stemp = stemp + abs( cx[i_] );
  }
  dzsum1_v = stemp;
  return( dzsum1_v );
  
  //     End of DZSUM1
  
} // end of function 


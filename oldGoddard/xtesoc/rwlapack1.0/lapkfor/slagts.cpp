/*
 * C++ implementation of Lapack routine slagts
 *
 * $Id: slagts.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:59:40
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: slagts.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ONE = 1.0e0;
const float ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ slagts(const long &job, const long &n, float a[], float b[], 
 float c[], float d[], long in[], float y[], float &tol, long &info)
{
  long _do0, _do1, _do2, _do3, k, k_;
  float absak, ak, bignum, eps, pert, sfmin, temp;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SLAGTS may be used to solve one of the systems of equations
  
  //     (T - lambda*I)*x = y   or   (T - lambda*I)'*x = y,
  
  //  where T is an n by n tridiagonal matrix, for x, following the
  //  factorization of (T - lambda*I) as
  
  //     (T - lambda*I) = P*L*U ,
  
  //  by routine SLAGTF. The choice of equation to be solved is
  //  controlled by the argument JOB, and in each case there is an option
  //  to perturb zero or very small diagonal elements of U, this option
  //  being intended for use in applications such as inverse iteration.
  
  //  Arguments
  //  =========
  
  //  JOB     (input) INTEGER
  //          Specifies the job to be performed by SLAGTS as follows:
  //          =  1: The equations  (T - lambda*I)x = y  are to be solved,
  //                but diagonal elements of U are not to be perturbed.
  //          = -1: The equations  (T - lambda*I)x = y  are to be solved
  //                and, if overflow would otherwise occur, the diagonal
  //                elements of U are to be perturbed. See argument TOL
  //                below.
  //          =  2: The equations  (T - lambda*I)'x = y  are to be solved,
  //                but diagonal elements of U are not to be perturbed.
  //          = -2: The equations  (T - lambda*I)'x = y  are to be solved
  //                and, if overflow would otherwise occur, the diagonal
  //                elements of U are to be perturbed. See argument TOL
  //                below.
  
  //  N       (input) INTEGER
  //          The order of the matrix T.
  
  //  A       (input) REAL array, dimension (N)
  //          On entry, A must contain the diagonal elements of U as
  //          returned from SLAGTF.
  
  //  B       (input) REAL array, dimension (N-1)
  //          On entry, B must contain the first super-diagonal elements of
  //          U as returned from SLAGTF.
  
  //  C       (input) REAL array, dimension (N-1)
  //          On entry, C must contain the sub-diagonal elements of L as
  //          returned from SLAGTF.
  
  //  D       (input) REAL array, dimension (N-2)
  //          On entry, D must contain the second super-diagonal elements
  //          of U as returned from SLAGTF.
  
  //  IN      (input) INTEGER array, dimension (N)
  //          On entry, IN must contain details of the matrix P as returned
  //          from SLAGTF.
  
  //  Y       (input/output) REAL array, dimension (N)
  //          On entry, the right hand side vector y.
  
  //          On exit, Y is overwritten by the solution vector x.
  
  //  TOL     (input/output) REAL
  //          On entry with  JOB .lt. 0, TOL should be the minimum
  //          perturbation to be made to very small diagonal elements of U.
  //          TOL should normally be chosen as about eps*norm(U), where eps
  //          is the relative machine precision, but if TOL is supplied as
  //          non-positive, then it is reset to eps*max( abs( u(i,j) ) ).
  //          If  JOB .gt. 0  then TOL is not referenced.
  
  //          On exit, TOL is changed as described above, only if TOL is
  //          non-positive on entry. Otherwise TOL is unchanged.
  
  //  INFO    (output)
  //          = 0   : successful exit
  //          .lt. 0: if INFO = -k, the kth argument had an illegal value
  //          .gt. 0: overflow would occur when computing the INFO(th)
  //                  element of the solution vector x. This can only occur
  //                  when JOB is supplied as positive and either means
  //                  that a diagonal element of U is very small, or that
  //                  the elements of the right-hand side vector y are very
  //                  large.
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Executable Statements ..
  
  info = 0;
  if( (abs( job ) > 2) || (job == 0) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  if( info != 0 ) { 
    xerbla( "SLAGTS", -info );
    return;
  }
  
  if( n == 0 ) 
    return;
  
  eps = slamch( 'E'/*Epsilon*/ );
  sfmin = slamch( 'S'/*Safe minimum*/ );
  bignum = ONE/sfmin;
  
  if( job < 0 ) { 
    if( tol <= ZERO ) { 
      tol = abs( a[0] );
      if( n > 1 ) 
        tol = vmax( tol, abs( a[1] ), abs( b[0] ), FEND );
      for( k = 3, k_ = k - 1, _do0 = n; k <= _do0; k++, k_++ ) { 
        tol = vmax( tol, abs( a[k_] ), abs( b[k_ - 1] ), abs( d[k_ - 2] ), 
         FEND );
      }
      tol = tol*eps;
      if( tol == ZERO ) 
        tol = eps;
    }
  }
  
  if( abs( job ) == 1 ) { 
    for( k = 2, k_ = k - 1, _do1 = n; k <= _do1; k++, k_++ ) { 
      if( in[k_ - 1] == 0 ) { 
        y[k_] = y[k_] - c[k_ - 1]*y[k_ - 1];
      }
      else { 
        temp = y[k_ - 1];
        y[k_ - 1] = y[k_];
        y[k_] = temp - c[k_ - 1]*y[k_];
      }
    }
    if( job == 1 ) { 
      for( k = n, k_ = k - 1; k >= 1; k--, k_-- ) { 
        if( k <= n - 2 ) { 
          temp = y[k_] - b[k_]*y[k_ + 1] - d[k_]*y[k_ + 2];
        }
        else if( k == n - 1 ) { 
          temp = y[k_] - b[k_]*y[k_ + 1];
        }
        else { 
          temp = y[k_];
        }
        ak = a[k_];
        absak = abs( ak );
        if( absak < ONE ) { 
          if( absak < sfmin ) { 
            if( absak == ZERO || abs( temp )*sfmin > absak ) { 
              info = k;
              return;
            }
            else { 
              temp = temp*bignum;
              ak = ak*bignum;
            }
          }
          else if( abs( temp ) > absak*bignum ) { 
            info = k;
            return;
          }
        }
        y[k_] = temp/ak;
      }
    }
    else { 
      for( k = n, k_ = k - 1; k >= 1; k--, k_-- ) { 
        if( k <= n - 2 ) { 
          temp = y[k_] - b[k_]*y[k_ + 1] - d[k_]*y[k_ + 2];
        }
        else if( k == n - 1 ) { 
          temp = y[k_] - b[k_]*y[k_ + 1];
        }
        else { 
          temp = y[k_];
        }
        ak = a[k_];
        pert = sign( tol, ak );
L_40:
        ;
        absak = abs( ak );
        if( absak < ONE ) { 
          if( absak < sfmin ) { 
            if( absak == ZERO || abs( temp )*sfmin > absak ) { 
              ak = ak + pert;
              pert = 2*pert;
              goto L_40;
            }
            else { 
              temp = temp*bignum;
              ak = ak*bignum;
            }
          }
          else if( abs( temp ) > absak*bignum ) { 
            ak = ak + pert;
            pert = 2*pert;
            goto L_40;
          }
        }
        y[k_] = temp/ak;
      }
    }
  }
  else { 
    
    //        Come to here if  JOB = 2 or -2
    
    if( job == 2 ) { 
      for( k = 1, k_ = k - 1, _do2 = n; k <= _do2; k++, k_++ ) { 
        if( k >= 3 ) { 
          temp = y[k_] - b[k_ - 1]*y[k_ - 1] - d[k_ - 2]*
           y[k_ - 2];
        }
        else if( k == 2 ) { 
          temp = y[k_] - b[k_ - 1]*y[k_ - 1];
        }
        else { 
          temp = y[k_];
        }
        ak = a[k_];
        absak = abs( ak );
        if( absak < ONE ) { 
          if( absak < sfmin ) { 
            if( absak == ZERO || abs( temp )*sfmin > absak ) { 
              info = k;
              return;
            }
            else { 
              temp = temp*bignum;
              ak = ak*bignum;
            }
          }
          else if( abs( temp ) > absak*bignum ) { 
            info = k;
            return;
          }
        }
        y[k_] = temp/ak;
      }
    }
    else { 
      for( k = 1, k_ = k - 1, _do3 = n; k <= _do3; k++, k_++ ) { 
        if( k >= 3 ) { 
          temp = y[k_] - b[k_ - 1]*y[k_ - 1] - d[k_ - 2]*
           y[k_ - 2];
        }
        else if( k == 2 ) { 
          temp = y[k_] - b[k_ - 1]*y[k_ - 1];
        }
        else { 
          temp = y[k_];
        }
        ak = a[k_];
        pert = sign( tol, ak );
L_70:
        ;
        absak = abs( ak );
        if( absak < ONE ) { 
          if( absak < sfmin ) { 
            if( absak == ZERO || abs( temp )*sfmin > absak ) { 
              ak = ak + pert;
              pert = 2*pert;
              goto L_70;
            }
            else { 
              temp = temp*bignum;
              ak = ak*bignum;
            }
          }
          else if( abs( temp ) > absak*bignum ) { 
            ak = ak + pert;
            pert = 2*pert;
            goto L_70;
          }
        }
        y[k_] = temp/ak;
      }
    }
    
    for( k = n, k_ = k - 1; k >= 2; k--, k_-- ) { 
      if( in[k_ - 1] == 0 ) { 
        y[k_ - 1] = y[k_ - 1] - c[k_ - 1]*y[k_];
      }
      else { 
        temp = y[k_ - 1];
        y[k_ - 1] = y[k_];
        y[k_] = temp - c[k_ - 1]*y[k_];
      }
    }
  }
  
  //     End of SLAGTS
  
  return;
} // end of function 


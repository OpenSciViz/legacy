/*
 * C++ implementation of Lapack routine zhegs2
 *
 * $Id: zhegs2.cpp,v 1.2 1993/07/21 22:22:14 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:47:21
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zhegs2.cpp,v $
 * Revision 1.2  1993/07/21  22:22:14  alv
 * ported to Microsoft visual C++
 *
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ONE = 1.0e0;
const double HALF = 0.5e0;
const DComplex CONE = DComplex(1.0e0);
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zhegs2(const long &itype, const char &uplo, const long &n, 
 DComplex *a, const long &lda, DComplex *b, const long &ldb, long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
  int upper;
  long _do0, _do1, _do2, _do3, k, k_;
  double akk, bkk;
  DComplex ct;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZHEGS2 reduces a DComplex Hermitian-definite generalized
  //  eigenproblem to standard form.
  
  //  If ITYPE = 1, the problem is A*x = lambda*B*x,
  //  and A is overwritten by inv(U')*A*inv(U) or inv(L)*A*inv(L')
  
  //  If ITYPE = 2 or 3, the problem is A*B*x = lambda*x or
  //  B*A*x = lambda*x, and A is overwritten by U*A*U` or L'*A*L.
  
  //  B must have been previously factorized as U'*U or L*L' by ZPOTRF.
  
  //  Arguments
  //  =========
  
  //  ITYPE   (input) INTEGER
  //          = 1: compute inv(U')*A*inv(U) or inv(L)*A*inv(L');
  //          = 2 or 3: compute U*A*U' or L'*A*L.
  
  //  UPLO    (input) CHARACTER
  //          Specifies whether the upper or lower triangular part of the
  //          Hermitian matrix A is stored, and how B has been factorized.
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The order of the matrices A and B.  N >= 0.
  
  //  A       (input/output) COMPLEX*16 array, dimension (LDA,N)
  //          On entry, the Hermitian matrix A.  If UPLO = 'U', the leading
  //          n by n upper triangular part of A contains the upper
  //          triangular part of the matrix A, and the strictly lower
  //          triangular part of A is not referenced.  If UPLO = 'L', the
  //          leading n by n lower triangular part of A contains the lower
  //          triangular part of the matrix A, and the strictly upper
  //          triangular part of A is not referenced.
  
  //          On exit, if INFO = 0, the transformed matrix, stored in the
  //          same format as A.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  B       (input) COMPLEX*16 array, dimension (LDB,N)
  //          The triangular factor from the Cholesky factorization of B,
  //          as returned by ZPOTRF.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit.
  //          < 0:  if INFO = -i, the i-th argument had an illegal value.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  if( itype < 1 || itype > 3 ) { 
    info = -1;
  }
  else if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -2;
  }
  else if( n < 0 ) { 
    info = -3;
  }
  else if( lda < max( 1, n ) ) { 
    info = -5;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -7;
  }
  if( info != 0 ) { 
    xerbla( "ZHEGS2", -info );
    return;
  }
  
  if( itype == 1 ) { 
    if( upper ) { 
      
      //           Compute inv(U')*A*inv(U)
      
      for( k = 1, k_ = k - 1, _do0 = n; k <= _do0; k++, k_++ ) { 
        
        //              Update the upper triangle of A(k:n,k:n)
        
        akk = real(A(k_,k_));
        bkk = real(B(k_,k_));
        akk = akk/pow(bkk, 2);
        A(k_,k_) = (DComplex)(akk);
        if( k < n ) { 
          zdscal( n - k, ONE/bkk, &A(k_ + 1,k_), lda );
          ct = DComplex(-HALF*akk);
          zlacgv( n - k, &A(k_ + 1,k_), lda );
          zlacgv( n - k, &B(k_ + 1,k_), ldb );
          zaxpy( n - k, ct, &B(k_ + 1,k_), ldb, &A(k_ + 1,k_), 
           lda );
          zher2( uplo, n - k, -(CONE), &A(k_ + 1,k_), lda, 
           &B(k_ + 1,k_), ldb, &A(k_ + 1,k_ + 1), lda );
          zaxpy( n - k, ct, &B(k_ + 1,k_), ldb, &A(k_ + 1,k_), 
           lda );
          zlacgv( n - k, &B(k_ + 1,k_), ldb );
          ztrsv( uplo, 'C'/*Conjugate transpose*/, 'N'/*Non-unit*/
           , n - k, &B(k_ + 1,k_ + 1), ldb, &A(k_ + 1,k_), 
           lda );
          zlacgv( n - k, &A(k_ + 1,k_), lda );
        }
      }
    }
    else { 
      
      //           Compute inv(L)*A*inv(L')
      
      for( k = 1, k_ = k - 1, _do1 = n; k <= _do1; k++, k_++ ) { 
        
        //              Update the lower triangle of A(k:n,k:n)
        
        akk = real(A(k_,k_));
        bkk = real(B(k_,k_));
        akk = akk/pow(bkk, 2);
        A(k_,k_) = (DComplex)(akk);
        if( k < n ) { 
          zdscal( n - k, ONE/bkk, &A(k_,k_ + 1), 1 );
          ct = DComplex(-HALF*akk);
          zaxpy( n - k, ct, &B(k_,k_ + 1), 1, &A(k_,k_ + 1), 
           1 );
          zher2( uplo, n - k, -(CONE), &A(k_,k_ + 1), 1, 
           &B(k_,k_ + 1), 1, &A(k_ + 1,k_ + 1), lda );
          zaxpy( n - k, ct, &B(k_,k_ + 1), 1, &A(k_,k_ + 1), 
           1 );
          ztrsv( uplo, 'N'/*No transpose*/, 'N'/*Non-unit*/
           , n - k, &B(k_ + 1,k_ + 1), ldb, &A(k_,k_ + 1), 
           1 );
        }
      }
    }
  }
  else { 
    if( upper ) { 
      
      //           Compute U*A*U'
      
      for( k = 1, k_ = k - 1, _do2 = n; k <= _do2; k++, k_++ ) { 
        
        //              Update the upper triangle of A(1:k,1:k)
        
        akk = real(A(k_,k_));
        bkk = real(B(k_,k_));
        ztrmv( uplo, 'N'/*No transpose*/, 'N'/*Non-unit*/
         , k - 1, b, ldb, &A(k_,0), 1 );
        ct = DComplex(HALF*akk);
        zaxpy( k - 1, ct, &B(k_,0), 1, &A(k_,0), 1 );
        zher2( uplo, k - 1, CONE, &A(k_,0), 1, &B(k_,0), 1, 
         a, lda );
        zaxpy( k - 1, ct, &B(k_,0), 1, &A(k_,0), 1 );
        zdscal( k - 1, bkk, &A(k_,0), 1 );
        A(k_,k_) = (DComplex)(akk*pow(bkk, 2));
      }
    }
    else { 
      
      //           Compute L'*A*L
      
      for( k = 1, k_ = k - 1, _do3 = n; k <= _do3; k++, k_++ ) { 
        
        //              Update the lower triangle of A(1:k,1:k)
        
        akk = real(A(k_,k_));
        bkk = real(B(k_,k_));
        zlacgv( k - 1, &A(0,k_), lda );
        ztrmv( uplo, 'C'/*Conjugate transpose*/, 'N'/*Non-unit*/
         , k - 1, b, ldb, &A(0,k_), lda );
        ct = DComplex(HALF*akk);
        zlacgv( k - 1, &B(0,k_), ldb );
        zaxpy( k - 1, ct, &B(0,k_), ldb, &A(0,k_), lda );
        zher2( uplo, k - 1, CONE, &A(0,k_), lda, &B(0,k_), 
         ldb, a, lda );
        zaxpy( k - 1, ct, &B(0,k_), ldb, &A(0,k_), lda );
        zlacgv( k - 1, &B(0,k_), ldb );
        zdscal( k - 1, bkk, &A(0,k_), lda );
        zlacgv( k - 1, &A(0,k_), lda );
        A(k_,k_) = (DComplex)(akk*pow(bkk, 2));
      }
    }
  }
  return;
  
  //     End of ZHEGS2
  
#undef  B
#undef  A
} // end of function 


/*
 * C++ implementation of Lapack routine sstev
 *
 * $Id: sstev.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:02:49
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: sstev.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ZERO = 0.0e0;
const float ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ sstev(const char &jobz, const long &n, float d[], float e[], 
 float *z, const long &ldz, float work[], long &info)
{
#define Z(I_,J_)  (*(z+(I_)*(ldz)+(J_)))
  int wantz;
  long imax, imaxd, imaxe, iscale;
  float bignum, eps, rmax, rmin, safmin, sigma, smlnum, tnrm;

  
  //  -- LAPACK driver routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SSTEV  computes all eigenvalues and, optionally, eigenvectors of a
  //  real symmetric tridiagonal matrix by calling the recommended sequence
  //  of LAPACK routines.
  
  //  Arguments
  //  =========
  
  //  JOBZ    (input) CHARACTER*1
  //          Specifies whether or not to compute the eigenvectors:
  //          = 'N':  Compute eigenvalues only.
  //          = 'V':  Compute eigenvalues and eigenvectors.
  
  //  N       (input) INTEGER
  //          The number of rows and columns in the matrix.  N >= 0.
  
  //  D       (input/output) REAL array, dimension (N)
  //          On entry, D contains the diagonal elements of the
  //          tridiagonal matrix.
  //          On exit, if INFO = 0, D contains the eigenvalues in ascending
  //          order.  If INFO > 0, the eigenvalues are correct for indices
  //          1, 2, ..., INFO-1, but they are unordered and may not be the
  //          smallest eigenvalues of the matrix.
  
  //  E       (input/workspace) REAL array, dimension (N-1)
  //          On entry, E contains the subdiagonal elements of the
  //          tridiagonal matrix in positions 1 through N-1.
  //          On exit, the contents of E are destroyed.
  
  //  Z       (output) REAL array, dimension (LDZ, N)
  //          If JOBZ = 'V', then if INFO = 0 on exit, Z contains the
  //          orthonormal eigenvectors of the matrix.  If INFO > 0, Z
  //          contains the eigenvectors associated with only the stored
  //          eigenvalues.
  //          If JOBZ = 'N', then Z is not referenced.
  
  //  LDZ     (input) INTEGER
  //          The leading dimension of the array Z.  LDZ >= 1, and if
  //          JOBZ = 'V', LDZ >= max(1,N).
  
  //  WORK    (workspace) REAL array, dimension (max(1,2*N-2))
  //          If JOBZ = 'N', WORK is not referenced.
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit.
  //          < 0:  if INFO = -i, the i-th argument had an illegal value.
  //          > 0:  if INFO = +i, the algorithm terminated before finding
  //                the i-th eigenvalue.
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  wantz = lsame( jobz, 'V' );
  
  info = 0;
  if( !(wantz || lsame( jobz, 'N' )) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( ldz < 1 || (wantz && ldz < n) ) { 
    info = -6;
  }
  
  if( info != 0 ) { 
    xerbla( "SSTEV ", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  if( n == 1 ) { 
    if( wantz ) 
      Z(0,0) = ONE;
    return;
  }
  
  //     Get machine constants.
  
  safmin = slamch( 'S'/*Safe minimum*/ );
  eps = slamch( 'P'/*Precision*/ );
  smlnum = safmin/eps;
  bignum = ONE/smlnum;
  rmin = sqrt( smlnum );
  rmax = sqrt( bignum );
  
  //     Scale matrix to allowable range, if necessary.
  
  iscale = 0;
  imaxd = isamax( n, d, 1 );
  imaxe = isamax( n - 1, e, 1 );
  tnrm = max( abs( d[imaxd - 1] ), abs( e[imaxe - 1] ) );
  if( tnrm > ZERO && tnrm < rmin ) { 
    iscale = 1;
    sigma = rmin/tnrm;
  }
  else if( tnrm > rmax ) { 
    iscale = 1;
    sigma = rmax/tnrm;
  }
  if( iscale == 1 ) { 
    sscal( n, sigma, d, 1 );
    sscal( n - 1, sigma, &e[0], 1 );
  }
  
  //     For eigenvalues only, call SSTERF.  For eigenvalues and
  //     eigenvectors, call SSTEQR.
  
  if( !wantz ) { 
    ssterf( n, d, e, info );
  }
  else { 
    ssteqr( 'I', n, d, e, z, ldz, work, info );
  }
  
  //     If matrix was scaled, then rescale eigenvalues appropriately.
  
  if( iscale == 1 ) { 
    if( info == 0 ) { 
      imax = n;
    }
    else { 
      imax = info - 1;
    }
    sscal( imax, ONE/sigma, d, 1 );
  }
  
  return;
  
  //     End of SSTEV
  
#undef  Z
} // end of function 


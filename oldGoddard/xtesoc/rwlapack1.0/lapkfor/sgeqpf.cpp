/*
 * C++ implementation of Lapack routine sgeqpf
 *
 * $Id: sgeqpf.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:58:45
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: sgeqpf.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ZERO = 0.0e0;
const float ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ sgeqpf(const long &m, const long &n, float *a, const long &lda, 
 long jpvt[], float tau[], float work[], long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  long _do0, _do1, _do2, _do3, i, i_, itemp, j, j_, ma, mn, 
   pvt;
  float aii, temp, temp2;

  
  //  -- LAPACK test routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SGEQPF computes a QR factorization with column pivoting of a
  //  real m by n matrix A: A*P = Q*R
  
  //  Arguments
  //  =========
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix A. M >= 0.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix A. N >= 0
  
  //  A       (input/output) REAL array, dimension (LDA,N)
  //          On entry, the m by n matrix A.
  //          On exit, the upper triangle of the array contains the
  //          min(m,n) by n upper triangular matrix R; the elements
  //          below the diagonal, together with the array TAU,
  //          represent the orthogonal matrix Q as a product of
  //          min(m,n) elementary reflectors.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A. LDA >= max(1,M).
  
  //  JPVT    (input/output) INTEGER array, dimension (N)
  //          on entry: If JPVT(I) <> 0, column I of A is permuted
  //          to the front of AP (a leading column)
  //          IF JPVT(I) == 0, column I of A is a free column.
  //          on exit: If JPVT(I) = K, then the Ith column of AP
  //          was the Kth column of A.
  
  //  TAU     (output) REAL array, dimension (min(M,N))
  //          Stores further details of
  //          the orthogonal matrix Q (see A).
  
  //  WORK    (workspace) REAL array, dimension (3*N)
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument had an illegal value
  
  //  Further Details
  //  ===============
  
  //  The matrix Q is represented as a product of elementary reflectors
  
  //     Q = H(1) H(2) . . . H(n)
  
  //  Each H(i) has the form
  
  //     H = I - tau * v * v'
  
  //  where tau is a real scalar, and v is a real vector with
  //  v(1:i-1) = 0 and v(i) = 1; v(i+1:m) is stored on exit in A(i+1:m,i).
  
  //  The matrix P is represented in jpvt as follows: If
  //     jpvt(j) = i
  //  then the jth column of P is the ith canonical unit vector.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input arguments
  
  info = 0;
  if( m < 0 ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( lda < max( 1, m ) ) { 
    info = -4;
  }
  if( info != 0 ) { 
    xerbla( "SGEQPF", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( m == 0 || n == 0 ) 
    return;
  
  mn = min( m, n );
  
  //     Move initial columns up front
  
  itemp = 1;
  for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
    if( jpvt[i_] != 0 ) { 
      if( i != itemp ) { 
        sswap( m, &A(i_,0), 1, &A(itemp - 1,0), 1 );
        jpvt[i_] = jpvt[itemp - 1];
        jpvt[itemp - 1] = i;
      }
      else { 
        jpvt[i_] = i;
      }
      itemp = itemp + 1;
    }
    else { 
      jpvt[i_] = i;
    }
  }
  itemp = itemp - 1;
  
  //     Compute the QR factorization and update remaining columns
  
  if( itemp > 0 ) { 
    ma = min( itemp, m );
    sgeqr2( m, ma, a, lda, tau, work, info );
    if( ma < n ) { 
      sorm2r( 'L'/*Left*/, 'T'/*Transpose*/, m, n - ma, ma, 
       a, lda, tau, &A(ma,0), lda, work, info );
    }
  }
  
  if( itemp < mn ) { 
    
    //        Initialize partial column norms. The first n entries of
    //        work store the exact column norms.
    
    for( i = itemp + 1, i_ = i - 1, _do1 = n; i <= _do1; i++, i_++ ) { 
      work[i_] = snrm2( m - itemp, &A(i_,itemp), 1 );
      work[n + i_] = work[i_];
    }
    
    //        Compute factorization
    
    for( i = itemp + 1, i_ = i - 1, _do2 = mn; i <= _do2; i++, i_++ ) { 
      
      //           Determine ith pivot column and swap if necessary
      
      pvt = (i - 1) + isamax( n - i + 1, &work[i_], 1 );
      
      if( pvt != i ) { 
        sswap( m, &A(pvt - 1,0), 1, &A(i_,0), 1 );
        itemp = jpvt[pvt - 1];
        jpvt[pvt - 1] = jpvt[i_];
        jpvt[i_] = itemp;
        work[pvt - 1] = work[i_];
        work[n + pvt - 1] = work[n + i_];
      }
      
      //           Generate elementary reflector H(i)
      
      if( i < m ) { 
        slarfg( m - i + 1, A(i_,i_), &A(i_,i_ + 1), 1, tau[i_] );
      }
      else { 
        slarfg( 1, A(m - 1,m - 1), &A(m - 1,m - 1), 1, tau[m - 1] );
      }
      
      if( i < n ) { 
        
        //              Apply H(i) to A(i:m,i+1:n) from the left
        
        aii = A(i_,i_);
        A(i_,i_) = ONE;
        slarf( 'L'/*LEFT*/, m - i + 1, n - i, &A(i_,i_), 
         1, tau[i_], &A(i_ + 1,i_), lda, &work[n*2] );
        A(i_,i_) = aii;
      }
      
      //           Update partial column norms
      
      for( j = i + 1, j_ = j - 1, _do3 = n; j <= _do3; j++, j_++ ) { 
        if( work[j_] != ZERO ) { 
          temp = ONE - pow(abs( A(j_,i_) )/work[j_], 2);
          temp = max( temp, ZERO );
          temp2 = ONE + 0.05*temp*pow(work[j_]/work[n + j_], 2);
          if( temp2 == ONE ) { 
            if( m - i > 0 ) { 
              work[j_] = snrm2( m - i, &A(j_,i_ + 1), 
               1 );
              work[n + j_] = work[j_];
            }
            else { 
              work[j_] = ZERO;
              work[n + j_] = ZERO;
            }
          }
          else { 
            work[j_] = work[j_]*sqrt( temp );
          }
        }
      }
      
    }
  }
  return;
  
  //     End of SGEQPF
  
#undef  A
} // end of function 


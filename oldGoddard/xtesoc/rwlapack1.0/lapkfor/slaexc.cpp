/*
 * C++ implementation of Lapack routine slaexc
 *
 * $Id: slaexc.cpp,v 1.3 1993/10/15 05:27:20 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:59:35
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: slaexc.cpp,v $
 * Revision 1.3  1993/10/15  05:27:20  alv
 * worked around weird sun bug
 *
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ZERO = 0.0e0;
const float ONE = 1.0e0;
const float TEN = 1.0e1;
/* 
 * If the following two lines are used, SUN C++ v3 complains a couple
 * hundred lines down that LDD and LDX are undefined - yikes!
const long LDD = 4;
const long LDX = 2;
 */
#define LDD 4l
#define LDX 2l
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ slaexc(const int &wantq, const long &n, float *t, 
 const long &ldt, float *q, const long &ldq, const long &j1, const long &n1, 
 const long &n2, float work[], long &info)
{
#define T(I_,J_)  (*(t+(I_)*(ldt)+(J_)))
#define Q(I_,J_)  (*(q+(I_)*(ldq)+(J_)))
  long ierr, j2, j3, j4, k, nd;
  float cs, d[4][LDD], dnorm, eps, scale, smlnum, sn, t11, t22, 
   t33, tau, tau1, tau2, temp, thresh, u[3], u1[3], u2[3], wi1, 
   wi2, wr1, wr2, x[2][LDX], xnorm;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SLAEXC swaps adjacent diagonal blocks T11 and T22 of order 1 or 2 in
  //  an upper quasi-triangular matrix T by an orthogonal similarity
  //  transformation.
  
  //  T must be in Schur canonical form, that is, block upper triangular
  //  with 1-by-1 and 2-by-2 diagonal blocks; each 2-by-2 diagonal block
  //  has its diagonal elemnts equal and its off-diagonal elements of
  //  opposite sign.
  
  //  Arguments
  //  =========
  
  //  WANTQ   (input) LOGICAL
  //          = .TRUE. : accumulate the transformation in the matrix Q;
  //          = .FALSE.: do not accumulate the transformation.
  
  //  N       (input) INTEGER
  //          The order of the matrix T. N >= 0.
  
  //  T       (input/output) REAL array, dimension (LDT,N)
  //          On entry, the upper quasi-triangular matrix T, in Schur
  //          canonical form.
  //          On exit, the updated matrix T, again in Schur canonical form.
  
  //  LDT     (input)  INTEGER
  //          The leading dimension of the array T. LDT >= max(1,N).
  
  //  Q       (input/output) REAL array, dimension (LDQ,N)
  //          On entry, if WANTQ is .TRUE., the orthogonal matrix Q.
  //          On exit, if WANTQ is .TRUE., the updated matrix Q.
  //          If WANTQ is .FALSE., Q is not referenced.
  
  //  LDQ     (input) INTEGER
  //          The leading dimension of the array Q.
  //          LDQ >= 1; and if WANTQ is .TRUE., LDQ >= N.
  
  //  J1      (input) INTEGER
  //          The index of the first row of the first block T11.
  
  //  N1      (input) INTEGER
  //          The order of the first block T11. N1 = 0, 1 or 2.
  
  //  N2      (input) INTEGER
  //          The order of the second block T22. N2 = 0, 1 or 2.
  
  //  WORK    (workspace) REAL array, dimension (N)
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          = 1: the transformed matrix T would be too far from Schur
  //               form; the blocks are not swapped and T and Q are
  //               unchanged.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Local Arrays ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  info = 0;
  
  //     Quick return if possible
  
  if( (n == 0 || n1 == 0) || n2 == 0 ) 
    return;
  if( j1 + n1 > n ) 
    return;
  
  j2 = j1 + 1;
  j3 = j1 + 2;
  j4 = j1 + 3;
  
  if( n1 == 1 && n2 == 1 ) { 
    
    //        Swap two 1-by-1 blocks.
    
    t11 = T(j1 - 1,j1 - 1);
    t22 = T(j2 - 1,j2 - 1);
    
    //        Determine the transformation to perform the interchange.
    
    slartg( T(j2 - 1,j1 - 1), t22 - t11, cs, sn, temp );
    
    //        Apply transformation to the matrix T.
    
    if( j3 <= n ) 
      srot( n - j1 - 1, &T(j3 - 1,j1 - 1), ldt, &T(j3 - 1,j2 - 1), 
       ldt, cs, sn );
    srot( j1 - 1, &T(j1 - 1,0), 1, &T(j2 - 1,0), 1, cs, sn );
    
    T(j1 - 1,j1 - 1) = t22;
    T(j2 - 1,j2 - 1) = t11;
    
    if( wantq ) { 
      
      //           Accumulate transformation in the matrix Q.
      
      srot( n, &Q(j1 - 1,0), 1, &Q(j2 - 1,0), 1, cs, sn );
    }
    
  }
  else { 
    
    //        Swapping involves at least one 2-by-2 block.
    
    //        Copy the diagonal block of order N1+N2 to the local array D
    //        and compute its norm.
    
    nd = n1 + n2;
    slacpy( 'F'/*Full*/, nd, nd, &T(j1 - 1,j1 - 1), ldt, (float*)d, 
     LDD );
    dnorm = slange( 'M'/*Max*/, nd, nd, (float*)d, LDD, work );
    
    //        Compute machine-dependent threshold for test for accepting
    //        swap.
    
    eps = slamch( 'P' );
    smlnum = slamch( 'S' )/eps;
    thresh = max( TEN*eps*dnorm, smlnum );
    
    //        Solve T11*X - X*T22 = scale*T12 for X.
    
    slasy2( FALSE, FALSE, -1, n1, n2, (float*)d, LDD, &d[n1][n1], 
     LDD, &d[n1][0], LDD, scale, (float*)x, LDX, xnorm, ierr );
    
    //        Swap the adjacent diagonal blocks.
    
    k = n1 + n1 + n2 - 3;
    switch( k ) { 
      case 1: goto L_10;
      case 2: goto L_20;
      case 3: goto L_30;
    }
    
L_10:
    ;
    
    //        N1 = 1, N2 = 2: generate elementary reflector H so that:
    
    //        ( scale, X11, X12 ) H = ( 0, 0, * )
    
    u[0] = scale;
    u[1] = x[0][0];
    u[2] = x[1][0];
    slarfg( 3, u[2], u, 1, tau );
    u[2] = ONE;
    t11 = T(j1 - 1,j1 - 1);
    
    //        Perform swap provisionally on diagonal block in D.
    
    slarfx( 'L', 3, 3, u, tau, (float*)d, LDD, work );
    slarfx( 'R', 3, 3, u, tau, (float*)d, LDD, work );
    
    //        Test whether to reject swap.
    
    if( vmax( abs( d[0][2] ), abs( d[1][2] ), abs( d[2][2] - t11 ), 
     FEND ) > thresh ) 
      goto L_50;
    
    //        Accept swap: apply transformation to the entire matrix T.
    
    slarfx( 'L', 3, n - j1 + 1, u, tau, &T(j1 - 1,j1 - 1), ldt, 
     work );
    slarfx( 'R', j2, 3, u, tau, &T(j1 - 1,0), ldt, work );
    
    T(j1 - 1,j3 - 1) = ZERO;
    T(j2 - 1,j3 - 1) = ZERO;
    T(j3 - 1,j3 - 1) = t11;
    
    if( wantq ) { 
      
      //           Accumulate transformation in the matrix Q.
      
      slarfx( 'R', n, 3, u, tau, &Q(j1 - 1,0), ldq, work );
    }
    goto L_40;
    
L_20:
    ;
    
    //        N1 = 2, N2 = 1: generate elementary reflector H so that:
    
    //        H (  -X11 ) = ( * )
    //          (  -X21 ) = ( 0 )
    //          ( scale ) = ( 0 )
    
    u[0] = -x[0][0];
    u[1] = -x[0][1];
    u[2] = scale;
    slarfg( 3, u[0], &u[1], 1, tau );
    u[0] = ONE;
    t33 = T(j3 - 1,j3 - 1);
    
    //        Perform swap provisionally on diagonal block in D.
    
    slarfx( 'L', 3, 3, u, tau, (float*)d, LDD, work );
    slarfx( 'R', 3, 3, u, tau, (float*)d, LDD, work );
    
    //        Test whether to reject swap.
    
    if( vmax( abs( d[0][1] ), abs( d[0][2] ), abs( d[0][0] - t33 ), 
     FEND ) > thresh ) 
      goto L_50;
    
    //        Accept swap: apply transformation to the entire matrix T.
    
    slarfx( 'R', j3, 3, u, tau, &T(j1 - 1,0), ldt, work );
    slarfx( 'L', 3, n - j1, u, tau, &T(j2 - 1,j1 - 1), ldt, work );
    
    T(j1 - 1,j1 - 1) = t33;
    T(j1 - 1,j2 - 1) = ZERO;
    T(j1 - 1,j3 - 1) = ZERO;
    
    if( wantq ) { 
      
      //           Accumulate transformation in the matrix Q.
      
      slarfx( 'R', n, 3, u, tau, &Q(j1 - 1,0), ldq, work );
    }
    goto L_40;
    
L_30:
    ;
    
    //        N1 = 2, N2 = 2: generate elementary reflectors H(1) and H(2) so
    //        that:
    
    //        H(2) H(1) (  -X11  -X12 ) = (  *  * )
    //                  (  -X21  -X22 )   (  0  * )
    //                  ( scale    0  )   (  0  0 )
    //                  (    0  scale )   (  0  0 )
    
    u1[0] = -x[0][0];
    u1[1] = -x[0][1];
    u1[2] = scale;
    slarfg( 3, u1[0], &u1[1], 1, tau1 );
    u1[0] = ONE;
    
    temp = -tau1*(x[1][0] + u1[1]*x[1][1]);
    u2[0] = -temp*u1[1] - x[1][1];
    u2[1] = -temp*u1[2];
    u2[2] = scale;
    slarfg( 3, u2[0], &u2[1], 1, tau2 );
    u2[0] = ONE;
    
    //        Perform swap provisionally on diagonal block in D.
    
    slarfx( 'L', 3, 4, u1, tau1, (float*)d, LDD, work );
    slarfx( 'R', 4, 3, u1, tau1, (float*)d, LDD, work );
    slarfx( 'L', 3, 4, u2, tau2, &d[0][1], LDD, work );
    slarfx( 'R', 4, 3, u2, tau2, &d[1][0], LDD, work );
    
    //        Test whether to reject swap.
    
    if( vmax( abs( d[0][2] ), abs( d[1][2] ), abs( d[0][3] ), 
     abs( d[1][3] ), FEND ) > thresh ) 
      goto L_50;
    
    //        Accept swap: apply transformation to the entire matrix T.
    
    slarfx( 'L', 3, n - j1 + 1, u1, tau1, &T(j1 - 1,j1 - 1), ldt, 
     work );
    slarfx( 'R', j4, 3, u1, tau1, &T(j1 - 1,0), ldt, work );
    slarfx( 'L', 3, n - j1 + 1, u2, tau2, &T(j1 - 1,j2 - 1), ldt, 
     work );
    slarfx( 'R', j4, 3, u2, tau2, &T(j2 - 1,0), ldt, work );
    
    T(j1 - 1,j3 - 1) = ZERO;
    T(j2 - 1,j3 - 1) = ZERO;
    T(j1 - 1,j4 - 1) = ZERO;
    T(j2 - 1,j4 - 1) = ZERO;
    
    if( wantq ) { 
      
      //           Accumulate transformation in the matrix Q.
      
      slarfx( 'R', n, 3, u1, tau1, &Q(j1 - 1,0), ldq, work );
      slarfx( 'R', n, 3, u2, tau2, &Q(j2 - 1,0), ldq, work );
    }
    
L_40:
    ;
    
    if( n2 == 2 ) { 
      
      //           Standardize new 2-by-2 block T11
      
      slanv2( T(j1 - 1,j1 - 1), T(j2 - 1,j1 - 1), T(j1 - 1,j2 - 1), 
       T(j2 - 1,j2 - 1), wr1, wi1, wr2, wi2, cs, sn );
      srot( n - j1 - 1, &T(j1 + 1,j1 - 1), ldt, &T(j1 + 1,j2 - 1), 
       ldt, cs, sn );
      srot( j1 - 1, &T(j1 - 1,0), 1, &T(j2 - 1,0), 1, cs, sn );
      if( wantq ) 
        srot( n, &Q(j1 - 1,0), 1, &Q(j2 - 1,0), 1, cs, sn );
    }
    
    if( n1 == 2 ) { 
      
      //           Standardize new 2-by-2 block T22
      
      j3 = j1 + n2;
      j4 = j3 + 1;
      slanv2( T(j3 - 1,j3 - 1), T(j4 - 1,j3 - 1), T(j3 - 1,j4 - 1), 
       T(j4 - 1,j4 - 1), wr1, wi1, wr2, wi2, cs, sn );
      if( j3 + 2 <= n ) 
        srot( n - j3 - 1, &T(j3 + 1,j3 - 1), ldt, &T(j3 + 1,j4 - 1), 
         ldt, cs, sn );
      srot( j3 - 1, &T(j3 - 1,0), 1, &T(j4 - 1,0), 1, cs, sn );
      if( wantq ) 
        srot( n, &Q(j3 - 1,0), 1, &Q(j4 - 1,0), 1, cs, sn );
    }
    
  }
  return;
  
  //     Exit with INFO = 1 if swap was rejected.
  
L_50:
  info = 1;
  return;
  
  //     End of SLAEXC
  
#undef  T
#undef  Q
} // end of function 


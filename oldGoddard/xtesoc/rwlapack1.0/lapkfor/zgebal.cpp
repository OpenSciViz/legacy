/*
 * C++ implementation of Lapack routine zgebal
 *
 * $Id: zgebal.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:46:04
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zgebal.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
const double SCLFAC = 1.0e1;
const double FACTOR = 0.95e0;
// end of PARAMETER translations

inline double zgebal_cabs1(DComplex cdum) { return abs( real( (cdum) ) ) + 
   abs( imag( (cdum) ) ); }
RWLAPKDECL void /*FUNCTION*/ zgebal(const char &job, const long &n, DComplex *a, const long &lda, 
 long &ilo, long &ihi, double scale[], long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  // PARAMETER translations
  const double B2 = SCLFAC*SCLFAC;
  // end of PARAMETER translations

  int noconv;
  long _do0, _do1, _do2, _do3, _do4, _do5, _do6, i, i_, iexc, 
   j, j_, k, l, m;
  double c, f, g, r, s;
  DComplex cdum;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZGEBAL does permutations to isolate eigenvalues and/or balances a
  //  DComplex general matrix.  Balancing may reduce the 1-norm of the
  //  matrix and can improve the accuracy of the computed eigenvalues
  //  and/or eigenvectors.
  
  //  Arguments
  //  =========
  
  //  JOB     (input) CHARACTER*1
  //          Specifies the operations to be done:
  //          = 'N', do nothing with the input matrix, but set
  //                 ILO = 1, IHI = N, SCALE(I) = ONE.
  //          = 'P', only permute to isolate eigenvalues.
  //          = 'S', only balance the input matrix.
  //          = 'B', both permute and balance.
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  A       (input/output) COMPLEX*16 array, dimension (LDA,N)
  //          On entry, A contains the input matrix to be balanced.
  //          On exit,  A contains the permuted and/or balanced matrix.
  //          See Further Details.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the matrix A. LDA >= max(1,N).
  
  //  ILO     (output) INTEGER
  //  IHI     (output) INTEGER
  //          On exit, ILO and IHI are two integers such that A(I,J)
  //          is equal to zero if
  //             (1) I is greater than J and
  //             (2) J = 1,...,ILO-1 or I = IHI+1,...,N.
  
  //  SCALE   (output) DOUBLE PRECISION array, dimension (N)
  //          On exit, SCALE contains information about the permutations
  //          and scaling factors used.  If P(J) is the index of the
  //          row and column interchanged with row and column J and D(J,J)
  //          is the scaling factor used to balance row and column J of the
  //          submatrix of A, then
  //          SCALE(J) = P(J),    for J = 1,...,ILO-1
  //                   = D(J,J),      J = ILO,...,IHI
  //                   = P(J)         J = IHI+1,...,N.
  //          The order in which the interchanges are made is N to IHI+1,
  //          then 1 to ILO-1.
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit.
  //          < 0:  if INFO = -k, the k-th argument had an illegal value.
  
  //  Further Details
  //  ===============
  
  //  The permutations consist of row and column interchanges which put
  //  the matrix in the form
  
  //             ( T1   X   Y  )
  //     P A P = (  0   B   Z  )
  //             (  0   0   T2 )
  
  //  where T1 and T2 are upper triangular matrices whose eigenvalues lie
  //  along the diagonal.  The column indices ILO and IHI mark the starting
  //  and ending columns of the submatrix B. Balancing consists of applying
  //  a diagonal similarity transformation inv(D) * B * D to make the
  //  1-norms of each row of B and its corresponding column nearly equal.
  //  The output matrix is
  
  //     ( T1     X*D          Y    )
  //     (  0  inv(D)*B*D  inv(D)*Z ).
  //     (  0      0           T2   )
  
  //  Information about the permutations P and the diagonal matrix D is
  //  returned in the vector SCALE.
  
  //  This subroutine is based on the EISPACK routine CBAL.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Statement Functions ..
  //     ..
  //     .. Statement Function definitions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters
  
  info = 0;
  if( ((!lsame( job, 'N' ) && !lsame( job, 'P' )) && !lsame( job, 
   'S' )) && !lsame( job, 'B' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( lda < max( 1, n ) ) { 
    info = -3;
  }
  if( info != 0 ) { 
    xerbla( "ZGEBAL", -info );
    return;
  }
  
  k = 1;
  l = n;
  
  if( n == 0 ) 
    goto L_210;
  
  if( lsame( job, 'N' ) ) { 
    for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
      scale[i_] = ONE;
    }
    goto L_210;
  }
  
  if( lsame( job, 'S' ) ) 
    goto L_120;
  
  //     Permutation to isolate eigenvalues if possible
  
  goto L_50;
  
  //     Row and column exchange.
  
L_20:
  ;
  scale[m - 1] = j;
  if( j == m ) 
    goto L_30;
  
  zswap( l, &A(j - 1,0), 1, &A(m - 1,0), 1 );
  zswap( n - k + 1, &A(k - 1,j - 1), lda, &A(k - 1,m - 1), lda );
  
L_30:
  ;
  switch( iexc ) { 
    case 1: goto L_40;
    case 2: goto L_80;
  }
  
  //     Search for rows isolating an eigenvalue and push them down.
  
L_40:
  ;
  if( l == 1 ) 
    goto L_210;
  l = l - 1;
  
L_50:
  ;
  for( j = l, j_ = j - 1; j >= 1; j--, j_-- ) { 
    
    for( i = 1, i_ = i - 1, _do1 = l; i <= _do1; i++, i_++ ) { 
      if( i == j ) 
        goto L_60;
      if( real( A(i_,j_) ) != ZERO || imag( A(i_,j_) ) != ZERO ) 
        goto L_70;
L_60:
      ;
    }
    
    m = l;
    iexc = 1;
    goto L_20;
L_70:
    ;
  }
  
  goto L_90;
  
  //     Search for columns isolating an eigenvalue and push them left.
  
L_80:
  ;
  k = k + 1;
  
L_90:
  ;
  for( j = k, j_ = j - 1, _do2 = l; j <= _do2; j++, j_++ ) { 
    
    for( i = k, i_ = i - 1, _do3 = l; i <= _do3; i++, i_++ ) { 
      if( i == j ) 
        goto L_100;
      if( real( A(j_,i_) ) != ZERO || imag( A(j_,i_) ) != ZERO ) 
        goto L_110;
L_100:
      ;
    }
    
    m = k;
    iexc = 2;
    goto L_20;
L_110:
    ;
  }
  
L_120:
  ;
  for( i = k, i_ = i - 1, _do4 = l; i <= _do4; i++, i_++ ) { 
    scale[i_] = ONE;
  }
  
  if( lsame( job, 'P' ) ) 
    goto L_210;
  
  //     Balance the submatrix in rows K to L.
  
  //     Iterative loop for norm reduction
  
L_140:
  ;
  noconv = FALSE;
  
  for( i = k, i_ = i - 1, _do5 = l; i <= _do5; i++, i_++ ) { 
    c = ZERO;
    r = ZERO;
    
    for( j = k, j_ = j - 1, _do6 = l; j <= _do6; j++, j_++ ) { 
      if( j == i ) 
        goto L_150;
      c = c + zgebal_cabs1( A(i_,j_) );
      r = r + zgebal_cabs1( A(j_,i_) );
L_150:
      ;
    }
    
    //        Guard against zero C or R due to underflow.
    
    if( c == ZERO || r == ZERO ) 
      goto L_200;
    g = r/SCLFAC;
    f = ONE;
    s = c + r;
L_160:
    ;
    if( c >= g ) 
      goto L_170;
    f = f*SCLFAC;
    c = c*B2;
    goto L_160;
    
L_170:
    ;
    g = r*SCLFAC;
L_180:
    ;
    if( c < g ) 
      goto L_190;
    f = f/SCLFAC;
    c = c/B2;
    goto L_180;
    
    //        Now balance.
    
L_190:
    ;
    if( (c + r)/f >= FACTOR*s ) 
      goto L_200;
    g = ONE/f;
    scale[i_] = scale[i_]*f;
    noconv = TRUE;
    
    zdscal( n - k + 1, g, &A(k - 1,i_), lda );
    zdscal( l, f, &A(i_,0), 1 );
    
L_200:
    ;
  }
  
  if( noconv ) 
    goto L_140;
  
L_210:
  ;
  ilo = k;
  ihi = l;
  
  return;
  
  //     End of ZGEBAL
  
#undef  A
} // end of function 


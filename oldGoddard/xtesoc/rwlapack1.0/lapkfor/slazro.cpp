/*
 * C++ implementation of Lapack routine slazro
 *
 * $Id: slazro.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:01:02
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: slazro.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

RWLAPKDECL void /*FUNCTION*/ slazro(const long &m, const long &n, const float &alpha, const float &beta, 
   float *a, const long &lda)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  long _do0, _do1, _do2, i, i_, j, j_;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SLAZRO initializes a 2-D array A to BETA on the diagonal and
  //  ALPHA on the offdiagonals.
  
  //  Arguments
  //  =========
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix A.  M >= 0.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix A.  N >= 0.
  
  //  ALPHA   (input) REAL
  //          The constant to which the offdiagonal elements are to be set.
  
  //  BETA    (input) REAL
  //          The constant to which the diagonal elements are to be set.
  
  //  A       (output) REAL array, dimension (LDA,N)
  //          On exit, the leading m by n submatrix of A is set such that
  //             A(i,j) = ALPHA,  1 <= i <= m, 1 <= j <= n, i <> j
  //             A(i,i) = BETA,   1 <= i <= min(m,n).
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,M).
  
  //     .. Local Scalars ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
    for( i = 1, i_ = i - 1, _do1 = m; i <= _do1; i++, i_++ ) { 
      A(j_,i_) = alpha;
    }
  }
  
  for( i = 1, i_ = i - 1, _do2 = min( m, n ); i <= _do2; i++, i_++ ) { 
    A(i_,i_) = beta;
  }
  
  return;
  
  //     End of SLAZRO
  
#undef  A
} // end of function 


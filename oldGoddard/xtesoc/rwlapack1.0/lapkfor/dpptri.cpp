/*
 * C++ implementation of lapack routine dpptri
 *
 * $Id: dpptri.cpp,v 1.6 1993/04/06 20:42:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:37:37
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dpptri.cpp,v $
 * Revision 1.6  1993/04/06  20:42:09  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.5  1993/03/19  18:41:23  alv
 * now passes chars explicitly, rather than indirection of a string, to shut up SUN warnings
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:16:49  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:08:36  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dpptri(const char &uplo, const long &n, double ap[], long &info)
{
  int upper;
  long _do0, _do1, j, j_, jc, jj, jjn;
  double ajj;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DPPTRI computes the inverse of a real symmetric positive definite
  //  matrix A using the Cholesky factorization A = U'*U or A = L*L'
  //  computed by DPPTRF.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the factor stored in AP is upper or lower
  //          triangular.
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  AP      (input/output) DOUBLE PRECISION array, dimension (N*(N+1)/2)
  //          On entry, the triangular factor U or L from the Cholesky
  //          factorization A = U'*U or A = L*L', packed columnwise as a
  //          linear array.  The j-th column of U or L is stored in the
  //          array AP as follows:
  //          if UPLO = 'U', AP(i + (j-1)*j/2) = U(i,j) for 1<=i<=j;
  //          if UPLO = 'L', AP(i + (j-1)*(2n-j)/2) = L(i,j) for j<=i<=n.
  
  //          On exit, the upper or lower triangle of the (symmetric)
  //          inverse of A, overwriting the input factor U or L.
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: if INFO = k, the (k,k) element of the factor U or L is
  //               zero, and the inverse could not be computed.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  if( info != 0 ) { 
    xerbla( "DPPTRI", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  //     Invert the triangular Cholesky factor U or L.
  
  dtptri( uplo, 'N'/* Non-unit */, n, ap, info );
  if( info > 0 ) 
    return;
  
  if( upper ) { 
    
    //        Compute the product inv(U) * inv(U)'.
    
    jj = 0;
    for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
      jc = jj + 1;
      jj = jj + j;
      if( j > 1 ) 
        dspr( 'U'/* Upper */, j - 1, ONE, &ap[jc - 1], 1, ap );
      ajj = ap[jj - 1];
      dscal( j, ajj, &ap[jc - 1], 1 );
    }
    
  }
  else { 
    
    //        Compute the product inv(L)' * inv(L).
    
    jj = 1;
    for( j = 1, j_ = j - 1, _do1 = n; j <= _do1; j++, j_++ ) { 
      jjn = jj + n - j + 1;
      ap[jj - 1] = ddot( n - j + 1, &ap[jj - 1], 1, &ap[jj - 1], 
       1 );
      if( j < n ) 
        dtpmv( 'L'/* Lower */, 'T'/* Transpose */, 'N'/* Non-unit */
         , n - j, &ap[jjn - 1], &ap[jj], 1 );
      jj = jjn;
    }
  }
  
  return;
  
  //     End of DPPTRI
  
} // end of function 


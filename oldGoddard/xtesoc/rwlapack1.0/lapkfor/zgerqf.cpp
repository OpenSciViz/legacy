/*
 * C++ implementation of Lapack routine zgerqf
 *
 * $Id: zgerqf.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:46:42
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zgerqf.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

RWLAPKDECL void /*FUNCTION*/ zgerqf(const long &m, const long &n, DComplex *a, const long &lda, 
   DComplex tau[], DComplex work[], const long &lwork, long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  long _do0, _do1, i, i_, ib, iinfo, iws, k, ki, kk, ldwork, 
   mu, nb, nbmin, nu, nx;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZGERQF computes an RQ factorization of a DComplex m by n matrix A:
  //  A = R * Q.
  
  //  Arguments
  //  =========
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix A.  M >= 0.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix A.  N >= 0.
  
  //  A       (input/output) COMPLEX*16 array, dimension (LDA,N)
  //          On entry, the m by n matrix A.
  //          On exit, if m <= n, the upper triangle of the subarray
  //          A(1:m,n-m+1:n) contains the m by m upper triangular matrix R;
  //          if m >= n, the elements on and above the (m-n)-th subdiagonal
  //          contain the m by n upper trapezoidal matrix R; the remaining
  //          elements, with the array TAU, represent the unitary matrix
  //          Q as a product of elementary reflectors (see Further
  //          Details).
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,M).
  
  //  TAU     (output) COMPLEX*16 array, dimension (min(M,N))
  //          The scalar factors of the elementary reflectors (see Further
  //          Details).
  
  //  WORK    (workspace) COMPLEX*16 array, dimension (LWORK)
  //          On exit, if INFO = 0, WORK(1) returns the minimum value of
  //          LWORK required to use the optimal blocksize.
  
  //  LWORK   (input) INTEGER
  //          The dimension of the array WORK.  LWORK >= max(1,M).
  //          For optimum performance LWORK should be at least M*NB,
  //          where NB is the optimal blocksize.
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument had an illegal value
  
  //  Further Details
  //  ===============
  
  //  The matrix Q is represented as a product of elementary reflectors
  
  //     Q = H(1)' H(2)' . . . H(k)', where k = min(m,n).
  
  //  Each H(i) has the form
  
  //     H(i) = I - tau * v * v'
  
  //  where tau is a DComplex scalar, and v is a DComplex vector with
  //  v(n-k+i+1:n) = 0 and v(n-k+i) = 1; conjg(v(1:n-k+i-1)) is stored on
  //  exit in A(m-k+i,1:n-k+i-1), and tau in TAU(i).
  
  //  =====================================================================
  
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input arguments
  
  info = 0;
  if( m < 0 ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( lda < max( 1, m ) ) { 
    info = -4;
  }
  else if( lwork < max( 1, m ) ) { 
    info = -7;
  }
  if( info != 0 ) { 
    xerbla( "ZGERQF", -info );
    return;
  }
  
  //     Quick return if possible
  
  k = min( m, n );
  if( k == 0 ) { 
    work[0] = DComplex((double)1);
    return;
  }
  
  //     Determine the block size.
  
  nb = ilaenv( 1, "ZGERQF", " ", m, n, -1, -1 );
  nbmin = 2;
  nx = 1;
  iws = m;
  if( nb > 1 && nb < k ) { 
    
    //        Determine when to cross over from blocked to unblocked code.
    
    nx = max( 0, ilaenv( 3, "ZGERQF", " ", m, n, -1, -1 ) );
    if( nx < k ) { 
      
      //           Determine if workspace is large enough for blocked code.
      
      ldwork = m;
      iws = ldwork*nb;
      if( lwork < iws ) { 
        
        //              Not enough workspace to use optimal NB:  reduce NB and
        //              determine the minimum value of NB.
        
        nb = lwork/ldwork;
        nbmin = max( 2, ilaenv( 2, "ZGERQF", " ", m, n, -1, 
         -1 ) );
      }
    }
  }
  
  if( (nb >= nbmin && nb < k) && nx < k ) { 
    
    //        Use blocked code initially.
    //        The last kk rows are handled by the block method.
    
    ki = ((k - nx - 1)/nb)*nb;
    kk = min( k, ki + nb );
    
    for( i = k - kk + ki + 1, i_ = i - 1, _do0=docnt(i,k - kk + 
     1,_do1 = -nb); _do0 > 0; i += _do1, i_ += _do1, _do0-- ) { 
      ib = min( k - i + 1, nb );
      
      //           Compute the RQ factorization of the current block
      //           A(m-k+i:m-k+i+ib-1,1:n-k+i+ib-1)
      
      zgerq2( ib, n - k + i + ib - 1, &A(0,m - k + i_), lda, 
       &tau[i_], work, iinfo );
      if( m - k + i > 1 ) { 
        
        //              Form the triangular factor of the block reflector
        //              H = H(i+ib-1) . . . H(i+1) H(i)
        
        zlarft( 'B'/*Backward*/, 'R'/*Rowwise*/, n - k + 
         i + ib - 1, ib, &A(0,m - k + i_), lda, &tau[i_], 
         work, ldwork );
        
        //              Apply H to A(1:m-k+i-1,1:n-k+i+ib-1) from the right
        
        zlarfb( 'R'/*Right*/, 'N'/*No transpose*/, 'B'/*Backward*/
         , 'R'/*Rowwise*/, m - k + i - 1, n - k + i + ib - 
         1, ib, &A(0,m - k + i_), lda, work, ldwork, a, lda, 
         &work[ib], ldwork );
      }
    }
    mu = m - k + i + nb - 1;
    nu = n - k + i + nb - 1;
  }
  else { 
    mu = m;
    nu = n;
  }
  
  //     Use unblocked code to factor the last or only block
  
  if( mu > 0 && nu > 0 ) 
    zgerq2( mu, nu, a, lda, tau, work, iinfo );
  
  work[0] = DComplex((double)iws);
  return;
  
  //     End of ZGERQF
  
#undef  A
} // end of function 


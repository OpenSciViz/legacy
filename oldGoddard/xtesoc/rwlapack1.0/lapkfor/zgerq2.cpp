/*
 * C++ implementation of Lapack routine zgerq2
 *
 * $Id: zgerq2.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:46:40
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zgerq2.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const DComplex ONE = DComplex(1.0e0);
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zgerq2(const long &m, const long &n, DComplex *a, const long &lda, 
 DComplex tau[], DComplex work[], long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  long i, i_, k;
  DComplex alpha;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZGERQ2 computes an RQ factorization of a DComplex m by n matrix A:
  //  A = R * Q.
  
  //  Arguments
  //  =========
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix A.  M >= 0.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix A.  N >= 0.
  
  //  A       (input/output) COMPLEX*16 array, dimension (LDA,N)
  //          On entry, the m by n matrix A.
  //          On exit, if m <= n, the upper triangle of the subarray
  //          A(1:m,n-m+1:n) contains the m by m upper triangular matrix R;
  //          if m >= n, the elements on and above the (m-n)-th subdiagonal
  //          contain the m by n upper trapezoidal matrix R; the remaining
  //          elements, with the array TAU, represent the unitary matrix
  //          Q as a product of elementary reflectors (see Further
  //          Details).
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,M).
  
  //  TAU     (output) COMPLEX*16 array, dimension (min(M,N))
  //          The scalar factors of the elementary reflectors (see Further
  //          Details).
  
  //  WORK    (workspace) COMPLEX*16 array, dimension (M)
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument had an illegal value
  
  //  Further Details
  //  ===============
  
  //  The matrix Q is represented as a product of elementary reflectors
  
  //     Q = H(1)' H(2)' . . . H(k)', where k = min(m,n).
  
  //  Each H(i) has the form
  
  //     H(i) = I - tau * v * v'
  
  //  where tau is a DComplex scalar, and v is a DComplex vector with
  //  v(n-k+i+1:n) = 0 and v(n-k+i) = 1; conjg(v(1:n-k+i-1)) is stored on
  //  exit in A(m-k+i,1:n-k+i-1), and tau in TAU(i).
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input arguments
  
  info = 0;
  if( m < 0 ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( lda < max( 1, m ) ) { 
    info = -4;
  }
  if( info != 0 ) { 
    xerbla( "ZGERQ2", -info );
    return;
  }
  
  k = min( m, n );
  
  for( i = k, i_ = i - 1; i >= 1; i--, i_-- ) { 
    
    //        Generate elementary reflector H(i) to annihilate
    //        A(m-k+i,1:n-k+i-1)
    
    zlacgv( n - k + i, &A(0,m - k + i_), lda );
    alpha = A(n - k + i_,m - k + i_);
    zlarfg( n - k + i, alpha, &A(0,m - k + i_), lda, tau[i_] );
    
    //        Apply H(i) to A(1:m-k+i-1,1:n-k+i) from the right
    
    A(n - k + i_,m - k + i_) = ONE;
    zlarf( 'R'/*Right*/, m - k + i - 1, n - k + i, &A(0,m - k + i_), 
     lda, tau[i_], a, lda, work );
    A(n - k + i_,m - k + i_) = alpha;
    zlacgv( n - k + i - 1, &A(0,m - k + i_), lda );
  }
  return;
  
  //     End of ZGERQ2
  
#undef  A
} // end of function 


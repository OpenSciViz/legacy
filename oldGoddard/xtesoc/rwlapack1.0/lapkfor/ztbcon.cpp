/*
 * C++ implementation of Lapack routine ztbcon
 *
 * $Id: ztbcon.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:51:03
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: ztbcon.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ONE = 1.0e0;
const double ZERO = 0.0e0;
// end of PARAMETER translations

inline double ztbcon_cabs1(DComplex zdum) { return abs( real( (zdum) ) ) + 
   abs( imag( (zdum) ) ); }
RWLAPKDECL void /*FUNCTION*/ ztbcon(const char &norm, const char &uplo, const char &diag, const long &n, 
 const long &kd, DComplex *ab, const long &ldab, double &rcond, DComplex work[], 
 double rwork[], long &info)
{
#define AB(I_,J_) (*(ab+(I_)*(ldab)+(J_)))
  int nounit, onenrm, upper;
  char normin;
  long ix, kase, kase1;
  double ainvnm, anorm, scale, smlnum, xnorm;
  DComplex zdum;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZTBCON estimates the reciprocal of the condition number of a
  //  triangular band matrix A, in either the 1-norm or the infinity-norm.
  
  //  The norm of A is computed and an estimate is obtained for
  //  norm(inv(A)), then the reciprocal of the condition number is
  //  computed as
  //     RCOND = 1 / ( norm(A) * norm(inv(A)) ).
  
  //  Arguments
  //  =========
  
  //  NORM    (input) CHARACTER*1
  //          Specifies whether the 1-norm condition number or the
  //          infinity-norm condition number is required:
  //          = '1' or 'O':  1-norm
  //          = 'I':         Infinity-norm
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the matrix A is upper or lower triangular.
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  DIAG    (input) CHARACTER*1
  //          Specifies whether or not the matrix A is unit triangular.
  //          = 'N':  Non-unit triangular
  //          = 'U':  Unit triangular
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  KD      (input) INTEGER
  //          The number of superdiagonals or subdiagonals of the
  //          triangular band matrix A.  KD >= 0.
  
  //  AB      (input) COMPLEX*16 array, dimension (LDAB,N)
  //          The upper or lower triangular band matrix A, stored in the
  //          first kd+1 rows of the array. The j-th column of A is stored
  //          in the j-th column of the array AB as follows:
  //          if UPLO = 'U', AB(kd+1+i-j,j) = A(i,j) for max(1,j-kd)<=i<=j;
  //          if UPLO = 'L', AB(1+i-j,j)    = A(i,j) for j<=i<=min(n,j+kd).
  //          If DIAG = 'U', the diagonal elements of A are not referenced
  //          and are assumed to be 1.
  
  //  LDAB    (input) INTEGER
  //          The leading dimension of the array AB.  LDAB >= KD+1.
  
  //  RCOND   (output) DOUBLE PRECISION
  //          The reciprocal of the condition number of the matrix A,
  //          computed as RCOND = 1/(norm(A) * norm(inv(A))).
  
  //  WORK    (workspace) COMPLEX*16 array, dimension (2*N)
  
  //  RWORK   (workspace) DOUBLE PRECISION array, dimension (N)
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0:  if INFO = -k, the k-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Statement Functions ..
  //     ..
  //     .. Statement Function definitions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  onenrm = norm == '1' || lsame( norm, 'O' );
  nounit = lsame( diag, 'N' );
  
  if( !onenrm && !lsame( norm, 'I' ) ) { 
    info = -1;
  }
  else if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -2;
  }
  else if( !nounit && !lsame( diag, 'U' ) ) { 
    info = -3;
  }
  else if( n < 0 ) { 
    info = -4;
  }
  else if( kd < 0 ) { 
    info = -5;
  }
  else if( ldab < kd + 1 ) { 
    info = -7;
  }
  if( info != 0 ) { 
    xerbla( "ZTBCON", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) { 
    rcond = ONE;
    return;
  }
  
  rcond = ZERO;
  smlnum = dlamch( 'S'/*Safe minimum*/ )*(double)( max( n, 1 ) );
  
  //     Compute the 1-norm of the triangular matrix A or A'.
  
  anorm = zlantb( norm, uplo, diag, n, kd, ab, ldab, rwork );
  
  //     Continue only if ANORM > 0.
  
  if( anorm > ZERO ) { 
    
    //        Estimate the 1-norm of the inverse of A.
    
    ainvnm = ZERO;
    normin = 'N';
    if( onenrm ) { 
      kase1 = 1;
    }
    else { 
      kase1 = 2;
    }
    kase = 0;
L_10:
    ;
    zlacon( n, &work[n], work, ainvnm, kase );
    if( kase != 0 ) { 
      if( kase == kase1 ) { 
        
        //              Multiply by inv(A).
        
        zlatbs( uplo, 'N'/*No transpose*/, diag, normin, 
         n, kd, ab, ldab, work, scale, rwork, info );
      }
      else { 
        
        //              Multiply by inv(A').
        
        zlatbs( uplo, 'C'/*Conjugate transpose*/, diag, normin, 
         n, kd, ab, ldab, work, scale, rwork, info );
      }
      normin = 'Y';
      
      //           Multiply by 1/SCALE if doing so will not cause overflow.
      
      if( scale != ONE ) { 
        ix = izamax( n, work, 1 );
        xnorm = ztbcon_cabs1( work[ix - 1] );
        if( scale < xnorm*smlnum || scale == ZERO ) 
          goto L_20;
        zdrscl( n, scale, work, 1 );
      }
      goto L_10;
    }
    
    //        Compute the estimate of the reciprocal condition number.
    
    if( ainvnm != ZERO ) 
      rcond = (ONE/anorm)/ainvnm;
  }
  
L_20:
  ;
  return;
  
  //     End of ZTBCON
  
#undef  AB
} // end of function 


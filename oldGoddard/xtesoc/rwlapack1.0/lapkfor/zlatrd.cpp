/*
 * C++ implementation of Lapack routine zlatrd
 *
 * $Id: zlatrd.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:49:29
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zlatrd.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const DComplex ZERO = DComplex(0.0e0);
const DComplex ONE = DComplex(1.0e0);
const DComplex HALF = DComplex(0.5e0);
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zlatrd(const char &uplo, const long &n, const long &nb, DComplex *a, 
 const long &lda, double e[], DComplex tau[], DComplex *w, const long &ldw)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define W(I_,J_)  (*(w+(I_)*(ldw)+(J_)))
  long _do0, _do1, i, i_, iw;
  DComplex alpha;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZLATRD reduces NB rows and columns of a DComplex Hermitian matrix A to
  //  Hermitian tridiagonal form by a unitary similarity
  //  transformation Q' * A * Q, and returns the matrices V and W which are
  //  needed to apply the transformation to the unreduced part of A.
  
  //  If UPLO = 'U', ZLATRD reduces the last NB rows and columns of a
  //  matrix, of which the upper triangle is supplied;
  //  if UPLO = 'L', ZLATRD reduces the first NB rows and columns of a
  //  matrix, of which the lower triangle is supplied.
  
  //  This is an auxiliary routine called by ZHETRD.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER
  //          Specifies whether the upper or lower triangular part of the
  //          Hermitian matrix A is stored:
  //          = 'U': Upper triangular
  //          = 'L': Lower triangular
  
  //  N       (input) INTEGER
  //          The order of the matrix A.
  
  //  NB      (input) INTEGER
  //          The number of rows and columns to be reduced.
  
  //  A       (input/output) COMPLEX*16 array, dimension (LDA,N)
  //          On entry, the Hermitian matrix A.  If UPLO = 'U', the leading
  //          n-by-n upper triangular part of A contains the upper
  //          triangular part of the matrix A, and the strictly lower
  //          triangular part of A is not referenced.  If UPLO = 'L', the
  //          leading n-by-n lower triangular part of A contains the lower
  //          triangular part of the matrix A, and the strictly upper
  //          triangular part of A is not referenced.
  //          On exit:
  //          if UPLO = 'U', the last NB columns have been reduced to
  //            tridiagonal form, with the diagonal elements overwriting
  //            the diagonal elements of A; the elements above the diagonal
  //            with the array TAU, represent the unitary matrix Q as a
  //            product of elementary reflectors;
  //          if UPLO = 'L', the first NB columns have been reduced to
  //            tridiagonal form, with the diagonal elements overwriting
  //            the diagonal elements of A; the elements below the diagonal
  //            with the array TAU, represent the  unitary matrix Q as a
  //            product of elementary reflectors.
  //          See Further Details.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  E       (output) DOUBLE PRECISION array, dimension (N-1)
  //          If UPLO = 'U', E(n-nb:n-1) contains the superdiagonal
  //          elements of the last NB columns of the reduced matrix;
  //          if UPLO = 'L', E(1:nb) contains the subdiagonal elements of
  //          the first NB columns of the reduced matrix.
  
  //  TAU     (output) COMPLEX*16 array, dimension (N)
  //          The scalar factors of the elementary reflectors, stored in
  //          TAU(n-nb:n-1) if UPLO = 'U', and in TAU(1:nb) if UPLO = 'L'.
  //          See Further Details.
  
  //  W       (output) COMPLEX*16 array, dimension (LDW,NB)
  //          The n-by-nb matrix W required to update the unreduced part
  //          of A.
  
  //  LDW     (input) INTEGER
  //          The leading dimension of the array W. LDW >= max(1,N).
  
  //  Further Details
  //  ===============
  
  //  If UPLO = 'U', the matrix Q is represented as a product of elementary
  //  reflectors
  
  //     Q = H(n) H(n-1) . . . H(n-nb+1).
  
  //  Each H(i) has the form
  
  //     H(i) = I - tau * v * v'
  
  //  where tau is a DComplex scalar, and v is a DComplex vector with
  //  v(i:n) = 0 and v(i-1) = 1; v(1:i-1) is stored on exit in A(1:i-1,i),
  //  and tau in TAU(i-1).
  
  //  If UPLO = 'L', the matrix Q is represented as a product of elementary
  //  reflectors
  
  //     Q = H(1) H(2) . . . H(nb).
  
  //  Each H(i) has the form
  
  //     H(i) = I - tau * v * v'
  
  //  where tau is a DComplex scalar, and v is a DComplex vector with
  //  v(1:i) = 0 and v(i+1) = 1; v(i+1:n) is stored on exit in A(i+1:n,i),
  //  and tau in TAU(i).
  
  //  The elements of the vectors v together form the n-by-nb matrix V
  //  which is needed, with W, to apply the transformation to the unreduced
  //  part of the matrix, using a Hermitian rank-2k update of the form:
  //  A := A - V*W' - W*V'.
  
  //  The contents of A on exit are illustrated by the following examples
  //  with n = 5 and nb = 2:
  
  //  if UPLO = 'U':                       if UPLO = 'L':
  
  //    (  a   a   a   v4  v5 )              (  d                  )
  //    (      a   a   v4  v5 )              (  1   d              )
  //    (          a   1   v5 )              (  v1  1   a          )
  //    (              d   1  )              (  v1  v2  a   a      )
  //    (                  d  )              (  v1  v2  a   a   a  )
  
  //  where d denotes a diagonal element of the reduced matrix, a denotes
  //  an element of the original matrix that is unchanged, and vi denotes
  //  an element of the vector defining H(i).
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Quick return if possible
  
  if( n <= 0 ) 
    return;
  
  if( lsame( uplo, 'U' ) ) { 
    
    //        Reduce last NB columns of upper triangle
    
    for( i = n, i_ = i - 1, _do0 = n - nb + 1; i >= _do0; i--, i_-- ) { 
      iw = i - n + nb;
      if( i < n ) { 
        
        //              Update A(1:i,i)
        
        A(i_,i_) = DComplex(real( A(i_,i_) ));
        zlacgv( n - i, &W(iw,i_), ldw );
        zgemv( 'N'/*No transpose*/, i, n - i, -(ONE), &A(i_ + 1,0), 
         lda, &W(iw,i_), ldw, ONE, &A(i_,0), 1 );
        zlacgv( n - i, &W(iw,i_), ldw );
        zlacgv( n - i, &A(i_ + 1,i_), lda );
        zgemv( 'N'/*No transpose*/, i, n - i, -(ONE), &W(iw,0), 
         ldw, &A(i_ + 1,i_), lda, ONE, &A(i_,0), 1 );
        zlacgv( n - i, &A(i_ + 1,i_), lda );
        A(i_,i_) = DComplex(real( A(i_,i_) ));
      }
      if( i > 1 ) { 
        
        //              Generate elementary reflector H(i) to annihilate
        //              A(1:i-2,i)
        
        alpha = A(i_,i_ - 1);
        zlarfg( i - 1, alpha, &A(i_,0), 1, tau[i_ - 1] );
        e[i_ - 1] = real(alpha);
        A(i_,i_ - 1) = ONE;
        
        //              Compute W(1:i-1,i)
        
        zhemv( 'U'/*Upper*/, i - 1, ONE, a, lda, &A(i_,0), 
         1, ZERO, &W(iw - 1,0), 1 );
        if( i < n ) { 
          zgemv( 'C'/*Conjugate transpose*/, i - 1, n - 
           i, ONE, &W(iw,0), ldw, &A(i_,0), 1, ZERO, &W(iw - 1,i_ + 1), 
           1 );
          zgemv( 'N'/*No transpose*/, i - 1, n - i, -(ONE), 
           &A(i_ + 1,0), lda, &W(iw - 1,i_ + 1), 1, ONE, 
           &W(iw - 1,0), 1 );
          zgemv( 'C'/*Conjugate transpose*/, i - 1, n - 
           i, ONE, &A(i_ + 1,0), lda, &A(i_,0), 1, ZERO, 
           &W(iw - 1,i_ + 1), 1 );
          zgemv( 'N'/*No transpose*/, i - 1, n - i, -(ONE), 
           &W(iw,0), ldw, &W(iw - 1,i_ + 1), 1, ONE, &W(iw - 1,0), 
           1 );
        }
        zscal( i - 1, tau[i_ - 1], &W(iw - 1,0), 1 );
        alpha = -(HALF*tau[i_ - 1]*zdotc( i - 1, &W(iw - 1,0), 
         1, &A(i_,0), 1 ));
        zaxpy( i - 1, alpha, &A(i_,0), 1, &W(iw - 1,0), 1 );
      }
      
    }
  }
  else { 
    
    //        Reduce first NB columns of lower triangle
    
    for( i = 1, i_ = i - 1, _do1 = nb; i <= _do1; i++, i_++ ) { 
      
      //           Update A(i:n,i)
      
      A(i_,i_) = DComplex(real( A(i_,i_) ));
      zlacgv( i - 1, &W(0,i_), ldw );
      zgemv( 'N'/*No transpose*/, n - i + 1, i - 1, -(ONE), 
       &A(0,i_), lda, &W(0,i_), ldw, ONE, &A(i_,i_), 1 );
      zlacgv( i - 1, &W(0,i_), ldw );
      zlacgv( i - 1, &A(0,i_), lda );
      zgemv( 'N'/*No transpose*/, n - i + 1, i - 1, -(ONE), 
       &W(0,i_), ldw, &A(0,i_), lda, ONE, &A(i_,i_), 1 );
      zlacgv( i - 1, &A(0,i_), lda );
      A(i_,i_) = DComplex(real( A(i_,i_) ));
      if( i < n ) { 
        
        //              Generate elementary reflector H(i) to annihilate
        //              A(i+2:n,i)
        
        alpha = A(i_,i_ + 1);
        zlarfg( n - i, alpha, &A(i_,min( i + 2, n ) - 1), 
         1, tau[i_] );
        e[i_] = real(alpha);
        A(i_,i_ + 1) = ONE;
        
        //              Compute W(i+1:n,i)
        
        zhemv( 'L'/*Lower*/, n - i, ONE, &A(i_ + 1,i_ + 1), 
         lda, &A(i_,i_ + 1), 1, ZERO, &W(i_,i_ + 1), 1 );
        zgemv( 'C'/*Conjugate transpose*/, n - i, i - 1, 
         ONE, &W(0,i_ + 1), ldw, &A(i_,i_ + 1), 1, ZERO, &W(i_,0), 
         1 );
        zgemv( 'N'/*No transpose*/, n - i, i - 1, -(ONE), 
         &A(0,i_ + 1), lda, &W(i_,0), 1, ONE, &W(i_,i_ + 1), 
         1 );
        zgemv( 'C'/*Conjugate transpose*/, n - i, i - 1, 
         ONE, &A(0,i_ + 1), lda, &A(i_,i_ + 1), 1, ZERO, &W(i_,0), 
         1 );
        zgemv( 'N'/*No transpose*/, n - i, i - 1, -(ONE), 
         &W(0,i_ + 1), ldw, &W(i_,0), 1, ONE, &W(i_,i_ + 1), 
         1 );
        zscal( n - i, tau[i_], &W(i_,i_ + 1), 1 );
        alpha = -(HALF*tau[i_]*zdotc( n - i, &W(i_,i_ + 1), 
         1, &A(i_,i_ + 1), 1 ));
        zaxpy( n - i, alpha, &A(i_,i_ + 1), 1, &W(i_,i_ + 1), 
         1 );
      }
      
    }
  }
  
  return;
  
  //     End of ZLATRD
  
#undef  W
#undef  A
} // end of function 


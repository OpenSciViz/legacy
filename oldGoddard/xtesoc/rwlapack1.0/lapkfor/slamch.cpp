/*
 * C++ implementation of Lapack routine slamch
 *
 * $Id: slamch.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:59:49
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: slamch.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

RWLAPKDECL void slamc1(long&,long&,int&,int&);
RWLAPKDECL void slamc2(long&,long&,int&,float&,long&,float&,long&,float&);
RWLAPKDECL float slamc3(const float&,const float&);
RWLAPKDECL void slamc4(long&,const float&,const long&);
RWLAPKDECL void slamc5(const long&,const long&,const long&,const int&,long&,float&);

// PARAMETER translations
const float ONE = 1.0e0;
const float ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL float /*FUNCTION*/ slamch(const char &cmach)
{
  int lrnd;
  long beta, imax, imin, it;
  float rmach, slamch_v, small;
  static float base, emax, emin, eps, prec, rmax, rmin, rnd, sfmin, 
   t;
  static int first = TRUE;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SLAMCH determines single precision machine parameters.
  
  //  Arguments
  //  =========
  
  //  CMACH   (input) CHARACTER*1
  //          Specifies the value to be returned by SLAMCH:
  //          = 'E' or 'e',   SLAMCH := eps
  //          = 'S' or 's ,   SLAMCH := sfmin
  //          = 'B' or 'b',   SLAMCH := base
  //          = 'P' or 'p',   SLAMCH := eps*base
  //          = 'N' or 'n',   SLAMCH := t
  //          = 'R' or 'r',   SLAMCH := rnd
  //          = 'M' or 'm',   SLAMCH := emin
  //          = 'U' or 'u',   SLAMCH := rmin
  //          = 'L' or 'l',   SLAMCH := emax
  //          = 'O' or 'o',   SLAMCH := rmax
  
  //          where
  
  //          eps   = relative machine precision
  //          sfmin = safe minimum, such that 1/sfmin does not overflow
  //          base  = base of the machine
  //          prec  = eps*base
  //          t     = number of (base) digits in the mantissa
  //          rnd   = 1.0 when rounding occurs in addition, 0.0 otherwise
  //          emin  = minimum exponent before (gradual) underflow
  //          rmin  = underflow threshold - base**(emin-1)
  //          emax  = largest exponent before overflow
  //          rmax  = overflow threshold  - (base**emax)*(1-eps)
  
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Save statement ..
  //     ..
  //     .. Data statements ..
  //     ..
  //     .. Executable Statements ..
  
  if( first ) { 
    first = FALSE;
    slamc2( beta, it, lrnd, eps, imin, rmin, imax, rmax );
    base = beta;
    t = it;
    if( lrnd ) { 
      rnd = ONE;
      eps = (pow(base, 1 - it))/2;
    }
    else { 
      rnd = ZERO;
      eps = pow(base, 1 - it);
    }
    prec = eps*base;
    emin = imin;
    emax = imax;
    sfmin = rmin;
    small = ONE/rmax;
    if( small >= sfmin ) { 
      
      //           Use SMALL plus a bit, to avoid the possibility of rounding
      //           causing overflow when computing  1/sfmin.
      
      sfmin = small*(ONE + eps);
    }
  }
  
  if( lsame( cmach, 'E' ) ) { 
    rmach = eps;
  }
  else if( lsame( cmach, 'S' ) ) { 
    rmach = sfmin;
  }
  else if( lsame( cmach, 'B' ) ) { 
    rmach = base;
  }
  else if( lsame( cmach, 'P' ) ) { 
    rmach = prec;
  }
  else if( lsame( cmach, 'N' ) ) { 
    rmach = t;
  }
  else if( lsame( cmach, 'R' ) ) { 
    rmach = rnd;
  }
  else if( lsame( cmach, 'M' ) ) { 
    rmach = emin;
  }
  else if( lsame( cmach, 'U' ) ) { 
    rmach = rmin;
  }
  else if( lsame( cmach, 'L' ) ) { 
    rmach = emax;
  }
  else if( lsame( cmach, 'O' ) ) { 
    rmach = rmax;
  }
  
  slamch_v = rmach;
  return( slamch_v );
  
  //     End of SLAMCH
  
} // end of function 

// ***********************************************************************

RWLAPKDECL void /*FUNCTION*/ slamc1(long &beta, long &t, int &rnd, 
   int &ieee1)
{
  static int lieee1, lrnd;
  static long lbeta, lt;
  float a, b, c, f, one, qtr, savec, t1, t2;
  static int first = TRUE;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SLAMC1 determines the machine parameters given by BETA, T, RND, and
  //  IEEE1.
  
  //  Arguments
  //  =========
  
  //  BETA    (output) INTEGER
  //          The base of the machine.
  
  //  T       (output) INTEGER
  //          The number of ( BETA ) digits in the mantissa.
  
  //  RND     (output) LOGICAL
  //          Specifies whether proper rounding  ( RND = .TRUE. )  or
  //          chopping  ( RND = .FALSE. )  occurs in addition. This may not
  //          be a reliable guide to the way in which the machine performs
  //          its arithmetic.
  
  //  IEEE1   (output) LOGICAL
  //          Specifies whether rounding appears to be done in the IEEE
  //          'round to nearest' style.
  
  //  Further Details
  //  ===============
  
  //  The routine is based on the routine  ENVRON  by Malcolm and
  //  incorporates suggestions by Gentleman and Marovich. See
  
  //     Malcolm M. A. (1972) Algorithms to reveal properties of
  //        floating-point arithmetic. Comms. of the ACM, 15, 949-951.
  
  //     Gentleman W. M. and Marovich S. B. (1974) More on algorithms
  //        that reveal properties of floating point arithmetic units.
  //        Comms. of the ACM, 17, 276-277.
  
  
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Save statement ..
  //     ..
  //     .. Data statements ..
  //     ..
  //     .. Executable Statements ..
  
  if( first ) { 
    first = FALSE;
    one = 1;
    
    //        LBETA,  LIEEE1,  LT and  LRND  are the  local values  of  BETA,
    //        IEEE1, T and RND.
    
    //        Throughout this routine  we use the function  SLAMC3  to ensure
    //        that relevant values are  stored and not held in registers,  or
    //        are not affected by optimizers.
    
    //        Compute  a = 2.0**m  with the  smallest positive integer m such
    //        that
    
    //           fl( a + 1.0 ) = a.
    
    a = 1;
    c = 1;
    
    //+       WHILE( C.EQ.ONE )LOOP
L_10:
    ;
    if( c == one ) { 
      a = 2*a;
      c = slamc3( a, one );
      c = slamc3( c, -a );
      goto L_10;
    }
    //+       END WHILE
    
    //        Now compute  b = 2.0**m  with the smallest positive integer m
    //        such that
    
    //           fl( a + b ) .gt. a.
    
    b = 1;
    c = slamc3( a, b );
    
    //+       WHILE( C.EQ.A )LOOP
L_20:
    ;
    if( c == a ) { 
      b = 2*b;
      c = slamc3( a, b );
      goto L_20;
    }
    //+       END WHILE
    
    //        Now compute the base.  a and c  are neighbouring floating point
    //        numbers  in the  interval  ( beta**t, beta**( t + 1 ) )  and so
    //        their difference is beta. Adding 0.25 to c is to ensure that it
    //        is truncated to beta and not ( beta - 1 ).
    
    qtr = one/4;
    savec = c;
    c = slamc3( c, -a );
    lbeta = c + qtr;
    
    //        Now determine whether rounding or chopping occurs,  by adding a
    //        bit  less  than  beta/2  and a  bit  more  than  beta/2  to  a.
    
    b = lbeta;
    f = slamc3( b/2, -b/100 );
    c = slamc3( f, a );
    if( c == a ) { 
      lrnd = TRUE;
    }
    else { 
      lrnd = FALSE;
    }
    f = slamc3( b/2, b/100 );
    c = slamc3( f, a );
    if( (lrnd) && (c == a) ) 
      lrnd = FALSE;
    
    //        Try and decide whether rounding is done in the  IEEE  'round to
    //        nearest' style. B/2 is half a unit in the last place of the two
    //        numbers A and SAVEC. Furthermore, A is even, i.e. has last  bit
    //        zero, and SAVEC is odd. Thus adding B/2 to A should not  change
    //        A, but adding B/2 to SAVEC should change SAVEC.
    
    t1 = slamc3( b/2, a );
    t2 = slamc3( b/2, savec );
    lieee1 = ((t1 == a) && (t2 > savec)) && lrnd;
    
    //        Now find  the  mantissa, t.  It should  be the  integer part of
    //        log to the base beta of a,  however it is safer to determine  t
    //        by powering.  So we find t as the smallest positive integer for
    //        which
    
    //           fl( beta**t + 1.0 ) = 1.0.
    
    lt = 0;
    a = 1;
    c = 1;
    
    //+       WHILE( C.EQ.ONE )LOOP
L_30:
    ;
    if( c == one ) { 
      lt = lt + 1;
      a = a*lbeta;
      c = slamc3( a, one );
      c = slamc3( c, -a );
      goto L_30;
    }
    //+       END WHILE
    
  }
  
  beta = lbeta;
  t = lt;
  rnd = lrnd;
  ieee1 = lieee1;
  return;
  
  //     End of SLAMC1
  
} // end of function 

// ***********************************************************************

RWLAPKDECL void /*FUNCTION*/ slamc2(long &beta, long &t, int &rnd, 
    float &eps,  long &emin,  float &rmin,  long &emax,  float &rmax)
{
  int ieee, lieee1, lrnd;
  long _do0, gnmin, gpmin, i, i_, ngnmin, ngpmin;
  static long lbeta, lemax, lemin, lt;
  float a, b, c, half, one, rbase, sixth, small, third, two, zero;
  static float leps, lrmax, lrmin;
  static int first = TRUE;
  static int iwarn = FALSE;
#if 0
  static F77FMT _fmts[] = {
   9999, "(//' WARNING. The value EMIN may be incorrect:-','  EMIN = ',\
i8,/' If, after inspection, the value EMIN looks',\
' acceptable please comment out ',/\
' the IF block as marked within the code of routine',' SLAMC2,',/\
' otherwise supply EMIN explicitly.',/)",
   0L,"" };
#endif
  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SLAMC2 determines the machine parameters specified in its argument
  //  list.
  
  //  Arguments
  //  =========
  
  //  BETA    (output) INTEGER
  //          The base of the machine.
  
  //  T       (output) INTEGER
  //          The number of ( BETA ) digits in the mantissa.
  
  //  RND     (output) LOGICAL
  //          Specifies whether proper rounding  ( RND = .TRUE. )  or
  //          chopping  ( RND = .FALSE. )  occurs in addition. This may not
  //          be a reliable guide to the way in which the machine performs
  //          its arithmetic.
  
  //  EPS     (output) REAL
  //          The smallest positive number such that
  
  //             fl( 1.0 - EPS ) .LT. 1.0,
  
  //          where fl denotes the computed value.
  
  //  EMIN    (output) INTEGER
  //          The minimum exponent before (gradual) underflow occurs.
  
  //  RMIN    (output) REAL
  //          The smallest normalized number for the machine, given by
  //          BASE**( EMIN - 1 ), where  BASE  is the floating point value
  //          of BETA.
  
  //  EMAX    (output) INTEGER
  //          The maximum exponent before overflow occurs.
  
  //  RMAX    (output) REAL
  //          The largest positive number for the machine, given by
  //          BASE**EMAX * ( 1 - EPS ), where  BASE  is the floating point
  //          value of BETA.
  
  //  Further Details
  //  ===============
  
  //  The computation of  EPS  is based on a routine PARANOIA by
  //  W. Kahan of the University of California at Berkeley.
  
  
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Save statement ..
  //     ..
  //     .. Data statements ..
  //     ..
  //     .. Executable Statements ..
  
  if( first ) { 
    first = FALSE;
    zero = 0;
    one = 1;
    two = 2;
    
    //        LBETA, LT, LRND, LEPS, LEMIN and LRMIN  are the local values of
    //        BETA, T, RND, EPS, EMIN and RMIN.
    
    //        Throughout this routine  we use the function  SLAMC3  to ensure
    //        that relevant values are stored  and not held in registers,  or
    //        are not affected by optimizers.
    
    //        SLAMC1 returns the parameters  LBETA, LT, LRND and LIEEE1.
    
    slamc1( lbeta, lt, lrnd, lieee1 );
    
    //        Start to find EPS.
    
    b = lbeta;
    a = pow(b, -lt);
    leps = a;
    
    //        Try some tricks to see whether or not this is the correct  EPS.
    
    b = two/3;
    half = one/2;
    sixth = slamc3( b, -half );
    third = slamc3( sixth, sixth );
    b = slamc3( third, -half );
    b = slamc3( b, sixth );
    b = abs( b );
    if( b < leps ) 
      b = leps;
    
    leps = 1;
    
    //+       WHILE( ( LEPS.GT.B ).AND.( B.GT.ZERO ) )LOOP
L_10:
    ;
    if( (leps > b) && (b > zero) ) { 
      leps = b;
      c = slamc3( half*leps, (pow(two, 5))*(pow(leps, 2)) );
      c = slamc3( half, -c );
      b = slamc3( half, c );
      c = slamc3( half, -b );
      b = slamc3( half, c );
      goto L_10;
    }
    //+       END WHILE
    
    if( a < leps ) 
      leps = a;
    
    //        Computation of EPS complete.
    
    //        Now find  EMIN.  Let A = + or - 1, and + or - (1 + BASE**(-3)).
    //        Keep dividing  A by BETA until (gradual) underflow occurs. This
    //        is detected when we cannot recover the previous A.
    
    rbase = one/lbeta;
    small = one;
    for( i = 1, i_ = i - 1; i <= 3; i++, i_++ ) { 
      small = slamc3( small*rbase, zero );
    }
    a = slamc3( one, small );
    slamc4( ngpmin, one, lbeta );
    slamc4( ngnmin, -one, lbeta );
    slamc4( gpmin, a, lbeta );
    slamc4( gnmin, -a, lbeta );
    ieee = FALSE;
    
    if( (ngpmin == ngnmin) && (gpmin == gnmin) ) { 
      if( ngpmin == gpmin ) { 
        lemin = ngpmin;
        //            ( Non twos-complement machines, no gradual underflow;
        //              e.g.,  VAX )
      }
      else if( (gpmin - ngpmin) == 3 ) { 
        lemin = ngpmin - 1 + lt;
        ieee = TRUE;
        //            ( Non twos-complement machines, with gradual underflow;
        //              e.g., IEEE standard followers )
      }
      else { 
        lemin = min( ngpmin, gpmin );
        //            ( A guess; no known machine )
        iwarn = TRUE;
      }
      
    }
    else if( (ngpmin == gpmin) && (ngnmin == gnmin) ) { 
      if( abs( ngpmin - ngnmin ) == 1 ) { 
        lemin = max( ngpmin, ngnmin );
        //            ( Twos-complement machines, no gradual underflow;
        //              e.g., CYBER 205 )
      }
      else { 
        lemin = min( ngpmin, ngnmin );
        //            ( A guess; no known machine )
        iwarn = TRUE;
      }
      
    }
    else if( (abs( ngpmin - ngnmin ) == 1) && (gpmin == gnmin) ) { 
      if( (gpmin - min( ngpmin, ngnmin )) == 3 ) { 
        lemin = max( ngpmin, ngnmin ) - 1 + lt;
        //            ( Twos-complement machines with gradual underflow;
        //              no known machine )
      }
      else { 
        lemin = min( ngpmin, ngnmin );
        //            ( A guess; no known machine )
        iwarn = TRUE;
      }
      
    }
    else { 
      lemin = vmin( ngpmin, ngnmin, gpmin, gnmin, IEND );
      //         ( A guess; no known machine )
      iwarn = TRUE;
    }
    // **
    // Comment out this if block if EMIN is ok
#if 0    
    if( iwarn ) { 
      first = TRUE;
      writef( 6, FMTR(9999), "%ld\n", lemin );
    }
#endif    
    // **
    
    //        Assume IEEE arithmetic if we found denormalised  numbers above,
    //        or if arithmetic seems to round in the  IEEE style,  determined
    //        in routine SLAMC1. A true IEEE machine should have both  things
    //        true; however, faulty machines may have one or the other.
    
    ieee = ieee || lieee1;
    
    //        Compute  RMIN by successive division by  BETA. We could compute
    //        RMIN as BASE**( EMIN - 1 ),  but some machines underflow during
    //        this computation.
    
    lrmin = 1;
    for( i = 1, i_ = i - 1, _do0 = 1 - lemin; i <= _do0; i++, i_++ ) { 
      lrmin = slamc3( lrmin*rbase, zero );
    }
    
    //        Finally, call SLAMC5 to compute EMAX and RMAX.
    
    slamc5( lbeta, lt, lemin, ieee, lemax, lrmax );
  }
  
  beta = lbeta;
  t = lt;
  rnd = lrnd;
  eps = leps;
  emin = lemin;
  rmin = lrmin;
  emax = lemax;
  rmax = lrmax;
  
  return;
  
  
  //     End of SLAMC2
  
} // end of function 

// ***********************************************************************

RWLAPKDECL float /*FUNCTION*/ slamc3(const float &a, const float &b)
{
  float slamc3_v;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SLAMC3  is intended to force  A  and  B  to be stored prior to doing
  //  the addition of  A  and  B ,  for use in situations where optimizers
  //  might hold one of these in a register.
  
  //  Arguments
  //  =========
  
  //  A, B    (input) REAL
  //          The values A and B.
  
  
  //     .. Executable Statements ..
  
  slamc3_v = a + b;
  
  return( slamc3_v );
  
  //     End of SLAMC3
  
} // end of function 

// ***********************************************************************

RWLAPKDECL void /*FUNCTION*/ slamc4(long &emin, const float &start, const long &base)
{
  long _do0, _do1, i, i_;
  float a, b1, b2, c1, c2, d1, d2, one, rbase, zero;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SLAMC4 is a service routine for SLAMC2.
  
  //  Arguments
  //  =========
  
  //  EMIN    (output) EMIN
  //          The minimum exponent before (gradual) underflow, computed by
  //          setting A = START and dividing by BASE until the previous A
  //          can not be recovered.
  
  //  START   (input) REAL
  //          The starting point for determining EMIN.
  
  //  BASE    (input) INTEGER
  //          The base of the machine.
  
  
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Executable Statements ..
  
  a = start;
  one = 1;
  rbase = one/base;
  zero = 0;
  emin = 1;
  b1 = slamc3( a*rbase, zero );
  c1 = a;
  c2 = a;
  d1 = a;
  d2 = a;
  //+    WHILE( ( C1.EQ.A ).AND.( C2.EQ.A ).AND.
  //    $       ( D1.EQ.A ).AND.( D2.EQ.A )      )LOOP
L_10:
  ;
  if( (((c1 == a) && (c2 == a)) && (d1 == a)) && (d2 == a) ) { 
    emin = emin - 1;
    a = b1;
    b1 = slamc3( a/base, zero );
    c1 = slamc3( b1*base, zero );
    d1 = zero;
    for( i = 1, i_ = i - 1, _do0 = base; i <= _do0; i++, i_++ ) { 
      d1 = d1 + b1;
    }
    b2 = slamc3( a*rbase, zero );
    c2 = slamc3( b2/rbase, zero );
    d2 = zero;
    for( i = 1, i_ = i - 1, _do1 = base; i <= _do1; i++, i_++ ) { 
      d2 = d2 + b2;
    }
    goto L_10;
  }
  //+    END WHILE
  
  return;
  
  //     End of SLAMC4
  
} // end of function 

// ***********************************************************************

RWLAPKDECL void /*FUNCTION*/ slamc5(const long &beta, const long &p, const long &emin, 
   const int &ieee, long &emax, float &rmax)
{
  long _do0, _do1, exbits, expsum, i, i_, lexp, nbits, try_, 
   uexp;
  float oldy, recbas, y, z;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SLAMC5 attempts to compute RMAX, the largest machine floating-point
  //  number, without overflow.  It assumes that EMAX + abs(EMIN) sum
  //  approximately to a power of 2.  It will fail on machines where this
  //  assumption does not hold, for example, the Cyber 205 (EMIN = -28625,
  //  EMAX = 28718).  It will also fail if the value supplied for EMIN is
  //  too large (i.e. too close to zero), probably with overflow.
  
  //  Arguments
  //  =========
  
  //  BETA    (input) INTEGER
  //          The base of floating-point arithmetic.
  
  //  P       (input) INTEGER
  //          The number of base BETA digits in the mantissa of a
  //          floating-point value.
  
  //  EMIN    (input) INTEGER
  //          The minimum exponent before (gradual) underflow.
  
  //  IEEE    (input) LOGICAL
  //          A logical flag specifying whether or not the arithmetic
  //          system is thought to comply with the IEEE standard.
  
  //  EMAX    (output) INTEGER
  //          The largest exponent before overflow
  
  //  RMAX    (output) REAL
  //          The largest machine floating-point number.
  
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     First compute LEXP and UEXP, two powers of 2 that bound
  //     abs(EMIN). We then assume that EMAX + abs(EMIN) will sum
  //     approximately to the bound that is closest to abs(EMIN).
  //     (EMAX is the exponent of the required number RMAX).
  
  lexp = 1;
  exbits = 1;
L_10:
  ;
  try_ = lexp*2;
  if( try_ <= (-emin) ) { 
    lexp = try_;
    exbits = exbits + 1;
    goto L_10;
  }
  if( lexp == -emin ) { 
    uexp = lexp;
  }
  else { 
    uexp = try_;
    exbits = exbits + 1;
  }
  
  //     Now -LEXP is less than or equal to EMIN, and -UEXP is greater
  //     than or equal to EMIN. EXBITS is the number of bits needed to
  //     store the exponent.
  
  if( (uexp + emin) > (-lexp - emin) ) { 
    expsum = 2*lexp;
  }
  else { 
    expsum = 2*uexp;
  }
  
  //     EXPSUM is the exponent range, approximately equal to
  //     EMAX - EMIN + 1 .
  
  emax = expsum + emin - 1;
  nbits = 1 + exbits + p;
  
  //     NBITS is the total number of bits needed to store a
  //     floating-point number.
  
  if( (nbits%2/*mod( nbits, 2 )*/ == 1) && (beta == 2) ) { 
    
    //        Either there are an odd number of bits used to store a
    //        floating-point number, which is unlikely, or some bits are
    //        not used in the representation of numbers, which is possible,
    //        (e.g. Cray machines) or the mantissa has an implicit bit,
    //        (e.g. IEEE machines, Dec Vax machines), which is perhaps the
    //        most likely. We have to assume the last alternative.
    //        If this is true, then we need to reduce EMAX by one because
    //        there must be some way of representing zero in an implicit-bit
    //        system. On machines like Cray, we are reducing EMAX by one
    //        unnecessarily.
    
    emax = emax - 1;
  }
  
  if( ieee ) { 
    
    //        Assume we are on an IEEE machine which reserves one exponent
    //        for infinity and NaN.
    
    emax = emax - 1;
  }
  
  //     Now create RMAX, the largest machine number, which should
  //     be equal to (1.0 - BETA**(-P)) * BETA**EMAX .
  
  //     First compute 1.0 - BETA**(-P), being careful that the
  //     result is less than 1.0 .
  
  recbas = ONE/beta;
  z = beta - ONE;
  y = ZERO;
  for( i = 1, i_ = i - 1, _do0 = p; i <= _do0; i++, i_++ ) { 
    z = z*recbas;
    if( y < ONE ) 
      oldy = y;
    y = slamc3( y, z );
  }
  if( y >= ONE ) 
    y = oldy;
  
  //     Now multiply by BETA**EMAX to get RMAX.
  
  for( i = 1, i_ = i - 1, _do1 = emax; i <= _do1; i++, i_++ ) { 
    y = slamc3( y*beta, ZERO );
  }
  
  rmax = y;
  return;
  
  //     End of SLAMC5
  
} // end of function 


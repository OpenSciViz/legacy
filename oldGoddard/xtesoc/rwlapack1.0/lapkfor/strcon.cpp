/*
 * C++ implementation of Lapack routine strcon
 *
 * $Id: strcon.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:03:28
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: strcon.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ONE = 1.0e0;
const float ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ strcon(const char &norm, const char &uplo, const char &diag, long &n, 
 float *a, long &lda, float &rcond, float work[], long iwork[], 
 long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  int nounit, onenrm, upper;
  char normin;
  long ix, kase, kase1;
  float ainvnm, anorm, scale, smlnum, xnorm;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  STRCON estimates the reciprocal of the condition number of a
  //  triangular matrix A, in either the 1-norm or the infinity-norm.
  
  //  The norm of A is computed and an estimate is obtained for
  //  norm(inv(A)), then the reciprocal of the condition number is
  //  computed as
  //     RCOND = 1 / ( norm(A) * norm(inv(A)) ).
  
  //  Arguments
  //  =========
  
  //  NORM    (input) CHARACTER*1
  //          Specifies whether the 1-norm condition number or the
  //          infinity-norm condition number is required:
  //          = '1' or 'O':  1-norm
  //          = 'I':         Infinity-norm
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the matrix A is upper or lower triangular.
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  DIAG    (input) CHARACTER*1
  //          Specifies whether or not the matrix A is unit triangular.
  //          = 'N':  Non-unit triangular
  //          = 'U':  Unit triangular
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  A       (input) REAL array, dimension (LDA,N)
  //          The triangular matrix A.  If UPLO = 'U', the leading n by n
  //          upper triangular part of the array A contains the upper
  //          triangular matrix, and the strictly lower triangular part of
  //          A is not referenced.  If UPLO = 'L', the leading n by n lower
  //          triangular part of the array A contains the lower triangular
  //          matrix, and the strictly upper triangular part of A is not
  //          referenced.  If DIAG = 'U', the diagonal elements of A are
  //          also not referenced and are assumed to be 1.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  RCOND   (output) REAL
  //          The reciprocal of the condition number of the matrix A,
  //          computed as RCOND = 1/(norm(A) * norm(inv(A))).
  
  //  WORK    (workspace) REAL array, dimension (3*N)
  
  //  IWORK   (workspace) INTEGER array, dimension (N)
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0:  if INFO = -k, the k-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  onenrm = norm == '1' || lsame( norm, 'O' );
  nounit = lsame( diag, 'N' );
  
  if( !onenrm && !lsame( norm, 'I' ) ) { 
    info = -1;
  }
  else if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -2;
  }
  else if( !nounit && !lsame( diag, 'U' ) ) { 
    info = -3;
  }
  else if( n < 0 ) { 
    info = -4;
  }
  else if( lda < max( 1, n ) ) { 
    info = -6;
  }
  if( info != 0 ) { 
    xerbla( "STRCON", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) { 
    rcond = ONE;
    return;
  }
  
  rcond = ZERO;
  smlnum = slamch( 'S'/*Safe minimum*/ )*(float)( max( 1, n ) );
  
  //     Compute the norm of the triangular matrix A.
  
  anorm = slantr( norm, uplo, diag, n, n, a, lda, work );
  
  //     Continue only if ANORM > 0.
  
  if( anorm > ZERO ) { 
    
    //        Estimate the norm of the inverse of A.
    
    ainvnm = ZERO;
    normin = 'N';
    if( onenrm ) { 
      kase1 = 1;
    }
    else { 
      kase1 = 2;
    }
    kase = 0;
L_10:
    ;
    slacon( n, &work[n], work, iwork, ainvnm, kase );
    if( kase != 0 ) { 
      if( kase == kase1 ) { 
        
        //              Multiply by inv(A).
        
        slatrs( uplo, 'N'/*No transpose*/, diag, normin, 
         n, a, lda, work, scale, &work[n*2], info );
      }
      else { 
        
        //              Multiply by inv(A').
        
        slatrs( uplo, 'T'/*Transpose*/, diag, normin, n, 
         a, lda, work, scale, &work[n*2], info );
      }
      normin = 'Y';
      
      //           Multiply by 1/SCALE if doing so will not cause overflow.
      
      if( scale != ONE ) { 
        ix = isamax( n, work, 1 );
        xnorm = abs( work[ix - 1] );
        if( scale < xnorm*smlnum || scale == ZERO ) 
          goto L_20;
        srscl( n, scale, work, 1 );
      }
      goto L_10;
    }
    
    //        Compute the estimate of the reciprocal condition number.
    
    if( ainvnm != ZERO ) 
      rcond = (ONE/anorm)/ainvnm;
  }
  
L_20:
  ;
  return;
  
  //     End of STRCON
  
#undef  A
} // end of function 


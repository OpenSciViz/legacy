/*
 * C++ implementation of lapack routine dhsein
 *
 * $Id: dhsein.cpp,v 1.6 1993/04/06 20:40:50 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:34:51
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dhsein.cpp,v $
 * Revision 1.6  1993/04/06  20:40:50  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.5  1993/03/19  18:41:23  alv
 * now passes chars explicitly, rather than indirection of a string, to shut up SUN warnings
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:15:03  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:07:01  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dhsein(const char &job, const char &eigsrc, const char &initv, int select[], 
 long &n, double *h, long &ldh, double wr[], double wi[], 
 double *vl, long &ldvl, double *vr, long &ldvr, long &mm, 
 long &m, double work[], long ifaill[], long ifailr[], 
 long &info)
{
#define H(I_,J_)  (*(h+(I_)*(ldh)+(J_)))
#define VL(I_,J_) (*(vl+(I_)*(ldvl)+(J_)))
#define VR(I_,J_) (*(vr+(I_)*(ldvr)+(J_)))
  int bothv, fromqr, leftv, noinit, pair, rightv;
  long _do0, _do1, _do2, _do3, _do4, _do5, _do6, _do7, _do8, 
   i, i_, iinfo, k, k_, kl, kln, kr, ksi, ksr, ldwork;
  double bignum, eps3, hnorm, smlnum, ulp, unfl, wki, wkr;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DHSEIN uses inverse iteration to find specified right and/or left
  //  eigenvectors of a real upper Hessenberg matrix H.
  
  //  The right eigenvector x and the left eigenvector y of the matrix H
  //  corresponding to an eigenvalue w are defined by:
  
  //               H x = w x,     y' H = w y'
  
  //  where y' denotes the conjugate transpose of the vector y.
  
  //  Arguments
  //  =========
  
  //  JOB     (input) CHARACTER*1
  //          = 'R': compute right eigenvectors only;
  //          = 'L': compute left eigenvectors only;
  //          = 'B': compute both right and left eigenvectors.
  
  //  EIGSRC  (input) CHARACTER*1
  //          Specifies the source of eigenvalues supplied in WR and WI:
  //          = 'Q': the eigenvalues were found using DHSEQR; thus, if
  //                 H has zero sub-diagonal entries, and so is
  //                 block-triangular, then the j-th eigenvalue can be
  //                 assumed to be an eigenvalue of the block containing
  //                 the j-th row/column.  This property allows DHSEIN to
  //                 perform inverse iteration on just one diagonal block.
  //          = 'N': no assumptions are made on the correspondence
  //                 between eigenvalues and diagonal blocks.  In this
  //                 case, DHSEIN must always perform inverse iteration
  //                 using the whole matrix H.
  
  //  INITV   (input) CHARACTER*1
  //          Specifies whether initial starting vectors are supplied for
  //          inverse iteration:
  //          = 'N': no initial vectors are supplied;
  //          = 'U': user-supplied initial vectors are stored in the arrays
  //                 VL and/or VR.
  
  //  SELECT  (input/output) LOGICAL array, dimension(N)
  //          Specifies the eigenvectors to be computed. To select the
  //          real eigenvector corresponding to a real eigenvalue WR(j),
  //          SELECT(j) must be set to .TRUE.. To select the DComplex
  //          eigenvector corresponding to a DComplex eigenvalue
  //          (WR(j),WI(j)), with DComplex conjugate (WR(j+1),WI(j+1)),
  //          either SELECT(j) or SELECT(j+1) or both must be set to
  //          .TRUE.; then on exit SELECT(j) is .TRUE. and SELECT(j+1) is
  //          .FALSE..
  
  //  N       (input) INTEGER
  //          The order of the matrix H.  N >= 0.
  
  //  H       (input) DOUBLE PRECISION array, dimension (LDH,N)
  //          The upper Hessenberg matrix H.
  
  //  LDH     (input) INTEGER
  //          The leading dimension of the array H.  LDH >= max(1,N).
  
  //  WR      (input/output) DOUBLE PRECISION array, dimension (N)
  //  WI      (input) DOUBLE PRECISION array, dimension (N)
  //          On entry, the real and imaginary parts of the eigenvalues of
  //          H; a DComplex conjugate pair of eigenvalues must be stored in
  //          consecutive elements of WR and WI.
  //          On exit, WR may have been altered since close eigenvalues
  //          are perturbed slightly in searching for independent
  //          eigenvectors.
  
  //  VL      (input/output) DOUBLE PRECISION array, dimension (LDVL,MM)
  //          On entry, if INITV = 'U' and JOB = 'L' or 'B', VL must
  //          contain starting vectors for the inverse iteration for the
  //          left eigenvectors; the starting vector for each eigenvector
  //          must be in the same column(s) in which the eigenvector will
  //          be stored.
  //          On exit, if JOB = 'L' or 'B', the left eigenvectors
  //          specified by SELECT will be stored consecutively in the
  //          columns of VL, in the same order as their eigenvalues. A
  //          DComplex eigenvector corresponding to a DComplex eigenvalue is
  //          stored in two consecutive columns, the first holding the real
  //          part and the second the imaginary part.
  //          If JOB = 'R', VL is not referenced.
  
  //  LDVL    (input) INTEGER
  //          The leading dimension of the array VL.
  //          LDVL >= max(1,N) if JOB = 'L' or 'B'; LDVL >= 1 otherwise.
  
  //  VR      (input/output) DOUBLE PRECISION array, dimension (LDVR,MM)
  //          On entry, if INITV = 'U' and JOB = 'R' or 'B', VR must
  //          contain starting vectors for the inverse iteration for the
  //          right eigenvectors; the starting vector for each eigenvector
  //          must be in the same column(s) in which the eigenvector will
  //          be stored.
  //          On exit, if JOB = 'R' or 'B', the right eigenvectors
  //          specified by SELECT will be stored consecutively in the
  //          columns of VR, in the same order as their eigenvalues. A
  //          DComplex eigenvector corresponding to a DComplex eigenvalue is
  //          stored in two consecutive columns, the first holding the real
  //          part and the second the imaginary part.
  //          If JOB = 'L', VR is not referenced.
  
  //  LDVR    (input) INTEGER
  //          The leading dimension of the array VR.
  //          LDVR >= max(1,N) if JOB = 'R' or 'B'; LDVR >= 1 otherwise.
  
  //  MM      (input) INTEGER
  //          The number of columns in the arrays VL and/or VR. MM >= M.
  
  //  M       (output) INTEGER
  //          The number of columns in the arrays VL and/or VR required to
  //          store the eigenvectors; each selected real eigenvector
  //          occupies one column and each selected DComplex eigenvector
  //          occupies two columns.
  
  //  WORK    (workspace) DOUBLE PRECISION array, dimension ((N+2)*N)
  
  //  IFAILL  (output) INTEGER array, dimension (MM)
  //          If JOB = 'L' or 'B', IFAILL(i) = j > 0 if the left
  //          eigenvector in the i-th column of VL (corresponding to the
  //          eigenvalue w(j)) failed to converge; IFAILL(i) = 0 if the
  //          eigenvector converged satisfactorily. If the i-th and (i+1)th
  //          columns of VL hold a DComplex eigenvector, then IFAILL(i) and
  //          IFAILL(i+1) are set to the same value.
  //          If JOB = 'R', IFAILL is not referenced.
  
  //  IFAILR  (output) INTEGER array, dimension (MM)
  //          If JOB = 'R' or 'B', IFAILR(i) = j > 0 if the right
  //          eigenvector in the i-th column of VR (corresponding to the
  //          eigenvalue w(j)) failed to converge; IFAILR(i) = 0 if the
  //          eigenvector converged satisfactorily. If the i-th and (i+1)th
  //          columns of VR hold a DComplex eigenvector, then IFAILR(i) and
  //          IFAILR(i+1) are set to the same value.
  //          If JOB = 'L', IFAILR is not referenced.
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument had an illegal value
  //          > 0: INFO is the number of eigenvectors which failed to
  //               converge; see IFAILL and IFAILR for further details.
  
  //  Further Details
  //  ===============
  
  //  Each eigenvector is normalized so that the element of largest
  //  magnitude has magnitude 1; here the magnitude of a DComplex number
  //  (x,y) is taken to be |x|+|y|.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Decode and test the input parameters.
  
  bothv = lsame( job, 'B' );
  rightv = lsame( job, 'R' ) || bothv;
  leftv = lsame( job, 'L' ) || bothv;
  
  fromqr = lsame( eigsrc, 'Q' );
  
  noinit = lsame( initv, 'N' );
  
  //     Set M to the number of columns required to store the selected
  //     eigenvectors, and standardize the array SELECT.
  
  m = 0;
  pair = FALSE;
  for( k = 1, k_ = k - 1, _do0 = n; k <= _do0; k++, k_++ ) { 
    if( pair ) { 
      pair = FALSE;
      select[k_] = FALSE;
    }
    else { 
      if( wi[k_] == ZERO ) { 
        if( select[k_] ) 
          m = m + 1;
      }
      else { 
        pair = TRUE;
        if( select[k_] || select[k_ + 1] ) { 
          select[k_] = TRUE;
          m = m + 2;
        }
      }
    }
  }
  
  info = 0;
  if( !rightv && !leftv ) { 
    info = -1;
  }
  else if( !fromqr && !lsame( eigsrc, 'N' ) ) { 
    info = -2;
  }
  else if( !noinit && !lsame( initv, 'N' ) ) { 
    info = -3;
  }
  else if( n < 0 ) { 
    info = -5;
  }
  else if( ldh < max( 1, n ) ) { 
    info = -7;
  }
  else if( ldvl < 1 || (leftv && ldvl < n) ) { 
    info = -11;
  }
  else if( ldvr < 1 || (rightv && ldvr < n) ) { 
    info = -13;
  }
  else if( mm < m ) { 
    info = -14;
  }
  if( info != 0 ) { 
    xerbla( "DHSEIN", -info );
    return;
  }
  
  //     Quick return if possible.
  
  if( n == 0 ) 
    return;
  
  //     Set machine-dependent constants.
  
  unfl = dlamch( 'S'/* Safe minimum */ );
  ulp = dlamch( 'P'/* Precision */ );
  smlnum = unfl*(n/ulp);
  bignum = (ONE - ulp)/smlnum;
  
  ldwork = n + 1;
  
  kl = 1;
  kln = 0;
  if( fromqr ) { 
    kr = 0;
  }
  else { 
    kr = n;
  }
  ksr = 1;
  
  for( k = 1, k_ = k - 1, _do1 = n; k <= _do1; k++, k_++ ) { 
    if( select[k_] ) { 
      
      //           Compute eigenvector(s) corresponding to W(K).
      
      if( fromqr ) { 
        
        //              If affiliation of eigenvalues is known, check whether
        //              the matrix splits.
        
        //              Determine KL and KR such that 1 <= KL <= K <= KR <= N
        //              and H(KL,KL-1) and H(KR+1,KR) are zero (or KL = 1 or
        //              KR = N).
        
        //              Then inverse iteration can be performed with the
        //              submatrix H(KL:N,KL:N) for a left eigenvector, and with
        //              the submatrix H(1:KR,1:KR) for a right eigenvector.
        
        for( i = k, i_ = i - 1, _do2 = kl + 1; i >= _do2; i--, i_-- ) { 
          if( H(i_ - 1,i_) == ZERO ) 
            goto L_30;
        }
L_30:
        ;
        kl = i;
        if( k > kr ) { 
          for( i = k, i_ = i - 1, _do3 = n - 1; i <= _do3; i++, i_++ ) { 
            if( H(i_,i_ + 1) == ZERO ) 
              goto L_50;
          }
L_50:
          ;
          kr = i;
        }
      }
      
      if( kl != kln ) { 
        kln = kl;
        
        //              Compute infinity-norm of submatrix H(KL:KR,KL:KR) if it
        //              has not ben computed before.
        
        hnorm = dlanhs( 'I', kr - kl + 1, &H(kl - 1,kl - 1), 
         ldh, work );
        if( hnorm > ZERO ) { 
          eps3 = hnorm*ulp;
        }
        else { 
          eps3 = smlnum;
        }
      }
      
      //           Perturb eigenvalue if it is close to any previous
      //           selected eigenvalues affiliated to the submatrix
      //           H(KL:KR,KL:KR). Close roots are modified by EPS3.
      
      wkr = wr[k_];
      wki = wi[k_];
L_60:
      ;
      for( i = k - 1, i_ = i - 1, _do4 = kl; i >= _do4; i--, i_-- ) { 
        if( select[i_] && abs( wr[i_] - wkr ) + abs( wi[i_] - 
         wki ) < eps3 ) { 
          wkr = wkr + eps3;
          goto L_60;
        }
      }
      wr[k_] = wkr;
      
      pair = wki != ZERO;
      if( pair ) { 
        ksi = ksr + 1;
      }
      else { 
        ksi = ksr;
      }
      if( leftv ) { 
        
        //              Compute left eigenvector.
        
        dlaein( FALSE, noinit, n - kl + 1, &H(kl - 1,kl - 1), 
         ldh, wkr, wki, &VL(ksr - 1,kl - 1), &VL(ksi - 1,kl - 1), 
         work, ldwork, &work[n*n + n], eps3, smlnum, bignum, 
         iinfo );
        if( iinfo > 0 ) { 
          if( pair ) { 
            info = info + 2;
          }
          else { 
            info = info + 1;
          }
          ifaill[ksr - 1] = k;
          ifaill[ksi - 1] = k;
        }
        else { 
          ifaill[ksr - 1] = 0;
          ifaill[ksi - 1] = 0;
        }
        for( i = 1, i_ = i - 1, _do5 = kl - 1; i <= _do5; i++, i_++ ) { 
          VL(ksr - 1,i_) = ZERO;
        }
        if( pair ) { 
          for( i = 1, i_ = i - 1, _do6 = kl - 1; i <= _do6; i++, i_++ ) { 
            VL(ksi - 1,i_) = ZERO;
          }
        }
      }
      if( rightv ) { 
        
        //              Compute right eigenvector.
        
        dlaein( TRUE, noinit, kr, h, ldh, wkr, wki, &VR(ksr - 1,0), 
         &VR(ksi - 1,0), work, ldwork, &work[n*n + n], eps3, 
         smlnum, bignum, iinfo );
        if( iinfo > 0 ) { 
          if( pair ) { 
            info = info + 2;
          }
          else { 
            info = info + 1;
          }
          ifailr[ksr - 1] = k;
          ifailr[ksi - 1] = k;
        }
        else { 
          ifailr[ksr - 1] = 0;
          ifailr[ksi - 1] = 0;
        }
        for( i = kr + 1, i_ = i - 1, _do7 = n; i <= _do7; i++, i_++ ) { 
          VR(ksr - 1,i_) = ZERO;
        }
        if( pair ) { 
          for( i = kr + 1, i_ = i - 1, _do8 = n; i <= _do8; i++, i_++ ) { 
            VR(ksi - 1,i_) = ZERO;
          }
        }
      }
      
      if( pair ) { 
        ksr = ksr + 2;
      }
      else { 
        ksr = ksr + 1;
      }
    }
  }
  
  return;
  
  //     End of DHSEIN
  
#undef  VR
#undef  VL
#undef  H
} // end of function 


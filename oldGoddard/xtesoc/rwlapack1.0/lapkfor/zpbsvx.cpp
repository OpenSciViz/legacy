/*
 * C++ implementation of Lapack routine zpbsvx
 *
 * $Id: zpbsvx.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:49:45
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zpbsvx.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zpbsvx(const char &fact, const char &uplo, const long &n, const long &kd, 
 const long &nrhs, DComplex *ab, const long &ldab, DComplex *afb, const long &ldafb, 
 char &equed, double s[], DComplex *b, const long &ldb, DComplex *x, const long &ldx, 
 double &rcond, double ferr[], double berr[], DComplex work[], double rwork[], 
 long &info)
{
#define AB(I_,J_) (*(ab+(I_)*(ldab)+(J_)))
#define AFB(I_,J_)  (*(afb+(I_)*(ldafb)+(J_)))
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
#define X(I_,J_)  (*(x+(I_)*(ldx)+(J_)))
  int equil, nofact, upper;
  long _do0, _do1, _do2, _do3, _do4, _do5, _do6, i, i_, infequ, 
   j, j1, j2, j_;
  double amax, anorm, scond;

  
  //  -- LAPACK driver routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZPBSVX uses the Cholesky factorization A = U'*U or A = L*L' to
  //  compute the solution to a DComplex system of linear equations
  //     A * X = B,
  //  where A is an N by N Hermitian positive definite band matrix and X
  //  and B are N by NRHS matrices.
  
  //  Error bounds on the solution and a condition estimate are also
  //  provided.
  
  //  Description
  //  ===========
  
  //  The following steps are performed by this subroutine:
  
  //  1. If FACT = 'E', real scaling factors are computed to equilibrate
  //     the system:
  //        diag(S) * A * diag(S) * inv(diag(S)) * X = diag(S) * B
  //     Whether or not the system will be equilibrated depends on the
  //     scaling of the matrix A, but if equilibration is used, A is
  //     overwritten by diag(S)*A*diag(S) and B by diag(S)*B.
  
  //  2. If FACT = 'N' or 'E', the Cholesky decomposition is used to
  //     factor the matrix A (after equilibration if FACT = 'E') as
  //        A = U'* U ,  if UPLO = 'U', or
  //        A = L * L',  if UPLO = 'L',
  //     where U is an upper triangular matrix, L is a lower triangular
  //     matrix, and ' indicates conjugate transpose.
  
  //  3. The factored form of A is used to estimate the condition number
  //     of the matrix A.  If the reciprocal of the condition number is
  //     less than machine precision, steps 4-6 are skipped.
  
  //  4. The system of equations A*X = B is solved for X using the
  //     factored form of A.
  
  //  5. Iterative refinement is applied to improve the computed solution
  //     vectors and calculate error bounds and backward error estimates
  //     for them.
  
  //  6. If FACT = 'E' and equilibration was used, the vectors X are
  //     premultiplied by diag(S) so that they solve the original system
  //     before equilibration.
  
  //  Arguments
  //  =========
  
  //  FACT    (input) CHARACTER*1
  //          Specifies whether or not the factored form of the matrix A is
  //          supplied on entry, and if not, whether the matrix A should be
  //          equilibrated before it is factored.
  //          = 'F':  On entry, AFB contains the factored form of A. AB and
  //                  AFB will not be modified.
  //          = 'N':  The matrix A will be copied to AFB and factored.
  //          = 'E':  The matrix A will be equilibrated if necessary, then
  //                  copied to AFB and factored.
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangular part of the
  //          Hermitian matrix A is stored:
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The number of linear equations, i.e., the order of the
  //          matrix A.  N >= 0.
  
  //  KD      (input) INTEGER
  //          The number of super-diagonals of the matrix A if UPLO = 'U',
  //          or the number of sub-diagonals if UPLO = 'L'.  KD >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right hand sides, i.e., the number of columns
  //          of the matrices B and X.  NRHS >= 0.
  
  //  AB      (input/output) COMPLEX*16 array, dimension (LDAB,N)
  //          On entry, the upper or lower triangle of the Hermitian band
  //          matrix A, stored in the first KD+1 rows of the array.  The
  //          j-th column of A is stored in the j-th column of the array AB
  //          as follows:
  //          if UPLO = 'U', AB(KD+1+i-j,j) = A(i,j) for max(1,j-KD)<=i<=j;
  //          if UPLO = 'L', AB(1+i-j,j)    = A(i,j) for j<=i<=min(N,j+KD).
  //          See below for further details.
  
  //          On exit, if EQUED = 'Y', A is replaced by diag(S)*A*diag(S).
  
  //  LDAB    (input) INTEGER
  //          The leading dimension of the array A.  LDAB >= KD+1.
  
  //  AFB     (input or output) COMPLEX*16 array, dimension (LDAFB,N)
  //          If FACT = 'F', then AFB is an input argument and on entry
  //          contains the triangular factor U or L from the Cholesky
  //          factorization A = U'*U or A = L*L' of the band matrix A, in
  //          the same storage format as A (see AB).
  
  //          If FACT = 'N', then AFB is an output argument and on exit
  //          returns the triangular factor U or L from the Cholesky
  //          factorization A = U'*U or A = L*L'.
  
  //          If FACT = 'E', then AFB is an output argument and on exit
  //          returns the triangular factor U or L from the Cholesky
  //          factorization A = U'*U or A = L*L' of the equilibrated
  //          matrix A (see the description of A for the form of the
  //          equilibrated matrix).
  
  //  LDAFB   (input) INTEGER
  //          The leading dimension of the array AFB.  LDAFB >= KD+1.
  
  //  EQUED   (output) CHARACTER*1
  //          Specifies the form of equilibration that was done.
  //          = 'N':  No equilibration (always true if FACT = 'F' or 'N').
  //          = 'Y':  Equilibration was done, i.e., A has been replaced by
  //                  diag(S) * A * diag(S).
  
  //  S       (output) DOUBLE PRECISION array, dimension (N)
  //          The scale factors for A.  Not assigned if FACT = 'F' or 'N'.
  
  //  B       (input/output) COMPLEX*16 array, dimension (LDB,NRHS)
  //          On entry, the n-by-nrhs right-hand side matrix B.
  //          On exit, if EQUED = 'N', B is not modified; if EQUED = 'Y',
  //          B is overwritten by diag(S) * B.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  X       (output) COMPLEX*16 array, dimension (LDX,NRHS)
  //          If INFO = 0, the n-by-nrhs solution matrix X to the original
  //          system of equations.  Note that if EQUED = 'Y', A and B are
  //          modified on exit, and the solution to the equilibrated system
  //          is inv(diag(S))*X.
  
  //  LDX     (input) INTEGER
  //          The leading dimension of the array X.  LDX >= max(1,N).
  
  //  RCOND   (output) DOUBLE PRECISION
  //          The estimate of the reciprocal condition number of the matrix
  //          A.  If RCOND is less than the machine precision (in
  //          particular, if RCOND = 0), the matrix is singular to working
  //          precision.  This condition is indicated by a return code of
  //          INFO > 0, and the solution and error bounds are not computed.
  
  //  FERR    (output) DOUBLE PRECISION array, dimension (NRHS)
  //          The estimated forward error bounds for each solution vector
  //          X(j) (the j-th column of the solution matrix X).
  //          If XTRUE is the true solution, FERR(j) bounds the magnitude
  //          of the largest entry in (X(j) - XTRUE) divided by the
  //          magnitude of the largest entry in X(j).  The quality of the
  //          error bound depends on the quality of the estimate of
  //          norm(inv(A)) computed in the code; if the estimate of
  //          norm(inv(A)) is accurate, the error bound is guaranteed.
  
  //  BERR    (output) DOUBLE PRECISION array, dimension (NRHS)
  //          The componentwise relative backward error of each solution
  //          solution vector X(j) (i.e., the smallest relative change in
  //          any entry of A or B that makes X(j) an exact solution).
  
  //  WORK    (workspace) COMPLEX*16 array, dimension (2*N)
  
  //  RWORK   (workspace) DOUBLE PRECISION array, dimension (N)
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: if INFO = k, and k is
  //               <= N: if INFO = k, the leading minor of order k of A is
  //                     not positive definite, so the factorization could
  //                     not be completed, and the solution has not been
  //                     computed.
  //               = N+1: RCOND is less than machine precision.  The
  //                     factorization has been completed, but the matrix
  //                     is singular to working precision, and the solution
  //                     and error bounds have not been computed.
  
  //  Further Details
  //  ===============
  
  //  The band storage scheme is illustrated by the following example, when
  //  N = 6, KD = 2, and UPLO = 'U':
  
  //  Two-dimensional storage of the Hermitian matrix A:
  
  //     a11  a12  a13
  //          a22  a23  a24
  //               a33  a34  a35
  //                    a44  a45  a46
  //                         a55  a56
  //     (aij=conjg(aji))         a66
  
  //  Band storage of the upper triangle of A:
  
  //      *    *   a13  a24  a35  a46
  //      *   a12  a23  a34  a45  a56
  //     a11  a22  a33  a44  a55  a66
  
  //  Similarly, if UPLO = 'L' the format of A is as follows:
  
  //     a11  a22  a33  a44  a55  a66
  //     a21  a32  a43  a54  a65   *
  //     a31  a42  a53  a64   *    *
  
  //  Array elements marked * are not used by the routine.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  nofact = lsame( fact, 'N' );
  equil = lsame( fact, 'E' );
  upper = lsame( uplo, 'U' );
  if( (!nofact && !equil) && !lsame( fact, 'F' ) ) { 
    info = -1;
  }
  else if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -2;
  }
  else if( n < 0 ) { 
    info = -3;
  }
  else if( kd < 0 ) { 
    info = -4;
  }
  else if( nrhs < 0 ) { 
    info = -5;
  }
  else if( ldab < kd + 1 ) { 
    info = -7;
  }
  else if( ldafb < kd + 1 ) { 
    info = -9;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -13;
  }
  else if( ldx < max( 1, n ) ) { 
    info = -15;
  }
  if( info != 0 ) { 
    xerbla( "ZPBSVX", -info );
    return;
  }
  
  equed = 'N';
  if( equil ) { 
    
    //        Compute row and column scalings to equilibrate the matrix A.
    
    zpbequ( uplo, n, kd, ab, ldab, s, scond, amax, infequ );
    if( infequ == 0 ) { 
      
      //           Equilibrate the matrix.
      
      zlaqsb( uplo, n, kd, ab, ldab, s, scond, amax, equed );
      
      //           Scale the right hand side.
      
      if( lsame( equed, 'Y' ) ) { 
        for( j = 1, j_ = j - 1, _do0 = nrhs; j <= _do0; j++, j_++ ) { 
          for( i = 1, i_ = i - 1, _do1 = n; i <= _do1; i++, i_++ ) { 
            B(j_,i_) = s[i_]*B(j_,i_);
          }
        }
      }
    }
  }
  
  if( nofact || equil ) { 
    
    //        Compute the Cholesky factorization A = U'*U or A = L*L'.
    
    if( upper ) { 
      for( j = 1, j_ = j - 1, _do2 = n; j <= _do2; j++, j_++ ) { 
        j1 = max( j - kd, 1 );
        zcopy( j - j1 + 1, &AB(j_,kd - j + j1), 1, &AFB(j_,kd - j + j1), 
         1 );
      }
    }
    else { 
      for( j = 1, j_ = j - 1, _do3 = n; j <= _do3; j++, j_++ ) { 
        j2 = min( j + kd, n );
        zcopy( j2 - j + 1, &AB(j_,0), 1, &AFB(j_,0), 1 );
      }
    }
    
    zpbtrf( uplo, n, kd, afb, ldafb, info );
    
    //        Return if INFO is non-zero.
    
    if( info != 0 ) { 
      if( info > 0 ) 
        rcond = ZERO;
      return;
    }
  }
  
  //     Compute the norm of the matrix A.
  
  anorm = zlansb( '1', uplo, n, kd, ab, ldab, rwork );
  
  //     Compute the reciprocal of the condition number of A.
  
  zpbcon( uplo, n, kd, afb, ldafb, anorm, rcond, work, rwork, info );
  
  //     Return if the matrix is singular to working precision.
  
  if( rcond < dlamch( 'E'/*Epsilon*/ ) ) { 
    info = n + 1;
    return;
  }
  
  //     Compute the solution vectors X.
  
  zlacpy( 'F'/*Full*/, n, nrhs, b, ldb, x, ldx );
  zpbtrs( uplo, n, kd, nrhs, afb, ldafb, x, ldx, info );
  
  //     Use iterative refinement to improve the computed solutions and
  //     compute error bounds and backward error estimates for them.
  
  zpbrfs( uplo, n, kd, nrhs, ab, ldab, afb, ldafb, b, ldb, x, ldx, 
   ferr, berr, work, rwork, info );
  
  //     Transform the solution vectors X to solutions of the original
  //     system.
  
  if( lsame( equed, 'Y' ) ) { 
    for( j = 1, j_ = j - 1, _do4 = nrhs; j <= _do4; j++, j_++ ) { 
      for( i = 1, i_ = i - 1, _do5 = n; i <= _do5; i++, i_++ ) { 
        X(j_,i_) = s[i_]*X(j_,i_);
      }
    }
    for( j = 1, j_ = j - 1, _do6 = nrhs; j <= _do6; j++, j_++ ) { 
      ferr[j_] = ferr[j_]/scond;
    }
  }
  
  return;
  
  //     End of ZPBSVX
  
#undef  X
#undef  B
#undef  AFB
#undef  AB
} // end of function 


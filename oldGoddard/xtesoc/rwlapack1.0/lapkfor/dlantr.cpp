/*
 * C++ implementation of lapack routine dlantr
 *
 * $Id: dlantr.cpp,v 1.4 1993/04/06 20:41:08 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:35:37
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dlantr.cpp,v $
 * Revision 1.4  1993/04/06  20:41:08  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.3  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.2  1993/03/05  23:15:32  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:07:23  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ONE = 1.0e0;
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL double /*FUNCTION*/ dlantr(const char &norm, const char &uplo, const char &diag, const long &m, 
 const long &n, double *a, const long &lda, double work[])
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  int udiag;
  long _do0, _do1, _do10, _do11, _do12, _do13, _do14, _do15, 
   _do16, _do17, _do18, _do19, _do2, _do20, _do21, _do22, _do23, 
   _do24, _do25, _do26, _do27, _do28, _do29, _do3, _do30, _do31, 
   _do4, _do5, _do6, _do7, _do8, _do9, i, i_, j, j_;
  double dlantr_v, scale, sum, value;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DLANTR  returns the value of the one norm,  or the Frobenius norm, or
  //  the  infinity norm,  or the  element of  largest absolute value  of a
  //  trapezoidal or triangular matrix A.
  
  //  Description
  //  ===========
  
  //  DLANTR returns the value
  
  //     DLANTR = ( max(abs(A(i,j))), NORM = 'M' or 'm'
  //              (
  //              ( norm1(A),         NORM = '1', 'O' or 'o'
  //              (
  //              ( normI(A),         NORM = 'I' or 'i'
  //              (
  //              ( normF(A),         NORM = 'F', 'f', 'E' or 'e'
  
  //  where  norm1  denotes the  one norm of a matrix (maximum column sum),
  //  normI  denotes the  infinity norm  of a matrix  (maximum row sum) and
  //  normF  denotes the  Frobenius norm of a matrix (square root of sum of
  //  squares).  Note that  max(abs(A(i,j)))  is not a  matrix norm.
  
  //  Arguments
  //  =========
  
  //  NORM    (input) CHARACTER*1
  //          Specifies the value to be returned in DLANTR as described
  //          above.
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the matrix A is upper or lower trapezoidal.
  //          = 'U':  Upper trapezoidal
  //          = 'L':  Lower trapezoidal
  //          Note that A is triangular instead of trapezoidal if M = N.
  
  //  DIAG    (input) CHARACTER*1
  //          Specifies whether or not the matrix A has unit diagonal.
  //          = 'N':  Non-unit diagonal
  //          = 'U':  Unit diagonal
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix A.  M >= 0, and if
  //          UPLO = 'U', M <= N.  When M = 0, DLANTR is set to zero.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix A.  N >= 0, and if
  //          UPLO = 'L', N <= M.  When N = 0, DLANTR is set to zero.
  
  //  A       (input) DOUBLE PRECISION array, dimension (LDA,N)
  //          The trapezoidal matrix A (A is triangular if M = N).
  //          If UPLO = 'U', the leading m by n upper trapezoidal part of
  //          the array A contains the upper trapezoidal matrix, and the
  //          strictly lower triangular part of A is not referenced.
  //          If UPLO = 'L', the leading m by n lower trapezoidal part of
  //          the array A contains the lower trapezoidal matrix, and the
  //          strictly upper triangular part of A is not referenced.  Note
  //          that when DIAG = 'U', the diagonal elements of A are not
  //          referenced and are assumed to be one.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(M,1).
  
  //  WORK    (workspace) DOUBLE PRECISION array, dimension (LWORK),
  //          where LWORK >= M when NORM = 'I'; otherwise, WORK is not
  //          referenced.
  
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  if( min( m, n ) == 0 ) { 
    value = ZERO;
  }
  else if( lsame( norm, 'M' ) ) { 
    
    //        Find max(abs(A(i,j))).
    
    if( lsame( diag, 'U' ) ) { 
      value = ONE;
      if( lsame( uplo, 'U' ) ) { 
        for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
          for( i = 1, i_ = i - 1, _do1 = min( m, j - 1 ); i <= _do1; i++, i_++ ) { 
            value = max( value, abs( A(j_,i_) ) );
          }
        }
      }
      else { 
        for( j = 1, j_ = j - 1, _do2 = n; j <= _do2; j++, j_++ ) { 
          for( i = j + 1, i_ = i - 1, _do3 = m; i <= _do3; i++, i_++ ) { 
            value = max( value, abs( A(j_,i_) ) );
          }
        }
      }
    }
    else { 
      value = ZERO;
      if( lsame( uplo, 'U' ) ) { 
        for( j = 1, j_ = j - 1, _do4 = n; j <= _do4; j++, j_++ ) { 
          for( i = 1, i_ = i - 1, _do5 = min( m, j ); i <= _do5; i++, i_++ ) { 
            value = max( value, abs( A(j_,i_) ) );
          }
        }
      }
      else { 
        for( j = 1, j_ = j - 1, _do6 = n; j <= _do6; j++, j_++ ) { 
          for( i = j, i_ = i - 1, _do7 = m; i <= _do7; i++, i_++ ) { 
            value = max( value, abs( A(j_,i_) ) );
          }
        }
      }
    }
  }
  else if( (lsame( norm, 'O' )) || (norm == '1') ) { 
    
    //        Find norm1(A).
    
    value = ZERO;
    udiag = lsame( diag, 'U' );
    if( lsame( uplo, 'U' ) ) { 
      for( j = 1, j_ = j - 1, _do8 = n; j <= _do8; j++, j_++ ) { 
        if( (udiag) && (j <= m) ) { 
          sum = ONE;
          for( i = 1, i_ = i - 1, _do9 = j - 1; i <= _do9; i++, i_++ ) { 
            sum = sum + abs( A(j_,i_) );
          }
        }
        else { 
          sum = ZERO;
          for( i = 1, i_ = i - 1, _do10 = min( m, j ); i <= _do10; i++, i_++ ) { 
            sum = sum + abs( A(j_,i_) );
          }
        }
        value = max( value, sum );
      }
    }
    else { 
      for( j = 1, j_ = j - 1, _do11 = n; j <= _do11; j++, j_++ ) { 
        if( udiag ) { 
          sum = ONE;
          for( i = j + 1, i_ = i - 1, _do12 = m; i <= _do12; i++, i_++ ) { 
            sum = sum + abs( A(j_,i_) );
          }
        }
        else { 
          sum = ZERO;
          for( i = j, i_ = i - 1, _do13 = m; i <= _do13; i++, i_++ ) { 
            sum = sum + abs( A(j_,i_) );
          }
        }
        value = max( value, sum );
      }
    }
  }
  else if( lsame( norm, 'I' ) ) { 
    
    //        Find normI(A).
    
    if( lsame( uplo, 'U' ) ) { 
      if( lsame( diag, 'U' ) ) { 
        for( i = 1, i_ = i - 1, _do14 = m; i <= _do14; i++, i_++ ) { 
          work[i_] = ONE;
        }
        for( j = 1, j_ = j - 1, _do15 = n; j <= _do15; j++, j_++ ) { 
          for( i = 1, i_ = i - 1, _do16 = min( m, j - 1 ); i <= _do16; i++, i_++ ) { 
            work[i_] = work[i_] + abs( A(j_,i_) );
          }
        }
      }
      else { 
        for( i = 1, i_ = i - 1, _do17 = m; i <= _do17; i++, i_++ ) { 
          work[i_] = ZERO;
        }
        for( j = 1, j_ = j - 1, _do18 = n; j <= _do18; j++, j_++ ) { 
          for( i = 1, i_ = i - 1, _do19 = min( m, j ); i <= _do19; i++, i_++ ) { 
            work[i_] = work[i_] + abs( A(j_,i_) );
          }
        }
      }
    }
    else { 
      if( lsame( diag, 'U' ) ) { 
        for( i = 1, i_ = i - 1, _do20 = n; i <= _do20; i++, i_++ ) { 
          work[i_] = ONE;
        }
        for( i = n + 1, i_ = i - 1, _do21 = m; i <= _do21; i++, i_++ ) { 
          work[i_] = ZERO;
        }
        for( j = 1, j_ = j - 1, _do22 = n; j <= _do22; j++, j_++ ) { 
          for( i = j + 1, i_ = i - 1, _do23 = m; i <= _do23; i++, i_++ ) { 
            work[i_] = work[i_] + abs( A(j_,i_) );
          }
        }
      }
      else { 
        for( i = 1, i_ = i - 1, _do24 = m; i <= _do24; i++, i_++ ) { 
          work[i_] = ZERO;
        }
        for( j = 1, j_ = j - 1, _do25 = n; j <= _do25; j++, j_++ ) { 
          for( i = j, i_ = i - 1, _do26 = m; i <= _do26; i++, i_++ ) { 
            work[i_] = work[i_] + abs( A(j_,i_) );
          }
        }
      }
    }
    value = ZERO;
    for( i = 1, i_ = i - 1, _do27 = m; i <= _do27; i++, i_++ ) { 
      value = max( value, work[i_] );
    }
  }
  else if( (lsame( norm, 'F' )) || (lsame( norm, 'E' )) ) { 
    
    //        Find normF(A).
    
    if( lsame( uplo, 'U' ) ) { 
      if( lsame( diag, 'U' ) ) { 
        scale = ONE;
        sum = min( m, n );
        for( j = 2, j_ = j - 1, _do28 = n; j <= _do28; j++, j_++ ) { 
          dlassq( min( m, j - 1 ), &A(j_,0), 1, scale, sum );
        }
      }
      else { 
        scale = ZERO;
        sum = ONE;
        for( j = 1, j_ = j - 1, _do29 = n; j <= _do29; j++, j_++ ) { 
          dlassq( min( m, j ), &A(j_,0), 1, scale, sum );
        }
      }
    }
    else { 
      if( lsame( diag, 'U' ) ) { 
        scale = ONE;
        sum = min( m, n );
        for( j = 1, j_ = j - 1, _do30 = n; j <= _do30; j++, j_++ ) { 
          dlassq( m - j, &A(j_,min( m, j + 1 ) - 1), 1, 
           scale, sum );
        }
      }
      else { 
        scale = ZERO;
        sum = ONE;
        for( j = 1, j_ = j - 1, _do31 = n; j <= _do31; j++, j_++ ) { 
          dlassq( m - j + 1, &A(j_,j_), 1, scale, sum );
        }
      }
    }
    value = scale*sqrt( sum );
  }
  
  dlantr_v = value;
  return( dlantr_v );
  
  //     End of DLANTR
  
#undef  A
} // end of function 


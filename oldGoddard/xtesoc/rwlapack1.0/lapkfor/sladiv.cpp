/*
 * C++ implementation of Lapack routine sladiv
 *
 * $Id: sladiv.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:59:27
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: sladiv.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

RWLAPKDECL void /*FUNCTION*/ sladiv(const float &a, const float &b, const float &c, const float &d, float &p, 
   float &q)
{
  float e, f;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SLADIV performs DComplex division in  real arithmetic
  
  //                        a + i*b
  //             p + i*q = ---------
  //                        c + i*d
  
  //  The algorithm is due to Robert L. Smith and can be found
  //  in D. Knuth, The art of Computer Programming, Vol.2, p.195
  
  //  Arguments
  //  =========
  
  //  A       (input) REAL
  //  B       (input) REAL
  //  C       (input) REAL
  //  D       (input) REAL
  //          The scalars a, b, c, and d in the above expression.
  
  //  P       (output) REAL
  //  Q       (output) REAL
  //          The scalars p and q in the above expression.
  
  //     .. Local Scalars ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  if( abs( d ) < abs( c ) ) { 
    e = d/c;
    f = c + d*e;
    p = (a + b*e)/f;
    q = (b - a*e)/f;
  }
  else { 
    e = c/d;
    f = d + c*e;
    p = (b + a*e)/f;
    q = (-a + b*e)/f;
  }
  
  return;
  
  //     End of SLADIV
  
} // end of function 


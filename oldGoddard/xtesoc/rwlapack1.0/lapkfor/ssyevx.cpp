/*
 * C++ implementation of Lapack routine ssyevx
 *
 * $Id: ssyevx.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:02:56
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: ssyevx.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ZERO = 0.0e0;
const float ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ ssyevx(const char &jobz, const char &range, const char &uplo, const long &n, 
 float *a, const long &lda, const float &vl, const float &vu, const long &il, const long &iu, 
 const float &abstol, const long &m, float w[], float *z, const long &ldz, float work[], 
 const long &lwork, long iwork[], long ifail[], long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define Z(I_,J_)  (*(z+(I_)*(ldz)+(J_)))
  int alleig, indeig, lower, valeig, wantz;
  char order;
  long _do0, _do1, _do2, _do3, _do4, i, i_, iinfo, imax, indd, 
   inde, indibl, indisp, indiwo, indtau, indwkn, indwrk, iscale, 
   itmp1, j, j_, jj, jj_, llwork, llwrkn, lopt, nsplit;
  float abstll, anrm, bignum, eps, rmax, rmin, safmin, sigma, smlnum, 
   tmp1, vll, vuu;

  
  //  -- LAPACK driver routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SSYEVX computes selected eigenvalues and, optionally, eigenvectors
  //  of a real symmetric matrix A by calling the recommended sequence of
  //  LAPACK routines.  Eigenvalues/vectors can be selected by specifying
  //  either a range of values or a range of indices for the desired
  //  eigenvalues.
  
  //  Arguments
  //  =========
  
  //  JOBZ    (input) CHARACTER*1
  //          Specifies whether or not to compute the eigenvectors:
  //          = 'N':  Compute eigenvalues only.
  //          = 'V':  Compute eigenvalues and eigenvectors.
  
  //  RANGE   (input) CHARACTER*1
  //          = 'A': all eigenvalues will be found.
  //          = 'V': all eigenvalues in the half-open interval (VL,VU]
  //                 will be found.
  //          = 'I': the IL-th through IU-th eigenvalues will be found.
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangular part of the
  //          symmetric matrix A is stored:
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The number of rows and columns of the matrix A.  N >= 0.
  
  //  A       (input/workspace) REAL array, dimension (LDA, N)
  //          On entry, the symmetric matrix A.  If UPLO = 'U', only the
  //          upper triangular part of A is used to define the elements of
  //          the symmetric matrix.  If UPLO = 'L', only the lower
  //          triangular part of A is used to define the elements of the
  //          symmetric matrix.
  
  //          On exit, the lower triangle (if UPLO='L') or the upper
  //          triangle (if UPLO='U') of A, including the diagonal, is
  //          destroyed.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  VL      (input) REAL
  //          If RANGE='V', the lower bound of the interval to be searched
  //          for eigenvalues.  Not referenced if RANGE = 'A' or 'I'.
  
  //  VU      (input) REAL
  //          If RANGE='V', the upper bound of the interval to be searched
  //          for eigenvalues.  Not referenced if RANGE = 'A' or 'I'.
  
  //  IL      (input) INTEGER
  //          If RANGE='I', the index (from smallest to largest) of the
  //          smallest eigenvalue to be returned.  IL >= 1.
  //          Not referenced if RANGE = 'A' or 'V'.
  
  //  IU      (input) INTEGER
  //          If RANGE='I', the index (from smallest to largest) of the
  //          largest eigenvalue to be returned.  min(IL,N) <= IU <= N.
  //          Not referenced if RANGE = 'A' or 'V'.
  
  //  ABSTOL  (input) REAL
  //          The absolute error tolerance for the eigenvalues.
  //          An approximate eigenvalue is accepted as converged
  //          when it is determined to lie in an interval [a,b]
  //          of width less than or equal to
  
  //                  ABSTOL + EPS *   max( |a|,|b| ) ,
  
  //          where EPS is the machine precision.  If ABSTOL is less than
  //          or equal to zero, then  EPS*|T|  will be used in its place,
  //          where |T| is the 1-norm of the tridiagonal matrix obtained
  //          by reducing A to tridiagonal form.  For most problems,
  //          this is the appropriate level of accuracy to request.
  //          For certain strongly graded matrices, greater accuracy
  //          can be obtained in very small eigenvalues by setting
  //          ABSTOL to some very small positive number.
  //          (Note, however, that if ABSTOL is less than sqrt(unfl),
  //          where unfl is the underflow threshold, then sqrt(unfl)
  //          will be used in its place.)
  
  //          See "Computing Small Singular Values of Bidiagonal Matrices
  //          with Guaranteed High Relative Accuracy," by Demmel and
  //          Kahan, LAPACK Working Note #3.
  
  //  M       (output) INTEGER
  //          Total number of eigenvalues found.  0 <= M <= N.
  
  //  W       (output) REAL array, dimension (N)
  //          On normal exit, the first M entries contain the selected
  //          eigenvalues in ascending order.
  
  //  Z       (output) REAL array, dimension (LDZ, N)
  //          If JOBZ = 'V', then on normal exit the first M columns of Z
  //          contain the orthonormal eigenvectors of the matrix
  //          corresponding to the selected eigenvalues.  If an eigenvector
  //          fails to converge, then that column of Z contains the latest
  //          approximation to the eigenvector, and the index of the
  //          eigenvector is returned in IFAIL.
  //          If JOBZ = 'N', then Z is not referenced.
  
  //  LDZ     (input) INTEGER
  //          The leading dimension of the array Z.  LDZ >= 1, and if
  //          JOBZ = 'V', LDZ >= max(1,N).
  
  //  WORK    (workspace) REAL array, dimension (LWORK)
  //          On exit, WORK(1) is set to the dimension of the work array
  //          needed to obtain optimal performance from this routine.
  //          See the description of LWORK below.
  
  //  LWORK   (input) INTEGER
  //          The length of the array WORK.  LWORK >= max(1,7*N).
  //          For optimal efficiency, LWORK should be at least (NB+3)*N,
  //          where NB is the blocksize for SSYTRD returned by ILAENV.
  
  //  IWORK   (workspace) INTEGER array, dimension (5*N)
  
  //  IFAIL   (output) INTEGER array, dimension (N)
  //          If JOBZ = 'V', then on normal exit, the first M elements of
  //          IFAIL are zero.  If INFO > 0 on exit, then IFAIL contains the
  //          indices of the eigenvectors that failed to converge.
  //          If JOBZ = 'N', then IFAIL is not referenced.
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit.
  //          < 0:  if INFO = -i, the i-th argument had an illegal value.
  //          > 0:  if INFO = +i, then i eigenvectors failed to converge.
  //                Their indices are stored in array IFAIL.
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  lower = lsame( uplo, 'L' );
  wantz = lsame( jobz, 'V' );
  alleig = lsame( range, 'A' );
  valeig = lsame( range, 'V' );
  indeig = lsame( range, 'I' );
  
  info = 0;
  if( !(wantz || lsame( jobz, 'N' )) ) { 
    info = -1;
  }
  else if( !((alleig || valeig) || indeig) ) { 
    info = -2;
  }
  else if( !(lower || lsame( uplo, 'U' )) ) { 
    info = -3;
  }
  else if( n < 0 ) { 
    info = -4;
  }
  else if( lda < max( 1, n ) ) { 
    info = -6;
  }
  else if( (valeig && n > 0) && vu <= vl ) { 
    info = -8;
  }
  else if( indeig && il < 1 ) { 
    info = -9;
  }
  else if( indeig && (iu < min( n, il ) || iu > n) ) { 
    info = -10;
  }
  else if( ldz < 1 || (wantz && ldz < n) ) { 
    info = -15;
  }
  else if( lwork < max( 1, 7*n ) ) { 
    info = -17;
  }
  
  if( info != 0 ) { 
    xerbla( "SSYEVX", -info );
    return;
  }
  
  //     Quick return if possible
  
  m = 0;
  if( n == 0 ) { 
    work[0] = 1;
    return;
  }
  
  if( n == 1 ) { 
    work[0] = 7;
    if( alleig || indeig ) { 
      m = 1;
      w[0] = A(0,0);
    }
    else { 
      if( vl < A(0,0) && vu >= A(0,0) ) { 
        m = 1;
        w[0] = A(0,0);
      }
    }
    if( wantz ) 
      Z(0,0) = ONE;
    return;
  }
  
  //     Get machine constants.
  
  safmin = slamch( 'S'/*Safe minimum*/ );
  eps = slamch( 'P'/*Precision*/ );
  smlnum = safmin/eps;
  bignum = ONE/smlnum;
  rmin = sqrt( smlnum );
  rmax = min( sqrt( bignum ), ONE/sqrt( sqrt( safmin ) ) );
  
  //     Scale matrix to allowable range, if necessary.
  
  iscale = 0;
  abstll = abstol;
  if( valeig ) { 
    vll = vl;
    vuu = vu;
  }
  anrm = slansy( 'M', uplo, n, a, lda, work );
  if( anrm > ZERO && anrm < rmin ) { 
    iscale = 1;
    sigma = rmin/anrm;
  }
  else if( anrm > rmax ) { 
    iscale = 1;
    sigma = rmax/anrm;
  }
  if( iscale == 1 ) { 
    if( lower ) { 
      for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
        sscal( n - j + 1, sigma, &A(j_,j_), 1 );
      }
    }
    else { 
      for( j = 1, j_ = j - 1, _do1 = n; j <= _do1; j++, j_++ ) { 
        sscal( j, sigma, &A(j_,0), 1 );
      }
    }
    if( abstol > 0 ) 
      abstll = abstol*sigma;
    if( valeig ) { 
      vll = vl*sigma;
      vuu = vu*sigma;
    }
  }
  
  //     Call SSYTRD to reduce symmetric matrix to tridiagonal form.
  
  indtau = 1;
  inde = indtau + n;
  indd = inde + n;
  indwrk = indd + n;
  llwork = lwork - indwrk + 1;
  ssytrd( uplo, n, a, lda, &work[indd - 1], &work[inde - 1], &work[indtau - 1], 
   &work[indwrk - 1], llwork, iinfo );
  lopt = 3*n + work[indwrk - 1];
  
  //     If all eigenvalues are desired and ABSTOL is less than or equal to
  //     zero, then call SSTERF or SORGTR and SSTEQR.  If this fails for
  //     some eigenvalue, then try SSTEBZ.
  
  if( (alleig || ((indeig && il == 1) && iu == n)) && (abstol <= ZERO) ) { 
    scopy( n, &work[indd - 1], 1, w, 1 );
    if( !wantz ) { 
      ssterf( n, w, &work[inde - 1], info );
    }
    else { 
      slacpy( 'A', n, n, a, lda, z, ldz );
      sorgtr( uplo, n, z, ldz, &work[indtau - 1], &work[indwrk - 1], 
       llwork, iinfo );
      ssteqr( jobz, n, w, &work[inde - 1], z, ldz, &work[indwrk - 1], 
       info );
      if( info == 0 ) { 
        for( i = 1, i_ = i - 1, _do2 = n; i <= _do2; i++, i_++ ) { 
          ifail[i_] = 0;
        }
      }
    }
    if( info == 0 ) { 
      m = n;
      goto L_40;
    }
    info = 0;
  }
  
  //     Otherwise, call SSTEBZ and, if eigenvectors are desired, SSTEIN.
  
  if( wantz ) { 
    order = 'B';
  }
  else { 
    order = 'E';
  }
  indibl = 1;
  indisp = indibl + n;
  indiwo = indisp + n;
  sstebz( range, order, n, vll, vuu, il, iu, abstll, &work[indd - 1], 
   &work[inde - 1], m, nsplit, w, &iwork[indibl - 1], &iwork[indisp - 1], 
   &work[indwrk - 1], &iwork[indiwo - 1], info );
  
  if( wantz ) { 
    sstein( n, &work[indd - 1], &work[inde - 1], m, w, &iwork[indibl - 1], 
     &iwork[indisp - 1], z, ldz, &work[indwrk - 1], &iwork[indiwo - 1], 
     ifail, info );
    
    //        Apply orthogonal matrix used in reduction to tridiagonal
    //        form to eigenvectors returned by SSTEIN.
    
    indwkn = inde;
    llwrkn = lwork - indwkn + 1;
    sormtr( 'L', uplo, 'N', n, m, a, lda, &work[indtau - 1], z, 
     ldz, &work[indwkn - 1], llwrkn, iinfo );
  }
  
  //     If matrix was scaled, then rescale eigenvalues appropriately.
  
L_40:
  ;
  if( iscale == 1 ) { 
    if( info == 0 ) { 
      imax = m;
    }
    else { 
      imax = info - 1;
    }
    sscal( imax, ONE/sigma, w, 1 );
  }
  
  //     If eigenvalues are not in order, then sort them, along with
  //     eigenvectors.
  
  if( wantz ) { 
    for( j = 1, j_ = j - 1, _do3 = m - 1; j <= _do3; j++, j_++ ) { 
      i = 0;
      tmp1 = w[j_];
      for( jj = j + 1, jj_ = jj - 1, _do4 = m; jj <= _do4; jj++, jj_++ ) { 
        if( w[jj_] < tmp1 ) { 
          i = jj;
          tmp1 = w[jj_];
        }
      }
      
      if( i != 0 ) { 
        itmp1 = iwork[indibl + i - 2];
        w[i - 1] = w[j_];
        iwork[indibl + i - 2] = iwork[indibl + j_ - 1];
        w[j_] = tmp1;
        iwork[indibl + j_ - 1] = itmp1;
        sswap( n, &Z(i - 1,0), 1, &Z(j_,0), 1 );
        if( info != 0 ) { 
          itmp1 = ifail[i - 1];
          ifail[i - 1] = ifail[j_];
          ifail[j_] = itmp1;
        }
      }
    }
  }
  
  //     Set WORK(1) to optimal workspace size.
  
  work[0] = max( 7*n, lopt );
  
  return;
  
  //     End of SSYEVX
  
#undef  Z
#undef  A
} // end of function 


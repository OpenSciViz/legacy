/*
 * C++ implementation of Lapack routine zgttrs
 *
 * $Id: zgttrs.cpp,v 1.2 1993/07/21 22:26:56 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:47:09
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zgttrs.cpp,v $
 * Revision 1.2  1993/07/21  22:26:56  alv
 * ported to Microsoft C++ v8
 *
// Revision 1.1  1993/06/24  22:47:05  alv
// Initial revision
//
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

RWLAPKDECL void /*FUNCTION*/ zgttrs(const char &trans, const long &n, const long &nrhs, 
   DComplex dl[], DComplex d[], DComplex du[], DComplex du2[], long ipiv[], 
   DComplex *b, const long &ldb, long &info)
{
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
  int notran;
  long _do0, _do1, _do2, _do3, _do4, _do5, i, i_, j, j_;
  DComplex temp;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZGTTRS solves one of the systems of equations
  //     A * X = B,  A**T * X = B,  or  A**H * X = B,
  //  with a tridiagonal matrix A using the LU factorization computed
  //  by ZGTTRF.
  
  //  Arguments
  //  =========
  
  //  TRANS   (input) CHARACTER
  //          Specifies the form of the system of equations.
  //          = 'N':  A * X = B     (No transpose)
  //          = 'T':  A**T * X = B  (Transpose)
  //          = 'C':  A**H * X = B  (Conjugate transpose)
  
  //  N       (input) INTEGER
  //          The order of the matrix A.
  
  //  NRHS    (input) INTEGER
  //          The number of right hand sides, i.e., the number of columns
  //          of the matrix B.  NRHS >= 0.
  
  //  DL      (input) COMPLEX*16 array, dimension (N-1)
  //          The (n-1) multipliers that define the matrix L from the
  //          LU factorization of A.
  
  //  D       (input) COMPLEX*16 array, dimension (N)
  //          The n diagonal elements of the upper triangular matrix U from
  //          the LU factorization of A.
  
  //  DU      (input) COMPLEX*16 array, dimension (N-1)
  //          The (n-1) elements of the first super-diagonal of U.
  
  //  DU2     (input) COMPLEX*16 array, dimension (N-2)
  //          The (n-2) elements of the second super-diagonal of U.
  
  //  IPIV    (input) INTEGER array, dimension (N)
  //          The pivot indices; for 1 <= i <= n, row i of the matrix was
  //          interchanged with row IPIV(i).  IPIV(i) will always be either
  //          i or i+1; IPIV(i) = i indicates a row interchange was not
  //          required.
  
  //  B       (input/output) COMPLEX*16 array, dimension (LDB,NRHS)
  //          On entry, the matrix of right hand side vectors B.
  //          On exit, B is overwritten by the solution vectors X.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  INFO    (output)
  //          = 0:  successful exit
  //          < 0:  if INFO = -k, the k-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  info = 0;
  notran = lsame( trans, 'N' );
  if( (!notran && !lsame( trans, 'T' )) && !lsame( trans, 'C' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( nrhs < 0 ) { 
    info = -3;
  }
  else if( ldb < max( n, 1 ) ) { 
    info = -10;
  }
  if( info != 0 ) { 
    xerbla( "ZGTTRS", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 || nrhs == 0 ) 
    return;
  
  if( notran ) { 
    
    //        Solve A*X = B using the LU factorization of A,
    //        overwriting each right hand side vector with its solution.
    
    for( j = 1, j_ = j - 1, _do0 = nrhs; j <= _do0; j++, j_++ ) { 
      
      //           Solve L*x = b.
      
      for( i = 1, i_ = i - 1, _do1 = n - 1; i <= _do1; i++, i_++ ) { 
        if( ipiv[i_] == i ) { 
          B(j_,i_ + 1) = B(j_,i_ + 1) - dl[i_]*B(j_,i_);
        }
        else { 
          temp = B(j_,i_);
          B(j_,i_) = B(j_,i_ + 1);
          B(j_,i_ + 1) = temp - dl[i_]*B(j_,i_);
        }
      }
      
      //           Solve U*x = b.
      
      B(j_,n - 1) = B(j_,n - 1)/d[n - 1];
      if( n > 1 ) 
        B(j_,n - 2) = (B(j_,n - 2) - du[n - 2]*B(j_,n - 1))/
         d[n - 2];
      for( i = n - 2, i_ = i - 1; i >= 1; i--, i_-- ) { 
        B(j_,i_) = (B(j_,i_) - du[i_]*B(j_,i_ + 1) - du2[i_]*
         B(j_,i_ + 2))/d[i_];
      }
    }
  }
  else if( lsame( trans, 'T' ) ) { 
    
    //        Solve A**T * X = B.
    
    for( j = 1, j_ = j - 1, _do2 = nrhs; j <= _do2; j++, j_++ ) { 
      
      //           Solve U**T * x = b.
      
      B(j_,0) = B(j_,0)/d[0];
      if( n > 1 ) 
        B(j_,1) = (B(j_,1) - du[0]*B(j_,0))/d[1];
      for( i = 3, i_ = i - 1, _do3 = n; i <= _do3; i++, i_++ ) { 
        B(j_,i_) = (B(j_,i_) - du[i_ - 1]*B(j_,i_ - 1) - du2[i_ - 2]*
         B(j_,i_ - 2))/d[i_];
      }
      
      //           Solve L**T * x = b.
      
      for( i = n - 1, i_ = i - 1; i >= 1; i--, i_-- ) { 
        if( ipiv[i_] == i ) { 
          B(j_,i_) = B(j_,i_) - dl[i_]*B(j_,i_ + 1);
        }
        else { 
          temp = B(j_,i_ + 1);
          B(j_,i_ + 1) = B(j_,i_) - dl[i_]*temp;
          B(j_,i_) = temp;
        }
      }
    }
  }
  else { 
    
    //        Solve A**H * X = B.
    
    for( j = 1, j_ = j - 1, _do4 = nrhs; j <= _do4; j++, j_++ ) { 
      
      //           Solve U**H * x = b.
      
      B(j_,0) = B(j_,0)/conj( d[0] );
      if( n > 1 ) 
        B(j_,1) = (B(j_,1) - conj( du[0] )*B(j_,0))/conj( d[1] );
      for( i = 3, i_ = i - 1, _do5 = n; i <= _do5; i++, i_++ ) { 
        // Added temp in here to help Microsoft compiler
        DComplex temp1 = conj( du[i_ - 1] )*B(j_,i_ - 1);
        DComplex temp2 = conj( du2[i_ - 2] )*B(j_,i_ - 2);
        B(j_,i_) = (B(j_,i_) - temp1 - temp2)/conj( d[i_] );
      }
      
      //           Solve L**H * x = b.
      
      for( i = n - 1, i_ = i - 1; i >= 1; i--, i_-- ) { 
        if( ipiv[i_] == i ) { 
          B(j_,i_) = B(j_,i_) - conj( dl[i_] )*B(j_,i_ + 1);
        }
        else { 
          temp = B(j_,i_ + 1);
          B(j_,i_ + 1) = B(j_,i_) - conj( dl[i_] )*temp;
          B(j_,i_) = temp;
        }
      }
    }
  }
  
  //     End of ZGTTRS
  
  return;
#undef  B
} // end of function 


/*
 * C++ implementation of Lapack routine spotrs
 *
 * $Id: spotrs.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:01:56
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: spotrs.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ spotrs(const char &uplo, const long &n, const long &nrhs, 
 float *a, const long &lda, float *b, const long &ldb, long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
  int upper;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SPOTRS solves a system of linear equations A*X = B with a symmetric
  //  positive definite matrix A using the Cholesky factorization A = U'*U
  //  or A = L*L' computed by SPOTRF.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the factor stored in A is upper or lower
  //          triangular.
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right hand sides, i.e., the number of columns
  //          of the matrix B.  NRHS >= 0.
  
  //  A       (input) REAL array, dimension (LDA,N)
  //          The triangular factor U or L from the Cholesky factorization
  //          A = U'*U or A = L*L', as computed by SPOTRF.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  B       (input/output) REAL array, dimension (LDB,NRHS)
  //          On entry, the right hand side vectors B for the system of
  //          linear equations.
  //          On exit, the solution vectors, X.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( nrhs < 0 ) { 
    info = -3;
  }
  else if( lda < max( 1, n ) ) { 
    info = -5;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -7;
  }
  if( info != 0 ) { 
    xerbla( "SPOTRS", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 || nrhs == 0 ) 
    return;
  
  if( upper ) { 
    
    //        Solve A*X = B where A = U'*U.
    
    //        Solve U'*X = B, overwriting B with X.
    
    strsm( 'L'/*Left*/, 'U'/*Upper*/, 'T'/*Transpose*/, 'N'/*Non-unit*/
     , n, nrhs, ONE, a, lda, b, ldb );
    
    //        Solve U*X = B, overwriting B with X.
    
    strsm( 'L'/*Left*/, 'U'/*Upper*/, 'N'/*No transpose*/, 
     'N'/*Non-unit*/, n, nrhs, ONE, a, lda, b, ldb );
  }
  else { 
    
    //        Solve A*X = B where A = L*L'.
    
    //        Solve L*X = B, overwriting B with X.
    
    strsm( 'L'/*Left*/, 'L'/*Lower*/, 'N'/*No transpose*/, 
     'N'/*Non-unit*/, n, nrhs, ONE, a, lda, b, ldb );
    
    //        Solve L'*X = B, overwriting B with X.
    
    strsm( 'L'/*Left*/, 'L'/*Lower*/, 'T'/*Transpose*/, 'N'/*Non-unit*/
     , n, nrhs, ONE, a, lda, b, ldb );
  }
  
  return;
  
  //     End of SPOTRS
  
#undef  B
#undef  A
} // end of function 


/*
 * C++ implementation of Lapack routine sspgv
 *
 * $Id: sspgv.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:02:29
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: sspgv.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

RWLAPKDECL void /*FUNCTION*/ sspgv(const long &itype, const char &jobz, const char &uplo, const long &n, 
   float ap[], float bp[], float w[], float *z, const long &ldz, float work[], 
   long &info)
{
#define Z(I_,J_)  (*(z+(I_)*(ldz)+(J_)))
  int upper, wantz;
  char trans;
  long _do0, _do1, j, j_, neig;

  
  //  -- LAPACK driver routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SSPGV computes the eigenvalues and optionally the eigenvectors of a
  //  real generalized symmetric-definite eigenproblem, of the form
  //  A*x=(lambda)*B*x,  A*Bx=(lambda)*x,  or B*A*x=(lambda)*x.
  //  Here A and B are assumed to be symmetric and B is also
  //  positive definite. The matrices A and B use packed storage.
  
  //  Arguments
  //  =========
  
  //  ITYPE   (input) INTEGER
  //          Specifies the problem type to be solved:
  //          = 1:  A*x = (lambda)*B*x
  //          = 2:  A*B*x = (lambda)*x
  //          = 3:  B*A*x = (lambda)*x
  
  //  JOBZ    (input) CHARACTER*1
  //          Specifies whether or not to compute the eigenvectors:
  //          = 'N':  Compute eigenvalues only.
  //          = 'V':  Compute eigenvalues and eigenvectors.
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangular part of the
  //          symmetric matrices A and B are stored.
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The number of rows and columns of the matrix A.  N >= 0.
  
  //  AP      (input/workspace) REAL array, dimension
  //                            (N*(N+1)/2)
  //          On entry, the upper or lower triangle of the symmetric matrix
  //          A, packed columnwise in a linear array.  The j-th column of A
  //          is stored in the array AP as follows:
  //          if UPLO = 'U', AP(i + (j-1)*j/2) = A(i,j) for 1<=i<=j;
  //          if UPLO = 'L', AP(i + (j-1)*(2n-j)/2) = A(i,j) for j<=i<=n.
  
  //          On exit, the contents of AP are destroyed.
  
  //  BP      (input/output) REAL array, dimension (LDB, N)
  //          On entry, the upper or lower triangle of the symmetric matrix
  //          B, packed columnwise in a linear array.  The j-th column of B
  //          is stored in the array BP as follows:
  //          if UPLO = 'U', BP(i + (j-1)*j/2) = B(i,j) for 1<=i<=j;
  //          if UPLO = 'L', BP(i + (j-1)*(2n-j)/2) = B(i,j) for j<=i<=n.
  
  //          On exit, the triangular factor U or L from the Cholesky
  //          factorization B = U'*U or B = L*L', in the same storage
  //          format as B.
  
  //  W       (output) REAL array, dimension (N)
  //          On exit, if INFO = 0, W contains the eigenvalues in ascending
  //          order.  If 0 < INFO <= N, the eigenvalues are correct for
  //          indices 1, 2, ..., INFO-1, but they are unordered and may not
  //          be the smallest eigenvalues of the matrix.
  
  //  Z       (output) REAL array, dimension (LDZ, N)
  //          If JOBZ = 'V', then if INFO = 0 on exit, Z contains the
  //          matrix Z of eigenvectors of the matrix A.  If 0 < INFO <= N,
  //          Z contains the eigenvectors associated with only the stored
  //          eigenvalues. The eigenvectors are normalized as follows:
  //          if ITYPE = 1 or 2, Z'*B*Z = I; if ITYPE = 3, Z'*inv(B)*Z = I.
  //          If JOBZ = 'N', then Z is not referenced.
  
  //  LDZ     (input) INTEGER
  //          The leading dimension of the array Z.  LDZ >= 1, and if
  //          JOBZ = 'V', LDZ >= max(1,N).
  
  //  WORK    (workspace) REAL array, dimension (3*N)
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit.
  //          < 0:  if INFO = -k, the k-th argument had an illegal value.
  //          > 0:  SPPTRF or SSPEV returned an error code
  //             <= n:  if INFO = k, then SSPEV terminated before finding
  //                    the k-th eigenvalue.
  //             > n:   if INFO = n + k, for 1 <= k <= n, then the leading
  //                    minor of order k of B is not positive definite.
  //                    The factorization of B could not be completed and
  //                    no eigenvalues or eigenvectors were computed.
  
  //  =====================================================================
  
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  wantz = lsame( jobz, 'V' );
  upper = lsame( uplo, 'U' );
  
  info = 0;
  if( itype < 0 || itype > 3 ) { 
    info = -1;
  }
  else if( !(wantz || lsame( jobz, 'N' )) ) { 
    info = -2;
  }
  else if( !(upper || lsame( uplo, 'L' )) ) { 
    info = -3;
  }
  else if( n < 0 ) { 
    info = -4;
  }
  else if( ldz < 1 || (wantz && ldz < n) ) { 
    info = -9;
  }
  if( info != 0 ) { 
    xerbla( "SSPGV ", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  //     Form a Cholesky factorization of B.
  
  spptrf( uplo, n, bp, info );
  if( info != 0 ) { 
    info = n + info;
    return;
  }
  
  //     Transform problem to standard eigenvalue problem and solve.
  
  sspgst( itype, uplo, n, ap, bp, info );
  sspev( jobz, uplo, n, ap, w, z, ldz, work, info );
  
  if( wantz ) { 
    
    //        Backtransform eigenvectors to the original problem.
    
    neig = n;
    if( info > 0 ) 
      neig = info - 1;
    if( itype == 1 || itype == 2 ) { 
      
      //           For A*x=(lambda)*B*x and A*B*x=(lambda)*x;
      //           backtransform eigenvectors: x = inv(L)'*y or inv(U)*y
      
      if( upper ) { 
        trans = 'N';
      }
      else { 
        trans = 'T';
      }
      
      for( j = 1, j_ = j - 1, _do0 = neig; j <= _do0; j++, j_++ ) { 
        stpsv( uplo, trans, 'N'/*Non-unit*/, n, bp, &Z(j_,0), 
         1 );
      }
      
    }
    else if( itype == 3 ) { 
      
      //           For B*A*x=(lambda)*x;
      //           backtransform eigenvectors: x = L*y or U'*y
      
      if( upper ) { 
        trans = 'T';
      }
      else { 
        trans = 'N';
      }
      
      for( j = 1, j_ = j - 1, _do1 = neig; j <= _do1; j++, j_++ ) { 
        stpmv( uplo, trans, 'N'/*Non-unit*/, n, bp, &Z(j_,0), 
         1 );
      }
    }
  }
  return;
  
  //     End of SSPGV
  
#undef  Z
} // end of function 


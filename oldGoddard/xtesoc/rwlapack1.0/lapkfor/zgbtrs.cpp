/*
 * C++ implementation of Lapack routine zgbtrs
 *
 * $Id: zgbtrs.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:46:01
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zgbtrs.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const DComplex ONE = DComplex(1.0e0);
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zgbtrs(const char &trans, const long &n, const long &kl, const long &ku, 
 const long &nrhs, DComplex *ab, const long &ldab, long ipiv[], DComplex *b, 
 const long &ldb, long &info)
{
#define AB(I_,J_) (*(ab+(I_)*(ldab)+(J_)))
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
  int lnoti, notran;
  long _do0, _do1, _do2, _do3, i, i_, j, j_, kd, l, lm;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZGBTRS solves a system of linear equations
  //     A * X = B,  A**T * X = B,  or  A**H * X = B
  //  with a general band matrix A using the LU factorization computed
  //  by ZGBTRF.
  
  //  Arguments
  //  =========
  
  //  TRANS   (input) CHARACTER*1
  //          Specifies the form of the system of equations.
  //          = 'N':  A * X = B     (No transpose)
  //          = 'T':  A**T * X = B  (Transpose)
  //          = 'C':  A**H * X = B  (Conjugate transpose)
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  KL      (input) INTEGER
  //          The number of subdiagonals within the band of A.  KL >= 0.
  
  //  KU      (input) INTEGER
  //          The number of superdiagonals within the band of A.  KU >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right hand sides, i.e., the number of columns
  //          of the matrix B.  NRHS >= 0.
  
  //  AB      (input) COMPLEX*16 array, dimension (LDAB,N)
  //          Details of the LU factorization of the band matrix A, as
  //          computed by ZGBTRF.  U is stored as an upper triangular band
  //          matrix with KL+KU superdiagonals in rows 1 to KL+KU+1, and
  //          the multipliers used during the factorization are stored in
  //          rows KL+KU+2 to 2*KL+KU+1.
  
  //  LDAB    (input) INTEGER
  //          The leading dimension of the array AB.  LDAB >= 2*KL+KU+1.
  
  //  IPIV    (input) INTEGER array, dimension (N)
  //          The pivot indices; for 1 <= i <= N, row i of the matrix was
  //          interchanged with row IPIV(i).
  
  //  B       (input/output) COMPLEX*16 array, dimension (LDB,NRHS)
  //          On entry, the right hand side vectors B for the system of
  //          linear equations.
  //          On exit, the solution vectors, X.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  notran = lsame( trans, 'N' );
  if( (!notran && !lsame( trans, 'T' )) && !lsame( trans, 'C' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( kl < 0 ) { 
    info = -3;
  }
  else if( ku < 0 ) { 
    info = -4;
  }
  else if( nrhs < 0 ) { 
    info = -5;
  }
  else if( ldab < (2*kl + ku + 1) ) { 
    info = -7;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -10;
  }
  if( info != 0 ) { 
    xerbla( "ZGBTRS", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 || nrhs == 0 ) 
    return;
  
  kd = ku + kl + 1;
  lnoti = kl > 0;
  
  if( notran ) { 
    
    //        Solve  A*X = B.
    
    //        Solve L*X = B, overwriting B with X.
    
    //        L is represented as a product of permutations and unit lower
    //        triangular matrices L = P(1) * L(1) * ... * P(n-1) * L(n-1),
    //        where each transformation L(i) is a rank-one modification of
    //        the identity matrix.
    
    if( lnoti ) { 
      for( j = 1, j_ = j - 1, _do0 = n - 1; j <= _do0; j++, j_++ ) { 
        lm = min( kl, n - j );
        l = ipiv[j_];
        if( l != j ) 
          zswap( nrhs, &B(0,l - 1), ldb, &B(0,j_), ldb );
        zgeru( lm, nrhs, -(ONE), &AB(j_,kd), 1, &B(0,j_), 
         ldb, &B(0,j_ + 1), ldb );
      }
    }
    
    for( i = 1, i_ = i - 1, _do1 = nrhs; i <= _do1; i++, i_++ ) { 
      
      //           Solve U*X = B, overwriting B with X.
      
      ztbsv( 'U'/*Upper*/, 'N'/*No transpose*/, 'N'/*Non-unit*/
       , n, kl + ku, ab, ldab, &B(i_,0), 1 );
    }
    
  }
  else if( lsame( trans, 'T' ) ) { 
    
    //        Solve A**T * X = B.
    
    for( i = 1, i_ = i - 1, _do2 = nrhs; i <= _do2; i++, i_++ ) { 
      
      //           Solve U**T * X = B, overwriting B with X.
      
      ztbsv( 'U'/*Upper*/, 'T'/*Transpose*/, 'N'/*Non-unit*/
       , n, kl + ku, ab, ldab, &B(i_,0), 1 );
    }
    
    //        Solve L**T * X = B, overwriting B with X.
    
    if( lnoti ) { 
      for( j = n - 1, j_ = j - 1; j >= 1; j--, j_-- ) { 
        lm = min( kl, n - j );
        zgemv( 'T'/*Transpose*/, lm, nrhs, -(ONE), &B(0,j_ + 1), 
         ldb, &AB(j_,kd), 1, ONE, &B(0,j_), ldb );
        l = ipiv[j_];
        if( l != j ) 
          zswap( nrhs, &B(0,l - 1), ldb, &B(0,j_), ldb );
      }
    }
    
  }
  else { 
    
    //        Solve A**H * X = B.
    
    for( i = 1, i_ = i - 1, _do3 = nrhs; i <= _do3; i++, i_++ ) { 
      
      //           Solve U**H * X = B, overwriting B with X.
      
      ztbsv( 'U'/*Upper*/, 'C'/*Conjugate transpose*/, 'N'/*Non-unit*/
       , n, kl + ku, ab, ldab, &B(i_,0), 1 );
    }
    
    //        Solve L**H * X = B, overwriting B with X.
    
    if( lnoti ) { 
      for( j = n - 1, j_ = j - 1; j >= 1; j--, j_-- ) { 
        lm = min( kl, n - j );
        zlacgv( nrhs, &B(0,j_), ldb );
        zgemv( 'C'/*Conjugate transpose*/, lm, nrhs, -(ONE), 
         &B(0,j_ + 1), ldb, &AB(j_,kd), 1, ONE, &B(0,j_), 
         ldb );
        zlacgv( nrhs, &B(0,j_), ldb );
        l = ipiv[j_];
        if( l != j ) 
          zswap( nrhs, &B(0,l - 1), ldb, &B(0,j_), ldb );
      }
    }
  }
  return;
  
  //     End of ZGBTRS
  
#undef  B
#undef  AB
} // end of function 


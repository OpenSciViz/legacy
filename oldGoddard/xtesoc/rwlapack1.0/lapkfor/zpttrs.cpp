/*
 * C++ implementation of Lapack routine zpttrs
 *
 * $Id: zpttrs.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:50:24
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zpttrs.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

RWLAPKDECL void /*FUNCTION*/ zpttrs(const char &uplo, const long &n, const long &nrhs, 
   double d[], DComplex e[], DComplex *b, const long &ldb, long &info)
{
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
  int upper;
  long _do0, _do1, _do2, _do3, i, i_, j, j_;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZPTTRS solves a tridiagonal system of the form
  //     A * X = B
  //  using the factorization A = U'*D*U or A = L*D*L' computed by ZPTTRF.
  //  D is a diagonal matrix specified in the vector D, U (or L) is a unit
  //  bidiagonal matrix whose superdiagonal (subdiagonal) is specified in
  //  the vector E, and X and B are N by NRHS matrices.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies the form of the factorization and whether the
  //          vector E is the superdiagonal of the upper bidiagonal factor
  //          U or the subdiagonal of the lower bidiagonal factor L.
  //          = 'U':  A = U'*D*U, E is the superdiagonal of U
  //          = 'L':  A = L*D*L', E is the subdiagonal of L
  
  //  N       (input) INTEGER
  //          The order of the tridiagonal matrix A.  N >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right hand sides, i.e., the number of columns
  //          of the matrix B.  NRHS >= 0.
  
  //  D       (input) DOUBLE PRECISION array, dimension (N)
  //          The n diagonal elements of the diagonal matrix D from the
  //          factorization A = U'*D*U or A = L*D*L'.
  
  //  E       (input) COMPLEX*16 array, dimension (N-1)
  //          If UPLO = 'U', the (n-1) superdiagonal elements of the unit
  //          bidiagonal factor U from the factorization A = U'*D*U.
  //          If UPLO = 'L', the (n-1) subdiagonal elements of the unit
  //          bidiagonal factor L from the factorization A = L*D*L'.
  
  //  B       (input/output) DOUBLE PRECISION array, dimension (LDB,NRHS)
  //          On entry, the right hand side vectors B for the system of
  //          linear equations.
  //          On exit, the solution vectors, X.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input arguments.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( nrhs < 0 ) { 
    info = -3;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -7;
  }
  if( info != 0 ) { 
    xerbla( "ZPTTRS", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  if( upper ) { 
    
    //        Solve A * X = B using the factorization A = U'*D*U,
    //        overwriting each right hand side vector with its solution.
    
    for( j = 1, j_ = j - 1, _do0 = nrhs; j <= _do0; j++, j_++ ) { 
      
      //           Solve U' * x = b.
      
      for( i = 2, i_ = i - 1, _do1 = n; i <= _do1; i++, i_++ ) { 
        B(j_,i_) = B(j_,i_) - B(j_,i_ - 1)*conj( e[i_ - 1] );
      }
      
      //           Solve D * U * x = b.
      
      B(j_,n - 1) = B(j_,n - 1)/d[n - 1];
      for( i = n - 1, i_ = i - 1; i >= 1; i--, i_-- ) { 
        B(j_,i_) = B(j_,i_)/d[i_] - B(j_,i_ + 1)*e[i_];
      }
    }
  }
  else { 
    
    //        Solve A * X = B using the factorization A = L*D*L',
    //        overwriting each right hand side vector with its solution.
    
    for( j = 1, j_ = j - 1, _do2 = nrhs; j <= _do2; j++, j_++ ) { 
      
      //           Solve L * x = b.
      
      for( i = 2, i_ = i - 1, _do3 = n; i <= _do3; i++, i_++ ) { 
        B(j_,i_) = B(j_,i_) - B(j_,i_ - 1)*e[i_ - 1];
      }
      
      //           Solve D * L' * x = b.
      
      B(j_,n - 1) = B(j_,n - 1)/d[n - 1];
      for( i = n - 1, i_ = i - 1; i >= 1; i--, i_-- ) { 
        B(j_,i_) = B(j_,i_)/d[i_] - B(j_,i_ + 1)*conj( e[i_] );
      }
    }
  }
  
  return;
  
  //     End of ZPTTRS
  
#undef  B
} // end of function 


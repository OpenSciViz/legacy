/*
 * C++ implementation of Lapack routine zlaein
 *
 * $Id: zlaein.cpp,v 1.2 1993/07/21 22:22:14 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:48:14
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zlaein.cpp,v $
 * Revision 1.2  1993/07/21  22:22:14  alv
 * ported to Microsoft visual C++
 *
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ONE = 1.0e0;
const double TENTH = 1.0e-1;
const DComplex ZERO = DComplex(0.0e0);
// end of PARAMETER translations

inline double zlaein_cabs1(DComplex cdum) { return abs( real( (cdum) ) ) + 
   abs( imag( (cdum) ) ); }
RWLAPKDECL void /*FUNCTION*/ zlaein(const int &rightv, const int &noinit, const long &n, 
 DComplex *h, const long &ldh, const DComplex &w, DComplex v[], DComplex *b, const long &ldb, 
 double rwork[], const double &eps3, const double &smlnum, long &info)
{
#define H(I_,J_)  (*(h+(I_)*(ldh)+(J_)))
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
  char normin, trans;
  long _do0, _do1, _do2, _do3, _do4, _do5, _do6, _do7, _do8, 
   _do9, i, i_, ierr, its, its_, j, j_;
  double growto, nrmsml, rootn, rtemp, scale, vnorm;
  DComplex cdum, ei, ej, temp, x;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZLAEIN uses inverse iteration to find a right or left eigenvector
  //  corresponding to the eigenvalue W of a DComplex upper Hessenberg
  //  matrix H.
  
  //  Arguments
  //  =========
  
  //  RIGHTV   (input) LOGICAL
  //          = .TRUE. : compute right eigenvector;
  //          = .FALSE.: compute left eigenvector.
  
  //  NOINIT   (input) LOGICAL
  //          = .TRUE. : no initial vector supplied in V
  //          = .FALSE.: initial vector supplied in V.
  
  //  N       (input) INTEGER
  //          The order of the matrix H.  N >= 0.
  
  //  H       (input) COMPLEX*16 array, dimension (LDH,N)
  //          The upper Hessenberg matrix H.
  
  //  LDH     (input) INTEGER
  //          The leading dimension of the array H.  LDH >= max(1,N).
  
  //  W       (input) COMPLEX*16
  //          The eigenvalue of H whose corresponding right or left
  //          eigenvector is to be computed.
  
  //  V       (input/output) COMPLEX*16 array, dimension (N)
  //          On entry, if NOINIT = .FALSE., V must contain a starting
  //          vector for inverse iteration; otherwise V need not be set.
  //          On exit, V contains the computed eigenvector, normalized so
  //          that the component of largest magnitude has magnitude 1; here
  //          the magnitude of a DComplex number (x,y) is taken to be
  //          |x| + |y|.
  
  //  B       (workspace) COMPLEX*16 array, dimension (LDB,N)
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  RWORK   (workspace) DOUBLE PRECISION array, dimension (N)
  
  //  EPS3    (input) DOUBLE PRECISION
  //          A small machine-dependent value which is used to perturb
  //          close eigenvalues, and to replace zero pivots.
  
  //  SMLNUM  (input) DOUBLE PRECISION
  //          A machine-dependent value close to the underflow threshold.
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          = 1:  inverse iteration did not converge; V is set to the
  //                last iterate.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Statement Functions ..
  //     ..
  //     .. Statement Function definitions ..
  //     ..
  //     .. Executable Statements ..
  
  info = 0;
  
  //     GROWTO is the threshold used in the acceptance test for an
  //     eigenvector.
  
  rootn = sqrt( (double)( n ) );
  growto = TENTH/rootn;
  nrmsml = max( ONE, eps3*rootn )*smlnum;
  
  //     Form B = H - W*I (except that the subdiagonal elements are not
  //     stored).
  
  for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
    for( i = 1, i_ = i - 1, _do1 = j - 1; i <= _do1; i++, i_++ ) { 
      B(j_,i_) = H(j_,i_);
    }
    B(j_,j_) = H(j_,j_) - w;
  }
  
  if( noinit ) { 
    
    //        Initialize V.
    
    for( i = 1, i_ = i - 1, _do2 = n; i <= _do2; i++, i_++ ) { 
      v[i_] = DComplex(eps3);
    }
  }
  else { 
    
    //        Scale supplied initial vector.
    
    vnorm = dznrm2( n, v, 1 );
    zdscal( n, (eps3*rootn)/max( vnorm, nrmsml ), v, 1 );
  }
  
  if( rightv ) { 
    
    //        LU decomposition with partial pivoting of B, replacing zero
    //        pivots by EPS3.
    
    for( i = 1, i_ = i - 1, _do3 = n - 1; i <= _do3; i++, i_++ ) { 
      ei = H(i_,i_ + 1);
      if( zlaein_cabs1( B(i_,i_) ) < zlaein_cabs1( ei ) ) { 
        
        //              Interchange rows and eliminate.
        
        x = zladiv( B(i_,i_), ei );
        B(i_,i_) = ei;
        for( j = i + 1, j_ = j - 1, _do4 = n; j <= _do4; j++, j_++ ) { 
          temp = B(j_,i_ + 1);
          B(j_,i_ + 1) = B(j_,i_) - x*temp;
          B(j_,i_) = temp;
        }
      }
      else { 
        
        //              Eliminate without interchange.
        
        if( ctocf(B(i_,i_)) == ctocf(ZERO) ) 
          B(i_,i_) = (DComplex)(eps3);
        x = zladiv( ei, B(i_,i_) );
        if( ctocf(x) != ctocf(ZERO) ) { 
          for( j = i + 1, j_ = j - 1, _do5 = n; j <= _do5; j++, j_++ ) { 
            B(j_,i_ + 1) = B(j_,i_ + 1) - x*B(j_,i_);
          }
        }
      }
    }
    if( ctocf(B(n - 1,n - 1)) == ctocf(ZERO) ) 
      B(n - 1,n - 1) = (DComplex)(eps3);
    
    trans = 'N';
    
  }
  else { 
    
    //        UL decomposition with partial pivoting of B, replacing zero
    //        pivots by EPS3.
    
    for( j = n, j_ = j - 1; j >= 2; j--, j_-- ) { 
      ej = H(j_ - 1,j_);
      if( zlaein_cabs1( B(j_,j_) ) < zlaein_cabs1( ej ) ) { 
        
        //              Interchange columns and eliminate.
        
        x = zladiv( B(j_,j_), ej );
        B(j_,j_) = ej;
        for( i = 1, i_ = i - 1, _do6 = j - 1; i <= _do6; i++, i_++ ) { 
          temp = B(j_ - 1,i_);
          B(j_ - 1,i_) = B(j_,i_) - x*temp;
          B(j_,i_) = temp;
        }
      }
      else { 
        
        //              Eliminate without interchange.
        
        if( ctocf(B(j_,j_)) == ctocf(ZERO) ) 
          B(j_,j_) = (DComplex)(eps3);
        x = zladiv( ej, B(j_,j_) );
        if( ctocf(x) != ctocf(ZERO) ) { 
          for( i = 1, i_ = i - 1, _do7 = j - 1; i <= _do7; i++, i_++ ) { 
            B(j_ - 1,i_) = B(j_ - 1,i_) - x*B(j_,i_);
          }
        }
      }
    }
    if( ctocf(B(0,0)) == ctocf(ZERO) ) 
      B(0,0) = (DComplex)(eps3);
    
    trans = 'C';
    
  }
  
  normin = 'N';
  for( its = 1, its_ = its - 1, _do8 = n; its <= _do8; its++, its_++ ) { 
    
    //        Solve U*x = scale*v for a right eigenvector
    //          or U'*x = scale*v for a left eigenvector,
    //        overwriting x on v.
    
    zlatrs( 'U'/*Upper*/, trans, 'N'/*Nonunit*/, normin, n, 
     b, ldb, v, scale, rwork, ierr );
    normin = 'Y';
    
    //        Test for sufficient growth in the norm of v.
    
    vnorm = dzasum( n, v, 1 );
    if( vnorm >= growto*scale ) 
      goto L_120;
    
    //        Choose new orthogonal starting vector and try again.
    
    rtemp = eps3/(rootn + ONE);
    v[0] = DComplex(eps3);
    for( i = 2, i_ = i - 1, _do9 = n; i <= _do9; i++, i_++ ) { 
      v[i_] = DComplex(rtemp);
    }
    v[n - its] = v[n - its] - eps3*rootn;
  }
  
  //     Failure to find eigenvector in N iterations.
  
  info = 1;
  
L_120:
  ;
  
  //     Normalize eigenvector.
  
  i = izamax( n, v, 1 );
  zdscal( n, ONE/zlaein_cabs1( v[i - 1] ), v, 1 );
  
  return;
  
  //     End of ZLAEIN
  
#undef  H
#undef  B
} // end of function 


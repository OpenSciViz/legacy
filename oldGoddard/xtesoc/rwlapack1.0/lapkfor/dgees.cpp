/*
 * C++ implementation of lapack routine dgees
 *
 * $Id: dgees.cpp,v 1.5 1993/04/06 20:40:26 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:33:56
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dgees.cpp,v $
 * Revision 1.5  1993/04/06  20:40:26  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:14:33  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:06:33  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dgees(const char &jobvs, const char &sort, int (*select)(double[],double[]), 
 const long &n, double *a, const long &lda, const long &sdim, double wr[], 
 double wi[], double *vs, const long &ldvs, double work[], const long &lwork, 
 int bwork[], long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define VS(I_,J_) (*(vs+(I_)*(ldvs)+(J_)))
  int cursl, lastsl, lst2sl, scalea, wantst, wantvs;
  long hswork, i, i1, i2, ibal, icond, idum[1], ierr, ieval, 
   ihi, ilo, inxt, ip, itau, iwrk, k, maxb, maxwrk, minwrk;
  double anrm, bignum, cscale, dum[1], eps, s, sep, smlnum;

  
  //  -- LAPACK driver routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  //     .. Function Arguments ..

/*
 * C++ implementation of lapack routine dsptrd
 *
 * $Id: dsptrd.cpp,v 1.5 1993/04/06 20:42:21 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:38:07
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dsptrd.cpp,v $
 * Revision 1.5  1993/04/06  20:42:21  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:17:11  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:08:59  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ONE = 1.0e0;
const double ZERO = 0.0e0;
const double HALF = 1.0e0/2.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dsptrd(const char &uplo, const long &n, double ap[], double d[], 
 double e[], double tau[], long &info)
{
  int upper;
  long _do0, i, i1, i1i1, i_, ii;
  double alpha, taui;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DSPTRD reduces a real symmetric matrix A stored in packed form to
  //  symmetric tridiagonal form T by an orthogonal similarity
  //  transformation: Q' * A * Q = T.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangular part of the
  //          symmetric matrix A is stored:
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  AP      (input/output) DOUBLE PRECISION array, dimension (N*(N+1)/2)
  //          On entry, the upper or lower triangle of the symmetric matrix
  //          A, packed columnwise in a linear array.  The j-th column of A
  //          is stored in the array AP as follows:
  //          if UPLO = 'U', AP(i + (j-1)*j/2) = A(i,j) for 1<=i<=j;
  //          if UPLO = 'L', AP(i + (j-1)*(2*n-j)/2) = A(i,j) for j<=i<=n.
  //          On exit, if UPLO = 'U', the diagonal and first superdiagonal
  //          of A are overwritten by the corresponding elements of the
  //          tridiagonal matrix T, and the elements above the first
  //          superdiagonal, with the array TAU, represent the orthogonal
  //          matrix Q as a product of elementary reflectors; if UPLO
  //          = 'L', the diagonal and first subdiagonal of A are over-
  //          written by the corresponding elements of the tridiagonal
  //          matrix T, and the elements below the first subdiagonal, with
  //          the array TAU, represent the orthogonal matrix Q as a product
  //          of elementary reflectors. See Further Details.
  
  //  D       (output) DOUBLE PRECISION array, dimension (N)
  //          The diagonal elements of the tridiagonal matrix T:
  //          D(i) = A(i,i).
  
  //  E       (output) DOUBLE PRECISION array, dimension (N-1)
  //          The off-diagonal elements of the tridiagonal matrix T:
  //          E(i) = A(i,i+1) if UPLO = 'U', E(i) = A(i+1,i) if UPLO = 'L'.
  
  //  TAU     (output) DOUBLE PRECISION array, dimension (N)
  //          The scalar factors of the elementary reflectors (see Further
  //          Details).
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit.
  //          < 0:  if INFO = -i, the i-th argument had an illegal value.
  
  //  Further Details
  //  ===============
  
  //  If UPLO = 'U', the matrix Q is represented as a product of elementary
  //  reflectors
  
  //     Q = H(n-1) . . . H(2) H(1).
  
  //  Each H(i) has the form
  
  //     H(i) = I - tau * v * v'
  
  //  where tau is a real scalar, and v is a real vector with
  //  v(i+1:n) = 0 and v(i) = 1; v(1:i-1) is stored on exit in AP,
  //  overwriting A(1:i-1,i+1), and tau is stored in TAU(i).
  
  //  If UPLO = 'L', the matrix Q is represented as a product of elementary
  //  reflectors
  
  //     Q = H(1) H(2) . . . H(n-1).
  
  //  Each H(i) has the form
  
  //     H(i) = I - tau * v * v'
  
  //  where tau is a real scalar, and v is a real vector with
  //  v(1:i) = 0 and v(i+1) = 1; v(i+2:n) is stored on exit in AP,
  //  overwriting A(i+2:n,i), and tau is stored in TAU(i).
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters
  
  info = 0;
  upper = lsame( uplo, 'U' );
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  if( info != 0 ) { 
    xerbla( "DSPTRD", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n <= 0 ) 
    return;
  
  if( upper ) { 
    
    //        Reduce the upper triangle of A.
    //        I1 is the index in AP of A(1,I+1).
    
    i1 = n*(n - 1)/2 + 1;
    for( i = n - 1, i_ = i - 1; i >= 1; i--, i_-- ) { 
      
      //           Generate elementary reflector H(i) = I - tau * v * v'
      //           to annihilate A(1:i-1,i+1)
      
      dlarfg( i, ap[i1 + i_ - 1], &ap[i1 - 1], 1, taui );
      e[i_] = ap[i1 + i_ - 1];
      
      if( taui != ZERO ) { 
        
        //              Apply H(i) from both sides to A(1:i,1:i)
        
        ap[i1 + i_ - 1] = ONE;
        
        //              Compute  y := tau * A * v  storing y in TAU(1:i)
        
        dspmv( uplo, i, taui, ap, &ap[i1 - 1], 1, ZERO, tau, 
         1 );
        
        //              Compute  w := y - 1/2 * tau * (y'*v) * v
        
        alpha = -HALF*taui*ddot( i, tau, 1, &ap[i1 - 1], 1 );
        daxpy( i, alpha, &ap[i1 - 1], 1, tau, 1 );
        
        //              Apply the transformation as a rank-2 update:
        //                 A := A - v * w' - w * v'
        
        dspr2( uplo, i, -ONE, &ap[i1 - 1], 1, tau, 1, ap );
        
        ap[i1 + i_ - 1] = e[i_];
      }
      d[i_ + 1] = ap[i1 + i_];
      tau[i_] = taui;
      i1 = i1 - i;
    }
    d[0] = ap[0];
  }
  else { 
    
    //        Reduce the lower triangle of A. II is the index in AP of
    //        A(i,i) and I1I1 is the index of A(i+1,i+1).
    
    ii = 1;
    for( i = 1, i_ = i - 1, _do0 = n - 1; i <= _do0; i++, i_++ ) { 
      i1i1 = ii + n - i + 1;
      
      //           Generate elementary reflector H(i) = I - tau * v * v'
      //           to annihilate A(i+2:n,i)
      
      dlarfg( n - i, ap[ii], &ap[ii + 1], 1, taui );
      e[i_] = ap[ii];
      
      if( taui != ZERO ) { 
        
        //              Apply H(i) from both sides to A(i+1:n,i+1:n)
        
        ap[ii] = ONE;
        
        //              Compute  y := tau * A * v  storing y in TAU(i+1:n)
        
        dspmv( uplo, n - i, taui, &ap[i1i1 - 1], &ap[ii], 
         1, ZERO, &tau[i_ + 1], 1 );
        
        //              Compute  w := y - 1/2 * tau * (y'*v) * v
        
        alpha = -HALF*taui*ddot( n - i, &tau[i_ + 1], 1, &ap[ii], 
         1 );
        daxpy( n - i, alpha, &ap[ii], 1, &tau[i_ + 1], 1 );
        
        //              Apply the transformation as a rank-2 update:
        //                 A := A - v * w' - w * v'
        
        dspr2( uplo, n - i, -ONE, &ap[ii], 1, &tau[i_ + 1], 
         1, &ap[i1i1 - 1] );
        
        ap[ii] = e[i_];
      }
      d[i_] = ap[ii - 1];
      tau[i_] = taui;
      ii = i1i1;
    }
    d[n - 1] = ap[ii - 1];
  }
  tau[n - 1] = ZERO;
  
  return;
  
  //     End of DSPTRD
  
} // end of function 


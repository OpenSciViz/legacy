/*
 * C++ implementation of lapack routine dlacon
 *
 * $Id: dlacon.cpp,v 1.6 1993/04/06 20:40:52 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:34:58
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dlacon.cpp,v $
 * Revision 1.6  1993/04/06  20:40:52  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.5  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.4  1993/03/19  16:57:19  alv
 * sprinkled in some const
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:15:07  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:07:04  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
long ITMAX = 5;
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
const double TWO = 2.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dlacon(const long &n, double v[], double x[], long isgn[], 
 double &est, long &kase)
{
  static long _do0, _do1, _do2, _do3, _do4, _do5, i, i_, iter, 
   j, jlast, jump;
  static double altsgn, estold, temp;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DLACON estimates the 1-norm of a square, real matrix A.
  //  Reverse communication is used for evaluating matrix-vector products.
  
  //  Arguments
  //  =========
  
  //  N      (input) INTEGER
  //         The order of the matrix.  N >= 1.
  
  //  V      (workspace) DOUBLE PRECISION array, dimension (N)
  //         On the final return, V = A*W,  where  EST = norm(V)/norm(W)
  //         (W is not returned).
  
  //  X      (input/output) DOUBLE PRECISION array, dimension (N)
  //         On an intermediate return, X should be overwritten by
  //               A * X,   if KASE=1,
  //               A' * X,  if KASE=2,
  //         and DLACON must be re-called with all the other parameters
  //         unchanged.
  
  //  ISGN   (workspace) INTEGER array, dimension (N)
  
  //  EST    (output) DOUBLE PRECISION
  //         An estimate (a lower bound) for norm(A).
  
  //  KASE   (input/output) INTEGER
  //         On the initial call to DLACON, KASE should be 0.
  //         On an intermediate return, KASE will be 1 or 2, indicating
  //         whether X should be overwritten by A * X  or A' * X.
  //         On the final return from DLACON, KASE will again be 0.
  
  //  Further Details
  //  ======= =======
  
  //  Contributed by Nick Higham, University of Manchester.
  //  Originally named SONEST, dated March 16, 1988.
  
  //  Reference: N.J. Higham, "FORTRAN codes for estimating the one-norm of
  //  a real or DComplex matrix, with applications to condition estimation",
  //  ACM Trans. Math. Soft., vol. 14, no. 4, pp. 381-396, December 1988.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Save statement ..
  //     ..
  //     .. Executable Statements ..
  
  if( kase == 0 ) { 
    for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
      x[i_] = ONE/(double)( n );
    }
    kase = 1;
    jump = 1;
    return;
  }
  
  switch( jump ) { 
    case 1: goto L_20;
    case 2: goto L_40;
    case 3: goto L_70;
    case 4: goto L_110;
    case 5: goto L_140;
  }
  
  //     ................ ENTRY   (JUMP = 1)
  //     FIRST ITERATION.  X HAS BEEN OVERWRITTEN BY A*X.
  
L_20:
  ;
  if( n == 1 ) { 
    v[0] = x[0];
    est = abs( v[0] );
    //        ... QUIT
    goto L_150;
  }
  est = dasum( n, x, 1 );
  
  for( i = 1, i_ = i - 1, _do1 = n; i <= _do1; i++, i_++ ) { 
    x[i_] = sign( ONE, x[i_] );
    isgn[i_] = nint( x[i_] );
  }
  kase = 2;
  jump = 2;
  return;
  
  //     ................ ENTRY   (JUMP = 2)
  //     FIRST ITERATION.  X HAS BEEN OVERWRITTEN BY TRANDPOSE(A)*X.
  
L_40:
  ;
  j = idamax( n, x, 1 );
  iter = 2;
  
  //     MAIN LOOP - ITERATIONS 2,3,...,ITMAX.
  
L_50:
  ;
  for( i = 1, i_ = i - 1, _do2 = n; i <= _do2; i++, i_++ ) { 
    x[i_] = ZERO;
  }
  x[j - 1] = ONE;
  kase = 1;
  jump = 3;
  return;
  
  //     ................ ENTRY   (JUMP = 3)
  //     X HAS BEEN OVERWRITTEN BY A*X.
  
L_70:
  ;
  dcopy( n, x, 1, v, 1 );
  estold = est;
  est = dasum( n, v, 1 );
  for( i = 1, i_ = i - 1, _do3 = n; i <= _do3; i++, i_++ ) { 
    if( nint( sign( ONE, x[i_] ) ) != isgn[i_] ) 
      goto L_90;
  }
  //     REPEATED SIGN VECTOR DETECTED, HENCE ALGORITHM HAS CONVERGED.
  goto L_120;
  
L_90:
  ;
  //     TEST FOR CYCLING.
  if( est <= estold ) 
    goto L_120;
  
  for( i = 1, i_ = i - 1, _do4 = n; i <= _do4; i++, i_++ ) { 
    x[i_] = sign( ONE, x[i_] );
    isgn[i_] = nint( x[i_] );
  }
  kase = 2;
  jump = 4;
  return;
  
  //     ................ ENTRY   (JUMP = 4)
  //     X HAS BEEN OVERWRITTEN BY TRANDPOSE(A)*X.
  
L_110:
  ;
  jlast = j;
  j = idamax( n, x, 1 );
  if( (x[jlast - 1] != abs( x[j - 1] )) && (iter < ITMAX) ) { 
    iter = iter + 1;
    goto L_50;
  }
  
  //     ITERATION COMPLETE.  FINAL STAGE.
  
L_120:
  ;
  altsgn = ONE;
  for( i = 1, i_ = i - 1, _do5 = n; i <= _do5; i++, i_++ ) { 
    x[i_] = altsgn*(ONE + (double)( i - 1 )/(double)( n - 1 ));
    altsgn = -altsgn;
  }
  kase = 1;
  jump = 5;
  return;
  
  //     ................ ENTRY   (JUMP = 5)
  //     X HAS BEEN OVERWRITTEN BY A*X.
  
L_140:
  ;
  temp = TWO*(dasum( n, x, 1 )/(double)( 3*n ));
  if( temp > est ) { 
    dcopy( n, x, 1, v, 1 );
    est = temp;
  }
  
L_150:
  ;
  kase = 0;
  return;
  
  //     End of DLACON
  
} // end of function 


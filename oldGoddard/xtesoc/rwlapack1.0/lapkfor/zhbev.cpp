/*
 * C++ implementation of Lapack routine zhbev
 *
 * $Id: zhbev.cpp,v 1.2 1993/07/21 22:22:14 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:47:10
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zhbev.cpp,v $
 * Revision 1.2  1993/07/21  22:22:14  alv
 * ported to Microsoft visual C++
 *
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zhbev(const char &jobz, const char &uplo, const long &n, const long &kd, 
 DComplex *ab, const long &ldab, double w[], DComplex *z, const long &ldz, 
 DComplex work[], double rwork[], long &info)
{
#define AB(I_,J_) (*(ab+(I_)*(ldab)+(J_)))
#define Z(I_,J_)  (*(z+(I_)*(ldz)+(J_)))
  int lower, wantz;
  long _do0, _do1, i, i_, iinfo, ilen, imax, inde, indrwk, iscale;
  double anrm, bignum, eps, rmax, rmin, safmin, sigma, smlnum;

  
  //  -- LAPACK driver routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZHBEV  computes all eigenvalues and, optionally, eigenvectors of a
  //  DComplex Hermitian band matrix A by calling the recommended sequence
  //  of LAPACK routines.
  
  //  Arguments
  //  =========
  
  //  JOBZ    (input) CHARACTER*1
  //          Specifies whether or not to compute the eigenvectors:
  //          = 'N':  Compute eigenvalues only.
  //          = 'V':  Compute eigenvalues and eigenvectors.
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangular part of the
  //          Hermitian band matrix A is stored:
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The number of rows and columns of the matrix A.  N >= 0.
  
  //  KD      (input) INTEGER
  //          The number of super-diagonals of the matrix A if UPLO = 'U',
  //          or the number of subdiagonals if UPLO = 'L'.  KD >= 0.
  
  //  AB      (input/output) COMPLEX*16 array, dimension (LDAB, N)
  //          On entry, the upper or lower triangle of the Hermitian band
  //          matrix A, stored in the first KD+1 rows of the array.  The
  //          j-th column of A is stored in the j-th column of the array AB
  //          as follows:
  //          if UPLO = 'U', AB(kd+1+i-j,j) = A(i,j) for max(1,j-kd)<=i<=j;
  //          if UPLO = 'L', AB(1+i-j,j)    = A(i,j) for j<=i<=min(n,j+kd).
  
  //          On exit, AB is overwritten by values generated during the
  //          reduction to tridiagonal form.
  
  //  LDAB    (input) INTEGER
  //          The leading dimension of the array AB.  LDAB >= KD + 1.
  
  //  W       (output) DOUBLE PRECISION array, dimension (N)
  //          On exit, if INFO = 0, W contains the eigenvalues in ascending
  //          order.  If INFO > 0, the eigenvalues are correct for indices
  //          1, 2, ..., INFO-1, but they are unordered and may not be the
  //          smallest eigenvalues of the matrix.
  
  //  Z       (output) COMPLEX*16 array, dimension (LDZ, N)
  //          If JOBZ = 'V', then if INFO = 0 on exit, Z contains the
  //          orthonormal eigenvectors of the matrix A.  If INFO > 0, Z
  //          contains the eigenvectors associated with only the stored
  //          eigenvalues.
  //          If JOBZ = 'N', then Z is not referenced.
  
  //  LDZ     (input) INTEGER
  //          The leading dimension of the array Z.  LDZ >= 1, and if
  //          JOBZ = 'V', LDZ >= max(1,N).
  
  //  WORK    (workspace) COMPLEX*16 array, dimension (N)
  
  //  RWORK   (workspace) DOUBLE PRECISION array, dimension (max(1,3*N-2))
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit.
  //          < 0:  if INFO = -i, the i-th argument had an illegal value.
  //          > 0:  if INFO = +i, the algorithm terminated before finding
  //                the i-th eigenvalue.
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  wantz = lsame( jobz, 'V' );
  lower = lsame( uplo, 'L' );
  
  info = 0;
  if( !(wantz || lsame( jobz, 'N' )) ) { 
    info = -1;
  }
  else if( !(lower || lsame( uplo, 'U' )) ) { 
    info = -2;
  }
  else if( n < 0 ) { 
    info = -3;
  }
  else if( kd < 0 ) { 
    info = -4;
  }
  else if( ldab < kd + 1 ) { 
    info = -6;
  }
  else if( ldz < 1 || (wantz && ldz < n) ) { 
    info = -9;
  }
  
  if( info != 0 ) { 
    xerbla( "ZHBEV ", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  if( n == 1 ) { 
    w[0] = real(AB(0,0));
    if( wantz ) 
      Z(0,0) = (DComplex)(ONE);
    return;
  }
  
  //     Get machine constants.
  
  safmin = dlamch( 'S'/*Safe minimum*/ );
  eps = dlamch( 'P'/*Precision*/ );
  smlnum = safmin/eps;
  bignum = ONE/smlnum;
  rmin = sqrt( smlnum );
  rmax = sqrt( bignum );
  
  //     Scale matrix to allowable range, if necessary.
  
  anrm = zlanhb( 'M', uplo, n, kd, ab, ldab, rwork );
  iscale = 0;
  if( anrm > ZERO && anrm < rmin ) { 
    iscale = 1;
    sigma = rmin/anrm;
  }
  else if( anrm > rmax ) { 
    iscale = 1;
    sigma = rmax/anrm;
  }
  if( iscale == 1 ) { 
    if( lower ) { 
      for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
        zdscal( min( kd + 1, n - i + 1 ), sigma, &AB(i_,0), 
         1 );
      }
    }
    else { 
      for( i = 1, i_ = i - 1, _do1 = n; i <= _do1; i++, i_++ ) { 
        ilen = min( i, kd + 1 );
        zdscal( ilen, sigma, &AB(i_,kd - ilen + 1), 1 );
      }
    }
  }
  
  //     Call ZHBTRD to reduce Hermitian band matrix to tridiagonal form.
  
  inde = 1;
  zhbtrd( jobz, uplo, n, kd, ab, ldab, w, &rwork[inde - 1], z, ldz, 
   work, iinfo );
  
  //     For eigenvalues only, call DSTERF.  For eigenvectors, call ZSTEQR.
  
  if( !wantz ) { 
    dsterf( n, w, &rwork[inde - 1], info );
  }
  else { 
    indrwk = inde + n;
    zsteqr( jobz, n, w, &rwork[inde - 1], z, ldz, &rwork[indrwk - 1], 
     info );
  }
  
  //     If matrix was scaled, then rescale eigenvalues appropriately.
  
  if( iscale == 1 ) { 
    if( info == 0 ) { 
      imax = n;
    }
    else { 
      imax = info - 1;
    }
    dscal( imax, ONE/sigma, w, 1 );
  }
  
  return;
  
  //     End of ZHBEV
  
#undef  Z
#undef  AB
} // end of function 


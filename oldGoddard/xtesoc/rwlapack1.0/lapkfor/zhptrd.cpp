/*
 * C++ implementation of Lapack routine zhptrd
 *
 * $Id: zhptrd.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:47:54
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zhptrd.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const DComplex ONE = DComplex(1.0e0);
const DComplex ZERO = DComplex(0.0e0);
const DComplex HALF = DComplex(1.0e0/2.0e0);
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zhptrd(const char &uplo, const long &n, DComplex ap[], double d[], 
 double e[], DComplex tau[], long &info)
{
  int upper;
  long _do0, i, i1, i1i1, i_, ii;
  DComplex alpha, taui;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZHPTRD reduces a DComplex Hermitian matrix A stored in packed form to
  //  real symmetric tridiagonal form T by a unitary similarity
  //  transformation: Q' * A * Q = T.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangular part of the
  //          Hermitian matrix A is stored:
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  AP      (input/output) COMPLEX*16 array, dimension (N*(N+1)/2)
  //          On entry, the upper or lower triangle of the Hermitian matrix
  //          A, packed columnwise in a linear array.  The j-th column of A
  //          is stored in the array AP as follows:
  //          if UPLO = 'U', AP(i + (j-1)*j/2) = A(i,j) for 1<=i<=j;
  //          if UPLO = 'L', AP(i + (j-1)*(2*n-j)/2) = A(i,j) for j<=i<=n.
  //          On exit, if UPLO = 'U', the diagonal and first superdiagonal
  //          of A are overwritten by the corresponding elements of the
  //          tridiagonal matrix T, and the elements above the first
  //          superdiagonal, with the array TAU, represent the unitary
  //          matrix Q as a product of elementary reflectors; if UPLO
  //          = 'L', the diagonal and first subdiagonal of A are over-
  //          written by the corresponding elements of the tridiagonal
  //          matrix T, and the elements below the first subdiagonal, with
  //          the array TAU, represent the unitary matrix Q as a product
  //          of elementary reflectors. See Further Details.
  
  //  D       (output) DOUBLE PRECISION array, dimension (N)
  //          The diagonal elements of the tridiagonal matrix T:
  //          D(i) = A(i,i).
  
  //  E       (output) DOUBLE PRECISION array, dimension (N-1)
  //          The off-diagonal elements of the tridiagonal matrix T:
  //          E(i) = A(i,i+1) if UPLO = 'U', E(i) = A(i+1,i) if UPLO = 'L'.
  
  //  TAU     (output) COMPLEX*16 array, dimension (N)
  //          The scalar factors of the elementary reflectors (see Further
  //          Details).
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit.
  //          < 0:  if INFO = -i, the i-th argument had an illegal value.
  
  //  Further Details
  //  ===============
  
  //  If UPLO = 'U', the matrix Q is represented as a product of elementary
  //  reflectors
  
  //     Q = H(n-1) . . . H(2) H(1).
  
  //  Each H(i) has the form
  
  //     H(i) = I - tau * v * v'
  
  //  where tau is a DComplex scalar, and v is a DComplex vector with
  //  v(i+1:n) = 0 and v(i) = 1; v(1:i-1) is stored on exit in AP,
  //  overwriting A(1:i-1,i+1), and tau is stored in TAU(i).
  
  //  If UPLO = 'L', the matrix Q is represented as a product of elementary
  //  reflectors
  
  //     Q = H(1) H(2) . . . H(n-1).
  
  //  Each H(i) has the form
  
  //     H(i) = I - tau * v * v'
  
  //  where tau is a DComplex scalar, and v is a DComplex vector with
  //  v(1:i) = 0 and v(i+1) = 1; v(i+2:n) is stored on exit in AP,
  //  overwriting A(i+2:n,i), and tau is stored in TAU(i).
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters
  
  info = 0;
  upper = lsame( uplo, 'U' );
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  if( info != 0 ) { 
    xerbla( "ZHPTRD", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n <= 0 ) 
    return;
  
  if( upper ) { 
    
    //        Reduce the upper triangle of A.
    //        I1 is the index in AP of A(1,I+1).
    
    i1 = n*(n - 1)/2 + 1;
    ap[i1 + n - 2] = DComplex(real( ap[i1 + n - 2] ));
    for( i = n - 1, i_ = i - 1; i >= 1; i--, i_-- ) { 
      
      //           Generate elementary reflector H(i) = I - tau * v * v'
      //           to annihilate A(1:i-1,i+1)
      
      alpha = ap[i1 + i_ - 1];
      zlarfg( i, alpha, &ap[i1 - 1], 1, taui );
      e[i_] = real(alpha);
      
      if( ctocf(taui) != ctocf(ZERO) ) { 
        
        //              Apply H(i) from both sides to A(1:i,1:i)
        
        ap[i1 + i_ - 1] = ONE;
        
        //              Compute  y := tau * A * v  storing y in TAU(1:i)
        
        zhpmv( uplo, i, taui, ap, &ap[i1 - 1], 1, ZERO, tau, 
         1 );
        
        //              Compute  w := y - 1/2 * tau * (y'*v) * v
        
        alpha = -(HALF*taui*zdotc( i, tau, 1, &ap[i1 - 1], 
         1 ));
        zaxpy( i, alpha, &ap[i1 - 1], 1, tau, 1 );
        
        //              Apply the transformation as a rank-2 update:
        //                 A := A - v * w' - w * v'
        
        zhpr2( uplo, i, -(ONE), &ap[i1 - 1], 1, tau, 1, ap );
        
      }
      ap[i1 + i_ - 1] = DComplex(e[i_]);
      d[i_ + 1] = real(ap[i1 + i_]);
      tau[i_] = taui;
      i1 = i1 - i;
    }
    d[0] = real(ap[0]);
  }
  else { 
    
    //        Reduce the lower triangle of A. II is the index in AP of
    //        A(i,i) and I1I1 is the index of A(i+1,i+1).
    
    ii = 1;
    ap[0] = DComplex(real( ap[0] ));
    for( i = 1, i_ = i - 1, _do0 = n - 1; i <= _do0; i++, i_++ ) { 
      i1i1 = ii + n - i + 1;
      
      //           Generate elementary reflector H(i) = I - tau * v * v'
      //           to annihilate A(i+2:n,i)
      
      alpha = ap[ii];
      if( i < n - 1 ) { 
        zlarfg( n - i, alpha, &ap[ii + 1], 1, taui );
      }
      else { 
        zlarfg( n - i, alpha, &ap[ii], 1, taui );
      }
      e[i_] = real(alpha);
      
      if( ctocf(taui) != ctocf(ZERO) ) { 
        
        //              Apply H(i) from both sides to A(i+1:n,i+1:n)
        
        ap[ii] = ONE;
        
        //              Compute  y := tau * A * v  storing y in TAU(i+1:n)
        
        zhpmv( uplo, n - i, taui, &ap[i1i1 - 1], &ap[ii], 
         1, ZERO, &tau[i_ + 1], 1 );
        
        //              Compute  w := y - 1/2 * tau * (y'*v) * v
        
        alpha = -(HALF*taui*zdotc( n - i, &tau[i_ + 1], 1, 
         &ap[ii], 1 ));
        zaxpy( n - i, alpha, &ap[ii], 1, &tau[i_ + 1], 1 );
        
        //              Apply the transformation as a rank-2 update:
        //                 A := A - v * w' - w * v'
        
        zhpr2( uplo, n - i, -(ONE), &ap[ii], 1, &tau[i_ + 1], 
         1, &ap[i1i1 - 1] );
        
      }
      ap[ii] = DComplex(e[i_]);
      d[i_] = real(ap[ii - 1]);
      tau[i_] = taui;
      ii = i1i1;
    }
    d[n - 1] = real(ap[ii - 1]);
  }
  tau[n - 1] = ZERO;
  
  return;
  
  //     End of ZHPTRD
  
} // end of function 


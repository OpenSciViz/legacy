/*
 * C++ implementation of lapack routine dptsvx
 *
 * $Id: dptsvx.cpp,v 1.6 1993/04/06 20:42:12 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:37:45
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dptsvx.cpp,v $
 * Revision 1.6  1993/04/06  20:42:12  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.5  1993/03/19  18:41:23  alv
 * now passes chars explicitly, rather than indirection of a string, to shut up SUN warnings
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:16:54  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:08:40  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dptsvx(const char &fact, long &n, long &nrhs, 
 double d[], double e[], double df[], double ef[], double *b, long &ldb, 
 double *x, long &ldx, double &rcond, double ferr[], double berr[], 
 double work[], long &info)
{
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
#define X(I_,J_)  (*(x+(I_)*(ldx)+(J_)))
  int nofact;
  double anorm;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DPTSVX uses the L*D*L' factorization to compute the solution to a
  //  system of linear equations
  //     A*X = B,
  //  where A is an n by n symmetric positive definite tridiagonal matrix
  //  and X and B are N by NRHS matrices.
  
  //  Error bounds on the solution and a condition estimate are also
  //  provided.
  
  //  Description
  //  ===========
  
  //  The following steps are performed by this subroutine:
  
  //  1. If FACT = 'N', the matrix A is factored as A = L*D*L', where L is
  //     a unit lower bidiagonal matrix and D is diagonal.  The
  //     factorization can also be regarded as having the form A = U'*D*U.
  
  //  2. The factored form of A is used to estimate the condition number
  //     of the matrix A.  If the reciprocal of the condition number is
  //     less than machine precision, steps 3 and 4 are skipped.
  
  //  3. The system of equations A*X = B is solved for X using the
  //     factored form of A.
  
  //  4. Iterative refinement is applied to improve the computed solution
  //     vectors and calculate error bounds and backward error estimates
  //     for them.
  
  //  Arguments
  //  =========
  
  //  FACT    (input) CHARACTER*1
  //          Specifies whether or not the factored form of A has been
  //          supplied on entry.
  //          = 'F':  On entry, DF and EF contain the factored form of A.
  //                  D, E, DF and EF will not be modified.
  //          = 'N':  The matrix A will be copied to DF and EF and
  //                  factored.
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right hand sides, i.e., the number of columns
  //          of the matrices B and X.  NRHS >= 0.
  
  //  D       (input) DOUBLE PRECISION array, dimension (N)
  //          The n diagonal elements of the tridiagonal matrix A.
  
  //  E       (input) DOUBLE PRECISION array, dimension (N-1)
  //          The (n-1) subdiagonal elements of the tridiagonal matrix A.
  
  //  DF      (input or output) DOUBLE PRECISION array, dimension (N)
  //          If FACT = 'F', then DF is an input argument and on entry
  //          contains the n diagonal elements of the diagonal matrix D
  //          from the L*D*L' factorization of A.
  
  //          If FACT = 'N', then DF is an output argument and on exit
  //          contains the n diagonal elements of the diagonal matrix D
  //          from the L*D*L' factorization of A.
  
  //  EF      (input or output) DOUBLE PRECISION array, dimension (N-1)
  //          If FACT = 'F', then EF is an input argument and on entry
  //          contains the (n-1) subdiagonal elements of the unit
  //          bidiagonal factor L from the L*D*L' factorization of A.
  
  //          If FACT = 'N', then EF is an output argument and on exit
  //          contains the (n-1) subdiagonal elements of the unit
  //          bidiagonal factor L from the L*D*L' factorization of A.
  
  //  B       (input) DOUBLE PRECISION array, dimension (LDB,NRHS)
  //          The n-by-nrhs right-hand side matrix B.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  X       (output) DOUBLE PRECISION array, dimension (LDX,NRHS)
  //          If INFO = 0, the n-by-nrhs solution matrix X.
  
  //  LDX     (input) INTEGER
  //          The leading dimension of the array X.  LDX >= max(1,N).
  
  //  RCOND   (output) DOUBLE PRECISION
  //          The estimate of the reciprocal condition number of the matrix
  //          A.  If RCOND is less than the machine precision (in
  //          particular, if RCOND = 0), the matrix is singular to working
  //          precision.  This condition is indicated by a return code of
  //          INFO > 0, and the solution and error bounds are not computed.
  
  //  FERR    (output) DOUBLE PRECISION array, dimension (NRHS)
  //          The estimated forward error bounds for each solution vector
  //          X(j) (the j-th column of the solution matrix X).
  //          If XTRUE is the true solution, FERR(j) bounds the magnitude
  //          of the largest entry in (X(j) - XTRUE) divided by the
  //          magnitude of the largest entry in X(j).  The quality of the
  //          error bound depends on the quality of the estimate of
  //          norm(inv(A)) computed in the code; if the estimate of
  //          norm(inv(A)) is accurate, the error bound is guaranteed.
  
  //  BERR    (output) DOUBLE PRECISION array, dimension (NRHS)
  //          The componentwise relative backward error of each solution
  //          vector X(j) (i.e., the smallest relative change in any
  //          entry of A or B that makes X(j) an exact solution).
  
  //  WORK    (workspace) DOUBLE PRECISION array, dimension (2*N)
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: if INFO = k, and k is
  //               <= N: if INFO = k, the leading minor of order k of A
  //                     is not positive definite, so the factorization
  //                     could not be completed unless k = N, and the
  //                     solution and error bounds could not be computed.
  //               = N+1: RCOND is less than machine precision.  The
  //                     factorization has been completed, but the matrix
  //                     is singular to working precision, and the solution
  //                     and error bounds have not been computed.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  nofact = lsame( fact, 'N' );
  if( !nofact && !lsame( fact, 'F' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( nrhs < 0 ) { 
    info = -3;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -9;
  }
  else if( ldx < max( 1, n ) ) { 
    info = -11;
  }
  if( info != 0 ) { 
    xerbla( "DPTSVX", -info );
    return;
  }
  
  if( nofact ) { 
    
    //        Compute the L*D*L' (or U'*D*U) factorization of A.
    
    dcopy( n, d, 1, df, 1 );
    if( n > 1 ) 
      dcopy( n - 1, e, 1, ef, 1 );
    dpttrf( n, df, ef, info );
    
    //        Return if INFO is non-zero.
    
    if( info != 0 ) { 
      if( info > 0 ) 
        rcond = ZERO;
      return;
    }
  }
  
  //     Compute the norm of the matrix A.
  
  anorm = dlanst( '1', n, d, e );
  
  //     Compute the reciprocal of the condition number of A.
  
  dptcon( n, df, ef, anorm, rcond, work, info );
  
  //     Return if the matrix is singular to working precision.
  
  if( rcond < dlamch( 'E'/* Epsilon */ ) ) { 
    info = n + 1;
    return;
  }
  
  //     Compute the solution vectors X.
  
  dlacpy( 'F'/* Full */, n, nrhs, b, ldb, x, ldx );
  dpttrs( n, nrhs, df, ef, x, ldx, info );
  
  //     Use iterative refinement to improve the computed solutions and
  //     compute error bounds and backward error estimates for them.
  
  dptrfs( n, nrhs, d, e, df, ef, b, ldb, x, ldx, ferr, berr, work, 
   info );
  
  return;
  
  //     End of DPTSVX
  
#undef  X
#undef  B
} // end of function 


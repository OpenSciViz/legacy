/*
 * C++ implementation of lapack routine dtptri
 *
 * $Id: dtptri.cpp,v 1.6 1993/04/06 20:42:46 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:38:55
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dtptri.cpp,v $
 * Revision 1.6  1993/04/06  20:42:46  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.5  1993/03/19  18:41:23  alv
 * now passes chars explicitly, rather than indirection of a string, to shut up SUN warnings
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:17:47  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:09:24  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ONE = 1.0e0;
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dtptri(const char &uplo, const char &diag, const long &n, double ap[], 
 long &info)
{
  int nounit, upper;
  long _do0, _do1, _do2, info_, j, j_, jc, jclast, jj;
  double ajj;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DTPTRI computes the inverse of a real upper or lower triangular
  //  matrix A stored in packed format.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the matrix A is upper or lower triangular.
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  DIAG    (input) CHARACTER*1
  //          Specifies whether or not the matrix A is unit triangular.
  //          = 'N':  Non-unit triangular
  //          = 'U':  Unit triangular
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  AP      (input/output) DOUBLE PRECISION array, dimension (N*(N+1)/2)
  
  //          On entry, the upper or lower triangular matrix A, stored
  //          columnwise in a linear array.  The j-th column of A is stored
  //          in the array AP as follows:
  //          if UPLO = 'U', AP((j-1)*j/2 + i) = A(i,j) for 1<=i<=j;
  //          if UPLO = 'L',
  //             AP((j-1)*(n-j) + j*(j+1)/2 + i-j) = A(i,j) for j<=i<=n.
  //          See below for further details.
  
  //          On exit, the (triangular) inverse of the original matrix, in
  //          the same packed storage format.
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          > 0: if INFO = k, A(k,k) is exactly zero.  The triangular
  //               matrix is singular and its inverse can not be computed.
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  
  //  Further Details
  //  ===============
  
  //  A triangular matrix A can be transferred to packed storage using one
  //  of the following program segments:
  
  //  UPLO = 'U':                      UPLO = 'L':
  
  //        JC = 1                           JC = 1
  //        DO 2 J = 1, N                    DO 2 J = 1, N
  //           DO 1 I = 1, J                    DO 1 I = J, N
  //              AP(JC+I-1) = A(I,J)              AP(JC+I-J) = A(I,J)
  //      1    CONTINUE                    1    CONTINUE
  //           JC = JC + J                      JC = JC + N - J + 1
  //      2 CONTINUE                       2 CONTINUE
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  nounit = lsame( diag, 'N' );
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( !nounit && !lsame( diag, 'U' ) ) { 
    info = -2;
  }
  else if( n < 0 ) { 
    info = -3;
  }
  if( info != 0 ) { 
    xerbla( "DTPTRI", -info );
    return;
  }
  
  //     Check for singularity if non-unit.
  
  if( nounit ) { 
    if( upper ) { 
      jj = 0;
      for( info = 1, info_ = info - 1, _do0 = n; info <= _do0; info++, info_++ ) { 
        jj = jj + info;
        if( ap[jj - 1] == ZERO ) 
          return;
      }
    }
    else { 
      jj = 1;
      for( info = 1, info_ = info - 1, _do1 = n; info <= _do1; info++, info_++ ) { 
        if( ap[jj - 1] == ZERO ) 
          return;
        jj = jj + n - info + 1;
      }
    }
    info = 0;
  }
  
  if( upper ) { 
    
    //        Compute inverse of upper triangular matrix.
    
    jc = 1;
    for( j = 1, j_ = j - 1, _do2 = n; j <= _do2; j++, j_++ ) { 
      if( nounit ) { 
        ap[jc + j_ - 1] = ONE/ap[jc + j_ - 1];
        ajj = -ap[jc + j_ - 1];
      }
      else { 
        ajj = -ONE;
      }
      
      //           Compute elements 1:j-1 of j-th column.
      
      dtpmv( 'U'/* Upper */, 'N'/* No transpose */, diag, j - 
       1, ap, &ap[jc - 1], 1 );
      dscal( j - 1, ajj, &ap[jc - 1], 1 );
      jc = jc + j;
    }
    
  }
  else { 
    
    //        Compute inverse of lower triangular matrix.
    
    jc = n*(n + 1)/2;
    for( j = n, j_ = j - 1; j >= 1; j--, j_-- ) { 
      if( nounit ) { 
        ap[jc - 1] = ONE/ap[jc - 1];
        ajj = -ap[jc - 1];
      }
      else { 
        ajj = -ONE;
      }
      if( j < n ) { 
        
        //              Compute elements j+1:n of j-th column.
        
        dtpmv( 'L'/* Lower */, 'N'/* No transpose */, diag, 
         n - j, &ap[jclast - 1], &ap[jc], 1 );
        dscal( n - j, ajj, &ap[jc], 1 );
      }
      jclast = jc;
      jc = jc - n + j - 2;
    }
  }
  
  return;
  
  //     End of DTPTRI
  
} // end of function 


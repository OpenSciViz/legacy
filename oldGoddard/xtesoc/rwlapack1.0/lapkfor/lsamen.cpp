/*
 * C++ implementation of lapack routine lsamen
 *
 * $Id: lsamen.cpp,v 1.4 1993/04/06 20:42:58 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:27:41
 * FOR_C++ Options SET: alloc do=rt no=p pf=xlapack s=dv str=l - prototypes
 *
 * $Log: lsamen.cpp,v $
 * Revision 1.4  1993/04/06  20:42:58  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.3  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.2  1993/03/05  23:18:04  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:09:44  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

RWLAPKDECL int /*FUNCTION*/ lsamen(const long &n, char *ca, int ca_s, char *cb, 
   int cb_s)
{
  int lsamen_v;
  long _do0, i, i_;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  LSAMEN  tests if the first N letters of CA are the same as the
  //  first N letters of CB, regardless of case.
  //  LSAMEN returns .TRUE. if CA and CB are equivalent except for case
  //  and .FALSE. otherwise.  LSAMEN also returns .FALSE. if LEN( CA )
  //  or LEN( CB ) is less than N.
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The number of characters in CA and CB to be compared.
  
  //  CA      (input) CHARACTER*(*)
  //  CB      (input) CHARACTER*(*)
  //          CA and CB specify two character strings of length at least N.
  //          Only the first N characters of each string will be accessed.
  
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  lsamen_v = FALSE;
  if( (ca_s - 1) < n || (cb_s - 1) < n ) 
    goto L_20;
  
  //     Do for each character in the two strings.
  
  for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
    
    //        Test if the characters are equal using LSAME.
    
    if( !lsame( ca[i - 1], cb[i - 1] ) ) 
      goto L_20;
    
  }
  lsamen_v = TRUE;
  
L_20:
  ;
  return( lsamen_v );
  
  //     End of LSAMEN
  
} // end of function 


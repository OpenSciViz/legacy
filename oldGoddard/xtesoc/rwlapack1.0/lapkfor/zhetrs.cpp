/*
 * C++ implementation of Lapack routine zhetrs
 *
 * $Id: zhetrs.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:47:39
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zhetrs.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const DComplex ONE = DComplex(1.0e0);
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zhetrs(const char &uplo, const long &n, const long &nrhs, 
 DComplex *a, const long &lda, long ipiv[], DComplex *b, const long &ldb, 
 long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
  int upper;
  long _do0, _do1, j, j_, k, kp;
  double s;
  DComplex ak, akm1, akm1k, bk, bkm1, denom;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZHETRS solves a system of linear equations A*X = B with a DComplex
  //  Hermitian matrix A using the factorization A = U*D*U' or A = L*D*L'
  //  computed by ZHETRF.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the details of the factorization are stored
  //          as an upper or lower triangular matrix.
  //          = 'U':  Upper triangular (form is A = U*D*U')
  //          = 'L':  Lower triangular (form is A = L*D*L')
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right hand sides, i.e., the number of columns
  //          of the matrix B.  NRHS >= 0.
  
  //  A       (input) COMPLEX*16 array, dimension (LDA,N)
  //          The block diagonal matrix D and the multipliers used to
  //          obtain the factor U or L as computed by ZHETRF.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  IPIV    (input) INTEGER array, dimension (N)
  //          Details of the interchanges and the block structure of D
  //          as determined by ZHETRF.
  
  //  B       (input/output) COMPLEX*16 array, dimension (LDB,NRHS)
  //          On entry, the right hand side vectors B for the system of
  //          linear equations.
  //          On exit, the solution vectors, X.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  info = 0;
  upper = lsame( uplo, 'U' );
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( nrhs < 0 ) { 
    info = -3;
  }
  else if( lda < max( 1, n ) ) { 
    info = -5;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -8;
  }
  if( info != 0 ) { 
    xerbla( "ZHETRS", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 || nrhs == 0 ) 
    return;
  
  if( upper ) { 
    
    //        Solve A*X = B, where A = U*D*U'.
    
    //        First solve U*D*X = B, overwriting B with X.
    
    //        K is the main loop index, decreasing from N to 1 in steps of
    //        1 or 2, depending on the size of the diagonal blocks.
    
    k = n;
L_10:
    ;
    
    //        If K < 1, exit from loop.
    
    if( k < 1 ) 
      goto L_30;
    
    if( ipiv[k - 1] > 0 ) { 
      
      //           1 x 1 diagonal block
      
      //           Interchange rows K and IPIV(K).
      
      kp = ipiv[k - 1];
      if( kp != k ) 
        zswap( nrhs, &B(0,k - 1), ldb, &B(0,kp - 1), ldb );
      
      //           Multiply by inv(U(K)), where U(K) is the transformation
      //           stored in column K of A.
      
      zgeru( k - 1, nrhs, -(ONE), &A(k - 1,0), 1, &B(0,k - 1), 
       ldb, &B(0,0), ldb );
      
      //           Multiply by the inverse of the diagonal block.
      
      s = real( ONE )/real( A(k - 1,k - 1) );
      zdscal( nrhs, s, &B(0,k - 1), ldb );
      k = k - 1;
    }
    else { 
      
      //           2 x 2 diagonal block
      
      //           Interchange rows K-1 and -IPIV(K).
      
      kp = -ipiv[k - 1];
      if( kp != k - 1 ) 
        zswap( nrhs, &B(0,k - 2), ldb, &B(0,kp - 1), ldb );
      
      //           Multiply by inv(U(K)), where U(K) is the transformation
      //           stored in columns K-1 and K of A.
      
      zgeru( k - 2, nrhs, -(ONE), &A(k - 1,0), 1, &B(0,k - 1), 
       ldb, &B(0,0), ldb );
      zgeru( k - 2, nrhs, -(ONE), &A(k - 2,0), 1, &B(0,k - 2), 
       ldb, &B(0,0), ldb );
      
      //           Multiply by the inverse of the diagonal block.
      
      akm1k = A(k - 1,k - 2);
      akm1 = A(k - 2,k - 2)/akm1k;
      ak = A(k - 1,k - 1)/conj( akm1k );
      denom = akm1*ak - ONE;
      for( j = 1, j_ = j - 1, _do0 = nrhs; j <= _do0; j++, j_++ ) { 
        bkm1 = B(j_,k - 2)/akm1k;
        bk = B(j_,k - 1)/conj( akm1k );
        B(j_,k - 2) = (ak*bkm1 - bk)/denom;
        B(j_,k - 1) = (akm1*bk - bkm1)/denom;
      }
      k = k - 2;
    }
    
    goto L_10;
L_30:
    ;
    
    //        Next solve U'*X = B, overwriting B with X.
    
    //        K is the main loop index, increasing from 1 to N in steps of
    //        1 or 2, depending on the size of the diagonal blocks.
    
    k = 1;
L_40:
    ;
    
    //        If K > N, exit from loop.
    
    if( k > n ) 
      goto L_50;
    
    if( ipiv[k - 1] > 0 ) { 
      
      //           1 x 1 diagonal block
      
      //           Multiply by inv(U'(K)), where U(K) is the transformation
      //           stored in column K of A.
      
      if( k > 1 ) { 
        zlacgv( nrhs, &B(0,k - 1), ldb );
        zgemv( 'C'/*Conjugate transpose*/, k - 1, nrhs, -(ONE), 
         b, ldb, &A(k - 1,0), 1, ONE, &B(0,k - 1), ldb );
        zlacgv( nrhs, &B(0,k - 1), ldb );
      }
      
      //           Interchange rows K and IPIV(K).
      
      kp = ipiv[k - 1];
      if( kp != k ) 
        zswap( nrhs, &B(0,k - 1), ldb, &B(0,kp - 1), ldb );
      k = k + 1;
    }
    else { 
      
      //           2 x 2 diagonal block
      
      //           Multiply by inv(U'(K+1)), where U(K+1) is the transformation
      //           stored in columns K and K+1 of A.
      
      if( k > 1 ) { 
        zlacgv( nrhs, &B(0,k - 1), ldb );
        zgemv( 'C'/*Conjugate transpose*/, k - 1, nrhs, -(ONE), 
         b, ldb, &A(k - 1,0), 1, ONE, &B(0,k - 1), ldb );
        zlacgv( nrhs, &B(0,k - 1), ldb );
        
        zlacgv( nrhs, &B(0,k), ldb );
        zgemv( 'C'/*Conjugate transpose*/, k - 1, nrhs, -(ONE), 
         b, ldb, &A(k,0), 1, ONE, &B(0,k), ldb );
        zlacgv( nrhs, &B(0,k), ldb );
      }
      
      //           Interchange rows K and -IPIV(K).
      
      kp = -ipiv[k - 1];
      if( kp != k ) 
        zswap( nrhs, &B(0,k - 1), ldb, &B(0,kp - 1), ldb );
      k = k + 2;
    }
    
    goto L_40;
L_50:
    ;
    
  }
  else { 
    
    //        Solve A*X = B, where A = L*D*L'.
    
    //        First solve L*D*X = B, overwriting B with X.
    
    //        K is the main loop index, increasing from 1 to N in steps of
    //        1 or 2, depending on the size of the diagonal blocks.
    
    k = 1;
L_60:
    ;
    
    //        If K > N, exit from loop.
    
    if( k > n ) 
      goto L_80;
    
    if( ipiv[k - 1] > 0 ) { 
      
      //           1 x 1 diagonal block
      
      //           Interchange rows K and IPIV(K).
      
      kp = ipiv[k - 1];
      if( kp != k ) 
        zswap( nrhs, &B(0,k - 1), ldb, &B(0,kp - 1), ldb );
      
      //           Multiply by inv(L(K)), where L(K) is the transformation
      //           stored in column K of A.
      
      if( k < n ) 
        zgeru( n - k, nrhs, -(ONE), &A(k - 1,k), 1, &B(0,k - 1), 
         ldb, &B(0,k), ldb );
      
      //           Multiply by the inverse of the diagonal block.
      
      s = real( ONE )/real( A(k - 1,k - 1) );
      zdscal( nrhs, s, &B(0,k - 1), ldb );
      k = k + 1;
    }
    else { 
      
      //           2 x 2 diagonal block
      
      //           Interchange rows K+1 and -IPIV(K).
      
      kp = -ipiv[k - 1];
      if( kp != k + 1 ) 
        zswap( nrhs, &B(0,k), ldb, &B(0,kp - 1), ldb );
      
      //           Multiply by inv(L(K)), where L(K) is the transformation
      //           stored in columns K and K+1 of A.
      
      if( k < n - 1 ) { 
        zgeru( n - k - 1, nrhs, -(ONE), &A(k - 1,k + 1), 1, 
         &B(0,k - 1), ldb, &B(0,k + 1), ldb );
        zgeru( n - k - 1, nrhs, -(ONE), &A(k,k + 1), 1, &B(0,k), 
         ldb, &B(0,k + 1), ldb );
      }
      
      //           Multiply by the inverse of the diagonal block.
      
      akm1k = A(k - 1,k);
      akm1 = A(k - 1,k - 1)/conj( akm1k );
      ak = A(k,k)/akm1k;
      denom = akm1*ak - ONE;
      for( j = 1, j_ = j - 1, _do1 = nrhs; j <= _do1; j++, j_++ ) { 
        bkm1 = B(j_,k - 1)/conj( akm1k );
        bk = B(j_,k)/akm1k;
        B(j_,k - 1) = (ak*bkm1 - bk)/denom;
        B(j_,k) = (akm1*bk - bkm1)/denom;
      }
      k = k + 2;
    }
    
    goto L_60;
L_80:
    ;
    
    //        Next solve L'*X = B, overwriting B with X.
    
    //        K is the main loop index, decreasing from N to 1 in steps of
    //        1 or 2, depending on the size of the diagonal blocks.
    
    k = n;
L_90:
    ;
    
    //        If K < 1, exit from loop.
    
    if( k < 1 ) 
      goto L_100;
    
    if( ipiv[k - 1] > 0 ) { 
      
      //           1 x 1 diagonal block
      
      //           Multiply by inv(L'(K)), where L(K) is the transformation
      //           stored in column K of A.
      
      if( k < n ) { 
        zlacgv( nrhs, &B(0,k - 1), ldb );
        zgemv( 'C'/*Conjugate transpose*/, n - k, nrhs, -(ONE), 
         &B(0,k), ldb, &A(k - 1,k), 1, ONE, &B(0,k - 1), ldb );
        zlacgv( nrhs, &B(0,k - 1), ldb );
      }
      
      //           Interchange rows K and IPIV(K).
      
      kp = ipiv[k - 1];
      if( kp != k ) 
        zswap( nrhs, &B(0,k - 1), ldb, &B(0,kp - 1), ldb );
      k = k - 1;
    }
    else { 
      
      //           2 x 2 diagonal block
      
      //           Multiply by inv(L'(K-1)), where L(K-1) is the transformation
      //           stored in columns K-1 and K of A.
      
      if( k < n ) { 
        zlacgv( nrhs, &B(0,k - 1), ldb );
        zgemv( 'C'/*Conjugate transpose*/, n - k, nrhs, -(ONE), 
         &B(0,k), ldb, &A(k - 1,k), 1, ONE, &B(0,k - 1), ldb );
        zlacgv( nrhs, &B(0,k - 1), ldb );
        
        zlacgv( nrhs, &B(0,k - 2), ldb );
        zgemv( 'C'/*Conjugate transpose*/, n - k, nrhs, -(ONE), 
         &B(0,k), ldb, &A(k - 2,k), 1, ONE, &B(0,k - 2), ldb );
        zlacgv( nrhs, &B(0,k - 2), ldb );
      }
      
      //           Interchange rows K and -IPIV(K).
      
      kp = -ipiv[k - 1];
      if( kp != k ) 
        zswap( nrhs, &B(0,k - 1), ldb, &B(0,kp - 1), ldb );
      k = k - 2;
    }
    
    goto L_90;
L_100:
    ;
  }
  
  return;
  
  //     End of ZHETRS
  
#undef  B
#undef  A
} // end of function 


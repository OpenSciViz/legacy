/*
 * C++ implementation of Lapack routine zungtr
 *
 * $Id: zungtr.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:51:46
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zungtr.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const DComplex ZERO = DComplex(0.0e0);
const DComplex ONE = DComplex(1.0e0);
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zungtr(const char &uplo, const long &n, DComplex *a, const long &lda, 
 DComplex tau[], DComplex work[], const long &lwork, long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  int upper;
  long _do0, _do1, _do2, _do3, _do4, i, i_, iinfo, j, j_;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZUNGTR generates a DComplex unitary matrix Q which is defined as the
  //  product of n-1 elementary reflectors of order n, as returned by
  //  ZHETRD:
  
  //  if UPLO = 'U', Q = H(n-1) . . . H(2) H(1),
  
  //  if UPLO = 'L', Q = H(1) H(2) . . . H(n-1).
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangle of the array A
  //          holds details of the elementary reflectors, as returned by
  //          ZHETRD:
  //          = 'U': Upper triangle;
  //          = 'L': Lower triangle.
  
  //  N       (input) INTEGER
  //          The order of the matrix Q. N >= 0.
  
  //  A       (input/output) COMPLEX*16 array, dimension (LDA,N)
  //          On entry, the vectors which define the elementary reflectors,
  //          as returned by ZHETRD.
  //          On exit, the n by n unitary matrix Q.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A. LDA >= N.
  
  //  TAU     (input) COMPLEX*16 array, dimension (N-1)
  //          TAU(i) must contain the scalar factor of the elementary
  //          reflector H(i), as returned by ZHETRD.
  
  //  WORK    (workspace) COMPLEX*16 array, dimension (LWORK)
  //          On exit, if INFO = 0, WORK(1) returns the minimum value of
  //          LWORK required to use the optimal blocksize.
  
  //  LWORK   (input) INTEGER
  //          The dimension of the array WORK. LWORK >= N-1.
  //          For optimum performance LWORK should be at least (N-1)*NB,
  //          where NB is the optimal blocksize.
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input arguments
  
  info = 0;
  upper = lsame( uplo, 'U' );
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( lda < max( 1, n ) ) { 
    info = -4;
  }
  else if( lwork < max( 1, n - 1 ) ) { 
    info = -7;
  }
  if( info != 0 ) { 
    xerbla( "ZUNGTR", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) { 
    work[0] = DComplex((double)1);
    return;
  }
  
  if( upper ) { 
    
    //        Q was determined by a call to ZHETRD with UPLO = 'U'
    
    //        Shift the vectors which define the elementary reflectors one
    //        column to the left, and set the last row and column of Q to
    //        those of the unit matrix
    
    for( j = 1, j_ = j - 1, _do0 = n - 1; j <= _do0; j++, j_++ ) { 
      for( i = 1, i_ = i - 1, _do1 = j - 1; i <= _do1; i++, i_++ ) { 
        A(j_,i_) = A(j_ + 1,i_);
      }
      A(j_,n - 1) = ZERO;
    }
    for( i = 1, i_ = i - 1, _do2 = n - 1; i <= _do2; i++, i_++ ) { 
      A(n - 1,i_) = ZERO;
    }
    A(n - 1,n - 1) = ONE;
    
    //        Generate Q(1:n-1,1:n-1)
    
    zungql( n - 1, n - 1, n - 1, a, lda, tau, work, lwork, iinfo );
    
  }
  else { 
    
    //        Q was determined by a call to ZHETRD with UPLO = 'L'.
    
    //        Shift the vectors which define the elementary reflectors one
    //        column to the right, and set the first row and column of Q to
    //        those of the unit matrix
    
    for( j = n, j_ = j - 1; j >= 2; j--, j_-- ) { 
      A(j_,0) = ZERO;
      for( i = j + 1, i_ = i - 1, _do3 = n; i <= _do3; i++, i_++ ) { 
        A(j_,i_) = A(j_ - 1,i_);
      }
    }
    A(0,0) = ONE;
    for( i = 2, i_ = i - 1, _do4 = n; i <= _do4; i++, i_++ ) { 
      A(0,i_) = ZERO;
    }
    if( n > 1 ) { 
      
      //           Generate Q(2:n,2:n)
      
      zungqr( n - 1, n - 1, n - 1, &A(1,1), lda, tau, work, 
       lwork, iinfo );
    }
  }
  return;
  
  //     End of ZUNGTR
  
#undef  A
} // end of function 


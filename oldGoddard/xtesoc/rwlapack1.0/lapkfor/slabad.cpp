/*
 * C++ implementation of Lapack routine slabad
 *
 * $Id: slabad.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:59:22
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: slabad.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

RWLAPKDECL void /*FUNCTION*/ slabad(float &small, float &large)
{

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SLABAD takes as input the values computed by SLAMCH for underflow and
  //  overflow, and returns the square root of each of these values if the
  //  log of LARGE is sufficiently large.  This subroutine is intended to
  //  identify machines with a large exponent range, such as the Crays, and
  //  redefine the underflow and overflow limits to be the square roots of
  //  the values computed by SLAMCH.  This subroutine is needed because
  //  SLAMCH does not compensate for poor arithmetic in the upper half of
  //  the exponent range, as is found on a Cray.
  
  //  Arguments
  //  =========
  
  //  SMALL   (input/output) REAL
  //          On entry, the underflow threshold as computed by SLAMCH.
  //          On exit, if LOG10(LARGE) is sufficiently large, the square
  //          root of SMALL, otherwise unchanged.
  
  //  LARGE   (input/output) REAL
  //          On entry, the overflow threshold as computed by SLAMCH.
  //          On exit, if LOG10(LARGE) is sufficiently large, the square
  //          root of LARGE, otherwise unchanged.
  
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     If it looks like we're on a Cray, take the square root of
  //     SMALL and LARGE to avoid overflow and underflow problems.
  
  if( log10( large ) > 2000. ) { 
    small = sqrt( small );
    large = sqrt( large );
  }
  
  return;
  
  //     End of SLABAD
  
} // end of function 


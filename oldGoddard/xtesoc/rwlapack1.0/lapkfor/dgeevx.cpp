/*
 * C++ implementation of lapack routine dgeevx
 *
 * $Id: dgeevx.cpp,v 1.5 1993/04/06 20:40:28 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:34:02
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dgeevx.cpp,v $
 * Revision 1.5  1993/04/06  20:40:28  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:14:36  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:06:34  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dgeevx(const char &balanc, const char &jobvl, const char &jobvr, const char &sense, 
 long &n, double *a, long &lda, double wr[], double wi[], 
 double *vl, long &ldvl, double *vr, long &ldvr, long &ilo, 
 long &ihi, double scale[], double &abnrm, double rconde[], double rcondv[], 
 double work[], long &lwork, long iwork[], long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define VL(I_,J_) (*(vl+(I_)*(ldvl)+(J_)))
#define VR(I_,J_) (*(vr+(I_)*(ldvr)+(J_)))
  int scalea, select[1], wantvl, wantvr, wntsnb, wntsne, wntsnn, 
   wntsnv;
  char job, side;
  long _do0, _do1, _do2, _do3, hswork, i, i_, icond, ierr, itau, 
   iwrk, k, k_, maxb, maxwrk, minwrk, nout;
  double anrm, bignum, cs, cscale, dum[1], eps, r, scl, smlnum, 
   sn;

  
  //  -- LAPACK driver routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  For an N by N real nonsymmetric matrix A, compute
  
  //     the eigenvalues (WR and WI)
  //     the left and/or right eigenvectors (VL and VR)
  //     a balancing transformation to improve the conditioning of the
  //        eigenvalues and eigenvectors (ILO, IHI, SCALE, and ABNRM)
  //     reciprocal condition numbers for the eigenvalues (RCONDE)
  //     reciprocal condition numbers for the right eigenvectors (RCONDV)
  
  //  The last four outputs are optional:
  
  //     JOBVL determines whether to compute left eigenvectors VL
  //     JOBVR determines whether to compute right eigenvectors VR
  //     BALANC determines how to balance the matrix (output in ILO, IHI,
  //        SCALE, and ABNRM)
  //     SENSE determines whether to compute reciprocal condition numbers
  //        RCONDE and RCONDV
  
  //  Balancing a matrix means permuting the rows and columns to make it
  //  more nearly upper triangular, and computing a diagonal similarity
  //  D * A * D**(-1), D a diagonal matrix, to make its rows and columns
  //  closer in norm and the condition numbers of its eigenvalues and
  //  eigenvectors smaller. These two steps, permuting and diagonal
  //  scaling, may be applied independently as determined by BALANC.
  //  The one-norm of the balanced matrix (the maximum of the sum of
  //  absolute values of entries of any column) is returned in ABNRM.
  
  //  The reciprocal condition numbers correspond to the balanced matrix.
  //  Permuting rows and columns will not change the condition numbers
  //  (in exact arithmetic) but diagonal scaling will.
  
  //  The reciprocal of the condition number of an eigenvalue lambda
  //  is defined as
  
  //          RCONDE(lambda) = |v'*u| / norm(u) * norm(v)
  
  //  where u and v are the right and left eigenvectors of T
  //  corresponding to lambda (v' denotes the conjugate transpose
  //  of v), and norm(u) denotes the Euclidean norm.
  //  These reciprocal condition numbers always lie between zero
  //  (very badly conditioned) and one (very well conditioned).
  //  These reciprocal condition numbers are returned in the array RCONDE.
  //  An approximate error bound for a computed eigenvalue
  //  WR(i)+sqrt(-1)*WI(i) is given by
  
  //                      EPS * ABRNM / RCONDE(i)
  
  //  where EPS = DLAMCH( 'P' ) is the machine precision.
  
  //  The reciprocal condition number of the right eigenvector u
  //  corresponding to lambda is defined as follows. Suppose
  
  //              T = [ lambda  c  ]
  //                  [   0    T22 ]
  
  //  Then the reciprocal condition number is
  
  //          RCONDV(lambda,T22) = sigma-min(T22 - lambda*I)
  
  //  where sigma-min denotes the smallest singular value.
  //  We approximate the smallest singular value by the reciprocal of
  //  an estimate of the one-norm of the inverse of T22 - lambda*I.
  //  When RCONDV is small, small changes in the matrix can cause large
  //  changes in u. These reciprocal condition numbers are returned in
  //  the array RCONDV.
  //  If BALANC = 'N' or 'P', an approximate error bound for a computed
  //  right eigenvector VR(i) is given by
  
  //                      EPS * ABRNM / RCONDV(i)
  
  //  where EPS = DLAMCH( 'P' ) is the machine precision.  When
  //  BALANC = 'S' or 'B', the interpretation of RCONDV(i) is more DComplex.
  
  //  See section 4.9 of the LAPACK Users' Guide for a detailed discussion
  //  of these condition numbers.
  
  //  Arguments
  //  =========
  
  //  BALANC  (input) CHARACTER*1
  //          Indicates how the input matrix should be diagonally scaled
  //          and/or permuted to improve the conditioning of its
  //          eigenvalues.
  //          = 'N': Do not diagonally scale or permute.
  //          = 'P': Perform permutations to make the matrix more nearly
  //                 upper triangular. Do not diagonally scale.
  //          = 'S': Diagonally scale the matrix, ie. replace A by
  //                 D*A*D**(-1), where D is a diagonal matrix chosen
  //                 to make the rows and columns of A more equal in
  //                 norm. Do not permute.
  //          = 'B': Both diagonally scale and permute A.
  
  //          Computed reciprocal condition numbers will be for the matrix
  //          after balancing and/or permuting. Permuting does not change
  //          condition numbers (in exact arithmetic), but balancing does.
  
  //  JOBVL   (input) CHARACTER*1
  //          Specifies whether or not to compute left eigenvectors of A.
  //          = 'N': left eigenvectors are not computed.
  //          = 'V': left eigenvectors are computed.
  
  //          If SENSE = 'E' or 'B', JOBVL must = 'V'.
  
  //  JOBVR   (input) CHARACTER*1
  //          Specifies whether or not to compute right eigenvectors of A.
  //          = 'N': right eigenvectors are not computed.
  //          = 'V': right eigenvectors are computed.
  
  //          If SENSE = 'E' or 'B', JOBVR must = 'V'.
  
  //  SENSE   (input) CHARACTER*1
  //          Determines which reciprocal condition numbers are computed.
  //          = 'N': None are computed.
  //          = 'E': Computed for eigenvalues only.
  //          = 'V': Computed for right eigenvectors only.
  //          = 'B': Computed for eigenvalues and right eigenvectors.
  
  //          If SENSE = 'E' or 'B', both left and right eigenvectors
  //          must also be computed (JOBVL = 'V' and JOBVR = 'V').
  
  //  N       (input) INTEGER
  //          The number of rows and columns of the input matrix A. N >= 0.
  
  //  A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)
  //          On entry, A is the matrix whose eigenvalues and eigenvectors
  //          are desired.
  //          On exit, A has been overwritten.  If JOBVL = 'V' or
  //          JOBVR = 'V', the upper Hessenberg part of A has been
  //          overwritten with the Schur form of the balanced version of A.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  WR      (output) DOUBLE PRECISION array, dimension (N)
  //  WI      (output) DOUBLE PRECISION array, dimension (N)
  //          On exit, WR and WI contain the real and imaginary parts,
  //          respectively, of the computed eigenvalues. The eigenvalues
  //          will not be in any particular order, except that DComplex
  //          conjugate pairs of eigenvalues will appear consecutively
  //          with the eigenvalue having the positive imaginary part
  //          first.
  
  //  VL      (output) DOUBLE PRECISION array, dimension (LDVL,N)
  //          The left eigenvectors will be stored one after another in
  //          the columns of VL, in the same order (but not necessarily
  //          the same position) as their eigenvalues. An eigenvector
  //          corresponding to a real eigenvalue will take up one column.
  //          An eigenvector pair corresponding to a DComplex conjugate
  //          pair of eigenvalues will take up two columns: the first
  //          column will hold the real part, the second will hold the
  //          imaginary part of the eigenvector corresponding to the
  //          eigenvalue with positive imaginary part.
  //          The eigenvectors will be normalized to have Euclidean
  //          norm equal to 1 and largest component real.
  
  //          If WI(j) = 0 so WR(j) is a real eigenvalue, then
  //             VL(j)' * A = WR(j) * VL(j)'
  //          where ' means transpose.  If WR(j)+i*WI(j) and
  //          WR(j+1)+i*WI(j+1) are a conjugate pair of eigenvalues, then
  //             (VL(j) + i*VL(j+1))' * A =
  //                (WR(j) + i*WI(j)) * (VL(j) + i*VL(j+1))'
  //          and
  //             (VL(j) - i*VL(j+1))' * A =
  //                (WR(j+1) + i*WI(j+1)) * (VL(j) - i*VL(j+1))' .
  //          Left eigenvectors of A are the same as the right
  //          eigenvectors of transpose(A).
  
  //          If JOBVL = 'N', VL is not referenced.
  
  //  LDVL    (input) INTEGER
  //          The leading dimension of the array VL.  LDVL >= 1, and if
  //          JOBVL = 'V', LDVL >= N.
  
  //  VR      (output) DOUBLE PRECISION array, dimension (LDVR,N)
  //          The right eigenvectors will be stored one after another in
  //          the columns of VR, in the same order (but not necessarily
  //          the same position) as their eigenvalues. An eigenvector
  //          corresponding to a real eigenvalue will take up one column.
  //          An eigenvector pair corresponding to a DComplex conjugate
  //          pair of eigenvalues will take up two columns: the first
  //          column will hold the real part, the second will hold the
  //          imaginary part of the eigenvector corresponding to the
  //          eigenvalue with positive imaginary part.
  //          The eigenvectors will be normalized to have Euclidean
  //          norm equal to 1 and largest component real.
  
  //          If WI(j) = 0 so WR(j) is a real eigenvalue, then
  //             A * VR(j) = WR(j) * VR(j) .
  //          If WR(j)+i*WI(j) and WR(j+1)+i*WI(j+1) are a conjugate pair
  //          of eigenvalues, then
  //             A * (VR(j) + i*VR(j+1)) =
  //                (WR(j) + i*WI(j)) * (VR(j) + i*VR(j+1))
  //          and
  //             A * (VR(j) - i*VR(j+1)) =
  //                (WR(j+1) + i*WI(j+1)) * (VR(j) - i*VR(j+1)) .
  
  //          If JOBVR = 'N', VR is not referenced.
  
  //  LDVR    (input) INTEGER
  //          The leading dimension of the array VR.  LDVR >= 1, and if
  //          JOBVR = 'V', LDVR >= N.
  
  //  ILO,IHI (output) INTEGER
  //          On exit, ILO, IHI and SCALE describe how A was balanced.
  //          The balanced A(i,j) is equal to zero if I is greater than
  //          J and J = 1,...,ILO-1 or I = IHI+1,...,N.
  
  //  SCALE   (output) DOUBLE PRECISION array, dimension (N)
  //          On exit, SCALE contains information determining the
  //          permutations and diagonal scaling factors used in balancing.
  //          Suppose that the principal submatrix in rows ILO through
  //          IHI has been balanced, that P(J) denotes the index inter-
  //          changed with J during the permutation step, and that the
  //          elements of the diagonal matrix used in diagonal scaling
  //          are denoted by D(I,J).  Then
  //          SCALE(J) = P(J),    for J = 1,...,ILO-1
  //                   = D(J,J),      J = ILO,...,IHI
  //                   = P(J)         J = IHI+1,...,N.
  //          the order in which the interchanges are made is N to IHI+1,
  //          then 1 to ILO-1.
  
  //  ABNRM   (output) DOUBLE PRECISION
  //          On exit, the one-norm of the balanced matrix (the maximum
  //          of the sum of absolute values of entries of any column)
  //          is returned in ABNRM.
  
  //  RCONDE  (output) DOUBLE PRECISION array, dimension (N)
  //          RCONDE(i) is the reciprocal condition number of eigenvalue
  //          WR(i) + sqrt(-1)*WI(i).
  
  //  RCONDV  (output) DOUBLE PRECISION array, dimension (N)
  //          RCONDV(i) is the reciprocal condition number of the i-th
  //          right eigenvector in VR.
  
  //  WORK    (workspace/output) DOUBLE PRECISION array, dimension (LWORK)
  //          On exit, WORK(1) contains the optimal workspace size LWORK
  //          for high performance.
  
  //  LWORK   (input) INTEGER
  //          The dimension of the array WORK.   If SENSE = 'N' or 'E',
  //          LWORK >= max(1,2*N), and if JOBVL = 'V' or JOBVR = 'V',
  //          LWORK >= 3*N.  If SENSE = 'V' or 'B', LWORK >= N*(N+6).
  //          The optimum value of LWORK for high performance is returned
  //          in WORK(1).
  
  //  IWORK   (workspace) INTEGER array, dimension (2*N-2)
  //          If SENSE = 'N' or 'E', not referenced.
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument had an illegal value.
  //          > 0: the QR algorithm failed to compute all the eigenvalues;
  //               if INFO = i, elements 1:ILO-1 and i+1:N of WR and WI
  //               contain eigenvalues which have converged, and the
  //               eigenvectors are not computed.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Local Arrays ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input arguments
  
  info = 0;
  wantvl = lsame( jobvl, 'V' );
  wantvr = lsame( jobvr, 'V' );
  wntsnn = lsame( sense, 'N' );
  wntsne = lsame( sense, 'E' );
  wntsnv = lsame( sense, 'V' );
  wntsnb = lsame( sense, 'B' );
  if( !(((lsame( balanc, 'N' ) || lsame( balanc, 'S' )) || lsame( balanc, 
   'P' )) || lsame( balanc, 'B' )) ) { 
    info = -1;
  }
  else if( (!wantvl) && (!lsame( jobvl, 'N' )) ) { 
    info = -2;
  }
  else if( (!wantvr) && (!lsame( jobvr, 'N' )) ) { 
    info = -3;
  }
  else if( !(((wntsnn || wntsne) || wntsnb) || wntsnv) || ((wntsne || 
   wntsnb) && !(wantvl && wantvr)) ) { 
    info = -4;
  }
  else if( n < 0 ) { 
    info = -5;
  }
  else if( lda < max( 1, n ) ) { 
    info = -7;
  }
  else if( ldvl < 1 || (wantvl && ldvl < n) ) { 
    info = -11;
  }
  else if( ldvr < 1 || (wantvr && ldvr < n) ) { 
    info = -13;
  }
  
  //     Compute workspace
  //      (Note: Comments in the code beginning "Workspace:" describe the
  //       minimal amount of workspace needed at that point in the code,
  //       as well as the preferred amount for good performance.
  //       NB refers to the optimal block size for the immediately
  //       following subroutine, as returned by ILAENV.
  //       HSWORK refers to the workspace preferred by DHSEQR, as
  //       calculated below. HSWORK is computed assuming ILO=1 and IHI=N,
  //       the worst case.)
  
  minwrk = 1;
  if( info == 0 && lwork >= 1 ) { 
    maxwrk = n + n*ilaenv( 1, "DGEHRD", " ", n, 1, n, 0 );
    if( (!wantvl) && (!wantvr) ) { 
      minwrk = max( 1, 2*n );
      if( !wntsnn ) 
        minwrk = max( minwrk, n*n + 6*n );
      maxb = max( ilaenv( 8, "DHSEQR", "SN", n, 1, n, -1 ), 
       2 );
      if( wntsnn ) { 
        k = vmin( maxb, n, max( 2, ilaenv( 4, "DHSEQR", "EN"
         , n, 1, n, -1 ) ), IEND );
      }
      else { 
        k = vmin( maxb, n, max( 2, ilaenv( 4, "DHSEQR", "SN"
         , n, 1, n, -1 ) ), IEND );
      }
      hswork = max( k*(k + 2), 2*n );
      maxwrk = vmax( maxwrk, 1, hswork, IEND );
      if( !wntsnn ) 
        maxwrk = max( maxwrk, n*n + 6*n );
    }
    else { 
      minwrk = max( 1, 3*n );
      if( (!wntsnn) && (!wntsne) ) 
        minwrk = max( minwrk, n*n + 6*n );
      maxb = max( ilaenv( 8, "DHSEQR", "SN", n, 1, n, -1 ), 
       2 );
      k = vmin( maxb, n, max( 2, ilaenv( 4, "DHSEQR", "EN", 
       n, 1, n, -1 ) ), IEND );
      hswork = max( k*(k + 2), 2*n );
      maxwrk = vmax( maxwrk, 1, hswork, IEND );
      maxwrk = max( maxwrk, n + (n - 1)*ilaenv( 1, "DORGHR", 
       " ", n, 1, n, -1 ) );
      if( (!wntsnn) && (!wntsne) ) 
        maxwrk = max( maxwrk, n*n + 6*n );
      maxwrk = vmax( maxwrk, 3*n, 1, IEND );
    }
    work[0] = maxwrk;
  }
  if( lwork < minwrk ) { 
    info = -21;
  }
  if( info != 0 ) { 
    xerbla( "DGEEVX", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  //     Get machine constants
  
  eps = dlamch( 'P' );
  smlnum = dlamch( 'S' );
  bignum = ONE/smlnum;
  dlabad( smlnum, bignum );
  smlnum = sqrt( smlnum )/eps;
  bignum = ONE/smlnum;
  
  //     Scale A if max entry outside range [SMLNUM,BIGNUM]
  
  icond = 0;
  anrm = dlange( 'M', n, n, a, lda, dum );
  scalea = FALSE;
  if( anrm > ZERO && anrm < smlnum ) { 
    scalea = TRUE;
    cscale = smlnum;
  }
  else if( anrm > bignum ) { 
    scalea = TRUE;
    cscale = bignum;
  }
  if( scalea ) 
    dlascl( 'G', 0, 0, anrm, cscale, n, n, a, lda, ierr );
  
  //     Balance the matrix and compute ABNRM
  
  dgebal( balanc, n, a, lda, ilo, ihi, scale, ierr );
  abnrm = dlange( '1', n, n, a, lda, dum );
  if( scalea ) { 
    dum[0] = abnrm;
    dlascl( 'G', 0, 0, cscale, anrm, 1, 1, dum, 1, ierr );
    abnrm = dum[0];
  }
  
  //     Reduce to upper Hessenberg form
  //     (Workspace: need 2*N, prefer N+N*NB)
  
  itau = 1;
  iwrk = itau + n;
  dgehrd( n, ilo, ihi, a, lda, &work[itau - 1], &work[iwrk - 1], 
   lwork - iwrk + 1, ierr );
  
  if( wantvl ) { 
    
    //        Want left eigenvectors
    //        Copy Householder vectors to VL
    
    side = 'L';
    dlacpy( 'L', n, n, a, lda, vl, ldvl );
    
    //        Generate orthogonal matrix in VL
    //        (Workspace: need 2*N-1, prefer N+(N-1)*NB)
    
    dorghr( n, ilo, ihi, vl, ldvl, &work[itau - 1], &work[iwrk - 1], 
     lwork - iwrk + 1, ierr );
    
    //        Perform QR iteration, accumulating Schur vectors in VL
    //        (Workspace: need 1, prefer HSWORK (see comments) )
    
    iwrk = itau;
    dhseqr( 'S', 'V', n, ilo, ihi, a, lda, wr, wi, vl, ldvl, &work[iwrk - 1], 
     lwork - iwrk + 1, info );
    
    if( wantvr ) { 
      
      //           Want left and right eigenvectors
      //           Copy Schur vectors to VR
      
      side = 'B';
      dlacpy( 'F', n, n, vl, ldvl, vr, ldvr );
    }
    
  }
  else if( wantvr ) { 
    
    //        Want right eigenvectors
    //        Copy Householder vectors to VR
    
    side = 'R';
    dlacpy( 'L', n, n, a, lda, vr, ldvr );
    
    //        Generate orthogonal matrix in VR
    //        (Workspace: need 2*N-1, prefer N+(N-1)*NB)
    
    dorghr( n, ilo, ihi, vr, ldvr, &work[itau - 1], &work[iwrk - 1], 
     lwork - iwrk + 1, ierr );
    
    //        Perform QR iteration, accumulating Schur vectors in VR
    //        (Workspace: need 1, prefer HSWORK (see comments) )
    
    iwrk = itau;
    dhseqr( 'S', 'V', n, ilo, ihi, a, lda, wr, wi, vr, ldvr, &work[iwrk - 1], 
     lwork - iwrk + 1, info );
    
  }
  else { 
    
    //        Compute eigenvalues only
    //        If condition numbers desired, compute Schur form
    
    if( wntsnn ) { 
      job = 'E';
    }
    else { 
      job = 'S';
    }
    
    //        (Workspace: need 1, prefer HSWORK (see comments) )
    
    iwrk = itau;
    dhseqr( job, 'N', n, ilo, ihi, a, lda, wr, wi, vr, ldvr, &work[iwrk - 1], 
     lwork - iwrk + 1, info );
  }
  
  //     If INFO > 0 from DHSEQR, then quit
  
  if( info > 0 ) 
    goto L_50;
  
  if( wantvl || wantvr ) { 
    
    //        Compute left and/or right eigenvectors
    //        (Workspace: need 3*N)
    
    dtrevc( side, 'O', select, n, a, lda, vl, ldvl, vr, ldvr, 
     n, nout, &work[iwrk - 1], ierr );
  }
  
  //     Compute condition numbers if desired
  //     (Workspace: need N*N+6*N unless SENSE = 'E')
  
  if( !wntsnn ) { 
    dtrsna( sense, 'A', select, n, a, lda, vl, ldvl, vr, ldvr, 
     rconde, rcondv, n, nout, &work[iwrk - 1], n, iwork, icond );
  }
  
  if( wantvl ) { 
    
    //        Undo balancing of left eigenvectors
    
    dgebak( balanc, 'L', n, ilo, ihi, scale, n, vl, ldvl, ierr );
    
    //        Normalize left eigenvectors and make largest component real
    
    for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
      if( wi[i_] == ZERO ) { 
        scl = ONE/dnrm2( n, &VL(i_,0), 1 );
        dscal( n, scl, &VL(i_,0), 1 );
      }
      else if( wi[i_] > ZERO ) { 
        scl = ONE/dlapy2( dnrm2( n, &VL(i_,0), 1 ), dnrm2( n, 
         &VL(i_ + 1,0), 1 ) );
        dscal( n, scl, &VL(i_,0), 1 );
        dscal( n, scl, &VL(i_ + 1,0), 1 );
        for( k = 1, k_ = k - 1, _do1 = n; k <= _do1; k++, k_++ ) { 
          work[k_] = pow(VL(i_,k_), 2) + pow(VL(i_ + 1,k_), 2);
        }
        k = idamax( n, work, 1 );
        dlartg( VL(i_,k - 1), VL(i_ + 1,k - 1), cs, sn, r );
        drot( n, &VL(i_,0), 1, &VL(i_ + 1,0), 1, cs, sn );
        VL(i_ + 1,k - 1) = ZERO;
      }
    }
  }
  
  if( wantvr ) { 
    
    //        Undo balancing of right eigenvectors
    
    dgebak( balanc, 'R', n, ilo, ihi, scale, n, vr, ldvr, ierr );
    
    //        Normalize right eigenvectors and make largest component real
    
    for( i = 1, i_ = i - 1, _do2 = n; i <= _do2; i++, i_++ ) { 
      if( wi[i_] == ZERO ) { 
        scl = ONE/dnrm2( n, &VR(i_,0), 1 );
        dscal( n, scl, &VR(i_,0), 1 );
      }
      else if( wi[i_] > ZERO ) { 
        scl = ONE/dlapy2( dnrm2( n, &VR(i_,0), 1 ), dnrm2( n, 
         &VR(i_ + 1,0), 1 ) );
        dscal( n, scl, &VR(i_,0), 1 );
        dscal( n, scl, &VR(i_ + 1,0), 1 );
        for( k = 1, k_ = k - 1, _do3 = n; k <= _do3; k++, k_++ ) { 
          work[k_] = pow(VR(i_,k_), 2) + pow(VR(i_ + 1,k_), 2);
        }
        k = idamax( n, work, 1 );
        dlartg( VR(i_,k - 1), VR(i_ + 1,k - 1), cs, sn, r );
        drot( n, &VR(i_,0), 1, &VR(i_ + 1,0), 1, cs, sn );
        VR(i_ + 1,k - 1) = ZERO;
      }
    }
  }
  
  //     Undo scaling if necessary
  
L_50:
  ;
  if( scalea ) { 
    dlascl( 'G', 0, 0, cscale, anrm, n - info, 1, &wr[info], max( n - 
     info, 1 ), ierr );
    dlascl( 'G', 0, 0, cscale, anrm, n - info, 1, &wi[info], max( n - 
     info, 1 ), ierr );
    if( info == 0 ) { 
      if( (wntsnv || wntsnb) && icond == 0 ) 
        dlascl( 'G', 0, 0, cscale, anrm, n, 1, rcondv, n, 
         ierr );
    }
    else { 
      dlascl( 'G', 0, 0, cscale, anrm, ilo - 1, 1, wr, n, ierr );
      dlascl( 'G', 0, 0, cscale, anrm, ilo - 1, 1, wi, n, ierr );
    }
  }
  
  work[0] = maxwrk;
  return;
  
  //     End of DGEEVX
  
#undef  VR
#undef  VL
#undef  A
} // end of function 


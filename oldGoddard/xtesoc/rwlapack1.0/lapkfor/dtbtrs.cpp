/*
 * C++ implementation of lapack routine dtbtrs
 *
 * $Id: dtbtrs.cpp,v 1.5 1993/04/06 20:42:44 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:38:51
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dtbtrs.cpp,v $
 * Revision 1.5  1993/04/06  20:42:44  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:17:40  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:09:22  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dtbtrs(const char &uplo, const char &trans, const char &diag, const long &n, 
 const long &kd, const long &nrhs, double *ab, const long &ldab, double *b, 
 const long &ldb, long &info)
{
#define AB(I_,J_) (*(ab+(I_)*(ldab)+(J_)))
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
  int nounit, upper;
  long _do0, _do1, _do2, info_, j, j_;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DTBTRS solves a triangular system of the form
  
  //     A * X = B  or  A' * X = B,
  
  //  where A is a triangular band matrix of order N, A' is the transpose
  //  of A, and B is an N by NRHS matrix.  A check is made to verify that A
  //  is nonsingular.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the matrix A is upper or lower triangular.
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  TRANS   (input) CHARACTER*1
  //          Specifies the operation applied to A.
  //          = 'N':  Solve  A * X = B  (No transpose)
  //          = 'T':  Solve  A'* X = B  (Transpose)
  //          = 'C':  Solve  A'* X = B  (Conjugate transpose = Transpose)
  
  //  DIAG    (input) CHARACTER*1
  //          Specifies whether or not the matrix A is unit triangular.
  //          = 'N':  Non-unit triangular
  //          = 'U':  Unit triangular
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  KD      (input) INTEGER
  //          The number of superdiagonals or subdiagonals of the
  //          triangular band matrix A.  KD >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right hand sides, i.e., the number of columns
  //          of the matrix B.  NRHS >= 0.
  
  //  AB      (input) DOUBLE PRECISION array, dimension (LDAB,N)
  //          The upper or lower triangular band matrix A, stored in the
  //          first kd+1 rows of AB.  The j-th column of A is stored
  //          in the j-th column of the array AB as follows:
  //          if UPLO = 'U', AB(kd+1+i-j,j) = A(i,j) for max(1,j-kd)<=i<=j;
  //          if UPLO = 'L', AB(1+i-j,j)    = A(i,j) for j<=i<=min(n,j+kd).
  //          If DIAG = 'U', the diagonal elements of A are not referenced
  //          and are assumed to be 1.
  
  //  LDAB    (input) INTEGER
  //          The leading dimension of the array AB.  LDAB >= KD+1.
  
  //  B       (input/output) DOUBLE PRECISION array, dimension (LDB,NRHS)
  //          On entry, the right hand side vectors B for the system of
  //          linear equations.
  //          On exit, if INFO = 0, the solution vectors X.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: if INFO = k, the k-th diagonal element of A is zero,
  //               indicating that the matrix is singular and the solutions
  //               X have not been computed.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  nounit = lsame( diag, 'N' );
  upper = lsame( uplo, 'U' );
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( (!lsame( trans, 'N' ) && !lsame( trans, 'T' )) && !lsame( trans, 
   'C' ) ) { 
    info = -2;
  }
  else if( !nounit && !lsame( diag, 'U' ) ) { 
    info = -3;
  }
  else if( n < 0 ) { 
    info = -4;
  }
  else if( kd < 0 ) { 
    info = -5;
  }
  else if( nrhs < 0 ) { 
    info = -6;
  }
  else if( ldab < kd + 1 ) { 
    info = -8;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -10;
  }
  if( info != 0 ) { 
    xerbla( "DTBTRS", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  //     Check for singularity.
  
  if( nounit ) { 
    if( upper ) { 
      for( info = 1, info_ = info - 1, _do0 = n; info <= _do0; info++, info_++ ) { 
        if( AB(info_,kd) == ZERO ) 
          return;
      }
    }
    else { 
      for( info = 1, info_ = info - 1, _do1 = n; info <= _do1; info++, info_++ ) { 
        if( AB(info_,0) == ZERO ) 
          return;
      }
    }
  }
  info = 0;
  
  //     Solve A * X = B  or  A' * X = B.
  
  for( j = 1, j_ = j - 1, _do2 = nrhs; j <= _do2; j++, j_++ ) { 
    dtbsv( uplo, trans, diag, n, kd, ab, ldab, &B(j_,0), 1 );
  }
  
  return;
  
  //     End of DTBTRS
  
#undef  B
#undef  AB
} // end of function 


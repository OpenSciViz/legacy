/*
 * C++ implementation of Lapack routine slar2v
 *
 * $Id: slar2v.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:00:19
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: slar2v.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

RWLAPKDECL void /*FUNCTION*/ slar2v(const long &n, float x[], float y[], float z[], 
   const long &incx, float c[], float s[], const long &incc)
{
  long _do0, i, i_, ic, ix;
  float ci, si, t1, t2, t3, t4, t5, t6, xi, yi, zi;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SLAR2V applies a vector of real plane rotations from both sides to
  //  a sequence of 2-by-2 real symmetric matrices, defined by the elements
  //  of the vectors x, y and z. For i = 1,2,...,n
  
  //     ( x(i)  z(i) ) := (  c(i)  s(i) ) ( x(i)  z(i) ) ( c(i) -s(i) )
  //     ( z(i)  y(i) )    ( -s(i)  c(i) ) ( z(i)  y(i) ) ( s(i)  c(i) )
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The number of plane rotations to be applied.
  
  //  X       (input/output) REAL array,
  //                         dimension (1+(N-1)*INCX)
  //          The vector x.
  
  //  Y       (input/output) REAL array,
  //                         dimension (1+(N-1)*INCX)
  //          The vector y.
  
  //  Z       (input/output) REAL array,
  //                         dimension (1+(N-1)*INCX)
  //          The vector z.
  
  //  INCX    (input) INTEGER
  //          The increment between elements of X, Y and Z. INCX > 0.
  
  //  C       (input) REAL array, dimension (1+(N-1)*INCC)
  //          The cosines of the plane rotations.
  
  //  S       (input) REAL array, dimension (1+(N-1)*INCC)
  //          The sines of the plane rotations.
  
  //  INCC    (input) INTEGER
  //          The increment between elements of C and S. INCC > 0.
  
  //  =====================================================================
  
  //     .. Local Scalars ..
  //     ..
  //     .. Executable Statements ..
  
  ix = 1;
  ic = 1;
  for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
    xi = x[ix - 1];
    yi = y[ix - 1];
    zi = z[ix - 1];
    ci = c[ic - 1];
    si = s[ic - 1];
    t1 = si*zi;
    t2 = ci*zi;
    t3 = t2 - si*xi;
    t4 = t2 + si*yi;
    t5 = ci*xi + t1;
    t6 = ci*yi - t1;
    x[ix - 1] = ci*t5 + si*t4;
    y[ix - 1] = ci*t6 - si*t3;
    z[ix - 1] = ci*t4 - si*t5;
    ix = ix + incx;
    ic = ic + incc;
  }
  
  //     End of SLAR2V
  
  return;
} // end of function 


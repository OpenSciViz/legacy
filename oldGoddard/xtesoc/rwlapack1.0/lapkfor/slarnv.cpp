/*
 * C++ implementation of Lapack routine slarnv
 *
 * $Id: slarnv.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:00:30
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: slarnv.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ONE = 1.0e0;
const float TWO = 2.0e0;
const long LV = 128;
const float TWOPI = 6.28318530717958623199592e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ slarnv(const long &idist, long iseed[], const long &n, 
 float x[])
{
  long _do0, _do1, _do2, _do3, i, i_, il, il2, iv, iv_;
  float u[LV];

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SLARNV returns a vector of n random real numbers from a uniform or
  //  normal distribution.
  
  //  Arguments
  //  =========
  
  //  IDIST   (input) INTEGER
  //          Specifies the distribution of the random numbers:
  //          = 1:  uniform (0,1)
  //          = 2:  uniform (-1,1)
  //          = 3:  normal (0,1)
  
  //  ISEED   (input/output) INTEGER array, dimension (4)
  //          On entry, the seed of the random number generator; the array
  //          elements must be between 0 and 4095, and ISEED(4) must be
  //          odd.
  //          On exit, the seed is updated.
  
  //  N       (input) INTEGER
  //          The number of random numbers to be generated.
  
  //  X       (output) REAL array, dimension (N)
  //          The generated random numbers.
  
  //  Further Details
  //  ===============
  
  //  This routine calls the auxiliary routine SLARUV to generate random
  //  real numbers from a uniform (0,1) distribution, in batches of up to
  //  128 using vectorisable code. The Box-Muller method is used to
  //  transform numbers from a uniform to a normal distribution.
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Local Arrays ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Executable Statements ..
  
  for( iv = 1, iv_ = iv - 1, _do0 = n; iv <= _do0; iv += LV/2, iv_ += LV/
   2 ) { 
    il = min( LV/2, n - iv + 1 );
    if( idist == 3 ) { 
      il2 = 2*il;
    }
    else { 
      il2 = il;
    }
    
    //        Call SLARUV to generate IL2 numbers from a uniform (0,1)
    //        distribution (IL2 <= LV)
    
    slaruv( iseed, il2, u );
    
    if( idist == 1 ) { 
      
      //           Copy generated numbers
      
      for( i = 1, i_ = i - 1, _do1 = il; i <= _do1; i++, i_++ ) { 
        x[iv + i_ - 1] = u[i_];
      }
    }
    else if( idist == 2 ) { 
      
      //           Convert generated numbers to uniform (-1,1) distribution
      
      for( i = 1, i_ = i - 1, _do2 = il; i <= _do2; i++, i_++ ) { 
        x[iv + i_ - 1] = TWO*u[i_] - ONE;
      }
    }
    else if( idist == 3 ) { 
      
      //           Convert generated numbers to normal (0,1) distribution
      
      for( i = 1, i_ = i - 1, _do3 = il; i <= _do3; i++, i_++ ) { 
        x[iv + i_ - 1] = sqrt( -TWO*log( u[i*2 - 2] ) )*cos( TWOPI*
         u[i*2 - 1] );
      }
    }
  }
  return;
  
  //     End of SLARNV
  
} // end of function 


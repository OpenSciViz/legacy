/*
 * C++ implementation of Lapack routine zdrot
 *
 * $Id: zdrot.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:45:47
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zdrot.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

RWLAPKDECL void /*FUNCTION*/ zdrot(const long &n, DComplex cx[], const long &incx, 
   DComplex cy[], const long &incy, const double &c, const double &s)
{
  long _do0, _do1, i, i_, ix, iy;
  DComplex ctemp;

  
  //     applies a plane rotation, where the cos and sin (c and s) are real
  //     and the vectors cx and cy are DComplex.
  //     jack dongarra, linpack, 3/11/78.
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Executable Statements ..
  
  if( n <= 0 ) 
    return;
  if( incx == 1 && incy == 1 ) 
    goto L_20;
  
  //        code for unequal increments or equal increments not equal
  //          to 1
  
  ix = 1;
  iy = 1;
  if( incx < 0 ) 
    ix = (-n + 1)*incx + 1;
  if( incy < 0 ) 
    iy = (-n + 1)*incy + 1;
  for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
    ctemp = c*cx[ix - 1] + s*cy[iy - 1];
    cy[iy - 1] = c*cy[iy - 1] - s*cx[ix - 1];
    cx[ix - 1] = ctemp;
    ix = ix + incx;
    iy = iy + incy;
  }
  return;
  
  //        code for both increments equal to 1
  
L_20:
  ;
  for( i = 1, i_ = i - 1, _do1 = n; i <= _do1; i++, i_++ ) { 
    ctemp = c*cx[i_] + s*cy[i_];
    cy[i_] = c*cy[i_] - s*cx[i_];
    cx[i_] = ctemp;
  }
  return;
} // end of function 


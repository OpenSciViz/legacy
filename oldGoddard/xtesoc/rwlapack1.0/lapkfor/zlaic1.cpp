/*
 * C++ implementation of Lapack routine zlaic1
 *
 * $Id: zlaic1.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:48:26
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zlaic1.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
const double TWO = 2.0e0;
const double HALF = 0.5e0;
const double FOUR = 4.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zlaic1(const long &job, const long &j, DComplex x[], 
 const double &sest, DComplex w[], const DComplex &gamma, double &sestpr, DComplex &s, 
 DComplex &c)
{
  double absalp, absest, absgam, b, eps, norma, s1, s2, scl, t, 
   test, tmp, zeta1, zeta2;
  DComplex alpha, cosine, sine;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZLAIC1 applies one step of incremental condition estimation in
  //  its simplest version:
  
  //  Let x, twonorm(x) = 1, be an approximate singular vector of an j-by-j
  //  lower triangular matrix L, such that
  //           twonorm(L*x) = sest
  //  Then ZLAIC1 computes sestpr, s, c such that
  //  the vector
  //                  [ s*x ]
  //           xhat = [  c  ]
  //  is an approximate singular vector of
  //                  [ L     0  ]
  //           Lhat = [ w' gamma ]
  //  in the sense that
  //           twonorm(Lhat*xhat) = sestpr.
  
  //  Depending on JOB, an estimate for the largest or smallest singular
  //  value is computed.
  
  //  Note that [s c]' and sestpr**2 is an eigenpair of the system
  
  //      diag(sest*sest, 0) + [alpha  gamma] * [ conjg(alpha) ]
  //                                            [ conjg(gamma) ]
  
  //  where  alpha =  conjg(x)'*w.
  
  //  Arguments
  //  =========
  
  //  JOB     (input) INTEGER
  //          = 1: an estimate for the largest singular value is computed.
  //          = 2: an estimate for the smallest singular value is computed.
  
  //  J       (input) INTEGER
  //          Length of X and W
  
  //  X       (input) COMPLEX*16 array, dimension (J)
  //          The j-vector x.
  
  //  SEST    (input) DOUBLE PRECISION
  //          Estimated singular value of j by j matrix L
  
  //  W       (input) COMPLEX*16 array, dimension (J)
  //          The j-vector w.
  
  //  GAMMA   (input) COMPLEX*16
  //          The diagonal element gamma.
  
  //  SESTPR  (output) DOUBLE PRECISION
  //          Estimated singular value of (j+1) by (j+1) matrix Lhat.
  
  //  S       (output) COMPLEX*16
  //          Sine needed in forming xhat.
  
  //  C       (output) COMPLEX*16
  //          Cosine needed in forming xhat.
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Executable Statements ..
  
  eps = dlamch( 'E'/*Epsilon*/ );
  alpha = zdotc( j, x, 1, w, 1 );
  
  absalp = abs( alpha );
  absgam = abs( gamma );
  absest = abs( sest );
  
  if( job == 1 ) { 
    
    //        Estimating largest singular value
    
    //        special cases
    
    if( sest == ZERO ) { 
      s1 = max( absgam, absalp );
      if( s1 == ZERO ) { 
        s = DComplex(ZERO);
        c = DComplex(ONE);
        sestpr = ZERO;
      }
      else { 
        s = alpha/s1;
        c = gamma/s1;
        tmp = real(sqrt( s*conj( s ) + c*conj( c ) ));
        s = s/tmp;
        c = c/tmp;
        sestpr = s1*tmp;
      }
      return;
    }
    else if( absgam <= eps*absest ) { 
      s = DComplex(ONE);
      c = DComplex(ZERO);
      tmp = max( absest, absalp );
      s1 = absest/tmp;
      s2 = absalp/tmp;
      sestpr = tmp*sqrt( s1*s1 + s2*s2 );
      return;
    }
    else if( absalp <= eps*absest ) { 
      s1 = absgam;
      s2 = absest;
      if( s1 <= s2 ) { 
        s = DComplex(ONE);
        c = DComplex(ZERO);
        sestpr = s2;
      }
      else { 
        s = DComplex(ZERO);
        c = DComplex(ONE);
        sestpr = s1;
      }
      return;
    }
    else if( absest <= eps*absalp || absest <= eps*absgam ) { 
      s1 = absgam;
      s2 = absalp;
      if( s1 <= s2 ) { 
        tmp = s1/s2;
        scl = sqrt( ONE + tmp*tmp );
        sestpr = s2*scl;
        s = (alpha/s2)/scl;
        c = (gamma/s2)/scl;
      }
      else { 
        tmp = s2/s1;
        scl = sqrt( ONE + tmp*tmp );
        sestpr = s1*scl;
        s = (alpha/s1)/scl;
        c = (gamma/s1)/scl;
      }
      return;
    }
    else { 
      
      //           normal case
      
      zeta1 = absalp/absest;
      zeta2 = absgam/absest;
      
      b = (ONE - zeta1*zeta1 - zeta2*zeta2)*HALF;
      c = DComplex(zeta1*zeta1);
      if( b > ZERO ) { 
        t = real(c/(b + sqrt( b*b + c )));
      }
      else { 
        t = real(sqrt( b*b + c ) - b);
      }
      
      sine = -((alpha/absest)/t);
      cosine = -((gamma/absest)/(ONE + t));
      tmp = real(sqrt( sine*conj( sine ) + cosine*conj( cosine ) ));
      s = sine/tmp;
      c = cosine/tmp;
      sestpr = sqrt( t + ONE )*absest;
      return;
    }
    
  }
  else if( job == 2 ) { 
    
    //        Estimating smallest singular value
    
    //        special cases
    
    if( sest == ZERO ) { 
      sestpr = ZERO;
      if( max( absgam, absalp ) == ZERO ) { 
        sine = DComplex(ONE);
        cosine = DComplex(ZERO);
      }
      else { 
        sine = -(conj( gamma ));
        cosine = conj( alpha );
      }
      s1 = max( abs( sine ), abs( cosine ) );
      s = sine/s1;
      c = cosine/s1;
      tmp = real(sqrt( s*conj( s ) + c*conj( c ) ));
      s = s/tmp;
      c = c/tmp;
      return;
    }
    else if( absgam <= eps*absest ) { 
      s = DComplex(ZERO);
      c = DComplex(ONE);
      sestpr = absgam;
      return;
    }
    else if( absalp <= eps*absest ) { 
      s1 = absgam;
      s2 = absest;
      if( s1 <= s2 ) { 
        s = DComplex(ZERO);
        c = DComplex(ONE);
        sestpr = s1;
      }
      else { 
        s = DComplex(ONE);
        c = DComplex(ZERO);
        sestpr = s2;
      }
      return;
    }
    else if( absest <= eps*absalp || absest <= eps*absgam ) { 
      s1 = absgam;
      s2 = absalp;
      if( s1 <= s2 ) { 
        tmp = s1/s2;
        scl = sqrt( ONE + tmp*tmp );
        sestpr = absest*(tmp/scl);
        s = -((conj( gamma )/s2)/scl);
        c = (conj( alpha )/s2)/scl;
      }
      else { 
        tmp = s2/s1;
        scl = sqrt( ONE + tmp*tmp );
        sestpr = absest/scl;
        s = -((conj( gamma )/s1)/scl);
        c = (conj( alpha )/s1)/scl;
      }
      return;
    }
    else { 
      
      //           normal case
      
      zeta1 = absalp/absest;
      zeta2 = absgam/absest;
      
      norma = max( ONE + zeta1*zeta1 + zeta1*zeta2, zeta1*zeta2 + 
       zeta2*zeta2 );
      
      //           See if root is closer to zero or to ONE
      
      test = ONE + TWO*(zeta1 - zeta2)*(zeta1 + zeta2);
      if( test >= ZERO ) { 
        
        //              root is close to zero, compute directly
        
        b = (zeta1*zeta1 + zeta2*zeta2 + ONE)*HALF;
        c = DComplex(zeta2*zeta2);
        t = real(c/(b + sqrt( abs( b*b - c ) )));
        sine = (alpha/absest)/(ONE - t);
        cosine = -((gamma/absest)/t);
        sestpr = sqrt( t + FOUR*eps*eps*norma )*absest;
      }
      else { 
        
        //              root is closer to ONE, shift by that amount
        
        b = (zeta2*zeta2 + zeta1*zeta1 - ONE)*HALF;
        c = DComplex(zeta1*zeta1);
        if( b >= ZERO ) { 
          t = real(-(c/(b + sqrt( b*b + c ))));
        }
        else { 
          t = real(b - sqrt( b*b + c ));
        }
        sine = -((alpha/absest)/t);
        cosine = -((gamma/absest)/(ONE + t));
        sestpr = sqrt( ONE + t + FOUR*eps*eps*norma )*absest;
      }
      tmp = real(sqrt( sine*conj( sine ) + cosine*conj( cosine ) ));
      s = sine/tmp;
      c = cosine/tmp;
      return;
      
    }
  }
  return;
  
  //     End of ZLAIC1
  
} // end of function 


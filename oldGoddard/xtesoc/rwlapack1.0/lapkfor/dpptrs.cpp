/*
 * C++ implementation of lapack routine dpptrs
 *
 * $Id: dpptrs.cpp,v 1.6 1993/04/06 20:42:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:37:38
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dpptrs.cpp,v $
 * Revision 1.6  1993/04/06  20:42:09  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.5  1993/03/19  18:41:23  alv
 * now passes chars explicitly, rather than indirection of a string, to shut up SUN warnings
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:16:50  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:08:37  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

RWLAPKDECL void /*FUNCTION*/ dpptrs(const char &uplo, const long &n, const long &nrhs, 
   double ap[], double *b, const long &ldb, long &info)
{
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
  int upper;
  long _do0, _do1, i, i_;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DPPTRS solves a system of linear equations A*X = B with a symmetric
  //  positive definite matrix A in packed storage using the Cholesky
  //  factorization A = U'*U or A = L*L' computed by DPPTRF.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the factor stored in A is upper or lower
  //          triangular.
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right hand sides, i.e., the number of columns
  //          of the matrix B.  NRHS >= 0.
  
  //  AP      (input) DOUBLE PRECISION array, dimension (N*(N+1)/2)
  //          The triangular factor U or L from the Cholesky factorization
  //          A = U'*U or A = L*L', packed columnwise in a linear array.
  //          The j-th column of U or L is stored in the array AP as
  //          follows:
  //          if UPLO = 'U', AP(i + (j-1)*j/2) = U(i,j) for 1<=i<=j;
  //          if UPLO = 'L', AP(i + (j-1)*(2n-j)/2) = L(i,j) for j<=i<=n.
  
  //  B       (input/output) DOUBLE PRECISION array, dimension (LDB,NRHS)
  //          On entry, the right hand side vectors B for the system of
  //          linear equations.
  //          On exit, the solution vectors, X.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( nrhs < 0 ) { 
    info = -3;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -6;
  }
  if( info != 0 ) { 
    xerbla( "DPPTRS", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 || nrhs == 0 ) 
    return;
  
  if( upper ) { 
    
    //        Solve A*X = B where A = U'*U.
    
    for( i = 1, i_ = i - 1, _do0 = nrhs; i <= _do0; i++, i_++ ) { 
      
      //           Solve U'*X = B, overwriting B with X.
      
      dtpsv( 'U'/* Upper */, 'T'/* Transpose */, 'N'/* Non-unit */
       , n, ap, &B(i_,0), 1 );
      
      //           Solve U*X = B, overwriting B with X.
      
      dtpsv( 'U'/* Upper */, 'N'/* No transpose */, 'N'/* Non-unit */
       , n, ap, &B(i_,0), 1 );
    }
  }
  else { 
    
    //        Solve A*X = B where A = L*L'.
    
    for( i = 1, i_ = i - 1, _do1 = nrhs; i <= _do1; i++, i_++ ) { 
      
      //           Solve L*Y = B, overwriting B with X.
      
      dtpsv( 'L'/* Lower */, 'N'/* No transpose */, 'N'/* Non-unit */
       , n, ap, &B(i_,0), 1 );
      
      //           Solve L'*X = Y, overwriting B with X.
      
      dtpsv( 'L'/* Lower */, 'T'/* Transpose */, 'N'/* Non-unit */
       , n, ap, &B(i_,0), 1 );
    }
  }
  
  return;
  
  //     End of DPPTRS
  
#undef  B
} // end of function 


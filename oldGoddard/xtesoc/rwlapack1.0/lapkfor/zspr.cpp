/*
 * C++ implementation of Lapack routine zspr
 *
 * $Id: zspr.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:50:30
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zspr.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const DComplex ZERO = DComplex(0.0e0,0.0e0);
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zspr(const char &uplo, const long &n, const DComplex &alpha, DComplex x[], 
 const long &incx, DComplex ap[])
{
  long _do0, _do1, _do2, _do3, _do4, _do5, _do6, _do7, i, i_, 
   info, ix, j, j_, jx, k, k_, kk, kx;
  DComplex temp;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZSPR    performs the symmetric rank 1 operation
  
  //     A := alpha*x*conjg( x' ) + A,
  
  //  where alpha is a DComplex scalar, x is an n element vector and A is an
  //  n by n symmetric matrix, supplied in packed form.
  
  //  Arguments
  //  ==========
  
  //  UPLO   - CHARACTER*1
  //           On entry, UPLO specifies whether the upper or lower
  //           triangular part of the matrix A is supplied in the packed
  //           array AP as follows:
  
  //              UPLO = 'U' or 'u'   The upper triangular part of A is
  //                                  supplied in AP.
  
  //              UPLO = 'L' or 'l'   The lower triangular part of A is
  //                                  supplied in AP.
  
  //           Unchanged on exit.
  
  //  N      - INTEGER
  //           On entry, N specifies the order of the matrix A.
  //           N must be at least zero.
  //           Unchanged on exit.
  
  //  ALPHA  - COMPLEX*16
  //           On entry, ALPHA specifies the scalar alpha.
  //           Unchanged on exit.
  
  //  X      - COMPLEX*16 array, dimension at least
  //           ( 1 + ( N - 1 )*abs( INCX ) ).
  //           Before entry, the incremented array X must contain the N-
  //           element vector x.
  //           Unchanged on exit.
  
  //  INCX   - INTEGER
  //           On entry, INCX specifies the increment for the elements of
  //           X. INCX must not be zero.
  //           Unchanged on exit.
  
  //  AP     - COMPLEX*16 array, dimension at least
  //           ( ( N*( N + 1 ) )/2 ).
  //           Before entry with  UPLO = 'U' or 'u', the array AP must
  //           contain the upper triangular part of the symmetric matrix
  //           packed sequentially, column by column, so that AP( 1 )
  //           contains a( 1, 1 ), AP( 2 ) and AP( 3 ) contain a( 1, 2 )
  //           and a( 2, 2 ) respectively, and so on. On exit, the array
  //           AP is overwritten by the upper triangular part of the
  //           updated matrix.
  //           Before entry with UPLO = 'L' or 'l', the array AP must
  //           contain the lower triangular part of the symmetric matrix
  //           packed sequentially, column by column, so that AP( 1 )
  //           contains a( 1, 1 ), AP( 2 ) and AP( 3 ) contain a( 2, 1 )
  //           and a( 3, 1 ) respectively, and so on. On exit, the array
  //           AP is overwritten by the lower triangular part of the
  //           updated matrix.
  //           Note that the imaginary parts of the diagonal elements need
  //           not be set, they are assumed to be zero, and on exit they
  //           are set to zero.
  
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  if( !lsame( uplo, 'U' ) && !lsame( uplo, 'L' ) ) { 
    info = 1;
  }
  else if( n < 0 ) { 
    info = 2;
  }
  else if( incx == 0 ) { 
    info = 5;
  }
  if( info != 0 ) { 
    xerbla( "ZSPR  ", info );
    return;
  }
  
  //     Quick return if possible.
  
  if( (n == 0) || (ctocf(alpha) == ctocf(ZERO)) ) 
    return;
  
  //     Set the start point in X if the increment is not unity.
  
  if( incx <= 0 ) { 
    kx = 1 - (n - 1)*incx;
  }
  else if( incx != 1 ) { 
    kx = 1;
  }
  
  //     Start the operations. In this version the elements of the array AP
  //     are accessed sequentially with one pass through AP.
  
  kk = 1;
  if( lsame( uplo, 'U' ) ) { 
    
    //        Form  A  when upper triangle is stored in AP.
    
    if( incx == 1 ) { 
      for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
        if( ctocf(x[j_]) != ctocf(ZERO) ) { 
          temp = alpha*x[j_];
          k = kk;
          for( i = 1, i_ = i - 1, _do1 = j - 1; i <= _do1; i++, i_++ ) { 
            ap[k - 1] = ap[k - 1] + x[i_]*temp;
            k = k + 1;
          }
          ap[kk + j_ - 1] = ap[kk + j_ - 1] + x[j_]*temp;
        }
        else { 
          ap[kk + j_ - 1] = ap[kk + j_ - 1];
        }
        kk = kk + j;
      }
    }
    else { 
      jx = kx;
      for( j = 1, j_ = j - 1, _do2 = n; j <= _do2; j++, j_++ ) { 
        if( ctocf(x[jx - 1]) != ctocf(ZERO) ) { 
          temp = alpha*x[jx - 1];
          ix = kx;
          for( k = kk, k_ = k - 1, _do3 = kk + j - 2; k <= _do3; k++, k_++ ) { 
            ap[k_] = ap[k_] + x[ix - 1]*temp;
            ix = ix + incx;
          }
          ap[kk + j_ - 1] = ap[kk + j_ - 1] + x[jx - 1]*
           temp;
        }
        else { 
          ap[kk + j_ - 1] = ap[kk + j_ - 1];
        }
        jx = jx + incx;
        kk = kk + j;
      }
    }
  }
  else { 
    
    //        Form  A  when lower triangle is stored in AP.
    
    if( incx == 1 ) { 
      for( j = 1, j_ = j - 1, _do4 = n; j <= _do4; j++, j_++ ) { 
        if( ctocf(x[j_]) != ctocf(ZERO) ) { 
          temp = alpha*x[j_];
          ap[kk - 1] = ap[kk - 1] + temp*x[j_];
          k = kk + 1;
          for( i = j + 1, i_ = i - 1, _do5 = n; i <= _do5; i++, i_++ ) { 
            ap[k - 1] = ap[k - 1] + x[i_]*temp;
            k = k + 1;
          }
        }
        else { 
          ap[kk - 1] = ap[kk - 1];
        }
        kk = kk + n - j + 1;
      }
    }
    else { 
      jx = kx;
      for( j = 1, j_ = j - 1, _do6 = n; j <= _do6; j++, j_++ ) { 
        if( ctocf(x[jx - 1]) != ctocf(ZERO) ) { 
          temp = alpha*x[jx - 1];
          ap[kk - 1] = ap[kk - 1] + temp*x[jx - 1];
          ix = jx;
          for( k = kk + 1, k_ = k - 1, _do7 = kk + n - j; k <= _do7; k++, k_++ ) { 
            ix = ix + incx;
            ap[k_] = ap[k_] + x[ix - 1]*temp;
          }
        }
        else { 
          ap[kk - 1] = ap[kk - 1];
        }
        jx = jx + incx;
        kk = kk + n - j + 1;
      }
    }
  }
  
  return;
  
  //     End of ZSPR
  
} // end of function 


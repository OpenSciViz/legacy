/*
 * C++ implementation of Lapack routine spptrs
 *
 * $Id: spptrs.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:02:07
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: spptrs.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

RWLAPKDECL void /*FUNCTION*/ spptrs(const char &uplo, const long &n, const long &nrhs, 
   float ap[], float *b, const long &ldb, long &info)
{
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
  int upper;
  long _do0, _do1, i, i_;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SPPTRS solves a system of linear equations A*X = B with a symmetric
  //  positive definite matrix A in packed storage using the Cholesky
  //  factorization A = U'*U or A = L*L' computed by SPPTRF.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the factor stored in A is upper or lower
  //          triangular.
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right hand sides, i.e., the number of columns
  //          of the matrix B.  NRHS >= 0.
  
  //  AP      (input) REAL array, dimension (N*(N+1)/2)
  //          The triangular factor U or L from the Cholesky factorization
  //          A = U'*U or A = L*L', packed columnwise in a linear array.
  //          The j-th column of U or L is stored in the array AP as
  //          follows:
  //          if UPLO = 'U', AP(i + (j-1)*j/2) = U(i,j) for 1<=i<=j;
  //          if UPLO = 'L', AP(i + (j-1)*(2n-j)/2) = L(i,j) for j<=i<=n.
  
  //  B       (input/output) REAL array, dimension (LDB,NRHS)
  //          On entry, the right hand side vectors B for the system of
  //          linear equations.
  //          On exit, the solution vectors, X.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( nrhs < 0 ) { 
    info = -3;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -6;
  }
  if( info != 0 ) { 
    xerbla( "SPPTRS", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 || nrhs == 0 ) 
    return;
  
  if( upper ) { 
    
    //        Solve A*X = B where A = U'*U.
    
    for( i = 1, i_ = i - 1, _do0 = nrhs; i <= _do0; i++, i_++ ) { 
      
      //           Solve U'*X = B, overwriting B with X.
      
      stpsv( 'U'/*Upper*/, 'T'/*Transpose*/, 'N'/*Non-unit*/
       , n, ap, &B(i_,0), 1 );
      
      //           Solve U*X = B, overwriting B with X.
      
      stpsv( 'U'/*Upper*/, 'N'/*No transpose*/, 'N'/*Non-unit*/
       , n, ap, &B(i_,0), 1 );
    }
  }
  else { 
    
    //        Solve A*X = B where A = L*L'.
    
    for( i = 1, i_ = i - 1, _do1 = nrhs; i <= _do1; i++, i_++ ) { 
      
      //           Solve L*Y = B, overwriting B with X.
      
      stpsv( 'L'/*Lower*/, 'N'/*No transpose*/, 'N'/*Non-unit*/
       , n, ap, &B(i_,0), 1 );
      
      //           Solve L'*X = Y, overwriting B with X.
      
      stpsv( 'L'/*Lower*/, 'T'/*Transpose*/, 'N'/*Non-unit*/
       , n, ap, &B(i_,0), 1 );
    }
  }
  
  return;
  
  //     End of SPPTRS
  
#undef  B
} // end of function 


/*
 * C++ implementation of Lapack routine zhsein
 *
 * $Id: zhsein.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:48:02
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zhsein.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const DComplex ZERO = DComplex(0.0e0);
const double RZERO = 0.0e0;
// end of PARAMETER translations

inline double zhsein_cabs1(DComplex cdum) { return abs( real( (cdum) ) ) + 
   abs( imag( (cdum) ) ); }
RWLAPKDECL void /*FUNCTION*/ zhsein(const char &job, const char &eigsrc, const char &initv, int select[], 
 const long &n, DComplex *h, const long &ldh, DComplex w[], DComplex *vl, 
 const long &ldvl, DComplex *vr, const long &ldvr, const long &mm, long &m, 
 DComplex work[], double rwork[], long ifaill[], long ifailr[], 
 long &info)
{
#define H(I_,J_)  (*(h+(I_)*(ldh)+(J_)))
#define VL(I_,J_) (*(vl+(I_)*(ldvl)+(J_)))
#define VR(I_,J_) (*(vr+(I_)*(ldvr)+(J_)))
  int bothv, fromqr, leftv, noinit, rightv;
  long _do0, _do1, _do2, _do3, _do4, _do5, _do6, i, i_, iinfo, 
   k, k_, kl, kln, kr, ks, ldwork;
  double eps3, hnorm, smlnum, ulp, unfl;
  DComplex cdum, wk;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZHSEIN uses inverse iteration to find specified right and/or left
  //  eigenvectors of a DComplex upper Hessenberg matrix H.
  
  //  The right eigenvector x and the left eigenvector y of the matrix H
  //  corresponding to an eigenvalue w are defined by:
  
  //               H x = w x,     y' H = w y'
  
  //  where y' denotes the conjugate transpose of the vector y.
  
  //  Arguments
  //  =========
  
  //  JOB     (input) CHARACTER*1
  //          = 'R': compute right eigenvectors only;
  //          = 'L': compute left eigenvectors only;
  //          = 'B': compute both right and left eigenvectors.
  
  //  EIGSRC  (input) CHARACTER*1
  //          Specifies the source of eigenvalues supplied in W:
  //          = 'Q': the eigenvalues were found using ZHSEQR; thus, if
  //                 H has zero sub-diagonal entries, and so is
  //                 block-triangular, then the j-th eigenvalue can be
  //                 assumed to be an eigenvalue of the block containing
  //                 the j-th row/column.  This property allows ZHSEIN to
  //                 perform inverse iteration on just one diagonal block.
  //          = 'N': no assumptions are made on the correspondence
  //                 between eigenvalues and diagonal blocks.  In this
  //                 case, ZHSEIN must always perform inverse iteration
  //                 using the whole matrix H.
  
  //  INITV   (input) CHARACTER*1
  //          Specifies whether initial starting vectors are supplied for
  //          inverse iteration:
  //          = 'N': no initial vectors are supplied;
  //          = 'U': user-supplied initial vectors are stored in the arrays
  //                 VL and/or VR.
  
  //  SELECT  (input) LOGICAL array, dimension (N)
  //          Specifies the eigenvectors to be computed. To select the
  //          eigenvector corresponding to the eigenvalue W(j),
  //          SELECT(j) must be set to .TRUE..
  
  //  N       (input) INTEGER
  //          The order of the matrix H.  N >= 0.
  
  //  H       (input) COMPLEX*16 array, dimension (LDH,N)
  //          The upper Hessenberg matrix H.
  
  //  LDH     (input) INTEGER
  //          The leading dimension of the array H.  LDH >= max(1,N).
  
  //  W       (input/output) COMPLEX*16 array, dimension (N)
  //          On entry, the eigenvalues of H.
  //          On exit, the real parts of W may have been altered since
  //          close eigenvalues are perturbed slightly in searching for
  //          independent eigenvectors.
  
  //  VL      (input/output) COMPLEX*16 array, dimension (LDVL,MM)
  //          On entry, if INITV = 'U' and JOB = 'L' or 'B', VL must
  //          contain starting vectors for the inverse iteration for the
  //          left eigenvectors; the starting vector for each eigenvector
  //          must be in the same column in which the eigenvector will be
  //          stored.
  //          On exit, if JOB = 'L' or 'B', the left eigenvectors
  //          specified by SELECT will be stored consecutively in the
  //          columns of VL, in the same order as their eigenvalues.
  //          If JOB = 'R', VL is not referenced.
  
  //  LDVL    (input) INTEGER
  //          The leading dimension of the array VL.
  //          LDVL >= max(1,N) if JOB = 'L' or 'B'; LDVL >= 1 otherwise.
  
  //  VR      (input/output) COMPLEX*16 array, dimension (LDVR,MM)
  //          On entry, if INITV = 'U' and JOB = 'R' or 'B', VR must
  //          contain starting vectors for the inverse iteration for the
  //          right eigenvectors; the starting vector for each eigenvector
  //          must be in the same column in which the eigenvector will be
  //          stored.
  //          On exit, if JOB = 'R' or 'B', the right eigenvectors
  //          specified by SELECT will be stored consecutively in the
  //          columns of VR, in the same order as their eigenvalues.
  //          If JOB = 'L', VR is not referenced.
  
  //  LDVR    (input) INTEGER
  //          The leading dimension of the array VR.
  //          LDVR >= max(1,N) if JOB = 'R' or 'B'; LDVR >= 1 otherwise.
  
  //  MM      (input) INTEGER
  //          The number of columns in the arrays VL and/or VR. MM >= M.
  
  //  M       (output) INTEGER
  //          The number of columns in the arrays VL and/or VR required to
  //          store the eigenvectors (= the number of .TRUE. elements in
  //          SELECT).
  
  //  WORK    (workspace) COMPLEX*16 array, dimension (N*N)
  
  //  RWORK   (workspace) DOUBLE PRECISION array, dimension (N)
  
  //  IFAILL  (output) INTEGER array, dimension (MM)
  //          If JOB = 'L' or 'B', IFAILL(i) = j > 0 if the left
  //          eigenvector in the i-th column of VL (corresponding to the
  //          eigenvalue w(j)) failed to converge; IFAILL(i) = 0 if the
  //          eigenvector converged satisfactorily.
  //          If JOB = 'R', IFAILL is not referenced.
  
  //  IFAILR  (output) INTEGER array, dimension (MM)
  //          If JOB = 'R' or 'B', IFAILR(i) = j > 0 if the right
  //          eigenvector in the i-th column of VR (corresponding to the
  //          eigenvalue w(j)) failed to converge; IFAILR(i) = 0 if the
  //          eigenvector converged satisfactorily.
  //          If JOB = 'L', IFAILR is not referenced.
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument had an illegal value
  //          > 0: INFO is the number of eigenvectors which failed to
  //               converge; see IFAILL and IFAILR for further details.
  
  //  Further Details
  //  ===============
  
  //  Each eigenvector is normalized so that the element of largest
  //  magnitude has magnitude 1; here the magnitude of a DComplex number
  //  (x,y) is taken to be |x|+|y|.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Statement Functions ..
  //     ..
  //     .. Statement Function definitions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Decode and test the input parameters.
  
  bothv = lsame( job, 'B' );
  rightv = lsame( job, 'R' ) || bothv;
  leftv = lsame( job, 'L' ) || bothv;
  
  fromqr = lsame( eigsrc, 'Q' );
  
  noinit = lsame( initv, 'N' );
  
  //     Set M to the number of columns required to store the selected
  //     eigenvectors.
  
  m = 0;
  for( k = 1, k_ = k - 1, _do0 = n; k <= _do0; k++, k_++ ) { 
    if( select[k_] ) 
      m = m + 1;
  }
  
  info = 0;
  if( !rightv && !leftv ) { 
    info = -1;
  }
  else if( !fromqr && !lsame( eigsrc, 'N' ) ) { 
    info = -2;
  }
  else if( !noinit && !lsame( initv, 'N' ) ) { 
    info = -3;
  }
  else if( n < 0 ) { 
    info = -5;
  }
  else if( ldh < max( 1, n ) ) { 
    info = -7;
  }
  else if( ldvl < 1 || (leftv && ldvl < n) ) { 
    info = -10;
  }
  else if( ldvr < 1 || (rightv && ldvr < n) ) { 
    info = -12;
  }
  else if( mm < m ) { 
    info = -13;
  }
  if( info != 0 ) { 
    xerbla( "ZHSEIN", -info );
    return;
  }
  
  //     Quick return if possible.
  
  if( n == 0 ) 
    return;
  
  //     Set machine-dependent constants.
  
  unfl = dlamch( 'S'/*Safe minimum*/ );
  ulp = dlamch( 'P'/*Precision*/ );
  smlnum = unfl*(n/ulp);
  
  ldwork = n;
  
  kl = 1;
  kln = 0;
  if( fromqr ) { 
    kr = 0;
  }
  else { 
    kr = n;
  }
  ks = 1;
  
  for( k = 1, k_ = k - 1, _do1 = n; k <= _do1; k++, k_++ ) { 
    if( select[k_] ) { 
      
      //           Compute eigenvector(s) corresponding to W(K).
      
      if( fromqr ) { 
        
        //              If affiliation of eigenvalues is known, check whether
        //              the matrix splits.
        
        //              Determine KL and KR such that 1 <= KL <= K <= KR <= N
        //              and H(KL,KL-1) and H(KR+1,KR) are zero (or KL = 1 or
        //              KR = N).
        
        //              Then inverse iteration can be performed with the
        //              submatrix H(KL:N,KL:N) for a left eigenvector, and with
        //              the submatrix H(1:KR,1:KR) for a right eigenvector.
        
        for( i = k, i_ = i - 1, _do2 = kl + 1; i >= _do2; i--, i_-- ) { 
          if( ctocf(H(i_ - 1,i_)) == ctocf(ZERO) ) 
            goto L_30;
        }
L_30:
        ;
        kl = i;
        if( k > kr ) { 
          for( i = k, i_ = i - 1, _do3 = n - 1; i <= _do3; i++, i_++ ) { 
            if( ctocf(H(i_,i_ + 1)) == ctocf(ZERO) ) 
              goto L_50;
          }
L_50:
          ;
          kr = i;
        }
      }
      
      if( kl != kln ) { 
        kln = kl;
        
        //              Compute infinity-norm of submatrix H(KL:KR,KL:KR) if it
        //              has not ben computed before.
        
        hnorm = zlanhs( 'I', kr - kl + 1, &H(kl - 1,kl - 1), 
         ldh, rwork );
        if( hnorm > RZERO ) { 
          eps3 = hnorm*ulp;
        }
        else { 
          eps3 = smlnum;
        }
      }
      
      //           Perturb eigenvalue if it is close to any previous
      //           selected eigenvalues affiliated to the submatrix
      //           H(KL:KR,KL:KR). Close roots are modified by EPS3.
      
      wk = w[k_];
L_60:
      ;
      for( i = k - 1, i_ = i - 1, _do4 = kl; i >= _do4; i--, i_-- ) { 
        if( select[i_] && zhsein_cabs1( w[i_] - wk ) < eps3 ) { 
          wk = wk + eps3;
          goto L_60;
        }
      }
      w[k_] = wk;
      
      if( leftv ) { 
        
        //              Compute left eigenvector.
        
        zlaein( FALSE, noinit, n - kl + 1, &H(kl - 1,kl - 1), 
         ldh, wk, &VL(ks - 1,kl - 1), work, ldwork, rwork, 
         eps3, smlnum, iinfo );
        if( iinfo > 0 ) { 
          info = info + 1;
          ifaill[ks - 1] = k;
        }
        else { 
          ifaill[ks - 1] = 0;
        }
        for( i = 1, i_ = i - 1, _do5 = kl - 1; i <= _do5; i++, i_++ ) { 
          VL(ks - 1,i_) = ZERO;
        }
      }
      if( rightv ) { 
        
        //              Compute right eigenvector.
        
        zlaein( TRUE, noinit, kr, h, ldh, wk, &VR(ks - 1,0), 
         work, ldwork, rwork, eps3, smlnum, iinfo );
        if( iinfo > 0 ) { 
          info = info + 1;
          ifailr[ks - 1] = k;
        }
        else { 
          ifailr[ks - 1] = 0;
        }
        for( i = kr + 1, i_ = i - 1, _do6 = n; i <= _do6; i++, i_++ ) { 
          VR(ks - 1,i_) = ZERO;
        }
      }
      ks = ks + 1;
    }
  }
  
  return;
  
  //     End of ZHSEIN
  
#undef  VR
#undef  VL
#undef  H
} // end of function 


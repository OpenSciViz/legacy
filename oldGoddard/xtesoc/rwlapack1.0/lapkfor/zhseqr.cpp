/*
 * C++ implementation of Lapack routine zhseqr
 *
 * $Id: zhseqr.cpp,v 1.2 1993/07/21 22:22:14 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:48:03
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zhseqr.cpp,v $
 * Revision 1.2  1993/07/21  22:22:14  alv
 * ported to Microsoft visual C++
 *
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const DComplex ZERO = DComplex(0.0e0);
const DComplex ONE = DComplex(1.0e0);
const double RZERO = 0.0e0;
const double RONE = 1.0e0;
const double CONST_ = 1.5e0;
const long NSMAX = 15;
// end of PARAMETER translations

inline double zhseqr_cabs1(DComplex cdum) { return abs( real( (cdum) ) ) + 
   abs( imag( (cdum) ) ); }
RWLAPKDECL void /*FUNCTION*/ zhseqr(const char &job, const char &compz, const long &n, const long &ilo, 
 const long &ihi, DComplex *h, const long &ldh, DComplex w[], DComplex *z, 
 const long &ldz, DComplex work[], const long &lwork, long &info)
{
#define H(I_,J_)  (*(h+(I_)*(ldh)+(J_)))
#define Z(I_,J_)  (*(z+(I_)*(ldz)+(J_)))
  // PARAMETER translations
  const long LDS = NSMAX;
  // end of PARAMETER translations

  char _c0[2], _c1[2];
  int initz, wantt, wantz;
  long _do0, _do1, _do10, _do11, _do12, _do13, _do2, _do3, _do4, 
   _do5, _do6, _do7, _do8, _do9, i, i1, i2, i_, ierr, ii, ii_, itemp, 
   itn, its, its_, j, j_, k, k_, l, maxb, nh, nr, ns, nv;
  double ovfl, rtemp, rwork, smlnum, tst1, ulp, unfl;
  DComplex cdum, s[NSMAX][LDS], tau, temp, v[NSMAX + 1], vv[NSMAX + 1];
#define NCHRTMPS 1
  CHRTMP _c[NCHRTMPS];
  ini_chrtmp(_c,NCHRTMPS);

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZHSEQR computes the eigenvalues of a DComplex upper Hessenberg
  //  matrix H, and, optionally, the matrices T and Z from the Schur
  //  decomposition H = Z T Z', where T is an upper triangular matrix
  //  (the Schur form), Z is the unitary matrix of Schur vectors, and Z'
  //  denotes the conjugate transpose of Z.
  
  //  Optionally Z may be postmultiplied into an input unitary matrix Q,
  //  so that this routine can give the Schur factorization of a matrix A
  //  which has been reduced to the Hessenberg form H by the unitary
  //  matrix Q:  A = Q*H*Q' = (QZ)*T*(QZ)'.
  
  //  Arguments
  //  =========
  
  //  JOB     (input) CHARACTER*1
  //          = 'E': compute eigenvalues only;
  //          = 'S': compute eigenvalues and the Schur form T.
  
  //  COMPZ   (input) CHARACTER*1
  //          = 'N': no Schur vectors are computed;
  //          = 'I': Z is initialized to the unit matrix and the matrix Z
  //                 of Schur vectors of H is returned;
  //          = 'V': Z must contain an unitary matrix Q on entry, and
  //                 the product Q*Z is returned.
  
  //  N       (input) INTEGER
  //          The order of the matrix H.  N >= 0.
  
  //  ILO     (input) INTEGER
  //  IHI     (input) INTEGER
  //          It is assumed that H is already upper triangular in rows
  //          and columns 1:ILO-1 and IHI+1:N. ILO and IHI are normally
  //          set by a previous call to ZGEBAL, and then passed to CGEHRD
  //          when the matrix output by ZGEBAL is reduced to Hessenberg
  //          form. Otherwise ILO and IHI should be set to 1 and N
  //          respectively.
  //          1 <= ILO <= max(1,IHI); IHI <= N.
  
  //  H       (input/output) COMPLEX*16 array, dimension (LDH,N)
  //          On entry, the upper Hessenberg matrix H.
  //          On exit, if JOB = 'S', H contains the upper triangular matrix
  //          T from the Schur decomposition (the Schur form). If
  //          JOB = 'E', the contents of H are unspecified on exit.
  
  //  LDH     (input) INTEGER
  //          The leading dimension of the array H. LDH >= max(1,N).
  
  //  W       (output) COMPLEX*16 array, dimension (N)
  //          The computed eigenvalues. If JOB = 'S', the eigenvalues are
  //          stored in the same order as on the diagonal of the Schur form
  //          returned in H, with W(i) = H(i,i).
  
  //  Z       (input/output) COMPLEX*16 array, dimension (LDZ,N)
  //          If COMPZ = 'N': Z is not referenced.
  //          If COMPZ = 'I': on entry, Z need not be set, and on exit, Z
  //          contains the unitary matrix Z of the Schur vectors of H.
  //          If COMPZ = 'V': on entry Z must contain an n-by-n matrix Q,
  //          which is assumed to be equal to the unit matrix except for
  //          the submatrix Z(ILO:IHI,ILO:IHI); on exit Z contains Q*Z.
  //          Normally Q is the unitary matrix generated by ZUNGHR after
  //          the call to ZGEHRD which formed the Hessenberg matrix H.
  
  //  LDZ     (input) INTEGER
  //          The leading dimension of the array Z. LDZ >= max(1,N).
  
  //  WORK    (workspace) COMPLEX*16 array, dimension (N)
  
  //  LWORK   (input) INTEGER
  //          This argument is now redundant.
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument had an illegal value
  //          > 0: ZHSEQR failed to compute all the eigenvalues in a total
  //               of 30*(IHI-ILO+1) iterations; if INFO = i, elements
  //               1:ilo-1 and i+1:n of W contain those eigenvalues which
  //               have been successfully computed.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Local Arrays ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Statement Functions ..
  //     ..
  //     .. Statement Function definitions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Decode and test the input parameters
  
  wantt = lsame( job, 'S' );
  initz = lsame( compz, 'I' );
  wantz = initz || lsame( compz, 'V' );
  
  info = 0;
  if( !lsame( job, 'E' ) && !wantt ) { 
    info = -1;
  }
  else if( !lsame( compz, 'N' ) && !wantz ) { 
    info = -2;
  }
  else if( n < 0 ) { 
    info = -3;
  }
  else if( ilo < 1 ) { 
    info = -4;
  }
  else if( ihi < min( ilo, n ) || ihi > n ) { 
    info = -5;
  }
  else if( ldh < max( 1, n ) ) { 
    info = -7;
  }
  else if( ldz < 1 || (wantz && ldz < max( 1, n )) ) { 
    info = -10;
  }
  if( info != 0 ) { 
    xerbla( "ZHSEQR", -info );
    return;
  }
  
  //     Initialize Z, if necessary
  
  if( initz ) 
    zlaset( 'F'/*Full*/, n, n, ZERO, ONE, z, ldz );
  
  //     Store the eigenvalues isolated by ZGEBAL.
  
  for( i = 1, i_ = i - 1, _do0 = ilo - 1; i <= _do0; i++, i_++ ) { 
    w[i_] = H(i_,i_);
  }
  for( i = ihi + 1, i_ = i - 1, _do1 = n; i <= _do1; i++, i_++ ) { 
    w[i_] = H(i_,i_);
  }
  
  //     Quick return if possible.
  
  if( n == 0 ) 
    return;
  if( ilo == ihi ) { 
    w[ilo - 1] = H(ilo - 1,ilo - 1);
    return;
  }
  
  //     Set rows and columns ILO to IHI to zero below the first
  //     subdiagonal.
  
  for( j = ilo, j_ = j - 1, _do2 = ihi - 2; j <= _do2; j++, j_++ ) { 
    for( i = j + 2, i_ = i - 1, _do3 = n; i <= _do3; i++, i_++ ) { 
      H(j_,i_) = ZERO;
    }
  }
  nh = ihi - ilo + 1;
  
  //     Determine the order of the multi-shift QR algorithm to be used.
  
  ns = ilaenv( 4, "ZHSEQR", f_concat(&_c[0],STR1(_c0,job),STR1(_c1,compz),
   NULL), n, ilo, ihi, -1 );
  maxb = ilaenv( 8, "ZHSEQR", f_concat(&_c[0],STR1(_c0,job),STR1(_c1,compz),
   NULL), n, ilo, ihi, -1 );
  if( (ns <= 1 || ns > nh) || maxb >= nh ) { 
    
    //        Use the standard double-shift algorithm
    
    zlahqr( wantt, wantz, n, ilo, ihi, h, ldh, w, ilo, ihi, z, 
     ldz, info );
    rel_chrtmp(_c,NCHRTMPS);
    return;
  }
  maxb = max( 2, maxb );
  ns = vmin( ns, maxb, NSMAX, IEND );
  
  //     Now 1 < NS <= MAXB < NH.
  
  //     Set machine-dependent constants for the stopping criterion.
  //     If norm(H) <= sqrt(OVFL), overflow should not occur.
  
  unfl = dlamch( 'S'/*Safe minimum*/ );
  ovfl = RONE/unfl;
  dlabad( unfl, ovfl );
  ulp = dlamch( 'P'/*Precision*/ );
  smlnum = unfl*(nh/ulp);
  
  //     I1 and I2 are the indices of the first row and last column of H
  //     to which transformations must be applied. If eigenvalues only are
  //     being computed, I1 and I2 are re-set inside the main loop.
  
  if( wantt ) { 
    i1 = 1;
    i2 = n;
  }
  else { 
    i1 = ilo;
    i2 = ihi;
  }
  
  //     Ensure that the subdiagonal elements are real.
  
  for( i = ilo + 1, i_ = i - 1, _do4 = ihi; i <= _do4; i++, i_++ ) { 
    temp = H(i_ - 1,i_);
    if( imag( temp ) != RZERO ) { 
      rtemp = dlapy2( real( temp ), imag( temp ) );
      H(i_ - 1,i_) = (DComplex)(rtemp);
      temp = temp/rtemp;
      if( i2 > i ) 
        zscal( i2 - i, conj( temp ), &H(i_ + 1,i_), ldh );
      zscal( i - i1, temp, &H(i_,i1 - 1), 1 );
      if( wantz ) 
        zscal( nh, temp, &Z(i_,ilo - 1), 1 );
    }
  }
  
  //     ITN is the total number of multiple-shift QR iterations allowed.
  
  itn = 30*nh;
  
  //     The main loop begins here. I is the loop index and decreases from
  //     IHI to ILO in steps of at most MAXB. Each iteration of the loop
  //     works with the active submatrix in rows and columns L to I.
  //     Eigenvalues I+1 to IHI have already converged. Either L = ILO, or
  //     H(L,L-1) is negligible so that the matrix splits.
  
  i = ihi;
L_60:
  ;
  if( i < ilo ) 
    goto L_180;
  
  //     Perform multiple-shift QR iterations on rows and columns ILO to I
  //     until a submatrix of order at most MAXB splits off at the bottom
  //     because a subdiagonal element has become negligible.
  
  l = ilo;
  for( its = 0, its_ = its - 1, _do5 = itn; its <= _do5; its++, its_++ ) { 
    
    //        Look for a single small subdiagonal element.
    
    for( k = i, k_ = k - 1, _do6 = l + 1; k >= _do6; k--, k_-- ) { 
      tst1 = zhseqr_cabs1( H(k_ - 1,k_ - 1) ) + zhseqr_cabs1( H(k_,k_) );
      if( tst1 == RZERO ) 
        tst1 = zlanhs( '1', i - l + 1, &H(l - 1,l - 1), ldh, 
         (double*)&rwork );
      if( abs( real( H(k_ - 1,k_) ) ) <= max( ulp*tst1, smlnum ) ) 
        goto L_80;
    }
L_80:
    ;
    l = k;
    if( l > ilo ) { 
      
      //           H(L,L-1) is negligible.
      
      H(l - 2,l - 1) = ZERO;
    }
    
    //        Exit from loop if a submatrix of order <= MAXB has split off.
    
    if( l >= i - maxb + 1 ) 
      goto L_170;
    
    //        Now the active submatrix is in rows and columns L to I. If
    //        eigenvalues only are being computed, only the active submatrix
    //        need be transformed.
    
    if( !wantt ) { 
      i1 = l;
      i2 = i;
    }
    
    if( its == 20 || its == 30 ) { 
      
      //           Exceptional shifts.
      
      for( ii = i - ns + 1, ii_ = ii - 1, _do7 = i; ii <= _do7; ii++, ii_++ ) { 
        w[ii_] = DComplex(CONST_*(abs( real( H(ii_ - 1,ii_) ) ) + 
         abs( real( H(ii_,ii_) ) )));
      }
    }
    else { 
      
      //           Use eigenvalues of trailing submatrix of order NS as shifts.
      
      zlacpy( 'F'/*Full*/, ns, ns, &H(i - ns,i - ns), ldh, 
       (DComplex*)s, LDS );
      zlahqr( FALSE, FALSE, ns, 1, ns, (DComplex*)s, LDS, &w[i - ns], 
       1, ns, z, ldz, ierr );
      if( ierr > 0 ) { 
        
        //              If ZLAHQR failed to compute all NS eigenvalues, use the
        //              unconverged diagonal elements as the remaining shifts.
        
        for( ii = 1, ii_ = ii - 1, _do8 = ierr; ii <= _do8; ii++, ii_++ ) { 
          w[i - ns + ii_] = s[ii_][ii_];
        }
      }
    }
    
    //        Form the first column of (G-w(1)) (G-w(2)) . . . (G-w(ns))
    //        where G is the Hessenberg submatrix H(L:I,L:I) and w is
    //        the vector of shifts (stored in W). The result is
    //        stored in the local array V.
    
    v[0] = ONE;
    for( ii = 2, ii_ = ii - 1, _do9 = ns + 1; ii <= _do9; ii++, ii_++ ) { 
      v[ii_] = ZERO;
    }
    nv = 1;
    for( j = i - ns + 1, j_ = j - 1, _do10 = i; j <= _do10; j++, j_++ ) { 
      zcopy( nv + 1, v, 1, vv, 1 );
      zgemv( 'N'/*No transpose*/, nv + 1, nv, ONE, &H(l - 1,l - 1), 
       ldh, vv, 1, -(w[j_]), v, 1 );
      nv = nv + 1;
      
      //           Scale V(1:NV) so that max(abs(V(i))) = 1. If V is zero,
      //           reset it to the unit vector.
      
      itemp = izamax( nv, v, 1 );
      rtemp = zhseqr_cabs1( v[itemp - 1] );
      if( rtemp == RZERO ) { 
        v[0] = ONE;
        for( ii = 2, ii_ = ii - 1, _do11 = nv; ii <= _do11; ii++, ii_++ ) { 
          v[ii_] = ZERO;
        }
      }
      else { 
        rtemp = max( rtemp, smlnum );
        zdscal( nv, RONE/rtemp, v, 1 );
      }
    }
    
    //        Multiple-shift QR step
    
    for( k = l, k_ = k - 1, _do12 = i - 1; k <= _do12; k++, k_++ ) { 
      
      //           The first iteration of this loop determines a reflection G
      //           from the vector V and applies it from left and right to H,
      //           thus creating a nonzero bulge below the subdiagonal.
      
      //           Each subsequent iteration determines a reflection G to
      //           restore the Hessenberg form in the (K-1)th column, and thus
      //           chases the bulge one step toward the bottom of the active
      //           submatrix. NR is the order of G.
      
      nr = min( ns + 1, i - k + 1 );
      if( k > l ) 
        zcopy( nr, &H(k_ - 1,k_), 1, v, 1 );
      zlarfg( nr, v[0], &v[1], 1, tau );
      if( k > l ) { 
        H(k_ - 1,k_) = v[0];
        for( ii = k + 1, ii_ = ii - 1, _do13 = i; ii <= _do13; ii++, ii_++ ) { 
          H(k_ - 1,ii_) = ZERO;
        }
      }
      v[0] = ONE;
      
      //           Apply G' from the left to transform the rows of the matrix
      //           in columns K to I2.
      
      zlarfx( 'L'/*Left*/, nr, i2 - k + 1, v, conj( tau ), 
       &H(k_,k_), ldh, work );
      
      //           Apply G from the right to transform the columns of the
      //           matrix in rows I1 to min(K+NR,I).
      
      zlarfx( 'R'/*Right*/, min( k + nr, i ) - i1 + 1, nr, 
       v, tau, &H(k_,i1 - 1), ldh, work );
      
      if( wantz ) { 
        
        //              Accumulate transformations in the matrix Z
        
        zlarfx( 'R'/*Right*/, nh, nr, v, tau, &Z(k_,ilo - 1), 
         ldz, work );
      }
    }
    
    //        Ensure that H(I,I-1) is real.
    
    temp = H(i - 2,i - 1);
    if( imag( temp ) != RZERO ) { 
      rtemp = dlapy2( real( temp ), imag( temp ) );
      H(i - 2,i - 1) = (DComplex)(rtemp);
      temp = temp/rtemp;
      if( i2 > i ) 
        zscal( i2 - i, conj( temp ), &H(i,i - 1), ldh );
      zscal( i - i1, temp, &H(i - 1,i1 - 1), 1 );
      if( wantz ) { 
        zscal( nh, temp, &Z(i - 1,ilo - 1), 1 );
      }
    }
    
  }
  
  //     Failure to converge in remaining number of iterations
  
  info = i;
  rel_chrtmp(_c,NCHRTMPS);
  return;
  
L_170:
  ;
  
  //     A submatrix of order <= MAXB in rows and columns L to I has split
  //     off. Use the double-shift QR algorithm to handle it.
  
  zlahqr( wantt, wantz, n, l, i, h, ldh, w, ilo, ihi, z, ldz, info );
  if( info > 0 ) {
    rel_chrtmp(_c,NCHRTMPS);
    return;
  }
  
  //     Decrement number of remaining iterations, and return to start of
  //     the main loop with a new value of I.
  
  itn = itn - its;
  i = l - 1;
  goto L_60;
  
L_180:
  ;
  rel_chrtmp(_c,NCHRTMPS);
  return;
  
  //     End of ZHSEQR
  
#undef  NCHRTMPS
#undef  Z
#undef  H
} // end of function 


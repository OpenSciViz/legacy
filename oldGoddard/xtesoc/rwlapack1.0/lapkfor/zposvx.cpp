/*
 * C++ implementation of Lapack routine zposvx
 *
 * $Id: zposvx.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:49:57
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zposvx.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zposvx(const char &fact, const char &uplo, const long &n, const long &nrhs, 
 DComplex *a, const long &lda, DComplex *af, const long &ldaf, char &equed, 
 double s[], DComplex *b, const long &ldb, DComplex *x, const long &ldx, 
 double &rcond, double ferr[], double berr[], DComplex work[], double rwork[], 
 long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define AF(I_,J_) (*(af+(I_)*(ldaf)+(J_)))
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
#define X(I_,J_)  (*(x+(I_)*(ldx)+(J_)))
  int equil, nofact;
  long _do0, _do1, _do2, _do3, _do4, i, i_, infequ, j, j_;
  double amax, anorm, scond;

  
  //  -- LAPACK driver routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZPOSVX uses the Cholesky factorization A = U'*U or A = L*L' to
  //  compute the solution to a DComplex system of linear equations
  //     A * X = B,
  //  where A is an N by N Hermitian positive definite matrix and X and B
  //  are N by NRHS matrices.
  
  //  Error bounds on the solution and a condition estimate are also
  //  provided.
  
  //  Description
  //  ===========
  
  //  The following steps are performed by this subroutine:
  
  //  1. If FACT = 'E', real scaling factors are computed to equilibrate
  //     the system:
  //        diag(S) * A * diag(S) * inv(diag(S)) * X = diag(S) * B
  //     Whether or not the system will be equilibrated depends on the
  //     scaling of the matrix A, but if equilibration is used, A is
  //     overwritten by diag(S)*A*diag(S) and B by diag(S)*B.
  
  //  2. If FACT = 'N' or 'E', the Cholesky decomposition is used to
  //     factor the matrix A (after equilibration if FACT = 'E') as
  //        A = U'* U ,  if UPLO = 'U', or
  //        A = L * L',  if UPLO = 'L',
  //     where U is an upper triangular matrix, L is a lower triangular
  //     matrix, and ' indicates conjugate transpose.
  
  //  3. The factored form of A is used to estimate the condition number
  //     of the matrix A.  If the reciprocal of the condition number is
  //     less than machine precision, steps 4-6 are skipped.
  
  //  4. The system of equations A*X = B is solved for X using the
  //     factored form of A.
  
  //  5. Iterative refinement is applied to improve the computed solution
  //     vectors and calculate error bounds and backward error estimates
  //     for them.
  
  //  6. If FACT = 'E' and equilibration was used, the vectors X are
  //     premultiplied by diag(S) so that they solve the original system
  //     before equilibration.
  
  //  Arguments
  //  =========
  
  //  FACT    (input) CHARACTER*1
  //          Specifies whether or not the factored form of the matrix A is
  //          supplied on entry, and if not, whether the matrix A should be
  //          equilibrated before it is factored.
  //          = 'F':  On entry, AF contains the factored form of A. A and
  //                  AF will not be modified.
  //          = 'N':  The matrix A will be copied to AF and factored.
  //          = 'E':  The matrix A will be equilibrated if necessary, then
  //                  copied to AF and factored.
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangular part of the
  //          Hermitian matrix A is stored:
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The number of linear equations, i.e., the order of the
  //          matrix A.  N >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right hand sides, i.e., the number of columns
  //          of the matrices B and X.  NRHS >= 0.
  
  //  A       (input/output) COMPLEX*16 array, dimension (LDA,N)
  //          On entry, the Hermitian matrix A.  If UPLO = 'U', the leading
  //          N by N upper triangular part of A contains the upper
  //          triangular part of the matrix A, and the strictly lower
  //          triangular part of A is not referenced.  If UPLO = 'L', the
  //          leading N by N lower triangular part of A contains the lower
  //          triangular part of the matrix A, and the strictly upper
  //          triangular part of A is not referenced.  A is not modified if
  //          FACT = 'F' or 'N', or if FACT = 'E' and EQUED = 'N' on exit.
  
  //          On exit, if EQUED = 'Y', A is replaced by diag(S)*A*diag(S).
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  AF      (input or output) COMPLEX*16 array, dimension (LDAF,N)
  //          If FACT = 'F', then AF is an input argument and on entry
  //          contains the triangular factor U or L from the Cholesky
  //          factorization A = U'*U or A = L*L', in the same storage
  //          format as A.
  
  //          If FACT = 'N', then AF is an output argument and on exit
  //          returns the triangular factor U or L from the Cholesky
  //          factorization A = U'*U or A = L*L' of the original matrix A.
  
  //          If FACT = 'E', then AF is an output argument and on exit
  //          returns the triangular factor U or L from the Cholesky
  //          factorization A = U'*U or A = L*L' of the equilibrated
  //          matrix A (see the description of A for the form of the
  //          equilibrated matrix).
  
  //  LDAF    (input) INTEGER
  //          The leading dimension of the array AF.  LDAF >= max(1,N).
  
  //  EQUED   (output) CHARACTER*1
  //          Specifies the form of equilibration that was done.
  //          = 'N':  No equilibration (always true if FACT = 'F' or 'N').
  //          = 'Y':  Equilibration was done, i.e., A has been replaced by
  //                  diag(S) * A * diag(S).
  
  //  S       (output) DOUBLE PRECISION array, dimension (N)
  //          The scale factors for A.  Not assigned if FACT = 'F' or 'N'.
  
  //  B       (input/output) COMPLEX*16 array, dimension (LDB,NRHS)
  //          On entry, the n-by-nrhs right-hand side matrix B.
  //          On exit, if EQUED = 'N', B is not modified; if EQUED = 'Y',
  //          B is overwritten by diag(S) * B.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  X       (output) COMPLEX*16 array, dimension (LDX,NRHS)
  //          If INFO = 0, the n-by-nrhs solution matrix X to the original
  //          system of equations.  Note that if EQUED = 'Y', A and B are
  //          modified on exit, and the solution to the equilibrated system
  //          is inv(diag(S))*X.
  
  //  LDX     (input) INTEGER
  //          The leading dimension of the array X.  LDX >= max(1,N).
  
  //  RCOND   (output) DOUBLE PRECISION
  //          The estimate of the reciprocal condition number of the matrix
  //          A.  If RCOND is less than the machine precision (in
  //          particular, if RCOND = 0), the matrix is singular to working
  //          precision.  This condition is indicated by a return code of
  //          INFO > 0, and the solution and error bounds are not computed.
  
  //  FERR    (output) DOUBLE PRECISION array, dimension (NRHS)
  //          The estimated forward error bounds for each solution vector
  //          X(j) (the j-th column of the solution matrix X).
  //          If XTRUE is the true solution, FERR(j) bounds the magnitude
  //          of the largest entry in (X(j) - XTRUE) divided by the
  //          magnitude of the largest entry in X(j).  The quality of the
  //          error bound depends on the quality of the estimate of
  //          norm(inv(A)) computed in the code; if the estimate of
  //          norm(inv(A)) is accurate, the error bound is guaranteed.
  
  //  BERR    (output) DOUBLE PRECISION array, dimension (NRHS)
  //          The componentwise relative backward error of each solution
  //          vector X(j)(i.e., the smallest relative change in
  //          any entry of A or B that makes X(j) an exact solution).
  
  //  WORK    (workspace) COMPLEX*16 array, dimension (2*N)
  
  //  RWORK   (workspace) DOUBLE PRECISION array, dimension (N)
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: if INFO = k, and k is
  //               <= N: if INFO = k, the leading minor of order k of A
  //                     is not positive definite, so the factorization
  //                     could not be completed, and the solution and error
  //                     bounds could not be computed.
  //               = N+1: RCOND is less than machine precision.  The
  //                     factorization has been completed, but the matrix
  //                     is singular to working precision, and the solution
  //                     and error bounds have not been computed.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  nofact = lsame( fact, 'N' );
  equil = lsame( fact, 'E' );
  if( (!nofact && !equil) && !lsame( fact, 'F' ) ) { 
    info = -1;
  }
  else if( !lsame( uplo, 'U' ) && !lsame( uplo, 'L' ) ) { 
    info = -2;
  }
  else if( n < 0 ) { 
    info = -3;
  }
  else if( nrhs < 0 ) { 
    info = -4;
  }
  else if( lda < max( 1, n ) ) { 
    info = -6;
  }
  else if( ldaf < max( 1, n ) ) { 
    info = -8;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -12;
  }
  else if( ldx < max( 1, n ) ) { 
    info = -14;
  }
  if( info != 0 ) { 
    xerbla( "ZPOSVX", -info );
    return;
  }
  
  equed = 'N';
  if( equil ) { 
    
    //        Compute row and column scalings to equilibrate the matrix A.
    
    zpoequ( n, a, lda, s, scond, amax, infequ );
    if( infequ == 0 ) { 
      
      //           Equilibrate the matrix.
      
      zlaqsy( uplo, n, a, lda, s, scond, amax, equed );
      
      //           Scale the right hand side.
      
      if( lsame( equed, 'Y' ) ) { 
        for( j = 1, j_ = j - 1, _do0 = nrhs; j <= _do0; j++, j_++ ) { 
          for( i = 1, i_ = i - 1, _do1 = n; i <= _do1; i++, i_++ ) { 
            B(j_,i_) = s[i_]*B(j_,i_);
          }
        }
      }
    }
  }
  
  if( nofact || equil ) { 
    
    //        Compute the Cholesky factorization A = U'*U or A = L*L'.
    
    zlacpy( uplo, n, n, a, lda, af, ldaf );
    zpotrf( uplo, n, af, ldaf, info );
    
    //        Return if INFO is non-zero.
    
    if( info != 0 ) { 
      if( info > 0 ) 
        rcond = ZERO;
      return;
    }
  }
  
  //     Compute the norm of the matrix A.
  
  anorm = zlansy( '1', uplo, n, a, lda, rwork );
  
  //     Compute the reciprocal of the condition number of A.
  
  zpocon( uplo, n, af, ldaf, anorm, rcond, work, rwork, info );
  
  //     Return if the matrix is singular to working precision.
  
  if( rcond < dlamch( 'E'/*Epsilon*/ ) ) { 
    info = n + 1;
    return;
  }
  
  //     Compute the solution vectors X.
  
  zlacpy( 'F'/*Full*/, n, nrhs, b, ldb, x, ldx );
  zpotrs( uplo, n, nrhs, af, ldaf, x, ldx, info );
  
  //     Use iterative refinement to improve the computed solutions and
  //     compute error bounds and backward error estimates for them.
  
  zporfs( uplo, n, nrhs, a, lda, af, ldaf, b, ldb, x, ldx, ferr, 
   berr, work, rwork, info );
  
  //     Transform the solution vectors X to solutions of the original
  //     system.
  
  if( lsame( equed, 'Y' ) ) { 
    for( j = 1, j_ = j - 1, _do2 = nrhs; j <= _do2; j++, j_++ ) { 
      for( i = 1, i_ = i - 1, _do3 = n; i <= _do3; i++, i_++ ) { 
        X(j_,i_) = s[i_]*X(j_,i_);
      }
    }
    for( j = 1, j_ = j - 1, _do4 = nrhs; j <= _do4; j++, j_++ ) { 
      ferr[j_] = ferr[j_]/scond;
    }
  }
  
  return;
  
  //     End of ZPOSVX
  
#undef  X
#undef  B
#undef  AF
#undef  A
} // end of function 


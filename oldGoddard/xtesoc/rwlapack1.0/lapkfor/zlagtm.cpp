/*
 * C++ implementation of Lapack routine zlagtm
 *
 * $Id: zlagtm.cpp,v 1.3 1993/07/21 22:26:56 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:48:18
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zlagtm.cpp,v $
 * Revision 1.3  1993/07/21  22:26:56  alv
 * ported to Microsoft C++ v8
 *
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ONE = 1.0e0;
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zlagtm(const char &trans, const long &n, const long &nrhs, 
 const double &alpha, DComplex dl[], DComplex d[], DComplex du[], DComplex *x, 
 const long &ldx, const double &beta, DComplex *b, const long &ldb)
{
#define X(I_,J_)  (*(x+(I_)*(ldx)+(J_)))
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
  long _do0, _do1, _do10, _do11, _do12, _do13, _do14, _do15, 
   _do2, _do3, _do4, _do5, _do6, _do7, _do8, _do9, i, i_, j, j_;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZLAGTM performs a matrix-vector product of the form
  
  //     B := alpha * A * X + beta * B
  
  //  where A is a tridiagonal matrix of order N, B and X are N by NRHS
  //  matrices, and alpha and beta are real scalars, each of which may be
  //  0., 1., or -1.
  
  //  Arguments
  //  =========
  
  //  TRANS   (input) CHARACTER
  //          Specifies the operation applied to A.
  //          = 'N':  No transpose, B := alpha * A * X + beta * B
  //          = 'T':  Transpose,    B := alpha * A**T * X + beta * B
  //          = 'C':  Conjugate transpose, B := alpha * A**H * X + beta * B
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right hand sides, i.e., the number of columns
  //          of the matrices X and B.
  
  //  ALPHA   (input) DOUBLE PRECISION
  //          The scalar alpha.  ALPHA must be 0., 1., or -1.; otherwise,
  //          it is assumed to be 0.
  
  //  DL      (input) COMPLEX*16 array, dimension (N-1)
  //          The (n-1) sub-diagonal elements of T.
  
  //  D       (input) COMPLEX*16 array, dimension (N)
  //          The diagonal elements of T.
  
  //  DU      (input) COMPLEX*16 array, dimension (N-1)
  //          The (n-1) super-diagonal elements of T.
  
  //  X       (input) COMPLEX*16 array, dimension (LDX,NRHS)
  //          The N by NRHS matrix X.
  //  LDX     (input) INTEGER
  //          The leading dimension of the array X.  LDX >= max(N,1).
  
  //  BETA    (input) DOUBLE PRECISION
  //          The scalar beta.  BETA must be 0., 1., or -1.; otherwise,
  //          it is assumed to be 1.
  
  //  B       (input/output) COMPLEX*16 array, dimension (LDB,NRHS)
  //          On entry, the N by NRHS matrix B.
  //          On exit, B is overwritten by the matrix expression
  //          B := alpha * A * X + beta * B.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(N,1).
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  if( n == 0 ) 
    return;
  
  //     Multiply B by BETA if BETA.NE.1.
  
  if( beta == ZERO ) { 
    for( j = 1, j_ = j - 1, _do0 = nrhs; j <= _do0; j++, j_++ ) { 
      for( i = 1, i_ = i - 1, _do1 = n; i <= _do1; i++, i_++ ) { 
        B(j_,i_) = (DComplex)(ZERO);
      }
    }
  }
  else if( beta == -ONE ) { 
    for( j = 1, j_ = j - 1, _do2 = nrhs; j <= _do2; j++, j_++ ) { 
      for( i = 1, i_ = i - 1, _do3 = n; i <= _do3; i++, i_++ ) { 
        B(j_,i_) = -(B(j_,i_));
      }
    }
  }
  
  if( alpha == ONE ) { 
    if( lsame( trans, 'N' ) ) { 
      
      //           Compute B := B + A*X
      
      for( j = 1, j_ = j - 1, _do4 = nrhs; j <= _do4; j++, j_++ ) { 
        if( n == 1 ) { 
          B(j_,0) = B(j_,0) + d[0]*X(j_,0);
        }
        else { 
          B(j_,0) = B(j_,0) + d[0]*X(j_,0) + du[0]*X(j_,1);
          B(j_,n - 1) = B(j_,n - 1) + dl[n - 2]*X(j_,n - 2) + 
           d[n - 1]*X(j_,n - 1);
          for( i = 2, i_ = i - 1, _do5 = n - 1; i <= _do5; i++, i_++ ) { 
            B(j_,i_) = B(j_,i_) + dl[i_ - 1]*X(j_,i_ - 1) + 
             d[i_]*X(j_,i_) + du[i_]*X(j_,i_ + 1);
          }
        }
      }
    }
    else if( lsame( trans, 'T' ) ) { 
      
      //           Compute B := B + A**T * X
      
      for( j = 1, j_ = j - 1, _do6 = nrhs; j <= _do6; j++, j_++ ) { 
        if( n == 1 ) { 
          B(j_,0) = B(j_,0) + d[0]*X(j_,0);
        }
        else { 
          B(j_,0) = B(j_,0) + d[0]*X(j_,0) + dl[0]*X(j_,1);
          B(j_,n - 1) = B(j_,n - 1) + du[n - 2]*X(j_,n - 2) + 
           d[n - 1]*X(j_,n - 1);
          for( i = 2, i_ = i - 1, _do7 = n - 1; i <= _do7; i++, i_++ ) { 
            B(j_,i_) = B(j_,i_) + du[i_ - 1]*X(j_,i_ - 1) + 
             d[i_]*X(j_,i_) + dl[i_]*X(j_,i_ + 1);
          }
        }
      }
    }
    else if( lsame( trans, 'C' ) ) { 
      
      //           Compute B := B + A**H * X
      
      for( j = 1, j_ = j - 1, _do8 = nrhs; j <= _do8; j++, j_++ ) { 
        if( n == 1 ) { 
          B(j_,0) = B(j_,0) + conj( d[0] )*X(j_,0);
        }
        else { 
          B(j_,0) = B(j_,0) + conj( d[0] )*X(j_,0) + conj( dl[0] )*
           X(j_,1);
   	  // temporaries put in for Microsoft compiler
          DComplex temp1 = conj( du[n - 2] )*X(j_,n - 2);
          DComplex temp2 = conj( d[n - 1] )*X(j_,n - 1);
          B(j_,n - 1) = B(j_,n - 1) + temp1 + temp2;
          for( i = 2, i_ = i - 1, _do9 = n - 1; i <= _do9; i++, i_++ ) { 
     	    // temporaries put in for Microsoft compiler
            DComplex temp1 = conj( du[i_ - 1] )*X(j_,i_ - 1);
            DComplex temp2 = conj( d[i_] )*X(j_,i_);
            B(j_,i_) = B(j_,i_) + temp1 + temp2 + conj( dl[i_] )*X(j_,i_ + 1);
          }
        }
      }
    }
  }
  else if( alpha == -ONE ) { 
    if( lsame( trans, 'N' ) ) { 
      
      //           Compute B := B - A*X
      
      for( j = 1, j_ = j - 1, _do10 = nrhs; j <= _do10; j++, j_++ ) { 
        if( n == 1 ) { 
          B(j_,0) = B(j_,0) - d[0]*X(j_,0);
        }
        else { 
          B(j_,0) = B(j_,0) - d[0]*X(j_,0) - du[0]*X(j_,1);
          B(j_,n - 1) = B(j_,n - 1) - dl[n - 2]*X(j_,n - 2) - 
           d[n - 1]*X(j_,n - 1);
          for( i = 2, i_ = i - 1, _do11 = n - 1; i <= _do11; i++, i_++ ) { 
            B(j_,i_) = B(j_,i_) - dl[i_ - 1]*X(j_,i_ - 1) - 
             d[i_]*X(j_,i_) - du[i_]*X(j_,i_ + 1);
          }
        }
      }
    }
    else if( lsame( trans, 'T' ) ) { 
      
      //           Compute B := B - A'*X
      
      for( j = 1, j_ = j - 1, _do12 = nrhs; j <= _do12; j++, j_++ ) { 
        if( n == 1 ) { 
          B(j_,0) = B(j_,0) - d[0]*X(j_,0);
        }
        else { 
          B(j_,0) = B(j_,0) - d[0]*X(j_,0) - dl[0]*X(j_,1);
          B(j_,n - 1) = B(j_,n - 1) - du[n - 2]*X(j_,n - 2) - 
           d[n - 1]*X(j_,n - 1);
          for( i = 2, i_ = i - 1, _do13 = n - 1; i <= _do13; i++, i_++ ) { 
            B(j_,i_) = B(j_,i_) - du[i_ - 1]*X(j_,i_ - 1) - 
             d[i_]*X(j_,i_) - dl[i_]*X(j_,i_ + 1);
          }
        }
      }
    }
    else if( lsame( trans, 'C' ) ) { 
      
      //           Compute B := B - A'*X
      
      for( j = 1, j_ = j - 1, _do14 = nrhs; j <= _do14; j++, j_++ ) { 
        if( n == 1 ) { 
          B(j_,0) = B(j_,0) - conj( d[0] )*X(j_,0);
        }
        else { 
          B(j_,0) = B(j_,0) - conj( d[0] )*X(j_,0) - conj( dl[0] )*
           X(j_,1);
	  DComplex temp1 = conj( d[n - 1] )*X(j_,n - 1);
          B(j_,n - 1) = B(j_,n - 1) - conj( du[n - 2] )*
           X(j_,n - 2) - temp1; 
          for( i = 2, i_ = i - 1, _do15 = n - 1; i <= _do15; i++, i_++ ) { 
            DComplex temp1 = conj( d[i_] )*X(j_,i_);
            DComplex temp2 = conj( dl[i_] )*X(j_,i_ + 1);
            B(j_,i_) = B(j_,i_) - conj( du[i_ - 1] )*X(j_,i_ - 1) - temp1 - temp2;
          }
        }
      }
    }
  }
  return;
  
  //     End of ZLAGTM
  
#undef  X
#undef  B
} // end of function 


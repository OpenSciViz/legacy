/*
 * C++ implementation of Lapack routine sgbequ
 *
 * $Id: sgbequ.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:58:04
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: sgbequ.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ONE = 1.0e0;
const float ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ sgbequ(const long &m, const long &n, const long &kl, const long &ku, 
 float *ab, const long &ldab, float r[], float c[], float &rowcnd, float &colcnd, 
 float &amax, long &info)
{
#define AB(I_,J_) (*(ab+(I_)*(ldab)+(J_)))
  long _do0, _do1, _do10, _do11, _do2, _do3, _do4, _do5, _do6, 
   _do7, _do8, _do9, i, i_, j, j_, kd;
  float bignum, rcmax, rcmin, smlnum;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SGBEQU computes row and column scalings intended to equilibrate an
  //  M by N band matrix A and reduce its condition number.  R returns the
  //  row scale factors and C the column scale factors, chosen to try to
  //  make the largest entry in each row and column of the matrix B with
  //  entries B(i,j)=R(i)*A(i,j)*C(j) have absolute value 1.  R(i) and C(j)
  //  are restricted to be between SMLNUM = smallest safe number and
  //  BIGNUM = largest safe number.
  
  //  ROWCND returns the ratio of the smallest R(i) to the largest R(i).
  //  If ROWCND >= 0.1 and AMAX is neither too large nor too small, it is
  //  not worth scaling by R.
  
  //  COLCND returns the ratio of the smallest C(i) to the largest C(i).
  //  If COLCND >= 0.1, it is not worth scaling by C.
  
  //  AMAX returns the absolute value of the largest matrix entry.
  //  If AMAX is very close to overflow or very close to underflow, the
  //  matrix should be scaled.
  
  //  Use of these scaling factors is not guaranteed to reduce the
  //  condition number of A but works well in practice.
  
  //  Arguments
  //  =========
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix A.  M >= 0.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix A.  N >= 0.
  
  //  KL      (input) INTEGER
  //          The number of subdiagonals within the band of A.  KL >= 0.
  
  //  KU      (input) INTEGER
  //          The number of superdiagonals within the band of A.  KU >= 0.
  
  //  AB      (input) REAL array, dimension (LDAB,N)
  //          The band matrix A, stored in rows 1 to KL+KU+1.  The j-th
  //          column of A is stored in the j-th column of the array AB as
  //          follows:
  //          AB(ku+1+i-j,j) = A(i,j) for max(1,j-ku)<=i<=min(m,j+kl).
  
  //  LDAB    (input) INTEGER
  //          The leading dimension of the array AB.  LDAB >= KL+KU+1.
  
  //  R       (output) REAL array, dimension (M)
  //          The row scale factors for A.  Unassigned if INFO > 0.
  
  //  C       (output) REAL array, dimension (N)
  //          The column scale factors for A.  Unassigned if INFO > 0.
  
  //  ROWCND  (output) REAL
  //          Ratio of the smallest R(i) to the largest R(i).
  //          Unassigned if 0 < INFO <= M.
  
  //  COLCND  (output) REAL
  //          Ratio of the smallest C(i) to the largest C(i).
  //          Unassigned if INFO > 0.
  
  //  AMAX    (output) REAL
  //          Absolute value of largest matrix entry.
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: if INFO = k, k<=M, the k-th row of A is exactly zero
  //               if INFO = k, k>M,  the k-M-th column of A is exactly
  //               zero
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters
  
  info = 0;
  if( m < 0 ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( kl < 0 ) { 
    info = -3;
  }
  else if( ku < 0 ) { 
    info = -4;
  }
  else if( ldab < kl + ku + 1 ) { 
    info = -6;
  }
  if( info != 0 ) { 
    xerbla( "SGBEQU", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( m == 0 || n == 0 ) { 
    rowcnd = ONE;
    colcnd = ONE;
    amax = ZERO;
    return;
  }
  
  //     Get machine constants.
  
  smlnum = slamch( 'S' );
  bignum = ONE/smlnum;
  
  //     Compute row scale factors.
  
  for( i = 1, i_ = i - 1, _do0 = m; i <= _do0; i++, i_++ ) { 
    r[i_] = ZERO;
  }
  
  //     Find the maximum element in each row.
  
  kd = ku + 1;
  for( j = 1, j_ = j - 1, _do1 = n; j <= _do1; j++, j_++ ) { 
    for( i = max( j - ku, 1 ), i_ = i - 1, _do2 = min( j + kl, 
     m ); i <= _do2; i++, i_++ ) { 
      r[i_] = max( r[i_], abs( AB(j_,kd + i_ - j) ) );
    }
  }
  
  //     Find the maximum and minimum scale factors.
  
  rcmin = bignum;
  rcmax = ZERO;
  for( i = 1, i_ = i - 1, _do3 = m; i <= _do3; i++, i_++ ) { 
    rcmax = max( rcmax, r[i_] );
    rcmin = min( rcmin, r[i_] );
  }
  amax = rcmax;
  
  if( rcmin == ZERO ) { 
    
    //        Find the first zero scale factor and return an error code.
    
    for( i = 1, i_ = i - 1, _do4 = m; i <= _do4; i++, i_++ ) { 
      if( r[i_] == ZERO ) { 
        info = i;
        return;
      }
    }
  }
  else { 
    
    //        Invert the scale factors.
    
    for( i = 1, i_ = i - 1, _do5 = m; i <= _do5; i++, i_++ ) { 
      r[i_] = ONE/min( max( r[i_], smlnum ), bignum );
    }
    
    //        Compute ROWCND = min(R(I)) / max(R(I))
    
    rowcnd = max( rcmin, smlnum )/min( rcmax, bignum );
  }
  
  //     Compute column scale factors
  
  for( j = 1, j_ = j - 1, _do6 = n; j <= _do6; j++, j_++ ) { 
    c[j_] = ZERO;
  }
  
  //     Find the maximum element in each column,
  //     assuming the row scaling computed above.
  
  kd = ku + 1;
  for( j = 1, j_ = j - 1, _do7 = n; j <= _do7; j++, j_++ ) { 
    for( i = max( j - ku, 1 ), i_ = i - 1, _do8 = min( j + kl, 
     m ); i <= _do8; i++, i_++ ) { 
      c[j_] = max( c[j_], abs( AB(j_,kd + i_ - j) )*r[i_] );
    }
  }
  
  //     Find the maximum and minimum scale factors.
  
  rcmin = bignum;
  rcmax = ZERO;
  for( j = 1, j_ = j - 1, _do9 = n; j <= _do9; j++, j_++ ) { 
    rcmin = min( rcmin, c[j_] );
    rcmax = max( rcmax, c[j_] );
  }
  
  if( rcmin == ZERO ) { 
    
    //        Find the first zero scale factor and return an error code.
    
    for( j = 1, j_ = j - 1, _do10 = n; j <= _do10; j++, j_++ ) { 
      if( c[j_] == ZERO ) { 
        info = m + j;
        return;
      }
    }
  }
  else { 
    
    //        Invert the scale factors.
    
    for( j = 1, j_ = j - 1, _do11 = n; j <= _do11; j++, j_++ ) { 
      c[j_] = ONE/min( max( c[j_], smlnum ), bignum );
    }
    
    //        Compute COLCND = min(C(J)) / max(C(J))
    
    colcnd = max( rcmin, smlnum )/min( rcmax, bignum );
  }
  
  return;
  
  //     End of SGBEQU
  
#undef  AB
} // end of function 


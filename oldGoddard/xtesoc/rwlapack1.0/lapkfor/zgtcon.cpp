/*
 * C++ implementation of Lapack routine zgtcon
 *
 * $Id: zgtcon.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:47:01
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zgtcon.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ONE = 1.0e0;
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zgtcon(const char &norm, const long &n, DComplex dl[], DComplex d[], 
 DComplex du[], DComplex du2[], long ipiv[], const double &anorm, double &rcond, 
 DComplex work[], long &info)
{
  int onenrm;
  long _do0, i, i_, kase, kase1;
  double ainvnm;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZGTCON estimates the reciprocal of the condition number of a DComplex
  //  tridiagonal matrix A using the LU factorization as computed by
  //  ZGTTRF.
  
  //  An estimate is obtained for norm(inv(A)), and the reciprocal of the
  //  condition number is computed as RCOND = 1 / (ANORM * norm(inv(A))).
  
  //  Arguments
  //  =========
  
  //  NORM    (input) CHARACTER*1
  //          Specifies whether the 1-norm condition number or the
  //          infinity-norm condition number is required:
  //          = '1' or 'O':  1-norm
  //          = 'I':         Infinity-norm
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  DL      (input) COMPLEX*16 array, dimension (N-1)
  //          The (n-1) multipliers that define the matrix L from the
  //          LU factorization of A as computed by ZGTTRF.
  
  //  D       (input) COMPLEX*16 array, dimension (N)
  //          The n diagonal elements of the upper triangular matrix U from
  //          the LU factorization of A.
  
  //  DU      (input) COMPLEX*16 array, dimension (N-1)
  //          The (n-1) elements of the first super-diagonal of U.
  
  //  DU2     (input) COMPLEX*16 array, dimension (N-2)
  //          The (n-2) elements of the second super-diagonal of U.
  
  //  IPIV    (input) INTEGER array, dimension (N)
  //          The pivot indices; for 1 <= i <= n, row i of the matrix was
  //          interchanged with row IPIV(i).  IPIV(i) will always be either
  //          i or i+1; IPIV(i) = i indicates a row interchange was not
  //          required.
  
  //  ANORM   (input) DOUBLE PRECISION
  //          The 1-norm of the original matrix A.
  
  //  RCOND   (output) DOUBLE PRECISION
  //          The reciprocal of the condition number of the matrix A,
  //          computed as RCOND = 1/(ANORM * AINVNM), where AINVNM is an
  //          estimate of the 1-norm of inv(A) computed in this routine.
  
  //  WORK    (workspace) COMPLEX*16 array, dimension (N)
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input arguments.
  
  info = 0;
  onenrm = norm == '1' || lsame( norm, 'O' );
  if( !onenrm && !lsame( norm, 'I' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( anorm < ZERO ) { 
    info = -8;
  }
  if( info != 0 ) { 
    xerbla( "ZGTCON", -info );
    return;
  }
  
  //     Quick return if possible
  
  rcond = ZERO;
  if( n == 0 ) { 
    rcond = ONE;
    return;
  }
  else if( anorm == ZERO ) { 
    return;
  }
  
  //     Check that D(1:N) is non-zero.
  
  for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
    if( ctocf(d[i_]) == ctocf(DComplex( ZERO, 0. )) ) 
      return;
  }
  
  ainvnm = ZERO;
  if( onenrm ) { 
    kase1 = 1;
  }
  else { 
    kase1 = 2;
  }
  kase = 0;
L_20:
  ;
  zlacon( n, &work[n], work, ainvnm, kase );
  if( kase != 0 ) { 
    if( kase == kase1 ) { 
      
      //           Multiply by inv(U)*inv(L).
      
      zgttrs( 'N'/*No transpose*/, n, 1, dl, d, du, du2, ipiv, 
       work, n, info );
    }
    else { 
      
      //           Multiply by inv(L')*inv(U').
      
      zgttrs( 'C'/*Conjugate transpose*/, n, 1, dl, d, du, 
       du2, ipiv, work, n, info );
    }
    goto L_20;
  }
  
  //     Compute the estimate of the reciprocal condition number.
  
  if( ainvnm != ZERO ) 
    rcond = (ONE/ainvnm)/anorm;
  
  return;
  
  //     End of ZGTCON
  
} // end of function 


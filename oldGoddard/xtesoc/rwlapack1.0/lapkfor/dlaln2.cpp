/*
 * C++ implementation of lapack routine dlaln2
 *
 * $Id: dlaln2.cpp,v 1.8 1993/04/06 20:41:01 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:35:19
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dlaln2.cpp,v $
 * Revision 1.8  1993/04/06  20:41:01  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.7  1993/03/19  19:28:59  alv
 * fixed constness of declaration
 *
 * Revision 1.6  1993/03/19  18:41:23  alv
 * now passes chars explicitly, rather than indirection of a string, to shut up SUN warnings
 *
 * Revision 1.5  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.4  1993/03/19  16:57:22  alv
 * sprinkled in some const
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:15:22  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:07:14  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
const double TWO = 2.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dlaln2(const int &ltrans, const long &na, const long &nw, 
 const double &smin, const double &ca, double *a, const long &lda, const double &d1, const double &d2, 
 double *b, const long &ldb, const double &wr, const double &wi, double *x, const long &ldx, 
 double &scale, double &xnorm, long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
#define X(I_,J_)  (*(x+(I_)*(ldx)+(J_)))
  long icmax, j, j_;
  double bbnd, bi1, bi2, bignum, bnorm, br1, br2, ci21, ci22, cmax, 
   cnorm, cr21, cr22, csi, csr, li21, lr21, smini, smlnum, temp, 
   u22abs, ui11, ui11r, ui12, ui12s, ui22, ur11, ur11r, ur12, ur12s, 
   ur22, xi1, xi2, xr1, xr2;
  static int zswap[4]={FALSE,FALSE,TRUE,TRUE};
  static int rswap[4]={FALSE,TRUE,FALSE,TRUE};
  static long ipivot[4][4]={1,2,3,4,2,1,4,3,3,4,1,2,4,3,2,1};
  // EQUIVALENCE translations
  double _e1[4], _e0[4];
  double (*const ci)[2] = (double(*)[2])_e0;
  double *const civ = (double*)_e0;
  double (*const cr)[2] = (double(*)[2])_e1;
  double *const crv = (double*)_e1;
  // end of EQUIVALENCE translations

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DLALN2 solves a system of the form  (ca A - w D ) X = s B
  //  or (ca A' - w D) X = s B   with possible scaling ("s") and
  //  perturbation of A.  (A' means A-transpose.)
  
  //  A is an NA x NA real matrix, ca is a real scalar, D is an NA x NA
  //  real diagonal matrix, w is a real or DComplex value, and X and B are
  //  NA x 1 matrices -- real if w is real, DComplex if w is DComplex.  NA
  //  may be 1 or 2.
  
  //  If w is DComplex, X and B are represented as NA x 2 matrices,
  //  the first column of each being the real part and the second
  //  being the imaginary part.
  
  //  "s" is a scaling factor (.LE. 1), computed by DLALN2, which is
  //  so chosen that X can be computed without overflow.  X is further
  //  scaled if necessary to assure that norm(ca A - w D)*norm(X) is less
  //  than overflow.
  
  //  If both singular values of (ca A - w D) are less than SMIN,
  //  SMIN*identity will be used instead of (ca A - w D).  If only one
  //  singular value is less than SMIN, one element of (ca A - w D) will be
  //  perturbed enough to make the smallest singular value roughly SMIN.
  //  If both singular values are at least SMIN, (ca A - w D) will not be
  //  perturbed.  In any case, the perturbation will be at most some small
  //  multiple of max( SMIN, ulp*norm(ca A - w D) ).  The singular values
  //  are computed by infinity-norm approximations, and thus will only be
  //  correct to a factor of 2 or so.
  
  //  Note: all input quantities are assumed to be smaller than overflow
  //  by a reasonable factor.  (See BIGNUM.)
  
  //  Arguments
  //  ==========
  
  //  LTRANS  (input) LOGICAL
  //          =.TRUE.:  A-transpose will be used.
  //          =.FALSE.: A will be used (not transposed.)
  
  //  NA      (input) INTEGER
  //          The size of the matrix A.  It may (only) be 1 or 2.
  
  //  NW      (input) INTEGER
  //          1 if "w" is real, 2 if "w" is DComplex.  It may only be 1
  //          or 2.
  
  //  SMIN    (input) DOUBLE PRECISION
  //          The desired lower bound on the singular values of A.  This
  //          should be a safe distance away from underflow or overflow,
  //          say, between (underflow/machine precision) and  (machine
  //          precision * overflow ).  (See BIGNUM and ULP.)
  
  //  CA      (input) DOUBLE PRECISION
  //          The coefficient c, which A is multiplied by.
  
  //  A       (input) DOUBLE PRECISION array, dimension (LDA,NA)
  //          The NA x NA matrix A.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of A.  It must be at least NA.
  
  //  D1      (input) DOUBLE PRECISION
  //          The 1,1 entry in the diagonal matrix D.
  
  //  D2      (input) DOUBLE PRECISION
  //          The 2,2 entry in the diagonal matrix D.  Not used if NW=1.
  
  //  B       (input) DOUBLE PRECISION array, dimension (LDB,NW)
  //          The NA x NW matrix B (right-hand side).  If NW=2 ("w" is
  //          DComplex), column 1 contains the real part of B and column 2
  //          contains the imaginary part.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of B.  It must be at least NA.
  
  //  WR      (input) DOUBLE PRECISION
  //          The real part of the scalar "w".
  
  //  WI      (input) DOUBLE PRECISION
  //          The imaginary part of the scalar "w".  Not used if NW=1.
  
  //  X       (output) DOUBLE PRECISION array, dimension (LDX,NW)
  //          The NA x NW matrix X (unknowns), as computed by DLALN2.
  //          If NW=2 ("w" is DComplex), on exit, column 1 will contain
  //          the real part of X and column 2 will contain the imaginary
  //          part.
  
  //  LDX     (input) INTEGER
  //          The leading dimension of X.  It must be at least NA.
  
  //  SCALE   (output) DOUBLE PRECISION
  //          The scale factor that B must be multiplied by to insure
  //          that overflow does not occur when computing X.  Thus,
  //          (ca A - w D) X  will be SCALE*B, not B (ignoring
  //          perturbations of A.)  It will be at most 1.
  
  //  XNORM   (output) DOUBLE PRECISION
  //          The infinity-norm of X, when X is regarded as an NA x NW
  //          real matrix.
  
  //  INFO    (output) INTEGER
  //          An error flag.  It will be set to zero if no error occurs,
  //          a negative number if an argument is in error, or a positive
  //          number if  ca A - w D  had to be perturbed.
  //          The possible values are:
  //          = 0: No error occurred, and (ca A - w D) did not have to be
  //                 perturbed.
  //          = 1: (ca A - w D) had to be perturbed to make its smallest
  //               (or only) singular value greater than SMIN.
  //          NOTE: In the interests of speed, this routine does not
  //                check the inputs for errors.
  
  //-----------------------------------------------------------------------
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Local Arrays ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Equivalences ..
  //     ..
  //     .. Data statements ..
  //     ..
  //     .. Executable Statements ..
  
  //     Compute BIGNUM
  
  smlnum = TWO*dlamch( 'S'/* Safe minimum */ );
  bignum = ONE/smlnum;
  smini = max( smin, smlnum );
  
  //     Don't check for input errors
  
  info = 0;
  
  //     Standard Initializations
  
  scale = ONE;
  
  if( na == 1 ) { 
    
    //        1 x 1  (i.e., scalar) system   C X = B
    
    if( nw == 1 ) { 
      
      //           Real 1x1 system.
      
      //           C = ca A - w D
      
      csr = ca*A(0,0) - wr*d1;
      cnorm = abs( csr );
      
      //           If | C | < SMINI, use C = SMINI
      
      if( cnorm < smini ) { 
        csr = smini;
        cnorm = smini;
        info = 1;
      }
      
      //           Check scaling for  X = B / C
      
      bnorm = abs( B(0,0) );
      if( cnorm < ONE && bnorm > ONE ) { 
        if( bnorm > bignum*cnorm ) 
          scale = ONE/bnorm;
      }
      
      //           Compute X
      
      X(0,0) = (B(0,0)*scale)/csr;
      xnorm = abs( X(0,0) );
    }
    else { 
      
      //           Complex 1x1 system (w is DComplex)
      
      //           C = ca A - w D
      
      csr = ca*A(0,0) - wr*d1;
      csi = -wi*d1;
      cnorm = abs( csr ) + abs( csi );
      
      //           If | C | < SMINI, use C = SMINI
      
      if( cnorm < smini ) { 
        csr = smini;
        csi = ZERO;
        cnorm = smini;
        info = 1;
      }
      
      //           Check scaling for  X = B / C
      
      bnorm = abs( B(0,0) ) + abs( B(1,0) );
      if( cnorm < ONE && bnorm > ONE ) { 
        if( bnorm > bignum*cnorm ) 
          scale = ONE/bnorm;
      }
      
      //           Compute X
      
      dladiv( scale*B(0,0), scale*B(1,0), csr, csi, X(0,0), 
       X(1,0) );
      xnorm = abs( X(0,0) ) + abs( X(1,0) );
    }
    
  }
  else { 
    
    //        2x2 System
    
    //        Compute the real part of  C = ca A - w D  (or  ca A' - w D )
    
    cr[0][0] = ca*A(0,0) - wr*d1;
    cr[1][1] = ca*A(1,1) - wr*d2;
    if( ltrans ) { 
      cr[1][0] = ca*A(0,1);
      cr[0][1] = ca*A(1,0);
    }
    else { 
      cr[0][1] = ca*A(0,1);
      cr[1][0] = ca*A(1,0);
    }
    
    if( nw == 1 ) { 
      
      //           Real 2x2 system  (w is real)
      
      //           Find the largest entry in C
      
      cmax = ZERO;
      icmax = 0;
      
      for( j = 1, j_ = j - 1; j <= 4; j++, j_++ ) { 
        if( abs( crv[j_] ) > cmax ) { 
          cmax = abs( crv[j_] );
          icmax = j;
        }
      }
      
      //           If norm(C) < SMINI, use SMINI*identity.
      
      if( cmax < smini ) { 
        bnorm = max( abs( B(0,0) ), abs( B(0,1) ) );
        if( smini < ONE && bnorm > ONE ) { 
          if( bnorm > bignum*smini ) 
            scale = ONE/bnorm;
        }
        temp = scale/smini;
        X(0,0) = temp*B(0,0);
        X(0,1) = temp*B(0,1);
        xnorm = temp*bnorm;
        info = 1;
        return;
      }
      
      //           Gaussian elimination with complete pivoting.
      
      ur11 = crv[icmax - 1];
      cr21 = crv[ipivot[icmax - 1][1] - 1];
      ur12 = crv[ipivot[icmax - 1][2] - 1];
      cr22 = crv[ipivot[icmax - 1][3] - 1];
      ur11r = ONE/ur11;
      lr21 = ur11r*cr21;
      ur22 = cr22 - ur12*lr21;
      
      //           If smaller pivot < SMINI, use SMINI
      
      if( abs( ur22 ) < smini ) { 
        ur22 = smini;
        info = 1;
      }
      if( rswap[icmax - 1] ) { 
        br1 = B(0,1);
        br2 = B(0,0);
      }
      else { 
        br1 = B(0,0);
        br2 = B(0,1);
      }
      br2 = br2 - lr21*br1;
      bbnd = max( abs( br1*(ur22*ur11r) ), abs( br2 ) );
      if( bbnd > ONE && abs( ur22 ) < ONE ) { 
        if( bbnd >= bignum*abs( ur22 ) ) 
          scale = ONE/bbnd;
      }
      
      xr2 = (br2*scale)/ur22;
      xr1 = (scale*br1)*ur11r - xr2*(ur11r*ur12);
      if( zswap[icmax - 1] ) { 
        X(0,0) = xr2;
        X(0,1) = xr1;
      }
      else { 
        X(0,0) = xr1;
        X(0,1) = xr2;
      }
      xnorm = max( abs( xr1 ), abs( xr2 ) );
      
      //           Further scaling if  norm(A) norm(X) > overflow
      
      if( xnorm > ONE && cmax > ONE ) { 
        if( xnorm > bignum/cmax ) { 
          temp = cmax/bignum;
          X(0,0) = temp*X(0,0);
          X(0,1) = temp*X(0,1);
          xnorm = temp*xnorm;
          scale = temp*scale;
        }
      }
    }
    else { 
      
      //           Complex 2x2 system  (w is DComplex)
      
      //           Find the largest entry in C
      
      ci[0][0] = -wi*d1;
      ci[0][1] = ZERO;
      ci[1][0] = ZERO;
      ci[1][1] = -wi*d2;
      cmax = ZERO;
      icmax = 0;
      
      for( j = 1, j_ = j - 1; j <= 4; j++, j_++ ) { 
        if( abs( crv[j_] ) + abs( civ[j_] ) > cmax ) { 
          cmax = abs( crv[j_] ) + abs( civ[j_] );
          icmax = j;
        }
      }
      
      //           If norm(C) < SMINI, use SMINI*identity.
      
      if( cmax < smini ) { 
        bnorm = max( abs( B(0,0) ) + abs( B(1,0) ), abs( B(0,1) ) + 
         abs( B(1,1) ) );
        if( smini < ONE && bnorm > ONE ) { 
          if( bnorm > bignum*smini ) 
            scale = ONE/bnorm;
        }
        temp = scale/smini;
        X(0,0) = temp*B(0,0);
        X(0,1) = temp*B(0,1);
        X(1,0) = temp*B(1,0);
        X(1,1) = temp*B(1,1);
        xnorm = temp*bnorm;
        info = 1;
        return;
      }
      
      //           Gaussian elimination with complete pivoting.
      
      ur11 = crv[icmax - 1];
      ui11 = civ[icmax - 1];
      cr21 = crv[ipivot[icmax - 1][1] - 1];
      ci21 = civ[ipivot[icmax - 1][1] - 1];
      ur12 = crv[ipivot[icmax - 1][2] - 1];
      ui12 = civ[ipivot[icmax - 1][2] - 1];
      cr22 = crv[ipivot[icmax - 1][3] - 1];
      ci22 = civ[ipivot[icmax - 1][3] - 1];
      if( icmax == 1 || icmax == 4 ) { 
        
        //              Code when off-diagonals of pivoted C are real
        
        if( abs( ur11 ) > abs( ui11 ) ) { 
          temp = ui11/ur11;
          ur11r = ONE/(ur11*(ONE + pow(temp, 2)));
          ui11r = -temp*ur11r;
        }
        else { 
          temp = ur11/ui11;
          ui11r = -ONE/(ui11*(ONE + pow(temp, 2)));
          ur11r = -temp*ui11r;
        }
        lr21 = cr21*ur11r;
        li21 = cr21*ui11r;
        ur12s = ur12*ur11r;
        ui12s = ur12*ui11r;
        ur22 = cr22 - ur12*lr21;
        ui22 = ci22 - ur12*li21;
      }
      else { 
        
        //              Code when diagonals of pivoted C are real
        
        ur11r = ONE/ur11;
        ui11r = ZERO;
        lr21 = cr21*ur11r;
        li21 = ci21*ur11r;
        ur12s = ur12*ur11r;
        ui12s = ui12*ur11r;
        ur22 = cr22 - ur12*lr21 + ui12*li21;
        ui22 = -ur12*li21 - ui12*lr21;
      }
      u22abs = abs( ur22 ) + abs( ui22 );
      
      //           If smaller pivot < SMINI, use SMINI
      
      if( u22abs < smini ) { 
        ur22 = smini;
        ui22 = ZERO;
        info = 1;
      }
      if( rswap[icmax - 1] ) { 
        br2 = B(0,0);
        br1 = B(0,1);
        bi2 = B(1,0);
        bi1 = B(1,1);
      }
      else { 
        br1 = B(0,0);
        br2 = B(0,1);
        bi1 = B(1,0);
        bi2 = B(1,1);
      }
      br2 = br2 - lr21*br1 + li21*bi1;
      bi2 = bi2 - li21*br1 - lr21*bi1;
      bbnd = max( (abs( br1 ) + abs( bi1 ))*(u22abs*(abs( ur11r ) + 
       abs( ui11r ))), abs( br2 ) + abs( bi2 ) );
      if( bbnd > ONE && u22abs < ONE ) { 
        if( bbnd >= bignum*u22abs ) { 
          scale = ONE/bbnd;
          br1 = scale*br1;
          bi1 = scale*bi1;
          br2 = scale*br2;
          bi2 = scale*bi2;
        }
      }
      
      dladiv( br2, bi2, ur22, ui22, xr2, xi2 );
      xr1 = ur11r*br1 - ui11r*bi1 - ur12s*xr2 + ui12s*xi2;
      xi1 = ui11r*br1 + ur11r*bi1 - ui12s*xr2 - ur12s*xi2;
      if( zswap[icmax - 1] ) { 
        X(0,0) = xr2;
        X(0,1) = xr1;
        X(1,0) = xi2;
        X(1,1) = xi1;
      }
      else { 
        X(0,0) = xr1;
        X(0,1) = xr2;
        X(1,0) = xi1;
        X(1,1) = xi2;
      }
      xnorm = max( abs( xr1 ) + abs( xi1 ), abs( xr2 ) + abs( xi2 ) );
      
      //           Further scaling if  norm(A) norm(X) > overflow
      
      if( xnorm > ONE && cmax > ONE ) { 
        if( xnorm > bignum/cmax ) { 
          temp = cmax/bignum;
          X(0,0) = temp*X(0,0);
          X(0,1) = temp*X(0,1);
          X(1,0) = temp*X(1,0);
          X(1,1) = temp*X(1,1);
          xnorm = temp*xnorm;
          scale = temp*scale;
        }
      }
    }
  }
  
  return;
  
  //     End of DLALN2
  
#undef  X
#undef  B
#undef  A
} // end of function 


/*
 * C++ implementation of Lapack routine slae2
 *
 * $Id: slae2.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:59:28
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: slae2.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ONE = 1.0e0;
const float TWO = 2.0e0;
const float ZERO = 0.0e0;
const float HALF = 0.5e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ slae2(const float &a, const float &b, const float &c, float &rt1, 
 float &rt2)
{
  float ab, acmn, acmx, adf, df, rt, sm, tb;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SLAE2  computes the eigenvalues of a 2-by-2 symmetric matrix
  //     [  A   B  ]
  //     [  B   C  ].
  //  On return, RT1 is the eigenvalue of larger absolute value, and RT2
  //  is the eigenvalue of smaller absolute value.
  
  //  Arguments
  //  =========
  
  //  A       (input) REAL
  //          The (1,1) entry of the 2-by-2 matrix.
  
  //  B       (input) REAL
  //          The (1,2) and (2,1) entries of the 2-by-2 matrix.
  
  //  C       (input) REAL
  //          The (2,2) entry of the 2-by-2 matrix.
  
  //  RT1     (output) REAL
  //          The eigenvalue of larger absolute value.
  
  //  RT2     (output) REAL
  //          The eigenvalue of smaller absolute value.
  
  //  Further Details
  //  ===============
  
  //  RT1 is accurate to a few ulps barring over/underflow.
  
  //  RT2 may be inaccurate if there is massive cancellation in the
  //  determinant A*C-B*B; higher precision or correctly rounded or
  //  correctly truncated arithmetic would be needed to compute RT2
  //  accurately in all cases.
  
  //  Overflow is possible only if RT1 is within a factor of 5 of overflow.
  //  Underflow is harmless if the input data is 0 or exceeds
  //     underflow_threshold / macheps.
  
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Compute the eigenvalues
  
  sm = a + c;
  df = a - c;
  adf = abs( df );
  tb = b + b;
  ab = abs( tb );
  if( abs( a ) > abs( c ) ) { 
    acmx = a;
    acmn = c;
  }
  else { 
    acmx = c;
    acmn = a;
  }
  if( adf > ab ) { 
    rt = adf*sqrt( ONE + pow(ab/adf, 2) );
  }
  else if( adf < ab ) { 
    rt = ab*sqrt( ONE + pow(adf/ab, 2) );
  }
  else { 
    
    //        Includes case AB=ADF=0
    
    rt = ab*sqrt( TWO );
  }
  if( sm < ZERO ) { 
    rt1 = HALF*(sm - rt);
    
    //        Order of execution important.
    //        To get fully accurate smaller eigenvalue,
    //        next line needs to be executed in higher precision.
    
    rt2 = (acmx/rt1)*acmn - (b/rt1)*b;
  }
  else if( sm > ZERO ) { 
    rt1 = HALF*(sm + rt);
    
    //        Order of execution important.
    //        To get fully accurate smaller eigenvalue,
    //        next line needs to be executed in higher precision.
    
    rt2 = (acmx/rt1)*acmn - (b/rt1)*b;
  }
  else { 
    
    //        Includes case RT1 = RT2 = 0
    
    rt1 = HALF*rt;
    rt2 = -HALF*rt;
  }
  return;
  
  //     End of SLAE2
  
} // end of function 


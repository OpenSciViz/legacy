/*
 * C++ implementation of Lapack routine zhpsv
 *
 * $Id: zhpsv.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:47:51
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zhpsv.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

RWLAPKDECL void /*FUNCTION*/ zhpsv(const char &uplo, const long &n, const long &nrhs, DComplex ap[], 
   long ipiv[], DComplex *b, const long &ldb, long &info)
{
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))

  
  //  -- LAPACK driver routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZHPSV computes the solution to a DComplex system of linear equations
  //     A * X = B,
  //  where A is an N by N Hermitian matrix stored in packed format and X
  //  and B are N by NRHS matrices.
  
  //  The diagonal pivoting method is used to factor A as
  //     A = U * D * U',  if UPLO = 'U', or
  //     A = L * D * L',  if UPLO = 'L',
  //  where U (or L) is a product of permutation and unit upper (lower)
  //  triangular matrices, D is Hermitian and block diagonal with 1-by-1
  //  and 2-by-2 diagonal blocks, and ' indicates conjugate transpose.
  //  The factored form of A is then used to solve the system of equations
  //  A * X = B.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangular part of the
  //          Hermitian matrix A is stored:
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The number of linear equations, i.e., the order of the
  //          matrix A.  N >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right hand sides, i.e., the number of columns
  //          of the matrix B.  NRHS >= 0.
  
  //  AP      (input/output) COMPLEX*16 array, dimension (N*(N+1)/2)
  //          On entry, the upper or lower triangle of the Hermitian matrix
  //          A, packed columnwise in a linear array.  The j-th column of A
  //          is stored in the array AP as follows:
  //          if UPLO = 'U', AP(i + (j-1)*j/2) = A(i,j) for 1<=i<=j;
  //          if UPLO = 'L', AP(i + (j-1)*(2n-j)/2) = A(i,j) for j<=i<=n.
  //          See below for further details.
  
  //          On exit, the block diagonal matrix D and the multipliers used
  //          to obtain the factor U or L from the factorization A = U*D*U'
  //          or A = L*D*L' as computed by ZHPTRF, stored as a packed
  //          triangular matrix in the same storage format as A.
  
  //  IPIV    (output) INTEGER array, dimension (N)
  //          Details of the interchanges and the block structure of D, as
  //          determined by ZHPTRF.  If IPIV(k) > 0, then rows and columns
  //          k and IPIV(k) were interchanged, and D(k,k) is a 1-by-1
  //          diagonal block.  If UPLO = 'U' and IPIV(k) = IPIV(k-1) < 0,
  //          then rows and columns k-1 and -IPIV(k) were interchanged and
  //          D(k-1:k,k-1:k) is a 2-by-2 diagonal block.  If UPLO = 'L' and
  //          IPIV(k) = IPIV(k+1) < 0, then rows and columns k+1 and
  //          -IPIV(k) were interchanged and D(k:k+1,k:k+1) is a 2-by-2
  //          diagonal block.
  
  //  B       (input/output) COMPLEX*16 array, dimension (LDB,NRHS)
  //          On entry, the N by NRHS matrix of right hand side vectors B
  //          for the system of equations A*X = B.
  //          On exit, if INFO = 0, the N by NRHS matrix of solution
  //          vectors X.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: if INFO = k, D(k,k) is exactly zero.  The factorization
  //               has been completed, but the block diagonal matrix D is
  //               exactly singular, so the solution could not be computed.
  
  //  Further Details
  //  ===============
  
  //  The packed storage scheme is illustrated by the following example
  //  when N = 4, UPLO = 'U':
  
  //  Two-dimensional storage of the Hermitian matrix A:
  
  //     a11 a12 a13 a14
  //         a22 a23 a24
  //             a33 a34     (aij = conjg(aji))
  //                 a44
  
  //  Packed storage of the upper triangle of A:
  
  //  AP = [ a11, a12, a22, a13, a23, a33, a14, a24, a34, a44 ]
  
  //  =====================================================================
  
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  if( !lsame( uplo, 'U' ) && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( nrhs < 0 ) { 
    info = -3;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -7;
  }
  if( info != 0 ) { 
    xerbla( "ZHPSV ", -info );
    return;
  }
  
  //     Compute the factorization A = U*D*U' or A = L*D*L'.
  
  zhptrf( uplo, n, ap, ipiv, info );
  if( info == 0 ) { 
    
    //        Solve the system A*X = B, overwriting B with X.
    
    zhptrs( uplo, n, nrhs, ap, ipiv, b, ldb, info );
    
  }
  return;
  
  //     End of ZHPSV
  
#undef  B
} // end of function 


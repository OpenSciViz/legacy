/*
 * C++ implementation of Lapack routine sormhr
 *
 * $Id: sormhr.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:01:24
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: sormhr.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

RWLAPKDECL void /*FUNCTION*/ sormhr(const char &side, const char &trans, const long &m, const long &n, 
   const long &ilo, const long &ihi, float *a, const long &lda, float tau[], 
   float *c, const long &ldc, float work[], const long &lwork, long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define C(I_,J_)  (*(c+(I_)*(ldc)+(J_)))
  int left;
  long i1, i2, iinfo, mi, nh, ni, nq, nw;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SORMHR overwrites the general real m by n matrix C with
  
  //        Q * C  if SIDE = 'L' and TRANS = 'N', or
  
  //        Q'* C  if SIDE = 'L' and TRANS = 'T', or
  
  //        C * Q  if SIDE = 'R' and TRANS = 'N', or
  
  //        C * Q' if SIDE = 'R' and TRANS = 'T',
  
  //  where Q is a real orthogonal matrix of order nq, with nq = m if
  //  SIDE = 'L' and nq = n if SIDE = 'R'. Q is defined as the product of
  //  ihi-ilo elementary reflectors, as returned by SGEHRD:
  
  //  Q = H(ilo) H(ilo+1) . . . H(ihi-1).
  
  //  Arguments
  //  =========
  
  //  SIDE    (input) CHARACTER*1
  //          = 'L': apply Q or Q' from the Left
  //          = 'R': apply Q or Q' from the Right
  
  //  TRANS   (input) CHARACTER*1
  //          = 'N': apply Q  (No transpose)
  //          = 'T': apply Q' (Transpose)
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix C. M >= 0.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix C. N >= 0.
  
  //  ILO     (input) INTEGER
  //  IHI     (input) INTEGER
  //          ILO and IHI must have the same values as in the previous call
  //          of SGEHRD. Q is equal to the unit matrix except in the
  //          submatrix Q(ilo+1:ihi,ilo+1:ihi).
  //          If SIDE = 'L', 1 <= ILO and  min(M,ILO) <= IHI <= M;
  //          if SIDE = 'R', 1 <= ILO and  min(N,ILO) <= IHI <= N;
  
  //  A       (input) REAL array, dimension
  //                               (LDA,M) if SIDE = 'L'
  //                               (LDA,N) if SIDE = 'R'
  //          The vectors which define the elementary reflectors, as
  //          returned by SGEHRD.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.
  //          LDA >= max(1,M) if SIDE = 'L'; LDA >= max(1,N) if SIDE = 'R'.
  
  //  TAU     (input) REAL array, dimension
  //                               (M-1) if SIDE = 'L'
  //                               (N-1) if SIDE = 'R'
  //          TAU(i) must contain the scalar factor of the elementary
  //          reflector H(i), as returned by SGEHRD.
  
  //  C       (input/output) REAL array, dimension (LDC,N)
  //          On entry, the m by n matrix C.
  //          On exit, C is overwritten by Q*C or Q'*C or C*Q' or C*Q.
  
  //  LDC     (input) INTEGER
  //          The leading dimension of the array C. LDC >= max(1,M).
  
  //  WORK    (workspace) REAL array, dimension (LWORK)
  //          On exit, if INFO = 0, WORK(1) returns the minimum value of
  //          LWORK required to use the optimal blocksize.
  
  //  LWORK   (input) INTEGER
  //          The dimension of the array WORK.
  //          If SIDE = 'L', LWORK >= max(1,N);
  //          if SIDE = 'R', LWORK >= max(1,M).
  //          For optimum performance LWORK should be at least N*NB
  //          if SIDE = 'L' and at least M*NB if SIDE = 'R', where NB is
  //          the optimal blocksize.
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input arguments
  
  info = 0;
  left = lsame( side, 'L' );
  
  //     NQ is the order of Q and NW is the minimum dimension of WORK
  
  if( left ) { 
    nq = m;
    nw = n;
  }
  else { 
    nq = n;
    nw = m;
  }
  if( !left && !lsame( side, 'R' ) ) { 
    info = -1;
  }
  else if( !lsame( trans, 'N' ) && !lsame( trans, 'T' ) ) { 
    info = -2;
  }
  else if( m < 0 ) { 
    info = -3;
  }
  else if( n < 0 ) { 
    info = -4;
  }
  else if( ilo < 1 ) { 
    info = -5;
  }
  else if( ihi < min( ilo, nq ) || ihi > nq ) { 
    info = -6;
  }
  else if( lda < max( 1, nq ) ) { 
    info = -8;
  }
  else if( ldc < max( 1, m ) ) { 
    info = -11;
  }
  else if( lwork < max( 1, nw ) ) { 
    info = -13;
  }
  if( info != 0 ) { 
    xerbla( "SORMHR", -info );
    return;
  }
  
  //     Quick return if possible
  
  nh = ihi - ilo;
  if( (m == 0 || n == 0) || nh == 0 ) { 
    work[0] = 1;
    return;
  }
  
  if( left ) { 
    mi = nh;
    ni = n;
    i1 = ilo + 1;
    i2 = 1;
  }
  else { 
    mi = m;
    ni = nh;
    i1 = 1;
    i2 = ilo + 1;
  }
  
  sormqr( side, trans, mi, ni, nh, &A(ilo - 1,ilo), lda, &tau[ilo - 1], 
   &C(i2 - 1,i1 - 1), ldc, work, lwork, iinfo );
  return;
  
  //     End of SORMHR
  
#undef  C
#undef  A
} // end of function 


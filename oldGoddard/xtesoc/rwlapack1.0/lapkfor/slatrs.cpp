/*
 * C++ implementation of Lapack routine slatrs
 *
 * $Id: slatrs.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:00:56
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: slatrs.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ZERO = 0.0e0;
const float HALF = 0.5e0;
const float ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ slatrs(const char &uplo, const char &trans, const char &diag, const char &normin, 
 const long &n, float *a, const long &lda, float x[], float &scale, float cnorm[], 
 long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  int notran, nounit, upper;
  long _do0, _do1, _do10, _do11, _do12, _do13, _do14, _do15, 
   _do16, _do17, _do2, _do3, _do4, _do5, _do6, _do7, _do8, _do9, 
   i, i_, imax, j, j_, jfirst, jinc, jlast;
  float bignum, grow, ovfl, rec, smlnum, sumj, tjj, tjjs, tmax, 
   tscal, ulp, unfl, uscal, xbnd, xj, xmax;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SLATRS solves one of the triangular systems
  
  //     A *x = s*b  or  A'*x = s*b
  
  //  with scaling to prevent overflow.  Here A is an upper or lower
  //  triangular matrix, A' denotes the transpose of A, x and b are
  //  n-element vectors, and s is a scaling factor, usually less than
  //  or equal to 1, chosen so that the components of x will be less than
  //  the overflow threshold.  If the unscaled problem will not cause
  //  overflow, the Level 2 BLAS routine STRSV is called.  If the matrix A
  //  is singular (A(j,j) = 0 for some j), then s is set to 0 and a
  //  non-trivial solution to A*x = 0 is returned.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the matrix A is upper or lower triangular.
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  TRANS   (input) CHARACTER*1
  //          Specifies the operation applied to A.
  //          = 'N':  Solve A * x = s*b  (No transpose)
  //          = 'T':  Solve A'* x = s*b  (Transpose)
  //          = 'C':  Solve A'* x = s*b  (Conjugate transpose = Transpose)
  
  //  DIAG    (input) CHARACTER*1
  //          Specifies whether or not the matrix A is unit triangular.
  //          = 'N':  Non-unit triangular
  //          = 'U':  Unit triangular
  
  //  NORMIN  (input) CHARACTER*1
  //          Specifies whether CNORM has been set or not.
  //          = 'Y':  CNORM contains the column norms on entry
  //          = 'N':  CNORM is not set on entry.  On exit, the norms will
  //                  be computed and stored in CNORM.
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  A       (input) REAL array, dimension (LDA,N)
  //          The triangular matrix A.  If UPLO = 'U', the leading n by n
  //          upper triangular part of the array A contains the upper
  //          triangular matrix, and the strictly lower triangular part of
  //          A is not referenced.  If UPLO = 'L', the leading n by n lower
  //          triangular part of the array A contains the lower triangular
  //          matrix, and the strictly upper triangular part of A is not
  //          referenced.  If DIAG = 'U', the diagonal elements of A are
  //          also not referenced and are assumed to be 1.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max (1,N).
  
  //  X       (input/output) REAL array, dimension (N)
  //          On entry, the right hand side b of the triangular system.
  //          On exit, X is overwritten by the solution vector x.
  
  //  SCALE   (output) REAL
  //          The scaling factor s for the triangular system
  //             A * x = s*b  or  A'* x = s*b.
  //          If SCALE = 0, the matrix A is singular or badly scaled, and
  //          the vector x is an exact or approximate solution to A*x = 0.
  
  //  CNORM   (input or output) REAL array, dimension (N)
  
  //          If NORMIN = 'Y', CNORM is an input variable and CNORM(j)
  //          contains the norm of the off-diagonal part of the j-th column
  //          of A.  If TRANS = 'N', CNORM(j) must be greater than or equal
  //          to the infinity-norm, and if TRANS = 'T' or 'C', CNORM(j)
  //          must be greater than or equal to the 1-norm.
  
  //          If NORMIN = 'N', CNORM is an output variable and CNORM(j)
  //          returns the 1-norm of the offdiagonal part of the j-th column
  //          of A.
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0:  if INFO = -k, the k-th argument had an illegal value
  
  //  Further Details
  //  ======= =======
  
  //  A rough bound on x is computed; if that is less than overflow, STRSV
  //  is called, otherwise, specific code is used which checks for possible
  //  overflow or divide-by-zero at every operation.
  
  //  A columnwise scheme is used for solving A*x = b.  The basic algorithm
  //  if A is lower triangular is
  
  //       x[1:n] := b[1:n]
  //       for j = 1, ..., n
  //            x(j) := x(j) / A(j,j)
  //            x[j+1:n] := x[j+1:n] - x(j) * A[j+1:n,j]
  //       end
  
  //  Define bounds on the components of x after j iterations of the loop:
  //     M(j) = bound on x[1:j]
  //     G(j) = bound on x[j+1:n]
  //  Initially, let M(0) = 0 and G(0) = max{x(i), i=1,...,n}.
  
  //  Then for iteration j+1 we have
  //     M(j+1) <= G(j) / | A(j+1,j+1) |
  //     G(j+1) <= G(j) + M(j+1) * | A[j+2:n,j+1] |
  //            <= G(j) ( 1 + CNORM(j+1) / | A(j+1,j+1) | )
  
  //  where CNORM(j+1) is greater than or equal to the infinity-norm of
  //  column j+1 of A, not counting the diagonal.  Hence
  
  //     G(j) <= G(0) product ( 1 + CNORM(i) / | A(i,i) | )
  //                  1<=i<=j
  //  and
  
  //     |x(j)| <= ( G(0) / |A(j,j)| ) product ( 1 + CNORM(i) / |A(i,i)| )
  //                                   1<=i< j
  
  //  Since |x(j)| <= M(j), we use the Level 2 BLAS routine STRSV if the
  //  reciprocal of the largest M(j), j=1,..,n, is larger than
  //  max(underflow, 1/overflow).
  
  //  The bound on x(j) is also used to determine when a step in the
  //  columnwise method can be performed without fear of overflow.  If
  //  the computed bound is greater than a large constant, x is scaled to
  //  prevent overflow, but if the bound overflows, x is set to 0, x(j) to
  //  1, and scale to 0, and a non-trivial solution to A*x = 0 is found.
  
  //  Similarly, a row-wise scheme is used to solve A'*x = b.  The basic
  //  algorithm for A upper triangular is
  
  //       for j = 1, ..., n
  //            x(j) := ( b(j) - A[1:j-1,j]' * x[1:j-1] ) / A(j,j)
  //       end
  
  //  We simultaneously compute two bounds
  //       G(j) = bound on ( b(i) - A[1:i-1,i]' * x[1:i-1] ), 1<=i<=j
  //       M(j) = bound on x(i), 1<=i<=j
  
  //  The initial values are G(0) = 0, M(0) = max{b(i), i=1,..,n}, and we
  //  add the constraint G(j) >= G(j-1) and M(j) >= M(j-1) for j >= 1.
  //  Then the bound on x(j) is
  
  //       M(j) <= M(j-1) * ( 1 + CNORM(j) ) / | A(j,j) |
  
  //            <= M(0) * product ( ( 1 + CNORM(i) ) / |A(i,i)| )
  //                      1<=i<=j
  
  //  and we can safely call STRSV if 1/M(n) and 1/G(n) are both greater
  //  than max(underflow, 1/overflow).
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  info = 0;
  upper = lsame( uplo, 'U' );
  notran = lsame( trans, 'N' );
  nounit = lsame( diag, 'N' );
  
  //     Test the input parameters.
  
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( (!notran && !lsame( trans, 'T' )) && !lsame( trans, 'C' )
    ) { 
    info = -2;
  }
  else if( !nounit && !lsame( diag, 'U' ) ) { 
    info = -3;
  }
  else if( !lsame( normin, 'Y' ) && !lsame( normin, 'N' ) ) { 
    info = -4;
  }
  else if( n < 0 ) { 
    info = -5;
  }
  else if( lda < max( 1, n ) ) { 
    info = -7;
  }
  if( info != 0 ) { 
    xerbla( "SLATRS", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  //     Determine machine dependent parameters to control overflow.
  
  unfl = slamch( 'S'/*Safe minimum*/ );
  ovfl = slamch( 'O'/*Overflow*/ );
  slabad( unfl, ovfl );
  ulp = slamch( 'E'/*Epsilon*/ )*slamch( 'B'/*Base*/ );
  smlnum = max( unfl/ulp, ONE/(ulp*ovfl) );
  bignum = (ONE - ulp)/smlnum;
  scale = ONE;
  
  if( lsame( normin, 'N' ) ) { 
    
    //        Compute the 1-norm of each column, not including the diagonal.
    
    if( upper ) { 
      
      //           A is upper triangular.
      
      for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
        cnorm[j_] = sasum( j - 1, &A(j_,0), 1 );
      }
    }
    else { 
      
      //           A is lower triangular.
      
      for( j = 1, j_ = j - 1, _do1 = n - 1; j <= _do1; j++, j_++ ) { 
        cnorm[j_] = sasum( n - j, &A(j_,j_ + 1), 1 );
      }
    }
  }
  
  //     Scale the column norms by TSCAL if the maximum entry in CNORM is
  //     greater than BIGNUM.
  
  imax = isamax( n, cnorm, 1 );
  tmax = cnorm[imax - 1];
  if( tmax <= bignum ) { 
    tscal = ONE;
  }
  else { 
    tscal = bignum/tmax;
    sscal( n, tscal, cnorm, 1 );
  }
  
  //     Compute a bound on the computed solution vector to see if the
  //     Level 2 BLAS routine STRSV can be used.
  
  j = isamax( n, x, 1 );
  xmax = abs( x[j - 1] );
  xbnd = xmax;
  if( notran ) { 
    
    //        Compute the growth in A * x = b.
    
    if( upper ) { 
      jfirst = n;
      jlast = 1;
      jinc = -1;
    }
    else { 
      jfirst = 1;
      jlast = n;
      jinc = 1;
    }
    if( nounit ) { 
      
      //           A is non-unit triangular.
      
      //           Compute GROW = 1/G(j) and XBND = 1/M(j).
      //           Initially, G(0) = max{x(i), i=1,...,n}.
      
      grow = ONE/max( xbnd, smlnum );
      xbnd = grow;
      for( j = jfirst, j_ = j - 1, _do2=docnt(j,jlast,_do3 = jinc); _do2 > 0; j += _do3, j_ += _do3, _do2-- ) { 
        
        //              Exit the loop if the growth factor is too small.
        
        if( grow <= smlnum ) 
          goto L_50;
        
        //              M(j) = G(j-1) / abs(A(j,j))
        
        tjj = abs( A(j_,j_) )*tscal;
        xbnd = min( xbnd, min( ONE, tjj )*grow );
        if( tjj + cnorm[j_] >= smlnum ) { 
          
          //                 G(j) = G(j-1)*( 1 + CNORM(j) / abs(A(j,j)) )
          
          grow = grow*(tjj/(tjj + cnorm[j_]));
        }
        else { 
          
          //                 G(j) could overflow, set GROW to 0.
          
          grow = ZERO;
        }
      }
      grow = xbnd;
    }
    else { 
      
      //           A is unit triangular.
      
      //           Compute GROW = 1/G(j), where G(0) = max{x(i), i=1,...,n}.
      
      grow = min( ONE, ONE/max( xbnd, smlnum ) );
      for( j = jfirst, j_ = j - 1, _do4=docnt(j,jlast,_do5 = jinc); _do4 > 0; j += _do5, j_ += _do5, _do4-- ) { 
        
        //              Exit the loop if the growth factor is too small.
        
        if( grow <= smlnum ) 
          goto L_50;
        
        //              G(j) = G(j-1)*( 1 + CNORM(j) )
        
        grow = grow*(ONE/(ONE + cnorm[j_]));
      }
    }
L_50:
    ;
    
  }
  else { 
    
    //        Compute the growth in A' * x = b.
    
    if( upper ) { 
      jfirst = 1;
      jlast = n;
      jinc = 1;
    }
    else { 
      jfirst = n;
      jlast = 1;
      jinc = -1;
    }
    if( nounit ) { 
      
      //           A is non-unit triangular.
      
      //           Compute GROW = 1/G(j) and XBND = 1/M(j).
      //           Initially, M(0) = max{x(i), i=1,...,n}.
      
      grow = ONE/max( xbnd, smlnum );
      xbnd = grow;
      for( j = jfirst, j_ = j - 1, _do6=docnt(j,jlast,_do7 = jinc); _do6 > 0; j += _do7, j_ += _do7, _do6-- ) { 
        
        //              Exit the loop if the growth factor is too small.
        
        if( grow <= smlnum ) 
          goto L_80;
        
        //              G(j) = max( G(j-1), M(j-1)*( 1 + CNORM(j) ) )
        
        xj = ONE + cnorm[j_];
        grow = min( grow, xbnd/xj );
        
        //              M(j) = M(j-1)*( 1 + CNORM(j) ) / abs(A(j,j))
        
        tjj = abs( A(j_,j_) )*tscal;
        if( xj > tjj ) 
          xbnd = xbnd*(tjj/xj);
      }
      grow = min( grow, xbnd );
    }
    else { 
      
      //           A is unit triangular.
      
      //           Compute GROW = 1/G(j), where G(0) = max{x(i), i=1,...,n}.
      
      grow = min( ONE, ONE/max( xbnd, smlnum ) );
      for( j = jfirst, j_ = j - 1, _do8=docnt(j,jlast,_do9 = jinc); _do8 > 0; j += _do9, j_ += _do9, _do8-- ) { 
        
        //              Exit the loop if the growth factor is too small.
        
        if( grow <= smlnum ) 
          goto L_80;
        
        //              G(j) = ( 1 + CNORM(j) )*G(j-1)
        
        xj = ONE + cnorm[j_];
        grow = grow/xj;
      }
    }
L_80:
    ;
  }
  
  if( (grow*tscal) > smlnum ) { 
    
    //        Use the Level 2 BLAS solve if the reciprocal of the bound on
    //        elements of X is not too small.
    
    strsv( uplo, trans, diag, n, a, lda, x, 1 );
  }
  else { 
    
    //        Use a Level 1 BLAS solve, scaling intermediate results.
    
    if( xmax > bignum ) { 
      
      //           Scale X so that its components are less than or equal to
      //           BIGNUM in absolute value.
      
      scale = bignum/xmax;
      sscal( n, scale, x, 1 );
      xmax = bignum;
    }
    
    if( notran ) { 
      
      //           Solve A * x = b
      
      for( j = jfirst, j_ = j - 1, _do10=docnt(j,jlast,_do11 = jinc); _do10 > 0; j += _do11, j_ += _do11, _do10-- ) { 
        
        //              Compute x(j) = b(j) / A(j,j), scaling x if necessary.
        
        xj = abs( x[j_] );
        if( nounit ) { 
          tjjs = A(j_,j_)*tscal;
          tjj = abs( tjjs );
          if( tjj > smlnum ) { 
            
            //                    abs(A(j,j)) > SMLNUM:
            
            if( tjj < ONE ) { 
              if( xj > tjj*bignum ) { 
                
                //                          Scale x by 1/b(j).
                
                rec = ONE/xj;
                sscal( n, rec, x, 1 );
                scale = scale*rec;
                xmax = xmax*rec;
              }
            }
            x[j_] = x[j_]/tjjs;
            xj = abs( x[j_] );
          }
          else if( tjj > ZERO ) { 
            
            //                    0 < abs(A(j,j)) <= SMLNUM:
            
            if( xj > tjj*bignum ) { 
              
              //                       Scale x by (1/abs(x(j)))*abs(A(j,j))*BIGNUM
              //                       to avoid overflow when dividing by A(j,j).
              
              rec = (tjj*bignum)/xj;
              if( cnorm[j_] > ONE ) { 
                
                //                          Scale by 1/CNORM(j) to avoid overflow when
                //                          multiplying x(j) times column j.
                
                rec = rec/cnorm[j_];
              }
              sscal( n, rec, x, 1 );
              scale = scale*rec;
              xmax = xmax*rec;
            }
            x[j_] = x[j_]/tjjs;
            xj = abs( x[j_] );
          }
          else { 
            
            //                    A(j,j) = 0:  Set x(1:n) = 0, x(j) = 1, and
            //                    scale = 0, and compute a solution to A*x = 0.
            
            for( i = 1, i_ = i - 1, _do12 = n; i <= _do12; i++, i_++ ) { 
              x[i_] = ZERO;
            }
            x[j_] = ONE;
            xj = ONE;
            scale = ZERO;
            xmax = ZERO;
          }
        }
        
        //              Scale x if necessary to avoid overflow when adding a
        //              multiple of column j of A.
        
        if( xj > ONE ) { 
          rec = ONE/xj;
          if( cnorm[j_] > (bignum - xmax)*rec ) { 
            
            //                    Scale x by 1/(2*abs(x(j))).
            
            rec = rec*HALF;
            sscal( n, rec, x, 1 );
            scale = scale*rec;
          }
        }
        else if( xj*cnorm[j_] > (bignum - xmax) ) { 
          
          //                 Scale x by 1/2.
          
          sscal( n, HALF, x, 1 );
          scale = scale*HALF;
        }
        
        if( upper ) { 
          if( j > 1 ) { 
            
            //                    Compute the update
            //                       x(1:j-1) := x(1:j-1) - x(j) * A(1:j-1,j)
            
            saxpy( j - 1, -x[j_]*tscal, &A(j_,0), 1, x, 
             1 );
            i = isamax( j - 1, x, 1 );
            xmax = abs( x[i - 1] );
          }
        }
        else { 
          if( j < n ) { 
            
            //                    Compute the update
            //                       x(j+1:n) := x(j+1:n) - x(j) * A(j+1:n,j)
            
            saxpy( n - j, -x[j_]*tscal, &A(j_,j_ + 1), 
             1, &x[j_ + 1], 1 );
            i = j + isamax( n - j, &x[j_ + 1], 1 );
            xmax = abs( x[i - 1] );
          }
        }
      }
      
    }
    else { 
      
      //           Solve A' * x = b
      
      for( j = jfirst, j_ = j - 1, _do13=docnt(j,jlast,_do14 = jinc); _do13 > 0; j += _do14, j_ += _do14, _do13-- ) { 
        
        //              Compute x(j) = b(j) - sum A(k,j)*x(k).
        //                                    k<>j
        
        xj = abs( x[j_] );
        uscal = tscal;
        rec = ONE/max( xmax, ONE );
        if( cnorm[j_] > (bignum - xj)*rec ) { 
          
          //                 If x(j) could overflow, scale x by 1/(2*XMAX).
          
          rec = rec*HALF;
          if( nounit ) { 
            tjjs = A(j_,j_)*tscal;
            tjj = abs( tjjs );
            if( tjj > ONE ) { 
              
              //                       Divide by A(j,j) when scaling x if A(j,j) > 1.
              
              rec = min( ONE, rec*tjj );
              uscal = uscal/tjjs;
            }
          }
          if( rec < ONE ) { 
            sscal( n, rec, x, 1 );
            scale = scale*rec;
            xmax = xmax*rec;
          }
        }
        
        sumj = ZERO;
        if( uscal == ONE ) { 
          
          //                 If the scaling needed for A in the dot product is 1,
          //                 call SDOT to perform the dot product.
          
          if( upper ) { 
            sumj = sdot( j - 1, &A(j_,0), 1, x, 1 );
          }
          else if( j < n ) { 
            sumj = sdot( n - j, &A(j_,j_ + 1), 1, &x[j_ + 1], 
             1 );
          }
        }
        else { 
          
          //                 Otherwise, use in-line code for the dot product.
          
          if( upper ) { 
            for( i = 1, i_ = i - 1, _do15 = j - 1; i <= _do15; i++, i_++ ) { 
              sumj = sumj + (A(j_,i_)*uscal)*x[i_];
            }
          }
          else if( j < n ) { 
            for( i = j + 1, i_ = i - 1, _do16 = n; i <= _do16; i++, i_++ ) { 
              sumj = sumj + (A(j_,i_)*uscal)*x[i_];
            }
          }
        }
        
        if( uscal == tscal ) { 
          
          //                 Compute x(j) := ( x(j) - sumj ) / A(j,j) if 1/A(j,j)
          //                 was not used to scale the dotproduct.
          
          x[j_] = x[j_] - sumj;
          if( nounit ) { 
            
            //                    Compute x(j) = x(j) / A(j,j), scaling if necessary.
            
            xj = abs( x[j_] );
            tjjs = A(j_,j_)*tscal;
            tjj = abs( tjjs );
            if( tjj > smlnum ) { 
              
              //                       abs(A(j,j)) > SMLNUM:
              
              if( tjj < ONE ) { 
                if( xj > tjj*bignum ) { 
                  
                  //                             Scale X by 1/abs(x(j)).
                  
                  rec = ONE/xj;
                  sscal( n, rec, x, 1 );
                  scale = scale*rec;
                  xmax = xmax*rec;
                }
              }
              x[j_] = x[j_]/tjjs;
            }
            else if( tjj > ZERO ) { 
              
              //                       0 < abs(A(j,j)) <= SMLNUM:
              
              if( xj > tjj*bignum ) { 
                
                //                          Scale x by (1/abs(x(j)))*abs(A(j,j))*BIGNUM.
                
                rec = (tjj*bignum)/xj;
                sscal( n, rec, x, 1 );
                scale = scale*rec;
                xmax = xmax*rec;
              }
              x[j_] = x[j_]/tjjs;
            }
            else { 
              
              //                       A(j,j) = 0:  Set x(1:n) = 0, x(j) = 1, and
              //                       scale = 0, and compute a solution to A'*x = 0.
              
              for( i = 1, i_ = i - 1, _do17 = n; i <= _do17; i++, i_++ ) { 
                x[i_] = ZERO;
              }
              x[j_] = ONE;
              scale = ZERO;
              xmax = ZERO;
            }
          }
        }
        else { 
          
          //                 Compute x(j) := x(j) / A(j,j)  - sumj if the dot
          //                 product has already been divided by 1/A(j,j).
          
          x[j_] = x[j_]/tjjs - sumj;
        }
        xmax = max( xmax, abs( x[j_] ) );
      }
    }
    scale = scale/tscal;
  }
  
  //     Scale the column norms by 1/TSCAL for return.
  
  if( tscal != ONE ) { 
    sscal( n, ONE/tscal, cnorm, 1 );
  }
  
  return;
  
  //     End of SLATRS
  
#undef  A
} // end of function 


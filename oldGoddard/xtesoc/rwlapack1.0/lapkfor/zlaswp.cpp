/*
 * C++ implementation of Lapack routine zlaswp
 *
 * $Id: zlaswp.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:49:19
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zlaswp.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

RWLAPKDECL void /*FUNCTION*/ zlaswp(const long &n, DComplex *a, const long &lda, const long &k1, 
   const long &k2, long ipiv[], const long &incx)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  long _do0, _do1, _do2, i, i_, ip, ix;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZLASWP performs a series of row interchanges on the matrix A.
  //  One row interchange is initiated for each of rows K1 through K2 of A.
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix A.
  
  //  A       (input/output) COMPLEX*16 array, dimension (LDA,N)
  //          On entry, the matrix of column dimension N to which the row
  //          interchanges will be applied.
  //          On exit, the permuted matrix.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.
  
  //  K1      (input) INTEGER
  //          The first element of IPIV for which a row interchange will
  //          be done.
  
  //  K2      (input) INTEGER
  //          The last element of IPIV for which a row interchange will
  //          be done.
  
  //  IPIV    (input) INTEGER array, dimension (M*abs(INCX))
  //          The vector of pivot indices.  Only the elements in positions
  //          K1 through K2 of IPIV are accessed.
  //          IPIV(K) = L implies rows K and L are to be interchanged.
  
  //  INCX    (input) INTEGER
  //          The increment between successive values of IPIV.  If IPIV
  //          is negative, the pivots are applied in reverse order.
  
  
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Executable Statements ..
  
  //     Interchange row I with row IPIV(I) for each of rows K1 through K2.
  
  if( incx == 0 ) 
    return;
  if( incx > 0 ) { 
    ix = k1;
  }
  else { 
    ix = 1 + (1 - k2)*incx;
  }
  if( incx == 1 ) { 
    for( i = k1, i_ = i - 1, _do0 = k2; i <= _do0; i++, i_++ ) { 
      ip = ipiv[i_];
      if( ip != i ) 
        zswap( n, &A(0,i_), lda, &A(0,ip - 1), lda );
    }
  }
  else if( incx > 1 ) { 
    for( i = k1, i_ = i - 1, _do1 = k2; i <= _do1; i++, i_++ ) { 
      ip = ipiv[ix - 1];
      if( ip != i ) 
        zswap( n, &A(0,i_), lda, &A(0,ip - 1), lda );
      ix = ix + incx;
    }
  }
  else if( incx < 0 ) { 
    for( i = k2, i_ = i - 1, _do2 = k1; i >= _do2; i--, i_-- ) { 
      ip = ipiv[ix - 1];
      if( ip != i ) 
        zswap( n, &A(0,i_), lda, &A(0,ip - 1), lda );
      ix = ix + incx;
    }
  }
  
  return;
  
  //     End of ZLASWP
  
#undef  A
} // end of function 


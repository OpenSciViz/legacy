/*
 * C++ implementation of Lapack routine slaev2
 *
 * $Id: slaev2.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:59:34
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: slaev2.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ONE = 1.0e0;
const float TWO = 2.0e0;
const float ZERO = 0.0e0;
const float HALF = 0.5e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ slaev2(const float &a, const float &b, const float &c, float &rt1, 
 float &rt2, float &cs1, float &sn1)
{
  long sgn1, sgn2;
  float ab, acmn, acmx, acs, adf, cs, ct, df, rt, sm, tb, tn;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SLAEV2 computes the eigendecomposition of a 2-by-2 symmetric matrix
  //     [  A   B  ]
  //     [  B   C  ].
  //  On return, RT1 is the eigenvalue of larger absolute value, RT2 is the
  //  eigenvalue of smaller absolute value, and (CS1,SN1) is the unit right
  //  eigenvector for RT1, giving the decomposition
  
  //     [ CS1  SN1 ] [  A   B  ] [ CS1 -SN1 ]  =  [ RT1  0  ]
  //     [-SN1  CS1 ] [  B   C  ] [ SN1  CS1 ]     [  0  RT2 ].
  
  //  Arguments
  //  =========
  
  //  A       (input) REAL
  //          The (1,1) entry of the 2-by-2 matrix.
  
  //  B       (input) REAL
  //          The (1,2) entry and the conjugate of the (2,1) entry of the
  //          2-by-2 matrix.
  
  //  C       (input) REAL
  //          The (2,2) entry of the 2-by-2 matrix.
  
  //  RT1     (output) REAL
  //          The eigenvalue of larger absolute value.
  
  //  RT2     (output) REAL
  //          The eigenvalue of smaller absolute value.
  
  //  CS1     (output) REAL
  //  SN1     (output) REAL
  //          The vector (CS1, SN1) is a unit right eigenvector for RT1.
  
  //  Further Details
  //  ===============
  
  //  RT1 is accurate to a few ulps barring over/underflow.
  
  //  RT2 may be inaccurate if there is massive cancellation in the
  //  determinant A*C-B*B; higher precision or correctly rounded or
  //  correctly truncated arithmetic would be needed to compute RT2
  //  accurately in all cases.
  
  //  CS1 and SN1 are accurate to a few ulps barring over/underflow.
  
  //  Overflow is possible only if RT1 is within a factor of 5 of overflow.
  //  Underflow is harmless if the input data is 0 or exceeds
  //     underflow_threshold / macheps.
  
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Compute the eigenvalues
  
  sm = a + c;
  df = a - c;
  adf = abs( df );
  tb = b + b;
  ab = abs( tb );
  if( abs( a ) > abs( c ) ) { 
    acmx = a;
    acmn = c;
  }
  else { 
    acmx = c;
    acmn = a;
  }
  if( adf > ab ) { 
    rt = adf*sqrt( ONE + pow(ab/adf, 2) );
  }
  else if( adf < ab ) { 
    rt = ab*sqrt( ONE + pow(adf/ab, 2) );
  }
  else { 
    
    //        Includes case AB=ADF=0
    
    rt = ab*sqrt( TWO );
  }
  if( sm < ZERO ) { 
    rt1 = HALF*(sm - rt);
    sgn1 = -1;
    
    //        Order of execution important.
    //        To get fully accurate smaller eigenvalue,
    //        next line needs to be executed in higher precision.
    
    rt2 = (acmx/rt1)*acmn - (b/rt1)*b;
  }
  else if( sm > ZERO ) { 
    rt1 = HALF*(sm + rt);
    sgn1 = 1;
    
    //        Order of execution important.
    //        To get fully accurate smaller eigenvalue,
    //        next line needs to be executed in higher precision.
    
    rt2 = (acmx/rt1)*acmn - (b/rt1)*b;
  }
  else { 
    
    //        Includes case RT1 = RT2 = 0
    
    rt1 = HALF*rt;
    rt2 = -HALF*rt;
    sgn1 = 1;
  }
  
  //     Compute the eigenvector
  
  if( df >= ZERO ) { 
    cs = df + rt;
    sgn2 = 1;
  }
  else { 
    cs = df - rt;
    sgn2 = -1;
  }
  acs = abs( cs );
  if( acs > ab ) { 
    ct = -tb/cs;
    sn1 = ONE/sqrt( ONE + ct*ct );
    cs1 = ct*sn1;
  }
  else { 
    if( ab == ZERO ) { 
      cs1 = ONE;
      sn1 = ZERO;
    }
    else { 
      tn = -cs/tb;
      cs1 = ONE/sqrt( ONE + tn*tn );
      sn1 = tn*cs1;
    }
  }
  if( sgn1 == sgn2 ) { 
    tn = cs1;
    cs1 = -sn1;
    sn1 = tn;
  }
  return;
  
  //     End of SLAEV2
  
} // end of function 


/*
 * C++ implementation of Lapack routine sptsv
 *
 * $Id: sptsv.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:02:12
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: sptsv.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

RWLAPKDECL void /*FUNCTION*/ sptsv(const long &n, const long &nrhs, float d[], float e[], 
   float *b, const long &ldb, long &info)
{
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SPTSV  solves the equation
  
  //     A*X = B,
  
  //  where A is an n by n symmetric positive definite tridiagonal matrix,
  //  using the L*D*L' factorization of A.
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right hand sides, i.e., the number of columns
  //          of the matrix B.  NRHS >= 0.
  
  //  D       (input/output) REAL array, dimension (N)
  //          On entry, the n diagonal elements of the tridiagonal matrix
  //          A.  On exit, the n diagonal elements of the diagonal matrix
  //          D from the L*D*L' factorization of A.
  
  //  E       (input/output) REAL array, dimension (N-1)
  //          On entry, the (n-1) subdiagonal elements of the tridiagonal
  //          matrix A.  On exit, the (n-1) subdiagonal elements of the
  //          unit bidiagonal factor L from the L*D*L' factorization of A.
  //          E can also be regarded as the superdiagonal of the unit
  //          bidiagonal factor U from the U'*D*U factorization of A.
  
  //  B       (input/output) REAL array, dimension (LDB,N)
  //          On entry, the N by NRHS matrix of right hand side vectors B.
  
  //          On exit, if INFO = 0, the N by NRHS matrix of solution
  //          vectors X.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: if INFO = k, the leading minor of order k is not
  //               positive definite, and the solution has not been
  //               computed.  The factorization has not been completed
  //               unless k = N.
  
  //  =====================================================================
  
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  if( n < 0 ) { 
    info = -1;
  }
  else if( nrhs < 0 ) { 
    info = -2;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -6;
  }
  if( info != 0 ) { 
    xerbla( "SPTSV ", -info );
    return;
  }
  
  //     Compute the L*D*L' (or U'*D*U) factorization of A.
  
  spttrf( n, d, e, info );
  if( info == 0 ) { 
    
    //        Solve the system A*X = B, overwriting B with X.
    
    spttrs( n, nrhs, d, e, b, ldb, info );
  }
  return;
  
  //     End of SPTSV
  
#undef  B
} // end of function 


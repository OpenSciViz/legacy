/*
 * C++ implementation of Lapack routine zlantp
 *
 * $Id: zlantp.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:48:46
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zlantp.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ONE = 1.0e0;
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL double /*FUNCTION*/ zlantp(const char &norm, const char &uplo, const char &diag, const long &n, 
 DComplex ap[], double work[])
{
  int udiag;
  long _do0, _do1, _do10, _do11, _do12, _do13, _do14, _do15, 
   _do16, _do17, _do18, _do19, _do2, _do20, _do21, _do22, _do23, 
   _do24, _do25, _do26, _do27, _do28, _do29, _do3, _do30, _do4, 
   _do5, _do6, _do7, _do8, _do9, i, i_, j, j_, k;
  double scale, sum, value, zlantp_v;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZLANTP  returns the value of the one norm,  or the Frobenius norm, or
  //  the  infinity norm,  or the  element of  largest absolute value  of a
  //  triangular matrix A, supplied in packed form.
  
  //  Description
  //  ===========
  
  //  ZLANTP returns the value
  
  //     ZLANTP = ( max(abs(A(i,j))), NORM = 'M' or 'm'
  //              (
  //              ( norm1(A),         NORM = '1', 'O' or 'o'
  //              (
  //              ( normI(A),         NORM = 'I' or 'i'
  //              (
  //              ( normF(A),         NORM = 'F', 'f', 'E' or 'e'
  
  //  where  norm1  denotes the  one norm of a matrix (maximum column sum),
  //  normI  denotes the  infinity norm  of a matrix  (maximum row sum) and
  //  normF  denotes the  Frobenius norm of a matrix (square root of sum of
  //  squares).  Note that  max(abs(A(i,j)))  is not a  matrix norm.
  
  //  Arguments
  //  =========
  
  //  NORM    (input) CHARACTER*1
  //          Specifies the value to be returned in ZLANTP as described
  //          above.
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the matrix A is upper or lower triangular.
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  DIAG    (input) CHARACTER*1
  //          Specifies whether or not the matrix A is unit triangular.
  //          = 'N':  Non-unit triangular
  //          = 'U':  Unit triangular
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.  When N = 0, ZLANTP is
  //          set to zero.
  
  //  AP      (input) COMPLEX*16 array, dimension (N*(N+1)/2)
  //          The upper or lower triangular matrix A, packed columnwise in
  //          a linear array.  The j-th column of A is stored in the array
  //          AP as follows:
  //          if UPLO = 'U', AP(i + (j-1)*j/2) = A(i,j) for 1<=i<=j;
  //          if UPLO = 'L', AP(i + (j-1)*(2n-j)/2) = A(i,j) for j<=i<=n.
  //          Note that when DIAG = 'U', the elements of the array AP
  //          corresponding to the diagonal elements of the matrix A are
  //          not referenced, but are assumed to be one.
  
  //  WORK    (workspace) DOUBLE PRECISION array, dimension (LWORK),
  //          where LWORK >= N when NORM = 'I'; otherwise, WORK is not
  //          referenced.
  
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  if( n == 0 ) { 
    value = ZERO;
  }
  else if( lsame( norm, 'M' ) ) { 
    
    //        Find max(abs(A(i,j))).
    
    k = 1;
    if( lsame( diag, 'U' ) ) { 
      value = ONE;
      if( lsame( uplo, 'U' ) ) { 
        for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
          for( i = k, i_ = i - 1, _do1 = k + j - 2; i <= _do1; i++, i_++ ) { 
            value = max( value, abs( ap[i_] ) );
          }
          k = k + j;
        }
      }
      else { 
        for( j = 1, j_ = j - 1, _do2 = n; j <= _do2; j++, j_++ ) { 
          for( i = k + 1, i_ = i - 1, _do3 = k + n - j; i <= _do3; i++, i_++ ) { 
            value = max( value, abs( ap[i_] ) );
          }
          k = k + n - j + 1;
        }
      }
    }
    else { 
      value = ZERO;
      if( lsame( uplo, 'U' ) ) { 
        for( j = 1, j_ = j - 1, _do4 = n; j <= _do4; j++, j_++ ) { 
          for( i = k, i_ = i - 1, _do5 = k + j - 1; i <= _do5; i++, i_++ ) { 
            value = max( value, abs( ap[i_] ) );
          }
          k = k + j;
        }
      }
      else { 
        for( j = 1, j_ = j - 1, _do6 = n; j <= _do6; j++, j_++ ) { 
          for( i = k, i_ = i - 1, _do7 = k + n - j; i <= _do7; i++, i_++ ) { 
            value = max( value, abs( ap[i_] ) );
          }
          k = k + n - j + 1;
        }
      }
    }
  }
  else if( (lsame( norm, 'O' )) || (norm == '1') ) { 
    
    //        Find norm1(A).
    
    value = ZERO;
    k = 1;
    udiag = lsame( diag, 'U' );
    if( lsame( uplo, 'U' ) ) { 
      for( j = 1, j_ = j - 1, _do8 = n; j <= _do8; j++, j_++ ) { 
        if( udiag ) { 
          sum = ONE;
          for( i = k, i_ = i - 1, _do9 = k + j - 2; i <= _do9; i++, i_++ ) { 
            sum = sum + abs( ap[i_] );
          }
        }
        else { 
          sum = ZERO;
          for( i = k, i_ = i - 1, _do10 = k + j - 1; i <= _do10; i++, i_++ ) { 
            sum = sum + abs( ap[i_] );
          }
        }
        k = k + j;
        value = max( value, sum );
      }
    }
    else { 
      for( j = 1, j_ = j - 1, _do11 = n; j <= _do11; j++, j_++ ) { 
        if( udiag ) { 
          sum = ONE;
          for( i = k + 1, i_ = i - 1, _do12 = k + n - j; i <= _do12; i++, i_++ ) { 
            sum = sum + abs( ap[i_] );
          }
        }
        else { 
          sum = ZERO;
          for( i = k, i_ = i - 1, _do13 = k + n - j; i <= _do13; i++, i_++ ) { 
            sum = sum + abs( ap[i_] );
          }
        }
        k = k + n - j + 1;
        value = max( value, sum );
      }
    }
  }
  else if( lsame( norm, 'I' ) ) { 
    
    //        Find normI(A).
    
    k = 1;
    if( lsame( uplo, 'U' ) ) { 
      if( lsame( diag, 'U' ) ) { 
        for( i = 1, i_ = i - 1, _do14 = n; i <= _do14; i++, i_++ ) { 
          work[i_] = ONE;
        }
        for( j = 1, j_ = j - 1, _do15 = n; j <= _do15; j++, j_++ ) { 
          for( i = 1, i_ = i - 1, _do16 = j - 1; i <= _do16; i++, i_++ ) { 
            work[i_] = work[i_] + abs( ap[k - 1] );
            k = k + 1;
          }
          k = k + 1;
        }
      }
      else { 
        for( i = 1, i_ = i - 1, _do17 = n; i <= _do17; i++, i_++ ) { 
          work[i_] = ZERO;
        }
        for( j = 1, j_ = j - 1, _do18 = n; j <= _do18; j++, j_++ ) { 
          for( i = 1, i_ = i - 1, _do19 = j; i <= _do19; i++, i_++ ) { 
            work[i_] = work[i_] + abs( ap[k - 1] );
            k = k + 1;
          }
        }
      }
    }
    else { 
      if( lsame( diag, 'U' ) ) { 
        for( i = 1, i_ = i - 1, _do20 = n; i <= _do20; i++, i_++ ) { 
          work[i_] = ONE;
        }
        for( j = 1, j_ = j - 1, _do21 = n; j <= _do21; j++, j_++ ) { 
          k = k + 1;
          for( i = j + 1, i_ = i - 1, _do22 = n; i <= _do22; i++, i_++ ) { 
            work[i_] = work[i_] + abs( ap[k - 1] );
            k = k + 1;
          }
        }
      }
      else { 
        for( i = 1, i_ = i - 1, _do23 = n; i <= _do23; i++, i_++ ) { 
          work[i_] = ZERO;
        }
        for( j = 1, j_ = j - 1, _do24 = n; j <= _do24; j++, j_++ ) { 
          for( i = j, i_ = i - 1, _do25 = n; i <= _do25; i++, i_++ ) { 
            work[i_] = work[i_] + abs( ap[k - 1] );
            k = k + 1;
          }
        }
      }
    }
    value = ZERO;
    for( i = 1, i_ = i - 1, _do26 = n; i <= _do26; i++, i_++ ) { 
      value = max( value, work[i_] );
    }
  }
  else if( (lsame( norm, 'F' )) || (lsame( norm, 'E' )) ) { 
    
    //        Find normF(A).
    
    if( lsame( uplo, 'U' ) ) { 
      if( lsame( diag, 'U' ) ) { 
        scale = ONE;
        sum = n;
        k = 2;
        for( j = 2, j_ = j - 1, _do27 = n; j <= _do27; j++, j_++ ) { 
          zlassq( j - 1, &ap[k - 1], 1, scale, sum );
          k = k + j;
        }
      }
      else { 
        scale = ZERO;
        sum = ONE;
        k = 1;
        for( j = 1, j_ = j - 1, _do28 = n; j <= _do28; j++, j_++ ) { 
          zlassq( j, &ap[k - 1], 1, scale, sum );
          k = k + j;
        }
      }
    }
    else { 
      if( lsame( diag, 'U' ) ) { 
        scale = ONE;
        sum = n;
        k = 2;
        for( j = 1, j_ = j - 1, _do29 = n - 1; j <= _do29; j++, j_++ ) { 
          zlassq( n - j, &ap[k - 1], 1, scale, sum );
          k = k + n - j + 1;
        }
      }
      else { 
        scale = ZERO;
        sum = ONE;
        k = 1;
        for( j = 1, j_ = j - 1, _do30 = n; j <= _do30; j++, j_++ ) { 
          zlassq( n - j + 1, &ap[k - 1], 1, scale, sum );
          k = k + n - j + 1;
        }
      }
    }
    value = scale*sqrt( sum );
  }
  
  zlantp_v = value;
  return( zlantp_v );
  
  //     End of ZLANTP
  
} // end of function 


/*
 * C++ implementation of Lapack routine slasyf
 *
 * $Id: slasyf.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:00:48
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: slasyf.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ZERO = 0.0e0;
const float ONE = 1.0e0;
const float EIGHT = 8.0e0;
const float SEVTEN = 17.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ slasyf(const char &uplo, const long &n, const long &nb, long &kb, 
 float *a, const long &lda, long ipiv[], float *w, const long &ldw, 
 long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define W(I_,J_)  (*(w+(I_)*(ldw)+(J_)))
  long _do0, _do1, _do2, _do3, _do4, _do5, _do6, _do7, imax, 
   j, j_, jb, jj, jj_, jmax, jp, k, kk, kkw, kp, kstep, kw;
  float absakk, alpha, colmax, d11, d21, d22, r1, rowmax, t;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SLASYF computes a partial factorization of a real symmetric matrix A
  //  using the Bunch-Kaufman diagonal pivoting method. The partial
  //  factorization has the form:
  
  //  A  =  ( I  U12 ) ( A11  0  ) (  I    0   )  if UPLO = 'U', or:
  //        ( 0  U22 ) (  0   D  ) ( U12' U22' )
  
  //  A  =  ( L11  0 ) (  D   0  ) ( L11' L21' )  if UPLO = 'L'
  //        ( L21  I ) (  0  A22 ) (  0    I   )
  
  //  where the order of D is at most NB. The actual order is returned in
  //  the argument KB, and is either NB or NB-1, or N if N <= NB.
  
  //  SLASYF is an auxiliary routine called by SSYTRF. It uses blocked code
  //  (calling Level 3 BLAS) to update the submatrix A11 (if UPLO = 'U') or
  //  A22 (if UPLO = 'L').
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangular part of the
  //          symmetric matrix A is stored:
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  NB      (input) INTEGER
  //          The maximum number of columns of the matrix A that should be
  //          factored.  NB should be at least 2 to allow for 2-by-2 pivot
  //          blocks.
  
  //  KB      (output) INTEGER
  //          The number of columns of A that were actually factored.
  //          KB is either NB-1 or NB, or N if N <= NB.
  
  //  A       (input/output) REAL array, dimension (LDA,N)
  //          On entry, the symmetric matrix A.  If UPLO = 'U', the leading
  //          n-by-n upper triangular part of A contains the upper
  //          triangular part of the matrix A, and the strictly lower
  //          triangular part of A is not referenced.  If UPLO = 'L', the
  //          leading n-by-n lower triangular part of A contains the lower
  //          triangular part of the matrix A, and the strictly upper
  //          triangular part of A is not referenced.
  //          On exit, A contains details of the partial factorization.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  IPIV    (output) INTEGER array, dimension (N)
  //          Details of the interchanges and the block structure of D.
  //          If UPLO = 'U', only the last KB elements of IPIV are set;
  //          if UPLO = 'L', only the first KB elements are set.
  
  //          If IPIV(k) > 0, then rows and columns k and IPIV(k) were
  //          interchanged and D(k,k) is a 1-by-1 diagonal block.
  //          If UPLO = 'U' and IPIV(k) = IPIV(k-1) < 0, then rows and
  //          columns k-1 and -IPIV(k) were interchanged and D(k-1:k,k-1:k)
  //          is a 2-by-2 diagonal block.  If UPLO = 'L' and IPIV(k) =
  //          IPIV(k+1) < 0, then rows and columns k+1 and -IPIV(k) were
  //          interchanged and D(k:k+1,k:k+1) is a 2-by-2 diagonal block.
  
  //  W       (workspace) REAL array, dimension (LDW,NB)
  
  //  LDW     (input) INTEGER
  //          The leading dimension of the array W.  LDW >= max(1,N).
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          > 0: if INFO = k, D(k,k) is exactly zero.  The factorization
  //               has been completed, but the block diagonal matrix D is
  //               exactly singular.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  info = 0;
  
  //     Initialize ALPHA for use in choosing pivot block size.
  
  alpha = (ONE + sqrt( SEVTEN ))/EIGHT;
  
  if( lsame( uplo, 'U' ) ) { 
    
    //        Factorize the trailing columns of A using the upper triangle
    //        of A and working backwards, and compute the matrix W = U12*D
    //        for use in updating A11
    
    //        K is the main loop index, decreasing from N in steps of 1 or 2
    
    //        KW is the column of W which corresponds to column K of A
    
    k = n;
L_10:
    ;
    kw = nb + k - n;
    
    //        Exit from loop
    
    if( (k <= n - nb + 1 && nb < n) || k < 1 ) 
      goto L_30;
    
    //        Copy column K of A to column KW of W and update it
    
    scopy( k, &A(k - 1,0), 1, &W(kw - 1,0), 1 );
    if( k < n ) 
      sgemv( 'N'/*No transpose*/, k, n - k, -ONE, &A(k,0), 
       lda, &W(kw,k - 1), ldw, ONE, &W(kw - 1,0), 1 );
    
    kstep = 1;
    
    //        Determine rows and columns to be interchanged and whether
    //        a 1-by-1 or 2-by-2 pivot block will be used
    
    absakk = abs( W(kw - 1,k - 1) );
    
    //        IMAX is the row-index of the largest off-diagonal element in
    //        column K, and COLMAX is its absolute value
    
    if( k > 1 ) { 
      imax = isamax( k - 1, &W(kw - 1,0), 1 );
      colmax = abs( W(kw - 1,imax - 1) );
    }
    else { 
      colmax = ZERO;
    }
    
    if( max( absakk, colmax ) == ZERO ) { 
      
      //           Column K is zero: set INFO and continue
      
      if( info == 0 ) 
        info = k;
      kp = k;
    }
    else { 
      if( absakk >= alpha*colmax ) { 
        
        //              no interchange, use 1-by-1 pivot block
        
        kp = k;
      }
      else { 
        
        //              Copy column IMAX to column KW-1 of W and update it
        
        scopy( imax, &A(imax - 1,0), 1, &W(kw - 2,0), 1 );
        scopy( k - imax, &A(imax,imax - 1), lda, &W(kw - 2,imax), 
         1 );
        if( k < n ) 
          sgemv( 'N'/*No transpose*/, k, n - k, -ONE, &A(k,0), 
           lda, &W(kw,imax - 1), ldw, ONE, &W(kw - 2,0), 
           1 );
        
        //              JMAX is the column-index of the largest off-diagonal
        //              element in row IMAX, and ROWMAX is its absolute value
        
        jmax = imax + isamax( k - imax, &W(kw - 2,imax), 1 );
        rowmax = abs( W(kw - 2,jmax - 1) );
        if( imax > 1 ) { 
          jmax = isamax( imax - 1, &W(kw - 2,0), 1 );
          rowmax = max( rowmax, abs( W(kw - 2,jmax - 1) ) );
        }
        
        if( absakk >= alpha*colmax*(colmax/rowmax) ) { 
          
          //                 no interchange, use 1-by-1 pivot block
          
          kp = k;
        }
        else if( abs( W(kw - 2,imax - 1) ) >= alpha*rowmax ) { 
          
          //                 interchange rows and columns K and IMAX, use 1-by-1
          //                 pivot block
          
          kp = imax;
          
          //                 copy column KW-1 of W to column KW
          
          scopy( k, &W(kw - 2,0), 1, &W(kw - 1,0), 1 );
        }
        else { 
          
          //                 interchange rows and columns K-1 and IMAX, use 2-by-2
          //                 pivot block
          
          kp = imax;
          kstep = 2;
        }
      }
      
      kk = k - kstep + 1;
      kkw = nb + kk - n;
      
      //           Updated column KP is already stored in column KKW of W
      
      if( kp != kk ) { 
        
        //              Copy non-updated column KK to column KP
        
        A(k - 1,kp - 1) = A(k - 1,kk - 1);
        scopy( k - 1 - kp, &A(kk - 1,kp), 1, &A(kp,kp - 1), 
         lda );
        scopy( kp, &A(kk - 1,0), 1, &A(kp - 1,0), 1 );
        
        //              Interchange rows KK and KP in last KK columns of A and W
        
        sswap( n - kk + 1, &A(kk - 1,kk - 1), lda, &A(kk - 1,kp - 1), 
         lda );
        sswap( n - kk + 1, &W(kkw - 1,kk - 1), ldw, &W(kkw - 1,kp - 1), 
         ldw );
      }
      
      if( kstep == 1 ) { 
        
        //              1-by-1 pivot block D(k): column KW of W now holds
        
        //              W(k) = U(k)*D(k)
        
        //              where U(k) is the k-th column of U
        
        //              Store U(k) in column k of A
        
        scopy( k, &W(kw - 1,0), 1, &A(k - 1,0), 1 );
        r1 = ONE/A(k - 1,k - 1);
        sscal( k - 1, r1, &A(k - 1,0), 1 );
      }
      else { 
        
        //              2-by-2 pivot block D(k): columns KW and KW-1 of W now
        //              hold
        
        //              ( W(k-1) W(k) ) = ( U(k-1) U(k) )*D(k)
        
        //              where U(k) and U(k-1) are the k-th and (k-1)-th columns
        //              of U
        
        if( k > 2 ) { 
          
          //                 Store U(k) and U(k-1) in columns k and k-1 of A
          
          d21 = W(kw - 1,k - 2);
          d11 = W(kw - 1,k - 1)/d21;
          d22 = W(kw - 2,k - 2)/d21;
          t = ONE/(d11*d22 - ONE);
          d21 = t/d21;
          for( j = 1, j_ = j - 1, _do0 = k - 2; j <= _do0; j++, j_++ ) { 
            A(k - 2,j_) = d21*(d11*W(kw - 2,j_) - W(kw - 1,j_));
            A(k - 1,j_) = d21*(d22*W(kw - 1,j_) - W(kw - 2,j_));
          }
        }
        
        //              Copy D(k) to A
        
        A(k - 2,k - 2) = W(kw - 2,k - 2);
        A(k - 1,k - 2) = W(kw - 1,k - 2);
        A(k - 1,k - 1) = W(kw - 1,k - 1);
      }
    }
    
    //        Store details of the interchanges in IPIV
    
    if( kstep == 1 ) { 
      ipiv[k - 1] = kp;
    }
    else { 
      ipiv[k - 1] = -kp;
      ipiv[k - 2] = -kp;
    }
    
    //        Decrease K and return to the start of the main loop
    
    k = k - kstep;
    goto L_10;
    
L_30:
    ;
    
    //        Update the upper triangle of A11 (= A(1:k,1:k)) as
    
    //        A11 := A11 - U12*D*U12' = A11 - U12*W'
    
    //        computing blocks of NB columns at a time
    
    for( j = ((k - 1)/nb)*nb + 1, j_ = j - 1, _do1=docnt(j,1,_do2 = -nb); _do1 > 0; j += _do2, j_ += _do2, _do1-- ) { 
      jb = min( nb, k - j + 1 );
      
      //           Update the upper triangle of the diagonal block
      
      for( jj = j, jj_ = jj - 1, _do3 = j + jb - 1; jj <= _do3; jj++, jj_++ ) { 
        sgemv( 'N'/*No transpose*/, jj - j + 1, n - k, -ONE, 
         &A(k,j_), lda, &W(kw,jj_), ldw, ONE, &A(jj_,j_), 
         1 );
      }
      
      //           Update the rectangular superdiagonal block
      
      sgemm( 'N'/*No transpose*/, 'T'/*Transpose*/, j - 1, 
       jb, n - k, -ONE, &A(k,0), lda, &W(kw,j_), ldw, ONE, &A(j_,0), 
       lda );
    }
    
    //        Put U12 in standard form by partially undoing the interchanges
    //        in columns k+1:n
    
    j = k + 1;
L_60:
    ;
    jj = j;
    jp = ipiv[j - 1];
    if( jp < 0 ) { 
      jp = -jp;
      j = j + 1;
    }
    j = j + 1;
    if( jp != jj && j <= n ) 
      sswap( n - j + 1, &A(j - 1,jp - 1), lda, &A(j - 1,jj - 1), 
       lda );
    if( j <= n ) 
      goto L_60;
    
    //        Set KB to the number of columns factorized
    
    kb = n - k;
    
  }
  else { 
    
    //        Factorize the leading columns of A using the lower triangle
    //        of A and working forwards, and compute the matrix W = L21*D
    //        for use in updating A22
    
    //        K is the main loop index, increasing from 1 in steps of 1 or 2
    
    k = 1;
L_70:
    ;
    
    //        Exit from loop
    
    if( (k >= nb && nb < n) || k > n ) 
      goto L_90;
    
    //        Copy column K of A to column K of W and update it
    
    scopy( n - k + 1, &A(k - 1,k - 1), 1, &W(k - 1,k - 1), 1 );
    sgemv( 'N'/*No transpose*/, n - k + 1, k - 1, -ONE, &A(0,k - 1), 
     lda, &W(0,k - 1), ldw, ONE, &W(k - 1,k - 1), 1 );
    
    kstep = 1;
    
    //        Determine rows and columns to be interchanged and whether
    //        a 1-by-1 or 2-by-2 pivot block will be used
    
    absakk = abs( W(k - 1,k - 1) );
    
    //        IMAX is the row-index of the largest off-diagonal element in
    //        column K, and COLMAX is its absolute value
    
    if( k < n ) { 
      imax = k + isamax( n - k, &W(k - 1,k), 1 );
      colmax = abs( W(k - 1,imax - 1) );
    }
    else { 
      colmax = ZERO;
    }
    
    if( max( absakk, colmax ) == ZERO ) { 
      
      //           Column K is zero: set INFO and continue
      
      if( info == 0 ) 
        info = k;
      kp = k;
    }
    else { 
      if( absakk >= alpha*colmax ) { 
        
        //              no interchange, use 1-by-1 pivot block
        
        kp = k;
      }
      else { 
        
        //              Copy column IMAX to column K+1 of W and update it
        
        scopy( imax - k, &A(k - 1,imax - 1), lda, &W(k,k - 1), 
         1 );
        scopy( n - imax + 1, &A(imax - 1,imax - 1), 1, &W(k,imax - 1), 
         1 );
        sgemv( 'N'/*No transpose*/, n - k + 1, k - 1, -ONE, 
         &A(0,k - 1), lda, &W(0,imax - 1), ldw, ONE, &W(k,k - 1), 
         1 );
        
        //              JMAX is the column-index of the largest off-diagonal
        //              element in row IMAX, and ROWMAX is its absolute value
        
        jmax = k - 1 + isamax( imax - k, &W(k,k - 1), 1 );
        rowmax = abs( W(k,jmax - 1) );
        if( imax < n ) { 
          jmax = imax + isamax( n - imax, &W(k,imax), 1 );
          rowmax = max( rowmax, abs( W(k,jmax - 1) ) );
        }
        
        if( absakk >= alpha*colmax*(colmax/rowmax) ) { 
          
          //                 no interchange, use 1-by-1 pivot block
          
          kp = k;
        }
        else if( abs( W(k,imax - 1) ) >= alpha*rowmax ) { 
          
          //                 interchange rows and columns K and IMAX, use 1-by-1
          //                 pivot block
          
          kp = imax;
          
          //                 copy column K+1 of W to column K
          
          scopy( n - k + 1, &W(k,k - 1), 1, &W(k - 1,k - 1), 
           1 );
        }
        else { 
          
          //                 interchange rows and columns K+1 and IMAX, use 2-by-2
          //                 pivot block
          
          kp = imax;
          kstep = 2;
        }
      }
      
      kk = k + kstep - 1;
      
      //           Updated column KP is already stored in column KK of W
      
      if( kp != kk ) { 
        
        //              Copy non-updated column KK to column KP
        
        A(k - 1,kp - 1) = A(k - 1,kk - 1);
        scopy( kp - k - 1, &A(kk - 1,k), 1, &A(k,kp - 1), 
         lda );
        scopy( n - kp + 1, &A(kk - 1,kp - 1), 1, &A(kp - 1,kp - 1), 
         1 );
        
        //              Interchange rows KK and KP in first KK columns of A and W
        
        sswap( kk, &A(0,kk - 1), lda, &A(0,kp - 1), lda );
        sswap( kk, &W(0,kk - 1), ldw, &W(0,kp - 1), ldw );
      }
      
      if( kstep == 1 ) { 
        
        //              1-by-1 pivot block D(k): column k of W now holds
        
        //              W(k) = L(k)*D(k)
        
        //              where L(k) is the k-th column of L
        
        //              Store L(k) in column k of A
        
        scopy( n - k + 1, &W(k - 1,k - 1), 1, &A(k - 1,k - 1), 
         1 );
        if( k < n ) { 
          r1 = ONE/A(k - 1,k - 1);
          sscal( n - k, r1, &A(k - 1,k), 1 );
        }
      }
      else { 
        
        //              2-by-2 pivot block D(k): columns k and k+1 of W now hold
        
        //              ( W(k) W(k+1) ) = ( L(k) L(k+1) )*D(k)
        
        //              where L(k) and L(k+1) are the k-th and (k+1)-th columns
        //              of L
        
        if( k < n - 1 ) { 
          
          //                 Store L(k) and L(k+1) in columns k and k+1 of A
          
          d21 = W(k - 1,k);
          d11 = W(k,k)/d21;
          d22 = W(k - 1,k - 1)/d21;
          t = ONE/(d11*d22 - ONE);
          d21 = t/d21;
          for( j = k + 2, j_ = j - 1, _do4 = n; j <= _do4; j++, j_++ ) { 
            A(k - 1,j_) = d21*(d11*W(k - 1,j_) - W(k,j_));
            A(k,j_) = d21*(d22*W(k,j_) - W(k - 1,j_));
          }
        }
        
        //              Copy D(k) to A
        
        A(k - 1,k - 1) = W(k - 1,k - 1);
        A(k - 1,k) = W(k - 1,k);
        A(k,k) = W(k,k);
      }
    }
    
    //        Store details of the interchanges in IPIV
    
    if( kstep == 1 ) { 
      ipiv[k - 1] = kp;
    }
    else { 
      ipiv[k - 1] = -kp;
      ipiv[k] = -kp;
    }
    
    //        Increase K and return to the start of the main loop
    
    k = k + kstep;
    goto L_70;
    
L_90:
    ;
    
    //        Update the lower triangle of A22 (= A(k:n,k:n)) as
    
    //        A22 := A22 - L21*D*L21' = A22 - L21*W'
    
    //        computing blocks of NB columns at a time
    
    for( j = k, j_ = j - 1, _do5=docnt(j,n,_do6 = nb); _do5 > 0; j += _do6, j_ += _do6, _do5-- ) { 
      jb = min( nb, n - j + 1 );
      
      //           Update the lower triangle of the diagonal block
      
      for( jj = j, jj_ = jj - 1, _do7 = j + jb - 1; jj <= _do7; jj++, jj_++ ) { 
        sgemv( 'N'/*No transpose*/, j + jb - jj, k - 1, -ONE, 
         &A(0,jj_), lda, &W(0,jj_), ldw, ONE, &A(jj_,jj_), 
         1 );
      }
      
      //           Update the rectangular subdiagonal block
      
      if( j + jb <= n ) 
        sgemm( 'N'/*No transpose*/, 'T'/*Transpose*/, n - 
         j - jb + 1, jb, k - 1, -ONE, &A(0,j_ + jb), lda, 
         &W(0,j_), ldw, ONE, &A(j_,j_ + jb), lda );
    }
    
    //        Put L21 in standard form by partially undoing the interchanges
    //        in columns 1:k-1
    
    j = k - 1;
L_120:
    ;
    jj = j;
    jp = ipiv[j - 1];
    if( jp < 0 ) { 
      jp = -jp;
      j = j - 1;
    }
    j = j - 1;
    if( jp != jj && j >= 1 ) 
      sswap( j, &A(0,jp - 1), lda, &A(0,jj - 1), lda );
    if( j >= 1 ) 
      goto L_120;
    
    //        Set KB to the number of columns factorized
    
    kb = k - 1;
    
  }
  return;
  
  //     End of SLASYF
  
#undef  W
#undef  A
} // end of function 


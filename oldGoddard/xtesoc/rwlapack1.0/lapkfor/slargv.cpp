/*
 * C++ implementation of Lapack routine slargv
 *
 * $Id: slargv.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:00:29
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: slargv.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ZERO = 0.0e0;
const float ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ slargv(const long &n, float x[], const long &incx, float y[], 
 const long &incy, float c[], const long &incc)
{
  long _do0, i, i_, ic, ix, iy;
  float tt, w, xi, yi;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SLARGV generates a vector of real plane rotations, determined by
  //  elements of the real vectors x and y. For i = 1,2,...,n
  
  //     (  c(i)  s(i) ) ( x(i) ) = ( a(i) )
  //     ( -s(i)  c(i) ) ( y(i) ) = (   0  )
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The number of plane rotations to be generated.
  
  //  X       (input/output) REAL array,
  //                         dimension (1+(N-1)*INCX)
  //          On entry, the vector x.
  //          On exit, x(i) is overwritten by a(i), for i = 1,...,n.
  
  //  INCX    (input) INTEGER
  //          The increment between elements of X. INCX > 0.
  
  //  Y       (input/output) REAL array,
  //                         dimension (1+(N-1)*INCY)
  //          On entry, the vector y.
  //          On exit, the sines of the plane rotations.
  
  //  INCY    (input) INTEGER
  //          The increment between elements of Y. INCY > 0.
  
  //  C       (output) REAL array, dimension (1+(N-1)*INCC)
  //          The cosines of the plane rotations.
  
  //  INCC    (input) INTEGER
  //          The increment between elements of C. INCC > 0.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  ix = 1;
  iy = 1;
  ic = 1;
  for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
    xi = x[ix - 1];
    yi = y[iy - 1];
    if( xi == ZERO ) { 
      c[ic - 1] = ZERO;
      y[iy - 1] = ONE;
      x[ix - 1] = yi;
    }
    else { 
      w = max( abs( xi ), abs( yi ) );
      xi = xi/w;
      yi = yi/w;
      tt = sqrt( xi*xi + yi*yi );
      c[ic - 1] = xi/tt;
      y[iy - 1] = yi/tt;
      x[ix - 1] = w*tt;
    }
    ix = ix + incx;
    iy = iy + incy;
    ic = ic + incc;
  }
  return;
  
  //     End of SLARGV
  
} // end of function 


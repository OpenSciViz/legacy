/*
 * C++ implementation of Lapack routine zgeev
 *
 * $Id: zgeev.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:46:15
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zgeev.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zgeev(const char &jobvl, const char &jobvr, const long &n, DComplex *a, 
 const long &lda, DComplex w[], DComplex *vl, const long &ldvl, DComplex *vr, 
 const long &ldvr, DComplex work[], const long &lwork, double rwork[], 
 long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define VL(I_,J_) (*(vl+(I_)*(ldvl)+(J_)))
#define VR(I_,J_) (*(vr+(I_)*(ldvr)+(J_)))
  int scalea, select[1], wantvl, wantvr;
  char side;
  long _do0, _do1, _do2, _do3, hswork, i, i_, ibal, ierr, ihi, 
   ilo, irwork, itau, iwrk, k, k_, maxb, maxwrk, minwrk, nout;
  double anrm, bignum, cscale, dum[1], eps, scl, smlnum;
  DComplex tmp;

  
  //  -- LAPACK driver routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  For an N by N DComplex nonsymmetric matrix A, compute
  
  //     the eigenvalues (W)
  //     the left and/or right eigenvectors (VL and VR)
  
  //  The eigenvectors are computed optionally:
  
  //     JOBVL determines whether to compute left eigenvectors VL
  //     JOBVR determines whether to compute right eigenvectors VR
  
  //  On return array W contains the eigenvalues. Optionally,
  //  VR contains the right eigenvectors, and VL the left eigenvectors.
  
  //  Arguments
  //  =========
  
  //  JOBVL   (input) CHARACTER*1
  //          Specifies whether or not to compute left eigenvectors of A.
  //          = 'N': left eigenvectors are not computed.
  //          = 'V': left eigenvectors are computed.
  
  //  JOBVR   (input) CHARACTER*1
  //          Specifies whether or not to compute right eigenvectors of A.
  //          = 'N': right eigenvectors are not computed.
  //          = 'V': right eigenvectors are computed.
  
  //  N       (input) INTEGER
  //          The number of rows and columns of the input matrix A. N >= 0.
  
  //  A       (input/output) COMPLEX*16 array, dimension (LDA,N)
  //          On entry, A is the matrix whose eigenvalues and eigenvectors
  //          are desired.
  //          On exit, A has been overwritten.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  W       (output) COMPLEX*16 array, dimension (N)
  //          On exit, W contains the eigenvalues.
  
  //  VL      (output) COMPLEX*16 array, dimension (LDVL,N)
  //          The left eigenvectors will be stored one after another in
  //          the columns of VL, in the same order as their eigenvalues.
  //          The eigenvectors will be normalized to have Euclidean
  //          norm equal to 1, and to have the largest component real.
  //          Specifically, VL(j)' * A = W(j) * VL(j)'  where ' means
  //          conjugate-transpose.  Left eigenvectors of A are the same as
  //          the right eigenvectors of conjugate-transpose(A).
  
  //          If JOBVL = 'N', VL is not referenced.
  
  //  LDVL    (input) INTEGER
  //          The leading dimension of the array VL.  LDVL >= 1, and if
  //          JOBVL = 'V', LDVL >= N.
  
  //  VR      (output) COMPLEX*16 array, dimension (LDVR,N)
  //          The right eigenvectors will be stored one after another in
  //          the columns of VR, in the same order as their eigenvalues.
  //          The eigenvectors will be normalized to have Euclidean
  //          norm equal to 1, and to have the largest component real.
  //          Specifically, A * VR(j) = W(j) * VR(j).
  
  //          If JOBVR = 'N', VR is not referenced.
  
  //  LDVR    (input) INTEGER
  //          The leading dimension of the array VR.  LDVR >= 1, and if
  //          JOBVR = 'V', LDVR >= N.
  
  //  WORK    (workspace/output) COMPLEX*16 array, dimension (LWORK)
  //          On exit, WORK(1) contains the optimal workspace size LWORK
  //          for high performance.
  
  //  LWORK   (input) INTEGER
  //          The dimension of the array WORK.  LWORK >= max(1,2*N).
  //          For good performance, LWORK must generally be larger.
  //          The optimum value of LWORK for high performance is returned
  //          in WORK(1).
  
  //  RWORK   (workspace) DOUBLE PRECISION array, dimension (2*N)
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument had an illegal value.
  //          > 0: the QR algorithm failed to compute all the eigenvalues;
  //               if INFO = i, elements 1:ILO-1 and i+1:N of W contain
  //               eigenvalues which have converged, and the eigenvectors
  //               are not computed.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Local Arrays ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input arguments
  
  info = 0;
  wantvl = lsame( jobvl, 'V' );
  wantvr = lsame( jobvr, 'V' );
  if( (!wantvl) && (!lsame( jobvl, 'N' )) ) { 
    info = -1;
  }
  else if( (!wantvr) && (!lsame( jobvr, 'N' )) ) { 
    info = -2;
  }
  else if( n < 0 ) { 
    info = -3;
  }
  else if( lda < max( 1, n ) ) { 
    info = -5;
  }
  else if( ldvl < 1 || (wantvl && ldvl < n) ) { 
    info = -8;
  }
  else if( ldvr < 1 || (wantvr && ldvr < n) ) { 
    info = -10;
  }
  
  //     Compute workspace
  //      (Note: Comments in the code beginning "Workspace:" describe the
  //       minimal amount of workspace needed at that point in the code,
  //       as well as the preferred amount for good performance.
  //       CWorkspace refers to DComplex workspace, and RWorkspace to real
  //       workspace. NB refers to the optimal block size for the
  //       immediately following subroutine, as returned by ILAENV.
  //       HSWORK refers to the workspace preferred by ZHSEQR, as
  //       calculated below. HSWORK is computed assuming ILO=1 and IHI=N,
  //       the worst case.)
  
  minwrk = 1;
  if( info == 0 && lwork >= 1 ) { 
    maxwrk = n + n*ilaenv( 1, "ZGEHRD", " ", n, 1, n, 0 );
    if( (!wantvl) && (!wantvr) ) { 
      minwrk = max( 1, 2*n );
      maxb = max( ilaenv( 8, "ZHSEQR", "EN", n, 1, n, -1 ), 
       2 );
      k = vmin( maxb, n, max( 2, ilaenv( 4, "ZHSEQR", "EN", 
       n, 1, n, -1 ) ), IEND );
      hswork = max( k*(k + 2), 2*n );
      maxwrk = max( maxwrk, hswork );
    }
    else { 
      minwrk = max( 1, 2*n );
      maxwrk = max( maxwrk, n + (n - 1)*ilaenv( 1, "ZUNGHR", 
       " ", n, 1, n, -1 ) );
      maxb = max( ilaenv( 8, "ZHSEQR", "SV", n, 1, n, -1 ), 
       2 );
      k = vmin( maxb, n, max( 2, ilaenv( 4, "ZHSEQR", "SV", 
       n, 1, n, -1 ) ), IEND );
      hswork = max( k*(k + 2), 2*n );
      maxwrk = vmax( maxwrk, hswork, 2*n, IEND );
    }
    work[0] = DComplex((double)maxwrk);
  }
  if( lwork < minwrk ) { 
    info = -12;
  }
  if( info != 0 ) { 
    xerbla( "ZGEEV ", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  //     Get machine constants
  
  eps = dlamch( 'P' );
  smlnum = dlamch( 'S' );
  bignum = ONE/smlnum;
  dlabad( smlnum, bignum );
  smlnum = sqrt( smlnum )/eps;
  bignum = ONE/smlnum;
  
  //     Scale A if max entry outside range [SMLNUM,BIGNUM]
  
  anrm = zlange( 'M', n, n, a, lda, dum );
  scalea = FALSE;
  if( anrm > ZERO && anrm < smlnum ) { 
    scalea = TRUE;
    cscale = smlnum;
  }
  else if( anrm > bignum ) { 
    scalea = TRUE;
    cscale = bignum;
  }
  if( scalea ) 
    zlascl( 'G', 0, 0, anrm, cscale, n, n, a, lda, ierr );
  
  //     Balance the matrix
  //     (CWorkspace: none)
  //     (RWorkspace: need N)
  
  ibal = 1;
  zgebal( 'B', n, a, lda, ilo, ihi, &rwork[ibal - 1], ierr );
  
  //     Reduce to upper Hessenberg form
  //     (CWorkspace: need 2*N, prefer N+N*NB)
  //     (RWorkspace: none)
  
  itau = 1;
  iwrk = itau + n;
  zgehrd( n, ilo, ihi, a, lda, &work[itau - 1], &work[iwrk - 1], 
   lwork - iwrk + 1, ierr );
  
  if( wantvl ) { 
    
    //        Want left eigenvectors
    //        Copy Householder vectors to VL
    
    side = 'L';
    zlacpy( 'L', n, n, a, lda, vl, ldvl );
    
    //        Generate unitary matrix in VL
    //        (CWorkspace: need 2*N-1, prefer N+(N-1)*NB)
    //        (RWorkspace: none)
    
    zunghr( n, ilo, ihi, vl, ldvl, &work[itau - 1], &work[iwrk - 1], 
     lwork - iwrk + 1, ierr );
    
    //        Perform QR iteration, accumulating Schur vectors in VL
    //        (CWorkspace: need 1, prefer HSWORK (see comments) )
    //        (RWorkspace: none)
    
    iwrk = itau;
    zhseqr( 'S', 'V', n, ilo, ihi, a, lda, w, vl, ldvl, &work[iwrk - 1], 
     lwork - iwrk + 1, info );
    
    if( wantvr ) { 
      
      //           Want left and right eigenvectors
      //           Copy Schur vectors to VR
      
      side = 'B';
      zlacpy( 'F', n, n, vl, ldvl, vr, ldvr );
    }
    
  }
  else if( wantvr ) { 
    
    //        Want right eigenvectors
    //        Copy Householder vectors to VR
    
    side = 'R';
    zlacpy( 'L', n, n, a, lda, vr, ldvr );
    
    //        Generate unitary matrix in VR
    //        (CWorkspace: need 2*N-1, prefer N+(N-1)*NB)
    //        (RWorkspace: none)
    
    zunghr( n, ilo, ihi, vr, ldvr, &work[itau - 1], &work[iwrk - 1], 
     lwork - iwrk + 1, ierr );
    
    //        Perform QR iteration, accumulating Schur vectors in VR
    //        (CWorkspace: need 1, prefer HSWORK (see comments) )
    //        (RWorkspace: none)
    
    iwrk = itau;
    zhseqr( 'S', 'V', n, ilo, ihi, a, lda, w, vr, ldvr, &work[iwrk - 1], 
     lwork - iwrk + 1, info );
    
  }
  else { 
    
    //        Compute eigenvalues only
    //        (CWorkspace: need 1, prefer HSWORK (see comments) )
    //        (RWorkspace: none)
    
    iwrk = itau;
    zhseqr( 'E', 'N', n, ilo, ihi, a, lda, w, vr, ldvr, &work[iwrk - 1], 
     lwork - iwrk + 1, info );
  }
  
  //     If INFO > 0 from ZHSEQR, then quit
  
  if( info > 0 ) 
    goto L_50;
  
  if( wantvl || wantvr ) { 
    
    //        Compute left and/or right eigenvectors
    //        (CWorkspace: need 2*N)
    //        (RWorkspace: need 2*N)
    
    irwork = ibal + n;
    ztrevc( side, 'O', select, n, a, lda, vl, ldvl, vr, ldvr, 
     n, nout, &work[iwrk - 1], &rwork[irwork - 1], ierr );
  }
  
  if( wantvl ) { 
    
    //        Undo balancing of left eigenvectors
    //        (CWorkspace: none)
    //        (RWorkspace: need N)
    
    zgebak( 'B', 'L', n, ilo, ihi, &rwork[ibal - 1], n, vl, ldvl, 
     ierr );
    
    //        Normalize left eigenvectors and make largest component real
    
    for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
      scl = ONE/dznrm2( n, &VL(i_,0), 1 );
      zdscal( n, scl, &VL(i_,0), 1 );
      for( k = 1, k_ = k - 1, _do1 = n; k <= _do1; k++, k_++ ) { 
        rwork[irwork + k_ - 1] = pow(real( VL(i_,k_) ), 2) + 
         pow(imag( VL(i_,k_) ), 2);
      }
      k = idamax( n, &rwork[irwork - 1], 1 );
      tmp = conj( VL(i_,k - 1) )/sqrt( rwork[irwork + k - 2] );
      zscal( n, tmp, &VL(i_,0), 1 );
      VL(i_,k - 1) = DComplex( real( VL(i_,k - 1) ), ZERO );
    }
  }
  
  if( wantvr ) { 
    
    //        Undo balancing of right eigenvectors
    //        (CWorkspace: none)
    //        (RWorkspace: need N)
    
    zgebak( 'B', 'R', n, ilo, ihi, &rwork[ibal - 1], n, vr, ldvr, 
     ierr );
    
    //        Normalize right eigenvectors and make largest component real
    
    for( i = 1, i_ = i - 1, _do2 = n; i <= _do2; i++, i_++ ) { 
      scl = ONE/dznrm2( n, &VR(i_,0), 1 );
      zdscal( n, scl, &VR(i_,0), 1 );
      for( k = 1, k_ = k - 1, _do3 = n; k <= _do3; k++, k_++ ) { 
        rwork[irwork + k_ - 1] = pow(real( VR(i_,k_) ), 2) + 
         pow(imag( VR(i_,k_) ), 2);
      }
      k = idamax( n, &rwork[irwork - 1], 1 );
      tmp = conj( VR(i_,k - 1) )/sqrt( rwork[irwork + k - 2] );
      zscal( n, tmp, &VR(i_,0), 1 );
      VR(i_,k - 1) = DComplex( real( VR(i_,k - 1) ), ZERO );
    }
  }
  
  //     Undo scaling if necessary
  
L_50:
  ;
  if( scalea ) { 
    zlascl( 'G', 0, 0, cscale, anrm, n - info, 1, &w[info], max( n - 
     info, 1 ), ierr );
    if( info > 0 ) { 
      zlascl( 'G', 0, 0, cscale, anrm, ilo - 1, 1, w, n, ierr );
    }
  }
  
  work[0] = DComplex((double)maxwrk);
  return;
  
  //     End of ZGEEV
  
#undef  VR
#undef  VL
#undef  A
} // end of function 


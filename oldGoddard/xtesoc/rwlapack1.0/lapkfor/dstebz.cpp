/*
 * C++ implementation of lapack routine dstebz
 *
 * $Id: dstebz.cpp,v 1.6 1993/06/24 19:06:29 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:38:14
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dstebz.cpp,v $
 * Revision 1.6  1993/06/24  19:06:29  alv
 * made some more parms const to stop warning from zlasrc files
 *
 * Revision 1.5  1993/04/06  20:42:24  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:17:15  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:09:04  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
const double TWO = 2.0e0;
const double FUDGE = 2.0e0;
const double RELFAC = 2.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dstebz(const char &range, const char &order, const long &n, const double &vl, 
 const double &vu, const long &il, const long &iu, const double &abstol, double d[], 
 double e[], long &m, long &nsplit, double w[], long iblock[], 
 long isplit[], double work[], long iwork[], long &info)
{
  // PARAMETER translations
  double HALF = 1.0e0/TWO;
  // end of PARAMETER translations

  int ncnvrg, toofew;
  long _do0, _do1, _do10, _do11, _do12, _do13, _do14, _do2, 
   _do3, _do4, _do5, _do6, _do7, _do8, _do9, ib, ibegin, idiscl, 
   idiscu, idumma[1], ie, iend, iinfo, im, in, ioff, iorder, iout, 
   irange, itmax, itmp1, iw, iwoff, j, j_, jb, jb_, jdisc, jdisc_, 
   je, je_, nb, nwl, nwu;
  double atoli, bnorm, gl, gu, pivmin, rtoli, safemn, tmp1, tmp2, 
   tnorm, ulp, wkill, wl, wlu, wu, wul;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DSTEBZ computes the eigenvalues of a symmetric tridiagonal
  //  matrix.  The user may ask for all eigenvalues, all eigenvalues
  //  in the interval (VL, VU], or the IL-th through IU-th eigenvalues.
  
  //  See W. Kahan "Accurate Eigenvalues of a Symmetric Tridiagonal
  //  Matrix", Report CS41, Computer Science Dept., Stanford
  //  University, July 21, 1966
  
  //  Arguments
  //  =========
  
  //  RANGE   (input) CHARACTER
  //          Specifies which eigenvalues are to be found.
  //          = 'A': ("All")   all eigenvalues will be found.
  //          = 'V': ("Value") all eigenvalues in the half-open interval
  //                           (VL, VU] will be found.
  //          = 'I': ("Index") the IL-th through IU-th eigenvalues (of the
  //                           entire matrix) will be found.
  
  //  ORDER   (input) CHARACTER
  //          Specifies the order in which the eigenvalues and their
  //          block numbers will be stored in W and IBLOCK:
  //          = 'B': ("By Block") the eigenvalues will be grouped by
  //                              split-off block (see IBLOCK, ISPLIT) and
  //                              ordered from smallest to largest within
  //                              the block.
  //          = 'E': ("Entire matrix")
  //                              the eigenvalues for the entire matrix
  //                              will be ordered from smallest to
  //                              largest.
  
  //  N       (input) INTEGER
  //          The dimension of the tridiagonal matrix T.
  
  //  VL      (input) DOUBLE PRECISION
  //          If RANGE='V', the lower bound of the interval to be searched
  //          for eigenvalues.  Eigenvalues less than or equal to VL will
  //          not be returned.  Not referenced if RANGE='A' or 'I'.
  
  //  VU      (input) DOUBLE PRECISION
  //          If RANGE='V', the upper bound of the interval to be searched
  //          for eigenvalues.  Eigenvalues greater than VU will not be
  //          returned.  VU must be greater than VL.  Not referenced if
  //          RANGE='A' or 'I'.
  
  //  IL      (input) INTEGER
  //          If RANGE='I', the index (from smallest to largest) of the
  //          smallest eigenvalue to be returned.  IL must be at least 1.
  //          Not referenced if RANGE='A' or 'V'.
  
  //  IU      (input) INTEGER
  //          If RANGE='I', the index (from smallest to largest) of the
  //          largest eigenvalue to be returned.  IU must be at least IL
  //          and no greater than N.  Not referenced if RANGE='A' or 'V'.
  
  //  ABSTOL  (input) DOUBLE PRECISION
  //          The absolute tolerance for the eigenvalues.  An eigenvalue
  //          (or cluster) is considered to be located if it has been
  //          determined to lie in an interval whose width is ABSTOL or
  //          less.  If ABSTOL is less than or equal to zero, then ULP*|T|
  //          will be used, where |T| means the 1-norm of T.
  
  //  D       (input) DOUBLE PRECISION array, dimension (N)
  //          The diagonal entries of the tridiagonal matrix T.  To avoid
  //          overflow, the matrix must be scaled so that its largest
  //          entry is no greater than overflow**(1/2) * underflow**(1/4)
  //          in absolute value, and for greatest accuracy, it should not
  //          be much smaller than that.
  
  //  E       (input) DOUBLE PRECISION array, dimension (N-1)
  //          The offdiagonal entries of the tridiagonal matrix T must be
  //          in elements 1 through N-1, of the array E.
  //          To avoid overflow, the matrix must be scaled so that its
  //          largest entry is no greater than overflow**(1/2) *
  //          underflow**(1/4) in absolute value, and for greatest
  //          accuracy, it should not be much smaller than that.
  
  //  M       (output) INTEGER
  //          The actual number of eigenvalues found.  It will be in the
  //          range 0 to N.  (See also the description of INFO=2,3.)
  
  //  NSPLIT  (output) INTEGER
  //          The number of diagonal blocks which T is considered to
  //          consist of.  It will be between 1 and N.
  
  //  W       (output) DOUBLE PRECISION array, dimension (N)
  //          On exit, the first M elements of W will contain the
  //          eigenvalues.  (DSTEBZ may use the remaining N-M elements as
  //          workspace.)
  
  //  IBLOCK  (output) INTEGER array, dimension (N)
  //          At each row/column j where E(j) is zero or small, the
  //          matrix T is considered to split into a block diagonal
  //          matrix.  On exit, IBLOCK(i) specifies which block (from 1 to
  //          the number of blocks) the eigenvalue W(i) belongs to.
  //          (DSTEBZ may use the remaining N-M elements as workspace.)
  //          NOTE:  in the (theoretically impossible) event that
  //          bisection does not converge for all eigenvalues, INFO is set
  //          to 1 or 3, and the ones for which it did not are identified
  //          by a *negative* block number.
  
  //  ISPLIT  (output) INTEGER array, dimension (N)
  //          The splitting points, at which T breaks up into submatrices.
  //          The first submatrix consists of rows/columns 1 to ISPLIT(1),
  //          the second of rows/columns ISPLIT(1)+1 through ISPLIT(2),
  //          etc., and the NSPLIT-th consists of rows/columns
  //          ISPLIT(NSPLIT-1)+1 through ISPLIT(NSPLIT)=N.
  //          (Only the first NSPLIT elements will actually be used, but
  //          since the user cannot know a priori what value NSPLIT will
  //          have, N words must be reserved for ISPLIT.)
  
  //  WORK    (workspace) DOUBLE PRECISION array, dimension (4*N)
  //          Workspace.
  
  //  IWORK   (workspace) INTEGER array, dimension (3*N)
  //          Workspace.
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: This case includes a number of "impossible" errors.
  //          = 1-3: This includes a number of cases, any or all of
  //                 which might conceivably occur.  The output arrays are
  //                 defined, but the results may not be as accurate as
  //                 desired, or eigenvalues may be missing.
  //                 1,3: Bisection failed to converge for some
  //                      eigenvalues; these eigenvalues are flagged by a
  //                      negative block number.  The effect is that the
  //                      eigenvalues may not be as accurate as the
  //                      absolute and relative tolerances.  This is
  //                      generally caused by arithmetic which is less
  //                      accurate than DLAMCH says.
  //                 2,3: RANGE='I' only: Not all of the eigenvalues IL:IU
  //                      were found.
  //                      Effect: M < IU+1-IL
  //                      Cause:  non-monotonic arithmetic, causing the
  //                              Sturm sequence to be non-monotonic.
  //                      Cure:   recalculate, using RANGE='A', and pick
  //                              out eigenvalues IL:IU.  In some cases,
  //                              increasing the PARAMETER "FUDGE" may
  //                              make things work.
  //          = 4: RANGE='I', and the Gershgorin interval initially used
  //               was too small.  No eigenvalues were computed.
  //               Probable cause: your machine has sloppy floating-point
  //                               arithmetic.
  //               Cure: Increase the PARAMETER "FUDGE", recompile, and
  //                     try again.
  
  //  Internal Parameters
  //  ===================
  
  //  RELFAC  DOUBLE PRECISION, default = 2.0e0
  //          The relative tolerance.  An interval (a,b] lies within
  //          "relative tolerance" if  b-a < RELFAC*ulp*max(|a|,|b|),
  //          where "ulp" is the machine precision (distance from 1 to
  //          the next larger floating point number.)
  
  //  FUDGE   DOUBLE PRECISION, default = 2
  //          A "fudge factor" to widen the Gershgorin intervals.  Ideally,
  //          a value of 1 should work, but on machines with sloppy
  //          arithmetic, this needs to be larger.  The default for
  //          publicly released versions should be large enough to handle
  //          the worst machine around.  Note that this has no effect
  //          on accuracy of the solution.
  
  //-----------------------------------------------------------------------
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Local Arrays ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  info = 0;
  
  //     Decode RANGE
  
  if( lsame( range, 'A' ) ) { 
    irange = 1;
  }
  else if( lsame( range, 'V' ) ) { 
    irange = 2;
  }
  else if( lsame( range, 'I' ) ) { 
    irange = 3;
  }
  else { 
    irange = 0;
  }
  
  //     Decode ORDER
  
  if( lsame( order, 'B' ) ) { 
    iorder = 2;
  }
  else if( lsame( order, 'E' ) || lsame( order, 'A' ) ) { 
    iorder = 1;
  }
  else { 
    iorder = 0;
  }
  
  //     Check for Errors
  
  if( irange <= 0 ) { 
    info = -1;
  }
  else if( iorder <= 0 ) { 
    info = -2;
  }
  else if( n < 0 ) { 
    info = -3;
  }
  else if( irange == 2 && vl >= vu ) { 
    info = -5;
  }
  else if( irange == 3 && (il < 1 || il > max( 1, n )) ) { 
    info = -6;
  }
  else if( irange == 3 && (iu < min( n, il ) || iu > n) ) { 
    info = -7;
  }
  
  if( info != 0 ) { 
    xerbla( "DSTEBZ", -info );
    return;
  }
  
  //     Initialize error flags
  
  info = 0;
  ncnvrg = FALSE;
  toofew = FALSE;
  
  //     Quick return if possible
  
  m = 0;
  if( n == 0 ) 
    return;
  
  //     Simplifications:
  
  if( (irange == 3 && il == 1) && iu == n ) 
    irange = 1;
  
  //     Get machine constants
  //     NB is the minimum vector length for vector bisection, or 0
  //     if only scalar is to be done.
  
  safemn = dlamch( 'S' );
  ulp = dlamch( 'P' );
  rtoli = ulp*RELFAC;
  nb = ilaenv( 1, "DSTEBZ", " ", n, -1, -1, -1 );
  if( nb <= 1 ) 
    nb = 0;
  
  //     Special Case when N=1
  
  if( n == 1 ) { 
    nsplit = 1;
    isplit[0] = 1;
    if( irange == 2 && (vl >= d[0] || vu < d[0]) ) { 
      m = 0;
    }
    else { 
      w[0] = d[0];
      iblock[0] = 1;
      m = 1;
    }
    return;
  }
  
  //     Compute Splitting Points
  
  nsplit = 1;
  work[n - 1] = ZERO;
  pivmin = ONE;
  
  for( j = 2, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
    tmp1 = pow(e[j_ - 1], 2);
    if( abs( d[j_]*d[j_ - 1] )*pow(ulp, 2) + safemn > tmp1 ) { 
      isplit[nsplit - 1] = j - 1;
      nsplit = nsplit + 1;
      work[j_ - 1] = ZERO;
    }
    else { 
      work[j_ - 1] = tmp1;
      pivmin = max( pivmin, tmp1 );
    }
  }
  isplit[nsplit - 1] = n;
  pivmin = pivmin*safemn;
  
  //     Compute Interval and ATOLI
  
  if( irange == 3 ) { 
    
    //        RANGE='I': Compute the interval containing eigenvalues
    //                   IL through IU.
    
    //        Compute Gershgorin interval for entire (split) matrix
    //        and use it as the initial interval
    
    gu = d[0];
    gl = d[0];
    tmp1 = ZERO;
    
    for( j = 1, j_ = j - 1, _do1 = n - 1; j <= _do1; j++, j_++ ) { 
      tmp2 = sqrt( work[j_] );
      gu = max( gu, d[j_] + tmp1 + tmp2 );
      gl = min( gl, d[j_] - tmp1 - tmp2 );
      tmp1 = tmp2;
    }
    
    gu = max( gu, d[n - 1] + tmp1 );
    gl = min( gl, d[n - 1] - tmp1 );
    tnorm = max( abs( gl ), abs( gu ) );
    gl = gl - FUDGE*tnorm*ulp*n - FUDGE*TWO*pivmin;
    gu = gu + FUDGE*tnorm*ulp*n + FUDGE*pivmin;
    
    //        Compute Iteration parameters
    
    itmax = (long)( (log( tnorm + pivmin ) - log( pivmin ))/log( TWO ) ) + 
     2;
    if( abstol <= ZERO ) { 
      atoli = ulp*tnorm;
    }
    else { 
      atoli = abstol;
    }
    
    work[n] = gl;
    work[n + 1] = gl;
    work[n + 2] = gu;
    work[n + 3] = gu;
    work[n + 4] = gl;
    work[n + 5] = gu;
    iwork[0] = -1;
    iwork[1] = -1;
    iwork[2] = n + 1;
    iwork[3] = n + 1;
    iwork[4] = il - 1;
    iwork[5] = iu;
    
    dlaebz( 3, itmax, n, 2, 2, nb, atoli, rtoli, pivmin, d, e, 
     work, &iwork[4], &work[n], &work[n + 4], iout, iwork, w, 
     iblock, iinfo );
    
    if( iwork[5] == iu ) { 
      wl = work[n];
      wlu = work[n + 2];
      nwl = iwork[0];
      wu = work[n + 3];
      wul = work[n + 1];
      nwu = iwork[3];
    }
    else { 
      wl = work[n + 1];
      wlu = work[n + 3];
      nwl = iwork[1];
      wu = work[n + 2];
      wul = work[n];
      nwu = iwork[2];
    }
    
    if( ((nwl < 0 || nwl >= n) || nwu < 1) || nwu > n ) { 
      info = 4;
      return;
    }
  }
  else { 
    
    //        RANGE='A' or 'V' -- Set ATOLI
    
    tnorm = max( abs( d[0] ) + abs( e[0] ), abs( d[n - 1] ) + 
     abs( e[n - 2] ) );
    
    for( j = 2, j_ = j - 1, _do2 = n - 1; j <= _do2; j++, j_++ ) { 
      tnorm = max( tnorm, abs( d[j_] ) + abs( e[j_ - 1] ) + 
       abs( e[j_] ) );
    }
    
    if( abstol <= ZERO ) { 
      atoli = ulp*tnorm;
    }
    else { 
      atoli = abstol;
    }
    
    if( irange == 2 ) { 
      wl = vl;
      wu = vu;
    }
  }
  
  //     Find Eigenvalues -- Loop Over Blocks and recompute NWL and NWU.
  //     NWL accumulates the number of eigenvalues .le. WL,
  //     NWU accumulates the number of eigenvalues .le. WU
  
  m = 0;
  iend = 0;
  info = 0;
  nwl = 0;
  nwu = 0;
  
  for( jb = 1, jb_ = jb - 1, _do3 = nsplit; jb <= _do3; jb++, jb_++ ) { 
    ioff = iend;
    ibegin = ioff + 1;
    iend = isplit[jb_];
    in = iend - ioff;
    
    if( in == 1 ) { 
      
      //           Special Case -- IN=1
      
      if( irange == 1 || wl >= d[ibegin - 1] - pivmin ) 
        nwl = nwl + 1;
      if( irange == 1 || wu >= d[ibegin - 1] - pivmin ) 
        nwu = nwu + 1;
      if( irange == 1 || (wl < d[ibegin - 1] - pivmin && wu >= 
       d[ibegin - 1] - pivmin) ) { 
        m = m + 1;
        w[m - 1] = d[ibegin - 1];
        iblock[m - 1] = jb;
      }
    }
    else { 
      
      //           General Case -- IN > 1
      
      //           Compute Gershgorin Interval
      //           and use it as the initial interval
      
      gu = d[ibegin - 1];
      gl = d[ibegin - 1];
      tmp1 = ZERO;
      
      for( j = ibegin, j_ = j - 1, _do4 = iend - 1; j <= _do4; j++, j_++ ) { 
        tmp2 = abs( e[j_] );
        gu = max( gu, d[j_] + tmp1 + tmp2 );
        gl = min( gl, d[j_] - tmp1 - tmp2 );
        tmp1 = tmp2;
      }
      
      gu = max( gu, d[iend - 1] + tmp1 );
      gl = min( gl, d[iend - 1] - tmp1 );
      bnorm = max( abs( gl ), abs( gu ) );
      gl = gl - FUDGE*bnorm*ulp*in - FUDGE*pivmin;
      gu = gu + FUDGE*bnorm*ulp*in + FUDGE*pivmin;
      
      if( irange > 1 ) { 
        gl = max( gl, wl );
        gu = min( gu, wu );
        if( gl >= gu ) 
          goto L_70;
      }
      
      //           Set Up Initial Interval
      
      work[n] = gl;
      work[n + in] = gu;
      dlaebz( 1, 0, in, in, 1, nb, atoli, rtoli, pivmin, &d[ibegin - 1], 
       &e[ibegin - 1], &work[ibegin - 1], idumma, &work[n], 
       &work[n + in*2], im, iwork, &w[m], &iblock[m], iinfo );
      
      nwl = nwl + iwork[0];
      nwu = nwu + iwork[in];
      iwoff = m - iwork[0];
      
      //           Compute Eigenvalues
      
      itmax = (long)( (log( gu - gl + pivmin ) - log( pivmin ))/
       log( TWO ) ) + 2;
      dlaebz( 2, itmax, in, in, 1, nb, atoli, rtoli, pivmin, 
       &d[ibegin - 1], &e[ibegin - 1], &work[ibegin - 1], idumma, 
       &work[n], &work[n + in*2], iout, iwork, &w[m], &iblock[m], 
       iinfo );
      
      //           Copy Eigenvalues Into W and IBLOCK
      //           Use -JB for block number for unconverged eigenvalues.
      
      for( j = 1, j_ = j - 1, _do5 = iout; j <= _do5; j++, j_++ ) { 
        tmp1 = HALF*(work[j_ + n] + work[j_ + in + n]);
        
        //              Flag non-convergence.
        
        if( j > iout - iinfo ) { 
          ncnvrg = TRUE;
          ib = -jb;
        }
        else { 
          ib = jb;
        }
        for( je = iwork[j_] + 1 + iwoff, je_ = je - 1, _do6 = iwork[j_ + in] + 
         iwoff; je <= _do6; je++, je_++ ) { 
          w[je_] = tmp1;
          iblock[je_] = ib;
        }
      }
      
      m = m + im;
    }
L_70:
    ;
  }
  
  //     If RANGE='I', then (WL,WU) contains eigenvalues NWL+1,...,NWU
  //     If NWL+1 < IL or NWU > IU, discard extra eigenvalues.
  
  if( irange == 3 ) { 
    im = 0;
    idiscl = il - 1 - nwl;
    idiscu = nwu - iu;
    
    if( idiscl > 0 || idiscu > 0 ) { 
      for( je = 1, je_ = je - 1, _do7 = m; je <= _do7; je++, je_++ ) { 
        if( w[je_] <= wlu && idiscl > 0 ) { 
          idiscl = idiscl - 1;
        }
        else if( w[je_] >= wul && idiscu > 0 ) { 
          idiscu = idiscu - 1;
        }
        else { 
          im = im + 1;
          w[im - 1] = w[je_];
          iblock[im - 1] = iblock[je_];
        }
      }
      m = im;
    }
    if( idiscl > 0 || idiscu > 0 ) { 
      
      //           Code to deal with effects of bad arithmetic:
      //           Some low eigenvalues to be discarded are not in (WL,WLU],
      //           or high eigenvalues to be discarded are not in (WUL,WU]
      //           so just kill off the smallest IDISCL/largest IDISCU
      //           eigenvalues, by simply finding the smallest/largest
      //           eigenvalue(s).
      
      //           (If N(w) is monotone non-decreasing, this should never
      //               happen.)
      
      if( idiscl > 0 ) { 
        wkill = wu;
        for( jdisc = 1, jdisc_ = jdisc - 1, _do8 = idiscl; jdisc <= _do8; jdisc++, jdisc_++ ) { 
          iw = 0;
          for( je = 1, je_ = je - 1, _do9 = m; je <= _do9; je++, je_++ ) { 
            if( iblock[je_] != 0 && (w[je_] < wkill || 
             iw == 0) ) { 
              iw = je;
              wkill = w[je_];
            }
          }
          iblock[iw - 1] = 0;
        }
      }
      if( idiscu > 0 ) { 
        
        wkill = wl;
        for( jdisc = 1, jdisc_ = jdisc - 1, _do10 = idiscu; jdisc <= _do10; jdisc++, jdisc_++ ) { 
          iw = 0;
          for( je = 1, je_ = je - 1, _do11 = m; je <= _do11; je++, je_++ ) { 
            if( iblock[je_] != 0 && (w[je_] > wkill || 
             iw == 0) ) { 
              iw = je;
              wkill = w[je_];
            }
          }
          iblock[iw - 1] = 0;
        }
      }
      im = 0;
      for( je = 1, je_ = je - 1, _do12 = m; je <= _do12; je++, je_++ ) { 
        if( iblock[je_] != 0 ) { 
          im = im + 1;
          w[im - 1] = w[je_];
          iblock[im - 1] = iblock[je_];
        }
      }
      m = im;
    }
    if( idiscl < 0 || idiscu < 0 ) { 
      toofew = TRUE;
    }
  }
  
  //     If ORDER='B', do nothing -- the eigenvalues are already sorted
  //        by block.
  //     If ORDER='E' or 'A', sort the eigenvalues from smallest to largest
  
  if( iorder == 1 && nsplit > 1 ) { 
    for( je = 1, je_ = je - 1, _do13 = m - 1; je <= _do13; je++, je_++ ) { 
      ie = 0;
      tmp1 = w[je_];
      for( j = je + 1, j_ = j - 1, _do14 = m; j <= _do14; j++, j_++ ) { 
        if( w[j_] < tmp1 ) { 
          ie = j;
          tmp1 = w[j_];
        }
      }
      
      if( ie != 0 ) { 
        itmp1 = iblock[ie - 1];
        w[ie - 1] = w[je_];
        iblock[ie - 1] = iblock[je_];
        w[je_] = tmp1;
        iblock[je_] = itmp1;
      }
    }
  }
  
  info = 0;
  if( ncnvrg ) 
    info = info + 1;
  if( toofew ) 
    info = info + 2;
  return;
  
  //     End of DSTEBZ
  
} // end of function 


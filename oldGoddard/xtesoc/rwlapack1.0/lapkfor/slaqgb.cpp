/*
 * C++ implementation of Lapack routine slaqgb
 *
 * $Id: slaqgb.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:00:11
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: slaqgb.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ONE = 1.0e0;
const float THRESH = 0.1e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ slaqgb(const long &m, const long &n, const long &kl, const long &ku, 
 float *ab, const long &ldab, float r[], float c[], float &rowcnd, float &colcnd, 
 float &amax, char &equed)
{
#define AB(I_,J_) (*(ab+(I_)*(ldab)+(J_)))
  long _do0, _do1, _do2, _do3, _do4, _do5, i, i_, j, j_;
  float cj, large, small;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SLAQGB equilibrates a general M by N band matrix A with KL
  //  subdiagonals and KU superdiagonals using the row and scaling factors
  //  in the vectors R and C.
  
  //  Arguments
  //  =========
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix A.  M >= 0.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix A.  N >= 0.
  
  //  KL      (input) INTEGER
  //          The number of subdiagonals within the band of A.  KL >= 0.
  
  //  KU      (input) INTEGER
  //          The number of superdiagonals within the band of A.  KU >= 0.
  
  //  AB      (input/output) REAL array, dimension (LDAB,N)
  //          On entry, the matrix A in band storage, in rows 1 to KL+KU+1.
  //          The j-th column of A is stored in the j-th column of the
  //          array AB as follows:
  //          AB(ku+1+i-j,j) = A(i,j) for max(1,j-ku)<=i<=min(m,j+kl)
  
  //          On exit, the equilibrated matrix, in the same storage format
  //          as A.  See EQUED for the form of the equilibrated matrix.
  
  //  LDAB    (input) INTEGER
  //          The leading dimension of the array AB.  LDA >= KL+KU+1.
  
  //  R       (output) REAL array, dimension (M)
  //          The row scale factors for A.
  
  //  C       (output) REAL array, dimension (N)
  //          The column scale factors for A.
  
  //  ROWCND  (output) REAL
  //          Ratio of the smallest R(i) to the largest R(i).
  
  //  COLCND  (output) REAL
  //          Ratio of the smallest C(i) to the largest C(i).
  
  //  AMAX    (input) REAL
  //          Absolute value of largest matrix entry.
  
  //  EQUED   (output) CHARACTER*1
  //          Specifies the form of equilibration that was done.
  //          = 'N':  No equilibration
  //          = 'R':  Row equilibration, i.e., A has been premultiplied by
  //                  diag(R).
  //          = 'C':  Column equilibration, i.e., A has been postmultiplied
  //                  by diag(C).
  //          = 'B':  Both row and column equilibration, i.e., A has been
  //                  replaced by diag(R) * A * diag(C).
  
  //  Internal Parameters
  //  ===================
  
  //  THRESH is a threshold value used to decide if row or column scaling
  //  should be done based on the ratio of the row or column scaling
  //  factors.  If ROWCND < THRESH, row scaling is done, and if
  //  COLCND < THRESH, column scaling is done.
  
  //  LARGE and SMALL are threshold values used to decide if row scaling
  //  should be done based on the absolute size of the largest matrix
  //  element.  If AMAX > LARGE or AMAX < SMALL, row scaling is done.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Quick return if possible
  
  if( m <= 0 || n <= 0 ) { 
    equed = 'N';
    return;
  }
  
  //     Initialize LARGE and SMALL.
  
  small = slamch( 'S'/*Safe minimum*/ )/slamch( 'P'/*Precision*/
    );
  large = ONE/small;
  
  if( (rowcnd >= THRESH && amax >= small) && amax <= large ) { 
    
    //        No row scaling
    
    if( colcnd >= THRESH ) { 
      
      //           No column scaling
      
      equed = 'N';
    }
    else { 
      
      //           Column scaling
      
      for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
        cj = c[j_];
        for( i = max( 1, j - ku ), i_ = i - 1, _do1 = min( m, 
         j + kl ); i <= _do1; i++, i_++ ) { 
          AB(j_,ku + 1 + i_ - j) = cj*AB(j_,ku + 1 + i_ - j);
        }
      }
      equed = 'C';
    }
  }
  else if( colcnd >= THRESH ) { 
    
    //        Row scaling, no column scaling
    
    for( j = 1, j_ = j - 1, _do2 = n; j <= _do2; j++, j_++ ) { 
      for( i = max( 1, j - ku ), i_ = i - 1, _do3 = min( m, 
       j + kl ); i <= _do3; i++, i_++ ) { 
        AB(j_,ku + 1 + i_ - j) = r[i_]*AB(j_,ku + 1 + i_ - j);
      }
    }
    equed = 'R';
  }
  else { 
    
    //        Row and column scaling
    
    for( j = 1, j_ = j - 1, _do4 = n; j <= _do4; j++, j_++ ) { 
      cj = c[j_];
      for( i = max( 1, j - ku ), i_ = i - 1, _do5 = min( m, 
       j + kl ); i <= _do5; i++, i_++ ) { 
        AB(j_,ku + 1 + i_ - j) = cj*r[i_]*AB(j_,ku + 1 + i_ - j);
      }
    }
    equed = 'B';
  }
  
  return;
  
  //     End of SLAQGB
  
#undef  AB
} // end of function 


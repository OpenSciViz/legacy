/*
 * C++ implementation of Lapack routine zrot
 *
 * $Id: zrot.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:50:26
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zrot.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

RWLAPKDECL void /*FUNCTION*/ zrot(const long &n, DComplex cx[], const long &incx, 
   DComplex cy[], const long &incy, const double &c, const DComplex &s)
{
  long _do0, _do1, i, i_, ix, iy;
  DComplex stemp;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZROT   applies a plane rotation, where the cos (C) is real and the
  //  sin (S) is DComplex, and the vectors CX and CY are DComplex.
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The number of elements in the vectors CX and CY.
  
  //  CX      (input/output) COMPLEX*16 array, dimension (N)
  //          On input, the vector X.
  //          On output, CX is overwritten with C*X + S*Y.
  
  //  INCX    (input) INTEGER
  //          The increment between successive values of CY.  INCX <> 0.
  
  //  CY      (input/output) COMPLEX*16 array, dimension (N)
  //          On input, the vector Y.
  //          On output, CY is overwritten with -CONJG(S)*X + C*Y.
  
  //  INCY    (input) INTEGER
  //          The increment between successive values of CY.  INCX <> 0.
  
  //  C       (input) DOUBLE PRECISION
  //  S       (input) COMPLEX*16
  //          C and S define a rotation
  //             [  C          S  ]
  //             [ -conjg(S)   C  ]
  //          where C*C + S*CONJG(S) = 1.0.
  
  
  //     .. Local Scalars ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  if( n <= 0 ) 
    return;
  if( incx == 1 && incy == 1 ) 
    goto L_20;
  
  //     Code for unequal increments or equal increments not equal to 1
  
  ix = 1;
  iy = 1;
  if( incx < 0 ) 
    ix = (-n + 1)*incx + 1;
  if( incy < 0 ) 
    iy = (-n + 1)*incy + 1;
  for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
    stemp = c*cx[ix - 1] + s*cy[iy - 1];
    cy[iy - 1] = c*cy[iy - 1] - conj( s )*cx[ix - 1];
    cx[ix - 1] = stemp;
    ix = ix + incx;
    iy = iy + incy;
  }
  return;
  
  //     Code for both increments equal to 1
  
L_20:
  ;
  for( i = 1, i_ = i - 1, _do1 = n; i <= _do1; i++, i_++ ) { 
    stemp = c*cx[i_] + s*cy[i_];
    cy[i_] = c*cy[i_] - conj( s )*cx[i_];
    cx[i_] = stemp;
  }
  return;
} // end of function 


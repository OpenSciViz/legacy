/*
 * C++ implementation of Lapack routine zgbtrf
 *
 * $Id: zgbtrf.cpp,v 1.2 1993/07/07 23:00:32 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:45:59
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zgbtrf.cpp,v $
 * Revision 1.2  1993/07/07  23:00:32  alv
 * NBMAX reduced for DOS to avoid running out of memory
 *
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const DComplex ONE = DComplex(1.0e0);
const DComplex ZERO = DComplex(0.0e0);
#if defined(__MSDOS__)
const long NBMAX = 8;
#else
const long NBMAX = 64;
#endif
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zgbtrf(const long &m, const long &n, const long &kl, const long &ku, 
 DComplex *ab, const long &ldab, long ipiv[], long &info)
{
#define AB(I_,J_) (*(ab+(I_)*(ldab)+(J_)))
  // PARAMETER translations
  const long LDWORK = NBMAX + 1;
  // end of PARAMETER translations

  long _do0, _do1, _do10, _do11, _do12, _do13, _do14, _do15, 
   _do16, _do17, _do18, _do2, _do3, _do4, _do5, _do6, _do7, _do8, 
   _do9, _i, i, i2, i3, i_, ii, ii_, ip, j, j2, j3, j_, jb, jj, 
   jj_, jm, jp, ju, k2, km, kv, nb, nw;
  DComplex temp;
#define NALLOC  2
  void *_alloc[NALLOC];
  DComplex (*const work13)[LDWORK] = (DComplex(*)[LDWORK])(_alloc[0]=(void*)new DComplex[NBMAX*
   LDWORK]);
  DComplex (*const work31)[LDWORK] = (DComplex(*)[LDWORK])(_alloc[1]=(void*)new DComplex[NBMAX*
   LDWORK]);
  for( _i=0; _i < NALLOC; _i++ ) { 
    if( _alloc[_i] == NULL )
      memerr( "unable to allocate object in zgbtrf()" );
  }

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZGBTRF computes an LU factorization of a DComplex m-by-n band matrix A
  //  using partial pivoting with row interchanges.
  
  //  This is the blocked version of the algorithm, calling Level 3 BLAS.
  
  //  Arguments
  //  =========
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix A.  M >= 0.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix A.  N >= 0.
  
  //  KL      (input) INTEGER
  //          The number of subdiagonals within the band of A.  KL >= 0.
  
  //  KU      (input) INTEGER
  //          The number of superdiagonals within the band of A.  KU >= 0.
  
  //  AB      (input/output) COMPLEX*16 array, dimension (LDAB,N)
  //          On entry, the matrix A in band storage, in rows KL+1 to
  //          2*KL+KU+1; rows 1 to KL of the array need not be set.
  //          The j-th column of A is stored in the j-th column of the
  //          array AB as follows:
  //          AB(kl+ku+1+i-j,j) = A(i,j) for max(1,j-ku)<=i<=min(m,j+kl)
  
  //          On exit, details of the factorization: U is stored as an
  //          upper triangular band matrix with KL+KU superdiagonals in
  //          rows 1 to KL+KU+1, and the multipliers used during the
  //          factorization are stored in rows KL+KU+2 to 2*KL+KU+1.
  //          See below for further details.
  
  //  LDAB    (input) INTEGER
  //          The leading dimension of the array AB.  LDAB >= 2*KL+KU+1.
  
  //  IPIV    (output) INTEGER array, dimension (min(M,N))
  //          The pivot indices; for 1 <= i <= min(M,N), row i of the
  //          matrix was interchanged with row IPIV(i).
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument had an illegal value
  //          > 0: if INFO = +i, U(i,i) is exactly zero. The factorization
  //               has been completed, but the factor U is exactly
  //               singular, and division by zero will occur if it is used
  //               to solve a system of equations.
  
  //  Further Details
  //  ===============
  
  //  The band storage scheme is illustrated by the following example, when
  //  M = N = 6, KL = 2, KU = 1:
  
  //  On entry:                       On exit:
  
  //      *    *    *    +    +    +       *    *    *   u14  u25  u36
  //      *    *    +    +    +    +       *    *   u13  u24  u35  u46
  //      *   a12  a23  a34  a45  a56      *   u12  u23  u34  u45  u56
  //     a11  a22  a33  a44  a55  a66     u11  u22  u33  u44  u55  u66
  //     a21  a32  a43  a54  a65   *      m21  m32  m43  m54  m65   *
  //     a31  a42  a53  a64   *    *      m31  m42  m53  m64   *    *
  
  //  Array elements marked * are not used by the routine; elements marked
  //  + need not be set on entry, but are required by the routine to store
  //  elements of U because of fill-in resulting from the row interchanges.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Local Arrays ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     KV is the number of superdiagonals in the factor U, allowing for
  //     fill-in
  
  kv = ku + kl;
  
  //     Test the input parameters.
  
  info = 0;
  if( m < 0 ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( kl < 0 ) { 
    info = -3;
  }
  else if( ku < 0 ) { 
    info = -4;
  }
  else if( ldab < kl + kv + 1 ) { 
    info = -6;
  }
  if( info != 0 ) { 
    xerbla( "ZGBTRF", -info );
    for( _i=0; _i < NALLOC; _i++ )
      delete _alloc[_i];
    return;
  }
  
  //     Quick return if possible
  
  if( m == 0 || n == 0 )  {
    for( _i=0; _i < NALLOC; _i++ )
      delete _alloc[_i];
    return;
  }
  
  //     Determine the block size for this environment
  
  nb = ilaenv( 1, "ZGBTRF", " ", m, n, kl, ku );
  
  //     The block size must not exceed the limit set by the size of the
  //     local arrays WORK13 and WORK31.
  
  nb = min( nb, NBMAX );
  
  if( nb <= 1 || nb > kl ) { 
    
    //        Use unblocked code
    
    zgbtf2( m, n, kl, ku, ab, ldab, ipiv, info );
  }
  else { 
    
    //        Use blocked code
    
    //        Zero the superdiagonal elements of the work array WORK13
    
    for( j = 1, j_ = j - 1, _do0 = nb; j <= _do0; j++, j_++ ) { 
      for( i = 1, i_ = i - 1, _do1 = j - 1; i <= _do1; i++, i_++ ) { 
        work13[j_][i_] = ZERO;
      }
    }
    
    //        Zero the subdiagonal elements of the work array WORK31
    
    for( j = 1, j_ = j - 1, _do2 = nb; j <= _do2; j++, j_++ ) { 
      for( i = j + 1, i_ = i - 1, _do3 = nb; i <= _do3; i++, i_++ ) { 
        work31[j_][i_] = ZERO;
      }
    }
    
    //        Gaussian elimination with partial pivoting
    
    //        Set fill-in elements in columns KU+2 to KV to zero
    
    for( j = ku + 2, j_ = j - 1, _do4 = min( kv, n ); j <= _do4; j++, j_++ ) { 
      for( i = kv - j + 2, i_ = i - 1, _do5 = kl; i <= _do5; i++, i_++ ) { 
        AB(j_,i_) = ZERO;
      }
    }
    
    //        JU is the index of the last column affected by the current
    //        stage of the factorization
    
    ju = 1;
    
    for( j = 1, j_ = j - 1, _do6=docnt(j,min( m, n ),_do7 = nb); _do6 > 0; j += _do7, j_ += _do7, _do6-- ) { 
      jb = min( nb, min( m, n ) - j + 1 );
      
      //           The active part of the matrix is partitioned
      
      //              A11   A12   A13
      //              A21   A22   A23
      //              A31   A32   A33
      
      //           Here A11, A21 and A31 denote the current block of JB columns
      //           which is about to be factorized. The number of rows in the
      //           partitioning are JB, I2, I3 respectively, and the numbers
      //           of columns are JB, J2, J3. The superdiagonal elements of A13
      //           and the subdiagonal elements of A31 lie outside the band.
      
      i2 = min( kl - jb, m - j - jb + 1 );
      i3 = min( jb, m - j - kl + 1 );
      
      //           J2 and J3 are computed after JU has been updated.
      
      //           Factorize the current block of JB columns
      
      for( jj = j, jj_ = jj - 1, _do8 = j + jb - 1; jj <= _do8; jj++, jj_++ ) { 
        
        //              Set fill-in elements in column JJ+KV to zero
        
        if( jj + kv <= n ) { 
          for( i = 1, i_ = i - 1, _do9 = kl; i <= _do9; i++, i_++ ) { 
            AB(jj_ + kv,i_) = ZERO;
          }
        }
        
        //              Find pivot and test for singularity. KM is the number of
        //              subdiagonal elements in the current column.
        
        km = min( kl, m - jj );
        jp = izamax( km + 1, &AB(jj_,kv), 1 );
        ipiv[jj_] = jp + jj - j;
        if( ctocf(AB(jj_,kv + jp - 1)) != ctocf(ZERO) ) { 
          ju = max( ju, min( jj + ku + jp - 1, n ) );
          if( jp != 1 ) { 
            
            //                    Apply interchange to columns J to J+JB-1
            
            if( jp + jj - 1 < j + kl ) { 
              
              zswap( jb, &AB(j_,kv + 1 + jj_ - j), ldab - 
               1, &AB(j_,kv + jp + jj_ - j), ldab - 
               1 );
            }
            else { 
              
              //                       The interchange affects columns J to JJ-1 of A31
              //                       which are stored in the work array WORK31
              
              zswap( jj - j, &AB(j_,kv + 1 + jj_ - j), 
               ldab - 1, &work31[0][jp + jj_ - j - kl], 
               LDWORK );
              zswap( j + jb - jj, &AB(jj_,kv), ldab - 
               1, &AB(jj_,kv + jp - 1), ldab - 1 );
            }
          }
          
          //                 Compute multipliers
          
          zscal( km, ONE/AB(jj_,kv), &AB(jj_,kv + 1), 1 );
          
          //                 Update trailing submatrix within the band and within
          //                 the current block. JM is the index of the last column
          //                 which needs to be updated.
          
          jm = min( ju, j + jb - 1 );
          if( jm > jj ) 
            zgeru( km, jm - jj, -(ONE), &AB(jj_,kv + 1), 
             1, &AB(jj_ + 1,kv - 1), ldab - 1, &AB(jj_ + 1,kv), 
             ldab - 1 );
        }
        else { 
          
          //                 If pivot is zero, set INFO to the index of the pivot
          //                 unless a zero pivot has already been found.
          
          if( info == 0 ) 
            info = jj;
        }
        
        //              Copy current column of A31 into the work array WORK31
        
        nw = min( jj - j + 1, i3 );
        if( nw > 0 ) 
          zcopy( nw, &AB(jj_,kv + kl + 1 - jj + j_), 1, 
           &work31[jj_ - j + 1][0], 1 );
      }
      if( j + jb <= n ) { 
        
        //              Apply the row interchanges to the other blocks.
        
        j2 = min( ju - j + 1, kv ) - jb;
        j3 = max( 0, ju - j - kv + 1 );
        
        //              Use ZLASWP to apply the row interchanges to A12, A22, and
        //              A32.
        
        zlaswp( j2, &AB(j_ + jb,kv - jb), ldab - 1, 1, jb, 
         &ipiv[j_], 1 );
        
        //              Adjust the pivot indices.
        
        for( i = j, i_ = i - 1, _do10 = j + jb - 1; i <= _do10; i++, i_++ ) { 
          ipiv[i_] = ipiv[i_] + j - 1;
        }
        
        //              Apply the row interchanges to A13, A23, and A33
        //              columnwise.
        
        k2 = j - 1 + jb + j2;
        for( i = 1, i_ = i - 1, _do11 = j3; i <= _do11; i++, i_++ ) { 
          jj = k2 + i;
          for( ii = j + i - 1, ii_ = ii - 1, _do12 = j + 
           jb - 1; ii <= _do12; ii++, ii_++ ) { 
            ip = ipiv[ii_];
            if( ip != ii ) { 
              temp = AB(jj - 1,kv + 1 + ii_ - jj);
              AB(jj - 1,kv + 1 + ii_ - jj) = AB(jj - 1,kv + ip - jj);
              AB(jj - 1,kv + ip - jj) = temp;
            }
          }
        }
        
        //              Update the relevant part of the trailing submatrix
        
        if( j2 > 0 ) { 
          
          //                 Update A12
          
          ztrsm( 'L'/*Left*/, 'L'/*Lower*/, 'N'/*No transpose*/
           , 'U'/*Unit*/, jb, j2, ONE, &AB(j_,kv), ldab - 
           1, &AB(j_ + jb,kv - jb), ldab - 1 );
          
          if( i2 > 0 ) { 
            
            //                    Update A22
            
            zgemm( 'N'/*No transpose*/, 'N'/*No transpose*/
             , i2, j2, jb, -(ONE), &AB(j_,kv + jb), ldab - 
             1, &AB(j_ + jb,kv - jb), ldab - 1, ONE, &AB(j_ + jb,kv), 
             ldab - 1 );
          }
          
          if( i3 > 0 ) { 
            
            //                    Update A32
            
            zgemm( 'N'/*No transpose*/, 'N'/*No transpose*/
             , i3, j2, jb, -(ONE), (DComplex*)work31, LDWORK, 
             &AB(j_ + jb,kv - jb), ldab - 1, ONE, &AB(j_ + jb,kv + kl - jb), 
             ldab - 1 );
          }
        }
        
        if( j3 > 0 ) { 
          
          //                 Copy the lower triangle of A13 into the work array
          //                 WORK13
          
          for( jj = 1, jj_ = jj - 1, _do13 = j3; jj <= _do13; jj++, jj_++ ) { 
            for( ii = jj, ii_ = ii - 1, _do14 = jb; ii <= _do14; ii++, ii_++ ) { 
              work13[jj_][ii_] = AB(jj_ + j + kv - 1,ii_ - jj + 1);
            }
          }
          
          //                 Update A13 in the work array
          
          ztrsm( 'L'/*Left*/, 'L'/*Lower*/, 'N'/*No transpose*/
           , 'U'/*Unit*/, jb, j3, ONE, &AB(j_,kv), ldab - 
           1, (DComplex*)work13, LDWORK );
          
          if( i2 > 0 ) { 
            
            //                    Update A23
            
            zgemm( 'N'/*No transpose*/, 'N'/*No transpose*/
             , i2, j3, jb, -(ONE), &AB(j_,kv + jb), ldab - 
             1, (DComplex*)work13, LDWORK, ONE, &AB(j_ + kv,jb), 
             ldab - 1 );
          }
          
          if( i3 > 0 ) { 
            
            //                    Update A33
            
            zgemm( 'N'/*No transpose*/, 'N'/*No transpose*/
             , i3, j3, jb, -(ONE), (DComplex*)work31, LDWORK, 
             (DComplex*)work13, LDWORK, ONE, &AB(j_ + kv,kl), 
             ldab - 1 );
          }
          
          //                 Copy the lower triangle of A13 back into place
          
          for( jj = 1, jj_ = jj - 1, _do15 = j3; jj <= _do15; jj++, jj_++ ) { 
            for( ii = jj, ii_ = ii - 1, _do16 = jb; ii <= _do16; ii++, ii_++ ) { 
              AB(jj_ + j + kv - 1,ii_ - jj + 1) = work13[jj_][ii_];
            }
          }
        }
      }
      else { 
        
        //              Adjust the pivot indices.
        
        for( i = j, i_ = i - 1, _do17 = j + jb - 1; i <= _do17; i++, i_++ ) { 
          ipiv[i_] = ipiv[i_] + j - 1;
        }
      }
      
      //           Partially undo the interchanges in the current block to
      //           restore the upper triangular form of A31 and copy the upper
      //           triangle of A31 back into place
      
      for( jj = j + jb - 1, jj_ = jj - 1, _do18 = j; jj >= _do18; jj--, jj_-- ) { 
        jp = ipiv[jj_] - jj + 1;
        if( jp != 1 ) { 
          
          //                 Apply interchange to columns J to JJ-1
          
          if( jp + jj - 1 < j + kl ) { 
            
            //                    The interchange does not affect A31
            
            zswap( jj - j, &AB(j_,kv + 1 + jj_ - j), ldab - 
             1, &AB(j_,kv + jp + jj_ - j), ldab - 1 );
          }
          else { 
            
            //                    The interchange does affect A31
            
            zswap( jj - j, &AB(j_,kv + 1 + jj_ - j), ldab - 
             1, &work31[0][jp + jj_ - j - kl], LDWORK );
          }
        }
        
        //              Copy the current column of A31 back into place
        
        nw = min( i3, jj - j + 1 );
        if( nw > 0 ) 
          zcopy( nw, &work31[jj_ - j + 1][0], 1, &AB(jj_,kv + kl + 1 - jj + j_), 
           1 );
      }
    }
  }
  
  for( _i=0; _i < NALLOC; _i++ )
    delete _alloc[_i];
  return;
  
  //     End of ZGBTRF
  
#undef NALLOC
#undef  AB
} // end of function 


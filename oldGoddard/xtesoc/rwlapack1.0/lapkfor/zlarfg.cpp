/*
 * C++ implementation of Lapack routine zlarfg
 *
 * $Id: zlarfg.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:49:02
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zlarfg.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ONE = 1.0e0;
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zlarfg(const long &n, DComplex &alpha, DComplex x[], 
 const long &incx, DComplex &tau)
{
  long _do0, j, j_, knt;
  double alphi, alphr, beta, rsafmn, safmin, xnorm;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZLARFG generates a DComplex elementary reflector H of order n, such
  //  that
  
  //        H' * ( alpha ) = ( beta ),   H' * H = I.
  //             (   x   )   (   0  )
  
  //  where alpha and beta are scalars, with beta real, and x is an
  //  (n-1)-element DComplex vector. H is represented in the form
  
  //        H = I - tau * ( 1 ) * ( 1 v' ) ,
  //                      ( v )
  
  //  where tau is a DComplex scalar and v is a DComplex (n-1)-element
  //  vector. Note that H is not hermitian.
  
  //  If the elements of x are all zero and alpha is real, then tau = 0
  //  and H is taken to be the unit matrix.
  
  //  Otherwise  1 <= real(tau) <= 2  and  abs(tau-1) <= 1 .
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The order of the elementary reflector.
  
  //  ALPHA   (input/output) COMPLEX*16
  //          On entry, the value alpha.
  //          On exit, it is overwritten with the value beta.
  
  //  X       (input/output) COMPLEX*16 array, dimension
  //                         (1+(N-2)*abs(INCX))
  //          On entry, the vector x.
  //          On exit, it is overwritten with the vector v.
  
  //  INCX    (input) INTEGER
  //          The increment between elements of X. INCX <> 0.
  
  //  TAU     (output) COMPLEX*16
  //          The value tau.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Executable Statements ..
  
  if( n <= 0 ) { 
    tau = DComplex(ZERO);
    return;
  }
  
  xnorm = dznrm2( n - 1, x, incx );
  alphr = real( alpha );
  alphi = imag( alpha );
  
  if( xnorm == ZERO && alphi == ZERO ) { 
    
    //        H  =  I
    
    tau = DComplex(ZERO);
  }
  else { 
    
    //        general case
    
    beta = -sign( dlapy3( alphr, alphi, xnorm ), alphr );
    safmin = dlamch( 'S' );
    rsafmn = ONE/safmin;
    
    if( abs( beta ) < safmin ) { 
      
      //           XNORM, BETA may be inaccurate; scale X and recompute them
      
      knt = 0;
L_10:
      ;
      knt = knt + 1;
      zdscal( n - 1, rsafmn, x, incx );
      beta = beta*rsafmn;
      alphi = alphi*rsafmn;
      alphr = alphr*rsafmn;
      if( abs( beta ) < safmin ) 
        goto L_10;
      
      //           New BETA is at most 1, at least SAFMIN
      
      xnorm = dznrm2( n - 1, x, incx );
      alpha = DComplex( alphr, alphi );
      beta = -sign( dlapy3( alphr, alphi, xnorm ), alphr );
      tau = DComplex( (beta - alphr)/beta, -alphi/beta );
      alpha = zladiv( DComplex( ONE, 0. ), alpha - beta );
      zscal( n - 1, alpha, x, incx );
      
      //           If ALPHA is subnormal, it may lose relative accuracy
      
      alpha = DComplex(beta);
      for( j = 1, j_ = j - 1, _do0 = knt; j <= _do0; j++, j_++ ) { 
        alpha = alpha*safmin;
      }
    }
    else { 
      tau = DComplex( (beta - alphr)/beta, -alphi/beta );
      alpha = zladiv( DComplex( ONE, 0. ), alpha - beta );
      zscal( n - 1, alpha, x, incx );
      alpha = DComplex(beta);
    }
  }
  
  return;
  
  //     End of ZLARFG
  
} // end of function 


/*
 * C++ implementation of lapack routine dlauum
 *
 * $Id: dlauum.cpp,v 1.6 1993/04/06 20:41:35 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:36:32
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dlauum.cpp,v $
 * Revision 1.6  1993/04/06  20:41:35  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.5  1993/03/19  18:41:23  alv
 * now passes chars explicitly, rather than indirection of a string, to shut up SUN warnings
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:16:06  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:07:52  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dlauum(const char &uplo, const long &n, double *a, const long &lda, 
 long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  char _c0[2];
  int upper;
  long _do0, _do1, _do2, _do3, i, i_, ib, nb;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DLAUUM computes the product U * U' or L' * L, where the triangular
  //  factor U or L is stored in the upper or lower triangular part of
  //  the array A.
  
  //  If UPLO = 'U' or 'u' then the upper triangle of the result is stored,
  //  overwriting the factor U in A.
  //  If UPLO = 'L' or 'l' then the lower triangle of the result is stored,
  //  overwriting the factor L in A.
  
  //  This is the blocked form of the algorithm, calling Level 3 BLAS.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the triangular factor stored in the array A
  //          is upper or lower triangular:
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The order of the triangular factor U or L.  N >= 0.
  
  //  A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)
  //          On entry, the triangular factor U or L.
  //          On exit, if UPLO = 'U', the upper triangle of A is
  //          overwritten with the upper triangle of the product U * U';
  //          if UPLO = 'L', the lower triangle of A is overwritten with
  //          the lower triangle of the product L' * L.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( lda < max( 1, n ) ) { 
    info = -4;
  }
  if( info != 0 ) { 
    xerbla( "DLAUUM", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  //     Determine the block size for this environment.
  
  nb = ilaenv( 1, "DLAUUM", STR1(_c0,uplo), n, -1, -1, -1 );
  
  if( nb <= 1 || nb >= n ) { 
    
    //        Use unblocked code
    
    dlauu2( uplo, n, a, lda, info );
  }
  else { 
    
    //        Use blocked code
    
    if( upper ) { 
      
      //           Compute the product U * U'.
      
      for( i = 1, i_ = i - 1, _do0=docnt(i,n,_do1 = nb); _do0 > 0; i += _do1, i_ += _do1, _do0-- ) { 
        ib = min( nb, n - i + 1 );
        dtrmm( 'R'/* Right */, 'U'/* Upper */, 'T'/* Transpose */
         , 'N'/* Non-unit */, i - 1, ib, ONE, &A(i_,i_), lda, 
         &A(i_,0), lda );
        dlauu2( 'U'/* Upper */, ib, &A(i_,i_), lda, info );
        if( i + ib <= n ) { 
          dgemm( 'N'/* No transpose */, 'T'/* Transpose */
           , i - 1, ib, n - i - ib + 1, ONE, &A(i_ + ib,0), 
           lda, &A(i_ + ib,i_), lda, ONE, &A(i_,0), lda );
          dsyrk( 'U'/* Upper */, 'N'/* No transpose */, ib, 
           n - i - ib + 1, ONE, &A(i_ + ib,i_), lda, ONE, 
           &A(i_,i_), lda );
        }
      }
    }
    else { 
      
      //           Compute the product L' * L.
      
      for( i = 1, i_ = i - 1, _do2=docnt(i,n,_do3 = nb); _do2 > 0; i += _do3, i_ += _do3, _do2-- ) { 
        ib = min( nb, n - i + 1 );
        dtrmm( 'L'/* Left */, 'L'/* Lower */, 'T'/* Transpose */
         , 'N'/* Non-unit */, ib, i - 1, ONE, &A(i_,i_), lda, 
         &A(0,i_), lda );
        dlauu2( 'L'/* Lower */, ib, &A(i_,i_), lda, info );
        if( i + ib <= n ) { 
          dgemm( 'T'/* Transpose */, 'N'/* No transpose */
           , ib, i - 1, n - i - ib + 1, ONE, &A(i_,i_ + ib), 
           lda, &A(0,i_ + ib), lda, ONE, &A(0,i_), lda );
          dsyrk( 'L'/* Lower */, 'T'/* Transpose */, ib, n - 
           i - ib + 1, ONE, &A(i_,i_ + ib), lda, ONE, &A(i_,i_), 
           lda );
        }
      }
    }
  }
  
  return;
  
  //     End of DLAUUM
  
#undef  A
} // end of function 


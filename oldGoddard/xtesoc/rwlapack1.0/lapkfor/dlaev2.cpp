/*
 * C++ implementation of lapack routine dlaev2
 *
 * $Id: dlaev2.cpp,v 1.6 1993/04/06 20:40:56 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:35:07
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dlaev2.cpp,v $
 * Revision 1.6  1993/04/06  20:40:56  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.5  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.4  1993/03/19  16:57:20  alv
 * sprinkled in some const
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:15:12  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:07:09  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ONE = 1.0e0;
const double TWO = 2.0e0;
const double ZERO = 0.0e0;
const double HALF = 0.5e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dlaev2(const double &a, const double &b, const double &c, double &rt1, 
 double &rt2, double &cs1, double &sn1)
{
  long sgn1, sgn2;
  double ab, acmn, acmx, acs, adf, cs, ct, df, rt, sm, tb, tn;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DLAEV2 computes the eigendecomposition of a 2-by-2 symmetric matrix
  //     [  A   B  ]
  //     [  B   C  ].
  //  On return, RT1 is the eigenvalue of larger absolute value, RT2 is the
  //  eigenvalue of smaller absolute value, and (CS1,SN1) is the unit right
  //  eigenvector for RT1, giving the decomposition
  
  //     [ CS1  SN1 ] [  A   B  ] [ CS1 -SN1 ]  =  [ RT1  0  ]
  //     [-SN1  CS1 ] [  B   C  ] [ SN1  CS1 ]     [  0  RT2 ].
  
  //  Arguments
  //  =========
  
  //  A       (input) DOUBLE PRECISION
  //          The (1,1) entry of the 2-by-2 matrix.
  
  //  B       (input) DOUBLE PRECISION
  //          The (1,2) entry and the conjugate of the (2,1) entry of the
  //          2-by-2 matrix.
  
  //  C       (input) DOUBLE PRECISION
  //          The (2,2) entry of the 2-by-2 matrix.
  
  //  RT1     (output) DOUBLE PRECISION
  //          The eigenvalue of larger absolute value.
  
  //  RT2     (output) DOUBLE PRECISION
  //          The eigenvalue of smaller absolute value.
  
  //  CS1     (output) DOUBLE PRECISION
  //  SN1     (output) DOUBLE PRECISION
  //          The vector (CS1, SN1) is a unit right eigenvector for RT1.
  
  //  Further Details
  //  ===============
  
  //  RT1 is accurate to a few ulps barring over/underflow.
  
  //  RT2 may be inaccurate if there is massive cancellation in the
  //  determinant A*C-B*B; higher precision or correctly rounded or
  //  correctly truncated arithmetic would be needed to compute RT2
  //  accurately in all cases.
  
  //  CS1 and SN1 are accurate to a few ulps barring over/underflow.
  
  //  Overflow is possible only if RT1 is within a factor of 5 of overflow.
  //  Underflow is harmless if the input data is 0 or exceeds
  //     underflow_threshold / macheps.
  
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Compute the eigenvalues
  
  sm = a + c;
  df = a - c;
  adf = abs( df );
  tb = b + b;
  ab = abs( tb );
  if( abs( a ) > abs( c ) ) { 
    acmx = a;
    acmn = c;
  }
  else { 
    acmx = c;
    acmn = a;
  }
  if( adf > ab ) { 
    rt = adf*sqrt( ONE + pow(ab/adf, 2) );
  }
  else if( adf < ab ) { 
    rt = ab*sqrt( ONE + pow(adf/ab, 2) );
  }
  else { 
    
    //        Includes case AB=ADF=0
    
    rt = ab*sqrt( TWO );
  }
  if( sm < ZERO ) { 
    rt1 = HALF*(sm - rt);
    sgn1 = -1;
    
    //        Order of execution important.
    //        To get fully accurate smaller eigenvalue,
    //        next line needs to be executed in higher precision.
    
    rt2 = (acmx/rt1)*acmn - (b/rt1)*b;
  }
  else if( sm > ZERO ) { 
    rt1 = HALF*(sm + rt);
    sgn1 = 1;
    
    //        Order of execution important.
    //        To get fully accurate smaller eigenvalue,
    //        next line needs to be executed in higher precision.
    
    rt2 = (acmx/rt1)*acmn - (b/rt1)*b;
  }
  else { 
    
    //        Includes case RT1 = RT2 = 0
    
    rt1 = HALF*rt;
    rt2 = -HALF*rt;
    sgn1 = 1;
  }
  
  //     Compute the eigenvector
  
  if( df >= ZERO ) { 
    cs = df + rt;
    sgn2 = 1;
  }
  else { 
    cs = df - rt;
    sgn2 = -1;
  }
  acs = abs( cs );
  if( acs > ab ) { 
    ct = -tb/cs;
    sn1 = ONE/sqrt( ONE + ct*ct );
    cs1 = ct*sn1;
  }
  else { 
    if( ab == ZERO ) { 
      cs1 = ONE;
      sn1 = ZERO;
    }
    else { 
      tn = -cs/tb;
      cs1 = ONE/sqrt( ONE + tn*tn );
      sn1 = tn*cs1;
    }
  }
  if( sgn1 == sgn2 ) { 
    tn = cs1;
    cs1 = -sn1;
    sn1 = tn;
  }
  return;
  
  //     End of DLAEV2
  
} // end of function 


/*
 * C++ implementation of Lapack routine sopgtr
 *
 * $Id: sopgtr.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:01:03
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: sopgtr.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ZERO = 0.0e0;
const float ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ sopgtr(const char &uplo, const long &n, float ap[], float tau[], 
 float *q, const long &ldq, float work[], long &info)
{
#define Q(I_,J_)  (*(q+(I_)*(ldq)+(J_)))
  int upper;
  long _do0, _do1, _do2, _do3, _do4, _do5, i, i_, iinfo, ij, 
   j, j_;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SOPGTR generates a real orthogonal matrix Q which is defined as the
  //  product of n-1 elementary reflectors of order n, as returned by
  //  SSPTRD using packed storage:
  
  //  if UPLO = 'U', Q = H(n-1) . . . H(2) H(1),
  
  //  if UPLO = 'L', Q = H(1) H(2) . . . H(n-1).
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies the storage scheme used in the previous call of
  //          SSPTRD:
  //          = 'U': Upper triangular packed storage;
  //          = 'L': Lower triangular packed storage.
  
  //  N       (input) INTEGER
  //          The order of the matrix Q. N >= 0.
  
  //  AP      (input) REAL array, dimension (N*(N+1)/2)
  //          The vectors which define the elementary reflectors, as
  //          returned by SSPTRD.
  
  //  TAU     (input) REAL array, dimension (N-1)
  //          TAU(i) must contain the scalar factor of the elementary
  //          reflector H(i), as returned by SSPTRD.
  
  //  Q       (output) REAL array, dimension (LDQ,N)
  //          The n by n orthogonal matrix Q.
  
  //  LDQ     (input) INTEGER
  //          The leading dimension of the array Q. LDQ >= max(1,N).
  
  //  WORK    (workspace) REAL array, dimension (N-1)
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input arguments
  
  info = 0;
  upper = lsame( uplo, 'U' );
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( ldq < max( 1, n ) ) { 
    info = -6;
  }
  if( info != 0 ) { 
    xerbla( "SOPGTR", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  if( upper ) { 
    
    //        Q was determined by a call to SSPTRD with UPLO = 'U'
    
    //        Unpack the vectors which define the elementary reflectors and
    //        set the last row and column of Q equal to those of the unit
    //        matrix
    
    ij = 2;
    for( j = 1, j_ = j - 1, _do0 = n - 1; j <= _do0; j++, j_++ ) { 
      for( i = 1, i_ = i - 1, _do1 = j - 1; i <= _do1; i++, i_++ ) { 
        Q(j_,i_) = ap[ij - 1];
        ij = ij + 1;
      }
      ij = ij + 2;
      Q(j_,n - 1) = ZERO;
    }
    for( i = 1, i_ = i - 1, _do2 = n - 1; i <= _do2; i++, i_++ ) { 
      Q(n - 1,i_) = ZERO;
    }
    Q(n - 1,n - 1) = ONE;
    
    //        Generate Q(1:n-1,1:n-1)
    
    sorg2l( n - 1, n - 1, n - 1, q, ldq, tau, work, iinfo );
    
  }
  else { 
    
    //        Q was determined by a call to SSPTRD with UPLO = 'L'.
    
    //        Unpack the vectors which define the elementary reflectors and
    //        set the first row and column of Q equal to those of the unit
    //        matrix
    
    Q(0,0) = ONE;
    for( i = 2, i_ = i - 1, _do3 = n; i <= _do3; i++, i_++ ) { 
      Q(0,i_) = ZERO;
    }
    ij = 3;
    for( j = 2, j_ = j - 1, _do4 = n; j <= _do4; j++, j_++ ) { 
      Q(j_,0) = ZERO;
      for( i = j + 1, i_ = i - 1, _do5 = n; i <= _do5; i++, i_++ ) { 
        Q(j_,i_) = ap[ij - 1];
        ij = ij + 1;
      }
      ij = ij + 2;
    }
    if( n > 1 ) { 
      
      //           Generate Q(2:n,2:n)
      
      sorg2r( n - 1, n - 1, n - 1, &Q(1,1), ldq, tau, work, 
       iinfo );
    }
  }
  return;
  
  //     End of SOPGTR
  
#undef  Q
} // end of function 


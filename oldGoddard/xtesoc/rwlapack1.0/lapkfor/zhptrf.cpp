/*
 * C++ implementation of Lapack routine zhptrf
 *
 * $Id: zhptrf.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:47:55
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zhptrf.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
const double EIGHT = 8.0e0;
const double SEVTEN = 17.0e0;
// end of PARAMETER translations

inline double zhptrf_cabs1(DComplex zdum) { return abs( real( (zdum) ) ) + 
   abs( imag( (zdum) ) ); }
RWLAPKDECL void /*FUNCTION*/ zhptrf(const char &uplo, const long &n, DComplex ap[], long ipiv[], 
 long &info)
{
  int upper;
  long _do0, _do1, _do2, _do3, imax, j, j_, jmax, k, kc, kk, 
   knc, kp, kpc, kstep, kx, npp;
  double absakk, alpha, c, colmax, r1, r2, rowmax;
  DComplex s, t, zdum;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZHPTRF computes the factorization of a DComplex Hermitian packed
  //  matrix A using the Bunch-Kaufman diagonal pivoting method:
  
  //     A = U*D*U'  or  A = L*D*L'
  
  //  where U (or L) is a product of permutation and unit upper (lower)
  //  triangular matrices, U' is the conjugate transpose of U, and D is
  //  Hermitian and block diagonal with 1-by-1 and 2-by-2 diagonal blocks.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangular part of the
  //          Hermitian matrix A is stored:
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  AP      (input/output) COMPLEX*16 array, dimension (N*(N+1)/2)
  //          On entry, the upper or lower triangle of the Hermitian matrix
  //          A, packed columnwise in a linear array.  The j-th column of A
  //          is stored in the array AP as follows:
  //          if UPLO = 'U', AP(i + (j-1)*j/2) = A(i,j) for 1<=i<=j;
  //          if UPLO = 'L', AP(i + (j-1)*(2n-j)/2) = A(i,j) for j<=i<=n.
  
  //          On exit, the block diagonal matrix D and the multipliers used
  //          to obtain the factor U or L, stored as a packed triangular
  //          matrix overwriting A (see below for further details).
  
  //  IPIV    (output) INTEGER array, dimension (N)
  //          Details of the interchanges and the block structure of D.
  //          If IPIV(k) > 0, then rows and columns k and IPIV(k) were
  //          interchanged and D(k,k) is a 1-by-1 diagonal block.
  //          If UPLO = 'U' and IPIV(k) = IPIV(k-1) < 0, then rows and
  //          columns k-1 and -IPIV(k) were interchanged and D(k-1:k,k-1:k)
  //          is a 2-by-2 diagonal block.  If UPLO = 'L' and IPIV(k) =
  //          IPIV(k+1) < 0, then rows and columns k+1 and -IPIV(k) were
  //          interchanged and D(k:k+1,k:k+1) is a 2-by-2 diagonal block.
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: if INFO = k, D(k,k) is exactly zero.  The factorization
  //               has been completed, but the block diagonal matrix D is
  //               exactly singular, and division by zero will occur if it
  //               is used to solve a system of equations.
  
  //  Further Details
  //  ===============
  
  //  If UPLO = 'U', then A = U*D*U', where
  //     U = P(n)*U(n)* ... *P(k)U(k)* ...,
  //  i.e., U is a product of terms P(k)*U(k), where k decreases from n to
  //  1 in steps of 1 or 2, and D is a block diagonal matrix with 1-by-1
  //  and 2-by-2 diagonal blocks D(k).  P(k) is a permutation matrix as
  //  defined by IPIV(k), and U(k) is a unit upper triangular matrix, such
  //  that if the diagonal block D(k) is of order s (s = 1 or 2), then
  
  //             (   I    v    0   )   k-s
  //     U(k) =  (   0    I    0   )   s
  //             (   0    0    I   )   n-k
  //                k-s   s   n-k
  
  //  If s = 1, D(k) overwrites A(k,k), and v overwrites A(1:k-1,k).
  //  If s = 2, the upper triangle of D(k) overwrites A(k-1,k-1), A(k-1,k),
  //  and A(k,k), and v overwrites A(1:k-2,k-1:k).
  
  //  If UPLO = 'L', then A = L*D*L', where
  //     L = P(1)*L(1)* ... *P(k)*L(k)* ...,
  //  i.e., L is a product of terms P(k)*L(k), where k increases from 1 to
  //  n in steps of 1 or 2, and D is a block diagonal matrix with 1-by-1
  //  and 2-by-2 diagonal blocks D(k).  P(k) is a permutation matrix as
  //  defined by IPIV(k), and L(k) is a unit lower triangular matrix, such
  //  that if the diagonal block D(k) is of order s (s = 1 or 2), then
  
  //             (   I    0     0   )  k-1
  //     L(k) =  (   0    I     0   )  s
  //             (   0    v     I   )  n-k-s+1
  //                k-1   s  n-k-s+1
  
  //  If s = 1, D(k) overwrites A(k,k), and v overwrites A(k+1:n,k).
  //  If s = 2, the lower triangle of D(k) overwrites A(k,k), A(k+1,k),
  //  and A(k+1,k+1), and v overwrites A(k+2:n,k:k+1).
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Statement Functions ..
  //     ..
  //     .. Statement Function definitions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  if( info != 0 ) { 
    xerbla( "ZHPTRF", -info );
    return;
  }
  
  //     Initialize ALPHA for use in choosing pivot block size.
  
  alpha = (ONE + sqrt( SEVTEN ))/EIGHT;
  
  if( upper ) { 
    
    //        Factorize A as U*D*U' using the upper triangle of A
    
    //        K is the main loop index, decreasing from N to 1 in steps of
    //        1 or 2
    
    k = n;
    kc = (n - 1)*n/2 + 1;
L_10:
    ;
    knc = kc;
    
    //        If K < 1, exit from loop
    
    if( k < 1 ) 
      goto L_70;
    kstep = 1;
    
    //        Determine rows and columns to be interchanged and whether
    //        a 1-by-1 or 2-by-2 pivot block will be used
    
    absakk = abs( real( ap[kc + k - 2] ) );
    
    //        IMAX is the row-index of the largest off-diagonal element in
    //        column K, and COLMAX is its absolute value
    
    if( k > 1 ) { 
      imax = izamax( k - 1, &ap[kc - 1], 1 );
      colmax = zhptrf_cabs1( ap[kc + imax - 2] );
    }
    else { 
      colmax = ZERO;
    }
    
    if( max( absakk, colmax ) == ZERO ) { 
      
      //           Column K is zero: set INFO and continue
      
      if( info == 0 ) 
        info = k;
      kp = k;
    }
    else { 
      if( absakk >= alpha*colmax ) { 
        
        //              no interchange, use 1-by-1 pivot block
        
        kp = k;
      }
      else { 
        
        //              JMAX is the column-index of the largest off-diagonal
        //              element in row IMAX, and ROWMAX is its absolute value
        
        rowmax = ZERO;
        jmax = imax;
        kx = imax*(imax + 1)/2 + imax;
        for( j = imax + 1, j_ = j - 1, _do0 = k; j <= _do0; j++, j_++ ) { 
          if( zhptrf_cabs1( ap[kx - 1] ) > rowmax ) { 
            rowmax = zhptrf_cabs1( ap[kx - 1] );
            jmax = j;
          }
          kx = kx + j;
        }
        kpc = (imax - 1)*imax/2 + 1;
        if( imax > 1 ) { 
          jmax = izamax( imax - 1, &ap[kpc - 1], 1 );
          rowmax = max( rowmax, zhptrf_cabs1( ap[kpc + jmax - 2] ) );
        }
        
        if( absakk >= alpha*colmax*(colmax/rowmax) ) { 
          
          //                 no interchange, use 1-by-1 pivot block
          
          kp = k;
        }
        else if( abs( real( ap[kpc + imax - 2] ) ) >= alpha*
         rowmax ) { 
          
          //                 interchange rows and columns K and IMAX, use 1-by-1
          //                 pivot block
          
          kp = imax;
        }
        else { 
          
          //                 interchange rows and columns K-1 and IMAX, use 2-by-2
          //                 pivot block
          
          kp = imax;
          kstep = 2;
        }
      }
      
      kk = k - kstep + 1;
      if( kstep == 2 ) 
        knc = knc - k + 1;
      if( kp != kk ) { 
        
        //              Interchange rows and columns KK and KP in the leading
        //              submatrix A(1:k,1:k)
        
        zswap( kp, &ap[knc - 1], 1, &ap[kpc - 1], 1 );
        kx = knc + kp - 1;
        for( j = kk, j_ = j - 1, _do1 = kp; j >= _do1; j--, j_-- ) { 
          t = conj( ap[knc + j_ - 1] );
          ap[knc + j_ - 1] = conj( ap[kx - 1] );
          ap[kx - 1] = t;
          kx = kx - j + 1;
        }
        if( kstep == 2 ) { 
          t = ap[kc + k - 3];
          ap[kc + k - 3] = ap[kc + kp - 2];
          ap[kc + kp - 2] = t;
        }
      }
      
      //           Update the leading submatrix
      
      if( kstep == 1 ) { 
        
        //              1-by-1 pivot block D(k): column k now holds
        
        //              W(k) = U(k)*D(k)
        
        //              where U(k) is the k-th column of U
        
        //              Perform a rank-1 update of A(1:k-1,1:k-1) as
        
        //              A := A - U(k)*D(k)*U(k)' = A - W(k)*1/D(k)*W(k)'
        
        r1 = ONE/real( ap[kc + k - 2] );
        zhpr( uplo, k - 1, -r1, &ap[kc - 1], 1, ap );
        
        //              Store U(k) in column k
        
        zdscal( k - 1, r1, &ap[kc - 1], 1 );
      }
      else { 
        
        //              2-by-2 pivot block D(k): columns k and k-1 now hold
        
        //              ( W(k-1) W(k) ) = ( U(k-1) U(k) )*D(k)
        
        //              where U(k) and U(k-1) are the k-th and (k-1)-th columns
        //              of U
        
        //              Perform a rank-2 update of A(1:k-2,1:k-2) as
        
        //              A := A - ( U(k-1) U(k) )*D(k)*( U(k-1) U(k) )'
        //                 = A - ( W(k-1) W(k) )*inv(D(k))*( W(k-1) W(k) )'
        
        //              Convert this to two rank-1 updates by using the eigen-
        //              decomposition of D(k)
        
        zlaev2( ap[kc - 2], ap[kc + k - 3], ap[kc + k - 2], 
         r1, r2, c, s );
        r1 = ONE/r1;
        r2 = ONE/r2;
        zrot( k - 2, &ap[knc - 1], 1, &ap[kc - 1], 1, c, s );
        zhpr( uplo, k - 2, -r1, &ap[knc - 1], 1, ap );
        zhpr( uplo, k - 2, -r2, &ap[kc - 1], 1, ap );
        
        //              Store U(k) and U(k-1) in columns k and k-1
        
        zdscal( k - 2, r1, &ap[knc - 1], 1 );
        zdscal( k - 2, r2, &ap[kc - 1], 1 );
        zrot( k - 2, &ap[knc - 1], 1, &ap[kc - 1], 1, c, -(s) );
      }
    }
    
    //        Store details of the interchanges in IPIV
    
    if( kstep == 1 ) { 
      ipiv[k - 1] = kp;
    }
    else { 
      ipiv[k - 1] = -kp;
      ipiv[k - 2] = -kp;
    }
    
    //        Decrease K and return to the start of the main loop
    
    k = k - kstep;
    kc = knc - k;
    goto L_10;
    
  }
  else { 
    
    //        Factorize A as L*D*L' using the lower triangle of A
    
    //        K is the main loop index, increasing from 1 to N in steps of
    //        1 or 2
    
    k = 1;
    kc = 1;
    npp = n*(n + 1)/2;
L_40:
    ;
    knc = kc;
    
    //        If K > N, exit from loop
    
    if( k > n ) 
      goto L_70;
    kstep = 1;
    
    //        Determine rows and columns to be interchanged and whether
    //        a 1-by-1 or 2-by-2 pivot block will be used
    
    absakk = abs( real( ap[kc - 1] ) );
    
    //        IMAX is the row-index of the largest off-diagonal element in
    //        column K, and COLMAX is its absolute value
    
    if( k < n ) { 
      imax = k + izamax( n - k, &ap[kc], 1 );
      colmax = zhptrf_cabs1( ap[kc + imax - k - 1] );
    }
    else { 
      colmax = ZERO;
    }
    
    if( max( absakk, colmax ) == ZERO ) { 
      
      //           Column K is zero: set INFO and continue
      
      if( info == 0 ) 
        info = k;
      kp = k;
    }
    else { 
      if( absakk >= alpha*colmax ) { 
        
        //              no interchange, use 1-by-1 pivot block
        
        kp = k;
      }
      else { 
        
        //              JMAX is the column-index of the largest off-diagonal
        //              element in row IMAX, and ROWMAX is its absolute value
        
        rowmax = ZERO;
        kx = kc + imax - k;
        for( j = k, j_ = j - 1, _do2 = imax - 1; j <= _do2; j++, j_++ ) { 
          if( zhptrf_cabs1( ap[kx - 1] ) > rowmax ) { 
            rowmax = zhptrf_cabs1( ap[kx - 1] );
            jmax = j;
          }
          kx = kx + n - j;
        }
        kpc = npp - (n - imax + 1)*(n - imax + 2)/2 + 1;
        if( imax < n ) { 
          jmax = imax + izamax( n - imax, &ap[kpc], 1 );
          rowmax = max( rowmax, zhptrf_cabs1( ap[kpc + jmax - imax - 1] ) );
        }
        
        if( absakk >= alpha*colmax*(colmax/rowmax) ) { 
          
          //                 no interchange, use 1-by-1 pivot block
          
          kp = k;
        }
        else if( abs( real( ap[kpc - 1] ) ) >= alpha*rowmax ) { 
          
          //                 interchange rows and columns K and IMAX, use 1-by-1
          //                 pivot block
          
          kp = imax;
        }
        else { 
          
          //                 interchange rows and columns K+1 and IMAX, use 2-by-2
          //                 pivot block
          
          kp = imax;
          kstep = 2;
        }
      }
      
      kk = k + kstep - 1;
      if( kstep == 2 ) 
        knc = knc + n - k + 1;
      if( kp != kk ) { 
        
        //              Interchange rows and columns KK and KP in the trailing
        //              submatrix A(k:n,k:n)
        
        zswap( n - kp + 1, &ap[knc + kp - kk - 1], 1, &ap[kpc - 1], 
         1 );
        kx = knc + kp - kk;
        for( j = kk, j_ = j - 1, _do3 = kp; j <= _do3; j++, j_++ ) { 
          t = conj( ap[knc + j_ - kk] );
          ap[knc + j_ - kk] = conj( ap[kx - 1] );
          ap[kx - 1] = t;
          kx = kx + n - j;
        }
        if( kstep == 2 ) { 
          t = ap[kc];
          ap[kc] = ap[kc + kp - k - 1];
          ap[kc + kp - k - 1] = t;
        }
      }
      
      //           Update the trailing submatrix
      
      if( kstep == 1 ) { 
        
        //              1-by-1 pivot block D(k): column k now holds
        
        //              W(k) = L(k)*D(k)
        
        //              where L(k) is the k-th column of L
        
        if( k < n ) { 
          
          //                 Perform a rank-1 update of A(k+1:n,k+1:n) as
          
          //                 A := A - L(k)*D(k)*L(k)' = A - W(k)*(1/D(k))*W(k)'
          
          r1 = ONE/real( ap[kc - 1] );
          zhpr( uplo, n - k, -r1, &ap[kc], 1, &ap[kc + n - k] );
          
          //                 Store L(k) in column K
          
          zdscal( n - k, r1, &ap[kc], 1 );
        }
      }
      else { 
        
        //              2-by-2 pivot block D(k): columns K and K+1 now hold
        
        //              ( W(k) W(k+1) ) = ( L(k) L(k+1) )*D(k)
        
        //              where L(k) and L(k+1) are the k-th and (k+1)-th columns
        //              of L
        
        if( k < n - 1 ) { 
          
          //                 Perform a rank-2 update of A(k+2:n,k+2:n) as
          
          //                 A := A - ( L(k) L(k+1) )*D(k)*( L(k) L(k+1) )'
          //                    = A - ( W(k) W(k+1) )*inv(D(k))*( W(k) W(k+1) )'
          
          //                 Convert this to two rank-1 updates by using the eigen-
          //                 decomposition of D(k)
          
          zlaev2( ap[kc - 1], conj( ap[kc] ), ap[knc - 1], 
           r1, r2, c, s );
          r1 = ONE/r1;
          r2 = ONE/r2;
          zrot( n - k - 1, &ap[kc + 1], 1, &ap[knc], 1, 
           c, s );
          zhpr( uplo, n - k - 1, -r1, &ap[kc + 1], 1, &ap[knc + n - k - 1] );
          zhpr( uplo, n - k - 1, -r2, &ap[knc], 1, &ap[knc + n - k - 1] );
          
          //                 Store L(k) and L(k+1) in columns k and k+1
          
          zdscal( n - k - 1, r1, &ap[kc + 1], 1 );
          zdscal( n - k - 1, r2, &ap[knc], 1 );
          zrot( n - k - 1, &ap[kc + 1], 1, &ap[knc], 1, 
           c, -(s) );
        }
      }
    }
    
    //        Store details of the interchanges in IPIV
    
    if( kstep == 1 ) { 
      ipiv[k - 1] = kp;
    }
    else { 
      ipiv[k - 1] = -kp;
      ipiv[k] = -kp;
    }
    
    //        Increase K and return to the start of the main loop
    
    k = k + kstep;
    kc = knc + n - k + 2;
    goto L_40;
    
  }
  
L_70:
  ;
  return;
  
  //     End of ZHPTRF
  
} // end of function 


/*
 * C++ implementation of Lapack routine slanv2
 *
 * $Id: slanv2.cpp,v 1.3 1993/10/15 05:56:33 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:00:07
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: slanv2.cpp,v $
 * Revision 1.3  1993/10/15  05:56:33  alv
 * commented out unused info definition
 *
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ZERO = 0.0e0;
const float HALF = 0.5e0;
const float ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ slanv2(float &a, float &b, float &c, float &d, float &rt1r, 
 float &rt1i, float &rt2r, float &rt2i, float &cs, float &sn)
{
  // long info;
  float aa, bb, cc, cs1, dd, p, sab, sac, sigma, sn1, tau, temp;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SLANV2 computes the Schur factorization of a real 2-by-2 nonsymmetric
  //  matrix in standard form:
  
  //       [ A  B ] = [ CS -SN ] [ AA  BB ] [ CS  SN ]
  //       [ C  D ]   [ SN  CS ] [ CC  DD ] [-SN  CS ]
  
  //  where either
  //  1) CC = 0 so that AA and DD are real eigenvalues of the matrix, or
  //  2) AA = DD and BB*CC < 0, so that AA + or - sqrt(BB*CC) are DComplex
  //  conjugate eigenvalues.
  
  //  Arguments
  //  =========
  
  //  A       (input/output) REAL
  //  B       (input/output) REAL
  //  C       (input/output) REAL
  //  D       (input/output) REAL
  //          On entry, the elements of the input matrix.
  //          On exit, they are overwritten by the elements of the
  //          standardised Schur form.
  
  //  RT1R    (output) REAL
  //  RT1I    (output) REAL
  //  RT2R    (output) REAL
  //  RT2I    (output) REAL
  //          The real and imaginary parts of the eigenvalues. If the
  //          eigenvalues are both real, abs(RT1R) >= abs(RT2R); if the
  //          eigenvalues are a DComplex conjugate pair, RT1I > 0.
  
  //  CS      (output) REAL
  //  SN      (output) REAL
  //          Parameters of the rotation matrix.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  // info = 0;
  
  //     Initialize CS and SN
  
  cs = ONE;
  sn = ZERO;
  
  if( c == ZERO ) { 
    goto L_10;
    
  }
  else if( b == ZERO ) { 
    
    //        Swap rows and columns
    
    cs = ZERO;
    sn = ONE;
    temp = d;
    d = a;
    a = temp;
    b = -c;
    c = ZERO;
    goto L_10;
  }
  else if( a == d && sign( ONE, b ) != sign( ONE, c ) ) { 
    goto L_10;
  }
  else { 
    
    //        Make diagonal elements equal
    
    temp = a - d;
    p = HALF*temp;
    sigma = b + c;
    tau = slapy2( sigma, temp );
    cs1 = sqrt( HALF*(ONE + abs( sigma )/tau) );
    sn1 = -(p/(tau*cs1))*sign( ONE, sigma );
    
    //        Compute [ AA  BB ] = [ A  B ] [ CS1 -SN1 ]
    //                [ CC  DD ]   [ C  D ] [ SN1  CS1 ]
    
    aa = a*cs1 + b*sn1;
    bb = -a*sn1 + b*cs1;
    cc = c*cs1 + d*sn1;
    dd = -c*sn1 + d*cs1;
    
    //        Compute [ A  B ] = [ CS1  SN1 ] [ AA  BB ]
    //                [ C  D ]   [-SN1  CS1 ] [ CC  DD ]
    
    a = aa*cs1 + cc*sn1;
    b = bb*cs1 + dd*sn1;
    c = -aa*sn1 + cc*cs1;
    d = -bb*sn1 + dd*cs1;
    
    //        Accumulate transformation
    
    temp = cs*cs1 - sn*sn1;
    sn = cs*sn1 + sn*cs1;
    cs = temp;
    
    temp = HALF*(a + d);
    a = temp;
    d = temp;
    
    if( c != ZERO ) { 
      if( b != ZERO ) { 
        if( sign( ONE, b ) == sign( ONE, c ) ) { 
          
          //                 Real eigenvalues: reduce to upper triangular form
          
          sab = sqrt( abs( b ) );
          sac = sqrt( abs( c ) );
          p = sign( sab*sac, c );
          tau = ONE/sqrt( abs( b + c ) );
          a = temp + p;
          d = temp - p;
          b = b - c;
          c = ZERO;
          cs1 = sab*tau;
          sn1 = sac*tau;
          temp = cs*cs1 - sn*sn1;
          sn = cs*sn1 + sn*cs1;
          cs = temp;
        }
      }
      else { 
        b = -c;
        c = ZERO;
        temp = cs;
        cs = -sn;
        sn = temp;
      }
    }
  }
  
L_10:
  ;
  
  //     Store eigenvalues in (RT1R,RT1I) and (RT2R,RT2I).
  
  rt1r = a;
  rt2r = d;
  if( c == ZERO ) { 
    rt1i = ZERO;
    rt2i = ZERO;
  }
  else { 
    rt1i = sqrt( abs( b ) )*sqrt( abs( c ) );
    rt2i = -rt1i;
  }
  return;
  
  //     End of SLANV2
  
} // end of function 


/*
 * C++ implementation of Lapack routine sgebak
 *
 * $Id: sgebak.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:58:15
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: sgebak.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ sgebak(const char &job, const char &side, const long &n, const long &ilo, 
 const long &ihi, float scale[], const long &m, float *v, const long &ldv, 
 long &info)
{
#define V(I_,J_)  (*(v+(I_)*(ldv)+(J_)))
  int leftv, rightv;
  long _do0, _do1, _do2, _do3, i, i_, ii, ii_, k;
  float s;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SGEBAK forms the right or left eigenvectors of a real general matrix
  //  by backward transformation on the computed eigenvectors of the matrix
  //  preprocessed by SGEBAL.
  
  //  Arguments
  //  =========
  
  //  JOB     (input) CHARACTER*1
  //          Specifies the type of backward transformation required:
  //          = 'N', do nothing, return immediately.
  //          = 'P', do backward transformation for permutation.
  //          = 'S', do backward transformation for balancing.
  //          = 'B', do backward transformations for both permutation and
  //                 balancing.
  //          JOB must be the same as the JOB parameter in SGEBAL.
  
  //  SIDE    (input) CHARACTER*1
  //          Specifies whether the eigenvectors given in the array V
  //          are right eigenvectors or left eigenvectors.
  //          = 'R', right eigenvectors
  //          = 'L', left eigenvectors
  
  //  N       (input) INTEGER
  //          The number of rows of the matrix V.  N >= 0.
  
  //  ILO     (input) INTEGER
  //  IHI     (input) INTEGER
  //          ILO and IHI are integers determined by SGEBAL.
  
  //  SCALE   (input) REAL array, dimension (N)
  //          SCALE contains information determining the permutations
  //          and/or scaling factors used by SGEBAL.
  
  //  M       (input) INTEGER
  //          M is the number of columns of the matrix of eigenvectors V
  //          to be back transformed.
  
  //  V       (input/output) REAL array, dimension (LDV,M)
  //          On entry, V contains the real and imaginary parts of the
  //          eigenvectors to be backward transformed in its first M
  //          columns.
  //          On exit, V contains the real and imaginary parts of the
  //          transformed eigenvectors in its first M columns.
  
  //  LDV     (input) INTEGER
  //          The leading dimension of the matrix V. LDV >= max(1,N).
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit.
  //          < 0:  if INFO = -k, the k-th argument had an illegal value.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Decode and Test the input parameters
  
  rightv = lsame( side, 'R' );
  leftv = lsame( side, 'L' );
  
  info = 0;
  if( ((!lsame( job, 'N' ) && !lsame( job, 'P' )) && !lsame( job, 
   'S' )) && !lsame( job, 'B' ) ) { 
    info = -1;
  }
  else if( !rightv && !leftv ) { 
    info = -2;
  }
  else if( n < 0 ) { 
    info = -3;
  }
  else if( ilo < 1 ) { 
    info = -4;
  }
  else if( ihi < ilo || ihi > max( 1, n ) ) { 
    info = -5;
  }
  else if( m < 0 ) { 
    info = -7;
  }
  else if( ldv < max( 1, n ) ) { 
    info = -9;
  }
  if( info != 0 ) { 
    xerbla( "SGEBAK", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  if( m == 0 ) 
    return;
  if( lsame( job, 'N' ) ) 
    return;
  
  if( ilo == ihi ) 
    goto L_30;
  
  //     Backward balance
  
  if( lsame( job, 'S' ) || lsame( job, 'B' ) ) { 
    
    if( rightv ) { 
      for( i = ilo, i_ = i - 1, _do0 = ihi; i <= _do0; i++, i_++ ) { 
        s = scale[i_];
        sscal( m, s, &V(0,i_), ldv );
      }
    }
    
    if( leftv ) { 
      for( i = ilo, i_ = i - 1, _do1 = ihi; i <= _do1; i++, i_++ ) { 
        s = ONE/scale[i_];
        sscal( m, s, &V(0,i_), ldv );
      }
    }
    
  }
  
  //     Backward permutation
  
  //     For  I = ILO-1 step -1 until 1,
  //              IHI+1 step 1 until N do --
  
L_30:
  ;
  if( lsame( job, 'P' ) || lsame( job, 'B' ) ) { 
    if( rightv ) { 
      for( ii = 1, ii_ = ii - 1, _do2 = n; ii <= _do2; ii++, ii_++ ) { 
        i = ii;
        if( i >= ilo && i <= ihi ) 
          goto L_40;
        if( i < ilo ) 
          i = ilo - ii;
        k = scale[i - 1];
        if( k == i ) 
          goto L_40;
        sswap( m, &V(0,i - 1), ldv, &V(0,k - 1), ldv );
L_40:
        ;
      }
    }
    
    if( leftv ) { 
      for( ii = 1, ii_ = ii - 1, _do3 = n; ii <= _do3; ii++, ii_++ ) { 
        i = ii;
        if( i >= ilo && i <= ihi ) 
          goto L_50;
        if( i < ilo ) 
          i = ilo - ii;
        k = scale[i - 1];
        if( k == i ) 
          goto L_50;
        sswap( m, &V(0,i - 1), ldv, &V(0,k - 1), ldv );
L_50:
        ;
      }
    }
  }
  
  return;
  
  //     End of SGEBAK
  
#undef  V
} // end of function 


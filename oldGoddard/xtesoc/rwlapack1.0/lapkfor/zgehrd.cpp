/*
 * C++ implementation of Lapack routine zgehrd
 *
 * $Id: zgehrd.cpp,v 1.2 1993/07/07 23:00:32 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:46:21
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zgehrd.cpp,v $
 * Revision 1.2  1993/07/07  23:00:32  alv
 * NBMAX reduced for DOS to avoid running out of memory
 *
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
#if defined(__MSDOS__)
const long NBMAX = 8;
#else
const long NBMAX = 64;
#endif
const DComplex ZERO = DComplex(0.0e0);
const DComplex ONE = DComplex(1.0e0);
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zgehrd(const long &n, const long &ilo, const long &ihi, 
 DComplex *a, const long &lda, DComplex tau[], DComplex work[], const long &lwork, 
 long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  // PARAMETER translations
  const long LDT = NBMAX + 1;
  // end of PARAMETER translations

  long _do0, _do1, _do2, _do3, i, i_, ib, iinfo, iws, ldwork, 
   nb, nbmin, nh, nx;
  DComplex ei;
  DComplex (*const t)[LDT]=(DComplex(*)[LDT])new DComplex[NBMAX*LDT];
  if( t == NULL )
    memerr( "unable to allocate object in zgehrd()" );

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZGEHRD reduces a DComplex general matrix A to upper Hessenberg form H
  //  by a unitary similarity transformation:  Q' * A * Q = H .
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  ILO     (input) INTEGER
  //  IHI     (input) INTEGER
  //          It is assumed that A is already upper triangular in rows
  //          and columns 1:ILO-1 and IHI+1:N. ILO and IHI are normally
  //          set by a previous call to ZGEBAL; otherwise they should be
  //          set to 1 and N respectively. See Further Details. If N > 0,
  //          1 <= ILO <= IHI <= N; otherwise set ILO = 1, IHI = N.
  
  //  A       (input/output) COMPLEX*16 array, dimension (LDA,N)
  //          On entry, the n by n general matrix to be reduced.
  //          On exit, the upper triangle and the first subdiagonal of A
  //          are overwritten with the upper Hessenberg matrix H, and the
  //          elements below the first subdiagonal, with the array TAU,
  //          represent the unitary matrix Q as a product of elementary
  //          reflectors. See Further Details.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  TAU     (output) COMPLEX*16 array, dimension (N)
  //          The scalar factors of the elementary reflectors (see Further
  //          Details). Elements 1:ILO-1 and IHI:N of TAU are set to zero.
  
  //  WORK    (workspace) COMPLEX*16 array, dimension (LWORK)
  //          On exit, if INFO = 0, WORK(1) returns the minimum value of
  //          LWORK required to use the optimum blocksize.
  
  //  LWORK   (input) INTEGER
  //          The length of the array WORK.  LWORK >= max(1,N).
  //          For optimum performance LWORK should be at least N*NB,
  //          where NB is the optimal blocksize.
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0:  if INFO = -i, the i-th argument had an illegal value.
  
  //  Further Details
  //  ===============
  
  //  The matrix Q is represented as a product of (ihi-ilo) elementary
  //  reflectors
  
  //     Q = H(ilo) H(ilo+1) . . . H(ihi-1).
  
  //  Each H(i) has the form
  
  //     H(i) = I - tau * v * v'
  
  //  where tau is a DComplex scalar, and v is a DComplex vector with
  //  v(1:i) = 0, v(i+1) = 1 and v(ihi+1:n) = 0; v(i+2:ihi) is stored on
  //  exit in A(i+2:ihi,i), and tau in TAU(i).
  
  //  The contents of A are illustrated by the following example, with
  //  n = 7, ilo = 2 and ihi = 6:
  
  //  on entry                         on exit
  
  //  ( a   a   a   a   a   a   a )    (  a   a   h   h   h   h   a )
  //  (     a   a   a   a   a   a )    (      a   h   h   h   h   a )
  //  (     a   a   a   a   a   a )    (      h   h   h   h   h   h )
  //  (     a   a   a   a   a   a )    (      v2  h   h   h   h   h )
  //  (     a   a   a   a   a   a )    (      v2  v3  h   h   h   h )
  //  (     a   a   a   a   a   a )    (      v2  v3  v4  h   h   h )
  //  (                         a )    (                          a )
  
  //  where a denotes an element of the original matrix A, h denotes a
  //  modified element of the upper Hessenberg matrix H, and vi denotes an
  //  element of the vector defining H(i).
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Local Arrays ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters
  
  info = 0;
  if( n < 0 ) { 
    info = -1;
  }
  else if( ilo < 1 ) { 
    info = -2;
  }
  else if( ihi < min( ilo, n ) || ihi > n ) { 
    info = -3;
  }
  else if( lda < max( 1, n ) ) { 
    info = -5;
  }
  else if( lwork < max( 1, n ) ) { 
    info = -8;
  }
  if( info != 0 ) { 
    xerbla( "ZGEHRD", -info );
    delete [] t;
    return;
  }
  
  //     Set elements 1:ILO-1 and IHI:N of TAU to zero
  
  for( i = 1, i_ = i - 1, _do0 = ilo - 1; i <= _do0; i++, i_++ ) { 
    tau[i_] = ZERO;
  }
  for( i = max( 1, ihi ), i_ = i - 1, _do1 = n; i <= _do1; i++, i_++ ) { 
    tau[i_] = ZERO;
  }
  
  //     Quick return if possible
  
  nh = ihi - ilo + 1;
  if( nh <= 1 ) { 
    work[0] = DComplex((double)1);
    delete [] t;
    return;
  }
  
  //     Determine the block size.
  
  nb = ilaenv( 1, "ZGEHRD", " ", n, ilo, ihi, -1 );
  nbmin = 2;
  iws = 1;
  if( nb > 1 && nb < nh ) { 
    
    //        Determine when to cross over from blocked to unblocked code
    //        (last block is always handled by unblocked code).
    
    nx = max( nb, ilaenv( 3, "ZGEHRD", " ", n, ilo, ihi, -1 ) );
    if( nx < nh ) { 
      
      //           Determine if workspace is large enough for blocked code.
      
      iws = n*nb;
      if( lwork < iws ) { 
        
        //              Not enough workspace to use optimal NB:  determine the
        //              minimum value of NB, and reduce NB or force use of
        //              unblocked code.
        
        nbmin = max( 2, ilaenv( 2, "ZGEHRD", " ", n, ilo, 
         ihi, -1 ) );
        if( lwork >= n*nbmin ) { 
          nb = lwork/n;
        }
        else { 
          nb = 1;
        }
      }
    }
  }
  ldwork = n;
  
  if( nb < nbmin || nb >= nh ) { 
    
    //        Use unblocked code below
    
    i = ilo;
    
  }
  else { 
    
    //        Use blocked code
    
    for( i = ilo, i_ = i - 1, _do2=docnt(i,ihi - 1 - nx,_do3 = nb); _do2 > 0; i += _do3, i_ += _do3, _do2-- ) { 
      ib = min( nb, ihi - i );
      
      //           Reduce columns i:i+ib-1 to Hessenberg form, returning the
      //           matrices V and T of the block reflector H = I - V*T*V'
      //           which performs the reduction, and also the matrix Y = A*V*T
      
      zlahrd( ihi, i, ib, &A(i_,0), lda, &tau[i_], (DComplex*)t, 
       LDT, work, ldwork );
      
      //           Apply the block reflector H to A(1:ihi,i+ib:ihi) from the
      //           right, computing  A := A - Y * V'. V(i+ib,ib-1) must be set
      //           to 1.
      
      ei = A(i_ + ib - 1,i_ + ib);
      A(i_ + ib - 1,i_ + ib) = ONE;
      zgemm( 'N'/*No transpose*/, 'C'/*Conjugate transpose*/
       , ihi, ihi - i - ib + 1, ib, -(ONE), work, ldwork, &A(i_,i_ + ib), 
       lda, ONE, &A(i_ + ib,0), lda );
      A(i_ + ib - 1,i_ + ib) = ei;
      
      //           Apply the block reflector H to A(i+1:ihi,i+ib:n) from the
      //           left
      
      zlarfb( 'L'/*Left*/, 'C'/*Conjugate transpose*/, 'F'/*Forward*/
       , 'C'/*Columnwise*/, ihi - i, n - i - ib + 1, ib, &A(i_,i_ + 1), 
       lda, (DComplex*)t, LDT, &A(i_ + ib,i_ + 1), lda, work, 
       ldwork );
    }
  }
  
  //     Use unblocked code to reduce the rest of the matrix
  
  zgehd2( n, i, ihi, a, lda, tau, work, iinfo );
  work[0] = DComplex((double)iws);
  
  delete [] t;
  return;
  
  //     End of ZGEHRD
  
#undef  A
} // end of function 


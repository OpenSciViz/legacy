/*
 * C++ implementation of lapack routine dppsv
 *
 * $Id: dppsv.cpp,v 1.5 1993/04/06 20:42:07 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:37:33
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dppsv.cpp,v $
 * Revision 1.5  1993/04/06  20:42:07  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:16:47  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:08:34  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

RWLAPKDECL void /*FUNCTION*/ dppsv(const char &uplo, const long &n, const long &nrhs, double ap[], 
   double *b, const long &ldb, long &info)
{
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))

  
  //  -- LAPACK driver routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DPPSV computes the solution to a real system of linear equations
  //     A * X = B,
  //  where A is an N by N symmetric positive definite matrix stored in
  //  packed format and X and B are N by NRHS matrices.
  
  //  The Cholesky decomposition is used to factor A as
  //     A = U'* U ,  if UPLO = 'U', or
  //     A = L * L',  if UPLO = 'L',
  //  where U is an upper triangular matrix, L is a lower triangular
  //  matrix, and ' indicates transpose.  The factored form of A
  //  is then used to solve the system of equations A * X = B.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangular part of the
  //          symmetric matrix A is stored:
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The number of linear equations, i.e., the order of the
  //          matrix A.  N >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right hand sides, i.e., the number of columns
  //          of the matrix B.  NRHS >= 0.
  
  //  AP      (input/output) DOUBLE PRECISION array, dimension (N*(N+1)/2)
  //          On entry, the upper or lower triangle of the symmetric matrix
  //          A, packed columnwise in a linear array.  The j-th column of A
  //          is stored in the array AP as follows:
  //          if UPLO = 'U', AP(i + (j-1)*j/2) = A(i,j) for 1<=i<=j;
  //          if UPLO = 'L', AP(i + (j-1)*(2n-j)/2) = A(i,j) for j<=i<=n.
  //          See below for further details.
  
  //          On exit, if INFO = 0, the factor U or L from the Cholesky
  //          factorization A = U'*U or A = L*L', in the same storage
  //          format as A.
  
  //  B       (input/output) DOUBLE PRECISION array, dimension (LDB,NRHS)
  //          On entry, the N by NRHS matrix of right hand side vectors B
  //          for the system of equations A*X = B.
  //          On exit, if INFO = 0, the N by NRHS matrix of solution
  //          vectors X.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: if INFO = k, the leading minor of order k of A is not
  //               positive definite, so the factorization could not be
  //               completed, and the solution has not been computed.
  
  //  Further Details
  //  ===============
  
  //  The packed storage scheme is illustrated by the following example
  //  when N = 4, UPLO = 'U':
  
  //  Two-dimensional storage of the symmetric matrix A:
  
  //     a11 a12 a13 a14
  //         a22 a23 a24
  //             a33 a34     (aij = conjg(aji))
  //                 a44
  
  //  Packed storage of the upper triangle of A:
  
  //  AP = [ a11, a12, a22, a13, a23, a33, a14, a24, a34, a44 ]
  
  //  =====================================================================
  
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  if( !lsame( uplo, 'U' ) && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( nrhs < 0 ) { 
    info = -3;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -6;
  }
  if( info != 0 ) { 
    xerbla( "DPPSV ", -info );
    return;
  }
  
  //     Compute the Cholesky factorization A = U'*U or A = L*L'.
  
  dpptrf( uplo, n, ap, info );
  if( info == 0 ) { 
    
    //        Solve the system A*X = B, overwriting B with X.
    
    dpptrs( uplo, n, nrhs, ap, b, ldb, info );
    
  }
  return;
  
  //     End of DPPSV
  
#undef  B
} // end of function 


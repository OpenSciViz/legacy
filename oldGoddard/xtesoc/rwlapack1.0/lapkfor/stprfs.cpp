/*
 * C++ implementation of Lapack routine stprfs
 *
 * $Id: stprfs.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:03:22
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: stprfs.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ZERO = 0.0e0;
const float ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ stprfs(const char &uplo, const char &trans, const char &diag, const long &n, 
 const long &nrhs, float ap[], float *b, const long &ldb, float *x, const long &ldx, 
 float ferr[], float berr[], float work[], long iwork[], long &info)
{
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
#define X(I_,J_)  (*(x+(I_)*(ldx)+(J_)))
  int notran, nounit, upper;
  char transt;
  long _do0, _do1, _do10, _do11, _do12, _do13, _do14, _do15, 
   _do16, _do17, _do18, _do19, _do2, _do20, _do21, _do22, _do23, 
   _do3, _do4, _do5, _do6, _do7, _do8, _do9, i, i_, j, j_, k, k_, 
   kase, kc, nz;
  float eps, lstres, s, safe1, safe2, safmin, xk;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  STPRFS provides error bounds and backward error estimates for the
  //  solution to a system of linear equations with a triangular packed
  //  coefficient matrix.
  
  //  The solution vectors X must be computed by STPTRS or some other
  //  means before entering this routine.  STPRFS does not do iterative
  //  refinement because doing so can not improve the backward error.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the matrix A is upper or lower triangular.
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  TRANS   (input) CHARACTER*1
  //          Specifies the form of the system of equations.
  //          = 'N':  A * X = B  (No transpose)
  //          = 'T':  A'* X = B  (Transpose)
  //          = 'C':  A'* X = B  (Conjugate transpose = Transpose)
  
  //  DIAG    (input) CHARACTER*1
  //          Specifies whether or not the matrix A is unit triangular.
  //          = 'N':  Non-unit triangular
  //          = 'U':  Unit triangular
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right hand sides, i.e., the number of columns
  //          of the matrices B and X.  NRHS >= 0.
  
  //  AP      (input) REAL array, dimension (N*(N+1)/2)
  //          The upper or lower triangular matrix A, packed columnwise in
  //          a linear array.  The j-th column of A is stored in the array
  //          AP as follows:
  //          if UPLO = 'U', AP(i + (j-1)*j/2) = A(i,j) for 1<=i<=j;
  //          if UPLO = 'L', AP(i + (j-1)*(2n-j)/2) = A(i,j) for j<=i<=n.
  //          If DIAG = 'U', the diagonal elements of A are not referenced
  //          and are assumed to be 1.
  
  //  B       (input) REAL array, dimension (LDB,NRHS)
  //          The right hand side vectors for the system of linear
  //          equations.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  X       (input) REAL array, dimension (LDX,NRHS)
  //          The solution vectors for the system of linear equations.
  
  //  LDX     (input) INTEGER
  //          The leading dimension of the array X.  LDX >= max(1,N).
  
  //  FERR    (output) REAL array, dimension (NRHS)
  //          The estimated forward error bounds for each solution vector
  //          X.  If XTRUE is the true solution, FERR bounds the magnitude
  //          of the largest entry in (X - XTRUE) divided by the magnitude
  //          of the largest entry in X.  The quality of the error bound
  //          depends on the quality of the estimate of norm(inv(A))
  //          computed in the code; if the estimate of norm(inv(A)) is
  //          accurate, the error bound is guaranteed.
  
  //  BERR    (output) REAL array, dimension (NRHS)
  //          The componentwise relative backward error of each solution
  //          vector (i.e., the smallest relative change in any entry of A
  //          or B that makes X an exact solution).
  
  //  WORK    (workspace) REAL array, dimension (3*N)
  
  //  IWORK   (workspace) INTEGER array, dimension (N)
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  notran = lsame( trans, 'N' );
  nounit = lsame( diag, 'N' );
  
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( (!notran && !lsame( trans, 'T' )) && !lsame( trans, 'C' )
    ) { 
    info = -2;
  }
  else if( !nounit && !lsame( diag, 'U' ) ) { 
    info = -3;
  }
  else if( n < 0 ) { 
    info = -4;
  }
  else if( nrhs < 0 ) { 
    info = -5;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -8;
  }
  else if( ldx < max( 1, n ) ) { 
    info = -10;
  }
  if( info != 0 ) { 
    xerbla( "STPRFS", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 || nrhs == 0 ) { 
    for( j = 1, j_ = j - 1, _do0 = nrhs; j <= _do0; j++, j_++ ) { 
      ferr[j_] = ZERO;
      berr[j_] = ZERO;
    }
    return;
  }
  
  if( notran ) { 
    transt = 'T';
  }
  else { 
    transt = 'N';
  }
  
  //     NZ = maximum number of nonzero entries in each row of A, plus 1
  
  nz = n + 1;
  eps = slamch( 'E'/*Epsilon*/ );
  safmin = slamch( 'S'/*Safe minimum*/ );
  safe1 = nz*safmin;
  safe2 = safe1/eps;
  
  //     Do for each right hand side
  
  for( j = 1, j_ = j - 1, _do1 = nrhs; j <= _do1; j++, j_++ ) { 
    
    //        Compute residual R = B - op(A) * X,
    //        where op(A) = A or A', depending on TRANS.
    
    scopy( n, &X(j_,0), 1, &work[n], 1 );
    stpmv( uplo, trans, diag, n, ap, &work[n], 1 );
    saxpy( n, -ONE, &B(j_,0), 1, &work[n], 1 );
    
    //        Compute componentwise relative backward error from formula
    
    //        max(i) ( abs(R(i)) / ( abs(op(A))*abs(X) + abs(B) )(i) )
    
    //        where abs(Z) is the componentwise absolute value of the matrix
    //        or vector Z.  If the i-th component of the denominator is less
    //        than SAFE2, then SAFE1 is added to the i-th components of the
    //        numerator and denominator before dividing.
    
    for( i = 1, i_ = i - 1, _do2 = n; i <= _do2; i++, i_++ ) { 
      work[i_] = abs( B(j_,i_) );
    }
    
    if( notran ) { 
      
      //           Compute abs(A)*abs(X) + abs(B).
      
      if( upper ) { 
        kc = 1;
        if( nounit ) { 
          for( k = 1, k_ = k - 1, _do3 = n; k <= _do3; k++, k_++ ) { 
            xk = abs( X(j_,k_) );
            for( i = 1, i_ = i - 1, _do4 = k; i <= _do4; i++, i_++ ) { 
              work[i_] = work[i_] + abs( ap[kc + i_ - 1] )*
               xk;
            }
            kc = kc + k;
          }
        }
        else { 
          for( k = 1, k_ = k - 1, _do5 = n; k <= _do5; k++, k_++ ) { 
            xk = abs( X(j_,k_) );
            for( i = 1, i_ = i - 1, _do6 = k - 1; i <= _do6; i++, i_++ ) { 
              work[i_] = work[i_] + abs( ap[kc + i_ - 1] )*
               xk;
            }
            work[k_] = work[k_] + xk;
            kc = kc + k;
          }
        }
      }
      else { 
        kc = 1;
        if( nounit ) { 
          for( k = 1, k_ = k - 1, _do7 = n; k <= _do7; k++, k_++ ) { 
            xk = abs( X(j_,k_) );
            for( i = k, i_ = i - 1, _do8 = n; i <= _do8; i++, i_++ ) { 
              work[i_] = work[i_] + abs( ap[kc + i_ - k] )*
               xk;
            }
            kc = kc + n - k + 1;
          }
        }
        else { 
          for( k = 1, k_ = k - 1, _do9 = n; k <= _do9; k++, k_++ ) { 
            xk = abs( X(j_,k_) );
            for( i = k + 1, i_ = i - 1, _do10 = n; i <= _do10; i++, i_++ ) { 
              work[i_] = work[i_] + abs( ap[kc + i_ - k] )*
               xk;
            }
            work[k_] = work[k_] + xk;
            kc = kc + n - k + 1;
          }
        }
      }
    }
    else { 
      
      //           Compute abs(A')*abs(X) + abs(B).
      
      if( upper ) { 
        kc = 1;
        if( nounit ) { 
          for( k = 1, k_ = k - 1, _do11 = n; k <= _do11; k++, k_++ ) { 
            s = ZERO;
            for( i = 1, i_ = i - 1, _do12 = k; i <= _do12; i++, i_++ ) { 
              s = s + abs( ap[kc + i_ - 1] )*abs( X(j_,i_) );
            }
            work[k_] = work[k_] + s;
            kc = kc + k;
          }
        }
        else { 
          for( k = 1, k_ = k - 1, _do13 = n; k <= _do13; k++, k_++ ) { 
            s = abs( X(j_,k_) );
            for( i = 1, i_ = i - 1, _do14 = k - 1; i <= _do14; i++, i_++ ) { 
              s = s + abs( ap[kc + i_ - 1] )*abs( X(j_,i_) );
            }
            work[k_] = work[k_] + s;
            kc = kc + k;
          }
        }
      }
      else { 
        kc = 1;
        if( nounit ) { 
          for( k = 1, k_ = k - 1, _do15 = n; k <= _do15; k++, k_++ ) { 
            s = ZERO;
            for( i = k, i_ = i - 1, _do16 = n; i <= _do16; i++, i_++ ) { 
              s = s + abs( ap[kc + i_ - k] )*abs( X(j_,i_) );
            }
            work[k_] = work[k_] + s;
            kc = kc + n - k + 1;
          }
        }
        else { 
          for( k = 1, k_ = k - 1, _do17 = n; k <= _do17; k++, k_++ ) { 
            s = abs( X(j_,k_) );
            for( i = k + 1, i_ = i - 1, _do18 = n; i <= _do18; i++, i_++ ) { 
              s = s + abs( ap[kc + i_ - k] )*abs( X(j_,i_) );
            }
            work[k_] = work[k_] + s;
            kc = kc + n - k + 1;
          }
        }
      }
    }
    s = ZERO;
    for( i = 1, i_ = i - 1, _do19 = n; i <= _do19; i++, i_++ ) { 
      if( work[i_] > safe2 ) { 
        s = max( s, abs( work[n + i_] )/work[i_] );
      }
      else { 
        s = max( s, (abs( work[n + i_] ) + safe1)/(work[i_] + 
         safe1) );
      }
    }
    berr[j_] = s;
    
    //        Bound error from formula
    
    //        norm(X - XTRUE) / norm(X) .le. FERR =
    //        norm( abs(inv(op(A)))*
    //           ( abs(R) + NZ*EPS*( abs(op(A))*abs(X)+abs(B) ))) / norm(X)
    
    //        where
    //          norm(Z) is the magnitude of the largest component of Z
    //          inv(op(A)) is the inverse of op(A)
    //          abs(Z) is the componentwise absolute value of the matrix or
    //             vector Z
    //          NZ is the maximum number of nonzeros in any row of A, plus 1
    //          EPS is machine epsilon
    
    //        The i-th component of abs(R)+NZ*EPS*(abs(op(A))*abs(X)+abs(B))
    //        is incremented by SAFE1 if the i-th component of
    //        abs(op(A))*abs(X) + abs(B) is less than SAFE2.
    
    //        Use SLACON to estimate the infinity-norm of the matrix
    //           inv(op(A)) * diag(W),
    //        where W = abs(R) + NZ*EPS*( abs(op(A))*abs(X)+abs(B) )))
    
    for( i = 1, i_ = i - 1, _do20 = n; i <= _do20; i++, i_++ ) { 
      if( work[i_] > safe2 ) { 
        work[i_] = abs( work[n + i_] ) + nz*eps*work[i_];
      }
      else { 
        work[i_] = abs( work[n + i_] ) + nz*eps*work[i_] + 
         safe1;
      }
    }
    
    kase = 0;
L_210:
    ;
    slacon( n, &work[n*2], &work[n], iwork, ferr[j_], kase );
    if( kase != 0 ) { 
      if( kase == 1 ) { 
        
        //              Multiply by diag(W)*inv(op(A)').
        
        stpsv( uplo, transt, diag, n, ap, &work[n], 1 );
        for( i = 1, i_ = i - 1, _do21 = n; i <= _do21; i++, i_++ ) { 
          work[n + i_] = work[i_]*work[n + i_];
        }
      }
      else { 
        
        //              Multiply by inv(op(A))*diag(W).
        
        for( i = 1, i_ = i - 1, _do22 = n; i <= _do22; i++, i_++ ) { 
          work[n + i_] = work[i_]*work[n + i_];
        }
        stpsv( uplo, trans, diag, n, ap, &work[n], 1 );
      }
      goto L_210;
    }
    
    //        Normalize error.
    
    lstres = ZERO;
    for( i = 1, i_ = i - 1, _do23 = n; i <= _do23; i++, i_++ ) { 
      lstres = max( lstres, abs( X(j_,i_) ) );
    }
    if( lstres != ZERO ) 
      ferr[j_] = ferr[j_]/lstres;
    
  }
  
  return;
  
  //     End of STPRFS
  
#undef  X
#undef  B
} // end of function 


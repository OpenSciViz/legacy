/*
 * C++ implementation of Lapack routine zlarnv
 *
 * $Id: zlarnv.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:49:09
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zlarnv.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
const double TWO = 2.0e0;
const long LV = 128;
const double TWOPI = 6.28318530717958623199592e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zlarnv(const long &idist, long iseed[], const long &n, 
 DComplex x[])
{
  long _do0, _do1, _do2, _do3, _do4, _do5, i, i_, il, iv, iv_;
  double u[LV];

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZLARNV returns a vector of n random DComplex numbers from a uniform or
  //  normal distribution.
  
  //  Arguments
  //  =========
  
  //  IDIST   (input) INTEGER
  //          Specifies the distribution of the random numbers:
  //          = 1:  real and imaginary parts each uniform (0,1)
  //          = 2:  real and imaginary parts each uniform (-1,1)
  //          = 3:  real and imaginary parts each normal (0,1)
  //          = 4:  uniformly distributed on the disc abs(z) < 1
  //          = 5:  uniformly distributed on the circle abs(z) = 1
  
  //  ISEED   (input/output) INTEGER array, dimension (4)
  //          On entry, the seed of the random number generator; the array
  //          elements must be between 0 and 4095, and ISEED(4) must be
  //          odd.
  //          On exit, the seed is updated.
  
  //  N       (input) INTEGER
  //          The number of random numbers to be generated.
  
  //  X       (output) COMPLEX*16 array, dimension (N)
  //          The generated random numbers.
  
  //  Further Details
  //  ===============
  
  //  This routine calls the auxiliary routine DLARUV to generate random
  //  real numbers from a uniform (0,1) distribution, in batches of up to
  //  128 using vectorisable code. The Box-Muller method is used to
  //  transform numbers from a uniform to a normal distribution.
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Local Arrays ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Executable Statements ..
  
  for( iv = 1, iv_ = iv - 1, _do0 = n; iv <= _do0; iv += LV/2, iv_ += LV/
   2 ) { 
    il = min( LV/2, n - iv + 1 );
    
    //        Call DLARUV to generate 2*IL real numbers from a uniform (0,1)
    //        distribution (2*IL <= LV)
    
    dlaruv( iseed, 2*il, u );
    
    if( idist == 1 ) { 
      
      //           Copy generated numbers
      
      for( i = 1, i_ = i - 1, _do1 = il; i <= _do1; i++, i_++ ) { 
        x[iv + i_ - 1] = DComplex( u[i*2 - 2], u[i*2 - 1] );
      }
    }
    else if( idist == 2 ) { 
      
      //           Convert generated numbers to uniform (-1,1) distribution
      
      for( i = 1, i_ = i - 1, _do2 = il; i <= _do2; i++, i_++ ) { 
        x[iv + i_ - 1] = DComplex( TWO*u[i*2 - 2] - ONE, TWO*
         u[i*2 - 1] - ONE );
      }
    }
    else if( idist == 3 ) { 
      
      //           Convert generated numbers to normal (0,1) distribution
      
      for( i = 1, i_ = i - 1, _do3 = il; i <= _do3; i++, i_++ ) { 
        x[iv + i_ - 1] = sqrt( -TWO*log( u[i*2 - 2] ) )*exp( DComplex( ZERO, 
         TWOPI*u[i*2 - 1] ) );
      }
    }
    else if( idist == 4 ) { 
      
      //           Convert generated numbers to DComplex numbers uniformly
      //           distributed on the unit disk
      
      for( i = 1, i_ = i - 1, _do4 = il; i <= _do4; i++, i_++ ) { 
        x[iv + i_ - 1] = sqrt( u[i*2 - 2] )*exp( DComplex( ZERO, 
         TWOPI*u[i*2 - 1] ) );
      }
    }
    else if( idist == 5 ) { 
      
      //           Convert generated numbers to DComplex numbers uniformly
      //           distributed on the unit circle
      
      for( i = 1, i_ = i - 1, _do5 = il; i <= _do5; i++, i_++ ) { 
        x[iv + i_ - 1] = exp( DComplex( ZERO, TWOPI*u[i*2 - 1] ) );
      }
    }
  }
  return;
  
  //     End of ZLARNV
  
} // end of function 


/*
 * C++ implementation of lapack routine dgtcon
 *
 * $Id: dgtcon.cpp,v 1.6 1993/04/06 20:40:46 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:34:43
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dgtcon.cpp,v $
 * Revision 1.6  1993/04/06  20:40:46  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.5  1993/03/19  18:41:23  alv
 * now passes chars explicitly, rather than indirection of a string, to shut up SUN warnings
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:14:58  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:06:57  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ONE = 1.0e0;
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dgtcon(const char &norm, long &n, double dl[], double d[], 
 double du[], double du2[], long ipiv[], double &anorm, double &rcond, 
 double work[], long iwork[], long &info)
{
  int onenrm;
  long _do0, i, i_, kase, kase1;
  double ainvnm;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DGTCON estimates the reciprocal of the condition number of a real
  //  tridiagonal matrix A using the LU factorization as computed by
  //  DGTTRF.
  
  //  An estimate is obtained for norm(inv(A)), and the reciprocal of the
  //  condition number is computed as RCOND = 1 / (ANORM * norm(inv(A))).
  
  //  Arguments
  //  =========
  
  //  NORM    (input) CHARACTER*1
  //          Specifies whether the 1-norm condition number or the
  //          infinity-norm condition number is required:
  //          = '1' or 'O':  1-norm
  //          = 'I':         Infinity-norm
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  DL      (input) DOUBLE PRECISION array, dimension (N-1)
  //          The (n-1) multipliers that define the matrix L from the
  //          LU factorization of A as computed by DGTTRF.
  
  //  D       (input) DOUBLE PRECISION array, dimension (N)
  //          The n diagonal elements of the upper triangular matrix U from
  //          the LU factorization of A.
  
  //  DU      (input) DOUBLE PRECISION array, dimension (N-1)
  //          The (n-1) elements of the first super-diagonal of U.
  
  //  DU2     (input) DOUBLE PRECISION array, dimension (N-2)
  //          The (n-2) elements of the second super-diagonal of U.
  
  //  IPIV    (input) INTEGER array, dimension (N)
  //          The pivot indices; for 1 <= i <= n, row i of the matrix was
  //          interchanged with row IPIV(i).  IPIV(i) will always be either
  //          i or i+1; IPIV(i) = i indicates a row interchange was not
  //          required.
  
  //  ANORM   (input) DOUBLE PRECISION
  //          The 1-norm of the original matrix A.
  
  //  RCOND   (output) DOUBLE PRECISION
  //          The reciprocal of the condition number of the matrix A,
  //          computed as RCOND = 1/(ANORM * AINVNM), where AINVNM is an
  //          estimate of the 1-norm of inv(A) computed in this routine.
  
  //  WORK    (workspace) DOUBLE PRECISION array, dimension (N)
  
  //  IWORK   (workspace) INTEGER array, dimension (N)
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input arguments.
  
  info = 0;
  onenrm = norm == '1' || lsame( norm, 'O' );
  if( !onenrm && !lsame( norm, 'I' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( anorm < ZERO ) { 
    info = -8;
  }
  if( info != 0 ) { 
    xerbla( "DGTCON", -info );
    return;
  }
  
  //     Quick return if possible
  
  rcond = ZERO;
  if( n == 0 ) { 
    rcond = ONE;
    return;
  }
  else if( anorm == ZERO ) { 
    return;
  }
  
  //     Check that D(1:N) is non-zero.
  
  for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
    if( d[i_] == ZERO ) 
      return;
  }
  
  ainvnm = ZERO;
  if( onenrm ) { 
    kase1 = 1;
  }
  else { 
    kase1 = 2;
  }
  kase = 0;
L_20:
  ;
  dlacon( n, &work[n], work, iwork, ainvnm, kase );
  if( kase != 0 ) { 
    if( kase == kase1 ) { 
      
      //           Multiply by inv(U)*inv(L).
      
      dgttrs( 'N'/* No transpose */, n, 1, dl, d, du, du2, ipiv, 
       work, n, info );
    }
    else { 
      
      //           Multiply by inv(L')*inv(U').
      
      dgttrs( 'T'/* Transpose */, n, 1, dl, d, du, du2, ipiv, 
       work, n, info );
    }
    goto L_20;
  }
  
  //     Compute the estimate of the reciprocal condition number.
  
  if( ainvnm != ZERO ) 
    rcond = (ONE/ainvnm)/anorm;
  
  return;
  
  //     End of DGTCON
  
} // end of function 


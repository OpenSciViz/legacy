/*
 * C++ implementation of Lapack routine sopmtr
 *
 * $Id: sopmtr.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:01:04
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: sopmtr.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ sopmtr(const char &side, const char &uplo, const char &trans, const long &m, 
 const long &n, float ap[], float tau[], float *c, const long &ldc, float work[], 
 long &info)
{
#define C(I_,J_)  (*(c+(I_)*(ldc)+(J_)))
  int forwrd, left, notran, upper;
  long _do0, _do1, _do2, _do3, i, i1, i2, i3, i_, ic, ii, jc, 
   mi, ni, nq;
  float aii;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SOPMTR overwrites the general real m by n matrix C with
  
  //        Q * C  if SIDE = 'L' and TRANS = 'N', or
  
  //        Q'* C  if SIDE = 'L' and TRANS = 'T', or
  
  //        C * Q  if SIDE = 'R' and TRANS = 'N', or
  
  //        C * Q' if SIDE = 'R' and TRANS = 'T',
  
  //  where Q is a real orthogonal matrix of order nq, with nq = m if
  //  SIDE = 'L' and nq = n if SIDE = 'R'. Q is defined as the product of
  //  nq-1 elementary reflectors, as returned by SSPTRD using packed
  //  storage:
  
  //  if UPLO = 'U', Q = H(nq-1) . . . H(2) H(1);
  
  //  if UPLO = 'L', Q = H(1) H(2) . . . H(nq-1).
  
  //  Arguments
  //  =========
  
  //  SIDE    (input) CHARACTER*1
  //          = 'L': apply Q or Q' from the Left
  //          = 'R': apply Q or Q' from the Right
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies the storage scheme used in the previous call of
  //          SSPTRD:
  //          = 'U': Upper triangular packed storage;
  //          = 'L': Lower triangular packed storage.
  
  //  TRANS   (input) CHARACTER*1
  //          = 'N': apply Q  (No transpose)
  //          = 'T': apply Q' (Transpose)
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix C. M >= 0.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix C. N >= 0.
  
  //  AP      (input) REAL array, dimension
  //                               (M*(M+1)/2) if SIDE = 'L'
  //                               (N*(N+1)/2) if SIDE = 'R'
  //          The vectors which define the elementary reflectors, as
  //          returned by SSPTRD.
  
  //  TAU     (input) REAL array, dimension (M-1) if SIDE = 'L'
  //                                     or (N-1) if SIDE = 'R'
  //          TAU(i) must contain the scalar factor of the elementary
  //          reflector H(i), as returned by SSPTRD.
  
  //  C       (input/output) REAL array, dimension (LDC,N)
  //          On entry, the m by n matrix C.
  //          On exit, C is overwritten by Q*C or Q'*C or C*Q' or C*Q.
  
  //  LDC     (input) INTEGER
  //          The leading dimension of the array C. LDC >= max(1,M).
  
  //  WORK    (workspace) REAL array, dimension
  //                                   (N) if SIDE = 'L'
  //                                   (M) if SIDE = 'R'
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input arguments
  
  info = 0;
  left = lsame( side, 'L' );
  notran = lsame( trans, 'N' );
  upper = lsame( uplo, 'U' );
  
  //     NQ is the order of Q
  
  if( left ) { 
    nq = m;
  }
  else { 
    nq = n;
  }
  if( !left && !lsame( side, 'R' ) ) { 
    info = -1;
  }
  else if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -2;
  }
  else if( !notran && !lsame( trans, 'T' ) ) { 
    info = -3;
  }
  else if( m < 0 ) { 
    info = -4;
  }
  else if( n < 0 ) { 
    info = -5;
  }
  else if( ldc < max( 1, m ) ) { 
    info = -9;
  }
  if( info != 0 ) { 
    xerbla( "SOPMTR", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( m == 0 || n == 0 ) 
    return;
  
  if( upper ) { 
    
    //        Q was determined by a call to SSPTRD with UPLO = 'U'
    
    forwrd = (left && notran) || (!left && !notran);
    
    if( forwrd ) { 
      i1 = 1;
      i2 = nq - 1;
      i3 = 1;
      ii = 2;
    }
    else { 
      i1 = nq - 1;
      i2 = 1;
      i3 = -1;
      ii = nq*(nq + 1)/2 - 1;
    }
    
    if( left ) { 
      ni = n;
    }
    else { 
      mi = m;
    }
    
    for( i = i1, i_ = i - 1, _do0=docnt(i,i2,_do1 = i3); _do0 > 0; i += _do1, i_ += _do1, _do0-- ) { 
      if( left ) { 
        
        //              H(i) is applied to C(1:i,1:n)
        
        mi = i;
      }
      else { 
        
        //              H(i) is applied to C(1:m,1:i)
        
        ni = i;
      }
      
      //           Apply H(i)
      
      aii = ap[ii - 1];
      ap[ii - 1] = ONE;
      slarf( side, mi, ni, &ap[ii - i], 1, tau[i_], c, ldc, 
       work );
      ap[ii - 1] = aii;
      
      if( forwrd ) { 
        ii = ii + i + 2;
      }
      else { 
        ii = ii - i - 1;
      }
    }
  }
  else { 
    
    //        Q was determined by a call to SSPTRD with UPLO = 'L'.
    
    forwrd = (left && !notran) || (!left && notran);
    
    if( forwrd ) { 
      i1 = 1;
      i2 = nq - 1;
      i3 = 1;
      ii = 2;
    }
    else { 
      i1 = nq - 1;
      i2 = 1;
      i3 = -1;
      ii = nq*(nq + 1)/2 - 1;
    }
    
    if( left ) { 
      ni = n;
      jc = 1;
    }
    else { 
      mi = m;
      ic = 1;
    }
    
    for( i = i1, i_ = i - 1, _do2=docnt(i,i2,_do3 = i3); _do2 > 0; i += _do3, i_ += _do3, _do2-- ) { 
      aii = ap[ii - 1];
      ap[ii - 1] = ONE;
      if( left ) { 
        
        //              H(i) is applied to C(i+1:m,1:n)
        
        mi = m - i;
        ic = i + 1;
      }
      else { 
        
        //              H(i) is applied to C(1:m,i+1:n)
        
        ni = n - i;
        jc = i + 1;
      }
      
      //           Apply H(i)
      
      slarf( side, mi, ni, &ap[ii - 1], 1, tau[i_], &C(jc - 1,ic - 1), 
       ldc, work );
      ap[ii - 1] = aii;
      
      if( forwrd ) { 
        ii = ii + nq - i + 1;
      }
      else { 
        ii = ii - nq + i - 2;
      }
    }
  }
  return;
  
  //     End of SOPMTR
  
#undef  C
} // end of function 


/*
 * C++ implementation of Lapack routine zdrscl
 *
 * $Id: zdrscl.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:45:48
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zdrscl.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zdrscl(const long &n, const double &sa, DComplex sx[], const long &incx)
{
  int done;
  double bignum, cden, cden1, cnum, cnum1, mul, smlnum;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZDRSCL multiplies an n-element DComplex vector x by the real scalar
  //  1/a.  This is done without overflow or underflow as long as
  //  the final result x/a does not overflow or underflow.
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The number of components of the vector x.
  
  //  SA      (input) DOUBLE PRECISION
  //          The scalar a which is used to divide each component of x.
  //          SA must be >= 0, or the subroutine will divide by zero.
  
  //  SX      (input/output) COMPLEX*16 array, dimension
  //                         (1+(N-1)*abs(INCX))
  //          The n-element vector x.
  
  //  INCX    (input) INTEGER
  //          The increment between successive values of the vector SX.
  //          > 0:  SX(1) = X(1) and SX(1+(i-1)*INCX) = x(i),     1< i<= n
  //          < 0:  SX(1) = X(n) and SX(1+(i-1)*INCX) = x(n-i+1), 1< i<= n
  
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Quick return if possible
  
  if( n <= 0 ) 
    return;
  
  //     Get machine parameters
  
  smlnum = dlamch( 'S' );
  bignum = ONE/smlnum;
  dlabad( smlnum, bignum );
  
  //     Initialize the denominator to SA and the numerator to 1.
  
  cden = sa;
  cnum = ONE;
  
L_10:
  ;
  cden1 = cden*smlnum;
  cnum1 = cnum/bignum;
  if( abs( cden1 ) > abs( cnum ) && cnum != ZERO ) { 
    
    //        Pre-multiply X by SMLNUM if CDEN is large compared to CNUM.
    
    mul = smlnum;
    done = FALSE;
    cden = cden1;
  }
  else if( abs( cnum1 ) > abs( cden ) ) { 
    
    //        Pre-multiply X by BIGNUM if CDEN is small compared to CNUM.
    
    mul = bignum;
    done = FALSE;
    cnum = cnum1;
  }
  else { 
    
    //        Multiply X by CNUM / CDEN and return.
    
    mul = cnum/cden;
    done = TRUE;
  }
  
  //     Scale the vector X by MUL
  
  zdscal( n, mul, sx, incx );
  
  if( !done ) 
    goto L_10;
  
  return;
  
  //     End of ZDRSCL
  
} // end of function 


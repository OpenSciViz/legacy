/*
 * C++ implementation of Lapack routine sgeql2
 *
 * $Id: sgeql2.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:58:42
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: sgeql2.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ sgeql2(const long &m, const long &n, float *a, const long &lda, 
 float tau[], float work[], long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  long i, i_, k;
  float aii;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SGEQL2 computes a QL factorization of a real m by n matrix A:
  //  A = Q * L.
  
  //  Arguments
  //  =========
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix A.  M >= 0.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix A.  N >= 0.
  
  //  A       (input/output) REAL array, dimension (LDA,N)
  //          On entry, the m by n matrix A.
  //          On exit, if m >= n, the lower triangle of the subarray
  //          A(m-n+1:m,1:n) contains the n by n lower triangular matrix L;
  //          if m <= n, the elements on and below the (n-m)-th
  //          superdiagonal contain the m by n lower trapezoidal matrix L;
  //          the remaining elements, with the array TAU, represent the
  //          orthogonal matrix Q as a product of elementary reflectors
  //          (see Further Details).
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,M).
  
  //  TAU     (output) REAL array, dimension (min(M,N))
  //          The scalar factors of the elementary reflectors (see Further
  //          Details).
  
  //  WORK    (workspace) REAL array, dimension (N)
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument had an illegal value
  
  //  Further Details
  //  ===============
  
  //  The matrix Q is represented as a product of elementary reflectors
  
  //     Q = H(k) . . . H(2) H(1), where k = min(m,n).
  
  //  Each H(i) has the form
  
  //     H(i) = I - tau * v * v'
  
  //  where tau is a real scalar, and v is a real vector with
  //  v(m-k+i+1:m) = 0 and v(m-k+i) = 1; v(1:m-k+i-1) is stored on exit in
  //  A(1:m-k+i-1,n-k+i), and tau in TAU(i).
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input arguments
  
  info = 0;
  if( m < 0 ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( lda < max( 1, m ) ) { 
    info = -4;
  }
  if( info != 0 ) { 
    xerbla( "SGEQL2", -info );
    return;
  }
  
  k = min( m, n );
  
  for( i = k, i_ = i - 1; i >= 1; i--, i_-- ) { 
    
    //        Generate elementary reflector H(i) to annihilate
    //        A(1:m-k+i-1,n-k+i)
    
    slarfg( m - k + i, A(n - k + i_,m - k + i_), &A(n - k + i_,0), 
     1, tau[i_] );
    
    //        Apply H(i) to A(1:m-k+i,1:n-k+i-1) from the left
    
    aii = A(n - k + i_,m - k + i_);
    A(n - k + i_,m - k + i_) = ONE;
    slarf( 'L'/*Left*/, m - k + i, n - k + i - 1, &A(n - k + i_,0), 
     1, tau[i_], a, lda, work );
    A(n - k + i_,m - k + i_) = aii;
  }
  return;
  
  //     End of SGEQL2
  
#undef  A
} // end of function 


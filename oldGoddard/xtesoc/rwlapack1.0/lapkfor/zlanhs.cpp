/*
 * C++ implementation of Lapack routine zlanhs
 *
 * $Id: zlanhs.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:48:37
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zlanhs.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ONE = 1.0e0;
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL double /*FUNCTION*/ zlanhs(const char &norm, const long &n, DComplex *a, const long &lda, 
 double work[])
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  long _do0, _do1, _do2, _do3, _do4, _do5, _do6, _do7, _do8, 
   i, i_, j, j_;
  double scale, sum, value, zlanhs_v;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZLANHS  returns the value of the one norm,  or the Frobenius norm, or
  //  the  infinity norm,  or the  element of  largest absolute value  of a
  //  Hessenberg matrix A.
  
  //  Description
  //  ===========
  
  //  ZLANHS returns the value
  
  //     ZLANHS = ( max(abs(A(i,j))), NORM = 'M' or 'm'
  //              (
  //              ( norm1(A),         NORM = '1', 'O' or 'o'
  //              (
  //              ( normI(A),         NORM = 'I' or 'i'
  //              (
  //              ( normF(A),         NORM = 'F', 'f', 'E' or 'e'
  
  //  where  norm1  denotes the  one norm of a matrix (maximum column sum),
  //  normI  denotes the  infinity norm  of a matrix  (maximum row sum) and
  //  normF  denotes the  Frobenius norm of a matrix (square root of sum of
  //  squares).  Note that  max(abs(A(i,j)))  is not a  matrix norm.
  
  //  Arguments
  //  =========
  
  //  NORM    (input) CHARACTER*1
  //          Specifies the value to be returned in ZLANHS as described
  //          above.
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.  When N = 0, ZLANHS is
  //          set to zero.
  
  //  A       (input) COMPLEX*16 array, dimension (LDA,N)
  //          The n by n upper Hessenberg matrix A; the part of A below the
  //          first sub-diagonal is not referenced.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(N,1).
  
  //  WORK    (workspace) DOUBLE PRECISION array, dimension (LWORK),
  //          where LWORK >= N when NORM = 'I'; otherwise, WORK is not
  //          referenced.
  
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  if( n == 0 ) { 
    value = ZERO;
  }
  else if( lsame( norm, 'M' ) ) { 
    
    //        Find max(abs(A(i,j))).
    
    value = ZERO;
    for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
      for( i = 1, i_ = i - 1, _do1 = min( n, j + 1 ); i <= _do1; i++, i_++ ) { 
        value = max( value, abs( A(j_,i_) ) );
      }
    }
  }
  else if( (lsame( norm, 'O' )) || (norm == '1') ) { 
    
    //        Find norm1(A).
    
    value = ZERO;
    for( j = 1, j_ = j - 1, _do2 = n; j <= _do2; j++, j_++ ) { 
      sum = ZERO;
      for( i = 1, i_ = i - 1, _do3 = min( n, j + 1 ); i <= _do3; i++, i_++ ) { 
        sum = sum + abs( A(j_,i_) );
      }
      value = max( value, sum );
    }
  }
  else if( lsame( norm, 'I' ) ) { 
    
    //        Find normI(A).
    
    for( i = 1, i_ = i - 1, _do4 = n; i <= _do4; i++, i_++ ) { 
      work[i_] = ZERO;
    }
    for( j = 1, j_ = j - 1, _do5 = n; j <= _do5; j++, j_++ ) { 
      for( i = 1, i_ = i - 1, _do6 = min( n, j + 1 ); i <= _do6; i++, i_++ ) { 
        work[i_] = work[i_] + abs( A(j_,i_) );
      }
    }
    value = ZERO;
    for( i = 1, i_ = i - 1, _do7 = n; i <= _do7; i++, i_++ ) { 
      value = max( value, work[i_] );
    }
  }
  else if( (lsame( norm, 'F' )) || (lsame( norm, 'E' )) ) { 
    
    //        Find normF(A).
    
    scale = ZERO;
    sum = ONE;
    for( j = 1, j_ = j - 1, _do8 = n; j <= _do8; j++, j_++ ) { 
      zlassq( min( n, j + 1 ), &A(j_,0), 1, scale, sum );
    }
    value = scale*sqrt( sum );
  }
  
  zlanhs_v = value;
  return( zlanhs_v );
  
  //     End of ZLANHS
  
#undef  A
} // end of function 


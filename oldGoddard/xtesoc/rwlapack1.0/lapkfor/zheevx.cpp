/*
 * C++ implementation of Lapack routine zheevx
 *
 * $Id: zheevx.cpp,v 1.2 1993/07/21 22:22:14 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:47:19
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zheevx.cpp,v $
 * Revision 1.2  1993/07/21  22:22:14  alv
 * ported to Microsoft visual C++
 *
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zheevx(const char &jobz, const char &range, const char &uplo, const long &n, 
 DComplex *a, const long &lda, const double &vl, const double &vu, const long &il, 
 const long &iu, const double &abstol, long &m, double w[], DComplex *z, 
 const long &ldz, DComplex work[], const long &lwork, double rwork[], long iwork[], 
 long ifail[], long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define Z(I_,J_)  (*(z+(I_)*(ldz)+(J_)))
  int alleig, indeig, lower, valeig, wantz;
  char order;
  long _do0, _do1, _do2, _do3, _do4, i, i_, iinfo, imax, indd, 
   inde, indibl, indisp, indiwk, indrwk, indtau, indwrk, iscale, 
   itmp1, j, j_, jj, jj_, llwork, lopt, nsplit;
  double abstll, anrm, bignum, eps, rmax, rmin, safmin, sigma, smlnum, 
   tmp1, vll, vuu;

  
  //  -- LAPACK driver routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZHEEVX computes selected eigenvalues and, optionally, eigenvectors
  //  of a DComplex Hermitian matrix A by calling the recommended sequence
  //  of LAPACK routines.  Eigenvalues/vectors can be selected by
  //  specifying either a range of values or a range of indices for the
  //  desired eigenvalues.
  
  //  Arguments
  //  =========
  
  //  JOBZ    (input) CHARACTER*1
  //          Specifies whether or not to compute the eigenvectors:
  //          = 'N':  Compute eigenvalues only.
  //          = 'V':  Compute eigenvalues and eigenvectors.
  
  //  RANGE   (input) CHARACTER*1
  //          = 'A': all eigenvalues will be found.
  //          = 'V': all eigenvalues in the half-open interval (VL,VU]
  //                 will be found.
  //          = 'I': the IL-th through IU-th eigenvalues will be found.
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangular part of the
  //          symmetric matrix A is stored:
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The number of rows and columns of the matrix A.  N >= 0.
  
  //  A       (input/workspace) COMPLEX*16 array, dimension (LDA, N)
  //          On entry, the Hermitian matrix A.  If UPLO = 'U', only the
  //          upper triangular part of A is used to define the elements of
  //          the Hermitian matrix.  If UPLO = 'L', only the lower
  //          triangular part of A is used to define the elements of the
  //          Hermitian matrix.
  
  //          On exit, the lower triangle (if UPLO='L') or the upper
  //          triangle (if UPLO='U') of A, including the diagonal, is
  //          destroyed.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  VL      (input) DOUBLE PRECISION
  //          If RANGE='V', the lower bound of the interval to be searched
  //          for eigenvalues.  Not referenced if RANGE = 'A' or 'I'.
  
  //  VU      (input) DOUBLE PRECISION
  //          If RANGE='V', the upper bound of the interval to be searched
  //          for eigenvalues.  Not referenced if RANGE = 'A' or 'I'.
  
  //  IL      (input) INTEGER
  //          If RANGE='I', the index (from smallest to largest) of the
  //          smallest eigenvalue to be returned.  IL >= 1.
  //          Not referenced if RANGE = 'A' or 'V'.
  
  //  IU      (input) INTEGER
  //          If RANGE='I', the index (from smallest to largest) of the
  //          largest eigenvalue to be returned.  min(IL,N) <= IU <= N.
  //          Not referenced if RANGE = 'A' or 'V'.
  
  //  ABSTOL  (input) DOUBLE PRECISION
  //          The absolute error tolerance for the eigenvalues.
  //          An approximate eigenvalue is accepted as converged
  //          when it is determined to lie in an interval [a,b]
  //          of width less than or equal to
  
  //                  ABSTOL + EPS *   max( |a|,|b| ) ,
  
  //          where EPS is the machine precision.  If ABSTOL is less than
  //          or equal to zero, then  EPS*|T|  will be used in its place,
  //          where |T| is the 1-norm of the tridiagonal matrix obtained
  //          by reducing A to tridiagonal form.  For most problems,
  //          this is the appropriate level of accuracy to request.
  //          For certain strongly graded matrices, greater accuracy
  //          can be obtained in very small eigenvalues by setting
  //          ABSTOL to some very small positive number.
  //          (Note, however, that if ABSTOL is less than sqrt(unfl),
  //          where unfl is the underflow threshold, then sqrt(unfl)
  //          will be used in its place.)
  
  //          See "Computing Small Singular Values of Bidiagonal Matrices
  //          with Guaranteed High Relative Accuracy," by Demmel and
  //          Kahan, LAPACK Working Note #3.
  
  //  M       (output) INTEGER
  //          Total number of eigenvalues found.  0 <= M <= N.
  
  //  W       (output) DOUBLE PRECISION array, dimension (N)
  //          On normal exit, the first M entries contain the selected
  //          eigenvalues in ascending order.
  
  //  Z       (output) COMPLEX*16 array, dimension (LDZ, N)
  //          If JOBZ = 'V', then on normal exit the first M columns of Z
  //          contain the orthonormal eigenvectors of the matrix
  //          corresponding to the selected eigenvalues.  If an eigenvector
  //          fails to converge, then that column of Z contains the latest
  //          approximation to the eigenvector, and the index of the
  //          eigenvector is returned in IFAIL.
  //          If JOBZ = 'N', then Z is not referenced.
  
  //  LDZ     (input) INTEGER
  //          The leading dimension of the array Z.  LDZ >= 1, and if
  //          JOBZ = 'V', LDZ >= max(1,N).
  
  //  WORK    (workspace) COMPLEX*16 array, dimension (LWORK)
  //          On exit, WORK(1) is set to the dimension of the DComplex work
  //          array needed to obtain optimal performance from this routine.
  //          See the description of LWORK below.
  
  //  LWORK   (input) INTEGER
  //          The length of the array WORK.  LWORK >= max(1,2*N-1).
  //          For optimal efficiency, LWORK should be at least (NB+1)*N,
  //          where NB is the blocksize for ZHETRD returned by ILAENV.
  
  //  RWORK   (workspace) DOUBLE PRECISION array, dimension (6*N)
  
  //  IWORK   (workspace) INTEGER array, dimension (5*N)
  
  //  IFAIL   (output) INTEGER array, dimension (N)
  //          If JOBZ = 'V', then on normal exit, the first M elements of
  //          IFAIL are zero.  If INFO > 0 on exit, then IFAIL contains the
  //          indices of the eigenvectors that failed to converge.
  //          If JOBZ = 'N', then IFAIL is not referenced.
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit.
  //          < 0:  if INFO = -i, the i-th argument had an illegal value.
  //          > 0:  if INFO = +i, then i eigenvectors failed to converge.
  //                Their indices are stored in array IFAIL.
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  lower = lsame( uplo, 'L' );
  wantz = lsame( jobz, 'V' );
  alleig = lsame( range, 'A' );
  valeig = lsame( range, 'V' );
  indeig = lsame( range, 'I' );
  
  info = 0;
  if( !(wantz || lsame( jobz, 'N' )) ) { 
    info = -1;
  }
  else if( !((alleig || valeig) || indeig) ) { 
    info = -2;
  }
  else if( !(lower || lsame( uplo, 'U' )) ) { 
    info = -3;
  }
  else if( n < 0 ) { 
    info = -4;
  }
  else if( lda < max( 1, n ) ) { 
    info = -6;
  }
  else if( (valeig && n > 0) && vu <= vl ) { 
    info = -8;
  }
  else if( indeig && il < 1 ) { 
    info = -9;
  }
  else if( indeig && (iu < min( n, il ) || iu > n) ) { 
    info = -10;
  }
  else if( ldz < 1 || (wantz && ldz < n) ) { 
    info = -15;
  }
  else if( lwork < max( 1, 2*n - 1 ) ) { 
    info = -17;
  }
  
  if( info != 0 ) { 
    xerbla( "ZHEEVX", -info );
    return;
  }
  
  //     Quick return if possible
  
  m = 0;
  if( n == 0 ) { 
    work[0] = DComplex((double)1);
    return;
  }
  
  if( n == 1 ) { 
    work[0] = DComplex((double)1);
    if( alleig || indeig ) { 
      m = 1;
      w[0] = real(A(0,0));
    }
    else if( valeig ) { 
      if( vl < real( A(0,0) ) && vu >= real( A(0,0) ) ) { 
        m = 1;
        w[0] = real(A(0,0));
      }
    }
    if( wantz ) 
      Z(0,0) = (DComplex)(ONE);
    return;
  }
  
  //     Get machine constants.
  
  safmin = dlamch( 'S'/*Safe minimum*/ );
  eps = dlamch( 'P'/*Precision*/ );
  smlnum = safmin/eps;
  bignum = ONE/smlnum;
  rmin = sqrt( smlnum );
  rmax = min( sqrt( bignum ), ONE/sqrt( sqrt( safmin ) ) );
  
  //     Scale matrix to allowable range, if necessary.
  
  iscale = 0;
  abstll = abstol;
  if( valeig ) { 
    vll = vl;
    vuu = vu;
  }
  anrm = zlanhe( 'M', uplo, n, a, lda, rwork );
  if( anrm > ZERO && anrm < rmin ) { 
    iscale = 1;
    sigma = rmin/anrm;
  }
  else if( anrm > rmax ) { 
    iscale = 1;
    sigma = rmax/anrm;
  }
  if( iscale == 1 ) { 
    if( lower ) { 
      for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
        zdscal( n - j + 1, sigma, &A(j_,j_), 1 );
      }
    }
    else { 
      for( j = 1, j_ = j - 1, _do1 = n; j <= _do1; j++, j_++ ) { 
        zdscal( j, sigma, &A(j_,0), 1 );
      }
    }
    if( abstol > 0 ) 
      abstll = abstol*sigma;
    if( valeig ) { 
      vll = vl*sigma;
      vuu = vu*sigma;
    }
  }
  
  //     Call ZHETRD to reduce Hermitian matrix to tridiagonal form.
  
  indd = 1;
  inde = n + 1;
  indtau = 1;
  indwrk = indtau + n;
  llwork = lwork - indwrk + 1;
  zhetrd( uplo, n, a, lda, &rwork[indd - 1], &rwork[inde - 1], &work[indtau - 1], 
   &work[indwrk - 1], llwork, iinfo );
  lopt = real(n + work[indwrk - 1]);
  
  //     If all eigenvalues are desired and ABSTOL is less than or equal to
  //     zero, then call DSTERF or ZUNGTR and ZSTEQR.  If this fails for
  //     some eigenvalue, then try DSTEBZ.
  
  if( (alleig || ((indeig && il == 1) && iu == n)) && (abstol <= 
   ZERO) ) { 
    dcopy( n, &rwork[indd - 1], 1, w, 1 );
    if( !wantz ) { 
      dsterf( n, w, &rwork[inde - 1], info );
    }
    else { 
      zlacpy( 'A', n, n, a, lda, z, ldz );
      zungtr( uplo, n, z, ldz, &work[indtau - 1], &work[indwrk - 1], 
       llwork, iinfo );
      indrwk = inde + n;
      zsteqr( jobz, n, w, &rwork[inde - 1], z, ldz, &rwork[indrwk - 1], 
       info );
      if( info == 0 ) { 
        for( i = 1, i_ = i - 1, _do2 = n; i <= _do2; i++, i_++ ) { 
          ifail[i_] = 0;
        }
      }
    }
    if( info == 0 ) { 
      m = n;
      goto L_40;
    }
    info = 0;
  }
  
  //     Otherwise, call DSTEBZ and, if eigenvectors are desired, ZSTEIN.
  
  if( wantz ) { 
    order = 'B';
  }
  else { 
    order = 'E';
  }
  indrwk = inde + n;
  indibl = 1;
  indisp = indibl + n;
  indiwk = indisp + n;
  dstebz( range, order, n, vll, vuu, il, iu, abstll, &rwork[indd - 1], 
   &rwork[inde - 1], m, nsplit, w, &iwork[indibl - 1], &iwork[indisp - 1], 
   &rwork[indrwk - 1], &iwork[indiwk - 1], info );
  
  if( wantz ) { 
    zstein( n, &rwork[indd - 1], &rwork[inde - 1], m, w, &iwork[indibl - 1], 
     &iwork[indisp - 1], z, ldz, &rwork[indrwk - 1], &iwork[indiwk - 1], 
     ifail, info );
    
    //        Apply unitary matrix used in reduction to tridiagonal
    //        form to eigenvectors returned by ZSTEIN.
    
    zunmtr( 'L', uplo, 'N', n, m, a, lda, &work[indtau - 1], z, 
     ldz, &work[indwrk - 1], llwork, iinfo );
  }
  
  //     If matrix was scaled, then rescale eigenvalues appropriately.
  
L_40:
  ;
  if( iscale == 1 ) { 
    if( info == 0 ) { 
      imax = m;
    }
    else { 
      imax = info - 1;
    }
    dscal( imax, ONE/sigma, w, 1 );
  }
  
  //     If eigenvalues are not in order, then sort them, along with
  //     eigenvectors.
  
  if( wantz ) { 
    for( j = 1, j_ = j - 1, _do3 = m - 1; j <= _do3; j++, j_++ ) { 
      i = 0;
      tmp1 = w[j_];
      for( jj = j + 1, jj_ = jj - 1, _do4 = m; jj <= _do4; jj++, jj_++ ) { 
        if( w[jj_] < tmp1 ) { 
          i = jj;
          tmp1 = w[jj_];
        }
      }
      
      if( i != 0 ) { 
        itmp1 = iwork[indibl + i - 2];
        w[i - 1] = w[j_];
        iwork[indibl + i - 2] = iwork[indibl + j_ - 1];
        w[j_] = tmp1;
        iwork[indibl + j_ - 1] = itmp1;
        zswap( n, &Z(i - 1,0), 1, &Z(j_,0), 1 );
        if( info != 0 ) { 
          itmp1 = ifail[i - 1];
          ifail[i - 1] = ifail[j_];
          ifail[j_] = itmp1;
        }
      }
    }
  }
  
  //     Set WORK(1) to optimal DComplex workspace size.
  
  work[0] = DComplex((double)max( 2*n - 1, lopt ));
  
  return;
  
  //     End of ZHEEVX
  
#undef  Z
#undef  A
} // end of function 


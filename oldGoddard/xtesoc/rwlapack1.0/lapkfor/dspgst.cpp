/*
 * C++ implementation of lapack routine dspgst
 *
 * $Id: dspgst.cpp,v 1.6 1993/04/06 20:42:19 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:38:00
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dspgst.cpp,v $
 * Revision 1.6  1993/04/06  20:42:19  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.5  1993/03/19  18:41:23  alv
 * now passes chars explicitly, rather than indirection of a string, to shut up SUN warnings
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:17:04  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:08:50  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ONE = 1.0e0;
const double HALF = 0.5e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dspgst(const long &itype, const char &uplo, const long &n, 
 double ap[], double bp[], long &info)
{
  int upper;
  long _do0, _do1, _do2, _do3, j, j1, j1j1, j_, jj, k, k1, k1k1, 
   k_, kk;
  double ajj, akk, bjj, bkk, ct;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DSPGST reduces a real symmetric-definite generalized eigenproblem
  //  to standard form, using packed storage.
  
  //  If ITYPE = 1, the problem is A*x = lambda*B*x,
  //  and A is overwritten by inv(U')*A*inv(U) or inv(L)*A*inv(L')
  
  //  If ITYPE = 2 or 3, the problem is A*B*x = lambda*x or
  //  B*A*x = lambda*x, and A is overwritten by U*A*U` or L'*A*L.
  
  //  B must have been previously factorized as U'*U or L*L' by DPPTRF.
  
  //  Arguments
  //  =========
  
  //  ITYPE   (input) INTEGER
  //          = 1: compute inv(U')*A*inv(U) or inv(L)*A*inv(L');
  //          = 2 or 3: compute U*A*U' or L'*A*L.
  
  //  UPLO    (input) CHARACTER
  //          Specifies whether the upper or lower triangular part of the
  //          symmetric matrix A is stored, and how B has been factorized.
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The order of the matrices A and B.  N >= 0.
  
  //  AP      (input/output) DOUBLE PRECISION array, dimension (N*(N+1)/2)
  //          On entry, the upper or lower triangle of the symmetric matrix
  //          A, packed columnwise in a linear array.  The j-th column of A
  //          is stored in the array AP as follows:
  //          if UPLO = 'U', AP(i + (j-1)*j/2) = A(i,j) for 1<=i<=j;
  //          if UPLO = 'L', AP(i + (j-1)*(2n-j)/2) = A(i,j) for j<=i<=n.
  
  //          On exit, if INFO = 0, the transformed matrix, stored in the
  //          same format as A.
  
  //  BP      (input) DOUBLE PRECISION array, dimension (N*(N+1)/2)
  //          The triangular factor from the Cholesky factorization of B,
  //          stored in the same format as A, as returned by DPPTRF.
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit.
  //          < 0:  if INFO = -i, the i-th argument had an illegal value.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  if( itype < 1 || itype > 3 ) { 
    info = -1;
  }
  else if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -2;
  }
  else if( n < 0 ) { 
    info = -3;
  }
  if( info != 0 ) { 
    xerbla( "DSPGST", -info );
    return;
  }
  
  if( itype == 1 ) { 
    if( upper ) { 
      
      //           Compute inv(U')*A*inv(U)
      
      //           J1 and JJ are the indices of A(1,j) and A(j,j)
      
      jj = 0;
      for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
        j1 = jj + 1;
        jj = jj + j;
        
        //              Compute the j-th column of the upper triangle of A
        
        bjj = bp[jj - 1];
        dtpsv( uplo, 'T'/* Transpose */, 'N'/* Nonunit */, j, 
         bp, &ap[j1 - 1], 1 );
        dspmv( uplo, j - 1, -ONE, ap, &bp[j1 - 1], 1, ONE, 
         &ap[j1 - 1], 1 );
        dscal( j - 1, ONE/bjj, &ap[j1 - 1], 1 );
        ap[jj - 1] = (ap[jj - 1] - ddot( j - 1, &ap[j1 - 1], 
         1, &bp[j1 - 1], 1 ))/bjj;
      }
    }
    else { 
      
      //           Compute inv(L)*A*inv(L')
      
      //           KK and K1K1 are the indices of A(k,k) and A(k+1,k+1)
      
      kk = 1;
      for( k = 1, k_ = k - 1, _do1 = n; k <= _do1; k++, k_++ ) { 
        k1k1 = kk + n - k + 1;
        
        //              Update the lower triangle of A(k:n,k:n)
        
        akk = ap[kk - 1];
        bkk = bp[kk - 1];
        akk = akk/pow(bkk, 2);
        ap[kk - 1] = akk;
        if( k < n ) { 
          dscal( n - k, ONE/bkk, &ap[kk], 1 );
          ct = -HALF*akk;
          daxpy( n - k, ct, &bp[kk], 1, &ap[kk], 1 );
          dspr2( uplo, n - k, -ONE, &ap[kk], 1, &bp[kk], 
           1, &ap[k1k1 - 1] );
          daxpy( n - k, ct, &bp[kk], 1, &ap[kk], 1 );
          dtpsv( uplo, 'N'/* No transpose */, 'N'/* Non-unit */
           , n - k, &bp[k1k1 - 1], &ap[kk], 1 );
        }
        kk = k1k1;
      }
    }
  }
  else { 
    if( upper ) { 
      
      //           Compute U*A*U'
      
      //           K1 and KK are the indices of A(1,k) and A(k,k)
      
      kk = 0;
      for( k = 1, k_ = k - 1, _do2 = n; k <= _do2; k++, k_++ ) { 
        k1 = kk + 1;
        kk = kk + k;
        
        //              Update the upper triangle of A(1:k,1:k)
        
        akk = ap[kk - 1];
        bkk = bp[kk - 1];
        dtpmv( uplo, 'N'/* No transpose */, 'N'/* Non-unit */
         , k - 1, bp, &ap[k1 - 1], 1 );
        ct = HALF*akk;
        daxpy( k - 1, ct, &bp[k1 - 1], 1, &ap[k1 - 1], 1 );
        dspr2( uplo, k - 1, ONE, &ap[k1 - 1], 1, &bp[k1 - 1], 
         1, ap );
        daxpy( k - 1, ct, &bp[k1 - 1], 1, &ap[k1 - 1], 1 );
        dscal( k - 1, bkk, &ap[k1 - 1], 1 );
        ap[kk - 1] = akk*pow(bkk, 2);
      }
    }
    else { 
      
      //           Compute L'*A*L
      
      //           JJ and J1J1 are the indices of A(j,j) and A(j+1,j+1)
      
      jj = 1;
      for( j = 1, j_ = j - 1, _do3 = n; j <= _do3; j++, j_++ ) { 
        j1j1 = jj + n - j + 1;
        
        //              Compute the j-th column of the lower triangle of A
        
        ajj = ap[jj - 1];
        bjj = bp[jj - 1];
        ap[jj - 1] = ajj*bjj + ddot( n - j, &ap[jj], 1, &bp[jj], 
         1 );
        dscal( n - j, bjj, &ap[jj], 1 );
        dspmv( uplo, n - j, ONE, &ap[j1j1 - 1], &bp[jj], 1, 
         ONE, &ap[jj], 1 );
        dtpmv( uplo, 'T'/* Transpose */, 'N'/* Non-unit */, 
         n - j + 1, &bp[jj - 1], &ap[jj - 1], 1 );
        jj = j1j1;
      }
    }
  }
  return;
  
  //     End of DSPGST
  
} // end of function 


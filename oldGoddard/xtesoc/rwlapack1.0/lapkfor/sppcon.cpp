/*
 * C++ implementation of Lapack routine sppcon
 *
 * $Id: sppcon.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:01:57
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: sppcon.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ONE = 1.0e0;
const float ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ sppcon(const char &uplo, long &n, float ap[], float &anorm, 
 float &rcond, float work[], long iwork[], long &info)
{
  int upper;
  char normin;
  long ix, kase;
  float ainvnm, scale, scalel, scaleu, smlnum;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SPPCON estimates the reciprocal of the condition number of a real
  //  symmetric positive definite packed matrix using the Cholesky
  //  factorization A = U'*U or A = L*L' computed by SPPTRF.
  
  //  An estimate is obtained for norm(inv(A)), and the reciprocal of the
  //  condition number is computed as RCOND = 1 / (ANORM * norm(inv(A))).
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the factor stored in A is upper or lower
  //          triangular.
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  AP      (input) REAL array, dimension (N*(N+1)/2)
  //          The triangular factor U or L from the Cholesky factorization
  //          A = U'*U or A = L*L', packed columnwise in a linear array.
  //          The j-th column of U or L is stored in the array AP as
  //          follows:
  //          if UPLO = 'U', AP(i + (j-1)*j/2) = U(i,j) for 1<=i<=j;
  //          if UPLO = 'L', AP(i + (j-1)*(2n-j)/2) = L(i,j) for j<=i<=n.
  
  //  ANORM   (input) REAL
  //          The 1-norm (or infinity-norm) of the symmetric matrix A.
  
  //  RCOND   (output) REAL
  //          The reciprocal of the condition number of the matrix A,
  //          computed as RCOND = 1/(ANORM * AINVNM), where AINVNM is an
  //          estimate of the 1-norm of inv(A) computed in this routine.
  
  //  WORK    (workspace) REAL array, dimension (3*N)
  
  //  IWORK   (workspace) INTEGER array, dimension (N)
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( anorm < ZERO ) { 
    info = -4;
  }
  if( info != 0 ) { 
    xerbla( "SPPCON", -info );
    return;
  }
  
  //     Quick return if possible
  
  rcond = ZERO;
  if( n == 0 ) { 
    rcond = ONE;
    return;
  }
  else if( anorm == ZERO ) { 
    return;
  }
  
  smlnum = slamch( 'S'/*Safe minimum*/ );
  
  //     Estimate the 1-norm of the inverse.
  
  kase = 0;
  normin = 'N';
L_10:
  ;
  slacon( n, &work[n], work, iwork, ainvnm, kase );
  if( kase != 0 ) { 
    if( upper ) { 
      
      //           Multiply by inv(U').
      
      slatps( 'U'/*Upper*/, 'T'/*Transpose*/, 'N'/*Non-unit*/
       , normin, n, ap, work, scalel, &work[n*2], info );
      normin = 'Y';
      
      //           Multiply by inv(U).
      
      slatps( 'U'/*Upper*/, 'N'/*No transpose*/, 'N'/*Non-unit*/
       , normin, n, ap, work, scaleu, &work[n*2], info );
    }
    else { 
      
      //           Multiply by inv(L).
      
      slatps( 'L'/*Lower*/, 'N'/*No transpose*/, 'N'/*Non-unit*/
       , normin, n, ap, work, scalel, &work[n*2], info );
      normin = 'Y';
      
      //           Multiply by inv(L').
      
      slatps( 'L'/*Lower*/, 'T'/*Transpose*/, 'N'/*Non-unit*/
       , normin, n, ap, work, scaleu, &work[n*2], info );
    }
    
    //        Multiply by 1/SCALE if doing so will not cause overflow.
    
    scale = scalel*scaleu;
    if( scale != ONE ) { 
      ix = isamax( n, work, 1 );
      if( scale < abs( work[ix - 1] )*smlnum || scale == ZERO ) 
        goto L_20;
      srscl( n, scale, work, 1 );
    }
    goto L_10;
  }
  
  //     Compute the estimate of the reciprocal condition number.
  
  if( ainvnm != ZERO ) 
    rcond = (ONE/ainvnm)/anorm;
  
L_20:
  ;
  return;
  
  //     End of SPPCON
  
} // end of function 


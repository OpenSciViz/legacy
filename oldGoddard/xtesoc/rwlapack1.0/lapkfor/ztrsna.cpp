/*
 * C++ implementation of Lapack routine ztrsna
 *
 * $Id: ztrsna.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:51:23
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: ztrsna.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ZERO = 0.0e0;
const double ONE = 1.0e0 + 0;
// end of PARAMETER translations

inline double ztrsna_cabs1(DComplex cdum) { return abs( real( (cdum) ) ) + 
   abs( imag( (cdum) ) ); }
RWLAPKDECL void /*FUNCTION*/ ztrsna(const char &job, const char &howmny, int select[], 
 const long &n, DComplex *t, const long &ldt, DComplex *vl, const long &ldvl, 
 DComplex *vr, const long &ldvr, double s[], double sep[], const long &mm, 
 long &m, DComplex *work, const long &ldwork, double rwork[], long &info)
{
#define T(I_,J_)  (*(t+(I_)*(ldt)+(J_)))
#define VL(I_,J_) (*(vl+(I_)*(ldvl)+(J_)))
#define VR(I_,J_) (*(vr+(I_)*(ldvr)+(J_)))
#define WORK(I_,J_) (*(work+(I_)*(ldwork)+(J_)))
  int somcon, wantbh, wants, wantsp;
  char normin;
  long _do0, _do1, _do2, i, i_, ierr, ix, j, j_, k, k_, kase, 
   ks;
  double bignum, eps, est, lnrm, rnrm, scale, smlnum, xnorm;
  DComplex cdum, dummy[1], prod;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZTRSNA estimates reciprocal condition numbers for specified
  //  eigenvalues and/or right eigenvectors of a DComplex upper triangular
  //  matrix T (or of any matrix Q*T*Q' with Q unitary).
  
  //  Arguments
  //  =========
  
  //  JOB     (input) CHARACTER*1
  //          Specifies whether condition numbers are required for
  //          eigenvalues (S) or eigenvectors (SEP):
  //          = 'E': for eigenvalues only (S);
  //          = 'V': for eigenvectors only (SEP);
  //          = 'B': for both eigenvalues and eigenvectors (S and SEP).
  
  //  HOWMNY  (input) CHARACTER*1
  //          = 'A': compute condition numbers for all eigenpairs;
  //          = 'S': compute condition numbers for selected eigenpairs
  //                 specified by the array SELECT.
  
  //  SELECT  (input) LOGICAL array, dimension (N)
  //          If HOWMNY = 'S', SELECT specifies the eigenpairs for which
  //          condition numbers are required. To select condition numbers
  //          for the j-th eigenpair, SELECT(j) must be set to .TRUE..
  //          If HOWMNY = 'A', SELECT is not referenced.
  
  //  N       (input) INTEGER
  //          The order of the matrix T. N >= 0.
  
  //  T       (input) COMPLEX*16 array, dimension (LDT,N)
  //          The upper triangular matrix T.
  
  //  LDT     (input) INTEGER
  //          The leading dimension of the array T. LDT >= max(1,N).
  
  //  VL      (input) COMPLEX*16 array, dimension (LDVL,M)
  //          If JOB = 'E' or 'B', VL must contain left eigenvectors of T
  //          (or of any Q*T*Q' with Q unitary), corresponding to the
  //          eigenpairs specified by HOWMNY and SELECT. The eigenvectors
  //          must be stored in consecutive columns of VL, as returned by
  //          ZHSEIN or ZTREVC.
  //          If JOB = 'V', VL is not referenced.
  
  //  LDVL    (input) INTEGER
  //          The leading dimension of the array VL.
  //          LDVL >= 1; and if JOB = 'E' or 'B', LDVL >= N.
  
  //  VR      (input) COMPLEX*16 array, dimension (LDVR,M)
  //          If JOB = 'E' or 'B', VR must contain right eigenvectors of T
  //          (or of any Q*T*Q' with Q unitary), corresponding to the
  //          eigenpairs specified by HOWMNY and SELECT. The eigenvectors
  //          must be stored in consecutive columns of VR, as returned by
  //          ZHSEIN or ZTREVC.
  //          If JOB = 'V', VR is not referenced.
  
  //  LDVR    (input) INTEGER
  //          The leading dimension of the array VR.
  //          LDVR >= 1; and if JOB = 'E' or 'B', LDVR >= N.
  
  //  S       (output) DOUBLE PRECISION array, dimension (MM)
  //          If JOB = 'E' or 'B', the reciprocal condition numbers of the
  //          selected eigenvalues, stored in consecutive elements of the
  //          array. Thus S(j), SEP(j), and the j-th columns of VL and VR
  //          all correspond to the same eigenpair (but not in general the
  //          j-th eigenpair, unless all eigenpairs are selected).
  //          If JOB = 'V', S is not referenced.
  
  //  SEP     (output) DOUBLE PRECISION array, dimension (MM)
  //          If JOB = 'V' or 'B', the estimated reciprocal condition
  //          numbers of the selected eigenvectors, stored in consecutive
  //          elements of the array.
  //          If JOB = 'E', SEP is not referenced.
  
  //  MM      (input) INTEGER
  //          The number of elements in the arrays S and SEP. MM >= M.
  
  //  M       (output) INTEGER
  //          The number of elements of the arrays S and SEP used to store
  //          the specified condition numbers. If HOWMNY = 'A', M is set
  //          to N.
  
  //  WORK    (workspace) COMPLEX*16 array, dimension (LDWORK,N+1)
  //          If JOB = 'E', WORK is not referenced.
  
  //  LDWORK  (input) INTEGER
  //          The leading dimension of the array WORK.
  //          LDWORK >= 1; and if JOB = 'V' or 'B', LDWORK >= N.
  
  //  RWORK   (workspace) DOUBLE PRECISION array, dimension (N)
  //          If JOB = 'E', RWORK is not referenced.
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument had an illegal value
  
  //  Further Details
  //  ===============
  
  //  The reciprocal of the condition number of an eigenvalue lambda is
  //  defined as
  
  //          S(lambda) = |v'*u| / (norm(u)*norm(v))
  
  //  where u and v are the right and left eigenvectors of T corresponding
  //  to lambda; v' denotes the conjugate transpose of v, and norm(u)
  //  denotes the Euclidean norm. These reciprocal condition numbers always
  //  lie between zero (very badly conditioned) and one (very well
  //  conditioned). If n = 1, S(lambda) is defined to be 1.
  
  //  An approximate error bound for a computed eigenvalue W(i) is given by
  
  //                      EPS * norm(T) / S(i)
  
  //  where EPS is the machine precision.
  
  //  The reciprocal of the condition number of the right eigenvector u
  //  corresponding to lambda is defined as follows. Suppose
  
  //              T = ( lambda  c  )
  //                  (   0    T22 )
  
  //  Then the reciprocal condition number is
  
  //          SEP( lambda, T22 ) = sigma-min( T22 - lambda*I )
  
  //  where sigma-min denotes the smallest singular value. We approximate
  //  the smallest singular value by the reciprocal of an estimate of the
  //  one-norm of the inverse of T22 - lambda*I. If n = 1, SEP(1) is
  //  defined to be abs(T(1,1)).
  
  //  An approximate error bound for a computed right eigenvector VR(i)
  //  is given by
  
  //                      EPS * norm(T) / SEP(i)
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Local Arrays ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Statement Functions ..
  //     ..
  //     .. Statement Function definitions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Decode and test the input parameters
  
  wantbh = lsame( job, 'B' );
  wants = lsame( job, 'E' ) || wantbh;
  wantsp = lsame( job, 'V' ) || wantbh;
  
  somcon = lsame( howmny, 'S' );
  
  //     Set M to the number of eigenpairs for which condition numbers are
  //     to be computed.
  
  if( somcon ) { 
    m = 0;
    for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
      if( select[j_] ) 
        m = m + 1;
    }
  }
  else { 
    m = n;
  }
  
  info = 0;
  if( !wants && !wantsp ) { 
    info = -1;
  }
  else if( !lsame( howmny, 'A' ) && !somcon ) { 
    info = -2;
  }
  else if( n < 0 ) { 
    info = -4;
  }
  else if( ldt < max( 1, n ) ) { 
    info = -6;
  }
  else if( ldvl < 1 || (wants && ldvl < n) ) { 
    info = -8;
  }
  else if( ldvr < 1 || (wants && ldvr < n) ) { 
    info = -10;
  }
  else if( mm < m ) { 
    info = -13;
  }
  else if( ldwork < 1 || (wantsp && ldwork < n) ) { 
    info = -16;
  }
  if( info != 0 ) { 
    xerbla( "ZTRSNA", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  if( n == 1 ) { 
    if( somcon ) { 
      if( !select[0] ) 
        return;
    }
    if( wants ) 
      s[0] = ONE;
    if( wantsp ) 
      sep[0] = abs( T(0,0) );
    return;
  }
  
  //     Get machine constants
  
  eps = dlamch( 'P' );
  smlnum = dlamch( 'S' )/eps;
  bignum = ONE/smlnum;
  dlabad( smlnum, bignum );
  
  ks = 1;
  for( k = 1, k_ = k - 1, _do1 = n; k <= _do1; k++, k_++ ) { 
    
    if( somcon ) { 
      if( !select[k_] ) 
        goto L_50;
    }
    
    if( wants ) { 
      
      //           Compute the reciprocal condition number of the k-th
      //           eigenvalue.
      
      prod = zdotc( n, &VR(ks - 1,0), 1, &VL(ks - 1,0), 1 );
      rnrm = dznrm2( n, &VR(ks - 1,0), 1 );
      lnrm = dznrm2( n, &VL(ks - 1,0), 1 );
      s[ks - 1] = abs( prod )/(rnrm*lnrm);
      
    }
    
    if( wantsp ) { 
      
      //           Estimate the reciprocal condition number of the k-th
      //           eigenvector.
      
      //           Copy the matrix T to the array WORK and swap the k-th
      //           diagonal element to the (1,1) position.
      
      zlacpy( 'F'/*Full*/, n, n, t, ldt, work, ldwork );
      ztrexc( 'N'/*No Q*/, n, work, ldwork, dummy, 1, k, 1, 
       ierr );
      
      //           Form  C = T22 - lambda*I in WORK(2:N,2:N).
      
      for( i = 2, i_ = i - 1, _do2 = n; i <= _do2; i++, i_++ ) { 
        WORK(i_,i_) = WORK(i_,i_) - WORK(0,0);
      }
      
      //           Estimate a lower bound for the 1-norm of inv(C'). The 1st
      //           and (N+1)th columns of WORK are used to store work vectors.
      
      sep[ks - 1] = ZERO;
      est = ZERO;
      kase = 0;
      normin = 'N';
L_30:
      ;
      zlacon( n - 1, &WORK(n,0), work, est, kase );
      
      if( kase != 0 ) { 
        if( kase == 1 ) { 
          
          //                 Solve C'*x = scale*b
          
          zlatrs( 'U'/*Upper*/, 'C'/*Conjugate transpose*/
           , 'N'/*Nonunit*/, normin, n - 1, &WORK(1,1), 
           ldwork, work, scale, rwork, ierr );
        }
        else { 
          
          //                 Solve C*x = scale*b
          
          zlatrs( 'U'/*Upper*/, 'N'/*No transpose*/, 'N'/*Nonunit*/
           , normin, n - 1, &WORK(1,1), ldwork, work, scale, 
           rwork, ierr );
        }
        normin = 'Y';
        if( scale != ONE ) { 
          
          //                 Multiply by 1/SCALE if doing so will not cause
          //                 overflow.
          
          ix = izamax( n - 1, work, 1 );
          xnorm = ztrsna_cabs1( WORK(0,ix - 1) );
          if( scale < xnorm*smlnum || scale == ZERO ) 
            goto L_40;
          zdrscl( n, scale, work, 1 );
        }
        goto L_30;
      }
      
      sep[ks - 1] = ONE/max( est, smlnum );
    }
    
L_40:
    ;
    ks = ks + 1;
L_50:
    ;
  }
  return;
  
  //     End of ZTRSNA
  
#undef  WORK
#undef  VR
#undef  VL
#undef  T
} // end of function 


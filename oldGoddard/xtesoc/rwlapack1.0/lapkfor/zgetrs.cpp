/*
 * C++ implementation of Lapack routine zgetrs
 *
 * $Id: zgetrs.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:47:00
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zgetrs.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const DComplex ONE = DComplex(1.0e0);
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zgetrs(const char &trans, const long &n, const long &nrhs, 
 DComplex *a, const long &lda, long ipiv[], DComplex *b, const long &ldb, 
 long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
  int notran;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZGETRS solves a system of linear equations
  //     A * X = B,  A**T * X = B,  or  A**H * X = B
  //  with a general n by n matrix A using the LU factorization computed
  //  by ZGETRF.
  
  //  Arguments
  //  =========
  
  //  TRANS   (input) CHARACTER*1
  //          Specifies the form of the system of equations.
  //          = 'N':  A * X = B     (No transpose)
  //          = 'T':  A**T * X = B  (Transpose)
  //          = 'C':  A**H * X = B  (Conjugate transpose)
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right hand sides, i.e., the number of columns
  //          of the matrix B.  NRHS >= 0.
  
  //  A       (input) COMPLEX*16 array, dimension (LDA,N)
  //          The factors L and U from the factorization A = P*L*U
  //          as computed by ZGETRF.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  IPIV    (input) INTEGER array, dimension (N)
  //          The pivot indices from ZGETRF; for 1<=i<=N, row i of the
  //          matrix was interchanged with row IPIV(i).
  
  //  B       (input/output) COMPLEX*16 array, dimension (LDB,NRHS)
  //          On entry, the right hand side vectors B for the system of
  //          linear equations.
  //          On exit, the solution vectors, X.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  notran = lsame( trans, 'N' );
  if( (!notran && !lsame( trans, 'T' )) && !lsame( trans, 'C' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( nrhs < 0 ) { 
    info = -3;
  }
  else if( lda < max( 1, n ) ) { 
    info = -5;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -8;
  }
  if( info != 0 ) { 
    xerbla( "ZGETRS", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 || nrhs == 0 ) 
    return;
  
  if( notran ) { 
    
    //        Solve A * X = B.
    
    //        Apply row interchanges to the right hand sides.
    
    zlaswp( nrhs, b, ldb, 1, n, ipiv, 1 );
    
    //        Solve L*X = B, overwriting B with X.
    
    ztrsm( 'L'/*Left*/, 'L'/*Lower*/, 'N'/*No transpose*/, 
     'U'/*Unit*/, n, nrhs, ONE, a, lda, b, ldb );
    
    //        Solve U*X = B, overwriting B with X.
    
    ztrsm( 'L'/*Left*/, 'U'/*Upper*/, 'N'/*No transpose*/, 
     'N'/*Non-unit*/, n, nrhs, ONE, a, lda, b, ldb );
  }
  else { 
    
    //        Solve A**T * X = B  or A**H * X = B.
    
    //        Solve U'*X = B, overwriting B with X.
    
    ztrsm( 'L'/*Left*/, 'U'/*Upper*/, trans, 'N'/*Non-unit*/
     , n, nrhs, ONE, a, lda, b, ldb );
    
    //        Solve L'*X = B, overwriting B with X.
    
    ztrsm( 'L'/*Left*/, 'L'/*Lower*/, trans, 'U'/*Unit*/, n, 
     nrhs, ONE, a, lda, b, ldb );
    
    //        Apply row interchanges to the solution vectors.
    
    zlaswp( nrhs, b, ldb, 1, n, ipiv, -1 );
  }
  
  return;
  
  //     End of ZGETRS
  
#undef  B
#undef  A
} // end of function 


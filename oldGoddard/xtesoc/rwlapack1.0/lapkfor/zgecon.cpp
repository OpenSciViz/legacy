/*
 * C++ implementation of Lapack routine zgecon
 *
 * $Id: zgecon.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:46:08
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zgecon.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ONE = 1.0e0;
const double ZERO = 0.0e0;
// end of PARAMETER translations

inline double zgecon_cabs1(DComplex zdum) { return abs( real( (zdum) ) ) + 
   abs( imag( (zdum) ) ); }
RWLAPKDECL void /*FUNCTION*/ zgecon(const char &norm, const long &n, DComplex *a, const long &lda, 
 const double &anorm, double &rcond, DComplex work[], double rwork[], long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  int onenrm;
  char normin;
  long ix, kase, kase1;
  double ainvnm, scale, sl, smlnum, su;
  DComplex zdum;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZGECON estimates the reciprocal of the condition number of a DComplex
  //  general matrix A, in either the 1-norm or the infinity-norm, using
  //  the LU factorization computed by ZGETRF.
  
  //  An estimate is obtained for norm(inv(A)), and the reciprocal of the
  //  condition number is computed as
  //     RCOND = 1 / ( norm(A) * norm(inv(A)) ).
  
  //  Arguments
  //  =========
  
  //  NORM    (input) CHARACTER*1
  //          Specifies whether the 1-norm condition number or the
  //          infinity-norm condition number is required:
  //          = '1' or 'O':  1-norm
  //          = 'I':         Infinity-norm
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  A       (input) COMPLEX*16 array, dimension (LDA,N)
  //          The factors L and U from the factorization A = P*L*U
  //          as computed by ZGETRF.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  ANORM   (input) DOUBLE PRECISION
  //          If NORM = '1' or 'O', the 1-norm of the original matrix A.
  //          If NORM = 'I', the infinity-norm of the original matrix A.
  
  //  RCOND   (output) DOUBLE PRECISION
  //          The reciprocal of the condition number of the matrix A,
  //          computed as RCOND = 1/(norm(A) * norm(inv(A))).
  
  //  WORK    (workspace) COMPLEX*16 array, dimension (2*N)
  
  //  RWORK   (workspace) DOUBLE PRECISION array, dimension (2*N)
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Statement Functions ..
  //     ..
  //     .. Statement Function definitions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  onenrm = norm == '1' || lsame( norm, 'O' );
  if( !onenrm && !lsame( norm, 'I' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( lda < max( 1, n ) ) { 
    info = -4;
  }
  else if( anorm < ZERO ) { 
    info = -5;
  }
  if( info != 0 ) { 
    xerbla( "ZGECON", -info );
    return;
  }
  
  //     Quick return if possible
  
  rcond = ZERO;
  if( n == 0 ) { 
    rcond = ONE;
    return;
  }
  else if( anorm == ZERO ) { 
    return;
  }
  
  smlnum = dlamch( 'S'/*Safe minimum*/ );
  
  //     Estimate the norm of inv(A).
  
  ainvnm = ZERO;
  normin = 'N';
  if( onenrm ) { 
    kase1 = 1;
  }
  else { 
    kase1 = 2;
  }
  kase = 0;
L_10:
  ;
  zlacon( n, &work[n], work, ainvnm, kase );
  if( kase != 0 ) { 
    if( kase == kase1 ) { 
      
      //           Multiply by inv(L).
      
      zlatrs( 'L'/*Lower*/, 'N'/*No transpose*/, 'U'/*Unit*/
       , normin, n, a, lda, work, sl, rwork, info );
      
      //           Multiply by inv(U).
      
      zlatrs( 'U'/*Upper*/, 'N'/*No transpose*/, 'N'/*Non-unit*/
       , normin, n, a, lda, work, su, &rwork[n], info );
    }
    else { 
      
      //           Multiply by inv(U').
      
      zlatrs( 'U'/*Upper*/, 'C'/*Conjugate transpose*/, 'N'/*Non-unit*/
       , normin, n, a, lda, work, su, &rwork[n], info );
      
      //           Multiply by inv(L').
      
      zlatrs( 'L'/*Lower*/, 'C'/*Conjugate transpose*/, 'U'/*Unit*/
       , normin, n, a, lda, work, sl, rwork, info );
    }
    
    //        Divide X by 1/(SL*SU) if doing so will not cause overflow.
    
    scale = sl*su;
    normin = 'Y';
    if( scale != ONE ) { 
      ix = izamax( n, work, 1 );
      if( scale < zgecon_cabs1( work[ix - 1] )*smlnum || scale == 
       ZERO ) 
        goto L_20;
      zdrscl( n, scale, work, 1 );
    }
    goto L_10;
  }
  
  //     Compute the estimate of the reciprocal condition number.
  
  if( ainvnm != ZERO ) 
    rcond = (ONE/ainvnm)/anorm;
  
L_20:
  ;
  return;
  
  //     End of ZGECON
  
#undef  A
} // end of function 


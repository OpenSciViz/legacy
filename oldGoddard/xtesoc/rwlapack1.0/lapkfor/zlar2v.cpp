/*
 * C++ implementation of Lapack routine zlar2v
 *
 * $Id: zlar2v.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:48:57
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zlar2v.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

RWLAPKDECL void /*FUNCTION*/ zlar2v(const long &n, DComplex x[], DComplex y[], DComplex z[], 
   const long &incx, double c[], DComplex s[], const long &incc)
{
  long _do0, i, i_, ic, ix;
  double ci, sii, sir, t1i, t1r, t5, t6, xi, yi, zii, zir;
  DComplex si, t2, t3, t4, zi;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZLAR2V applies a vector of DComplex plane rotations with real cosines
  //  from both sides to a sequence of 2-by-2 DComplex Hermitian matrices,
  //  defined by the elements of the vectors x, y and z. For i = 1,2,...,n
  
  //     (       x(i)  z(i) ) :=
  //     ( conjg(z(i)) y(i) )
  
  //       (  c(i) conjg(s(i)) ) (       x(i)  z(i) ) ( c(i) -conjg(s(i)) )
  //       ( -s(i)       c(i)  ) ( conjg(z(i)) y(i) ) ( s(i)        c(i)  )
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The number of plane rotations to be applied.
  
  //  X       (input/output) COMPLEX*16 array, dimension (1+(N-1)*INCX)
  //          The vector x; the elements of x are assumed to be real.
  
  //  Y       (input/output) COMPLEX*16 array, dimension (1+(N-1)*INCX)
  //          The vector y; the elements of y are assumed to be real.
  
  //  Z       (input/output) COMPLEX*16 array, dimension (1+(N-1)*INCX)
  //          The vector z.
  
  //  INCX    (input) INTEGER
  //          The increment between elements of X, Y and Z. INCX > 0.
  
  //  C       (input) DOUBLE PRECISION array, dimension (1+(N-1)*INCC)
  //          The cosines of the plane rotations.
  
  //  S       (input) COMPLEX*16 array, dimension (1+(N-1)*INCC)
  //          The sines of the plane rotations.
  
  //  INCC    (input) INTEGER
  //          The increment between elements of C and S. INCC > 0.
  
  //  =====================================================================
  
  //     .. Local Scalars ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  ix = 1;
  ic = 1;
  for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
    xi = real( x[ix - 1] );
    yi = real( y[ix - 1] );
    zi = z[ix - 1];
    zir = real( zi );
    zii = imag( zi );
    ci = c[ic - 1];
    si = s[ic - 1];
    sir = real( si );
    sii = imag( si );
    t1r = sir*zir - sii*zii;
    t1i = sir*zii + sii*zir;
    t2 = ci*zi;
    t3 = t2 - conj( si )*xi;
    t4 = conj( t2 ) + si*yi;
    t5 = ci*xi + t1r;
    t6 = ci*yi - t1r;
    x[ix - 1] = DComplex(ci*t5 + (sir*real( t4 ) + sii*imag( t4 )));
    y[ix - 1] = DComplex(ci*t6 - (sir*real( t3 ) - sii*imag( t3 )));
    z[ix - 1] = ci*t3 + conj( si )*DComplex( t6, t1i );
    ix = ix + incx;
    ic = ic + incc;
  }
  return;
  
  //     End of ZLAR2V
  
} // end of function 


/*
 * C++ implementation of lapack routine dlaexc
 *
 * $Id: dlaexc.cpp,v 1.7 1993/04/06 20:40:57 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:35:08
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dlaexc.cpp,v $
 * Revision 1.7  1993/04/06  20:40:57  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.6  1993/03/19  22:46:26  alv
 * changed a static long to a #define due to SUN bug
 *
 * Revision 1.5  1993/03/19  18:41:23  alv
 * now passes chars explicitly, rather than indirection of a string, to shut up SUN warnings
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:15:12  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:07:09  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
const double TEN = 1.0e1;
/*
 * const long LDD = 4;
 * const long LDX = 2;
 * If I use the above, sun complains a couple hundred lines
 * down that symbol LDD is undefined --- scary stuff
 */
#define LDD 4L
#define LDX 2L
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dlaexc(const int &wantq, const long &n, double *t, 
 const long &ldt, double *q, const long &ldq, const long &j1, const long &n1, 
 const long &n2, double work[], long &info)
{
#define T(I_,J_)  (*(t+(I_)*(ldt)+(J_)))
#define Q(I_,J_)  (*(q+(I_)*(ldq)+(J_)))
  long ierr, j2, j3, j4, k, nd;
  double cs, d[4][LDD], dnorm, eps, scale, smlnum, sn, t11, t22, 
   t33, tau, tau1, tau2, temp, thresh, u[3], u1[3], u2[3], wi1, 
   wi2, wr1, wr2, x[2][LDX], xnorm;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DLAEXC swaps adjacent diagonal blocks T11 and T22 of order 1 or 2 in
  //  an upper quasi-triangular matrix T by an orthogonal similarity
  //  transformation.
  
  //  T must be in Schur canonical form, that is, block upper triangular
  //  with 1-by-1 and 2-by-2 diagonal blocks; each 2-by-2 diagonal block
  //  has its diagonal elemnts equal and its off-diagonal elements of
  //  opposite sign.
  
  //  Arguments
  //  =========
  
  //  WANTQ   (input) LOGICAL
  //          = .TRUE. : accumulate the transformation in the matrix Q;
  //          = .FALSE.: do not accumulate the transformation.
  
  //  N       (input) INTEGER
  //          The order of the matrix T. N >= 0.
  
  //  T       (input/output) DOUBLE PRECISION array, dimension (LDT,N)
  //          On entry, the upper quasi-triangular matrix T, in Schur
  //          canonical form.
  //          On exit, the updated matrix T, again in Schur canonical form.
  
  //  LDT     (input)  INTEGER
  //          The leading dimension of the array T. LDT >= max(1,N).
  
  //  Q       (input/output) DOUBLE PRECISION array, dimension (LDQ,N)
  //          On entry, if WANTQ is .TRUE., the orthogonal matrix Q.
  //          On exit, if WANTQ is .TRUE., the updated matrix Q.
  //          If WANTQ is .FALSE., Q is not referenced.
  
  //  LDQ     (input) INTEGER
  //          The leading dimension of the array Q.
  //          LDQ >= 1; and if WANTQ is .TRUE., LDQ >= N.
  
  //  J1      (input) INTEGER
  //          The index of the first row of the first block T11.
  
  //  N1      (input) INTEGER
  //          The order of the first block T11. N1 = 0, 1 or 2.
  
  //  N2      (input) INTEGER
  //          The order of the second block T22. N2 = 0, 1 or 2.
  
  //  WORK    (workspace) DOUBLE PRECISION array, dimension (N)
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          = 1: the transformed matrix T would be too far from Schur
  //               form; the blocks are not swapped and T and Q are
  //               unchanged.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Local Arrays ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  info = 0;
  
  //     Quick return if possible
  
  if( (n == 0 || n1 == 0) || n2 == 0 ) 
    return;
  if( j1 + n1 > n ) 
    return;
  
  j2 = j1 + 1;
  j3 = j1 + 2;
  j4 = j1 + 3;
  
  if( n1 == 1 && n2 == 1 ) { 
    
    //        Swap two 1-by-1 blocks.
    
    t11 = T(j1 - 1,j1 - 1);
    t22 = T(j2 - 1,j2 - 1);
    
    //        Determine the transformation to perform the interchange.
    
    dlartg( T(j2 - 1,j1 - 1), t22 - t11, cs, sn, temp );
    
    //        Apply transformation to the matrix T.
    
    if( j3 <= n ) 
      drot( n - j1 - 1, &T(j3 - 1,j1 - 1), ldt, &T(j3 - 1,j2 - 1), 
       ldt, cs, sn );
    drot( j1 - 1, &T(j1 - 1,0), 1, &T(j2 - 1,0), 1, cs, sn );
    
    T(j1 - 1,j1 - 1) = t22;
    T(j2 - 1,j2 - 1) = t11;
    
    if( wantq ) { 
      
      //           Accumulate transformation in the matrix Q.
      
      drot( n, &Q(j1 - 1,0), 1, &Q(j2 - 1,0), 1, cs, sn );
    }
    
  }
  else { 
    
    //        Swapping involves at least one 2-by-2 block.
    
    //        Copy the diagonal block of order N1+N2 to the local array D
    //        and compute its norm.
    
    nd = n1 + n2;
    dlacpy( 'F'/* Full */, nd, nd, &T(j1 - 1,j1 - 1), ldt, (double*)d, LDD );
    dnorm = dlange( 'M'/* Max */, nd, nd, (double*)d, LDD, work );
    
    //        Compute machine-dependent threshold for test for accepting
    //        swap.
    
    eps = dlamch( 'P' );
    smlnum = dlamch( 'S' )/eps;
    thresh = max( TEN*eps*dnorm, smlnum );
    
    //        Solve T11*X - X*T22 = scale*T12 for X.
    
    dlasy2( FALSE, FALSE, -1, n1, n2, (double*)d, LDD, &d[n1][n1], 
     LDD, &d[n1][0], LDD, scale, (double*)x, LDX, xnorm, ierr );
    
    //        Swap the adjacent diagonal blocks.
    
    k = n1 + n1 + n2 - 3;
    switch( k ) { 
      case 1: goto L_10;
      case 2: goto L_20;
      case 3: goto L_30;
    }
    
L_10:
    ;
    
    //        N1 = 1, N2 = 2: generate elementary reflector H so that:
    
    //        ( scale, X11, X12 ) H = ( 0, 0, * )
    
    u[0] = scale;
    u[1] = x[0][0];
    u[2] = x[1][0];
    dlarfg( 3, u[2], u, 1, tau );
    u[2] = ONE;
    t11 = T(j1 - 1,j1 - 1);
    
    //        Perform swap provisionally on diagonal block in D.
    
    dlarfx( 'L', 3, 3, u, tau, (double*)d, LDD, work );
    dlarfx( 'R', 3, 3, u, tau, (double*)d, LDD, work );
    
    //        Test whether to reject swap.
    
    if( vmax( abs( d[0][2] ), abs( d[1][2] ), abs( d[2][2] - t11 ), 
     FEND ) > thresh ) 
      goto L_50;
    
    //        Accept swap: apply transformation to the entire matrix T.
    
    dlarfx( 'L', 3, n - j1 + 1, u, tau, &T(j1 - 1,j1 - 1), ldt, 
     work );
    dlarfx( 'R', j2, 3, u, tau, &T(j1 - 1,0), ldt, work );
    
    T(j1 - 1,j3 - 1) = ZERO;
    T(j2 - 1,j3 - 1) = ZERO;
    T(j3 - 1,j3 - 1) = t11;
    
    if( wantq ) { 
      
      //           Accumulate transformation in the matrix Q.
      
      dlarfx( 'R', n, 3, u, tau, &Q(j1 - 1,0), ldq, work );
    }
    goto L_40;
    
L_20:
    ;
    
    //        N1 = 2, N2 = 1: generate elementary reflector H so that:
    
    //        H (  -X11 ) = ( * )
    //          (  -X21 ) = ( 0 )
    //          ( scale ) = ( 0 )
    
    u[0] = -x[0][0];
    u[1] = -x[0][1];
    u[2] = scale;
    dlarfg( 3, u[0], &u[1], 1, tau );
    u[0] = ONE;
    t33 = T(j3 - 1,j3 - 1);
    
    //        Perform swap provisionally on diagonal block in D.
    
    dlarfx( 'L', 3, 3, u, tau, (double*)d, LDD, work );
    dlarfx( 'R', 3, 3, u, tau, (double*)d, LDD, work );
    
    //        Test whether to reject swap.
    
    if( vmax( abs( d[0][1] ), abs( d[0][2] ), abs( d[0][0] - t33 ), 
     FEND ) > thresh ) 
      goto L_50;
    
    //        Accept swap: apply transformation to the entire matrix T.
    
    dlarfx( 'R', j3, 3, u, tau, &T(j1 - 1,0), ldt, work );
    dlarfx( 'L', 3, n - j1, u, tau, &T(j2 - 1,j1 - 1), ldt, work );
    
    T(j1 - 1,j1 - 1) = t33;
    T(j1 - 1,j2 - 1) = ZERO;
    T(j1 - 1,j3 - 1) = ZERO;
    
    if( wantq ) { 
      
      //           Accumulate transformation in the matrix Q.
      
      dlarfx( 'R', n, 3, u, tau, &Q(j1 - 1,0), ldq, work );
    }
    goto L_40;
    
L_30:
    ;
    
    //        N1 = 2, N2 = 2: generate elementary reflectors H(1) and H(2) so
    //        that:
    
    //        H(2) H(1) (  -X11  -X12 ) = (  *  * )
    //                  (  -X21  -X22 )   (  0  * )
    //                  ( scale    0  )   (  0  0 )
    //                  (    0  scale )   (  0  0 )
    
    u1[0] = -x[0][0];
    u1[1] = -x[0][1];
    u1[2] = scale;
    dlarfg( 3, u1[0], &u1[1], 1, tau1 );
    u1[0] = ONE;
    
    temp = -tau1*(x[1][0] + u1[1]*x[1][1]);
    u2[0] = -temp*u1[1] - x[1][1];
    u2[1] = -temp*u1[2];
    u2[2] = scale;
    dlarfg( 3, u2[0], &u2[1], 1, tau2 );
    u2[0] = ONE;
    
    //        Perform swap provisionally on diagonal block in D.
    
    dlarfx( 'L', 3, 4, u1, tau1, (double*)d, LDD, work );
    dlarfx( 'R', 4, 3, u1, tau1, (double*)d, LDD, work );
    dlarfx( 'L', 3, 4, u2, tau2, &d[0][1], LDD, work );
    dlarfx( 'R', 4, 3, u2, tau2, &d[1][0], LDD, work );
    
    //        Test whether to reject swap.
    
    if( vmax( abs( d[0][2] ), abs( d[1][2] ), abs( d[0][3] ), 
     abs( d[1][3] ), FEND ) > thresh ) 
      goto L_50;
    
    //        Accept swap: apply transformation to the entire matrix T.
    
    dlarfx( 'L', 3, n - j1 + 1, u1, tau1, &T(j1 - 1,j1 - 1), ldt, 
     work );
    dlarfx( 'R', j4, 3, u1, tau1, &T(j1 - 1,0), ldt, work );
    dlarfx( 'L', 3, n - j1 + 1, u2, tau2, &T(j1 - 1,j2 - 1), ldt, 
     work );
    dlarfx( 'R', j4, 3, u2, tau2, &T(j2 - 1,0), ldt, work );
    
    T(j1 - 1,j3 - 1) = ZERO;
    T(j2 - 1,j3 - 1) = ZERO;
    T(j1 - 1,j4 - 1) = ZERO;
    T(j2 - 1,j4 - 1) = ZERO;
    
    if( wantq ) { 
      
      //           Accumulate transformation in the matrix Q.
      
      dlarfx( 'R', n, 3, u1, tau1, &Q(j1 - 1,0), ldq, work );
      dlarfx( 'R', n, 3, u2, tau2, &Q(j2 - 1,0), ldq, work );
    }
    
L_40:
    ;
    
    if( n2 == 2 ) { 
      
      //           Standardize new 2-by-2 block T11
      
      dlanv2( T(j1 - 1,j1 - 1), T(j2 - 1,j1 - 1), T(j1 - 1,j2 - 1), 
       T(j2 - 1,j2 - 1), wr1, wi1, wr2, wi2, cs, sn );
      drot( n - j1 - 1, &T(j1 + 1,j1 - 1), ldt, &T(j1 + 1,j2 - 1), 
       ldt, cs, sn );
      drot( j1 - 1, &T(j1 - 1,0), 1, &T(j2 - 1,0), 1, cs, sn );
      if( wantq ) 
        drot( n, &Q(j1 - 1,0), 1, &Q(j2 - 1,0), 1, cs, sn );
    }
    
    if( n1 == 2 ) { 
      
      //           Standardize new 2-by-2 block T22
      
      j3 = j1 + n2;
      j4 = j3 + 1;
      dlanv2( T(j3 - 1,j3 - 1), T(j4 - 1,j3 - 1), T(j3 - 1,j4 - 1), 
       T(j4 - 1,j4 - 1), wr1, wi1, wr2, wi2, cs, sn );
      if( j3 + 2 <= n ) 
        drot( n - j3 - 1, &T(j3 + 1,j3 - 1), ldt, &T(j3 + 1,j4 - 1), 
         ldt, cs, sn );
      drot( j3 - 1, &T(j3 - 1,0), 1, &T(j4 - 1,0), 1, cs, sn );
      if( wantq ) 
        drot( n, &Q(j3 - 1,0), 1, &Q(j4 - 1,0), 1, cs, sn );
    }
    
  }
  return;
  
  //     Exit with INFO = 1 if swap was rejected.
  
L_50:
  ;
  info = 1;
  return;
  
  //     End of DLAEXC
  
#undef  T
#undef  Q
} // end of function 


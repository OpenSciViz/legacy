/*
 * C++ implementation of lapack routine dspevx
 *
 * $Id: dspevx.cpp,v 1.6 1993/04/06 20:42:18 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:37:58
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dspevx.cpp,v $
 * Revision 1.6  1993/04/06  20:42:18  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.5  1993/03/19  18:41:23  alv
 * now passes chars explicitly, rather than indirection of a string, to shut up SUN warnings
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:17:03  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:08:48  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dspevx(const char &jobz, const char &range, const char &uplo, long &n, 
 double ap[], double &vl, double &vu, long &il, long &iu, 
 double &abstol, long &m, double w[], double *z, long &ldz, 
 double work[], long iwork[], long ifail[], long &info)
{
#define Z(I_,J_)  (*(z+(I_)*(ldz)+(J_)))
  int alleig, indeig, valeig, wantz;
  char order;
  long _do0, _do1, _do2, i, i_, iinfo, imax, indd, inde, indibl, 
   indisp, indiwo, indtau, indwrk, iscale, itmp1, j, j_, jj, jj_, 
   nsplit;
  double abstll, anrm, bignum, eps, rmax, rmin, safmin, sigma, smlnum, 
   tmp1, vll, vuu;

  
  //  -- LAPACK driver routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DSPEVX computes selected eigenvalues and, optionally, eigenvectors
  //  of a real symmetric matrix A in packed storage by calling the
  //  recommended sequence of LAPACK routines.  Eigenvalues/vectors can be
  //  selected by specifying either a range of values or a range of indices
  //  for the desired eigenvalues.
  
  //  Arguments
  //  =========
  
  //  JOBZ    (input) CHARACTER*1
  //          Specifies whether or not to compute the eigenvectors:
  //          = 'N':  Compute eigenvalues only.
  //          = 'V':  Compute eigenvalues and eigenvectors.
  
  //  RANGE   (input) CHARACTER*1
  //          = 'A': all eigenvalues will be found.
  //          = 'V': all eigenvalues in the half-open interval (VL,VU]
  //                 will be found.
  //          = 'I': the IL-th through IU-th eigenvalues will be found.
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangular part of the
  //          symmetric matrix A is stored:
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The number of rows and columns of the matrix A.  N >= 0.
  
  //  AP      (input/output) DOUBLE PRECISION array, dimension (N*(N+1)/2)
  //          On entry, the upper or lower triangle of the symmetric matrix
  //          A, packed columnwise in a linear array.  The j-th column of A
  //          is stored in the array AP as follows:
  //          if UPLO = 'U', AP(i + (j-1)*j/2) = A(i,j) for 1<=i<=j;
  //          if UPLO = 'L', AP(i + (j-1)*(2*n-j)/2) = A(i,j) for j<=i<=n.
  
  //          On exit, AP is overwritten by values generated during the
  //          reduction to tridiagonal form.  If UPLO = 'U', the diagonal
  //          and first superdiagonal of the tridiagonal matrix T overwrite
  //          the corresponding elements of A, and if UPLO = 'L', the
  //          diagonal and first subdiagonal of T overwrite the
  //          corresponding elements of A.
  
  //  VL      (input) DOUBLE PRECISION
  //          If RANGE='V', the lower bound of the interval to be searched
  //          for eigenvalues.  Not referenced if RANGE = 'A' or 'I'.
  
  //  VU      (input) DOUBLE PRECISION
  //          If RANGE='V', the upper bound of the interval to be searched
  //          for eigenvalues.  Not referenced if RANGE = 'A' or 'I'.
  
  //  IL      (input) INTEGER
  //          If RANGE='I', the index (from smallest to largest) of the
  //          smallest eigenvalue to be returned.  IL >= 1.
  //          Not referenced if RANGE = 'A' or 'V'.
  
  //  IU      (input) INTEGER
  //          If RANGE='I', the index (from smallest to largest) of the
  //          largest eigenvalue to be returned.  min(IL,N) <= IU <= N.
  //          Not referenced if RANGE = 'A' or 'V'.
  
  //  ABSTOL  (input) DOUBLE PRECISION
  //          The absolute error tolerance for the eigenvalues.
  //          An approximate eigenvalue is accepted as converged
  //          when it is determined to lie in an interval [a,b]
  //          of width less than or equal to
  
  //                  ABSTOL + EPS *   max( |a|,|b| ) ,
  
  //          where EPS is the machine precision.  If ABSTOL is less than
  //          or equal to zero, then  EPS*|T|  will be used in its place,
  //          where |T| is the 1-norm of the tridiagonal matrix obtained
  //          by reducing AP to tridiagonal form.  For most problems,
  //          this is the appropriate level of accuracy to request.
  //          For certain strongly graded matrices, greater accuracy
  //          can be obtained in very small eigenvalues by setting
  //          ABSTOL to zero or to some very small positive number.
  //          (Note, however, that if ABSTOL is less than sqrt(unfl),
  //          where unfl is the underflow threshold, then sqrt(unfl)
  //          will be used in its place.)
  
  //          See "Computing Small Singular Values of Bidiagonal Matrices
  //          with Guaranteed High Relative Accuracy," by Demmel and
  //          Kahan, LAPACK Working Note #3.
  
  //  M       (output) INTEGER
  //          Total number of eigenvalues found.  0 <= M <= N.
  
  //  W       (output) DOUBLE PRECISION array, dimension (N)
  //          On normal exit, the first M entries contain the selected
  //          eigenvalues in ascending order.
  
  //  Z       (output) DOUBLE PRECISION array, dimension (LDZ, N)
  //          If JOBZ = 'V', then on normal exit the first M columns of Z
  //          contain the orthonormal eigenvectors of the matrix
  //          corresponding to the selected eigenvalues.  If an eigenvector
  //          fails to converge, then that column of Z contains the latest
  //          approximation to the eigenvector, and the index of the
  //          eigenvector is returned in IFAIL.
  //          If JOBZ = 'N', then Z is not referenced.
  
  //  LDZ     (input) INTEGER
  //          The leading dimension of the array Z.  LDZ >= 1, and if
  //          JOBZ = 'V', LDZ >= max(1,N).
  
  //  WORK    (workspace) DOUBLE PRECISION array, dimension (7*N)
  
  //  IWORK   (workspace) INTEGER array, dimension (5*N)
  
  //  IFAIL   (output) INTEGER array, dimension (N)
  //          If JOBZ = 'V', then on normal exit, the first M elements of
  //          IFAIL are zero.  If INFO > 0 on exit, then IFAIL contains the
  //          indices of the eigenvectors that failed to converge.
  //          If JOBZ = 'N', then IFAIL is not referenced.
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit.
  //          < 0:  if INFO = -i, the i-th argument had an illegal value.
  //          > 0:  if INFO = +i, then i eigenvectors failed to converge.
  //                Their indices are stored in array IFAIL.
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  wantz = lsame( jobz, 'V' );
  alleig = lsame( range, 'A' );
  valeig = lsame( range, 'V' );
  indeig = lsame( range, 'I' );
  
  info = 0;
  if( !(wantz || lsame( jobz, 'N' )) ) { 
    info = -1;
  }
  else if( !((alleig || valeig) || indeig) ) { 
    info = -2;
  }
  else if( !(lsame( uplo, 'L' ) || lsame( uplo, 'U' )) ) { 
    info = -3;
  }
  else if( n < 0 ) { 
    info = -4;
  }
  else if( (valeig && n > 0) && vu <= vl ) { 
    info = -7;
  }
  else if( indeig && il < 1 ) { 
    info = -8;
  }
  else if( indeig && (iu < min( n, il ) || iu > n) ) { 
    info = -9;
  }
  else if( ldz < 1 || (wantz && ldz < n) ) { 
    info = -14;
  }
  
  if( info != 0 ) { 
    xerbla( "DSPEVX", -info );
    return;
  }
  
  //     Quick return if possible
  
  m = 0;
  if( n == 0 ) 
    return;
  
  if( n == 1 ) { 
    if( alleig || indeig ) { 
      m = 1;
      w[0] = ap[0];
    }
    else { 
      if( vl < ap[0] && vu >= ap[0] ) { 
        m = 1;
        w[0] = ap[0];
      }
    }
    if( wantz ) 
      Z(0,0) = ONE;
    return;
  }
  
  //     Get machine constants.
  
  safmin = dlamch( 'S'/* Safe minimum */ );
  eps = dlamch( 'P'/* Precision */ );
  smlnum = safmin/eps;
  bignum = ONE/smlnum;
  rmin = sqrt( smlnum );
  rmax = min( sqrt( bignum ), ONE/sqrt( sqrt( safmin ) ) );
  
  //     Scale matrix to allowable range, if necessary.
  
  iscale = 0;
  abstll = abstol;
  if( valeig ) { 
    vll = vl;
    vuu = vu;
  }
  anrm = dlansp( 'M', uplo, n, ap, work );
  if( anrm > ZERO && anrm < rmin ) { 
    iscale = 1;
    sigma = rmin/anrm;
  }
  else if( anrm > rmax ) { 
    iscale = 1;
    sigma = rmax/anrm;
  }
  if( iscale == 1 ) { 
    dscal( (n*(n + 1))/2, sigma, ap, 1 );
    if( abstol > 0 ) 
      abstll = abstol*sigma;
    if( valeig ) { 
      vll = vl*sigma;
      vuu = vu*sigma;
    }
  }
  
  //     Call DSPTRD to reduce symmetric packed matrix to tridiagonal form.
  
  indtau = 1;
  inde = indtau + n;
  indd = inde + n;
  dsptrd( uplo, n, ap, &work[indd - 1], &work[inde - 1], &work[indtau - 1], 
   iinfo );
  
  //     If all eigenvalues are desired and ABSTOL is less than or equal
  //     to zero, then call DSTERF or DOPGTR and DSTEQR.  If this fails
  //     for some eigenvalue, then try DSTEBZ.
  
  if( (alleig || ((indeig && il == 1) && iu == n)) && (abstol <= 
   ZERO) ) { 
    dcopy( n, &work[indd - 1], 1, w, 1 );
    if( !wantz ) { 
      dsterf( n, w, &work[inde - 1], info );
    }
    else { 
      indwrk = indd + n;
      dopgtr( uplo, n, ap, &work[indtau - 1], z, ldz, &work[indwrk - 1], 
       iinfo );
      dsteqr( jobz, n, w, &work[inde - 1], z, ldz, &work[indwrk - 1], 
       info );
      if( info == 0 ) { 
        for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
          ifail[i_] = 0;
        }
      }
    }
    if( info == 0 ) { 
      m = n;
      goto L_20;
    }
    info = 0;
  }
  
  //     Otherwise, call DSTEBZ and, if eigenvectors are desired, DSTEIN.
  
  if( wantz ) { 
    order = 'B';
  }
  else { 
    order = 'E';
  }
  indwrk = indd + n;
  indibl = 1;
  indisp = indibl + n;
  indiwo = indisp + n;
  dstebz( range, order, n, vll, vuu, il, iu, abstll, &work[indd - 1], 
   &work[inde - 1], m, nsplit, w, &iwork[indibl - 1], &iwork[indisp - 1], 
   &work[indwrk - 1], &iwork[indiwo - 1], info );
  
  if( wantz ) { 
    dstein( n, &work[indd - 1], &work[inde - 1], m, w, &iwork[indibl - 1], 
     &iwork[indisp - 1], z, ldz, &work[indwrk - 1], &iwork[indiwo - 1], 
     ifail, info );
    
    //        Apply orthogonal matrix used in reduction to tridiagonal
    //        form to eigenvectors returned by DSTEIN.
    
    dopmtr( 'L', uplo, 'N', n, m, ap, &work[indtau - 1], z, ldz, 
     &work[indwrk - 1], info );
  }
  
  //     If matrix was scaled, then rescale eigenvalues appropriately.
  
L_20:
  ;
  if( iscale == 1 ) { 
    if( info == 0 ) { 
      imax = m;
    }
    else { 
      imax = info - 1;
    }
    dscal( imax, ONE/sigma, w, 1 );
  }
  
  //     If eigenvalues are not in order, then sort them, along with
  //     eigenvectors.
  
  if( wantz ) { 
    for( j = 1, j_ = j - 1, _do1 = m - 1; j <= _do1; j++, j_++ ) { 
      i = 0;
      tmp1 = w[j_];
      for( jj = j + 1, jj_ = jj - 1, _do2 = m; jj <= _do2; jj++, jj_++ ) { 
        if( w[jj_] < tmp1 ) { 
          i = jj;
          tmp1 = w[jj_];
        }
      }
      
      if( i != 0 ) { 
        itmp1 = iwork[indibl + i - 2];
        w[i - 1] = w[j_];
        iwork[indibl + i - 2] = iwork[indibl + j_ - 1];
        w[j_] = tmp1;
        iwork[indibl + j_ - 1] = itmp1;
        dswap( n, &Z(i - 1,0), 1, &Z(j_,0), 1 );
        if( info != 0 ) { 
          itmp1 = ifail[i - 1];
          ifail[i - 1] = ifail[j_];
          ifail[j_] = itmp1;
        }
      }
    }
  }
  
  return;
  
  //     End of DSPEVX
  
#undef  Z
} // end of function 


/*
 * C++ implementation of Lapack routine zlacpy
 *
 * $Id: zlacpy.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:48:10
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zlacpy.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

RWLAPKDECL void /*FUNCTION*/ zlacpy(const char &uplo, const long &m, const long &n, DComplex *a, 
   const long &lda, DComplex *b, const long &ldb)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
  long _do0, _do1, _do2, _do3, _do4, _do5, i, i_, j, j_;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZLACPY copies all or part of a two-dimensional matrix A to another
  //  matrix B.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies the part of the matrix A to be copied to B.
  //          = 'U':      Upper triangular part
  //          = 'L':      Lower triangular part
  //          Otherwise:  All of the matrix A
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix A.  M >= 0.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix A.  N >= 0.
  
  //  A       (input) COMPLEX*16 array, dimension (LDA,N)
  //          The m by n matrix A.  If UPLO = 'U', only the upper trapezium
  //          is accessed; if UPLO = 'L', only the lower trapezium is
  //          accessed.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,M).
  
  //  B       (output) COMPLEX*16 array, dimension (LDB,N)
  //          On exit, B = A in the locations specified by UPLO.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,M).
  
  //  =====================================================================
  
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  if( lsame( uplo, 'U' ) ) { 
    for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
      for( i = 1, i_ = i - 1, _do1 = min( j, m ); i <= _do1; i++, i_++ ) { 
        B(j_,i_) = A(j_,i_);
      }
    }
    
  }
  else if( lsame( uplo, 'L' ) ) { 
    for( j = 1, j_ = j - 1, _do2 = n; j <= _do2; j++, j_++ ) { 
      for( i = j, i_ = i - 1, _do3 = m; i <= _do3; i++, i_++ ) { 
        B(j_,i_) = A(j_,i_);
      }
    }
    
  }
  else { 
    for( j = 1, j_ = j - 1, _do4 = n; j <= _do4; j++, j_++ ) { 
      for( i = 1, i_ = i - 1, _do5 = m; i <= _do5; i++, i_++ ) { 
        B(j_,i_) = A(j_,i_);
      }
    }
  }
  
  return;
  
  //     End of ZLACPY
  
#undef  B
#undef  A
} // end of function 


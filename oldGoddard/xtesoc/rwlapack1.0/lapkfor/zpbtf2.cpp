/*
 * C++ implementation of Lapack routine zpbtf2
 *
 * $Id: zpbtf2.cpp,v 1.2 1993/07/21 22:22:14 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:49:46
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zpbtf2.cpp,v $
 * Revision 1.2  1993/07/21  22:22:14  alv
 * ported to Microsoft visual C++
 *
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ONE = 1.0e0;
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zpbtf2(const char &uplo, const long &n, const long &kd, DComplex *ab, 
 const long &ldab, long &info)
{
#define AB(I_,J_) (*(ab+(I_)*(ldab)+(J_)))
  int upper;
  long _do0, _do1, j, j_, kld, kn;
  double ajj;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZPBTF2 computes the Cholesky factorization of a DComplex Hermitian
  //  positive definite band matrix A.
  
  //  The factorization has the form
  //     A = U' * U ,  if UPLO = 'U', or
  //     A = L  * L',  if UPLO = 'L',
  //  where U is an upper triangular matrix, U' is the conjugate transpose
  //  of U, and L is lower triangular.
  
  //  This is the unblocked version of the algorithm, calling Level 2 BLAS.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangular part of the
  //          Hermitian matrix A is stored:
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  KD      (input) INTEGER
  //          The number of super-diagonals of the matrix A if UPLO = 'U',
  //          or the number of sub-diagonals if UPLO = 'L'.  KD >= 0.
  
  //  AB      (input/output) COMPLEX*16 array, dimension (LDAB,N)
  //          On entry, the upper or lower triangle of the Hermitian band
  //          matrix A, stored in the first KD+1 rows of the array.  The
  //          j-th column of A is stored in the j-th column of the array AB
  //          as follows:
  //          if UPLO = 'U', AB(kd+1+i-j,j) = A(i,j) for max(1,j-kd)<=i<=j;
  //          if UPLO = 'L', AB(1+i-j,j)    = A(i,j) for j<=i<=min(n,j+kd).
  
  //          On exit, if INFO = 0, the triangular factor U or L from the
  //          Cholesky factorization A = U'*U or A = L*L' of the band
  //          matrix A, in the same storage format as A.
  
  //  LDAB    (input) INTEGER
  //          The leading dimension of the array AB.  LDAB >= KD+1.
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: if INFO = k, the leading minor of order k is not
  //               positive definite, and the factorization could not be
  //               completed.
  
  //  Further Details
  //  ===============
  
  //  The band storage scheme is illustrated by the following example, when
  //  N = 6, KD = 2, and UPLO = 'U':
  
  //  On entry:                       On exit:
  
  //      *    *   a13  a24  a35  a46      *    *   u13  u24  u35  u46
  //      *   a12  a23  a34  a45  a56      *   u12  u23  u34  u45  u56
  //     a11  a22  a33  a44  a55  a66     u11  u22  u33  u44  u55  u66
  
  //  Similarly, if UPLO = 'L' the format of A is as follows:
  
  //  On entry:                       On exit:
  
  //     a11  a22  a33  a44  a55  a66     l11  l22  l33  l44  l55  l66
  //     a21  a32  a43  a54  a65   *      l21  l32  l43  l54  l65   *
  //     a31  a42  a53  a64   *    *      l31  l42  l53  l64   *    *
  
  //  Array elements marked * are not used by the routine.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( kd < 0 ) { 
    info = -3;
  }
  else if( ldab < kd + 1 ) { 
    info = -5;
  }
  if( info != 0 ) { 
    xerbla( "ZPBTF2", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  kld = max( 1, ldab - 1 );
  
  if( upper ) { 
    
    //        Compute the Cholesky factorization A = U'*U.
    
    for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
      
      //           Compute U(J,J) and test for non-positive-definiteness.
      
      ajj = real( AB(j_,kd) );
      if( ajj <= ZERO ) { 
        AB(j_,kd) = (DComplex)(ajj);
        goto L_30;
      }
      ajj = sqrt( ajj );
      AB(j_,kd) = (DComplex)(ajj);
      
      //           Compute elements J+1:J+KN of row J and update the
      //           trailing submatrix within the band.
      
      kn = min( kd, n - j );
      if( kn > 0 ) { 
        zdscal( kn, ONE/ajj, &AB(j_ + 1,kd - 1), kld );
        zlacgv( kn, &AB(j_ + 1,kd - 1), kld );
        zher( 'U'/*Upper*/, kn, -ONE, &AB(j_ + 1,kd - 1), 
         kld, &AB(j_ + 1,kd), kld );
        zlacgv( kn, &AB(j_ + 1,kd - 1), kld );
      }
    }
  }
  else { 
    
    //        Compute the Cholesky factorization A = L*L'.
    
    for( j = 1, j_ = j - 1, _do1 = n; j <= _do1; j++, j_++ ) { 
      
      //           Compute L(J,J) and test for non-positive-definiteness.
      
      ajj = real( AB(j_,0) );
      if( ajj <= ZERO ) { 
        AB(j_,0) = (DComplex)(ajj);
        goto L_30;
      }
      ajj = sqrt( ajj );
      AB(j_,0) = (DComplex)(ajj);
      
      //           Compute elements J+1:J+KN of column J and update the
      //           trailing submatrix within the band.
      
      kn = min( kd, n - j );
      if( kn > 0 ) { 
        zdscal( kn, ONE/ajj, &AB(j_,1), 1 );
        zher( 'L'/*Lower*/, kn, -ONE, &AB(j_,1), 1, &AB(j_ + 1,0), 
         kld );
      }
    }
  }
  return;
  
L_30:
  ;
  info = j;
  return;
  
  //     End of ZPBTF2
  
#undef  AB
} // end of function 


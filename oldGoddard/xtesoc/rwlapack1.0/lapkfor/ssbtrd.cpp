/*
 * C++ implementation of Lapack routine ssbtrd
 *
 * $Id: ssbtrd.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:02:21
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: ssbtrd.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ZERO = 0.0e0;
const float ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ ssbtrd(const char &vect, const char &uplo, const long &n, const long &kd, 
 float *ab, const long &ldab, float d[], float e[], float *q, const long &ldq, 
 float work[], long &info)
{
#define AB(I_,J_) (*(ab+(I_)*(ldab)+(J_)))
#define Q(I_,J_)  (*(q+(I_)*(ldq)+(J_)))
  int upper, wantq;
  long _do0, _do1, _do10, _do11, _do12, _do13, _do14, _do15, 
   _do16, _do17, _do18, _do19, _do2, _do3, _do4, _do5, _do6, _do7, 
   _do8, _do9, i, i_, j, j1, j2, j_, k, k_, kd1, kdn, l, l_, nr, 
   nrt;
  float temp;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SSBTRD reduces a real symmetric band matrix A to symmetric
  //  tridiagonal form T by an orthogonal similarity transformation:
  //  Q' * A * Q = T.
  
  //  Arguments
  //  =========
  
  //  VECT    (input) CHARACTER*1
  //          Specifies whether or not the matrix Q is to be formed.
  //          = 'N': do not form Q
  //          = 'V': form Q
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangular part of the
  //          symmetric matrix A is stored:
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  KD      (input) INTEGER
  //          The number of super-diagonals of the matrix A if UPLO = 'U',
  //          or the number of sub-diagonals if UPLO = 'L'.  KD >= 0.
  
  //  AB      (input/output) REAL array, dimension (LDAB,N)
  //          On entry, the upper or lower triangle of the symmetric band
  //          matrix A, stored in the first KD+1 rows of the array.  The
  //          j-th column of A is stored in the j-th column of the array AB
  //          as follows:
  //          if UPLO = 'U', AB(kd+1+i-j,j) = A(i,j) for max(1,j-kd)<=i<=j;
  //          if UPLO = 'L', AB(1+i-j,j)    = A(i,j) for j<=i<=min(n,j+kd).
  //          On exit, the diagonal elements of A are overwritten by the
  //          diagonal elements of the tridiagonal matrix T; if KD > 0, the
  //          elements on the first superdiagonal (if UPLO = 'U') or the
  //          first subdiagonal (if UPLO = 'L') are overwritten by the
  //          offdiagonal elements of T; the rest of A is overwritten by
  //          values generated during the reduction.
  
  //  LDAB    (input) INTEGER
  //          The leading dimension of the array AB.  LDAB >= KD+1.
  
  //  D       (output) REAL array, dimension (N)
  //          The diagonal elements of the tridiagonal matrix T.
  
  //  E       (output) REAL array, dimension (N-1)
  //          The off-diagonal elements of the tridiagonal matrix T:
  //          E(i) = T(i,i+1) if UPLO = 'U'; E(i) = T(i+1,i) if UPLO = 'L'.
  
  //  Q       (output) REAL array, dimension (LDQ,N)
  //          If VECT = 'V', the n by n orthogonal matrix Q.
  //          If VECT = 'N', the array Q is not referenced.
  
  //  LDQ     (input) INTEGER
  //          The leading dimension of the array Q.
  //          LDQ >= max(1,N) if VECT = 'V'.
  
  //  WORK    (workspace) REAL array, dimension (N)
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit 
  //          < 0:  if INFO = -i, the i-th argument had an illegal value.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters
  
  wantq = lsame( vect, 'V' );
  upper = lsame( uplo, 'U' );
  kd1 = kd + 1;
  info = 0;
  if( !wantq && !lsame( vect, 'N' ) ) { 
    info = -1;
  }
  else if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -2;
  }
  else if( n < 0 ) { 
    info = -3;
  }
  else if( kd < 0 ) { 
    info = -4;
  }
  else if( ldab < kd1 ) { 
    info = -6;
  }
  else if( ldq < max( 1, n ) && wantq ) { 
    info = -10;
  }
  if( info != 0 ) { 
    xerbla( "SSBTRD", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  //     Initialize Q to the unit matrix, if needed
  
  if( wantq ) 
    slazro( n, n, ZERO, ONE, q, ldq );
  
  //     Wherever possible, plane rotations are generated and applied in
  //     vector operations of length NR over the index set J1:J2:KD1.
  
  //     The cosines and sines of the plane rotations are stored in the
  //     arrays D and WORK.
  
  kdn = min( n - 1, kd );
  if( upper ) { 
    
    if( kd > 1 ) { 
      
      //           Reduce to tridiagonal form, working with upper triangle
      
      nr = 0;
      j1 = kdn + 2;
      j2 = 1;
      
      for( i = 1, i_ = i - 1, _do0 = n - 2; i <= _do0; i++, i_++ ) { 
        
        //              Reduce i-th row of matrix to tridiagonal form
        
        for( k = kdn + 1, k_ = k - 1; k >= 2; k--, k_-- ) { 
          j1 = j1 + kdn;
          j2 = j2 + kdn;
          
          if( nr > 0 ) { 
            
            //                    generate plane rotations to annihilate nonzero
            //                    elements which have been created outside the band
            
            slargv( nr, &AB(j1 - 2,0), kd1*ldab, &work[j1 - 1], 
             kd1, &d[j1 - 1], kd1 );
            
            //                    apply rotations from the right
            
            for( l = 1, l_ = l - 1, _do1 = kd - 1; l <= _do1; l++, l_++ ) { 
              slartv( nr, &AB(j1 - 2,l_ + 1), kd1*ldab, 
               &AB(j1 - 1,l_), kd1*ldab, &d[j1 - 1], 
               &work[j1 - 1], kd1 );
            }
          }
          
          if( k > 2 ) { 
            if( k <= n - i + 1 ) { 
              
              //                       generate plane rotation to annihilate a(i,i+k-1)
              //                       within the band
              
              slartg( AB(i + k_ - 2,kd - k + 2), AB(i + k_ - 1,kd - k + 1), 
               d[i + k_ - 1], work[i + k_ - 1], temp );
              AB(i + k_ - 2,kd - k + 2) = temp;
              
              //                       apply rotation from the right
              
              srot( k - 3, &AB(i + k_ - 2,kd - k + 3), 
               1, &AB(i + k_ - 1,kd - k + 2), 1, d[i + k_ - 1], 
               work[i + k_ - 1] );
            }
            nr = nr + 1;
            j1 = j1 - kdn - 1;
          }
          
          //                 apply plane rotations from both sides to diagonal
          //                 blocks
          
          if( nr > 0 ) 
            slar2v( nr, &AB(j1 - 2,kd1 - 1), &AB(j1 - 1,kd1 - 1), 
             &AB(j1 - 1,kd - 1), kd1*ldab, &d[j1 - 1], 
             &work[j1 - 1], kd1 );
          
          //                 apply plane rotations from the left
          
          for( l = 1, l_ = l - 1, _do2 = kd - 1; l <= _do2; l++, l_++ ) { 
            if( j2 + l > n ) { 
              nrt = nr - 1;
            }
            else { 
              nrt = nr;
            }
            if( nrt > 0 ) 
              slartv( nrt, &AB(j1 + l_,kd - l - 1), 
               kd1*ldab, &AB(j1 + l_,kd - l), kd1*ldab, 
               &d[j1 - 1], &work[j1 - 1], kd1 );
          }
          
          if( wantq ) { 
            
            //                    accumulate product of plane rotations in Q
            
            for( j = j1, j_ = j - 1, _do3=docnt(j,j2,_do4 = kd1); _do3 > 0; j += _do4, j_ += _do4, _do3-- ) { 
              srot( n, &Q(j_ - 1,0), 1, &Q(j_,0), 1, 
               d[j_], work[j_] );
            }
          }
          
          if( j2 + kdn > n ) { 
            
            //                    adjust J2 to keep within the bounds of the matrix
            
            nr = nr - 1;
            j2 = j2 - kdn - 1;
          }
          
          for( j = j1, j_ = j - 1, _do5=docnt(j,j2,_do6 = kd1); _do5 > 0; j += _do6, j_ += _do6, _do5-- ) { 
            
            //                    create nonzero element a(j-1,j+kd) outside the band
            //                    and store it in WORK
            
            work[j_ + kd] = work[j_]*AB(j_ + kd,0);
            AB(j_ + kd,0) = d[j_]*AB(j_ + kd,0);
          }
        }
      }
    }
    
    if( kd > 0 ) { 
      
      //           copy off-diagonal elements to E
      
      for( i = 1, i_ = i - 1, _do7 = n - 1; i <= _do7; i++, i_++ ) { 
        e[i_] = AB(i_ + 1,kd - 1);
      }
    }
    else { 
      
      //           set E to zero if original matrix was diagonal
      
      for( i = 1, i_ = i - 1, _do8 = n - 1; i <= _do8; i++, i_++ ) { 
        e[i_] = ZERO;
      }
    }
    
    //        copy diagonal elements to D
    
    for( i = 1, i_ = i - 1, _do9 = n; i <= _do9; i++, i_++ ) { 
      d[i_] = AB(i_,kd1 - 1);
    }
    
  }
  else { 
    
    if( kd > 1 ) { 
      
      //           Reduce to tridiagonal form, working with lower triangle
      
      nr = 0;
      j1 = kdn + 2;
      j2 = 1;
      
      for( i = 1, i_ = i - 1, _do10 = n - 2; i <= _do10; i++, i_++ ) { 
        
        //              Reduce i-th column of matrix to tridiagonal form
        
        for( k = kdn + 1, k_ = k - 1; k >= 2; k--, k_-- ) { 
          j1 = j1 + kdn;
          j2 = j2 + kdn;
          
          if( nr > 0 ) { 
            
            //                    generate plane rotations to annihilate nonzero
            //                    elements which have been created outside the band
            
            slargv( nr, &AB(j1 - kd1 - 1,kd1 - 1), kd1*
             ldab, &work[j1 - 1], kd1, &d[j1 - 1], kd1 );
            
            //                    apply plane rotations from one side
            
            for( l = 1, l_ = l - 1, _do11 = kd - 1; l <= _do11; l++, l_++ ) { 
              slartv( nr, &AB(j1 - kd1 + l_,kd1 - l - 1), 
               kd1*ldab, &AB(j1 - kd1 + l_,kd1 - l), 
               kd1*ldab, &d[j1 - 1], &work[j1 - 1], 
               kd1 );
            }
          }
          
          if( k > 2 ) { 
            if( k <= n - i + 1 ) { 
              
              //                       generate plane rotation to annihilate a(i+k-1,i)
              //                       within the band
              
              slartg( AB(i_,k_ - 1), AB(i_,k_), d[i + k_ - 1], 
               work[i + k_ - 1], temp );
              AB(i_,k_ - 1) = temp;
              
              //                       apply rotation from the left
              
              srot( k - 3, &AB(i_ + 1,k_ - 2), ldab - 
               1, &AB(i_ + 1,k_ - 1), ldab - 1, d[i + k_ - 1], 
               work[i + k_ - 1] );
            }
            nr = nr + 1;
            j1 = j1 - kdn - 1;
          }
          
          //                 apply plane rotations from both sides to diagonal
          //                 blocks
          
          if( nr > 0 ) 
            slar2v( nr, &AB(j1 - 2,0), &AB(j1 - 1,0), 
             &AB(j1 - 2,1), kd1*ldab, &d[j1 - 1], &work[j1 - 1], 
             kd1 );
          
          //                 apply plane rotations from the right
          
          for( l = 1, l_ = l - 1, _do12 = kd - 1; l <= _do12; l++, l_++ ) { 
            if( j2 + l > n ) { 
              nrt = nr - 1;
            }
            else { 
              nrt = nr;
            }
            if( nrt > 0 ) 
              slartv( nrt, &AB(j1 - 2,l_ + 2), kd1*ldab, 
               &AB(j1 - 1,l_ + 1), kd1*ldab, &d[j1 - 1], 
               &work[j1 - 1], kd1 );
          }
          
          if( wantq ) { 
            
            //                    accumulate product of plane rotations in Q
            
            for( j = j1, j_ = j - 1, _do13=docnt(j,j2,_do14 = kd1); _do13 > 0; j += _do14, j_ += _do14, _do13-- ) { 
              srot( n, &Q(j_ - 1,0), 1, &Q(j_,0), 1, 
               d[j_], work[j_] );
            }
          }
          
          if( j2 + kdn > n ) { 
            
            //                    adjust J2 to keep within the bounds of the matrix
            
            nr = nr - 1;
            j2 = j2 - kdn - 1;
          }
          
          for( j = j1, j_ = j - 1, _do15=docnt(j,j2,_do16 = kd1); _do15 > 0; j += _do16, j_ += _do16, _do15-- ) { 
            
            //                    create nonzero element a(j+kd,j-1) outside the
            //                    band and store it in WORK
            
            work[j_ + kd] = work[j_]*AB(j_,kd1 - 1);
            AB(j_,kd1 - 1) = d[j_]*AB(j_,kd1 - 1);
          }
        }
      }
    }
    
    if( kd > 0 ) { 
      
      //           copy off-diagonal elements to E
      
      for( i = 1, i_ = i - 1, _do17 = n - 1; i <= _do17; i++, i_++ ) { 
        e[i_] = AB(i_,1);
      }
    }
    else { 
      
      //           set E to zero if original matrix was diagonal
      
      for( i = 1, i_ = i - 1, _do18 = n - 1; i <= _do18; i++, i_++ ) { 
        e[i_] = ZERO;
      }
    }
    
    //        copy diagonal elements to D
    
    for( i = 1, i_ = i - 1, _do19 = n; i <= _do19; i++, i_++ ) { 
      d[i_] = AB(i_,0);
    }
  }
  
  return;
  
  //     End of SSBTRD
  
#undef  Q
#undef  AB
} // end of function 


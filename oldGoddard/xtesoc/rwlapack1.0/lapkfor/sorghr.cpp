/*
 * C++ implementation of Lapack routine sorghr
 *
 * $Id: sorghr.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:01:09
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: sorghr.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ZERO = 0.0e0;
const float ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ sorghr(const long &n, const long &ilo, const long &ihi, 
 float *a, const long &lda, float tau[], float work[], const long &lwork, 
 long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  long _do0, _do1, _do2, _do3, _do4, _do5, _do6, _do7, i, i_, 
   iinfo, j, j_, nh;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SORGHR generates a real orthogonal matrix Q which is defined as the
  //  product of ihi-ilo elementary reflectors of order n, as returned by
  //  SGEHRD:
  
  //  Q = H(ilo) H(ilo+1) . . . H(ihi-1).
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The order of the matrix Q. N >= 0.
  
  //  ILO     (input) INTEGER
  //  IHI     (input) INTEGER
  //          ILO and IHI must have the same values as in the previous call
  //          of SGEHRD. Q is equal to the unit matrix except in the
  //          submatrix Q(ilo+1:ihi,ilo+1:ihi).  If N > 0,
  //          1 <= ILO <= IHI <= N; otherwise ILO = 1 and IHI = N.
  
  //  A       (input/output) REAL array, dimension (LDA,N)
  //          On entry, the vectors which define the elementary reflectors,
  //          as returned by SGEHRD.
  //          On exit, the n by n orthogonal matrix Q.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A. LDA >= max(1,N).
  
  //  TAU     (input) REAL array, dimension (N-1)
  //          TAU(i) must contain the scalar factor of the elementary
  //          reflector H(i), as returned by SGEHRD.
  
  //  WORK    (workspace) REAL array, dimension (LWORK)
  //          On exit, if INFO = 0, WORK(1) returns the minimum value of
  //          LWORK required to use the optimal blocksize.
  
  //  LWORK   (input) INTEGER
  //          The dimension of the array WORK. LWORK >= IHI-ILO.
  //          For optimum performance LWORK should be at least
  //          (IHI-ILO)*NB, where NB is the optimal blocksize.
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input arguments
  
  info = 0;
  if( n < 0 ) { 
    info = -1;
  }
  else if( ilo < 1 ) { 
    info = -2;
  }
  else if( ihi < min( ilo, n ) || ihi > n ) { 
    info = -3;
  }
  else if( lda < max( 1, n ) ) { 
    info = -5;
  }
  else if( lwork < max( 1, ihi - ilo ) ) { 
    info = -8;
  }
  if( info != 0 ) { 
    xerbla( "SORGHR", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) { 
    work[0] = 1;
    return;
  }
  
  //     Shift the vectors which define the elementary reflectors one
  //     column to the right, and set the first ilo and the last n-ihi
  //     rows and columns to those of the unit matrix
  
  for( j = ihi, j_ = j - 1, _do0 = ilo + 1; j >= _do0; j--, j_-- ) { 
    for( i = 1, i_ = i - 1, _do1 = j - 1; i <= _do1; i++, i_++ ) { 
      A(j_,i_) = ZERO;
    }
    for( i = j + 1, i_ = i - 1, _do2 = ihi; i <= _do2; i++, i_++ ) { 
      A(j_,i_) = A(j_ - 1,i_);
    }
    for( i = ihi + 1, i_ = i - 1, _do3 = n; i <= _do3; i++, i_++ ) { 
      A(j_,i_) = ZERO;
    }
  }
  for( j = 1, j_ = j - 1, _do4 = ilo; j <= _do4; j++, j_++ ) { 
    for( i = 1, i_ = i - 1, _do5 = n; i <= _do5; i++, i_++ ) { 
      A(j_,i_) = ZERO;
    }
    A(j_,j_) = ONE;
  }
  for( j = ihi + 1, j_ = j - 1, _do6 = n; j <= _do6; j++, j_++ ) { 
    for( i = 1, i_ = i - 1, _do7 = n; i <= _do7; i++, i_++ ) { 
      A(j_,i_) = ZERO;
    }
    A(j_,j_) = ONE;
  }
  
  nh = ihi - ilo;
  if( nh > 0 ) { 
    
    //        Generate Q(ilo+1:ihi,ilo+1:ihi)
    
    sorgqr( nh, nh, nh, &A(ilo,ilo), lda, &tau[ilo - 1], work, 
     lwork, iinfo );
  }
  return;
  
  //     End of SORGHR
  
#undef  A
} // end of function 


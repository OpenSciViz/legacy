/*
 * C++ implementation of Lapack routine zlacon
 *
 * $Id: zlacon.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:48:09
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zlacon.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const long ITMAX = 5;
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
const double TWO = 2.0e0;
const DComplex CZERO = DComplex(0.0e0,0.0e0);
const DComplex CONE = DComplex(1.0e0,0.0e0);
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zlacon(const long &n, DComplex v[], DComplex x[], double &est, 
 long &kase)
{
  static long _do0, _do1, _do2, _do3, _do4, i, i_, iter, j, 
   jlast, jump;
  static double altsgn, estold, temp;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZLACON estimates the 1-norm of a square, DComplex matrix A.
  //  Reverse communication is used for evaluating matrix-vector products.
  
  //  Arguments
  //  =========
  
  //  N      (input) INTEGER
  //         The order of the matrix.  N >= 1.
  
  //  V      (workspace) COMPLEX*16 array, dimension (N)
  //         On the final return, V = A*W,  where  EST = norm(V)/norm(W)
  //         (W is not returned).
  
  //  X      (input/output) COMPLEX*16 array, dimension (N)
  //         On an intermediate return, X should be overwritten by
  //               A * X,   if KASE=1,
  //               A' * X,  if KASE=2,
  //         where A' is the conjugate transpose of A, and ZLACON must be
  //         re-called with all the other parameters unchanged.
  
  //  EST    (output) DOUBLE PRECISION
  //         An estimate (a lower bound) for norm(A).
  
  //  KASE   (input/output) INTEGER
  //         On the initial call to ZLACON, KASE should be 0.
  //         On an intermediate return, KASE will be 1 or 2, indicating
  //         whether X should be overwritten by A * X  or A' * X.
  //         On the final return from ZLACON, KASE will again be 0.
  
  //  Further Details
  //  ======= =======
  
  //  Contributed by Nick Higham, University of Manchester.
  //  Originally named CONEST, dated March 16, 1988.
  
  //  Reference: N.J. Higham, "FORTRAN codes for estimating the one-norm of
  //  a real or DComplex matrix, with applications to condition estimation",
  //  ACM Trans. Math. Soft., vol. 14, no. 4, pp. 381-396, December 1988.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Save statement ..
  //     ..
  //     .. Executable Statements ..
  
  if( kase == 0 ) { 
    for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
      x[i_] = DComplex( ONE/(double)( n ), 0. );
    }
    kase = 1;
    jump = 1;
    return;
  }
  
  switch( jump ) { 
    case 1: goto L_20;
    case 2: goto L_40;
    case 3: goto L_70;
    case 4: goto L_90;
    case 5: goto L_120;
  }
  
  //     ................ ENTRY   (JUMP = 1)
  //     FIRST ITERATION.  X HAS BEEN OVERWRITTEN BY A*X.
  
L_20:
  ;
  if( n == 1 ) { 
    v[0] = x[0];
    est = abs( v[0] );
    //        ... QUIT
    goto L_130;
  }
  est = dzsum1( n, x, 1 );
  
  for( i = 1, i_ = i - 1, _do1 = n; i <= _do1; i++, i_++ ) { 
    if( abs( x[i_] ) != ZERO ) { 
      x[i_] = x[i_]/DComplex( abs( x[i_] ), 0. );
    }
    else { 
      x[i_] = CONE;
    }
  }
  kase = 2;
  jump = 2;
  return;
  
  //     ................ ENTRY   (JUMP = 2)
  //     FIRST ITERATION.  X HAS BEEN OVERWRITTEN BY ZTRANS(A)*X.
  
L_40:
  ;
  j = izmax1( n, x, 1 );
  iter = 2;
  
  //     MAIN LOOP - ITERATIONS 2,3,...,ITMAX.
  
L_50:
  ;
  for( i = 1, i_ = i - 1, _do2 = n; i <= _do2; i++, i_++ ) { 
    x[i_] = CZERO;
  }
  x[j - 1] = CONE;
  kase = 1;
  jump = 3;
  return;
  
  //     ................ ENTRY   (JUMP = 3)
  //     X HAS BEEN OVERWRITTEN BY A*X.
  
L_70:
  ;
  zcopy( n, x, 1, v, 1 );
  estold = est;
  est = dzsum1( n, v, 1 );
  
  //     TEST FOR CYCLING.
  if( est <= estold ) 
    goto L_100;
  
  for( i = 1, i_ = i - 1, _do3 = n; i <= _do3; i++, i_++ ) { 
    if( abs( x[i_] ) != ZERO ) { 
      x[i_] = x[i_]/DComplex( abs( x[i_] ), 0. );
    }
    else { 
      x[i_] = CONE;
    }
  }
  kase = 2;
  jump = 4;
  return;
  
  //     ................ ENTRY   (JUMP = 4)
  //     X HAS BEEN OVERWRITTEN BY ZTRANS(A)*X.
  
L_90:
  ;
  jlast = j;
  j = izmax1( n, x, 1 );
  if( (real( x[jlast - 1] ) != abs( real( x[j - 1] ) )) && (iter < 
   ITMAX) ) { 
    iter = iter + 1;
    goto L_50;
  }
  
  //     ITERATION COMPLETE.  FINAL STAGE.
  
L_100:
  ;
  altsgn = ONE;
  for( i = 1, i_ = i - 1, _do4 = n; i <= _do4; i++, i_++ ) { 
    x[i_] = DComplex( altsgn*(ONE + (double)( i - 1 )/(double)( n - 
     1 )), 0. );
    altsgn = -altsgn;
  }
  kase = 1;
  jump = 5;
  return;
  
  //     ................ ENTRY   (JUMP = 5)
  //     X HAS BEEN OVERWRITTEN BY A*X.
  
L_120:
  ;
  temp = TWO*(dzsum1( n, x, 1 )/(double)( 3*n ));
  if( temp > est ) { 
    zcopy( n, x, 1, v, 1 );
    est = temp;
  }
  
L_130:
  ;
  kase = 0;
  return;
  
  //     End of ZLACON
  
} // end of function 


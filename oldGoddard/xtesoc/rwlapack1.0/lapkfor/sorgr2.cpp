/*
 * C++ implementation of Lapack routine sorgr2
 *
 * $Id: sorgr2.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:01:16
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: sorgr2.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ONE = 1.0e0;
const float ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ sorgr2(const long &m, const long &n, const long &k, float *a, 
 const long &lda, float tau[], float work[], long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  long _do0, _do1, _do2, _do3, i, i_, ii, j, j_, l, l_;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SORGR2 generates an m by n real matrix Q with orthonormal rows,
  //  which is defined as the last m rows of a product of k elementary
  //  reflectors of order n
  
  //        Q  =  H(1) H(2) . . . H(k)
  
  //  as returned by SGERQF.
  
  //  Arguments
  //  =========
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix Q. M >= 0.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix Q. N >= M.
  
  //  K       (input) INTEGER
  //          The number of elementary reflectors whose product defines the
  //          matrix Q. M >= K >= 0.
  
  //  A       (input/output) REAL array, dimension (LDA,N)
  //          On entry, the (m-k+i)-th row must contain the vector which
  //          defines the elementary reflector H(i), for i = 1,2,...,k, as
  //          returned by SGERQF in the last k rows of its array argument
  //          A.
  //          On exit, the m by n matrix Q.
  
  //  LDA     (input) INTEGER
  //          The first dimension of the array A. LDA >= max(1,M).
  
  //  TAU     (input) REAL array, dimension (K)
  //          TAU(i) must contain the scalar factor of the elementary
  //          reflector H(i), as returned by SGERQF.
  
  //  WORK    (workspace) REAL array, dimension (M)
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument has an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input arguments
  
  info = 0;
  if( m < 0 ) { 
    info = -1;
  }
  else if( n < m ) { 
    info = -2;
  }
  else if( k < 0 || k > m ) { 
    info = -3;
  }
  else if( lda < max( 1, m ) ) { 
    info = -5;
  }
  if( info != 0 ) { 
    xerbla( "SORGR2", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( m <= 0 ) 
    return;
  
  if( k < m ) { 
    
    //        Initialise rows 1:m-k to rows of the unit matrix
    
    for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
      for( l = 1, l_ = l - 1, _do1 = m - k; l <= _do1; l++, l_++ ) { 
        A(j_,l_) = ZERO;
      }
      if( j > n - m && j <= n - k ) 
        A(j_,m - n + j_) = ONE;
    }
  }
  
  for( i = 1, i_ = i - 1, _do2 = k; i <= _do2; i++, i_++ ) { 
    ii = m - k + i;
    
    //        Apply H(i) to A(1:m-k+i,1:n-k+i) from the right
    
    A(n - m + ii - 1,ii - 1) = ONE;
    slarf( 'R'/*Right*/, ii - 1, n - m + ii, &A(0,ii - 1), lda, 
     tau[i_], a, lda, work );
    sscal( n - m + ii - 1, -tau[i_], &A(0,ii - 1), lda );
    A(n - m + ii - 1,ii - 1) = ONE - tau[i_];
    
    //        Set A(m-k+i,n-k+i+1:n) to zero
    
    for( l = n - m + ii + 1, l_ = l - 1, _do3 = n; l <= _do3; l++, l_++ ) { 
      A(l_,ii - 1) = ZERO;
    }
  }
  return;
  
  //     End of SORGR2
  
#undef  A
} // end of function 


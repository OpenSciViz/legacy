/*
 * C++ implementation of Lapack routine zlaesy
 *
 * $Id: zlaesy.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:48:15
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zlaesy.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
const DComplex CONE = DComplex(1.0e0,0.0e0);
const double HALF = 0.5e0;
const double THRESH = 0.1e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zlaesy(const DComplex &a, const DComplex &b, const DComplex &c, DComplex &rt1, 
 DComplex &rt2, DComplex &evscal, DComplex &cs1, DComplex &sn1)
{
  double babs, evnorm, tabs, z;
  DComplex s, t, tmp;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZLAESY computes the eigendecomposition of a 2x2 symmetric matrix
  //     ( ( A, B );( B, C ) )
  //  provided the norm of the matrix of eigenvectors is larger than
  //  some threshold value.
  
  //  RT1 is the eigenvalue of larger absolute value, and RT2 of
  //  smaller absolute value.  If the eigenvectors are computed, then
  //  on return ( CS1, SN1 ) is the unit eigenvector for RT1, hence
  
  //  [  CS1     SN1   ] . [ A  B ] . [ CS1    -SN1   ] = [ RT1  0  ]
  //  [ -SN1     CS1   ]   [ B  C ]   [ SN1     CS1   ]   [  0  RT2 ]
  
  //  Arguments
  //  =========
  
  //  A       (input) COMPLEX*16
  //          The ( 1, 1 ) entry of input matrix.
  
  //  B       (input) COMPLEX*16
  //          The ( 1, 2 ) entry of input matrix.  The ( 2, 1 ) entry is
  //          also given by B, since the 2 x 2 matrix is symmetric.
  
  //  C       (input) COMPLEX*16
  //          The ( 2, 2 ) entry of input matrix.
  
  //  RT1     (output) COMPLEX*16
  //          The eigenvalue of larger modulus.
  
  //  RT2     (output) COMPLEX*16
  //          The eigenvalue of smaller modulus.
  
  //  EVSCAL  (output) COMPLEX*16
  //          The DComplex value by which the eigenvector matrix was scaled
  //          to make it orthonormal.  If EVSCAL is zero, the eigenvectors
  //          were not computed.  This means one of two things:  the 2 x 2
  //          matrix could not be diagonalized, or the norm of the matrix
  //          of eigenvectors before scaling was larger than the threshold
  //          value THRESH (set below).
  
  //  CS1     (output) COMPLEX*16
  //  SN1     (output) COMPLEX*16
  //          If EVSCAL .NE. 0,  ( CS1, SN1 ) is the unit right eigenvector
  //          for RT1.
  
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  
  //     Special case:  The matrix is actually diagonal.
  //     To avoid divide by zero later, we treat this case separately.
  
  if( abs( b ) == ZERO ) { 
    rt1 = a;
    rt2 = c;
    if( abs( rt1 ) < abs( rt2 ) ) { 
      tmp = rt1;
      rt1 = rt2;
      rt2 = tmp;
      cs1 = DComplex(ZERO);
      sn1 = DComplex(ONE);
    }
    else { 
      cs1 = DComplex(ONE);
      sn1 = DComplex(ZERO);
    }
  }
  else { 
    
    //        Compute the eigenvalues and eigenvectors.
    //        The characteristic equation is
    //           lamba **2 - (A+C) lamba + (A*C - B*B)
    //        and we solve it using the quadratic formula.
    
    s = (a + c)*HALF;
    t = (a - c)*HALF;
    
    //        Take the square root carefully to avoid over/under flow.
    
    babs = abs( b );
    tabs = abs( t );
    z = max( babs, tabs );
    if( min( babs, tabs ) > ZERO ) 
      t = z*sqrt( pow(t/z, 2) + pow(b/z, 2) );
    
    //        Compute the two eigenvalues.  RT1 and RT2 are exchanged
    //        if necessary so that RT1 will have the greater magnitude.
    
    rt1 = s + t;
    rt2 = s - t;
    if( abs( rt1 ) < abs( rt2 ) ) { 
      tmp = rt1;
      rt1 = rt2;
      rt2 = tmp;
    }
    
    //        Choose CS1 = 1 and SN1 to satisfy the first equation, then
    //        scale the components of this eigenvector so that the matrix
    //        of eigenvectors X satisfies  X * X' = I .  (No scaling is
    //        done if the norm of the eigenvalue matrix is less than THRESH.)
    
    sn1 = (rt1 - a)/b;
    tabs = abs( sn1 );
    if( tabs > ONE ) { 
      t = tabs*sqrt( pow(ONE/tabs, 2) + pow(sn1/tabs, 2) );
    }
    else { 
      t = sqrt( CONE + sn1*sn1 );
    }
    evnorm = abs( t );
    if( evnorm >= THRESH ) { 
      evscal = CONE/t;
      cs1 = evscal;
      sn1 = sn1*evscal;
    }
    else { 
      evscal = DComplex(ZERO);
    }
  }
  return;
  
  //     End of ZLAESY
  
} // end of function 


/*
 * C++ implementation of Lapack routine sgelq2
 *
 * $Id: sgelq2.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:58:34
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: sgelq2.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ sgelq2(const long &m, const long &n, float *a, const long &lda, 
 float tau[], float work[], long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  long _do0, i, i_, k;
  float aii;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SGELQ2 computes an LQ factorization of a real m by n matrix A:
  //  A = L * Q.
  
  //  Arguments
  //  =========
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix A.  M >= 0.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix A.  N >= 0.
  
  //  A       (input/output) REAL array, dimension (LDA,N)
  //          On entry, the m by n matrix A.
  //          On exit, the elements on and below the diagonal of the array
  //          contain the m by min(m,n) lower trapezoidal matrix L (L is
  //          lower triangular if m <= n); the elements above the diagonal,
  //          with the array TAU, represent the orthogonal matrix Q as a
  //          product of elementary reflectors (see Further Details).
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,M).
  
  //  TAU     (output) REAL array, dimension (min(M,N))
  //          The scalar factors of the elementary reflectors (see Further
  //          Details).
  
  //  WORK    (workspace) REAL array, dimension (M)
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument had an illegal value
  
  //  Further Details
  //  ===============
  
  //  The matrix Q is represented as a product of elementary reflectors
  
  //     Q = H(k) . . . H(2) H(1), where k = min(m,n).
  
  //  Each H(i) has the form
  
  //     H(i) = I - tau * v * v'
  
  //  where tau is a real scalar, and v is a real vector with
  //  v(1:i-1) = 0 and v(i) = 1; v(i+1:n) is stored on exit in A(i,i+1:n),
  //  and tau in TAU(i).
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input arguments
  
  info = 0;
  if( m < 0 ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( lda < max( 1, m ) ) { 
    info = -4;
  }
  if( info != 0 ) { 
    xerbla( "SGELQ2", -info );
    return;
  }
  
  k = min( m, n );
  
  for( i = 1, i_ = i - 1, _do0 = k; i <= _do0; i++, i_++ ) { 
    
    //        Generate elementary reflector H(i) to annihilate A(i,i+1:n)
    
    slarfg( n - i + 1, A(i_,i_), &A(min( i + 1, n ) - 1,i_), lda, 
     tau[i_] );
    if( i < m ) { 
      
      //           Apply H(i) to A(i+1:m,i:n) from the right
      
      aii = A(i_,i_);
      A(i_,i_) = ONE;
      slarf( 'R'/*Right*/, m - i, n - i + 1, &A(i_,i_), lda, 
       tau[i_], &A(i_,i_ + 1), lda, work );
      A(i_,i_) = aii;
    }
  }
  return;
  
  //     End of SGELQ2
  
#undef  A
} // end of function 


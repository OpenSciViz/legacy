/*
 * C++ implementation of Lapack routine sgels
 *
 * $Id: sgels.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:58:36
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: sgels.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ZERO = 0.0e0;
const float ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ sgels(const char &trans, const long &m, const long &n, const long &nrhs, 
 float *a, const long &lda, float *b, const long &ldb, float work[], const long &lwork, 
 long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
  int tpsd;
  long _do0, _do1, _do2, _do3, brow, i, i_, iascl, ibscl, j, 
   j_, mn, nb, scllen, wsize;
  float anrm, bignum, bnrm, rwork[1], smlnum;

  
  //  -- LAPACK driver routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SGELS solves square or over- and underdetermined linear systems
  //  involving the m-by-n matrix A and the right-hand side B
  //  using orthogonal reductions.  It is assumed that A has full rank.
  
  //  Cases:
  
  //  1) m >= n, TRANS = 'N':
  //     Solve the least-squares problem min|| A*X - B ||.
  //     Computing  A = [ Q1, Q2 ] [ R ] n
  //                      n  m-n   [ 0 ] m-n
  //     the least-squares solution is X = inv(R) * Q1' * B.
  //     A is overwritten by its QR factorization
  //     and B is overwritten by [ X  ]
  //                             [ RS ]
  //     where RS = Q2'*B. The residual B - A*X is Q2*RS.
  
  //  2) m >= n, TRANS = 'T':
  //     Solve the underdetermined system A' * X = B.
  //     Computing A = [ Q1, Q2 ] [ R ] n
  //                     n   m-n  [ 0 ] m-n
  //     the minimum-norm solution is X = Q1 * inv(R') * B.
  //     A is overwritten by its QR factorization, B is overwritten by X.
  
  //  3) m < n, TRANS = 'N':
  //     Solve the underdetermined system A * X = B.
  //     Computing A = [ L , 0] [ Q1 ] m
  //                     m  n-m [ Q2 ] n-m
  //     the minimum-norm solution is X = Q1' * inv(L) * B.
  //     A is overwritten by its LQ factorization, B is overwritten by X.
  
  //  4) m < n, TRANS = 'T':
  //     Solve the least-squares problem min|| A'*X - B ||.
  //     Computing A = [ L , 0] [ Q1 ] m
  //                     m  n-m [ Q2 ] n-m
  //     the least-squares solution is X = inv(L') * Q1 * B.
  //     A is overwritten by its LQ factorization
  //     and B is overwritten by [ X  ]
  //                             [ RS ]
  //     where RS = Q2*B. The residual B - A*X is Q2'*RS.
  
  //  Arguments
  //  =========
  
  //  TRANS   (input) CHARACTER
  //          If TRANS = 'N', we compute the minimum-norm or least-squares
  //          solution A * X = B.
  //          If TRANS = 'T', we compute the minimum-norm or least-squares
  //          solution A' * X = B.
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix A.  M >= 0.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix A.  N >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right-hand sides. NRHS >=0.
  
  //  A       (input/output) REAL array, dimension (LDA,N)
  //          On entry, the M by N matrix A.
  //          On exit,
  //            if M >= N, A has been overwritten by its QR factorization.
  //            if M <  N, A has been overwritten by its LQ factorization.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,M).
  
  //  B       (input/output) REAL array, dimension (LDB,NRHS)
  //          on entry,
  //            if TRANS = 'N', the M by NRHS right-hand side.
  //            if TRANS = 'T', the N by NRHS right-hand side.
  //          on exit,
  //            if M >= N and TRANS = 'N', the first N rows of B
  //                      contain the least-squares solution, the
  //                      remaining M - N rows the residual.
  //            if M >= N and TRANS = 'T', B contains the M by NRHS
  //                      minimum-norm solution.
  //            if M <  N and TRANS = 'N', B contains the N by NRHS
  //                      minimum-norm solution.
  //            if M <  N and TRANS = 'T', the first M rows of B
  //                      contain the least-squares solution, the
  //                      remaining N - M rows the residual.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B. LDB >= MAX(M,N).
  
  //  WORK    (workspace) REAL array, dimension (LWORK)
  //          on output, work(1) contains the workspace length required
  //                     for optimum efficiency
  
  //  LWORK   (input) INTEGER
  //          The dimension of the array WORK.
  //          LWORK >= MN + MAX(1,M,N,NRHS) where MN = min(M,N).
  //          The block algorithm will not be used unless
  //          LWORK >= MN + MAX(1,M,N,NRHS) * NB
  //          where NB is the block size for this environment.
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Local Arrays ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input arguments.
  
  info = 0;
  mn = min( m, n );
  if( !(lsame( trans, 'N' ) || lsame( trans, 'T' )) ) { 
    info = -1;
  }
  else if( m < 0 ) { 
    info = -2;
  }
  else if( n < 0 ) { 
    info = -3;
  }
  else if( nrhs < 0 ) { 
    info = -4;
  }
  else if( lda < max( 1, m ) ) { 
    info = -6;
  }
  else if( ldb < max( m, n ) ) { 
    info = -8;
  }
  else if( lwork < max( 1, mn + vmax( m, n, nrhs, IEND ) ) ) { 
    info = -10;
  }
  
  //     Figure out optimal block size
  
  if( info == 0 || info == -10 ) { 
    
    tpsd = TRUE;
    if( lsame( trans, 'N' ) ) 
      tpsd = FALSE;
    
    if( m >= n ) { 
      nb = ilaenv( 1, "SGEQRF", " ", m, n, -1, -1 );
      if( tpsd ) { 
        nb = max( nb, ilaenv( 1, "SORMQR", "LN", m, nrhs, 
         n, -1 ) );
      }
      else { 
        nb = max( nb, ilaenv( 1, "SORMQR", "LT", m, nrhs, 
         n, -1 ) );
      }
    }
    else { 
      nb = ilaenv( 1, "SGELQF", " ", m, n, -1, -1 );
      if( tpsd ) { 
        nb = max( nb, ilaenv( 1, "SORMLQ", "LT", n, nrhs, 
         m, -1 ) );
      }
      else { 
        nb = max( nb, ilaenv( 1, "SORMLQ", "LN", n, nrhs, 
         m, -1 ) );
      }
    }
    
    wsize = mn + vmax( m, n, nrhs, IEND )*nb;
    work[0] = (float)( wsize );
    
  }
  
  if( info != 0 ) { 
    xerbla( "SGELS ", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( vmin( m, n, nrhs, IEND ) == 0 ) { 
    slaset( 'F'/*Full*/, max( m, n ), nrhs, ZERO, ZERO, b, ldb );
    return;
  }
  
  //     Get machine parameters
  
  smlnum = slamch( 'S' )/slamch( 'P' );
  bignum = ONE/smlnum;
  slabad( smlnum, bignum );
  
  //     Scale A, B if max entry outside range [SMLNUM,BIGNUM]
  
  anrm = slange( 'M', m, n, a, lda, rwork );
  iascl = 0;
  if( anrm > ZERO && anrm < smlnum ) { 
    
    //        Scale matrix norm up to SMLNUM
    
    slascl( 'G', 0, 0, anrm, smlnum, m, n, a, lda, info );
    iascl = 1;
  }
  else if( anrm > bignum ) { 
    
    //        Scale matrix norm down to BIGNUM
    
    slascl( 'G', 0, 0, anrm, bignum, m, n, a, lda, info );
    iascl = 2;
  }
  else if( anrm == ZERO ) { 
    
    //        Matrix all zero. Return zero solution.
    
    slaset( 'F', max( m, n ), nrhs, ZERO, ZERO, b, ldb );
    goto L_50;
  }
  
  brow = m;
  if( tpsd ) 
    brow = n;
  bnrm = slange( 'M', brow, nrhs, b, ldb, rwork );
  ibscl = 0;
  if( bnrm > ZERO && bnrm < smlnum ) { 
    
    //        Scale matrix norm up to SMLNUM
    
    slascl( 'G', 0, 0, bnrm, smlnum, brow, nrhs, b, ldb, info );
    ibscl = 1;
  }
  else if( bnrm > bignum ) { 
    
    //        Scale matrix norm down to BIGNUM
    
    slascl( 'G', 0, 0, bnrm, bignum, brow, nrhs, b, ldb, info );
    ibscl = 2;
  }
  
  if( m >= n ) { 
    
    //        compute QR factorization of A
    
    sgeqrf( m, n, a, lda, &work[0], &work[mn], lwork - mn, info );
    
    //        workspace at least N, optimally N*NB
    
    if( !tpsd ) { 
      
      //           Least-Squares Problem min || A * X - B ||
      
      //           B(1:M,1:NRHS) := Q' * B(1:M,1:NRHS)
      
      sormqr( 'L'/*Left*/, 'T'/*Transpose*/, m, nrhs, n, a, 
       lda, &work[0], b, ldb, &work[mn], lwork - mn, info );
      
      //           workspace at least NRHS, optimally NRHS*NB
      
      //           B(1:N,1:NRHS) := inv(R) * B(1:N,1:NRHS)
      
      strsm( 'L'/*Left*/, 'U'/*Upper*/, 'N'/*No transpose*/
       , 'N'/*Non-unit*/, n, nrhs, ONE, a, lda, b, ldb );
      
      scllen = n;
      
    }
    else { 
      
      //           Overdetermined system of equations A' * X = B
      
      //           B(1:N,1:NRHS) := inv(R') * B(1:N,1:NRHS)
      
      strsm( 'L'/*Left*/, 'U'/*Upper*/, 'T'/*Transpose*/, 
       'N'/*Non-unit*/, n, nrhs, ONE, a, lda, b, ldb );
      
      //           B(N+1:M,1:NRHS) = ZERO
      
      for( j = 1, j_ = j - 1, _do0 = nrhs; j <= _do0; j++, j_++ ) { 
        for( i = n + 1, i_ = i - 1, _do1 = m; i <= _do1; i++, i_++ ) { 
          B(j_,i_) = ZERO;
        }
      }
      
      //           B(1:M,1:NRHS) := Q(1:N,:) * B(1:N,1:NRHS)
      
      sormqr( 'L'/*Left*/, 'N'/*No transpose*/, m, nrhs, n, 
       a, lda, &work[0], b, ldb, &work[mn], lwork - mn, info );
      
      //           workspace at least NRHS, optimally NRHS*NB
      
      scllen = m;
      
    }
    
  }
  else { 
    
    //        Compute LQ factorization of A
    
    sgelqf( m, n, a, lda, &work[0], &work[mn], lwork - mn, info );
    
    //        workspace at least M, optimally M*NB.
    
    if( !tpsd ) { 
      
      //           underdetermined system of equations A * X = B
      
      //           B(1:M,1:NRHS) := inv(L) * B(1:M,1:NRHS)
      
      strsm( 'L'/*Left*/, 'L'/*Lower*/, 'N'/*No transpose*/
       , 'N'/*Non-unit*/, m, nrhs, ONE, a, lda, b, ldb );
      
      //           B(M+1:N,1:NRHS) = 0
      
      for( j = 1, j_ = j - 1, _do2 = nrhs; j <= _do2; j++, j_++ ) { 
        for( i = m + 1, i_ = i - 1, _do3 = n; i <= _do3; i++, i_++ ) { 
          B(j_,i_) = ZERO;
        }
      }
      
      //           B(1:N,1:NRHS) := Q(1:N,:)' * B(1:M,1:NRHS)
      
      sormlq( 'L'/*Left*/, 'T'/*Transpose*/, n, nrhs, m, a, 
       lda, &work[0], b, ldb, &work[mn], lwork - mn, info );
      
      //           workspace at least NRHS, optimally NRHS*NB
      
      scllen = n;
      
    }
    else { 
      
      //           overdetermined system min || A' * X - B ||
      
      //           B(1:N,1:NRHS) := Q * B(1:N,1:NRHS)
      
      sormlq( 'L'/*Left*/, 'N'/*No transpose*/, n, nrhs, m, 
       a, lda, &work[0], b, ldb, &work[mn], lwork - mn, info );
      
      //           workspace at least NRHS, optimally NRHS*NB
      
      //           B(1:M,1:NRHS) := inv(L') * B(1:M,1:NRHS)
      
      strsm( 'L'/*Left*/, 'L'/*Lower*/, 'T'/*Transpose*/, 
       'N'/*Non-unit*/, m, nrhs, ONE, a, lda, b, ldb );
      
      scllen = m;
      
    }
    
  }
  
  //     Undo scaling
  
  if( iascl == 1 ) { 
    slascl( 'G', 0, 0, anrm, smlnum, scllen, nrhs, b, ldb, info );
  }
  else if( iascl == 2 ) { 
    slascl( 'G', 0, 0, anrm, bignum, scllen, nrhs, b, ldb, info );
  }
  if( ibscl == 1 ) { 
    slascl( 'G', 0, 0, smlnum, bnrm, scllen, nrhs, b, ldb, info );
  }
  else if( ibscl == 2 ) { 
    slascl( 'G', 0, 0, bignum, bnrm, scllen, nrhs, b, ldb, info );
  }
  
L_50:
  ;
  work[0] = (float)( wsize );
  
  return;
  
  //     End of SGELS
  
#undef  B
#undef  A
} // end of function 


/*
 * C++ implementation of Lapack routine zhbevx
 *
 * $Id: zhbevx.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:47:12
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zhbevx.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
const DComplex CZERO = DComplex(0.0e0);
const DComplex CONE = DComplex(1.0e0);
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zhbevx(const char &jobz, const char &range, const char &uplo, const long &n, 
 const long &kd, DComplex *ab, const long &ldab, DComplex *q, const long &ldq, 
 const double &vl, const double &vu, const long &il, const long &iu, const double &abstol, 
 long &m, double w[], DComplex *z, const long &ldz, DComplex work[], 
 double rwork[], long iwork[], long ifail[], long &info)
{
#define AB(I_,J_) (*(ab+(I_)*(ldab)+(J_)))
#define Q(I_,J_)  (*(q+(I_)*(ldq)+(J_)))
#define Z(I_,J_)  (*(z+(I_)*(ldz)+(J_)))
  int alleig, indeig, lower, valeig, wantz;
  char order;
  long _do0, _do1, _do2, _do3, _do4, _do5, i, i_, iinfo, ilen, 
   imax, indd, inde, indibl, indisp, indiwk, indrwk, indwrk, iscale, 
   itmp1, j, j_, jj, jj_, nsplit;
  double abstll, anrm, bignum, eps, rmax, rmin, safmin, sigma, smlnum, 
   tmp1, vll, vuu;

  
  //  -- LAPACK driver routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZHBEVX computes selected eigenvalues and, optionally, eigenvectors
  //  of a DComplex Hermitian band matrix A by calling the recommended
  //  sequence of LAPACK routines.  Eigenvalues/vectors can be selected by
  //  specifying either a range of values or a range of indices for the
  //  desired eigenvalues.
  
  //  Arguments
  //  =========
  
  //  JOBZ    (input) CHARACTER*1
  //          Specifies whether or not to compute the eigenvectors:
  //          = 'N':  Compute eigenvalues only.
  //          = 'V':  Compute eigenvalues and eigenvectors.
  
  //  RANGE   (input) CHARACTER*1
  //          = 'A': all eigenvalues will be found.
  //          = 'V': all eigenvalues in the half-open interval (VL,VU]
  //                 will be found.
  //          = 'I': the IL-th through IU-th eigenvalues will be found.
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangular part of the
  //          Hermitian band matrix A is stored:
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The number of rows and columns of the matrix A.  N >= 0.
  
  //  KD      (input) INTEGER
  //          The number of super-diagonals of the matrix A if UPLO = 'U',
  //          or the number of subdiagonals if UPLO = 'L'.  KD >= 0.
  
  //  AB      (input/output) COMPLEX*16 array, dimension (LDAB, N)
  //          On entry, the upper or lower triangle of the Hermitian band
  //          matrix A, stored in the first KD+1 rows of the array.  The
  //          j-th column of A is stored in the j-th column of the array AB
  //          as follows:
  //          if UPLO = 'U', AB(kd+1+i-j,j) = A(i,j) for max(1,j-kd)<=i<=j;
  //          if UPLO = 'L', AB(1+i-j,j)    = A(i,j) for j<=i<=min(n,j+kd).
  
  //          On exit, AB is overwritten by values generated during the
  //          reduction to tridiagonal form.
  
  //  LDAB    (input) INTEGER
  //          The leading dimension of the array AB.  LDAB >= KD + 1.
  
  //  Q       (output) COMPLEX*16 array, dimension (LDQ, N)
  //          If JOBZ = 'V', the N by N unitary matrix used in the
  //                          reduction to tridiagonal form.
  //          If JOBZ = 'N', the array Q is not referenced.
  
  //  LDQ     (input) INTEGER
  //          The leading dimension of the array Q.  If JOBZ = 'V', then
  //          LDQ >= max(1,N).
  
  //  VL      (input) DOUBLE PRECISION
  //          If RANGE='V', the lower bound of the interval to be searched
  //          for eigenvalues.  Not referenced if RANGE = 'A' or 'I'.
  
  //  VU      (input) DOUBLE PRECISION
  //          If RANGE='V', the upper bound of the interval to be searched
  //          for eigenvalues.  Not referenced if RANGE = 'A' or 'I'.
  
  //  IL      (input) INTEGER
  //          If RANGE='I', the index (from smallest to largest) of the
  //          smallest eigenvalue to be returned.  IL >= 1.
  //          Not referenced if RANGE = 'A' or 'V'.
  
  //  IU      (input) INTEGER
  //          If RANGE='I', the index (from smallest to largest) of the
  //          largest eigenvalue to be returned.  min(IL,N) <= IU <= N.
  //          Not referenced if RANGE = 'A' or 'V'.
  
  //  ABSTOL  (input) DOUBLE PRECISION
  //          The absolute error tolerance for the eigenvalues.
  //          An approximate eigenvalue is accepted as converged
  //          when it is determined to lie in an interval [a,b]
  //          of width less than or equal to
  
  //                  ABSTOL + EPS *   max( |a|,|b| ) ,
  
  //          where EPS is the machine precision.  If ABSTOL is less than
  //          or equal to zero, then  EPS*|T|  will be used in its place,
  //          where |T| is the 1-norm of the tridiagonal matrix obtained
  //          by reducing AB to tridiagonal form.  For most problems,
  //          this is the appropriate level of accuracy to request.
  //          For certain strongly graded matrices, greater accuracy
  //          can be obtained in very small eigenvalues by setting
  //          ABSTOL to zero or to some very small positive number.
  //          (Note, however, that if ABSTOL is less than sqrt(unfl),
  //          where unfl is the underflow threshold, then sqrt(unfl)
  //          will be used in its place.)
  
  //          See "Computing Small Singular Values of Bidiagonal Matrices
  //          with Guaranteed High Relative Accuracy," by Demmel and
  //          Kahan, LAPACK Working Note #3.
  
  //  M       (output) INTEGER
  //          Total number of eigenvalues found.  0 <= M <= N.
  
  //  W       (output) DOUBLE PRECISION array, dimension (N)
  //          On normal exit, the first M entries contain the selected
  //          eigenvalues in ascending order.
  
  //  Z       (output) COMPLEX*16 array, dimension (LDZ, N)
  //          If JOBZ = 'V', then on normal exit the first M columns of Z
  //          contain the orthonormal eigenvectors of the matrix
  //          corresponding to the selected eigenvalues.  If an eigenvector
  //          fails to converge, then that column of Z contains the latest
  //          approximation to the eigenvector, and the index of the
  //          eigenvector is returned in IFAIL.
  //          If JOBZ = 'N', then Z is not referenced.
  
  //  LDZ     (input) INTEGER
  //          The leading dimension of the array Z.  LDZ >= 1, and if
  //          JOBZ = 'V', LDZ >= max(1,N).
  
  //  WORK    (workspace) COMPLEX*16 array, dimension (N)
  
  //  RWORK   (workspace) DOUBLE PRECISION array, dimension (6*N)
  
  //  IWORK   (workspace) INTEGER array, dimension (5*N)
  
  //  IFAIL   (output) INTEGER array, dimension (N)
  //          If JOBZ = 'V', then on normal exit, the first M elements of
  //          IFAIL are zero.  If INFO > 0 on exit, then IFAIL contains the
  //          indices of the eigenvectors that failed to converge.
  //          If JOBZ = 'N', then IFAIL is not referenced.
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit.
  //          < 0:  if INFO = -i, the i-th argument had an illegal value.
  //          > 0:  if INFO = +i, then i eigenvectors failed to converge.
  //                Their indices are stored in array IFAIL.
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  wantz = lsame( jobz, 'V' );
  alleig = lsame( range, 'A' );
  valeig = lsame( range, 'V' );
  indeig = lsame( range, 'I' );
  lower = lsame( uplo, 'L' );
  
  info = 0;
  if( !(wantz || lsame( jobz, 'N' )) ) { 
    info = -1;
  }
  else if( !((alleig || valeig) || indeig) ) { 
    info = -2;
  }
  else if( !(lower || lsame( uplo, 'U' )) ) { 
    info = -3;
  }
  else if( n < 0 ) { 
    info = -4;
  }
  else if( kd < 0 ) { 
    info = -5;
  }
  else if( ldab < kd + 1 ) { 
    info = -7;
  }
  else if( ldq < n ) { 
    info = -9;
  }
  else if( (valeig && n > 0) && vu <= vl ) { 
    info = -11;
  }
  else if( indeig && il < 1 ) { 
    info = -12;
  }
  else if( indeig && (iu < min( n, il ) || iu > n) ) { 
    info = -13;
  }
  else if( ldz < 1 || (wantz && ldz < n) ) { 
    info = -18;
  }
  
  if( info != 0 ) { 
    xerbla( "ZHBEVX", -info );
    return;
  }
  
  //     Quick return if possible
  
  m = 0;
  if( n == 0 ) 
    return;
  
  if( n == 1 ) { 
    if( alleig || indeig ) { 
      m = 1;
      w[0] = real(AB(0,0));
    }
    else { 
      if( vl < real( AB(0,0) ) && vu >= real( AB(0,0) ) ) { 
        m = 1;
        w[0] = real(AB(0,0));
      }
    }
    if( wantz ) 
      Z(0,0) = CONE;
    return;
  }
  
  //     Get machine constants.
  
  safmin = dlamch( 'S'/*Safe minimum*/ );
  eps = dlamch( 'P'/*Precision*/ );
  smlnum = safmin/eps;
  bignum = ONE/smlnum;
  rmin = sqrt( smlnum );
  rmax = min( sqrt( bignum ), ONE/sqrt( sqrt( safmin ) ) );
  
  //     Scale matrix to allowable range, if necessary.
  
  iscale = 0;
  abstll = abstol;
  if( valeig ) { 
    vll = vl;
    vuu = vu;
  }
  anrm = zlanhb( 'M', uplo, n, kd, ab, ldab, rwork );
  if( anrm > ZERO && anrm < rmin ) { 
    iscale = 1;
    sigma = rmin/anrm;
  }
  else if( anrm > rmax ) { 
    iscale = 1;
    sigma = rmax/anrm;
  }
  if( iscale == 1 ) { 
    if( lower ) { 
      for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
        zdscal( min( kd + 1, n - i + 1 ), sigma, &AB(i_,0), 
         1 );
      }
    }
    else { 
      for( i = 1, i_ = i - 1, _do1 = n; i <= _do1; i++, i_++ ) { 
        ilen = min( i, kd + 1 );
        zdscal( ilen, sigma, &AB(i_,kd - ilen + 1), 1 );
      }
    }
    if( abstol > 0 ) 
      abstll = abstol*sigma;
    if( valeig ) { 
      vll = vl*sigma;
      vuu = vu*sigma;
    }
  }
  
  //     Call ZHBTRD to reduce Hermitian band matrix to tridiagonal form.
  
  indd = 1;
  inde = indd + n;
  indwrk = 1;
  zhbtrd( jobz, uplo, n, kd, ab, ldab, &rwork[indd - 1], &rwork[inde - 1], 
   q, ldq, &work[indwrk - 1], iinfo );
  
  //     If all eigenvalues are desired and ABSTOL is less than or equal
  //     to zero, then call DSTERF or ZSTEQR.  If this fails for some
  //     eigenvalue, then try DSTEBZ.
  
  if( (alleig || ((indeig && il == 1) && iu == n)) && (abstol <= 
   ZERO) ) { 
    dcopy( n, &rwork[indd - 1], 1, w, 1 );
    if( !wantz ) { 
      dsterf( n, w, &rwork[inde - 1], info );
    }
    else { 
      zlacpy( 'A', n, n, q, ldq, z, ldz );
      indrwk = inde + n;
      zsteqr( jobz, n, w, &rwork[inde - 1], z, ldz, &rwork[indrwk - 1], 
       info );
      if( info == 0 ) { 
        for( i = 1, i_ = i - 1, _do2 = n; i <= _do2; i++, i_++ ) { 
          ifail[i_] = 0;
        }
      }
    }
    if( info == 0 ) { 
      m = n;
      goto L_50;
    }
    info = 0;
  }
  
  //     Otherwise, call DSTEBZ and, if eigenvectors are desired, ZSTEIN.
  
  if( wantz ) { 
    order = 'B';
  }
  else { 
    order = 'E';
  }
  indrwk = inde + n;
  indibl = 1;
  indisp = indibl + n;
  indiwk = indisp + n;
  dstebz( range, order, n, vll, vuu, il, iu, abstll, &rwork[indd - 1], 
   &rwork[inde - 1], m, nsplit, w, &iwork[indibl - 1], &iwork[indisp - 1], 
   &rwork[indrwk - 1], &iwork[indiwk - 1], info );
  
  if( wantz ) { 
    zstein( n, &rwork[indd - 1], &rwork[inde - 1], m, w, &iwork[indibl - 1], 
     &iwork[indisp - 1], z, ldz, &rwork[indrwk - 1], &iwork[indiwk - 1], 
     ifail, info );
    
    //        Apply unitary matrix used in reduction to tridiagonal
    //        form to eigenvectors returned by ZSTEIN.
    
    for( j = 1, j_ = j - 1, _do3 = m; j <= _do3; j++, j_++ ) { 
      zcopy( n, &Z(j_,0), 1, &work[0], 1 );
      zgemv( 'N', n, n, CONE, q, ldq, work, 1, CZERO, &Z(j_,0), 
       1 );
    }
  }
  
  //     If matrix was scaled, then rescale eigenvalues appropriately.
  
L_50:
  ;
  if( iscale == 1 ) { 
    if( info == 0 ) { 
      imax = m;
    }
    else { 
      imax = info - 1;
    }
    dscal( imax, ONE/sigma, w, 1 );
  }
  
  //     If eigenvalues are not in order, then sort them, along with
  //     eigenvectors.
  
  if( wantz ) { 
    for( j = 1, j_ = j - 1, _do4 = m - 1; j <= _do4; j++, j_++ ) { 
      i = 0;
      tmp1 = w[j_];
      for( jj = j + 1, jj_ = jj - 1, _do5 = m; jj <= _do5; jj++, jj_++ ) { 
        if( w[jj_] < tmp1 ) { 
          i = jj;
          tmp1 = w[jj_];
        }
      }
      
      if( i != 0 ) { 
        itmp1 = iwork[indibl + i - 2];
        w[i - 1] = w[j_];
        iwork[indibl + i - 2] = iwork[indibl + j_ - 1];
        w[j_] = tmp1;
        iwork[indibl + j_ - 1] = itmp1;
        zswap( n, &Z(i - 1,0), 1, &Z(j_,0), 1 );
        if( info != 0 ) { 
          itmp1 = ifail[i - 1];
          ifail[i - 1] = ifail[j_];
          ifail[j_] = itmp1;
        }
      }
    }
  }
  
  return;
  
  //     End of ZHBEVX
  
#undef  Z
#undef  Q
#undef  AB
} // end of function 


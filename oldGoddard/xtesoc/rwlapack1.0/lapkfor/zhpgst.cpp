/*
 * C++ implementation of Lapack routine zhpgst
 *
 * $Id: zhpgst.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:47:46
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zhpgst.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ONE = 1.0e0;
const double HALF = 0.5e0;
const DComplex CONE = DComplex(1.0e0);
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zhpgst(const long &itype, const char &uplo, const long &n, 
 DComplex ap[], DComplex bp[], long &info)
{
  int upper;
  long _do0, _do1, _do2, _do3, j, j1, j1j1, j_, jj, k, k1, k1k1, 
   k_, kk;
  double ajj, akk, bjj, bkk;
  DComplex ct;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZHPGST reduces a DComplex Hermitian-definite generalized
  //  eigenproblem to standard form, using packed storage.
  
  //  If ITYPE = 1, the problem is A*x = lambda*B*x,
  //  and A is overwritten by inv(U')*A*inv(U) or inv(L)*A*inv(L')
  
  //  If ITYPE = 2 or 3, the problem is A*B*x = lambda*x or
  //  B*A*x = lambda*x, and A is overwritten by U*A*U` or L'*A*L.
  
  //  B must have been previously factorized as U'*U or L*L' by ZPPTRF.
  
  //  Arguments
  //  =========
  
  //  ITYPE   (input) INTEGER
  //          = 1: compute inv(U')*A*inv(U) or inv(L)*A*inv(L');
  //          = 2 or 3: compute U*A*U' or L'*A*L.
  
  //  UPLO    (input) CHARACTER
  //          Specifies whether the upper or lower triangular part of the
  //          Hermitian matrix A is stored, and how B has been factorized.
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The order of the matrices A and B.  N >= 0.
  
  //  AP      (input/output) COMPLEX*16 array, dimension (N*(N+1)/2)
  //          On entry, the upper or lower triangle of the Hermitian matrix
  //          A, packed columnwise in a linear array.  The j-th column of A
  //          is stored in the array AP as follows:
  //          if UPLO = 'U', AP(i + (j-1)*j/2) = A(i,j) for 1<=i<=j;
  //          if UPLO = 'L', AP(i + (j-1)*(2n-j)/2) = A(i,j) for j<=i<=n.
  
  //          On exit, if INFO = 0, the transformed matrix, stored in the
  //          same format as A.
  
  //  BP      (input) COMPLEX*16 array, dimension (N*(N+1)/2)
  //          The triangular factor from the Cholesky factorization of B,
  //          stored in the same format as A, as returned by ZPPTRF.
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit.
  //          < 0:  if INFO = -i, the i-th argument had an illegal value.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  if( itype < 1 || itype > 3 ) { 
    info = -1;
  }
  else if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -2;
  }
  else if( n < 0 ) { 
    info = -3;
  }
  if( info != 0 ) { 
    xerbla( "ZHPGST", -info );
    return;
  }
  
  if( itype == 1 ) { 
    if( upper ) { 
      
      //           Compute inv(U')*A*inv(U)
      
      //           J1 and JJ are the indices of A(1,j) and A(j,j)
      
      jj = 0;
      for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
        j1 = jj + 1;
        jj = jj + j;
        
        //              Compute the j-th column of the upper triangle of A
        
        ap[jj - 1] = DComplex(real( ap[jj - 1] ));
        bjj = real(bp[jj - 1]);
        ztpsv( uplo, 'C'/*Conjugate transpose*/, 'N'/*Non-unit*/
         , j, bp, &ap[j1 - 1], 1 );
        zhpmv( uplo, j - 1, -(CONE), ap, &bp[j1 - 1], 1, CONE, 
         &ap[j1 - 1], 1 );
        zdscal( j - 1, ONE/bjj, &ap[j1 - 1], 1 );
        ap[jj - 1] = (ap[jj - 1] - zdotc( j - 1, &ap[j1 - 1], 
         1, &bp[j1 - 1], 1 ))/bjj;
      }
    }
    else { 
      
      //           Compute inv(L)*A*inv(L')
      
      //           KK and K1K1 are the indices of A(k,k) and A(k+1,k+1)
      
      kk = 1;
      for( k = 1, k_ = k - 1, _do1 = n; k <= _do1; k++, k_++ ) { 
        k1k1 = kk + n - k + 1;
        
        //              Update the lower triangle of A(k:n,k:n)
        
        akk = real(ap[kk - 1]);
        bkk = real(bp[kk - 1]);
        akk = akk/pow(bkk, 2);
        ap[kk - 1] = DComplex(akk);
        if( k < n ) { 
          zdscal( n - k, ONE/bkk, &ap[kk], 1 );
          ct = DComplex(-HALF*akk);
          zaxpy( n - k, ct, &bp[kk], 1, &ap[kk], 1 );
          zhpr2( uplo, n - k, -(CONE), &ap[kk], 1, &bp[kk], 
           1, &ap[k1k1 - 1] );
          zaxpy( n - k, ct, &bp[kk], 1, &ap[kk], 1 );
          ztpsv( uplo, 'N'/*No transpose*/, 'N'/*Non-unit*/
           , n - k, &bp[k1k1 - 1], &ap[kk], 1 );
        }
        kk = k1k1;
      }
    }
  }
  else { 
    if( upper ) { 
      
      //           Compute U*A*U'
      
      //           K1 and KK are the indices of A(1,k) and A(k,k)
      
      kk = 0;
      for( k = 1, k_ = k - 1, _do2 = n; k <= _do2; k++, k_++ ) { 
        k1 = kk + 1;
        kk = kk + k;
        
        //              Update the upper triangle of A(1:k,1:k)
        
        akk = real(ap[kk - 1]);
        bkk = real(bp[kk - 1]);
        ztpmv( uplo, 'N'/*No transpose*/, 'N'/*Non-unit*/
         , k - 1, bp, &ap[k1 - 1], 1 );
        ct = DComplex(HALF*akk);
        zaxpy( k - 1, ct, &bp[k1 - 1], 1, &ap[k1 - 1], 1 );
        zhpr2( uplo, k - 1, CONE, &ap[k1 - 1], 1, &bp[k1 - 1], 
         1, ap );
        zaxpy( k - 1, ct, &bp[k1 - 1], 1, &ap[k1 - 1], 1 );
        zdscal( k - 1, bkk, &ap[k1 - 1], 1 );
        ap[kk - 1] = DComplex(akk*pow(bkk, 2));
      }
    }
    else { 
      
      //           Compute L'*A*L
      
      //           JJ and J1J1 are the indices of A(j,j) and A(j+1,j+1)
      
      jj = 1;
      for( j = 1, j_ = j - 1, _do3 = n; j <= _do3; j++, j_++ ) { 
        j1j1 = jj + n - j + 1;
        
        //              Compute the j-th column of the lower triangle of A
        
        ajj = real(ap[jj - 1]);
        bjj = real(bp[jj - 1]);
        ap[jj - 1] = ajj*bjj + zdotc( n - j, &ap[jj], 1, &bp[jj], 
         1 );
        zdscal( n - j, bjj, &ap[jj], 1 );
        zhpmv( uplo, n - j, CONE, &ap[j1j1 - 1], &bp[jj], 
         1, CONE, &ap[jj], 1 );
        ztpmv( uplo, 'C'/*Conjugate transpose*/, 'N'/*Non-unit*/
         , n - j + 1, &bp[jj - 1], &ap[jj - 1], 1 );
        jj = j1j1;
      }
    }
  }
  return;
  
  //     End of ZHPGST
  
} // end of function 


/*
 * C++ implementation of lapack routine dtrrfs
 *
 * $Id: dtrrfs.cpp,v 1.6 1993/04/06 20:42:49 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:39:04
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dtrrfs.cpp,v $
 * Revision 1.6  1993/04/06  20:42:49  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.5  1993/03/19  18:41:23  alv
 * now passes chars explicitly, rather than indirection of a string, to shut up SUN warnings
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:17:52  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:09:27  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dtrrfs(const char &uplo, const char &trans, const char &diag, const long &n, 
 const long &nrhs, double *a, const long &lda, double *b, const long &ldb, 
 double *x, const long &ldx, double ferr[], double berr[], double work[], 
 long iwork[], long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
#define X(I_,J_)  (*(x+(I_)*(ldx)+(J_)))
  int notran, nounit, upper;
  char transt;
  long _do0, _do1, _do10, _do11, _do12, _do13, _do14, _do15, 
   _do16, _do17, _do18, _do19, _do2, _do20, _do21, _do22, _do23, 
   _do3, _do4, _do5, _do6, _do7, _do8, _do9, i, i_, j, j_, k, k_, 
   kase, nz;
  double eps, lstres, s, safe1, safe2, safmin, xk;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DTRRFS provides error bounds and backward error estimates for the
  //  solution to a system of linear equations with a triangular
  //  coefficient matrix.
  
  //  The solution vectors X must be computed by DTRTRS or some other
  //  means before entering this routine.  DTRRFS does not do iterative
  //  refinement because doing so can not improve the backward error.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the matrix A is upper or lower triangular.
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  TRANS   (input) CHARACTER*1
  //          Specifies the form of the system of equations.
  //          = 'N':  A * X = B  (No transpose)
  //          = 'T':  A'* X = B  (Transpose)
  //          = 'C':  A'* X = B  (Conjugate transpose = Transpose)
  
  //  DIAG    (input) CHARACTER*1
  //          Specifies whether or not the matrix A is unit triangular.
  //          = 'N':  Non-unit triangular
  //          = 'U':  Unit triangular
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right hand sides, i.e., the number of columns
  //          of the matrices B and X.  NRHS >= 0.
  
  //  A       (input) DOUBLE PRECISION array, dimension (LDA,N)
  //          The triangular matrix A.  If UPLO = 'U', the leading n by n
  //          upper triangular part of the array A contains the upper
  //          triangular matrix, and the strictly lower triangular part of
  //          A is not referenced.  If UPLO = 'L', the leading n by n lower
  //          triangular part of the array A contains the lower triangular
  //          matrix, and the strictly upper triangular part of A is not
  //          referenced.  If DIAG = 'U', the diagonal elements of A are
  //          also not referenced and are assumed to be 1.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  B       (input) DOUBLE PRECISION array, dimension (LDB,NRHS)
  //          The right hand side vectors for the system of linear
  //          equations.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  X       (input) DOUBLE PRECISION array, dimension (LDX,NRHS)
  //          The solution vectors for the system of linear equations.
  
  //  LDX     (input) INTEGER
  //          The leading dimension of the array X.  LDX >= max(1,N).
  
  //  FERR    (output) DOUBLE PRECISION array, dimension (NRHS)
  //          The estimated forward error bounds for each solution vector
  //          X.  If XTRUE is the true solution, FERR bounds the magnitude
  //          of the largest entry in (X - XTRUE) divided by the magnitude
  //          of the largest entry in X.  The quality of the error bound
  //          depends on the quality of the estimate of norm(inv(A))
  //          computed in the code; if the estimate of norm(inv(A)) is
  //          accurate, the error bound is guaranteed.
  
  //  BERR    (output) DOUBLE PRECISION array, dimension (NRHS)
  //          The componentwise relative backward error of each solution
  //          vector (i.e., the smallest relative change in any entry of A
  //          or B that makes X an exact solution).
  
  //  WORK    (workspace) DOUBLE PRECISION array, dimension (3*N)
  
  //  IWORK   (workspace) INTEGER array, dimension (N)
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  notran = lsame( trans, 'N' );
  nounit = lsame( diag, 'N' );
  
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( (!notran && !lsame( trans, 'T' )) && !lsame( trans, 'C' )
    ) { 
    info = -2;
  }
  else if( !nounit && !lsame( diag, 'U' ) ) { 
    info = -3;
  }
  else if( n < 0 ) { 
    info = -4;
  }
  else if( nrhs < 0 ) { 
    info = -5;
  }
  else if( lda < max( 1, n ) ) { 
    info = -7;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -9;
  }
  else if( ldx < max( 1, n ) ) { 
    info = -11;
  }
  if( info != 0 ) { 
    xerbla( "DTRRFS", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 || nrhs == 0 ) { 
    for( j = 1, j_ = j - 1, _do0 = nrhs; j <= _do0; j++, j_++ ) { 
      ferr[j_] = ZERO;
      berr[j_] = ZERO;
    }
    return;
  }
  
  if( notran ) { 
    transt = 'T';
  }
  else { 
    transt = 'N';
  }
  
  //     NZ = maximum number of nonzero entries in each row of A, plus 1
  
  nz = n + 1;
  eps = dlamch( 'E'/* Epsilon */ );
  safmin = dlamch( 'S'/* Safe minimum */ );
  safe1 = nz*safmin;
  safe2 = safe1/eps;
  
  //     Do for each right hand side
  
  for( j = 1, j_ = j - 1, _do1 = nrhs; j <= _do1; j++, j_++ ) { 
    
    //        Compute residual R = B - op(A) * X,
    //        where op(A) = A or A', depending on TRANS.
    
    dcopy( n, &X(j_,0), 1, &work[n], 1 );
    dtrmv( uplo, trans, diag, n, a, lda, &work[n], 1 );
    daxpy( n, -ONE, &B(j_,0), 1, &work[n], 1 );
    
    //        Compute componentwise relative backward error from formula
    
    //        max(i) ( abs(R(i)) / ( abs(op(A))*abs(X) + abs(B) )(i) )
    
    //        where abs(Z) is the componentwise absolute value of the matrix
    //        or vector Z.  If the i-th component of the denominator is less
    //        than SAFE2, then SAFE1 is added to the i-th components of the
    //        numerator and denominator before dividing.
    
    for( i = 1, i_ = i - 1, _do2 = n; i <= _do2; i++, i_++ ) { 
      work[i_] = abs( B(j_,i_) );
    }
    
    if( notran ) { 
      
      //           Compute abs(A)*abs(X) + abs(B).
      
      if( upper ) { 
        if( nounit ) { 
          for( k = 1, k_ = k - 1, _do3 = n; k <= _do3; k++, k_++ ) { 
            xk = abs( X(j_,k_) );
            for( i = 1, i_ = i - 1, _do4 = k; i <= _do4; i++, i_++ ) { 
              work[i_] = work[i_] + abs( A(k_,i_) )*
               xk;
            }
          }
        }
        else { 
          for( k = 1, k_ = k - 1, _do5 = n; k <= _do5; k++, k_++ ) { 
            xk = abs( X(j_,k_) );
            for( i = 1, i_ = i - 1, _do6 = k - 1; i <= _do6; i++, i_++ ) { 
              work[i_] = work[i_] + abs( A(k_,i_) )*
               xk;
            }
            work[k_] = work[k_] + xk;
          }
        }
      }
      else { 
        if( nounit ) { 
          for( k = 1, k_ = k - 1, _do7 = n; k <= _do7; k++, k_++ ) { 
            xk = abs( X(j_,k_) );
            for( i = k, i_ = i - 1, _do8 = n; i <= _do8; i++, i_++ ) { 
              work[i_] = work[i_] + abs( A(k_,i_) )*
               xk;
            }
          }
        }
        else { 
          for( k = 1, k_ = k - 1, _do9 = n; k <= _do9; k++, k_++ ) { 
            xk = abs( X(j_,k_) );
            for( i = k + 1, i_ = i - 1, _do10 = n; i <= _do10; i++, i_++ ) { 
              work[i_] = work[i_] + abs( A(k_,i_) )*
               xk;
            }
            work[k_] = work[k_] + xk;
          }
        }
      }
    }
    else { 
      
      //           Compute abs(A')*abs(X) + abs(B).
      
      if( upper ) { 
        if( nounit ) { 
          for( k = 1, k_ = k - 1, _do11 = n; k <= _do11; k++, k_++ ) { 
            s = ZERO;
            for( i = 1, i_ = i - 1, _do12 = k; i <= _do12; i++, i_++ ) { 
              s = s + abs( A(k_,i_) )*abs( X(j_,i_) );
            }
            work[k_] = work[k_] + s;
          }
        }
        else { 
          for( k = 1, k_ = k - 1, _do13 = n; k <= _do13; k++, k_++ ) { 
            s = abs( X(j_,k_) );
            for( i = 1, i_ = i - 1, _do14 = k - 1; i <= _do14; i++, i_++ ) { 
              s = s + abs( A(k_,i_) )*abs( X(j_,i_) );
            }
            work[k_] = work[k_] + s;
          }
        }
      }
      else { 
        if( nounit ) { 
          for( k = 1, k_ = k - 1, _do15 = n; k <= _do15; k++, k_++ ) { 
            s = ZERO;
            for( i = k, i_ = i - 1, _do16 = n; i <= _do16; i++, i_++ ) { 
              s = s + abs( A(k_,i_) )*abs( X(j_,i_) );
            }
            work[k_] = work[k_] + s;
          }
        }
        else { 
          for( k = 1, k_ = k - 1, _do17 = n; k <= _do17; k++, k_++ ) { 
            s = abs( X(j_,k_) );
            for( i = k + 1, i_ = i - 1, _do18 = n; i <= _do18; i++, i_++ ) { 
              s = s + abs( A(k_,i_) )*abs( X(j_,i_) );
            }
            work[k_] = work[k_] + s;
          }
        }
      }
    }
    s = ZERO;
    for( i = 1, i_ = i - 1, _do19 = n; i <= _do19; i++, i_++ ) { 
      if( work[i_] > safe2 ) { 
        s = max( s, abs( work[n + i_] )/work[i_] );
      }
      else { 
        s = max( s, (abs( work[n + i_] ) + safe1)/(work[i_] + 
         safe1) );
      }
    }
    berr[j_] = s;
    
    //        Bound error from formula
    
    //        norm(X - XTRUE) / norm(X) .le. FERR =
    //        norm( abs(inv(op(A)))*
    //           ( abs(R) + NZ*EPS*( abs(op(A))*abs(X)+abs(B) ))) / norm(X)
    
    //        where
    //          norm(Z) is the magnitude of the largest component of Z
    //          inv(op(A)) is the inverse of op(A)
    //          abs(Z) is the componentwise absolute value of the matrix or
    //             vector Z
    //          NZ is the maximum number of nonzeros in any row of A, plus 1
    //          EPS is machine epsilon
    
    //        The i-th component of abs(R)+NZ*EPS*(abs(op(A))*abs(X)+abs(B))
    //        is incremented by SAFE1 if the i-th component of
    //        abs(op(A))*abs(X) + abs(B) is less than SAFE2.
    
    //        Use DLACON to estimate the infinity-norm of the matrix
    //           inv(op(A)) * diag(W),
    //        where W = abs(R) + NZ*EPS*( abs(op(A))*abs(X)+abs(B) )))
    
    for( i = 1, i_ = i - 1, _do20 = n; i <= _do20; i++, i_++ ) { 
      if( work[i_] > safe2 ) { 
        work[i_] = abs( work[n + i_] ) + nz*eps*work[i_];
      }
      else { 
        work[i_] = abs( work[n + i_] ) + nz*eps*work[i_] + 
         safe1;
      }
    }
    
    kase = 0;
L_210:
    ;
    dlacon( n, &work[n*2], &work[n], iwork, ferr[j_], kase );
    if( kase != 0 ) { 
      if( kase == 1 ) { 
        
        //              Multiply by diag(W)*inv(op(A)').
        
        dtrsv( uplo, transt, diag, n, a, lda, &work[n], 1 );
        for( i = 1, i_ = i - 1, _do21 = n; i <= _do21; i++, i_++ ) { 
          work[n + i_] = work[i_]*work[n + i_];
        }
      }
      else { 
        
        //              Multiply by inv(op(A))*diag(W).
        
        for( i = 1, i_ = i - 1, _do22 = n; i <= _do22; i++, i_++ ) { 
          work[n + i_] = work[i_]*work[n + i_];
        }
        dtrsv( uplo, trans, diag, n, a, lda, &work[n], 1 );
      }
      goto L_210;
    }
    
    //        Normalize error.
    
    lstres = ZERO;
    for( i = 1, i_ = i - 1, _do23 = n; i <= _do23; i++, i_++ ) { 
      lstres = max( lstres, abs( X(j_,i_) ) );
    }
    if( lstres != ZERO ) 
      ferr[j_] = ferr[j_]/lstres;
    
  }
  
  return;
  
  //     End of DTRRFS
  
#undef  X
#undef  B
#undef  A
} // end of function 


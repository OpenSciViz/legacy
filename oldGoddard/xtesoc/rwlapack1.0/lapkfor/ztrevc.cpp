/*
 * C++ implementation of Lapack routine ztrevc
 *
 * $Id: ztrevc.cpp,v 1.2 1993/07/21 22:22:14 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:51:16
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: ztrevc.cpp,v $
 * Revision 1.2  1993/07/21  22:22:14  alv
 * ported to Microsoft visual C++
 *
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
const DComplex CMZERO = DComplex(0.0e0);
const DComplex CMONE = DComplex(1.0e0);
// end of PARAMETER translations

inline double ztrevc_cabs1(DComplex cdum) { return abs( real( (cdum) ) ) + 
   abs( imag( (cdum) ) ); }
RWLAPKDECL void /*FUNCTION*/ ztrevc(const char &job, const char &howmny, int select[], 
 const long &n, DComplex *t, const long &ldt, DComplex *vl, const long &ldvl, 
 DComplex *vr, const long &ldvr, const long &mm, long &m, DComplex work[], 
 double rwork[], long &info)
{
#define T(I_,J_)  (*(t+(I_)*(ldt)+(J_)))
#define VL(I_,J_) (*(vl+(I_)*(ldvl)+(J_)))
#define VR(I_,J_) (*(vr+(I_)*(ldvr)+(J_)))
  int allv, bothv, leftv, over, rightv, somev;
  long _do0, _do1, _do10, _do11, _do2, _do3, _do4, _do5, _do6, 
   _do7, _do8, _do9, i, i_, ii, is, j, j_, k, k_, ki, ki_;
  double ovfl, remax, scale, smin, smlnum, ulp, unfl;
  DComplex cdum;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZTREVC computes all or some right and/or left eigenvectors of a
  //  DComplex upper triangular matrix T.
  
  //  The right eigenvector x and the left eigenvector y of T corresponding
  //  to an eigenvalue w are defined by:
  
  //               T*x = w*x,     y'*T = w*y'
  
  //  where y' denotes the conjugate transpose of the vector y.
  
  //  The routine may either return the matrices X and/or Y of right or
  //  left eigenvectors of T, or the products Q*X and/or Q*Y, where Q is an
  //  input unitary matrix. If T was obtained from the Schur factorization
  //  of an original matrix A = Q*T*Q', then Q*X and/or Q*Y are the
  //  matrices of right or left eigenvectors of A.
  
  //  Arguments
  //  =========
  
  //  JOB     (input) CHARACTER*1
  //          = 'R': compute right eigenvectors only
  //          = 'L': compute left eigenvectors only
  //          = 'B': compute both right and left eigenvectors
  
  //  HOWMNY  (input) CHARACTER*1
  //          Specifies how many left/right eigenvectors are wanted and the
  //          form of the eigenvector matrix X or Y returned in VR or VL.
  //          = 'A': compute all right and/or left eigenvectors;
  //          = 'O': compute all right and/or left eigenvectors, multiplied
  //                 on the left by an input (generally unitary) matrix;
  //          = 'S': compute some right and/or left eigenvectors, specified
  //                 by the logical array SELECT.
  
  //  SELECT  (input) LOGICAL array, dimension (N)
  //          If HOWMNY = 'S', SELECT specifies the eigenvectors to be
  //          computed.  To select the eigenvector corresponding to the
  //          j-th eigenvalue, SELECT(j) must be set to .TRUE..
  //          If HOWMNY = 'A' or 'O', SELECT is not referenced.
  
  //  N       (input) INTEGER
  //          The order of the matrix T. N >= 0.
  
  //  T       (input/output) COMPLEX*16 array, dimension (LDT,N)
  //          The upper triangular matrix T. T is modified, but restored.
  
  //  LDT     (input) INTEGER
  //          The leading dimension of the array T. LDT >= max(1,N).
  
  //  VL      (input/output) COMPLEX*16 array, dimension (LDVL,MM)
  //          On entry, if JOB = 'L' or 'B' and HOWMNY = 'O', VL must
  //          contain an n-by-n matrix Q (usually the unitary matrix Q of
  //          Schur vectors returned by ZHSEQR).
  //          On exit, if JOB = 'L' or 'B', VL contains:
  //          if HOWMNY = 'A', the matrix Y of left eigenvectors of T;
  //          if HOWMNY = 'O', the matrix Q*Y;
  //          if HOWMNY = 'S', the left eigenvectors of T specified by
  //                           SELECT, stored consecutively in the columns
  //                           of VL, in the same order as their
  //                           eigenvalues.
  //          If JOB = 'R', VL is not referenced.
  
  //  LDVL    (input) INTEGER
  //          The leading dimension of the array VL. LDVL >= max(1,N).
  
  //  VR      (input/output) COMPLEX*16 array, dimension (LDVR,MM)
  //          On entry, if JOB = 'R' or 'B' and HOWMNY = 'O', VR must
  //          contain an n-by-n matrix Q (usually the unitary matrix Q of
  //          Schur vectors returned by ZHSEQR).
  //          On exit, if JOB = 'R' or 'B', VR contains:
  //          if HOWMNY = 'A', the matrix X of right eigenvectors of T;
  //          if HOWMNY = 'O', the matrix Q*X;
  //          if HOWMNY = 'S', the right eigenvectors of T specified by
  //                           SELECT, stored consecutively in the columns
  //                           of VR, in the same order as their
  //                           eigenvalues.
  //          If JOB = 'L', VR is not referenced.
  
  //  LDVR    (input) INTEGER
  //          The leading dimension of the array VR. LDVR >= max(1,N).
  
  //  MM      (input) INTEGER
  //          The number of columns in the arrays VL and/or VR. MM >= M.
  
  //  M       (output) INTEGER
  //          The number of columns in the arrays VL and/or VR required to
  //          store the eigenvectors. If HOWMNY = 'A' or 'O', M is set
  //          to N.
  
  //  WORK    (workspace) COMPLEX*16 array, dimension (2*N)
  
  //  RWORK   (workspace) DOUBLE PRECISION array, dimension (N)
  
  //  INFO    (output) INTEGER
  //          = 0:   successful exit
  //          < 0:   if INFO = -i, the i-th argument had an illegal value
  
  //  Further Details
  //  ===============
  
  //  The algorithm used in this program is basically backward (forward)
  //  substitution, with scaling to make the the code robust against
  //  possible overflow.
  
  //  Each eigenvector is normalized so that the element of largest
  //  magnitude has magnitude 1; here the magnitude of a DComplex number
  //  (x,y) is taken to be |x| + |y|.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Statement Functions ..
  //     ..
  //     .. Statement Function definitions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Decode and test the input parameters
  
  bothv = lsame( job, 'B' );
  rightv = lsame( job, 'R' ) || bothv;
  leftv = lsame( job, 'L' ) || bothv;
  
  allv = lsame( howmny, 'A' );
  over = lsame( howmny, 'O' );
  somev = lsame( howmny, 'S' );
  
  //     Set M to the number of columns required to store the selected
  //     eigenvectors.
  
  if( somev ) { 
    m = 0;
    for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
      if( select[j_] ) 
        m = m + 1;
    }
  }
  else { 
    m = n;
  }
  
  info = 0;
  if( !rightv && !leftv ) { 
    info = -1;
  }
  else if( (!allv && !over) && !somev ) { 
    info = -2;
  }
  else if( n < 0 ) { 
    info = -4;
  }
  else if( ldt < max( 1, n ) ) { 
    info = -6;
  }
  else if( ldvl < 1 || (leftv && ldvl < n) ) { 
    info = -8;
  }
  else if( ldvr < 1 || (rightv && ldvr < n) ) { 
    info = -10;
  }
  else if( mm < m ) { 
    info = -11;
  }
  if( info != 0 ) { 
    xerbla( "ZTREVC", -info );
    return;
  }
  
  //     Quick return if possible.
  
  if( n == 0 ) 
    return;
  
  //     Set the constants to control overflow.
  
  unfl = dlamch( 'S'/*Safe minimum*/ );
  ovfl = ONE/unfl;
  dlabad( unfl, ovfl );
  ulp = dlamch( 'P'/*Precision*/ );
  smlnum = unfl*(n/ulp);
  
  //     Store the diagonal elements of T in working array WORK.
  
  for( i = 1, i_ = i - 1, _do1 = n; i <= _do1; i++, i_++ ) { 
    work[i_ + n] = T(i_,i_);
  }
  
  //     Compute 1-norm of each column of strictly upper triangular
  //     part of T to control overflow in triangular solver.
  
  rwork[0] = ZERO;
  for( j = 2, j_ = j - 1, _do2 = n; j <= _do2; j++, j_++ ) { 
    rwork[j_] = dzasum( j - 1, &T(j_,0), 1 );
  }
  
  if( rightv ) { 
    
    //        Compute right eigenvectors.
    
    is = m;
    for( ki = n, ki_ = ki - 1; ki >= 1; ki--, ki_-- ) { 
      
      if( somev ) { 
        if( !select[ki_] ) 
          goto L_80;
      }
      smin = max( ulp*(ztrevc_cabs1( T(ki_,ki_) )), smlnum );
      
      work[0] = CMONE;
      
      //           Form right-hand side.
      
      for( k = 1, k_ = k - 1, _do3 = ki - 1; k <= _do3; k++, k_++ ) { 
        work[k_] = -(T(ki_,k_));
      }
      
      //           Solve the triangular system:
      //              (T(1:KI-1,1:KI-1) - T(KI,KI))*X = SCALE*WORK.
      
      for( k = 1, k_ = k - 1, _do4 = ki - 1; k <= _do4; k++, k_++ ) { 
        T(k_,k_) = T(k_,k_) - T(ki_,ki_);
        if( ztrevc_cabs1( T(k_,k_) ) < smin ) 
          T(k_,k_) = (DComplex)(smin);
      }
      
      if( ki > 1 ) { 
        zlatrs( 'U'/*Upper*/, 'N'/*No transpose*/, 'N'/*Non-unit*/
         , 'Y', ki - 1, t, ldt, &work[0], scale, rwork, info );
        work[ki_] = DComplex(scale);
      }
      
      //           Copy the vector x or Q*x to VR and normalize.
      
      if( !over ) { 
        zcopy( ki, &work[0], 1, &VR(is - 1,0), 1 );
        
        ii = izamax( ki, &VR(is - 1,0), 1 );
        remax = ONE/ztrevc_cabs1( VR(is - 1,ii - 1) );
        zdscal( ki, remax, &VR(is - 1,0), 1 );
        
        for( k = ki + 1, k_ = k - 1, _do5 = n; k <= _do5; k++, k_++ ) { 
          VR(is - 1,k_) = CMZERO;
        }
      }
      else { 
        if( ki > 1 ) 
          zgemv( 'N', n, ki - 1, CMONE, vr, ldvr, &work[0], 
           1, DComplex( scale, 0. ), &VR(ki_,0), 1 );
        
        ii = izamax( n, &VR(ki_,0), 1 );
        remax = ONE/ztrevc_cabs1( VR(ki_,ii - 1) );
        zdscal( n, remax, &VR(ki_,0), 1 );
      }
      
      //           Set back the original diagonal elements of T.
      
      for( k = 1, k_ = k - 1, _do6 = ki - 1; k <= _do6; k++, k_++ ) { 
        T(k_,k_) = work[k_ + n];
      }
      
      is = is - 1;
L_80:
      ;
    }
  }
  
  if( leftv ) { 
    
    //        Compute left eigenvectors.
    
    is = 1;
    for( ki = 1, ki_ = ki - 1, _do7 = n; ki <= _do7; ki++, ki_++ ) { 
      
      if( somev ) { 
        if( !select[ki_] ) 
          goto L_130;
      }
      smin = max( ulp*(ztrevc_cabs1( T(ki_,ki_) )), smlnum );
      
      work[n - 1] = CMONE;
      
      //           Form right-hand side.
      
      for( k = ki + 1, k_ = k - 1, _do8 = n; k <= _do8; k++, k_++ ) { 
        work[k_] = -(conj( T(k_,ki_) ));
      }
      
      //           Solve the triangular system:
      //              (T(KI+1:N,KI+1:N) - T(KI,KI))'*X = SCALE*WORK.
      
      for( k = ki + 1, k_ = k - 1, _do9 = n; k <= _do9; k++, k_++ ) { 
        T(k_,k_) = T(k_,k_) - T(ki_,ki_);
        if( ztrevc_cabs1( T(k_,k_) ) < smin ) 
          T(k_,k_) = (DComplex)(smin);
      }
      
      if( ki < n ) { 
        zlatrs( 'U'/*Upper*/, 'C'/*Conjugate transpose*/
         , 'N'/*Non-unit*/, 'Y', n - ki, &T(ki_ + 1,ki_ + 1), 
         ldt, &work[ki_ + 1], scale, rwork, info );
        work[ki_] = DComplex(scale);
      }
      
      //           Copy the vector x or Q*x to VL and normalize.
      
      if( !over ) { 
        zcopy( n - ki + 1, &work[ki_], 1, &VL(is - 1,ki_), 
         1 );
        
        ii = izamax( n - ki + 1, &VL(is - 1,ki_), 1 ) + ki - 
         1;
        remax = ONE/ztrevc_cabs1( VL(is - 1,ii - 1) );
        zdscal( n - ki + 1, remax, &VL(is - 1,ki_), 1 );
        
        for( k = 1, k_ = k - 1, _do10 = ki - 1; k <= _do10; k++, k_++ ) { 
          VL(is - 1,k_) = CMZERO;
        }
      }
      else { 
        if( ki < n ) 
          zgemv( 'N', n, n - ki, CMONE, &VL(ki_ + 1,0), 
           ldvl, &work[ki_ + 1], 1, DComplex( scale, 0. ), 
           &VL(ki_,0), 1 );
        
        ii = izamax( n, &VL(ki_,0), 1 );
        remax = ONE/ztrevc_cabs1( VL(ki_,ii - 1) );
        zdscal( n, remax, &VL(ki_,0), 1 );
      }
      
      //           Set back the original diagonal elements of T.
      
      for( k = ki + 1, k_ = k - 1, _do11 = n; k <= _do11; k++, k_++ ) { 
        T(k_,k_) = work[k_ + n];
      }
      
      is = is + 1;
L_130:
      ;
    }
  }
  
  return;
  
  //     End of ZTREVC
  
#undef  VR
#undef  VL
#undef  T
} // end of function 


/*
 * C++ implementation of Lapack routine zunglq
 *
 * $Id: zunglq.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:51:39
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zunglq.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const DComplex ZERO = DComplex(0.0e0);
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zunglq(const long &m, const long &n, const long &k, DComplex *a, 
 const long &lda, DComplex tau[], DComplex work[], const long &lwork, long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  long _do0, _do1, _do2, _do3, _do4, _do5, i, i_, ib, iinfo, 
   iws, j, j_, ki, kk, l, l_, ldwork, nb, nbmin, nx;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZUNGLQ generates an m by n DComplex matrix Q with orthonormal rows,
  //  which is defined as the first m rows of a product of k elementary
  //  reflectors of order n
  
  //        Q  =  H(k)' . . . H(2)' H(1)'
  
  //  as returned by ZGELQF.
  
  //  Arguments
  //  =========
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix Q. M >= 0.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix Q. N >= M.
  
  //  K       (input) INTEGER
  //          The number of elementary reflectors whose product defines the
  //          matrix Q. M >= K >= 0.
  
  //  A       (input/output) COMPLEX*16 array, dimension (LDA,N)
  //          On entry, the i-th row must contain the vector which defines
  //          the elementary reflector H(i), for i = 1,2,...,k, as returned
  //          by ZGELQF in the first k rows of its array argument A.
  //          On exit, the m by n matrix Q.
  
  //  LDA     (input) INTEGER
  //          The first dimension of the array A. LDA >= max(1,M).
  
  //  TAU     (input) COMPLEX*16 array, dimension (K)
  //          TAU(i) must contain the scalar factor of the elementary
  //          reflector H(i), as returned by ZGELQF.
  
  //  WORK    (workspace) COMPLEX*16 array, dimension (LWORK)
  //          On exit, if INFO = 0, WORK(1) returns the minimum value of
  //          LWORK required to use the optimal blocksize.
  
  //  LWORK   (input) INTEGER
  //          The dimension of the array WORK. LWORK >= max(1,M).
  //          For optimum performance LWORK should be at least M*NB, where
  //          NB is the optimal blocksize.
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit;
  //          < 0: if INFO = -i, the i-th argument has an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input arguments
  
  info = 0;
  if( m < 0 ) { 
    info = -1;
  }
  else if( n < m ) { 
    info = -2;
  }
  else if( k < 0 || k > m ) { 
    info = -3;
  }
  else if( lda < max( 1, m ) ) { 
    info = -5;
  }
  else if( lwork < max( 1, m ) ) { 
    info = -8;
  }
  if( info != 0 ) { 
    xerbla( "ZUNGLQ", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( m <= 0 ) { 
    work[0] = DComplex((double)1);
    return;
  }
  
  //     Determine the block size.
  
  nb = ilaenv( 1, "ZUNGLQ", " ", m, n, k, -1 );
  nbmin = 2;
  nx = 0;
  iws = m;
  if( nb > 1 && nb < k ) { 
    
    //        Determine when to cross over from blocked to unblocked code.
    
    nx = max( 0, ilaenv( 3, "ZUNGLQ", " ", m, n, k, -1 ) );
    if( nx < k ) { 
      
      //           Determine if workspace is large enough for blocked code.
      
      ldwork = m;
      iws = ldwork*nb;
      if( lwork < iws ) { 
        
        //              Not enough workspace to use optimal NB:  reduce NB and
        //              determine the minimum value of NB.
        
        nb = lwork/ldwork;
        nbmin = max( 2, ilaenv( 2, "ZUNGLQ", " ", m, n, k, 
         -1 ) );
      }
    }
  }
  
  if( (nb >= nbmin && nb < k) && nx < k ) { 
    
    //        Use blocked code after the last block.
    //        The first kk rows are handled by the block method.
    
    ki = ((k - nx - 1)/nb)*nb;
    kk = min( k, ki + nb );
    
    //        Set A(kk+1:m,1:kk) to zero.
    
    for( j = 1, j_ = j - 1, _do0 = kk; j <= _do0; j++, j_++ ) { 
      for( i = kk + 1, i_ = i - 1, _do1 = m; i <= _do1; i++, i_++ ) { 
        A(j_,i_) = ZERO;
      }
    }
  }
  else { 
    kk = 0;
  }
  
  //     Use unblocked code for the last or only block.
  
  if( kk < m ) 
    zungl2( m - kk, n - kk, k - kk, &A(kk,kk), lda, &tau[kk], 
     work, iinfo );
  
  if( kk > 0 ) { 
    
    //        Use blocked code
    
    for( i = ki + 1, i_ = i - 1, _do2=docnt(i,1,_do3 = -nb); _do2 > 0; i += _do3, i_ += _do3, _do2-- ) { 
      ib = min( nb, k - i + 1 );
      if( i + ib <= m ) { 
        
        //              Form the triangular factor of the block reflector
        //              H = H(i) H(i+1) . . . H(i+ib-1)
        
        zlarft( 'F'/*Forward*/, 'R'/*Rowwise*/, n - i + 
         1, ib, &A(i_,i_), lda, &tau[i_], work, ldwork );
        
        //              Apply H' to A(i+ib:m,i:n) from the right
        
        zlarfb( 'R'/*Right*/, 'C'/*Conjugate transpose*/
         , 'F'/*Forward*/, 'R'/*Rowwise*/, m - i - ib + 
         1, n - i + 1, ib, &A(i_,i_), lda, work, ldwork, &A(i_,i_ + ib), 
         lda, &work[ib], ldwork );
      }
      
      //           Apply H' to columns i:n of current block
      
      zungl2( ib, n - i + 1, ib, &A(i_,i_), lda, &tau[i_], work, 
       iinfo );
      
      //           Set columns 1:i-1 of current block to zero
      
      for( j = 1, j_ = j - 1, _do4 = i - 1; j <= _do4; j++, j_++ ) { 
        for( l = i, l_ = l - 1, _do5 = i + ib - 1; l <= _do5; l++, l_++ ) { 
          A(j_,l_) = ZERO;
        }
      }
    }
  }
  
  work[0] = DComplex((double)iws);
  return;
  
  //     End of ZUNGLQ
  
#undef  A
} // end of function 


/*
 * C++ implementation of lapack routine dtptrs
 *
 * $Id: dtptrs.cpp,v 1.5 1993/04/06 20:42:47 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:38:56
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dtptrs.cpp,v $
 * Revision 1.5  1993/04/06  20:42:47  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:17:48  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:09:25  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dtptrs(const char &uplo, const char &trans, const char &diag, const long &n, 
 const long &nrhs, double ap[], double *b, const long &ldb, long &info)
{
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
  int nounit, upper;
  long _do0, _do1, _do2, info_, j, j_, jc;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DTPTRS solves a triangular system of the form
  
  //     A * x = b  or  A' * x = b,
  
  //  where A is a triangular matrix of order N stored in packed format,
  //  A' is the transpose of A, and b is an N by NRHS matrix.  A check is
  //  made to verify that A is nonsingular.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the matrix A is upper or lower triangular.
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  TRANS   (input) CHARACTER*1
  //          Specifies the operation applied to A.
  //          = 'N':  Solve  A * x = b  (No transpose)
  //          = 'T':  Solve  A'* x = b  (Transpose)
  //          = 'C':  Solve  A'* x = b  (Conjugate transpose = Transpose)
  
  //  DIAG    (input) CHARACTER*1
  //          Specifies whether or not the matrix A is unit triangular.
  //          = 'N':  Non-unit triangular
  //          = 'U':  Unit triangular
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right hand sides, i.e., the number of columns
  //          of the matrix B.  NRHS >= 0.
  
  //  AP      (input) DOUBLE PRECISION array, dimension (N*(N+1)/2)
  //          The upper or lower triangular matrix A, packed columnwise in
  //          a linear array.  The j-th column of A is stored in the array
  //          AP as follows:
  //          if UPLO = 'U', AP((j-1)*j/2 + i) = A(i,j) for 1<=i<=j;
  //          if UPLO = 'L',
  //             AP((j-1)*(n-j) + j*(j+1)/2 + i-j) = A(i,j) for j<=i<=n.
  
  //  B       (input/output) DOUBLE PRECISION array, dimension (LDB,NRHS)
  //          On entry, the right hand side vectors b for the system of
  //          linear equations.
  //          On exit, if INFO = 0, the solution vectors x.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: if INFO = k, the k-th diagonal element of A is zero,
  //               indicating that the matrix is singular and the solutions
  //               x have not been computed.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  nounit = lsame( diag, 'N' );
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( (!lsame( trans, 'N' ) && !lsame( trans, 'T' )) && !lsame( trans, 
   'C' ) ) { 
    info = -2;
  }
  else if( !nounit && !lsame( diag, 'U' ) ) { 
    info = -3;
  }
  else if( n < 0 ) { 
    info = -4;
  }
  else if( nrhs < 0 ) { 
    info = -5;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -8;
  }
  if( info != 0 ) { 
    xerbla( "DTPTRS", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  //     Check for singularity.
  
  if( nounit ) { 
    if( upper ) { 
      jc = 1;
      for( info = 1, info_ = info - 1, _do0 = n; info <= _do0; info++, info_++ ) { 
        if( ap[jc + info_ - 1] == ZERO ) 
          return;
        jc = jc + info;
      }
    }
    else { 
      jc = 1;
      for( info = 1, info_ = info - 1, _do1 = n; info <= _do1; info++, info_++ ) { 
        if( ap[jc - 1] == ZERO ) 
          return;
        jc = jc + n - info + 1;
      }
    }
  }
  info = 0;
  
  //     Solve A * x = b  or  A' * x = b.
  
  for( j = 1, j_ = j - 1, _do2 = nrhs; j <= _do2; j++, j_++ ) { 
    dtpsv( uplo, trans, diag, n, ap, &B(j_,0), 1 );
  }
  
  return;
  
  //     End of DTPTRS
  
#undef  B
} // end of function 


/*
 * C++ implementation of Lapack routine zlargv
 *
 * $Id: zlargv.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:49:08
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zlargv.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zlargv(const long &n, DComplex x[], const long &incx, 
 DComplex y[], const long &incy, double c[], const long &incc)
{
  long _do0, i, i_, ic, ix, iy;
  double absx, absy, tt, w;
  DComplex t, xi, yi;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZLARGV generates a vector of DComplex plane rotations with real
  //  cosines, determined by elements of the DComplex vectors x and y.
  //  For i = 1,2,...,n
  
  //     (        c(i)   s(i) ) ( x(i) ) = ( a(i) )
  //     ( -conjg(s(i))  c(i) ) ( y(i) ) = (   0  )
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The number of plane rotations to be generated.
  
  //  X       (input/output) COMPLEX*16 array, dimension (1+(N-1)*INCX)
  //          On entry, the vector x.
  //          On exit, x(i) is overwritten by a(i), for i = 1,...,n.
  
  //  INCX    (input) INTEGER
  //          The increment between elements of X. INCX > 0.
  
  //  Y       (input/output) COMPLEX*16 array, dimension (1+(N-1)*INCY)
  //          On entry, the vector y.
  //          On exit, the sines of the plane rotations.
  
  //  INCY    (input) INTEGER
  //          The increment between elements of Y. INCY > 0.
  
  //  C       (output) DOUBLE PRECISION array, dimension (1+(N-1)*INCC)
  //          The cosines of the plane rotations.
  
  //  INCC    (input) INTEGER
  //          The increment between elements of C. INCC > 0.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  ix = 1;
  iy = 1;
  ic = 1;
  for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
    xi = x[ix - 1];
    yi = y[iy - 1];
    absx = abs( xi );
    if( absx == ZERO ) { 
      c[ic - 1] = ZERO;
      y[iy - 1] = DComplex(ONE);
      x[ix - 1] = yi;
    }
    else { 
      absy = abs( yi );
      w = max( absx, absy );
      t = xi/absx;
      absx = absx/w;
      absy = absy/w;
      tt = sqrt( absx*absx + absy*absy );
      c[ic - 1] = absx/tt;
      y[iy - 1] = (t*conj( yi ))/(w*tt);
      x[ix - 1] = t*(w*tt);
    }
    ix = ix + incx;
    iy = iy + incy;
    ic = ic + incc;
  }
  return;
  
  //     End of ZLARGV
  
} // end of function 


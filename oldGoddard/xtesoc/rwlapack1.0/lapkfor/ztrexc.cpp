/*
 * C++ implementation of Lapack routine ztrexc
 *
 * $Id: ztrexc.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:51:18
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: ztrexc.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

RWLAPKDECL void /*FUNCTION*/ ztrexc(const char &compq, const long &n, DComplex *t, const long &ldt, 
   DComplex *q, const long &ldq, const long &ifst, const long &ilst, long &info)
{
#define T(I_,J_)  (*(t+(I_)*(ldt)+(J_)))
#define Q(I_,J_)  (*(q+(I_)*(ldq)+(J_)))
  int wantq;
  long _do0, _do1, k, k_, m1, m2, m3;
  double cs;
  DComplex sn, t11, t22, temp;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZTREXC reorders the Schur factorization of a DComplex matrix
  //  A = Q*T*Q', so that the diagonal element of T with row index IFST is
  //  moved to row ILST.
  
  //  The Schur form T is reordered by a unitary similarity transformation
  //  Z'*T*Z, and optionally the matrix Q of Schur vectors is updated by
  //  postmultplying it with Z.
  
  //  Arguments
  //  =========
  
  //  COMPQ   (input) CHARACTER*1
  //          = 'V': update the matrix Q of Schur vectors;
  //          = 'N': do not update Q.
  
  //  N       (input) INTEGER
  //          The order of the matrix T. N >= 0.
  
  //  T       (input/output) COMPLEX*16 array, dimension (LDT,N)
  //          On entry, the upper triangular matrix T.
  //          On exit, T is overwritten by the reordered matrix T.
  
  //  LDT     (input) INTEGER
  //          The leading dimension of the array T. LDT >= max(1,N).
  
  //  Q       (input/output) COMPLEX*16 array, dimension (LDQ,N)
  //          On entry, if COMPQ = 'V', the matrix Q of Schur vectors.
  //          On exit, if COMPQ = 'V', Q has been postmultiplied by the
  //          unitary transformation matrix Z which reorders T.
  //          If COMPQ = 'N', Q is not referenced.
  
  //  LDQ     (input) INTEGER
  //          The leading dimension of the array Q.
  //          LDQ >= 1; and if COMPQ = 'V', LDQ >= max(1,N).
  
  //  IFST    (input) INTEGER
  //  ILST    (input) INTEGER
  //          Specify the re-ordering of the diagonal elements of T:
  //          The element with row index IFST is moved to row ILST by a
  //          sequence of transpositions between adjacent elements.
  //          1 <= IFST <= N; 1 <= ILST <= N.
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Decode and test the input parameters.
  
  info = 0;
  wantq = lsame( compq, 'V' );
  if( !lsame( compq, 'N' ) && !wantq ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( ldt < max( 1, n ) ) { 
    info = -4;
  }
  else if( ldq < 1 || (wantq && ldq < max( 1, n )) ) { 
    info = -6;
  }
  else if( ifst < 1 || ifst > n ) { 
    info = -7;
  }
  else if( ilst < 1 || ilst > n ) { 
    info = -8;
  }
  if( info != 0 ) { 
    xerbla( "ZTREXC", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 1 || ifst == ilst ) 
    return;
  
  if( ifst < ilst ) { 
    
    //        Move the IFST-th diagonal element forward down the diagonal.
    
    m1 = 0;
    m2 = -1;
    m3 = 1;
  }
  else { 
    
    //        Move the IFST-th diagonal element backward up the diagonal.
    
    m1 = -1;
    m2 = 0;
    m3 = -1;
  }
  
  for( k = ifst + m1, k_ = k - 1, _do0=docnt(k,ilst + m2,_do1 = m3); _do0 > 0; k += _do1, k_ += _do1, _do0-- ) { 
    
    //        Interchange the k-th and (k+1)-th diagonal elements.
    
    t11 = T(k_,k_);
    t22 = T(k_ + 1,k_ + 1);
    
    //        Determine the transformation to perform the interchange.
    
    zlartg( T(k_ + 1,k_), t22 - t11, cs, sn, temp );
    
    //        Apply transformation to the matrix T.
    
    if( k + 2 <= n ) 
      zrot( n - k - 1, &T(k_ + 2,k_), ldt, &T(k_ + 2,k_ + 1), 
       ldt, cs, sn );
    zrot( k - 1, &T(k_,0), 1, &T(k_ + 1,0), 1, cs, conj( sn ) );
    
    T(k_,k_) = t22;
    T(k_ + 1,k_ + 1) = t11;
    
    if( wantq ) { 
      
      //           Accumulate transformation in the matrix Q.
      
      zrot( n, &Q(k_,0), 1, &Q(k_ + 1,0), 1, cs, conj( sn ) );
    }
    
  }
  
  return;
  
  //     End of ZTREXC
  
#undef  T
#undef  Q
} // end of function 


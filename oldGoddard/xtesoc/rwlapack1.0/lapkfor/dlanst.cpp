/*
 * C++ implementation of lapack routine dlanst
 *
 * $Id: dlanst.cpp,v 1.4 1993/04/06 20:41:06 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:35:31
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dlanst.cpp,v $
 * Revision 1.4  1993/04/06  20:41:06  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.3  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.2  1993/03/05  23:15:28  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:07:20  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ONE = 1.0e0;
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL double /*FUNCTION*/ dlanst(const char &norm, const long &n, double d[], double e[])
{
  long _do0, _do1, i, i_;
  double anorm, dlanst_v, scale, sum;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DLANST  returns the value of the one norm,  or the Frobenius norm, or
  //  the  infinity norm,  or the  element of  largest absolute value  of a
  //  real symmetric tridiagonal matrix A.
  
  //  Description
  //  ===========
  
  //  DLANST returns the value
  
  //     DLANST = ( max(abs(A(i,j))), NORM = 'M' or 'm'
  //              (
  //              ( norm1(A),         NORM = '1', 'O' or 'o'
  //              (
  //              ( normI(A),         NORM = 'I' or 'i'
  //              (
  //              ( normF(A),         NORM = 'F', 'f', 'E' or 'e'
  
  //  where  norm1  denotes the  one norm of a matrix (maximum column sum),
  //  normI  denotes the  infinity norm  of a matrix  (maximum row sum) and
  //  normF  denotes the  Frobenius norm of a matrix (square root of sum of
  //  squares).  Note that  max(abs(A(i,j)))  is not a  matrix norm.
  
  //  Arguments
  //  =========
  
  //  NORM    (input) CHARACTER*1
  //          Specifies the value to be returned in DLANST as described
  //          above.
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.  When N = 0, DLANST is
  //          set to zero.
  
  //  D       (input) DOUBLE PRECISION array, dimension (N)
  //          The diagonal elements of A.
  
  //  E       (input) DOUBLE PRECISION array, dimension (N-1)
  //          The (n-1) sub-diagonal or super-diagonal elements of A.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  if( n <= 0 ) { 
    anorm = ZERO;
  }
  else if( lsame( norm, 'M' ) ) { 
    
    //        Find max(abs(A(i,j))).
    
    anorm = abs( d[n - 1] );
    for( i = 1, i_ = i - 1, _do0 = n - 1; i <= _do0; i++, i_++ ) { 
      anorm = max( anorm, abs( d[i_] ) );
      anorm = max( anorm, abs( e[i_] ) );
    }
  }
  else if( (lsame( norm, 'O' ) || norm == '1') || lsame( norm, 'I' ) ) { 
    
    //        Find norm1(A).
    
    if( n == 1 ) { 
      anorm = abs( d[0] );
    }
    else { 
      anorm = max( abs( d[0] ) + abs( e[0] ), abs( e[n - 2] ) + 
       abs( d[n - 1] ) );
      for( i = 2, i_ = i - 1, _do1 = n - 1; i <= _do1; i++, i_++ ) { 
        anorm = max( anorm, abs( d[i_] ) + abs( e[i_] ) + 
         abs( e[i_ - 1] ) );
      }
    }
  }
  else if( (lsame( norm, 'F' )) || (lsame( norm, 'E' )) ) { 
    
    //        Find normF(A).
    
    scale = ZERO;
    sum = ONE;
    if( n > 1 ) { 
      dlassq( n - 1, e, 1, scale, sum );
      sum = 2*sum;
    }
    dlassq( n, d, 1, scale, sum );
    anorm = scale*sqrt( sum );
  }
  
  dlanst_v = anorm;
  return( dlanst_v );
  
  //     End of DLANST
  
} // end of function 


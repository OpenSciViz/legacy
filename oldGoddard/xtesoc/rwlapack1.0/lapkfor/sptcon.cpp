/*
 * C++ implementation of Lapack routine sptcon
 *
 * $Id: sptcon.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:02:08
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: sptcon.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ONE = 1.0e0;
const float ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ sptcon(const long &n, float d[], float e[], const float &anorm, 
 float &rcond, float work[], long &info)
{
  long _do0, _do1, i, i_, ix;
  float ainvnm;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SPTCON estimates the reciprocal of the condition number of a real
  //  symmetric positive definite tridiagonal matrix using the
  //  factorization A = L*D*L' computed by SPTTRF.
  
  //  An estimate is obtained for norm(inv(A)), and the reciprocal of the
  //  condition number is computed as RCOND = 1 / (ANORM * norm(inv(A))).
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  D       (input) REAL array, dimension (N)
  //          The n diagonal elements of the diagonal matrix D from the
  //          L*D*L' factorization of A.
  
  //  E       (input) REAL array, dimension (N-1)
  //          The (n-1) subdiagonal elements of the unit bidiagonal factor
  //          L from the L*D*L' factorization of A.  E can also be regarded
  //          as the superdiagonal of the unit bidiagonal factor U from the
  //          factorization A = U'*D*U.
  
  //  ANORM   (input) REAL
  //          The 1-norm of the original matrix A.
  
  //  RCOND   (output) REAL
  //          The reciprocal of the condition number of the matrix A,
  //          computed as RCOND = 1/(ANORM * AINVNM), where AINVNM is an
  //          estimate of the 1-norm of inv(A) computed in this routine.
  
  //  WORK    (workspace) REAL array, dimension (N)
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  
  //  Further Details
  //  ===============
  
  //  The method used is described in Nicholas J. Higham, "Efficient
  //  Algorithms for Computing the Condition Number of a Tridiagonal
  //  Matrix", SIAM J. Sci. Stat. Comput., Vol. 7, No. 1, January 1986.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input arguments.
  
  info = 0;
  if( n < 0 ) { 
    info = -1;
  }
  else if( anorm < ZERO ) { 
    info = -4;
  }
  if( info != 0 ) { 
    xerbla( "SPTCON", -info );
    return;
  }
  
  //     Quick return if possible
  
  rcond = ZERO;
  if( n == 0 ) { 
    rcond = ONE;
    return;
  }
  else if( anorm == ZERO ) { 
    return;
  }
  
  //     Check that D(1:N) is positive.
  
  for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
    if( d[i_] <= ZERO ) 
      return;
  }
  
  //     Solve M(A) * x = e, where M(A) = (m(i,j)) is given by
  
  //        m(i,j) =  abs(A(i,j)), i = j,
  //        m(i,j) = -abs(A(i,j)), i .ne. j,
  
  //     and e = [ 1, 1, ..., 1 ]'.  Note M(A) = M(L)*D*M(L)'.
  
  //     Solve M(L) * x = e.
  
  work[0] = ONE;
  for( i = 2, i_ = i - 1, _do1 = n; i <= _do1; i++, i_++ ) { 
    work[i_] = ONE + work[i_ - 1]*abs( e[i_ - 1] );
  }
  
  //     Solve D * M(L)' * x = b.
  
  work[n - 1] = work[n - 1]/d[n - 1];
  for( i = n - 1, i_ = i - 1; i >= 1; i--, i_-- ) { 
    work[i_] = work[i_]/d[i_] + work[i_ + 1]*abs( e[i_] );
  }
  
  //     Compute AINVNM = max(x(i)), 1<=i<=n.
  
  ix = isamax( n, work, 1 );
  ainvnm = abs( work[ix - 1] );
  
  //     Compute the estimate of the reciprocal condition number.
  
  if( ainvnm != ZERO ) 
    rcond = (ONE/ainvnm)/anorm;
  
  return;
  
  //     End of SPTCON
  
} // end of function 


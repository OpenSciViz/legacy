/*
 * C++ implementation of lapack routine dsygv
 *
 * $Id: dsygv.cpp,v 1.6 1993/04/06 20:42:32 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:38:32
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dsygv.cpp,v $
 * Revision 1.6  1993/04/06  20:42:32  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.5  1993/03/19  18:41:23  alv
 * now passes chars explicitly, rather than indirection of a string, to shut up SUN warnings
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:17:25  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:09:13  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dsygv(const long &itype, const char &jobz, const char &uplo, const long &n, 
 double *a, const long &lda, double *b, const long &ldb, double w[], double work[], 
 const long &lwork, long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
  int upper, wantz;
  char trans;
  long neig;

  
  //  -- LAPACK driver routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DSYGV computes the eigenvalues and optionally the eigenvectors of a
  //  real generalized symmetric-definite eigenproblem, of the form
  //  A*x=(lambda)*B*x,  A*Bx=(lambda)*x,  or B*A*x=(lambda)*x.
  //  Here A and B are assumed to be symmetric and B is also
  //  positive definite.
  
  //  Arguments
  //  =========
  
  //  ITYPE   (input) INTEGER
  //          Specifies the problem type to be solved:
  //          = 1:  A*x = (lambda)*B*x
  //          = 2:  A*B*x = (lambda)*x
  //          = 3:  B*A*x = (lambda)*x
  
  //  JOBZ    (input) CHARACTER*1
  //          Specifies whether or not to compute the eigenvectors:
  //          = 'N':  Compute eigenvalues only.
  //          = 'V':  Compute eigenvalues and eigenvectors.
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangular part of the
  //          symmetric matrices A and B are stored.
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The number of rows and columns of the matrix A.  N >= 0.
  
  //  A       (input/output) DOUBLE PRECISION array, dimension (LDA, N)
  //          On entry, the symmetric matrix A.  If UPLO = 'U', only the
  //          upper triangular part of A is used to define the elements of
  //          the symmetric matrix.  If UPLO = 'L', only the lower
  //          triangular part of A is used to define the elements of the
  //          symmetric matrix.
  
  //          If JOBZ = 'V', then if INFO = 0 on exit, A contains the
  //          matrix Z of eigenvectors of the matrix A.  If 0 < INFO <= N,
  //          A contains the eigenvectors associated with only the stored
  //          eigenvalues. The eigenvectors are normalized as follows:
  //          if ITYPE = 1 or 2, Z'*B*Z = I; if ITYPE = 3, Z'*inv(B)*Z = I.
  //          If JOBZ = 'N', then on exit the upper triangle (if UPLO='U')
  //          or the lower triangle (if UPLO='L') of A, including the
  //          diagonal, is destroyed.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  B       (input/output) DOUBLE PRECISION array, dimension (LDB, N)
  //          On entry, the symmetric matrix B.  If UPLO = 'U', only the
  //          upper triangular part of B is used to define the elements of
  //          the symmetric matrix.  If UPLO = 'L', only the lower
  //          triangular part of B is used to define the elements of the
  //          symmetric matrix.
  
  //          On exit, if INFO <= N, the part of B containing the matrix is
  //          overwritten by the triangular factor U or L from the Cholesky
  //          factorization B = U'*U or B = L*L'.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  W       (output) DOUBLE PRECISION array, dimension (N)
  //          On exit, if INFO = 0, W contains the eigenvalues in ascending
  //          order.  If 0 < INFO <= N, the eigenvalues are correct for
  //          indices 1, 2, ..., INFO-1, but they are unordered and may not
  //          be the smallest eigenvalues of the matrix.
  
  //  WORK    (workspace) DOUBLE PRECISION array, dimension (LWORK)
  //          On exit, WORK(1) is set to the dimension of the work array
  //          needed to obtain optimal performance from this routine.
  //          See the description of LWORK below.
  
  //  LWORK   (input) INTEGER
  //          The length of the array WORK.  LWORK >= max(1,3*N-1).
  //          For optimal efficiency, LWORK should be at least (NB+2)*N,
  //          where NB is the blocksize for DSYTRD returned by ILAENV.
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit.
  //          < 0:  if INFO = -k, the k-th argument had an illegal value.
  //          > 0:  DPOTRF or DSYEV returned an error code
  //             <= n:  if INFO = k, then DSYEV terminated before finding
  //                    the k-th eigenvalue.
  //             > n:   if INFO = n + k, for 1 <= k <= n, then the leading
  //                    minor of order k of B is not positive definite.
  //                    The factorization of B could not be completed and
  //                    no eigenvalues or eigenvectors were computed.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  wantz = lsame( jobz, 'V' );
  upper = lsame( uplo, 'U' );
  
  info = 0;
  if( itype < 0 || itype > 3 ) { 
    info = -1;
  }
  else if( !(wantz || lsame( jobz, 'N' )) ) { 
    info = -2;
  }
  else if( !(upper || lsame( uplo, 'L' )) ) { 
    info = -3;
  }
  else if( n < 0 ) { 
    info = -4;
  }
  else if( lda < max( 1, n ) ) { 
    info = -6;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -8;
  }
  else if( lwork < max( 1, 3*n - 1 ) ) { 
    info = -11;
  }
  if( info != 0 ) { 
    xerbla( "DSYGV ", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  //     Form a Cholesky factorization of B.
  
  dpotrf( uplo, n, b, ldb, info );
  if( info != 0 ) { 
    info = n + info;
    return;
  }
  
  //     Transform problem to standard eigenvalue problem and solve.
  
  dsygst( itype, uplo, n, a, lda, b, ldb, info );
  dsyev( jobz, uplo, n, a, lda, w, work, lwork, info );
  
  if( wantz ) { 
    
    //        Backtransform eigenvectors to the original problem.
    
    neig = n;
    if( info > 0 ) 
      neig = info - 1;
    if( itype == 1 || itype == 2 ) { 
      
      //           For A*x=(lambda)*B*x and A*B*x=(lambda)*x;
      //           backtransform eigenvectors: x = inv(L)'*y or inv(U)*y
      
      if( upper ) { 
        trans = 'N';
      }
      else { 
        trans = 'T';
      }
      
      dtrsm( 'L'/* Left */, uplo, trans, 'N'/* Non-unit */, n, 
       neig, ONE, b, ldb, a, lda );
      
    }
    else if( itype == 3 ) { 
      
      //           For B*A*x=(lambda)*x;
      //           backtransform eigenvectors: x = L*y or U'*y
      
      if( upper ) { 
        trans = 'T';
      }
      else { 
        trans = 'N';
      }
      
      dtrmm( 'L'/* Left */, uplo, trans, 'N'/* Non-unit */, n, 
       neig, ONE, b, ldb, a, lda );
    }
  }
  return;
  
  //     End of DSYGV
  
#undef  B
#undef  A
} // end of function 


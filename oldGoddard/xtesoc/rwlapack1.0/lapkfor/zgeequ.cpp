/*
 * C++ implementation of Lapack routine zgeequ
 *
 * $Id: zgeequ.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:46:10
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zgeequ.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ONE = 1.0e0;
const double ZERO = 0.0e0;
// end of PARAMETER translations

inline double zgeequ_cabs1(DComplex zdum) { return abs( real( (zdum) ) ) + 
   abs( imag( (zdum) ) ); }
inline double zgeequ_cabs2(DComplex zdum) { return abs( real( (zdum) )/
   2.e0 ) + abs( imag( (zdum) )/2.e0 ); }
RWLAPKDECL void /*FUNCTION*/ zgeequ(const long &m, const long &n, DComplex *a, const long &lda, 
 double r[], double c[], double &rowcnd, double &colcnd, double &amax, 
 long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  long _do0, _do1, _do10, _do11, _do2, _do3, _do4, _do5, _do6, 
   _do7, _do8, _do9, i, i_, j, j_;
  double bignum, rcmax, rcmin, smlnum;
  DComplex zdum;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZGEEQU computes row and column scalings intended to equilibrate an
  //  M by N matrix A and reduce its condition number.  R returns the row
  //  scale factors and C the column scale factors, chosen to try to make
  //  the largest entry in each row and column of the matrix B with entries
  //  B(i,j)=R(i)*A(i,j)*C(j) have absolute value 1.  R(i) and C(j) are
  //  restricted to be between SMLNUM = smallest safe number and
  //  BIGNUM = largest safe number.
  
  //  ROWCND returns the ratio of the smallest R(i) to the largest R(i).
  //  If ROWCND >= 0.1 and AMAX is neither too large nor too small, it is
  //  not worth scaling by R.
  
  //  COLCND returns the ratio of the smallest C(i) to the largest C(i).
  //  If COLCND >= 0.1, it is not worth scaling by C.
  
  //  AMAX returns the absolute value of the largest matrix entry.
  //  If AMAX is very close to overflow or very close to underflow, the
  //  matrix should be scaled.
  
  //  Use of these scaling factors is not guaranteed to reduce the
  //  condition number of A but works well in practice.
  
  //  Arguments
  //  =========
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix A.  M >= 0.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix A.  N >= 0.
  
  //  A       (input) COMPLEX*16 array, dimension (LDA,N)
  //          The m by n matrix whose equilibration factors are
  //          to be computed.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,M).
  
  //  R       (output) DOUBLE PRECISION array, dimension (M)
  //          The row scale factors for A.  Unassigned if INFO > 0.
  
  //  C       (output) DOUBLE PRECISION array, dimension (N)
  //          The column scale factors for A.  Unassigned if INFO > 0.
  
  //  ROWCND  (output) DOUBLE PRECISION
  //          Ratio of the smallest R(i) to the largest R(i).
  //          Unassigned if 0 < INFO <= M.
  
  //  COLCND  (output) DOUBLE PRECISION
  //          Ratio of the smallest C(i) to the largest C(i).
  //          Unassigned if INFO > 0.
  
  //  AMAX    (output) DOUBLE PRECISION
  //          Absolute value of largest matrix entry.
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: if INFO = k, k<=M, the k-th row of A is exactly zero
  //               if INFO = k, k>M,  the k-M-th column of A is exactly
  //               zero
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Statement Functions ..
  //     ..
  //     .. Statement Function definitions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  if( m < 0 ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( lda < max( 1, m ) ) { 
    info = -4;
  }
  if( info != 0 ) { 
    xerbla( "ZGEEQU", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( m == 0 || n == 0 ) { 
    rowcnd = ONE;
    colcnd = ONE;
    amax = ZERO;
    return;
  }
  
  //     Get machine constants.
  
  smlnum = dlamch( 'S' );
  bignum = ONE/smlnum;
  
  //     Compute row scale factors.
  
  for( i = 1, i_ = i - 1, _do0 = m; i <= _do0; i++, i_++ ) { 
    r[i_] = ZERO;
  }
  
  //     Find the maximum element in each row.
  
  for( j = 1, j_ = j - 1, _do1 = n; j <= _do1; j++, j_++ ) { 
    for( i = 1, i_ = i - 1, _do2 = m; i <= _do2; i++, i_++ ) { 
      r[i_] = max( r[i_], zgeequ_cabs1( A(j_,i_) ) );
    }
  }
  
  //     Find the maximum and minimum scale factors.
  
  rcmin = bignum;
  rcmax = ZERO;
  for( i = 1, i_ = i - 1, _do3 = m; i <= _do3; i++, i_++ ) { 
    rcmax = max( rcmax, r[i_] );
    rcmin = min( rcmin, r[i_] );
  }
  amax = rcmax;
  
  if( rcmin == ZERO ) { 
    
    //        Find the first zero scale factor and return an error code.
    
    for( i = 1, i_ = i - 1, _do4 = m; i <= _do4; i++, i_++ ) { 
      if( r[i_] == ZERO ) { 
        info = i;
        return;
      }
    }
  }
  else { 
    
    //        Invert the scale factors.
    
    for( i = 1, i_ = i - 1, _do5 = m; i <= _do5; i++, i_++ ) { 
      r[i_] = ONE/min( max( r[i_], smlnum ), bignum );
    }
    
    //        Compute ROWCND = min(R(I)) / max(R(I))
    
    rowcnd = max( rcmin, smlnum )/min( rcmax, bignum );
  }
  
  //     Compute column scale factors
  
  for( j = 1, j_ = j - 1, _do6 = n; j <= _do6; j++, j_++ ) { 
    c[j_] = ZERO;
  }
  
  //     Find the maximum element in each column,
  //     assuming the row scaling computed above.
  
  for( j = 1, j_ = j - 1, _do7 = n; j <= _do7; j++, j_++ ) { 
    for( i = 1, i_ = i - 1, _do8 = m; i <= _do8; i++, i_++ ) { 
      c[j_] = max( c[j_], zgeequ_cabs1( A(j_,i_) )*r[i_] );
    }
  }
  
  //     Find the maximum and minimum scale factors.
  
  rcmin = bignum;
  rcmax = ZERO;
  for( j = 1, j_ = j - 1, _do9 = n; j <= _do9; j++, j_++ ) { 
    rcmin = min( rcmin, c[j_] );
    rcmax = max( rcmax, c[j_] );
  }
  
  if( rcmin == ZERO ) { 
    
    //        Find the first zero scale factor and return an error code.
    
    for( j = 1, j_ = j - 1, _do10 = n; j <= _do10; j++, j_++ ) { 
      if( c[j_] == ZERO ) { 
        info = m + j;
        return;
      }
    }
  }
  else { 
    
    //        Invert the scale factors.
    
    for( j = 1, j_ = j - 1, _do11 = n; j <= _do11; j++, j_++ ) { 
      c[j_] = ONE/min( max( c[j_], smlnum ), bignum );
    }
    
    //        Compute COLCND = min(C(J)) / max(C(J))
    
    colcnd = max( rcmin, smlnum )/min( rcmax, bignum );
  }
  
  return;
  
  //     End of ZGEEQU
  
#undef  A
} // end of function 


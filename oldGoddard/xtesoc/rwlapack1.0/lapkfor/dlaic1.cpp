/*
 * C++ implementation of lapack routine dlaic1
 *
 * $Id: dlaic1.cpp,v 1.7 1993/04/06 20:41:00 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:35:17
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dlaic1.cpp,v $
 * Revision 1.7  1993/04/06  20:41:00  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.6  1993/03/19  18:41:23  alv
 * now passes chars explicitly, rather than indirection of a string, to shut up SUN warnings
 *
 * Revision 1.5  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.4  1993/03/19  16:57:21  alv
 * sprinkled in some const
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:15:21  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:07:14  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
const double TWO = 2.0e0;
const double HALF = 0.5e0;
const double FOUR = 4.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dlaic1(const long &job, const long &j, double x[], const double &sest, 
 double w[], const double &gamma, double &sestpr, double &s, double &c)
{
  double absalp, absest, absgam, alpha, b, cosine, eps, norma, s1, 
   s2, sine, t, test, tmp, zeta1, zeta2;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DLAIC1 applies one step of incremental condition estimation in
  //  its simplest version:
  
  //  Let x, twonorm(x) = 1, be an approximate singular vector of an j-by-j
  //  lower triangular matrix L, such that
  //           twonorm(L*x) = sest
  //  Then DLAIC1 computes sestpr, s, c such that
  //  the vector
  //                  [ s*x ]
  //           xhat = [  c  ]
  //  is an approximate singular vector of
  //                  [ L     0  ]
  //           Lhat = [ w' gamma ]
  //  in the sense that
  //           twonorm(Lhat*xhat) = sestpr.
  
  //  Depending on JOB, an estimate for the largest or smallest singular
  //  value is computed.
  
  //  Note that [s c]' and sestpr**2 is an eigenpair of the system
  
  //      diag(sest*sest, 0) + [alpha  gamma] * [ alpha ]
  //                                            [ gamma ]
  
  //  where  alpha =  x'*w.
  
  //  Arguments
  //  =========
  
  //  JOB     (input) INTEGER
  //          = 1: an estimate for the largest singular value is computed.
  //          = 2: an estimate for the smallest singular value is computed.
  
  //  J       (input) INTEGER
  //          Length of X and W
  
  //  X       (input) DOUBLE PRECISION array, dimension (J)
  //          The j-vector x.
  
  //  SEST    (input) DOUBLE PRECISION
  //          Estimated singular value of j by j matrix L
  
  //  W       (input) DOUBLE PRECISION array, dimension (J)
  //          The j-vector w.
  
  //  GAMMA   (input) DOUBLE PRECISION
  //          The diagonal element gamma.
  
  //  SESTPR  (output) DOUBLE PRECISION
  //          Estimated singular value of (j+1) by (j+1) matrix Lhat.
  
  //  S       (output) DOUBLE PRECISION
  //          Sine needed in forming xhat.
  
  //  C       (output) DOUBLE PRECISION
  //          Cosine needed in forming xhat.
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Executable Statements ..
  
  eps = dlamch( 'E'/* Epsilon */ );
  alpha = ddot( j, x, 1, w, 1 );
  
  absalp = abs( alpha );
  absgam = abs( gamma );
  absest = abs( sest );
  
  if( job == 1 ) { 
    
    //        Estimating largest singular value
    
    //        special cases
    
    if( sest == ZERO ) { 
      s1 = max( absgam, absalp );
      if( s1 == ZERO ) { 
        s = ZERO;
        c = ONE;
        sestpr = ZERO;
      }
      else { 
        s = alpha/s1;
        c = gamma/s1;
        tmp = sqrt( s*s + c*c );
        s = s/tmp;
        c = c/tmp;
        sestpr = s1*tmp;
      }
      return;
    }
    else if( absgam <= eps*absest ) { 
      s = ONE;
      c = ZERO;
      tmp = max( absest, absalp );
      s1 = absest/tmp;
      s2 = absalp/tmp;
      sestpr = tmp*sqrt( s1*s1 + s2*s2 );
      return;
    }
    else if( absalp <= eps*absest ) { 
      s1 = absgam;
      s2 = absest;
      if( s1 <= s2 ) { 
        s = ONE;
        c = ZERO;
        sestpr = s2;
      }
      else { 
        s = ZERO;
        c = ONE;
        sestpr = s1;
      }
      return;
    }
    else if( absest <= eps*absalp || absest <= eps*absgam ) { 
      s1 = absgam;
      s2 = absalp;
      if( s1 <= s2 ) { 
        tmp = s1/s2;
        s = sqrt( ONE + tmp*tmp );
        sestpr = s2*s;
        c = (gamma/s2)/s;
        s = sign( ONE, alpha )/s;
      }
      else { 
        tmp = s2/s1;
        c = sqrt( ONE + tmp*tmp );
        sestpr = s1*c;
        s = (alpha/s1)/c;
        c = sign( ONE, gamma )/c;
      }
      return;
    }
    else { 
      
      //           normal case
      
      zeta1 = alpha/absest;
      zeta2 = gamma/absest;
      
      b = (ONE - zeta1*zeta1 - zeta2*zeta2)*HALF;
      c = zeta1*zeta1;
      if( b > ZERO ) { 
        t = c/(b + sqrt( b*b + c ));
      }
      else { 
        t = sqrt( b*b + c ) - b;
      }
      
      sine = -zeta1/t;
      cosine = -zeta2/(ONE + t);
      tmp = sqrt( sine*sine + cosine*cosine );
      s = sine/tmp;
      c = cosine/tmp;
      sestpr = sqrt( t + ONE )*absest;
      return;
    }
    
  }
  else if( job == 2 ) { 
    
    //        Estimating smallest singular value
    
    //        special cases
    
    if( sest == ZERO ) { 
      sestpr = ZERO;
      if( max( absgam, absalp ) == ZERO ) { 
        sine = ONE;
        cosine = ZERO;
      }
      else { 
        sine = -gamma;
        cosine = alpha;
      }
      s1 = max( abs( sine ), abs( cosine ) );
      s = sine/s1;
      c = cosine/s1;
      tmp = sqrt( s*s + c*c );
      s = s/tmp;
      c = c/tmp;
      return;
    }
    else if( absgam <= eps*absest ) { 
      s = ZERO;
      c = ONE;
      sestpr = absgam;
      return;
    }
    else if( absalp <= eps*absest ) { 
      s1 = absgam;
      s2 = absest;
      if( s1 <= s2 ) { 
        s = ZERO;
        c = ONE;
        sestpr = s1;
      }
      else { 
        s = ONE;
        c = ZERO;
        sestpr = s2;
      }
      return;
    }
    else if( absest <= eps*absalp || absest <= eps*absgam ) { 
      s1 = absgam;
      s2 = absalp;
      if( s1 <= s2 ) { 
        tmp = s1/s2;
        c = sqrt( ONE + tmp*tmp );
        sestpr = absest*(tmp/c);
        s = -(gamma/s2)/c;
        c = sign( ONE, alpha )/c;
      }
      else { 
        tmp = s2/s1;
        s = sqrt( ONE + tmp*tmp );
        sestpr = absest/s;
        c = (alpha/s1)/s;
        s = -sign( ONE, gamma )/s;
      }
      return;
    }
    else { 
      
      //           normal case
      
      zeta1 = alpha/absest;
      zeta2 = gamma/absest;
      
      norma = max( ONE + zeta1*zeta1 + abs( zeta1*zeta2 ), abs( zeta1*
       zeta2 ) + zeta2*zeta2 );
      
      //           See if root is closer to zero or to ONE
      
      test = ONE + TWO*(zeta1 - zeta2)*(zeta1 + zeta2);
      if( test >= ZERO ) { 
        
        //              root is close to zero, compute directly
        
        b = (zeta1*zeta1 + zeta2*zeta2 + ONE)*HALF;
        c = zeta2*zeta2;
        t = c/(b + sqrt( abs( b*b - c ) ));
        sine = zeta1/(ONE - t);
        cosine = -zeta2/t;
        sestpr = sqrt( t + FOUR*eps*eps*norma )*absest;
      }
      else { 
        
        //              root is closer to ONE, shift by that amount
        
        b = (zeta2*zeta2 + zeta1*zeta1 - ONE)*HALF;
        c = zeta1*zeta1;
        if( b >= ZERO ) { 
          t = -c/(b + sqrt( b*b + c ));
        }
        else { 
          t = b - sqrt( b*b + c );
        }
        sine = -zeta1/t;
        cosine = -zeta2/(ONE + t);
        sestpr = sqrt( ONE + t + FOUR*eps*eps*norma )*absest;
      }
      tmp = sqrt( sine*sine + cosine*cosine );
      s = sine/tmp;
      c = cosine/tmp;
      return;
      
    }
  }
  return;
  
  //     End of DLAIC1
  
} // end of function 


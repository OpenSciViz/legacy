/*
 * C++ implementation of Lapack routine zhetrf
 *
 * $Id: zhetrf.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:47:36
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zhetrf.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

RWLAPKDECL void /*FUNCTION*/ zhetrf(const char &uplo, const long &n, DComplex *a, const long &lda, 
   long ipiv[], DComplex work[], const long &lwork, long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  char _c0[2];
  int upper;
  long _do0, iinfo, iws, j, j_, k, kb, ldwork, nb, nbmin;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZHETRF computes the factorization of a DComplex Hermitian matrix A
  //  using the Bunch-Kaufman diagonal pivoting method:
  
  //     A = U*D*U'  or  A = L*D*L'
  
  //  where U (or L) is a product of permutation and unit upper (lower)
  //  triangular matrices, U' is the conjugate transpose of U, and D is
  //  Hermitian and block diagonal with 1-by-1 and 2-by-2 diagonal blocks.
  
  //  This is the blocked version of the algorithm, calling Level 3 BLAS.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangular part of the
  //          Hermitian matrix A is stored:
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  A       (input/output) COMPLEX*16 array, dimension (LDA,N)
  //          On entry, the Hermitian matrix A.  If UPLO = 'U', the leading
  //          n-by-n upper triangular part of A contains the upper
  //          triangular part of the matrix A, and the strictly lower
  //          triangular part of A is not referenced.  If UPLO = 'L', the
  //          leading n-by-n lower triangular part of A contains the lower
  //          triangular part of the matrix A, and the strictly upper
  //          triangular part of A is not referenced.
  
  //          On exit, the block diagonal matrix D and the multipliers used
  //          to obtain the factor U or L (see below for further details).
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  IPIV    (output) INTEGER array, dimension (N)
  //          Details of the interchanges and the block structure of D.
  //          If IPIV(k) > 0, then rows and columns k and IPIV(k) were
  //          interchanged and D(k,k) is a 1-by-1 diagonal block.
  //          If UPLO = 'U' and IPIV(k) = IPIV(k-1) < 0, then rows and
  //          columns k-1 and -IPIV(k) were interchanged and D(k-1:k,k-1:k)
  //          is a 2-by-2 diagonal block.  If UPLO = 'L' and IPIV(k) =
  //          IPIV(k+1) < 0, then rows and columns k+1 and -IPIV(k) were
  //          interchanged and D(k:k+1,k:k+1) is a 2-by-2 diagonal block.
  
  //  WORK    (workspace) COMPLEX*16 array, dimension (LWORK)
  //          If INFO returns 0, then WORK(1) returns N*NB, the minimum
  //          value of LWORK required to use the optimal blocksize.
  
  //  LWORK   (input) INTEGER
  //          The length of WORK.  LWORK should be >= N*NB, where NB is the
  //          blocksize returned by ILAENV.
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: if INFO = k, D(k,k) is exactly zero.  The factorization
  //               has been completed, but the block diagonal matrix D is
  //               exactly singular, and division by zero will occur if it
  //               is used to solve a system of equations.
  
  //  Further Details
  //  ===============
  
  //  If UPLO = 'U', then A = U*D*U', where
  //     U = P(n)*U(n)* ... *P(k)U(k)* ...,
  //  i.e., U is a product of terms P(k)*U(k), where k decreases from n to
  //  1 in steps of 1 or 2, and D is a block diagonal matrix with 1-by-1
  //  and 2-by-2 diagonal blocks D(k).  P(k) is a permutation matrix as
  //  defined by IPIV(k), and U(k) is a unit upper triangular matrix, such
  //  that if the diagonal block D(k) is of order s (s = 1 or 2), then
  
  //             (   I    v    0   )   k-s
  //     U(k) =  (   0    I    0   )   s
  //             (   0    0    I   )   n-k
  //                k-s   s   n-k
  
  //  If s = 1, D(k) overwrites A(k,k), and v overwrites A(1:k-1,k).
  //  If s = 2, the upper triangle of D(k) overwrites A(k-1,k-1), A(k-1,k),
  //  and A(k,k), and v overwrites A(1:k-2,k-1:k).
  
  //  If UPLO = 'L', then A = L*D*L', where
  //     L = P(1)*L(1)* ... *P(k)*L(k)* ...,
  //  i.e., L is a product of terms P(k)*L(k), where k increases from 1 to
  //  n in steps of 1 or 2, and D is a block diagonal matrix with 1-by-1
  //  and 2-by-2 diagonal blocks D(k).  P(k) is a permutation matrix as
  //  defined by IPIV(k), and L(k) is a unit lower triangular matrix, such
  //  that if the diagonal block D(k) is of order s (s = 1 or 2), then
  
  //             (   I    0     0   )  k-1
  //     L(k) =  (   0    I     0   )  s
  //             (   0    v     I   )  n-k-s+1
  //                k-1   s  n-k-s+1
  
  //  If s = 1, D(k) overwrites A(k,k), and v overwrites A(k+1:n,k).
  //  If s = 2, the lower triangle of D(k) overwrites A(k,k), A(k+1,k),
  //  and A(k+1,k+1), and v overwrites A(k+2:n,k:k+1).
  
  //  =====================================================================
  
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( lda < max( 1, n ) ) { 
    info = -4;
  }
  else if( lwork < 1 ) { 
    info = -7;
  }
  if( info != 0 ) { 
    xerbla( "ZHETRF", -info );
    return;
  }
  
  //     Determine the block size
  
  nb = ilaenv( 1, "ZHETRF", STR1(_c0,uplo), n, -1, -1, -1 );
  nbmin = 2;
  ldwork = n;
  if( nb > 1 && nb < n ) { 
    iws = ldwork*nb;
    if( lwork < iws ) { 
      nb = max( lwork/ldwork, 1 );
      nbmin = max( 2, ilaenv( 2, "ZHETRF", STR1(_c0,uplo), n, 
       -1, -1, -1 ) );
    }
  }
  else { 
    iws = 1;
  }
  if( nb < nbmin ) 
    nb = n;
  
  if( upper ) { 
    
    //        Factorize A as U*D*U' using the upper triangle of A
    
    //        K is the main loop index, decreasing from N to 1 in steps of
    //        KB, where KB is the number of columns factorized by ZLAHEF;
    //        KB is either NB or NB-1, or K for the last block
    
    k = n;
L_10:
    ;
    
    //        If K < 1, exit from loop
    
    if( k < 1 ) 
      goto L_40;
    
    if( k > nb ) { 
      
      //           Factorize columns k-kb+1:k of A and use blocked code to
      //           update columns 1:k-kb
      
      zlahef( uplo, k, nb, kb, a, lda, ipiv, work, n, iinfo );
    }
    else { 
      
      //           Use unblocked code to factorize columns 1:k of A
      
      zhetf2( uplo, k, a, lda, ipiv, iinfo );
      kb = k;
    }
    
    //        Set INFO on the first occurrence of a zero pivot
    
    if( info == 0 && iinfo > 0 ) 
      info = iinfo;
    
    //        Decrease K and return to the start of the main loop
    
    k = k - kb;
    goto L_10;
    
  }
  else { 
    
    //        Factorize A as L*D*L' using the lower triangle of A
    
    //        K is the main loop index, increasing from 1 to N in steps of
    //        KB, where KB is the number of columns factorized by ZLAHEF;
    //        KB is either NB or NB-1, or N-K+1 for the last block
    
    k = 1;
L_20:
    ;
    
    //        If K > N, exit from loop
    
    if( k > n ) 
      goto L_40;
    
    if( k <= n - nb ) { 
      
      //           Factorize columns k:k+kb-1 of A and use blocked code to
      //           update columns k+kb:n
      
      zlahef( uplo, n - k + 1, nb, kb, &A(k - 1,k - 1), lda, 
       &ipiv[k - 1], work, n, iinfo );
    }
    else { 
      
      //           Use unblocked code to factorize columns k:n of A
      
      zhetf2( uplo, n - k + 1, &A(k - 1,k - 1), lda, &ipiv[k - 1], 
       iinfo );
      kb = n - k + 1;
    }
    
    //        Set INFO on the first occurrence of a zero pivot
    
    if( info == 0 && iinfo > 0 ) 
      info = iinfo + k - 1;
    
    //        Adjust IPIV
    
    for( j = k, j_ = j - 1, _do0 = k + kb - 1; j <= _do0; j++, j_++ ) { 
      if( ipiv[j_] > 0 ) { 
        ipiv[j_] = ipiv[j_] + k - 1;
      }
      else { 
        ipiv[j_] = ipiv[j_] - k + 1;
      }
    }
    
    //        Increase K and return to the start of the main loop
    
    k = k + kb;
    goto L_20;
    
  }
  
L_40:
  ;
  work[0] = DComplex((double)iws);
  return;
  
  //     End of ZHETRF
  
#undef  A
} // end of function 


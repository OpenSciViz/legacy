/*
 * C++ implementation of lapack routine dgbsvx
 *
 * $Id: dgbsvx.cpp,v 1.6 1993/04/06 20:40:20 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:33:42
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dgbsvx.cpp,v $
 * Revision 1.6  1993/04/06  20:40:20  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.5  1993/03/19  18:41:23  alv
 * now passes chars explicitly, rather than indirection of a string, to shut up SUN warnings
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:14:25  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:06:26  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dgbsvx(const char &fact, const char &trans, long &n, long &kl, 
 long &ku, long &nrhs, double *ab, long &ldab, double *afb, 
 long &ldafb, long ipiv[], char &equed, double r[], double c[], 
 double *b, long &ldb, double *x, long &ldx, double &rcond, 
 double ferr[], double berr[], double work[], long iwork[], long &info)
{
#define AB(I_,J_) (*(ab+(I_)*(ldab)+(J_)))
#define AFB(I_,J_)  (*(afb+(I_)*(ldafb)+(J_)))
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
#define X(I_,J_)  (*(x+(I_)*(ldx)+(J_)))
  int colequ, equil, nofact, notran, rowequ;
  char norm;
  long _do0, _do1, _do10, _do2, _do3, _do4, _do5, _do6, _do7, 
   _do8, _do9, i, i_, infequ, j, j1, j2, j_;
  double amax, anorm, colcnd, rowcnd;

  
  //  -- LAPACK driver routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DGBSVX uses the LU factorization to compute the solution to a real
  //  system of linear equations
  //     A * X = B,
  //  where A is a band matrix of order N with KL subdiagonals and KU
  //  superdiagonals, and X and B are N by NRHS matrices.
  
  //  Error bounds on the solution and a condition estimate are also
  //  provided.
  
  //  Description
  //  ===========
  
  //  The following steps are performed by this subroutine:
  
  //  1. If FACT = 'E', real scaling factors are computed to equilibrate
  //     the system:
  //        TRANS = 'N':  diag(R)*A*diag(C)     *inv(diag(C))*X = diag(R)*B
  //        TRANS = 'T': (diag(R)*A*diag(C))**T *inv(diag(R))*X = diag(C)*B
  //        TRANS = 'C': (diag(R)*A*diag(C))**H *inv(diag(R))*X = diag(C)*B
  //     Whether or not the system will be equilibrated depends on the
  //     scaling of the matrix A, but if equilibration is used, A is
  //     overwritten by diag(R)*A*diag(C) and B by diag(R)*B (if TRANS='N')
  //     or diag(C)*B (if TRANS = 'T' or 'C').
  
  //  2. If FACT = 'N' or 'E', the LU decomposition is used to factor the
  //     matrix A (after equilibration if FACT = 'E') as
  //        A = L * U,
  //     where L is a product of permutation and unit lower triangular
  //     matrices with KL subdiagonals, and U is upper triangular with
  //     KL+KU superdiagonals.
  
  //  3. The factored form of A is used to estimate the condition number
  //     of the matrix A.  If the reciprocal of the condition number is
  //     less than machine precision, steps 4-6 are skipped.
  
  //  4. The system of equations A*X = B is solved for X using the
  //     factored form of A.
  
  //  5. Iterative refinement is applied to improve the computed solution
  //     vectors and calculate error bounds and backward error estimates
  //     for them.
  
  //  6. If FACT = 'E' and equilibration was used, the vectors X are
  //     premultiplied by diag(C) (if TRANS = 'N') or diag(R) (if
  //     TRANS = 'T' or 'C') so that they solve the original system before
  //     equilibration.
  
  //  Arguments
  //  =========
  
  //  FACT    (input) CHARACTER*1
  //          Specifies whether or not the factored form of the matrix A is
  //          supplied on entry, and if not, whether the matrix A should be
  //          equilibrated before it is factored.
  //          = 'F':  On entry, AFB and IPIV contain the factored form of
  //                  A.  AB, AFB, and IPIV are not modified.
  //          = 'N':  The matrix A will be copied to AFB and factored.
  //          = 'E':  The matrix A will be equilibrated if necessary, then
  //                  copied to AFB and factored.
  
  //  TRANS   (input) CHARACTER*1
  //          Specifies the form of the system of equations.
  //          = 'N':  A * X = B     (No transpose)
  //          = 'T':  A**T * X = B  (Transpose)
  //          = 'C':  A**H * X = B  (Transpose)
  
  //  N       (input) INTEGER
  //          The number of linear equations, i.e., the order of the
  //          matrix A.  N >= 0.
  
  //  KL      (input) INTEGER
  //          The number of subdiagonals within the band of A.  KL >= 0.
  
  //  KU      (input) INTEGER
  //          The number of superdiagonals within the band of A.  KU >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right hand sides, i.e., the number of columns
  //          of the matrices B and X.  NRHS >= 0.
  
  //  AB      (input/output) DOUBLE PRECISION array, dimension (LDAB,N)
  //          On entry, the matrix A in band storage, in rows 1 to KL+KU+1.
  //          The j-th column of A is stored in the j-th column of the
  //          array AB as follows:
  //          AB(KU+1+i-j,j) = A(i,j) for max(1,j-KU)<=i<=min(N,j+kl)
  
  //          On exit, if EQUED .ne. 'N', A is scaled as follows:
  //          EQUED = 'R':  A := diag(R) * A
  //          EQUED = 'C':  A := A * diag(C)
  //          EQUED = 'B':  A := diag(R) * A * diag(C).
  
  //  LDAB    (input) INTEGER
  //          The leading dimension of the array AB.  LDAB >= KL+KU+1.
  
  //  AFB     (input or output) DOUBLE PRECISION array, dimension (LDAFB,N)
  //          If FACT = 'F', then AFB is an input argument and on entry
  //          contains details of the LU factorization of the band matrix
  //          A, as computed by DGBTRF.  U is stored as an upper triangular
  //          band matrix with KL+KU superdiagonals in rows 1 to KL+KU+1,
  //          and the multipliers used during the factorization are stored
  //          in rows KL+KU+2 to 2*KL+KU+1.
  
  //          If FACT = 'N', then AFB is an output argument and on exit
  //          returns details of the LU factorization of A.
  
  //          If FACT = 'E', then AFB is an output argument and on exit
  //          returns details of the LU factorization of the equilibrated
  //          matrix A (see the description of AB for the form of the
  //          equilibrated matrix).
  
  //  LDAFB   (input) INTEGER
  //          The leading dimension of the array AFB.  LDAFB >= 2*KL+KU+1.
  
  //  IPIV    (input or output) INTEGER array, dimension (N)
  //          If FACT = 'F', then IPIV is an input argument and on entry
  //          contains the pivot indices from the factorization A = L*U
  //          as computed by DGBTRF; row i of the matrix was interchanged
  //          with row IPIV(i).
  
  //          If FACT = 'N', then IPIV is an output argument and on exit
  //          contains the pivot indices from the factorization A = L*U
  //          of the original matrix A.
  
  //          If FACT = 'E', then IPIV is an output argument and on exit
  //          contains the pivot indices from the factorization A = L*U
  //          of the equilibrated matrix A.
  
  //  EQUED   (output) CHARACTER*1
  //          Specifies the form of equilibration that was done.
  //          = 'N':  No equilibration (always true if FACT = 'F' or 'N').
  //          = 'R':  Row equilibration, i.e., A has been premultiplied by
  //                  diag(R).
  //          = 'C':  Column equilibration, i.e., A has been postmultiplied
  //                  by diag(C).
  //          = 'B':  Both row and column equilibration, i.e., A has been
  //                  replaced by diag(R) * A * diag(C).
  
  //  R       (output) DOUBLE PRECISION array, dimension (N)
  //          The row scale factors for A.  If EQUED = 'R' or 'B', A is
  //          multiplied on the left by diag(R).  R is not assigned if
  //          FACT = 'F' or 'N'.
  
  //  C       (output) DOUBLE PRECISION array, dimension (N)
  //          The column scale factors for A.  If EQUED = 'C' or 'B', A is
  //          multiplied on the right by diag(C). C is not assigned if
  //          FACT = 'F' or 'N'.
  
  //  B       (input/output) DOUBLE PRECISION array, dimension (LDB,NRHS)
  //          On entry, the right-hand side matrix B.
  //          On exit, if EQUED = 'N', B is not modified; if TRANS = 'N'
  //          and EQUED = 'R' or 'B', B is overwritten by diag(R)*B; if
  //          TRANS = 'T' or 'C' and EQUED = 'C' or 'B', B is overwritten
  //          by diag(C)*B.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  X       (output) DOUBLE PRECISION array, dimension (LDX,NRHS)
  //          If INFO = 0, the n-by-nrhs solution matrix X to the original
  //          system of equations.  Note that A and B are modified on exit
  //          if EQUED .ne. 'N', and the solution to the equilibrated
  //          system is inv(diag(C))*X if TRANS = 'N' and EQUED = 'C' or
  //          'B', or inv(diag(R))*X if TRANS = 'T' or 'C' and EQUED = 'R'
  //          or 'B'.
  
  //  LDX     (input) INTEGER
  //          The leading dimension of the array X.  LDX >= max(1,N).
  
  //  RCOND   (output) DOUBLE PRECISION
  //          The estimate of the reciprocal condition number of the matrix
  //          A.  If RCOND is less than the machine precision (in
  //          particular, if RCOND = 0), the matrix is singular to working
  //          precision.  This condition is indicated by a return code of
  //          INFO > 0, and the solution and error bounds are not computed.
  
  //  FERR    (output) DOUBLE PRECISION array, dimension (NRHS)
  //          The estimated forward error bounds for each solution vector
  //          X(j) (the j-th column of the solution matrix X).
  //          If XTRUE is the true solution, FERR(j) bounds the magnitude
  //          of the largest entry in (X(j) - XTRUE) divided by the
  //          magnitude of the largest entry in X(j).  The quality of the
  //          error bound depends on the quality of the estimate of
  //          norm(inv(A)) computed in the code; if the estimate of
  //          norm(inv(A)) is accurate, the error bound is guaranteed.
  
  //  BERR    (output) DOUBLE PRECISION array, dimension (NRHS)
  //          The componentwise relative backward error of each solution
  //          vector X(j) (i.e., the smallest relative change in
  //          any entry of A or B that makes X(j) an exact solution).
  
  //  WORK    (workspace) DOUBLE PRECISION array, dimension (3*N)
  
  //  IWORK   (workspace) INTEGER array, dimension (N)
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: if INFO = k <= N, U(k,k) is exactly zero, and if
  //               INFO = N+1, the factor U is nonsingular, but RCOND is
  //               less than machine precision.  The factorization has been
  //               completed, but the matrix A is singular to working
  //               precision, and the solution and error bounds have not
  //               been computed.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  nofact = lsame( fact, 'N' );
  equil = lsame( fact, 'E' );
  notran = lsame( trans, 'N' );
  if( (!nofact && !equil) && !lsame( fact, 'F' ) ) { 
    info = -1;
  }
  else if( (!notran && !lsame( trans, 'T' )) && !lsame( trans, 'C' )
    ) { 
    info = -2;
  }
  else if( n < 0 ) { 
    info = -3;
  }
  else if( kl < 0 ) { 
    info = -4;
  }
  else if( ku < 0 ) { 
    info = -5;
  }
  else if( nrhs < 0 ) { 
    info = -6;
  }
  else if( ldab < kl + ku + 1 ) { 
    info = -8;
  }
  else if( ldafb < 2*kl + ku + 1 ) { 
    info = -10;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -16;
  }
  else if( ldx < max( 1, n ) ) { 
    info = -18;
  }
  if( info != 0 ) { 
    xerbla( "DGBSVX", -info );
    return;
  }
  
  equed = 'N';
  rowequ = FALSE;
  colequ = FALSE;
  if( equil ) { 
    
    //        Compute row and column scalings to equilibrate the matrix A.
    
    dgbequ( n, n, kl, ku, ab, ldab, r, c, rowcnd, colcnd, amax, 
     infequ );
    if( infequ == 0 ) { 
      
      //           Equilibrate the matrix.
      
      dlaqgb( n, n, kl, ku, ab, ldab, r, c, rowcnd, colcnd, 
       amax, equed );
      rowequ = lsame( equed, 'R' ) || lsame( equed, 'B' );
      colequ = lsame( equed, 'C' ) || lsame( equed, 'B' );
      
      //           Scale the right hand side.
      
      if( notran ) { 
        if( rowequ ) { 
          for( j = 1, j_ = j - 1, _do0 = nrhs; j <= _do0; j++, j_++ ) { 
            for( i = 1, i_ = i - 1, _do1 = n; i <= _do1; i++, i_++ ) { 
              B(j_,i_) = r[i_]*B(j_,i_);
            }
          }
        }
      }
      else if( colequ ) { 
        for( j = 1, j_ = j - 1, _do2 = nrhs; j <= _do2; j++, j_++ ) { 
          for( i = 1, i_ = i - 1, _do3 = n; i <= _do3; i++, i_++ ) { 
            B(j_,i_) = c[i_]*B(j_,i_);
          }
        }
      }
    }
  }
  
  if( nofact || equil ) { 
    
    //        Compute the LU factorization of the band matrix A.
    
    for( j = 1, j_ = j - 1, _do4 = n; j <= _do4; j++, j_++ ) { 
      j1 = max( j - ku, 1 );
      j2 = min( j + kl, n );
      dcopy( j2 - j1 + 1, &AB(j_,ku - j + j1), 1, &AFB(j_,kl + ku - j + j1), 
       1 );
    }
    
    dgbtrf( n, n, kl, ku, afb, ldafb, ipiv, info );
    
    //        Return if INFO is non-zero.
    
    if( info != 0 ) { 
      if( info > 0 ) 
        rcond = ZERO;
      return;
    }
  }
  
  //     Compute the norm of the matrix A.
  
  if( notran ) { 
    norm = '1';
  }
  else { 
    norm = 'I';
  }
  anorm = dlangb( norm, n, kl, ku, ab, ldab, work );
  
  //     Compute the reciprocal of the condition number of A.
  
  dgbcon( norm, n, kl, ku, afb, ldafb, ipiv, anorm, rcond, work, 
   iwork, info );
  
  //     Return if the matrix is singular to working precision.
  
  if( rcond < dlamch( 'E'/* Epsilon */ ) ) { 
    info = n + 1;
    return;
  }
  
  //     Compute the solution vectors X.
  
  dlacpy( 'F'/* Full */, n, nrhs, b, ldb, x, ldx );
  dgbtrs( trans, n, kl, ku, nrhs, afb, ldafb, ipiv, x, ldx, info );
  
  //     Use iterative refinement to improve the computed solutions and
  //     compute error bounds and backward error estimates for them.
  
  dgbrfs( trans, n, kl, ku, nrhs, ab, ldab, afb, ldafb, ipiv, b, 
   ldb, x, ldx, ferr, berr, work, iwork, info );
  
  //     Transform the solution vectors X to solutions of the original
  //     system.
  
  if( equil ) { 
    if( notran ) { 
      if( colequ ) { 
        for( j = 1, j_ = j - 1, _do5 = nrhs; j <= _do5; j++, j_++ ) { 
          for( i = 1, i_ = i - 1, _do6 = n; i <= _do6; i++, i_++ ) { 
            X(j_,i_) = c[i_]*X(j_,i_);
          }
        }
        for( j = 1, j_ = j - 1, _do7 = nrhs; j <= _do7; j++, j_++ ) { 
          ferr[j_] = ferr[j_]/colcnd;
        }
      }
    }
    else if( rowequ ) { 
      for( j = 1, j_ = j - 1, _do8 = nrhs; j <= _do8; j++, j_++ ) { 
        for( i = 1, i_ = i - 1, _do9 = n; i <= _do9; i++, i_++ ) { 
          X(j_,i_) = r[i_]*X(j_,i_);
        }
      }
      for( j = 1, j_ = j - 1, _do10 = nrhs; j <= _do10; j++, j_++ ) { 
        ferr[j_] = ferr[j_]/rowcnd;
      }
    }
  }
  
  return;
  
  //     End of DGBSVX
  
#undef  X
#undef  B
#undef  AFB
#undef  AB
} // end of function 


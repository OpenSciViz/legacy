/*
 * C++ implementation of lapack routine dlagtf
 *
 * $Id: dlagtf.cpp,v 1.6 1993/04/06 20:40:57 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:35:10
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dlagtf.cpp,v $
 * Revision 1.6  1993/04/06  20:40:57  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.5  1993/03/19  18:41:23  alv
 * now passes chars explicitly, rather than indirection of a string, to shut up SUN warnings
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:15:13  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:07:10  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dlagtf(const long &n, double a[], const double &lambda, 
 double b[], double c[], const double &tol, double d[], long in[], long &info)
{
  long _do0, k, k_;
  double eps, mult, piv1, piv2, scale1, scale2, temp, tl;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DLAGTF factorizes the matrix (T - lambda*I), where T is an n by n
  //  tridiagonal matrix and lambda is a scalar, as
  
  //     T - lambda*I = PLU,
  
  //  where P is a permutation matrix, L is a unit lower tridiagonal matrix
  //  with at most one non-zero sub-diagonal elements per column and U is
  //  an upper triangular matrix with at most two non-zero super-diagonal
  //  elements per column.
  
  //  The factorization is obtained by Gaussian elimination with partial
  //  pivoting and implicit row scaling.
  
  //  The parameter LAMBDA is included in the routine so that DLAGTF may
  //  be used, in conjunction with DLAGTS, to obtain eigenvectors of T by
  //  inverse iteration.
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The order of the matrix T.
  
  //  A       (input/output) DOUBLE PRECISION array, dimension (N)
  //          On entry, A must contain the diagonal elements of T.
  
  //          On exit, A is overwritten by the n diagonal elements of the
  //          upper triangular matrix U of the factorization of T.
  
  //  LAMBDA  (input) DOUBLE PRECISION
  //          On entry, the scalar lambda.
  
  //  B       (input/output) DOUBLE PRECISION array, dimension (N-1)
  //          On entry, B must contain the (n-1) super-diagonal elements of
  //          T.
  
  //          On exit, B is overwritten by the (n-1) super-diagonal
  //          elements of the matrix U of the factorization of T.
  
  //  C       (input/output) DOUBLE PRECISION array, dimension (N-1)
  //          On entry, C must contain the (n-1) sub-diagonal elements of
  //          T.
  
  //          On exit, C is overwritten by the (n-1) sub-diagonal elements
  //          of the matrix L of the factorization of T.
  
  //  TOL     (input) DOUBLE PRECISION
  //          On entry, a relative tolerance used to indicate whether or
  //          not the matrix (T - lambda*I) is nearly singular. TOL should
  //          normally be chose as approximately the largest relative error
  //          in the elements of T. For example, if the elements of T are
  //          correct to about 4 significant figures, then TOL should be
  //          set to about 5*10**(-4). If TOL is supplied as less than eps,
  //          where eps is the relative machine precision, then the value
  //          eps is used in place of TOL.
  
  //  D       (output) DOUBLE PRECISION array, dimension (N-2)
  //          On exit, D is overwritten by the (n-2) second super-diagonal
  //          elements of the matrix U of the factorization of T.
  
  //  IN      (output) INTEGER array, dimension (N)
  //          On exit, IN contains details of the permutation matrix P. If
  //          an interchange occurred at the kth step of the elimination,
  //          then IN(k) = 1, otherwise IN(k) = 0. The element IN(n)
  //          returns the smallest positive integer j such that
  
  //             abs( u(j,j) ).le. norm( (T - lambda*I)(j) )*TOL,
  
  //          where norm( A(j) ) denotes the sum of the absolute values of
  //          the jth row of the matrix A. If no such j exists then IN(n)
  //          is returned as zero. If IN(n) is returned as positive, then a
  //          diagonal element of U is small, indicating that
  //          (T - lambda*I) is singular or nearly singular,
  
  //  INFO    (output)
  //          = 0   : successful exit
  //          .lt. 0: if INFO = -k, the kth argument had an illegal value
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Executable Statements ..
  
  info = 0;
  if( n < 0 ) { 
    info = -1;
    xerbla( "DLAGTF", -info );
    return;
  }
  
  if( n == 0 ) 
    return;
  
  a[0] = a[0] - lambda;
  in[n - 1] = 0;
  if( n == 1 ) { 
    if( a[0] == ZERO ) 
      in[0] = 1;
    return;
  }
  
  eps = dlamch( 'E'/* Epsilon */ );
  
  tl = max( tol, eps );
  scale1 = abs( a[0] ) + abs( b[0] );
  for( k = 1, k_ = k - 1, _do0 = n - 1; k <= _do0; k++, k_++ ) { 
    a[k_ + 1] = a[k_ + 1] - lambda;
    scale2 = abs( c[k_] ) + abs( a[k_ + 1] );
    if( k < (n - 1) ) 
      scale2 = scale2 + abs( b[k_ + 1] );
    if( a[k_] == ZERO ) { 
      piv1 = ZERO;
    }
    else { 
      piv1 = abs( a[k_] )/scale1;
    }
    if( c[k_] == ZERO ) { 
      in[k_] = 0;
      piv2 = ZERO;
      scale1 = scale2;
      if( k < (n - 1) ) 
        d[k_] = ZERO;
    }
    else { 
      piv2 = abs( c[k_] )/scale2;
      if( piv2 <= piv1 ) { 
        in[k_] = 0;
        scale1 = scale2;
        c[k_] = c[k_]/a[k_];
        a[k_ + 1] = a[k_ + 1] - c[k_]*b[k_];
        if( k < (n - 1) ) 
          d[k_] = ZERO;
      }
      else { 
        in[k_] = 1;
        mult = a[k_]/c[k_];
        a[k_] = c[k_];
        temp = a[k_ + 1];
        a[k_ + 1] = b[k_] - mult*temp;
        if( k < (n - 1) ) { 
          d[k_] = b[k_ + 1];
          b[k_ + 1] = -mult*d[k_];
        }
        b[k_] = temp;
        c[k_] = mult;
      }
    }
    if( (max( piv1, piv2 ) <= tl) && (in[n - 1] == 0) ) 
      in[n - 1] = k;
  }
  if( (abs( a[n - 1] ) <= scale1*tl) && (in[n - 1] == 0) ) 
    in[n - 1] = n;
  
  return;
  
  //     End of DLAGTF
  
} // end of function 


/*
 * C++ implementation of Lapack routine slarf
 *
 * $Id: slarf.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:00:20
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: slarf.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ONE = 1.0e0;
const float ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ slarf(const char &side, const long &m, const long &n, float v[], 
 const long &incv, const float &tau, float *c, const long &ldc, float work[])
{
#define C(I_,J_)  (*(c+(I_)*(ldc)+(J_)))

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SLARF applies a real elementary reflector H to a real m by n matrix
  //  C, from either the left or the right. H is represented in the form
  
  //        H = I - tau * v * v'
  
  //  where tau is a real scalar and v is a real vector.
  
  //  If tau = 0, then H is taken to be the unit matrix.
  
  //  Arguments
  //  =========
  
  //  SIDE    (input) CHARACTER*1
  //          = 'L': form  H * C
  //          = 'R': form  C * H
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix C.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix C.
  
  //  V       (input) REAL array, dimension
  //                     (1 + (M-1)*abs(INCV)) if SIDE = 'L'
  //                  or (1 + (N-1)*abs(INCV)) if SIDE = 'R'
  //          The vector v in the representation of H. V is not used if
  //          TAU = 0.
  
  //  INCV    (input) INTEGER
  //          The increment between elements of v. INCV <> 0.
  
  //  TAU     (input) REAL
  //          The value tau in the representation of H.
  
  //  C       (input/output) REAL array, dimension (LDC,N)
  //          On entry, the m by n matrix C.
  //          On exit, C is overwritten by the matrix H * C if SIDE = 'L',
  //          or C * H if SIDE = 'R'.
  
  //  LDC     (input) INTEGER
  //          The leading dimension of the array C. LDC >= max(1,M).
  
  //  WORK    (workspace) REAL array, dimension
  //                         (N) if SIDE = 'L'
  //                      or (M) if SIDE = 'R'
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Executable Statements ..
  
  if( lsame( side, 'L' ) ) { 
    
    //        Form  H * C
    
    if( tau != ZERO ) { 
      
      //           w := C' * v
      
      sgemv( 'T'/*Transpose*/, m, n, ONE, c, ldc, v, incv, 
       ZERO, work, 1 );
      
      //           C := C - v * w'
      
      sger( m, n, -tau, v, incv, work, 1, c, ldc );
    }
  }
  else { 
    
    //        Form  C * H
    
    if( tau != ZERO ) { 
      
      //           w := C * v
      
      sgemv( 'N'/*No transpose*/, m, n, ONE, c, ldc, v, incv, 
       ZERO, work, 1 );
      
      //           C := C - w * v'
      
      sger( m, n, -tau, work, 1, v, incv, c, ldc );
    }
  }
  return;
  
  //     End of SLARF
  
#undef  C
} // end of function 


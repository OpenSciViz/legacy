/*
 * C++ implementation of lapack routine dsecnd
 *
 * $Id: dsecnd.cpp,v 1.3 1993/04/06 20:42:16 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:37:54
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dsecnd.cpp,v $
 * Revision 1.3  1993/04/06  20:42:16  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.2  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.1  1993/03/03  16:08:45  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

RWLAPKDECL double /*FUNCTION*/ dsecnd()
{
  float etime(), t1, tarray[2];
  double dsecnd_v;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //  Purpose
  //  =======
  
  //  DSECND returns the user time for a process in seconds.
  //  This version gets the time from the system function ETIME.
  
  //     .. Local Scalars ..
  //     ..
  //     .. Local Arrays ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Executable Statements ..
  
  t1 = etime( tarray );
  dsecnd_v = tarray[0];
  return( dsecnd_v );
  
  //     End of DSECND
  
} // end of function 


/*
 * C++ implementation of lapack routine dlazro
 *
 * $Id: dlazro.cpp,v 1.4 1993/04/06 20:41:35 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:36:34
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dlazro.cpp,v $
 * Revision 1.4  1993/04/06  20:41:35  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.3  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.2  1993/03/05  23:16:07  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:07:53  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

RWLAPKDECL void /*FUNCTION*/ dlazro(const long &m, const long &n, const double &alpha, 
   const double &beta, double *a, const long &lda)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  long _do0, _do1, _do2, i, i_, j, j_;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DLAZRO initializes a 2-D array A to BETA on the diagonal and
  //  ALPHA on the offdiagonals.
  
  //  Arguments
  //  =========
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix A.  M >= 0.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix A.  N >= 0.
  
  //  ALPHA   (input) DOUBLE PRECISION
  //          The constant to which the offdiagonal elements are to be set.
  
  //  BETA    (input) DOUBLE PRECISION
  //          The constant to which the diagonal elements are to be set.
  
  //  A       (output) DOUBLE PRECISION array, dimension (LDA,N)
  //          On exit, the leading m by n submatrix of A is set such that
  //             A(i,j) = ALPHA,  1 <= i <= m, 1 <= j <= n, i <> j
  //             A(i,i) = BETA,   1 <= i <= min(m,n).
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,M).
  
  //     .. Local Scalars ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
    for( i = 1, i_ = i - 1, _do1 = m; i <= _do1; i++, i_++ ) { 
      A(j_,i_) = alpha;
    }
  }
  
  for( i = 1, i_ = i - 1, _do2 = min( m, n ); i <= _do2; i++, i_++ ) { 
    A(i_,i_) = beta;
  }
  
  return;
  
  //     End of DLAZRO
  
#undef  A
} // end of function 


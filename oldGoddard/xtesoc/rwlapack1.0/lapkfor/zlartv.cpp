/*
 * C++ implementation of Lapack routine zlartv
 *
 * $Id: zlartv.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:49:12
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zlartv.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

RWLAPKDECL void /*FUNCTION*/ zlartv(const long &n, DComplex x[], const long &incx, 
   DComplex y[], const long &incy, double c[], DComplex s[], const long &incc)
{
  long _do0, i, i_, ic, ix, iy;
  DComplex xi, yi;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZLARTV applies a vector of DComplex plane rotations with real cosines
  //  to elements of the DComplex vectors x and y. For i = 1,2,...,n
  
  //     ( x(i) ) := (        c(i)   s(i) ) ( x(i) )
  //     ( y(i) )    ( -conjg(s(i))  c(i) ) ( y(i) )
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The number of plane rotations to be applied.
  
  //  X       (input/output) COMPLEX*16 array, dimension (1+(N-1)*INCX)
  //          The vector x.
  
  //  INCX    (input) INTEGER
  //          The increment between elements of X. INCX > 0.
  
  //  Y       (input/output) COMPLEX*16 array, dimension (1+(N-1)*INCY)
  //          The vector y.
  
  //  INCY    (input) INTEGER
  //          The increment between elements of Y. INCY > 0.
  
  //  C       (input) DOUBLE PRECISION array, dimension (1+(N-1)*INCC)
  //          The cosines of the plane rotations.
  
  //  S       (input) COMPLEX*16 array, dimension (1+(N-1)*INCC)
  //          The sines of the plane rotations.
  
  //  INCC    (input) INTEGER
  //          The increment between elements of C and S. INCC > 0.
  
  //  =====================================================================
  
  //     .. Local Scalars ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  ix = 1;
  iy = 1;
  ic = 1;
  for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
    xi = x[ix - 1];
    yi = y[iy - 1];
    x[ix - 1] = c[ic - 1]*xi + s[ic - 1]*yi;
    y[iy - 1] = c[ic - 1]*yi - conj( s[ic - 1] )*xi;
    ix = ix + incx;
    iy = iy + incy;
    ic = ic + incc;
  }
  return;
  
  //     End of ZLARTV
  
} // end of function 


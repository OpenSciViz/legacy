/*
 * C++ implementation of Lapack routine sstein
 *
 * $Id: sstein.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:02:44
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: sstein.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ZERO = 0.0;
const float ONE = 1.0;
const float TEN = 1.0e1;
const float ODM3 = 1.0e-3;
const float ODM1 = 1.0e-1;
const long MAXITS = 5;
const long EXTRA = 2;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ sstein(const long &n, float d[], float e[], const long &m, 
 float w[], long iblock[], long isplit[], float *z, const long &ldz, 
 float work[], long iwork[], long ifail[], long &info)
{
#define Z(I_,J_)  (*(z+(I_)*(ldz)+(J_)))
  long _do0, _do1, _do2, _do3, _do4, _do5, _do6, b1, blksiz, 
   bn, gpind, i, i_, iinfo, indrv1, indrv2, indrv3, indrv4, indrv5, 
   iseed[4], its, j, j1, j_, jblk, jmax, nblk, nblk_, nrmchk;
  float ctr, eps, eps1, nrm, onenrm, ortol, pertol, scl, sep, stpcrt, 
   tol, xj, xjm;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SSTEIN computes the eigenvectors of a symmetric tridiagonal
  //  matrix corresponding to specified eigenvalues using inverse
  //  iteration.
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The number of rows and columns in the matrix.  N >= 0.
  
  //  D       (input) REAL array, dimension (N)
  //          The diagonal entries of the tridiagonal matrix.
  
  //  E       (input) REAL array, dimension (N)
  //          Contains the subdiagonal entries of the tridiagonal matrix
  //          in positions 1 through N-1.  E( N ) is arbitrary.
  
  //  M       (input) INTEGER
  //          The number of eigenvectors to be found.  0 <= M <= N.
  
  //  W       (input) REAL array, dimension (N)
  //          The first M elements of W contain the eigenvalues for
  //          which eigenvectors are to be computed.  The eigenvalues
  //          should be grouped by split-off block and ordered from
  //          smallest to largest within the block.  ( The output array
  //          W from SSTEBZ with ORDER = 'B' is expected here. )
  
  //  IBLOCK  (input) INTEGER array, dimension (N)
  //          The submatrix indices associated with the corresponding
  //          eigenvalues in W -- 1 for eigenvalues belonging to the
  //          first submatrix from the top, 2 for those belonging to
  //          the second submatrix, etc.  ( The output array IBLOCK
  //          from SSTEBZ is expected here. )
  
  //  ISPLIT  (input) INTEGER array, dimension (N)
  //          The splitting points, at which T breaks up into submatrices.
  //          The first submatrix consists of rows/columns 1 to
  //          ISPLIT( 1 ), the second of rows/columns ISPLIT( 1 )+1
  //          through ISPLIT( 2 ), etc.
  //          ( The output array ISPLIT from SSTEBZ is expected here. )
  
  //  Z       (output) REAL array, dimension (LDZ, M)
  //          On output, Z contains the orthogonal eigenvectors of
  //          associated with the specified eigenvalues.  Any vector
  //          which fails to converge is set to its value on the
  //          fifth iteration.
  
  //  LDZ     (input) INTEGER
  //          The leading dimension of the array Z.  LDZ >= max( 1, N ).
  
  //  WORK    (workspace) REAL array, dimension (5*N)
  
  //  IWORK   (workspace) INTEGER array, dimension (N)
  
  //  IFAIL   (output) INTEGER array, dimension (M)
  //          On normal exit, all elements of IFAIL are zero.
  //          If one or more eigenvectors fail to converge after
  //          MAXITS iterations, then their indices are stored in
  //          array IFAIL.
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit.
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: if INFO = +k, then k eigenvectors failed to converge
  //               in MAXITS iterations.  Their indices are stored in
  //               array IFAIL.
  
  //  Internal Parameters
  //  ===================
  
  //  MAXITS  INTEGER, default = 5
  //          The maximum number of iterations performed.
  
  //  EXTRA   INTEGER, default = 2
  //          The number of iterations performed after norm growth
  //          criterion is satisfied, should be at least 1.
  
  //-----------------------------------------------------------------------
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Local Arrays ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  for( i = 1, i_ = i - 1, _do0 = m; i <= _do0; i++, i_++ ) { 
    ifail[i_] = 0;
  }
  
  if( n < 0 ) { 
    info = -1;
  }
  else if( m < 0 || m > n ) { 
    info = -4;
  }
  else if( ldz < n ) { 
    info = -9;
  }
  
  if( info != 0 ) { 
    xerbla( "SSTEIN", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 || m == 0 ) { 
    return;
  }
  else if( n == 1 ) { 
    Z(0,0) = ONE;
    return;
  }
  
  //     Get machine constants.
  
  eps = slamch( 'P'/*Precision*/ );
  
  //     Initialize seed for random number generator SLARNV.
  
  for( i = 1, i_ = i - 1; i <= 4; i++, i_++ ) { 
    iseed[i_] = 1;
  }
  
  //     Initialize pointers.
  
  indrv1 = 0;
  indrv2 = indrv1 + n;
  indrv3 = indrv2 + n;
  indrv4 = indrv3 + n;
  indrv5 = indrv4 + n;
  
  //     Compute eigenvectors of matrix blocks.
  
  gpind = 1;
  j1 = 1;
  for( nblk = 1, nblk_ = nblk - 1, _do1 = iblock[m - 1]; nblk <= _do1; nblk++, nblk_++ ) { 
    
    //        Find starting and ending indices of block nblk.
    
    if( nblk == 1 ) { 
      b1 = 1;
    }
    else { 
      b1 = isplit[nblk_ - 1] + 1;
    }
    bn = isplit[nblk_];
    blksiz = bn - b1 + 1;
    if( blksiz == 1 ) 
      goto L_40;
    
    //        Compute reorthogonalization criterion and stopping criterion.
    
    onenrm = abs( d[b1 - 1] ) + abs( e[b1 - 1] );
    onenrm = max( onenrm, abs( d[bn - 1] ) + abs( e[bn - 2] ) );
    for( i = b1 + 1, i_ = i - 1, _do2 = bn - 1; i <= _do2; i++, i_++ ) { 
      onenrm = max( onenrm, abs( d[i_] ) + abs( e[i_ - 1] ) + 
       abs( e[i_] ) );
    }
    ortol = ODM3*onenrm;
    
    stpcrt = sqrt( ODM1/blksiz );
    
    //        Loop through eigenvalues of block nblk.
    
L_40:
    ;
    jblk = 0;
    for( j = j1, j_ = j - 1, _do3 = m; j <= _do3; j++, j_++ ) { 
      if( iblock[j_] != nblk ) { 
        j1 = j;
        goto L_140;
      }
      jblk = jblk + 1;
      xj = w[j_];
      
      //           Skip all the work if the block size is one.
      
      if( blksiz == 1 ) { 
        work[indrv1] = ONE;
        goto L_100;
      }
      
      //           If eigenvalues j and j-1 are too close, add a relatively
      //           small perturbation.
      
      if( jblk > 1 ) { 
        eps1 = abs( eps*xj );
        if( xj - w[j_ - 1] < -eps1 ) { 
          info = -5;
          xerbla( "SSTEIN", -info );
          return;
        }
        pertol = TEN*eps1;
        sep = xj - xjm;
        if( sep < pertol ) 
          xj = xjm + pertol;
      }
      
      its = 0;
      nrmchk = 0;
      
      //           Get random starting vector.
      
      slarnv( 3, iseed, blksiz, &work[indrv1] );
      
      //           Copy the matrix T so it won't be destroyed in factorization.
      
      scopy( blksiz, &d[b1 - 1], 1, &work[indrv4], 1 );
      scopy( blksiz - 1, &e[b1 - 1], 1, &work[indrv2 + 1], 1 );
      scopy( blksiz - 1, &e[b1 - 1], 1, &work[indrv3], 1 );
      
      //           Compute LU factors with partial pivoting  ( PT = LU )
      
      tol = ZERO;
      slagtf( blksiz, &work[indrv4], xj, &work[indrv2 + 1], 
       &work[indrv3], tol, &work[indrv5], iwork, iinfo );
      
      //           Update iteration count.
      
L_50:
      ;
      its = its + 1;
      if( its > MAXITS ) 
        goto L_80;
      
      //           Normalize and scale the righthand side vector Pb.
      
      scl = blksiz*onenrm*max( eps, abs( work[indrv4 + blksiz - 1] ) )/
       sasum( blksiz, &work[indrv1], 1 );
      sscal( blksiz, scl, &work[indrv1], 1 );
      
      //           Solve the system LU = Pb.
      
      slagts( -1, blksiz, &work[indrv4], &work[indrv2 + 1], 
       &work[indrv3], &work[indrv5], iwork, &work[indrv1], tol, 
       iinfo );
      
      //           Reorthogonalize by modified Gram-Schmidt if eigenvalues are
      //           close enough.
      
      if( jblk == 1 ) 
        goto L_70;
      if( abs( xj - xjm ) > ortol ) 
        gpind = j;
      if( gpind != j ) { 
        for( i = gpind, i_ = i - 1, _do4 = j - 1; i <= _do4; i++, i_++ ) { 
          ctr = -sdot( blksiz, &work[indrv1], 1, &Z(i_,b1 - 1), 
           1 );
          saxpy( blksiz, ctr, &Z(i_,b1 - 1), 1, &work[indrv1], 
           1 );
        }
      }
      
      //           Check the infinity norm of the iterate.
      
L_70:
      ;
      jmax = isamax( blksiz, &work[indrv1], 1 );
      nrm = abs( work[indrv1 + jmax - 1] );
      
      //           Continue for additional iterations after norm reaches
      //           stopping criterion.
      
      if( nrm < stpcrt ) 
        goto L_50;
      nrmchk = nrmchk + 1;
      if( nrmchk < EXTRA + 1 ) 
        goto L_50;
      
      goto L_90;
      
      //           If stopping criterion was not satisfied, update info and
      //           store eigenvector number in array ifail.
      
L_80:
      ;
      info = info + 1;
      ifail[info - 1] = j;
      
      //           Accept iterate as jth eigenvector.
      
L_90:
      ;
      scl = ONE/snrm2( blksiz, &work[indrv1], 1 );
      sscal( blksiz, scl, &work[indrv1], 1 );
L_100:
      ;
      for( i = 1, i_ = i - 1, _do5 = n; i <= _do5; i++, i_++ ) { 
        Z(j_,i_) = ZERO;
      }
      for( i = 1, i_ = i - 1, _do6 = blksiz; i <= _do6; i++, i_++ ) { 
        Z(j_,b1 + i_ - 1) = work[indrv1 + i_];
      }
      
      //           Save the shift to check eigenvalue spacing at next
      //           iteration.
      
      xjm = xj;
      
    }
L_140:
    ;
  }
  
  return;
  
  //     End of SSTEIN
  
#undef  Z
} // end of function 


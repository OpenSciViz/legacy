/*
 * C++ implementation of Lapack routine zunm2r
 *
 * $Id: zunm2r.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:51:49
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zunm2r.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const DComplex ONE = DComplex(1.0e0);
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zunm2r(const char &side, const char &trans, const long &m, const long &n, 
 const long &k, DComplex *a, const long &lda, DComplex tau[], DComplex *c, 
 const long &ldc, DComplex work[], long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define C(I_,J_)  (*(c+(I_)*(ldc)+(J_)))
  int left, notran;
  long _do0, _do1, i, i1, i2, i3, i_, ic, jc, mi, ni, nq;
  DComplex aii, taui;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZUNM2R overwrites the general DComplex m-by-n matrix C with
  
  //        Q * C  if SIDE = 'L' and TRANS = 'N', or
  
  //        Q'* C  if SIDE = 'L' and TRANS = 'C', or
  
  //        C * Q  if SIDE = 'R' and TRANS = 'N', or
  
  //        C * Q' if SIDE = 'R' and TRANS = 'C',
  
  //  where Q is a DComplex unitary matrix defined as the product of k
  //  elementary reflectors
  
  //        Q = H(1) H(2) . . . H(k)
  
  //  as returned by ZGEQRF. Q is of order m if SIDE = 'L' and of order n
  //  if SIDE = 'R'.
  
  //  Arguments
  //  =========
  
  //  SIDE    (input) CHARACTER*1
  //          = 'L': apply Q or Q' from the Left
  //          = 'R': apply Q or Q' from the Right
  
  //  TRANS   (input) CHARACTER*1
  //          = 'N': apply Q  (No transpose)
  //          = 'C': apply Q' (Conjugate transpose)
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix C. M >= 0.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix C. N >= 0.
  
  //  K       (input) INTEGER
  //          The number of elementary reflectors whose product defines
  //          the matrix Q.
  //          If SIDE = 'L', M >= K >= 0;
  //          if SIDE = 'R', N >= K >= 0.
  
  //  A       (input) COMPLEX*16 array, dimension (LDA,K)
  //          The i-th column must contain the vector which defines the
  //          elementary reflector H(i), for i = 1,2,...,k, as returned by
  //          ZGEQRF in the first k columns of its array argument A.
  //          A is modified by the routine but restored on exit.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.
  //          If SIDE = 'L', LDA >= max(1,M);
  //          if SIDE = 'R', LDA >= max(1,N).
  
  //  TAU     (input) COMPLEX*16 array, dimension (K)
  //          TAU(i) must contain the scalar factor of the elementary
  //          reflector H(i), as returned by ZGEQRF.
  
  //  C       (input/output) COMPLEX*16 array, dimension (LDC,N)
  //          On entry, the m-by-n matrix C.
  //          On exit, C is overwritten by Q*C or Q'*C or C*Q' or C*Q.
  
  //  LDC     (input) INTEGER
  //          The leading dimension of the array C. LDC >= max(1,M).
  
  //  WORK    (workspace) COMPLEX*16 array, dimension
  //                                   (N) if SIDE = 'L',
  //                                   (M) if SIDE = 'R'
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input arguments
  
  info = 0;
  left = lsame( side, 'L' );
  notran = lsame( trans, 'N' );
  
  //     NQ is the order of Q
  
  if( left ) { 
    nq = m;
  }
  else { 
    nq = n;
  }
  if( !left && !lsame( side, 'R' ) ) { 
    info = -1;
  }
  else if( !notran && !lsame( trans, 'C' ) ) { 
    info = -2;
  }
  else if( m < 0 ) { 
    info = -3;
  }
  else if( n < 0 ) { 
    info = -4;
  }
  else if( k < 0 || k > nq ) { 
    info = -5;
  }
  else if( lda < max( 1, nq ) ) { 
    info = -7;
  }
  else if( ldc < max( 1, m ) ) { 
    info = -10;
  }
  if( info != 0 ) { 
    xerbla( "ZUNM2R", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( (m == 0 || n == 0) || k == 0 ) 
    return;
  
  if( (left && !notran) || (!left && notran) ) { 
    i1 = 1;
    i2 = k;
    i3 = 1;
  }
  else { 
    i1 = k;
    i2 = 1;
    i3 = -1;
  }
  
  if( left ) { 
    ni = n;
    jc = 1;
  }
  else { 
    mi = m;
    ic = 1;
  }
  
  for( i = i1, i_ = i - 1, _do0=docnt(i,i2,_do1 = i3); _do0 > 0; i += _do1, i_ += _do1, _do0-- ) { 
    if( left ) { 
      
      //           H(i) or H(i)' is applied to C(i:m,1:n)
      
      mi = m - i + 1;
      ic = i;
    }
    else { 
      
      //           H(i) or H(i)' is applied to C(1:m,i:n)
      
      ni = n - i + 1;
      jc = i;
    }
    
    //        Apply H(i) or H(i)'
    
    if( notran ) { 
      taui = tau[i_];
    }
    else { 
      taui = conj( tau[i_] );
    }
    aii = A(i_,i_);
    A(i_,i_) = ONE;
    zlarf( side, mi, ni, &A(i_,i_), 1, taui, &C(jc - 1,ic - 1), 
     ldc, work );
    A(i_,i_) = aii;
  }
  return;
  
  //     End of ZUNM2R
  
#undef  C
#undef  A
} // end of function 


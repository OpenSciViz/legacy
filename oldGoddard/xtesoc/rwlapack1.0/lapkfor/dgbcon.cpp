/*
 * C++ implementation of lapack routine dgbcon
 *
 * $Id: dgbcon.cpp,v 1.6 1993/04/06 20:40:18 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:33:36
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dgbcon.cpp,v $
 * Revision 1.6  1993/04/06  20:40:18  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.5  1993/03/19  18:41:23  alv
 * now passes chars explicitly, rather than indirection of a string, to shut up SUN warnings
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:13:59  alv
 * added const here and there
 *
 * Revision 1.2  1993/03/05  23:14:21  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:06:23  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ONE = 1.0e0;
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dgbcon(const char &norm, long &n, long &kl, long &ku, 
 double *ab, long &ldab, long ipiv[], double &anorm, double &rcond, 
 double work[], long iwork[], long &info)
{
#define AB(I_,J_) (*(ab+(I_)*(ldab)+(J_)))
  int lnoti, onenrm;
  char normin;
  long _do0, ix, j, j_, jp, kase, kase1, kd, lm;
  double ainvnm, scale, smlnum, t;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DGBCON estimates the reciprocal of the condition number of a real
  //  general band matrix A, in either the 1-norm or the infinity-norm,
  //  using the LU factorization computed by DGBTRF.
  
  //  An estimate is obtained for norm(inv(A)), and RCOND is computed as
  //     RCOND = 1 / ( norm(A) * norm(inv(A)) ).
  
  //  Arguments
  //  =========
  
  //  NORM    (input) CHARACTER*1
  //          Specifies whether the 1-norm condition number or the
  //          infinity-norm condition number is required:
  //          = '1' or 'O':  1-norm
  //          = 'I':         Infinity-norm
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  KL      (input) INTEGER
  //          The number of subdiagonals within the band of A.  KL >= 0.
  
  //  KU      (input) INTEGER
  //          The number of superdiagonals within the band of A.  KU >= 0.
  
  //  AB      (input) DOUBLE PRECISION array, dimension (LDAB,N)
  //          Details of the LU factorization of the band matrix A, as
  //          computed by DGBTRF.  U is stored as an upper triangular band
  //          matrix with KL+KU superdiagonals in rows 1 to KL+KU+1, and
  //          the multipliers used during the factorization are stored in
  //          rows KL+KU+2 to 2*KL+KU+1.
  
  //  LDAB    (input) INTEGER
  //          The leading dimension of the array AB.  LDAB >= 2*KL+KU+1.
  
  //  IPIV    (input) INTEGER array, dimension (N)
  //          The pivot indices; for 1 <= i <= N, row i of the matrix was
  //          interchanged with row IPIV(i).
  
  //  ANORM   (input) DOUBLE PRECISION
  //          If NORM = '1' or 'O', the 1-norm of the original matrix A.
  //          If NORM = 'I', the infinity-norm of the original matrix A.
  
  //  RCOND   (output) DOUBLE PRECISION
  //          The reciprocal of the condition number of the matrix A,
  //          computed as RCOND = 1/(norm(A) * norm(inv(A))).
  
  //  WORK    (workspace) DOUBLE PRECISION array, dimension (3*N)
  
  //  IWORK   (workspace) INTEGER array, dimension (N)
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  onenrm = norm == '1' || lsame( norm, 'O' );
  if( !onenrm && !lsame( norm, 'I' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( kl < 0 ) { 
    info = -3;
  }
  else if( ku < 0 ) { 
    info = -4;
  }
  else if( ldab < 2*kl + ku + 1 ) { 
    info = -6;
  }
  else if( anorm < ZERO ) { 
    info = -8;
  }
  if( info != 0 ) { 
    xerbla( "DGBCON", -info );
    return;
  }
  
  //     Quick return if possible
  
  rcond = ZERO;
  if( n == 0 ) { 
    rcond = ONE;
    return;
  }
  else if( anorm == ZERO ) { 
    return;
  }
  
  smlnum = dlamch( 'S'/* Safe minimum */ );
  
  //     Estimate the norm of inv(A).
  
  ainvnm = ZERO;
  normin = 'N';
  if( onenrm ) { 
    kase1 = 1;
  }
  else { 
    kase1 = 2;
  }
  kd = kl + ku + 1;
  lnoti = kl > 0;
  kase = 0;
L_10:
  ;
  dlacon( n, &work[n], work, iwork, ainvnm, kase );
  if( kase != 0 ) { 
    if( kase == kase1 ) { 
      
      //           Multiply by inv(L).
      
      if( lnoti ) { 
        for( j = 1, j_ = j - 1, _do0 = n - 1; j <= _do0; j++, j_++ ) { 
          lm = min( kl, n - j );
          jp = ipiv[j_];
          t = work[jp - 1];
          if( jp != j ) { 
            work[jp - 1] = work[j_];
            work[j_] = t;
          }
          daxpy( lm, -t, &AB(j_,kd), 1, &work[j_ + 1], 1 );
        }
      }
      
      //           Multiply by inv(U).
      
      dlatbs( 'U'/* Upper */, 'N'/* No transpose */, 'N'/* Non-unit */
       , normin, n, kl + ku, ab, ldab, work, scale, &work[n*2], 
       info );
    }
    else { 
      
      //           Multiply by inv(U').
      
      dlatbs( 'U'/* Upper */, 'T'/* Transpose */, 'N'/* Non-unit */
       , normin, n, kl + ku, ab, ldab, work, scale, &work[n*2], 
       info );
      
      //           Multiply by inv(L').
      
      if( lnoti ) { 
        for( j = n - 1, j_ = j - 1; j >= 1; j--, j_-- ) { 
          lm = min( kl, n - j );
          work[j_] = work[j_] - ddot( lm, &AB(j_,kd), 1, 
           &work[j_ + 1], 1 );
          jp = ipiv[j_];
          if( jp != j ) { 
            t = work[jp - 1];
            work[jp - 1] = work[j_];
            work[j_] = t;
          }
        }
      }
    }
    
    //        Divide X by 1/SCALE if doing so will not cause overflow.
    
    normin = 'Y';
    if( scale != ONE ) { 
      ix = idamax( n, work, 1 );
      if( scale < abs( work[ix - 1] )*smlnum || scale == ZERO ) 
        goto L_40;
      drscl( n, scale, work, 1 );
    }
    goto L_10;
  }
  
  //     Compute the estimate of the reciprocal condition number.
  
  if( ainvnm != ZERO ) 
    rcond = (ONE/ainvnm)/anorm;
  
L_40:
  ;
  return;
  
  //     End of DGBCON
  
#undef  AB
} // end of function 


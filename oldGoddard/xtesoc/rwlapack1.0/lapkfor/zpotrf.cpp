/*
 * C++ implementation of Lapack routine zpotrf
 *
 * $Id: zpotrf.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:50:00
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zpotrf.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ONE = 1.0e0;
const DComplex CONE = DComplex(1.0e0);
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zpotrf(const char &uplo, const long &n, DComplex *a, const long &lda, 
 long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  char _c0[2];
  int upper;
  long _do0, _do1, _do2, _do3, j, j_, jb, nb;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZPOTRF computes the Cholesky factorization of a DComplex Hermitian
  //  positive definite matrix A.
  
  //  The factorization has the form
  //     A = U' * U ,  if UPLO = 'U', or
  //     A = L  * L',  if UPLO = 'L',
  //  where U is an upper triangular matrix and L is lower triangular.
  
  //  This is the block version of the algorithm, calling Level 3 BLAS.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangular part of the
  //          Hermitian matrix A is stored.
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  A       (input/output) COMPLEX*16 array, dimension (LDA,N)
  //          On entry, the Hermitian matrix A.  If UPLO = 'U', the leading
  //          n by n upper triangular part of A contains the upper
  //          triangular part of the matrix A, and the strictly lower
  //          triangular part of A is not referenced.  If UPLO = 'L', the
  //          leading n by n lower triangular part of A contains the lower
  //          triangular part of the matrix A, and the strictly upper
  //          triangular part of A is not referenced.
  
  //          On exit, if INFO = 0, the factor U or L from the Cholesky
  //          factorization A = U'*U or A = L*L'.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: if INFO = k, the leading minor of order k is not
  //               positive definite, and the factorization could not be
  //               completed.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( lda < max( 1, n ) ) { 
    info = -4;
  }
  if( info != 0 ) { 
    xerbla( "ZPOTRF", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  //     Determine the block size for this environment.
  
  nb = ilaenv( 1, "ZPOTRF", STR1(_c0,uplo), n, -1, -1, -1 );
  if( nb <= 1 || nb >= n ) { 
    
    //        Use unblocked code.
    
    zpotf2( uplo, n, a, lda, info );
  }
  else { 
    
    //        Use blocked code.
    
    if( upper ) { 
      
      //           Compute the Cholesky factorization A = U'*U.
      
      for( j = 1, j_ = j - 1, _do0=docnt(j,n,_do1 = nb); _do0 > 0; j += _do1, j_ += _do1, _do0-- ) { 
        
        //              Update and factorize the current diagonal block and test
        //              for non-positive-definiteness.
        
        jb = min( nb, n - j + 1 );
        zherk( 'U'/*Upper*/, 'C'/*Conjugate transpose*/, 
         jb, j - 1, -ONE, &A(j_,0), lda, ONE, &A(j_,j_), lda );
        zpotf2( 'U'/*Upper*/, jb, &A(j_,j_), lda, info );
        if( info != 0 ) 
          goto L_30;
        if( j + jb <= n ) { 
          
          //                 Compute the current block row.
          
          zgemm( 'C'/*Conjugate transpose*/, 'N'/*No transpose*/
           , jb, n - j - jb + 1, j - 1, -(CONE), &A(j_,0), 
           lda, &A(j_ + jb,0), lda, CONE, &A(j_ + jb,j_), 
           lda );
          ztrsm( 'L'/*Left*/, 'U'/*Upper*/, 'C'/*Conjugate transpose*/
           , 'N'/*Non-unit*/, jb, n - j - jb + 1, CONE, 
           &A(j_,j_), lda, &A(j_ + jb,j_), lda );
        }
      }
      
    }
    else { 
      
      //           Compute the Cholesky factorization A = L*L'.
      
      for( j = 1, j_ = j - 1, _do2=docnt(j,n,_do3 = nb); _do2 > 0; j += _do3, j_ += _do3, _do2-- ) { 
        
        //              Update and factorize the current diagonal block and test
        //              for non-positive-definiteness.
        
        jb = min( nb, n - j + 1 );
        zherk( 'L'/*Lower*/, 'N'/*No transpose*/, jb, j - 
         1, -ONE, &A(0,j_), lda, ONE, &A(j_,j_), lda );
        zpotf2( 'L'/*Lower*/, jb, &A(j_,j_), lda, info );
        if( info != 0 ) 
          goto L_30;
        if( j + jb <= n ) { 
          
          //                 Compute the current block column.
          
          zgemm( 'N'/*No transpose*/, 'C'/*Conjugate transpose*/
           , n - j - jb + 1, jb, j - 1, -(CONE), &A(0,j_ + jb), 
           lda, &A(0,j_), lda, CONE, &A(j_,j_ + jb), lda );
          ztrsm( 'R'/*Right*/, 'L'/*Lower*/, 'C'/*Conjugate transpose*/
           , 'N'/*Non-unit*/, n - j - jb + 1, jb, CONE, 
           &A(j_,j_), lda, &A(j_,j_ + jb), lda );
        }
      }
    }
  }
  goto L_40;
  
L_30:
  ;
  info = info + j - 1;
  
L_40:
  ;
  return;
  
  //     End of ZPOTRF
  
#undef  A
} // end of function 


/*
 * C++ implementation of Lapack routine zlassq
 *
 * $Id: zlassq.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:49:18
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zlassq.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zlassq(const long &n, DComplex x[], const long &incx, 
 double &scale, double &sumsq)
{
  long _do0, _do1, ix, ix_;
  double temp1;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZLASSQ returns the values scl and ssq such that
  
  //     ( scl**2 )*ssq = x( 1 )**2 +...+ x( n )**2 + ( scale**2 )*sumsq,
  
  //  where x( i ) = abs( X( 1 + ( i - 1 )*INCX ) ). The value of sumsq is
  //  assumed to be at least unity and the value of ssq will then satisfy
  
  //     1.0 .le. ssq .le. ( sumsq + 2*n ).
  
  //  scale is assumed to be non-negative and scl returns the value
  
  //     scl = max( scale, abs( real( x( i ) ) ), abs( aimag( x( i ) ) ) ),
  //            i
  
  //  scale and sumsq must be supplied in SCALE and SUMSQ respectively.
  //  SCALE and SUMSQ are overwritten by scl and ssq respectively.
  
  //  The routine makes only one pass through the vector X.
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The number of elements to be used from the vector X.
  
  //  X       (input) DOUBLE PRECISION
  //          The vector x as described above.
  //             x( i )  = X( 1 + ( i - 1 )*INCX ), 1 <= i <= n.
  
  //  INCX    (input) INTEGER
  //          The increment between successive values of the vector X.
  //          INCX > 0.
  
  //  SCALE   (input/output) DOUBLE PRECISION
  //          On entry, the value  scale  in the equation above.
  //          On exit, SCALE is overwritten with the value  scl .
  
  //  SUMSQ   (input/output) DOUBLE PRECISION
  //          On entry, the value  sumsq  in the equation above.
  //          On exit, SUMSQ is overwritten with the value  ssq .
  
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  if( n > 0 ) { 
    for( ix = 1, ix_ = ix - 1, _do0=docnt(ix,1 + (n - 1)*incx,_do1 = incx); _do0 > 0; ix += _do1, ix_ += _do1, _do0-- ) { 
      if( real( x[ix_] ) != ZERO ) { 
        temp1 = abs( real( x[ix_] ) );
        if( scale < temp1 ) { 
          sumsq = 1 + sumsq*pow(scale/temp1, 2);
          scale = temp1;
        }
        else { 
          sumsq = sumsq + pow(temp1/scale, 2);
        }
      }
      if( imag( x[ix_] ) != ZERO ) { 
        temp1 = abs( imag( x[ix_] ) );
        if( scale < temp1 ) { 
          sumsq = 1 + sumsq*pow(scale/temp1, 2);
          scale = temp1;
        }
        else { 
          sumsq = sumsq + pow(temp1/scale, 2);
        }
      }
    }
  }
  
  return;
  
  //     End of ZLASSQ
  
} // end of function 


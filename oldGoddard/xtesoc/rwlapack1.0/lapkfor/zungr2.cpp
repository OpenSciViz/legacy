/*
 * C++ implementation of Lapack routine zungr2
 *
 * $Id: zungr2.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:51:43
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zungr2.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const DComplex ONE = DComplex(1.0e0);
const DComplex ZERO = DComplex(0.0e0);
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zungr2(const long &m, const long &n, const long &k, DComplex *a, 
 const long &lda, DComplex tau[], DComplex work[], long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  long _do0, _do1, _do2, _do3, i, i_, ii, j, j_, l, l_;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZUNGR2 generates an m by n DComplex matrix Q with orthonormal rows,
  //  which is defined as the last m rows of a product of k elementary
  //  reflectors of order n
  
  //        Q  =  H(1)' H(2)' . . . H(k)'
  
  //  as returned by ZGERQF.
  
  //  Arguments
  //  =========
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix Q. M >= 0.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix Q. N >= M.
  
  //  K       (input) INTEGER
  //          The number of elementary reflectors whose product defines the
  //          matrix Q. M >= K >= 0.
  
  //  A       (input/output) COMPLEX*16 array, dimension (LDA,N)
  //          On entry, the (m-k+i)-th row must contain the vector which
  //          defines the elementary reflector H(i), for i = 1,2,...,k, as
  //          returned by ZGERQF in the last k rows of its array argument
  //          A.
  //          On exit, the m-by-n matrix Q.
  
  //  LDA     (input) INTEGER
  //          The first dimension of the array A. LDA >= max(1,M).
  
  //  TAU     (input) COMPLEX*16 array, dimension (K)
  //          TAU(i) must contain the scalar factor of the elementary
  //          reflector H(i), as returned by ZGERQF.
  
  //  WORK    (workspace) COMPLEX*16 array, dimension (M)
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument has an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input arguments
  
  info = 0;
  if( m < 0 ) { 
    info = -1;
  }
  else if( n < m ) { 
    info = -2;
  }
  else if( k < 0 || k > m ) { 
    info = -3;
  }
  else if( lda < max( 1, m ) ) { 
    info = -5;
  }
  if( info != 0 ) { 
    xerbla( "ZUNGR2", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( m <= 0 ) 
    return;
  
  if( k < m ) { 
    
    //        Initialise rows 1:m-k to rows of the unit matrix
    
    for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
      for( l = 1, l_ = l - 1, _do1 = m - k; l <= _do1; l++, l_++ ) { 
        A(j_,l_) = ZERO;
      }
      if( j > n - m && j <= n - k ) 
        A(j_,m - n + j_) = ONE;
    }
  }
  
  for( i = 1, i_ = i - 1, _do2 = k; i <= _do2; i++, i_++ ) { 
    ii = m - k + i;
    
    //        Apply H(i)' to A(1:m-k+i,1:n-k+i) from the right
    
    zlacgv( n - m + ii - 1, &A(0,ii - 1), lda );
    A(n - m + ii - 1,ii - 1) = ONE;
    zlarf( 'R'/*Right*/, ii - 1, n - m + ii, &A(0,ii - 1), lda, 
     conj( tau[i_] ), a, lda, work );
    zscal( n - m + ii - 1, -(tau[i_]), &A(0,ii - 1), lda );
    zlacgv( n - m + ii - 1, &A(0,ii - 1), lda );
    A(n - m + ii - 1,ii - 1) = ONE - conj( tau[i_] );
    
    //        Set A(m-k+i,n-k+i+1:n) to zero
    
    for( l = n - m + ii + 1, l_ = l - 1, _do3 = n; l <= _do3; l++, l_++ ) { 
      A(l_,ii - 1) = ZERO;
    }
  }
  return;
  
  //     End of ZUNGR2
  
#undef  A
} // end of function 


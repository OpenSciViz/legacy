/*
 * C++ implementation of lapack routine dorgr2
 *
 * $Id: dorgr2.cpp,v 1.6 1993/04/06 20:41:42 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:36:48
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dorgr2.cpp,v $
 * Revision 1.6  1993/04/06  20:41:42  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.5  1993/03/19  18:41:23  alv
 * now passes chars explicitly, rather than indirection of a string, to shut up SUN warnings
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:16:16  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:08:01  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ONE = 1.0e0;
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dorgr2(const long &m, const long &n, const long &k, double *a, 
 const long &lda, double tau[], double work[], long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  long _do0, _do1, _do2, _do3, i, i_, ii, j, j_, l, l_;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DORGR2 generates an m by n real matrix Q with orthonormal rows,
  //  which is defined as the last m rows of a product of k elementary
  //  reflectors of order n
  
  //        Q  =  H(1) H(2) . . . H(k)
  
  //  as returned by DGERQF.
  
  //  Arguments
  //  =========
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix Q. M >= 0.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix Q. N >= M.
  
  //  K       (input) INTEGER
  //          The number of elementary reflectors whose product defines the
  //          matrix Q. M >= K >= 0.
  
  //  A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)
  //          On entry, the (m-k+i)-th row must contain the vector which
  //          defines the elementary reflector H(i), for i = 1,2,...,k, as
  //          returned by DGERQF in the last k rows of its array argument
  //          A.
  //          On exit, the m by n matrix Q.
  
  //  LDA     (input) INTEGER
  //          The first dimension of the array A. LDA >= max(1,M).
  
  //  TAU     (input) DOUBLE PRECISION array, dimension (K)
  //          TAU(i) must contain the scalar factor of the elementary
  //          reflector H(i), as returned by DGERQF.
  
  //  WORK    (workspace) DOUBLE PRECISION array, dimension (M)
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument has an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input arguments
  
  info = 0;
  if( m < 0 ) { 
    info = -1;
  }
  else if( n < m ) { 
    info = -2;
  }
  else if( k < 0 || k > m ) { 
    info = -3;
  }
  else if( lda < max( 1, m ) ) { 
    info = -5;
  }
  if( info != 0 ) { 
    xerbla( "DORGR2", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( m <= 0 ) 
    return;
  
  if( k < m ) { 
    
    //        Initialise rows 1:m-k to rows of the unit matrix
    
    for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
      for( l = 1, l_ = l - 1, _do1 = m - k; l <= _do1; l++, l_++ ) { 
        A(j_,l_) = ZERO;
      }
      if( j > n - m && j <= n - k ) 
        A(j_,m - n + j_) = ONE;
    }
  }
  
  for( i = 1, i_ = i - 1, _do2 = k; i <= _do2; i++, i_++ ) { 
    ii = m - k + i;
    
    //        Apply H(i) to A(1:m-k+i,1:n-k+i) from the right
    
    A(n - m + ii - 1,ii - 1) = ONE;
    dlarf( 'R'/* Right */, ii - 1, n - m + ii, &A(0,ii - 1), lda, 
     tau[i_], a, lda, work );
    dscal( n - m + ii - 1, -tau[i_], &A(0,ii - 1), lda );
    A(n - m + ii - 1,ii - 1) = ONE - tau[i_];
    
    //        Set A(m-k+i,n-k+i+1:n) to zero
    
    for( l = n - m + ii + 1, l_ = l - 1, _do3 = n; l <= _do3; l++, l_++ ) { 
      A(l_,ii - 1) = ZERO;
    }
  }
  return;
  
  //     End of DORGR2
  
#undef  A
} // end of function 


/*
 * C++ implementation of Lapack routine zgetf2
 *
 * $Id: zgetf2.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:46:56
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zgetf2.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const DComplex ONE = DComplex(1.0e0);
const DComplex ZERO = DComplex(0.0e0);
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zgetf2(const long &m, const long &n, DComplex *a, const long &lda, 
 long ipiv[], long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  long _do0, j, j_, jp;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZGETF2 computes an LU factorization of a general m-by-n matrix A
  //  using partial pivoting with row interchanges.
  
  //  The factorization has the form
  //     A = P * L * U
  //  where P is a permutation matrix, L is lower triangular with unit
  //  diagonal elements (lower trapezoidal if m > n), and U is upper
  //  triangular (upper trapezoidal if m < n).
  
  //  This is the right-looking Level 2 BLAS version of the algorithm.
  
  //  Arguments
  //  =========
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix A.  M >= 0.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix A.  N >= 0.
  
  //  A       (input/output) COMPLEX*16 array, dimension (LDA,N)
  //          On entry, the m by n matrix to be factored.
  //          On exit, the factors L and U from the factorization
  //          A = P*L*U; the unit diagonal elements of L are not stored.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,M).
  
  //  IPIV    (output) INTEGER array, dimension (min(M,N))
  //          The pivot indices; for 1 <= i <= min(M,N), row i of the
  //          matrix was interchanged with row IPIV(i).
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: if INFO = k, U(k,k) is exactly zero. The factorization
  //               has been completed, but the factor U is exactly
  //               singular, and division by zero will occur if it is used
  //               to solve a system of equations.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  if( m < 0 ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( lda < max( 1, m ) ) { 
    info = -4;
  }
  if( info != 0 ) { 
    xerbla( "ZGETF2", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( m == 0 || n == 0 ) 
    return;
  
  for( j = 1, j_ = j - 1, _do0 = min( m, n ); j <= _do0; j++, j_++ ) { 
    
    //        Find pivot and test for singularity.
    
    jp = j - 1 + izamax( m - j + 1, &A(j_,j_), 1 );
    ipiv[j_] = jp;
    if( ctocf(A(j_,jp - 1)) != ctocf(ZERO) ) { 
      
      //           Apply the interchange to columns 1:N.
      
      if( jp != j ) 
        zswap( n, &A(0,j_), lda, &A(0,jp - 1), lda );
      
      //           Compute elements J+1:M of J-th column.
      
      if( j < m ) 
        zscal( m - j, ONE/A(j_,j_), &A(j_,j_ + 1), 1 );
      
    }
    else if( info == 0 ) { 
      
      info = j;
    }
    
    if( j + 1 <= n ) { 
      
      //           Update trailing submatrix.
      
      zgeru( m - j, n - j, -(ONE), &A(j_,j_ + 1), 1, &A(j_ + 1,j_), 
       lda, &A(j_ + 1,j_ + 1), lda );
    }
  }
  return;
  
  //     End of ZGETF2
  
#undef  A
} // end of function 


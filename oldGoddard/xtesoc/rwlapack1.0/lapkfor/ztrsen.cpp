/*
 * C++ implementation of Lapack routine ztrsen
 *
 * $Id: ztrsen.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:51:21
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: ztrsen.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ ztrsen(const char &job, const char &compq, int select[], 
 const long &n, DComplex *t, const long &ldt, DComplex *q, const long &ldq, 
 DComplex w[], long &m, double &s, double &sep, DComplex work[], 
 const long &lwork, long &info)
{
#define T(I_,J_)  (*(t+(I_)*(ldt)+(J_)))
#define Q(I_,J_)  (*(q+(I_)*(ldq)+(J_)))
  int wantbh, wantq, wants, wantsp;
  long _do0, _do1, _do2, ierr, k, k_, kase, ks, n1, n2, nn;
  double est, rnorm, rwork[1], scale;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZTRSEN reorders the Schur factorization of a DComplex matrix
  //  A = Q*T*Q', so that a selected cluster of eigenvalues appears in the
  //  leading positions on the diagonal of the upper triangular matrix T,
  //  and the leading columns of Q form an orthonormal basis of the
  //  corresponding right invariant subspace.
  
  //  Optionally the routine computes the reciprocal condition numbers of
  //  the cluster of eigenvalues and/or the invariant subspace.
  
  //  Arguments
  //  =========
  
  //  JOB     (input) CHARACTER*1
  //          Specifies whether condition numbers are required for the
  //          cluster of eigenvalues (S) or the invariant subspace (SEP):
  //          = 'N': none;
  //          = 'E': for eigenvalues only (S);
  //          = 'V': for invariant subspace only (SEP);
  //          = 'B': for both eigenvalues and invariant subspace (S and
  //                 SEP).
  
  //  COMPQ   (input) CHARACTER*1
  //          = 'V': update the matrix Q of Schur vectors;
  //          = 'N': do not update Q.
  
  //  SELECT  (input) LOGICAL array, dimension (N)
  //          SELECT specifies the eigenvalues in the selected cluster. To
  //          select the j-th eigenvalue, SELECT(j) must be set to .TRUE..
  
  //  N       (input) INTEGER
  //          The order of the matrix T. N >= 0.
  
  //  T       (input/output) COMPLEX*16 array, dimension(LDT,N)
  //          On entry, the upper triangular matrix T.
  //          On exit, T is overwritten by the reordered matrix T, with the
  //          selected eigenvalues in the leading positions on the
  //          diagonal.
  
  //  LDT     (input) INTEGER
  //          The leading dimension of the array T. LDT >= max(1,N).
  
  //  Q       (input/output) COMPLEX*16 array, dimension (LDQ,N)
  //          On entry, if COMPQ = 'V', the matrix Q of Schur vectors.
  //          On exit, if COMPQ = 'V', Q has been postmultiplied by the
  //          unitary transformation matrix which reorders T; the leading M
  //          columns of Q form an orthonormal basis for the specified
  //          invariant subspace.
  //          If COMPQ = 'N', Q is not referenced.
  
  //  LDQ     (input) INTEGER
  //          The leading dimension of the array Q.
  //          LDQ >= 1; and if COMPQ = 'V', LDQ >= N.
  
  //  W       (output) COMPLEX*16
  //          The reordered eigenvalues of T, in the same order as they
  //          appear on the diagonal of the output matrix T.
  
  //  M       (output) INTEGER
  //          The dimension of the specified invariant subspace.
  
  //  S       (output) DOUBLE PRECISION
  //          If JOB = 'E' or 'B', S is a lower bound on the reciprocal
  //          condition number for the selected cluster of eigenvalues.
  //          S cannot underestimate the true reciprocal condition number
  //          by more than a factor of sqrt(N). If M = 0 or N, S = 1.
  //          If JOB = 'N' or 'V', S is not referenced.
  
  //  SEP     (output) DOUBLE PRECISION
  //          If JOB = 'V' or 'B', SEP is the estimated reciprocal
  //          condition number of the specified invariant subspace. If
  //          M = 0 or N, SEP = norm(T).
  //          If JOB = 'N' or 'E', SEP is not referenced.
  
  //  WORK    (workspace) COMPLEX*16 array, dimension (LWORK)
  //          If JOB = 'N', WORK is not referenced.
  
  //  LWORK   (input) INTEGER
  //          The dimension of the array WORK.
  //          If JOB = 'N', LWORK >= 1;
  //          if JOB = 'E', LWORK = M*(N-M);
  //          if JOB = 'V' or 'B', LWORK >= 2*M*(N-M).
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument had an illegal value
  
  //  Further Details
  //  ===============
  
  //  ZTRSEN first collects the selected eigenvalues by computing a unitary
  //  transformation Z to move them to the top left corner of T. In other
  //  words, the selected eigenvalues are the eigenvalues of T11 in:
  
  //                Z'*T*Z = ( T11 T12 ) n1
  //                         (  0  T22 ) n2
  //                            n1  n2
  
  //  where N = n1+n2 and Z' means the conjugate transpose of Z. The first
  //  n1 columns of Z span the specified invariant subspace of T.
  
  //  If T has been obtained from the Schur factorization of a matrix
  //  A = Q*T*Q', then the reordered Schur factorization of A is given by
  //  A = (Q*Z)*(Z'*T*Z)*(Q*Z)', and the first n1 columns of Q*Z span the
  //  corresponding invariant subspace of A.
  
  //  The reciprocal condition number of the average of the eigenvalues of
  //  T11 may be returned in S. S lies between 0 (very badly conditioned)
  //  and 1 (very well conditioned). It is computed as follows. First we
  //  compute R so that
  
  //                         P = ( I  R ) n1
  //                             ( 0  0 ) n2
  //                               n1 n2
  
  //  is the projector on the invariant subspace associated with T11.
  //  R is the solution of the Sylvester equation:
  
  //                        T11*R - R*T22 = T12.
  
  //  Let F-norm(M) denote the Frobenius-norm of M and 2-norm(M) denote
  //  the two-norm of M. Then S is computed as the lower bound
  
  //                      (1 + F-norm(R)**2)**(-1/2)
  
  //  on the reciprocal of 2-norm(P), the true reciprocal condition number.
  //  S cannot underestimate 1 / 2-norm(P) by more than a factor of
  //  sqrt(N).
  
  //  An approximate error bound for the computed average of the
  //  eigenvalues of T11 is
  
  //                         EPS * norm(T) / S
  
  //  where EPS is the machine precision.
  
  //  The reciprocal condition number of the right invariant subspace
  //  spanned by the first n1 columns of Z (or of Q*Z) is returned in SEP.
  //  SEP is defined as the separation of T11 and T22:
  
  //                     sep( T11, T22 ) = sigma-min( C )
  
  //  where sigma-min(C) is the smallest singular value of the
  //  n1*n2-by-n1*n2 matrix
  
  //     C  = kprod( I(n2), T11 ) - kprod( transpose(T22), I(n1) )
  
  //  I(m) is an m by m identity matrix, and kprod denotes the Kronecker
  //  product. We estimate sigma-min(C) by the reciprocal of an estimate of
  //  the 1-norm of inverse(C). The true reciprocal 1-norm of inverse(C)
  //  cannot differ from sigma-min(C) by more than a factor of sqrt(n1*n2).
  
  //  When SEP is small, small changes in T can cause large changes in
  //  the invariant subspace. An approximate bound on the maximum angular
  //  error in the computed right invariant subspace is
  
  //                      EPS * norm(T) / SEP
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Local Arrays ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Decode and test the input parameters.
  
  wantbh = lsame( job, 'B' );
  wants = lsame( job, 'E' ) || wantbh;
  wantsp = lsame( job, 'V' ) || wantbh;
  wantq = lsame( compq, 'V' );
  
  //     Set M to the number of selected eigenvalues.
  
  m = 0;
  for( k = 1, k_ = k - 1, _do0 = n; k <= _do0; k++, k_++ ) { 
    if( select[k_] ) 
      m = m + 1;
  }
  
  n1 = m;
  n2 = n - m;
  nn = n1*n2;
  
  info = 0;
  if( (!lsame( job, 'N' ) && !wants) && !wantsp ) { 
    info = -1;
  }
  else if( !lsame( compq, 'N' ) && !wantq ) { 
    info = -2;
  }
  else if( n < 0 ) { 
    info = -4;
  }
  else if( ldt < max( 1, n ) ) { 
    info = -6;
  }
  else if( ldq < 1 || (wantq && ldq < n) ) { 
    info = -8;
  }
  else if( (lwork < 1 || ((wants && !wantsp) && lwork < nn)) || 
   (wantsp && lwork < 2*nn) ) { 
    info = -14;
  }
  if( info != 0 ) { 
    xerbla( "ZTRSEN", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( m == n || m == 0 ) { 
    if( wants ) 
      s = ONE;
    if( wantsp ) 
      sep = zlange( '1', n, n, t, ldt, rwork );
    goto L_40;
  }
  
  //     Collect the selected eigenvalues at the top left corner of T.
  
  ks = 0;
  for( k = 1, k_ = k - 1, _do1 = n; k <= _do1; k++, k_++ ) { 
    if( select[k_] ) { 
      ks = ks + 1;
      
      //           Swap the K-th eigenvalue to position KS.
      
      if( k != ks ) 
        ztrexc( compq, n, t, ldt, q, ldq, k, ks, ierr );
    }
  }
  
  if( wants ) { 
    
    //        Solve the Sylvester equation for R:
    
    //           T11*R - R*T22 = scale*T12
    
    zlacpy( 'F', n1, n2, &T(n1,0), ldt, work, n1 );
    ztrsyl( 'N', 'N', -1, n1, n2, t, ldt, &T(n1,n1), ldt, work, 
     n1, scale, ierr );
    
    //        Estimate the reciprocal of the condition number of the cluster
    //        of eigenvalues.
    
    rnorm = zlange( 'F', n1, n2, work, n1, rwork );
    if( rnorm == ZERO ) { 
      s = ONE;
    }
    else { 
      s = scale/(sqrt( scale*scale/rnorm + rnorm )*sqrt( rnorm ));
    }
  }
  
  if( wantsp ) { 
    
    //        Estimate sep(T11,T22).
    
    est = ZERO;
    kase = 0;
L_30:
    ;
    zlacon( nn, &work[nn], work, est, kase );
    if( kase != 0 ) { 
      if( kase == 1 ) { 
        
        //              Solve T11*R - R*T22 = scale*X.
        
        ztrsyl( 'N', 'N', -1, n1, n2, t, ldt, &T(n1,n1), ldt, 
         work, n1, scale, ierr );
      }
      else { 
        
        //              Solve T11'*R - R*T22' = scale*X.
        
        ztrsyl( 'C', 'C', -1, n1, n2, t, ldt, &T(n1,n1), ldt, 
         work, n1, scale, ierr );
      }
      goto L_30;
    }
    
    sep = scale/est;
  }
  
L_40:
  ;
  
  //     Copy reordered eigenvalues to W.
  
  for( k = 1, k_ = k - 1, _do2 = n; k <= _do2; k++, k_++ ) { 
    w[k_] = T(k_,k_);
  }
  return;
  
  //     End of ZTRSEN
  
#undef  T
#undef  Q
} // end of function 


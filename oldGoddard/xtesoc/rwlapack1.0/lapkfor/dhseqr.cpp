/*
 * C++ implementation of lapack routine dhseqr
 *
 * $Id: dhseqr.cpp,v 1.6 1993/04/06 20:40:50 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:34:53
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dhseqr.cpp,v $
 * Revision 1.6  1993/04/06  20:40:50  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.5  1993/03/19  18:41:23  alv
 * now passes chars explicitly, rather than indirection of a string, to shut up SUN warnings
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:15:04  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:07:02  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
const double TWO = 2.0e0;
const double CONST_ = 1.5e0;
const long NSMAX = 15;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dhseqr(const char &job, const char &compz, const long &n, const long &ilo, 
 const long &ihi, double *h, const long &ldh, double wr[], double wi[], 
 double *z, const long &ldz, double work[], const long &/*lwork*/, long &info)
{
#define H(I_,J_)  (*(h+(I_)*(ldh)+(J_)))
#define Z(I_,J_)  (*(z+(I_)*(ldz)+(J_)))
  // PARAMETER translations
  const long LDS = NSMAX;
  // end of PARAMETER translations

  char _c0[2], _c1[2];
  int initz, wantt, wantz;
  long _do0, _do1, _do10, _do11, _do12, _do2, _do3, _do4, _do5, 
   _do6, _do7, _do8, _do9, i, i1, i2, i_, ierr, ii, ii_, itemp, 
   itn, its, its_, j, j_, k, k_, l, maxb, nh, nr, ns, nv;
  double absw, ovfl, s[NSMAX][LDS], smlnum, tau, temp, tst1, ulp, 
   unfl, v[NSMAX + 1], vv[NSMAX + 1];
#define NCHRTMPS 1
  CHRTMP _c[NCHRTMPS];
  ini_chrtmp(_c,NCHRTMPS);

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DHSEQR computes the eigenvalues of a real upper Hessenberg matrix H
  //  and, optionally, the matrices T and Z from the Schur decomposition
  //  H = Z T Z', where T is an upper quasi-triangular matrix (the Schur
  //  form), Z is the orthogonal matrix of Schur vectors, and Z' denotes
  //  the transpose of Z.
  
  //  Optionally Z may be postmultiplied into an input orthogonal matrix Q,
  //  so that this routine can give the Schur factorization of a matrix A
  //  which has been reduced to the Hessenberg form H by the orthogonal
  //  matrix Q:  A = Q*H*Q' = (QZ)*T*(QZ)'.
  
  //  Arguments
  //  =========
  
  //  JOB     (input) CHARACTER*1
  //          = 'E': compute eigenvalues only;
  //          = 'S': compute eigenvalues and the Schur form T.
  
  //  COMPZ   (input) CHARACTER*1
  //          = 'N': no Schur vectors are computed;
  //          = 'I': Z is initialized to the unit matrix and the matrix Z
  //                 of Schur vectors of H is returned;
  //          = 'V': Z must contain an orthogonal matrix Q on entry, and
  //                 the product Q*Z is returned.
  
  //  N       (input) INTEGER
  //          The order of the matrix H.  N >= 0.
  
  //  ILO     (input) INTEGER
  //  IHI     (input) INTEGER
  //          It is assumed that H is already upper triangular in rows
  //          and columns 1:ILO-1 and IHI+1:N. ILO and IHI are normally
  //          set by a previous call to DGEBAL, and then passed to SGEHRD
  //          when the matrix output by DGEBAL is reduced to Hessenberg
  //          form. Otherwise ILO and IHI should be set to 1 and N
  //          respectively.
  //          1 <= ILO <= max(1,IHI); IHI <= N.
  
  //  H       (input/output) DOUBLE PRECISION array, dimension (LDH,N)
  //          On entry, the upper Hessenberg matrix H.
  //          On exit, if JOB = 'S', H contains the upper quasi-triangular
  //          matrix T from the Schur decomposition (the Schur form);
  //          2-by-2 diagonal blocks (corresponding to DComplex conjugate
  //          pairs of eigenvalues) are returned in standard form, with
  //          H(i,i) = H(i+1,i+1) and H(i+1,i)*H(i,i+1) < 0. If JOB = 'E',
  //          the contents of H are unspecified on exit.
  
  //  LDH     (input) INTEGER
  //          The leading dimension of the array H. LDH >= max(1,N).
  
  //  WR      (output) DOUBLE PRECISION array, dimension (N)
  //  WI      (output) DOUBLE PRECISION array, dimension (N)
  //          The real and imaginary parts, respectively, of the computed
  //          eigenvalues. If two eigenvalues are computed as a DComplex
  //          conjugate pair, they are stored in consecutive elements of
  //          WR and WI, say the i-th and (i+1)th, with WI(i) > 0 and
  //          WI(i+1) < 0. If JOB = 'S', the eigenvalues are stored in the
  //          same order as on the diagonal of the Schur form returned in
  //          H, with WR(i) = H(i,i) and, if H(i:i+1,i:i+1) is a 2-by-2
  //          diagonal block, WI(i) = sqrt(H(i+1,i)*H(i,i+1)) and
  //          WI(i+1) = -WI(i).
  
  //  Z       (input/output) DOUBLE PRECISION array, dimension (LDZ,N)
  //          If COMPZ = 'N': Z is not referenced.
  //          If COMPZ = 'I': on entry, Z need not be set, and on exit, Z
  //          contains the orthogonal matrix Z of the Schur vectors of H.
  //          If COMPZ = 'V': on entry Z must contain an n-by-n matrix Q,
  //          which is assumed to be equal to the unit matrix except for
  //          the submatrix Z(ILO:IHI,ILO:IHI); on exit Z contains Q*Z.
  //          Normally Q is the orthogonal matrix generated by DORGHR after
  //          the call to DGEHRD which formed the Hessenberg matrix H.
  
  //  LDZ     (input) INTEGER
  //          The leading dimension of the array Z. LDZ >= max(1,N).
  
  //  WORK    (workspace) DOUBLE PRECISION array, dimension (N)
  
  //  LWORK   (input) INTEGER
  //          This argument is redundant.
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument had an illegal value
  //          > 0: DHSEQR failed to compute all the eigenvalues in a total
  //               of 30*(IHI-ILO+1) iterations; if INFO = i, elements
  //               1:ilo-1 and i+1:n of WR and WI contain those
  //               eigenvalues which have been successfully computed.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Local Arrays ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Decode and test the input parameters
  
  wantt = lsame( job, 'S' );
  initz = lsame( compz, 'I' );
  wantz = initz || lsame( compz, 'V' );
  
  info = 0;
  if( !lsame( job, 'E' ) && !wantt ) { 
    info = -1;
  }
  else if( !lsame( compz, 'N' ) && !wantz ) { 
    info = -2;
  }
  else if( n < 0 ) { 
    info = -3;
  }
  else if( ilo < 1 ) { 
    info = -4;
  }
  else if( ihi < min( ilo, n ) || ihi > n ) { 
    info = -5;
  }
  else if( ldh < max( 1, n ) ) { 
    info = -7;
  }
  else if( ldz < 1 || (wantz && ldz < max( 1, n )) ) { 
    info = -11;
  }
  if( info != 0 ) { 
    xerbla( "DHSEQR", -info );
    return;
  }
  
  //     Initialize Z, if necessary
  
  if( initz ) 
    dlaset( 'F'/* Full */, n, n, ZERO, ONE, z, ldz );
  
  //     Store the eigenvalues isolated by DGEBAL.
  
  for( i = 1, i_ = i - 1, _do0 = ilo - 1; i <= _do0; i++, i_++ ) { 
    wr[i_] = H(i_,i_);
    wi[i_] = ZERO;
  }
  for( i = ihi + 1, i_ = i - 1, _do1 = n; i <= _do1; i++, i_++ ) { 
    wr[i_] = H(i_,i_);
    wi[i_] = ZERO;
  }
  
  //     Quick return if possible.
  
  if( n == 0 ) 
    return;
  if( ilo == ihi ) { 
    wr[ilo - 1] = H(ilo - 1,ilo - 1);
    wi[ilo - 1] = ZERO;
    return;
  }
  
  //     Set rows and columns ILO to IHI to zero below the first
  //     subdiagonal.
  
  for( j = ilo, j_ = j - 1, _do2 = ihi - 2; j <= _do2; j++, j_++ ) { 
    for( i = j + 2, i_ = i - 1, _do3 = n; i <= _do3; i++, i_++ ) { 
      H(j_,i_) = ZERO;
    }
  }
  nh = ihi - ilo + 1;
  
  //     Determine the order of the multi-shift QR algorithm to be used.
  
  ns = ilaenv( 4, "DHSEQR", f_concat(&_c[0],STR1(_c0,job),STR1(_c1,compz),
   NULL), n, ilo, ihi, -1 );
  maxb = ilaenv( 8, "DHSEQR", f_concat(&_c[0],STR1(_c0,job),STR1(_c1,compz),
   NULL), n, ilo, ihi, -1 );
  if( (ns <= 2 || ns > nh) || maxb >= nh ) { 
    
    //        Use the standard double-shift algorithm
    
    dlahqr( wantt, wantz, n, ilo, ihi, h, ldh, wr, wi, ilo, ihi, 
     z, ldz, info );
    rel_chrtmp(_c,NCHRTMPS);
    return;
  }
  maxb = max( 3, maxb );
  ns = vmin( ns, maxb, NSMAX, IEND );
  
  //     Now 2 < NS <= MAXB < NH.
  
  //     Set machine-dependent constants for the stopping criterion.
  //     If norm(H) <= sqrt(OVFL), overflow should not occur.
  
  unfl = dlamch( 'S'/* Safe minimum */ );
  ovfl = ONE/unfl;
  dlabad( unfl, ovfl );
  ulp = dlamch( 'P'/* Precision */ );
  smlnum = unfl*(nh/ulp);
  
  //     I1 and I2 are the indices of the first row and last column of H
  //     to which transformations must be applied. If eigenvalues only are
  //     being computed, I1 and I2 are set inside the main loop.
  
  if( wantt ) { 
    i1 = 1;
    i2 = n;
  }
  
  //     ITN is the total number of multiple-shift QR iterations allowed.
  
  itn = 30*nh;
  
  //     The main loop begins here. I is the loop index and decreases from
  //     IHI to ILO in steps of at most MAXB. Each iteration of the loop
  //     works with the active submatrix in rows and columns L to I.
  //     Eigenvalues I+1 to IHI have already converged. Either L = ILO or
  //     H(L,L-1) is negligible so that the matrix splits.
  
  i = ihi;
L_50:
  ;
  l = ilo;
  if( i < ilo ) 
    goto L_170;
  
  //     Perform multiple-shift QR iterations on rows and columns ILO to I
  //     until a submatrix of order at most MAXB splits off at the bottom
  //     because a subdiagonal element has become negligible.
  
  for( its = 0, its_ = its - 1, _do4 = itn; its <= _do4; its++, its_++ ) { 
    
    //        Look for a single small subdiagonal element.
    
    for( k = i, k_ = k - 1, _do5 = l + 1; k >= _do5; k--, k_-- ) { 
      tst1 = abs( H(k_ - 1,k_ - 1) ) + abs( H(k_,k_) );
      if( tst1 == ZERO ) 
        tst1 = dlanhs( '1', i - l + 1, &H(l - 1,l - 1), ldh, 
         work );
      if( abs( H(k_ - 1,k_) ) <= max( ulp*tst1, smlnum ) ) 
        goto L_70;
    }
L_70:
    ;
    l = k;
    if( l > ilo ) { 
      
      //           H(L,L-1) is negligible.
      
      H(l - 2,l - 1) = ZERO;
    }
    
    //        Exit from loop if a submatrix of order <= MAXB has split off.
    
    if( l >= i - maxb + 1 ) 
      goto L_160;
    
    //        Now the active submatrix is in rows and columns L to I. If
    //        eigenvalues only are being computed, only the active submatrix
    //        need be transformed.
    
    if( !wantt ) { 
      i1 = l;
      i2 = i;
    }
    
    if( its == 20 || its == 30 ) { 
      
      //           Exceptional shifts.
      
      for( ii = i - ns + 1, ii_ = ii - 1, _do6 = i; ii <= _do6; ii++, ii_++ ) { 
        wr[ii_] = CONST_*(abs( H(ii_ - 1,ii_) ) + abs( H(ii_,ii_) ));
        wi[ii_] = ZERO;
      }
    }
    else { 
      
      //           Use eigenvalues of trailing submatrix of order NS as shifts.
      
      dlacpy( 'F'/* Full */, ns, ns, &H(i - ns,i - ns), ldh, 
       (double*)s, LDS );
      dlahqr( FALSE, FALSE, ns, 1, ns, (double*)s, LDS, &wr[i - ns], 
       &wi[i - ns], 1, ns, z, ldz, ierr );
      if( ierr > 0 ) { 
        
        //              If DLAHQR failed to compute all NS eigenvalues, use the
        //              unconverged diagonal elements as the remaining shifts.
        
        for( ii = 1, ii_ = ii - 1, _do7 = ierr; ii <= _do7; ii++, ii_++ ) { 
          wr[i - ns + ii_] = s[ii_][ii_];
          wi[i - ns + ii_] = ZERO;
        }
      }
    }
    
    //        Form the first column of (G-w(1)) (G-w(2)) . . . (G-w(ns))
    //        where G is the Hessenberg submatrix H(L:I,L:I) and w is
    //        the vector of shifts (stored in WR and WI). The result is
    //        stored in the local array V.
    
    v[0] = ONE;
    for( ii = 2, ii_ = ii - 1, _do8 = ns + 1; ii <= _do8; ii++, ii_++ ) { 
      v[ii_] = ZERO;
    }
    nv = 1;
    for( j = i - ns + 1, j_ = j - 1, _do9 = i; j <= _do9; j++, j_++ ) { 
      if( wi[j_] >= ZERO ) { 
        if( wi[j_] == ZERO ) { 
          
          //                 real shift
          
          dcopy( nv + 1, v, 1, vv, 1 );
          dgemv( 'N'/* No transpose */, nv + 1, nv, ONE, 
           &H(l - 1,l - 1), ldh, vv, 1, -wr[j_], v, 1 );
          nv = nv + 1;
        }
        else if( wi[j_] > ZERO ) { 
          
          //                 DComplex conjugate pair of shifts
          
          dcopy( nv + 1, v, 1, vv, 1 );
          dgemv( 'N'/* No transpose */, nv + 1, nv, ONE, 
           &H(l - 1,l - 1), ldh, v, 1, -TWO*wr[j_], vv, 
           1 );
          itemp = idamax( nv + 1, vv, 1 );
          temp = ONE/max( abs( vv[itemp - 1] ), smlnum );
          dscal( nv + 1, temp, vv, 1 );
          absw = dlapy2( wr[j_], wi[j_] );
          temp = (temp*absw)*absw;
          dgemv( 'N'/* No transpose */, nv + 2, nv + 1, ONE, 
           &H(l - 1,l - 1), ldh, vv, 1, temp, v, 1 );
          nv = nv + 2;
        }
        
        //              Scale V(1:NV) so that max(abs(V(i))) = 1. If V is zero,
        //              reset it to the unit vector.
        
        itemp = idamax( nv, v, 1 );
        temp = abs( v[itemp - 1] );
        if( temp == ZERO ) { 
          v[0] = ONE;
          for( ii = 2, ii_ = ii - 1, _do10 = nv; ii <= _do10; ii++, ii_++ ) { 
            v[ii_] = ZERO;
          }
        }
        else { 
          temp = max( temp, smlnum );
          dscal( nv, ONE/temp, v, 1 );
        }
      }
    }
    
    //        Multiple-shift QR step
    
    for( k = l, k_ = k - 1, _do11 = i - 1; k <= _do11; k++, k_++ ) { 
      
      //           The first iteration of this loop determines a reflection G
      //           from the vector V and applies it from left and right to H,
      //           thus creating a nonzero bulge below the subdiagonal.
      
      //           Each subsequent iteration determines a reflection G to
      //           restore the Hessenberg form in the (K-1)th column, and thus
      //           chases the bulge one step toward the bottom of the active
      //           submatrix. NR is the order of G.
      
      nr = min( ns + 1, i - k + 1 );
      if( k > l ) 
        dcopy( nr, &H(k_ - 1,k_), 1, v, 1 );
      dlarfg( nr, v[0], &v[1], 1, tau );
      if( k > l ) { 
        H(k_ - 1,k_) = v[0];
        for( ii = k + 1, ii_ = ii - 1, _do12 = i; ii <= _do12; ii++, ii_++ ) { 
          H(k_ - 1,ii_) = ZERO;
        }
      }
      v[0] = ONE;
      
      //           Apply G from the left to transform the rows of the matrix in
      //           columns K to I2.
      
      dlarfx( 'L'/* Left */, nr, i2 - k + 1, v, tau, &H(k_,k_), 
       ldh, work );
      
      //           Apply G from the right to transform the columns of the
      //           matrix in rows I1 to min(K+NR,I).
      
      dlarfx( 'R'/* Right */, min( k + nr, i ) - i1 + 1, nr, 
       v, tau, &H(k_,i1 - 1), ldh, work );
      
      if( wantz ) { 
        
        //              Accumulate transformations in the matrix Z
        
        dlarfx( 'R'/* Right */, nh, nr, v, tau, &Z(k_,ilo - 1), 
         ldz, work );
      }
    }
    
  }
  
  //     Failure to converge in remaining number of iterations
  
  info = i;
  rel_chrtmp(_c,NCHRTMPS);
  return;
  
L_160:
  ;
  
  //     A submatrix of order <= MAXB in rows and columns L to I has split
  //     off. Use the double-shift QR algorithm to handle it.
  
  dlahqr( wantt, wantz, n, l, i, h, ldh, wr, wi, ilo, ihi, z, ldz, 
   info );
  if( info > 0 ) {
    rel_chrtmp(_c,NCHRTMPS);
    return;
  }
  
  //     Decrement number of remaining iterations, and return to start of
  //     the main loop with a new value of I.
  
  itn = itn - its;
  i = l - 1;
  goto L_50;
  
L_170:
  ;
  rel_chrtmp(_c,NCHRTMPS);
  return;
  
  //     End of DHSEQR
  
#undef  NCHRTMPS
#undef  Z
#undef  H
} // end of function 


/*
 * C++ implementation of Lapack routine zgtrfs
 *
 * $Id: zgtrfs.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:47:03
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zgtrfs.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const long ITMAX = 5;
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
const double TWO = 2.0e0;
const double THREE = 3.0e0;
// end of PARAMETER translations

inline double zgtrfs_cabs1(DComplex zdum) { return abs( real( (zdum) ) ) + 
   abs( imag( (zdum) ) ); }
RWLAPKDECL void /*FUNCTION*/ zgtrfs(const char &trans, const long &n, const long &nrhs, 
 DComplex dl[], DComplex d[], DComplex du[], DComplex dlf[], DComplex df[], 
 DComplex duf[], DComplex du2[], long ipiv[], DComplex *b, const long &ldb, 
 DComplex *x, const long &ldx, double ferr[], double berr[], DComplex work[], 
 double rwork[], long &info)
{
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
#define X(I_,J_)  (*(x+(I_)*(ldx)+(J_)))
  int notran;
  char transn, transt;
  long _do0, _do1, _do2, _do3, _do4, _do5, _do6, _do7, _do8, 
   count, i, i_, j, j_, kase, nz;
  double eps, lstres, s, safe1, safe2, safmin;
  DComplex zdum;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZGTRFS improves the computed solution to a system of linear
  //  equations when the coefficient matrix is tridiagonal and provides
  //  error bounds and backward error estimates for the solutions.
  
  //  Arguments
  //  =========
  
  //  TRANS   (input) CHARACTER*1
  //          Specifies the form of the system of equations.
  //          = 'N':  A * X = B     (No transpose)
  //          = 'T':  A**T * X = B  (Transpose)
  //          = 'C':  A**H * X = B  (Conjugate transpose)
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right hand sides, i.e., the number of columns
  //          of the matrix B.  NRHS >= 0.
  
  //  DL      (input) COMPLEX*16 array, dimension (N-1)
  //          The (n-1) sub-diagonal elements of A.
  
  //  D       (input) COMPLEX*16 array, dimension (N)
  //          The diagonal elements of A.
  
  //  DU      (input) COMPLEX*16 array, dimension (N-1)
  //          The (n-1) super-diagonal elements of A.
  
  //  DLF     (input) COMPLEX*16 array, dimension (N-1)
  //          The (n-1) multipliers that define the matrix L from the
  //          LU factorization of A as computed by ZGTTRF.
  
  //  DF      (input) COMPLEX*16 array, dimension (N)
  //          The n diagonal elements of the upper triangular matrix U from
  //          the LU factorization of A.
  
  //  DUF     (input) COMPLEX*16 array, dimension (N-1)
  //          The (n-1) elements of the first super-diagonal of U.
  
  //  DU2     (input) COMPLEX*16 array, dimension (N-2)
  //          The (n-2) elements of the second super-diagonal of U.
  
  //  IPIV    (input) INTEGER array, dimension (N)
  //          The pivot indices; for 1 <= i <= n, row i of the matrix was
  //          interchanged with row IPIV(i).  IPIV(i) will always be either
  //          i or i+1; IPIV(i) = i indicates a row interchange was not
  //          required.
  
  //  B       (input) COMPLEX*16 array, dimension (LDB,NRHS)
  //          The right hand side vectors for the system of linear
  //          equations.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  X       (input/output) COMPLEX*16 array, dimension (LDX,NRHS)
  //          On entry, the solution vectors.
  //          On exit, the improved solution vectors.
  
  //  LDX     (input) INTEGER
  //          The leading dimension of the array X.  LDX >= max(1,N).
  
  //  FERR    (output) DOUBLE PRECISION array, dimension (NRHS)
  //          The estimated forward error bounds for each solution vector
  //          X.  If XTRUE is the true solution, FERR bounds the magnitude
  //          of the largest entry in (X - XTRUE) divided by the magnitude
  //          of the largest entry in X.  The quality of the error bound
  //          depends on the quality of the estimate of norm(inv(A))
  //          computed in the code; if the estimate of norm(inv(A)) is
  //          accurate, the error bound is guaranteed.
  
  //  BERR    (output) DOUBLE PRECISION array, dimension (NRHS)
  //          The componentwise relative backward error of each solution
  //          vector (i.e., the smallest relative change in any entry of A
  //          or B that makes X an exact solution).
  
  //  WORK    (workspace) COMPLEX*16 array, dimension (2*N)
  
  //  RWORK   (workspace) INTEGER array, dimension (N)
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  
  //  Internal Parameters
  //  ===================
  
  //  ITMAX is the maximum number of steps of iterative refinement.
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Statement Functions ..
  //     ..
  //     .. Statement Function definitions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  notran = lsame( trans, 'N' );
  if( (!notran && !lsame( trans, 'T' )) && !lsame( trans, 'C' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( nrhs < 0 ) { 
    info = -3;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -13;
  }
  else if( ldx < max( 1, n ) ) { 
    info = -15;
  }
  if( info != 0 ) { 
    xerbla( "ZGTRFS", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 || nrhs == 0 ) { 
    for( j = 1, j_ = j - 1, _do0 = nrhs; j <= _do0; j++, j_++ ) { 
      ferr[j_] = ZERO;
      berr[j_] = ZERO;
    }
    return;
  }
  
  if( notran ) { 
    transn = 'N';
    transt = 'C';
  }
  else { 
    transn = 'C';
    transt = 'N';
  }
  
  //     NZ = maximum number of nonzero entries in each row of A, plus 1
  
  nz = 4;
  eps = dlamch( 'E'/*Epsilon*/ );
  safmin = dlamch( 'S'/*Safe minimum*/ );
  safe1 = nz*safmin;
  safe2 = safe1/eps;
  
  //     Do for each right hand side
  
  for( j = 1, j_ = j - 1, _do1 = nrhs; j <= _do1; j++, j_++ ) { 
    
    count = 1;
    lstres = THREE;
L_20:
    ;
    
    //        Loop until stopping criterion is satisfied.
    
    //        Compute residual R = B - op(A) * X,
    //        where op(A) = A, A**T, or A**H, depending on TRANS.
    
    zcopy( n, &B(j_,0), 1, work, 1 );
    zlagtm( trans, n, 1, -ONE, dl, d, du, &X(j_,0), ldx, ONE, 
     work, n );
    
    //        Compute abs(op(A))*abs(x) + abs(b) for use in the backward
    //        error bound.
    
    if( notran ) { 
      if( n == 1 ) { 
        rwork[0] = zgtrfs_cabs1( B(j_,0) ) + zgtrfs_cabs1( d[0] )*
         zgtrfs_cabs1( X(j_,0) );
      }
      else { 
        rwork[0] = zgtrfs_cabs1( B(j_,0) ) + zgtrfs_cabs1( d[0] )*
         zgtrfs_cabs1( X(j_,0) ) + zgtrfs_cabs1( du[0] )*zgtrfs_cabs1( X(j_,1) );
        for( i = 2, i_ = i - 1, _do2 = n - 1; i <= _do2; i++, i_++ ) { 
          rwork[i_] = zgtrfs_cabs1( B(j_,i_) ) + zgtrfs_cabs1( dl[i_ - 1] )*
           zgtrfs_cabs1( X(j_,i_ - 1) ) + zgtrfs_cabs1( d[i_] )*
           zgtrfs_cabs1( X(j_,i_) ) + zgtrfs_cabs1( du[i_] )*
           zgtrfs_cabs1( X(j_,i_ + 1) );
        }
        rwork[n - 1] = zgtrfs_cabs1( B(j_,n - 1) ) + zgtrfs_cabs1( dl[n - 2] )*
         zgtrfs_cabs1( X(j_,n - 2) ) + zgtrfs_cabs1( d[n - 1] )*
         zgtrfs_cabs1( X(j_,n - 1) );
      }
    }
    else { 
      if( n == 1 ) { 
        rwork[0] = zgtrfs_cabs1( B(j_,0) ) + zgtrfs_cabs1( d[0] )*
         zgtrfs_cabs1( X(j_,0) );
      }
      else { 
        rwork[0] = zgtrfs_cabs1( B(j_,0) ) + zgtrfs_cabs1( d[0] )*
         zgtrfs_cabs1( X(j_,0) ) + zgtrfs_cabs1( dl[0] )*zgtrfs_cabs1( X(j_,1) );
        for( i = 2, i_ = i - 1, _do3 = n - 1; i <= _do3; i++, i_++ ) { 
          rwork[i_] = zgtrfs_cabs1( B(j_,i_) ) + zgtrfs_cabs1( du[i_ - 1] )*
           zgtrfs_cabs1( X(j_,i_ - 1) ) + zgtrfs_cabs1( d[i_] )*
           zgtrfs_cabs1( X(j_,i_) ) + zgtrfs_cabs1( dl[i_] )*
           zgtrfs_cabs1( X(j_,i_ + 1) );
        }
        rwork[n - 1] = zgtrfs_cabs1( B(j_,n - 1) ) + zgtrfs_cabs1( du[n - 2] )*
         zgtrfs_cabs1( X(j_,n - 2) ) + zgtrfs_cabs1( d[n - 1] )*
         zgtrfs_cabs1( X(j_,n - 1) );
      }
    }
    
    //        Compute componentwise relative backward error from formula
    
    //        max(i) ( abs(R(i)) / ( abs(op(A))*abs(X) + abs(B) )(i) )
    
    //        where abs(Z) is the componentwise absolute value of the matrix
    //        or vector Z.  If the i-th component of the denominator is less
    //        than SAFE2, then SAFE1 is added to the i-th components of the
    //        numerator and denominator before dividing.
    
    s = ZERO;
    for( i = 1, i_ = i - 1, _do4 = n; i <= _do4; i++, i_++ ) { 
      if( rwork[i_] > safe2 ) { 
        s = max( s, zgtrfs_cabs1( work[i_] )/rwork[i_] );
      }
      else { 
        s = max( s, (zgtrfs_cabs1( work[i_] ) + safe1)/(rwork[i_] + 
         safe1) );
      }
    }
    berr[j_] = s;
    
    //        Test stopping criterion. Continue iterating if
    //           1) The residual BERR(J) is larger than machine epsilon, and
    //           2) BERR(J) decreased by at least a factor of 2 during the
    //              last iteration, and
    //           3) At most ITMAX iterations tried.
    
    if( (berr[j_] > eps && TWO*berr[j_] <= lstres) && count <= 
     ITMAX ) { 
      
      //           Update solution and try again.
      
      zgttrs( trans, n, 1, dlf, df, duf, du2, ipiv, work, n, 
       info );
      zaxpy( n, DComplex( ONE, 0. ), work, 1, &X(j_,0), 1 );
      lstres = berr[j_];
      count = count + 1;
      goto L_20;
    }
    
    //        Bound error from formula
    
    //        norm(X - XTRUE) / norm(X) .le. FERR =
    //        norm( abs(inv(op(A)))*
    //           ( abs(R) + NZ*EPS*( abs(op(A))*abs(X)+abs(B) ))) / norm(X)
    
    //        where
    //          norm(Z) is the magnitude of the largest component of Z
    //          inv(op(A)) is the inverse of op(A)
    //          abs(Z) is the componentwise absolute value of the matrix or
    //             vector Z
    //          NZ is the maximum number of nonzeros in any row of A, plus 1
    //          EPS is machine epsilon
    
    //        The i-th component of abs(R)+NZ*EPS*(abs(op(A))*abs(X)+abs(B))
    //        is incremented by SAFE1 if the i-th component of
    //        abs(op(A))*abs(X) + abs(B) is less than SAFE2.
    
    //        Use ZLACON to estimate the infinity-norm of the matrix
    //           inv(op(A)) * diag(W),
    //        where W = abs(R) + NZ*EPS*( abs(op(A))*abs(X)+abs(B) )))
    
    for( i = 1, i_ = i - 1, _do5 = n; i <= _do5; i++, i_++ ) { 
      if( rwork[i_] > safe2 ) { 
        rwork[i_] = zgtrfs_cabs1( work[i_] ) + nz*eps*rwork[i_];
      }
      else { 
        rwork[i_] = zgtrfs_cabs1( work[i_] ) + nz*eps*rwork[i_] + 
         safe1;
      }
    }
    
    kase = 0;
L_70:
    ;
    zlacon( n, &work[n], work, ferr[j_], kase );
    if( kase != 0 ) { 
      if( kase == 1 ) { 
        
        //              Multiply by diag(W)*inv(op(A)**H).
        
        zgttrs( transt, n, 1, dlf, df, duf, du2, ipiv, work, 
         n, info );
        for( i = 1, i_ = i - 1, _do6 = n; i <= _do6; i++, i_++ ) { 
          work[i_] = rwork[i_]*work[i_];
        }
      }
      else { 
        
        //              Multiply by inv(op(A))*diag(W).
        
        for( i = 1, i_ = i - 1, _do7 = n; i <= _do7; i++, i_++ ) { 
          work[i_] = rwork[i_]*work[i_];
        }
        zgttrs( transn, n, 1, dlf, df, duf, du2, ipiv, work, 
         n, info );
      }
      goto L_70;
    }
    
    //        Normalize error.
    
    lstres = ZERO;
    for( i = 1, i_ = i - 1, _do8 = n; i <= _do8; i++, i_++ ) { 
      lstres = max( lstres, zgtrfs_cabs1( X(j_,i_) ) );
    }
    if( lstres != ZERO ) 
      ferr[j_] = ferr[j_]/lstres;
    
  }
  
  return;
  
  //     End of ZGTRFS
  
#undef  X
#undef  B
} // end of function 


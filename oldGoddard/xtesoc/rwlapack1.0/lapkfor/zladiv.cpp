/*
 * C++ implementation of Lapack routine zladiv
 *
 * $Id: zladiv.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:48:13
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zladiv.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

RWLAPKDECL DComplex /*FUNCTION*/ zladiv(const DComplex &x, const DComplex &y)
{
  double zi, zr;
  DComplex zladiv_v;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZLADIV := X / Y, where X and Y are DComplex.  The computation of X / Y
  //  will not overflow on an intermediary step unless the results
  //  overflows.
  
  //  Arguments
  //  =========
  
  //  X       (input) COMPLEX*16
  //  Y       (input) COMPLEX*16
  //          The DComplex scalars X and Y.
  
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  dladiv( real( x ), imag( x ), real( y ), imag( y ), zr, zi );
  zladiv_v = DComplex( zr, zi );
  
  return( zladiv_v );
  
  //     End of ZLADIV
  
} // end of function 


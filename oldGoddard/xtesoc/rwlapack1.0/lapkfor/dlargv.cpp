/*
 * C++ implementation of lapack routine dlargv
 *
 * $Id: dlargv.cpp,v 1.4 1993/04/06 20:41:22 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:36:01
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dlargv.cpp,v $
 * Revision 1.4  1993/04/06  20:41:22  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.3  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.2  1993/03/05  23:15:45  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:07:38  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dlargv(const long &n, double x[], const long &incx, 
 double y[], const long &incy, double c[], const long &incc)
{
  long _do0, i, i_, ic, ix, iy;
  double tt, w, xi, yi;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DLARGV generates a vector of real plane rotations, determined by
  //  elements of the real vectors x and y. For i = 1,2,...,n
  
  //     (  c(i)  s(i) ) ( x(i) ) = ( a(i) )
  //     ( -s(i)  c(i) ) ( y(i) ) = (   0  )
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The number of plane rotations to be generated.
  
  //  X       (input/output) DOUBLE PRECISION array,
  //                         dimension (1+(N-1)*INCX)
  //          On entry, the vector x.
  //          On exit, x(i) is overwritten by a(i), for i = 1,...,n.
  
  //  INCX    (input) INTEGER
  //          The increment between elements of X. INCX > 0.
  
  //  Y       (input/output) DOUBLE PRECISION array,
  //                         dimension (1+(N-1)*INCY)
  //          On entry, the vector y.
  //          On exit, the sines of the plane rotations.
  
  //  INCY    (input) INTEGER
  //          The increment between elements of Y. INCY > 0.
  
  //  C       (output) DOUBLE PRECISION array, dimension (1+(N-1)*INCC)
  //          The cosines of the plane rotations.
  
  //  INCC    (input) INTEGER
  //          The increment between elements of C. INCC > 0.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  ix = 1;
  iy = 1;
  ic = 1;
  for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
    xi = x[ix - 1];
    yi = y[iy - 1];
    if( xi == ZERO ) { 
      c[ic - 1] = ZERO;
      y[iy - 1] = ONE;
      x[ix - 1] = yi;
    }
    else { 
      w = max( abs( xi ), abs( yi ) );
      xi = xi/w;
      yi = yi/w;
      tt = sqrt( xi*xi + yi*yi );
      c[ic - 1] = xi/tt;
      y[iy - 1] = yi/tt;
      x[ix - 1] = w*tt;
    }
    ix = ix + incx;
    iy = iy + incy;
    ic = ic + incc;
  }
  return;
  
  //     End of DLARGV
  
} // end of function 


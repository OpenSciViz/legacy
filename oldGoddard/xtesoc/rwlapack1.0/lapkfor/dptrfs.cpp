/*
 * C++ implementation of lapack routine dptrfs
 *
 * $Id: dptrfs.cpp,v 1.6 1993/04/06 20:42:11 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:37:42
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dptrfs.cpp,v $
 * Revision 1.6  1993/04/06  20:42:11  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.5  1993/03/19  18:41:23  alv
 * now passes chars explicitly, rather than indirection of a string, to shut up SUN warnings
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:16:53  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:08:39  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const long ITMAX = 5;
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
const double TWO = 2.0e0;
const double THREE = 3.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dptrfs(const long &n, const long &nrhs, double d[], 
 double e[], double df[], double ef[], double *b, const long &ldb, double *x, 
 const long &ldx, double ferr[], double berr[], double work[], long &info)
{
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
#define X(I_,J_)  (*(x+(I_)*(ldx)+(J_)))
  long _do0, _do1, _do2, _do3, _do4, _do5, _do6, count, i, i_, 
   ix, j, j_, nz;
  double bi, cx, dx, eps, ex, lstres, s, safe1, safe2, safmin;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DPTRFS improves the computed solution to a system of linear
  //  equations when the coefficient matrix is symmetric positive definite
  //  and tridiagonal and provides error bounds and backward error
  //  estimates for the solutions.
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right hand sides, i.e., the number of columns
  //          of the matrix B.  NRHS >= 0.
  
  //  D       (input) DOUBLE PRECISION array, dimension (N)
  //          The n diagonal elements of the tridiagonal matrix A.
  
  //  E       (input) DOUBLE PRECISION array, dimension (N-1)
  //          The (n-1) subdiagonal elements of the tridiagonal matrix A.
  
  //  DF      (input) DOUBLE PRECISION array, dimension (N)
  //          The n diagonal elements of the diagonal matrix D from the
  //          L*D*L' factorization of A.
  
  //  EF      (input) DOUBLE PRECISION array, dimension (N-1)
  //          The (n-1) subdiagonal elements of the unit bidiagonal factor
  //          L from the L*D*L' factorization of A.
  
  //  B       (input) DOUBLE PRECISION array, dimension (LDB,NRHS)
  //          The right hand side vectors for the system of linear
  //          equations.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  X       (input/output) DOUBLE PRECISION array, dimension (LDX,NRHS)
  //          On entry, the solution vectors.
  //          On exit, the improved solution vectors.
  
  //  LDX     (input) INTEGER
  //          The leading dimension of the array X.  LDX >= max(1,N).
  
  //  FERR    (output) DOUBLE PRECISION array, dimension (NRHS)
  //          The estimated forward error bounds for each solution vector
  //          X.  If XTRUE is the true solution, FERR bounds the magnitude
  //          of the largest entry in (X - XTRUE) divided by the magnitude
  //          of the largest entry in X.  The quality of the error bound
  //          depends on the quality of the estimate of norm(inv(A))
  //          computed in the code; if the estimate of norm(inv(A)) is
  //          accurate, the error bound is guaranteed.
  
  //  BERR    (output) DOUBLE PRECISION array, dimension (NRHS)
  //          The componentwise relative backward error of each solution
  //          vector (i.e., the smallest relative change in any entry of A
  //          or B that makes X an exact solution).
  
  //  WORK    (workspace) DOUBLE PRECISION array, dimension (2*N)
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  
  //  Internal Parameters
  //  ===================
  
  //  ITMAX is the maximum number of steps of iterative refinement.
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  if( n < 0 ) { 
    info = -1;
  }
  else if( nrhs < 0 ) { 
    info = -2;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -8;
  }
  else if( ldx < max( 1, n ) ) { 
    info = -10;
  }
  if( info != 0 ) { 
    xerbla( "DPTRFS", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 || nrhs == 0 ) { 
    for( j = 1, j_ = j - 1, _do0 = nrhs; j <= _do0; j++, j_++ ) { 
      ferr[j_] = ZERO;
      berr[j_] = ZERO;
    }
    return;
  }
  
  //     NZ = maximum number of nonzero entries in each row of A, plus 1
  
  nz = 4;
  eps = dlamch( 'E'/* Epsilon */ );
  safmin = dlamch( 'S'/* Safe minimum */ );
  safe1 = nz*safmin;
  safe2 = safe1/eps;
  
  //     Do for each right hand side
  
  for( j = 1, j_ = j - 1, _do1 = nrhs; j <= _do1; j++, j_++ ) { 
    
    count = 1;
    lstres = THREE;
L_20:
    ;
    
    //        Loop until stopping criterion is satisfied.
    
    //        Compute residual R = B - A * X.  Also compute
    //        abs(A)*abs(x) + abs(b) for use in the backward error bound.
    
    if( n == 1 ) { 
      bi = B(j_,0);
      dx = d[0]*X(j_,0);
      work[n] = bi - dx;
      work[0] = abs( bi ) + abs( dx );
    }
    else { 
      bi = B(j_,0);
      dx = d[0]*X(j_,0);
      ex = e[0]*X(j_,1);
      work[n] = bi - dx - ex;
      work[0] = abs( bi ) + abs( dx ) + abs( ex );
      for( i = 2, i_ = i - 1, _do2 = n - 1; i <= _do2; i++, i_++ ) { 
        bi = B(j_,i_);
        cx = e[i_ - 1]*X(j_,i_ - 1);
        dx = d[i_]*X(j_,i_);
        ex = e[i_]*X(j_,i_ + 1);
        work[n + i_] = bi - cx - dx - ex;
        work[i_] = abs( bi ) + abs( cx ) + abs( dx ) + abs( ex );
      }
      bi = B(j_,n - 1);
      cx = e[n - 2]*X(j_,n - 2);
      dx = d[n - 1]*X(j_,n - 1);
      work[n + n - 1] = bi - cx - dx;
      work[n - 1] = abs( bi ) + abs( cx ) + abs( dx );
    }
    
    //        Compute componentwise relative backward error from formula
    
    //        max(i) ( abs(R(i)) / ( abs(A)*abs(X) + abs(B) )(i) )
    
    //        where abs(Z) is the componentwise absolute value of the matrix
    //        or vector Z.  If the i-th component of the denominator is less
    //        than SAFE2, then SAFE1 is added to the i-th components of the
    //        numerator and denominator before dividing.
    
    s = ZERO;
    for( i = 1, i_ = i - 1, _do3 = n; i <= _do3; i++, i_++ ) { 
      if( work[i_] > safe2 ) { 
        s = max( s, abs( work[n + i_] )/work[i_] );
      }
      else { 
        s = max( s, (abs( work[n + i_] ) + safe1)/(work[i_] + 
         safe1) );
      }
    }
    berr[j_] = s;
    
    //        Test stopping criterion. Continue iterating if
    //           1) The residual BERR(J) is larger than machine epsilon, and
    //           2) BERR(J) decreased by at least a factor of 2 during the
    //              last iteration, and
    //           3) At most ITMAX iterations tried.
    
    if( (berr[j_] > eps && TWO*berr[j_] <= lstres) && count <= 
     ITMAX ) { 
      
      //           Update solution and try again.
      
      dpttrs( n, 1, df, ef, &work[n], n, info );
      daxpy( n, ONE, &work[n], 1, &X(j_,0), 1 );
      lstres = berr[j_];
      count = count + 1;
      goto L_20;
    }
    
    //        Bound error from formula
    
    //        norm(X - XTRUE) / norm(X) .le. FERR =
    //        norm( abs(inv(A))*
    //           ( abs(R) + NZ*EPS*( abs(A)*abs(X)+abs(B) ))) / norm(X)
    
    //        where
    //          norm(Z) is the magnitude of the largest component of Z
    //          inv(A) is the inverse of A
    //          abs(Z) is the componentwise absolute value of the matrix or
    //             vector Z
    //          NZ is the maximum number of nonzeros in any row of A, plus 1
    //          EPS is machine epsilon
    
    //        The i-th component of abs(R)+NZ*EPS*(abs(A)*abs(X)+abs(B))
    //        is incremented by SAFE1 if the i-th component of
    //        abs(A)*abs(X) + abs(B) is less than SAFE2.
    
    for( i = 1, i_ = i - 1, _do4 = n; i <= _do4; i++, i_++ ) { 
      if( work[i_] > safe2 ) { 
        work[i_] = abs( work[n + i_] ) + nz*eps*work[i_];
      }
      else { 
        work[i_] = abs( work[n + i_] ) + nz*eps*work[i_] + 
         safe1;
      }
    }
    ix = idamax( n, work, 1 );
    ferr[j_] = work[ix - 1];
    
    //        Estimate the norm of inv(A).
    
    //        Solve M(A) * x = e, where M(A) = (m(i,j)) is given by
    
    //           m(i,j) =  abs(A(i,j)), i = j,
    //           m(i,j) = -abs(A(i,j)), i .ne. j,
    
    //        and e = [ 1, 1, ..., 1 ]'.  Note M(A) = M(L)*D*M(L)'.
    
    //        Solve M(L) * x = e.
    
    work[0] = ONE;
    for( i = 2, i_ = i - 1, _do5 = n; i <= _do5; i++, i_++ ) { 
      work[i_] = ONE + work[i_ - 1]*abs( ef[i_ - 1] );
    }
    
    //        Solve D * M(L)' * x = b.
    
    work[n - 1] = work[n - 1]/df[n - 1];
    for( i = n - 1, i_ = i - 1; i >= 1; i--, i_-- ) { 
      work[i_] = work[i_]/df[i_] + work[i_ + 1]*abs( ef[i_] );
    }
    
    //        Compute norm(inv(A)) = max(x(i)), 1<=i<=n.
    
    ix = idamax( n, work, 1 );
    ferr[j_] = ferr[j_]*abs( work[ix - 1] );
    
    //        Normalize error.
    
    lstres = ZERO;
    for( i = 1, i_ = i - 1, _do6 = n; i <= _do6; i++, i_++ ) { 
      lstres = max( lstres, abs( X(j_,i_) ) );
    }
    if( lstres != ZERO ) 
      ferr[j_] = ferr[j_]/lstres;
    
  }
  
  return;
  
  //     End of DPTRFS
  
#undef  X
#undef  B
} // end of function 


/*
 * C++ implementation of lapack routine dpptrf
 *
 * $Id: dpptrf.cpp,v 1.6 1993/04/06 20:42:08 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:37:36
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dpptrf.cpp,v $
 * Revision 1.6  1993/04/06  20:42:08  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.5  1993/03/19  18:41:23  alv
 * now passes chars explicitly, rather than indirection of a string, to shut up SUN warnings
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:16:49  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:08:35  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ONE = 1.0e0;
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dpptrf(const char &uplo, const long &n, double ap[], long &info)
{
  int upper;
  long _do0, _do1, j, j_, jc, jj;
  double ajj;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DPPTRF computes the Cholesky factorization of a real symmetric
  //  positive definite matrix A stored in packed format.
  
  //  The factorization has the form
  //     A = U' * U ,  if UPLO = 'U', or
  //     A = L  * L',  if UPLO = 'L',
  //  where U is an upper triangular matrix and L is lower triangular.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangular part of the
  //          symmetric matrix A is stored:
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  AP      (input/output) DOUBLE PRECISION array, dimension (N*(N+1)/2)
  //          On entry, the upper or lower triangle of the symmetric matrix
  //          A, packed columnwise in a linear array.  The j-th column of A
  //          is stored in the array AP as follows:
  //          if UPLO = 'U', AP(i + (j-1)*j/2) = A(i,j) for 1<=i<=j;
  //          if UPLO = 'L', AP(i + (j-1)*(2n-j)/2) = A(i,j) for j<=i<=n.
  //          See below for further details.
  
  //          On exit, if INFO = 0, the triangular factor U or L from the
  //          Cholesky factorization A = U'*U or A = L*L', in the same
  //          storage format as A.
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: if INFO = k, the leading minor of order k is not
  //               positive definite, and the factorization could not be
  //               completed.
  
  //  Further Details
  //  ======= =======
  
  //  The packed storage scheme is illustrated by the following example
  //  when N = 4, UPLO = 'U':
  
  //  Two-dimensional storage of the symmetric matrix A:
  
  //     a11 a12 a13 a14
  //         a22 a23 a24
  //             a33 a34     (aij = aji)
  //                 a44
  
  //  Packed storage of the upper triangle of A:
  
  //  AP = [ a11, a12, a22, a13, a23, a33, a14, a24, a34, a44 ]
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  if( info != 0 ) { 
    xerbla( "DPPTRF", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  if( upper ) { 
    
    //        Compute the Cholesky factorization A = U'*U.
    
    jj = 0;
    for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
      jc = jj + 1;
      jj = jj + j;
      
      //           Compute elements 1:J-1 of column J.
      
      if( j > 1 ) 
        dtpsv( 'U'/* Upper */, 'T'/* Transpose */, 'N'/* Non-unit */
         , j - 1, ap, &ap[jc - 1], 1 );
      
      //           Compute U(J,J) and test for non-positive-definiteness.
      
      ajj = ap[jj - 1] - ddot( j - 1, &ap[jc - 1], 1, &ap[jc - 1], 
       1 );
      if( ajj <= ZERO ) { 
        ap[jj - 1] = ajj;
        goto L_30;
      }
      ap[jj - 1] = sqrt( ajj );
    }
  }
  else { 
    
    //        Compute the Cholesky factorization A = L*L'.
    
    jj = 1;
    for( j = 1, j_ = j - 1, _do1 = n; j <= _do1; j++, j_++ ) { 
      
      //           Compute L(J,J) and test for non-positive-definiteness.
      
      ajj = ap[jj - 1];
      if( ajj <= ZERO ) { 
        ap[jj - 1] = ajj;
        goto L_30;
      }
      ajj = sqrt( ajj );
      ap[jj - 1] = ajj;
      
      //           Compute elements J+1:N of column J and update the trailing
      //           submatrix.
      
      if( j < n ) { 
        dscal( n - j, ONE/ajj, &ap[jj], 1 );
        dspr( 'L'/* Lower */, n - j, -ONE, &ap[jj], 1, &ap[jj + n - j] );
        jj = jj + n - j + 1;
      }
    }
  }
  goto L_40;
  
L_30:
  ;
  info = j;
  
L_40:
  ;
  return;
  
  //     End of DPPTRF
  
} // end of function 


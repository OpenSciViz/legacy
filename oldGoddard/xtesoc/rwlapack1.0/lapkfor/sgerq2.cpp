/*
 * C++ implementation of Lapack routine sgerq2
 *
 * $Id: sgerq2.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:58:50
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: sgerq2.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ sgerq2(const long &m, const long &n, float *a, const long &lda, 
 float tau[], float work[], long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  long i, i_, k;
  float aii;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SGERQ2 computes an RQ factorization of a real m by n matrix A:
  //  A = R * Q.
  
  //  Arguments
  //  =========
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix A.  M >= 0.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix A.  N >= 0.
  
  //  A       (input/output) REAL array, dimension (LDA,N)
  //          On entry, the m by n matrix A.
  //          On exit, if m <= n, the upper triangle of the subarray
  //          A(1:m,n-m+1:n) contains the m by m upper triangular matrix R;
  //          if m >= n, the elements on and above the (m-n)-th subdiagonal
  //          contain the m by n upper trapezoidal matrix R; the remaining
  //          elements, with the array TAU, represent the orthogonal matrix
  //          Q as a product of elementary reflectors (see Further
  //          Details).
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,M).
  
  //  TAU     (output) REAL array, dimension (min(M,N))
  //          The scalar factors of the elementary reflectors (see Further
  //          Details).
  
  //  WORK    (workspace) REAL array, dimension (M)
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument had an illegal value
  
  //  Further Details
  //  ===============
  
  //  The matrix Q is represented as a product of elementary reflectors
  
  //     Q = H(1) H(2) . . . H(k), where k = min(m,n).
  
  //  Each H(i) has the form
  
  //     H(i) = I - tau * v * v'
  
  //  where tau is a real scalar, and v is a real vector with
  //  v(n-k+i+1:n) = 0 and v(n-k+i) = 1; v(1:n-k+i-1) is stored on exit in
  //  A(m-k+i,1:n-k+i-1), and tau in TAU(i).
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input arguments
  
  info = 0;
  if( m < 0 ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( lda < max( 1, m ) ) { 
    info = -4;
  }
  if( info != 0 ) { 
    xerbla( "SGERQ2", -info );
    return;
  }
  
  k = min( m, n );
  
  for( i = k, i_ = i - 1; i >= 1; i--, i_-- ) { 
    
    //        Generate elementary reflector H(i) to annihilate
    //        A(m-k+i,1:n-k+i-1)
    
    slarfg( n - k + i, A(n - k + i_,m - k + i_), &A(0,m - k + i_), 
     lda, tau[i_] );
    
    //        Apply H(i) to A(1:m-k+i-1,1:n-k+i) from the right
    
    aii = A(n - k + i_,m - k + i_);
    A(n - k + i_,m - k + i_) = ONE;
    slarf( 'R'/*Right*/, m - k + i - 1, n - k + i, &A(0,m - k + i_), 
     lda, tau[i_], a, lda, work );
    A(n - k + i_,m - k + i_) = aii;
  }
  return;
  
  //     End of SGERQ2
  
#undef  A
} // end of function 


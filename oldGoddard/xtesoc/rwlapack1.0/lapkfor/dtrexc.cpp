/*
 * C++ implementation of lapack routine dtrexc
 *
 * $Id: dtrexc.cpp,v 1.6 1993/04/06 20:42:49 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:39:02
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dtrexc.cpp,v $
 * Revision 1.6  1993/04/06  20:42:49  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.5  1993/03/19  21:41:05  alv
 * const added to args
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:17:51  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:09:27  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dtrexc(const char &compq, const long &n, double *t, const long &ldt, 
 double *q, const long &ldq, long &ifst, long &ilst, double work[], 
 long &info)
{
#define T(I_,J_)  (*(t+(I_)*(ldt)+(J_)))
#define Q(I_,J_)  (*(q+(I_)*(ldq)+(J_)))
  int wantq;
  long here, nbf, nbl, nbnext;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DTREXC reorders the real Schur factorization of a real matrix
  //  A = Q*T*Q', so that the diagonal block of T with row index IFST is
  //  moved to row ILST.
  
  //  The real Schur form T is reordered by an orthogonal similarity
  //  transformation Z'*T*Z, and optionally the matrix Q of Schur vectors
  //  is updated by postmultiplying it with Z.
  
  //  T must be in Schur canonical form, that is, block upper triangular
  //  with 1-by-1 and 2-by-2 diagonal blocks; each 2-by-2 diagonal block
  //  has its diagonal elemnts equal and its off-diagonal elements of
  //  opposite sign.
  
  //  Arguments
  //  =========
  
  //  COMPQ   (input) CHARACTER*1
  //          = 'V': update the matrix Q of Schur vectors;
  //          = 'N': do not update Q.
  
  //  N       (input) INTEGER
  //          The order of the matrix T. N >= 0.
  
  //  T       (input/output) DOUBLE PRECISION array, dimension (LDT,N)
  //          On entry, the upper quasi-triangular matrix T, in Schur
  //          Schur canonical form.
  //          On exit, T is overwritten by the reordered matrix T, again in
  //          Schur canonical form..
  
  //  LDT     (input) INTEGER
  //          The leading dimension of the array T. LDT >= max(1,N).
  
  //  Q       (input/output) DOUBLE PRECISION array, dimension (LDQ,N)
  //          On entry, if COMPQ = 'V', the matrix Q of Schur vectors.
  //          On exit, if COMPQ = 'V', Q has been postmultiplied by the
  //          orthogonal transformation matrix Z which reorders T.
  //          If COMPQ = 'N', Q is not referenced.
  
  //  LDQ     (input) INTEGER
  //          The leading dimension of the array Q.
  //          LDQ >= 1; and if COMPQ = 'V', LDQ >= max(1,N).
  
  //  IFST    (input/output) INTEGER
  //  ILST    (input/output) INTEGER
  //          Specify the re-ordering of the diagonal blocks of T.
  //          The block with row index IFST is moved to row ILST, by a
  //          sequence of transpositions between adjacent blocks.
  //          On exit, if IFST pointed on entry to the second row of a
  //          2-by-2 block, it is changed to point to the first row; ILST
  //          always points to the first row of the block in its final
  //          position (which may differ from its input value by +1 or -1).
  //          1 <= IFST <= N; 1 <= ILST <= N.
  
  //  WORK    (workspace) DOUBLE PRECISION array, dimension (N)
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument had an illegal value
  //          = 1: two adjacent blocks were too close to swap (the problem
  //               is very ill-conditioned); T may have been partially
  //               reordered, and ILST points to the first row of the
  //               current position of the block being moved.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Decode and test the input arguments.
  
  info = 0;
  wantq = lsame( compq, 'V' );
  if( !wantq && !lsame( compq, 'N' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( ldt < max( 1, n ) ) { 
    info = -4;
  }
  else if( ldq < 1 || (wantq && ldq < max( 1, n )) ) { 
    info = -6;
  }
  else if( ifst < 1 || ifst > n ) { 
    info = -7;
  }
  else if( ilst < 1 || ilst > n ) { 
    info = -8;
  }
  if( info != 0 ) { 
    xerbla( "DTREXC", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n <= 1 ) 
    return;
  
  //     Determine the first row of specified block
  //     and find out it is 1 by 1 or 2 by 2.
  
  if( ifst > 1 ) { 
    if( T(ifst - 2,ifst - 1) != ZERO ) 
      ifst = ifst - 1;
  }
  nbf = 1;
  if( ifst < n ) { 
    if( T(ifst - 1,ifst) != ZERO ) 
      nbf = 2;
  }
  
  //     Determine the first row of the final block
  //     and find out it is 1 by 1 or 2 by 2.
  
  if( ilst > 1 ) { 
    if( T(ilst - 2,ilst - 1) != ZERO ) 
      ilst = ilst - 1;
  }
  nbl = 1;
  if( ilst < n ) { 
    if( T(ilst - 1,ilst) != ZERO ) 
      nbl = 2;
  }
  
  if( ifst == ilst ) 
    return;
  
  if( ifst < ilst ) { 
    
    //        Update ILST
    
    if( nbf == 2 && nbl == 1 ) 
      ilst = ilst - 1;
    if( nbf == 1 && nbl == 2 ) 
      ilst = ilst + 1;
    
    here = ifst;
    
L_10:
    ;
    
    //        Swap block with next one below
    
    if( nbf == 1 || nbf == 2 ) { 
      
      //           Current block either 1 by 1 or 2 by 2
      
      nbnext = 1;
      if( here + nbf + 1 <= n ) { 
        if( T(here + nbf - 1,here + nbf) != ZERO ) 
          nbnext = 2;
      }
      dlaexc( wantq, n, t, ldt, q, ldq, here, nbf, nbnext, work, 
       info );
      if( info != 0 ) { 
        ilst = here;
        return;
      }
      here = here + nbnext;
      
      //           Test if 2 by 2 block breaks into two 1 by 1 blocks
      
      if( nbf == 2 ) { 
        if( T(here - 1,here) == ZERO ) 
          nbf = 3;
      }
      
    }
    else { 
      
      //           Current block consists of two 1 by 1 blocks each of which
      //           must be swapped individually
      
      nbnext = 1;
      if( here + 3 <= n ) { 
        if( T(here + 1,here + 2) != ZERO ) 
          nbnext = 2;
      }
      dlaexc( wantq, n, t, ldt, q, ldq, here + 1, 1, nbnext, 
       work, info );
      if( info != 0 ) { 
        ilst = here;
        return;
      }
      if( nbnext == 1 ) { 
        
        //              Swap two 1 by 1 blocks, no problems possible
        
        dlaexc( wantq, n, t, ldt, q, ldq, here, 1, nbnext, 
         work, info );
        here = here + 1;
      }
      else { 
        
        //              Recompute NBNEXT in case 2 by 2 split
        
        if( T(here,here + 1) == ZERO ) 
          nbnext = 1;
        if( nbnext == 2 ) { 
          
          //                 2 by 2 Block did not split
          
          dlaexc( wantq, n, t, ldt, q, ldq, here, 1, nbnext, 
           work, info );
          if( info != 0 ) { 
            ilst = here;
            return;
          }
          here = here + 2;
        }
        else { 
          
          //                 2 by 2 Block did split
          
          dlaexc( wantq, n, t, ldt, q, ldq, here, 1, 1, 
           work, info );
          dlaexc( wantq, n, t, ldt, q, ldq, here + 1, 1, 
           1, work, info );
          here = here + 2;
        }
      }
    }
    if( here < ilst ) 
      goto L_10;
    
  }
  else { 
    
    here = ifst;
L_20:
    ;
    
    //        Swap block with next one above
    
    if( nbf == 1 || nbf == 2 ) { 
      
      //           Current block either 1 by 1 or 2 by 2
      
      nbnext = 1;
      if( here >= 3 ) { 
        if( T(here - 3,here - 2) != ZERO ) 
          nbnext = 2;
      }
      dlaexc( wantq, n, t, ldt, q, ldq, here - nbnext, nbnext, 
       nbf, work, info );
      if( info != 0 ) { 
        ilst = here;
        return;
      }
      here = here - nbnext;
      
      //           Test if 2 by 2 block breaks into two 1 by 1 blocks
      
      if( nbf == 2 ) { 
        if( T(here - 1,here) == ZERO ) 
          nbf = 3;
      }
      
    }
    else { 
      
      //           Current block consists of two 1 by 1 blocks each of which
      //           must be swapped individually
      
      nbnext = 1;
      if( here >= 3 ) { 
        if( T(here - 3,here - 2) != ZERO ) 
          nbnext = 2;
      }
      dlaexc( wantq, n, t, ldt, q, ldq, here - nbnext, nbnext, 
       1, work, info );
      if( info != 0 ) { 
        ilst = here;
        return;
      }
      if( nbnext == 1 ) { 
        
        //              Swap two 1 by 1 blocks, no problems possible
        
        dlaexc( wantq, n, t, ldt, q, ldq, here, nbnext, 1, 
         work, info );
        here = here - 1;
      }
      else { 
        
        //              Recompute NBNEXT in case 2 by 2 split
        
        if( T(here - 2,here - 1) == ZERO ) 
          nbnext = 1;
        if( nbnext == 2 ) { 
          
          //                 2 by 2 Block did not split
          
          dlaexc( wantq, n, t, ldt, q, ldq, here - 1, 2, 
           1, work, info );
          if( info != 0 ) { 
            ilst = here;
            return;
          }
          here = here - 2;
        }
        else { 
          
          //                 2 by 2 Block did split
          
          dlaexc( wantq, n, t, ldt, q, ldq, here, 1, 1, 
           work, info );
          dlaexc( wantq, n, t, ldt, q, ldq, here - 1, 1, 
           1, work, info );
          here = here - 2;
        }
      }
    }
    if( here > ilst ) 
      goto L_20;
  }
  ilst = here;
  
  return;
  
  //     End of DTREXC
  
#undef  T
#undef  Q
} // end of function 


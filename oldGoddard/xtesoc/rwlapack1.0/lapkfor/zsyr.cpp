/*
 * C++ implementation of Lapack routine zsyr
 *
 * $Id: zsyr.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:50:50
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zsyr.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const DComplex ZERO = DComplex(0.0e0,0.0e0);
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zsyr(const char &uplo, const long &n, const DComplex &alpha, DComplex x[], 
 const long &incx, DComplex *a, const long &lda)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  long _do0, _do1, _do2, _do3, _do4, _do5, _do6, _do7, i, i_, 
   info, ix, j, j_, jx, kx;
  DComplex temp;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZSYR   performs the symmetric rank 1 operation
  
  //     A := alpha*x*( x' ) + A,
  
  //  where alpha is a DComplex scalar, x is an n element vector and A is an
  //  n by n symmetric matrix.
  
  //  Arguments
  //  ==========
  
  //  UPLO   - CHARACTER*1
  //           On entry, UPLO specifies whether the upper or lower
  //           triangular part of the array A is to be referenced as
  //           follows:
  
  //              UPLO = 'U' or 'u'   Only the upper triangular part of A
  //                                  is to be referenced.
  
  //              UPLO = 'L' or 'l'   Only the lower triangular part of A
  //                                  is to be referenced.
  
  //           Unchanged on exit.
  
  //  N      - INTEGER
  //           On entry, N specifies the order of the matrix A.
  //           N must be at least zero.
  //           Unchanged on exit.
  
  //  ALPHA  - COMPLEX*16
  //           On entry, ALPHA specifies the scalar alpha.
  //           Unchanged on exit.
  
  //  X      - COMPLEX*16 array, dimension at least
  //           ( 1 + ( N - 1 )*abs( INCX ) ).
  //           Before entry, the incremented array X must contain the N-
  //           element vector x.
  //           Unchanged on exit.
  
  //  INCX   - INTEGER
  //           On entry, INCX specifies the increment for the elements of
  //           X. INCX must not be zero.
  //           Unchanged on exit.
  
  //  A      - COMPLEX*16 array, dimension( LDA, N )
  //           Before entry with  UPLO = 'U' or 'u', the leading n by n
  //           upper triangular part of the array A must contain the upper
  //           triangular part of the symmetric matrix and the strictly
  //           lower triangular part of A is not referenced. On exit, the
  //           upper triangular part of the array A is overwritten by the
  //           upper triangular part of the updated matrix.
  //           Before entry with UPLO = 'L' or 'l', the leading n by n
  //           lower triangular part of the array A must contain the lower
  //           triangular part of the symmetric matrix and the strictly
  //           upper triangular part of A is not referenced. On exit, the
  //           lower triangular part of the array A is overwritten by the
  //           lower triangular part of the updated matrix.
  
  //  LDA    - INTEGER
  //           On entry, LDA specifies the first dimension of A as declared
  //           in the calling (sub) program. LDA must be at least
  //           max( 1, N ).
  //           Unchanged on exit.
  
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  if( !lsame( uplo, 'U' ) && !lsame( uplo, 'L' ) ) { 
    info = 1;
  }
  else if( n < 0 ) { 
    info = 2;
  }
  else if( incx == 0 ) { 
    info = 5;
  }
  else if( lda < max( 1, n ) ) { 
    info = 7;
  }
  if( info != 0 ) { 
    xerbla( "ZSYR  ", info );
    return;
  }
  
  //     Quick return if possible.
  
  if( (n == 0) || (ctocf(alpha) == ctocf(ZERO)) ) 
    return;
  
  //     Set the start point in X if the increment is not unity.
  
  if( incx <= 0 ) { 
    kx = 1 - (n - 1)*incx;
  }
  else if( incx != 1 ) { 
    kx = 1;
  }
  
  //     Start the operations. In this version the elements of A are
  //     accessed sequentially with one pass through the triangular part
  //     of A.
  
  if( lsame( uplo, 'U' ) ) { 
    
    //        Form  A  when A is stored in upper triangle.
    
    if( incx == 1 ) { 
      for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
        if( ctocf(x[j_]) != ctocf(ZERO) ) { 
          temp = alpha*x[j_];
          for( i = 1, i_ = i - 1, _do1 = j; i <= _do1; i++, i_++ ) { 
            A(j_,i_) = A(j_,i_) + x[i_]*temp;
          }
        }
      }
    }
    else { 
      jx = kx;
      for( j = 1, j_ = j - 1, _do2 = n; j <= _do2; j++, j_++ ) { 
        if( ctocf(x[jx - 1]) != ctocf(ZERO) ) { 
          temp = alpha*x[jx - 1];
          ix = kx;
          for( i = 1, i_ = i - 1, _do3 = j; i <= _do3; i++, i_++ ) { 
            A(j_,i_) = A(j_,i_) + x[ix - 1]*temp;
            ix = ix + incx;
          }
        }
        jx = jx + incx;
      }
    }
  }
  else { 
    
    //        Form  A  when A is stored in lower triangle.
    
    if( incx == 1 ) { 
      for( j = 1, j_ = j - 1, _do4 = n; j <= _do4; j++, j_++ ) { 
        if( ctocf(x[j_]) != ctocf(ZERO) ) { 
          temp = alpha*x[j_];
          for( i = j, i_ = i - 1, _do5 = n; i <= _do5; i++, i_++ ) { 
            A(j_,i_) = A(j_,i_) + x[i_]*temp;
          }
        }
      }
    }
    else { 
      jx = kx;
      for( j = 1, j_ = j - 1, _do6 = n; j <= _do6; j++, j_++ ) { 
        if( ctocf(x[jx - 1]) != ctocf(ZERO) ) { 
          temp = alpha*x[jx - 1];
          ix = jx;
          for( i = j, i_ = i - 1, _do7 = n; i <= _do7; i++, i_++ ) { 
            A(j_,i_) = A(j_,i_) + x[ix - 1]*temp;
            ix = ix + incx;
          }
        }
        jx = jx + incx;
      }
    }
  }
  
  return;
  
  //     End of ZSYR
  
#undef  A
} // end of function 


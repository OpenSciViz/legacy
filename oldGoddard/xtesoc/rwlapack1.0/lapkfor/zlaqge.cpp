/*
 * C++ implementation of Lapack routine zlaqge
 *
 * $Id: zlaqge.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:48:51
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zlaqge.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ONE = 1.0e0;
const double THRESH = 0.1e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zlaqge(const long &m, const long &n, DComplex *a, const long &lda, 
 double r[], double c[], const double &rowcnd, const double &colcnd, const double &amax, 
 char &equed)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  long _do0, _do1, _do2, _do3, _do4, _do5, i, i_, j, j_;
  double cj, large, small;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZLAQGE equilibrates a general M by N matrix A using the row and
  //  scaling factors in the vectors R and C.
  
  //  Arguments
  //  =========
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix A.  M >= 0.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix A.  N >= 0.
  
  //  A       (input/output) COMPLEX*16 array, dimension (LDA,N)
  //          On entry, the M by N matrix A.
  //          On exit, the equilibrated matrix.  See EQUED for the form of
  //          the equilibrated matrix.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(M,1).
  
  //  R       (input) DOUBLE PRECISION array, dimension (M)
  //          The row scale factors for A.
  
  //  C       (input) DOUBLE PRECISION array, dimension (N)
  //          The column scale factors for A.
  
  //  ROWCND  (input) DOUBLE PRECISION
  //          Ratio of the smallest R(i) to the largest R(i).
  
  //  COLCND  (input) DOUBLE PRECISION
  //          Ratio of the smallest C(i) to the largest C(i).
  
  //  AMAX    (input) DOUBLE PRECISION
  //          Absolute value of largest matrix entry.
  
  //  EQUED   (output) CHARACTER*1
  //          Specifies the form of equilibration that was done.
  //          = 'N':  No equilibration
  //          = 'R':  Row equilibration, i.e., A has been premultiplied by
  //                  diag(R).
  //          = 'C':  Column equilibration, i.e., A has been postmultiplied
  //                  by diag(C).
  //          = 'B':  Both row and column equilibration, i.e., A has been
  //                  replaced by diag(R) * A * diag(C).
  
  //  Internal Parameters
  //  ===================
  
  //  THRESH is a threshold value used to decide if row or column scaling
  //  should be done based on the ratio of the row or column scaling
  //  factors.  If ROWCND < THRESH, row scaling is done, and if
  //  COLCND < THRESH, column scaling is done.
  
  //  LARGE and SMALL are threshold values used to decide if row scaling
  //  should be done based on the absolute size of the largest matrix
  //  element.  If AMAX > LARGE or AMAX < SMALL, row scaling is done.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Quick return if possible
  
  if( m <= 0 || n <= 0 ) { 
    equed = 'N';
    return;
  }
  
  //     Initialize LARGE and SMALL.
  
  small = dlamch( 'S'/*Safe minimum*/ )/dlamch( 'P'/*Precision*/
    );
  large = ONE/small;
  
  if( (rowcnd >= THRESH && amax >= small) && amax <= large ) { 
    
    //        No row scaling
    
    if( colcnd >= THRESH ) { 
      
      //           No column scaling
      
      equed = 'N';
    }
    else { 
      
      //           Column scaling
      
      for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
        cj = c[j_];
        for( i = 1, i_ = i - 1, _do1 = m; i <= _do1; i++, i_++ ) { 
          A(j_,i_) = cj*A(j_,i_);
        }
      }
      equed = 'C';
    }
  }
  else if( colcnd >= THRESH ) { 
    
    //        Row scaling, no column scaling
    
    for( j = 1, j_ = j - 1, _do2 = n; j <= _do2; j++, j_++ ) { 
      for( i = 1, i_ = i - 1, _do3 = m; i <= _do3; i++, i_++ ) { 
        A(j_,i_) = r[i_]*A(j_,i_);
      }
    }
    equed = 'R';
  }
  else { 
    
    //        Row and column scaling
    
    for( j = 1, j_ = j - 1, _do4 = n; j <= _do4; j++, j_++ ) { 
      cj = c[j_];
      for( i = 1, i_ = i - 1, _do5 = m; i <= _do5; i++, i_++ ) { 
        A(j_,i_) = cj*r[i_]*A(j_,i_);
      }
    }
    equed = 'B';
  }
  
  return;
  
  //     End of ZLAQGE
  
#undef  A
} // end of function 


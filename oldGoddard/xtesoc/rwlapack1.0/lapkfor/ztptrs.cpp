/*
 * C++ implementation of Lapack routine ztptrs
 *
 * $Id: ztptrs.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:51:13
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: ztptrs.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const DComplex ZERO = DComplex(0.0e0);
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ ztptrs(const char &uplo, const char &trans, const char &diag, const long &n, 
 const long &nrhs, DComplex ap[], DComplex *b, const long &ldb, long &info)
{
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
  int nounit, upper;
  long _do0, _do1, _do2, info_, j, j_, jc;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZTPTRS solves a triangular system of the form
  
  //     A * x = b,  A**T * x = b,  or  A**H * x = b,
  
  //  where A is a triangular matrix of order N stored in packed format,
  //  A**T is the transpose of A, A**H is the conjugate transpose of A,
  //  and b is an N by NRHS matrix.  A check is made to verify that A is
  //  nonsingular.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the matrix A is upper or lower triangular.
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  TRANS   (input) CHARACTER*1
  //          Specifies the operation applied to A.
  //          = 'N':  Solve  A * x = b     (No transpose)
  //          = 'T':  Solve  A**T * x = b  (Transpose)
  //          = 'C':  Solve  A**H * x = b  (Conjugate transpose)
  
  //  DIAG    (input) CHARACTER*1
  //          Specifies whether or not the matrix A is unit triangular.
  //          = 'N':  Non-unit triangular
  //          = 'U':  Unit triangular
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right hand sides, i.e., the number of columns
  //          of the matrix B.  NRHS >= 0.
  
  //  AP      (input) COMPLEX*16 array, dimension (N*(N+1)/2)
  //          The upper or lower triangular matrix A, packed columnwise in
  //          a linear array.  The j-th column of A is stored in the array
  //          AP as follows:
  //          if UPLO = 'U', AP((j-1)*j/2 + i) = A(i,j) for 1<=i<=j;
  //          if UPLO = 'L',
  //             AP((j-1)*(n-j) + j*(j+1)/2 + i-j) = A(i,j) for j<=i<=n.
  
  //  B       (input/output) COMPLEX*16 array, dimension (LDB,NRHS)
  //          On entry, the right hand side vectors b for the system of
  //          linear equations.
  //          On exit, if INFO = 0, the solution vectors x.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: if INFO = k, the k-th diagonal element of A is zero,
  //               indicating that the matrix is singular and the solutions
  //               x have not been computed.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  nounit = lsame( diag, 'N' );
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( (!lsame( trans, 'N' ) && !lsame( trans, 'T' )) && !lsame( trans, 
   'C' ) ) { 
    info = -2;
  }
  else if( !nounit && !lsame( diag, 'U' ) ) { 
    info = -3;
  }
  else if( n < 0 ) { 
    info = -4;
  }
  else if( nrhs < 0 ) { 
    info = -5;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -8;
  }
  if( info != 0 ) { 
    xerbla( "ZTPTRS", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  //     Check for singularity.
  
  if( nounit ) { 
    if( upper ) { 
      jc = 1;
      for( info = 1, info_ = info - 1, _do0 = n; info <= _do0; info++, info_++ ) { 
        if( ctocf(ap[jc + info_ - 1]) == ctocf(ZERO) ) 
          return;
        jc = jc + info;
      }
    }
    else { 
      jc = 1;
      for( info = 1, info_ = info - 1, _do1 = n; info <= _do1; info++, info_++ ) { 
        if( ctocf(ap[jc - 1]) == ctocf(ZERO) ) 
          return;
        jc = jc + n - info + 1;
      }
    }
  }
  info = 0;
  
  //     Solve  A * x = b,  A**T * x = b,  or  A**H * x = b.
  
  for( j = 1, j_ = j - 1, _do2 = nrhs; j <= _do2; j++, j_++ ) { 
    ztpsv( uplo, trans, diag, n, ap, &B(j_,0), 1 );
  }
  
  return;
  
  //     End of ZTPTRS
  
#undef  B
} // end of function 


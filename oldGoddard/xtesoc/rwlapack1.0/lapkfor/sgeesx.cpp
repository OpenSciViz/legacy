/*
 * C++ implementation of Lapack routine sgeesx
 *
 * $Id: sgeesx.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:58:25
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: sgeesx.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ZERO = 0.0e0;
const float ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ sgeesx(const char &jobvs, const char &sort, int (*select)(float[],float[]), 
 const char &sense, const long &n, float *a, const long &lda, const long &sdim, 
 float wr[], float wi[], float *vs, const long &ldvs, const float &rconde, 
 const float &rcondv, float work[], const long &lwork, long iwork[], const long &liwork, 
 int bwork[], long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define VS(I_,J_) (*(vs+(I_)*(ldvs)+(J_)))
  int cursl, lastsl, lst2sl, scalea, wantsb, wantse, wantsn, 
   wantst, wantsv, wantvs;
  long hswork, i, i1, i2, ibal, icond, ierr, ieval, ihi, ilo, 
   inxt, ip, itau, iwrk, k, maxb, maxwrk, minwrk;
  float anrm, bignum, cscale, dum[1], eps, smlnum;

  
  //  -- LAPACK driver routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  //     .. Function Arguments ..

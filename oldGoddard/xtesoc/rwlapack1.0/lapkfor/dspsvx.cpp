/*
 * C++ implementation of lapack routine dspsvx
 *
 * $Id: dspsvx.cpp,v 1.6 1993/04/06 20:42:21 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:38:06
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dspsvx.cpp,v $
 * Revision 1.6  1993/04/06  20:42:21  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.5  1993/03/19  18:41:23  alv
 * now passes chars explicitly, rather than indirection of a string, to shut up SUN warnings
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:17:10  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:08:58  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dspsvx(const char &fact, const char &uplo, long &n, long &nrhs, 
 double ap[], double afp[], long ipiv[], double *b, long &ldb, 
 double *x, long &ldx, double &rcond, double ferr[], double berr[], 
 double work[], long iwork[], long &info)
{
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
#define X(I_,J_)  (*(x+(I_)*(ldx)+(J_)))
  int nofact;
  double anorm;

  
  //  -- LAPACK driver routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DSPSVX uses the diagonal pivoting factorization A = U*D*U' or
  //  A = L*D*L' to compute the solution to a real system of linear
  //  equations
  //     A * X = B,
  //  where A is an N by N symmetric matrix stored in packed format and X
  //  and B are N by NRHS matrices.
  
  //  Error bounds on the solution and a condition estimate are also
  //  provided.
  
  //  Description
  //  ===========
  
  //  The following steps are performed by this subroutine:
  
  //  1. If FACT = 'N', the diagonal pivoting method is used to factor A as
  //        A = U * D * U',  if UPLO = 'U', or
  //        A = L * D * L',  if UPLO = 'L',
  //     where U (or L) is a product of permutation and unit upper (lower)
  //     triangular matrices, D is symmetric and block diagonal with 1-by-1
  //     and 2-by-2 diagonal blocks, and ' indicates transpose.
  
  //  2. The factored form of A is used to estimate the condition number
  //     of the matrix A.  If the reciprocal of the condition number is
  //     less than machine precision, steps 3 and 4 are skipped.
  
  //  3. The system of equations A*X = B is solved for X using the
  //     factored form of A.
  
  //  4. Iterative refinement is applied to improve the computed solution
  //     vectors and calculate error bounds and backward error estimates
  //     for them.
  
  //  Arguments
  //  =========
  
  //  FACT    (input) CHARACTER*1
  //          Specifies whether or not the factored form of A has been
  //          supplied on entry.
  //          = 'F':  On entry, AFP and IPIV contain the factored form of
  //                  A.  AP, AFP and IPIV will not be modified.
  //          = 'N':  The matrix A will be copied to AFP and factored.
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangular part of the
  //          symmetric matrix A is stored:
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The number of linear equations, i.e., the order of the
  //          matrix A.  N >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right-hand sides, i.e., the number of columns
  //          of the matrices B and X.  NRHS >= 0.
  
  //  AP      (input) DOUBLE PRECISION array, dimension (N*(N+1)/2)
  //          The upper or lower triangle of the symmetric matrix A, packed
  //          columnwise in a linear array.  The j-th column of A is stored
  //          in the array AP as follows:
  //          if UPLO = 'U', AP(i + (j-1)*j/2) = A(i,j) for 1<=i<=j;
  //          if UPLO = 'L', AP(i + (j-1)*(2n-j)/2) = A(i,j) for j<=i<=n.
  //          See below for further details.
  
  //  AFP     (input or output) DOUBLE PRECISION array, dimension
  //                            (N*(N+1)/2)
  //          If FACT = 'F', then AFP is an input argument and on entry
  //          contains the block diagonal matrix D and the multipliers used
  //          to obtain the factor U or L from the factorization A = U*D*U'
  //          or A = L*D*L' as computed by DSPTRF, stored as a packed
  //          triangular matrix in the same storage format as A.
  
  //          If FACT = 'N', then AFP is an output argument and on exit
  //          contains the block diagonal matrix D and the multipliers used
  //          to obtain the factor U or L from the factorization A = U*D*U'
  //          or A = L*D*L' as computed by DSPTRF, stored as a packed
  //          triangular matrix in the same storage format as A.
  
  //  IPIV    (input or output) INTEGER array, dimension (N)
  //          If FACT = 'F', then IPIV is an input argument and on entry
  //          contains details of the interchanges and the block structure
  //          of D, as determined by DSPTRF.
  //          If IPIV(k) > 0, then rows and columns k and IPIV(k) were
  //          interchanged and D(k,k) is a 1-by-1 diagonal block.
  //          If UPLO = 'U' and IPIV(k) = IPIV(k-1) < 0, then rows and
  //          columns k-1 and -IPIV(k) were interchanged and D(k-1:k,k-1:k)
  //          is a 2-by-2 diagonal block.  If UPLO = 'L' and IPIV(k) =
  //          IPIV(k+1) < 0, then rows and columns k+1 and -IPIV(k) were
  //          interchanged and D(k:k+1,k:k+1) is a 2-by-2 diagonal block.
  
  //          If FACT = 'N', then IPIV is an output argument and on exit
  //          contains details of the interchanges and the block structure
  //          of D, as determined by DSPTRF.
  
  //  B       (input) DOUBLE PRECISION array, dimension (LDB,NRHS)
  //          The n-by-nrhs right-hand side matrix B.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  X       (output) DOUBLE PRECISION array, dimension (LDX,NRHS)
  //          If INFO = 0, the n-by-nrhs solution matrix X.
  
  //  LDX     (input) INTEGER
  //          The leading dimension of the array X.  LDX >= max(1,N).
  
  //  RCOND   (output) DOUBLE PRECISION
  //          The estimate of the reciprocal condition number of the matrix
  //          A.  If RCOND is less than the machine precision (in
  //          particular, if RCOND = 0), the matrix is singular to working
  //          precision.  This condition is indicated by a return code of
  //          INFO > 0, and the solution and error bounds are not computed.
  
  //  FERR    (output) DOUBLE PRECISION array, dimension (NRHS)
  //          The estimated forward error bounds for each solution vector
  //          X(j) (the j-th column of the solution matrix X).
  //          If XTRUE is the true solution, FERR(j) bounds the magnitude
  //          of the largest entry in (X(j) - XTRUE) divided by the
  //          magnitude of the largest entry in X(j).  The quality of the
  //          error bound depends on the quality of the estimate of
  //          norm(inv(A)) computed in the code; if the estimate of
  //          norm(inv(A)) is accurate, the error bound is guaranteed.
  
  //  BERR    (output) DOUBLE PRECISION array, dimension (NRHS)
  //          The componentwise relative backward error of each solution
  //          vector X(j) (i.e., the smallest relative change in any
  //          entry of A or B that makes X(j) an exact solution).
  
  //  WORK    (workspace) DOUBLE PRECISION array, dimension (3*N)
  
  //  IWORK   (workspace) INTEGER array, dimension (N)
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0 and <= N: if INFO = k, D(k,k) is exactly zero.  The
  //               factorization has been completed, but the block diagonal
  //               matrix D is exactly singular, so the solution and error
  //               bounds could not be computed.
  //          = N+1: the block diagonal matrix D is nonsingular, but RCOND
  //               is less than machine precision.  The factorization has
  //               been completed, but the matrix is singular to working
  //               precision, so the solution and error bounds have not
  //               been computed.
  
  //  Further Details
  //  ===============
  
  //  The packed storage scheme is illustrated by the following example
  //  when N = 4, UPLO = 'U':
  
  //  Two-dimensional storage of the symmetric matrix A:
  
  //     a11 a12 a13 a14
  //         a22 a23 a24
  //             a33 a34     (aij = aji)
  //                 a44
  
  //  Packed storage of the upper triangle of A:
  
  //  AP = [ a11, a12, a22, a13, a23, a33, a14, a24, a34, a44 ]
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  nofact = lsame( fact, 'N' );
  if( !nofact && !lsame( fact, 'F' ) ) { 
    info = -1;
  }
  else if( !lsame( uplo, 'U' ) && !lsame( uplo, 'L' ) ) { 
    info = -2;
  }
  else if( n < 0 ) { 
    info = -3;
  }
  else if( nrhs < 0 ) { 
    info = -4;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -9;
  }
  else if( ldx < max( 1, n ) ) { 
    info = -11;
  }
  if( info != 0 ) { 
    xerbla( "DSPSVX", -info );
    return;
  }
  
  if( nofact ) { 
    
    //        Compute the factorization A = U*D*U' or A = L*D*L'.
    
    dcopy( n*(n + 1)/2, ap, 1, afp, 1 );
    dsptrf( uplo, n, afp, ipiv, info );
    
    //        Return if INFO is non-zero.
    
    if( info != 0 ) { 
      if( info > 0 ) 
        rcond = ZERO;
      return;
    }
  }
  
  //     Compute the norm of the matrix A.
  
  anorm = dlansp( 'I', uplo, n, ap, work );
  
  //     Compute the reciprocal of the condition number of A.
  
  dspcon( uplo, n, afp, ipiv, anorm, rcond, work, iwork, info );
  
  //     Return if the matrix is singular to working precision.
  
  if( rcond < dlamch( 'E'/* Epsilon */ ) ) { 
    info = n + 1;
    return;
  }
  
  //     Compute the solution vectors X.
  
  dlacpy( 'F'/* Full */, n, nrhs, b, ldb, x, ldx );
  dsptrs( uplo, n, nrhs, afp, ipiv, x, ldx, info );
  
  //     Use iterative refinement to improve the computed solutions and
  //     compute error bounds and backward error estimates for them.
  
  dsprfs( uplo, n, nrhs, ap, afp, ipiv, b, ldb, x, ldx, ferr, berr, 
   work, iwork, info );
  
  return;
  
  //     End of DSPSVX
  
#undef  X
#undef  B
} // end of function 


/*
 * C++ implementation of Lapack routine sgtsvx
 *
 * $Id: sgtsvx.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:59:14
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: sgtsvx.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ sgtsvx(const char &fact, const char &trans, const long &n, const long &nrhs, 
 float dl[], float d[], float du[], float dlf[], float df[], float duf[], 
 float du2[], long ipiv[], float *b, const long &ldb, float *x, 
 const long &ldx, const float &rcond, float ferr[], float berr[], float work[], 
 long iwork[], long &info)
{
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
#define X(I_,J_)  (*(x+(I_)*(ldx)+(J_)))
  int nofact, notran;
  char norm;
  float anorm;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SGTSVX uses the LU factorization to compute the solution to a real
  //  system of linear equations
  //     A * X = B,
  //  where A is a tridiagonal matrix of order N and X and B are N by NRHS
  //  matrices.
  
  //  Error bounds on the solution and a condition estimate are also
  //  provided.
  
  //  Description
  //  ===========
  
  //  The following steps are performed by this subroutine:
  
  //  1. If FACT = 'N', the LU decomposition is used to factor the matrix A
  //     as A = L * U, where L is a product of permutation and unit lower
  //     bidiagonal matrices and U is upper triangular with nonzeros in
  //     only the main diagonal and first two superdiagonals.
  
  //  2. The factored form of A is used to estimate the condition number
  //     of the matrix A.  If the reciprocal of the condition number is
  //     less than machine precision, steps 3 and 4 are skipped.
  
  //  3. The system of equations A*X = B is solved for X using the
  //     factored form of A.
  
  //  4. Iterative refinement is applied to improve the computed solution
  //     vectors and calculate error bounds and backward error estimates
  //     for them.
  
  //  Arguments
  //  =========
  
  //  FACT    (input) CHARACTER*1
  //          Specifies whether or not the factored form of A has been
  //          supplied on entry.
  //          = 'F':  DLF, DF, DUF, DU2, and IPIV contain the factored
  //                  form of A; DL, D, DU, DLF, DF, DUF, DU2 and IPIV
  //                  will not be modified.
  //          = 'N':  The matrix will be copied to DLF, DF, and DUF
  //                  and factored.
  
  //  TRANS   (input) CHARACTER*1
  //          Specifies the form of the system of equations.
  //          = 'N':  A * X = B     (No transpose)
  //          = 'T':  A**T * X = B  (Transpose)
  //          = 'C':  A**H * X = B  (Conjugate transpose = Transpose)
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right hand sides, i.e., the number of columns
  //          of the matrix B.  NRHS >= 0.
  
  //  DL      (input) REAL array, dimension (N-1)
  //          The (n-1) subdiagonal elements of A.
  
  //  D       (input) REAL array, dimension (N)
  //          The n diagonal elements of A.
  
  //  DU      (input) REAL array, dimension (N-1)
  //          The (n-1) super-diagonal elements of A.
  
  //  DLF     (input or output) REAL array, dimension (N-1)
  //          If FACT = 'F', then DLF is an input argument and on entry
  //          contains the (n-1) multipliers that define the matrix L from
  //          the LU factorization of A as computed by SGTTRF.
  
  //          If FACT = 'N', then DLF is an output argument and on exit
  //          contains the (n-1) multipliers that define the matrix L from
  //          the LU factorization of A.
  
  //  DF      (input or output) REAL array, dimension (N)
  //          If FACT = 'F', then DF is an input argument and on entry
  //          contains the n diagonal elements of the upper triangular
  //          matrix U from the LU factorization of A.
  
  //          If FACT = 'N', then DF is an output argument and on exit
  //          contains the n diagonal elements of the upper triangular
  //          matrix U from the LU factorization of A.
  
  //  DUF     (input or output) REAL array, dimension (N-1)
  //          If FACT = 'F', then DUF is an input argument and on entry
  //          contains the (n-1) elements of the first super-diagonal of U.
  
  //          If FACT = 'N', then DUF is an output argument and on exit
  //          contains the (n-1) elements of the first super-diagonal of U.
  
  //  DU2     (input or output) REAL array, dimension (N-2)
  //          If FACT = 'F', then DU2 is an input argument and on entry
  //          contains the (n-2) elements of the second super-diagonal of
  //          U.
  
  //          If FACT = 'N', then DU2 is an output argument and on exit
  //          contains the (n-2) elements of the second super-diagonal of
  //          U.
  
  //  IPIV    (input) INTEGER array, dimension (N)
  //          If FACT = 'F', then IPIV is an input argument and on entry
  //          contains the pivot indices from the LU factorization of A as
  //          computed by SGTTRF.
  
  //          If FACT = 'N', then IPIV is an output argument and on exit
  //          contains the pivot indices from the LU factorization of A;
  //          row i of the matrix was interchanged with row IPIV(i).
  //          IPIV(i) will always be either i or i+1; IPIV(i) = i indicates
  //          a row interchange was not required.
  
  //  B       (input) REAL array, dimension (LDB,NRHS)
  //          The n-by-nrhs right-hand side matrix B.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  X       (output) REAL array, dimension (LDX,NRHS)
  //          If INFO = 0, the n-by-nrhs solution matrix X.
  
  //  LDX     (input) INTEGER
  //          The leading dimension of the array X.  LDX >= max(1,N).
  
  //  RCOND   (output) REAL
  //          The estimate of the reciprocal condition number of the matrix
  //          A.  If RCOND is less than the machine precision (in
  //          particular, if RCOND = 0), the matrix is singular to working
  //          precision.  This condition is indicated by a return code of
  //          INFO > 0, and the solution and error bounds are not computed.
  
  //  FERR    (output) REAL array, dimension (NRHS)
  //          The estimated forward error bound for each solution vector
  //          X(j) (the j-th column of the solution matrix X).
  //          If XTRUE is the true solution, FERR(j) bounds the magnitude
  //          of the largest entry in (X(j) - XTRUE) divided by the
  //          magnitude of the largest entry in X(j).  The quality of the
  //          error bound depends on the quality of the estimate of
  //          norm(inv(A)) computed in the code; if the estimate of
  //          norm(inv(A)) is accurate, the error bound is guaranteed.
  
  //  BERR    (output) REAL array, dimension (NRHS)
  //          The componentwise relative backward error of each solution
  //          vector X(j) (i.e., the smallest relative change in any
  //          entry of A or B that makes X(j) an exact solution).
  
  //  WORK    (workspace) REAL array, dimension (3*N)
  
  //  IWORK   (workspace) INTEGER array, dimension (N)
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: if INFO = k <= N, U(k,k) is exactly zero, or if
  //               INFO = N+1, the factor U is nonsingular, but RCOND is
  //               less than machine precision.  The factorization has been
  //               completed, but the matrix A is singular to working
  //               precision, and the solution and error bounds have not
  //               been computed.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  info = 0;
  nofact = lsame( fact, 'N' );
  notran = lsame( trans, 'N' );
  if( !nofact && !lsame( fact, 'F' ) ) { 
    info = -1;
  }
  else if( (!notran && !lsame( trans, 'T' )) && !lsame( trans, 'C' )
    ) { 
    info = -2;
  }
  else if( n < 0 ) { 
    info = -3;
  }
  else if( nrhs < 0 ) { 
    info = -4;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -14;
  }
  else if( ldx < max( 1, n ) ) { 
    info = -16;
  }
  if( info != 0 ) { 
    xerbla( "SGTSVX", -info );
    return;
  }
  
  if( nofact ) { 
    
    //        Compute the LU factorization of A.
    
    scopy( n, d, 1, df, 1 );
    if( n > 1 ) { 
      scopy( n - 1, dl, 1, dlf, 1 );
      scopy( n - 1, du, 1, duf, 1 );
    }
    sgttrf( n, dlf, df, duf, du2, ipiv, info );
    
    //        Return if INFO is non-zero.
    
    if( info != 0 ) { 
      if( info > 0 ) 
        rcond = ZERO;
      return;
    }
  }
  
  //     Compute the norm of the matrix A.
  
  if( notran ) { 
    norm = '1';
  }
  else { 
    norm = 'I';
  }
  anorm = slangt( norm, n, dl, d, du );
  
  //     Compute the reciprocal of the condition number of A.
  
  sgtcon( norm, n, dlf, df, duf, du2, ipiv, anorm, rcond, work, 
   iwork, info );
  
  //     Return if the matrix is singular to working precision.
  
  if( rcond < slamch( 'E'/*Epsilon*/ ) ) { 
    info = n + 1;
    return;
  }
  
  //     Compute the solution vectors X.
  
  slacpy( 'F'/*Full*/, n, nrhs, b, ldb, x, ldx );
  sgttrs( trans, n, nrhs, dlf, df, duf, du2, ipiv, x, ldx, info );
  
  //     Use iterative refinement to improve the computed solutions and
  //     compute error bounds and backward error estimates for them.
  
  sgtrfs( trans, n, nrhs, dl, d, du, dlf, df, duf, du2, ipiv, b, 
   ldb, x, ldx, ferr, berr, work, iwork, info );
  
  return;
  
  //     End of SGTSVX
  
#undef  X
#undef  B
} // end of function 


/*
 * C++ implementation of Lapack routine zgtsvx
 *
 * $Id: zgtsvx.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:47:06
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zgtsvx.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zgtsvx(const char &fact, const char &trans, const long &n, const long &nrhs, 
 DComplex dl[], DComplex d[], DComplex du[], DComplex dlf[], DComplex df[], 
 DComplex duf[], DComplex du2[], long ipiv[], DComplex *b, const long &ldb, 
 DComplex *x, const long &ldx, double &rcond, double ferr[], double berr[], 
 DComplex work[], double rwork[], long &info)
{
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
#define X(I_,J_)  (*(x+(I_)*(ldx)+(J_)))
  int nofact, notran;
  char norm;
  double anorm;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZGTSVX uses the LU factorization to compute the solution to a DComplex
  //  system of linear equations
  //     A * X = B,
  //  where A is a tridiagonal matrix of order N and X and B are N by NRHS
  //  matrices.
  
  //  Error bounds on the solution and a condition estimate are also
  //  provided.
  
  //  Description
  //  ===========
  
  //  The following steps are performed by this subroutine:
  
  //  1. If FACT = 'N', the LU decomposition is used to factor the matrix A
  //     as A = L * U, where L is a product of permutation and unit lower
  //     bidiagonal matrices and U is upper triangular with nonzeros in
  //     only the main diagonal and first two superdiagonals.
  
  //  2. The factored form of A is used to estimate the condition number
  //     of the matrix A.  If the reciprocal of the condition number is
  //     less than machine precision, steps 3 and 4 are skipped.
  
  //  3. The system of equations A*X = B is solved for X using the
  //     factored form of A.
  
  //  4. Iterative refinement is applied to improve the computed solution
  //     vectors and calculate error bounds and backward error estimates
  //     for them.
  
  //  Arguments
  //  =========
  
  //  FACT    (input) CHARACTER*1
  //          Specifies whether or not the factored form of A has been
  //          supplied on entry.
  //          = 'F':  DLF, DF, DUF, DU2, and IPIV contain the factored form
  //                  of A; DL, D, DU, DLF, DF, DUF, DU2 and IPIV will not
  //                  be modified.
  //          = 'N':  The matrix will be copied to DLF, DF, and DUF
  //                  and factored.
  
  //  TRANS   (input) CHARACTER*1
  //          Specifies the form of the system of equations.
  //          = 'N':  A * X = B     (No transpose)
  //          = 'T':  A**T * X = B  (Transpose)
  //          = 'C':  A**H * X = B  (Conjugate transpose)
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right hand sides, i.e., the number of columns
  //          of the matrix B.  NRHS >= 0.
  
  //  DL      (input) COMPLEX*16 array, dimension (N-1)
  //          The (n-1) subdiagonal elements of A.
  
  //  D       (input) COMPLEX*16 array, dimension (N)
  //          The n diagonal elements of A.
  
  //  DU      (input) COMPLEX*16 array, dimension (N-1)
  //          The (n-1) superdiagonal elements of A.
  
  //  DLF     (input or output) COMPLEX*16 array, dimension (N-1)
  //          If FACT = 'F', then DLF is an input argument and on entry
  //          contains the (n-1) multipliers that define the matrix L from
  //          the LU factorization of A as computed by ZGTTRF.
  
  //          If FACT = 'N', then DLF is an output argument and on exit
  //          contains the (n-1) multipliers that define the matrix L from
  //          the LU factorization of A.
  
  //  DF      (input or output) COMPLEX*16 array, dimension (N)
  //          If FACT = 'F', then DF is an input argument and on entry
  //          contains the n diagonal elements of the upper triangular
  //          matrix U from the LU factorization of A.
  
  //          If FACT = 'N', then DF is an output argument and on exit
  //          contains the n diagonal elements of the upper triangular
  //          matrix U from the LU factorization of A.
  
  //  DUF     (input or output) COMPLEX*16 array, dimension (N-1)
  //          If FACT = 'F', then DUF is an input argument and on entry
  //          contains the (n-1) elements of the first super-diagonal of U.
  
  //          If FACT = 'N', then DUF is an output argument and on exit
  //          contains the (n-1) elements of the first super-diagonal of U.
  
  //  DU2     (input or output) COMPLEX*16 array, dimension (N-2)
  //          If FACT = 'F', then DU2 is an input argument and on entry
  //          contains the (n-2) elements of the second super-diagonal of
  //          U.
  
  //          If FACT = 'N', then DU2 is an output argument and on exit
  //          contains the (n-2) elements of the second super-diagonal of
  //          U.
  
  //  IPIV    (input) INTEGER array, dimension (N)
  //          If FACT = 'F', then IPIV is an input argument and on entry
  //          contains the pivot indices from the LU factorization of A as
  //          computed by ZGTTRF.
  
  //          If FACT = 'N', then IPIV is an output argument and on exit
  //          contains the pivot indices from the LU factorization of A;
  //          row i of the matrix was interchanged with row IPIV(i).
  //          IPIV(i) will always be either i or i+1; IPIV(i) = i indicates
  //          a row interchange was not required.
  
  //  B       (input) COMPLEX*16 array, dimension (LDB,NRHS)
  //          The n-by-nrhs right-hand side matrix B.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  X       (output) COMPLEX*16 array, dimension (LDX,NRHS)
  //          If INFO = 0, the n-by-nrhs solution matrix X.
  
  //  LDX     (input) INTEGER
  //          The leading dimension of the array X.  LDX >= max(1,N).
  
  //  RCOND   (output) DOUBLE PRECISION
  //          The estimate of the reciprocal condition number of the matrix
  //          A.  If RCOND is less than the machine precision (in
  //          particular, if RCOND = 0), the matrix is singular to working
  //          precision.  This condition is indicated by a return code of
  //          INFO > 0, and the solution and error bounds are not computed.
  
  //  FERR    (output) DOUBLE PRECISION array, dimension (NRHS)
  //          The estimated forward error bounds for each solution vector
  //          X(j) (the j-th column of the solution matrix X).
  //          If XTRUE is the true solution, FERR(j) bounds the magnitude
  //          of the largest entry in (X(j) - XTRUE) divided by the
  //          magnitude of the largest entry in X(j).  The quality of the
  //          error bound depends on the quality of the estimate of
  //          norm(inv(A)) computed in the code; if the estimate of
  //          norm(inv(A)) is accurate, the error bound is guaranteed.
  
  //  BERR    (output) DOUBLE PRECISION array, dimension (NRHS)
  //          The componentwise relative backward error of each solution
  //          vector X(j) (i.e., the smallest relative change in any entry
  //          of A or B that makes X(j) an exact solution).
  
  //  WORK    (workspace) COMPLEX*16 array, dimension (2*N)
  
  //  RWORK   (workspace) DOUBLE PRECISION array, dimension (N)
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: if INFO = k <= N, U(k,k) is exactly zero, or if
  //               INFO = N+1, the factor U is nonsingular, but RCOND is
  //               less than machine precision.  The factorization has been
  //               completed, but the matrix A is singular to working
  //               precision, and the solution and error bounds have not
  //               been computed.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  info = 0;
  nofact = lsame( fact, 'N' );
  notran = lsame( trans, 'N' );
  if( !nofact && !lsame( fact, 'F' ) ) { 
    info = -1;
  }
  else if( (!notran && !lsame( trans, 'T' )) && !lsame( trans, 'C' )
    ) { 
    info = -2;
  }
  else if( n < 0 ) { 
    info = -3;
  }
  else if( nrhs < 0 ) { 
    info = -4;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -14;
  }
  else if( ldx < max( 1, n ) ) { 
    info = -16;
  }
  if( info != 0 ) { 
    xerbla( "ZGTSVX", -info );
    return;
  }
  
  if( nofact ) { 
    
    //        Compute the LU factorization of A.
    
    zcopy( n, d, 1, df, 1 );
    if( n > 1 ) { 
      zcopy( n - 1, dl, 1, dlf, 1 );
      zcopy( n - 1, du, 1, duf, 1 );
    }
    zgttrf( n, dlf, df, duf, du2, ipiv, info );
    
    //        Return if INFO is non-zero.
    
    if( info != 0 ) { 
      if( info > 0 ) 
        rcond = ZERO;
      return;
    }
  }
  
  //     Compute the norm of the matrix A.
  
  if( notran ) { 
    norm = '1';
  }
  else { 
    norm = 'I';
  }
  anorm = zlangt( norm, n, dl, d, du );
  
  //     Compute the reciprocal of the condition number of A.
  
  zgtcon( norm, n, dlf, df, duf, du2, ipiv, anorm, rcond, work, 
   info );
  
  //     Return if the matrix is singular to working precision.
  
  if( rcond < dlamch( 'E'/*Epsilon*/ ) ) { 
    info = n + 1;
    return;
  }
  
  //     Compute the solution vectors X.
  
  zlacpy( 'F'/*Full*/, n, nrhs, b, ldb, x, ldx );
  zgttrs( trans, n, nrhs, dlf, df, duf, du2, ipiv, x, ldx, info );
  
  //     Use iterative refinement to improve the computed solutions and
  //     compute error bounds and backward error estimates for them.
  
  zgtrfs( trans, n, nrhs, dl, d, du, dlf, df, duf, du2, ipiv, b, 
   ldb, x, ldx, ferr, berr, work, rwork, info );
  
  return;
  
  //     End of ZGTSVX
  
#undef  X
#undef  B
} // end of function 


/*
 * C++ implementation of Lapack routine zgels
 *
 * $Id: zgels.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:46:25
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zgels.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
const DComplex CZERO = DComplex(0.0e0);
const DComplex CONE = DComplex(1.0e0);
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zgels(const char &trans, const long &m, const long &n, const long &nrhs, 
 DComplex *a, const long &lda, DComplex *b, const long &ldb, DComplex work[], 
 const long &lwork, long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
  int tpsd;
  long _do0, _do1, _do2, _do3, brow, i, i_, iascl, ibscl, j, 
   j_, mn, nb, scllen, wsize;
  double anrm, bignum, bnrm, rwork[1], smlnum;

  
  //  -- LAPACK driver routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZGELS solves square or over- and underdetermined linear systems
  //  involving the m-by-n matrix A and the right-hand side B
  //  using orthogonal reductions.  It is assumed that A has full rank.
  
  //  Cases:
  
  //  1) m >= n, TRANS = 'N':
  //     Solve the least-squares problem min|| A*X - B ||.
  //     Computing  A = [ Q1, Q2 ] [ R ] n
  //                      n  m-n   [ 0 ] m-n
  //     the least-squares solution is X = inv(R) * Q1' * B.
  //     A is overwritten by its QR factorization
  //     and B is overwritten by [ X  ]
  //                             [ RS ]
  //     where RS = Q2'*B. The residual B - A*X is Q2*RS.
  
  //  2) m >= n, TRANS = 'C':
  //     Solve the underdetermined system A' * X = B.
  //     Computing A = [ Q1, Q2 ] [ R ] n
  //                     n   m-n  [ 0 ] m-n
  //     the minimum-norm solution is X = Q1 * inv(R') * B.
  //     A is overwritten by its QR factorization, B is overwritten by X.
  
  //  3) m < n, TRANS = 'N':
  //     Solve the underdetermined system A * X = B.
  //     Computing A = [ L , 0] [ Q1 ] m
  //                     m  n-m [ Q2 ] n-m
  //     the minimum-norm solution is X = Q1' * inv(L) * B.
  //     A is overwritten by its LQ factorization, B is overwritten by X.
  
  //  4) m < n, TRANS = 'C':
  //     Solve the least-squares problem min|| A'*X - B ||.
  //     Computing A = [ L , 0] [ Q1 ] m
  //                     m  n-m [ Q2 ] n-m
  //     the least-squares solution is X = inv(L') * Q1 * B.
  //     A is overwritten by its LQ factorization
  //     and B is overwritten by [ X  ]
  //                             [ RS ]
  //     where RS = Q2*B. The residual B - A*X is Q2'*RS.
  
  //  Arguments
  //  =========
  
  //  TRANS   (input) CHARACTER
  //          If TRANS = 'N', we compute the minimum-norm or least-squares
  //          solution A * X = B.
  //          If TRANS = 'C', we compute the minimum-norm or least-squares
  //          solution A' * X = B.  (A' = conjugate transpose of A)
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix A.  M >= 0.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix A.  N >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right-hand sides. NRHS >=0.
  
  //  A       (input/output) COMPLEX*16 array, dimension (LDA,N)
  //          On entry, the M by N matrix A.
  //          On exit,
  //            if M >= N, A has been overwritten by its QR factorization.
  //            if M <  N, A has been overwritten by its LQ factorization.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,M).
  
  //  B       (input/output) COMPLEX*16 array, dimension (LDB,NRHS)
  //          on entry,
  //            if TRANS = 'N', the M by NRHS right-hand side.
  //            if TRANS = 'C', the N by NRHS right-hand side.
  //          on exit,
  //            if M >= N and TRANS = 'N', the first N rows of B
  //                      contain the least-squares solution, the
  //                      remaining M - N rows the residual.
  //            if M >= N and TRANS = 'C', B contains the M by NRHS
  //                      minimum-norm solution.
  //            if M <  N and TRANS = 'N', B contains the N by NRHS
  //                      minimum-norm solution.
  //            if M <  N and TRANS = 'C', the first M rows of B
  //                      contain the least-squares solution, the
  //                      remaining N - M rows the residual.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B. LDB >= MAX(1,M,N).
  
  //  WORK    (workspace) COMPLEX*16 array, dimension (LWORK)
  //          on output, work(1) contains the workspace length required
  //                     for optimum efficiency
  
  //  LWORK   (input) INTEGER
  //          The dimension of the array WORK.
  //          LWORK >= max(1,MN + MAX(M,N,NRHS)) where MN = min(M,N).
  //          The block algorithm will not be used unless
  //          LWORK >= max(1,MN + MAX(M,N,NRHS) * NB)
  //          where NB is the block size for this environment.
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Local Arrays ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input arguments.
  
  info = 0;
  mn = min( m, n );
  if( !(lsame( trans, 'N' ) || lsame( trans, 'C' )) ) { 
    info = -1;
  }
  else if( m < 0 ) { 
    info = -2;
  }
  else if( n < 0 ) { 
    info = -3;
  }
  else if( nrhs < 0 ) { 
    info = -4;
  }
  else if( lda < max( 1, m ) ) { 
    info = -6;
  }
  else if( ldb < vmax( 1, m, n, IEND ) ) { 
    info = -8;
  }
  else if( lwork < max( 1, mn + vmax( m, n, nrhs, IEND ) ) ) { 
    info = -10;
  }
  
  //     Figure out optimal block size
  
  if( info == 0 || info == -10 ) { 
    
    tpsd = TRUE;
    if( lsame( trans, 'N' ) ) 
      tpsd = FALSE;
    
    if( m >= n ) { 
      nb = ilaenv( 1, "ZGEQRF", " ", m, n, -1, -1 );
      if( tpsd ) { 
        nb = max( nb, ilaenv( 1, "ZUNMQR", "LN", m, nrhs, 
         n, -1 ) );
      }
      else { 
        nb = max( nb, ilaenv( 1, "ZUNMQR", "LC", m, nrhs, 
         n, -1 ) );
      }
    }
    else { 
      nb = ilaenv( 1, "ZGELQF", " ", m, n, -1, -1 );
      if( tpsd ) { 
        nb = max( nb, ilaenv( 1, "ZUNMLQ", "LC", n, nrhs, 
         m, -1 ) );
      }
      else { 
        nb = max( nb, ilaenv( 1, "ZUNMLQ", "LN", n, nrhs, 
         m, -1 ) );
      }
    }
    
    wsize = mn + vmax( m, n, nrhs, IEND )*nb;
    work[0] = DComplex((double)( wsize ));
    
  }
  
  if( info != 0 ) { 
    xerbla( "ZGELS ", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( vmin( m, n, nrhs, IEND ) == 0 ) { 
    zlaset( 'F'/*Full*/, max( m, n ), nrhs, CZERO, CZERO, b, 
     ldb );
    return;
  }
  
  //     Get machine parameters
  
  smlnum = dlamch( 'S' )/dlamch( 'P' );
  bignum = ONE/smlnum;
  dlabad( smlnum, bignum );
  
  //     Scale A, B if max entry outside range [SMLNUM,BIGNUM]
  
  anrm = zlange( 'M', m, n, a, lda, rwork );
  iascl = 0;
  if( anrm > ZERO && anrm < smlnum ) { 
    
    //        Scale matrix norm up to SMLNUM
    
    zlascl( 'G', 0, 0, anrm, smlnum, m, n, a, lda, info );
    iascl = 1;
  }
  else if( anrm > bignum ) { 
    
    //        Scale matrix norm down to BIGNUM
    
    zlascl( 'G', 0, 0, anrm, bignum, m, n, a, lda, info );
    iascl = 2;
  }
  else if( anrm == ZERO ) { 
    
    //        Matrix all zero. Return zero solution.
    
    zlaset( 'F', max( m, n ), nrhs, CZERO, CZERO, b, ldb );
    goto L_50;
  }
  
  brow = m;
  if( tpsd ) 
    brow = n;
  bnrm = zlange( 'M', brow, nrhs, b, ldb, rwork );
  ibscl = 0;
  if( bnrm > ZERO && bnrm < smlnum ) { 
    
    //        Scale matrix norm up to SMLNUM
    
    zlascl( 'G', 0, 0, bnrm, smlnum, brow, nrhs, b, ldb, info );
    ibscl = 1;
  }
  else if( bnrm > bignum ) { 
    
    //        Scale matrix norm down to BIGNUM
    
    zlascl( 'G', 0, 0, bnrm, bignum, brow, nrhs, b, ldb, info );
    ibscl = 2;
  }
  
  if( m >= n ) { 
    
    //        compute QR factorization of A
    
    zgeqrf( m, n, a, lda, &work[0], &work[mn], lwork - mn, info );
    
    //        workspace at least N, optimally N*NB
    
    if( !tpsd ) { 
      
      //           Least-Squares Problem min || A * X - B ||
      
      //           B(1:M,1:NRHS) := Q' * B(1:M,1:NRHS)
      
      zunmqr( 'L'/*Left*/, 'C'/*Conjugate transpose*/, m, 
       nrhs, n, a, lda, &work[0], b, ldb, &work[mn], lwork - 
       mn, info );
      
      //           workspace at least NRHS, optimally NRHS*NB
      
      //           B(1:N,1:NRHS) := inv(R) * B(1:N,1:NRHS)
      
      ztrsm( 'L'/*Left*/, 'U'/*Upper*/, 'N'/*No transpose*/
       , 'N'/*Non-unit*/, n, nrhs, CONE, a, lda, b, ldb );
      
      scllen = n;
      
    }
    else { 
      
      //           Overdetermined system of equations A' * X = B
      
      //           B(1:N,1:NRHS) := inv(R') * B(1:N,1:NRHS)
      
      ztrsm( 'L'/*Left*/, 'U'/*Upper*/, 'C'/*Conjugate transpose*/
       , 'N'/*Non-unit*/, n, nrhs, CONE, a, lda, b, ldb );
      
      //           B(N+1:M,1:NRHS) = ZERO
      
      for( j = 1, j_ = j - 1, _do0 = nrhs; j <= _do0; j++, j_++ ) { 
        for( i = n + 1, i_ = i - 1, _do1 = m; i <= _do1; i++, i_++ ) { 
          B(j_,i_) = CZERO;
        }
      }
      
      //           B(1:M,1:NRHS) := Q(1:N,:) * B(1:N,1:NRHS)
      
      zunmqr( 'L'/*Left*/, 'N'/*No transpose*/, m, nrhs, n, 
       a, lda, &work[0], b, ldb, &work[mn], lwork - mn, info );
      
      //           workspace at least NRHS, optimally NRHS*NB
      
      scllen = m;
      
    }
    
  }
  else { 
    
    //        Compute LQ factorization of A
    
    zgelqf( m, n, a, lda, &work[0], &work[mn], lwork - mn, info );
    
    //        workspace at least M, optimally M*NB.
    
    if( !tpsd ) { 
      
      //           underdetermined system of equations A * X = B
      
      //           B(1:M,1:NRHS) := inv(L) * B(1:M,1:NRHS)
      
      ztrsm( 'L'/*Left*/, 'L'/*Lower*/, 'N'/*No transpose*/
       , 'N'/*Non-unit*/, m, nrhs, CONE, a, lda, b, ldb );
      
      //           B(M+1:N,1:NRHS) = 0
      
      for( j = 1, j_ = j - 1, _do2 = nrhs; j <= _do2; j++, j_++ ) { 
        for( i = m + 1, i_ = i - 1, _do3 = n; i <= _do3; i++, i_++ ) { 
          B(j_,i_) = CZERO;
        }
      }
      
      //           B(1:N,1:NRHS) := Q(1:N,:)' * B(1:M,1:NRHS)
      
      zunmlq( 'L'/*Left*/, 'C'/*Conjugate transpose*/, n, 
       nrhs, m, a, lda, &work[0], b, ldb, &work[mn], lwork - 
       mn, info );
      
      //           workspace at least NRHS, optimally NRHS*NB
      
      scllen = n;
      
    }
    else { 
      
      //           overdetermined system min || A' * X - B ||
      
      //           B(1:N,1:NRHS) := Q * B(1:N,1:NRHS)
      
      zunmlq( 'L'/*Left*/, 'N'/*No transpose*/, n, nrhs, m, 
       a, lda, &work[0], b, ldb, &work[mn], lwork - mn, info );
      
      //           workspace at least NRHS, optimally NRHS*NB
      
      //           B(1:M,1:NRHS) := inv(L') * B(1:M,1:NRHS)
      
      ztrsm( 'L'/*Left*/, 'L'/*Lower*/, 'C'/*Conjugate transpose*/
       , 'N'/*Non-unit*/, m, nrhs, CONE, a, lda, b, ldb );
      
      scllen = m;
      
    }
    
  }
  
  //     Undo scaling
  
  if( iascl == 1 ) { 
    zlascl( 'G', 0, 0, anrm, smlnum, scllen, nrhs, b, ldb, info );
  }
  else if( iascl == 2 ) { 
    zlascl( 'G', 0, 0, anrm, bignum, scllen, nrhs, b, ldb, info );
  }
  if( ibscl == 1 ) { 
    zlascl( 'G', 0, 0, smlnum, bnrm, scllen, nrhs, b, ldb, info );
  }
  else if( ibscl == 2 ) { 
    zlascl( 'G', 0, 0, bignum, bnrm, scllen, nrhs, b, ldb, info );
  }
  
L_50:
  ;
  work[0] = DComplex((double)( wsize ));
  
  return;
  
  //     End of ZGELS
  
#undef  B
#undef  A
} // end of function 


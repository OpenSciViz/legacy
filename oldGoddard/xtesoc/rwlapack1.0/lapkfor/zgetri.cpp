/*
 * C++ implementation of Lapack routine zgetri
 *
 * $Id: zgetri.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:46:58
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zgetri.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const DComplex ZERO = DComplex(0.0e0);
const DComplex ONE = DComplex(1.0e0);
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zgetri(const long &n, DComplex *a, const long &lda, long ipiv[], 
 DComplex work[], const long &lwork, long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  long _do0, _do1, _do2, _do3, _do4, i, i_, iws, j, j_, jb, 
   jj, jj_, jp, ldwork, nb, nbmin, nn;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZGETRI computes the inverse of a matrix using the LU factorization
  //  computed by ZGETRF.
  
  //  This method inverts U and then computes inv(A) by solving the system
  //  inv(A)*L = inv(U) for inv(A).
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  A       (input/output) COMPLEX*16 array, dimension (LDA,N)
  //          On entry, the factors L and U from the factorization
  //          A = P*L*U as computed by ZGETRF.
  //          On exit, if INFO = 0, the inverse of the original matrix A.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  IPIV    (input) INTEGER array, dimension (N)
  //          The pivot indices from ZGETRF; for 1<=i<=N, row i of the
  //          matrix was interchanged with row IPIV(i).
  
  //  WORK    (workspace) COMPLEX*16 array, dimension (LWORK)
  //          If INFO returns 0, then WORK(1) returns N*NB, the minimum
  //          value of LWORK required to use the optimal blocksize.
  
  //  LWORK   (input) INTEGER
  //          The dimension of the array WORK.  LWORK >= max(1,N).
  //          For optimal performance LWORK should be at least N*NB,
  //          where NB is the optimal blocksize returned by ILAENV.
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: if INFO = k, U(k,k) is exactly zero; the matrix is
  //               singular and its inverse could not be computed.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  work[0] = DComplex((double)max( n, 1 ));
  if( n < 0 ) { 
    info = -1;
  }
  else if( lda < max( 1, n ) ) { 
    info = -3;
  }
  else if( lwork < n ) { 
    info = -6;
  }
  if( info != 0 ) { 
    xerbla( "ZGETRI", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  //     Form inv(U).  If INFO > 0 from ZTRTRI, then U is singular,
  //     and the inverse is not computed.
  
  ztrtri( 'U'/*Upper*/, 'N'/*Non-unit*/, n, a, lda, info );
  if( info > 0 ) 
    return;
  
  //     Determine the block size for this environment.
  
  nb = ilaenv( 1, "ZGETRI", " ", n, -1, -1, -1 );
  nbmin = 2;
  ldwork = n;
  if( nb > 1 && nb < n ) { 
    iws = max( ldwork*nb, 1 );
    if( lwork < iws ) { 
      nb = lwork/ldwork;
      nbmin = max( 2, ilaenv( 2, "ZGETRI", " ", n, -1, -1, -1 ) );
    }
  }
  else { 
    iws = n;
  }
  
  //     Solve the equation inv(A)*L = inv(U) for inv(A).
  
  if( nb < nbmin || nb >= n ) { 
    
    //        Use unblocked code.
    
    for( j = n, j_ = j - 1; j >= 1; j--, j_-- ) { 
      
      //           Copy current column of L to WORK and replace with zeros.
      
      for( i = j + 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
        work[i_] = A(j_,i_);
        A(j_,i_) = ZERO;
      }
      
      //           Compute current column of inv(A).
      
      if( j < n ) 
        zgemv( 'N'/*No transpose*/, n, n - j, -(ONE), &A(j_ + 1,0), 
         lda, &work[j_ + 1], 1, ONE, &A(j_,0), 1 );
    }
  }
  else { 
    
    //        Use blocked code.
    
    nn = ((n - 1)/nb)*nb + 1;
    for( j = nn, j_ = j - 1, _do1=docnt(j,1,_do2 = -nb); _do1 > 0; j += _do2, j_ += _do2, _do1-- ) { 
      jb = min( nb, n - j + 1 );
      
      //           Copy current block column of L to WORK and replace with
      //           zeros.
      
      for( jj = j, jj_ = jj - 1, _do3 = j + jb - 1; jj <= _do3; jj++, jj_++ ) { 
        for( i = jj + 1, i_ = i - 1, _do4 = n; i <= _do4; i++, i_++ ) { 
          work[i_ + (jj - j)*ldwork] = A(jj_,i_);
          A(jj_,i_) = ZERO;
        }
      }
      
      //           Compute current block column of inv(A).
      
      if( j + jb <= n ) 
        zgemm( 'N'/*No transpose*/, 'N'/*No transpose*/, 
         n, jb, n - j - jb + 1, -(ONE), &A(j_ + jb,0), lda, 
         &work[j_ + jb], ldwork, ONE, &A(j_,0), lda );
      ztrsm( 'R'/*Right*/, 'L'/*Lower*/, 'N'/*No transpose*/
       , 'U'/*Unit*/, n, jb, ONE, &work[j_], ldwork, &A(j_,0), 
       lda );
    }
  }
  
  //     Apply column interchanges.
  
  for( j = n - 1, j_ = j - 1; j >= 1; j--, j_-- ) { 
    jp = ipiv[j_];
    if( jp != j ) 
      zswap( n, &A(j_,0), 1, &A(jp - 1,0), 1 );
  }
  
  work[0] = DComplex((double)iws);
  return;
  
  //     End of ZGETRI
  
#undef  A
} // end of function 


/*
 * C++ implementation of Lapack routine spteqr
 *
 * $Id: spteqr.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:02:09
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: spteqr.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ZERO = 0.0;
const float ONE = 1.0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ spteqr(const char &compz, const long &n, float d[], float e[], 
 float *z, const long &ldz, float work[], long &info)
{
#define Z(I_,J_)  (*(z+(I_)*(ldz)+(J_)))
  long _do0, _do1, _do2, i, i_, icompz, nru;
  float c[1][1], vt[1][1];

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SPTEQR computes all eigenvalues and, optionally, eigenvectors of a
  //  symmetric positive definite tridiagonal matrix by first factoring the
  //  matrix using SPTTRF and then calling SBDSQR to compute the singular
  //  values of the bidiagonal factor.  This routine computes the
  //  eigenvalues of the positive definite tridiagonal matrix to high
  //  relative accuracy.  This means that if the eigenvalues range over
  //  many orders of magnitude in size, then the small eigenvalues and
  //  corresponding eigenvectors will be computed more accurately than, for
  //  example, with the standard QR method.  The eigenvectors of a full or
  //  band symmetric matrix can also be found if SSYTRD or SSPTRD or SSBTRD
  //  has been used to reduce this matrix to tridiagonal form.
  //  (The reduction to tridiagonal form, however, may preclude the
  //  possibility of obtaining high relative accuracy in the small
  //  eigenvalues of the original matrix, if these eigenvalues range over
  //  many orders of magnitude.)
  
  //  Arguments
  //  =========
  
  //  COMPZ   (input) CHARACTER*1
  //          Specifies whether eigenvectors are to be computed
  //          as follows
  
  //             COMPZ = 'N' or 'n'   Compute eigenvalues only.
  
  //             COMPZ = 'V' or 'v'   Compute eigenvectors of original
  //                                  symmetric matrix also.
  //                                  Array Z contains the orthogonal
  //                                  matrix used to reduce the original
  //                                  matrix to tridiagonal form.
  
  //             COMPZ = 'I' or 'i'   Compute eigenvectors of
  //                                  tridiagonal matrix also.
  
  //  N       (input) INTEGER
  //          The number of rows and columns in the matrix.  N >= 0.
  
  //  D       (input/output) REAL array, dimension (N)
  //          On entry, D contains the diagonal elements of the
  //          tridiagonal matrix.
  //          On normal exit, D contains the eigenvalues, in descending
  //          order.
  
  //  E       (input/output) REAL array, dimension (N)
  //          On entry, E contains the subdiagonal elements of the
  //          tridiagonal matrix in positions 1 through N-1.
  //          E(N) is arbitrary.
  //          On exit, E has been destroyed.
  
  //  Z       (input/output) REAL array, dimension (LDZ, N)
  //          If  COMPZ = 'V' or 'v', then:
  //          On entry, Z contains the orthogonal matrix used in the
  //          reduction to tridiagonal form.
  //          If  COMPZ = 'V' or 'v' or 'I' or 'i', then:
  //          On exit, Z contains the orthonormal eigenvectors of the
  //          symmetric tridiagonal (or full) matrix.  If an error exit
  //          is made, Z contains the eigenvectors associated with the
  //          stored eigenvalues.
  
  //          If  COMPZ = 'N' or 'n', then Z is not referenced.
  
  //  LDZ     (input) INTEGER
  //          The leading dimension of the array Z.  If eigenvectors are
  //          desired, then  LDZ >= max( 1, N ).  In any case, LDZ >= 1.
  
  //  WORK    (workspace) REAL array, dimension (max(1,4*N-4))
  //          Workspace used in computing eigenvectors.
  //          If  COMPZ = 'N' or 'n', then WORK is not referenced.
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit.
  //          < 0:  if INFO = -i, the i-th argument had an illegal value.
  //          > 0:  if INFO = +i, 1 <= i <= N, the Cholesky factorization
  //                              of the matrix could not be performed
  //                              because the i-th principal minor was not
  //                              positive definite.
  //                if INFO = N+i, 1 <= i <= N, the i-th singular value
  //                              of the bidiagonal factor failed to
  //                              converge.
  
  //     .. Parameters ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Local Arrays ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  
  if( lsame( compz, 'N' ) ) { 
    icompz = 0;
  }
  else if( lsame( compz, 'V' ) ) { 
    icompz = 1;
  }
  else if( lsame( compz, 'I' ) ) { 
    icompz = 2;
  }
  else { 
    icompz = -1;
  }
  if( icompz < 0 ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( (ldz < 1) || (icompz > 0 && ldz < max( 1, n )) ) { 
    info = -6;
  }
  if( info != 0 ) { 
    xerbla( "SPTEQR", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  if( n == 1 ) { 
    if( icompz > 0 ) 
      Z(0,0) = ONE;
    return;
  }
  if( icompz == 2 ) 
    slazro( n, n, ZERO, ONE, z, ldz );
  
  //     Call SPTTRF to factor the matrix.
  
  spttrf( n, d, e, info );
  if( info != 0 ) 
    return;
  for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
    d[i_] = sqrt( d[i_] );
  }
  for( i = 1, i_ = i - 1, _do1 = n - 1; i <= _do1; i++, i_++ ) { 
    e[i_] = e[i_]*d[i_];
  }
  
  //     Call SBDSQR to compute the singular values/vectors of the
  //     bidiagonal factor.
  
  if( icompz > 0 ) { 
    nru = n;
  }
  else { 
    nru = 0;
  }
  sbdsqr( 'L'/*Lower*/, n, 0, nru, 0, d, e, (float*)vt, 1, z, ldz, 
   (float*)c, 1, work, info );
  
  //     Square the singular values.
  
  if( info == 0 ) { 
    for( i = 1, i_ = i - 1, _do2 = n; i <= _do2; i++, i_++ ) { 
      d[i_] = d[i_]*d[i_];
    }
  }
  
  return;
  
  //     End of SPTEQR
  
#undef  Z
} // end of function 


/*
 * C++ implementation of Lapack routine zlaset
 *
 * $Id: zlaset.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:49:15
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zlaset.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

RWLAPKDECL void /*FUNCTION*/ zlaset(const char &uplo, const long &m, const long &n, const DComplex &alpha, 
   const DComplex &beta, DComplex *a, const long &lda)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  long _do0, _do1, _do2, _do3, _do4, _do5, _do6, _do7, _do8, 
   i, i_, j, j_;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZLASET initializes a 2-D array A to BETA on the diagonal and
  //  ALPHA on the offdiagonals.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies the part of the matrix A to be set.
  //          = 'U':      Upper triangular part is set. The lower triangle
  //                      is unchanged.
  //          = 'L':      Lower triangular part is set. The upper triangle
  //                      is unchanged.
  //          Otherwise:  All of the matrix A is set.
  
  //  M       (input) INTEGER
  //          On entry, M specifies the number of rows of A.
  
  //  N       (input) INTEGER
  //          On entry, N specifies the number of columns of A.
  
  //  ALPHA   (input) COMPLEX*16
  //          All the offdiagonal array elements are set to ALPHA.
  
  //  BETA    (input) COMPLEX*16
  //          All the diagonal array elements are set to BETA.
  
  //  A       (input/output) COMPLEX*16 array, dimension (LDA,N)
  //          On entry, the m by n matrix A.
  //          On exit, A(i,j) = ALPHA, 1 <= i <= m, 1 <= j <= n, i.ne.j;
  //                   A(i,i) = BETA , 1 <= i <= min(m,n)
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,M).
  
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  if( lsame( uplo, 'U' ) ) { 
    
    //        Set the diagonal to BETA and the strictly upper triangular
    //        part of the array to ALPHA.
    
    for( j = 2, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
      for( i = 1, i_ = i - 1, _do1 = min( j - 1, m ); i <= _do1; i++, i_++ ) { 
        A(j_,i_) = alpha;
      }
    }
    for( i = 1, i_ = i - 1, _do2 = min( n, m ); i <= _do2; i++, i_++ ) { 
      A(i_,i_) = beta;
    }
    
  }
  else if( lsame( uplo, 'L' ) ) { 
    
    //        Set the diagonal to BETA and the strictly lower triangular
    //        part of the array to ALPHA.
    
    for( j = 1, j_ = j - 1, _do3 = min( m, n ); j <= _do3; j++, j_++ ) { 
      for( i = j + 1, i_ = i - 1, _do4 = m; i <= _do4; i++, i_++ ) { 
        A(j_,i_) = alpha;
      }
    }
    for( i = 1, i_ = i - 1, _do5 = min( n, m ); i <= _do5; i++, i_++ ) { 
      A(i_,i_) = beta;
    }
    
  }
  else { 
    
    //        Set the array to BETA on the diagonal and ALPHA on the
    //        offdiagonal.
    
    for( j = 1, j_ = j - 1, _do6 = n; j <= _do6; j++, j_++ ) { 
      for( i = 1, i_ = i - 1, _do7 = m; i <= _do7; i++, i_++ ) { 
        A(j_,i_) = alpha;
      }
    }
    for( i = 1, i_ = i - 1, _do8 = min( m, n ); i <= _do8; i++, i_++ ) { 
      A(i_,i_) = beta;
    }
  }
  
  return;
  
  //     End of ZLASET
  
#undef  A
} // end of function 


/* 
 * Rogue Wave's versions of second() and dsecnd().
 *
 * Based on code from RWTools timer.cpp.
 *
 * Not a translation!
 *
 * $Log: rwscnd.cpp,v $
 * Revision 1.1  1993/03/03  16:09:47  alv
 * Initial revision
 *
 */

#include <rw/compiler.h>

#ifdef RW_NO_CLOCK
  
  // The function clock() is not defined in either <time.h>
  // (where it should be according to standard C), or in
  // <stdlib.h> (a common alternative).  Declare it here.
  // This is a traditional representation for clock().  You
  // may have to adjust CLOCKS_PER_SEC to match your own machine.

  extern "C" long clock();
  static const double CLOCKS_PER_SEC = 1e06;

#else

#include <time.h>
#include <stdlib.h>

#endif

float second()  { return (float)clock()/CLOCKS_PER_SEC; }
double dsecnd() { return (double)clock()/CLOCKS_PER_SEC; }

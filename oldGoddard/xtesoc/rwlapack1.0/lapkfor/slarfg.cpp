/*
 * C++ implementation of Lapack routine slarfg
 *
 * $Id: slarfg.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:00:24
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: slarfg.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ONE = 1.0e0;
const float ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ slarfg(const long &n, float &alpha, float x[], const long &incx, 
 float &tau)
{
  long _do0, j, j_, knt;
  float beta, rsafmn, safmin, xnorm;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SLARFG generates a real elementary reflector H of order n, such
  //  that
  
  //        H * ( alpha ) = ( beta ),   H' * H = I.
  //            (   x   )   (   0  )
  
  //  where alpha and beta are scalars, and x is an (n-1)-element real
  //  vector. H is represented in the form
  
  //        H = I - tau * ( 1 ) * ( 1 v' ) ,
  //                      ( v )
  
  //  where tau is a real scalar and v is a real (n-1)-element
  //  vector.
  
  //  If the elements of x are all zero, then tau = 0 and H is taken to be
  //  the unit matrix.
  
  //  Otherwise  1 <= tau <= 2.
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The order of the elementary reflector.
  
  //  ALPHA   (input/output) REAL
  //          On entry, the value alpha.
  //          On exit, it is overwritten with the value beta.
  
  //  X       (input/output) REAL array, dimension
  //                         (1+(N-2)*abs(INCX))
  //          On entry, the vector x.
  //          On exit, it is overwritten with the vector v.
  
  //  INCX    (input) INTEGER
  //          The increment between elements of X. INCX <> 0.
  
  //  TAU     (output) REAL
  //          The value tau.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Executable Statements ..
  
  if( n <= 1 ) { 
    tau = ZERO;
    return;
  }
  
  xnorm = snrm2( n - 1, x, incx );
  
  if( xnorm == ZERO ) { 
    
    //        H  =  I
    
    tau = ZERO;
  }
  else { 
    
    //        general case
    
    beta = -sign( slapy2( alpha, xnorm ), alpha );
    safmin = slamch( 'S' );
    if( abs( beta ) < safmin ) { 
      
      //           XNORM, BETA may be inaccurate; scale X and recompute them
      
      rsafmn = ONE/safmin;
      knt = 0;
L_10:
      ;
      knt = knt + 1;
      sscal( n - 1, rsafmn, x, incx );
      beta = beta*rsafmn;
      alpha = alpha*rsafmn;
      if( abs( beta ) < safmin ) 
        goto L_10;
      
      //           New BETA is at most 1, at least SAFMIN
      
      xnorm = snrm2( n - 1, x, incx );
      beta = -sign( slapy2( alpha, xnorm ), alpha );
      tau = (beta - alpha)/beta;
      sscal( n - 1, ONE/(alpha - beta), x, incx );
      
      //           If ALPHA is subnormal, it may lose relative accuracy
      
      alpha = beta;
      for( j = 1, j_ = j - 1, _do0 = knt; j <= _do0; j++, j_++ ) { 
        alpha = alpha*safmin;
      }
    }
    else { 
      tau = (beta - alpha)/beta;
      sscal( n - 1, ONE/(alpha - beta), x, incx );
      alpha = beta;
    }
  }
  
  return;
  
  //     End of SLARFG
  
} // end of function 


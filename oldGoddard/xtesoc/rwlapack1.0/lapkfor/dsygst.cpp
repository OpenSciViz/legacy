/*
 * C++ implementation of lapack routine dsygst
 *
 * $Id: dsygst.cpp,v 1.6 1993/04/06 20:42:30 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:38:30
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dsygst.cpp,v $
 * Revision 1.6  1993/04/06  20:42:30  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.5  1993/03/19  18:41:23  alv
 * now passes chars explicitly, rather than indirection of a string, to shut up SUN warnings
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:17:24  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:09:12  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ONE = 1.0e0;
const double HALF = 0.5e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dsygst(const long &itype, const char &uplo, const long &n, 
 double *a, const long &lda, double *b, const long &ldb, long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
  char _c0[2];
  int upper;
  long _do0, _do1, _do2, _do3, _do4, _do5, _do6, _do7, k, k_, 
   kb, nb;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DSYGST reduces a real symmetric-definite generalized eigenproblem
  //  to standard form.
  
  //  If ITYPE = 1, the problem is A*x = lambda*B*x,
  //  and A is overwritten by inv(U')*A*inv(U) or inv(L)*A*inv(L')
  
  //  If ITYPE = 2 or 3, the problem is A*B*x = lambda*x or
  //  B*A*x = lambda*x, and A is overwritten by U*A*U` or L'*A*L.
  
  //  B must have been previously factorized as U'*U or L*L' by DPOTRF.
  
  //  Arguments
  //  =========
  
  //  ITYPE   (input) INTEGER
  //          = 1: compute inv(U')*A*inv(U) or inv(L)*A*inv(L');
  //          = 2 or 3: compute U*A*U' or L'*A*L.
  
  //  UPLO    (input) CHARACTER
  //          Specifies whether the upper or lower triangular part of the
  //          symmetric matrix A is stored, and how B has been factorized.
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The order of the matrices A and B.  N >= 0.
  
  //  A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)
  //          On entry, the symmetric matrix A.  If UPLO = 'U', the leading
  //          n by n upper triangular part of A contains the upper
  //          triangular part of the matrix A, and the strictly lower
  //          triangular part of A is not referenced.  If UPLO = 'L', the
  //          leading n by n lower triangular part of A contains the lower
  //          triangular part of the matrix A, and the strictly upper
  //          triangular part of A is not referenced.
  
  //          On exit, if INFO = 0, the transformed matrix, stored in the
  //          same format as A.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  B       (input) DOUBLE PRECISION array, dimension (LDB,N)
  //          The triangular factor from the Cholesky factorization of B,
  //          as returned by DPOTRF.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit.
  //          < 0:  if INFO = -i, the i-th argument had an illegal value.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  if( itype < 1 || itype > 3 ) { 
    info = -1;
  }
  else if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -2;
  }
  else if( n < 0 ) { 
    info = -3;
  }
  else if( lda < max( 1, n ) ) { 
    info = -5;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -7;
  }
  if( info != 0 ) { 
    xerbla( "DSYGST", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  //     Determine the block size for this environment.
  
  nb = ilaenv( 1, "DSYGST", STR1(_c0,uplo), n, -1, -1, -1 );
  
  if( nb <= 1 || nb >= n ) { 
    
    //        Use unblocked code
    
    dsygs2( itype, uplo, n, a, lda, b, ldb, info );
  }
  else { 
    
    //        Use blocked code
    
    if( itype == 1 ) { 
      if( upper ) { 
        
        //              Compute inv(U')*A*inv(U)
        
        for( k = 1, k_ = k - 1, _do0=docnt(k,n,_do1 = nb); _do0 > 0; k += _do1, k_ += _do1, _do0-- ) { 
          kb = min( n - k + 1, nb );
          
          //                 Update the upper triangle of A(k:n,k:n)
          
          dsygs2( itype, uplo, kb, &A(k_,k_), lda, &B(k_,k_), 
           ldb, info );
          if( k + kb <= n ) { 
            dtrsm( 'L'/* Left */, uplo, 'T'/* Transpose */
             , 'N'/* Non-unit */, kb, n - k - kb + 1, ONE, 
             &B(k_,k_), ldb, &A(k_ + kb,k_), lda );
            dsymm( 'L'/* Left */, uplo, kb, n - k - kb + 
             1, -HALF, &A(k_,k_), lda, &B(k_ + kb,k_), 
             ldb, ONE, &A(k_ + kb,k_), lda );
            dsyr2k( uplo, 'T'/* Transpose */, n - k - kb + 
             1, kb, -ONE, &A(k_ + kb,k_), lda, &B(k_ + kb,k_), 
             ldb, ONE, &A(k_ + kb,k_ + kb), lda );
            dsymm( 'L'/* Left */, uplo, kb, n - k - kb + 
             1, -HALF, &A(k_,k_), lda, &B(k_ + kb,k_), 
             ldb, ONE, &A(k_ + kb,k_), lda );
            dtrsm( 'R'/* Right */, uplo, 'N'/* No transpose */
             , 'N'/* Non-unit */, kb, n - k - kb + 1, ONE, 
             &B(k_ + kb,k_ + kb), ldb, &A(k_ + kb,k_), 
             lda );
          }
        }
      }
      else { 
        
        //              Compute inv(L)*A*inv(L')
        
        for( k = 1, k_ = k - 1, _do2=docnt(k,n,_do3 = nb); _do2 > 0; k += _do3, k_ += _do3, _do2-- ) { 
          kb = min( n - k + 1, nb );
          
          //                 Update the lower triangle of A(k:n,k:n)
          
          dsygs2( itype, uplo, kb, &A(k_,k_), lda, &B(k_,k_), 
           ldb, info );
          if( k + kb <= n ) { 
            dtrsm( 'R'/* Right */, uplo, 'T'/* Transpose */
             , 'N'/* Non-unit */, n - k - kb + 1, kb, ONE, 
             &B(k_,k_), ldb, &A(k_,k_ + kb), lda );
            dsymm( 'R'/* Right */, uplo, n - k - kb + 1, 
             kb, -HALF, &A(k_,k_), lda, &B(k_,k_ + kb), 
             ldb, ONE, &A(k_,k_ + kb), lda );
            dsyr2k( uplo, 'N'/* No transpose */, n - k - 
             kb + 1, kb, -ONE, &A(k_,k_ + kb), lda, &B(k_,k_ + kb), 
             ldb, ONE, &A(k_ + kb,k_ + kb), lda );
            dsymm( 'R'/* Right */, uplo, n - k - kb + 1, 
             kb, -HALF, &A(k_,k_), lda, &B(k_,k_ + kb), 
             ldb, ONE, &A(k_,k_ + kb), lda );
            dtrsm( 'L'/* Left */, uplo, 'N'/* No transpose */
             , 'N'/* Non-unit */, n - k - kb + 1, kb, ONE, 
             &B(k_ + kb,k_ + kb), ldb, &A(k_,k_ + kb), 
             lda );
          }
        }
      }
    }
    else { 
      if( upper ) { 
        
        //              Compute U*A*U'
        
        for( k = 1, k_ = k - 1, _do4=docnt(k,n,_do5 = nb); _do4 > 0; k += _do5, k_ += _do5, _do4-- ) { 
          kb = min( n - k + 1, nb );
          
          //                 Update the upper triangle of A(1:k+kb-1,1:k+kb-1)
          
          dtrmm( 'L'/* Left */, uplo, 'N'/* No transpose */
           , 'N'/* Non-unit */, k - 1, kb, ONE, b, ldb, &A(k_,0), 
           lda );
          dsymm( 'R'/* Right */, uplo, k - 1, kb, HALF, &A(k_,k_), 
           lda, &B(k_,0), ldb, ONE, &A(k_,0), lda );
          dsyr2k( uplo, 'N'/* No transpose */, k - 1, kb, 
           ONE, &A(k_,0), lda, &B(k_,0), ldb, ONE, a, lda );
          dsymm( 'R'/* Right */, uplo, k - 1, kb, HALF, &A(k_,k_), 
           lda, &B(k_,0), ldb, ONE, &A(k_,0), lda );
          dtrmm( 'R'/* Right */, uplo, 'T'/* Transpose */, 
           'N'/* Non-unit */, k - 1, kb, ONE, &B(k_,k_), 
           ldb, &A(k_,0), lda );
          dsygs2( itype, uplo, kb, &A(k_,k_), lda, &B(k_,k_), 
           ldb, info );
        }
      }
      else { 
        
        //              Compute L'*A*L
        
        for( k = 1, k_ = k - 1, _do6=docnt(k,n,_do7 = nb); _do6 > 0; k += _do7, k_ += _do7, _do6-- ) { 
          kb = min( n - k + 1, nb );
          
          //                 Update the lower triangle of A(1:k+kb-1,1:k+kb-1)
          
          dtrmm( 'R'/* Right */, uplo, 'N'/* No transpose */
           , 'N'/* Non-unit */, kb, k - 1, ONE, b, ldb, &A(0,k_), 
           lda );
          dsymm( 'L'/* Left */, uplo, kb, k - 1, HALF, &A(k_,k_), 
           lda, &B(0,k_), ldb, ONE, &A(0,k_), lda );
          dsyr2k( uplo, 'T'/* Transpose */, k - 1, kb, ONE, 
           &A(0,k_), lda, &B(0,k_), ldb, ONE, a, lda );
          dsymm( 'L'/* Left */, uplo, kb, k - 1, HALF, &A(k_,k_), 
           lda, &B(0,k_), ldb, ONE, &A(0,k_), lda );
          dtrmm( 'L'/* Left */, uplo, 'T'/* Transpose */, 
           'N'/* Non-unit */, kb, k - 1, ONE, &B(k_,k_), 
           ldb, &A(0,k_), lda );
          dsygs2( itype, uplo, kb, &A(k_,k_), lda, &B(k_,k_), 
           ldb, info );
        }
      }
    }
  }
  return;
  
  //     End of DSYGST
  
#undef  B
#undef  A
} // end of function 


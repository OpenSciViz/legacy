/*
 * C++ implementation of lapack routine dlabad
 *
 * $Id: dlabad.cpp,v 1.5 1993/04/06 20:40:51 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:34:55
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dlabad.cpp,v $
 * Revision 1.5  1993/04/06  20:40:51  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:15:05  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:07:03  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

RWLAPKDECL void /*FUNCTION*/ dlabad(double &small, double &large)
{

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DLABAD takes as input the values computed by DLAMCH for underflow and
  //  overflow, and returns the square root of each of these values if the
  //  log of LARGE is sufficiently large.  This subroutine is intended to
  //  identify machines with a large exponent range, such as the Crays, and
  //  redefine the underflow and overflow limits to be the square roots of
  //  the values computed by DLAMCH.  This subroutine is needed because
  //  DLAMCH does not compensate for poor arithmetic in the upper half of
  //  the exponent range, as is found on a Cray.
  
  //  Arguments
  //  =========
  
  //  SMALL   (input/output) DOUBLE PRECISION
  //          On entry, the underflow threshold as computed by DLAMCH.
  //          On exit, if LOG10(LARGE) is sufficiently large, the square
  //          root of SMALL, otherwise unchanged.
  
  //  LARGE   (input/output) DOUBLE PRECISION
  //          On entry, the overflow threshold as computed by DLAMCH.
  //          On exit, if LOG10(LARGE) is sufficiently large, the square
  //          root of LARGE, otherwise unchanged.
  
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     If it looks like we're on a Cray, take the square root of
  //     SMALL and LARGE to avoid overflow and underflow problems.
  
  if( log10( large ) > 2000.e0 ) { 
    small = sqrt( small );
    large = sqrt( large );
  }
  
  return;
  
  //     End of DLABAD
  
} // end of function 


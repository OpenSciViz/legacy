/*
 * C++ implementation of Lapack routine zlauum
 *
 * $Id: zlauum.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:49:36
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zlauum.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ONE = 1.0e0;
const DComplex CONE = DComplex(1.0e0);
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zlauum(const char &uplo, const long &n, DComplex *a, const long &lda, 
 long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  char _c0[2];
  int upper;
  long _do0, _do1, _do2, _do3, i, i_, ib, nb;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZLAUUM computes the product U * U' or L' * L, where the triangular
  //  factor U or L is stored in the upper or lower triangular part of
  //  the array A.
  
  //  If UPLO = 'U' or 'u' then the upper triangle of the result is stored,
  //  overwriting the factor U in A.
  //  If UPLO = 'L' or 'l' then the lower triangle of the result is stored,
  //  overwriting the factor L in A.
  
  //  This is the blocked form of the algorithm, calling Level 3 BLAS.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the triangular factor stored in the array A
  //          is upper or lower triangular:
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The order of the triangular factor U or L.  N >= 0.
  
  //  A       (input/output) COMPLEX*16 array, dimension (LDA,N)
  //          On entry, the triangular factor U or L.
  //          On exit, if UPLO = 'U', the upper triangle of A is
  //          overwritten with the upper triangle of the product U * U';
  //          if UPLO = 'L', the lower triangle of A is overwritten with
  //          the lower triangle of the product L' * L.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( lda < max( 1, n ) ) { 
    info = -4;
  }
  if( info != 0 ) { 
    xerbla( "ZLAUUM", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  //     Determine the block size for this environment.
  
  nb = ilaenv( 1, "ZLAUUM", STR1(_c0,uplo), n, -1, -1, -1 );
  
  if( nb <= 1 || nb >= n ) { 
    
    //        Use unblocked code
    
    zlauu2( uplo, n, a, lda, info );
  }
  else { 
    
    //        Use blocked code
    
    if( upper ) { 
      
      //           Compute the product U * U'.
      
      for( i = 1, i_ = i - 1, _do0=docnt(i,n,_do1 = nb); _do0 > 0; i += _do1, i_ += _do1, _do0-- ) { 
        ib = min( nb, n - i + 1 );
        ztrmm( 'R'/*Right*/, 'U'/*Upper*/, 'C'/*Conjugate transpose*/
         , 'N'/*Non-unit*/, i - 1, ib, CONE, &A(i_,i_), lda, 
         &A(i_,0), lda );
        zlauu2( 'U'/*Upper*/, ib, &A(i_,i_), lda, info );
        if( i + ib <= n ) { 
          zgemm( 'N'/*No transpose*/, 'C'/*Conjugate transpose*/
           , i - 1, ib, n - i - ib + 1, CONE, &A(i_ + ib,0), 
           lda, &A(i_ + ib,i_), lda, CONE, &A(i_,0), lda );
          zherk( 'U'/*Upper*/, 'N'/*No transpose*/, ib, 
           n - i - ib + 1, ONE, &A(i_ + ib,i_), lda, ONE, 
           &A(i_,i_), lda );
        }
      }
    }
    else { 
      
      //           Compute the product L' * L.
      
      for( i = 1, i_ = i - 1, _do2=docnt(i,n,_do3 = nb); _do2 > 0; i += _do3, i_ += _do3, _do2-- ) { 
        ib = min( nb, n - i + 1 );
        ztrmm( 'L'/*Left*/, 'L'/*Lower*/, 'C'/*Conjugate transpose*/
         , 'N'/*Non-unit*/, ib, i - 1, CONE, &A(i_,i_), lda, 
         &A(0,i_), lda );
        zlauu2( 'L'/*Lower*/, ib, &A(i_,i_), lda, info );
        if( i + ib <= n ) { 
          zgemm( 'C'/*Conjugate transpose*/, 'N'/*No transpose*/
           , ib, i - 1, n - i - ib + 1, CONE, &A(i_,i_ + ib), 
           lda, &A(0,i_ + ib), lda, CONE, &A(0,i_), lda );
          zherk( 'L'/*Lower*/, 'C'/*Conjugate transpose*/
           , ib, n - i - ib + 1, ONE, &A(i_,i_ + ib), lda, 
           ONE, &A(i_,i_), lda );
        }
      }
    }
  }
  
  return;
  
  //     End of ZLAUUM
  
#undef  A
} // end of function 


/*
 * C++ implementation of Lapack routine slantb
 *
 * $Id: slantb.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:00:02
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: slantb.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ONE = 1.0e0;
const float ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL float /*FUNCTION*/ slantb(const char &norm, const char &uplo, const char &diag, const long &n, 
 const long &k, float *ab, const long &ldab, float work[])
{
#define AB(I_,J_) (*(ab+(I_)*(ldab)+(J_)))
  int udiag;
  long _do0, _do1, _do10, _do11, _do12, _do13, _do14, _do15, 
   _do16, _do17, _do18, _do19, _do2, _do20, _do21, _do22, _do23, 
   _do24, _do25, _do26, _do27, _do28, _do29, _do3, _do30, _do4, 
   _do5, _do6, _do7, _do8, _do9, i, i_, j, j_, l;
  float scale, slantb_v, sum, value;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SLANTB  returns the value of the one norm,  or the Frobenius norm, or
  //  the  infinity norm,  or the element of  largest absolute value  of an
  //  n by n triangular band matrix A,  with ( k + 1 ) diagonals.
  
  //  Description
  //  ===========
  
  //  SLANTB returns the value
  
  //     SLANTB = ( max(abs(A(i,j))), NORM = 'M' or 'm'
  //              (
  //              ( norm1(A),         NORM = '1', 'O' or 'o'
  //              (
  //              ( normI(A),         NORM = 'I' or 'i'
  //              (
  //              ( normF(A),         NORM = 'F', 'f', 'E' or 'e'
  
  //  where  norm1  denotes the  one norm of a matrix (maximum column sum),
  //  normI  denotes the  infinity norm  of a matrix  (maximum row sum) and
  //  normF  denotes the  Frobenius norm of a matrix (square root of sum of
  //  squares).  Note that  max(abs(A(i,j)))  is not a  matrix norm.
  
  //  Arguments
  //  =========
  
  //  NORM    (input) CHARACTER*1
  //          Specifies the value to be returned in SLANTB as described
  //          above.
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the matrix A is upper or lower triangular.
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  DIAG    (input) CHARACTER*1
  //          Specifies whether or not the matrix A is unit triangular.
  //          = 'N':  Non-unit triangular
  //          = 'U':  Unit triangular
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.  When N = 0, SLANTB is
  //          set to zero.
  
  //  K       (input) INTEGER
  //          The number of super-diagonals of the matrix A if UPLO = 'U',
  //          or the number of sub-diagonals of the matrix A if UPLO = 'L'.
  //          K >= 0.
  
  //  AB      (input) REAL array, dimension (LDAB,N)
  //          The upper or lower triangular band matrix A, stored in the
  //          first k+1 rows of AB.  The j-th column of A is stored
  //          in the j-th column of the array AB as follows:
  //          if UPLO = 'U', AB(k+1+i-j,j) = A(i,j) for max(1,j-k)<=i<=j;
  //          if UPLO = 'L', AB(1+i-j,j)   = A(i,j) for j<=i<=min(n,j+k).
  //          Note that when DIAG = 'U', the elements of the array AB
  //          corresponding to the diagonal elements of the matrix A are
  //          not referenced, but are assumed to be one.
  
  //  LDAB    (input) INTEGER
  //          The leading dimension of the array AB.  LDAB >= K+1.
  
  //  WORK    (workspace) REAL array, dimension (LWORK),
  //          where LWORK >= N when NORM = 'I'; otherwise, WORK is not
  //          referenced.
  
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  if( n == 0 ) { 
    value = ZERO;
  }
  else if( lsame( norm, 'M' ) ) { 
    
    //        Find max(abs(A(i,j))).
    
    if( lsame( diag, 'U' ) ) { 
      value = ONE;
      if( lsame( uplo, 'U' ) ) { 
        for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
          for( i = max( k + 2 - j, 1 ), i_ = i - 1, _do1 = k; i <= _do1; i++, i_++ ) { 
            value = max( value, abs( AB(j_,i_) ) );
          }
        }
      }
      else { 
        for( j = 1, j_ = j - 1, _do2 = n; j <= _do2; j++, j_++ ) { 
          for( i = 2, i_ = i - 1, _do3 = min( n + 1 - j, 
           k + 1 ); i <= _do3; i++, i_++ ) { 
            value = max( value, abs( AB(j_,i_) ) );
          }
        }
      }
    }
    else { 
      value = ZERO;
      if( lsame( uplo, 'U' ) ) { 
        for( j = 1, j_ = j - 1, _do4 = n; j <= _do4; j++, j_++ ) { 
          for( i = max( k + 2 - j, 1 ), i_ = i - 1, _do5 = k + 
           1; i <= _do5; i++, i_++ ) { 
            value = max( value, abs( AB(j_,i_) ) );
          }
        }
      }
      else { 
        for( j = 1, j_ = j - 1, _do6 = n; j <= _do6; j++, j_++ ) { 
          for( i = 1, i_ = i - 1, _do7 = min( n + 1 - j, 
           k + 1 ); i <= _do7; i++, i_++ ) { 
            value = max( value, abs( AB(j_,i_) ) );
          }
        }
      }
    }
  }
  else if( (lsame( norm, 'O' )) || (norm == '1') ) { 
    
    //        Find norm1(A).
    
    value = ZERO;
    udiag = lsame( diag, 'U' );
    if( lsame( uplo, 'U' ) ) { 
      for( j = 1, j_ = j - 1, _do8 = n; j <= _do8; j++, j_++ ) { 
        if( udiag ) { 
          sum = ONE;
          for( i = max( k + 2 - j, 1 ), i_ = i - 1, _do9 = k; i <= _do9; i++, i_++ ) { 
            sum = sum + abs( AB(j_,i_) );
          }
        }
        else { 
          sum = ZERO;
          for( i = max( k + 2 - j, 1 ), i_ = i - 1, _do10 = k + 
           1; i <= _do10; i++, i_++ ) { 
            sum = sum + abs( AB(j_,i_) );
          }
        }
        value = max( value, sum );
      }
    }
    else { 
      for( j = 1, j_ = j - 1, _do11 = n; j <= _do11; j++, j_++ ) { 
        if( udiag ) { 
          sum = ONE;
          for( i = 2, i_ = i - 1, _do12 = min( n + 1 - j, 
           k + 1 ); i <= _do12; i++, i_++ ) { 
            sum = sum + abs( AB(j_,i_) );
          }
        }
        else { 
          sum = ZERO;
          for( i = 1, i_ = i - 1, _do13 = min( n + 1 - j, 
           k + 1 ); i <= _do13; i++, i_++ ) { 
            sum = sum + abs( AB(j_,i_) );
          }
        }
        value = max( value, sum );
      }
    }
  }
  else if( lsame( norm, 'I' ) ) { 
    
    //        Find normI(A).
    
    value = ZERO;
    if( lsame( uplo, 'U' ) ) { 
      if( lsame( diag, 'U' ) ) { 
        for( i = 1, i_ = i - 1, _do14 = n; i <= _do14; i++, i_++ ) { 
          work[i_] = ONE;
        }
        for( j = 1, j_ = j - 1, _do15 = n; j <= _do15; j++, j_++ ) { 
          l = k + 1 - j;
          for( i = max( 1, j - k ), i_ = i - 1, _do16 = j - 
           1; i <= _do16; i++, i_++ ) { 
            work[i_] = work[i_] + abs( AB(j_,l + i_) );
          }
        }
      }
      else { 
        for( i = 1, i_ = i - 1, _do17 = n; i <= _do17; i++, i_++ ) { 
          work[i_] = ZERO;
        }
        for( j = 1, j_ = j - 1, _do18 = n; j <= _do18; j++, j_++ ) { 
          l = k + 1 - j;
          for( i = max( 1, j - k ), i_ = i - 1, _do19 = j; i <= _do19; i++, i_++ ) { 
            work[i_] = work[i_] + abs( AB(j_,l + i_) );
          }
        }
      }
    }
    else { 
      if( lsame( diag, 'U' ) ) { 
        for( i = 1, i_ = i - 1, _do20 = n; i <= _do20; i++, i_++ ) { 
          work[i_] = ONE;
        }
        for( j = 1, j_ = j - 1, _do21 = n; j <= _do21; j++, j_++ ) { 
          l = 1 - j;
          for( i = j + 1, i_ = i - 1, _do22 = min( n, j + 
           k ); i <= _do22; i++, i_++ ) { 
            work[i_] = work[i_] + abs( AB(j_,l + i_) );
          }
        }
      }
      else { 
        for( i = 1, i_ = i - 1, _do23 = n; i <= _do23; i++, i_++ ) { 
          work[i_] = ZERO;
        }
        for( j = 1, j_ = j - 1, _do24 = n; j <= _do24; j++, j_++ ) { 
          l = 1 - j;
          for( i = j, i_ = i - 1, _do25 = min( n, j + k ); i <= _do25; i++, i_++ ) { 
            work[i_] = work[i_] + abs( AB(j_,l + i_) );
          }
        }
      }
    }
    for( i = 1, i_ = i - 1, _do26 = n; i <= _do26; i++, i_++ ) { 
      value = max( value, work[i_] );
    }
  }
  else if( (lsame( norm, 'F' )) || (lsame( norm, 'E' )) ) { 
    
    //        Find normF(A).
    
    if( lsame( uplo, 'U' ) ) { 
      if( lsame( diag, 'U' ) ) { 
        scale = ONE;
        sum = n;
        if( k > 0 ) { 
          for( j = 2, j_ = j - 1, _do27 = n; j <= _do27; j++, j_++ ) { 
            slassq( min( j - 1, k ), &AB(j_,max( k + 2 - j, 1 ) - 1), 
             1, scale, sum );
          }
        }
      }
      else { 
        scale = ZERO;
        sum = ONE;
        for( j = 1, j_ = j - 1, _do28 = n; j <= _do28; j++, j_++ ) { 
          slassq( min( j, k + 1 ), &AB(j_,max( k + 2 - j, 1 ) - 1), 
           1, scale, sum );
        }
      }
    }
    else { 
      if( lsame( diag, 'U' ) ) { 
        scale = ONE;
        sum = n;
        if( k > 0 ) { 
          for( j = 1, j_ = j - 1, _do29 = n - 1; j <= _do29; j++, j_++ ) { 
            slassq( min( n - j, k ), &AB(j_,1), 1, scale, 
             sum );
          }
        }
      }
      else { 
        scale = ZERO;
        sum = ONE;
        for( j = 1, j_ = j - 1, _do30 = n; j <= _do30; j++, j_++ ) { 
          slassq( min( n - j + 1, k + 1 ), &AB(j_,0), 1, 
           scale, sum );
        }
      }
    }
    value = scale*sqrt( sum );
  }
  
  slantb_v = value;
  return( slantb_v );
  
  //     End of SLANTB
  
#undef  AB
} // end of function 


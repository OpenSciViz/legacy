/*
 * C++ implementation of Lapack routine zgbcon
 *
 * $Id: zgbcon.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:45:49
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zgbcon.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ONE = 1.0e0;
const double ZERO = 0.0e0;
// end of PARAMETER translations

inline double zgbcon_cabs1(DComplex zdum) { return abs( real( (zdum) ) ) + 
   abs( imag( (zdum) ) ); }
RWLAPKDECL void /*FUNCTION*/ zgbcon(const char &norm, const long &n, const long &kl, const long &ku, 
 DComplex *ab, const long &ldab, long ipiv[], const double &anorm, double &rcond, 
 DComplex work[], double rwork[], long &info)
{
#define AB(I_,J_) (*(ab+(I_)*(ldab)+(J_)))
  int lnoti, onenrm;
  char normin;
  long _do0, ix, j, j_, jp, kase, kase1, kd, lm;
  double ainvnm, scale, smlnum;
  DComplex t, zdum;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZGBCON estimates the reciprocal of the condition number of a DComplex
  //  general band matrix A, in either the 1-norm or the infinity-norm,
  //  using the LU factorization computed by ZGBTRF.
  
  //  An estimate is obtained for norm(inv(A)), and RCOND is computed as
  //     RCOND = 1 / ( norm(A) * norm(inv(A)) ).
  
  //  Arguments
  //  =========
  
  //  NORM    (input) CHARACTER*1
  //          Specifies whether the 1-norm condition number or the
  //          infinity-norm condition number is required:
  //          = '1' or 'O':  1-norm
  //          = 'I':         Infinity-norm
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  KL      (input) INTEGER
  //          The number of subdiagonals within the band of A.  KL >= 0.
  
  //  KU      (input) INTEGER
  //          The number of superdiagonals within the band of A.  KU >= 0.
  
  //  AB      (input) COMPLEX*16 array, dimension (LDAB,N)
  //          Details of the LU factorization of the band matrix A, as
  //          computed by ZGBTRF.  U is stored as an upper triangular band
  //          matrix with KL+KU superdiagonals in rows 1 to KL+KU+1, and
  //          the multipliers used during the factorization are stored in
  //          rows KL+KU+2 to 2*KL+KU+1.
  
  //  LDAB    (input) INTEGER
  //          The leading dimension of the array AB.  LDAB >= 2*KL+KU+1.
  
  //  IPIV    (input) INTEGER array, dimension (N)
  //          The pivot indices; for 1 <= i <= N, row i of the matrix was
  //          interchanged with row IPIV(i).
  
  //  ANORM   (input) DOUBLE PRECISION
  //          If NORM = '1' or 'O', the 1-norm of the original matrix A.
  //          If NORM = 'I', the infinity-norm of the original matrix A.
  
  //  RCOND   (output) DOUBLE PRECISION
  //          The reciprocal of the condition number of the matrix A,
  //          computed as RCOND = 1/(norm(A) * norm(inv(A))).
  
  //  WORK    (workspace) COMPLEX*16 array, dimension (2*N)
  
  //  RWORK   (workspace) DOUBLE PRECISION array, dimension (N)
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Statement Functions ..
  //     ..
  //     .. Statement Function definitions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  onenrm = norm == '1' || lsame( norm, 'O' );
  if( !onenrm && !lsame( norm, 'I' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( kl < 0 ) { 
    info = -3;
  }
  else if( ku < 0 ) { 
    info = -4;
  }
  else if( ldab < 2*kl + ku + 1 ) { 
    info = -6;
  }
  else if( anorm < ZERO ) { 
    info = -8;
  }
  if( info != 0 ) { 
    xerbla( "ZGBCON", -info );
    return;
  }
  
  //     Quick return if possible
  
  rcond = ZERO;
  if( n == 0 ) { 
    rcond = ONE;
    return;
  }
  else if( anorm == ZERO ) { 
    return;
  }
  
  smlnum = dlamch( 'S'/*Safe minimum*/ );
  
  //     Estimate the norm of inv(A).
  
  ainvnm = ZERO;
  normin = 'N';
  if( onenrm ) { 
    kase1 = 1;
  }
  else { 
    kase1 = 2;
  }
  kd = kl + ku + 1;
  lnoti = kl > 0;
  kase = 0;
L_10:
  ;
  zlacon( n, &work[n], work, ainvnm, kase );
  if( kase != 0 ) { 
    if( kase == kase1 ) { 
      
      //           Multiply by inv(L).
      
      if( lnoti ) { 
        for( j = 1, j_ = j - 1, _do0 = n - 1; j <= _do0; j++, j_++ ) { 
          lm = min( kl, n - j );
          jp = ipiv[j_];
          t = work[jp - 1];
          if( jp != j ) { 
            work[jp - 1] = work[j_];
            work[j_] = t;
          }
          zaxpy( lm, -(t), &AB(j_,kd), 1, &work[j_ + 1], 
           1 );
        }
      }
      
      //           Multiply by inv(U).
      
      zlatbs( 'U'/*Upper*/, 'N'/*No transpose*/, 'N'/*Non-unit*/
       , normin, n, kl + ku, ab, ldab, work, scale, rwork, info );
    }
    else { 
      
      //           Multiply by inv(U').
      
      zlatbs( 'U'/*Upper*/, 'C'/*Conjugate transpose*/, 'N'/*Non-unit*/
       , normin, n, kl + ku, ab, ldab, work, scale, rwork, info );
      
      //           Multiply by inv(L').
      
      if( lnoti ) { 
        for( j = n - 1, j_ = j - 1; j >= 1; j--, j_-- ) { 
          lm = min( kl, n - j );
          work[j_] = work[j_] - zdotc( lm, &AB(j_,kd), 1, 
           &work[j_ + 1], 1 );
          jp = ipiv[j_];
          if( jp != j ) { 
            t = work[jp - 1];
            work[jp - 1] = work[j_];
            work[j_] = t;
          }
        }
      }
    }
    
    //        Divide X by 1/SCALE if doing so will not cause overflow.
    
    normin = 'Y';
    if( scale != ONE ) { 
      ix = izamax( n, work, 1 );
      if( scale < zgbcon_cabs1( work[ix - 1] )*smlnum || scale == 
       ZERO ) 
        goto L_40;
      zdrscl( n, scale, work, 1 );
    }
    goto L_10;
  }
  
  //     Compute the estimate of the reciprocal condition number.
  
  if( ainvnm != ZERO ) 
    rcond = (ONE/ainvnm)/anorm;
  
L_40:
  ;
  return;
  
  //     End of ZGBCON
  
#undef  AB
} // end of function 


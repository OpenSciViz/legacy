/*
 * C++ implementation of Lapack routine zlartg
 *
 * $Id: zlartg.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:49:10
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zlartg.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ONE = 1.0e0;
const double ZERO = 0.0e0;
const DComplex CZERO = DComplex(0.0e0);
const DComplex CONE = DComplex(1.0e0);
// end of PARAMETER translations

inline double zlartg_abs1(DComplex t) { return abs( real( (t) ) ) + 
   abs( imag( (t) ) ); }
inline double zlartg_abssq(DComplex t) { return pow(real( (t) ), 2) + 
   pow(imag( (t) ), 2); }
RWLAPKDECL void /*FUNCTION*/ zlartg(const DComplex &f, const DComplex &g, double &cs, DComplex &sn, 
 DComplex &r)
{
  double d, di, f1, f2, fa, g1, g2, ga;
  DComplex fs, gs, ss, t;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZLARTG generates a plane rotation so that
  
  //     [  CS  SN  ]     [ F ]     [ R ]
  //     [  __      ]  .  [   ]  =  [   ]   where CS**2 + |SN|**2 = 1.
  //     [ -SN  CS  ]     [ G ]     [ 0 ]
  
  //  This is a faster version of the BLAS1 routine ZROTG, except for
  //  the following differences:
  //     F and G are unchanged on return.
  //     If G=0, then CS=1 and SN=0.
  //     If F=0 and (G .ne. 0), then CS=0 and SN=1 without doing any
  //        floating point operations.
  
  //  Arguments
  //  =========
  
  //  F       (input) COMPLEX*16
  //          The first component of vector to be rotated.
  
  //  G       (input) COMPLEX*16
  //          The second component of vector to be rotated.
  
  //  CS      (output) DOUBLE PRECISION
  //          The cosine of the rotation.
  
  //  SN      (output) COMPLEX*16
  //          The sine of the rotation.
  
  //  R       (output) COMPLEX*16
  //          The nonzero component of the rotated vector.
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Statement Functions ..
  //     ..
  //     .. Statement Function definitions ..
  //     ..
  //     .. Executable Statements ..
  
  //     [ 25 or 38 ops for main paths ]
  
  f1 = zlartg_abs1( f );
  g1 = zlartg_abs1( g );
  
  if( f1 >= g1 ) { 
    if( f1 == ZERO ) { 
      cs = ONE;
      sn = CZERO;
      r = CZERO;
    }
    else { 
      gs = g/f1;
      g2 = zlartg_abssq( gs );
      fs = f/f1;
      f2 = zlartg_abssq( fs );
      d = sqrt( ONE + g2/f2 );
      cs = ONE/d;
      r = f*d;
      sn = conj( gs )*fs*(cs/f2);
    }
  }
  else { 
    fs = f/g1;
    f2 = zlartg_abssq( fs );
    if( f2 == ZERO ) { 
      cs = ZERO;
      sn = CONE;
      r = g;
    }
    else { 
      fa = sqrt( f2 );
      gs = g/g1;
      g2 = zlartg_abssq( gs );
      ga = sqrt( g2 );
      d = sqrt( ONE + f2/g2 );
      di = ONE/d;
      ss = (conj( gs )*fs)/(fa*ga);
      sn = ss*di;
      cs = (fa/ga)*di;
      r = g*ss*d;
    }
  }
  return;
  
  //     End of ZLARTG
  
} // end of function 


/*
 * C++ implementation of Lapack routine sspcon
 *
 * $Id: sspcon.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:02:23
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: sspcon.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ONE = 1.0e0;
const float ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ sspcon(const char &uplo, long &n, float ap[], long ipiv[], 
 float &anorm, float &rcond, float work[], long iwork[], long &info)
{
  int upper;
  long _do0, i, i_, ip, kase;
  float ainvnm;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SSPCON estimates the reciprocal of the condition number of a real
  //  symmetric packed matrix A using the factorization A = U*D*U' or
  //  A = L*D*L' computed by SSPTRF.
  
  //  An estimate is obtained for norm(inv(A)), and the reciprocal of the
  //  condition number is computed as RCOND = 1 / (ANORM * norm(inv(A))).
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the details of the factorization are stored
  //          as an upper or lower triangular matrix.
  //          = 'U':  Upper triangular (form is A = U*D*U')
  //          = 'L':  Lower triangular (form is A = L*D*L')
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  AP      (input) REAL array, dimension (N*(N+1)/2)
  //          The block diagonal matrix D and the multipliers used to
  //          obtain the factor U or L as computed by SSPTRF, stored as a
  //          packed triangular matrix.
  
  //  IPIV    (input) INTEGER array, dimension (N)
  //          Details of the interchanges and the block structure of D
  //          as determined by SSPTRF.
  
  //  ANORM   (input) REAL
  //          The 1-norm of the original matrix A.
  
  //  RCOND   (output) REAL
  //          The reciprocal of the condition number of the matrix A,
  //          computed as RCOND = 1/(ANORM * AINVNM), where AINVNM is an
  //          estimate of the 1-norm of inv(A) computed in this routine.
  
  //  WORK    (workspace) REAL array, dimension (2*N)
  
  //  IWORK    (workspace) INTEGER array, dimension (N)
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( anorm < ZERO ) { 
    info = -5;
  }
  if( info != 0 ) { 
    xerbla( "SSPCON", -info );
    return;
  }
  
  //     Quick return if possible
  
  rcond = ZERO;
  if( n == 0 ) { 
    rcond = ONE;
    return;
  }
  else if( anorm <= ZERO ) { 
    return;
  }
  
  //     Check that the diagonal matrix D is nonsingular.
  
  if( upper ) { 
    
    //        Upper triangular storage: examine D from bottom to top
    
    ip = n*(n + 1)/2;
    for( i = n, i_ = i - 1; i >= 1; i--, i_-- ) { 
      if( ipiv[i_] > 0 && ap[ip - 1] == ZERO ) 
        return;
      ip = ip - i;
    }
  }
  else { 
    
    //        Lower triangular storage: examine D from top to bottom.
    
    ip = 1;
    for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
      if( ipiv[i_] > 0 && ap[ip - 1] == ZERO ) 
        return;
      ip = ip + n - i + 1;
    }
  }
  
  //     Estimate the 1-norm of the inverse.
  
  kase = 0;
L_30:
  ;
  slacon( n, &work[n], work, iwork, ainvnm, kase );
  if( kase != 0 ) { 
    
    //        Multiply by inv(L*D*L') or inv(U*D*U').
    
    ssptrs( uplo, n, 1, ap, ipiv, work, n, info );
    goto L_30;
  }
  
  //     Compute the estimate of the reciprocal condition number.
  
  if( ainvnm != ZERO ) 
    rcond = (ONE/ainvnm)/anorm;
  
  return;
  
  //     End of SSPCON
  
} // end of function 


/*
 * C++ implementation of Lapack routine strsna
 *
 * $Id: strsna.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:03:38
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: strsna.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ZERO = 0.0e0;
const float ONE = 1.0e0;
const float TWO = 2.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ strsna(const char &job, const char &howmny, int select[], 
 long &n, float *t, long &ldt, float *vl, long &ldvl, 
 float *vr, long &ldvr, float s[], float sep[], long &mm, 
 long &m, float *work, long &ldwork, long iwork[], long &info)
{
#define T(I_,J_)  (*(t+(I_)*(ldt)+(J_)))
#define VL(I_,J_) (*(vl+(I_)*(ldvl)+(J_)))
#define VR(I_,J_) (*(vr+(I_)*(ldvr)+(J_)))
#define WORK(I_,J_) (*(work+(I_)*(ldwork)+(J_)))
  int pair, somcon, wantbh, wants, wantsp;
  long _do0, _do1, _do2, _do3, _do4, i, i_, ierr, ifst, ilst, 
   j, j_, k, k_, kase, ks, n2, nn;
  float bignum, cond, cs, delta, dumm, dummy[1], eps, est, lnrm, 
   mu, prod, prod1, prod2, rnrm, scale, smlnum, sn;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  STRSNA estimates reciprocal condition numbers for specified
  //  eigenvalues and/or right eigenvectors of a real upper
  //  quasi-triangular matrix T (or of any matrix Q*T*Q' with Q
  //  orthogonal).
  
  //  T must be in Schur canonical form, that is, block upper triangular
  //  with 1-by-1 and 2-by-2 diagonal blocks; each 2-by-2 diagonal block
  //  has its diagonal elemnts equal and its off-diagonal elements of
  //  opposite sign.
  
  //  Arguments
  //  =========
  
  //  JOB     (input) CHARACTER*1
  //          Specifies whether condition numbers are required for
  //          eigenvalues (S) or eigenvectors (SEP):
  //          = 'E': for eigenvalues only (S);
  //          = 'V': for eigenvectors only (SEP);
  //          = 'B': for both eigenvalues and eigenvectors (S and SEP).
  
  //  HOWMNY  (input) CHARACTER*1
  //          = 'A': compute condition numbers for all eigenpairs;
  //          = 'S': compute condition numbers for selected eigenpairs
  //                 specified by the array SELECT.
  
  //  SELECT  (input) LOGICAL array, dimension (N)
  //          If HOWMNY = 'S', SELECT specifies the eigenpairs for which
  //          condition numbers are required. To select condition numbers
  //          for the eigenpair corresponding to a real eigenvalue w(j),
  //          SELECT(j) must be set to .TRUE.. To select condition numbers
  //          corresponding to a DComplex conjugate pair of eigenvalues w(j)
  //          and w(j+1), either SELECT(j) or SELECT(j+1) or both, must be
  //          set to .TRUE..
  //          If HOWMNY = 'A', SELECT is not referenced.
  
  //  N       (input) INTEGER
  //          The order of the matrix T. N >= 0.
  
  //  T       (input) REAL array, dimension (LDT,N)
  //          The upper quasi-triangular matrix T, in Schur canonical form.
  
  //  LDT     (input) INTEGER
  //          The leading dimension of the array T. LDT >= max(1,N).
  
  //  VL      (input) REAL array, dimension (LDVL,M)
  //          If JOB = 'E' or 'B', VL must contain left eigenvectors of T
  //          (or of any Q*T*Q' with Q orthogonal), corresponding to the
  //          eigenpairs specified by HOWMNY and SELECT. The eigenvectors
  //          must be stored in consecutive columns of VL, as returned by
  //          SHSEIN or STREVC.
  //          If JOB = 'V', VL is not referenced.
  
  //  LDVL    (input) INTEGER
  //          The leading dimension of the array VL.
  //          LDVL >= 1; and if JOB = 'E' or 'B', LDVL >= N.
  
  //  VR      (input) REAL array, dimension (LDVR,M)
  //          If JOB = 'E' or 'B', VR must contain right eigenvectors of T
  //          (or of any Q*T*Q' with Q orthogonal), corresponding to the
  //          eigenpairs specified by HOWMNY and SELECT. The eigenvectors
  //          must be stored in consecutive columns of VR, as returned by
  //          SHSEIN or STREVC.
  //          If JOB = 'V', VR is not referenced.
  
  //  LDVR    (input) INTEGER
  //          The leading dimension of the array VR.
  //          LDVR >= 1; and if JOB = 'E' or 'B', LDVR >= N.
  
  //  S       (output) REAL array, dimension (MM)
  //          If JOB = 'E' or 'B', the reciprocal condition numbers of the
  //          selected eigenvalues, stored in consecutive elements of the
  //          array. For a DComplex conjugate pair of eigenvalues two
  //          consecutive elements of S are set to the same value. Thus
  //          S(j), SEP(j), and the j-th columns of VL and VR all
  //          correspond to the same eigenpair (but not in general the
  //          j-th eigenpair, unless all eigenpairs are selected).
  //          If JOB = 'V', S is not referenced.
  
  //  SEP     (output) REAL array, dimension (MM)
  //          If JOB = 'V' or 'B', the estimated reciprocal condition
  //          numbers of the selected eigenvectors, stored in consecutive
  //          elements of the array. For a DComplex eigenvector two
  //          consecutive elements of SEP are set to the same value. If
  //          the eigenvalues cannot be reordered to compute SEP(j), SEP(j)
  //          is set to 0; this can only occur when the true value would be
  //          very small anyway.
  //          If JOB = 'E', SEP is not referenced.
  
  //  MM      (input) INTEGER
  //          The number of elements in the arrays S and SEP. MM >= M.
  
  //  M       (output) INTEGER
  //          The number of elements of the arrays S and SEP used to store
  //          the specified condition numbers; for each selected real
  //          eigenvalue one element is used, and for each selected DComplex
  //          conjugate pair of eigenvalues, two elements are used. If
  //          HOWMNY = 'A', M is set to N.
  
  //  WORK    (workspace) REAL array, dimension (LDWORK,N+1)
  //          If JOB = 'E', WORK is not referenced.
  
  //  LDWORK  (input) INTEGER
  //          The leading dimension of the array WORK.
  //          LDWORK >= 1; and if JOB = 'V' or 'B', LDWORK >= N.
  
  //  IWORK   (workspace) INTEGER array, dimension (N)
  //          If JOB = 'E', IWORK is not referenced.
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument had an illegal value
  
  //  Further Details
  //  ===============
  
  //  The reciprocal of the condition number of an eigenvalue lambda is
  //  defined as
  
  //          S(lambda) = |v'*u| / (norm(u)*norm(v))
  
  //  where u and v are the right and left eigenvectors of T corresponding
  //  to lambda; v' denotes the transpose of v, and norm(u) denotes the
  //  Euclidean norm. These reciprocal condition numbers always lie between
  //  zero (very badly conditioned) and one (very well conditioned). If
  //  n = 1, S(lambda) is defined to be 1.
  
  //  An approximate error bound for a computed eigenvalue W(i) is given by
  
  //                      EPS * norm(T) / S(i)
  
  //  where EPS is the machine precision.
  
  //  The reciprocal of the condition number of the right eigenvector u
  //  corresponding to lambda is defined as follows. Suppose
  
  //              T = ( lambda  c  )
  //                  (   0    T22 )
  
  //  Then the reciprocal condition number is
  
  //          SEP( lambda, T22 ) = sigma-min( T22 - lambda*I )
  
  //  where sigma-min denotes the smallest singular value. We approximate
  //  the smallest singular value by the reciprocal of an estimate of the
  //  one-norm of the inverse of T22 - lambda*I. If n = 1, SEP(1) is
  //  defined to be abs(T(1,1)).
  
  //  An approximate error bound for a computed right eigenvector VR(i)
  //  is given by
  
  //                      EPS * norm(T) / SEP(i)
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Local Arrays ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Decode and test the input parameters
  
  wantbh = lsame( job, 'B' );
  wants = lsame( job, 'E' ) || wantbh;
  wantsp = lsame( job, 'V' ) || wantbh;
  
  somcon = lsame( howmny, 'S' );
  
  info = 0;
  if( !wants && !wantsp ) { 
    info = -1;
  }
  else if( !lsame( howmny, 'A' ) && !somcon ) { 
    info = -2;
  }
  else if( n < 0 ) { 
    info = -4;
  }
  else if( ldt < max( 1, n ) ) { 
    info = -6;
  }
  else if( ldvl < 1 || (wants && ldvl < n) ) { 
    info = -8;
  }
  else if( ldvr < 1 || (wants && ldvr < n) ) { 
    info = -10;
  }
  else { 
    
    //        Set M to the number of eigenpairs for which condition numbers
    //        are required, and test MM.
    
    if( somcon ) { 
      m = 0;
      pair = FALSE;
      for( k = 1, k_ = k - 1, _do0 = n; k <= _do0; k++, k_++ ) { 
        if( pair ) { 
          pair = FALSE;
        }
        else { 
          if( k < n ) { 
            if( T(k_,k_ + 1) == ZERO ) { 
              if( select[k_] ) 
                m = m + 1;
            }
            else { 
              pair = TRUE;
              if( select[k_] || select[k_ + 1] ) 
                m = m + 2;
            }
          }
          else { 
            if( select[n - 1] ) 
              m = m + 1;
          }
        }
      }
    }
    else { 
      m = n;
    }
    
    if( mm < m ) { 
      info = -13;
    }
    else if( ldwork < 1 || (wantsp && ldwork < n) ) { 
      info = -16;
    }
  }
  if( info != 0 ) { 
    xerbla( "STRSNA", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  if( n == 1 ) { 
    if( somcon ) { 
      if( !select[0] ) 
        return;
    }
    if( wants ) 
      s[0] = ONE;
    if( wantsp ) 
      sep[0] = abs( T(0,0) );
    return;
  }
  
  //     Get machine constants
  
  eps = slamch( 'P' );
  smlnum = slamch( 'S' )/eps;
  bignum = ONE/smlnum;
  slabad( smlnum, bignum );
  
  ks = 0;
  pair = FALSE;
  for( k = 1, k_ = k - 1, _do1 = n; k <= _do1; k++, k_++ ) { 
    
    //        Determine whether T(k,k) begins a 1-by-1 or 2-by-2 block.
    
    if( pair ) { 
      pair = FALSE;
      goto L_60;
    }
    else { 
      if( k < n ) 
        pair = T(k_,k_ + 1) != ZERO;
    }
    
    //        Determine whether condition numbers are required for the k-th
    //        eigenpair.
    
    if( somcon ) { 
      if( pair ) { 
        if( !select[k_] && !select[k_ + 1] ) 
          goto L_60;
      }
      else { 
        if( !select[k_] ) 
          goto L_60;
      }
    }
    
    ks = ks + 1;
    
    if( wants ) { 
      
      //           Compute the reciprocal condition number of the k-th
      //           eigenvalue.
      
      if( !pair ) { 
        
        //              Real eigenvalue.
        
        prod = sdot( n, &VR(ks - 1,0), 1, &VL(ks - 1,0), 1 );
        rnrm = snrm2( n, &VR(ks - 1,0), 1 );
        lnrm = snrm2( n, &VL(ks - 1,0), 1 );
        s[ks - 1] = abs( prod )/(rnrm*lnrm);
      }
      else { 
        
        //              Complex eigenvalue.
        
        prod1 = sdot( n, &VR(ks - 1,0), 1, &VL(ks - 1,0), 
         1 );
        prod1 = prod1 + sdot( n, &VR(ks,0), 1, &VL(ks,0), 
         1 );
        prod2 = sdot( n, &VL(ks - 1,0), 1, &VR(ks,0), 1 );
        prod2 = prod2 - sdot( n, &VL(ks,0), 1, &VR(ks - 1,0), 
         1 );
        rnrm = slapy2( snrm2( n, &VR(ks - 1,0), 1 ), snrm2( n, 
         &VR(ks,0), 1 ) );
        lnrm = slapy2( snrm2( n, &VL(ks - 1,0), 1 ), snrm2( n, 
         &VL(ks,0), 1 ) );
        cond = slapy2( prod1, prod2 )/(rnrm*lnrm);
        s[ks - 1] = cond;
        s[ks] = cond;
      }
    }
    
    if( wantsp ) { 
      
      //           Estimate the reciprocal condition number of the k-th
      //           eigenvector.
      
      //           Copy the matrix T to the array WORK and swap the diagonal
      //           block beginning at T(k,k) to the (1,1) position.
      
      slacpy( 'F'/*Full*/, n, n, t, ldt, work, ldwork );
      ifst = k;
      ilst = 1;
      strexc( 'N'/*No Q*/, n, work, ldwork, dummy, 1, ifst, 
       ilst, &WORK(n,0), ierr );
      
      if( ierr == 1 || ierr == 2 ) { 
        
        //              Could not swap because blocks not well separated
        
        scale = ONE;
        est = bignum;
      }
      else { 
        
        //              Reordering successful
        
        if( WORK(0,1) == ZERO ) { 
          
          //                 Form C = T22 - lambda*I in WORK(2:N,2:N).
          
          for( i = 2, i_ = i - 1, _do2 = n; i <= _do2; i++, i_++ ) { 
            WORK(i_,i_) = WORK(i_,i_) - WORK(0,0);
          }
          n2 = 1;
          nn = n - 1;
        }
        else { 
          
          //                 Triangularize the 2 by 2 block by unitary
          //                 transformation U = [  cs   i*ss ]
          //                                    [ i*ss   cs  ].
          //                 such that the (1,1) position of WORK is DComplex
          //                 eigenvalue lambda with positive imaginary part. (2,2)
          //                 position of WORK is the DComplex eigenvalue lambda
          //                 with negative imaginary  part.
          
          mu = sqrt( abs( WORK(1,0) ) )*sqrt( abs( WORK(0,1) ) );
          delta = slapy2( mu, WORK(0,1) );
          cs = mu/delta;
          sn = -WORK(0,1)/delta;
          
          //                 Form
          
          //                 C' = WORK(2:N,2:N) + i*[rwork(1) ..... rwork(n-1) ]
          //                                        [   mu                     ]
          //                                        [         ..               ]
          //                                        [             ..           ]
          //                                        [                  mu      ]
          //                 where C' is conjugate transpose of DComplex matrix C,
          //                 and RWORK is stored starting in the N+1-st column of
          //                 WORK.
          
          for( j = 3, j_ = j - 1, _do3 = n; j <= _do3; j++, j_++ ) { 
            WORK(j_,1) = cs*WORK(j_,1);
            WORK(j_,j_) = WORK(j_,j_) - WORK(0,0);
          }
          WORK(1,1) = ZERO;
          
          WORK(n,0) = TWO*mu;
          for( i = 2, i_ = i - 1, _do4 = n - 1; i <= _do4; i++, i_++ ) { 
            WORK(n,i_) = sn*WORK(i_ + 1,0);
          }
          n2 = 2;
          nn = 2*(n - 1);
        }
        
        //              Estimate norm(inv(C'))
        
        est = ZERO;
        kase = 0;
L_50:
        ;
        slacon( nn, &WORK(n + 1,0), &WORK(n + 3,0), iwork, 
         est, kase );
        if( kase != 0 ) { 
          if( kase == 1 ) { 
            if( n2 == 1 ) { 
              
              //                       Real eigenvalue: solve C'*x = scale*c.
              
              slaqtr( TRUE, TRUE, n - 1, &WORK(1,1), 
               ldwork, dummy, dumm, scale, &WORK(n + 3,0), 
               &WORK(n + 5,0), ierr );
            }
            else { 
              
              //                       Complex eigenvalue: solve
              //                       C'*(p+iq) = scale*(c+id) in real arithmetic.
              
              slaqtr( TRUE, FALSE, n - 1, &WORK(1,1), 
               ldwork, &WORK(n,0), mu, scale, &WORK(n + 3,0), 
               &WORK(n + 5,0), ierr );
            }
          }
          else { 
            if( n2 == 1 ) { 
              
              //                       Real eigenvalue: solve C*x = scale*c.
              
              slaqtr( FALSE, TRUE, n - 1, &WORK(1,1), 
               ldwork, dummy, dumm, scale, &WORK(n + 3,0), 
               &WORK(n + 5,0), ierr );
            }
            else { 
              
              //                       Complex eigenvalue: solve
              //                       C*(p+iq) = scale*(c+id) in real arithmetic.
              
              slaqtr( FALSE, FALSE, n - 1, &WORK(1,1), 
               ldwork, &WORK(n,0), mu, scale, &WORK(n + 3,0), 
               &WORK(n + 5,0), ierr );
              
            }
          }
          
          goto L_50;
        }
      }
      
      sep[ks - 1] = scale/max( est, smlnum );
      if( pair ) 
        sep[ks] = sep[ks - 1];
    }
    
    if( pair ) 
      ks = ks + 1;
    
L_60:
    ;
  }
  return;
  
  //     End of STRSNA
  
#undef  WORK
#undef  VR
#undef  VL
#undef  T
} // end of function 


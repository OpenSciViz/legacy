/*
 * C++ implementation of Lapack routine slaebz
 *
 * $Id: slaebz.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:59:29
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: slaebz.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ZERO = 0.0e0;
const float TWO = 2.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ slaebz(const long &ijob, const long &nitmax, const long &n, 
 const long &mmax, const long &minp, const long &nbmin, const float &abstol, const float &reltol, 
 const float &pivmin, float d[], float e[], float e2[], long nval[], 
 float *ab, float c[], long &mout, long *nab, float work[], 
 long iwork[], long &info)
{
#define AB(I_,J_) (*(ab+(I_)*(mmax)+(J_)))
#define NAB(I_,J_)  (*(nab+(I_)*(mmax)+(J_)))
  // PARAMETER translations
  const float HALF = 1.0e0/TWO;
  // end of PARAMETER translations

  long _do0, _do1, _do10, _do11, _do2, _do3, _do4, _do5, _do6, 
   _do7, _do8, _do9, itmp1, itmp2, j, j_, ji, ji_, jit, jit_, jp, 
   jp_, kf, kfnew, kl, klnew;
  float tmp1, tmp2;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SLAEBZ contains the iteration loops which compute and use the
  //  function N(w), which is the count of eigenvalues of a symmetric
  //  tridiagonal matrix T less than or equal to its argument  w.  It
  //  performs a choice of two types of loops:
  
  //  IJOB=1, followed by
  //  IJOB=2: It takes as input a list of intervals and returns a list of
  //          sufficiently small intervals whose union contains the same
  //          eigenvalues as the union of the original intervals.
  //          The input intervals are (AB(j,1),AB(j,2)], j=1,...,MINP.
  //          The output interval (AB(j,1),AB(j,2)] will contain
  //          eigenvalues NAB(j,1)+1,...,NAB(j,2), where 1 <= j <= MOUT.
  
  //  IJOB=3: It performs a binary search in each input interval
  //          (AB(j,1),AB(j,2)] for a point  w(j)  such that
  //          N(w(j))=NVAL(j), and uses  C(j)  as the starting point of
  //          the search.  If such a w(j) is found, then on output
  //          AB(j,1)=AB(j,2)=w.  If no such w(j) is found, then on output
  //          (AB(j,1),AB(j,2)] will be a small interval containing the
  //          point where N(w) jumps through NVAL(j), unless that point
  //          lies outside the initial interval.
  
  //  Note that the intervals are in all cases half-open intervals,
  //  i.e., of the form  (a,b] , which includes  b  but not  a .
  
  //  See W. Kahan "Accurate Eigenvalues of a Symmetric Tridiagonal
  //  Matrix", Report CS41, Computer Science Dept., Stanford
  //  University, July 21, 1966
  
  //  Note: the arguments are, in general, *not* checked for unreasonable
  //  values.
  
  //  Arguments
  //  =========
  
  //  IJOB    (input) INTEGER
  //          Specifies what is to be done:
  //          = 1:  Compute NAB for the initial intervals.
  //          = 2:  Perform bisection iteration to find eigenvalues of T.
  //          = 3:  Perform bisection iteration to invert N(w), i.e.,
  //                to find a point which has a specified number of
  //                eigenvalues of T to its left.
  //          Other values will cause SLAEBZ to return with INFO=-1.
  
  //  NITMAX  (input) INTEGER
  //          The maximum number of "levels" of bisection to be
  //          performed, i.e., an interval of width W will not be made
  //          smaller than 2^(-NITMAX) * W.  If not all intervals
  //          have converged after NITMAX iterations, then INFO is set
  //          to the number of non-converged intervals.
  
  //  N       (input) INTEGER
  //          The dimension n of the tridiagonal matrix T.  It must be at
  //          least 1.
  
  //  MMAX    (input) INTEGER
  //          The maximum number of intervals.  If more than MMAX intervals
  //          are generated, then SLAEBZ will quit with INFO=MMAX+1.
  
  //  MINP    (input) INTEGER
  //          The initial number of intervals.  It may not be greater than
  //          MMAX.
  
  //  NBMIN   (input) INTEGER
  //          The smallest number of intervals that should be processed
  //          using a vector loop.  If zero, then only the scalar loop
  //          will be used.
  
  //  ABSTOL  (input) REAL
  //          The minimum (absolute) width of an interval.  When an
  //          interval is narrower than ABSTOL, or than RELTOL times the
  //          larger (in magnitude) endpoint, then it is considered to be
  //          sufficiently small, i.e., converged.  This must be at least
  //          zero.
  
  //  RELTOL  (input) REAL
  //          The minimum relative width of an interval.  When an interval
  //          is narrower than ABSTOL, or than RELTOL times the larger (in
  //          magnitude) endpoint, then it is considered to be
  //          sufficiently small, i.e., converged.  Note: this should
  //          always be at least radix*machine epsilon.
  
  //  PIVMIN  (input) REAL
  //          The minimum absolute value of a "pivot" in the Sturm
  //          sequence loop.  This *must* be at least  max |e(j)**2| *
  //          safe_min  and at least safe_min, where safe_min is at least
  //          the smallest number that can divide one without overflow.
  
  //  D       (input) REAL array, dimension (N)
  //          The diagonal entries of the tridiagonal matrix T.  To avoid
  //          underflow, the matrix should be scaled so that its largest
  //          entry is no greater than  overflow**(1/2) * underflow**(1/4)
  //          in absolute value.  To assure the most accurate computation
  //          of small eigenvalues, the matrix should be scaled to be
  //          not much smaller than that, either.
  
  //  E       (input) REAL array, dimension (N)
  //          The offdiagonal entries of the tridiagonal matrix T in
  //          positions 1 through N-1.  E(N) is arbitrary.
  //          To avoid underflow, the
  //          matrix should be scaled so that its largest entry is no
  //          greater than  overflow**(1/2) * underflow**(1/4) in absolute
  //          value.  To assure the most accurate computation of small
  //          eigenvalues, the matrix should be scaled to be not much
  //          smaller than that, either.
  
  //  E2      (input) REAL array, dimension (N)
  //          The squares of the offdiagonal entries of the tridiagonal
  //          matrix T.  E2(N) is ignored.
  
  //  NVAL    (input/output) INTEGER array, dimension (MINP)
  //          If IJOB=1 or 2, not referenced.
  //          If IJOB=3, the desired values of N(w).  The elements of NVAL
  //          will be reordered to correspond with the intervals in AB.
  //          Thus, NVAL(j) on output will not, in general be the same as
  //          NVAL(j) on input, but it will correspond with the interval
  //          (AB(j,1),AB(j,2)] on output.
  
  //  AB      (input/output) REAL array, dimension (MMAX,2)
  //          The endpoints of the intervals.  AB(j,1) is  a(j), the left
  //          endpoint of the j-th interval, and AB(j,2) is b(j), the
  //          right endpoint of the j-th interval.  The input intervals
  //          will, in general, be modified, split, and reordered by the
  //          calculation.
  
  //  C       (input/workspace) REAL array, dimension (MMAX)
  //          If IJOB=1, ignored.
  //          If IJOB=2, workspace.
  //          If IJOB=3, then on input C(j) should be initialized to the
  //          first search point in the binary search.
  
  //  MOUT    (output) INTEGER
  //          If IJOB=1, the number of eigenvalues in the intervals.
  //          If IJOB=2 or 3, the number of intervals output.
  //          If IJOB=3, MOUT will equal MINP.
  
  //  NAB     (input/output) INTEGER array, dimension (MMAX,2)
  //          If IJOB=1, then on output NAB(i,j) will be set to N(AB(i,j)).
  //          If IJOB=2, then on input, NAB(i,j) should be set.  It must
  //             satisfy the condition:
  //             N(AB(i,1)) <= NAB(i,1) <= NAB(i,2) <= N(AB(i,2)),
  //             which means that in interval i only eigenvalues
  //             NAB(i,1)+1,...,NAB(i,2) will be considered.  Usually,
  //             NAB(i,j)=N(AB(i,j)), from a previous call to SLAEBZ with
  //             IJOB=1.
  //             On output, NAB(i,j) will contain
  //             max(na(k),min(nb(k),N(AB(i,j)))), where k is the index of
  //             the input interval that the output interval
  //             (AB(j,1),AB(j,2)] came from, and na(k) and nb(k) are the
  //             the input values of NAB(k,1) and NAB(k,2).
  //          If IJOB=3, then on output, NAB(i,j) contains N(AB(i,j)),
  //             unless N(w) > NVAL(i) for all search points  w , in which
  //             case NAB(i,1) will not be modified, i.e., the output
  //             value will be the same as the input value (modulo
  //             reorderings -- see NVAL and AB), or unless N(w) < NVAL(i)
  //             for all search points  w , in which case NAB(i,2) will
  //             not be modified.  Normally, NAB should be set to some
  //             distinctive value(s) before SLAEBZ is called.
  
  //  WORK    (workspace) REAL array, dimension (MMAX)
  //          Workspace.
  
  //  IWORK   (workspace) INTEGER array, dimension (MMAX)
  //          Workspace.
  
  //  INFO    (output) INTEGER
  //          = 0:       All intervals converged.
  //          = 1--MMAX: The last INFO intervals did not converge.
  //          = MMAX+1:  More than MMAX intervals were generated.
  
  //  Further Details
  //  ===============
  
  //      This routine is intended to be called only by other LAPACK
  //  routines, thus the interface is less user-friendly.  It is intended
  //  for two purposes:
  
  //  (a) finding eigenvalues.  In this case, SLAEBZ should have one or
  //      more initial intervals set up in AB, and SLAEBZ should be called
  //      with IJOB=1.  This sets up NAB, and also counts the eigenvalues.
  //      Intervals with no eigenvalues would usually be thrown out at
  //      this point.  Also, if not all the eigenvalues in an interval i
  //      are desired, NAB(i,1) can be increased or NAB(i,2) decreased.
  //      For example, set NAB(i,1)=NAB(i,2)-1 to get the largest
  //      eigenvalue.  SLAEBZ is then called with IJOB=2 and MMAX
  //      no smaller than the value of MOUT returned by the call with
  //      IJOB=1.  After this (IJOB=2) call, eigenvalues NAB(i,1)+1
  //      through NAB(i,2) are approximately AB(i,1) (or AB(i,2)) to the
  //      tolerance specified by ABSTOL and RELTOL.
  
  //  (b) finding an interval (a',b'] containing eigenvalues w(f),...,w(l).
  //      In this case, start with a Gershgorin interval  (a,b).  Set up
  //      AB to contain 2 search intervals, both initially (a,b).  One
  //      NVAL entry should contain  f-1  and the other should contain  l
  //      , while C should contain a and b, resp.  NAB(i,1) should be -1
  //      and NAB(i,2) should be N+1, to flag an error if the desired
  //      interval does not lie in (a,b).  SLAEBZ is then called with
  //      IJOB=3.  On exit, if w(f-1) < w(f), then one of the intervals --
  //      j -- will have AB(j,1)=AB(j,2) and NAB(j,1)=NAB(j,2)=f-1, while
  //      if, to the specified tolerance, w(f-k)=...=w(f+r), k > 0 and r
  //      >= 0, then the interval will have  N(AB(j,1))=NAB(j,1)=f-k and
  //      N(AB(j,2))=NAB(j,2)=f+r.  The cases w(l) < w(l+1) and
  //      w(l-r)=...=w(l+k) are handled similarly.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Check for Errors
  
  info = 0;
  if( ijob < 1 || ijob > 3 ) { 
    info = -1;
    return;
  }
  
  //     Initialize NAB
  
  if( ijob == 1 ) { 
    
    //        Compute the number of eigenvalues in the initial intervals.
    
    mout = 0;
    for( ji = 1, ji_ = ji - 1, _do0 = minp; ji <= _do0; ji++, ji_++ ) { 
      for( jp = 1, jp_ = jp - 1; jp <= 2; jp++, jp_++ ) { 
        tmp1 = d[0] - AB(jp_,ji_);
        if( abs( tmp1 ) < pivmin ) 
          tmp1 = -pivmin;
        NAB(jp_,ji_) = 0;
        if( tmp1 <= ZERO ) 
          NAB(jp_,ji_) = 1;
        
        for( j = 2, j_ = j - 1, _do1 = n; j <= _do1; j++, j_++ ) { 
          tmp1 = d[j_] - e2[j_ - 1]/tmp1 - AB(jp_,ji_);
          if( abs( tmp1 ) < pivmin ) 
            tmp1 = -pivmin;
          if( tmp1 <= ZERO ) 
            NAB(jp_,ji_) = NAB(jp_,ji_) + 1;
        }
      }
      mout = mout + NAB(1,ji_) - NAB(0,ji_);
    }
    return;
  }
  
  //     Initialize for loop
  
  //     KF and KL have the following meaning:
  //        Intervals 1,...,KF-1 have converged.
  //        Intervals KF,...,KL  still need to be refined.
  
  kf = 1;
  kl = minp;
  
  //     If IJOB=2, initialize C.
  //     If IJOB=3, use the user-supplied starting point.
  
  if( ijob == 2 ) { 
    for( ji = 1, ji_ = ji - 1, _do2 = minp; ji <= _do2; ji++, ji_++ ) { 
      c[ji_] = HALF*(AB(0,ji_) + AB(1,ji_));
    }
  }
  
  //     Iteration loop
  
  for( jit = 1, jit_ = jit - 1, _do3 = nitmax; jit <= _do3; jit++, jit_++ ) { 
    
    //        Loop over intervals
    
    if( kl - kf + 1 >= nbmin && nbmin > 0 ) { 
      
      //           Begin of Parallel Version of the loop
      
      for( ji = kf, ji_ = ji - 1, _do4 = kl; ji <= _do4; ji++, ji_++ ) { 
        
        //              Compute N(c), the number of eigenvalues less than c
        
        work[ji_] = d[0] - c[ji_];
        if( abs( work[ji_] ) <= pivmin ) 
          work[ji_] = -pivmin;
        iwork[ji_] = 0;
        if( work[ji_] <= ZERO ) 
          iwork[ji_] = 1;
        
        for( j = 2, j_ = j - 1, _do5 = n; j <= _do5; j++, j_++ ) { 
          work[ji_] = d[j_] - e2[j_ - 1]/work[ji_] - c[ji_];
          if( abs( work[ji_] ) <= pivmin ) 
            work[ji_] = -pivmin;
          if( work[ji_] <= ZERO ) 
            iwork[ji_] = iwork[ji_] + 1;
        }
      }
      
      if( ijob <= 2 ) { 
        
        //              IJOB=2: Choose all intervals containing eigenvalues.
        
        klnew = kl;
        for( ji = kf, ji_ = ji - 1, _do6 = kl; ji <= _do6; ji++, ji_++ ) { 
          
          //                 Insure that N(w) is monotone
          
          iwork[ji_] = min( NAB(1,ji_), max( NAB(0,ji_), 
           iwork[ji_] ) );
          
          //                 Update the Queue -- add intervals if both halves
          //                 contain eigenvalues.
          
          if( iwork[ji_] == NAB(1,ji_) ) { 
            
            //                    No eigenvalue in the upper interval:
            //                    just use the lower interval.
            
            AB(1,ji_) = c[ji_];
            
          }
          else if( iwork[ji_] == NAB(0,ji_) ) { 
            
            //                    No eigenvalue in the lower interval:
            //                    just use the upper interval.
            
            AB(0,ji_) = c[ji_];
          }
          else { 
            klnew = klnew + 1;
            if( klnew <= mmax ) { 
              
              //                       Eigenvalue in both intervals -- add upper to
              //                       queue.
              
              AB(1,klnew - 1) = AB(1,ji_);
              NAB(1,klnew - 1) = NAB(1,ji_);
              AB(0,klnew - 1) = c[ji_];
              NAB(0,klnew - 1) = iwork[ji_];
              AB(1,ji_) = c[ji_];
              NAB(1,ji_) = iwork[ji_];
            }
            else { 
              info = mmax + 1;
            }
          }
        }
        if( info != 0 ) 
          return;
        kl = klnew;
      }
      else { 
        
        //              IJOB=3: Binary search.  Keep only the interval containing
        //                      w   s.t. N(w) = NVAL
        
        for( ji = kf, ji_ = ji - 1, _do7 = kl; ji <= _do7; ji++, ji_++ ) { 
          if( iwork[ji_] <= nval[ji_] ) { 
            AB(0,ji_) = c[ji_];
            NAB(0,ji_) = iwork[ji_];
          }
          if( iwork[ji_] >= nval[ji_] ) { 
            AB(1,ji_) = c[ji_];
            NAB(1,ji_) = iwork[ji_];
          }
        }
      }
      
    }
    else { 
      
      //           End of Parallel Version of the loop
      
      //           Begin of Serial Version of the loop
      
      klnew = kl;
      for( ji = kf, ji_ = ji - 1, _do8 = kl; ji <= _do8; ji++, ji_++ ) { 
        
        //              Compute N(w), the number of eigenvalues less than w
        
        tmp1 = c[ji_];
        tmp2 = d[0] - tmp1;
        if( abs( tmp2 ) <= pivmin ) 
          tmp2 = -pivmin;
        itmp1 = 0;
        if( tmp2 <= ZERO ) 
          itmp1 = 1;
        
        for( j = 2, j_ = j - 1, _do9 = n; j <= _do9; j++, j_++ ) { 
          tmp2 = d[j_] - e2[j_ - 1]/tmp2 - tmp1;
          if( abs( tmp2 ) <= pivmin ) 
            tmp2 = -pivmin;
          if( tmp2 <= ZERO ) 
            itmp1 = itmp1 + 1;
        }
        
        if( ijob <= 2 ) { 
          
          //                 IJOB=2: Choose all intervals containing eigenvalues.
          
          //                 Insure that N(w) is monotone
          
          itmp1 = min( NAB(1,ji_), max( NAB(0,ji_), itmp1 ) );
          
          //                 Update the Queue -- add intervals if both halves
          //                 contain eigenvalues.
          
          if( itmp1 == NAB(1,ji_) ) { 
            
            //                    No eigenvalue in the upper interval:
            //                    just use the lower interval.
            
            AB(1,ji_) = tmp1;
            
          }
          else if( itmp1 == NAB(0,ji_) ) { 
            
            //                    No eigenvalue in the lower interval:
            //                    just use the upper interval.
            
            AB(0,ji_) = tmp1;
          }
          else if( klnew < mmax ) { 
            
            //                    Eigenvalue in both intervals -- add upper to queue.
            
            klnew = klnew + 1;
            AB(1,klnew - 1) = AB(1,ji_);
            NAB(1,klnew - 1) = NAB(1,ji_);
            AB(0,klnew - 1) = tmp1;
            NAB(0,klnew - 1) = itmp1;
            AB(1,ji_) = tmp1;
            NAB(1,ji_) = itmp1;
          }
          else { 
            info = mmax + 1;
            return;
          }
        }
        else { 
          
          //                 IJOB=3: Binary search.  Keep only the interval
          //                         containing  w  s.t. N(w) = NVAL
          
          if( itmp1 <= nval[ji_] ) { 
            AB(0,ji_) = tmp1;
            NAB(0,ji_) = itmp1;
          }
          if( itmp1 >= nval[ji_] ) { 
            AB(1,ji_) = tmp1;
            NAB(1,ji_) = itmp1;
          }
        }
      }
      kl = klnew;
      
      //           End of Serial Version of the loop
      
    }
    
    //        Check for convergence
    
    kfnew = kf;
    for( ji = kf, ji_ = ji - 1, _do10 = kl; ji <= _do10; ji++, ji_++ ) { 
      tmp1 = abs( AB(1,ji_) - AB(0,ji_) );
      tmp2 = max( abs( AB(1,ji_) ), abs( AB(0,ji_) ) );
      if( tmp1 < vmax( abstol, pivmin, reltol*tmp2, FEND ) || 
       NAB(0,ji_) >= NAB(1,ji_) ) { 
        
        //              Converged -- Swap with position KFNEW,
        //                           then increment KFNEW
        
        if( ji > kfnew ) { 
          tmp1 = AB(0,ji_);
          tmp2 = AB(1,ji_);
          itmp1 = NAB(0,ji_);
          itmp2 = NAB(1,ji_);
          AB(0,ji_) = AB(0,kfnew - 1);
          AB(1,ji_) = AB(1,kfnew - 1);
          NAB(0,ji_) = NAB(0,kfnew - 1);
          NAB(1,ji_) = NAB(1,kfnew - 1);
          AB(0,kfnew - 1) = tmp1;
          AB(1,kfnew - 1) = tmp2;
          NAB(0,kfnew - 1) = itmp1;
          NAB(1,kfnew - 1) = itmp2;
          if( ijob == 3 ) { 
            itmp1 = nval[ji_];
            nval[ji_] = nval[kfnew - 1];
            nval[kfnew - 1] = itmp1;
          }
        }
        kfnew = kfnew + 1;
      }
    }
    kf = kfnew;
    
    //        Choose Midpoints
    
    for( ji = kf, ji_ = ji - 1, _do11 = kl; ji <= _do11; ji++, ji_++ ) { 
      c[ji_] = HALF*(AB(0,ji_) + AB(1,ji_));
    }
    
    //        If no more intervals to refine, quit.
    
    if( kf > kl ) 
      goto L_140;
  }
  
  //     Converged
  
L_140:
  ;
  info = max( kl + 1 - kf, 0 );
  mout = kl;
  
  return;
  
  //     End of SLAEBZ
  
#undef  AB
#undef  NAB
} // end of function 


/*
 * C++ implementation of lapack routine dlahrd
 *
 * $Id: dlahrd.cpp,v 1.5 1993/04/06 20:41:00 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:35:16
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dlahrd.cpp,v $
 * Revision 1.5  1993/04/06  20:41:00  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.4  1993/03/19  18:41:23  alv
 * now passes chars explicitly, rather than indirection of a string, to shut up SUN warnings
 *
 * Revision 1.3  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.2  1993/03/05  23:15:20  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:07:13  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dlahrd(const long &n, const long &k, const long &nb, double *a, 
 const long &lda, double tau[], double *t, const long &ldt, double *y, 
 const long &ldy)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define T(I_,J_)  (*(t+(I_)*(ldt)+(J_)))
#define Y(I_,J_)  (*(y+(I_)*(ldy)+(J_)))
  long _do0, i, i_;
  double ei;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DLAHRD reduces the first NB columns of a real general n-by-(n-k+1)
  //  matrix A so that elements below the k-th subdiagonal are zero. The
  //  reduction is performed by an orthogonal similarity transformation
  //  Q' * A * Q. The routine returns the matrices V and T which determine
  //  Q as a block reflector I - V*T*V', and also the matrix Y = A * V * T.
  
  //  This is an auxiliary routine called by DGEHRD.
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The order of the matrix A.
  
  //  K       (input) INTEGER
  //          The offset for the reduction. Elements below the k-th
  //          subdiagonal in the first NB columns are reduced to zero.
  
  //  NB      (input) INTEGER
  //          The number of columns to be reduced.
  
  //  A       (input/output) DOUBLE PRECISION array, dimension (LDA,N-K+1)
  //          On entry, the n-by-(n-k+1) general matrix A.
  //          On exit, the elements on and above the k-th subdiagonal in
  //          the first NB columns are overwritten with the corresponding
  //          elements of the reduced matrix; the elements below the k-th
  //          subdiagonal, with the array TAU, represent the matrix Q as a
  //          product of elementary reflectors. The other columns of A are
  //          unchanged. See Further Details.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  TAU     (output) DOUBLE PRECISION array, dimension (NB)
  //          The scalar factors of the elementary reflectors. See Further
  //          Details.
  
  //  T       (output) DOUBLE PRECISION array, dimension (NB,NB)
  //          The upper triangular matrix T.
  
  //  LDT     (input) INTEGER
  //          The leading dimension of the array T.  LDT >= NB.
  
  //  Y       (output) DOUBLE PRECISION array, dimension (LDY,NB)
  //          The n-by-nb matrix Y.
  
  //  LDY     (input) INTEGER
  //          The leading dimension of the array Y. LDY >= N.
  
  //  Further Details
  //  ===============
  
  //  The matrix Q is represented as a product of nb elementary reflectors
  
  //     Q = H(1) H(2) . . . H(nb).
  
  //  Each H(i) has the form
  
  //     H(i) = I - tau * v * v'
  
  //  where tau is a real scalar, and v is a real vector with
  //  v(1:i+k-1) = 0, v(i+k) = 1; v(i+k+1:n) is stored on exit in
  //  A(i+k+1:n,i), and tau in TAU(i).
  
  //  The elements of the vectors v together form the (n-k+1)-by-nb matrix
  //  V which is needed, with T and Y, to apply the transformation to the
  //  unreduced part of the matrix, using an update of the form:
  //  A := (I - V*T*V') * (A - Y*V').
  
  //  The contents of A on exit are illustrated by the following example
  //  with n = 7, k = 3 and nb = 2:
  
  //     ( a   h   a   a   a )
  //     ( a   h   a   a   a )
  //     ( a   h   a   a   a )
  //     ( h   h   a   a   a )
  //     ( v1  h   a   a   a )
  //     ( v1  v2  a   a   a )
  //     ( v1  v2  a   a   a )
  
  //  where a denotes an element of the original matrix A, h denotes a
  //  modified element of the upper Hessenberg matrix H, and vi denotes an
  //  element of the vector defining H(i).
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Quick return if possible
  
  if( n <= 1 ) 
    return;
  
  for( i = 1, i_ = i - 1, _do0 = nb; i <= _do0; i++, i_++ ) { 
    if( i > 1 ) { 
      
      //           Update A(1:n,i)
      
      //           Compute i-th column of A - Y * V'
      
      dgemv( 'N'/* No transpose */, n, i - 1, -ONE, y, ldy, &A(0,k + i_ - 1), 
       lda, ONE, &A(i_,0), 1 );
      
      //           Apply I - V * T' * V' to this column (call it b) from the
      //           left, using the last column of T as workspace
      
      //           Let  V = ( V1 )   and   b = ( b1 )   (first I-1 rows)
      //                    ( V2 )             ( b2 )
      
      //           where V1 is unit lower triangular
      
      //           w := V1' * b1
      
      dcopy( i - 1, &A(i_,k), 1, &T(nb - 1,0), 1 );
      dtrmv( 'L'/* Lower */, 'T'/* Transpose */, 'U'/* Unit */, 
       i - 1, &A(0,k), lda, &T(nb - 1,0), 1 );
      
      //           w := w + V2'*b2
      
      dgemv( 'T'/* Transpose */, n - k - i + 1, i - 1, ONE, &A(0,k + i_), 
       lda, &A(i_,k + i_), 1, ONE, &T(nb - 1,0), 1 );
      
      //           w := T'*w
      
      dtrmv( 'U'/* Upper */, 'T'/* Transpose */, 'N'/* Non-unit */
       , i - 1, t, ldt, &T(nb - 1,0), 1 );
      
      //           b2 := b2 - V2*w
      
      dgemv( 'N'/* No transpose */, n - k - i + 1, i - 1, -ONE, 
       &A(0,k + i_), lda, &T(nb - 1,0), 1, ONE, &A(i_,k + i_), 
       1 );
      
      //           b1 := b1 - V1*w
      
      dtrmv( 'L'/* Lower */, 'N'/* No transpose */, 'U'/* Unit */
       , i - 1, &A(0,k), lda, &T(nb - 1,0), 1 );
      daxpy( i - 1, -ONE, &T(nb - 1,0), 1, &A(i_,k), 1 );
      
      A(i_ - 1,k + i_ - 1) = ei;
    }
    
    //        Generate the elementary reflector H(i) to annihilate
    //        A(k+i+1:n,i)
    
    dlarfg( n - k - i + 1, A(i_,k + i_), &A(i_,min( k + i + 1, n ) - 1), 
     1, tau[i_] );
    ei = A(i_,k + i_);
    A(i_,k + i_) = ONE;
    
    //        Compute  Y(1:n,i)
    
    dgemv( 'N'/* No transpose */, n, n - k - i + 1, ONE, &A(i_ + 1,0), 
     lda, &A(i_,k + i_), 1, ZERO, &Y(i_,0), 1 );
    dgemv( 'T'/* Transpose */, n - k - i + 1, i - 1, ONE, &A(0,k + i_), 
     lda, &A(i_,k + i_), 1, ZERO, &T(i_,0), 1 );
    dgemv( 'N'/* No transpose */, n, i - 1, -ONE, y, ldy, &T(i_,0), 
     1, ONE, &Y(i_,0), 1 );
    dscal( n, tau[i_], &Y(i_,0), 1 );
    
    //        Compute T(1:i,i)
    
    dscal( i - 1, -tau[i_], &T(i_,0), 1 );
    dtrmv( 'U'/* Upper */, 'N'/* No transpose */, 'N'/* Non-unit */
     , i - 1, t, ldt, &T(i_,0), 1 );
    T(i_,i_) = tau[i_];
    
  }
  A(nb - 1,k + nb - 1) = ei;
  
  return;
  
  //     End of DLAHRD
  
#undef  Y
#undef  T
#undef  A
} // end of function 


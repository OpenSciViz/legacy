/*
 * C++ implementation of lapack routine dlahqr
 *
 * $Id: dlahqr.cpp,v 1.6 1993/04/06 20:40:59 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:35:14
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dlahqr.cpp,v $
 * Revision 1.6  1993/04/06  20:40:59  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.5  1993/03/19  18:41:23  alv
 * now passes chars explicitly, rather than indirection of a string, to shut up SUN warnings
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:15:19  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:07:12  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
const double DAT1 = 0.75e0;
const double DAT2 = -0.4375e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dlahqr(const int &wantt, const int &wantz, const long &n, 
 const long &ilo, const long &ihi, double *h, const long &ldh, double wr[], 
 double wi[], const long &iloz, const long &ihiz, double *z, const long &ldz, 
 long &info)
{
#define H(I_,J_)  (*(h+(I_)*(ldh)+(J_)))
#define Z(I_,J_)  (*(z+(I_)*(ldz)+(J_)))
  long _do0, _do1, _do2, _do3, _do4, _do5, _do6, _do7, _do8, 
   _do9, i, i1, i2, itn, its, its_, j, j_, k, k_, l, m, m_, nh, 
   nr, nz;
  double cs, h00, h10, h11, h12, h21, h22, h33, h33s, h43h34, h44, 
   h44s, ovfl, s, smlnum, sn, sum, t1, t2, t3, tst1, ulp, unfl, 
   v[3], v1, v2, v3, work[1];

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DLAHQR is an auxiliary routine called by DHSEQR to update the
  //  eigenvalues and Schur decomposition already computed by DHSEQR, by
  //  dealing with the Hessenberg submatrix in rows and columns ILO to IHI.
  
  //  Arguments
  //  =========
  
  //  WANTT   (input) LOGICAL
  //          = .TRUE. : the full Schur form T is required;
  //          = .FALSE.: only eigenvalues are required.
  
  //  WANTZ   (input) LOGICAL
  //          = .TRUE. : the matrix of Schur vectors Z is required;
  //          = .FALSE.: Schur vectors are not required.
  
  //  N       (input) INTEGER
  //          The order of the matrix H.  N >= 0.
  
  //  ILO     (input) INTEGER
  //  IHI     (input) INTEGER
  //          It is assumed that H is already upper quasi-triangular in
  //          rows and columns IHI+1:N, and that H(ILO,ILO-1) = 0 (unless
  //          ILO = 1). DLAHQR works primarily with the Hessenberg
  //          submatrix in rows and columns ILO to IHI, but applies
  //          transformations to all of H if WANTT is .TRUE..
  //          1 <= ILO <= max(1,IHI); IHI <= N.
  
  //  H       (input/output) DOUBLE PRECISION array, dimension (LDH,N)
  //          On entry, the upper Hessenberg matrix H.
  //          On exit, if WANTT is .TRUE., H is upper quasi-triangular in
  //          rows and columns ILO:IHI, with any 2-by-2 diagonal blocks in
  //          standard form. If WANTT is .FALSE., the contents of H are
  //          unspecified on exit.
  
  //  LDH     (input) INTEGER
  //          The leading dimension of the array H. LDH >= max(1,N).
  
  //  WR      (output) DOUBLE PRECISION array, dimension (N)
  //  WI      (output) DOUBLE PRECISION array, dimension (N)
  //          The real and imaginary parts, respectively, of the computed
  //          eigenvalues ILO to IHI are stored in the corresponding
  //          elements of WR and WI. If two eigenvalues are computed as a
  //          DComplex conjugate pair, they are stored in consecutive
  //          elements of WR and WI, say the i-th and (i+1)th, with
  //          WI(i) > 0 and WI(i+1) < 0. If WANTT is .TRUE., the
  //          eigenvalues are stored in the same order as on the diagonal
  //          of the Schur form returned in H, with WR(i) = H(i,i), and, if
  //          H(i:i+1,i:i+1) is a 2-by-2 diagonal block,
  //          WI(i) = sqrt(H(i+1,i)*H(i,i+1)) and WI(i+1) = -WI(i).
  
  //  ILOZ    (input) INTEGER
  //  IHIZ    (input) INTEGER
  //          Specify the rows of Z to which transformations must be
  //          applied if WANTZ is .TRUE..
  //          1 <= ILOZ <= ILO; IHI <= IHIZ <= N.
  
  //  Z       (input/output) DOUBLE PRECISION array, dimension (LDZ,N)
  //          If WANTZ is .TRUE., on entry Z must contain the current
  //          matrix Z of transformations accumulated by DHSEQR, and on
  //          exit Z has been updated; transformations are applied only to
  //          the submatrix Z(ILOZ:IHIZ,ILO:IHI).
  //          If WANTZ is .FALSE., Z is not referenced.
  
  //  LDZ     (input) INTEGER
  //          The leading dimension of the array Z. LDZ >= max(1,N).
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          > 0: DLAHQR failed to compute all the eigenvalues ILO to IHI
  //               in a total of 30*(IHI-ILO+1) iterations; if INFO = i,
  //               elements i+1:ihi of WR and WI contain those eigenvalues
  //               which have been successfully computed.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Local Arrays ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  info = 0;
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  if( ilo == ihi ) { 
    wr[ilo - 1] = H(ilo - 1,ilo - 1);
    wi[ilo - 1] = ZERO;
    return;
  }
  
  nh = ihi - ilo + 1;
  nz = ihiz - iloz + 1;
  
  //     Set machine-dependent constants for the stopping criterion.
  //     If norm(H) <= sqrt(OVFL), overflow should not occur.
  
  unfl = dlamch( 'S'/* Safe minimum */ );
  ovfl = ONE/unfl;
  dlabad( unfl, ovfl );
  ulp = dlamch( 'P'/* Precision */ );
  smlnum = unfl*(nh/ulp);
  
  //     I1 and I2 are the indices of the first row and last column of H
  //     to which transformations must be applied. If eigenvalues only are
  //     being computed, I1 and I2 are set inside the main loop.
  
  if( wantt ) { 
    i1 = 1;
    i2 = n;
  }
  
  //     ITN is the total number of QR iterations allowed.
  
  itn = 30*nh;
  
  //     The main loop begins here. I is the loop index and decreases from
  //     IHI to ILO in steps of 1 or 2. Each iteration of the loop works
  //     with the active submatrix in rows and columns L to I.
  //     Eigenvalues I+1 to IHI have already converged. Either L = ILO or
  //     H(L,L-1) is negligible so that the matrix splits.
  
  i = ihi;
L_10:
  ;
  l = ilo;
  if( i < ilo ) 
    goto L_150;
  
  //     Perform QR iterations on rows and columns ILO to I until a
  //     submatrix of order 1 or 2 splits off at the bottom because a
  //     subdiagonal element has become negligible.
  
  for( its = 0, its_ = its - 1, _do0 = itn; its <= _do0; its++, its_++ ) { 
    
    //        Look for a single small subdiagonal element.
    
    for( k = i, k_ = k - 1, _do1 = l + 1; k >= _do1; k--, k_-- ) { 
      tst1 = abs( H(k_ - 1,k_ - 1) ) + abs( H(k_,k_) );
      if( tst1 == ZERO ) 
        tst1 = dlanhs( '1', i - l + 1, &H(l - 1,l - 1), ldh, 
         work );
      if( abs( H(k_ - 1,k_) ) <= max( ulp*tst1, smlnum ) ) 
        goto L_30;
    }
L_30:
    ;
    l = k;
    if( l > ilo ) { 
      
      //           H(L,L-1) is negligible
      
      H(l - 2,l - 1) = ZERO;
    }
    
    //        Exit from loop if a submatrix of order 1 or 2 has split off.
    
    if( l >= i - 1 ) 
      goto L_140;
    
    //        Now the active submatrix is in rows and columns L to I. If
    //        eigenvalues only are being computed, only the active submatrix
    //        need be transformed.
    
    if( !wantt ) { 
      i1 = l;
      i2 = i;
    }
    
    if( its == 10 || its == 20 ) { 
      
      //           Exceptional shift.
      
      s = abs( H(i - 2,i - 1) ) + abs( H(i - 3,i - 2) );
      h44 = DAT1*s;
      h33 = h44;
      h43h34 = DAT2*s*s;
    }
    else { 
      
      //           Prepare to use Wilkinson's double shift
      
      h44 = H(i - 1,i - 1);
      h33 = H(i - 2,i - 2);
      h43h34 = H(i - 2,i - 1)*H(i - 1,i - 2);
    }
    
    //        Look for two consecutive small subdiagonal elements.
    
    for( m = i - 2, m_ = m - 1, _do2 = l; m >= _do2; m--, m_-- ) { 
      
      //           Determine the effect of starting the double-shift QR
      //           iteration at row M, and see if this would make H(M,M-1)
      //           negligible.
      
      h11 = H(m_,m_);
      h22 = H(m_ + 1,m_ + 1);
      h21 = H(m_,m_ + 1);
      h12 = H(m_ + 1,m_);
      h44s = h44 - h11;
      h33s = h33 - h11;
      v1 = (h33s*h44s - h43h34)/h21 + h12;
      v2 = h22 - h11 - h33s - h44s;
      v3 = H(m_ + 1,m_ + 2);
      s = abs( v1 ) + abs( v2 ) + abs( v3 );
      v1 = v1/s;
      v2 = v2/s;
      v3 = v3/s;
      v[0] = v1;
      v[1] = v2;
      v[2] = v3;
      if( m == l ) 
        goto L_50;
      h00 = H(m_ - 1,m_ - 1);
      h10 = H(m_ - 1,m_);
      tst1 = abs( v1 )*(abs( h00 ) + abs( h11 ) + abs( h22 ));
      if( abs( h10 )*(abs( v2 ) + abs( v3 )) <= ulp*tst1 ) 
        goto L_50;
    }
L_50:
    ;
    
    //        Double-shift QR step
    
    for( k = m, k_ = k - 1, _do3 = i - 1; k <= _do3; k++, k_++ ) { 
      
      //           The first iteration of this loop determines a reflection G
      //           from the vector V and applies it from left and right to H,
      //           thus creating a nonzero bulge below the subdiagonal.
      
      //           Each subsequent iteration determines a reflection G to
      //           restore the Hessenberg form in the (K-1)th column, and thus
      //           chases the bulge one step toward the bottom of the active
      //           submatrix. NR is the order of G.
      
      nr = min( 3, i - k + 1 );
      if( k > m ) 
        dcopy( nr, &H(k_ - 1,k_), 1, v, 1 );
      dlarfg( nr, v[0], &v[1], 1, t1 );
      if( k > m ) { 
        H(k_ - 1,k_) = v[0];
        H(k_ - 1,k_ + 1) = ZERO;
        if( k < i - 1 ) 
          H(k_ - 1,k_ + 2) = ZERO;
      }
      else if( m > l ) { 
        H(k_ - 1,k_) = -H(k_ - 1,k_);
      }
      v2 = v[1];
      t2 = t1*v2;
      if( nr == 3 ) { 
        v3 = v[2];
        t3 = t1*v3;
        
        //              Apply G from the left to transform the rows of the matrix
        //              in columns K to I2.
        
        for( j = k, j_ = j - 1, _do4 = i2; j <= _do4; j++, j_++ ) { 
          sum = H(j_,k_) + v2*H(j_,k_ + 1) + v3*H(j_,k_ + 2);
          H(j_,k_) = H(j_,k_) - sum*t1;
          H(j_,k_ + 1) = H(j_,k_ + 1) - sum*t2;
          H(j_,k_ + 2) = H(j_,k_ + 2) - sum*t3;
        }
        
        //              Apply G from the right to transform the columns of the
        //              matrix in rows I1 to min(K+3,I).
        
        for( j = i1, j_ = j - 1, _do5 = min( k + 3, i ); j <= _do5; j++, j_++ ) { 
          sum = H(k_,j_) + v2*H(k_ + 1,j_) + v3*H(k_ + 2,j_);
          H(k_,j_) = H(k_,j_) - sum*t1;
          H(k_ + 1,j_) = H(k_ + 1,j_) - sum*t2;
          H(k_ + 2,j_) = H(k_ + 2,j_) - sum*t3;
        }
        
        if( wantz ) { 
          
          //                 Accumulate transformations in the matrix Z
          
          for( j = iloz, j_ = j - 1, _do6 = ihiz; j <= _do6; j++, j_++ ) { 
            sum = Z(k_,j_) + v2*Z(k_ + 1,j_) + v3*Z(k_ + 2,j_);
            Z(k_,j_) = Z(k_,j_) - sum*t1;
            Z(k_ + 1,j_) = Z(k_ + 1,j_) - sum*t2;
            Z(k_ + 2,j_) = Z(k_ + 2,j_) - sum*t3;
          }
        }
      }
      else if( nr == 2 ) { 
        
        //              Apply G from the left to transform the rows of the matrix
        //              in columns K to I2.
        
        for( j = k, j_ = j - 1, _do7 = i2; j <= _do7; j++, j_++ ) { 
          sum = H(j_,k_) + v2*H(j_,k_ + 1);
          H(j_,k_) = H(j_,k_) - sum*t1;
          H(j_,k_ + 1) = H(j_,k_ + 1) - sum*t2;
        }
        
        //              Apply G from the right to transform the columns of the
        //              matrix in rows I1 to min(K+3,I).
        
        for( j = i1, j_ = j - 1, _do8 = i; j <= _do8; j++, j_++ ) { 
          sum = H(k_,j_) + v2*H(k_ + 1,j_);
          H(k_,j_) = H(k_,j_) - sum*t1;
          H(k_ + 1,j_) = H(k_ + 1,j_) - sum*t2;
        }
        
        if( wantz ) { 
          
          //                 Accumulate transformations in the matrix Z
          
          for( j = iloz, j_ = j - 1, _do9 = ihiz; j <= _do9; j++, j_++ ) { 
            sum = Z(k_,j_) + v2*Z(k_ + 1,j_);
            Z(k_,j_) = Z(k_,j_) - sum*t1;
            Z(k_ + 1,j_) = Z(k_ + 1,j_) - sum*t2;
          }
        }
      }
    }
    
  }
  
  //     Failure to converge in remaining number of iterations
  
  info = i;
  return;
  
L_140:
  ;
  
  if( l == i ) { 
    
    //        H(I,I-1) is negligible: one eigenvalue has converged.
    
    wr[i - 1] = H(i - 1,i - 1);
    wi[i - 1] = ZERO;
  }
  else if( l == i - 1 ) { 
    
    //        H(I-1,I-2) is negligible: a pair of eigenvalues have converged.
    
    //        Transform the 2-by-2 submatrix to standard Schur form,
    //        and compute and store the eigenvalues.
    
    dlanv2( H(i - 2,i - 2), H(i - 1,i - 2), H(i - 2,i - 1), H(i - 1,i - 1), 
     wr[i - 2], wi[i - 2], wr[i - 1], wi[i - 1], cs, sn );
    
    if( wantt ) { 
      
      //           Apply the transformation to the rest of H and to Z, as
      //           required.
      
      if( i2 > i ) 
        drot( i2 - i, &H(i,i - 2), ldh, &H(i,i - 1), ldh, 
         cs, sn );
      drot( i - i1 - 1, &H(i - 2,i1 - 1), 1, &H(i - 1,i1 - 1), 
       1, cs, sn );
      if( wantz ) { 
        drot( nz, &Z(i - 2,iloz - 1), 1, &Z(i - 1,iloz - 1), 
         1, cs, sn );
      }
    }
  }
  
  //     Decrement number of remaining iterations, and return to start of
  //     the main loop with new value of I.
  
  itn = itn - its;
  i = l - 1;
  goto L_10;
  
L_150:
  ;
  return;
  
  //     End of DLAHQR
  
#undef  Z
#undef  H
} // end of function 


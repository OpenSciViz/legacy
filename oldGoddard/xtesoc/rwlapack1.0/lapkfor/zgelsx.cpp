/*
 * C++ implementation of Lapack routine zgelsx
 *
 * $Id: zgelsx.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:46:29
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zgelsx.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const long IMAX = 1;
const long IMIN = 2;
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
const DComplex CZERO = DComplex(0.0e0);
const DComplex CONE = DComplex(1.0e0);
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zgelsx(const long &m, const long &n, const long &nrhs, 
 DComplex *a, const long &lda, DComplex *b, const long &ldb, long jpvt[], 
 const double &rcond, const long &rank, DComplex work[], double rwork[], long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
  // PARAMETER translations
  const double DONE = ZERO;
  const double NTDONE = ONE;
  // end of PARAMETER translations

  long _do0, _do1, _do2, _do3, _do4, _do5, _do6, i, i_, iascl, 
   ibscl, ismax, ismin, j, j_, k, mn;
  double anrm, bignum, bnrm, smax, smaxpr, smin, sminpr, smlnum;
  DComplex c1, c2, s1, s2, t1, t2;

  
  //  -- LAPACK driver routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZGELSX computes the minimum-norm solution to
  //      min || A * X - B ||
  //  using a complete orthogonal factorization. That is, first we compute
  //  a QR factorization with column pivoting
  //      A * P = Q * [ R11 R12 ]
  //                  [  0  R22 ]
  //  with the size RANK of R11 such that it is the largest leading
  //  submatrix whose condition number is less than 1/RCOND.
  //  Then, considering R22 to be negligible, we annihilate R12 through
  //  orthogonal transformations from the right, arriving at
  //     A * P = Q * [ T11 0 ] * Y
  //                 [  0  0 ]
  //  The minimum-norm solution is then
  //     X = P * Y' [ inv(T11)*Q1'*B ]
  //                [        0       ]
  //  where Q1 are the first RANK columns of Q.
  
  //  Arguments
  //  =========
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix A.  M >= 0.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix A.  N >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right-hand sides. NRHS >=0.
  
  //  A       (input/output) COMPLEX*16 array, dimension (LDA,N)
  //          On entry, the M by N matrix A.
  //          On exit, A has been overwritten by its complete orthogonal
  //          factorization.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,M).
  
  //  B       (input/output) COMPLEX*16 array, dimension (LDB,NRHS)
  //          on entry, the M by NRHS right-hand side.
  //          on exit, the first N rows of B contain the minimum-norm
  //                   solution.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B. LDB >= max(1,M,N).
  
  //  JPVT    (input/output) INTEGER array, dimension (N)
  //          on entry: If JPVT(i) <> 0, a(:,i) is permuted
  //          to the front of AP, otherwise column i is a free column.
  //          on exit: If JPVT(i) = k, then the i-th column of AP
  //          was the k-th column of A.
  
  //  RCOND   (input) DOUBLE PRECISION
  //          The goal of the complete orthogonal factorization is to
  //          identify a well-conditioned triangular matrix whose
  //          condition number is less than 1/RCOND.
  
  //  RANK    (output) INTEGER
  //          The size of the largest leading triangular matrix in the
  //          QR factorization (with pivoting) of A, whose condition number
  //          is less than 1/RCOND.
  
  //  WORK    (workspace) COMPLEX*16 array, dimension
  //                      (MN + max( N, 2*MN, MN+NRHS )),
  //          where MN = min(M,N)
  
  //  RWORK   (workspace) DOUBLE PRECISION array, dimension (2*N)
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  mn = min( m, n );
  ismin = mn + 1;
  ismax = 2*mn + 1;
  
  //     Test the input arguments.
  
  info = 0;
  if( m < 0 ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( nrhs < 0 ) { 
    info = -3;
  }
  else if( lda < max( 1, m ) ) { 
    info = -5;
  }
  else if( ldb < vmax( 1, m, n, IEND ) ) { 
    info = -7;
  }
  
  if( info != 0 ) { 
    xerbla( "ZGELSX", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( vmin( m, n, nrhs, IEND ) == 0 ) { 
    rank = 0;
    return;
  }
  
  //     Get machine parameters
  
  smlnum = dlamch( 'S' )/dlamch( 'P' );
  bignum = ONE/smlnum;
  dlabad( smlnum, bignum );
  
  //     Scale A, B if max entries outside range [SMLNUM,BIGNUM]
  
  anrm = zlange( 'M', m, n, a, lda, rwork );
  iascl = 0;
  if( anrm > ZERO && anrm < smlnum ) { 
    
    //        Scale matrix norm up to SMLNUM
    
    zlascl( 'G', 0, 0, anrm, smlnum, m, n, a, lda, info );
    iascl = 1;
  }
  else if( anrm > bignum ) { 
    
    //        Scale matrix norm down to BIGNUM
    
    zlascl( 'G', 0, 0, anrm, bignum, m, n, a, lda, info );
    iascl = 2;
  }
  else if( anrm == ZERO ) { 
    
    //        Matrix all zero. Return zero solution.
    
    zlaset( 'F', max( m, n ), nrhs, CZERO, CZERO, b, ldb );
    rank = 0;
    goto L_100;
  }
  
  bnrm = zlange( 'M', m, nrhs, b, ldb, rwork );
  ibscl = 0;
  if( bnrm > ZERO && bnrm < smlnum ) { 
    
    //        Scale matrix norm up to SMLNUM
    
    zlascl( 'G', 0, 0, bnrm, smlnum, m, nrhs, b, ldb, info );
    ibscl = 1;
  }
  else if( bnrm > bignum ) { 
    
    //        Scale matrix norm down to BIGNUM
    
    zlascl( 'G', 0, 0, bnrm, bignum, m, nrhs, b, ldb, info );
    ibscl = 2;
  }
  
  //     Compute QR factorization with column pivoting of A:
  //        A * P = Q * R
  
  zgeqpf( m, n, a, lda, jpvt, &work[0], &work[mn], rwork, info );
  
  //     DComplex workspace MN+N. Real workspace 2*N. Details of Householder
  //     rotations stored in WORK(1:MN).
  
  //     Determine RANK using incremental condition estimation
  
  work[ismin - 1] = CONE;
  work[ismax - 1] = CONE;
  smax = abs( A(0,0) );
  smin = smax;
  if( abs( A(0,0) ) == ZERO ) { 
    rank = 0;
    zlaset( 'F', max( m, n ), nrhs, CZERO, CZERO, b, ldb );
    goto L_100;
  }
  else { 
    rank = 1;
  }
  
L_10:
  ;
  if( rank < mn ) { 
    i = rank + 1;
    zlaic1( IMIN, rank, &work[ismin - 1], smin, &A(i - 1,0), A(i - 1,i - 1), 
     sminpr, s1, c1 );
    zlaic1( IMAX, rank, &work[ismax - 1], smax, &A(i - 1,0), A(i - 1,i - 1), 
     smaxpr, s2, c2 );
    
    if( smaxpr*rcond <= sminpr ) { 
      for( i = 1, i_ = i - 1, _do0 = rank; i <= _do0; i++, i_++ ) { 
        work[ismin + i_ - 1] = s1*work[ismin + i_ - 1];
        work[ismax + i_ - 1] = s2*work[ismax + i_ - 1];
      }
      work[ismin + rank - 1] = c1;
      work[ismax + rank - 1] = c2;
      smin = sminpr;
      smax = smaxpr;
      rank = rank + 1;
      goto L_10;
    }
  }
  
  //     Logically partition R = [ R11 R12 ]
  //                             [  0  R22 ]
  //     where R11 = R(1:RANK,1:RANK)
  
  //     [R11,R12] = [ T11, 0 ] * Y
  
  if( rank < n ) 
    ztzrqf( rank, n, a, lda, &work[mn], info );
  
  //     Details of Householder rotations stored in WORK(MN+1:2*MN)
  
  //     B(1:M,1:NRHS) := Q' * B(1:M,1:NRHS)
  
  zunm2r( 'L'/*Left*/, 'C'/*Conjugate transpose*/, m, nrhs, mn, 
   a, lda, &work[0], b, ldb, &work[mn*2], info );
  
  //     workspace NRHS
  
  //      B(1:RANK,1:NRHS) := inv(T11) * B(1:RANK,1:NRHS)
  
  ztrsm( 'L'/*Left*/, 'U'/*Upper*/, 'N'/*No transpose*/, 'N'/*Non-unit*/
   , rank, nrhs, CONE, a, lda, b, ldb );
  
  for( i = rank + 1, i_ = i - 1, _do1 = n; i <= _do1; i++, i_++ ) { 
    for( j = 1, j_ = j - 1, _do2 = nrhs; j <= _do2; j++, j_++ ) { 
      B(j_,i_) = CZERO;
    }
  }
  
  //     B(1:N,1:NRHS) := Y' * B(1:N,1:NRHS)
  
  if( rank < n ) { 
    for( i = 1, i_ = i - 1, _do3 = rank; i <= _do3; i++, i_++ ) { 
      zlatzm( 'L'/*Left*/, n - rank + 1, nrhs, &A(rank,i_), 
       lda, conj( work[mn + i_] ), &B(0,i_), &B(0,rank), ldb, 
       &work[mn*2] );
    }
  }
  
  //     workspace NRHS
  
  //     B(1:N,1:NRHS) := P * B(1:N,1:NRHS)
  
  for( j = 1, j_ = j - 1, _do4 = nrhs; j <= _do4; j++, j_++ ) { 
    for( i = 1, i_ = i - 1, _do5 = n; i <= _do5; i++, i_++ ) { 
      work[2*mn + i_] = DComplex(NTDONE);
    }
    for( i = 1, i_ = i - 1, _do6 = n; i <= _do6; i++, i_++ ) { 

/*
 * C++ implementation of Lapack routine zhegst
 *
 * $Id: zhegst.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:47:23
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zhegst.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ONE = 1.0e0;
const DComplex CONE = DComplex(1.0e0);
const DComplex HALF = DComplex(0.5e0);
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zhegst(const long &itype, const char &uplo, const long &n, 
 DComplex *a, const long &lda, DComplex *b, const long &ldb, long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
  char _c0[2];
  int upper;
  long _do0, _do1, _do2, _do3, _do4, _do5, _do6, _do7, k, k_, 
   kb, nb;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZHEGST reduces a DComplex Hermitian-definite generalized
  //  eigenproblem to standard form.
  
  //  If ITYPE = 1, the problem is A*x = lambda*B*x,
  //  and A is overwritten by inv(U')*A*inv(U) or inv(L)*A*inv(L')
  
  //  If ITYPE = 2 or 3, the problem is A*B*x = lambda*x or
  //  B*A*x = lambda*x, and A is overwritten by U*A*U` or L'*A*L.
  
  //  B must have been previously factorized as U'*U or L*L' by ZPOTRF.
  
  //  Arguments
  //  =========
  
  //  ITYPE   (input) INTEGER
  //          = 1: compute inv(U')*A*inv(U) or inv(L)*A*inv(L');
  //          = 2 or 3: compute U*A*U' or L'*A*L.
  
  //  UPLO    (input) CHARACTER
  //          Specifies whether the upper or lower triangular part of the
  //          Hermitian matrix A is stored, and how B has been factorized.
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The order of the matrices A and B.  N >= 0.
  
  //  A       (input/output) COMPLEX*16 array, dimension (LDA,N)
  //          On entry, the Hermitian matrix A.  If UPLO = 'U', the leading
  //          n by n upper triangular part of A contains the upper
  //          triangular part of the matrix A, and the strictly lower
  //          triangular part of A is not referenced.  If UPLO = 'L', the
  //          leading n by n lower triangular part of A contains the lower
  //          triangular part of the matrix A, and the strictly upper
  //          triangular part of A is not referenced.
  
  //          On exit, if INFO = 0, the transformed matrix, stored in the
  //          same format as A.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  B       (input) COMPLEX*16 array, dimension (LDB,N)
  //          The triangular factor from the Cholesky factorization of B,
  //          as returned by ZPOTRF.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit.
  //          < 0:  if INFO = -i, the i-th argument had an illegal value.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  if( itype < 1 || itype > 3 ) { 
    info = -1;
  }
  else if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -2;
  }
  else if( n < 0 ) { 
    info = -3;
  }
  else if( lda < max( 1, n ) ) { 
    info = -5;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -7;
  }
  if( info != 0 ) { 
    xerbla( "ZHEGST", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  //     Determine the block size for this environment.
  
  nb = ilaenv( 1, "ZHEGST", STR1(_c0,uplo), n, -1, -1, -1 );
  
  if( nb <= 1 || nb >= n ) { 
    
    //        Use unblocked code
    
    zhegs2( itype, uplo, n, a, lda, b, ldb, info );
  }
  else { 
    
    //        Use blocked code
    
    if( itype == 1 ) { 
      if( upper ) { 
        
        //              Compute inv(U')*A*inv(U)
        
        for( k = 1, k_ = k - 1, _do0=docnt(k,n,_do1 = nb); _do0 > 0; k += _do1, k_ += _do1, _do0-- ) { 
          kb = min( n - k + 1, nb );
          
          //                 Update the upper triangle of A(k:n,k:n)
          
          zhegs2( itype, uplo, kb, &A(k_,k_), lda, &B(k_,k_), 
           ldb, info );
          if( k + kb <= n ) { 
            ztrsm( 'L'/*Left*/, uplo, 'C'/*Conjugate transpose*/
             , 'N'/*Non-unit*/, kb, n - k - kb + 1, CONE, 
             &B(k_,k_), ldb, &A(k_ + kb,k_), lda );
            zhemm( 'L'/*Left*/, uplo, kb, n - k - kb + 
             1, -(HALF), &A(k_,k_), lda, &B(k_ + kb,k_), 
             ldb, CONE, &A(k_ + kb,k_), lda );
            zher2k( uplo, 'C'/*Conjugate transpose*/, 
             n - k - kb + 1, kb, CONE, &A(k_ + kb,k_), 
             lda, &B(k_ + kb,k_), ldb, ONE, &A(k_ + kb,k_ + kb), 
             lda );
            zhemm( 'L'/*Left*/, uplo, kb, n - k - kb + 
             1, -(HALF), &A(k_,k_), lda, &B(k_ + kb,k_), 
             ldb, CONE, &A(k_ + kb,k_), lda );
            ztrsm( 'R'/*Right*/, uplo, 'N'/*No transpose*/
             , 'N'/*Non-unit*/, kb, n - k - kb + 1, CONE, 
             &B(k_ + kb,k_ + kb), ldb, &A(k_ + kb,k_), 
             lda );
          }
        }
      }
      else { 
        
        //              Compute inv(L)*A*inv(L')
        
        for( k = 1, k_ = k - 1, _do2=docnt(k,n,_do3 = nb); _do2 > 0; k += _do3, k_ += _do3, _do2-- ) { 
          kb = min( n - k + 1, nb );
          
          //                 Update the lower triangle of A(k:n,k:n)
          
          zhegs2( itype, uplo, kb, &A(k_,k_), lda, &B(k_,k_), 
           ldb, info );
          if( k + kb <= n ) { 
            ztrsm( 'R'/*Right*/, uplo, 'C'/*Conjugate transpose*/
             , 'N'/*Non-unit*/, n - k - kb + 1, kb, CONE, 
             &B(k_,k_), ldb, &A(k_,k_ + kb), lda );
            zhemm( 'R'/*Right*/, uplo, n - k - kb + 1, 
             kb, -(HALF), &A(k_,k_), lda, &B(k_,k_ + kb), 
             ldb, CONE, &A(k_,k_ + kb), lda );
            zher2k( uplo, 'N'/*No transpose*/, n - k - 
             kb + 1, kb, -(CONE), &A(k_,k_ + kb), lda, 
             &B(k_,k_ + kb), ldb, ONE, &A(k_ + kb,k_ + kb), 
             lda );
            zhemm( 'R'/*Right*/, uplo, n - k - kb + 1, 
             kb, -(HALF), &A(k_,k_), lda, &B(k_,k_ + kb), 
             ldb, CONE, &A(k_,k_ + kb), lda );
            ztrsm( 'L'/*Left*/, uplo, 'N'/*No transpose*/
             , 'N'/*Non-unit*/, n - k - kb + 1, kb, CONE, 
             &B(k_ + kb,k_ + kb), ldb, &A(k_,k_ + kb), 
             lda );
          }
        }
      }
    }
    else { 
      if( upper ) { 
        
        //              Compute U*A*U'
        
        for( k = 1, k_ = k - 1, _do4=docnt(k,n,_do5 = nb); _do4 > 0; k += _do5, k_ += _do5, _do4-- ) { 
          kb = min( n - k + 1, nb );
          
          //                 Update the upper triangle of A(1:k+kb-1,1:k+kb-1)
          
          ztrmm( 'L'/*Left*/, uplo, 'N'/*No transpose*/
           , 'N'/*Non-unit*/, k - 1, kb, CONE, b, ldb, 
           &A(k_,0), lda );
          zhemm( 'R'/*Right*/, uplo, k - 1, kb, HALF, &A(k_,k_), 
           lda, &B(k_,0), ldb, CONE, &A(k_,0), lda );
          zher2k( uplo, 'N'/*No transpose*/, k - 1, kb, 
           CONE, &A(k_,0), lda, &B(k_,0), ldb, ONE, a, lda );
          zhemm( 'R'/*Right*/, uplo, k - 1, kb, HALF, &A(k_,k_), 
           lda, &B(k_,0), ldb, CONE, &A(k_,0), lda );
          ztrmm( 'R'/*Right*/, uplo, 'C'/*Conjugate transpose*/
           , 'N'/*Non-unit*/, k - 1, kb, CONE, &B(k_,k_), 
           ldb, &A(k_,0), lda );
          zhegs2( itype, uplo, kb, &A(k_,k_), lda, &B(k_,k_), 
           ldb, info );
        }
      }
      else { 
        
        //              Compute L'*A*L
        
        for( k = 1, k_ = k - 1, _do6=docnt(k,n,_do7 = nb); _do6 > 0; k += _do7, k_ += _do7, _do6-- ) { 
          kb = min( n - k + 1, nb );
          
          //                 Update the lower triangle of A(1:k+kb-1,1:k+kb-1)
          
          ztrmm( 'R'/*Right*/, uplo, 'N'/*No transpose*/
           , 'N'/*Non-unit*/, kb, k - 1, CONE, b, ldb, 
           &A(0,k_), lda );
          zhemm( 'L'/*Left*/, uplo, kb, k - 1, HALF, &A(k_,k_), 
           lda, &B(0,k_), ldb, CONE, &A(0,k_), lda );
          zher2k( uplo, 'C'/*Conjugate transpose*/, k - 
           1, kb, CONE, &A(0,k_), lda, &B(0,k_), ldb, ONE, 
           a, lda );
          zhemm( 'L'/*Left*/, uplo, kb, k - 1, HALF, &A(k_,k_), 
           lda, &B(0,k_), ldb, CONE, &A(0,k_), lda );
          ztrmm( 'L'/*Left*/, uplo, 'C'/*Conjugate transpose*/
           , 'N'/*Non-unit*/, kb, k - 1, CONE, &B(k_,k_), 
           ldb, &A(0,k_), lda );
          zhegs2( itype, uplo, kb, &A(k_,k_), lda, &B(k_,k_), 
           ldb, info );
        }
      }
    }
  }
  return;
  
  //     End of ZHEGST
  
#undef  B
#undef  A
} // end of function 


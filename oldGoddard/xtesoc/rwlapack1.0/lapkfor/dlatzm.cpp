/*
 * C++ implementation of lapack routine dlatzm
 *
 * $Id: dlatzm.cpp,v 1.5 1993/04/06 20:41:33 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:36:30
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dlatzm.cpp,v $
 * Revision 1.5  1993/04/06  20:41:33  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.4  1993/03/19  18:41:23  alv
 * now passes chars explicitly, rather than indirection of a string, to shut up SUN warnings
 *
 * Revision 1.3  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.2  1993/03/05  23:16:05  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:07:51  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ONE = 1.0e0;
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dlatzm(const char &side, const long &m, const long &n, double v[], 
 const long &incv, const double &tau, double *c1, double *c2, const long &ldc, 
 double work[])
{
#define C1(I_,J_) (*(c1+(I_)*(ldc)+(J_)))
#define C2(I_,J_) (*(c2+(I_)*(ldc)+(J_)))

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DLATZM applies a Householder matrix generated by DTZRQF to a matrix.
  
  //  Let P = I - tau*u*u',   u = ( 1 ),
  //                              ( v )
  //  where v is an (m-1) vector if SIDE = 'L', or a (n-1) vector if
  //  SIDE = 'R'.
  
  //  If SIDE equals 'L', let
  //         C = [ C1 ] 1
  //             [ C2 ] m-1
  //               n
  //  Then C is overwritten by P*C.
  
  //  If SIDE equals 'R', let
  //         C = [ C1, C2 ] m
  //                1  n-1
  //  Then C is overwritten by C*P.
  
  //  Arguments
  //  =========
  
  //  SIDE    (input) CHARACTER*1
  //          = 'L': form P * C
  //          = 'R': form C * P
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix C.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix C.
  
  //  V       (input) DOUBLE PRECISION array, dimension
  //                  (1 + (M-1)*abs(INCV)) if SIDE = 'L'
  //                  (1 + (N-1)*abs(INCV)) if SIDE = 'R'
  //          The vector v in the representation of P. V is not used
  //          if TAU = 0.
  
  //  INCV    (input) INTEGER
  //          The increment between elements of v. INCV <> 0
  
  //  TAU     (input) DOUBLE PRECISION
  //          The value tau in the representation of P.
  
  //  C1      (input/output) DOUBLE PRECISION array, dimension
  //                         (LDC,N) if SIDE = 'L'
  //                         (M,1)   if SIDE = 'R'
  //          On entry, the n-vector C1 if SIDE = 'L', or the m-vector C1
  //          if SIDE = 'R'.
  
  //          On exit, the first row of P*C if SIDE = 'L', or the first
  //          column of C*P if SIDE = 'R'.
  
  //  C2      (input/output) DOUBLE PRECISION array, dimension
  //                         (LDC, N)   if SIDE = 'L'
  //                         (LDC, N-1) if SIDE = 'R'
  //          On entry, the (m - 1) x n matrix C2 if SIDE = 'L', or the
  //          m x (n - 1) matrix C2 if SIDE = 'R'.
  
  //          On exit, rows 2:m of P*C if SIDE = 'L', or columns 2:m of C*P
  //          if SIDE = 'R'.
  
  //  LDC     (input) INTEGER
  //          The leading dimension of the arrays C1 and C2. LDC >= (1,M).
  
  //  WORK    (workspace) DOUBLE PRECISION array, dimension
  //                      (N) if SIDE = 'L'
  //                      (M) if SIDE = 'R'
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  if( (min( m, n ) == 0) || (tau == ZERO) ) 
    return;
  
  if( lsame( side, 'L' ) ) { 
    
    //        w := C1 + v' * C2
    
    dcopy( n, c1, ldc, work, 1 );
    dgemv( 'T'/* Transpose */, m - 1, n, ONE, c2, ldc, v, incv, 
     ONE, work, 1 );
    
    //        [ C1 ] := [ C1 ] - tau* [ 1 ] * w'
    //        [ C2 ]    [ C2 ]        [ v ]
    
    daxpy( n, -tau, work, 1, c1, ldc );
    dger( m - 1, n, -tau, v, incv, work, 1, c2, ldc );
    
  }
  else if( lsame( side, 'R' ) ) { 
    
    //        w := C1 + C2 * v
    
    dcopy( m, c1, 1, work, 1 );
    dgemv( 'N'/* No transpose */, m, n - 1, ONE, c2, ldc, v, incv, 
     ONE, work, 1 );
    
    //        [ C1, C2 ] := [ C1, C2 ] - tau* w * [ 1 , v']
    
    daxpy( m, -tau, work, 1, c1, 1 );
    dger( m, n - 1, -tau, work, 1, v, incv, c2, ldc );
  }
  
  return;
  
  //     End of DLATZM
  
#undef  C2
#undef  C1
} // end of function 


/*
 * C++ implementation of Lapack routine zlarfx
 *
 * $Id: zlarfx.cpp,v 1.5 1993/07/27 15:23:02 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:49:05
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zlarfx.cpp,v $
 * Revision 1.5  1993/07/27  15:23:02  alv
 * further simplified to compile with Borland optimizer on
 *
 * Revision 1.4  1993/07/21  22:18:17  alv
 * simplification now applied to all MSDOS, not just Borland
 *
 * Revision 1.3  1993/07/06  20:15:34  alv
 * simplified so that MS-DOS Borland compiler could digest
 *
 * Revision 1.2  1993/06/29  23:12:18  alv
 * Reworked to cut Borland warnings down to the point where it would compile
 *
// Revision 1.1  1993/06/24  22:47:05  alv
// Initial revision
//
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const DComplex ZERO = DComplex(0.0e0);
const DComplex ONE = DComplex(1.0e0);
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zlarfx(const char &side, const long &m, const long &n, DComplex v[], 
 const DComplex &taucopy, DComplex *c, const long &ldc, DComplex work[])
{
  DComplex tau = taucopy;   // Using tau as a const DComplex& parameter generates too many warnings with bcc due to bad Borland complex implementation
#define C(I_,J_)  (*(c+(I_)*(ldc)+(J_)))
  long _do0, _do1, _do10, _do11, _do12, _do13, _do14, _do15, 
   _do16, _do17, _do18, _do19, _do2, _do3, _do4, _do5, _do6, _do7, 
   _do8, _do9, j, j_;
  DComplex sum, t1, t10, t2, t3, t4, t5, t6, t7, t8, t9, v1, v10, 
   v2, v3, v4, v5, v6, v7, v8, v9;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZLARFX applies a DComplex elementary reflector H to a DComplex m by n
  //  matrix C, from either the left or the right. H is represented in the
  //  form
  
  //        H = I - tau * v * v'
  
  //  where tau is a DComplex scalar and v is a DComplex vector.
  
  //  If tau = 0, then H is taken to be the unit matrix
  
  //  This version uses inline code if H has order < 11.
  
  //  Arguments
  //  =========
  
  //  SIDE    (input) CHARACTER*1
  //          = 'L': form  H * C
  //          = 'R': form  C * H
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix C.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix C.
  
  //  V       (input) COMPLEX*16 array, dimension (M) if SIDE = 'L'
  //                                        or (N) if SIDE = 'R'
  //          The vector v in the representation of H.
  
  //  TAU     (input) COMPLEX*16
  //          The value tau in the representation of H.
  
  //  C       (input/output) COMPLEX*16 array, dimension (LDC,N)
  //          On entry, the m by n matrix C.
  //          On exit, C is overwritten by the matrix H * C if SIDE = 'L',
  //          or C * H if SIDE = 'R'.
  
  //  LDC     (input) INTEGER
  //          The leading dimension of the array C. LDA >= max(1,M).
  
  //  WORK    (workspace) COMPLEX*16 array, dimension (N) if SIDE = 'L'
  //                                            or (M) if SIDE = 'R'
  //          WORK is not referenced if H has order < 11.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  if( ctocf(tau) == ctocf(ZERO) ) 
    return;
  if( lsame( side, 'L' ) ) { 
    
    //        Form  H * C, where H has order m.
    
    switch( m ) { 
      case 1: goto L_10;
      case 2: goto L_30;
      case 3: goto L_50;
#if !defined(__MSDOS__)
      case 4: goto L_70;
      case 5: goto L_90;
      case 6: goto L_110;
      case 7: goto L_130;
      case 8: goto L_150;
      case 9: goto L_170;
      case 10: goto L_190;
#endif
    }
    
    //        Code for general M
    
    //        w := C'*v
    
    zgemv( 'C'/*Conjugate transpose*/, m, n, ONE, c, ldc, v, 
     1, ZERO, work, 1 );
    
    //        C := C - tau * v * w'
    
    zgerc( m, n, -(tau), v, 1, work, 1, c, ldc );
    goto L_410;
L_10:
    ;
    
    //        Special code for 1 x 1 Householder
    
    t1 = ONE - tau*v[0]*conj( v[0] );
    for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
      C(j_,0) = t1*C(j_,0);
    }
    goto L_410;
L_30:
    ;
    
    //        Special code for 2 x 2 Householder
    
    v1 = conj( v[0] );
    t1 = tau*conj( v1 );
    v2 = conj( v[1] );
    t2 = tau*conj( v2 );
    for( j = 1, j_ = j - 1, _do1 = n; j <= _do1; j++, j_++ ) { 
      sum = v1*C(j_,0) + v2*C(j_,1);
      C(j_,0) = C(j_,0) - sum*t1;
      C(j_,1) = C(j_,1) - sum*t2;
    }
    goto L_410;
L_50:
    ;
    
    //        Special code for 3 x 3 Householder
    
    v1 = conj( v[0] );
    t1 = tau*conj( v1 );
    v2 = conj( v[1] );
    t2 = tau*conj( v2 );
    v3 = conj( v[2] );
    t3 = tau*conj( v3 );
    for( j = 1, j_ = j - 1, _do2 = n; j <= _do2; j++, j_++ ) { 
      sum = v1*C(j_,0) + v2*C(j_,1) + v3*C(j_,2);
      C(j_,0) = C(j_,0) - sum*t1;
      C(j_,1) = C(j_,1) - sum*t2;
      C(j_,2) = C(j_,2) - sum*t3;
    }
    goto L_410;
#if !defined(__MSDOS__)
L_70:
    ;
    
    //        Special code for 4 x 4 Householder
    
    v1 = conj( v[0] );
    t1 = tau*conj( v1 );
    v2 = conj( v[1] );
    t2 = tau*conj( v2 );
    v3 = conj( v[2] );
    t3 = tau*conj( v3 );
    v4 = conj( v[3] );
    t4 = tau*conj( v4 );
    for( j = 1, j_ = j - 1, _do3 = n; j <= _do3; j++, j_++ ) { 
      sum = v1*C(j_,0) + v2*C(j_,1) + v3*C(j_,2) + v4*C(j_,3);
      C(j_,0) = C(j_,0) - sum*t1;
      C(j_,1) = C(j_,1) - sum*t2;
      C(j_,2) = C(j_,2) - sum*t3;
      C(j_,3) = C(j_,3) - sum*t4;
    }
    goto L_410;
L_90:
    ;
    
    //        Special code for 5 x 5 Householder
    
    v1 = conj( v[0] );
    t1 = tau*conj( v1 );
    v2 = conj( v[1] );
    t2 = tau*conj( v2 );
    v3 = conj( v[2] );
    t3 = tau*conj( v3 );
    v4 = conj( v[3] );
    t4 = tau*conj( v4 );
    v5 = conj( v[4] );
    t5 = tau*conj( v5 );
    for( j = 1, j_ = j - 1, _do4 = n; j <= _do4; j++, j_++ ) { 
      sum = v1*C(j_,0) + v2*C(j_,1) + v3*C(j_,2) + v4*C(j_,3) + 
       v5*C(j_,4);
      C(j_,0) = C(j_,0) - sum*t1;
      C(j_,1) = C(j_,1) - sum*t2;
      C(j_,2) = C(j_,2) - sum*t3;
      C(j_,3) = C(j_,3) - sum*t4;
      C(j_,4) = C(j_,4) - sum*t5;
    }
    goto L_410;
L_110:
    ;
    
    //        Special code for 6 x 6 Householder
    
    v1 = conj( v[0] );
    t1 = tau*conj( v1 );
    v2 = conj( v[1] );
    t2 = tau*conj( v2 );
    v3 = conj( v[2] );
    t3 = tau*conj( v3 );
    v4 = conj( v[3] );
    t4 = tau*conj( v4 );
    v5 = conj( v[4] );
    t5 = tau*conj( v5 );
    v6 = conj( v[5] );
    t6 = tau*conj( v6 );
    for( j = 1, j_ = j - 1, _do5 = n; j <= _do5; j++, j_++ ) { 
      sum = v1*C(j_,0) + v2*C(j_,1) + v3*C(j_,2) + v4*C(j_,3) + 
       v5*C(j_,4) + v6*C(j_,5);
      C(j_,0) = C(j_,0) - sum*t1;
      C(j_,1) = C(j_,1) - sum*t2;
      C(j_,2) = C(j_,2) - sum*t3;
      C(j_,3) = C(j_,3) - sum*t4;
      C(j_,4) = C(j_,4) - sum*t5;
      C(j_,5) = C(j_,5) - sum*t6;
    }
    goto L_410;
L_130:
    ;
    
    //        Special code for 7 x 7 Householder
    
    v1 = conj( v[0] );
    t1 = tau*conj( v1 );
    v2 = conj( v[1] );
    t2 = tau*conj( v2 );
    v3 = conj( v[2] );
    t3 = tau*conj( v3 );
    v4 = conj( v[3] );
    t4 = tau*conj( v4 );
    v5 = conj( v[4] );
    t5 = tau*conj( v5 );
    v6 = conj( v[5] );
    t6 = tau*conj( v6 );
    v7 = conj( v[6] );
    t7 = tau*conj( v7 );
    for( j = 1, j_ = j - 1, _do6 = n; j <= _do6; j++, j_++ ) { 
      sum = v1*C(j_,0) + v2*C(j_,1) + v3*C(j_,2) + v4*C(j_,3) + 
       v5*C(j_,4) + v6*C(j_,5) + v7*C(j_,6);
      C(j_,0) = C(j_,0) - sum*t1;
      C(j_,1) = C(j_,1) - sum*t2;
      C(j_,2) = C(j_,2) - sum*t3;
      C(j_,3) = C(j_,3) - sum*t4;
      C(j_,4) = C(j_,4) - sum*t5;
      C(j_,5) = C(j_,5) - sum*t6;
      C(j_,6) = C(j_,6) - sum*t7;
    }
    goto L_410;
L_150:
    ;
    
    //        Special code for 8 x 8 Householder
    
    v1 = conj( v[0] );
    t1 = tau*conj( v1 );
    v2 = conj( v[1] );
    t2 = tau*conj( v2 );
    v3 = conj( v[2] );
    t3 = tau*conj( v3 );
    v4 = conj( v[3] );
    t4 = tau*conj( v4 );
    v5 = conj( v[4] );
    t5 = tau*conj( v5 );
    v6 = conj( v[5] );
    t6 = tau*conj( v6 );
    v7 = conj( v[6] );
    t7 = tau*conj( v7 );
    v8 = conj( v[7] );
    t8 = tau*conj( v8 );
    for( j = 1, j_ = j - 1, _do7 = n; j <= _do7; j++, j_++ ) { 
      sum = v1*C(j_,0) + v2*C(j_,1) + v3*C(j_,2) + v4*C(j_,3) + 
       v5*C(j_,4) + v6*C(j_,5) + v7*C(j_,6) + v8*C(j_,7);
      C(j_,0) = C(j_,0) - sum*t1;
      C(j_,1) = C(j_,1) - sum*t2;
      C(j_,2) = C(j_,2) - sum*t3;
      C(j_,3) = C(j_,3) - sum*t4;
      C(j_,4) = C(j_,4) - sum*t5;
      C(j_,5) = C(j_,5) - sum*t6;
      C(j_,6) = C(j_,6) - sum*t7;
      C(j_,7) = C(j_,7) - sum*t8;
    }
    goto L_410;
L_170:
    ;
    
    //        Special code for 9 x 9 Householder
    
    v1 = conj( v[0] );
    t1 = tau*conj( v1 );
    v2 = conj( v[1] );
    t2 = tau*conj( v2 );
    v3 = conj( v[2] );
    t3 = tau*conj( v3 );
    v4 = conj( v[3] );
    t4 = tau*conj( v4 );
    v5 = conj( v[4] );
    t5 = tau*conj( v5 );
    v6 = conj( v[5] );
    t6 = tau*conj( v6 );
    v7 = conj( v[6] );
    t7 = tau*conj( v7 );
    v8 = conj( v[7] );
    t8 = tau*conj( v8 );
    v9 = conj( v[8] );
    t9 = tau*conj( v9 );
    for( j = 1, j_ = j - 1, _do8 = n; j <= _do8; j++, j_++ ) { 
      sum = v1*C(j_,0) + v2*C(j_,1) + v3*C(j_,2) + v4*C(j_,3) + 
       v5*C(j_,4) + v6*C(j_,5) + v7*C(j_,6) + v8*C(j_,7) + v9*
       C(j_,8);
      C(j_,0) = C(j_,0) - sum*t1;
      C(j_,1) = C(j_,1) - sum*t2;
      C(j_,2) = C(j_,2) - sum*t3;
      C(j_,3) = C(j_,3) - sum*t4;
      C(j_,4) = C(j_,4) - sum*t5;
      C(j_,5) = C(j_,5) - sum*t6;
      C(j_,6) = C(j_,6) - sum*t7;
      C(j_,7) = C(j_,7) - sum*t8;
      C(j_,8) = C(j_,8) - sum*t9;
    }
    goto L_410;
L_190:
    ;
    
    //        Special code for 10 x 10 Householder
    
    v1 = conj( v[0] );
    t1 = tau*conj( v1 );
    v2 = conj( v[1] );
    t2 = tau*conj( v2 );
    v3 = conj( v[2] );
    t3 = tau*conj( v3 );
    v4 = conj( v[3] );
    t4 = tau*conj( v4 );
    v5 = conj( v[4] );
    t5 = tau*conj( v5 );
    v6 = conj( v[5] );
    t6 = tau*conj( v6 );
    v7 = conj( v[6] );
    t7 = tau*conj( v7 );
    v8 = conj( v[7] );
    t8 = tau*conj( v8 );
    v9 = conj( v[8] );
    t9 = tau*conj( v9 );
    v10 = conj( v[9] );
    t10 = tau*conj( v10 );
    for( j = 1, j_ = j - 1, _do9 = n; j <= _do9; j++, j_++ ) { 
      sum = v1*C(j_,0) + v2*C(j_,1) + v3*C(j_,2) + v4*C(j_,3) + 
       v5*C(j_,4) + v6*C(j_,5) + v7*C(j_,6) + v8*C(j_,7) + v9*
       C(j_,8) + v10*C(j_,9);
      C(j_,0) = C(j_,0) - sum*t1;
      C(j_,1) = C(j_,1) - sum*t2;
      C(j_,2) = C(j_,2) - sum*t3;
      C(j_,3) = C(j_,3) - sum*t4;
      C(j_,4) = C(j_,4) - sum*t5;
      C(j_,5) = C(j_,5) - sum*t6;
      C(j_,6) = C(j_,6) - sum*t7;
      C(j_,7) = C(j_,7) - sum*t8;
      C(j_,8) = C(j_,8) - sum*t9;
      C(j_,9) = C(j_,9) - sum*t10;
    }
    goto L_410;
#endif
  }
  else { 
    
    //        Form  C * H, where H has order n.
    
    switch( n ) { 
      case 1: goto L_210;
      case 2: goto L_230;
      case 3: goto L_250;
#if !defined(__MSDOS__)
      case 4: goto L_270;
      case 5: goto L_290;
      case 6: goto L_310;
      case 7: goto L_330;
      case 8: goto L_350;
      case 9: goto L_370;
      case 10: goto L_390;
#endif
    }
    
    //        Code for general N
    
    //        w := C * v
    
    zgemv( 'N'/*No transpose*/, m, n, ONE, c, ldc, v, 1, ZERO, 
     work, 1 );
    
    //        C := C - tau * w * v'
    
    zgerc( m, n, -(tau), work, 1, v, 1, c, ldc );
    goto L_410;
L_210:
    ;
    
    //        Special code for 1 x 1 Householder
    
    t1 = ONE - tau*v[0]*conj( v[0] );
    for( j = 1, j_ = j - 1, _do10 = m; j <= _do10; j++, j_++ ) { 
      C(0,j_) = t1*C(0,j_);
    }
    goto L_410;
L_230:
    ;
    
    //        Special code for 2 x 2 Householder
    
    v1 = v[0];
    t1 = tau*conj( v1 );
    v2 = v[1];
    t2 = tau*conj( v2 );
    for( j = 1, j_ = j - 1, _do11 = m; j <= _do11; j++, j_++ ) { 
      sum = v1*C(0,j_) + v2*C(1,j_);
      C(0,j_) = C(0,j_) - sum*t1;
      C(1,j_) = C(1,j_) - sum*t2;
    }
    goto L_410;
L_250:
    ;
    
    //        Special code for 3 x 3 Householder
    
    v1 = v[0];
    t1 = tau*conj( v1 );
    v2 = v[1];
    t2 = tau*conj( v2 );
    v3 = v[2];
    t3 = tau*conj( v3 );
    for( j = 1, j_ = j - 1, _do12 = m; j <= _do12; j++, j_++ ) { 
      sum = v1*C(0,j_) + v2*C(1,j_) + v3*C(2,j_);
      C(0,j_) = C(0,j_) - sum*t1;
      C(1,j_) = C(1,j_) - sum*t2;
      C(2,j_) = C(2,j_) - sum*t3;
    }
    goto L_410;
#if !defined(__MSDOS__)
L_270:
    ;
    
    //        Special code for 4 x 4 Householder
    
    v1 = v[0];
    t1 = tau*conj( v1 );
    v2 = v[1];
    t2 = tau*conj( v2 );
    v3 = v[2];
    t3 = tau*conj( v3 );
    v4 = v[3];
    t4 = tau*conj( v4 );
    for( j = 1, j_ = j - 1, _do13 = m; j <= _do13; j++, j_++ ) { 
      sum = v1*C(0,j_) + v2*C(1,j_) + v3*C(2,j_) + v4*C(3,j_);
      C(0,j_) = C(0,j_) - sum*t1;
      C(1,j_) = C(1,j_) - sum*t2;
      C(2,j_) = C(2,j_) - sum*t3;
      C(3,j_) = C(3,j_) - sum*t4;
    }
    goto L_410;
L_290:
    ;
    
    //        Special code for 5 x 5 Householder
    
    v1 = v[0];
    t1 = tau*conj( v1 );
    v2 = v[1];
    t2 = tau*conj( v2 );
    v3 = v[2];
    t3 = tau*conj( v3 );
    v4 = v[3];
    t4 = tau*conj( v4 );
    v5 = v[4];
    t5 = tau*conj( v5 );
    for( j = 1, j_ = j - 1, _do14 = m; j <= _do14; j++, j_++ ) { 
      sum = v1*C(0,j_) + v2*C(1,j_) + v3*C(2,j_) + v4*C(3,j_) + 
       v5*C(4,j_);
      C(0,j_) = C(0,j_) - sum*t1;
      C(1,j_) = C(1,j_) - sum*t2;
      C(2,j_) = C(2,j_) - sum*t3;
      C(3,j_) = C(3,j_) - sum*t4;
      C(4,j_) = C(4,j_) - sum*t5;
    }
    goto L_410;
L_310:
    ;
    
    //        Special code for 6 x 6 Householder
    
    v1 = v[0];
    t1 = tau*conj( v1 );
    v2 = v[1];
    t2 = tau*conj( v2 );
    v3 = v[2];
    t3 = tau*conj( v3 );
    v4 = v[3];
    t4 = tau*conj( v4 );
    v5 = v[4];
    t5 = tau*conj( v5 );
    v6 = v[5];
    t6 = tau*conj( v6 );
    for( j = 1, j_ = j - 1, _do15 = m; j <= _do15; j++, j_++ ) { 
      sum = v1*C(0,j_) + v2*C(1,j_) + v3*C(2,j_) + v4*C(3,j_) + 
       v5*C(4,j_) + v6*C(5,j_);
      C(0,j_) = C(0,j_) - sum*t1;
      C(1,j_) = C(1,j_) - sum*t2;
      C(2,j_) = C(2,j_) - sum*t3;
      C(3,j_) = C(3,j_) - sum*t4;
      C(4,j_) = C(4,j_) - sum*t5;
      C(5,j_) = C(5,j_) - sum*t6;
    }
    goto L_410;
L_330:
    ;
    
    //        Special code for 7 x 7 Householder
    
    v1 = v[0];
    t1 = tau*conj( v1 );
    v2 = v[1];
    t2 = tau*conj( v2 );
    v3 = v[2];
    t3 = tau*conj( v3 );
    v4 = v[3];
    t4 = tau*conj( v4 );
    v5 = v[4];
    t5 = tau*conj( v5 );
    v6 = v[5];
    t6 = tau*conj( v6 );
    v7 = v[6];
    t7 = tau*conj( v7 );
    for( j = 1, j_ = j - 1, _do16 = m; j <= _do16; j++, j_++ ) { 
      sum = v1*C(0,j_) + v2*C(1,j_) + v3*C(2,j_) + v4*C(3,j_) + 
       v5*C(4,j_) + v6*C(5,j_) + v7*C(6,j_);
      C(0,j_) = C(0,j_) - sum*t1;
      C(1,j_) = C(1,j_) - sum*t2;
      C(2,j_) = C(2,j_) - sum*t3;
      C(3,j_) = C(3,j_) - sum*t4;
      C(4,j_) = C(4,j_) - sum*t5;
      C(5,j_) = C(5,j_) - sum*t6;
      C(6,j_) = C(6,j_) - sum*t7;
    }
    goto L_410;
L_350:
    ;
    
    //        Special code for 8 x 8 Householder
    
    v1 = v[0];
    t1 = tau*conj( v1 );
    v2 = v[1];
    t2 = tau*conj( v2 );
    v3 = v[2];
    t3 = tau*conj( v3 );
    v4 = v[3];
    t4 = tau*conj( v4 );
    v5 = v[4];
    t5 = tau*conj( v5 );
    v6 = v[5];
    t6 = tau*conj( v6 );
    v7 = v[6];
    t7 = tau*conj( v7 );
    v8 = v[7];
    t8 = tau*conj( v8 );
    for( j = 1, j_ = j - 1, _do17 = m; j <= _do17; j++, j_++ ) { 
      sum = v1*C(0,j_) + v2*C(1,j_) + v3*C(2,j_) + v4*C(3,j_) + 
       v5*C(4,j_) + v6*C(5,j_) + v7*C(6,j_) + v8*C(7,j_);
      C(0,j_) = C(0,j_) - sum*t1;
      C(1,j_) = C(1,j_) - sum*t2;
      C(2,j_) = C(2,j_) - sum*t3;
      C(3,j_) = C(3,j_) - sum*t4;
      C(4,j_) = C(4,j_) - sum*t5;
      C(5,j_) = C(5,j_) - sum*t6;
      C(6,j_) = C(6,j_) - sum*t7;
      C(7,j_) = C(7,j_) - sum*t8;
    }
    goto L_410;
L_370:
    ;
    
    //        Special code for 9 x 9 Householder
    
    v1 = v[0];
    t1 = tau*conj( v1 );
    v2 = v[1];
    t2 = tau*conj( v2 );
    v3 = v[2];
    t3 = tau*conj( v3 );
    v4 = v[3];
    t4 = tau*conj( v4 );
    v5 = v[4];
    t5 = tau*conj( v5 );
    v6 = v[5];
    t6 = tau*conj( v6 );
    v7 = v[6];
    t7 = tau*conj( v7 );
    v8 = v[7];
    t8 = tau*conj( v8 );
    v9 = v[8];
    t9 = tau*conj( v9 );
    for( j = 1, j_ = j - 1, _do18 = m; j <= _do18; j++, j_++ ) { 
      sum = v1*C(0,j_) + v2*C(1,j_) + v3*C(2,j_) + v4*C(3,j_) + 
       v5*C(4,j_) + v6*C(5,j_) + v7*C(6,j_) + v8*C(7,j_) + v9*
       C(8,j_);
      C(0,j_) = C(0,j_) - sum*t1;
      C(1,j_) = C(1,j_) - sum*t2;
      C(2,j_) = C(2,j_) - sum*t3;
      C(3,j_) = C(3,j_) - sum*t4;
      C(4,j_) = C(4,j_) - sum*t5;
      C(5,j_) = C(5,j_) - sum*t6;
      C(6,j_) = C(6,j_) - sum*t7;
      C(7,j_) = C(7,j_) - sum*t8;
      C(8,j_) = C(8,j_) - sum*t9;
    }
    goto L_410;
L_390:
    ;
    
    //        Special code for 10 x 10 Householder
    
    v1 = v[0];
    t1 = tau*conj( v1 );
    v2 = v[1];
    t2 = tau*conj( v2 );
    v3 = v[2];
    t3 = tau*conj( v3 );
    v4 = v[3];
    t4 = tau*conj( v4 );
    v5 = v[4];
    t5 = tau*conj( v5 );
    v6 = v[5];
    t6 = tau*conj( v6 );
    v7 = v[6];
    t7 = tau*conj( v7 );
    v8 = v[7];
    t8 = tau*conj( v8 );
    v9 = v[8];
    t9 = tau*conj( v9 );
    v10 = v[9];
    t10 = tau*conj( v10 );
    for( j = 1, j_ = j - 1, _do19 = m; j <= _do19; j++, j_++ ) { 
      sum = v1*C(0,j_) + v2*C(1,j_) + v3*C(2,j_) + v4*C(3,j_) + 
       v5*C(4,j_) + v6*C(5,j_) + v7*C(6,j_) + v8*C(7,j_) + v9*
       C(8,j_) + v10*C(9,j_);
      C(0,j_) = C(0,j_) - sum*t1;
      C(1,j_) = C(1,j_) - sum*t2;
      C(2,j_) = C(2,j_) - sum*t3;
      C(3,j_) = C(3,j_) - sum*t4;
      C(4,j_) = C(4,j_) - sum*t5;
      C(5,j_) = C(5,j_) - sum*t6;
      C(6,j_) = C(6,j_) - sum*t7;
      C(7,j_) = C(7,j_) - sum*t8;
      C(8,j_) = C(8,j_) - sum*t9;
      C(9,j_) = C(9,j_) - sum*t10;
    }
    goto L_410;
#endif
  }
L_410:
  ;
  return;
  
  //     End of ZLARFX
  
#undef  C
} // end of function 


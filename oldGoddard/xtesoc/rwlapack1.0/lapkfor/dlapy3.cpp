/*
 * C++ implementation of lapack routine dlapy3
 *
 * $Id: dlapy3.cpp,v 1.4 1993/04/06 20:41:10 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:35:41
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dlapy3.cpp,v $
 * Revision 1.4  1993/04/06  20:41:10  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.3  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.2  1993/03/05  23:15:34  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:07:25  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL double /*FUNCTION*/ dlapy3(const double &x, const double &y, const double &z)
{
  double dlapy3_v, w, xabs, yabs, zabs;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DLAPY3 returns sqrt(x**2+y**2+z**2), taking care not to cause
  //  unnecessary overflow.
  
  //  Arguments
  //  =========
  
  //  X       (input) DOUBLE PRECISION
  //  Y       (input) DOUBLE PRECISION
  //  Z       (input) DOUBLE PRECISION
  //          X, Y and Z specify the values x, y and z.
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  xabs = abs( x );
  yabs = abs( y );
  zabs = abs( z );
  w = vmax( xabs, yabs, zabs, FEND );
  if( w == ZERO ) { 
    dlapy3_v = ZERO;
  }
  else { 
    dlapy3_v = w*sqrt( pow(xabs/w, 2) + pow(yabs/w, 2) + pow(zabs/
     w, 2) );
  }
  return( dlapy3_v );
  
  //     End of DLAPY3
  
} // end of function 


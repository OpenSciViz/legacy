/*
 * C++ implementation of Lapack routine ztrtri
 *
 * $Id: ztrtri.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:51:28
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: ztrtri.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const DComplex ONE = DComplex(1.0e0);
const DComplex ZERO = DComplex(0.0e0);
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ ztrtri(const char &uplo, const char &diag, const long &n, DComplex *a, 
 const long &lda, long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  char _c0[2], _c1[2];
  int nounit, upper;
  long _do0, _do1, _do2, _do3, _do4, info_, j, j_, jb, nb, nn;
#define NCHRTMPS 1
  CHRTMP _c[NCHRTMPS];
  ini_chrtmp(_c,NCHRTMPS);

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZTRTRI computes the inverse of a DComplex upper or lower triangular
  //  matrix A.
  
  //  This is the Level 3 BLAS version of the algorithm.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the matrix A is upper or lower triangular.
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  DIAG    (input) CHARACTER*1
  //          Specifies whether or not the matrix A is unit triangular.
  //          = 'N':  Non-unit triangular
  //          = 'U':  Unit triangular
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  A       (input/output) COMPLEX*16 array, dimension (LDA,N)
  
  //          On entry, the triangular matrix A.  If UPLO = 'U', the
  //          leading n by n upper triangular part of the array A contains
  //          the upper triangular matrix, and the strictly lower
  //          triangular part of A is not referenced.  If UPLO = 'L', the
  //          leading n by n lower triangular part of the array A contains
  //          the lower triangular matrix, and the strictly upper
  //          triangular part of A is not referenced.  If DIAG = 'U', the
  //          diagonal elements of A are also not referenced and are
  //          assumed to be 1.
  
  //          On exit, the (triangular) inverse of the original matrix, in
  //          the same storage format.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          > 0: if INFO = k, A(k,k) is exactly zero.  The triangular
  //               matrix is singular and its inverse can not be computed.
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  nounit = lsame( diag, 'N' );
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( !nounit && !lsame( diag, 'U' ) ) { 
    info = -2;
  }
  else if( n < 0 ) { 
    info = -3;
  }
  else if( lda < max( 1, n ) ) { 
    info = -5;
  }
  if( info != 0 ) { 
    xerbla( "ZTRTRI", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  //     Check for singularity if non-unit.
  
  if( nounit ) { 
    for( info = 1, info_ = info - 1, _do0 = n; info <= _do0; info++, info_++ ) { 
      if( ctocf(A(info_,info_)) == ctocf(ZERO) ) 
        return;
    }
    info = 0;
  }
  
  //     Determine the block size for this environment.
  
  nb = ilaenv( 1, "ZTRTRI", f_concat(&_c[0],STR1(_c0,uplo),STR1(_c1,diag),
   NULL), n, -1, -1, -1 );
  if( nb <= 1 || nb >= n ) { 
    
    //        Use unblocked code
    
    ztrti2( uplo, diag, n, a, lda, info );
  }
  else { 
    
    //        Use blocked code
    
    if( upper ) { 
      
      //           Compute inverse of upper triangular matrix
      
      for( j = 1, j_ = j - 1, _do1=docnt(j,n,_do2 = nb); _do1 > 0; j += _do2, j_ += _do2, _do1-- ) { 
        jb = min( nb, n - j + 1 );
        
        //              Compute rows 1:j-1 of current block column
        
        ztrmm( 'L'/*Left*/, 'U'/*Upper*/, 'N'/*No transpose*/
         , diag, j - 1, jb, ONE, a, lda, &A(j_,0), lda );
        ztrsm( 'R'/*Right*/, 'U'/*Upper*/, 'N'/*No transpose*/
         , diag, j - 1, jb, -(ONE), &A(j_,j_), lda, &A(j_,0), 
         lda );
        
        //              Compute inverse of current diagonal block
        
        ztrti2( 'U'/*Upper*/, diag, jb, &A(j_,j_), lda, info );
      }
    }
    else { 
      
      //           Compute inverse of lower triangular matrix
      
      nn = ((n - 1)/nb)*nb + 1;
      for( j = nn, j_ = j - 1, _do3=docnt(j,1,_do4 = -nb); _do3 > 0; j += _do4, j_ += _do4, _do3-- ) { 
        jb = min( nb, n - j + 1 );
        if( j + jb <= n ) { 
          
          //                 Compute rows j+jb:n of current block column
          
          ztrmm( 'L'/*Left*/, 'L'/*Lower*/, 'N'/*No transpose*/
           , diag, n - j - jb + 1, jb, ONE, &A(j_ + jb,j_ + jb), 
           lda, &A(j_,j_ + jb), lda );
          ztrsm( 'R'/*Right*/, 'L'/*Lower*/, 'N'/*No transpose*/
           , diag, n - j - jb + 1, jb, -(ONE), &A(j_,j_), 
           lda, &A(j_,j_ + jb), lda );
        }
        
        //              Compute inverse of current diagonal block
        
        ztrti2( 'L'/*Lower*/, diag, jb, &A(j_,j_), lda, info );
      }
    }
  }
  
  rel_chrtmp(_c,NCHRTMPS);
  return;
  
  //     End of ZTRTRI
  
#undef  NCHRTMPS
#undef  A
} // end of function 


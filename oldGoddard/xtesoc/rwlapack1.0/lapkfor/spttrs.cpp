/*
 * C++ implementation of Lapack routine spttrs
 *
 * $Id: spttrs.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:02:15
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: spttrs.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

RWLAPKDECL void /*FUNCTION*/ spttrs(const long &n, const long &nrhs, float d[], float e[], 
   float *b, const long &ldb, long &info)
{
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
  long _do0, _do1, i, i_, j, j_;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SPTTRS solves a tridiagonal system of the form
  //     A * X = B
  //  using the L*D*L' factorization of A computed by SPTTRF.  D is a
  //  diagonal matrix specified in the vector D, L is a unit bidiagonal
  //  matrix whose subdiagonal is specified in the vector E, and X and B
  //  are N by NRHS matrices.
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The order of the tridiagonal matrix A.  N >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right hand sides, i.e., the number of columns
  //          of the matrix B.  NRHS >= 0.
  
  //  D       (input) REAL array, dimension (N)
  //          The n diagonal elements of the diagonal matrix D from the
  //          L*D*L' factorization of A.
  
  //  E       (input) REAL array, dimension (N-1)
  //          The (n-1) subdiagonal elements of the unit bidiagonal factor
  //          L from the L*D*L' factorization of A.  E can also be regarded
  //          as the superdiagonal of the unit bidiagonal factor U from the
  //          factorization A = U'*D*U.
  
  //  B       (input/output) REAL array, dimension (LDB,NRHS)
  //          On entry, the right hand side vectors B for the system of
  //          linear equations.
  //          On exit, the solution vectors, X.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input arguments.
  
  info = 0;
  if( n < 0 ) { 
    info = -1;
  }
  else if( nrhs < 0 ) { 
    info = -2;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -6;
  }
  if( info != 0 ) { 
    xerbla( "SPTTRS", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  //     Solve A * X = B using the factorization A = L*D*L',
  //     overwriting each right hand side vector with its solution.
  
  for( j = 1, j_ = j - 1, _do0 = nrhs; j <= _do0; j++, j_++ ) { 
    
    //        Solve L * x = b.
    
    for( i = 2, i_ = i - 1, _do1 = n; i <= _do1; i++, i_++ ) { 
      B(j_,i_) = B(j_,i_) - B(j_,i_ - 1)*e[i_ - 1];
    }
    
    //        Solve D * L' * x = b.
    
    B(j_,n - 1) = B(j_,n - 1)/d[n - 1];
    for( i = n - 1, i_ = i - 1; i >= 1; i--, i_-- ) { 
      B(j_,i_) = B(j_,i_)/d[i_] - B(j_,i_ + 1)*e[i_];
    }
  }
  
  return;
  
  //     End of SPTTRS
  
#undef  B
} // end of function 


/*
 * C++ implementation of Lapack routine zlacgv
 *
 * $Id: zlacgv.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:48:08
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zlacgv.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

RWLAPKDECL void /*FUNCTION*/ zlacgv(const long &n, DComplex x[], const long &incx)
{
  long _do0, _do1, i, i_, ioff;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZLACGV conjugates a DComplex vector of length N.
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The length of the vector X.  N >= 0.
  
  //  X       (input/output) COMPLEX*16 array, dimension
  //                         (1+(N-1)*abs(INCX))
  //          On entry, the vector of length N to be conjugated.
  //          On exit, X is overwritten with conjg(X).
  
  //  INCX    (input) INTEGER
  //          The spacing between successive elements of X.
  
  
  //     .. Local Scalars ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  if( incx == 1 ) { 
    for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
      x[i_] = conj( x[i_] );
    }
  }
  else { 
    ioff = 1;
    if( incx < 0 ) 
      ioff = 1 - (n - 1)*incx;
    for( i = 1, i_ = i - 1, _do1 = n; i <= _do1; i++, i_++ ) { 
      x[ioff - 1] = conj( x[ioff - 1] );
      ioff = ioff + incx;
    }
  }
  return;
  
  //     End of ZLACGV
  
} // end of function 


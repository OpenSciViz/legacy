/*
 * C++ implementation of lapack routine dstev
 *
 * $Id: dstev.cpp,v 1.6 1993/04/06 20:42:26 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:38:22
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dstev.cpp,v $
 * Revision 1.6  1993/04/06  20:42:26  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.5  1993/03/19  18:41:23  alv
 * now passes chars explicitly, rather than indirection of a string, to shut up SUN warnings
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:17:18  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:09:08  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dstev(const char &jobz, const long &n, double d[], double e[], 
 double *z, const long &ldz, double work[], long &info)
{
#define Z(I_,J_)  (*(z+(I_)*(ldz)+(J_)))
  int wantz;
  long imax, imaxd, imaxe, iscale;
  double bignum, eps, rmax, rmin, safmin, sigma, smlnum, tnrm;

  
  //  -- LAPACK driver routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DSTEV  computes all eigenvalues and, optionally, eigenvectors of a
  //  real symmetric tridiagonal matrix by calling the recommended sequence
  //  of LAPACK routines.
  
  //  Arguments
  //  =========
  
  //  JOBZ    (input) CHARACTER*1
  //          Specifies whether or not to compute the eigenvectors:
  //          = 'N':  Compute eigenvalues only.
  //          = 'V':  Compute eigenvalues and eigenvectors.
  
  //  N       (input) INTEGER
  //          The number of rows and columns in the matrix.  N >= 0.
  
  //  D       (input/output) DOUBLE PRECISION array, dimension (N)
  //          On entry, D contains the diagonal elements of the
  //          tridiagonal matrix.
  //          On exit, if INFO = 0, D contains the eigenvalues in ascending
  //          order.  If INFO > 0, the eigenvalues are correct for indices
  //          1, 2, ..., INFO-1, but they are unordered and may not be the
  //          smallest eigenvalues of the matrix.
  
  //  E       (input/workspace) DOUBLE PRECISION array, dimension (N-1)
  //          On entry, E contains the subdiagonal elements of the
  //          tridiagonal matrix in positions 1 through N-1.
  //          On exit, the contents of E are destroyed.
  
  //  Z       (output) DOUBLE PRECISION array, dimension (LDZ, N)
  //          If JOBZ = 'V', then if INFO = 0 on exit, Z contains the
  //          orthonormal eigenvectors of the matrix.  If INFO > 0, Z
  //          contains the eigenvectors associated with only the stored
  //          eigenvalues.
  //          If JOBZ = 'N', then Z is not referenced.
  
  //  LDZ     (input) INTEGER
  //          The leading dimension of the array Z.  LDZ >= 1, and if
  //          JOBZ = 'V', LDZ >= max(1,N).
  
  //  WORK    (workspace) DOUBLE PRECISION array, dimension (max(1,2*N-2))
  //          If JOBZ = 'N', WORK is not referenced.
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit.
  //          < 0:  if INFO = -i, the i-th argument had an illegal value.
  //          > 0:  if INFO = +i, the algorithm terminated before finding
  //                the i-th eigenvalue.
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  wantz = lsame( jobz, 'V' );
  
  info = 0;
  if( !(wantz || lsame( jobz, 'N' )) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( ldz < 1 || (wantz && ldz < n) ) { 
    info = -6;
  }
  
  if( info != 0 ) { 
    xerbla( "DSTEV ", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  if( n == 1 ) { 
    if( wantz ) 
      Z(0,0) = ONE;
    return;
  }
  
  //     Get machine constants.
  
  safmin = dlamch( 'S'/* Safe minimum */ );
  eps = dlamch( 'P'/* Precision */ );
  smlnum = safmin/eps;
  bignum = ONE/smlnum;
  rmin = sqrt( smlnum );
  rmax = sqrt( bignum );
  
  //     Scale matrix to allowable range, if necessary.
  
  iscale = 0;
  imaxd = idamax( n, d, 1 );
  imaxe = idamax( n - 1, e, 1 );
  tnrm = max( abs( d[imaxd - 1] ), abs( e[imaxe - 1] ) );
  if( tnrm > ZERO && tnrm < rmin ) { 
    iscale = 1;
    sigma = rmin/tnrm;
  }
  else if( tnrm > rmax ) { 
    iscale = 1;
    sigma = rmax/tnrm;
  }
  if( iscale == 1 ) { 
    dscal( n, sigma, d, 1 );
    dscal( n - 1, sigma, &e[0], 1 );
  }
  
  //     For eigenvalues only, call DSTERF.  For eigenvalues and
  //     eigenvectors, call DSTEQR.
  
  if( !wantz ) { 
    dsterf( n, d, e, info );
  }
  else { 
    dsteqr( 'I', n, d, e, z, ldz, work, info );
  }
  
  //     If matrix was scaled, then rescale eigenvalues appropriately.
  
  if( iscale == 1 ) { 
    if( info == 0 ) { 
      imax = n;
    }
    else { 
      imax = info - 1;
    }
    dscal( imax, ONE/sigma, d, 1 );
  }
  
  return;
  
  //     End of DSTEV
  
#undef  Z
} // end of function 


/*
 * C++ implementation of Lapack routine stzrqf
 *
 * $Id: stzrqf.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:03:48
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: stzrqf.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ONE = 1.0e0;
const float ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ stzrqf(const long &m, const long &n, float *a, const long &lda, 
 float tau[], long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  long _do0, i, i_, k, k_, m1;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  STZRQF reduces the m by n ( m.le.n ) upper trapezoidal matrix A
  //  to upper triangular form by means of orthogonal transformations.
  
  //  The m by n ( m.le.n ) upper trapezoidal matrix A given by
  
  //     A = ( U  X ),
  
  //  where U is an m by m upper triangular matrix, is factorized as
  
  //     A = ( R  0 ) * P,
  
  //  where P is an n by n orthogonal matrix and R is an m by m upper
  //  triangular matrix.
  
  //  The  factorization is obtained by Householder's method.  The kth
  //  transformation matrix, P( k ), which is used to introduce zeros into
  //  the ( m - k + 1 )th row of A, is given in the form
  
  //     P( k ) = ( I     0   ),
  //              ( 0  T( k ) )
  
  //  where
  
  //     T( k ) = I - tau*u( k )*u( k )',   u( k ) = (   1    ),
  //                                                 (   0    )
  //                                                 ( z( k ) )
  
  //  tau is a scalar and z( k ) is an ( n - m ) element vector.
  //  tau and z( k ) are chosen to annihilate the elements of the kth row
  //  of X.
  
  //  The scalar tau is returned in the kth element of TAU and the vector
  //  u( k ) in the kth row of A, such that the elements of z( k ) are
  //  in  a( k, m + 1 ), ..., a( k, n ). The elements of R are returned in
  //  the upper triangular part of A.
  
  //  P is given by
  
  //     P =  P( 1 ) * P( 2 ) * ... * P( m ).
  
  //  Arguments
  //  =========
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix A.  M >= 0.
  //          If M = 0 then an immediate return is effected.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix A.  N >= M.
  
  //  A       (input/output) REAL array, dimension
  //                         (LDA,max(1,N))
  
  //          On entry, the leading M by N upper trapezoidal part of the
  //          array A must contain the matrix to be factorized.
  
  //          On exit, the M by M upper triangular part of A will contain
  //          the upper triangular matrix R and the remaining M by (N - M)
  //          upper trapezoidal part of A will contain details of the
  //          factorization as described above.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,M).
  
  //  TAU     (output) REAL array, dimension (max(1,M))
  //          On exit, TAU( k ) contains the scalar tau( k ) for the
  //          ( m - k + 1 )th transformation as described above.  If
  //          P( k ) = I  then TAU( k ) = 0.0.
  
  //  INFO    (output) INTEGER
  //          =    0: successful exit
  //          .lt. 0: INFO = -k, the k-th argument had an illegal value
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  if( m < 0 ) { 
    info = -1;
  }
  else if( n < m ) { 
    info = -2;
  }
  else if( lda < max( 1, m ) ) { 
    info = -4;
  }
  if( info != 0 ) { 
    xerbla( "STZRQF", -info );
    return;
  }
  
  //     Perform the factorization.
  
  if( m == 0 ) 
    return;
  if( m == n ) { 
    for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
      tau[i_] = ZERO;
    }
  }
  else { 
    m1 = min( m + 1, n );
    for( k = m, k_ = k - 1; k >= 1; k--, k_-- ) { 
      
      //           Use a Householder reflection to zero the kth row of A.
      //           First set up the reflection.
      
      slarfg( n - m + 1, A(k_,k_), &A(m1 - 1,k_), lda, tau[k_] );
      
      if( (tau[k_] != ZERO) && (k > 1) ) { 
        
        //              We now perform the operation  A := A*P( k ).
        
        //              Use the first ( k - 1 ) elements of TAU to store  a( k ),
        //              where  a( k ) consists of the first ( k - 1 ) elements of
        //              the  kth column  of  A.  Also  let  B  denote  the  first
        //              ( k - 1 ) rows of the last ( n - m ) columns of A.
        
        scopy( k - 1, &A(k_,0), 1, tau, 1 );
        
        //              Form   w = a( k ) + B*z( k )  in TAU.
        
        sgemv( 'N'/*No transpose*/, k - 1, n - m, ONE, &A(m1 - 1,0), 
         lda, &A(m1 - 1,k_), lda, ONE, tau, 1 );
        
        //              Now form  a( k ) := a( k ) - tau*w
        //              and       B      := B      - tau*w*z( k )'.
        
        saxpy( k - 1, -tau[k_], tau, 1, &A(k_,0), 1 );
        sger( k - 1, n - m, -tau[k_], tau, 1, &A(m1 - 1,k_), 
         lda, &A(m1 - 1,0), lda );
      }
    }
  }
  
  return;
  
  //     End of STZRQF
  
#undef  A
} // end of function 


/*
 * C++ implementation of Lapack routine scsum1
 *
 * $Id: scsum1.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:58:01
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: scsum1.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

RWLAPKDECL float /*FUNCTION*/ scsum1(const long &n, FComplex cx[], const long &incx)
{
  long _do0, _do1, _do2, i, i_, nincx;
  float scsum1_v, stemp;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SCSUM1 takes the sum of the absolute values of a DComplex
  //  vector and returns a single precision result.
  
  //  Based on SCASUM from the Level 1 BLAS.
  //  The change is to use the 'genuine' absolute value.
  
  //  Contributed by Nick Higham for use with CLACON.
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The number of elements in the vector CX.
  
  //  CX      (input) COMPLEX array, dimension (N)
  //          The vector whose elements will be summed.
  
  //  INCX    (input) INTEGER
  //          The spacing between successive values of CX.  INCX > 0.
  
  //     .. Local Scalars ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  scsum1_v = 0.0e0;
  stemp = 0.0e0;
  if( n <= 0 ) 
    return( scsum1_v );
  if( incx == 1 ) 
    goto L_20;
  
  //     CODE FOR INCREMENT NOT EQUAL TO 1
  
  nincx = n*incx;
  for( i = 1, i_ = i - 1, _do0=docnt(i,nincx,_do1 = incx); _do0 > 0; i += _do1, i_ += _do1, _do0-- ) { 
    
    //        NEXT LINE MODIFIED.
    
    stemp = stemp + abs( cx[i_] );
  }
  scsum1_v = stemp;
  return( scsum1_v );
  
  //     CODE FOR INCREMENT EQUAL TO 1
  
L_20:
  ;
  for( i = 1, i_ = i - 1, _do2 = n; i <= _do2; i++, i_++ ) { 
    
    //        NEXT LINE MODIFIED.
    
    stemp = stemp + abs( cx[i_] );
  }
  scsum1_v = stemp;
  return( scsum1_v );
  
  //     End of SCSUM1
  
} // end of function 


/*
 * C++ implementation of lapack routine dlangb
 *
 * $Id: dlangb.cpp,v 1.4 1993/04/06 20:41:02 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:35:23
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dlangb.cpp,v $
 * Revision 1.4  1993/04/06  20:41:02  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.3  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.2  1993/03/05  23:15:24  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:07:16  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ONE = 1.0e0;
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL double /*FUNCTION*/ dlangb(const char &norm, const long &n, const long &kl, 
 const long &ku, double *ab, const long &ldab, double work[])
{
#define AB(I_,J_) (*(ab+(I_)*(ldab)+(J_)))
  long _do0, _do1, _do2, _do3, _do4, _do5, _do6, _do7, _do8, 
   i, i_, j, j_, k, l;
  double dlangb_v, scale, sum, value;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DLANGB  returns the value of the one norm,  or the Frobenius norm, or
  //  the  infinity norm,  or the element of  largest absolute value  of an
  //  n by n band matrix  A,  with kl sub-diagonals and ku super-diagonals.
  
  //  Description
  //  ===========
  
  //  DLANGB returns the value
  
  //     DLANGB = ( max(abs(A(i,j))), NORM = 'M' or 'm'
  //              (
  //              ( norm1(A),         NORM = '1', 'O' or 'o'
  //              (
  //              ( normI(A),         NORM = 'I' or 'i'
  //              (
  //              ( normF(A),         NORM = 'F', 'f', 'E' or 'e'
  
  //  where  norm1  denotes the  one norm of a matrix (maximum column sum),
  //  normI  denotes the  infinity norm  of a matrix  (maximum row sum) and
  //  normF  denotes the  Frobenius norm of a matrix (square root of sum of
  //  squares).  Note that  max(abs(A(i,j)))  is not a  matrix norm.
  
  //  Arguments
  //  =========
  
  //  NORM    (input) CHARACTER*1
  //          Specifies the value to be returned in DLANGB as described
  //          above.
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.  When N = 0, DLANGB is
  //          set to zero.
  
  //  KL      (input) INTEGER
  //          The number of sub-diagonals of the matrix A.  KL >= 0.
  
  //  KU      (input) INTEGER
  //          The number of super-diagonals of the matrix A.  KU >= 0.
  
  //  AB      (input) DOUBLE PRECISION array, dimension (LDAB,N)
  //          The band matrix A, stored in rows 1 to KL+KU+1.  The j-th
  //          column of A is stored in the j-th column of the array AB as
  //          follows:
  //          AB(ku+1+i-j,j) = A(i,j) for max(1,j-ku)<=i<=min(n,j+kl).
  
  //  LDAB    (input) INTEGER
  //          The leading dimension of the array AB.  LDAB >= KL+KU+1.
  
  //  WORK    (workspace) DOUBLE PRECISION array, dimension (LWORK),
  //          where LWORK >= N when NORM = 'I'; otherwise, WORK is not
  //          referenced.
  
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  if( n == 0 ) { 
    value = ZERO;
  }
  else if( lsame( norm, 'M' ) ) { 
    
    //        Find max(abs(A(i,j))).
    
    value = ZERO;
    for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
      for( i = max( ku + 2 - j, 1 ), i_ = i - 1, _do1 = min( n + 
       ku + 1 - j, kl + ku + 1 ); i <= _do1; i++, i_++ ) { 
        value = max( value, abs( AB(j_,i_) ) );
      }
    }
  }
  else if( (lsame( norm, 'O' )) || (norm == '1') ) { 
    
    //        Find norm1(A).
    
    value = ZERO;
    for( j = 1, j_ = j - 1, _do2 = n; j <= _do2; j++, j_++ ) { 
      sum = ZERO;
      for( i = max( ku + 2 - j, 1 ), i_ = i - 1, _do3 = min( n + 
       ku + 1 - j, kl + ku + 1 ); i <= _do3; i++, i_++ ) { 
        sum = sum + abs( AB(j_,i_) );
      }
      value = max( value, sum );
    }
  }
  else if( lsame( norm, 'I' ) ) { 
    
    //        Find normI(A).
    
    for( i = 1, i_ = i - 1, _do4 = n; i <= _do4; i++, i_++ ) { 
      work[i_] = ZERO;
    }
    for( j = 1, j_ = j - 1, _do5 = n; j <= _do5; j++, j_++ ) { 
      k = ku + 1 - j;
      for( i = max( 1, j - ku ), i_ = i - 1, _do6 = min( n, 
       j + kl ); i <= _do6; i++, i_++ ) { 
        work[i_] = work[i_] + abs( AB(j_,k + i_) );
      }
    }
    value = ZERO;
    for( i = 1, i_ = i - 1, _do7 = n; i <= _do7; i++, i_++ ) { 
      value = max( value, work[i_] );
    }
  }
  else if( (lsame( norm, 'F' )) || (lsame( norm, 'E' )) ) { 
    
    //        Find normF(A).
    
    scale = ZERO;
    sum = ONE;
    for( j = 1, j_ = j - 1, _do8 = n; j <= _do8; j++, j_++ ) { 
      l = max( 1, j - ku );
      k = ku + 1 - j + l;
      dlassq( min( n, j + kl ) - l + 1, &AB(j_,k - 1), 1, scale, 
       sum );
    }
    value = scale*sqrt( sum );
  }
  
  dlangb_v = value;
  return( dlangb_v );
  
  //     End of DLANGB
  
#undef  AB
} // end of function 


/*
 * C++ implementation of Lapack routine sgeev
 *
 * $Id: sgeev.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:58:27
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: sgeev.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ZERO = 0.0e0;
const float ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ sgeev(const char &jobvl, const char &jobvr, const long &n, float *a, 
 const long &lda, float wr[], float wi[], float *vl, const long &ldvl, 
 float *vr, const long &ldvr, float work[], const long &lwork, long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define VL(I_,J_) (*(vl+(I_)*(ldvl)+(J_)))
#define VR(I_,J_) (*(vr+(I_)*(ldvr)+(J_)))
  int scalea, select[1], wantvl, wantvr;
  char side;
  long _do0, _do1, _do2, _do3, hswork, i, i_, ibal, ierr, ihi, 
   ilo, itau, iwrk, k, k_, maxb, maxwrk, minwrk, nout;
  float anrm, bignum, cs, cscale, dum[1], eps, r, scl, smlnum, sn;

  
  //  -- LAPACK driver routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  For an N by N real nonsymmetric matrix A, compute
  
  //     the eigenvalues (WR and WI)
  //     the left and/or right eigenvectors (VL and VR)
  
  //  The eigenvectors are computed optionally:
  
  //     JOBVL determines whether to compute left eigenvectors VL
  //     JOBVR determines whether to compute right eigenvectors VR
  
  //  On return array WR contains the real parts of the eigenvalues, and
  //  WI contains the imaginary parts. Optionally, VR contains the right
  //  eigenvectors, and VL the left eigenvectors.
  
  //  Arguments
  //  =========
  
  //  JOBVL   (input) CHARACTER*1
  //          Specifies whether or not to compute left eigenvectors of A.
  //          = 'N': left eigenvectors are not computed.
  //          = 'V': left eigenvectors are computed.
  
  //  JOBVR   (input) CHARACTER*1
  //          Specifies whether or not to compute right eigenvectors of A.
  //          = 'N': right eigenvectors are not computed.
  //          = 'V': right eigenvectors are computed.
  
  //  N       (input) INTEGER
  //          The number of rows and columns of the input matrix A. N >= 0.
  
  //  A       (input/output) REAL array, dimension (LDA,N)
  //          On entry, A is the matrix whose eigenvalues and eigenvectors
  //          are desired.
  //          On exit, A has been overwritten.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  WR      (output) REAL array, dimension (N)
  //  WI      (output) REAL array, dimension (N)
  //          On exit, WR and WI contain the real and imaginary parts,
  //          respectively, of the computed eigenvalues. The eigenvalues
  //          will not be in any particular order, except that DComplex
  //          conjugate pairs of eigenvalues will appear consecutively
  //          with the eigenvalue having the positive imaginary part
  //          first.
  
  //  VL      (output) REAL array, dimension (LDVL,N)
  //          The left eigenvectors will be stored one after another in
  //          the columns of VL, in the same order (but not necessarily
  //          the same position) as their eigenvalues. An eigenvector
  //          corresponding to a real eigenvalue will take up one column.
  //          An eigenvector pair corresponding to a DComplex conjugate
  //          pair of eigenvalues will take up two columns: the first
  //          column will hold the real part, the second will hold the
  //          imaginary part of the eigenvector corresponding to the
  //          eigenvalue with positive imaginary part.
  //          The eigenvectors will be normalized to have Euclidean
  //          norm equal to 1, and to have the largest component real.
  
  //          If WI(j) = 0 so WR(j) is a real eigenvalue, then
  //             VL(j)' * A = WR(j) * VL(j)'
  //          where ' means transpose.  If WR(j)+i*WI(j) and
  //          WR(j+1)+i*WI(j+1) are a conjugate pair of eigenvalues, then
  //             (VL(j) + i*VL(j+1))' * A =
  //                (WR(j) + i*WI(j)) * (VL(j) + i*VL(j+1))'
  //          and
  //             (VL(j) - i*VL(j+1))' * A =
  //                (WR(j+1) + i*WI(j+1)) * (VL(j) - i*VL(j+1))' .
  //          Left eigenvectors of A are the same as the right
  //          eigenvectors of transpose(A).
  
  //          If JOBVL = 'N', VL is not referenced.
  
  //  LDVL    (input) INTEGER
  //          The leading dimension of the array VL.  LDVL >= 1, and if
  //          JOBVL = 'V', LDVL >= N.
  
  //  VR      (output) REAL array, dimension (LDVR,N)
  //          The right eigenvectors will be stored one after another in
  //          the columns of VR, in the same order (but not necessarily
  //          the same position) as their eigenvalues. An eigenvector
  //          corresponding to a real eigenvalue will take up one column.
  //          An eigenvector pair corresponding to a DComplex conjugate
  //          pair of eigenvalues will take up two columns: the first
  //          column will hold the real part, the second will hold the
  //          imaginary part of the eigenvector corresponding to the
  //          eigenvalue with positive imaginary part.
  //          The eigenvectors will be normalized to have Euclidean
  //          norm equal to 1, and to have the largest component real.
  
  //          If WI(j) = 0 so WR(j) is a real eigenvalue, then
  //             A * VR(j) = WR(j) * VR(j) .
  //          If WR(j)+i*WI(j) and WR(j+1)+i*WI(j+1) are a conjugate pair
  //          of eigenvalues, then
  //             A * (VR(j) + i*VR(j+1)) =
  //                (WR(j) + i*WI(j)) * (VR(j) + i*VR(j+1))
  //          and
  //             A * (VR(j) - i*VR(j+1)) =
  //                (WR(j+1) + i*WI(j+1)) * (VR(j) - i*VR(j+1)) .
  
  //          If JOBVR = 'N', VR is not referenced.
  
  //  LDVR    (input) INTEGER
  //          The leading dimension of the array VR.  LDVR >= 1, and if
  //          JOBVR = 'V', LDVR >= N.
  
  //  WORK    (workspace/output) REAL array, dimension (LWORK)
  //          On exit, WORK(1) contains the optimal workspace size LWORK
  //          for high performance.
  
  //  LWORK   (input) INTEGER
  //          The dimension of the array WORK.  LWORK >= max(1,3*N), and
  //          if JOBVL = 'V' or JOBVR = 'V', LWORK >= 4*N.  For good
  //          performance, LWORK must generally be larger.  The optimum
  //          value of LWORK for high performance is returned in WORK(1).
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument had an illegal value.
  //          > 0: the QR algorithm failed to compute all the eigenvalues;
  //               if INFO = i, elements 1:ILO-1 and i+1:N of WR and WI
  //               contain eigenvalues which have converged, and the
  //               eigenvectors are not computed.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Local Arrays ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input arguments
  
  info = 0;
  wantvl = lsame( jobvl, 'V' );
  wantvr = lsame( jobvr, 'V' );
  if( (!wantvl) && (!lsame( jobvl, 'N' )) ) { 
    info = -1;
  }
  else if( (!wantvr) && (!lsame( jobvr, 'N' )) ) { 
    info = -2;
  }
  else if( n < 0 ) { 
    info = -3;
  }
  else if( lda < max( 1, n ) ) { 
    info = -5;
  }
  else if( ldvl < 1 || (wantvl && ldvl < n) ) { 
    info = -9;
  }
  else if( ldvr < 1 || (wantvr && ldvr < n) ) { 
    info = -11;
  }
  
  //     Compute workspace
  //      (Note: Comments in the code beginning "Workspace:" describe the
  //       minimal amount of workspace needed at that point in the code,
  //       as well as the preferred amount for good performance.
  //       NB refers to the optimal block size for the immediately
  //       following subroutine, as returned by ILAENV.
  //       HSWORK refers to the workspace preferred by SHSEQR, as
  //       calculated below. HSWORK is computed assuming ILO=1 and IHI=N,
  //       the worst case.)
  
  minwrk = 1;
  if( info == 0 && lwork >= 1 ) { 
    maxwrk = 2*n + n*ilaenv( 1, "SGEHRD", " ", n, 1, n, 0 );
    if( (!wantvl) && (!wantvr) ) { 
      minwrk = max( 1, 3*n );
      maxb = max( ilaenv( 8, "SHSEQR", "EN", n, 1, n, -1 ), 
       2 );
      k = vmin( maxb, n, max( 2, ilaenv( 4, "SHSEQR", "EN", 
       n, 1, n, -1 ) ), IEND );
      hswork = max( k*(k + 2), 2*n );
      maxwrk = vmax( maxwrk, n + 1, n + hswork, IEND );
    }
    else { 
      minwrk = max( 1, 4*n );
      maxwrk = max( maxwrk, 2*n + (n - 1)*ilaenv( 1, "SORGHR"
       , " ", n, 1, n, -1 ) );
      maxb = max( ilaenv( 8, "SHSEQR", "SV", n, 1, n, -1 ), 
       2 );
      k = vmin( maxb, n, max( 2, ilaenv( 4, "SHSEQR", "SV", 
       n, 1, n, -1 ) ), IEND );
      hswork = max( k*(k + 2), 2*n );
      maxwrk = vmax( maxwrk, n + 1, n + hswork, IEND );
      maxwrk = max( maxwrk, 4*n );
    }
    work[0] = maxwrk;
  }
  if( lwork < minwrk ) { 
    info = -13;
  }
  if( info != 0 ) { 
    xerbla( "SGEEV ", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  //     Get machine constants
  
  eps = slamch( 'P' );
  smlnum = slamch( 'S' );
  bignum = ONE/smlnum;
  slabad( smlnum, bignum );
  smlnum = sqrt( smlnum )/eps;
  bignum = ONE/smlnum;
  
  //     Scale A if max entry outside range [SMLNUM,BIGNUM]
  
  anrm = slange( 'M', n, n, a, lda, dum );
  scalea = FALSE;
  if( anrm > ZERO && anrm < smlnum ) { 
    scalea = TRUE;
    cscale = smlnum;
  }
  else if( anrm > bignum ) { 
    scalea = TRUE;
    cscale = bignum;
  }
  if( scalea ) 
    slascl( 'G', 0, 0, anrm, cscale, n, n, a, lda, ierr );
  
  //     Balance the matrix
  //     (Workspace: need N)
  
  ibal = 1;
  sgebal( 'B', n, a, lda, ilo, ihi, &work[ibal - 1], ierr );
  
  //     Reduce to upper Hessenberg form
  //     (Workspace: need 3*N, prefer 2*N+N*NB)
  
  itau = ibal + n;
  iwrk = itau + n;
  sgehrd( n, ilo, ihi, a, lda, &work[itau - 1], &work[iwrk - 1], 
   lwork - iwrk + 1, ierr );
  
  if( wantvl ) { 
    
    //        Want left eigenvectors
    //        Copy Householder vectors to VL
    
    side = 'L';
    slacpy( 'L', n, n, a, lda, vl, ldvl );
    
    //        Generate orthogonal matrix in VL
    //        (Workspace: need 3*N-1, prefer 2*N+(N-1)*NB)
    
    sorghr( n, ilo, ihi, vl, ldvl, &work[itau - 1], &work[iwrk - 1], 
     lwork - iwrk + 1, ierr );
    
    //        Perform QR iteration, accumulating Schur vectors in VL
    //        (Workspace: need N+1, prefer N+HSWORK (see comments) )
    
    iwrk = itau;
    shseqr( 'S', 'V', n, ilo, ihi, a, lda, wr, wi, vl, ldvl, &work[iwrk - 1], 
     lwork - iwrk + 1, info );
    
    if( wantvr ) { 
      
      //           Want left and right eigenvectors
      //           Copy Schur vectors to VR
      
      side = 'B';
      slacpy( 'F', n, n, vl, ldvl, vr, ldvr );
    }
    
  }
  else if( wantvr ) { 
    
    //        Want right eigenvectors
    //        Copy Householder vectors to VR
    
    side = 'R';
    slacpy( 'L', n, n, a, lda, vr, ldvr );
    
    //        Generate orthogonal matrix in VR
    //        (Workspace: need 3*N-1, prefer 2*N+(N-1)*NB)
    
    sorghr( n, ilo, ihi, vr, ldvr, &work[itau - 1], &work[iwrk - 1], 
     lwork - iwrk + 1, ierr );
    
    //        Perform QR iteration, accumulating Schur vectors in VR
    //        (Workspace: need N+1, prefer N+HSWORK (see comments) )
    
    iwrk = itau;
    shseqr( 'S', 'V', n, ilo, ihi, a, lda, wr, wi, vr, ldvr, &work[iwrk - 1], 
     lwork - iwrk + 1, info );
    
  }
  else { 
    
    //        Compute eigenvalues only
    //        (Workspace: need N+1, prefer N+HSWORK (see comments) )
    
    iwrk = itau;
    shseqr( 'E', 'N', n, ilo, ihi, a, lda, wr, wi, vr, ldvr, &work[iwrk - 1], 
     lwork - iwrk + 1, info );
  }
  
  //     If INFO > 0 from SHSEQR, then quit
  
  if( info > 0 ) 
    goto L_50;
  
  if( wantvl || wantvr ) { 
    
    //        Compute left and/or right eigenvectors
    //        (Workspace: need 4*N)
    
    strevc( side, 'O', select, n, a, lda, vl, ldvl, vr, ldvr, 
     n, nout, &work[iwrk - 1], ierr );
  }
  
  if( wantvl ) { 
    
    //        Undo balancing of left eigenvectors
    //        (Workspace: need N)
    
    sgebak( 'B', 'L', n, ilo, ihi, &work[ibal - 1], n, vl, ldvl, 
     ierr );
    
    //        Normalize left eigenvectors and make largest component real
    
    for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
      if( wi[i_] == ZERO ) { 
        scl = ONE/snrm2( n, &VL(i_,0), 1 );
        sscal( n, scl, &VL(i_,0), 1 );
      }
      else if( wi[i_] > ZERO ) { 
        scl = ONE/slapy2( snrm2( n, &VL(i_,0), 1 ), snrm2( n, 
         &VL(i_ + 1,0), 1 ) );
        sscal( n, scl, &VL(i_,0), 1 );
        sscal( n, scl, &VL(i_ + 1,0), 1 );
        for( k = 1, k_ = k - 1, _do1 = n; k <= _do1; k++, k_++ ) { 
          work[iwrk + k_ - 1] = pow(VL(i_,k_), 2) + pow(VL(i_ + 1,k_), 2);
        }
        k = isamax( n, &work[iwrk - 1], 1 );
        slartg( VL(i_,k - 1), VL(i_ + 1,k - 1), cs, sn, r );
        srot( n, &VL(i_,0), 1, &VL(i_ + 1,0), 1, cs, sn );
        VL(i_ + 1,k - 1) = ZERO;
      }
    }
  }
  
  if( wantvr ) { 
    
    //        Undo balancing of right eigenvectors
    //        (Workspace: need N)
    
    sgebak( 'B', 'R', n, ilo, ihi, &work[ibal - 1], n, vr, ldvr, 
     ierr );
    
    //        Normalize right eigenvectors and make largest component real
    
    for( i = 1, i_ = i - 1, _do2 = n; i <= _do2; i++, i_++ ) { 
      if( wi[i_] == ZERO ) { 
        scl = ONE/snrm2( n, &VR(i_,0), 1 );
        sscal( n, scl, &VR(i_,0), 1 );
      }
      else if( wi[i_] > ZERO ) { 
        scl = ONE/slapy2( snrm2( n, &VR(i_,0), 1 ), snrm2( n, 
         &VR(i_ + 1,0), 1 ) );
        sscal( n, scl, &VR(i_,0), 1 );
        sscal( n, scl, &VR(i_ + 1,0), 1 );
        for( k = 1, k_ = k - 1, _do3 = n; k <= _do3; k++, k_++ ) { 
          work[iwrk + k_ - 1] = pow(VR(i_,k_), 2) + pow(VR(i_ + 1,k_), 2);
        }
        k = isamax( n, &work[iwrk - 1], 1 );
        slartg( VR(i_,k - 1), VR(i_ + 1,k - 1), cs, sn, r );
        srot( n, &VR(i_,0), 1, &VR(i_ + 1,0), 1, cs, sn );
        VR(i_ + 1,k - 1) = ZERO;
      }
    }
  }
  
  //     Undo scaling if necessary
  
L_50:
  ;
  if( scalea ) { 
    slascl( 'G', 0, 0, cscale, anrm, n - info, 1, &wr[info], max( n - 
     info, 1 ), ierr );
    slascl( 'G', 0, 0, cscale, anrm, n - info, 1, &wi[info], max( n - 
     info, 1 ), ierr );
    if( info > 0 ) { 
      slascl( 'G', 0, 0, cscale, anrm, ilo - 1, 1, wr, n, ierr );
      slascl( 'G', 0, 0, cscale, anrm, ilo - 1, 1, wi, n, ierr );
    }
  }
  
  work[0] = maxwrk;
  return;
  
  //     End of SGEEV
  
#undef  VR
#undef  VL
#undef  A
} // end of function 


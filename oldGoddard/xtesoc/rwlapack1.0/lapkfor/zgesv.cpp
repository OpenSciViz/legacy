/*
 * C++ implementation of Lapack routine zgesv
 *
 * $Id: zgesv.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:46:43
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zgesv.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

RWLAPKDECL void /*FUNCTION*/ zgesv(const long &n, const long &nrhs, DComplex *a, const long &lda, 
   long ipiv[], DComplex *b, const long &ldb, long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))

  
  //  -- LAPACK driver routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZGESV computes the solution to a DComplex system of linear equations
  //     A * X = B,
  //  where A is an N by N matrix and X and B are N by NRHS matrices.
  
  //  The LU decomposition with partial pivoting and row interchanges is
  //  used to factor A as
  //     A = P * L * U,
  //  where P is a permutation matrix, L is unit lower triangular, and U is
  //  upper triangular.  The factored form of A is then used to solve the
  //  system of equations A * X = B.
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The number of linear equations, i.e., the order of the
  //          matrix A.  N >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right hand sides, i.e., the number of columns
  //          of the matrix B.  NRHS >= 0.
  
  //  A       (input/output) COMPLEX*16 array, dimension (LDA,N)
  //          On entry, the N by N matrix of coefficients A.
  //          On exit, the factors L and U from the factorization
  //          A = P*L*U; the unit diagonal elements of L are not stored.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  IPIV    (output) INTEGER array, dimension (N)
  //          The pivot indices that define the permutation matrix P;
  //          row i of the matrix was interchanged with row IPIV(i).
  
  //  B       (input/output) COMPLEX*16 array, dimension (LDB,NRHS)
  //          On entry, the N by NRHS matrix of right hand side vectors B
  //          for the system of equations A*X = B.
  //          On exit, if INFO = 0, the N by NRHS matrix of solution
  //          vectors X.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: if INFO = k, U(k,k) is exactly zero.  The factorization
  //               has been completed, but the factor U is exactly
  //               singular, so the solution could not be computed.
  
  //  =====================================================================
  
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  if( n < 0 ) { 
    info = -1;
  }
  else if( nrhs < 0 ) { 
    info = -2;
  }
  else if( lda < max( 1, n ) ) { 
    info = -4;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -7;
  }
  if( info != 0 ) { 
    xerbla( "ZGESV ", -info );
    return;
  }
  
  //     Compute the LU factorization of A.
  
  zgetrf( n, n, a, lda, ipiv, info );
  if( info == 0 ) { 
    
    //        Solve the system A*X = B, overwriting B with X.
    
    zgetrs( 'N'/*No transpose*/, n, nrhs, a, lda, ipiv, b, ldb, 
     info );
  }
  return;
  
  //     End of ZGESV
  
#undef  B
#undef  A
} // end of function 


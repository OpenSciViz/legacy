/*
 * C++ implementation of Lapack routine zhetri
 *
 * $Id: zhetri.cpp,v 1.2 1993/07/21 22:22:14 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:47:37
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zhetri.cpp,v $
 * Revision 1.2  1993/07/21  22:22:14  alv
 * ported to Microsoft visual C++
 *
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ONE = 1.0e0;
const DComplex CONE = DComplex(1.0e0);
const DComplex ZERO = DComplex(0.0e0);
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zhetri(const char &uplo, const long &n, DComplex *a, const long &lda, 
 long ipiv[], DComplex work[], long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  int upper;
  long _do0, _do1, _do2, _do3, _do4, info_, j, j_, k, kp;
  double ak, akp1, d, t;
  DComplex akkp1, temp;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZHETRI computes the inverse of a DComplex Hermitian indefinite matrix
  //  A using the factorization A = U*D*U' or A = L*D*L' computed by
  //  ZHETRF.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the details of the factorization are stored
  //          as an upper or lower triangular matrix.
  //          = 'U':  Upper triangular (form is A = U*D*U')
  //          = 'L':  Lower triangular (form is A = L*D*L')
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  A       (input/output) COMPLEX*16 array, dimension (LDA,N)
  //          On entry, the block diagonal matrix D and the multipliers
  //          used to obtain the factor U or L as computed by ZHETRF.
  
  //          On exit, if INFO = 0, the (Hermitian) inverse of the original
  //          matrix.  If UPLO = 'U', the upper triangular part of the
  //          inverse is formed and the part of A below the diagonal is not
  //          referenced; if UPLO = 'L' the lower triangular part of the
  //          inverse is formed and the part of A above the diagonal is
  //          not referenced.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  IPIV    (input) INTEGER array, dimension (N)
  //          Details of the interchanges and the block structure of D
  //          as determined by ZHETRF.
  
  //  WORK    (workspace) COMPLEX*16 array, dimension (N)
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: if INFO = k, D(k,k) = 0; the matrix is singular and its
  //               inverse could not be computed.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( lda < max( 1, n ) ) { 
    info = -4;
  }
  if( info != 0 ) { 
    xerbla( "ZHETRI", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  //     Check that the diagonal matrix D is nonsingular.
  
  if( upper ) { 
    
    //        Upper triangular storage: examine D from bottom to top
    
    for( info = n, info_ = info - 1; info >= 1; info--, info_-- ) { 
      if( ipiv[info_] > 0 && ctocf(A(info_,info_)) == ctocf(ZERO) ) 
        return;
    }
  }
  else { 
    
    //        Lower triangular storage: examine D from top to bottom.
    
    for( info = 1, info_ = info - 1, _do0 = n; info <= _do0; info++, info_++ ) { 
      if( ipiv[info_] > 0 && ctocf(A(info_,info_)) == ctocf(ZERO) ) 
        return;
    }
  }
  info = 0;
  
  if( upper ) { 
    
    //        Compute inv(A) from the factorization A = U*D*U'.
    
    //        K is the main loop index, increasing from 1 to N in steps of
    //        1 or 2, depending on the size of the diagonal blocks.
    
    k = 1;
L_30:
    ;
    
    //        If K > N, exit from loop.
    
    if( k > n ) 
      goto L_60;
    
    if( ipiv[k - 1] > 0 ) { 
      
      //           1 x 1 diagonal block
      
      //           Invert the diagonal block.
      
      A(k - 1,k - 1) = (DComplex)(ONE/real( A(k - 1,k - 1) ));
      
      //           Compute column K of the inverse.
      
      if( k > 1 ) { 
        zcopy( k - 1, &A(k - 1,0), 1, work, 1 );
        zhemv( uplo, k - 1, -(CONE), a, lda, work, 1, ZERO, 
         &A(k - 1,0), 1 );
        A(k - 1,k - 1) = A(k - 1,k - 1) - real( zdotc( k - 
         1, work, 1, &A(k - 1,0), 1 ) );
      }
      
      //           Interchange rows and columns K and IPIV(K).
      
      kp = ipiv[k - 1];
      if( kp != k ) { 
        zswap( kp, &A(kp - 1,0), 1, &A(k - 1,0), 1 );
        for( j = k, j_ = j - 1, _do1 = kp; j >= _do1; j--, j_-- ) { 
          temp = conj( A(k - 1,j_) );
          A(k - 1,j_) = conj( A(j_,kp - 1) );
          A(j_,kp - 1) = temp;
        }
      }
      k = k + 1;
    }
    else { 
      
      //           2 x 2 diagonal block
      
      //           Invert the diagonal block.
      
      t = abs( A(k,k - 1) );
      ak = real( A(k - 1,k - 1) )/t;
      akp1 = real( A(k,k) )/t;
      akkp1 = A(k,k - 1)/t;
      d = t*(ak*akp1 - ONE);
      A(k - 1,k - 1) = (DComplex)(akp1/d);
      A(k,k) = (DComplex)(ak/d);
      A(k,k - 1) = -(akkp1/d);
      
      //           Compute columns K and K+1 of the inverse.
      
      if( k > 1 ) { 
        zcopy( k - 1, &A(k - 1,0), 1, work, 1 );
        zhemv( uplo, k - 1, -(CONE), a, lda, work, 1, ZERO, 
         &A(k - 1,0), 1 );
        A(k - 1,k - 1) = A(k - 1,k - 1) - real( zdotc( k - 
         1, work, 1, &A(k - 1,0), 1 ) );
        A(k,k - 1) = A(k,k - 1) - zdotc( k - 1, &A(k - 1,0), 
         1, &A(k,0), 1 );
        zcopy( k - 1, &A(k,0), 1, work, 1 );
        zhemv( uplo, k - 1, -(CONE), a, lda, work, 1, ZERO, 
         &A(k,0), 1 );
        A(k,k) = A(k,k) - real( zdotc( k - 1, work, 1, &A(k,0), 
         1 ) );
      }
      
      //           Interchange rows and columns K and -IPIV(K).
      
      kp = -ipiv[k - 1];
      if( kp != k ) { 
        zswap( kp, &A(kp - 1,0), 1, &A(k - 1,0), 1 );
        for( j = k, j_ = j - 1, _do2 = kp; j >= _do2; j--, j_-- ) { 
          temp = conj( A(k - 1,j_) );
          A(k - 1,j_) = conj( A(j_,kp - 1) );
          A(j_,kp - 1) = temp;
        }
        temp = A(k,kp - 1);
        A(k,kp - 1) = A(k,k - 1);
        A(k,k - 1) = temp;
      }
      k = k + 2;
    }
    
    goto L_30;
L_60:
    ;
    
  }
  else { 
    
    //        Compute inv(A) from the factorization A = L*D*L'.
    
    //        K is the main loop index, increasing from 1 to N in steps of
    //        1 or 2, depending on the size of the diagonal blocks.
    
    k = n;
L_70:
    ;
    
    //        If K < 1, exit from loop.
    
    if( k < 1 ) 
      goto L_100;
    
    if( ipiv[k - 1] > 0 ) { 
      
      //           1 x 1 diagonal block
      
      //           Invert the diagonal block.
      
      A(k - 1,k - 1) = (DComplex)(ONE/real( A(k - 1,k - 1) ));
      
      //           Compute column K of the inverse.
      
      if( k < n ) { 
        zcopy( n - k, &A(k - 1,k), 1, work, 1 );
        zhemv( uplo, n - k, -(CONE), &A(k,k), lda, work, 1, 
         ZERO, &A(k - 1,k), 1 );
        A(k - 1,k - 1) = A(k - 1,k - 1) - real( zdotc( n - 
         k, work, 1, &A(k - 1,k), 1 ) );
      }
      
      //           Interchange rows and columns K and IPIV(K).
      
      kp = ipiv[k - 1];
      if( kp != k ) { 
        for( j = kp, j_ = j - 1, _do3 = k; j >= _do3; j--, j_-- ) { 
          temp = conj( A(k - 1,j_) );
          A(k - 1,j_) = conj( A(j_,kp - 1) );
          A(j_,kp - 1) = temp;
        }
        zswap( n - kp + 1, &A(k - 1,kp - 1), 1, &A(kp - 1,kp - 1), 
         1 );
      }
      k = k - 1;
    }
    else { 
      
      //           2 x 2 diagonal block
      
      //           Invert the diagonal block.
      
      t = abs( A(k - 2,k - 1) );
      ak = real( A(k - 2,k - 2) )/t;
      akp1 = real( A(k - 1,k - 1) )/t;
      akkp1 = A(k - 2,k - 1)/t;
      d = t*(ak*akp1 - ONE);
      A(k - 2,k - 2) = (DComplex)(akp1/d);
      A(k - 1,k - 1) = (DComplex)(ak/d);
      A(k - 2,k - 1) = -(akkp1/d);
      
      //           Compute columns K-1 and K of the inverse.
      
      if( k < n ) { 
        zcopy( n - k, &A(k - 1,k), 1, work, 1 );
        zhemv( uplo, n - k, -(CONE), &A(k,k), lda, work, 1, 
         ZERO, &A(k - 1,k), 1 );
        A(k - 1,k - 1) = A(k - 1,k - 1) - real( zdotc( n - 
         k, work, 1, &A(k - 1,k), 1 ) );
        A(k - 2,k - 1) = A(k - 2,k - 1) - zdotc( n - k, &A(k - 1,k), 
         1, &A(k - 2,k), 1 );
        zcopy( n - k, &A(k - 2,k), 1, work, 1 );
        zhemv( uplo, n - k, -(CONE), &A(k,k), lda, work, 1, 
         ZERO, &A(k - 2,k), 1 );
        A(k - 2,k - 2) = A(k - 2,k - 2) - real( zdotc( n - 
         k, work, 1, &A(k - 2,k), 1 ) );
      }
      
      //           Interchange rows and columns K-1 and -IPIV(K).
      
      kp = -ipiv[k - 1];
      if( kp != k ) { 
        temp = A(k - 2,k - 1);
        A(k - 2,k - 1) = A(k - 2,kp - 1);
        A(k - 2,kp - 1) = temp;
        for( j = kp, j_ = j - 1, _do4 = k; j >= _do4; j--, j_-- ) { 
          temp = conj( A(k - 1,j_) );
          A(k - 1,j_) = conj( A(j_,kp - 1) );
          A(j_,kp - 1) = temp;
        }
        zswap( n - kp + 1, &A(k - 1,kp - 1), 1, &A(kp - 1,kp - 1), 
         1 );
      }
      k = k - 2;
    }
    
    goto L_70;
L_100:
    ;
  }
  
  return;
  
  //     End of ZHETRI
  
#undef  A
} // end of function 


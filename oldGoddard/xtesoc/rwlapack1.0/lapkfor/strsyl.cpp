/*
 * C++ implementation of Lapack routine strsyl
 *
 * $Id: strsyl.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:03:40
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: strsyl.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ZERO = 0.0e0;
const float ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ strsyl(const char &trana, const char &tranb, const long &isgn, 
 const long &m, const long &n, float *a, const long &lda, float *b, const long &ldb, 
 float *c, const long &ldc, float &scale, long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
#define C(I_,J_)  (*(c+(I_)*(ldc)+(J_)))
  int notrna, notrnb;
  long _do0, _do1, _do10, _do11, _do12, _do13, _do14, _do15, 
   _do16, _do17, _do18, _do19, _do2, _do20, _do3, _do4, _do5, _do6, 
   _do7, _do8, _do9, i, i_, ierr, j, j_, k, k1, k2, k_, knext, l, 
   l1, l2, l_, lnext;
  float a11, bignum, da11, db, dum[1], eps, scaloc, sgn, smin, smlnum, 
   suml, sumr, vec[2][2], x[2][2], xnorm;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  STRSYL solves the real Sylvester matrix equation:
  
  //     op(A)*X + X*op(B) = scale*C or
  //     op(A)*X - X*op(B) = scale*C,
  
  //  where op(A) = A or A**T, and  A and B are both upper quasi-
  //  triangular. A is m-by-m and B is n-by-n; the right hand side C and
  //  the solution X are m-by-n; and scale is an output scale factor, set
  //  <= 1 to avoid overflow in X.
  
  //  A and B must be in Schur canonical form, that is, block upper
  //  triangular with 1-by-1 and 2-by-2 diagonal blocks; each 2-by-2
  //  diagonal block has its diagonal elements equal and its off-diagonal
  //  elements of opposite sign.
  
  //  Arguments
  //  =========
  
  //  TRANA   (input) CHARACTER*1
  //          Specifies the option op(A):
  //          = 'N': op(A) = A    (No transpose)
  //          = 'T': op(A) = A**T (Transpose)
  //          = 'C': op(A) = A**T (Conjugate transpose = Transpose)
  
  //  TRANB   (input) CHARACTER*1
  //          Specifies the option op(B):
  //          = 'N': op(B) = B    (No transpose)
  //          = 'T': op(B) = B**T (Transpose)
  //          = 'C': op(B) = B**T (Conjugate transpose = Transpose)
  
  //  ISGN    (input) INTEGER
  //          Specifies the sign in the equation:
  //          = +1: solve op(A)*X + X*op(B) = scale*C
  //          = -1: solve op(A)*X - X*op(B) = scale*C
  
  //  M       (input) INTEGER
  //          The order of the matrix A, and the number of rows in the
  //          matrices X and C. M >= 0.
  
  //  N       (input) INTEGER
  //          The order of the matrix B, and the number of columns in the
  //          matrices X and C. N >= 0.
  
  //  A       (input) REAL array, dimension (LDA,M)
  //          The upper quasi-triangular matrix A, in Schur canonical form.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A. LDA >= max(1,M).
  
  //  B       (input) REAL array, dimension (LDB,N)
  //          The upper quasi-triangular matrix B, in Schur canonical form.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B. LDB >= max(1,N).
  
  //  C       (input/output) REAL array, dimension (LDC,N)
  //          On entry, the m-by-n right hand side matrix C.
  //          On exit, C is overwritten by the solution matrix X.
  
  //  LDC     (input) INTEGER
  //          The leading dimension of the array C. LDC >= max(1,M)
  
  //  SCALE   (output) REAL
  //          The scale factor, scale, set <= 1 to avoid overflow in X.
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument had an illegal value
  //          = 1: A and B have common or very close eigenvalues; perturbed
  //               values were used to solve the equation (but the matrices
  //               A and B are unchanged).
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Local Arrays ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Decode and Test input parameters
  
  notrna = lsame( trana, 'N' );
  notrnb = lsame( tranb, 'N' );
  
  info = 0;
  if( (!notrna && !lsame( trana, 'T' )) && !lsame( trana, 'C' ) ) { 
    info = -1;
  }
  else if( (!notrnb && !lsame( tranb, 'T' )) && !lsame( tranb, 'C' )
    ) { 
    info = -2;
  }
  else if( isgn != 1 && isgn != -1 ) { 
    info = -3;
  }
  else if( m < 0 ) { 
    info = -4;
  }
  else if( n < 0 ) { 
    info = -5;
  }
  else if( lda < max( 1, m ) ) { 
    info = -7;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -9;
  }
  else if( ldc < max( 1, m ) ) { 
    info = -11;
  }
  if( info != 0 ) { 
    xerbla( "STRSYL", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( m == 0 || n == 0 ) 
    return;
  
  //     Set constants to control overflow
  
  eps = slamch( 'P' );
  smlnum = slamch( 'S' );
  bignum = ONE/smlnum;
  slabad( smlnum, bignum );
  smlnum = smlnum*(float)( m*n )/eps;
  bignum = ONE/smlnum;
  
  smin = vmax( smlnum, eps*slange( 'M', m, m, a, lda, dum ), eps*
   slange( 'M', n, n, b, ldb, dum ), FEND );
  
  scale = ONE;
  sgn = isgn;
  
  if( notrna && notrnb ) { 
    
    //        Solve    A*X + ISGN*X*B = scale*C.
    
    //        The (K,L)th block of X is determined starting from
    //        bottom-left corner column by column by
    
    //         A(K,K)*X(K,L) + ISGN*X(K,L)*B(L,L) = C(K,L) - R(K,L)
    
    //        Where
    //                  M                         L-1
    //        R(K,L) = SUM [A(K,I)*X(I,L)] + ISGN*SUM [X(K,J)*B(J,L)].
    //                I=K+1                       J=1
    
    //        Start column loop (index = L)
    //        L1 (L2) : column index of the first (first) row of X(K,L).
    
    lnext = 1;
    for( l = 1, l_ = l - 1, _do0 = n; l <= _do0; l++, l_++ ) { 
      if( l < lnext ) 
        goto L_70;
      if( l == n ) { 
        l1 = l;
        l2 = l;
      }
      else { 
        if( B(l_,l_ + 1) != ZERO ) { 
          l1 = l;
          l2 = l + 1;
          lnext = l + 2;
        }
        else { 
          l1 = l;
          l2 = l;
          lnext = l + 1;
        }
      }
      
      //           Start row loop (index = K)
      //           K1 (K2): row index of the first (last) row of X(K,L).
      
      knext = m;
      for( k = m, k_ = k - 1; k >= 1; k--, k_-- ) { 
        if( k > knext ) 
          goto L_60;
        if( k == 1 ) { 
          k1 = k;
          k2 = k;
        }
        else { 
          if( A(k_ - 1,k_) != ZERO ) { 
            k1 = k - 1;
            k2 = k;
            knext = k - 2;
          }
          else { 
            k1 = k;
            k2 = k;
            knext = k - 1;
          }
        }
        
        if( l1 == l2 && k1 == k2 ) { 
          suml = sdot( m - k1, &A(min( k1 + 1, m ) - 1,k1 - 1), 
           lda, &C(l1 - 1,min( k1 + 1, m ) - 1), 1 );
          sumr = sdot( l1 - 1, &C(0,k1 - 1), ldc, &B(l1 - 1,0), 
           1 );
          vec[0][0] = C(l1 - 1,k1 - 1) - (suml + sgn*sumr);
          scaloc = ONE;
          
          a11 = A(k1 - 1,k1 - 1) + sgn*B(l1 - 1,l1 - 1);
          da11 = abs( a11 );
          if( da11 <= smin ) { 
            a11 = smin;
            da11 = smin;
            info = 1;
          }
          db = abs( vec[0][0] );
          if( da11 < ONE && db > ONE ) { 
            if( db > bignum*da11 ) 
              scaloc = ONE/db;
          }
          x[0][0] = (vec[0][0]*scaloc)/a11;
          
          if( scaloc != ONE ) { 
            for( j = 1, j_ = j - 1, _do1 = n; j <= _do1; j++, j_++ ) { 
              sscal( m, scaloc, &C(j_,0), 1 );
            }
            scale = scale*scaloc;
          }
          C(l1 - 1,k1 - 1) = x[0][0];
          
        }
        else if( l1 == l2 && k1 != k2 ) { 
          
          suml = sdot( m - k2, &A(min( k2 + 1, m ) - 1,k1 - 1), 
           lda, &C(l1 - 1,min( k2 + 1, m ) - 1), 1 );
          sumr = sdot( l1 - 1, &C(0,k1 - 1), ldc, &B(l1 - 1,0), 
           1 );
          vec[0][0] = C(l1 - 1,k1 - 1) - (suml + sgn*sumr);
          
          suml = sdot( m - k2, &A(min( k2 + 1, m ) - 1,k2 - 1), 
           lda, &C(l1 - 1,min( k2 + 1, m ) - 1), 1 );
          sumr = sdot( l1 - 1, &C(0,k2 - 1), ldc, &B(l1 - 1,0), 
           1 );
          vec[0][1] = C(l1 - 1,k2 - 1) - (suml + sgn*sumr);
          
          slaln2( FALSE, 2, 1, smin, ONE, &A(k1 - 1,k1 - 1), 
           lda, ONE, ONE, (float*)vec, 2, -sgn*B(l1 - 1,l1 - 1), 
           ZERO, (float*)x, 2, scaloc, xnorm, ierr );
          if( ierr != 0 ) 
            info = 1;
          
          if( scaloc != ONE ) { 
            for( j = 1, j_ = j - 1, _do2 = n; j <= _do2; j++, j_++ ) { 
              sscal( m, scaloc, &C(j_,0), 1 );
            }
            scale = scale*scaloc;
          }
          C(l1 - 1,k1 - 1) = x[0][0];
          C(l1 - 1,k2 - 1) = x[0][1];
          
        }
        else if( l1 != l2 && k1 == k2 ) { 
          
          suml = sdot( m - k1, &A(min( k1 + 1, m ) - 1,k1 - 1), 
           lda, &C(l1 - 1,min( k1 + 1, m ) - 1), 1 );
          sumr = sdot( l1 - 1, &C(0,k1 - 1), ldc, &B(l1 - 1,0), 
           1 );
          vec[0][0] = sgn*(C(l1 - 1,k1 - 1) - (suml + sgn*
           sumr));
          
          suml = sdot( m - k1, &A(min( k1 + 1, m ) - 1,k1 - 1), 
           lda, &C(l2 - 1,min( k1 + 1, m ) - 1), 1 );
          sumr = sdot( l1 - 1, &C(0,k1 - 1), ldc, &B(l2 - 1,0), 
           1 );
          vec[0][1] = sgn*(C(l2 - 1,k1 - 1) - (suml + sgn*
           sumr));
          
          slaln2( TRUE, 2, 1, smin, ONE, &B(l1 - 1,l1 - 1), 
           ldb, ONE, ONE, (float*)vec, 2, -sgn*A(k1 - 1,k1 - 1), 
           ZERO, (float*)x, 2, scaloc, xnorm, ierr );
          if( ierr != 0 ) 
            info = 1;
          
          if( scaloc != ONE ) { 
            for( j = 1, j_ = j - 1, _do3 = n; j <= _do3; j++, j_++ ) { 
              for( i = 1, i_ = i - 1, _do4 = m; i <= _do4; i++, i_++ ) { 
                sscal( m, scaloc, &C(j_,0), 1 );
              }
            }
            scale = scale*scaloc;
          }
          C(l1 - 1,k1 - 1) = x[0][0];
          C(l2 - 1,k1 - 1) = x[0][1];
          
        }
        else if( l1 != l2 && k1 != k2 ) { 
          
          suml = sdot( m - k2, &A(min( k2 + 1, m ) - 1,k1 - 1), 
           lda, &C(l1 - 1,min( k2 + 1, m ) - 1), 1 );
          sumr = sdot( l1 - 1, &C(0,k1 - 1), ldc, &B(l1 - 1,0), 
           1 );
          vec[0][0] = C(l1 - 1,k1 - 1) - (suml + sgn*sumr);
          
          suml = sdot( m - k2, &A(min( k2 + 1, m ) - 1,k1 - 1), 
           lda, &C(l2 - 1,min( k2 + 1, m ) - 1), 1 );
          sumr = sdot( l1 - 1, &C(0,k1 - 1), ldc, &B(l2 - 1,0), 
           1 );
          vec[1][0] = C(l2 - 1,k1 - 1) - (suml + sgn*sumr);
          
          suml = sdot( m - k2, &A(min( k2 + 1, m ) - 1,k2 - 1), 
           lda, &C(l1 - 1,min( k2 + 1, m ) - 1), 1 );
          sumr = sdot( l1 - 1, &C(0,k2 - 1), ldc, &B(l1 - 1,0), 
           1 );
          vec[0][1] = C(l1 - 1,k2 - 1) - (suml + sgn*sumr);
          
          suml = sdot( m - k2, &A(min( k2 + 1, m ) - 1,k2 - 1), 
           lda, &C(l2 - 1,min( k2 + 1, m ) - 1), 1 );
          sumr = sdot( l1 - 1, &C(0,k2 - 1), ldc, &B(l2 - 1,0), 
           1 );
          vec[1][1] = C(l2 - 1,k2 - 1) - (suml + sgn*sumr);
          
          slasy2( FALSE, FALSE, isgn, 2, 2, &A(k1 - 1,k1 - 1), 
           lda, &B(l1 - 1,l1 - 1), ldb, (float*)vec, 2, 
           scaloc, (float*)x, 2, xnorm, ierr );
          if( ierr != 0 ) 
            info = 1;
          
          if( scaloc != ONE ) { 
            for( j = 1, j_ = j - 1, _do5 = n; j <= _do5; j++, j_++ ) { 
              sscal( m, scaloc, &C(j_,0), 1 );
            }
            scale = scale*scaloc;
          }
          C(l1 - 1,k1 - 1) = x[0][0];
          C(l2 - 1,k1 - 1) = x[1][0];
          C(l1 - 1,k2 - 1) = x[0][1];
          C(l2 - 1,k2 - 1) = x[1][1];
        }
        
L_60:
        ;
      }
      
L_70:
      ;
    }
    
  }
  else if( !notrna && notrnb ) { 
    
    //        Solve    A' *X + ISGN*X*B = scale*C.
    
    //        The (K,L)th block of X is determined starting from
    //        upper-left corner column by column by
    
    //          A(K,K)'*X(K,L) + ISGN*X(K,L)*B(L,L) = C(K,L) - R(K,L)
    
    //        Where
    //                   K-1                        L-1
    //          R(K,L) = SUM [A(I,K)'*X(I,L)] +ISGN*SUM [X(K,J)*B(J,L)]
    //                   I=1                        J=1
    
    //        Start column loop (index = L)
    //        L1 (L2): column index of the first (last) row of X(K,L)
    
    lnext = 1;
    for( l = 1, l_ = l - 1, _do6 = n; l <= _do6; l++, l_++ ) { 
      if( l < lnext ) 
        goto L_130;
      if( l == n ) { 
        l1 = l;
        l2 = l;
      }
      else { 
        if( B(l_,l_ + 1) != ZERO ) { 
          l1 = l;
          l2 = l + 1;
          lnext = l + 2;
        }
        else { 
          l1 = l;
          l2 = l;
          lnext = l + 1;
        }
      }
      
      //           Start row loop (index = K)
      //           K1 (K2): row index of the first (last) row of X(K,L)
      
      knext = 1;
      for( k = 1, k_ = k - 1, _do7 = m; k <= _do7; k++, k_++ ) { 
        if( k < knext ) 
          goto L_120;
        if( k == m ) { 
          k1 = k;
          k2 = k;
        }
        else { 
          if( A(k_,k_ + 1) != ZERO ) { 
            k1 = k;
            k2 = k + 1;
            knext = k + 2;
          }
          else { 
            k1 = k;
            k2 = k;
            knext = k + 1;
          }
        }
        
        if( l1 == l2 && k1 == k2 ) { 
          suml = sdot( k1 - 1, &A(k1 - 1,0), 1, &C(l1 - 1,0), 
           1 );
          sumr = sdot( l1 - 1, &C(0,k1 - 1), ldc, &B(l1 - 1,0), 
           1 );
          vec[0][0] = C(l1 - 1,k1 - 1) - (suml + sgn*sumr);
          scaloc = ONE;
          
          a11 = A(k1 - 1,k1 - 1) + sgn*B(l1 - 1,l1 - 1);
          da11 = abs( a11 );
          if( da11 <= smin ) { 
            a11 = smin;
            da11 = smin;
            info = 1;
          }
          db = abs( vec[0][0] );
          if( da11 < ONE && db > ONE ) { 
            if( db > bignum*da11 ) 
              scaloc = ONE/db;
          }
          x[0][0] = (vec[0][0]*scaloc)/a11;
          
          if( scaloc != ONE ) { 
            for( j = 1, j_ = j - 1, _do8 = n; j <= _do8; j++, j_++ ) { 
              sscal( m, scaloc, &C(j_,0), 1 );
            }
            scale = scale*scaloc;
          }
          C(l1 - 1,k1 - 1) = x[0][0];
          
        }
        else if( l1 == l2 && k1 != k2 ) { 
          
          suml = sdot( k1 - 1, &A(k1 - 1,0), 1, &C(l1 - 1,0), 
           1 );
          sumr = sdot( l1 - 1, &C(0,k1 - 1), ldc, &B(l1 - 1,0), 
           1 );
          vec[0][0] = C(l1 - 1,k1 - 1) - (suml + sgn*sumr);
          
          suml = sdot( k1 - 1, &A(k2 - 1,0), 1, &C(l1 - 1,0), 
           1 );
          sumr = sdot( l1 - 1, &C(0,k2 - 1), ldc, &B(l1 - 1,0), 
           1 );
          vec[0][1] = C(l1 - 1,k2 - 1) - (suml + sgn*sumr);
          
          slaln2( TRUE, 2, 1, smin, ONE, &A(k1 - 1,k1 - 1), 
           lda, ONE, ONE, (float*)vec, 2, -sgn*B(l1 - 1,l1 - 1), 
           ZERO, (float*)x, 2, scaloc, xnorm, ierr );
          if( ierr != 0 ) 
            info = 1;
          
          if( scaloc != ONE ) { 
            for( j = 1, j_ = j - 1, _do9 = n; j <= _do9; j++, j_++ ) { 
              sscal( m, scaloc, &C(j_,0), 1 );
            }
            scale = scale*scaloc;
          }
          C(l1 - 1,k1 - 1) = x[0][0];
          C(l1 - 1,k2 - 1) = x[0][1];
          
        }
        else if( l1 != l2 && k1 == k2 ) { 
          
          suml = sdot( k1 - 1, &A(k1 - 1,0), 1, &C(l1 - 1,0), 
           1 );
          sumr = sdot( l1 - 1, &C(0,k1 - 1), ldc, &B(l1 - 1,0), 
           1 );
          vec[0][0] = sgn*(C(l1 - 1,k1 - 1) - (suml + sgn*
           sumr));
          
          suml = sdot( k1 - 1, &A(k1 - 1,0), 1, &C(l2 - 1,0), 
           1 );
          sumr = sdot( l1 - 1, &C(0,k1 - 1), ldc, &B(l2 - 1,0), 
           1 );
          vec[0][1] = sgn*(C(l2 - 1,k1 - 1) - (suml + sgn*
           sumr));
          
          slaln2( TRUE, 2, 1, smin, ONE, &B(l1 - 1,l1 - 1), 
           ldb, ONE, ONE, (float*)vec, 2, -sgn*A(k1 - 1,k1 - 1), 
           ZERO, (float*)x, 2, scaloc, xnorm, ierr );
          if( ierr != 0 ) 
            info = 1;
          
          if( scaloc != ONE ) { 
            for( j = 1, j_ = j - 1, _do10 = n; j <= _do10; j++, j_++ ) { 
              sscal( m, scaloc, &C(j_,0), 1 );
            }
            scale = scale*scaloc;
          }
          C(l1 - 1,k1 - 1) = x[0][0];
          C(l2 - 1,k1 - 1) = x[0][1];
          
        }
        else if( l1 != l2 && k1 != k2 ) { 
          
          suml = sdot( k1 - 1, &A(k1 - 1,0), 1, &C(l1 - 1,0), 
           1 );
          sumr = sdot( l1 - 1, &C(0,k1 - 1), ldc, &B(l1 - 1,0), 
           1 );
          vec[0][0] = C(l1 - 1,k1 - 1) - (suml + sgn*sumr);
          
          suml = sdot( k1 - 1, &A(k1 - 1,0), 1, &C(l2 - 1,0), 
           1 );
          sumr = sdot( l1 - 1, &C(0,k1 - 1), ldc, &B(l2 - 1,0), 
           1 );
          vec[1][0] = C(l2 - 1,k1 - 1) - (suml + sgn*sumr);
          
          suml = sdot( k1 - 1, &A(k2 - 1,0), 1, &C(l1 - 1,0), 
           1 );
          sumr = sdot( l1 - 1, &C(0,k2 - 1), ldc, &B(l1 - 1,0), 
           1 );
          vec[0][1] = C(l1 - 1,k2 - 1) - (suml + sgn*sumr);
          
          suml = sdot( k1 - 1, &A(k2 - 1,0), 1, &C(l2 - 1,0), 
           1 );
          sumr = sdot( l1 - 1, &C(0,k2 - 1), ldc, &B(l2 - 1,0), 
           1 );
          vec[1][1] = C(l2 - 1,k2 - 1) - (suml + sgn*sumr);
          
          slasy2( TRUE, FALSE, isgn, 2, 2, &A(k1 - 1,k1 - 1), 
           lda, &B(l1 - 1,l1 - 1), ldb, (float*)vec, 2, 
           scaloc, (float*)x, 2, xnorm, ierr );
          if( ierr != 0 ) 
            info = 1;
          
          if( scaloc != ONE ) { 
            for( j = 1, j_ = j - 1, _do11 = n; j <= _do11; j++, j_++ ) { 
              sscal( m, scaloc, &C(j_,0), 1 );
            }
            scale = scale*scaloc;
          }
          C(l1 - 1,k1 - 1) = x[0][0];
          C(l2 - 1,k1 - 1) = x[1][0];
          C(l1 - 1,k2 - 1) = x[0][1];
          C(l2 - 1,k2 - 1) = x[1][1];
        }
        
L_120:
        ;
      }
L_130:
      ;
    }
    
  }
  else if( !notrna && !notrnb ) { 
    
    //        Solve    A'*X + ISGN*X*B' = scale*C.
    
    //        The (K,L)th block of X is determined starting from
    //        top-right corner column by column by
    
    //           A(K,K)'*X(K,L) + ISGN*X(K,L)*B(L,L)' = C(K,L) - R(K,L)
    
    //        Where
    //                     K-1                          N
    //            R(K,L) = SUM [A(I,K)'*X(I,L)] + ISGN*SUM [X(K,J)*B(L,J)'].
    //                     I=1                        J=L+1
    
    //        Start column loop (index = L)
    //        L1 (L2): column index of the first (last) row of X(K,L)
    
    lnext = n;
    for( l = n, l_ = l - 1; l >= 1; l--, l_-- ) { 
      if( l > lnext ) 
        goto L_190;
      if( l == 1 ) { 
        l1 = l;
        l2 = l;
      }
      else { 
        if( B(l_ - 1,l_) != ZERO ) { 
          l1 = l - 1;
          l2 = l;
          lnext = l - 2;
        }
        else { 
          l1 = l;
          l2 = l;
          lnext = l - 1;
        }
      }
      
      //           Start row loop (index = K)
      //           K1 (K2): row index of the first (last) row of X(K,L)
      
      knext = 1;
      for( k = 1, k_ = k - 1, _do12 = m; k <= _do12; k++, k_++ ) { 
        if( k < knext ) 
          goto L_180;
        if( k == m ) { 
          k1 = k;
          k2 = k;
        }
        else { 
          if( A(k_,k_ + 1) != ZERO ) { 
            k1 = k;
            k2 = k + 1;
            knext = k + 2;
          }
          else { 
            k1 = k;
            k2 = k;
            knext = k + 1;
          }
        }
        
        if( l1 == l2 && k1 == k2 ) { 
          suml = sdot( k1 - 1, &A(k1 - 1,0), 1, &C(l1 - 1,0), 
           1 );
          sumr = sdot( n - l1, &C(min( l1 + 1, n ) - 1,k1 - 1), 
           ldc, &B(min( l1 + 1, n ) - 1,l1 - 1), ldb );
          vec[0][0] = C(l1 - 1,k1 - 1) - (suml + sgn*sumr);
          scaloc = ONE;
          
          a11 = A(k1 - 1,k1 - 1) + sgn*B(l1 - 1,l1 - 1);
          da11 = abs( a11 );
          if( da11 <= smin ) { 
            a11 = smin;
            da11 = smin;
            info = 1;
          }
          db = abs( vec[0][0] );
          if( da11 < ONE && db > ONE ) { 
            if( db > bignum*da11 ) 
              scaloc = ONE/db;
          }
          x[0][0] = (vec[0][0]*scaloc)/a11;
          
          if( scaloc != ONE ) { 
            for( j = 1, j_ = j - 1, _do13 = n; j <= _do13; j++, j_++ ) { 
              sscal( m, scaloc, &C(j_,0), 1 );
            }
            scale = scale*scaloc;
          }
          C(l1 - 1,k1 - 1) = x[0][0];
          
        }
        else if( l1 == l2 && k1 != k2 ) { 
          
          suml = sdot( k1 - 1, &A(k1 - 1,0), 1, &C(l1 - 1,0), 
           1 );
          sumr = sdot( n - l2, &C(min( l2 + 1, n ) - 1,k1 - 1), 
           ldc, &B(min( l2 + 1, n ) - 1,l1 - 1), ldb );
          vec[0][0] = C(l1 - 1,k1 - 1) - (suml + sgn*sumr);
          
          suml = sdot( k1 - 1, &A(k2 - 1,0), 1, &C(l1 - 1,0), 
           1 );
          sumr = sdot( n - l2, &C(min( l2 + 1, n ) - 1,k2 - 1), 
           ldc, &B(min( l2 + 1, n ) - 1,l1 - 1), ldb );
          vec[0][1] = C(l1 - 1,k2 - 1) - (suml + sgn*sumr);
          
          slaln2( TRUE, 2, 1, smin, ONE, &A(k1 - 1,k1 - 1), 
           lda, ONE, ONE, (float*)vec, 2, -sgn*B(l1 - 1,l1 - 1), 
           ZERO, (float*)x, 2, scaloc, xnorm, ierr );
          if( ierr != 0 ) 
            info = 1;
          
          if( scaloc != ONE ) { 
            for( j = 1, j_ = j - 1, _do14 = n; j <= _do14; j++, j_++ ) { 
              sscal( m, scaloc, &C(j_,0), 1 );
            }
            scale = scale*scaloc;
          }
          C(l1 - 1,k1 - 1) = x[0][0];
          C(l1 - 1,k2 - 1) = x[0][1];
          
        }
        else if( l1 != l2 && k1 == k2 ) { 
          
          suml = sdot( k1 - 1, &A(k1 - 1,0), 1, &C(l1 - 1,0), 
           1 );
          sumr = sdot( n - l2, &C(min( l2 + 1, n ) - 1,k1 - 1), 
           ldc, &B(min( l2 + 1, n ) - 1,l1 - 1), ldb );
          vec[0][0] = sgn*(C(l1 - 1,k1 - 1) - (suml + sgn*
           sumr));
          
          suml = sdot( k1 - 1, &A(k1 - 1,0), 1, &C(l2 - 1,0), 
           1 );
          sumr = sdot( n - l2, &C(min( l2 + 1, n ) - 1,k1 - 1), 
           ldc, &B(min( l2 + 1, n ) - 1,l2 - 1), ldb );
          vec[0][1] = sgn*(C(l2 - 1,k1 - 1) - (suml + sgn*
           sumr));
          
          slaln2( FALSE, 2, 1, smin, ONE, &B(l1 - 1,l1 - 1), 
           ldb, ONE, ONE, (float*)vec, 2, -sgn*A(k1 - 1,k1 - 1), 
           ZERO, (float*)x, 2, scaloc, xnorm, ierr );
          if( ierr != 0 ) 
            info = 1;
          
          if( scaloc != ONE ) { 
            for( j = 1, j_ = j - 1, _do15 = n; j <= _do15; j++, j_++ ) { 
              sscal( m, scaloc, &C(j_,0), 1 );
            }
            scale = scale*scaloc;
          }
          C(l1 - 1,k1 - 1) = x[0][0];
          C(l2 - 1,k1 - 1) = x[0][1];
          
        }
        else if( l1 != l2 && k1 != k2 ) { 
          
          suml = sdot( k1 - 1, &A(k1 - 1,0), 1, &C(l1 - 1,0), 
           1 );
          sumr = sdot( n - l2, &C(min( l2 + 1, n ) - 1,k1 - 1), 
           ldc, &B(min( l2 + 1, n ) - 1,l1 - 1), ldb );
          vec[0][0] = C(l1 - 1,k1 - 1) - (suml + sgn*sumr);
          
          suml = sdot( k1 - 1, &A(k1 - 1,0), 1, &C(l2 - 1,0), 
           1 );
          sumr = sdot( n - l2, &C(min( l2 + 1, n ) - 1,k1 - 1), 
           ldc, &B(min( l2 + 1, n ) - 1,l2 - 1), ldb );
          vec[1][0] = C(l2 - 1,k1 - 1) - (suml + sgn*sumr);
          
          suml = sdot( k1 - 1, &A(k2 - 1,0), 1, &C(l1 - 1,0), 
           1 );
          sumr = sdot( n - l2, &C(min( l2 + 1, n ) - 1,k2 - 1), 
           ldc, &B(min( l2 + 1, n ) - 1,l1 - 1), ldb );
          vec[0][1] = C(l1 - 1,k2 - 1) - (suml + sgn*sumr);
          
          suml = sdot( k1 - 1, &A(k2 - 1,0), 1, &C(l2 - 1,0), 
           1 );
          sumr = sdot( n - l2, &C(min( l2 + 1, n ) - 1,k2 - 1), 
           ldc, &B(min( l2 + 1, n ) - 1,l2 - 1), ldb );
          vec[1][1] = C(l2 - 1,k2 - 1) - (suml + sgn*sumr);
          
          slasy2( TRUE, TRUE, isgn, 2, 2, &A(k1 - 1,k1 - 1), 
           lda, &B(l1 - 1,l1 - 1), ldb, (float*)vec, 2, 
           scaloc, (float*)x, 2, xnorm, ierr );
          if( ierr != 0 ) 
            info = 1;
          
          if( scaloc != ONE ) { 
            for( j = 1, j_ = j - 1, _do16 = n; j <= _do16; j++, j_++ ) { 
              sscal( m, scaloc, &C(j_,0), 1 );
            }
            scale = scale*scaloc;
          }
          C(l1 - 1,k1 - 1) = x[0][0];
          C(l2 - 1,k1 - 1) = x[1][0];
          C(l1 - 1,k2 - 1) = x[0][1];
          C(l2 - 1,k2 - 1) = x[1][1];
        }
        
L_180:
        ;
      }
L_190:
      ;
    }
    
  }
  else if( notrna && !notrnb ) { 
    
    //        Solve    A*X + ISGN*X*B' = scale*C.
    
    //        The (K,L)th block of X is determined starting from
    //        bottom-right corner column by column by
    
    //            A(K,K)*X(K,L) + ISGN*X(K,L)*B(L,L)' = C(K,L) - R(K,L)
    
    //        Where
    //                      M                          N
    //            R(K,L) = SUM [A(K,I)*X(I,L)] + ISGN*SUM [X(K,J)*B(L,J)'].
    //                    I=K+1                      J=L+1
    
    //        Start column loop (index = L)
    //        L1 (L2): column index of the first (last) row of X(K,L)
    
    lnext = n;
    for( l = n, l_ = l - 1; l >= 1; l--, l_-- ) { 
      if( l > lnext ) 
        goto L_250;
      if( l == 1 ) { 
        l1 = l;
        l2 = l;
      }
      else { 
        if( B(l_ - 1,l_) != ZERO ) { 
          l1 = l - 1;
          l2 = l;
          lnext = l - 2;
        }
        else { 
          l1 = l;
          l2 = l;
          lnext = l - 1;
        }
      }
      
      //           Start row loop (index = K)
      //           K1 (K2): row index of the first (last) row of X(K,L)
      
      knext = m;
      for( k = m, k_ = k - 1; k >= 1; k--, k_-- ) { 
        if( k > knext ) 
          goto L_240;
        if( k == 1 ) { 
          k1 = k;
          k2 = k;
        }
        else { 
          if( A(k_ - 1,k_) != ZERO ) { 
            k1 = k - 1;
            k2 = k;
            knext = k - 2;
          }
          else { 
            k1 = k;
            k2 = k;
            knext = k - 1;
          }
        }
        
        if( l1 == l2 && k1 == k2 ) { 
          suml = sdot( m - k1, &A(min( k1 + 1, m ) - 1,k1 - 1), 
           lda, &C(l1 - 1,min( k1 + 1, m ) - 1), 1 );
          sumr = sdot( n - l1, &C(min( l1 + 1, n ) - 1,k1 - 1), 
           ldc, &B(min( l1 + 1, n ) - 1,l1 - 1), ldb );
          vec[0][0] = C(l1 - 1,k1 - 1) - (suml + sgn*sumr);
          scaloc = ONE;
          
          a11 = A(k1 - 1,k1 - 1) + sgn*B(l1 - 1,l1 - 1);
          da11 = abs( a11 );
          if( da11 <= smin ) { 
            a11 = smin;
            da11 = smin;
            info = 1;
          }
          db = abs( vec[0][0] );
          if( da11 < ONE && db > ONE ) { 
            if( db > bignum*da11 ) 
              scaloc = ONE/db;
          }
          x[0][0] = (vec[0][0]*scaloc)/a11;
          
          if( scaloc != ONE ) { 
            for( j = 1, j_ = j - 1, _do17 = n; j <= _do17; j++, j_++ ) { 
              sscal( m, scaloc, &C(j_,0), 1 );
            }
            scale = scale*scaloc;
          }
          C(l1 - 1,k1 - 1) = x[0][0];
          
        }
        else if( l1 == l2 && k1 != k2 ) { 
          
          suml = sdot( m - k2, &A(min( k2 + 1, m ) - 1,k1 - 1), 
           lda, &C(l1 - 1,min( k2 + 1, m ) - 1), 1 );
          sumr = sdot( n - l2, &C(min( l2 + 1, n ) - 1,k1 - 1), 
           ldc, &B(min( l2 + 1, n ) - 1,l1 - 1), ldb );
          vec[0][0] = C(l1 - 1,k1 - 1) - (suml + sgn*sumr);
          
          suml = sdot( m - k2, &A(min( k2 + 1, m ) - 1,k2 - 1), 
           lda, &C(l1 - 1,min( k2 + 1, m ) - 1), 1 );
          sumr = sdot( n - l2, &C(min( l2 + 1, n ) - 1,k2 - 1), 
           ldc, &B(min( l2 + 1, n ) - 1,l1 - 1), ldb );
          vec[0][1] = C(l1 - 1,k2 - 1) - (suml + sgn*sumr);
          
          slaln2( FALSE, 2, 1, smin, ONE, &A(k1 - 1,k1 - 1), 
           lda, ONE, ONE, (float*)vec, 2, -sgn*B(l1 - 1,l1 - 1), 
           ZERO, (float*)x, 2, scaloc, xnorm, ierr );
          if( ierr != 0 ) 
            info = 1;
          
          if( scaloc != ONE ) { 
            for( j = 1, j_ = j - 1, _do18 = n; j <= _do18; j++, j_++ ) { 
              sscal( m, scaloc, &C(j_,0), 1 );
            }
            scale = scale*scaloc;
          }
          C(l1 - 1,k1 - 1) = x[0][0];
          C(l1 - 1,k2 - 1) = x[0][1];
          
        }
        else if( l1 != l2 && k1 == k2 ) { 
          
          suml = sdot( m - k1, &A(min( k1 + 1, m ) - 1,k1 - 1), 
           lda, &C(l1 - 1,min( k1 + 1, m ) - 1), 1 );
          sumr = sdot( n - l2, &C(min( l2 + 1, n ) - 1,k1 - 1), 
           ldc, &B(min( l2 + 1, n ) - 1,l1 - 1), ldb );
          vec[0][0] = sgn*(C(l1 - 1,k1 - 1) - (suml + sgn*
           sumr));
          
          suml = sdot( m - k1, &A(min( k1 + 1, m ) - 1,k1 - 1), 
           lda, &C(l2 - 1,min( k1 + 1, m ) - 1), 1 );
          sumr = sdot( n - l2, &C(min( l2 + 1, n ) - 1,k1 - 1), 
           ldc, &B(min( l2 + 1, n ) - 1,l2 - 1), ldb );
          vec[0][1] = sgn*(C(l2 - 1,k1 - 1) - (suml + sgn*
           sumr));
          
          slaln2( FALSE, 2, 1, smin, ONE, &B(l1 - 1,l1 - 1), 
           ldb, ONE, ONE, (float*)vec, 2, -sgn*A(k1 - 1,k1 - 1), 
           ZERO, (float*)x, 2, scaloc, xnorm, ierr );
          if( ierr != 0 ) 
            info = 1;
          
          if( scaloc != ONE ) { 
            for( j = 1, j_ = j - 1, _do19 = n; j <= _do19; j++, j_++ ) { 
              sscal( m, scaloc, &C(j_,0), 1 );
            }
            scale = scale*scaloc;
          }
          C(l1 - 1,k1 - 1) = x[0][0];
          C(l2 - 1,k1 - 1) = x[0][1];
          
        }
        else if( l1 != l2 && k1 != k2 ) { 
          
          suml = sdot( m - k2, &A(min( k2 + 1, m ) - 1,k1 - 1), 
           lda, &C(l1 - 1,min( k2 + 1, m ) - 1), 1 );
          sumr = sdot( n - l2, &C(min( l2 + 1, n ) - 1,k1 - 1), 
           ldc, &B(min( l2 + 1, n ) - 1,l1 - 1), ldb );
          vec[0][0] = C(l1 - 1,k1 - 1) - (suml + sgn*sumr);
          
          suml = sdot( m - k2, &A(min( k2 + 1, m ) - 1,k1 - 1), 
           lda, &C(l2 - 1,min( k2 + 1, m ) - 1), 1 );
          sumr = sdot( n - l2, &C(min( l2 + 1, n ) - 1,k1 - 1), 
           ldc, &B(min( l2 + 1, n ) - 1,l2 - 1), ldb );
          vec[1][0] = C(l2 - 1,k1 - 1) - (suml + sgn*sumr);
          
          suml = sdot( m - k2, &A(min( k2 + 1, m ) - 1,k2 - 1), 
           lda, &C(l1 - 1,min( k2 + 1, m ) - 1), 1 );
          sumr = sdot( n - l2, &C(min( l2 + 1, n ) - 1,k2 - 1), 
           ldc, &B(min( l2 + 1, n ) - 1,l1 - 1), ldb );
          vec[0][1] = C(l1 - 1,k2 - 1) - (suml + sgn*sumr);
          
          suml = sdot( m - k2, &A(min( k2 + 1, m ) - 1,k2 - 1), 
           lda, &C(l2 - 1,min( k2 + 1, m ) - 1), 1 );
          sumr = sdot( n - l2, &C(min( l2 + 1, n ) - 1,k2 - 1), 
           ldc, &B(min( l2 + 1, n ) - 1,l2 - 1), ldb );
          vec[1][1] = C(l2 - 1,k2 - 1) - (suml + sgn*sumr);
          
          slasy2( FALSE, TRUE, isgn, 2, 2, &A(k1 - 1,k1 - 1), 
           lda, &B(l1 - 1,l1 - 1), ldb, (float*)vec, 2, 
           scaloc, (float*)x, 2, xnorm, ierr );
          if( ierr != 0 ) 
            info = 1;
          
          if( scaloc != ONE ) { 
            for( j = 1, j_ = j - 1, _do20 = n; j <= _do20; j++, j_++ ) { 
              sscal( m, scaloc, &C(j_,0), 1 );
            }
            scale = scale*scaloc;
          }
          C(l1 - 1,k1 - 1) = x[0][0];
          C(l2 - 1,k1 - 1) = x[1][0];
          C(l1 - 1,k2 - 1) = x[0][1];
          C(l2 - 1,k2 - 1) = x[1][1];
        }
        
L_240:
        ;
      }
L_250:
      ;
    }
    
  }
  
  return;
  
  //     End of STRSYL
  
#undef  C
#undef  B
#undef  A
} // end of function 


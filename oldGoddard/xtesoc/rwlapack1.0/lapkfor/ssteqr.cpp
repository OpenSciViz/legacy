/*
 * C++ implementation of Lapack routine ssteqr
 *
 * $Id: ssteqr.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:02:45
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: ssteqr.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ZERO = 0.0;
const float ONE = 1.0;
const float TWO = 2.0;
const long MAXIT = 30;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ ssteqr(const char &compz, const long &n, float d[], float e[], 
 float *z, const long &ldz, float work[], long &info)
{
#define Z(I_,J_)  (*(z+(I_)*(ldz)+(J_)))
  long _do0, _do1, _do2, _do3, _do4, _do5, _do6, i, i_, icompz, 
   ii, ii_, j, j_, jtot, k, l, l1, lend, lendm1, lendp1, lm1, m, 
   m_, mm, mm1, nconv, nm1, nmaxit;
  float b, c, eps, f, g, p, r, rt1, rt2, s, tst;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SSTEQR computes all eigenvalues and, optionally, eigenvectors of a
  //  symmetric tridiagonal matrix using the implicit QL or QR method.
  //  The eigenvectors of a full or band symmetric matrix can also be found
  //  if SSYTRD or SSPTRD or SSBTRD has been used to reduce this matrix to
  //  tridiagonal form.
  
  //  Arguments
  //  =========
  
  //  COMPZ   (input) CHARACTER*1
  //          Specifies whether eigenvectors are to be computed
  //          as follows
  
  //             COMPZ = 'N' or 'n'     Compute eigenvalues only.
  
  //             COMPZ = 'V' or 'v'     Compute eigenvectors of original
  //                                    symmetric matrix also.
  //                                    Array Z contains the orthogonal
  //                                    matrix used to reduce the original
  //                                    matrix to tridiagonal form.
  
  //             COMPZ = 'I' or 'i'     Compute eigenvectors of
  //                                    tridiagonal matrix also.
  
  //  N       (input) INTEGER
  //          The number of rows and columns in the matrix.  N >= 0.
  
  //  D       (input/output) REAL array, dimension (N)
  //          On entry, D contains the diagonal elements of the
  //          tridiagonal matrix.
  //          On exit, D contains the eigenvalues, in ascending order.
  //          If an error exit is made, the eigenvalues are correct
  //          for indices 1,2,...,INFO-1, but they are unordered and
  //          may not be the smallest eigenvalues of the matrix.
  
  //  E       (input/output) REAL array, dimension (N)
  //          On entry, E contains the subdiagonal elements of the
  //          tridiagonal matrix in positions 1 through N-1.
  //          E(N) is arbitrary.
  //          On exit, E has been destroyed.
  
  //  Z       (input/output) REAL array, dimension (LDZ, N)
  //          If  COMPZ = 'V' or 'v', then:
  //          On entry, Z contains the orthogonal matrix used in the
  //          reduction to tridiagonal form.
  //          If  COMPZ = 'V' or 'v' or 'I' or 'i', then:
  //          On exit, Z contains the orthonormal eigenvectors of the
  //          symmetric tridiagonal (or full) matrix.  If an error exit
  //          is made, Z contains the eigenvectors associated with the
  //          stored eigenvalues.
  
  //          If  COMPZ = 'N' or 'n', then Z is not referenced.
  
  //  LDZ     (input) INTEGER
  //          The leading dimension of the array Z.  If eigenvectors are
  //          desired, then  LDZ >= max( 1, N ).  In any case,  LDZ >= 1.
  
  //  WORK    (workspace) REAL array, dimension (max(1,2*N-2))
  //          Workspace used in computing eigenvectors.
  //          If  COMPZ = 'N' or 'n', then WORK is not referenced.
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit.
  //          < 0:  if INFO = -i, the i-th argument had an illegal value.
  //          > 0:  if INFO = +i, the i-th eigenvalue has not converged
  //                              after a total of  30*N  iterations.
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  
  if( lsame( compz, 'N' ) ) { 
    icompz = 0;
  }
  else if( lsame( compz, 'V' ) ) { 
    icompz = 1;
  }
  else if( lsame( compz, 'I' ) ) { 
    icompz = 2;
  }
  else { 
    icompz = -1;
  }
  if( icompz < 0 ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( (ldz < 1) || (icompz > 0 && ldz < max( 1, n )) ) { 
    info = -6;
  }
  if( info != 0 ) { 
    xerbla( "SSTEQR", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  if( n == 1 ) { 
    if( icompz > 0 ) 
      Z(0,0) = ONE;
    return;
  }
  
  //     Determine the unit roundoff for this environment.
  
  eps = slamch( 'E' );
  
  //     Compute the eigenvalues and eigenvectors of the tridiagonal
  //     matrix.
  
  if( icompz == 2 ) 
    slazro( n, n, ZERO, ONE, z, ldz );
  
  e[n - 1] = ZERO;
  nmaxit = n*MAXIT;
  jtot = 0;
  nconv = 0;
  
  //     Determine where the matrix splits and choose QL or QR iteration
  //     for each block, according to whether top or bottom diagonal
  //     element is smaller.
  
  l1 = 1;
  nm1 = n - 1;
  
L_10:
  ;
  if( l1 > n ) 
    goto L_150;
  if( l1 <= nm1 ) { 
    for( m = l1, m_ = m - 1, _do0 = nm1; m <= _do0; m++, m_++ ) { 
      tst = abs( e[m_] );
      if( tst <= eps*(abs( d[m_] ) + abs( d[m_ + 1] )) ) 
        goto L_30;
    }
  }
  m = n;
  
L_30:
  ;
  l = l1;
  lend = m;
  if( abs( d[lend - 1] ) < abs( d[l - 1] ) ) { 
    l = lend;
    lend = l1;
  }
  l1 = m + 1;
  
  if( lend >= l ) { 
    
    //        QL Iteration
    
    //        Look for small subdiagonal element.
    
L_40:
    ;
    if( l != lend ) { 
      lendm1 = lend - 1;
      for( m = l, m_ = m - 1, _do1 = lendm1; m <= _do1; m++, m_++ ) { 
        tst = abs( e[m_] );
        if( tst <= eps*(abs( d[m_] ) + abs( d[m_ + 1] )) ) 
          goto L_60;
      }
    }
    
    m = lend;
    
L_60:
    ;
    p = d[l - 1];
    if( m == l ) 
      goto L_80;
    
    //        If remaining matrix is 2 by 2, use SLAE2 or SLAEV2
    //        to compute its eigensystem.
    
    if( m == l + 1 ) { 
      if( icompz > 0 ) { 
        slaev2( d[l - 1], e[l - 1], d[l], rt1, rt2, c, s );
        work[l - 1] = c;
        work[n + l - 2] = s;
        slasr( 'R', 'V', 'B', n, 2, &work[l - 1], &work[n + l - 2], 
         &Z(l - 1,0), ldz );
      }
      else { 
        slae2( d[l - 1], e[l - 1], d[l], rt1, rt2 );
      }
      d[l - 1] = rt1;
      d[l] = rt2;
      nconv = nconv + 2;
      l = l + 2;
      if( l <= lend ) 
        goto L_40;
      goto L_10;
    }
    
    if( jtot == nmaxit ) 
      goto L_140;
    jtot = jtot + 1;
    
    //        Form shift.
    
    g = (d[l] - p)/(TWO*e[l - 1]);
    r = slapy2( g, ONE );
    g = d[m - 1] - p + (e[l - 1]/(g + sign( r, g )));
    
    s = ONE;
    c = ONE;
    p = ZERO;
    
    //        Inner loop
    
    mm1 = m - 1;
    for( i = mm1, i_ = i - 1, _do2 = l; i >= _do2; i--, i_-- ) { 
      f = s*e[i_];
      b = c*e[i_];
      slartg( g, f, c, s, r );
      e[i_ + 1] = r;
      g = d[i_ + 1] - p;
      r = (d[i_] - g)*s + TWO*c*b;
      p = s*r;
      d[i_ + 1] = g + p;
      g = c*r - b;
      
      //           If eigenvectors are desired, then save rotations.
      
      if( icompz > 0 ) { 
        work[i_] = c;
        work[n - 1 + i_] = -s;
      }
      
    }
    
    //        If eigenvectors are desired, then apply saved rotations.
    
    if( icompz > 0 ) { 
      mm = m - l + 1;
      slasr( 'R', 'V', 'B', n, mm, &work[l - 1], &work[n + l - 2], 
       &Z(l - 1,0), ldz );
    }
    
    d[l - 1] = d[l - 1] - p;
    e[l - 1] = g;
    e[m - 1] = ZERO;
    goto L_40;
    
    //        Eigenvalue found.
    
L_80:
    ;
    d[l - 1] = p;
    nconv = nconv + 1;
    
    l = l + 1;
    if( l <= lend ) 
      goto L_40;
    goto L_10;
    
  }
  else { 
    
    //        QR Iteration
    
    //        Look for small superdiagonal element.
    
L_90:
    ;
    if( l != lend ) { 
      lendp1 = lend + 1;
      for( m = l, m_ = m - 1, _do3 = lendp1; m >= _do3; m--, m_-- ) { 
        tst = abs( e[m_ - 1] );
        if( tst <= eps*(abs( d[m_] ) + abs( d[m_ - 1] )) ) 
          goto L_110;
      }
    }
    
    m = lend;
    
L_110:
    ;
    p = d[l - 1];
    if( m == l ) 
      goto L_130;
    
    //        If remaining matrix is 2 by 2, use SLAE2 or SLAEV2
    //        to compute its eigensystem.
    
    if( m == l - 1 ) { 
      if( icompz > 0 ) { 
        slaev2( d[l - 2], e[l - 2], d[l - 1], rt1, rt2, c, 
         s );
        work[m - 1] = c;
        work[n + m - 2] = s;
        slasr( 'R', 'V', 'F', n, 2, &work[m - 1], &work[n + m - 2], 
         &Z(l - 2,0), ldz );
      }
      else { 
        slae2( d[l - 2], e[l - 2], d[l - 1], rt1, rt2 );
      }
      d[l - 2] = rt1;
      d[l - 1] = rt2;
      nconv = nconv + 2;
      l = l - 2;
      if( l >= lend ) 
        goto L_90;
      goto L_10;
    }
    
    if( jtot == nmaxit ) 
      goto L_140;
    jtot = jtot + 1;
    
    //        Form shift.
    
    g = (d[l - 2] - p)/(TWO*e[l - 2]);
    r = slapy2( g, ONE );
    g = d[m - 1] - p + (e[l - 2]/(g + sign( r, g )));
    
    s = ONE;
    c = ONE;
    p = ZERO;
    
    //        Inner loop
    
    lm1 = l - 1;
    for( i = m, i_ = i - 1, _do4 = lm1; i <= _do4; i++, i_++ ) { 
      f = s*e[i_];
      b = c*e[i_];
      slartg( g, f, c, s, r );
      if( i != 1 ) 
        e[i_ - 1] = r;
      g = d[i_] - p;
      r = (d[i_ + 1] - g)*s + TWO*c*b;
      p = s*r;
      d[i_] = g + p;
      g = c*r - b;
      
      //           If eigenvectors are desired, then save rotations.
      
      if( icompz > 0 ) { 
        work[i_] = c;
        work[n - 1 + i_] = s;
      }
      
    }
    
    //        If eigenvectors are desired, then apply saved rotations.
    
    if( icompz > 0 ) { 
      mm = l - m + 1;
      slasr( 'R', 'V', 'F', n, mm, &work[m - 1], &work[n + m - 2], 
       &Z(m - 1,0), ldz );
    }
    
    d[l - 1] = d[l - 1] - p;
    e[lm1 - 1] = g;
    if( m != 1 ) 
      e[m - 2] = ZERO;
    goto L_90;
    
    //        Eigenvalue found.
    
L_130:
    ;
    d[l - 1] = p;
    nconv = nconv + 1;
    
    l = l - 1;
    if( l >= lend ) 
      goto L_90;
    goto L_10;
    
  }
  
  //     Set error -- no convergence to an eigenvalue after a total
  //     of N*MAXIT iterations.
  
L_140:
  ;
  info = nconv;
  return;
  
  //     Order eigenvalues and eigenvectors.
  
L_150:
  ;
  for( ii = 2, ii_ = ii - 1, _do5 = n; ii <= _do5; ii++, ii_++ ) { 
    i = ii - 1;
    k = i;
    p = d[i - 1];
    for( j = ii, j_ = j - 1, _do6 = n; j <= _do6; j++, j_++ ) { 
      if( d[j_] < p ) { 
        k = j;
        p = d[j_];
      }
    }
    if( k != i ) { 
      d[k - 1] = d[i - 1];
      d[i - 1] = p;
      if( icompz > 0 ) 
        sswap( n, &Z(i - 1,0), 1, &Z(k - 1,0), 1 );
    }
  }
  
  return;
  
  //     End of SSTEQR
  
#undef  Z
} // end of function 


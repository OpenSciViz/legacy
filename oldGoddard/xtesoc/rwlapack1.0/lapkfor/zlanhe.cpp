/*
 * C++ implementation of Lapack routine zlanhe
 *
 * $Id: zlanhe.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:48:34
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zlanhe.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ONE = 1.0e0;
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL double /*FUNCTION*/ zlanhe(const char &norm, const char &uplo, const long &n, DComplex *a, 
 const long &lda, double work[])
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  long _do0, _do1, _do10, _do11, _do12, _do2, _do3, _do4, _do5, 
   _do6, _do7, _do8, _do9, i, i_, j, j_;
  double absa, scale, sum, value, zlanhe_v;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZLANHE  returns the value of the one norm,  or the Frobenius norm, or
  //  the  infinity norm,  or the  element of  largest absolute value  of a
  //  DComplex hermitian matrix A.
  
  //  Description
  //  ===========
  
  //  ZLANHE returns the value
  
  //     ZLANHE = ( max(abs(A(i,j))), NORM = 'M' or 'm'
  //              (
  //              ( norm1(A),         NORM = '1', 'O' or 'o'
  //              (
  //              ( normI(A),         NORM = 'I' or 'i'
  //              (
  //              ( normF(A),         NORM = 'F', 'f', 'E' or 'e'
  
  //  where  norm1  denotes the  one norm of a matrix (maximum column sum),
  //  normI  denotes the  infinity norm  of a matrix  (maximum row sum) and
  //  normF  denotes the  Frobenius norm of a matrix (square root of sum of
  //  squares).  Note that  max(abs(A(i,j)))  is not a  matrix norm.
  
  //  Arguments
  //  =========
  
  //  NORM    (input) CHARACTER*1
  //          Specifies the value to be returned in ZLANHE as described
  //          above.
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangular part of the
  //          hermitian matrix A is to be referenced.
  //          = 'U':  Upper triangular part of A is referenced
  //          = 'L':  Lower triangular part of A is referenced
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.  When N = 0, ZLANHE is
  //          set to zero.
  
  //  A       (input) COMPLEX*16 array, dimension (LDA,N)
  //          The hermitian matrix A.  If UPLO = 'U', the leading n by n
  //          upper triangular part of A contains the upper triangular part
  //          of the matrix A, and the strictly lower triangular part of A
  //          is not referenced.  If UPLO = 'L', the leading n by n lower
  //          triangular part of A contains the lower triangular part of
  //          the matrix A, and the strictly upper triangular part of A is
  //          not referenced. Note that the imaginary parts of the diagonal
  //          elements need not be set and are assumed to be zero.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(N,1).
  
  //  WORK    (workspace) DOUBLE PRECISION array, dimension (LWORK),
  //          where LWORK >= N when NORM = 'I' or '1' or 'O'; otherwise,
  //          WORK is not referenced.
  
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  if( n == 0 ) { 
    value = ZERO;
  }
  else if( lsame( norm, 'M' ) ) { 
    
    //        Find max(abs(A(i,j))).
    
    value = ZERO;
    if( lsame( uplo, 'U' ) ) { 
      for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
        for( i = 1, i_ = i - 1, _do1 = j - 1; i <= _do1; i++, i_++ ) { 
          value = max( value, abs( A(j_,i_) ) );
        }
        value = max( value, abs( real( A(j_,j_) ) ) );
      }
    }
    else { 
      for( j = 1, j_ = j - 1, _do2 = n; j <= _do2; j++, j_++ ) { 
        value = max( value, abs( real( A(j_,j_) ) ) );
        for( i = j + 1, i_ = i - 1, _do3 = n; i <= _do3; i++, i_++ ) { 
          value = max( value, abs( A(j_,i_) ) );
        }
      }
    }
  }
  else if( ((lsame( norm, 'I' )) || (lsame( norm, 'O' ))) || (norm == 
   '1') ) { 
    
    //        Find normI(A) ( = norm1(A), since A is hermitian).
    
    value = ZERO;
    if( lsame( uplo, 'U' ) ) { 
      for( j = 1, j_ = j - 1, _do4 = n; j <= _do4; j++, j_++ ) { 
        sum = ZERO;
        for( i = 1, i_ = i - 1, _do5 = j - 1; i <= _do5; i++, i_++ ) { 
          absa = abs( A(j_,i_) );
          sum = sum + absa;
          work[i_] = work[i_] + absa;
        }
        work[j_] = sum + abs( real( A(j_,j_) ) );
      }
      for( i = 1, i_ = i - 1, _do6 = n; i <= _do6; i++, i_++ ) { 
        value = max( value, work[i_] );
      }
    }
    else { 
      for( i = 1, i_ = i - 1, _do7 = n; i <= _do7; i++, i_++ ) { 
        work[i_] = ZERO;
      }
      for( j = 1, j_ = j - 1, _do8 = n; j <= _do8; j++, j_++ ) { 
        sum = work[j_] + abs( real( A(j_,j_) ) );
        for( i = j + 1, i_ = i - 1, _do9 = n; i <= _do9; i++, i_++ ) { 
          absa = abs( A(j_,i_) );
          sum = sum + absa;
          work[i_] = work[i_] + absa;
        }
        value = max( value, sum );
      }
    }
  }
  else if( (lsame( norm, 'F' )) || (lsame( norm, 'E' )) ) { 
    
    //        Find normF(A).
    
    scale = ZERO;
    sum = ONE;
    if( lsame( uplo, 'U' ) ) { 
      for( j = 2, j_ = j - 1, _do10 = n; j <= _do10; j++, j_++ ) { 
        zlassq( j - 1, &A(j_,0), 1, scale, sum );
      }
    }
    else { 
      for( j = 1, j_ = j - 1, _do11 = n - 1; j <= _do11; j++, j_++ ) { 
        zlassq( n - j, &A(j_,j_ + 1), 1, scale, sum );
      }
    }
    sum = 2*sum;
    for( i = 1, i_ = i - 1, _do12 = n; i <= _do12; i++, i_++ ) { 
      if( real( A(i_,i_) ) != ZERO ) { 
        absa = abs( real( A(i_,i_) ) );
        if( scale < absa ) { 
          sum = ONE + sum*pow(scale/absa, 2);
          scale = absa;
        }
        else { 
          sum = sum + pow(absa/scale, 2);
        }
      }
    }
    value = scale*sqrt( sum );
  }
  
  zlanhe_v = value;
  return( zlanhe_v );
  
  //     End of ZLANHE
  
#undef  A
} // end of function 


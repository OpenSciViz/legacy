/*
 * C++ implementation of Lapack routine sgelss
 *
 * $Id: sgelss.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:58:38
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: sgelss.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ZERO = 0.0e0;
const float ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ sgelss(const long &m, const long &n, const long &nrhs, 
 float *a, const long &lda, float *b, const long &ldb, float s[], const float &rcond, 
 const long &rank, float work[], const long &lwork, long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
  long _do0, _do1, _do2, _do3, _do4, _do5, _do6, _do7, _do8, 
   bl, chunk, i, i_, iascl, ibscl, ie, il, itau, itaup, itauq, iwork, 
   ldwork, maxmn, maxwrk, minmn, minwrk, mm, mnthr;
  float anrm, bignum, bnrm, eps, sfmin, smlnum, thr, vdum[1];

  
  //  -- LAPACK driver routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SGELSS uses the singular value decomposition of A to solve the least
  //  squares problem of finding X to minimize the Euclidean norm of each
  //  column of A*X-B, where A is M by N, and X and B are N by NRHS.
  //  If M is greater than or equal to N the problem is overdetermined, and
  //  if M is less than N the problem is underdetermined; in this case a
  //  minimum norm solution is returned.  The solution X overwrites B.
  
  //  The singular values of A smaller than RCOND times the largest
  //  singular value are treated as zero in solving the least squares
  //  problem; in this case a minimum norm solution is returned.
  //  The actual singular values are returned in S in decreasing order.
  
  //  Arguments
  //  =========
  
  //  M       (input) INTEGER
  //          The number of rows of A. M must be at least 0.
  
  //  N       (input) INTEGER
  //          The number of columns of A. N must be at least 0.
  
  //  NRHS    (input) INTEGER
  //          The number of columns of B and X. NRHS must be at least 0.
  
  //  A       (input/output) REAL array, dimension (LDA,N)
  //          On input, the matrix specifying the least squares problem.
  //          On output, A has been overwritten with its right singular
  //          vectors.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of A in the calling subprogram.
  //          LDA must be at least max(1,M).
  
  //  B       (input/output) REAL array, dimension (LDB,NRHS)
  //          On input, B contains the right hand sides of the least
  //          squares problem in rows 1 through M. On output, B contains
  //          the solution X in rows 1 through N.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of B in the calling subprogram.
  //          LDB must be at least max(1,MAX( M, N ) ).
  
  //  S       (output) REAL array, dimension (min(M,N))
  //          On output, contains the singular values of A in decreasing
  //          order.
  
  //  RCOND   (input) REAL
  //          The singular values of A less than or equal to RCOND times
  //          the largest singular value are treated as zero in solving
  //          the least squares problem. If RCOND is negative,
  //          machine precision is used instead.
  //          For example, if diag(S)*X=B were the least squares problem,
  //          where diag(S) is a diagonal matrix of singular values, the
  //          solution would be X(i) = B(i) / S(i) if S(i) is greater than
  //          RCOND*max(S), and X(i) = 0 if S(i) is less than or equal to
  //          RCOND*max(S).
  
  //  RANK    (output) INTEGER
  //          The number of singular values of A greater than RCOND times
  //          the largest singular value.
  
  //  WORK    (workspace/output) REAL array, dimension (LWORK)
  //          On exit, WORK(1) contains the optimal workspace size LWORK
  //          for high performance.
  
  //  LWORK   (input) INTEGER
  //          The dimension of the array WORK. LWORK must be at least 1.
  //          The exact minimum amount of workspace needed depends on M,
  //          N and NRHS. As long as LWORK is at least
  //              3*N+MAX(2*N,NRHS,M) if M is greater than or equal to N
  //              3*M+MAX(2*M,NRHS,N) otherwise
  //          the code will execute correctly.
  //          For good performance, LWORK should generally be larger.
  //          The optimum value of LWORK for high performance is
  //          returned in WORK(1).
  
  //  INFO    (output) INTEGER
  //          0  - successful exit
  //          <0 - if INFO = -i, the i-th argument had an illegal value.
  //          >0 - if SBDSQR did not converge, INFO specifies how many
  //               superdiagonals of an intermediate bidiagonal form
  //               did not converge to zero.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Local Arrays ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input arguments
  
  info = 0;
  minmn = min( m, n );
  maxmn = max( m, n );
  mnthr = ilaenv( 6, "SGELSS", " ", m, n, nrhs, -1 );
  if( m < 0 ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( nrhs < 0 ) { 
    info = -3;
  }
  else if( lda < max( 1, m ) ) { 
    info = -5;
  }
  else if( ldb < max( 1, maxmn ) ) { 
    info = -7;
  }
  
  //     Compute workspace
  //      (Note: Comments in the code beginning "Workspace:" describe the
  //       minimal amount of workspace needed at that point in the code,
  //       as well as the preferred amount for good performance.
  //       NB refers to the optimal block size for the immediately
  //       following subroutine, as returned by ILAENV.)
  
  minwrk = 1;
  if( info == 0 && lwork >= 1 ) { 
    maxwrk = 0;
    mm = m;
    if( m >= n && m >= mnthr ) { 
      
      //           Path 1a - overdetermined, with many more rows than columns
      
      mm = n;
      maxwrk = max( maxwrk, n + n*ilaenv( 1, "SGEQRF", " ", 
       m, n, -1, -1 ) );
      maxwrk = max( maxwrk, n + nrhs*ilaenv( 1, "SORMQR", "LT"
       , m, nrhs, n, -1 ) );
    }
    if( m >= n ) { 
      
      //           Path 1 - overdetermined or exactly determined
      
      maxwrk = max( maxwrk, 3*n + (mm + n)*ilaenv( 1, "SGEBRD"
       , " ", mm, n, -1, -1 ) );
      maxwrk = max( maxwrk, 3*n + nrhs*ilaenv( 1, "SORMBR", 
       "QLT", mm, nrhs, n, -1 ) );
      maxwrk = max( maxwrk, 3*n + (n - 1)*ilaenv( 1, "SORGBR"
       , "P", n, n, n, -1 ) );
      maxwrk = max( maxwrk, 5*n - 4 );
      maxwrk = max( maxwrk, n*nrhs );
      minwrk = vmax( 3*n + mm, 3*n + nrhs, 5*n - 4, IEND );
    }
    if( n > m ) { 
      minwrk = vmax( 3*m + nrhs, 3*m + n, 5*m - 4, IEND );
      if( n >= mnthr ) { 
        
        //              Path 2a - underdetermined, with many more columns
        //              than rows
        
        maxwrk = m + m*ilaenv( 1, "SGELQF", " ", m, n, -1, 
         -1 );
        maxwrk = max( maxwrk, m*m + 4*m + 2*m*ilaenv( 1, "SGEBRD"
         , " ", m, m, -1, -1 ) );
        maxwrk = max( maxwrk, m*m + 4*m + nrhs*ilaenv( 1, 
         "SORMBR", "QLT", m, nrhs, m, -1 ) );
        maxwrk = max( maxwrk, m*m + 4*m + (m - 1)*ilaenv( 1, 
         "SORGBR", "P", m, m, m, -1 ) );
        maxwrk = max( maxwrk, m*m + 6*m - 4 );
        if( nrhs > 1 ) { 
          maxwrk = max( maxwrk, m*m + m + m*nrhs );
        }
        else { 
          maxwrk = max( maxwrk, m*m + 2*m );
        }
        maxwrk = max( maxwrk, m + nrhs*ilaenv( 1, "SORMLQ"
         , "LT", n, nrhs, m, -1 ) );
      }
      else { 
        
        //              Path 2 - underdetermined
        
        maxwrk = 3*m + (n + m)*ilaenv( 1, "SGEBRD", " ", m, 
         n, -1, -1 );
        maxwrk = max( maxwrk, 3*m + nrhs*ilaenv( 1, "SORMBR"
         , "QLT", m, nrhs, m, -1 ) );
        maxwrk = max( maxwrk, 3*m + m*ilaenv( 1, "SORGBR", 
         "P", m, n, m, -1 ) );
        maxwrk = max( maxwrk, 5*m - 4 );
        maxwrk = max( maxwrk, n*nrhs );
      }
    }
    minwrk = min( minwrk, maxwrk );
    work[0] = maxwrk;
  }
  
  minwrk = max( minwrk, 1 );
  if( lwork < minwrk ) 
    info = -12;
  if( info != 0 ) { 
    xerbla( "SGELSS", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( m == 0 || n == 0 ) { 
    rank = 0;
    return;
  }
  
  //     Get machine parameters
  
  eps = slamch( 'P' );
  sfmin = slamch( 'S' );
  smlnum = sfmin/eps;
  bignum = ONE/smlnum;
  slabad( smlnum, bignum );
  
  //     Scale A if max entry outside range [SMLNUM,BIGNUM]
  
  anrm = slange( 'M', m, n, a, lda, work );
  iascl = 0;
  if( anrm > ZERO && anrm < smlnum ) { 
    
    //        Scale matrix norm up to SMLNUM
    
    slascl( 'G', 0, 0, anrm, smlnum, m, n, a, lda, info );
    iascl = 1;
  }
  else if( anrm > bignum ) { 
    
    //        Scale matrix norm down to BIGNUM
    
    slascl( 'G', 0, 0, anrm, bignum, m, n, a, lda, info );
    iascl = 2;
  }
  else if( anrm == ZERO ) { 
    
    //        Matrix all zero. Return zero solution.
    
    slaset( 'F', max( m, n ), nrhs, ZERO, ZERO, b, ldb );
    slaset( 'F', minmn, 1, ZERO, ZERO, s, 1 );
    rank = 0;
    goto L_70;
  }
  
  //     Scale B if max entry outside range [SMLNUM,BIGNUM]
  
  bnrm = slange( 'M', m, nrhs, b, ldb, work );
  ibscl = 0;
  if( bnrm > ZERO && bnrm < smlnum ) { 
    
    //        Scale matrix norm up to SMLNUM
    
    slascl( 'G', 0, 0, bnrm, smlnum, m, nrhs, b, ldb, info );
    ibscl = 1;
  }
  else if( bnrm > bignum ) { 
    
    //        Scale matrix norm down to BIGNUM
    
    slascl( 'G', 0, 0, bnrm, bignum, m, nrhs, b, ldb, info );
    ibscl = 2;
  }
  
  //     Overdetermined case
  
  if( m >= n ) { 
    
    //        Path 1 - overdetermined or exactly determined
    
    mm = m;
    if( m >= mnthr ) { 
      
      //           Path 1a - overdetermined, with many more rows than columns
      
      mm = n;
      itau = 1;
      iwork = itau + n;
      
      //           Compute A=Q*R
      //           (Workspace: need 2*N, prefer N+N*NB)
      
      sgeqrf( m, n, a, lda, &work[itau - 1], &work[iwork - 1], 
       lwork - iwork + 1, info );
      
      //           Multiply B by transpose(Q)
      //           (Workspace: need N+NRHS, prefer N+NRHS*NB)
      
      sormqr( 'L', 'T', m, nrhs, n, a, lda, &work[itau - 1], 
       b, ldb, &work[iwork - 1], lwork - iwork + 1, info );
      
      //           Zero out below R
      
      if( n > 1 ) 
        slaset( 'L', n - 1, n - 1, ZERO, ZERO, &A(0,1), lda );
    }
    
    ie = 1;
    itauq = ie + n;
    itaup = itauq + n;
    iwork = itaup + n;
    
    //        Bidiagonalize R in A
    //        (Workspace: need 3*N+MM, prefer 3*N+(MM+N)*NB)
    
    sgebrd( mm, n, a, lda, s, &work[ie - 1], &work[itauq - 1], 
     &work[itaup - 1], &work[iwork - 1], lwork - iwork + 1, info );
    
    //        Multiply B by transpose of left bidiagonalizing vectors of R
    //        (Workspace: need 3*N+NRHS, prefer 3*N+NRHS*NB)
    
    sormbr( 'Q', 'L', 'T', mm, nrhs, n, a, lda, &work[itauq - 1], 
     b, ldb, &work[iwork - 1], lwork - iwork + 1, info );
    
    //        Generate right bidiagonalizing vectors of R in A
    //        (Workspace: need 4*N-1, prefer 3*N+(N-1)*NB)
    
    sorgbr( 'P', n, n, n, a, lda, &work[itaup - 1], &work[iwork - 1], 
     lwork - iwork + 1, info );
    iwork = ie + n;
    
    //        Perform bidiagonal QR iteration
    //          multiply B by transpose of left singular vectors
    //          compute right singular vectors in A
    //        (Workspace: need 5*N-4)
    
    sbdsqr( 'U', n, n, 0, nrhs, s, &work[ie - 1], a, lda, vdum, 
     1, b, ldb, &work[iwork - 1], info );
    if( info != 0 ) 
      goto L_70;
    
    //        Multiply B by reciprocals of singular values
    
    thr = max( rcond*s[0], sfmin );
    if( thr < ZERO ) 
      thr = max( eps*s[0], sfmin );
    rank = 0;
    for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
      if( s[i_] > thr ) { 
        srscl( nrhs, s[i_], &B(0,i_), ldb );
        rank = rank + 1;
      }
      else { 
        slaset( 'F', 1, nrhs, ZERO, ZERO, &B(0,i_), ldb );
      }
    }
    
    //        Multiply B by right singular vectors
    //        (Workspace: need N, prefer N*NRHS)
    
    if( lwork >= ldb*nrhs && nrhs > 1 ) { 
      sgemm( 'T', 'N', n, nrhs, n, ONE, a, lda, b, ldb, ZERO, 
       work, ldb );
      slacpy( 'G', n, nrhs, work, ldb, b, ldb );
    }
    else if( nrhs > 1 ) { 
      chunk = lwork/n;
      for( i = 1, i_ = i - 1, _do1=docnt(i,nrhs,_do2 = chunk); _do1 > 0; i += _do2, i_ += _do2, _do1-- ) { 
        bl = min( nrhs - i + 1, chunk );
        sgemm( 'T', 'N', n, bl, n, ONE, a, lda, b, ldb, ZERO, 
         work, n );
        slacpy( 'G', n, bl, work, n, b, ldb );
      }
    }
    else { 
      sgemv( 'T', n, n, ONE, a, lda, b, 1, ZERO, work, 1 );
      scopy( n, work, 1, b, 1 );
    }
    
  }
  else if( n >= mnthr && lwork >= 4*m + m*m + vmax( m, 2*m - 4, 
   nrhs, n - 3*m, IEND ) ) { 
    
    //        Path 2a - underdetermined, with many more columns than rows
    //        and sufficient workspace for an efficient algorithm
    
    ldwork = m;
    if( lwork >= max( 4*m + m*lda + vmax( m, 2*m - 4, nrhs, n - 
     3*m, IEND ), m*lda + m + m*nrhs ) ) 
      ldwork = lda;
    itau = 1;
    iwork = m + 1;
    
    //        Compute A=L*Q
    //        (Workspace: need 2*M, prefer M+M*NB)
    
    sgelqf( m, n, a, lda, &work[itau - 1], &work[iwork - 1], lwork - 
     iwork + 1, info );
    il = iwork;
    
    //        Copy L to WORK(IL), zeroing out above it
    
    slacpy( 'L', m, m, a, lda, &work[il - 1], ldwork );
    slaset( 'U', m - 1, m - 1, ZERO, ZERO, &work[il + ldwork - 1], 
     ldwork );
    ie = il + ldwork*m;
    itauq = ie + m;
    itaup = itauq + m;
    iwork = itaup + m;
    
    //        Bidiagonalize L in WORK(IL)
    //        (Workspace: need M*M+5*M, prefer M*M+4*M+2*M*NB)
    
    sgebrd( m, m, &work[il - 1], ldwork, s, &work[ie - 1], &work[itauq - 1], 
     &work[itaup - 1], &work[iwork - 1], lwork - iwork + 1, info );
    
    //        Multiply B by transpose of left bidiagonalizing vectors of L
    //        (Workspace: need M*M+4*M+NRHS, prefer M*M+4*M+NRHS*NB)
    
    sormbr( 'Q', 'L', 'T', m, nrhs, m, &work[il - 1], ldwork, 
     &work[itauq - 1], b, ldb, &work[iwork - 1], lwork - iwork + 
     1, info );
    
    //        Generate right bidiagonalizing vectors of R in WORK(IL)
    //        (Workspace: need M*M+5*M-1, prefer M*M+4*M+(M-1)*NB)
    
    sorgbr( 'P', m, m, m, &work[il - 1], ldwork, &work[itaup - 1], 
     &work[iwork - 1], lwork - iwork + 1, info );
    iwork = ie + m;
    
    //        Perform bidiagonal QR iteration,
    //           computing right singular vectors of L in WORK(IL) and
    //           multiplying B by transpose of left singular vectors
    //        (Workspace: need M*M+6*M-4)
    
    sbdsqr( 'U', m, m, 0, nrhs, s, &work[ie - 1], &work[il - 1], 
     ldwork, a, lda, b, ldb, &work[iwork - 1], info );
    if( info != 0 ) 
      goto L_70;
    
    //        Multiply B by reciprocals of singular values
    
    thr = max( rcond*s[0], sfmin );
    if( thr < ZERO ) 
      thr = max( eps*s[0], sfmin );
    rank = 0;
    for( i = 1, i_ = i - 1, _do3 = m; i <= _do3; i++, i_++ ) { 
      if( s[i_] > thr ) { 
        srscl( nrhs, s[i_], &B(0,i_), ldb );
        rank = rank + 1;
      }
      else { 
        slaset( 'F', 1, nrhs, ZERO, ZERO, &B(0,i_), ldb );
      }
    }
    iwork = ie;
    
    //        Multiply B by right singular vectors of L in WORK(IL)
    //        (Workspace: need M*M+2*M, prefer M*M+M+M*NRHS)
    
    if( lwork >= ldb*nrhs + iwork - 1 && nrhs > 1 ) { 
      sgemm( 'T', 'N', m, nrhs, m, ONE, &work[il - 1], ldwork, 
       b, ldb, ZERO, &work[iwork - 1], ldb );
      slacpy( 'G', m, nrhs, &work[iwork - 1], ldb, b, ldb );
    }
    else if( nrhs > 1 ) { 
      chunk = (lwork - iwork + 1)/m;
      for( i = 1, i_ = i - 1, _do4=docnt(i,nrhs,_do5 = chunk); _do4 > 0; i += _do5, i_ += _do5, _do4-- ) { 
        bl = min( nrhs - i + 1, chunk );
        sgemm( 'T', 'N', m, bl, m, ONE, &work[il - 1], ldwork, 
         &B(i_,0), ldb, ZERO, &work[iwork - 1], n );
        slacpy( 'G', m, bl, &work[iwork - 1], n, b, ldb );
      }
    }
    else { 
      sgemv( 'T', m, m, ONE, &work[il - 1], ldwork, &B(0,0), 
       1, ZERO, &work[iwork - 1], 1 );
      scopy( m, &work[iwork - 1], 1, &B(0,0), 1 );
    }
    
    //        Zero out below first M rows of B
    
    slaset( 'F', n - m, nrhs, ZERO, ZERO, &B(0,m), ldb );
    iwork = itau + m;
    
    //        Multiply transpose(Q) by B
    //        (Workspace: need M+NRHS, prefer M+NRHS*NB)
    
    sormlq( 'L', 'T', n, nrhs, m, a, lda, &work[itau - 1], b, 
     ldb, &work[iwork - 1], lwork - iwork + 1, info );
    
  }
  else { 
    
    //        Path 2 - remaining underdetermined cases
    
    ie = 1;
    itauq = ie + m;
    itaup = itauq + m;
    iwork = itaup + m;
    
    //        Bidiagonalize A
    //        (Workspace: need 3*M+N, prefer 3*M+(M+N)*NB)
    
    sgebrd( m, n, a, lda, s, &work[ie - 1], &work[itauq - 1], 
     &work[itaup - 1], &work[iwork - 1], lwork - iwork + 1, info );
    
    //        Multiply B by transpose of left bidiagonalizing vectors
    //        (Workspace: need 3*M+NRHS, prefer 3*M+NRHS*NB)
    
    sormbr( 'Q', 'L', 'T', m, nrhs, n, a, lda, &work[itauq - 1], 
     b, ldb, &work[iwork - 1], lwork - iwork + 1, info );
    
    //        Generate right bidiagonalizing vectors in A
    //        (Workspace: need 4*M, prefer 3*M+M*NB)
    
    sorgbr( 'P', m, n, m, a, lda, &work[itaup - 1], &work[iwork - 1], 
     lwork - iwork + 1, info );
    iwork = ie + m;
    
    //        Perform bidiagonal QR iteration,
    //           computing right singular vectors of A in A and
    //           multiplying B by transpose of left singular vectors
    //        (Workspace: need 5*M-4)
    
    sbdsqr( 'L', m, n, 0, nrhs, s, &work[ie - 1], a, lda, vdum, 
     1, b, ldb, &work[iwork - 1], info );
    if( info != 0 ) 
      goto L_70;
    
    //        Multiply B by reciprocals of singular values
    
    thr = max( rcond*s[0], sfmin );
    if( thr < ZERO ) 
      thr = max( eps*s[0], sfmin );
    rank = 0;
    for( i = 1, i_ = i - 1, _do6 = m; i <= _do6; i++, i_++ ) { 
      if( s[i_] > thr ) { 
        srscl( nrhs, s[i_], &B(0,i_), ldb );
        rank = rank + 1;
      }
      else { 
        slaset( 'F', 1, nrhs, ZERO, ZERO, &B(0,i_), ldb );
      }
    }
    
    //        Multiply B by right singular vectors of A
    //        (Workspace: need N, prefer N*NRHS)
    
    if( lwork >= ldb*nrhs && nrhs > 1 ) { 
      sgemm( 'T', 'N', n, nrhs, m, ONE, a, lda, b, ldb, ZERO, 
       work, ldb );
      slacpy( 'F', n, nrhs, work, ldb, b, ldb );
    }
    else if( nrhs > 1 ) { 
      chunk = lwork/n;
      for( i = 1, i_ = i - 1, _do7=docnt(i,nrhs,_do8 = chunk); _do7 > 0; i += _do8, i_ += _do8, _do7-- ) { 
        bl = min( nrhs - i + 1, chunk );
        sgemm( 'T', 'N', n, bl, m, ONE, a, lda, &B(i_,0), 
         ldb, ZERO, work, n );
        slacpy( 'F', n, bl, work, n, &B(i_,0), ldb );
      }
    }
    else { 
      sgemv( 'T', m, n, ONE, a, lda, b, 1, ZERO, work, 1 );
      scopy( n, work, 1, b, 1 );
    }
  }
  
  //     Undo scaling
  
  if( iascl == 1 ) { 
    slascl( 'G', 0, 0, anrm, smlnum, n, nrhs, b, ldb, info );
    slascl( 'G', 0, 0, smlnum, anrm, minmn, 1, s, minmn, info );
  }
  else if( iascl == 2 ) { 
    slascl( 'G', 0, 0, anrm, bignum, n, nrhs, b, ldb, info );
    slascl( 'G', 0, 0, bignum, anrm, minmn, 1, s, minmn, info );
  }
  if( ibscl == 1 ) { 
    slascl( 'G', 0, 0, smlnum, bnrm, n, nrhs, b, ldb, info );
  }
  else if( ibscl == 2 ) { 
    slascl( 'G', 0, 0, bignum, bnrm, n, nrhs, b, ldb, info );
  }
  
L_70:
  ;
  work[0] = maxwrk;
  return;
  
  //     End of SGELSS
  
#undef  B
#undef  A
} // end of function 


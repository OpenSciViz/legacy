/*
 * C++ implementation of Lapack routine sgesvx
 *
 * $Id: sgesvx.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:59:04
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: sgesvx.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ sgesvx(const char &fact, const char &trans, const long &n, const long &nrhs, 
 float *a, const long &lda, float *af, const long &ldaf, long ipiv[], 
 const char &equed, float r[], float c[], float *b, const long &ldb, float *x, 
 const long &ldx, const float &rcond, float ferr[], float berr[], float work[], 
 long iwork[], long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define AF(I_,J_) (*(af+(I_)*(ldaf)+(J_)))
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
#define X(I_,J_)  (*(x+(I_)*(ldx)+(J_)))
  int colequ, equil, nofact, notran, rowequ;
  char norm;
  long _do0, _do1, _do2, _do3, _do4, _do5, _do6, _do7, _do8, 
   _do9, i, i_, infequ, j, j_;
  float amax, anorm, colcnd, rowcnd;

  
  //  -- LAPACK driver routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SGESVX uses the LU factorization to compute the solution to a real
  //  system of linear equations
  //     A * X = B,
  //  where A is an N by N matrix and X and B are N by NRHS matrices.
  
  //  Error bounds on the solution and a condition estimate are also
  //  provided.
  
  //  Description
  //  ===========
  
  //  The following steps are performed by this subroutine:
  
  //  1. If FACT = 'E', real scaling factors are computed to equilibrate
  //     the system:
  //        TRANS = 'N':  diag(R)*A*diag(C)     *inv(diag(C))*X = diag(R)*B
  //        TRANS = 'T': (diag(R)*A*diag(C))**T *inv(diag(R))*X = diag(C)*B
  //        TRANS = 'C': (diag(R)*A*diag(C))**H *inv(diag(R))*X = diag(C)*B
  //     Whether or not the system will be equilibrated depends on the
  //     scaling of the matrix A, but if equilibration is used, A is
  //     overwritten by diag(R)*A*diag(C) and B by diag(R)*B (if TRANS='N')
  //     or diag(C)*B (if TRANS = 'T' or 'C').
  
  //  2. If FACT = 'N' or 'E', the LU decomposition is used to factor the
  //     matrix A (after equilibration if FACT = 'E') as
  //        A = P * L * U,
  //     where P is a permutation matrix, L is a unit lower triangular
  //     matrix, and U is upper triangular.
  
  //  3. The factored form of A is used to estimate the condition number
  //     of the matrix A.  If the reciprocal of the condition number is
  //     less than machine precision, steps 4-6 are skipped.
  
  //  4. The system of equations A*X = B is solved for X using the
  //     factored form of A.
  
  //  5. Iterative refinement is applied to improve the computed solution
  //     vectors and calculate error bounds and backward error estimates
  //     for them.
  
  //  6. If FACT = 'E' and equilibration was used, the vectors X are
  //     premultiplied by diag(C) (if TRANS = 'N') or diag(R) (if
  //     TRANS = 'T' or 'C') so that they solve the original system before
  //     equilibration.
  
  //  Arguments
  //  =========
  
  //  FACT    (input) CHARACTER*1
  //          Specifies whether or not the factored form of the matrix A is
  //          supplied on entry, and if not, whether the matrix A should be
  //          equilibrated before it is factored.
  //          = 'F':  On entry, AF and IPIV contain the factored form of A.
  //                  A, AF, and IPIV are not modified.
  //          = 'N':  The matrix A will be copied to AF and factored.
  //          = 'E':  The matrix A will be equilibrated if necessary, then
  //                  copied to AF and factored.
  
  //  TRANS   (input) CHARACTER*1
  //          Specifies the form of the system of equations.
  //          = 'N':  A * X = B     (No transpose)
  //          = 'T':  A**T * X = B  (Transpose)
  //          = 'C':  A**H * X = B  (Transpose)
  
  //  N       (input) INTEGER
  //          The number of linear equations, i.e., the order of the
  //          matrix A.  N >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right hand sides, i.e., the number of columns
  //          of the matrices B and X.  NRHS >= 0.
  
  //  A       (input/output) REAL array, dimension (LDA,N)
  //          On entry, the n-by-n matrix A.  A is not modified if
  //          FACT = 'F' or 'N', or if FACT = 'E' and EQUED = 'N' on exit.
  
  //          On exit, if EQUED .ne. 'N', A is scaled as follows:
  //          EQUED = 'R':  A := diag(R) * A
  //          EQUED = 'C':  A := A * diag(C)
  //          EQUED = 'B':  A := diag(R) * A * diag(C).
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  AF      (input or output) REAL array, dimension (LDAF,N)
  //          If FACT = 'F', then AF is an input argument and on entry
  //          contains the factors L and U from the factorization
  //          A = P*L*U as computed by SGETRF.
  
  //          If FACT = 'N', then AF is an output argument and on exit
  //          returns the factors L and U from the factorization A = P*L*U
  //          of the original matrix A.
  
  //          If FACT = 'E', then AF is an output argument and on exit
  //          returns the factors L and U from the factorization A = P*L*U
  //          of the equilibrated matrix A (see the description of A for
  //          the form of the equilibrated matrix).
  
  //  LDAF    (input) INTEGER
  //          The leading dimension of the array AF.  LDAF >= max(1,N).
  
  //  IPIV    (input or output) INTEGER array, dimension (N)
  //          If FACT = 'F', then IPIV is an input argument and on entry
  //          contains the pivot indices from the factorization A = P*L*U
  //          as computed by SGETRF; row i of the matrix was interchanged
  //          with row IPIV(i).
  
  //          If FACT = 'N', then IPIV is an output argument and on exit
  //          contains the pivot indices from the factorization A = P*L*U
  //          of the original matrix A.
  
  //          If FACT = 'E', then IPIV is an output argument and on exit
  //          contains the pivot indices from the factorization A = P*L*U
  //          of the equilibrated matrix A.
  
  //  EQUED   (output) CHARACTER*1
  //          Specifies the form of equilibration that was done.
  //          = 'N':  No equilibration  (always true if FACT = 'F' or 'N').
  //          = 'R':  Row equilibration, i.e., A has been premultiplied by
  //                  diag(R).
  //          = 'C':  Column equilibration, i.e., A has been postmultiplied
  //                  by diag(C).
  //          = 'B':  Both row and column equilibration, i.e., A has been
  //                  replaced by diag(R) * A * diag(C).
  
  //  R       (output) REAL array, dimension (N)
  //          The row scale factors for A.  If EQUED = 'R' or 'B', A is
  //          multiplied on the left by diag(R).  R is not assigned if
  //          FACT = 'F' or 'N'.
  
  //  C       (output) REAL array, dimension (N)
  //          The column scale factors for A.  If EQUED = 'C' or 'B', A is
  //          multiplied on the right by diag(C). C is not assigned if
  //          FACT = 'F' or 'N'.
  
  //  B       (input/output) REAL array, dimension (LDB,NRHS)
  //          On entry, the n-by-nrhs right-hand side matrix B.
  //          On exit, if EQUED = 'N', B is not modified; if TRANS = 'N'
  //          and EQUED = 'R' or 'B', B is overwritten by diag(R)*B; if
  //          TRANS = 'T' or 'C' and EQUED = 'C' or 'B', B is overwritten
  //          by diag(C)*B.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  X       (output) REAL array, dimension (LDX,NRHS)
  //          If INFO = 0, the n-by-nrhs solution matrix X to the original
  //          system of equations.  Note that A and B are modified on exit
  //          if EQUED .ne. 'N', and the solution to the equilibrated
  //          system is inv(diag(C))*X if TRANS = 'N' and EQUED = 'C' or
  //          'B', or inv(diag(R))*X if TRANS = 'T' or 'C' and EQUED = 'R'
  //          or 'B'.
  
  //  LDX     (input) INTEGER
  //          The leading dimension of the array X.  LDX >= max(1,N).
  
  //  RCOND   (output) REAL
  //          The estimate of the reciprocal condition number of the matrix
  //          A.  If RCOND is less than the machine precision (in
  //          particular, if RCOND = 0), the matrix is singular to working
  //          precision.  This condition is indicated by a return code of
  //          INFO > 0, and the solution and error bounds are not computed.
  
  //  FERR    (output) REAL array, dimension (NRHS)
  //          The estimated forward error bounds for each solution vector
  //          X(j) (the j-th column of solution matrix X).
  //          If XTRUE is the true solution, FERR(j) bounds the magnitude
  //          of the largest entry in (X(j) - XTRUE) divided by
  //          the magnitude of the largest entry in X(j).  The quality of
  //          the error bound depends on the quality of the estimate of
  //          norm(inv(A)) computed in the code; if the estimate of
  //          norm(inv(A)) is accurate, the error bound is guaranteed.
  
  //  BERR    (output) REAL array, dimension (NRHS)
  //          The componentwise relative backward error of each solution
  //          vector X(j) (i.e., the smallest relative change in
  //          any entry of A or B that makes X(j) an exact solution).
  
  //  WORK    (workspace) REAL array, dimension (3*N)
  
  //  IWORK   (workspace) INTEGER array, dimension (N)
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: if INFO = k <= N, U(k,k) is exactly zero, or if
  //               INFO = N+1, the factor U is nonsingular, but RCOND is
  //               less than machine precision.  The factorization has been
  //               completed, but the matrix A is singular to working
  //               precision, and the solution and error bounds have not
  //               been computed.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  nofact = lsame( fact, 'N' );
  equil = lsame( fact, 'E' );
  notran = lsame( trans, 'N' );
  if( (!nofact && !equil) && !lsame( fact, 'F' ) ) { 
    info = -1;
  }
  else if( (!notran && !lsame( trans, 'T' )) && !lsame( trans, 'C' )
    ) { 
    info = -2;
  }
  else if( n < 0 ) { 
    info = -3;
  }
  else if( nrhs < 0 ) { 
    info = -4;
  }
  else if( lda < max( 1, n ) ) { 
    info = -6;
  }
  else if( ldaf < max( 1, n ) ) { 
    info = -8;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -14;
  }
  else if( ldx < max( 1, n ) ) { 
    info = -16;
  }
  if( info != 0 ) { 
    xerbla( "SGESVX", -info );
    return;
  }
  
  equed = 'N';
  rowequ = FALSE;
  colequ = FALSE;
  if( equil ) { 
    
    //        Compute row and column scalings to equilibrate the matrix A.
    
    sgeequ( n, n, a, lda, r, c, rowcnd, colcnd, amax, infequ );
    if( infequ == 0 ) { 
      
      //           Equilibrate the matrix.
      
      slaqge( n, n, a, lda, r, c, rowcnd, colcnd, amax, equed );
      rowequ = lsame( equed, 'R' ) || lsame( equed, 'B' );
      colequ = lsame( equed, 'C' ) || lsame( equed, 'B' );
      
      //           Scale the right hand side.
      
      if( notran ) { 
        if( rowequ ) { 
          for( j = 1, j_ = j - 1, _do0 = nrhs; j <= _do0; j++, j_++ ) { 
            for( i = 1, i_ = i - 1, _do1 = n; i <= _do1; i++, i_++ ) { 
              B(j_,i_) = r[i_]*B(j_,i_);
            }
          }
        }
      }
      else if( colequ ) { 
        for( j = 1, j_ = j - 1, _do2 = nrhs; j <= _do2; j++, j_++ ) { 
          for( i = 1, i_ = i - 1, _do3 = n; i <= _do3; i++, i_++ ) { 
            B(j_,i_) = c[i_]*B(j_,i_);
          }
        }
      }
    }
  }
  
  if( nofact || equil ) { 
    
    //        Compute the LU factorization of A.
    
    slacpy( 'F'/*Full*/, n, n, a, lda, af, ldaf );
    sgetrf( n, n, af, ldaf, ipiv, info );
    
    //        Return if INFO is non-zero.
    
    if( info != 0 ) { 
      if( info > 0 ) 
        rcond = ZERO;
      return;
    }
  }
  
  //     Compute the norm of the matrix A.
  
  if( notran ) { 
    norm = '1';
  }
  else { 
    norm = 'I';
  }
  anorm = slange( norm, n, n, a, lda, work );
  
  //     Compute the reciprocal of the condition number of A.
  
  sgecon( norm, n, af, ldaf, anorm, rcond, work, iwork, info );
  
  //     Return if the matrix is singular to working precision.
  
  if( rcond < slamch( 'E'/*Epsilon*/ ) ) { 
    info = n + 1;
    return;
  }
  
  //     Compute the solution vectors X.
  
  slacpy( 'F'/*Full*/, n, nrhs, b, ldb, x, ldx );
  sgetrs( trans, n, nrhs, af, ldaf, ipiv, x, ldx, info );
  
  //     Use iterative refinement to improve the computed solutions and
  //     compute error bounds and backward error estimates for them.
  
  sgerfs( trans, n, nrhs, a, lda, af, ldaf, ipiv, b, ldb, x, ldx, 
   ferr, berr, work, iwork, info );
  
  //     Transform the solution vectors X to solutions of the original
  //     system.
  
  if( equil ) { 
    if( notran ) { 
      if( colequ ) { 
        for( j = 1, j_ = j - 1, _do4 = nrhs; j <= _do4; j++, j_++ ) { 
          for( i = 1, i_ = i - 1, _do5 = n; i <= _do5; i++, i_++ ) { 
            X(j_,i_) = c[i_]*X(j_,i_);
          }
        }
        for( j = 1, j_ = j - 1, _do6 = nrhs; j <= _do6; j++, j_++ ) { 
          ferr[j_] = ferr[j_]/colcnd;
        }
      }
    }
    else if( rowequ ) { 
      for( j = 1, j_ = j - 1, _do7 = nrhs; j <= _do7; j++, j_++ ) { 
        for( i = 1, i_ = i - 1, _do8 = n; i <= _do8; i++, i_++ ) { 
          X(j_,i_) = r[i_]*X(j_,i_);
        }
      }
      for( j = 1, j_ = j - 1, _do9 = nrhs; j <= _do9; j++, j_++ ) { 
        ferr[j_] = ferr[j_]/rowcnd;
      }
    }
  }
  
  return;
  
  //     End of SGESVX
  
#undef  X
#undef  B
#undef  AF
#undef  A
} // end of function 


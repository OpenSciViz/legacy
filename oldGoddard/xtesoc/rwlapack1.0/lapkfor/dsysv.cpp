/*
 * C++ implementation of lapack routine dsysv
 *
 * $Id: dsysv.cpp,v 1.5 1993/04/06 20:42:35 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:38:35
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dsysv.cpp,v $
 * Revision 1.5  1993/04/06  20:42:35  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:17:28  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:09:14  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

RWLAPKDECL void /*FUNCTION*/ dsysv(const char &uplo, const long &n, const long &nrhs, double *a, 
   const long &lda, long ipiv[], double *b, const long &ldb, double work[], 
   const long &lwork, long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))

  
  //  -- LAPACK driver routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DSYSV computes the solution to a real system of linear equations
  //     A * X = B,
  //  where A is an N by N symmetric matrix and X and B are N by NRHS
  //  matrices.
  
  //  The diagonal pivoting method is used to factor A as
  //     A = U * D * U',  if UPLO = 'U', or
  //     A = L * D * L',  if UPLO = 'L',
  //  where U (or L) is a product of permutation and unit upper (lower)
  //  triangular matrices, D is symmetric and block diagonal with 1-by-1
  //  and 2-by-2 diagonal blocks, and ' indicates transpose.  The factored
  //  form of A is then used to solve the system of equations A * X = B.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangular part of the
  //          symmetric matrix A is stored:
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The number of linear equations, i.e., the order of the
  //          matrix A.  N >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right hand sides, i.e., the number of columns
  //          of the matrix B.  NRHS >= 0.
  
  //  A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)
  //          On entry, the symmetric matrix A.  If UPLO = 'U', the leading
  //          N by N upper triangular part of A contains the upper
  //          triangular part of the matrix A, and the strictly lower
  //          triangular part of A is not referenced.  If UPLO = 'L', the
  //          leading N by N lower triangular part of A contains the lower
  //          triangular part of the matrix A, and the strictly upper
  //          triangular part of A is not referenced.
  
  //          On exit, if INFO = 0, the block diagonal matrix D and the
  //          multipliers used to obtain the factor U or L from the
  //          factorization A = U*D*U' or A = L*D*L' as computed by DSYTRF.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  IPIV    (output) INTEGER array, dimension (N)
  //          Details of the interchanges and the block structure of D, as
  //          determined by DSYTRF.  If IPIV(k) > 0, then rows and columns
  //          k and IPIV(k) were interchanged, and D(k,k) is a 1-by-1
  //          diagonal block.  If UPLO = 'U' and IPIV(k) = IPIV(k-1) < 0,
  //          then rows and columns k-1 and -IPIV(k) were interchanged and
  //          D(k-1:k,k-1:k) is a 2-by-2 diagonal block.  If UPLO = 'L' and
  //          IPIV(k) = IPIV(k+1) < 0, then rows and columns k+1 and
  //          -IPIV(k) were interchanged and D(k:k+1,k:k+1) is a 2-by-2
  //          diagonal block.
  
  //  B       (input/output) DOUBLE PRECISION array, dimension (LDB,NRHS)
  //          On entry, the N by NRHS matrix of right hand side vectors B
  //          for the system of equations A*X = B.
  //          On exit, if INFO = 0, the N by NRHS matrix of solution
  //          vectors X.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  WORK    (workspace) DOUBLE PRECISION array, dimension (LWORK)
  
  //  LWORK   (input) INTEGER
  //          The length of WORK.  LWORK must be at least 1, and for best
  //          performance should be at least N*NB, where NB is the optimal
  //          blocksize for DSYTRF.  On exit, if INFO = 0, WORK(1) returns
  //          N*NB, the minimum value of LWORK required to use the optimal
  //          blocksize.
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: if INFO = k, D(k,k) is exactly zero.  The factorization
  //               has been completed, but the block diagonal matrix D is
  //               exactly singular, so the solution could not be computed.
  
  //  =====================================================================
  
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  if( !lsame( uplo, 'U' ) && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( nrhs < 0 ) { 
    info = -3;
  }
  else if( lda < max( 1, n ) ) { 
    info = -5;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -8;
  }
  else if( lwork < 1 ) { 
    info = -10;
  }
  if( info != 0 ) { 
    xerbla( "DSYSV ", -info );
    return;
  }
  
  //     Compute the factorization A = U*D*U' or A = L*D*L'.
  
  dsytrf( uplo, n, a, lda, ipiv, work, lwork, info );
  if( info == 0 ) { 
    
    //        Solve the system A*X = B, overwriting B with X.
    
    dsytrs( uplo, n, nrhs, a, lda, ipiv, b, ldb, info );
    
  }
  return;
  
  //     End of DSYSV
  
#undef  B
#undef  A
} // end of function 


/*
 * C++ implementation of lapack routine dgetrs
 *
 * $Id: dgetrs.cpp,v 1.6 1993/04/06 20:40:46 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:34:42
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dgetrs.cpp,v $
 * Revision 1.6  1993/04/06  20:40:46  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.5  1993/03/19  18:41:23  alv
 * now passes chars explicitly, rather than indirection of a string, to shut up SUN warnings
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:14:57  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:06:56  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dgetrs(const char &trans, const long &n, const long &nrhs, 
 double *a, const long &lda, long ipiv[], double *b, const long &ldb, 
 long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
  int notran;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DGETRS solves a system of linear equations
  //     A * X = B  or  A' * X = B
  //  with a general n by n matrix A using the LU factorization computed
  //  by DGETRF.
  
  //  Arguments
  //  =========
  
  //  TRANS   (input) CHARACTER*1
  //          Specifies the form of the system of equations.
  //          = 'N':  A * X = B  (No transpose)
  //          = 'T':  A'* X = B  (Transpose)
  //          = 'C':  A'* X = B  (Conjugate transpose = Transpose)
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right hand sides, i.e., the number of columns
  //          of the matrix B.  NRHS >= 0.
  
  //  A       (input) DOUBLE PRECISION array, dimension (LDA,N)
  //          The factors L and U from the factorization A = P*L*U
  //          as computed by DGETRF.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  IPIV    (input) INTEGER array, dimension (N)
  //          The pivot indices from DGETRF; for 1<=i<=N, row i of the
  //          matrix was interchanged with row IPIV(i).
  
  //  B       (input/output) DOUBLE PRECISION array, dimension (LDB,NRHS)
  //          On entry, the right hand side vectors B for the system of
  //          linear equations.
  //          On exit, the solution vectors, X.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  notran = lsame( trans, 'N' );
  if( (!notran && !lsame( trans, 'T' )) && !lsame( trans, 'C' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( nrhs < 0 ) { 
    info = -3;
  }
  else if( lda < max( 1, n ) ) { 
    info = -5;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -8;
  }
  if( info != 0 ) { 
    xerbla( "DGETRS", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 || nrhs == 0 ) 
    return;
  
  if( notran ) { 
    
    //        Solve A * X = B.
    
    //        Apply row interchanges to the right hand sides.
    
    dlaswp( nrhs, b, ldb, 1, n, ipiv, 1 );
    
    //        Solve L*X = B, overwriting B with X.
    
    dtrsm( 'L'/* Left */, 'L'/* Lower */, 'N'/* No transpose */, 
     'U'/* Unit */, n, nrhs, ONE, a, lda, b, ldb );
    
    //        Solve U*X = B, overwriting B with X.
    
    dtrsm( 'L'/* Left */, 'U'/* Upper */, 'N'/* No transpose */, 
     'N'/* Non-unit */, n, nrhs, ONE, a, lda, b, ldb );
  }
  else { 
    
    //        Solve A' * X = B.
    
    //        Solve U'*X = B, overwriting B with X.
    
    dtrsm( 'L'/* Left */, 'U'/* Upper */, 'T'/* Transpose */, 'N'/* Non-unit */
     , n, nrhs, ONE, a, lda, b, ldb );
    
    //        Solve L'*X = B, overwriting B with X.
    
    dtrsm( 'L'/* Left */, 'L'/* Lower */, 'T'/* Transpose */, 'U'/* Unit */
     , n, nrhs, ONE, a, lda, b, ldb );
    
    //        Apply row interchanges to the solution vectors.
    
    dlaswp( nrhs, b, ldb, 1, n, ipiv, -1 );
  }
  
  return;
  
  //     End of DGETRS
  
#undef  B
#undef  A
} // end of function 


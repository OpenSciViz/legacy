/*
 * C++ implementation of Lapack routine zptcon
 *
 * $Id: zptcon.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:50:16
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zptcon.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ONE = 1.0e0;
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zptcon(const long &n, double d[], DComplex e[], const double &anorm, 
 double &rcond, double rwork[], long &info)
{
  long _do0, _do1, i, i_, ix;
  double ainvnm;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZPTCON estimates the reciprocal of the condition number of a DComplex
  //  Hermitian positive definite tridiagonal matrix using the
  //  factorization A = L*D*L' computed by ZPTTRF.
  
  //  An estimate is obtained for norm(inv(A)), and the reciprocal of the
  //  condition number is computed as RCOND = 1 / (ANORM * norm(inv(A))).
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  D       (input) DOUBLE PRECISION array, dimension (N)
  //          The n real diagonal elements of the diagonal matrix D from
  //          the L*D*L' factorization of A.
  
  //  E       (input) COMPLEX*16 array, dimension (N-1)
  //          The (n-1) subdiagonal elements of the unit bidiagonal factor
  //          L from the L*D*L' factorization of A.  E can also be regarded
  //          as the superdiagonal of the unit bidiagonal factor U from the
  //          factorization A = U'*D*U.
  
  //  ANORM   (input) DOUBLE PRECISION
  //          The 1-norm of the original matrix A.
  
  //  RCOND   (output) DOUBLE PRECISION
  //          The reciprocal of the condition number of the matrix A,
  //          computed as RCOND = 1/(ANORM * AINVNM), where AINVNM is an
  //          estimate of the 1-norm of inv(A) computed in this routine.
  
  //  RWORK   (workspace) DOUBLE PRECISION array, dimension (N)
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  
  //  Further Details
  //  ===============
  
  //  The method used is described in Nicholas J. Higham, "Efficient
  //  Algorithms for Computing the Condition Number of a Tridiagonal
  //  Matrix", SIAM J. Sci. Stat. Comput., Vol. 7, No. 1, January 1986.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input arguments.
  
  info = 0;
  if( n < 0 ) { 
    info = -1;
  }
  else if( anorm < ZERO ) { 
    info = -4;
  }
  if( info != 0 ) { 
    xerbla( "ZPTCON", -info );
    return;
  }
  
  //     Quick return if possible
  
  rcond = ZERO;
  if( n == 0 ) { 
    rcond = ONE;
    return;
  }
  else if( anorm == ZERO ) { 
    return;
  }
  
  //     Check that D(1:N) is positive.
  
  for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
    if( d[i_] <= ZERO ) 
      return;
  }
  
  //     Solve M(A) * x = e, where M(A) = (m(i,j)) is given by
  
  //        m(i,j) =  abs(A(i,j)), i = j,
  //        m(i,j) = -abs(A(i,j)), i .ne. j,
  
  //     and e = [ 1, 1, ..., 1 ]'.  Note M(A) = M(L)*D*M(L)'.
  
  //     Solve M(L) * x = e.
  
  rwork[0] = ONE;
  for( i = 2, i_ = i - 1, _do1 = n; i <= _do1; i++, i_++ ) { 
    rwork[i_] = ONE + rwork[i_ - 1]*abs( e[i_ - 1] );
  }
  
  //     Solve D * M(L)' * x = b.
  
  rwork[n - 1] = rwork[n - 1]/d[n - 1];
  for( i = n - 1, i_ = i - 1; i >= 1; i--, i_-- ) { 
    rwork[i_] = rwork[i_]/d[i_] + rwork[i_ + 1]*abs( e[i_] );
  }
  
  //     Compute AINVNM = max(x(i)), 1<=i<=n.
  
  ix = idamax( n, rwork, 1 );
  ainvnm = abs( rwork[ix - 1] );
  
  //     Compute the estimate of the reciprocal condition number.
  
  if( ainvnm != ZERO ) 
    rcond = (ONE/ainvnm)/anorm;
  
  return;
  
  //     End of ZPTCON
  
} // end of function 


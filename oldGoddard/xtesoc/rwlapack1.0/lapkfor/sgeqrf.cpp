/*
 * C++ implementation of Lapack routine sgeqrf
 *
 * $Id: sgeqrf.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:58:47
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: sgeqrf.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

RWLAPKDECL void /*FUNCTION*/ sgeqrf(const long &m, const long &n, float *a, const long &lda, 
   float tau[], float work[], const long &lwork, long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  long _do0, _do1, i, i_, ib, iinfo, iws, k, ldwork, nb, nbmin, 
   nx;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SGEQRF computes a QR factorization of a real m by n matrix A:
  //  A = Q * R.
  
  //  Arguments
  //  =========
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix A.  M >= 0.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix A.  N >= 0.
  
  //  A       (input/output) REAL array, dimension (LDA,N)
  //          On entry, the m by n matrix A.
  //          On exit, the elements on and above the diagonal of the array
  //          contain the min(m,n) by n upper trapezoidal matrix R (R is
  //          upper triangular if m >= n); the elements below the diagonal,
  //          with the array TAU, represent the orthogonal matrix Q as a
  //          product of elementary reflectors (see Further Details).
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,M).
  
  //  TAU     (output) REAL array, dimension (min(M,N))
  //          The scalar factors of the elementary reflectors (see Further
  //          Details).
  
  //  WORK    (workspace) REAL array, dimension (LWORK)
  //          On exit, if INFO = 0, WORK(1) returns the minimum value of
  //          LWORK required to use the optimal blocksize.
  
  //  LWORK   (input) INTEGER
  //          The dimension of the array WORK.  LWORK >= max(1,N).
  //          For optimum performance LWORK should be at least N*NB,
  //          where NB is the optimal blocksize.
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument had an illegal value
  
  //  Further Details
  //  ===============
  
  //  The matrix Q is represented as a product of elementary reflectors
  
  //     Q = H(1) H(2) . . . H(k), where k = min(m,n).
  
  //  Each H(i) has the form
  
  //     H(i) = I - tau * v * v'
  
  //  where tau is a real scalar, and v is a real vector with
  //  v(1:i-1) = 0 and v(i) = 1; v(i+1:m) is stored on exit in A(i+1:m,i),
  //  and tau in TAU(i).
  
  //  =====================================================================
  
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input arguments
  
  info = 0;
  if( m < 0 ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( lda < max( 1, m ) ) { 
    info = -4;
  }
  else if( lwork < max( 1, n ) ) { 
    info = -7;
  }
  if( info != 0 ) { 
    xerbla( "SGEQRF", -info );
    return;
  }
  
  //     Quick return if possible
  
  k = min( m, n );
  if( k == 0 ) { 
    work[0] = 1;
    return;
  }
  
  //     Determine the block size.
  
  nb = ilaenv( 1, "SGEQRF", " ", m, n, -1, -1 );
  nbmin = 2;
  nx = 0;
  iws = n;
  if( nb > 1 && nb < k ) { 
    
    //        Determine when to cross over from blocked to unblocked code.
    
    nx = max( 0, ilaenv( 3, "SGEQRF", " ", m, n, -1, -1 ) );
    if( nx < k ) { 
      
      //           Determine if workspace is large enough for blocked code.
      
      ldwork = n;
      iws = ldwork*nb;
      if( lwork < iws ) { 
        
        //              Not enough workspace to use optimal NB:  reduce NB and
        //              determine the minimum value of NB.
        
        nb = lwork/ldwork;
        nbmin = max( 2, ilaenv( 2, "SGEQRF", " ", m, n, -1, 
         -1 ) );
      }
    }
  }
  
  if( (nb >= nbmin && nb < k) && nx < k ) { 
    
    //        Use blocked code initially
    
    for( i = 1, i_ = i - 1, _do0=docnt(i,k - nx,_do1 = nb); _do0 > 0; i += _do1, i_ += _do1, _do0-- ) { 
      ib = min( k - i + 1, nb );
      
      //           Compute the QR factorization of the current block
      //           A(i:m,i:i+ib-1)
      
      sgeqr2( m - i + 1, ib, &A(i_,i_), lda, &tau[i_], work, 
       iinfo );
      if( i + ib <= n ) { 
        
        //              Form the triangular factor of the block reflector
        //              H = H(i) H(i+1) . . . H(i+ib-1)
        
        slarft( 'F'/*Forward*/, 'C'/*Columnwise*/, m - i + 
         1, ib, &A(i_,i_), lda, &tau[i_], work, ldwork );
        
        //              Apply H' to A(i:m,i+ib:n) from the left
        
        slarfb( 'L'/*Left*/, 'T'/*Transpose*/, 'F'/*Forward*/
         , 'C'/*Columnwise*/, m - i + 1, n - i - ib + 1, 
         ib, &A(i_,i_), lda, work, ldwork, &A(i_ + ib,i_), 
         lda, &work[ib], ldwork );
      }
    }
  }
  else { 
    i = 1;
  }
  
  //     Use unblocked code to factor the last or only block.
  
  if( i <= k ) 
    sgeqr2( m - i + 1, n - i + 1, &A(i - 1,i - 1), lda, &tau[i - 1], 
     work, iinfo );
  
  work[0] = iws;
  return;
  
  //     End of SGEQRF
  
#undef  A
} // end of function 


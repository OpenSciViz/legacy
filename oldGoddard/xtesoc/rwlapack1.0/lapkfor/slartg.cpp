/*
 * C++ implementation of Lapack routine slartg
 *
 * $Id: slartg.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:00:31
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: slartg.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ZERO = 0.0e0;
const float ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ slartg(const float &f, const float &g, float &cs, float &sn, 
 float &r)
{
  float t, tt;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SLARTG generate a plane rotation so that
  
  //     [  CS  SN  ]  .  [ F ]  =  [ R ]   where CS**2 + SN**2 = 1.
  //     [ -SN  CS  ]     [ G ]     [ 0 ]
  
  //  This is a faster version of the BLAS1 routine SROTG, except for
  //  the following differences:
  //     F and G are unchanged on return.
  //     If G=0, then CS=1 and SN=0.
  //     If F=0 and (G .ne. 0), then CS=0 and SN=1 without doing any
  //        floating point operations (saves work in SBDSQR when
  //        there are zeros on the diagonal).
  
  //  Arguments
  //  =========
  
  //  F       (input) REAL
  //          The first component of vector to be rotated.
  
  //  G       (input) REAL
  //          The second component of vector to be rotated.
  
  //  CS      (output) REAL
  //          The cosine of the rotation.
  
  //  SN      (output) REAL
  //          The sine of the rotation.
  
  //  R       (output) REAL
  //          The nonzero component of the rotated vector.
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  if( f == ZERO ) { 
    if( g == ZERO ) { 
      cs = ONE;
      sn = ZERO;
      r = ZERO;
    }
    else { 
      cs = ZERO;
      sn = ONE;
      r = g;
    }
  }
  else { 
    if( abs( f ) > abs( g ) ) { 
      t = g/f;
      tt = sqrt( ONE + t*t );
      cs = ONE/tt;
      sn = t*cs;
      r = f*tt;
    }
    else { 
      t = f/g;
      tt = sqrt( ONE + t*t );
      sn = ONE/tt;
      cs = t*sn;
      r = g*tt;
    }
  }
  return;
  
  //     End of SLARTG
  
} // end of function 


/*
 * C++ implementation of lapack routine dgecon
 *
 * $Id: dgecon.cpp,v 1.6 1993/04/06 20:40:25 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:33:54
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dgecon.cpp,v $
 * Revision 1.6  1993/04/06  20:40:25  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.5  1993/03/19  18:41:23  alv
 * now passes chars explicitly, rather than indirection of a string, to shut up SUN warnings
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:14:32  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:06:32  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ONE = 1.0e0;
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dgecon(const char &norm, long &n, double *a, long &lda, 
 double &anorm, double &rcond, double work[], long iwork[], long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  int onenrm;
  char normin;
  long ix, kase, kase1;
  double ainvnm, scale, sl, smlnum, su;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DGECON estimates the reciprocal of the condition number of a real
  //  general matrix A, in either the 1-norm or the infinity-norm, using
  //  the LU factorization computed by DGETRF.
  
  //  An estimate is obtained for norm(inv(A)), and the reciprocal of the
  //  condition number is computed as
  //     RCOND = 1 / ( norm(A) * norm(inv(A)) ).
  
  //  Arguments
  //  =========
  
  //  NORM    (input) CHARACTER*1
  //          Specifies whether the 1-norm condition number or the
  //          infinity-norm condition number is required:
  //          = '1' or 'O':  1-norm
  //          = 'I':         Infinity-norm
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  A       (input) DOUBLE PRECISION array, dimension (LDA,N)
  //          The factors L and U from the factorization A = P*L*U
  //          as computed by DGETRF.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  ANORM   (input) DOUBLE PRECISION
  //          If NORM = '1' or 'O', the 1-norm of the original matrix A.
  //          If NORM = 'I', the infinity-norm of the original matrix A.
  
  //  RCOND   (output) DOUBLE PRECISION
  //          The reciprocal of the condition number of the matrix A,
  //          computed as RCOND = 1/(norm(A) * norm(inv(A))).
  
  //  WORK    (workspace) DOUBLE PRECISION array, dimension (4*N)
  
  //  IWORK   (workspace) INTEGER array, dimension (N)
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  onenrm = norm == '1' || lsame( norm, 'O' );
  if( !onenrm && !lsame( norm, 'I' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( lda < max( 1, n ) ) { 
    info = -4;
  }
  else if( anorm < ZERO ) { 
    info = -5;
  }
  if( info != 0 ) { 
    xerbla( "DGECON", -info );
    return;
  }
  
  //     Quick return if possible
  
  rcond = ZERO;
  if( n == 0 ) { 
    rcond = ONE;
    return;
  }
  else if( anorm == ZERO ) { 
    return;
  }
  
  smlnum = dlamch( 'S'/* Safe minimum */ );
  
  //     Estimate the norm of inv(A).
  
  ainvnm = ZERO;
  normin = 'N';
  if( onenrm ) { 
    kase1 = 1;
  }
  else { 
    kase1 = 2;
  }
  kase = 0;
L_10:
  ;
  dlacon( n, &work[n], work, iwork, ainvnm, kase );
  if( kase != 0 ) { 
    if( kase == kase1 ) { 
      
      //           Multiply by inv(L).
      
      dlatrs( 'L'/* Lower */, 'N'/* No transpose */, 'U'/* Unit */
       , normin, n, a, lda, work, sl, &work[n*2], info );
      
      //           Multiply by inv(U).
      
      dlatrs( 'U'/* Upper */, 'N'/* No transpose */, 'N'/* Non-unit */
       , normin, n, a, lda, work, su, &work[n*3], info );
    }
    else { 
      
      //           Multiply by inv(U').
      
      dlatrs( 'U'/* Upper */, 'T'/* Transpose */, 'N'/* Non-unit */
       , normin, n, a, lda, work, su, &work[n*3], info );
      
      //           Multiply by inv(L').
      
      dlatrs( 'L'/* Lower */, 'T'/* Transpose */, 'U'/* Unit */
       , normin, n, a, lda, work, sl, &work[n*2], info );
    }
    
    //        Divide X by 1/(SL*SU) if doing so will not cause overflow.
    
    scale = sl*su;
    normin = 'Y';
    if( scale != ONE ) { 
      ix = idamax( n, work, 1 );
      if( scale < abs( work[ix - 1] )*smlnum || scale == ZERO ) 
        goto L_20;
      drscl( n, scale, work, 1 );
    }
    goto L_10;
  }
  
  //     Compute the estimate of the reciprocal condition number.
  
  if( ainvnm != ZERO ) 
    rcond = (ONE/ainvnm)/anorm;
  
L_20:
  ;
  return;
  
  //     End of DGECON
  
#undef  A
} // end of function 


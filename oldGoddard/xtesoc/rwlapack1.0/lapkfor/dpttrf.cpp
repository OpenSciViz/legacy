/*
 * C++ implementation of lapack routine dpttrf
 *
 * $Id: dpttrf.cpp,v 1.5 1993/04/06 20:42:13 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:37:46
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dpttrf.cpp,v $
 * Revision 1.5  1993/04/06  20:42:13  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:16:55  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:08:41  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dpttrf(const long &n, double d[], double e[], long &info)
{
  long _do0, i, i_;
  double di, ei;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DPTTRF computes the L*D*L' factorization of a real symmetric
  //  positive definite tridiagonal matrix A.  The factorization may also
  //  be regarded as having the form A = U'*D*U.
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  D       (input/output) DOUBLE PRECISION array, dimension (N)
  //          On entry, the n diagonal elements of the tridiagonal matrix
  //          A.  On exit, the n diagonal elements of the diagonal matrix
  //          D from the L*D*L' factorization of A.
  
  //  E       (input/output) DOUBLE PRECISION array, dimension (N-1)
  //          On entry, the (n-1) subdiagonal elements of the tridiagonal
  //          matrix A.  On exit, the (n-1) subdiagonal elements of the
  //          unit bidiagonal factor L from the L*D*L' factorization of A.
  //          E can also be regarded as the superdiagonal of the unit
  //          bidiagonal factor U from the U'*D*U factorization of A.
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: if INFO = k, the leading minor of order k is not
  //               positive definite; if k < N, the factorization could not
  //               be completed, while if k = N, the factorization was
  //               completed, but D(N) = 0.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  if( n < 0 ) { 
    info = -1;
    xerbla( "DPTTRF", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  //     Compute the L*D*L' (or U'*D*U) factorization of A.
  
  for( i = 1, i_ = i - 1, _do0 = n - 1; i <= _do0; i++, i_++ ) { 
    
    //        Drop out of the loop if d(i) <= 0: the matrix is not positive
    //        definite.
    
    di = d[i_];
    if( di <= ZERO ) 
      goto L_20;
    
    //        Solve for e(i) and d(i+1).
    
    ei = e[i_];
    e[i_] = ei/di;
    d[i_ + 1] = d[i_ + 1] - e[i_]*ei;
  }
  
  //     Check d(n) for positive definiteness.
  
  i = n;
  if( d[i - 1] > ZERO ) 
    goto L_30;
  
L_20:
  ;
  info = i;
  
L_30:
  ;
  return;
  
  //     End of DPTTRF
  
} // end of function 


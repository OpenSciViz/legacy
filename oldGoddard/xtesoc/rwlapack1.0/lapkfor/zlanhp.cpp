/*
 * C++ implementation of Lapack routine zlanhp
 *
 * $Id: zlanhp.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:48:35
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zlanhp.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ONE = 1.0e0;
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL double /*FUNCTION*/ zlanhp(const char &norm, const char &uplo, const long &n, DComplex ap[], 
 double work[])
{
  long _do0, _do1, _do10, _do11, _do12, _do2, _do3, _do4, _do5, 
   _do6, _do7, _do8, _do9, i, i_, j, j_, k;
  double absa, scale, sum, value, zlanhp_v;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZLANHP  returns the value of the one norm,  or the Frobenius norm, or
  //  the  infinity norm,  or the  element of  largest absolute value  of a
  //  DComplex hermitian matrix A,  supplied in packed form.
  
  //  Description
  //  ===========
  
  //  ZLANHP returns the value
  
  //     ZLANHP = ( max(abs(A(i,j))), NORM = 'M' or 'm'
  //              (
  //              ( norm1(A),         NORM = '1', 'O' or 'o'
  //              (
  //              ( normI(A),         NORM = 'I' or 'i'
  //              (
  //              ( normF(A),         NORM = 'F', 'f', 'E' or 'e'
  
  //  where  norm1  denotes the  one norm of a matrix (maximum column sum),
  //  normI  denotes the  infinity norm  of a matrix  (maximum row sum) and
  //  normF  denotes the  Frobenius norm of a matrix (square root of sum of
  //  squares).  Note that  max(abs(A(i,j)))  is not a  matrix norm.
  
  //  Arguments
  //  =========
  
  //  NORM    (input) CHARACTER*1
  //          Specifies the value to be returned in ZLANHP as described
  //          above.
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangular part of the
  //          hermitian matrix A is supplied.
  //          = 'U':  Upper triangular part of A is supplied
  //          = 'L':  Lower triangular part of A is supplied
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.  When N = 0, ZLANHP is
  //          set to zero.
  
  //  AP      (input) COMPLEX*16 array, dimension (N*(N+1)/2)
  //          The upper or lower triangle of the hermitian matrix A, packed
  //          columnwise in a linear array.  The j-th column of A is stored
  //          in the array AP as follows:
  //          if UPLO = 'U', AP(i + (j-1)*j/2) = A(i,j) for 1<=i<=j;
  //          if UPLO = 'L', AP(i + (j-1)*(2n-j)/2) = A(i,j) for j<=i<=n.
  //          Note that the  imaginary parts of the diagonal elements need
  //          not be set and are assumed to be zero.
  
  //  WORK    (workspace) DOUBLE PRECISION array, dimension (LWORK),
  //          where LWORK >= N when NORM = 'I' or '1' or 'O'; otherwise,
  //          WORK is not referenced.
  
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  if( n == 0 ) { 
    value = ZERO;
  }
  else if( lsame( norm, 'M' ) ) { 
    
    //        Find max(abs(A(i,j))).
    
    value = ZERO;
    if( lsame( uplo, 'U' ) ) { 
      k = 0;
      for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
        for( i = k + 1, i_ = i - 1, _do1 = k + j - 1; i <= _do1; i++, i_++ ) { 
          value = max( value, abs( ap[i_] ) );
        }
        k = k + j;
        value = max( value, abs( real( ap[k - 1] ) ) );
      }
    }
    else { 
      k = 1;
      for( j = 1, j_ = j - 1, _do2 = n; j <= _do2; j++, j_++ ) { 
        value = max( value, abs( real( ap[k - 1] ) ) );
        for( i = k + 1, i_ = i - 1, _do3 = k + n - j; i <= _do3; i++, i_++ ) { 
          value = max( value, abs( ap[i_] ) );
        }
        k = k + n - j + 1;
      }
    }
  }
  else if( ((lsame( norm, 'I' )) || (lsame( norm, 'O' ))) || (norm == 
   '1') ) { 
    
    //        Find normI(A) ( = norm1(A), since A is hermitian).
    
    value = ZERO;
    k = 1;
    if( lsame( uplo, 'U' ) ) { 
      for( j = 1, j_ = j - 1, _do4 = n; j <= _do4; j++, j_++ ) { 
        sum = ZERO;
        for( i = 1, i_ = i - 1, _do5 = j - 1; i <= _do5; i++, i_++ ) { 
          absa = abs( ap[k - 1] );
          sum = sum + absa;
          work[i_] = work[i_] + absa;
          k = k + 1;
        }
        work[j_] = sum + abs( real( ap[k - 1] ) );
        k = k + 1;
      }
      for( i = 1, i_ = i - 1, _do6 = n; i <= _do6; i++, i_++ ) { 
        value = max( value, work[i_] );
      }
    }
    else { 
      for( i = 1, i_ = i - 1, _do7 = n; i <= _do7; i++, i_++ ) { 
        work[i_] = ZERO;
      }
      for( j = 1, j_ = j - 1, _do8 = n; j <= _do8; j++, j_++ ) { 
        sum = work[j_] + abs( real( ap[k - 1] ) );
        k = k + 1;
        for( i = j + 1, i_ = i - 1, _do9 = n; i <= _do9; i++, i_++ ) { 
          absa = abs( ap[k - 1] );
          sum = sum + absa;
          work[i_] = work[i_] + absa;
          k = k + 1;
        }
        value = max( value, sum );
      }
    }
  }
  else if( (lsame( norm, 'F' )) || (lsame( norm, 'E' )) ) { 
    
    //        Find normF(A).
    
    scale = ZERO;
    sum = ONE;
    k = 2;
    if( lsame( uplo, 'U' ) ) { 
      for( j = 2, j_ = j - 1, _do10 = n; j <= _do10; j++, j_++ ) { 
        zlassq( j - 1, &ap[k - 1], 1, scale, sum );
        k = k + j;
      }
    }
    else { 
      for( j = 1, j_ = j - 1, _do11 = n - 1; j <= _do11; j++, j_++ ) { 
        zlassq( n - j, &ap[k - 1], 1, scale, sum );
        k = k + n - j + 1;
      }
    }
    sum = 2*sum;
    k = 1;
    for( i = 1, i_ = i - 1, _do12 = n; i <= _do12; i++, i_++ ) { 
      if( real( ap[k - 1] ) != ZERO ) { 
        absa = abs( real( ap[k - 1] ) );
        if( scale < absa ) { 
          sum = ONE + sum*pow(scale/absa, 2);
          scale = absa;
        }
        else { 
          sum = sum + pow(absa/scale, 2);
        }
      }
      if( lsame( uplo, 'U' ) ) { 
        k = k + i + 1;
      }
      else { 
        k = k + n - i + 1;
      }
    }
    value = scale*sqrt( sum );
  }
  
  zlanhp_v = value;
  return( zlanhp_v );
  
  //     End of ZLANHP
  
} // end of function 


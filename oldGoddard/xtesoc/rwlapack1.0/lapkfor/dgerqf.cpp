/*
 * C++ implementation of lapack routine dgerqf
 *
 * $Id: dgerqf.cpp,v 1.6 1993/04/06 20:40:37 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:34:24
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dgerqf.cpp,v $
 * Revision 1.6  1993/04/06  20:40:37  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.5  1993/03/19  18:41:23  alv
 * now passes chars explicitly, rather than indirection of a string, to shut up SUN warnings
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:14:50  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:06:45  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

RWLAPKDECL void /*FUNCTION*/ dgerqf(const long &m, const long &n, double *a, const long &lda, 
   double tau[], double work[], const long &lwork, long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  long _do0, _do1, i, i_, ib, iinfo, iws, k, ki, kk, ldwork, 
   mu, nb, nbmin, nu, nx;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DGERQF computes an RQ factorization of a real m by n matrix A:
  //  A = R * Q.
  
  //  Arguments
  //  =========
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix A.  M >= 0.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix A.  N >= 0.
  
  //  A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)
  //          On entry, the m by n matrix A.
  //          On exit, if m <= n, the upper triangle of the subarray
  //          A(1:m,n-m+1:n) contains the m by m upper triangular matrix R;
  //          if m >= n, the elements on and above the (m-n)-th subdiagonal
  //          contain the m by n upper trapezoidal matrix R; the remaining
  //          elements, with the array TAU, represent the orthogonal matrix
  //          Q as a product of elementary reflectors (see Further
  //          Details).
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,M).
  
  //  TAU     (output) DOUBLE PRECISION array, dimension (min(M,N))
  //          The scalar factors of the elementary reflectors (see Further
  //          Details).
  
  //  WORK    (workspace) DOUBLE PRECISION array, dimension (LWORK)
  //          On exit, if INFO = 0, WORK(1) returns the minimum value of
  //          LWORK required to use the optimal blocksize.
  
  //  LWORK   (input) INTEGER
  //          The dimension of the array WORK.  LWORK >= max(1,M).
  //          For optimum performance LWORK should be at least M*NB,
  //          where NB is the optimal blocksize.
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument had an illegal value
  
  //  Further Details
  //  ===============
  
  //  The matrix Q is represented as a product of elementary reflectors
  
  //     Q = H(1) H(2) . . . H(k), where k = min(m,n).
  
  //  Each H(i) has the form
  
  //     H(i) = I - tau * v * v'
  
  //  where tau is a real scalar, and v is a real vector with
  //  v(n-k+i+1:n) = 0 and v(n-k+i) = 1; v(1:n-k+i-1) is stored on exit in
  //  A(m-k+i,1:n-k+i-1), and tau in TAU(i).
  
  //  =====================================================================
  
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input arguments
  
  info = 0;
  if( m < 0 ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( lda < max( 1, m ) ) { 
    info = -4;
  }
  else if( lwork < max( 1, m ) ) { 
    info = -7;
  }
  if( info != 0 ) { 
    xerbla( "DGERQF", -info );
    return;
  }
  
  //     Quick return if possible
  
  k = min( m, n );
  if( k == 0 ) { 
    work[0] = 1;
    return;
  }
  
  //     Determine the block size.
  
  nb = ilaenv( 1, "DGERQF", " ", m, n, -1, -1 );
  nbmin = 2;
  nx = 1;
  iws = m;
  if( nb > 1 && nb < k ) { 
    
    //        Determine when to cross over from blocked to unblocked code.
    
    nx = max( 0, ilaenv( 3, "DGERQF", " ", m, n, -1, -1 ) );
    if( nx < k ) { 
      
      //           Determine if workspace is large enough for blocked code.
      
      ldwork = m;
      iws = ldwork*nb;
      if( lwork < iws ) { 
        
        //              Not enough workspace to use optimal NB:  reduce NB and
        //              determine the minimum value of NB.
        
        nb = lwork/ldwork;
        nbmin = max( 2, ilaenv( 2, "DGERQF", " ", m, n, -1, 
         -1 ) );
      }
    }
  }
  
  if( (nb >= nbmin && nb < k) && nx < k ) { 
    
    //        Use blocked code initially.
    //        The last kk rows are handled by the block method.
    
    ki = ((k - nx - 1)/nb)*nb;
    kk = min( k, ki + nb );
    
    for( i = k - kk + ki + 1, i_ = i - 1, _do0=docnt(i,k - kk + 
     1,_do1 = -nb); _do0 > 0; i += _do1, i_ += _do1, _do0-- ) { 
      ib = min( k - i + 1, nb );
      
      //           Compute the RQ factorization of the current block
      //           A(m-k+i:m-k+i+ib-1,1:n-k+i+ib-1)
      
      dgerq2( ib, n - k + i + ib - 1, &A(0,m - k + i_), lda, 
       &tau[i_], work, iinfo );
      if( m - k + i > 1 ) { 
        
        //              Form the triangular factor of the block reflector
        //              H = H(i+ib-1) . . . H(i+1) H(i)
        
        dlarft( 'B'/* Backward */, 'R'/* Rowwise */, n - k + 
         i + ib - 1, ib, &A(0,m - k + i_), lda, &tau[i_], 
         work, ldwork );
        
        //              Apply H to A(1:m-k+i-1,1:n-k+i+ib-1) from the right
        
        dlarfb( 'R'/* Right */, 'N'/* No transpose */, 'B'/* Backward */
         , 'R'/* Rowwise */, m - k + i - 1, n - k + i + ib - 
         1, ib, &A(0,m - k + i_), lda, work, ldwork, a, lda, 
         &work[ib], ldwork );
      }
    }
    mu = m - k + i + nb - 1;
    nu = n - k + i + nb - 1;
  }
  else { 
    mu = m;
    nu = n;
  }
  
  //     Use unblocked code to factor the last or only block
  
  if( mu > 0 && nu > 0 ) 
    dgerq2( mu, nu, a, lda, tau, work, iinfo );
  
  work[0] = iws;
  return;
  
  //     End of DGERQF
  
#undef  A
} // end of function 


/*
 * C++ implementation of Lapack routine sorg2r
 *
 * $Id: sorg2r.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:01:07
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: sorg2r.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ONE = 1.0e0;
const float ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ sorg2r(const long &m, const long &n, const long &k, float *a, 
 const long &lda, float tau[], float work[], long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  long _do0, _do1, _do2, i, i_, j, j_, l, l_;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SORG2R generates an m by n real matrix Q with orthonormal columns,
  //  which is defined as the first n columns of a product of k elementary
  //  reflectors of order m
  
  //        Q  =  H(1) H(2) . . . H(k)
  
  //  as returned by SGEQRF.
  
  //  Arguments
  //  =========
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix Q. M >= 0.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix Q. M >= N >= 0.
  
  //  K       (input) INTEGER
  //          The number of elementary reflectors whose product defines the
  //          matrix Q. N >= K >= 0.
  
  //  A       (input/output) REAL array, dimension (LDA,N)
  //          On entry, the i-th column must contain the vector which
  //          defines the elementary reflector H(i), for i = 1,2,...,k, as
  //          returned by SGEQRF in the first k columns of its array
  //          argument A.
  //          On exit, the m-by-n matrix Q.
  
  //  LDA     (input) INTEGER
  //          The first dimension of the array A. LDA >= max(1,M).
  
  //  TAU     (input) REAL array, dimension (K)
  //          TAU(i) must contain the scalar factor of the elementary
  //          reflector H(i), as returned by SGEQRF.
  
  //  WORK    (workspace) REAL array, dimension (N)
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument has an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input arguments
  
  info = 0;
  if( m < 0 ) { 
    info = -1;
  }
  else if( n < 0 || n > m ) { 
    info = -2;
  }
  else if( k < 0 || k > n ) { 
    info = -3;
  }
  else if( lda < max( 1, m ) ) { 
    info = -5;
  }
  if( info != 0 ) { 
    xerbla( "SORG2R", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n <= 0 ) 
    return;
  
  //     Initialise columns k+1:n to columns of the unit matrix
  
  for( j = k + 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
    for( l = 1, l_ = l - 1, _do1 = m; l <= _do1; l++, l_++ ) { 
      A(j_,l_) = ZERO;
    }
    A(j_,j_) = ONE;
  }
  
  for( i = k, i_ = i - 1; i >= 1; i--, i_-- ) { 
    
    //        Apply H(i) to A(i:m,i:n) from the left
    
    if( i < n ) { 
      A(i_,i_) = ONE;
      slarf( 'L'/*Left*/, m - i + 1, n - i, &A(i_,i_), 1, tau[i_], 
       &A(i_ + 1,i_), lda, work );
    }
    if( i < m ) 
      sscal( m - i, -tau[i_], &A(i_,i_ + 1), 1 );
    A(i_,i_) = ONE - tau[i_];
    
    //        Set A(1:i-1,i) to zero
    
    for( l = 1, l_ = l - 1, _do2 = i - 1; l <= _do2; l++, l_++ ) { 
      A(i_,l_) = ZERO;
    }
  }
  return;
  
  //     End of SORG2R
  
#undef  A
} // end of function 


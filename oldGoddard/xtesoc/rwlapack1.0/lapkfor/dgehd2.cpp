/*
 * C++ implementation of lapack routine dgehd2
 *
 * $Id: dgehd2.cpp,v 1.6 1993/04/06 20:40:29 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:34:04
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dgehd2.cpp,v $
 * Revision 1.6  1993/04/06  20:40:29  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.5  1993/03/19  18:41:23  alv
 * now passes chars explicitly, rather than indirection of a string, to shut up SUN warnings
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:14:37  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:06:35  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dgehd2(const long &n, const long &ilo, const long &ihi, 
 double *a, const long &lda, double tau[], double work[], long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  long _do0, i, i_;
  double aii;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DGEHD2 reduces a real general matrix A to upper Hessenberg form H by
  //  an orthogonal similarity transformation:  Q' * A * Q = H .
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  ILO     (input) INTEGER
  //  IHI     (input) INTEGER
  //          It is assumed that A is already upper triangular in rows
  //          and columns 1:ILO-1 and IHI+1:N. ILO and IHI are normally
  //          set by a previous call to DGEBAL; otherwise they should be
  //          set to 1 and N respectively. See Further Details.
  //          1 <= ILO <= IHI <= max(1,N).
  
  //  A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)
  //          On entry, the n by n general matrix to be reduced.
  //          On exit, the upper triangle and the first subdiagonal of A
  //          are overwritten with the upper Hessenberg matrix H, and the
  //          elements below the first subdiagonal, with the array TAU,
  //          represent the orthogonal matrix Q as a product of elementary
  //          reflectors. See Further Details.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  TAU     (output) DOUBLE PRECISION array, dimension (N)
  //          The scalar factors of the elementary reflectors (see Further
  //          Details). Elements 1:ILO-1 and IHI:N of TAU are set to zero.
  
  //  WORK    (workspace) DOUBLE PRECISION array, dimension (N)
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit.
  //          < 0:  if INFO = -i, the i-th argument had an illegal value.
  
  //  Further Details
  //  ===============
  
  //  The matrix Q is represented as a product of (ihi-ilo) elementary
  //  reflectors
  
  //     Q = H(ilo) H(ilo+1) . . . H(ihi-1).
  
  //  Each H(i) has the form
  
  //     H(i) = I - tau * v * v'
  
  //  where tau is a real scalar, and v is a real vector with
  //  v(1:i) = 0, v(i+1) = 1 and v(ihi+1:n) = 0; v(i+2:ihi) is stored on
  //  exit in A(i+2:ihi,i), and tau in TAU(i).
  
  //  The contents of A are illustrated by the following example, with
  //  n = 7, ilo = 2 and ihi = 6:
  
  //  on entry                         on exit
  
  //  ( a   a   a   a   a   a   a )    (  a   a   h   h   h   h   a )
  //  (     a   a   a   a   a   a )    (      a   h   h   h   h   a )
  //  (     a   a   a   a   a   a )    (      h   h   h   h   h   h )
  //  (     a   a   a   a   a   a )    (      v2  h   h   h   h   h )
  //  (     a   a   a   a   a   a )    (      v2  v3  h   h   h   h )
  //  (     a   a   a   a   a   a )    (      v2  v3  v4  h   h   h )
  //  (                         a )    (                          a )
  
  //  where a denotes an element of the original matrix A, h denotes a
  //  modified element of the upper Hessenberg matrix H, and vi denotes an
  //  element of the vector defining H(i).
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters
  
  info = 0;
  if( n < 0 ) { 
    info = -1;
  }
  else if( ilo < 1 ) { 
    info = -2;
  }
  else if( ihi < ilo || ihi > max( 1, n ) ) { 
    info = -3;
  }
  else if( lda < max( 1, n ) ) { 
    info = -5;
  }
  if( info != 0 ) { 
    xerbla( "DGEHD2", -info );
    return;
  }
  
  for( i = ilo, i_ = i - 1, _do0 = ihi - 1; i <= _do0; i++, i_++ ) { 
    
    //        Compute elementary reflector H(i) to annihilate A(i+2:ihi,i)
    
    dlarfg( ihi - i, A(i_,i_ + 1), &A(i_,min( i + 2, n ) - 1), 
     1, tau[i_] );
    aii = A(i_,i_ + 1);
    A(i_,i_ + 1) = ONE;
    
    //        Apply H(i) to A(1:ihi,i+1:ihi) from the right
    
    dlarf( 'R'/* Right */, ihi, ihi - i, &A(i_,i_ + 1), 1, tau[i_], 
     &A(i_ + 1,0), lda, work );
    
    //        Apply H(i) to A(i+1:ihi,i+1:n) from the left
    
    dlarf( 'L'/* Left */, ihi - i, n - i, &A(i_,i_ + 1), 1, tau[i_], 
     &A(i_ + 1,i_ + 1), lda, work );
    
    A(i_,i_ + 1) = aii;
  }
  
  return;
  
  //     End of DGEHD2
  
#undef  A
} // end of function 


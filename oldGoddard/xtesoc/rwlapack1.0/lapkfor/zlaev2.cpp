/*
 * C++ implementation of Lapack routine zlaev2
 *
 * $Id: zlaev2.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:48:17
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zlaev2.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zlaev2(const DComplex &a, const DComplex &b, const DComplex &c, double &rt1, 
 double &rt2, double &cs1, DComplex &sn1)
{
  double t;
  DComplex w;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZLAEV2 computes the eigendecomposition of a 2-by-2 Hermitian matrix
  //     [  A         B  ]
  //     [  CONJG(B)  C  ].
  //  On return, RT1 is the eigenvalue of larger absolute value, RT2 is the
  //  eigenvalue of smaller absolute value, and (CS1,SN1) is the unit right
  //  eigenvector for RT1, giving the decomposition
  
  //  [ CS1  CONJG(SN1) ] [    A     B ] [ CS1 -CONJG(SN1) ] = [ RT1  0  ]
  //  [-SN1     CS1     ] [ CONJG(B) C ] [ SN1     CS1     ]   [  0  RT2 ].
  
  //  Arguments
  //  =========
  
  //  A      (input) COMPLEX*16
  //         The (1,1) entry of the 2-by-2 matrix.
  
  //  B      (input) COMPLEX*16
  //         The (1,2) entry and the conjugate of the (2,1) entry of the
  //         2-by-2 matrix.
  
  //  C      (input) COMPLEX*16
  //         The (2,2) entry of the 2-by-2 matrix.
  
  //  RT1    (output) DOUBLE PRECISION
  //         The eigenvalue of larger absolute value.
  
  //  RT2    (output) DOUBLE PRECISION
  //         The eigenvalue of smaller absolute value.
  
  //  CS1    (output) DOUBLE PRECISION
  //  SN1    (output) COMPLEX*16
  //         The vector (CS1, SN1) is a unit right eigenvector for RT1.
  
  //  Further Details
  //  ===============
  
  //  RT1 is accurate to a few ulps barring over/underflow.
  
  //  RT2 may be inaccurate if there is massive cancellation in the
  //  determinant A*C-B*B; higher precision or correctly rounded or
  //  correctly truncated arithmetic would be needed to compute RT2
  //  accurately in all cases.
  
  //  CS1 and SN1 are accurate to a few ulps barring over/underflow.
  
  //  Overflow is possible only if RT1 is within a factor of 5 of overflow.
  //  Underflow is harmless if the input data is 0 or exceeds
  //     underflow_threshold / macheps.
  
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  if( abs( b ) == ZERO ) { 
    w = DComplex(ONE);
  }
  else { 
    w = conj( b )/abs( b );
  }
  dlaev2( real( a ), abs( b ), real( c ), rt1, rt2, cs1, t );
  sn1 = w*t;
  return;
  
  //     End of ZLAEV2
  
} // end of function 


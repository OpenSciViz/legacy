/*
 * C++ implementation of lapack routine dlaqtr
 *
 * $Id: dlaqtr.cpp,v 1.6 1993/04/06 20:41:13 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:35:48
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dlaqtr.cpp,v $
 * Revision 1.6  1993/04/06  20:41:13  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.5  1993/03/19  21:41:05  alv
 * const added to args
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:15:39  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:07:33  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dlaqtr(const int &ltran, const int &lreal, const long &n, 
 double *t, const long &ldt, double b[], const double &w, double &scale, double x[], 
 double work[], long &info)
{
#define T(I_,J_)  (*(t+(I_)*(ldt)+(J_)))
  int notran;
  long _do0, _do1, _do2, _do3, _do4, _do5, i, i_, ierr, j, j1, 
   j2, j_, jnext, k, k_, n1, n2;
  double bignum, d[2][2], eps, rec, scaloc, si, smin, sminw, smlnum, 
   sr, tjj, tmp, v[2][2], xj, xmax, xnorm, z;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DLAQTR solves the real quasi-triangular system
  
  //     op(T)*p = scale*c,               if LREAL = .TRUE.
  
  //  or the DComplex quasi-triangular systems
  
  //     op(T + iB)*(p+iq) = scale*(c+id),  if LREAL = .FALSE.
  
  //  in real arithmetic, where T is upper quasi-triangular.
  //  If LREAL = .FALSE., then the first diagonal block of T
  //  must be 1 by 1, B is the specially structured matrix
  
  //                 B = [ b(1) b(2) ... b(n) ]
  //                     [       w            ]
  //                     [           w        ]
  //                     [              .     ]
  //                     [                 w  ]
  
  //  op(A) = A or A', A' denotes the conjugate transpose of
  //  matrix A.
  
  //  On input, X = [ c ].  On output, X = [ p ].
  //                [ d ]                  [ q ]
  
  //  This subroutine is designed for the condition number estimation
  //  in routine DTRSNA.
  
  //  Arguments
  //  =========
  
  //  LTRAN   (input) LOGICAL
  //          On entry, LTRAN specifies the option of conjugate
  //          transpose:
  //             = .FALSE.,    op(T+i*B) = T+i*B,
  //             = .TRUE.,     op(T+i*B) = (T+i*B)'.
  
  //  LREAL   (input) LOGICAL
  //          On entry, LREAL specifies the input matrix
  //          structure:
  //             = .FALSE.,    the input is DComplex
  //             = .TRUE.,     the input is real
  
  //  N       (input) INTEGER
  //          On entry, N specifies the order of T+i*B. N >= 0.
  
  //  T       (input) DOUBLE PRECISION array, dimension(LDT,N)
  //          On entry, T contains a matrix in Schur canonical form.
  //          If LREAL = .FALSE., then the first diagonal block
  //          of T must be 1 by 1.
  
  //  LDT     (input) INTEGER
  //          The leading dimension of the matrix T. LDT >= max(1,N).
  
  //  B       (input) DOUBLE PRECISION array, dimension(N)
  //          On entry, B contains the elements to form the matrix
  //          B as described above.
  //          If LREAL = .TRUE., B is not referenced.
  
  //  W       (input) DOUBLE PRECISION
  //          On entry, W is the diagonal element of the matrix B.
  //          If LREAL = .TRUE., W is not referenced.
  
  //  SCALE   (output) DOUBLE PRECISION
  //          On exit, SCALE is the scale factor.
  
  //  X       (input/output) DOUBLE PRECISION array, dimension(2*N)
  //          On entry, X contains the right hand side of the system.
  //          On exit, X is overwritten by the solution.
  
  //  WORK    (workspace) DOUBLE PRECISION array, dimension(N)
  
  //  INFO    (output) INTEGER
  //          On exit, INFO is set to
  //             0: successful exit.
  //               1: the some diagonal 1 by 1 block has been perturbed by
  //                  a small number SMIN to keep nonsingularity.
  //               2: the some diagonal 2 by 2 block has been perturbed by
  //                  a small number in DLALN2 to keep nonsingularity.
  //          NOTE: In the interests of speed, this routine does not
  //                check the inputs for errors.
  
  //-----------------------------------------------------------------------
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Local Arrays ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Do not test the input parameters for errors
  
  notran = !ltran;
  info = 0;
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  //     Set constants to control overflow
  
  eps = dlamch( 'P' );
  smlnum = dlamch( 'S' )/eps;
  bignum = ONE/smlnum;
  
  xnorm = dlange( 'M', n, n, t, ldt, (double*)d );
  if( !lreal ) 
    xnorm = vmax( xnorm, abs( w ), dlange( 'M', n, 1, b, n, (double*)d ), 
     FEND );
  smin = max( smlnum, eps*xnorm );
  
  //     Compute 1-norm of each column of strictly upper triangular
  //     part of T to control overflow in triangular solver.
  
  work[0] = ZERO;
  for( j = 2, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
    work[j_] = dasum( j - 1, &T(j_,0), 1 );
  }
  
  if( !lreal ) { 
    for( i = 2, i_ = i - 1, _do1 = n; i <= _do1; i++, i_++ ) { 
      work[i_] = work[i_] + abs( b[i_] );
    }
  }
  
  n2 = 2*n;
  n1 = n;
  if( !lreal ) 
    n1 = n2;
  k = idamax( n1, x, 1 );
  xmax = abs( x[k - 1] );
  scale = ONE;
  
  if( xmax > bignum ) { 
    scale = bignum/xmax;
    dscal( n1, scale, x, 1 );
    xmax = bignum;
  }
  
  if( lreal ) { 
    
    if( notran ) { 
      
      //           Solve T*p = scale*c
      
      jnext = n;
      for( j = n, j_ = j - 1; j >= 1; j--, j_-- ) { 
        if( j > jnext ) 
          goto L_30;
        j1 = j;
        j2 = j;
        jnext = j - 1;
        if( j > 1 && T(j_ - 1,j_) != ZERO ) { 
          j1 = j - 1;
          j2 = j;
          jnext = j - 2;
        }
        
        if( j1 == j2 ) { 
          
          //                 Meet 1 by 1 diagonal block
          
          //                 Scale to avoid overflow when computing
          //                     x(j) = b(j)/T(j,j)
          
          xj = abs( x[j1 - 1] );
          tjj = abs( T(j1 - 1,j1 - 1) );
          tmp = T(j1 - 1,j1 - 1);
          if( tjj < smin ) { 
            tmp = smin;
            tjj = smin;
            info = 1;
          }
          
          if( xj == ZERO ) 
            goto L_30;
          
          if( tjj < ONE ) { 
            if( xj > bignum*tjj ) { 
              rec = ONE/xj;
              dscal( n, rec, x, 1 );
              scale = scale*rec;
              xmax = xmax*rec;
            }
          }
          x[j1 - 1] = x[j1 - 1]/tmp;
          xj = abs( x[j1 - 1] );
          
          //                 Scale x if necessary to avoid overflow when adding a
          //                 multiple of column j1 of T.
          
          if( xj > ONE ) { 
            rec = ONE/xj;
            if( work[j1 - 1] > (bignum - xmax)*rec ) { 
              dscal( n, rec, x, 1 );
              scale = scale*rec;
            }
          }
          if( j1 > 1 ) { 
            daxpy( j1 - 1, -x[j1 - 1], &T(j1 - 1,0), 1, 
             x, 1 );
            k = idamax( j1 - 1, x, 1 );
            xmax = abs( x[k - 1] );
          }
          
        }
        else { 
          
          //                 Meet 2 by 2 diagonal block
          
          //                 Call 2 by 2 linear system solve, to take
          //                 care of possible overflow by scaling factor.
          
          d[0][0] = x[j1 - 1];
          d[0][1] = x[j2 - 1];
          dlaln2( FALSE, 2, 1, smin, ONE, &T(j1 - 1,j1 - 1), 
           ldt, ONE, ONE, (double*)d, 2, ZERO, ZERO, (double*)v, 
           2, scaloc, xnorm, ierr );
          if( ierr != 0 ) 
            info = 2;
          
          if( scaloc != ONE ) { 
            dscal( n, scaloc, x, 1 );
            scale = scale*scaloc;
          }
          x[j1 - 1] = v[0][0];
          x[j2 - 1] = v[0][1];
          
          //                 Scale V(1,1) (= X(J1)) and/or V(2,1) (=X(J2))
          //                 to avoid overflow in updating right-hand side.
          
          xj = max( abs( v[0][0] ), abs( v[0][1] ) );
          if( xj > ONE ) { 
            rec = ONE/xj;
            if( max( work[j1 - 1], work[j2 - 1] ) > (bignum - 
             xmax)*rec ) { 
              dscal( n, rec, x, 1 );
              scale = scale*rec;
            }
          }
          
          //                 Update right-hand side
          
          if( j1 > 1 ) { 
            daxpy( j1 - 1, -x[j1 - 1], &T(j1 - 1,0), 1, 
             x, 1 );
            daxpy( j1 - 1, -x[j2 - 1], &T(j2 - 1,0), 1, 
             x, 1 );
            k = idamax( j1 - 1, x, 1 );
            xmax = abs( x[k - 1] );
          }
          
        }
        
L_30:
        ;
      }
      
    }
    else { 
      
      //           Solve T'*p = scale*c
      
      jnext = 1;
      for( j = 1, j_ = j - 1, _do2 = n; j <= _do2; j++, j_++ ) { 
        if( j < jnext ) 
          goto L_40;
        j1 = j;
        j2 = j;
        jnext = j + 1;
        if( j < n && T(j_,j_ + 1) != ZERO ) { 
          j1 = j;
          j2 = j + 1;
          jnext = j + 2;
        }
        
        if( j1 == j2 ) { 
          
          //                 1 by 1 diagonal block
          
          //                 Scale if necessary to avoid overflow in forming the
          //                 right-hand side entry by inner product.
          
          xj = abs( x[j1 - 1] );
          if( xmax > ONE ) { 
            rec = ONE/xmax;
            if( work[j1 - 1] > (bignum - xj)*rec ) { 
              dscal( n, rec, x, 1 );
              scale = scale*rec;
              xmax = xmax*rec;
            }
          }
          
          x[j1 - 1] = x[j1 - 1] - ddot( j1 - 1, &T(j1 - 1,0), 
           1, x, 1 );
          
          xj = abs( x[j1 - 1] );
          tjj = abs( T(j1 - 1,j1 - 1) );
          tmp = T(j1 - 1,j1 - 1);
          if( tjj < smin ) { 
            tmp = smin;
            tjj = smin;
            info = 1;
          }
          
          if( tjj < ONE ) { 
            if( xj > bignum*tjj ) { 
              rec = ONE/xj;
              dscal( n, rec, x, 1 );
              scale = scale*rec;
              xmax = xmax*rec;
            }
          }
          x[j1 - 1] = x[j1 - 1]/tmp;
          xmax = max( xmax, abs( x[j1 - 1] ) );
          
        }
        else { 
          
          //                 2 by 2 diagonal block
          
          //                 Scale if necessary to avoid overflow in forming the
          //                 right-hand side entries by inner product.
          
          xj = max( abs( x[j1 - 1] ), abs( x[j2 - 1] ) );
          if( xmax > ONE ) { 
            rec = ONE/xmax;
            if( max( work[j2 - 1], work[j1 - 1] ) > (bignum - 
             xj)*rec ) { 
              dscal( n, rec, x, 1 );
              scale = scale*rec;
              xmax = xmax*rec;
            }
          }
          
          d[0][0] = x[j1 - 1] - ddot( j1 - 1, &T(j1 - 1,0), 
           1, x, 1 );
          d[0][1] = x[j2 - 1] - ddot( j1 - 1, &T(j2 - 1,0), 
           1, x, 1 );
          
          dlaln2( TRUE, 2, 1, smin, ONE, &T(j1 - 1,j1 - 1), 
           ldt, ONE, ONE, (double*)d, 2, ZERO, ZERO, (double*)v, 
           2, scaloc, xnorm, ierr );
          if( ierr != 0 ) 
            info = 2;
          
          if( scaloc != ONE ) { 
            dscal( n, scaloc, x, 1 );
            scale = scale*scaloc;
          }
          x[j1 - 1] = v[0][0];
          x[j2 - 1] = v[0][1];
          xmax = vmax( abs( x[j1 - 1] ), abs( x[j2 - 1] ), 
           xmax, FEND );
          
        }
L_40:
        ;
      }
    }
    
  }
  else { 
    
    sminw = max( eps*abs( w ), smin );
    if( notran ) { 
      
      //           Solve (T + iB)*(p+iq) = c+id
      
      jnext = n;
      for( j = n, j_ = j - 1; j >= 1; j--, j_-- ) { 
        if( j > jnext ) 
          goto L_70;
        j1 = j;
        j2 = j;
        jnext = j - 1;
        if( j > 1 && T(j_ - 1,j_) != ZERO ) { 
          j1 = j - 1;
          j2 = j;
          jnext = j - 2;
        }
        
        if( j1 == j2 ) { 
          
          //                 1 by 1 diagonal block
          
          //                 Scale if necessary to avoid overflow in division
          
          z = w;
          if( j1 == 1 ) 
            z = b[0];
          xj = abs( x[j1 - 1] ) + abs( x[n + j1 - 1] );
          tjj = abs( T(j1 - 1,j1 - 1) ) + abs( z );
          tmp = T(j1 - 1,j1 - 1);
          if( tjj < sminw ) { 
            tmp = sminw;
            tjj = sminw;
            info = 1;
          }
          
          if( xj == ZERO ) 
            goto L_70;
          
          if( tjj < ONE ) { 
            if( xj > bignum*tjj ) { 
              rec = ONE/xj;
              dscal( n2, rec, x, 1 );
              scale = scale*rec;
              xmax = xmax*rec;
            }
          }
          dladiv( x[j1 - 1], x[n + j1 - 1], tmp, z, sr, 
           si );
          x[j1 - 1] = sr;
          x[n + j1 - 1] = si;
          xj = abs( x[j1 - 1] ) + abs( x[n + j1 - 1] );
          
          //                 Scale x if necessary to avoid overflow when adding a
          //                 multiple of column j1 of T.
          
          if( xj > ONE ) { 
            rec = ONE/xj;
            if( work[j1 - 1] > (bignum - xmax)*rec ) { 
              dscal( n2, rec, x, 1 );
              scale = scale*rec;
            }
          }
          
          if( j1 > 1 ) { 
            daxpy( j1 - 1, -x[j1 - 1], &T(j1 - 1,0), 1, 
             x, 1 );
            daxpy( j1 - 1, -x[n + j1 - 1], &T(j1 - 1,0), 
             1, &x[n], 1 );
            
            x[0] = x[0] + b[j1 - 1]*x[n + j1 - 1];
            x[n] = x[n] - b[j1 - 1]*x[j1 - 1];
            
            xmax = ZERO;
            for( k = 1, k_ = k - 1, _do3 = j1 - 1; k <= _do3; k++, k_++ ) { 
              xmax = max( xmax, abs( x[k_] ) + abs( x[k_ + n] ) );
            }
          }
          
        }
        else { 
          
          //                 Meet 2 by 2 diagonal block
          
          d[0][0] = x[j1 - 1];
          d[0][1] = x[j2 - 1];
          d[1][0] = x[n + j1 - 1];
          d[1][1] = x[n + j2 - 1];
          dlaln2( FALSE, 2, 2, sminw, ONE, &T(j1 - 1,j1 - 1), 
           ldt, ONE, ONE, (double*)d, 2, ZERO, -w, (double*)v, 
           2, scaloc, xnorm, ierr );
          if( ierr != 0 ) 
            info = 2;
          
          if( scaloc != ONE ) { 
            dscal( 2*n, scaloc, x, 1 );
            scale = scaloc*scale;
          }
          x[j1 - 1] = v[0][0];
          x[j2 - 1] = v[0][1];
          x[n + j1 - 1] = v[1][0];
          x[n + j2 - 1] = v[1][1];
          
          //                 Scale X(J1), .... to avoid overflow in
          //                 updating right hand side.
          
          xj = max( abs( v[0][0] ) + abs( v[1][0] ), abs( v[0][1] ) + 
           abs( v[1][1] ) );
          if( xj > ONE ) { 
            rec = ONE/xj;
            if( max( work[j1 - 1], work[j2 - 1] ) > (bignum - 
             xmax)*rec ) { 
              dscal( n2, rec, x, 1 );
              scale = scale*rec;
            }
          }
          
          //                 Update the right-hand side.
          
          if( j1 > 1 ) { 
            daxpy( j1 - 1, -x[j1 - 1], &T(j1 - 1,0), 1, 
             x, 1 );
            daxpy( j1 - 1, -x[j2 - 1], &T(j2 - 1,0), 1, 
             x, 1 );
            
            daxpy( j1 - 1, -x[n + j1 - 1], &T(j1 - 1,0), 
             1, &x[n], 1 );
            daxpy( j1 - 1, -x[n + j2 - 1], &T(j2 - 1,0), 
             1, &x[n], 1 );
            
            x[0] = x[0] + b[j1 - 1]*x[n + j1 - 1] + b[j2 - 1]*
             x[n + j2 - 1];
            x[n] = x[n] - b[j1 - 1]*x[j1 - 1] - b[j2 - 1]*
             x[j2 - 1];
            
            xmax = ZERO;
            for( k = 1, k_ = k - 1, _do4 = j1 - 1; k <= _do4; k++, k_++ ) { 
              xmax = max( abs( x[k_] ) + abs( x[k_ + n] ), 
               xmax );
            }
          }
          
        }
L_70:
        ;
      }
      
    }
    else { 
      
      //           Solve (T + iB)'*(p+iq) = c+id
      
      jnext = 1;
      for( j = 1, j_ = j - 1, _do5 = n; j <= _do5; j++, j_++ ) { 
        if( j < jnext ) 
          goto L_80;
        j1 = j;
        j2 = j;
        jnext = j + 1;
        if( j < n && T(j_,j_ + 1) != ZERO ) { 
          j1 = j;
          j2 = j + 1;
          jnext = j + 2;
        }
        
        if( j1 == j2 ) { 
          
          //                 1 by 1 diagonal block
          
          //                 Scale if necessary to avoid overflow in forming the
          //                 right-hand side entry by inner product.
          
          xj = abs( x[j1 - 1] ) + abs( x[j1 + n - 1] );
          if( xmax > ONE ) { 
            rec = ONE/xmax;
            if( work[j1 - 1] > (bignum - xj)*rec ) { 
              dscal( n2, rec, x, 1 );
              scale = scale*rec;
              xmax = xmax*rec;
            }
          }
          
          x[j1 - 1] = x[j1 - 1] - ddot( j1 - 1, &T(j1 - 1,0), 
           1, x, 1 );
          x[n + j1 - 1] = x[n + j1 - 1] - ddot( j1 - 1, 
           &T(j1 - 1,0), 1, &x[n], 1 );
          if( j1 > 1 ) { 
            x[j1 - 1] = x[j1 - 1] - b[j1 - 1]*x[n];
            x[n + j1 - 1] = x[n + j1 - 1] + b[j1 - 1]*
             x[0];
          }
          xj = abs( x[j1 - 1] ) + abs( x[j1 + n - 1] );
          
          z = w;
          if( j1 == 1 ) 
            z = b[0];
          
          //                 Scale if necessary to avoid overflow in
          //                 DComplex division
          
          tjj = abs( T(j1 - 1,j1 - 1) ) + abs( z );
          tmp = T(j1 - 1,j1 - 1);
          if( tjj < sminw ) { 
            tmp = sminw;
            tjj = sminw;
            info = 1;
          }
          
          if( tjj < ONE ) { 
            if( xj > bignum*tjj ) { 
              rec = ONE/xj;
              dscal( n2, rec, x, 1 );
              scale = scale*rec;
              xmax = xmax*rec;
            }
          }
          dladiv( x[j1 - 1], x[n + j1 - 1], tmp, -z, sr, 
           si );
          x[j1 - 1] = sr;
          x[j1 + n - 1] = si;
          xmax = max( abs( x[j1 - 1] ) + abs( x[j1 + n - 1] ), 
           xmax );
          
        }
        else { 
          
          //                 2 by 2 diagonal block
          
          //                 Scale if necessary to avoid overflow in forming the
          //                 right-hand side entry by inner product.
          
          xj = max( abs( x[j1 - 1] ) + abs( x[n + j1 - 1] ), 
           abs( x[j2 - 1] ) + abs( x[n + j2 - 1] ) );
          if( xmax > ONE ) { 
            rec = ONE/xmax;
            if( max( work[j1 - 1], work[j2 - 1] ) > (bignum - 
             xj)/xmax ) { 
              dscal( n2, rec, x, 1 );
              scale = scale*rec;
              xmax = xmax*rec;
            }
          }
          
          d[0][0] = x[j1 - 1] - ddot( j1 - 1, &T(j1 - 1,0), 
           1, x, 1 );
          d[0][1] = x[j2 - 1] - ddot( j1 - 1, &T(j2 - 1,0), 
           1, x, 1 );
          d[1][0] = x[n + j1 - 1] - ddot( j1 - 1, &T(j1 - 1,0), 
           1, &x[n], 1 );
          d[1][1] = x[n + j2 - 1] - ddot( j1 - 1, &T(j2 - 1,0), 
           1, &x[n], 1 );
          d[0][0] = d[0][0] - b[j1 - 1]*x[n];
          d[0][1] = d[0][1] - b[j2 - 1]*x[n];
          d[1][0] = d[1][0] + b[j1 - 1]*x[0];
          d[1][1] = d[1][1] + b[j2 - 1]*x[0];
          
          dlaln2( TRUE, 2, 2, sminw, ONE, &T(j1 - 1,j1 - 1), 
           ldt, ONE, ONE, (double*)d, 2, ZERO, w, (double*)v, 
           2, scaloc, xnorm, ierr );
          if( ierr != 0 ) 
            info = 2;
          
          if( scaloc != ONE ) { 
            dscal( n2, scaloc, x, 1 );
            scale = scaloc*scale;
          }
          x[j1 - 1] = v[0][0];
          x[j2 - 1] = v[0][1];
          x[n + j1 - 1] = v[1][0];
          x[n + j2 - 1] = v[1][1];
          xmax = vmax( abs( x[j1 - 1] ) + abs( x[n + j1 - 1] ), 
           abs( x[j2 - 1] ) + abs( x[n + j2 - 1] ), xmax, 
           FEND );
          
        }
        
L_80:
        ;
      }
      
    }
    
  }
  
  return;
  
  //     End of DLAQTR
  
#undef  T
} // end of function 


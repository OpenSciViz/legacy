/*
 * C++ implementation of Lapack routine zhptri
 *
 * $Id: zhptri.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:47:57
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zhptri.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ONE = 1.0e0;
const DComplex CONE = DComplex(1.0e0);
const DComplex ZERO = DComplex(0.0e0);
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zhptri(const char &uplo, const long &n, DComplex ap[], long ipiv[], 
 DComplex work[], long &info)
{
  int upper;
  long _do0, _do1, _do2, _do3, _do4, info_, itmp, j, j_, k, 
   kc, kcnext, kp, kpc, nall;
  double ak, akp1, d, t;
  DComplex akkp1, temp;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZHPTRI computes the inverse of a DComplex Hermitian indefinite matrix
  //  A in packed storage using the factorization A = U*D*U' or A = L*D*L'
  //  computed by ZHPTRF.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the details of the factorization are stored
  //          as an upper or lower triangular matrix.
  //          = 'U':  Upper triangular (form is A = U*D*U')
  //          = 'L':  Lower triangular (form is A = L*D*L')
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  AP      (input/output) COMPLEX*16 array, dimension (N*(N+1)/2)
  //          On entry, the block diagonal matrix D and the multipliers
  //          used to obtain the factor U or L as computed by ZHPTRF,
  //          stored as a packed triangular matrix.
  
  //          On exit, if INFO = 0, the (Hermitian) inverse of the original
  //          matrix, stored as a packed triangular matrix. The j-th column
  //          of inv(A) is stored in the array AP as follows:
  //          if UPLO = 'U', AP(i + (j-1)*j/2) = inv(A)(i,j) for 1<=i<=j;
  //          if UPLO = 'L',
  //             AP(i + (j-1)*(2n-j)/2) = inv(A)(i,j) for j<=i<=n.
  
  //  IPIV    (input) INTEGER array, dimension (N)
  //          Details of the interchanges and the block structure of D
  //          as determined by ZHPTRF.
  
  //  WORK    (workspace) COMPLEX*16 array, dimension (N)
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: if INFO = k, D(k,k) = 0; the matrix is singular and its
  //               inverse could not be computed.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  if( info != 0 ) { 
    xerbla( "ZHPTRI", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  //     Check that the diagonal matrix D is nonsingular.
  
  if( upper ) { 
    
    //        Upper triangular storage: examine D from bottom to top
    
    kp = n*(n + 1)/2;
    for( info = n, info_ = info - 1; info >= 1; info--, info_-- ) { 
      if( ipiv[info_] > 0 && ctocf(ap[kp - 1]) == ctocf(ZERO) ) 
        return;
      kp = kp - info;
    }
  }
  else { 
    
    //        Lower triangular storage: examine D from top to bottom.
    
    kp = 1;
    for( info = 1, info_ = info - 1, _do0 = n; info <= _do0; info++, info_++ ) { 
      if( ipiv[info_] > 0 && ctocf(ap[kp - 1]) == ctocf(ZERO) ) 
        return;
      kp = kp + n - info + 1;
    }
  }
  info = 0;
  
  if( upper ) { 
    
    //        Compute inv(A) from the factorization A = U*D*U'.
    
    //        K is the main loop index, increasing from 1 to N in steps of
    //        1 or 2, depending on the size of the diagonal blocks.
    
    k = 1;
    kc = 1;
L_30:
    ;
    
    //        If K > N, exit from loop.
    
    if( k > n ) 
      goto L_60;
    
    if( ipiv[k - 1] > 0 ) { 
      
      //           1 x 1 diagonal block
      
      //           Invert the diagonal block.
      
      ap[kc + k - 2] = DComplex(ONE/real( ap[kc + k - 2] ));
      
      //           Compute column K of the inverse.
      
      if( k > 1 ) { 
        zcopy( k - 1, &ap[kc - 1], 1, work, 1 );
        zhpmv( uplo, k - 1, -(CONE), ap, work, 1, ZERO, &ap[kc - 1], 
         1 );
        ap[kc + k - 2] = ap[kc + k - 2] - real( zdotc( k - 
         1, work, 1, &ap[kc - 1], 1 ) );
      }
      
      //           Interchange rows and columns K and IPIV(K).
      
      kp = ipiv[k - 1];
      if( kp != k ) { 
        kpc = (kp - 1)*kp/2 + 1;
        zswap( kp, &ap[kpc - 1], 1, &ap[kc - 1], 1 );
        kpc = kc + kp - 1;
        for( j = k, j_ = j - 1, _do1 = kp; j >= _do1; j--, j_-- ) { 
          temp = conj( ap[kc + j_ - 1] );
          ap[kc + j_ - 1] = conj( ap[kpc - 1] );
          ap[kpc - 1] = temp;
          kpc = kpc - (j - 1);
        }
      }
      kc = kc + k;
      k = k + 1;
    }
    else { 
      
      //           2 x 2 diagonal block
      
      //           Invert the diagonal block.
      
      kcnext = kc + k;
      t = abs( ap[kcnext + k - 2] );
      ak = real( ap[kc + k - 2] )/t;
      akp1 = real( ap[kcnext + k - 1] )/t;
      akkp1 = ap[kcnext + k - 2]/t;
      d = t*(ak*akp1 - ONE);
      ap[kc + k - 2] = DComplex(akp1/d);
      ap[kcnext + k - 1] = DComplex(ak/d);
      ap[kcnext + k - 2] = -(akkp1/d);
      
      //           Compute columns K and K+1 of the inverse.
      
      if( k > 1 ) { 
        zcopy( k - 1, &ap[kc - 1], 1, work, 1 );
        zhpmv( uplo, k - 1, -(CONE), ap, work, 1, ZERO, &ap[kc - 1], 
         1 );
        ap[kc + k - 2] = ap[kc + k - 2] - real( zdotc( k - 
         1, work, 1, &ap[kc - 1], 1 ) );
        ap[kcnext + k - 2] = ap[kcnext + k - 2] - zdotc( k - 
         1, &ap[kc - 1], 1, &ap[kcnext - 1], 1 );
        zcopy( k - 1, &ap[kcnext - 1], 1, work, 1 );
        zhpmv( uplo, k - 1, -(CONE), ap, work, 1, ZERO, &ap[kcnext - 1], 
         1 );
        ap[kcnext + k - 1] = ap[kcnext + k - 1] - real( zdotc( k - 
         1, work, 1, &ap[kcnext - 1], 1 ) );
      }
      
      //           Interchange rows and columns K and -IPIV(K).
      
      kp = -ipiv[k - 1];
      if( kp != k ) { 
        kpc = (kp - 1)*kp/2 + 1;
        zswap( kp, &ap[kpc - 1], 1, &ap[kc - 1], 1 );
        kpc = kc + kp - 1;
        for( j = k, j_ = j - 1, _do2 = kp; j >= _do2; j--, j_-- ) { 
          temp = conj( ap[kc + j_ - 1] );
          ap[kc + j_ - 1] = conj( ap[kpc - 1] );
          ap[kpc - 1] = temp;
          kpc = kpc - (j - 1);
        }
        temp = ap[kcnext + kp - 2];
        ap[kcnext + kp - 2] = ap[kcnext + k - 2];
        ap[kcnext + k - 2] = temp;
      }
      kc = kcnext + k + 1;
      k = k + 2;
    }
    
    goto L_30;
L_60:
    ;
    
  }
  else { 
    
    //        Compute inv(A) from the factorization A = L*D*L'.
    
    //        K is the main loop index, increasing from 1 to N in steps of
    //        1 or 2, depending on the size of the diagonal blocks.
    
    nall = n*(n + 1)/2;
    k = n;
    kc = nall + 1;
L_70:
    ;
    
    //        If K < 1, exit from loop.
    
    if( k < 1 ) 
      goto L_100;
    
    kc = kc - (n - k + 1);
    if( ipiv[k - 1] > 0 ) { 
      
      //           1 x 1 diagonal block
      
      //           Invert the diagonal block.
      
      ap[kc - 1] = DComplex(ONE/real( ap[kc - 1] ));
      
      //           Compute column K of the inverse.
      
      if( k < n ) { 
        zcopy( n - k, &ap[kc], 1, work, 1 );
        zhpmv( uplo, n - k, -(CONE), &ap[kc + n - k], work, 
         1, ZERO, &ap[kc], 1 );
        ap[kc - 1] = ap[kc - 1] - real( zdotc( n - k, work, 
         1, &ap[kc], 1 ) );
      }
      
      //           Interchange rows and columns K and IPIV(K).
      
      kp = ipiv[k - 1];
      if( kp != k ) { 
        kpc = nall + 1 - (n - kp + 1)*(n - kp + 2)/2;
        itmp = kpc;
        for( j = kp, j_ = j - 1, _do3 = k; j >= _do3; j--, j_-- ) { 
          temp = conj( ap[kc + j_ - k] );
          ap[kc + j_ - k] = conj( ap[kpc - 1] );
          ap[kpc - 1] = temp;
          kpc = kpc - (n - j + 1);
        }
        kpc = itmp;
        zswap( n - kp + 1, &ap[kc + kp - k - 1], 1, &ap[kpc - 1], 
         1 );
      }
      k = k - 1;
    }
    else { 
      
      //           2 x 2 diagonal block
      
      //           Invert the diagonal block.
      
      kcnext = kc - (n - k + 2);
      t = abs( ap[kcnext] );
      ak = real( ap[kcnext - 1] )/t;
      akp1 = real( ap[kc - 1] )/t;
      akkp1 = ap[kcnext]/t;
      d = t*(ak*akp1 - ONE);
      ap[kcnext - 1] = DComplex(akp1/d);
      ap[kc - 1] = DComplex(ak/d);
      ap[kcnext] = -(akkp1/d);
      
      //           Compute columns K-1 and K of the inverse.
      
      if( k < n ) { 
        zcopy( n - k, &ap[kc], 1, work, 1 );
        zhpmv( uplo, n - k, -(CONE), &ap[kc + (n - k + 1) - 1], 
         work, 1, ZERO, &ap[kc], 1 );
        ap[kc - 1] = ap[kc - 1] - real( zdotc( n - k, work, 
         1, &ap[kc], 1 ) );
        ap[kcnext] = ap[kcnext] - zdotc( n - k, &ap[kc], 1, 
         &ap[kcnext + 1], 1 );
        zcopy( n - k, &ap[kcnext + 1], 1, work, 1 );
        zhpmv( uplo, n - k, -(CONE), &ap[kc + (n - k + 1) - 1], 
         work, 1, ZERO, &ap[kcnext + 1], 1 );
        ap[kcnext - 1] = ap[kcnext - 1] - real( zdotc( n - 
         k, work, 1, &ap[kcnext + 1], 1 ) );
      }
      
      //           Interchange rows and columns K-1 and -IPIV(K).
      
      kp = -ipiv[k - 1];
      if( kp != k ) { 
        temp = ap[kcnext];
        ap[kcnext] = ap[kcnext + kp - (k - 1) - 1];
        ap[kcnext + kp - (k - 1) - 1] = temp;
        kpc = nall + 1 - (n - kp + 1)*(n - kp + 2)/2;
        itmp = kpc;
        for( j = kp, j_ = j - 1, _do4 = k; j >= _do4; j--, j_-- ) { 
          temp = conj( ap[kc + j_ - k] );
          ap[kc + j_ - k] = conj( ap[kpc - 1] );
          ap[kpc - 1] = temp;
          kpc = kpc - (n - j + 1);
        }
        kpc = itmp;
        zswap( n - kp + 1, &ap[kc + kp - k - 1], 1, &ap[kpc - 1], 
         1 );
      }
      kc = kcnext;
      k = k - 2;
    }
    
    goto L_70;
L_100:
    ;
  }
  
  return;
  
  //     End of ZHPTRI
  
} // end of function 


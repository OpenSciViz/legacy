/*
 * C++ implementation of Lapack routine slassq
 *
 * $Id: slassq.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:00:42
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: slassq.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ slassq(const long &n, float x[], const long &incx, float &scale, 
 float &sumsq)
{
  long _do0, _do1, ix, ix_;
  float absxi;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SLASSQ  returns the values  scl  and  smsq  such that
  
  //     ( scl**2 )*smsq = x( 1 )**2 +...+ x( n )**2 + ( scale**2 )*sumsq,
  
  //  where  x( i ) = X( 1 + ( i - 1 )*INCX ). The value of  sumsq  is
  //  assumed to be non-negative and  scl  returns the value
  
  //     scl = max( scale, abs( x( i ) ) ).
  
  //  scale and sumsq must be supplied in SCALE and SUMSQ and
  //  scl and smsq are overwritten on SCALE and SUMSQ respectively.
  
  //  The routine makes only one pass through the vector x.
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The number of elements to be used from the vector X.
  
  //  X       (input) REAL
  //          The vector for which a scaled sum of squares is computed.
  //             x( i )  = X( 1 + ( i - 1 )*INCX ), 1 <= i <= n.
  
  //  INCX    (input) INTEGER
  //          The increment between successive values of the vector X.
  //          INCX > 0.
  
  //  SCALE   (input/output) REAL
  //          On entry, the value  scale  in the equation above.
  //          On exit, SCALE is overwritten with  scl , the scaling factor
  //          for the sum of squares.
  
  //  SUMSQ   (input/output) REAL
  //          On entry, the value  sumsq  in the equation above.
  //          On exit, SUMSQ is overwritten with  smsq , the basic sum of
  //          squares from which  scl  has been factored out.
  
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  if( n > 0 ) { 
    for( ix = 1, ix_ = ix - 1, _do0=docnt(ix,1 + (n - 1)*incx,_do1 = incx); _do0 > 0; ix += _do1, ix_ += _do1, _do0-- ) { 
      if( x[ix_] != ZERO ) { 
        absxi = abs( x[ix_] );
        if( scale < absxi ) { 
          sumsq = 1 + sumsq*pow(scale/absxi, 2);
          scale = absxi;
        }
        else { 
          sumsq = sumsq + pow(absxi/scale, 2);
        }
      }
    }
  }
  return;
  
  //     End of SLASSQ
  
} // end of function 


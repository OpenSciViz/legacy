/*
 * C++ implementation of lapack routine dlasy2
 *
 * $Id: dlasy2.cpp,v 1.7 1993/04/06 20:41:30 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:36:17
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dlasy2.cpp,v $
 * Revision 1.7  1993/04/06  20:41:30  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.6  1993/03/19  21:40:01  alv
 * yet another arg consted
 *
 * Revision 1.5  1993/03/19  21:28:19  alv
 * fixed constness of args
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:15:59  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:07:46  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
const double TWO = 2.0e0;
const double HALF = 0.5e0;
const double EIGHT = 8.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dlasy2(const int &ltranl, const int &ltranr, const long &isgn, 
 const long &n1, const long &n2, double *tl, const long &ldtl, double *tr, 
 const long &ldtr, double *b, const long &ldb, double &scale, double *x, 
 const long &ldx, double &xnorm, long &info)
{
#define TL(I_,J_) (*(tl+(I_)*(ldtl)+(J_)))
#define TR(I_,J_) (*(tr+(I_)*(ldtr)+(J_)))
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
#define X(I_,J_)  (*(x+(I_)*(ldx)+(J_)))
  int bswap, xswap;
  long i, i_, ip, ip_, ipiv, ipsv, j, j_, jp, jp_, jpiv[4], 
   jpsv, k, k_;
  double bet, btmp[4], eps, gam, l21, sgn, smin, smlnum, t16[4][4], 
   tau1, temp, tmp[4], u11, u12, u22, x2[2], xmax;
  static long locu12[4]={3,4,1,2};
  static long locl21[4]={2,1,4,3};
  static long locu22[4]={4,3,2,1};
  static int xswpiv[4]={FALSE,FALSE,TRUE,TRUE};
  static int bswpiv[4]={FALSE,TRUE,FALSE,TRUE};

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DLASY2 solves for the N1 by N2 matrix X, 1 <= N1,N2 <= 2, in
  
  //         op(TL)*X + ISGN*X*op(TR) = SCALE*B,
  
  //  where TL is N1 by N1, TR is N2 by N2, B is N1 by N2, and ISGN = 1 or
  //  -1.  op(T) = T or T', where T' denotes the transpose of T.
  
  //  Arguments
  //  =========
  
  //  LTRANL  (input) LOGICAL
  //          On entry, LTRANL specifies the op(TL):
  //             = .FALSE., op(TL) = TL,
  //             = .TRUE., op(TL) = TL'.
  
  //  LTRANR  (input) LOGICAL
  //          On entry, LTRANR specifies the op(TR):
  //            = .FALSE., op(TR) = TR,
  //            = .TRUE., op(TR) = TR'.
  
  //  ISGN    (input) INTEGER
  //          On entry, ISGN specifies the sign of the equation
  //          as described before. ISGN may only be 1 or -1.
  
  //  N1      (input) INTEGER
  //          On entry, N1 specifies the order of matrix TL.
  //          N1 may only be 0, 1 or 2.
  
  //  N2      (input) INTEGER
  //          On entry, N2 specifies the order of matrix TR.
  //          N2 may only be 0, 1 or 2.
  
  //  TL      (input) DOUBLE PRECISION array, dimension (LDTL,2)
  //          On entry, TL contains an N1 by N1 matrix.
  
  //  LDTL    (input) INTEGER
  //          The leading dimension of the matrix TL. LDTL >= max(1,N1).
  
  //  TR      (input) DOUBLE PRECISION array, dimension (LDTR,2)
  //          On entry, TR contains an N2 by N2 matrix.
  
  //  LDTR    (input) INTEGER
  //          The leading dimension of the matrix TR. LDTR >= max(1,N2).
  
  //  B       (input) DOUBLE PRECISION array, dimension (LDB,2)
  //          On entry, the N1 by N2 matrix B contains the right-hand
  //          side of the equation.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the matrix B. LDB >= max(1,N1).
  
  //  SCALE   (output) DOUBLE PRECISION
  //          On exit, SCALE contains the scale factor. SCALE is chosen
  //          less than or equal to 1 to prevent the solution overflowing.
  
  //  X       (output) DOUBLE PRECISION array, dimension (LDX,2)
  //          On exit, X contains the N1 by N2 solution.
  
  //  LDX     (input) INTEGER
  //          The leading dimension of the matrix X. LDX >= max(1,N1).
  
  //  XNORM   (output) DOUBLE PRECISION
  //          On exit, XNORM is the infinity-norm of the solution.
  
  //  INFO    (output) INTEGER
  //          On exit, INFO is set to
  //             0: successful exit.
  //             1: TL and TR have too close eigenvalues, so TL or
  //                TR is perturbed to get a nonsingular equation.
  //          NOTE: In the interests of speed, this routine does not
  //                check the inputs for errors.
  
  //-----------------------------------------------------------------------
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Local Arrays ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Data statements ..
  //     ..
  //     .. Executable Statements ..
  
  //     Do not check the input parameters for errors
  
  info = 0;
  
  //     Quick return if possible
  
  if( n1 == 0 || n2 == 0 ) 
    return;
  
  //     Set constants to control overflow
  
  eps = dlamch( 'P' );
  smlnum = dlamch( 'S' )/eps;
  sgn = isgn;
  
  k = n1 + n1 + n2 - 2;
  switch( k ) { 
    case 1: goto L_10;
    case 2: goto L_20;
    case 3: goto L_30;
    case 4: goto L_50;
  }
  
  //     1 by 1: TL11*X + SGN*X*TR11 = B11
  
L_10:
  ;
  tau1 = TL(0,0) + sgn*TR(0,0);
  bet = abs( tau1 );
  if( bet <= smlnum ) { 
    tau1 = smlnum;
    bet = smlnum;
    info = 1;
  }
  
  scale = ONE;
  gam = abs( B(0,0) );
  if( smlnum*gam > bet ) 
    scale = ONE/gam;
  
  X(0,0) = (B(0,0)*scale)/tau1;
  xnorm = abs( X(0,0) );
  return;
  
  //     1 by 2:
  //     TL11*[X11 X12] + ISGN*[X11 X12]*op[TR11 TR12]  = [B11 B12]
  //                                       [TR21 TR22]
  
L_20:
  ;
  
  smin = max( eps*vmax( abs( TL(0,0) ), abs( TR(0,0) ), abs( TR(1,0) ), 
   abs( TR(0,1) ), abs( TR(1,1) ), FEND ), smlnum );
  tmp[0] = TL(0,0) + sgn*TR(0,0);
  tmp[3] = TL(0,0) + sgn*TR(1,1);
  if( ltranr ) { 
    tmp[1] = sgn*TR(0,1);
    tmp[2] = sgn*TR(1,0);
  }
  else { 
    tmp[1] = sgn*TR(1,0);
    tmp[2] = sgn*TR(0,1);
  }
  btmp[0] = B(0,0);
  btmp[1] = B(1,0);
  goto L_40;
  
  //     2 by 1:
  //          op[TL11 TL12]*[X11] + ISGN* [X11]*TR11  = [B11]
  //            [TL21 TL22] [X21]         [X21]         [B21]
  
L_30:
  ;
  smin = max( eps*vmax( abs( TR(0,0) ), abs( TL(0,0) ), abs( TL(1,0) ), 
   abs( TL(0,1) ), abs( TL(1,1) ), FEND ), smlnum );
  tmp[0] = TL(0,0) + sgn*TR(0,0);
  tmp[3] = TL(1,1) + sgn*TR(0,0);
  if( ltranl ) { 
    tmp[1] = TL(1,0);
    tmp[2] = TL(0,1);
  }
  else { 
    tmp[1] = TL(0,1);
    tmp[2] = TL(1,0);
  }
  btmp[0] = B(0,0);
  btmp[1] = B(0,1);
L_40:
  ;
  
  //     Solve 2 by 2 system using complete pivoting.
  //     Set pivots less than SMIN to SMIN.
  
  ipiv = idamax( 4, tmp, 1 );
  u11 = tmp[ipiv - 1];
  if( abs( u11 ) <= smin ) { 
    info = 1;
    u11 = smin;
  }
  u12 = tmp[locu12[ipiv - 1] - 1];
  l21 = tmp[locl21[ipiv - 1] - 1]/u11;
  u22 = tmp[locu22[ipiv - 1] - 1] - u12*l21;
  xswap = xswpiv[ipiv - 1];
  bswap = bswpiv[ipiv - 1];
  if( abs( u22 ) <= smin ) { 
    info = 1;
    u22 = smin;
  }
  if( bswap ) { 
    temp = btmp[1];
    btmp[1] = btmp[0] - l21*temp;
    btmp[0] = temp;
  }
  else { 
    btmp[1] = btmp[1] - l21*btmp[0];
  }
  scale = ONE;
  if( (TWO*smlnum)*abs( btmp[1] ) > abs( u22 ) || (TWO*smlnum)*abs( btmp[0] ) > 
   abs( u11 ) ) { 
    scale = HALF/max( abs( btmp[0] ), abs( btmp[1] ) );
    btmp[0] = btmp[0]*scale;
    btmp[1] = btmp[1]*scale;
  }
  x2[1] = btmp[1]/u22;
  x2[0] = btmp[0]/u11 - (u12/u11)*x2[1];
  if( xswap ) { 
    temp = x2[1];
    x2[1] = x2[0];
    x2[0] = temp;
  }
  X(0,0) = x2[0];
  if( n1 == 1 ) { 
    X(1,0) = x2[1];
    xnorm = abs( X(0,0) ) + abs( X(1,0) );
  }
  else { 
    X(0,1) = x2[1];
    xnorm = max( abs( X(0,0) ), abs( X(0,1) ) );
  }
  return;
  
  //     2 by 2:
  //     op[TL11 TL12]*[X11 X12] +ISGN* [X11 X12]*op[TR11 TR12] = [B11 B12]
  //       [TL21 TL22] [X21 X22]        [X21 X22]   [TR21 TR22]   [B21 B22]
  
  //     Solve equivalent 4 by 4 system using complete pivoting.
  //     Set pivots less than SMIN to SMIN.
  
L_50:
  ;
  smin = vmax( abs( TR(0,0) ), abs( TR(1,0) ), abs( TR(0,1) ), abs( TR(1,1) ), 
   FEND );
  smin = vmax( smin, abs( TL(0,0) ), abs( TL(1,0) ), abs( TL(0,1) ), 
   abs( TL(1,1) ), FEND );
  smin = max( eps*smin, smlnum );
  btmp[0] = ZERO;
  dcopy( 16, btmp, 0, (double*)t16, 1 );
  t16[0][0] = TL(0,0) + sgn*TR(0,0);
  t16[1][1] = TL(1,1) + sgn*TR(0,0);
  t16[2][2] = TL(0,0) + sgn*TR(1,1);
  t16[3][3] = TL(1,1) + sgn*TR(1,1);
  if( ltranl ) { 
    t16[1][0] = TL(0,1);
    t16[0][1] = TL(1,0);
    t16[3][2] = TL(0,1);
    t16[2][3] = TL(1,0);
  }
  else { 
    t16[1][0] = TL(1,0);
    t16[0][1] = TL(0,1);
    t16[3][2] = TL(1,0);
    t16[2][3] = TL(0,1);
  }
  if( ltranr ) { 
    t16[2][0] = sgn*TR(1,0);
    t16[3][1] = sgn*TR(1,0);
    t16[0][2] = sgn*TR(0,1);
    t16[1][3] = sgn*TR(0,1);
  }
  else { 
    t16[2][0] = sgn*TR(0,1);
    t16[3][1] = sgn*TR(0,1);
    t16[0][2] = sgn*TR(1,0);
    t16[1][3] = sgn*TR(1,0);
  }
  btmp[0] = B(0,0);
  btmp[1] = B(0,1);
  btmp[2] = B(1,0);
  btmp[3] = B(1,1);
  
  //     Perform elimination
  
  for( i = 1, i_ = i - 1; i <= 3; i++, i_++ ) { 
    xmax = ZERO;
    for( ip = i, ip_ = ip - 1; ip <= 4; ip++, ip_++ ) { 
      for( jp = i, jp_ = jp - 1; jp <= 4; jp++, jp_++ ) { 
        if( abs( t16[jp_][ip_] ) >= xmax ) { 
          xmax = abs( t16[jp_][ip_] );
          ipsv = ip;
          jpsv = jp;
        }
      }
    }
    if( ipsv != i ) { 
      dswap( 4, &t16[0][ipsv - 1], 4, &t16[0][i_], 4 );
      temp = btmp[i_];
      btmp[i_] = btmp[ipsv - 1];
      btmp[ipsv - 1] = temp;
    }
    if( jpsv != i ) 
      dswap( 4, &t16[jpsv - 1][0], 1, &t16[i_][0], 1 );
    jpiv[i_] = jpsv;
    if( abs( t16[i_][i_] ) < smin ) { 
      info = 1;
      t16[i_][i_] = smin;
    }
    for( j = i + 1, j_ = j - 1; j <= 4; j++, j_++ ) { 
      t16[i_][j_] = t16[i_][j_]/t16[i_][i_];
      btmp[j_] = btmp[j_] - t16[i_][j_]*btmp[i_];
      for( k = i + 1, k_ = k - 1; k <= 4; k++, k_++ ) { 
        t16[k_][j_] = t16[k_][j_] - t16[i_][j_]*t16[k_][i_];
      }
    }
  }
  if( abs( t16[3][3] ) < smin ) 
    t16[3][3] = smin;
  scale = ONE;
  if( (((EIGHT*smlnum)*abs( btmp[0] ) > abs( t16[0][0] ) || (EIGHT*
   smlnum)*abs( btmp[1] ) > abs( t16[1][1] )) || (EIGHT*smlnum)*
   abs( btmp[2] ) > abs( t16[2][2] )) || (EIGHT*smlnum)*abs( btmp[3] ) > 
   abs( t16[3][3] ) ) { 
    scale = (ONE/EIGHT)/vmax( abs( btmp[0] ), abs( btmp[1] ), 
     abs( btmp[2] ), abs( btmp[3] ), FEND );
    btmp[0] = btmp[0]*scale;
    btmp[1] = btmp[1]*scale;
    btmp[2] = btmp[2]*scale;
    btmp[3] = btmp[3]*scale;
  }
  for( i = 1, i_ = i - 1; i <= 4; i++, i_++ ) { 
    k = 5 - i;
    temp = ONE/t16[k - 1][k - 1];
    tmp[k - 1] = btmp[k - 1]*temp;
    for( j = k + 1, j_ = j - 1; j <= 4; j++, j_++ ) { 
      tmp[k - 1] = tmp[k - 1] - (temp*t16[j_][k - 1])*tmp[j_];
    }
  }
  for( i = 1, i_ = i - 1; i <= 3; i++, i_++ ) { 
    if( jpiv[-i + 3] != 4 - i ) { 
      temp = tmp[-i + 3];
      tmp[-i + 3] = tmp[jpiv[-i + 3] - 1];
      tmp[jpiv[-i + 3] - 1] = temp;
    }
  }
  X(0,0) = tmp[0];
  X(0,1) = tmp[1];
  X(1,0) = tmp[2];
  X(1,1) = tmp[3];
  xnorm = max( abs( tmp[0] ) + abs( tmp[2] ), abs( tmp[1] ) + abs( tmp[3] ) );
  return;
  
  //     End of DLASY2
  
#undef  X
#undef  TR
#undef  TL
#undef  B
} // end of function 


/*
 * C++ implementation of lapack routine dsterf
 *
 * $Id: dsterf.cpp,v 1.5 1993/04/06 20:42:26 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:38:20
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dsterf.cpp,v $
 * Revision 1.5  1993/04/06  20:42:26  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:17:17  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:09:07  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ZERO = 0.0e0;
const double ONE = 1.0e0;
const double TWO = 2.0e0;
const long MAXIT = 30;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dsterf(const long &n, double d[], double e[], long &info)
{
  long _do0, _do1, _do2, _do3, _do4, _do5, _do6, _do7, i, i_, 
   ii, ii_, j, j_, jtot, k, l, l1, lend, lendm1, lendp1, lm1, m, 
   m_, mm1, nconv, nm1, nmaxit;
  double alpha, bb, c, eps, gamma, oldc, oldgam, p, r, rt1, rt2, 
   rte, s, sigma, tst;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DSTERF computes all eigenvalues of a symmetric tridiagonal matrix
  //  using the Pal-Walker-Kahan variant of the QL or QR algorithm.
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The number of rows and columns in the matrix.  N >= 0.
  
  //  D       (input/output) DOUBLE PRECISION array, dimension (N)
  //          On entry, D contains the diagonal elements of the
  //          tridiagonal matrix.
  //          On exit, D contains the eigenvalues in ascending order.
  //          If an error exit is made, the eigenvalues are correct
  //          but unordered for indices 1,2,...,INFO-1.
  
  //  E       (input/output) DOUBLE PRECISION array, dimension (N)
  //          On entry, E contains the subdiagonal elements of the
  //          tridiagonal matrix in positions 1 through N-1.
  //          E(N) is arbitrary.
  //          On exit, E has been destroyed.
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit.
  //          < 0:  if INFO = -i, the i-th argument had an illegal value.
  //          > 0:  if INFO = +i, the i-th eigenvalue has not converged
  //                after a total of  30*N  iterations.
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  
  //     Quick return if possible
  
  if( n < 0 ) { 
    info = -1;
    xerbla( "DSTERF", -info );
    return;
  }
  if( n <= 1 ) 
    return;
  
  //     Determine the unit roundoff for this environment.
  
  eps = dlamch( 'E' );
  
  //     Compute the eigenvalues of the tridiagonal matrix.
  
  for( i = 1, i_ = i - 1, _do0 = n - 1; i <= _do0; i++, i_++ ) { 
    e[i_] = pow(e[i_], 2);
  }
  e[n - 1] = ZERO;
  
  nmaxit = n*MAXIT;
  sigma = ZERO;
  jtot = 0;
  nconv = 0;
  
  //     Determine where the matrix splits and choose QL or QR iteration
  //     for each block, according to whether top or bottom diagonal
  //     element is smaller.
  
  l1 = 1;
  nm1 = n - 1;
  
L_20:
  ;
  if( l1 > n ) 
    goto L_160;
  if( l1 <= nm1 ) { 
    for( m = l1, m_ = m - 1, _do1 = nm1; m <= _do1; m++, m_++ ) { 
      tst = sqrt( abs( e[m_] ) );
      if( tst <= eps*(abs( d[m_] ) + abs( d[m_ + 1] )) ) 
        goto L_40;
    }
  }
  m = n;
  
L_40:
  ;
  l = l1;
  lend = m;
  if( abs( d[lend - 1] ) < abs( d[l - 1] ) ) { 
    l = lend;
    lend = l1;
  }
  l1 = m + 1;
  
  if( lend >= l ) { 
    
    //        QL Iteration
    
    //        Look for small subdiagonal element.
    
L_50:
    ;
    if( l != lend ) { 
      lendm1 = lend - 1;
      for( m = l, m_ = m - 1, _do2 = lendm1; m <= _do2; m++, m_++ ) { 
        tst = sqrt( abs( e[m_] ) );
        if( tst <= eps*(abs( d[m_] ) + abs( d[m_ + 1] )) ) 
          goto L_70;
      }
    }
    
    m = lend;
    
L_70:
    ;
    p = d[l - 1];
    if( m == l ) 
      goto L_90;
    
    //        If remaining matrix is 2 by 2, use DLAE2 to compute its
    //        eigenvalues.
    
    if( m == l + 1 ) { 
      rte = sqrt( e[l - 1] );
      dlae2( d[l - 1], rte, d[l], rt1, rt2 );
      d[l - 1] = rt1;
      d[l] = rt2;
      nconv = nconv + 2;
      l = l + 2;
      if( l <= lend ) 
        goto L_50;
      goto L_20;
    }
    
    if( jtot == nmaxit ) 
      goto L_150;
    jtot = jtot + 1;
    
    //        Form shift.
    
    rte = sqrt( e[l - 1] );
    sigma = (d[l] - p)/(TWO*rte);
    r = dlapy2( sigma, ONE );
    sigma = p - (rte/(sigma + sign( r, sigma )));
    
    c = ONE;
    s = ZERO;
    gamma = d[m - 1] - sigma;
    p = gamma*gamma;
    
    //        Inner loop
    
    mm1 = m - 1;
    for( i = mm1, i_ = i - 1, _do3 = l; i >= _do3; i--, i_-- ) { 
      bb = e[i_];
      r = p + bb;
      e[i_ + 1] = s*r;
      oldc = c;
      c = p/r;
      s = bb/r;
      oldgam = gamma;
      alpha = d[i_];
      gamma = c*(alpha - sigma) - s*oldgam;
      d[i_ + 1] = oldgam + (alpha - gamma);
      if( c != ZERO ) { 
        p = (gamma*gamma)/c;
      }
      else { 
        p = oldc*bb;
      }
    }
    
    e[l - 1] = s*p;
    d[l - 1] = sigma + gamma;
    e[m - 1] = ZERO;
    goto L_50;
    
    //        Eigenvalue found.
    
L_90:
    ;
    d[l - 1] = p;
    nconv = nconv + 1;
    
    l = l + 1;
    if( l <= lend ) 
      goto L_50;
    goto L_20;
    
  }
  else { 
    
    //        QR Iteration
    
    //        Look for small superdiagonal element.
    
L_100:
    ;
    if( l != lend ) { 
      lendp1 = lend + 1;
      for( m = l, m_ = m - 1, _do4 = lendp1; m >= _do4; m--, m_-- ) { 
        tst = sqrt( abs( e[m_ - 1] ) );
        if( tst <= eps*(abs( d[m_] ) + abs( d[m_ - 1] )) ) 
          goto L_120;
      }
    }
    
    m = lend;
    
L_120:
    ;
    p = d[l - 1];
    if( m == l ) 
      goto L_140;
    
    //        If remaining matrix is 2 by 2, use DLAE2 to compute its
    //        eigenvalues.
    
    if( m == l - 1 ) { 
      rte = sqrt( e[l - 2] );
      dlae2( d[l - 1], rte, d[l - 2], rt1, rt2 );
      d[l - 1] = rt1;
      d[l - 2] = rt2;
      nconv = nconv + 2;
      l = l - 2;
      if( l >= lend ) 
        goto L_100;
      goto L_20;
    }
    
    if( jtot == nmaxit ) 
      goto L_150;
    jtot = jtot + 1;
    
    //        Form shift.
    
    rte = sqrt( e[l - 2] );
    sigma = (d[l - 2] - p)/(TWO*rte);
    r = dlapy2( sigma, ONE );
    sigma = p - (rte/(sigma + sign( r, sigma )));
    
    c = ONE;
    s = ZERO;
    gamma = d[m - 1] - sigma;
    p = gamma*gamma;
    
    //        Inner loop
    
    lm1 = l - 1;
    for( i = m, i_ = i - 1, _do5 = lm1; i <= _do5; i++, i_++ ) { 
      bb = e[i_];
      r = p + bb;
      if( i != 1 ) 
        e[i_ - 1] = s*r;
      oldc = c;
      c = p/r;
      s = bb/r;
      oldgam = gamma;
      alpha = d[i_ + 1];
      gamma = c*(alpha - sigma) - s*oldgam;
      d[i_] = oldgam + (alpha - gamma);
      if( c != ZERO ) { 
        p = (gamma*gamma)/c;
      }
      else { 
        p = oldc*bb;
      }
    }
    
    e[lm1 - 1] = s*p;
    d[l - 1] = sigma + gamma;
    if( m != 1 ) 
      e[m - 2] = ZERO;
    goto L_100;
    
    //        Eigenvalue found.
    
L_140:
    ;
    d[l - 1] = p;
    nconv = nconv + 1;
    
    l = l - 1;
    if( l >= lend ) 
      goto L_100;
    goto L_20;
    
  }
  
  //     Set error -- no convergence to an eigenvalue after a total
  //     of N*MAXIT iterations.
  
L_150:
  ;
  info = nconv;
  return;
  
  //     Sort eigenvalues in increasing order.
  
L_160:
  ;
  for( ii = 2, ii_ = ii - 1, _do6 = n; ii <= _do6; ii++, ii_++ ) { 
    i = ii - 1;
    k = i;
    p = d[i - 1];
    for( j = ii, j_ = j - 1, _do7 = n; j <= _do7; j++, j_++ ) { 
      if( d[j_] < p ) { 
        k = j;
        p = d[j_];
      }
    }
    if( k != i ) { 
      d[k - 1] = d[i - 1];
      d[i - 1] = p;
    }
  }
  
  return;
  
  //     End of DSTERF
  
} // end of function 


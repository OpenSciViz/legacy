/*
 * C++ implementation of lapack routine dgbtf2
 *
 * $Id: dgbtf2.cpp,v 1.5 1993/04/06 20:40:21 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:33:43
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dgbtf2.cpp,v $
 * Revision 1.5  1993/04/06  20:40:21  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:14:26  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:06:27  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ONE = 1.0e0;
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dgbtf2(const long &m, const long &n, const long &kl, const long &ku, 
 double *ab, const long &ldab, long ipiv[], long &info)
{
#define AB(I_,J_) (*(ab+(I_)*(ldab)+(J_)))
  long _do0, _do1, _do2, _do3, i, i_, j, j_, jp, ju, km, kv;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DGBTF2 computes an LU factorization of a real m-by-n band matrix A
  //  using partial pivoting with row interchanges.
  
  //  This is the unblocked version of the algorithm, calling Level 2 BLAS.
  
  //  Arguments
  //  =========
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix A.  M >= 0.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix A.  N >= 0.
  
  //  KL      (input) INTEGER
  //          The number of subdiagonals within the band of A.  KL >= 0.
  
  //  KU      (input) INTEGER
  //          The number of superdiagonals within the band of A.  KU >= 0.
  
  //  AB      (input/output) DOUBLE PRECISION array, dimension (LDAB,N)
  //          On entry, the matrix A in band storage, in rows KL+1 to
  //          2*KL+KU+1; rows 1 to KL of the array need not be set.
  //          The j-th column of A is stored in the j-th column of the
  //          array AB as follows:
  //          AB(kl+ku+1+i-j,j) = A(i,j) for max(1,j-ku)<=i<=min(m,j+kl)
  
  //          On exit, details of the factorization: U is stored as an
  //          upper triangular band matrix with KL+KU superdiagonals in
  //          rows 1 to KL+KU+1, and the multipliers used during the
  //          factorization are stored in rows KL+KU+2 to 2*KL+KU+1.
  //          See below for further details.
  
  //  LDAB    (input) INTEGER
  //          The leading dimension of the array AB.  LDAB >= 2*KL+KU+1.
  
  //  IPIV    (output) INTEGER array, dimension (min(M,N))
  //          The pivot indices; for 1 <= i <= min(M,N), row i of the
  //          matrix was interchanged with row IPIV(i).
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument had an illegal value
  //          > 0: if INFO = +i, U(i,i) is exactly zero. The factorization
  //               has been completed, but the factor U is exactly
  //               singular, and division by zero will occur if it is used
  //               to solve a system of equations.
  
  //  Further Details
  //  ===============
  
  //  The band storage scheme is illustrated by the following example, when
  //  M = N = 6, KL = 2, KU = 1:
  
  //  On entry:                       On exit:
  
  //      *    *    *    +    +    +       *    *    *   u14  u25  u36
  //      *    *    +    +    +    +       *    *   u13  u24  u35  u46
  //      *   a12  a23  a34  a45  a56      *   u12  u23  u34  u45  u56
  //     a11  a22  a33  a44  a55  a66     u11  u22  u33  u44  u55  u66
  //     a21  a32  a43  a54  a65   *      m21  m32  m43  m54  m65   *
  //     a31  a42  a53  a64   *    *      m31  m42  m53  m64   *    *
  
  //  Array elements marked * are not used by the routine; elements marked
  //  + need not be set on entry, but are required by the routine to store
  //  elements of U, because of fill-in resulting from the row
  //  interchanges.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     KV is the number of superdiagonals in the factor U, allowing for
  //     fill-in.
  
  kv = ku + kl;
  
  //     Test the input parameters.
  
  info = 0;
  if( m < 0 ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( kl < 0 ) { 
    info = -3;
  }
  else if( ku < 0 ) { 
    info = -4;
  }
  else if( ldab < kl + kv + 1 ) { 
    info = -6;
  }
  if( info != 0 ) { 
    xerbla( "DGBTF2", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( m == 0 || n == 0 ) 
    return;
  
  //     Gaussian elimination with partial pivoting
  
  //     Set fill-in elements in columns KU+2 to KV to zero.
  
  for( j = ku + 2, j_ = j - 1, _do0 = min( kv, n ); j <= _do0; j++, j_++ ) { 
    for( i = kv - j + 2, i_ = i - 1, _do1 = kl; i <= _do1; i++, i_++ ) { 
      AB(j_,i_) = ZERO;
    }
  }
  
  //     JU is the index of the last column affected by the current stage
  //     of the factorization.
  
  ju = 1;
  
  for( j = 1, j_ = j - 1, _do2 = min( m, n ); j <= _do2; j++, j_++ ) { 
    
    //        Set fill-in elements in column J+KV to zero.
    
    if( j + kv <= n ) { 
      for( i = 1, i_ = i - 1, _do3 = kl; i <= _do3; i++, i_++ ) { 
        AB(j_ + kv,i_) = ZERO;
      }
    }
    
    //        Find pivot and test for singularity. KM is the number of
    //        subdiagonal elements in the current column.
    
    km = min( kl, m - j );
    jp = idamax( km + 1, &AB(j_,kv), 1 );
    ipiv[j_] = jp + j - 1;
    if( AB(j_,kv + jp - 1) != ZERO ) { 
      ju = max( ju, min( j + ku + jp - 1, n ) );
      
      //           Apply interchange to columns J to JU.
      
      if( jp != 1 ) 
        dswap( ju - j + 1, &AB(j_,kv + jp - 1), ldab - 1, 
         &AB(j_,kv), ldab - 1 );
      
      if( km > 0 ) { 
        
        //              Compute multipliers.
        
        dscal( km, ONE/AB(j_,kv), &AB(j_,kv + 1), 1 );
        
        //              Update trailing submatrix within the band.
        
        if( ju > j ) 
          dger( km, ju - j, -ONE, &AB(j_,kv + 1), 1, &AB(j_ + 1,kv - 1), 
           ldab - 1, &AB(j_ + 1,kv), ldab - 1 );
      }
    }
    else { 
      
      //           If pivot is zero, set INFO to the index of the pivot
      //           unless a zero pivot has already been found.
      
      if( info == 0 ) 
        info = j;
    }
  }
  return;
  
  //     End of DGBTF2
  
#undef  AB
} // end of function 


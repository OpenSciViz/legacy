/*
 * C++ implementation of lapack routine dormtr
 *
 * $Id: dormtr.cpp,v 1.5 1993/04/06 20:41:49 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:37:05
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dormtr.cpp,v $
 * Revision 1.5  1993/04/06  20:41:49  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:16:27  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:08:19  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

RWLAPKDECL void /*FUNCTION*/ dormtr(const char &side, const char &uplo, const char &trans, const long &m, 
   const long &n, double *a, const long &lda, double tau[], double *c, 
   const long &ldc, double work[], const long &lwork, long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define C(I_,J_)  (*(c+(I_)*(ldc)+(J_)))
  int left, upper;
  long i1, i2, iinfo, mi, ni, nq, nw;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DORMTR overwrites the general real m by n matrix C with
  
  //        Q * C  if SIDE = 'L' and TRANS = 'N', or
  
  //        Q'* C  if SIDE = 'L' and TRANS = 'T', or
  
  //        C * Q  if SIDE = 'R' and TRANS = 'N', or
  
  //        C * Q' if SIDE = 'R' and TRANS = 'T',
  
  //  where Q is a real orthogonal matrix of order nq, with nq = m if
  //  SIDE = 'L' and nq = n if SIDE = 'R'. Q is defined as the product of
  //  nq-1 elementary reflectors, as returned by DSYTRD:
  
  //  if UPLO = 'U', Q = H(nq-1) . . . H(2) H(1);
  
  //  if UPLO = 'L', Q = H(1) H(2) . . . H(nq-1).
  
  //  Arguments
  //  =========
  
  //  SIDE    (input) CHARACTER*1
  //          = 'L': apply Q or Q' from the Left
  //          = 'R': apply Q or Q' from the Right
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangle of the array A
  //          holds details of the elementary reflectors, as returned by
  //          DSYTRD:
  //          = 'U': Upper triangle;
  //          = 'L': Lower triangle.
  
  //  TRANS   (input) CHARACTER*1
  //          = 'N': apply Q  (No transpose)
  //          = 'T': apply Q' (Transpose)
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix C. M >= 0.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix C. N >= 0.
  
  //  A       (input) DOUBLE PRECISION array, dimension
  //                               (LDA,M) if SIDE = 'L'
  //                               (LDA,N) if SIDE = 'R'
  //          The vectors which define the elementary reflectors, as
  //          returned by DSYTRD.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.
  //          LDA >= max(1,M) if SIDE = 'L'; LDA >= max(1,N) if SIDE = 'R'.
  
  //  TAU     (input) DOUBLE PRECISION array, dimension
  //                               (M-1) if SIDE = 'L'
  //                               (N-1) if SIDE = 'R'
  //          TAU(i) must contain the scalar factor of the elementary
  //          reflector H(i), as returned by DSYTRD.
  
  //  C       (input/output) DOUBLE PRECISION array, dimension (LDC,N)
  //          On entry, the m-by-n matrix C.
  //          On exit, C is overwritten by Q*C or Q'*C or C*Q' or C*Q.
  
  //  LDC     (input) INTEGER
  //          The leading dimension of the array C. LDC >= max(1,M).
  
  //  WORK    (workspace) DOUBLE PRECISION array, dimension (LWORK)
  //          On exit, if INFO = 0, WORK(1) returns the minimum value of
  //          LWORK required to use the optimal blocksize.
  
  //  LWORK   (input) INTEGER
  //          The dimension of the array WORK.
  //          If SIDE = 'L', LWORK >= max(1,N);
  //          if SIDE = 'R', LWORK >= max(1,M).
  //          For optimum performance LWORK should be at least N*NB
  //          if SIDE = 'L' and at least M*NB if SIDE = 'R', where NB is
  //          the optimal blocksize.
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input arguments
  
  info = 0;
  left = lsame( side, 'L' );
  upper = lsame( uplo, 'U' );
  
  //     NQ is the order of Q and NW is the minimum dimension of WORK
  
  if( left ) { 
    nq = m;
    nw = n;
  }
  else { 
    nq = n;
    nw = m;
  }
  if( !left && !lsame( side, 'R' ) ) { 
    info = -1;
  }
  else if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -2;
  }
  else if( !lsame( trans, 'N' ) && !lsame( trans, 'T' ) ) { 
    info = -3;
  }
  else if( m < 0 ) { 
    info = -4;
  }
  else if( n < 0 ) { 
    info = -5;
  }
  else if( lda < max( 1, nq ) ) { 
    info = -7;
  }
  else if( ldc < max( 1, m ) ) { 
    info = -10;
  }
  else if( lwork < max( 1, nw ) ) { 
    info = -12;
  }
  if( info != 0 ) { 
    xerbla( "DORMTR", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( (m == 0 || n == 0) || nq == 1 ) { 
    work[0] = 1;
    return;
  }
  
  if( left ) { 
    mi = m - 1;
    ni = n;
  }
  else { 
    mi = m;
    ni = n - 1;
  }
  
  if( upper ) { 
    
    //        Q was determined by a call to DSYTRD with UPLO = 'U'
    
    dormql( side, trans, mi, ni, nq - 1, &A(1,0), lda, tau, c, 
     ldc, work, lwork, iinfo );
  }
  else { 
    
    //        Q was determined by a call to DSYTRD with UPLO = 'L'
    
    if( left ) { 
      i1 = 2;
      i2 = 1;
    }
    else { 
      i1 = 1;
      i2 = 2;
    }
    dormqr( side, trans, mi, ni, nq - 1, &A(0,1), lda, tau, &C(i2 - 1,i1 - 1), 
     ldc, work, lwork, iinfo );
  }
  return;
  
  //     End of DORMTR
  
#undef  C
#undef  A
} // end of function 


/*
 * C++ implementation of Lapack routine zlarf
 *
 * $Id: zlarf.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:48:58
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zlarf.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const DComplex ONE = DComplex(1.0e0);
const DComplex ZERO = DComplex(0.0e0);
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zlarf(const char &side, const long &m, const long &n, DComplex v[], 
 const long &incv, const DComplex &tau, DComplex *c, const long &ldc, DComplex work[])
{
#define C(I_,J_)  (*(c+(I_)*(ldc)+(J_)))

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZLARF applies a DComplex elementary reflector H to a DComplex m by n
  //  matrix C, from either the left or the right. H is represented in the
  //  form
  
  //        H = I - tau * v * v'
  
  //  where tau is a DComplex scalar and v is a DComplex vector.
  
  //  If tau = 0, then H is taken to be the unit matrix.
  
  //  To apply H' (the conjugate transpose of H), supply conjg(tau) instead
  //  tau.
  
  //  Arguments
  //  =========
  
  //  SIDE    (input) CHARACTER*1
  //          = 'L': form  H * C
  //          = 'R': form  C * H
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix C.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix C.
  
  //  V       (input) COMPLEX*16 array, dimension
  //                     (1 + (M-1)*abs(INCV)) if SIDE = 'L'
  //                  or (1 + (N-1)*abs(INCV)) if SIDE = 'R'
  //          The vector v in the representation of H. V is not used if
  //          TAU = 0.
  
  //  INCV    (input) INTEGER
  //          The increment between elements of v. INCV <> 0.
  
  //  TAU     (input) COMPLEX*16
  //          The value tau in the representation of H.
  
  //  C       (input/output) COMPLEX*16 array, dimension (LDC,N)
  //          On entry, the m-by-n matrix C.
  //          On exit, C is overwritten by the matrix H * C if SIDE = 'L',
  //          or C * H if SIDE = 'R'.
  
  //  LDC     (input) INTEGER
  //          The leading dimension of the array C. LDA >= max(1,M).
  
  //  WORK    (workspace) COMPLEX*16 array, dimension
  //                         (N) if SIDE = 'L'
  //                      or (M) if SIDE = 'R'
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Executable Statements ..
  
  if( lsame( side, 'L' ) ) { 
    
    //        Form  H * C
    
    if( ctocf(tau) != ctocf(ZERO) ) { 
      
      //           w := C' * v
      
      zgemv( 'C'/*Conjugate transpose*/, m, n, ONE, c, ldc, 
       v, incv, ZERO, work, 1 );
      
      //           C := C - v * w'
      
      zgerc( m, n, -(tau), v, incv, work, 1, c, ldc );
    }
  }
  else { 
    
    //        Form  C * H
    
    if( ctocf(tau) != ctocf(ZERO) ) { 
      
      //           w := C * v
      
      zgemv( 'N'/*No transpose*/, m, n, ONE, c, ldc, v, incv, 
       ZERO, work, 1 );
      
      //           C := C - w * v'
      
      zgerc( m, n, -(tau), work, 1, v, incv, c, ldc );
    }
  }
  return;
  
  //     End of ZLARF
  
#undef  C
} // end of function 


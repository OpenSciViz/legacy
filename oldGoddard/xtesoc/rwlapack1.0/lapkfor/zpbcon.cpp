/*
 * C++ implementation of Lapack routine zpbcon
 *
 * $Id: zpbcon.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:49:39
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zpbcon.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ONE = 1.0e0;
const double ZERO = 0.0e0;
// end of PARAMETER translations

inline double zpbcon_cabs1(DComplex zdum) { return abs( real( (zdum) ) ) + 
   abs( imag( (zdum) ) ); }
RWLAPKDECL void /*FUNCTION*/ zpbcon(const char &uplo, const long &n, const long &kd, DComplex *ab, 
 const long &ldab, const double &anorm, double &rcond, DComplex work[], double rwork[], 
 long &info)
{
#define AB(I_,J_) (*(ab+(I_)*(ldab)+(J_)))
  int upper;
  char normin;
  long ix, kase;
  double ainvnm, scale, scalel, scaleu, smlnum;
  DComplex zdum;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZPBCON estimates the reciprocal of the condition number of a DComplex
  //  Hermitian positive definite band matrix using the Cholesky
  //  factorization A = U'*U or A = L*L' computed by ZPBTRF.
  
  //  An estimate is obtained for norm(inv(A)), and the reciprocal of the
  //  condition number is computed as RCOND = 1 / (ANORM * norm(inv(A))).
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the factor stored in AB is upper or lower
  //          triangular.
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  KD      (input) INTEGER
  //          The number of super-diagonals of the matrix A if UPLO = 'U',
  //          or the number of sub-diagonals if UPLO = 'L'.  KD >= 0.
  
  //  AB      (input) COMPLEX*16 array, dimension (LDAB,N)
  //          The triangular factor U or L from the Cholesky factorization
  //          A = U'*U or A = L*L' of the band matrix A, stored in the
  //          first KD+1 rows of the array.  The j-th column of U or L is
  //          stored in the array AB as follows:
  //          if UPLO ='U', AB(kd+1+i-j,j) = U(i,j) for max(1,j-kd)<=i<=j;
  //          if UPLO ='L', AB(1+i-j,j)    = L(i,j) for j<=i<=min(n,j+kd).
  
  //  LDAB    (input) INTEGER
  //          The leading dimension of the array AB.  LDAB >= KD+1.
  
  //  ANORM   (input) DOUBLE PRECISION
  //          The 1-norm (or infinity-norm) of the Hermitian band matrix A.
  
  //  RCOND   (output) DOUBLE PRECISION
  //          The reciprocal of the condition number of the matrix A,
  //          computed as RCOND = 1/(ANORM * AINVNM), where AINVNM is an
  //          estimate of the 1-norm of inv(A) computed in this routine.
  
  //  WORK    (workspace) COMPLEX*16 array, dimension (2*N)
  
  //  RWORK   (workspace) DOUBLE PRECISION array, dimension (N)
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Statement Functions ..
  //     ..
  //     .. Statement Function definitions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( kd < 0 ) { 
    info = -3;
  }
  else if( ldab < kd + 1 ) { 
    info = -5;
  }
  else if( anorm < ZERO ) { 
    info = -6;
  }
  if( info != 0 ) { 
    xerbla( "ZPBCON", -info );
    return;
  }
  
  //     Quick return if possible
  
  rcond = ZERO;
  if( n == 0 ) { 
    rcond = ONE;
    return;
  }
  else if( anorm == ZERO ) { 
    return;
  }
  
  smlnum = dlamch( 'S'/*Safe minimum*/ );
  
  //     Estimate the 1-norm of the inverse.
  
  kase = 0;
  normin = 'N';
L_10:
  ;
  zlacon( n, &work[n], work, ainvnm, kase );
  if( kase != 0 ) { 
    if( upper ) { 
      
      //           Multiply by inv(U').
      
      zlatbs( 'U'/*Upper*/, 'C'/*Conjugate transpose*/, 'N'/*Non-unit*/
       , normin, n, kd, ab, ldab, work, scalel, rwork, info );
      normin = 'Y';
      
      //           Multiply by inv(U).
      
      zlatbs( 'U'/*Upper*/, 'N'/*No transpose*/, 'N'/*Non-unit*/
       , normin, n, kd, ab, ldab, work, scaleu, rwork, info );
    }
    else { 
      
      //           Multiply by inv(L).
      
      zlatbs( 'L'/*Lower*/, 'N'/*No transpose*/, 'N'/*Non-unit*/
       , normin, n, kd, ab, ldab, work, scalel, rwork, info );
      normin = 'Y';
      
      //           Multiply by inv(L').
      
      zlatbs( 'L'/*Lower*/, 'C'/*Conjugate transpose*/, 'N'/*Non-unit*/
       , normin, n, kd, ab, ldab, work, scaleu, rwork, info );
    }
    
    //        Multiply by 1/SCALE if doing so will not cause overflow.
    
    scale = scalel*scaleu;
    if( scale != ONE ) { 
      ix = izamax( n, work, 1 );
      if( scale < zpbcon_cabs1( work[ix - 1] )*smlnum || scale == 
       ZERO ) 
        goto L_20;
      zdrscl( n, scale, work, 1 );
    }
    goto L_10;
  }
  
  //     Compute the estimate of the reciprocal condition number.
  
  if( ainvnm != ZERO ) 
    rcond = (ONE/ainvnm)/anorm;
  
L_20:
  ;
  
  return;
  
  //     End of ZPBCON
  
#undef  AB
} // end of function 


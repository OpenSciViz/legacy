/*
 * C++ implementation of Lapack routine ztrsyl
 *
 * $Id: ztrsyl.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:51:24
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: ztrsyl.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ONE = 1.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ ztrsyl(const char &trana, const char &tranb, const long &isgn, 
 const long &m, const long &n, DComplex *a, const long &lda, DComplex *b, 
 const long &ldb, DComplex *c, const long &ldc, double &scale, long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
#define C(I_,J_)  (*(c+(I_)*(ldc)+(J_)))
  int notrna, notrnb;
  long _do0, _do1, _do2, _do3, _do4, _do5, _do6, _do7, j, j_, 
   k, k_, l, l_;
  double bignum, da11, db, dum[1], eps, scaloc, sgn, smin, smlnum;
  DComplex a11, suml, sumr, vec, x11;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZTRSYL solves the DComplex Sylvester matrix equation:
  
  //     op(A)*X + X*op(B) = scale*C or
  //     op(A)*X - X*op(B) = scale*C,
  
  //  where op(A) = A or A**H, and A and B are both upper triangular. A is
  //  m-by-m and B is n-by-n; the right hand side C and the solution X are
  //  m-by-n; and scale is an output scale factor, set <= 1 to avoid
  //  overflow in X.
  
  //  Arguments
  //  =========
  
  //  TRANA   (input) CHARACTER*1
  //          Specifies the option op(A):
  //          = 'N': op(A) = A    (No transpose)
  //          = 'C': op(A) = A**H (Conjugate transpose)
  
  //  TRANB   (input) CHARACTER*1
  //          Specifies the option op(B):
  //          = 'N': op(B) = B    (No transpose)
  //          = 'C': op(B) = B**H (Conjugate transpose)
  
  //  ISGN    (input) INTEGER
  //          Specifies the sign in the equation:
  //          = +1: solve op(A)*X + X*op(B) = scale*C
  //          = -1: solve op(A)*X - X*op(B) = scale*C
  
  //  M       (input) INTEGER
  //          The order of the matrix A, and the number of rows in the
  //          matrices X and C. M >= 0.
  
  //  N       (input) INTEGER
  //          The order of the matrix B, and the number of columns in the
  //          matrices X and C. N >= 0.
  
  //  A       (input) COMPLEX*16 array, dimension (LDA,M)
  //          The upper triangular matrix A.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A. LDA >= max(1,M).
  
  //  B       (input) COMPLEX*16 array, dimension (LDB,N)
  //          The upper triangular matrix B.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B. LDB >= max(1,N).
  
  //  C       (input/output) COMPLEX*16 array, dimension (LDC,N)
  //          On entry, the m-by-n right hand side matrix C.
  //          On exit, C is overwritten by the solution matrix X.
  
  //  LDC     (input) INTEGER
  //          The leading dimension of the array C. LDC >= max(1,M)
  
  //  SCALE   (output) DOUBLE PRECISION
  //          The scale factor, scale, set <= 1 to avoid overflow in X.
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument had an illegal value
  //          = 1: A and B have common or very close eigenvalues; perturbed
  //               values were used to solve the equation (but the matrices
  //               A and B are unchanged).
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Local Arrays ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Decode and Test input parameters
  
  notrna = lsame( trana, 'N' );
  notrnb = lsame( tranb, 'N' );
  
  info = 0;
  if( (!notrna && !lsame( trana, 'T' )) && !lsame( trana, 'C' ) ) { 
    info = -1;
  }
  else if( (!notrnb && !lsame( tranb, 'T' )) && !lsame( tranb, 'C' )
    ) { 
    info = -2;
  }
  else if( isgn != 1 && isgn != -1 ) { 
    info = -3;
  }
  else if( m < 0 ) { 
    info = -4;
  }
  else if( n < 0 ) { 
    info = -5;
  }
  else if( lda < max( 1, m ) ) { 
    info = -7;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -9;
  }
  else if( ldc < max( 1, m ) ) { 
    info = -11;
  }
  if( info != 0 ) { 
    xerbla( "ZTRSYL", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( m == 0 || n == 0 ) 
    return;
  
  //     Set constants to control overflow
  
  eps = dlamch( 'P' );
  smlnum = dlamch( 'S' );
  bignum = ONE/smlnum;
  dlabad( smlnum, bignum );
  smlnum = smlnum*(double)( m*n )/eps;
  bignum = ONE/smlnum;
  smin = vmax( smlnum, eps*zlange( 'M', m, m, a, lda, dum ), eps*
   zlange( 'M', n, n, b, ldb, dum ), FEND );
  scale = ONE;
  sgn = isgn;
  
  if( notrna && notrnb ) { 
    
    //        Solve    A*X + ISGN*X*B = scale*C.
    
    //        The (K,L)th block of X is determined starting from
    //        bottom-left corner column by column by
    
    //            A(K,K)*X(K,L) + ISGN*X(K,L)*B(L,L) = C(K,L) - R(K,L)
    
    //        Where
    //                    M                        L-1
    //          R(K,L) = SUM [A(K,I)*X(I,L)] +ISGN*SUM [X(K,J)*B(J,L)].
    //                  I=K+1                      J=1
    
    for( l = 1, l_ = l - 1, _do0 = n; l <= _do0; l++, l_++ ) { 
      for( k = m, k_ = k - 1; k >= 1; k--, k_-- ) { 
        
        suml = zdotu( m - k, &A(min( k + 1, m ) - 1,k_), lda, 
         &C(l_,min( k + 1, m ) - 1), 1 );
        sumr = zdotu( l - 1, &C(0,k_), ldc, &B(l_,0), 1 );
        vec = C(l_,k_) - (suml + sgn*sumr);
        
        scaloc = ONE;
        a11 = A(k_,k_) + sgn*B(l_,l_);
        da11 = abs( real( a11 ) ) + abs( imag( a11 ) );
        if( da11 <= smin ) { 
          a11 = DComplex(smin);
          da11 = smin;
          info = 1;
        }
        db = abs( real( vec ) ) + abs( imag( vec ) );
        if( da11 < ONE && db > ONE ) { 
          if( db > bignum*da11 ) 
            scaloc = ONE/db;
        }
        x11 = (vec*DComplex( scaloc, 0. ))/a11;
        
        if( scaloc != ONE ) { 
          for( j = 1, j_ = j - 1, _do1 = n; j <= _do1; j++, j_++ ) { 
            zdscal( m, scaloc, &C(j_,0), 1 );
          }
          scale = scale*scaloc;
        }
        C(l_,k_) = x11;
        
      }
    }
    
  }
  else if( !notrna && notrnb ) { 
    
    //        Solve    A' *X + ISGN*X*B = scale*C.
    
    //        The (K,L)th block of X is determined starting from
    //        upper-left corner column by column by
    
    //            A'(K,K)*X(K,L) + ISGN*X(K,L)*B(L,L) = C(K,L) - R(K,L)
    
    //        Where
    //                   K-1                         L-1
    //          R(K,L) = SUM [A'(I,K)*X(I,L)] + ISGN*SUM [X(K,J)*B(J,L)]
    //                   I=1                         J=1
    
    for( l = 1, l_ = l - 1, _do2 = n; l <= _do2; l++, l_++ ) { 
      for( k = 1, k_ = k - 1, _do3 = m; k <= _do3; k++, k_++ ) { 
        
        suml = zdotc( k - 1, &A(k_,0), 1, &C(l_,0), 1 );
        sumr = zdotu( l - 1, &C(0,k_), ldc, &B(l_,0), 1 );
        vec = C(l_,k_) - (suml + sgn*sumr);
        
        scaloc = ONE;
        a11 = conj( A(k_,k_) ) + sgn*B(l_,l_);
        da11 = abs( real( a11 ) ) + abs( imag( a11 ) );
        if( da11 <= smin ) { 
          a11 = DComplex(smin);
          da11 = smin;
          info = 1;
        }
        db = abs( real( vec ) ) + abs( imag( vec ) );
        if( da11 < ONE && db > ONE ) { 
          if( db > bignum*da11 ) 
            scaloc = ONE/db;
        }
        
        x11 = (vec*DComplex( scaloc, 0. ))/a11;
        
        if( scaloc != ONE ) { 
          for( j = 1, j_ = j - 1, _do4 = n; j <= _do4; j++, j_++ ) { 
            zdscal( m, scaloc, &C(j_,0), 1 );
          }
          scale = scale*scaloc;
        }
        C(l_,k_) = x11;
        
      }
    }
    
  }
  else if( !notrna && !notrnb ) { 
    
    //        Solve    A'*X + ISGN*X*B' = C.
    
    //        The (K,L)th block of X is determined starting from
    //        upper-right corner column by column by
    
    //            A'(K,K)*X(K,L) + ISGN*X(K,L)*B'(L,L) = C(K,L) - R(K,L)
    
    //        Where
    //                    K-1
    //           R(K,L) = SUM [A'(I,K)*X(I,L)] +
    //                    I=1
    //                           N
    //                     ISGN*SUM [X(K,J)*B'(L,J)].
    //                          J=L+1
    
    for( l = n, l_ = l - 1; l >= 1; l--, l_-- ) { 
      for( k = 1, k_ = k - 1, _do5 = m; k <= _do5; k++, k_++ ) { 
        
        suml = zdotc( k - 1, &A(k_,0), 1, &C(l_,0), 1 );
        sumr = zdotc( n - l, &C(min( l + 1, n ) - 1,k_), ldc, 
         &B(min( l + 1, n ) - 1,l_), ldb );
        vec = C(l_,k_) - (suml + sgn*conj( sumr ));
        
        scaloc = ONE;
        a11 = conj( A(k_,k_) + sgn*B(l_,l_) );
        da11 = abs( real( a11 ) ) + abs( imag( a11 ) );
        if( da11 <= smin ) { 
          a11 = DComplex(smin);
          da11 = smin;
          info = 1;
        }
        db = abs( real( vec ) ) + abs( imag( vec ) );
        if( da11 < ONE && db > ONE ) { 
          if( db > bignum*da11 ) 
            scaloc = ONE/db;
        }
        
        x11 = (vec*DComplex( scaloc, 0. ))/a11;
        
        if( scaloc != ONE ) { 
          for( j = 1, j_ = j - 1, _do6 = n; j <= _do6; j++, j_++ ) { 
            zdscal( m, scaloc, &C(j_,0), 1 );
          }
          scale = scale*scaloc;
        }
        C(l_,k_) = x11;
        
      }
    }
    
  }
  else if( notrna && !notrnb ) { 
    
    //        Solve    A*X + ISGN*X*B' = C.
    
    //        The (K,L)th block of X is determined starting from
    //        bottom-left corner column by column by
    
    //           A(K,K)*X(K,L) + ISGN*X(K,L)*B'(L,L) = C(K,L) - R(K,L)
    
    //        Where
    //                    M                          N
    //          R(K,L) = SUM [A(K,I)*X(I,L)] + ISGN*SUM [X(K,J)*B'(L,J)]
    //                  I=K+1                      J=L+1
    
    for( l = n, l_ = l - 1; l >= 1; l--, l_-- ) { 
      for( k = m, k_ = k - 1; k >= 1; k--, k_-- ) { 
        
        suml = zdotu( m - k, &A(min( k + 1, m ) - 1,k_), lda, 
         &C(l_,min( k + 1, m ) - 1), 1 );
        sumr = zdotc( n - l, &C(min( l + 1, n ) - 1,k_), ldc, 
         &B(min( l + 1, n ) - 1,l_), ldb );
        vec = C(l_,k_) - (suml + sgn*conj( sumr ));
        
        scaloc = ONE;
        a11 = A(k_,k_) + sgn*conj( B(l_,l_) );
        da11 = abs( real( a11 ) ) + abs( imag( a11 ) );
        if( da11 <= smin ) { 
          a11 = DComplex(smin);
          da11 = smin;
          info = 1;
        }
        db = abs( real( vec ) ) + abs( imag( vec ) );
        if( da11 < ONE && db > ONE ) { 
          if( db > bignum*da11 ) 
            scaloc = ONE/db;
        }
        
        x11 = (vec*DComplex( scaloc, 0. ))/a11;
        
        if( scaloc != ONE ) { 
          for( j = 1, j_ = j - 1, _do7 = n; j <= _do7; j++, j_++ ) { 
            zdscal( m, scaloc, &C(j_,0), 1 );
          }
          scale = scale*scaloc;
        }
        C(l_,k_) = x11;
        
      }
    }
    
  }
  
  return;
  
  //     End of ZTRSYL
  
#undef  C
#undef  B
#undef  A
} // end of function 


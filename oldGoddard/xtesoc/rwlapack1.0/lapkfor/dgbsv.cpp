/*
 * C++ implementation of lapack routine dgbsv
 *
 * $Id: dgbsv.cpp,v 1.6 1993/04/06 20:40:20 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:33:41
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dgbsv.cpp,v $
 * Revision 1.6  1993/04/06  20:40:20  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.5  1993/03/19  18:41:23  alv
 * now passes chars explicitly, rather than indirection of a string, to shut up SUN warnings
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:14:24  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:06:25  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

RWLAPKDECL void /*FUNCTION*/ dgbsv(const long &n, const long &kl, const long &ku, const long &nrhs, 
   double *ab, const long &ldab, long ipiv[], double *b, const long &ldb, 
   long &info)
{
#define AB(I_,J_) (*(ab+(I_)*(ldab)+(J_)))
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))

  
  //  -- LAPACK driver routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DGBSV computes the solution to a real system of linear equations
  //     A * X = B,
  //  where A is a band matrix of order N with KL subdiagonals and KU
  //  superdiagonals, and X and B are N by NRHS matrices.
  
  //  The LU decomposition with partial pivoting and row interchanges is
  //  used to factor A as
  //     A = L * U,
  //  where L is a product of permutation and unit lower triangular
  //  matrices with KL subdiagonals, and U is upper triangular with KL+KU
  //  superdiagonals.  The factored form of A is then used to solve the
  //  system of equations A * X = B.
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The number of linear equations, i.e., the order of the
  //          matrix A.  N >= 0.
  
  //  KL      (input) INTEGER
  //          The number of subdiagonals within the band of A.  KL >= 0.
  
  //  KU      (input) INTEGER
  //          The number of superdiagonals within the band of A.  KU >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right hand sides, i.e., the number of columns
  //          of the matrix B.  NRHS >= 0.
  
  //  AB      (input/output) DOUBLE PRECISION array, dimension (LDAB,N)
  //          On entry, the matrix A in band storage, in rows KL+1 to
  //          2*KL+KU+1; rows 1 to KL of the array need not be set.
  //          The j-th column of A is stored in the j-th column of the
  //          array AB as follows:
  //          AB(KL+KU+1+i-j,j) = A(i,j) for max(1,j-KU)<=i<=min(N,j+KL)
  
  //          On exit, details of the factorization: U is stored as an
  //          upper triangular band matrix with KL+KU superdiagonals in
  //          rows 1 to KL+KU+1, and the multipliers used during the
  //          factorization are stored in rows KL+KU+2 to 2*KL+KU+1.
  //          See below for further details.
  
  //  LDAB    (input) INTEGER
  //          The leading dimension of the array AB.  LDAB >= 2*KL+KU+1.
  
  //  IPIV    (output) INTEGER array, dimension (N)
  //          The pivot indices that define the permutation matrix P;
  //          row i of the matrix was interchanged with row IPIV(i).
  
  //  B       (input/output) DOUBLE PRECISION array, dimension (LDB,NRHS)
  //          On entry, the N by NRHS matrix of right hand side vectors B
  //          for the system of equations A*X = B.
  //          On exit, if INFO = 0, the N by NRHS matrix of solution
  //          vectors X.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: if INFO = k, U(k,k) is exactly zero.  The factorization
  //               has been completed, but the factor U is exactly
  //               singular, and the solution has not been computed.
  
  //  Further Details
  //  ===============
  
  //  The band storage scheme is illustrated by the following example, when
  //  M = N = 6, KL = 2, KU = 1:
  
  //  On entry:                       On exit:
  
  //      *    *    *    +    +    +       *    *    *   u14  u25  u36
  //      *    *    +    +    +    +       *    *   u13  u24  u35  u46
  //      *   a12  a23  a34  a45  a56      *   u12  u23  u34  u45  u56
  //     a11  a22  a33  a44  a55  a66     u11  u22  u33  u44  u55  u66
  //     a21  a32  a43  a54  a65   *      m21  m32  m43  m54  m65   *
  //     a31  a42  a53  a64   *    *      m31  m42  m53  m64   *    *
  
  //  Array elements marked * are not used by the routine; elements marked
  //  + need not be set on entry, but are required by the routine to store
  //  elements of U because of fill-in resulting from the row interchanges.
  
  //  =====================================================================
  
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  if( n < 0 ) { 
    info = -1;
  }
  else if( kl < 0 ) { 
    info = -2;
  }
  else if( ku < 0 ) { 
    info = -3;
  }
  else if( nrhs < 0 ) { 
    info = -4;
  }
  else if( ldab < 2*kl + ku + 1 ) { 
    info = -6;
  }
  else if( ldb < max( n, 1 ) ) { 
    info = -9;
  }
  if( info != 0 ) { 
    xerbla( "DGBSV ", -info );
    return;
  }
  
  //     Compute the LU factorization of the band matrix A.
  
  dgbtrf( n, n, kl, ku, ab, ldab, ipiv, info );
  if( info == 0 ) { 
    
    //        Solve the system A*X = B, overwriting B with X.
    
    dgbtrs( 'N'/* No transpose */, n, kl, ku, nrhs, ab, ldab, ipiv, 
     b, ldb, info );
  }
  return;
  
  //     End of DGBSV
  
#undef  B
#undef  AB
} // end of function 


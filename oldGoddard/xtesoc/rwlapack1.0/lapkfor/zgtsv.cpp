/*
 * C++ implementation of Lapack routine zgtsv
 *
 * $Id: zgtsv.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:47:05
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zgtsv.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const DComplex ZERO = DComplex(0.0e0);
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zgtsv(const long &n, const long &nrhs, DComplex dl[], 
 DComplex d[], DComplex du[], DComplex *b, const long &ldb, long &info)
{
#define B(I_,J_)  (*(b+(I_)*(ldb)+(J_)))
  long _do0, _do1, _do2, _do3, j, j_, k, k_;
  DComplex mult, temp;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZGTSV  solves the equation
  
  //     A*X = B,
  
  //  where A is an n by n tridiagonal matrix, by Gaussian elimination with
  //  partial pivoting.
  
  //  Note that the equation  A'*X = B  may be solved by interchanging the
  //  order of the arguments DU and DL.
  
  //  Arguments
  //  =========
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  NRHS    (input) INTEGER
  //          The number of right hand sides, i.e., the number of columns
  //          of the matrix B.  NRHS >= 0.
  
  //  DL      (input/output) COMPLEX*16 array, dimension (N-1)
  //          On entry, DL must contain the (n-1) sub-diagonal elements of
  //          A.
  
  //          On exit, DL is overwritten by the (n-2) elements of the
  //          second super-diagonal of the upper triangular matrix U from
  //          the LU factorization of A, in DL(1), ..., DL(n-2).
  
  //  D       (input/output) COMPLEX*16 array, dimension (N)
  //          On entry, D must contain the diagonal elements of A.
  
  //          On exit, D is overwritten by the n diagonal elements of U.
  
  //  DU      (input/output) COMPLEX*16 array, dimension (N-1)
  //          On entry, DU must contain the (n-1) super-diagonal elements
  //          of A.
  
  //          On exit, DU is overwritten by the (n-1) elements of the first
  //          super-diagonal of U.
  
  //  B       (input/output) COMPLEX*16 array, dimension (LDB,N)
  //          On entry, the N by NRHS matrix of right hand side vectors B.
  
  //          On exit, if INFO = 0, the N by NRHS matrix of solution
  //          vectors X.
  
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  
  //  INFO    (output)
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: if INFO = k, U(k,k) is exactly zero, and the solution
  //               has not been computed.  The factorization has not been
  //               completed unless INFO = N.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Executable Statements ..
  
  info = 0;
  if( n < 0 ) { 
    info = -1;
  }
  else if( nrhs < 0 ) { 
    info = -2;
  }
  else if( ldb < max( 1, n ) ) { 
    info = -7;
  }
  if( info != 0 ) { 
    xerbla( "ZGTSV ", -info );
    return;
  }
  
  if( n == 0 ) 
    return;
  
  for( k = 1, k_ = k - 1, _do0 = n - 1; k <= _do0; k++, k_++ ) { 
    if( ctocf(dl[k_]) == ctocf(ZERO) ) { 
      
      //           Subdiagonal is zero, no elimination is required.
      
      if( ctocf(d[k_]) == ctocf(ZERO) ) { 
        
        //              Diagonal is zero: set INFO = K and return; a unique
        //              solution can not be found.
        
        info = k;
        return;
      }
    }
    else if( abs( d[k_] ) >= abs( dl[k_] ) ) { 
      
      //           No row interchange required
      
      mult = dl[k_]/d[k_];
      d[k_ + 1] = d[k_ + 1] - mult*du[k_];
      for( j = 1, j_ = j - 1, _do1 = nrhs; j <= _do1; j++, j_++ ) { 
        B(j_,k_ + 1) = B(j_,k_ + 1) - mult*B(j_,k_);
      }
      if( k < (n - 1) ) 
        dl[k_] = ZERO;
    }
    else { 
      
      //           Interchange rows K and K+1
      
      mult = d[k_]/dl[k_];
      d[k_] = dl[k_];
      temp = d[k_ + 1];
      d[k_ + 1] = du[k_] - mult*temp;
      if( k < (n - 1) ) { 
        dl[k_] = du[k_ + 1];
        du[k_ + 1] = -(mult*dl[k_]);
      }
      du[k_] = temp;
      for( j = 1, j_ = j - 1, _do2 = nrhs; j <= _do2; j++, j_++ ) { 
        temp = B(j_,k_);
        B(j_,k_) = B(j_,k_ + 1);
        B(j_,k_ + 1) = temp - mult*B(j_,k_ + 1);
      }
    }
  }
  if( ctocf(d[n - 1]) == ctocf(ZERO) ) { 
    info = n;
    return;
  }
  
  //     Back solve with the matrix U from the factorization.
  
  for( j = 1, j_ = j - 1, _do3 = nrhs; j <= _do3; j++, j_++ ) { 
    B(j_,n - 1) = B(j_,n - 1)/d[n - 1];
    if( n > 1 ) 
      B(j_,n - 2) = (B(j_,n - 2) - du[n - 2]*B(j_,n - 1))/d[n - 2];
    for( k = n - 2, k_ = k - 1; k >= 1; k--, k_-- ) { 
      B(j_,k_) = (B(j_,k_) - du[k_]*B(j_,k_ + 1) - dl[k_]*B(j_,k_ + 2))/
       d[k_];
    }
  }
  
  return;
  
  //     End of ZGTSV
  
#undef  B
} // end of function 


/*
 * C++ implementation of Lapack routine zlahqr
 *
 * $Id: zlahqr.cpp,v 1.2 1993/07/21 22:22:14 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:48:23
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zlahqr.cpp,v $
 * Revision 1.2  1993/07/21  22:22:14  alv
 * ported to Microsoft visual C++
 *
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const DComplex ZERO = DComplex(0.0e0);
const DComplex ONE = DComplex(1.0e0);
const double RZERO = 0.0e0;
const double RONE = 1.0e0;
const double HALF = 0.5e0;
// end of PARAMETER translations

inline double zlahqr_cabs1(DComplex cdum) { return abs( real( (cdum) ) ) + 
   abs( imag( (cdum) ) ); }
RWLAPKDECL void /*FUNCTION*/ zlahqr(const int &wantt, const int &wantz, const long &n, 
 const long &ilo, const long &ihi, DComplex *h, const long &ldh, DComplex w[], 
 const long &iloz, const long &ihiz, DComplex *z, const long &ldz, long &info)
{
#define H(I_,J_)  (*(h+(I_)*(ldh)+(J_)))
#define Z(I_,J_)  (*(z+(I_)*(ldz)+(J_)))
  long _do0, _do1, _do2, _do3, _do4, _do5, _do6, _do7, i, i1, 
   i2, itn, its, its_, j, j_, k, k_, l, m, m_, nh, nz;
  double h10, h21, ovfl, rtemp, rwork[1], s, smlnum, t2, tst1, ulp, 
   unfl;
  DComplex cdum, h11, h11s, h22, sum, t, t1, temp, u, v[2], v2, x, 
   y;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZLAHQR is an auxiliary routine called by ZHSEQR to update the
  //  eigenvalues and Schur decomposition already computed by ZHSEQR, by
  //  dealing with the Hessenberg submatrix in rows and columns ILO to IHI.
  
  //  Arguments
  //  =========
  
  //  WANTT   (input) LOGICAL
  //          = .TRUE. : the full Schur form T is required;
  //          = .FALSE.: only eigenvalues are required.
  
  //  WANTZ   (input) LOGICAL
  //          = .TRUE. : the matrix of Schur vectors Z is required;
  //          = .FALSE.: Schur vectors are not required.
  
  //  N       (input) INTEGER
  //          The order of the matrix H.  N >= 0.
  
  //  ILO     (input) INTEGER
  //  IHI     (input) INTEGER
  //          It is assumed that H is already upper triangular in rows and
  //          columns IHI+1:N, and that H(ILO,ILO-1) = 0 (unless ILO = 1).
  //          ZLAHQR works primarily with the Hessenberg submatrix in rows
  //          and columns ILO to IHI, but applies transformations to all of
  //          H if WANTT is .TRUE..
  //          1 <= ILO <= max(1,IHI); IHI <= N.
  
  //  H       (input/output) COMPLEX*16 array, dimension (LDH,N)
  //          On entry, the upper Hessenberg matrix H.
  //          On exit, if WANTT is .TRUE., H is upper triangular in rows
  //          and columns ILO:IHI, with any 2-by-2 diagonal blocks in
  //          standard form. If WANTT is .FALSE., the contents of H are
  //          unspecified on exit.
  
  //  LDH     (input) INTEGER
  //          The leading dimension of the array H. LDH >= max(1,N).
  
  //  W       (output) COMPLEX*16 array, dimension (N)
  //          The computed eigenvalues ILO to IHI are stored in the
  //          corresponding elements of W. If WANTT is .TRUE., the
  //          eigenvalues are stored in the same order as on the diagonal
  //          of the Schur form returned in H, with W(i) = H(i,i).
  
  //  ILOZ    (input) INTEGER
  //  IHIZ    (input) INTEGER
  //          Specify the rows of Z to which transformations must be
  //          applied if WANTZ is .TRUE..
  //          1 <= ILOZ <= ILO; IHI <= IHIZ <= N.
  
  //  Z       (input/output) COMPLEX*16 array, dimension (LDZ,N)
  //          If WANTZ is .TRUE., on entry Z must contain the current
  //          matrix Z of transformations accumulated by ZHSEQR, and on
  //          exit Z has been updated; transformations are applied only to
  //          the submatrix Z(ILOZ:IHIZ,ILO:IHI).
  //          If WANTZ is .FALSE., Z is not referenced.
  
  //  LDZ     (input) INTEGER
  //          The leading dimension of the array Z. LDZ >= max(1,N).
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          > 0: ZLAHQR failed to compute all the eigenvalues ILO to IHI
  //               in a total of 30*(IHI-ILO+1) iterations; if INFO = i,
  //               elements i+1:ihi of W contain those eigenvalues which
  //               have been successfully computed.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. Local Arrays ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Statement Functions ..
  //     ..
  //     .. Statement Function definitions ..
  //     ..
  //     .. Executable Statements ..
  
  info = 0;
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  if( ilo == ihi ) { 
    w[ilo - 1] = H(ilo - 1,ilo - 1);
    return;
  }
  
  nh = ihi - ilo + 1;
  nz = ihiz - iloz + 1;
  
  //     Set machine-dependent constants for the stopping criterion.
  //     If norm(H) <= sqrt(OVFL), overflow should not occur.
  
  unfl = dlamch( 'S'/*Safe minimum*/ );
  ovfl = RONE/unfl;
  dlabad( unfl, ovfl );
  ulp = dlamch( 'P'/*Precision*/ );
  smlnum = unfl*(nh/ulp);
  
  //     I1 and I2 are the indices of the first row and last column of H
  //     to which transformations must be applied. If eigenvalues only are
  //     being computed, I1 and I2 are set inside the main loop.
  
  if( wantt ) { 
    i1 = 1;
    i2 = n;
  }
  
  //     ITN is the total number of QR iterations allowed.
  
  itn = 30*nh;
  
  //     The main loop begins here. I is the loop index and decreases from
  //     IHI to ILO in steps of 1. Each iteration of the loop works
  //     with the active submatrix in rows and columns L to I.
  //     Eigenvalues I+1 to IHI have already converged. Either L = ILO, or
  //     H(L,L-1) is negligible so that the matrix splits.
  
  i = ihi;
L_10:
  ;
  if( i < ilo ) 
    goto L_130;
  
  //     Perform QR iterations on rows and columns ILO to I until a
  //     submatrix of order 1 splits off at the bottom because a
  //     subdiagonal element has become negligible.
  
  l = ilo;
  for( its = 0, its_ = its - 1, _do0 = itn; its <= _do0; its++, its_++ ) { 
    
    //        Look for a single small subdiagonal element.
    
    for( k = i, k_ = k - 1, _do1 = l + 1; k >= _do1; k--, k_-- ) { 
      tst1 = zlahqr_cabs1( H(k_ - 1,k_ - 1) ) + zlahqr_cabs1( H(k_,k_) );
      if( tst1 == RZERO ) 
        tst1 = zlanhs( '1', i - l + 1, &H(l - 1,l - 1), ldh, 
         rwork );
      if( abs( real( H(k_ - 1,k_) ) ) <= max( ulp*tst1, smlnum ) ) 
        goto L_30;
    }
L_30:
    ;
    l = k;
    if( l > ilo ) { 
      
      //           H(L,L-1) is negligible
      
      H(l - 2,l - 1) = ZERO;
    }
    
    //        Exit from loop if a submatrix of order 1 has split off.
    
    if( l >= i ) 
      goto L_120;
    
    //        Now the active submatrix is in rows and columns L to I. If
    //        eigenvalues only are being computed, only the active submatrix
    //        need be transformed.
    
    if( !wantt ) { 
      i1 = l;
      i2 = i;
    }
    
    if( its == 10 || its == 20 ) { 
      
      //           Exceptional shift.
      
      t = DComplex(abs( real( H(i - 2,i - 1) ) ) + abs( real( H(i - 3,i - 2) ) ));
    }
    else { 
      
      //           Wilkinson's shift.
      
      t = H(i - 1,i - 1);
      u = H(i - 1,i - 2)*real( H(i - 2,i - 1) );
      if( ctocf(u) != ctocf(ZERO) ) { 
        x = HALF*(H(i - 2,i - 2) - t);
        y = sqrt( x*x + u );
        if( real( x )*real( y ) + imag( x )*imag( y ) < RZERO ) 
          y = -(y);
        t = t - zladiv( u, x + y );
      }
    }
    
    //        Look for two consecutive small subdiagonal elements.
    
    for( m = i - 1, m_ = m - 1, _do2 = l; m >= _do2; m--, m_-- ) { 
      
      //           Determine the effect of starting the single-shift QR
      //           iteration at row M, and see if this would make H(M,M-1)
      //           negligible.
      
      h11 = H(m_,m_);
      h22 = H(m_ + 1,m_ + 1);
      h11s = h11 - t;
      h21 = real(H(m_,m_ + 1));
      s = zlahqr_cabs1( h11s ) + abs( h21 );
      h11s = h11s/s;
      h21 = h21/s;
      v[0] = h11s;
      v[1] = DComplex(h21);
      if( m == l ) 
        goto L_50;
      h10 = real(H(m_ - 1,m_));
      tst1 = zlahqr_cabs1( h11s )*(zlahqr_cabs1( h11 ) + zlahqr_cabs1( h22 ));
      if( abs( h10*h21 ) <= ulp*tst1 ) 
        goto L_50;
    }
L_50:
    ;
    
    //        Single-shift QR step
    
    for( k = m, k_ = k - 1, _do3 = i - 1; k <= _do3; k++, k_++ ) { 
      
      //           The first iteration of this loop determines a reflection G
      //           from the vector V and applies it from left and right to H,
      //           thus creating a nonzero bulge below the subdiagonal.
      
      //           Each subsequent iteration determines a reflection G to
      //           restore the Hessenberg form in the (K-1)th column, and thus
      //           chases the bulge one step toward the bottom of the active
      //           submatrix.
      
      //           V(2) is always real before the call to ZLARFG, and hence
      //           after the call T2 ( = T1*V(2) ) is also real.
      
      if( k > m ) 
        zcopy( 2, &H(k_ - 1,k_), 1, v, 1 );
      zlarfg( 2, v[0], &v[1], 1, t1 );
      if( k > m ) { 
        H(k_ - 1,k_) = v[0];
        H(k_ - 1,k_ + 1) = ZERO;
      }
      v2 = v[1];
      t2 = real( t1*v2 );
      
      //           Apply G from the left to transform the rows of the matrix
      //           in columns K to I2.
      
      for( j = k, j_ = j - 1, _do4 = i2; j <= _do4; j++, j_++ ) { 
        sum = conj( t1 )*H(j_,k_) + t2*H(j_,k_ + 1);
        H(j_,k_) = H(j_,k_) - sum;
        H(j_,k_ + 1) = H(j_,k_ + 1) - sum*v2;
      }
      
      //           Apply G from the right to transform the columns of the
      //           matrix in rows I1 to min(K+2,I).
      
      for( j = i1, j_ = j - 1, _do5 = min( k + 2, i ); j <= _do5; j++, j_++ ) { 
        sum = t1*H(k_,j_) + t2*H(k_ + 1,j_);
        H(k_,j_) = H(k_,j_) - sum;
        H(k_ + 1,j_) = H(k_ + 1,j_) - sum*conj( v2 );
      }
      
      if( wantz ) { 
        
        //              Accumulate transformations in the matrix Z
        
        for( j = iloz, j_ = j - 1, _do6 = ihiz; j <= _do6; j++, j_++ ) { 
          sum = t1*Z(k_,j_) + t2*Z(k_ + 1,j_);
          Z(k_,j_) = Z(k_,j_) - sum;
          Z(k_ + 1,j_) = Z(k_ + 1,j_) - sum*conj( v2 );
        }
      }
      
      if( k == m && m > l ) { 
        
        //              If the QR step was started at row M > L because two
        //              consecutive small subdiagonals were found, then extra
        //              scaling must be performed to ensure that H(M,M-1) remains
        //              real.
        
        temp = ONE - t1;
        temp = temp/dlapy2( real( temp ), imag( temp ) );
        H(m - 1,m) = H(m - 1,m)*conj( temp );
        if( m + 2 <= i ) 
          H(m,m + 1) = H(m,m + 1)*temp;
        for( j = m, j_ = j - 1, _do7 = i; j <= _do7; j++, j_++ ) { 
          if( j != m + 1 ) { 
            if( i2 > j ) 
              zscal( i2 - j, temp, &H(j_ + 1,j_), ldh );
            zscal( j - i1, conj( temp ), &H(j_,i1 - 1), 
             1 );
            if( wantz ) { 
              zscal( nz, conj( temp ), &Z(j_,iloz - 1), 
               1 );
            }
          }
        }
      }
    }
    
    //        Ensure that H(I,I-1) is real.
    
    temp = H(i - 2,i - 1);
    if( imag( temp ) != RZERO ) { 
      rtemp = dlapy2( real( temp ), imag( temp ) );
      H(i - 2,i - 1) = (DComplex)(rtemp);
      temp = temp/rtemp;
      if( i2 > i ) 
        zscal( i2 - i, conj( temp ), &H(i,i - 1), ldh );
      zscal( i - i1, temp, &H(i - 1,i1 - 1), 1 );
      if( wantz ) { 
        zscal( nz, temp, &Z(i - 1,iloz - 1), 1 );
      }
    }
    
  }
  
  //     Failure to converge in remaining number of iterations
  
  info = i;
  return;
  
L_120:
  ;
  
  //     H(I,I-1) is negligible: one eigenvalue has converged.
  
  w[i - 1] = H(i - 1,i - 1);
  
  //     Decrement number of remaining iterations, and return to start of
  //     the main loop with new value of I.
  
  itn = itn - its;
  i = l - 1;
  goto L_10;
  
L_130:
  ;
  return;
  
  //     End of ZLAHQR
  
#undef  Z
#undef  H
} // end of function 


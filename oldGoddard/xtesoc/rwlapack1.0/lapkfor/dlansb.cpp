/*
 * C++ implementation of lapack routine dlansb
 *
 * $Id: dlansb.cpp,v 1.4 1993/04/06 20:41:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:35:28
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dlansb.cpp,v $
 * Revision 1.4  1993/04/06  20:41:05  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.3  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.2  1993/03/05  23:15:27  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:07:19  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ONE = 1.0e0;
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL double /*FUNCTION*/ dlansb(const char &norm, const char &uplo, const long &n, const long &k, 
 double *ab, const long &ldab, double work[])
{
#define AB(I_,J_) (*(ab+(I_)*(ldab)+(J_)))
  long _do0, _do1, _do10, _do11, _do2, _do3, _do4, _do5, _do6, 
   _do7, _do8, _do9, i, i_, j, j_, l;
  double absa, dlansb_v, scale, sum, value;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DLANSB  returns the value of the one norm,  or the Frobenius norm, or
  //  the  infinity norm,  or the element of  largest absolute value  of an
  //  n by n symmetric band matrix A,  with k super-diagonals.
  
  //  Description
  //  ===========
  
  //  DLANSB returns the value
  
  //     DLANSB = ( max(abs(A(i,j))), NORM = 'M' or 'm'
  //              (
  //              ( norm1(A),         NORM = '1', 'O' or 'o'
  //              (
  //              ( normI(A),         NORM = 'I' or 'i'
  //              (
  //              ( normF(A),         NORM = 'F', 'f', 'E' or 'e'
  
  //  where  norm1  denotes the  one norm of a matrix (maximum column sum),
  //  normI  denotes the  infinity norm  of a matrix  (maximum row sum) and
  //  normF  denotes the  Frobenius norm of a matrix (square root of sum of
  //  squares).  Note that  max(abs(A(i,j)))  is not a  matrix norm.
  
  //  Arguments
  //  =========
  
  //  NORM    (input) CHARACTER*1
  //          Specifies the value to be returned in DLANSB as described
  //          above.
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangular part of the
  //          band matrix A is supplied.
  //          = 'U':  Upper triangular part is supplied
  //          = 'L':  Lower triangular part is supplied
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.  When N = 0, DLANSB is
  //          set to zero.
  
  //  K       (input) INTEGER
  //          The number of super-diagonals or sub-diagonals of the
  //          band matrix A.  K >= 0.
  
  //  AB      (input) DOUBLE PRECISION array, dimension (LDAB,N)
  //          The upper or lower triangle of the symmetric band matrix A,
  //          stored in the first K+1 rows of AB.  The j-th column of A is
  //          stored in the j-th column of the array AB as follows:
  //          if UPLO = 'U', AB(k+1+i-j,j) = A(i,j) for max(1,j-k)<=i<=j;
  //          if UPLO = 'L', AB(1+i-j,j)   = A(i,j) for j<=i<=min(n,j+k).
  
  //  LDAB    (input) INTEGER
  //          The leading dimension of the array AB.  LDAB >= K+1.
  
  //  WORK    (workspace) DOUBLE PRECISION array, dimension (LWORK),
  //          where LWORK >= N when NORM = 'I' or '1' or 'O'; otherwise,
  //          WORK is not referenced.
  
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  if( n == 0 ) { 
    value = ZERO;
  }
  else if( lsame( norm, 'M' ) ) { 
    
    //        Find max(abs(A(i,j))).
    
    value = ZERO;
    if( lsame( uplo, 'U' ) ) { 
      for( j = 1, j_ = j - 1, _do0 = n; j <= _do0; j++, j_++ ) { 
        for( i = max( k + 2 - j, 1 ), i_ = i - 1, _do1 = k + 
         1; i <= _do1; i++, i_++ ) { 
          value = max( value, abs( AB(j_,i_) ) );
        }
      }
    }
    else { 
      for( j = 1, j_ = j - 1, _do2 = n; j <= _do2; j++, j_++ ) { 
        for( i = 1, i_ = i - 1, _do3 = min( n + 1 - j, k + 
         1 ); i <= _do3; i++, i_++ ) { 
          value = max( value, abs( AB(j_,i_) ) );
        }
      }
    }
  }
  else if( ((lsame( norm, 'I' )) || (lsame( norm, 'O' ))) || (norm == 
   '1') ) { 
    
    //        Find normI(A) ( = norm1(A), since A is symmetric).
    
    value = ZERO;
    if( lsame( uplo, 'U' ) ) { 
      for( j = 1, j_ = j - 1, _do4 = n; j <= _do4; j++, j_++ ) { 
        sum = ZERO;
        l = k + 1 - j;
        for( i = max( 1, j - k ), i_ = i - 1, _do5 = j - 1; i <= _do5; i++, i_++ ) { 
          absa = abs( AB(j_,l + i_) );
          sum = sum + absa;
          work[i_] = work[i_] + absa;
        }
        work[j_] = sum + abs( AB(j_,k) );
      }
      for( i = 1, i_ = i - 1, _do6 = n; i <= _do6; i++, i_++ ) { 
        value = max( value, work[i_] );
      }
    }
    else { 
      for( i = 1, i_ = i - 1, _do7 = n; i <= _do7; i++, i_++ ) { 
        work[i_] = ZERO;
      }
      for( j = 1, j_ = j - 1, _do8 = n; j <= _do8; j++, j_++ ) { 
        sum = work[j_] + abs( AB(j_,0) );
        l = 1 - j;
        for( i = j + 1, i_ = i - 1, _do9 = min( n, j + k ); i <= _do9; i++, i_++ ) { 
          absa = abs( AB(j_,l + i_) );
          sum = sum + absa;
          work[i_] = work[i_] + absa;
        }
        value = max( value, sum );
      }
    }
  }
  else if( (lsame( norm, 'F' )) || (lsame( norm, 'E' )) ) { 
    
    //        Find normF(A).
    
    scale = ZERO;
    sum = ONE;
    if( k > 0 ) { 
      if( lsame( uplo, 'U' ) ) { 
        for( j = 2, j_ = j - 1, _do10 = n; j <= _do10; j++, j_++ ) { 
          dlassq( min( j - 1, k ), &AB(j_,max( k + 2 - j, 1 ) - 1), 
           1, scale, sum );
        }
        l = k + 1;
      }
      else { 
        for( j = 1, j_ = j - 1, _do11 = n - 1; j <= _do11; j++, j_++ ) { 
          dlassq( min( n - j, k ), &AB(j_,1), 1, scale, 
           sum );
        }
        l = 1;
      }
      sum = 2*sum;
    }
    else { 
      l = 1;
    }
    dlassq( n, &AB(0,l - 1), ldab, scale, sum );
    value = scale*sqrt( sum );
  }
  
  dlansb_v = value;
  return( dlansb_v );
  
  //     End of DLANSB
  
#undef  AB
} // end of function 


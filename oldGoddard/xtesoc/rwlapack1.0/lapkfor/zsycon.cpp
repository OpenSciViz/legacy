/*
 * C++ implementation of Lapack routine zsycon
 *
 * $Id: zsycon.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:50:47
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zsycon.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const double ONE = 1.0e0;
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zsycon(const char &uplo, const long &n, DComplex *a, const long &lda, 
 long ipiv[], const double &anorm, double &rcond, DComplex work[], long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  int upper;
  long _do0, i, i_, kase;
  double ainvnm;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZSYCON estimates the reciprocal of the condition number of a DComplex
  //  symmetric matrix A using the factorization A = U*D*U' or A = L*D*L'
  //  computed by ZSYTRF.
  
  //  An estimate is obtained for norm(inv(A)), and the reciprocal of the
  //  condition number is computed as RCOND = 1 / (ANORM * norm(inv(A))).
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the details of the factorization are stored
  //          as an upper or lower triangular matrix.
  //          = 'U':  Upper triangular (form is A = U*D*U')
  //          = 'L':  Lower triangular (form is A = L*D*L')
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  A       (input) COMPLEX*16 array, dimension (LDA,N)
  //          The block diagonal matrix D and the multipliers used to
  //          obtain the factor U or L as computed by ZSYTRF.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  IPIV    (input) INTEGER array, dimension (N)
  //          Details of the interchanges and the block structure of D
  //          as determined by ZSYTRF.
  
  //  ANORM   (input) DOUBLE PRECISION
  //          The 1-norm of the original matrix A.
  
  //  RCOND   (output) DOUBLE PRECISION
  //          The reciprocal of the condition number of the matrix A,
  //          computed as RCOND = 1/(ANORM * AINVNM), where AINVNM is an
  //          estimate of the 1-norm of inv(A) computed in this routine.
  
  //  WORK    (workspace) COMPLEX*16 array, dimension (2*N)
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( lda < max( 1, n ) ) { 
    info = -4;
  }
  else if( anorm < ZERO ) { 
    info = -6;
  }
  if( info != 0 ) { 
    xerbla( "ZSYCON", -info );
    return;
  }
  
  //     Quick return if possible
  
  rcond = ZERO;
  if( n == 0 ) { 
    rcond = ONE;
    return;
  }
  else if( anorm <= ZERO ) { 
    return;
  }
  
  //     Check that the diagonal matrix D is nonsingular.
  
  if( upper ) { 
    
    //        Upper triangular storage: examine D from bottom to top
    
    for( i = n, i_ = i - 1; i >= 1; i--, i_-- ) { 
      if( ipiv[i_] > 0 && ctocf(A(i_,i_)) == DComplex(0.,0.) ) 
        return;
    }
  }
  else { 
    
    //        Lower triangular storage: examine D from top to bottom.
    
    for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
      if( ipiv[i_] > 0 && ctocf(A(i_,i_)) == DComplex(0.,0.) ) 
        return;
    }
  }
  
  //     Estimate the 1-norm of the inverse.
  
  kase = 0;
L_30:
  ;
  zlacon( n, &work[n], work, ainvnm, kase );
  if( kase != 0 ) { 
    
    //        Multiply by inv(L*D*L') or inv(U*D*U').
    
    zsytrs( uplo, n, 1, a, lda, ipiv, work, n, info );
    goto L_30;
  }
  
  //     Compute the estimate of the reciprocal condition number.
  
  if( ainvnm != ZERO ) 
    rcond = (ONE/ainvnm)/anorm;
  
  return;
  
  //     End of ZSYCON
  
#undef  A
} // end of function 


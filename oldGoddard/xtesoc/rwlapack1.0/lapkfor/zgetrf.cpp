/*
 * C++ implementation of Lapack routine zgetrf
 *
 * $Id: zgetrf.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:46:57
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zgetrf.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const DComplex ONE = DComplex(1.0e0);
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zgetrf(const long &m, const long &n, DComplex *a, const long &lda, 
 long ipiv[], long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  long _do0, _do1, _do2, i, i_, iinfo, j, j_, jb, nb;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZGETRF computes an LU factorization of a general m-by-n matrix A
  //  using partial pivoting with row interchanges.
  
  //  The factorization has the form
  //     A = P * L * U
  //  where P is a permutation matrix, L is lower triangular with unit
  //  diagonal elements (lower trapezoidal if m > n), and U is upper
  //  triangular (upper trapezoidal if m < n).
  
  //  This is the right-looking Level 3 BLAS version of the algorithm.
  
  //  Arguments
  //  =========
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix A.  M >= 0.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix A.  N >= 0.
  
  //  A       (input/output) COMPLEX*16 array, dimension (LDA,N)
  //          On entry, the m by n matrix to be factored.
  //          On exit, the factors L and U from the factorization
  //          A = P*L*U; the unit diagonal elements of L are not stored.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,M).
  
  //  IPIV    (output) INTEGER array, dimension (min(M,N))
  //          The pivot indices; for 1 <= i <= min(M,N), row i of the
  //          matrix was interchanged with row IPIV(i).
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  //          > 0: if INFO = k, U(k,k) is exactly zero. The factorization
  //               has been completed, but the factor U is exactly
  //               singular, and division by zero will occur if it is used
  //               to solve a system of equations.
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  if( m < 0 ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( lda < max( 1, m ) ) { 
    info = -4;
  }
  if( info != 0 ) { 
    xerbla( "ZGETRF", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( m == 0 || n == 0 ) 
    return;
  
  //     Determine the block size for this environment.
  
  nb = ilaenv( 1, "ZGETRF", " ", m, n, -1, -1 );
  if( nb <= 1 || nb >= min( m, n ) ) { 
    
    //        Use unblocked code.
    
    zgetf2( m, n, a, lda, ipiv, info );
  }
  else { 
    
    //        Use blocked code.
    
    for( j = 1, j_ = j - 1, _do0=docnt(j,min( m, n ),_do1 = nb); _do0 > 0; j += _do1, j_ += _do1, _do0-- ) { 
      jb = min( min( m, n ) - j + 1, nb );
      
      //           Factor diagonal and subdiagonal blocks and test for exact
      //           singularity.
      
      zgetf2( m - j + 1, jb, &A(j_,j_), lda, &ipiv[j_], iinfo );
      
      //           Adjust INFO and the pivot indices.
      
      if( info == 0 && iinfo > 0 ) 
        info = iinfo + j - 1;
      for( i = j, i_ = i - 1, _do2 = min( m, j + jb - 1 ); i <= _do2; i++, i_++ ) { 
        ipiv[i_] = j - 1 + ipiv[i_];
      }
      
      //           Apply interchanges to columns 1:J-1.
      
      zlaswp( j - 1, a, lda, j, j + jb - 1, ipiv, 1 );
      
      if( j + jb <= n ) { 
        
        //              Apply interchanges to columns J+JB:N.
        
        zlaswp( n - j - jb + 1, &A(j_ + jb,0), lda, j, j + 
         jb - 1, ipiv, 1 );
        
        //              Compute block row of U.
        
        ztrsm( 'L'/*Left*/, 'L'/*Lower*/, 'N'/*No transpose*/
         , 'U'/*Unit*/, jb, n - j - jb + 1, ONE, &A(j_,j_), 
         lda, &A(j_ + jb,j_), lda );
        if( j + jb <= m ) { 
          
          //                 Update trailing submatrix.
          
          zgemm( 'N'/*No transpose*/, 'N'/*No transpose*/
           , m - j - jb + 1, n - j - jb + 1, jb, -(ONE), 
           &A(j_,j_ + jb), lda, &A(j_ + jb,j_), lda, ONE, 
           &A(j_ + jb,j_ + jb), lda );
        }
      }
    }
  }
  return;
  
  //     End of ZGETRF
  
#undef  A
} // end of function 


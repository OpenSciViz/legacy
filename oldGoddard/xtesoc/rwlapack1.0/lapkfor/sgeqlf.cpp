/*
 * C++ implementation of Lapack routine sgeqlf
 *
 * $Id: sgeqlf.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:58:43
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: sgeqlf.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

RWLAPKDECL void /*FUNCTION*/ sgeqlf(const long &m, const long &n, float *a, const long &lda, 
   float tau[], float work[], const long &lwork, long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  long _do0, _do1, i, i_, ib, iinfo, iws, k, ki, kk, ldwork, 
   mu, nb, nbmin, nu, nx;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SGEQLF computes a QL factorization of a real m by n matrix A:
  //  A = Q * L.
  
  //  Arguments
  //  =========
  
  //  M       (input) INTEGER
  //          The number of rows of the matrix A.  M >= 0.
  
  //  N       (input) INTEGER
  //          The number of columns of the matrix A.  N >= 0.
  
  //  A       (input/output) REAL array, dimension (LDA,N)
  //          On entry, the m by n matrix A.
  //          On exit, if m >= n, the lower triangle of the subarray
  //          A(m-n+1:m,1:n) contains the n by n lower triangular matrix L;
  //          if m <= n, the elements on and below the (n-m)-th
  //          superdiagonal contain the m by n lower trapezoidal matrix L;
  //          the remaining elements, with the array TAU, represent the
  //          orthogonal matrix Q as a product of elementary reflectors
  //          (see Further Details).
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,M).
  
  //  TAU     (output) REAL array, dimension (min(M,N))
  //          The scalar factors of the elementary reflectors (see Further
  //          Details).
  
  //  WORK    (workspace) REAL array, dimension (LWORK)
  //          On exit, if INFO = 0, WORK(1) returns the minimum value of
  //          LWORK required to use the optimal blocksize.
  
  //  LWORK   (input) INTEGER
  //          The dimension of the array WORK.  LWORK >= max(1,N).
  //          For optimum performance LWORK should be at least N*NB,
  //          where NB is the optimal blocksize.
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument had an illegal value
  
  //  Further Details
  //  ===============
  
  //  The matrix Q is represented as a product of elementary reflectors
  
  //     Q = H(k) . . . H(2) H(1), where k = min(m,n).
  
  //  Each H(i) has the form
  
  //     H(i) = I - tau * v * v'
  
  //  where tau is a real scalar, and v is a real vector with
  //  v(m-k+i+1:m) = 0 and v(m-k+i) = 1; v(1:m-k+i-1) is stored on exit in
  //  A(1:m-k+i-1,n-k+i), and tau in TAU(i).
  
  //  =====================================================================
  
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input arguments
  
  info = 0;
  if( m < 0 ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( lda < max( 1, m ) ) { 
    info = -4;
  }
  else if( lwork < max( 1, n ) ) { 
    info = -7;
  }
  if( info != 0 ) { 
    xerbla( "SGEQLF", -info );
    return;
  }
  
  //     Quick return if possible
  
  k = min( m, n );
  if( k == 0 ) { 
    work[0] = 1;
    return;
  }
  
  //     Determine the block size.
  
  nb = ilaenv( 1, "SGEQLF", " ", m, n, -1, -1 );
  nbmin = 2;
  nx = 1;
  iws = n;
  if( nb > 1 && nb < k ) { 
    
    //        Determine when to cross over from blocked to unblocked code.
    
    nx = max( 0, ilaenv( 3, "SGEQLF", " ", m, n, -1, -1 ) );
    if( nx < k ) { 
      
      //           Determine if workspace is large enough for blocked code.
      
      ldwork = n;
      iws = ldwork*nb;
      if( lwork < iws ) { 
        
        //              Not enough workspace to use optimal NB:  reduce NB and
        //              determine the minimum value of NB.
        
        nb = lwork/ldwork;
        nbmin = max( 2, ilaenv( 2, "SGEQLF", " ", m, n, -1, 
         -1 ) );
      }
    }
  }
  
  if( (nb >= nbmin && nb < k) && nx < k ) { 
    
    //        Use blocked code initially.
    //        The last kk columns are handled by the block method.
    
    ki = ((k - nx - 1)/nb)*nb;
    kk = min( k, ki + nb );
    
    for( i = k - kk + ki + 1, i_ = i - 1, _do0=docnt(i,k - kk + 
     1,_do1 = -nb); _do0 > 0; i += _do1, i_ += _do1, _do0-- ) { 
      ib = min( k - i + 1, nb );
      
      //           Compute the QL factorization of the current block
      //           A(1:m-k+i+ib-1,n-k+i:n-k+i+ib-1)
      
      sgeql2( m - k + i + ib - 1, ib, &A(n - k + i_,0), lda, 
       &tau[i_], work, iinfo );
      if( n - k + i > 1 ) { 
        
        //              Form the triangular factor of the block reflector
        //              H = H(i+ib-1) . . . H(i+1) H(i)
        
        slarft( 'B'/*Backward*/, 'C'/*Columnwise*/, m - 
         k + i + ib - 1, ib, &A(n - k + i_,0), lda, &tau[i_], 
         work, ldwork );
        
        //              Apply H' to A(1:m-k+i+ib-1,1:n-k+i-1) from the left
        
        slarfb( 'L'/*Left*/, 'T'/*Transpose*/, 'B'/*Backward*/
         , 'C'/*Columnwise*/, m - k + i + ib - 1, n - k + 
         i - 1, ib, &A(n - k + i_,0), lda, work, ldwork, a, 
         lda, &work[ib], ldwork );
      }
    }
    mu = m - k + i + nb - 1;
    nu = n - k + i + nb - 1;
  }
  else { 
    mu = m;
    nu = n;
  }
  
  //     Use unblocked code to factor the last or only block
  
  if( mu > 0 && nu > 0 ) 
    sgeql2( mu, nu, a, lda, tau, work, iinfo );
  
  work[0] = iws;
  return;
  
  //     End of SGEQLF
  
#undef  A
} // end of function 


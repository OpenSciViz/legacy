/*
 * C++ implementation of Lapack routine zlauu2
 *
 * $Id: zlauu2.cpp,v 1.1 1993/06/24 22:47:05 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:49:35
 * FOR_C++ Options SET: alloc do=rt no=p pf=zlapack,dlapack,xlapack,zbla,dbla s=dv str=l - prototypes
 *
 * $Log: zlauu2.cpp,v $
 * Revision 1.1  1993/06/24  22:47:05  alv
 * Initial revision
 *
 */

#define RWLAPACK 1  /* Ensure correct blas are read */
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */
#include "rw/dcomplex.h"

// PARAMETER translations
const DComplex ONE = DComplex(1.0e0);
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ zlauu2(const char &uplo, const long &n, DComplex *a, const long &lda, 
 long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  int upper;
  long _do0, _do1, i, i_;
  double aii;

  
  //  -- LAPACK auxiliary routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  ZLAUU2 computes the product U * U' or L' * L, where the triangular
  //  factor U or L is stored in the upper or lower triangular part of
  //  the array A.
  
  //  If UPLO = 'U' or 'u' then the upper triangle of the result is stored,
  //  overwriting the factor U in A.
  //  If UPLO = 'L' or 'l' then the lower triangle of the result is stored,
  //  overwriting the factor L in A.
  
  //  This is the unblocked form of the algorithm, calling Level 2 BLAS.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the triangular factor stored in the array A
  //          is upper or lower triangular:
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The order of the triangular factor U or L.  N >= 0.
  
  //  A       (input/output) COMPLEX*16 array, dimension (LDA,N)
  //          On entry, the triangular factor U or L.
  //          On exit, if UPLO = 'U', the upper triangle of A is
  //          overwritten with the upper triangle of the product U * U';
  //          if UPLO = 'L', the lower triangle of A is overwritten with
  //          the lower triangle of the product L' * L.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( lda < max( 1, n ) ) { 
    info = -4;
  }
  if( info != 0 ) { 
    xerbla( "ZLAUU2", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) 
    return;
  
  if( upper ) { 
    
    //        Compute the product U * U'.
    
    for( i = 1, i_ = i - 1, _do0 = n; i <= _do0; i++, i_++ ) { 
      aii = real(A(i_,i_));
      if( i < n ) { 
        A(i_,i_) = DComplex(aii*aii + real( zdotc( n - i, &A(i_ + 1,i_), 
         lda, &A(i_ + 1,i_), lda ) ));
        zlacgv( n - i, &A(i_ + 1,i_), lda );
        zgemv( 'N'/*No transpose*/, i - 1, n - i, ONE, &A(i_ + 1,0), 
         lda, &A(i_ + 1,i_), lda, DComplex( aii, 0. ), &A(i_,0), 
         1 );
        zlacgv( n - i, &A(i_ + 1,i_), lda );
      }
      else { 
        zdscal( i, aii, &A(i_,0), 1 );
      }
    }
    
  }
  else { 
    
    //        Compute the product L' * L.
    
    for( i = 1, i_ = i - 1, _do1 = n; i <= _do1; i++, i_++ ) { 
      aii = real(A(i_,i_));
      if( i < n ) { 
        A(i_,i_) = DComplex(aii*aii + real( zdotc( n - i, &A(i_,i_ + 1), 
         1, &A(i_,i_ + 1), 1 ) ));
        zlacgv( i - 1, &A(0,i_), lda );
        zgemv( 'C'/*Conjugate transpose*/, n - i, i - 1, 
         ONE, &A(0,i_ + 1), lda, &A(i_,i_ + 1), 1, DComplex( aii, 
         0. ), &A(0,i_), lda );
        zlacgv( i - 1, &A(0,i_), lda );
      }
      else { 
        zdscal( i, aii, &A(0,i_), lda );
      }
    }
  }
  
  return;
  
  //     End of ZLAUU2
  
#undef  A
} // end of function 


/*
 * C++ implementation of lapack routine dpocon
 *
 * $Id: dpocon.cpp,v 1.6 1993/04/06 20:41:58 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the Fortran using Cobalt Blue's FOR_C++,
 * and then massaged slightly to Rogue Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 07:37:17
 * FOR_C++ Options SET: alloc do=rt no=p pf=dlapack,xlapack,dbla s=dv str=l - prototypes
 *
 * $Log: dpocon.cpp,v $
 * Revision 1.6  1993/04/06  20:41:58  alv
 * added const to parameters; added include lapkdefs
 *
 * Revision 1.5  1993/03/19  18:41:23  alv
 * now passes chars explicitly, rather than indirection of a string, to shut up SUN warnings
 *
 * Revision 1.4  1993/03/19  17:18:24  alv
 * added RWLAPKDECL linkage specifier
 *
 * Revision 1.3  1993/03/09  16:14:40  alv
 * made parms const
 *
 * Revision 1.2  1993/03/05  23:16:38  alv
 * changed ref parms to const ref
 *
 * Revision 1.1  1993/03/03  16:08:25  alv
 * Initial revision
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const double ONE = 1.0e0;
const double ZERO = 0.0e0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ dpocon(const char &uplo, long &n, double *a, long &lda, 
 double &anorm, double &rcond, double work[], long iwork[], long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  int upper;
  char normin;
  long ix, kase;
  double ainvnm, scale, scalel, scaleu, smlnum;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  DPOCON estimates the reciprocal of the condition number of a real
  //  symmetric positive definite matrix using the Cholesky factorization
  //  A = U'*U or A = L*L' computed by DPOTRF.
  
  //  An estimate is obtained for norm(inv(A)), and the reciprocal of the
  //  condition number is computed as RCOND = 1 / (ANORM * norm(inv(A))).
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the factor stored in A is upper or lower
  //          triangular.
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  A       (input) DOUBLE PRECISION array, dimension (LDA,N)
  //          The triangular factor U or L from the Cholesky factorization
  //          A = U'*U or A = L*L', as computed by DPOTRF.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  ANORM   (input) DOUBLE PRECISION
  //          The 1-norm (or infinity-norm) of the symmetric matrix A.
  
  //  RCOND   (output) DOUBLE PRECISION
  //          The reciprocal of the condition number of the matrix A,
  //          computed as RCOND = 1/(ANORM * AINVNM), where AINVNM is an
  //          estimate of the 1-norm of inv(A) computed in this routine.
  
  //  WORK    (workspace) DOUBLE PRECISION array, dimension (3*N)
  
  //  IWORK   (workspace) INTEGER array, dimension (N)
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0: if INFO = -k, the k-th argument had an illegal value
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters.
  
  info = 0;
  upper = lsame( uplo, 'U' );
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( lda < max( 1, n ) ) { 
    info = -4;
  }
  else if( anorm < ZERO ) { 
    info = -5;
  }
  if( info != 0 ) { 
    xerbla( "DPOCON", -info );
    return;
  }
  
  //     Quick return if possible
  
  rcond = ZERO;
  if( n == 0 ) { 
    rcond = ONE;
    return;
  }
  else if( anorm == ZERO ) { 
    return;
  }
  
  smlnum = dlamch( 'S'/* Safe minimum */ );
  
  //     Estimate the 1-norm of inv(A).
  
  kase = 0;
  normin = 'N';
L_10:
  ;
  dlacon( n, &work[n], work, iwork, ainvnm, kase );
  if( kase != 0 ) { 
    if( upper ) { 
      
      //           Multiply by inv(U').
      
      dlatrs( 'U'/* Upper */, 'T'/* Transpose */, 'N'/* Non-unit */
       , normin, n, a, lda, work, scalel, &work[n*2], info );
      normin = 'Y';
      
      //           Multiply by inv(U).
      
      dlatrs( 'U'/* Upper */, 'N'/* No transpose */, 'N'/* Non-unit */
       , normin, n, a, lda, work, scaleu, &work[n*2], info );
    }
    else { 
      
      //           Multiply by inv(L).
      
      dlatrs( 'L'/* Lower */, 'N'/* No transpose */, 'N'/* Non-unit */
       , normin, n, a, lda, work, scalel, &work[n*2], info );
      normin = 'Y';
      
      //           Multiply by inv(L').
      
      dlatrs( 'L'/* Lower */, 'T'/* Transpose */, 'N'/* Non-unit */
       , normin, n, a, lda, work, scaleu, &work[n*2], info );
    }
    
    //        Multiply by 1/SCALE if doing so will not cause overflow.
    
    scale = scalel*scaleu;
    if( scale != ONE ) { 
      ix = idamax( n, work, 1 );
      if( scale < abs( work[ix - 1] )*smlnum || scale == ZERO ) 
        goto L_20;
      drscl( n, scale, work, 1 );
    }
    goto L_10;
  }
  
  //     Compute the estimate of the reciprocal condition number.
  
  if( ainvnm != ZERO ) 
    rcond = (ONE/ainvnm)/anorm;
  
L_20:
  ;
  return;
  
  //     End of DPOCON
  
#undef  A
} // end of function 


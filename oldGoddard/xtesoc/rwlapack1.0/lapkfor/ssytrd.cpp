/*
 * C++ implementation of Lapack routine ssytrd
 *
 * $Id: ssytrd.cpp,v 1.2 1993/10/13 21:47:09 alv Exp $
 *
 *************************************************************
 *
 * Rogue Wave Software, Inc.
 * P.O. Box 2328
 * Corvallis, OR 97339
 *
 * Copyright (C) 1993.
 * This software is subject to copyright protection under the
 * laws of the United States and other countries.
 *
 *************************************************************
 *
 * Translated from the default Fortran bla using Cobalt
 * Blue's FOR_C++, and then massaged slightly to Rogue
 * Wave format.
 *
 * Translated by FOR_C++, v1.1 (P), on 02/18/93 at 08:03:10
 * FOR_C++ Options SET: alloc do=rt no=p pf=slapack,xlapack,sbla s=dv str=l - prototypes
 *
 * $Log: ssytrd.cpp,v $
 * Revision 1.2  1993/10/13  21:47:09  alv
 * fixed up constness of parameters
 *
 */

#include "rw/lapkdefs.h"
#include "rw/bla.h"
#include "rw/lapack.h"
#include "rw/fortran.h" /* Fortran run time library */

// PARAMETER translations
const float ONE = 1.0;
// end of PARAMETER translations

RWLAPKDECL void /*FUNCTION*/ ssytrd(const char &uplo, const long &n, float *a, const long &lda, 
 float d[], float e[], float tau[], float work[], const long &lwork, 
 long &info)
{
#define A(I_,J_)  (*(a+(I_)*(lda)+(J_)))
  char _c0[2];
  int upper;
  long _do0, _do1, _do2, _do3, _do4, _do5, i, i_, iinfo, iws, 
   j, j_, kk, ldwork, nb, nbmin, nx;

  
  //  -- LAPACK routine (version 1.0) --
  //     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
  //     Courant Institute, Argonne National Lab, and Rice University
  //     February 29, 1992
  
  //     .. Scalar Arguments ..
  //     ..
  //     .. Array Arguments ..
  //     ..
  
  //  Purpose
  //  =======
  
  //  SSYTRD reduces a real symmetric matrix A to symmetric tridiagonal
  //  form T by an orthogonal similarity transformation: Q' * A * Q = T.
  
  //  Arguments
  //  =========
  
  //  UPLO    (input) CHARACTER*1
  //          Specifies whether the upper or lower triangular part of the
  //          symmetric matrix A is stored:
  //          = 'U':  Upper triangular
  //          = 'L':  Lower triangular
  
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  
  //  A       (input/output) REAL array, dimension (LDA,N)
  //          On entry, the symmetric matrix A.  If UPLO = 'U', the leading
  //          n-by-n upper triangular part of A contains the upper
  //          triangular part of the matrix A, and the strictly lower
  //          triangular part of A is not referenced.  If UPLO = 'L', the
  //          leading n-by-n lower triangular part of A contains the lower
  //          triangular part of the matrix A, and the strictly upper
  //          triangular part of A is not referenced.
  //          On exit, if UPLO = 'U', the diagonal and first superdiagonal
  //          of A are overwritten by the corresponding elements of the
  //          tridiagonal matrix T, and the elements above the first
  //          superdiagonal, with the array TAU, represent the orthogonal
  //          matrix Q as a product of elementary reflectors; if UPLO
  //          = 'L', the diagonal and first subdiagonal of A are over-
  //          written by the corresponding elements of the tridiagonal
  //          matrix T, and the elements below the first subdiagonal, with
  //          the array TAU, represent the orthogonal matrix Q as a product
  //          of elementary reflectors. See Further Details.
  
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  
  //  D       (output) REAL array, dimension (N)
  //          The diagonal elements of the tridiagonal matrix T:
  //          D(i) = A(i,i).
  
  //  E       (output) REAL array, dimension (N-1)
  //          The off-diagonal elements of the tridiagonal matrix T:
  //          E(i) = A(i,i+1) if UPLO = 'U', E(i) = A(i+1,i) if UPLO = 'L'.
  
  //  TAU     (output) REAL array, dimension (N)
  //          The scalar factors of the elementary reflectors (see Further
  //          Details).
  
  //  WORK    (workspace) REAL array, dimension (LWORK)
  //          On exit, if INFO = 0, WORK(1) returns the minimum value of
  //          LWORK required to use the optimal blocksize.
  
  //  LWORK   (input) INTEGER
  //          The dimension of the array WORK.  LWORK >= 1.
  //          For optimum performance LWORK should be at least N*NB,
  //          where NB is the optimal blocksize.
  
  //  INFO    (output) INTEGER
  //          = 0:  successful exit.
  //          < 0:  if INFO = -i, the i-th argument had an illegal value.
  
  //  Further Details
  //  ===============
  
  //  If UPLO = 'U', the matrix Q is represented as a product of elementary
  //  reflectors
  
  //     Q = H(n-1) . . . H(2) H(1).
  
  //  Each H(i) has the form
  
  //     H(i) = I - tau * v * v'
  
  //  where tau is a real scalar, and v is a real vector with
  //  v(i+1:n) = 0 and v(i) = 1; v(1:i-1) is stored on exit in
  //  A(1:i-1,i+1), and tau in TAU(i).
  
  //  If UPLO = 'L', the matrix Q is represented as a product of elementary
  //  reflectors
  
  //     Q = H(1) H(2) . . . H(n-1).
  
  //  Each H(i) has the form
  
  //     H(i) = I - tau * v * v'
  
  //  where tau is a real scalar, and v is a real vector with
  //  v(1:i) = 0 and v(i+1) = 1; v(i+2:n) is stored on exit in A(i+2:n,i),
  //  and tau in TAU(i).
  
  //  The contents of A on exit are illustrated by the following examples
  //  with n = 5:
  
  //  if UPLO = 'U':                       if UPLO = 'L':
  
  //    (  d   e   v2  v3  v4 )              (  d                  )
  //    (      d   e   v3  v4 )              (  e   d              )
  //    (          d   e   v4 )              (  v1  e   d          )
  //    (              d   e  )              (  v1  v2  e   d      )
  //    (                  d  )              (  v1  v2  v3  e   d  )
  
  //  where d and e denote diagonal and off-diagonal elements of T, and vi
  //  denotes an element of the vector defining H(i).
  
  //  =====================================================================
  
  //     .. Parameters ..
  //     ..
  //     .. Local Scalars ..
  //     ..
  //     .. External Subroutines ..
  //     ..
  //     .. Intrinsic Functions ..
  //     ..
  //     .. External Functions ..
  //     ..
  //     .. Executable Statements ..
  
  //     Test the input parameters
  
  info = 0;
  upper = lsame( uplo, 'U' );
  if( !upper && !lsame( uplo, 'L' ) ) { 
    info = -1;
  }
  else if( n < 0 ) { 
    info = -2;
  }
  else if( lda < max( 1, n ) ) { 
    info = -4;
  }
  else if( lwork < 1 ) { 
    info = -9;
  }
  if( info != 0 ) { 
    xerbla( "SSYTRD", -info );
    return;
  }
  
  //     Quick return if possible
  
  if( n == 0 ) { 
    work[0] = 1;
    return;
  }
  
  //     Determine the block size.
  
  nb = ilaenv( 1, "SSYTRD", STR1(_c0,uplo), n, -1, -1, -1 );
  nx = n;
  iws = 1;
  if( nb > 1 && nb < n ) { 
    
    //        Determine when to cross over from blocked to unblocked code
    //        (last block is always handled by unblocked code).
    
    nx = max( nb, ilaenv( 3, "SSYTRD", STR1(_c0,uplo), n, -1, 
     -1, -1 ) );
    if( nx < n ) { 
      
      //           Determine if workspace is large enough for blocked code.
      
      ldwork = n;
      iws = ldwork*nb;
      if( lwork < iws ) { 
        
        //              Not enough workspace to use optimal NB:  determine the
        //              minimum value of NB, and reduce NB or force use of
        //              unblocked code by setting NX = N.
        
        nb = lwork/ldwork;
        nbmin = ilaenv( 2, "SSYTRD", STR1(_c0,uplo), n, -1, 
         -1, -1 );
        if( nb < nbmin ) 
          nx = n;
      }
    }
    else { 
      nx = n;
    }
  }
  else { 
    nb = 1;
  }
  
  if( upper ) { 
    
    //        Reduce the upper triangle of A.
    //        Columns 1:kk are handled by the unblocked method.
    
    kk = n - ((n - nx + nb - 1)/nb)*nb;
    for( i = n - nb + 1, i_ = i - 1, _do0=docnt(i,kk + 1,_do1 = -nb); _do0 > 0; i += _do1, i_ += _do1, _do0-- ) { 
      
      //           Reduce columns i:i+nb-1 to tridiagonal form and form the
      //           matrix W which is needed to update the unreduced part of
      //           the matrix
      
      slatrd( uplo, i + nb - 1, nb, a, lda, e, tau, work, ldwork );
      
      //           Update the unreduced submatrix A(1:i-1,1:i-1), using an
      //           update of the form:  A := A - V*W' - W*V'
      
      ssyr2k( uplo, 'N'/*No transpose*/, i - 1, nb, -ONE, &A(i_,0), 
       lda, work, ldwork, ONE, a, lda );
      
      //           Copy superdiagonal elements back into A, and diagonal
      //           elements into D
      
      for( j = i, j_ = j - 1, _do2 = i + nb - 1; j <= _do2; j++, j_++ ) { 
        A(j_,j_ - 1) = e[j_ - 1];
        d[j_] = A(j_,j_);
      }
    }
    
    //        Use unblocked code to reduce the last or only block
    
    ssytd2( uplo, kk, a, lda, d, e, tau, iinfo );
  }
  else { 
    
    //        Reduce the lower triangle of A
    
    for( i = 1, i_ = i - 1, _do3=docnt(i,n - nx,_do4 = nb); _do3 > 0; i += _do4, i_ += _do4, _do3-- ) { 
      
      //           Reduce columns i:i+nb-1 to tridiagonal form and form the
      //           matrix W which is needed to update the unreduced part of
      //           the matrix
      
      slatrd( uplo, n - i + 1, nb, &A(i_,i_), lda, &e[i_], &tau[i_], 
       work, ldwork );
      
      //           Update the unreduced submatrix A(i+ib:n,i+ib:n), using
      //           an update of the form:  A := A - V*W' - W*V'
      
      ssyr2k( uplo, 'N'/*No transpose*/, n - i - nb + 1, nb, 
       -ONE, &A(i_,i_ + nb), lda, &work[nb], ldwork, ONE, &A(i_ + nb,i_ + nb), 
       lda );
      
      //           Copy subdiagonal elements back into A, and diagonal
      //           elements into D
      
      for( j = i, j_ = j - 1, _do5 = i + nb - 1; j <= _do5; j++, j_++ ) { 
        A(j_,j_ + 1) = e[j_];
        d[j_] = A(j_,j_);
      }
    }
    
    //        Use unblocked code to reduce the last or only block
    
    ssytd2( uplo, n - i + 1, &A(i - 1,i - 1), lda, &d[i - 1], 
     &e[i - 1], &tau[i - 1], iinfo );
  }
  
  work[0] = iws;
  return;
  
  //     End of SSYTRD
  
#undef  A
} // end of function 


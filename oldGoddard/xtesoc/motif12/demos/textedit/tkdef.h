/* 
 * (c) Copyright 1989, 1990, 1991, 1992 OPEN SOFTWARE FOUNDATION, INC. 
 * ALL RIGHTS RESERVED 
*/ 
/* 
 * Motif Release 1.2
*/ 
/*   $RCSfile: tkdef.h,v $ $Revision: 1.3 $ $Date: 92/03/13 15:43:57 $ */


/************************************************************
 *     tkdef.h -- typedefs for toolkit-specific dialogue layer
 ************************************************************/

enum warn_reasons { warn_open, warn_write, warn_save, warn_remove };

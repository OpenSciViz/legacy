/* 
 * (c) Copyright 1989, 1990, 1991, 1992 OPEN SOFTWARE FOUNDATION, INC. 
 * ALL RIGHTS RESERVED 
*/ 
/* 
 * Motif Release 1.2
*/ 
/*   $RCSfile: file.h,v $ $Revision: 1.3 $ $Date: 92/03/13 15:43:30 $ */

/************************************************************
 *     file.h -- dealing with files and filenames
 ************************************************************/

extern FileRemove();
extern void FileSaveText();
extern char *FileGetText();
extern char *FileTrailingPart();

/* 
 * (c) Copyright 1989, 1990, 1991, 1992 OPEN SOFTWARE FOUNDATION, INC. 
 * ALL RIGHTS RESERVED 
*/ 
/* 
 * Motif Release 1.2
*/ 
/*   $RCSfile: app.h,v $ $Revision: 1.3 $ $Date: 92/03/13 15:43:10 $ */

/************************************************************
 *     app.h -- toolkit-independent code
 ************************************************************/

extern char *AppBufferName();

extern char *AppReadFile( );
extern void AppSaveFile( );
extern void AppTransferFile( );

extern void AppNewFile( );
extern AppRemoveFile();

extern AppOpenReadFile( );
extern AppOpenSaveFile( );
extern AppOpenTransferFile( );

extern void AppCompleteSaveAsFile( );
extern void AppCompleteCopyFile( );
extern AppCompleteMoveFile();



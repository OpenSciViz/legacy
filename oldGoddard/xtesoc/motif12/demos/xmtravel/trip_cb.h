/*
 * (c) Copyright 1989, 1990, 1991, 1992 OPEN SOFTWARE FOUNDATION, INC. 
 * ALL RIGHTS RESERVED 
 */
/*
 * Motif Release 1.2
 */


extern void	t_create_widgets();
extern void	number_changed();
extern void	date_changed();
extern void	airlines_selected();
extern void	origin_selected();
extern void	destination_selected();
extern void	find_flights_activate();
extern void	cancel_activate();

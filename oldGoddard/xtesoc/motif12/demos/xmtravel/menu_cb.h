/*
 * (c) Copyright 1989, 1990, 1991, 1992 OPEN SOFTWARE FOUNDATION, INC. 
 * ALL RIGHTS RESERVED 
 */
/*
 * Motif Release 1.2
 */

extern void 	file_exit_activate();
extern void 	client_delete_activate();
extern void 	help_on_context_activate();
extern void 	help_on_window_activate();
extern void 	help_on_version_activate();
extern void     help_on_help_activate();
extern void     help_on_keys_activate();
extern void     help_on_index_activate();
extern void     help_tutorial_activate();
extern void	view_brief_changed();
extern void	view_detail_changed();
extern void	view_home_changed();
extern void	view_business_changed();
extern void     file_new_activate();
extern void     file_open_activate();
extern void     file_save_activate();
extern void     file_save_as_activate();
extern void     file_print_activate();
extern void     file_close_activate();
extern void     option_print_activate();
extern void     client_check_iten_activate();
extern void     client_bill_activate();




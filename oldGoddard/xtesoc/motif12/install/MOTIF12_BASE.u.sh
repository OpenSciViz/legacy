:
###############################################################################
#                                                                             #
#                Copyright (c) 1990-1991 Bluestone Consulting, Inc.	      #
#                                                                             #
#      All rights reserved.  This notice is  intended  as  a  precaution      #
#      against  inadvertent publication, and shall not be deemed to con-      #
#      stitute an acknowledgment that publication has  occurred  nor  to      #
#      imply  any  waiver  of confidentiality.  The year included in the      #
#      notice is the year of the creation of the work.                        #
#                                                                             #
# Script to uninstall OSF/Motif 1.2 environment			      #
#                                                                             #
###############################################################################

HOSTNAME=`/usr/ucb/hostname`

PRODUCT="OSF/Motif 1.2 Development Environment"
echo "--------------------------------------------------"
echo ""
echo "Found \"$PRODUCT\""
echo  "Uninstall now? [y or n]   "
        read answer
        case "$answer" in

                y|Y)
		sleep 0
                ;;

                *)
                echo ""
                echo "Not uninstalling \"$PRODUCT\""
                exit 0
                ;;
        esac

echo ""
echo "Uninstalling \"$PRODUCT\""
echo ""
TOPDIR=$1
cd $TOPDIR
echo ""

# Read the configuration file to find out how this stuff was installed
if [ -f install/configuration.$HOSTNAME ]
then
	. install/configuration.$HOSTNAME
else
	echo "Hmm...looks like Motif was never installed or the file"
	echo "<MOTIFDIR>/install/configuration was modified or removed"
	echo "If you suspect this to be the case, try to rerun Install"
	echo "to properly initialize the configuration file."
	exit 0
fi

# If they did choose links in the Install, then remove them
if [ "$wantlinks" = "yes" ] || [ "$wantlinks" = "y" ]
then
XMLIBS="libXm.a
libMrm.a
libUil.a
libXm.so.1.2
libXm.so
libMrm.so.1.2
libMrm.so
libUil.so.1.2
libUil.so
Xm"

XMINCS="Xm
Mrm
uil"

XMBINS="mwm
uil
xmbind"

XMBITS="xm_error
  xm_hour16
  xm_hour16m
  xm_hour32
  xm_hour32m
  xm_information
  xm_noenter16
  xm_noenter16m
  xm_noenter32
  xm_noenter32m
  xm_question
  xm_warning
  xm_working"

XMSYSS="system.mwmrc
XKeysymDB"

XMAPPS="Mwm"


#
# Search and destroy our links
echo "********************************************************"
echo "Checking around for MOTIF header files"
  for i in $XMINCS
  do
      if [ -h $X11INCDIR/$i ]
      then
                echo "Removing link $X11INCDIR/$i";rm -f $X11INCDIR/$i
      fi
  done
  echo

echo "Checking around for MOTIF libraries"
for i in $XMLIBS
do
    if [ -h $X11LIBDIR/$i ]
    then
  		echo "Removing link $X11LIBDIR/$i";rm -f $X11LIBDIR/$i
    fi
done
echo
echo "Checking around for MOTIF binaries"
for i in $XMBINS
do
    if [ -h $X11BINDIR/$i ]
    then
  		echo "Removing link $X11BINDIR/$i";rm -f $X11BINDIR/$i
    fi
done
echo
  echo "Checking around for MOTIF bitmaps"
  for i in $XMBITS
  do
      if [ -h $X11BITDIR/$i ]
      then
                echo "Removing link $X11BITDIR/$i";rm -f $X11BITDIR/$i
      fi
  done
  echo

echo "Checking around for MOTIF system files"
for i in $XMSYSS
do
    if [ -h $X11SYSDIR/$i ]
    then
  		echo "Removing link $X11SYSDIR/$i";rm -f $X11SYSDIR/$i
    fi
done
echo
echo "Checking around for MOTIF resource files"
for i in $XMAPPS
do
    if [ -h $X11APPDIR/$i ]
    then
  		echo "Removing link $X11APPDIR/$i";rm -f $X11APPDIR/$i
    fi
done

else #wantlinks was not set to yes
echo "********************************************************"
echo "No links were made in the original Installation."
echo "Setting state of Motif software to UNINSTALLED"
fi #wantlinks

rm -f install/i_MOTIF12_BASE.$HOSTNAME
rm -f install/configuration.$HOSTNAME
echo "********************************************************"
echo "OSF/MOTIF 1.2 is now uninstalled..."

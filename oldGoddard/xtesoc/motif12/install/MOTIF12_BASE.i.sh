:
###############################################################################
#                                                                             #
#                Copyright (c) 1991 Bluestone Consulting, Inc.	      #
#                                                                             #
#      All rights reserved.  This notice is  intended  as  a  precaution      #
#      against  inadvertent publication, and shall not be deemed to con-      #
#      stitute an acknowledgment that publication has  occurred  nor  to      #
#      imply  any  waiver  of confidentiality.  The year included in the      #
#      notice is the year of the creation of the work.                        #
#                                                                             #
# Script to install OSF/Motif 1.2 (without sources) environment			      #
#                                                                             #
###############################################################################

HOSTNAME=`/usr/ucb/hostname`


PRODUCT="OSF/Motif 1.2 Development Environment"
echo "--------------------------------------------------"
echo ""
echo "Found \"$PRODUCT\""
echo ""
echo  "Install now? [y or n]   "
        read answer
        case "$answer" in

                y|Y)
                ;;

		*)
                echo ""
                echo "Not installing \"$PRODUCT\""
		exit 0
                ;;
        esac

echo ""
echo "Installing \"$PRODUCT\""
echo ""
TOPDIR=$1
cd $TOPDIR
MOTIFROOT=`pwd`
wantlinks="no"
X11INCDIR=/usr/include
X11BITDIR=/usr/include/X11/bitmaps
X11LIBDIR=/usr/lib
X11BINDIR=/usr/bin/X11
X11SYSDIR=/usr/lib/X11
X11APPDIR=/usr/lib/X11/app-defaults
	
X11DIRS="/usr/bin/X11
/usr/lib/X11
/usr/include/X11"
	
	
checknull()
{
read answer
if [ "$answer" = "" ] || [ "$answer" = " " ]
then
	echo  "Sorry!  Cannot enter a null path.  Enter again:  " 
	checknull
fi
}

main_loop ()
# Query if user is using a different configuration for X11 and Xm
{
echo
echo "*********************************************************************"
echo "The installation script assumes the following X11 R5 configuration:"
echo
echo "  The X11 R5 include directory (X11) in:        /usr/include"
echo "  The X11 R5 libraries in:			/usr/lib"
echo "  The X11 R5 binaries in:			/usr/bin/X11"
echo "  The X11 R5 bitmaps directory in:              /usr/include/X11/bitmaps"
echo "  The X11 R5 system files in:			/usr/lib/X11"
echo "  The app-defaults or XAPPLRESDIR directory:	/usr/lib/X11/app-defaults"
echo

echo  "Are you using a different configuration? [no]: $NL"
read newconfig
echo "*********************************************************************"

# Query the user for the location of X11R5 include, libs, and binaries
if [ "$newconfig" = "yes" ] || [ "$newconfig" = "y" ]
then
    echo  "Enter the path containing the X11 R5 include directory: $NL"
	checknull
	X11INCDIR=$answer
    echo  "Enter the path containing the X11 R5 libraries        : $NL"
	checknull
    	X11LIBDIR=$answer
    echo  "Enter the path containing the X11 R5 binaries         : $NL"
	checknull
    	X11BINDIR=$answer
    echo  "Enter the path containing the X11 R5 bitmap directory: $NL"
	checknull
	X11BITDIR=$answer
    echo  "Enter the path containing the X11 R5 system files     : $NL"
	checknull
    	X11SYSDIR=$answer
    echo  "Enter the path containing the X11 R5 app-defaults files   : $NL"
	checknull
    	X11APPDIR=$answer

    echo
fi

# Ask if the user wants links made into his system or X area

echo "*********************************************************************"
echo "The installation script would like to perform links in these areas."
echo "If you are unsure, please refer to the release notes for more details"
echo "on these links:"
echo
echo "          $X11INCDIR"
echo "		$X11LIBDIR"
echo "		$X11BINDIR"
echo "          $X11BITDIR"
echo "		$X11SYSDIR"
echo "		$X11APPDIR"
echo
echo  "Do you want to have these links set? [yes]:  "
	read wantlinks
echo "*********************************************************************"




#Start the installation with a request for info
# Make the softlinks if they are needed
if [ "$wantlinks" = "yes" ] || [ "$wantlinks" = "y" ] || [ "$wantlinks" = "" ]
then

	wantlinks="yes"
	# See if the X11 R5 directories given actually exist
	# These can be either real directories or symbolic links
	echo "Checking integrity of the X Window System, Release 5..."
	echo ""
	LIST="$X11INCDIR/X11 $X11LIBDIR $X11BINDIR $X11BITDIR $X11SYSDIR $X11APPDIR"
	for i in $LIST
	do
	    if [ ! -d $i ]
	    then
		echo "Error: Directory $i does not exist!"
		echo  "Create $i ?  [yes]:   "
		read create
		if [ "$create" = "yes" ] || [ "$create" = "y" ] || [ "$create" = "" ]
		then
			mkdir $i
		else
			echo
			echo "Not making $i.  Please resubmit X11 R5 dir locations"
			main_loop
		fi
	    else
		echo "Found $i..."
	    fi
	done
	
	echo "X11 R5 looks to be properly installed, continuing installation..."
	
	#
	# Validate that there is no MOTIF Stuff out there.

	XM_BASE="$X11LIBDIR/libXm.a
	$X11LIBDIR/libMrm.a
	$X11LIBDIR/libUil.a
	$X11LIBDIR/libXm.so.1.2
	$X11LIBDIR/libMrm.so.1.2
	$X11LIBDIR/libUil.so.1.2
	$X11LIBDIR/libXm.so
	$X11LIBDIR/libMrm.so
	$X11LIBDIR/libUil.so
	$X11LIBDIR/Xm
	$X11BINDIR/mwm
	$X11BINDIR/uil
	$X11BINDIR/xmbind
	$X11SYSDIR/system.mwmrc
	$X11APPDIR/Mwm"

	OSF_HEADERS="$X11INCDIR/Mrm
	$X11INCDIR/Xm
	$X11INCDIR/uil"

	XM_BITMAPS="$X11BITDIR/xm_error
	$X11BITDIR/xm_hour16
        $X11BITDIR/xm_hour16m
        $X11BITDIR/xm_hour32
        $X11BITDIR/xm_hour32m
        $X11BITDIR/xm_information
        $X11BITDIR/xm_noenter16
        $X11BITDIR/xm_noenter16m
        $X11BITDIR/xm_noenter32
        $X11BITDIR/xm_noenter32m
        $X11BITDIR/xm_question
        $X11BITDIR/xm_warning
        $X11BITDIR/xm_working"


	XM_FILES="$XM_BASE $OSF_HEADERS $XM_BITMAPS"
	echo " "
	echo "*********************************************************************"
	echo "Checking for other versions of Motif in target area..."
	echo " "
	echo "If one is found, it must be removed or you must reconfigure"
	echo "the links you wish to set."
	echo " "
	for i in $XM_FILES
	do
		if [ -h $i ] || [ -f $i ]
	    	then
		echo "File exists: Found $i !"
		echo  "Should I remove it now? [n]:  "
		read foobar
		case "$foobar" in
			y|Y) 
				echo "Removing $i "
				rm -f $i
			;;
			*) 
				echo
				echo "*********************************************************************"
				echo "This package cannot share areas with other Motif"
				echo "versions.  Please reconfigure the directories"
				echo "you would like links from:"
				main_loop
	      		;;
			esac
	    	fi
	done
	echo ""
	echo "Base requirements satisfied, installation continuing..."
	
	#
	# Now install by creating symbolic links from the OSF/Motif 1.2
	# source directory tree to the standard targets, thusly:
	# (First install the main directories)

	XMLIBSTUB=usr/lib
	XMLIBS="libXm.a
	libMrm.a
	libUil.a
	libXm.so.1.2
	libXm.so
	libMrm.so.1.2
	libMrm.so
	libUil.so.1.2
	libUil.so
	Xm"
	
	XMINCSTUB=usr/include
	XMINCS="Xm
	Mrm
	uil"

	XMBITSTUB=usr/include/X11/bitmaps
        XMBITS="xm_error
          xm_hour16
          xm_hour16m
          xm_hour32
          xm_hour32m
          xm_information
          xm_noenter16
          xm_noenter16m
          xm_noenter32
          xm_noenter32m
          xm_question
          xm_warning
          xm_working"

	XMBINSTUB=usr/bin/X11
	XMBINS="mwm
	uil
	xmbind"

	XMSYSSTUB=usr/lib/X11
	XMSYSS="system.mwmrc
	XKeysymDB"

	XMAPPSTUB=usr/lib/X11/app-defaults
	XMAPPS="Mwm"

	echo
	echo "Now actually installing OSF/Motif 1.2..."
	echo " "
	echo "Making links...please wait..."

       for i in $XMINCS
        do
          if [ -d $MOTIFROOT/$XMINCSTUB/$i ]
          then
            ln -s $MOTIFROOT/$XMINCSTUB/$i $X11INCDIR/$i
            if [ $? -eq 1 ]
            then
                echo "Link for $i failed...not linking..."
            fi  
          fi
        done

	for i in $XMLIBS
	do
	  if [ -f $MOTIFROOT/$XMLIBSTUB/$i ]
	  then
	    ln -s $MOTIFROOT/$XMLIBSTUB/$i $X11LIBDIR/$i
	    if [ $? -eq 1 ]
	    then
	        echo "Link for $i failed...not linking..."
	    fi  
	  fi
	done
	for i in $XMBINS
	do
	  if [ -f $MOTIFROOT/$XMBINSTUB/$i ]
	  then
	    ln -s $MOTIFROOT/$XMBINSTUB/$i $X11BINDIR/$i
	    if [ $? -eq 1 ]
	    then
	        echo "Link for $i failed...not linking..."
	    fi  
	  fi
	done
        for i in $XMBITS
        do
          if [ -f $MOTIFROOT/$XMBITSTUB/$i ]
          then
            ln -s $MOTIFROOT/$XMBITSTUB/$i $X11BITDIR/$i
            if [ $? -eq 1 ]
            then
                echo "Link for $i failed...not linking..."
            fi  
          fi
        done
	for i in $XMSYSS
	do
	  if [ -f $MOTIFROOT/$XMSYSSTUB/$i ]
	  then
	    ln -s $MOTIFROOT/$XMSYSSTUB/$i $X11SYSDIR/$i
	    if [ $? -eq 1 ]
	    then
	        echo "Link for $i failed...not linking..."
	    fi  
	  fi
	done
	for i in $XMAPPS
	do
	  if [ -f $MOTIFROOT/$XMAPPSTUB/$i ]
	  then
	    ln -s $MOTIFROOT/$XMAPPSTUB/$i $X11APPDIR/$i
	    if [ $? -eq 1 ]
	    then
	        echo "Link for $i failed...not linking..."
	    fi  
	  fi
	done


# Assume that install went well, so we can save some of this
# config info...



else #wantlinks=no
	wantlinks="no"

	echo "IMPORTANT!!"
	echo "==========="
	echo "Since you don't want links to be made, you may need to"
	echo "refer to the OSF/Motif release notes shipped with this"
	echo "package.  To successfully run and develop with Motif,"
	echo "you may need to modify your PATH variable, as well as"
	echo "tailor any Makefiles to reflect the current location"
	echo "of the Motif libraries and header files"
	echo
	echo  "Hit <RETURN> when ready:  "
	read spud
	echo "*********************************************************************"
	echo "Continuing installation..."
fi	#wantlinks

} #main_loop

# Run the install
main_loop

# Do this stuff whether links were made or not

touch install/i_MOTIF12_BASE.$HOSTNAME
chmod 744 install/i_MOTIF12_BASE.$HOSTNAME
echo "#Full product name:		$PRODUCT"    > install/configuration.$HOSTNAME
echo "#Were links made? [default no]	$wantlinks" >> install/configuration.$HOSTNAME
echo "#Motif root directory:		$MOTIFROOT" >> install/configuration.$HOSTNAME
echo "#X11 Include Directory:         $X11INCDIR" >> install/configuration.$HOSTNAME
echo "#X11 Library Directory:		$X11LIBDIR" >> install/configuration.$HOSTNAME
echo "#X11 Binary Directory: 		$X11BINDIR" >> install/configuration.$HOSTNAME
echo "#X11 Bitmap Directory:          $X11BITDIR" >> install/configuration.$HOSTNAME
echo "#X11 System Directory: 		$X11SYSDIR" >> install/configuration.$HOSTNAME
echo "#X11 Application Directory:	$X11APPDIR" >> install/configuration.$HOSTNAME
echo "" >> install/configuration.$HOSTNAME
echo "wantlinks=$wantlinks" >> install/configuration.$HOSTNAME
echo "# If wantlinks is no, disregard the following info" >> install/configuration.$HOSTNAME
echo "" >> install/configuration.$HOSTNAME
echo "X11INCDIR=$X11INCDIR" >> install/configuration.$HOSTNAME
echo "X11LIBDIR=$X11LIBDIR" >> install/configuration.$HOSTNAME
echo "X11BINDIR=$X11BINDIR" >> install/configuration.$HOSTNAME
echo "X11BITDIR=$X11BITDIR" >> install/configuration.$HOSTNAME
echo "X11SYSDIR=$X11SYSDIR" >> install/configuration.$HOSTNAME
echo "X11APPDIR=$X11APPDIR" >> install/configuration.$HOSTNAME

echo ""
echo  "Press <RETURN> to continue:     "
read sludge
	echo "*********************************************************************"
echo "Motif maintains man pages in"
echo "$MOTIFROOT/usr/man/man1 and $MOTIFROOT/usr/man/man3."
echo ""
	echo "You may add $MOTIFROOT/usr/man to your MANPATH environment variable"
	echo "or copy the files therein to the appropriate places in /usr/man"
	echo " "
	echo "Various Motif demos can be found in $MOTIFROOT/demos."
	echo ""
	echo "OSF/MOTIF 1.2 is now installed...enjoy!"
	echo ""
	echo "*********************************************************************"
echo  "Press <RETURN> to continue:     "
read salsa
	echo ""

...\" **
...\" **
...\" **  (c) Copyright 1989, 1990, 1992 by Open Software Foundation, Inc.
...\" **      All Rights Reserved.
...\" **
...\" **  (c) Copyright 1987, 1988, 1989, by Hewlett-Packard Company
...\" **
...\" **
.TH XmScrolledWindow 3X "" "" "" ""
.SH NAME
\*LXmScrolledWindow\*O\(emThe ScrolledWindow widget class
.iX "XmScrolledWindow"
.iX "widget class" "ScrolledWindow"
.SH SYNOPSIS
.sS
.iS
\&#include <Xm/ScrolledW.h>
.iE
.sE
.SH DESCRIPTION
The ScrolledWindow widget combines one or two ScrollBar widgets and a
viewing area to implement a visible window onto some other (usually larger)
data display.  The visible part of the window can be scrolled through the
larger display by the use of ScrollBars.
.PP 
To use ScrolledWindow, an application first creates a ScrolledWindow
widget, any needed ScrollBar widgets, and a widget capable of displaying
any desired data as the work area of ScrolledWindow.  ScrolledWindow
positions the work area widget and displays the ScrollBars if so
requested.  When the user performs some action on the ScrollBar, the
application is notified through the normal ScrollBar callback
interface.
.PP 
ScrolledWindow can be configured to operate automatically so
that it performs all scrolling and display actions with no need for application
program involvement.  It can also be configured to provide a minimal support
framework in which the application is responsible for processing all user input
and making all visual changes to the displayed data in response to that input.
.PP 
When ScrolledWindow is performing automatic scrolling it creates a
clipping window and automatically creates the scroll bars.
Conceptually, this window becomes the viewport through which
the user examines the larger underlying data area.  The application simply
creates the desired data, then makes that data the work area of the
ScrolledWindow.
When the user moves the slider to change the displayed data, the
workspace is moved under the viewing area so that a new portion of the data
becomes visible.
.PP 
Sometimes it is impractical for an application to create a
large data space and simply display it through a small clipping window.  For
example, in a text editor, creating a single data area that consisted of a
large file would involve an undesirable amount of overhead.
The application needs to use a ScrolledWindow
(a small viewport onto some larger data), but needs to be notified
when the user scrolled the viewport so it could bring in more data from
storage and update the display area.  For these cases the ScrolledWindow can be
configured so that it provides only visual layout support.  No clipping window
is created, and the application must maintain the data displayed in the
work area, as well as respond to user input on the ScrollBars.
.PP
The user can specify resources in a resource file for the automatically
created widgets that contain the horizontal and vertical scrollbars of
the ScrolledWindow widget.  The names of these widgets are "HorScrollBar"
and "VertScrollBar", and remain consistent whether created by
\*LXmCreateScrolledList\*O, \*LXmCreateScrolledText\*O or
\*LXmCreateScrolledWindow\*O.
.SS "Classes"
ScrolledWindow inherits behavior and
resources from \*LCore\*O, \*LComposite\*O,
\*LConstraint\*O, and \*LXmManager\*O Classes.
.PP 
The class pointer is \*LxmScrolledWindowWidgetClass\*O.
.PP 
The class name is \*LXmScrolledWindow\*O.
.SS "New Resources"
The following table defines a set of widget resources used by the programmer
to specify data.  The programmer can also set the resource values for the
inherited classes to set attributes for this widget.  To reference a
resource by name or by class in a .Xdefaults file, remove the \*LXmN\*O or
\*LXmC\*O prefix and use the remaining letters.  To specify one of the defined
values for a resource in a .Xdefaults file, remove the \*LXm\*O prefix and use
the remaining letters (in either lowercase or uppercase, but include any
underscores between words).
The codes in the access column indicate if the given resource can be
set at creation time (C),
set by using \*LXtSetValues\*O (S),
retrieved by using \*LXtGetValues\*O (G), or is not applicable (N/A).
.P 
.TS 
center allbox;
cBp9 ssss
lBp8 lBp8 lBp8 lBp8 lBp8
lp8 lp8 lp8 lp8 lp8.
XmScrolledWindow Resource Set
Name	Class	Type	Default	Access
_
XmNclipWindow	XmCClipWindow	Widget	dynamic	G
XmNhorizontalScrollBar	XmCHorizontalScrollBar	Widget	dynamic	CSG
XmNscrollBarDisplayPolicy	XmCScrollBarDisplayPolicy	unsigned char	dynamic	CSG
XmNscrollBarPlacement	XmCScrollBarPlacement	unsigned char	XmBOTTOM_RIGHT	CSG
XmNscrolledWindowMarginHeight	XmCScrolledWindowMarginHeight	Dimension	0	CSG
XmNscrolledWindowMarginWidth	XmCScrolledWindowMarginWidth	Dimension	0	CSG
XmNscrollingPolicy	XmCScrollingPolicy	unsigned char	XmAPPLICATION_DEFINED	CG
XmNspacing	XmCSpacing	Dimension	4	CSG
XmNtraverseObscuredCallback	XmCCallback	XtCallbackList	NULL	CSG
XmNverticalScrollBar	XmCVerticalScrollBar	Widget	dynamic	CSG
XmNvisualPolicy	XmCVisualPolicy	unsigned char	dynamic	G
XmNworkWindow	XmCWorkWindow	Widget	NULL    	CSG
.TE
.VL  
.LI "\*LXmNclipWindow\*O"
Specifies the widget ID of the clipping area.  This
is automatically created by
ScrolledWindow when the \*LXmNvisualPolicy\*O resource is set to
\*LXmCONSTANT\*O and
can only be read by the application.  Any attempt to set this resource to a
new value causes a warning message to be printed by the scrolled
window.  If the \*LXmNvisualPolicy\*O resource is set to \*LXmVARIABLE\*O, this
resource is set to NULL, and no clipping window is created.
.LI "\*LXmNhorizontalScrollBar\*O"
Specifies the widget ID of the horizontal ScrollBar.
This is automatically created by ScrolledWindow when the
\*LXmNscrollingPolicy\*O is initialized to \*LXmAUTOMATIC\*O; otherwise,
the default is NULL.
.LI "\*LXmNscrollBarDisplayPolicy\*O"
Controls the automatic placement of the ScrollBars.  If
it is set to \*LXmAS_NEEDED\*O and if \*LXmNscrollingPolicy\*O is
set to \*LXmAUTOMATIC\*O, ScrollBars are displayed only if the
workspace exceeds the clip area in one or both dimensions.  A resource value
of \*LXmSTATIC\*O causes the ScrolledWindow
to display the ScrollBars whenever
they are managed, regardless of the relationship between the clip window
and the work area.  This resource must be \*LXmSTATIC\*O when
\*LXmNscrollingPolicy\*O is \*LXmAPPLICATION_DEFINED\*O.
The default is \*LXmAS_NEEDED\*O when \*LXmNscrollingPolicy\*O is
\*LXmAUTOMATIC\*O, and \*LXmSTATIC\*O otherwise.
.LI "\*LXmNscrollBarPlacement\*O"
Specifies the positioning of the ScrollBars
in relation to the work window.  The following are the values:
.wH .rS 
.ML
.LI
\*LXmTOP_LEFT\*O\(emThe horizontal ScrollBar is placed above the
work window; the vertical ScrollBar to
the left.
.LI
\*LXmBOTTOM_LEFT\*O\(emThe horizontal ScrollBar is placed below the
work window; the vertical ScrollBar to
the left.
.LI
\*LXmTOP_RIGHT\*O\(emThe horizontal ScrollBar is placed above the
work window; the vertical ScrollBar to
the right.
.LI
\*LXmBOTTOM_RIGHT\*O\(emThe horizontal ScrollBar is placed below the
work window; the vertical ScrollBar to
the right.
.LE 
.wH .rE
.PP
The default value may depend on the value of the
\*LXmNstringDirection\*O resource.
.nL
.ne 5
.LI "\*LXmNscrolledWindowMarginHeight\*O"
Specifies the margin height on the top and bottom of the
ScrolledWindow.
.LI "\*LXmNscrolledWindowMarginWidth\*O"
Specifies the margin width on the right and left sides of the
ScrolledWindow.
.LI "\*LXmNscrollingPolicy\*O"
Performs automatic scrolling of the
work area with no application interaction.  If the value of this resource
is \*LXmAUTOMATIC\*O, ScrolledWindow automatically creates the
ScrollBars; attaches callbacks to the
ScrollBars; sets the visual policy to \*LXmCONSTANT\*O;
and automatically moves the
work area through the clip window in response to any user interaction with
the ScrollBars.  An application can also add its own callbacks to the
ScrollBars.  This allows the application to be notified of a scroll event
without having to perform any layout procedures.
.PP
\*LNOTE\*O:  Since the
ScrolledWindow adds callbacks to the ScrollBars, an application should not
perform an \*LXtRemoveAllCallbacks\*O on any of the ScrollBar widgets.
.PP
When \*LXmNscrollingPolicy\*O is set to
\*LXmAPPLICATION_DEFINED\*O, the application
is responsible for all aspects of scrolling.  The ScrollBars must be created
by the application, and it is responsible for performing any visual changes
in the work area in response to user input.
.PP
This resource must be set to the desired policy at the time the
ScrolledWindow is created.  It cannot be changed through \*LSetValues\*O.
.LI "\*LXmNspacing\*O"
Specifies the distance that separates the ScrollBars from the
work window.
.LI "\*LXmNtraverseObscuredCallback\*O"
Specifies a list of callbacks that is called when traversing to
a widget or gadget that is obscured due to its position in the
work area relative to the location of the ScrolledWindow viewport.
This resource is valid only when \*LXmNscrollingPolicy\*O is
\*LXmAUTOMATIC\*O.  If this resource is NULL, an obscured widget
cannot be traversed to.  The callback reason is
\*LXmCR_OBSCURED_TRAVERSAL\*O.
.LI "\*LXmNverticalScrollBar\*O"
Specifies the widget ID of the vertical ScrollBar.
This is automatically created by ScrolledWindow when the
\*LXmNscrollingPolicy\*O is initialized to \*LXmAUTOMATIC\*O; otherwise,
the default is NULL.
.nL
.ne 6
.LI "\*LXmNvisualPolicy\*O"
Grows the ScrolledWindow to match the size of the work area, or it can
be used as a static viewport onto a larger data space.  If the visual policy
is \*LXmVARIABLE\*O, the ScrolledWindow forces the ScrollBar display
policy to \*LXmSTATIC\*O and
allow the work area to grow or shrink
at any time and adjusts its layout to accommodate the new size.  When
the policy is \*LXmCONSTANT\*O, the work area grows or shrinks
as requested, but a clipping window forces the size of the visible
portion to remain constant.  The only time the viewing area can grow is
in response
to a resize from the ScrolledWindow's parent.
The default is \*LXmCONSTANT\*O when \*LXmNscrollingPolicy\*O is
\*LXmAUTOMATIC\*O, and \*LXmVARIABLE\*O otherwise.
.PP
\*LNOTE\*O:  This resource must be set to the desired policy at the time the
ScrolledWindow is created.  It cannot be changed through \*LSetValues\*O.
.LI "\*LXmNworkWindow\*O"
Specifies the widget ID of the viewing area.
.LE 
.SS "Inherited Resources"
ScrolledWindow inherits behavior and resources from the following
superclasses.  For a complete description of each resource, refer to the
man page for that superclass.
.P
.ne 3i
.TS 
center allbox;
cBp9 ssss
lBp8 lBp8 lBp8 lBp8 lBp8
lp8 lp8 lp8 lp8 lp8.
XmManager Resource Set
Name	Class	Type	Default	Access
_
XmNbottomShadowColor	XmCBottomShadowColor	Pixel	dynamic	CSG
XmNbottomShadowPixmap	XmCBottomShadowPixmap	Pixmap	XmUNSPECIFIED_PIXMAP	CSG
XmNforeground	XmCForeground	Pixel	dynamic	CSG
XmNhelpCallback	XmCCallback	XtCallbackList	NULL	C
XmNhighlightColor	XmCHighlightColor	Pixel	dynamic	CSG
XmNhighlightPixmap	XmCHighlightPixmap	Pixmap	dynamic	CSG
XmNinitialFocus	XmCInitialFocus	Widget	NULL	CSG
XmNnavigationType	XmCNavigationType	XmNavigationType	XmTAB_GROUP	CSG
XmNshadowThickness	XmCShadowThickness	Dimension	dynamic	CSG
XmNstringDirection	XmCStringDirection	XmStringDirection	dynamic	CG
XmNtopShadowColor	XmCTopShadowColor	Pixel	dynamic	CSG
XmNtopShadowPixmap	XmCTopShadowPixmap	Pixmap	dynamic	CSG
XmNtraversalOn	XmCTraversalOn	Boolean	True	CSG
XmNunitType	XmCUnitType	unsigned char	dynamic	CSG
XmNuserData	XmCUserData	XtPointer	NULL	CSG
.TE
.P 
.TS 
center allbox;
cBp9 ssss
lBp8 lBp8 lBp8 lBp8 lBp8
lp8 lp8 lp8 lp8 lp8.
Composite Resource Set
Name	Class	Type	Default	Access
_
XmNchildren	XmCReadOnly	WidgetList	NULL	G
XmNinsertPosition	XmCInsertPosition	XtOrderProc	NULL	CSG
XmNnumChildren	XmCReadOnly	Cardinal	0	G
.TE
.P 
.wH .in 0 
.TS 
center allbox;
cBp9 ssss
lBp8 lBp8 lBp8 lBp8 lBp8
lp8 lp8 lp8 lp8 lp8.
Core Resource Set
Name	Class	Type	Default	Access
_
XmNaccelerators	XmCAccelerators	XtAccelerators	dynamic	CSG
XmNancestorSensitive	XmCSensitive	Boolean	dynamic	G
XmNbackground	XmCBackground	Pixel	dynamic	CSG
XmNbackgroundPixmap	XmCPixmap	Pixmap	XmUNSPECIFIED_PIXMAP	CSG
XmNborderColor	XmCBorderColor	Pixel	XtDefaultForeground	CSG
XmNborderPixmap	XmCPixmap	Pixmap	XmUNSPECIFIED_PIXMAP	CSG
XmNborderWidth	XmCBorderWidth	Dimension	0	CSG
XmNcolormap	XmCColormap	Colormap	dynamic	CG
XmNdepth	XmCDepth	int	dynamic	CG
XmNdestroyCallback	XmCCallback	XtCallbackList	NULL	C
XmNheight	XmCHeight	Dimension	dynamic	CSG
XmNinitialResourcesPersistent	XmCInitialResourcesPersistent	Boolean	True	C
XmNmappedWhenManaged	XmCMappedWhenManaged	Boolean	True	CSG
XmNscreen	XmCScreen	Screen *	dynamic	CG
.wH .tH
XmNsensitive	XmCSensitive	Boolean	True	CSG
XmNtranslations	XmCTranslations	XtTranslations	dynamic	CSG
XmNwidth	XmCWidth	Dimension	dynamic	CSG
XmNx	XmCPosition	Position	0	CSG
XmNy	XmCPosition	Position	0	CSG
.TE
.wH .in  
.SS "Callback Information"
The application must use the ScrollBar callbacks to be notified of user
input.
.PP
ScrolledWindow defines a callback structure
for use with \*LXmNtraverseObscuredCallback\*O callbacks. The
\*LXmNtraverseObscuredCallback\*O resource provides a mechanism
for traversal to obscured widgets (or gadgets) due to their
position in the work area of a ScrolledWindow.  The
\*LXmNtraverseObscuredCallback\*O routine has responsibility
for adjusting the position of the work area such that the
specified traversal destination widget is positioned within
the viewport of the ScrolledWindow.  A NULL \*LXmNtraverseObscuredCallback\*O
resource causes obscured widgets within the ScrolledWindow
to be non-traversable.
.PP
Traversal to an obscured widget or gadget requires these
conditions to be met: the widget or gadget can be obscured
only due to its position in the work area of a ScrolledWindow
relative to the viewport; the viewport of the associated ScrolledWindow is
fully visible, or can be made so by virtue of ancestral
\*LXmNtraverseObscuredCallback\*O routines; and the
\*LXmNtraverseObscuredCallback\*O resource must be non-NULL.
.PP
When ScrolledWindow widgets are nested, the
\*LXmNtraverseObscuredCallback\*O routine for each ScrolledWindow
that obscures the traversal destination is called in ascending order
within the given hierarchy.
.PP
A pointer to the following structure is passed to callbacks
for \*LXmNtraverseObscuredCallback\*O.
.sS
.iS
.ta .25i 1.75i
typedef struct
.ne 4
{
	int	\*Vreason\*L;
	XEvent	*\*Vevent\*L:
        Widget	\*Vtraversal_destination\*L;
        XmTraversalDirection	\*Vdirection\*L;
} XmTraverseObscuredCallbackStruct;
.iE
.sE
.wH .fi
.VL .75i
.LI "\*Vreason\*O"
Indicates why the callback was invoked.
.LI "\*Vevent\*O"
Points to the \*LXEvent\*O that triggered the callback.
.LI "\*Vtraversal_destination\*O"
Specifies the widget or gadget to traverse to, which will
be a descendant of the work window.
.LI "\*Vdirection\*O"
Specifies the direction of traversal.  See 
the description of the \*Vdirection\*O parameter in
the \*LXmProcessTraversal\*O man page for an explanation
of the valid values.
.LE
.SS "Translations"
XmScrolledWindow includes the translations from XmManager.
.SS "Additional Behavior"
.ne 6i
This widget has the additional behavior described below:
.VL  
.LI "\*LKPageUp\*O:"
If \*LXmNscrollingPolicy\*O is \*LXmAUTOMATIC\*O,
scrolls the window up the height of the viewport.
The distance scrolled may be reduced to provide some overlap.
The actual distance scrolled depends on the
\*LXmNpageIncrement\*O resource of the vertical ScrollBar.
.nL
.ne 8
.LI "\*LKPageDown\*O:"
If \*LXmNscrollingPolicy\*O is \*LXmAUTOMATIC\*O,
scrolls the window down the height of the viewport.
The distance scrolled may be reduced to provide some overlap.
The actual distance scrolled depends on the
\*LXmNpageIncrement\*O resource of the vertical ScrollBar.
.LI "\*LKPageLeft\*O:"
If \*LXmNscrollingPolicy\*O is \*LXmAUTOMATIC\*O,
scrolls the window left the width of the viewport.
The distance scrolled may be reduced to provide some overlap.
The actual distance scrolled depends on the
\*LXmNpageIncrement\*O resource of the horizontal ScrollBar.
.LI "\*LKPageRight\*O:"
If \*LXmNscrollingPolicy\*O is \*LXmAUTOMATIC\*O,
scrolls the window right the width of the viewport.
The distance scrolled may be reduced to provide some overlap.
The actual distance scrolled depends on the
\*LXmNpageIncrement\*O resource of the horizontal ScrollBar.
.LI "\*LKBeginLine\*O:"
If \*LXmNscrollingPolicy\*O is \*LXmAUTOMATIC\*O,
scrolls the window horizontally to the edge corresponding to the
horizontal ScrollBar's minimum value.
.LI "\*LKEndLine\*O:"
If \*LXmNscrollingPolicy\*O is \*LXmAUTOMATIC\*O,
scrolls the window horizontally to the edge corresponding to the
horizontal ScrollBar's maximum value.
.P 
.ne 6i
.LI "\*LKBeginData\*O:"
If \*LXmNscrollingPolicy\*O is \*LXmAUTOMATIC\*O,
scrolls the window vertically to the edge corresponding to the
vertical ScrollBar's minimum value.
.LI "\*LKEndData\*O:"
If \*LXmNscrollingPolicy\*O is \*LXmAUTOMATIC\*O,
scrolls the window vertically to the edge corresponding to the
vertical ScrollBar's maximum value.
.LE
.PP
Certain applications will want to replace the page bindings with
ones that are specific to the content of the scrolled area.
.cS
.SS "Geometry Management"
ScrolledWindow makes extensive use of the \*LXtQueryGeometry\*O
functionality to facilitate geometry communication between
application levels.  In the \*LXmAPPLICATION_DEFINED\*O scrolling
policy, the WorkWindow's query procedure is called by the
ScrolledWindow whenever the ScrolledWindow is going to change
its size.  The widget calculates the largest possible
workspace area and passes this size to the WorkWindow widget's
query procedure.  The query procedure can then examine this new
size and determine if any changes, such as
managing or unmanaging a ScrollBar, are necessary.  The query procedure
performs whatever actions it needs and then returns to the
ScrolledWindow.  The ScrolledWindow then examines the
ScrollBars to see which (if any) are managed, allocates a
portion of the visible space for them, and resizes the
WorkWindow to fit in the rest of the space.
.PP 
When the scrolling policy is \*LXmCONSTANT\*O, the ScrolledWindow can
be queried to return the optimal size for a given dimension.
The optimal size is defined to be the size that just
encloses the WorkWindow.  By using this mechanism, an application
can size the ScrolledWindow so that it needs to display a
ScrollBar only for one dimension.  When the ScrolledWindow's query
procedure is called via \*LXtQueryGeometry\*O, the request is examined
to see if the width or height has been specified.  If so, the
routine uses the given dimension as the basis for its
calculations.  It determines the minimum value for the other
dimension that just encloses the WorkWindow, fills in the
appropriate elements in the reply structure, and returns to the
calling program.  Occasionally, using the specified width or
height and the other minimum dimension results in neither
ScrollBar appearing.  When this happens, the query procedure
sets both the width and height fields, indicating that in
this situation the ideal size causes a change in both
dimensions.  If the calling application sets both the width and
height fields, the ScrolledWindow determines the minimum
size for both dimensions and return those values in the reply
structure.
.cE
.nL
.ne 10
.SS "Virtual Bindings"
The bindings for virtual keys are vendor specific.
For information about bindings for virtual buttons and keys, see \*LVirtualBindings(3X)\*O.
.SH RELATED INFORMATION
.na
\*LComposite(3X)\*O,
\*LConstraint(3X)\*O,
\*LCore(3X)\*O,
\*LXmCreateScrolledWindow(3X)\*O,
\*LXmManager(3X)\*O,
\*LXmProcessTraversal(3X)\*O,
\*LXmScrollBar(3X)\*O,
\*LXmScrollVisible(3X)\*O, and
\*LXmScrolledWindowSetAreas(3X)\*O.
.ad

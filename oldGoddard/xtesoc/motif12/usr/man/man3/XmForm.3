...\" **
...\" **
...\" **  (c) Copyright 1989, 1990, 1992 by Open Software Foundation, Inc.
...\" **      All Rights Reserved.
...\" **
...\" **  (c) Copyright 1987, 1988, 1989, by Hewlett-Packard Company
...\" **
...\" **  (c) Copyright 1987, 1988 by Digital Equipment Corporation,
...\" **      Maynard, MA.  All Rights Reserved.
...\" **
...\" **
.TH XmForm 3X "" "" "" ""
.SH NAME
\*LXmForm\*O\(emThe Form widget class
.iX "XmForm"
.iX "widget class" "Form"
.SH SYNOPSIS
.sS
.iS
\&#include <Xm/Form.h>
.iE
.sE
.SH DESCRIPTION
Form is a container widget with no input semantics of its own.
Constraints are placed on children of the Form to define attachments
for each of the child's four sides.
These attachments can be to the Form, to another child widget or gadget,
to a relative position within the Form, or to the initial position of
the child.
The attachments determine the layout behavior of the Form when resizing
occurs.
.PP
The default value for \*LXmNinitialFocus\*O is the value of
\*LXmNdefaultButton\*O.
.P
Following are some important considerations in using a Form:
.ML
.LI
Every child must have an attachment on either the left or the right.
If initialization or \*LXtSetValues\*O leaves a widget without
such an attachment, the result depends upon the value of
\*LXmNrubberPositioning\*O.
.PP
If \*LXmNrubberPositioning\*O is False, the child is given an
\*LXmNleftAttachment\*O of \*LXmATTACH_FORM\*O and an
\*LXmNleftOffset\*O equal to its current \*Vx\*O value.
.PP
.ne 10
If \*LXmNrubberPositioning\*O is True, the child is given an
\*LXmNleftAttachment\*O of \*LXmATTACH_POSITION\*O and an
\*LXmNleftPosition\*O proportional to the current \*Vx\*O value divided
by the width of the Form.
.PP
In either case, if the child has not been previously given an \*Vx\*O
value, its \*Vx\*O value is taken to be 0, which places the child at the
left side of the Form.
.LI
If you want to create a child without any attachments, and then
later (e.g., after creating and managing it, but before realizing it)
give it a right attachment via \*LXtSetValues\*O, you must set its
\*LXmNleftAttachment\*O to \*LXmATTACH_NONE\*O at the same time.
.LI
The \*LXmNresizable\*O resource controls only whether a geometry request
by the child will be granted.
It has no effect on whether the child's size can be changed because of
changes in geometry of the Form or of other children.
.LI
Every child has a preferred width, based on geometry requests it
makes (whether they are granted or not).
.LI
If a child has attachments on both the left and the right sides,
its size is completely controlled by the Form.
It can be shrunk below its preferred width or enlarged above it, if
necessary, due to other constraints.
In addition, the child's geometry requests to change its own width may
be refused.
.LI
If a child has attachments on only its left or right side, it will
always be at its preferred width (if resizable, otherwise at is current
width).
This may cause it to be clipped by the Form or by other children.
.LI
If a child's left (or right) attachment is set to \*LXmATTACH_SELF\*O, its
corresponding left (or right) offset is forced to 0.
The attachment is then changed to \*LXmATTACH_POSITION\*O, with a
position that corresponds to \*Vx\*O value of the child's left (or
right) edge.
To fix the position of a side at a specific \*Vx\*O value use
\*LXmATTACH_FORM\*O or \*LXmATTACH_OPPOSITE_FORM\*O with the \*Vx\*O
value as the left (or right) offset.
.LI
Unmapping a child has no effect on the Form except that the child
is not mapped.
.nL
.ne 15
.LI
Unmanaging a child unmaps it.
If no other child is attached to it, or if all children attached to it
and all children recursively attached to them are also all unmanaged,
all of those children are treated as if they did not exist in
determining the size of the Form.
.LI
When using \*LXtSetValues\*O to change the \*LXmNx\*O resource of a
child, you must simultaneously set its left attachment to either
\*LXmATTACH_SELF\*O or \*LXmATTACH_NONE\*O.
Otherwise, the request is not granted.
If \*LXmNresizable\*O is False, the request is granted only if the
child's size can remain the same.
.LI
A left (or right) attachment of \*LXmATTACH_WIDGET\*O, where
\*LXmNleftWidget\*O (or \*LXmNrightWidget\*O) is NULL, acts like an
attachment of \*LXmATTACH_FORM\*O.
.LI
If an attachment is made to a widget that is not a child of the
Form, but an ancestor of the widget is a child of the Form, the
attachment is made to the ancestor.
.LE 
.PP 
All these considerations are true of top and bottom attachments as well,
with top acting like left, bottom acting like right, \*Vy\*O acting like
\*Vx\*O, and height acting like width.
.SS "Classes"
Form inherits behavior and resources from \*LCore\*O,
\*LComposite\*O, \*LConstraint\*O,
\*LXmManager\*O, and \*LXmBulletinBoard\*O classes.
.PP 
The class pointer is \*LxmFormWidgetClass\*O.
.PP 
The class name is \*LXmForm\*O.
.SS "New Resources"
The following table defines a set of widget resources used by the
programmer to specify data.  The programmer can also set the resource
values for the inherited classes to set attributes for this widget.
To reference a resource by name or by class in a .Xdefaults file,
remove the \*LXmN\*O or \*LXmC\*O prefix and use the remaining letters.
To specify one of the defined values for a resource in a .Xdefaults
file, remove the \*LXm\*O prefix and use the remaining letters (in
.ne 10
either lowercase or uppercase, but include any underscores between
words).  The codes in the access column indicate if the given resource
can be set at creation time (C), set by using \*LXtSetValues\*O
(S), retrieved by using \*LXtGetValues\*O (G), or is not
applicable (N/A).
.P 
.TS 
center allbox;
cBp9 ssss
lBp8 lBp8 lBp8 lBp8 lBp8
lp8 lp8 lp8 lp8 lp8.
XmForm Resource Set
Name	Class	Type	Default	Access
_
XmNfractionBase	XmCMaxValue	int	100	CSG
XmNhorizontalSpacing	XmCSpacing	Dimension	0	CSG
XmNrubberPositioning	XmCRubberPositioning	Boolean	False	CSG
XmNverticalSpacing	XmCSpacing	Dimension	0	CSG
.TE
.VL  
.LI "\*LXmNfractionBase\*O"
Specifies the denominator used in calculating the relative position of
a child widget using \*LXmATTACH_POSITION\*O constraints.
The value must not be 0.
.PP
If the value of a child's \*LXmNleftAttachment\*O (or
\*LXmNrightAttachment\*O) is \*LXmATTACH_POSITION\*O, the position of
the left (or right) side of the child is relative to the left
side of the Form and is a fraction of the width of the Form.
This fraction is the value of the child's \*LXmNleftPosition\*O (or
\*LXmNrightPosition\*O) resource divided by the value of the Form's
\*LXmNfractionBase\*O.
.PP
If the value of a child's \*LXmNtopAttachment\*O (or
\*LXmNbottomAttachment\*O) is \*LXmATTACH_POSITION\*O, the position of
the top (or bottom) side of the child is relative to the top
side of the Form and is a fraction of the height of the Form.
This fraction is the value of the child's \*LXmNtopPosition\*O (or
\*LXmNbottomPosition\*O) resource divided by the value of the Form's
\*LXmNfractionBase\*O.
.nL
.ne 15
.LI "\*LXmNhorizontalSpacing\*O"
Specifies the offset for right and left attachments.  This resource is
only used if no offset resource is specified (when attaching to a 
widget), or if no margin resource is specified (when attaching 
to the Form).
.LI "\*LXmNrubberPositioning\*O"
Indicates the default near (left) and top attachments for a child of the
Form.
(\*LNote:\*O  Whether this resource actually applies to the left or
right side of the child and its attachment may depend on the value of
the \*LXmNstringDirection\*O resource.)
.PP
The default left attachment is applied whenever initialization or
\*LXtSetValues\*O leaves the child without either a left or right
attachment.
The default top attachment is applied whenever initialization or
\*LXtSetValues\*O leaves the child without either a top or bottom
attachment.
.P
If this Boolean resource is set to False, \*LXmNleftAttachment\*O and
\*LXmNtopAttachment\*O default to \*LXmATTACH_FORM\*O,
\*LXmNleftOffset\*O defaults to the current \*Vx\*O value of the left
side of the child,
and \*LXmNtopOffset\*O defaults to the current
\*Vy\*O value of the child.
The effect is to position the child according to its absolute distance
from the left or top side of the Form.
.PP
If this resource is set to True, \*LXmNleftAttachment\*O and
\*LXmNtopAttachment\*O default to \*LXmATTACH_POSITION\*O,
\*LXmNleftPosition\*O defaults to a value proportional to the current
\*Vx\*O value of the left side of the child divided by the width of the
Form, and \*LXmNtopPosition\*O defaults to a value proportional to the
current \*Vy\*O value of the child divided by the height of the Form.
The effect is to position the child relative to the left or top
side of the Form and in proportion to the width or height of the Form.
.LI "\*LXmNverticalSpacing\*O"
Specifies the offset for top and bottom attachments.  This resource is
only used if no offset resource is specified (when attaching to a 
widget), or if no margin resource is specified (when attaching 
to the Form).
.P 
.wH .in 0 
.TS 
center allbox;
cBp9 ssss
lBp8 lBp8 lBp8 lBp8 lBp8
lp8 lp8 lp8 lp8 lp8.
XmForm Constraint Resource Set
Name	Class	Type	Default	Access
_
XmNbottomAttachment	XmCAttachment	unsigned char	XmATTACH_NONE	CSG
XmNbottomOffset	XmCOffset	int	0	CSG
XmNbottomPosition	XmCAttachment	int	0	CSG
XmNbottomWidget	XmCWidget	Widget	NULL	CSG
XmNleftAttachment	XmCAttachment	unsigned char	XmATTACH_NONE	CSG
XmNleftOffset	XmCOffset	int	0	CSG
XmNleftPosition	XmCAttachment	int	0	CSG
XmNleftWidget	XmCWidget	Widget	NULL	CSG
XmNresizable	XmCBoolean	Boolean	True	CSG
XmNrightAttachment	XmCAttachment	unsigned char	XmATTACH_NONE	CSG
XmNrightOffset	XmCOffset	int	0	CSG
XmNrightPosition	XmCAttachment	int	0	CSG
XmNrightWidget	XmCWidget	Widget	NULL	CSG
XmNtopAttachment	XmCAttachment	unsigned char	XmATTACH_NONE	CSG
.wH .tH
XmNtopOffset	XmCOffset	int	0	CSG
XmNtopPosition	XmCAttachment	int	0	CSG
XmNtopWidget	XmCWidget	Widget	NULL	CSG
.TE
.wH .in  
.LI "\*LXmNbottomAttachment\*O"
Specifies attachment of the bottom side of the child.  It can have the
following values:
.wH .rS 
.ML
.LI
\*LXmATTACH_NONE\*O\(emDo not attach the bottom side of the child.
.LI
\*LXmATTACH_FORM\*O\(emAttach the bottom side of the child to the bottom
side of the Form.
.LI
\*LXmATTACH_OPPOSITE_FORM\*O\(emAttach the bottom side of the child to
the top side of the Form.
\*LXmNbottomOffset\*O can be used to determine the visibility of the
child.
.LI
\*LXmATTACH_WIDGET\*O\(emAttach the bottom side of the child to the top
side of the widget or
gadget specified in the \*LXmNbottomWidget\*O resource.
If \*LXmNbottomWidget\*O is NULL, \*LXmATTACH_WIDGET\*O is replaced by
\*LXmATTACH_FORM\*O, and the child is attached to the bottom side of the
Form.
.LI
\*LXmATTACH_OPPOSITE_WIDGET\*O\(emAttach the bottom side of the child to
the bottom side of the widget or
gadget specified in the \*LXmNbottomWidget\*O resource.
.nL
.ne 20
.LI
\*LXmATTACH_POSITION\*O\(emAttach the bottom side of the child to a
position that is relative to
the top side of the Form and in proportion to the height of the Form.
This position is determined by the \*LXmNbottomPosition\*O and
\*LXmNfractionBase\*O resources.
.LI
\*LXmATTACH_SELF\*O\(emAttach the bottom side of the child to a position
that is proportional
to the current \*Vy\*O value of the bottom of the child divided by the
height of the Form.
This position is determined by the \*LXmNbottomPosition\*O and
\*LXmNfractionBase\*O resources.
.ne 4
\*LXmNbottomPosition\*O is set to a value proportional to the current
\*Vy\*O value of the bottom of the child divided by the height of the
Form.
.LE 
.wH .rE
.LI "\*LXmNbottomOffset\*O"
Specifies the constant offset between the bottom side of the
child and the object to which it is
attached.
The relationship established remains, regardless of any resizing operations
that occur.
When this resource is explicitly set, the value of \*LXmNverticalSpacing\*O
is ignored.
.LI "\*LXmNbottomPosition\*O"
This resource is used to determine the position of the bottom side of
the child when the child's \*LXmNbottomAttachment\*O is set to
\*LXmATTACH_POSITION\*O.
In this case the position of the bottom side of the child is relative to
the top side of the Form and is a fraction of the height of the Form.
This fraction is the value of the child's \*LXmNbottomPosition\*O
resource divided by the value of the Form's \*LXmNfractionBase\*O.
For example, if the child's \*LXmNbottomPosition\*O is 50, the Form's
\*LXmNfractionBase\*O is 100, and the Form's height is 200, the position
of the bottom side of the child is 100.
.LI "\*LXmNbottomWidget\*O"
Specifies the widget or gadget to which
the bottom side of the child is attached.
This resource is used if \*LXmNbottomAttachment\*O is set to either
\*LXmATTACH_WIDGET\*O
or \*LXmATTACH_OPPOSITE_WIDGET\*O.
.PP
A string-to-widget resource converter is automatically installed for use
with this resource.
With this converter, the widget that is to be the value of the resource
must exist at the time the widget that has the resource is created.
.nL
.ne 15
.LI "\*LXmNleftAttachment\*O"
Specifies attachment of the near (left) side of the child.
(\*LNote:\*O  Whether this resource actually applies to the left or
right side of the child and its attachment may depend on the value of
the \*LXmNstringDirection\*O resource.)
It can have the following values:
.wH .rS 
.ML
.LI
\*LXmATTACH_NONE\*O\(emDo not attach the left side of the child.
If \*LXmNrightAttachment\*O is also \*LXmATTACH_NONE\*O, this value is
ignored and the child is given a default left attachment.
.LI
\*LXmATTACH_FORM\*O\(emAttach the left side of the child to the left
side of the Form.
.LI
\*LXmATTACH_OPPOSITE_FORM\*O\(emAttach the left side of the child to the
right side of the Form.
\*LXmNleftOffset\*O can be used to determine the visibility of the
child.
.LI
\*LXmATTACH_WIDGET\*O\(emAttach the left side of the child to the right
side of the widget or
gadget specified in the \*LXmNleftWidget\*O resource.
If \*LXmNleftWidget\*O is NULL, \*LXmATTACH_WIDGET\*O is replaced by
\*LXmATTACH_FORM\*O, and the child is attached to the left side of the
Form.
.LI
\*LXmATTACH_OPPOSITE_WIDGET\*O\(emAttach the left side of the child to
the left side of the widget or
gadget specified in the \*LXmNleftWidget\*O resource.
.LI
\*LXmATTACH_POSITION\*O\(emAttach the left side of the child to a
position that is relative to
the left side of the Form and in proportion to the width of the Form.
This position is determined by the \*LXmNleftPosition\*O and
\*LXmNfractionBase\*O resources.
.LI
\*LXmATTACH_SELF\*O\(emAttach the left side of the child to a position
that is proportional to
the current \*Vx\*O value of the left side of the child divided by the
width of the Form.
This position is determined by the \*LXmNleftPosition\*O and
\*LXmNfractionBase\*O resources.
.ne 6
\*LXmNleftPosition\*O is set to a value proportional to the current
\*Vx\*O value of the left side of the child divided by the width of the
Form.
.LE 
.wH .rE
.ne 15
.LI "\*LXmNleftOffset\*O"
Specifies the constant offset between the near (left) side of the
child and the object to which it is attached.
(\*LNote:\*O  Whether this resource actually applies to the left or
right side of the child and its attachment may depend on the value of
the \*LXmNstringDirection\*O resource.)
The relationship established remains, regardless of any resizing operations
that occur.
When this resource is explicitly set, the value of \*LXmNhorizontalSpacing\*O
is ignored.
.LI "\*LXmNleftPosition\*O"
This resource is used to determine the position of the near (left) side
of the child when the child's \*LXmNleftAttachment\*O is set to
\*LXmATTACH_POSITION\*O.
(\*LNote:\*O  Whether this resource actually applies to the left or
right side of the child and its attachment may depend on the value of
the \*LXmNstringDirection\*O resource.)
.PP
In this case the position of the left side of the child is relative to
the left side of the Form and is a fraction of the width of the Form.
This fraction is the value of the child's \*LXmNleftPosition\*O resource
divided by the value of the Form's \*LXmNfractionBase\*O.
For example, if the child's \*LXmNleftPosition\*O is 50, the Form's
\*LXmNfractionBase\*O is 100, and the Form's width is 200, the position
of the left side of the child is 100.
.LI "\*LXmNleftWidget\*O"
Specifies the widget or gadget to which the near (left) side of the
child is attached.
(\*LNote:\*O  Whether this resource actually applies to the left or
right side of the child and its attachment may depend on the value of
the \*LXmNstringDirection\*O resource.)
This resource is used if \*LXmNleftAttachment\*O
is set to either \*LXmATTACH_WIDGET\*O
or \*LXmATTACH_OPPOSITE_WIDGET\*O.
.PP
A string-to-widget resource converter is automatically installed for use
with this resource.
With this converter, the widget that is to be the value of the resource
must exist at the time the widget that has the resource is created.
.nL
.ne 4
.LI "\*LXmNresizable\*O"
This Boolean resource specifies whether or not a child's request for a
new size is (conditionally) granted by the Form.
If this resource is set to True the request is granted if possible.
If this resource is set to False the request is always refused.
.PP
If a child has both left and right attachments, its width is completely
controlled by the Form, regardless of the value of the child's
\*LXmNresizable\*O resource.
If a child has a left or right attachment but not both, the child's
\*LXmNwidth\*O is used in setting its width if the value of the child's
\*LXmNresizable\*O resource is True.
These conditions are also true for top and bottom attachments, with
height acting like width.
.LI "\*LXmNrightAttachment\*O"
Specifies attachment of the far (right) side of the child.
(\*LNote:\*O  Whether this resource actually applies to the left or
right side of the child and its attachment may depend on the value of
the \*LXmNstringDirection\*O resource.)
It can have the following values:
.wH .rS 
.ML
.LI
\*LXmATTACH_NONE\*O\(emDo not attach the right side of the child.
.LI
\*LXmATTACH_FORM\*O\(emAttach the right side of the child to the right
side of the Form.
.LI
\*LXmATTACH_OPPOSITE_FORM\*O\(emAttach the right side of the child to
the left side of the Form.
\*LXmNrightOffset\*O can be used to determine the visibility of the
child.
.LI
\*LXmATTACH_WIDGET\*O\(emAttach the right side of the child to the left
side of the widget or
gadget specified in the \*LXmNrightWidget\*O resource.
If \*LXmNrightWidget\*O is NULL, \*LXmATTACH_WIDGET\*O is replaced by
\*LXmATTACH_FORM\*O, and the child is attached to the right side of the
Form.
.LI
\*LXmATTACH_OPPOSITE_WIDGET\*O\(emAttach the right side of the child to
the right side of the widget or
gadget specified in the \*LXmNrightWidget\*O resource.
.nL
.ne 4
.LI
\*LXmATTACH_POSITION\*O\(emAttach the right side of the child to a
position that is relative to
the left side of the Form and in proportion to the width of the Form.
This position is determined by the \*LXmNrightPosition\*O and
\*LXmNfractionBase\*O resources.
.LI
\*LXmATTACH_SELF\*O\(emAttach the right side of the child to a position
that is proportional to
the current \*Vx\*O value of the right side of the child divided by the
width of the Form.
This position is determined by the \*LXmNrightPosition\*O and
\*LXmNfractionBase\*O resources.
\*LXmNrightPosition\*O is set to a value proportional to the current
\*Vx\*O value of the right side of the child divided by the width of the
Form.
.LE 
.wH .rE
.LI "\*LXmNrightOffset\*O"
Specifies the constant offset between the far (right) side of the
child and the object to which it is attached.
(\*LNote:\*O  Whether this resource actually applies to the left or
right side of the child and its attachment may depend on the value of
the \*LXmNstringDirection\*O resource.)
The relationship established remains, regardless of any resizing operations
that occur.
When this resource is explicitly set, the value of \*LXmNhorizontalSpacing\*O
is ignored.
.LI "\*LXmNrightPosition\*O"
This resource is used to determine the position of the far (right) side
of the child when the child's \*LXmNrightAttachment\*O is set to
\*LXmATTACH_POSITION\*O.
(\*LNote:\*O  Whether this resource actually applies to the left or
right side of the child and its attachment may depend on the value of
the \*LXmNstringDirection\*O resource.)
.PP
In this case the position of the right side of the child is relative to
the left side of the Form and is a fraction of the width of the Form.
This fraction is the value of the child's \*LXmNrightPosition\*O resource
divided by the value of the Form's \*LXmNfractionBase\*O.
For example, if the child's \*LXmNrightPosition\*O is 50, the Form's
\*LXmNfractionBase\*O is 100, and the Form's width is 200, the position
of the right side of the child is 100.
.nL
.ne 5
.LI "\*LXmNrightWidget\*O"
Specifies the widget or gadget
to which the far (right) side of the child is attached.
(\*LNote:\*O  Whether this resource actually applies to the left or
right side of the child and its attachment may depend on the value of
the \*LXmNstringDirection\*O resource.)
This resource is used if \*LXmNrightAttachment\*O
is set to either \*LXmATTACH_WIDGET\*O
or \*LXmATTACH_OPPOSITE_WIDGET\*O.
.PP
A string-to-widget resource converter is automatically installed for use
with this resource.
With this converter, the widget that is to be the value of the resource
must exist at the time the widget that has the resource is created.
.LI "\*LXmNtopAttachment\*O"
Specifies attachment of the top side of the child.  It can have
following values:
.wH .rS 
.ML
.LI
\*LXmATTACH_NONE\*O\(emDo not attach the top side of the child.
If \*LXmNbottomAttachment\*O is also \*LXmATTACH_NONE\*O, this value is
ignored and the child is given a default top attachment.
.LI
\*LXmATTACH_FORM\*O\(emAttach the top side of the child to the top side
of the Form.
.LI
\*LXmATTACH_OPPOSITE_FORM\*O\(emAttach the top side of the child to the
bottom side of the Form.
\*LXmNtopOffset\*O can be used to determine the visibility of the
child.
.LI
\*LXmATTACH_WIDGET\*O\(emAttach the top side of the child to the bottom
side of the widget or
gadget specified in the \*LXmNtopWidget\*O resource.
If \*LXmNtopWidget\*O is NULL, \*LXmATTACH_WIDGET\*O is replaced by
\*LXmATTACH_FORM\*O, and the child is attached to the top side of the
Form.
.LI
\*LXmATTACH_OPPOSITE_WIDGET\*O\(emAttach the top side of the child to
the top side of the widget or
gadget specified in the \*LXmNtopWidget\*O resource.
.LI
\*LXmATTACH_POSITION\*O\(emAttach the top side of the child to a
position that is relative to
the top side of the Form and in proportion to the height of the Form.
This position is determined by the \*LXmNtopPosition\*O and
\*LXmNfractionBase\*O resources.
.nL
.ne 10
.LI
\*LXmATTACH_SELF\*O\(emAttach the top side of the child to a position
that is proportional to
the current \*Vy\*O value of the child divided by the height of the
Form.
This position is determined by the \*LXmNtopPosition\*O and
\*LXmNfractionBase\*O resources.
\*LXmNtopPosition\*O is set to a value proportional to the current
\*Vy\*O value of the child divided by the height of the Form.
.LE 
.wH .rE
.LI "\*LXmNtopOffset\*O"
Specifies the constant offset between the top side of the
child and the object to which it is
attached.
The relationship established remains, regardless of any resizing operations
that occur.
When this resource is explicitly set, the value of \*LXmNverticalSpacing\*O
is ignored.
.LI "\*LXmNtopPosition\*O"
This resource is used to determine the position of the top side of
the child when the child's \*LXmNtopAttachment\*O is set to
\*LXmATTACH_POSITION\*O.
In this case the position of the top side of the child is relative to
the top side of the Form and is a fraction of the height of the Form.
This fraction is the value of the child's \*LXmNtopPosition\*O
resource divided by the value of the Form's \*LXmNfractionBase\*O.
For example, if the child's \*LXmNtopPosition\*O is 50, the Form's
\*LXmNfractionBase\*O is 100, and the Form's height is 200, the position
of the top side of the child is 100.
.LI "\*LXmNtopWidget\*O"
Specifies the widget or gadget to which the top
side of the child is attached.
This resource is used if \*LXmNtopAttachment\*O is
set to either \*LXmATTACH_WIDGET\*O or \*LXmATTACH_OPPOSITE_WIDGET\*O.
.PP
A string-to-widget resource converter is automatically installed for use
with this resource.
With this converter, the widget that is to be the value of the resource
must exist at the time the widget that has the resource is created.
.LE 
.SS "Inherited Resources"
Form inherits behavior and resources from the following
superclasses.  For a complete description of each resource, refer to the
man page for that superclass.
.P 
.TS 
center allbox;
cBp9 ssss
lBp8 lBp8 lBp8 lBp8 lBp8
lp8 lp8 lp8 lp8 lp8.
XmBulletinBoard Resource Set
Name	Class	Type	Default	Access
_
XmNallowOverlap	XmCAllowOverlap	Boolean	True	CSG
XmNautoUnmanage	XmCAutoUnmanage	Boolean	True	CG
XmNbuttonFontList	XmCButtonFontList	XmFontList	dynamic	CSG
XmNcancelButton	XmCWidget	Widget	NULL	SG
XmNdefaultButton	XmCWidget	Widget	NULL	SG
XmNdefaultPosition	XmCDefaultPosition	Boolean	True	CSG
XmNdialogStyle	XmCDialogStyle	unsigned char	dynamic	CSG
XmNdialogTitle	XmCDialogTitle	XmString	NULL	CSG
XmNfocusCallback	XmCCallback	XtCallbackList	NULL	C
XmNlabelFontList	XmCLabelFontList	XmFontList	dynamic	CSG
XmNmapCallback	XmCCallback	XtCallbackList	NULL	C
XmNmarginHeight	XmCMarginHeight	Dimension	0	CSG
XmNmarginWidth	XmCMarginWidth	Dimension	0 	CSG
XmNnoResize	XmCNoResize	Boolean	False	CSG
.wH .tH
XmNresizePolicy	XmCResizePolicy	unsigned char	XmRESIZE_ANY	CSG
XmNshadowType	XmCShadowType	unsigned char	XmSHADOW_OUT	CSG
XmNtextFontList	XmCTextFontList	XmFontList	dynamic	CSG
XmNtextTranslations	XmCTranslations	XtTranslations	NULL	C
XmNunmapCallback	XmCCallback	XtCallbackList	NULL	C
.TE
.P 
.TS 
center allbox;
cBp9 ssss
lBp8 lBp8 lBp8 lBp8 lBp8
lp8 lp8 lp8 lp8 lp8.
XmManager Resource Set
Name	Class	Type	Default	Access
_
XmNbottomShadowColor	XmCBottomShadowColor	Pixel	dynamic	CSG
XmNbottomShadowPixmap	XmCBottomShadowPixmap	Pixmap	XmUNSPECIFIED_PIXMAP	CSG
XmNforeground	XmCForeground	Pixel	dynamic	CSG
XmNhelpCallback	XmCCallback	XtCallbackList	NULL	C
XmNhighlightColor	XmCHighlightColor	Pixel	dynamic	CSG
XmNhighlightPixmap	XmCHighlightPixmap	Pixmap	dynamic	CSG
XmNinitialFocus	XmCInitialFocus	Widget	dynamic	CSG
XmNnavigationType	XmCNavigationType	XmNavigationType	XmTAB_GROUP	CSG
XmNshadowThickness	XmCShadowThickness	Dimension	dynamic	CSG
XmNstringDirection	XmCStringDirection	XmStringDirection	dynamic	CG
XmNtopShadowColor	XmCTopShadowColor	Pixel	dynamic	CSG
XmNtopShadowPixmap	XmCTopShadowPixmap	Pixmap	dynamic	CSG
XmNtraversalOn	XmCTraversalOn	Boolean	True	CSG
XmNunitType	XmCUnitType	unsigned char	dynamic	CSG
XmNuserData	XmCUserData	XtPointer	NULL	CSG
.TE
.P 
.TS 
center allbox;
cBp9 ssss
lBp8 lBp8 lBp8 lBp8 lBp8
lp8 lp8 lp8 lp8 lp8.
Composite Resource Set
Name	Class	Type	Default	Access
_
XmNchildren	XmCReadOnly	WidgetList	NULL	G
XmNinsertPosition	XmCInsertPosition	XtOrderProc	NULL	CSG
XmNnumChildren	XmCReadOnly	Cardinal	0	G
.TE
.P 
.wH .in 0 
.TS 
center allbox;
cBp9 ssss
lBp8 lBp8 lBp8 lBp8 lBp8
lp8 lp8 lp8 lp8 lp8.
Core Resource Set
Name	Class	Type	Default	Access
_
XmNaccelerators	XmCAccelerators	XtAccelerators	dynamic	N/A
XmNancestorSensitive	XmCSensitive	Boolean	dynamic	G
XmNbackground	XmCBackground	Pixel	dynamic	CSG
XmNbackgroundPixmap	XmCPixmap	Pixmap	XmUNSPECIFIED_PIXMAP	CSG
XmNborderColor	XmCBorderColor	Pixel	XtDefaultForeground	CSG
XmNborderPixmap	XmCPixmap	Pixmap	XmUNSPECIFIED_PIXMAP	CSG
XmNborderWidth	XmCBorderWidth	Dimension	0	CSG
XmNcolormap	XmCColormap	Colormap	dynamic	CG
XmNdepth	XmCDepth	int	dynamic	CG
XmNdestroyCallback	XmCCallback	XtCallbackList	NULL	C
XmNheight	XmCHeight	Dimension	dynamic	CSG
XmNinitialResourcesPersistent	XmCInitialResourcesPersistent	Boolean	True	C
XmNmappedWhenManaged	XmCMappedWhenManaged	Boolean	True	CSG
XmNscreen	XmCScreen	Screen *	dynamic	CG
.wH .tH
XmNsensitive	XmCSensitive	Boolean	True	CSG
XmNtranslations	XmCTranslations	XtTranslations	dynamic	CSG
XmNwidth	XmCWidth	Dimension	dynamic	CSG
XmNx	XmCPosition	Position	0	CSG
XmNy	XmCPosition	Position	0	CSG
.TE
.wH .in  
.SS "Translations"
XmForm inherits translations from XmBulletinBoard.
.SH RELATED INFORMATION
.na
\*LComposite(3X)\*O, \*LConstraint(3X)\*O, \*LCore(3X)\*O,
\*LXmBulletinBoard(3X)\*O,
\*LXmCreateForm\*O, \*LXmCreateFormDialog(3X)\*O, and
\*LXmManager(3X)\*O.
.ad

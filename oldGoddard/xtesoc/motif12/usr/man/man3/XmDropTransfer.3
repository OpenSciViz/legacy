...\" **
...\" **
...\" **  (c) Copyright 1991, 1992 by Open Software Foundation, Inc.
...\" **      All Rights Reserved.
...\" **
...\" **  (c) Copyright 1991 by Hewlett-Packard Company
...\" **
...\" **
.TH XmDropTransfer 3X "" "" "" ""
.SH NAME
\*LXmDropTransfer\*O\(emThe DropTransfer widget class
.iX "XmDropSite"
.iX "widget class" "DropTransfer"
.SH SYNOPSIS
.sS
.iS
\&#include <Xm/DragDrop.h>
.iE
.sE
.SH DESCRIPTION
DropTransfer provides a set of resources that identifies
the procedures and associated information required by the
toolkit in order to process and complete a drop transaction.
Clients should not explicitly create a DropTransfer widget.
Instead, a client initiates a transfer by calling
\*LXmDropTransferStart\*O, which initializes and returns a
DropTransfer widget.  If this function is called within an
\*LXmNdropProc\*O callback, the actual transfers are initiated
after the callback returns.  Even if no data needs to be transferred,
\*LXmDropTransferStart\*O needs to be called (typically with
no arguments, or just setting \*LXmNtransferStatus\*O) to finish
the drag and drop transaction.
.PP
The \*LXmNdropTransfers\*O resource specifies a transfer
list that describes the requested target types for the source
data.  A transfer list is an array of \*LXmDropTransferEntryRec\*O
structures, each of which identifies a target type.  The
transfer list is analogous to the MULTIPLE selections capability
defined in the \*EInter-Client Communication Conventions Manual\*O
(ICCCM). 
.PP
The DropTransfer resource, \*LXmNtransferProc\*O, specifies a
transfer procedure of type XtSelectionCallbackProc that
delivers the requested selection data.  This procedure operates
in conjunction with the underlying Xt selection capabilities and
is called for each target in the transfer list.  Additional target
types can be requested after a transfer is initiated by calling
the \*LXmDropTransferAdd\*O function.
.SS "Structures"
An \*LXmDropTransferEntry\*O is a pointer to the following structure of
type \*LXmDropTransferEntryRec\*O, which identifies a selection
target associated with a given drop transaction:  
.sS
.iS
.ta .25i 1.25i
typedef struct
{
	XtPointer	\*Vclient_data\*L;
	Atom	\*Vtarget\*L;
} XmDropTransferEntryRec, *XmDropTransferEntry;
.iE
.sE
.PP
.VL 1i
.LI "\*Vclient_data\*O
Specifies any additional information required 
by this selection target
.LI "\*Vtarget\*O
Specifies a selection target associated with the drop
operation
.LE
.SS "Classes"
DropTransfer inherits behavior and a resource from \*LObject\*O.
.PP
The class pointer is \*LxmDropTransferObjectClass\*O.
.PP
The class name is \*LXmDropTransfer\*O.
.SS "New Resources"
The following table defines a set of widget resources used by the
programmer to specify data.  The programmer can also set the
resource values for the inherited classes to set attributes for
this widget.  To reference a resource by name or by class in
a .Xdefaults file, remove the \*LXmN\*O or \*LXmC\*O prefix and use
the remaining letters.  To specify one of the defined values for a
resource in a .Xdefaults file, remove the \*LXm\*O prefix and use
the remaining letters (in either lowercase or uppercase, but include
any underscores between words).  The codes in the access column
indicate if the given resource can be set at creation time (\*LC\*O),
set by using \*LXtSetValues\*O (\*LS\*O), retrieved by using
\*LXtGetValues\*O (\*LG\*O), or is not applicable (\*LN/A\*O).
.PP
.TS
 center allbox;
cBp9 ssss
lBp8 lBp8 lBp8 lBp8 lBp8
lp8 lp8 lp8 lp8 lp8.
XmDropTransfer Resource Set
Name	Class	Type	Default	Access
_
XmNdropTransfers	XmCDropTransfers	XmDropTransferEntryRec *	NULL	CG
XmNincremental	XmCIncremental	Boolean	False	CSG
XmNnumDropTransfers	XmCNumDropTransfers	Cardinal	0	CSG
XmNtransferProc	XmCTransferProc	XtSelectionCallbackProc	NULL	CSG
XmNtransferStatus	XmCTransferStatus	unsigned char	XmTRANSFER_SUCCESS	CSG
.TE
.PP
.VL
.LI "\*LXmNdropTransfers\*O"
Specifies the address of an array of drop transfer entry records.  The
drop transfer is complete when all the entries in the list have been
processed.
.LI "\*LXmNincremental\*O"
Specifies a Boolean value that indicates whether the transfer on the
receiver side uses the Xt incremental selection transfer mechanism
described in \*EX Toolkit Intrinsics\(emC Language Interface\*O.
If the value is True, the receiver uses incremental transfer; if the
value is False, the receiver uses atomic transfer.
.LI "\*LXmNnumDropTransfers\*O"
Specifies the number of entries in \*LXmNdropTransfers\*O.  If
this resource is set to 0 at any time, the transfer is considered
complete.  The value of \*LXmNtransferStatus\*O determines the
completion handshaking process.
.LI "\*LXmNtransferProc\*O"
Specifies a procedure of type \*LXtSelectionCallbackProc\*O that
delivers the requested selection values.
The \*Vwidget\*O argument passed to this procedure is the DropTransfer
widget.
The selection atom passed is _MOTIF_DROP.
For additional information on selection callback procedures, see \*EX
Toolkit Intrinsics\(emC Language Interface\*O.
.LI "\*LXmNtransferStatus\*O"
Specifies the current status of the drop transfer.  The client
updates this value when the transfer ends and communicates
the value to the initiator.  The possible values are
.VL 2i
.LI "\*LXmTRANSFER_SUCCESS\*O
The transfer succeeded.
.LI "\*LXmTRANSFER_FAILURE\*O
The transfer failed.
.LE
.LE
.SS "Inherited Resources"
DropTransfer inherits behavior and a resource from \*LObject\*O.
For a complete description of this resource, refer
to the \*LObject\*O reference page.
.PP
.TS 
center allbox;
cBp9 ssss
lBp8 lBp8 lBp8 lBp8 lBp8
lp8 lp8 lp8 lp8 lp8.
Object Resource Set
Name	Class	Type	Default	Access
_
XmNdestroyCallback	XmCCallback	XtCallbackList	NULL	C
.TE
.PP
.SH "RELATED INFORMATION"
.na
\*LObject(3X)\*O,
\*LXmDisplay(3X)\*O,
\*LXmDragContext(3X)\*O,
\*LXmDragIcon(3X)\*O,
\*LXmDropSite(3X)\*O,
\*LXmDropTransferAdd(3X)\*O, and
\*LXmDropTransferStart(3X)\*O.
.ad

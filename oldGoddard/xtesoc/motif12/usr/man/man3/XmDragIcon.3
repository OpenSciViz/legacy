...\" **
...\" **
...\" **  (c) Copyright 1991, 1992 by Open Software Foundation, Inc.
...\" **      All Rights Reserved.
...\" **
...\" **  (c) Copyright 1991 by Hewlett-Packard Company
...\" **
...\" **
.TH XmDragIcon 3X "" "" "" ""
.SH NAME
\*LXmDragIcon\*O\(emThe DragIcon widget class
.iX "XmDragIcon"
.iX "widget class" "DragIcon"
.SH SYNOPSIS
.sS
.iS
\&#include <Xm/DragDrop.h>
.iE
.sE
.SH DESCRIPTION
A DragIcon is a component of the visual used to represent the source
data in a drag and drop transaction.  During a drag operation, a real
or simulated X cursor provides drag-over visuals consisting of a
static portion that represents the object being dragged, and dynamic
cues that provide visual feedback during the drag operation.  The
visual is attained by blending together various \*LXmDragIcons\*O
specified in the \*LXmDragContext\*O associated with the drag
operation.
.PP
The static portion of the drag-over visual is the graphic
representation that identifies the drag source.  For example,
when a user drags several items within a list, a DragIcon depicting a
list might be supplied as the visual.  The \*LXmDragContext\*O
resources, \*LXmNsourceCursorIcon\*O or \*LXmNsourcePixmapIcon\*O,
specify a DragIcon to use for the static portion of the visual.
.PP
A drag-over visual incorporates dynamic cues in order to provide
visual feedback in response to the user's actions.  For instance,
the drag-over visual might use different indicators to identify
the type of operation (copy, link, or move) being performed.  Dynamic
cues could also alert the user that a drop site is valid or invalid
as the pointer traverses the drop site.  The \*LXmNoperationCursorIcon\*O
and \*LXmNstateCursorIcon\*O resources of \*LXmDragContext\*O specify
DragIcons for dynamic cues.
.PP
A drag-over visual typically consists of a source, operation and
state DragIcon.  The \*LXmNblendModel\*O resource of \*LXmDragContext\*O
offers several options that determine which icons are blended
to produce the drag-over visual.  DragIcon resources control
the relative position of the operation and state icons (if used).
If a particular DragIcon is not specified, the toolkit uses the 
\*LXmScreen\*O default DragIcons.
.PP
An application initializes a DragIcon with the function 
\*LXmCreateDragIcon\*O or through entries in the resource
database.  If a pixmap and its mask (optional) are specified
in the resource database, the toolkit converts the 
values in the X11 Bitmap file format and assigns values to
the corresponding resources.
.SS "Classes"
DragIcon inherits behavior and a resource from \*LObject\*O.
.PP
The class pointer is \*LxmDragIconObjectClass\*O.
.PP
The class name is \*LXmDragIcon\*O.
.SS "New Resources"
The following table defines a set of widget resources used by the
programmer to specify data.  The programmer can also set the
resource values for the inherited classes to set attributes for
this widget.  To reference a resource by name or by class in
a .Xdefaults file, remove the \*LXmN\*O or \*LXmC\*O prefix and use
the remaining letters.  To specify one of the defined values for a
resource in a .Xdefaults file, remove the \*LXm\*O prefix and use
the remaining letters (in either lowercase or uppercase, but include
any underscores between words).  The codes in the access column 
indicate if the given resource can be set at creation time (\*LC\*O),
set by using \*LXtSetValues\*O (\*LS\*O), retrieved by using
\*LXtGetValues\*O (\*LG\*O), or is not applicable (\*LN/A\*O).
.PP
.TS
center allbox;
cBp9 ssss
lBp8 lBp8 lBp8 lBp8 lBp8
lp8 lp8 lp8 lp8 lp8.
XmDragIcon Resource Set
Name	Class	Type	Default	Access
_
XmNattachment	XmCAttachment	unsigned char	XmATTACH_NORTH_WEST	CSG
XmNdepth	XmCDepth	int	1	CSG
XmNheight	XmCHeight	Dimension	0	CSG
XmNhotX	XmCHot	Position	0	CSG
XmNhotY	XmCHot	Position	0	CSG	 
XmNmask	XmCPixmap	Pixmap	XmUNSPECIFIED_PIXMAP	CSG
XmNoffsetX	XmCOffset	Position	0	CSG
XmNoffsetY	XmCOffset	Position	0	CSG
XmNpixmap	XmCPixmap	Pixmap	XmUNSPECIFIED_PIXMAP	CSG
XmNwidth	XmCWidth	Dimension	0	CSG
.TE
.PP
.VL 1.25i
.LI "\*LXmNattachment\*O"
Specifies a relative location on the source icon for the attachment of
the state or operation icon.  The origin of the state and operation
icons is aligned with the specified compass point on the source icon.
The \*LXmNoffsetX\*O and \*LXmNoffsetY\*O resources can be used to further
refine the icon positions.  The possible values are
.VL
.LI "\*LXmATTACH_NORTH_WEST\*O"
Attaches the origin of the state or operation icon to the northwest
point on the source icon.
.LI "\*LXmATTACH_NORTH\*O"
Attaches the origin of the state or operation icon to the north
point on the source icon.
.LI "\*LXmATTACH_NORTH_EAST\*O"
Attaches the origin of the state or operation icon to the northeast
point on the source icon.
.LI "\*LXmATTACH_EAST\*O"
Attaches the origin of the state or operation icon to the east
point on the source icon.
.LI "\*LXmATTACH_SOUTH_EAST\*O"
Attaches the origin of the state or operation icon to the southeast
point on the source icon.
.LI "\*LXmATTACH_SOUTH\*O"
Attaches the origin of the state or operation icon to the south
point on the source icon.
.LI "\*LXmATTACH_SOUTH_WEST\*O"
Attaches the origin of the state or operation icon to the southwest
point on the source icon.
.LI "\*LXmATTACH_WEST\*O"
Attaches the origin of the state or operation icon to the west
point on the source icon.
.LI "\*LXmATTACH_CENTER\*O"
Attaches the origin of the state or operation icon to the
center of the source icon.  The \*LXmNoffsetX\*O and \*LXmNoffsetY\*O
resources may be used to center the attached icon.
.LI "\*LXmATTACH_HOT\*O"
Attaches the hotspot coordinates of a state or operation DragIcon to
an \*Vx,y\*O position on the source icon.  The \*Vx,y\*O coordinate
is taken from the event passed to the \*LXmDragStart\*O function,
and made relative to the widget passed as an argument to the same
function.
.LE
.LI "\*LXmNdepth\*O"
Specifies the depth of the pixmap.
.LI "\*LXmNheight\*O"
Specifies the height of the pixmap.
.LI "\*LXmNhotX\*O"
Specifies the x-coordinate of the hotspot of a cursor DragIcon
in relation to the origin of the pixmap bounding box.
.LI "\*LXmNhotY\*O"
Specifies the y-coordinate of the hotspot of a cursor DragIcon
in relation to the origin of the pixmap bounding box.
.LI "\*LXmNmask\*O"
Specifies a pixmap of depth one to use as the DragIcon mask
pixmap.
.LI "\*LXmNoffsetX\*O"
Specifies a horizontal offset (in pixels) of the origin of the state
or operation icon relative to the attachment point on the source icon.
A positive offset value moves the origin to the right; a negative value
moves the origin to the left.
.LI "\*LXmNoffsetY\*O"
Specifies a vertical offset (in pixels) of the origin of the state or
operation icon relative to the attachment point on the source icon.  A
positive offset value moves the origin down; a negative value moves the
origin up.
.LI "\*LXmNpixmap\*O"
Specifies a pixmap to use as the DragIcon pixmap.
.LI "\*LXmNwidth\*O"
Specifies the width of the pixmap.
.LE
.SS "Inherited Resources"
DragIcon inherits behavior and a resource from \*LObject\*O.
For a complete description of this resource, refer to the
\*LObject\*O man page.
.PP
.TS 
center allbox;
cBp9 ssss
lBp8 lBp8 lBp8 lBp8 lBp8
lp8 lp8 lp8 lp8 lp8.
Object Resource Set
Name	Class	Type	Default	Access
_
XmNdestroyCallback	XmCCallback	XtCallbackList	NULL	C
.TE
.PP
.SH "RELATED INFORMATION"
.na
\*LObject(3X)\*O,
\*LXmCreateDragIcon(3X)\*O,
\*LXmDisplay(3X)\*O,
\*LXmDragContext(3X)\*O,
\*LXmDropSite(3X)\*O,
\*LXmDropTransfer(3X)\*O, and
\*LXmScreen(3X)\*O.
.ad

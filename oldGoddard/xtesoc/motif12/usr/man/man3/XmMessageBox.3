...\" **
...\" **
...\" **  (c) Copyright 1989, 1990, 1992 by Open Software Foundation, Inc.
...\" **      All Rights Reserved.
...\" **
...\" **  (c) Copyright 1987, 1988, 1989, by Hewlett-Packard Company
...\" **
...\" **  (c) Copyright 1987, 1988 by Digital Equipment Corporation,
...\" **      Maynard, MA.  All Rights Reserved.
...\" **
...\" **
.TH XmMessageBox 3X "" "" "" ""
.SH NAME
\*LXmMessageBox\*O\(emThe MessageBox widget class
.iX "XmMessageBox"
.iX "widget class" "MessageBox"
.SH SYNOPSIS
.sS
.iS
\&#include <Xm/MessageB.h>
.iE
.sE
.SH DESCRIPTION
MessageBox is a dialog class used for creating simple message dialogs.
Convenience dialogs based on MessageBox are provided for several common
interaction tasks, which include giving information, asking questions, and
reporting errors.
.PP 
A MessageBox dialog is typically transient in nature, displayed for the
duration of a single interaction.
MessageBox is a subclass of XmBulletinBoard and
depends on it for much of its general dialog behavior.
.PP
The default value for \*LXmNinitialFocus\*O 
is the value of \*LXmNdefaultButton\*O.
.PP 
A typical MessageBox contains a message symbol, a message, and up to
three standard default PushButtons:  \*LOK, Cancel\*O, and \*LHelp\*O.
It is laid out with the symbol and message on top and the
PushButtons on the bottom.  The help button is positioned to the side
of the other push buttons.
You can localize the default symbols and button labels for MessageBox
convenience dialogs.
.PP
The user can specify resources in a resource file for the gadgets
created automatically that contain the MessageBox symbol pixmap
and separator.  The gadget names are "Symbol" and "Separator".
.PP
A MessageBox can also be customized by creating and managing new
children that are added to the MessageBox children created
automatically by the convenience dialogs.
In the case of
TemplateDialog, only the separator child is created by default.
If the callback, string, or pixmap symbol resources are specified,
the appropriate child will be created.
.PP
Additional children are laid out in the following manner:
.ML
.LI
The first MenuBar child is placed at the top of the window.
.LI
All \*LXmPushButton\*O
widgets or gadgets, and their subclasses are
placed after the \*LOK\*O button in the order of their creation.
.LI
A child that is not in the above categories is placed above
the row of buttons.  If a message label exists, the child is placed below
the label.  If a message pixmap exists, but a message label is absent, the
child is placed on the same row as the pixmap.  The child behaves as a
work area and grows or shrinks to fill the space above the
row of buttons.  The layout of multiple work area children is
undefined.
.LE
.PP 
.ne 15
At initialization, MessageBox looks for the following bitmap files:
.wH .rS 
.ML
.LI
xm_error
.LI
xm_information
.LI
xm_question
.LI
xm_working
.LI
xm_warning
.LE 
.wH .rE
.PP 
See \*LXmGetPixmap(3X)\*O for a list of the paths that are searched for
these files.
.SS "Classes"
MessageBox inherits behavior and resources from \*LCore\*O,
\*LComposite\*O, \*LConstraint\*O,
\*LXmManager\*O, and \*LXmBulletinBoard\*O.
.PP 
The class pointer is \*LxmMessageBoxWidgetClass\*O.
.PP 
The class name is \*LXmMessageBox\*O.
.SS "New Resources"
The following table defines a set of widget resources used by the programmer
to specify data.  The programmer can also set the resource values for the
inherited classes to set attributes for this widget.  To reference a
resource by name or by class in a .Xdefaults file, remove the \*LXmN\*O or
\*LXmC\*O prefix and use the remaining letters.  To specify one of the defined
values for a resource in a .Xdefaults file, remove the \*LXm\*O prefix and use
the remaining letters (in either lowercase or uppercase, but include any
underscores between words).
The codes in the access column indicate if the given resource can be
set at creation time (C),
set by using \*LXtSetValues\*O (S),
retrieved by using \*LXtGetValues\*O (G), or is not applicable (N/A).
.P 
.TS 
center allbox;
cBp9 ssss
lBp8 lBp8 lBp8 lBp8 lBp8
lp8 lp8 lp8 lp8 lp8.
XmMessageBox Resource Set
Name	Class	Type	Default	Access
_
XmNcancelCallback	XmCCallback	XtCallbackList	NULL	C
XmNcancelLabelString	XmCCancelLabelString	XmString	dynamic	CSG
XmNdefaultButtonType	XmCDefaultButtonType	unsigned char	XmDIALOG_OK_BUTTON	CSG
XmNdialogType	XmCDialogType	unsigned char	XmDIALOG_MESSAGE	CSG
XmNhelpLabelString	XmCHelpLabelString	XmString	dynamic	CSG
XmNmessageAlignment	XmCAlignment	unsigned char	XmALIGNMENT_BEGINNING	CSG
XmNmessageString	XmCMessageString	XmString	""	CSG
XmNminimizeButtons	XmCMinimizeButtons	Boolean	False	CSG
XmNokCallback	XmCCallback	XtCallbackList	NULL	C
XmNokLabelString	XmCOkLabelString	XmString	dynamic	CSG
XmNsymbolPixmap	XmCPixmap	Pixmap	dynamic	CSG
.TE
.nL
.ne 20
.VL  
.LI "\*LXmNcancelCallback\*O"
Specifies the list of callbacks that is called when
the user clicks on the cancel button.
The reason sent by the callback is \*LXmCR_CANCEL\*O.
.LI "\*LXmNcancelLabelString\*O"
Specifies the string label for the cancel button.
The default for this resource depends on the locale.
In the C locale the default is "Cancel".
.LI "\*LXmNdefaultButtonType\*O"
Specifies the default PushButton.
A value of \*LXmDIALOG_NONE\*O means that there should be no default
PushButton.
The following are valid types:
.wH .rS 
.ML
.LI
\*LXmDIALOG_CANCEL_BUTTON\*O
.LI
\*LXmDIALOG_OK_BUTTON\*O
.LI
\*LXmDIALOG_HELP_BUTTON\*O
.LI
\*LXmDIALOG_NONE\*O
.LE
.wH .rE
.LI "\*LXmNdialogType\*O"
Specifies the type of MessageBox dialog, which determines
the default message symbol.
The following are the possible values for this resource:
.wH .rS 
.ML
.LI
\*LXmDIALOG_ERROR\*O\(emindicates an ErrorDialog
.LI
\*LXmDIALOG_INFORMATION\*O\(emindicates an InformationDialog
.LI
\*LXmDIALOG_MESSAGE\*O\(emindicates a MessageDialog.
This is the default MessageBox dialog type.
It does not have an associated message symbol.
.LI
\*LXmDIALOG_QUESTION\*O\(emindicates a QuestionDialog
.LI
\*LXmDIALOG_TEMPLATE\*O\(emindicates a TemplateDialog.
The TemplateDialog contains only a separator child.  It does not
have an associated message symbol.
.LI
\*LXmDIALOG_WARNING\*O\(emindicates a WarningDialog
.LI
\*LXmDIALOG_WORKING\*O\(emindicates a WorkingDialog
.LE
.wH .rE
.PP
If this resource is changed via \*LXtSetValues\*O, the symbol bitmap is
modified to the new \*LXmNdialogType\*O bitmap unless
\*LXmNsymbolPixmap\*O is also being set in the call to
\*LXtSetValues\*O.
If the dialog type does not have an associated message symbol, then no
bitmap will be displayed.
.nL
.LI "\*LXmNhelpLabelString\*O"
Specifies the string label for the help button.
The default for this resource depends on the locale.
In the C locale the default is "Help".
.nL
.ne 12
.LI "\*LXmNmessageAlignment\*O"
Controls the alignment of the message Label.
Possible values include the following:
.wH .rS 
.ML
.LI
\*LXmALIGNMENT_BEGINNING\*O\(emthe default
.LI
\*LXmALIGNMENT_CENTER\*O
.LI
\*LXmALIGNMENT_END\*O
.LE 
.wH .rE
.LI "\*LXmNmessageString\*O"
Specifies the string to be used as the message.
.LI "\*LXmNminimizeButtons\*O"
Sets the buttons to the width of the widest button and height of the
tallest button if False.  If True, button width and height are
set to the preferred size of each button.
.LI "\*LXmNokCallback\*O"
Specifies the list of callbacks that is called when
the user clicks on the OK button.
The reason sent by the callback is \*LXmCR_OK\*O.
.LI "\*LXmNokLabelString\*O"
Specifies the string label for the OK button.
The default for this resource depends on the locale.
In the C locale the default is "OK".
.LI "\*LXmNsymbolPixmap\*O"
Specifies the pixmap label to be used as the message symbol.
.LE 
.SS "Inherited Resources"
MessageBox inherits behavior and resources from the following
superclasses.  For a complete description of each resource, refer to the
man page for that superclass.
.P 
.TS 
center allbox;
cBp9 ssss
lBp8 lBp8 lBp8 lBp8 lBp8
lp8 lp8 lp8 lp8 lp8.
XmBulletinBoard Resource Set
Name	Class	Type	Default	Access
_
XmNallowOverlap	XmCAllowOverlap	Boolean	True	CSG
XmNautoUnmanage	XmCAutoUnmanage	Boolean	True	CG
XmNbuttonFontList	XmCButtonFontList	XmFontList	dynamic	CSG
XmNcancelButton	XmCWidget	Widget	Cancel button	SG
XmNdefaultButton	XmCWidget	Widget	dynamic	SG
XmNdefaultPosition	XmCDefaultPosition	Boolean	True	CSG
XmNdialogStyle	XmCDialogStyle	unsigned char	dynamic	CSG
XmNdialogTitle	XmCDialogTitle	XmString	NULL	CSG
XmNfocusCallback	XmCCallback	XtCallbackList	NULL	C
XmNlabelFontList	XmCLabelFontList	XmFontList	dynamic	CSG
XmNmapCallback	XmCCallback	XtCallbackList	NULL	C
XmNmarginHeight	XmCMarginHeight	Dimension	10	CSG
XmNmarginWidth	XmCMarginWidth	Dimension	10 	CSG
XmNnoResize	XmCNoResize	Boolean	False	CSG
.wH .tH
XmNresizePolicy	XmCResizePolicy	unsigned char	XmRESIZE_ANY	CSG
XmNshadowType	XmCShadowType	unsigned char	XmSHADOW_OUT	CSG
XmNtextFontList	XmCTextFontList	XmFontList	dynamic	CSG
XmNtextTranslations	XmCTranslations	XtTranslations	NULL	C
XmNunmapCallback	XmCCallback	XtCallbackList	NULL	C
.TE
.P 
.TS 
center allbox;
cBp9 ssss
lBp8 lBp8 lBp8 lBp8 lBp8
lp8 lp8 lp8 lp8 lp8.
XmManager Resource Set
Name	Class	Type	Default	Access
_
XmNbottomShadowColor	XmCBottomShadowColor	Pixel	dynamic	CSG
XmNbottomShadowPixmap	XmCBottomShadowPixmap	Pixmap	XmUNSPECIFIED_PIXMAP	CSG
XmNforeground	XmCForeground	Pixel	dynamic	CSG
XmNhelpCallback	XmCCallback	XtCallbackList	NULL	C
XmNhighlightColor	XmCHighlightColor	Pixel	dynamic	CSG
XmNhighlightPixmap	XmCHighlightPixmap	Pixmap	dynamic	CSG
XmNinitialFocus	XmCInitialFocus	Widget	dynamic	CSG
XmNnavigationType	XmCNavigationType	XmNavigationType	XmTAB_GROUP	CSG
XmNshadowThickness	XmCShadowThickness	Dimension	dynamic	CSG
XmNstringDirection	XmCStringDirection	XmStringDirection	dynamic	CG
XmNtopShadowColor	XmCTopShadowColor	Pixel	dynamic	CSG
XmNtopShadowPixmap	XmCTopShadowPixmap	Pixmap	dynamic	CSG
XmNtraversalOn	XmCTraversalOn	Boolean	True	CSG
XmNunitType	XmCUnitType	unsigned char	dynamic	CSG
XmNuserData	XmCUserData	XtPointer	NULL	CSG
.TE
.P 
.TS 
center allbox;
cBp9 ssss
lBp8 lBp8 lBp8 lBp8 lBp8
lp8 lp8 lp8 lp8 lp8.
Composite Resource Set
Name	Class	Type	Default	Access
_
XmNchildren	XmCReadOnly	WidgetList	NULL	G
XmNinsertPosition	XmCInsertPosition	XtOrderProc	NULL	CSG
XmNnumChildren	XmCReadOnly	Cardinal	0	G
.TE
.P 
.wH .in 0 
.TS 
center allbox;
cBp9 ssss
lBp8 lBp8 lBp8 lBp8 lBp8
lp8 lp8 lp8 lp8 lp8.
Core Resource Set
Name	Class	Type	Default	Access
_
XmNaccelerators	XmCAccelerators	XtAccelerators	dynamic	N/A
XmNancestorSensitive	XmCSensitive	Boolean	dynamic	G
XmNbackground	XmCBackground	Pixel	dynamic	CSG
XmNbackgroundPixmap	XmCPixmap	Pixmap	XmUNSPECIFIED_PIXMAP	CSG
XmNborderColor	XmCBorderColor	Pixel	XtDefaultForeground	CSG
XmNborderPixmap	XmCPixmap	Pixmap	XmUNSPECIFIED_PIXMAP	CSG
XmNborderWidth	XmCBorderWidth	Dimension	0	CSG
XmNcolormap	XmCColormap	Colormap	dynamic	CG
XmNdepth	XmCDepth	int	dynamic	CG
XmNdestroyCallback	XmCCallback	XtCallbackList	NULL	C
XmNheight	XmCHeight	Dimension	dynamic	CSG
XmNinitialResourcesPersistent	XmCInitialResourcesPersistent	Boolean	True	C
XmNmappedWhenManaged	XmCMappedWhenManaged	Boolean	True	CSG
XmNscreen	XmCScreen	Screen *	dynamic	CG
.wH .tH
XmNsensitive	XmCSensitive	Boolean	True	CSG
XmNtranslations	XmCTranslations	XtTranslations	dynamic	CSG
XmNwidth	XmCWidth	Dimension	dynamic	CSG
XmNx	XmCPosition	Position	0	CSG
XmNy	XmCPosition	Position	0	CSG
.TE
.wH .in  
.SS "Callback Information"
A pointer to the following structure is passed to each callback:
.sS
.iS
.ta .25i 1.50i
typedef struct
{
	int	\*Vreason\*L;
	\*LXEvent	\*V* event\*L;
} XmAnyCallbackStruct;
.iE
.sE
.wH .fi
.VL .75i 
.LI "\*Vreason\*O"
Indicates why the callback was invoked
.LI "\*Vevent\*O"
Points to the \*LXEvent\*O that triggered the callback
.LE 
.SS "Translations"
.ne 20
XmMessageBox includes the translations from XmManager.
.SS "Additional Behavior"
The XmMessageBox widget has the additional behavior described below:
.VL  
.LI "\*LMAny\ KCancel\*O:"
Calls the activate callbacks for the cancel button if it is sensitive.
.LI "\*LKActivate\*O:"
Calls the activate callbacks for the button with the keyboard focus.
If no button has the keyboard focus, calls the activate callbacks
for the default button if it is sensitive.
.LI "\*L<Ok\ Button\ Activated>\*O:"
Calls the callbacks for \*LXmNokCallback\*O.
.LI "\*L<Cancel\ Button\ Activated>\*O:"
Calls the callbacks for \*LXmNcancelCallback\*O.
.LI "\*L<Help\ Button\ Activated>\*O:"
Calls the callbacks for \*LXmNhelpCallback\*O.
.LI "\*L<FocusIn>\*O:"
Calls the callbacks for \*LXmNfocusCallback\*O.
.LI "\*L<Map>\*O:"
Calls the callbacks for \*LXmNmapCallback\*O if the parent is a
DialogShell.
.LI "\*L<Unmap>\*O:"
Calls the callbacks for \*LXmNunmapCallback\*O if the parent is a
DialogShell.
.LE
.SS "Virtual Bindings"
The bindings for virtual keys are vendor specific.
For information about bindings for virtual buttons and keys, see \*LVirtualBindings(3X)\*O.
.nL
.ne 20
.SH RELATED INFORMATION
.na
\*LComposite(3X)\*O,
\*LConstraint(3X)\*O,
\*LCore(3X)\*O,
\*LXmBulletinBoard(3X)\*O,
\*LXmCreateErrorDialog(3X)\*O,
\*LXmCreateInformationDialog(3X)\*O,
\*LXmCreateMessageBox(3X)\*O,
\*LXmCreateMessageDialog(3X)\*O,
\*LXmCreateQuestionDialog(3X)\*O,
\*LXmCreateTemplateDialog(3X)\*O,
\*LXmCreateWarningDialog(3X)\*O,
\*LXmCreateWorkingDialog(3X)\*O,
\*LXmManager(3X)\*O, and
\*LXmMessageBoxGetChild(3X)\*O.
.ad

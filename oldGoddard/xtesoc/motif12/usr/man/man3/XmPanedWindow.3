...\" **
...\" **
...\" **  (c) Copyright 1989, 1990, 1992 by Open Software Foundation, Inc.
...\" **      All Rights Reserved.
...\" **
...\" **  (c) Copyright 1987, 1988, 1989, by Hewlett-Packard Company
...\" **
...\" **  (c) Copyright 1988 by Massachusetts Institute of Technology
...\" **
...\" **  (c) Copyright 1987, 1988 by Digital Equipment Corporation,
...\" **      Maynard, MA.  All Rights Reserved.
...\" **
...\" **
.TH XmPanedWindow 3X "" "" "" ""
.SH NAME
\*LXmPanedWindow\*O\(emThe PanedWindow widget class
.iX "XmPanedWindow"
.iX "widget class" "PanedWindow"
.SH SYNOPSIS
.sS
.iS
\&#include <Xm/PanedW.h>
.iE
.sE
.SH DESCRIPTION
PanedWindow is a composite widget that lays out children in a
vertically tiled format.  Children appear in top-to-bottom fashion, with
the first child inserted appearing at the top of the PanedWindow and the
last child inserted appearing at the bottom.  The
PanedWindow grows to match the width of its widest child and all
other children are forced to this width. The height of the PanedWindow
is equal to the sum of the heights of all its children, the spacing
between them, and the size of the top and bottom margins.
.PP 
The user can also adjust the size of the panes.  To
facilitate this adjustment, a pane control sash is created for most
children.  The sash appears as a square box positioned on the bottom of
the pane that it controls.  The user can adjust the size of
a pane by using the mouse or keyboard.
.PP 
The PanedWindow is also a constraint widget, which means that it
creates and manages a set of constraints for each child.  You can
specify a minimum and maximum size for each pane.  The PanedWindow
does not allow a pane to be resized below its minimum size or beyond its
maximum size.  Also, when the minimum size of a pane is equal to its maximum
size, no control sash is presented for that pane or
for the lowest pane.
.PP 
The default \*LXmNinsertPosition\*O procedure for PanedWindow causes
sashes to be inserted at the end of the list of children and causes
non-sash widgets to be inserted after other non-sash children but before
any sashes.
.PP
All panes and sashes in a PanedWindow must be tab groups.  When a pane is
inserted as a child of the PanedWindow, if the pane's
\*LXmNnavigationType\*O is not \*LXmEXCLUSIVE_TAB_GROUP\*O, PanedWindow
sets it to \*LXmSTICKY_TAB_GROUP\*O.
.SS "Classes"
PanedWindow inherits behavior and resources from the
\*LCore\*O, \*LComposite\*O, \*LConstraint\*O, and \*LXmManager\*O classes.
.PP 
The class pointer is \*LxmPanedWindowWidgetClass\*O.
.PP 
The class name is \*LXmPanedWindow\*O.
.SS "New Resources"
The following table defines a set of widget resources used by the programmer
to specify data.  The programmer can also set the resource values for the
inherited classes to set attributes for this widget.  To reference a
resource by name or by class in a .Xdefaults file, remove the \*LXmN\*O or
\*LXmC\*O prefix and use the remaining letters.  To specify one of the defined
values for a resource in a .Xdefaults file, remove the \*LXm\*O prefix and use
the remaining letters (in either lowercase or uppercase, but include any
underscores between words).
The codes in the access column indicate if the given resource can be
set at creation time (C),
set by using \*LXtSetValues\*O (S),
retrieved by using \*LXtGetValues\*O (G), or is not applicable (N/A).
.P 
.TS 
center allbox;
cBp9 ssss
lBp8 lBp8 lBp8 lBp8 lBp8
lp8 lp8 lp8 lp8 lp8.
XmPanedWindow Resource Set
Name	Class	Type	Default	Access
_
XmNmarginHeight	XmCMarginHeight	Dimension	3	CSG
XmNmarginWidth	XmCMarginWidth	Dimension	3	CSG
XmNrefigureMode	XmCBoolean	Boolean	True	CSG
XmNsashHeight	 XmCSashHeight	Dimension	10	CSG
XmNsashIndent	XmCSashIndent	Position	-10	CSG
XmNsashShadowThickness	XmCShadowThickness	Dimension	dynamic	CSG
XmNsashWidth	 XmCSashWidth	Dimension	10	CSG
XmNseparatorOn	XmCSeparatorOn	Boolean	True	CSG
XmNspacing	XmCSpacing	Dimension	8	CSG
.TE
.VL  
.LI "\*LXmNmarginHeight\*O"
Specifies the distance between the top and bottom edges of the PanedWindow
and its children.
.LI "\*LXmNmarginWidth\*O"
Specifies the distance between the left and right edges of the PanedWindow
and its children.
.LI "\*LXmNrefigureMode\*O"
Determines whether the panes' positions are recomputed and repositioned
when programmatic changes are being made to the PanedWindow.
Setting this resource to True resets the children to their appropriate
positions.
.LI "\*LXmNsashHeight\*O"
Specifies the height of the sash.
.LI "\*LXmNsashIndent\*O"
Specifies the horizontal placement of the sash along each pane.  A positive
value causes the sash to be offset from the near (left) side of the PanedWindow,
and a negative value causes the sash to be offset from the far (right)
side of the PanedWindow.  If the offset is greater than the width of the
PanedWindow minus the width of the sash, the sash is placed flush
against the near side of the PanedWindow.
.PP
Whether the placement actually corresponds to the left or right side of
the PanedWindow may depend on the value of the \*LXmNstringDirection\*O
resource.
.LI "\*LXmNsashShadowThickness\*O"
Specifies the thickness of the shadows of the sashes.
.LI "\*LXmNsashWidth\*O"
Specifies the width of the sash.
.LI "\*LXmNseparatorOn\*O"
Determines whether a separator is created between each of the panes.
Setting this resource to True creates a Separator at the
midpoint between each of the panes.
.LI "\*LXmNspacing\*O"
Specifies the distance between each child pane.
.ne 1.75i
.P 
.TS 
center allbox;
cBp9 ssss
lBp8 lBp8 lBp8 lBp8 lBp8
lp8 lp8 lp8 lp8 lp8.
XmPanedWindow Constraint Resource Set
Name	Class	Type	Default	Access
_
XmNallowResize	XmCBoolean	Boolean	False	CSG
XmNpaneMaximum	XmCPaneMaximum	Dimension	1000	CSG
XmNpaneMinimum	XmCPaneMinimum	Dimension	1	CSG
XmNpositionIndex	XmCPositionIndex	short	XmLAST_POSITION	CSG
XmNskipAdjust	XmCBoolean	Boolean	False	CSG
.TE
.LI "\*LXmNallowResize\*O"
Allows an application to specify whether the PanedWindow
should allow a pane to request to be resized.  This flag has an
effect only after the PanedWindow and its children have been realized.
If this flag is set to True, the PanedWindow tries to honor requests
to alter the height of the pane. If False, it always denies pane
requests to resize.
.LI "\*LXmNpaneMaximum\*O"
Allows an application to specify the maximum size to which a pane
may be resized.  This value must be greater than the specified minimum.
.LI "\*LXmNpaneMinimum\*O"
Allows an application to specify the minimum size to which a pane
may be resized.  This value must be greater than 0.
.LI "\*LXmNpositionIndex\*O"
Specifies the position of the widget in its parent's list of
children (the list of pane children, not including sashes).  The value
is an integer that is no less than zero and no greater than
the number of children in the list at the time the value is
specified.  A value of zero means that the child is placed at the
beginning of the list.  The value can also be  specified as
\*LXmLAST_POSITION\*O (the default), which means that the child
is placed at the end of the list.  Any other value is ignored.
\*LXtGetValues\*O returns the position of the widget in its parent's
child list at the time of the call to \*LXtGetValues\*O.
.PP
When a widget is inserted into its parent's child list, the positions
of any existing children that are greater than or equal to the
specified widget's \*LXmNpositionIndex\*O are increased by one.
The effect of a call to \*LXtSetValues\*O for \*LXmNpositionIndex\*O
is to remove the specified widget from its parent's child list, decrease
by one the positions of any existing children that are greater than
the specified widget's former position in the list, and then insert
the specified widget into its parent's child list as described in the
preceding sentence.
.LI "\*LXmNskipAdjust\*O"
When set to True, this Boolean resource allows an application to specify
that the PanedWindow should not automatically resize this pane.
.LE 
.SS "Inherited Resources"
PanedWindow inherits behavior and resources from the following
superclasses.  For a complete description of each resource, refer to the
man page for that superclass.
.P 
.TS 
center allbox;
cBp9 ssss
lBp8 lBp8 lBp8 lBp8 lBp8
lp8 lp8 lp8 lp8 lp8.
XmManager Resource Set
Name	Class	Type	Default	Access
_
XmNbottomShadowColor	XmCBottomShadowColor	Pixel	dynamic	CSG
XmNbottomShadowPixmap	XmCBottomShadowPixmap	Pixmap	XmUNSPECIFIED_PIXMAP	CSG
XmNforeground	XmCForeground	Pixel	dynamic	CSG
XmNhelpCallback	XmCCallback	XtCallbackList	NULL	C
XmNhighlightColor	XmCHighlightColor	Pixel	dynamic	CSG
XmNhighlightPixmap	XmCHighlightPixmap	Pixmap	dynamic	CSG
XmNinitialFocus	XmCInitialFocus	Widget	NULL	CSG
XmNnavigationType	XmCNavigationType	XmNavigationType	XmTAB_GROUP	CSG
XmNshadowThickness	XmCShadowThickness	Dimension	2	CSG
XmNstringDirection	XmCStringDirection	XmStringDirection	dynamic	CG
XmNtopShadowColor	XmCTopShadowColor	Pixel	dynamic	CSG
XmNtopShadowPixmap	XmCTopShadowPixmap	Pixmap	dynamic	CSG
XmNtraversalOn	XmCTraversalOn	Boolean	True	CSG
XmNunitType	XmCUnitType	unsigned char	dynamic	CSG
XmNuserData	XmCUserData	XtPointer	NULL	CSG
.TE
.P 
.wH .in 0 
.TS 
center allbox;
cBp9 ssss
lBp8 lBp8 lBp8 lBp8 lBp8
lp8 lp8 lp8 lp8 lp8.
Core Resource Set
Name	Class	Type	Default	Access
_
XmNaccelerators	XmCAccelerators	XtAccelerators	dynamic	CSG
XmNancestorSensitive	XmCSensitive	Boolean	dynamic	G
XmNbackground	XmCBackground	Pixel	dynamic	CSG
XmNbackgroundPixmap	XmCPixmap	Pixmap	XmUNSPECIFIED_PIXMAP	CSG
XmNborderColor	XmCBorderColor	Pixel	XtDefaultForeground	CSG
XmNborderPixmap	XmCPixmap	Pixmap	XmUNSPECIFIED_PIXMAP	CSG
XmNborderWidth	XmCBorderWidth	Dimension	0	CSG
XmNcolormap	XmCColormap	Colormap	dynamic	CG
XmNdepth	XmCDepth	int	dynamic	CG
XmNdestroyCallback	XmCCallback	XtCallbackList	NULL	C
XmNheight	XmCHeight	Dimension	dynamic	CSG
XmNinitialResourcesPersistent	XmCInitialResourcesPersistent	Boolean	True	C
XmNmappedWhenManaged	XmCMappedWhenManaged	Boolean	True	CSG
XmNscreen	XmCScreen	Screen *	dynamic	CG
.wH .tH
XmNsensitive	XmCSensitive	Boolean	True	CSG
XmNtranslations	XmCTranslations	XtTranslations	dynamic	CSG
XmNwidth	XmCWidth	Dimension	dynamic	CSG
XmNx	XmCPosition	Position	0	CSG
XmNy	XmCPosition	Position	0	CSG
.TE
.wH .in  
.P 
.TS 
center allbox;
cBp9 ssss
lBp8 lBp8 lBp8 lBp8 lBp8
lp8 lp8 lp8 lp8 lp8.
Composite Resource Set
Name	Class	Type	Default	Access
_
XmNchildren	XmCReadOnly	WidgetList	NULL	G
XmNinsertPosition	XmCInsertPosition	XtOrderProc	default procedure	CSG
XmNnumChildren	XmCReadOnly	Cardinal	0	G
.TE
.SS "Translations"
XmPanedWindow inherits translations from XmManager.
.PP 
The translations for sashes within the PanedWindow are listed below.
These translations may not directly correspond to a
translation table.
.iS
.ta 1.5i
BSelect Press:	SashAction(Start)
BSelect Motion:	SashAction(Move)
BSelect Release:	SashAction(Commit)
.sp \n(PDu
BDrag Press:	SashAction(Start)
BDrag Motion:	SashAction(Move)
BDrag Release:	SashAction(Commit)
.sp \n(PDu
KUp:	SashAction(Key,DefaultIncr,Up)
MCtrl KUp:	SashAction(Key,LargeIncr,Up)
.sp \n(PDu
KDown:	SashAction(Key,DefaultIncr,Down)
MCtrl KDown:	SashAction(Key,LargeIncr,Down)
.sp \n(PDu
KNextField:	NextTabGroup()
KPrevField:	PrevTabGroup()
.sp \n(PDu
KHelp:	Help()
.wH .fi
.iE
.SS "Action Routines"
The XmPanedWindow action routines are described below:
.VL  
.LI "\*LHelp()\*O:"
Calls the callbacks for \*LXmNhelpCallback\*O if any exist.  If there are no help
callbacks for this widget, this action calls the help callbacks
for the nearest ancestor that has them.
.LI "\*LNextTabGroup()\*O:"
Moves the keyboard focus to the next tab group.
By default each pane and sash is a tab group.
.LI "\*LPrevTabGroup()\*O:"
Moves the keyboard focus to the previous tab group.
By default each pane and sash is a tab group.
.LI "\*LSashAction(\*Vaction\*L)\*O\ or\ \*LSashAction(Key,\*Vincrement\*L,\*Vdirection\*L)\*O:"
The \*LStart\*O action activates the interactive placement of the pane's borders.
The \*LMove\*O action causes the sash to track the position of the pointer.
If one of the panes reaches its minimum or maximum size, adjustment
continues with the next adjustable pane.
The \*LCommit\*O action ends sash motion.
.P
When sash action is caused by a keyboard event, the sash with the keyboard
focus is moved according to the \*Vincrement\*O and \*Vdirection\*O
specified.  \*LDefaultIncr\*O adjusts the sash by one line.
\*LLargeIncr\*O adjusts the sash by one view region.  The \*Vdirection\*O
is specified as either \*LUp\*O or \*LDown\*O.
.P
Note that the SashAction action routine is not a direct action routine
of the XmPanedWindow, but rather an action of the Sash control created
by the XmPanedWindow.
.LE
.SS "Additional Behavior"
This widget has the additional behavior described below:
.VL  
.LI "\*L<FocusIn>\*O:"
Moves the keyboard focus to the sash and highlights it.
.LI "\*L<FocusOut>\*O:"
Unsets the keyboard focus in the sash and unhighlights it.
.LE
.SS "Virtual Bindings"
The bindings for virtual keys are vendor specific.
For information about bindings for virtual buttons and keys, see \*LVirtualBindings(3X)\*O.
.SH RELATED INFORMATION
.na
\*LComposite(3X)\*O, \*LConstraint(3X)\*O,
\*LCore(3X)\*O, \*LXmCreatePanedWindow(3X)\*O, and \*LXmManager(3X)\*O.
.ad

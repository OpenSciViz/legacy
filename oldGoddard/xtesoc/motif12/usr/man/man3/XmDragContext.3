...\" **
...\" **
...\" **  (c) Copyright 1991, 1992 by Open Software Foundation, Inc.
...\" **      All Rights Reserved.
...\" **
...\" **  (c) Copyright 1991 by Hewlett-Packard Company
...\" **
...\" **
.TH XmDragContext 3X "" "" "" ""
.SH NAME
\*LXmDragContext\*O\(emThe DragContext widget class
.iX "XmDragContext"
.iX "widget class" "DragContext"
.SH SYNOPSIS
.sS
.iS
\&#include <Xm/DragDrop.h>
.iE
.sE
.SH DESCRIPTION
DragContexts are special widgets used in drag and drop transactions.
A DragContext is implemented as a widget, but a client does not
explicitly create a DragContext widget.  Instead, a client initiates
a drag and drop transaction by calling \*LXmDragStart\*O, and this
routine initializes and returns a DragContext widget.  There is a
unique DragContext for each drag operation.  The toolkit frees a
DragContext when a transaction is complete; therefore, an application
programmer should not explicitly destroy a DragContext.
.PP
Initiator and receiver clients both use DragContexts to track
the state of a transaction.  When the initiator and receiver of
a transaction are in the same client, they share the same
DragContext instance.  If they are in different clients, there
are two separate DragContexts.  In this case, the initiator calls
\*LXmDragStart\*O and the toolkit provides a DragContext for the
receiver client.  The only resources pertinent to the receiver
are \*LXmNexportTargets\*O and \*LXmNnumExportTargets\*O.  These
can both be passed as arguments to the \*LXmDropSiteRetrieve\*O
function to obtain information about the current drop site.
.PP
In general, in order to receive data, a drop site must share at least
one target type and operation in common with a drag source.  The
DragContext resource, \*LXmNexportTargets\*O, identifies the selection
targets for the drag source.  These export targets are compared with the
\*LXmNimportTargets\*O resource list specified by a drop site.
The DragContext resource, \*LXmNdragOperations\*O, identifies the
valid operations that can be applied to the source data by the
initiator.  The drop site counterpart resource is
\*LXmNdropSiteOperations\*O, which indicates a drop site's supported
operations.
.PP
A client uses DragIcon widgets to define the drag-over animation
effects associated with a given drag and drop transaction. 
An initiator specifies a set of drag icons, selects a blending
model, and sets foreground and background cursor colors with
DragContext resources.
.PP
The type of drag-over visual used to represent a drag operation
depends on the drag protocol style.  In preregister mode, the server
is grabbed, and either a cursor or a pixmap may be used as a drag-over
visual.  In dynamic mode, drag-over visuals must be implemented
with the X cursor.  If the resulting drag protocol style is
Drop Only or None and the \*LXmNdragInitiatorProtocolStyle\*O is 
\*LXmDRAG_DYNAMIC\*O or \*LXmDRAG_PREFER_DYNAMIC\*O,  
then a dynamic visual style (cursor) is used.  Otherwise, a preregister
visual style is used.
.SS "Classes"
DragContext inherits behavior and resources from \*LCore\*O.
.PP
The class pointer is \*LxmDragContextClass\*O.
.PP
The class name is \*LXmDragContext\*O.
.SS "New Resources"
The following table defines a set of widget resources used by the
programmer to specify data.  The programmer can also set the
resource values for the inherited classes to set attributes for
this widget.  To reference a resource by name or by class in
a .Xdefaults file, remove the \*LXmN\*O or \*LXmC\*O prefix and use
the remaining letters.  To specify one of the defined values for a
resource in a .Xdefaults file, remove the \*LXm\*O prefix and use
the remaining letters (in either lowercase or uppercase, but include
any underscores between words).  The codes in the access column
indicate if the given resource can be set at creation time (\*LC\*O),
set by using \*LXtSetValues\*O (\*LS\*O), retrieved by using
\*LXtGetValues\*O (\*LG\*O), or is not applicable (\*LN/A\*O).
.PP
.TS
center allbox;
cBp9 ssss
lBp8 lBp8 lBp8 lBp8 lBp8
lp8 lp8 lp8 lp8 lp8.
XmDragContext Resource Set
Name	Class	Type	Default	Access
_
XmNblendModel	XmCBlendModel	unsigned char	XmBLEND_ALL	CG
XmNclientData	XmCClientData	XtPointer	NULL	CSG
XmNconvertProc	XmCConvertProc	XtConvertSelectionIncrProc	NULL	CSG
XmNcursorBackground	XmCCursorBackground	Pixel	dynamic	CSG	
XmNcursorForeground	XmCCursorForeground	Pixel	dynamic	CSG
XmNdragDropFinishCallback	XmCCallback	XtCallbackList	NULL	CSG
XmNdragMotionCallback	XmCCallback	XtCallbackList	NULL	C
XmNdragOperations	XmCDragOperations	unsigned char	XmDROP_COPY | XmDROP_MOVE	C
XmNdropFinishCallback	XmCCallback	XtCallbackList	NULL	C
XmNdropSiteEnterCallback	XmCCallback	XtCallbackList	NULL	C
XmNdropSiteLeaveCallback	XmCCallback	XtCallbackList	NULL	C
XmNdropStartCallback	XmCCallback	XtCallbackList	NULL	C
XmNexportTargets	XmCExportTargets	Atom *	NULL	CSG
XmNincremental	XmCIncremental	Boolean	False	CSG
XmNinvalidCursorForeground	XmCCursorForeground	Pixel	dynamic	CSG
XmNnoneCursorForeground	XmCCursorForeground	Pixel	dynamic	CSG
XmNnumExportTargets	XmCNumExportTargets	Cardinal	0	CSG
XmNoperationChangedCallback	XmCCallback	XtCallbackList	NULL	C
XmNoperationCursorIcon	XmCOperationCursorIcon	Widget	dynamic	CSG
XmNsourceCursorIcon	XmCSourceCursorIcon	Widget	dynamic	CSG
XmNsourcePixmapIcon	XmCSourcePixmapIcon	Widget	dynamic	CSG
XmNstateCursorIcon	XmCStateCursorIcon	Widget	dynamic	CSG
XmNtopLevelEnterCallback	XmCCallback	XtCallbackList	NULL	C
XmNtopLevelLeaveCallback	XmCCallback	XtCallbackList	NULL	C
XmNvalidCursorForeground	XmCCursorForeground	Pixel	dynamic	CSG
.TE
.PP
.VL
.LI "\*LXmNblendModel\*O"
Specifies which combination of DragIcons are blended to produce
a drag-over visual.
.VL
.LI "\*LXmBLEND_ALL\*O"
Blends all three DragIcons - the source, state and operation icon.
The icons are layered from top to bottom with the operation icon
on top and the source icon on the bottom.
The hotspot is derived from the state icon.
.LI "\*LXmBLEND_STATE_SOURCE\*O"
Blends the state and source icons only.  The hotspot is derived
from the state icon.
.LI "\*LXmBLEND_JUST_SOURCE\*O"
Specifies that only the source icon is used, which the initiator
updates as required.
.LI "\*LXmBLEND_NONE\*O"
Specifies that no drag-over visual is generated.  The client
tracks the drop site status through callback routines and updates
the drag-over visuals as necessary.
.LE
.LI "\*LXmNclientData\*O"
Specifies the client data to be passed to the \*LXmNconvertProc\*O
when it is invoked.
.LI "\*LXmNconvertProc\*O"
Specifies a procedure of type \*LXtConvertSelectionIncrProc\*O that
converts the source data to the format(s) requested by the receiver
client.
The \*Vwidget\*O argument passed to this procedure is the DragContext
widget.
The selection atom passed is _MOTIF_DROP.
If \*LXmNincremental\*O is False, the procedure should ignore the
\*Vmax_length\*O, \*Vclient_data\*O, and \*Vrequest_id\*O arguments and
should handle the conversion atomically.
Data returned by \*LXmNconvertProc\*O must be allocated using
\*LXtMalloc\*O and will be freed automatically by the toolkit after the
transfer.
For additional information on selection conversion procedures, see \*EX
Toolkit Intrinsics\(emC Language Interface\*O.
.LI "\*LXmNcursorBackground\*O"
Specifies the background pixel value of the cursor.
.LI "\*LXmNcursorForeground\*O"
Specifies the foreground pixel value of the cursor when the state icon
is not blended.  This resource defaults to the foreground color of the
widget passed to the \*LXmDragStart\*O function.
.LI "\*LXmNdragDropFinishCallback\*O"
Specifies the list of callbacks that are called when the transaction is
completed.  The type of the structure whose address is passed to this
callback is \*LXmDragDropFinishCallbackStruct\*O.  The reason sent by
the callback is \*LXmCR_DRAG_DROP_FINISH\*O.
.LI "\*LXmNdragMotionCallback\*O"
Specifies the list of callbacks that are invoked when the pointer moves.
The type of structure whose address is passed to this callback is
\*LXmDragMotionCallbackStruct\*O.   The reason sent by the callback
is \*LXmCR_DRAG_MOTION\*O.
.LI "\*LXmNdragOperations\*O"
Specifies the set of valid operations associated with an initiator
client for a drag transaction.
This resource is a bit mask that is formed by combining one or
more of the following values using a bitwise operation such as
inclusive OR (|):
\*LXmDROP_COPY\*O, \*LXmDROP_LINK\*O, \*LXmDROP_MOVE\*O.
The value \*LXmDROP_NOOP\*O for this resource indicates that no
operations are valid.
For Text and TextField widgets, this resource is set to
\*LXmDROP_COPY\*O | \*LXmDROP_MOVE\*O; for List widgets, it is set to
\*LXmDROP_COPY\*O.
.LI "\*LXmNdropFinishCallback\*O"
Specifies the list of callbacks that are invoked when the drop
is completed.   The type of the structure whose address is passed to 
this callback is \*LXmDropFinishCallbackStruct\*O.   The reason sent
by the callback is \*LXmCR_DROP_FINISH\*O.
.LI "\*LXmNdropSiteEnterCallback\*O"
Specifies the list of callbacks that are invoked when the pointer enters
a drop site.  The type of the structure whose address is passed to this
callback is \*LXmDropSiteEnterCallbackStruct\*O.  The reason sent by the
callback is \*LXmCR_DROP_SITE_ENTER\*O.
.LI "\*LXmNdropSiteLeaveCallback\*O"
Specifies the list of callbacks that are invoked when the pointer leaves
a drop site.  The type of the structure whose address is passed to this
callback is \*LXmDropSiteLeaveCallbackStruct\*O.  The reason sent by
the callback is \*LXmCR_DROP_SITE_LEAVE\*O.
.LI "\*LXmNdropStartCallback\*O"
Specifies the list of callbacks that are invoked when a drop is
initiated.  The type of the structure whose address is passed to this
callback is \*LXmDropStartCallbackStruct\*O.  The reason sent by the
callback is \*LXmCR_DROP_START\*O.
.LI "\*LXmNexportTargets\*O"
Specifies the list of target atoms associated with this source.
This resource identifies the selection targets this source
can be converted to.
.LI "\*LXmNincremental\*O"
Specifies a Boolean value that indicates whether the transfer on the
initiator side uses the Xt incremental selection transfer mechanism
described in \*EX Toolkit Intrinsics\(emC Language Interface\*O.
If the value is True, the initiator uses incremental transfer; if the
value is False, the initiator uses atomic transfer.
.LI "\*LXmNinvalidCursorForeground\*O"
Specifies the foreground pixel value of the cursor when the state
is invalid.  This resource defaults to the value of the
\*LXmNcursorForeground\*O resource.
.LI "\*LXmNnoneCursorForeground\*O"
Specifies the foreground pixel value of the cursor when the state
is none.  This resource defaults to the value of the 
\*LXmNcursorForeground\*O resource.
.LI "\*LXmNnumExportTargets\*O"
Specifies the number of entries in the list of export targets.
.LI "\*LXmNoperationChangedCallback\*O"
Specifies the list of callbacks that are invoked when the drag
is started and when the user requests that a different operation
be applied to the drop.
The type of the structure whose address is passed to this callback
is \*LXmOperationChangedCallbackStruct\*O.  The reason sent by the
callback is \*LXmCR_OPERATION_CHANGED\*O.
.LI "\*LXmNoperationCursorIcon\*O"
Specifies the cursor icon used to designate the type of operation
performed by the drag transaction.  If  NULL, \*LXmScreen\*O
resources provide default icons for copy, link, and move
operations.
.LI "\*LXmNsourceCursorIcon\*O"
Specifies the cursor icon used to represent the source when
a dynamic visual style is used.  If NULL, the
\*LXmNdefaultSourceCursorIcon\*O resource of \*LXmScreen\*O provides
a default cursor icon.
.LI "\*LXmNsourcePixmapIcon\*O"
Specifies the pixmap icon used to represent the source when
a preregister visual style is used.  The icon is used in conjunction
with the colormap of the widget passed to \*LXmDragStart\*O.
If NULL, \*LXmNsourceCursorIcon\*O is used.  
.LI "\*LXmNstateCursorIcon\*O"
Specifies the cursor icon used to designate the state of a drop site.
If NULL, \*LXmScreen\*O resources provide default icons for a valid, 
invalid, and no drop site condition.
.LI "\*LXmNtopLevelEnterCallback\*O"
Specifies the list of callbacks that are called when the pointer enters
a top-level window or root window (due to changing screens).  The type
of the structure whose address is passed to this callback is
\*LXmTopLevelEnterCallbackStruct\*O.  The reason sent by the
callback is \*LXmCR_TOP_LEVEL_ENTER\*O.
.LI "\*LXmNtopLevelLeaveCallback\*O"
Specifies the list of callbacks that are called when the pointer
leaves a top level window or the root window (due to changing
screens).  The type of the structure whose address is
passed to this callback is \*LXmTopLevelLeaveCallbackStruct\*O.  The
reason sent by the callback is \*LXmCR_TOP_LEVEL_LEAVE\*O.
.LI "\*LXmNvalidCursorForeground\*O"
Specifies the foreground pixel value of the cursor designated as a
valid cursor icon.
.LE
.SS "Inherited Resources"
DragContext inherits behavior and resources from the following
superclass.  For a complete description of each resource, refer
to the \*LCore\*O man page.
.PP
.wH .in 0 
.TS 
center allbox;
cBp9 ssss
lBp8 lBp8 lBp8 lBp8 lBp8
lp8 lp8 lp8 lp8 lp8.
Core Resource Set
Name	Class	Type	Default	Access
_
XmNaccelerators	XmCAccelerators	XtAccelerators	dynamic	CSG
XmNancestorSensitive	XmCSensitive	Boolean	dynamic	G
XmNbackground	XmCBackground	Pixel	dynamic	CSG
XmNbackgroundPixmap	XmCPixmap	Pixmap	XmUNSPECIFIED_PIXMAP	CSG
XmNborderColor	XmCBorderColor	Pixel	XtDefaultForeground	CSG
XmNborderPixmap	XmCPixmap	Pixmap	XmUNSPECIFIED_PIXMAP	CSG
XmNborderWidth	XmCBorderWidth	Dimension	0	CSG
XmNcolormap	XmCColormap	Colormap	dynamic	CG
XmNdepth	XmCDepth	int	dynamic	CG
XmNdestroyCallback	XmCCallback	XtCallbackList	NULL	C
XmNheight	XmCHeight	Dimension	dynamic	CSG
XmNinitialResourcesPersistent	XmCInitialResourcesPersistent	Boolean	True	C
XmNmappedWhenManaged	XmCMappedWhenManaged	Boolean	True	CSG
XmNscreen	XmCScreen	Screen *	dynamic	CG
.wH .tH
XmNsensitive	XmCSensitive	Boolean	True	CSG
XmNtranslations	XmCTranslations	XtTranslations	dynamic	CSG
XmNwidth	XmCWidth	Dimension	dynamic	CSG
XmNx	XmCPosition	Position	0	CSG
XmNy	XmCPosition	Position	0	CSG
.TE
.wH .in  
.PP
.SS "Callback Information"
Each of the DragContext callbacks has an associated callback
structure. 
.PP
.nL
.PP
A pointer to the following structure is passed to the
\*LXmNdragDropFinishCallback\*O callback.
.ne 10
.sS
.iS
.ta .25i 1.5i
typedef struct
{
	int	\*Vreason\*L;
	XEvent	*\*Vevent\*L;
	Time	\*VtimeStamp\*L;
}XmDragDropFinishCallbackStruct, *XmDragDropFinishCallback;
.iE
.sE
.VL 1.25i
.LI "\*Vreason\*O"
Indicates why the callback was invoked.
.LI "\*Vevent\*O"
Points to the \*LXEvent\*O that triggered the callback.
.LI "\*Vtimestamp\*O"
Specifies the time at which either the drag or the drop was completed.
.LE
.PP
A pointer to the following structure is passed to callbacks for
\*LXmNdragMotionCallback\*O. 
.nL
.ne 10
.sS
.iS
.ta .25i 1.5i
typedef struct
{
	int	\*Vreason\*L;
	XEvent	*\*Vevent\*L;
	Time	\*VtimeStamp\*L;
	unsigned char	\*Voperation\*L;
	unsigned char	\*Voperations\*L;
	unsigned char	\*VdropSiteStatus\*L;
	Position	\*Vx\*L;
	Position	\*Vy\*L;
}XmDragMotionCallbackStruct, *XmDragMotionCallback;
.iE
.sE
.VL 1.25i
.LI "\*Vreason\*O"
Indicates why the callback was invoked.
.LI "\*Vevent\*O"
Points to the \*LXEvent\*O that triggered the callback.
.LI "\*VtimeStamp\*O"
Specifies the timestamp of the logical event.
.LI "\*Voperation\*O"
Identifies an operation.
.PP
If the toolkit has just called a DropSite's \*LXmNdragProc\*O, the
toolkit initializes \*Voperation\*O to the value of the \*Voperation\*O
member of the \*LXmDragProcCallbackStruct\*O at the time the DropSite's
\*LXmNdragProc\*O returns.
.PP
If the toolkit has not called an \*LXmNdragProc\*O and the pointer is
within an active drop site, the toolkit initializes \*Voperation\*O by
selecting an operation from the bitwise AND of the initial value of the
\*Voperations\*O member and the value of the DropSite's
\*LXmNdropSiteOperations\*O resource.
The toolkit searches this set first for \*LXmDROP_MOVE\*O, then for
\*LXmDROP_COPY\*O, then for \*LXmDROP_LINK\*O, and initializes
\*Voperation\*O to the first operation it finds in the set.
If the toolkit finds none of these operations in the set, it initializes
\*Voperation\*O to \*LXmDROP_NOOP\*O.
.PP
If the toolkit has not called an \*LXmNdragProc\*O and the pointer is
not within an active drop site, the toolkit initializes \*Voperation\*O
by selecting an operation from the initial value of the \*Voperations\*O
member.
The toolkit searches this set first for \*LXmDROP_MOVE\*O, then for
\*LXmDROP_COPY\*O, then for \*LXmDROP_LINK\*O, and initializes
\*Voperation\*O to the first operation it finds in the set.
If the toolkit finds none of these operations in the set, it initializes
\*Voperation\*O to \*LXmDROP_NOOP\*O.
.LI "\*Voperations\*O"
Indicates the set of operations supported for the source data.
.PP
If the toolkit has just called a DropSite's \*LXmNdragProc\*O, the
toolkit initializes \*Voperations\*O to the bitwise AND of the
DropSite's \*LXmNdropOperations\*O and the value of the \*Voperations\*O
member of the \*LXmDragProcCallbackStruct\*O at the time the DropSite's
\*LXmNdragProc\*O returns.
If the resulting set of operations is empty, the toolkit initializes
\*Voperations\*O to \*LXmDROP_NOOP\*O.
.PP
If the toolkit has not called an \*LXmNdragProc\*O and the user does not
select an operation (by pressing a modifier key), the toolkit
initializes \*Voperations\*O to the value of the DragContext's
\*LXmNdragOperations\*O resource.
.PP
If the toolkit has not called an \*LXmNdragProc\*O and the user does
select an operation, the toolkit initializes \*Voperations\*O to the
bitwise AND of the corresponding operation and the value of the
DragContext's \*LXmNdragOperations\*O resource.
If the resulting set of operations is empty, the toolkit initializes
\*Voperations\*O to \*LXmDROP_NOOP\*O.
.LI "\*VdropSiteStatus\*O"
Indicates whether or not a drop site is valid.
.PP
If the toolkit has just called a DropSite's \*LXmNdragProc\*O, the
toolkit initializes \*VdropSiteStatus\*O to the value of the
\*VdropSiteStatus\*O member of the \*LXmDragProcCallbackStruct\*O at the
time the DropSite's \*LXmNdragProc\*O returns.
.PP
If the toolkit has not called an \*LXmNdragProc\*O, it initializes
\*VdropSiteStatus\*O as follows:
the toolkit initializes \*VdropSiteStatus\*O to \*LXmNO_DROP_SITE\*O if
the pointer is over an inactive drop site or is not over a drop site.
The toolkit initializes \*VdropSiteStatus\*O to \*LXmDROP_SITE_VALID\*O
if all the following conditions are met:
.ML
.LI
The pointer is over an active drop site.
.LI
The DragContext's \*LXmNexportTargets\*O and the DropSite's
\*LXmNimportTargets\*O are compatible.
.LI
The initial value of the \*Voperation\*O member is not
\*LXmDROP_NOOP\*O.
.LE
Otherwise, the toolkit initializes \*VdropSiteStatus\*O to
\*LXmDROP_SITE_INVALID\*O.
.LE
.nL
.PP
A pointer to the following structure is passed for the 
\*LXmNdropFinishCallback\*O callback:
.ne 10
.sS
.iS
.ta .25i 1.5i
typedef struct
{
	int	\*Vreason\*L;
	XEvent	*\*Vevent\*L;
	Time	\*VtimeStamp\*L;
	unsigned char	\*Voperation\*L;
	unsigned char	\*Voperations\*L;
	unsigned char	\*VdropSiteStatus\*L;
	unsigned char	\*VdropAction\*L;
	unsigned char	\*VcompletionStatus\*L;
}XmDropFinishCallbackStruct, *XmDropFinishCallback;
.iE
.sE
.VL 1.25i
.LI "\*Vreason\*O"
Indicates why the callback was invoked.
.LI "\*Vevent\*O"
Points to the \*LXEvent\*O that triggered the callback.
.LI "\*VtimeStamp\*O"
Specifies the time at which the drop was completed.
.LI "\*Voperation\*O"
Identifies an operation.
.PP
If the pointer is over an active drop site when the drop begins, the
toolkit initializes \*Voperation\*O to the value of the \*Voperation\*O
member of the \*LXmDropProcCallbackStruct\*O at the time the DropSite's
\*LXmNdropProc\*O returns.
.PP
If the pointer is not over an active drop site when the drop begins, the
toolkit initializes \*Voperation\*O by selecting an operation from the
initial value of the \*Voperations\*O member.
The toolkit searches this set first for \*LXmDROP_MOVE\*O, then for
\*LXmDROP_COPY\*O, then for \*LXmDROP_LINK\*O, and initializes
\*Voperation\*O to the first operation it finds in the set.
If it finds none of these operations in the set, it initializes
\*Voperation\*O to \*LXmDROP_NOOP\*O.
.LI "\*Voperations\*O"
Indicates the set of operations supported for the source data.
.PP
If the pointer is over an active drop site when the drop begins, the
toolkit initializes \*Voperations\*O to the bitwise AND of the
DropSite's \*LXmNdropOperations\*O and the value of the \*Voperations\*O
member of the \*LXmDropProcCallbackStruct\*O at the time the DropSite's
\*LXmNdropProc\*O returns.
If the resulting set of operations is empty, the toolkit initializes
\*Voperations\*O to \*LXmDROP_NOOP\*O.
.PP
If the pointer is not over an active drop site when the drop begins and
if the user does not select an operation (by pressing a modifier key),
the toolkit initializes \*Voperations\*O to the value of the
DragContext's \*LXmNdragOperations\*O resource.
.PP
If the pointer is not over an active drop site when the drop begins and
if the user does select an operation, the toolkit initializes
\*Voperations\*O to the bitwise AND of the corresponding operation and
the value of the DragContext's \*LXmNdragOperations\*O resource.
If the resulting set of operations is empty, the toolkit initializes
\*Voperations\*O to \*LXmDROP_NOOP\*O.
.LI "\*VdropSiteStatus\*O"
Indicates whether or not a drop site is valid.
.PP
If the pointer is over an active drop site when the drop begins, the
toolkit initializes \*VdropSiteStatus\*O to the value of the
\*VdropSiteStatus\*O member of the \*LXmDropProcCallbackStruct\*O at the
time the DropSite's \*LXmNdropProc\*O returns.
.PP
If the pointer is not over an active drop site when the drop begins, the
toolkit initializes \*VdropSiteStatus\*O to \*LXmNO_DROP_SITE\*O.
.LI "\*VdropAction\*O"
Identifies the drop action.  The values are: \*LXmDROP\*O,
\*LXmDROP_CANCEL\*O, \*LXmDROP_HELP\*O, and \*LXmDROP_INTERRUPT\*O.
The value \*LXmDROP_INTERRUPT\*O is currently unsupported; if
specified, it will be interpreted as an \*LXmDROP_CANCEL\*O.
.LI "\*VcompletionStatus\*O"
An IN/OUT member that indicates the status of the drop action.
After the last callback procedure has returned, the final value of this
member determines what visual transition effects will be applied.
There are two values:
.VL .25i
.LI "\*LXmDROP_SUCCESS\*O"
The drop was successful.
.LI "\*LXmDROP_FAILURE\*O"
The drop was unsuccessful.
.LE
.LE
.nL
.PP
A pointer to the following structure is passed to callbacks for
\*LXmNdropSiteEnterCallback\*O:
.ne 10
.sS
.iS
.ta .25i 1.5i
typedef struct
{
	int	\*Vreason\*L;
	XEvent	*\*Vevent\*L;
	Time	\*VtimeStamp\*L;
	unsigned char	\*Voperation\*L;
	unsigned char	\*Voperations\*L;
	unsigned char	\*VdropSiteStatus\*L;
	Position	\*Vx\*L;
	Position	\*Vy\*L;
}XmDropSiteEnterCallbackStruct, *XmDropSiteEnterCallback;
.iE
.sE
.VL 1.25i
.LI "\*Vreason\*O"
Indicates why the callback was invoked.
.LI "\*Vevent\*O"
Points to the \*LXEvent\*O that triggered the callback.
.LI "\*Vtimestamp\*O"
Specifies the time the crossing event occurred.
.LI "\*Voperation\*O"
Identifies an operation.
.PP
If the toolkit has just called a DropSite's \*LXmNdragProc\*O, the
toolkit initializes \*Voperation\*O to the value of the \*Voperation\*O
member of the \*LXmDragProcCallbackStruct\*O at the time the DropSite's
\*LXmNdragProc\*O returns.
.PP
If the toolkit has not called an \*LXmNdragProc\*O, it initializes
\*Voperation\*O by selecting an operation from the bitwise AND of the
initial value of the \*Voperations\*O member and the value of the
DropSite's \*LXmNdropSiteOperations\*O resource.
The toolkit searches this set first for \*LXmDROP_MOVE\*O, then for
\*LXmDROP_COPY\*O, then for \*LXmDROP_LINK\*O, and initializes
\*Voperation\*O to the first operation it finds in the set.
If the toolkit finds none of these operations in the set, it initializes
\*Voperation\*O to \*LXmDROP_NOOP\*O.
.LI "\*Voperations\*O"
Indicates the set of operations supported for the source data.
.PP
If the toolkit has just called a DropSite's \*LXmNdragProc\*O, the
toolkit initializes \*Voperations\*O to the bitwise AND of the
DropSite's \*LXmNdropOperations\*O and the value of the \*Voperations\*O
member of the \*LXmDragProcCallbackStruct\*O at the time the DropSite's
\*LXmNdragProc\*O returns.
If the resulting set of operations is empty, the toolkit initializes
\*Voperations\*O to \*LXmDROP_NOOP\*O.
.PP
If the toolkit has not called an \*LXmNdragProc\*O and the user does not
select an operation (by pressing a modifier key), the toolkit
initializes \*Voperations\*O to the value of the DragContext's
\*LXmNdragOperations\*O resource.
.PP
If the toolkit has not called an \*LXmNdragProc\*O and the user does
select an operation, the toolkit initializes \*Voperations\*O to the
bitwise AND of the corresponding operation and the value of the
DragContext's \*LXmNdragOperations\*O resource.
If the resulting set of operations is empty, the toolkit initializes
\*Voperations\*O to \*LXmDROP_NOOP\*O.
.LI "\*VdropSiteStatus\*O"
Indicates whether or not a drop site is valid.
.PP
If the toolkit has just called a DropSite's \*LXmNdragProc\*O, the
toolkit initializes \*VdropSiteStatus\*O to the value of the
\*VdropSiteStatus\*O member of the \*LXmDragProcCallbackStruct\*O at the
time the DropSite's \*LXmNdragProc\*O returns.
.PP
If the toolkit has not called an \*LXmNdragProc\*O, it initializes
\*VdropSiteStatus\*O as follows:
the toolkit initializes \*VdropSiteStatus\*O to \*LXmDROP_SITE_VALID\*O
if the DragContext's \*LXmNexportTargets\*O and the DropSite's
\*LXmNimportTargets\*O are compatible and if the initial value of the
\*Voperation\*O member is not \*LXmDROP_NOOP\*O.
Otherwise, the toolkit initializes \*VdropSiteStatus\*O to
\*LXmDROP_SITE_INVALID\*O.
.LI "\*Vx\*O"
Indicates the x-coordinate of the pointer in root window coordinates.
.LI "\*Vy\*O"
Indicates the y-coordinate of the pointer in root window coordinates.
.LE
.nL
.PP
A pointer to the following structure is passed to callbacks for
\*LXmNdropSiteLeaveCallback\*O.
.ne 10
.sS
.iS
.ta .25i 1.5i
typedef struct
{
	int	\*Vreason\*L;
	XEvent	*\*Vevent\*L;
	Time	\*VtimeStamp\*L;
}XmDropSiteLeaveCallbackStruct, *XmDropSiteLeaveCallback;
.iE
.sE
.VL 1.25i
.LI "\*Vreason\*O"
Indicates why the callback was invoked.
.LI "\*Vevent\*O"
Points to the \*LXEvent\*O that triggered the callback.
.LI "\*VtimeStamp\*O"
Specifies the timestamp of the logical event.
.LE
.PP
A pointer to the following structure is passed for the 
\*LXmNdropStartCallback\*O callback:
.ne 10
.sS
.iS
.ta .25i 1.5i
typedef struct
{
	int	\*Vreason\*L;
	XEvent	*\*Vevent\*L;
	Time	\*VtimeStamp\*L;
	unsigned char	\*Voperation\*L;
	unsigned char	\*Voperations\*L;
	unsigned char	\*VdropSiteStatus\*L;
	unsigned char	\*VdropAction\*L;
	Position	\*Vx\*L;
	Position	\*Vy\*L;
}XmDropStartCallbackStruct, *XmDropStartCallback;
.iE
.sE
.VL 1.25i
.LI "\*Vreason\*O"
Indicates why the callback was invoked.
.LI "\*Vevent\*O"
Points to the \*LXEvent\*O that triggered the callback.
.LI "\*VtimeStamp\*O"
Specifies the time at which the drag was completed.
.LI "\*Voperation\*O"
Identifies an operation.
.PP
If the pointer is over an active drop site when the drop begins, the
toolkit initializes \*Voperation\*O to the value of the \*Voperation\*O
member of the \*LXmDropProcCallbackStruct\*O at the time the DropSite's
\*LXmNdropProc\*O returns.
.PP
If the pointer is not over an active drop site when the drop begins, the
toolkit initializes \*Voperation\*O by selecting an operation from the
initial value of the \*Voperations\*O member.
The toolkit searches this set first for \*LXmDROP_MOVE\*O, then for
\*LXmDROP_COPY\*O, then for \*LXmDROP_LINK\*O, and initializes
\*Voperation\*O to the first operation it finds in the set.
If it finds none of these operations in the set, it initializes
\*Voperation\*O to \*LXmDROP_NOOP\*O.
.LI "\*Voperations\*O"
Indicates the set of operations supported for the source data.
.PP
If the pointer is over an active drop site when the drop begins, the
toolkit initializes \*Voperations\*O to the bitwise AND of the
DropSite's \*LXmNdropOperations\*O and the value of the \*Voperations\*O
member of the \*LXmDropProcCallbackStruct\*O at the time the DropSite's
\*LXmNdropProc\*O returns.
If the resulting set of operations is empty, the toolkit initializes
\*Voperations\*O to \*LXmDROP_NOOP\*O.
.PP
If the pointer is not over an active drop site when the drop begins and
if the user does not select an operation (by pressing a modifier key),
the toolkit initializes \*Voperations\*O to the value of the
DragContext's \*LXmNdragOperations\*O resource.
.PP
If the pointer is not over an active drop site when the drop begins and
if the user does select an operation, the toolkit initializes
\*Voperations\*O to the bitwise AND of the corresponding operation and
the value of the DragContext's \*LXmNdragOperations\*O resource.
If the resulting set of operations is empty, the toolkit initializes
\*Voperations\*O to \*LXmDROP_NOOP\*O.
.LI "\*VdropSiteStatus\*O"
Indicates whether or not a drop site is valid.
.PP
If the pointer is over an active drop site when the drop begins, the
toolkit initializes \*VdropSiteStatus\*O to the value of the
\*VdropSiteStatus\*O member of the \*LXmDropProcCallbackStruct\*O at the
time the DropSite's \*LXmNdropProc\*O returns.
.PP
If the pointer is not over an active drop site when the drop begins, the
toolkit initializes \*VdropSiteStatus\*O to \*LXmNO_DROP_SITE\*O.
.LI "\*VdropAction\*O"
An IN/OUT member that identifies the drop action.
The values are \*LXmDROP\*O, \*LXmDROP_CANCEL\*O, \*LXmDROP_HELP\*O,
and \*LXmDROP_INTERRUPT\*O.  The value of \*VdropAction\*O can be
modified to change the action actually initiated.  
The value \*LXmDROP_INTERRUPT\*O is currently unsupported; if
specified, it will be interpreted as a \*LXmDROP_CANCEL\*O.
.LI "\*Vx\*O"
Indicates the x-coordinate of the pointer in root window coordinates.
.LI "\*Vy\*O"
Indicates the y-coordinate of the pointer in root window coordinates.
.LE
.nL
.PP
A pointer to the following structure is passed to the 
\*LXmNoperationChangedCallback\*O callback:
.ne 10
.sS
.iS
.ta .25i 1.5i
typedef struct
{
	int	\*Vreason\*L;
	XEvent	*\*Vevent\*L;
	Time	\*VtimeStamp\*L;
	unsigned char	\*Voperation\*L;
	unsigned char	\*Voperations\*L;
	unsigned char	\*VdropSiteStatus\*L;
}XmOperationChangedCallbackStruct, *XmOperationChangedCallback;
.iE
.sE
.VL 1.25i
.LI "\*Vreason\*O"
Indicates why the callback was invoked.
.LI "\*Vevent\*O"
Points to the \*LXEvent\*O that triggered the callback.
.LI "\*Vtimestamp\*O"
Specifies the time at which the crossing event occurred.
.LI "\*Voperation\*O"
Identifies an operation.
.PP
If the toolkit has just called a DropSite's \*LXmNdragProc\*O, the
toolkit initializes \*Voperation\*O to the value of the \*Voperation\*O
member of the \*LXmDragProcCallbackStruct\*O at the time the DropSite's
\*LXmNdragProc\*O returns.
.PP
If the toolkit has not called an \*LXmNdragProc\*O and the pointer is
within an active drop site, the toolkit initializes \*Voperation\*O by
selecting an operation from the bitwise AND of the initial value of the
\*Voperations\*O member and the value of the DropSite's
\*LXmNdropSiteOperations\*O resource.
The toolkit searches this set first for \*LXmDROP_MOVE\*O, then for
\*LXmDROP_COPY\*O, then for \*LXmDROP_LINK\*O, and initializes
\*Voperation\*O to the first operation it finds in the set.
If the toolkit finds none of these operations in the set, it initializes
\*Voperation\*O to \*LXmDROP_NOOP\*O.
.PP
If the toolkit has not called an \*LXmNdragProc\*O and the pointer is
not within an active drop site, the toolkit initializes \*Voperation\*O
by selecting an operation from the initial value of the \*Voperations\*O
member.
The toolkit searches this set first for \*LXmDROP_MOVE\*O, then for
\*LXmDROP_COPY\*O, then for \*LXmDROP_LINK\*O, and initializes
\*Voperation\*O to the first operation it finds in the set.
If the toolkit finds none of these operations in the set, it initializes
\*Voperation\*O to \*LXmDROP_NOOP\*O.
.LI "\*Voperations\*O"
Indicates the set of operations supported for the source data.
.PP
If the toolkit has just called a DropSite's \*LXmNdragProc\*O, the
toolkit initializes \*Voperations\*O to the bitwise AND of the
DropSite's \*LXmNdropOperations\*O and the value of the \*Voperations\*O
member of the \*LXmDragProcCallbackStruct\*O at the time the DropSite's
\*LXmNdragProc\*O returns.
If the resulting set of operations is empty, the toolkit initializes
\*Voperations\*O to \*LXmDROP_NOOP\*O.
.PP
If the toolkit has not called an \*LXmNdragProc\*O and the user does not
select an operation (by pressing a modifier key), the toolkit
initializes \*Voperations\*O to the value of the DragContext's
\*LXmNdragOperations\*O resource.
.PP
If the toolkit has not called an \*LXmNdragProc\*O and the user does
select an operation, the toolkit initializes \*Voperations\*O to the
bitwise AND of the corresponding operation and the value of the
DragContext's \*LXmNdragOperations\*O resource.
If the resulting set of operations is empty, the toolkit initializes
\*Voperations\*O to \*LXmDROP_NOOP\*O.
.LI "\*VdropSiteStatus\*O"
Indicates whether or not a drop site is valid.
.PP
If the toolkit has just called a DropSite's \*LXmNdragProc\*O, the
toolkit initializes \*VdropSiteStatus\*O to the value of the
\*VdropSiteStatus\*O member of the \*LXmDragProcCallbackStruct\*O at the
time the DropSite's \*LXmNdragProc\*O returns.
.PP
If the toolkit has not called an \*LXmNdragProc\*O, it initializes
\*VdropSiteStatus\*O as follows:
the toolkit initializes \*VdropSiteStatus\*O to \*LXmNO_DROP_SITE\*O if
the pointer is over an inactive drop site or is not over a drop site.
The toolkit initializes \*VdropSiteStatus\*O to \*LXmDROP_SITE_VALID\*O
if all the following conditions are met:
.ML
.LI
The pointer is over an active drop site
.LI
The DragContext's \*LXmNexportTargets\*O and the DropSite's
\*LXmNimportTargets\*O are compatible
.LI
The initial value of the \*Voperation\*O member is not
\*LXmDROP_NOOP\*O
.LE
Otherwise, the toolkit initializes \*VdropSiteStatus\*O to
\*LXmDROP_SITE_INVALID\*O.
.LE
.nL
.PP
A pointer to the following structure is passed to callbacks for
\*LXmNtopLevelEnterCallback\*O:
.ne 10
.sS
.iS
.ta .25i 1.5i
typedef struct
{
	int	\*Vreason\*L;
	XEvent	*\*Vevent\*L;
	Time	\*Vtimestamp\*L;
	Screen	\*Vscreen\*L;
	Window	\*Vwindow\*L;
	Position	\*Vx\*L;
	Position	\*Vy\*L;
	unsigned char	\*VdragProtocolStyle\*L;
}XmTopLevelEnterCallbackStruct, *XmTopLevelEnterCallback;
.iE
.sE
.VL 1.25i
.LI "\*Vreason\*O"
Indicates why the callback was invoked.
.LI "\*Vevent\*O"
Points to the \*LXEvent\*O that triggered the callback.
.LI "\*VtimeStamp\*O"
Specifies the timestamp of the logical event.
.LI "\*Vscreen\*O"
Specifies the screen associated with the top-level window or root
window being entered.
.LI "\*Vwindow\*O"
Specifies the ID of the top-level window or root window being entered.
.LI "\*Vx\*O"
Indicates the x-coordinate of the pointer in root window coordinates.
.LI "\*Vy\*O"
Indicates the y-coordinate of the pointer in root window coordinates.
.LI "\*VdragProtocolStyle\*O"
Specifies the protocol style adopted by the initiator.  The values
are \*LXmDRAG_DROP_ONLY\*O, \*LXmDRAG_DYNAMIC\*O, \*LXmDRAG_NONE\*O,
and \*LXmDRAG_PREREGISTER\*O.
.LE
.nL
.PP
A pointer to the following structure is passed to callbacks for
\*LXmNtopLevelLeaveCallback\*O:
.ne 10
.sS
.iS
.ta .25i 1.5i
typedef struct
{
	int	\*Vreason\*L;
	XEvent	*\*Vevent\*L;
	Time	\*Vtimestamp\*L;
	Screen	\*Vscreen\*L;
	Window	\*Vwindow\*L;
}XmTopLevelLeaveCallbackStruct, *XmTopLevelLeaveCallback;
.iE
.sE
.VL 1.25i
.LI "\*Vreason\*O"
Indicates why the callback was invoked.
.LI "\*Vevent\*O"
Points to the \*LXEvent\*O that triggered the callback.
.LI "\*VtimeStamp\*O"
Specifies the timestamp of the logical event.
.LI "\*Vscreen\*O"
Specifies a screen associated with the top-level window or root
window being left.
.LI "\*Vwindow\*O"
Specifies the ID of the top-level window or root window being left.
.LE
.PP
.SS "Translations"
The XmDragContext translations are listed below.
These translations may not directly correspond to a
translation table.
.iS
.ta 1.5i
BDrag Motion:	DragMotion()
BDrag Release:	FinishDrag()
.sp \n(PDu
KCancel:	CancelDrag()
KHelp:	HelpDrag()
.nL
.wH .fi
.iE
.SS "Action Routines"
The XmDragContext action routines are described below:
.VL
.LI "\*LCancelDrag()\*O:"
Cancels the drag operation and frees the associated
DragContext.
.LI "\*LDragMotion()\*O:"
Drags the selected data as the pointer is moved.
.LI "\*LFinishDrag()\*O:"
Finishes the drag operation and starts the drop operation.
.LI "\*LHelpDrag()\*O:"
Initiates a conditional drop that enables the receiver to provide
help information to the user.  The user can cancel or continue the
drop operation in response to this information.
.LE
.SS "Virtual Bindings"
The bindings for virtual keys are vendor specific.
For information about bindings for virtual buttons and keys,
see \*LVirtualBindings(3X)\*O.
.PP
.SH "RELATED INFORMATION"
.na
\*LCore(3X)\*O,
\*LXmDisplay(3X)\*O,
\*LXmDragCancel(3X)\*O,
\*LXmDragIcon(3X)\*O,
\*LXmDragStart(3X)\*O,
\*LXmDropSite(3X)\*O,
\*LXmDropTransfer(3X)\*O, and
\*LXmScreen(3X)\*O.
.ad

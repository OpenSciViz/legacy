/* 
 * (c) Copyright 1989, 1990, 1991, 1992 OPEN SOFTWARE FOUNDATION, INC. 
 * ALL RIGHTS RESERVED 
*/ 
/* 
 * Motif Release 1.2
*/ 
/*   $RCSfile: TransltnsP.h,v $ $Revision: 1.6 $ $Date: 92/03/13 16:52:46 $ */
/*
*  (c) Copyright 1989, DIGITAL EQUIPMENT CORPORATION, MAYNARD, MASS. */
/*
*  (c) Copyright 1987, 1988, 1989, 1990, 1991, 1992 HEWLETT-PACKARD COMPANY */
#ifndef _XmTransltnsP_h
#define _XmTransltnsP_h

#ifdef __cplusplus
extern "C" {
#endif


#ifndef _XmConst
#define _XmConst
#endif


extern _XmConst char _XmArrowB_defaultTranslations[];
extern _XmConst char _XmBulletinB_defaultTranslations[];
extern _XmConst char _XmCascadeB_menubar_events[];
extern _XmConst char _XmCascadeB_p_events[];
extern _XmConst char _XmDrawingA_defaultTranslations[];
extern _XmConst char _XmDrawingA_traversalTranslations[];
extern _XmConst char _XmDrawnB_defaultTranslations[];
extern _XmConst char _XmFrame_defaultTranslations[];
extern _XmConst char _XmLabel_defaultTranslations[];
extern _XmConst char _XmLabel_menuTranslations[];
extern _XmConst char _XmLabel_menu_traversal_events[];
extern _XmConst char _XmList_ListXlations1[];
extern _XmConst char _XmList_ListXlations2[];
extern _XmConst char _XmManager_managerTraversalTranslations[];
extern _XmConst char _XmManager_defaultTranslations[];
extern _XmConst char _XmMenuShell_translations [];
extern _XmConst char _XmPrimitive_defaultTranslations[];
extern _XmConst char _XmPushB_defaultTranslations[];
extern _XmConst char _XmPushB_menuTranslations[];
extern _XmConst char _XmRowColumn_menu_traversal_table[];
extern _XmConst char _XmRowColumn_bar_table[];
extern _XmConst char _XmRowColumn_option_table[];
extern _XmConst char _XmRowColumn_menu_table[];
extern _XmConst char _XmSash_defTranslations[];
extern _XmConst char _XmScrollBar_defaultTranslations[];
extern _XmConst char _XmScrolledW_ScrolledWindowXlations[];
extern _XmConst char _XmScrolledW_ClipWindowTranslationTable[];
extern _XmConst char _XmScrolledW_WorkWindowTranslationTable[];
extern _XmConst char _XmSelectioB_defaultTextAccelerators[];
extern _XmConst char _XmTearOffB_overrideTranslations[];
extern _XmConst char _XmTextF_EventBindings1[];
extern _XmConst char _XmTextF_EventBindings2[]; 
extern _XmConst char _XmTextF_EventBindings3[];
extern _XmConst char _XmTextIn_XmTextEventBindings1[];
extern _XmConst char _XmTextIn_XmTextEventBindings2[];
extern _XmConst char _XmTextIn_XmTextEventBindings3[];
extern _XmConst char _XmTextIn_XmTextEventBindings3[];
extern _XmConst char _XmToggleB_defaultTranslations[];
extern _XmConst char _XmToggleB_menuTranslations[];
extern _XmConst char _XmVirtKeys_fallbackBindingString[];

/* The following keybindings have been "grandfathered" 
   for backward compatablility.
*/
extern _XmConst char _XmVirtKeys_acornFallbackBindingString[];
extern _XmConst char _XmVirtKeys_apolloFallbackBindingString[];
extern _XmConst char _XmVirtKeys_dgFallbackBindingString[];
extern _XmConst char _XmVirtKeys_decFallbackBindingString[];
extern _XmConst char _XmVirtKeys_dblclkFallbackBindingString[];
extern _XmConst char _XmVirtKeys_hpFallbackBindingString[];
extern _XmConst char _XmVirtKeys_ibmFallbackBindingString[];
extern _XmConst char _XmVirtKeys_ingrFallbackBindingString[];
extern _XmConst char _XmVirtKeys_megatekFallbackBindingString[];
extern _XmConst char _XmVirtKeys_motorolaFallbackBindingString[];
extern _XmConst char _XmVirtKeys_sgiFallbackBindingString[];
extern _XmConst char _XmVirtKeys_siemensWx200FallbackBindingString[];
extern _XmConst char _XmVirtKeys_siemens9733FallbackBindingString[];
extern _XmConst char _XmVirtKeys_sunFallbackBindingString[];
extern _XmConst char _XmVirtKeys_tekFallbackBindingString[];

#ifdef __cplusplus
}  /* Close scope of 'extern "C"' declaration which encloses file. */
#endif

#endif /* _XmTransltnsP_h */
/* DON'T ADD ANYTHING AFTER THIS #endif */




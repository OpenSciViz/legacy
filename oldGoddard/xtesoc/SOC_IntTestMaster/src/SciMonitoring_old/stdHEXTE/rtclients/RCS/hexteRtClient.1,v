head	1.2;
access
	soccm
	tgasaway;
symbols
	Build4_1:1.1;
locks; strict;
comment	@# @;


1.2
date	97.03.03.23.43.24;	author rbentley;	state Exp;
branches;
next	1.1;

1.1
date	95.02.27.03.06.51;	author rbentley;	state Exp;
branches;
next	;


desc
@realtime client man page
@


1.2
log
@added start/end times for remote monitoring
@
text
@.\"
.\"  $Id: hexteRtClient.1,v 1.1 1995/02/27 03:06:51 rbentley Exp $
.\"
.TH hexteRtClient 1 "26 February 1995"
.SH NAME
hexteRtClient \- HEXTE real-time client for Science Monitoring and Health
and Safety
.SH SYNOPSIS
\fBhexteRtClient\fP  [\fB-ascii\fP] [\fB-daemonize\fP]
 [\fB-display-client name\fP] [\fB-host machine\fP] [\fB-port #\fP] [\fB-name name\fP] [\fB-utstart1 yyyy:ddd:hh:mm:ss] [\fB-utend1 yyyy:ddd:hh:mm:ss] [\fB-utstart2 yyyy:ddd:hh:mm:ss] [\fB-utend2 yyyy:ddd:hh:mm:ss] [....upto 6 time intervals] [\fB-read-from-stdin\fP]
.SH DESCRIPTION
The \fIhexteRtClient\fP process is used primarily as the interface between the
realtime server and the HEXTE display clients for Health and Safety and Science Monitoring.
It collects data packets of the type specified by the -name option until it has 
enough to assemble a 16-second partition, which it then sends to the display client.
.PP
.SH OPTIONS
Options may appear in any order on the command line.
.TP 10
.B -display-client  name
This is the name of X client program to execute for graphical display.  The 
choices are as follows:
  hexteScM1:  Event and Burst Lists
  hexteScM2:  Histogram and Multiscalar Binning
  hexteArchSpectra: Archive Histogram
  hexteArchLiteCrv: Archive Multiscalar Binning
  HxHnSDisplays:  Health and Safety
.EL
.TP
.B -host machine
This is the name of the realtime server machine.
.TP
.B -port #
This is the port number to connect to realtime server.
.TP
.B -name name
This is the name we advertise to the realtime server to indicate which kind of 
data we would like to receive.  The choices are HEXTE-HK for housekeeping
data (to be used with the Health and Safety and Archive display clients) and
HEXTE-Science for science data (to be used with the other Science Monitoring 
display clients).
.TP
.B -utstart1 -utend1 ... -utstart6 -utend6
16 second data partitions are accepted if they have a start time falling within
one of these start/end pairs, that is the start time of the partition is >=
utstartN and < utendN, N = 1 to 6.
.TP
.B -read-from-stdin
Read from stdin instead of over a socket.  Used mainly for testing and debugging.
.TP
.B -ascii
Produce ASCII output to stdout.  For each 16-second partition, the display will
show the application ID, the cluster number, and the time, followed by a tabular 
output of the data.  This is used mainly for testing and debugging. 
.TP
.B -daemonize
Run as a daemon.
.SH EXAMPLES
Collect HEXTE Housekeeping telemetry data from a realtime server running on xitserv
and put up an Archive Histogram (Energy Spectra) display:

hexteRtClient -host xitserv.gsfc.nasa.gov -port 5094 -name HEXTE-HK -display-client hexteArchSpectra
.PP
Collect HEXTE Science telemetry data from a realtime server running on xitserv
and put up an Event List display:  

hexteRtClient -host xitserv.gsfc.nasa.gov -port 5094 -name HEXTE-Science -display-client hexteScM1
.SH AUTHOR
Richard Bentley, UCSD/CASS, rbentley@@mamacass.ucsd.edu
@


1.1
log
@Initial revision
@
text
@d2 1
a2 1
.\"  $Id: hexteRtClient.man1,v 1.1 1994/05/16 04:39:37 rbentley Exp $
d10 1
a10 1
 [\fB-display-client name\fP] [\fB-host machine\fP] [\fB-port #\fP] [\fB-name name\fP] [\fB-read-from-stdin\fP]
d42 5
@

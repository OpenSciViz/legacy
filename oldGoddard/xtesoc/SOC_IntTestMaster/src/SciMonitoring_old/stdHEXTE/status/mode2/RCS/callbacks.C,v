head	1.5;
access
	dhon
	rbarnett
	rbentley
	socops
	soccm;
symbols
	Build5_0_1:1.5
	Build4_5_1:1.4
	Build4_3_2:1.3
	Build4_1:1.2
	Build4:1.2
	Build3_3:1.2
	Current:1.2
	Build3_2:1.2
	Build3_1:1.1;
locks; strict;
comment	@ * @;


1.5
date	95.12.24.08.33.27;	author rbentley;	state Exp;
branches;
next	1.4;

1.4
date	95.09.23.00.05.52;	author rbentley;	state Exp;
branches;
next	1.3;

1.3
date	95.06.30.22.12.42;	author rbentley;	state Exp;
branches;
next	1.2;

1.2
date	94.09.09.17.02.58;	author rbentley;	state Exp;
branches;
next	1.1;

1.1
date	94.06.29.18.46.43;	author rbentley;	state Exp;
branches;
next	;


desc
@callback functions for HEXTE Science monitoring;
So far, this works for mode 2 table 4 only.  Stay tuned.
@


1.5
log
@added setNewer() for newer data only option
@
text
@// RCS: $Id: callbacks.C,v 1.4 1995/09/23 00:05:52 rbentley Exp $
//
// Callback functions for HEXTE Science Monitoring X-Display client
// CHANGE LOG:
// 26-Jun-94 ucrdb: Changed name of select to selectDet;
//		    Changed args of selectDet to start from 1 intstead of 0;
//		    Set value of clusterIdSelected in selectDet
//
#include <unistd.h>
#include <stdio.h>
#include <rw/rwtime.h>
#include "iostream.h"
#include "strstream.h"
#include "hexteScM2.h"		// all globals, except...
#include "pan_hxScM2.h"		// global TAEPanel* hxScM2P
#include "pan_pickDets.h"	// global TAEPanel* pickDetsP
#include "SciCommon.h"

////////////////////////////////////////////////////////////////////////
void save(void) {
//  cerr << "Entered save callback..." << endl;
// first things first
  int tmp_freeze = isFrozen(hxScM2P); // save original state of freeze
  if( !tmp_freeze )
    toggleFreeze(hxScM2P); // force state to frozen

// now perform the save to the constructed filename:
  RWTime t; // current time
  char *userName = "ascii"; // cuserid(NULL);
  char *winName = "HEXTE Science Mode 2"; // hxScM2P->Title("hxScM2");
  strstream os;
  if( getenv("SOCOPS") != NULL )
    os << getenv("SOCOPS") << "/tmp/" << userName << "." << winName 
      << "." << t;
  else
    os << "'" << "." << "/tmp/" << userName << ".ascii." << winName << "." << t << "'";

  char *fileName = os.str();

  dumpAscii(fileName); // this is defined in graphics.C
  
  delete fileName;
// restore original state of freeze
  if( !tmp_freeze )
    toggleFreeze(hxScM2P); // toggle back
}

////////////////////////////////////////////////////////////////////////
void quit(void) {
  cerr << "Entered quit callback..." << endl;
  exit(0);
}

////////////////////////////////////////////////////////////////////////
void selectDet(int cluster, int detector) {
//  cerr << "Entered select callback..." << cluster << " " << detector << endl;
// for de-sensitizing and sensitizing, set states to False/True

  static char *selectCheckBox[2][4] = 
              { "detI1", "detI2", "detI3", "detI4",
                "detII1", "detII2", "detII3", "detII4" };

  static char *valuesOfCheckBox[2] = { "no", "yes" };

  clusterIdSelected = cluster;	// save this for draw() & showStatus();
// toggle state of the checkbox, this can be used for initial state
  if( selectedDet[cluster-1][detector-1] ) {
    selectedDet[cluster-1][detector-1] = 0;
    hxScM2P->Update(selectCheckBox[cluster-1][detector-1], "no");
//    cerr << "select> De-selected detector: " << cluster << " " << detector << endl;
  }
  else {
    selectedDet[cluster-1][detector-1] = 1;
    hxScM2P->Update(selectCheckBox[cluster-1][detector-1], "yes");
//    cerr << "select> Selected detector: " << cluster << " " << detector << endl;
  }
}

////////////////////////////////////////////////////////////////////////
void selectInt(int cluster, int integration) {
//  cerr << "Entered select callback..." << cluster << " " << integration << endl;
// for de-sensitizing and sensitizing, set states to False/True

  static char *selectCheckBox[2][16] = 
              { "integI1",   "integI2",   "integI3",   "integI4",
                "integI5",   "integI6",   "integI7",   "integI8",
                "integI9",   "integI10",  "integI11",  "integI12",
                "integI13",  "integI14",  "integI15",  "integI16",

                "integII1",  "integII2",  "integII3",  "integII4",
                "integII5",  "integII6",  "integII7",  "integII8",
                "integII9",  "integII10", "integII11", "integII12",
                "integII13", "integII14", "integII15", "integII16",
 };

  static char *valuesOfCheckBox[2] = { "no", "yes" };

  clusterIdSelected = cluster;	// save this for draw() & showStatus();
// toggle state of the checkbox, this can be used for initial state
  if( selectedInt[cluster-1][integration-1] ) {
    selectedInt[cluster-1][integration-1] = 0;
    hxScM2P->Update(selectCheckBox[cluster-1][integration-1], "no");
//    cerr << "select> De-selected integration: " << cluster << " " << integration << endl;
  }
  else {
    selectedInt[cluster-1][integration-1] = 1;
    hxScM2P->Update(selectCheckBox[cluster-1][integration-1], "yes");
//    cerr << "select> Selected integration: " << cluster << " " << integration << endl;
  }
}

////////////////////////////////////////////////////////////////////////
void selectPHA(int cluster, int phaRange) {
//  cerr << "Entered select callback..." << cluster << " " << phaRange << endl;
// for de-sensitizing and sensitizing, set states to False/True

  static char *selectCheckBox[2][9] = 
              { 
                "chkLostEventsA",
                "chkRangeA1"    ,  "chkRangeA2",  "chkRangeA3",  "chkRangeA4",
                "chkRangeA5"    ,  "chkRangeA6",  "chkRangeA7",  "chkRangeA8",

                "chkLostEventsB",
                "chkRangeB1"    ,  "chkRangeB2",  "chkRangeB3",  "chkRangeB4",
                "chkRangeB5"    ,  "chkRangeB6",  "chkRangeB7",  "chkRangeB8",
              };

  static char *valuesOfCheckBox[2] = { "no", "yes" };

  clusterIdSelected = cluster;	// save this for draw() & showStatus();
// toggle state of the checkbox, this can be used for initial state
  if( selectedPHA[cluster-1][phaRange] ) {
    selectedPHA[cluster-1][phaRange] = 0;
    hxScM2P->Update(selectCheckBox[cluster-1][phaRange], "no");
//    cerr << "select> De-selected phaRange: " << cluster << " " << phaRange << endl;
  }
  else {
    selectedPHA[cluster-1][phaRange] = 1;
    hxScM2P->Update(selectCheckBox[cluster-1][phaRange], "yes");
//    cerr << "select> Selected phaRange: " << cluster << " " << phaRange << endl;
  }
}

////////////////////////////////////////////////////////////////////////
void clearAccum(int always) {
  cerr << "Entered clearAccum callback..." << endl;
  if( always && clearAlways) { // toggle global
    clearAlways = 0;
  }
  else if( always ) {
    clearAlways = 1;
  }
  else
    clearNow(); // extern in graphics.C
}

////////////////////////////////////////////////////////////////////////
void ringBell() {
  if( ringBellAlways ) { // toggle
    ringBellAlways = 0;
  }
  else {
    ringBellAlways = 1;
  }
}

/////////////////////////////////////////////////////////////////////
void setNewer() {
  if( newerDataOnly ) { // toggle
    newerDataOnly = 0;
  }
  else {
    newerDataOnly = 1;
  }
}
@


1.4
log
@modified callbacks for detector, integration, and PHA range selection
@
text
@d1 1
a1 1
// RCS: $Id: callbacks.C,v 1.2 1994/09/09 17:02:58 rbentley Exp $
d21 1
a21 1
//  cout << "Entered save callback..." << endl;
d50 1
a50 1
  cout << "Entered quit callback..." << endl;
d146 1
a146 1
  cout << "Entered clearAccum callback..." << endl;
d164 10
@


1.3
log
@added  #include <rw/rwtime.h> as part of solaris port
@
text
@d19 1
d21 1
a21 1
  cout << "Entered save callback..." << endl;
d48 1
d54 1
a54 1
// for now just allow 1
a57 5
  static char *selectCheckBoxState[2][4] = 
              { "Select.DetI1.state", "Select.DetI2.state",
		"Select.DetI3.state", "Select.DetI4.state",
                "Select.DetII1.state", "Select.DetII2.state",
		"Select.DetII3.state", "Select.DetII4.state", };
a64 2
  int newTrue = 0;

a72 1
    newTrue = 1;
a76 10
/* if selection were exclusive, turn off the others
  if( newTrue ) {  
    for( int i = 0; i < 4 ; i++ ) {
      if( i != detector ) {
        selectedDet[cluster-1][detector-1] = 0;
	hxScM2P->Update(selectCheckBox[cluster-1][i], "no");
      }
    }
  }
*/
d79 1
a79 1
// for now just allow 1
a82 18
  static char *selectCheckBoxState[2][16] = 
              { "Select.IntI1.state",  "Select.IntI2.state",
		"Select.IntI3.state",  "Select.IntI4.state",
		"Select.IntI5.state",  "Select.IntI6.state",
		"Select.IntI7.state",  "Select.IntI8.state",
		"Select.IntI9.state",  "Select.IntI10.state",
		"Select.IntI11.state", "Select.IntI12.state",
		"Select.IntI13.state", "Select.IntI14.state",
		"Select.IntI15.state", "Select.IntI16.state",

                "Select.IntII1.state",  "Select.IntII2.state",
		"Select.IntII3.state",  "Select.IntII4.state",
		"Select.IntII5.state",  "Select.IntII6.state",
		"Select.IntII7.state",  "Select.IntII8.state",
		"Select.IntII9.state",  "Select.IntII10.state",
		"Select.IntII11.state", "Select.IntII12.state",
		"Select.IntII13.state", "Select.IntII14.state",
		"Select.IntII15.state", "Select.IntII16.state", };
d85 9
a93 9
              { "intI1",   "intI2",   "intI3",   "intI4",
                "intI5",   "intI6",   "intI7",   "intI8",
                "intI9",   "intI10",  "intI11",  "intI12",
                "intI13",  "intI14",  "intI15",  "intI16",

                "intII1",  "intII2",  "intII3",  "intII4",
                "IntII5",  "IntII6",  "IntII7",  "IntII8",
                "IntII9",  "IntII10", "IntII11", "IntII12",
                "IntII13", "IntII14", "IntII15", "IntII16",
a97 2
  int newTrue = 0;

a105 1
    newTrue = 1;
d112 33
d157 1
a165 8
/*
void print_callback()
{
     AtPlotterGeneratePostscript("out.ps", (Widget) plotter,
				 "Widget Profits", 0, 0, 400, 250, False);
     fprintf(stderr, "Plot dumped to out.ps\n");
}
*/
@


1.2
log
@added selectInt function for selecting integrations
@
text
@d1 1
a1 1
// RCS: $Id: callbacks.C,v 1.1 1994/06/29 18:46:43 rbentley Exp $
d11 1
@


1.1
log
@Initial revision
@
text
@d1 1
a1 1
// RCS: $Id$
d59 1
a59 1
		"Select.DetII3.state", "Select.DetII4.state" };
d94 54
a175 12












@

head	1.1;
access
	jschwan
	soccm;
symbols
	Build5_3:1.1;
locks
	jschwan:1.1; strict;
comment	@ * @;


1.1
date	96.09.10.14.25.35;	author jschwan;	state Exp;
branches;
next	;


desc
@Contains methods previously inlined in SciMonitor.h.
@


1.1
log
@Initial revision
@
text
@// ---------------------------------------------------------------------------
// File Name   : SciMonitor.C
// Subsystem   : SOC Science Monitoring
// Programmer  : Julie Schwan, Hughes STX/NASA GSFC
// Description : This program was created to facilitate functions
//               that were previously inlined in the SciMonitor.h header file.
//
// ----------------------------------------------------------------------------
static const char rcsid[] = "$Id: $";
 
//
//  Program developed for use by the Science Monitoring subsystem
//

//#include <rw/cstring.h>
//#include <rw/dvec.h>

//#include <taepanel.h>
//#include <taeitem.h>

#include "SciMonitor.h"
//#include "SciStream.h"

void SciMonitor::drawStatus(TaeItem* titem, RWCString& rws) {
  titem->Update(rws.data());
}

void SciMonitor::drawStatus(TaeItem* titem, double time, RWCString& rws) {
  strstream s, t;
  t << SOCTime2(time) << '\0';
  RWCString rst(t.str());
  delete t.str();
  rst.remove(rst.first('.')); // eliminates sub-seconds.
  s << rst << "  (MET = " << (int) time << ")" << "  |  " << rws << '\0';
  titem->Update(s.str());
  delete s.str();
}

void SciMonitor::drawStatus(TaeItem* titem, RWCString& rws, double time) {
  strstream s, t;
  t << SOCTime2(time) << '\0';
  RWCString rst(t.str());
  delete t.str();
  rst.remove(rst.first('.')); // eliminates sub-seconds.
  s << rws << "  |  " <<  rst << "  (MET = " << (int) time << ")" << '\0';
  titem->Update(s.str());
  delete s.str();
}

void SciMonitor::barShade(unsigned int s, int barNo) {
  switch(s) {
    case 0: XtVaSetValues(_bar[barNo], XtNshading, AtGRAY0, NULL);
            XtVaSetValues(_bar2[barNo], XtNshading, AtGRAY0, NULL);
            break;
    case 1: XtVaSetValues(_bar[barNo], XtNshading, AtGRAY1, NULL);
            XtVaSetValues(_bar2[barNo], XtNshading, AtGRAY1, NULL);
            break;
    case 2: XtVaSetValues(_bar[barNo], XtNshading, AtGRAY2, NULL);
            XtVaSetValues(_bar2[barNo], XtNshading, AtGRAY2, NULL);
	    break;
    case 3: XtVaSetValues(_bar[barNo], XtNshading, AtGRAY3, NULL);
            XtVaSetValues(_bar2[barNo], XtNshading, AtGRAY3, NULL);
	     break;
    case 4: XtVaSetValues(_bar[barNo], XtNshading, AtGRAY4, NULL);
            XtVaSetValues(_bar2[barNo], XtNshading, AtGRAY4, NULL);
	    break;
    case 5: XtVaSetValues(_bar[barNo], XtNshading, AtGRAY5, NULL);
            XtVaSetValues(_bar2[barNo], XtNshading, AtGRAY5, NULL);
	    break;
    case 6: XtVaSetValues(_bar[barNo], XtNshading, AtGRAY6, NULL);
            XtVaSetValues(_bar2[barNo], XtNshading, AtGRAY6, NULL);
	    break;
    case 7: XtVaSetValues(_bar[barNo], XtNshading, AtGRAY7, NULL);
            XtVaSetValues(_bar2[barNo], XtNshading, AtGRAY7, NULL);
	    break;
    case 8: XtVaSetValues(_bar[barNo], XtNshading, AtGRAY8, NULL);
            XtVaSetValues(_bar2[barNo], XtNshading, AtGRAY8, NULL);
	    break;
    case 9: XtVaSetValues(_bar[barNo], XtNshading, AtGRAY9, NULL);
            XtVaSetValues(_bar2[barNo], XtNshading, AtGRAY9, NULL);
	    break;
    case 10:
    default: XtVaSetValues(_bar[barNo], XtNshading, AtGRAY10, NULL);
             XtVaSetValues(_bar2[barNo], XtNshading, AtGRAY10, NULL);
	     break;
  }
}

SciMonitor::SciMonitor(TaePanel* parent,
		       const char* XworkName,
		       const char* title,
		       short adjustWidth,
		       short adjustHeight,
		       const char *subtitle,
		       const char *xlabel,
		       int deltaXTics,
		       const char *ylabel,
		       int deltaYTics,
		       int numberOfLines,
		       int numberOfBars) {
  _parentPanel = parent; 
  _numLines = numberOfLines;
  _numBars = numberOfBars;
  _deltaXTics = deltaXTics;
  _deltaYTics = deltaYTics;
  _deltaX2Tics = deltaXTics;
  _deltaY2Tics = deltaYTics;

  _title = new char[1+strlen(title)];
  strcpy(_title, title);
  _subtitle = new char[1+strlen(subtitle)];
  strcpy(_subtitle, subtitle);
  _x2label = new char[1+strlen(subtitle)];
  strcpy(_x2label, subtitle);

  _xlabel = new char[1+strlen(xlabel)];
  strcpy(_xlabel, xlabel);
  _ylabel = new char[1+strlen(ylabel)];
  strcpy(_ylabel, ylabel);
  _y2label = new char[1+strlen(ylabel)];
  strcpy(_y2label, " ");

  _plotter = ::createAtPlotter(parent,
			       XworkName,
			       title,
			       adjustWidth,
			       adjustHeight);

  ::initAtPlotter(_plotter, _bar, _bar2, _line, _line2,
		  _xaxis, _x2axis, _yaxis, _y2axis,
		  _subtitle, _xlabel, _ylabel, numberOfLines, numberOfBars);

#ifdef DEBUG
  cerr << "Widgets created:\n" <<
          "plotter " << (unsigned ) _plotter << "\n" <<
          "bar[0] " << (unsigned ) _bar[0] << "\n" <<
          "line[0] " << (unsigned ) _line[0] << "\n" <<
          "xaxis " << (unsigned ) _xaxis << "\n" <<
          "x2axis " << (unsigned ) _x2axis << "\n" <<
          "yaxis " << (unsigned ) _yaxis << "\n" <<
          "y2axis " << (unsigned ) _y2axis << endl;
#endif
}

void SciMonitor::forceXscale(double min, double max, int deltaTics) {
  _deltaXTics = deltaTics;
  XtVaSetValues(_xaxis, XtNautoScale, False, XtNmin, &min, XtNmax, &max, NULL);
}

void SciMonitor::forceX2scale(double min, double max, int deltaTics) {
  _deltaX2Tics = deltaTics;
  XtVaSetValues(_x2axis, XtNautoScale, False, XtNmin, &min, XtNmax, &max, NULL);
}

void SciMonitor::forceYscale(double min, double max, int deltaTics) {
  _deltaYTics = deltaTics;
  XtVaSetValues(_yaxis, XtNautoScale, False, XtNmin, &min, XtNmax, &max, NULL);
}

void SciMonitor::forceY2scale(double min, double max, int deltaTics) {
  _deltaY2Tics = deltaTics;
  XtVaSetValues(_y2axis, XtNautoScale, False, XtNmin, &min, XtNmax, &max, NULL);
}

void SciMonitor::quit() {
  pid_t ppid;
  if( ppid = parentpid() == 0 )
    ppid = getppid();

  kill(ppid, SIGTERM);
}

int SciMonitor::blockInput(int bl) {
  return ::blockInput(bl); // use SciStream.h function
}

XtInputId SciMonitor::setIOhandler(void (*handler)(), int fd) {
  _XinputId = ::setIOhandler(_parentPanel, handler, fd);
  return _XinputId;
}

void SciMonitor::toggleFreeze() {
  ::toggleFreeze(_parentPanel);
}

int SciMonitor::isFrozen() {
  return ::isFrozen(_parentPanel);
}

void SciMonitor::snapShot(const char* panelName) {
  ::snapShot(_parentPanel, panelName);
}

void SciMonitor::setCursor() {
  ::setCursor(_parentPanel);
}

void SciMonitor::unsetCursor() {
  ::unsetCursor(_parentPanel);
}

void SciMonitor::ringBell() {
  XBell(XtDisplay(_plotter), 1);
}

void SciMonitor::postScript(char* fileName) {
  char *name = (char *)_parentPanel->Name();
  RWCString tmp(name);
  if( fileName == NULL ) {
    char ranchar[5]; // random ascii values 97-122 (a-z)
    ranchar[4] = '\0';
    for( int ic = 0; ic < 4; ic++ ) {
#ifdef sun
      ranchar[ic] = (char)(97.0 + 25.0*(rand() / (65536.*65536.-1.0)));
#endif
#ifdef sgi
      ranchar[ic] = (char)(97.0 + 25.0*(rand() / 32767.0));
#endif
    }
    tmp.prepend("./");
    tmp.append(".");
    tmp.append(ranchar);
    tmp.append(".ps");
    fileName = (char *)tmp.data();
  }
  int landscape = 1;
  int lowerLeftX = 50;
  int lowerLeftY = 50;
  int upperRightX = 725;
  int upperRightY = 525;
//  Dimension width, height;
//  XtVaGetValues(_plotter, XtNwidth, &width, XtNheight, &height, NULL);  
  AtPlotterGeneratePostscript(fileName, _plotter, name,
			      lowerLeftX, lowerLeftY,
			      upperRightX, upperRightY,
			      landscape);
}

void SciMonitor::drawLine(const DoubleVec *x, const DoubleVec *y, int lineNo, int nP, int offset) {
  //blockInput(1);
  //give line curves slightly higer rank order than bars
  XtVaSetValues(_line[lineNo], XtNrankOrder, 1 + MAX_CURVES - lineNo, NULL);

  DoubleVec v = *x;
  double min = minValue(v);
  double max = maxValue(v);
  double interval = (max - min) / _deltaXTics; // v[_deltaXTics] - v[0]; // length had better > 4!
  interval = ceil(interval);
  XtVaSetValues(_xaxis, XtNmin, &min, XtNmax, &max,
		XtNticInterval, &interval, NULL);
  
  double scmin, scmax; // check the current scale min/max
  XtVaGetValues(_yaxis, XtNmin, &scmin, XtNmax, &scmax, NULL);
  DoubleVec w = *y;
  min = minValue(w);
  max = maxValue(w);
  if( min != scmin ) min = scmin ;
  if( max != scmax ) max = scmax ;
  interval = (max - min) / _deltaYTics; // w[_deltaYTics] - w[0]; // length had better > 4!
  interval = ceil(interval);
  XtVaSetValues(_yaxis, XtNmin, &min, XtNmax, &max,
		XtNticInterval, &interval, NULL);

  if (nP == 0) nP = x->length() ;

#ifdef DEBUG
  cerr << "drawLine> x min, max, interval: " << min << " " << max << 
          " " << interval << endl;
#endif
  showLine(lineNo);
  AtXYLinePlotAttachData( _line[lineNo],
			 (XtPointer) x->data(), AtDouble, sizeof(double),
			 (XtPointer) y->data(), AtDouble, sizeof(double),
			  1+offset, nP );
}

void SciMonitor::drawBar(const DoubleVec *x, const DoubleVec *y, int barNo, int nP, int offset) {
  //blockInput(1);
  // give bar curves slightly lower rank order than lines 
  XtVaSetValues(_bar[barNo], XtNrankOrder, MAX_CURVES - barNo, NULL);

  DoubleVec v = *x;
  double min = minValue(v);
  double max = maxValue(v);
  double interval = (max - min) / _deltaXTics; // v[_deltaXTics] - v[0]; // length had better > 4!
  interval = ceil(interval);
  XtVaSetValues(_xaxis, XtNmin, &min, XtNmax, &max,
		XtNticInterval, &interval, NULL);
 
  double scmin, scmax; // check the current scale min/max
  XtVaGetValues(_yaxis, XtNmin, &scmin, XtNmax, &scmax, NULL);
  DoubleVec w = *y;
  min = minValue(w);
  max = maxValue(w);
  if( min != scmin ) min = scmin ;
  if( max != scmax ) max = scmax ;
  interval = (max - min) / _deltaYTics; // w[_deltaYTics] - w[0]; // length had better > 4!
  interval = ceil(interval);
  XtVaSetValues(_yaxis, XtNmin, &min, XtNmax, &max,
		XtNticInterval, &interval, NULL);

  if (nP == 0) nP = x->length() ;

#ifdef DEBUG
  cerr << "drawBar> x min, max, interval: " << min << " " << max <<
          " " << interval << endl;
#endif
  showBar(barNo);

  AtXYBarPlotAttachData( _bar[barNo],
			 (XtPointer) x->data(), AtDouble, sizeof(double),
			 (XtPointer) y->data(), AtDouble, sizeof(double),
			  1+offset, nP );
}

void SciMonitor::drawLine2(const DoubleVec *x, const DoubleVec *y, int lineNo, int nP, int offset) {
  XtVaSetValues(_line[lineNo], XtNrankOrder, 1 + MAX_CURVES - lineNo, NULL);

  DoubleVec v = *x;
  double min = minValue(v);
  double max = maxValue(v);
  double interval = (max - min) / _deltaXTics; // v[_deltaXTics] - v[0]; // length had better > 4!
  interval = ceil(interval);
  XtVaSetValues(_x2axis, XtNmin, &min, XtNmax, &max,
		XtNticInterval, &interval, NULL);
  
  double scmin, scmax; // check the current scale min/max
  XtVaGetValues(_y2axis, XtNmin, &scmin, XtNmax, &scmax, NULL);
  DoubleVec w = *y;
  min = minValue(w);
  max = maxValue(w);
  if( min != scmin ) min = scmin ;
  if( max != scmax ) max = scmax ;
  interval = (max - min) / _deltaYTics; // w[_deltaYTics] - w[0]; // length had better > 4!
  interval = ceil(interval);
  XtVaSetValues(_y2axis, XtNmin, &min, XtNmax, &max,
		XtNticInterval, &interval, NULL);


  if (nP == 0) nP = x->length() ;

#ifdef DEBUG
  cerr << "drawLine> x min, max, interval: " << min << " " << max << 
          " " << interval << endl;
#endif
  showLine2(lineNo);

  AtXYLinePlotAttachData( _line2[lineNo],
			 (XtPointer) x->data(), AtDouble, sizeof(double),
			 (XtPointer) y->data(), AtDouble, sizeof(double),
			  1+offset, nP );
}

void SciMonitor::drawBar2(const DoubleVec *x, const DoubleVec *y, int barNo, int nP, int offset) {
  XtVaSetValues(_bar[barNo], XtNrankOrder, MAX_CURVES - barNo, NULL);

  DoubleVec v = *x;
  double min = minValue(v);
  double max = maxValue(v);
  double interval = (max - min) / _deltaXTics; // v[_deltaXTics] - v[0]; // length had better > 4!
  interval = ceil(interval);
  XtVaSetValues(_x2axis, XtNmin, &min, XtNmax, &max,
		XtNticInterval, &interval, NULL);
  
  double scmin, scmax; // check the current scale min/max
  XtVaGetValues(_y2axis, XtNmin, &scmin, XtNmax, &scmax, NULL);
  DoubleVec w = *y;
  min = minValue(w);
  max = maxValue(w);
  if( min != scmin ) min = scmin ;
  if( max != scmax ) max = scmax ;
  interval = (max - min) / _deltaYTics; // w[_deltaYTics] - w[0]; // length had better > 4!
  XtVaSetValues(_y2axis, XtNmin, &min, XtNmax, &max,
		XtNticInterval, &interval, NULL);

  if (nP == 0) nP = x->length() ;

#ifdef DEBUG
  cerr << "drawBar> x min, max, interval: " << min << " " << max <<
          " " << interval << endl;
#endif
  showBar2(barNo);

  AtXYBarPlotAttachData( _bar2[barNo],
			 (XtPointer) x->data(), AtDouble, sizeof(double),
			 (XtPointer) y->data(), AtDouble, sizeof(double),
			  1+offset, nP );
  //XFlush(XtDisplay(_plotter));
  //blockInput(0);
}

void SciMonitor::setColor(const char *color, Widget w) {
  XtVaSetValues(w, XtVaTypedArg,
		XtNforeground, XtRString, (String )color, 1+strlen(color),
		NULL);
}

void SciMonitor::lineColor(const char *color, int lineNo, int axis) {
  Widget w;
  if( axis <= 1 ) 
    w = _line[lineNo];
  else
    w = _line2[lineNo];

  setColor(color, w);
}

void SciMonitor::barColor(const char *color, int barNo, int axis) {
  Widget w;
  if( axis <= 1 ) 
    w = _bar[barNo];
  else
    w = _bar2[barNo];

  setColor(color, w);
}

void SciMonitor::resetTitle(const char* title) {
  XtVaSetValues(_plotter,
		XtNtitle, title,
		NULL);  
}

void SciMonitor::resetX1Y1Axes(const char* x1label, const char* y1label,
			  int deltaX1Tics, int deltaY1Tics) {
  int ticsAndNums = 0;
  if( deltaX1Tics ) {
    ticsAndNums = 1;
    _deltaXTics = deltaX1Tics;
  }
  if( deltaY1Tics ) {
    ticsAndNums = 1;
    _deltaYTics = deltaY1Tics;
  }

  if( x1label ) {
    delete _xlabel;
    _xlabel = new char[1+strlen(x1label)];
    strcpy(_xlabel, x1label);

    XtVaSetValues(_xaxis,
	          XtNlabel, _xlabel,
		  XtNdrawTics, ticsAndNums,
		  XtNdrawNumbers, ticsAndNums,
		  XtNdrawGrid, False,
		  XtNdrawSubgrid, False,
		  XtNautoTics, False,
		  XtNautoSubtics, False,
		  XtNticsInside, False,
		  XtNticsOutside, True,
		  XtNnumbersOutside, True,
		  NULL);
    // Now re-attach axis to the parent 
    XtVaSetValues(_plotter,
                  XtNxAxis, _xaxis,
                  NULL);
  }
	
  if( y1label ) {
    delete _ylabel;
    _ylabel = new char[1+strlen(y1label)];
    strcpy(_ylabel, y1label);

    XtVaSetValues(_yaxis,
	          XtNlabel, _ylabel,
		  XtNdrawTics, ticsAndNums,
		  XtNdrawNumbers, ticsAndNums,
		  XtNdrawGrid, False,
		  XtNdrawSubgrid, False,
		  XtNautoTics, False,
		  XtNautoSubtics, False,
		  XtNticsInside, False,
		  XtNticsOutside, True,
		  XtNnumbersOutside, True,
		  NULL);
    XtVaSetValues(_plotter,
                  XtNyAxis, _yaxis,
                  NULL);
  }
}

void SciMonitor::resetX2Y2Axes(const char* x2label, const char* y2label,
			  int deltaX2Tics, int deltaY2Tics) {
  _deltaX2Tics = deltaX2Tics;
  _deltaY2Tics = deltaY2Tics;

  int ticsAndNums = 0;
  if( deltaX2Tics ) {
    ticsAndNums = 1;
    _deltaX2Tics = deltaX2Tics;
  }
  if( deltaY2Tics ) {
    ticsAndNums = 1;
    _deltaY2Tics = deltaY2Tics;
  }

  if( x2label ) {
    delete _x2label;
    _x2label = new char[1+strlen(x2label)];
    strcpy(_x2label, x2label);

    XtVaSetValues(_x2axis,
	          XtNlabel, _x2label,
		  XtNdrawTics, ticsAndNums,
		  XtNdrawNumbers, ticsAndNums,
		  XtNdrawGrid, False,
		  XtNdrawSubgrid, False,
		  XtNautoTics, False,
		  XtNautoSubtics, False,
		  XtNticsInside, False,
		  XtNticsOutside, True,
		  XtNnumbersOutside, True,
		  NULL);
    // Now re-attach axis to the parent 
    XtVaSetValues(_plotter,
                  XtNx2Axis, _x2axis,
                  NULL);
  }
  if( y2label ) {
    delete _x2label;
    _y2label = new char[1+strlen(y2label)];
    strcpy(_y2label, y2label);
    XtVaSetValues(_y2axis,
	          XtNlabel, _y2label,
		  XtNdrawTics, ticsAndNums,
		  XtNdrawNumbers, ticsAndNums,
		  XtNdrawGrid, False,
		  XtNdrawSubgrid, False,
		  XtNautoTics, False,
		  XtNautoSubtics, False,
		  XtNticsInside, False,
		  XtNticsOutside, True,
		  XtNnumbersOutside, True,
		  NULL);
    // Now re-attach axis to the parent 
    XtVaSetValues(_plotter,
                  XtNy2Axis, _y2axis,
                  NULL);
  }
}
@

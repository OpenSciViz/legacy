head	1.5;
access
	jschwan
	soccm;
symbols
	Build5_3:1.5;
locks; strict;
comment	@ * @;


1.5
date	96.09.23.21.47.37;	author jschwan;	state Exp;
branches;
next	1.4;

1.4
date	96.09.23.21.45.14;	author jschwan;	state Exp;
branches;
next	1.3;

1.3
date	96.09.17.14.35.49;	author jschwan;	state Exp;
branches;
next	1.2;

1.2
date	96.09.12.15.27.08;	author jschwan;	state Exp;
branches;
next	1.1;

1.1
date	96.09.05.18.15.09;	author jschwan;	state Exp;
branches;
next	;


desc
@This file contains previously inlined functions from SciXImage.h
@


1.5
log
@*** empty log message ***
@
text
@// ---------------------------------------------------------------------------
// File Name   : SciXImage.C
// Subsystem   : SOC Science Monitoring
// Programmer  : Julie Schwan, Hughes STX/NASA GSFC
// Description : This program was created to facilitate functions
//               that were previously inlined in the SciXImage.h header file.
//		 This file contains functions which implement drawing the
//		 color maps.
//
// ----------------------------------------------------------------------------
static const char rcsid[] = "$Id: SciXImage.C,v 1.4 1996/09/23 21:45:14 jschwan Exp jschwan $";

//
//  Program developed for use by the Science Monitoring subsystem
//

#include "rw/ucgenmat.h"
#include "SciXImage.h"

void SciXImage::saveOn(RWvostream& vos) const {
  UCharGenMat::saveOn(vos);
}

void SciXImage::restoreFrom(RWvistream& vis) {
  UCharGenMat::restoreFrom(vis);
}

SOCbostream& operator<<(SOCbostream& bos, const SciXImage& sim) { 
  sim.saveOn(bos.rwstrm());
  return bos;
}

SOCbistream& operator>>(SOCbistream& bis, SciXImage& sim) { 
  sim.restoreFrom(bis.rwstrm());
  return bis;
}

//int SciXImage::readGlobeFile() {
//  int retVal = -1;
//  ifstream ifs("./GlobeElev512x256.RWUCharGenMat.rwbstream");
//  if( ifs.fail() ) { // try sochome/etc
//    RWCString file = "/etc/GlobeElev512x256.RWUCharGenMat.rwbstream";
//    char* sochm = getenv("SOCHOME");
//    if( !sochm ) {
//      file.prepend(sochm);
//      ifs.open(file.data());
//    }
//  }
//  if( ! ifs.fail() ) {
//    RWbistream bis(ifs);
//    bis >> *this;
//    retVal = (int) bis.good();
//  }
//  return retVal;
//}

DoubleVec SciXImage::linearScale(UCharVec& ucv, const DoubleVec& indata) {
  DoubleVec minmax(2, 0.0);
  minmax[0] = minValue(indata);
  minmax[1] = maxValue(indata);
  double diff = minmax[1] - minmax[0];

  ucv.resize(indata.length());

  for( int i = 0; i < indata.length(); i++ ) {
    ucv[i] = (unsigned char ) (_scmap->entries()-1) * (indata[i]-minmax[0])/diff;
    //cerr << "linearScale> i, power, colorCell: " << i << ", " << indata[i] 
    //     << ", " << (int) ucv[i] << endl;
  }
  return minmax;
}

DoubleVec SciXImage::logScale(UCharVec& ucv, const DoubleVec& indata){
  DoubleVec minmax(2, 0.0);
  minmax[0] = minValue(indata);
  if( minmax[0] < 1.0 )
    minmax[0] = 1.0;
  minmax[1] = minmax[0] + maxValue(indata);
  double lgmn = log(minmax[0]);
  double diff = log(minmax[1]) - lgmn;

  ucv.resize(indata.length());

  for( int i=0; i < indata.length(); i++ ) {
    ucv[i] = (unsigned char ) (_scmap->entries()-1) * (log(minmax[0]+indata[i])-lgmn)/diff;
    //cerr << "linearScale> i, power, colorCell: " << i << ", " << indata[i] 
    //     << ", " << (int) ucv[i] << endl;
  }
  return minmax;
}

//
// the scanline data should be within the range of colorCell[0]
// to colorCell[_scmap->entries() - 1]
//
void SciXImage::draw(int xsrc, int ysrc, int xdst, int ydst, Window win) {
  //reset state of scanLines Vec from current state of matrix:
  if( _scanLines->length() > 0 ) {
    for( int rowNum = 0; rowNum < rows(); rowNum++ ) {
      //cerr << "SciXImage::draw> matrix dim.: " << rows() << " " << cols() << endl;
      //cerr << "SciXImage::draw> scanline Vec dim.: " << _scanLines->length() << endl;
      //cerr << "SciXImage::draw> set row# " << rowNum << " width= " << _width << endl;
      UCharVec viewRow = row(rowNum);
      UCharVec viewScan = _scanLines->slice((rowNum*_zoom)*_width, _zoom*_width);
      //cerr << "SciXImage::draw> zoom= " << _zoom 
      //     << " viewScan= " << viewScan.length()
      //     << " viewRow " << viewRow.length() << endl;
      if( _zoom == 1 ) {
	for( int colNum = 0; colNum < cols(); colNum++ )
	  viewScan[colNum] = (unsigned char) colorCellPixelAt(viewRow[colNum]);
      }
      else { // replicate
        //cerr << "SciXImage::draw> viewRepScan len: " << viewRepScan.length() << endl;
        for( int zoomRow = 0; zoomRow < _zoom; zoomRow++ ) {
          UCharVec viewRepScan = viewScan.slice(zoomRow*_width, _width);
	  for( int colNum = 0, elem = 0; elem < _width; elem++, colNum = elem/_zoom ) {
            viewRepScan[elem] = (unsigned char) colorCellPixelAt(viewRow[colNum]);
	  //cerr << "SciXImage::draw> viewRepScan elem: " << elem << endl;
	  }
	}
      }
    }
    Window w; // allow alternate window for target draw:
    if( win )
      w = win;
    else
      w = XtWindow(_widget);

    XPutImage(_scmap->_display, w, _gc, _ximage,
              xsrc, xdst, ysrc, ydst, _width, _height);
    //XFlush(_scmap->_display);
  }
}

void SciXImage::drawCMap(Window win) {
  // --- debug comment ---
  cerr << "Entering drawCMap()...\n";

  // sets zoom to 1:
  _zoom = 1;
  // sets internal image data to linear ramp for sampling all color cells:
  int hratio = 1;
  if( _height > _scmap->entries() )
    hratio  = _height / (_scmap->entries()-1);
  else
    cerr << "SciXImage.drawCMap> insufficient pixel height for CMAP, clipping image..." << endl;

  int ir = rows() - 1;
  int il = 0;
  int val = _scmap->entries() - 1;

  //This draws in the colors for the colorWork widget row by row
  //The  subtraction of 1 from each is how we have reversed the color contour bar

  while( ir >= 0 && val >= 0 )
  {
    row(il) = val = (unsigned char) (ir / hratio);
    //cerr << "row " << ir << " set to: " << ir/hratio
    //     << " : #rows " << rows() << " : entries() " << _scmap->entries()
    //     << endl;
    ir--;
    il++;
  }

  /*    
  for( int ir = 0, val = 0; ir < rows() && val < _scmap->entries(); ir++ ) {
    row(ir) = val = (unsigned char) (ir / hratio);
    //cerr << "row " << ir << " set to: " << ir/hratio << endl;
  }
  */

  draw(0, 0, 0, 0, win);
}

void SciXImage::resetData(const UCharGenMat& gm) {
  UCharGenMat& tmp = *this;
  if( this != &gm ) {
    _width = _zoom*gm.cols();
    _height = _zoom*gm.rows();
    tmp.resize(_height, _width);
    tmp = gm;
  }
}

void SciXImage::_create(Dimension width, Dimension height) {
  _visual = DefaultVisual(_scmap->_display, _scmap->_screenNum);
  _gc = XCreateGC(_scmap->_display, XtWindow(_widget), 0, 0);
  _ximage = 0;
  if( width == 0 ) {
    XtVaGetValues(_widget, XtNwidth, &width, NULL);
  }
  _width = width;

  if( height == 0 ) {
    XtVaGetValues(_widget, XtNheight, &height, NULL);
  }
  _height = height;

  _scanLines = new UCharVec(_width*_height, 0);

  cerr << "SciXImage::_create> w, h = " << _width << ", " << _height << endl;
  
  resize(_height/_zoom, _width/_zoom);

  _ximage = XCreateImage(_scmap->_display, _visual, 8,  ZPixmap, 0,
			 (char*)_scanLines->data(), _width, _height, 8, _width);
}

SciXImage::SciXImage(Window win, const SciXCMap* cmap, int zoom,
	             Dimension width, Dimension height) :
                     UCharGenMat() {
  _scmap = (SciXCMap*) cmap;
  _widget = XtWindowToWidget(_scmap->_display, win);
  _zoom = zoom;
  _create(width, height);
}

SciXImage::SciXImage(Widget wgt, const SciXCMap* cmap, int zoom,
	             Dimension width, Dimension height) :
                     UCharGenMat() {
  _scmap = (SciXCMap* ) cmap;
  _widget = wgt;
  _zoom = zoom;
  _create(width, height);
}

SciXImage::SciXImage(const SciXImage& rhs) : 
           UCharGenMat((const UCharGenMat&) rhs) {
  if( &rhs != this ) { // shallow copy
    _visual = rhs._visual; 
    _depth = rhs._depth; 
    _gc = rhs._gc;
    _bitmapPad = rhs._bitmapPad;
    _bytesPerLine = rhs._bytesPerLine;
    _ximage = rhs._ximage;
    _widget = rhs._widget;
    _height = rhs._height, 
    _width = rhs._width;
    _scanLines = rhs._scanLines;
    _scmap = rhs._scmap;
    _zoom = rhs._zoom;
 }
}
 
SciXImage& SciXImage::operator=(const UCharGenMat& rhs) {
  this->resize(rhs.rows(), rhs.cols());
  UCharGenMat *tmp = (UCharGenMat *) this;
  *tmp = (UCharGenMat& ) rhs;

  return *this;
}

SciXImage& SciXImage::operator=(const SciXImage& rhs) {
  if( &rhs != this ) { 
    this->resize(rhs.rows(), rhs.cols());
    UCharGenMat *tmp = (UCharGenMat *) this;
    *tmp = (UCharGenMat& ) rhs;
    _visual = rhs._visual; 
    _depth = rhs._depth; 
    _gc = rhs._gc;
    _bitmapPad = rhs._bitmapPad;
    _bytesPerLine = rhs._bytesPerLine;
    _ximage = rhs._ximage;
    _widget = rhs._widget;
    _height = rhs._height, 
    _width = rhs._width;
    _scanLines = rhs._scanLines;
    _scmap = rhs._scmap;
    _zoom = rhs._zoom;
  }
  return *this;
}

void SciXImage::append(const UCharVec& v) {
  resize(rows(), 1+cols());
  col(cols()-1) = v;
}

void SciXImage::prepend(const UCharVec& v) {
  resize(rows(), 1+cols()); // add a col
  for( int i = cols()-1; i > 0; i-- ) // shift right
    col(i) = col(i-1);
  col(0) = v; // set first
}

void SciXImage::shiftForNew(const UCharVec& v) {
  for( int i = 1; i < cols(); i++ ) // shift left
    col(i-1) = col(i);

  col(cols() - 1) = v; // insert new data from v
}

void SciXImage::shiftForOld(const UCharVec& v) {
  for( int i = cols()-1; i > 0; i-- ) // shift right 
    col(i) = col(i-1);

  col(0) = v;
}

void SciXImage::wrapLeft() {
  UCharVec v = col(0);
  for( int i = 1; i < cols(); i++ ) // shift left
    col(i-1) = col(i);

  col(cols() - 1) = v; // insert new data from v
}
@


1.4
log
@*** empty log message ***
@
text
@d7 2
d11 1
a11 1
static const char rcsid[] = "$Id: SciXImage.C,v 1.3 1996/09/17 14:35:49 jschwan Exp jschwan $";
@


1.3
log
@*** empty log message ***
@
text
@d9 1
a9 1
static const char rcsid[] = "$Id: SciXImage.C,v 1.2 1996/09/12 15:27:08 jschwan Exp jschwan $";
d156 3
a158 3
    cerr << "row " << ir << " set to: " << ir/hratio
         << " : #rows " << rows() << " : entries() " << _scmap->entries()
         << endl;
@


1.2
log
@Commented out readGlobeFile() function - incomplete.
@
text
@d9 1
a9 1
static const char rcsid[] = "$Id: SciXImage.C,v 1.1 1996/09/05 18:15:09 jschwan Exp jschwan $";
d134 3
d145 19
a163 1
    
d168 1
@


1.1
log
@Initial revision
@
text
@d9 2
a10 10
static const char rcsid[] = "$Id: $";
//
// .NAME    SciXImage.C
// .LIBRARY 
// .HEADER  SOC Science Monitoring
// .INCLUDE SciXImage.h
// .FILE    SciXImage.C
// .AUTHOR  XTE-SOC 
// ----------------------------------------------------------------------------
 
d15 1
d18 2
a19 17
int SciXImage::readGlobeFile() {
  int retVal = -1;
  ifstream ifs("./GlobeElev512x256.RWUCharGenMat.rwbstream");
  if( ifs.fail() ) { // try sochome/etc
    RWCString file = "/etc/GlobeElev512x256.RWUCharGenMat.rwbstream";
    char* sochm = getenv("SOCHOME");
    if( !sochm ) {
      file.prepend(sochm);
      ifs.open(file.data());
    }
  }
  if( ! ifs.fail() ) {
    RWbistream bis(ifs);
    bis >> *this;
    retVal = (int) bis.good();
  }
  return retVal;
d22 33
d56 4
a59 1
  diff = minmax[1] - minmax[0];
@

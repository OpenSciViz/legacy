head	4.11;
access
	dhon
	jschwan
	soccm;
symbols
	Build4_4:4.11
	Build4_3_1:4.11
	Build4_1:4.11
	Build3:4.10
	Build4:4.11
	Current:4.11;
locks
	dhon:4.11; strict;
comment	@ * @;


4.11
date	94.12.14.21.19.18;	author dhon;	state Exp;
branches;
next	4.10;

4.10
date	94.12.14.20.03.31;	author dhon;	state Exp;
branches;
next	4.9;

4.9
date	94.12.10.21.06.39;	author dhon;	state Exp;
branches;
next	4.8;

4.8
date	94.12.05.23.15.55;	author dhon;	state Exp;
branches;
next	4.7;

4.7
date	94.11.07.18.42.15;	author dhon;	state Exp;
branches;
next	4.6;

4.6
date	94.11.07.18.36.58;	author dhon;	state Exp;
branches;
next	4.5;

4.5
date	94.10.14.14.23.45;	author dhon;	state Exp;
branches;
next	4.4;

4.4
date	94.10.14.13.40.32;	author dhon;	state Exp;
branches;
next	4.3;

4.3
date	94.10.14.12.32.13;	author dhon;	state Exp;
branches;
next	4.2;

4.2
date	94.10.14.12.30.22;	author dhon;	state Exp;
branches;
next	4.1;

4.1
date	94.10.13.22.05.38;	author dhon;	state Exp;
branches;
next	;


desc
@@


4.11
log
@*** empty log message ***
@
text
@// RCS: "$Id: ExpectCounts.h,v 4.10 1994/12/14 20:03:31 dhon Exp dhon $"
#ifndef __EXPECTCOUNTS_H__
#define __EXPECTCOUNTS_H__

#include <stdlib.h>
#include <iostream.h>
#include <strstream.h>
#include <rw/collstr.h>
#include <rw/cstring.h>

class ExpectCounts: public RWCollectableString {

public:
  ExpectCounts();
  ExpectCounts(const char*);
  ExpectCounts(const RWCString&);
  ExpectCounts(const RWCollectableString&);
  ExpectCounts(const ExpectCounts&);

  RWCString obsId() const;
  RWCString pcaCountString() const;
  RWCString hexteCountString() const;
  RWCString targetString() const;

  void pcaKevAll(double[6]) const;
  double pcaKev0To3p2() const;
  double pcaKev3p2To4p3() const;
  double pcaKev4p3To5p7() const;
  double pcaKev5p7To8p6() const;
  double pcaKev8p6To12p1() const;
  double pcaKev12p1To60() const;

  void hexteKevAll(double[4]) const;
  double hexteKev15To30() const;
  double hexteKev30To60() const;
  double hexteKev60To125() const;
  double hexteKev125To250() const;
};

inline ExpectCounts::ExpectCounts(): RWCollectableString() {}

inline ExpectCounts::ExpectCounts(const char* s): RWCollectableString(s) {}

inline ExpectCounts::ExpectCounts(const RWCString& s): RWCollectableString(s) {}

inline ExpectCounts::ExpectCounts(const RWCollectableString& s): RWCollectableString(s) {}

inline ExpectCounts::ExpectCounts(const ExpectCounts& ec): RWCollectableString(ec.data()) {}


inline RWCString ExpectCounts::obsId() const {
  RWCString retVal = *this;
  // the expected format is: 
  // "observationID PCR c1 c2 c3 c4 c5 c6 HCR c1 c2 c3 c4"
  size_t pca = index("PCR");
  retVal = retVal(0, pca-1);
  return retVal;
}

inline void ExpectCounts::pcaKevAll(double counts[6]) const {
  const RWCString &s = *this;
  // the expected format is: 
  // "observationID PCR c1 c2 c3 c4 c5 c6 HCR c1 c2 c3 c4"
  size_t pca = s.index("PCR");
  size_t hexte = s.index("HCR");

  RWCString pcaCounts = s(pca, hexte-1);

  size_t _c = pcaCounts.index(" ", 0);
  size_t c_ = pcaCounts.index(" ", _c+1);
  RWCString c = pcaCounts(_c, c_);
  counts[0] = atof(c.data());
#ifdef DEBUG
  cerr << 0 << " " << _c << " " << c_ << " " << counts[0] << endl;
#endif
  for( int i = 1; i < 6; i++ ) {
    _c = c_;
    c_ = pcaCounts.index(" ", _c+1);
    c = pcaCounts(_c, c_);
    counts[i] = atof(c.data());
#ifdef DEBUG
    cerr << i << " " << _c << " " << c_ << " " << counts[i] << endl;
#endif
  }
}

inline double ExpectCounts::pcaKev0To3p2() const {
  double *counts = new double[6];
  pcaKevAll(counts);
  double retVal = counts[0];
  delete counts;
  return retVal;
}

inline double ExpectCounts::pcaKev3p2To4p3() const {
  double *counts = new double[6];
  pcaKevAll(counts);
  double retVal = counts[1];
  delete counts;
  return retVal;
}

inline double ExpectCounts::pcaKev4p3To5p7() const {
  double *counts = new double[6];
  pcaKevAll(counts);
  double retVal = counts[2];
  delete counts;
  return retVal;
}

inline double ExpectCounts::pcaKev5p7To8p6() const {
  double *counts = new double[6];
  pcaKevAll(counts);
  double retVal = counts[3];
  delete counts;
  return retVal;
}

inline double ExpectCounts::pcaKev8p6To12p1() const {
  double *counts = new double[6];
  pcaKevAll(counts);
  double retVal = counts[4];
  delete counts;
  return retVal;
}

inline double ExpectCounts::pcaKev12p1To60() const {
  double *counts = new double[6];
  pcaKevAll(counts);
  double retVal = counts[5];
  delete counts;
  return retVal;
}

inline void ExpectCounts::hexteKevAll(double counts[4]) const {
  const RWCString &s = *this;
  // the expected format is: 
  // "observationID PCR c1 c2 c3 c4 c5 c6 HCR c1 c2 c3 c4"
  size_t hexte = s.index("HCR");

  RWCString hexteCounts = s(hexte, s.length()-1);
  hexteCounts.append(" ");
  size_t _c = hexteCounts.index(" ", 0);
  size_t c_ = hexteCounts.index(" ", _c+1);
  RWCString c = hexteCounts(_c, c_);
  counts[0] = atof(c.data());
#ifdef DEBUG
  cerr << 0 << " " << _c << " " << c_ << " " << counts[0] << endl;
#endif
  for( int i = 1; i < 4; i++ ) {
    _c = c_;
    c_ = hexteCounts.index(" ", _c+1);
    c = hexteCounts(_c, c_);
    counts[i] = atof(c.data());
#ifdef DEBUG
    cerr << i << " " << _c << " " << c_ << " " << counts[i] << endl;
#endif
  }
}

inline double ExpectCounts::hexteKev15To30() const {
  double *counts = new double[4];
  hexteKevAll(counts);
  double retVal = counts[0];
  delete counts;
  return retVal;
}

inline double ExpectCounts::hexteKev30To60() const {
  double *counts = new double[4];
  hexteKevAll(counts);
  double retVal = counts[1];
  delete counts;
  return retVal;
}

inline double ExpectCounts::hexteKev60To125() const {
  double *counts = new double[4];
  hexteKevAll(counts);
  double retVal = counts[2];
  delete counts;
  return retVal;
}

inline double ExpectCounts::hexteKev125To250() const {
  double *counts = new double[4];
  hexteKevAll(counts);
  double retVal = counts[3];
  delete counts;
  return retVal;
}

inline RWCString ExpectCounts::pcaCountString() const {
  strstream s;
  double *counts = new double[6];
  pcaKevAll(counts);
  s << counts[0] << " (0--3.2Kev), "
    << counts[1] << " (3.2--4.3Kev), "
    << counts[2] << " (4.3--5.7Kev), "
    << counts[3] << " (5.7--8.6Kev), "
    << counts[4] << " (8.6--12.1Kev), "
    << counts[5] << " (12.1--60.0Kev) ";
  delete [] counts;
  RWCString retVal(s.str()); delete s.str();
  return retVal;
}
  
inline RWCString ExpectCounts::hexteCountString() const {
  strstream s;
  double *counts = new double[4];
  hexteKevAll(counts);
  s << counts[0] << " (15--30Kev), "
    << counts[1] << " (30--60Kev), "
    << counts[2] << " (60--125Kev), "
    << counts[3] << " (125--250Kev) ";
  delete [] counts;
  RWCString retVal(s.str()); delete s.str();
  return retVal;
}

inline RWCString ExpectCounts::targetString() const {
  RWCString retVal = *this;
  size_t idx = retVal.index("TARG");
  if( idx != RW_NPOS )
    retVal.remove(0, idx);
  else
    retVal = "No Target information currently available!";

  return retVal;
}

#endif // __EXPECTCOUNTS_H__   








@


4.10
log
@*** empty log message ***
@
text
@d1 1
a1 1
// RCS: "$Id: ExpectCounts.h,v 4.9 1994/12/10 21:06:39 dhon Exp dhon $"
d225 1
a225 1
    retVal.strip(RWCString::leading, idx);
@


4.9
log
@countsString methods
@
text
@d1 1
a1 1
// RCS: "$Id: ExpectCounts.h,v 4.8 1994/12/05 23:15:55 dhon Exp dhon $"
d20 1
d23 1
a23 1
  RWCString obsId() const;
d218 11
@


4.8
log
@switch int to double
@
text
@d1 1
a1 1
// RCS: "$Id: ExpectCounts.h,v 4.7 1994/11/07 18:42:15 dhon Exp dhon $"
d7 1
d20 3
a22 1
  void obsId(RWCString&) const;
d49 3
a51 2
inline void ExpectCounts::obsId(RWCString& id) const {
  const RWCString &s = *this;
d54 3
a56 2
  size_t pca = s.index("PCR");
  id = s(0, pca-1);
d189 28
@


4.7
log
@*** empty log message ***
@
text
@d1 1
a1 1
// RCS: "$Id: ExpectCounts.h,v 4.6 1994/11/07 18:36:58 dhon Exp dhon $"
d21 13
a33 13
  void pcaKevAll(int[6]) const;
  int pcaKev0To3p2() const;
  int pcaKev3p2To4p3() const;
  int pcaKev4p3To5p7() const;
  int pcaKev5p7To8p6() const;
  int pcaKev8p6To12p1() const;
  int pcaKev12p1To60() const;

  void hexteKevAll(int[4]) const;
  int hexteKev15To30() const;
  int hexteKev30To60() const;
  int hexteKev60To125() const;
  int hexteKev125To250() const;
d54 1
a54 1
inline void ExpectCounts::pcaKevAll(int counts[6]) const {
d66 1
a66 1
  counts[0] = atoi(c.data());
d74 1
a74 1
    counts[i] = atoi(c.data());
d81 2
a82 2
inline int ExpectCounts::pcaKev0To3p2() const {
  int *counts = new int[6];
d84 1
a84 1
  int retVal = counts[0];
d89 2
a90 2
inline int ExpectCounts::pcaKev3p2To4p3() const {
  int *counts = new int[6];
d92 1
a92 1
  int retVal = counts[1];
d97 2
a98 2
inline int ExpectCounts::pcaKev4p3To5p7() const {
  int *counts = new int[6];
d100 1
a100 1
  int retVal = counts[2];
d105 2
a106 2
inline int ExpectCounts::pcaKev5p7To8p6() const {
  int *counts = new int[6];
d108 1
a108 1
  int retVal = counts[3];
d113 2
a114 2
inline int ExpectCounts::pcaKev8p6To12p1() const {
  int *counts = new int[6];
d116 1
a116 1
  int retVal = counts[4];
d121 2
a122 2
inline int ExpectCounts::pcaKev12p1To60() const {
  int *counts = new int[6];
d124 1
a124 1
  int retVal = counts[5];
d129 1
a129 1
inline void ExpectCounts::hexteKevAll(int counts[4]) const {
d136 1
d140 1
a140 1
  counts[0] = atoi(c.data());
d148 1
a148 1
    counts[i] = atoi(c.data());
d155 2
a156 2
inline int ExpectCounts:: hexteKev15To30() const {
  int *counts = new int[4];
d158 1
a158 1
  int retVal = counts[0];
d163 2
a164 2
inline int ExpectCounts::hexteKev30To60() const {
  int *counts = new int[4];
d166 1
a166 1
  int retVal = counts[1];
d171 2
a172 2
inline int ExpectCounts::hexteKev60To125() const {
  int *counts = new int[4];
d174 1
a174 1
  int retVal = counts[2];
d179 2
a180 2
inline int ExpectCounts::hexteKev125To250() const {
  int *counts = new int[4];
d182 1
a182 1
  int retVal = counts[3];
@


4.6
log
@use new off heap instad of stack, so that subclass compiles!
@
text
@d1 1
a1 1
// RCS: "$Id: ExpectCounts.h,v 4.5 1994/10/14 14:23:45 dhon Exp dhon $"
d84 1
a84 1
  return counts[0];
d86 1
d92 1
a92 1
  return counts[1];
d94 1
d100 1
a100 1
  return counts[2];
d102 1
d108 1
a108 1
  return counts[3];
d110 1
d116 1
a116 1
  return counts[4];
d118 1
d124 1
a124 1
  return counts[5];
d126 1
d157 1
a157 1
  return counts[0];
d159 1
d165 1
a165 1
  return counts[1];
d167 1
d173 1
a173 1
  return counts[2];
d175 1
d181 1
a181 1
  return counts[3];
d183 1
@


4.5
log
@eliminate all usage of RWCSubString. who needs it anyway.
@
text
@d1 1
a1 1
// RCS: "$Id: ExpectCounts.h,v 4.4 1994/10/14 13:40:32 dhon Exp dhon $"
d82 1
a82 1
  int counts[6];
d85 1
d89 1
a89 1
  int counts[6];
d92 1
d96 1
a96 1
  int counts[6];
d99 1
d103 1
a103 1
  int counts[6];
d106 1
d110 1
a110 1
  int counts[6];
d113 1
d117 1
a117 1
  int counts[6];
d120 1
d149 1
a149 1
  int counts[4];
d152 1
d156 1
a156 1
  int counts[4];
d159 1
d163 1
a163 1
  int counts[4];
d166 1
d170 1
a170 1
  int counts[4];
d173 1
@


4.4
log
@added debug
@
text
@d1 1
a1 1
// RCS: "$Id: ExpectCounts.h,v 4.3 1994/10/14 12:32:13 dhon Exp dhon $"
d65 1
a65 1
  RWCSubString c = pcaCounts(_c, c_);
@


4.3
log
@global subst. pcaALl -> pcaKevAll, ditto for hexte
@
text
@d1 1
a1 1
// RCS: "$Id: ExpectCounts.h,v 4.2 1994/10/14 12:30:22 dhon Exp dhon $"
d6 1
d67 3
a69 1

d75 3
d128 3
a130 1

d136 3
@


4.2
log
@now used RWCString.index properly.
@
text
@d1 1
a1 1
// RCS: "$Id: ExpectCounts.h,v 4.1 1994/10/13 22:05:38 dhon Exp dhon $"
d20 1
a20 1
  void pcaAll(int[6]) const;
d28 1
a28 1
  void hexteAll(int[4]) const;
d53 1
a53 1
inline void ExpectCounts::pcaAll(int counts[6]) const {
d77 1
a77 1
  pcaAll(counts);
d83 1
a83 1
  pcaAll(counts);
d89 1
a89 1
  pcaAll(counts);
d95 1
a95 1
  pcaAll(counts);
d101 1
a101 1
  pcaAll(counts);
d107 1
a107 1
  pcaAll(counts);
d111 1
a111 1
inline void ExpectCounts::hexteAll(int counts[4]) const {
@


4.1
log
@*** empty log message ***
@
text
@d1 1
a1 1
// RCS: "$Id$"
d46 1
a46 1
  RWCString &s = *this;
d54 1
a54 1
  RWCString &s = *this;
d63 1
a63 1
  size_t c_ = pcaCounts.index(" ", 1);
d67 6
a72 24
  _c = c_;
  c_ = pcaCounts.index(" ", 2);
  c = pcaCounts(_c, c_);
  counts[1] = atoi(c.data());

  _c = c_;
  c_ = pcaCounts.index(" ", 3);
  c = pcaCounts(_c, c_);
  counts[2] = atoi(c.data());

  _c = c_;
  c_ = pcaCounts.index(" ", 4);
  c = pcaCounts(_c, c_);
  counts[3] = atoi(c.data());

  _c = c_;
  c_ = pcaCounts.index(" ", 5);
  c = pcaCounts(_c, c_);
  counts[4] = atoi(c.data());

  _c = c_;
  c_ = pcaCounts.index(" ", 6);
  c = pcaCounts(_c, c_);
  counts[5] = atoi(c.data());
d112 1
a112 1
  RWCString &s = *this;
d119 1
a119 1
  size_t c_ = hexteCounts.index(" ", 1);
d123 6
a128 24
  _c = c_;
  c_ = hexteCounts.index(" ", 2);
  c = hexteCounts(_c, c_);
  counts[1] = atoi(c.data());

  _c = c_;
  c_ = hexteCounts.index(" ", 3);
  c = hexteCounts(_c, c_);
  counts[2] = atoi(c.data());

  _c = c_;
  c_ = hexteCounts.index(" ", 4);
  c = hexteCounts(_c, c_);
  counts[3] = atoi(c.data());

  _c = c_;
  c_ = hexteCounts.index(" ", 5);
  c = hexteCounts(_c, c_);
  counts[4] = atoi(c.data());

  _c = c_;
  c_ = hexteCounts.index(" ", 6);
  c = hexteCounts(_c, c_);
  counts[5] = atoi(c.data());
d133 1
a133 1
  hexteAll(counts);
d139 1
a139 1
  hexteAll(counts);
d145 1
a145 1
  hexteAll(counts);
d151 1
a151 1
  hexteAll(counts);
@

head	1.1;
access;
symbols
	Build4_5_2:1.1;
locks
	rhee:1.1; strict;
comment	@# @;


1.1
date	95.09.18.21.47.37;	author rhee;	state Exp;
branches;
next	;


desc
@updated
@


1.1
log
@Initial revision
@
text
@                        Functional Test Plan
                        --------------------


Subsystem: Science Monitoring - PCA              Date:  09/15/95 

Build: 4.5.2           

Author: Hwa-Ja Rhee                              Site:  PCA         
                                                                       
Test Name: Add channel boundary info to an ascii file header created
           from 'pcaSColor'

Test Goals/Objectives:

     - The pcaSColor program has a option to save current spectrum
       data in ascii file with descriptive header.
       Test goal is to verify that channel boundaries of each
       spectral region in different colors are  added to current header.


Requirements Verified:

     - DR # 572 - PCA/SM adding more info to an ascii file text header.


Test Description:

     - Supply EDS/PCA Standard Mode 2  packets to the display client,
       pcaSColor, at the user specified rate, and save current spectrum
       data in an ascii file. Then examine this file to verify
       that channel boundary info is added to the header.


External Elements required for test:

     - shell script - $SOCHOME/test/bin/$ARCH/run-pcaSColor
     - input data files - /socops/reference/std2-PCA5.1a55189
     - output data files - $SOCOPS/tmp/pcaSpecColors.data     
     - pcaRtClient - accepts packets, checks configuration ids,         
                     constructs partitions and feed them to
                     the display clients.
     - pktgen - does the role of real-time server.
                delivers packets to the pcaRtClient at the
                user specified rate.


Test Procedure:

     1.setenv SOCHOME, SOCLOCAL and SOCOPS properly.                  
     2.Ensure proper installation of binaries and scripts in "test" area.
         a. Change directory to $SOCHOME/src/SciMonitoring/PCA/specColors
                                (or   ../src/SciMonitoring/PCA/specColors)    
         b. Create a new program binary (pcaSColor), 
            and install test script, using the line :
               
            gmake  install   (installs "pcaSColor" in $SOCHOME/bin/$ARCH )
            gmake  install-test  (installs "run_pcaSColor" script
                                  in $SOCHOME/test/bin/$ARCH )
            gmake  install-doc   (installs "pcaSMtestDR572.txt" testplan
                                  in $SOCHOME/doc )
         
       
     3.Run the test script by typing :

            $SOCHOME/test/bin/$ARCH/run-pcaSColor
            (if you are already in right directory, simply type :
                run-pcaSColor )

     4.After some seconds, "pcaSColor" GUI should appear.
          a. Any time during execution, save spectrum data
             by clicking on 'Save(in ascii)' button under 'File'  
             item of the Menubar.
             This action writes(or overwrites) 'pcaSpecColors.data' 
             in $SOCOPS/tmp directory.
          
         
     5.To exit from the program, click on "Quit" button 
       under "File" item of the Menubar on the "pcaSColor" GUI panel.

        
Expected Results:

     1.Inspect 'pcaSpecColors.data' in $SOCOPS/tmp and verify 
       a line
       '//Channels for color boundaries are : xxx-xxx yyy-yyy (zzz-zzz)'
       exists among other header text.
 

Verification of Results:

     - The tester should observe "Expected Results 1 "         
       described above.       


Optional:

     - During the program execution, press "Adjust Color Bands" button
       at the middle right.  A window pops up, where a user reset
       the color boundaries and press OK. 
       (note : make sure each colormax > colormin)
       New color range will show on the spectrum. 
       Click on "Save(ascii)" and check ascii file header to verify
       that latest info is saved.
  
     Feel free to check updated *.hlp file. (Comments are welcome)
     - Click on "Info" button under the "Help" button of the Menubar.
       Pointer will become a "?".
     - Click on any Widget area.
                   or
     - Click on any panel area, not occupied by any of the Widgets.
     

@

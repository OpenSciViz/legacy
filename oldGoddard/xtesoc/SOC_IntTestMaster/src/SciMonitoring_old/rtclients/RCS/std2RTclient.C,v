head	2.2;
access
	soccm;
symbols
	Build2:2.1;
locks
	ehm:2.2; strict;
comment	@ * @;


2.2
date	94.01.25.12.33.21;	author ehm;	state Exp;
branches;
next	2.1;

2.1
date	93.11.30.20.11.18;	author ehm;	state Release;
branches;
next	;


desc
@Standard 2 Real Time client
@


2.2
log
@updated with new template. Supports read-from-stdin
@
text
@//
// $Id: std2RTclient.C,v 2.2 1994/01/24 19:44:08 ehm Exp ehm $
//
// This is the only module in the rtclient code which should need to be
// modified by realtime client writers.  Simply replace the innards of
// client_specific_initialization() and process_a_packet() with the correct
// code for your situation.  A few of the other functions in here may be
// tweaked where explicitly mentioned.
//

#include <iostream.h>
#include <new.h>
#include <signal.h>
#include <stdarg.h>
#include <stdlib.h>
#include <unistd.h>

#include "science_if.h"
#include "EARun.h"
#include "EdsPartition.h"
#include "PktPartHdr.h"
#include "PktPartData.h"
#include "PktTlm.h"
#include "SCDataCache.h"
#include "ObsParamBlock.h"
#include "PartMap.h"
#include "Utilities.h"
#include "rtclient.h"  /* Entry points into librtclient.a */

#ifdef mips
#include "List.h"
static  List<SpacecraftData> x1;
static RWTPtrOrderedVector<PktSwHouse> x2;
static RWTPtrOrderedVector<ObsComp> x3;
#endif

#include "CluArray.h"
static CluArray<int> x4;
static CluArray<char> x5;
static CluArray<RangeList*> x6;
static CluArray<Descriptor> x7;

static ObsParamBlockPtr config;

//
// To work around a template bug.
//
#include "Descriptor.h"



//
// The port of the Realtime Server to which we connect to get telemetry.
//
static short server_port;


//
// The machine on which the Realtime Server is executing.
//
static const char* server_host;


//
// Should we run as a daemon?
//
static int run_as_daemon;


//
// Should we read packets from STDIN instead of from the Realtime Server?
//
static int read_from_stdin;


//
// This is the name we advertise to the Realtime Server.  It is the same as
// the entry in the relevant $SOCHOME/etc/RT.CLIENTS table.  You will probably
// want to hardcode the standard name for your client here, instead of relying
// on command line arguments and parse_arguments() to set it.
//
static const char* client_name;


//
// Our name -- set in main().  Not necessarily the same as the name we
// advertise to the Realtime Server.  This is used for reporting of errors
// and messages.
//
static char* ProgName;


//
// The Spacecraft Cache object.  Query this object to get whatever S/C data
// you've requested to be cached.  This is instantiated and maintained by
// the librtclient.a code.
//
extern SCDataCache SC;

// handler for a Full EARun
void handleEAR (const EARun*)
{
}


// handler for a full partition (Quick Look)
void handlePart(const EdsPartition* thePart)
{
  unsigned short *unpackedData;
  int j, pcuid, lld;
  // get the configuration from the partition's config ID
  unsigned long configId = thePart->getConfigurationID();
  if (configId == 0x2000005)
  {
    ios::sync_with_stdio();
    cout << "Start Time: " << thePart->obsStartTime() + 16 * thePart->partNumber()<< endl;
    unpackedData = thePart->getData();
    for (j=0; j < 128 ; j++)
    {
      printf("%3d ", j);
      for (pcuid=0; pcuid < 5 ; pcuid++)
	for (lld=0; lld < 6 ; lld++)
	  printf("%4d ",unpackedData[j + lld * 128 + 768 * pcuid]);
      printf("\n");
    }
    printf("%3d ", 128);
    for (pcuid=0; pcuid < 5 ; pcuid++)
      for (lld=0; lld < 6 ; lld++)
	printf("%4d ",unpackedData[4145 +  lld  + 6 * pcuid]);
    printf("\n");
    
    for (j=0; j < 32 ; j++)
    {
      printf("%3d ", j);
      for (pcuid=0; pcuid < 5 ; pcuid++)
      {
	printf("%5d ", unpackedData[ 3840 + j + 32 *pcuid]);
      }
      printf("\n");
    }
    printf("%3d ", 32);
    for (pcuid=0; pcuid < 5 ; pcuid++)
    {
      printf("%5d ", unpackedData[ 4175 + pcuid]);
    }
    printf("\n");
    for (j=0; j < 29 ; j++)
    {
      printf("%3d ", j + 1);
      for (pcuid=0; pcuid < 5 ; pcuid++)
      {
	printf("%5d ", unpackedData[ 4000 + 5 * j + pcuid]);
      }
      printf("\n");
    }
    fflush(stdout);
    delete  unpackedData;
  }
}

// handler for a completed configuration
void monitorConfig(const ObsParamDump* theDump)
{
  cerr << " complete Obs ParamDump" <<endl;
  
}

//
// This is the routine that does the packet-specific processing.  Do not
// change the signature of this function.  Each time one of the packets in
// which you've specified an interest is received by the realtime client
// process, this function is called -- this includes any packets containing
// S/C data in which you've indicated an interest in receiving, as well.
// The caching of S/C data is done for you in the librtclient.a code, but only
// if you specify that you want it cached.  You can specify that you want
// to get S/C data from the Realtime Server which you must then choose whether
// you want to cache it or not.  You really have three options when you're
// receiving spacecraft data:
//
//   1) cache it and ignore the S/C packets themselves;
//   2) just deal with the raw S/C packets;
//   3) both (1) & (2).
//
// For non-spacecraft data you must deal directly with the packets.
//

void process_a_packet (const PktTlm* pkt)
{
  //
  // You've got a valid PktTlm which will be the virtually correct type.
  // You should probably check the application ID to make sure it is one
  // of the ones that you want, just as a double check.  Then do with it
  // as you will.  If you need to cache the packet you must make a copy of
  // it.  The routine that calls process_a_packet() deletes the packet
  // when this function returns.
  //
  
  PktTlm pktMaker(Exemplar());
//  PktTlm* pkt = pktMaker.make((unsigned char*)tpkt->rawData());
  if ((((PktEds *)pkt)->streamID() ==  6) || (((PktEds *)pkt)->streamID() ==  7))
  {
    pkt->apply();
  }
}


//
// Put any specific initialization stuff you need into here.  This is the place
// to specify the types of S/C data to be cached.  Do not change the signature
// of this function.
//

static void client_specific_initialization (void)
{
  //
  // Enable each of the types of spacecraft data in which you're interested.
  //
  //
  // Put your code here.
  //
  // Get the configuration, since we are selecting a single mode.
  config = ObsParamBlock::restore(0x02000005);
  ObsParamDump::setDisposition(monitorConfig);
  EdsPartition::setDisposition(handlePart);
  
}


//
// Print out usage string and exit.  If you change parse_arguments(),
// make the relevant changes here as well.
//

static void usage (void)
{
  cerr << "\nusage: " << ProgName
       << " [-daemonize]"
#ifdef XTEST
         << " -display-client name"
#endif
         << " -host hostname"
         << " -name client-name"
         << " [-port #]"
         << " [-read-from-stdin]\n\n";
  exit(1);
}


//
// Parse our arguments.  Feel free to add your own client-specific options.
// If you do so, make sure to update usage() appropriately.
//

static void parse_arguments (char**& argv)
{
  //
  // Parse any arguments.
  //
  //    -daemonize           -- run as a daemon
  //    -display-client name -- name of X client program to execute
  //    -host machine        -- realtime server machine
  //    -port #              -- port number to connect torealtime server
  //    -name name           -- the name we advertise to the realtime server
  //    -read-from-stdin     -- read from stdin instead of over a socket
  //
  while (*++argv && **argv == '-')
  {
    if (strcmp(*argv, "-host") ==  0)
    {
      if (!(server_host = *++argv))
	error("No hostname supplied.");
    }
    else if (strcmp(*argv, "-port") == 0)
    {
      if (*++argv)
      {
	if ((server_port = (short) atoi(*argv)) <= 0)
	  error("Port # must be positive short.");
	continue;
      }
      else
	error("No port # supplied.");
    }
    else if (strcmp(*argv, "-daemonize") == 0)
      run_as_daemon = 1;
#ifdef XTEST
    else if (strcmp(*argv, "-display-client") ==  0)
    {
      if (!(display_client = *++argv))
	error("No display client name supplied.");
    }
#endif
    else if (strcmp(*argv, "-name") ==  0)
    {
      if (!(client_name = *++argv))
	error("No name supplied.");
    }
    else if (strcmp(*argv, "-read-from-stdin") == 0)
      read_from_stdin = 1;
    else
    {
      message("`%s' is not a valid option, exiting ...", *argv);
      usage();
    }
  }
  
  if (read_from_stdin && run_as_daemon)
  {
    run_as_daemon = 0;
    message("Using `-read-from-stdin' implies ! `-daemonize'.");
    
  }
  
  if (!server_host && !read_from_stdin)
  {
    message("`realtime-server-host' or `read-from-stdin' must be specified.");
    usage();
  }
  
  if (!read_from_stdin && !client_name)
    usage();
  
  //
  // Check that basic environment variables are set.
  //
  if (getenv("SOCHOME") == 0)
    error("The `SOCHOME' environment variable must be set.");
  
  if (getenv("SOCOPS") == 0)
    error("The `SOCOPS' environment variable must be set.");
  
}


//
// Simple error handler for use by PktCCSDS.  Change this if you want
// to do something fancier.
//

static void packet_error_handler (const char* msg) { error(msg); }


//
// A simple Out-Of-Memory Exception handler for main().  Feel free
// to change this if you really can do better than exit.
//

static void free_store_exception (void)
{
#ifdef ATEXIT
  error("Out of memory, exiting ...");
#else
  message("Out of memory, exiting ...");
  if (!read_from_stdin)
    force_exit_of_other_half();
  exit(1);
#endif
}


//
// The main routine.  Please do not change this without very good reason.
// client_specific_initialization() is placed such that you should be able
// to override many of my assumptions if you so choose.
//

int main (int, char* argv[])
{
  ProgName = argv[0];
  
  set_new_handler(free_store_exception);
  
  PktCCSDS::setErrorHandler(packet_error_handler);
  
  initialize_signal_handlers(catch_a_signal);
  
  parse_arguments(argv);
  
  if (run_as_daemon)
    //
    // Feel free to daemonize to someplace other than the filesystem
    // root if it makes sense for your application.
    //
    daemonize(client_name, "/");
  
#ifdef ATEXIT
  if (!read_from_stdin)
    if (atexit(force_exit_of_other_half))
      error("File %s, line %d, atexit() failed.",
	    __FILE__, __LINE__);
#endif
  
  if (read_from_stdin)
  {
    //
    // Do client-specific setup.
    //
    client_specific_initialization();
    read_packets_from_stdin();
  }
  else
  {
    //
    // Now fork to create a child process. The parent process
    // (aka back-end) reads packets from the Realtime Server and
    // places them in the ring buffer.  The child process
    // (aka front-end) reads these packets and does the appropriate
    // processing.
    //
    if ((PID = fork()) == 0)
    {
      //
      // This code is only executed by the front-end portion of the
      // realtime client; that part that is actually interested in 
      // the realtime analysis/display of the data in the packets.
      //
      PPID = getppid();
      //
      // Do client-specific setup.
      //
      client_specific_initialization();
      //
      // The primitive event mechanism in process_packets() isn't
      // compatible with the X Event mechanism.  If you want to drive
      // X Windows directly from this code, instead of fork()ing and
      // exec()ing a standalone X Windows process to which you write
      // the required data, please talk to me so we can work out the
      // best way to do it, together.  I have some ideas.  I just
      // haven't felt like putting in the time, if it wasn't clear
      // that it was going to be used.
      //
      process_packets();
    }
        else
            //
            // This code is only executed by the back-end of the realtime
            // client.  It is the code that maintains the connection to the
            // realtime server and gives well-built CCSDS Telemetry packets
            // to the front-end of the realtime client.
            //
            connect_to_realtime_server(client_name, server_port, server_host);
    }

    return 0;
}





@


2.1
log
@Initial release
@
text
@d1 10
a10 4
//   RCS Stamp: $Id$ 
//   Description : Real time client for Standard 2 configuration
//   Author      : Edward Morgan - ehm@@space.mit.edu
//   Copyright   : Massachusetts Institute of Technology
d12 6
d27 2
a28 4
#include <new.h>
#include <stdarg.h>

static const char rcsid[]= "$Id$"; 
a29 2
// static variables to ensure template instatiation
// we ifdef mips because the only place we are running cfront 3.0.1 is on decs
d45 55
d107 1
a107 1
 void handlePart(const EdsPartition* thePart)
d115 1
a156 1
    delete  thePart;
a167 63




//
// Entry points into librtclient.a.
//
extern void catch_a_signal (int sig);
extern void daemonize (const char* name, const char* dir);
extern void force_death_of_both_halves (void);
extern void initialize_signal_handlers (void (*catcher)(int));
extern void message (const char *format, ...);
extern void read_packets_from_realtime_server (const char*, short, const char*);


//
// Generic Options.
//

//
// The port of the Realtime Server to which we connect to get telemetry.
//
static short realtime_server_port=6503;

//
// The machine on which the Realtime Server is executing.
//
static const char* realtime_server_host;

//
// Should we run as a daemon?
//
static int run_as_daemon;

//
// This is the name we advertise to the Realtime Server.  It is the same as
// the entry in the relevant $SOCHOME/etc/RT.CLIENTS table.  You will probably
// want to hardcode the standard name for your client here, instead of relying
// on command line arguments and parse_arguments() to set it.
//
static const char* client_name;

//
// Our name -- set in main().  Not necessarily the same as the name we
// advertise to the Realtime Server.  This is used for reporting of errors
// and messages.
//
static char* ProgName;

//
// Please feel free to put any other options you want for you client here.
// You must update parse_arguments() and usage() appropriately.
//


//
// The Spacecraft Cache object.  Query this object to get whatever S/C data
// you've requested to be cached.  This is instantiated and maintained by
// the librtclient.a code.
//
extern SCDataCache SC;


d172 13
a184 4
// process, this function is called.  This is not called when S/C data in which
// you've indicated an interest is received.  The caching of S/C data is done
// for you in the librtclient.a code.  You can of course use any of the cached
// data in the `SC' object from within this function.
a185 1
long plen;
d187 1
a187 1
void process_a_packet (const PktTlm* tpkt)
d189 9
a197 12
    //
    // You've got a valid PktTlm.  You should probably check the application
    // ID to make sure it is one of the ones that you want, just as a double
    // check.  Then do with it as you will.
    //

    //
    // I will just print out some stuff and go on.
    //
//    cout << "App Id = "       << pkt->applicationID()
  //       << ", SequenceNo = " << pkt->sequenceNumber()
  //     << ", Length = "     << pkt->packetLength()   << endl;
d199 2
a200 15

  
  PktTlm* pkt = pktMaker.make((unsigned char*)tpkt->rawData());
//  pkt->dumpHeader(cout);

  if (((PktEds *)pkt)->streamID() == 6)
  {
    pkt->apply();
//    cerr << "config ID: " << ((PktPartHdr *)pkt)->configurationID() << endl;
//    cerr << "Partition Length " << config->partMap()->packedSize(((PktPartHdr *) pkt)->partitionLength()) << " " << ((PktPartHdr *) pkt)->partitionLength() <<  endl;
//    cerr << "Partition Length " << config->partMap()->packedSize() << endl;
//    plen = 0;
  }

  if (((PktEds *)pkt)->streamID() == 7)
a202 2
//    plen += ((PktPartData *) pkt)->dataBufferSize();
//    cerr << "Length so far "<< plen/2 << endl;
a203 2
  delete  pkt;
  delete tpkt;
d218 3
a220 3
//    SC.enableCaching(SC_ATTITUDE);
//    SC.enableCaching(SC_POSITION);
  
d225 1
d236 10
a245 3
    cerr << "\nusage: " << ProgName << " [-daemonize] [-port #]"
         << " -host hostname -name client-name\n\n";
    exit(1);
d256 45
a300 9
    //
    // Parse any arguments.
    //
    //    -daemonize        -- run as a daemon
    //    -host machine     -- realtime server machine
    //    -port #           -- port number to connect torealtime server
    //    -name name        -- the name we advertise to the realtime server
    //
    while (*++argv && **argv == '-') 
d302 2
a303 28
        if (strcmp(*argv, "-host") ==  0)
        {
            if (!(realtime_server_host = *++argv))
                error("No hostname supplied.");
        }
        else if (strcmp(*argv, "-port") == 0)
        {
            if (*++argv)
            {
                if ((realtime_server_port = (short) atoi(*argv)) <= 0)
                    error("Port # must be positive short.");
                continue;
            }
            else
	      realtime_server_port = 6503;
        }
        else if (strcmp(*argv, "-daemonize") == 0)
            run_as_daemon = 1;
        else if (strcmp(*argv, "-name") ==  0)
        {
            if (!(client_name = *++argv))
	      client_name = argv[0];
        }
        else
        {
            message("`%s' is not a valid option, exiting ...", *argv);
            usage();
        }
d305 27
a331 6

    //
    // Hostname and name must be supplied.
    //
    if (!realtime_server_host || !client_name)
        usage();
d340 1
a340 4
static void packet_error_handler (const char* msg)
{
    error(msg);
}
d350 8
a357 1
    error("Out of memory, exiting ...");
d369 26
a394 8
    ProgName = argv[0];

    set_new_handler(free_store_exception);

    PktCCSDS::setErrorHandler(packet_error_handler);

    initialize_signal_handlers(catch_a_signal);

d396 1
a396 1
    // Do IT-specific setup.
d399 44
d444 2
a445 1
    parse_arguments(argv);
a446 2
    if (run_as_daemon)
        daemonize(client_name, "/");
a447 5
#ifdef ATEXIT
    if (atexit(force_death_of_both_halves))
        error("File %s, line %d, atexit(force_death_of_both_halves) failed.",
              __FILE__, __LINE__);
#endif
a448 2
    read_packets_from_realtime_server(client_name, realtime_server_port,
                                      realtime_server_host);
a449 2
    return 0;
}
@

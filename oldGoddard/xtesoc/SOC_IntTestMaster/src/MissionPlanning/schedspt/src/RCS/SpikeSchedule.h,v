head	4.5;
access
	soccm;
symbols;
locks; strict;
comment	@ * @;


4.5
date	97.04.25.18.54.48;	author buehler;	state Exp;
branches;
next	;


desc
@Class to represent the complete scheduling state of a long
term scheduling period, and manage SPIKE input files..
@


4.5
log
@*** empty log message ***
@
text
@//======================================================================
// SpikeSchedule.h
//
// RCS Stamp: $Id: SpikeSchedule.h,v 4.5 1995/11/04 19:56:42 buehler Exp $
// Author:    Royce Buehler   - buehler@@space.mit.edu
// Copyright: Massachusetts Institute of Technology

// .NAME SpikeSchedule.h
// .LIBRARY schedspt
// .HEADER Info needed to (re)create a schedule in SPIKE
// .INCLUDE SpikeSchedule.h
// .FILE basicSpkSched.C
// .FILE SpikeSchedule.C
// .FILE schedFile.C
// .SECTION Author
// Royce Buehler, buehler, MIT, buehler@@mit.edu
// .SECTION  Description
// A SpikeSchedule is the SPIKE wrapper object corresponding to a
// SPIKE .sched file. It records a long term schedule, and short term
// schedules for all its bins. This includes a list of ObsDesIds,
// constraint file names, and (TBI) non-default SPIKE parameters.
// Notes on object identity:
//    A SpikeSchedule is initially created with the basic constructor.
// It can be adjusted with a variety of modifiers, but once the save()
// command has been issued, and an SPTScheduleId() associated with the
// SpikeSchedule, its state is frozen. Future calls to the modifiers
// will have no effect. 
//
// REVISION HISTORY
// 4.3  Jun  9 95 reb Drop unneeded includes, refs to method auxSched
//======================================================================

#ifndef _SPIKESCHEDULE_H
#define _SPIKESCHEDULE_H

#include <rw/collect.h>
#include <rw/tvordvec.h>    // RW template class for ordered vectors.
#include <rw/tpordvec.h> 
#include <rw/tvhdict.h>     // RW template class for value based dictionary.

#include <SOCTime2.h>       // SOC utility class SOCTime
#include <ObsID.h>        // SOC Mission Planning class ObservationID

#include "mit_td.h"       // MIT type definitions
#include "edsCid.h"       // MIT RW Class ID definitions.
#include "SptEnums.h"    // targState defined.
#include "SPTScheduleId.h"  // Public handles to SpikeSchedules.

class ObservationDescription;
class SPTScheduleId;
class SPTSchedule;
class SptSchedItem;      // Declared in SPTSchedule.h
class SPIKELongTermScheduler; //Declared in SPIKE_LTS.h
class LispNode;
class ofstream;
class ostream;

#define clockTy unsigned long
typedef unsigned  yearTy;

//==================================================================
// .SECTION Class SpikeAssignedVar.
// Really just a struct, class SpikeAssignedVar is needed because 
// RWTPtrOrderedVector calls for a class as its template argument. Contains
// the name of an assigned SPIKE variable and its integer value.
//==================================================================
class SpikeAssignedVar
{
public:
  // Default constructor. Empty name string, value 0.
  SpikeAssignedVar(void);
  ~SpikeAssignedVar(void);

  // Equality operator. All data members equal.
  RWBoolean operator==(const SpikeAssignedVar&);
  
  RWCString name;
  long value;
};

//==================================================================
// .SECTION Template structs
// The compiler will not accept a nested structure as a template type,
// so relConstEntry has to be public.
// Every observation involved in a relativeTo constraint appears
// two or three times in a relConstrEntry - once as thisObs, and once
// as prevObs and/or nextObs.
// The structs spikeNumParam and spikeStrParam hold the parameter name
// and value for a global SPIKE parameter such as *XTE-TIME-QUANTUM*.
//==================================================================

struct relConstrEntry {
  ObservationID prevObs;
  ObservationID thisObs;
  ObservationID nextObs;
  RWCString constrFile;
public:
  int operator==( const relConstrEntry& another) const;
};

class spikeNumParam {
public:
  spikeNumParam(void);
  ~spikeNumParam(void);
  int operator==(const spikeNumParam& other) const;
  RWCString name;
  double value;
};

struct spikeStrParam {
  spikeStrParam(void);
  ~spikeStrParam(void);
  int operator==(const spikeStrParam& other) const;
  RWCString name;
  RWCString value;
};

//==================================================================
// Class SpikeSchedule.
//==================================================================

class SpikeSchedule : public RWCollectable
{
// Basic methods: Constructors, destructor, persistence, operators.

friend ostream& operator<< (ostream& out, const SpikeSchedule& aSched);
  RWDECLARE_COLLECTABLE(SpikeSchedule)

public:

SpikeSchedule ();

SpikeSchedule (const SpikeSchedule&);

// The basic constructor for SpikeSchedule. 
SpikeSchedule(SOCTime2 begin, long secondsInWeek, int binCnt);

// Create a schedule from the SPIKE file named fn.sched.
SpikeSchedule(RWCString fn);

~SpikeSchedule();

SpikeSchedule& operator=(const SpikeSchedule& );

//==================================================================
// Methods to support Rogue Wave persistence and collectability follow.
//==================================================================

//#if RWTOOLS < 0x0600 && ! defined(RWspace)
//#define RWspace unsigned
//#endif
  RWspace   binaryStoreSize()               const;
  RWBoolean isEqual(const RWCollectable*)   const;
  void      restoreGuts(RWFile&);
  void      restoreGuts(RWvistream&);
  void      saveGuts(RWFile&)               const;
  void      saveGuts(RWvostream&)           const;

  // Assign an Id if necessary, and do a persistent save.
  void save(SPTScheduleId::scopeValue aScope, const char* userName = "");

  // Restore in current object the SpikeSchedule on persistent file fn.
  void restore(const RWCString& fn );

//==================================================================
// Public accessors and modifiers.
//==================================================================

  // Return TRUE if the two SpikeSchedules have same values.
  int operator== (const SpikeSchedule& ) const;

  // Return TRUE if this is the null SpikeSchedule, FALSE otherwise. 
  int operator! () const;

/*
RWBoolean isCompatibleWith( SpikeSchedule& another);
*/

// Return handle to recover this SpikeSchedule.
const SPTScheduleId getId() const;

const SOCTime2 getStart() const;
const SOCTime2 getEnd() const;

// Add anObs to the long term schedule, in state TS_UNSCHEDULED.
void appendUnscheduled( ObservationDescription& anObs);

void cleanTargets(void);

// Write a SPIKE .sched file corresponding to this schedule.
// [Implemented in schedFile.C]
void writeSpikeInputs(const RWCString& aRoot, ostream& anOutputStream) const;

// Auxiliary function used by writeSpikeInputs to write var-value pairs.
// [Implemented in schedFile.C]
void writeAssignedVars(ostream& out) const;

// Auxiliary function used by writeSpikeInputs to write bin and start
// time assignments as truncated Julian dates.
// [Implemented in schedFile.C]
void writeCommitments(ostream& out) const;

// Read contents of a SPIKE .sched file into this schedule.
// [Implemented in schedFile.C]
int readSpikeInputs(istream& anInputStream);

// Write script for automatic mode.
void writeSpikeScript(ostream& anOutputStream) const;

// What is the scheduled start time for the given observation?
SOCTime2 when( ObservationID anObs) const;

// To which bin has the given observation been assigned? 
int whichBin ( ObservationID anObs) const;

// Returns TRUE if this observation is known to this schedule.
int hasObservation( ObservationID anObs) const;

targState getTargetState( ObservationID anObs) const;

// Returns all observations this scheduling problem is trying to schedule.
RWTValOrderedVector<ObservationID>* getObsIdList(void) const;

// Returns the long-term schedule.
const SPTSchedule* getLtsSchedule(void) const;

// Display long term schedule in a simple test format
void showRawLts( ostream& out) const;

// Read a file in the format given above, and set the LTS accordingly.
void readRawLts( istream& instr);

// Returns the short term schedule for the nth bin. Bins start at 0.
const SPTSchedule* getStsSchedule(int n) const;

//=================================================================
//% SPIKE_LTS and SPIKE_STS use the next set of methods to initialize 
//% SPIKE parameters and locking states between LTS construction and
//% calling the schedule() method.
//=================================================================
int setSpikeParameter(const RWCString& paramName, double paramValue);
int setSpikeParameter(const RWCString& paramName, 
		      const RWCString& paramValue);

// Manipulate bin-wide assignment locks. Observations that have been
// individually locked into their bins remain so after unlockBin is called.
int lockBin(int n);
int unlockBin(int n);

// Lock/unlock a single observation's bin assignment.
int lockObsBin(const ObservationID& anID);
int unlockObsBin(const ObservationID& anID);

// Lock/unlock bin assignments of all observations in a given set.
void lockBinsUntil(const SOCTime2& aTime, int aPriority);
void unlockBinsAfter(const SOCTime2& aTime, int aPriority);

// Lock/unlock specific start times of all observations in a given set.
// lockUntil implicitly calls lockBinsUntil; To undo it completely,
// call both unlockAfter and unlockBinsAfter.
void lockUntil (const SOCTime2& aTime, int aPriority);
void unlockAfter(const SOCTime2& aTime, int aPriority);

// Lock items with short-term schedule start times after aTime into their bins.
void lockBinsIfScheduled (const SOCTime2& aTime);

// Undo bin assignments of any observation with no specific start time.
void deassignUnlessScheduled(void);
 
//=================================================================
//% Useful conversion functions:

//  Return the starting moment of the week given a time in the week.
SOCTime2 timeToSchedWeekStart(const SOCTime2& aTime) const;

// Return number of the weekly bin to which aTime belongs.
int timeToBin( SOCTime2 aTime) const;

// Convert a bin number to start time of its scheduling week.
SOCTime2 binToWeek( int aBin) const;

// Convert aTime to the start time of the scheduling week it lies in.
int weekToBin (SOCTime2 aTime) const;

//=================================================================
// Private members.
private:

enum SpikeSchedProc { SSPS_CREATING, SSPS_CLEAN, SSPS_DIRTY, SSPS_SAVED };

// Auxiliary function to copy constructor; see basicSpkSch.C.
void copyGuts(const SpikeSchedule& other);

// Add anItem to the long term schedule.
void appendItem(const SptSchedItem& anItem);

// Add anItem to the short term schedule for the week beginning at aWeek.
void appendItem(const SptSchedItem& anItem, SOCTime2 aWeek);

// Update relConstraints table from observation descriptions.
void updateRelConstraints( ObservationDescription& anObs);

// Write a default value for a numeric SPIKE global to the .sched file.
void writeNumGlobalDefault(const RWCString& globalName,
		      double defaultVal,
		      ostream& out) const;

// Write SPIKE .constr file for the given observation.
int writeConstraintFile( ObservationDescription& anObs);

// Used by writeConstraintFile to write PHASEANGLERANGE, DAYOFWEEK, TIMEOFDAY.
void writePhaseConstraints(ObservationDescription& anObs, ofstream& outf);

// Used by writeConstraintFile to write RELATIVETO.
void writeRelConstraints(ObservationDescription& anObs, ofstream& outf);

// Writes names of target files as a single Lisp list.
void writeTargFileNames( ostream&) const;

// Writes names of constraint files as a single Lisp list.
void writeConstrFileNames( ostream&) const;

// [Next 10 methods implemented in schedFile.C]

// Writes names of variables locked into bins as a single Lisp list.
void writeLockedInBins( ostream& out) const;

// Writes numbers of locked bins as a single Lisp list.
void writeLockedBins( ostream& out ) const;

void procSchedNode( LispNode& aNode);

//% The following functions are used by procSchedNode.
void procSchedToken( LispNode& aNode);
void setSchedGlobal( LispNode* aNode);
void setFromAssignedList( LispNode* aNode);
void setFromCommitList( LispNode* aNode);
void setFromLockedList( LispNode* aNode);
void setFromIgnoredList( LispNode* aNode);
void setFromLockedBins( LispNode* aNode);

  SOCTime2 start;       // Beginning of longterm schedule period
  SOCTime2 end;
  float    oversub;       // % oversubscribed
  long     weekLength;    // Length of week in seconds
  int      weekCount;       // Number of bins in schedule (1 to 52)
  RWTValOrderedVector<RWCString> constrFiles; 
  // Names of constraint files used by targets in this schedule.
  RWTValOrderedVector<ObservationID>   dirtyTargets;  
   // Targets needing prepping.
  RWTValOrderedVector<relConstrEntry> relConstraints;
  // Catalog of relativeTo constraints specified in constrFiles.

  RWTValOrderedVector<spikeNumParam>    nGlobals;  //SPIKE global params.
  RWTValOrderedVector<spikeStrParam>    sGlobals;
  SPTSchedule* lts;     // The long term schedule
  SPTSchedule *sts;    // Array of short term scheds, with weekCount entries.
  RWTValOrderedVector<SpikeAssignedVar> targValues;
  // Looks up the value of the target as a variable in a SPIKE OBS_CSP.
  SpikeSchedProc procState;  // Do I have an Id? Do I need saving?
  SPTScheduleId* id;   // Handle to this object seen outside SPIKE wrapper.
};

// Stream insertion operator
ostream& operator<< (ostream& out, const SpikeSchedule& aSched);

// Transform a SOCTime to a SPIKE truncated Julian Date in days.
double  socToTjd( const SOCTime2&);

// Transform a SPIKE truncated Julian Date to a SOCTime.
SOCTime2 tjdToSoc( double);

// Return base Julian Date for SPIKE Truncated Julian Dates (*JD-REFERENCE*).
clockTy getSpikeZero(void);

// Read all the schedules generated by the current SPIKE run.
// [Implemented in schedFile.C]
int readSchedules(RWTPtrOrderedVector<SpikeSchedule>& schedList,
		  const RWCString& savedSchedsName);

// Read the file userName.sched into this object.
void readSchedule( const RWCString& userName, 
			RWTPtrOrderedVector<SpikeSchedule>& theScheds);

double socToTjd( const SOCTime2& aTime );
SOCTime2 tjdToSoc( double tjdDays);


#endif        // _SPIKESCHEDULE_H




@

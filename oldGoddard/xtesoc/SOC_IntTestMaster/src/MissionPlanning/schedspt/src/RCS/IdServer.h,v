head	3.1;
access
	soccm;
symbols;
locks; strict;
comment	@ * @;


3.1
date	97.04.25.18.51.21;	author buehler;	state Exp;
branches;
next	;


desc
@Server to generate unique IDs for objects.
@


3.1
log
@*** empty log message ***
@
text
@//===================================================================
// IdServer.h
// RCS Stamp: $Id: IdServer.h,v 3.1 1994/05/12 00:16:59 buehler Submitted $
// Author:    Royce Buehler  - buehler@@space.mit.edu
// Copyright: Massachusetts Institute of Technology
// Subsytem:  
// .NAME IdServer
// .LIBRARY ?
// .INCLUDE IdServer
// .FILE IdServer.C
//
// .SECTION Description:
// An IdServer provides a computer-generated long integer,
// guaranteed to be unique within a pool of IDs.
// The pool is specified by a string when the server is create.
// These pools are persistent, so you will not begin over again at 1
// when instantiating a server from a new executable.
// However, the intent is to have only one server per pool going
// at a time. You will get warnings of oddities due to unexpected
// concurrent servers, but only a modicum of damage control.
//
// When the first server for a pool is instantiated, it will not find
// its permanent file. It will issue a warning and ask the user's
// permission to continue.
//===================================================================

#ifndef IdServer_H
#define IdServer_H

#include <rw/cstring.h>

class IdServer 
{
  friend class IdServ_init;
public:
  IdServer(void);
// Default constructor. Creates a server for the "Void" pool.

// NB: There are no pointer data members, so compiler-supplied
// copy constructor, destructor, and assignment operator are good as is.

  IdServer(RWCString thePool);
// Return an Id server for this pool. Get it from disk, 
// ($SOCOPS for now, later maybe a utility subdirectory)
// if it exists, else create it.

  int reset(void);
// Reset the server. IDs will be re-used, starting again from 1.

  long getNextId(void);
// return the next unused Id for this pool. Updates disk image.


  long peekNextId(void);
// return next unused Id for this pool. Does not update disk image.
// Does not check against disk image. Use at own risk. 



protected:
  static char * servPath;  // Pathname for directory with server files.
  RWCString fullPath;         // Full path and filename for the id server file.
  long nextId;      // next unused ID number within this pool.
  RWCString pool;   // Name of pool of IDs being served. 
  long check;       // Last value of nextId read from file
};

// Tired old trick to force static member initialization to follow proper
// order. See ARM, or Meyer, Effective C++, pp. 178-82.

class IdServ_init
{
public:
  static int count;
  IdServ_init();
  ~IdServ_init();
};
 
static IdServ_init IdServerInitializer;

#endif  IdServer_H

@

head	4.1;
access
	soccm;
symbols;
locks; strict;
comment	@ * @;


4.1
date	97.04.25.18.41.15;	author cui;	state Exp;
branches;
next	;


desc
@Include file for class AsmTimeLine, initial Doug's version
@


4.1
log
@*** empty log message ***
@
text
@// -*- Mode: C++; fill-column: 73; fill-prefix: "//# " -*-
#ifndef AsmTimeLine_H
#define AsmTimeLine_H

#define AsmTimeLineRcsId_H \
"$Id: AsmTimeLine.h,v 4.1 1995/07/11 14:55:07 cui Exp $"

// Description : A class for facilitating ASM rotation planning
// Author      : Douglas Alan <nessus@@mit.edu>
// Copyright   : Massachusetts Institute of Technology, 1995

// For documentation on all of the classes and methods declared in this
// file, see the ".C" file.

// .file AsmTimeLine.h
// .file AsmTimeLine.C
// .options -e -h

#include <mit_td.h>
#include <rw/cstring.h>
#include <rw/dvec.h>
#include <SOCTime2.h>

class AsmTimeLineGoodSegment;
class ostream;

//*****************************************************************************
//*****                                                                   *****
//*****                 SatelliteBearing: concrete class                  *****
//*****                                                                   *****
//*****************************************************************************

class SatelliteBearing
{
  //* Instance variables:
  DoubleVec			_position;
  DoubleVec			_orientation;
  RWCString                     _targetId;

public:

  //* Constructors, etc:
  SatelliteBearing();
  SatelliteBearing(const DoubleVec& position, const DoubleVec& orientation,
                   const RWCString& targetId);
  ~SatelliteBearing() {}
  //* Accessor methods:
  DoubleVec          position() const { return _position; }
  DoubleVec          orientation() const { return _orientation; }
  RWCString          targetId() const { return _targetId; }
  void               setPosition(const DoubleVec& pos) { _position = pos; }
  void		     setOrientation(const DoubleVec& tg) { _orientation = tg; }
  void               setTargetId(const RWCString& id) { _targetId = id; }

  //* Nonvirtual methods:
  void				_print(ostream& s) const;

};

ostream& operator<<(ostream& s, const SatelliteBearing& bearing);


//*****************************************************************************
//*****                                                                   *****
//*****                       Class hierachy                              *****
//*****                                                                   *****
//*****             AsmTimeLineSegment:     abstract base class           *****
//*****		    AsmTimeLineGoodSegment: node class	                  *****
//*****             AsmTimeLineBadSegment:  node class                    *****
//*****                                                                   *****
//*****************************************************************************

class AsmTimeLineSegment {
  friend class AsmTimeLine;
  friend class AsmTimeLineIter;
  friend class ConstAsmTimeLineIter;

  typedef AsmTimeLineSegment		This;

  //* Instance variables:
  This*					_nextSegment;
  const SOCTime2			_startTime;

  //* Private constructors, etc:
  virtual ~AsmTimeLineSegment();
  
  //* Private setter methods:
  void					setNextSegment(This* segment)
                                           { _nextSegment = segment; }
  //* Private methods:
  AsmTimeLineGoodSegment*		nextGoodSegment();
  const AsmTimeLineGoodSegment*		nextGoodSegment() const
                                           { return ((This*) this)->
					       nextGoodSegment();
					   }

protected:

  //* Protected constructors, etc:
  AsmTimeLineSegment(const SOCTime2& startTime, This* nextSegment = 0);

public:

  //* Accessor methods:
  const SOCTime2&			startTime() const { return _startTime;}
  This*					nextSegment() { return _nextSegment; }
  const This*				nextSegment() const
                                           { return _nextSegment; }



  //* Nonvirtual methods:
  const AsmTimeLineGoodSegment* 	castToGoodSegment() const
                                          { return ((This*) this)->
					       castToGoodSegment();
					   }

  //* Virtual casting methods:
  virtual AsmTimeLineGoodSegment*	castToGoodSegment();

  //* Virtual methods:
  virtual RWCString			adjective() const = 0;
  virtual void				_print(ostream& s) const;
  virtual void				print(ostream& s) const = 0;
};

ostream& operator<<(ostream& s, const AsmTimeLineSegment& segment);


//-----------------------------------------------------------------------------
// AsmTimeLineGoodSegment: concrete subclass of AsmTimeLineSegment
//-----------------------------------------------------------------------------

class AsmTimeLineGoodSegment: public AsmTimeLineSegment {

  typedef AsmTimeLineGoodSegment	This;
  typedef AsmTimeLineSegment		Base;

  //* Instance variables:
  SatelliteBearing			_satelliteBearing;

  //* Disallow copying and assignment:
  AsmTimeLineGoodSegment(const This&);
  operator=(const This&);

public:

  //* Constructors, etc:
  AsmTimeLineGoodSegment(const SOCTime2& startTime,
			 const SatelliteBearing& bearing,
			 Base* nextSegment = 0);

  //* Accessor methods:
  const SatelliteBearing&		satelliteBearing() const
    					   { return _satelliteBearing; }


  //* Virtual casting methods inherited from AsmTimeLineSegment:
  This*					castToGoodSegment();

  //* Virtual methods inherited from base class:
  RWCString				adjective() const;
  void					_print(ostream& s) const;
  void					print(ostream& s) const;
};


//-----------------------------------------------------------------------------
// AsmTimeLineBadSegment: concrete subclass of AsmTimeLineSegment
//-----------------------------------------------------------------------------

class AsmTimeLineBadSegment: public AsmTimeLineSegment {

  typedef AsmTimeLineBadSegment		This;
  typedef AsmTimeLineSegment		Base;

  //* Disallow copying and assignment:
  AsmTimeLineBadSegment(const This&);
  operator=(const This&);

public:
  
  //* Constructors, etc:
  AsmTimeLineBadSegment(const SOCTime2& startTime, Base* nextSegment = 0);

  //* Virtual methods inherited from base class:
  RWCString				adjective() const;
  void					_print(ostream& s) const;
  void					print(ostream& s) const;
};


//*****************************************************************************
//*****                                                                   *****
//*****                 AsmTimeLine: concrete class                       *****
//*****                                                                   *****
//*****************************************************************************

class AsmTimeLine
{
  friend class AsmTimeLineIter;
  friend class ConstAsmTimeLineIter;

  typedef AsmTimeLine		This;

  //* Instance variables:
  AsmTimeLineSegment*		_firstSegment;
  AsmTimeLineSegment*		_lastSegment;
  SOCTime2			_endTime;
  RWCString			_errorMessage;
  MitBool			_errorFlag;
  MitBool			_hasErrorBeenCleared;
  MitBool			_isConsistent;

  //* Disallow copying and assignment:
  AsmTimeLine(const This&);
  operator=(const This&);

  //* Private methods:
  void				setError(const RWCString& message);

public:

  //* Constructors, etc:
  AsmTimeLine(const SOCTime2& startTime, const SOCTime2& endTime,
	      const SatelliteBearing* bearing);

  ~AsmTimeLine();

  //* Accessor methods:
  const SOCTime2&		startTime() const
                                   { return _firstSegment->startTime(); }
  const SOCTime2&		endTime() const { return _endTime; }
  MitBool			errorFlag() const { return _errorFlag; }
  RWCString			errorMessage() const { return _errorMessage; }
  
  //* Nonvirtual methods:
  void				addBadTransition(const SOCTime2& startTime);
  void			 	addGoodTransition(
				     const SOCTime2& startTime,
				     const SatelliteBearing& bearing);
  void				clearError() { _hasErrorBeenCleared = mitTrue;}
  void				_print(ostream& s) const;
  void 			        update();
};

ostream& operator<<(ostream& s, const AsmTimeLine& timeLine);


//-----------------------------------------------------------------------------
// AsmTimeLineIter: iterator class
//-----------------------------------------------------------------------------

class AsmTimeLineIter {
  
  //* Instance variables:
  AsmTimeLineSegment*		_currentSegment;
  
public:
  
  //* Constructor:
  AsmTimeLineIter(AsmTimeLine& timeLine)
    : _currentSegment(timeLine._firstSegment)
  {}

  //* Nonvirtual methods:
  operator			MitBool() const
                                   { return MitBool(!!_currentSegment); }
  AsmTimeLineSegment&		operator*() const { return *_currentSegment; }
  AsmTimeLineSegment*		operator->() const { return _currentSegment; }
  AsmTimeLineIter&		operator++()
                                   { _currentSegment = 
				       _currentSegment->nextSegment();
				     return *this;
				   }
};


//-----------------------------------------------------------------------------
// ConstAsmTimeLineIter: const iterator class
//-----------------------------------------------------------------------------

class ConstAsmTimeLineIter {

  //* Instance variables:
  const AsmTimeLineSegment*	_currentSegment;
  
public:
  
  //* Constructor:
  ConstAsmTimeLineIter(const AsmTimeLine& timeLine)
    : _currentSegment(timeLine._firstSegment)
  {}

  //* Nonvirtual methods:
  operator			MitBool() const
                                   { return MitBool(!!_currentSegment); }
  const AsmTimeLineSegment&	operator*() const { return *_currentSegment; }
  const AsmTimeLineSegment*	operator->() const { return _currentSegment; }
  ConstAsmTimeLineIter&		operator++()
                                   { _currentSegment = 
				       _currentSegment->nextSegment();
				     return *this;
				   }
};

#endif // AsmTimeLine_H
@

head	5.0;
access
	soccm;
symbols;
locks
	buehler:5.0; strict;
comment	@ * @;


5.0
date	96.10.24.23.47.23;	author buehler;	state Exp;
branches;
next	;


desc
@Convert celestial coordinates of satellite to earth coordinates,
given time.
@


5.0
log
@initial revision.
@
text
@//============================================================================
// toLatLong.C
// Description : A program that converts a unit vector in celestial Cartesian 
//               coordinates to the earth coordinates: (latitude, longitude),
//               at a specified time.
// Author      : Wei Cui <cui@@space.mit.edu>
// Copyright   : Massachusetts Institute of Technology, 1996
static const char rcsid[] = 
"$Id: $";
//============================================================================

#include <math.h>
#include <rw/dvec.h>
#include <rw/dgenmat.h>
#include "arpConst.h"

extern DoubleGenMat Rot1Axis (int naxis, double theta);

DoubleVec toLatLong(double time, const DoubleVec& x)
{
    double U2G=6.0+35./60.+48.75/3600.0;
    double pday = (time - TZERO)/3600.0/24.0;
    double ang = pday*solarToSid + U2G/24.0;
    ang = ang - int(ang);
    ang = ang*2.0*pi;
    DoubleVec retVec(2, 0.0);
    DoubleGenMat mat = Rot1Axis(3, ang);
    DoubleVec pos = product(mat, x);
    double ampl = sqrt(pos(0)*pos(0)+pos(1)*pos(1));
    retVec(0) = atan(pos(2)/ampl)*r2d;
    retVec(1) = atan2(pos(1),pos(0))*r2d;
    
    return retVec;
}

@

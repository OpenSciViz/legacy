head	5.0;
access
	soccm;
symbols;
locks
	buehler:5.0; strict;
comment	@ * @;


5.0
date	96.03.13.21.48.55;	author buehler;	state Exp;
branches;
next	4.1;

4.1
date	95.11.22.19.40.48;	author buehler;	state Exp;
branches;
next	4.0;

4.0
date	95.08.22.20.38.12;	author buehler;	state Exp;
branches;
next	;


desc
@A version of the rotation planner which runs without any ephemeris
file, ignores target positions and occultations. Its purpose is to
guarantee that the ASM keeps turning, even if there are no observations
or something is terribly wrong with the ephemeris.
@


5.0
log
@postlaunch version.
@
text
@//===========================================================
// mp_AsmRotPlan_short.C
// Description : A simple-minded ASM rotation planner.
// Author      : Wei Cui <cui@@space.mit.edu>
// Copyright   : Massachusetts Institute of Technology, 1995
// RCS stamp:
static const char rcsId[] = "$Id: mp_AsmRotPlan_short.C,v 4.1 1995/11/22 19:40:48 buehler Exp $";
//===========================================================

#include <math.h>
#include <stdlib.h>
#include <stream.h>

#include <CmdArgs.h>
#include <EventRoster.h>
#include "SPTScheduleId.h"
#include "LispNode.h"
#include "AsmDwell.h"
#include "AsmDwellSeq.h"
#include "arpConst.h"
#include "DWseqConst.h" 

//#----------------------------------------------------------------------------
//# Forward declarations:
//#----------------------------------------------------------------------------

static void error(const RWCString& err_msg, int err_code);
static void dwells(ofstream& osum,ofstream& odb,EventRoster* er,
		   const RWCString& ObsId,AsmDwellSeq& ds,int num);
static void slew(ofstream& osum,EventRoster* er,const RWCString& ObsId,
                 AsmDwellSeq& ds,double slew1,double slew2);

//#----------------------------------------------------------------------------
//# main(): The program.  
//#----------------------------------------------------------------------------

int main(int argc, char* argv[])
{
//* set up error handler.  
    RWCString err_msg;


//#----------------------------------------------------------------------------
//# Declare command line arguments:
//#----------------------------------------------------------------------------

    CmdArgStr inEventsStr('i', "in_event", "file", "the input event file.",
			  CmdArg::isREQ);
    CmdArgStr outputFileStr('o', "out_event", "file", "the output event file.",
			    CmdArg::isREQ);
    CmdArgStr raDecFileStr('r', "ra_dec", "file", 
			   "the file with source RA and Dec.", CmdArg::isREQ);
    CmdArgStr summaryFileStr('s', "summary", "file", 
			     "the Dwell plan summary.", CmdArg::isREQ);  
    CmdArgStr DBFileStr('b', "data_base", "file", 
			"the data analysis summary.", CmdArg::isREQ);
  
//* Declare the command line, and the argument iterator:
    CmdLine cmdLine(*argv, &outputFileStr, &inEventsStr, &raDecFileStr, 
                    &summaryFileStr, &DBFileStr, 0);
    cmdLine.flags(CmdLine::KWDS_ONLY);
    CmdArgvIter argIter(--argc, ++argv);  

//* Parse the command line arguments:
    cmdLine.parse(argIter);
  

//#----------------------------------------------------------------------------
//# Open the output stream:
//#----------------------------------------------------------------------------

    RWCString eventOutName(outputFileStr);
    if (eventOutName.subString(".events").isNull() ) {
	eventOutName += ".events";
    }
    ofstream outf(eventOutName, ios::out);
    if (!outf.good()) {
	err_msg = " unable to open output file! ";
	err_msg += eventOutName;
	error(err_msg, 1);
    }
    outf << setprecision(12);
  
//#----------------------------------------------------------------------------
//# Open the RA&Dec table:
//#----------------------------------------------------------------------------

    RWCString raDecName(raDecFileStr);
    if(raDecName.subString(".positions") .isNull()) {
	raDecName += ".positions";
    }
    ifstream raDecFile(raDecName, ios::in);
    if (! raDecFile.good()) {
	err_msg = " unable to open target file! ";
	err_msg += raDecName;
	error(err_msg, 2);
    }


//#----------------------------------------------------------------------------
//# Open the dwell sequence summary file:
//#----------------------------------------------------------------------------

    RWCString summaryName(summaryFileStr);
    ofstream summaryFile(summaryName, ios::out);
    if (!summaryFile.good()) {
	err_msg = " unable to open dwell sequence summary file! ";
	err_msg += summaryName;
	error(err_msg, 3);
    }
  

//#----------------------------------------------------------------------------
//# Open the planning dwell analysis database file:
//#----------------------------------------------------------------------------

    RWCString DBName(DBFileStr);
    ofstream DBFile(DBName, ios::out);
    if (!DBFile.good()) {
	err_msg = " unable to open output file for pointing file! ";
	err_msg += DBName;
	error(err_msg, 4);
    }
    else {
      DBFile << "START_TIME\tDWELL_SEQ_ID\tDWELL_ID\tDwell_DURATION\tSSC1_RA\t\tSSC1_DEC\tSSC1_ROLL\tSSC2_RA\t\tSSC2_DEC\tSSC2_ROLL\tSSC3_RA\t\tSSC3_DEC\tSSC3_ROLL\tDWELL_POS"<<endl;
      DBFile << endl;
    }

  

//#----------------------------------------------------------------------------
//# Open and read in the Event Roster file:
//#----------------------------------------------------------------------------

    RWCString eventinName(inEventsStr);
    if (eventinName.subString(".events").isNull() ) {
	eventinName += ".events";
    }    
    ifstream inEvents(eventinName, ios::in);    
    if (!inEvents.good()) {
	err_msg = " unable to open input file! ";
	err_msg += eventinName;
	error(err_msg, 2);
    }
  
    EventRoster* aRoster = (EventRoster *) 0;
    LispNode* rootNode = new LispNode();
    rootNode->setType(LispNode::ROOT);
    LispNode* nextNode = parseLisp(inEvents, 0);
    rootNode->setFirstChild(nextNode);
    SPTScheduleId aSched;
    aRoster = procRosterRoot( *rootNode, aSched );

    SOCTime2 STime = aRoster->getStartTime();
    SOCTime2 ETime = aRoster->getEndTime();

    cout << "\nThe event roster starts at " << STime 
	 << ", and ends at " << ETime << endl;

    cout << "\n[Start adding dwell events to the event roster ..." << flush;

    RWCString ObsId[100], dum;
    SOCTime2 ObsTime[100];
    int ObsNum = 0;
    DoubleVec target(3, 0.0);
    
    ObsTime[0] = STime;
    raDecFile.seekg(0, ios::beg);
    if(!raDecFile.eof()) {
        raDecFile >> dum >> ObsId[ObsNum] >> dum >> target(0) >> dum
                  >> target(1) >> dum >> target(2);
    }
    else {
        cout << "Please enter the observation id of last week's observation: "
             << flush;
        cin >> ObsId[ObsNum];
    }
    ObsNum++;
    OrbitalEvent *orbEvent = 0;    
    if (aRoster) {
	for (int ii = 0; ii  <  aRoster->length() ; ii++) {
	    orbEvent = aRoster->at(ii);
	    switch (orbEvent->mEvent) {
	    case OET_SLEW:
		ObsTime[ObsNum] = orbEvent->mTime;
                ObsId[ObsNum] = orbEvent->mObs;
                ObsNum++;
		break;

	    default:
                break;
	    }
	}
    }

//* Initialize the dwell sequence parameters
    SOCTime2 dsTime = STime;
    double dsPos = antiHOME;
    double dsSlew1 = -Frange;
    double dsSlew2 = 0.0;
    int dsNum = 0;
    double dsDur = int(DwellDur+DwellStep/AsmSlewRate+AsmSettleTime)+1.0;
    AsmDwellSeq ds(dsTime,dsPos,dsSlew1,dsSlew2,dsNum,dsDur,DwellStep,target);
    
//* Initial full rewind
    unsigned startOff = 120;
    ds.setPeriod(DwellStep);
    summaryFile << "Initial ASM slew: " << endl;
    slew(summaryFile,aRoster,ObsId[0],ds,dsSlew1,dsSlew2);
    dsTime = ds.getTime();
    ds.setTime(dsTime+startOff);
    dsTime = ds.getTime();
    int Rndwell = int((antiHOME - HOME)/DwellStep);
    double dwTime = Rndwell*dsDur+EAstart+DStimeOffset+DWmargin;
    double slTime = (antiHOME - HOME)/AsmSlewRate+AsmSettleTime
        +EAstart+DStimeOffset+DWmargin;
    ObsTime[ObsNum] = ETime-(dwTime+slTime);
    
    int ndum = 1;
    for(int snum = 0; snum < ObsNum; snum++) {
        while(dsTime < ObsTime[snum+1]) {
            dwells(summaryFile,DBFile,aRoster,ObsId[snum],ds,Rndwell);
            dsTime = ds.getTime();
            ndum = 1;
            while((dsTime>=ObsTime[snum+ndum])&&((snum+ndum)<ObsNum)) ndum++;
            if((snum+ndum)>=ObsNum) ndum = ObsNum-snum;
            ndum--;
            slew(summaryFile,aRoster,ObsId[snum+ndum],ds,dsSlew1,dsSlew2);
            dsTime = ds.getTime();
        }
    }
    double endtime = EventRoster::socToMjd(ETime);
    double spikeTime = EventRoster::socToMjd(dsTime);
    if(dsTime < (ETime-dwTime)) 
        Rndwell = int(((endtime-spikeTime)/Seconds2Day
                       -(EAstart+DStimeOffset+DWmargin))/dsDur);
    dwells(summaryFile,DBFile,aRoster,ObsId[ObsNum-1],ds,Rndwell);
    cout << "\n... end adding dwell events to the event roster]" << endl;

    cout << "\n[Start writing out the modified event roster ..." << flush;
    aRoster->printEventsFile(outf);
    cout << "\n... end writing out the modified event roster]" << endl;
    
//* the code ran successfully, return 0
    return 0;
}



//#----------------------------------------------------------------------------
//# dwells(): local procedure. Given the number of dwells, it builds a dwell 
//# sequence, insert it to the event roster, write to the dwell sequence 
//# summary file, and make entries to the planning dwell analysis database.
//#----------------------------------------------------------------------------

static void
dwells(ofstream& osum,ofstream& odb,EventRoster* er,const RWCString& ObsId, 
       AsmDwellSeq& ds,int num)
{
    RWCString dsName, errmsg; 
    if(num > 0) {
	OrbitalEvent* orbEvent = new OrbitalEvent();  
	ds.clear();
	ds.setSlew1(0.0);
	ds.setSlew2(0.0);
	ds.setNum(num);
	ds.addDwell();
	dsName = ds.makeFile();
	osum << ds;
	ds.writeDwell(odb);
	orbEvent->mEvent = OET_DWELL;
	orbEvent->mObs = ObsId;
	orbEvent->mTime = ds.getTime();
	orbEvent->mDwell = dsName;
	er->insert(orbEvent);
	ds.update();
    }
    else {
        errmsg = " A null dwell sequence is dropped! ";
        error(errmsg, 23);
    }
    if(!ds.checkBound()) {
	cout << " \nAt time " << ds.getTime() << ", " << flush;
	cout << " the ASM is at " << ds.getPos() << endl;
	errmsg = " The ASM is out of the rotation boundaries! ";
	error(errmsg, 20);
    }
}


//#----------------------------------------------------------------------------
//# slew(): local procedure. Given slew, it builds a dwell sequence, insert it
//# to the event roster, and write to the dwell sequence summary file. 
//#----------------------------------------------------------------------------

static void
slew(ofstream& osum,EventRoster* er,const RWCString& ObsId,AsmDwellSeq& ds,
     double slew1,double slew2)
{
    RWCString dsName, errmsg; 
    int Islew1=(int)fabs(slew1/AsmDegree),Islew2=(int)fabs(slew2/AsmDegree);
    if(Islew1 || Islew2) {
	OrbitalEvent* orbEvent = new OrbitalEvent();
	ds.setNum(0);
	ds.setSlew1(slew1);
        ds.setSlew2(slew2);
	dsName = ds.makeFile();
	osum << ds;
	orbEvent->mEvent = OET_DWELL;
	orbEvent->mObs = ObsId;
	orbEvent->mTime = ds.getTime();
	orbEvent->mDwell = dsName;
	er->insert(orbEvent);
	ds.update();
    }
    else {
        errmsg = " A null dwell sequence is dropped! ";
        error(errmsg, 23);
    }
    if(!ds.checkBound()) {
	cout << " \nAt time " << ds.getTime() << ", " << flush;
	cout << " the ASM is at " << ds.getPos() << endl;
	errmsg = " The ASM is out of the rotation boundaries! ";
	error(errmsg, 20);
    }
}    


//#----------------------------------------------------------------------------
//# error(): issues warnings, or terminates the program with a return status
//# after outputting a fatal error message.
//#----------------------------------------------------------------------------

static void
error(const RWCString& err_msg, int err_code) 
{
    if(err_code <= 20) { 
	cerr << " \nFatal[" << err_code << "]: mp_AsmRotPlan_short Aborting; " 
	     << err_msg << ".\n";
	exit(err_code);
    }
    else
	cerr << " \nWarning[" << err_code << "]: " << err_msg << "\n";
}





@


4.1
log
@IOC version, handles week boundaries.
@
text
@d7 1
a7 1
static const char rcsId[] = "$Id: mp_AsmRotPlan.C,v 4.11 1995/05/25 21:05:36 buehler Exp $";
d202 1
a202 1
    double dsDur = DwellDur + DwellStep/AsmSlewRate + AsmSettleTime;
d206 1
d211 2
a212 1
    
@


4.0
log
@Initial version.
@
text
@d30 2
a31 2
static void slew(ofstream& osum,EventRoster* er,
		 const RWCString& ObsId,AsmDwellSeq& ds,double slew);
d51 2
d59 2
a60 2
    CmdLine cmdLine(*argv, &outputFileStr, &inEventsStr, &summaryFileStr, 
		    &DBFileStr, 0);
d78 1
a78 1
	err_msg = " unable to open output file ";
d85 16
d107 1
a107 1
	err_msg = " unable to open dwell sequence summary file ";
d120 1
a120 1
	err_msg = " unable to open output file for analysis database ";
d141 1
a141 1
	err_msg = " unable to open input file ";
a159 1

d162 3
a164 2
//* dum observation ID and dum target
    RWCString ObsId ("NIL");
d166 23
d190 6
d199 2
a200 1
    double dsSlew = -Frange;
d203 3
a205 3
    double SpikeMjd = EventRoster::socToMjd(dsTime);

    AsmDwellSeq ds(dsTime,dsPos,dsSlew,dsNum,dsDur,DwellStep,target);
d208 2
a209 1
    slew(summaryFile,aRoster,ObsId,ds,dsSlew);
a210 1
    double endtime = EventRoster::socToMjd(ETime);
d212 17
a228 6
    double totTime = Rndwell*dsDur+Frange/AsmSlewRate+AsmSettleTime;
    while(SpikeMjd < (endtime - totTime*Seconds2Day)) {
	dwells(summaryFile,DBFile,aRoster,ObsId,ds,Rndwell); 
	slew(summaryFile,aRoster,ObsId,ds,dsSlew);
	dsTime = ds.getTime();
	SpikeMjd = EventRoster::socToMjd(dsTime);
d230 6
a235 1

d262 2
a263 1
	ds.setSlew(0.0);
d276 4
d296 1
a296 1
     double slew)
d299 2
a300 1
    if(slew != 0.0) {
d303 2
a304 1
	ds.setSlew(slew);
d314 4
d326 1
d336 1
a336 1
	cerr << " \nFatal[" << err_code << "]: mp_AsmRotPlan Aborting; " 
d341 1
a341 1
	cerr << " \nWarning[" << err_code << "]: " << err_msg << ".\n";
d343 2
@

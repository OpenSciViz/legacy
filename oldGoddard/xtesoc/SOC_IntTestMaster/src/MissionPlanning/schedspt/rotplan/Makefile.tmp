# Description: Makefile for ASM Rotation planning software.
# RCS Stamp: $Id: Makefile,v 5.5 1996/10/24 23:48:56 buehler Exp $
# Author:    Ed Morgan
# Copyright: MIT Center for Space Research

# Uses gnumake.
# Environment must define SOCHOME, SOCLOCAL.
# Environment should define CCROOT_I to avoid superfluous warnings.

#gnumake has the following default implicit rules relevant here:
# .o from .C or .cc   $(CXX) -c $(CPPFLAGS) $(CXXFLAGS) $< -o $@
# n from n.o          $(CC) $(LDFLAGS) n.o $(LOADLIBS) $^ -o $@
# n from RCS/n,v      $(CO) $(COFLAGS)

#Please build objects by default on my local bin directory
%.o: %.C
	$(CXX) -c  $(CPPFLAGS) $(CXXFLAGS) $< -o $@

CXX = CC
CC=cc
CO=co

# Rule to get header and source files out of RCS if the RCS file is changed.
# Do this only if the environment variable FORCE_CO is set.
ifdef FORCE_CO
%.C:RCS/ %.C,v; co %.C
%.h:RCS/ %.h,v; co ../inc/%.h
%.1:RCS/ %.1,v; co %.1
%.csh:RCS/ %.csh,v; co %.csh
endif
ifndef CGMIT_FLAGS
CGMIT_FLAGS=
endif

HERE:=$(shell pwd)

INC_PATH=-I../inc -I$(SOCHOME)/include  -I$(SOCLOCAL)/include
CPPFLAGS= -g $(INC_PATH) -DMIT_CMDGEN $(CGMIT_FLAGS)
LIB_PATH:= -L$(SOCHOME)/lib/$(ARCH)  -L$(SOCLOCAL)/lib 
OC_LIBS = -lObsSced -lUtil -lmitutil -lrwmth -lrwtool -lm
LIBS:=  -lrotplan $(OC_LIBS)
LOADLIBS= $(LIBS)
LDFLAGS= $(LIB_PATH)
CXXFLAGS =-ptr./ptrepository.$(ARCH) 
MAN_PAGES=mp_AsmRotPlan.1 mp_AsmRotPlan_short.1
TEST_EPHEMS= X1995037EPHXTE.01 X1996017EPHSH.cur 
TEST_SHORT_INPUTS= sts2.events sts2.positions arpScriptTest.in1 \
 arpScriptTest.in2
TEST_INPUT_FILES=  $(TEST_SHORT_INPUTS)
  
TEST_OUTPUT_FILES= ssts2 osts2.events dsts2 arpScriptTest.out1 \
 arpScriptTest.out2
TEST_SCRIPTS= AsmRotTest.csh checkEvents arpScriptTest.csh stripArpEvts.csh
TEST_DOCS= tp_mp_AsmRotPlan.doc

# Separate template repository for objectcenter.
CLXFLAGS:=-pta -ptr./ptocrepository.$(ARCH)

vpath %.h ../inc:$(SOCHOME)/include
vpath %.h,v ../inc
vpath %.o .
vpath %.a $(SOCHOME)/lib/$(ARCH)
vpath %.C .:../tst
vpath %.C,v ../tst

# LIBRARY is the name of the library this subdirectory builds
LIBRARY:=$(SOCHOME)/lib/$(ARCH)/librotplan.a

#Sources for objects to be placed in public library.
SOURCES:= AsmDwell.C AsmDwellSeq.C AsmTimeLine.C Sun_pos.C ASMcoord.C \
  XTEcoord.C Rot1Axis.C eulerRotMat.C getPA.C radecTOxyz.C xyzTOradec.C \
  cross.C dmin.C myAtan2.C intToString.C roundToMinute.C daToPoint.C \
  Sat_pos.C toLatLon.C

#Sources that do not go into the public library
AUX_SOURCES:= mp_AsmRotPlan.C mp_AsmRotPlan_short.C

OPS_SCRIPTS:= AsmRotPlan.csh wap2plan.csh
#public header files
ICD_INCLUDES:=
SOC_INCLUDES:=  $(ICD_INCLUDES) DWseqConst.h AsmDwell.h AsmDwellSeq.h \
   AsmTimeLine.h arpConst.h badRegions.h
SOC_INCDEPS:= $(SOC_INCLUDES:%.h=../inc/%.h)

#all header files
INCLUDES:= $(SOC_INCLUDES) 
INCDEPS:= $(INCLUDES:%.h=../inc/%.h)

# Object files to go into library libcgeds
OBJECTS:= $(SOURCES:.C=.o)

#-------------------TARGETS START HERE---------------------------

$(LIBRARY): $(LIBRARY)($(OBJECTS))
	chmod 664 $@
	ranlib $@

#implicit rule for building executable from source
%: %.C
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS) $<\
	 -o $@ $(LOADLIBS) -lCmdLine 
#	$(RM) $@.o

#implicit rule for building library member from source
(%.o): %.C
	$(CXX) -c $(CXXFLAGS) $(CPPFLAGS)  $< -o $%
	$(AR) $(ARFLAGS) $@ $%
#	ranlib $@
#	$(RM) $%

mp_AsmRotPlan: mp_AsmRotPlan.o $(LIBRARY) 
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS) mp_AsmRotPlan.o\
	-o mp_AsmRotPlan $(LOADLIBS)	-lCmdLine
mp_AsmRotPlan_short: mp_AsmRotPlan_short.o $(LIBRARY) 
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS) mp_AsmRotPlan_short.o\
	-o mp_AsmRotPlan_short $(LOADLIBS)	-lCmdLine

test: 

symbolicRelease:
	rcs -N$(SYMNAME): -s$(STATUS) RCS/*,v
	cd test; rcs -N$(SYMNAME): -s$(STATUS) RCS/*,v

#Objectcenter targets. make <executable>.cl in objectcenter.
%.cl: %.C $(INCLUDES) $(SOURCES)
	#setopt tmpl_instantiate_flg $(CLXFLAGS) $(CPPFLAGS)
	#load  $(CPPFLAGS) $< $(SOURCES) \
            $(LIB_PATH)  $(OC_LIBS) -lCmdLine
	#link


#cleanish: Clear executables, core dump and object files
cleanish:
	-$(RM) *.o core

#clean: Clear the above, backup files, template repositories, and library
clean: cleanish
	-$(RM) *~ *.bak *..c
	-cd ../inc; $(RM) *~
	-$(RM) ptrepository.$(ARCH)/*
	-rmdir ptrepository.$(ARCH)
	-$(RM) ptocrepository.$(ARCH)/*
	-rmdir ptocrepository.$(ARCH)


#realclean -do all the above and an rcsclean
realclean: clean
	rcsclean
	cd ../inc; rcsclean

install-test: $(TEST_INPUT_FILES:%=../tst/%) $(TEST_OUTPUT_FILES:%=../tst/%) \
  $(TEST_SCRIPTS:%=../tst/%) $(TEST_DOCS:%=../tst/%)
	-cd ../tst; chmod ug+x $(TEST_SCRIPTS)
	-cd $(SOCHOME)/test/inputs; $(RM)  $(TEST_INPUT_FILES)
	-cd ../tst; cp $(TEST_SHORT_INPUTS) $(SOCHOME)/test/inputs
	-cd $(SOCHOME)/test/inputs; $(RM) $(TEST_EPHEMS)
	ln -s $(HERE)/../tst/RCS/X1995037EPHXTE.01.transfer \
   $(SOCHOME)/test/inputs/X1995037EPHXTE.01
	ln -s $(HERE)../tst/RCS/X1996017EPHSH.cur.transfer \
   $(SOCHOME)/test/inputs/X1996017EPHSH.cur
	-cd $(SOCHOME)/test/outputs; $(RM) $(TEST_OUTPUT_FILES)
	-cd ../tst; cp $(TEST_OUTPUT_FILES) $(SOCHOME)/test/outputs
	-cd $(SOCHOME)/test/scripts; $(RM) $(TEST_SCRIPTS)
	-cd ../tst; cp $(TEST_SCRIPTS) $(SOCHOME)/test/scripts
	-cd $(SOCHOME)/doc; $(RM) $(TEST_DOCS)
	-cd ../tst; cp $(TEST_DOCS) $(SOCHOME)/doc

#-----------------SOC  installation---------------------

pubhdrs:  $(INCDEPS)
	-cd $(SOCHOME)/include; $(RM) $(SOC_INCLUDES)	
	-cp -p $(SOC_INCDEPS) $(SOCHOME)/include
	-cd $(SOCHOME)/include; chmod ug+w $(SOC_INCLUDES)

publibs:  $(LIBRARY)($(OBJECTS)) 
	chmod 664 $(SOCHOME)/lib/$(ARCH)/librotplan.a
	ranlib $(SOCHOME)/lib/$(ARCH)/librotplan.a

manpages: $(MAN_PAGES)
	@ if [ ! -d $(SOCHOME)/man ]; \
             then mkdir $(SOCHOME)/man; \
	     else :; \
             fi
	@ if [ ! -d $(SOCHOME)/man/man1 ]; \
	     then mkdir $(SOCHOME)/man/man1; \
             else :; \
	     fi
	$(foreach file, $(MAN_PAGES), \
	     rm -f $(SOCHOME)/man/man1/$(file);\
             cp $(file) $(SOCHOME)/man/man1; )

install: $(AUX_SOURCES:%.C=%) manpages $(OPS_SCRIPTS)
	-cd $(SOCHOME)/bin/$(ARCH); $(RM) $(AUX_SOURCES:%.C=%) $(OPS_SCRIPTS) 
	 cp  $(AUX_SOURCES:%.C=%) AsmRotPlan.csh $(SOCHOME)/bin/$(ARCH)
	-rm mp_AsmRotPlan 
	 chmod 555 $(SOCHOME)/bin/$(ARCH)/AsmRotPlan.csh
	 chmod 555 $(SOCHOME)/bin/$(ARCH)/wap2plan.csh
	-cd $(SOCHOME)/bin/$(ARCH); chmod 755  $(AUX_SOURCES:%.C=%)

uninstall:
	-cd $(SOCHOME)/bin/$(ARCH); $(RM) $(AUX_SOURCES:%.C=%) AsmRotPlan.csh
	-cd $(SOCHOME)/test/inputs; $(RM)  $(TEST_INPUT_FILES)
	-cd $(SOCHOME)/test/outputs; $(RM) $(TEST_OUTPUT_FILES)
	-cd $(SOCHOME)/test/scripts; $(RM) $(TEST_SCRIPTS)


# ----------Closure of templates in library member----------------------

# To prevent doing ar at each make, and to encapsulate templates used
# in library but not mentioned explicitly in include files, the "dummy"
# targets are used to place all template instances referred to in the
# library into library members.
PTREP=ptrepository.$(ARCH)
CLOSEFLAGS:= -pta  -pth -ptr../$(PTREP) -I../../inc
closure:
	-mkdir pttemp
	-cd pttemp; ar x $(LIBRARY) $(OBJECTS);
	-cd pttemp; $(CXX) $(CLOSEFLAGS) $(CPPFLAGS) $(OBJECTS) 
	$(RM) -r pttemp
	ar rv $(LIBRARY) $(strip $(PTREP))/*.o
	ranlib $(LIBRARY)
#make sure we have the ptrep directory for the dummy files
dummy.$(ARCH):
	mkdir $@

# ilist. adds the template objects to the library with ar
ilist.d.$(ARCH): ilist.$(ARCH)
	ar rv $(LIBRARY) `cat ilist.$(ARCH)`
	@touch ilist.d.$(ARCH) 

#------------------dependency checking----------------

make.d.$(ARCH): 
	touch $@
makeaux.d.$(ARCH):
	touch $@

depend: make.d.$(ARCH) $(SOURCES) makeaux.d.$(ARCH) $(AUX_SOURCES) 
	@touch make.d.tmp
	makedepend -fmake.d.tmp -I$(CCROOT_I) $(CPPFLAGS) \
          -- $(SOURCES)
#	@echo LIBRARY is $(LIBRARY)
	sed 's/\(.*[.]o\)/$$(LIBRARY)\(\1\) /' make.d.tmp > make.d.$(ARCH)
	@$(RM) make.d.tmp
	@touch makeaux.d.$(ARCH)
	makedepend -fmakeaux.d.$(ARCH) -- -I$(CCROOT_I) $(CPPFLAGS) \
          -- $(AUX_SOURCES)
	@$(RM) makeaux.d.tmp
#	@echo "Scads of error messages? Set var CCROOT_I to the C++ includes"

#dependency files
#  Note: following file includes makeaux.d.$(ARCH).
include make.d.$(ARCH)





head	3.0;
access
	soccm;
symbols
	Build4_3_1:3.0
	Build4_3:3.0
	OldSOCTime:3.0
	Build4_2:3.0
	For_Build4_1:3.0
	E2e:3.0
	ItsAlive:3.0
	Build4:3.0
	Current:3.0
	Build3_3:3.0
	Build3_2:3.0;
locks
	buehler:3.0; strict;
comment	@;;; @;


3.0
date	94.04.01.19.36.12;	author buehler;	state Submitted;
branches;
next	;


desc
@Sample file, showing how fast-event can be subclassed & modified.
@


3.0
log
@Initial XTE version.
@
text
@;;; -*- Mode:Common-Lisp; Package:USER; Base:10 -*-
;;;
;;; hst-fast-event.lisp
;;;
;;; $Log: hst-fast-event.lisp,v $
;;; Revision 1.1  1991/11/26  15:40:41  johnston
;;; Initial revision
;;;
;;;
;;; HST-specific event calculation and suitability
;;;
;;; creation 16Nov91 MDJ from fast-event.lisp
;;;

#| ----------------------------------------------------------------------
Most HST-specific changes should be possible in this file only, without
affecting fast-event.lisp   Example:  adding GS acq/re-acq times to 
orbital viewing suitability calculation. All required changes are in 
hst-fast-event.lisp

1.  Add new slot 'acq-time to HST-EVENT-CALC to record acq time

2.  Create (or modify) an :AFTER method for 
SET-VIEWING-PARMS-FOR-EVENT-CALCULATION for the class 'hst-event-calc 
to set the value of 'acq-time slot.

3.  Create (or modify) an :AROUND method for 
GET-VIEWING-PARMS-FOR-EVENT-CALCULATION for the class 'hst-event-calc
to change the cache key to include 'acq-time

4.  Modify the function SET-HST-EVENT-CALC-ORBITAL-VIEWING to take 
acq-time as a keyword argument and call SET-VIEWING-PARMS-FOR-EVENT-CALCULATION
with the desired value.

5.  Override or wrap the method COMPUTE-ORBITAL-VIEWING-SUITABILITY for the
class 'hst-event-calc to use the new parameter for computing the suitability.
Similarly, override or wrap any other methods used to compute suitability.
---------------------------------------------------------------------- |#


;;; ----------------------------------------------------------------------
;;;                                                 Class:  HST-EVENT-CALC
;;; ----------------------------------------------------------------------

(defclass HST-EVENT-CALC (event-calc)
  (
   (acq-time 
    :initform nil :reader acq-time
    :documentation
    "GS acq or re-acq time, days, if relevant.  GS acqs and re-acq can 
      always occur in bright time, whether or not the observation requires
      dark time.")
   )
  (:documentation "for recording event calculation parameters"))


;;; ----------------------------------------------------------------------
;;;                                    The current HST-EVENT-CALC instance
;;; ----------------------------------------------------------------------

(defvar *CURRENT-HST-EVENT-CALC* nil
  "the currently active HST-event-calc instance")

(setf *CURRENT-HST-EVENT-CALC* nil)


(defun CURRENT-HST-EVENT-CALC ()
  
  "return current HST-event-calc instance, or create and return if one does
    not exist."
  
  (or *CURRENT-HST-EVENT-CALC*
      (setf *CURRENT-HST-EVENT-CALC* (make-instance 'HST-event-calc))))


;;; ----------------------------------------------------------------------
;;;           External calls to set up the current HST-EVENT-CALC instance
;;; ----------------------------------------------------------------------

;;;
;;; --- externally callable functions to set up event calc parameters ---
;;;


(defun SET-HST-EVENT-CALC-POINTING (&key name ra dec position-vector
                                         (units :DEGREES)
                                         (cell-center nil)
                                         (observer *ST*)
                                         (observer-name "HST")
                                         (target-type :FIXED-TARGET)
                                         (position-data nil)
                                         (visibility-pcf nil))
  
  "Set up pointing data for event calculation.  Usage:
    :name is print name (optional)
    :ra and :dec OR :position-vector can be specified
    :units - :RADIANS or :DEGREES.  Only used if :ra and :dec are supplied.
      default is :DEGREES
    :cell-center - if t, the position is adjusted to the nearest cell
      center.  Default is nil
    :observer - instance of viewing satellite, default *ST*
    :observer name - should be specified if observer
      instance does not exist when this function is called.  Default is \"HST\"
    :target-type can be :FIXED-TARGET (default) or :MOVING-TARGET
    :position-data - required only for :moving-target
    :visibility-pcf - required only for :moving-target"

  ;; (set-event-calc-pointing :name :fred :ra 12 :dec -15 :units :degrees)
  (cond ((eq target-type :fixed-target)
         (set-target-for-event-calculation
          (current-hst-event-calc)
          :target-name (string-it name)
          :target-ra ra 
          :target-dec dec 
          :target-position-vector position-vector
          :ra-dec-in-degrees (cond ((eq units :degrees) t)
                                   ((eq units :radians) nil)
                                   (t (error "unknown units")))
          
          :cell-center cell-center 
          :the-observer observer
          :the-observer-name observer-name))
        ((eq target-type :moving-target)
         (set-moving-target-for-event-calculation
          (current-hst-event-calc)
          :target-name (string-it name)
          :position-data position-data
          :visibility-pcf visibility-pcf
          :the-observer observer
          :the-observer-name observer-name))
        (t (error "unrecognized target type ~a" target-type))))


(defun SET-HST-EVENT-CALC-ROLL (roll-type 
                                &key roll-offset-angle 
                                roll-angle)
  
  "Set roll (orientation) for event calculation.  Must be set after
    pointing is specified (with SET-EVENT-CALC-POINTING).  roll-spec
    for low-earth-orbit-satellites is of the form:
     nil - clear roll spec
     :nominal - use nominal roll
     :offset-from-nominal :roll-offset-angle <angle> 
     :fixed :roll-angle <angle>
    Angles are in radians.  Examples:
     (set-event-calc-roll nil) ; clear roll
     (set-event-calc-roll :nominal)
     (set-event-calc-roll :offset-from-nominal :roll-offset-angle 0.1)
     (set-event-calc-roll :fixed :roll-angle -0.1)
     "
  (let (roll-spec)
    (cond ((or (eq roll-type nil)
               (eq roll-type :nominal))
           (setf roll-spec (list roll-type)))
          ((eq roll-type :offset-from-nominal)
           (setf roll-spec (list roll-type :roll-offset-angle roll-offset-angle)))
          ((eq roll-type :fixed)
           (setf roll-spec (list roll-type :roll-angle roll-angle)))
          (t (error "invalid roll type: ~a" roll-type)))
    (set-roll-for-event-calculation (current-hst-event-calc) roll-spec)))


(defun SET-HST-EVENT-CALC-DATES (&key start end (default-sample-interval 1))
  
  "Set up date ranges for event calculation.  Usage:
    :start and :end are required, may be in date or time format
    e.g. '1-jan-1991, 4289.5, etc.
    :default-sample-interval is in days, default is 1"
  ;; (set-event-calc-dates :start '1-jan-1991 :end '31-dec-1992)
  
  (set-dates-for-event-calculation
   (current-hst-event-calc)
   :start start 
   :end end 
   :sample default-sample-interval))


(defun SET-HST-EVENT-CALC-SUN-CONSTRAINT (&key angle-table sample-interval)
  
  "Set up sun constraint.
    :angle-table is a list of angle ranges in DEGREES and suitability
      values, e.g. '((0 50 0)(50 180 1))
      If nil the constraint will be turned off.
    :sample-interval is interval in days, or nil to use default."
  ;; (set-event-calc-sun-constraint :angle-table '((0 50 0)(50 180 1)))
  ;; (set-event-calc-sun-constraint :angle-table nil)
  
  (set-sun-exclusion-for-event-calculation 
   (current-hst-event-calc)
   :sun-angle-table angle-table 
   :interval sample-interval))
  

(defun SET-HST-EVENT-CALC-MOON-CONSTRAINT (&key angle-table sample-interval)
  
  "Set up moon constraint.
    :angle-table is a list of angle ranges in DEGREES and suitability
      values, e.g. '((0 15 0)(15 20 0.5)(20 180 1))
      If nil the constraint will be turned off.
    :sample-interval is interval in days, or nil to use default."
  ;; (set-event-calc-moon-constraint :angle-table '((0 50 0)(50 180 1)))
  
  (set-moon-exclusion-for-event-calculation 
   (current-hst-event-calc)
   :moon-angle-table angle-table 
   :interval sample-interval))
  

(defun SET-HST-EVENT-CALC-ZODIACAL-LIGHT (&key wavelength brightness 
                                               sample-interval)

  "Set up zodiacal light constraint.
    :wavelength is wavelength
    :brightness is target flux density at wavelength
   If either is nil the constraint will be turned off."
  ;; (set-event-calc-zodiacal-light) ;turn off
  ;; (set-event-calc-zodiacal-light :wavelength 5500 :brightness 1e-10)

  (set-zod-light-for-event-calculation 
   (current-hst-event-calc)
   :zod-wavelength wavelength 
   :zod-brightness brightness 
   :interval sample-interval))


(defun SET-HST-EVENT-CALC-ORBITAL-VIEWING 
       (&key orbit-model 
             bright-limb-angle
             dark-limb-angle
             (limb-angles-in-degrees nil)
             (terminator-angle 90.0)
             (terminator-angle-in-degrees t)
             dark
             (dark-sun-limb-angle -0.25)
             (dark-sun-limb-angle-in-degrees t)
             northern-hemisphere-only
             viewing-time
             (interruptible nil)
             (min-viewing-time nil)
             (view-suitability-table nil)
             (high-accuracy nil)
             ;; additional HST-specific parameters
             (acq-time nil)
             sample-interval)
  
  "Set up orbital viewing constraint.
    :orbit-model is orbit model name (automatically loaded if necessary)
    :bright-limb-angle and :dark-limb-angle are earth avoidance angles;
      both are required
    :limb-angles-in-degrees should be t if both limb angles are degrees, 
      nil if radians
    :terminator-angle may be degrees or radians, depending on flag
      :terminator-angle-in-degrees: it is angle to terminator from sun on
      earth surface
    :dark is t if observation requires shadow viewing, nil otherwise
    :dark-sun-limb-angle may be radians or degrees, depending on 
       :dark-sun-limb-angle-in-degrees. Negative limb angles
       are below horizon
    :northern-hemisphere-only is t if preference for viewing in norther
       hemisphere is to be applied, nil otherwise
    :viewing-time is observation time in days, if viewing duration constraint
      is to be calculated, nil otherwise
    :interruptible is flag t if observation is interruptible, nil if not.
      If non-interruptible, then :viewing-time is taken to be the duration.
      If interruptible, then :min-viewing-time should be specified as the 
      minimum size of an interrupted segment
    :min-viewing-time in days is minimum duration of interrupted segment for
      and interruptible observation
    :view-suitability-table is TBD, default is nil.
    :high-accuracy may be t or nil
    :sample-interval is interval in days, or nil for default
    :acq-time is GS acq or re-acq time, in days
    "

  ;; (set-event-calc-orbital-viewing :orbit-model "test" :bright-limb-angle 10 
  ;;   :dark-limb-angle 10 :angles-in-degrees t)
  
  (set-viewing-parms-for-event-calculation
   (current-hst-event-calc)
   :orbit-model-name orbit-model

   :bright-angle bright-limb-angle 
   :dark-angle dark-limb-angle
   :limb-angles-in-degrees limb-angles-in-degrees    
   :terminator-limit (if terminator-angle-in-degrees 
                       (radians terminator-angle) terminator-angle)
   
   :dark-flag dark 
   :dark-sun-limb-limit (if dark-sun-limb-angle-in-degrees
                          (radians dark-sun-limb-angle) dark-sun-limb-angle)

   :view-duration viewing-time
   :view-interruptible interruptible
   :view-minimum-duration min-viewing-time
   :view-suitability-table view-suitability-table

   :north-flag northern-hemisphere-only

   :high-accuracy-flag high-accuracy
   :interval sample-interval

   :acq-time acq-time
   ))


(defun SET-HST-EVENT-CALC-SENSOR-LIMITS 
  (&key sensor-name sun moon bright-limb dark-limb (angles-in-degrees nil))

  "Set up limits for sensor viewing angles.  Angles may be in radians or
    degrees (in which case angles-in-degrees should be t).
    Missing angles are ignored."

  (set-sensor-limits-for-event-calculation 
   (current-hst-event-calc)
   sensor-name
   :sun sun :moon moon :bright-limb bright-limb :dark-limb dark-limb 
   :angles-in-degrees angles-in-degrees))
  

;;; ----------------------------------------------------------------------
;;;               Externally callable functions to get HST-event-calc PCFs
;;; ----------------------------------------------------------------------

;;; --- externally callable functions to get constraint PCFs

(defun GET-HST-SUN-PCF ()
  (sun-constraint-pcf (current-hst-event-calc)))

(defun GET-HST-MOON-PCF ()
  (moon-constraint-pcf (current-hst-event-calc)))

(defun GET-HST-ZODIACAL-LIGHT-PCF ()
  (zodiacal-light-constraint-pcf (current-hst-event-calc)))

(defun GET-HST-ORBITAL-VIEWING-PCF ()
  (orbital-viewing-pcf (current-hst-event-calc)))


;;; ----------------------------------------------------------------------
;;;                                         Event cache read/write for HST
;;; ----------------------------------------------------------------------

;;; NOTE:  NEED TO GENERATE HEADER AS LIST OF STRINGS
;;;    (write-readable-spike-heading 
;;;      :prop proposal :version version
;;;      :stream stream
;;;      :title (format nil "Proposal Events in ~a" filename)
;;;      :otherinfo `((:signature ,(signature-name-string :time nil))))


(defun SAVE-PROPOSAL-EVENT-CACHE (proposal version &key (type 'event-calc))
  
  "save event caches to file in orbit model directory, named by proposal"
  
  (let ((EC (current-hst-event-calc)))
    (with-slots (orbit-model) EC
      (let ((filename (spike-default-file-name proposal version 
                                               :events orbit-model)))
        (save-event-cache EC 
                          filename
                          :verbose t
                          :compile nil
                          :header nil
                          :type type)))))


(defun RESTORE-PROPOSAL-EVENT-CACHE (proposal version &key (type 'event-calc))
  
  "restore event caches from file in orbit model directory, named by proposal"
  
  (let ((EC (current-hst-event-calc)))
    (with-slots (orbit-model) EC
      
      (let ((filename (spike-default-file-name proposal version 
                                               :compiled-events orbit-model)))
        (restore-event-cache EC filename :verbose t :type type)))))


;;; --- compatibility with previous event caches
;;;
;;; These functions are used in old event caches: they are kept around
;;; so that old caches can be loaded without error (but the caches will not
;;; be used).  Delete when the time is right.

(defun ADD-SUN (&key params pcf (type 'event-calc))
  "add element into the sun cache"
  (declare (ignore params pcf type)))

(defun ADD-MOON (&key params pcf (type 'event-calc))
  "add element into the moon cache"
  (declare (ignore params pcf type)))

(defun ADD-ORB-VIEWING (&key params pcf (type 'event-calc))
  "add element into the orbital viewing cache"
  (declare (ignore params pcf type)))

(defun ADD-ZODIACAL-LIGHT (&key params pcf (type 'event-calc))
  "add element into the zodiacal light event cache"
  (declare (ignore params pcf type)))


;;; ----------------------------------------------------------------------
;;;                         Override/Additional methods for HST-EVENT-CALC
;;; ----------------------------------------------------------------------

(defmethod SET-VIEWING-PARMS-FOR-EVENT-CALCULATION :AFTER
  ((HEC HST-event-calc)
   &key
   (acq-time nil)
   &allow-other-keys)
  (setf (slot-value HEC 'acq-time) acq-time))


(defmethod GET-VIEWING-PARMS-FOR-EVENT-CALCULATION :AROUND
  ((HEC HST-event-calc))
  (let ((original-parms (call-next-method))
        (additional-parms (list (acq-time HEC))))
    (append original-parms
            (rationalize-numbers-in-nested-list  
             additional-parms))))


;;; override the default ORB-VIEWING-SAMPLE method to NOT compute some 
;;; PCFs which are currently not used for suitability calculation.

(defmethod ORB-VIEWING-SAMPLE :AROUND
  ((HEC HST-event-calc) time
   &key 
   (omit-geo-pcf nil)
   (omit-dark-limb-pcf nil)
   (omit-bright-limb-pcf nil)
   (omit-vis-pcf nil)
   (omit-dark-pcf nil)
   (sensor-name-list nil)
   (named-vector-list nil)
   (verbose nil))
  ;; call-next-method with constructed arg list
  (declare (ignore omit-geo-pcf omit-dark-limb-pcf omit-bright-limb-pcf
                   omit-vis-pcf omit-dark-pcf))
  (call-next-method
   HEC time 
   :omit-geo-pcf          t 
   :omit-dark-limb-pcf    t 
   :omit-bright-limb-pcf  t
   :omit-vis-pcf          nil
   :omit-dark-pcf         (not (dark HEC))
   :sensor-name-list      sensor-name-list
   :named-vector-list     named-vector-list
   :verbose               verbose)
  )

;;; Note:  this ignores min-view-duration for interruptible obs

(defmethod COMPUTE-ORBITAL-VIEWING-SUITABILITY
  ((HEC hst-event-calc) 
   &key
   sample
   (stream nil)
   &allow-other-keys)

  (with-orb-viewing-sample sample
    (let* (;; pcf 1 when target can be viewed, 0 otherwise:
           ;; depends only on whether dark required or not
           (view-pcf  (if (dark HEC)
                        (pcf-multiply dark-pcf vis-pcf)
                        vis-pcf))
           ;; total time in view period, days
           (view-duration (summed-interval-duration 
                           (pcf-non-zero-intervals view-pcf n1 n2)))
           ;; acq time overhead, or 0 if not provided
           (acq (or (acq-time HEC) 0))
           ;; time on target, or 0 if not provided
           (required-view-duration (or (viewing-time HEC) 0))
           ;; nort viewing preference
           (north-term 1)
           ;; for the computed suitability value
           suit-value
           )
      
      ;; adjust for preference for viewing in northern hemisphere
      (if (northern-hemisphere-only HEC)
        (setf north-term 
              (north-suit-factor HEC n1 n2 view-pcf north-pcf)))
      
      ;; four cases, depending on whether dark or not, interruptible or not
      (cond 
       
       ((dark HEC)                      ; dark viewing
        ;; need some additional quantities: total dark time, bright time
        ;; before shadow starts
        (let ((total-dark (summed-interval-duration
                           (pcf-non-zero-intervals dark-pcf n1 n2)))
              ;; CAVEAT!  this next form is looking only at the first
              ;; dark interval, needs to be fixed for more than one
              (bright-before-shadow
               (first 
                (multiple-value-list
                 (bright-dark-bright-times n1 n2 vis-pcf view-pcf)))))
          (cond ((interruptible HEC)      ; dark interruptible
                 (setf suit-value
                       (if (and (> total-dark 0)
                                (>= view-duration (* 0.8 total-dark))
                                (>= bright-before-shadow acq))
                         (/ view-duration total-dark)
                         0.01))
                 (when stream
                   (format stream "~%DARK INT view ~6,2f dark ~6,2f bbs ~6,2f acq ~6,2f suit ~6,2f"
                           (* 1440 view-duration) (* 1440 total-dark)
                           (* 1440 bright-before-shadow) (* 1440 acq) suit-value))
                 )
                (t                        ; dark non-int
                 (setf suit-value
                       (if (and (<= required-view-duration view-duration)
                                (>= bright-before-shadow acq))
                         1 0.01))
                 (when stream
                   (format stream "~%DARK NONINT view ~6,2f dark ~6,2f obs ~6,2f bbs ~6,2f acq ~6,2f suit ~6,2f"
                           (* 1440 view-duration) (* 1440 total-dark)
                           (* 1440 required-view-duration)
                           (* 1440 bright-before-shadow) (* 1440 acq) suit-value))
                 ))))
       
       (t                               ; bright viewing
        (cond ((interruptible HEC)      ; bright and interruptible
               (setf suit-value 1)
               (when stream
                 (format stream "~%BRIGHT INT view ~6,2f suit ~6,2f"
                         (* 1440 view-duration) suit-value))
               )
              (t                        ; bright and non-int
               (setf suit-value
                     (if (>= (- view-duration acq) required-view-duration)
                       1 0.01))
               (when stream
                 (format stream "~%BRIGHT NONINT view ~6,2f obs ~6,2f acq ~6,2f suit ~6,2f"
                         (* 1440 view-duration) (* 1440 required-view-duration)
                         (* 1440 acq) suit-value))
               ))))

      ;; get total suitability
      (setf suit-value (* suit-value north-term))

      ;; round, but don't round to zero
      (setf suit-value
            (if (< suit-value 0.005) ;bigger than this will be rounded to 0.01
              0.005
              (coerce (/ (round suit-value 0.01) 100) 'double-float)))
      
      (when stream
        (format stream " north ~6,2f tot ~6,2f" north-term suit-value))
            
      suit-value)))


;;; --- unchanged from fast-event version except for penalty factor now = 0.8
(defmethod NORTH-SUIT-FACTOR ((HEC hst-event-calc)
                              n1 n2 vis-pcf north-pcf)

  "Input n1 n2 node crossing times for orbit sample,
    vis-pcf target visibility pcf (1 visible, 0 not)
    north-pcf observer in northern hemisphere pcf (1 in north, 0 not),
    return suitability factor for northern hemisphere viewing"

  (declare (ignore n1 n2))
  (let* ((vis-north-pcf (pcf-multiply vis-pcf north-pcf))
	 (vis-intervals (pcf-non-zero-intervals vis-pcf))
	 (vis-north-intervals (pcf-non-zero-intervals vis-north-pcf))
	 ;; fraction of the visibility period in north
	 (vis-north-fraction
	   (/ (summed-interval-duration vis-north-intervals)
	      (summed-interval-duration vis-intervals)))
	 ;; penalty should be between 0 and 1: 0 means no penalty, 1 means maximum.
	 (penalty 0.8)
	 factor)
    ;; if vis north fraction is 1, then all visibility is in northern hemisphere
    ;; if zero, then all in south
    ;; apply penalty factor for the difference from 1
    ;; vis-north-fraction 1 => factor is 1
    ;; vis-north-fraction 0 => factor is 1-penalty
    (setf factor (- 1 (* penalty (- 1 vis-north-fraction))))
    factor))

;;; ----------------------------------------------------------------------
;;;                                   Creating orbit models from SPSS fits
;;; ----------------------------------------------------------------------

(defun ORBIT-FILE-FROM-SPSS-FIT ()
  
  "Given the parameters from an SPSS orbit fit,
    this writes an orbit file named new-orbit.model.
    This must be copied to the correct directory
    and named orbit.model"
  
  (let (fit-start a1 a2 f2t0 f3t0 f3t1 f4t0 ;input parameters w/ spss names
	model-name start-date end-date
	(file (if (fboundp 'make-temp-file)
		  (funcall 'make-temp-file "new-orbit.model")
		"new-orbit.model")))
    
    ;;use read-line for strings, read-from-string for numbers
    (format t "~%Enter the name of this orbit model ")
    (setf model-name (read-from-string (read-line)))
    (format t "~%Now I need the start and end dates that this model is valid in Spike")
    (format t "~% Note that these dates CAN be different than the fit date used in SPSS")
    (format t "~%Enter the start date in the form DD-MMM-YY, e.g. 31-Dec-88 ")
    (setf start-date (read-line))
    (format t "~%Enter the end date in the form DD-MMM-YY, e.g. 01-Jul-89 ")
    (setf end-date (read-line))
    
    (format t "~%~%Now you will enter data from the SPSS output")
    (format t "~% labelled \"ST/TDRS Orbit Coefficients\"~%")
    (format t "~%Enter ST FIT START DATE")
    (format t "~% e.g. 1990.155:00:00:00 ")
    (setf fit-start (read-line))
    (format t "~%Enter a1 ")
    (setf a1 (read-from-string (read-line)))
    (format t "~%Enter a2 ")
    (setf a2 (read-from-string (read-line)))
    (format t "~%Enter F2 t**0 ")
    (setf f2t0 (read-from-string (read-line)))
    (format t "~%Enter F3 t**0 ")
    (setf f3t0 (read-from-string (read-line)))
    (format t "~%Enter F4 t**0 ")
    (setf f4t0 (read-from-string (read-line)))
    (format t "~%Enter F3 t**1 ")
    (setf f3t1 (read-from-string (read-line)))
    
    (multiple-value-bind
      (inclination time-asc-node ra-asc-node
                   nodal-regression-rate period semi-major-axis)
      (spike-parms-from-spss-fit 
       :fit-start fit-start :a1 a1 :a2 a2 
       :f2t0 f2t0 :f3t0 f3t0 :f3t1 f3t1 :f4t0 f4t0)
      
      (with-open-file (stream file :direction :output :if-exists :supersede)
        
        (format stream "#| Orbit model created from SPSS fit parameters~%")
        (time-stamp stream)
        (format stream "~%~a" 
		(if (fboundp 'spike-full-version-string)
		    (funcall 'spike-full-version-string)
		  "--unspecified Spike version--"))
        (format stream "~%~%SPSS parameters were:")
        (format stream "~%  Fit Start: ~a" fit-start)
        (format stream "~%  a1: ~a, a2: ~a" a1 a2)
        (format stream "~%  F2t**0: ~a, F3t**0: ~a, F4t**0: ~a" f2t0 f3t0 f4t0)
        (format stream "~%  F3t**1: ~a" f3t1)
        (format stream "~%|#~%~%")
        
        (format stream "~%(setf *current-orbit-model*    \"~a\")" model-name)
        (format stream "~%(setf *orbit-model-start-date* (dmy-to-time '~a))" start-date)
        (format stream "~%(setf *orbit-model-end-date*   (dmy-to-time '~a))" end-date)
        
	;; FORMERLY:  this wrote forms to create new instances of *ST*, *TDRSE* and *TDRSW*
	;; changed to only update orbit model parameters
        ;;(format stream "~%(setf *ST* (make-low-earth-orbit-satellite")
        ;;(format stream "~%       \"HST\"")
        ;;(format stream "~%        ~a" inclination)
        ;;(format stream " ; inclination in degrees")
        ;;(format stream "~%        ~a" time-asc-node)
        ;;(format stream " ; time asc node in truncated julian date")
        ;;(format stream "~%        ~a" ra-asc-node)
        ;;(format stream " ; ra of asc node in degrees ")
        ;;(format stream "~%        ~a" nodal-regression-rate)
        ;;(format stream " ; nodal regression rate in degrees/day")
        ;;(format stream "~%        ~a" period)
        ;;(format stream " ; nodal period in minutes")
        ;;(format stream "~%        ~a" semi-major-axis)
        ;;(format stream " ; semi-major axis in km")
        ;;(format stream "))")
        ;;(format stream "~%~%(setf *TDRSE* (make-geostationary-satellite")
        ;;(format stream "~%               \"TDRS-East\" -41.0 86164.09054 42164.69))")
        ;;(format stream "~%~%(setf *TDRSW* (make-geostationary-satellite")
        ;;(format stream "~%               \"TDRS-West\" -171.0 86164.09054 42164.69))")

        (format stream "~%(update-orbit-parameters")
        (format stream "~%       *ST*")
        (format stream "~%       :inclination     ~20a  ;deg" inclination)
        (format stream "~%       :time-asc-node   ~20a  ;TJD or date" time-asc-node)
        (format stream "~%       :RA-asc-node     ~20a  ;deg" ra-asc-node)
        (format stream "~%       :regression-rate ~20a  ;deg/day" nodal-regression-rate)
        (format stream "~%       :period          ~20a  ;minutes" period)
        (format stream "~%       :semimajor-axis  ~20a  ;km" semi-major-axis)
        (format stream "~%       )")
        
	(format stream "~%~%(update-orbit-parameters")
	(format stream "~% *TDRSE*")
	(format stream "~% :longitude         -41.0                    ;deg")
	(format stream "~% :period            86164.09054              ;sec")
	(format stream "~% :semimajor-axis    42164.69                 ;km")
	(format stream "~% )")

	(format stream "~%~%(update-orbit-parameters")
	(format stream "~% *TDRSW*")
	(format stream "~% :longitude         -171.0                   ;deg")
	(format stream "~% :period            86164.09054              ;sec")
	(format stream "~% :semimajor-axis    42164.69                 ;km")
	(format stream "~% )")
	
        ))
    
    (format t "~%~%File ~a created" file)
    (format t "~%YOU MUST MOVE THIS TO A DIRECTORY NAMED ~A WHERE THE EVENT FILES RESIDE" model-name)
    (format t "~%AND RENAME IT TO \"ORBIT.MODEL\"")
    ))


(defun SPIKE-PARMS-FROM-SPSS-FIT (&key fit-start a1 a2
                                       f2t0 f3t0 f3t1 f4t0)
  
  "Given values for the spss fit parameters, return multiple
  values for inclination time-asc-node ra-asc-node
       nodal-regression-rate period semi-major-axis"
  
  (let* ((fit-start-tjd (tjd-from-sogs-date fit-start))
         (inclination f4t0)
         (time-asc-node (+ fit-start-tjd (/ (- a1) (* 86400 a2))))
         (ra-asc-node f3t0)
         (nodal-regression-rate (* f3t1 86400))
         (period (/ 360/60 a2))
         (semi-major-axis f2t0))
    (values 
     inclination time-asc-node ra-asc-node
     nodal-regression-rate period semi-major-axis)))

;;; ----------------------------------------------------------------------
;;;                                                        ALIVENESS TESTS
;;; ----------------------------------------------------------------------

#|

*CURRENT-HST-EVENT-CALC*
(current-hst-event-calc)
*CURRENT-HST-EVENT-CALC*


(set-hst-event-calc-pointing :name "Fred" :ra 123.34 :dec -45.23
                             :units :degrees
                             :observer *ST*
                             :observer-name (name *ST*))

(set-HST-event-calc-roll :nominal)
(set-HST-event-calc-roll :offset-from-nominal :roll-offset-angle -0.1)
(set-HST-event-calc-roll :fixed :roll-angle 1.2)
(set-HST-event-calc-roll nil)

(set-HST-event-calc-dates :start '1-jan-1991 :end '15-jan-1991
                          :default-sample-interval 1)
(set-HST-event-calc-sun-constraint :angle-table '((0 50 0)(50 180 1))
                                   :sample-interval 1)
(set-HST-event-calc-moon-constraint :angle-table '((0 15 0)(15 180 1))
				:sample-interval 1/3)
(set-HST-event-calc-zodiacal-light :wavelength 5500 :brightness 1e-10)
;; bright
(set-HST-event-calc-orbital-viewing 
 :orbit-model "test" :bright-limb-angle 10 
 :dark-limb-angle 10 :limb-angles-in-degrees t)
;; dark
(set-HST-event-calc-orbital-viewing 
 :orbit-model "test" :bright-limb-angle 10 
 :dark t :dark-limb-angle 10 :limb-angles-in-degrees t)
(describe (current-HST-event-calc))

(print (get-HST-orbital-viewing-pcf))
(time (get-HST-orbital-viewing-pcf))
(print (get-HST-moon-pcf))
(print (get-HST-zodiacal-light-pcf))
(print (get-HST-sun-pcf))

(event-cache-report (current-HST-event-calc))
(clear-event-cache (current-HST-event-calc))
(save-event-cache (current-HST-event-calc) 
                  "clipper:temp.cache" :compile nil :verbose t)
(restore-event-cache (current-HST-event-calc)
                     "clipper:temp.cache" :verbose t)

;; calc and show contributions
;; same bright/dark limb 
(set-HST-event-calc-orbital-viewing 
 :orbit-model "test" :bright-limb-angle 10 
 :dark-limb-angle 10 :limb-angles-in-degrees t)
(compute-orbital-viewing-pcf (current-HST-event-calc) 
                             :cache-results nil :stream t)

;; include terminator & different bright/dark limb
(set-HST-event-calc-orbital-viewing 
 :orbit-model "test" :bright-limb-angle 20 
 :dark-limb-angle 10 :limb-angles-in-degrees t
 :terminator-angle 104 :terminator-angle-in-degrees t
 :high-accuracy t 
 )
(gc)
(time
 (compute-orbital-viewing-pcf (current-HST-event-calc) 
                             :cache-results nil :stream t))

;; include dark time, northern hemisphere
(set-HST-event-calc-orbital-viewing 
 :orbit-model "test" :bright-limb-angle 20 
 :dark-limb-angle 10 :limb-angles-in-degrees t
 :terminator-angle 104 :terminator-angle-in-degrees t
 :dark t :dark-sun-limb-angle -1 :dark-sun-limb-angle-in-degrees t
 :northern-hemisphere-only t
 )
(compute-orbital-viewing-pcf (current-HST-event-calc) 
                             :cache-results nil :stream t)


;; test adding acq-time
(set-HST-event-calc-orbital-viewing :orbit-model "test" :bright-limb-angle 10 
 :dark-limb-angle 10 :limb-angles-in-degrees t :acq-time (/ 15.0 1440.0))
(describe (current-HST-event-calc))
(print (get-HST-orbital-viewing-pcf))
(compute-orbital-viewing-pcf (current-HST-event-calc) 
                             :cache-results nil :stream t)

;; bright non-int, north
(set-HST-event-calc-orbital-viewing :orbit-model "test" :bright-limb-angle 10 
 :dark-limb-angle 10 :limb-angles-in-degrees t :acq-time (/ 15.0 1440.0)
 :dark nil :viewing-time (/ 42.0 1440.0) :interruptible nil
 :northern-hemisphere-only t)
(compute-orbital-viewing-pcf (current-HST-event-calc) 
                             :cache-results nil :stream t)
;; example output:
name: Fred
  dark NIL  sun angle for dark:  -0.25 deg
  dark earth  10.00 deg bright earth  10.00 deg, terminator  90.00 deg
  view time  42.00 min, NON-INTERRUPIBLE min-view NIL
  max vis interval:  59.71 min
BRIGHT NONINT view  56.22 obs  42.00 acq  15.00 suit   0.01 north   0.34 tot   0.01
BRIGHT NONINT view  56.28 obs  42.00 acq  15.00 suit   0.01 north   0.35 tot   0.01
BRIGHT NONINT view  56.36 obs  42.00 acq  15.00 suit   0.01 north   0.37 tot   0.01
BRIGHT NONINT view  56.45 obs  42.00 acq  15.00 suit   0.01 north   0.39 tot   0.01
BRIGHT NONINT view  56.58 obs  42.00 acq  15.00 suit   0.01 north   0.41 tot   0.01
BRIGHT NONINT view  56.73 obs  42.00 acq  15.00 suit   0.01 north   0.42 tot   0.01
BRIGHT NONINT view  56.91 obs  42.00 acq  15.00 suit   0.01 north   0.44 tot   0.01
BRIGHT NONINT view  57.13 obs  42.00 acq  15.00 suit   1.00 north   0.46 tot   0.46
BRIGHT NONINT view  57.40 obs  42.00 acq  15.00 suit   1.00 north   0.48 tot   0.48
BRIGHT NONINT view  57.71 obs  42.00 acq  15.00 suit   1.00 north   0.49 tot   0.49
BRIGHT NONINT view  58.09 obs  42.00 acq  15.00 suit   1.00 north   0.51 tot   0.51
BRIGHT NONINT view  58.55 obs  42.00 acq  15.00 suit   1.00 north   0.53 tot   0.53
BRIGHT NONINT view  59.10 obs  42.00 acq  15.00 suit   1.00 north   0.55 tot   0.55
BRIGHT NONINT view  59.71 obs  42.00 acq  15.00 suit   1.00 north   0.57 tot   0.57


;; bright int, north
(set-HST-event-calc-orbital-viewing :orbit-model "test" :bright-limb-angle 10 
 :dark-limb-angle 10 :limb-angles-in-degrees t :acq-time (/ 15.0 1440.0)
 :dark nil :viewing-time (/ 42.0 1440.0) :interruptible t
 :northern-hemisphere-only t)
(compute-orbital-viewing-pcf (current-HST-event-calc) 
                             :cache-results nil :stream t)

;; dark non-int, north
(set-HST-event-calc-orbital-viewing :orbit-model "test" :bright-limb-angle 10 
 :dark-limb-angle 10 :limb-angles-in-degrees t :acq-time (/ 15.0 1440.0)
 :dark t :viewing-time (/ 30.0 1440.0) :interruptible nil
 :northern-hemisphere-only t)
(compute-orbital-viewing-pcf (current-HST-event-calc) 
                             :cache-results nil :stream t)

;; dark int, north
(set-HST-event-calc-orbital-viewing :orbit-model "test" :bright-limb-angle 10 
 :dark-limb-angle 10 :limb-angles-in-degrees t :acq-time (/ 15.0 1440.0)
 :dark t :viewing-time (/ 30.0 1440.0) :interruptible t
 :northern-hemisphere-only t)
(compute-orbital-viewing-pcf (current-HST-event-calc) 
                             :cache-results nil :stream t)
|#@

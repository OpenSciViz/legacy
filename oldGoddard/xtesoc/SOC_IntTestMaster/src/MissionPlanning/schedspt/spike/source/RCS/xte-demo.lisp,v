head	3.0;
access
	soccm;
symbols
	Build4_3_1:3.0
	Build4_3:3.0
	OldSOCTime:3.0
	Build4_2:3.0
	For_Build4_1:3.0
	E2e:3.0
	ItsAlive:3.0
	Build4:3.0
	Current:3.0
	Build3_3:3.0
	Build3_2:3.0;
locks
	buehler:3.0; strict;
comment	@;;; @;


3.0
date	94.04.01.19.36.12;	author buehler;	state Submitted;
branches;
next	;


desc
@Currently mysterious.
@


3.0
log
@Initial XTE version.
@
text
@;;; -*- Mode:Common-Lisp; Package:USER; Base:10 -*-
;;;
;;; xte-demo.lisp
;;;
;;; +--------------------------------------------------------------------+
;;; |                                                                    |
;;; |   THIS FILE IS PART OF EXPORT SPIKE, DISTRIBUTED TO SITES OUTSIDE  |
;;; |   STSCI.  CHANGES TO THIS FILE MUST BE DISCUSSED BEFORE THEY ARE   |
;;; |   MADE, FOLLOWING THE ADVANCE PLANNING SYSTEMS BRANCH PROCEDURE.   |
;;; |                                                                    |
;;; +--------------------------------------------------------------------+
;;;
;;; $Log: xte-demo.lisp,v $
;;; Revision 1.1  1991/12/01  00:15:01  johnston
;;; Initial revision
;;;
;;;
;;; creation 5 June 1991 MDJ
;;;
;;; this file gives examples of running integrated long-term/short-term
;;; scheduling for Astro-D
;;;

#|
;;; parameter setup

;;; the following sets up for six-months with 1-week bins for 
;;; long-term scheduling

(setf *XTE-SCHEDULE-START*   (dmy-to-time '1-jan-1992))
(setf *XTE-SCHEDULE-END*     (dmy-to-time '1-jul-1992))
(setf *XTE-TIME-QUANTUM*     7)
(setf *XTE-SEGMENT-START*    (dmy-to-time '1-jan-1992))
(setf *XTE-SEGMENT-DURATION* 7)
(setf *XTE-SEGMENT-COUNT*    27)
(setf *XTE-CAPACITY-UNIT*    0.01)
(setf *XTE-OVERSUBSCRIPTION-FACTOR* 0.50)

;;; for short term scheduling use 5 min time quantization
;;; other parameters in xte-csp.lisp
(setf   *XTE-SHORT-TERM-TIME-QUANTUM* (/ 5.0 1440.0))

(report-xte-schedule-times)
(report-xte-segment-times)
(XTE-targ-files)

;;; Change the pathname below if needed

;;; prep a file (and compile prep file)
(xte-prep-file "xte-test")

;;; test whether file needs to be prepped
(XTE-targ-file-needs-to-be-prepped "xray10")
(XTE-targ-file-needs-to-be-prepped "xte-test")

;;; load, prep if needed.  xte-test.targ contains 3 dummy targets and
;;; makes a good aliveness test
(xte-init-prep-and-load '("xte-test"))

;;; load all of the AXAF targets (933).  Each file xray1.targ, xray2.targ,
;;; etc. contains 100 targets, except xray10.targ which contains the last 33.
;;; If you want to load a subset (a good idea to start with) comment out
;;; some of these.
(xte-init-prep-and-load 
 '(;; "xray1" "xray2" "xray3" "xray4" "xray5"
   ;; "xray6" "xray7" "xray8" "xray9"
   "xray10"
   ))

;;; Run the long-term scheduler.  (time ...) print time and space 
;;; statistics.  There are three search algorithms, early-greedy-schedule
;;; is the fastest, the other two do a better job.  Each one finishes with
;;; a schedule built, each can be run as many times as desired.
(time (early-greedy-schedule *XTE-LONG-TERM-SCHEDULE*))
(time (max-pref-schedule *XTE-LONG-TERM-SCHEDULE*))
(time (elminc-schedule *XTE-LONG-TERM-SCHEDULE*))
(time (hi-prio-max-pref-schedule *XTE-LONG-TERM-SCHEDULE*))

;;; report current long-term schedule to lisp listener
(schedule-report (obs-csp *XTE-LONG-TERM-SCHEDULE*))

;;; postscript plot of long term schedule.  Specify the output postscript
;;; file.
(generate-XTE-csp-plot 
 "temp/xte-long.ps"
 :obs-csp-chronika *xte-long-term-schedule*)

;; plot task details (only for long-term tasks)
(plot-XTE-task-details *XTE-long-term-schedule* 
                          :id 'dummy-3 
                          :file "temp/xte-ev.ps" ;"clipper:xte-ev.ps"
                          :ptop 600 :pbottom 100)


;;; ---------------------------------------- SHORT TERM

;;; make a short-term schedule from the exposures assigned to a specifed
;;; bin.  Bin ranges from 0 through 27 for the 6-month example above.
;;; this one does bin 0
(time (make-xte-short-term-schedule 0))

;;; run the scheduling algorithms on the short-term schedule
(time (early-greedy-schedule *XTE-SHORT-TERM-SCHEDULE*))
(time (max-pref-schedule *XTE-SHORT-TERM-SCHEDULE*))
(time (elminc-schedule *XTE-SHORT-TERM-SCHEDULE*))

;;; report short-term schedule
(schedule-report (obs-csp *XTE-SHORT-TERM-SCHEDULE*))

;;; postscript plot of short term schedule: specify the fraction of the whole
;;; timespan to plot, and the output postscript file

;;; plot the whole schedule
(generate-XTE-csp-plot-short-term
 "temp/xte-short1.ps"	;pathname
 :start-fraction 0 :end-fraction 1)

;; plot first day only
(generate-XTE-csp-plot-short-term
 "temp/xte-short2.ps"	;pathname
 :start-fraction 0 :end-fraction 0.5)
(generate-XTE-csp-plot-short-term
 "temp/xte-short3.ps"	;pathname
 :start-fraction 0.5 :end-fraction 1)

|#
@

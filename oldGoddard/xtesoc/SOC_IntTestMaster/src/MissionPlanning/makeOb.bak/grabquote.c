#include <stdio.h>
#include <ctype.h>

void grabquote(FILE *ifp,char loadme[])
{
  int c, i;
  while ( (c=fgetc(ifp)) != '"')
    ; /* grab leading quote */
  i = 0;
  while ((c = fgetc(ifp)) != '"')
    loadme[i++]=c;
  while ((c=fgetc(ifp)) != '\n')
    ;
  loadme[i]='\0';
#ifdef DEBUG
printf("(%s)\n",loadme);
fflush(stdout);
#endif
}

/* reads in one proposal file, prints out data to summary file */

#include <stdio.h>
#include <string.h>

#include "odbdefs.h"
extern char *keys[];

/* note that strcmp returns 0 for a match (0 = true) */
/* even though 0 is usually false, not true */

#ifdef DEBUG
#define KEYME(x) {printf("got key %s\n",x);}
#else
#define KEYME(x) {;}
#endif

void read_in(FILE *ifp,FILE *ifp2,int iopt)
     /* 1 = for summary file, 2 = for email output file */
{
  struct target_struct t;
  int i, j, inull, itemp;
  float fnull;
  char key[4], line[81], buffint[5], cnull[80];
  void grabquote(FILE *,char *),blank_it(struct target_struct);
  void summary(FILE *,struct target_struct);
  void blankpad(char *,int, int);
  void for_rps(struct target_struct, FILE *);


  blank_it(t);
  grabquote(ifp,t.obs_id);
  grabquote(ifp,t.expire);
  GRABINT(ifp,t.ao);
  GRABINT(ifp,t.pri);
  grabquote(ifp,t.tar_name);
  GRABFL(ifp,t.ra_2000);
  GRABFL(ifp,t.dec_2000);
  grabquote(ifp,line);
  strncpy(t.x_fullname,line,40);
  GRABFL(ifp,t.tot_telem);
  GRABFL(ifp,t.tot_obs_time);
  grabquote(ifp,line);  KEYME(line); /* the CST keyword */
  GRABINT(ifp,t.num_constr);
/* read in any time constraints that exist */
  for (i=0; i<t.num_constr; ++i) /* look for n TCs */
    {
#ifdef DEBUG
      printf("grabbing constraint %d out of %d\n",i+1,t.num_constr);
#endif
      grabquote(ifp,key); KEYME(key); /* grab first TC keyword */
      j = 0;
      while (strcmp(key,keys[j+44]) != 0)
	++j;   /* which keyword is it? */
      switch (j)
	{
	case 0:  /* ATR */
	  /* ignore str first_time, last_time; */
	  grabquote(ifp,cnull);
	  grabquote(ifp,cnull);
	  break;
	case 1:  /* ALT */
	  /* ignore alt_id, alt_is, alt_has */
	  grabquote(ifp,cnull);
	  GRABINT(ifp,inull);
	  GRABINT(ifp,inull);
	  strcpy(t.x_alternate,"Y\0");
	  break;
	case 2:  /* CON */
	  /* ignore int con_splits; */
	  GRABINT(ifp,inull);
	  break;
	case 3:  /* DOW */
	  /* ignore int first, last; */
	  GRABINT(ifp,inull);
	  GRABINT(ifp,inull);
	  break;
	case 4:  /* INT */
	  GRABINT(ifp,t.inter_min_time);
	  break;
	case 5:  /* ORD */
	  break;
	case 6:  /* ORN */
	  break;
	case 7:  /* PAR */
	  GRABFL(ifp,t.min_phase);
	  GRABFL(ifp,t.max_phase);
	  GRABFL(ifp,t.phase_epoch);
	  GRABFL(ifp,t.period);
	  break;
	case 8:  /* REL */
	  /* ignore str lead_id, trail_id; int min_sep, max_sep; */
	  grabquote(ifp,cnull);
	  grabquote(ifp,cnull);
	  GRABINT(ifp,inull);
	  GRABINT(ifp,inull);
	  break;
	case 9:  /* TOD */
	  GRABINT(ifp,itemp);
	  t.tod_hb = itemp/3600;
	  t.tod_mnb = (itemp - (t.tod_hb*3600))/60;
	  GRABINT(ifp,itemp);
	  t.tod_he = itemp/3600;
	  t.tod_mne = (itemp - (t.tod_he*3600))/60;
	  break;
	case 10:  /* SCA */
	  strcpy(t.x_scan,"Y\0");
	  break;
	case 11:  /* RAS */
	  strcpy(t.x_scan,"Y\0");
	  break;
	case 12:  /* TOO */
	  grabquote(ifp,t.other_too);
	  GRABFL(ifp,t.too_min_count);
	  GRABFL(ifp,t.too_max_count);
	  break;
	default:
	  break;
	}
    }

#ifdef DEBUG
  printf("done TCs\n");
#endif
  grabquote(ifp,key); KEYME(key); /* grab and ignore */  /* CFG */
  if (strcmp(key,keys[0]) != 0)
    printf("key mismatch, possible error for %s (got %s).\n",keys[0],key);

  GRABINT(ifp,t.x_nconfig);
  for (i=0; i < t.x_nconfig; ++i)
    {
      grabquote(ifp,key); KEYME(key);
      j = 0;
      while (strcmp(key,keys[j+7]) != 0)
        ++j;   /* which keyword is it? */
      t.x_instr_flag[j] = 1; /* flag it as existing */
      grabquote(ifp,t.x_config[i]);
      GRABFL(ifp,t.x_telem[i]);
    }

  grabquote(ifp,key); KEYME(key); /* grab and ignore */  /* OCF */
  if (strcmp(key,keys[1]) != 0)
    printf("key mismatch, possible error for %s (got %s).\n",keys[1],key);
  GRABINT(ifp,inull); /* ignore noccult */
  for (i=0; i < inull; ++i)
    {
      grabquote(ifp,key); KEYME(key);
      j = 8;
      while (strcmp(key,keys[j+7+8]) != 0)
        ++j;   /* which keyword is it? */
      t.x_instr_flag[j] = 1; /* flag it as existing */
      grabquote(ifp,t.x_config[i]);
      GRABFL(ifp,t.x_telem[i]);
    }

  grabquote(ifp,key); KEYME(key); /* grab and ignore */  /* HIP */
  if (strcmp(key,keys[2]) != 0)
    printf("key mismatch, possible error for %s (got %s).\n",keys[2],key);
  GRABINT(ifp,t.x_nhip);
  for (i=0; i < t.x_nhip; ++i)
    {
      grabquote(ifp,key); KEYME(key);
      j = 0;
      while (strcmp(key,keys[j+7+16]) != 0)
        ++j;   /* which keyword is it? */
      t.x_instr_flag[j+16] = 1; /* flag it as existing */
      grabquote(ifp,t.x_config[j+16]);
    }

  grabquote(ifp,key); KEYME(key); /* grab and ignore */  /* PCR */
  if (strcmp(key,keys[3]) != 0)
    printf("key mismatch, possible error for %s (got %s).\n",keys[3],key);
  for (i=0; i < 9; ++i) /* note that we skip some keywords, not used here */
    GRABFL(ifp,t.x_ct_rates[i]);

  grabquote(ifp,key); KEYME(key); /* grab and ignore */  /* HCR */
  if (strcmp(key,keys[4]) != 0)
    printf("key mismatch, possible error for %s (got %s).\n",keys[4],key);
  for (i=9; i < 13; ++i) /* note that we skip some keywords, not used here */
    GRABFL(ifp,t.x_ct_rates[i]);

  grabquote(ifp,key); KEYME(key);
  if (strcmp(key,keys[5]) != 0)  /* REM */
    printf("remarks key may be missing.\n");
  i = 0;
  while (i != (-1))
    {
      blankpad(line,80,0);
      grabquote(ifp,line);
      if ( strncmp("END",line,3) == 0)
	i = (-1); /* it's the end, so exit */
      else if (i < 5)
	{
	  strcpy(t.remarks[i],line);
	  ++i;
	}
    }

  reformat(t,2);
  if (iopt == 1) /* summary file */
    summary(ifp2,t);
}

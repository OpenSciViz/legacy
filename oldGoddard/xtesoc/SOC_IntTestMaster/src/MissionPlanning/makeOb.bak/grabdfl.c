/* name: grabdfl(i,fl) */
/* reason: Routine returns an double float of the specified number of digits,
           independent of where the decimal point is.  The decimal point,
           if entered, will count as a digit.  It ignores non-numbers.
           If just return is entered, it assumes the default */
/* sample usage:  fl = grabdfl(isize,fl));
           (use isize<=9 if not relevant)*/
/* returns: an integer of up to the desired size */
/* init: requires "double grabfl()" */
/* warnings: size of >9 is risky, as 9 digits with no decimal is too large
             processing of decimal point is always a factor to consider.
	     Make sure there is a default! */
#include <stdio.h>
float grabdfl(int isize,float ff)
{
  float fred;
  float qq, pp;
  int i, ineg, iblow, c;
  ineg = 0; /* assumes it is positive, first */
  iblow = 1;
  fred=0.0;
  qq=10.0; pp = 1.0; c = '\n';
  printf("  ( default = %f) ",ff);
  for (i=0; i<isize; ++i)
     {
      c=getchar();
      if (iblow == 0 && (c-48 > 0 || c-48 <10))
	iblow = 1;
      if (i == 0 && (c == EOF || c == '\n' || c-48 <0 || c-48 >10))
	iblow = 0;
      if (i == 0 && c == '-')
	ineg = 1;
      if (c == EOF || c == '\n')
	i=isize;
      else if (c-48 >= 0 && c-48 <10)
	fred = (fred*qq) + ( ((int)c-48.0)*pp);
      else if (c == '.') qq = 1.0;
      if (qq < 10.0) pp = pp*0.1;
    }
  while (c != EOF && c != '\n')
    c=getchar();
  if (ineg == 1)fred = (-fred);
  if (iblow == 0)fred = ff;
  return (fred);
}

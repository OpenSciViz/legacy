#include <stdio.h>
#include <string.h>

#include "odbdefs.h"

void blank_it(struct target_struct t)
{
  void blankpad(char *,int,int);
  int i;

  blankpad(t.obs_id,IDLENGTH-1,1);
  blankpad(t.expire,TIMEL-1,1);
  blankpad(t.tar_name,20,1);

  t.ao = 1;
  t.pri = 3;
  t.tot_obs_time = 0;
  t.num_constr = 0;

  blankpad(t.pi_lname,27,1);
  blankpad(t.pi_fname,17,1);
  blankpad(t.pi_mname,11,1);

  t.ra_2000 = 0.0; t.dec_2000 = 0.0; t.tot_telem = 0;

  for (i=0;i<5;++i)
    strcpy(t.remarks[i],"\0");

  t.pca_total_count = 0.0;
  t.pca_min_count = 0.0;
  t.pca_max_count = 0.0;
  t.pca_band1 = 0.0;
  t.pca_band2 = 0.0;
  t.pca_band3 = 0.0;
  t.pca_band4 = 0.0;
  t.pca_band5 = 0.0;
  t.pca_band6 = 0.0;
  t.hexte_band1 = 0.0;
  t.hexte_band2 = 0.0;
  t.hexte_band3 = 0.0;
  t.hexte_band4 = 0.0;
  blankpad(t.pca_config1,30,1);
  t.pca_telem1 = 0.0;
  blankpad(t.pca_config2,30,1);
  t.pca_telem2 = 0.0;
  blankpad(t.pca_config3,30,1);
  t.pca_telem3 = 0.0;
  blankpad(t.pca_config4,30,1);
  t.pca_telem4 = 0.0;
  blankpad(t.pca_config5,30,1);
  t.pca_telem5 = 0.0;
  blankpad(t.pca_config6,30,1);
  t.pca_telem6 = 0.0;
  t.pca_telem_sub = 0.0;
  blankpad(t.hexte_modea,30,1);
  blankpad(t.hexte_energya,10,1);
  blankpad(t.hexte_dwella,10,1);
  blankpad(t.hexte_anglea,10,1);
  blankpad(t.hexte_bursta,8,1);
  blankpad(t.hexte_modeb,30,1);
  blankpad(t.hexte_energyb,10,1);
  blankpad(t.hexte_dwellb,10,1);
  blankpad(t.hexte_angleb,10,1);
  blankpad(t.hexte_burstb,8,1);
  t.hexte_telema = 0.0;
  t.hexte_telemb = 0.0;
  t.hexte_telem_sub = 0.0;

  blankpad(t.coob,1,1);
  t.cyb = 1994;
  t.cmb = 1 ;
  t.cdb = 1;
  t.chb = 0;
  t.cmnb = 0;
  t.cye = 2001;
  t.cme = 1;
  t.cde = 1;
  t.che = 0;
  t.cmne = 0;

  blankpad(t.tod,1,1);
  t.tod_hb= 0;
  t.tod_mnb = 0;
  t.tod_he = 0;
  t.tod_mne = 0;

  blankpad(t.monitor,1,1);

  t.min_int = 0.0;
  t.max_int = 1.0;

  blankpad(t.phase_dep,1,1);
  t.phase_epoch = 0.0;
  t.period = 0.0;
  t.min_phase = 0.0;
  t.max_phase = 0.0;

  blankpad(t.inter,1,1);
  t.inter_min_time = 0.0;

  blankpad(t.too,1,1);
  t.too_min_count = 0.0;
  t.too_max_count = 0.0;
  blankpad(t.other_too,2,1);
}

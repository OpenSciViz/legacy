#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "odbdefs.h"
extern char *keys[];

void writeout(FILE *ifp,struct target_struct t)
{
  int i, alt_is;
  char line[81];

  printf("debug: obs_id = %s.\n",t.obs_id);
  if (1)
    return;

  reformat(t,1); 

/*  printf("fuck-- alter so DEFAULT gets changed to -1\n");
  printf("fuck-- and go back and verify that blanks are converted to _\n"); */

  fprintf(ifp,"%c%s%c\n",34,t.obs_id,34);
  fprintf(ifp,"%c%s%c\n",34,t.expire,34);
  fprintf(ifp,"%d\n",t.ao);
  fprintf(ifp,"%d\n",t.pri);
  fprintf(ifp,"%c%s%c\n",34,t.tar_name,34);
  fprintf(ifp,"%f\n",t.ra_2000);
  fprintf(ifp,"%f\n",t.dec_2000);
  fprintf(ifp,"%c%s %s %s%c\n",34,t.pi_fname,t.pi_mname,t.pi_lname,34);
  fprintf(ifp,"%f\n",t.tot_telem);
  fprintf(ifp,"%d\n",t.tot_obs_time);
  fprintf(ifp,"%cCST%c\n",34,34);

#ifdef DEBUG
  printf("%c%s%c\n",34,t.obs_id,34);
  printf("%c%s%c\n",34,t.expire,34);
  printf("%d\n",t.ao);
  printf("%d\n",t.pri);
  printf("%c%s%c\n",34,t.tar_name,34);
  printf("%f\n",t.ra_2000);
  printf("%f\n",t.dec_2000);
  printf("%c%s %s %s%c\n",34,t.pi_fname,t.pi_mname,t.pi_lname,34);
  printf("%f\n",t.tot_telem);
  printf("%d\n",t.tot_obs_time);
  printf("%cCST%c\n",34,34);
#endif

  fprintf(ifp,"%d\n",t.num_constr);
/* print out any time constraints that exist */
  if (t.coob[0] == 'Y' || t.coob[0] == 'y')
    {
      fprintf(ifp,"%cATR%c\n",34,34);
      fprintf(ifp,"%c%d %d %d %d %d%c\n",34,
	      t.cyb, t.cmb, t.cdb, t.chb, t.cmnb, 34);
      fprintf(ifp,"%c%d %d %d %d %d%c\n",34,
	      t.cye, t.cme, t.cde, t.che, t.cmne, 34);
    }
  if (t.x_alternate[0] == 'Y' || t.x_alternate[0] == 'y')
    {
      fprintf(ifp,"%cALT%c\n",34,34);
      fprintf(ifp,"%c%s%c\n",34,t.x_alt_obs_id,34);
      if (t.obs_id[9] == '9' && t.obs_id[10] == '9')
	alt_is = 1; /* proposal numbers with -99- as the obs number are alts */
      else
	alt_is = 0;
      fprintf(ifp,"%d\n",alt_is); /* alt_is */
      fprintf(ifp,"%d\n",!alt_is); /* alt_has */
    }	
  if (t.inter[0] == 'Y' || t.inter[0] == 'y')
    {
      fprintf(ifp,"%cINT%c\n",34,34);
      fprintf(ifp,"%d\n",t.inter_min_time);
    }
  if (t.phase_dep[0] == 'Y' || t.phase_dep[0] == 'y')
    {
      fprintf(ifp,"%cPAR%c\n",34,34);
      fprintf(ifp,"%f\n",t.min_phase);
      fprintf(ifp,"%f\n",t.max_phase);
      fprintf(ifp,"%f\n",t.phase_epoch);
      fprintf(ifp,"%f\n",t.period);
    }	
  if (t.tod[0] == 'Y' || t.tod[0] == 'y')
    {
      fprintf(ifp,"%cTOD%c\n",34,34);
      fprintf(ifp,"%d:%d\n",t.tod_hb,t.tod_mnb);
      fprintf(ifp,"%d:%d\n",t.tod_he,t.tod_mne);
    }
  if (t.x_scan[0] == 'Y' || t.x_scan[0] == 'y')
    fprintf(ifp,"%cSCA%c\n%cRAS%c",34,34,34,34);
  if (t.too[0] == 'Y' || t.too[0] == 'y')
    {
      fprintf(ifp,"%c%s%c\n",34,t.other_too,34);
      fprintf(ifp,"%f\n",t.too_min_count);
      fprintf(ifp,"%f\n",t.too_max_count);
    }

  fprintf(ifp,"%c%s%c\n",34,keys[0],34);  /* CFG */
  fprintf(ifp,"%d\n",t.x_nconfig);

  for (i=0; i < 8; ++i)
    {
      fprintf(ifp,"%c%s%c\n",34,keys[i+7],34);
      fprintf(ifp,"%c%s%c\n",34,t.x_config[i],34);
      fprintf(ifp,"%f\n",t.x_telem[i]);
    }

  fprintf(ifp,"%c%s%c\n",34,keys[1],34);  /* OCF */
  fprintf(ifp,"0\n");

  fprintf(ifp,"%c%s%c\n",34,keys[2],34);  /* HIP */
  fprintf(ifp,"%d\n",t.x_nhip);
  for (i=0; i < 8; ++i)
    if (t.x_instr_flag[i+16] == 1) /* config specified */
      {
	fprintf(ifp,"%c%s%c\n",34,keys[i+7+16],34);
	if (i == 3 || i == 7)
	  fprintf(ifp,"%c%s%c\n",34,t.x_config[i+16],34);
	else
	  fprintf(ifp,"%s\n",t.x_config[i+16]);
      }

  fprintf(ifp,"%c%s%c\n",34,keys[3],34);  /* PCR */
  for (i=0; i < 9; ++i)
    {
/*      fprintf(ifp,"%c%s%c\n",34,keys[i+7+24],34); */
      fprintf(ifp,"%f\n",t.x_ct_rates[i]);
    }
  fprintf(ifp,"%c%s%c\n",34,keys[4],34);  /* HCR */
  for (i=9; i < 13; ++i)
    fprintf(ifp,"%f\n",t.x_ct_rates[i]);

  fprintf(ifp,"%c%s%c\n",34,keys[5],34);  /* REM */
  i = 0;
  strcpy(line,t.remarks[0]); /* get first line of remarks, if any */
  if (strlen(line) > 0)
    {
      fprintf(ifp,"%c%s%c\n",34,line,34);
      ++i;;
      strcpy(line,t.remarks[i]);
    }
  fprintf(ifp,"%c%s%c\n",34,keys[6],34);  /* END */
}

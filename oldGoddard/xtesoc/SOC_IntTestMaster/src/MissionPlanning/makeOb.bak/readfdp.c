/* name: readfdp(str,i) */
/* reason: Routine reads in a string of up to i characters, after printing
      what the previous value was and allowing it to be used as a default.
      Hitting return without entering data keeps the default (previous)
      value for that string.  It handles the printing of the default value.
      Unlike read_def(), it pads the string (if a new one was entered) out
      to the full length. */
/* sample usage: char str[80]; 
                 printf("Name? "); read_dp(str,isize); 
              (isize should be <80) */
/* returns: a string, loaded into the given string variable */
/* init: requires routine "trimprintf()" (no declaration needed) to work.
         requires routine "is_blank()" (declared int) to work.
         no initialization required other than the string itself, which
         should ideally have some value. */
/* warnings: string returned is length i, so array must be at least size i+1
             (ex: to get "sandy" use: char str[6]; read_dp(str,5)) */
#include <stdio.h>
#include <ctype.h>
int readfdp(s,value,isize)
char s[];
int isize;
float value;
{
  void trprintf();
  int c, i, idef, is_blank();
  idef = 0; /* initially, assume new value will appear */
  c = '\n';
  printf("(default = %f)  ",value);

  for (i=0; i<isize; ++i)
    {
      c=getchar();
      if (c == EOF || c == '\n')
	{
	  if (i == 0)
	    {
	      idef=1;    /* use default instead */
	      i=isize+1;
	    }
	  else
	    while (i < isize)
	      s[i++] = ' ';
	}
      else
	{
	  if (islower(c))c = toupper(c);
	  s[i] = c;

/*	  if ( c == 39)
	    {
	      if (i > isize-2)
		s[i]=' ';
	      else
		{
		  ++i;
		  s[i]=c;
		}
	    }
*/

	}
    }
  if (idef == 0)s[isize]='\0';  /* 'end' only if string has changed */
  while (c != EOF && c != '\n')
    c=getchar();
  return(idef);
}

/* old routine to convert an integer to a string, before I learned how to use
   sscanf (hey, why change something that works?) */

#include <stdio.h>

void get_num(char c[],int i)
{
  int i1, i2, i3, i4;
  if (i > 9999)printf("  Warning, number exceeds 1000, error.\n");
  i1 = 0; i2 = 0; i3 = 0, i4 = 0;
  i4 = i/1000;
  i3 = (i - i4*1000)/100;
  i2 = (i - (i4*1000) - (i3*100))/10;
  i1 = i - (i4*1000) - (i3*100) - (i2*10);
  if (i > 999)
    {
      c[0]=i4+48;
      c[1]=i3+48;
      c[2]=i2+48;
      c[3]=i1+48;
      c[4]='\0';
    }
  else if (i > 99)
    {
      c[0]=i3+48;
      c[1]=i2+48;
      c[2]=i1+48;
      c[3]='\0';
    }
  else if (i > 9)
    {
      c[0]=i2+48;
      c[1]=i1+48;
      c[2]='\0';
    }
  else
    {
      c[0]=i1+48;
      c[1]='\0';
    }
}

	

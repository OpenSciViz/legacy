/* name: log_from_yn(i) */
/* reason: this is one of the best input things.  If you call it,
   for case 0: it returns a 0 if the person answers yes, and -1 if they 
   don't, without needing any intermediate variables (default = No).
   for case 1: it returns a 0 if the person answers no, and -1 if they 
   don't, without needing any intermediate variables (default = Yes). */
/* sample usage:      printf("Do you want to delete this file? (y/N) ");
                      if (log_from_yn(0) == 0) call_delete("all");  */
/* sample usage2:     printf("Do you want to delete this file? (Y/n) ");
                      if (log_from_yn(1) == 0) call_delete("all");  */
/* returns: either 0 (yes) or 1 (no) */
/* init: none required, can do as "int log_from_yn()" */
/* warnings: remember, 0 returned is the opposite of the default.  If
   input variable is not 1, it is assumed to be 0 (default No) */

#include <stdio.h>
#include <ctype.h>
int log_yn(i)
int i;
{
  int ireturn, c;
  c=getchar();
  if (i == 1)
    {
      ireturn = 0;
      if (c == 'n' || c == 'N')ireturn = 1;
    }
  else
    {
      ireturn = 1;
      if (c == 'y' || c == 'Y')ireturn = 0;
    }
  while (c != EOF && c != '\n')
    c = getchar();
  return(ireturn);
}

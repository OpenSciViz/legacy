#include <stdio.h>
#include <ctype.h>
#include <string.h>

void formtime(snull,i1,i2,i3,i4,i5)
char snull[];
int i1, i2, i3, i4, i5;
{
  char buff[5];
  if (i2 == 1)
    strcpy(snull,"January \0");
  else if (i2 == 2)
    strcpy(snull,"February \0");
  else if (i2 == 3)
    strcpy(snull,"March \0");
  else if (i2 == 4)
    strcpy(snull,"April \0");
  else if (i2 == 5)
    strcpy(snull,"May \0");
  else if (i2 == 6)
    strcpy(snull,"June \0");
  else if (i2 == 7)
    strcpy(snull,"July \0");
  else if (i2 == 8)
    strcpy(snull,"Aug \0");
  else if (i2 == 9)
    strcpy(snull,"September \0");
  else if (i2 == 10)
    strcpy(snull,"October \0");
  else if (i2 == 11)
    strcpy(snull,"November \0");
  else if (i2 == 12)
    strcpy(snull,"December \0");
  else
    strcpy(snull,"error \0");

  get_num(buff,i3); /* add day */
  strcat(snull,buff); strcat(snull," \0");

  get_num(buff,i1); /* add year */
  strcat(snull,buff); strcat(snull," \0");

  get_num(buff,i4); /* add hours */
  if (i4 <10)
    strcat(snull,"0\0");
  strcat(snull,buff); strcat(snull,":\0");

  get_num(buff,i5); /* add minutes */
  if (i5 < 10)
    strcat(snull,"0\0");
  strcat(snull,buff); strcat(snull,":\0");

  strcat(snull,"00\0");
}


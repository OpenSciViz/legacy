#include <stdio.h>
#include <string.h>

#include "odbdefs.h"

void summary(FILE *ifp2, struct target_struct t)

{
  fprintf(ifp2,"%14s  %-20s %-12s %6.2f %6.2f %3.1f %6.1f %1d  %2d\n",
	  t.obs_id, t.tar_name,t.pi_lname,t.ra_2000,t.dec_2000,
	  t.tot_obs_time/1000.,t.tot_telem,t.pri,t.num_constr);
}

/* name : parseme(float,itype) */
/* returns a float for ra or degrees (depending on what is needed) after
   either decimals or HH MM SS.S are entered */
#include <stdio.h>
#include <ctype.h>
float parseme(value,itype)
float value;
int itype;
{
  void get_num();
  int readfdp();
  char mys[12];
  float sec;
  int hour, min, iret;

  printf("\n                           ");
  iret=readfdp(mys,value,11); /* pulls back a generic string */

  if (iret == 0) /* new value was entered */
    {
      if ( /* first check if it is HH MM S or DD MM S */
  (isdigit(mys[0]) && isdigit(mys[1]) && isspace(mys[2]) && isdigit(mys[3])
      && isdigit(mys[4]) && isspace(mys[5]) && isdigit(mys[6]))
      || /*                  -DD MM S or +DD MM S */
  ( (mys[0] == '-' || mys[0] == '+') && 
      isdigit(mys[1]) && isdigit(mys[2]) && isspace(mys[3]) && isdigit(mys[4])
      && isdigit(mys[5]) && isspace(mys[6]) && isdigit(mys[7]))
      || /*                    H MM S or D MM S */
  (isdigit(mys[0]) && isspace(mys[1]) && isdigit(mys[2]) && isdigit(mys[3])
      && isspace(mys[4]) && isdigit(mys[5]))
      || /*                    -D MM S or +D MM S */
  ( (mys[0] == '-' || mys[0] == '+') && 
      isdigit(mys[1]) && isspace(mys[2]) && isdigit(mys[3]) && isdigit(mys[4])
      && isspace(mys[5]) && isdigit(mys[6]))
      || /*                    HH M S or DD M S */
  (isdigit(mys[0]) && isdigit(mys[1]) && isspace(mys[2]) && isdigit(mys[3])
      && isspace(mys[4]) && isdigit(mys[5]))
      || /*                    -DD M S or +DD M S */
  ( (mys[0] == '-' || mys[0] == '+') &&  
      isdigit(mys[1]) && isdigit(mys[2]) && isspace(mys[3]) && isdigit(mys[4])
      && isspace(mys[5]) && isdigit(mys[6]))
      || /*                    H M S or D M S */
  (isdigit(mys[0]) && isspace(mys[1]) && isdigit(mys[2]) && isspace(mys[3])
      && isdigit(mys[4]))
      || /*                    -D M S or +D M S */
  ( (mys[0] == '-' || mys[0] == '+') &&  
      isdigit(mys[1]) && isspace(mys[2]) && isdigit(mys[3]) && isspace(mys[4])
      && isdigit(mys[5]))
      )
	{ /* it is in HH MM SS.S format, and can be processed as such */
	  sscanf(mys,"%d %d %f",&hour,&min,&sec);
	  if (itype == 1 && (hour < 0 || hour > 60))
	    printf("Error, hours are not reasonable, continuing anyway.\n");
	  else if (itype == 2 && (hour < -90 || hour > 90))
	    printf("Error, degrees are not reasonable, continuing anyway.\n");
	  if (min < 0 || min > 60)
	    printf("Error, minutes are not reasonable, continuing anyway.\n");
	  if (sec < 0. || sec > 60.)
	    printf("Error, seconds are not reasonable, continuing anyway.\n");
	  if (itype == 1) /* is RA */
	  value =  sec/3600.0 + ((float)min)/60.0 + ((float) hour)*15.0;
	  else /* is DEC */
	    {
	      if (hour < 0)
		value = ((float) hour) - sec/3600.0 + ((float)min)/60.0;
	      else if (hour > 0 && mys[0] == '-') /* the false negative */
		value = 
		  0- ( ((float)hour) + sec/3600.0 + ((float)min)/60.0);
	      else
		value = (float)hour + sec/3600.0 + ((float)min)/60.0;
	    }
	  printf("    (Converting to decimal degrees = %8.4f)\n",value);
	}
      else
	{
	  sscanf(mys,"%f",&value); 
	}
    }
  return(value);
}

/* name: is_blank(str,i) */
/* reason: Routine checks if (for up to i characters, typically one less
    than the string array size), the string is blank (all spaces or nil).
    It returns a 0 if it is blank (yes), -1 if it is not (no).
    if string length is greater than 100, it defaults to 100
    if string length is less than the given isize, it defaults to string length
    if string length is greater than the given isize, naturally it only
    checks for the isize given */
/* sample usage: char str[80]; is_blank(str,79); */
/* returns: a string, loaded into the given string variable */
/* init: no initialization required, but can do as "int is_blank()" */
/* warnings: string length should be 1 less than the array size, and
     this routine can only check to a maximum size of 100 characters,
     requests for larger will only check the first 100 characters. */
#include <stdio.h>
#include <string.h>
int is_blank(char s[],int isize)
{
  int i,ilen;
  char bl[101];
  for (i=0;i<100;++i)
    bl[i]=' ';
  bl[100]='\0';
  i = (-1);
  ilen = strlen(s);
  if ( ilen > 100 && isize > 100)isize=100;
  if (ilen < isize) isize = ilen;
  if (strncmp(s,bl,isize) == 0)i=0;
  return(i);
}

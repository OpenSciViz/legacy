#ifdef DEBUG
#define GRABINT(x,y)  {fscanf(x,"%d",&y); printf("(%d)\n",y);}
#define GRABFL(x,y)   {fscanf(x,"%f",&y); printf("(%f)\n",y);}
#else
#define GRABINT(x,y)  fscanf(x,"%d",&y)
#define GRABFL(x,y)  fscanf(x,"%f",&y)
#endif

#define TIMEL 27  /* max length of dates, i.e. September 21 1995 00:00:00 */
#define IDLENGTH 15  /* length of observation ID, currently xxxxx-aa-bb-cc */
#define KEYNUM 13  /* number of time-criticals possible */
struct target_struct {
/* x_nconfig, nhip, instr_flag, config, telem, and ct_rates are all
   derived/converted in reformat() */
  char x_alt_obs_id[15]; 
       /* not in DB, derived from pinb - prnb - target_no - 99 */
  int x_nconfig, x_nhip; /* not in ODB, need to be derived by counting them */
  int x_instr_flag[24]; /*  8 flags for use with the x_config 
         + 8 for OCF (ignored)
         + 8 for HEXTE (hexte_energya,anglea,dwella,bursta, ditto for b) */
  char x_config[24][31]; /* link to variables:
                       pca_config1 thru 6 + hexte_modea + hexte_modeb 
                       + 8 ignored OCF settings plus 8 for HEXTE */
  float x_telem[9]; /* link to variables:
                     pca_telem1 thru 6 + hexte_telem_sub + 
                     hexte_telema + hexte_telemb */
  float x_ct_rates[13]; /* link to variables;
                       pca_total_count + pca_min_count + pca_max_count +
                       pca_band1 thru 6 + hexte_band1 thru 4 */
  char x_fullname[41];
  int x_tot_obs_time;
  char x_alternate[2], x_scan[2];

    int ao, pri, tot_obs_time, num_constr;
    char obs_id[IDLENGTH],expire[TIMEL],tar_name[21];
    char pi_fname[18], pi_mname[13], pi_lname[28];
    float ra_2000, dec_2000, tot_telem;
    char remarks[5][81];
/* instrument stuff */
    float pca_total_count, pca_min_count, pca_max_count, pca_band1;
    float pca_band2, pca_band3, pca_band4, pca_band5, pca_band6;
    float hexte_band1, hexte_band2, hexte_band3, hexte_band4;
    char pca_config1[31], pca_config2[31], pca_config3[31], pca_config4[31];
    char pca_config5[31], pca_config6[31];
    float pca_telem1, pca_telem2, pca_telem3, pca_telem4, pca_telem5;
    float pca_telem6, pca_telem_sub;
    char hexte_modea[31], hexte_energya[11], hexte_dwella[11], hexte_anglea[11];
    char hexte_bursta[9], hexte_modeb[31], hexte_energyb[11], hexte_dwellb[11];
    char hexte_angleb[11], hexte_burstb[9];
    float hexte_telema, hexte_telemb, hexte_telem_sub;
/* time constraints */
    char coob[2];
    int cyb, cmb, cdb, chb, cmnb, cye, cme, cde, che, cmne;
    char tod[2];
    int tod_hb, tod_mnb, tod_he, tod_mne;
    char monitor[2];
    float min_int, max_int;
    char phase_dep[2];
    float phase_epoch, period, min_phase, max_phase;
    char inter[2];
    float inter_min_time;
    char too[2];
    float too_min_count, too_max_count;
    char other_too[2];
  };

/* name: grabdint(i) */
/* reason: Routine returns an integer of the specified number of digits,
           ignoring non-numbers.  Returning just "return" yields whatever
           the previous default was-- so make sure there is one!
           Allows a default value if just return is entered.  */
/* sample usage:  i = grabdint(isize);
         (use isize<=9 if not relevant)*/
/* returns: an integer of up to the desired size */
/* init: no initialization required, but can do as "long grabint()" */
/* warnings: maximum size passed should not exceed 9 (due to machine limit) */
/* hack added-- if isize is 1, cannot allow negative */
#include <stdio.h>
#include <ctype.h>
long grabdint(int isize,int ii)
{
  long ifred;
  int c, i, iblow, ineg;
  ifred = 0;
  ineg = 0; /* assumes it is positive, first */
  iblow = 1; /* indicates something good was input (anticipating) */
  c = '\n';
  printf(" (default = %d) ",ii);
  for (i=0; i<isize; ++i)
    {
      c=getchar();
      if (iblow == 0 && (c-48 > 0 || c-48 <10))
	iblow = 1; /* assumes finally there was a good input */
      if (i == 0 && (c == EOF || c == '\n' || c-48 <0 || c-48 >10))
	iblow = 0;
      if (i == 0 && c == '-')
	{
	  ineg = 1;
	  if (isize == 1)printf
	    ("Negative value is incorrect, using default value instead.\n");
	}
      if (c== EOF || c== '\n')
	  i=isize+1;
      else if (c-48 >= 0 && c-48 < 10)
	ifred = (ifred*10) + ( (int)c - 48);
    }
  while (c != EOF && c != '\n')
    c=getchar();
  if (ineg == 1)ifred = (-ifred);
  if (iblow == 0)ifred = ii;
  return (ifred);
}

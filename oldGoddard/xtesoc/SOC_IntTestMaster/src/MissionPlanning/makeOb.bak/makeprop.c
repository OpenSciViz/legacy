#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>

#include "keys.h"

int AO;

main(int argc, char *argv[])
{
  FILE *ifp, *ifp2;
  int iopt, idebug;
  int chdir(), closedir();
  void read_dir(int);
  long grabint(int);
  char fname[160],a[80];
/*  struct dirent *readdir(), */
  struct direct *ele;
  DIR *opendir(), *dp;
  void blank_it(struct target_struct),summary(FILE,struct target_struct);
  void writeout(FILE,struct target_struct),readtdef(char,int,int,int);
  void readlive();

  AO = 1; /* for now */

  printf("Okay, this is really betaware... it does not check for violations\n");
  printf("of values, allow editing, or do anything graceful.  BUT, it does\n");
  printf("write out a proposal file in the proper output.  Changes later.\n");

  idebug = 0;
  if (argc > 1 && (argv[1][0] == 'd')) /* someone wants the debug mode */
    {
      printf("(Debug mode on)\n");
      idebug = 1;
    }
  else if (argc > 1)
    printf(
	 "(usage: %cmakeprop%c, or %cmakeprop d%c for debugging)\n",
	   34,34,34,34);

  printf("Welcome to makeprop, the XTE tool for adding TOOs\n");
  if (idebug == 1) /* 0 = no debug output, 1 = all debug output */
    readlive();
  else
    readlive(); /* default is no debug */

  return(0); /* exit with no comments */
}

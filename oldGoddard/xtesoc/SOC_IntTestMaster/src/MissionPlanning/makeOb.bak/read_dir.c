/* hunts through directories and calls 'read_in()' */

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>

void read_dir(int iopt)
/* iopt = 1 reads in files, then writes out a summary file */
/* iopt = 2 reads in the files, then writes out as XTE proposal format */
{
  FILE *ifp, *ifp2;
/*  int chdir(char *), closedir(DIR *); */
  void readtdef(char*,int,int,int);
  long grabint();
  char fname[160],a[80];
/*  struct dirent *readdir(DIR *), */
  struct dirent *ele;
/*  DIR *opendir(char *),*/
  DIR *dp;
  void read_in(FILE *,FILE *,int);

  strcpy(a,getenv("SOCOPS"));
  if ( a != NULL)
    strcat(a,"/planning/proposed");
  else
    printf("Cannot find env variable SOCOPS.\n");
  if (chdir("a") != NULL)
    {
      printf(
   "Cannot find $SOCOPS/planning/proposed, using current directory instead.\n");
      strcpy(a,".\0");
    }
  if ( (dp=opendir(a)) == NULL)
    printf("oops, cannot read directory %s, aborting.\n",a);
  else
    {
      if (iopt == 1) /* summary file */
	{
	  printf("Please enter file name for summary output: ");
	  strcpy(fname,"default.out");
	  readtdef(fname,159,1,0);
	  if ( (ifp2 = fopen(fname,"w")) == NULL)
	    {
	      printf("Could not open output file, aborting.\n");
	      return;
	    }
	  else
	    {
	      fprintf(ifp2,
		      "    ID            Source               PI         ");
	      fprintf(ifp2,
		      "  RA     Dec  Exp  Tele Pri TC\n");
	    }
	} /* end of summary file prep */
      printf(
	     "Processing files in directory %s,\nsummary in %s\n",a,fname);
      while ( (ele=readdir(dp)) != NULL)
	{
	  if (ele->d_name[0] != '.' && /* now do the check to see if the */
	      ele->d_name[5] == '-' && /* file name seems correct */
	      ele->d_name[8] == '-' && ele->d_name[11] == '-' &&
	      strlen(ele->d_name) == 14)
	    {
	      strcpy(fname,a); strcat(fname,"/");
	      strcat(fname,ele->d_name);
	      printf("processing file %s...\n",ele->d_name);
	      if ( (ifp = fopen(fname,"r")) == NULL)
		printf("file open failed for %s\n",fname);
	      else /* input fine, continue */
		{
		  if (iopt == 2) /* open file for proposal output */
		    {
		      strcat(fname,".proposal");
		      if ( (ifp2 = fopen(fname,"w")) == NULL)
			{
			  printf("output file open failed for %s\n",fname);
			  return;
			}
		    }
		  read_in(ifp,ifp2,iopt);
		  if (fclose(ifp) != NULL)
		    printf("input file had problems closing.\n");
		  if (iopt == 2 && fclose(ifp) != NULL)
		    printf("output file %s had problems closing.\n",fname);
		}
	    }
	}
      if (iopt == 1 && fclose(ifp2) != NULL) /* close summary file */
	printf("had problems closing summary file.\n");;
    }
  if ((closedir(dp)) != NULL)
    printf("oops, cannot close directory.\n");
}

#define TIMEL 27  /* max length of dates, i.e. September 21 1995 00:00:00 */
#define IDLENGTH 15  /* length of observation ID, currently xxxxx-aa-bb-cc */
#define KEYNUM 13  /* number of time-criticals possible */

#ifdef DEBUG
#define GRABINT(x,y)  {fscanf(x,"%d",&y); printf("(%d)\n",y);}
#define GRABFL(x,y)   {fscanf(x,"%f",&y); printf("(%f)\n",y);}
#else
#define GRABINT(x,y)  fscanf(x,"%d",&y)
#define GRABFL(x,y)  fscanf(x,"%f",&y)
#endif

struct tc_structure {
    int tc_list[KEYNUM];
    char first_time[TIMEL],last_time[TIMEL],alt_id[IDLENGTH];
    char lead_id[IDLENGTH], trail_id[IDLENGTH], trigger[9];
    int alt_is, alt_has, con_splits, first_day, last_day, min_time;
    int min_sep, max_sep, first_tod,last_tod;
    float min_phase, max_phase, epoch, period, asm_min, asm_max;
  };

struct instr_structure {
    int nconfig, noccult;

    char cfg_config[8][41], ocf_config[8][41];
    float cfg_telem[8], ocf_telem[8];
    int instr_flag[37]; /* 0-7 is CFG, 8-15 is OCF, 16-23 are HIP, 
			   24-36 are PCA:   0 = n/a, 1 = specified */

    int nhip;
    float hex_value[8];
    char burst_a[21], burst_b[21];

    float ct_rates[13];
  };

struct target_struct {
    int ao, pri, exposure, num_tcs;
    char obs_id[IDLENGTH],expire[TIMEL],name[21], pi_name[41];
    float ra, dec, telemetry;
    char remarks[5][81];
    struct tc_structure tc_struct;
    struct instr_structure instr_struct;
  };

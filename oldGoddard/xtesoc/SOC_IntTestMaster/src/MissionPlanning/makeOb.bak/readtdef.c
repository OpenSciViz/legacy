/* note-- this should be modified, right now it is the same as read_dp, but
  I want it to screen out bad characters for file name loading */

/* it takes a third option: 0 -> truncate at first space */
/*                          1 -> screen out spaces and replace with _ */
/*                          2 -> keep spaces as is */
/* Thus this is version 3.0 of this routine */
/* it takes a fourth option: 0 -> keep input as is */
/*                           1 -> convert all lower case to upper case */
/* Thus this is version 4.0 of this routine */

/* based on read_dp(str,i) */
#include <stdio.h>
#include <ctype.h>
void readtdef(char s[],int isize,int iopts,int icap)
{
  void trprintf(char *);
  int is_blank(char *,int);
  int c, i, idef, is_blank();
  idef = 0; /* initially, assume new value will appear */
  c = '\n';
  if (is_blank(s,isize) == 0)
    printf("(default = <none>) ");
  else
    {
      printf("(default = ");
      trprintf(s);
      printf(") ");
    }

  for (i=0; i<isize; ++i)
    {
      c=getchar();
      if (c == EOF || c == '\n')
	{
	  if (i == 0)
	    {
	      idef=1;    /* use default instead */
	      i=isize+1;
	    }
	  else
	    while (i < isize)
	      s[i++] = ' ';
	}
      else
	{
	  if (icap == 1  && islower(c))
	    c = toupper(c);
	  if (iopts == 1 && !isalnum(c) && c != '.')c='_';
	  s[i] = c;

/*	  if ( c == 39)
	    {
	      if (i > isize-2)
		s[i]=' ';
	      else
		{
		  ++i;
		  s[i]=c;
		}
	    }
*/

	}
    }
  if (idef == 0)s[isize]='\0';  /* 'end' only if string has changed */
  while (c != EOF && c != '\n')
    c=getchar();

  if (iopts == 0)
    for (i=0;i<isize;++i)
      if (s[i] == ' ')
	s[i]='\0'; /* truncate string at first space */
}

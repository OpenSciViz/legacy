/* name: trprintf(str) */
/* reason: This routine is a drop-in replacement for printf("%s"), where
       it prints the string without any trailing blanks (for use when the
       string is of a fixed length, but may contain less information than
       its full length, i.e. "sandy     " is a size 10 string).  The
       integer should be the string size of less, and is the MAXIMUM number
       of characters this will print. */
/* sample usage:    char str[80];
               printf("Name = "); trprintf(stdout,s,10); printf("\n"); */
/* returns: this actual prints to the standard output using printf. */
/* init: requires routine "is_blank()" (declared int) to work.
         no initializations required, it is a void */
/* warnings: the integer i should be of array (size-1) or less!! */

#include <stdio.h>
#include <string.h>

void trprintf(char s[])
{
  int i, ilimit;
  int isize;
  int is_blank(char *,int);

  isize = strlen(s);
  ilimit = 0;
  if (is_blank(s,isize) != 0 && isize != 0)  
    /* (made sure it wasn't all blank) */
    {
      for (i=0;i<isize;++i) /* finds last character */
	if (s[i] != ' ')
	  ilimit=i+1;
      for (i=0; i<ilimit; ++i)
	printf("%c",s[i]);
    }
}

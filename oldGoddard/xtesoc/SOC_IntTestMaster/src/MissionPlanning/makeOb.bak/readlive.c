/* prompts for information, then writes out a proposal file */

#include <stdio.h>
#include <string.h>
#include <ctype.h>

extern char *keys[];

#define STRIP(x) {int iii;iii=strlen(x);while(x[iii]==' '||x[iii]=='\0')x[iii--]='\0';}

int AOid;  /* predefine AO, can't be more than a single digit!! */

int iprocess;

#define TIMEL 27  /* max length of dates, i.e. September 21 1995 00:00:00 */
#define IDLENGTH 15  /* length of observation ID, currently xxxxx-aa-bb-cc */
#define KEYNUM 13  /* number of time-criticals possible */


void readlive()
{
  struct target_struct {
    int ao, pri, num_constr;
    float tot_obs_time;
    char obs_id[IDLENGTH],expire[TIMEL],tar_name[21];
    char pi_fname[18], pi_mname[13], pi_lname[28];
    float ra_2000, dec_2000, tot_telem;
    char remarks[5][81];
/* instrument stuff */
    float pca_total_count, pca_min_count, pca_max_count, pca_band1;
    float pca_band2, pca_band3, pca_band4, pca_band5, pca_band6;
    float hexte_band1, hexte_band2, hexte_band3, hexte_band4;
    char pca_config1[31], pca_config2[31], pca_config3[31], pca_config4[31];
    char pca_config5[31], pca_config6[31];
    float pca_telem1, pca_telem2, pca_telem3, pca_telem4, pca_telem5;
    float pca_telem6, pca_telem_sub;
    char hexte_modea[31], hexte_energya[11], hexte_dwella[11], hexte_anglea[11];
    char hexte_bursta[9], hexte_modeb[31], hexte_energyb[11], hexte_dwellb[11];
    char hexte_angleb[11], hexte_burstb[9];
    float hexte_telema, hexte_telemb, hexte_telem_sub;
/* time constraints */
    char coob[2];
    int cyb, cmb, cdb, chb, cmnb, cye, cme, cde, che, cmne;
    char tod[2];
    int tod_hb, tod_mnb, tod_he, tod_mne;
    char monitor[2];
    float min_int, max_int;
    char phase_dep[2];
    float phase_epoch, period, min_phase, max_phase;
    char inter[2];
    float inter_min_time;
    char too[2];
    float too_min_count, too_max_count;
    char other_too[2];
/* used by reformatter, ignored here */
  char x_alt_obs_id[15], x_config[24][31], x_fullname[41];
  int x_nconfig, x_nhip, x_instr_flag[24], x_tot_obs_time;
  float x_telem[9], x_ct_rates[13];
  char x_alternate[2], x_scan[2], x_primary[2];
  };

  struct target_struct a;  /* to hold odb values */
  int i,j,k, l, it, jt, inum_obs, kk;
  char snull[81], tremarks[401], tc1[2], tc2[2], tc3[2], tc4[2], tc5[2];
  char tc6[2], tc7[2], tc8[2], fname[200];
  char ct[2], ct1[2], ct2[2], obschar[3];

  FILE *ifp;

  void readin(char[], int);
  float parseme(float, int);
  float grabdfl(int, float);
  int grabdint(int, int), igiveup, ilen;

  int iopt;
  iopt = 1; /* for now, means 'write out file' */

  igiveup = 0; /* code to exit loop */

  k = 0;

  AOid = 9;

  blank_it(a);

  for (i=0;i<401;++i)
    tremarks[i]='\0'; /* make sure remarks are cleared out each time */
  for (i=0;i<81;++i)
    for (j=0;j<5;++j)
      a.remarks[j][i] = '\0';

  do {

    if (igiveup == 0 || igiveup == 1)
      {
	printf("Please enter proposal ID (e.g. 10401): ");
	it=grabint(9,it);
	printf("  enter target number (usually 1): ");
	jt=grabint(9,jt);

	if (it < 10000)	  /* hack for IOC, keep? */
	  if (it < 10)
	    sprintf(a.obs_id,"%1d000%1d",AOid, it);
	  else
	    sprintf(a.obs_id,"%1d00%2d",AOid, it);
	else	/* the normal AO case */
	  sprintf(a.obs_id,"%5d",it);

	strcat(a.obs_id,"-");
	sprintf(snull,"%d",jt);
	if (jt<10)
	  {
	    strcat(a.obs_id,"0");
	    strncat(a.obs_id,snull,1);
	  }
	else
	  strncat(a.obs_id,snull,2);
  
	strcat(a.obs_id,"-01-00\0");
	
	a.ao=a.obs_id[0]-48;

	printf("    (setting the number of time constraints to 0, for now.)\n");
	a.num_constr = 0;

	printf("  PI first name: ");
	readin(a.pi_fname,17);
	printf("  PI middle name: ");
	readin(a.pi_mname,12);
	printf("  PI last name: ");
	readin(a.pi_lname,27);
	STRIP(a.pi_fname); STRIP (a.pi_lname); STRIP (a.pi_mname);

	strcpy(a.expire,"December 25 1999 00:00:00\0");

	j = 0; l = 0;
	for (i=0; i<strlen(tremarks); ++i)
	  {
	    if (tremarks[i] == '\0' || j > 4)
	      i=strlen(tremarks)+1; /* force an exit */
	    else
	      {
		if(tremarks[i] == '\n')
		  {
		    a.remarks[j][l] = '\0';
		    ++j; l = 0;
		  } /* new line, 0th character */
		else if (l == 80) /* force a new line */
		  {
		    a.remarks[j][80] == '\0';
		    ++j; l = 0; a.remarks[j][l] = tremarks[i]; l++;}
		else
		  {a.remarks[j][l] = tremarks[i]; l++;}
	      }
	  }
      }

    if (igiveup == 0 || igiveup == 2)
      {
	printf("  enter target name: ");
	readin(a.tar_name,20);
	for (ilen=0;i<strlen(a.tar_name);++i)
	  if (a.tar_name[ilen] == ' ')
	    a.tar_name[ilen] = '_';

	do
	  {
	    printf("  enter the RA (J2000 decimal degrees or HH MM SS.S): ");
	    a.ra_2000=parseme(a.ra_2000,1);
	  } while (a.ra_2000 < 0.0 || a.ra_2000 > 360.0);
	do
	  {
	    printf("  enter the Dec (J2000 decimal degrees or DD MM SS.S): ");
	    a.dec_2000=parseme(a.dec_2000,2);
	  } while (a.dec_2000 < -90.0 || a.dec_2000 > 90.0);

	printf("  enter total observation time (in ksec): ");
	a.tot_obs_time= grabdfl(9, a.tot_obs_time);
	printf("  number of pointings? (usually 1): ");
	inum_obs=grabint(9,inum_obs);
	printf("  enter total telemetry rates (kps): ");
	a.tot_telem= grabdfl(9, a.tot_telem);
	printf("    (setting Priority to 1 and leaving remarks blank.)\n");
	a.pri=1;
	if (a.pri == 0) /* no priority assigned */
	  a.pri = 3; /* current default, may change */
  
	for (i=0;i<strlen(a.tar_name);++i)
	  if (a.tar_name[i] == ' ')
	    a.tar_name[i] = '_';

	STRIP(a.tar_name); 
      }

    if (igiveup == 0 | igiveup == 3)
      {
	strcpy(a.x_alternate,"N\0");
	strcpy(a.x_primary,"Y\0");

	/* blank these just to be safe */
	strcpy(tc1,"N\0"); strcpy(tc2,"N\0"); strcpy(tc3,"N\0");
	strcpy(tc4,"N\0"); strcpy(tc5,"N\0"); strcpy(tc6,"N\0");
	strcpy(tc7,"N\0"); if (a.x_scan[0] == 'Y')strcpy(tc7,"Y\0");
	strcpy(tc8,"N\0");
	
	if (a.num_constr > 0) /* 'find time constraint' section */
	  {
	    /* 
	      EXEC SQL SELECT coob, tod, monitor, contig, too, other_too, phase_dep
	      INTO :tc1, :tc2, :tc3, :tc4, :tc5, :tc6, :tc8
	      */
	    
	    if (a.x_primary[0] != 'I' && a.x_primary[0] != 'H' &&
		a.x_primary[0] != 'N')
	      {
		printf(
		       "  Warning, alt flag is not I, H or N for %s, is %s, setting to %s.\n",
		       a.obs_id,a.x_primary,a.x_alternate);
		strcpy(a.x_primary,a.x_alternate);
	      }
	    
	    a.num_constr = 0;
	    if (tc1[0] == 'Y') ++a.num_constr; /* Coord */
	    if (tc3[0] == 'Y') ++a.num_constr; /* Monitor */
	    if (tc2[0] == 'Y') ++a.num_constr; /* TOD */
	    if (tc4[0] == 'Y') ++a.num_constr; /* Interruptable */
	    if (tc5[0] == 'Y' || tc6[0] == 'Y') ++a.num_constr; /* TOO */
	    if (a.x_primary[0] != 'N') ++a.num_constr; /* Alternate */
	    if (tc7[0] == 'Y') ++a.num_constr; /* Scan/Raster */
	    if (tc8[0] == 'Y') ++a.num_constr; /* Phase constraint */
	    
	    
	    if (a.x_primary[0] != 'I' && tc1[0] == 'Y') /* no TCs for alts */
	      { /* Coordinated */
		/* 
		  EXEC SQL SELECT cyb, cmb, cdb, chb, cmnb,
		  cye, cme, cde, che, cmne
		  INTO  :a.cyb, :a.cmb, :a.cdb, :a.chb, :a.cmnb,
		  :a.cye, :a.cme, :a.cde, :a.che, :a.cmne
		  */
		;
	      }
	    if (a.x_primary[0] != 'I' && tc2[0] == 'Y') /* no TCs for alts */
	      { /* TOD */
		/* 
		  EXEC SQL SELECT tod_hb, tod_mnb, tod_he, tod_mne
		  INTO  :a.tod_hb, :a.tod_mnb, :a.tod_he, :a.tod_mne
		  */
		;
	      }
	    if (a.x_primary[0] != 'I' && tc3[0] == 'Y') /* no TCs for alts */
	      { /* Monitor */
		/* 
		  EXEC SQL SELECT min_int, max_int
		  INTO  :a.min_int, :a.max_int
		  */
		;
	      }
	    if (a.x_primary[0] != 'I' && tc4[0] == 'Y') /* no TCs for alts */
	      { /* Contig */
		/*
		  EXEC SQL SELECT contig_min_time
		  INTO  :a.inter_min_time
		  */
		;
	      }
	    if (a.x_primary[0] != 'I' && tc5[0] == 'Y') /* no TCs for alts */
	      { /* TOO */
		/*
		  EXEC SQL SELECT too_min_count, too_max_count
		  INTO  :a.too_min_count, :a.too_max_count
		  */
		;
	      }
	    if (a.x_primary[0] != 'I' && tc8[0] == 'Y') /* no TCs for alts */
	      { /* PAR */
		/*
		  EXEC SQL SELECT phase_epoch, period, min_phase, max_phase
		  INTO  :a.phase_epoch, :a.period, :a.min_phase, :a.max_phase
		  */
		;
	      }
	    
	  } /* closes 'find time constraint' section */

	strcpy(ct,a.x_alternate);
      }

    if (igiveup == 0 || igiveup == 4)
      {
	printf("--- INSTRUMENT MODES ---\n");
	
	printf("  Enter PCA total count rate: ");
	a.pca_total_count=grabdfl(9,a.pca_total_count);
	printf("  Enter PCA min count rate: ");
	a.pca_min_count=grabdfl(9,a.pca_min_count);
	printf("  Enter PCA max count rate: ");
	a.pca_max_count=grabdfl(9,a.pca_max_count);
	printf("  Enter PCA band 1 count rate: ");
	a.pca_band1=grabdfl(9,a.pca_band1);
	printf("  Enter PCA band 2 count rate: ");
	a.pca_band2=grabdfl(9,a.pca_band2);
	printf("  Enter PCA band 3 count rate: ");
	a.pca_band3=grabdfl(9,a.pca_band3);
	printf("  Enter PCA band 4 count rate: ");
	a.pca_band4=grabdfl(9,a.pca_band4);
	printf("  Enter PCA band 5 count rate: ");
	a.pca_band5=grabdfl(9,a.pca_band5);
	printf("  Enter PCA band 6 count rate: ");
	a.pca_band6=grabdfl(9,a.pca_band6);
	
	strcpy(a.pca_config1,"\0");
	a.pca_telem1=1.1;
	strcpy(a.pca_config2,"\0");
	a.pca_telem2=1.1;
	
	printf("  Enter PCA configuration 1 (or NULL): ");
	readin(a.pca_config3,30);
	printf("  Enter telemetry rate for this configuration: ");
	a.pca_telem3=grabdfl(9,a.pca_telem3);
	printf("  Enter PCA configuration 2 (or NULL): ");
	readin(a.pca_config4,30);
	printf("  Enter telemetry rate for this configuration: ");
	a.pca_telem4=grabdfl(9,a.pca_telem4);
	printf("  Enter PCA configuration 3 (or NULL): ");
	readin(a.pca_config5,30);
	printf("  Enter telemetry rate for this configuration: ");
	a.pca_telem5=grabdfl(9,a.pca_telem5);
	printf("  Enter PCA configuration 4 (or NULL): ");
	readin(a.pca_config6,30);
	printf("  Enter telemetry rate for this configuration: ");
	a.pca_telem6=grabdfl(9,a.pca_telem6);
	printf("  Enter total PCA telemetry estimate: ");
	a.pca_telem_sub=grabdfl(9,a.pca_telem_sub);
	printf("  Enter HEXTE band 1 count rate: ");
	a.hexte_band1=grabdfl(9,a.hexte_band1);
	printf("  Enter HEXTE band 2 count rate: ");
	a.hexte_band2=grabdfl(9,a.hexte_band2);
	printf("  Enter HEXTE band 3 count rate: ");
	a.hexte_band3=grabdfl(9,a.hexte_band3);
	printf("  Enter HEXTE band 4 count rate: ");
	a.hexte_band4=grabdfl(9,a.hexte_band4);
	printf("  Enter HEXTE A configuration (or NULL): ");
	readin(a.hexte_modea,30);
	printf("  Enter telemetry rate for this configuration: ");
	a.hexte_telema=grabdfl(9,a.hexte_telema);
	printf("  Enter LLD energy for A: ");
	readin(a.hexte_energya,10);
	printf("  Enter rocking angle for A: ");
	readin(a.hexte_anglea,10);
	printf("  Enter dwell time for A: ");
	readin(a.hexte_dwella,10);
	printf("  Enter Burst value for A: ");
	readin(a.hexte_bursta,8);
	printf("  Enter HEXTE B configuration (or NULL): ");
	readin(a.hexte_modeb,30);
	printf("  Enter telemetry rate for this configuration: ");
	a.hexte_telemb=grabdfl(9,a.hexte_telemb);
	printf("  Enter LLD energy for B: ");
	readin(a.hexte_energyb,10);
	printf("  Enter rocking angle for B: ");
	readin(a.hexte_angleb,10);
	printf("  Enter dwell time for B: ");
	readin(a.hexte_dwellb,10);
	printf("  Enter Burst value for B: ");
	readin(a.hexte_burstb,8);
	printf("  Enter total HEXTE telemetry estimate: ");
	a.hexte_telem_sub=grabdfl(9,a.hexte_telem_sub);
      }

    do {

      printf(" .............................................. \n");
      printf(" 0 = write out file          1 = edit Cover Sheet info\n");
      printf(" 2 = edit target specs       3 = edit TCs\n");
      printf(" 4 = edit configurations     5 = exit without saving\n");

      printf("Choice: ");
      
      igiveup=grabdint(1,igiveup);

      if (igiveup == 5)
	printf(
	  "Do you really want to exit without saving this information? (y/N) ");
      printf("Continue? (y/n) ");
      if (log_yn(0) == 1)
	igiveup = 99;

    } while (igiveup < 0 || igiveup > 5);

  } while (igiveup != 0);


  STRIP(a.pca_config1); STRIP(a.pca_config2); STRIP(a.pca_config3); 
  STRIP(a.pca_config4); STRIP(a.pca_config5); STRIP(a.pca_config6); 
  STRIP(a.hexte_modea); STRIP(a.hexte_modeb);
  STRIP(a.hexte_energya); STRIP(a.hexte_energyb);
  STRIP(a.hexte_dwella); STRIP(a.hexte_dwellb);
  STRIP(a.hexte_anglea); STRIP(a.hexte_angleb);
  STRIP(a.hexte_bursta); STRIP(a.hexte_burstb);

  if (strncmp(a.pca_config1,"STANDARD1",9) == 0)
    strcpy(a.pca_config1,"\0");
  if (strncmp(a.pca_config2,"STANDARD2",9) == 0)
    strcpy(a.pca_config2,"\0");

  a.x_nconfig = 6; /* I put in defaults and then do stuff! */
/*	if (strlen(a.pca_config1) == 0) strcpy(a.pca_config1,"Null\0");
	if (strlen(a.pca_config2) == 0) strcpy(a.pca_config2,"Null\0"); */
	if (strlen(a.pca_config3) == 0) strcpy(a.pca_config3,"Null\0");
	if (strlen(a.pca_config4) == 0) strcpy(a.pca_config4,"Null\0");
	if (strlen(a.pca_config5) == 0) strcpy(a.pca_config5,"Null\0");
	if (strlen(a.pca_config6) == 0) strcpy(a.pca_config6,"Null\0");

/* BEGIN REFORMATTING */

  if ( (strlen(a.pi_lname)+strlen(a.pi_mname)+strlen(a.pi_fname)) < 40)
    {
      strcpy(a.x_fullname,a.pi_fname); strcat(a.x_fullname," ");
      strcat(a.x_fullname,a.pi_mname); strcat(a.x_fullname," ");
      strcat(a.x_fullname,a.pi_lname); strcat(a.x_fullname," ");
    }
  else if ( (strlen(a.pi_lname)+strlen(a.pi_fname)) < 40)
    {
      strcpy(a.x_fullname,a.pi_fname); strcat(a.x_fullname," ");
      strcat(a.x_fullname,a.pi_lname); strcat(a.x_fullname," ");
    }
  else
    strcpy(a.x_fullname,a.pi_lname); /* only room for last name */
  a.x_tot_obs_time = (int)(a.tot_obs_time*1000); 
  /* cast to conv rps to prop */

/* PAUSE REFORMATTING */

  if (inum_obs > 1 && tc3[0] != 'Y') /* multi pts but not a Monitor */
    {
      if (tc7[0] != 'Y') /* also not a Scan! */
	{
	  printf(
"  Warning-- Monitor/Scan TC not given but has %d pts, renaming as Scan.\n");
	  strcpy(tc7,"Y\0"); /* make a monitor */	
	  fflush(stdout);	
	  ++a.num_constr; /* Scan */
	}
    }

/*       a.x_nconfig = 0; */
  a.x_nconfig = 6; /* a hack, for now-- debug */
  for (i=0;i<24;++i) /* zero array of flags */
    a.x_instr_flag[i]=1;  /* ha! assume all exist, for now! */
  a.x_nhip = 0;

/*      strcpy(a.x_config[0],a.pca_config1);
      strcpy(a.x_config[1],a.pca_config2); */
  strcpy(a.x_config[2],a.pca_config3);
  strcpy(a.x_config[3],a.pca_config4);
  strcpy(a.x_config[4],a.pca_config5);
  strcpy(a.x_config[5],a.pca_config6);
  strcpy(a.x_config[6],a.hexte_modea);

  if (is_blank(a.hexte_modeb,30) == 0) /* HEXTE B defaults to same as A */
    strcpy(a.x_config[7],a.hexte_modea);
  else
    strcpy(a.x_config[7],a.hexte_modeb);

  strcpy(a.x_config[16],a.hexte_energya);
  strcpy(a.x_config[17],a.hexte_anglea);
  strcpy(a.x_config[18],a.hexte_dwella);
  strcpy(a.x_config[19],a.hexte_bursta);

  if (is_blank(a.hexte_energyb,10)==0) /* HEXTE B defaults to same as A */
    strcpy(a.x_config[20],a.hexte_energya);
  else
    strcpy(a.x_config[20],a.hexte_energyb);
  if (is_blank(a.hexte_angleb,10)==0) /* HEXTE B defaults to same as A */
    strcpy(a.x_config[21],a.hexte_anglea);
  else
    strcpy(a.x_config[21],a.hexte_angleb);
  if (is_blank(a.hexte_dwellb,10)==0) /* HEXTE B defaults to same as A */
    strcpy(a.x_config[22],a.hexte_dwella);
  else
    strcpy(a.x_config[22],a.hexte_dwellb);
  if (is_blank(a.hexte_burstb,8)==0) /* HEXTE B defaults to same as A */
    strcpy(a.x_config[23],a.hexte_bursta);
  else
    strcpy(a.x_config[23],a.hexte_burstb);

  a.x_telem[0] = a.pca_telem1;
  a.x_telem[1] = a.pca_telem2;
  a.x_telem[2] = a.pca_telem3;
  a.x_telem[3] = a.pca_telem4;
  a.x_telem[4] = a.pca_telem5;
  a.x_telem[5] = a.pca_telem6;
  a.x_telem[6] = a.hexte_telema;
  if (a.hexte_telemb == 0.)  /* HEXTE B defaults to same as A */
    a.x_telem[7] = a.hexte_telema;
  else
    a.x_telem[7] = a.hexte_telemb;
  a.x_telem[8] = a.hexte_telem_sub;

  a.x_ct_rates[0] = a.pca_total_count;
  a.x_ct_rates[1] = a.pca_min_count;
  a.x_ct_rates[2] = a.pca_max_count;
  a.x_ct_rates[3] = a.pca_band1;
  a.x_ct_rates[4] = a.pca_band2;
  a.x_ct_rates[5] = a.pca_band3;
  a.x_ct_rates[6] = a.pca_band4;
  a.x_ct_rates[7] = a.pca_band5;
  a.x_ct_rates[8] = a.pca_band6;
  a.x_ct_rates[9] = a.hexte_band1;
  a.x_ct_rates[10] = a.hexte_band2;
  a.x_ct_rates[11] = a.hexte_band3;
  a.x_ct_rates[12] = a.hexte_band4;

/*  for (i=16; i< 24; ++i)
    if (!(strcmp(a.x_config[i],"IT DEFAULT")) not default
    if    (is_blank(a.x_config[i],25) != 0) 
      {
        a.x_instr_flag[i] = 1;
        ++a.x_nhip;
      }
*/


  for (i=0; i<24; ++i)
    if (strncmp(a.x_config[i],"IT DEFAULT", 10) == 0)
      strcpy(a.x_config[i],"-1\0");

/* END REFORMATTING */
/* DETERMINE POINTING NUMBER FOR OBS ID */
	/* note that inum_obs is only > 1 for Monitor campaigns */

  if (inum_obs > 98) /* remember, -99- is reserved for alternates! */
    {
      printf(
	 "Warning-- many pointings (>98), odd numbering scheme will result\n");
      if (inum_obs > 594)
	printf(
	       "Error-- too many pointings, need to recompile code\n");
    }


  for (k=1;k<=inum_obs;++k)
    { /* usually loop only is done once */
      obschar[0] = '0'; obschar[1] = '1'; /* the default, 1 pointing */

      if (k > 99) /* need to increment SOC-reserved part of seq_no */
	a.obs_id[12]='1';
      if (k > 198) /* need to increment SOC-reserved part of seq_no */
	a.obs_id[12]='2';
      if (k > 297) /* need to increment SOC-reserved part of seq_no */
	a.obs_id[12]='3';
      if (k > 396) /* need to increment SOC-reserved part of seq_no */
	a.obs_id[12]='4';
      if (k > 495) /* need to increment SOC-reserved part of seq_no */
	a.obs_id[12]='5';

      if (k > 1) /* for Monitors, update pointing number in obs_id */
	{
	  kk = k;
	  while (kk > 99)
	    kk = kk-99;
	  get_num(obschar,kk);
	  if (kk < 10)
	    {
	      obschar[1] = obschar[0];
	      obschar[0] = '0';
	    }
	}
      a.obs_id[9] = obschar[0];
      a.obs_id[10] = obschar[1];
      if (a.x_primary[0] == 'I') /* is an alternate */
	{ /* note that obschar still holds the ID for the primary */
	  a.obs_id[9] = '9';
	  a.obs_id[10] = '9';
	}

      if (k > 1) /* add indentation to prettify screen output */
	printf("  ");

      if (a.x_primary[0] == 'I')
	printf("Processing alternate %s(%s)  ",a.obs_id,a.tar_name);
      else if (a.x_primary[0] == 'H')
	printf("Processing primary %s(%s)  ",a.obs_id,a.tar_name);
      else if (a.x_primary[0] == 'N')
	printf("Processing %s(%s)  ",a.obs_id,a.tar_name);
      else
	printf("Processing %s(%s)  ",a.obs_id,a.tar_name);
      fflush(stdout);
      printf("%d TCs: | %s %s %s %s %s %s %s %s %s|\n",
	     a.num_constr,tc1,tc2,tc3,tc4,tc5,tc6,tc7,tc8,a.x_primary);

      if (iopt == 1) /* write out files */
	{	

	  strcpy(fname,getenv("SOCOPS"));
	  strcat(fname,"/planning/proposed/\0");
	  strcat(fname,a.obs_id);

          ifp = fopen(fname,"w");

	  if (ifp == NULL)
	    {
	      printf(
    "Sorry, cannot open output file, please check if tree is in place.\n");
	      exit();
	    }

/* BEGIN WRITEOUT */
	  fprintf(ifp,"%c%s%c\n",34,a.obs_id,34);
	  fprintf(ifp,"%c%s%c\n",34,a.expire,34);
	  fprintf(ifp,"%d\n",a.ao);
	  fprintf(ifp,"%d\n",a.pri);
	  fprintf(ifp,"%c%s%c\n",34,a.tar_name,34);
	  fprintf(ifp,"%.4lf\n",a.ra_2000);
	  fprintf(ifp,"%.4lf\n",a.dec_2000);
	  fprintf(ifp,"%c%s%c\n",34,a.x_fullname,34);
	  fprintf(ifp,"%f\n",a.tot_telem);
	     /* note that inum_obs holds the # of pointings, always 1 unless
	        it is a monitor campaign */
	  fprintf(ifp,"%d\n",a.x_tot_obs_time/inum_obs);

	  fprintf(ifp,"%cCST%c\n",34,34);


/* TIME CONSTRAINTS */
	  fprintf(ifp,"%d\n",a.num_constr);

	  if (tc1[0] == 'Y')
	    {
	      a.coob[0] = tc1[0];
	      fprintf(ifp,"%cATR%c\n",34,34);
	      formtime(snull,a.cyb,a.cmb,a.cdb,a.chb,a.cmnb);
	      fprintf(ifp,"%c%s%c\n",34,snull,34);
	      formtime(snull,a.cye,a.cme,a.cde,a.che,a.cmne);
	      fprintf(ifp,"%c%s%c\n",34,snull,34);
	    }

	  if (a.x_primary[0] == 'I') /* is the alternate pointing */
            {
	      fprintf(ifp,"%cALT%c\n",34,34);
	      a.obs_id[9] = obschar[0]; /* primary ID is still saved here */
	      a.obs_id[10] = obschar[1];
 	      fprintf(ifp,"%c%s%c\n",34,a.obs_id,34);
	      fprintf(ifp,"1\n0\n");
 	      a.obs_id[9]='9';
	      a.obs_id[10]='9';
	    }
	  else if (a.x_primary[0] == 'H')
            {
	      fprintf(ifp,"%cALT%c\n",34,34);
 	      a.obs_id[9]='9'; /* always, the alternate is ID -99- */
	      a.obs_id[10]='9';
	      fprintf(ifp,"%c%s%c\n",34,a.obs_id,34); 
	      a.obs_id[9] = obschar[0]; /* primary ID is still saved here */
	      a.obs_id[10] = obschar[1];
	      fprintf(ifp,"0\n1\n");
	    }
	  if (tc4[0] == 'Y')
	    {
	      fprintf(ifp,"%cINT%c\n",34,34);
	      fprintf(ifp,"%d\n",a.inter_min_time*1000);
	    }
	  if (tc8[0] == 'Y')
	    {
	      fprintf(ifp,"%cPAR%c\n",34,34);
	      fprintf(ifp,"%f\n",a.min_phase);
	      fprintf(ifp,"%f\n",a.max_phase);
	      fprintf(ifp,"%f\n",a.phase_epoch);
	      fprintf(ifp,"%f\n",a.period);
	    }
	  if (tc3[0] == 'Y')
	    {
	      fprintf(ifp,"%cREL%c\n",34,34);
	    /* here, we can overwrite the saved obschar because it is
	       never used again before being re-created for the next pt */
	      if (k == 1) /* first pointing of the monitor */
	     fprintf(ifp,"%c-1%c\n",34,34); /* i.e. nothing leading up to it */
	      else
		{
		  get_num(obschar,k-1);
		  if (k-1 < 10)
		    {
		      obschar[1] = obschar[0];
		      obschar[0] = '0';
		    }
	  a.obs_id[9] = obschar[0]; /* primary ID is still saved here */
		  a.obs_id[10] = obschar[1];
		  fprintf(ifp,"%c%s%c\n",34,a.obs_id,34); /* leadingObsID */
		}
	      if (k == inum_obs) /* last pointing of the monitor */
		fprintf(ifp,"%c-1%c\n",34,34); /* i.e. nothing following it */
	      else
		{
		  get_num(obschar,k+1);
		  if (k+1 < 10)
		    {
		      obschar[1] = obschar[0];
		      obschar[0] = '0';
		    }
		 a.obs_id[9] = obschar[0]; /* primary ID is still saved here */
		  a.obs_id[10] = obschar[1];
		  fprintf(ifp,"%c%s%c\n",34,a.obs_id,34); /* trailingObsID */
		}
	      fprintf(ifp,"%d\n",a.min_int*1000);
	      fprintf(ifp,"%d\n",a.max_int*1000);
	    }
	  if (tc2[0] == 'Y')
	    {
	      fprintf(ifp,"%cTOD%c\n",34,34);
	      fprintf(ifp,"%d\n",a.tod_hb*3600 + a.tod_mnb*60);
	      fprintf(ifp,"%d\n",a.tod_he*3600 + a.tod_mne*60);
	    }
	  if (tc7[0] == 'Y')
	    {
	      fprintf(ifp,"%cSCA%c\n",34,34);
	    }
	  if (tc5[0] == 'Y' || tc6[0] == 'Y')
	    {
	      fprintf(ifp,"%cTOO%c\n",34,34);
	      if (tc5[0] == 'Y' && tc6[0] =='Y')
		fprintf(ifp,"%cBOTH%c\n",34,34);
	      else if (tc5[0] == 'Y')
		fprintf(ifp,"%cASM%c\n",34,34);
	      else if (tc6[0] == 'Y')
		{
		  fprintf(ifp,"%cEXTERNAL%c\n",34,34);
		  a.too_min_count = (-1);
		  a.too_max_count = (-1);
		}
	      fprintf(ifp,"%f\n",a.too_min_count);
	      fprintf(ifp,"%f\n",a.too_max_count);
	    }
	    
/* END OF TIME CONSTRAINTS */

	  fprintf(ifp,"%c%s%c\n",34,keys[0],34);  /* CFG */
	  fprintf(ifp,"%d\n",a.x_nconfig); 

	  for (i=2; i < 8; ++i) /* ditched standard 1 and 2 */
	    {
	      fprintf(ifp,"%c%s%c\n",34,keys[i+7],34);
	      fprintf(ifp,"%c%s%c\n",34,a.x_config[i],34);
	      fprintf(ifp,"%f\n",a.x_telem[i]);
	    }
	  fprintf(ifp,"%c%s%c\n",34,keys[1],34);  /* OCF */
	  fprintf(ifp,"0\n");
	  fprintf(ifp,"%c%s%c\n",34,keys[2],34);  /* HIP */
/*	  fprintf(ifp,"%d\n",a.x_nhip); */
	  fprintf(ifp,"%d\n",8); /* for now, all parameters are written out */
	  for (i=0; i < 8; ++i)
	    if (a.x_instr_flag[i+16] == 1) /* config specified */
	      {
	        fprintf(ifp,"%c%s%c\n",34,keys[i+7+16],34);
	        if (i == 3 || i == 7 || i == 1 || i == 5)
	          fprintf(ifp,"%c%s%c\n",34,a.x_config[i+16],34);
	        else
	          fprintf(ifp,"%s\n",a.x_config[i+16]);
	      }
	  fprintf(ifp,"%c%s%c\n",34,keys[3],34);  /* PCR */
	  for (i=0; i < 9; ++i)
	    {
	      fprintf(ifp,"%f\n",a.x_ct_rates[i]);
	    }
	  fprintf(ifp,"%c%s%c\n",34,keys[4],34);  /* HCR */
	  for (i=9; i < 13; ++i)
	    fprintf(ifp,"%f\n",a.x_ct_rates[i]);
	  fprintf(ifp,"%c%s%c\n",34,keys[5],34);  /* REM */
	  for (i=0;i<5;++i)
	   {
	     STRIP(a.remarks[i]);
	     if (strlen(a.remarks[i]) > 0 && is_blank(a.remarks[i]) != 0)
 	       fprintf(ifp,"%c%s%c\n",34,a.remarks[i],34);
	   }
	  fprintf(ifp,"%c%s%c\n",34,keys[6],34);  /* END */

/* END OF WRITEOUT */

          fclose(ifp);
        }

    }

printf("Finished processing.\n"); fflush(stdout);

}

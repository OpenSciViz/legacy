head	3.7;
access
	ejohnson
	soccm;
symbols
	Build3:3.1;
locks; strict;
comment	@# @;


3.7
date	95.12.01.18.10.20;	author ejohnson;	state Exp;
branches;
next	3.6;

3.6
date	95.08.02.13.44.28;	author ejohnson;	state Exp;
branches;
next	3.5;

3.5
date	95.06.16.17.47.58;	author ejohnson;	state Exp;
branches;
next	3.4;

3.4
date	95.02.28.20.31.42;	author ejohnson;	state Exp;
branches;
next	3.3;

3.3
date	95.02.28.20.07.12;	author ejohnson;	state Exp;
branches;
next	3.2;

3.2
date	95.02.24.21.31.31;	author ejohnson;	state Exp;
branches;
next	3.1;

3.1
date	94.05.11.13.22.35;	author ejohnson;	state Exp;
branches;
next	;


desc
@Creation
@


3.7
log
@Added -s startTime to man page.
@
text
@.\" Variables: TITLE, 
.\"            CAPTITLE, 
.\"            SHRT_DESCRIPTION, 
.\"            DESC_PAR1, 
.\"            DESC_PAR2,
.\"            OPTIONS,
.\"            REQUIRED,
.\"            DATE,
.\"            OPTION1,
.\"            OPTION2,
.\"            OPTION1_DESC,
.\"            OPTION2_DESC,
.\"            FILE1,
.\"            FILE1_DESC,
.\"            ENTRY1,
.\"            ENTRY2,
.\"            AUTHER,
.\"            BUGLIST
.\" @@(#)TITLE.1 1.21 $Id: mp_createSTS.1,v 3.6 1995/08/02 13:44:28 ejohnson Exp ejohnson $
.\" Part of the XTE spacecraft software
.\"
.TH MP subsystem  Feb_24,_1995
.SH 1
mp_createSTS \- Short Term scheduler program
.LP
.B mp_createSTS
[
.I -c -g -r<1/2/3/4> -m <1/2/3> -d 
.I -p path -h -u -o -x -t 
.I -s startTime
.I -sched schedName
.I -lastWeekInsert insertFile
.I -lastWeekValues "obName ra dec in_saa in_occult"
.I -k "spikeParms" -a
] 
.IR -n 
.I "name" 
.I -M 
.I "comment" 
.I -e 
.I fileName
.SH DESCRIPTION
.IX  "mp_createSTS command"  ""  "\fLfinger\fP \(em info on users"
.IX  users  "info on users"  users  "info on users \(em \fLfinger\fP"
.IX  login  "info on users"  login  "info on users \(em \fLfinger\fP"
.LP
Using the output of long term schedule produces a short term schedule.
Includes the production of several reports, command generation input, and
instrument configuring DCF's.
.TP
.B \-n "string"
Name of the person creating the schedule.  This name is added to reports.
This is required.
.TP
.B \-M "string"
Comment on why this schedule is being produced. This comment is added to reports. 
This is required
.TP
.B \-e fileName
Name of the month long ephemeris file to us in calculating orbit.
This is required
.TP
.B \-r <1/2/3/4>
Strategy for making a schedule (in auto mode [-m 1] )
1 = (Early Greedy) [DEFAULT]
2 = (Earlest Min. Conficts) 
3 = (Max. Preference)
4 = (High Priority).
This only make a difference in [-m 1] (auto schedule) mode.
.TP
.B \-m <1/2/3>
Mode of creating the short term schedule
1 = (auto schedule) 
2 = (bring up SPIKE GUI) [DEFAULT]
3 = (don't use SPIKE). 
Auto schedule uses spike but does not allow human editing.  3 (don't use SPIKE) uses previous creatred SPIKE outputs to creaet the reports needed, this is usefull when some changes in the output of SPIKE are needed (via needle or hand editing of SPIKE events ffile) (p.s. this picks out the schedule marked as "SELECTED" in $SPIKEROOT/sched/save-scheds.current)
.TP
.B \-s MM-DD-YY[:HH:MM:SS]
The start time of the week you are to schedule.
.TP
.B \-p path
Use a path other the environment variable SOCOPS.
.TP
.B \-h
Produce help output
.TP
.B \-c
Produce a command generation insert file.  This file contains the listing of all the events in a week and when the dcf's (desired configurations of instruments) should be executed.  This file appears in $SOCOPS/planning/WeekXXX where XXX is the number of the week.
.TP
.B \-g
Produce a short term schedule report for the GOF.  This file  contains the listing of all the events in a week and appears in $SOCOPS/planning/gofreports.
.TP
.B \-o
Don't update the observation database with information SPIKE produces.  Used during test or unoffical runs of system.
.TP
.B \-u
Run the command generation output.  Produces the dcf's.
.TP
.B \-x
Produce expected counts output. 
.TP
.B \-t
Produce predicted telemetry report.
.TP
.B \-a
Adjust a schedule with a new ephemeris file.
Used with the phantom mode -m 3.
.TP
.B \-k "spike_parm_name1 value1 spike_parm_name2 value2 ..." [Not implimited]
Change the spike parmeters to values named.  See SPIKE documentation for name of parameters.
.TP
.B \-sched schedName
Selects a schedule already make.  This is useful when not actually running SPIKE (ie [-m 3]).  It changes the file "SELECTED" in $SPIKEROOT/sched/saved-sched.current.
.TP
.B \-lastWeekInsert insertFileName
Selects a insertFile to use to get information on what state we were in at the end of last week.
.TP
.B \-lastWeekValues obName ra dec "\( in\_saa TRUE=1, FALSE=0\)"
.B      "\( in\_occult TRUE=1, FALSE=0\)                                     "
If there was no insert file for last week or some other problem you can hand enter the state you left spacecraft in from last week here.
.SH FILES
.PD 0
.TP 
.B $SOCOPS/planning/gofreports/ShortTermScheduler_WWW_VVV.report
The GOF report on the Short Term schedule produced.
.LP
.PD 0
.TP
.B $SOCOPS/planning/WeekWWW/dtlWWW_VVV.ins
The "timeline" the goes into command generations dtl, prt, dap to produce commands.
.LP
.PD 0
.TP
.B $SOCOPS/planning/WeekWWW/dcfWWW_VVV.csh
The csh program that creates all the DCF's needed by the week in questions.
.LP
.PD 0
.TP
.B $SOCOPS/planning/WeekWWW/ExpectCounts
Required input into science monitoring.
.LP
.PD 0
.TP
.B $SOCOPS/command/mocdeliver/XYYYYDDD_VV_S_REP.RPT
Telemetry report for the MOC.
.LP
.PD 0
.TP
.B $SOCOPS/command/configs/ACS/NNNNN-NN-NN-NN_ACS.dcf
ACS commands needed for timeline.
.LP
.PD 0
.TP
.B $SOCOPS/command/configs/EDS/
.B NNNNN-NN-NN-NN_<EVENT>_EDS.dcf
EDS commands needed for timeline.
.LP
.PD 0
.TP
.B $SOCOPS/command/configs/HEXTE/
NNNNN-NN-NN-NN_<EVENT>_HEXTE.dcf
HEXTE commands neede for the timeline.
.PD
.SH EXAMPLE
.PD. 0
.LP
// create observation database from proposals
.LP
.B mp_createDB  
.LP
// create long term schedule.
.LP 
.B mp_createLTS 
-m 2 -t 04-22-95 -s 04-22-95 -c 2 -e X1995100EPHLO.01 -M ejohnson -n "example run"
.LP
.B mp_createSTS -c -g -e X1995100EPHL0.01 -x -t -M ejohnson -n "example STS run"
.SH "SEE ALSO"
.BR mp_createLTS,
.BR command 
.BR generation,
.BR MP-GOF 
.BR ICD
.LP
Earl Johnson,
Feb 24, 1995, ejohnson@@xema.stx.com.
.SH BUGS
.LP
1 - (-lastValue and -lastInsert) dont work yet.
2 - (-k spikeParms) not implimited.
@


3.6
log
@Noted that -k option not implimited.
@
text
@d19 1
a19 1
.\" @@(#)TITLE.1 1.21 $Id: mp_createSTS.1,v 3.5 1995/06/16 17:47:58 ejohnson Exp ejohnson $
d30 1
d78 3
d118 2
a119 1
.B \-lastWeekValues "obName ra dec {in_saa TRUE =1, FALSE = 0} {in_occult TRUE=1, FALSE=0}
@


3.5
log
@Improved.
@
text
@d19 1
a19 1
.\" @@(#)TITLE.1 1.21 $Id: mp_createSTS.1,v 3.4 1995/02/28 20:31:42 ejohnson Exp ejohnson $
d105 1
a105 1
.B \-k "spike_parm_name1 value1 spike_parm_name2 value2 ..."
d184 1
@


3.4
log
@*** empty log message ***
@
text
@d19 1
a19 1
.\" @@(#)TITLE.1 1.21 $Id: mp_createSTS.1,v 3.3 1995/02/28 20:07:12 ejohnson Exp ejohnson $
d30 3
d51 1
a51 1
Name of the person creating the schedule
d55 1
a55 1
Comment on why this schedule is being produced.
d59 1
a59 1
Name of the month long ephemeris file to us in calculating orbit
d67 2
a68 1
4 = (High Priority)
d74 2
a75 1
3 = (don't use SPIKE)
d84 1
a84 1
Produce a command generation insert file.
d87 1
a87 1
Produce a short term schedule report for the GOF
d90 1
a90 1
Don't update the observation database with information SPIKE produces.
d93 1
a93 1
Run the command generation output.
d96 1
a96 1
Produce expected counts output
d106 10
a115 3
Change the spike parmeters to values named.


d159 13
d183 1
a183 1
BUGLIST
@


3.3
log
@Updated with new parmeters
@
text
@d19 1
a19 1
.\" @@(#)TITLE.1 1.21 $Id: mp_createSTS.1,v 3.2 1995/02/24 21:31:31 ejohnson Exp ejohnson $
d28 1
a28 1
.I -c -g -r -m <1/2/3> -d 
d59 7
@


3.2
log
@*** empty log message ***
@
text
@d19 1
a19 1
.\" @@(#)TITLE.1 1.21 $Id: mp_createSTS.1,v 3.1 1994/05/11 13:22:35 ejohnson Exp ejohnson $
d28 3
a30 1
.I -c -g -r -m <1/2/3> -d -p path -h -u -o -x -t
d32 6
a37 1
.IR -n "name" -M "comment" -e  fileName
d47 1
a47 1
.B \--n "string"
d49 1
d51 1
a51 1
.B \--M "string"
d53 1
d55 1
a55 1
.B \--e fileName
d57 1
d59 1
a59 1
.B \--m <1/2/3>
d65 1
a65 1
.B \--p path
d68 1
a68 1
.B \--h
d71 1
a71 1
.B \--c
d74 1
a74 1
.B \--g
d77 1
a77 1
.B \--o
d80 1
a80 1
.B \--u
d82 2
a83 1
.B \--x
d85 2
a86 1
.B \--t
d88 7
d99 1
a99 1
.TP 20
d103 1
d108 1
d113 1
d118 1
d123 1
d128 1
d130 2
a131 1
.B $SOCOPS/command/configs/EDS/NNNNN-NN-NN-NN_<EVENT>_EDS.dcf
d134 1
d136 2
a137 1
.B $SOCOPS/command/configs/HEXTE/NNNNN-NN-NN-NN_<EVENT>_HEXTE.dcf
@


3.1
log
@Creation
@
text
@d19 1
a19 1
.\" @@(#)TITLE.1 1.21 $Id$
d22 1
a22 1
.TH CAPTITLE 1 DATE
d24 3
a26 2
TITLE \- SHRT_DESCRIPTION
.B TITLE
d28 1
a28 1
.I OPTIONS
d30 1
a30 1
.IR REQUIRED 
d32 1
a32 1
.IX  "TITLE command"  ""  "\fLfinger\fP \(em info on users"
d36 55
a90 1
DESC_PAR1
d92 3
a94 1
DESC_PAR2
d96 3
a98 1
DESC_PAR3
a99 1
.SH OPTIONS
d101 3
a103 2
.B \-OPTION1
OPTION1_DESC
d105 3
a107 7
.B \-OPTION2
OPTION2_DESC
.SH FILES
.PD 0
.TP 20
.B FILE1
FILE1_DESC
d109 2
a110 2
.B FILE2
FILE2_DESC
d113 5
a117 2
.BR ENTRY1,
.BR ENTRY2,
d120 1
a120 1
DATE, ejohnson@@xema.stx.com.
@

# Christina Williams Heikkila
# This function is called by many programs, edit carefully!
# Leap-seconds and leap-years need to be hand-edited into this subroutine.
# This subroutine is meant to be a library; other scripts call it using
#   'require /home/socops/quickfix/METconv.pl;'.  The path must be given, so
#   that scripts in directories other than quickfix can use it. 
#   SO DONT MOVE THIS SCRIPT FROM /home/socops/quickfix/!!!!!!!!
# This script has been tested and is Year-2000-compliant.
# These dates have been tested (converted in both directions) and
# have been found to agree with xTime:
# 1999:001:00:00:00 = 157766404
# 2000:100:10:10:10 = 197892614
# 2000:366:10:10:10 = 220875014  # oops, this one doesn't work 
                                 # going MET -> yyyy:ddd:hh:mm:ss
# 2001:001:00:00:00 = 220924804
# 2004:001:00:00:00 = 315532804
# 2004:034:00:00:00 = 318384004
# these work within the limits that the only leap seconds applied
# are the ones through Jan 1 1999.  New leap seconds must be coded 
# here to take effect.
# BUG: there is a problem in going from MET to yyyy:doy:hh:mm:ss
# on the last day of the year in leap years (day 366). 
# The conversion results in the correct time and year, but gives
# the day of year to be 365 instead of 366.

package METconv;

# SUBROUTINE TO TRANSLATE MET TO YYYY:DDD:HH:MM:SS AND VICE VERSA
 
sub convert {
    local($btime) = @_;
    $btime =~ s/ /:/g;
 
  if ($btime =~ /:/) {  # input is in YYYY:DDD:HH:MM:SS, I hope.
 
    local($year,$doy,$hour,$min,$sec) = split(/:/, $btime);
    if ($year <1900) {$year = $year + 1900};
    $dayoffset = ($year - 1994) * 365;  # Spacecraft MET, not XTE MET
    $doy--;
    if ($year > 1996) { $dayoffset++ }; # 1996 is a leap year
    if ($year > 2000) { $dayoffset++ }; # 2000 is a leap year
    if ($year > 2004) { $dayoffset++ }; # 2004 is a leap year
    # ^^^ during the leap years themselves, the dayoffset is already in the doy
    $day = $doy + $dayoffset;
    if ($day >= 181)  {$sec++}; #add one leap second for July 1 1994
    if ($day >= 730)  {$sec++}; #add one leap second for  Jan 1 1996
    if ($day >= 1277) {$sec++}; #add one leap second for July 1 1997
    if ($day >= 1826) {$sec++}; #add one leap second for Jan 1 1999
    $met = $sec + ($min * 60) + ($hour * 3600) + ($day * 86400);
    return $met;
 
  } else {  # I assume input is met, in seconds.
 
    local($met) = $btime;
    $met =~ s/^0*//g;
    if ($met > 15638400) {$met--}; #subtract one leap second for July 1 1994
    if ($met > 63072001) {$met--}; #subtract one leap second for Jan 1 1996
    if ($met > 110332802) {$met--}; #subtract one leap second for July 1 1997
    if ($met > 157766403) {$met--}; #subtract one leap second for Jan 1 1999
    $day=$met/86400.0;
    $yeardiff=int(int($day)/365);
    $year=1994+$yeardiff;
    $day-- if $year > 1996;   # 1996 is a leap year
    $day-- if $year > 2000;   # 2000 is a leap year
    $day-- if $year > 2004;   # 2004 is a leap year
    $yeardiff=int(int($day)/365);
    $year=1994+$yeardiff;
    $doy = 1 + int($day - ($yeardiff*365));
    $xhour=(($day-int($day))*24.0);
    $hour=int($xhour);
    $xmin=(($xhour-int($xhour))*60.0);
    $min=int($xmin);
    #$sec=int(($xmin-int($xmin))*60.0);
    $sec=(($xmin-int($xmin))*60.0);
    $fracsec=$sec-int($sec);
    $sec=int($sec); 
    $sec++ if $fracsec >= 0.5;
    until ($sec < 60) {
       $sec=60-$sec;
       $min++
    }
    until ($min < 60) {
       $min=60-$min;
       $hour++
    }
    until ($hour < 24) {
       $hour=24-$hour;
       $doy++;
    }
    until ($doy < 366 || $year eq "1996") {
       $doy=366-$doy;
       $year++;
    }
    if ($doy < 100) {$doy = "0". $doy};
    if ($doy < 10) {$doy = "0". $doy};
    if ($hour < 10) {$hour = "0". $hour};
    if ($min < 10) {$min = "0". $min};
    if ($sec < 10) {$sec = "0". $sec};
    $output="$year:$doy:$hour:$min:$sec";
    return $output;
  }
}
1;

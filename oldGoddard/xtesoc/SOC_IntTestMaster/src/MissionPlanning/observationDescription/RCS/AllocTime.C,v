head	2.17;
access
	ejohnson
	socops
	subrata
	soccm;
symbols
	Build_4_3_2_Sim4_qf:2.17
	Build4_3_2:2.17
	Build4_3_1:2.16
	Build4_3c:2.16
	Build4_3b:2.16
	Build4_3a:2.16
	Build4_3:2.16
	LAST_SOCTime_Version:2.16
	Build4_2:2.16
	Build4_1:2.16
	Build4:2.16
	Build3_3:2.16
	Build3_2:2.16
	Build3_1:2.12
	Build3:2.15
	Current:2.12
	Build2:2.5
	Build1:1.6;
locks
	subrata:2.17; strict;
comment	@ * @;


2.17
date	95.06.15.16.41.47;	author ejohnson;	state Exp;
branches;
next	2.16;

2.16
date	94.08.23.15.54.35;	author ejohnson;	state Exp;
branches;
next	2.15;

2.15
date	94.08.12.22.14.36;	author ejohnson;	state Exp;
branches;
next	2.14;

2.14
date	94.08.12.19.21.58;	author ejohnson;	state Exp;
branches;
next	2.13;

2.13
date	94.08.02.13.09.20;	author ejohnson;	state Exp;
branches;
next	2.12;

2.12
date	94.05.03.14.31.20;	author ejohnson;	state Exp;
branches;
next	2.11;

2.11
date	94.04.15.14.04.54;	author ejohnson;	state Exp;
branches;
next	2.10;

2.10
date	94.04.13.01.00.43;	author ejohnson;	state Exp;
branches;
next	2.9;

2.9
date	94.04.10.19.12.09;	author ejohnson;	state Exp;
branches;
next	2.8;

2.8
date	94.03.28.13.50.34;	author ejohnson;	state Exp;
branches;
next	2.7;

2.7
date	94.03.14.21.08.31;	author ejohnson;	state Exp;
branches;
next	2.6;

2.6
date	94.03.08.16.46.53;	author ejohnson;	state Exp;
branches;
next	2.5;

2.5
date	93.12.07.15.12.51;	author ejohnson;	state Release;
branches;
next	2.4;

2.4
date	93.12.01.06.08.47;	author ejohnson;	state Release;
branches;
next	2.3;

2.3
date	93.11.22.22.54.42;	author ejohnson;	state Exp;
branches;
next	2.2;

2.2
date	93.11.17.22.05.56;	author ejohnson;	state Exp;
branches;
next	2.1;

2.1
date	93.10.12.19.18.05;	author leone;	state Exp;
branches;
next	2.0;

2.0
date	93.08.20.21.52.10;	author leone;	state Exp;
branches;
next	1.6;

1.6
date	93.07.06.20.39.14;	author leone;	state Exp;
branches;
next	1.5;

1.5
date	93.06.28.22.45.58;	author leone;	state Exp;
branches;
next	1.4;

1.4
date	93.05.28.20.45.55;	author leone;	state Exp;
branches;
next	1.3;

1.3
date	93.05.27.00.53.39;	author leone;	state Exp;
branches;
next	1.2;

1.2
date	93.04.21.15.57.47;	author leone;	state Exp;
branches;
next	1.1;

1.1
date	93.04.20.19.59.04;	author leone;	state Exp;
branches;
next	;


desc
@Creation
@


2.17
log
@Fixed Unit test.
@
text
@// Subsystem: Mission Planning(ObservationDescription class)
// RCS: $Id: AllocTime.C,v 2.16 1994/08/23 15:54:35 ejohnson Exp ejohnson $
// Programmer: Earl Johnson,  ejohnson@@xema.stx.com,  original Rick Leone
//
// Description: The file AllocTime.H is the class description for the class
// AllocatedTime of the XTE-SOC class library. This class is a subclass
// of AllocatedTime class and is an attribute of the ObservationDe-
// scription class. The AllocatedTime object is the amount of exposure
// time that the NASAHQ has awarded the observation.
//

static const char rcsid[] = "$Id: AllocTime.C,v 2.16 1994/08/23 15:54:35 ejohnson Exp ejohnson $";


// System headers
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <rw/vstream.h>
#include <rw/rwfile.h>
#include <rw/cstring.h>

// Local headers
#include "trace.h"
#include "mp_global.h"
#include "iostream_err.h"
#include "rwvstream_err.h"
#include "rwfile_err.h"
#include "GOF_fuzzy.h"
#include "stringDB.h"
#include "stringIO.h"
#include "GOF_io.h"
#include "random.h"
#include "AllocTime.h"

// Macros
RWDEFINE_COLLECTABLE(AllocatedTime, AllocatedTimeID)

// File scope variables

// External variables

// External functions

// Structures and unions

// Signal catching functions

// Functions

// To advoid warning on rcsid
NOREF_RCSID

//***********************************************************
// AllocatedTime
//
// Description:
// Default constructor for the AllocatedTime Class.
// Uses: AllocatedTime::setConstraintName().
// FKEYS ~~CREATION ~~VIRGIN ~~TOTAL_STATE
AllocatedTime::AllocatedTime(void)
{
  TRACEM("AllocatedTime::AllocatedTime(void)");

   constraintName = ALLOCATEDTIME;
   in_Error = FALSE;

  RETURN_VOID;
}

//***********************************************************
// AllocatedTime
//
// Description:
// Default constructor for the AllocatedTime Class.
// Uses: AllocatedTime::setConstraintName().
// FKEYS ~~CREATION ~~VIRGIN ~~TOTAL_STATE
AllocatedTime::AllocatedTime(Seconds seconds):duration(seconds)
{
  TRACEM("AllocatedTime::AllocatedTime(Seconds seconds)");

   constraintName = ALLOCATEDTIME;
   in_Error = FALSE;

  RETURN_VOID;
}

//***********************************************************
// AllocatedTime
//
// Description:
// Constructor for the AllocatedTime Class.
// Uses: AllocatedTime::setConstraintName().
// FKEYS ~~CREATION ~~VIRGIN ~~TOTAL_STATE
AllocatedTime::AllocatedTime(int days, long hours, long minutes, long seconds):duration((days * 86400 + hours * 3600 + minutes * 60 + seconds))
{
  TRACEM("AllocatedTime::AllocatedTime(int days, long hours, long minutes, long seconds)");

   PARM_CHECK((days < 0));
   PARM_CHECK((hours < 0));
   PARM_CHECK((hours >= 24));
   PARM_CHECK((minutes < 0));
   PARM_CHECK((minutes >= 60));
   PARM_CHECK((seconds < 0));
   PARM_CHECK((seconds >= 60));

   constraintName = ALLOCATEDTIME;
   in_Error = FALSE;


  RETURN_VOID;
}

//***********************************************************
// AllocatedTime
//
// Description:
// Constructor for the AllocatedTime Class.
// Uses: AllocatedTime::setConstraintName().
// FKEYS ~~CREATION ~~VIRGIN ~~TOTAL_STATE
AllocatedTime::AllocatedTime(CreateData xD, Seconds minTimeD, Seconds maxTimeD)
{
  TRACEM("AllocatedTime::AllocatedTime(CreateData xD, Seconds minTimeD, Seconds maxTimeD)");

   PARM_CHECK((minTimeD < maxTimeD));

   NOREF1(xD);                  // Parameter only used to create another creation method

   duration = random(minTimeD, maxTimeD);

   constraintName = ALLOCATEDTIME;
   in_Error = FALSE;

  RETURN_VOID;
}

//***********************************************************
// AllocatedTime
//
// Description:
// Constructor for the AllocatedTime Class.
// Uses: AllocatedTime::setConstraintName().
// FKEYS ~~CREATION ~~VIRGIN ~~TOTAL_STATE
AllocatedTime::~AllocatedTime(void)
{
  TRACEM("AllocatedTime::~AllocatedTime(void)");

  RETURN_VOID;
}

//***********************************************************
// getDuration
//
// Description:
// Retruns the value of the duration attribute.
// Uses: uses no other methods
// FKEYS ~~ACCESS ~~VIRGIN
Seconds
AllocatedTime::getDuration(void) const
{
  TRACEM("AllocatedTime::getDuration(void) const");

   RETURN(duration);
}

//***********************************************************
// showMe
//
// Description:
// Displays the value of the duration attribute
// Uses: uses no other methods
// FKEYS ~~DISPLAY ~~OSTREAM ~~VIRTUAL ~~VIRGIN
void
AllocatedTime::showMe(ostream & str) const
{
  TRACEM("AllocatedTime::showMe(ostream & str) const");

   PARM_CHECK(inError(str));

   str << STR(1, "\t\tAllocatedTime") << endl;
   str << STR(1, "\t\tduration\t\t") << duration << endl;
   str << endl;

  RETURN_VOID;
}

//***********************************************************
// selfCheck
//
// Description:
//
// Uses: no other methods.
// FKEYS ~~VALUE_CHECK ~~OSTREAM ~~RWCSTRING ~~DISPLAY ~~VIRTUAL ~~VIRGIN ~~TOTAL_STATE
BOOL
AllocatedTime::selfCheck(ostream & str, RWCString & id) const
{
  TRACEM("AllocatedTime::selfCheck(ostream & str, RWCString & id) const");

   PARM_CHECK(inError(str));

   int             retStat = 0;

   if (duration < 0)
   {
      str << id << STR(1, ", error, AllocatedTime::duration of observation is < 0 sec") << endl;
   } else
   {
      retStat = 1;
   }
   RETURN(retStat);
}

//***********************************************************
// selfCheck
//
// Description:
//
// Uses: no other methods.
// FKEYS ~~VALUE_CHECK ~~VIRTUAL ~~VIRGIN ~~TOTAL_STATE
BOOL
AllocatedTime::selfCheck(void) const
{
  TRACEM("AllocatedTime::selfCheck(void) const");

   int             retStat = 0;

   if (duration < 0)
   {
   } else
   {
      retStat = 1;
   }
   RETURN(retStat);
}

//***********************************************************
//
//
// Description:
// Uses:
// FKEYS ~~ACCESS ~~VIRGIN ~~TOTAL_STATE ~~COUT ~~UNFINISHED
BOOL
AllocatedTime::edit(void)
{
  TRACEM("AllocatedTime::edit(void)");

   Seconds         time1 = 0U;

   do
   {
      cout << STR(1, "AllocatedTime is : ") << duration << STR(1, "Seconds") << endl << endl;

      cout << STR(1, "\tEdit allocated time constraint:") << endl;

      if (setOrNot(STR(1, " allocated time")))
      {
         long            timeLength = 0L;

         editTimeLength(timeLength);
         duration = timeLength;
      } else
      {
      }

   } while (!yOrN(STR(1, "Finished editing allocated time constraint")));

   return (TRUE);
}

//***********************************************************
//
//
// Description:
// Uses:
// FKEYS ~~ACCESS ~~VIRGIN ~~TOTAL_STATE ~~RWFILE ~~WRITE ~~UNFINISHED
BOOL
AllocatedTime::fuzzySelfCheck(RWCString & strD) const
{
  TRACEM("AllocatedTime::fuzzySelfCheck(RWCString & strD) const");

   fuzzyTimeIntervalCheck((double) duration, strD);

   RETURN(TRUE);
}


//***********************************************************
//
//
// Description:
// Uses:
// FKEYS ~~ACCESS ~~VIRGIN ~~TOTAL_STATE ~~UNFINISHED
int
AllocatedTime::getNumNodeStates(void) const
{
  TRACEM("AllocatedTime::getNumNodeStates(void) const");
//  duration = 0
   // duration = ULONG_MAX

   RETURN(2);
}

//***********************************************************
// GOFname  (const char* name)
//
// Description:
// Compare a string to the key name used in the GOF file for
// AllocatedTime
// Uses:
// FKEYS  ~~VIRGIN ~~TOTAL_STATE  ~~UNFINISHED ~~STATIC_FUNC
BOOL
AllocatedTime::GOFname(const char *name)
{
  TRACE("AllocatedTime::GOFname(const char *name)");

   PARM_CHECK((name == (char *) NULL));

   BOOL            ret = (strcmp(name, STR(1, "XXX")) == 0);

   RETURN(ret);
}

//***********************************************************
// differences  (SchedulingConstraint* otherD, RWCString& diffD)
//
// Description:
// Write differences between to AllocatedTime to a string.  If there
// is no difference in there values return FALSE.
// Uses:
// FKEYS ~~VIRGIN ~~TOTAL_STATE  ~~UNFINISHED ~~VIRTUAL ~~RWSTRING
BOOL
AllocatedTime::differences(SchedulingConstraint & otherD, RWCString & diffD) const
{
  TRACEM("AllocatedTime::differences(SchedulingConstraint & otherD, RWCString & diffD) const");

   PARM_CHECK(inError(otherD));

   BOOL            ret;
   AllocatedTime  *other;

   if ((other = castTo_AllocatedTime(&otherD)) == CAST(AllocatedTime *, NULL))
   {   RETURN(FALSE);    }

   if (other->duration != duration)
   {
      char            buffer[80];

      sprintf(buffer, "%L", duration);

      diffD.APPEND(STR(1, "duration   "));
      diffD.APPEND(buffer);
      diffD.APPEND(STR(1, "            "));

      sprintf(buffer, "%L", other->duration);
      diffD.APPEND(buffer);
      diffD.APPEND("\n");

      ret = TRUE;
   }
   RETURN(ret);
}

//***********************************************************
// compare  (AllocatedTime& xD)
//
// Description:
// Compare the time line relationship between two AllocatedTime(s).
// Uses:
// FKEYS ~~VIRGIN ~~TOTAL_STATE  ~~UNFINISHED
LineRel
AllocatedTime::compare(AllocatedTime & xD) const
{
  TRACEM("AllocatedTime::compare(AllocatedTime & xD) const");

   PARM_CHECK(inError(xD));

   LineRel         ret;

   if (xD.duration == duration)
   {
      ret = EQUAL;
   } else if (xD.duration > duration)
   {
      ret = LESSTHAN;
   } else if (xD.duration < duration)
   {
      ret = GREATERTHAN;
   }
   RETURN(ret);
}

//***********************************************************
// combine  (AllocatedTime& xD)
//
// Description:
// If possible add a time range of another AllocatedTime to this
// one.  If not possible return FALSE.
// Uses:
// FKEYS ~~VIRGIN ~~TOTAL_STATE  ~~UNFINISHED
BOOL
AllocatedTime::combine(AllocatedTime & xD)
{
  TRACEM("AllocatedTime::combine(AllocatedTime & xD)");

   PARM_CHECK(inError(xD));

   duration += xD.duration;

   RETURN(TRUE);
}

//***********************************************************
// title  (RWCString& strD)
//
// Description:
// Write a title to a string for columns containing AllocatedTime
// attributes.
// Uses:
// FKEYS ~~VIRGIN ~~TOTAL_STATE  ~~UNFINISHED ~~RWSTRING
void
AllocatedTime::title(RWCString & strD)
{
  TRACE("AllocatedTime::title(RWCString & strD)");

   strD.APPEND(STR(1, "\t\tduration in seconds"));

  RETURN_VOID;
}

//***********************************************************
// fields  (RWCString& strD)
//
// Description:
// Write to a string the attributes of the AllocatedTime.
// Uses:
// FKEYS ~~VIRGIN ~~TOTAL_STATE  ~~UNFINISHED ~~RWSTRING
void
AllocatedTime::fields(RWCString & strD) const
{
  TRACEM("AllocatedTime::fields(RWCString & strD) const");

   char            buffer[80];

   sprintf(buffer, "%L", duration);

   strD.APPEND(STR(1, "\t\t\t\t\t"));
   strD.APPEND(buffer);

  RETURN_VOID;
}

//***********************************************************
// restoreGuts
//
// Description:
// Retrieves the object from disk.
// Uses: AllocatedTime::restoreGuts(), RWFile::Read().
// FKEYS ~~PERSISTANCE ~~RWFILE ~~READ ~~VIRTUAL ~~VIRGIN ~~TOTAL_STATE
void
AllocatedTime::restoreGuts(RWFile & fileD)
{
  TRACEM("AllocatedTime::restoreGuts(RWFile & fileD)");

   PARM_CHECK(inError(fileD));

   SchedulingConstraint::restoreGuts(fileD);
   fileD.Read(duration);

  RETURN_VOID;
}

//***********************************************************
// restoreGuts
//
// Description:
// Retrieves the object from disk.
// Uses: AllocatedTime::restoreGuts(), RWvistream::>>.
// FKEYS ~~PERSISTANCE ~~RWVISTREAM ~~READ ~~VIRTUAL ~~VIRGIN ~~TOTAL_STATE
void
AllocatedTime::restoreGuts(RWvistream & strm)
{
  TRACEM("AllocatedTime::restoreGuts(RWvistream & strm)");

   PARM_CHECK(inError(strm));

   SchedulingConstraint::restoreGuts(strm);
   strm >> duration;

  RETURN_VOID;
}

//***********************************************************
// saveGuts
//
// Description:
// Save the object to a disk file.
// Uses: AllocatedTime::restoreGuts(), RWFile::Write().
// FKEYS ~~PERSISTANCE ~~RWFILE ~~WRITE ~~VIRTUAL ~~VIRGIN ~~TOTAL_STATE
void
AllocatedTime::saveGuts(RWFile & fileD) const
{
  TRACEM("AllocatedTime::saveGuts(RWFile & fileD) const");

   PARM_CHECK(inError(fileD));

   SchedulingConstraint::saveGuts(fileD);
   fileD.Write(duration);

  RETURN_VOID;
}

//***********************************************************
// saveGuts
//
// Description:
// Saves the object to a disk file.
// Uses: AllocatedTime::restoreGuts(), RWvostream::<<.
// FKEYS ~~PERSISTANCE ~~RWVOSTREAM ~~WRITE ~~VIRTUAL ~~VIRGIN ~~TOTAL_STATE
void
AllocatedTime::saveGuts(RWvostream & strm) const
{
  TRACEM("AllocatedTime::saveGuts(RWvostream & strm) const");

   PARM_CHECK(inError(strm));

   SchedulingConstraint::saveGuts(strm);
   strm << duration;

  RETURN_VOID;
}

//***********************************************************
// binaryStoreSize  ()
//
// Description:
// Calculates the size of space on disk needed to store this object.
// Uses:
// FKEYS  ~~VIRGIN ~~TOTAL_STATE  ~~UNFINISHED
#if RWTOOLS < 0x600 && ! defined(RWspace)
#define RWspace unsigned
#endif
RWspace 
AllocatedTime::binaryStoreSize() const
{
  TRACEM("AllocatedTime::saveGuts(RWvostream & strm) const");

   RWspace          ret = (RWspace)sizeof(duration); //CAST

   ret += (RWspace)SchedulingConstraint::binaryStoreSize(); // CAST

   RETURN(ret);
}


//***********************************************************
//
//
// Description:
// Uses:
// FKEYS ~~ACCESS ~~VIRGIN ~~TOTAL_STATE ~~RWFILE ~~READ ~~UNFINISHED
BOOL
AllocatedTime::readFromGOF(istream & fileD)
{
  TRACEM("AllocatedTime::readFromGOF(istream & fileD)");

   PARM_CHECK(inError(fileD));

   RWCString       line;

   int             totalErrors = 0;
   BOOL            errorFlag = FALSE;

   readLine(fileD, line);
   duration = parse_GOFfloat(line, errorFlag);
   if (errorFlag)
   {
      duration = 0.0;
      *report << STR(1, ",\tallocTelem bad data ") << endl;
      totalErrors++;
   }
   RETURN(totalErrors);
}

//***********************************************************
//
//
// Description:
// Uses:
// FKEYS ~~ACCESS ~~VIRGIN ~~TOTAL_STATE ~~RWFILE ~~WRITE ~~UNFINISHED
BOOL
AllocatedTime::writeToGOF(ostream & fileD) const
{
  TRACEM("AllocatedTime::writeToGOF(ostream & fileD) const");

   PARM_CHECK(inError(fileD));

   fileD << duration << endl;

   RETURN(TRUE);
}


//***********************************************************
// castTo_AllocatedTime  (SchedulingConstraint* xD)
//
// Description:
// Cast a SchedulingConstraint* to a AllocatedTime* if possible if not return
// (AllocatedTime*)NULL.
// Uses:
// FKEYS  ~~VIRGIN  ~~UNFINISHED ~~CAST
AllocatedTime  *
castTo_AllocatedTime(SchedulingConstraint * xD)
{
   TRACE("castTo_AllocatedTime(SchedulingConstraint* xD)");

   if (xD->constraintName == ALLOCATEDTIME)
   {   RETURN((AllocatedTime *) xD);    }
   RETURN((AllocatedTime *) NULL);
}
//***********************************************************
// 
//
// Description:
// Uses:
// FKEYS  
BOOL                  
operator==(AllocatedTime& rhsD, AllocatedTime& lhsD)
{
   return( (rhsD.duration==lhsD.duration) );
}

//***********************************************************
// 
//
// Description:
// Uses:
// FKEYS  
BOOL
AllocatedTime::isEqual(const RWCollectable* x) const
{
   AllocatedTime* x1 = castTo_AllocatedTime((SchedulingConstraint*)this);
   AllocatedTime* x2 = castTo_AllocatedTime((SchedulingConstraint*)x);
   if ( (x1==(AllocatedTime*)NULL) || (x2==(AllocatedTime*)NULL) ) return(FALSE);

   return( *x1==*x2 );
}

 
// COMPILE_TEST:
// CC -DALLOCTIME_TEST -o AllocTime.Test AllocTime.C -L$SOCHOME/lib/$ARCH -lObsSced -lrwtool

// COMPILE_QUICK_TEST:
// CC -DALLOCTIME_TEST -o AllocTime.Test AllocTime.C -L$SOCHOME/lib/$ARCH -lObsSced -lrwtool

// COMPILE_PURE_TEST:
// purify -inuse-at-exit -mail-to-user=ejohnson@@xema.stx.com -logfile=AllocTime.C.purify_log CC -DPURE -DALLOCTIME_TEST -o AllocTime.Pure.Test AllocTime.C -L$SOCHOME/lib/SunOS4 -I/socsoft/local/pure/purify-2.1-sunos4 -lObsDes -lrwtool

#if defined ALLOCTIME_TEST || MAKE_MP_MAINS

#include <fstream.h>

void
main(void)
{
   RWCString       str1;


   AllocatedTime   a;
   AllocatedTime   b(DUMDUM, 1000);
   AllocatedTime   c(1000);
   AllocatedTime   d(5, 10);

   cout << "c.getDuration()" << c.getDuration() << endl;

#define OBJECT   AllocatedTime
#define str(x)   #x

   cout << str(OBJECT) << "::GOFname( XXX ) = " << OBJECT::GOFname("XXX") << endl;
   cout << str(OBJECT) << "::GOFname( YYY ) = " << OBJECT::GOFname("YYY") << endl;

   OBJECT::title(str1);
   cout << str(OBJECT) << "::title(str1) = " << endl << str1 << endl;

   // == Talyorize end

   SchedulingConstraint::report = &cout;

   c.showMe(cout);
   c.selfCheck(cout, str1);

   // cout << "c.edit() = " << endl << c.edit() << endl;

   c.fuzzySelfCheck(str1);
   cout << "c.fuzzySelfCheck(str1): str1 = " << str1 << endl;

   cout << "c.getNumNodeStates() = " << c.getNumNodeStates() << endl;

   cout << "c.differences(&a, str1) = " << c.differences(a, str1) << endl;
   cout << str1 << endl;

   cout << "c.compare(a)" << c.compare(a) << endl;

   cout <<  "b.combine(a)" << b.combine(a) << endl;

   c.fields(str1);

   cout << "c.binaryStoreSize() = " << c.binaryStoreSize() << endl;


   {
      {
         RWFile          file(__FILE__ "RWFile");

         c.saveGuts(file);
      }
      {
         RWFile          file(__FILE__ "RWFile");

         c.restoreGuts(file);
      }

      {
         {
            ofstream         file2(__FILE__ "GOFout");

            c.writeToGOF(file2);
         }
         { 
            ifstream         file2(__FILE__ "GOFout");

            b.readFromGOF(file2);
         }
 
         cout << "b==a -> " << ((b==a)?"TRUE":"FALSE") << endl;  // ver2
         cout << "b==c -> " << ((b==c)?"TRUE":"FALSE") << endl;  // ver2
         
      }
   }
   exit(0);
 }

#endif
@


2.16
log
@*** empty log message ***
@
text
@d2 1
a2 1
// RCS: $Id: AllocTime.C,v 2.15 1994/08/12 22:14:36 ejohnson Exp ejohnson $
d12 1
a12 1
static const char rcsid[] = "$Id: AllocTime.C,v 2.15 1994/08/12 22:14:36 ejohnson Exp ejohnson $";
d650 1
a650 1
// CC -DALLOCTIME_TEST -o AllocTime.Test AllocTime.C -L$SOCHOME/lib/SunOS4 -lObsDes -lrwtool
d653 1
a653 1
// CC -DALLOCTIME_TEST -o AllocTime.Test AllocTime.C -L$SOCHOME/lib/SunOS4 -lObsDes -lrwtool
d691 1
a691 1
   cout << "c.edit() = " << endl << c.edit() << endl;
@


2.15
log
@,
,
@
text
@d2 1
a2 1
// RCS: $Id: AllocTime.C,v 2.14 1994/08/12 19:21:58 ejohnson Exp ejohnson $
d12 1
a12 1
static const char rcsid[] = "$Id: AllocTime.C,v 2.14 1994/08/12 19:21:58 ejohnson Exp ejohnson $";
d547 1
a547 1
   size_t          ret = sizeof(duration);
d549 1
a549 1
   ret += SchedulingConstraint::binaryStoreSize();
a742 1
#endif
@


2.14
log
@..
@
text
@a0 1
// Program Name: AllocatedTime.C
d2 1
a2 1
// RCS: $Id: AllocTime.C,v 2.13 1994/08/02 13:09:20 ejohnson Exp $
d12 1
a12 1
static const char rcsid[] = "$Id: AllocTime.C,v 2.13 1994/08/02 13:09:20 ejohnson Exp $";
d743 1
@


2.13
log
@*** empty log message ***
@
text
@d3 1
a3 1
// RCS: $Id: AllocTime.C,v 2.12 1994/05/03 14:31:20 ejohnson Exp $
d13 1
a13 1
static const char rcsid[] = "$Id: AllocTime.C,v 2.12 1994/05/03 14:31:20 ejohnson Exp $";
d64 1
a64 1
   TRACE("AllocatedTime::AllocatedTime(void)");
d81 1
a81 1
   TRACE("AllocatedTime::AllocatedTime(Seconds seconds)");
d98 1
a98 1
   TRACE("AllocatedTime::AllocatedTime(int days, long hours, long minutes, long seconds)");
d124 1
a124 1
   TRACE("AllocatedTime::AllocatedTime(CreateData xD, Seconds minTimeD, Seconds maxTimeD)");
d147 1
a147 1
   TRACE("AllocatedTime::~AllocatedTime(void)");
d162 1
a162 1
   TRACE("AllocatedTime::getDuration(void) const");
d177 1
a177 1
   TRACE("AllocatedTime::showMe(ostream & str) const");
d198 1
a198 1
   TRACE("AllocatedTime::selfCheck(ostream & str, RWCString & id) const");
d224 1
a224 1
   TRACE("AllocatedTime::selfCheck(void) const");
d246 1
a246 1
   TRACE("AllocatedTime::edit(void)");
d280 1
a280 1
   TRACE("AllocatedTime::fuzzySelfCheck(RWCString & strD) const");
d297 1
a297 1
   TRACE("AllocatedTime::getNumNodeStates(void) const");
d315 1
a315 1
   TRACE("AllocatedTime::GOFname(const char *name)");
d335 1
a335 1
   TRACE("AllocatedTime::differences(SchedulingConstraint & otherD, RWCString & diffD) const");
d374 1
a374 1
   TRACE("AllocatedTime::compare(AllocatedTime & xD) const");
d404 1
a404 1
   TRACE("AllocatedTime::combine(AllocatedTime & xD)");
d424 1
a424 1
   TRACE("AllocatedTime::title(RWCString & strD)");
d441 1
a441 1
   TRACE("AllocatedTime::fields(RWCString & strD) const");
d463 1
a463 1
   TRACE("AllocatedTime::restoreGuts(RWFile & fileD)");
d483 1
a483 1
   TRACE("AllocatedTime::restoreGuts(RWvistream & strm)");
d503 1
a503 1
   TRACE("AllocatedTime::saveGuts(RWFile & fileD) const");
d523 1
a523 1
   TRACE("AllocatedTime::saveGuts(RWvostream & strm) const");
d546 1
a546 1
   TRACE("AllocatedTime::saveGuts(RWvostream & strm) const");
d565 1
a565 1
   TRACE("AllocatedTime::readFromGOF(istream & fileD)");
d594 1
a594 1
   TRACE("AllocatedTime::writeToGOF(ostream & fileD) const");
@


2.12
log
@*** empty log message ***
@
text
@d3 1
a3 1
// RCS: $Id: AllocTime.C,v 2.11 1994/04/15 14:04:54 ejohnson Exp $
d13 1
a13 1
static const char rcsid[] = "$Id: AllocTime.C,v 2.11 1994/04/15 14:04:54 ejohnson Exp $";
d28 1
d540 4
a543 1
unsigned
@


2.11
log
@""
@
text
@d3 1
a3 1
// RCS: $Id: AllocTime.C,v 2.10 1994/04/13 01:00:43 ejohnson Exp ejohnson $
d13 1
a13 1
static const char rcsid[] = "$Id: AllocTime.C,v 2.10 1994/04/13 01:00:43 ejohnson Exp ejohnson $";
d723 1
a723 1
            a.writeToGOF(file2);
@


2.10
log
@""
@
text
@d3 1
a3 1
// RCS: $Id: AllocTime.C,v 2.9 1994/04/10 19:12:09 ejohnson Exp ejohnson $
d13 1
a13 1
static const char rcsid[] = "$Id: AllocTime.C,v 2.9 1994/04/10 19:12:09 ejohnson Exp ejohnson $";
d655 1
a655 1
#ifdef ALLOCTIME_TEST
@


2.9
log
@""
@
text
@d3 1
a3 1
// RCS: $Id: AllocTime.C,v 2.8 1994/03/28 13:50:34 ejohnson Exp ejohnson $
d13 1
a13 1
static const char rcsid[] = "$Id: AllocTime.C,v 2.8 1994/03/28 13:50:34 ejohnson Exp ejohnson $";
d647 1
a647 1
// CC -DALLOCTIME_TEST AllocTime.C -L$SOCHOME/lib/SunOS4 -lObsDes -lrwtool
d650 1
a650 1
// CC -DALLOCTIME_TEST AllocTime.C -L$SOCHOME/lib/SunOS4 -lObsDes -lrwtool
d653 1
a653 1
// purify -inuse-at-exit -mail-to-user=ejohnson@@xema.stx.com -logfile=AllocTime.C.purify_log CC -DPURE -DALLOCTIME_TEST AllocTime.C -L$SOCHOME/lib/SunOS4 -I/socsoft/local/pure/purify-2.1-sunos4 -lObsDes -lrwtool
a654 4

// COMPILE_TEST: 
// CC -DALLOCTIME_TEST AllocTime.C ScedConst.C random.C trace.C GOF_fuzzy.C GOF_io.C LineSeg.C stringDB.C stringIO.C -lrwtool

d720 2
a721 2
         ifstream         file1(__FILE__ "GOFin");
         ofstream         file2(__FILE__ "GOFout");
d723 11
a733 2
         c.readFromGOF(file1);
         c.writeToGOF(file2);
d736 1
a736 1
   exit;
@


2.8
log
@"."
@
text
@d3 1
a3 1
// RCS: $Id: AllocTime.C,v 2.7 1994/03/14 21:08:31 ejohnson Exp ejohnson $
d13 1
a13 1
static const char rcsid[] = "$Id: AllocTime.C,v 2.7 1994/03/14 21:08:31 ejohnson Exp ejohnson $";
d66 1
d68 1
a68 1
   return;
d80 1
a80 1
   TRACE("AllocatedTime::AllocatedTime(long seconds)");
d83 1
d85 1
a85 1
   return;
d99 7
a105 7
   PARM_CHECK((days > 0));
   PARM_CHECK((hours > 0));
   PARM_CHECK((hours < 24));
   PARM_CHECK((minutes > 0));
   PARM_CHECK((minutes < 60));
   PARM_CHECK((seconds > 0));
   PARM_CHECK((seconds < 60));
d108 1
d111 1
a111 1
   return;
d132 1
d134 1
a134 1
   return;
d148 1
a148 1
   return;
d161 1
a161 1
   TRACE("AllocatedTime::getDuration(void)");
d176 1
a176 1
   TRACE("AllocatedTime::showMe(ostream& str) const");
d184 1
a184 1
   return;
d197 1
a197 1
   TRACE("AllocatedTime::selfCheck(ostream& str, RWCString& id) const");
d223 1
a223 1
   TRACE("AllocatedTime::selfCheck(void)");
d245 1
a245 1
   TRACE("AllocatedTime::edit(void) const");
d279 1
a279 1
   TRACE("AllocatedTime::fuzzySelfCheck(RWCString& strD) const");
d314 1
a314 1
   TRACE("AbsoluteTimeRange::GOFname(const char* name)");
d334 1
a334 1
   TRACE("AllocatedTime::differences(SchedulingConstraint* otherD, RWCString& diffD) const");
d373 1
a373 1
   TRACE("AllocatedTime::compare(AllocatedTime& xD) const");
d403 1
a403 1
   TRACE("AllocatedTime::combine(AllocatedTime& xD)");
d423 1
a423 1
   TRACE("AllocatedTime::title(RWCString& strD)");
d427 1
a427 1
   return;
d440 1
a440 1
   TRACE("AllocatedTime::fields(RWCString& strD) const");
d449 1
a449 1
   return;
d462 1
a462 1
   TRACE("AllocatedTime::restoreGuts(RWFile& fileD");
d469 1
a469 1
   return;
d482 1
a482 1
   TRACE("AllocatedTime::restoreGuts(RWvistream& strm)");
d489 1
a489 1
   return;
d502 1
a502 1
   TRACE("AllocatedTime::saveGuts(RWFile& fileD)");
d509 1
a509 1
   return;
d522 1
a522 1
   TRACE("AllocatedTime::saveGuts(RWvostream& strm)");
d529 1
a529 1
   return;
d542 1
a542 1
   TRACE("AllocatedTime::binaryStoreSize() const");
d561 1
a561 1
   TRACE("AllocatedTime::readFromGOF(istream& fileD)");
d590 1
a590 1
   TRACE("AllocatedTime::writeToGOF(ostream& fileD) const");
d645 3
d649 7
d731 1
@


2.7
log
@*** empty log message ***
@
text
@d3 1
a3 1
// RCS: $Id: AllocTime.C,v 2.6 1994/03/08 16:46:53 ejohnson Exp ejohnson $
d13 1
a13 1
static const char rcsid[] = "$Id: AllocTime.C,v 2.6 1994/03/08 16:46:53 ejohnson Exp ejohnson $";
d613 28
@


2.6
log
@Build3 start
@
text
@d3 1
a3 1
// RCS: $Id: AllocTime.C,v 2.5 1993/12/07 15:12:51 ejohnson Release ejohnson $
d13 1
a13 1
static const char rcsid[] = "$Id: AllocTime.C,v 2.5 1993/12/07 15:12:51 ejohnson Release ejohnson $";
d338 1
a338 1
      RETURN(FALSE);
d610 1
a610 1
      RETURN((AllocatedTime *) xD);
d614 2
@


2.5
log
@*** empty log message ***
@
text
@d3 2
a4 2
//RCS: $Id: AllocTime.C,v 2.4 1993/12/01 06:08:47 ejohnson Release ejohnson $
// Programmer:  Rick Leone, NASA/GSFC Code 664
d8 1
a8 1
// of SchedulingConstraint class and is an attribute of the ObservationDe-
d13 1
a13 1
static const char rcsid[]="$Id$";
d17 6
d27 7
d37 1
a37 1
RWDEFINE_COLLECTABLE(AllocatedTime,AllocatedTimeID)
d51 2
d59 2
a60 2
// Uses: SchedulingConstraint::setConstraintName().
//
d63 3
a65 1
    Trace    trace = "AllocatedTime::AllocatedTime(void)";
d67 1
a67 1
  setConstraintName(ALLOCATEDTIME);
d69 1
d75 13
a87 1
// Uses: SchedulingConstraint::setConstraintName().
d89 5
a93 1
AllocatedTime::AllocatedTime(long seconds):duration(seconds)
d95 11
a105 1
    Trace    trace = "AllocatedTime::AllocatedTime(long seconds)";
d107 2
a108 1
  setConstraintName(ALLOCATEDTIME);
d110 1
d116 19
a134 1
// Uses: SchedulingConstraint::setConstraintName().
d136 5
a140 1
AllocatedTime::AllocatedTime(int days, long hours, long minutes, long seconds):duration(( days*86400+hours*3600+minutes*60+seconds) )
d142 1
a142 1
    Trace    trace = "AllocatedTime::AllocatedTime(int days, long hours, long minutes, long seconds)";
d144 1
a144 1
  setConstraintName(ALLOCATEDTIME);
d146 1
d153 2
a154 2
//
long
d157 1
a157 1
    Trace    trace = "AllocatedTime::getDuration(void)";
d159 1
a159 1
  return duration;
d161 1
d168 1
a168 1
//
d170 1
a170 1
AllocatedTime::showMe(ostream& str) const
d172 10
a181 1
    Trace    trace = "AllocatedTime::showMe(ostream& str)";
d183 24
a206 3
    str << "\t\tAllocatedTime" << endl;
    str << "\t\tduration\t\t" <<  duration << endl;
    str << endl;
d208 1
d215 69
d285 4
d290 128
a417 1
AllocatedTime::selfCheck(ostream& str, RWCString& id) const
d419 1
a419 1
    Trace    trace = "AllocatedTime::selfCheck(ostream& str, RWCString& id)";
d421 3
a423 8
  int retStat = 0;
  if(  duration < 0 )
  {
    str << id << ", error, AllocatedTime::duration of observation is < 0 sec" << endl;
  } else {
    retStat = 1;
  }
  return retStat;
d425 1
d427 1
a427 1
// saveGuts
d430 20
a449 2
// Save the object to a disk file.
// Uses: SchedulingConstraint::restoreGuts(), RWFile::Write().
d451 4
d456 1
a456 1
AllocatedTime::saveGuts(RWFile& fileD) const
d458 1
a458 1
    Trace    trace = "AllocatedTime::saveGuts(RWFile& fileD)";
d460 6
a465 2
  SchedulingConstraint::saveGuts(fileD);
  fileD.Write(duration);
d467 1
d473 17
a489 1
// Uses: SchedulingConstraint::restoreGuts(), RWFile::Read().
d491 4
d496 1
a496 1
AllocatedTime::restoreGuts(RWFile& fileD)
d498 1
a498 1
    Trace    trace = "AllocatedTime::restoreGuts(RWFile& fileD";
d500 6
a505 2
  SchedulingConstraint::restoreGuts(fileD);
  fileD.Read(duration);
d507 1
d513 2
a514 2
// Uses: SchedulingConstraint::restoreGuts(), RWvostream::<<.
//
d516 1
a516 1
AllocatedTime::saveGuts(RWvostream& strm) const
d518 6
a523 1
    Trace    trace = "AllocatedTime::saveGuts(RWvostream& strm)";
d525 1
a525 2
  SchedulingConstraint::saveGuts(strm);
  strm << duration;
d527 1
d529 1
a529 1
// restoreGuts
d532 18
a549 2
// Retrieves the object from disk.
// Uses: SchedulingConstraint::restoreGuts(), RWvistream::>>.
d551 5
a555 2
void
AllocatedTime::restoreGuts(RWvistream& strm)
d557 8
a564 1
    Trace    trace = "AllocatedTime::restoreGuts(RWvistream& strm)";
d566 9
a574 2
  SchedulingConstraint::restoreGuts(strm);
  strm >> duration;
d576 1
d578 1
a578 1
// operator=
d581 17
a597 2
// Memberwise assignment operator.
// Uses: SchedulingConstraint::restoreGuts(), RWvistream::>>.
d599 20
d620 1
a620 1
AllocatedTime::operator=( const AllocatedTime& allt)
d622 25
a646 1
    Trace    trace = "AllocatedTime::operator=( const AllocatedTime& allt)";
d648 34
a681 2
   duration = allt.getDuration();
}
d683 5
d689 1
@


2.4
log
@"Final changes for Build2"
@
text
@d3 1
a3 1
//RCS: $Id: AllocTime.C,v 2.3 1993/11/22 22:54:42 ejohnson Exp ejohnson $
d13 3
@


2.3
log
@""
@
text
@d3 1
a3 1
//RCS: $Id: AllocTime.C,v 2.2 1993/11/17 22:05:56 ejohnson Exp ejohnson $
d16 1
d45 2
d58 2
d71 2
d85 2
d99 2
d115 2
d136 2
d151 2
d166 2
d181 2
d196 2
a200 1

@


2.2
log
@*** empty log message ***
@
text
@a1 1
// LastEdit: Wed Dec 29, 1992
d3 2
a4 2
//RCS: $Id: AllocTime.C,v 2.1 1993/10/12 19:18:05 leone Exp ejohnson $
// Programmer:  Rick Leone, NASA/GSFC Code 664 
d6 2
a7 2
// Description: The file AllocTime.H is the class description for the class 
// AllocatedTime of the XTE-SOC class library. This class is a subclass 
d10 1
a10 1
// time that the NASAHQ has awarded the observation. 
a11 2
//  Feature test switches
//#define _POSIX_SOURCE 1
d14 1
d16 1
d18 1
d20 1
a20 1
RWDEFINE_COLLECTABLE(AllocatedTime,603)
a33 1
// Main
d39 1
a39 1
// Default constructor for the AllocatedTime Class. 
d43 1
a43 1
  {
d45 1
a45 1
  }
d50 1
a50 1
// Default constructor for the AllocatedTime Class. 
d54 1
a54 1
  {
d56 1
a56 1
  }
d61 1
a61 1
// Constructor for the AllocatedTime Class. 
d65 1
a65 1
  {
d67 1
a67 11
  }
//***********************************************************
// ~AllocatedTime
//
// Description:
// Destructor for the AllocatedTime Class. 
// Uses: no other methods.
//
AllocatedTime::~AllocatedTime(void)
  {
  }
d72 1
a72 1
// Retruns the value of the duration attribute. 
d75 3
a77 2
long  AllocatedTime::getDuration(void) const
  {
d79 1
a79 1
  }
d84 1
a84 1
// Displays the value of the duration attribute 
d87 7
a93 6
void AllocatedTime::showMe(ostream& str) const
  {
    str<<"\t\tAllocatedTime"<<endl;
    str<<"\t\tduration\t\t"<< duration<<endl;
    str<<endl;
  }
d98 1
a98 1
// 
d101 3
a103 1
int AllocatedTime::selfCheck(ostream& str, RWCString& id) const {
d105 5
a109 2
  if(  duration < 0 ) {
    str<<id<<", error, AllocatedTime::duration of observation is < 0 sec"<<endl;
a110 2
  else
    retStat = 1;
d117 1
a117 1
// Save the object to a disk file. 
d120 6
a125 5
void AllocatedTime::saveGuts(RWFile& f) const
  {
  SchedulingConstraint::saveGuts(f);
  f.Write(duration);
  }
d130 1
a130 1
// Retrieves the object from disk. 
d133 6
a138 5
void AllocatedTime::restoreGuts(RWFile& f)
  {
  SchedulingConstraint::restoreGuts(f);
  f.Read(duration);
  }
d143 1
a143 1
// Saves the object to a disk file. 
d146 3
a148 2
void AllocatedTime::saveGuts(RWvostream& strm) const
  {
d151 1
a151 1
  }
d156 1
a156 1
// Retrieves the object from disk. 
d159 3
a161 2
void AllocatedTime::restoreGuts(RWvistream& strm)
  {
d164 1
a164 1
  }
d166 1
a166 1
// operator= 
d169 1
a169 1
// Memberwise assignment operator. 
d172 3
a174 2
void AllocatedTime::operator=( const AllocatedTime& allt)
  {
d176 1
a176 3
  }


@


2.1
log
@""
@
text
@d4 1
a4 1
//RCS: $Id: AllocTime.C,v 2.0 1993/08/20 21:52:10 leone Exp leone $
d100 1
a100 1
    str<<"\t\tduration\t"<< duration<<endl;
@


2.0
log
@NewBuild
@
text
@d4 1
a4 1
//RCS: $Id: AllocTime.C,v 1.6 1993/07/06 20:39:14 leone Exp leone $
d91 29
@


1.6
log
@added the assignment operator
@
text
@d4 1
a4 1
//RCS: $Id: AllocTime.C,v 1.5 1993/06/28 22:45:58 leone Exp leone $
@


1.5
log
@*** empty log message ***
@
text
@d4 1
a4 1
//RCS: $Id: AllocTime.C,v 1.4 1993/05/28 20:45:55 leone Exp leone $
d86 1
a86 1
long  AllocatedTime::getDuration(void)
d138 11
@


1.4
log
@"updated header files to make compatable with genman"
@
text
@d4 1
a4 1
//RCS: $Id: AllocTime.C,v 1.3 1993/05/27 00:53:39 leone Exp leone $
d18 1
a18 1
#include "AllocTime.H"
@


1.3
log
@*** empty log message ***
@
text
@d4 1
a4 1
// RCS Stamp: $Id
d36 50
a85 48
/************************************************************
 *     Method Name: AllocatedTime
 *
 *     Purpose: Default constructor for the AllocatedTime Class. 
 *
 *     Uses: SchedulingConstraint::setConstraintName().
 *
 *     Input: void
 *
 *     Return: no return value
 **************************************************************/
AllocatedTime::AllocatedTime(void){setConstraintName(ALLOCATEDTIME);}
AllocatedTime::AllocatedTime(long seconds):duration(seconds){setConstraintName(ALLOCATEDTIME);}
/************************************************************
 *     Method Name: AllocatedTime
 *
 *     Purpose: constructor for the AllocatedTime Class. 
 *
 *     Uses: SchedulingConstraint::setConstraintName().
 *
 *     Input: int, long, long, long.
 *
 *     Return: no return value
 **************************************************************/
AllocatedTime::AllocatedTime(int days, long hours, long minutes, long seconds):duration(( days*86400+hours*3600+minutes*60+seconds) ){setConstraintName(ALLOCATEDTIME);}
/************************************************************
 *     Method Name: ~AllocatedTime
 *
 *     Purpose: Destructor for the AllocatedTime Class. 
 *
 *     Uses: no other methods.
 *
 *     Input: void
 *
 *     Return: no return value
 **************************************************************/
AllocatedTime::~AllocatedTime(void){}
/************************************************************
 *     Method Name: getDuration
 *
 *     Purpose: Retruns the value of the duration attribute. 
 *
 *     Uses: uses no other methods
 *
 *     Input: void
 *
 *     Return: long
 **************************************************************/
d87 1
a87 1
{
d89 8
a96 12
}
/************************************************************
 *     Method Name: saveGuts
 *
 *     Purpose: save the object to a disk file. 
 *
 *     Uses: SchedulingConstraint::restoreGuts(), RWFile::Write().
 *
 *     Input: RWFile
 *
 *     Return: void
 **************************************************************/
d98 1
a98 1
{
d101 8
a108 12
}
/************************************************************
 *     Method Name: restoreGuts
 *
 *     Purpose: Retrieves the object from disk. 
 *
 *     Uses: SchedulingConstraint::restoreGuts(), RWFile::Read().
 *
 *     Input: RWFile
 *
 *     Return: void
 **************************************************************/
d110 1
a110 1
{
d113 8
a120 12
}
/************************************************************
 *     Method Name: saveGuts
 *
 *     Purpose: saves the object to a disk file. 
 *
 *     Uses: SchedulingConstraint::restoreGuts(), RWvostream::<<.
 *
 *     Input: RWvostream
 *
 *     Return: void
 **************************************************************/
d122 1
a122 1
{
d125 8
a132 12
}
/************************************************************
 *     Method Name: restoreGuts
 *
 *     Purpose: Retrieves the object from disk. 
 *
 *     Uses: SchedulingConstraint::restoreGuts(), RWvistream::>>.
 *
 *     Input: RWvistream
 *
 *     Return: void
 **************************************************************/
d134 1
a134 1
{
d137 1
a137 1
}
a142 1
//RCS: $Id: AllocTime.C,v 1.2 1993/04/21 15:57:47 leone Exp leone $
@


1.2
log
@the line //RCS $Id$ was added to the end of all .C and .H files, the make
>> file was updated to include the "SOC STANDARDS."
@
text
@d14 1
a14 1
#define _POSIX_SOURCE 1
d20 1
a20 1
RWDEFINE_COLLECTABLE(AllocatedTime,0003)
d157 1
a157 1
//RCS: $Id$
@


1.1
log
@Initial revision
@
text
@d157 1
@

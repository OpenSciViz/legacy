head	1.1;
access
	soccm;
symbols
	Build3_1:1.1
	Build1:1.1;
locks; strict;
comment	@# @;


1.1
date	93.05.28.20.40.07;	author leone;	state Exp;
branches;
next	;


desc
@Creation
@


1.1
log
@Initial revision
@
text
@.\" Generated automatically by genman.awk version 2 from 
.\"     ObsDes.H
.\"
.TH OBSDES.H subclass "May 28 10:45" "" "RWCOLLECTABLE" 
.SH "NAME"
ObsDes.H \- class to define the observation
.PP
.SH "SYNOPSIS"
.nf
.B 
#include <SOC/ObsDes.H>
.PP
.fi
.SH "AUTHOR"
Rick Leone, NASA/Goddard Space Flight Center
email leone@@xenopus.stx.com leone@@rosserv.gsfc.nasa.gov

.PP
.SH "DESCRIPTION:"
The file ObsDes.H is the class description for the class 
ObservationDescription of the XTE-SOC class library. The Observation
Description object is created in the Proposal Management subsystem and 
is an attribute of the Proposal object.

 Feature test switches
define _POSIX_SOURCE 1
.PP
.nf
.SH "ENUMERATIONS"
.nf
eventTypesSLEWSTART, SLEWSTOP, EOSTART, EOSTOP, SAASTART, SAASTOP};
    typedef eventTypes EVENT;
    External variables
    External functions
    Structures and unions
    Signal catching functions
    Functions
    Main
    class ObservationDescription : public RWCollectable
    RWDECLARE_COLLECTABLE(ObservationDescription)
    public:
    the following are virtual functions derived from RWCollectable and
    have no definition in the virtual base class SchedulingConstraints.
    virtual unsigned binaryStoreSize() const;
    void restoreGuts(RWFile&);
    void restoreGuts(RWvistream&);
    void saveGuts(RWFile&) const;
    void saveGuts(RWvostream&) const;
    ObservationDescription(void);
    ObservationDescription(ObservationID anID  RWTime aTime);
    ObservationDescription(ProposalID& propID);
    ObservationDescription(RWFile& af);
    ~ObservationDescription(void);
    void setObservationID(ProposalID& aPropID);
    ObservationID getObservationID(void);
    RWCString getIDNumber(void);
    RWCString getProposalID(void);
    RWBoolean isMyID(ObservationID& anID) const;
    SegmentID getNextSegmentID(void);
    RWCString getCurrentSegmentID(void);
    int getNumberOfSegments(void);
    void addStartTime(RWTime& startTime);
    RWTime getStartTime(void);
    void addExpirationTime(RWTime& startTime);
    RWTime getExpirationTime(void);
    void addWeeklyObservationGroup(RWTime startTime); setStartTime()?
    WeeklyObservationGroup* getWeeklyGroup(void);
    void setRequestedTime(long anInterval);
    long getRequestedTime(void);
    void setRequestedTelemetry(int anInt);
    int getRequestedTelemetry(void);
    void addSource(Source* aSource);
    void addPrimarySource(Source* aSource);
    void deleteSource(Source* aSource);
    void sourceList(RWOrdered& sourceList);
    Source* getNextSource(void);
    Source* getPrimarySource(void);
    Source* getSource(int anInt);
    int numberOfSources(void);
    void showSources(void);
    void showConstraints(void);
    void showAttributes(void);
    RWBoolean hasConstraint(CONSTRAINT name);
    int numberOfConstraints(void);
    void constraintList(RWSet& sc);
    void addAbsoluteTimeRange(RWTime& first  RWTime& last);
    RWTime getFirstAbsoluteTime(void);
    RWTime getLastAbsoluteTime(void);
    void addAllocatedTelemetry(int anInt);
    int getAllocatedTelemetry(void);
    void addAllocatedTime(long anInterval);
    long getAllocatedTime(void);
    void addAlternate(RWCString& anObsDes);
    RWCString getAlternate(void);
    ObservationDescription* getNextAlternate(void);
    int numberOfAlternate(void);
    RWBoolean getContiguous(void);
    void addContiguous(void);
    RWBoolean getInterruptNot(void);
    void addInterruptNot(void);
    void addPhaseAngleRange(double min double max long aPeriod RWTime& anEpoch);
    double getMinPhase(void);
    double getMaxPhase(void);
    RWTime getEpoch(void);
    long getPeriod(void);
    void addPriority(int anInt);
    void resetPriority(int anInt);
    Priority getPriority(void);
    int getPriorityValue(void);
    RWBoolean checkPriority(void);
    void addRelativeTo(RWCString& leading  RWCString& trailing  long sep);
    RWCString getLeadingEvent(void);
    RWCString getTrailingEvent(void);
    long getTimeSeparation(void);
    RWBoolean getTOO(void);
    void addTOO(void);
    ObservationDescription* clone(void);
    void setCloneOf(ObservationDescription* theParentObservation);
    ObservationDescription* getCloneOf(void);
    RWBoolean scheduleObservation(RWCollectableString events  RWTime* eventTimes  ConfigurationTimeline theTimeLine);
    int howManySubsystems(void);
    RWTime getExposeStart(void);
    void setExposeStart(RWTime& aTime);
    RWTime getExposeStop(void);
    void setExposeStop(RWTime&);
    void updateExposedTime(long anInterval);
    long getExposedTime(void);
    float percentComplete(void);
    double getRa(void);
    double getDec(void);
    Position getTargetPosition(void);
    void displayAndEdit(UserID anUser);
    void draftNewProposal(UserID anUser  ObservationID anObsID);
    void draftProposal(UserID anUser  ProposalID aProposalID);
    void display(UserID anUser  ProposalID aProposalID);
    void dutyEdit(UserID anUser  ProposalID aProposalID);
    private:
    ObservationID observationID;
    RWTime expirationTime;
    RWTime startTime;
    RWTime exposeStart;
    RWTime exposeStop;
    long exposedTime;
    long requestedTime;
    int requestedTelemetry;
    RWOrdered* sources;
    Ordered list of pointers to Source objects
    the first source on the list is the primary source. This atribute could be
    simply a collection of RWCollectableString objects which are the filenames
    of the source objects.
    RWSet* constraints; SchedulingConstraint objects
    RWSetIterator constIter;
    SchedulingConstraint* getConstraint(CONSTRAINT);
    ObservationDescription* cloneOf;
    RWSet macros;                ConfigurationTimeline objects
    macros will consist of one on target ConfigurationTimeline
    object. This restriction is due to the new design where there is only one
    subobservation per observation.
    the comment CHANGE will be a note that the
    method or attribute must be changed due to the new design.
.PP
.SH "DEFINED MACROS"
.nf
OBSDES_H
.PP
.SH "INCLUDED FILES"
.nf
<rw/cstring.h>
<rw/rwtime.h>
<rw/rwset.h>
<rw/ordcltn.h>
"ObsID.H"
"AbsTimeRange.H"
"AllocTelem.H"
"AllocTime.H"
"Alternate.H"
"Contiguous.H"
"InterruptNot.H"
"PhaseAngRag.H"
"ProposalID.H"
"Priority.H"
"RelativeTo.H"
"TOO.H"
"Source.H"
"ScedConst.H"
.PP
.SH "SOURCE FILES"
.nf
ObsDes.C
ObsDes.H
.PP
.SH "SUMMARY"
.nf
.B 
void ObservationDescription::restoreGuts(RWFile& f)
    Restores the object from disk
    Uses: RWCollectable::restoreGuts(), RW::restoreFrom(), RWFile::Read(),
    RWCollectable::recursiveRestoreFrom(),ObservationID::restoreFrom()
    
.PP
.B 
void ObservationDescription::restoreGuts(RWvistream& strm)
    Restores the object from disk
    Uses:RWCollectable::restoreGuts(), RW::restoreFrom(), RWvistream::>>,
    RWCollectable::recursiveRestoreFrom(), ObservationID::restoreFrom()
    
.PP
.B 
void ObservationDescription::saveGuts(RWFile& f) const
    Saves the object on disk
    Uses:  RWCollectable::saveGuts(), RW::saveOn(), RWFile::Write(),
    RWCollectable::recursiveSaveOn(), ObservationID::saveOn()
    
.PP
.B 
void ObservationDescription::saveGuts(RWvostream& strm) const
    Saves the object on disk
    Uses:  RWCollectable::saveGuts(), RW::saveOn(), RWvostream::<<,
    RWCollectable::recursiveSaveOn(), observationID::saveOn()
    
.PP
.B 
ObservationDescription::ObservationDescription(void):exposedTime(0),requestedTime(0),requestedTelemetry(0),startTime(GMTOFF),expirationTime(GMTOFF),exposeStart(GMTOFF),exposeStop(GMTOFF)
    Default constructor for the ObservationDescription class.
    Uses:  
    
.PP
.B 
ObservationDescription::ObservationDescription(ProposalID& propID):observationID(propID.getNextObservationID()),exposedTime(0),requestedTime(0),requestedTelemetry(0),startTime(GMTOFF),expirationTime(GMTOFF),exposeStart(GMTOFF),exposeStop(GMTOFF)
    Constructor for the ObservationDescription class.
    Uses:  ProposalID::getNextObservation()
    
.PP
.B 
ObservationDescription::~ObservationDescription(void)
    Destructor for the ObservationDescription class.
    Uses:  
    
.PP
.B 
void ObservationDescription::setObservationID(ProposalID& aPropID)
    sets the observationID attribute of an ObservationDescription
    object that was created using the Default constructor.
    Uses: ObservationID::setObservationID(),
    ProposalID::getNextObservationID()
    
.PP
.B 
SchedulingConstraint* ObservationDescription::getConstraint(CONSTRAINT name)
    A private method that selects the requesteded Scheduling-
    constraint object from the constraints attribute.
    Uses: SchedulingConstraint::getConstraintName(), RWSetIterator::()
    
.PP
.B 
ObservationID ObservationDescription::getObservationID(void)
    Retruns the observationID attribute.
    Uses: no other methods
    
.PP
.B 
RWCString ObservationDescription::getIDNumber(void)
    Retruns the observationID attribute as a string.
    Uses: ObservationID::getObservationID()
    
.PP
.B 
RWCString ObservationDescription::getProposalID(void)
    Retruns the ID of the Proposal that contains this 
    observation, as a string.
    Uses: ObservationID::getProposalID()
    
.PP
.B 
RWBoolean ObservationDescription::isMyID(ObservationID& anID) const
    Retruns TRUE if the input matches the observationID attribute.
    Uses: ObservationID::getObservationID()
    
.PP
.B 
SegmentID ObservationDescription::getNextSegmentID(void)
    Issues the next SegmentID.
    Uses: ObservationID::getNextSegmentID()
    
.PP
.B 
int ObservationDescription::getNumberOfSegments(void)
    Retruns the number of SegmentID objects that have 
    been issued.
    Uses: ObservationID::getNumberOfSegments()
    
.PP
.B 
void ObservationDescription::addStartTime(RWTime& aTime)
    Sets the startTime attribute by adding the difference 
    between aTime and the present value of startTime. This is a work
    around for no assignment operator in the RWTime class, where one
    need not know the present value of start time. 
    Uses: RWTime::seconds(), RWTime::+=.
    
.PP
.B 
RWTime ObservationDescription::getStartTime(void)
    Returns the startTime attribute.
    Uses: no other methods.
    
.PP
.B 
void ObservationDescription::addExpirationTime(RWTime& aTime)
    Sets the expirationTime attribute by adding the difference 
    between aTime and the present value of expirationTime. This is a work
    around for no assignment operator in the RWTime class, where one
    need not know the present value of expirationTime. 
    Uses: RWTime::seconds(), RWTime ::+=.
    
.PP
.B 
RWTime ObservationDescription::getExpirationTime(void)
    Returns the startTime attribute.
    Uses: no other methods.
    
.PP
.B 
void ObservationDescription::setRequestedTime(long anInterval)
    Sets the requestedTime attribute.
    Uses: no other method
    
.PP
.B 
long ObservationDescription::getRequestedTime(void)
    Returns the requestedTime attribute.
    Uses: no other methods
    
.PP
.B 
void ObservationDescription::setRequestedTelemetry(int anInt)
    Sets the requestedTime attribute.
    Uses: no other methods
    
.PP
.B 
int ObservationDescription::getRequestedTelemetry(void)
    Returns the requestedTelemetry attribute.
    Uses: no other methods
    
.PP
.B 
void ObservationDescription::addPrimarySource(Source* aSource)
    Inserts the primary source into the first position of
    of the sources attribute.
    Uses: RWOrdered::prepend().
    
.PP
.B 
void ObservationDescription::addSource(Source* aSource)
    Inserts a source object into the sources attribute.
    Uses: RWOrdered::insert().
    
.PP
.B 
void ObservationDescription::deleteSource(Source* aSource)
    deletes a source object the sources attribute.
    Uses: RWOrdered::remove().
    
.PP
.B 
Source* ObservationDescription::getPrimarySource(void)
    Retruns the primary source, which is in the first 
    position of of the sources attribute.
    Uses: RWOrdered::first().
    
.PP
.B 
int ObservationDescription::numberOfSources(void)
    Retruns the number of source objects in the sources attribute.
    Uses: RWOrdered::entries().
    
.PP
.B 
void ObservationDescription::sourceList(RWOrdered& sourceList)
    Writes the contents of the sources attribute into
    a RWOrdered reference.
    Uses: RWOrderedIterator::(), RWOrdered::insert().
    
.PP
.B 
void ObservationDescription::showSources(void)
    Displays the contents of the each of the source
    objects in the sources attribute on standard out. To do this
    a RWOrdered iterator is created using the dereferenced source
    attribute as the input.
    Uses: RWOrderedIterator::(), Source::showAttributes().
    
.PP
.B 
void ObservationDescription::showConstraints(void)
    Displays all of the attributes of all, of the constraints 
    objects in the constraints attribute, to standard out. To do this
    a RWSetIterator is created using the dereferenced constraints 
    attribute as the input.
    Uses: RWSetIterator::(),RWSetIterator::reset(),
    SchedulingConstraint::getConstraintName(), 
    AbsoluteTimeRange::getFirstTime(), AbsoluteTimeRange::getLastTime(), 
    AllocatedTelemetry::getTelemetryVolume(), AllocatedTime::getDuration(),
    Alternate::getAlternate(), PhaseAngleRange::getMinPhase(), 
    PhaseAngleRange::getMaxPhase(), PhaseAngleRange::getEpoch(), 
    PhaseAngleRange::getPeriod(), Priority::getPriority(), 
    RelativeTo::getLeadingEvent(), RelativeTo::getTrailingEvent(),
    RelativeTo::getTimeSeparation().
    
.PP
.B 
void ObservationDescription::showAttributes(void)
    Displays all of the attributes on standard out.
    This includes the constraints and suorces and their attributes.
    Uses: getIDNumber(), getStartTime(), getExpirationTime(),
    getExposeTime(), getExposeStop(), getExposeTime(), getRequestedTime(),
    getRequestedTelemetry(), showConstraints(), showSources().
    
.PP
.B 
RWBoolean ObservationDescription::hasConstraint(CONSTRAINT name)
    returns true if the name appears in the constraints 
    attribute. Answers the question, "does this observationDescription
    have this constraint?"
    Uses: getConstraint().
    
.PP
.B 
int ObservationDescription::numberOfConstraints(void)
    Returns the number of SchedulingConstraint objects in
    the constraints attribute.
    Uses: RWSet.entries().
    
.PP
.B 
void ObservationDescription::constraintList(RWSet& sc)
    Places the SchedulingConstraint objects of the constraints
    attribute in a RWSet object.
    Uses: RWSet.insert().
    
.PP
.B 
void ObservationDescription::addAbsoluteTimeRange(RWTime& first, RWTime& last)
    Inserts an AbsoluteTimeRange object into the 
    constraints attribute.
    Uses: RWSet.insert(), AbsoluteTimeRange::AbsoluteTimeRange().
    
.PP
.B 
RWTime ObservationDescription::getFirstAbsoluteTime(void)
    Returns the firstTime attribute of the AbsoluteTimeRange 
    object in the constraints attribute.
    Uses: getConstraint(), AbsoluteTimeRange::getFirstTime().
    
.PP
.B 
RWTime ObservationDescription::getLastAbsoluteTime(void)
    Returns the lastTime attribute of the AbsoluteTimeRange 
    object in the constraints attribute.
    Uses: getConstraint(), AbsoluteTimeRange::getlastTime().
    
.PP
.B 
void ObservationDescription::addAllocatedTelemetry(int anInt)
    Inserts an AllocatedTelemetry object in the 
    constraints attribute.
    Uses: RWSet::insert(), AllocatedTelemetry::AllocatedTelemetry().
    
.PP
.B 
int ObservationDescription::getAllocatedTelemetry(void)
    Inserts an AllocatedTelemetry object in the 
    constraints attribute.
    Uses: getConstraint(), AllocatedTelemetry::getTelemetryVolume().
    
.PP
.B 
void ObservationDescription::addAllocatedTime(long anInterval)
    Inserts an AllocatedTime object in the 
    constraints attribute.
    Uses: RWSet::insert(),AllocatedTime::AllocatedTime().
    
.PP
.B 
long ObservationDescription::getAllocatedTime(void)
    Returns the duration attribute of the AllocatedTelemetry 
    object in the constraints attribute.
    Uses: getConstraint(), AllocatedTime::getDuration().
    
.PP
.B 
void ObservationDescription::addAlternate(RWCString& anObsDes)
    Inserts an Alternate object in the constraints attribute.
    Uses: RWSet::insert(), Alternate::Alternate().
    
.PP
.B 
RWCString ObservationDescription::getAlternate(void)
    returns the value of the alternate attribute of the
    Alternate constraint object.
    Uses: RWSet::insert(), Alternate::Alternate().
    
.PP
.B 
RWBoolean ObservationDescription::getContiguous(void)
    returns the ID of the Alternate observation.
    Uses: ?
    
    ObservationDescription* ObservationDescription::getNextAlternate(void)  {  }
    int ObservationDescription::numberOfAlternate(void)  {  }
    Needs to be revised!!!
    ***********************************************************
    getContiguous
    
    to affirm the presence of a contiguous object in the 
    constraints attribute.
    Uses: getConstraint().
    
.PP
.B 
void ObservationDescription::addContiguous(void)
    returns the value of the maxSplits attribute of
    contiguous object.
    Uses: ?
    
    ***********************************************************
    contiguousDontCare
    
    returns the value of the dontCare attribute of the
    contiguous object.
    Uses: ?
    
    Needs to be revised!!!
    ***********************************************************
    addContiguous
    
    Inserts a contiguous object in the constraints attribute.
    Uses: RWSet::insert(), Contiguos::Contiguous().
    
.PP
.B 
RWBoolean ObservationDescription::getInterruptNot(void)
    returns the value of the firstTime of day attribute
    of the TimeOfDay constraint object.
    Uses: ?
    
    Needs to be revised!!!
    ***********************************************************
    getLastTimeOfDay
    
    returns the value of the lastTimeOfDay attribute of
    the timeOfDay constraint object.
    Uses: ?
    
    Needs to be revised!!!
    ***********************************************************
    addLastTimeOfDay
    
    
    
    Uses: ?
    
    Needs to be revised!!!
    ***********************************************************
    addDayOfWeek
    
    
    
    Uses: ?
    
    Needs to be revised!!!
    ***********************************************************
    getFistDayOfWeek
    
    returns the value of the firstDay attribute of 
    the dayOfWeek constraint object.
    Uses: ?
    
    Needs to be revised!!!
    ***********************************************************
    getLastDayOfWeek
    
    returns the value of the lastDay attribute of the
    dayOfWeek constraint object.
    Uses: ?
    
    Needs to be revised!!!
    ***********************************************************
    getInterruptNotMinTime
    
    returns the value of the minTime attribute of the
    interruptNot constraint object.
    Uses: ?
    
.PP
.B 
void ObservationDescription::addInterruptNot(void)
    Inserts an InterruptNot object in the constraints attribute.
    Uses: RWSet::insert(), InterruptNot::InterruptNot().
    
.PP
.B 
void ObservationDescription::addPhaseAngleRange(double min, double max, long aPeriod, RWTime& anEpoch)
    adds the orbitDay constraint object to the 
    constraints attribute.
    Uses: ?
    
    Needs to be revised!!!
    ***********************************************************
    getOrbitDay
    
    returns TRUE if the orbitDay constraint is present
    Uses: ?
    
    Needs to be revised!!!
    ***********************************************************
    addOrbitNight
    
    adds the orbitNight constraint to the constraints
    attribute.
    Uses: ?
    
    ***********************************************************
    getOrbitNight
    
    returns the value of the orbitNight attribute. 
    Returns true if the orbitNight constraint is present.
    Uses: ?
    
    ***********************************************************
    addPhaseAngleRange
    
    Inserts a PhaseAngleRange object in the constraints attribute.
    Uses: RWSet::insert(), PhaseAngleRange::PhaseAngleRange().
    
.PP
.B 
double ObservationDescription::getMinPhase(void)
    Returns the value of the maxPhase attribute of the 
    PhaseAngleRange object in the constraints attribute.
    Uses: getConstraint(), PhaseAngleRange::getMinPhase().
    
.PP
.B 
double ObservationDescription::getMaxPhase(void)
    Returns the value of the minPhase attribute of the 
    PhaseAngleRange object in the constraints attribute.
    Uses: getConstraint(), PhaseAngleRange::getMaxPhase().
    
.PP
.B 
RWTime ObservationDescription::getEpoch(void)
    Returns the value of the epoch attribute of the 
    PhaseAngleRange object in the constraints attribute.
    Uses: getConstraint(), PhaseAngleRange::getEpoch().
    
.PP
.B 
long ObservationDescription::getPeriod(void)
    Returns the value of the period attribute of the 
    PhaseAngleRange object in the constraints attribute.
    Uses: getConstraint(), PhaseAngleRange::getPeriod().
    
.PP
.B 
void ObservationDescription::addPriority(int anInt)
    Inserts a Priority object in the constraints attribute.
    Uses: RWSet::insert(), Priority::Priority().
    
.PP
.B 
RWBoolean ObservationDescription::checkPriority(void)
    Returns TRUE if value of the priority attribute of
    the Priority object in the constraints attribute, is in the 
    acceptable range of values. 
    Uses: getConstraint(), Priority::checkPriority().
    
.PP
.B 
int ObservationDescription::getPriorityValue(void)
    Returns the value of the priority attribute of the
    Priority object in the constraints attribute. 
    Uses: getConstraint(), Priority::getPriority().
    
.PP
.B 
void ObservationDescription::addRelativeTo(RWCString& leading, RWCString& trailing, long sep)
    Inserts an RelativeTo object in the constraints attribute. 
    Uses: RWSet::insert(), RelativeTo::RelativeTo().
    
.PP
.B 
RWCString ObservationDescription::getLeadingEvent(void)
    Retruns the value of the leadingEvent attribute of the
    RelativeTo object in the constraints attribute. 
    Uses: getConstraint(), RelativeTo::getLeadingEvent().
    
.PP
.B 
RWCString ObservationDescription::getTrailingEvent(void)
    Retruns the value of the trailingEvent attribute of the
    RelativeTo object in the constraints attribute. 
    Uses: getConstraint(), RelativeTo::getTrailingEvent().
    
.PP
.B 
long ObservationDescription::getTimeSeparation(void)
    Retruns the value of the timeSeparation attribute of the
    RelativeTo object in the constraints attribute. 
    Uses: getConstraint(), RelativeTo::getTimeSeparation().
    
.PP
.B 
RWBoolean ObservationDescription::getTOO(void)
    Affirms the presence of a TOO object in the
    constraints attribute. 
    Uses: getConstraint().
    
.PP
.B 
void ObservationDescription::addTOO(void)
    Inserts a TOO object in the constraints attribute. 
    Uses: RWSet::insert().
    
.PP
.B 
RWTime ObservationDescription::getExposeStart(void)
    Returns the value of the ExposeStart attribute. 
    Uses: no other methods
    
.PP
.B 
void ObservationDescription::setExposeStart(RWTime& aTime)
    Sets the exposeStart attribute by adding the difference 
    between aTime and the present value of exposeStart. This is a work
    around for no assignment operator in the RWTime class, where one
    need not know the present value of exposeStart. 
    Uses: RWTime::seconds().
    
.PP
.B 
RWTime ObservationDescription::getExposeStop(void)
    Returns the value of the ExposeStop attribute. 
    Uses: RWTime::seconds().
    
.PP
.B 
void ObservationDescription::setExposeStop(RWTime& aTime)
    Sets the exposeStop attribute by adding the difference 
    between aTime and the present value of exposeStop. This is a work
    around for no assignment operator in the RWTime class, where one
    need not know the present value of exposeStop. 
    Uses: RWTime::seconds().
    
.PP
.B 
void ObservationDescription::updateExposedTime(long anInterval)
    Sets the value of the ExposeTime attribute. 
    Uses: no other methods.
    
.PP
.B 
long ObservationDescription::getExposedTime(void)
    Retruns the value of the ExposeStart attribute. 
    Uses: no other methods.
    
.PP
.B 
float ObservationDescription::percentComplete(void)
    Retruns the value (allocateTime/exposedTime)
    Uses: getAllocatedTime().
    
.PP
.B 
double ObservationDescription::getRa(void)
    Retruns the value of the Ra attribute of the 
    primary source object
    Uses: ?
    
.PP
.B 
double ObservationDescription::getDec(void)
    Retruns the value of the Dec attribute of the
    primary source object
    Uses: ?
    
@

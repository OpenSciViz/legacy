head	1.1;
access
	soccm;
symbols
	Build3_1:1.1
	Build1:1.1;
locks; strict;
comment	@# @;


1.1
date	93.05.28.20.40.00;	author leone;	state Exp;
branches;
next	;


desc
@Creation
@


1.1
log
@Initial revision
@
text
@.\" Generated automatically by genman.awk version 2 from 
.\"     AllocTime.H
.\"
.TH ALLOCTIME.H subclass "May 28 09:06" "" "SCHEDULINGCONSTRAINT" 
.SH "NAME"
AllocTime.H \- class to constrain the target exposure time
.PP
.SH "SYNOPSIS"
.nf
.B 
#include <SOC/AllocTime.H>
.PP
.B 
class AllocatedTime : public SchedulingConstraint
.PP
.B 
    Public members
.B 
        AllocatedTime(void);
.B 
        AllocatedTime(long seconds);
.B 
        AllocatedTime(int days, long hours = 0, long minutes = 0, long seconds = 0);
.B 
        ~AllocatedTime(void);
.B 
        long getDuration(void);
.B 
        void saveGuts(RWFile&) const;
.B 
        void restoreGuts(RWFile&);
.B 
        void saveGuts(RWvostream&)const;
.B 
        void restoreGuts(RWvistream&);
.PP
.B 
    Private members
.B 
        RWDECLARE_COLLECTABLE(AllocatedTime)
.B 
        long duration;
.PP
.fi
.SH "AUTHOR"
Rick Leone, NASA/Goddard Space Flight Center
email leone@@xenopus.stx.com leone@@rosserv.gsfc.nasa.gov

.PP
.SH "DESCRIPTION:"
The file AllocTime.H is the class description for the class 
AllocatedTime of the XTE-SOC class library. This class is a subclass 
of SchedulingConstraint class and is an attribute of the ObservationDe-
scription class. The AllocatedTime object is the amount of exposure
time that the NASAHQ has awarded the observation. 

 Feature test switches
define _POSIX_SOURCE 1
.PP
.nf
.SH "DEFINED MACROS"
.nf
AllocatedTime_H
.PP
.SH "INCLUDED FILES"
.nf
"ScedConst.H"
.PP
.SH "SOURCE FILES"
.nf
AllocTime.C
AllocTime.H
.PP
.SH "SUMMARY"
.nf
.B 
AllocatedTime::AllocatedTime(void)
    Default constructor for the AllocatedTime Class. 
    Uses: SchedulingConstraint::setConstraintName().
    
.PP
.B 
AllocatedTime::AllocatedTime(long seconds):duration(seconds)
    Default constructor for the AllocatedTime Class. 
    Uses: SchedulingConstraint::setConstraintName().
    
.PP
.B 
AllocatedTime::AllocatedTime(int days, long hours, long minutes, long seconds):duration(( days*86400+hours*3600+minutes*60+seconds) )
    Constructor for the AllocatedTime Class. 
    Uses: SchedulingConstraint::setConstraintName().
    
.PP
.B 
AllocatedTime::~AllocatedTime(void)
    Destructor for the AllocatedTime Class. 
    Uses: no other methods.
    
.PP
.B 
long  AllocatedTime::getDuration(void)
    Retruns the value of the duration attribute. 
    Uses: uses no other methods
    
.PP
.B 
void AllocatedTime::saveGuts(RWFile& f) const
    Save the object to a disk file. 
    Uses: SchedulingConstraint::restoreGuts(), RWFile::Write().
    
.PP
.B 
void AllocatedTime::restoreGuts(RWFile& f)
    Retrieves the object from disk. 
    Uses: SchedulingConstraint::restoreGuts(), RWFile::Read().
    
.PP
.B 
void AllocatedTime::saveGuts(RWvostream& strm) const
    Saves the object to a disk file. 
    Uses: SchedulingConstraint::restoreGuts(), RWvostream::<<.
    
.PP
.B 
void AllocatedTime::restoreGuts(RWvistream& strm)
    Retrieves the object from disk. 
    Uses: SchedulingConstraint::restoreGuts(), RWvistream::>>.
    
@

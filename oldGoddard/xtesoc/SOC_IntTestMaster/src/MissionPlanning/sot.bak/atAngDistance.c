#include "atFunctions.h"
#include "atError.h"
#include <math.h>

int                 /* Finding angular distance                 */
atAngDistance(      /*                   ver 1.0   92/07/01  ay */
        AtVect x,   /* input: */
        AtVect y,   /* input: */
        double *r)  /* output: angular distance between x and y in radian */
{
  double  d1;
  d1 = ( x[0]*y[0] + x[1]*y[1] + x[2]*y[2] ) / atNorm(x) / atNorm(y) ;
  if ( d1 > 1.0-EPS ) { d1 = 1.0; }
  if ( d1 < -1.0+EPS ) { d1 = -1.0; }
  *r = acos( d1 );
  if ( *r < 0.0 || *r > PI ) { return (-1); }
  return (NORMAL_END);
}


#include "atFunctions.h"
#include "atError.h"

/* 
 * Find Rotation Matrix for conversion of equatorial coordinate systems 
 * correcting for precession (SAISA)
 */
int atPrecessRM(
	double mjd0,	/* input: modified Julian Day for the original coord*/
	double mjd,	/* input: modified Julian Day for the new coord*/
	AtRotMat rm)	/* output: rotation matrix to correct precession*/
{
    AtEulerAng ea;

    atPrecessEuler(mjd0, mjd, &ea);
    atEulerToRM(&ea, rm);
    return 0;
} 

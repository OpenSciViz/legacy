#include "atFunctions.h"
#include "atError.h"
#include <math.h>

/* 
 * calc Greenwich sidereal time (KOSEJI)
 */
int atSidereal(
	double mjd,		/* input: time in MJD */
	double *gsttod) 	/* output: Greenwich sidereal time (radian)
				 at mjd true of date*/ 
{
    /* Builtin functions */
    double d_mod(double *, double *);

    /* Local variables */
    static double d, m, x;

    d = (double) ((int) (mjd));
    x = (d - 15020.) / 36525.;
    m = (mjd - d) * 24. * 60.;
    *gsttod = (x * 3.8708e-4 + 36000.7689) * x + 99.6909833 + m * .25068447;
    *gsttod = fmod(*gsttod, 360.) * DEG2RAD;
    return 0;
}
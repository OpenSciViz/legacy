/************************************************************************/
/*									*/
/*	type definitions of "At" library 				*/
/*						v1.5 93/01/26 N.Kawai   */
/*									*/
/************************************************************************/

/* 
 * 	Three-dimensional Cartesian Vector.
 */
typedef double AtVect[3];

/* 
 * 	3 x 3 Rotation Matrix.
 */
typedef double AtRotMat[3][3];

/* 
 * 	Euler angle notation of rotation. (usually z-y-z)
 */
typedef struct {double phi; 	/* First Euler Angle (radian)*/
		double theta; 	/* Second Euler Angle (radian)*/
		double psi;}	/* Third Euler Angle (radian)*/
	AtEulerAng;
		
/* 
 * 	Three-dimensional Vector in Polar coordinate.
 */

typedef struct	{double r;	/* radial component */
		double lon; 	/* longitude or R.A. (radian)*/
		double lat;}	/* latitude or declination (radian)*/
	AtPolarVect;

typedef struct	{unsigned int hour;	/* hour part  */
		 unsigned int min; 	/* minuit */
		 double sec;}		/* second */
	AtRightAscension;
	

typedef struct  {int sign;              /* +1: north or -1:south */
                 unsigned int deg;      /* degree part  */
                 unsigned int min;      /* minuit */
                 double sec;}           /* second */
        AtDeclination;
	

typedef struct	{AtRightAscension ra;	/* right ascension in hhmmss  */
		AtDeclination dec; 	/* declination in ddmmss */
		}
	AtPolarVect60;
	
/* 
 * 	Quaternion notation of rotation (Q-parameters).
 */
typedef double AtQuat[4];


typedef struct	{int yr;	/* year  */
		int mo; 	/* month */
		int dy; 	/* day */
		int hr; 	/* hour */
		int mn; 	/* minuit */
		int sc; 	/* sec */
		float ms; 	/* ms */
		}
	AtTime;
	

/**************************************************************************/
/*									*/
/*	Global Variable (COMMON variables in FORTRAN)			*/
/*									*/
/**************************************************************************/
/*
 *	Orbital Elements (angles are in radian)
 */

struct atElement {
    AtTime itz;	/* Epoch of Elements in year, month, day, hour, min, sec */
    double semiax;	/* Semi-major Axis in km */
    double eccent;	/* Eccentricity */
    double aincln;	/* Inclination */
    double ragome;	/* Right Ascension of Ascending Node */
    double smaome;	/* Argument of Perigee (angle from ascending node) */
    double omean0;	/* Mean Anomaly at the Epoch */
    double adot;	/* Time Derivative of "semiax" in km/day */
    double eccdot;      /* Time Derivative of "eccent" in /day (v1.6)*/
    double aindot;      /* Time Derivative of "aincln" in radian/day (v1.6)*/
    double ragdot;	/* Time Derivative of "ragome" in radian/day */
    double smodot;	/* Time Derivative of "smaome" in radian/day */
    double znbar;	/* Time Derivative of Mean Anomaly in radian/day,
    			i.e. Mean Motion */
    double znbadt;	/* 2nd Derivative of Mean Anomaly in radian/day**2 */
    double mjdz;	/* Modified Julian Day of the Epoch */
    double perige;	/* Perigee */
    double apoge;	/* Apogee */
}; /* atElement; moved by Alex Antunes, 3/3/95 */
 

/**************************************************************************/
/*									*/
/*	Basic constants							*/
/*									*/
/**************************************************************************/

#define PI 3.1415926535897932385
#define TWO_PI 6.283185307179586477
#define DEG2RAD 0.017453292519943295769
#define RAD2DEG 57.295779513082320877
#define EARTH_RADIUS 6378.140
#define MOON_RADIUS 1738.
#define EARTH_E2 .0066934216
#define AU 149597870.
#define EPS 1.e-12
#define MJD_B1950 33281.923
#define MJD_J2000 51544.500

/**************************************************************************/
/*									*/
/*	Basic unit conversion						*/
/*									*/
/**************************************************************************/

/* 
 * covert Right Ascension in hr,min,s to radian.
 */
double atRAToRadian(		/* right ascension in radian  */
	AtRightAscension ra);	/* right ascension in hhmmss  */

/* 
 * covert Declination in deg,min,s to radian.
 */
double atDecToRadian(		/* declination in radian  */
	AtDeclination dec); 	/* declination in ddmmss */

/* 
 * covert R.A. and Dec in hr/deg,min,s to Vector.
 */
int atPol60ToVect(
	    AtPolarVect60 *p, 	/* polar vector in hh/deg mm ss */
	    AtVect x);		/* output: vector */

/* 
 * covert Vector to R.A. and Dec in hr/deg,min,s.
 */
int atVectToPol60(
	    AtVect x,		/* output: vector */
	    AtPolarVect60 *p); 	/* polar vector in hh/deg mm ss */

/* 
 * covert Vector to R.A. and Dec in degree.
 */
int atVectToPolDeg(
	    AtVect x,		/* input: vector */
	    double *r, 		/* vector length */
	    double *alpha, 	/* R.A. in degree */
	    double *delta); 	/* Dec. in degree */

/* 
 * covert R.A. and Dec in degree to Vector
 */
int atPolDegToVect(
	    double r, 		/* vector length */
	    double alpha, 	/* R.A. in degree */
	    double delta, 	/* Dec. in degree */
	    AtVect x);		/* output: vector */


/************************************************************************/
/*									*/
/*	Basic coordinate conversion subroutines				*/
/*									*/
/************************************************************************/

/* 
 * ROTATE A VECTOR WITH ROTATION MATRIX.
 */
int atRotVect(
	AtRotMat rm,	/* input: rotation matrix */
	AtVect x,	/* input: vector */
	AtVect y);	/* output: vector */


/* 
 * CALC ROTATION MATRIX FOR AXIS AND ROLL ANGLE.
 */
int atSetRotMat(
	AtVect axis,	/* input: axis of rotation, should be non zero */
	double roll,	/* input: roll angle around axis (radian) */
	AtRotMat rm);	/* output: rotation matrix */

/* 
 * ROTATION MATRIX defined by New Z-axis and a vector in (+)X-Z half plane.
 */
int atSetRotMatZX(
	AtVect zAxis,	/* input: vector defining new z-axis */
	AtVect xAxis,	/* input: vector in new +X-Z half plane */
	AtRotMat rm);	/* output: rotation matrix */

/* 
 * CALC INVERSE ROTATION MATRIX.
 */
int atInvRotMat(
	AtRotMat rm,	/* input: rotation matrix */
	AtRotMat rm2);	/* output: inversed rotation matrix */

/* 
 * NORMALIZE A VECTOR.
 */
int atNormVect(
	AtVect x,	/* input: vector */
	AtVect y);	/* output: normalized vector*/

/* 
 * Norm (Absolute size) of A VECTOR.
 */
double atNorm(	/* output: normalized vector*/
	AtVect x);	/* input: vector */

/* 
 * product of two rotation matrices rm2 = rm1 rm0
 */

int atRMProd(
	AtRotMat rm0,	/* input: rotation matrix to be multiplied*/
	AtRotMat rm1,	/* input: rotation matrix to multiply*/
	AtRotMat rm2);	/* output: product */

/* 
 * Checking consistency (unitarity) of a rotation matrix
 */

int atRMCheck(	
	AtRotMat rm);	/* input/output: rotation matrix to be checked*/

/* 
 * FIND EULER ANGLES FOR A ROTATION MATRIX.
 */
int atRMToEuler(
	AtRotMat rm,		/* input: rotation matrix */
	AtEulerAng *ea);	/* output: z-y-z Euler Angle (radian) */

/* 
 * FIND ROTATION MATRIX FOR A EULER ANGLE SET.
 */
int atEulerToRM(
	AtEulerAng *ea,		/* input: z-y-z Euler Angle (radian) */
	AtRotMat rm);		/* output: rotation matrix */

/* 
 * convert a Rotation Matrix to Quaternion.
 */
int atRMToQuat(
	AtRotMat rm,	/* input: rotation matrix */
	AtQuat q);	/* output: quaternion */

/* 
 * FIND ROTATION MATRIX FOR A EULER ANGLE SET.
 */
int atQuatToRM(
	AtQuat q,		/* input: quaternion */
	AtRotMat rm);		/* output: rotation matrix */

/* 
 * product of two quaternion: q2 = q0 q1
 * 	(in matrix representation: rm(q2) = rm(q1) rm(q0).
 *	note the inverse order of quaternion as compared to matrix )
 */

int atQuatProd(
	AtQuat q0,	/* input: quaternion to be multiplied*/
	AtQuat q1,	/* input: quaternion to multiply*/
	AtQuat q2);	/* output: product */
/************************************************************************/
/*									*/
/*	Attitude and Orbit functions Library based on CSUB.FORT77	*/
/*									*/
/************************************************************************/

/* HENKAN */

/* 
 * calc inverse vector  (RVSVCT)
 */
int atInvVect(
	AtVect x,	/* in: vector */
	AtVect y);	/* out: reversed vector */

/* 
 * add vectors (z = x + y)
 */
int atAddVect(
	AtVect x,	/* in: vector */
	AtVect y,	/* in: vector */
	AtVect z);	/* out: sum vector of x and y*/

/* 
 * copy vectors (z = x)
 */
int atCopyVect(
	AtVect x,	/* in: vector */
	AtVect z);	/* out: copy vector of x*/

/* 
 * multiply and add vectors (z = f*x + g*y)
 */
int atMulAddVect(
	double f,	/* in: multiplicand for x */
	AtVect x,	/* in: vector */
	double g,	/* in: multiplicand for y */
	AtVect y,	/* in: vector */
	AtVect z);	/* out: answer*/

/* 
 * calc angular distance (RIKAKU)
 */
int atAngDistance(
	AtVect x,	/* input */
	AtVect y, 	/* input */
	double *r);	/* output: angular distance between x and y 
				in radian */

		
/* 
 * calc crossing points of cones (CROSS)
 */
int atCrossPts(
	AtVect x, 	/* input */
	double r1, 	/* input: angular distance from x (radian) */
	AtVect y, 	/* input */
	double r2, 	/* input: angular distance from y (radian) */
	AtVect z[2]);	/* output: two crossing points of the two cones */

/* 
 * calc Vector Product (Gaiseki) of two vectors (SPOLE)
 */
int atVectProd(
	AtVect x, 	/* input */
	AtVect y, 	/* input */
	AtVect z);	/* output: vector (outer) product of x and y */

/* 
 * calc Scalar Product (Naiseki, Dot-prooduct) of two vectors 
 */
double atScalProd(	/* output: scaler (inner) product of x and y */
	AtVect x, 	/* input */
	AtVect y);	/* input */

/* 
 * set Euler angles (EURST2)
 */
int atSetEuler(
	AtPolarVect *z, 	/* input: new Z-axis in old coordinate */
	AtPolarVect *y, 	/* input: vector in new Z-(+Y) plane*/
	AtEulerAng *ea);	/* output:Z-Y-Z Euler angle for the rotation */

/* 
 * rotate polar vector with Euler angles (ROTAT2)
 */
int atRotPVect(
	AtEulerAng *ea, 	/* input */
	AtPolarVect *x, 	/* input */
	AtPolarVect *y);	/* output: result */



/* RECPOL */
/* 
 * convert coordinate from Cartesian to polar (RECPOL)
 */
int atVectToPol(
	AtVect x, 		/* input */
	AtPolarVect *y);	/* output: result */

/* 
 * convert coordinate from polar to Cartesian(POLREC)
 */
int atPolToVect(
	AtPolarVect *x, 	/* input */
	AtVect y);		/* output: result */
/* 
 * convert R.A. and Dec from hh.mm.ss to radian
 */
int atConvPol(
	AtPolarVect60 *x, 	/* input */
	AtPolarVect *y); 	/* result */


/* MJD */
/* 
 * convert UT to Modified Julian Day (MJULIA)
 */
int atMJulian(
	AtTime *time,		/* input: yr,mo,day,hr,min,s,ms */
	double *mjd);		/* output: modified julian day */

/* 
 * convert Modified Julian Day to UT (MJDATE)
 */
int atMJDate(
	double mjd,	    /* input:modified julian day */
	AtTime *time);	    /* output: yr,mo,day,hr,min,s,ms */


/* 
 * format time in character (yy/mm/dd hh:mm:ss.ssssss) char[25]
 */
int atCTime(
	AtTime *time,		/* input: yr,mo,day,hr,min,s,ms */
	char *ctime);		/* output: formated date (char [25]) */


/************************************************************************/
/*									*/
/*	Functions related to ephemeis and orbit				*/
/*									*/
/************************************************************************/

/* definitions of coordinate used in the description of functions:

	sidereal: coordinate system fixed to the sky, 
		refers to J2000 equatorial coordinate unless specified.
	
	geodetic: geocentric coordinate system  bound to the earth.
		North pole is on Z-axis, (0,0) (equator at Greenwich longitude) 
		defines X-axis, and y-axis is at east longitude = 90 deg.

	ground:	ground coordinate system at a tracking station on the earth
	 	surface. Zenith defines the Z-axis, X-axis in the north, 
		and Y in the west.
		    
	azimuth and elevation: ground coordinate in polar representation, but
		the azimuth is measured from north as zero, then rotates
		in clockwise to the east, south, and to the west.
		    
	geographical: Position of a place on the earth in 
		    "longitude", "latitude", and "altitude" in usual 
		    geographical representation.
		    
*/

/* SAISA */
/* 
 * convert equatorial coordinate systems correcting for precession (SAISA)
 */
int atPrecession(
	double mjd0,	/* input: modified Julian Day */
	AtVect x0,	/* input: vector in equatorial coordiante at mjd0 */ 
	double mjd,	/* input: modified Julian Day */
	AtVect x);	/* output: vector in equatorial coordiante at mjd1 */ 

/* 
 * Find Rotation Matrix for conversion of equatorial coordinate systems 
 * correcting for precession (SAISA)
 */
int atPrecessRM(
	double mjd0,	/* input: modified Julian Day for the original coord*/
	double mjd,	/* input: modified Julian Day for the new coord*/
	AtRotMat rm);	/* output: rotation matrix to correct precession*/

/* 
 * Find Euler Angles for conversion of equatorial coordinate systems 
 * correcting for precession (SAISA)
 */
int atPrecessEuler(
	double mjd0,	/* input: modified Julian Day for the original coord*/
	double mjd,	/* input: modified Julian Day for the new coord*/
	AtEulerAng *ea);	/* output: Euler Ang to correct precession*/
	
/* 
 * conversion of equatorial coordinate systems 
 * correcting for precession (based on SAISA in ASTRO-C csub1)
 */

int atSaisa(
	double mjd0,		/* input: MJD for the original coord*/
	AtPolarVect *pv0,	/* input: original polar coordinate */
	double mjd,		/* input: MJD for the new coord*/
	AtPolarVect *pv);	/* output: Euler Ang to correct precession*/

/* ORBIT */
/* 
 * set orbital elements (ELMST2): for plain text orbit file.
 */
int atSetElement(	/* return value: condition code */
	char *filename,	/* input: path name of the orbital element file */
	double mjd0,	/* input: modified Julian Day */
	int kchk);	/* input: if 0, calc 2nd derivative of mean anom. */

/* 
 * set orbital elements: FITS version, link with "FITSIO" library.
 */
int atSetElement2(	/* return value: condition code */
	char *filename,	/* input: FRF orbital element file name*/
	double mjd0,	/* input: modified Julian Day */
	int kchk);	/* input: if 0, calc 2nd derivative of mean anom. */

/* 
 * calc path number (PATNUM)
 */
int atPathNum(
	double mjd,	/* input: time in MJD */
	char path[11]);	/* output: path name */

/* 
 * calc satellite position (SATPNT) with the earth center as origin
 */
int atSatPos(
	double mjd,	/* input: time in MJD */
	AtVect x);	/* output: vector to the satellite */ 

/* 
 * convert sidereal position to geodetic/geographical position on earth (GEODCR)
 */
int atGeodcr(
	double mjd,	/* input: time in MJD */
	AtVect x,	/* input: vector in equatorial coordinate */ 
	double *heigh,	/* output: altitude from the earth surface */
	double *longi,	/* output: altitude from the earth surface */
	double *latt);	/* output: altitude from the earth surface */
	
int atGeodetic(
	double mjd,	/* input: time in MJD */
	AtVect x,	/* input: vector in sidereal equatorial coordinate */ 
	AtVect y);	/* output: vector in geodetic coordinate at mjd */ 
	
/* 
 * convert polar geodetic coordinate latitude radial distance
 * to the geographic latitude and altitude from the earth surface
 * correcting for the ellipsoidal shape of the earth
 */
int atEllipsoid(
	AtPolarVect *xp, /* input: vector in celestial coordinate */ 
	double *latt,	/* output: latitude on the earth surface */
	double *heigh);	/* output: altitude from the earth surface */

/* 
 * calc rotation matrix for converting equatorial position (J2000)
 *	to the geocentric position on earth 
 */
int atSetGeoRM(
	double mjd,	/* input: time in MJD */
	AtRotMat rm);	/* output: rotation matrix */ 
	
/* 
 * calc sidereal time (KOSEJI)
 */
int atSidereal(
	double mjd,		/* input: time in MJD */
	double *gsttod); 	/* output: Greenwich sidereal time (radian)
				 at mjd true of date*/ 

/* 
 * solve Kepler equation (KEPLER)  g + e sin E = E
 */
int atKepler(
	double g,	/* input: mean anomaly */
	double eccent,	/* input: eccentricity */
	double *e);	/* output: eccentric anomaly */

/* 
 * coordinate conversion from orbital plane to celestial (SATINA)
 */
int atOrbPlane(
	AtVect x,	/* input: vector in orbital plane */ 
	double ol,	/* input: Angle from Ascending Node */
	double ob, 	/* input: Right Ascention of the Ascending Node */
	double ai,	/* input: inclination of the orbit */
	AtVect y);	/* output: vector in celestial coordinate at mjd */ 


/* 
 * convert geographic location of Tracking Station on Earth to Geodetic
 */
int atGeographic(
	AtPolarVect *y,	/* input: tracking station position;
				y.r:altitude from Earth surface 
				y.lon, y.lat: longitude and lattitude */ 
	AtVect z);	/* output: geodetic coordinate of station */ 

/* 
 * Convert from Geodetic to Ground Coordinate at the Tracking Station
 */
int atGroundCoord(
	AtVect station,	/* input: tracking station in geographic coord */
	AtRotMat rm);		/* output: rotation matrix to local coord*/



/* 
 * Set Up geodetic vector to the tracking station and Rotation Matirix
 * to convert sidefloat coord to Ground (Earth-bound) coord
 *       Based on "ADAZEL"
 */

int atAzElSet(
	AtPolarVect *y,	/* input: geographic tracking station position :
				y.r:altitude from Earth surface 
				y.lon, y.lat: longitude and lattitude */ 
	AtVect vStation,	/* output: geodetic coordinate of station */ 
	AtRotMat stationRM); /* output: rotation matrix which converts
				geographic coord to local coord at station*/
/* 
 * Azimuth and Elevation for a vector in Ground (Earth-bound) coord
 */
int atAzEl(
	AtVect x,	    /* input: satellite pos. in geodetic coord. */ 
	AtVect stationV,    /* input: station pos. in geodetic coord. */ 
	AtRotMat stationRM, /* input: rotation matrix which converts geographic
				coord of tracking station to local coord */
	AtPolarVect *y);    /* output: local coordinate:
			    y.r: distance, y.lon:azimuth, y.lat:elevation */


/* 
 * examine earth occultation of specified direction (YEARTH)
 */
int atEarthOccult(
	AtVect satVect,	/* input: satellite pos. in equatorial coord. */ 
	AtVect xVect,	/* input: direction to be examined. */ 
	AtVect sunVect,	/* input: vector to the sun */
	int *flag, 	/* output: condition 0:sky, 1:dark, 2:bright earth */
	double *el);	/* output: elevation angle of z from the earth edge */ 


/* 
 * Set up Cut-off Rigidity Table (RIGSET)
 */ 
int atRigSet(
	char *filename);  /* input: path name of the cut-off rigidity file */

/* 
 * calc Cut-off Rigidity (RIGIDY)
 */
int atRigidity(
	AtPolarVect *x,	/* input: polar vector for geographic position */ 
	float *rig);	/* output: cut-off rigidity (GeV/c) */ 


/* 
 * Set up Table of Geomagnetic Field (GASET)
 */
int atGeomagSet(
	double mjd,	/* input: time in MJD */
	int nmax);	/* input: maximum order of spherical harmonics */

/* 
 * calc Geomagnetic Field (GA)
 */
int atGeomag(
	AtPolarVect *x,	/* input: polar vector for geodetic position */ 
	AtVect xs,	/* input: sidereal vector to the satellite */ 
	AtVect field);	/* output: sidereal direction of field line */ 


/* 
 * calc position of planets, sun and moon (PLANETS, MOON in SOLARSYS)
 */
int atSun(
	double mjd,	/* input: time in MJD */
	AtVect pos);	/* output: vector to the sun in A.U.*/ 

int atPlanet(
	double mjd,	/* input: time in MJD */
	AtVect pos[9],	/* output: vector to the planets and sun (A.U.) */ 
	double size[9],	/* output: their visual size (radian)*/
	double mag[9]);	/* output: their visual magnitude*/

/* 
 * calc position of the moon from the earth center in TOD coordinate.
 */
int atMoon(
	double mjd,	/* input: time in MJD */
	AtVect pos,	/* output: vector to the moon (km) */ 
	double *size,	/* output: their visual size (radian)*/
	double *phase,	/* output: phase (radian 0:new, pi:full)*/
	double *distan);/* output: distance to moon (km)*/


/* 
 * calc if the point is in the "Brazil Anomaly" (SAA)
 */
int atBrazil(
	double lon,	/* input: longitude in radian */
	double lat,	/* input: latitude in radian */
	int *flag);	/* output: =1 if in SAA, =0 if outside */ 


/************************************************************************/
/*									*/
/*	New for Astro-D 						*/
/*									*/
/************************************************************************/

/*
 * convert geocentric vector to Barycentric
 */
int atBarycentric(
	double mjd,	/* input: time in MJD */
	AtVect x,	/* input: vector in geocentric coordinate */ 
	AtVect y);	/* output: vector in barycentric coordinate at mjd */ 

/*
 * convert MJD to TDT (terrestial dynamical time)
 */
int atTDT(
	double mjd,	/* input: time in MJD */
	double *tdt);	/* output: time in TDT */ 

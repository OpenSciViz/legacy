#include "atFunctions.h"
#include "atError.h"
#include <math.h>

/* 
 * convert polar geodetic coordinate latitude radial distance
 * to the geographic latitude and altitude from the earth surface
 * correcting for the ellipsoidal shape of the earth
 */
int atEllipsoid(
	AtPolarVect *xp, /* input: vector in celestial coordinate */ 
	double *latt,	/* output: latitude on the earth surface */
	double *heigh)	/* output: altitude from the earth surface */
	
{
    /* Initialized data */
    static double eps = 1e-7;
    static int nrmax = 5;

    /* Local variables */
    double beta, gamm, sing, latt2, rc, s, d__1;
    int nr;

    /* RS,DS -) HEIGH, LATT  (DISTORTIION OF THE EARTH) */
    nr = 0;
    s = 1e99;
    *latt = xp->lat;
    while(nr != nrmax && s > eps) {
	d__1 = cos(*latt);
	d__1 = 1./ (1. - EARTH_E2 * (d__1 * d__1));
	rc = EARTH_RADIUS * sqrt((1. - EARTH_E2)*d__1);
	beta = *latt+EARTH_E2*sin(*latt*2)*d__1/2;
	gamm = beta - *latt;
	sing = sin(gamm);
	d__1 = rc * sing;
	*heigh = sqrt(xp->r * xp->r - d__1 * d__1) - rc * cos(gamm);
	latt2 = xp->lat - asin(*heigh * sing / xp->r);
	s = fabs(latt2 - *latt);
	*latt = latt2;
	++nr;
    }
    return 0;
}
#include "atFunctions.h"
#include "atError.h"
#include <math.h>

/* 
 * Normalizing a vector  	ver 1.0  92/07/01  ay
 *             			ver 1.5  93/01/26  n.kawai    
 */
 
int atNormVect(
	   AtVect x,       /* input:  vector */	 
	   AtVect y)       /* output: normalized vector*/ 
{ 
  double nrm; 
  int    k1;
  if( (nrm=x[0]*x[0]+x[1]*x[1]+x[2]*x[2]) == 0.0 ) { 
    for (k1=0; k1<3;k1++) { y[k1]=0.0; } 
    return (NULL_VECTOR);
    } 
  else if (nrm == 1.0) { 
    nrm = sqrt( nrm ); 
    for (k1=0; k1<3;k1++) { y[k1]=x[k1]; } 
    }
  else { 
    nrm = sqrt( nrm ); 
    for (k1=0; k1<3;k1++) { y[k1]=x[k1]/nrm; } 
    }
  return (NORMAL_END);
}







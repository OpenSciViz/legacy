#include "atFunctions.h"
#include "atError.h"
#include <math.h>

/*                                                                     */
/*     Originally coded in fortran (MJULIA) by K.Mitsuda.              */
/*     Translated in C and revised by A.Yoshida for ASTRO-D.           */
/*                                                                     */
int               /* Converting UT to Modified Julian Day (MJULIA)     */
atMJulian(        /*                            ver 1.0  92/07/02  ay  */
        AtTime *time,          /* input:  yr,mo,dy,hr,mn,sc,ms */
        double *mjd)           /* output: Modified Julian Day */
     /* Note: Day of the week is given by ( (mjd+3)%7 )  (0=SUN, 1=MON, etc) */
{
  double  y,m,d;
  if (time->mo > 2) { y = time->yr;      m = time->mo; }
  else              { y = time->yr - 1;  m = time->mo + 12; }
  if (time->yr < 10)       { y += 2000.0; }
  else if (time->yr < 100) { y += 1900.0; }
  d = time->dy + ( time->hr + ( time->mn + ( time->sc + time->ms/1000.0 )
                     /60.0 ) /60.0 ) /24.0;
  *mjd = ( floor(y*365.25) - 678912.0 )  /* <- should be calculated first */   
            + floor(y/400.0) - floor(y/100.0)
                                + floor((m-2.0)*30.59) + d ;
  return (NORMAL_END);
}






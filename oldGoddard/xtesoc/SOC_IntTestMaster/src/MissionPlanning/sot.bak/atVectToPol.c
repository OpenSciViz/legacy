#include "atFunctions.h"
#include "atError.h"
#include <math.h>

int           /* Converting the coordinate from Cartesian to Polar (RECPOL) */
atVectToPol(  /*                                    ver 1.0  92/07/01  ay   */
        AtVect x,              /* input */
        AtPolarVect *y)        /* output: result */
{
  double  norm01, c, s;
  norm01 = x[0]*x[0] + x[1]*x[1];
  if ( (y->r = sqrt( norm01 + x[2]*x[2] )) == 0.0 ) { 
    y->lon = y->lat = 0.0;
    return (NULL_VECTOR); 
    }
  norm01 = sqrt( norm01 );
  y->lat = asin( x[2]/y->r );
  c = x[0]/norm01;  s = x[1]/norm01;
  if (norm01 < EPS) { y->lon = 0.0; }
  else if ( fabs( s ) < EPS ) { y->lon = (1.0 - c/fabs(c))*PI/2.0; }
  else { y->lon = atan((1.0-c)/s)*2.0; }
  while( y->lon >= 2.*PI ) { y->lon -= 2.*PI; }
  while( y->lon < 0.0 )    { y->lon += 2.*PI; }
  return (NORMAL_END);
}







#include <stdio.h>
#include <math.h>
#include "atFunctions.h"
#include "atError.h"

#define MJD1950 33282.0
#define MJD2000 51545.0

double rad=57.29577951;

/* calculates ra and dec of sun for mjd, returns as a 3-vect */

int atSun_new(double mjd, AtVect pos)
{
  /* taken from Christina William's version of Frank Marshall's version
     of the 1994 nautical almanac page c24, accurate to 0.01 degrees
     from 1950 to 2050 */

  AtPolarVect y50P;
  double l, g, elon, obl, snl, jd, sunra, sundec;

  /* we use a varient form of MJD */
  jd = mjd - 51544.5;

  /* l is mean longitude of the sun, corrected for aberration */
  l = 280.466 + 0.9856474 * jd;
  while (l > 360.0)
    l = l - 360.0;
  while (l < 0.0)
    l = l + 360.0;

  /* g is mean anomaly */
  g = 357.528 + 0.9856003 * jd;
  while (g > 360.0)
    g = g - 360.0;
  while (g < 0.0)
    g = g + 360.0;

  /* elon is ecliptic longitude */
  elon = 1.0 + sin(g/rad) * 1.915 + sin(2.0*g/rad) * 0.02;

  /* obl is obliquity of the ecliptic */
  obl = 23.440 - (0.0000007 * jd) ;
  snl = sin(elon/rad);
  sunra = rad * atan2(snl * cos(obl/rad), cos(elon/rad));

  if (sunra < 0.0)
    sunra = sunra + 360.0;

  sundec = rad * asin( sin(obl/rad) * snl); /* dec in degrees */

  y50P.lon = sunra/rad; /* convert back to radians (yes, it's inefficient) */
  y50P.lat = sundec/rad; /* convert back to radians (yes, it's inefficient) */
  y50P.r = 60.0;
  atPolToVect(&y50P,pos);

}


/* 
 * calc position of the sun in 1950 equinox
 * Modified 5/23/95 by Antunes to calculate for equinox 2000 * /
 */
int atSun(
	double mjd,	/* input: time in MJD */
	AtVect pos)	/* output: vector to the sun in A.U.*/ 
{
    double l, m, r, t;
    AtVect oldpos;

    t = mjd - 4.5e4;
    m = t * .985600267 + 27.26464;
    m = fmod(m, 360.) * DEG2RAD;
    l = t * .985609104 + 309.44862 + sin(m) * 1.91553 + sin(m * 2) * .0201;
    r = 1.00014 - cos(m) * .01672 - sin(m * 2) * 1.4e-4;
    l = fmod(l, 360.) * DEG2RAD;
    pos[0] = r * cos(l);
    pos[1] = r * .91744 * sin(l);
    pos[2] = r * .39788 * sin(l);

/*    atPrecession(MJD1950,oldpos,MJD2000,pos); */
/*    atPrecession(MJD1950,oldpos,mjd,pos); */

    return 0;
}

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "atFunctions.h"
#include "atError.h"

main(int argc, char *argv[] )
{
  int parseme(char*, char*, char*, int*, int*, int*), revmonth(char,char,char);
  double mjd, tmjd, sra, sdec, dlon, dlat;
  double goodtime, badtime, eff, eff2, ontime;
  char ras[9],decs[9], daily[3], c1[10],c2[10],c3[10], myname[200];

  double lontable[11],lattable[11];

  int num_saa, i, ii, j, k, isaa, bsun, inum, tskip, itally, idaily, qq;
  int istep, num_months, igotdate, ineedcoords, ephem_type, ibin;

  double elvy, ecorn, snangle;
  int iocc;
  static AtPolarVect y50P={1.,30.,60.};
  AtVect vSat, vSun, y50, y50t;

  AtVect vFov, vTarget2000;
  AtPolarVect pvSun;
  AtEulerAng eulerAng;
  AtRotMat eulerRM, eulerInvRM;

  FILE *ifp;
  char ephemname[200], line[200];
  AtVect gSat;
  AtPolarVect gSatP;
  double latt, height, tlon, tlat, tx, ty, tz;
  AtTime times;

  ephem_type = 0; /* hard-coded to be using yearly 10 minute spacing */

  printf(
"SOT v3.0: Suitability of Target, an XTE efficiency calculator.\n");

  qq = argc; igotdate = 0; idaily = (-1); ineedcoords = 0;

  if (qq < 2) /* no arguements given */
    {
      printf("  Usage:\n");
  printf(
"  sot DD MM YY -e EPHEM.out -ra RA2000 -dec Dec2000 -n months -id name\n");
    }

  num_months = 12; /* default-coding to do 6 months or end-of-ephem */
  strcpy(ephemname,"\0");
  strcpy(myname,"\0");

  for (i = 0; i < qq; ++i)
    {
      if ( (strncmp(argv[i],"-n\0",2) == 0) && i+1 < qq) /* num months */
	sscanf(argv[i+1],"%d",&num_months);
      else if ( (strncmp(argv[i],"-e\0",2) == 0) && i+1 < qq) /* ephem name */
	strcpy(ephemname,argv[i+1]);
      else if ( (strncmp(argv[i],"-id\0",3) == 0) && i+1 < qq)
	strcpy(myname,argv[i+1]);  /* optional target name */
      else if ( (strncmp(argv[i],"-r\0",2) == 0) && i+1 < qq) /* RA */
	{
	  ++ineedcoords;
	  strcpy(ras,argv[i+1]);
	  sscanf(ras,"%lf",&sra);
	}
      else if ( (strncmp(argv[i],"-d\0",2) == 0) && i+1 < qq) /* Dec */
	{
	  ++ineedcoords;
	  strcpy(decs,argv[i+1]);
	  sscanf(decs,"%lf",&sdec);
	}
    }

  for (i = 0; i < qq - 2; ++i)
    if (strncmp(argv[i],"test\0",2) != 0 && strncmp(argv[i],"-e\0",2) != 0
	&& isdigit(argv[i][0]))
	/* there are at least 3 elements left, and we've found a number */
      {
	qq = i; /* so we only call it once */
	strcpy(c1,argv[qq]); strcpy(c2,argv[qq+1]); strcpy(c3,argv[qq+2]);
	sscanf(c1,"%d",&ii);
	times.dy = ii;
	sscanf(c3,"%d",&ii);
	if (ii > 1900)ii=ii-1900;
	times.yr = ii;
	sscanf(c2,"%d",&ii);
	if (ii <= 0 || ii > 12)
	  ii=revmonth(c2[0],c2[1],c2[2]);
	times.mo = ii;
	igotdate = 1;
      }

/* fuck */

/* get tmjd, sra, sdec, and ephemname from user */

  if (igotdate == 0)
    {
      printf("Please enter DD MM YY: ");
      scanf("%d %d %d",&times.dy, &times.mo, &times.yr);
    }

  if (strlen(ephemname) == 0) /* no ephem */
    {
      printf(
	  "Please enter processed ephem name (i.e. X1996032X6MON.01.out): ");
      scanf("%s",ephemname);
    }

  if (ineedcoords < 2)
    {
      printf("Please enter target RA (J2000 decimal degrees only): ");
      scanf("%s",ras);
      sscanf(ras,"%lf",&sra);
 
      printf("Please enter target Dec (J2000 decimal degrees only): ");
      scanf("%s",decs);
      sscanf(decs,"%lf",&sdec);
    }

    printf(
"Weekly efficiency, %d months from %2d/%2d/%2d: (%8.4lf,%8.4lf) %s\n",
	   num_months, times.dy, times.mo, times.yr, sra, sdec, myname);

  times.hr = 0; times.mn = 0; times.sc = 0; times.ms = 0.0;
  atMJulian(&times,&tmjd);

/* open ephem */
  if ( (ifp=fopen(ephemname,"r")) == NULL)
    {    
      printf("Error opening ephemeris, exiting\n");
      exit;
    }
  fscanf(ifp,"%s %d",line,&inum);
  
  lontable[ 1]= (-90.0); lattable[ 1] = (-30.0);
  lontable[ 2]= (-90.0); lattable[ 1] = (  5.0);
  lontable[ 3]= (-15.0); lattable[ 1] = (  5.0);
  lontable[ 4]= (-15.0); lattable[ 1] = (  0.0);
  lontable[ 5]= (  5.0); lattable[ 1] = (  0.0);
  lontable[ 6]= (  5.0); lattable[ 1] = (-10.0);
  lontable[ 7]= ( 20.0); lattable[ 1] = (-10.0);
  lontable[ 8]= ( 20.0); lattable[ 1] = (-20.0);
  lontable[ 9]= ( 35.0); lattable[ 1] = (-20.0);
  lontable[10]= ( 35.0); lattable[ 1] = (-30.0);
  num_saa = 11;

  goodtime = 0.0; badtime = 0.0; ontime = 0.0;

  printf(
      "(wait while we find the first ephem point.\n");
  do {
    fscanf(ifp,"%d %d %d %d %d %d %lf %lf %lf %lf %lf",
	   &times.yr, &times.mo, &times.dy, &times.hr, &times.mn, &times.sc,
	   &tx, &ty, &tz, &tlon, &tlat);
    --inum;
    atMJulian(&times,&mjd);
  } while  (inum > 0 && mjd < tmjd);

  itally = 0;

  if (ephem_type == 0)
    {
      istep = 1; /* yearly ephems have 10 minute spacing */
      ibin = 144;
    }
  else
    {
      istep = 10; /* other ephems have 1 minute spacing */
      ibin = 1440;
    }

  for (i=0; i< (ibin/istep)*30*num_months; ++i) /* 1440/istep is one day */
    {
      for (k=0; k<istep; ++k) /* do in <istep> minute hops */
	{
	  fscanf(ifp,"%d %d %d %d %d %d %lf %lf %lf %lf %lf",
		 &times.yr, &times.mo, &times.dy, &times.hr, 
		 &times.mn, &times.sc, &tx, &ty, &tz, &tlon, &tlat);
	  --inum;
	}

      atMJulian(&times,&mjd);
      ++itally;
      
      if (inum == 0) /* oops, ran out of ephem */
	i = (ibin/istep)*30*num_months; /* force an exit */

      vSat[0] = tx * 10000.0; vSat[1] = ty * 10000.0; vSat[2] = tz * 10000.0;
      atGeodetic(mjd,vSat,gSat);
      atVectToPol(gSat,&gSatP);
      atEllipsoid(&gSatP,&latt,&height);
      gSatP.lat = latt;
      dlat = gSatP.lat*RAD2DEG;
      dlon = gSatP.lon*RAD2DEG;

      /* figure out occultation */

      y50P.r = 1.0;
      y50P.lon = sra * DEG2RAD;
      y50P.lat = sdec * DEG2RAD;

      atPolToVect(&y50P, y50t);
      atPrecession(MJD_J2000,y50t,mjd,y50);

      atSun(mjd, vSun);
      atEarthOccult2(vSat, y50, vSun, &iocc, &elvy, &ecorn); /* iocc is occ */
      if (iocc == 0 && elvy < 3.0*DEG2RAD)
	iocc = 3; /* harsher criteria for earth occult */
      if (iocc == 0 && ecorn*RAD2DEG < 71.0)
	iocc = 3; /* even harsher criteria for earth occult */
      if (iocc != 0)
	iocc = 1; /* non-zero is occulted */

      /* figure out Sun angle */
      vFov[0] = 0.0; vFov[1] = 0.0; vFov[2] = 1.0;
      atVectToPol(vSun,&pvSun);
      atSetEuler(&y50P,&pvSun,&eulerAng);
      atEulerToRM(&eulerAng,eulerRM);
      atInvRotMat(eulerRM,eulerInvRM);
      atRotVect(eulerInvRM,vFov,vTarget2000);
      atAngDistance(vSun,vTarget2000,&snangle);
      snangle = snangle * RAD2DEG;
      if (snangle < 35.0)
	bsun = 1; /* bad sun angle */
      else
	bsun = 0; /* good sun angle */

      /* figure out SAA given dlon, dlat (in Deg) */

      if (dlon >= 180.) dlon -= 360.;
      isaa=0; /* assume it's not in, first, then set to 1 if it is */
  
      /* might be in SAA */
      for (j=1;j<num_saa;++j)
	if (dlon > lontable[j] && dlon < lontable[j+1] && 
	    dlat < lattable[j+1] && dlat > -30)
	  isaa=1;

      if (bsun == 1 || isaa == 1 || iocc == 1) /* bad sun/in saa/occulted */
	badtime += 1.0;
      else
	goodtime += 1.0;

      if (iocc == 0 && bsun == 0) /* non-occulted, good sun (theor max) */
	ontime += 1.0;

      if (itally == ibin/istep*7) /* 288 bins * 5 min/bin = 1 day */
	{
	  eff = 100.0 * (goodtime / (goodtime + badtime));
	  eff2 = 100.0 * (ontime / (goodtime + badtime));

	    printf(
"week ending %2d/%2d/%2d: efficiency =%5.1lf (%5.1lf non-occ), sun angle is %3.1lf %s\n",
		   times.dy,times.mo,times.yr, eff, eff2, snangle, myname);

	  goodtime = 0.0; badtime = 0.0; ontime = 0.0; /* zero & begin again */
	  itally = 0;
	}
    }

  fclose(ifp);
  printf("\nDone\n");

}

#include "atFunctions.h"
#include "atError.h"

int                     /* Making a vector prodact          */
atVectProd(             /*           ver 1.0  92/07/01  ay  */
        AtVect x,       /* input */
        AtVect y,       /* input */
        AtVect z)       /* output: vector (outer) product of x and y */
{
  z[0] = x[1]*y[2] - x[2]*y[1];
  z[1] = x[2]*y[0] - x[0]*y[2];
  z[2] = x[0]*y[1] - x[1]*y[0];
  return (NORMAL_END);
}


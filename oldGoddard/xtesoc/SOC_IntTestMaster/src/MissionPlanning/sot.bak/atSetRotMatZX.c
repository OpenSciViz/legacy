#include "atFunctions.h"

/* 
 * ROTATION MATRIX defined by New Z-axis and a vector in (+)X-Z half plane.
 */
int atSetRotMatZX(
	AtVect zAxis,	/* input: vector defining new z-axis */
	AtVect xAxis,	/* input: vector in new +X-Z half plane */
	AtRotMat rm)	/* output: rotation matrix */
{
    AtVect x, y, z, yAxis;
    int i, code;
    
    atVectProd(zAxis, xAxis, yAxis);
    if ((code = atNormVect(zAxis, z))!=0) return code;
    if ((code = atNormVect(yAxis, y))!=0) return code;
    atVectProd(y, z, x);
    
    for (i=0; i<3; i++) {
	rm[0][i] = x[i];
	rm[1][i] = y[i];
	rm[2][i] = z[i];
    };
    return 0;
}    
#include "atFunctions.h"
#include "atError.h"

int                         /* Making an Inverse Rotation Matrix     */
atInvRotMat(                /*                 ver 1.0  92/07/01  ay */
	    AtRotMat rm,    /* input: rotation matrix */
	    AtRotMat rm2)   /* output: inversed rotation matrix */
{ 
  int   i1, i2;
  for (i1=0; i1<3; i1++) {
    for (i2=0; i2<3; i2++) { rm2[i1][i2] = rm[i2][i1]; }
    }
  return (NORMAL_END);
}


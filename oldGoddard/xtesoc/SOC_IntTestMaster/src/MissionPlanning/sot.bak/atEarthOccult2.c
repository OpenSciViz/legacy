#include <math.h>
#include "atFunctions.h"
#include "atError.h"

/* 
 * examine earth occultation of specified direction (YEARTH)
 */
int atEarthOccult2(
	AtVect satVect,	/* input: satellite pos. in equatorial coord. */ 
	AtVect xVect,	/* input: direction to be examined. (normalized) */ 
	AtVect sunVect,	/* input: vector to the sun */
	int *flag, 	/* output: condition 0:sky, 1:dark, 2:bright earth */
	double *el,	/* output: elevation angle of z from the earth edge */ 
        double *ecorn)  /* output: elevation of the FOV from earth center */
{
    double earthSize, xDist, dot, zCross, satDistance;
    AtRotMat rm;
    AtVect satV, earthVect, xV;
    
    atInvVect(satVect, earthVect);
    satDistance = atNorm(earthVect);
    earthSize = asin(EARTH_RADIUS / satDistance);
    atAngDistance(xVect, earthVect, &xDist);
    *el = xDist - earthSize;
    *ecorn = xDist;
    
    if (*el <= - EPS) {
	atSetRotMatZX(sunVect, satVect, rm);
	atRotVect(rm, satVect, satV);
	atRotVect(rm, xVect, xV);
	dot = atScalProd(earthVect, xVect);
	zCross = satV[2] + xV[2]* (dot 
		    - sqrt( EARTH_RADIUS * EARTH_RADIUS 
		    - satDistance * satDistance + dot * dot));
	if (zCross < 0.) {
	    *flag = 1;		/* Dark Earth */
	} else {
	    *flag = 2;		/* Bright Earth */
	}
    } else {
	*flag = 0;
    }
    return 0;
} 

/* this takes a month, and returns it as a number */

#include <stdio.h>
#include <ctype.h>
#include <string.h>

int revmonth(char c1,char c2,char c3)
{
  void monthcon(char[],int);
  int i;
  char c[4];
  c[0] = toupper(c1);
  c[1] = toupper(c2);
  c[2] = toupper(c3);
  c[3]='\0';
  if (!strcmp(c,"JAN"))i=1;
  else if (!strcmp(c,"FEB"))i=2;
  else if (!strcmp(c,"MAR"))i=3;
  else if (!strcmp(c,"APR"))i=4;
  else if (!strcmp(c,"MAY"))i=5;
  else if (!strcmp(c,"JUN"))i=6;
  else if (!strcmp(c,"JUL"))i=7;
  else if (!strcmp(c,"AUG"))i=8;
  else if (!strcmp(c,"SEP"))i=9;
  else if (!strcmp(c,"OCT"))i=10;
  else if (!strcmp(c,"NOV"))i=11;
  else if (!strcmp(c,"DEC"))i=12;
  else
    i = 0;
  return(i);
}

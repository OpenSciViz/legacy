#include "atFunctions.h"
#include "atError.h"
                                         
/* 
 * ROTATE A VECTOR WITH ROTATION MATRIX.
 */

int atRotVect(
	AtRotMat rm,	/* input: rotation matrix */
	AtVect x,	/* input: vector */
	AtVect y)	/* output: vector */
{
    int i,j;
    
    for (i=0; i<3; i++) {
    	y[i] = 0;
	for (j=0; j<3; j++) {
	    y[i] += rm[i][j]*x[j];
	};
    };
    return (NORMAL_END);
}

head	1.1;
access
	dhon
	mxb
	soccm;
symbols;
locks; strict;
comment	@# @;


1.1
date	95.03.28.21.57.21;	author mxb;	state Exp;
branches;
next	;


desc
@@


1.1
log
@Initial revision
@
text
@.\" ==========================================================================
.\" File Name   : ACSMMclient.1
.\" Subsystem   : Mission Monitoring
.\" Programmer  : Mihai Berdan, Hughes STX
.\" Description :
.\"
.\"   Man page for the dmIndex process.
.\"
.\" RCS: $Id:$
.\"
.\" ==========================================================================
.TH ACSMMclient 1 "March 28 1995" "Mission Monitoring"
.SH NAME
ACSMMclient \- Receives apid 14 packets from Data Ingest, creates
ACS Configurations, compares the observed connfigurations against
the predicted ones and sends them via socket to MMGui (MMclient)
.\" ==========================================================================
.SH SYNOPSIS 1 (Production Mode)
.\" ==========================================================================
.B ACSMMclient
.RB [-read-from-stdin]
.RB [-MMclient
.IR <HostName
.IR where
.IR MMclient
.IR is
.IR running>]
.RB [-debug
.IR to
.IR display
.IR debugging
.IR info]
.RB [-compare
.IR <0>
.IR not
.IR to
.IR compare
.IR against
.IR predicted
.IR timeline,
.IR <1> 
.IR to
.IR compare
.IR against
.IR predicted
.IR timeline]
.RB [-timeout 
.IR <xxx>
.IR (msec)
.IR sets
.IR the
.IR time
.IR interval
.IR to
.IR send
.IR configurations]
.RB <
.RB <filename>
.\" ==========================================================================
.SH SYNOPSIS 2 (Real Time)
.\" ==========================================================================
.B ACSMMclient
.RB [-port
.IR <port
.IR number
.IR (5094)>]
.RB [-host
.IR <HostName
.IR where
.IR RTserver
.IR is
.IR running>]
.RB [-name
.IR ACS0-7]
.RB [-MMclient
.IR <HostName
.IR where
.IR MMclient
.IR is
.IR running>]
.RB [-debug
.IR to
.IR display
.IR debugging
.IR info]
.RB [-compare
.IR <0>
.IR not
.IR to
.IR compare
.IR against
.IR predicted
.IR timeline,
.IR <1> 
.IR to
.IR compare
.IR against
.IR predicted
.IR timeline]
.\" ==========================================================================
.SH DESCRIPTION
.\" ==========================================================================
.B ACSMMclient
Uses as input telemetry packets and generates Measured Configurations.
The telemetry packets are obtained from a file (Production Mode) or
via a socket stream (Real Time). The output of the process is used
by the Mission Monitoring processes (MMclient - Production Mode or
MMGui - Real Time) to monitor the mission.
.\" ==========================================================================
.SH OPTIONS - General
.\" ==========================================================================
.TP
.\" ------------------------------------
.B -compare
.\" ------------------------------------
.br
This option is in general used for development only. In real life one shouldn't use it.
By default the process will set -compare to 1.
.TP
.\" ------------------------------------
.B -timeout
.\" ------------------------------------
.br
Is also used during the development only. This will allow to the process while processing
packets from a file to behave as a real time process (from a MMGui point of view).
.TP
.\" ------------------------------------
.B -MMclient
.\" ------------------------------------
.br
Spec. the host where the monitoring process is running.
.TP
.\" ------------------------------------
.B -host
.\" ------------------------------------
.br
Spec. the host where the RTserver process is running.
.TP
.\" ------------------------------------
.B -port
.\" ------------------------------------
.br
Spec. the port number where the connection with the RTserver is expected to be established.
In general this value is 5094.
.\" ==========================================================================
.SH ENVIRONMENT
.\" ==========================================================================
.TP
.B $SOCHOME
Is expected to be set before running the program. If this is not set correctly,
some of the instrument libraries (such as hexte) will force the program to exit.
Also, this variable will be used by the command generation libraries to compute
the path for the predicted config. time line.
This process will not produce or use any other files otherwise.
.\" ==========================================================================
.SH SEE ALSO
.\" ==========================================================================
.IR "Mission Monitoring design document" ,
.br
.BR MMclient (1),
.BR MMGui (1),

@

head	2.2;
access
	paritosh
	soccm;
symbols
	Build3:2.2
	Build2:2.1;
locks; strict;
comment	@# @;


2.2
date	94.06.09.19.33.44;	author paritosh;	state Exp;
branches;
next	2.1;

2.1
date	93.11.01.20.08.28;	author paritosh;	state Release;
branches;
next	;


desc
@Creation
@


2.2
log
@*** empty log message ***
@
text
@'\"t
.\"
.\"  $Id: rtclient.1,v 1.0 1993/09/30 13:24:55 paritosh Exp $
.\"
.TH MISSION-MONITORING 1
.SH NAME
MM-RTclient \- receives telemetry packets from realtime server.
.SH SYNOPSIS
\fBMM-RTclient\fP [-daemonize]  [-port #] [-host
.I hostname
] 
[-name 
.I client-name
]
.br
.SH DESCRIPTION
This is a daemon, normally started at system boot time, whose
sole purpose is to receive packets from Realtime Server which receives them from Realtime Ingest.  
It has a well-known Transmission Control Protocol
.I (TCP)
port over which it requests
connection from realtime server and receives packet in which
it is interested.
This process is known as the Realtime Client.
.PP
The Realtime Server passes packets along by application ID (ApID).
That is to say, this realtime client process informs the Realtime Server which ApIDs it is interested in.  Then each time the Realtime Server receives a
telemetry packet of that ApID, it passes it to this client. A 
.I MM-RTclient 
can get packets from more than one ApID.
.PP
There also exists a mechanism by which 
.I MM-RTclient 
can specify
certain types of Spacecraft data in which it is interested.
These Spacecraft data types are translated to the respective ApID, so
that the Realtime Server deals strictly with ApIDs.  The types of
Spacecraft data supported and how to use them is detailed in the Interface
Control Documents (ICD) with each of the Instrument Teams (IT).
.PP
.I MM-RTclient 
informs the Realtime Server which ApIDs and
Spacecraft data types it is interested in by providing the XTESOC a
one-line table entry that the Realtime Server reads on startup. 
The XTESOC puts each of these entries into a table entitled
.I RT.CLIENTS
that Realtime Server expects to read from $SOCHOME/etc/RT.CLIENTS.
Entries in the table take the form:
.nf

ClientName   ListOfApIDs and/or SCIDs 

.fi
.I ClientName
is a mnemonic name for the realtime client.  When 
.I MM-RTclient
connects to the Realtime Server it passes its name to the Realtime
Server which verifies that it is one of the names of valid clients in the
RT.CLIENTS table. 
.I ListOfApIDs and/or SCIDs
is a list of all the ApIDs and/or Spacecraft data types 
.I MM-RTclient
wants to receive.
The ApIDs in this list, as well as each of the fields in the
line, must be separated from one another by spaces, tabs or commas.
Further details of RT.CLIENTS and Server process are given in their respective man pages.

.PP
For security reasons every machine that
receives XTE Telemetry, which includes
.I MM-RTclient
, must be
authorized by the XTESOC to do so. 
The on-disk table of machines authorized to connect to Realtime Ingest
and the Realtime Server or to receive XTE telemetry from the Realtime
Server is the file
.I $SOCHOME/etc/AUTH.HOSTS
.SH SWITCHES
.TP 10
.B -daemonize
This instructs 
.I MM-RTclient 
to run as a daemon.  This option is
expected to be enabled during normal operation; that is to say, 
.I MM-RTclient 
will normally be running as a daemon.
.TP
.B "-port #"
This option specifies the well-known TCP port in the Realtime Server
process to which 
.I MM-RTclient 
must connect in order to receive
telemetry packets from Realtime Server.
That is to say, this specifies the private communication channel
between Realtime Server and 
.I MM-RTclient.
.TP
.B "-name name"
This option specifies the name we advertise to the Realtime server. It is same as the entry in the relevant $SOCHOME/etc/RT.CLIENTS table. In the future versions this will be hard-coded in the program.
.TP
.B "-host machine"
this option specifies the name of the machine on which Realtime Server
is running.
.SH "ENVIRONMENT VARIABLES"
.IP SOCHOME
This is the root directory of the XTESOC Integration and Test
directory.
There should be a subdirectory
.I etc
below this directory.  The
.I etc
directory contains the file of authorized hosts and realtime clients.
.SH FILES
.PD 0
.TP 30
.B $SOCHOME/etc/RT.CLIENTS
table mapping realtime clients to their ApIDs and Spacecraft data types
.TP
.B $SOCHOME/etc/AUTH.HOSTS
machines authorized to connect to Realtime Ingest and the Realtime Server.
.TP
.PD
.SH BUGS
There are no known bugs.
.fi
.SH SEE ALSO
.BR rtingest (1)  rtserver (1)
.SH AUTHORS
Paritosh Malaviya (paritosh@@rosserv.gsfc.nasa.gov)
Mike Lijewski (lijewski@@rosserv.gsfc.nasa.gov)
@


2.1
log
@Creation
@
text
@d1 1
d102 1
a102 1
This option specifies the name of the machine on which Realtime Server
d129 2
a130 2
Mike Lijewski (lijewski@@xtesoc2.stx.com, lijewski@@rosserv.gsfc.nasa.gov)
Paritosh Malaviya (paritosh@@xyster.stx.com, paritosh@@rosserv.gsfc.nasa.gov)
@

#!/usr/local/bin/blt_wish -f
##!! CHECK THE VARS USED TO SET LIMITS AND SCROLL!!

# blt_wish script to display downlink packets
# and configuration state of EDS EAs
# $Id: EdsMMDisplay.tcl,v 1.15 1995/10/25 16:08:41 ehm Exp $

# TBD: check for error status starting rt_client
# check to see if we have the new env variable set:
# XTE_TCL_LIBRARY; if not, default it to $SOCHOME/lib/tcl at the very least
# try to lok in the dir in which we executed
if {[file dirname $argv0 ] != "."} {
    set xpath $argv0
    set path [file dirname $xpath]
    puts " xxxx $path"
} else {
    set path [pwd]
    puts "<dot>  $path"
}

set tcl_precision 17
#set auto_path [linsert $auto_path 0 $env(SOCHOME)/lib/tcl ]
set auto_path [linsert $auto_path 0 $env(SOCHOME)/bin/$env(ARCH) ]
set auto_path [linsert $auto_path 0 $path ]

if [info exists env(XTE_TCL_LIBRARY) ]  {
    [linsert $auto_path 0 $env(XTE_TCL_LIBRARY)  ]
} 

# global variables and top level widgets
frame .top
frame .bot
#frame .f
set inputFile -1;
global ltext;
set iSource rtserver
set dataNAME EDS-HIGH


set rtClient "EdsMMclient "

#initialize global label vars
foreach ea {0 1 2 3 4 5 6 7} {
    set config$ea ??
    set configName$ea ??
    set count$ea ??
    set runState($ea) ??
}

# set the maximum number of items that we are keeping here...
set pointsToDisplay 100
# number of points to display
# note that each point occurs at a 64 second interval

set pointsToKeep 1000
# number of points to keep around

global xmax; set xmax [expr $pointsToDisplay*64]
set xmin 0;





#max value of x axis

#define a var  to hold the total number of data points we keep.

#variable to keep track of which downlink packets we'll get
set counts(1) 0; set  counts(2) 0; set counts(3) 0;set  counts(4) 0;
set counts(5) 0; set counts(6) 0; set counts(7) 0; set  counts(0) 0;

set baseColor [lindex [. configure -background ] 4]
# default background color - use to reset labels

# arrays to hold 
#the data
set EA_DATA(2) ""
set EA_DATAx(0) {}
set EA_DATAx(1) {}
set EA_DATAx(2) {}
set EA_DATAx(3) {}
set EA_DATAx(4) {}
set EA_DATAx(5) {}
set EA_DATAx(6) {}
set EA_DATAx(7) {}

set EA_DATAy(0) {}
set EA_DATAy(1) {}
set EA_DATAy(2) {}
set EA_DATAy(3) {}
set EA_DATAy(4) {}
set EA_DATAy(5) {}
set EA_DATAy(6) {}
set EA_DATAy(7) {}

proc formatLabels {graph value} {
    set secsInDay [expr 60*60*24]
    set secsInHour [expr 60*60]
    set day [expr int($value/$secsInDay)]
    set hour [expr int(($value-$day*$secsInDay)/3600.0)]
    set min [expr int(($value-($day*$secsInDay+$hour*$secsInHour))/60.0)]
    $graph xaxis configure -title "UT Day $day"
    set foo [expr ($day*$secsInDay+$hour*$secsInHour)]
    return " ${hour}h ${min}m "
}
proc processInput {ifile event} {
    #    global ltext;
    global counts config
    global pointsToKeep pointsToDisplay
    global xmax xmin EA_DATA EA_DATAx EA_DATAy;
    global runState
    foreach var {0 1 2 3 4 5 6 7} {
        global config$var
        global configName$var
        global count$var
    }

    set line [gets $ifile];
    puts $line
    if [regexp "EDS_EA" $line ] {
        regexp  {EA[0-9]} $line eaMatch
        #     puts $eaMatch
        set ea [string range $eaMatch 2 end];
        set graph .ff[expr $ea].ea[expr $ea]
        set scroll .ff[expr $ea].sb$ea   
        set lineList [split $line ]
        set foo [lindex $lineList 3]
        if {  $foo == 0xffffffff } {
        } else {
            set config$ea $foo
            set configName$ea [lindex $lineList 4]
        }
        set runState($ea) [lindex $lineList 6]

        if { [llength $lineList] == 8 } {
            set yval [ lindex $lineList 7]	
            set metVal [lindex $lineList 1]
            
            if {$metVal >= $counts($ea) } {
                # only display data with a time stamp newer that that
                #  last one we have. This prevents getting bogged down by old
                # data.
                set counts($ea) $metVal
                #display the number of downlink packets on a graph  but only
                # display 100 points
                if { $counts($ea) > $xmax } {
                    #	 set xmax [expr $counts($ea) + 64*20 ]; 
                    #	 set xmin  [expr $counts($ea) -64*80] ;
                    set xmax [expr $counts($ea)  ]; 
                    set xmin  [expr $counts($ea) -$pointsToDisplay*64] ;
                }


                $graph xaxis configure -max $xmax -min $xmin

                $scroll set $pointsToKeep $pointsToDisplay \
                    $counts($ea) [expr $counts($ea)+$pointsToDisplay*64]

                lappend EA_DATAx($ea) $metVal 
                lappend EA_DATAy($ea)  $yval
                #	    lappend EA_DATA($ea) "$metVal $yval"
                #$graph element append line {$metVal $yval }
                
                $graph element configure line\
                    -xdata $EA_DATAx($ea) -ydata $EA_DATAy($ea)
                # now we can truncate the number of points in memory by using
                #lrange.. we still can't sort before plotting though.
                set numPts [llength $EA_DATAx($ea)]
                set lower [expr $numPts*64 - $pointsToDisplay*64 ]
                if { $lower < 0} {
                    set lower 0 
                }
                set currMax [lindex [$graph xaxis configure -max] 4]
                set currMin [lindex [$graph xaxis configure -min] 4]

                .ff[expr $ea].sb[expr $ea] set [expr $pointsToKeep*64]\
                    [expr $pointsToDisplay*64] \
                    [expr int($currMin)] [expr int($currMax)]

                # truncate the number of points we are keeping
                if { $numPts > $pointsToKeep } {
                    set EA_DATAx($ea) [lrange $EA_DATAx($ea) \
                                           [expr $numPts - $pointsToKeep]\
                                           $pointsToKeep]
                    set EA_DATAy($ea) [lrange $EA_DATAy($ea) \
                                           [expr $numPts - $pointsToKeep]\
                                           $pointsToKeep]
                    # should we change the range on the scrollbar?
                }
                

                # $graph element configure line -data $EA_DATA($ea) 
                # start using -data $EA_DATA($ea) - test this tho
                # then we'd truncate the list using lappend/lrange /lreplace
                # keeping only the last n
                set count$ea $yval
            } 
        }
    }     
}



# configure the graphs and such
foreach ea {0 1 2 3 4 5 6 7} {
    # graphs
    set frame .ff[expr $ea]
    frame $frame -borderwidth 2 -relief ridge
    set item $frame.ea[expr $ea]
    blt_graph $item -height 225 -width 280
    set title "Telemetry Estimate EA[expr $ea ]"
    $item legend configure -mapped false
    $item configure -title $title 
    # setting x axis max causes problems
    $item yaxis configure -title "bits/s" -min 0 
    $item element create line -symbol scross

    $item xaxis configure -command formatLabels
    $item xaxis configure -title "UT" -min 0 \
        -max [expr $pointsToDisplay*64] -rotate -90
    # each data points arrivess every 64 seconds...

    $item xaxis configure -stepsize 3600 -subticks 6
    
    #scroll bar
    scrollbar $frame.sb$ea -orient horizontal \
        -command "handleScroll $item  $frame.sb$ea"
    $frame.sb$ea set $pointsToKeep $pointsToDisplay  \
        0 [expr $pointsToDisplay*64]
    #labels
    label $frame.configlea$ea -foreground navy -textvariable config$ea \
	-width 28
    label $frame.configName[expr $ea] -textvariable configName[expr $ea]  -width 31
    label $frame.runState[expr $ea] -textvariable runState([expr $ea]) -width 28
    #    label $frame.countlea[expr $ea] -textvariable count[expr $ea] 
}

proc handleScroll {graph sbar index} { 
    global pointsToKeep pointsToDisplay
    # current limits of the graph
    set currMax [lindex [$graph xaxis configure -max] 4]
    set currMin [lindex [$graph xaxis configure -min] 4]

    if {$index>=0 && $index<=[expr $pointsToKeep*64-$pointsToDisplay*64] } {
        $graph xaxis configure -min [expr $index]\
            -max [expr $index+$pointsToDisplay*64]

        set right [expr $index + $pointsToDisplay*64]
        $sbar set [expr $pointsToKeep*64] [expr $pointsToDisplay*64] \
            $index $right
    }
}

button .print -background grey -text "Print Graphs" -command {
    #   .ff1.ea1 psconfigure -pagex 2i -pagey 3i 
    #    .ff1.ea1 postscript 
    #    .ff2.ea2 postscript 
    .print flash
    dialog .d "message" "Printing Not Implemented Yet" {} -1 OK 
}

#pack .f 
foreach ea {0 1 2 3 4 5 6 7} {
    frame .ff$ea.x -borderwidth 3 -relief raised; 
    frame .ff$ea.y -borderwidth 3 -relief raised; 
    frame .ff$ea.z -borderwidth 3 -relief raised; 
    lower .ff$ea.z
    lower .ff$ea.y 
    lower .ff$ea.x
    label .lc1$ea -text "Config ID:"
    label .lc2$ea -text "Name:" 
    label .lc3$ea -text "RunState:" 
    pack .lc1$ea .ff$ea.configlea$ea  -side left -anchor w -in .ff$ea.x 
    pack .lc2$ea .ff$ea.configName$ea  -side left  -in .ff$ea.y 
    pack .lc3$ea .ff$ea.runState$ea  -side left  -in .ff$ea.z
    pack .ff$ea.ea$ea .ff$ea.sb$ea -fill both
    pack .ff$ea.x .ff$ea.y .ff$ea.z .ff$ea.configlea$ea -side top -anchor w
    pack .ff$ea.configlea$ea
    #    pack .ff$ea.countLabel$ea .ff$ea.countlea$ea -side left

}

# trace the run state so that the interface can change to reflect the ranges
trace variable runState w traceRunState

proc traceRunState { name element op } {
    upvar $name x
    global baseColor
    if { $x($element) == "PANIC" } {
	.ff$element.runState$element configure -background red
    } else {
	.ff$element.runState$element configure -background $baseColor
    }
}


setupInput .configBox processInput
#blt_table .f .ff0 {0,0} .ff1 {0,1} .ff2 {0,2} .ff3 {0,3}\
    .ff4 {1,0} .ff5 {1,1} .ff6 {1,2} .ff7 {1,3} 
pack .ff0 .ff1 .ff2 .ff3 -in .top -side left
pack .ff4 .ff5 .ff6 .ff7 -in .bot -side left
pack .top .bot
pack .configBox .print -side left

after 1500 refresh 1500


# update the clients
#proc refresh {} {
#    update idletasks
#
#    after 1500 refresh
#}










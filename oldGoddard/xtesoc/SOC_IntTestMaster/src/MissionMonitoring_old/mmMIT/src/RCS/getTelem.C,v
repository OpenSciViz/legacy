head	1.2;
access
	soccm;
symbols
	Build4_5_3:1.2;
locks
	ehm:1.2; strict;
comment	@ * @;


1.2
date	95.11.06.02.51.22;	author ehm;	state Exp;
branches;
next	1.1;

1.1
date	95.11.05.20.02.17;	author ehm;	state Exp;
branches;
next	;


desc
@Calculate EDS telemetry from Housekeeping data.
@


1.2
log
@clean up excess dribble.
@
text
@//   RCS: $Id: getTelem.C,v 1.1 1995/11/05 20:02:17 ehm Exp ehm $ 
//   Author      : Edward Morgan
//   Copyright 1994, Massachusetts Institute of Technology

static const char rcsid[]= "$Id: getTelem.C,v 1.1 1995/11/05 20:02:17 ehm Exp ehm $"; 
#include "getTelem.h"
#include <PktSwHouse.h>
#include <log_if.h>
#include <ObsParamBlock.h>
#include <PcaObsParamBlock.h>
#include <AsmObsParamBlock.h>
#include <BinnedMode.h>
#include <PulsarFoldMode.h>
#include <BurstCatcherMode.h>
#include <PosHistMode.h>
#include <MultTimeSeriesMode.h>
#include <PartMap.h>
#include <FftMode.h>

int VariableRate(unsigned short numTelemPkts, unsigned short  numPartitions)
{
  int telemRate=0;
  if (numTelemPkts < numPartitions)
    telemRate += 4000* numTelemPkts; // Who knows how big these are?
  else
    {
      // Partition Header packets are 256 bits, 1 per partition.
      telemRate += 256 * numPartitions;
      numTelemPkts -= numPartitions;
      if (numTelemPkts < numPartitions)
	{
	  telemRate += 4000* numTelemPkts; // Who knows how big these are?
	  numTelemPkts -= numPartitions;
	}
      telemRate += 8192 * numTelemPkts;
    }
  return telemRate/64;
}

int BinnedRate(unsigned long configId, unsigned short numTelemPkts, unsigned short  numPartitions)
{
  int telemRate=0;
    // Restore the Configuration from the database
  ObsParamBlockPtr paramBlock = ObsParamBlock::restore(configId);
  PartMapPtr partMap=paramBlock->partMap();
  PcaObsParamBlockPtr pcaBlock = paramBlock->castToPcaObsParamBlock();
  BinnedModePtr binnedBlock = pcaBlock->castToBinnedMode();
  int datasize = partMap->packedSize((int) binnedBlock->wordsPerPartition());
  telemRate += 272 * numPartitions;
  int numpkts =  datasize/503;
  telemRate += 8192*numpkts*numPartitions;
  datasize -= numpkts * 503;
  if (datasize > 0)
    {
      telemRate += numPartitions * ((16 * datasize) + (18 *8));
      numpkts++;
    }
  numpkts++;
  if (numpkts * numPartitions < numTelemPkts)
    telemRate += 1000 * (numTelemPkts - numpkts * numPartitions);
  return telemRate/64;
}

int FFTRate(unsigned long configId, unsigned short numTelemPkts, unsigned short  numPartitions)
{
  int telemRate=0;
    // Restore the Configuration from the database
  ObsParamBlockPtr paramBlock = ObsParamBlock::restore(configId);
  PartMapPtr partMap=paramBlock->partMap();
  PcaObsParamBlockPtr pcaBlock = paramBlock->castToPcaObsParamBlock();
  FftModePtr fftBlock = pcaBlock->castToFftMode();
  int datasize = partMap->packedSize((int) fftBlock->wordsPerDataSet());
  telemRate += 272 * numPartitions;
  int numpkts =  datasize/503;
  telemRate += 8192*numpkts*numPartitions;
  datasize -= numpkts * 503;
  if (datasize > 0)
    {
      telemRate += numPartitions * ((16 * datasize) + (18 *8));
      numpkts++;
    }
  numpkts++;
  if (numpkts * numPartitions < numTelemPkts)
    telemRate += 1000 * (numTelemPkts - numpkts * numPartitions);
  return telemRate/64;
}

int CBRate(unsigned long configId, unsigned short numTelemPkts, unsigned short  numPartitions)
{
  int telemRate=0;
    // Restore the Configuration from the database
  ObsParamBlockPtr paramBlock = ObsParamBlock::restore(configId);
  PartMapPtr partMap=paramBlock->partMap();
  PcaObsParamBlockPtr pcaBlock = paramBlock->castToPcaObsParamBlock();
  BurstCatcherModePtr burstCatcherBlock = pcaBlock->castToBurstCatcherMode();
  if (numTelemPkts < numPartitions)
    telemRate += 4000* numTelemPkts; // Who knows how big these are?
  else
    {
      // Partition Header packets are 256 bits, 1 per partition.
      telemRate += 256 * numPartitions;
      numTelemPkts -= numPartitions;
      if (numTelemPkts < numPartitions)
	{
	  telemRate += 4000* numTelemPkts; // Who knows how big these are?
	  numTelemPkts -= numPartitions;
	}
      telemRate += 8192 * numTelemPkts;
    }
  return telemRate/64;
}

int PulsarFoldRate(unsigned long configId, unsigned short numTelemPkts, unsigned short  numPartitions)
{
  int telemRate=0;
    // Restore the Configuration from the database
  ObsParamBlockPtr paramBlock = ObsParamBlock::restore(configId);
  PartMapPtr partMap=paramBlock->partMap();
  PcaObsParamBlockPtr pcaBlock = paramBlock->castToPcaObsParamBlock();
  PulsarFoldModePtr pulsarFoldBlock = pcaBlock->castToPulsarFoldMode();
  int datasize = partMap->packedSize((int) pulsarFoldBlock->partitionLength());
  telemRate += 272 * numPartitions;
  int numpkts =  datasize/503;
  telemRate += 8192*numpkts*numPartitions;
  datasize -= numpkts * 503;
  if (datasize > 0)
    {
      telemRate += numPartitions * ((16 * datasize) + (18 *8));
      numpkts++;
    }
  numpkts++;
  if (numpkts * numPartitions < numTelemPkts)
    telemRate += 1000 * (numTelemPkts - numpkts * numPartitions);
  return telemRate/64;
}

int PosHistRate(unsigned long configId, unsigned short numTelemPkts, unsigned short  numPartitions)
{
  int telemRate=0;
    // Restore the Configuration from the database
  ObsParamBlockPtr paramBlock = ObsParamBlock::restore(configId);
  PartMapPtr partMap=paramBlock->partMap();
  AsmObsParamBlockPtr asmBlock = paramBlock->castToAsmObsParamBlock();
  PosHistModePtr posHistBlock = asmBlock->castToPosHistMode();
  int datasize = partMap->packedSize((int) posHistBlock->wordsPerPartition());
  telemRate += 272 * numPartitions;
  int numpkts =  datasize/503;
  telemRate += 8192*numpkts*numPartitions;
  datasize -= numpkts * 503;
  if (datasize > 0)
    {
      telemRate += numPartitions * ((16 * datasize) + (18 *8));
      numpkts++;
    }
  numpkts++;
  if (numpkts * numPartitions < numTelemPkts)
    telemRate += 1000 * (numTelemPkts - numpkts * numPartitions);
  return telemRate/64;
}

int MTSRate(unsigned long configId, unsigned short numTelemPkts, unsigned short  numPartitions)
{
  int telemRate=0;
    // Restore the Configuration from the database
  ObsParamBlockPtr paramBlock = ObsParamBlock::restore(configId);
  PartMapPtr partMap=paramBlock->partMap();
  AsmObsParamBlockPtr asmBlock = paramBlock->castToAsmObsParamBlock();
  MultTimeSeriesModePtr multTimeSeriesBlock = asmBlock->castToMultTimeSeriesMode();
  int datasize = partMap->packedSize((int) multTimeSeriesBlock->timeSeriesIntervalTransferSize());
  telemRate += 272 * numPartitions;
  int numpkts =  datasize/503;
  telemRate += 8192*numpkts*numPartitions;
  datasize -= numpkts * 503;
  if (datasize > 0)
    {
      telemRate += numPartitions * ((16 * datasize) + (18 *8));
      numpkts++;
    }
  numpkts++;
  if (numpkts * numPartitions < numTelemPkts)
    telemRate += 8192 * (numTelemPkts - numpkts * numPartitions);
  return telemRate/64;
}

int getTelem(const PktSwHouse* pkt, unsigned long configId)
{
  unsigned short numCmdPkts = pkt->getStat(LOG_DMA_UPLINK_INTS);
  unsigned short numTelemPkts = pkt->getStat(LOG_DMA_DOWNLINK_INTS);
  unsigned short numPartitions = pkt->getStat(LOG_TIMING_FF_INTS);

  int telemRate = 10; 
  numTelemPkts--; 
  // Command Acks are usually 80 bytes which is 10 bits/s/cmd.
  telemRate += numCmdPkts * 10; 
  numTelemPkts -= numCmdPkts;
  switch (( configId & 0xff000000) >> 24)
    {
    case 0:
//      cout << " Idle Mode ";
      telemRate += VariableRate(numTelemPkts, numPartitions);
      return telemRate;

    case 1:
//      cout << " Event Mode ";
      telemRate += VariableRate(numTelemPkts, numPartitions);
      return telemRate;

    case 2:
//      cout << " Binned Mode ";
      telemRate += BinnedRate(configId, numTelemPkts, numPartitions);
      return telemRate;

    case 3:
//      cout << " Single-Bit Mode ";
      telemRate += VariableRate(numTelemPkts, numPartitions);
      return telemRate;

    case 4:
//      cout << " FFT Mode ";
      telemRate += FFTRate(configId, numTelemPkts, numPartitions);
      return telemRate;

    case 5:
//      cout << " Burst Catcher Event Mode ";
      telemRate += VariableRate(numTelemPkts, numPartitions);
      return telemRate;

    case 6:
//      cout << " Burst Catcher Binned Mode ";
      telemRate += CBRate(configId, numTelemPkts, numPartitions);
      return telemRate;

    case 8:
//      cout << " PulsarFold Fold Mode ";
      telemRate += PulsarFoldRate(configId, numTelemPkts, numPartitions);
      return telemRate;

    case 0xa:
//      cout << " Burst Trigger Mode ";
      telemRate += BinnedRate(configId, numTelemPkts, numPartitions);
      return telemRate;

    case 0xc:
//      cout << " Delta Binned Mode ";
      telemRate += BinnedRate(configId, numTelemPkts, numPartitions);
      return telemRate;

    case 0x81:
//      cout << " ASM Event Mode ";
      telemRate += VariableRate(numTelemPkts, numPartitions);
      return telemRate;

    case 0x82:
//      cout << " ASM Position Histogram Mode ";
      telemRate += PosHistRate(configId, numTelemPkts, numPartitions);
      return telemRate;

    case 0x84:
//      cout << " ASM Multiple Time Series Mode ";
      telemRate += MTSRate(configId, numTelemPkts, numPartitions);
      return telemRate;

    default: 
      cerr << " Unknown Mode " << configId << endl;
      telemRate += VariableRate(numTelemPkts, numPartitions);
      return telemRate;
    }
}

@


1.1
log
@Initial revision
@
text
@d1 1
a1 1
//   RCS: $Id$ 
d5 1
a5 1
static const char rcsid[]= "$Id$"; 
a58 2
  if (numpkts * numPartitions > numTelemPkts)
    cerr << "Expected " << numpkts *numPartitions << " packets, recieved "<< numTelemPkts <<endl;
a145 1
  cout << "P " << datasize;
@

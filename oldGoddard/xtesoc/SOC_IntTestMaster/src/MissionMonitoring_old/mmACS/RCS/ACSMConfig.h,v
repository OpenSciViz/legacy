head	2.22;
access
	mxb
	rbarnett
	dhon
	soccm;
symbols
	Build4_3_1:2.19
	Build4_3:2.19
	Latest:2.19
	Build4:2.19
	Build4_0:2.16
	Build3_3:2.14
	Build3_2:2.14
	Build3_1:2.5
	Build3:2.5
	Build2:2.5;
locks; strict;
comment	@ * @;


2.22
date	95.07.21.14.38.46;	author mxb;	state Exp;
branches;
next	2.21;

2.21
date	95.07.21.14.11.06;	author mxb;	state Exp;
branches;
next	2.20;

2.20
date	95.07.13.13.49.52;	author mxb;	state Exp;
branches;
next	2.19;

2.19
date	94.12.14.14.34.45;	author mxb;	state Exp;
branches;
next	2.18;

2.18
date	94.12.07.20.49.09;	author mxb;	state Exp;
branches;
next	2.17;

2.17
date	94.12.07.16.07.58;	author mxb;	state Exp;
branches;
next	2.16;

2.16
date	94.11.23.21.10.08;	author mxb;	state Exp;
branches;
next	2.15;

2.15
date	94.11.16.22.11.23;	author mxb;	state Exp;
branches;
next	2.14;

2.14
date	94.10.19.15.19.44;	author mxb;	state Exp;
branches;
next	2.13;

2.13
date	94.10.11.18.19.04;	author mxb;	state Exp;
branches;
next	2.12;

2.12
date	94.10.05.19.08.16;	author mxb;	state Exp;
branches;
next	2.11;

2.11
date	94.10.03.13.20.20;	author mxb;	state Exp;
branches;
next	2.10;

2.10
date	94.09.29.16.39.34;	author mxb;	state Exp;
branches;
next	2.9;

2.9
date	94.09.27.13.58.55;	author mxb;	state Exp;
branches;
next	2.8;

2.8
date	94.09.21.18.35.46;	author mxb;	state Exp;
branches;
next	2.7;

2.7
date	94.09.15.17.06.17;	author mxb;	state Exp;
branches;
next	2.6;

2.6
date	94.07.26.20.25.39;	author rbarnett;	state Exp;
branches;
next	2.5;

2.5
date	94.01.06.20.59.47;	author paritosh;	state Release;
branches;
next	2.4;

2.4
date	93.12.17.23.14.14;	author paritosh;	state Release;
branches;
next	2.3;

2.3
date	93.12.14.06.01.31;	author paritosh;	state Release;
branches;
next	2.2;

2.2
date	93.12.01.15.50.11;	author paritosh;	state Release;
branches;
next	2.1;

2.1
date	93.12.01.01.49.46;	author paritosh;	state Release;
branches;
next	;


desc
@Creation
@


2.22
log
@*** empty log message ***
@
text
@//
// $Id: ACSMConfig.h,v 2.20 1995/07/13 13:49:52 mxb Exp mxb $
//
// ACSMeasuredConfig - 	Define an ACS Measured Configuration Class.
//
// This is a subclass of SOC abstract base class MeasuredConfig for
// specifying the measured configurations of the Attitude Control 
// subsystem. Used to construct persistent measured configuration 
// objects from the telemetry and for verifying them against predicted 
// configurations
//


#ifndef _ACSMEASCONFIG_H_
#define _ACSMEASCONFIG_H_
#define ACSMeasuredConfig_ID 1520
#define ACSMeasuredConfig_LT 1 
#define Q_RANGE 0.1
#define ADR_RANGE 0.016666667


#include "MeasConfig.h"
#include "PktTlm.h"

#include "ACSPredictedConfig.h"
#include "ACSmodel.h"
#include "AccACS0.h"

#include <rw/collect.h>
#include <rw/rwfile.h>
#include <rw/vstream.h>
#include <rw/cstring.h>



//
// ACS Measured Configuration class
//

class ACSMeasuredConfig: public MeasuredConfig
{

public:

//
// Construct a measured configuration 
//
  
  ACSMeasuredConfig  ();

  ACSMeasuredConfig(ACSMeasuredConfig*) ;

  ACSMeasuredConfig(PktTlm* , char *);

  ACSMeasuredConfig(long,double*); //for testing only

  ACSMeasuredConfig(const ACSMeasuredConfig&);

//Assignement operator

  ACSMeasuredConfig& operator =(const ACSMeasuredConfig&);

  inline float getQuaternion (const unsigned char*);

  int getAcsMode() const;

  int getAcsPredMode() const;

  void setAcsMode(int);

  void setSunVec(DoubleVec &);

  void setAcsPredMode(int);

//
// The destructor
//

  ~ACSMeasuredConfig (void);

    

//
// Compares the present measured configuration with the
// previous one and returns TRUE if this  method detects
//a significant change, FALSE otherwise.
//
// I do not think that you may need this.
  


  RWBoolean hasChanged(const MeasuredConfig*) ;

// This is mainly what I need from you.
  void verifyConfig(const PredictedConfig*) ;


  void setConfigLifeTime(unsigned int);

  void setConfigError(int,const char*,const char*);


//
// Outputs the current measured configuration
//
  void printError(ostream&) const;
  void print (ostream& ostr) const;
  void printShort (ostream& ostr) const;
  void printLong (ostream& ostr) const;

  double getQ0() const;
  double getQ1() const;
  double getQ2() const;
  double getQ3() const;
  double getRA() const;
  double getDEC() const;
  double getROLL() const;

  void saveGuts (RWFile&) const;
  void saveGuts (RWvostream&) const;
  void restoreGuts (RWFile&);
  void restoreGuts (RWvistream&);

  RWspace binaryStoreSize(void) const;
  int compareTo  (const RWCollectable*) const;
  RWBoolean isEqual (const RWCollectable*) const;
  unsigned hash (void) const;



private:

// Quaternions

  double q[4];
  int _quatOK;

//   enum ACSMODE {HOLD, SLEW} is declared in $SOCHOME/include/ACSDesiredConfig.h
 
  int _acsMode;
  int _acsPredMode;

  double _rightAscension;
  double _declination;
  double _rollBias;
  double _slewRate;

  int    _dontKnowRA;
  int    _dontKnowDec;
  int    _dontKnowRoll;
  int    _dontKnowRate;
  float  quat_def;
  float  tol_def;



  RWDECLARE_COLLECTABLE(ACSMeasuredConfig)


};


#endif






@


2.21
log
@*** empty log message ***
@
text
@d2 1
a2 1
// $Id: ACSMConfig.h,v 2.20 1995/07/13 13:49:52 mxb Exp $
d16 2
d20 1
@


2.20
log
@*** empty log message ***
@
text
@d2 1
a2 1
// $Id: ACSMConfig.h,v 2.19 1994/12/14 14:34:45 mxb Exp $
a15 2
#define ACSMeasuredConfig_ID 1520
#define ACSMeasuredConfig_LT 1 
@


2.19
log
@*** empty log message ***
@
text
@d2 1
a2 1
// $Id: ACSMConfig.h,v 2.18 1994/12/07 20:49:09 mxb Exp $
a21 1

d69 2
@


2.18
log
@*** empty log message ***
@
text
@d2 1
a2 1
// $Id: ACSMConfig.h,v 2.17 1994/12/07 16:07:58 mxb Exp $
d55 2
a112 4
  void   setQ0(double);
  void   setQ1(double);
  void   setQ2(double);
  void   setQ3(double);
@


2.17
log
@*** empty log message ***
@
text
@d2 1
a2 1
// $Id: ACSMConfig.h,v 2.16 1994/11/23 21:10:08 mxb Exp $
a60 2
  MeasuredConfig* Insert(MeasuredConfig*);

@


2.16
log
@*** empty log message ***
@
text
@d2 1
a2 1
// $Id: ACSMConfig.h,v 2.15 1994/11/16 22:11:23 mxb Exp $
d61 2
d67 2
d71 2
d98 2
d113 4
@


2.15
log
@*** empty log message ***
@
text
@d2 1
a2 1
// $Id: ACSMConfig.h,v 2.14 1994/10/19 15:19:44 mxb Exp $
d18 2
@


2.14
log
@*** empty log message ***
@
text
@d2 1
a2 1
// $Id: ACSMConfig.h,v 2.13 1994/10/11 18:19:04 mxb Exp $
d63 2
d103 3
@


2.13
log
@*** empty log message ***
@
text
@d2 1
a2 1
// $Id: ACSMConfig.h,v 2.12 1994/10/05 19:08:16 mxb Exp $
d17 1
a17 1
#define ACSMeasuredConfig_LT 8 
d60 3
@


2.12
log
@*** empty log message ***
@
text
@d2 1
a2 1
// $Id: ACSMConfig.h,v 2.11 1994/10/03 13:20:20 mxb Exp mxb $
d94 5
@


2.11
log
@*** empty log message ***
@
text
@d2 1
a2 1
// $Id: ACSMConfig.h,v 2.10 1994/09/29 16:39:34 mxb Exp $
d17 1
a17 1
#define ACSMeasuredConfig_LT 1
d111 1
a111 1
  static int _quatOK;
d116 1
@


2.10
log
@defined copy constr, assignement operator ...etc
@
text
@d2 1
a2 1
// $Id: ACSMConfig.h,v 2.9 1994/09/27 13:58:55 mxb Exp $
d16 2
a17 1
#define ACSMEASUREDCONFIG_ID 1520
a31 1
#define ACSMeasuredConfig_ID 1520
@


2.9
log
@*** empty log message ***
@
text
@d2 1
a2 1
// $Id: ACSMConfig.h,v 2.8 1994/09/21 18:35:46 mxb Exp mxb $
d31 1
a31 1
#define ACSMeasConfig_ID 1520
d53 6
@


2.8
log
@*** empty log message ***
@
text
@d2 1
a2 1
// $Id: ACSMConfig.h,v 2.7 1994/09/15 17:06:17 mxb Exp mxb $
d51 1
a51 1
  ACSMeasuredConfig(PktTlm* , char *, char *);
@


2.7
log
@*** empty log message ***
@
text
@d2 1
a2 1
// $Id: ACSMConfig.h,v 1.4 1994/09/14 12:11:42 mxb Exp mxb $
d51 1
a51 1
  ACSMeasuredConfig(PktTlm*);
@


2.6
log
@*** empty log message ***
@
text
@d2 1
a2 1
// $Id: ACSMConfig.h,v 2.5 1994/01/06 20:59:47 paritosh Release rbarnett $
a18 1
#include "VerifReport.h"
d67 1
d71 1
a71 1
  RWBoolean hasChanged(const MeasuredConfig*) const  ;
d73 2
a75 1
  VerificationReport* verifyConfig(const PredictedConfig*) const ;
d77 1
a78 1
  unsigned int getConfigTime() const;
a79 8

  SubsystemId getSubsystemId() const;






d83 1
a83 1

d109 1
a109 1
  ACSMODE _acsMode;
d120 2
a121 1

a122 2
  unsigned long configTime;
  SubsystemId subsysID;
@


2.5
log
@"."
@
text
@d2 1
a2 1
// $Id: ACSMConfig.h,v 2.4 1993/12/17 23:14:14 paritosh Release paritosh $
d100 1
a100 1
  unsigned binaryStoreSize(void) const;
@


2.4
log
@"."
@
text
@d2 1
a2 1
// $Id: ACSMConfig.h,v 2.3 1993/12/14 06:01:31 paritosh Release paritosh $
d25 1
@


2.3
log
@"for redelivery on dec 10."
@
text
@d2 1
a2 1
// $Id: ACSMConfig.h,v 2.2 1993/12/01 15:50:11 paritosh Release paritosh $
d127 1
@


2.2
log
@"."
@
text
@a0 1

d2 1
a2 1
// $Id: ACSMConfig.h,v 2.1 1993/12/01 01:49:46 paritosh Release paritosh $
d21 1
a21 1
#include "PktCCSDS.h"
d24 1
d50 4
d108 1
d110 3
d127 4
@


2.1
log
@Creation
@
text
@d1 1
d3 1
a3 1
// $Id: ACSMeasConfig.h,v 1.0 1993/09/20 11:45:00 paritosh Exp $
@

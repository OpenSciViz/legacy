head	3.21;
access
	savkoor
	paritosh
	mxb
	dhon
	soccm;
symbols
	Build4_3_1:3.21
	Build4:3.21
	Build3_3:3.20
	Build3_2:3.16
	Build3_1:3.6
	Build3:3.5
	Build2:3.1;
locks; strict;
comment	@ * @;


3.21
date	94.11.30.20.39.31;	author savkoor;	state Exp;
branches;
next	3.20;

3.20
date	94.11.08.16.34.47;	author savkoor;	state Exp;
branches;
next	3.19;

3.19
date	94.11.01.18.17.09;	author savkoor;	state Exp;
branches;
next	3.18;

3.18
date	94.10.28.16.20.22;	author savkoor;	state Exp;
branches;
next	3.17;

3.17
date	94.10.21.21.08.58;	author savkoor;	state Exp;
branches;
next	3.16;

3.16
date	94.10.11.18.21.19;	author mxb;	state Exp;
branches;
next	3.15;

3.15
date	94.10.05.20.19.45;	author savkoor;	state Exp;
branches;
next	3.14;

3.14
date	94.10.03.13.21.14;	author mxb;	state Exp;
branches;
next	3.13;

3.13
date	94.09.29.16.40.20;	author mxb;	state Exp;
branches;
next	3.12;

3.12
date	94.09.28.17.56.48;	author mxb;	state Exp;
branches;
next	3.11;

3.11
date	94.09.27.19.06.34;	author mxb;	state Exp;
branches;
next	3.10;

3.10
date	94.09.23.14.27.45;	author mxb;	state Exp;
branches;
next	3.9;

3.9
date	94.09.23.13.34.03;	author mxb;	state Exp;
branches;
next	3.8;

3.8
date	94.09.14.12.12.15;	author mxb;	state Exp;
branches;
next	3.7;

3.7
date	94.09.06.17.50.39;	author mxb;	state Exp;
branches;
next	3.6;

3.6
date	94.07.08.00.35.42;	author savkoor;	state Exp;
branches;
next	3.5;

3.5
date	94.05.24.19.32.12;	author paritosh;	state Exp;
branches;
next	3.4;

3.4
date	94.05.05.18.31.03;	author paritosh;	state Exp;
branches;
next	3.3;

3.3
date	94.05.05.17.59.16;	author paritosh;	state Exp;
branches;
next	3.2;

3.2
date	94.04.30.20.35.10;	author savkoor;	state Exp;
branches;
next	3.1;

3.1
date	93.12.30.21.38.29;	author savkoor;	state Release;
branches;
next	;


desc
@Creation
@


3.21
log
@*** empty log message ***
@
text
@#ifndef _PCAMEASURED_H
#define _PCAMEASURED_H

//
// .NAME     PCAMeasConfig.h - define a PCA Instrument Team MeasuredConfig Class
// .LIBRARY  libmm
// .HEADER   MissionMonitoring
// .INCLUDE  PCAMeasConfig.h
// .FILE     PCAMeasConfig.C
// .VERSION  XTE-PCA V4.0
// .OPTIONS  
// 
// .SECTION AUTHOR
// Vikram Savkoor
//
// .SECTION Description:
// This is a subclass of SOC abstract base class MeasuredConfig for
// specifying the measured configurations of the PCA Instrument Team 
// subsystem. Used to construct persistent measured configuration 
// objects from the telemetry and for verifying them against predicted 
// configurations
//
// RCS: $Id: PCAMeasConfig.h,v 3.20 1994/11/08 16:34:47 savkoor Exp $
//

#include "MeasConfig.h"
#include "PktPca.h"
#include "PcaHKPart.h"

#include <rw/collect.h>
#include <rw/rwfile.h>
#include <rw/vstream.h>

#define PCAMeasuredConfig_ID 1530
#define PCAMeasuredConfig_LT 4
#define maxPCUs 5


//
// PCA Measured Configuration class
//
class PCAMeasuredConfig: public MeasuredConfig
{
     RWDECLARE_COLLECTABLE (PCAMeasuredConfig)

   public:

     //
     // Construct a measured configuration 
     //
     PCAMeasuredConfig  (void);
     PCAMeasuredConfig  (char*);

     //
     // The destructor
     //
     ~PCAMeasuredConfig (void);

     //
     // Copy constructor
     //
     PCAMeasuredConfig (const PCAMeasuredConfig& );

     // Description:
     // Reads PCA packet partitions and builds 
     // a PCA Measured Config object. Returns TRUE if
     // the object is complete; FALSE if incomplete
     
     RWBoolean buildPackets (const PcaHKPart&);

     // Description:
     // The assignment operator
     
     PCAMeasuredConfig& operator= (const PCAMeasuredConfig&);

     int operator!= (const PCAMeasuredConfig&) const;


     //
     // Compares the present measured configuration with the
     // previous one and returns TRUE if the updateConfig()
     // method detects a significant change, FALSE otherwise.
     //
     RWBoolean hasChanged (const MeasuredConfig*) ;

     //
     //
     //
     long getConfigTime () const;

     //
     // compares this Measuredconfig with the given Predictedconfig
     // and returns a VerificationReport object describing the
     // comparison results. May return a null pointer if there were
     // no significant differences
     //
     void verifyConfig (const PredictedConfig*);


     //
     // Outputs the error messages
     //
     void printError( ostream& ) const;

     //
     // Outputs the current measured configuration
     //
     void print (ostream& ostr) const;

     //
     // save and restore the object to file/stream
     //
     void saveGuts (RWFile&) const;
     void saveGuts (RWvostream&) const;
     void restoreGuts (RWFile&);
     void restoreGuts (RWvistream&);

     //
     // returns the Subsystem ID 
     //
     SubsystemId getSubsystemId () const;

     //
     // returns the number of bytes required to store the object
     //
#if RWTOOLS < 0x0600 && ! defined(RWspace)
#define RWspace unsigned
#endif
     RWspace binaryStoreSize () const;

     //
     //
     //
     void resetFlags ();

     //
     // returns TRUE if arg.=TRUE and one one or more stateFlag[] = TRUE
     // returns TRUE if arg.=FALSE and one or more stateFlag[] = FALSE
     //
     RWBoolean getState (RWBoolean) const;

     //
     // data 
     //
     PcaHKPart state[maxPCUs];
     RWBoolean stateFlag[maxPCUs];
     long _time;

};


class mmPcaInit
{
public:
    mmPcaInit();
    ~mmPcaInit();
};


#endif


@


3.20
log
@*** empty log message ***
@
text
@d23 1
a23 1
// RCS: $Id: PCAMeasConfig.h,v 3.19 1994/11/01 18:17:09 savkoor Exp $
d69 1
a69 1
     int buildPackets (const PcaHKPart&);
d140 1
a140 1
     int getState (int) const;
d146 1
a146 1
     int stateFlag[maxPCUs];
@


3.19
log
@static anMMPcainit moved .h to .C file
@
text
@d1 3
d5 10
a14 2
// PCAMeasuredConfig - 	Define a PCA Instrument Team Measured Configuration 
//			Class.
d16 1
d23 1
a23 1
// RCS: $Id: PCAMeasConfig.h,v 3.18 1994/10/28 16:20:22 savkoor Exp $
a25 3
#ifndef _PCAMEASURED_H
#define _PCAMEASURED_H

a26 1
//#include "PCAPredConfig.h"
d64 1
a64 1
     //
d68 2
a69 2
     //
     RWBoolean buildPackets (const PcaHKPart&);
d71 1
a71 1
     //
d73 1
a73 1
     //
d76 1
a76 1
     RWBoolean operator!= (const PCAMeasuredConfig&) const;
d140 1
a140 1
     RWBoolean getState (RWBoolean) const;
d146 1
a146 1
     RWBoolean stateFlag[maxPCUs];
a156 2
   
//    static int count;
a158 5

//ostream& operator <<(ostream& os, const PCAMeasuredConfig&);
//static mmPcaInit anMMPcainit;


@


3.18
log
@*** empty log message ***
@
text
@d11 1
a11 1
// RCS: $Id: PCAMeasConfig.h,v 3.17 1994/10/21 21:08:58 savkoor Exp savkoor $
d155 1
a155 1
static mmPcaInit anMMPcainit;
@


3.17
log
@*** empty log message ***
@
text
@d11 1
a11 1
// RCS: $Id: PCAMeasConfig.h,v 3.15 1994/10/05 20:19:45 savkoor Exp $
a17 1
#include "VerifReport.h"
@


3.16
log
@*** empty log message ***
@
text
@d28 1
a28 1
#define PCAMeasuredConfig_LT 8
@


3.15
log
@*** empty log message ***
@
text
@d11 1
a11 1
// RCS: $Id: PCAMeasConfig.h,v 3.4 1994/05/05 18:31:03 paritosh Exp $
d28 1
a28 1
#define PCAMeasuredConfig_LT 4
@


3.14
log
@*** empty log message ***
@
text
@d11 1
a11 1
// RCS: $Id: PCAMeasConfig.h,v 3.13 1994/09/29 16:40:20 mxb Exp $
d18 2
a19 1
#include "PCAPredConfig.h"
d29 1
d44 1
a45 2
 
     PCAMeasuredConfig();
a46 2
     PCAMeasuredConfig (const PcaHKPart*);

a56 5
     PCAMeasuredConfig (PCAMeasuredConfig*);


     RWBoolean PCAMeasuredConfig::buildPackets (const PcaHKPart& );

d62 2
d82 7
d90 2
a91 1
     void verifyConfig (const PredictedConfig*) ;
d94 1
a94 1
     // Outputs the error msg's
d96 1
a96 2
     void printError(ostream&) const;

d104 2
a105 1
     RWspace binaryStoreSize(void) const;
d111 3
d116 11
d129 6
a134 1
   private:
d138 3
a140 3
     PcaHKPart state[MaxPCUs];
     int stateFlag[MaxPCUs];

d155 1
a155 1
ostream& operator <<(ostream& os, const PCAMeasuredConfig&);
@


3.13
log
@redefined copy constr. redefined assignement operator ...etc
@
text
@d11 1
a11 1
// RCS: $Id: PCAMeasConfig.h,v 3.12 1994/09/28 17:56:48 mxb Exp mxb $
d27 1
@


3.12
log
@defined binaryStoreSize()
@
text
@d11 1
a11 1
// RCS: $Id: PCAMeasConfig.h,v 3.11 1994/09/27 19:06:34 mxb Exp mxb $
d57 2
@


3.11
log
@TOOK OUT getConfigTime() which is implemented on the base class
@
text
@d11 1
a11 1
// RCS: $Id: PCAMeasConfig.h,v 3.10 1994/09/23 14:27:45 mxb Exp $
d98 1
@


3.10
log
@Upgraded the default constructor
@
text
@d11 1
a11 1
// RCS: $Id: PCAMeasConfig.h,v 3.9 1994/09/23 13:34:03 mxb Exp mxb $
a81 4
     //
     unsigned int getConfigTime () const;

     //
@


3.9
log
@modified the constructor PCAMeasuredConfig(char *host_name)
@
text
@d11 1
a11 1
// RCS: $Id: PCAMeasConfig.h,v 3.8 1994/09/14 12:12:15 mxb Exp $
d41 3
a43 1
     PCAMeasuredConfig  (*char );
@


3.8
log
@*** empty log message ***
@
text
@d11 1
a11 1
// RCS: $Id: PCAMeasConfig.h,v 3.7 1994/09/06 17:50:39 mxb Exp mxb $
d41 1
a41 1
     PCAMeasuredConfig  (void);
@


3.7
log
@modified verifyConfig method to comply  with latest design
@
text
@d11 1
a11 1
// RCS: $Id: PCAMeasConfig.h,v 3.6 1994/07/08 00:35:42 savkoor Exp $
a17 1
#include "VerifReport.h"
d43 2
d55 3
a62 2
     RWBoolean buildPackets (const PcaHKPart&);

d76 1
a76 1
     RWBoolean hasChanged (const MeasuredConfig*) const;
d88 5
d126 1
a126 1
    static int count;
@


3.6
log
@*** empty log message ***
@
text
@d11 1
a11 1
// RCS: $Id: PCAMeasConfig.h,v 3.4 1994/05/05 18:31:03 paritosh Exp $
d84 1
a84 1
     VerificationReport* verifyConfig (const PredictedConfig*) const;
@


3.5
log
@added PCAPredConfig.h , removed #define MaxPCUs 5
@
text
@d11 1
a11 1
// RCS: $Id: PCAMeasConfig.h,v 3.4 1994/05/05 18:31:03 paritosh Exp paritosh $
a29 4

//enum boolean {FALSE, TRUE};


a42 1
     PCAMeasuredConfig  (const PcaHKPart&);
d55 7
d100 1
a100 1

d106 2
a107 1
     PcaHKPart *state;
@


3.4
log
@included mmPcaHandler class
@
text
@d11 1
a11 1
// RCS: $Id: PCAMeasConfig.h,v 3.3 1994/05/05 17:59:16 paritosh Exp paritosh $
d19 1
a19 1
#include "PredictedConfig.h"
a29 1
#define	MaxPCUs	5
@


3.3
log
@*** empty log message ***
@
text
@d11 1
a11 1
// RCS: $Id:$
d110 15
@


3.2
log
@*** empty log message ***
@
text
@a66 4
     //
     // Makes a Clone 
     //
     PCAMeasuredConfig* Clone (PCAMeasuredConfig*);
a67 1

@


3.1
log
@Creation
@
text
@d21 1
a21 3
//#include "AccPcaHK.h"
#include "PktTlm.h"
#include "decodePcaHK.h"
d48 1
a48 1
     PCAMeasuredConfig  (PktTlm*);
d110 1
a110 1
     PcaHKData *state;
@

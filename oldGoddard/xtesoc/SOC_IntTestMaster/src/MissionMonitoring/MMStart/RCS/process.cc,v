head	1.1;
access;
symbols;
locks; strict;
comment	@// @;


1.1
date	98.01.31.21.39.10;	author savkoor;	state Exp;
branches;
next	;


desc
@Initial version.
@


1.1
log
@Initial revision
@
text
@
static const char* const rcsid = "$Id: process.cc,v 1.2 1997/09/17 16:56:11 savkoor Exp savkoor $";

#include "process.h"
#include <sys/wait.h>

static void exit_action_default(process *p)
{
  cerr<<p->name()<<" Died !"<<endl;
  p->RemoveInput();
}

PROCESS_CB process::exit_action=exit_action_default;
PROCESS_CB process::set_exit_action(PROCESS_CB theFunc)
{
   PROCESS_CB pc = exit_action;
   exit_action=theFunc;
   return pc;
}

//
// constructor
//
process::process()
{
  struct utsname utname;
	if(uname(&utname)<0)
        {
           perror("uname");
	}
	memset(nam,0,sizeof(nam));
	memset(wnam,0,sizeof(wnam));
	memset(cnam,0,sizeof(cnam));
	memset(rtnam,0,sizeof(rtnam));
	memset(options,0,sizeof(options));
	sprintf(host,"%s","xrtserv");
	sprintf(mmg,"%s",utname.nodename);
	port=5094;
	process_status=0;
	fd=-1;
	pid=-1;
	verify=1;
	debug=0;
	realtime=1;
	downstream=0;
	restart=0;
	saved_from_file=0;
	memset(pktfile,0,sizeof(pktfile));
}

//
// constructor - takes in the name of the process, it's 
// window name (for updating display), and the client name.
// xrtserv is the default server host. 
// 5094 is the default port number.
//
process::process(char *n, char *wn, char *rtn)
{
  struct utsname utname;
  if(uname(&utname)<0)
  {
	perror("uname");
  }

  sprintf(nam,"%s",n);
  sprintf(wnam,"%s",wn);
  sprintf(rtnam,"%s",rtn);
  memset(cnam,0,sizeof(cnam));
  memset(options,0,sizeof(options));
  sprintf(host,"%s","xrtserv");
  sprintf(mmg,"%s",utname.nodename);
  process_status=0;
  port=5094;
  fd=-1;
  pid=-1;
  verify=1;
  debug=0;
  realtime=1;
  downstream=0;
  restart=1;
  saved_from_file=0;
  memset(pktfile,0,sizeof(pktfile));

  //
  // if any configurations were saved, then
  // restore them.
  //
  char path[200];
  sprintf(path,"%s/stats/%s.%s",
  getenv("SOCOPS"),utname.nodename,n);
  RWFile file(path);
  if( file.isValid() )
  {
    if( file.Exists() && !file.IsEmpty() )
    {
       restore(file);
       saved_from_file=1;
    }
  }

}

//
// the copy constructor
//
process::process(const process& p)
{
	sprintf(nam,"%s",p.nam);
	sprintf(wnam,"%s",p.wnam);
	sprintf(cnam,"%s",p.cnam);
	sprintf(mmg,"%s",p.mmg);
	process_status=p.process_status;
	InputId=p.InputId;
	fd=p.fd;
	sprintf(host,"%s",p.host);
	port=p.port;
	pid=p.pid;
	verify=p.verify;
	debug=p.debug;
	realtime=p.realtime;
	restart=p.restart;
	saved_from_file=p.saved_from_file;	
	downstream=p.downstream;
	sprintf(pktfile,"%s",p.pktfile);
	sprintf(rtnam,"%s",p.rtnam);
	sprintf(options,"%s",p.options);
}


//
// Removes the process from being monitored. close the pipe
//
void process::RemoveInput()
{
    if(fd>=0)
    {
         if( waitpid(pid, (int *)0, 0) != pid )
           cerr<<"Error! attempt to remove unknown process id: "<<pid<<endl;
         XtRemoveInput(InputId);
         close(fd);
         fd=-1;
         pid=0;
         if(process_status==1)
                  process_status=0;
         //
         // set the end time
         //
         time_t etime;
         time(&etime);
         setEndTime(&etime);
cerr<<nam<<": exited on "<<ctime(&endTime)<<endl;
     }
}	

//
// Destructor
//
process::~process()
{
  Kill();
  RemoveInput();
}

//
// set the process name - the executable
//
void process::name(char *n)
{
  sprintf(nam,"%s",n);
}
//
// return the process name - the executable
//
const char* process::name()
{
   return nam;
}

//
// set the client name - for realtime packets
//
void process::rtname(char *rtn)
{
  sprintf(rtnam,"%s",rtn);
}

//
// return the client name - for realtime packets
//
const char* process::rtname()
{
  return rtnam;
}

// 
// set the window item title - used for updating
// the display.
//
void process::wname(char *wn)
{
  sprintf(wnam,"%s",wn);
}

// 
// return the window item title - used for updating
// the display.
//
const char* process::wname()
{
  return wnam;
}

//
// set the command line options
//
void process::cname(char *cn)
{
  sprintf(cnam,"%s",cn);
}

//
// return the command line options
//
const char* process::cname()
{
  return cnam;
}

// 
// set the process file descriptor
//
int process::Fd()
{
  return fd;
}

//
// set the process status.
// status = 0 by default,
// 1 when started,
// -1 when killed.
// 
void process::status(int s)
{
  process_status =s;
}

// 
// return the process status
//
int process::status()
{
  return process_status;
}


void process::Host(char *h)
{
  sprintf(host,"%s",h);
}

const char* process::Host()
{
  return host;
}

void process::MMG(char *h)
{
  sprintf(mmg,"%s",h);
}

const char* process::MMG()
{
  return mmg;
}

void process::Port(int p)
{
  port = p;
}

int process::Port()
{
  return port;
}

//
// sets the time the process is started.
//
void process::setStartTime( const time_t* aTime )
{
  startTime = *aTime;
}

//
// returns the time the process was started
//
const time_t* process::getStartTime() const
{
  return &startTime;
}

//
// sets the time the process ended.
//
void process::setEndTime( const time_t* aTime )
{
  endTime = *aTime;
}

//
// returns the time the process ended.
//
const time_t* process::getEndTime() const
{
  return &endTime;
}

//
// starts a process. 
// 
int process::start_process(Widget W)
{
  int fdes[2];
  pid_t proc_id;
  
#ifdef DEBUG
cerr<<"Start_process: ";
#endif

  if( pipe(fdes)<0 )
     return -1;

  proc_id=fork();

  if(proc_id<0)
    return -1;

#ifdef DEBUG
cerr<<"proc_id "<<proc_id<<endl;
#endif

   if(proc_id>0)
   {  //parent process

      // keep the read fd open to monitor the child
      close(fdes[1]);
      fd=fdes[0];
      process_status=1;
      pid=proc_id; // the childs process id

      time(&startTime);
cerr<<nam<<": started on "<<ctime(&startTime)<<endl;

      if(W)
      {
        XtAppContext appc = XtWidgetToApplicationContext(W);
        InputId           = XtAppAddInput(appc,fd,
                            (XtPointer)XtInputReadMask,
                            (XtInputCallbackProc)
                            exit_action,this);
      }
      return fd;
    }
    else
    { //child process
#ifdef DEBUG
cerr<<"Child process: "<<cnam<<endl;
#endif

      //
      // seperate the command line options for execv
      //
      char *token;
      char arglist[200];
      char *cline[100];
      int count=0;
      strcpy(arglist,cnam);
      token = strtok(arglist," ");
      if(token)
      {
        cline[0] = (char *)malloc(strlen(token)+1);
        sprintf(cline[0],"%s",token);
        count++;
        while(token=strtok(NULL," "))
        {
          cline[count]=(char *)malloc(strlen(token)+1);
          sprintf(cline[count],"%s",token);
          count++;
        }
        cline[count]=NULL;
      }

      close(fdes[0]);
      char dum_buf[200];
      sprintf(dum_buf,"%s/%s/%s/%s",
                  getenv("SOCHOME"),
                  (debug?"qfbin":"bin"),
                  getenv("ARCH"),nam);

#ifdef DEBUG
cerr<<dum_buf;
for(int id=0; cline[id]!=NULL; id++ )
  cerr<<" "<<cline[id];
cerr<<endl;
#endif

      //
      // make the child process the sessions and group leader
      // 
      setsid();
      if(realtime)
      {
         if( execv(dum_buf,cline)==-1)
         {
            //
            // if execv succeeds, this never gets executed
            // 
            write(fdes[1]," ",1);
            kill(getpid(),9);
         }
      }
      else
      {
        sprintf(dum_buf,"%s %s < %s",
        dum_buf,cnam,pktfile);
        system(dum_buf);
        kill(getpid(),9);
      }

      for(int i=0;i<count;i++)
      {
        free(cline[i]);
      }
    } // end child
}

//
// kill the parent and child processes
//
void process::Kill()
{
  int sunos5=1;
  if(pid>0)
  {
    char buf[300];
    int ppid=-1, id=-1, gid=-1, kill_child=0;
    int sec_ppid=-1, sec_pid=-1;
    if(strcmp(getenv("ARCH"),"SunOS4")==0)
    {
        sunos5=0;
        sprintf(buf,"/bin/ps -xj");
    }
    else
    {
        sunos5=1;
        sprintf(buf,"/bin/ps -efj");
    }

    FILE *f=popen(buf,"r");	
    while(fgets(buf,200,f)!=NULL)
    {
        if(!sunos5){
            ppid=atoi(strtok(buf," "));
            id = atoi(strtok(NULL," "));
            gid =atoi(strtok(NULL," "));
        }
        else{
            strtok(buf," ");
            id = atoi(strtok(NULL," "));
            ppid = atoi(strtok(NULL," "));
            gid = atoi(strtok(NULL," "));
        }

        if(gid == pid){
           sec_ppid=ppid;
           sec_pid=id;
           kill(id,9);
           kill_child=1;
        }
     }

     if(!kill_child)
     {
          kill(pid,9);
     }
     pclose(f);
  }
}

//
// restore process configuration
//
void process::restore(RWFile &file)
{
  file.Read(verify);
  file.Read(debug);
  file.Read(realtime);
  file.Read(restart);
  file.Read(downstream);
  file.Read(pktfile,sizeof(pktfile));
  file.Read(options,sizeof(options));
  file.Read(nam,sizeof(nam));
  file.Read(wnam,sizeof(wnam));
  file.Read(cnam,sizeof(cnam));
  file.Read(host,sizeof(host));
  file.Read(mmg,sizeof(mmg));
  file.Read(rtnam,sizeof(rtnam));
  file.Read(port);
  process_status=0;
  fd=-1;
  pid=-1;
}

//
// Save process configuration
//
void process::save()
{
  char path[200];
  struct utsname utname;
  if(uname(&utname)<0)
  {
        perror("uname");
  }

  sprintf(path,"%s/stats/%s.%s",
          getenv("SOCOPS"),utname.nodename,nam);
  RWFile file(path);
  if( !file.isValid() )
  {
      perror(path);
      return;
  }
  
  file.Write(verify);
  file.Write(debug);
  file.Write(realtime);
  file.Write(restart);
  file.Write(downstream);
  file.Write(pktfile,sizeof(pktfile));
  file.Write(options,sizeof(options));
  file.Write(nam,sizeof(nam));
  file.Write(wnam,sizeof(wnam));
  file.Write(cnam,sizeof(cnam));
  file.Write(host,sizeof(host));
  file.Write(mmg,sizeof(mmg));
  file.Write(rtnam,sizeof(rtnam));
  file.Write(port);
}

  
@

head	1.1;
access
	soccm
	savkoor;
symbols;
locks; strict;
comment	@ * @;


1.1
date	97.02.19.21.39.44;	author soccm;	state Exp;
branches;
next	;


desc
@Calculates Quaternions
@


1.1
log
@Initial revision
@
text
@static const char* const rcsId =
"$Id$";

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iostream.h>
#include <iomanip.h>
#include <math.h>

#include "ACSstate.h"

double ACSstate::PCAboreError[3] = {1.0,0.0,0.0} ;
char* ACSstate::PCAboreFile = 0 ;

static const double pi = M_PI;
static const double p2 = 2.0 * M_PI;
static const double crd = 180.0 / M_PI;

static char* readPCAbore(void)
{
	static char* file = "/etc/PCAboreError.dat" ;
	char* sochome = getenv("SOCHOME");
  char line[256] ;
  double x=1, y=0, z=0;

	if( !sochome ) return file ;

	char* fullpath = strcpy(new char[strlen(sochome)+strlen(file)+1], sochome);
	strcat(fullpath,file) ;
	FILE* bfile = fopen(fullpath,"r") ;
  if( !bfile ) return file ;
  while( fgets(line, 255, bfile) ) {
    if( strncmp(line, "//", 2) == 0 ) continue ;
    sscanf(line, "%lf %lf %lf", &x, &y, &z ) ;
    cerr << "Read from " << fullpath << ": "
         << setprecision(10) << x << ", "
         << setprecision(10) << y << ", "
         << setprecision(10) << z << endl ;
  }
  ACSstate::pcaBoreError(x, y, z) ;
  return fullpath ;
}

ACSstate::ACSstate(void) :
  _time(0), _ra(0), _dec(0),
  _latitude(0), _longitude(0), _sunAng(0)
{
  _quats[0] = _quats[1] = _quats[2] = _quats[3] = 0.0 ;
  _earthBody[0] = _earthBody[1] = _earthBody[2] = 0.0 ;
  _sunBody[0] = _sunBody[1] = _sunBody[2] = 0.0 ;
  if( !PCAboreFile ) PCAboreFile = readPCAbore() ;
}

void ACSstate::pcaBoreError(double x, double y, double z)
{
  PCAboreError[0] = x ;
  PCAboreError[1] = z ;
  PCAboreError[2] = y ;
}

float ACSstate::latitude(void) const
{
  return ::crd * _latitude ;
}

float ACSstate::longitude(void) const
{
	return ::crd * ((_longitude > pi) ? _longitude - p2 : _longitude) ;
}
 
float ACSstate::rollBias(void)
{
  const float* s = uSunBody(); 
  return s[0]==0.0 ? 0.0 : (90.0 - (crd*atan2(s[2], s[1])));
}

void ACSstate::quats(const float* f)
{
  _quats[0] = f[0] ;
  _quats[1] = f[1] ;
  _quats[2] = f[2] ;
  _quats[3] = f[3] ;
}

void ACSstate::uSunBody(const float* f)
{
  _sunBody[0] = f[0] ;
  _sunBody[1] = f[1] ;
  _sunBody[2] = f[2] ;
}
 
void ACSstate::uEarthBody(const float* f)
{
  _earthBody[0] = f[0] ;
  _earthBody[1] = f[1] ;
  _earthBody[2] = f[2] ;
}
 
void ACSstate::radec(void)
{

 
  // Set up attitude matrix (direction cosine matrix)

  double q11, q22, q33, q44, q12, q34, q13, q24, q23, q14;

  q11 = _quats[0] * _quats[0];
  q22 = _quats[1] * _quats[1];
  q33 = _quats[2] * _quats[2];
  q44 = _quats[3] * _quats[3];
  q12 = _quats[0] * _quats[1];
  q34 = _quats[2] * _quats[3];
  q13 = _quats[0] * _quats[2];
  q24 = _quats[1] * _quats[3];
  q23 = _quats[1] * _quats[2];
  q14 = _quats[0] * _quats[3];

  double att[3][3];

  att[0][0] = q11 - q22 - q33 + q44;
  att[0][1] = 2.0 * (q12 + q34);
  att[0][2] = 2.0 * (q13 - q24);
  att[1][0] = 2.0 * (q12 - q34);
  att[1][1] = -q11 + q22 - q33 + q44;
  att[1][2] = 2.0 * (q23 + q14);
  att[2][0] = 2.0 * (q13 + q24);
  att[2][1] = 2.0 * (q23 - q14);
  att[2][2] = -q11 - q22 + q33 + q44;
 
  // Do transformation of boresight in body to inertial reference (J2000)
  // V(3,1) = ATT(3,3) X BORE(3,1)

  double v[3] ;

  v[0] = att[0][0] * PCAboreError[0] +
         att[1][0] * PCAboreError[1] +
         att[2][0] * PCAboreError[2] ;
  v[1] = att[0][1] * PCAboreError[0] +
         att[1][1] * PCAboreError[1] +
         att[2][1] * PCAboreError[2] ;
  v[2] = att[0][2] * PCAboreError[0] +
         att[1][2] * PCAboreError[1] +
         att[2][2] * PCAboreError[2] ;
 
  // Extract RA from vector
  // RA = arctan(y/x)
  // atan2 computes angle and phase in range -pi/2 to pi/2
  // Add 2*pi if value is negative because we need RA to be 0 to 360

  _ra = atan2(v[1],v[0]) ;
  if( _ra < 0.0 ) _ra += p2 ;
  _ra *= crd ;

  // Extract DEC from vector
  // DEC = arcsin(z)
  // The vector must be normalized first.

  double r ;
  r = sqrt(v[0]*v[0] + v[1] * v[1] + v[2] * v[2]) ;
  _dec = crd * asin(v[2]/r) ;

}

@

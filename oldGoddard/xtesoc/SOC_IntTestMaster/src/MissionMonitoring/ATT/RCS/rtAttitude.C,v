head	1.1;
access
	soccm
	savkoor;
symbols;
locks; strict;
comment	@ * @;


1.1
date	97.02.19.21.39.44;	author soccm;	state Exp;
branches;
next	;


desc
@real-time client for attitude display
@


1.1
log
@Initial revision
@
text
@// ===========================================================================
// File Name   : rtAttitude.C
// Subsystem   : Data Ingest
// Programmer  : Randall D. Barnette -- Hughes STX
// Description : Realtime client for Realtime Attitude Display.
//
// RCS:
static const char* const rcsid =
"$Id: rtAttitude.C,v 1.1 1996/09/30 15:13:20 soccm Exp $";
//
// ===========================================================================

#include <iostream.h>
#include <new.h>
#include <signal.h>
#include <stdarg.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <SOCMessage.h>

#include "PktTlm.h"
#include "Utilities.h"
#include "rtclient.h"  /* Entry points into librtclient.a */

// The port of the Realtime Server to which we connect to get telemetry.

static const short default_server_port = 0 ;
static       short         server_port = default_server_port ;

// The machine on which the Realtime Server is executing.

static const char* const default_server_host = "xrtserv" ;
static const char*               server_host = default_server_host ;

// Should we read packets from STDIN instead of from the Realtime Server?

static int read_from_stdin = 0 ;

// This is the name we advertise to the Realtime Server. It is the same as
// the entry in the relevant $SOCHOME/etc/RT.CLIENTS table.

static const char* const default_client_name = "attitude-mon" ;
static const char*               client_name = default_client_name ;

// Program name -- set in main(). Not necessarily the same as the name we
// advertise to the Realtime Server. This is used for reporting of errors
// and messages.

static char* ProgName;

// This is the name of the X client that to which we write data needing to
// be displayed.  If it is not specified as a full pathname, we prepend the
// following string to it:
//
//   $SOCHOME/bin/$ARCH/
//
// It is expected to be a executable process.

static const char* const default_display_client = "attitude" ;
static const char*               display_client = default_display_client ;

// This is the file descriptor on which the realtime client writes data
// to its associated X window process.

static int window_fd;

static void issuedBy(const char* s)
{
	char *newName = new char[strlen(ProgName)+strlen(s)+1] ;
	strcpy( newName, ProgName ) ;
	strcat( newName, s ) ;
	SOCMessageManager::issuedBy(newName) ;
  delete newName ;
}

// We get sent a SIGPIPE when we try to do a write() on a broken pipe.
// What this'll mean is that the X client has died (been killed).  We
// want to know when this happens.

static void sigpipe_catcher(int)
{
  cerr << display_client << " is terminated; exiting." << endl ;
  (void) close(window_fd);
	exit(0);
}

// This initializes the X client that the realtime client will be driving.
//
// `pathname' is expected to be the full pathname of the X window process
// that we exec().
//
// Make sure to chose a name that reflects the specific client you're writing
// and that isn't likely to conflict with others.

static void initialize_x_client(const char* pathname)
{

  if(strchr(pathname, '/') == 0)
    cerr << "The full pathname of the X client must be supplied." << endl ;

  int fd[2];

  if(pipe(fd) < 0) {
    cerr << __FILE__ << ", line "
         << __LINE__ << ", pipe() failed."
         << endl ;
    exit(-1) ;
  }

  switch(fork()) {
  case -1: // No more processes available.
    cerr << __FILE__ << ", line "
         << __LINE__ << ", fork() failed."
         << endl ;
    exit(-1) ;
  case 0: // In the child - where the X window process will run.
    {
      issuedBy("-client") ;
      //
      // Set pipe to be standard input and then exec() window code.
      //
      if(STDIN_FILENO != fd[0]) {
        dup2(fd[0], STDIN_FILENO);
        (void) close(fd[0]);
      }
      (void) close(fd[1]);

      //
      // We set the name of the process that we're starting to the tail
      // of the full pathname.  We also set the `-name' of the client as
      // known to X to the same value.
      //
      char* name = strrchr(pathname, '/') + 1;
      if(name == 0)
        //
        // Watch out for jokers who pass "...../".
        //
        name = "";

      execl(pathname, name, "-name", name, (char*) 0);
      //
      // If we get here, the exec() failed.
      //
      syserror("File %s, line %d, execl(%s) failed",
                   __FILE__, __LINE__, pathname);
    }
  default: // In the parent - this is the realtime client.
    //
    // We close fd[0] so that we'll get a SIGPIPE when a
    // write to the X window process fails.
    //
    (void) close(fd[0]);
    window_fd = fd[1];   // The descriptor to which we write the data.
    //
    // So we catch SIGPIPE.
    //
    catch_signal(SIGPIPE, sigpipe_catcher);
  }
}

// This is the routine that does the packet-specific processing.
// It is called by external progrms.
// The routine that calls process_a_packet() deletes the packet
// when this function returns.

void process_a_packet(const PktTlm* pkt)
{
	int len = pkt->packetLength() + 7 ;

	if(!(write_bytes(window_fd, pkt->rawData(), len) == len)) {
		message("Write to display client failed -- will try to restart.");
		(void) close(window_fd);
	}
}

// Print out usage string and exit.  If you change parse_arguments(),
// make the relevant changes here as well.

static void usage(void)
{
	cerr << endl
			 << "usage: " << ProgName << " [options]" << endl << endl
			 << "where options are:                                  (default)"
       << endl
			 << " -host <host>     realtime server machine           ("
       << default_server_host << ")" << endl
			 << " -display <prog>  client program to execute         ("
       << default_display_client << ")" << endl
			 << " -name <client>   name for server client            ("
       << default_client_name << ")" << endl
			 << " -port <port>     realtime server port              ("
       << default_server_port << ")" << endl
			 << " -read-from-stdin read from stdin instead of socket (OFF)" << endl
			 << endl ;
	exit(1);
}

// Parse arguments.

static void parse_arguments(char**& argv)
{
  //
  // Parse any arguments.
  //
  //    -display name        -- name of X client program to execute
  //    -host machine        -- realtime server machine
  //    -port #              -- port number to connect torealtime server
  //    -name name           -- the name we advertise to the realtime server
  //    -read-from-stdin     -- read from stdin instead of over a socket
  //
  while(*++argv && **argv == '-') {
    if(strcmp(*argv, "-host") ==  0) {
      if(!(server_host = *++argv)) error("No hostname supplied.");
    } else if(strcmp(*argv, "-port") == 0) {
      if(*++argv) {
        if((server_port = (short) atoi(*argv)) <= 0)
          error("Port # must be positive short.");
      } else
        error("No port # supplied.");
    } else if(strcmp(*argv, "-display") == 0) {
      if(!(display_client = *++argv)) error("No display client name supplied.");
    } else if(strcmp(*argv, "-name") == 0) {
      if(!(client_name = *++argv)) error("No name supplied.");
    } else if(strcmp(*argv, "-read-from-stdin") == 0) {
      read_from_stdin = 1;
    } else {
      cerr << endl << "Unknown option '" << *argv << "'" << endl ;
      usage();
    }
  }

  //
  // Check that basic environment variables are set.
  //
	const char* arch    ;
	const char* sochome ;
	const char* socops ;

  if((sochome = getenv("SOCHOME")) == 0)
    error("The `SOCHOME' environment variable must be set.");

  if((socops = getenv("SOCOPS")) == 0)
    error("The `SOCOPS' environment variable must be set.");

  if((arch = getenv("ARCH")) == 0)
    error("The `ARCH' environment variable must be set.");

  if(!display_client)
    error("No display client name supplied.");

  if(display_client[0] != '/') {

    //
    // Try to build a full pathname.
    //

    char* pathname = new char[strlen(sochome) + strlen(display_client)+
                                      (arch == 0 ? 0 : strlen(arch)) + 7];

    (void) strcpy(pathname, sochome);
    (void) strcat(pathname, "/bin/");
		(void) strcat(pathname, arch);
		(void) strcat(pathname, "/");
    (void) strcat(pathname, display_client);

    if(!is_regular_file(pathname))
      error("`%s' isn't a regular file.", pathname);

    display_client = pathname;
  }
}

// Simple error handler for use by PktCCSDS.  Change this if you want
// to do something fancier.

static void packet_error_handler (const char* msg) { error(msg); }

// A simple Out-Of-Memory Exception handler for main().  Feel free
// to change this if you really can do better than exit.

static void free_store_exception (void)
{
  message("Out of memory, exiting ...");
  if (!read_from_stdin)
    force_exit_of_other_half();
  exit(1);
}

// The main routine.

int main(int, char** argv)
{
  ProgName = argv[0];
  SOCMessageManager::issuedBy(ProgName) ;
  SOCMessageManager::logToScreen(1) ;

  set_new_handler(free_store_exception);

  PktCCSDS::setErrorHandler(packet_error_handler);

  initialize_signal_handlers(catch_a_signal);

  parse_arguments(argv);

  if(read_from_stdin) {
    //
    // Do client-specific setup.
    //
    initialize_x_client(display_client);
    read_packets_from_stdin();
  } else {
    //
    // Now fork to create a child process. The parent process
    // (aka back-end) reads packets from the Realtime Server and
    // places them in the ring buffer.  The child process
    // (aka front-end) reads these packets and does the appropriate
    // processing.
    //
    if((PID = fork()) == 0) {
      issuedBy("-child") ;
      //
      // This code is only executed by the front-end portion of the
      // realtime client; that part that is actually interested in 
      // the realtime analysis/display of the data in the packets.
      //
      PPID = getppid();
      //
      // Do client-specific setup.
      //
      initialize_x_client(display_client);
      //
      // The primitive event mechanism in process_packets() isn't
      // compatible with the X Event mechanism.  If you want to drive
      // X Windows directly from this code, instead of fork()ing and
      // exec()ing a standalone X Windows process to which you write
      // the required data, please talk to me so we can work out the
      // best way to do it, together.  I have some ideas.  I just
      // haven't felt like putting in the time, if it wasn't clear
      // that it was going to be used.
      //
      process_packets();
    } else {
      //
      // This code is only executed by the back-end of the realtime
      // client.  It is the code that maintains the connection to the
      // realtime server and gives well-built CCSDS Telemetry packets
      // to the front-end of the realtime client.
      //
      (void) catch_signal(SIGCHLD, rtclient_chld_catcher);
      SOCThrow(SOC_INFO, "Connect to %s on %d as %s",
          server_host, server_port, client_name);
      connect_to_realtime_server(client_name, server_port, server_host);
    }
  }

  return 0;
}

@

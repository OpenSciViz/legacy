head	1.2;
access
	soccm
	savkoor;
symbols;
locks; strict;
comment	@ * @;


1.2
date	98.01.16.19.05.19;	author savkoor;	state Exp;
branches;
next	1.1;

1.1
date	97.02.19.21.39.44;	author soccm;	state Exp;
branches;
next	;


desc
@display GUI
@


1.2
log
@fixed memory leaks
@
text
@// ===========================================================================
// File Name   : attitude.C
// Subsystem   : Data Ingest
// Programmer  : Randall D. Barnette -- Hughes STX
// Description : X display for Realtime Attitude Display.
//
// RCS:
static const char* const rcsid =
"$Id: attitude.C,v 1.1 1997/02/19 21:39:44 soccm Exp $";
//
// ===========================================================================
 

// System headers
#include <math.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <netdb.h>
#include <fcntl.h>

// X Windows/Motif headers
#include <X11/Intrinsic.h>
#include <Xm/LabelG.h>
#include <Xm/ToggleBG.h>
#include <Xm/CascadeBG.h>
#include <Xm/MainW.h>
#include <Xm/Frame.h>
#include <Xm/RowColumn.h>
#include <Xm/Form.h>
#include <Xm/MessageB.h>
#include <Xm/SeparatoG.h>
#include <Xm/TextF.h>

// Rogue Wave headers
#include <rw/rwdate.h>

// SOF headers
#include <Utilities.h>
#include <PktTlm.h>
#include <ACS0.h>
#include <ACS3.h>
#include <ACS7.h>
#include <SOCTime2.h>

// Local headers
#include "ACSstate.h"

#include "attitude.xbm"

static const char* FIFONAME = "fifos/xephem_in_fifo" ;
static int fifo = 0 ;

static ACSstate theSCstate;

// --------------------------------------------------------------------------
// Main Window parameters
// --------------------------------------------------------------------------

//
// Some colorful fallback resources -- produced from resource file.
//
static String attitudeFallbacks[] = {
#include "attitude.h"
  (char *) NULL
} ;

struct VarRec {
  char *serverHost ;
  char *clientName ;
  short serverPort ;
} attitudeVarRec ;

static XtAppContext appcontext;
static Widget appshell ;

static Widget UT___v ;
static Widget MET__v ;
static Widget ra___v ;
static Widget dec__v ;
static Widget roll_v ;
//static Widget suna_v ;
static Widget long_v ;
static Widget lat__v ;
static Widget alt__v ;

// --------------------------------------------------------------------------
// The Quaternion Dialog Box
// --------------------------------------------------------------------------

static Widget quatD ;
static Widget q1_v ;
static Widget q2_v ;
static Widget q3_v ;
static Widget q4_v ;
static int quaternions_displayed = 0 ;

void window_done_cb(Widget, XtPointer, XtPointer) ;

// --------------------------------------------------------------------------
// This program starts fast and builds additional widgets in the background.
// --------------------------------------------------------------------------

static int next = 0 ;
Boolean BackgroundConstructor(XtPointer) ;

// --------------------------------------------------------------------------
// statpkt is how we get data.
// --------------------------------------------------------------------------

static PktTlm* pkt ;

// --------------------------------------------------------------------------
// The other widgets.
// --------------------------------------------------------------------------

static int modulus ;
static Widget helpD, workingD, warningD, errorD, quitD, creditsD ;
static Widget disp_menu, wind_menu ;

enum FileOptions {
  FileOptions_RECONNECT = 0,
  FileOptions_CREDITS,
  FileOptions_QUIT_BUTTON
} ;

enum DisplayOption {
  DisplayOption_10_SEC = 0,
  DisplayOption_5_SEC,
  DisplayOption_1_SEC,
  DisplayOption_EVERYTHING
} ;

enum WindowOption {
  WindowOption_QUATERNIONS = 0,
  WindowOption_OCCULTATION,
  WindowOption_BORESIGHT,
  WindowOption_STATISTICS
} ;

enum HelpOption {
  HelpOption_GENERAL = 0,
  HelpOption_INDICATORS,
  HelpOption_STRIPCHART,
  HelpOption_BARCHART,
  HelpOption_HISTOGRAMS,
  HelpOption_STATISTICS
} ;

// --------------------------------------------------------------------------
// Time functions
// --------------------------------------------------------------------------

enum TIME_format {
  UT_format,
  MET_format
} ;

static void setTimeString(Widget w, unsigned long t, TIME_format f)
{
  XmString s ;
  char buf[64] ;
  static const RWTime rwt0 = SOCTime2::getRW_SC0() ;

  switch( f ) {
  case UT_format :
    {
      RWTime rwt(rwt0) ;
      rwt += t ;
      RWDate rwd(rwt,RWZone::utc()) ;
      sprintf( buf, "%4d:%03d:%02d:%02d:%02d",
        rwd.year(), rwd.day(), rwt.hourGMT(), rwt.minuteGMT(), rwt.second());
    }
    break ;
  case MET_format :
    (void) sprintf(buf, "%d", t ) ;
    break ;
  }
  s = XmStringCreateLocalized(buf) ;
  XtVaSetValues(w, XmNlabelString, s, NULL);
  XmStringFree(s);
}

//
// Make the table of attitude statistics.
//

static Widget make_attpos_table(Widget parent)
{

  Widget t[4] ;
  Widget a[8] ;
  Widget p[8] ;
  int i = 0 ;

// The hierarchy is:
//                      apt_F
//                        |
//                -----------------
//                |               |
//              timeR           atpoR
//                                |
//                       -----------------
//                       |               |
//                     att_R           pos_R

  Widget apt_F = XmCreateRowColumn( parent,"apt_F", NULL, 0 ) ;
  Widget timeR = XmCreateRowColumn( apt_F ,"timeR", NULL, 0 ) ;
  Widget atpoR = XmCreateRowColumn( apt_F ,"atpoR", NULL, 0 ) ;
  Widget att_R = XmCreateRowColumn( atpoR ,"att_R", NULL, 0 ) ;
  Widget pos_R = XmCreateRowColumn( atpoR ,"pos_R", NULL, 0 ) ;

  UT___v = t[i++] = XmCreateLabelGadget( timeR,"UT___v", NULL, 0 ) ;
           t[i++] = XmCreateLabelGadget( timeR,"UT___l", NULL, 0 ) ;
  MET__v = t[i++] = XmCreateLabelGadget( timeR,"MET__v", NULL, 0 ) ;
           t[i++] = XmCreateLabelGadget( timeR,"MET__l", NULL, 0 ) ;

  XtManageChildren(t,i) ;

           i = 0 ;
           a[i++] = XmCreateLabelGadget( att_R,"ra___l", NULL, 0 ) ;
           a[i++] = XmCreateLabelGadget( att_R,"dec__l", NULL, 0 ) ;
           a[i++] = XmCreateLabelGadget( att_R,"roll_l", NULL, 0 ) ;
           //a[i++] = XmCreateLabelGadget( att_R,"sunv_l", NULL, 0 ) ;

  ra___v = a[i++] = XmCreateLabelGadget( att_R,"ra___v", NULL, 0 ) ;
  dec__v = a[i++] = XmCreateLabelGadget( att_R,"dec__v", NULL, 0 ) ;
  roll_v = a[i++] = XmCreateLabelGadget( att_R,"roll_v", NULL, 0 ) ;
  //suna_v = a[i++] = XmCreateLabelGadget( att_R,"suna_v", NULL, 0 ) ;

  XtManageChildren(a,i) ;

           i = 0 ;
           p[i++] = XmCreateLabelGadget( pos_R,"long_l", NULL, 0 ) ;
           p[i++] = XmCreateLabelGadget( pos_R,"lat__l", NULL, 0 ) ;
           p[i++] = XmCreateLabelGadget( pos_R,"alt__l", NULL, 0 ) ;
           //p[i++] = XmCreateLabelGadget( pos_R,"blankl", NULL, 0 ) ;

  long_v = p[i++] = XmCreateLabelGadget( pos_R,"long_v", NULL, 0 ) ;
  lat__v = p[i++] = XmCreateLabelGadget( pos_R,"lat__v", NULL, 0 ) ;
  alt__v = p[i++] = XmCreateLabelGadget( pos_R,"alt__v", NULL, 0 ) ;
           //p[i++] = XmCreateLabelGadget( pos_R,"blankl", NULL, 0 ) ;

  XtManageChildren(p,i) ;

  XtManageChild(apt_F) ;
  XtManageChild(timeR) ;
  XtManageChild(atpoR) ;
  XtManageChild(att_R) ;
  XtManageChild(pos_R) ;

  return apt_F ;
}

//
// Make the table of quaternions
//

static Widget make_quat_table(Widget parent)
{

  Widget a[8] ;
  int i = 0 ;

// The hierarchy is:
//                  quat_R
//                    |
//                ----------
//                |  |  |  |
//                q1 q2 q3 q4

  Widget quat_D = XmCreateTemplateDialog( parent,"quat_D", NULL, 0 ) ;
  Widget quat_R = XmCreateRowColumn( quat_D,"quat_R", NULL, 0 ) ;

         i = 0 ;
         a[i++] = XmCreateLabelGadget( quat_R,"q1_l", NULL, 0 ) ;
         a[i++] = XmCreateLabelGadget( quat_R,"q2_l", NULL, 0 ) ;
         a[i++] = XmCreateLabelGadget( quat_R,"q3_l", NULL, 0 ) ;
         a[i++] = XmCreateLabelGadget( quat_R,"q4_l", NULL, 0 ) ;

  q1_v = a[i++] = XmCreateLabelGadget( quat_R,"q1_v", NULL, 0 ) ;
  q2_v = a[i++] = XmCreateLabelGadget( quat_R,"q2_v", NULL, 0 ) ;
  q3_v = a[i++] = XmCreateLabelGadget( quat_R,"q3_v", NULL, 0 ) ;
  q4_v = a[i++] = XmCreateLabelGadget( quat_R,"q4_v", NULL, 0 ) ;

  XtManageChildren(a,i) ;
  XtManageChild(quat_R) ;

  XtAddCallback(quat_D,XmNokCallback,window_done_cb,(XtPointer)WindowOption_QUATERNIONS);
  quaternions_displayed = 0 ;
  return quat_D ;
}

Widget make_credits_popup(Widget parent)
{
  // These are automatically filled in by RCS. Do not edit!
  static const char* rcs[] = {
    "$Author: soccm $",
    "$Date: 1997/02/19 21:39:44 $",
    "$Revision: 1.1 $",
    "$State: Exp $",
    "$Source: /home/gof/savkoor/src/MM2/ATT/RCS/attitude.C,v $"
  } ;

  Arg arg[6] ;
  XmString s = XmStringCreateLtoR("Written by Randy Barnette",
                         XmSTRING_DEFAULT_CHARSET) ;
 
  XtSetArg(arg[0], XmNmessageString, s ) ;
  Widget w = XmCreateInformationDialog(parent, "creditsD", arg, 1 ) ;
  XmStringFree(s) ;

  Widget c = XmCreateRowColumn(w, "creditsrc", NULL, 0 ) ;

  XtSetArg(arg[0], XmNmarginWidth, 0 ) ;
  Widget q = XmCreateSeparatorGadget( c, "creditsS", arg, 1 ) ;
  Widget m = XmCreateLabelGadget( c, "creditsL", arg, 1 ) ;

  Widget f = XmCreateFrame(c, "creditsF", NULL, 0 ) ;

  XtSetArg(arg[0], XmNmarginWidth, 0 ) ;
  XtSetArg(arg[1], XmNmarginHeight, 0 ) ;
  XtSetArg(arg[2], XmNborderWidth, 0 ) ;
  Widget r = XmCreateForm(f, "creditsR", arg, 3 ) ;

  char *st = "RCS info:" ;
  s = XmStringCreateLocalized(st) ;
  XtVaSetValues(m, XmNlabelString, s, NULL ) ;
  XmStringFree(s) ;

  Widget items[10] ;

  char buf[128] ;
  for( int i = 0 ; i < 10 ; i++ ) {
    sprintf(buf, "credits_%d", i ) ;
    switch(i) {
    case 0:
      XtSetArg( arg[0], XmNalignment,       XmALIGNMENT_END) ;
      XtSetArg( arg[1], XmNtopAttachment,   XmATTACH_FORM) ;
      XtSetArg( arg[2], XmNrightAttachment, XmATTACH_OPPOSITE_FORM) ;
      XtSetArg( arg[3], XmNmarginWidth,     3) ;
      XtSetArg( arg[4], XmNrightOffset,     -80) ;
      XtSetArg( arg[5], XmNrightOffset,     -80) ;
      break ;
    case 1: case 2: case 3: case 4:
      XtSetArg( arg[0], XmNalignment,       XmALIGNMENT_END) ;
      XtSetArg( arg[1], XmNtopAttachment,   XmATTACH_WIDGET) ;
      XtSetArg( arg[2], XmNrightAttachment, XmATTACH_OPPOSITE_FORM) ;
      XtSetArg( arg[3], XmNtopWidget,       items[i-1] ) ;
      XtSetArg( arg[4], XmNmarginWidth,     3);
      XtSetArg( arg[5], XmNrightOffset,     -80);
      break ;
    case 5:
      XtSetArg( arg[0], XmNtopAttachment,   XmATTACH_FORM) ;
      XtSetArg( arg[1], XmNleftAttachment,  XmATTACH_WIDGET) ;
      XtSetArg( arg[2], XmNleftWidget,      items[0] ) ;
      XtSetArg( arg[3], XmNmarginWidth,     3) ;
      XtSetArg( arg[4], XmNrightOffset,     -80) ;
      XtSetArg( arg[5], XmNrightOffset,     -80) ;
      break ;
    case 6: case 7: case 8: case 9:
      XtSetArg( arg[0], XmNtopAttachment,   XmATTACH_WIDGET) ;
      XtSetArg( arg[1], XmNleftAttachment,  XmATTACH_WIDGET) ;
      XtSetArg( arg[2], XmNtopWidget,       items[i-1] ) ;
      XtSetArg( arg[3], XmNleftWidget,      items[i-5] ) ;
      XtSetArg( arg[4], XmNmarginWidth,     3) ;
      XtSetArg( arg[5], XmNrightOffset,     -80) ;
      break ;
    }
    items[i] = XmCreateLabelGadget( r, buf, arg, 6 ) ;
  }

  char *p ;
  int term ;

  for( i = 0 ; i < 5 ; i++ ) {
    term = strlen(rcs[i]) - 3 ;
    strncpy( buf, rcs[i]+1, term ) ;
    buf[term] = '\0' ;
    p = strchr(buf, ':') ; ++p ; *p = '\0' ; ++p ;
    s = XmStringCreateLocalized(buf) ;
    XtSetArg(arg[0], XmNlabelString, s ) ;
    XtSetValues( items[i], arg, 1 ) ;
    XmStringFree(s) ;
    s = XmStringCreateLocalized(p) ;
    XtSetArg(arg[0], XmNlabelString, s ) ;
    XtSetValues( items[i+5], arg, 1 ) ;
    XmStringFree(s) ;
  }

  XtUnmanageChild(XmMessageBoxGetChild(w,XmDIALOG_CANCEL_BUTTON));
  XtManageChild(f) ;
  XtManageChild(c) ;
  XtManageChild(q) ;
  XtManageChild(m) ;
  XtManageChild(r) ;
  XtManageChildren(items,10) ;

  return w ;
}

static void updateValues(void)
{
  XmString s ;
  char buf[32] ;
  unsigned long stime = theSCstate.time() ;
  static float conv = M_PI / 180.0 ;

  setTimeString(UT___v,stime,UT_format);
  setTimeString(MET__v,stime,MET_format);

  theSCstate.radec() ;

  if( fifo ) {
    (void) sprintf( buf, "RA:%9.6f Dec:%9.6f Epoch:2000.0 FOV:2.0\n",
      theSCstate.ra() * conv, theSCstate.dec() * conv ) ;
    (void) write(fifo,buf,strlen(buf));
  }

  (void) sprintf(buf, "%7.2f\260  ", theSCstate.ra() ) ;
  s = XmStringCreateLocalized(buf) ;
  XtVaSetValues(ra___v, XmNlabelString, s, NULL);
  XmStringFree(s);

  (void) sprintf(buf, "%+7.2f\260  ", theSCstate.dec() ) ;
  s = XmStringCreateLocalized(buf) ;
  XtVaSetValues(dec__v, XmNlabelString, s, NULL);
  XmStringFree(s);

  (void) sprintf(buf, "%+7.2f\260  ", theSCstate.rollBias() ) ;
  s = XmStringCreateLocalized(buf) ;
  XtVaSetValues(roll_v, XmNlabelString, s, NULL);
  XmStringFree(s);

  float lo, la ;
  char ew,ns;

  lo = theSCstate.longitude() ;
  la = theSCstate.latitude() ;
  if( lo < 0.0 ) {
    lo *= -1.0 ;
    ew = 'W' ;
  } else {
    ew = 'E' ;
  }
	(void) sprintf(buf, "%6.2f\260 %c   ", lo, ew ) ;
  s = XmStringCreateLocalized(buf) ;
  XtVaSetValues(long_v, XmNlabelString, s, NULL);
  XmStringFree(s);

  if( la < 0.0 ) {
    la *= -1.0 ;
    ns = 'S' ;
  } else {
    ns = 'N' ;
  }
	(void) sprintf(buf, "%6.2f\260 %c   ", la, ns ) ;
  s = XmStringCreateLocalized(buf) ;
  XtVaSetValues(lat__v, XmNlabelString, s, NULL);
  XmStringFree(s);

/*
  (void) sprintf(buf, "%+7.2f\260  ", theSCstate.sunAng() ) ;
  s = XmStringCreateLocalized(buf) ;
  XtVaSetValues(suna_v, XmNlabelString, s, NULL);
  XmStringFree(s);
*/

  (void) sprintf(buf, "%7.2f Km", theSCstate.altitude() ) ;
  s = XmStringCreateLocalized(buf) ;
  XtVaSetValues(alt__v, XmNlabelString, s, NULL);
  XmStringFree(s);

  if( quaternions_displayed ) {
    const float* q = theSCstate.quats() ;
    (void) sprintf(buf, "%f", q[0] ) ;
    s = XmStringCreateLocalized(buf) ;
    XtVaSetValues(q1_v, XmNlabelString, s, NULL);
    XmStringFree(s);

    (void) sprintf(buf, "%f", q[1] ) ;
    s = XmStringCreateLocalized(buf) ;
    XtVaSetValues(q2_v, XmNlabelString, s, NULL);
    XmStringFree(s);

    (void) sprintf(buf, "%f", q[2] ) ;
    s = XmStringCreateLocalized(buf) ;
    XtVaSetValues(q3_v, XmNlabelString, s, NULL);
    XmStringFree(s);

    (void) sprintf(buf, "%f", q[3] ) ;
    s = XmStringCreateLocalized(buf) ;
    XtVaSetValues(q4_v, XmNlabelString, s, NULL);
    XmStringFree(s);
  }

}

void attitudeHandler(const ACS0* thePart)
{
  static unsigned long x = 0 ;

  if( ++x % 40 ) return ;

  float q[4] ;
 
  theSCstate.time((unsigned long) thePart->time() ) ;

  q[0] = thePart->qx() ;
  q[1] = thePart->qy() ;
  q[2] = thePart->qz() ;
  q[3] = thePart->qs() ;

  theSCstate.quats(q) ;

  updateValues() ;
}

void configHandler(const ACS3* thePart)
{
  static unsigned long last = 0 ;
  unsigned long t = thePart->time() ;
  if( t < last ) return ;
  last = t ;
  theSCstate.latitude(thePart->earthLat()) ;
  theSCstate.longitude(thePart->earthLon()) ;
  theSCstate.sunAng(acos(thePart->sunAvoidance())) ;
}

void atmosHandler(const ACS7* thePart)
{
  static unsigned long last = 0 ;
  unsigned long t = thePart->time() ;
  if( t < last ) return ;
  last = t ;
  theSCstate.altitude(thePart->altitude()) ;
}

void PktHandler(const PktSC* thePkt)
{
  static float e[3], s[3] ;
  static unsigned long last[4] = {0,0,0,0};
  unsigned long t = thePkt->seconds() ;

  switch(thePkt->applicationID()) {
  case 24 :
    if( t < last[0] ) break ;
    last[0] = t ;
    s[0] = thePkt->SFP(0) ;
    s[1] = thePkt->SFP(4) ;
    s[2] = thePkt->SFP(8) ;
    e[0] = thePkt->SFP(12) ;
    e[1] = thePkt->SFP(16) ;
    e[2] = thePkt->SFP(20) ;
    theSCstate.uSunBody(s) ;
    theSCstate.uEarthBody(e) ;
    break ;
  default :
    break ;
  }
}

// --------------------------------------------------------------------------
// Read new data, extend the strip chart and update the statistics table.
// --------------------------------------------------------------------------

static void read_more_data(XtPointer, int* source, XtInputId* id)
{
  static PktTlm pMaker(Exemplar()) ;
  pkt = pMaker.make(*source) ;

  if( !pkt ) {

    XtRemoveInput(*id);
    // Force window uniconified
    XMapWindow(XtDisplay(appshell), XtWindow(appshell));
    XtSetSensitive(appshell, FALSE);          // Make window insensitive
    XtManageChild(errorD); // Popup error message
    XBell(XtDisplay(appshell),10) ;

  } else {
    pkt->apply() ;
    delete pkt;
  }
  return ;
}

void quit_cb(Widget w, XtPointer, XtPointer)
{
  XtDestroyApplicationContext(XtWidgetToApplicationContext(w));
  exit(0) ;
}

void reconnect_cb(Widget, XtPointer client_data, XtPointer)
{
  next = 10 ;
  XtManageChild(workingD) ;
  XtAppAddWorkProc(appcontext, BackgroundConstructor, (Widget) client_data) ;
  XtSetSensitive(appshell, TRUE);          // Make window sensitive
}

void file_cb(Widget, XtPointer which, XtPointer)
{
  switch( (FileOptions) which ) {
  case FileOptions_RECONNECT   : XtManageChild(warningD) ; break ;
  case FileOptions_CREDITS     : XtManageChild(creditsD) ; break ;
  case FileOptions_QUIT_BUTTON : XtManageChild(quitD) ; break ;
  }
}

void disp_cb(Widget, XtPointer which, XtPointer data)
{
  XmToggleButtonCallbackStruct* call = (XmToggleButtonCallbackStruct*) data ;
  if( (call->reason == XmCR_VALUE_CHANGED) && (call->set == False) )
    return ;
  switch( (DisplayOption) which ) {
  case DisplayOption_10_SEC:
    modulus = 10 ;
    break ;
  case DisplayOption_5_SEC:
    modulus = 5 ;
    break ;
  case DisplayOption_1_SEC:
    modulus = 1 ;
    break ;
  case DisplayOption_EVERYTHING:
    modulus = 0 ;
    break ;
  }
  cerr << "modulus = " << modulus << endl ;
}

void window_done_cb(Widget, XtPointer data, XtPointer)
{
  static WidgetList ch = 0 ;
  if( !ch ) XtVaGetValues(wind_menu, XmNchildren, &ch, NULL) ;
  XmToggleButtonGadgetSetState(ch[(int) data],False,True) ;
  return ;
}

void window_cb(Widget w, XtPointer which, XtPointer data)
{
  XmToggleButtonCallbackStruct* call = (XmToggleButtonCallbackStruct*) data ;

  switch( (WindowOption) which ) {
  case WindowOption_QUATERNIONS :
    quaternions_displayed = call->set ;
    w = quatD ;
    break ;
  case WindowOption_OCCULTATION :
    XtManageChild(warningD) ;
    return ;
  case WindowOption_BORESIGHT :
    XtManageChild(warningD) ;
    return ;
  case WindowOption_STATISTICS :
    XtManageChild(warningD) ;
    return ;
  }

  if( call->set == True ) XtManageChild(w) ;
  else XtUnmanageChild(w) ;
  return ;
}

void main_help_cb(Widget, XtPointer which, XtPointer)
{
  switch( (HelpOption) which ) {
  case HelpOption_GENERAL : break ;
  case HelpOption_INDICATORS : break ;
  case HelpOption_STRIPCHART : break ;
  case HelpOption_BARCHART : break ;
  case HelpOption_HISTOGRAMS : break ;
  case HelpOption_STATISTICS : break ;
  }

  XtManageChild(helpD) ;
}

Boolean BackgroundConstructor(XtPointer p)
{
	static char* socops = 0 ;
  static int fd = -1 ;
  Widget parent = (Widget) p ;
  char buf[256] ;

  switch( next ) {

  case 0 : // Open standard input
    fd = fileno(stdin);
    ACS0::setDisposition(attitudeHandler) ; // apid 14
    ACS3::setDisposition(configHandler) ; // apid 17
    ACS7::setDisposition(atmosHandler) ; // apid 21
    PktACS::setDisposition(PktHandler) ; // apid 24
    next++ ;
    break ;

  case 1 : // make popups
    errorD = XmCreateErrorDialog(parent, "errorD", NULL, 0 ) ;
    XtAddCallback(errorD,XmNokCallback,reconnect_cb,parent) ;
    XtAddCallback(errorD,XmNcancelCallback,quit_cb,0) ;
    next++ ;
    break ;

  case 2 : // make popups
    quitD = XmCreateQuestionDialog(parent, "quitD", NULL, 0 ) ;
    XtAddCallback(quitD,XmNokCallback,quit_cb,0) ;
    XtUnmanageChild(XmMessageBoxGetChild(quitD,XmDIALOG_HELP_BUTTON));
    next++ ;
    break ;

  case 3 : // make popups
    creditsD = make_credits_popup(parent) ;
    next++ ;
    break ;

  case 4 : // make popups
    warningD = XmCreateWarningDialog(parent, "warningD", NULL, 0 ) ;
    XtUnmanageChild(XmMessageBoxGetChild(warningD,XmDIALOG_CANCEL_BUTTON));
    XtUnmanageChild(XmMessageBoxGetChild(warningD,XmDIALOG_HELP_BUTTON));
    next++ ;
    break ;

  case 5 : // make popups
    helpD = XmCreateWarningDialog(parent, "helpD", NULL, 0 ) ;
    XtUnmanageChild(XmMessageBoxGetChild(helpD,XmDIALOG_CANCEL_BUTTON));
    XtUnmanageChild(XmMessageBoxGetChild(helpD,XmDIALOG_HELP_BUTTON));
    next++ ;
    break ;

  case 6 :
    {
      WidgetList ch ;
      XtVaGetValues(disp_menu, XmNchildren, &ch, NULL) ;
      XmToggleButtonGadgetSetState(ch[0],True,True) ;
    }
    next++ ;
    break ;

  case 7 : // make popups
    quatD = make_quat_table(parent) ;
    next++ ;
    break ;

  case 8 :

    next++ ;

    if( !(socops = getenv( "SOCOPS" ))) break ;

		sprintf(buf,"%s/%s", socops, FIFONAME ) ;

		// unlink(buf) ;

		// if( mkfifo( buf, 0666 ) < 0 ) {
		//   perror("mkfifo") ;
    //   break ;
		// }

    // The fifo is written by this program, so it should only be opened
    // with O_WRONLY, however, fifo rules dictate that an open for
    // writing-only will block until a process opens the file for reading.
    // Unfortunately, if O_NONBLOCK is set, an open for writing-only will
    // return an error if no process currently has the file open for reading.
    // On the other hand, if O_NONBLOCK is set, an open for READING-only will
    // return without delay; this is what we really want. So the fifo gets
    // openned read-write with O_NONBLOCK set.
    // Thus, we can now write the fifo regardless of whether anything is
    // listening!

		if( (fifo = open(buf, O_RDWR | O_NONBLOCK )) < 0 ) {
			perror("open") ;
      fifo = 0 ;
		}

    break ;
  case 9 :
    XtAppAddInput(appcontext, fd, (XtPointer) XtInputReadMask,
                read_more_data, (XtPointer) &fd);
    next++ ;
    break ;

  default :
#ifdef DEBUG
    cerr << "Done constructing" << endl ;
#endif /* DEBUG */
    return True ;

  }

  return False ;
}

//
// Print the error message and die.
//
void usage(int argc, char** argv)
{
  if( argc > 1 )
    cerr << endl << "Error: unknown option '" << argv[1] << "'" << endl
         << endl ;

  cerr << "Usage: " << argv[0]
       << " [-host server-host] [-port server-port] [-client client-name]"
       << endl << endl ;

  exit(-1) ;
}

//
// These are non-widget resources for this application. They are added
// in this fashion to take advantage of the built-in X Windows
// command-line parsing.
//
static XtResource attitudeResources[] = {
  //
  // serverHost is the machine we will connect to for real-time packet
  // service. The default is xrtserv.
  // This resource corresponds to command-line option -host.
  //
  {
    "serverHost", "ServerHost", XtRString, sizeof(String),
    XtOffsetOf(VarRec, serverHost), XtRString, (XtPointer) "xrtserv"
  } ,
  //
  // serverPort is the socket port number we will connect to for real-time
  // packet service. The default is zero, which forces the service to be
  // "looked up" in the services entries.
  // This resource corresponds to command-line option -port.
  //
  {
    "serverPort", "ServerPort", XtRShort, sizeof(short),
    XtOffsetOf(VarRec, serverPort), XtRImmediate, (XtPointer) 0
  } ,
  //
  // clientName is the string to identify this application to the realtime
  // server process. The default is attitude-mon.
  // This resource corresponds to command-line option -client.
  //
  {
    "clientName", "ClientName", XtRString, sizeof(String),
    XtOffsetOf(VarRec, clientName), XtRString, (XtPointer) "attitude-mon"
  }
} ;

//
// The following identify the text and value of command-line
// arguments.
//
static XrmOptionDescRec attitudeOptions[] = {
  // option     resource     value-type       default-value
  { "-host"  , "serverHost", XrmoptionSepArg, (XPointer) NULL },
  { "-port"  , "serverPort", XrmoptionSepArg, (XPointer) NULL },
  { "-client", "clientName", XrmoptionSepArg, (XPointer) NULL }
} ;

int main(int argc, char** argv)
{

  Pixmap attitude_pixmap;

  appshell = XtAppInitialize(&appcontext, "Attitude",
                             attitudeOptions, XtNumber(attitudeOptions), 
                             &argc, argv, attitudeFallbacks, NULL, 0);

  if( argc != 1 ) usage(argc, argv) ;

  XtGetApplicationResources(appshell, &attitudeVarRec, attitudeResources,
                            XtNumber(attitudeResources), NULL, 0) ;

#ifdef DEBUG

  String s ;

  char* x ;

  if( x = getenv("XUSERFILESEARCHPATH") ) {

    char* xufsp = strcpy( new char[strlen(x)+1], x ) ;

    char* p = strtok(xufsp,":") ;

    do {
      s = XtResolvePathname(XtDisplay(appshell),"app-defaults",
            NULL,NULL,p,NULL,0,NULL);
      if( s ) break ;
    } while( p = strtok(0,":") ) ;
  }

  if( !s )
    s = XtResolvePathname(XtDisplay(appshell),"app-defaults",
          NULL,NULL,NULL,NULL,0,NULL);

  if( s ) cerr << "Resource file is " << s << endl ;
  else    cerr << "No Resource file found" << endl ;

#endif

  (void) signal(SIGINT,  SIG_IGN);
#ifndef DEBUG
  //
  // So I can get core dumps when debugging.
  //
  (void) signal(SIGQUIT, SIG_IGN);
#endif
  (void) signal(SIGTSTP, SIG_IGN);

  attitude_pixmap = XCreateBitmapFromData(XtDisplay(appshell),
                                     RootWindowOfScreen(XtScreen(appshell)),
                                     (char*) attitude_bits,
                                     attitude_width, attitude_height);
  if (attitude_pixmap == 0)
    printf("File %s, line %d, couldn't make XTE pixmap.",
          __FILE__, __LINE__);
  else
    XtVaSetValues(appshell, XtNiconPixmap, attitude_pixmap, NULL);

  // The main window

  Widget main_w = XmCreateMainWindow(appshell,"main_w", NULL, 0 ) ;

  Widget main_helpD = XmCreateInformationDialog(main_w, "main_helpD", NULL, 0 ) ;

  // The Menubar

  Widget menubar = XmVaCreateSimpleMenuBar(main_w, "menubar",
                   XmVaCASCADEBUTTON, NULL, 0,
                   XmVaCASCADEBUTTON, NULL, 0,
                   XmVaCASCADEBUTTON, NULL, 0,
                   NULL ) ;

  // I can't add this one to the simple list because I need an ID to make
  // it the special help button.

  Widget helpB = XmCreateCascadeButtonGadget(menubar, "button_3", NULL, 0 ) ;
  XtVaSetValues(menubar, XmNmenuHelpWidget, helpB, NULL ) ;
  XtManageChild(helpB) ;

  // The File menu

  Widget file_menu = XmVaCreateSimplePulldownMenu(menubar, "file_menu",
                     0, file_cb,
                     XmVaPUSHBUTTON, NULL, NULL, NULL, NULL,
                     XmVaPUSHBUTTON, NULL, NULL, NULL, NULL,
                     XmVaSEPARATOR,
                     XmVaPUSHBUTTON, NULL, NULL, NULL, NULL,
                     NULL ) ;

  // The Display menu

         disp_menu = XmVaCreateSimplePulldownMenu(menubar, "disp_menu",
                     1, disp_cb,
                     XmNradioBehavior, True,
                     XmNradioAlwaysOne, True,
                     XmNbuttonSet, 0,
                     XmVaRADIOBUTTON, NULL, NULL, NULL, NULL,
                     XmVaRADIOBUTTON, NULL, NULL, NULL, NULL,
                     XmVaRADIOBUTTON, NULL, NULL, NULL, NULL,
                     XmVaRADIOBUTTON, NULL, NULL, NULL, NULL,
                     NULL ) ;

  // The Windows menu

         wind_menu = XmVaCreateSimplePulldownMenu(menubar, "window_menu",
                     2, window_cb,
                     XmVaCHECKBUTTON, NULL, NULL, NULL, NULL,
                     XmVaCHECKBUTTON, NULL, NULL, NULL, NULL,
                     XmVaCHECKBUTTON, NULL, NULL, NULL, NULL,
                     XmVaCHECKBUTTON, NULL, NULL, NULL, NULL,
                     NULL ) ;

  // The HELP menu

  Widget help_menu = XmVaCreateSimplePulldownMenu(menubar, "help_menu",
                     3, main_help_cb,
                     XmVaPUSHBUTTON, NULL, NULL, NULL, NULL,
                     XmVaPUSHBUTTON, NULL, NULL, NULL, NULL,
                     XmVaPUSHBUTTON, NULL, NULL, NULL, NULL,
                     XmVaPUSHBUTTON, NULL, NULL, NULL, NULL,
                     XmVaPUSHBUTTON, NULL, NULL, NULL, NULL,
                     XmVaPUSHBUTTON, NULL, NULL, NULL, NULL,
                     NULL ) ;

  XtManageChild(main_w) ;
  XtManageChild(menubar) ;

  Widget box = make_attpos_table(main_w);

  XmMainWindowSetAreas(main_w, menubar, NULL, NULL, NULL, box ) ;

  XtRealizeWidget(appshell);

  XtAppAddWorkProc(appcontext,BackgroundConstructor,main_w) ;

  XtAppMainLoop(appcontext);

  return 0;
}
@


1.1
log
@Initial revision
@
text
@d9 1
a9 1
"$Id: attitude.C,v 1.1 1995/10/17 17:00:13 soccm Exp soccm $";
d303 2
a304 2
    "$Date: 6/13/96 13:40:24 EDT $",
    "$Revision: 1.2 $",
d306 1
a306 1
    "$Source: /homebck/Build/src/Ingest/attitude-win/RCS/attitude.C,v $"
d587 1
@

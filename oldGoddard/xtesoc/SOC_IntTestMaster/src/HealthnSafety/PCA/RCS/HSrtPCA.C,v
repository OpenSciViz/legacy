head	2.2;
access
	gmanicka
	soccm;
symbols
	Build2:2.2;
locks; strict;
comment	@ * @;


2.2
date	93.12.28.20.19.31;	author gmanicka;	state Release;
branches;
next	2.1;

2.1
date	93.12.09.02.49.36;	author gmanicka;	state Release;
branches;
next	;


desc
@Creation
@


2.2
log
@"."
@
text
@//
// HSrtPCA.C            
//
//
// This is a sample realtime client of the Realtime Server using TCP/IP.
// Writers of realtime clients should use this code as a template for the
// necessary communication.  This client is slightly different than the
// UDP one I released earlier.  In particular, note that this client will
// daemonize by default; you probably want to use the `-do-not-daemonize'
// flag when debugging.  You also need to pass your name to the realtime
// server on startup; use the `-name' option to do this.  The realtime
// server maintains a mapping of client names to lists of ApIDs; by passing
// your name to the realtime server, it then knows which ApIDs it should pass
// you.
//
// Take a look at `process_a_packet(const PktTlm* pkt)'.  You need only
// replace this function with one of your own.  It would be wisest to put
// your code into a separate module so that updates to this template require
// as few modifications to your code as possible.  Note that
// `process_a_packet(const PktTlm* pkt)' is called for each packet read from
// the realtime server.  You should not try to delete the packet.  If you need
// to maintain (cache) packets you'll need to make a copy of the packet passed
// into `process_a_packet(const PktTlm* pkt)' using the copy constructor.
//

static const char rcsid[] = "$Id$";

#include <iostream.h>
#include <limits.h>
#include <new.h>
#include <setjmp.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <sys/param.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/stat.h>
#include <unistd.h>
#include <math.h>
#include <signal.h>

//
// We include the class HKMdataPCA which contains the methods
// to decode the PCA telemetry packets. 
//

#include "HKMdataPCA.h"

//
// These functions aren't prototyped with ObjectCenter 2.0 on my Sun.
// They may not be defined on your machine either.
//
#if defined(sun)
extern "C"
{
#ifdef __GNUG__
    int openlog (const char*, int, int);
    int syslog  (int, char*, ...);
#else
    int umask   (int);
#endif
}
#endif


//
// Some POSIX stuff.
//
#ifndef EXIT_SUCCESS
#define EXIT_SUCCESS 0
#endif
#ifndef EXIT_FAILURE
#define EXIT_FAILURE 1
#endif


//
// Settable options.
//
const int maxparams = 5;      // maximum number of H&S parameters to monitor
static short port;            // The Realtime Server port we connect to.
static int do_not_daemonize;  // Do not run as a daemon.
static const char* our_name;  // The name we advertise to the Realtime Server.
static const char* hostname;  // The Realtime Server machine.
static const char* param[maxparams] ; // parameter names to be monitored

//
// Declare global instance of class HKMdataPCA
//

static HKMdataPCA PCAdata;

//
// Declare global pointers to pipe to mplot
//

static FILE *sfp[maxparams];
static int pindex;

//
// Our name -- set in main().  Not necessarily the same as the name
// we advertise to the Realtime Server.  This is used for reporting of errors
// and messages.
//
static char* ProgName;

//
// We need to catch SIGPIPE signal
// So we define a SIGPIPE signal handler
//
static sigjmp_buf sigjmpbuf;
static int canjump = 0;

static void sigpipe_handler(int signo)
{
  if (canjump == 0) return;	// spurious SIGPIPE - ignore
  if (signo == SIGPIPE) {
	pclose(sfp[pindex]);
	sfp[pindex] = (FILE *)0;
  }
  canjump = 0;
  siglongjmp(sigjmpbuf, 1);	// do not return, but jump back
}



//
// Print out usage string and exit.
//

static void usage (void)
{
    cerr << "\nusage: " << ProgName << " [-do-not-daemonize] -host hostname"
         << " -apid # -name client-name"
         << " -port # -select param1 [param2 param3 ... param5] \n\n";
    exit(1);
}


//
// This initializes our error handler.  This forces open the connection
// between us and syslogd().  We do this because it is quite possible that
// when running in production, Ingest will already have open() the maximum
// number of file descriptors allowed by the OS.  We do this only when
// running as a daemon, which is the usual way we run.  It is only when
// debugging that `do_not_daemonize' gets set.
//

static void initialize_error_handling (const char* program)
{
    openlog(program, LOG_CONS | LOG_NDELAY, LOG_DAEMON);
}


//
// Our error-handling function.  This assumes that /etc/syslog.conf is set up
// to send all LOG_ERR messages both to /dev/console and to /usr/adm/messages
// or other system-defined logfile. If we're debugging and we haven't
// daemonized, we write to stderr. We assume that all error message already
// have their final punctuation.
//

void error (const char *format, ...)
{
    char  buffer[512]; // Should be large enough.
    va_list ap;
    va_start(ap, format);
    (void) vsprintf(buffer, format, ap);
    va_end(ap);
    if (do_not_daemonize)
        cerr << buffer << "\n";
    else
        syslog(LOG_ERR, buffer);
    
    exit(EXIT_FAILURE);
}


//
// Returns a static string containing a system call error message.
// This is a helper function for syserror() when we're debugging.
//

static const char* sys_error_msg (void)
{
    const int LEN = 256;
    static char msg[LEN];

    if (errno != 0)
    {
        if (errno > 0 && errno < sys_nerr)
            //
            // TODO - replace with strerror() when available.
            //
            (void) strcpy(msg, sys_errlist[errno]);
        else
            (void) sprintf(msg, "unknown error, errno = %d", errno);
    }

    return msg;
}


//
// Our system call error-handling function.  This should only be called when
// a system call fails.  This assumes that /etc/syslog.conf is set up to send
// all LOG_ERR messages both to /dev/console and to /usr/adm/messages or other
// system-defined logfile.  If we're debugging and we haven't daemonized, we
// write to stderr.  We asssume that syscall error messages don't have any
// final punctuation.  We add a comma between the passed message and the
// syscall error string itself.
//

static void syserror (const char *format, ...)
{
    char  buffer[512]; // Should be large enough.
    va_list ap;
    va_start(ap, format);
    (void)vsprintf(buffer, format, ap);
    va_end(ap);
    if (do_not_daemonize)
        cerr << buffer << ", " << sys_error_msg() << "\n";
    else
    {
        (void)strcat(buffer, ", %m.");  // Append syscall error string.
        syslog(LOG_ERR, buffer);
    }

    exit(EXIT_FAILURE);
}


//
// Our message function -- prints message to /dev/console and logs it to a
// file.  This assumes that /etc/syslog.conf is set up to send all LOG_ERR
// messages both to /dev/console and to /usr/adm/messages or other
// system-defined logfile.  If we're debugging and we haven't daemonized,
// we write to stderr.
//

static void message (const char *format, ...)
{
    static char buffer[512]; // Should be large enough.
    va_list ap;
    va_start(ap, format);
    (void) vsprintf(buffer, format, ap);
    va_end(ap);
    if (do_not_daemonize)
        cerr << buffer << "\n";
    else
        syslog(LOG_ERR, buffer);
}


//
// Out-Of-Memory Exception handler for main.
//

static void free_store_exception (void)
{
    error("Out of memory, exiting ...");
}


//
// Returns the number of open files per process supported by the OS.
// Returns _POSIX_OPEN_MAX if the value is indeterminate and -1 on error.
//

static long maximum_open_files (void)
{
#ifdef OPEN_MAX
    return OPEN_MAX;
#else
    errno = 0;
    long value = sysconf(_SC_OPEN_MAX);
    if (value < 0)
        value =  errno == 0 ? _POSIX_OPEN_MAX : -1;
    return value;
#endif
}


//
// Become a daemon.  Exits on error.  Error handling will not have been
// initialized when this function is called, so we finagle `do_not_daemonize'
// to force all output to go to the terminal.  Normally one would also do a
// chdir() here, but I've decided to separate that out.
//

static void daemonize (void)
{
    do_not_daemonize = 1;

    switch (fork())
    {
      case -1:
        //
        // No more processes available.
        //
        syserror("File %s, line %d, fork() failed", __FILE__, __LINE__);
      case 0:
      {
          //
          // Close all open files.
          //
          long max = maximum_open_files();
          for (int i = 0; i < max; i++)
              (void) close(i);
          //
          // Become session leader.
          //
          if (setsid() < 0)
              syserror("File %s, line %d, setsid() failed",
                       __FILE__, __LINE__);
          //
          // Clear the umask.  This way each time I create a file through
          // open(2), the creating mask I specify will, be the one that
          // takes effect.  Note that umask(2) returns the previous value
          // of the umask; it apparently doesn't fail.
          //
          (void) umask(0);

          do_not_daemonize = 0;

          break;
      }
      default:
        //
        // Parent exits.
        //
        exit(EXIT_SUCCESS);
    }
}

//
// Return the packet length field from the incoming ccsds packet
//

inline int packetLength (const unsigned char*  buf)
{
    return (buf[4] << 8) + buf[5] ;
}

//
// Return the application ID of the packet
//

inline int applicationId(const unsigned char* buf)
{
    return (buf[0]  & 0x07) << 8 | buf[1] ;
}

//
// Return the sequence number of the packet
//

inline int sequenceNumber(const unsigned char* buf)
{
    return (buf[2]  & 0x03f) <<  8 | buf[3] ;
}


//
// Read n bytes into the buffer.  Works with sockets and files.  We need this
// since a read on a socket or pipe can return less than we asked for.
// Returns the number of bytes read.
//

static int read_bytes (int fd, unsigned char* buf, int nbytes)
{
    int nread;
    int nleft = nbytes;

    while (nleft > 0)
    {
        //
        // Under ObjectCenter 2.0 on my Sun, they expect
        // read() to have the prototype:
        //
        //   int read (int, char*, int);
        //
        // This is wrong, but what can I do?
        //
#ifdef sun
         if ((nread = read(fd, (char*)buf, nleft)) <= 0) break;
#else
         if ((nread = read(fd, buf, nleft)) <= 0) break;
#endif
        nleft -= nread;
        buf   += nread;
    }

    return nbytes - nleft;
}


//
// Write n bytes from buffer to the  file descriptor.  Works with sockets,
// pipes, and files.  We need this since a read on a socket or pipe can
// return less than we asked for.  Returns the number of bytes written.
//

static int write_bytes (int fd, const unsigned char* buf, int nbytes)
{
    int nwritten;
    int nleft = nbytes;

    while (nleft > 0)
    {
        //
        // Under ObjectCenter 2.0 on my Sun, they expect
        // write() to have the prototype:
        //
        //   int write (int, const char*, int);
        //
        // This is wrong, but what can I do?
        //
#if defined(sun)
        if ((nwritten = write(fd, (const char*) buf, nleft)) <= 0) break;
#else
        if ((nwritten = write(fd, buf, nleft)) <= 0) break;
#endif
        nleft -= nwritten;
        buf   += nwritten;
    }

    return nbytes - nleft;
}


//
// Read a telemetry packet from the file descriptor.  We return a constant
// pointer to the packet.  We maintain the storage for the packet it static
// storage; that is to say, the raw packet itself is in static storage we
// maintain here.  We do this for speed reasons.  What this means is that
// this client must be single threaded; it must operate solely on one packet
// at a time.  If a user needs to cache packets they must use the copy
// constructor to make a copy of this packet and maintain it themselves.
//

static const unsigned char* read_a_telemetry_packet (int fd)
{

    const int HEADERSIZE = 6;

    //
    // All XTE CCSDS AOS packets are <= 1024 bytes in size.
    //
    static unsigned char buf[1024];

    //
    // Read packet header to determine packet size.
    //
    if (read_bytes(fd, buf, HEADERSIZE) != HEADERSIZE)
        return 0;

    //

    //
    // PrimaryHdrSize + packetLength() + 1 == length of entire packet.
    //

    int len = packetLength (buf) + 1;
    
    //
    // Read the rest of the packet data into the buffer.
    //
    if (read_bytes(fd, &buf[HEADERSIZE], len) != len)
        return 0;

    return buf;
}


//
// Establish a socket connection to the host on the port.  Returns the file
// descriptor describing the connection, or -1 if connect() failed.  Exits on
// any other error.  We're interested in a connect() failure so that we can
// implement our own recovery mechanism.
//

static int connect_to_host_on_port (const char* hostname, short port)
{
    int fd;
    struct hostent *hp;
    struct sockaddr_in sin;
    
    if ((hp = gethostbyname(hostname)) == 0) 
        syserror("File %s, line %d, gethostbyname(%s) failed",
                 __FILE__, __LINE__, hostname);
    
    if ((fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        syserror("File %s, line %d, socket() failed", __FILE__, __LINE__);
        
    memset((char *)&sin, 0, sizeof(sin));
    sin.sin_family = AF_INET;
    sin.sin_port = htons(port);
    memcpy(&sin.sin_addr, hp->h_addr, hp->h_length);
    
    return connect(fd, (sockaddr*)&sin, sizeof(sin)) < 0 ? -1 : fd;
}


//
// Tell the Realtime Server who we are.  The name must be in the
// table of realtime clients which is read by the Realtime Server
// on startup.
//

static void identify_ourselves (const char* name, int fd)
{
    //
    // The name must be in a special realtime server command packet.
    // The first byte of the command packet is a command identifier;
    // currently it must be set to 0x01; the second byte is the length
    // byte; following the length byte is a our name as an array of
    // characters, including the NULL byte.  The length byte must be set
    // to strlen(name) + 1.
    //
    char cmd[256];
    cmd[0] = 0x01;
    cmd[1] = strlen(name) + 1;
    memcpy(&cmd[2], name, cmd[1]);

    if (write_bytes(fd, (const unsigned char*)cmd, cmd[1]+2) != cmd[1]+2)
        error("File %s, line %d, write_bytes() failed",
              __FILE__, __LINE__);
}


//
// Process a packet read from the Realtime Server.
//

static void process_a_packet (const unsigned char* buf)
{
    //
    // You've got a valid PktTlm.  You should probably check the application
    // ID to make sure it is one of the ones that you want.  Then do with it
    // as you will.
    //
    // It would probably be best if you put all the code you need to process
    // packets into a separate module.  That way, the communication stuff here
    // will be separate from the packet specific stuff.
    //
    // Do not delete the passed `pkt'.  If you want to keep a copy of this
    // packet, use the copy constructor to make your own copy.
    //

       double d;

       PCAdata.decodeHK(buf);
       pindex = 0;
       sigsetjmp(sigjmpbuf, 1); canjump = 1;
       
       while (pindex < maxparams && param[pindex]) {
	  d = PCAdata.getkernel(param[pindex]);
          d = (PCAdata.IsError() ? (double)0 : d);
	  if (sfp[pindex])
	    fwrite((char *)&d, sizeof(double), 1, sfp[pindex]);
	  pindex++;
       }
      
       

}

//
// We do a popen to mplot program and initialize its
// file pointer
//

static void initialize_strip_chart(void)
{
    char *mcmd = "/socsoft/SOC_IntTestMaster/bin/HSmplot";
        
    for (int i = 0; i < maxparams && param[i]; i++) {
	char pcmd[255];
	sprintf(pcmd, "%s %s", mcmd, param[i]);
	if (!(sfp[i] = popen(pcmd, "w")))
	   error("popen failed");
    	setbuf(sfp[i], (char *)0);
    }

}


//
// Read a stream of packets from Realtime Server.
//

static void read_packets_from_realtime_server (const char* name)
{
    static int fd;

    static jmp_buf jmpbuf;



    while ((fd = connect_to_host_on_port(hostname, port)) == -1)
    {
        message("Connection to Realtime Server %s on port %d failed."
                " Will try again in one minute.", hostname, port);
        sleep(60);
    }

    initialize_strip_chart();

    if (setjmp(jmpbuf))
    {
        //
        // We return to here on a longjmp() -- when our connection with the
        // Realtime Server is severed.
        //
        do
        {
            message("Connection to Realtime Server %s on port %d failed;"
                    " Will try again in one minute.", hostname, port);
            sleep(60);
        } while ((fd = connect_to_host_on_port(hostname, port)) == -1);
    }

    identify_ourselves(name, fd);

    for (const unsigned char* buf;;)
    {
        if ((buf = read_a_telemetry_packet(fd)) == 0)
        {
            //
            // The connection with the realtime server has been  severed.  Try
            // to reestablish the connection.  It could also be that the
            // telemetry stream is bad.  We should get a realistic message
            // to this effect, in this case.
            //
            (void) close(fd);
            longjmp(jmpbuf, 1);
        }
        else
                process_a_packet(buf);
                
    }
}


//
// Parse our arguments.
//

static void parse_arguments (char**& argv)
{
    //
    // We want to guarantee that all messages go to stdout.
    // To do this we must set do_not_daemonize to true on entry
    // and only set it to what it should be on return.  That is, if
    // `-do-not-daemonize' is set in this function, we don't
    // actually turn it on until we return.
    //
    do_not_daemonize = 1;

    int daemonize_flag = 0;

    //
    // Parse any arguments.
    //
    //   -do-not-daemonize  -- do not run as a daemon.
    //    -host machine     -- realtime server machine.
    //    -port #           -- port number realtime server listens to.
    //    -name name        -- the name we advertise to the realtime server.
    //    -select	    -- the PCA H&S parameters we want to monitor
    //			    -- a maximum of maxparams allowed
    //
    while (*++argv && **argv == '-') 
    {
        if (strcmp(*argv, "-host") ==  0)
        {
            if (!(hostname = *++argv))
                error("No hostname supplied");
        }
        else if (strcmp(*argv, "-port") == 0)
        {
            if (*++argv)
            {
                if ((port = (short) atoi(*argv)) <= 0)
                    error("Port # must be positive short");
                continue;
            }
            else
                error("No port # supplied");
        }
        else if (strcmp(*argv, "-do-not-daemonize") == 0)
            daemonize_flag = 1;
        else if (strcmp(*argv, "-name") ==  0)
        {
            if (!(our_name = *++argv))
                error("No name supplied");
        }
        else if (strcmp(*argv, "-select") ==  0)
        {
	   int k = 0;
           while (*(argv+1) && **(argv+1) != '-')
	    if (k < maxparams)
	      if (PCAdata.IsValidMnem(*++argv))
              	param[k++] = *argv;
	      else
		message("Ignoring invalid parameter `%s'", *argv);
            else
	      message("Only %d parameters can be monitored ... Ignoring `%s'",
		maxparams, *++argv);
	    if (!k)
                error("No parameter(s) selected");
        }
        else
        {
            message("`%s' is not a valid option, exiting ...", *argv);
            usage();
        }
    }

    //
    // Hostname, port, and name must be supplied.
    //
    if (!hostname || !port || !our_name)
        usage();

    do_not_daemonize = daemonize_flag;
}

//
// Simple error handler for use by PktCCSDS.
//

static void packet_error_handler (const char* msg) { error(msg); }


//
// The main routine.
//

int main (int, char* argv[])
{
    ProgName = argv[0];

    parse_arguments(argv);

    //
    // Make the filesystem root our working directory.
    //
      if (chdir("/") < 0)
        syserror("File %s, line %d, chdir(%s) failed",
                 __FILE__, __LINE__, "/");

    if (!do_not_daemonize)
        daemonize();

    initialize_error_handling(our_name);

    set_new_handler(free_store_exception);

    if (signal(SIGPIPE, sigpipe_handler) == SIG_ERR)
	syserror("File %s, line %d, signal(SIGPIPE, sigpipe_handler) failed",
                 __FILE__, __LINE__);

    read_packets_from_realtime_server(our_name);

    return 0;
}
@


2.1
log
@Creation
@
text
@a3 1
// $Id$
d26 2
@

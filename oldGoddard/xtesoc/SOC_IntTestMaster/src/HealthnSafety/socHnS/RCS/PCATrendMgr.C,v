head	1.6;
access
	rbarnett
	rbentley
	soccm
	jschwan
	savkoor
	gromeo;
symbols
	Build4_3_1:1.6
	Build4:1.4
	Build3_3:1.1;
locks; strict;
comment	@ * @;


1.6
date	95.05.25.18.03.48;	author kmurphy;	state Exp;
branches;
next	1.5;

1.5
date	95.03.19.19.40.33;	author leone;	state Exp;
branches;
next	1.4;

1.4
date	95.01.17.13.38.32;	author leone;	state Exp;
branches;
next	1.3;

1.3
date	94.12.28.18.11.01;	author leone;	state Exp;
branches;
next	1.2;

1.2
date	94.12.28.14.29.51;	author leone;	state Exp;
branches;
next	1.1;

1.1
date	94.11.13.17.31.36;	author leone;	state Exp;
branches;
next	;


desc
@@


1.6
log
@Solaris changes as per DR #434
@
text
@// ---------------------------------------------------------------------------
// File Name   : PCATrendMgr.C
// Subsystem   : SOC Health & Safety
// Programmer  : Rick Leone, Hughes STX/NASA/GSFC
// Description : PCATrendMgr is the interface to a collection of Trend objects 
//
// ----------------------------------------------------------------------------
static const char rcsid[] = "$Id: PCATrendMgr.C,v 1.5 1995/03/19 19:40:33 leone Exp $";
//
// .NAME    PCATrendMgr - Produces PCATrendMgr data from  housekeeping data
// .LIBRARY 
// .HEADER  SOC Health & Safety
// .INCLUDE PCATrendMgr.h
// .FILE    PCATrendMgr.C
// .AUTHOR  XTE-SOC 
// ----------------------------------------------------------------------------


#include "PCATrendMgr.h"

//
// ----- required for rogue wave -----
//

RWDEFINE_COLLECTABLE( PCATrendMgr, PCA_TREND_MGR_ID )

//
// --- PCATrendMgr Class Member Function Definitions --
//

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Name: PCATrendMgr
// Description:
// Dumby constructor to satisfy Rogue Wave
//
//
//
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     PCATrendMgr::PCATrendMgr( void )
{

}
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Name: PCATrendMgr
// Description:
// 
//
//
//
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
PCATrendMgr::PCATrendMgr( const char* _path, long& _persist_rate )
{
  path = _path;
  persistRate = _persist_rate;
  // ----------- debug comments ----------
  //cerr<<"PCATrendMgr::PCATrendMgr -> leaving ..............."<<endl;
}

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Name: ~PCATrendMgr
// Description:
// 
//
//
//
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
PCATrendMgr::~PCATrendMgr( void )
{ 
}
  
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Name: compareTo
// Description:
// 
//
//
//
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
int PCATrendMgr::compareTo( const RWCollectable* rhs ) const
{
 
  const PCATrendMgr* tmp = ( PCATrendMgr* )rhs;
  return ( lastSaved == tmp->getLastSaved() ) ?  0 : ( ( lastSaved > tmp->getLastSaved() ) ? 1 : -1 );
}

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Name: 
// Description:
// Spawns a process and opens a pipe to let parent know if the Trend.file 
// has been updated while the rtclient is running.  If the file has been
// updated, rereadfile() is issued.
//
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
int PCATrendMgr::setIndicator( void ) const
{
  int ret_val = 0;
  
  // --- create a unidirectional pipe
  // --- spawn a process
  // --- check status of path/trend.file
  // --- if touched send flag to PCATrendMgr in parent process

  return ret_val;
}

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Name: setDisplay 
// Description:
//
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void PCATrendMgr::setDisplay( const Descriptor&, SciDisplay& )const 
{
}
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Name: getLastSave 
// Description:
//
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
long PCATrendMgr::getLastSaved( void )const 
{
  return lastSaved;
}

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Name: setUp 
// Description:
//
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
int PCATrendMgr::setUp( instrument_token /* _instrument */ )
{
  int ret_val = 0;
  deltaTime = 8;

  //
  // --- get persisted trendList, if exists ---
  //

  RWCString restore_path = getenv("SOCOPS");
  restore_path          += "/etc/PCATrendMgr.soc";
  
  // ------------ debug comment ----------------
  //cerr<<"setUP restore_path: "<<restore_path<<endl;

  ifstream f( restore_path.data() );
  if( f ){

    // ------------- debug comment ---------------
    cerr<<"there is a persisted PCATrendMgr:"<<endl;
    cerr<<restore_path<<endl;

    RWpistream stream( f );
    stream >> *this;
    showIDs();
  }
  else
    SOCThrow( SOC_RESPONSE, "Error: PCATrendMgr::setUp could not"
	     " open file: \n\t", restore_path.data() );
  f.close();

  //
  // -- create a string with the full path of the trend secification file ---
  //

  RWCString full_path  = getenv("SOCHOME");
  full_path           += "/etc/PCA.trend.spec";

  // --------------------- debug comment ---------------------
  //cerr<<"PCATrendMgr::setUp -> reading from "<<full_path<<endl;

  //
  // --- Open the trend specification file ---
  //

  fstream  trend_spec( full_path.data(), ios::in );
  if( !trend_spec ){
    SOCThrow( SOC_RESPONSE, "Error: PCATrendMgr::setUp could not"
	     " open trend spec file: \n\t", full_path.data() );
    exit(0);
  }

  //
  // --- read the trend specification file line by line ---
  //

  RWSortedVector listOfStrings;        // place the names of trends here
  char in_buf[PCA_T_MGR_MAX_LINE];         // temporary store incoming line
  RWCString test_line( "" );           // incoming line read from file
  RWCString modu( "" );                // modulus
  RWCString order( "" );               // polynomial fit order
  RWCollectableString *final_exp;      // the final expression
  RWCRegexp comment( "#" );            // this line is a comment, skip it
  RWCRegexp detect( "[0-9]_" );        // regular expression for integer
  RWCRegexp one_digit( "[0-9]" );      // regular expression for integer
  RWCRegexp ints( "[0-9][0-9]+" );     // regular expression for integer
  RWCRegexp parm( "[^ ]+" );           // regular exp. for housekeeping 
//  RWCRegexp parm( "[A-Z][0-9]?[A-Z]+[0-9]?" );// regular exp. for housekeeping 
  size_t index_i      = 0;
  size_t index_f      = 0;
  size_t* index_ptr   = new size_t();
  int l_num           = 0;
  RWBoolean set_order = FALSE;

  // --- open while loop to parse each line in the trend spec file ---
  while( trend_spec.getline( in_buf, PCA_T_MGR_MAX_LINE ) ){
    // --------- debug comment ----------
    //cerr<<"echo: in_buf == "<<in_buf<<endl;
    l_num++;
    test_line = in_buf;
    final_exp = new RWCollectableString();
    *final_exp += "PCA.";
    
   
    //
    // --- parse each line of the trend file ---
    //
    
    index_i    = 0;
    *index_ptr  = 0;
    index_i    = test_line.index( comment, index_ptr, index_i );
    if( !( *index_ptr ) ){

      // --- get detector number ---
      index_i    = 0;
      index_i    = test_line.index( detect, index_ptr, index_i );
      if( index_i != RW_NPOS ){
	*final_exp += test_line( index_i, 1 ); 
	*final_exp += ".";
	// ------------ debug comment ----------
	//cerr<<"index_ptr = "<<*index_ptr<<endl;
	//cerr<<"index_i = "<<index_i<<endl;
      }
     
      // --- get parmeter name ---
      index_i    = test_line.index( parm, index_ptr, index_i + *index_ptr );
      if( index_i != RW_NPOS ){
	*final_exp += test_line( index_i, *index_ptr ); 
	*final_exp += ".";
	// ------------ debug comment ----------
	//cerr<<"index_ptr = "<<*index_ptr<<endl;
	//cerr<<"index_i = "<<index_i<<endl;
      }
       
      // --- get the duration ---
      index_i    = test_line.index( ints, index_ptr, index_i + *index_ptr );
      if( index_i != RW_NPOS ){
	modu = test_line( index_i, *index_ptr );
	*final_exp += modu; 
	// ------------ debug comment ----------
	//cerr<<"index_ptr = "<<*index_ptr<<endl;
	//cerr<<"index_i = "<<index_i<<endl;
      }

      // --- get the expansion order was specified--- 
      if( test_line.contains( "polynomial", RWCString::ignoreCase ) ){
	index_i    = test_line.index( one_digit, index_ptr, index_i + *index_ptr );
	if( index_i != RW_NPOS ){
	  order  = ".";
	  order += test_line( index_i, 1 ); 
	  set_order = TRUE;
	  // ------------ debug comment ----------
	  //cerr<<"index_ptr = "<<*index_ptr<<endl;
	  //cerr<<"index_i = "<<index_i<<endl;
	}
      }
    
      // --- get the optional modulus --- 
      index_i    = test_line.index( ints, index_ptr, index_i + *index_ptr );
      if( index_i != RW_NPOS ){
	*final_exp += ".";
	*final_exp += test_line( index_i, *index_ptr ); 
	// ------------ debug comment ----------
	//cerr<<"index_ptr = "<<*index_ptr<<endl;
	//cerr<<"index_i = "<<index_i<<endl;
      }
      // if not present, affix the duration as default modulus
      else{
	*final_exp += ".";
	*final_exp += modu;
      }

      // --- get the optional modulus if orbit was specified ---
	
      if( set_order ){
	*final_exp += order;
	set_order = FALSE;
      }
      // ------------ debug comment ----------
      //cerr<<"final expression"<<*final_exp<<endl;

      listOfStrings.insert( final_exp );

    } // close if( !*index_ptr )   
  
} // close the parse file while loop 

  //
  // --- check persisted trends for matches on the list
  //
  
  Trend *test_trend;
  Trend *new_trend;
  RWBoolean match = FALSE;
  RWCollectableString *test_string;
  RWSortedVectorIterator next_string( listOfStrings );
  RWSortedVectorIterator next_trend( trendList );
  RWCRegexp poly_order( "\.[0-9][0-9]+\.[0-9][0-9]+\.[0-9]" ); 
  // signature of polytrend
  
  while( test_string = ( RWCollectableString* )next_string() ){
    
    // --------- debug comment ---------
    //cerr<<"test_string: "<<*test_string<<endl;
    next_trend.reset();
    while( test_trend = ( Trend* )next_trend() ){
      
      if( test_trend->isMyID( ( RWCString )( *test_string ) ) ){
	match = TRUE;
	break;
      }
    } // --- close trend loop ---
    
    if( !match ){
      
      // ------- debug ------
      //cerr<<" no match ....... so make the trend."<<endl;
      if( ( index_i = test_string->index( poly_order ) ) != RW_NPOS ){
	  new_trend = new PolyTrendPCA( ( RWCString )( *test_string ) );
	  trendList.insert( new_trend );
	  // ----- debug comment ----
	  //cerr<<"new poly trend was inserted on trend list"<<endl;
	}
      else{
	  new_trend = new SimpleTrendPCA( ( RWCString )( *test_string ) );
	  trendList.insert( new_trend );
	  // ----- debug comment ----
	  //cerr<<"new simple trend was inserted on trend list"<<endl;
	}
      match = FALSE;
      }       
  } // --- close string loop --- 
    
  //
    // --- check for defunct trends and remove them ---
  //
  
  match = FALSE;
  next_trend.reset();
  while( test_trend = ( Trend* )next_trend() ){

    next_string.reset();
    while( test_string = ( RWCollectableString* )next_string() ){
      
      if( test_trend->isMyID( ( RWCString )( *test_string ) ) ){
	match = TRUE;
	break;
      }
    } //--- close defunct trend, string loop ---
    
    if( !match ){
      test_trend = ( Trend* )trendList.remove( test_trend );
      delete test_trend;
      match = FALSE;
    }
  } // --- close defunct trend loop ---
  // --- create trend objects that are in the file but not on list
  
  next_string.reset();
  while( test_string = ( RWCollectableString* )next_string() )
    delete test_string;

  // ------------ debug comment ----------
  //cerr<<"PCATrendMgr::setUp done.........."<<endl;
  delete index_ptr;  
  return ret_val;

}
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Name: resetPath 
// Description:
//
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void PCATrendMgr::resetPath( const char* _path )
{
  path = _path;
}
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Name: setStarted 
// Description:
//
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void PCATrendMgr::setStarted( const long& _seconds )
{
  started = _seconds;
}
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Name: rereadFile 
// Description:
//
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
int PCATrendMgr::rereadFile( void ) const
{
  int ret_val = 0;

  return ret_val;
}
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Name: persist 
// Description:
//
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void PCATrendMgr::persist( const long& _seconds )
{
  static long last_persisted = 0;
  last_persisted += deltaTime;

  // ---------------------------- debug comment --------------------------
  //cerr<<"PCATrendMgr::persist -> last_persisted = "<< last_persisted<<endl;

  if( last_persisted >= persistRate ){
    last_persisted = 0;
    lastSaved      = _seconds;

    // -------------------- debug comment --------------------------
    //cerr<<"last pesisted at time "<< _seconds <<endl;
    //cerr<<"PCATrendMgr::persist -> about to open file ......."<<endl;

    RWCString full_path = getenv("SOCOPS");
    full_path          += "/etc/PCATrendMgr.soc";
    ofstream f( full_path.data() );
    if( f ){

      // ---------------- debug comment ----------------
      //cerr<<"file: "<<full_path<<" was opened... "<<endl;

      RWpostream stream( f );
      stream << *this;

      // --------------------------- debug comment -----------------------
      //cerr<<"PCATrendMgr::persist -> writing trends to file ......."<<endl;
    }
    showIDs();
  }
}

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Name: length 
// Description:
//
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
int PCATrendMgr::length( void ) const
{
  return trendList.entries();
}

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Name: showIDs
// Description:
//
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void PCATrendMgr::showIDs( void ) const
{
  // ----------------------- debug comment -----------------
  //cerr<<"PCATrendMgr::showIDs -> entering ..........."<<endl;

  Trend *t;
  int num = ( int ) trendList.entries();
  for( size_t i = 0; i < num; i++ ){
    t = ( Trend* )trendList( i );
    
    // --- debug comment ---
    //cout<< t->getID() <<endl;
  }
  
  // ----------------------- debug comment ----------------
  //cerr<<"PCATrendMgr::showIDs -> leaving ..........."<<endl;
}

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Name: process 
// Description:
//
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void PCATrendMgr::process( const RWCollectable* _part, const char* _path )
{
  long new_time = ( ( PcaHKPart* ) _part )->time();

  if( new_time > lastSaved ){
    Trend *t;
    persist( new_time );
    int num = ( int ) trendList.entries();

    // ----------------------------- debug comment -----------------------
    //cerr<<"PCATrendMgr::process -> processing "<<num <<" entries. "<<endl;

    for( size_t i = 0; i < num; i++ ){
      t = ( Trend* )trendList( i );

      // ----------------------------- debug comment -----------------------
      //cerr<<"PCATrendMgr::process -> about to process   "<<t->getID()<<endl;
      t->process( _part );

      // ----------------------------- debug comment -----------------------
      //cerr<<"PCATrendMgr::process -> checkingTime       "<<t->getID()<<endl;
      if( t->checkTime( new_time, deltaTime ) ){

	// ----------------------------- debug comment ----------------------
	//cerr<<"PCATrendMgr::process -> about to writeData "<<t->getID()<<endl;
	t->writeData( _path );
	t->reset( 0.0 );
      }
    }
  }

  // ------------------ debug comment -------------------
  //cerr<<"PCATrendMgr::process -> leaving process "<<endl;
  
}

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Name: binaryStoreSize
// Description:
// 
//
//
//
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
RWspace PCATrendMgr::binaryStoreSize( void ) const
{
  RWspace size = 0;
  
  size += sizeof( trendList );

  Trend * tr;
  RWOrderedIterator itr( trendList );
  while( tr = ( Trend* )itr() )
    size += tr->binaryStoreSize();
  return size;
}

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Name: isEqual
// Description:
// 
//
//
//
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  RWBoolean PCATrendMgr::isEqual( const RWCollectable* obj) const
//{
//  const PCATrendMgr* t = ( const PCATrendMgr* )obj;
//  if( path == t->path )
//    return TRUE;
//  else
//    return FALSE;
//}

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Name: hash
// Description:
// 
//
//
//
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  unsigned  PCATrendMgr::hash() const{}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Name: restoreGuts
// Description:
// 
//
//
//
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  void PCATrendMgr::restoreGuts( RWFile& f )
{
  TrendMgr::restoreGuts( f );
//  RWCollectable::restoreGuts( f );
//  f.Read( lastSaved );
//  f.Read( persistRate );
//  f.Read( started );
//  f.Read( deltaTime );

//  f >> path;
//  f >> trendList;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Name: saveGuts
// Description:
// 
//
//
//
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  void PCATrendMgr::saveGuts( RWFile& f ) const
{

  TrendMgr::saveGuts( f );
//  RWCollectable::saveGuts( f );
//  f.Write( lastSaved );
//  f.Write( persistRate );
//  f.Write( started );
//  f.Write( deltaTime );

//  f << path;
//  f << trendList;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Name: restoreGuts
// Description:
// 
//
//
//
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  void PCATrendMgr::restoreGuts( RWvistream& v )
{
  //cerr<<"PCATrendMgr::restoreGuts -> starting to build ..........."<<endl;
  TrendMgr::restoreGuts( v );
//  RWCollectable::restoreGuts( v );

//  v >> lastSaved;
//  v >> persistRate;
//  v >> started;
//  v >> deltaTime;
//  v >> path;
//  v >> trendList;
  //cerr<<"PCATrendMgr::restoreGuts -> leaving ..........."<<endl;

}

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Name: saveGuts
// Description:
// 
//
//
//
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  void PCATrendMgr::saveGuts( RWvostream& v ) const
{
  //cerr<<"PCATrendMgr::saveGuts -> starting to write ..........."<<endl;

  TrendMgr::saveGuts( v );
//  RWCollectable::saveGuts( v );
  
//  v << lastSaved;
//  v << persistRate;
//  v << started;
//  v << deltaTime;
//  v << path;
//  v << trendList;
  //cerr<<"PCATrendMgr::restoreGuts -> leaving ..........."<<endl;

}







@


1.5
log
@setUp reads PCU?.trends.soc from $SOCOPS/etc instead of $SOCHOME/etc.
@
text
@d8 1
a8 1
static char rcsid[] = "$Id: PCATrendMgr.C,v 1.4 1995/01/17 13:38:32 leone Exp leone $";
d137 1
a137 1
int PCATrendMgr::setUp( instrument_token _instrument )
@


1.4
log
@*** empty log message ***
@
text
@d8 1
a8 1
static char rcsid[] = "$Id: PCATrendMgr.C,v 1.3 1994/12/28 18:11:01 leone Exp leone $";
d146 1
a146 1
  RWCString restore_path = getenv("SOCHOME");
d163 3
d169 1
a169 1
  // --- create a string with the full path of the trend secification file ---
d184 2
a185 1
    SOCThrow( SOC_RESPONSE, "Error: PCATrendMgr::setUp could not open trend spec file: \n\t", full_path );
d439 1
a439 1
    RWCString full_path = getenv("SOCHOME");
d681 1
@


1.3
log
@*** empty log message ***
@
text
@d8 1
a8 1
static rcsid[] = "$Id: PCATrendMgr.C,v 1.2 1994/12/28 14:29:51 leone Exp leone $";
d156 3
a158 1
    //cerr<<"there is a persisted PCATrendMgr"<<endl;
@


1.2
log
@redeliver for build4.
@
text
@d8 1
a8 1
static rcsid[] = $Id: PCATrendMgr.C,v 1.1 1994/11/13 17:31:36 leone Exp leone $
@


1.1
log
@Initial revision
@
text
@d4 2
a5 2
// Programmer  : Rick Leone, Hughes STX/GSFC
// Description : PCATrendMgr is the interface to a collection of Trend class 
d8 1
a8 1
// RCS: $Id:$
d146 5
a150 1
  RWCString full_path( path );
d152 1
a152 2
  full_path += "/PCATrendMgr.soc";
  ifstream f( full_path.data() );
d154 3
a160 1

d167 2
a168 2
  full_path  = path;
  full_path += "/PCA.trend.spec";
d207 2
a208 1

d313 2
d325 3
a327 1

d331 2
d337 2
d433 2
a434 2
    RWCString full_path = path;
    full_path          += "/PCATrendMgr.soc";
@

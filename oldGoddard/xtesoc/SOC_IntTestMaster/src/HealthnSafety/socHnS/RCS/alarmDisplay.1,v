head	1.2;
access
	rbarnett
	rbentley
	soccm
	jschwan
	savkoor
	gromeo;
symbols
	Build5_0_3:1.1;
locks; strict;
comment	@# @;


1.2
date	96.08.05.19.43.57;	author jschwan;	state Exp;
branches;
next	1.1;

1.1
date	95.03.01.12.17.08;	author leone;	state Exp;
branches;
next	;


desc
@man page for alarmDisplay.
@


1.2
log
@Revised for Build 5.1 changes.
@
text
@.\" ==========================================================================
.\" File Name   : alarmDisplay.1
.\" Subsystem   : Health & Safety
.\" Programmer  : Julie Schwan - Hughes STX
.\" Description :
.\"
.\"  Man page for the alarm display process.
.\"  View with : nroff -man alarmDisplay.1 | more
.\"  Print with: troff -man alarmDisplay.1 | dpost | /bin/lp
.\" 
.\" RCS: $Id: alarmDisplay.1,v 1.1 1995/03/01 12:17:08 leone Exp jschwan $
.\" 
.\" ==========================================================================
.TH alarmDisplay 1 "July 25, 1996" "Health & Safety"
.SH NAME
alarmDisplay - displays sochkHEX and sochkPCA alarm report files.
.\" ==========================================================================
.SH SYNOPSIS
.\" ==========================================================================
.B  alarmDisplay
-i PCA | HEXTE [-read-from-stdin] [-p 
.IR parameter "] [-a" 
.IR alarm "] [-d"
.IR detector ]
[-date
.I  dd/mm/yyyy
[-ts
.IR  hh:mm:ss ]
[-s
.I  seconds
| -tf
.IR hh:mm:ss ]]
[-day 
.I METday
[-ts
.IR hh:mm:ss ]
[-s
.I seconds
| -tf
.IR hh:mm:ss ]]
.BR [ -ints
.I METseconds
[-intf
.I METseconds
| -s
.IR seconds ]]
.\" ==========================================================================
.SH DESCRIPTION
.\" ==========================================================================
.B alarmDisplay
displays alarm report files. The displayed data may be filtered by time, 
housekeeping parameter, alarm condition, and detector number.
Files are read from $SOCOPS/... or read from standard input using the
-read-from-stdin switch.  The -i switch must be specified with either PCA
or HEXTE, all other filter switches are optional.

.\" ==========================================================================
.SH OPTIONS
.\" ==========================================================================
.TP
.\" ------------------------------------
.B -p
.\" ------------------------------------
.br
Specify a housekeeping parameter to be displayed, filters out all other
housekeeping parameters
.TP
.\" ------------------------------------
.B -a
.\" ------------------------------------
.br
Specify an alarm condition to be displayed, filters out all other
alarm conditions. The choices are red, yellow and normal.
.TP
.\" ------------------------------------
.B -d
.\" ------------------------------------
.br
Specify a detector to be displayed, filters out all other
detectors. The choices for HEXTE are 1 and 2 for each cluster.
The choices for PCA are 1, 2, 3, 4, and 5 for each pcu.
.TP
.\" ------------------------------------
.B -date
.\" ------------------------------------
.br
calendar date. Takes the options -ts, -s or -tf. If
no option is given the interval displayed will be the 24 hour period
starting at 00:00:00 of the date. If -ts is given the interval displayed
starts at hh:mm:ss and extends until 23:59:59 or the time specified by
-tf or for -s seconds
.TP
.\" ------------------------------------
.BI "-day
.\" ------------------------------------
.br        
Mission Elapse Time day. Takes the options -ts, -s or -tf. If
no option is given the interval displayed will be the 24 hour period
starting at 00:00:00 of the date. If -ts is given the interval displayed
starts at hh:mm:ss and extends until 23:59:59 or the time specified by
-tf or -s seconds
.TP
.\" ------------------------------------
.BI "-ints
.\" ------------------------------------
Interval, start. Specifies the starting time, in seconds, of an interval.
-ints Takes the options -intf or -s. The -s option specifies 
the duration of the interval in seconds. The -intf option specifies
the final time of the interval in seconds. 
.\" ==========================================================================
.SH ENVIRONMENT
.\" ==========================================================================
.TP
.B $SOCOPS
This is the root directory under which the path will be set for 
reading the trend files.
.TP
.B $SOCHOME
This is the root directory under which the etc files will be read
to initialize the process.
.br 
.sp
.br
.\" ==========================================================================
.SH SEE ALSO
.\" ==========================================================================
.IR "SOC Health & Safety Subsystem Design" ,
.br
.\" ==========================================================================
.SH AUTHOR
.\" ==========================================================================
Julie Schwan (jschwan@@xema.stx.com)
.\" ==========================================================================
.SH HISTORY
.\" ==========================================================================
Version 6.0, July 1996
@


1.1
log
@Initial revision
@
text
@d4 1
a4 1
.\" Programmer  : Rick Leone, Hughes STX
d7 1
a7 1
.\"   Man page for the trend process.
d9 1
a9 1
.\"  Print with: troff -man -t -rD1 alarmDisplay.1 | lpr -t [-Pprinter]
d11 1
a11 1
.\" RCS: $Id:$
d14 1
a14 1
.TH alarmDisplay 1 "February 1, 1995" "Health & Safety"
d16 1
a16 1
alarmDisplay \- displays alarm report files.
d52 4
a55 3
housekeeping parameter, and detector. Files are read from $SOCOPS/... or
read from stdin using the -read-from-stdin switch. The -i switch must be 
specified with either PCA or HEXTE, all other filter switches are optional.
d73 1
a73 1
alarm conditions. The choices are red, yellow, normal.
d80 2
a81 2
detectors. The choices hexte are 1 and 2 for cluster and pca
has the choices, 1, 2, 3, 4, and 5. for each pcu.
a120 4
.\" ==========================================================================
.SH FILES
.\" ==========================================================================
.rb
d122 2
a123 1
trendfiles are located in $SOCOPS/..........
a127 1
.I trendfiles(5)
d132 1
a132 1
Rick Leone (leone@@xylem.stx.com)
d136 1
a136 5
Version 1.0, February 1995




@

head	1.4;
access
	rbarnett
	rbentley
	soccm
	jschwan
	savkoor
	gromeo;
symbols
	Build4_3_1:1.4
	Build4:1.3
	Build3_3:1.1;
locks; strict;
comment	@ * @;


1.4
date	95.05.25.18.08.34;	author kmurphy;	state Exp;
branches;
next	1.3;

1.3
date	94.12.28.18.18.49;	author leone;	state Exp;
branches;
next	1.2;

1.2
date	94.12.28.15.20.08;	author leone;	state Exp;
branches;
next	1.1;

1.1
date	94.11.13.17.31.36;	author leone;	state Exp;
branches;
next	;


desc
@@


1.4
log
@Solaris changes as per DR #434
@
text
@// ---------------------------------------------------------------------------
// File Name   : PolyTrendPCA.C
// Subsystem   : SOC Health & Safety
// Programmer  : Rick Leone, Hughes STX/GSFC
// Description : PolyTrendPCA is a subclass of the PolyTrend class which 
//               collects data and performs a polynomialregression fit.
//
// ----------------------------------------------------------------------------
static const char rcsid[] = "$Id: PolyTrendPCA.C,v 1.2 1995/05/09 16:40:59 soccm Exp $";
//
// .NAME    PolyTrendPCA - Produces trend data for housekeeping data
// .LIBRARY 
// .HEADER  SOC Health & Safety
// .INCLUDE PolyTrendPCA.h
// .FILE    PolyTrendPCA.C
// .AUTHOR  XTE-SOC 
// ----------------------------------------------------------------------------

#include "PolyTrendPCA.h"

//
// -------- required for rogue wave ----------
//

RWDEFINE_COLLECTABLE( PolyTrendPCA, POLY_TREND_PCA_ID )

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Name: PolyTrendPCA
// Description:
// A do nothing constructor to satisfy Rogue Wave
// Uses:
// Comments:
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
PolyTrendPCA::PolyTrendPCA( void ){}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Name: PolyTrendPCA
// Description:
// A do nothing constructor to satisfy Rogue Wave
// Uses:
// Comments:
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
PolyTrendPCA::PolyTrendPCA( const RWCString& _full_name )
{
  id = _full_name;

  RWCRegexp reg_digits( "[0-9]+" );  // regular expression for numbers
  RWCRegexp reg_instru( "[A-Z]+" );  // regular expression for instrument
  RWCRegexp reg_house_k( "[^\.]+" );  // regular expression for housekeeping
  RWCRegexp dot( "\." );
  size_t index_i     = 0;
  size_t index_f     = 0;
  size_t* index_ptr  = new size_t();

  //
  // --- parse each line of the trend file ---
  //

  // --- get instrument name ---
  RWCString instrument( "" );
  index_i      = 0;
  index_i      = _full_name.index( reg_instru, index_ptr, index_i );

  // ----------------- debug comment -----------------
  //cerr<<"index_i = "<<index_i<<" *index_ptr = "<<*index_ptr<<endl;
  if( index_i != RW_NPOS )
    instrument = _full_name( index_i, *index_ptr );

  // ----------------- debug comment -----------------
  //cerr<<"instrument:"<<instrument<<endl;

  // --- get detector number ---
  RWCString detector( "" );
  index_i      = _full_name.index( dot, index_ptr, index_i );
  index_i      = _full_name.index( reg_digits, index_ptr, index_i );

  // ----------------- debug comment -----------------
  //cerr<<"index_i = "<<index_i<<" *index_ptr = "<<*index_ptr<<endl;
  if( index_i != RW_NPOS )
    detector   = _full_name( index_i, *index_ptr );

  // ----------------- debug comment -----------------
  //cerr<<"detector:"<<detector<<endl;

  // --- get houkeeping parameter name ---
  RWCString housekeeping( "" );
  index_i      = _full_name.index( dot, index_ptr, index_i + *index_ptr );
  index_i      = _full_name.index( reg_house_k, index_ptr, index_i );

  // ----------------- debug comment -----------------
  //cerr<<"index_i = "<<index_i<<" *index_ptr = "<<*index_ptr<<endl;
  if( index_i != RW_NPOS )
    housekeeping   = _full_name( index_i, *index_ptr );

  // ----------------- debug comment -----------------
  //cerr<<"housekeeping:"<<housekeeping<<endl;

  // --- get the duration ---
  RWCString dura_tion( "" );
  index_i      = _full_name.index( dot, index_ptr, index_i+ *index_ptr );
  index_i      = _full_name.index( reg_digits, index_ptr, index_i );

  // ----------------- debug comment -----------------
  //cerr<<"index_i = "<<index_i<<" *index_ptr = "<<*index_ptr<<endl;
  if( index_i != RW_NPOS )
    dura_tion  = _full_name( index_i, *index_ptr );

  // ----------------- debug comment -----------------
  //cerr<<"dura_tion:"<<dura_tion<<endl;

  // --- get the  modulus --- 
  RWCString modu_lus( "" );
  index_i      = _full_name.index( dot, index_ptr, index_i + *index_ptr );
  index_i      = _full_name.index( reg_digits, index_ptr, index_i );

  // ----------------- debug comment -----------------
  //cerr<<"index_i = "<<index_i<<" *index_ptr = "<<*index_ptr<<endl;
  if( index_i != RW_NPOS )
    modu_lus   = _full_name( index_i, *index_ptr );

  // ----------------- debug comment -----------------
  //cerr<<"modu_lus:"<<modu_lus<<endl;

  // --- get the order of the fit polynomial
  RWCString order( "" );
  index_i      = _full_name.index( dot, index_ptr, index_i + *index_ptr );
  index_i      = _full_name.index( reg_digits, index_ptr, index_i );

  // ----------------- debug comment -----------------
  //cerr<<"index_i = "<<index_i<<" *index_ptr = "<<*index_ptr<<endl;
  if( index_i != RW_NPOS )
    order      = _full_name( index_i, *index_ptr );

  // ----------------- debug comment -----------------
  //cerr<<"order:"<<order<<endl;


  duration = atoi( dura_tion.data() );
  orderN   = atoi( order.data() );
  modulus  = atoi( modu_lus.data() );
  
  //
  // --- construct the descriptor attribute ---
  //

  descriptor  = "O[XTE]&I[";
  descriptor += instrument.data();
  descriptor += "]&D[";
  descriptor += detector.data();
  descriptor += "]&H[";
  descriptor += housekeeping.data();
  descriptor += "]";

  // ------------ debug comments -------------
  //  cerr<<"Trend.id       = "<<id<<endl;
  //  cerr<<"Trend.duration = "<<duration<<endl;
  //  cerr<<"Trend.modulus  = "<<modulus<<endl;
  //cerr<<"PolyTrendPCA::PolyTrendPCA: id = "<<id<<endl;
  //cerr<<"PolyTrendPCA::PolyTrendPCA: descriptor = "<<descriptor<<endl;

  unsigned packetRate = 8;
  RWCString tmp_string( "PCA." );
  
  //
  // -- all pca house keeping data is on one packet --
  // -- which comes at the rate of 1 packet/second --
  //

  unsigned elements = ( ( unsigned int )duration )/packetRate + 1;
  dataBin           = new DoubleVec( elements, 0.0 );

  delete index_ptr;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Name: PolyTrendPCA
// Description:
// A do nothing constructor to satisfy Rogue Wave
// Uses:
// Comments:
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
PolyTrendPCA::PolyTrendPCA( const Descriptor& _id, const long& _duration, 
		     const long& _modulus, const int& _order )
{
  orderN     = _order;
  duration   = _duration;
  modulus    = _modulus;
  descriptor = _id.getID();

  // ------------ debug comments -------------
  //  cerr<<"Trend.id       = "<<id<<endl;
  //  cerr<<"Trend.duration = "<<duration<<endl;
  //  cerr<<"Trend.modulus  = "<<modulus<<endl;
  //cerr<<"PolyTrendPCA::PolyTrendPCA: _id = "<<_id<<endl;

  unsigned packetRate = 8;
  RWCString tmp_string( "PCA." );

  //
  // -- all pca house keeping data is on one packet --
  // -- which comes at the rate of 1 packet/second --
  //

  unsigned elements = ( ( unsigned int )duration )/packetRate + 1;
  dataBin           = new DoubleVec( elements, 0.0 );
    
  char* tmp_buf = new char[128];
  tmp_string += _id.detector();
  tmp_string += ".";
  tmp_string += _id.housekeeping();
  tmp_string += ".";
  tmp_string += sprintf( tmp_buf, "%d", _duration );
  tmp_string += ".";
  tmp_string += sprintf( tmp_buf, "%d", _modulus );
  tmp_string += ".";
  tmp_string += sprintf( tmp_buf, "%d", _order );

  id = tmp_string;
  delete tmp_buf;

  // --------------- debug commment -----------------
  //cerr<<"PolyTrendPCA::PolyTrendPCA: id = "<<id<<endl;

}
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Name: ~PolyTrendPCA
// Description:
// A do nothing constructor to satisfy Rogue Wave
// Uses:
// Comments:
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
PolyTrendPCA::~PolyTrendPCA( void ){}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Name: process
// Description:
// A do nothing constructor to satisfy Rogue Wave
// Uses:
// Comments:
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
int  PolyTrendPCA::process( const RWCollectable* _part )
{
  int ret_val = 1;

  // ------------ debug comments -------------
  //cerr<<"Inside  PolyTrendPCA::process "<<endl;

  //
  // --- get information from partition convert to useful form ---
  //

  DataValue* dv;
  AccPcaHK   accessor( *( ( PcaHKPart* ) _part) );
  Descriptor d( descriptor );
  dv = accessor.access( d );
  long new_time = ( ( PcaHKPart* ) _part )->time();

  if( !dv ){
    //cerr<<"DataValue not set by accessor.access()"<<endl;
    return 0;
  }

  // ------------ debug comments -------------
  //cerr<<"after Accessor.access( d )"<<endl;
  //cerr<<"The time is "<<new_time()<<endl;

  double new_data;

  // ------------ debug comments -------------
  //cerr<<"descriptor = "<<d<<endl;

  // 
  // -- get the data type and convert the data to a double --
  //

  // ------------ debug comments -------------
  //cerr<<"before data type"<<endl;

  const DataType * dt = dv->getDataType();

  // ------------ debug comments -------------
  //cerr<<"after data type"<<endl;
  //cerr<<"DataType pointer = "<<dv<<endl;
  //cerr<<"PolyTrendPCA::process just before switch statement"<<endl;

  switch( dt->dataType() )
    {
      //
      // The data types currently used by PCA follow:
      //
    case BaseType_DOUBLE :
      {
	//cerr<<"PolyTren::process just before double"<<endl;
	new_data = ( double )dv->getDouble();
	//cerr<<"PolyTren::process just after double"<<endl;
	break;
      }
    case BaseType_FLOAT :
      {
	//cerr<<"PolyTren::process just before float"<<endl;
	new_data = ( double )dv->getFloat();
	//cerr<<"PolyTren::process just after float"<<endl;
	break;
      }
    case BaseType_INT :
      {
	//cerr<<"PolyTren::process just before int"<<endl;
	new_data = ( double )dv->getInt();
	//cerr<<"PolyTren::process just after int"<<endl;
	break;
      }

    case BaseType_CHAR :
      {
	//cerr<<"PolyTren::process just before char"<<endl;
	new_data = ( double )dv->getChar();
	//cerr<<"PolyTren::process just after char"<<endl;
	break;
      }
    case BaseType_SHORT :
      {
	//cerr<<"PolyTren::process just before short"<<endl;
	new_data = ( double )dv->getShort();
	//cerr<<"PolyTren::process just after short"<<endl;
	break;
      }
      // Currently no other PCA DataValue data types.
    }


  //
  // --- set startTime for first iteration -------
  //

  if( ( startTime == 0 ) && !( new_time%modulus ) )
    startTime = new_time;

  //
  // --- collect points if startTime
  //

  if( ( startTime != 0 ) && !( startTime%modulus ) ){
    
    stopTime  = new_time;
    nPoints++;

    // ------------ debug comments -------------
    //cerr<<id<<" now processing "<<nPoints<<" th point"<<endl;
    //cerr<<"new_time = "<<new_time<<endl;
    //cerr<<"new_data = "<<new_data<<endl;
    
    ( *dataBin )[nPoints - 1] = new_data;
  }

  // ------------ debug comments -------------
  //cerr<<"Leaving PolyTrendPCA::process"<<endl;

  delete dv;
  return ret_val;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Name: writeData
// Description:
// Writes the Poly trend statistics to a file in the specified path
// Uses: linearRegression, average, stdDeviation
// Comments: Returns 1 if write to file was successful, other returns 0.
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
int  PolyTrendPCA::writeData( const char* _path )
{

  // ------------ debug comments -------------
  //cerr<<"PolyTrendPCA::writeData, now entering....."<<endl;

  calculateCoeffs();
  //
  // ----- calculate coeffs before entering this method ------
  //

  if( coefficients->length() == 0 ){
    cerr<<"writeData: coefficient vector not set."<<endl;
    SOCThrow( SOC_RESPONSE,
	     "coefficients not set for %s , unable to write trend", id );
    return ( 0 );
  }


  //
  // --- try opening the out file before proceding ------
  //

  int ret_val = 1;
  ofstream out_file;

  //
  // --- create file name from id and duration 
  //

  char * full_path = (char *) calloc( 256, sizeof( char ) );
  char * file_name = (char *) calloc( 256, sizeof( char ) );
  char * tmp_str   = (char *) calloc( 64 , sizeof( char ) );  

  strcat( full_path, _path );
  strcat( full_path, "/" );
  strcat( full_path, id.data() );
  out_file.open( full_path, ios::app );

  if( !out_file ){
    cerr<<"Unable to open file for writing trends:\n"<<full_path<<endl;
    SOCThrow( SOC_RESPONSE, "PolyTrendPCA::writeData: cannot open %s , unable to write trend", full_path );
    free( file_name );
    free( full_path );
    free( tmp_str );
    return ( 0 );
  }


  // ------------ debug comments -------------
  //cerr<<"PolyTrendPCA::writeData -> Path is ... "<<full_path<<endl;

  //
  // ----- write data to file -----
  //

  out_file.fill( '0' );
  out_file.setf( ios::showpoint );
  
  out_file<<startTime<<" "     <<flush;
  out_file<<stopTime<<" "      <<flush;
  out_file<<orderN<<" "        <<flush;
  for( int i = 0; i <= orderN; i++ )
    out_file<<(*coefficients)[i]<<" "<<flush;
  double zero = 0.0;
  if( orderN < 1 ) out_file<<zero<<" "<<flush;
  if( orderN < 2 ) out_file<<zero<<" "<<flush;
  if( orderN < 3 ) out_file<<zero<<flush;
  out_file<<endl;
  out_file.close();
  ret_val = 1;

  // ------------ debug comments -------------
  //cerr<<"PolyTrendPCA::Leaving writeData...."<<endl;

  free( file_name );
  free( full_path );
  free( tmp_str );
  return ret_val;
 
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Name: checkTime
// Description:
// A do nothing constructor to satisfy Rogue Wave
// Uses:
// Comments:
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
int PolyTrendPCA::checkTime( const long& /* _in_time */ , const long& _del_t )
{
  int ret_val = 0;

  if( nPoints > 1 ){
    long last_time = ( startTime + duration ) - ( duration % _del_t );
    if(  nPoints == dataBin->length()  ){
      // ----------- deebug comments -----------------------------
      //cerr<<"PolyTrendPCA::checkTime -> Ready to write nPoints = "
      //<<nPoints<<endl;
      //cerr<<id<<" startTime: "<<startTime<<" stopTime: "<<stopTime<<endl;
      //cerr<<"thePacket->time() = "<<_in_time<<endl;
      ret_val = 1;
    }
  }

  return ret_val;
}

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Name: binaryStoreSize
// Description:
// 
// Uses:
// Comments:
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  RWspace PolyTrendPCA::binaryStoreSize() const
{
  RWspace size = 0;
  
  size += Trend::binaryStoreSize();
  size += sizeof( int );
  return size;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Name: hash
// Description:
// 
// Uses:
// Comments:
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//unsigned  PolyTrendPCA::hash() const;
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Name: restoreGuts
// Description:
// 
// Uses:
// Comments:
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  void PolyTrendPCA::restoreGuts( RWFile& f )
{
  Trend::restoreGuts( f );
  f.Read( orderN );
}
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Name: saveGuts
// Description:
// 
// Uses:
// Comments:
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  void PolyTrendPCA::saveGuts( RWFile& f ) const
{
  Trend::saveGuts( f );
  f.Write( orderN );
}
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Name: restoreGuts
// Description:
// 
// Uses:
// Comments:
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  void PolyTrendPCA::restoreGuts( RWvistream& v )
{
  // ------------------- debug comments ------------------
  //cerr<<"PolyTrendPCA::restoreGuts ................"<<endl;
  Trend::restoreGuts( v );
  v >> orderN;
  // ------------------- debug comments ------------------
  //cerr<<"PolyTrendPCA::restoreGuts, leaving ......."<<endl;
}
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Name: saveGuts
// Description:
// 
// Uses:
// Comments:
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  void PolyTrendPCA::saveGuts( RWvostream& v ) const 
{
  Trend::saveGuts( v );
  v << orderN;
}

@


1.3
log
@*** empty log message ***
@
text
@d9 1
a9 1
static char rcsid[] = "$Id: PolyTrendPCA.C,v 1.2 1994/12/28 15:20:08 leone Exp leone $";
d399 3
a401 3
  char * full_path = calloc( 256, sizeof( char ) );
  char * file_name = calloc( 256, sizeof( char ) );
  char * tmp_str   = calloc( 64 , sizeof( char ) );  
d458 1
a458 1
int PolyTrendPCA::checkTime( const long& _in_time, const long& _del_t )
@


1.2
log
@redelivered for build4.
@
text
@d9 1
a9 1
static char rcsid[] = $Id: PolyTrendPCA.C,v 1.1 1994/11/13 17:31:36 leone Exp leone $
@


1.1
log
@Initial revision
@
text
@d5 2
a6 3
// Description : PolyTrendPCA is a subclass of the Trend class which collects
//               data and prints min value, max value, mean, standard deviation
//               and slope and intercept of linear regression fit.
d9 1
a9 1
// RCS: $Id: $
d431 6
a436 1
  out_file<<*coefficients<<endl;        // loops through coefficients
@

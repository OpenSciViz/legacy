head	1.3;
access
	rbarnett
	rbentley
	soccm
	jschwan
	savkoor
	gromeo;
symbols
	Build4:1.3
	Build3_3:1.1;
locks; strict;
comment	@ * @;


1.3
date	94.12.28.19.45.59;	author leone;	state Exp;
branches;
next	1.2;

1.2
date	94.12.28.16.04.46;	author leone;	state Exp;
branches;
next	1.1;

1.1
date	94.11.13.17.32.55;	author leone;	state Exp;
branches;
next	;


desc
@@


1.3
log
@*** empty log message ***
@
text
@// ---------------------------------------------------------------------------
// File Name   : SimpleTrendHEX.h
// Subsystem   : SOC Health & Safety
// Programmer  : Rick Leone, Hughes STX/GSFC
// Description : SimpleTrend is a subclass of the Trend class which collects
//               data and prints min value, max value, mean, standard deviation
//               and slope and intercept of linear regression fit.
//
// ----------------------------------------------------------------------------
// RCS: $Id: SimpleTrendHEX.h,v 1.2 1994/12/28 16:04:46 leone Exp leone $
//
// .NAME    SimpleTrendHEX - Produces several trend data for housekeeping data
// .LIBRARY 
// .HEADER  SOC Health & Safety
// .INCLUDE Trend.h, DataValue.h
// .FILE    SimpleTrendHEX.h
// .AUTHOR  XTE-SOC 
// ----------------------------------------------------------------------------

#ifndef SIMPLE_TREND_HEX_H
#define SIMPLE_TREND_HEX_H

#include <rw/cstring.h>
#include <rw/collect.h>
#include <rw/lsqfit.h>
#include <rw/dvec.h>
#include <fstream.h>
#include <rw/regexp.h>

#include "Descriptor.h"
#include "Trend.h"
#include "SimpleTrend.h"
#include "DataValue.h"
#include "DataType.h"
#include "HxHk128Part.h"
#include "AccHxHk128.h"

#define SIMP_TREND_HEX_ID 941


//
// --- forward declare Trend Class ------
//

//class Trend;

//
// ---- SimpleTrend Class Definition -----
//

class SimpleTrendHEX : public SimpleTrend {

  //
  // ---- required by rogue wave ----
  //

  RWDECLARE_COLLECTABLE( SimpleTrendHEX )

  //
  // --- private data members -----
  // 

  // -- at the expense of storage space, these attributes ---
  // -- are set each time a partition is processed. Thus  ---
  // -- eliminating processing when access methods return ---
  // -- their values.                                     ---

  RWCString writePath;

  //
  // --- public  member functions ----
  //

  public:

  SimpleTrendHEX( void );
  SimpleTrendHEX( const RWCString&, const char* );
  SimpleTrendHEX( const Descriptor& d, const long& duration, long&, 
		  const char* );
  ~SimpleTrendHEX( void );
  
  //
  // ------ inherited from Trend -------
  //

  int  reset( const double& );
  int  process( const RWCollectable* );
  int  writeData( const char* );
  int  checkTime( const long&, const long&  );
  
  //
  // ------ some useful operators ---
  //


  //
  // ------- Rogue Wave virtuals for persistence ----
  //

  int compareTo( const RWCollectable* ) const;  
  RWspace binaryStoreSize() const;
  RWBoolean isEqual( const RWCollectable* ) const;
  //unsigned  hash() const;
  void restoreGuts( RWFile& );
  void saveGuts( RWFile& ) const;
  void restoreGuts( RWvistream& );
  void saveGuts( RWvostream& ) const;  

};

#endif




@


1.2
log
@redelivered for build4.
@
text
@d10 1
a10 1
static char rcsid[] = $Id: SimpleTrendHEX.h,v 1.1 1994/11/13 17:32:55 leone Exp leone $
@


1.1
log
@Initial revision
@
text
@d10 1
a10 1
// RCS: $Id:$
a80 6
//  double min( void );
//  double max( void );
//  double average( void );
//  double stdDeviation( void   );
//  int linearRegression( double&, double& );

@

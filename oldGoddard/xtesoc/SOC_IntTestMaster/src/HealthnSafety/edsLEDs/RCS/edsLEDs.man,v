head	1.2;
access
	rbarnett
	kmurphy
	soccm;
symbols
	Build4:1.2
	Build3_3:1.2
	Build3_2:1.2
	Build3_1:1.2
	Build3:1.2
	Current:1.2;
locks; strict;
comment	@# @;


1.2
date	94.01.23.20.15.23;	author lijewski;	state Exp;
branches;
next	1.1;

1.1
date	94.01.19.15.11.29;	author lijewski;	state Exp;
branches;
next	;


desc
@note specialized yet
@


1.2
log
@now OK
@
text
@.\"
.\"  $Id: edsLEDs.man,v 1.1 1994/01/19 15:11:29 lijewski Exp lijewski $
.\"
.TH INGEST 1
.SH NAME
edsLEDs \- realtime client displaying EDS housekeeping LED panel
.SH SYNOPSIS
\fBedsLEDs\fP [-daemonize] -display-client name [-read-from-stdin] [-port #]
-host hostname -name client-name
.br
.SH DESCRIPTION
This realtime client displays a panel of LEDs representing the
Housekeeping status of the EDS instrument.
The various bits in each
LED are explained in the Help file accessible via the HELP button on
the display.
It can be killed or halted by sending it a SIGTERM signal;
i.e. type
.I "kill -TERM pid"
, where
.I pid
is the process ID of the
to-be-killed
.I edsLEDs
process, provided you're either the owner
of the process or root.
For debugging purposes, a core dump of the process can be obtained by
sending it a SIGQUIT;
i.e. type
.I "kill -QUIT pid."
.SH SWITCHES
.TP
.B -daemonize
This instruct the realtime client to run as a daemon.  It will then
write all its error messages and debugging messages using syslog(3).
This option is expected to always be enabled during normal operations.
It is assumed that the /etc/syslog.conf file corresponding to the machine
on which the process is running 
is set up so that LOG_ERR messages are sent to the console and
appended to a log file.
When this option is not enabled, all error messages and debugging messages
instead go to the terminal.
.TP
.B "-display-client name"
This option is used to specify the name of the executable program that
.I edsLEDs
starts up and to which it communicates its X window
requests.
If
.I name
is not a full pathname; i.e. if the first character isn't a slash (/),
then the string
.I $SOCHOME/bin/$ARCH/
will be prepended to it.
The resulting string is expected to be the full pathname to the
executable program.
It defaults to
.I edsLEDs-win.
.TP
.B -read-from-stdin
When this option is enabled
.I edsLEDs
expects to read telemetry from its standard input instead of over the
connection to the Realtime Server.
This option overrules `-daemonize'.
.I edsLEDs exits when there are no more packets to be read
from stdin, or on any error.
None of the options
.I -port,
.I -host
or
.I -name
are used (hence they needn't be set) when this option is used.
This is here strictly for debugging purposes so that we can feed known
files of telemetry through
.I edsLEDs.
.TP
.B "-port #"
This option specifies the well-known port on which the Realtime Server
listens for connections from valid realtime clients and then passes
the appropriate telemetry to those interested (and connected) realtime
clients.
If this option is not set, there must be a well-defined TCP service in
.I "/etc/services"
named
.I realtime-server
from which the realtime client can determine a valid port number with
which to establish a connection with the Realtime Server.
Under normal operations it is
expected that a valid /etc/services entry will be supplied on all the
XTESOC machines so that this option doesn't need to be specified.
.TP
.B "-host hostname"
This option specifies the host machine on which the Realtime Server is
running.  This option must be specified unless
.I -read-from-stdin
is set.
.TP
.B "-name client-name"
This option specifies the name of the
.I edsLEDs
realtime client.
The name must correspond to an entry in the RT.CLIENTS table.
It is currently set in $SOCHOME/etc/RT.CLIENTS to
.I EDS-LED.
.SH "ENVIRONMENT VARIABLES"
.IP DISPLAY
This sets the machine and screen on which any X Windows opened by the
realtime client will be displayed.
.IP SOCHOME
This is the root directory of the XTESOC Integration and Test
directory.
There should be a subdirectory,
.I etc
,below this directory which should contain both the AUTH.HOSTS and
RT.CLIENTS files.
.IP SOCOPS
This is the root of the SOC operations directory tree.
.SH FILES
.PD 0
.TP 30
.TP
.B $SOCHOME/etc/AUTH.HOSTS
machines authorized to connect to the Realtime Server (and Realtime Ingest)
.TP
.B $SOCHOME/etc/RT.CLIENTS
table mapping realtime clients to their ApIDs and Spacecraft data types
.PD
.SH BUGS
Known current known.
.SH AUTHORS
The realtime client template was written by Mike Lijewski.
Ann Davis is responsible for the original implementation of the LED panel.
@


1.1
log
@Initial revision
@
text
@d2 1
a2 1
.\"  $Id: rtclient.man,v 2.3 1994/01/11 14:34:59 lijewski Exp $
d6 1
a6 1
rtclient \- generic realtime client.
d8 1
a8 1
\fBrtclient\fP [-daemonize] -display-client name [-read-from-stdin] [-port #]
d12 6
a17 52
This is a manpage for a generic realtime client.
The purpose of a realtime client is to analyze some subset
of the XTE CCSDS AOS Telemetry packets that arrive at the XTESOC in
realtime from the DCF.
.PP
Each realtime client connects to the Realtime Server over a well-known
Transmission Control Protocol
.I (TCP)
port.
After connecting to the Realtime Server the realtime client must tell
the Realtime Server who it is by passing its name to the Realtime
Server.
The name of the realtime client must be in the RT.CLIENTS file, or
else the Realtime Server will close the connection.
.PP
The name of each realtime client establishes the mapping of client
connections in the Realtime Server to the set of ApIDs and Spacecraft
(S/C) data that each realtime client is interested in receiving.
Only one realtime client for each name in RT.CLIENTS may be actively
connected to the Realtime Server at any one time.
The format for an entry in the RT.CLIENTS table is fully documented in
the RT.CLIENTS table itself.
.PP
The Realtime Server also checks that the machine from which it
receives a connection request is authorized to connect to it.
The file AUTH.HOSTS contains the names of all machines which are
authorized to connect to the Realtime Server (and Realtime Ingest).
Connections from machines that aren't in AUTH.HOSTS are refused.
.PP
Realtime clients have the capability to cache certain types of
Spacecraft (S/C) data for later perusal via an embedded SCDataCache
object.
In order to cache a specific type of S/C data the entry corresponding
to that realtime client in RT.CLIENTS must specify the appropriate S/C
mnemonic and the realtime client must also explicitly call the appropriate
member function on the embedded SCDataCache object.
The available types of S/C data and their corresponding mnemonics are
detailed in RT.CLIENTS; the appropriate SCDataCache member function,
and how to call it, are detailed in the realtime template code.
How to use a SCDataCache object is detailed in its manpage, obtainable
via
.I genman
from SCDataCache.h.
There is also a test program
.I tcache
provided in the $SOCHOME/src/Ingest directory, together with an
appropriate
.I tcache
target in the Ingest Subsystem Makefile, that can further illuminate
how to use the spacecraft data cache.
.PP
Any realtime client can be killed or halted by sending it a SIGTERM signal;
d23 3
a25 1
to-be-killed realtime client process, provided you're either the owner
d45 3
a47 4
This option is only recognized when the realtime client is compiled to
drive its own X window client.
In that case, it is the name of the executable program that the
realtime client starts up and to which it communicates its X window
d57 2
d61 4
a64 3
When this option is enabled, the realtime client expects to read
telemetry from its standard input instead of over the connection to
the Realtime Server.
d66 1
a66 1
The realtime client exits when there are no more packets to be read
d75 2
a76 1
files of telemetry through realtime clients.
d100 3
a102 1
This option specifies the name of the realtime client.
d104 2
d130 1
a130 1
Bugs, where?
d133 1
@

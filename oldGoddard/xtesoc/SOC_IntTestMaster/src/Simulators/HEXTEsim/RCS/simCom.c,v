head	1.1;
access
	soccm;
symbols
	Build4_1:1.1;
locks; strict;
comment	@ * @;


1.1
date	95.02.22.17.41.30;	author dgruber;	state Exp;
branches;
next	;


desc
@@


1.1
log
@Initial revision
@
text
@/*
// .NAME simCom.c - process to compose and send commands to the hexte Simulator
// .LIBRARY HEXTE
// .HEADER Hexte Simulator
// .FILE simCom.c
// .FILE simCom.h
//
// .SECTION AUTHOR
//  Duane Gruber,  UCSD
//  Greg Wood, UCSD
//
// .SECTION Description:
// A simple (to write) command interface to the Hexte Simulator.  Prompts
/* user first for instrument function, then for parameters of the function.
   Opens Socket connection to simulator process.  Composes command packets in
   Hexte format, including a part of the CCSDS header, a checksum, and byte swapping.
   Supports 4 commands at present _ SET_PULSER, NEW_MODE, MEM_LOAD and an
   unofficial command to toggle the cluster number.
   Command line option: -s<hostname> opens socket on host <hostname>
*/
#include <stdio.h>
#include <stdlib.h>
#include "inet.h"
#include "hexteSim.h"
#include "simCom.h"
#define SERVER_LUN 7654
#define COM_APID 50
	/* command's apid is in dispute, 1/14/95 */
FILE* txtfpt=0;
struct sockaddr_in serv_addr, cli_addr, serv_cmd, cli_cmd;
/* routine services HEXTE commanding in three steps: 1) interface to user
 to specify configuration, 2) generate commands, 3) place them in a stack
 for Hexte to process.  My plan is for functions 1) and 2) to be moved to
 a separate process, with a pipe to this process (Hexte simulator) disgorging
 commands onto the stack 3) within the simulator.
 HxSetTpg and HxSetScience are the interfaces 1), HxCmdTpg and HxCmdScience
 compose the commands */
#define CMD_LEN 26
struct sockaddr_in serv_addr,  cli_addr;

void CCSDS_CmdSim(unsigned short * ptr)
/* Puts in CCSDS header into command packets. */
{
    static unsigned short int ssc = 0;
    unsigned char * cptr = (unsigned char *)ptr;
    cptr[0] = 0x18;
    cptr[1] = COM_APID;  /*  if CID=1 */
    cptr[2] = 0xc | ssc>>4;
    cptr[3] = 0x00ff & ssc;
    cptr[4] = 0; cptr[5] = 0x34;   /* Pkt length */
    cptr[6] = 0;  /* Last two bytes reserved for someone else. */
    cptr[7] = 0;
    ssc++;
}

void map(unsigned short * ptr)
{
    unsigned char * cptr = (unsigned char *)ptr;
    int i;
    unsigned short len,max=20;
    unsigned char tmp;

    /*    for(i=0;i<20;i++)
	  printf("%2.2x ",cptr[i]);
	  putchar('\n');
     */

    len = ptr[2]>>9;		/* number of short (2 byte) ints */

    for(i=0;i<len-1;i++)
    {
	tmp = cptr[i*2];
	cptr[i*2] = cptr[i*2+1];
	cptr[i*2+1] = tmp;
    }             /* byte swaped the parts. */
    memmove(cptr+8,cptr,32);
    CCSDS_CmdSim(ptr);			/* put on CCSDS header. */
    /*    if(len<=12)
	  max = len*2;
	  for(i=0;i<max+8;i++)
	  printf("%2.2x ",cptr[i]);
	  putchar('\n');
     */
}
/*  Sample Main for testing map() function...*
int main()
{
    unsigned short ptr[22];
    ptr[2] = 4<<9;
    ptr[0] = 0xe0f0;
    ptr[1] = 0xe1f1;
    ptr[3] = 0xe3f3;
    ptr[4] = 0xe4f4;
    ptr[5] = 0xe4f4;
    ptr[6] = 0xe5f5;
    map(ptr);
}
*/
int sockfd;
int main(int argc, char * argv[])
{
   extern char *optarg;
   extern int optind, opterr;
   int n=CMD_LEN;
   int nw, c_opt, tpg, val;
   int errflag = 0;
   int bServer = 0;
   char servername[240];
   unsigned short int cmd [13];
   unsigned short int cmda[13];
   unsigned short int modetable[16];
   short int pulsper, pht, dest[4], decay, ramp;
   short xtr = 0;

 while((c_opt=getopt(argc,argv,"vs:")) != EOF)
    switch(c_opt) {
        case 's': 
           bServer = 1;
           strcpy(servername, optarg);
           break;
        
        default:
        case '?': 
           errflag++;
           break;
        }
 if(optind<(argc-1))
    errflag++;
 if(errflag) {
    fprintf(stderr, "usage: hxCmd [-s server]\n");
    exit(2);
    }

/* open socket */

   if((sockfd=serv_open((bServer)? servername :"",SERVER_LUN))<0)
     return(1);
   while (1)
   {
       tpg = HxTpgPow();
       if(tpg){
	   if(tpg==1)
	     cmd[0] = 1;
	   else
	     cmd[0] = 0;	/* cmd[0] is a flag to flipTpg 1->turn on 0-> turn off.*/
	   HxFlipTpg(cmd);
	   map( cmd );
	   nw = sendto(sockfd, (char *) cmd, n, 0, 
		       (struct sockaddr *)&serv_addr, sizeof(serv_addr));
	   printf("socket write %i bytes %x %x %x\n",nw,cmd[0],cmd[1],cmd[3]);
       }
       if(HxSetBurst(&val)) {
	   HxCmdBurst(cmd,val);
	   map( cmd );
	   nw = sendto(sockfd, (char *) cmd, n, 0, 
		       (struct sockaddr *)&serv_addr, sizeof(serv_addr));
	   printf("socket write %i bytes %x %x %x\n",nw,cmd[0],cmd[1],cmd[3]);
       }
       if(HxSetApMod(&val)) {
	   HxCmdApMod(cmd,val);
	   map( cmd );
	   nw = sendto(sockfd, (char *) cmd, n, 0, 
		       (struct sockaddr *)&serv_addr, sizeof(serv_addr));
	   printf("socket write %i bytes %x %x %x\n",nw,cmd[0],cmd[1],cmd[3]);
       }
       if(HxSetXtr()){
	   HxCmdXtr(cmd);
	   map ( cmd );
	   nw = sendto(sockfd, (char *) cmd, n, 0, 
		       (struct sockaddr *)&serv_addr, sizeof(serv_addr));
	   printf("socket write %i bytes %x %x %x\n",nw,cmd[0],cmd[1],cmd[3]);
       }       
       if(HxSetTpg(&pulsper, &pht, dest, &decay, &ramp)){
	   HxCmdTpg(pulsper, pht, dest, decay, ramp, cmd);
	   map ( cmd );
	   nw = sendto(sockfd, (char *) cmd, n, 0, 
		      (struct sockaddr *)&serv_addr, sizeof(serv_addr));
	   printf("socket write %i bytes %x %x %x\n",nw,cmd[0],cmd[1],cmd[3]);
       }
       if(HxSetScience(modetable)){
	   HxCmdScience(modetable, cmd, cmda);
	   if(cmd[0])                  /* mem load for special table */
	   {
	       map ( cmd );	       
	       nw = sendto(sockfd, (char *) cmd, n, 0, 
			   (struct sockaddr *)&serv_addr, sizeof(serv_addr));
	       printf("socket write %i bytes %x %x %x\n",nw,cmd[0],cmd[1],cmd[3]);
	       sleep(1);   /*wait one second before next cmd  */
	   }
	   map ( cmda );
	   nw = sendto(sockfd, (char *) cmda, n, 0, 
		       (struct sockaddr *)&serv_addr, sizeof(serv_addr));
	   printf("socket write %i bytes %x %x %x\n",nw,cmda[0],cmda[1],cmda[3]);
       }
   }
}
@

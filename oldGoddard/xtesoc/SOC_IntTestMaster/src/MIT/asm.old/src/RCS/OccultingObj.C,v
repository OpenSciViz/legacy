head	5.2;
access
	soccm
	nessus;
symbols
	merged-qlgo-process-da:5.2
	Build5_0_3:5.2
	Berczuk_final:5.2
	Beta3:5.2
	Beta2:5.2
	Beta1a:5.1
	Beta1:5.1
	Current:5.1
	Build4_3:1.1
	Build4_2a:1.1
	Build4_2:1.1
	Build4_1:1.1
	Build4:1.1
	BUILD4:1.1
	Build3_3:1.1
	Build3_2:1.1
	Build3_1:1.1
	Build3:1.1;
locks; strict;
comment	@ * @;


5.2
date	95.12.07.20.38.05;	author berczuk;	state Rel;
branches;
next	5.1;

5.1
date	95.09.18.21.05.10;	author berczuk;	state Rel;
branches;
next	4.1;

4.1
date	95.06.09.15.59.36;	author berczuk;	state Exp;
branches;
next	1.1;

1.1
date	94.05.12.20.34.57;	author berczuk;	state Exp;
branches;
next	;


desc
@Class to model an occulting object
@


5.2
log
@made occults() const and fixed up formatting and var names.
@
text
@//   RCS: $Id: OccultingObj.C,v 5.1 1995/09/18 21:05:10 berczuk Rel berczuk $ 
//   Author      : Steve Berczuk
//   Copyright 1994, Massachusetts Institute of Technology


#include <math.h>
#include <rw/dvec.h>
#include <SkyPt.h>

#include <OccultingObj.h>

static const char rcsid[]=
"$Id: OccultingObj.C,v 5.1 1995/09/18 21:05:10 berczuk Rel berczuk $"; 


SphericalOccultingObj::SphericalOccultingObj(double radius)
:theRadius(radius)
{


}


// Description: 
// returns a boolean value indicating if the source at
// position srcPos is occulted from an observer looking in the
// direction specified by vecTo Obj (ie, the pointing direction)
//, by this object. 
//  srcPos: positionof source in celestial sphere.
//  vecToObj: vector from a viewpoint to the center of this object
int SphericalOccultingObj::occults (const SkyPt& srcPos, 
				    const DoubleVec& vecToObj) const
{

    double magP =sqrt(dot(vecToObj,vecToObj));
    // magnitude of vector from to spacecraft
    
    double clipAngle = asin(theRadius/magP);
    // largest angle (between us and object we are looking at)
    // for which occultation will occur
    
    double pCosTheta= dot(srcPos.cartesian(),vecToObj);
    // the dot product between the source vector and the vector to the
    // object this = |p| cos(theta) where |p| is the magnitude of the
    // s/c position vector

    double theta=acos(pCosTheta/magP);
    // angle between s/c position vector and vector to occulting object

    return( (theta <= clipAngle) ? 1 : 0);
    // return 1 if we occult the source, 0 else.
} 


// rename vecToObj --> vecToUs
XteAngle SphericalOccultingObj::angleBetweenObjAndSrc(const SkyPt& srcPos,
                                                         const DoubleVec& 
                                                         vecToObj) const
{
    // vecToUs is a vector from the center of the object to the viewpoint
    // (ie spacecraft..)
    double magP =sqrt(dot(vecToObj,vecToObj));
    // magnitude of vector from to spacecraft
    
    double pCosTheta= dot(srcPos.cartesian(),vecToObj);
    // the dot product between the source vector and the vector to the
    // object this = |p| cos(theta) where |p| is the magnitude of the
    // s/c position vector

    double theta=acos(pCosTheta/magP);
    // angle between s/c position vector and vector to occulting object

    return XteAngle(theta);
    
}







@


5.1
log
@new build
@
text
@d1 1
a1 1
//   RCS: $Id: OccultingObj.C,v 4.1 1995/06/09 15:59:36 berczuk Exp berczuk $ 
d13 1
a13 1
"$Id: OccultingObj.C,v 4.1 1995/06/09 15:59:36 berczuk Exp berczuk $"; 
d32 1
a32 1
				    const DoubleVec& vecToObj)
d52 23
d76 2
@


4.1
log
@Bump to Rev4
@
text
@d1 1
a1 1
//   RCS: $Id: OccultingObj.C,v 1.1 1994/05/12 20:34:57 berczuk Exp berczuk $ 
d13 1
a13 1
"$Id: OccultingObj.C,v 1.1 1994/05/12 20:34:57 berczuk Exp berczuk $"; 
d27 2
a28 1
// direction specified by vecTo Obj, by this object. 
d36 1
a36 1
    // magnitude of vector from viwepoint to us
d39 2
a40 1
    // largest angle for which occultation will occur
d45 1
a45 1
    // s/c to object vector
d48 1
a48 1
    // angle between source vector and vector to occulting object
@


1.1
log
@Initial revision
@
text
@d1 1
a1 1
//   RCS: $Id$ 
d13 1
a13 1
"$Id$"; 
@

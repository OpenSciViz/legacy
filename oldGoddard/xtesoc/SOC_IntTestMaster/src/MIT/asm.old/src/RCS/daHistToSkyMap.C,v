head	5.1;
access
	soccm
	nessus;
symbols;
locks; strict;
comment	@ * @;


5.1
date	96.11.15.18.39.49;	author nessus;	state Exp;
branches;
next	1.2;

1.2
date	96.10.07.23.57.02;	author nessus;	state Exp;
branches;
next	1.1;

1.1
date	96.10.07.23.24.17;	author nessus;	state Exp;
branches;
next	;


desc
@This program converts a set of "da*" files into a "sky map" that is
in an ascii format suitable for loading into "Pair".
@


5.1
log
@Modified to look for files by week, rather than by day.
@
text
@//   RCS: $Id: daHistToSkyMap.C,v 1.2 1996/10/07 23:57:02 nessus Exp nessus $
//   Author      : Michael Vezie
//   Copyright 1994, Massachusetts Institute of Technology
// This file acts like daHistToAscii, except that the output (position
// histograms and aspect) is written to the output in a form that is
// readable by the xteasm code written by Garrett Jernigan and Michael
// Vezie, at UC Berkeley Space Sciences Lab (format described in
// xferpkt.h).
//
// Tips for compiling (for those not native to Boston :)
/*

setenv SOCHOME /nfs/gx5-1/d1/sochome
setenv SOCOPS /nfs/gx5-1/d1/socops
setenv LM_LICENSE_FILE /xte/vendor/CenterLine/configs/license.dat.dist
setenv LD_LIBRARY_PATH /xte/vendor/CenterLine//sparc-sunos4/lib:$LD_LIBRARY_PATH


# And then...

make asmSkyMap-client

*/
#include <iostream.h>
#include <strstream.h>
#include <PosHistogram.h>

#include <fcntl.h>

// My own stuff...
#include "xferpkt.h"

static const char rcsid[]= 
"$ID$"; 

static xferpkt xp;

char *outfile;
int outfd;

void
getHist(uint32 startTime, int seq)
{
    PosHistogram* theHist=0;
    theHist = PosHistogram::restore(startTime,seq);

    if (theHist == NULL)
    {
	cerr << "Couldn't find PosHistogram for (" << startTime << ", " <<
		seq << ")" <<endl;
	exit(1);
    }

    xp.rotang = theHist->asmRotation().radians();

    const AttitudeQuaternion att = theHist->scAttitude();
    xp.q1 = att.x;
    xp.q2 = att.y;
    xp.q3 = att.z;
    xp.q4 = att.w;

    const PositionCoordinates pc = theHist->scPosition();
    xp.x = pc.x;
    xp.y = pc.y;
    xp.z = pc.z;

    //// This is the big question.  I want Dwell Num to be 
    //// the absolute dwell number (eg. 66494956), and Dwell Seq
    //// to be the number of the particular dwell (0 to 64, etc).
    //// The dwell Seq seems fine, but the dwell num is way off.
    //cerr << "Dwell Num: " << theHist->dwellSequence() << ", ";
    //cerr << "Dwell Seq: " << theHist->dwellSequenceID() << endl;

    xp.deltat = theHist->getDwellDuration();
    // xp.dwell_num = theHist->dwellSequence();
    // xp.dwell_seq = theHist->dwellSequenceID();
    xp.dwell_num = startTime;
    xp.dwell_seq = seq;

    const int maxchans = 4;
    const int maxlen = 338;

    for (int d=0; d<3; d++)
    {
	for (int w=0; w<8; w++)
	{
	    int nchans = theHist->getNumChannels(d);
	    //cerr << "nchans: " << nchans << endl;
	    int chan;
	    for (chan=0; chan<nchans; chan++)
	    {
	        //cerr << "d: " << d << ", chan: " << chan << ", w: " << w <<endl;
		IntVec iv = theHist->getHistogram((SSC_ID)d, chan, w);
		if (iv.length() != maxlen)
		{
		    cerr << form("SSC: %d, chan: %d, wire; %d", d, chan, w)
			<< endl;
		    cerr << "Whoops!  current length " << iv.length()
				<< " != " << maxlen << endl;
		}
		//cerr << "Before bcopy(" << iv.length << " * sizeof(int))"<<endl;
		bcopy(iv.data(),xp.hists[d][w][chan],iv.length() * sizeof(int));
	    }
	    // If some detector is missing a channel
	    for ( ; chan < maxchans; chan ++)
	    {
		    //cerr << "Bzero'ing" << endl;
		    bzero(xp.hists[d][w][chan],maxlen * sizeof(int));
	    }
	}
    }
    //cerr << "Before write" << endl;
    if (write(outfd, (const char *)&xp, sizeof(xp)) != sizeof(xp))
       perror(outfile);
}

// main program
int main(int argc, char* argv[])
{
    PosHistogram::storeArchivesByWeek();
    if(argc != 3 && argc != 4)
    {
      cerr<< "\nusage: " <<argv[0] 
	  <<" <dwellStartTime> <dwellSequenceID> [ output_file ]"
	  <<"\n program will retrieve the histogram from the PH archive in\n"
	  <<"$SOCOPS/asm/data\n";
      exit(-1);
    }
    
    uint32 startTime = atoi(argv[1]);
    int seq= atoi(argv[2]);
    if (argc == 4)
    {
       outfd = open(argv[3], O_WRONLY|O_CREAT|O_TRUNC, 0666);
       outfile = argv[3];
    }
    else
    {
       outfd = 1;
       outfile = "stdout";
    }

    cerr << " processing start: " <<startTime
         << " dwell# " << seq <<endl;
    getHist(startTime, seq);

    exit (0);
}
@


1.2
log
@Apparently the signature of PosHistogram::restore changed to use a uint32
rather than an int since the last time this program was compiled, so I
modified daHistToSkyMap to reflect this.
@
text
@d1 1
a1 1
//   RCS: $Id: daHistToSkyMap.C,v 1.1 1996/10/07 23:24:17 nessus Exp nessus $
d120 1
@


1.1
log
@Initial revision
@
text
@d1 1
a1 1
//   RCS: $Id: asmSkyMap-client.C,v 1.8 1996/01/06 21:46:29 dv Exp dv $
d42 1
a42 1
getHist(int startTime, int seq)
d129 1
a129 1
    int startTime = atoi(argv[1]);
@

head	5.6;
access
	soccm
	nessus;
symbols
	merged-qlgo-process-da:5.6
	Build5_0_3:5.6
	Berczuk_final:5.6
	Beta3:5.6
	Beta2:5.5
	Beta1a:5.4
	Beta1:5.4
	Current:5.1
	Build4_3:1.1
	Build4_2a:1.1
	Build4_2:1.1
	Build4_1:1.1
	Build4:1.1;
locks; strict;
comment	@ * @;


5.6
date	95.12.21.20.30.17;	author berczuk;	state Rel;
branches;
next	5.5;

5.5
date	95.12.07.20.37.25;	author berczuk;	state Rel;
branches;
next	5.4;

5.4
date	95.10.24.21.04.07;	author berczuk;	state Rel;
branches;
next	5.3;

5.3
date	95.10.24.14.39.49;	author berczuk;	state Exp;
branches;
next	5.2;

5.2
date	95.10.24.14.31.10;	author berczuk;	state Exp;
branches;
next	5.1;

5.1
date	95.09.18.21.04.40;	author berczuk;	state Exp;
branches;
next	4.1;

4.1
date	95.06.09.15.58.53;	author berczuk;	state Exp;
branches;
next	1.1;

1.1
date	95.01.10.20.19.34;	author berczuk;	state Exp;
branches;
next	;


desc
@y
Class to transform histogram data in various ways.
@


5.6
log
@added solar array rotation setting for SSCpos hist.
 (do I really need this class any more?)
@
text
@//   RCS: $Id: HistXForm.C,v 5.5 1995/12/07 20:37:25 berczuk Rel berczuk $ 
//   Author      : Steve Berczuk
//   Copyright 1995, Massachusetts Institute of Technology

#include <rw/igenmat.h>
#include <SscPosHist.h>
#include <PosHistogram.h>
#include <Ssc.h>
#include "HistXForm.h"
#include <rw/dvecpik.h>

static const char rcsid[]= "$Id: HistXForm.C,v 5.5 1995/12/07 20:37:25 berczuk Rel berczuk $"; 

HistXForm::HistXForm()
{
}

CoAddXForm::CoAddXForm() 
{
}

SscPosHist CoAddXForm::coadd(const PosHistogram& ph, SSC_ID whichSsc) const
{
    int npos=ph.positionBins();
    DoubleVec histogram(npos*Ssc::NumWires,0);
    int nc=ph.getNumChannels(whichSsc);
    // flatten out the resulting array (position xwire) into 1 d
    for(int wire=0; wire < Ssc::NumWires; wire++)
    {
        for (int c=0 ; c <nc; c++){
            histogram(RWRange(wire*npos, npos*(1+wire) -1))+=
                ph.getHistogram(whichSsc,c,wire);
        }
    }
    SscDwellParams oldPars= ph.getParams().dwellParamsForSSC(whichSsc);
    RWTValVector<uint32> newBounds(2,0);
    newBounds[0] = oldPars.energyBoundaries(0);
    newBounds[1] = oldPars.energyBoundaries(oldPars. numChannels());
    
    SscDwellParams newPars(newBounds,
                           oldPars.numberOfPositionBins(),
                           oldPars.numberOfPositionIds(),
                           oldPars.binMin(),
                           oldPars.dwellDurationInSeconds(),
                           oldPars.configurationID());
    
    AttitudeQuaternion att(ph.scAttitude());
    PositionCoordinates pos(ph.scPosition());

    SscPosHist thd(histogram, newPars,att,pos,
                  ph.asmRotation(),
                   ph.solarArrayRotation(),
                  whichSsc);
    thd.dwellStartTime(ph.dwellStartTime());
    thd.dwellSequenceID(ph.dwellSequenceID());
    thd.dwellSequence(ph.dwellSequence());
    
    return thd;
    
}


SelectorXForm::SelectorXForm(int channel)
:theChannel(channel)
{
    
}


SscPosHist SelectorXForm::select(const PosHistogram& ph, SSC_ID whichSsc) const
{

    int npos=ph.positionBins();
    DoubleVec histogram(npos*Ssc::NumWires,0);
    for(int w =0; w< Ssc::NumWires; w++) {
        histogram(RWRange(w*npos, (1+w)*npos-1))= 
            ph.getHistogram(whichSsc, theChannel,w);
    }
    SscDwellParams oldPars= ph.getParams().dwellParamsForSSC(whichSsc);
    RWTValVector<uint32> newBounds(2,0);
    newBounds[0] = oldPars.energyBoundaries(theChannel);
    newBounds[1] = oldPars.energyBoundaries(theChannel+1);
    
    SscDwellParams newPars(newBounds,
                        oldPars.numberOfPositionBins(),
                        oldPars.numberOfPositionIds(),
                        oldPars.binMin(),
                        oldPars.dwellDurationInSeconds(),
                           oldPars.configurationID());
    
    SscPosHist thd(histogram,newPars);
    thd.dwellStartTime(ph.dwellStartTime());
    thd.dwellSequenceID(ph.dwellSequenceID());
    thd.dwellSequence(ph.dwellSequence());
    return thd;
    
}

@


5.5
log
@sundry simple changes.
@
text
@d1 1
a1 1
//   RCS: $Id: HistXForm.C,v 5.4 1995/10/24 21:04:07 berczuk Rel berczuk $ 
d12 1
a12 1
static const char rcsid[]= "$Id: HistXForm.C,v 5.4 1995/10/24 21:04:07 berczuk Rel berczuk $"; 
d52 1
@


5.4
log
@renamed DwellParams --> SscDwellParams
@
text
@d1 1
a1 1
//   RCS: $Id: HistXForm.C,v 5.3 1995/10/24 14:39:49 berczuk Exp berczuk $ 
d12 1
a12 1
static const char rcsid[]= "$Id: HistXForm.C,v 5.3 1995/10/24 14:39:49 berczuk Exp berczuk $"; 
d41 5
a45 4
                        oldPars.numberOfPositionBins(),
                        oldPars.numberOfPositionIds(),
                        oldPars.binMin(),
                        oldPars.dwellDurationInSeconds());
d87 2
a88 1
                        oldPars.dwellDurationInSeconds());
@


5.3
log
@renamed HistDwell-> SscPosHist
@
text
@d1 1
a1 1
//   RCS: $Id: HistXForm.C,v 5.2 1995/10/24 14:31:10 berczuk Exp berczuk $ 
d12 1
a12 1
static const char rcsid[]= "$Id: HistXForm.C,v 5.2 1995/10/24 14:31:10 berczuk Exp berczuk $"; 
d35 1
a35 1
    DwellParams oldPars= ph.getParams().dwellParamsForSSC(whichSsc);
d40 1
a40 1
    DwellParams newPars(newBounds,
d77 1
a77 1
    DwellParams oldPars= ph.getParams().dwellParamsForSSC(whichSsc);
d82 1
a82 1
    DwellParams newPars(newBounds,
@


5.2
log
@checkpoint before renaiming HistDwell class
@
text
@d1 1
a1 1
//   RCS: $Id: HistXForm.C,v 5.1 1995/09/18 21:04:40 berczuk Exp berczuk $ 
d6 1
a6 1
#include <HistDwell.h>
d12 1
a12 1
static const char rcsid[]= "$Id: HistXForm.C,v 5.1 1995/09/18 21:04:40 berczuk Exp berczuk $"; 
d22 1
a22 1
HistDwell CoAddXForm::coadd(const PosHistogram& ph, SSC_ID whichSsc) const
d49 1
a49 1
    HistDwell thd(histogram, newPars,att,pos,
d68 1
a68 1
HistDwell SelectorXForm::select(const PosHistogram& ph, SSC_ID whichSsc) const
d88 1
a88 1
    HistDwell thd(histogram,newPars);
@


5.1
log
@new build
@
text
@d1 1
a1 1
//   RCS: $Id: CoAddXForm.C,v 4.1 1995/06/09 15:58:53 berczuk Exp berczuk $ 
d12 1
a12 1
static const char rcsid[]= "$Id: HistXForm.C,v 4.1 1995/06/09 15:58:53 berczuk Exp berczuk $"; 
a25 1

d46 2
d49 3
a51 1
    HistDwell thd(histogram, newPars);
@


4.1
log
@Bump to Rev4
@
text
@d1 1
a1 1
//   RCS: $Id: HistXForm.C,v 1.1 1995/01/10 20:19:34 berczuk Exp berczuk $ 
d3 1
a3 1
//   Copyright 1994, Massachusetts Institute of Technology
d6 1
a6 1

d12 1
a12 1
static const char rcsid[]= "$Id: HistXForm.C,v 1.1 1995/01/10 20:19:34 berczuk Exp berczuk $"; 
d14 1
a14 2
HistXForm::HistXForm(const PosHistogram& ph) 
:thePosHist(ph)
d18 1
a18 1
DoubleVec HistXForm::coadd(SSC_ID whichSsc)
d20 5
a24 1
    int npos=thePosHist.positionBins();
a25 1
    IntGenMat coAdda(Ssc::NumWires,npos,0);
d27 1
a27 9
/*
    for(int w=0; w<Ssc::NumWires; w++) {

        {
            coAdda.row(w)+= thePosHist.getHistogram(whichSsc,c,w);
        }
    }
*/
    int nc=thePosHist.getNumChannels(whichSsc);
d33 1
a33 2
                thePosHist.getHistogram(whichSsc,c,wire);
            //coAdda.row(wire);
d36 11
a46 1
    return histogram;
d48 7
d57 3
a59 1
DoubleVec HistXForm::select(SSC_ID whichSsc, int whichChannel)
d61 2
d64 5
a68 1
    int npos=thePosHist.positionBins();
d72 1
a72 1
            thePosHist.getHistogram(whichSsc, whichChannel,w);
d74 4
a77 1
    return histogram;
d79 12
a91 1

@


1.1
log
@Initial revision
@
text
@d1 1
a1 1
//   RCS: $Id$ 
d12 1
a12 1
static const char rcsid[]= "$Id$"; 
@

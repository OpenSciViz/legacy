head	1.2;
access
	soccm
	nessus;
symbols
	Current:1.2
	Build4_3:1.2
	Build4_2a:1.2
	Build4_2:1.2
	Build4_1:1.2
	Build4:1.2;
locks; strict;
comment	@# @;


1.2
date	94.11.28.21.40.22;	author berczuk;	state Exp;
branches;
next	1.1;

1.1
date	94.11.14.20.40.44;	author berczuk;	state Exp;
branches;
next	;


desc
@man page for asm HK display client
@


1.2
log
@cleaned up for Build 4.0
@
text
@.\"
.\"  $Id: asmHK-client.man,v 1.1 1994/11/14 20:40:44 berczuk Exp berczuk $
.\"
.TH Asm Analysis 1
.SH NAME
asmHK-client \- realtime client to display asm HK data and
configuration of ASM EAs from telemetry.
asmHKDisplay \- TCL scripts to display this data
.SH SYNOPSIS
\fBasmHK-client\fP  [-read-from-stdin] [-port #] -host hostname -name
client-name [-poshist ] [-mtsdata ]
.br
\fBasmHKDisplay\fP
.br
.SH DESCRIPTION
asmHK-client reads packets from a real time connection, and
generates line oriented output that can be used to view the edsMonitor
is based on the protocols of the
.I rtclient 
(see) template
.PP
Each realtime client connects to the Realtime Server over a well-known
Transmission Control Protocol
.I (TCP)
port.
After connecting to the Realtime Server the realtime client must tell
the Realtime Server who it is by passing its name to the Realtime
Server. The information may also be displayed by using the client
.I asmHKDisplay
.PP
See
.I rtclient
for restrictions relating to naming clients and host authorization.
.SH SWITCHES
The following switches apply only when using the asmHK-client
directly. When the display programs are used, there the user is
presented with a dialog to enter this information.
.TP
.B -read-from-stdin
When this option is enabled, the realtime client expects to read
telemetry from its standard input instead of over the connection to
the Realtime Server.
This option overrules `-daemonize'.
The realtime client exits when there are no more packets to be read
from stdin, or on any error.
None of the options
.I -port,
.I -host
or
.I -name
are used (hence they needn't be set) when this option is used.
This is here strictly for debugging purposes so that we can feed known
files of telemetry through realtime clients.
.TP
.B "-port #"
This option specifies the well-known port on which the Realtime Server
listens for connections from valid realtime clients and then passes
the appropriate telemetry to those interested (and connected) realtime
clients.
If this option is not set, there must be a well-defined TCP service in
.I "/etc/services"
named
.I realtime-server
from which the realtime client can determine a valid port number with
which to establish a connection with the Realtime Server.
Under normal operations it is
expected that a valid /etc/services entry will be supplied on all the
XTESOC machines so that this option doesn't need to be specified.
.TP
.B "-host hostname"
This option specifies the host machine on which the Realtime Server is
running.  This option must be specified unless
.I -read-from-stdin
is set.
.TP
.B "-name client-name"
This option specifies the name of the realtime client.
The name must correspond to an entry in the RT.CLIENTS table.
.SH "OUTPUT FORMATS"
.SS MTS ASM Housekeeping data
One Housekeeping Packet will result is a display the looks like this:
.IP
ASM-MET 247762
.br
ASM-DRIVE-Angle (0 DEGREES)
.br
ASM-DRIVE-Drive5vdc 4.875
.br
ASM-DRIVE-Drive28vdc -4.71094
.br
ASM-DRIVE-GearMotor 22.313
.br
ASM-DRIVE-Ssc1Support 21.4248
.br
ASM-DRIVE-Ssc2Support 21.4737
.br
ASM-DRIVE-UpperBearing 21.7683
.br
ASM-DRIVE-LowerBearing 21.4248
.br
ASM-DRIVE-MountingFlange 20.9868
.br
ASM-DRIVE-ControlBPCBoard 22.1638
.br
ASM-SSC1-SERIALNo 1
.br
ASM-SSC1-LATCHED 0
.br
ASM-SSC1-HiVoltage -1782.16
.br
ASM-SSC1-6vdc 5.97656
.br
ASM-SSC1-p12vdc 12.0117
.br
ASM-SSC1-m12vdc -12.0044
.br
ASM-SSC1-CollMask 21.181
.br
ASM-SSC1-CollLeftMiddle 21.3271
.br
ASM-SSC1-CollLeftRear 20.8417
.br
ASM-SSC1-CollLeftFront 21.719
.br
ASM-SSC1-CollRightMiddle 21.5227
.br
ASM-SSC1-CollRightRear 20.89
.br
ASM-SSC1-CollRightFront 21.6699
.br
ASM-SSC1-RearRadiator 21.5227
.br
ASM-SSC1-TailRadiator 21.8668
.br
ASM-SSC1-LvpsCase 31.1327
.br
ASM-SSC1-FrontRadiator 22.663
.br
ASM-SSC1-CapsPCBoard 24.7689
.br
ASM-SSC2-SERIALNo 2
.br
ASM-SSC2-LATCHED 0
.br
ASM-SSC2-HiVoltage -1789.61
.br
ASM-SSC2-6vdc 6.01611
.br
ASM-SSC2-p12vdc 11.9897
.br
ASM-SSC2-m12vdc -12.063
.br
ASM-SSC2-CollMask 21.4737
.br
ASM-SSC2-CollLeftMiddle 21.7683
.br
ASM-SSC2-CollLeftRear 21.1324
.br
ASM-SSC2-CollLeftFront 21.6699
.br
ASM-SSC2-CollRightMiddle 21.6699
.br
ASM-SSC2-CollRightRear 21.2784
.br
ASM-SSC2-CollRightFront 21.719
.br
ASM-SSC2-RearRadiator 21.8175
.br
ASM-SSC2-TailRadiator 22.1142
.br
ASM-SSC2-LvpsCase 31.1945
.br
ASM-SSC2-FrontRadiator 22.7634
.br
ASM-SSC2-CapsPCBoard 24.7689
.br
ASM-SSC3-SERIALNo 3
.br
ASM-SSC3-LATCHED 0
.br
ASM-SSC3-HiVoltage -1790.03
.br
ASM-SSC3-6vdc 5.98096
.br
ASM-SSC3-p12vdc 12.0117
.br
ASM-SSC3-m12vdc -12.0557
.br
ASM-SSC3-CollMask 21.8668
.br
ASM-SSC3-CollLeftMiddle 21.8175
.br
ASM-SSC3-CollLeftRear 21.8175
.br
ASM-SSC3-CollLeftFront 21.6208
.br
ASM-SSC3-CollRightMiddle 21.7683
.br
ASM-SSC3-CollRightRear 21.6699
.br
ASM-SSC3-CollRightFront 21.5717
.br
ASM-SSC3-RearRadiator 21.6699
.br
ASM-SSC3-TailRadiator 21.719
.br
ASM-SSC3-LvpsCase 30.9478
.br
ASM-SSC3-FrontRadiator 22.313
.br
ASM-SSC3-CapsPCBoard 24.6638
.SS EA Configuration information
Configuration information is displayed when a complete partition is
available. The line containing this info looks like:
.IP
COMPLETE Part: apID: 48 # 23 ObsStartTime 247488. 2 Pkts. config ID:0x84000001
.SH "ENVIRONMENT VARIABLES"
.IP DISPLAY
This sets the machine and screen on which any X Windows opened by the
realtime client will be displayed.
.IP SOCHOME
This is the root directory of the XTESOC Integration and Test
directory.
There should be a subdirectory,
.I etc
,below this directory which should contain both the AUTH.HOSTS and
RT.CLIENTS files.
.IP XTE_TCL_LIBRARY
This should point to the directory containing XTE-specific TCL library
files. If this is not set, the TCL clients look at $SOCHOME/lib/tcl
.IP SOCHOME
.IP SOCOPS
This is the root of the SOC operations directory tree.
.SH "SEE ALSO"
rtclient
.SH BUGS
Bugs, where?
.SH AUTHORS
The realtime client template was written by Mike Lijewski. 
The display clients and tcl scripts were written by Steve Berczuk.
@


1.1
log
@Initial revision
@
text
@d2 1
a2 1
.\"  $Id$
a211 1

@

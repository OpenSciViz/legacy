head	1.1;
access
	soccm
	nessus;
symbols;
locks; strict;
comment	@ * @;


1.1
date	96.06.04.22.17.54;	author nessus;	state Exp;
branches;
next	;


desc
@@


1.1
log
@Initial revision
@
text
@/* fitgrid.c
 * Alan M. Levine
 * April 17, 1996
 *
 * This program reads in an ASCII dwell object, computes response functions to
 * cataloged sources, and performs fits for a grid of delta-phi, delta-position_angle
 * values.
 * 
 * This is the first program to use calibration parameters that specify DA and 
 * SSC pointing offsets.
 */


#include <math.h>
#include <stdio.h>
#include "ssc_b.h"
#include "dwasclib.h"
#include "fit.h"
#include "prints.h"

#define NDELPHI 5
#define NDELTLT 1
#define DELPHI 0.025
#define DELTLT 0.04
#define PHICEN 0.0
#define TLTCEN 0.0

#define MET_ZERO  62985600.0
#define MJD_ZERO  50082.0
#define CTS_MIN   2000
#define SIGRMV    2.0

#define PTCL_AREA 121.920   /* cm**2, full wire; 101.07 in orig. hist mode */
#define XBG_AREA  67.285    /* w/o mask consider. */
#define SSC_AREA  28.793    /* sum of on-axis response */
#define OPEN_FRAC 0.4386  /* open portion of mask, incl. support bar */

#define PI 3.1415926535

void main(argc,argv)
  int argc;
  char **argv;
{
    struct ascdw dwell;
    struct ascdw *pdw;

    char xcat[30], name[40][30];
    double stars[40][13], T[40][2720], fe55ph[1][2720];
    double mjd, eav, sscra, sscdec, sscpa;
    double ascale;
    int offset, dsum, nstar, npos, nsamp, ssc_num;
    int i, j;

    double qav[4], dr, sscrmat[3][3];
    double chisq;
    double y[RSPBINS], siginsq[RSPBINS], a[MAXSRCS], asig1[MAXSRCS];
    int ntot, nmod;

    void posresp_b(), sscmat(), sscptg(), sscresppack(), datapack(), fixresp();
    void printsumhdr(), printsum(), fe55poshist();
    int rdasmcat();
    void fe55pack(), printfe55();

    int iphi, itlt;
    double dltphi, dlttlt, neweulerssc[3], chisqgrid[NDELPHI][NDELTLT];

    dr = PI/180.0;

    pdw = &dwell;
    strcpy(xcat,"asmcat_1.0");

/* READ IN ASCII VERSION OF ASM DWELL OBJECT  */
    readasciidw(pdw);
    printasciihdr(pdw);
    /* printasciidata(pdw); */
    fflush(stdout);

/* PERFORM INITIALIZATIONS */
    mjd = (pdw->dwstartsec + (0.5 * pdw->exptm) - MET_ZERO) / 86400.0 + MJD_ZERO;
    ascale = SSC_AREA/pdw->exptm;
    offset = 0;
    eav = 4.0;
    nsamp = 1;
    sscinit_b(pdw->bins,pdw->bin1,pdw->keepbins,1);

/* Get average quaternion */
    dwquatav(pdw,qav);
    printf("qav = %lf %lf %lf %lf\n",qav[0],qav[1],qav[2],qav[3]);
    fflush(stdout);

    for(ssc_num = 0; ssc_num < 3; ssc_num++) 
	{
	    dsum = pdw->ssc_sum[ssc_num][0] + pdw->ssc_sum[ssc_num][1] + pdw->ssc_sum[ssc_num][2];
	    printf("ssc_num, dsum = %d %d\n",ssc_num,dsum);

	    if(dsum >= CTS_MIN) 
		{      
		    printf("eulerda= %lf %lf %lf\n",eulerda[0],eulerda[1],eulerda[2]);
		    printf("eulerssc = %lf %lf %lf\n",eulerssc[ssc_num][0],eulerssc[ssc_num][1],eulerssc[ssc_num][2]);

                    /* Get predicted histogram of 6 keV calibration photon events */
		    fe55poshist(ssc_num,pdw->bins,pdw->bin1,pdw->keepbins,nsamp,
				pdw->exptm,mjd,fe55ph);
		    /* printfe55(pdw->keepbins,fe55ph); */
      		    fe55pack(0,0,pdw->keepbins,fe55ph); /* Pack fe55ph into fe55mod */

/* Slip & slide loops start here */
	    for (iphi=0;iphi<NDELPHI;++iphi)
	      {
		for (itlt=0;itlt<NDELTLT;++itlt)
		   {
		    for(i=0;i<3;++i)
			neweulerssc[i] = eulerssc[ssc_num][i];
		    if (ssc_num < 2)
			{
			    neweulerssc[0] += (iphi-NDELPHI/2)*DELPHI+PHICEN;
			    neweulerssc[2] += (itlt-NDELTLT/2)*DELTLT+TLTCEN;
			}
		    else
			{
			    neweulerssc[1] -= (iphi-NDELPHI/2)*DELPHI+PHICEN;
			    neweulerssc[0] += (itlt-NDELTLT/2)*DELTLT+TLTCEN;
			}
		    printf("neweulerssc = %lf %lf %lf\n",neweulerssc[0],neweulerssc[1],
			   neweulerssc[2]);

		    sscmat(qav,eulerda,pdw->asmrot,neweulerssc,sscrmat);
/*		    for(i=0;i<3;++i)
			{
			    for(j=0;j<3;++j)
				printf(" %lf",sscrmat[i][j]);
			    printf("\n");
			}
*/
		    sscptg(sscrmat,&sscra,&sscdec,&sscpa);
		    npos = (pdw->npos + 1)/2;
		    printf("npos,mjd = %d %lf\n",npos,mjd);
		    printf("sscra,sscdec,sscpa = %lf %lf %lf\n",sscra,sscdec,sscpa);
		    nstar = rdasmcat2(xcat,&pdw->xtepos[npos][1],mjd,sscrmat,stars,name);

		    fprintf(stderr,"calculate response matrix for %d sources and 2 types of BG\n",
			    nstar-2);

		    if(nstar > 40)
			{
			    fprintf(stderr,"exit: #_sources + 2_BG > 40\n");
			    exit(-3);
			}

		    sscrespinit();
		    posresp_b(ssc_num,eav,pdw->bins,pdw->bin1,pdw->keepbins,nsamp,
			      nstar,stars,T);

                    /* Copy "T" into section of "sscresp" */
		    sscresppack(0,0,nstar,T);

/* Initialize data and statistical error arrays for fitting */
		    datapack(pdw,0,0,ssc_num,y,siginsq);

/* Fix response functions and data array for burned-out anodes, empirical 
efficiencies, etc. */
		    fixresp(0,0,ssc_num,nstar,pdw->dwstartsec,338,y);

/* Perform fitting sequence */
                    ntot = 338*8;
		    chisq = fitseq(y,siginsq,a,nstar,stars,asig1,ntot,&nmod,SIGRMV);

/* Print results */

/* Need functions to:
 *   Print response functions
 *   Print residuals
 *   Print fit solutions
 */

/*   Print fitted model */
		    if ( (iphi == NDELPHI/2) && (itlt == NDELTLT/2))
			{
			    printmodel(pdw->dwseq,pdw->dwno,0,0,nmod,a);
			    printresid(pdw->dwseq,pdw->dwno,0,0,nmod,a,y);
			    printsqdev(pdw->dwseq,pdw->dwno,0,0,nmod,a,y);
			}

/*   Print solution summary */
		    printsumhdr(pdw,ssc_num,sscra,sscdec,sscpa,chisq,nmod,dsum);
		    printsum(nmod,name,stars,ascale,a,asig1);

		    chisqgrid[iphi][itlt] = chisq;

		   } /* end of itlt loop */
	      } /* end of iphi loop */

/*    print grid of chi-square values */
            printf("itlt=   ");
	    for (itlt=0;itlt<NDELTLT;++itlt)
		printf("%8d",itlt);
            printf("\n");
	    for (iphi=0;iphi<NDELPHI;++iphi)
	      {
		printf("iphi %2d",iphi);  
		for (itlt=0;itlt<NDELTLT;++itlt)
		   {
		       printf(" %7.2lf",chisqgrid[iphi][itlt]);
		   }
		printf("\n");
	      }

		} /* end if for dsum > CTS_MIN */
	}   /* end loop for ssc_num */
} /* end of main program fitgrid.c */

/******************************************************************************/
void fe55pack(ndw,nssc,pbinct,fe55ph)
int ndw,nssc,pbinct;
double fe55ph[1][2720];
{
    int kbins, j, k, indt, inds;

    kbins = pbinct+1;
    for(j=0;j<8;++j)
	for(k=0;k<kbins;++k)
		{
		    indt = (kbins+2)*j+k+1;
		    inds = kbins*(8*(2*ndw+nssc)+j)+k;
		    fe55mod[inds] = fe55ph[0][indt];
/*		    fe55mod[inds] = 0.0; */
		}

    return;
}

/******************************************************************************/
void printfe55(pbinct,fe55ph)
int pbinct;
double fe55ph[1][2720];
{
    int i, j, ibin;

    for (i=0;i<=pbinct;++i)
	{
	    printf("%3d  ",i);
	    for (j=0;j<8;++j)
		{
		    ibin = (pbinct+1+2)*j+i+1;
		    printf("%8.3lf ",fe55ph[0][ibin]);
		}
	    printf("\n");
	}
    return;
}

/******************************************************************************/
/* Copy "T" into section of "sscresp" */
/* nssc = 0 if data from 1 SSC is being fit (or first of two SSCs)
 *      = 1 if second of 2 SSCs
 */
void sscresppack(ndw,nssc,nsrc,T)
double T[40][2720];
int ndw, nssc,nsrc;
{
    int i, j, k, indt, inds;

    for(i=0;i<nsrc;++i)
	for(j=0;j<8;++j)
	    for(k=0;k<338;++k)
		{
		    indt = 340*j+k+1;
		    inds = 338*(8*(2*ndw+nssc)+j)+k;
		    sscresp[i][inds] = T[i][indt];
		}
    return;
}

/******************************************************************************/
/* Copy data into section of "y" */
void datapack(pdw,ndw,nssc,ssc_num,y,siginsq)
struct ascdw *pdw;
int ndw, nssc, ssc_num;
double y[RSPBINS], siginsq[RSPBINS];
{
    int j, k, ien, in;

	for(j=0;j<8;++j)
	    for(k=0;k<338;++k)
		{
		    in = 338*(8*(2*ndw+nssc)+j)+k;
		    y[in] = 0.0;
		    for(ien=0;ien<pdw->nchan;++ien)
			y[in] += (double) pdw->poshist[ssc_num][ien][k][j];
		    siginsq[in] = 1.0;
		}
    return;
}

/******************************************************************************/
/* This function adjusts the computed response functions and data array for:
 *  1) burned out anodes
 * 
 * This works for one SSC for one dwell.
 */
void fixresp(ndw,nssc,ssc_num,nstar,dwsec,keepbins,y)
int ndw, nssc, ssc_num, nstar, keepbins;
double dwsec;
double y[RSPBINS];
{
    int j, k, loc;

    /* Ignore anode code 6 (has strong changing "dip") for now */
    if(ssc_num == 1)
	{
	    for(k=0; k <= keepbins; k++)
		{
		    loc = keepbins*(8*(2*ndw+nssc)+6)+k;
		    y[loc] = 0.0;
		    fe55mod[loc] = 0.0;
		    for(j = 0; j < nstar; j++) sscresp[j][loc] = 0.0;
		}
	}

    if( (ssc_num == 1) && (dwsec > 64050000.0) )
	{
	    for(k=0; k <= keepbins; k++)
		{
		    loc = keepbins*(8*(2*ndw+nssc)+4)+k;
		    y[loc] = 0.0;
		    fe55mod[loc] = 0.0;
		    for(j = 0; j < nstar; j++) sscresp[j][loc] = 0.0;
		}
	}
      
    if( (ssc_num == 2) && (dwsec > 63525500.0) )
	{
	    for(k=0; k <= keepbins; k++)
		{
		    loc = keepbins*(8*(2*ndw+nssc)+3)+k;
		    y[loc] = 0.0;
		    fe55mod[loc] = 0.0;
		    for(j = 0; j < nstar; j++) sscresp[j][loc] = 0.0;
		    loc = keepbins*(8*(2*ndw+nssc)+4)+k;
		    y[loc] = 0.0;
		    fe55mod[loc] = 0.0;
		    for(j = 0; j < nstar; j++) sscresp[j][loc] = 0.0;
		}
	}
      
}

/******************************************************************************/
/* Print summary header of the results of one fit */
void printsumhdr(pdw,ssc_num,ra,dec,pa,chisq,nmod,dsum)
struct ascdw *pdw;
double ra, dec, pa, chisq;
int ssc_num, nmod, dsum;
{
      printf("%.1lf  %d  %2d  %d   %8.3lf   ", pdw->dwstartsec, pdw->dwseq,
	      pdw->dwno, ssc_num, pdw->asmrot);
      printf("%8.4lf %8.4lf %8.3lf  %6.3lf   %2d   %4d  ", ra, dec, pa,
	     chisq, nmod-2, dsum);
/*      printf("  %7.4lf  %7.4lf  %5.3lf   %6.3lf", shift_th, shift_phi, 
 *     shift_pa, chibest);
 */
      printf("\n");
}

/******************************************************************************/
/* Print summary of the results of one fit */
void printsum(nmod,name,stars,ascale,a,asig)
char name[MAXSRCS][30];
double stars[MAXSRCS][13], ascale, a[MAXSRCS], asig[MAXSRCS];
int nmod;
{
    int i;

    for(i=0;i<nmod;++i)
	{
            /* print source name, phi, theta, earthangle, fit intensity, fit error */
	    printf("%3d  %30s  %8.3lf %8.3lf %8.3lf   %8.3lf %8.3lf\n",
		   i,&name[i][0],stars[i][1],stars[i][2],stars[i][6],ascale*a[i],
		   ascale*asig[i]);
	}

}
@

head	1.5;
access
	soccm
	nessus;
symbols
	merged-qlgo-process-da:1.2;
locks; strict;
comment	@# @;


1.5
date	96.11.05.16.35.06;	author nessus;	state Exp;
branches;
next	1.4;

1.4
date	96.11.01.22.50.54;	author nessus;	state Exp;
branches;
next	1.3;

1.3
date	96.07.03.20.14.52;	author nessus;	state Exp;
branches;
next	1.2;

1.2
date	96.06.05.22.27.18;	author nessus;	state Exp;
branches;
next	1.1;

1.1
date	96.06.04.22.17.54;	author nessus;	state Exp;
branches;
next	;


desc
@@


1.5
log
@Fixed weiGo target to depend on object files created from C source files.
@
text
@###############################################################################
### You must use GNU make with this Makefile.
###
### $Id: Makefile,v 1.4 1996/11/01 22:50:54 nessus Exp nessus $
###
### Description : Makefile for Ron's ASM Analysis software
### Author      : Douglas Alan <nessus@@space.mit.edu>
### Copyright   : Massachusetts Institute of Technology, 1996
###############################################################################


###############################################################################
### The following make variables should be defined via environment variables:
###############################################################################

### SOCMASTER --  Directory for master version of SOC-developed libraries
###		  and headers
### SOCHOME   --  Directory for personal copies of SOC-developed libraries
###	          and headers
### SOCLOCAL  --  Directory where third-party libraries and headers live
### CCROOT_I  --  Directory where headers provided by the compiler vendor live
### ARCH      --  String that is used by convention to name subdirectories
###		  which hold binaries for the current CPU
### PURIFY    --  Set this environment varibale to "purify" or "purify
###               -{options}" if you wish to link with purify.  Otherwise,
###               leave it unset.


### Setup include and library path depending on which environment variables
### are set:

ifdef SOCMASTER
   SOCMASTER_INC := -I$(SOCMASTER)/include
   SOCMASTER_LIB := -L$(SOCMASTER)/lib/$(ARCH)
else
   SOCMASTERINC :=
   SOCMASTERLIB :=
endif

ifdef SOCHOME
   SOCHOME_INC := -I$(SOCHOME)/include
   SOCHOME_LIB := -L$(SOCHOME)/lib/$(ARCH)
else
   SOCHOME_INC :=
   SOCHOME_LIB :=
endif

ifdef SOCLOCAL 
   SOCLOCAL_INC := -I$(SOCLOCAL)/include
   SOCLOCAL_LIB := -L$(SOCLOCAL)/lib/$(ARCH)
else
   SOCLOCAL_INC :=
   SOCLOCAL_LIB :=
endif


###############################################################################
### Variable definitions:
###############################################################################

CC :=		cc
CFLAGS :=	-g
CXX :=		CC
CXXFLAGS :=	-g -pta
CPPFLAGS :=	$(SOCHOME_INC) $(SOCMASTER_INC) $(SOCLOCAL_INC)
libpath := 	$(SOCHOME_LIB) $(SOCMASTER_LIB) $(SOCLOCAL_LIB)
link :=		$(PURIFY) $(CXX) -Bstatic $(CFLAGS) $(CPPFLAGS) $(libpath)

library := $(SOCHOME)/lib/$(ARCH)/libQlAnalysis.a

libs := -lm

sources := dwasclib.C qlAnalysisTools.C sscinit1.C sscinit2.C sscinit3.C \
	weiSetup.C
c-sources := getfov.c dwposresp.c sunpos.c earthangle.c getfovcoord.c \
	todays.c gaussint.c
headers := dwasclib.h  qlAnalysisTools.h ssc2.h weiSetup.h
sources-with-main := qlgo.C dwcheck.C weiGo.C

objects := $(sources:.C=.o) $(c-sources:.c=.o)
all-sources := $(sources) $(c-sources)

###############################################################################
### THE DEFAULT TARGET
###############################################################################

default-target: qlgo

# check: 
#	@@./test-EdsConfig
# .PHONY: check

###############################################################################
### EXECUTABLES -- Executables are made by this makefile via the implicit rule
### below.  Among the executables that can be built are the following:
###
### qlgo -- [TBD].
###############################################################################

executables := $(sources-with-main:.C=)

### Turn off implicit rules that we don't want:
%: %.o 
%: %.C
%: %.c

### This implicit rule will compile and link any executable of our
### executables from a .o file of the same name:

%: %.o $(objects)
	$(link) -o $@@ $@@.o $(objects) $(libs)
.PRECIOUS: %.o

### We have a special rule for building weiGo because it contains duplicate
### copies of some procedures.  Also, we don't want it to contain a copy of
### qlAnalyze(), which is really big.

weiGo: weiGo.o dwasclib.o sscinit1.o sscinit2.o sscinit3.o weiSetup.o \
	$(c-sources:.c=.o)
	$(link) -o $@@ $@@.o dwasclib.o sscinit1.o sscinit2.o sscinit3.o \
	weiSetup.o $(c-sources:.c=.o) $(libs)


###############################################################################
### TARGET clean -- deletes all .o, executable, and dependency files.
###
### TARGET realclean -- delelets all files that 'clean' deletes, plus
### it also deletes all files ending in ~ or ".bak".
###############################################################################

.PHONY: cleano cleanexe cleanpt cleand clean realclean

cleano:
	rm -f *.o core *..c

cleanexe:
	rm -f a.out test $(executables)

cleanpt:
	rm -rf ptrepository pttmp

cleand:
	rm -f Makefile.d
	rm -rf .d

clean:	cleano cleanexe cleanpt cleand


# Target 'realclean' removes all files ending in ~ or ".bak".  It also
# deletes the db.tar

realclean: clean
	@@echo
	@@echo "Deleting ~ and .bak files..."
	$(RM) *~ *.bak
	@@echo "Running rcsclean..."
	rcsclean


###############################################################################
### TARGET sources -- checks out all of the sources
###############################################################################

.PHONY: sources

sources: $(all-sources) $(headers) Makefile


###############################################################################
### TARGET pubhdrs -- Installs in $SOCHOME/include all of the .h files and all
### of the template .C files.
###############################################################################

.PHONY: pubhdrs pubhdrs-socmaster

pubhdrs: $(headers)
	@@echo "Installing header files..."
	cd $(SOCHOME)/include; $(RM) $(headers)
	cp -p $(headers) $(SOCHOME)/include
	cd $(SOCHOME)/include; chmod 444 $(headers)

## This target installs the headers into $SOCMASTER, rather than $SOCHOME:

pubhdrs-socmaster: $(headers)
	@@echo 'Installing header files into $$SOCMASTER...'
	cd $(SOCMASTER)/include; \
	$(RM) $(headers)
	cp -p $(headers) $(SOCMASTER)/include
	cd $(SOCMASTER)/include; chmod 444 $(headers)


###############################################################################
### TARGET publibs -- Installs in $SOCHOME/lib/$ARCH a library containing all
### of the .o files.
###############################################################################

.PHONY: publibs 

publibs: $(objects)
	$(RM) $(library)
	ar rc $(library) $(objects)
ifneq ($(ARCH),SunOS5)
	ranlib $(library)
endif


###############################################################################
### DEPENDENCIES -- this section creates, reads in, and updates the
### dependency files.  The environment variable CCROOT_I should be set
### to the location of the standard C++ include files.
###
### TARGET depend -- Update the dependencies file.
###############################################################################

.PHONY: depend

depend: sources
ifdef CCROOT_I
	@@echo "Building depencies file (Makefile.d)..."
	makedepend -f - $(CPPFLAGS) -I$(CCROOT_I) $(all-sources) > Makefile.d
else # CCROOT_I
	@@echo
	@@echo "WARNING: $$(CCROOT_I) is not set, which means that the depend"
	@@echo "file cannot be built properly.  If you are building EdsConfig"
	@@echo "from scratch then this does not matter...."
	@@echo
	cp /dev/null Makefile.d
endif # CCROOT_I

Makefile.d:
	touch Makefile.d

include Makefile.d
@


1.4
log
@Added support for building weiGo.
@
text
@d4 1
a4 1
### $Id: Makefile,v 1.3 1996/07/03 20:14:52 nessus Exp nessus $
d118 2
a119 1
weiGo: weiGo.o dwasclib.o sscinit1.o sscinit2.o sscinit3.o weiSetup.o
@


1.3
log
@Updated Makefile to support pubhdrs and publibs targets.
@
text
@d4 1
a4 1
### $Id: Makefile,v 1.2 1996/06/05 22:27:18 nessus Exp nessus $
d73 2
a74 1
sources := dwasclib.C qlAnalysisTools.C sscinit1.C sscinit2.C sscinit3.C
d77 2
a78 2
headers := dwasclib.h  qlAnalysisTools.h ssc2.h
sources-with-main := qlgo.C dwcheck.C
d84 1
a84 2
### THE DEFAULT TARGET -- builds a test program and runs it to verify proper
### operation of the test-EdsConfig module.
d113 8
@


1.2
log
@Added rcsclean to "make realclean".
@
text
@d4 1
a4 1
### $Id: Makefile,v 1.1 1996/06/04 22:17:54 nessus Exp nessus $
d69 2
d73 1
a73 1
sources := dwasclib.C sscinit1.C sscinit2.C sscinit3.C
d76 1
a76 1
headers := dwasclib.h  ssc2.h
d158 38
@


1.1
log
@Initial revision
@
text
@d4 1
a4 1
### $Id: Makefile,v 1.1 1996/06/04 20:12:22 nessus Exp $
d145 2
@

head	5.12;
access
	soccm
	nessus;
symbols
	Build5_0_3:5.12
	Berczuk_final:5.12
	Beta3:5.12
	Beta2:5.11
	Beta1a:5.10
	Beta1:5.9
	Current:5.3
	Build4_3:3.4
	Build4_2a:3.4
	Build4_2:3.4
	Build4_1:3.4
	Build4:3.4
	BUILD4:3.3
	Build3_3:3.2
	Build3_2:3.2
	Build3_1:3.2
	Build3:3.1
	Build2:2.1;
locks; strict;
comment	@ * @;


5.12
date	95.12.21.20.12.41;	author berczuk;	state Rel;
branches;
next	5.11;

5.11
date	95.12.07.20.29.51;	author berczuk;	state Rel;
branches;
next	5.10;

5.10
date	95.11.30.14.54.27;	author berczuk;	state Rel;
branches;
next	5.9;

5.9
date	95.11.09.20.57.32;	author berczuk;	state Rel;
branches;
next	5.8;

5.8
date	95.11.03.22.14.47;	author berczuk;	state Exp;
branches;
next	5.7;

5.7
date	95.11.03.19.08.31;	author berczuk;	state Exp;
branches;
next	5.6;

5.6
date	95.10.24.21.03.36;	author berczuk;	state Exp;
branches;
next	5.5;

5.5
date	95.10.24.18.05.51;	author berczuk;	state Exp;
branches;
next	5.4;

5.4
date	95.10.24.14.32.07;	author berczuk;	state Exp;
branches;
next	5.3;

5.3
date	95.10.13.19.43.35;	author berczuk;	state Exp;
branches;
next	5.2;

5.2
date	95.09.26.13.10.12;	author berczuk;	state Exp;
branches;
next	5.1;

5.1
date	95.09.18.20.53.44;	author berczuk;	state Exp;
branches;
next	4.1;

4.1
date	95.06.09.16.00.18;	author berczuk;	state Exp;
branches;
next	3.6;

3.6
date	95.06.09.15.55.46;	author berczuk;	state Exp;
branches;
next	3.5;

3.5
date	95.05.23.21.17.59;	author berczuk;	state Exp;
branches;
next	3.4;

3.4
date	95.01.10.20.22.18;	author berczuk;	state Exp;
branches;
next	3.3;

3.3
date	94.11.28.22.37.33;	author berczuk;	state Exp;
branches;
next	3.2;

3.2
date	94.06.30.13.54.36;	author berczuk;	state Exp;
branches;
next	3.1;

3.1
date	94.04.29.21.40.57;	author berczuk;	state Exp;
branches;
next	2.3;

2.3
date	94.04.29.21.35.39;	author berczuk;	state Exp;
branches;
next	2.2;

2.2
date	94.02.01.19.39.43;	author berczuk;	state Exp;
branches;
next	2.1;

2.1
date	93.09.20.16.21.22;	author berczuk;	state Release;
branches;
next	;


desc
@Class to hold ASM results
@


5.12
log
@added calibration version/occultation info.
@
text
@//   RCS Stamp: $Id: AsmSoln.h,v 5.11 1995/12/07 20:29:51 berczuk Rel berczuk $ 
//   Description : Asm Solution set for Quicklook analysis
//                 This class will conceptually contain an
//                 array laid out as follows:
//                 source name/ intensity soln/quality data
//   Author      : Steve Berczuk -berczuk@@mit.edu
//   Copyright   : Massachusetts Institute of Technology

#ifndef ASMSOLN_h
#define ASMSOLN_h
#include <rw/rwtime.h>
#include <rw/rwdate.h>
#include <rw/collstr.h>
#include <rw/tvordvec.h>
#include <rw/tvvector.h>

#include <SscIStructs.h>
#include <AsmDefs.h>
#include <PosHistParams.h>
#include <TooRecord.h>
#include "EdsResult.h"

class ostream;
class AsmSource;
class SOCTime2;
class ModelFitAnswer;
class AsmSourceList;

class Ssc;




// energy boundary represents the list of boundaries
struct EnergyBoundary
{
  int lower;
  int upper;
};

struct ChiSqVal // struct holding chi Sq parameters
{
  ChiSqVal(double chiSq=0, int dof=0)
    :theChiSq(chiSq),
  ndof(dof)
    {
    }
  double theChiSq;
  int ndof;
  // number of degrees of freedom
};



// Class AsmIntensRec is an abstract base class that models a generic single
// source solution to a fit. 
class AsmIntensRec 
{
    friend ostream& operator<<(ostream&, const AsmIntensRec&);
public:
    AsmIntensRec(const RWCString& name,
                 double intens,
                 double modelSum,
                 int dwellDuration,
                 double variance,
                 const ChiSqVal& chisq);
    
    virtual  AsmIntensRec* clone() =0;

    RWCString sourceName() const
    {
        return theSourceName;
    }
    
    
    virtual double sourceRAinDegrees() const =0;
    virtual double sourceDECinDegrees() const =0;

    virtual double offsetPhiInDegrees() const =0;
    virtual double offsetThetaInDegrees() const =0;
    virtual XteAngle earthAngle() const=0;
    virtual OccultFlags occultationFlags() const =0;
    
    double variance() const 
    {
        return theVariance;
    }
    
    double transmission() const
    {
        return theModelSum/areaFactor();
    }
    
    void setToo(const TooRecord& t)
    {
        tooRecord = t;
    }

    virtual int isBackground() const 
    {
        return 0;
        
    }
    
    TooRecord getTooRecord() const 
    {
        return tooRecord;
        
    }
    
    void setTooRecord(const TooRecord& t) 
    {
        tooRecord = t;
    }
    


    int isToo() const
    {
        return (int)tooRecord;
        
    }
    virtual double areaFactor() const =0;
    // return the areaFactor to divide by to convert the value in
    // counts/cm^2 to the value in counts

    double intensity() const;
    // returns the intensity in units of counts
    // the raw fit results (in cts/cm^2) is multiplied by the exposed detector
    // area and divided by exposure time
    
    double error() const
    {
        return (sqrt(theVariance)*areaFactor())/theExposureTime;
    }
    
    
    // returns the error normalized by area and exposure time
/*
    void setChiSq(ChiSqVal v)
    {
        theChiSqVal=v;
    }
*/
//    void setVariance(double var)=0;
    int  operator ==(const AsmIntensRec& a);
     int operator <(const AsmIntensRec& );
    virtual void display(ostream& os) const =0;
    virtual void displayFits(ostream& os) const;
    
protected:

    double theVariance;
    double theIntensity;
    int theExposureTime;
    double theModelSum;
    
    ChiSqVal theChiSq;
    RWCString theSourceName;
    TooRecord tooRecord;
    
};




// AsmSourceIntensity models a single SOURCE solution. A source has a position
// and in addition to the generic
// solution parameters.

class AsmSourceIntensity : public AsmIntensRec
{
public:
  AsmSourceIntensity();
    virtual  ~AsmSourceIntensity();
//  AsmSourceIntensity(RWCString, double intensity );
   AsmSourceIntensity(const RWCString& name, 
                      const SscOffset& offset,
                      // offset of src from center of SSC FOV 
                      const XteAngle& sourceRA,
                      const XteAngle& sourceDEC,
                      double intensity, 
                      double modelSum,
                      int dwellDuration,
                      ChiSqVal,
                      double variance,
                      const XteAngle& earthAngle,
                      const OccultFlags& o);
    virtual  AsmIntensRec* clone();

    
    virtual double sourceRAinDegrees() const
    {
        return sourceRA.degrees();
        
    }
    
    virtual double sourceDECinDegrees() const
    {
        return sourceDEC.degrees();
    }
    

    virtual double offsetThetaInDegrees() const
    {
        return theOffset.dtheta.degrees();
        
    }
    
    virtual double offsetPhiInDegrees() const
    {
        return theOffset.dphi.degrees();
    }
    

    double areaFactor() const
    {
        return  28.793;
    }
    
    XteAngle earthAngle() const
    {
        return theEarthAngle;
    }
    virtual OccultFlags occultationFlags() const 
    {
        return occultFlags;
    }
    
    
//    void setVariance(double var);
    int  operator ==(const AsmIntensRec& a);
    int operator <(const AsmIntensRec& ) {return 0;}
    virtual void display(ostream& os) const;
//    virtual void displayFits(ostream& os) const;
protected:
    int source_id;    
    SscOffset theOffset;
    
    XteAngle sourceRA;
    XteAngle sourceDEC;
    XteAngle theEarthAngle;
    OccultFlags occultFlags;

    XteAngle sunAngle;
    // angle between source and sun

    RWTime theObsTime;
    // Time of observation
    
    RWTValVector<EnergyBoundary> eChannels;
    // list of energy Channels, the number will be known at construction time
    
    RWTime theAnalysisTime;
    //Time this record was calculated
    // perhaps a TOO flag of some sort should be here, or a record
    // indicating ownership based on catalog?
    

};



// AsmBackground Intensity models backround solutions for which
// RA and dec of source do not make sense
class AsmBackgroundIntensity: public AsmIntensRec
{

public:
    AsmBackgroundIntensity(RWCString name,
                           double intensity, 
                           double modelSum,
                           int dwellDuration,
                           ChiSqVal=ChiSqVal(),
                           double variance =0.0);
    virtual  AsmIntensRec* clone();

    virtual ~AsmBackgroundIntensity() 
    {
    }
    
    virtual double sourceRAinDegrees() const
    {
        return -999;
        
        
    }
    XteAngle earthAngle() const
    {
        return XteAngle(0);
        // for background display 0 degrees by convention
    }
    virtual OccultFlags occultationFlags() const 
    {
        return OccultFlags();
        
    }
        
    int isBackground() const 
    {
        return 1;
        
    }
    double areaFactor() const;
    

    virtual double sourceDECinDegrees() const
    {
        return -999;
        
    }
    virtual double offsetPhiInDegrees() const {return 0;};
    virtual double offsetThetaInDegrees() const {return 0;
    }
    

    
    virtual void display(ostream& os) const ;
//    virtual void displayFits(ostream& os) const ;
};



/*inline void AsmSourceIntensity::setVariance(double var)
{
  theVariance=var;
}
*/

// AsmFitResult provides an interface to creating AsmIntensRecs of the 
// appropriate type (source or background). Using this wrapper class allows
// the use of a stack based list of intensities, whicle at the same time 
// providing for virtual behaviour of different solutions classes.
class AsmFitResult
{
    friend ostream& operator<<(ostream&, const AsmFitResult&);
public:
    AsmFitResult();
    
    AsmFitResult(const RWCString&, 
                 const SscOffset& offset,
                 const XteAngle& sourceRA,
                 const XteAngle& sourceDEC,
                 double intensity, 
                 double modelSum,
                 int dwellDuration,
                 ChiSqVal,
                 double variance, const XteAngle& earthAngle,
                 const OccultFlags& occult);
    // non background source
    AsmFitResult(int dwellDuration, 
                 const ModelFitAnswer& ans,
                 int answerIndex);
    // create a Fit result from an intermediate result

    AsmFitResult(RWCString,
                 double intensity, 
                 double modelSum,
                 int dwellDuration,
                 ChiSqVal=ChiSqVal(),
                 double variance=0.0);

    AsmFitResult(const AsmFitResult&);
    
    ~AsmFitResult();
    
    AsmFitResult& operator=(const AsmFitResult& that);
    
// background result: no location
    RWCString sourceName() const
    {
        return theFit->sourceName();
    }
    
    
    int checkForToo(const AsmSource* sourceToCheck);
    // check if the result is a limit too, given the min and 
    // max limits in the catalog.
    // This function takes into account any thresholding wrt
    // a limit being within certain limits

    int isToo() const
    {
        return theFit->isToo();
    }

    int isBackground() const
    {
        return theFit->isBackground();
        
    }
    
    TooRecord getTooRecord() const
    {
        return theFit->getTooRecord();
    }
    
    void setToo(const TooRecord& t)
    {
        theFit->setToo(t);
    }
    
    virtual double sourceRAinDegrees() const
    {
        return theFit->sourceRAinDegrees();
    }
    
    virtual double sourceDECinDegrees() const
    {
        return theFit->sourceDECinDegrees();
    }

    XteAngle earthAngle() const
    {
        return theFit->earthAngle();
    }
    
    double variance() const 
    {
        return theFit->variance();
    }

    double error() const 
    {
        return theFit->error();
    }

    
    double intensity() const
    {
        return theFit->intensity();
    }

    ChiSqVal chiSquare();
    // to be implemented

    int  operator ==(const AsmFitResult& a)
    {
        return (*theFit == *(a.theFit));
    }
    
    int operator <(const AsmFitResult& ) {return 0;}
    void display(ostream& os) const;
    void displayFits(ostream& os) const;
    
private:
    AsmIntensRec* theFit;

    
};


struct SscInfo
{
    SscInfo(const Ssc& ssc);
    SscInfo()
    {
    }
    
    SSC_ID theId;
    SscPointing thePointing;
        
};

struct DwellInfo
{
    DwellInfo()
    {
    }
    DwellInfo(const SscDwellParams& dp, 
              const XteAngle& asmAngle,
              const RWTValVector<SscInfo>& sscs,
              int dwellSeq, 
              int dwellNum, 
              double dwellStart
            )
        :theDwellParams(dp), 
         theSscs(sscs), 
         theAsmRotation(asmAngle),
         theDwellNumber(dwellNum),
         theDwellSequence(dwellSeq),
         theDwellStartTime(dwellStart),
         theConfigID(dp.configurationID())
    {
    }
    DwellInfo(const SscDwellParams& dp, const XteAngle& asmAngle,
              const SscInfo& ssc, int dwellSeq, int dwellNum, 
              double dwellStart);
    
    // special case: only 1 ssc/dwell as for quick look
    
    SscDwellParams theDwellParams;
    RWTValVector<SscInfo> theSscs;
    XteAngle theAsmRotation;
    // the rotation of the ASM for this dwell
    int theDwellNumber;
    int theDwellSequence;
    double theDwellStartTime;
    int theConfigID; // theConfiguration ID
    
};



// AsmSoln contains the <N> solutions from an single SSC source fit
// It contains the parameters which will vary from dwell to dwell as well
// as the list of individual solved intensities.
class AsmSoln :public EdsResult
{
friend ostream& operator<<(ostream& os, const AsmSoln&);
public:
    AsmSoln (const DwellInfo&);
    // special case: data from a 1 ssc dwell (as in quick look)
    
    AsmSoln(const RWTValVector<DwellInfo>&);
    
    
    AsmSoln() ;
    
    ~AsmSoln();
    void addEntry(const AsmFitResult& );
    void addAnswers(const ModelFitAnswer&, 
                    int dwellDurationInSeconds);
    // add a set of answers to the solution

    int entries() const
    {
        return theSolutions.entries();
    }

    void setCalibrationVersion(int v) 
    {
        theCalibrationsVersion=v;
    }
    
    void setCreator(const char* creator) 
    {
        theCreator=creator;
    }
    void setOrigin(const char* creator) 
    {
        theOrigin=creator;
    }
    
    int numSources() const;
    int numUpperToos() const;
    int numLowerToos() const;
    
    SOCTime2 firstDwellStartTime() const;
    SOCTime2 lastDwellEndTime() const;
    

    //does not include background
    
    AsmFitResult  entry(int i) const
    {
        return theSolutions[i];
    }
   int indexOfSolution(const RWCString& name) const;
    // return the index of the solution for a source in the fit by name
    // return RW_NPOS if they source does not exist
    // will return the index of the FIRST object in the list that
    // has this name
        
    double startTime() const 
    {
        return 0;
        
            //theDwellStartTime;
    }
    // return the start time of the first Dwell in the solution

    double dwellEnd() const 
    {
        return 0;
        
            //theDwellStartTime + theDwellParams.dwellDurationInSeconds();
    }
    // return the end of the last dwell for which we have a solution

    
    void setChiSq(ChiSqVal v)
    {
        theChiSqVal=v;
    }
    
    double redChiSq() const
    {
        return theChiSqVal.theChiSq/theChiSqVal.ndof;
        
    }
    void setNremove(int remove)
    {
        theRemoveCount =remove;
    }
    
    int ndof() const
    {
        return theChiSqVal.ndof;
    }
    

    void display(ostream& os,SSC_ID ssc) const;
    void display(ostream& ) const
    {
    }
    
    
    
    void displayFits(ostream& os) const;
    
private:
    int theCalibrationsVersion;
    // what version of calibrations were used for this
    // solution/fit

    RWCString theCreator;
    RWCString theOrigin;
    ChiSqVal theChiSqVal;
    // the chi sq parameters for this fit
    RWTValOrderedVector<AsmFitResult> theSolutions;
    int theRemoveCount;
    
    RWTime analysisTime;
    RWTValVector<DwellInfo> theDwells;
};



#endif
@


5.11
log
@added switches to set origin and creator of solutions.
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: AsmSoln.h,v 5.10 1995/11/30 14:54:27 berczuk Rel berczuk $ 
d81 3
a83 1

d149 1
a149 1
    virtual void displayFits(ostream& os) const =0;
d187 2
a188 1
                      const XteAngle& earthAngle);
a219 1

d221 10
d235 1
a235 1
    virtual void displayFits(ostream& os) const;
d243 1
a243 2
    
  // the calculated intensity
d245 3
d288 11
a298 1
    
d312 2
a313 2
    virtual double offsetPhiInDegrees() const {return -999;};
    virtual double offsetThetaInDegrees() const {return -999;
d319 1
a319 1
    virtual void displayFits(ostream& os) const ;
d348 2
a349 1
                 double variance, const XteAngle& earthAngle);
d412 5
d517 1
a517 1

d531 5
d613 4
@


5.10
log
@added addAnswers() method
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: AsmSoln.h,v 5.9 1995/11/09 20:57:32 berczuk Rel berczuk $ 
d183 3
a185 2
                      ChiSqVal=ChiSqVal(),
                      double variance=0.0);
d230 2
a231 1

d323 2
a324 2
                 ChiSqVal=ChiSqVal(),
                 double variance=0.0);
d452 2
a453 1
         theDwellStartTime(dwellStart)
d469 2
d501 9
a509 1

d578 2
@


5.9
log
@added methods for extending solution model to handle toos
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: AsmSoln.h,v 5.8 1995/11/03 22:14:47 berczuk Exp berczuk $ 
d55 1
a55 1
// Class AsmIntensRes is an abstract base class that models a generic single
a74 4
    virtual int sourceID() const 
    {
        return -1;
    }
d78 4
d166 1
a166 1
// and a source_id (a key to the source database) in addition to the generic
d176 2
a177 1
                      int sourceID,
a187 1
    
d199 2
a200 1
    int sourceID() const 
d202 2
a203 1
        return source_id;
d205 6
d225 2
d286 4
d291 1
d315 1
a315 1
                 int sourceID,
d324 5
d335 1
d337 1
a347 4
    int sourceID() const 
    {
        return theFit->sourceID();
    }
d488 1
a488 2
                    int dwellDurationInSeconds,
                    const AsmSourceList* cat=0);
@


5.8
log
@added a display method to keep quick look displays working for
now. hacked in a dummy display() to match virtual method of base
class.
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: AsmSoln.h,v 5.7 1995/11/03 19:08:31 berczuk Exp berczuk $ 
d26 2
d373 5
d469 5
d494 6
a499 1
    
@


5.7
log
@added error() and transmission() methods.
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: AsmSoln.h,v 5.6 1995/10/24 21:03:36 berczuk Exp berczuk $ 
d521 5
a525 1
    void display(ostream& os) const;
@


5.6
log
@removed extra constructor. Renamed DwellParams class
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: AsmSoln.h,v 5.5 1995/10/24 18:05:51 berczuk Exp berczuk $ 
d61 1
d84 6
a89 1

d128 7
a134 1

d152 2
d178 1
d244 1
d302 1
d309 1
d510 4
d530 2
a531 1

@


5.5
log
@changed ndof() to return an int rather than a double.
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: AsmSoln.h,v 5.4 1995/10/24 14:32:07 berczuk Exp berczuk $ 
d395 1
a395 1
    DwellInfo(const DwellParams& dp, 
d410 1
a410 1
    DwellInfo(const DwellParams& dp, const XteAngle& asmAngle,
d416 1
a416 1
    DwellParams theDwellParams;
a433 9

    AsmSoln(SSC_ID id, 
            const SkyPt& sscPointing,
            const XteAngle& sscRoll,
            int dwellSeq, 
            int dwellNum, 
            double dwellStartTime,
            const DwellParams& dp);

@


5.4
log
@checkpoint before renaming HistDwell class
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: AsmSoln.h,v 5.3 1995/10/13 19:43:35 berczuk Exp berczuk $ 
d502 1
a502 1
    double ndof() const
@


5.3
log
@fixed model based on test with RR
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: AsmSoln.h,v 5.2 1995/09/26 13:10:12 berczuk Exp berczuk $ 
d25 2
d114 2
a115 1
    // return the areaFactor to divide by to get the value in counts
d189 1
a189 1
        return 31.8457;
d460 5
@


5.2
log
@added modeling of window bars.
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: AsmSoln.h,v 5.1 1995/09/18 20:53:44 berczuk Exp berczuk $ 
a10 3



d17 1
a17 2
#include <SkyPt.h>
#include <XteAngle.h>
d25 1
d29 1
d375 11
d387 34
d422 2
d432 1
a432 1
    AsmSoln(SSC_ID id, int exposure_in_secs,
d435 3
a437 2
            int numPosBins,
            int dwellSeq, int dwellNum, double dwellStartTime,
d439 7
d466 1
a466 1
    void setDwellParams(const DwellParams& p)
d468 3
a470 1
        theDwellParams=p;
d472 1
a472 2
    
    // set the parameter set.
d474 1
a474 1
    SkyPt sscPointing() const 
d476 3
a478 1
        return sscPointingDir;
d480 1
d482 2
a483 1
    double dwellStart() const 
d485 1
a485 1
        return theDwellStartTime;
a486 5

    double dwellEnd() const 
    {
        return theDwellStartTime + exposure;
    }
d488 1
a488 1
    XteAngle sscRoll() const
d490 2
a491 1
        return sscRollAngle;
d494 1
a494 1
    void setChiSq(ChiSqVal v)
d496 1
a496 1
        theChiSqVal=v;
d499 1
a508 1
    SSC_ID sscId;
a509 4
    SkyPt sscPointingDir;
    XteAngle sscRollAngle;

    int exposure;
d511 1
a511 28
    int numPositionBins;
    int theDwellNumber;
    int theDwellSequence;
    double theDwellStartTime;
    DwellParams theDwellParams;    
};


// PosHist result contains the solutions from a partition (3 dwells, one
// from each SSC). Each PosHistResult in a data set is distinguished by the
// Dwell Sequence ID.
class PosHistResult: public EdsResult
{
public:
    PosHistResult(const AsmSoln& sol1,
                  const AsmSoln& sol2,
                  const AsmSoln& sol3,
                  int dwellSeqId,
                  int met_start,
                  const PosHistParams& p);
    void display(ostream& os) const;
    void displayFits(ostream& os)const ;
private:
    AsmSoln theSols[3];
    int dwellSequenceID;
    int startMET;
    RWTime analysisTime;
    PosHistParams paramSet;
@


5.1
log
@new asm model/fits
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: AsmSoln.h,v 4.1 1995/06/09 16:00:18 berczuk Exp berczuk $ 
a11 1
class ostream;
d27 2
d83 1
d88 1
a88 1
    
d310 6
d351 1
@


4.1
log
@bump to Rev 4
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: AsmSoln.h,v 3.6 1995/06/09 15:55:46 berczuk Exp berczuk $ 
d60 1
d111 7
a119 2
    double intensity() const
    {return theIntensity;}
d136 1
d160 3
a162 1
                      double intensity, ChiSqVal=ChiSqVal(),
d184 4
d226 1
d247 1
d249 1
d255 1
d283 1
d288 3
a290 1
                 double intensity, ChiSqVal=ChiSqVal(),
d382 2
a383 2
            int dwellSeq, int dwellNum, double dwellStartTime
            );
@


3.6
log
@checkpoint before changing model
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: AsmSoln.h,v 3.5 1995/05/23 21:17:59 berczuk Exp berczuk $ 
@


3.5
log
@added Too checking and started fits output
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: AsmSoln.h,v 3.4 1995/01/10 20:22:18 berczuk Exp berczuk $ 
d85 7
d228 6
d265 2
a266 1
                 double intensity, ChiSqVal=ChiSqVal(),
a272 1
    
d293 6
d343 1
d373 2
d376 3
d384 7
d396 10
a420 2

    
d436 1
a436 1

d458 1
@


3.4
log
@Baseline before adding more Too Analysis
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: AsmSoln.h,v 3.3 1994/11/28 22:37:33 berczuk Exp berczuk $ 
d14 1
d16 1
d21 2
d24 2
d29 2
a31 1

d52 3
a54 6

// An AsmFittedIntensity is an entry in the Soloution that contains
// the results of a particular fit
// this class will be replaced by AsmIntensRec
// 
class AsmFittedIntensity 
d56 1
a56 1
  friend ostream& operator<<(ostream&, const AsmFittedIntensity&);
d58 6
a63 4
  AsmFittedIntensity();
  ~AsmFittedIntensity();
//  AsmFittedIntensity(RWCString, double intensity );
   AsmFittedIntensity(RWCString, double intensity, ChiSqVal=ChiSqVal() );
d70 7
d81 9
d91 14
a104 1
  
d107 11
d119 1
d121 7
a127 2
    ChiSqVal chiSquare();
    // to be implemented
d129 46
a174 3
  void setVariance(double var);
  int  operator ==(const AsmFittedIntensity& a);
  int operator <(const AsmFittedIntensity& ) {return 0;}
d176 4
a179 3
  RWCollectableString theSourceName;
    // name of the source
  double theIntensity;
d182 11
a192 4
  ChiSqVal theChiSq;
  double theVariance;
   RWTime theObsTime;
  // Time of observation
a193 7
  RWTValVector<EnergyBoundary> eChannels;
  // list of energy Channels, the number will be known at construction time
  
  RWTime theAnalysisTime;
  //Time this record was calculated
 // perhaps a TOO flag of some sort should be here, or a record
    // indicating ownership based on catalog?
d196 5
a200 1
inline void AsmFittedIntensity::setVariance(double var)
d202 32
d236 1
d238 41
a278 1
// AsmSoln contains the solutions from an source fit
d280 29
d310 2
d313 20
d337 9
a345 1
    AsmSoln(SSC_ID id=SSC1);
d347 1
a347 1
    void addEntry(AsmFittedIntensity);
d352 3
a354 1
    AsmFittedIntensity  entry(int i) const
d359 10
a372 2
    void display(ostream& os) 
    {os << *this;}
d374 7
d384 1
a384 1
    RWTValOrderedVector<AsmFittedIntensity> theSolutions;
d386 11
a396 1
    
d399 4
d408 6
a413 2
                  const AsmSoln& sol3);
    void display(ostream& os);
d416 3
@


3.3
log
@Checkpoint for Build 4.0
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: AsmSoln.h,v 3.2 1994/06/30 13:54:36 berczuk Exp berczuk $ 
d59 4
a62 4
  double variance();
  // to be inplemented

    double intensity() {return theIntensity;}
d64 9
d74 2
a75 2
  ChiSqVal chiSquare();
  // to be implemented
d81 2
a82 1
  RWCollectableString sourceName;// cstring or collectable?
d96 2
a97 1
  
d116 9
d129 2
a130 1
    void display(ostream& os){os << *this;}
@


3.2
log
@Build 3_1 intermediate release
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: AsmSoln.h,v 3.1 1994/04/29 21:40:57 berczuk Exp berczuk $ 
d19 4
d80 1
a80 1
  
a94 4
// we declare a generic vector class and subclass to add
// our own signatures
//declare(GVector,AsmFittedIntensity)
//declare(GOrderedVector,AsmFittedIntensity)     
d96 3
a98 2
//class AsmSoln :public GOrderedVector(AsmFittedIntensity)
class AsmSoln :public RWTValOrderedVector<AsmFittedIntensity>
d102 3
a104 3
  AsmSoln();
  ~AsmSoln();
  void addEntry(AsmFittedIntensity);
d106 5
a110 1
    {theChiSqVal=v;}
d112 1
a112 1
   ChiSqVal theChiSqVal;
d114 4
d119 9
d129 1
@


3.1
log
@Build3 Initial Release
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: AsmSoln.h,v 2.3 1994/04/29 21:35:39 berczuk Exp berczuk $ 
d104 5
a108 1
  
@


2.3
log
@*** empty log message ***
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: AsmSoln.h,v 2.2 1994/02/01 19:39:43 berczuk Exp berczuk $ 
@


2.2
log
@added extra fit parameters
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: AsmSoln.h,v 2.1 1993/09/20 16:21:22 berczuk Release berczuk $ 
d57 3
@


2.1
log
@initial revsion: basic functions
@
text
@d1 1
a1 1
//   RCS Stamp: $Id$ 
d14 1
d16 2
a17 1
//#include <rw/gordvec.h>
d19 23
a41 1
#include <rw/tvordvec.h>
d44 2
d52 10
a61 1
  AsmFittedIntensity(RWCString, double intensity );
d66 1
a66 1
  double intensity;
d68 5
d74 6
d82 4
@

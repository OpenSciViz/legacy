head	1.2;
access
	soccm
	nessus
	gromeo;
symbols;
locks; strict;
comment	@ * @;


1.2
date	94.02.17.15.30.54;	author ehm;	state Exp;
branches;
next	1.1;

1.1
date	94.01.25.12.39.40;	author ehm;	state Exp;
branches;
next	;


desc
@realtime client for the calibration portion
of standard configuration 1.
@


1.2
log
@added a comment
@
text
@\//
//   RCS Stamp: $Id: std1Calibration.C,v 1.1 1994/01/25 12:39:40 ehm Exp ehm $ 
//   Description : Real time client for Standard 1 calibration data
//   Author      : Edward Morgan - ehm@@space.mit.edu
//   Copyright   : Massachusetts Institute of Technology

#include <iostream.h>
#include <new.h>
#include <signal.h>
#include <stdarg.h>
#include <stdlib.h>
#include <unistd.h>

#include "science_if.h"
#include "EARun.h"
#include "EdsPartition.h"
#include "PktPartHdr.h"
#include "PktPartData.h"
#include "ObsParamBlock.h"
#include "PartMap.h"
#include "PktTlm.h"
#include "SCDataCache.h"
#include "Utilities.h"
#include "rtclient.h"  /* Entry points into librtclient.a */


//
// To work around a template bug.
//
#include "Descriptor.h"


//
// The port of the Realtime Server to which we connect to get telemetry.
//
static short server_port;


//
// The machine on which the Realtime Server is executing.
//
static const char* server_host;


//
// Should we run as a daemon?
//
static int run_as_daemon;


//
// Should we read packets from STDIN instead of from the Realtime Server?
//
static int read_from_stdin;


//
// This is the name we advertise to the Realtime Server.  It is the same as
// the entry in the relevant $SOCHOME/etc/RT.CLIENTS table.  You will probably
// want to hardcode the standard name for your client here, instead of relying
// on command line arguments and parse_arguments() to set it.
//
static const char* client_name;


//
// Our name -- set in main().  Not necessarily the same as the name we
// advertise to the Realtime Server.  This is used for reporting of errors
// and messages.
//
static char* ProgName;


//
// The Spacecraft Cache object.  Query this object to get whatever S/C data
// you've requested to be cached.  This is instantiated and maintained by
// the librtclient.a code.
//
extern SCDataCache SC;



#include "CluArray.h"
static CluArray<int> x4;
static CluArray<char> x5;
static CluArray<RangeList*> x6;
static CluArray<Descriptor> x7;

static ObsParamBlockPtr config;

// handler for a full partition (Quick Look)
 void handlePart(const EdsPartition* thePart)
{
  unsigned short *unpackedData;
  int j, pcuid, lld;
  // get the configuration from the partition's config ID
  unsigned long configId = thePart->getConfigurationID();

//This ugly line of code slows things down, but enables
// mixing of printf's with couts. When the printf's are
// removed, this line should go too.
  ios::sync_with_stdio();

  if (configId == 0x2000004)
  {
    cout << "Start Time: " << thePart->obsStartTime() + 128 * thePart->partNumber()<< endl;
    unpackedData = thePart->getData();

/////////////////////////////////////////////////////////
//
//  The calibration data starts at location 8192, and is a
//  5 x 6 x 256 array. 5 PCUs 6 LLDs and 256 Pulse Height channels.
//
////////////////////////////////////////////////////////

    for (j=0; j < 256 ; j++)
    {
      printf("%3d ", j);
      for (pcuid=0; pcuid < 5 ; pcuid++)
	for (lld=0; lld < 6 ; lld++)
	  printf("%4d ",unpackedData[8192 + j + lld * 256 + 1536 * pcuid]);
      printf("\n");
    }
    fflush(stdout);
    delete  unpackedData;
  }
}

//
// This is the routine that does the packet-specific processing.  Do not
// change the signature of this function.  Each time one of the packets in
// which you've specified an interest is received by the realtime client
// process, this function is called -- this includes any packets containing
// S/C data in which you've indicated an interest in receiving, as well.
// The caching of S/C data is done for you in the librtclient.a code, but only
// if you specify that you want it cached.  You can specify that you want
// to get S/C data from the Realtime Server which you must then choose whether
// you want to cache it or not.  You really have three options when you're
// receiving spacecraft data:
//
//   1) cache it and ignore the S/C packets themselves;
//   2) just deal with the raw S/C packets;
//   3) both (1) & (2).
//
// For non-spacecraft data you must deal directly with the packets.
//

void process_a_packet (const PktTlm* pkt)
{
  if ((((PktEds *)pkt)->streamID() ==  6) || (((PktEds *)pkt)->streamID() ==  7))
    {
      pkt->apply();
    }
}


//
// Put any specific initialization stuff you need into here.  This is the place
// to specify the types of S/C data to be cached.  Do not change the signature
// of this function.
//

static void client_specific_initialization (void)
{
    //
    // Enable each of the types of spacecraft data in which you're interested.
    //
//    SC.enableCaching(SCData::ATTITUDE);
//    SC.enableCaching(SCData::POSITION);

    //
    // Only the PCA people seem interested in these.
    //
//    SC.enableCaching(SCData::PCU_TEMP_A);
//    SC.enableCaching(SCData::PCU_TEMP_B);

    //
    // Put your code here.
    //
  config = ObsParamBlock::restore(0x02000005);
  EdsPartition::setDisposition(handlePart);
}


//
// Print out usage string and exit.  If you change parse_arguments(),
// make the relevant changes here as well.
//

static void usage (void)
{
    cerr << "\nusage: " << ProgName
         << " [-daemonize]"
#ifdef XTEST
         << " -display-client name"
#endif
         << " -host hostname"
         << " -name client-name"
         << " [-port #]"
         << " [-read-from-stdin]\n\n";
    exit(1);
}


//
// Parse our arguments.  Feel free to add your own client-specific options.
// If you do so, make sure to update usage() appropriately.
//

static void parse_arguments (char**& argv)
{
    //
    // Parse any arguments.
    //
    //    -daemonize           -- run as a daemon
    //    -display-client name -- name of X client program to execute
    //    -host machine        -- realtime server machine
    //    -port #              -- port number to connect torealtime server
    //    -name name           -- the name we advertise to the realtime server
    //    -read-from-stdin     -- read from stdin instead of over a socket
    //
    while (*++argv && **argv == '-')
    {
        if (strcmp(*argv, "-host") ==  0)
        {
            if (!(server_host = *++argv))
                error("No hostname supplied.");
        }
        else if (strcmp(*argv, "-port") == 0)
        {
            if (*++argv)
            {
                if ((server_port = (short) atoi(*argv)) <= 0)
                    error("Port # must be positive short.");
                continue;
            }
            else
                error("No port # supplied.");
        }
        else if (strcmp(*argv, "-daemonize") == 0)
            run_as_daemon = 1;
#ifdef XTEST
        else if (strcmp(*argv, "-display-client") ==  0)
        {
            if (!(display_client = *++argv))
                error("No display client name supplied.");
        }
#endif
        else if (strcmp(*argv, "-name") ==  0)
        {
            if (!(client_name = *++argv))
                error("No name supplied.");
        }
        else if (strcmp(*argv, "-read-from-stdin") == 0)
            read_from_stdin = 1;
        else
        {
            message("`%s' is not a valid option, exiting ...", *argv);
            usage();
        }
    }

    if (read_from_stdin && run_as_daemon)
    {
        run_as_daemon = 0;
        message("Using `-read-from-stdin' implies ! `-daemonize'.");

    }

    if (!server_host && !read_from_stdin)
    {
        message("`realtime-server-host' or `read-from-stdin' must be specified.");
        usage();
    }

    if (!read_from_stdin && !client_name)
        usage();

    //
    // Check that basic environment variables are set.
    //
    if (getenv("SOCHOME") == 0)
        error("The `SOCHOME' environment variable must be set.");

    if (getenv("SOCOPS") == 0)
        error("The `SOCOPS' environment variable must be set.");

#ifdef XTEST
    if (!display_client)
        error("No display client name supplied.");

    if (display_client[0] != '/')
    {
        //
        // Try to build a full pathname.
        //
        const char* arch    = getenv("ARCH");
        const char* sochome = getenv("SOCHOME");

        char* pathname = new char[strlen(sochome) + strlen(display_client)+
                                  (arch == 0 ? 0 : strlen(arch)) + 7];

        (void) strcpy(pathname, sochome);
        (void) strcat(pathname, "/bin/");
        if (arch)
        {
            (void) strcat(pathname, arch);
            (void) strcat(pathname, "/");
        }
        (void) strcat(pathname, display_client);

        if (!is_regular_file(pathname))
            error("`%s' isn't a regular file.", pathname);

        display_client = pathname;
    }
#endif
}


//
// Simple error handler for use by PktCCSDS.  Change this if you want
// to do something fancier.
//

static void packet_error_handler (const char* msg) { error(msg); }


//
// A simple Out-Of-Memory Exception handler for main().  Feel free
// to change this if you really can do better than exit.
//

static void free_store_exception (void)
{
#ifdef ATEXIT
    error("Out of memory, exiting ...");
#else
    message("Out of memory, exiting ...");
    if (!read_from_stdin)
        force_exit_of_other_half();
    exit(1);
#endif
}


//
// The main routine.  Please do not change this without very good reason.
// client_specific_initialization() is placed such that you should be able
// to override many of my assumptions if you so choose.
//

int main (int, char* argv[])
{
    ProgName = argv[0];

    set_new_handler(free_store_exception);

    PktCCSDS::setErrorHandler(packet_error_handler);

    initialize_signal_handlers(catch_a_signal);

    parse_arguments(argv);

    if (run_as_daemon)
        //
        // Feel free to daemonize to someplace other than the filesystem
        // root if it makes sense for your application.
        //
        daemonize(client_name, "/");

#ifdef ATEXIT
    if (!read_from_stdin)
        if (atexit(force_exit_of_other_half))
            error("File %s, line %d, atexit() failed.",
                  __FILE__, __LINE__);
#endif

    if (read_from_stdin)
    {
        //
        // Do client-specific setup.
        //
        client_specific_initialization();
        read_packets_from_stdin();
    }
    else
    {
        //
        // Now fork to create a child process. The parent process
        // (aka back-end) reads packets from the Realtime Server and
        // places them in the ring buffer.  The child process
        // (aka front-end) reads these packets and does the appropriate
        // processing.
        //
        if ((PID = fork()) == 0)
        {
            //
            // This code is only executed by the front-end portion of the
            // realtime client; that part that is actually interested in 
            // the realtime analysis/display of the data in the packets.
            //
            PPID = getppid();
            //
            // Do client-specific setup.
            //
            client_specific_initialization();
            //
            // The primitive event mechanism in process_packets() isn't
            // compatible with the X Event mechanism.  If you want to drive
            // X Windows directly from this code, instead of fork()ing and
            // exec()ing a standalone X Windows process to which you write
            // the required data, please talk to me so we can work out the
            // best way to do it, together.  I have some ideas.  I just
            // haven't felt like putting in the time, if it wasn't clear
            // that it was going to be used.
            //
            process_packets();
        }
        else
            //
            // This code is only executed by the back-end of the realtime
            // client.  It is the code that maintains the connection to the
            // realtime server and gives well-built CCSDS Telemetry packets
            // to the front-end of the realtime client.
            //
            connect_to_realtime_server(client_name, server_port, server_host);
    }

    return 0;
}
@


1.1
log
@Initial revision
@
text
@d1 2
a2 2
//
//   RCS Stamp: $Id: std1Calibration.C,v 1.1 1994/01/24 22:59:37 ehm Exp ehm $ 
d108 8
@

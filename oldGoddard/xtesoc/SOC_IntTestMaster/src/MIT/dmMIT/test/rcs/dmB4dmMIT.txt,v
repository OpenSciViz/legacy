head	1.4;
access
	soccm
	nessus;
symbols
	CodeFreeze_5_95:1.4
	Build4_3:1.4
	Build4_2:1.4
	Build4_1:1.4
	Build4:1.4
	Build3_3:1.2
	Build3_2:1.2
	Build3_1:1.2
	Build3:1.1;
locks
	berczuk:1.4; strict;
comment	@# @;


1.4
date	94.12.15.19.54.06;	author berczuk;	state Exp;
branches;
next	1.3;

1.3
date	94.12.14.20.55.32;	author berczuk;	state Exp;
branches;
next	1.2;

1.2
date	94.06.30.13.58.41;	author berczuk;	state Exp;
branches;
next	1.1;

1.1
date	94.03.28.18.42.16;	author berczuk;	state Exp;
branches;
next	;


desc
@Data Management Test Plan
@


1.4
log
@added guidelines for running generic test.
@
text
@$Id: dmB4dmMIT.txt,v 1.3 1994/12/14 20:55:32 berczuk Exp berczuk $
Subsystem: Data Management
Author: Steve Berczuk                           
Site: MIT
Test Name: Data Management Tests
Test Goals: 
        - verify that EDS Accessors are accessed correctly.
        - verify that Datasets can be saved and restored.

Test Description: 
(for details read the comments in the source code for each test.)
The test is started by a script which executes programs and compares
the results to the correct data.
       1) Ingest a packet file. When a partition is accumulated, an
Accessor is created and a number of descriptor queries are sent to it.
        2) When a EARun is accumulated, other access tests are run.
        3) A dataset is created from the EARun, and then it is
restored, and compared.

External Elements Required
The following programs should be build and be installed in
$SOCHOME/test or $SOCHOME/bin
        - accessTest
        
A Packet file is required to be in $SOCHOME/test/inputs

TEST PROCEDURE:
- type testDmMIT.csh at the shell prompt

-(optional) If you wish to run a consistency check against a generic
packet file the give the shell script an argument. This will run the
accessor tests, outputting ERROR messages if there is an
inconsistency, but will not compare the results with the baseline.
To execute: type testDmMIT.csh <file name> at the prompt

VERIFICATION OF RESULTS
- testDmMIT will issue error messages in the event of an error.
- if running the baseline test a diff showing any differences between
the expected and actual output will be produced.
@


1.3
log
@update for new format required by build 4
@
text
@d1 1
a1 1
$Id$
d28 1
a28 1
- type testDmMIT at the shell prompt
d30 6
d38 2
@


1.2
log
@build 3_1 release
@
text
@d1 8
a8 5
	      Test Plan for MIT Data Management Software
		   MIT Document Number 64-70204.01
			by Stephen P. Berczuk
		    MIT Center for Space Research
			   berczuk@@mit.edu
d10 9
a18 1
RCS Revision: $Revision$
d20 6
a25 6
INTRODUCTION, AUDIENCE, & SCOPE
This document describes the tests to be performed to validate the MIT
Data Management Software. The tests in this document will fulfill the
following roles:
 - Installation verification
 - Requirement verification
d27 2
a28 6
The audience for this document includes:
 - Software Engineers who wish to perform regression testing after a
change has been made.
 - Quality Assurance Personell who wish to perform verification
testing.
 - Management who wish to inspect the reqirements vaildation process.
d30 2
a31 59
This document is meant to serve as a user manual for the tests and the
meanings of the requirements are not discussed in detail. Each test
case will contain the following componets:
 - Name: The name of the test, which will provide a key to the
makefile target to be used to execute the test.
 - Short Description: a brief description of the test.
 - Requirements Validated: a brief listing of the requirements that
the test validates.
 - Inputs, outputs, and interpretation of results: A description of
the components of the test procedure.

Each test target performs the following basic steps:
 - builds a test program
 - executes a test 
 - compares the test output to "correct" test output
 - reports on differences if any.
Any deviations from this basic plan will be discussed in the
individual tests.

The user is referred to the Makefile for detailed information about
the test execution procedure, and to the test source file for details
of the test.

** NOTE**
All of these tests assume that the following canonical packet files
are accessible and present in the directory pointed to by the Makefile
variable TEST_DATA_DIR. TEST_DATA_DIR defaults to $SOCOPS/.sampleData
 
 - Name: accessTest
 - Short Description: tests basic data management / access functions
 - Requirements tested: 
	* saving of an EARun to a dataset and retreiving it.
	* accessing RAW data.
	* accessing non-mode specific descriptors.
	* accessing data from standard PCA descriptors (Standard 1 & 2).
 - Inputs, outputs, and interpretation of results: 
The inputs to this test will be:
	- a standard packet file
	- a baseline input file
	- a configuration file
The output will be 2 parts : 
	- the output from processing the packets
	-  a status message reporting the success or failure of the tests.
 - Detail Description:
 Packets are processed until a partition is assembled. When a full
partition is obtained, an accessor is constructed and then all
standard descriptors are requested. The following descriptors are
accessed:
DATAMODE,UNPACKED_LENGTH,TIME,ALL,SPILLAGE,MODESPECIFIC

when a full EARun is assembled, a data set is created and is saved. 

----- cut case here:  test template--
 - Name: accessTest
 - Short Description: 
 - Requirements Validated: 
 - Inputs, outputs, and interpretation of results: 


@


1.1
log
@Initial revision
@
text
@d7 1
a7 1
RCS Revision: $Rev$
d43 2
a44 1
the test execution procedure, and to the test module.
d59 13
a71 4
The inputs to this test will be a standard packet file, and a baseline
input file. The output will be 2 parts : the output from processing
the packets, and a status message reporting the success or failure of
the tests.
d73 1
a73 1

@

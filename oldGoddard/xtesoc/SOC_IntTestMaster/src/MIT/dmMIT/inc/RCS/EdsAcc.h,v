head	3.7;
access
	soccm
	nessus;
symbols
	CodeFreeze_5_95:3.5
	Build4_3:3.5
	Build4_2:3.5
	Build4_1:3.5
	Build4:3.5
	Build3_3:3.5
	Build3_2:3.5
	Build3_1:3.2
	Build3:3.2
	Build2:1.2;
locks; strict;
comment	@ * @;


3.7
date	95.09.28.15.08.58;	author berczuk;	state Exp;
branches;
next	3.6;

3.6
date	95.09.28.13.56.22;	author berczuk;	state Exp;
branches;
next	3.5;

3.5
date	94.08.12.20.52.48;	author berczuk;	state Exp;
branches;
next	3.4;

3.4
date	94.08.12.14.40.05;	author berczuk;	state Exp;
branches;
next	3.3;

3.3
date	94.07.11.15.02.19;	author berczuk;	state Exp;
branches;
next	3.2;

3.2
date	94.05.19.18.33.32;	author berczuk;	state Exp;
branches;
next	3.1;

3.1
date	94.04.29.16.24.26;	author berczuk;	state Exp;
branches;
next	2.7;

2.7
date	94.04.28.22.51.33;	author berczuk;	state Exp;
branches;
next	2.6;

2.6
date	94.03.11.22.21.54;	author berczuk;	state Exp;
branches;
next	2.5;

2.5
date	94.03.07.19.59.03;	author berczuk;	state Exp;
branches;
next	2.4;

2.4
date	94.02.01.15.21.33;	author berczuk;	state Exp;
branches;
next	2.3;

2.3
date	93.12.15.22.24.18;	author berczuk;	state Exp;
branches;
next	2.2;

2.2
date	93.12.15.21.27.22;	author berczuk;	state Exp;
branches;
next	2.1;

2.1
date	93.11.30.21.31.42;	author berczuk;	state Exp;
branches;
next	1.2;

1.2
date	93.11.09.22.33.01;	author berczuk;	state Release;
branches;
next	1.1;

1.1
date	93.11.01.15.33.55;	author berczuk;	state Exp;
branches;
next	;


desc
@Base class for Eds accessors
@


3.7
log
@removed accessEdsSpecial function since the descriptors it checks for
are now in the ConfigDB.
@
text
@//   RCS Stamp: $Id: EdsAcc.h,v 3.6 1995/09/28 13:56:22 berczuk Exp berczuk $
//   Description : Base class for EDS Accessors
//   Author      : Steve Berczuk - berczuk@@mit.edu
//   Copyright   : Massachusetts Institute of Technology

// .NAME EdsAcc
// .LIBRARY Data Management
// .HEADER Access Data from an EdsPartition 
// .INCLUDE EdsAcc.h
// .FILE  ../src/EdsAcc.C
// .SECTION Author
// Steve Berczuk, berczuk, MIT, berczuk@@mit.edu
// .SECTION Description
// This class moderates data access requests from the data management subsystem
// to the data in EdsPartitions.

#ifndef EDSACC_h
#define EDSACC_h

class EdsPartition;
class DataValue;
class EdsDataSet;


#include <rw/collect.h>
#include <Accessor.h>
#include <RcPointer.h>
#include <EdsPartition.h>


#define EdsAcc_ID 214
class EdsAcc : public Accessor
{
    
public:
    EdsAcc(const EdsPartition* p =0,EdsDataSet* ds =0);
    // create an accessor from a partition and a pointer to a parent dataset

    ~EdsAcc();
    //destructor
    
    void setDataSet(const EdsDataSet* ds);
    
    void flushCache();
    // flush the data cache for this accessor

    DataValue* access(const Descriptor&) const;
    // access the data pointed to by this accessor
    // the access operation may cache the data from the partition
    // contained by this accessor, so you may want to call flushCache.
 
    int descriptorCount() const;
    // return a count of the number of items in the descriptor list

    int isAvailable(const Descriptor& d) const;
    // returns 1 if the listed descriptor is available, 0 else
    
    int apid() const;
    //return the appid of the packets in this partition
  
    // rogue wave overridden methods

    int compareTo(const RWCollectable*) const;
    // compares the Accessors for ordering

    unsigned long partNumber() const;
    // return the sequence number of the partiton we are accessing
    
    //Persistence methods
    RWspace binaryStoreSize() const;

    void restoreGuts(RWFile&);
    void restoreGuts(RWvistream&);
    void saveGuts(RWFile&) const;
    void saveGuts(RWvostream&) const;

private:
    EdsPartition* thePartition;
    // the partition being accessed
    
    RcPointer<UShortValVec> dataCache;
    //Cache to hold unpacked part data

    const EdsDataSet* parentDataSet;
    // data set in which this accessor is contained.
    
    DataValue* accessEdsStandard(const Descriptor& d) const ;
    // return a DataValue if the descriptor is generic
    // NULL otherwise

    DataValue* accessZ(const Descriptor& d) const;

    DataValue* accessCoda(const Descriptor& d) const;
    
    RWDECLARE_COLLECTABLE(EdsAcc)

};


inline void EdsAcc::setDataSet(const EdsDataSet* ds)
{
    parentDataSet=ds;
}

#endif
@


3.6
log
@added data members to handle cacheing of Part Data.
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: EdsAcc.h,v 3.5 1994/08/12 20:52:48 berczuk Exp berczuk $
a89 3

    DataValue* accessEdsSpecial(const Descriptor& d) const ;
    // access Eds data that has some instrument specific behaviour
@


3.5
log
@added accessors for Coda data
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: EdsAcc.h,v 3.4 1994/08/12 14:40:05 berczuk Exp berczuk $
d27 2
d43 4
a46 1
        
d49 2
a50 2
    // this should be a pure virtual function, but since it is a collectable
    // class it cannot be
d81 3
@


3.4
log
@added accessors for modeSpecific that reference the data set
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: EdsAcc.h,v 3.3 1994/07/11 15:02:19 berczuk Exp berczuk $
d88 1
a88 1
    DataValue* accessCodaData(const Descriptor& d) const;
d93 1
@


3.3
log
@made persistence work with RW 6
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: EdsAcc.h,v 3.2 1994/05/19 18:33:32 berczuk Exp berczuk $
d22 1
d24 1
a24 1
#include <rw5-6compat.h>
d32 1
d34 2
a35 4
  
    EdsAcc(const EdsPartition* =0);
    // create an accessor with a known number of energy channels
    // and a known number of Position Bins
d39 3
a41 1

d60 3
d76 4
a79 1
    DataValue* accessEdsStandard(const Descriptor& ) const ;
d83 7
a89 1
    DataValue* accessZ(const Descriptor& ) const;
d94 4
@


3.2
log
@added new functions
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: EdsAcc.h,v 3.1 1994/04/29 16:24:26 berczuk Exp berczuk $
a5 2


d23 1
d60 2
a61 1
    unsigned binaryStoreSize() const;
d68 1
a68 1
    const EdsPartition* thePartition;
@


3.1
log
@Build 3 Initial Release
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: EdsAcc.h,v 2.7 1994/04/28 22:51:33 berczuk Exp berczuk $
a5 3
// .NAME EDSAcc
// .HEADER Base class for EDS Accessors
// .INCLUDE EdsAcc.h
d12 1
a12 1
// .FILE  ../src/EdsAcc.h
d34 11
a44 11
  EdsAcc(const EdsPartition* =0);
  // create an accessor with a known number of energy channels
  // and a known number of Position Bins

  ~EdsAcc();
  //destructor

  DataValue* access(const Descriptor&) const;
  // access the data pointed to by this accessor
  // this should be a pure virtual function, but since it is a collectable
  // class it cannot be
d46 2
a47 2
  int descriptorCount() const;
  // return a count of the number of items in the descriptor list
d49 2
a50 2
  int isAvailable(const Descriptor& d) const;
  // returns 1 if the listed descriptor is available, 0 else
d52 2
a53 2
  int apid() const;
  //return the appid of the packets in this partition
d55 1
a55 1
  // rogue wave overridden methods
d57 13
a69 2
  int compareTo(const RWCollectable*) const;
  // compares the Accessors for ordering
d71 3
a73 10
  //Persistence methods
  unsigned binaryStoreSize() const;
  void restoreGuts(RWFile&);
  void restoreGuts(RWvistream&);
  void saveGuts(RWFile&) const;
  void saveGuts(RWvostream&) const;

protected:
  const EdsPartition* thePartition;
  // the partition being accessed
d75 2
a76 1
  RWDECLARE_COLLECTABLE(EdsAcc)
@


2.7
log
@added destructor
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: EdsAcc.h,v 2.6 1994/03/11 22:21:54 berczuk Exp berczuk $
@


2.6
log
@added isAvailable() method
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: EdsAcc.h,v 2.5 1994/03/07 19:59:03 berczuk Exp berczuk $
d40 3
@


2.5
log
@added compareTo() method
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: EdsAcc.h,v 2.4 1994/02/01 15:21:33 berczuk Exp berczuk $
d49 3
@


2.4
log
@removed descriptors() method
changed desriptor* to desciptor&
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: EdsAcc.h,v 2.3 1993/12/15 22:24:18 berczuk Exp berczuk $
d9 6
a14 1
// .FILE PktEds.h
d16 2
a17 1

d19 2
a20 3
// This class describes a position Histogram. The Histogram is a binned
// strucure where the number of energy channels and number of position bins 
// is configurable.
a44 1

d52 5
a56 1
  
a64 1
  RWDECLARE_COLLECTABLE(EdsAcc)
d68 2
a69 2
  // subclassed will cache obs parameters from configuration
  // as part of their protected data
@


2.3
log
@removed the makeAccessor() method
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: EdsAcc.h,v 2.2 1993/12/15 21:27:22 berczuk Exp berczuk $
d36 1
a36 1
  DataValue* access(Descriptor*) const;
d41 1
a41 3
  const Descriptor** descriptors() const ;
  // return the descriptors constained by this dataset

a47 2
//  static EdsAcc* makeAccessor(const EdsPartition* thePart);
  // allocate an EdsAccessor of the appropriate subclass
@


2.2
log
@Constructor now takes a const EdsAcc*; stores a const ptr to a
shared partition
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: EdsAcc.h,v 2.1 1993/11/30 21:31:42 berczuk Exp berczuk $
d50 1
a50 1
  static EdsAcc* makeAccessor(const EdsPartition* thePart);
@


2.1
log
@build2 baseline
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: EdsAcc.h,v 1.2 1993/11/09 22:33:01 berczuk Release berczuk $
d32 1
a32 1
  EdsAcc(EdsPartition* =0);
d62 1
a62 1
  EdsPartition* thePartition;
@


1.2
log
@added makeAccessor() method
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: EdsAcc.h,v 1.1 1993/11/01 15:33:55 berczuk Exp berczuk $
@


1.1
log
@Initial revision
@
text
@d1 1
a1 1
//   RCS Stamp: $Id$
d50 3
a52 1
 
@

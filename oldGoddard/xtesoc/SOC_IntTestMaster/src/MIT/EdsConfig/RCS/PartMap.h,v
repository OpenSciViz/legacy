head	5.2;
access
	soccm
	nessus
	gromeo;
symbols
	Consistent-switch-to-solaris:5.2
	Consistent-after-restore-dump-mostly-finished:5.2
	Fixed-DR805:5.1
	Consistent-GenericTrigger2:5.1
	Consistent-GenericTrigger:5.1
	Build5_0_3:5.1
	Interim5_0_2-1:5.1
	Consistent-SafetyTrigger:5.1
	Build5_0_1:5.1
	RciPointers:4.6
	Fixed-DR650:4.6
	Fixed-DR502:4.3
	Build4_3_1:4.3
	Build4_3:4.3
	Build4_2:4.2
	Consistent-post4-mod2:4.1
	Consistent-post4-mod1:4.1
	Build4:4.1
	Build3_3:3.4
	Consistent-no-template-instantiation:3.4
	Consistent-bm-param-dumps:3.2
	Build3_2:3.1
	Consistent-fix-for-RW6:3.1
	Build3_1:3.1
	Consistent-first-newdb:3.1
	C-structs-beta2:3.1
	Build3:3.1
	Current:5.2
	C-structs-beta:2.5
	Build2:2.3;
locks; strict;
comment	@ * @;


5.2
date	97.05.08.16.42.33;	author nessus;	state Exp;
branches;
next	5.1;

5.1
date	95.12.23.00.58.22;	author nessus;	state Exp;
branches;
next	4.6;

4.6
date	95.10.24.20.37.50;	author nessus;	state Exp;
branches;
next	4.5;

4.5
date	95.09.26.15.44.49;	author nessus;	state Exp;
branches;
next	4.4;

4.4
date	95.09.14.22.29.43;	author nessus;	state Exp;
branches;
next	4.3;

4.3
date	95.04.18.22.06.46;	author nessus;	state Exp;
branches;
next	4.2;

4.2
date	95.03.27.21.26.51;	author nessus;	state Exp;
branches;
next	4.1;

4.1
date	94.12.01.14.20.36;	author nessus;	state Exp;
branches;
next	3.5;

3.5
date	94.12.01.13.35.03;	author nessus;	state Exp;
branches;
next	3.4;

3.4
date	94.10.18.19.07.39;	author nessus;	state Exp;
branches;
next	3.3;

3.3
date	94.09.14.21.53.02;	author nessus;	state Exp;
branches;
next	3.2;

3.2
date	94.08.22.18.26.12;	author nessus;	state Exp;
branches;
next	3.1;

3.1
date	94.05.02.07.09.07;	author nessus;	state Exp;
branches;
next	2.6;

2.6
date	94.05.02.07.04.35;	author nessus;	state Exp;
branches;
next	2.5;

2.5
date	94.01.31.04.39.17;	author nessus;	state Exp;
branches;
next	2.4;

2.4
date	94.01.19.23.00.54;	author nessus;	state Exp;
branches;
next	2.3;

2.3
date	93.11.30.22.32.50;	author nessus;	state Release;
branches;
next	2.2;

2.2
date	93.11.09.19.45.20;	author nessus;	state Exp;
branches;
next	2.1;

2.1
date	93.10.27.17.56.18;	author nessus;	state Exp;
branches;
next	1.3;

1.3
date	93.10.20.21.16.18;	author nessus;	state Exp;
branches;
next	1.2;

1.2
date	93.10.19.20.08.00;	author nessus;	state Exp;
branches;
next	1.1;

1.1
date	93.10.15.16.02.38;	author nessus;	state Exp;
branches;
next	;


desc
@@


5.2
log
@First pass at getting parameter dump restores to work for most modes.
Have not yet implemented restores for FftMode.
Also turned on referenced-counter pointers to ObsParamItems.
@
text
@// -*- Mode: C++; fill-column: 79; fill-prefix: "//# " -*-
// $Id: PartMap.h,v 1.20 1997/05/08 00:41:57 nessus Exp $

// Description : [TBD]
// Author      : Douglas Alan <nessus@@mit.edu>
// Copyright   : Massachusetts Institute of Technology, 1993

#ifndef PartMap_h
#define PartMap_h

#include <IntelAddress.h>
#include <rw/collect.h>
#include "DynaVector.h"
#include "EacTypedefs.h"
#include "ObsParamItem.h"
#include "Problem.h"
#include "RcPointer.h"
#include "da_usual.h"

//*****************************************************************************
//*****                                                                   *****
//*****             PartMap: subclass of ObsParamItem                     *****
//*****                                                                   *****
//*****************************************************************************

class PartMap: public ObsParamItem
{
  friend class PartMapUnpackIter;

  //* Instance variables:
  DynaVector<int>		_wordCounts;
  DynaVector<char>		_packCodes;

protected:

  //* Protected nonvirtual methods:
  void				_print(ostream& s) const;

  //* Protected virtual methods inherited from ObsParamItem:
  void				_compare(const ObsParamItem& item,
					 ItemDiffs& diffs) const;

public:

  //* Class constants:
  typedef ObsParamItem          base;
  static const RWCString       className;
  enum PackCode { full, half, quarter, ignore };

  static const char* const	numberOfWordCountsDiff;
  static const char* const	numberOfPackCodesDiff;
  static const char* const	wordCountDiff;
  static const char* const	packCodesDiff;

  //* Constructors, etc:
  static PartMapPtr       	restore(RWCString partMapName);
  static PartMapPtr		restoreFromParamDump(const ObsParamDump& dump,
						     IntelAddress& address,
						     Problem& prob);

  //* Class procedures:
  static MitBool		isLegalFieldTag(RWCString tag);	

  //* Nonvirtual methods:
  void				addEntry(int count, PackCode code);
  void				compareNv(const PartMap& item,
					  ItemDiffs& diffs) const;
  int				unpackedSize() const;
  int				unpackedSize(int lPackedSize,
					     int& howMuchExtra = intBitBucket)
                                     const;
  int				packedSize() const;
  int				packedSize(int lPartitionSize,
					   int& howMuchExtra = intBitBucket)
                                     const;
  int				unpack(const uint16& source,
				       uint16& dest,
				       int lPackedSize) const;
  uint16*			unpack(const uint16& source,
				       int lPackedSize) const;
  RcPointer< DynaVector<uint16> >
                                unpack(const uint16 packedData[],
				       int lPackedSize,
				       int& howMuchExtra = intBitBucket) const;
  int				pack(const uint16& source,
				     uint16& dest,
				     int lPartitionSize) const;
  int				partitionSize() const;
  int				partitionSize(int lPackedSize) const;
  int				restorePartition(const uint16& source,
						 uint16& dest,
						 int lPackedSize) const;

  //* Virtual methods inherited from ObsParamItem:
  PartMapPtr			castToPartMap();
  ConstPartMapPtr		castToPartMap() const;
  void				makeEdsImage(EdsMemoryBlockImage& image,
					     EdsMemoryBlock& block) const;
  MitBool        	 	operator==(const ObsParamItem& item) const;
  void				print(ostream& s) const;
  void				printCstruct(ostream& s) const;
  void				restoreFromParser(const EacParser& parser);
  int				sizeOfEdsImage() const;
  MitBool			vIsLegalFieldTag(const RWCString& tag) const;

};


//*****************************************************************************
//*****                                                                   *****
//*****          PartMapUnpackIter: const iterator class                  *****
//*****                                                                   *****
//*****************************************************************************


class PartMapUnpackIter {

  //* Instance variables:
  PartMapPtr			_partMap;
  DynaVector<char>&		_packCodes;
  DynaVector<int>&		_wordCounts;
  const uint16*			_packedData;
  int				_packedSize;
  int				_packedChunkSize;
  PartMap::PackCode		_packCode;
  const int			_numOfEntries;
  int				_currentEntry;
  MitBool			_isThereMore;
  int				_howMuchExtraPackedData;
  int				_unpackedValueBufferIndex;
  uint16			_unpackedValueBuffer[4];

  //* Private methods:
  void				unpackOneUint16();

public:

  //* Constructors, etc:
  PartMapUnpackIter(const PartMapPtr partMap,
		    const uint16 packedData[],
		    int packedSize);

  //* Nonvirtual methods:
  int			       howMuchExtraPackedData() const
                                  { return _howMuchExtraPackedData; }
  operator		       MitBool() const;
  PartMapUnpackIter&           operator++();
  void			       operator++(int) { operator++(); }
  uint16		       operator*() const;
};

#endif // PartMap_h
@


5.1
log
@Bump version number
@
text
@d2 1
a2 1
// $Id: PartMap.h,v 4.6 1995/10/24 20:37:50 nessus Exp nessus $
@


4.6
log
@Added support for propper unpacking of EventBurstCatcherMode partitions.
@
text
@d2 1
a2 1
// $Id: PartMap.h,v 1.19 1995/10/18 19:54:00 nessus Exp nessus $
@


4.5
log
@First pass at getting BinnedMode param dumps working.  They seem to work
great except for breakpoints, which don't make it down.
@
text
@d2 1
a2 1
// $Id: PartMap.h,v 1.18 1995/09/22 17:14:36 nessus Exp nessus $
d30 1
a30 1
  // Instance variables:
d35 2
d39 4
d45 1
a45 1
  // Class constants:
d50 5
d66 2
a150 1

@


4.4
log
@I am checking this version in to make the new Problem class available
for use by Steve.
@
text
@d2 1
a2 1
// $Id: PartMap.h,v 1.17 1995/09/14 22:26:21 nessus Exp nessus $
d16 1
a43 1

d46 3
a48 2
  static PartMapPtr		restoreFromParamDump(const ObsParamDump&,
						     IntelAddress&);
@


4.3
log
@A number of files were modified in order to support unpacking of
SingleBitMode data.  A sytem for signalling errors and warnings was
also implemented.
@
text
@d2 1
a2 1
// $Id: PartMap.h,v 1.16 1995/04/18 21:58:35 nessus Exp nessus $
d38 1
d40 1
d42 1
@


4.2
log
@Check in of a number of files for Build4.2.  These changes are mostly
minor bug fixes.
@
text
@d2 1
a2 1
// $Id: PartMap.h,v 1.15 1995/03/27 21:25:23 nessus Exp nessus $
d16 1
d52 3
a54 1
  int				unpackedSize(int lPackedSize) const;
d56 3
a58 1
  int				packedSize(int lPartitionSize) const;
d64 4
@


4.1
log
@Bump version number
@
text
@d2 1
a2 1
// $Id: PartMap.h,v 3.5 1994/12/01 13:35:03 nessus Exp nessus $
d18 5
a22 3
//#############################################################################
//# PartMap: subclass of ObsParamItem
//#############################################################################
d26 1
d81 45
@


3.5
log
@Mods needed for Build 4.
@
text
@d2 1
a2 1
// $Id: PartMap.h,v 1.13 1994/11/23 22:02:22 nessus Exp nessus $
@


3.4
log
@Ridded my code of all templates that do link-time instatiation.
@
text
@d2 1
a2 1
// $Id: PartMap.h,v 1.12 1994/10/18 19:05:19 nessus Exp nessus $
a17 7

class PartMap;

// typedef EacPtr<PartMap>	     PartMapPtr;
typedef PartMap*		     PartMapPtr;


a23 2
// [TODO: make the RWCollectable stuff work again.]
//  RWDECLARE_COLLECTABLE(PartMap)
a24 2
private:

d37 1
a37 1
  // Constructors, etc:
d42 1
a42 1
  // Class procedures:
d45 1
a45 1
  // Methods:
d65 11
a75 9
  // Virtual methods inherited from ObsParamItem:
  virtual void			print(ostream& s) const;
  virtual void			printCstruct(ostream& s) const;
  virtual PartMapPtr		castToPartMap() { return this; }
  virtual ConstPartMapPtr	castToPartMap() const { return this; }
  virtual void			restoreFromParser(const EacParser& parser);
  virtual MitBool		vIsLegalFieldTag(const RWCString& tag) const
                                   { return isLegalFieldTag(tag); }
  virtual MitBool               operator==(const ObsParamItem& item) const;
a76 5
  // Methods inherited from class RWCollectable:
//   virtual void			saveGuts(RWFile& file) const;
//   virtual void			saveGuts(RWvostream& s) const;
//   virtual void			restoreGuts(RWFile& file);
//   virtual void			restoreGuts(RWvistream& s);
@


3.3
log
@Got first pass at PosHistMode working.
@
text
@d2 1
a2 1
// $Id: PartMap.h,v 1.11 1994/09/14 21:48:48 nessus Exp nessus $
d13 1
a13 1
#include "CluArray.h"
@


3.2
log
@BinnedMode parameter dump restores and operator==() now work.
@
text
@d2 1
a2 1
// $Id: PartMap.h,v 1.10 1994/08/22 18:21:35 nessus Exp nessus $
d37 2
a38 2
  CluArray<int>			_wordCounts;
  CluArray<char>		_packCodes;
@


3.1
log
@Bump version number
@
text
@d2 1
a2 1
// $Id: PartMap.h,v 2.6 1994/05/02 07:04:35 nessus Exp nessus $
d11 2
a12 1
#include "da_usual.h"
d14 1
d16 1
a16 1
#include <rw/collect.h>
d18 1
d45 1
d50 2
d84 1
a91 3

// Global procedures:
ostream&			operator<<(ostream& s, const PartMap& config);
@


2.6
log
@Check in of files for Build 3.
@
text
@d2 1
a2 1
// $Id: PartMap.h,v 1.9 1994/05/02 06:56:14 nessus Exp nessus $
@


2.5
log
@Fixes for Flight Software Beta ROM config C-struct making.  Also
added support for loading in a config file and for getting loaded
configs by ConfigId.
@
text
@d2 1
a2 1
// $Id: PartMap.h,v 1.7 1994/01/28 23:06:57 nessus Exp nessus $
d48 1
a48 1
  static EdsBool		isLegalFieldTag(RWCString tag);	
d74 3
a76 2
  virtual SuccessFlag		restoreFromParser(const EacParser& parser);
  virtual EdsBool		vIsLegalFieldTag(RWCString tag) const
@


2.4
log
@Check in of many files to support the 'make-structs' program, which will
be used to generate configurations in a form suitable for compiling
into the flight software.
@
text
@d2 1
a2 1
// $Id: PartMap.h,v 1.6 1994/01/14 20:59:43 nessus Exp nessus $
d47 3
d75 2
a76 1
  
@


2.3
log
@Added or modified rcs id header.
@
text
@d2 1
a2 1
// $Id$
a27 3
friend ostream& operator<<(ostream& s, const PartMap& pm);
friend istream& operator>>(istream& s, PartMap& pm);

d37 3
d44 2
a45 2
  // Static member fuctions:
  // static PartMapPtr       restore(RWCString partMapName);
d67 8
a74 1
//   // Methods inherited from class RWCollectable:
d80 3
@


2.2
log
@Build which allows Steve to play with six stubbed configurations.
@
text
@d1 7
@


2.1
log
@Only bumped revision number up to 2.1.
@
text
@d24 2
a25 1
  RWDECLARE_COLLECTABLE(PartMap)
d60 5
a64 5
  // Methods inherited from class RWCollectable:
  virtual void			saveGuts(RWFile& file) const;
  virtual void			saveGuts(RWvostream& s) const;
  virtual void			restoreGuts(RWFile& file);
  virtual void			restoreGuts(RWvistream& s);
@


1.3
log
@Added version of unpack that allocates the destination array.
@
text
@@


1.2
log
@Changed name of clu_array to CluArray.
@
text
@a44 3
  void				unpack(const uint16& source,
				       uint16& dest) const
                                   { unpack(source, dest, packedSize()); }
d48 2
a49 3
  void				pack(const uint16& source,
				     uint16& dest) const
                                   { pack(source, dest, partitionSize()); }
a54 5
  void				restorePartition(const uint16& source,
						  uint16& dest) const
                                   { restorePartition(source,
						      dest,
						      packedSize()); }
@


1.1
log
@Initial revision
@
text
@d5 1
a5 1
#include "clu_array.h"
d29 2
a30 2
  clu_array<int>		_wordCounts;
  clu_array<char>		_packCodes;
@

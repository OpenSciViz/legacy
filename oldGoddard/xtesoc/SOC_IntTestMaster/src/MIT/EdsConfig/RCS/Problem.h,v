head	5.2;
access
	soccm
	nessus
	gromeo;
symbols
	Consistent-switch-to-solaris:5.2
	Consistent-after-restore-dump-mostly-finished:5.2
	Fixed-DR805:5.1
	Consistent-GenericTrigger2:5.1
	Consistent-GenericTrigger:5.1
	Build5_0_3:5.1
	Interim5_0_2-1:5.1
	Consistent-SafetyTrigger:5.1
	Current:5.2
	Build5_0_1:5.1
	RciPointers:4.1
	Fixed-DR650:1.1;
locks; strict;
comment	@ * @;


5.2
date	97.05.08.16.42.37;	author nessus;	state Exp;
branches;
next	5.1;

5.1
date	95.12.23.00.58.36;	author nessus;	state Exp;
branches;
next	4.1;

4.1
date	95.11.21.02.45.03;	author nessus;	state Exp;
branches;
next	1.1;

1.1
date	95.09.14.22.47.03;	author nessus;	state Exp;
branches;
next	;


desc
@Problem - classes to facilitate error handling w/o exceptions.
@


5.2
log
@First pass at getting parameter dump restores to work for most modes.
Have not yet implemented restores for FftMode.
Also turned on referenced-counter pointers to ObsParamItems.
@
text
@// -*- Mode: C++; fill-column: 73; fill-prefix: "//# " -*-
#ifndef Problem_H
#define Problem_H \
"$Id: Problem.h,v 1.3 1997/05/08 00:42:06 nessus Exp $"

// Description : Declaration of the Problem class, which allows a
//               procedure to gracefully return errors and warnings to
//               its invoker (in the absence of C++ exceptions).
// Author      : Douglas Alan <nessus@@mit.edu>
// Copyright   : Massachusetts Institute of Technology, 1995

// For documentation on all of the classes and methods declared in this
// file, see the ".C" file.

// .file Problem.h
// .file Problem.C
// .options -e -h

#include <rw/cstring.h>
#include "DynaVector.h"

//*****************************************************************************
//*****                                                                   *****
//*****               ProblemHandler: abstract class                      *****
//*****                                                                   *****
//*****************************************************************************

class ProblemHandler {
  friend class Problem;

  //* Instance variables:
  MitBool			_isPrototype;
  int				_refCount;
  RWCString			_subsystemName;
  RWCString			_className;
  RWCString			_procedureName;

  //* Private nonvirtual methods:
  ProblemHandler*		makeHandler();
  void				incrementRefCount()
                                   { if (!_isPrototype) ++_refCount; }
  void				decrementRefCount()
                                   { if (!_isPrototype) --_refCount; }
  void				setError(const RWCString& message)
                                   { makeHandler(); _setError(message); }
  void				setWarning(const RWCString& message)
                                   { makeHandler(); _setWarning(message); }
  MitBool			isError() const
                                   { if (_isPrototype) return mitFalse;
				     else return _isError();
				   }
  MitBool			isWarning() const
    				   { if (_isPrototype) return mitFalse;
				     else return _isWarning();
				   }
  void				clear() { if (!_isPrototype) _clear(); }
  MitBool			hasProblemBeenHandled()
                                   { if (_isPrototype) return mitFalse;
				     else return _hasProblemBeenHandled();
				   }
  RWCString			message();


protected:

  enum ProtoFlag { isProto };

  //* Protected constructor:
  ProblemHandler();

  //* Protected accessor methods:
  RWCString			subsystemName() const { return _subsystemName;}
  RWCString			className() const     { return _className; }
  RWCString			procedureName() const { return _procedureName;}

  //* Protected setter methods:
  void				setIsPrototype() { _isPrototype = mitTrue; }

  //* Base protected virtual methods:

  virtual			~ProblemHandler();      // Dtor
  //^# The destructor should do something appropriate if it determines that
  //^# any errors or warnings have not been handled.

  virtual void			_setError(const RWCString& message) = 0;
  //^# _setError() is invoked by Problem::setError().

  virtual void			_setWarning(const RWCString& message) = 0;
  //^# _setWarning() is invoked by Problem::setWarning().

  virtual MitBool		_isError() const = 0;
  //^# _isError() should return true iff an error has been raised.

  virtual MitBool		_isWarning() const = 0;
  //^# _isWarning() should return true iff a warning has been raised.

  virtual void			_clear() = 0;
  //^# _clear() is invoked by Problem::clear().  If this method has been
  //^# called, then the destructor should be passive.

  virtual RWCString		_message() const = 0;
  //^# _message() creates the error message that is returned by
  //^# Problem::message().

  virtual ProblemHandler*	_makeHandler() const = 0;
  //^# _makeHandler() should create a new instance of the class that is
  //^# overriding this method.  This method does not need to check for
  //^# heap exhaustion.

  virtual MitBool		_hasProblemBeenHandled() const = 0;
  //^# _hasProblemBeenHandled() should return true iff an error or warning
  //^# was raised, and it is to be considered that the error or warnings
  //^# has been handled.  This usually means that the _clear() method has
  //^# been called.
};


//*****************************************************************************
//*****                                                                   *****
//*****      StandardProblemHandler: subclass of ProblemHandler           *****
//*****                                                                   *****
//*****************************************************************************

class StandardProblemHandler: public ProblemHandler {

  //* Instance variables:
  DynaVector<RWCString>		_errorMessages;
  DynaVector<RWCString>		_warningMessages;
  MitBool			_wasProblemCleared;

  //* Private class variables:
  static const
  StandardProblemHandler        _prototype;

  //* Private constructor:
  StandardProblemHandler(ProtoFlag);

protected:

  //* Protected constructor:
  StandardProblemHandler();

  // Protected accessor methods:

  int				errorCount() const
                                   { return _errorMessages.length(); }
  //^# Returns the number of errors that have been raised via this
  //^# ProblemHandler.

  int				warningCount() const
                                   { return _warningMessages.length(); }
  //^# Returns the number of warnings that have been raised via this
  //^# ProblemHandler.

  //* Protected virtual methods inherited from ProblemHandler:
  virtual void			_setError(const RWCString& message);
  virtual void			_setWarning(const RWCString& message);
  MitBool			_isError() const;
  virtual MitBool		_isWarning() const;
  virtual void			_clear();
  virtual RWCString		_message() const;
  ProblemHandler*		_makeHandler() const;
  virtual MitBool		_hasProblemBeenHandled() const;

public:

  //* Class procedures:
  static const
  StandardProblemHandler&	prototype();
};


//*****************************************************************************
//*****                                                                   *****
//*****       DefaultProblemHandler: subclass of ProblemHandler           *****
//*****                                                                   *****
//*****************************************************************************

class DefaultProblemHandler: public ProblemHandler {

  //* Private class variables:
  static const
  DefaultProblemHandler         _prototype;

  //* Private constructor:
  DefaultProblemHandler(ProtoFlag);


protected:

  //* Protected constructor:
  DefaultProblemHandler();

  //* Protected virtual methods inherited from ProblemHandler:
  virtual void			_setError(const RWCString& message);
  virtual void			_setWarning(const RWCString& message);
  MitBool			_isError() const;
  virtual MitBool		_isWarning() const;
  virtual void			_clear();
  virtual RWCString		_message() const;
  ProblemHandler*		_makeHandler() const;
  virtual MitBool		_hasProblemBeenHandled() const;

public:
  
  //* Class procedures:
  static const
  DefaultProblemHandler&	prototype();
};


//*****************************************************************************
//*****                                                                   *****
//*****     LoggingProblemHandler: subclass of StandardProblemHandler     *****
//*****                                                                   *****
//*****************************************************************************

class LoggingProblemHandler: public StandardProblemHandler {

protected:
  
  //* Private virtual methods inherited from ProblemHandler:
  virtual void			_setError(const RWCString& message);
  virtual void			_setWarning(const RWCString& message);
  MitBool			_isError() const;
  virtual MitBool		_isWarning() const;
  virtual void			_clear();
  virtual RWCString		_message() const;
  ProblemHandler*		_makeHandler() const;
  virtual MitBool		_hasProblemBeenHandled() const;

  //* Protected constructor
  LoggingProblemHandler();
};


//*****************************************************************************
//*****                                                                   *****
//*****               Problem: concrete handle class                      *****
//*****                        handle for ProblemHandler                  *****
//*****                                                                   *****
//*****************************************************************************

class Problem {
  
  //* Instance variables:
  ProblemHandler*		_handler;

  //* Class variables:
  static Problem		_theDefault;

  //* Private methods:
  void				free();

public:

  //* Constructors, etc:
  Problem(const ProblemHandler& prototype
             = StandardProblemHandler::prototype());  // Default ctor
  Problem(const Problem& orig);                       // Copy ctor
  Problem& operator=(const Problem& orig);            // Assignment operator
  ~Problem();			                      // Dtor

  //* Class procedures:
  static Problem&		theDefault();

  //* Setter methods:
  void				setSubsystemName(const RWCString& subsystem);
  void				setClassName(const RWCString& className);
  void				setProcedureName(const RWCString& procName);
  void				setInfo(const RWCString& subsystemName,
					const RWCString& className,
					const RWCString& procName);


  //* Nonvirtual methods:
  void				setError(const RWCString& message);
  void				setWarning(const RWCString& message);

  MitBool			isError() const;
  MitBool			isWarning() const;
  operator			MitBool() const;

  RWCString			message() const;

  void				clear();
  MitBool			hasProblemBeenHandled() const;
};


//-----------------------------------------------------------------------------
// Inline method definitions for class Problem
//-----------------------------------------------------------------------------

//# Returns a message describing the errors and warnings that have been
//# raised.  If there have been no warnings or errors, then the empty
//# string is returned.  (This assumes that if the ProblemHandler is
//# user-defined that it behaves accordingly.)


inline RWCString
Problem::message() const
{
  return _handler->message();
}


//# This method is to be used by the caller of a procedure that might
//# have raised an error or warning.  Use of this method is intended to
//# indicate to the Problem class that the caller has handled the errors
//# or or warnings.  It is considered a fatal error if an error is not
//# cleared and consequently the program will be aborted by the Problem
//# class when the Problem is destroyed.  If a warning is not cleared,
//# then a warning message will be output by the Problem class to 'cerr'.

inline void
Problem::clear()
{
  _handler->clear();
}

//# This method returns true if an only if an error or warning has been
//# raised and then handled.  By ``handled'' we usually mean that the
//# `clear()' method has been called.

inline MitBool
Problem::hasProblemBeenHandled() const
{
  return _handler->hasProblemBeenHandled();
} 


//# Returns true iff an error has been raised via the Problem.

inline MitBool
Problem::isError() const
{ 
  return MitBool(!_handler->_isPrototype && _handler->isError());
}


//# Returns true iff a warning has been raised via the Problem.

inline MitBool
Problem::isWarning() const
{
  return MitBool(!_handler->_isPrototype && _handler->isWarning());
}


//# Returns true iff either an error or a warning has been raised via the
//# Problem.

inline
Problem::operator MitBool() const
{
  return MitBool(!_handler->_isPrototype && (isError() || isWarning()));
}

#endif // ProblemHandler_H
@


5.1
log
@Bump version number
@
text
@d4 1
a4 1
"$Id: Problem.h,v 4.1 1995/11/21 02:45:03 nessus Exp nessus $"
a288 7


//-----------------------------------------------------------------------------
// Global procedures
//-----------------------------------------------------------------------------

void mitReportWarning(const char* warningMessage);
@


4.1
log
@Bump version number
@
text
@d4 1
a4 1
"$Id: Problem.h,v 1.1 1995/09/14 22:47:03 nessus Exp nessus $"
@


1.1
log
@Initial revision
@
text
@d4 1
a4 1
"$Id: Problem.h,v 1.1 1995/09/14 22:24:30 nessus Exp $"
@

head	5.3;
access
	soccm
	nessus
	gromeo;
symbols
	Consistent-switch-to-solaris:5.3
	Consistent-after-restore-dump-mostly-finished:5.3
	Fixed-DR805:5.2
	Consistent-GenericTrigger2:5.2
	Consistent-GenericTrigger:5.2
	Build5_0_3:5.2
	Interim5_0_2-1:5.2
	Consistent-SafetyTrigger:5.2
	Build5_0_1:5.1
	RciPointers:4.5
	Fixed-DR650:4.5
	Fixed-DR502:4.3
	Build4_3_1:4.3
	Build4_3:4.3
	Build4_2:4.3
	Consistent-post4-mod2:4.2
	Consistent-post4-mod1:4.1
	Build4:4.3
	Build3_3:3.5
	Consistent-no-template-instantiation:3.5
	Consistent-bm-param-dumps:3.5
	Build3_2:3.4
	Consistent-fix-for-RW6:3.4
	Build3_1:3.4
	Consistent-first-newdb:3.4
	C-structs-beta2:3.3
	Current:5.3
	Build3:3.2;
locks; strict;
comment	@ * @;


5.3
date	97.05.08.16.42.19;	author nessus;	state Exp;
branches;
next	5.2;

5.2
date	96.02.12.21.26.05;	author nessus;	state Exp;
branches;
next	5.1;

5.1
date	95.12.23.00.57.38;	author nessus;	state Exp;
branches;
next	4.5;

4.5
date	95.10.24.20.37.50;	author nessus;	state Exp;
branches;
next	4.4;

4.4
date	95.09.26.15.44.49;	author nessus;	state Exp;
branches;
next	4.3;

4.3
date	95.01.12.00.59.55;	author nessus;	state Exp;
branches;
next	4.2;

4.2
date	95.01.03.18.21.12;	author nessus;	state Exp;
branches;
next	4.1;

4.1
date	94.12.01.14.20.14;	author nessus;	state Exp;
branches;
next	3.5;

3.5
date	94.08.22.18.26.12;	author nessus;	state Exp;
branches;
next	3.4;

3.4
date	94.06.24.01.40.33;	author nessus;	state Exp;
branches;
next	3.3;

3.3
date	94.05.24.16.24.46;	author nessus;	state Exp;
branches;
next	3.2;

3.2
date	94.05.17.19.45.46;	author nessus;	state Exp;
branches;
next	3.1;

3.1
date	94.05.02.07.23.12;	author nessus;	state Exp;
branches;
next	1.1;

1.1
date	94.05.02.07.19.39;	author nessus;	state Exp;
branches;
next	;


desc
@Source code for the EventByEventMode class.
@


5.3
log
@First pass at getting parameter dump restores to work for most modes.
Have not yet implemented restores for FftMode.
Also turned on referenced-counter pointers to ObsParamItems.
@
text
@// -*- Mode: C++; fill-column: 73; fill-prefix: "//# " -*-
static const char rcsid[] = 
"$Id: EventByEventMode.C,v 1.12 1997/05/08 00:41:31 nessus Exp $";

// Description : Definition of the EventByEventMode class.
// Author      : Douglas Alan <nessus@@mit.edu>
// Copyright   : Massachusetts Institute of Technology, 1993

#include <ObsComp.h>
#include <ObsParamDump.h>

#include <MitStreamUtil.h>
#include <EacGlobalData.h>
#include <EacParser.h>
#include <EdsMemoryBlockImage.h>
#include <EventByEventMode.h>
#include <da_sugar.h>

static const char rcsid_H[] = EventByEventModeRcsId_H;

//*****************************************************************************
//*****                                                                   *****
//*****      EventByEventMode: concrete subclass of AsmObsParamBlock      *****
//*****                                                                   *****
//*****      EventByEventDspParams: concrete subclass of ObsParamItem     *****
//*****                                                                   *****
//*****************************************************************************

const int		defaultIntervalDuration = 1 << 20;  // 1 second
typedef EacParser	p;

//----------------------------------------------------------------------------
// Class constants
//----------------------------------------------------------------------------

const RWCString EventByEventMode::className = "EventByEventMode";
const RWCString EventByEventDspParams::className = "EventByEventDspParams";

const RWCString EventByEventMode::intervalDurationName = "intervalDuration";
const RWCString EventByEventMode::dspParamsName = "dspParams";
const RWCString EventByEventDspParams::cameraIdName = "cameraId";
const RWCString EventByEventDspParams::recordAllEventsName = "recordAllEvents";

// Diff tokens:
eacDefineDiffToken(EventByEventMode, intervalDuration)
eacDefineDiffToken(EventByEventMode, dspParams)
eacDefineDiffToken(EventByEventDspParams, cameraId)
eacDefineDiffToken(EventByEventDspParams, recordAllEvents)

//----------------------------------------------------------------------------
// Constructors
//----------------------------------------------------------------------------

//# [TBD] A new EventByEventMode object is not in a consistent state.
//# In order to make the object consistent, the client must use all
//# manditory setter methods (and may use some of the optional setter
//# methods) and then invoke the update() method.

ctor
EventByEventMode::EventByEventMode()
: _intervalDuration(defaultIntervalDuration),
  _dspParams(0)
{
  // TODO: This is a tenticle of a temporary kludge:
  // setMainDspLoadBlock("asmevent_dspblk");
}


ctor
EventByEventDspParams::EventByEventDspParams()
: _cameraId(0),
  _recordAllEvents(false)
{}


//-----------------------------------------------------------------------------
// configId(): virtual method of EventByEventMode inherited from ObsParamBlock.
// [TBD]
//-----------------------------------------------------------------------------

method ObsParamBlock::ConfigId
EventByEventMode::configId() const
{
  return ConfigId(ConfigId::asmSubsys,
		  ConfigId::eventByEventMode,
		  sequenceNumber());
}


//-----------------------------------------------------------------------------
// _print(): protected method of EventByEventMode
//-----------------------------------------------------------------------------

//# [TBD]

method void
EventByEventMode::_print(ostream& s) const
{
  StreamFlagResetter resetter(s);
  base::_print(s);
  s.setf(ios::showbase);
  s << hex;
  if (_intervalDuration != defaultIntervalDuration)
    s << " intervalDuration:" << _intervalDuration;
  s << " dspParams:";
  _dspParams->printNameOrObject(s);
}


method void
EventByEventDspParams::_print(ostream& s) const
{
  StreamFlagResetter resetter(s);
  base::_print(s);
  s.setf(ios::showbase);
  s << hex;
  s << " cameraId:" << int(cameraId());
  if (_recordAllEvents) s << " recordAllEvents:true";
}


//-----------------------------------------------------------------------------
// printCstruct(): virtual method of EventByEventMode inherited from
// ObsParamItem
//-----------------------------------------------------------------------------

//# [TBD] See documentation for ObsParamItem.

method void		
EventByEventMode::printCstruct(ostream& s) const
{
  StreamFlagResetter resetter(s);
  s.setf(ios::showbase);
  s << hex;
  s << "ASMEVENT_PARMS eacGen" << eacCapitalize(name()) << " =\n";
  s << "{\n";
  base::_printCstruct(s);
  s << ",\n";
  s << "  {\n";
  s << "    " << intervalDuration() << '\n';
  s << "  }\n";
  s << "};\n\n";
}


method void		
EventByEventDspParams::printCstruct(ostream& s) const
{
  StreamFlagResetter resetter(s);
  s.setf(ios::showbase);
  s << hex;
  s << "UINT16 eacGen" << eacCapitalize(name()) << "[]" << " =\n";
  s << "{\n";
  s << "  1,\n";
  s << "  0, 0x06800,\n";
  s << "  2,\n";
  s << "  " << int(cameraIdFs()) << ", " << isRecordingAllEventsFs() << '\n';
  s << "};\n\n";
}


//-----------------------------------------------------------------------------
// makeEdsImage(): virtual method of EventByEventMode and EventByEventDspParams
// inherited from ObsParamItem
//-----------------------------------------------------------------------------

method void
EventByEventMode::makeEdsImage(EdsMemoryBlockImage& image,
			       EdsMemoryBlock& block) const
{
  base::_makeEdsImage(image, block);
  image.appendUint32(intervalDuration());
}


method void
EventByEventDspParams::makeEdsImage(EdsMemoryBlockImage& image,
				    EdsMemoryBlock&) const
{
  image.appendAuxDspLoadBlockHeader(incrEdsImageSize);
  image.appendUint16(cameraIdFs());
  image.appendUint16(isRecordingAllEventsFs());
}


//-----------------------------------------------------------------------------
// sizeOfEdsImage(): virtual method of EventByEventMode and
// EventByEventDspParams inherited from ObsParamItem
//-----------------------------------------------------------------------------

method int
EventByEventMode::sizeOfEdsImage() const
{
  return base::_sizeOfEdsImage() + incrEdsImageSize;
}


method int
EventByEventDspParams::sizeOfEdsImage() const
{
  return EdsMemoryBlockImage::sizeOfAuxDspLoadBlockHeader + incrEdsImageSize;
}


//----------------------------------------------------------------------------
// update(): virtual method of EventByEventMode inherited from ObsParamItem
//----------------------------------------------------------------------------

//# [TBD] An EventByEventMode object will not be in a valid state until
//# all of the non-defaulting setter methods have been called and the
//# update method is invoked.  If at a later point, one or more setter
//# methods are used, the update method must be again invoked to return
//# the EventByEventMode object to a valid state.  This method is needed
//# because there are instance variable that are calculated based on the
//# values of the settable instance variables.  These calculated instance
//# variables are not updated until the update() method is invoked.

// TODO: move the above documentation to ObsParamItem::update().

method void
EventByEventMode::update()
{
  base::update();

  // Make sure that the DSP Params attribute has been set, and if it has
  // set the name of the Auxilliary DSP Load Block appropriately
  // according to the DSP Params attribute:
  // if (_dspParams) 
  //   setAuxDspLoadBlock("eacGen" + eacCapitalize(_dspParams->name()));
  if (!_dspParams) setAttributeNotSetError(dspParamsName);
}


method void
EventByEventDspParams::update()
{
  base::update();
  
  // Make sure that the Camera ID has been set:
  if(_cameraId == 0) setAttributeNotSetError(cameraIdName);
}


//-----------------------------------------------------------------------------
// isLegalFieldTag(): class procedure
//-----------------------------------------------------------------------------

//# [TBD] See documentation for ObsParamItem.

proc bool
EventByEventMode::isLegalFieldTag(const RWCString& tag)
{
  if (p::isEqual(tag, intervalDurationName) ||
      p::isEqual(tag, dspParamsName))
    {
      return true;
    } else return base::isLegalFieldTag(tag);
}


proc bool
EventByEventDspParams::isLegalFieldTag(const RWCString& tag)
{
  if (p::isEqual(tag, cameraIdName) ||
      p::isEqual(tag, recordAllEventsName))
    {
      return true;
    } else return base::isLegalFieldTag(tag);
}


//-----------------------------------------------------------------------------
// restoreFromParser(): virtual method inherited from ObsParamItem
//-----------------------------------------------------------------------------

//# [TBD] See documentation for ObsParamItem.

method void
EventByEventMode::restoreFromParser(const EacParser& parser)
{
  // Restore the base class part of ourself:
  base::restoreFromParser(parser);

  RWCString fieldName;
  RWCString unparsedField;
  SuccessFlag status;

  // Get the "intervalDuration" field:
  fieldName = intervalDurationName;
  unparsedField = parser[fieldName];
  if (!unparsedField.isNull()) {
    uint32 lIntervalDuration = p::parseUnsignedInt(unparsedField, status);
    if (status == succeeded) setIntervalDuration(lIntervalDuration);
    else setFieldParsingError(fieldName);
  }

  // Get the "dspParams" field:
  fieldName = "dspParams";
  unparsedField = parser[fieldName];
  if (!unparsedField.isNull()) {
    RWCString dspParamsName = p::parseWord(unparsedField, status);
    if (status == succeeded) {
      EventByEventDspParamsPtr params =
	EventByEventDspParams::restore(dspParamsName);
      if (params) setDspParams(params);
      else setError("dspParams field refers to unknown "
		    "EventByEventDspParams");
    } else setFieldParsingError(fieldName);
  }
}


method void
EventByEventDspParams::restoreFromParser(const EacParser& parser)
{
  // Restore the base class part of ourself:
  base::restoreFromParser(parser);

  RWCString fieldName;
  RWCString unparsedField;
  SuccessFlag status;

  // Get the "cameraId" field:
  fieldName = cameraIdName;
  unparsedField = parser[fieldName];
  if (!unparsedField.isNull()) {
    int lCameraId = p::parseInt(unparsedField, status);
    if (status == succeeded) setCameraId(lCameraId);
    else setFieldParsingError(fieldName);
  }

  // Get the "recordAllEvents" field:
  fieldName = recordAllEventsName;
  unparsedField = parser[fieldName];
  if (!unparsedField.isNull()) {
    bool flag = p::parseBool(unparsedField, status);
    if (status == succeeded) recordAllEvents(flag);
    else setFieldParsingError(fieldName);
  }
}


//-----------------------------------------------------------------------------
// _restoreFromParamDump(): protected virtual method inherited from
// ObsParamBlock
//-----------------------------------------------------------------------------

//# [TBD]

method void
EventByEventMode::_restoreFromParamDump(
      const ObsParamDump& dump, const uint8* albData, int albSize,
      Problem& prob)
{
  static const char* methodName = "_restoreFromParamDump";

  // Get the raw data for the EventByEventMode parameters:
  ObsComp* comp = dump.getComponent(OBS_COMP_ASMEVENT);
  if (!comp) {
    eacRaiseErrorM("Could not find EventByEventMode data in parameter dump.");
    return;
  }

  uint8* dData = checkHeap(comp->componentData());
  EacArrayDeleter<uint8*> dataDeleter(dData);
  const uint8* data = dData;

  // Check to make sure that the raw data is the correct length:
  const int length = comp->componentLength();
  if (length < incrEdsImageSize) {
    eacRaiseErrorM("EventByEventMode data in parameter dump is short.");
    return;
  }
  if (length > incrEdsImageSize) {
    eacRaiseWarningM("EventByEventMode data in parameter dump is too long.");
  }

  // Restore EventByEventMode parameters from the raw data:
  setIntervalDuration(eacGetUint32FromTelemetry(data));

  // Restore the Auxilliary Load Block:
  _dspParams = EventByEventDspParams::restoreFromParamDump(albData,
							   albSize, prob);
}


//-----------------------------------------------------------------------------
// restoreFromParamDump(): creator class procedur of EventByEventDspParams
//-----------------------------------------------------------------------------

proc EventByEventDspParamsPtr
EventByEventDspParams::restoreFromParamDump(const uint8* data, int size,
					    Problem& prob)
{
  static const char* methodName = "restoreFromParamDump";

  const int supposedSize = EdsMemoryBlockImage::sizeOfAuxDspLoadBlockHeader
                           + incrEdsImageSize;

  const int supposedSize1 = EdsMemoryBlockImage::sizeOfAuxDspLoadBlockHeader +
    ObsParamBlock::getSizeOfDspLoadBlockFromParamDump(data, prob,
						      className, methodName);
  
  // Make sure that the size indicated in the block header is what we were
  // expecting:
  if (supposedSize1 != supposedSize) {
    eacRaiseWarningM(
      "The size of the EventByEventDspParams data as indicated in the block "
      "header is not what was expected.  The value expected is " +
      eacIntToString(supposedSize) + "bytes, while the value in the block "
      "header is " + eacIntToString(supposedSize1) + " bytes.");
  }

  // Make sure that the raw data is of the correct size:
  if (size < supposedSize) {
    eacRaiseErrorM(
       "EventByEventDspParams data in parameter dump is too short.  The "
       "size of the data received is only " + eacIntToString(size) +
       " bytes, but the size indicated at the beginning of the "
       "downlinked EventByEventDspParams data is " +
       eacIntToString(supposedSize) + " bytes.");
    return 0;
  }
  if (size > supposedSize) {
    eacRaiseWarningM(
       "EventByEventDspParams data in parameter dump is too long.  The "
       "size of the data received is " + eacIntToString(size) +
       " bytes, but the size indicated at the beginning of the "
       "downlinked EventByEventDspParams data is only " +
       eacIntToString(supposedSize) + " bytes.");
  }


  // Allocate a EventByEventDspParams object on the heap:
  EventByEventDspParams& p = *checkHeap(new EventByEventDspParams);
  p.setFromParamDump();
  
  // Restore EventByEventDspParams parameters from the raw data:
  p.setCameraId(eacGetUint16FromTelemetry(data) + 1);
  p.recordAllEvents(MitBool(!eacGetUint16FromTelemetry(data)));

  p.update();
  return &p;
}


//-----------------------------------------------------------------------------
// _compare(): protected virtual method inherited from ObsParamItem
//-----------------------------------------------------------------------------

eacDefine_compare(EventByEventMode)
eacDefine_compare(EventByEventDspParams)


//-----------------------------------------------------------------------------
// compareNv(): method
//-----------------------------------------------------------------------------

// Warning: heavy use of macros follows.

method void
EventByEventMode::compareNv(const EventByEventMode& item, ItemDiffs& diffs)
     const
{
  base::compareNv(item, diffs);
  eacCompareMember(intervalDuration);
  eacCompareClassMemberNv(dspParams);
}


method void
EventByEventDspParams::compareNv(const EventByEventDspParams& item,
				 ItemDiffs& diffs) const
{
  base::compareNv(item, diffs);
  eacCompareMember(cameraId);
  eacCompareMember(recordAllEvents);
}


//-----------------------------------------------------------------------------
// restore(): class procedure of EventByEventMode.  [TBD]
//-----------------------------------------------------------------------------

proc EventByEventModePtr
EventByEventMode::restore(const RWCString& name)
{
  AsmObsParamBlockPtr config = base::restore(name);
  if (!config) return 0;
  else return config->castToEventByEventMode();
}


proc EventByEventDspParamsPtr
EventByEventDspParams::restore(const RWCString& name)
{
  ObsParamItemPtr item = base::restore(name);
  if (!item) return 0;
  else return item->castToEventByEventDspParams();
}


//-----------------------------------------------------------------------------
// timeBetweenIntervalsInSeconds(): virtual method of EventByEventMode
// inherited from ObsParamBlock
//-----------------------------------------------------------------------------

method double
EventByEventMode::timeBetweenIntervalsInSeconds() const
{
  return intervalDuration() * eacSecondsPerMicrotick;
}


//-----------------------------------------------------------------------------
// Simple virtual methods inherited from ObsParamItem
//-----------------------------------------------------------------------------

//# Virtual method inherited from ObsParamItem.  See documentation for
//# ObsParamItem.

method void
EventByEventMode::print(ostream& s) const
{
  s << "#<EventByEventMode";
  _print(s);
  s << ">";
}


method void
EventByEventDspParams::print(ostream& s) const
{
  s << "#<EventByEventDspParams";
  _print(s);
  s << ">";
}


//# Virtual method inherited from ObsParamItem.  See documentation for
//# ObsParamItem.

method bool
EventByEventMode::vIsLegalFieldTag(const RWCString& tag) const
{
  return isLegalFieldTag(tag);
}


method bool
EventByEventDspParams::vIsLegalFieldTag(const RWCString& tag) const
{
  return isLegalFieldTag(tag);
}


//----------------------------------------------------------------------------
// Simple virtual methods inherited from ObsParamBlock
//----------------------------------------------------------------------------

method ScienceMode
EventByEventMode::scienceMode() const
{
  return ASM_MODE_EVENT;
}


method ObsParamItemPtr
EventByEventMode::auxDspLoadBlock() const
{
  return ObsParamItemPtr(_dspParams);
}


method IntelAddress
EventByEventMode::mainDspLoadBlockAddress() const
{
  // TODO: get rid of this attrocious hardwired address:
  return 0x0000d630;
}


//----------------------------------------------------------------------------
// Simple virtual methods inherited from AsmObsParamBlock
//----------------------------------------------------------------------------

method EventByEventModePtr
EventByEventMode::castToEventByEventMode()
{
  return this;
}


method ConstEventByEventModePtr
EventByEventMode::castToEventByEventMode() const
{
  return this;
}


//----------------------------------------------------------------------------
// Simple virtual methods inherited from ObsParamItem
//----------------------------------------------------------------------------

method EventByEventDspParamsPtr
EventByEventDspParams::castToEventByEventDspParams()
{
  return this;
}


method ConstEventByEventDspParamsPtr
EventByEventDspParams::castToEventByEventDspParams() const
{
  return this;
}
@


5.2
log
@Added support for the new SafetyTrigger feature of MultTimeSeriesMode.
@
text
@d3 1
a3 1
"$Id: EventByEventMode.C,v 1.10 1996/02/12 21:23:46 nessus Exp $";
d9 3
d13 5
a17 5
#include "EacGlobalData.h"
#include "EacParser.h"
#include "EdsMemoryBlockImage.h"
#include "EventByEventMode.h"
#include "da_sugar.h"
d21 7
a27 7
//#############################################################################
//#####                                                                   #####
//#####        EventByEventMode: subclass of AsmObsParamBlock             #####
//#####                                                                   #####
//#####        EventByEventDspParams: subclass of ObsParamItem            #####
//#####                                                                   #####
//#############################################################################
d36 3
d44 5
d54 4
a57 5
// Description:
// [TBD] A new EventByEventMode object is not in a consistent state.
// In order to make the object consistent, the client must use all
// manditory setter methods (and may use some of the optional setter
// methods) and then invoke the update() method.
d59 2
a60 1
ctor EventByEventMode::EventByEventMode()
d69 2
a70 1
ctor EventByEventDspParams::EventByEventDspParams()
d81 2
a82 2
ObsParamBlock::ConfigId
method EventByEventMode::configId() const
d94 1
a94 2
// Description:
// [TBD]
d96 2
a97 2
void
method EventByEventMode::_print(ostream& s) const
d110 2
a111 2
void
method EventByEventDspParams::_print(ostream& s) const
d127 1
a127 2
// Description:
// [TBD] See documentation for ObsParamItem.
d129 1
a129 1
void		
d146 1
a146 1
void		
d180 1
a180 1
  image.appendAuxDspLoadBlockHeader(4);
d194 1
a194 1
  return base::_sizeOfEdsImage() + 4;
d201 1
a201 1
  return EdsMemoryBlockImage::sizeOfAuxDspLoadBlockHeader + 4;
d209 8
a216 9
// Description: [TBD] An EventByEventMode object will not be in a
// valid state until all of the non-defaulting setter methods have
// been called and the update method is invoked.  If at a later point,
// one or more setter methods are used, the update method must be
// again invoked to return the EventByEventMode object to a valid
// state.  This method is needed because there are instance variable
// that are calculated based on the values of the settable instance
// variables.  These calculated instance variables are not updated
// until the update() method is invoked.
d220 2
a221 2
void
method EventByEventMode::update()
d234 2
a235 2
void
method EventByEventDspParams::update()
d248 1
a248 2
// Description:
// [TBD] See documentation for ObsParamItem.
d250 2
a251 2
bool
proc EventByEventMode::isLegalFieldTag(const RWCString& tag)
d261 2
a262 2
bool
proc EventByEventDspParams::isLegalFieldTag(const RWCString& tag)
d276 1
a276 2
// Description:
// [TBD] See documentation for ObsParamItem.
d278 2
a279 2
void
method EventByEventMode::restoreFromParser(const EacParser& parser)
d313 2
a314 2
void
method EventByEventDspParams::restoreFromParser(const EacParser& parser)
d344 138
d485 2
a486 2
EventByEventModePtr
proc EventByEventMode::restore(const RWCString& name)
d494 2
a495 2
EventByEventDspParamsPtr
proc EventByEventDspParams::restore(const RWCString& name)
d519 2
a520 3
// Description:
// Virtual method inherited from ObsParamItem.  See documentation for
// ObsParamItem.
d522 2
a523 2
void
method EventByEventMode::print(ostream& s) const
d531 2
a532 2
void
method EventByEventDspParams::print(ostream& s) const
d540 2
a541 3
// Description:
// Virtual method inherited from ObsParamItem.  See documentation for
// ObsParamItem.
d543 2
a544 2
bool
method EventByEventMode::vIsLegalFieldTag(const RWCString& tag) const
d550 2
a551 2
bool
method EventByEventDspParams::vIsLegalFieldTag(const RWCString& tag) const
d561 2
a562 2
ScienceMode
method EventByEventMode::scienceMode() const
d571 1
a571 1
  return _dspParams;
a582 2


d587 2
a588 2
EventByEventModePtr
method EventByEventMode::castToEventByEventMode()
d594 2
a595 2
ConstEventByEventModePtr
method EventByEventMode::castToEventByEventMode() const
d605 2
a606 2
EventByEventDspParamsPtr
method EventByEventDspParams::castToEventByEventDspParams()
d612 2
a613 2
ConstEventByEventDspParamsPtr
method EventByEventDspParams::castToEventByEventDspParams() const
@


5.1
log
@Bump version number
@
text
@d3 1
a3 1
"$Id: EventByEventMode.C,v 4.5 1995/10/24 20:37:50 nessus Exp nessus $";
d418 1
a418 1
SCIENCE_MODE
@


4.5
log
@Added support for propper unpacking of EventBurstCatcherMode partitions.
@
text
@d3 1
a3 1
"$Id: EventByEventMode.C,v 1.9 1995/10/24 20:31:19 nessus Exp nessus $";
@


4.4
log
@First pass at getting BinnedMode param dumps working.  They seem to work
great except for breakpoints, which don't make it down.
@
text
@d3 1
a3 1
"$Id: EventByEventMode.C,v 1.8 1995/09/26 15:42:26 nessus Exp nessus $";
d54 1
a54 1
  setMainDspLoadBlock("asmevent_dspblk");
d219 3
a221 3
  if (_dspParams)
    setAuxDspLoadBlock("eacGen" + eacCapitalize(_dspParams->name()));
  else setAttributeNotSetError(dspParamsName);
@


4.3
log
@Bug fixes for Build 4.0.1.
@
text
@d3 1
a3 1
"$Id: EventByEventMode.C,v 1.7 1995/01/12 00:54:11 nessus Exp nessus $";
d94 2
a95 1
  s << " dspParams:" << _dspParams->name();
@


4.2
log
@EdsMemory is now complete.  (Modulo the inevitable bugs and some
improvements that need to be made soon.)
@
text
@d3 1
a3 1
"$Id: EventByEventMode.C,v 1.6 1994/08/22 18:21:35 nessus Exp nessus $";
d354 12
@


4.1
log
@Bump version number
@
text
@d3 1
a3 1
"$Id: EventByEventMode.C,v 3.5 1994/08/22 18:26:12 nessus Exp nessus $";
d12 1
d151 43
d410 17
@


3.5
log
@BinnedMode parameter dump restores and operator==() now work.
@
text
@d3 1
a3 1
"$Id: EventByEventMode.C,v 1.6 1994/08/22 18:21:35 nessus Exp nessus $";
@


3.4
log
@We now support loading of individual config items on demand, and we
also have a stubbed EdsMemory object.
@
text
@d3 1
a3 1
"$Id: EventByEventMode.C,v 3.3 1994/05/24 16:24:46 nessus Exp nessus $";
a309 20
}


//-----------------------------------------------------------------------------
// PROCEDURE operator<< : [TBD].
//-----------------------------------------------------------------------------

ostream&
proc operator<<(ostream& s, const EventByEventMode& config)
{
  config.print(s);
  return s;
}


ostream&
proc operator<<(ostream& s, const EventByEventDspParams& config)
{
  config.print(s);
  return s;
@


3.3
log
@Fixes needed for impending Flight Software ROM burn.
@
text
@d3 1
a3 1
"$Id: EventByEventMode.C,v 1.4 1994/05/24 16:21:19 nessus Exp nessus $";
a27 4
// TODO: this is a kludge.  Fix it:
local int 		greatestId = 0;


d64 14
a169 6

  // TODO: this is a kludge.  Fix it:
  if (!isConfigIdSet()) setConfigId(ConfigId(ConfigId::findFree,
					     ConfigId::asmSubsys,
					     ConfigId::eventByEventMode,
					     greatestId));
@


3.2
log
@Fixes to bugs in the original Build 3 submission.
@
text
@d3 1
a3 1
"$Id: EventByEventMode.C,v 1.3 1994/05/17 19:34:46 nessus Exp nessus $";
a55 5
  // TODO: this is a kludge.  Fix it:
  setConfigId(ConfigId(ConfigId::asmSubsys,
		       ConfigId::eventByEventMode,
		       ++greatestId));

d160 7
@


3.1
log
@Bump version number
@
text
@d3 1
a3 1
"$Id: EventByEventMode.C,v 1.1 1994/05/02 07:19:39 nessus Exp nessus $";
d165 6
a170 7
  
  // Make sure that the DSP Params attribute has been set:
  if (!_dspParams) setAttributeNotSetError(dspParamsName);

  // Set the name of the Auxilliary DSP Load Block appropriately according to
  // the DSP Params attribute:
  setAuxDspLoadBlock("eacGen" + eacCapitalize(_dspParams->name()));
d268 1
a268 2
  if (unparsedField.isNull()) setFieldMissingError(fieldName);
  else {
@


1.1
log
@Initial revision
@
text
@d3 1
a3 1
"$Id: EventByEventMode.C,v 1.1 1994/05/02 06:54:30 nessus Exp $";
@

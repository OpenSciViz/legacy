head	5.2;
access
	soccm
	nessus
	gromeo;
symbols
	Consistent-switch-to-solaris:5.2
	Consistent-after-restore-dump-mostly-finished:5.2
	Fixed-DR805:5.1
	Consistent-GenericTrigger2:5.1
	Consistent-GenericTrigger:5.1
	Build5_0_3:5.1
	Interim5_0_2-1:5.1
	Consistent-SafetyTrigger:5.1
	Current:5.2
	Build5_0_1:5.1
	RciPointers:4.1
	Build4_3_1:1.7
	Build4_3:1.7
	OldSOCTime:1.7
	Build4_2:1.7
	Build4_1:1.7
	BUILD:1.7
	Build4:1.7
	Build_4:1.7
	Build3_3:1.6
	Build3_2:1.5
	Build3_1:1.5
	Build3:1.3;
locks; strict;
comment	@ * @;


5.2
date	97.05.08.16.42.24;	author nessus;	state Exp;
branches;
next	5.1;

5.1
date	95.12.23.00.58.05;	author nessus;	state Exp;
branches;
next	4.1;

4.1
date	95.11.21.02.44.50;	author nessus;	state Exp;
branches;
next	1.7;

1.7
date	94.11.29.19.22.28;	author nessus;	state Submitted;
branches;
next	1.6;

1.6
date	94.10.18.23.06.35;	author nessus;	state Submitted;
branches;
next	1.5;

1.5
date	94.05.03.19.57.45;	author nessus;	state Exp;
branches;
next	1.4;

1.4
date	94.05.02.02.05.05;	author nessus;	state Exp;
branches;
next	1.3;

1.3
date	94.04.27.21.43.07;	author nessus;	state ForIntegration;
branches;
next	1.2;

1.2
date	94.04.22.14.34.55;	author nessus;	state Exp;
branches;
next	1.1;

1.1
date	94.04.20.16.43.32;	author nessus;	state Exp;
branches;
next	;


desc
@Bit twidling and range checking utilities.
@


5.2
log
@First pass at getting parameter dump restores to work for most modes.
Have not yet implemented restores for FftMode.
Also turned on referenced-counter pointers to ObsParamItems.
@
text
@// -*- Mode: C++; fill-column: 79; fill-prefix: "//# " -*-
#ifndef MitBitUtil_h
#define MitBitUtil_h

// $Id: MitBitUtil.h,v 1.1 1995/11/21 02:42:13 nessus Exp $

// Description : Generally useful simple procedures, classes, and constants
//               for MIT XTE software.
// Author      : Douglas Alan <nessus@@mit.edu>
// Copyright   : Massachusetts Institute of Technology, 1994

#include <mit_td.h>

// Constants:
const uint32		greatestUint32 = 0xFFFFFFFF;
const int		greatestUint24 = (1 << 24) - 1;
const int		greatestUint16 = (1 << 16) - 1;
const int		greatestUint15 = (1 << 15) - 1;
const int		greatestUint14 = (1 << 14) - 1;
const int		greatestUint13 = (1 << 13) - 1; 
const int		greatestUint8  = (1 << 8)  - 1;
const int		greatestUint7  = (1 << 7)  - 1;
const int		greatestUint4  = (1 << 4)  - 1;

const int		greatestInt32  = 0x7FFFFFFF;
const int		greatestInt16  = greatestUint15;
const int		greatestInt8   = greatestUint7;

const int		leastInt32     = -greatestInt32 - 1;
const int		leastInt16     = -greatestInt16 - 1;
const int		leastInt8      = -greatestInt8 - 1;

// Paranoid checking utilities:
inline MitBool		isValidUint32(double x)
                             { return MitBool(x >= 0 && x <= greatestUint32); }
inline MitBool		isValidUint24(int x)
                             { return MitBool(x >= 0 && x <= greatestUint24); }
inline MitBool 		isValidUint16(int x)
                             { return MitBool(x >= 0 && x <= greatestUint16); }

inline MitBool		isValidUint15(int x)
                             { return MitBool(x >= 0 && x <= greatestUint15); }
inline MitBool		isValidUint14(int x)
                             { return MitBool(x >= 0 && x <= greatestUint14); }
inline MitBool		isValidUint8(int x)
			     { return MitBool(x >= 0 && x <= greatestUint8); }
inline MitBool		isValidUint7(int x)
                             { return MitBool(x >= 0 && x <= greatestUint7); }
inline MitBool		isValidUint4(int x)
                             { return MitBool(x >=0 && x <= greatestUint4); }


inline MitBool      	isValidInt16(int x)
                             { return MitBool(x >= leastInt16 &&
			                      x <= greatestInt16);
			     }
inline MitBool		isValidInt8(int x)
                             { return MitBool(x >= leastInt8 &&
					      x <= greatestInt8);
			     }

// Bit twiddling utilities:
inline uint8		highByteOfUint16(uint16 x) { return x >> 8; }
inline uint8		lowByteOfUint16(int x) { return x & 0xFF; }
inline uint16		makeUint16(uint8 lowByte, uint8 highByte)
                             { return lowByte & highByte << 8; }

inline uint8		lowByteOfUint32(uint32 x) { return x & 0xFF; }
inline uint8		highByteOfUint32(uint32 x) { return x >> 24; }
inline uint8		lowMiddleByteOfUint32(uint32 x)
                             { return x >> 8 & 0xFF; }
inline uint8		highMiddleByteOfUint32(uint32 x)
                             { return x >> 16 & 0xFF; }
inline uint32		makeUint32(uint8 lowByte, uint8 lowMiddleByte,
				   uint8 highMiddleByte, uint8 highByte)
                             { return lowByte | lowMiddleByte << 8 |
			       highMiddleByte << 16 | highByte << 24;
			     }

#endif // MitBitUtil_h
@


5.1
log
@Bump version number
@
text
@d5 1
a5 1
// $Id: MitBitUtil.h,v 4.1 1995/11/21 02:44:50 nessus Exp nessus $
@


4.1
log
@Bump version number
@
text
@d5 1
a5 1
// $Id: MitBitUtil.h,v 1.7 1994/11/29 19:22:28 nessus Submitted nessus $
@


1.7
log
@Added makeUint32() and some other procedures.
@
text
@d5 1
a5 1
// $Id: MitBitUtil.h,v 1.6 1994/10/18 23:06:35 nessus Exp $
@


1.6
log
@Added makeUint16().
@
text
@d5 1
a5 1
// $Id: MitBitUtil.h,v 1.5 1994/05/03 19:57:45 nessus Exp nessus $
d66 13
a78 1
                             { return lowByte & (highByte << 8); }
@


1.5
log
@Cleaned up definitions of "greatest" constants.
@
text
@d5 1
a5 1
// $Id: MitBitUtil.h,v 1.4 1994/05/02 02:05:05 nessus Exp nessus $
d65 2
@


1.4
log
@Added constants for the least int 16 and the least int 8.
@
text
@d5 1
a5 1
// $Id: MitBitUtil.h,v 1.3 1994/04/27 21:43:07 nessus ForIntegration nessus $
d16 8
a23 7
const int		greatestUint24 = 0xFFFFFF;
const int		greatestUint16 = 0xFFFF;
const int		greatestUint15 = 0x7FFF;
const int		greatestUint14 = 0x3FFF;
const int		greatestUint8  = 0xFF;
const int		greatestUint7  = 0x7F;
const int		greatestUint4  = 0xF;
@


1.3
log
@Added some range constants.
@
text
@d5 1
a5 1
// $Id: MitBitUtil.h,v 1.2 1994/04/22 14:34:55 nessus Exp nessus $
d28 3
d50 2
d53 1
a53 1
                             { return MitBool(x >= -greatestInt16 - 1 &&
d57 1
a57 1
                             { return MitBool(x >= -greatestInt8 - 1 &&
@


1.2
log
@Changed EdsBool to MitBool and added include of mit_td.h
@
text
@d5 1
a5 1
// $Id: MitBitUtil.h,v 1.1 1994/04/20 16:43:32 nessus Exp nessus $
d14 15
d30 2
d33 1
a33 1
                             { return MitBool(x >=0 && x < 16 * 1024 * 1024); }
d35 1
a35 1
                             { return MitBool(x >= 0 && x < 64 * 1024); }
a36 4
inline MitBool      	isValidInt16(int x)
			     { return MitBool(x < 32 * 1024 &&
					      x >= -32 * 1024);
			     }
d38 1
a38 2
                             { return MitBool(x >= 0 && x < 32 * 1024); }

d40 1
a40 1
                             { return MitBool(x >= 0 && x < 16384); }
d42 1
a42 3
			     { return MitBool(x >= 0 && x < 256); }
inline MitBool		isValidInt8(int x)
                             { return MitBool(x >= -128 && x < 128); }
d44 1
a44 1
                             { return MitBool(x >= 0 && x < 128); }
d46 9
a54 1
                             { return MitBool(x >=0 && x < 16); }
@


1.1
log
@Initial revision
@
text
@d2 2
a3 1
// $Id: MitUtil.h,v 2.0 1994/04/11 19:44:07 nessus Exp nessus $
d5 2
d12 1
a12 2
#ifndef MitBitUtil_h
#define MitBitUtil_h
d15 4
a18 4
inline EdsBool		isValidUint24(int x)
                             { return EdsBool(x >=0 && x < 16 * 1024 * 1024); }
inline EdsBool 		isValidUint16(int x)
                             { return EdsBool(x >= 0 && x < 64 * 1024); }
d20 2
a21 2
inline EdsBool      	isValidInt16(int x)
			     { return EdsBool(x < 32 * 1024 &&
d24 2
a25 2
inline EdsBool		isValidUint15(int x)
                             { return EdsBool(x >= 0 && x < 32 * 1024); }
d27 10
a36 10
inline EdsBool		isValidUint14(int x)
                             { return EdsBool(x >= 0 && x < 16384); }
inline EdsBool		isValidUint8(int x)
			     { return EdsBool(x >= 0 && x < 256); }
inline EdsBool		isValidInt8(int x)
                             { return EdsBool(x >= -128 && x < 128); }
inline EdsBool		isValidUint7(int x)
                             { return EdsBool(x >= 0 && x < 128); }
inline EdsBool		isValidUint4(int x)
                             { return EdsBool(x >=0 && x < 16); }
@

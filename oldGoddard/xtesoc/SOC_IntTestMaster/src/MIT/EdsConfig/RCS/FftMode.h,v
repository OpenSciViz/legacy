head	5.3;
access
	soccm
	nessus
	gromeo;
symbols
	Consistent-switch-to-solaris:5.3
	Consistent-after-restore-dump-mostly-finished:5.3
	Fixed-DR805:5.2
	Consistent-GenericTrigger2:5.2
	Consistent-GenericTrigger:5.2
	Build5_0_3:5.2
	Interim5_0_2-1:5.2
	Consistent-SafetyTrigger:5.2
	Build5_0_1:5.1
	RciPointers:4.4
	Fixed-DR650:4.4
	Fixed-DR502:4.4
	Build4_3_1:4.4
	Build4_3:4.4
	Build4_2:4.4
	Consistent-post4-mod2:4.2
	Consistent-post4-mod1:4.1
	Build4:4.3
	Build3_3:3.5
	Consistent-no-template-instantiation:3.5
	Consistent-bm-param-dumps:3.5
	Build3_2:3.4
	Consistent-fix-for-RW6:3.4
	Build3_1:3.4
	Consistent-first-newdb:3.4
	C-structs-beta2:3.3
	Build3:3.2
	Current:5.3
	C-structs-beta:2.4
	Build2:2.3;
locks; strict;
comment	@ * @;


5.3
date	97.05.08.16.42.22;	author nessus;	state Exp;
branches;
next	5.2;

5.2
date	96.02.12.21.26.05;	author nessus;	state Exp;
branches;
next	5.1;

5.1
date	95.12.23.00.58.00;	author nessus;	state Exp;
branches;
next	4.4;

4.4
date	95.03.27.21.26.51;	author nessus;	state Exp;
branches;
next	4.3;

4.3
date	95.01.12.00.59.55;	author nessus;	state Exp;
branches;
next	4.2;

4.2
date	95.01.03.18.21.12;	author nessus;	state Exp;
branches;
next	4.1;

4.1
date	94.12.01.14.20.21;	author nessus;	state Exp;
branches;
next	3.5;

3.5
date	94.08.22.18.26.12;	author nessus;	state Exp;
branches;
next	3.4;

3.4
date	94.06.24.01.40.33;	author nessus;	state Exp;
branches;
next	3.3;

3.3
date	94.05.20.20.53.06;	author nessus;	state Exp;
branches;
next	3.2;

3.2
date	94.05.17.19.45.46;	author nessus;	state Exp;
branches;
next	3.1;

3.1
date	94.05.02.07.08.57;	author nessus;	state Exp;
branches;
next	2.5;

2.5
date	94.05.02.07.04.35;	author nessus;	state Exp;
branches;
next	2.4;

2.4
date	94.01.19.23.00.54;	author nessus;	state Exp;
branches;
next	2.3;

2.3
date	93.11.30.22.32.50;	author nessus;	state Release;
branches;
next	2.2;

2.2
date	93.11.02.00.34.15;	author nessus;	state Exp;
branches;
next	2.1;

2.1
date	93.10.27.17.56.18;	author nessus;	state Exp;
branches;
next	1.1;

1.1
date	93.10.15.16.02.38;	author nessus;	state Exp;
branches;
next	;


desc
@@


5.3
log
@First pass at getting parameter dump restores to work for most modes.
Have not yet implemented restores for FftMode.
Also turned on referenced-counter pointers to ObsParamItems.
@
text
@// -*- Mode: C++; fill-column: 73; fill-prefix: "//# " -*-
#ifndef FftMode_H
#define FftMode_H

#define FftModeRcsId_H \
"$Id: FftMode.h,v 1.14 1996/02/12 21:23:46 nessus Exp $"

// Description : FftMode - a class to model a Flight Software EA mode
// Author      : Douglas Alan <nessus@@mit.edu>
// Copyright   : Massachusetts Institute of Technology, 1993

// .FILE FftMode.h
// .FILE FftMode.C
// .OPTIONS -e -h

#include <MitBitUtil.h>
#include "EacGlobalData.h"
#include "PcaObsParamBlock.h"

//*****************************************************************************
//*****                                                                   *****
//*****          FftMode: subclass of PcaObsParamBlock                    *****
//*****                                                                   *****
//*****************************************************************************

class FftMode: public PcaObsParamBlock
{
  //* Instance variables:
  FftDspParamsPtr	_dspParams;
  uint32		_readOutTime;
  uint32		_processingInterval;
  int32			_transferInterval;
  uint16		_productInterval;
  MitBoolc		_isReadOutTimeSet;
  MitBoolc		_isConsistent;

  //^ _readOutTime, _processingInterval, and _transferInterval are in
  //^ microticks.  _productInterval is in number of
  //^ acumulation/processing intervals.  _transferInterval must be
  //^ greater than 0.

  //* Private methods:
  uint32		accumulationIntervalFs() const;
  uint32		processingIntervalFs() const;
  int32			transferIntervalFs() const;
  uint16		productIntervalFs() const;
  uint16		wordsPerDataSetFs() const;

  //* Private class procedures:
  static uint16		wordsPerDataSet(int productInterval);

protected:
  //* Protected nonvirtual methods:
  void			_print(ostream& s) const;

public:
   
  typedef PcaObsParamBlock
    			base;

  //* Constructors, etc:
  FftMode();
  static FftModePtr	restore(const RWCString& name);

  //* Class procedures:
  static MitBool	isLegalFieldTag(RWCString tag);
  
  //* Accessor methods:
  FftDspParamsPtr	dspParams() const;
  uint32		readOutTime() const;
  uint32		processingInterval() const;

  //* Setter methods:
  void			setDspParams(FftDspParamsPtr);
  void		      	setReadOutTime(uint32 microticks);
  void			setProcessingInterval(uint32 microticks);

  //* Quasi-accessor methods:
  uint32		accumulationInterval() const;
  int32			transferInterval() const;
  uint16		productInterval() const;
  uint16		wordsPerDataSet() const;

  //* Virtual methods inherited from ObsParamItem:
  void			makeEdsImage(EdsMemoryBlockImage& image,
				     EdsMemoryBlock& block) const;
  void			print(ostream& s) const;
  void			printCstruct(ostream& s) const;
  void			restoreFromParser(const EacParser& parser);
  int			sizeOfEdsImage() const;
  void			update();
  MitBool		vIsLegalFieldTag(const RWCString& tag) const;

  //* Virtual methods inherited from ObsParamBlock:
  ObsParamItemPtr	auxDspLoadBlock() const;
  virtual ConfigId	configId() const;
  IntelAddress		mainDspLoadBlockAddress() const;
  ScienceMode		scienceMode() const;
  double		timeBetweenIntervalsInSeconds() const;

  //* Virtual methods inherited from PcaObsParamBlock:
  FftModePtr		castToFftMode();
  ConstFftModePtr	castToFftMode() const;
};


//*****************************************************************************
//*****                                                                   *****
//*****               FftDspParams: subclass of ObsParamItem              *****
//*****                                                                   *****
//*****************************************************************************

class FftDspParams: public ObsParamItem
{
  //
  /* Instance variables: */
  uint16		_inputPreScale;
  int8			_outputShift; 
  //
  // _outputShift must be >= -1 and <= 14.

  //
  /* Private class constants: */
  static const RWCString inputPreScaleFieldName;
  static const RWCString outputShiftFieldName;

protected:
  //
  /* Private methods: */
  uint16		inputPreScaleFs() const;
  uint8			outputShiftFs() const;
  //
  /* Protected nonvirtual methods: */
  void			_print(ostream& s) const;

public:
   
  typedef ObsParamItem	base;

  //* Constructors, etc:
  FftDspParams();
  static
    FftDspParamsPtr	restore(const RWCString& name);

  //* Class procedures:
  static MitBool	isLegalFieldTag(const RWCString& tag);
  
  //* Accessor methods:
  uint16		inputPreScale() const;
  int8			outputShift() const;

  //* Setter methods:
  void			setInputPreScale(int);
  void			setOutputShift(int);

  //* Virtual methods inherited from ObsParamItem:
  FftDspParamsPtr       castToFftDspParams();
  ConstFftDspParamsPtr  castToFftDspParams() const;	
  void			makeEdsImage(EdsMemoryBlockImage& image,
				     EdsMemoryBlock& block) const;
  void			print(ostream& s) const;
  void			printCstruct(ostream& s) const;
  void			restoreFromParser(const EacParser& parser);
  int			sizeOfEdsImage() const;
  void			update();
  MitBool		vIsLegalFieldTag(const RWCString& tag) const;
};

//-----------------------------------------------------------------------------
// wordsPerDataSet(): class procedure of FftMode
//-----------------------------------------------------------------------------

//# Given a product interval, this procedure will return the number of
//# 16-bit words in a data set.

inline uint16
FftMode::wordsPerDataSet(int productInterval)
{
  return 1032 + 2 * productInterval;
}


//-----------------------------------------------------------------------------
// Accessor methods
//-----------------------------------------------------------------------------


inline FftDspParamsPtr
FftMode::dspParams() const
{
  assert(_dspParams);
  return _dspParams;
}

inline uint32
FftMode::readOutTime() const
{
  return _readOutTime;
}

inline uint32
FftMode::processingInterval() const
{
  return _processingInterval;
}

inline int32
FftMode::transferInterval() const
{
  assert(_isConsistent);
  assert(_transferInterval >= 0);
  return _transferInterval;
}

inline uint16
FftMode::productInterval() const
{
  assert(_isConsistent);
  return _productInterval;
}

inline uint16
FftDspParams::inputPreScale() const
{
  return _inputPreScale;
}

inline int8
FftDspParams::outputShift() const
{
  return _outputShift;
}

//-----------------------------------------------------------------------------
// Setter methods
//-----------------------------------------------------------------------------


//# Defaults to 0.

inline void
FftMode::setDspParams(FftDspParamsPtr value)
{
  _dspParams = value;
}


//# This attribute is required.

inline void
FftMode::setReadOutTime(uint32 microticks)
{
  _isConsistent = mitFalse;
  _readOutTime = microticks;
  _isReadOutTimeSet = mitTrue;
}


//# Defaults to 10k.

inline void
FftMode::setProcessingInterval(uint32 microticks)
{
  _isConsistent = mitFalse;
  _processingInterval = microticks;
}


inline void
FftDspParams::setInputPreScale(int scale)
{
  if (isValidUint16(scale)) _inputPreScale = scale;
  else setRangeError(inputPreScaleFieldName, 0, greatestUint16);
}

inline void
FftDspParams::setOutputShift(int shift)
{

  if (shift >= -1 && shift <= 14) _outputShift = shift;
  else setRangeError(outputShiftFieldName, -1, 14);
}

// TODO: setTimeBinDuration needs to become virtual so that inconsistency
// flag can be set.  (Actually shouldn't this be done some other way?
// For example, having a consistency flag in the base flag that needs to
// be set by update.)


//-----------------------------------------------------------------------------
// Quasi-accessor methods
//-----------------------------------------------------------------------------

inline uint32
FftMode::accumulationInterval() const
{
  // The accumulation interval is 256 times the time bin duration, unless this
  // product is less that 4 milliseconds, in which case the accumulation
  // interval is 4ms:
  uint32 result = timeBinDuration() * 256;
  if (result < msToMicroticks(4)) result = msToMicroticks(4);
  return result;
}

inline uint16
FftMode::wordsPerDataSet() const
{
  uint16 result = wordsPerDataSet(productInterval());
  assert(result <= 16 * 1024);
  return result;
}


//-----------------------------------------------------------------------------
// Private methods
//-----------------------------------------------------------------------------

// The following methods (whose names end with "Fs") return the attribute
// in a form that is suitable for the Flight Software.  Sometimes this is
// no different from what is expected in the Ground Software, but
// sometimes there is an off-by-one difference, or other slight
// distiction.

inline uint32		
FftMode::accumulationIntervalFs() const
{
  return accumulationInterval();
}

inline uint32		
FftMode::processingIntervalFs() const
{
  return processingInterval();
}

inline int32		
FftMode::transferIntervalFs() const
{
  return transferInterval();
}

inline uint16		
FftMode::productIntervalFs() const
{
  return productInterval() - 1;
}

inline uint16		
FftMode::wordsPerDataSetFs() const
{
  return wordsPerDataSet();
}

inline uint16		
FftDspParams::inputPreScaleFs() const
{
  return inputPreScale();
}

inline uint8			
FftDspParams::outputShiftFs() const
{
  return outputShift() + 1;
}

#endif // FftMode_H
@


5.2
log
@Added support for the new SafetyTrigger feature of MultTimeSeriesMode.
@
text
@@


5.1
log
@Bump version number
@
text
@d6 1
a6 1
"$Id: FftMode.h,v 4.4 1995/03/27 21:26:51 nessus Exp nessus $"
d98 1
a98 1
  SCIENCE_MODE		scienceMode() const;
@


4.4
log
@Check in of a number of files for Build4.2.  These changes are mostly
minor bug fixes.
@
text
@d6 1
a6 1
"$Id: FftMode.h,v 1.13 1995/03/21 17:53:50 nessus Exp nessus $"
@


4.3
log
@Bug fixes for Build 4.0.1.
@
text
@d6 1
a6 1
"$Id: FftMode.h,v 1.12 1995/01/12 00:54:11 nessus Exp nessus $"
d124 2
a125 2
  static const RWCString inputPreScaleName;
  static const RWCString outputShiftName;
d273 1
a273 1
  else setRangeError(inputPreScaleName, 0, greatestUint16);
d281 1
a281 1
  else setRangeError(outputShiftName, -1, 14);
@


4.2
log
@EdsMemory is now complete.  (Modulo the inevitable bugs and some
improvements that need to be made soon.)
@
text
@d6 1
a6 1
"$Id: FftMode.h,v 1.11 1994/08/22 18:21:35 nessus Exp nessus $"
d99 1
a99 1
  //  double		timeBetweenIntervalsInSeconds() const; // TODO
@


4.1
log
@Bump version number
@
text
@d6 1
a6 1
"$Id: FftMode.h,v 3.5 1994/08/22 18:26:12 nessus Exp nessus $"
d85 2
d90 1
d95 1
d97 1
d157 4
d164 1
a166 2
  FftDspParamsPtr       castToFftDspParams();
  ConstFftDspParamsPtr  castToFftDspParams() const;	
@


3.5
log
@BinnedMode parameter dump restores and operator==() now work.
@
text
@d6 1
a6 1
"$Id: FftMode.h,v 1.11 1994/08/22 18:21:35 nessus Exp nessus $"
@


3.4
log
@We now support loading of individual config items on demand, and we
also have a stubbed EdsMemory object.
@
text
@d6 1
a6 1
"$Id: FftMode.h,v 3.3 1994/05/20 20:53:06 nessus Exp nessus $";
d8 1
a8 1
// Description : Declaration of the FftMode class.
d12 4
d20 5
a24 6
// .NAME FftMode - a class to model a Flight Software EA mode
// .LIBRARY EdsConfig
// .HEADER FFT Mode
// .INCLUDE FftMode.h
// .FILE FftMode.C
// .FILE FftMode.h
a25 7

//#############################################################################
//#####                                                                   #####
//#####          FftMode: subclass of PcaObsParamBlock                    #####
//#####                                                                   #####
//#############################################################################

d28 1
a28 2
  //
  /* Instance variables: */
a35 5
  //
  // _readOutTime, _processingInterval, and _transferInterval are in
  // microticks.  _productInterval is in number of
  // acumulation/processing intervals.  _transferInterval must be
  // greater than 0.
d37 6
a42 2
  //
  /* Private methods: */
d48 2
a49 2
  //
  /* Private class procedures: */
d53 1
a53 2
  //
  /* Protected nonvirtual methods: */
d61 1
a61 2
  //
  /* Constructors, etc: */
d65 1
a65 2
  //
  /* Class procedures: */
d68 1
a68 2
  //
  /* Accessor methods: */
d72 2
a73 2
  //
  /* Setter methods: */
d77 2
a78 2
  //
  /* Quasi-accessor methods: */
d83 2
a84 2
  //
  /* Virtual methods inherited from ObsParamItem: */
d90 2
a91 2
  //
  /* Virtual methods inherited from ObsParamBlock: */
d95 2
a96 2
  //
  /* Virtual methods inherited from PcaObsParamBlock: */
a100 1
ostream& operator<<(ostream& s, const FftMode& config);
d102 5
a107 6
//#############################################################################
//#####                                                                   #####
//#####               FftDspParams: subclass of ObsParamItem              #####
//#####                                                                   #####
//#############################################################################

d135 1
a135 2
  //
  /* Constructors, etc: */
d139 2
a140 2
  //
  /* Class procedures: */
d143 1
a143 2
  //
  /* Accessor methods: */
d146 2
a147 2
  //
  /* Setter methods: */
d150 2
a151 2
  //
  /* Virtual methods inherited from ObsParamItem: */
a160 3
ostream& operator<<(ostream& s, const FftDspParams& config);


d165 3
d230 3
d240 2
d250 3
d260 1
d309 6
@


3.3
log
@Fixes required for next ROM burn.
@
text
@d6 1
a6 1
"$Id: FftMode.h,v 1.9 1994/05/20 20:51:37 nessus Exp nessus $";
d102 1
@


3.2
log
@Fixes to bugs in the original Build 3 submission.
@
text
@d6 1
a6 1
"$Id: FftMode.h,v 1.8 1994/05/17 19:34:46 nessus Exp nessus $";
d34 1
a38 2
  uint16		_inputPreScale;
  int8			_outputShift; 
d45 1
a45 1
  // greater than 0.  _outputShift must be >= -1 and <= 14.
a53 2
  uint16		inputPreScaleFs() const;
  uint8			outputShiftFs() const;
d71 1
a71 1
  static FftModePtr	restore(RWCString name);  // TODO
d79 1
a81 2
  uint16		inputPreScale() const;
  int8			outputShift() const;
d84 1
a86 2
  void			setInputPreScale(int);
  void			setOutputShift(int);
d113 64
d192 8
d228 1
a228 1
FftMode::inputPreScale() const
d234 1
a234 1
FftMode::outputShift() const
d244 7
d266 1
a266 1
FftMode::setInputPreScale(int scale)
d268 2
a269 4
  _inputPreScale = scale;
  if (!isValidUint16(scale))
    setError(RWCString("inputPreScale must be ") +
	     eacValidUint16DescriptionString);
d273 1
a273 1
FftMode::setOutputShift(int shift)
d275 3
a277 3
  _outputShift = shift;
  if (shift < -1 || shift > 14)
    setError("outputShift must be between -1 and 14");
d281 3
a283 1
// flag can be set.
d345 1
a345 1
FftMode::inputPreScaleFs() const
d351 1
a351 1
FftMode::outputShiftFs() const
@


3.1
log
@Bump version number
@
text
@d6 1
a6 1
"$Id: FftMode.h,v 2.5 1994/05/02 07:04:35 nessus Exp nessus $";
d125 1
a125 1
  return 1034 + 2 * productInterval;
@


2.5
log
@Check in of files for Build 3.
@
text
@d6 1
a6 1
"$Id: FftMode.h,v 1.7 1994/05/02 06:56:14 nessus Exp nessus $";
@


2.4
log
@Check in of many files to support the 'make-structs' program, which will
be used to generate configurations in a form suitable for compiling
into the flight software.
@
text
@d1 3
a3 2
// -*- Mode: C++; fill-column: 79; fill-prefix: "//# " -*-
// $Id: FftMode.h,v 1.4 1994/01/14 20:59:43 nessus Exp nessus $
d5 4
a8 1
// Description : [TBD]
d12 2
a13 3
#ifndef FftMode_h
#define FftMode_h

d16 8
d25 3
a27 1
//# FftMode: subclass of PcaObsParamBlock
d30 17
a46 1
class FftMode: public PcaObsParamBlock {
d48 12
a59 6
  // Instance variables:
  int16				_accumulationInterval;
  int16				_processingInterval;
  uint32			_transferInterval;
  int16				_productInterval;
  int16				_wordsPerDataSet;
d61 5
d67 3
d71 4
a74 2
  // Static member fuctions:
  // static FftModePtr restore(RWCString name);
d76 3
a78 11
  // Accessor methods:
  int				accumulationInterval() const
                                     { return _accumulationInterval; }
  int  				processingInterval() const
                                     { return _processingInterval; }
  uint32			transferInterval() const
                                     { return _transferInterval; }
  int				productInterval() const
                                     { return _productInterval; }
  int				wordsPerDataSet() const
                                     { return _wordsPerDataSet; }
d80 34
a113 21
  // Setter methods:
  void				setAccumulationInterval(int microticks)
                                     { assert(microticks > 5000 &&
					      microticks <= 12800);
				       _accumulationInterval = microticks; }
  void				setProcessingInterval(int microticks)
                                     { assert(microticks > 0 &&
					      microticks <= 12000);
				       _processingInterval = microticks; }
  void				setTransferInterval(uint32 microticks)
                                     { _transferInterval = microticks; }
  void				setProductInterval(int accumulationIntervals)
                                    { assert(accumulationIntervals >= 0 &&
					     accumulationIntervals <= 3584);
				      _productInterval = accumulationIntervals;
				    }
  void				setWordsPerDataSet(int words)
  				     { assert(words >= 2052 &&
					      words <= 16384);
				       _wordsPerDataSet = words;
				     };
d115 1
a115 2
  // Methods inherited from ObsParamBlock:
  virtual SCIENCE_MODE		scienceMode() const { return PCA_MODE_FFT; }
a116 3
  // Methods inherited from PcaObsParamBlock:
  virtual FftModePtr		castToFftMode() { return this; }
};
d118 165
a282 1
#endif // FftMode_h
@


2.3
log
@Added or modified rcs id header.
@
text
@d2 1
a2 1
// $Id$
d66 1
a66 1
  virtual SCIENCE_MODE		scienceMode() { return PCA_MODE_FFT; }
@


2.2
log
@Tried to make sure all of the cast methods are implemented correctly,
fixed ScienceMode() method to return the standard SCIENCE_MODE enum,
and added stub code for Royce to use.  This required the creation of
some global data.
@
text
@d1 7
@


2.1
log
@Only bumped revision number up to 2.1.
@
text
@d48 4
a51 4
                                     { assert(accumulationIntervals >= 0 &&
					      accumulationIntervals <= 3584);
				       _productInterval = accumulationIntervals;
				     }
d57 6
@


1.1
log
@Initial revision
@
text
@@

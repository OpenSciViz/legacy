head	5.1;
access
	gromeo;
symbols
	Consistent-switch-to-solaris:5.1;
locks; strict;
comment	@ * @;


5.1
date	98.05.27.20.14.09;	author nessus;	state Exp;
branches;
next	;


desc
@@


5.1
log
@Makefile: Checked all documentation into RCS (even binary files) and
modified Makefile accordingly.  Also fixed a typo involving
SOCMASTER_INC and SOCMASTER_LIB.

RotationPlan.C & RotationPlan.h: These files aren't actually used by
anything in EdsConfig.  They were written for Wei Cui's rotation
planner.  They are being checked in here because I wrote them, and I
can't think of a better place to put them.

.cvsignore: Tells CVS which files it shouldn't bug you about everytime
you do a CVS update.
@
text
@//==========================================================================
// RotationPlan.C
// RCS Stamp: $Id: RotationPlan.C,v 2.0 1993/10/13 18:17:12 buehler Exp $
// Description:
// Contains code to support the configuration parameters for ASM Position
// Histogram Mode, and the subset of those parameters specifying the
// dwell sequence ("Rotation Plan") for the All Sky Monitor.
// These parameters are encapsulated in the classes RotationPlan and
// PosHistParams.
//
// Author: Royce Buehler - buehler@@space.mit.edu
// Copyright: Massachusetts Institute of Technology
//===========================================================================

#include "RotationPlan.h"
#include "mem_map.h"
#include <memory.h>     // For byte-by-byte copies without conversions

RotationPlan RotationPlan::defaultPlan = RotationPlan(-1280, 0, 100, 12, 90);
UINT16 PosHistParams::defltWordsInPartition = 18432;

ostream& operator<<( ostream& out , RotationPlan& aPlan)
{
  out << "Rotation Plan: Turn " << (aPlan.firstSlewInSteps() / 2) 
      << " deg, then " << (aPlan.firstSlewInSteps() / 2) << " deg. ";
  out << aPlan.dwellPeriods() << " times dwell for " ;
  out << aPlan.majorTicksInDwell() << " seconds and turn " ;
  out << (aPlan.rotationSteps() / 2) << " deg." << endl;

  return(out);
}

void toPktData(unsigned char*, UINT16);
void toPktData(unsigned char*, INT16);

// Null constructor copies values from the static member defaultPlan
RotationPlan::RotationPlan()
{
  data = defaultPlan.data;
}

RotationPlan::RotationPlan(int slew1, int slew2, UINT16 dwellTime, 
			   int slewStep, UINT16 dwellCount)
{
  data.firstSlew = slew1;
  data.secondSlew = slew2;
  data.dwellPeriodCount = dwellCount;
  data.ticksInDwell = dwellTime;
  data.subsequentSlew = slewStep;
}

const unsigned char* RotationPlan::rawData(void) const
{
  return( (unsigned char *) &data);
}

int RotationPlan::getCommandPacketData(unsigned char* buf) const
{
  unsigned char* nextTwoPtr = buf;
  toPktData(nextTwoPtr, data.firstSlew);
  nextTwoPtr += 2;
  toPktData(nextTwoPtr, data.secondSlew);
  nextTwoPtr += 2;
  toPktData(nextTwoPtr, data.dwellPeriodCount);
  nextTwoPtr +=2;
  toPktData(nextTwoPtr, data.ticksInDwell);
  nextTwoPtr += 2;
  toPktData(nextTwoPtr, data.subsequentSlew);

  return( 10);
}

IntelAddress RotationPlan::where(void) const
{
  return ( IntelAddress(MMAP_PRAM_COPY + (2 * rotPlanWordOffset)));
	   
}


//************************************************************
// Member functions for class PosHistParams
//************************************************************

 PosHistParams::  PosHistParams( )
{
  thePlan = new RotationPlan();
  memcpy( (void*) &data, (void*) &(thePlan->data), 
	   sizeof(RotationPlan::rotPlanRawData));
  data.wordsInPartition = defltWordsInPartition;
} 


 PosHistParams::  PosHistParams( UINT16 count, 
		         RotationPlan* plan = &RotationPlan::defaultPlan)
: thePlan(plan)
{
  memcpy( (void*) &data, (void*) &(plan->data), 
	   sizeof(RotationPlan::rotPlanRawData));
  if (count == 0)
  {
    count = defltWordsInPartition;
  }
  data.wordsInPartition = count;
}


   PosHistParams::~PosHistParams( ) 
{
  delete thePlan;
}

int PosHistParams::getCommandPacketData(unsigned char* buf) const
{
  unsigned char* nextTwoPtr = buf;
  toPktData(nextTwoPtr, data.firstSlew);
  nextTwoPtr += 2;
  toPktData(nextTwoPtr, data.secondSlew);
  nextTwoPtr += 2;
  toPktData(nextTwoPtr, data.dwellPeriodCount);
  nextTwoPtr +=2;
  toPktData(nextTwoPtr, data.ticksInDwell);
  nextTwoPtr += 2;
  toPktData(nextTwoPtr, data.subsequentSlew);
  nextTwoPtr += 2;
  toPktData(nextTwoPtr, data.wordsInPartition);

  return( 12);
}

// Overrides ConfigChunk where() method, since PosHistParams always load into
// the same place.

IntelAddress PosHistParams:: where(void) const
{
  return ( IntelAddress(MMAP_PRAM_COPY + (2 * rotPlanWordOffset)));
}

  const RotationPlan* PosHistParams::  rotationPlan( void) const
{
  return(thePlan);
}


  void PosHistParams::   setWordsInHistogram( UINT16 count) 
{ 
  data.wordsInPartition = count;
}


  void PosHistParams::   setRotationPlan(  RotationPlan * aPlan)
{
  thePlan = aPlan;
  memcpy( (void*) &data, (void*) &(aPlan->data), 
	   sizeof(RotationPlan::rotPlanRawData));
}

void toPktData(unsigned char* dest, UINT16 data)
{ 
  *dest = (unsigned char) ((data & 0xFF00) >> 8);
  dest++;
  *dest = (unsigned char) (data & 0x00FF);
}

void toPktData(unsigned char* dest, INT16 data)
{
  *dest = (unsigned char) (((UINT16) data & (UINT16) 0xFF00) >> 8);
  dest++;
  *dest = (unsigned char) (data & 0x00FF);
}


@

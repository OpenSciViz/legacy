head	5.2;
access
	soccm
	nessus
	gromeo;
symbols
	Consistent-switch-to-solaris:5.2
	Consistent-after-restore-dump-mostly-finished:5.2
	Fixed-DR805:5.1
	Consistent-GenericTrigger2:5.1
	Consistent-GenericTrigger:5.1
	Build5_0_3:5.1
	Interim5_0_2-1:5.1
	Consistent-SafetyTrigger:5.1
	Current:5.2
	Build5_0_1:5.1
	RciPointers:4.1
	Fixed-DR650:1.2;
locks; strict;
comment	@ * @;


5.2
date	97.05.08.16.42.37;	author nessus;	state Exp;
branches;
next	5.1;

5.1
date	95.12.23.00.58.34;	author nessus;	state Exp;
branches;
next	4.1;

4.1
date	95.11.21.02.45.01;	author nessus;	state Exp;
branches;
next	1.2;

1.2
date	95.09.19.17.53.32;	author nessus;	state Exp;
branches;
next	1.1;

1.1
date	95.09.14.22.46.27;	author nessus;	state Exp;
branches;
next	;


desc
@Problem - classes to facilitate error handling w/o exceptions.
@


5.2
log
@First pass at getting parameter dump restores to work for most modes.
Have not yet implemented restores for FftMode.
Also turned on referenced-counter pointers to ObsParamItems.
@
text
@// -*- Mode: C++; fill-column: 73; fill-prefix: "//# " -*-
static const char rcsid[] = 
"$Id: Problem.C,v 1.4 1997/05/08 00:42:04 nessus Exp $";

// .name      Problem - classes to facilitate error handling w/o exceptions
// .author    Douglas Alan
// .copyright Massachusetts Institute of Technology, 1995
// .version   $Revision: 1.4 $
// .library   EdsConfigUtil
// .header    Problem

// See the file EdsConfigCodeStyle.txt for notes on the code style used here.

// [TBD] A Problem that is copied does not result in a copied
// ProblemHandler.  This is because Problem is a handle class.  This rule
// does not apply for a null Problem.  When you copy a Problem that has
// no errors or warnings, you get a new null Problem.  The reason for
// this is efficiency concerns.

// TODO: We should also have a ConstProblem class because right now a
// const Problem can be easily copied to a non-const Problem, and then the
// ProblemHandler will be vulnerable to modification.

#include <MitStreamUtil.h>
#include "EacUtil.h"
#include "Problem.h"
#include "da_sugar.h"

local const char rcsid_H[] = Problem_H;

//*****************************************************************************
//*****                                                                   *****
//*****                     Problem Overview                              *****
//*****                                                                   *****
//*****************************************************************************

// .section Overview of the Problem Class

// The Problem class was designed to aid in the handling of errors and
// warnings.  In particular, this class facilitates the communication of
// errors and warnings from a procedure to its caller.

// The Problem class was designed for use in a development environment
// that does not support exceptions.  If your compiler supports
// exceptions, then their use is much preferable to using the Problem
// class alone.  You may find that it is sometimes useful to use the
// Problem class along with exceptions (i.e., to have an exception return
// a Problem), since the Problem class allows a procedure to raise
// several errors and warnings at once, while with exceptions it is most
// convenient if only one error or warning needs to be reported.

// Perhaps the best way to introduce you to the Problem class is with an
// example.  Let's say that there is a procedure `massageFile(String
// filename)'.  It takes a filename as its arguments and opens the file
// for processing.  What will this procedure do if it turns out that the
// file cannot be opened?  

// The solution that the Problem class provides looks something like this:

//      void massageFile(String string, Problem& p = Problem::theDefault())
//      {
//        ...
//        if (couldNotOpenFile) {
//		p.setError("Could not open file.");
//              return;
//        }
//        ...
//      }

// The `Problem's setError() method is used to raise an error.  The
// caller might handle the error something like so:

//     Problem p;
//     massageFile(filename, p);
//     if (p) { if (p.isError()) {
//                 cerr << p.message();
//                 exit(1);
//              } else if (p.isWarning()) cerr << p.message();
//              p.clear();
//            }

// Since the above code probably won't make it into the manual page in a
// readable format, you might want to check the file Problem.C to see a
// readable version of this code.

// You may ask, ``Why is this approach any better than just returning a
// string to indicate an error?''  That is a good question.  `Problem'
// provides some features that a more typical approach would not.  First
// of all, with the Problem class an error that is raised must be handled
// and then cleared or the `Problem's destructor will abort the program.
// This helps to find the type of bug where a programmer forgets to check
// for errors.  Secondly, the `Problem' class is actually a handle class
// for another class called `ProblemHandler'.  A ProblemHandler is not
// actually created unless an error or warning is raised.  This allows
// the `Problem' class to be very space and time efficient in the typical
// case where no error or warning is raised.

// `Problem' has some other features that are also quite handy: (1)
// Multiple errors and warnings can be raised by a single call to a
// procedure.  (2) The name of the procedure or method and its subsystem
// and class can be registered.  This information will be used for
// outputting informative error and warning messages.  (3) Different
// handlers can be provided when constructing a `Problem'.  In this
// manner the caller can, if it wants, provide a custom handler that will
// automatically handle errors and warnings without intervention from the
// caller.  (4) `Problem' objects can be returned from a procedure as its
// return value (efficiently, without copying data), rather than passed
// down into a procedure and then modified.

// As an example of advantage #4, the previous code example could be
// rewritten like so:

//      Problem massageFile(String string)
//      {
//	  Problem p;
//        ...
//        if (couldNotOpenFile) {
//		p.setError("Could not open file.");
//              return p;
//	  }
//        ...
//      }

// The code in the caller might then look like this:

//     Problem p = massageFile(filename, p);
//     if (p) { handleProblem(p);
//              p.clear();
//            }

// Unfortunately, there are also two disadvantages associated with this
// approach: (1) If the caller decides not to handle the Problem and an
// error is raised, then the program won't be terminated until the scope
// in which the procedure is called ends.  (2) You may want to reserve
// the valuable return value of a procedure for returning data, rather
// than errors.

// When using the Problem class, it is important to keep in mind that the
// Problem class is a handle class.  When a Problem object is copied, you
// are only copying a pointer, and not what is pointed to.  You need to
// keep this in mind when you use a Problem object: when you copy a
// Problem object, only the handle is copied.  The ProblemHandler, which
// is what keeps track of the errors and warnings that have been raised,
// is not copied--it is shared between different copies of a Problem
// object.

// Another little complication needs to be kept in mind when you copy a
// ``null Problem''.  A null Problem is one none of whose `set' methods
// have been used.  When you copy a null Problem, unlike other `Problem's
// that you might copy, the copy will not behave like the original.  The
// reason for this is that a ProblemHandler is not made until a `set'
// method is used.  Thus if you copy a null Problem and then use a `set'
// method on both the copy and the original, the two different 'Problem's
// will not share the same ProblemHandler.  The excuse for all this
// subtlety is all in the name of efficiency.

// .end

// .section Problem Handlers

// If a user wishes to provide a custom handler, the way that the user
// does this to provide an argument to the Problem constructor.  This
// argument must be derived from ProblemHandler, and will determine how
// errors and warnings are to be handled.  The object passed in to the
// constructor must also be the ``protoype'' object for its class.
// Each subclass of ProblemHandler has a ``prototype'' object, which
// represents the case in which no errors or warnings have yet been
// raised.  By using a single prototype object for each class in the case
// in which an error has not been raised, we save the overhead of having
// to allocate any memory in the non-error case.  As soon as an error or
// warning is raised, the prototype object will arrange to construct a
// non-protoype ProblemHandler object of the appropriate subclass.  This
// non-prototype object will be allocated on the heap and will keep track
// of the errors and warnings that are raised.

// Three `ProblemHandler's have been provided here, and a user is
// welcome to define additional `ProblemHandler's if they wish.  The
// most typical ProblemHandler to use is `StandardProblemHandler'.  If
// a Problem is constructed using the default constructor, then the
// handler that is used is StandardProblemHandler.

// The next-most important ProblemHandler is `DefaultProblemHandler'.
// `DefaultProblemHandler' is the handler for a special Problem that is
// known as ``the default Problem'' and is accessed via the static method
// `Problem::theDefault()'.  This special Problem object was designed to
// be provided as the default `Problem' argument by a procedure.  Notice
// that in the code example given above, this is what is done.  Using
// this approach, if the caller knows that an error or warning will not
// occur, then the caller does not need to pass a Problem to the
// procedure.  If--despite what the caller had assumed--an error is
// raised, then the DefaultProblemHandler will output an error messge and
// abort the program immediately.  If a warning is raised, then a warning
// message will be output immediately to `cerr' and the ProblemHandler
// will consider the warning handled.

// An additional ProblemHandler, LoggingProblemHandler, has been
// provided for your convenience.  [TODO: This class has not yet been
// implemented.]  LoggingProblemHandler is derived from
// StandardProblemHandler and behaves just like
// StandardProblemHandler, except that all errors and warnings raised
// are also appended to a file.  The file used for logging is
// specified by a static method of the class.

// .end

// .section Defining Your Own Problem Handlers

// You are free to define your own additional ProblemHandlers, since
// many of the methods of ProblemHandler are virtual to allow for
// this.

// A new ProblemHandler is created on the heap by a Problem object at the
// first occasion when a `set' method is called on the Problem.  Until
// then the Problem only contains a pointer to a ``prototype'' object.

// If you define a new ProblemHandler subclass, you must also define a
// prototype object for the class, and this object must be made available
// to users of the Problem class.  A prototype ProblemHandler is merely
// one in which the protected non-virtual method
// ProblemHandler::setIsPrototype() has been called.  For each subclass
// of ProblemHandler that you define, you should make sure that there is
// exactly one such object.  It is not an error if there are more, but it
// is a waste space to have more.

// To create a new ProblemHandler, a Problem object calls the virtual
// _makeHandler() method on the prototype object.  You should override
// this virtual method to return a new object of the class you are
// defining.

// There are a number of other virtual methods that must be overridden
// and are documented below.

// .end

// .section Misc

// The following method documentation should be in the Summary section
// below, but does not appear because of a bug in genman++.

// Problem::Problem(const ProblemHandler& prototype) // default ctor

// Constructs a null Problem using `prototype' as its prototype.  It is
// a fatal bug if `prototype' is not a prototype ProblemHandler.  See
// the overview for more detail.

// .end

//-----------------------------------------------------------------------------
// File-scope constants
//-----------------------------------------------------------------------------

local const RWCString emptyString = "";

//-----------------------------------------------------------------------------
// Local procedures and macros used for terminating the program with an error
// message:
//-----------------------------------------------------------------------------


// #ifdef NDEBUG
// #   define DA_NDEBUG 1
// #else
// #   define DA_NDEBUG 0
// #endif // NDEBUG

// local proc void 
// lError1(const char* const message, const char* const filename,
//        const int line_number)
// {
//   cerr << "\nFATAL INTERNAL ERROR: " << message << '\n'
//        << "  File: " << filename << '\n'
//        << "  Line: " << line_number << '\n';
//   abort();
// }

// #define lError(message) \
//      lError1(message, __FILE__, __LINE__)


// local proc void
// lHeapError()
// {
//   cerr << "\nFATAL ERROR: Heap exhausted!\n";
//   abort();
// }


// template <class T> local proc inline T
// lCheckHeap(T arg)
// {
//   if (DA_NDEBUG && !arg) lHeapError();
//   return arg;
// }


//-----------------------------------------------------------------------------
// Class variables
//-----------------------------------------------------------------------------

const StandardProblemHandler
     StandardProblemHandler::_prototype(ProblemHandler::isProto);
const DefaultProblemHandler
     DefaultProblemHandler::_prototype(ProblemHandler::isProto);
Problem Problem::_theDefault(DefaultProblemHandler::prototype());

//-----------------------------------------------------------------------------
// reportedBy_message(): local procedure
//-----------------------------------------------------------------------------

local proc RWCString
reportedBy_message(const RWCString& subsysName,
		   const RWCString& className,
		   const RWCString& procName)
{
  bool s = subsysName.length() ? true : false;
  bool c = className.length() ? true : false;
  bool p = procName.length() ? true : false;

  static const RWCString reported = " reported";
  RWCString retval = reported;
  if (c || p) {
    retval += " by ";
    if (p) {
      if (c) retval += className + "::";
      retval += procName + "()";
    } else retval += "the " + className + " class";
  }
  if (s) retval += " in the " + subsysName + " subsystem";
  retval += ":\n\t";
  return retval;
}


//*****************************************************************************
//*****                                                                   *****
//*****               Problem: concrete handle class                      *****
//*****                        handle for ProblemHandler                  *****
//*****                                                                   *****
//*****************************************************************************

//-----------------------------------------------------------------------------
// default ctor of Problem
//-----------------------------------------------------------------------------

//# See the Misc Section for documentation on this method.  The reason
//# for this is a bug in genman++.

ctor
Problem::Problem(const ProblemHandler& prototype)
: _handler((ProblemHandler*) &prototype)
{
  assert(_handler->_isPrototype);
}


//-----------------------------------------------------------------------------
// copy ctor of Problem
//-----------------------------------------------------------------------------

//# Problem is a handle class, so you must be sure you understand what it
//# means to copy a Problem object.  See the overview about this.
//# Problem is a bit trickier than most handle classes.

ctor
Problem::Problem(const Problem& orig)
: _handler(orig._handler)
{
  _handler->incrementRefCount();
}


//-----------------------------------------------------------------------------
// operator=(): method of Problem
//-----------------------------------------------------------------------------

method Problem&
Problem::operator=(const Problem& orig)
{
  if (this != &orig) {
    orig._handler->incrementRefCount();
    free();
    _handler = orig._handler;
  }
  return *this;
}


//-----------------------------------------------------------------------------
// dtor of Problem
//-----------------------------------------------------------------------------

dtor
Problem::~Problem()
{
  free();
}


//-----------------------------------------------------------------------------
// theDefault(): class procedure of Problem
//-----------------------------------------------------------------------------

//# Returns ``the default Problem''.  See the overview for more detail.

method Problem&
Problem::theDefault()
{
  return _theDefault;
}


//-----------------------------------------------------------------------------
// free(): private method of Problem
//-----------------------------------------------------------------------------

// free() decrements the reference count, and if the count goes to zero,
// then it frees the ProblemHandler.  If an error was raised but not
// handled, then we abort.  Otherwise, if a warning was raised but not
// handled, we output a warning message to cerr.

method void
Problem::free()
{
  _handler->decrementRefCount();
  if (_handler->_refCount == 0) {
    assert(!_handler->_isPrototype);
    if (!hasProblemBeenHandled()) {
      if (isError()) {
	error("An error was raised, but not handled: " + message());
      } else if (isWarning()) daWarning(message());
    }
    delete _handler;
  }
}


//-----------------------------------------------------------------------------
// Simple methods of class Problem
//-----------------------------------------------------------------------------

//# Sets the name of the subsystem.  If an error or warning message is
//# generated, the subsystem name provided here will appear in the
//# message.

method void
Problem::setSubsystemName(const RWCString& subsystem)
{
  _handler = _handler->makeHandler();
  _handler->_subsystemName = subsystem;
}


//# Sets the name of the class.  If an error or warning message is
//# generated, the subsystem name provided here will appear in the
//# message.

method void
Problem::setClassName(const RWCString& className)
{
  _handler = _handler->makeHandler();
  _handler->_className = className;
}


//# Sets the name of the procedure or method.  If an error or warning
//# message is generated, the subsystem name provided here will appear in
//# the message.

method void
Problem::setProcedureName(const RWCString& procName)
{
  _handler = _handler->makeHandler();
  _handler->_procedureName = procName;
}


//# Equivalent to calling `setSubsystemName()', then `setClassName()',
//# and then `setProcedureName()'.

method void
Problem::setInfo(const RWCString& subsystemName,
		 const RWCString& className,
		 const RWCString& procName)
{
  _handler = _handler->makeHandler();
  _handler->_subsystemName = subsystemName;
  _handler->_className = className;
  _handler->_procedureName = procName;
}


//# Raises an error.  `message' should be a string describing the error.
//# This method can be called multiple times, in which case multiple
//# errors will be raised.  It is also permissable to raise both errors
//# and warnings in the same Problem object.

method void
Problem::setError(const RWCString& message)
{
  _handler = _handler->makeHandler();
  _handler->setError(message);
}


//# Raises a warning. `message' should be string describing the warning.
//# This method can be called multiple times, in which case multiple
//# warnings will be raised.  It is also permissable to raise both errors
//# and warnings in the same Problem object.

method void
Problem::setWarning(const RWCString& message)
{
  _handler = _handler->makeHandler();
  _handler->setWarning(message);
}


//*****************************************************************************
//*****                                                                   *****
//*****               ProblemHandler: abstract class                      *****
//*****                                                                   *****
//*****************************************************************************


//-----------------------------------------------------------------------------
// Constructor of ProblemHandler
//-----------------------------------------------------------------------------

//# Constructs a non-prototype ProblemHandler.  To make a ``prototype''
//# ProblemHandler, use this constructor, and then the setIsPrototype()
//# method.

ctor
ProblemHandler::ProblemHandler()
: _isPrototype(false),
  _refCount(1)
{}


//-----------------------------------------------------------------------------
// Destructor of ProblemHandler (virtual)
//-----------------------------------------------------------------------------

ProblemHandler::~ProblemHandler()
{}


//-----------------------------------------------------------------------------
// makeHandler(): private method of ProblemHandler
//-----------------------------------------------------------------------------

// If 'this' is a prototype object, then this method calls _makeHandler()
// and checks to make sure the heap wasn't exhausted.  Otherwise, it just
// returns 'this'.

method ProblemHandler*
ProblemHandler::makeHandler()
{
  if (_isPrototype) return checkHeap(_makeHandler());
  else return this;
}


//-----------------------------------------------------------------------------
// message(): private method of ProblemHandler
//-----------------------------------------------------------------------------

// If 'this' is a prototype object, then return the empty string.
// Otherwise, calls _message() and returns the result.

method RWCString
ProblemHandler::message()
{
  if (_isPrototype) return emptyString;
  else return _message();
}


//*****************************************************************************
//*****                                                                   *****
//*****      StandardProblemHandler: subclass of ProblemHandler           *****
//*****      DefaultProblemHandler:  subclass of ProblemHandler           *****
//*****                                                                   *****
//*****************************************************************************

//-----------------------------------------------------------------------------
// Constructor of StandardProblemHandler
//-----------------------------------------------------------------------------

//# Constructs a non-prototype StandardProblemHandler.

StandardProblemHandler::StandardProblemHandler()
: _wasProblemCleared(false)
{}


//# Constructs a prototype StandardProblemHandler.

StandardProblemHandler::StandardProblemHandler(ProtoFlag)
{
  setIsPrototype();
}


DefaultProblemHandler::DefaultProblemHandler()
{}


//# Constructs a prototype DefaultProblemHandler.

DefaultProblemHandler::DefaultProblemHandler(ProtoFlag)
{
  setIsPrototype();
}


//-----------------------------------------------------------------------------
// prototype(): class procedure
//-----------------------------------------------------------------------------

//# Returns the prototype StandardProblemHandler.

const StandardProblemHandler&
StandardProblemHandler::prototype()
{
  return _prototype;
}


//# Returns the prototype DefaultProblemHandler.

const DefaultProblemHandler&
DefaultProblemHandler::prototype()
{
  return _prototype;
}


//-----------------------------------------------------------------------------
// _setError(): protected virtual method inherited from ProblemHandler
//-----------------------------------------------------------------------------

//# See the documentation for the base class.

method void
StandardProblemHandler::_setError(const RWCString& message)
{
  _errorMessages.append(message);
}


//# See the documentation for the base class.

method void
DefaultProblemHandler::_setError(const RWCString& message)
{
  error("The following unhandled error was"
	+ reportedBy_message(subsystemName(), className(), procedureName())
	+ message);
}


//-----------------------------------------------------------------------------
// _setWarning(): protected virtual method inherited from ProblemHandler
//-----------------------------------------------------------------------------

//# See the documentation for the base class.

method void
StandardProblemHandler::_setWarning(const RWCString& message)
{
  _warningMessages.append(message);
}


//# See the documentation for the base class.

method void
DefaultProblemHandler::_setWarning(const RWCString& message)
{
  daWarning("The following warning message was"
	    + reportedBy_message(subsystemName(), className(),
				 procedureName())
	    + message);
}


//-----------------------------------------------------------------------------
// _isError(): protected virtual method inherited from ProblemHandler
//-----------------------------------------------------------------------------

//# See the documentation for the base class.

method MitBool
StandardProblemHandler::_isError() const
{
  return MitBool(!!_errorMessages.length());
}


//# See the documentation for the base class.

method MitBool
DefaultProblemHandler::_isError() const
{
  return false;
}


//-----------------------------------------------------------------------------
// _isWarning(): protected virtual method inherited from ProblemHandler
//-----------------------------------------------------------------------------

//# See the documentation for the base class.

method MitBool
StandardProblemHandler::_isWarning() const
{
  return MitBool(!!_warningMessages.length());
}


//# See the documentation for the base class.

method MitBool
DefaultProblemHandler::_isWarning() const
{
  return false;
}


//-----------------------------------------------------------------------------
//  _clear(): protected virtual method inherited from ProblemHandler
//-----------------------------------------------------------------------------

//# See the documentation for the base class.

method void
StandardProblemHandler::_clear()
{
  _wasProblemCleared = true;
}


//# See the documentation for the base class.

method void
DefaultProblemHandler::_clear()
{}


//-----------------------------------------------------------------------------
// _message(): protected virtual method inherited from ProblemHandler
//-----------------------------------------------------------------------------

//# See the documentation for the base class.  _message() returns a
//# string that indicates the number of warnings and errors that have
//# been raised, the subsystem, class, and procedure in which the
//# problems were raised, if they are known.  The message also lists all
//# of the individual error and warning messages.  If there have been no
//# warnings or errors, then the empty string is returned.

method RWCString
StandardProblemHandler::_message() const
{
  int nErrors = errorCount();
  int nWarnings = warningCount();
  if (nErrors + nWarnings < 1) return emptyString;
  RWCString retval;
  if (nErrors + nWarnings == 1)
    retval = RWCString("The following ") + (nErrors ? "error" : "warning")
             + " was";
  else {
    retval = "There were ";
    if (nErrors && nWarnings)
      retval += eacIntToString(nErrors) + " error"
	     + (nErrors > 1 ? "s" : "") + " and " + eacIntToString(nWarnings)
	     + " warning" + (nWarnings > 1 ? "s" : "");
    else if (nErrors) retval += eacIntToString(nErrors) + " errors";
    else retval += eacIntToString(nWarnings) + " warnings";
  }
  retval += reportedBy_message(subsystemName(), className(), procedureName());
  if (nErrors + nWarnings == 1) {
    if (nErrors) retval += _errorMessages[0];
    else retval += _warningMessages[0];
  } else {
    for (int i = 0; i < nErrors; ++i) {
      retval += RWCString(i ? "\t" : "") + "Error #" + eacIntToString(i + 1)
	     + ": " + _errorMessages[i] + "\n";
    }
    for (int i = 0; i < nWarnings; ++i) {
      retval += RWCString(i || nErrors ? "\t" : "") + "Warning #"
	     + eacIntToString(i + 1) + ": " + _warningMessages[i] + "\n";
    }
  }
  return retval;
}


//# Returns the empty string.  The reason for this is that the default
//# problem handler terminates the program immediately if an error is
//# raised, so there never will be a pending error, and it outputs a
//# warning message immediately if a warning is output, and thereafter
//# considers it handled.

method RWCString
DefaultProblemHandler::_message() const
{
  return emptyString;
}


//-----------------------------------------------------------------------------
// _makeHandler(): protected virtual method inherited from ProblemHandler
//-----------------------------------------------------------------------------

//# See the documentation for the base class.

method ProblemHandler*
StandardProblemHandler::_makeHandler() const
{
  return new StandardProblemHandler;
}


//# See the documentation for the base class.

method ProblemHandler*
DefaultProblemHandler::_makeHandler() const
{
  return new DefaultProblemHandler;
}


//-----------------------------------------------------------------------------
// hasProblemBeenHandled(): protected virtual method inherited from
//                          ProblemHandler
//-----------------------------------------------------------------------------

//# See the documentation for the base class.

method MitBool
StandardProblemHandler::_hasProblemBeenHandled() const
{
  return _wasProblemCleared;
}


//# See the documentation for the base class.

method MitBool
DefaultProblemHandler::_hasProblemBeenHandled() const
{
  return false;
}
@


5.1
log
@Bump version number
@
text
@d3 1
a3 1
"$Id: Problem.C,v 4.1 1995/11/21 02:45:01 nessus Exp nessus $";
d8 1
a8 1
// .version   $Revision: 4.1 $
d430 1
a430 1
      } else if (isWarning()) eacReportWarning(message());
d681 4
a684 4
  eacReportWarning("The following warning message was"
		   + reportedBy_message(subsystemName(), className(),
					procedureName())
		   + message);
a854 20
}


//*****************************************************************************
//*****                                                                   *****
//*****                    Global Procedures                              *****
//*****                                                                   *****
//*****************************************************************************


//-----------------------------------------------------------------------------
// mitReportWarning(): global procedure
//-----------------------------------------------------------------------------

//# Outputs a warning message to `cerr'.

proc void
mitReportWarning(const char* warningMessage)
{
  cerr << "WARNING: " << warningMessage << endl;
@


4.1
log
@Bump version number
@
text
@d3 1
a3 1
"$Id: Problem.C,v 1.2 1995/09/19 17:53:32 nessus Exp nessus $";
d8 1
a8 1
// .version   $Revision: 1.2 $
@


1.2
log
@Removed MitStringUtil.h from include list since there is no such file.
@
text
@d3 1
a3 1
"$Id: Problem.C,v 1.1 1995/09/14 22:23:41 nessus Exp $";
d8 1
a8 1
// .version   $Revision: 1.1 $
@


1.1
log
@Initial revision
@
text
@a24 1
#include <MitStringUtil.h>
@

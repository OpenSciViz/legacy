head	5.3;
access
	soccm
	nessus
	gromeo;
symbols
	Consistent-switch-to-solaris:5.3
	Consistent-after-restore-dump-mostly-finished:5.3
	Fixed-DR805:5.2
	Consistent-GenericTrigger2:5.2
	Consistent-GenericTrigger:5.2
	Build5_0_3:5.2
	Interim5_0_2-1:5.2
	Consistent-SafetyTrigger:5.2
	Build5_0_1:5.1
	RciPointers:4.3
	Fixed-DR650:4.3
	Fixed-DR502:4.3
	Build4_3_1:4.3
	Build4_3:4.3
	Build4_2:4.3
	Consistent-post4-mod2:4.2
	Consistent-post4-mod1:4.1
	Build4:4.3
	Build3_3:3.3
	Consistent-no-template-instantiation:3.3
	Consistent-bm-param-dumps:3.3
	Build3_2:3.2
	Consistent-fix-for-RW6:3.2
	Build3_1:3.2
	Consistent-first-newdb:3.2
	C-structs-beta2:3.1
	Build3:3.1
	Current:5.3
	C-structs-beta:2.4
	Build2:2.3;
locks; strict;
comment	@ * @;


5.3
date	97.05.08.16.42.19;	author nessus;	state Exp;
branches;
next	5.2;

5.2
date	96.02.12.21.26.05;	author nessus;	state Exp;
branches;
next	5.1;

5.1
date	95.12.23.00.57.40;	author nessus;	state Exp;
branches;
next	4.3;

4.3
date	95.01.12.00.59.55;	author nessus;	state Exp;
branches;
next	4.2;

4.2
date	95.01.03.18.21.12;	author nessus;	state Exp;
branches;
next	4.1;

4.1
date	94.12.01.14.20.16;	author nessus;	state Exp;
branches;
next	3.3;

3.3
date	94.08.22.18.26.12;	author nessus;	state Exp;
branches;
next	3.2;

3.2
date	94.06.24.01.40.33;	author nessus;	state Exp;
branches;
next	3.1;

3.1
date	94.05.02.07.08.53;	author nessus;	state Exp;
branches;
next	2.5;

2.5
date	94.05.02.07.04.35;	author nessus;	state Exp;
branches;
next	2.4;

2.4
date	94.01.19.23.00.54;	author nessus;	state Exp;
branches;
next	2.3;

2.3
date	93.11.30.22.32.50;	author nessus;	state Release;
branches;
next	2.2;

2.2
date	93.11.02.00.34.15;	author nessus;	state Exp;
branches;
next	2.1;

2.1
date	93.10.27.17.56.18;	author nessus;	state Exp;
branches;
next	1.1;

1.1
date	93.10.15.16.02.38;	author nessus;	state Exp;
branches;
next	;


desc
@@


5.3
log
@First pass at getting parameter dump restores to work for most modes.
Have not yet implemented restores for FftMode.
Also turned on referenced-counter pointers to ObsParamItems.
@
text
@// -*- Mode: C++; fill-column: 73; fill-prefix: "//# " -*-
#ifndef EventByEventMode_H
#define EventByEventMode_H

#define EventByEventModeRcsId_H \
"$Id: EventByEventMode.h,v 1.11 1997/05/08 00:41:33 nessus Exp $";

// Description : Declaration of the EventByEventMode class.
// Author      : Douglas Alan <nessus@@mit.edu>
// Copyright   : Massachusetts Institute of Technology, 1993

#include <MitBitUtil.h>
#include <AsmObsParamBlock.h>
#include <EacGlobalData.h>

// .NAME EventByEventMode - a class to model a Flight Software EA mode
// .LIBRARY EdsConfig
// .HEADER Event by Event Mode
// .INCLUDE EventByEventMode.h
// .FILE EventByEventMode.C
// .FILE EventByEventMode.h

//*****************************************************************************
//*****                                                                   *****
//*****     EventByEventMode: concrete subclass of AsmObsParamBlock       *****
//*****                                                                   *****
//*****************************************************************************

class EventByEventMode: public AsmObsParamBlock
{

  //* Instance variables:
  uint32			_intervalDuration;
  EventByEventDspParamsPtr	_dspParams;

  //* Private class constants:
  static const RWCString	intervalDurationName;
  static const RWCString   	dspParamsName;

protected:

  //* Protected nonvirtual methods:
  void		   		_print(ostream& s) const;

  //* Protected virtual methods inherited from ObsParamItem:
  void				_compare(const ObsParamItem& item,
					 ItemDiffs& diffs) const;
  void				restoreFromParser(const EacParser& parser);

  //* Protected virtual methods inherited from ObsParamBlock:
  void				_restoreFromParamDump(const ObsParamDump& dump,
						      const uint8* albData,
						      int albLength,
						      Problem& prob);
public:
   

  //* Class constants:
  typedef AsmObsParamBlock	base;
  static const RWCString	className;
  enum Constants	      { incrEdsImageSize = 4 };

  static const char* const	intervalDurationDiff;
  static const char* const	dspParamsDiff;

  //* Constructors, etc:
  EventByEventMode();
  static EventByEventModePtr	restore(const RWCString& name);

  //* Class procedures:
  static MitBool		isLegalFieldTag(const RWCString& tag);
  
  //* Accessor methods:
  uint32		   	intervalDuration() const;
  EventByEventDspParamsPtr 	dspParams() const;

  //* Setter methods:
  void				setIntervalDuration(uint32);
  void				setDspParams(EventByEventDspParamsPtr);

  //* Nonvirtual methods:
  void				compareNv(const EventByEventMode& item,
					  ItemDiffs& diffs) const;

  //* Virtual methods inherited from ObsParamItem:
  void		        	makeEdsImage(EdsMemoryBlockImage& image,
					     EdsMemoryBlock& block) const;
  void				print(ostream& s) const;
  void				printCstruct(ostream& s) const;
  int				sizeOfEdsImage() const;
  void				update();
  MitBool			vIsLegalFieldTag(const RWCString& tag) const;

  //* Virtual methods inherited from ObsParamBlock:
  ObsParamItemPtr		auxDspLoadBlock() const;
  ConfigId	        	configId() const;
  IntelAddress			mainDspLoadBlockAddress() const;
  ScienceMode			scienceMode() const;
  double			timeBetweenIntervalsInSeconds() const;

  //* Virtual methods inherited from AsmObsParamBlock:
  EventByEventModePtr	   	castToEventByEventMode();
  ConstEventByEventModePtr	castToEventByEventMode() const;	
};


//*****************************************************************************
//*****                                                                   *****
//*****     EventByEventDspParams: concrete subclass of ObsParamItem      *****
//*****                                                                   *****
//*****************************************************************************

class EventByEventDspParams: public ObsParamItem
{
  //* Instance variables:
  uint7			_cameraId;
  MitBoolc		_recordAllEvents;

  //* Private class constants:
  static const RWCString cameraIdName;
  static const RWCString recordAllEventsName;

protected:

  //* Private methods:
  uint7			cameraIdFs() const;
  MitBool		isRecordingAllEventsFs() const;

  //* Protected nonvirtual methods:
  void			_print(ostream& s) const;

  //* Protected virtual methods inherited from ObsParamItem:
  void			_compare(const ObsParamItem& item, ItemDiffs& diffs)
                             const;
  void			restoreFromParser(const EacParser& parser);


public:
   
  //* Class constants:
  typedef ObsParamItem	   base;
  static const RWCString   className;
  enum Constants         { incrEdsImageSize = 4 };

  static const char* const cameraIdDiff;
  static const char* const recordAllEventsDiff;

  //* Constructors, etc:
  EventByEventDspParams();
  static EventByEventDspParamsPtr restore(const RWCString& name);
  static EventByEventDspParamsPtr restoreFromParamDump(
                                     const uint8* data, int size, Problem&);

  //* Class procedures:
  static MitBool	isLegalFieldTag(const RWCString& tag);
  
  //* Accessor methods:
  uint7			cameraId() const;
  MitBool		isRecordingAllEvents() const;

  //* Setter methods:
  void			setCameraId(/* 1, 2, or 3*/ int);
  void			recordAllEvents(MitBool);

  //* Nonvirtual methods:
  void			compareNv(const EventByEventDspParams& item,
				  ItemDiffs& diffs) const;

  //* Virtual methods inherited from ObsParamItem:
  void			        makeEdsImage(EdsMemoryBlockImage& image,
					     EdsMemoryBlock& block) const;
  void			        print(ostream& s) const;
  void			        printCstruct(ostream& s) const;
  int			        sizeOfEdsImage() const;
  void			        update();
  MitBool		        vIsLegalFieldTag(const RWCString& tag) const;
  EventByEventDspParamsPtr      castToEventByEventDspParams();
  ConstEventByEventDspParamsPtr castToEventByEventDspParams() const;	
};


//-----------------------------------------------------------------------------
// Accessor methods
//-----------------------------------------------------------------------------

inline uint32
EventByEventMode::intervalDuration() const
{
  return _intervalDuration;
}


inline EventByEventDspParamsPtr
EventByEventMode::dspParams() const
{
  assert(_dspParams);
  return _dspParams;
}


inline uint7
EventByEventDspParams::cameraId() const
{
  assert(_cameraId);
  return _cameraId;
}


inline MitBool
EventByEventDspParams::isRecordingAllEvents() const
{
  return (MitBool) _recordAllEvents;
}


//-----------------------------------------------------------------------------
// Setter methods
//-----------------------------------------------------------------------------

inline void
EventByEventMode::setIntervalDuration(uint32 value)
{
  _intervalDuration = value;
}


inline void
EventByEventMode::setDspParams(EventByEventDspParamsPtr value)
{
  _dspParams = value;
}


inline void
EventByEventDspParams::setCameraId(int value)
{
  if (value >= 1 && value <= 3) _cameraId = value;
  else setRangeError(cameraIdName, 1, 3);
}


inline void
EventByEventDspParams::recordAllEvents(MitBool value)
{
  _recordAllEvents = value;
}


//-----------------------------------------------------------------------------
// cameraIdFs(): private method of EventByEventDspParams. [TBD]
//-----------------------------------------------------------------------------

inline uint7
EventByEventDspParams::cameraIdFs() const
{
  return cameraId() - 1;
}
 

//-----------------------------------------------------------------------------
// isRecordingAllEventsFs(): private method of EventByEventDspParams. [TBD]
//-----------------------------------------------------------------------------

inline MitBool
EventByEventDspParams::isRecordingAllEventsFs() const
{
  return MitBool(!isRecordingAllEvents());
}

#endif // EventByEventMode_H
@


5.2
log
@Added support for the new SafetyTrigger feature of MultTimeSeriesMode.
@
text
@d6 1
a6 1
"$Id: EventByEventMode.h,v 1.10 1996/02/12 21:23:46 nessus Exp $";
d13 2
a14 2
#include "AsmObsParamBlock.h"
#include "EacGlobalData.h"
d23 5
a27 5
//#############################################################################
//#####                                                                   #####
//#####        EventByEventMode: subclass of AsmObsParamBlock             #####
//#####                                                                   #####
//#############################################################################
d31 8
a38 8
  //
  /* Instance variables: */
  uint32		   _intervalDuration;
  EventByEventDspParamsPtr _dspParams;
  //
  /* Private class constants: */
  static const RWCString   intervalDurationName;
  static const RWCString   dspParamsName;
a40 3
  //
  /* Protected nonvirtual methods: */
  void			_print(ostream& s) const;
d42 13
a56 2
  typedef
    AsmObsParamBlock	base;
d58 9
a66 2
  //
  /* Constructors, etc: */
d68 1
a68 2
  static
    EventByEventModePtr	restore(const RWCString& name);
d70 2
a71 3
  //
  /* Class procedures: */
  static MitBool	isLegalFieldTag(const RWCString& tag);
d73 31
a103 29
  //
  /* Accessor methods: */
  uint32		   intervalDuration() const;
  EventByEventDspParamsPtr dspParams() const;
  //
  /* Setter methods: */
  void			setIntervalDuration(uint32);
  void			setDspParams(EventByEventDspParamsPtr);
  //
  /* Virtual methods inherited from ObsParamItem: */
  void		        makeEdsImage(EdsMemoryBlockImage& image,
				     EdsMemoryBlock& block) const;
  void			print(ostream& s) const;
  void			printCstruct(ostream& s) const;
  void			restoreFromParser(const EacParser& parser);
  int			sizeOfEdsImage() const;
  void			update();
  MitBool		vIsLegalFieldTag(const RWCString& tag) const;
  //
  /* Virtual methods inherited from ObsParamBlock: */
  ObsParamItemPtr	auxDspLoadBlock() const;
  ConfigId	        configId() const;
  IntelAddress		mainDspLoadBlockAddress() const;
  ScienceMode		scienceMode() const;
  double		timeBetweenIntervalsInSeconds() const;
  //
  /* Virtual methods inherited from AsmObsParamBlock: */
  EventByEventModePtr	   castToEventByEventMode();
  ConstEventByEventModePtr castToEventByEventMode() const;	
d107 5
a111 5
//#############################################################################
//#####                                                                   #####
//#####        EventByEventDspParams: subclass of ObsParamItem            #####
//#####                                                                   #####
//#############################################################################
d115 1
a115 2
  //
  /* Instance variables: */
d118 2
a119 2
  //
  /* Private class constants: */
d124 2
a125 2
  //
  /* Private methods: */
d128 2
a129 2
  //
  /* Protected nonvirtual methods: */
d132 6
d140 4
a143 1
  typedef ObsParamItem	base;
d145 4
a148 2
  //
  /* Constructors, etc: */
d150 5
a154 4
  static EventByEventDspParamsPtr
                        restore(const RWCString& name);
  //
  /* Class procedures: */
d157 1
a157 2
  //
  /* Accessor methods: */
d160 2
a161 2
  //
  /* Setter methods: */
d164 15
a178 14
  //
  /* Virtual methods inherited from ObsParamItem: */
  void			makeEdsImage(EdsMemoryBlockImage& image,
				     EdsMemoryBlock& block) const;
  void			print(ostream& s) const;
  void			printCstruct(ostream& s) const;
  void			restoreFromParser(const EacParser& parser);
  int			sizeOfEdsImage() const;
  void			update();
  MitBool		vIsLegalFieldTag(const RWCString& tag) const;
  EventByEventDspParamsPtr
			castToEventByEventDspParams();
  ConstEventByEventDspParamsPtr
			castToEventByEventDspParams() const;	
d261 1
a261 1
// cameraIdFs(): private method of EventByEventDspParams. [TBD]
@


5.1
log
@Bump version number
@
text
@d6 1
a6 1
"$Id: EventByEventMode.h,v 4.3 1995/01/12 00:59:55 nessus Exp nessus $";
d83 1
a83 1
  SCIENCE_MODE		scienceMode() const;
@


4.3
log
@Bug fixes for Build 4.0.1.
@
text
@d6 1
a6 1
"$Id: EventByEventMode.h,v 1.9 1995/01/12 00:54:11 nessus Exp nessus $";
@


4.2
log
@EdsMemory is now complete.  (Modulo the inevitable bugs and some
improvements that need to be made soon.)
@
text
@d6 1
a6 1
"$Id: EventByEventMode.h,v 1.8 1994/08/22 18:21:35 nessus Exp nessus $";
d84 1
a84 1
  //  double		timeBetweenIntervalsInSeconds() const; // TODO
@


4.1
log
@Bump version number
@
text
@d6 1
a6 1
"$Id: EventByEventMode.h,v 3.3 1994/08/22 18:26:12 nessus Exp nessus $";
d70 2
d75 1
d80 1
d82 1
d141 2
d146 1
@


3.3
log
@BinnedMode parameter dump restores and operator==() now work.
@
text
@d6 1
a6 1
"$Id: EventByEventMode.h,v 1.8 1994/08/22 18:21:35 nessus Exp nessus $";
@


3.2
log
@We now support loading of individual config items on demand, and we
also have a stubbed EdsMemory object.
@
text
@d6 1
a6 1
"$Id: EventByEventMode.h,v 3.1 1994/05/02 07:08:53 nessus Exp nessus $";
a85 1
ostream& operator<<(ostream& s, const EventByEventMode& config);
a86 1

a145 2

ostream& operator<<(ostream& s, const EventByEventDspParams& config);
@


3.1
log
@Bump version number
@
text
@d6 1
a6 1
"$Id: EventByEventMode.h,v 2.5 1994/05/02 07:04:35 nessus Exp nessus $";
d77 1
@


2.5
log
@Check in of files for Build 3.
@
text
@d6 1
a6 1
"$Id: EventByEventMode.h,v 1.6 1994/05/02 06:56:14 nessus Exp nessus $";
@


2.4
log
@Check in of many files to support the 'make-structs' program, which will
be used to generate configurations in a form suitable for compiling
into the flight software.
@
text
@d1 3
a3 2
// -*- Mode: C++; fill-column: 79; fill-prefix: "//# " -*-
// $Id: EventByEventMode.h,v 1.5 1994/01/14 20:59:43 nessus Exp nessus $
d5 4
a8 1
// Description : [TBD]
d12 1
a12 3
#ifndef EventByEventMode_h
#define EventByEventMode_h

d14 1
d16 7
d24 3
a26 1
//# EventByEventMode: subclass of AsmObsParamBlock
d29 10
a38 1
class EventByEventMode: public AsmObsParamBlock {
d40 4
a43 2
  // Instance variables:
  uint32			_intervalDuration;
d46 3
d50 5
a54 2
  // Static member fuctions:
  // static EventByEventModePtr restore(RWCString name);
d56 28
a83 3
  // Accessor methods:
  uint32			intervalDuration() const
                                     { return _intervalDuration; }
d85 1
a85 5
  // Setter methods:
  void				setIntervalDuration(int microticks)
                                     { assert(microticks > 524288 /*500ms*/);
				       _intervalDuration = microticks;
				     }
a86 2
  // Methods inherited from ObsParamBlock:
  virtual SCIENCE_MODE		scienceMode() const { return ASM_MODE_EVENT; }
d88 58
a145 2
  // Methods inherited from AsmObsParamBlock:
  virtual EventByEventModePtr	castToEventByEventMode() { return this; }
d147 93
a239 2
  	   
#endif // EventByEventMode_h
@


2.3
log
@Added or modified rcs id header.
@
text
@d2 1
a2 1
// $Id$
d28 1
a28 1
  uintint			intervalDuration() const
d38 1
a38 1
  virtual SCIENCE_MODE		scienceMode() { return ASM_MODE_EVENT; }
@


2.2
log
@Tried to make sure all of the cast methods are implemented correctly,
fixed ScienceMode() method to return the standard SCIENCE_MODE enum,
and added stub code for Royce to use.  This required the creation of
some global data.
@
text
@d1 7
@


2.1
log
@Only bumped revision number up to 2.1.
@
text
@d29 6
@


1.1
log
@Initial revision
@
text
@@

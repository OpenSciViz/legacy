head	1.4;
access
	soccm
	nessus;
symbols
	Current:1.4
	Build4_3:1.4
	Build4_2a:1.4
	Build4_2:1.4
	Build4_1:1.4
	Build4:1.4;
locks; strict;
comment	@# @;


1.4
date	94.11.28.22.23.39;	author berczuk;	state Exp;
branches;
next	1.3;

1.3
date	94.11.28.21.36.27;	author berczuk;	state Exp;
branches;
next	1.2;

1.2
date	94.11.14.20.49.51;	author berczuk;	state Exp;
branches;
next	1.1;

1.1
date	94.11.14.20.40.44;	author berczuk;	state Exp;
branches;
next	;


desc
@man page for asm Data Display client
@


1.4
log
@updated to add -catalog switch
@
text
@.\"
.\"  $Id: asmDataDisplayClient.man,v 1.3 1994/11/28 21:36:27 berczuk Exp berczuk $
.\"
.TH Asm Analysis 1
.SH NAME
asmDataDisplay-client \- realtime client to
decode partitions containing ASM Multiple Time Series and Position
Histogram Data. asmMTSDisplay, asmPosHistDisplay \- TCL scripts to
display Multiple Time Series and Position Histogram data. 
.SH SYNOPSIS
\fBasmDataDisplay-client\fP  [-read-from-stdin] [-port #] -host hostname -name
client-name [-sleep #] -catalog name [-poshist] [-mtsdata]
.br
\fBasmMTSDisplay\fP
.br
\fBasmPHDisplay\fP
.br
.SH DESCRIPTION
asmDataDisplay-client reads packets from a real time connection, and
generates line oriented output that can be used to view the edsMonitor
is based on the protocols of the
.I rtclient 
(see) template
.PP
Each realtime client connects to the Realtime Server over a well-known
Transmission Control Protocol
.I (TCP)
port.
After connecting to the Realtime Server the realtime client must tell
the Realtime Server who it is by passing its name to the Realtime
Server. The information may also be displayed by using the client
.I asmMTSDisplay
for Multiple Time Series Data or
.I asmPHDisplay
for Position Histogram Data.
.PP
See
.I rtclient
for restrictions relating to naming clients and host authorization.
else the Realtime Server will close the connection.
.SH SWITCHES
These switches need to be specified when using the real time client
directly. If the display tools are used, the user will be presented
with an interface to specify these parameters.
.TP
.B -read-from-stdin
When this option is enabled, the realtime client expects to read
telemetry from its standard input instead of over the connection to
the Realtime Server.
This option overrules `-daemonize'.
The realtime client exits when there are no more packets to be read
from stdin, or on any error.
None of the options
.I -port,
.I -host
or
.I -name
are used (hence they needn't be set) when this option is used.
This is here strictly for debugging purposes so that we can feed known
files of telemetry through realtime clients.
.TP
.B "-port #"
This option specifies the well-known port on which the Realtime Server
listens for connections from valid realtime clients and then passes
the appropriate telemetry to those interested (and connected) realtime
clients.
If this option is not set, there must be a well-defined TCP service in
.I "/etc/services"
named
.I realtime-server
from which the realtime client can determine a valid port number with
which to establish a connection with the Realtime Server.
Under normal operations it is
expected that a valid /etc/services entry will be supplied on all the
XTESOC machines so that this option doesn't need to be specified.
.TP
.B "-host hostname"
This option specifies the host machine on which the Realtime Server is
running.  This option must be specified unless
.I -read-from-stdin
is set.
.TP
.B "-name client-name"
This option specifies the name of the realtime client.
The name must correspond to an entry in the RT.CLIENTS table.
.TP
.B -sleep time
This option specifies the number of seconds to which to sleep between
partitions.  This is useful when reading data from a file.
.TP
.B -catalog name
Specify the text file to read in containing the catalog of the list of
sources. The format is defined elsewhere.
.TP
.B "-mtsdata"
Display multiple time series data, including background, time
series, and pulse height histograms.
.TP
.B "-poshist"
Display position histogram data.
.SH "ENVIRONMENT VARIABLES"
.IP SOCHOME
This is the root directory of the XTESOC Integration and Test
directory.
There should be a subdirectory,
.I etc
,below this directory which should contain both the AUTH.HOSTS and
RT.CLIENTS files.
.IP XTE_TCL_LIBRARY
This should point to the directory containing XTE-specific TCL library
files. If this is not set, the TCL clients look at $SOCHOME/lib/tcl
.IP SOCHOME
.IP SOCOPS
This is the root of the SOC operations directory tree.
.SH "SEE ALSO"
rtclient asmCatalog(5)
.SH BUGS
Bugs, where?
.SH AUTHORS
The realtime client template was written by Mike Lijewski. 
The client and tcl scripts were written by Steve Berczuk.
@


1.3
log
@cleaned up for release
@
text
@d2 1
a2 1
.\"  $Id: asmDataDisplayClient.man,v 1.2 1994/11/14 20:49:51 berczuk Exp berczuk $
d12 1
a12 1
client-name [-sleep #] [-poshist] [-mtsdata]
d91 4
d116 1
a116 1
rtclient
@


1.2
log
@style changes
@
text
@d2 1
a2 1
.\"  $Id: asmDataDisplayClient.man,v 1.1 1994/11/14 20:40:44 berczuk Exp berczuk $
d12 1
a12 1
client-name [-poshist ] [-mtsdata ]
a85 1

d87 4
a96 19
.SH "OUTPUT FORMATS"
.SS MTS Background Histograms
Downlink packets are displayed on a line with the following format:
.IP
BackHist ssc<##> numBins <# bins> : <data vector>
.SS MTS Time Series Histograms
Downlink packets are displayed on a line with the following format:
.IP
TimeSer ssc<#> ec<#> TimeBins <#> : <data vector>
.SS MTS Pulse Height Histograms
Downlink packets are displayed on a line with the following format:
.IP
PulseHeight ssc <#> anode <#> <# points in hist> : <data vector>
.SS Position Histograms
Downlink packets are displayed on a line with the following format:
.IP
<EA-identifier> <MET> Downlink <number of packets>
.TP
For Example: EDS_EA5 247443 Downlink 2
a97 3
.IP DISPLAY
This sets the machine and screen on which any X Windows opened by the
realtime client will be displayed.
d117 1
a117 1
The display clients and tcl scripts were written by Steve Berczuk.
@


1.1
log
@Initial revision
@
text
@d2 1
a2 1
.\"  $Id$
d15 1
d42 3
@

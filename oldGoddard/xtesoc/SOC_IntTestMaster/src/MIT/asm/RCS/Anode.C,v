head	5.8;
access
	soccm
	nessus
	gromeo;
symbols
	Consistent-switch-to-solaris:5.8
	merged-qlgo-process-da:5.8
	Build5_0_3:5.8
	Berczuk_final:5.8
	Beta3:5.7
	Beta2:5.6
	Beta1a:5.6
	Beta1:5.6
	Current:5.5;
locks; strict;
comment	@ * @;


5.8
date	96.01.14.19.29.50;	author berczuk;	state Rel;
branches;
next	5.7;

5.7
date	95.12.21.20.22.03;	author berczuk;	state Rel;
branches;
next	5.6;

5.6
date	95.11.09.21.01.00;	author berczuk;	state Rel;
branches;
next	5.5;

5.5
date	95.10.13.14.59.20;	author berczuk;	state Exp;
branches;
next	5.4;

5.4
date	95.10.03.21.20.19;	author berczuk;	state Exp;
branches;
next	5.3;

5.3
date	95.10.02.20.55.42;	author berczuk;	state Exp;
branches;
next	5.2;

5.2
date	95.09.26.13.17.41;	author berczuk;	state Exp;
branches;
next	5.1;

5.1
date	95.09.18.21.02.50;	author berczuk;	state Exp;
branches;
next	;


desc
@anode class
@


5.8
log
@fixed a typo and replaced references to cell width with cell Depth. No functional changes.
@
text
@//   RCS: $Id: Anode.C,v 5.7 1995/12/21 20:22:03 berczuk Rel berczuk $ 
//   Author      : Steve Berczuk
//   Copyright 1995, Massachusetts Institute of Technology

// REVIEWED 9/13/95 by ehm,aml,rr,nessus,cui
#undef RWBOUNDS_CHECK
#include <iostream.h>
#include <math.h>
//#include "ssc.h"
#include <AsmConsts.h>
#include <SscUtils.h>
#include <XteLine.h>
#include "CalibFunc.h"
#include "AnodeCell.h"
#include "DetectorWindow.h"
#include "Anode.h"
static const char rcsid[]= 
"$Id: Anode.C,v 5.7 1995/12/21 20:22:03 berczuk Rel berczuk $"; 

// Description:
// initialize the anode with the list of bins taking into account the window 
// rounding.
Anode::Anode(int id, int numBins, int numIds, int firstPos, const CalibFunc& f,
             double cellWd, const DetectorWindow& zWinLims)
    :theBins(numBins),
     anodeId(id),
     cellWidth(cellWd)
{

    // ignore binmin & binMax for now
    for (int i=0; i<numBins; i++){
        int ip = i + firstPos;
        // the first bin we care about starts at position 
        // binMin
        // check that we are doing the right thing with NANODE/2
        // ??should it be 4 or (4-1) ?
        // convert bin to an electrical bin position (a/(a+b))
        //??? is this correctly 0 based?
        double zlo=0.0;
        double zhi=0.0;
        double yhi=f((double)ip/(double)(numIds ));
        double ylo=f(double(ip+1)/(double)(numIds ));
        double y =(yhi+ylo)/2.0;
        
        int iside=1;            

        RWTValOrderedVector<AnodeCellRegion> zRegions(2);
        // there are typically 2 voids per cell there can be three
        if ( y <0)
            iside =0;

        // z limits
//        if ((y >= ywinlo[iside]) && (y<=ywinhi[iside])) {
        if (zWinLims.isInWindow(y,iside)) {
            zlo = (anodeId - NANODE/2.0 )*cellWidth;
            zhi = zlo+cellWidth;
            for(int ii=0; ii< 14; ii++) {
                // check to see where the open window intersects the
                // cell region
                if ((XteLine(zlo,zhi).overlaps(zWinLims[ii]) > 0)){
                    XteLine isection(XteLine(zlo,zhi)
                                     .intersection(zWinLims.windowExtent(ii,y)));
                    zRegions.append(AnodeCellRegion(isection.begin(),
                                                isection.end()));
//                    }
                }
            }
        }
        
        theBins(i) = AnodeCell(SscCell(ylo,yhi,zlo,zhi),zRegions);

        
    }
//    printCells(cout);
    
}

// Description:
// Return the minimum Z extent of this Anode's Cells
double Anode::zMinLimit() const
{
    return (anodeId-(NANODE/2.0 ))*cellWidth;


}

// Description:
// Return the maximum Z extent of this Anode's Cells
double Anode::zMaxLimit() const
{
    double zlo = (anodeId-(NANODE/2.0 ))*cellWidth;
    return zlo+cellWidth;
}

// DEscription:
// return the average with (in Y) of the Cells for this Anode
double Anode::averageBinWidthInY() const
{
    unsigned l = theBins.length();
    double sum =0;
    // use () rather than [] for access
    // since we don't need bounds checking here
    // because wee are only indexing within valid limits
    // and bound checking slows us down
    for(int i=0; i<l-1; i++){
        sum+=fabs(theBins(i+1).y() - theBins(i).y());
        
    }
    return sum/l;
    
}

// Description:
// print out the limits of the Cells in this anode.
//  Mostly used for diagnostic purposes.
void Anode::printCells(ostream& os) const
{
    unsigned l = theBins.length();
    for(int i=0; i<l; i++){
        os << " bin # " << i << " ";
        theBins[i].printLimits(os);
    }
    
}



@


5.7
log
@now uses calibration database rather than constants.
@
text
@d1 1
a1 1
//   RCS: $Id: Anode.C,v 5.6 1995/11/09 21:01:00 berczuk Rel berczuk $ 
d18 1
a18 1
"$Id: Anode.C,v 5.6 1995/11/09 21:01:00 berczuk Rel berczuk $"; 
d24 1
a24 1
             double cellDp, const DetectorWindow& zWinLims)
d27 1
a27 1
     cellDepth(cellDp)
d55 2
a56 2
            zlo = (anodeId - NANODE/2.0 )*cellDepth;
            zhi = zlo+cellDepth;
d82 1
a82 1
    return (anodeId-(NANODE/2.0 ))*cellDepth;
d91 2
a92 2
    double zlo = (anodeId-(NANODE/2.0 ))*cellDepth;
    return zlo+cellDepth;
@


5.6
log
@removed rnage checking from bins accessing for speed .
@
text
@d1 1
a1 1
//   RCS: $Id: Anode.C,v 5.5 1995/10/13 14:59:20 berczuk Exp berczuk $ 
d9 2
a10 1
#include "ssc.h"
d18 1
a18 1
"$Id: Anode.C,v 5.5 1995/10/13 14:59:20 berczuk Exp berczuk $"; 
a19 15
static  DetectorWindow zWinLims;
// Lower limits on windows in Y on each side 
/*
const double ywinlo [2] ={
    -(STRLN+STRBAR/2.0 - EPOXY),
    STRBAR/2.0 + EPOXY
};

// Upper limits on windows in Y on each side 
const double ywinhi[2] = {
    -(STRBAR/2.0 + EPOXY),
    STRLN+STRBAR/2.0 - EPOXY
};
*/

d23 2
a24 1
Anode::Anode(int id, int numBins, int numIds, int firstPos, const CalibFunc& f)
d26 2
a27 1
     anodeId(id)
d55 2
a56 2
            zlo = (anodeId - NANODE/2.0 )*CELLWD;
            zhi = zlo+CELLWD;
d82 1
a82 1
    return (anodeId-(NANODE/2.0 ))*CELLWD;
d91 2
a92 2
    double zlo = (anodeId-(NANODE/2.0 ))*CELLWD;
    return zlo+CELLWD;
@


5.5
log
@fixed cell area calculations.
@
text
@d1 1
a1 1
//   RCS: $Id: Anode.C,v 5.4 1995/10/03 21:20:19 berczuk Exp berczuk $ 
d6 1
a6 1

d17 1
a17 1
"$Id: Anode.C,v 5.4 1995/10/03 21:20:19 berczuk Exp berczuk $"; 
d82 1
a82 1
        theBins[i] = AnodeCell(SscCell(ylo,yhi,zlo,zhi),zRegions);
@


5.4
log
@fixed up detector window model.
@
text
@d1 1
a1 1
//   RCS: $Id: Anode.C,v 5.3 1995/10/02 20:55:42 berczuk Exp berczuk $ 
d17 1
a17 1
"$Id: Anode.C,v 5.3 1995/10/02 20:55:42 berczuk Exp berczuk $"; 
a33 43

//XteLine zWinLims[14];

/*
// initialize the array specifying the bounding box of the windows in Z
int initZLims()
{
    for(int ii=0; ii<14; ii++) {
        double begin= (ii-NWOPEN/2)*SWSEP +(SWSEP-STRWID)*0.5;
        double end = begin+STRWID;
        zWinLims[ii] = XteLine(begin,end);
    }
    return 0;
}

// static initializer to set up the array
const int foo = initZLims();

static XteLine windowExtent(int whichWindow, double y)
{
    int iside =1;
    if(y<0) iside=0;
    
    double z1,z2;
    z1= zWinLims[whichWindow].begin();
    z2= zWinLims[whichWindow].end();
    double dy = y-ywinlo[iside];
    if ((dy <= SCORN) ) {
        double delz =SCORN-sqrt(dy*(2*SCORN-dy));
        z1+=delz;
        z2-=delz;
    }
    dy = ywinhi[iside] - y;
    if ((dy <= SCORN) ) {
                            
        double delz =SCORN-sqrt(dy*(2*SCORN-dy));
        z1+=delz;
        z2-=delz;
    }
    return XteLine(z1,z2);
    
}
*/
a40 1
    
a41 1

d43 1
a43 1
    for (int i=0; i<numBins; ++i){
d55 1
a55 1
        double y =(yhi+ylo)/2;
d59 1
a59 1
        RWTValOrderedVector<AnodeCellRegion> zVoids(2);
d75 1
a75 9
/*
                    cout <<"window " << ii << "( "<<zWinLims[ii]
                         << ") intersects " <<
                        XteLine(zlo,zhi) <<" intersection: "
                         << XteLine(zlo,zhi).intersection(zWinLims[ii]) <<endl;
                    cout << "sub cell"<<isection.begin()<< " "<< isection.end()<<endl;
                    cout << "BBOX "<<zlo<< " "<<zhi<<" "<<ylo<<" "<<yhi<<endl;
 */                   
                    zVoids.append(AnodeCellRegion(isection.begin(),
d82 3
a84 1
        theBins[i] = AnodeCell(SscCell(ylo,yhi,zlo,zhi),zVoids);
d86 2
d128 1
a128 1
void Anode::printCells() const
d132 2
a133 1
        theBins[i].print();
@


5.3
log
@checkpoint before I change from using voids to using open areas.
@
text
@d1 1
a1 1
//   RCS: $Id: Anode.C,v 5.2 1995/09/26 13:17:41 berczuk Exp berczuk $ 
d14 1
d17 1
a17 1
"$Id: Anode.C,v 5.2 1995/09/26 13:17:41 berczuk Exp berczuk $"; 
d19 1
d21 1
d32 1
d35 1
d37 2
a38 1
XteLine zWinLims[14];
d49 1
d51 26
d104 1
a104 1
        RWTValOrderedVector<AnodeCellVoid> zVoids(2);
d110 2
a111 1
        if ((y >= ywinlo[iside]) && (y<=ywinhi[iside])) {
a114 1
                int idx=0;
d116 1
a116 1
                //
d119 2
a120 2
                                     .intersection(zWinLims[ii]));

d125 6
a130 26
                    cout << "void " <<isection.end()<< " "
                    << min(zhi,isection.end()+0.11)<<endl;


                    if(isection.end() < zhi){
                        double endz = isection.end();
                        double endz2=min(zhi,isection.end()+0.11);
                        double dy = y-ywinlo[iside];
//                    cout << " before: z: " << endz << " " <<endz2<<endl;
                        if ((dy <= SCORN) ) {
                             double delz =SCORN-sqrt(dy*(2*SCORN-dy));
                            endz-=delz;
                            endz2+=delz;
                            //                          cout << " corner1: z: " << endz << " " <<endz2<<endl;
                        }
                        dy = ywinhi[iside] - y;
                        if ((dy <= SCORN) ) {
                            
                            double delz =SCORN-sqrt(dy*(2*SCORN-dy));
                            endz-=delz;
                            endz2+=delz;
//                        cout << " corner: z: " << endz << " " <<endz2<<endl;
                        }
                        
                            zVoids.append(AnodeCellVoid(endz,endz2));
                    }
@


5.2
log
@added modeling of window bars.
@
text
@d1 1
a1 1
//   RCS: $Id: Anode.C,v 5.1 1995/09/18 21:02:50 berczuk Exp berczuk $ 
d10 1
d16 1
a16 1
"$Id: Anode.C,v 5.1 1995/09/18 21:02:50 berczuk Exp berczuk $"; 
d71 1
a71 1
       RWTValOrderedVector<AnodeCellVoid> zVoids(2);
d78 41
a118 35
             zlo = (anodeId - NANODE/2.0 )*CELLWD;
             zhi = zlo+CELLWD;
             for(int ii=0; ii< 14; ii++) {
                 int idx=0;
                 
                 if ((XteLine(zlo,zhi).overlaps(zWinLims[ii]) > 0)){
                     XteLine isection(XteLine(zlo,zhi)
                                      .intersection(zWinLims[ii]));
                     
                     //cout <<"window " << ii << "( "<<zWinLims[ii]
                     //   << ") intersects " <<
//                         XteLine(zlo,zhi) <<" intersection: "
                     // << XteLine(zlo,zhi).intersection(zWinLims[ii]) <<endl;
//                     cout << "void " <<isection.end()<< " "<< 
//                         isection.end()+0.11 <<endl;
                     zVoids[idx++] =AnodeCellVoid(isection.end(),
                                                 isection.end()+0.11);
                     
//                     c.addVoid(AnodeCellVoid(isection.end(), 
//                                             isection.end()+0.11));
                 }
             }
//             cout << " area of ANODE CELL " << c.area() <<" orig area "
             //               << boundingBox.area() <<endl<<"ZZ ";
//             c.print();
             
             
                 
             // truncate to the window z limits
            double dy = y-ywinlo[iside];
            //          cout <<" >> Y(L) "<< y<< " dy "  <<dy ;
            if (dy <= SCORN) {
                double delz =SCORN-sqrt(dy*(2*SCORN-dy));
                zlo+=delz;
                zhi-=delz;
d120 2
a121 10
            dy = ywinhi[iside] - y;
//            cout <<" || Y(H) "<< " dy "  <<dy <<endl;
            if (dy <= SCORN) {
                double delz =SCORN-sqrt(dy*(2*SCORN-dy));
                zlo+=delz;
                zhi-=delz;
            }

        } 
//        theBins[i].setLimits(ylo,yhi,zlo,zhi);
a122 1
        
@


5.1
log
@new build
@
text
@d1 1
a1 1
//   RCS: $Id$ 
d10 1
d12 1
d14 2
a15 1
static const char rcsid[]= "$Id$"; 
d31 10
d42 1
d47 1
a47 1
    :theBins(RWTValVector<SscCell>(numBins)),
d51 2
d69 3
d79 27
a105 1

d122 2
a123 1
        theBins[i].setLimits(ylo,yhi,zlo,zhi);
a124 1
            //= SscCell(ylo,yhi,zlo,zhi);
@

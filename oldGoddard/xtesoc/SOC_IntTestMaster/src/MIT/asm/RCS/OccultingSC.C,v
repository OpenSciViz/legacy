head	5.1;
access
	soccm
	nessus
	gromeo;
symbols
	Consistent-switch-to-solaris:5.1
	merged-qlgo-process-da:5.1
	Build5_0_3:5.1
	Berczuk_final:1.1
	Beta3:1.1;
locks; strict;
comment	@ * @;


5.1
date	96.04.05.20.56.25;	author nessus;	state Exp;
branches;
next	1.1;

1.1
date	95.12.11.19.59.41;	author berczuk;	state Rel;
branches;
next	;


desc
@@


5.1
log
@Bump version number
@
text
@// Steve Berczuk: modifications to dasmith c code:
// wrapped function calls to determine occultation to
// be member functions for occultingSC.
/* occultbysc.c
Alan M. Levine
September 1, 1995

This function computes whether a source as seen from an SSC, is occulted
 by the XTE spacecraft.  The physical model of the S/C is crude, but is
 still a lot better than neglecting such occultations completely.

sscid = 0 => SSC 1
      = 1 => SSC 2
      = 2 => SSC 3

sourcevec - 3-vector toward the celestial source in the
 S/C body coordinate system

asmang (degrees) - rotation angle of the ASM (zero has SSCs 1 & 2
 pointed along the S/C -X axis

sapyang, samyang (degrees) - solar array (plus Y & minus Y) wing angles

returned value -
  -1 - error
   0 - no occultation
   1 - source is occulted
*/


//#include "occultbysc.h"
// moved these values into OccultingSC.h
//#include <stdio.h>
#include <math.h>
#include <SkyPt.h>
#include "OccultingSC.h"


/* function to determine if a source is occulted by the S/C body */

int scbody(vector sourcevec, vector wincen1)
{
    int numinter;
    double x,y,xsclo,xschi,ysclo,yschi,dist,d;
    vector scnorm,scpt;
    
    xsclo = -0.5*SCA;
    xschi = 0.5*SCA;
    ysclo = -0.5*SCB;
    yschi = 0.5*SCB;

    scnorm.x = 0.0;
    scnorm.y = 0.0;
    scnorm.z = 1.0;
    d = ASMA+TA;

    scpt.x = 0.0;
    scpt.y = 0.0;
    scpt.z = 0.0;

    numinter = interpt(sourcevec,wincen1,scnorm,d,scpt,&dist);

    if ( (numinter == 1) && (dist > 0.0) )
	{
	    x = scpt.x;
	    y = scpt.y;

	    if ( (x > xsclo) && (x < xschi) && (y > ysclo) && (y < yschi))
		  return (1);   /* There is an occultation */
	}
    return (0);
}


/* function to determine solar array wing occultation
*/

int solarwing(int wingnum, OccultingSC spc, vector sourcevec,vector wincen)
{
    int numinter,answer;
    double sang,cosa,sina,dwing,dist;
    double xp,zp,xsa,ysa,xslo,xshi,yslo,yshi,hold;
    vector wingnorm,wingpt;
    
    xslo = -0.5*WC;
    xshi = 0.5*WC;
    yslo = 0.5*SCB+WA;
    yshi = 0.5*SCB+WA+WB;
    sang = spc.pwang;

    if(wingnum < 0){
      sang = -1.0*spc.mwang;
      hold = yslo;
      yslo = -yshi;
      yshi = -hold;
    }

    cosa = cos(sang*con);
    sina = sin(sang*con);
    wingnorm.x = sina;
    wingnorm.y = 0.0;
    wingnorm.z = cosa;
    wingpt.x = 0.0;
    wingpt.y = 0.0;
    wingpt.z = 0.0;

    dwing = (ASMA+TA+SCC)*cosa + SCD*sina;

    numinter = interpt(sourcevec,wincen,wingnorm,dwing,wingpt,&dist);
    
    if ( (numinter == 1) && (dist > 0.0))
	{
	    xp = wingpt.x-SCD;
	    zp = wingpt.z-(ASMA+TA+SCC);
            xsa = xp*cosa-zp*sina;
	    ysa = wingpt.y;

	    if ((xsa > xslo)&&(xsa < xshi)&&(ysa > yslo)&&(ysa < yshi))
		  answer = 1; 		/* There is an occultation */
	    else
		answer = 0;
	}
/* !!!!!!!!!!!  Defer difficult case */
    else if (numinter == 2) 
	answer = 2;
    else answer = 3;

    return(answer);
}


int occultbysc(SSC_ID sscid, const OccultingSC& xte1, vector source)
{
    int ioc1, ioc2, ioc3, answer;
    double ang;
    vector wincen;

if ( (sscid < 0) || (sscid > 2))
    return(-1);

/* SSC 3 FOV is never occulted */
if (sscid == 2)
    return(0);

/* set up vector to window center */
    ang = con*xte1.asmang;
    wincen.x = ASMB*sin(ang);
    wincen.y = ASMB*cos(ang);
    wincen.z = 0.0;
    if (sscid == 0)
	{
	    wincen.x *= -1.0;
	    wincen.y *= -1.0;
	}

    answer = 1;

    ioc1 = solarwing(1, xte1,source,wincen);
    if (ioc1 != 1)
    {
      ioc2 = solarwing(-1, xte1, source, wincen);
      if(ioc2 != 1){
        ioc3 = scbody(source,wincen);
        if (ioc3 != 1)
	      answer = 0;
      }
    }
    return(answer);
}



int OccultingSC::occults(SSC_ID ssc, const SkyPt& srcPos) const {
    // convert the skypt into a dasmith vector
    DoubleVec cartesian = srcPos.cartesian();
    vector v;
    v.x = cartesian[0];
    v.y=cartesian[1];
    v.z=cartesian[2];
    
    return (occultbysc(ssc,*this, v));
}



// static functions used as utilities
/* interpt.c
Alan M. Levine
September 1, 1995

This function determines the intersection point of a line and a plane.

The returned integer is:

-1 - if either the line tangent or plane normal vectors has zero length,
0 - if the line and plane do not intersect,
1 - if there is exactly one intersection point, and
2 - if the line is in the plane.

The returned value dist is the distance along the line from the point (linept)
to the intersection point (intersect).  The value is positive if the dot
product "(intersect - linept) dot (tanvec)" is positive.
*/
/*
#include <math.h>
#include <stdio.h>
#include "scOccult.h"

*/
/* 3-vector dot product */

double dot(vector avec, vector bvec)
{
  double dtprd=0.0;
  dtprd += avec.x*bvec.x;
  dtprd += avec.y*bvec.y;   
  dtprd += avec.z*bvec.z;
  return(dtprd);
}

/* 3-vector normalization to form a unit vector */

vector normvec(vector vect)
{
    double len;

    len = dot(vect,vect);
    if (len > 0.0)
	{
	    len = sqrt(len);
	    vect.x /= len;
            vect.y /= len;
            vect.z /= len;
      	}
    return(vect);
}

int interpt(vector tanvec,vector linept,vector planenorm,double dplane,vector& intersect,double *dist2)
{
    double plnp,tlnp,numer;
    vector tvec, planen;

    tvec = normvec(tanvec);
    planen = normvec(planenorm);

    tlnp = dot(tvec,planen);
    plnp = dot(linept,planen);
    numer = dplane - plnp;

    if (tlnp != 0.0)
     {
       *dist2 = numer/tlnp;

       intersect.x = *dist2*tvec.x + linept.x;
       intersect.y = *dist2*tvec.y + linept.y;
       intersect.z = *dist2*tvec.z + linept.z;
	   
       return(1);
     }
     else
     {
       if (numer == 0.0)
       {
         intersect.x = linept.x;
	 intersect.y = linept.y;
         intersect.z = linept.z;

	 return(2);
       }
       else
	 return(0);
     }
}
@


1.1
log
@Initial revision
@
text
@@

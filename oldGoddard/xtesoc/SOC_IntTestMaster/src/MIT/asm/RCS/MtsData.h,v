head	5.2;
access
	soccm
	nessus
	gromeo;
symbols
	Consistent-switch-to-solaris:5.2
	Build5_0_3:5.2
	Berczuk_final:5.2
	Beta3:5.2
	Beta2:5.2
	Beta1a:5.2
	Beta1:5.2
	Current:5.2
	Build4_3:1.3
	Build4_2a:1.3
	Build4_2:1.3
	Build4_1:1.2
	Build4:1.2
	BUILD4:1.2;
locks; strict;
comment	@ * @;


5.2
date	95.09.27.18.13.05;	author berczuk;	state Rel;
branches;
next	5.1;

5.1
date	95.09.18.20.55.33;	author berczuk;	state Exp;
branches;
next	4.1;

4.1
date	95.06.09.16.00.26;	author berczuk;	state Exp;
branches;
next	1.3;

1.3
date	95.03.22.15.52.29;	author berczuk;	state Exp;
branches;
next	1.2;

1.2
date	94.11.28.22.37.33;	author berczuk;	state Exp;
branches;
next	1.1;

1.1
date	94.11.14.16.56.33;	author berczuk;	state Exp;
branches;
next	;


desc
@class to model Multiple Time Series Data
@


5.2
log
@added oper== to class TimeSeriesParms.
@
text
@//   RCS: $Id: MtsData.h,v 5.1 1995/09/18 20:55:33 berczuk Exp berczuk $ 
//   Copyright 1994, Massachusetts Institute of Technology

// .NAME MtsData.h
// .LIBRARY libasm.a
// .HEADER ASM Analysis Software
// .INCLUDE MtsData.h
// .FILE  ../src/MtsData.C
// .SECTION Author
// Steve Berczuk, berczuk@@mit.edu
// .SECTION Description
// Interface for ASM Multiple Time Series mode data.
// Based on Multiple Time Series Mode Specification
// MIT #64-30112.060109 Rev C.
// From a EdsPartition Time Series/Background and Pulse Height 
// histograms are constructed and analyzed

#ifndef MtsData_H
#define MtsData_H
#include "EdsData.h"

#include "AsmDefs.h"
// includes the enums for SSCs

class EdsPartition;
class TimeSerHist;
 // Time series Histogram
class BackHist;
// Background Histogram
class PhHist;
// Pulse Height Histogram
class Displayer;

struct TimeSeriesParms
{
    int timeBinCount;
    // number of TIME BINS for Time Series Hists
    struct
    {
        int numEnergyBounds;
        int energyBounds[3][6];
        // up to 6 values per ssc
    } theBoundvals;
    TimeSeriesParms& operator=(const TimeSeriesParms&);
    
};



class MtsData :public AsmData
{
public:
    MtsData(const EdsPartition*);
    // construct using an EdsPartition
    
    ~MtsData();
    // destructor

    virtual void display(ostream&) const;
    virtual void display(Displayer&) const;
    virtual EdsResult* process(EdsDataAnalyzer& da) ;
    
    
    BackHist backgroundHist() const;
    // get the background histogram
    
    TimeSerHist timeSeriesHist() const;
    // get the Time series histogram

    PhHist pulseHeightHist() const;
    // get the pulse height histogram

    RWBoolean hasPulseHtHist() const;
    // returns TRUE if the partition in this hist contains
    // a PulseHeight Histogram

    RWBoolean hasTimeSerHist() const;
    // return true or false depending in whether the EDS was configured 
    // to collect TimeSeries (non Background) data.

    virtual MtsData* asMtsData() 
    {
        return this;
    }
    
private:
    EdsPartition* thePartition;
    
    int pulseHtIntervalLength;
    // Number of time series intervals in a pulse ht interval -1;

    // *** Parameters from Configuration
    // Time Series Histogram

    // pulse ht params
    int lowCutoffs[3];
    int highCutoffs[3];
    int numEnergyBins[3];


    TimeSeriesParms tsParms;
    
    int timeBinCount;

    int timeSerEnergyBins();
    // Pulse Height    
   
};

inline RWBoolean MtsData::hasTimeSerHist() const 
{
    return (tsParms.theBoundvals.numEnergyBounds > 1);
    // per Mult Time Ser Mode spec
}


#endif
@


5.1
log
@new model
@
text
@d1 1
a1 1
//   RCS: $Id: MtsData.h,v 4.1 1995/06/09 16:00:26 berczuk Exp berczuk $ 
d44 2
@


4.1
log
@bump to Rev 4
@
text
@d1 1
a1 1
//   RCS: $Id: MtsData.h,v 1.3 1995/03/22 15:52:29 berczuk Exp berczuk $ 
@


1.3
log
@added changes to handle gse configs.
@
text
@d1 1
a1 1
//   RCS: $Id: MtsData.h,v 1.2 1994/11/28 22:37:33 berczuk Exp berczuk $ 
@


1.2
log
@Checkpoint for Build 4.0
@
text
@d1 1
a1 1
//   RCS: $Id: MtsData.h,v 1.1 1994/11/14 16:56:33 berczuk Exp berczuk $ 
d75 4
d108 5
@


1.1
log
@Initial revision
@
text
@d1 1
a1 1
//   RCS: $Id: MtsData.h,v 3.1 1994/04/29 21:41:03 berczuk Exp berczuk $ 
d22 1
a22 1
#include "Ssc.h"
d59 1
@

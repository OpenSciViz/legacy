head	5.1;
access
	soccm
	nessus
	gromeo;
symbols
	Consistent-switch-to-solaris:5.1
	merged-qlgo-process-da:5.1
	Build5_0_3:5.1
	Berczuk_final:1.6
	Beta3:1.6
	Beta2:1.5
	Beta1a:1.5
	Beta1:1.5
	Current:1.3;
locks; strict;
comment	@ * @;


5.1
date	96.04.05.20.56.10;	author nessus;	state Exp;
branches;
next	1.6;

1.6
date	95.12.21.20.23.54;	author berczuk;	state Rel;
branches;
next	1.5;

1.5
date	95.11.15.18.30.54;	author berczuk;	state Rel;
branches;
next	1.4;

1.4
date	95.11.09.21.01.32;	author berczuk;	state Exp;
branches;
next	1.3;

1.3
date	95.10.13.14.59.20;	author berczuk;	state Exp;
branches;
next	1.2;

1.2
date	95.10.03.21.20.42;	author berczuk;	state Exp;
branches;
next	1.1;

1.1
date	95.09.26.13.18.48;	author berczuk;	state Exp;
branches;
next	;


desc
@Class to Model Cells on a wire. Similar to SscCell but allows voids.
@


5.1
log
@Bump version number
@
text
@//   RCS: $Id: AnodeCell.C,v 1.6 1995/12/21 20:23:54 berczuk Rel nessus $ 
//   Author      : Steve Berczuk
//   Copyright 1994, Massachusetts Institute of Technology
#include <assert.h>
#include <iostream.h>
#include <SscUtils.h>
#include <rw/tvsrtvec.h>
#include <AnodeCell.h>

static const char rcsid[]= 
"$Id: AnodeCell.C,v 1.6 1995/12/21 20:23:54 berczuk Rel nessus $"; 


double AnodeCell::area() const
{
    double area =0;
    int regionsToSub = theZRegions.entries();
    double yExtent = theBoundingBox.yLength();
    for(int i=0; i<regionsToSub; i++){
        // this hack saves us an array access
        // making the look SLIGHTLY faster
        const AnodeCellRegion& reg=theZRegions(i);
        double zExtent= reg.end -reg.begin;
        area += zExtent*yExtent;
    }
    return area;
}

void AnodeCell::addRegion(const AnodeCellRegion& v)
{
    // first truncate the voids to the boundingBox
    theZRegions.append(v);
}

AnodeCell AnodeCell::intersection(const AnodeCell& cell) const
{
    SscCell newBbox= theBoundingBox.intersection(cell.theBoundingBox);
    // check for no intersection, if the bounding boxes do not intersect then
    // do not bother checking the open regions
    // this depends on no intersection returning the null box
    if(newBbox == SscCell(0,0,0,0)){
        return AnodeCell(newBbox);
    }

    // take the union of the voids lists that are in the limits of the 
    // bounding box

    int zEntries = theZRegions.entries();
//    RWTValSortedVector<AnodeCellRegion> mergeList(zEntries+2);
    RWTValOrderedVector<AnodeCellRegion> mergeList(2*zEntries);
    for(int i=0; i<zEntries; i++){
        const AnodeCellRegion& r =theZRegions(i);
        // clip the void to the new bounding box
        if(r.end > newBbox.zMin()){
            double zmin=max(r.begin,newBbox.zMin());
            double zmax=min(r.end,newBbox.zMax());
            if(zmax> zmin)
                mergeList.insert(AnodeCellRegion(zmin,zmax));
        }
    }
    zEntries = cell.theZRegions.entries();
    for( i=0; i<zEntries; i++){
         const AnodeCellRegion& r =cell.theZRegions(i);
        if(r.end > newBbox.zMin()){
            double zmin=max(r.begin,newBbox.zMin());
            double zmax=min(r.end,newBbox.zMax());
            mergeList.insert(AnodeCellRegion(zmin,zmax));
        }
    }

    // merge the void lists
    zEntries = mergeList.entries();
    RWTValOrderedVector<AnodeCellRegion> theNewRegions(zEntries);
    for(i=0; i<zEntries; i++){
        theNewRegions.append(mergeList(i));
    }

    //  return AnodeCell(newBbox,theNewRegions);
  return AnodeCell(newBbox,theNewRegions);
}

void AnodeCell::printLimits(ostream& os) const
{
    os << theBoundingBox.yMin() << "\t" <<theBoundingBox.yMax()<<"\n";

}

void AnodeCell::print(ostream& os) const
{
    // walk through bounds and print
    os<<"boundingBox [ ";
    
    theBoundingBox.print(os);
    os<<"]\n";
    
    for(int i=0; i<theZRegions.entries(); i++) {
        SscCell(theBoundingBox.yMin(),theBoundingBox.yMax(),
                theZRegions(i).begin,theZRegions(i).end).print(os);

        

    }

}
ostream& operator <<(ostream& os, const AnodeCell& a)
{
    a.print(os);
    
    return os;
    
}
@


1.6
log
@calibration values are used rather than consts so constants .h file is
no longer included.
@
text
@d1 1
a1 1
//   RCS: $Id: AnodeCell.C,v 1.5 1995/11/15 18:30:54 berczuk Rel berczuk $ 
d11 1
a11 1
"$Id: AnodeCell.C,v 1.5 1995/11/15 18:30:54 berczuk Rel berczuk $"; 
@


1.5
log
@made some changes for performance.
@
text
@d1 1
a1 1
//   RCS: $Id: AnodeCell.C,v 1.4 1995/11/09 21:01:32 berczuk Exp berczuk $ 
a7 1
#include <ssc.h>
d11 1
a11 1
"$Id: AnodeCell.C,v 1.4 1995/11/09 21:01:32 berczuk Exp berczuk $"; 
@


1.4
log
@removed range checking from indexing for efficiency
@
text
@d1 1
a1 1
//   RCS: $Id: AnodeCell.C,v 1.3 1995/10/13 14:59:20 berczuk Exp berczuk $ 
d12 1
a12 1
"$Id: AnodeCell.C,v 1.3 1995/10/13 14:59:20 berczuk Exp berczuk $"; 
a13 6
AnodeCell::AnodeCell (const SscCell& boundingBox)
    :theBoundingBox(boundingBox),theZRegions(2)
{
    
    
}
a14 7
AnodeCell::AnodeCell(const SscCell& boundingBox, 
                     const RWTValOrderedVector<AnodeCellRegion>& bars)
    :theBoundingBox(boundingBox),theZRegions(bars)
{
    
}

a16 4
    // area is the area of the bounding box less the area indicated by the 
    // voids. Be sure the limit the voids to the limits of the bounding box;
//    double baseArea = theBoundingBox.area();
    // calculate the areas of the areas to be omitted...
a19 1
    
d21 4
a24 1
        double zExtent= theZRegions(i).end -theZRegions(i).begin;
d36 1
a36 1
AnodeCell AnodeCell::intersection(const AnodeCell& cell)
a37 1
    
d39 7
a48 1
    RWTValSortedVector<AnodeCellRegion> mergeList(4);
d50 2
d53 1
a53 2
        
        AnodeCellRegion r =theZRegions(i);
a57 1
            
d64 1
a64 1
        AnodeCellRegion r =theZRegions(i);
d78 3
a80 1
      return AnodeCell(newBbox,theNewRegions);
@


1.3
log
@fixed cell area calculations.
@
text
@d1 1
a1 1
//   RCS: $Id: AnodeCell.C,v 1.2 1995/10/03 21:20:42 berczuk Exp berczuk $ 
d4 1
d12 1
a12 1
"$Id: AnodeCell.C,v 1.2 1995/10/03 21:20:42 berczuk Exp berczuk $"; 
a41 2
    

a59 2

    
d67 1
a67 5
/*            if(zmax < zmin){
                cerr<<"warning!! zmax < zMin zmax:"<<zmax
                    <<"zmin;" <<zmin <<endl;
            }
 */           
a78 1
//        mergeList.insert(cell.theZRegions(i));
a79 1
        
a84 1
    
d86 1
a86 1
        theNewRegions.append(mergeList.at(i));
d88 1
a88 3
    
    return AnodeCell(newBbox,theNewRegions);
    
d90 1
d107 1
a107 1
                theZRegions[i].begin,theZRegions[i].end).print(os);
@


1.2
log
@fixed up detector window model.
@
text
@d1 1
a1 1
//   RCS: $Id: AnodeCell.C,v 1.1 1995/09/26 13:18:48 berczuk Exp berczuk $ 
d11 1
a11 1
"$Id: AnodeCell.C,v 1.1 1995/09/26 13:18:48 berczuk Exp berczuk $"; 
d42 1
d61 1
d64 14
a77 1
        mergeList.insert(theZRegions(i));
d81 8
a88 1
        mergeList.insert(cell.theZRegions(i));
d102 3
d106 3
a108 1
void AnodeCell::print() const
d111 5
a115 1
    theBoundingBox.print();
d117 2
a119 5
//            cout << " print void \n";
            
            SscCell(theBoundingBox.yMin(),theBoundingBox.yMax(),
                    theZRegions[i].begin,theZRegions[i].end).print();

d124 7
@


1.1
log
@Initial revision
@
text
@d1 1
a1 1
//   RCS: $Id$ 
d5 1
d10 3
a12 1
static const char rcsid[]= "$Id$"; 
d14 1
a14 1
    :theBoundingBox(boundingBox),theZVoids(2)
d21 2
a22 2
                     const RWTValOrderedVector<AnodeCellVoid>& bars)
    :theBoundingBox(boundingBox),theZVoids(bars)
d31 1
a31 1
    double baseArea = theBoundingBox.area();
d33 2
a34 2
    double subArea =0;
    int regionsToSub = theZVoids.entries();
d38 2
a39 2
        double zExtent= theZVoids(i).end -theZVoids(i).begin;
        subArea += zExtent*yExtent;
d42 1
a42 1
    return baseArea-subArea;
d45 1
a45 1
void AnodeCell::addVoid(const AnodeCellVoid& v)
d48 1
a48 1
    theZVoids.append(v);
d58 2
a59 2
    RWTValSortedVector<AnodeCellVoid> mergeList(4);
    int zEntries = theZVoids.entries();
d62 1
a62 1
        mergeList.insert(theZVoids(i));
d64 1
a64 1
    zEntries = cell.theZVoids.entries();
d66 1
a66 1
        mergeList.insert(cell.theZVoids(i));
a67 1
//    cout << " merged " <<mergeList.entries()<<endl;
d71 1
a71 1
    RWTValOrderedVector<AnodeCellVoid> theNewVoids(zEntries);
d74 1
a74 1
        theNewVoids.append(mergeList.at(i));
d77 1
a77 1
    return AnodeCell(newBbox,theNewVoids);
a83 1
   
d85 1
a85 3
    cout << " Cell has " <<theZVoids.entries()<<" voids\n";
    
    for(int i=0; i<<theZVoids.entries(); i++) {
d87 5
a91 2
        cout <<theBoundingBox.yMin() << " " <<theZVoids[i].begin<< "\n" 
             <<theBoundingBox.yMin() << " " <<theZVoids[i].end<<"\n";
a92 3
        cout <<theBoundingBox.yMax() << " " <<theZVoids[i].begin 
             << " " <<theBoundingBox.yMax() << " " <<theZVoids[i].end<<"\n";
        
d95 1
a95 1
    
@

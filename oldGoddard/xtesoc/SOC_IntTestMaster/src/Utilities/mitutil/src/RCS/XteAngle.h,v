head	1.5;
access;
symbols;
locks; strict;
comment	@ * @;


1.5
date	94.11.30.19.22.36;	author berczuk;	state Submitted;
branches;
next	;


desc
@moved from ../inc
@


1.5
log
@checked in with -k by soccm at 1997/05/16 17:06:13
@
text
@//   RCS: $Id: XteAngle.h,v 1.5 1994/11/30 19:22:36 berczuk Submitted $ 
//   Author      : Steve Berczuk
//   Copyright 1994, Massachusetts Institute of Technology


// ****************************************************************************
// Class XteAngle.
// Allows user to enter angles in terms of magnitude as well as units.  No cal-
// culations must be done to convert from radians to degrees or vice versa.
// Correct syntax: \(90 DEGREES)
// ****************************************************************************
#ifndef XTEANGLE_h
#define XTEANGLE_h


#include <math.h>



class istream;
class ostream;

class XteAngle
{
  
public:
  enum UNIT{RADIANS, DEGREES};   // Radians = 0, Degrees = 1
  friend istream& operator>> (istream&, XteAngle&);
  // Istream operator will input XteAngle in this form: (magnitude UNIT).
  // i.e. (30 DEGREES).
  // NOTE: the units should be in caps &Parameters are enclosed by parentheses.
   
  friend ostream& operator<< (ostream&, const XteAngle&);
  // Ostream operator will output XteAngle in equivalent form:
  // (magnitude UNIT). 

    XteAngle(const XteAngle& t);
    // clone constructor
    
    XteAngle(double mag, UNIT u = RADIANS); 
  // Constructor will default units to RADIANS.
  // The magnitude of the angle will be stored under \'mag\' in radians.

  XteAngle(int deg, int min, double secs);
//  constructor using degrees mins seconds

  XteAngle(void);                
  // Default magnitude to PI rad.

  ~XteAngle();                   

  static void setMode(UNIT);
  // Affects output of XteAngle.  
  // Indicate RADIANS or DEGREES and the ostream output will be set
  // accordingly:
  // (90 DEGREES) or (3.141 RADIANS). 


  // accessors
  double degrees() const ;
  // return the value in decimal degrees

  double radians() const ;
  // return the value in radians
  
  double cos() const; 
  // Computes the cosine of the angle and returns the value.

  double sin() const ;
  // Computes the sine of the angle and returns the value.

  double tan() const;
  // Computes the tangent of the angle and returns the value.

  XteAngle operator+ (const XteAngle&)const;
  //Adds the magnitudes of two XteAngles and returns the sum.

  XteAngle operator- (const XteAngle&)const;
  //Subtracts the magnitudes of two XteAngles and returns the difference.

  XteAngle operator/(double) const;
  // divides an angle by a double

  XteAngle operator-() const;
  // uniary minus operator; returns an angle with magnitude of
  // (-) this->mag
  
  XteAngle& operator+= (const XteAngle&);
  //Replaces the magnitude of this with the sum of the magnitudes of
  // this and X.

  XteAngle& operator-= (const XteAngle&);
  // Replaces the magnitude of this with the difference of the
  // magnitudes of this and X.


  int operator== (const XteAngle&)const;
  // Compares the magnitude of two XteAngles for equality.  
  // Returns \'1\' if equal, returns \'0\' otherwise.

  int operator> (const XteAngle&)const;
  // Checks to see if the magnitude of this is greater than magnitude of X.
  // Returns \'1\' if true, returns \'0\' otherwise.

  int operator< (const XteAngle&)const;
  // Checks to see if the magnitude of this is less than magnitude of X.
  // Returns \'1\' if true, returns \'0\' otherwise.

protected:
  double mag;
  // magnitude in radians
  
  static UNIT mode;

};

inline double XteAngle::degrees() const  {return mag*180.0/M_PI;}

inline double XteAngle::radians()const  {return mag;}

inline double cos(XteAngle angle) 
// Alternate function to find the cosine of an angle. 
// Can enter cos(XteAngle X)
// instead of calling function as X.cos().
{
  return angle.cos();
}

inline double sin(XteAngle angle) 
// Alternate function to find the sine of an angle.  
// Can enter sin(XteAngle X) instead of
// calling function as X.sin().
{
  return angle.sin();
}

inline double tan(XteAngle angle)
// Alternate function to find the tan of an angle.
// Can enter tan(XteAngle X) instead of
// calling function as X.tan().
{
  return angle.tan();
}

#endif

@

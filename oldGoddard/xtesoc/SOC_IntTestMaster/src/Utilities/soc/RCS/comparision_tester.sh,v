head	1.2;
access;
symbols;
locks; strict;
comment	@# @;


1.2
date	98.11.02.15.36.20;	author jonv;	state Exp;
branches;
next	1.1;

1.1
date	98.11.02.15.14.41;	author jonv;	state Exp;
branches;
next	;


desc
@used for testing the time_converter program
Lots of other things need ot be checked out to make this
script work.  See the comments in the script for details
@


1.2
log
@added RCS Id tag
@
text
@#!/bin/sh

# RCS: $Id:$

# To do the test, you will need to check out following files:
#   1. files for doing time conversions based on Toshi's older scripts:
#       util_time_original.pl  perl_time.pl  leaptable.txt
#   2. files for doing time conversions using the latest programs
#       util_time.pl  new_perl_time.pl
#       comparision_tester.sh
#       tai-utc.dat
#  (You will also need to make the execuatble time_converter - note that
#   time_converter.C requires XTETime.h and XTETime.C in order to compile)
# All files should be left in the current direcotry except tai-utc.dat
# Put the tai-utc.dat file in $SOCHOME/etc
#   (This file is used by XTETime to determine the leap seconds.
#    It might be a good idea to make sure that tai-utc.dat and the 
#    leaptable.txt file have the same leap seconds (from 1994 to 
#    the present).  The RCS versions 1.1 of each do have the same 
#    leap seconds up to the Jan 1, 1999 leap second. )
# They don't both have to be up to date, but they should both have the 
#  same leap seconds (over the mission lifetime at least - Toshi's data 
#  file does not have leap seconds before the mission).
#
# Once you have compiled time_converter, then you can just execute the test 
# script.  It will do some calculations with the new algorithms in 
# time_converter and then also show the output from the old algorithms found
# in the util_time perl routines.


echo 'Test script for "time_converter"'
echo ""
echo "This script test the workings of the time conversion program."
echo "There are three sets of tests in this script."
echo ""
echo "1.  The first set tests the compatibility of this script with "
echo "    Toshi's perl script time converters.  The time_converter program"
echo "    and Toshi's program are run for each type of conversion, and the"
echo "    ouputs are compared."
echo "    Also, one more program is run: a new copy of Toshi's script which"
echo "    has its innards replaced with the time_converter calls."
echo ""
echo "    Note that in order for this to work, you need to have the programs"
echo "    perl_time.pl (my perl program which calls Toshi's libraries) and "
echo "    util_time_original.pl (the actual libraries which were is use "
echo "    before this conversion program was available) in the current "
echo "    directory to do this test."
echo "    The newer perl routines whcih do conversions basesd on the C++"
echo "    program is called new_perl_time.pl, and it uses the script"
echo "    util_time.pl for doing its conversions."  
echo ""
echo "2.  The second test is just a test of every possible mode available to"
echo "    the time_converter program.  It shows the command line to be"
echo "    executed, and then executes it."
echo ""
echo "3.  The third test is just some Y2K testing of the conversions."
echo ""
echo ""
echo "######################################################################"
echo "START OF THE FIRST SET OF TESTS"
echo "This set of tests gives a comparison of results between"
echo "the old time conversion routines and the new."
echo "######################################################################"
echo "Testing MET to UTC"
echo "starting with MET: 014334453"
./time_converter -MET_to_UTC 014334453
./perl_time.pl met2time 014334453
./new_perl_time.pl met2time 014334453
echo ""
echo "starting with MET: 0802711294"
./time_converter -MET_to_UTC 0802711294
./perl_time.pl met2time 0802711294
./new_perl_time.pl met2time 0802711294
echo ""
echo "starting with MET: 0004899325"
./time_converter -MET_to_UTC 0004899325
./perl_time.pl met2time 0004899325
./new_perl_time.pl met2time 0004899325
echo ""
echo "Testing UTC to MET"
echo "starting with UTC: 1994:057:16:55:25 should get 0004899325"
./time_converter -UTC_to_MET 1994:057:16:55:25
./perl_time.pl time2met 1994:057:16:55:25
./new_perl_time.pl time2met 1994:057:16:55:25
echo ""
echo "starting with UTC:  2019:160:15:21:30"
./time_converter -UTC_to_MET 2019:160:15:21:30
./perl_time.pl time2met 2019:160:15:21:30
./new_perl_time.pl time2met 2019:160:15:21:30
echo ""
echo ""
echo "Testing MET to MJD"
echo ""
echo "starting with MET:  01234567890"
./time_converter -MET_to_MJD 01234567890
./perl_time.pl met2mjd 01234567890
./new_perl_time.pl met2mjd 01234567890
echo ""
echo ""
echo "Testing MJD to MET"
echo ""
echo "starting with MJD:  63641.980162037 + 0.00011574074  (extra 10 sec)"
echo "starting with MJD:  63641.98027777774"
./time_converter -MJD_to_MET 63641.98027777774
./perl_time.pl mjd2met 63641.98027777774
./new_perl_time.pl mjd2met 63641.98027777774
echo ""
echo ""
echo "Testing YD to YMD"
echo ""
echo "starting with YD: 1998:220"
./time_converter -YD_to_YMD_digits 1998:220
./perl_time.pl day2date 1998 220
./new_perl_time.pl day2date 1998 220
echo ""
echo "Note: Toshi's script only retrned 2 digits for the year, but"
echo "the new program is designed to return a 4 digit year."
echo ""
echo ""
echo "Testing get current time"
echo ""
./time_converter -current_UTC
./perl_time.pl nowtime
./new_perl_time.pl nowtime
echo ""
echo ""
echo "Testing YYYY:DDD to mission week"
echo ""
echo "starting with 1998:144"
./time_converter -YD_to_mission_week 1998:144
./perl_time.pl xte_week 1998 144
./new_perl_time.pl xte_week 1998 144
echo ""
echo ""
echo "Testing YYYY:DDD to mission day"
echo ""
echo "starting with 1998:144"
./time_converter -YD_to_mission_day 1998:144
./perl_time.pl xte_mission_day 1998 144
./new_perl_time.pl xte_mission_day 1998 144
echo ""
echo ""
echo "Testing YYYY:MM:DD to DDD (day of year)"
echo ""
echo "starting with 1998:12:31"
./time_converter -YMD_to_DDD 1998:12:31
./perl_time.pl date2day 1998 12 31
./new_perl_time.pl date2day 1998 12 31
echo ""
echo ""
echo ""
echo "######################################################################"
echo "START OF THE SECOND SET OF TESTS"
echo "This set of tests gives one example of each type of option to the "
echo "time_converter program."
echo "######################################################################"
echo ""
echo './time_converter -MET_to_UTC 0177344538'
./time_converter -MET_to_UTC 00177344538
echo ""
echo ""
echo './time_converter -UTC_to_MET 1994:001:00:00:01'
./time_converter -UTC_to_MET 1994:001:00:00:01
echo ""
echo ""
echo './time_converter -MET_to_MJD 1998:001:12:00:00'
./time_converter -MET_to_MJD 1998:001:12:00:00
echo ""
echo ""
echo './time_converter -MJD_to_MET 55200'
./time_converter -MJD_to_MET 55200
echo ""
echo ""
echo './time_converter -YD_to_YMD 2010:032'
./time_converter -YD_to_YMD 2010:032
echo ""
echo ""
echo './time_converter -YD_to_YMD_digits 2010:032'
./time_converter -YD_to_YMD_digits 2010:032
echo ""
echo ""
echo './time_converter -YMD_to_DDD 2015:12:31'
./time_converter -YMD_to_DDD 2015:12:31
echo ""
echo ""
echo './time_converter -current_MET'
./time_converter -current_MET
echo ""
echo ""
echo './time_converter -YD_to_mission_WEEK 1998:290'
./time_converter -YD_to_mission_WEEK 1998:290
echo ""
echo ""
echo './time_converter -YD_to_mission_day 1998:290'
./time_converter -YD_to_mission_day 1998:290
echo ""
echo ""
echo './time_converter -current_mission_day'
./time_converter -current_mission_day
echo ""
echo ""
echo './time_converter -current_mission_DAY'
./time_converter -current_mission_DAY
echo ""
echo ""
echo './time_converter -current_mission_week'
./time_converter -current_mission_week
echo ""
echo ""
echo './time_converter -current_mission_WEEK'
./time_converter -current_mission_WEEK
echo ""
echo ""
echo './time_converter -current_MJD'
./time_converter -current_MJD
echo ""
echo ""
echo './time_converter -YD_to_mission_week 1998:290'
./time_converter -YD_to_mission_week 1998:290
echo ""
echo ""
echo './time_converter -YD_to_mission_DAY 1998:290'
./time_converter -YD_to_mission_DAY 1998:290
echo ""
echo ""
echo './time_converter -MET_to_day 0193588902'
./time_converter -MET_to_day 0193588902
echo ""
echo ""
echo './time_converter -MET_to_DAY 0193588902'
./time_converter -MET_to_DAY 0193588902
echo ""
echo ""
echo './time_converter -MET_to_week 0193588902'
./time_converter -MET_to_week 0193588902
echo ""
echo ""
echo './time_converter -MET_to_WEEK 0193588902'
./time_converter -MET_to_WEEK 0193588902
echo ""
echo ""
echo './time_converter -current_UTC'
./time_converter -current_UTC
echo ""
echo ""
echo './time_converter -UTC_to_MJD 2000:001:00:00:00'
./time_converter -UTC_to_MJD 2000:001:00:00:00
echo ""
echo ""
echo './time_converter -UTC_to_day 1998:294:09:12:00'
./time_converter -UTC_to_day 1998:294:09:12:00
echo ""
echo ""
echo './time_converter -UTC_to_DAY 1998:294:09:12:00'
./time_converter -UTC_to_DAY 1998:294:09:12:00
echo ""
echo ""
echo './time_converter -UTC_to_week 1998:294:09:12:00'
./time_converter -UTC_to_week 1998:294:09:12:00
echo ""
echo ""
echo './time_converter -UTC_to_WEEK 1998:294:09:12:00'
./time_converter -UTC_to_WEEK 1998:294:09:12:00
echo ""
echo ""
echo './time_converter -MJD_to_UTC 64233.5'
./time_converter -MJD_to_UTC 64233.5
echo ""
echo ""
echo './time_converter -YD_to_YMD 1998:220'
./time_converter -YD_to_YMD 1998:220
echo ""
echo ""
echo ""
echo "######################################################################"
echo "START OF THE THIRD SET OF TESTS"
echo "This set of tests shows the Y2K compliance of the"
echo "time_converter_conversions."
echo "######################################################################"
echo ""
echo "./time_converter -UTC_to_MJD 1999:365:23:59:59"
./time_converter -UTC_to_MJD 1999:365:23:59:59
echo ""
echo "./time_converter -UTC_to_MJD 2000:001:00:00:01"
./time_converter -UTC_to_MJD 2000:001:00:00:01
echo ""
echo "./time_converter -UTC_to_MJD 2000:059:12:00:00"
./time_converter -UTC_to_MJD 2000:059:12:00:00
echo ""
echo "./time_converter -UTC_to_MJD 2000:060:12:00:00"
./time_converter -UTC_to_MJD 2000:060:12:00:00
echo ""
echo "./time_converter -YD_to_YMD 1999:365"
./time_converter -YD_to_YMD 1999:365
echo ""
echo "./time_converter -YD_to_YMD 2000:001"
./time_converter -YD_to_YMD 2000:001
echo ""
echo "./time_converter -YD_to_YMD 2000:059"
./time_converter -YD_to_YMD 2000:059
echo ""
echo "./time_converter -YD_to_YMD 2000:060"
./time_converter -YD_to_YMD 2000:060
echo ""
echo "./time_converter -YD_to_YMD 2000:061"
./time_converter -YD_to_YMD 2000:061
echo ""
echo "./time_converter -YD_to_YMD 2000:364"
./time_converter -YD_to_YMD 2000:364
echo ""
echo "./time_converter -YD_to_YMD 2000:365"
./time_converter -YD_to_YMD 2000:365
echo ""
echo "./time_converter -YD_to_YMD 2000:366"
./time_converter -YD_to_YMD 2000:366
echo ""

@


1.1
log
@Initial revision
@
text
@d3 2
@

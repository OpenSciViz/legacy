head	4.2;
access
	dhon
	rbarnett
	soccm
	jonv;
symbols
	Build4_3_1:4.1
	Build4:3.4
	Build3_3:3.3
	Build3_2:3.1
	Build3_1:3.1
	Build3:3.1
	Current:3.2;
locks;
comment	@ * @;


4.2
date	98.11.11.13.38.01;	author soccm;	state Exp;
branches;
next	4.1;

4.1
date	95.05.25.15.07.03;	author rbarnett;	state Exp;
branches;
next	3.5;

3.5
date	95.05.25.14.48.39;	author rbarnett;	state Exp;
branches;
next	3.4;

3.4
date	95.02.15.15.10.11;	author rbarnett;	state Exp;
branches;
next	3.3;

3.3
date	94.10.18.17.55.41;	author rbarnett;	state Exp;
branches;
next	3.2;

3.2
date	94.09.20.13.56.14;	author dhon;	state Exp;
branches;
next	3.1;

3.1
date	94.06.15.14.27.16;	author rbarnett;	state Exp;
branches;
next	1.9;

1.9
date	94.06.15.13.25.35;	author rbarnett;	state Exp;
branches;
next	1.8;

1.8
date	94.02.25.14.54.40;	author lijewski;	state Exp;
branches;
next	1.7;

1.7
date	94.01.28.14.17.04;	author lijewski;	state Exp;
branches;
next	1.6;

1.6
date	94.01.24.18.07.43;	author lijewski;	state Exp;
branches;
next	1.5;

1.5
date	94.01.19.15.19.36;	author lijewski;	state Exp;
branches;
next	1.4;

1.4
date	94.01.11.20.59.41;	author lijewski;	state Exp;
branches;
next	1.3;

1.3
date	94.01.07.15.34.56;	author lijewski;	state Exp;
branches;
next	1.2;

1.2
date	94.01.04.22.54.49;	author lijewski;	state Exp;
branches;
next	1.1;

1.1
date	94.01.04.22.30.19;	author lijewski;	state Exp;
branches;
next	;


desc
@initial cut at Externs file
@


4.2
log
@commented out the prototype for "gettimeofday" since the
new compiler gets it right
@
text
@// ===========================================================================
// File Name   : Externs.h
// Subsystem   : Utilities
// Programmer  : Mike Lijewski, Hughes STX
//               Randall D. Barnette, Hughes STX
// Description :
//
//
// RCS: $Id: Externs.h,v 4.1 1995/05/25 15:07:03 rbarnett Exp $
//
// .NAME    Externs - Prototypes for library functions
// .LIBRARY Util
// .HEADER  Utilities
// .INCLUDE Externs.h
// .VERSION $Revision: 4.1 $
// ===========================================================================

// .SECTION DESCRIPTION
// This file is intended to contain all of the extern "C" constructs needed
// by the XTESOC software. They're collected in one placed in order to
// facilitate porting, as well as to allow others to benefit from the
// experience of porting efforts. Ideally, such a file shouldn't be needed,
// but I've yet to run into a C++ system that properly prototyped all of
// its functions. A given function prototype should only appear once in
// this file.
//
// The C preprocessor defines we currently use are:

// .SECTION #define sgi
// Signifies the SGI C++ compiler.

// .SECTION #define ultrix
// Signifies Cfront 3.0 on Dec Ultrix.

// .SECTION #define __CLCC__
// Signifies ObjectCenter (OC) running on the SOC Suns
// under SunOS 4. Note that this is defined only when compiling
// with the OC compiler. You must explicitly set it for the OC interpreter.

// .SECTION #define __CLCC204__
// Signifies ObjectCenter (OC) version 2.0.4 running on the SOC Suns under
// SunOS 4. This symbol is added to the CL 2.0.4 CC shell script by the
// SOC to distinguish this version from later ones.

// .SECTION #define __CLCC206__
// Signifies ObjectCenter (OC) version 2.0.6 running on the SOC Suns under
// Solaris. This symbol is added to the CL 2.0.6 CC shell script by the
// SOC to distinguish this version from later ones.

// .SECTION #define __CLCC210__
// Signifies ObjectCenter (OC) version 2.1 running on the SOC Suns under
// SunOS 4. This symbol is added to the CL 2.1 CC shell script by the
// SOC to distinguish this version from later ones.

// .SECTION #define __CLCC211__
// Signifies ObjectCenter (OC) version 2.1 running on the SOC Suns under
// Solaris. This symbol is added to the CL 2.1 CC shell script by the
// SOC to distinguish this version from later ones.

// .SECTION #define sun
// Signifies Sun Sparcs running Solaris with Cfront 3.0
// and the Sun ANSI C compiler.

// .SECTION AUTHOR
// Randall D. Barnette,
// Hughes STX Corp.,
// <rbarnett@@xema.stx.com>

#ifndef EXTERNS_H
#define EXTERNS_H

#include <stddef.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>

extern "C" {

// These are not prototyped or improperly prototyped in
// Silicon Graphics (IRIX)

#if defined(sgi)
  void bzero  (char*, int);
  long random (void);
  int  srandom(unsigned);
  int  select (int, fd_set*, fd_set*, fd_set*, struct timeval*);
  void syslog (int, const char*, ...);
#endif

// These are not prototyped or improperly prototyped in
// DEC Ultrix

#if defined(ultrix)
  int           ftruncate(int, int);
  unsigned long htonl    (unsigned long);
  unsigned long htons    (unsigned short);
  int           semget   (key_t, int, int);
  int           semctl   (int, int, int, union semun);
  int           semop    (int, struct sembuf*, int);
  char*         shmat    (int, char*, int);
  int           shmctl   (int, int, struct shmid_ds*);
  int           shmget   (key_t, int, int);
  //
  // For some reason this union doesn't seem to be defined anywhere.
  //
  union semun {
    int     val;
    struct  semid_ds *buf;
    ushort* array;
  };
#endif

// These are not prototyped or improperly prototyped in
// all CenterLine versions

#if defined(__CLCC204__)
  int ftruncate(int, off_t);
  int rename   (const char*, const char*);
#endif

// These are not prototyped or improperly prototyped in
// CenterLine version 2.0.2 and in Ultrix

#if defined(ultrix)
  int open       (const char*, int, ...);
  int sigaction  (int, const struct sigaction*, struct sigaction*);
  int sigprocmask(int, const sigset_t*, sigset_t*);
#endif

// These are not prototyped or improperly prototyped in
// CenterLine versions 2.0.2 and 2.0.4 and in Ultrix

#if defined(__CLCC204__) || defined(ultrix)
  int mkdir(const char*, int);
  int umask(int);
  int fchmod(int, int);
#endif

// These are not prototyped or improperly prototyped in
// CenterLine, Ultrix, and Silicon Graphics

#if defined(__CLCC__) || defined(ultrix) || defined(sgi)
  int fsync      (int);
  int sigaddset  (sigset_t*, int);
  int sigdelset  (sigset_t*, int);
  int sigemptyset(sigset_t*);
  int sigfillset (sigset_t*);       
#endif

// These are not prototyped or improperly prototyped in
// SparcWorks, CenterLine, Ultrix, and Silicon Graphics

#if defined(sun) || defined(__CLCC__) || defined(ultrix) || defined(sgi)
  int gethostname(char*, int);
#endif

//#if !defined(BSD)
//  int gettimeofday(struct timeval*);
//#endif

}

#endif /* EXTERNS_H */
@


4.1
log
@Build freeze
@
text
@d9 1
a9 1
// RCS: $Id: Externs.h,v 3.5 1995/05/25 14:48:39 rbarnett Exp $
d15 1
a15 1
// .VERSION $Revision: 3.5 $
d158 3
a160 3
#if !defined(BSD)
  int gettimeofday(struct timeval*);
#endif
@


3.5
log
@update to genman text.
@
text
@d9 1
a9 1
// RCS: $Id: Externs.h,v 3.4 1995/02/15 15:10:11 rbarnett Exp $
d15 1
a15 1
// .VERSION $Revision$
@


3.4
log
@genman comments
@
text
@d9 1
a9 1
// RCS: $Id: Externs.h,v 3.5 1995/02/07 20:28:58 rbarnett Exp $
d15 1
a15 1
// .VERSION XTE-SOC V4.1
@


3.3
log
@Update for Solaris
@
text
@d8 11
d22 4
a25 3
// experience of others. Ideally, such a file shouldn't be needed, but I've
// yet to run into a C++ system that properly prototyped all of its functions.
// A given function prototype should only appear once in this file.
d28 40
a67 22
//
//    sgi         -- signifies the SGI C++ compiler.
//
//    ultrix      -- signifies Cfront 3.0 on Dec Ultrix.
//
//    __CLCC__    -- signifies ObjectCenter (OC) running on the SOC Suns
//                   under SunOS 4. This will go away when the SOC migrates to
//                   Solaris. Note that this is defined only when compiling
//                   with the OC compiler. You must explicitly set it for the
//                   OC interpreter.
//
//    __CLCC202__ -- signifies ObjectCenter (OC) version 2.0.2 running on the
//                   SOC Suns under SunOS 4. This symbol is added to the
//                   CL 2.0.2 CC shell script by the SOC to distinguish
//                   this version from later ones.
//
//    sun         -- signifies Sun Sparcs running Solaris with Cfront 3.0
//                   and the Sun ANSI C compiler.
//
// RCS: $Id: Externs.h,v 3.1 1994/06/15 14:27:16 rbarnett Exp $
//
// ===========================================================================
a91 7
// SparcWorks

#if defined(sun) && !defined(__CLCC__)
  int gettimeofday(struct timeval *);
#endif

// These are not prototyped or improperly prototyped in
d117 1
a117 1
#if defined(__CLCC__)
a122 9
// CenterLine AND (?????) GNU C++

#if defined(__CLCC__) && defined(__GNUG__)
  int munmap (caddr_t, int);
  int openlog(const char*, int, int);
  int syslog (int, char*, ...);
#endif

// These are not prototyped or improperly prototyped in
d125 1
a125 1
#if defined(__CLCC202__) || defined(ultrix)
d132 1
a132 1
// all CenterLine versions and in Ultrix
d134 1
a134 1
#if defined(__CLCC__) || defined(ultrix)
d137 1
a143 1
  int fchmod     (int, int);
d157 4
@


3.2
log
@new sgi CC and OS
@
text
@a122 1
  int fchmod(int, int);
d129 1
@


3.1
log
@Fixed some comments, added header
@
text
@d35 1
a35 1
// RCS: $Id$
d123 1
a129 1
  int fchmod     (int, int);
@


1.9
log
@Updated to distinguish between CL2.0.2 and CL2.0.4
@
text
@d1 6
d9 1
a9 1
// by the XTESOC software.  They're collected in one placed in order to
d11 1
a11 1
// experience of others.  Ideally, such a file shouldn't be needed, but I've
d28 3
a30 1
//                   SOC Suns under SunOS 4.
d35 1
a35 1
// $Id: Externs.h,v 1.8 1994/02/25 14:54:40 lijewski Exp rbarnett $
d37 1
d48 1
a48 2
extern "C"
{
d54 6
a59 6
    void bzero  (char*, int);
    long random (void);
    int  srandom(unsigned);
    int  select (int, fd_set*, fd_set*, fd_set*, struct timeval*);
    void syslog (int, const char*, ...);
#endif /* sgi */
d65 2
a66 2
    int gettimeofday (struct timeval *);
#endif /* sun && !__CLCC__ */
d72 18
a89 18
    int           ftruncate (int, int);
    unsigned long htonl     (unsigned long);
    unsigned long htons     (unsigned short);
    int           semget    (key_t, int, int);
    int           semctl    (int, int, int, union semun);
    int           semop     (int, struct sembuf*, int);
    char*         shmat     (int, char*, int);
    int           shmctl    (int, int, struct shmid_ds*);
    int           shmget    (key_t, int, int);
    //
    // For some reason this type doesn't seem to be defined anywhere.
    //
    union semun {
        int val;
        struct semid_ds *buf;
        ushort* array;
    };
#endif /* ultrix */
d95 3
a97 3
    int ftruncate (int, off_t);
    int rename    (const char*, const char*);
#endif /* __CLCC__ */
d100 1
a100 1
// CenterLine AND(?????) GNU C++
d103 4
a106 4
    int munmap  (caddr_t, int);
    int openlog (const char*, int, int);
    int syslog  (int, char*, ...);
#endif /* __CLCC__ && __CNUG__ */
d112 4
a115 4
    int open        (const char*, int, ...);
    int sigaction   (int, const struct sigaction*, struct sigaction*);
    int sigprocmask (int, const sigset_t*, sigset_t*);
#endif /* __CLCC202__ || ultrix */
d121 3
a123 3
    int mkdir       (const char*, int);
    int umask       (int);
#endif /* sgi */
d128 8
a135 8
#if defined(__CLCC__)  || defined(ultrix) || defined(sgi)
    int fchmod      (int, int);
    int fsync       (int);
    int sigaddset   (sigset_t*, int);
    int sigdelset   (sigset_t*, int);
    int sigemptyset (sigset_t*);
    int sigfillset  (sigset_t*);       
#endif /* sgi */
d138 5
a142 5
// SparcWorksm CenterLine, Ultrix, and Silicon Graphics

#if defined(sun) || defined(__CLCC__)  || defined(ultrix) || defined(sgi)
    int gethostname (char*, int);
#endif /* sgi */
d146 1
a146 1
#endif /*EXTERNS_H*/
@


1.8
log
@added <sys/stat.h>
@
text
@d11 1
a11 1
//    sgi      -- this signifies the SGI C++ compiler.
d13 1
a13 1
//    ultrix   -- this signifies Cfront 3.0 on Dec Ultrix.
d15 5
a19 5
//    __CLCC__ -- this signifies ObjectCenter (OC) running on the SOC Suns
//                under SunOS 4.  This will go away when the SOC migrates to
//                Solaris.  Note that this is defined only when compiling with
//                the OC compiler.  You must explicitly set it for the OC
//                interpreter.
d21 2
a22 2
//    sun      -- this signifies Sun Sparcs running Solaris with Cfront 3.0
//                 and the Sun ANSI C compiler.
d24 2
a25 1
// $Id: Externs.h,v 1.7 1994/01/28 14:17:04 lijewski Exp lijewski $
d27 2
a32 1

a38 1

d42 3
d48 1
a48 1
    int srandom (unsigned);
d51 1
a51 1
#endif
d53 3
d58 4
a61 1
#endif
d63 1
a63 1
#ifdef ultrix
d76 1
a76 2
    union semun
    {
d81 1
a81 1
#endif
d83 3
d89 4
a92 1
#endif
d98 1
a98 1
#endif
d100 4
a103 2
#if defined(__CLCC__) || defined(ultrix)
    int mkdir       (const char*, int);
d107 4
a110 1
#endif
d113 1
d115 4
a118 1
#endif
d127 4
a130 1
#endif
d134 1
a134 1
#endif
@


1.7
log
@Solaris seems to have umask() now.
@
text
@d24 1
a24 1
// $Id: Externs.h,v 1.6 1994/01/24 18:07:43 lijewski Exp lijewski $
d34 1
@


1.6
log
@modes to support latest installation of Cfront on ultrix at MIT
@
text
@d24 1
a24 1
// $Id: Externs.h,v 1.5 1994/01/19 15:19:36 lijewski Exp lijewski $
d91 1
a91 1
#if defined(__CLCC__) || defined(ultrix) || defined(sun)
@


1.5
log
@added more ultrix stuff (dup2, pipe, close, write)
@
text
@d24 1
a24 1
// $Id: Externs.h,v 1.4 1994/01/11 20:59:41 lijewski Exp lijewski $
d31 1
a52 4
    int           chmod     (char*, mode_t);
    int           close     (int);
    int           dup2      (int, int);
    int           flock     (int, int);
a55 1
    int           pipe      (int fd[2]);
a61 2
    int           waitpid   (pid_t, union wait *, int);
    int           write     (int, char*, int);
d75 1
a86 1
    int rename      (const char*, const char*);
@


1.4
log
@added missing umask() SunOS5
@
text
@d24 1
a24 1
// $Id: Externs.h,v 1.3 1994/01/07 15:34:56 lijewski Exp lijewski $
d53 2
d58 2
a59 1
    unsigned long htons     (unsigned short);      
d67 1
@


1.3
log
@added gethostname() and ftruncate()
@
text
@d24 1
a24 1
// $Id: Externs.h,v 1.2 1994/01/04 22:54:49 lijewski Exp lijewski $
d91 3
@


1.2
log
@added a few more comments
@
text
@d24 1
a24 1
// $Id: Externs.h,v 1.1 1994/01/04 22:30:19 lijewski Exp lijewski $
d52 12
a63 11
    int           chmod   (char*, mode_t);
    int           flock   (int, int);
    unsigned long htonl   (unsigned long);
    unsigned long htons   (unsigned short);      
    int           semget  (key_t, int, int);
    int           semctl  (int, int, int, union semun);
    int           semop   (int, struct sembuf*, int);
    char*         shmat   (int, char*, int);
    int           shmctl  (int, int, struct shmid_ds*);
    int           shmget  (key_t, int, int);
    int           waitpid (pid_t, union wait *, int);
d75 4
d86 4
a89 4
    int mkdir     (const char*, int);
    int open      (const char*, int, ...);
    int rename    (const char*, const char*);
    int sigaction (int, const struct sigaction*, struct sigaction*);
d91 1
a91 1
    int umask     (int);
d101 5
a105 1
#endif                                                     
@


1.1
log
@Initial revision
@
text
@d2 6
a7 5
// This file contains most of the extern "C"s needed by the Ingest subsystem,
// placed in one place to make porting easier.  Ideally, such a file wouldn't
// be needed, but I've yet to run into a C++ system that properly prototyped
// all of its functions.  A given function prototype should only appear once
// in this file.
d9 1
a9 1
// $Id: ingest-ext.h,v 2.4 1993/11/26 16:04:11 lijewski Release $
d11 3
a13 4

#ifndef INGEST_EXT_H
#define INGEST_EXT_H

d15 5
a19 1
// sgi      -- this signifies the SGI C++ compiler.
d21 2
a22 1
// ultrix   -- this signifies Cfront 3.0 on Dec Ultrix.
d24 1
a24 2
// __CLCC__ -- this signifies ObjectCenter running on the SOC Suns under
//              SunOS 4.  This will go away when the SOC migrates to Solaris.
a25 2
// sun      -- this signifies Sun Sparcs running Solaris with Cfront 3.0 and
//             the Sun ANSI C compiler.
d27 4
d35 1
d100 1
a100 1
#endif /*INGEST_EXT_H*/
@

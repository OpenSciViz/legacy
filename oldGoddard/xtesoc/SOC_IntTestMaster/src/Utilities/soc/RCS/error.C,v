head	4.1;
access
	dhon
	rbarnett
	soccm
	jonv;
symbols
	Build4_3_1:4.1
	Build4:1.4
	Build3_3:1.3
	Build3_2:1.3
	Build3_1:1.3
	Build3:1.3
	Current:1.3;
locks;
comment	@ * @;


4.1
date	95.05.25.15.07.03;	author rbarnett;	state Exp;
branches;
next	1.4;

1.4
date	95.02.15.15.10.11;	author rbarnett;	state Exp;
branches;
next	1.3;

1.3
date	94.01.11.21.06.39;	author lijewski;	state Exp;
branches;
next	1.2;

1.2
date	94.01.05.15.52.37;	author lijewski;	state Exp;
branches;
next	1.1;

1.1
date	94.01.04.23.34.57;	author lijewski;	state Exp;
branches;
next	;


desc
@initial entry into libUtil.a
@


4.1
log
@Build freeze
@
text
@//
// Output routines.
//

static const char rcsid[] = "$Id: error.C,v 1.4 1995/02/15 15:10:11 rbarnett Exp $";


#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>

#include "Externs.h"
#include "Utilities.h"


//
// Some POSIX stuff.
//
#ifndef EXIT_SUCCESS
#define EXIT_SUCCESS 0
#endif
#ifndef EXIT_FAILURE
#define EXIT_FAILURE 1
#endif


//
// This forces open the connection between us and syslogd().  Before this is
// called error(), syserror() and message() output to stderr.  After this is
// called they output via syslogd().  All messages that go to syslogd are also
// echoed to the system console. `program' is the name of the program that
// is calling syslogd.  The syslogd facility is set to LOG_DAEMON.  This is
// called by daemonize().
//

static int using_syslogd = 0;

void initialize_syslogd (const char* program)
{
    using_syslogd = 1;
    openlog(program, LOG_CONS | LOG_NDELAY, LOG_DAEMON);
}


//
// Our error-handling function.  This assume that all error message already
// have their final punctuation.  If daemonize() has been called all error
// messages are output with LOG_ERR priority via syslogd(); otherwise they're
// output to stderr.  Exits via exit() after the message has been output.
//

void error (const char *format, ...)
{
    char buffer[512]; // Should be large enough.
    va_list ap;
    va_start(ap, format);
    (void) vsprintf(buffer, format, ap);
    va_end(ap);
    if (using_syslogd)
        syslog(LOG_ERR, buffer);
    else
        (void) fprintf(stderr, "%s\n", buffer);
    exit(EXIT_FAILURE);
}


//
// Returns a static string containing a system call error message.
// This is a helper function for syserror() when we're debugging.
// We assume that the errno is valid.
//

static const char* sys_error_msg (void)
{
    const int LEN = 256;
    static char msg[LEN];

#if defined(BSD) || defined(ultrix)
    (void) strcpy(msg, sys_errlist[errno]);
#else
    (void) strcpy(msg, strerror(errno));
#endif

    return msg;
}


//
// Our system call error-handling function.  This outputs the passed error
// message together with a system-dependant error message indexed by errno.
// It should only be called immediately after a system call error.
// If daemonize() has been called all messages are output with LOG_ERR
// priority via syslogd(); otherwise they're output to stderr.  Exits via
// exit() after the message has been output.
//

void syserror (const char *format, ...)
{
    char buffer[512]; // Should be large enough.
    va_list ap;
    va_start(ap, format);
    (void) vsprintf(buffer, format, ap);
    va_end(ap);
    if (using_syslogd)
    {
        (void) strcat(buffer, ", %m.");  // Append syscall error string.
        syslog(LOG_ERR, buffer);
    }
    else
        (void) fprintf(stderr, "%s, %s\n", buffer, sys_error_msg());
    exit(EXIT_FAILURE);
}


//
// Our message-handling function.  This assume that all messages already have
// their final punctuation.  If daemonize() has been called, they're output
// with LOG_INFO priority via syslogd(); otherwise they're output to stderr.
//

void message (const char *format, ...)
{
    static char buffer[512]; // Should be large enough.
    va_list ap;
    va_start(ap, format);
    (void) vsprintf(buffer, format, ap);
    va_end(ap);
    if (using_syslogd)
        syslog(LOG_INFO, buffer);
    else
        (void) fprintf(stderr, "%s\n", buffer);
}
@


1.4
log
@genman comments
@
text
@d5 1
a5 1
static const char rcsid[] = "$Id: error.C,v 1.4 1995/02/07 20:28:20 rbarnett Exp $";
@


1.3
log
@added rcsid
@
text
@d5 1
a5 1
static const char rcsid[] = "$Id$";
d82 1
a82 1
#if defined(__CLCC__) || defined(ultrix)
@


1.2
log
@got it working
@
text
@d2 1
a2 1
// $Id: error.C,v 1.1 1994/01/04 23:34:57 lijewski Exp lijewski $
d5 1
d7 2
@


1.1
log
@Initial revision
@
text
@d2 1
a2 1
// $Id$
d5 8
d14 12
a25 1
#include "utilities.h"
d31 4
a34 1
// called they output via syslogd().
d47 4
a50 5
// Our error-handling function.  This assumes that /etc/syslog.conf is set up
// to send all LOG_ERR messages both to /dev/console and to /usr/adm/messages
// or other system-defined logfile.  We assume that all error message already
// have their final punctuation.  Before initialize_syslogd() is called,
// we output errors to stderr; afterwards they're output via syslogd().
d63 1
a63 1
        cerr << buffer << "\n";
d90 6
a95 7
// Our system call error-handling function.  This should only be called when a
// system call fails.  This assumes that /etc/syslog.conf is set up to send all
// LOG_ERR messages both to /dev/console and to /usr/adm/messages or other
// system-defined logfile.  We asssume that syscall error messages don't have
// any final punctuation.  We add a comma between the passed message and the
// syscall error string itself.  Before initialize_syslogd() is called,
// we output errors to stderr; afterwards they're output via syslogd().
d111 1
a111 1
        cerr << buffer << ", " << sys_error_msg() << "\n";
d117 3
a119 5
// Our message function -- prints message to /dev/console and logs it to a
// file.  This assumes that /etc/syslog.conf is set up to send all LOG_ERR
// messages both to /dev/console and to /usr/adm/messages or other
// system-defined logfile.  Before initialize_syslogd() is called,
// we output errors to stderr; afterwards they're output via syslogd().
d130 1
a130 1
        syslog(LOG_ERR, buffer);
d132 1
a132 1
        cerr << buffer << "\n";
@

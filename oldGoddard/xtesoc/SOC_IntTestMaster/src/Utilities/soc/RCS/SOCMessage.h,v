head	4.3;
access
	rbarnett
	soccm
	jonv;
symbols
	Build4_5_3:4.2
	Build4_3_1:4.1
	Build4_2:3.10
	Build4_1:3.9
	Build4:3.8
	Build3_3:3.6
	Build3_2:3.6;
locks;
comment	@ * @;


4.3
date	97.12.05.17.32.40;	author jonv;	state Exp;
branches;
next	4.2;

4.2
date	95.11.02.21.16.21;	author rbarnett;	state Exp;
branches;
next	4.1;

4.1
date	95.05.25.15.07.03;	author rbarnett;	state Exp;
branches;
next	3.11;

3.11
date	95.05.25.14.48.39;	author rbarnett;	state Exp;
branches;
next	3.10;

3.10
date	95.03.22.17.05.44;	author prao;	state Exp;
branches;
next	3.9;

3.9
date	95.02.21.15.09.15;	author prao;	state Exp;
branches;
next	3.8;

3.8
date	95.02.15.15.02.59;	author rbarnett;	state Exp;
branches;
next	3.7;

3.7
date	94.12.01.16.25.44;	author rbarnett;	state Exp;
branches;
next	3.6;

3.6
date	94.09.16.17.12.25;	author rbarnett;	state Exp;
branches;
next	3.5;

3.5
date	94.08.31.15.24.53;	author rbarnett;	state Exp;
branches;
next	3.4;

3.4
date	94.08.08.21.31.06;	author bfoutz;	state Exp;
branches;
next	3.3;

3.3
date	94.08.02.16.48.59;	author bfoutz;	state Exp;
branches;
next	3.2;

3.2
date	94.07.28.15.56.50;	author bfoutz;	state Exp;
branches;
next	3.1;

3.1
date	94.07.21.20.53.43;	author rbarnett;	state Exp;
branches;
next	;


desc
@SOCMessage declaration.
@


4.3
log
@added new message behavior for issuing pages
@
text
@// ===========================================================================
// File Name   : SOCMessage.h
// Subsystem   : Utilities
// Programmer  : Randall D. Barnette, Hughes STX
// Description : Contains the declaration of SOCMessage and subclasses.
//
// RCS: $Id: SOCMessage.h,v 4.2 1995/11/02 21:16:21 rbarnett Exp $
//
// .NAME    SOCMessage - A class to manage all SOC logged messages
// .LIBRARY Util
// .HEADER  Utilities  
// .INCLUDE SOCMessage.h
// .FILE    SOCMessage.C
// .VERSION $Revision: 4.2 $
// ===========================================================================

#ifndef SOCMESSAGE_H
#define SOCMESSAGE_H

// .SECTION DESCRIPTION
// There are six different Message classes that differ primarily in their
// default behavior. They are intended to consolidate all XTE SOC
// message activities into a single class hierarchy. SOC Messages are
// used in two different ways. 1) The global function SOCThrow takes
// a behavior argument and a printf style format with arguments, or
// 2) a user may declare a SOCMessage object and invoke it when
// appropriate.
//
// SOCMessage uses syslog to send messages to other hosts, to users,
// and to SOC message files.
// 
// The SOCMessage package consists of the SOCMessageManageer class,
// the base class SOCMessageObject, the predefined 'behavior' classes
// SOCNone, SOCDebug, SOCInfo, SOCReport, SOCResponse, and SOCError.
// There are also two global functions SOCThrow which "throw" an
// error message or a message object.

// .SECTION SEE ALSO
// syslog(3)

// .SECTION BUGS
// For the sake of a crude alarm GUI, a string "SOC_SEVR=n" is inserted
// into the message text. This approach clutters the syslog messages.
// It will be removed when a better implementation is available.

// .SECTION SYSLOG ISSUES
// SOCMessage operates through the syslog(1) mechanism. Each host using
// SOCMessage must have its syslog.conf file configured properly. Generally
// there is a master host which controls the actual file writing, and remote
// hosts which forward all syslog messages to the master. Each type needs
// different configurations. 

// The following must be added to the master host /etc/syslog.conf file for
// proper operation.
/*
# ==========================================================================
#                      XTE Message Logging
#
# The following lines are developed to coordinate with the XTE SOCMessage
# class provided for reliable error/message logging. There were two
# attempts at SOCMessage; both are supported here. Items with "facility"
# local0 are the old style. Items with local1, local2, local3, and local4
# are the new style. They do not conflict. Both can exist simultaneously.
#
# ** All other references to local[0-4] facilities should be deleted. **
#
# There are three sections here. The first is for all XTE SOF hosts.
# The second is for the XTE message master that actually writes the output
# files. It should write to LOCAL file systems only.
#
# The third section is for all other hosts in the network. They simply
# forward all messages to the master.
#
# ** Make sure that only one of section 2 & 3 is uncommented **
#
# Briefly, the old style logs different level messages all on the same
# facility but with different "priority". It has an annoying "or higher"
# feature. The new style uses separate facilities, but all at the same
# priority.
#
# See SOCMessage(3) and syslog.conf(4) for more info.
#
# ==========================================================================
#
# ---------------------------------------------------------------------------
# Section 1.
# These are for ALL hosts (master or non-master).
# They prevent XTE logging from going to the normal UNIX message file.
# ---------------------------------------------------------------------------
local0.none;local1.none;local2.none;local3.none;local4.none;local5.none			/var/adm/messages
local0.debug;local1.info;local2.info;local3.info;local4.info;local5.info		/dev/console
# ---------------------------------------------------------------------------
# Section 2.
# This is for non-master hosts.
# They send output to the master. Uncomment it if this host is NOT the
# master.
# ---------------------------------------------------------------------------
local0.info;local1.info;local2.info;local3.info;local4.info;local5.info	@@xsystem
# ---------------------------------------------------------------------------
# Section 3.
# These are for the XTE master host.
# They actually write the file. Uncomment them if this is the master host.
# ---------------------------------------------------------------------------
#
# Send LOCAL0,info (and higher) messages to the messages file
# Send LOCAL0,notice (and higher) messages to the reports file
# Send LOCAL0,err (and higher) messages to the response file
#
#local0.info						/socops/log/messages
#local0.notice						/socops/log/reports
#local0.err						/socops/log/errors
#
# Send LOCAL1 messages to the messages file
# Send LOCAL2 messages to the reports file
# Send LOCAL3 messages to the response file
# Send LOCAL4 messages to the errors file
#
#local1.info						/socops/log/messages
#local2.info						/socops/log/reports
#local3.info						/socops/log/reports
#local4.info						/socops/log/errors
#local5.info						/socops/log/pages
# Done with XTE logging
*/

// .SECTION AUTHOR
// Randall D. Barnette
// <rbarnett@@xema.stx.com>
// Hughes STX Corp.
// .END

// Enumerate the various behaviors of messages in the SOC.

enum SOCMessageBehavior {
  SOC_NONE     = 0, // If logToScreen() is true send message to stderr,
                    // otherwise, don't do anything.
  SOC_DEBUG    = 1, // Log a message to the stderr
  SOC_INFO     = 2, // Log a message to the message log, console, [stderr]
  SOC_REPORT   = 3, // Log a message to the report log, console, [stderr]
  SOC_RESPONSE = 4, // Log a message to the response log, console, [stderr],
                    // wait for response
  SOC_ERROR    = 5, // Log a message to the error log, console, [stderr], exit
  SOC_PAGE     = 6  // Log a message to the page log file and the console;
                    //     do not stop execution.
} ;

void SOCThrow(SOCMessageBehavior behavior, const char* format, ... ) ;

class SOCMessageManager {

public:

  //* Constructor
  SOCMessageManager(void) ;

  //* Destructor
  ~SOCMessageManager(void) ;

  //* Public member functions

  //% Allow user to set and fetch process name.
  //% This should be called in a main program with
  //% SOCMessageManager::issuedBy(argv[0]) ;

  static const char* issuedBy(void) const       ; // Is it set?
  static       void  issuedBy(const char* prog) ; // set it

  //% Setting logToScreen() on will echo all SOCThrow messages to STDERR.

  static int  logToScreen(void)   ; // Is it on?
  static void logToScreen(int on) ; // Turn it on

  //% Setting exitOnError() on will will cause SOC_ERROR messages to exit.

  static int  exitOnError(void)   ; // Is it on?
  static void exitOnError(int on) ; // Turn it on

private:

  int theOriginal ;

  static char* theProgram ;
  static int   toScreen ;
  static int   errorExit ;
  static int   isInitialized ;

  static int  syslogUp(void ) ; // Is it up?
  static void syslogUp(const char* program) ; // set it up

  friend void SOCThrow(SOCMessageBehavior behavior, const char* format, ... ) ;

} ;

// ==========================================================================
// SOCMessageObject Declaration
//  SOCMessageObject is the base class for all LOG/REPORT/ERROR messages.
//  All messages get logged to the SOC system log file
// ==========================================================================

class SOCMessageObject {

public:

  //* Assignment operators

  SOCMessageObject& operator=(const char* msg);
  SOCMessageObject& operator=(const SOCMessageObject& obj);

protected:

  SOCMessageObject(const char* msg) ;

  virtual void throwMessage(void) const = 0 ;
  virtual void throwMessage(const char* msg, ...) const = 0 ;

  const char* message(void) const ;

  friend void SOCThrow(const SOCMessageObject& obj, const char* format, ... );

private:

  char* theMessage ;

} ;

// Description:
// Return the message string associated with this object.

inline const char* SOCMessageObject::message(void) const
{
  return theMessage ;
}

// ==========================================================================
//
// Each of these define a different message behavior
//
// ==========================================================================

class SOCDebug : public SOCMessageObject {

public:

  SOCDebug(const char* msg) ;

  //% Throw the message with or without an additional message.

  virtual void throwMessage(void) const ;
  virtual void throwMessage(const char* format, ...) const ;

} ;

// ==========================================================================

class SOCInfo : public SOCMessageObject {

public:

  SOCInfo(const char* msg) ;

  //% Throw the message with or without an additional message.

  virtual void throwMessage(void) const ;
  virtual void throwMessage(const char* format, ...) const ;

} ;

// ==========================================================================

class SOCReport : public SOCMessageObject {

public:

  SOCReport(const char* msg) ;

  //% Throw the message with or without an additional message.

  virtual void throwMessage(void) const ;
  virtual void throwMessage(const char* format, ...) const ;

} ;

// ==========================================================================

class SOCResponse : public SOCMessageObject {

public:

  SOCResponse(const char* msg) ;

  //% Throw the message with or without an additional message.

  virtual void throwMessage(void) const ;
  virtual void throwMessage(const char* format, ...) const ;

} ;

// ==========================================================================

class SOCError : public SOCMessageObject {

public:

  SOCError(const char* msg) ;

  //% Throw the message with or without an additional message.

  virtual void throwMessage(void) const ;
  virtual void throwMessage(const char* format, ...) const ;

} ;

// ==========================================================================

class SOCPage : public SOCMessageObject {

public:

  SOCPage(const char* msg) ;

  //% Throw the message with or without an additional message.

  virtual void throwMessage(void) const ;
  virtual void throwMessage(const char* format, ...) const ;

} ;

void SOCThrow(const SOCMessageObject& obj, const char* format=0, ... ) ;

#endif /* SOCMESSAGE_H */

@


4.2
log
@removed SEVR message.
Now sends each priority to different file.
@
text
@d7 1
a7 1
// RCS: $Id: SOCMessage.h,v 4.1 1995/05/25 15:07:03 rbarnett Exp $
d14 1
a14 1
// .VERSION $Revision: 4.1 $
d90 2
a91 2
local0.none;local1.none;local2.none;local3.none;local4.none /var/adm/messages
local0.debug;local1.info;local2.info;local3.info;local4.info  /dev/console
d98 1
a98 1
local0.info;local1.info;local2.info;local3.info;local4.info @@xsystem
d109 3
a111 3
#local0.info            /socops/log/messages
#local0.notice            /socops/log/reports
#local0.err           /socops/log/errors
d118 5
a122 4
#local1.info            /socops/log/messages
#local2.info            /socops/log/reports
#local3.info            /socops/log/reports
#local4.info            /socops/log/errors
d142 3
a144 1
  SOC_ERROR    = 5  // Log a message to the error log, console, [stderr], exit
d305 15
@


4.1
log
@Build freeze
@
text
@d7 1
a7 1
// RCS: $Id: SOCMessage.h,v 3.11 1995/05/25 14:48:39 rbarnett Exp $
d14 1
a14 1
// .VERSION $Revision: 3.11 $
d46 79
d136 4
a139 4
  SOC_DEBUG    = 1, // Log a message to the SOC log, console, [stderr]
  SOC_INFO     = 2, // Log a message to the SOC log, console, [stderr]
  SOC_REPORT   = 3, // Log a message to the SOC log, console, SOCcp, [stderr]
  SOC_RESPONSE = 4, // Log a message to the SOC log, console, SOCcp, [stderr],
d141 1
a141 2
  SOC_ERROR    = 5  // Log a message to the SOC log, console, SOCcp, [stderr],
                    // exit
d184 1
a184 1
  static int  syslogUp(void       ) ; // Is it up?
@


3.11
log
@update to genman text.
@
text
@d7 1
a7 1
// RCS: $Id: SOCMessage.h,v 3.10 1995/05/09 16:37:57 soccm Exp $
d14 1
a14 1
// .VERSION $Revision: 3.10 $
@


3.10
log
@remove socket connections (decouple socmessage from socmainalarm)
@
text
@d7 1
a7 1
// RCS: $Id: SOCMessage.h,v 3.9 1995/02/21 15:09:15 prao Exp prao $
d14 1
a14 1
// .VERSION XTE-SOC V4.1
d41 5
d165 1
a165 1
  //% Throw the message with or without a additional message.
d180 1
a180 1
  //% Throw the message with or without a additional message.
d195 1
a195 1
  //% Throw the message with or without a additional message.
d210 1
a210 1
  //% Throw the message with or without a additional message.
d225 1
a225 1
  //% Throw the message with or without a additional message.
@


3.9
log
@Add socmessage buffer for socmainalarm panel
@
text
@d7 1
a7 1
// RCS: $Id: SOCMessage.h,v 3.8 1995/02/15 15:02:59 rbarnett Exp prao $
a60 8
///////////////////////////////////////////
// message structure for socmainalarm panel
struct message_type {
   char buf[512];
   int type_name;
};
//////////////////////////////////////////

@


3.8
log
@revised genman comments
@
text
@d7 1
a7 1
// RCS: $Id: SOCMessage.h,v 3.9 1995/02/03 16:17:53 rbarnett Exp $
d61 8
@


3.7
log
@fixed genman problems
@
text
@d7 1
a7 1
// RCS: $Id: SOCMessage.h,v 3.6 1994/09/16 17:12:25 rbarnett Exp $
d14 1
a14 1
// .VERSION XTE-SOC V4.0
d26 1
a26 1
// 2) a user may declare a SOCMessage object and 'throw' it when
d45 1
a48 4
//#ifdef c_plusplus
//extern "C" {
//#endif

a62 4
//#ifdef c_plusplus
//}
//#endif

d75 3
a77 2
  //% Allow user to set process name.
  //% Most likely done with SOCMessageManager::issuedBy(argv[0]) ;
d229 1
a229 1
#endif /* SOCERROR_H */
@


3.6
log
@update text
@
text
@d7 1
a7 1
// RCS: $Id: SOCMessage.h,v 3.5 1994/08/31 15:24:53 rbarnett Exp rbarnett $
d14 1
a14 2
// .VERSION XTE-SOC V3.2
// .OPTIONS -b -e -public
d26 1
a26 1
// 2) A user may declare a SOCMessage object and "throw" it when
d33 1
a33 1
// the base class SOCMessageObject, the predefined "behavior" classes
@


3.5
log
@removed the BText stuff, updated comments and test routine
@
text
@d7 1
a7 1
// RCS: $Id: SOCMessage.h,v 3.4 1994/08/08 21:31:06 bfoutz Exp rbarnett $
d49 3
a51 3
#ifdef c_plusplus
extern "C" {
#endif
d54 1
a54 1
	SOC_NONE     = 0, // If logToScreen() is true send message to stderr,
d57 3
a59 3
	SOC_INFO     = 2, // Log a message to the SOC log, console, [stderr]
	SOC_REPORT   = 3, // Log a message to the SOC log, console, SOCcp, [stderr]
	SOC_RESPONSE = 4, // Log a message to the SOC log, console, SOCcp, [stderr],
d61 1
a61 1
	SOC_ERROR    = 5  // Log a message to the SOC log, console, SOCcp, [stderr],
d67 3
a69 3
#ifdef c_plusplus
}
#endif
d75 2
a76 2
	//* Constructor
	SOCMessageManager(void) ;
d78 1
a78 1
	//* Destructor
d81 1
a81 1
	//* Public member functions
d83 2
a84 2
	//% Allow user to set process name.
	//% Most likely done with SOCMessageManager::issuedBy(argv[0]) ;
d86 2
a87 2
	static const char* issuedBy(void) const       ; // Is it set?
	static       void  issuedBy(const char* prog) ; // set it
d91 2
a92 2
	static int  logToScreen(void)   ; // Is it on?
	static void logToScreen(int on) ; // Turn it on
d96 2
a97 2
	static int  exitOnError(void)   ; // Is it on?
	static void exitOnError(int on) ; // Turn it on
d101 1
a101 1
	int theOriginal ;
d103 4
a106 4
	static char* theProgram ;
	static int   toScreen ;
	static int   errorExit ;
	static int   isInitialized ;
d108 2
a109 2
	static int  syslogUp(void       ) ; // Is it up?
	static void syslogUp(const char* program) ; // set it up
d111 1
a111 1
	friend void SOCThrow(SOCMessageBehavior behavior, const char* format, ... ) ;
d125 1
a125 1
	//* Assignment operators
d132 1
a132 1
	SOCMessageObject(const char* msg) ;
d134 2
a135 2
	virtual void throwMessage(void) const = 0 ;
	virtual void throwMessage(const char* msg, ...) const = 0 ;
d137 1
a137 1
	const char* message(void) const ;
d139 1
a139 1
	friend void SOCThrow(const SOCMessageObject& obj, const char* format, ... );
d143 1
a143 1
	char* theMessage ;
d152 1
a152 1
	return theMessage ;
d165 1
a165 1
	SOCDebug(const char* msg) ;
d167 1
a167 1
	//% Throw the message with or without a additional message.
d169 2
a170 2
	virtual void throwMessage(void) const ;
	virtual void throwMessage(const char* format, ...) const ;
d180 1
a180 1
	SOCInfo(const char* msg) ;
d182 1
a182 1
	//% Throw the message with or without a additional message.
d184 2
a185 2
	virtual void throwMessage(void) const ;
	virtual void throwMessage(const char* format, ...) const ;
d195 1
a195 1
	SOCReport(const char* msg) ;
d197 1
a197 1
	//% Throw the message with or without a additional message.
d199 2
a200 2
	virtual void throwMessage(void) const ;
	virtual void throwMessage(const char* format, ...) const ;
d210 1
a210 1
	SOCResponse(const char* msg) ;
d212 1
a212 1
	//% Throw the message with or without a additional message.
d214 2
a215 2
	virtual void throwMessage(void) const ;
	virtual void throwMessage(const char* format, ...) const ;
d225 1
a225 1
	SOCError(const char* msg) ;
d227 1
a227 1
	//% Throw the message with or without a additional message.
d229 2
a230 2
	virtual void throwMessage(void) const ;
	virtual void throwMessage(const char* format, ...) const ;
@


3.4
log
@Fixed bug causing Multiply defined errors.
@
text
@d7 1
a7 1
// RCS: $Id: SOCMessage.h,v 3.3 1994/08/02 16:48:59 bfoutz Exp bfoutz $
d14 1
a14 1
// .VERSION XTE-SOC V3.1
d23 15
a37 1
// default behavior.
d40 1
a40 1
// Accessor(dm), PktTlm(pkts)
d49 4
d55 2
a56 2
                          // otherwise, don't do anything.
        SOC_DEBUG    = 1, // Log a message to the SOC log, console, [stderr]
d65 5
a69 2
extern const char * SOCMessageBText[];
extern const int    SOCMessageBNumber;
d79 1
a79 1
        ~SOCMessageManager(void) ;
d83 1
a83 1
	//% Allow user to set process name
d89 1
a89 1
        //% Setting logToScreen() on will echo all SOCThrow messages to STDERR.
d94 2
a98 2
        static int useSyslog(void)      ; // Output messages to syslog?
        static void useSyslog(int on)   ; // set/unset
a106 1
        static int   use_syslog ;
d148 1
a148 1
// Return the message string associated with this object
d166 3
d181 3
d196 3
d211 3
d226 3
a234 1
void SOCThrow(SOCMessageBehavior behavior, const char* format, ... ) ;
d236 1
a236 1
#endif /* __SOCERROR_H__ */
@


3.3
log
@Added SOCMessageBText and SOCMessageBNumber.
@
text
@d7 1
a7 1
// RCS: $Id: SOCMessage.h,v 3.2 1994/07/28 15:56:50 bfoutz Exp bfoutz $
d47 2
a48 15
//
// We also list the behavior names in text, so behaviors can be specified
// in places other than code. (For example, in an initialization or 
// configuration file.)
//
const char * SOCMessageBText[] = {
        "None",
        "Debug",
        "Info",
        "Report",
        "Response",
        "Error"
};

const int SOCMessageBNumber = sizeof(SOCMessageBText)/sizeof(char*);
@


3.2
log
@Added SOC_DEBUG as a new SOCMessageBehavior.
Added SOCDebug class, uses syslog DEBUG level.
Changes SOCLog to SOCInfo.
Added useSyslog() to SOCMessageManager.
@
text
@d7 1
a7 1
// RCS: $Id: SOCMessage.h,v 3.1 1994/07/21 20:53:43 rbarnett Exp bfoutz $
d22 1
a22 1
// There are five different Message classes that differ primarily in their
d47 16
@


3.1
log
@Initial version
@
text
@d7 1
a7 1
// RCS: $Id$
d22 1
a22 1
// There are four different Message classes that differ primarily in their
d36 6
a41 4
	SOC_NONE     = 0, // Don't do anything (presently). May be used for debugging.
	SOC_INFO     = 1, // Log a message to the SOC log, console, [stderr]
	SOC_REPORT   = 2, // Log a message to the SOC log, console, SOCcp, [stderr]
	SOC_RESPONSE = 3, // Log a message to the SOC log, console, SOCcp, [stderr],
d43 1
a43 1
	SOC_ERROR    = 4  // Log a message to the SOC log, console, SOCcp, [stderr],
d55 1
a55 1
 ~SOCMessageManager(void) ;
d65 2
d73 2
d83 1
d138 13
a150 1
class SOCLog : public SOCMessageObject {
d154 1
a154 1
	SOCLog(const char* msg) ;
@

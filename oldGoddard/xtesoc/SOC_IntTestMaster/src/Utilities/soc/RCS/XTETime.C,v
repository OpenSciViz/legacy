head	4.9;
access
	rbarnett
	arots
	soccm
	jonv;
symbols
	Build6_0:4.5
	Build4_5_3:4.1;
locks;
comment	@ * @;


4.9
date	2000.04.28.16.40.01;	author jonv;	state Exp;
branches;
next	4.8;

4.8
date	99.11.10.17.15.30;	author jonv;	state Exp;
branches;
next	4.7;

4.7
date	98.11.02.15.14.03;	author jonv;	state Exp;
branches;
next	4.6;

4.6
date	98.07.27.14.39.52;	author soccm;	state Exp;
branches;
next	4.5;

4.5
date	97.10.28.15.43.04;	author arots;	state Exp;
branches;
next	4.4;

4.4
date	97.03.03.20.35.02;	author arots;	state Exp;
branches;
next	4.3;

4.3
date	96.10.08.13.51.32;	author arots;	state Exp;
branches;
next	4.2;

4.2
date	96.09.27.18.55.05;	author rbarnett;	state Exp;
branches;
next	4.1;

4.1
date	95.11.02.21.14.07;	author rbarnett;	state Exp;
branches;
next	;


desc
@Basic class to handle time conversions ala GOF
@


4.9
log
@if $SOCHOME was not defined, the setleaps routine was core dumping;
this is now fixed - if no $SOCHOME, it looks for tai-utc in current dir
@
text
@//----------------------------------------------------------------------
//
//  File:        XTETime.C
//  Programmer:  Arnold Rots  -  USRA
//  Date:        31 October 1995
//  Subsystem:   XFF
//  Library:     ObsCat
//  Description: Code for XTETime, XTETimeRange, XTRList classes
//
//----------------------------------------------------------------------
//
static const char * const rcsID = "$Id: XTETime.C,v 4.8 1999/11/10 17:15:30 jonv Exp $" ;

#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <iostream.h>
#include <iomanip.h> 
#include <stdlib.h>
#include "XTETime.h"
#define TAIUTC "tai-utc.dat"

const double XTETime::MJD0        = 2400000.5 ;  // JD - MJD
const double XTETime::MJDREF      = 49353.0 ;    // MJD at 1994.0
const double XTETime::TT2UT       = 60.184 ;     // UTC - TT at 1994.0
int    XTETime::NUMLEAPSECS = 0 ;      // Leap seconds: 1994.5, 1996.0, 1997.5
double XTETime::LEAPSECS[]  = {15638401.0, 63072002.0, 110332803.0} ;

static int daymonth[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31} ;
static const char * const month[12] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun",
         "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"} ;

//
//   ---------------------------------------------------------
// -- XTETime::XTETime (double tt, TimeSys ts, TimeFormat tf) --
//   ---------------------------------------------------------
//

// Description:
// Constructor: most general constructor
// Default for tf is SECS
XTETime::XTETime (double tt, TimeSys ts, TimeFormat tf)
  : t (tt), timeZero (0.0)
{
  setleaps() ;
  set (tt, ts, tf) ;
  return ;
}

//
//   ----------------------------------------------------------
// -- XTETime::XTETime (char *date, TimeSys ts, TimeFormat tf) --
//   ----------------------------------------------------------
//

// Description:
// Constructor: create from a date string
// Default for ts is UTC, for tf is DATE
XTETime::XTETime (const char *date, TimeSys ts, TimeFormat tf)
  : t (0.0), timeZero (0.0)
{
  setleaps() ;
  set (date, ts, tf) ;
  return ;
}
//
//   --------------------------
// -- XTETime::setleaps (void) --
//   --------------------------
//

// Description:
// Function to set leap second table
void XTETime::setleaps (void)
{
  if ( !NUMLEAPSECS ) {
    FILE *FF = NULL;
    char lsfile[256] ;
    char *socops = getenv("SOCHOME");
    if( socops ) {
      sprintf (lsfile, "%s/etc/%s", socops, TAIUTC);
      FF = fopen (lsfile, "r");
    }

    // If the file in /sochome/etc/ can't be opened, then try the 
    // current directory.
    if ( ! FF ) {
      sprintf(lsfile, "./%s", TAIUTC);
      FF = fopen (lsfile, "r");
    }

    if ( FF ) {

      long leapsMD ;
      while ( fscanf (FF, "%*d %*s  1 =JD 24%d.5 %*s %*lg S + (MJD - %*lg) X %*lg %*s",
		      &leapsMD) == 1 ) {
	leapsMD -= (long) (MJDREF + 0.1) ;
	if ( leapsMD > 0 ) {
	  LEAPSECS[NUMLEAPSECS] = leapsMD * 86400 + 1 + NUMLEAPSECS ;
	  NUMLEAPSECS++ ;
	}
      }
      fclose (FF) ;
    }
    else
      NUMLEAPSECS = 3 ;          // Leap seconds: 1994.5, 1996.0, 1997.5

  }
  return ;
}

//
//   -----------------------------------------------------
// -- XTETime::set (double tt, TimeSys ts, TimeFormat tf) --
//   -----------------------------------------------------
//

// Description:
// General set function
void XTETime::set (double tt, TimeSys ts, TimeFormat tf)
{
  switch (tf) {
  case JD:
    tt -= MJD0 ;
  case MJD:
    tt -= MJDREF ;
    tt *= 86400.0 ;
  case SECS:
    break ;
  default:                //  Error; do nothing
    return ;
  }

  switch (ts) {
  case MET:
    break ;
  case TT:
    tt -= TT2UT ;
    break ;
  case UTC:
    {
      for (int i=0; i<NUMLEAPSECS; i++)
        if ( tt >= LEAPSECS[i] )
          tt++ ;
    }
    break ;
  default:
    return ;
  }
  t = tt ;
  return ;
}

//
//   ------------------------------------------------------
// -- XTETime::set (char *date, TimeSys ts, TimeFormat tf) --
//   ------------------------------------------------------
//

// Description:
// General set function from a date string
void XTETime::set (const char *date, TimeSys ts, TimeFormat tf)
{
  int year, day, hour, minute;
  double second ;
  int n ;
  int m = 0 ;
  char mn[4] ;

  switch (tf) {
  case DATE:
    n = sscanf (date, "%d:%d:%d:%d:%lg", &year, &day, &hour, &minute, &second) ;
    if ( n != 5 )
      return ;
    break ;
  case CALDATE:
    n = sscanf (date, "%d%c%c%c%d at %d:%d:%lg",
    &year, mn, mn+1, mn+2, &day, &hour, &minute, &second) ;
    if ( n != 8 )
      return ;
    if ( year%4 )
      daymonth[1] = 28 ;
    else
      daymonth[1] = 29 ;
    mn[0] = toupper(mn[0]) ;
    mn[1] = tolower(mn[1]) ;
    mn[2] = tolower(mn[2]) ;
    mn[3] = 0 ;
    while ( strcmp(mn, month[m]) ) {
      if ( m > 11 )
  return ;
      day += daymonth[m++] ;
    }
    break ;
  default:
    return ;
  }

  day += (year - 1994) * 365 - 1 ;
  day += (year - 1993) / 4 ;
  second = (double) day * 86400 + hour * 3600 + minute * 60 + second ;
  set (second, ts, SECS) ;

  return ;
}

//
//   -----------------
// -- XTETime::monDay --
//   -----------------
//

// Description:
// Convert UTC or TT date string to calendar date string
const char *XTETime::monDay (const char *date) {
  char d[32] ;
  int year, day ;
  int m = 0 ;

  strcpy (d, date) ;
  sscanf (d, "%d:%d", &year, &day) ;
  if ( year%4 )
    daymonth[1] = 28 ;
  else
    daymonth[1] = 29 ;

  while ( day > daymonth[m] ) {
    day -= daymonth[m] ;
    m++ ;
  }
  sprintf (tdate, "%04d%s%02d at ", year, month[m], day) ;
  strcpy (tdate+13, d+9) ;
  return ( tdate ) ;
}

//
//   ------------------------------------------
// -- XTETime::get (TimeSys ts, TimeFormat tf) --
//   ------------------------------------------
//

// Description:
// Generalized time return function
double XTETime::get (TimeSys ts, TimeFormat tf) const {
  double tt ;
  switch (ts) {
  case UTC:
    tt = getUTC () ;
    break ;
  case TT:
    tt = getTT () ;
    break ;
  case MET:
    tt = getMET () ;
    break ;
  }
  switch (tf) {
  case MJD:
    tt = tt / 86400.0 + MJDREF ;
    break ;
  case JD:
    tt = tt / 86400.0 + MJDREF + MJD0 ;
    break ;
  default:
    break ;
  }
  return tt ;
}


//
//   ------------------------
// -- XTETime::getUTC (void) --
//   ------------------------
//

// Description:
// Return UTC seconds
double XTETime::getUTC (void) const {
  double tt = t + timeZero ;
  for (int i=NUMLEAPSECS-1; i>=0; i--)
    if ( tt >= LEAPSECS[i] )
      tt-- ;
  return tt ;
}

//
//   ----------------------------------------------
// -- XTETime::getDate (TimeSys ts, TimeFormat tf) --
//   ----------------------------------------------
//

// Description:
// Generalized date string return function
const char *XTETime::getDate (TimeSys ts, TimeFormat tf) {
  double tt = get (ts, SECS) ;
  int year, day, hour, minute, second ;
  int i = 2 ;

  second = (int) (tt + 0.5) ;
  day = second / 86400 ;
  second %= 86400 ;
  hour = second / 3600 ;
  second %= 3600 ;
  minute = second / 60 ;
  second %= 60 ;
  year = 1994 ;
  day++ ;
  while ( day > 365 ) {
    if ( !i ) {
      if ( day == 366 )
  break ;
      else
  day-- ;
    }
    day -= 365 ;
    year++ ;
    i = (i+1)%4 ;
  }

  sprintf (tdate, "%4d:%03d:%02d:%02d:%02d",
     year, day, hour, minute, second) ;
  switch (ts) {
  case TT:
    strcat (tdate, "TT") ;
    break ;
  case UTC:
    strcat (tdate, "UTC") ;
    break ;
  case MET:
    strcat (tdate, "MET") ;
    break ;
  default:
    break ;
  }

  if ( tf == CALDATE )
    return ( monDay (tdate) ) ;
  else
    return tdate ;
}

//
//   ---------------------------
// -- XTETimeRange::setEmpty () --
//   ---------------------------
//

// Description:
// Determine whether range is empty
void XTETimeRange::setEmpty (void) {
  double t1=start.getMET() ;
  double t2=stop.getMET() ;
  if ( ( t1 >= t2 ) || ( t1 <= 0.0 ) || ( t2 <= 0.0 ) )
    empty = 1 ;
  else
    empty = 0 ;
  return ;
}

//
//   -----------------------------
// -- XTETimeRange::printRange () --
//   -----------------------------
//
// Description:
// A two-liner in UTC date format
void XTETimeRange::printRange (void) {
  int saved_precision = cout.precision();
  cout.precision( 10 );
  cout << "---XTETimeRange - Empty: " << empty
       << ", Start: " << start.getMET() << " (" << UTStartDate () << ")\n"
       << "                       "
       << "  Stop:  " << stop.getMET() << " (" << UTStopDate () << ")\n" ;

  cout.precision( saved_precision );
  return ;
}
//
//   --------------------------------
// -- XTETimeRange::printRangeCal () --
//   --------------------------------
//

// Description:
// A two-liner in UTC calendar date format
void XTETimeRange::printRangeCal (void) {
  cout << "---XTETimeRange - Empty: " << empty
       << ", Start: " << start.getMET() << " (" << start.UTCalDate () << ")\n"
       << "                       "
       << "  Stop:  " << stop.getMET() << " (" << stop.UTCalDate () << ")\n" ;
  return ;
}

//
//   -----------------------
// -- XTRList::printList () --
//   -----------------------
//

// Description:
// Print list contents in UTC calendar date format
void XTRList::printList (void) {
  cout << "\nXTRList - Empty: " << empty << ", Number of ranges:: " << numXTRs
       << ", List range:\n" ;
  listRange.printRange () ;
  if ( numXTRs ) {
    cout << "Member ranges:\n" ;
    for (int i=0;i<numXTRs;i++)
      tr[i].printRange () ;
  }
  return ;
}

//
//   --------------------------
// -- XTRList::printListCal () --
//   --------------------------
//

// Description:
// Print list contents in UTC calendar date format
void XTRList::printListCal (void) {
  cout << "\nXTRList - Empty: " << empty << ", Number of ranges:: " << numXTRs
       << ", List range:\n" ;
  listRange.printRangeCal () ;
  if ( numXTRs ) {
    cout << "Member ranges:\n" ;
    for (int i=0;i<numXTRs;i++)
      tr[i].printRangeCal () ;
  }
  return ;
}

//
//   ----------------------------
// -- XTRList::XTRList (XTRList) --
//   ----------------------------
//

// Description:
// Copy constructor for a new TR list
XTRList::XTRList (const XTRList &trl)
{
  numXTRs = trl.numXTRs ;
  listRange = trl.listRange ;
  empty = trl.empty ;
  tr = new XTETimeRange[numXTRs] ;
  for (int i=0; i<numXTRs; i++)
    tr[i] = trl.tr[i] ;
  return ;
}

//
//   ------------------------------
// -- XTRList::operator= (XTRList) --
//   ------------------------------
//

// Description:
// Copy operator for a TR list
XTRList& XTRList::operator= (const XTRList &trl)
{
  delete [] tr ;
  numXTRs = trl.numXTRs ;
  listRange = trl.listRange ;
  empty = trl.empty ;
  tr = new XTETimeRange[numXTRs] ;
  for (int i=0; i<numXTRs; i++)
    tr[i] = trl.tr[i] ;
  return *this ;
}


//
//   -------------------------------------
// -- XTRList::XTRList (XTRList, XTRList) --
//   -------------------------------------
//

// Description:
// Construct a new TR list by "AND"ing two existing lists
XTRList::XTRList (const XTRList &trl1, const XTRList &trl2)
  : numXTRs (1), empty (1), tr (0) {

//  Trivial cases: if one of them is empty, the result is empty

  if ( trl1.isEmpty() || trl2.isEmpty() ) {
    numXTRs = 1 ;
    empty = 1 ;
    tr = new XTETimeRange () ;
    listRange = *tr ;
    return ;
  }

//  To minimize work, make sure the second one is the shortest

  const XTRList *list1 = &trl1 ;
  const XTRList *list2 = &trl2 ;
  int nlist1 = list1->numXTRs ;
  int nlist2 = list2->numXTRs ;
  if ( nlist1 < nlist2 ) {
    int i = nlist2 ;
    nlist2 = nlist1 ;
    nlist1 = i ;
    list1 = list2 ;
    list2 = &trl1 ;
  }

//  Simple case: second list has only one member

  if ( nlist2 == 1 ) {
    XTRList scratchlist (*list1) ;
    scratchlist.andRange ( list2->tr[0] ) ;
    numXTRs = scratchlist.numXTRs ;
    listRange = scratchlist.listRange ;
    empty = scratchlist.empty ;
    tr = new XTETimeRange[numXTRs] ;
    for (int i=0; i<numXTRs; i++)
      tr[i] = scratchlist.tr[i] ;
    return ;
  }

//  The full works: AND each range in list2 with all of list1
//                  OR the resulting lists

  XTRList buildlist ;
  int i ;
  for (i=0;i<nlist2;i++) {
    XTRList scratchlist (*list1) ;
    scratchlist.andRange ( list2->tr[i] ) ;
    buildlist.orList (scratchlist) ;
  }
  numXTRs = buildlist.numXTRs ;
  listRange = buildlist.listRange ;
  empty = buildlist.empty ;
  tr = new XTETimeRange[numXTRs] ;
  for (i=0; i<numXTRs; i++)
    tr[i] = buildlist.tr[i] ;
  return ;
}

//
//   -------------------------------
// -- XTRList::isInRange (XTETime&) --
//   -------------------------------
//

// Description:
// Return 0 if in range
int XTRList::isInRange (const XTETime &T) const {
  for (int i=0;i<numXTRs;i++)
    if ( !tr[i].isInRange (T) )
      return 0 ;
  return 1 ;
}

//
//   -----------------------------
// -- XTRList::isInRange (double) --
//   -----------------------------
//

// Description:
// Return 0 if in range
int XTRList::isInRange (double t) const {
  for (int i=0;i<numXTRs;i++)
    if ( !tr[i].isInRange (t) )
      return 0 ;
  return 1 ;
}

//
//   ------------------------------
// -- XTRList::getRange (XTETime&) --
//   ------------------------------
//

// Description:
// Return range in which XTETime object <T> falls
const XTETimeRange *XTRList::getRange (const XTETime &T) const {
  for (int i=0;i<numXTRs;i++)
    if ( !tr[i].isInRange (T) )
      return tr+i ;
  return NULL ;
}

//
//   ----------------------------
// -- XTRList::getRange (double) --
//   ----------------------------
//

// Description:
// Return range in which MET time <t> falls
const XTETimeRange *XTRList::getRange (double t) const {
  for (int i=0;i<numXTRs;i++)
    if ( !tr[i].isInRange (t) )
      return tr+i ;
  return NULL ;
}

//
//   -----------------------
// -- XTRList::totalTime () --
//   -----------------------
//

// Description:
// Return total time (in seconds), covered by the list
double XTRList::totalTime (void) const {
  double tt = 0.0 ;
  if ( !empty )
    for (int i=0;i<numXTRs;i++)
      tt += tr[i].totalTime () ;
  return tt ;
}

//
//   -----------------
// -- XTRList::orList --
//   -----------------
//

// Description:
// "OR" in another XTETime Range List
void XTRList::orList (const XTRList &trl) {

//  Do nothing if trl is empty

  if ( trl.empty )
    return ;

//  If *this is empty, replace it by trl

  if ( empty ) {
    delete [] tr ;
    numXTRs = trl.numXTRs ;
    listRange = trl.listRange ;
    empty = trl.empty ;
    tr = new XTETimeRange[numXTRs] ;
    for (int i=0; i<numXTRs; i++)
      tr[i] = trl.tr[i] ;
  }

//  Do the full thing

  else {
    int n = trl.numXTRs ;
    for (int i=0;i<n;i++)
      orRange ( trl.tr[i] ) ;
  }
  return ;
}

//
//   -----------------
// -- XTRList::notList --
//   -----------------
//

// Description:
// Negate a XTETime Range List over a specified time range
void XTRList::notList (const XTETimeRange &T) {

//  If the list was empty, the answer is just T ...

  if ( empty ) {

//  ... unless, of course, T was empty, too, in which case nothing changes

    if ( !T.isEmpty() ) {
      *tr = T ;
      listRange = T ;
      numXTRs = 1 ;
      empty = 0 ;
    }
  }

//  "Regular" case

  else {
    XTETimeRange *ntr = new XTETimeRange[numXTRs+1] ;
    ntr[0].setStart(1000.0) ;
    for (int i=0; i<numXTRs; i++) {
      ntr[i].setStop(tr[i].TStart()) ;
      ntr[i+1].setStart(tr[i].TStop()) ;
    }
    ntr[numXTRs].setStop(1.0e20) ;
    numXTRs++ ;
    delete [] tr ;
    tr = ntr ;
    setListRange () ;
    andRange (T) ;
  }
  return ;
}


//
//   -------------------
// -- XTRList::andRange --
//   -------------------
//

// Description:
// "AND" in an extra XTETime Range
void XTRList::andRange (const XTETimeRange &T) {
  int startin=0, stopin=0 ;
  int startafter=0, stopafter=0 ;
  int zap=0 ;
  int istart, istop ;
  int i ;
  double tstart = T.METStart () ;
  double tstop = T.METStop () ;
//
//  First the trivial cases
//
  if ( empty )
    return ;
  else if ( T.isEmpty () )
    zap = 1 ;
  else if ( ( tstart <= listRange.METStart () ) &&
      ( tstop >= listRange.METStop () ) )
    return ;
  else if ( tstop < listRange.METStart () )
    zap = 1 ;
  else if ( tstart > listRange.METStop () )
    zap = 1 ;
//
//  See where the start and stop times fall in the existing list
//  (add 1 to the indices)
  else {
    for (i=0;i<numXTRs;i++) {
      if ( !startin ) {
	istart = tr[i].isInRange (tstart) ;
	if ( !istart )
	  startin = i + 1 ;
	else if ( istart > 0 )
	  startafter = i + 1 ;
      }
      if ( !stopin ) {
	istop = tr[i].isInRange (tstop) ;
	if ( !istop )
	  stopin = i + 1 ;
	else if ( istop > 0 )
	  stopafter = i + 1 ;
      }
    }
//
//  Now figure out what to do
//    Which range do we start in?
//
    if ( startin ) {
      startin -- ;                     // Correct the index
      tr[startin].setStart (tstart) ;  // Adjust the time
    }

//      In between
    else if ( !stopin && ( startafter == stopafter ) )
      zap = 1 ;

//      Start after
    else
      startin = startafter ;

//    Which range do we stop in?
    if ( !zap ) {
      if ( stopin ) {
	stopin-- ;
	tr[stopin].setStop (tstop) ;
      }

//      Stop after
      else
	stopin = stopafter - 1 ;
    }
  }

//
//  Calculate the new length
  int newNumXTRs ;
  if ( zap ) {
    newNumXTRs = 1 ;
    empty = 1 ;
  }
  else
    newNumXTRs = stopin - startin + 1 ;

//  No change in number of ranges: done
  if ( numXTRs == newNumXTRs ) {
    if ( zap )
      tr->resetRange (0.0, 0.0) ;
    setListRange () ;
    return ;
  }

//
//  Make a new set of ranges
  XTETimeRange *newXTR = new XTETimeRange[newNumXTRs] ;

//
//    Rearrange the ranges
  if ( !zap ) {
//      Now copy the remaining ones
    int j=0 ;
    for (i=startin;i<=stopin;i++,j++)
      newXTR[j] = tr[i] ;
  }
  else
    newXTR->resetRange (0.0, 0.0) ;

//
//  Exchange the two lists
  delete [] tr ;
  tr = newXTR ;
  numXTRs = newNumXTRs ;
  setListRange () ;

  return ;
}

//
//   ------------------
// -- XTRList::orRange --
//   ------------------
//

// Description:
// "OR" in an extra XTETime Range
void XTRList::orRange (const XTETimeRange &T) {
  int startin=0, stopin=0 ;
  int startafter=0, stopafter=0 ;
  int before=0, after=0, between=0, straddle=0 ;
  int istart, istop ;
  int i ;
  double tstart = T.METStart () ;
  double tstop = T.METStop () ;

//
//  Handle the empties first
//
  if ( T.isEmpty () )
    return ;
  if ( empty ) {
    numXTRs = 1 ;
    empty = 0 ;
    tr[0] = T ;
    listRange = T ;
    return ;
  }

//
//  First the trivial cases
//
  if ( ( tstart <= listRange.METStart () ) &&
      ( tstop >= listRange.METStop () ) )
    straddle = 1 ;
  else if ( tstop < listRange.METStart () )
    before = 1 ;
  else if ( tstart > listRange.METStop () )
    after = 1 ;

//
//  See where the start and stop times fall in the existing list
//  (add 1 to the indices)
  else {
    for (i=0;i<numXTRs;i++) {
      if ( !startin ) {
	istart = tr[i].isInRange (tstart) ;
	if ( !istart )
	  startin = i + 1 ;
	else if ( istart > 0 )
	  startafter = i + 1 ;
      }
      if ( !stopin ) {
	istop = tr[i].isInRange (tstop) ;
	if ( !istop )
	  stopin = i + 1 ;
	else if ( istop > 0 )
	  stopafter = i + 1 ;
      }
    }
//
//  Now figure out what to do
//    Which range do we start in?
//
    if ( startin ) {
      if ( startin == stopin )  // If we're stopping in the same one, return
	return ;
      startin -- ;              // Correct the index
    }

//      In between
    else if ( !stopin && ( startafter == stopafter ) )
      between = stopafter ;

//      Somebody's start time needs to be adjusted
    else {
      startin = startafter ;
      tr[startin].setStart (tstart) ;
    }

//    Which range do we stop in?
    if ( stopin )
      stopin-- ;
//      Somebody's stop time needs to be adjusted
    else
      if ( stopafter && (!between) ) {
	stopin = stopafter - 1 ;
	tr[stopin].setStop (tstop) ;
      }
  }

//
//  The range list must now be non-empty
  empty = 0 ;

//
//  Calculate the new length
  int newNumXTRs ;
  if ( before + after + between )
    newNumXTRs = numXTRs + 1 ;
  else if ( straddle )
    newNumXTRs =  1;
  else
    newNumXTRs = numXTRs - stopin + startin ;

//  No change in number of ranges: done
  if ( numXTRs == newNumXTRs ) {
    if ( straddle )
      tr[0] = T ;
    setListRange () ;
    return ;
  }

//
//  Make a new set of ranges
  XTETimeRange *newXTR = new XTETimeRange[newNumXTRs] ;

//
//    Extra range before
  if ( before ) {
    newXTR[0] = T ;
    for (i=0;i<numXTRs;i++)
      newXTR[i+1] = tr[i] ;
  }

//
//    Extra range after
  else if ( after ) {
    for (i=0;i<numXTRs;i++)
      newXTR[i] = tr[i] ;
    newXTR[numXTRs] = T ;
  }

//
//    Straddling range
  else if ( straddle )
    newXTR[0] = T ;

//
//    Extra range in between
  else if ( between ) {
    for (i=0;i<between;i++)
      newXTR[i] = tr[i] ;
    newXTR[between] = T ;
    for (i=between;i<numXTRs;i++)
      newXTR[i+1] = tr[i] ;
  }

//
//    Rearrange the ranges
  else {
//      Cover the new part in a single range
    tr[stopin].setStart (tr[startin].METStart()) ;
//      Now copy the remaining ones
    int j=0 ;
    for (i=0;i<startin;i++,j++)
      newXTR[j] = tr[i] ;
    for (i=stopin;i<numXTRs;i++,j++)
      newXTR[j] = tr[i] ;
  }

//
//  Exchange the two lists
  delete [] tr ;
  tr = newXTR ;
  numXTRs = newNumXTRs ;
  setListRange () ;

  return ;
}

//
//   -----------------------
// -- XTRList::setListRange --
//   -----------------------
//

// Description:
// Update the list range
void XTRList::setListRange (void) {
  int i, j, remove=0 ;

  if ( numXTRs )
    empty = 0 ;
  for (i=0; i<numXTRs; i++)
    if ( tr[i].isEmpty() )
      remove++ ;
  if ( remove ) {
    if ( remove >= numXTRs ) {
      numXTRs = 0 ;
      empty = 1 ;
    }
    else {
      XTETimeRange *newXTR = new XTETimeRange[numXTRs - remove] ;
      for (i=0, j=0; i<numXTRs; i++)
	if ( !tr[i].isEmpty() )
	  newXTR[j++] = tr[i] ;
      delete [] tr ;
      tr = newXTR ;
      numXTRs -= remove ;
      empty = 0 ;
    }
  }

  if ( !empty )
    listRange.resetRange (tr[0].METStart(), tr[numXTRs-1].METStop()) ;
  else
    listRange.resetRange (0.0, -1.0) ;
}
@


4.8
log
@fixed bug in orRange for XTRList class
@
text
@d12 1
a12 1
static const char * const rcsID = "$Id: XTETime.C,v 4.7 1998/11/02 15:14:03 jonv Exp $" ;
d77 1
a77 1
    FILE *FF ;
d79 5
a83 2
    sprintf (lsfile, "%s/etc/%s", getenv ("SOCHOME"), TAIUTC);
    FF = fopen (lsfile, "r");
@


4.7
log
@  # A slight modification regarding where this program looks to find its
  # leap second file, tai-utc.dat.  If the leap seconds file is not found
  # in $SOCHOME/etc then the program also checks the current directory.
@
text
@d12 1
a12 1
static const char * const rcsID = "$Id: XTETime.C,v 4.6 1998/07/27 14:39:52 soccm Exp $" ;
d18 1
a362 1

d366 2
d372 2
d906 1
a906 1
      if ( stopafter ) {
@


4.6
log
@added #include<stdlib.h>
removed extra RCS Id
@
text
@d12 1
a12 1
static const char * const rcsID = "$Id: XTETime.C,v 4.5 1997/10/28 15:43:04 arots Exp soccm $" ;
d79 11
a89 1
    if ( FF = fopen (lsfile, "r") ) {
@


4.5
log
@Modified to get leap seconds from file in $SOCHOME/etc.
@
text
@a2 2
//  RCS: $Id: XTETime.C,v 4.4 1997/03/03 20:35:02 arots Exp arots $
//
d12 1
a12 1
static const char * const rcsID = "$Id: XTETime.C,v 4.4 1997/03/03 20:35:02 arots Exp arots $" ;
d18 1
@


4.4
log
@Added leap second 1997.5.
@
text
@d3 1
a3 1
//  RCS: $Id: XTETime.C,v 5.3 1996/10/08 13:43:13 soccm Exp $
d14 1
a14 1
static const char * const rcsID = "$Id: XTETime.C,v 5.3 1996/10/08 13:43:13 soccm Exp $" ;
d21 1
d26 2
a27 2
const int    XTETime::NUMLEAPSECS = 3 ;          // Leap seconds: 1994.5, 1996.0, 1997.5
const double XTETime::LEAPSECS[]  = {15638401.0, 63072002.0, 110332803.0} ;
d45 1
d62 1
d64 32
@


4.3
log
@Fixed single range straddling in XTRList.orRange.
@
text
@d25 2
a26 2
const int    XTETime::NUMLEAPSECS = 2 ;          // Leap seconds: 1994.5, 1996.0
const double XTETime::LEAPSECS[]  = {15638401.0, 63072002.0} ;
@


4.2
log
@*** empty log message ***
@
text
@d3 1
a3 1
//  RCS: $Id: XTETime.C,v 1.5 1996/03/19 20:56:14 soccm Exp $
d14 1
a14 1
static const char * const rcsID = "$Id: XTETime.C,v 1.5 1996/03/19 20:56:14 soccm Exp $" ;
d880 2
@


4.1
log
@initial revision
@
text
@d3 1
a3 1
//  RCS: $Id: XTETime.C,v 1.4 1995/11/02 18:35:31 arots Exp arots $
d14 1
a14 1
static const char * const rcsID = "$Id: XTETime.C,v 1.4 1995/11/02 18:35:31 arots Exp arots $" ;
d23 1
a23 1
const double XTETime::MJDREF      = 49352.0 ;    // MJD at 1994.0
d306 1
a306 1
  if ( ( t1 > t2 ) || ( t1 <= 0.0 ) || ( t2 <= 0.0 ) )
d404 1
a404 1
//   ----------------------------
d406 1
a406 1
//   ----------------------------
d413 1
d433 1
a433 2
: tr (0), numXTRs (0), empty (1)
{
d437 5
a441 6
  if ( trl1.isEmpty () ) {
    *this = trl1 ;
    return ;
  }
  if ( trl2.isEmpty () ) {
    *this = trl2 ;
d449 2
a450 2
  int nlist1 = list1->getNumXTRs () ;
  int nlist2 = list2->getNumXTRs () ;
d463 7
a469 2
    scratchlist.andRange ( *(list2->getRange (0)) ) ;
    *this = scratchlist ;
d477 2
a478 1
  for (int i=0;i<nlist2;i++) {
d480 1
a480 1
    scratchlist.andRange ( *(list2->getRange (0)) ) ;
d483 6
a488 1
  *this = buildlist ;
d577 4
a580 1
  if ( trl.isEmpty () )
d582 20
a601 3
  int n = trl.getNumXTRs() ;
  for (int i=0;i<n;i++)
    orRange ( *(trl.getRange(i)) ) ;
d614 30
a643 5
  XTETimeRange *ntr = new XTETimeRange[numXTRs+1] ;
  ntr[0].setStart(-1000.0) ;
  for (int i=0; i<numXTRs; i++) {
    ntr[i].setStop(tr[i].TStart()) ;
    ntr[i+1].setStart(tr[i].TStop()) ;
a644 5
  ntr[numXTRs].setStop(1.0e20) ;
  numXTRs++ ;
  delete [] tr ;
  tr = ntr ;
  andRange (T) ;
d659 1
a659 1
  int startbefore=0, stopafter=0 ;
d685 5
a689 5
  istart = tr[i].isInRange (tstart) ;
  if ( !istart )
    startin = i + 1 ;
  else if ( istart < 0 )
    startbefore = i + 1 ;
d692 5
a696 5
  istop = tr[i].isInRange (tstop) ;
  if ( !istop )
    stopin = i + 1 ;
  else if ( ( istop > 0 ) && ( !stopafter ) )
    stopafter = i + 1 ;
d707 1
d709 1
a709 1
    else if ( !stopin && ( (startbefore-stopafter) > 0 ) )
d711 10
a720 7
//      Start before
    else {
      if ( startbefore ) {
  if ( stopafter || stopin )
    startin = startbefore - 1 ;
  else
    cout << "Should not have happened: before = 1\n" ;
d722 2
d725 1
a725 6
  cout << "Should not have happened: after = 1\n" ;
    }
//    Which range do we stop in?
    if ( stopin ) {
      stopin-- ;
      tr[stopin].setStop (tstop) ;
a726 4
//      Somebody's stop time needs to be adjusted
    else
      if ( stopafter ) 
  stopin = stopafter - 1 ;
d728 1
d742 1
a742 1
      tr->resetRange (0.0, -1.0) ;
d746 1
d750 1
d760 2
a761 1
    newXTR->resetRange (0.0, -1.0) ;
d782 1
a782 1
  int startbefore=0, stopafter=0 ;
d788 1
d801 1
d812 1
d819 5
a823 5
  istart = tr[i].isInRange (tstart) ;
  if ( !istart )
    startin = i + 1 ;
  else if ( istart < 0 )
    startbefore = i + 1 ;
d826 5
a830 5
  istop = tr[i].isInRange (tstop) ;
  if ( !istop )
    stopin = i + 1 ;
  else if ( ( istop > 0 ) && ( !stopafter ) )
    stopafter = i + 1 ;
d839 1
a839 1
  return ;
d842 1
d844 1
a844 1
    else if ( !stopin && ( (startbefore-stopafter) > 0 ) )
d846 1
d849 2
a850 10
      if ( startbefore ) {
  if ( stopafter || stopin ) {
    startin = startbefore - 1 ;
    tr[startin].setStart (tstart) ;
  }
  else
    cout << "Should not have happened: before = 1\n" ;
      }
      else
  cout << "Should not have happened: after = 1\n" ;
d852 1
d859 2
a860 2
  stopin = stopafter - 1 ;
  tr[stopin].setStop (tstop) ;
d863 1
d867 1
d877 1
d883 1
d887 1
d895 1
d903 1
d908 1
d918 1
d931 1
d940 39
@

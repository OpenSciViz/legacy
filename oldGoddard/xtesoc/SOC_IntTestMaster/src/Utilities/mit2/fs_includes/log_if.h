/*-----------------------------------------------------------------------------

TITLE: Software Housekeeping Log Definition (LOG_IF) Include File

DOCUMENT NUMBER: 64-30110.67        REVISION: A

ORIGINATOR: James Francis

LETTER	ECO NO.		DESCRIPTION		CHECKED	APPROVED	DATE
-------------------------------------------------------------------------------
01                      Initial Formal
A                       Initial Letter Release
















































-----------------------------------------------------------------------------*/

/*===========================================================================
* 
*  $Source: /home/xpert/Build/src/Utilities/fs_includes/RCS/log_if.h,v $
*
*       MODULE NAME: SW Housekeeping Log Definition Include File (LOG_IF)
* 
*       PURPOSE: Provides Software Housekeeping Statistic Code Definitions.
* 
*       ASSUMPTIONS: NONE
*       
*       REFERENCES:
*       1. "XTE EDS Hardware Functional Descriptions and Requrements"
*           MIT 64-02006.01 Rev. E
* 
*       SOURCE IDENTIFICATION:          MIT 64-30110.67 Rev. A
* 
*  $Log: log_if.h,v $
*  Revision 5.1  1996/04/16 18:07:13  nessus
*  Bump version number
*
 * Revision 1.2  1994/06/17  20:01:47  berczuk
 * FS Build 2.6
 *
*   Revision 2.2  1994/04/28  19:55:47  jimf
*   Added counters for Interval, Uplink, Downlink, Minortick and FirstFlag
*   interrupts.
*
*   Revision 2.1  1994/01/12  03:24:28  jimf
*   Rev. A. Initial Letter Release
*
*   Revision 1.11  1993/10/07  17:34:54  jimf
*   Added title page, updated references and assigned part #.
*
*   Revision 1.10  1993/10/06  20:14:23  jimf
*   Set LOG_LAST_LOG to largest available slot.
*
*   Revision 1.9  1993/06/02  13:18:20  jimf
*   Added ASM Housekeeper Thread Log Identifiers
*
*   Revision 1.8  1993/04/05  21:33:12  danh
*   add ,
*
*   Revision 1.7  1993/04/05  21:24:49  danh
*   New item LOG_EVENT_DATA_LEN_PART_NO_END for binary search failed.
*
*   Revision 1.6  1993/02/01  18:46:27  jimf
*   Added LOG_SCIMODE_FF_DROP_PART generic housekeeping log which indicates
*   that the running mode's first flag handler dropped a partition.
*
*   Revision 1.5  1992/12/15  18:49:15  danh
*   Add  LOG_SUPER_SEND_PACKET_DROPPED for no buf available for SW_housekeeping
*
*   Revision 1.4  1992/12/11  18:10:02  jimf
*   Typo. Was missing "typedef".
*
*   Revision 1.3  1992/12/04  18:23:33  jimf
*   Added LOG_DMA_UPLINK_NO_BUF to indicate no buffer was available when
*   packet arrived.
*   Added LOG_DMA_UPLINK_NO_XFR to indicate that the DMA was setup after
*   the packet arrived.
*
*   Revision 1.2  1992/12/02  18:12:44  jimf
*   Deleted LOG_UPLINK_COMMAND_DROPPED.
*   Added LOG_UPLINK_GET_NO_THREAD and LOG_UPLINK_CALLBACK_NO_THREAD.
*
*   Revision 1.1  1992/11/24  17:55:58  jimf
*   Initial revision
*
* 
===========================================================================*/

#ifndef LOG_IF_H
#define LOG_IF_H

/* ---- Define the Software Housekeeping Statistic Codes ---- */
typedef enum e_log
{
  LOG_UPLINK_CALLBACK_NO_THREAD = 0,	/* Cmd addressed to bad thread */
  LOG_UPLINK_GET_NO_THREAD,		/* Queued Cmd addressed bad thread */
  LOG_UTIL_ACK_DROPPED,			/* Ack. was dropped */
  LOG_DMA_UPLINK_NO_BUF,		/* No buffer when packet arrived */
  LOG_DMA_UPLINK_NO_XFR,		/* DMA not setup when pkt arrived */
  LOG_SUPER_SEND_PACKET_DROPPED,	/* SUPER no buf for SW_housekeeping */
  LOG_SCIMODE_FF_DROP_PART,		/* Science Mode Dropped a Partition */
  LOG_EVENT_DATA_LEN_PART_NO_END,       /* Binary search did not locate EOD */
  LOG_HOUSE_SEND_DROPPED,	        /* Housekeeper Dropped a Data Set */
  LOG_DMA_UPLINK_INTS,		        /* DMA Uplink Interrupts */
  LOG_DMA_DOWNLINK_INTS,		/* DMA Downlink Interrupts */
  LOG_TIMING_INTERVAL_INTS,		/* TIMING Interval Interrupts */
  LOG_TIMING_FF_INTS,			/* TIMING First Flag Interrupts */
  LOG_TIMING_MINOR_INTS,		/* TIMING MinorTick Interrupts */

  LOG_LAST_LOG = 64
} E_LOG;

#endif /* LOG_IF_H */

/*-----------------------------------------------------------------------------

TITLE: Flight Software Panic Message (MSG_IF) Include File

DOCUMENT NUMBER: 64-30110.66        REVISION: A

ORIGINATOR: James Francis

LETTER	ECO NO.		DESCRIPTION		CHECKED	APPROVED	DATE
-------------------------------------------------------------------------------
01                      Initial Formal
A                       Initial Letter Release
















































-----------------------------------------------------------------------------*/

/*===========================================================================
* 
*  $Source: /home/xpert/Build/src/Utilities/fs_includes/RCS/msg_if.h,v $
*
*       MODULE NAME: Panic Message Definition Include File (MSG_IF)
* 
*       PURPOSE: Provides Panic Message Definitions.
* 
*       ASSUMPTIONS: NONE
*       
*       REFERENCES:
*       1. "XTE EDS Hardware Functional Descriptions and Requirements"
*           MIT 64-02006.01 Rev. E
* 
*       SOURCE IDENTIFICATION:          MIT 64-30110.66 Rev. A
* 
*  $Log: msg_if.h,v $
*  Revision 5.1  1996/04/16 18:07:14  nessus
*  Bump version number
*
 * Revision 1.2  1994/06/17  20:01:49  berczuk
 * FS Build 2.6
 *
*   Revision 2.1  1994/01/12  03:24:28  jimf
*   Rev. A. Initial Letter Release
*
*   Revision 1.27  1993/10/07  17:34:54  jimf
*   Added title page, updated references and assigned part #.
*
*   Revision 1.26  1993/09/22  17:09:10  jimf
*   Added unknown FFT interval type panic message.
*
*   Revision 1.25  1993/07/06  20:24:02  jimf
*   Removed MSG_STARTUP_QUERY_BAD_THREAD_ID
*
*   Revision 1.24  1993/06/02  19:50:08  jimf
*   Added Housekeeper "no data" panic message
*
*   Revision 1.23  1993/06/02  13:18:03  jimf
*   Added ASM Housekeeper Thread Panic Messages
*
*   Revision 1.22  1993/05/19  16:40:35  jimf
*   Removed MSG_TYPER_SELECT_SPECIAL
*
*   Revision 1.21  1993/05/13  17:44:30  jimf
*   Removed Typer's LLD_ONES_MATCH panic message. Prevents use of "select
*   all combinations" feature.
*
*   Revision 1.20  1993/05/12  19:39:45  jimf
*   Added panic if lld selection code is bad.
*
*   Revision 1.19  1993/05/12  04:03:11  jimf
*   Renamed PATMAP mnemonic "BAD_CODE" to "INTERNAL_ERROR"
*
*   Revision 1.18  1993/05/04  16:26:36  jimf
*   Added Typer Module panic messages.
*
*   Revision 1.17  1993/04/28  16:18:56  jimf
*   Added science mode panic if run_mode() gets NULL header pointer.
*
*   Revision 1.16  1993/04/26  19:21:54  jimf
*   Added RECODER panic messages.
*
*   Revision 1.15  1993/04/06  19:03:58  jimf
*   Added MSG_LAST_MESSAGE as a place holder. Handles comma handling
*   inconsistencies between GCT/GCC and Intermetrics.
*
*   Revision 1.14  1993/04/05  20:38:18  jimf
*   Added Position Histogram Mode Panic Messages.
*   Added Generic Science Mode panic message for interval client detecting
*   spillage, but events weren't enabled during the spilled interval.
*
*   Revision 1.13  1993/04/01  21:16:55  jimf
*   Added some Science Thread Setup Panic Messages.
*
*   Revision 1.12  1993/03/17  14:07:44  jimf
*   Added MSG_SCIMODE_PROCESS_UNLOCKED_BUF and
*   MSG_SCIMODE_PROCESS_BAD_PARTNUM.
*
*   Revision 1.11  1993/03/09  15:40:59  jimf
*   Inserted MSG_SUPER_WRITE_MEM_INVALID_COUNT panic message.
*
*   Revision 1.10  1993/02/25  04:13:13  jimf
*   Added PARTMAP Panic Messages.
*
*   Revision 1.9  1993/02/01  20:27:44  jimf
*   Inserted some STARTUP Panic Messages.
*
*   Revision 1.8  1993/02/01  18:46:27  jimf
*   Added MSG_SCIMODE_IV_DEAD_DSP generic panic message which indicates
*   that the running mode's interval interrupt handler detected that
*   the DSP was not reading its Input FIFO, and suspects that the DSP
*   has crashed.
*
*   Revision 1.7  1993/01/08  19:34:48  jimf
*   Replace TERMINATE_RESET command/panic code with AUTORESTART.
*
*   Revision 1.6  1993/01/05  19:25:37  jimf
*   Noted that MSG_UTIL_PTRFLAT_PTR_RANGE should be deleted.
*
*   Revision 1.5  1993/01/05  03:37:56  jimf
*   Added MSG_SCIENCE_DISPATCH_* Panic Message Codes.
*
*   Revision 1.4  1992/12/21  22:31:54  danh
*   added punctuation
*
*   Revision 1.3  1992/12/21  22:17:00  danh
*   Install SUPERvisior Panic messages
*
*   Revision 1.2  1992/11/24  18:39:19  jimf
*   Added MSG_SCIENCE_TERMINATE_RESET code which is the result of a
*   CMD_TERMINATE_RESET command sent to the Science Thread.
*
 * Revision 1.1  1992/11/24  17:55:58  jimf
 * Initial revision
 *
* 
===========================================================================*/

#ifndef MSG_IF_H
#define MSG_IF_H

/* ---- Defin the Panic Message Codes ---- */
typedef enum e_msg
{
  /* --- INTR Panic Messages --- */
  MSG_INTR_NESTING = 0,				/* Unbalanced Atomic */
  MSG_INTR_HANDLER_INVALID_LEVEL,		/* Bad Level Argument */
  MSG_INTR_HANDLER,				/* Handler exists */
  MSG_INTR_UNEXPECTED_INTR,			/* Unexpected Interpt */
  MSG_INTR_UNEXPECTED_TRAP,               	/* Unexpected Trap */

  /* --- DMA Panic Messages --- */
  MSG_DMA_SET_HANDLER_INVALID_CHANNEL,    	/* Bad Channel Arg. */
  MSG_DMA_SET_HANDLER_COMPLETE_CALLBACK,  	/* Callback exists */
  MSG_DMA_GET_BAD_ADDR,                   	/* Bad Up Xfr Address */
  MSG_DMA_SEND_BAD_ADDR,                  	/* Bad Dn Xfr Address */
  MSG_DMA_DOWNLINK_INT_SPURIOUS,          	/* Unexpected Dn Intr */
  MSG_DMA_UPLINK_INT_SPURIOUS,            	/* Unexpected Up Intr */

  /* --- BUFP Panic Messages --- */
  MSG_BUFP_SETUP_START_ADDR,              	/* Bad Start Addr. */
  MSG_BUFP_SETUP_BIG_POOL_SIZE,           	/* Size > 64K */
  MSG_BUFP_SETUP_SMALL_POOL_SIZE,         	/* Size < buf size */
  MSG_BUFP_SETUP_ODD_BUF_SIZE,            	/* Size is odd */
  MSG_BUFP_RELEASE_BAD_HDR,               	/* Bad buffer hdr. */

  /* --- UPLINK Panic Messages --- */
  MSG_UPLINK_INIT_NO_BUFFERS,             	/* No Up. buffers */
  MSG_UPLINK_BAD_XFR,                     	/* Xfr cnt mismatch */

  /* --- DOWNLINK Panic Messages --- */
  MSG_DOWNLINK_POST_BAD_LENGTH,           	/* Bad packet length */
  MSG_DOWNLINK_POST_BAD_PRIORITY, 	       	/* Bad priority level */
  MSG_DOWNLINK_BAD_XFR, 			/* Xfr cnt mismatch */

  /* --- TIMING Panic Messages --- */
  MSG_TIMING_IV_CLIENT_INSTALLED, 		/* Interval Exists */
  MSG_TIMING_FF_CLIENT_INSTALLED, 		/* First Flag Exists */
  MSG_TIMING_MINOR_CLIENT_INSTALLED,		/* MinorTick Exists */
  MSG_TIMING_FIRST_IV_ALREADY,			/* Interval running */
  MSG_TIMING_FIRST_IV_DURATION, 		/* Bad Duration */
  MSG_TIMING_NEXT_IV_DURATION,			/* Bad Duration */
  MSG_TIMING_NEXT_IV_NOT_RUNNING, 		/* Intrvl not running */
  MSG_TIMING_IV_DONE_NOT_RUNNING, 		/* Intrvl not running */
  MSG_TIMING_TIMEBIN_DURATION,			/* Bad Duration */
  MSG_TIMING_TIMEBIN_RUNNING,			/* Interval running */
  MSG_TIMING_IV_INT_SPURIOUS,			/* Unexpected Int. */
  MSG_TIMING_IV_INT_NESTED,			/* Short Interval  */
  MSG_TIMING_FF_INT_SPURIOUS,			/* Unexpected Int. */
  MSG_TIMING_FF_INT_NESTED,			/* FF too short */
  MSG_TIMING_MINOR_INT_SPURIOUS, 		/* Unexpected Int. */
  MSG_TIMING_MINOR_INT_NESTED,			/* Short MinorTick */

  /* --- UTIL Panic Messages --- */
  MSG_UTIL_STARTUP_NO_BUFFER,			/* No buffer */
  MSG_UTIL_ACK_BAD_DATA_LEN,			/* Back ack. length */
  MSG_UTIL_LOG_BAD_ARGUMENT,			/* Bad statistic id */
  MSG_UTIL_FLATPTR_PTR_RANGE,			/* Bad flat address */

  /* --- BREG Panic Messages --- */
  MSG_BREG_SET_BIT_ZERO, 			/* Zero Mask argument */
  MSG_BREG_SET_BIT_UNUSED,			/* Msk has unused bit */
  MSG_BREG_CLR_BIT_ZERO, 			/* Zero Mask argument */
  MSG_BREG_CLR_BIT_UNUSED,			/* Msk has unused bit */
  MSG_BREG_PULSE_ZERO,				/* Zero Mask argument */
  MSG_BREG_PULSE_BIT_UNUSED,			/* Msk has unused bit */

  /* --- FREG Panic Messages --- */
  MSG_FREG_SET_BIT_ZERO, 			/* Zero Mask argument */
  MSG_FREG_SET_BIT_UNUSED,			/* Msk has unused bit */
  MSG_FREG_CLR_BIT_ZERO, 			/* Zero Mask argument */
  MSG_FREG_CLR_BIT_UNUSED,			/* Msk has unused bit */
  MSG_FREG_PULSE_ZERO,				/* Zero Mask argument */
  MSG_FREG_PULSE_BIT_UNUSED,			/* Msk has unused bit */

  /* --- THREAD Panic Messages --- */
  MSG_TCREATE_NULL_FUNC, 			/* Start Addr is NULL */
  MSG_TCREATE_BAD_ID,				/* Bad Thread Id */
  MSG_TCREATE_ID_USED,				/* Id is in use */
  MSG_TCREATE_NULL_STACK, 			/* No stack */
  MSG_TCREATE_SMALL_STACK,			/* Stack too small */
  MSG_TCREATE_CORRUPTED, 			/* Corrupted */
  MSG_TEXISTS_BAD_ID,				/* Bad Thread Id */
  MSG_TMULTI_CORRUPTED, 			/* Corrupted */
  MSG_TMULTI_NO_THREADS, 			/* No threads exist */
  MSG_TMULTI_RETURN,				/* Multitask return */
  MSG_TWAIT_CORRUPTED,				/* Corrupted */
  MSG_TWAIT_NOT_MULTITASKING,			/* Not Multitasking */
  MSG_TCLEAR_CORRUPTED, 			/* Corrupted */
  MSG_TCLEAR_NOT_MULTITASKING,			/* Not Multitasking */
  MSG_TMULTI_THREAD_RETURN,			/* Thread returned */
  MSG_TSCHED_STACK_CORRUPTED,			/* Stack Corrupted */
  MSG_TSCHED_RESTORE_RETURNED,			/* Context swtch er */
  MSG_TNEXT_THREAD_NO_THREADS,			/* No threads exist */
  MSG_TSCAN_WAITING_WRONG_LIST, 		/* Thrd in wrong lst */
  MSG_TNOTIFY_BAD_ID,				/* Bad Thread Id */

  /* --- ROTUTIL Panic Messages --- */
  MSG_ROTUTIL_DURATION_STEPS,			/* Duration: Bad # of Steps */
  MSG_ROTUTIL_ROTATE_STEPS,			/* Rotate: Bad # of Steps */

  /* --- STARTUP Panic Messages --- */
  MSG_STARTUP_MAIN_PANIC,			/* Multitasking Returned */
  MSG_STARTUP_QUERY_INVALID,			/* Bad Query Id */
 
  /* --- PARTMAP Panic Messages --- */
  MSG_PARTMAP_SETUP_NULL_MAP,			/* NULL Partition Map Ptr */
  MSG_PARTMAP_SETUP_INTERNAL_ERROR,		/* Unknown packing code */
  MSG_PARTMAP_NEW_PARTITION_NO_MAP,		/* No map has been installed */
  MSG_PARTMAP_NEW_PARTITION_TOO_LONG,		/* Part. too long for map */
  MSG_PARTMAP_PACK_NO_MAP,			/* No map has been installed */
  MSG_PARTMAP_PACK_INTERNAL_ERROR,		/* Unknown packing code */
  MSG_PARTMAP_PACK_DATA_PARTMAP_TOO_SHORT,	/* Part. too long for map */

  /* --- RECODER Panic Messages --- */
  MSG_RECODER_DECODE_GROUPINGS_NULL,            /* No group table */
  MSG_RECODER_DECODE_POSITION_NULL,		/* No position table */

  /* --- TYPER Panic Messages --- */
  MSG_TYPER_DECODE_ENCODED_NULL,                /* No encoded table */
  MSG_TYPER_SELECT_DETECTOR,			/* Bad detector selection */
  MSG_TYPER_SELECT_LLDS,			/* Bad LLD Selection code */
  MSG_TYPER_LLD_ONE_BIT,			/* LLD Bitmap is zero */
  MSG_TYPER_LLD_TWO_OR_MORE,			/* Fewer than 2 bits set */
  MSG_TYPER_SPECIAL_TEST,			/* Bad operator code */

  /* --- SCIENCE Panic Messages --- */
  MSG_SCIENCE_AUTORESTART,			/* End of Obs. Cmnded Reset */
  MSG_SCIENCE_DISPATCH_INVALID_MODE_ID,		/* Invalid Mode Identifier */  
  MSG_SCIENCE_DISPATCH_UNKNOWN_MODE,		/* Unknown Science Mode */
  MSG_SCIENCE_SETUP_OBS_NO_DSPBLK,		/* No DSP Loads provided */
  MSG_SCIENCE_SETUP_PCA_NULL_REC,               /* No Recoder Record */

  /* --- SUPER Panic Messages --- */
  MSG_SUPER_READ_MEM_INVALID_ADDR,              /* address pointer is bad */
  MSG_SUPER_WRITE_MEM_INVALID_ADDR,             /* address pointer is bad */
  MSG_SUPER_WRITE_MEM_INVALID_COUNT,            /* write length is bad */
  MSG_SUPER_CALL_SUB_INVALID_ADDR,              /* address pointer is bad */
  MSG_SUPER_CALL_SUB_INVALID_RETURN_TYPE,       /* invalid return arg flag */

  /* --- HOUSE Panic Messages --- */
  MSG_HOUSE_ENABLE_CLIENT_ALREADY,              /* Already Enabled */
  MSG_HOUSE_SEND_NO_DATA,	                /* Sequence # not advanced */
  MSG_HOUSE_MINOR_CLIENT_ALREADY,		/* Already Acquiring */
  MSG_HOUSE_MINOR_CLIENT_QUERY,			/* Bad Query Id */

  /* --- Common Science Mode Panic Messages --- */
  MSG_SCIMODE_IV_DEAD_DSP,	                /* Dead DSP detected */
  MSG_SCIMODE_PROCESS_UNLOCKED_BUF,             /* Process unlocked Sci Buf */
  MSG_SCIMODE_PROCESS_BAD_PARTNUM,		/* Process bad partiton # */
  MSG_SCIMODE_IV_UNEXPECTED_SPILLAGE,		/* Spilled with no events */   
  MSG_SCIMODE_RUN_MODE_NULL_HDR,                /* Mode given NULL parm ptr */

  /* --- Mode Specific Panic Messages --- */
  MSG_POSITION_SETUP_DWELL_DURATION,            /* Short Dwell Duration */
  MSG_POSITION_SETUP_WORDS_PER_PARTITION,	/* Bad Partition Length */

  MSG_FFT_IV_BAD_IVTYPE,	                /* Unknown Interval Type */

  MSG_LAST_MESSAGE		/* Last Message Code */
} E_MSG;

#endif /* MSG_IF_H */

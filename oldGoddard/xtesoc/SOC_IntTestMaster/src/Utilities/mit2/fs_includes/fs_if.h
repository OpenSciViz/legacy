/*-----------------------------------------------------------------------------

TITLE: Flight Software Interfaces (FS_IF) Include File

DOCUMENT NUMBER: 64-30110.65        REVISION: A

ORIGINATOR: James Francis

LETTER	ECO NO.		DESCRIPTION		CHECKED	APPROVED	DATE
-------------------------------------------------------------------------------
01                      Initial Formal
A                       Initial Letter Release

















































-----------------------------------------------------------------------------*/

/*===========================================================================
* 
*  $Source: /home/xpert/Build/src/Utilities/fs_includes/RCS/fs_if.h,v $
*
*       MODULE NAME: Flight Software Interfaces Include File (FS_IF)
* 
*       PURPOSE: Provides Global Definitions for all Flight Software
*                Modules which are needed by the Ground Software.
* 
*       ASSUMPTIONS: NONE
*       
*       REFERENCES:
*       1. "XTE EDS Hardware Functional Descriptions and Requrements"
*           MIT 64-02006.01 Rev. E
* 
*       SOURCE IDENTIFICATION:          MIT 64-30110.65 Rev. A
* 
*  $Log: fs_if.h,v $
*  Revision 5.1  1996/04/16 18:07:12  nessus
*  Bump version number
*
 * Revision 1.2  1994/06/17  20:01:45  berczuk
 * FS Build 2.6
 *
*   Revision 2.1  1994/01/12  03:24:28  jimf
*   Rev. A. Initial Letter Release
*
*   Revision 1.11  1993/10/07  17:34:54  jimf
*   Added title page, updated references and assigned part #.
*
*   Revision 1.10  1993/01/08  19:34:48  jimf
*   Replace TERMINATE_RESET command/panic code with AUTORESTART.
*
*   Revision 1.9  1993/01/05  21:37:58  jimf
*   Added FS_DOWNLINK_HDR_SIZE and incorporated into FS_STREAM_SIZE.
*   Added FS_UPLINK_HDR_SIZE and used it to define FS_COMMAND_SIZE.
*
*   Revision 1.8  1993/01/05  20:07:14  jimf
*   Added the phrase "DOWNLINK_HEADER" to the not about FS_STREAM_SIZE.
*
*   Revision 1.7  1993/01/05  19:57:04  jimf
*   Created FS_STREAM_SIZE which ASSUMES THE SIZE OF THE TELEMTRY PACKET HEADER
*   IS 16 BYTES LONG!!!
*
*   Revision 1.6  1992/12/22  14:47:59  danh
*   Added Command Opcode and Reply Enumerated Types (jimf/danh).
*
*   Revision 1.5  1992/12/02  15:22:09  jimf
*   Changed THREAD_ALL_THREADS to THREAD_ID_ALL_THREADS.
*
*   Revision 1.4  1992/12/02  15:13:50  jimf
*   Added THREAD_ALL_THREADS to thread id list.
*
*   Revision 1.3  1992/12/01  21:58:10  jimf
*   Changed NUM_EA to FS_NUM_EA.
*   Added FS_UPLINK_SIZE and FS_DOWNLINK_SIZE to replace
*   DMA_UP_SIZE, DMA_DN_SIZE, DOWN_SIZE and UPLINK_PACKET_SIZE.
*
*   Revision 1.2  1992/11/24  18:08:15  jimf
*   Changed FS_NUM_EA back to NUM_EA for the moment (compatibility issues).
*
 * Revision 1.1  1992/11/24  17:55:58  jimf
 * Initial revision
 *
* 
===========================================================================*/

#ifndef FS_IF_H
#define FS_IF_H

/* ---- Define some Global System Interface Constants ---- */
#define FS_NUM_EA 	 (8)	       /* # of Event Analyzers in EDS */
#define FS_UPLINK_SIZE   (64)	       /* Max. size of Uplink Packet */
#define FS_DOWNLINK_SIZE (1024)	       /* Max size of Telem. Packet */


/* ---- Define the # bytes in a telemetry pkt useable for stream data ---- */
/* ----
 * NOTE: This constant is TIGHTLY COUPLED to the size of the CCSDS
 * Primary Header, the size of the Telemetry Packet's Secondary Header
 * and the size of the EDS Telemetry Header
 * (see downlink.h, DOWNLINK_HEADER).
---- */
#define FS_DOWNLINK_HDR_SIZE (16)
#define FS_STREAM_SIZE       (FS_DOWNLINK_SIZE - FS_DOWNLINK_HDR_SIZE)


/* ---- Define the # bytes in a command pkt useable for command data ---- */
/* ----
 * NOTE: This constant is TIGHTLY COUPLED to the size of the CCSDS
 * Primary Header, the size of the Command Packet's Secondary Header
 * and the size of the EDS Command Header
 * (see uplink.h, UPLINK_HEADER).
---- */
#define FS_UPLINK_HDR_SIZE   (10)
#define FS_COMMAND_SIZE      (FS_UPLINK_SIZE - FS_UPLINK_HDR_SIZE)


/* ---- Define Flight Software Thread Id's ---- */
typedef enum e_thread_id
{
  THREAD_ID_SUPERVISOR = 0,	/* Supervisor Thread (see super.h) */
  THREAD_ID_SCIENCE,		/* Science Thread (see science.h) */
  THREAD_ID_HOUSEKEEPER,	/* ASM Housekeeper Thread (see asmhouse.h) */

  /* -- Add New Threads Here (up to 8 total) -- */

  THREAD_ID_ALL_THREADS = 8	/* Use to Broadcast an Event to all threads */
				/* --- NOTE:
				 * DO NOT ATTEMPT TO ADDRESS A COMMAND PACKET
				 * TO THIS VALUE
				--- */
} E_THREAD_ID;

/* ---- Command Opcodes ---- */
typedef enum e_cmd_opcode
{
  CMD_SUPER_READ_MEM = 0,	/* Supervisor Thread Read Memory */
  CMD_SUPER_WRITE_MEM,		/* Supervisor Thread Write Memory */
  CMD_SUPER_CALL_SUB,		/* Supervisor Thread Call-Subroutine */

  CMD_SCIENCE_SUSPEND,		/* Science Thread Suspend Science Data */
  CMD_SCIENCE_RESUME,		/* Science Thread Resume Science Data */
  CMD_SCIENCE_TERMINATE,	/* Science Thread Terminate Observation */
  CMD_SCIENCE_AUTORESTART	/* Science Thread Reset EA after Obs. */

  /* -- Add Command Opcodes Here (up to 32 total) -- */

} E_CMD_OPCODE;

/* ---- Command Reply Codes ---- */
typedef enum e_cmd_replycode
{
  CMD_REPLY_OK = 0,		/* Command Executed Succesfully */
  CMD_REPLY_NONE,		/* Command Did Not Apply to Current State */
  CMD_REPLY_UNKNOWN,		/* Command Opcode Was Not Known by Thread */
  CMD_REPLY_ALREADY		/* Command's Effect Was Already Active */

  /* -- Add Command Replies Here (up to 64K total) -- */

} E_CMD_REPLY;

/* ---- Downlink Packet Streams Definitions ---- */
typedef enum e_stream_id
{
  STREAM_ID_STARTUP_MSG = 0,	/* Startup Message Packet (see util.h) */
  STREAM_ID_PANIC_MSG,		/* Panic Message Packet (see downlink.h) */
  STREAM_ID_CMD_ACK,		/* Command Ack Packet (see util.h) */
  STREAM_ID_SW_HOUSE,		/* SW Housekeeping Packet (see log_if.h) */
  STREAM_ID_ASM_SENSOR,		/* ASM Housekeeping Packet (see asmhouse.h) */
  STREAM_ID_OBS_PARAM,		/* Observation Dump Packet (see science.h) */
  STREAM_ID_PARTHDR,		/* Partition Header Packet (see science.h) */
  STREAM_ID_PARTDATA,		/* Partition Data Packet (see science.h) */
  STREAM_ID_OBS_CODA		/* Observation Coda Packet (see science.h) */

  /* -- Add Stream Id's Here (up to 256 total) -- */

} E_STREAM_ID;

/* ---- For Panic Message Codes, see msg_if.h ---- */
/* ---- For Software Housekeeping Codes, see log_if.h ---- */

#endif /* FS_IF_H */

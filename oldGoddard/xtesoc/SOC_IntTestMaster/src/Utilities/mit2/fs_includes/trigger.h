/*-----------------------------------------------------------------------------

TITLE: Binned Science Mode Burst Triggers (BINNED) Include File

DOCUMENT NUMBER: 64-30112.060801        REVISION: A

ORIGINATOR: James Francis

LETTER	ECO NO.		DESCRIPTION		CHECKED	APPROVED	DATE
-------------------------------------------------------------------------------
01                      Initial Version
A                       Initial Letter Release

















































-----------------------------------------------------------------------------*/

/*===========================================================================
* 
*  $Source: /home/xpert/Build/src/Utilities/fs_includes/RCS/trigger.h,v $
*
*       MODULE NAME: Binned Science Mode Burst Trigger Include File (BINNED)
* 
*       PURPOSE: Provides Structures/Definitions/Prototypes for the
*                Binned Science Mode's Baseline Burst Trigger Routines.
* 
*       ASSUMPTIONS: NONE
*
*       LIMITATIONS:
*                The words/call values in this file must be adjusted such
*                that the corresponding analysis routine takes less than
*                the time to transfer a packet to the SDS (approx. 8ms).
*       
*       REFERENCES:
*       1. "Science Thread Module Specification"
*           MIT 64-30112.0601 Rev. D
*       2. "Binned Mode Module Specification"
*           MIT 64-30110.010501 Rev. C
*       3. "XTE EDS Hardware Functional Descriptions and Requirements"
*           MIT 64-02006.01 Rev. E
* 
*       SOURCE IDENTIFICATION:          MIT 64-30112.060801 Rev. A
* 
*  $Log: trigger.h,v $
*  Revision 5.1  1996/04/16 18:07:17  nessus
*  Bump version number
*
 * Revision 1.1  1996/02/15  23:55:01  nessus
 * Initial revision
 *
 * Revision 2.2  1994/04/29  12:38:06  jimf
 * Added numerator pre-scale feature to Hardness Trigger ratio computation.
 *
 * Revision 2.1  1994/01/11  21:17:23  jimf
 * Rev. A. Initial Letter Release
 *
 * Revision 1.5  1993/12/01  19:57:55  jimf
 * Fixed a couple of typos.
 *
 * Revision 1.4  1993/09/20  16:24:06  jimf
 * Added TBR's on the words/call constants. These values must be chosen such
 * that their corresponding analysis routines run for no more than 8ms a
 * shot.
 *
 * Revision 1.3  1993/09/20  13:51:41  jimf
 * Made Edge Trigger partition/observation threshhold values signed 32-bit
 * values to ensure that comparison with computed difference is performed
 * using signed arithmetic.
 *
 * Revision 1.2  1993/09/17  16:57:15  jimf
 * Added state variables for all three triggers.
 * Modified all triggers to support bounded word counts per call.
 * Added hardness and edge triggers.
 *
 * Revision 1.1  1993/07/02  14:29:32  jimf
 * Initial revision
 *
* 
===========================================================================*/

#ifndef TRIGGER_H
#define TRIGGER_H

/* ---- Exported Function Prototypes ---- */
BOOL TRIGGERlevel (UINT8 *parms,	     /* Trigger Argument Structure */
		   UINT8 *triggered_mask,    /* Allowable triggers */
		   UINT32 partition_id,	     /* Partition Being Analyzed */
		   UINT16 call_id,	     /* # times called for Partition */
		   UINT16 *partition,	     /* Start of Partition */
		   UINT16 len		     /* # words in Partition */
		  );

BOOL TRIGGERedge (UINT8 *parms,		     /* Trigger Argument Structure */
		  UINT8 *triggered_mask,     /* Allowable triggers */
		  UINT32 partition_id,	     /* Partition Being Analyzed */
		  UINT16 call_id,	     /* # times called for Partition */
		  UINT16 *partition,	     /* Start of Partition */
		  UINT16 len		     /* # words in Partition */
		 );

BOOL TRIGGERhardness (UINT8 *parms,	     /* Trigger Argument Structure */
		      UINT8 *triggered_mask, /* Allowable triggers */
		      UINT32 partition_id,   /* Partition Being Analyzed */
		      UINT16 call_id,	     /* # times called for Partition */
		      UINT16 *partition,     /* Start of Partition */
		      UINT16 len	     /* # words in Partition */
		     );

/* ---- Define the Common Burst Trigger Flags ---- */
#define TRIGGER_FLAGS_MOST_INTERESTING (1)   /* Most Interesting Trigger */
#define TRIGGER_FLAGS_HEXTE            (2)   /* Trigger HEXTE */


/* ---- Define Level Trigger Call Limit ---- */
#define TRIGGER_LEVEL_WORDS_PER_CALL   (1024) /* Limit per Call */

/* ---- Define Edge Trigger Mean Length Limit and Call Limit ---- */
#define TRIGGER_EDGE_MEAN_LIMIT        (128)  /* Max length of 128 words */
#define TRIGGER_EDGE_WORDS_PER_CALL    (128)  /* Limit per Call */

/* ---- Define Hardness Trigger Call Limit ---- */
#define TRIGGER_HARD_WORDS_PER_CALL    (512)  /* Limit per Call */


/* ---- List of Baseline Trigger Types ---- */
typedef enum e_trigger_type
{
  TRIGGER_TYPE_LEVEL,			     /* Level Trigger */
  TRIGGER_TYPE_EDGE,			     /* Edge Trigger */
  TRIGGER_TYPE_HARDNESS			     /* Hardness Trigger */

} TRIGGER_TYPE;

/* ---- Burst Trigger Common Header ---- */
typedef struct s_trigger_header
{
  UINT8   length;			     /* Length of parameters */
  UINT8   type;				     /* Type of Trigger Parameters */

} TRIGGER_HEADER;

/* ---- Level Trigger Parameters ---- */
typedef struct s_trigger_level_parms
{
  TRIGGER_HEADER hdr;			     /* Trigger Header */
  UINT8          flags;			     /* Level Trigger Flags */
  UINT8          unused;		     /* Unused Pad Byte */
  UINT16         offset;		     /* Offset into partition */
  UINT16         length;		     /* Number of words to analyze */
  UINT16         threshhold;		     /* Level Threshhold */
} TRIGGER_LEVEL_PARMS;

/* ---- Edge Trigger Parameters ---- */
typedef struct s_trigger_edge_parms
{
  TRIGGER_HEADER hdr;			     /* Trigger Header */
  UINT8          flags;			     /* Level Trigger Flags */
  UINT8          mean_length;		     /* Length of Running Mean */
  UINT16         offset;		     /* Offset into partition */
  UINT16         length;		     /* Number of words to analyze */
  UINT16         threshhold;		     /* Edge Threshhold */
} TRIGGER_EDGE_PARMS;

/* ---- Hardness Trigger Parameters ---- */
typedef struct s_trigger_hardness_parms
{
  TRIGGER_HEADER hdr;			     /* Trigger Header */
  UINT8          flags;			     /* Level Trigger Flags */
  UINT8          scale;			     /* Numerator Scale */
  UINT16         num_offset;		     /* Offset to Numerator Range */
  UINT16         den_offset;		     /* Offset to Denominator Range */
  UINT16         length;		     /* Number of words to analyze */
  UINT16         threshhold;		     /* Hardness Threshhold */
} TRIGGER_HARDNESS_PARMS;


/* ---- Level Trigger State Variables ---- */
typedef struct s_trigger_level_vars
{
  UINT16 threshhold;			/* Inter-partition threshhold */
  UINT16 part_thresh;			/* Local partition threshhold */
  BOOL   trigger;			/* Local partition trigger flag */
} TRIGGER_LEVEL_VARS;

/* ---- Edge Trigger State Variables ---- */
typedef struct s_trigger_edge_vars
{
  UINT16 ring[TRIGGER_EDGE_MEAN_LIMIT]; /* Ring-buffer of partition values */
  UINT16 ringcnt;		        /* # values currently in ring */
  UINT16 index;				/* Current index into ring */
  UINT32 sum;				/* Current Sum of values in ring */
  INT32  threshhold;			/* Inter-partition threshhold */
  INT32  part_thresh;			/* Local partition threshhold */
  BOOL   trigger;			/* Local partition trigger flag */
} TRIGGER_EDGE_VARS;

/* ---- Hardness Trigger State Variables ---- */
typedef struct s_trigger_hard_vars
{
  UINT16 threshhold;			/* Inter-partition threshhold */
  UINT16 part_thresh;			/* Local partition threshhold */
  BOOL   trigger;			/* Local partition trigger flag */
} TRIGGER_HARD_VARS;

#endif /* TRIGGER_H */

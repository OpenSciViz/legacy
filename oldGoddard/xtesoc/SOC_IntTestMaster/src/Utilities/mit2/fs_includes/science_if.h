/*-----------------------------------------------------------------------------

TITLE: Science Thread Module (SCIENCE) Interface Include File

DOCUMENT NUMBER: 64-30112.0604        REVISION: A

ORIGINATOR: James Francis

LETTER	ECO NO.		DESCRIPTION		CHECKED	APPROVED	DATE
-------------------------------------------------------------------------------
01                      Initial Version
02                      References/Limits
A                       Initial Letter Release
















































-----------------------------------------------------------------------------*/

/*===========================================================================
* 
*  $Source: /home/xpert/Build/src/Utilities/fs_includes/RCS/science_if.h,v $
*
*       MODULE NAME: Science Thread Interfaces Include File (SCIENCE)
* 
*       PURPOSE: Provides Definitions for the Science Thread
*                Module which are needed by the Ground Software.
* 
*       ASSUMPTIONS: NONE
*       
*       REFERENCES:
*       1. "Science Thread Module Specification",
*           MIT 64-30112.0601 Rev. D
*       2. "XTE EDS Hardware Functional Descriptions and Requirements"
*           MIT 64-02006.01 Rev. E
* 
*       SOURCE IDENTIFICATION:          MIT 64-30112.0604 Rev. A
* 
*  $Log: science_if.h,v $
*  Revision 5.1  1996/04/16 18:07:15  nessus
*  Bump version number
*
 * Revision 1.2  1994/06/17  20:01:51  berczuk
 * FS Build 2.6
 *
 * Revision 2.1  1994/01/11  03:43:36  jimf
 * Rev. A. Initial Letter Release
 *
 * Revision 1.5  1993/10/07  15:01:18  jimf
 * Updated references, limitations. Eliminated TBDs
 *
 * Revision 1.4  1993/03/23  20:27:14  jimf
 * Changed SCIENCE_MAX_MODES from 256 to 255
 *
 * Revision 1.3  1993/03/08  18:45:55  jimf
 * Updated part number and some comments.
 *
 * Revision 1.2  1993/01/26  20:07:53  jimf
 * Fixed #define SCINECE_IF_H.
 *
 * Revision 1.1  1993/01/08  21:16:12  jimf
 * Initial revision
 *
* 
===========================================================================*/

#ifndef SCIENCE_IF_H
#define SCIENCE_IF_H

/* ---- Define some limitations ---- */
#define SCIENCE_MAX_MODES (255)	      /* Max. number of possible modes */
#define SCIENCE_MAX_COMP  (256)	      /* Max. # of component types */


/* ---- Enumerate all of the currently defined Science Modes Id's ---- */
/* ----
 * NOTE: If the order of these change, or if a new mode is INSERTED before
 * an existing mode, the dispatch table in science.c must be modified.
---- */ 
typedef enum e_science_mode
{
  PCA_MODE_BINNED = 0,		/* 0: Binned Mode */
  PCA_MODE_PULSAR,		/* 1: Pulsar Fold Mode */
  PCA_MODE_EVENT,		/* 2: Event Encoded Mode */
  PCA_MODE_SINGLEBIT,		/* 3: SingleBit Mode */
  PCA_MODE_CATCHER,		/* 4: Burst Catcher Mode */
  PCA_MODE_FFT,			/* 5: FFT Mode */
  
  ASM_MODE_POSITION,		/* 6: Position Histogram Mode */
  ASM_MODE_MULTIPLE,		/* 7: Multiple Time-Series Mode */
  ASM_MODE_EVENT		/* 8: Event by Event Mode */

  /* -- Add New Mode Codes Here (up to 255 total) -- */

} SCIENCE_MODE;


/* ---- Enumerate current defined Configuration Component Tags ---- */
typedef enum e_obs_comp
{
  OBS_COMP_HEADER = 0,		/* Observation Parameter Block Header */
  OBS_COMP_MAIN_DSP,		/* Main DSP Load Block */
  OBS_COMP_AUX_DSP,		/* Auxiliary DSP Load Block */
  OBS_COMP_PARTMAP,		/* Partition Map */

  OBS_COMP_PCA,			/* PCA Parameter Block */
  OBS_COMP_TYPER,		/* Typer Parameter Block */
  OBS_COMP_RECODER,		/* Recoder Record */
  OBS_COMP_REC_CORR,		/* Recoder Corrections Parameters */
  OBS_COMP_REC_GROUP,		/* Recoder Grouping Parameters */
  OBS_COMP_REC_POS,		/* Recoder Output Position Parameters */

  OBS_COMP_BINNED,		/* Binned Mode Parameters */
  OBS_COMP_TRIGGER,		/* Binned Mode's Burst Trigger Parameters */

  OBS_COMP_PULSAR,		/* Pulsar Fold Mode Parameters */
  OBS_COMP_EVENT,		/* Event Encoded Mode Parameters */
  OBS_COMP_SINGLEBIT,		/* SingleBit Mode Parameters */
  OBS_COMP_CATCHER,		/* Burst Catcher Mode Parameters */
  OBS_COMP_FFT,			/* FFT Mode Parameters */

  OBS_COMP_POSITION,		/* Position Histogram Mode Parameters */
  OBS_COMP_TIMESER,		/* Multiple Time-Series Mode Parameters */
  OBS_COMP_ASMEVENT		/* Event by Event Mode Parameters */

  /* --- Add New Component Tags Here (up to 256 total) --- */

} OBS_COMP_ID;


/* ---- Configuration Data Structures ---- */
/* ----
 * The component tags listed above define the existence of the various
 * configuration structures, but not their content. The following specifies
 * where each of the component structures are defined:
 *
 * Component Tag        File		Structure Typedef
 * -------------	----		-----------------
 * OBS_COMP_HEADER  	science.h	SCIENCE_OBS_HDR
 * OBS_COMP_MAIN_DSP	science.h	SCIENCE_DSP_LOAD
 * OBS_COMP_AUX_DSP	science.h	SCIENCE_DSP_LOAD
 * OBS_COMP_PARTMAP	partmap.h	Array of UINT16's
 *
 * OBS_COMP_PCA		science.h	SCIENCE_PCA_HDR
 * OBS_COMP_TYPER	typer.h		TYPER_ENCODED
 * OBS_COMP_RECODER	science.h	SCIENCE_RECODER_REC
 * OBS_COMP_REC_CORR	recoder.h	RECODER_CORRECTIONS
 * OBS_COMP_REC_GROUP	recoder.h	RECODER_GROUPINGS
 * OBS_COMP_REC_POS	recoder.h	RECODER_POSITIONS
 *
 * OBS_COMP_BINNED	binned.h	BINNED_HDR
 * OBS_COMP_TRIGGER	trigger.h	TRIGGER_HEADER
 *                                      In practice,
 *                                         TRIGGER_LEVEL_PARMS,
 *                                         TRIGGER_EDGE_PARMS,
 *                                         TRIGGER_HARDNESS_PARMS, or others
 *                                      all of which start with TRIGGER_HEADER
 *                                      and are discriminated by "type" field.
 *
 * OBS_COMP_PULSAR	pulsar.h	PULSAR_HDR
 * OBS_COMP_EVENT	event.h		EVENT_PARA_BLK
 * OBS_COMP_SINGLEBIT	singlebit.h	SINGLEBIT_HDR
 * OBS_COMP_CATCHER	catcher.h	CATCHER_HDR
 * OBS_COMP_FFT		fft.h		FFT_HDR
 *
 * OBS_COMP_POSITION	position.h	POSITION_HDR
 * OBS_COMP_TIMESER	timeser.h	TIMESERIES_HDR
 * OBS_COMP_ASMEVENT	asmevent.h	ASMEVENT_HDR
 *
---- */

/* ---- Telemetry Data Structures ---- */
/* ----
 * The Science Thread produces four types of telemetry packets,
 * Compoment Dumps, Partition Headers, Partition Data, and
 * Observation Coda's. The Stream Id's for these packets are defined
 * in fs_if.h. The data structures associated with each of these
 * stream id's are contained in science.h, and are named as follows:
 *
 * Stream Id			Structure Typedef
 * ---------			-----------------
 * STREAM_ID_OBS_PARAM		SCIENCE_DUMP_HDR (sequence = 0) or
 *                              SCIENCE_DUMP_DATA (sequence != 0)
 * STREAM_ID_PARTHDR		SCIENCE_PARTHDR
 * STREAM_ID_PARTDATA		SCIENCE_PARTDATA (body is mode dependent)
 * STREAM_ID_OBS_CODA		SCIENCE_OBS_CODA
 *
---- */

#endif /* SCIENCE_IF_H */

/*-----------------------------------------------------------------------------

TITLE: Patch Extensions Include File

DOCUMENT NUMBER: 64-30112.xxxxxx        REVISION: 01

ORIGINATOR: James Francis

LETTER	ECO NO.		DESCRIPTION		CHECKED	APPROVED	DATE
-------------------------------------------------------------------------------
01                      Initial Version


















































-----------------------------------------------------------------------------*/

/*===========================================================================
* 
*  $Source: /home/xpert/Build/src/Utilities/fs_includes/RCS/extensions.h,v $
*
*       MODULE NAME: Extensions Include File (EXTENSIONS)
* 
*       PURPOSE: Provides Definitions for new science modes and other
*                patch extensions.
* 
*       ASSUMPTIONS: NONE
*       
*       REFERENCES:
*       1. "Science Thread Module Specification"
*           MIT 64-30112.0601 Rev. E
*       2. "Multiple Time Series Mode Module Specification"
*           MIT 64-30112.010509 Rev. C
*       3. "XTE EDS Hardware Functional Descriptions and Requirements"
*           MIT 64-02006.01 Rev. E
* 
*       SOURCE IDENTIFICATION:          MIT 64-30112.xxxxxx Rev. 01
* 
*  $Log: extensions.h,v $
*  Revision 5.1  1996/04/16 18:07:10  nessus
*  Bump version number
*
 * Revision 1.2  1996/04/09  08:13:13  nessus
 * New version that supports PCA Safety Triggers.
 *
 * Revision 1.3  1996/04/05  18:53:27  ehm
 * Level Trigger now has upper/lower thresholds
 *
 * Revision 1.2  1996/04/04  20:08:58  ehm
 * Added support for PCA safety trigger
 *
 * Revision 1.1  1996/02/05  18:54:04  ehm
 * Initial revision
 *
* 
===========================================================================*/

#ifndef EXTENSIONS_H
#define EXTENSIONS_H 1

#include "science_if.h"
#include "trigger.h"

typedef enum e_new_science_mode
{
    FIRST_NEW_MODE = ASM_MODE_EVENT+1,
    ASM_MODE_TRIGTS = FIRST_NEW_MODE /* 9: Multiple Timeseries with Trigger */
} NEW_SCIENCE_MODE;

typedef enum e_new_obs_comp
{
    FIRST_NEW_COMP = OBS_COMP_ASMEVENT+1,
    OBS_COMP_TRIGTS = FIRST_NEW_COMP /* Multiple Timeseries with Trigger */
} NEW_OBS_COMP_ID;

typedef enum e_new_trigger_type
{
    FIRST_NEW_TRIGGER_TYPE = TRIGGER_TYPE_HARDNESS+1,
    TRIGGER_TYPE_TRIGTS_LEVEL = FIRST_NEW_TRIGGER_TYPE,	  /* Mult. Timeseries Trigger */
    TRIGGER_TYPE_TRIGSAF = TRIGGER_TYPE_TRIGTS_LEVEL + 1  /* PCA Safety Trigger */
} NEW_TRIGGER_TYPE;

/* ----- Multiple Timeseries with Trigger Level Trigger Parameters ---- */
typedef struct s_trigger_trigts_level_entry
{
    UINT16 bin_number;		/* Index of Bin to examine */
    UINT16 threshold;		/* Set LED if Bin >= Threshold */
    UINT16 led;			/* LED value to set when threshold exceeded */
} TRIGGER_TRIGTS_LEVEL_ENTRY;

typedef struct s_trigger_trigts_level_parms
{
    TRIGGER_HEADER hdr;		/* Trigger Header */

    /* Remainder of block contains TRIGGER_TRIGTS_LEVEL_ENTRYs */
    /* Number of entries is (hdr.length - 1) / 3 */

} TRIGGER_TRIGTS_LEVEL_PARMS;


/* ---- PCA Safety Trigger Parameters ---- */
typedef struct s_trigger_trigsaf_level_entry
{
    UINT16 bin_number;		/* Index of Bin to examine */
    UINT16 upper_threshold;	/* Set LED if Bin > Threshold */
    UINT16 lower_threshold;	/* Set LED if Bin < Threshold */
    UINT16 led;			/* LED value to set when threshold exceeded */
} TRIGGER_TRIGSAF_LEVEL_ENTRY;

typedef struct s_trigger_trigsaf_hard_entry
{
    UINT16 numerator_bin;	/* Index of Numerator Bin to examine */
    UINT16 denominator_bin;	/* Index of Denominator Bin to examine */
    UINT16 scale;		/* Factor to scale numerator by before the divide */
    UINT16 threshold;		/* Set LED if Bin >= Threshold */
    UINT16 led;			/* LED value to set when threshold exceeded */

} TRIGGER_TRIGSAF_HARD_ENTRY;

typedef struct s_tirgger_trigsaf_parms
{
    TRIGGER_HEADER hdr;		/* Trigger Header */
    UINT16         holdFlag;	/* Hold Flag */

    /* First is a UINT16 word specifying the number of level entries */
    /* followed by that number of TRIGGER_TRIGSAF_LEVEL_ENTRY structures */

    /* Next is a UINT16 word specifying the number of hardness entries */
    /* followed by that number of TRIGGER_TRIGSAF_HARD_ENTRY structures */
    
} TRIGGER_TRIGSAF_PARMS;

#endif /* EXTENSIONS_H */



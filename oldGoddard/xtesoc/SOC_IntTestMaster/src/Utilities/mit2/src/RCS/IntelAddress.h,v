head	4.0;
access
	soccm;
symbols
	Build4_3_1:3.6
	Build4_3:3.6
	OldSOCTime:3.6
	Build4_2:3.6
	Build4_1:3.6
	BUILD:3.6
	Build4:3.6
	Build_4:3.6
	Build3_3:3.5
	Build3_2:3.4
	Build3_1:3.2
	Build3:3.2
	Build2:2.0
	Build1:2.0;
locks
	buehler:4.0; strict;
comment	@ * @;


4.0
date	95.06.08.16.57.02;	author buehler;	state Exp;
branches;
next	3.6;

3.6
date	94.11.09.13.17.02;	author buehler;	state Submitted;
branches;
next	3.5;

3.5
date	94.08.17.18.07.41;	author berczuk;	state Submitted;
branches;
next	3.4;

3.4
date	94.08.12.19.03.53;	author berczuk;	state Submitted;
branches;
next	3.3;

3.3
date	94.08.01.20.12.47;	author buehler;	state Exp;
branches;
next	3.2;

3.2
date	94.03.23.16.39.29;	author buehler;	state Exp;
branches;
next	3.1;

3.1
date	94.03.23.15.54.54;	author buehler;	state Exp;
branches;
next	3.0;

3.0
date	93.12.15.21.36.52;	author buehler;	state Exp;
branches;
next	2.0;

2.0
date	93.11.29.01.37.46;	author buehler;	state Release;
branches;
next	1.1;

1.1
date	93.07.13.13.54.18;	author buehler;	state Exp;
branches;
next	1.0;

1.0
date	93.07.08.00.04.20;	author buehler;	state Exp;
branches;
next	0.2;

0.2
date	93.06.02.21.01.02;	author buehler;	state Exp;
branches;
next	0.1;

0.1
date	93.06.02.17.45.27;	author buehler;	state Exp;
branches;
next	0.0;

0.0
date	93.06.02.15.37.54;	author buehler;	state Exp;
branches;
next	;


desc
@Class headers for IntelAddress, whose objects represent 
addresses in segment:offset form for use with Intel family processors
@


4.0
log
@Forward declare ostream class.
@
text
@//===========================================================================
// IntelAddress.h
// RCS Stamp: $Id: IntelAddress.h,v 3.6 1994/11/09 13:17:02 buehler Submitted buehler $
// Author:    Royce Buehler   - buehler@@space.mit.edu
// Copyright: Massachusetts Institute of Technology
//
#ifndef INTEL_ADDR_H
#define INTEL_ADDR_H
// .NAME IntelAddress - Segment:offset address objects for Intel processors
// .LIBRARY MIT_Config_Tools
// .HEADER
// .INCLUDE IntelAddress.h
// .FILE IntelAddress.h
// .FILE IntelAddress.C
// .SECTION Description
// Class IntelAddress
// A simple class to handle segmented addresses as used by Intel
// microprocessors. Results are unspecified past one megabyte.
// REVISION HISTORY
// 4.0  Jun  6 95 reb Forward declare ostream class.
//==================================================================

#include "mit_td.h"
#include "Faults.h"

class ostream;
enum IntelAddressFault { IAF_NONE, IAF_OVERFLOW, IAF_UNDERFLOW, 
			 IAF_OUT_OF_SEG};

class MachineAddress
{
public:
    MachineAddress(UINT32 address)
    {
        asegment= (UINT16)((address & 0xFFFF0000) >>16);
        aoffset = (UINT16)(address & 0x0000FFFF);
    }
        
    UINT16 segment() const
    {
        return asegment;
    }
    UINT16 offset() const
    {
        return aoffset;
    } 
    
private:
    UINT16 asegment;
    UINT16 aoffset;
};


class IntelAddress 
{
  friend ostream& operator<< (ostream&, const IntelAddress&);
public:
  IntelAddress(void) : seg(0), off(0) {};
  // Null constructor creates an Intel address of 0:0

  IntelAddress(UINT16 segment, UINT16 offset);
  // Constructs an address of segment:offset. 
  // If result would point past one megabyte, produces an unspecified
  // address under one megabyte, sets "valid" flag off, and sets
  // fault condition to IAF_OVERFLOW.00

    IntelAddress(const MachineAddress& add);
    
    
  IntelAddress(UINT32 flatAddress);
  // Constructs Intel address from a flat address. Highest possible
  // segment base is used.
  // If the flat address is outside DOS address space (above 1 megabyte),
  // only the 20 least significant bits will be used, the new object
  // will have "valid" flag off, and the IAF_OVERFLOW fault will be set.
  
  UINT16        getSegment(void) const { return(seg); };
  // Returns the segment base.

  UINT16        getOffset(void) const { return(off); };
  // Returns the offset from the segment base.

  UINT32        getFlat(void) const { return( (((UINT32) seg) << 4) + off); };
  // Returns the flat address equivalent to this segment and offset.

  IntelAddress  operator+(long byteCount) const;
  // Return an address byteCount bytes past this one.
  // If the result is within segment, retain base segment;
  // Otherwise result is a new base with offset 0 to F.
  // May set IAF_OVERFLOW fault.

  IntelAddress operator-(long sub) const { return( *this + (- sub)); } ;
  // Return an address "sub" bytes previous to this one.
  // Retain segment base if possible.
  // May set IAF_UNDERFLOW fault.

  UINT32        operator-(const IntelAddress& )const ;
  // Return distance between the two addresses. Result is never negative.
  // If necessary, check first which address is higher.

  // Comparison operators follow: ==, <, >, <==, >==, !=. 
  // All compare the 2 physical addresses, regardless
  // of segment bases. Return 1 for true, 0 for false.

  int           operator==(const IntelAddress& ) const;
  int           operator<=(const IntelAddress& ) const;
  int           operator<(const IntelAddress& ) const;
  int           operator>(const IntelAddress& ) const;
  int           operator>=(const IntelAddress&) const;
  int           operator!=(const IntelAddress&) const;

  mitBOOL        isSameAs(const IntelAddress& ) const; 
  // True (1) if the two addresses have identical segments & offsets.

  mitBOOL        sharesSegmentWith(const IntelAddress& ) const;
  // Return 1 if this address and argument are within 64K of one another.

  IntelAddress  usingBase( const IntelAddress& ) const;
  // Return an address equal to this, but using the argument address as
  // the segment base. If out of range, sets IAF_OUT_OF_SEG fault
  // and returns the original address.

  void asChars( unsigned char * dest) const;
  // Places 4 bytes in "dest" in the proper order for a CCSDS Packet

  mitBOOL isValid(void) const { return(validp);};
  // Return 0 if never initialized with a valid Intel address.
  // isValid() remains YES when an illegal request is made to a valid
  // IntelAddress object. The request is ignored.

  static const Fault* getFault(void);
  // Return the fault encountered during the most recent operation on
  // this class capable of generating a fault. If there are no
  // outstanding faults, returns null pointer.

  static void         dontClearFault(void);
  // This command takes effect only through the next call to
  // IntelAddress::getFault(), which will return the most recent
  // fault that occurred (rather than the fault status of the most
  // recent faultable operation on this class.)

private:
  static Faultable theFault;
  UINT16 seg;
  UINT16 off;
  mitBOOL validp;
};

inline int
IntelAddress::operator==( const IntelAddress& otherAddr) const 
{
  return( *this - otherAddr == 0);
}

ostream& operator<< (ostream&, const IntelAddress&);

#endif  // INTEL_ADDR_H



@


3.6
log
@Changed BOOL to mitBOOL to avoid conflict with SOC Mission Planning.
@
text
@d3 1
a3 1
// RCS Stamp: $Id: IntelAddress.h,v 3.5 1994/08/17 18:07:41 berczuk Submitted buehler $
d19 2
a24 1
#include <iostream.h>
d26 1
@


3.5
log
@fixed machine address constructor.. (error was with parens)
@
text
@d3 1
a3 1
// RCS Stamp: $Id: IntelAddress.h,v 3.4 1994/08/12 19:03:53 berczuk Submitted berczuk $
d110 1
a110 1
  BOOL           isSameAs(const IntelAddress& ) const; 
d113 1
a113 1
  BOOL           sharesSegmentWith(const IntelAddress& ) const;
d124 1
a124 1
  BOOL isValid(void) const { return(validp);};
d144 1
a144 1
  BOOL validp;
@


3.4
log
@added constructor taking machineAddress, which is the address as it is
returned from the EDS
@
text
@d3 1
a3 1
// RCS Stamp: $Id: IntelAddress.h,v 3.3 1994/08/01 20:12:47 buehler Exp berczuk $
d33 2
a34 2
        asegment= (UINT16)(address & 0xFF00) >>8;
        aoffset = (UINT16)(address & 0x00FF);
@


3.3
log
@Changed valid to validp to fix glitch with ocenter.
@
text
@d3 1
a3 1
// RCS Stamp: $Id: IntelAddress.h,v 3.2 1994/03/23 16:39:29 buehler Exp buehler $
d28 24
d63 1
a63 1
  // fault condition to IAF_OVERFLOW.
d65 3
@


3.2
log
@Added declaration of << operator.
@
text
@d3 1
a3 1
// RCS Stamp: $Id: IntelAddress.h,v 3.1 1994/03/23 15:54:54 buehler Exp buehler $
d97 1
a97 1
  BOOL isValid(void) const { return(valid);};
d117 1
a117 1
  BOOL valid;
@


3.1
log
@Added prefix to values in IntelAddressFault enum to avoid
conflict with 3rd party macro.
@
text
@d3 1
a3 1
// RCS Stamp: $Id: IntelAddress.h,v 3.0 1993/12/15 21:36:52 buehler Exp buehler $
d126 1
d129 1
@


3.0
log
@Build 3.0. No change.
@
text
@d3 1
a3 1
// RCS Stamp: $Id: IntelAddress.h,v 2.0 1993/11/29 01:37:46 buehler Exp buehler $
d25 3
a29 1
  enum IntelAddressFault { NONE, OVERFLOW, UNDERFLOW, OUT_OF_SEG};
d39 1
a39 1
  // fault condition to OVERFLOW.
d46 1
a46 1
  // will have "valid" flag off, and the OVERFLOW fault will be set.
d61 1
a61 1
  // May set OVERFLOW fault.
d66 1
a66 1
  // May set UNDERFLOW fault.
d91 1
a91 1
  // the segment base. If out of range, sets OUT_OF_SEG fault
@


2.0
log
@Standardized RCS tags in file header.
@
text
@d3 1
a3 1
// RCS Stamp: $Id$
@


1.1
log
@Remove warnings
@
text
@d2 4
a5 1
// Class IntelAddress
a6 20
// $Header: /cygx1/h1/buehler/EDS/inc/RCS/IntelAddress.h,v 1.0 1993/07/08 00:04:20 buehler Exp buehler $
// $Author: buehler $
// $Date: 1993/07/08 00:04:20 $
/*
 * $Log: IntelAddress.h,v $
 * Revision 1.0  1993/07/08  00:04:20  buehler
 * Build 1 version
 *   Eliminated auxiliary class LongForPackets
 *
 * Revision 0.2  1993/06/02  21:01:02  buehler
 * Uninline isSameAs and sharesSegWith tests
 *
 * Revision 0.1  1993/06/02  17:45:27  buehler
 * Added member asChar() to place address in CCSDS packet
 *
 * Revision 0.0  1993/06/02  15:37:54  buehler
 * Starter kit version
 *
 */

d16 1
@


1.0
log
@Build 1 version
  Eliminated auxiliary class LongForPackets
@
text
@d4 1
a4 1
// $Header: /cygx1/h1/buehler/eds/inc/RCS/IntelAddress.h,v 0.2 1993/06/02 21:01:02 buehler Exp buehler $
d6 1
a6 1
// $Date: 1993/06/02 21:01:02 $
d8 5
a12 1
// $Log: IntelAddress.h,v $
a40 10
class LongForPackets
{
public:
  UINT32 data;
  void asChars( char * dest) const;
  // Places 4 bytes in "dest" in the proper order for a CCSDS Packet
};

ostream& operator<< (ostream&, const LongForPackets);

d108 1
a108 5
  LongForPackets        asEmittable() const;
  // Return address as a longword in format expected in CCSDS packets:
  // Offset high byte, Offset low, Segment high, Segment low.

  void asChars( char * dest) const;
@


0.2
log
@Uninline isSameAs and sharesSegWith tests
@
text
@d1 1
d4 1
a4 1
// $Header: /cygx1/h1/buehler/eds/inc/RCS/IntelAddress.h,v 0.1 1993/06/02 17:45:27 buehler Exp buehler $
d6 1
a6 1
// $Date: 1993/06/02 17:45:27 $
d9 3
d31 1
a31 2
// Author: Royce Buehler
// Date: January 27, 1993
@


0.1
log
@Added member asChar() to place address in CCSDS packet
@
text
@d3 1
a3 1
// $Header: /cygx1/h1/buehler/eds/inc/RCS/IntelAddress.h,v 0.0 1993/06/02 15:37:54 buehler Exp buehler $
d5 1
a5 1
// $Date: 1993/06/02 15:37:54 $
d8 3
a146 25
inline BOOL
IntelAddress::isSameAs( const IntelAddress& otherAddr) const 
{
  if ( seg == otherAddr.seg && off == otherAddr.off)
  {
    return(YES);
  }
  return(NO);
}

inline BOOL
IntelAddress::sharesSegmentWith(const IntelAddress& otherAddr) const 
{
  if (seg == otherAddr.seg)
  {
    return(YES);
  }

  UINT32 offBy = *this - otherAddr;
  if ( offBy <= 0x0000FFFF)
  {
    return(YES);
  }
  return(NO);
}
@


0.0
log
@Starter kit version
@
text
@d3 9
a11 4
// $Header$
// $Author$
// $Date$
// $Log$
d35 2
d111 3
@

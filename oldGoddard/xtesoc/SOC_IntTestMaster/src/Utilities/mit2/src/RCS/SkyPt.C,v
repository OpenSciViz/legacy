head	3.3;
access
	soccm;
symbols
	Build4_3_1:3.3
	Build4_3:3.2
	OldSOCTime:3.2
	Build4_2:3.2
	Build4_1:3.2
	BUILD:3.3
	Build4:3.2
	Build_4:3.2
	Build3_3:3.2
	Build3_2:3.2
	Build3_1:1.4
	Build3:1.4;
locks; strict;
comment	@ * @;


3.3
date	95.05.04.19.49.51;	author berczuk;	state Submitted;
branches;
next	3.2;

3.2
date	94.07.21.19.40.29;	author buehler;	state Submitted;
branches;
next	3.1;

3.1
date	94.07.21.18.02.18;	author buehler;	state Exp;
branches;
next	1.5;

1.5
date	94.07.05.15.59.27;	author berczuk;	state Exp;
branches;
next	1.4;

1.4
date	94.01.27.16.22.34;	author berczuk;	state Exp;
branches;
next	1.3;

1.3
date	94.01.26.19.48.16;	author berczuk;	state Exp;
branches;
next	1.2;

1.2
date	94.01.26.15.40.51;	author berczuk;	state Exp;
branches;
next	1.1;

1.1
date	93.12.28.22.31.39;	author berczuk;	state Exp;
branches;
next	;


desc
@Class to describe a point in the sky
@


3.3
log
@changed PI to M_PI for solaris compatability.
@
text
@//   RCS Stamp: $Id: SkyPt.C,v 3.2 1994/07/21 19:40:29 buehler Submitted berczuk $ 
//   Description : Source file for the SkyPtClass
//   Author      : Steve Berczuk -berczuk@@mit.edu
//   Copyright   : Massachusetts Institute of Technology

#include <iostream.h>
#include <rw/matherr.h>
#include "SkyPt.h"


SkyPt::SkyPt()
{


}

SkyPt::SkyPt( XteAngle the_ra, XteAngle the_dec, System ):
rightAscension(the_ra), declination(the_dec)
{
  // we only handle J2000 currently
}
 
SkyPt::SkyPt(const DoubleVec& vec,System)
{
//   if(vec.length()!=3) // complain if this is not a 3-vector
//     RWThrow(RWErrObject (MATH_NPOINTS));
      // use the rogue wave defined errors

   double atanMag = atan2(vec[1],vec[0]);
   // magnitude of arctan in radians
   // atan2 returns an angle (t) in [-180 < t < 180]
//   cout << "mag: " <<atanMag *180/PI <<endl;
   
   if(atanMag < 0) // we want an angle(t) in the range [0<t<360)
    {
     atanMag += 2*M_PI;
   }

   rightAscension =XteAngle(atanMag); 
   // convert the mag in radians to an XteAngle;
     
  declination = XteAngle(asin(
			     vec[2]/
			      (sqrt(sum(pow(vec,DoubleVec("[2.0 2.0 2.0]")))))
			     )
			 );

}


  SkyPt::~SkyPt( )
{
}



  DoubleVec SkyPt:: cartesian( ) const
{
   DoubleVec xyz(3, 0); // xyz pointing vector
   xyz(0) = cos(ra())*cos(dec());
   xyz(1) = sin(ra())*cos(dec());
   xyz(2) = sin(dec());

   return xyz;
}


  XteAngle SkyPt::glat() const
{
  return XteAngle(0,0,0);


}
 
  XteAngle SkyPt::glon() const
{
  return XteAngle(0,0,0);
}

void SkyPt::operator=(const SkyPt& pt)
{

  this->rightAscension = pt.rightAscension;
  this->declination = pt.declination;
}

ostream& operator<<(ostream& os, const SkyPt& pt)
{
  os << "RA " <<pt.ra() << " DEC: " <<pt.dec(); 
  return os;
}
@


3.2
log
@Use non-obsolete constructor for xyz pointing vector.
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: SkyPt.C,v 3.1 1994/07/21 18:02:18 buehler Exp buehler $ 
d36 1
a36 1
     atanMag += 2*PI;
@


3.1
log
@Commented out RWThrow line - RWError no longer exists in RW 6.0.
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: SkyPt.C,v 1.5 1994/07/05 15:59:27 berczuk Exp buehler $ 
d25 1
a25 1
   if(vec.length()!=3) // complain if this is not a 3-vector
d59 1
a59 1
   DoubleVec xyz(3); // xyz pointing vector
@


1.5
log
@fixes
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: SkyPt.C,v 1.4 1994/01/27 16:22:34 berczuk Exp berczuk $ 
d26 1
a26 1
     RWThrow(RWErrObject (MATH_NPOINTS));
@


1.4
log
@Constructor that takes a 3-vector now calculates quadrant of angle
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: SkyPt.C,v 1.3 1994/01/26 19:48:16 berczuk Exp berczuk $ 
d29 4
a32 2
  rightAscension=XteAngle(atan(vec[1]/vec[0]));
   // ra is arctan (y/x)4
d34 8
a41 2
   if(vec[0] <0) // compensate for quadrant
     rightAscension =rightAscension-XteAngle(PI);
a46 1
   //dec is arctan z/sqrt(sum of squares of x,y,z)
@


1.3
log
@fixed constructor that takes a vector...
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: SkyPt.C,v 1.2 1994/01/26 15:40:51 berczuk Exp berczuk $ 
d32 2
@


1.2
log
@added ostream operator; added constructor that takes a 3-vec
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: SkyPt.C,v 1.1 1993/12/28 22:31:39 berczuk Exp berczuk $ 
d22 2
a23 1
  SkyPt::SkyPt(const DoubleVec& vec,System)
d25 1
a25 1
   if(vec.length()!=3)
d30 5
a34 2
  declination = XteAngle(tan(
			     vec[2]/(sum(pow(vec,DoubleVec("[2 2 2]"))))
d37 1
@


1.1
log
@Initial revision
@
text
@d1 1
a1 1
//   RCS Stamp: $Id$ 
d6 2
a7 1

d17 1
a17 1
SkyPt::SkyPt( XteAngle the_ra, XteAngle the_dec, System sys):
d22 5
d28 5
d34 3
d71 6
@

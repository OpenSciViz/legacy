head	1.2;
access;
symbols;
locks; strict;
comment	@// @;


1.2
date	98.06.05.21.22.56;	author jonv;	state Exp;
branches;
next	1.1;

1.1
date	98.02.19.21.46.44;	author jonv;	state Exp;
branches;
next	;


desc
@first version of page_maker.cc
@


1.2
log
@added -problemID option for flagging problems with a unique ID
@
text
@
//============================================================================
// File Name   : page_maker.cc
// Subsystem   : Paging
// Programmer  : Jon D. Vandegriff, Raytheon STX
// Description : Send a page from the command line
// RCS stamp:
static const char rcsid[]=
"$Id: page_maker.cc,v 1.1 1998/02/19 21:46:44 jonv Exp $";
//
// ===========================================================================

/* 
This program accepts a string on the command line and sends it to the
paging system. It is intended for use in scripts and other programs which 
need to call the paging system when a notification is desired.

Several important things about the paging system are relevant here:
1. each page has associated with it a priority, a subsystem ID and a message;
2. messages are routed to groups (which are defined in the configuration 
        file of the paging system) based on the subsystem ID associated 
        with the page;
3. the association of page groups with subsystem ID's is established
        in a table in the file $SOCHOME/etc/SOCPAGE.CONFIG
        This is the default file.  Another may be specified on the 
        command line if desired.  A full path name for this file may be
        given, or if just a file name is given, it is assumed that the
        file is in the $SOCHOME/etc/ directory;

This program is as forgiving as possible so that once it is called, the 
chances are very high that a page will be issued.  In general, if some 
information is not provided, a default will be used.  Also, if the 
a variable which is not critical is given an incorrect value 
(invalid priority, inaccessible configuration file, no value for 
$SOCHOME), then the program attempt to use defaults so that the page 
can be sent anyway.

Fatal errors include: no configuration file found.
 Unless it can read a configuration file, the program has no way of 
 confirming that the subsystem id name given on the command line is 
 valid. If it is invalid, then SOCPage will not be able to call the 
 telalert program properly.
 It could just blindly issue the page, but, then it would not know
 if it was valid or not.

One possible misuse could occur with this program.  Since you
can speficy a config filename on the command line, this file may
be different than the config file used by other paging programs
(such as socPage).  If the subsystem given on the command line
exists in the specified config file but not in the main config
file, then the page will probably get messed up.

Another problem could occur if for some reason, this program 
can't access the config file even though the subsystem ID 
it wants to use is fine.  Recall that even if $SOCHOME is not set, 
this program tries to look in /sochome/etc.  If that file system 
is not accessible to the program (and no other file name was given 
for the config file), then it will not issue a page.

Finally, the exit status of this program will tell you whether or not a 
page was issued:
exit status     meaning
     0           everything went fine
     1           no page sent
    11           page sent, some defaults had to be used

*/

#include <iostream.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

// Local Headers

//#include <Utilities.h>
#include <SOCMessage.h>


#include <SOCPage.h>

#include <socPageStatus.h>

#include <rw/hashdict.h>
#include <rw/collstr.h>
#include <rw/regexp.h>

const int PAGE_SENT_OK = 0;
const int NO_PAGE_SENT = 1;
const int PAGE_SENT_BUT_ALTERED = 11;
int exit_status;


const RWCString    default_sub_sys_label = "UNSPECIFIED";
// const SubSystemID  default_sub_sys_ID = UNSPECIFIED;
const PRIORITY     default_priority = MEDIUM;

RWCString          this_prog_name;
RWCString          calling_program;

RWHashDictionary   *PageSubSysHD;
RWCString          sub_sys_label;
// SubSystemID        sub_sys_ID;

RWCString          message_text;

const char         *OK_PRIORITY_REGULAR_EXPRESSION = "-*[0-4]";
const char         *default_priority_string = "2";
RWCString          priority_string;
PRIORITY           priority;

int                to_screen;

RWCString          sub_sys_table_filename;
RWCString          full_table_filename;
const char         *default_table_filename = "SOCPAGE.CONFIG";

RWCString          problem_id;
int                problem_id_set = 0;

int show_help = 0;

//
// An error message facility which does not use stderr, because MIT
// programs which call this will die if it writes to stderr...
// 

void my_message (const char *format, ...)
{
    static char buffer[3000]; // Should be large enough.
    va_list ap;
    va_start(ap, format);
    (void) vsprintf(buffer, format, ap);
    va_end(ap);

    //if (using_syslogd)
    //    syslog(LOG_INFO, buffer);
    //else
    //    (void) fprintf(stderr, "%s\n", buffer);
    (void) fprintf(stdout, "%s\n", buffer);
}




//
// Print out some detailed instructions and exit.
//
static void usage(void)
{
  cout
  << "\nusage: " << this_prog_name << endl
  << "              [-pname program_name]\n"
  << "              [-ssid sub_sys_ID_name]\n"
  << "              [-problemID unique_ID_for_this_type_of_problem]\n"
  << "              [-priority priority]\n"
  << "              [-ssfile filename_of_subsys_table]\n"
  << "              [-noecho]\n"
  << "              [-usage]\n"
  << "              [-help]\n"
  << "              -m message_string" << endl
  << endl;
	if (show_help) {
	cout
  << "program_name       -- the name of the program which has detected the\n"
  << "                      pageable offense; if none is provided, the name\n"
  << "                      of this program is used.\n"
  << "                      DEFAULT:" << this_prog_name << endl
  << endl
  << "sub_sys_ID_label   -- specifies what subsystem originated the page;\n"
  << "                      also determines which group is paged for\n"
  << "                      offenses from this subsystem; the correspondence\n"
  << "                      between subsystem and page group is defined in a\n"
  << "                      file, described below."
  << "                      the spelling of the subsystem name is important;\n"
  << "                      capitalization is done for you.\n"
  << "                      DEFAULT VALUE: UNSPECIFIED" << endl
  << endl
  << "unique_ID_for_this_type_of_problem -- each paging problem can be given a"
  << "                      unique ID so that while one page is on hold, any"
  << "                      other pages which arrive with this unique ID will"
  << "                      be considered duplicates." << endl
  << endl
  << "priority           -- the priority of the page\n"
  << "                      DEFAULT: MEDIUM (int value is 3)" << endl
  << endl
  << "filename_of_subsys_table -- name of file containing columns of data\n"
  << "                      describing the different paging subsystem IDs\n"
  << "                      and the group or groups to page for offenses\n"
  << "                      from each subsystem.  This file is expected to\n"
  << "                      be found in the directory $SOCHOME/etc/,\n"
  << "                      unless the filename gives the full path (i.e.,\n"
  << "                      the name starts with a \"/\")\n"
  << "                      DEFAULT: " << default_table_filename << endl
  << endl
  << "The -noecho option -- do not send messages to the screen" << endl
  << "                      DEFAULT: send message to screen" << endl
  << endl
  << "The -usage option  -- write explantion of the various options and\n"
  << "                      arguments to stdout and exit.\n"
  << "                      WITH THIS OPTION, NO PAGE IS SENT,\n"
  << "                      EVEN IF OTHER OPTIONS ARE GIVEN." << endl
  << endl
  << "The -help option   -- shows more explanation about the options. Also\n"
  << "                      writes to stdout and exits.\n"
  << "                      WITH THIS OPTION, NO PAGE IS SENT,\n"
  << "                      EVEN IF OTHER OPTIONS ARE GIVEN." << endl
  << endl
  << "message string     -- contents of the page. The subsystem name and the\n"
  << "                      priority will be prepended to the message text.\n"
  << "                      The date, time, machine name, and process name\n"
  << "                      will also be prepended by the syslog routine\n"
  << "                      which sends the message to the log host.\n"
  <<"                      A message is required and no default is provided.\n"
  << endl;
	}

  exit(exit_status);

}




int is_valid_priority( RWCString p_string, PRIORITY &p_enum)
{
	// This function only sets the p_enum if the p_string looks like
	// a valid priority.

  RWCRegexp priority_regexp(OK_PRIORITY_REGULAR_EXPRESSION);
  size_t length;
  if (priority_regexp.index(p_string, &length) != RW_NPOS ) {

		// This is definitely very non-optimal, but will work for now...
		switch ( atoi(p_string.data()) ) {
		case -1 : 
			p_enum = NEGATIVE;
			break;
		case  0 :
			// p_enum = NONE;
			    // the NONE priority was (temporarily) removed due to
			    // linking probs in other modules Subrata was using
			p_enum = LOW;
			break;
		case  1 :
			p_enum = LOW;
			break;
		case  2 :
			p_enum = MEDIUM;
			break;
		case  3 :
			p_enum = HIGH;
			break;
		case  4 :
			p_enum = SUPER;
			break;
		default:

			// just in case something goes awry

			p_enum = default_priority;

			my_message("page_maker: WARNING - priority %s unknown\n"
              "USING DEFAULT PRIORITY: %d", 
							p_string.data(), (int) p_enum );									
			exit_status = PAGE_SENT_BUT_ALTERED;

		} // end of switch statement

    return 1;  // yes, the priority is set

  } else {

    return 0;  // no, the priority was not set.
  }
}



void parse_arguments(char**& argv)
{

  // Set the defaults

  calling_program = this_prog_name;
  sub_sys_table_filename = default_table_filename;

  sub_sys_label = default_sub_sys_label;
//  sub_sys_ID = default_sub_sys_ID;
	priority_string = default_priority_string;
  priority = default_priority;
  to_screen = 1;

  message_text = "";
	int message_given = 0;

  exit_status = PAGE_SENT_OK;

  while (*++argv && **argv == '-') {

    if (strcmp(*argv, "-pname") == 0) {
      calling_program = *++argv;
    }
    else if(strcmp(*argv, "-ssid") == 0) {
      sub_sys_label = *++argv;
      sub_sys_label.toUpper();
    }
    else if(strcmp(*argv, "-problemID") == 0) {
      problem_id = *++argv;
      problem_id_set = 1;
    }
    else if (strcmp(*argv, "-priority") == 0) {
      priority_string = *++argv;
    }
    else if (strcmp(*argv, "-ssfile") == 0) {
      sub_sys_table_filename = *++argv;
    }
    else if(strcmp(*argv, "-noecho") == 0) {
      to_screen = 0;
    }
    else if (strcmp(*argv,"-usage") == 0) {
			exit_status = NO_PAGE_SENT;
			usage();
    }
    else if (strcmp(*argv,"-help") == 0) {
			exit_status = NO_PAGE_SENT;
      show_help = 1;
			usage();
    }
    else if (strcmp(*argv,"-m") == 0) {
			message_given = 1;
      message_text = *++argv;
    }
    else {
      my_message ("Unknown option: %s\n", *argv);
			exit_status = NO_PAGE_SENT;
      usage();
    }

  }

	if (!message_given) {
		my_message("page_maker: ERROR - message text is required\n");
		exit_status = NO_PAGE_SENT;
		usage();
	}

	if (message_text[0] == '-') {
		my_message("page_maker: WARNING - the message begins with '-'\n"
						"            Is an argument being used for the message?\n");
	}


	// The function which checks the priority also sets the priority (which 
  // it gets it by reference), but only if it thinks the priority is a 
	// valid one.

	if (! is_valid_priority(priority_string, priority) ) {
		my_message("page_maker: WARNING - the value '%s' is not a valid priority\n"
						"USING DEFAULT PRIORITY %d",
						priority_string.data(), (int) priority );
		exit_status = PAGE_SENT_BUT_ALTERED;
	}


  // if the filename starts with a full slash, then just use it as is,
  // but otherwise prepend the directory name given by $SOCHOME/etc
  if ( sub_sys_table_filename[0] == '/' ) {
    full_table_filename = sub_sys_table_filename;
  } else {
    char *env_var;
    if ((env_var=getenv("SOCHOME")) == NULL) {

      full_table_filename = "/sochome/etc/";
      full_table_filename = full_table_filename + default_table_filename;

      char *str1="page_maker: WARNING - no setting for environment "
				         "variable SOCHOME\n";
      char *str2="            AND full path for config file not given\n";
      char *str3="(you must provide at least one of the above)\n";
      char *str4="Trying to use default configuration file: ";
      my_message("%s%s%s%s%s",str1,str2,str3,str4,full_table_filename.data());

      exit_status = PAGE_SENT_BUT_ALTERED;

    } else {
      RWCString sochome_env(env_var);
      full_table_filename = 
        sochome_env + "/etc/" + sub_sys_table_filename;
    }
  }


	// Next we want to verfity that the ssid being used is a valid one.
	// To do this, we MUST have access to a config file.
	// If the hash dictionary can't be made form the config file, exit
	// If the current subsystem ID can't be found in the HD then
	//     if the current sus system is the default one then
	//        exit
	//      else
	//         try to use the default sub sys name
	//         if it is not in the HD, exit
	//      endif
  // endif



  if (  ( PageSubSysHD = 
           makeSubSysDictionary(full_table_filename.data()) 
        ) == NULL  
     )
   {
		// This is a fatal error.  If the page is going to go out,
		// then there needs to be a line in the config file for the
		// given sub system.  In order to verify this, the config
		// file MUST be accessible.

    my_message( "page_maker: ERROR - Cannot load the paging subsystem\n"
						 "configuration file %s\n",
             full_table_filename.data() );
    exit_status = NO_PAGE_SENT;
		usage();

	 }

	// Make sure that the subsystem string provided 
	// exists in the config file.  If it is not there, 
	// then try to use the default subsystem string.  
	// If that is not present, then exit.

	RWCollectableString sub_sys_key(sub_sys_label);  
	                                               // the hash dictionary 
                                                 // requires a key to be of
                                                 // type RWCollectable
	socPageInfo *config_file_line;
	config_file_line = 
		(socPageInfo *) PageSubSysHD->findValue(&sub_sys_key);

	if ( config_file_line == NULL ) {

		if (sub_sys_label == default_sub_sys_label) {

			my_message("page_maker: ERROR - "
							"The default paging subsystem %s\n"
							"is not listed in the file %s\n",
							sub_sys_label.data(), full_table_filename.data() );

			exit_status = NO_PAGE_SENT;
			usage();

		} else {

			my_message("page_maker: WARNING - "
							"The paging subsystem %s\n"
							"is not listed in the file %s\n"
							"Attempting to use the default subsystem name %s\n",
							sub_sys_label.data(), full_table_filename.data(),
							default_sub_sys_label.data() );

			sub_sys_label = default_sub_sys_label;
			sub_sys_key   = default_sub_sys_label;

			config_file_line = 
				(socPageInfo *) PageSubSysHD->findValue(&sub_sys_key);
				
			if ( config_file_line == NULL ) {
				my_message("page_maker: ERROR - "
								"The default paging subsystem %s\n"
								"is not listed in the file %s\n",
								sub_sys_label.data(), full_table_filename.data() );
				exit_status = NO_PAGE_SENT;
				usage();
			}


		} // close of if stmt checking if the first ssid searched for was the
				  // default


	}  // close of initial if statement looking for non-NULL config file line

}


// This program uses a string name from the command line (or
// else a default string) for the paging subsystem
// identification.  Once verifying that the string is indeed in
// the config file, the page is issued by a call to SOCThrow
// itself, rather than SOCPage.  The reason for this is that
// SOCPage requires the subsystem id with which it is declared
// to be an existing value from an enumeration type.  It then
// uses this enumeration value to look up a string name for that
// subsystem in an array of strings which is hardcoded into the
// header file for the base page class.  If a subsystem is added
// to the file but not to the enumeration, the string lookup for
// that enumeration will cause problems, because there is no
// string associated with that subsystem id number.
// 
// The problem with this approach is that I need to duplicate
// exactly the format used by the SOCPage class for writing a
// page.  Fortunately, this is not too hard.  But now the 
// required format is in two places.


main(int, char** argv)
{
  this_prog_name = argv[0] ;

  parse_arguments(argv) ;

  // Assign process name
  SOCMessageManager::issuedBy(calling_program) ;

  // Turn on screen logging
  SOCMessageManager::logToScreen(to_screen) ;

  // Turn off exit on error
  SOCMessageManager::exitOnError(0) ;

  // All pages are supposed to come with an identifier which
  // is unique for the specific problem which is occurring.
  // If no identifier is given, then the message itself is used
  // as the identifier.

  // We can't call SOCPage.Throw(...) directly, so we exactly emulate its
  // call to the SOCThrow function.

  if (problem_id_set == 0) {
    // no problem id set on command line; use the message
    SOCThrow(SOC_PAGE, "%s %d %s %s %s",
     sub_sys_label.data(), (int) priority, message_text.data(),
     ID_INDICATOR, message_text.data() );
  } else {
    // use the problem id given on command line
    SOCThrow(SOC_PAGE, "%s %d %s %s %s",
     sub_sys_label.data(), (int) priority, message_text.data(),
     ID_INDICATOR, problem_id.data() );
  }
	// These calls to SOCThrow are the same as the one used in 
  // in SOCPage to issue a page.


  exit(exit_status) ;
}
@


1.1
log
@Initial revision
@
text
@d9 1
a9 1
"$Id$";
d72 1
d76 1
a76 1
#include <Utilities.h>
a78 2
// This occurs first since SOCPage uses it, but does not include it!!
#include <rw/cstring.h>  
d118 3
d124 23
d151 1
a151 1
  cerr
d155 1
d164 1
a164 1
	cerr
d179 5
d200 1
a200 1
  << "                      arguments to stderr and exit.\n"
d205 1
a205 1
  << "                      writes to stderr and exits.\n"
d263 1
a263 1
			message("page_maker: WARNING - priority %s unknown\n"
d308 4
d335 1
a335 1
      message ("Unknown option: %s\n", *argv);
d343 1
a343 1
		message("page_maker: ERROR - message text is required\n");
d349 1
a349 1
		message("page_maker: WARNING - the message begins with '-'\n"
d359 1
a359 1
		message("page_maker: WARNING - the value '%s' is not a valid priority\n"
d382 1
a382 1
      message("%s%s%s%s%s",str1,str2,str3,str4,full_table_filename.data());
d418 1
a418 1
    message( "page_maker: ERROR - Cannot load the paging subsystem\n"
d443 1
a443 1
			message("page_maker: ERROR - "
d453 1
a453 1
			message("page_maker: WARNING - "
d467 1
a467 1
				message("page_maker: ERROR - "
a481 1

d485 18
a504 22

/*
This program uses a string name from the command line (or
else a default string) for the paging subsystem
identification.  Once verifying that the string is indeed in
the config file, the page is issued by a call to SOCThrow
itself, rather than SOCPage.  The reason for this is that
SOCPage requires the subsystem id with which it is declared
to be an existing value from an enumeration type.  It then
uses this enumeration value to look up a string name for that
subsystem in an array of strings which is hardcoded into the
header file for the base page class.  If a subsystem is added
to the file but not to the enumeration, the string lookup for
that enumeration will cause problems, because there is no
string associated with that subsystem id number.

The problem with this approach is that I need to duplicate
exactly the format used by the SOCPage class for writing a
page.  Fortunately, this is not too hard.  But now the 
required format is in two places.
*/

d522 2
a523 6
  // Since almost anything can call this program, the id is 
  // just the message itself.

  SOCThrow(SOC_PAGE, "%s %d %s %s %s",
   sub_sys_label.data(), (int) priority, message_text.data(), ID_INDICATOR, 
   message_text.data() );
d528 13
a540 3
//SOCThrow(SOC_PAGE, "%s %d %s %s %s",
//         SubSystemStrings[subSystem], pry, buf, ID_INDICATOR, pid.data());
	// This is the call used in SOCPage to issue a page.
a541 2
  // SOCPageObject AnyPage(sub_sys_ID);
  // AnyPage.Throw( priority, message_text.data() );
@

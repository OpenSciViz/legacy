############################################################################
#
# File Name   : Makefile
# Subsystem   : Data Ingest GUI
# Programmer  : Randall D. Barnette, Hughes STX
#             : Jon Vandegriff, Raytheon STX
# Description :
#
#   Makefile for the Paging Subsystem
#
# Required ENVIRONMENT variables:
#   SOCHOME           -- where integration copies reside
#   SOCOPS            -- where integration data resides
#   SOCLOCAL          -- where integration 3rd party s/w resides
#   ARCH              -- designation of machine architecture and OS
#   SOCMASTER         -- if defined, Read0only source of SOC s/w
#
# Targets:
#   pubhdrs           -- Install public header files into RCS
#   publibs           -- Create and install libs into RCS
#   install           -- Install executables into RCS
#   depend            -- Create Include file dependency list
#   clean             -- Remove *.o and *.a files
#
# RCS: $Id$
#
############################################################################

ifndef CCROOT_I
  ifeq ($(ARCH),SunOS4)
    CCROOT_I = /socsoft/CenterLine/clc++/sparc-sunos4/incl
  endif
  ifeq ($(ARCH),SunOS5)
    CCROOT_I = /socsoft/CenterLine/clc++/sparc-solaris2/incl
  endif
endif

#
# The subdirectories to traverse.
#
DIRS=libpage socPage pageResponse
# etc inc ingest-win lib man pktgen pringest rtclient rtingest rtserver test

all:pubhdrs depend publibs install install-test

pubhdrs publibs install clean uninstall install-test:
	@$(foreach dir,$(DIRS),$(MAKE) -C $(dir) $@;)

realclean:
	@$(foreach dir,$(DIRS),$(MAKE) -C $(dir) $@;)
	-rcsclean

depend:
	@$(foreach dir,$(DIRS),$(MAKE) -C $(dir) CCROOT_I=$(CCROOT_I) $@;)

current:
	rcs -NCurrent: RCS/*,v
	@$(foreach dir,$(DIRS),rcs -NCurrent: $(dir)/RCS/*,v $@;)

tar: realclean
	tar cf ingest.tar RCS $(DIRS)
	compress -f ingest.tar

relabel:
	rcs -N$(BUILD): RCS/*
	@$(foreach dir,$(DIRS),rcs -N$(BUILD): $(dir)/RCS/*,v;)

.PHONY: pubhdrs depend publibs install clean realclean unistall install-test

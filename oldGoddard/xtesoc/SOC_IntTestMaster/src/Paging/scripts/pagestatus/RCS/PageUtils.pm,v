head	1.7;
access;
symbols;
locks; strict;
comment	@# @;


1.7
date	99.08.18.18.02.31;	author jonv;	state Exp;
branches;
next	1.6;

1.6
date	98.09.04.01.38.22;	author jonv;	state Exp;
branches;
next	1.5;

1.5
date	98.07.15.18.15.57;	author jonv;	state Exp;
branches;
next	1.4;

1.4
date	98.07.13.20.57.56;	author jonv;	state Exp;
branches;
next	1.3;

1.3
date	98.07.13.19.50.22;	author jonv;	state Exp;
branches;
next	1.2;

1.2
date	98.07.13.15.11.21;	author jonv;	state Exp;
branches;
next	1.1;

1.1
date	98.06.08.18.39.08;	author jonv;	state Exp;
branches;
next	;


desc
@common funcions used in several paging system programs
@


1.7
log
@changed rsh to use the correct version on Solaris 2.6 machines in the SOF
@
text
@# ===========================================================================
# Filename    : PageUtils.pm
# Subsystem   : Paging
# Programmer  : Jon Vandegriff, Raytheon STX
# Description : functions for use in many paging system components
#
# RCS: $Id: PageUtils.pm,v 1.6 1998/09/04 01:38:22 jonv Exp jonv $
# ===========================================================================



package PageUtils;

use Time::Local;

use Exporter();
@@ISA = qw(Exporter);
@@EXPORT = qw();  # do not export ANY symbols;

# ALL SUBROUTINES IN THIS MODULE MUST BE CALLED BY THEIR FULLY QUALIFIED NAMES

use strict;

sub pause_till_enter_with_message {
  my $message = shift;
  if (!defined($message)) {$message = "Press return to go on.\n";}
  print $message;
  my $wait_for_return = <STDIN>; 
}


sub page_through_lines {
  my $N = shift;    #max lines per screen
  my $lines_shown_this_screen = 0;

  foreach (@@_) {
    print $_,"\n";
    ++$lines_shown_this_screen;
    if ($lines_shown_this_screen == $N) {
      &pause_till_enter_with_message
          ("\n --- Press ENTER to show the next page. ---\n");
      $lines_shown_this_screen = 0;
    }
  }
  return;
}





sub get_crontab_lines_from_machine {

  my( $machine_name ) = shift;      # 1st arg is name of machine to look on
  my( $required_username ) = shift; # the subroutine checks that you are 
                                    #    the right user

  my( @@cron_lines ) = ();



  if ( !&is_user_correct( $required_username ) ) {
    print "PageUtils::get_crontab_lines_from_machine:\n".
          "This script is not being run as user $required_username\n";
    $cron_lines[0] = -1;

  } else {

    # Make sure that we have rsh access to the specified machine
    # if we don't, the open will fail, an error message will print,
    # and the subroutine will return

    # just see if the machine is accessible by a ping
    # this is not very thorough, but should provide at least a simple test
    # as to wether or not to proceed with trying to get the crontab contents

    my( $this_machine ) = `/usr/ucb/hostname`;
    chomp( $this_machine );

    my $ping_result;

    my( $get_crontab_command );

    if ( "$this_machine" eq "$machine_name" ) {
      # if we are alreay on the machine whose crontab file is desired,
      # then we don't need to rsh;  I think that would cause an error 
      # to try to get rsh acess to the local machine

      $get_crontab_command = "crontab -l |";  
      $ping_result = 1;  # we have easy access to the local machine
    } else {
      $get_crontab_command = "/usr/original/etc/rsh -n $machine_name crontab -l |";
      $ping_result = &ping_test($machine_name);
    }

    if ( !$ping_result) {

      # could not even ping the machine, so lets not try a rsh,
      # which can take several minutes to time out...
      $cron_lines[0] = -1;

    } else {
      my $got_crontab = open( CRONLIST, "$get_crontab_command");
      if (!$got_crontab) {
          $cron_lines[0] = -1;
          # the calling routine already prints out an error message
          # print "    Can't open $get_crontab_command on $machine_name: $!";
      } else {

	  @@cron_lines = <CRONLIST>;
          chomp( @@cron_lines );

          $got_crontab = close( CRONLIST );
          if (!$got_crontab) {
            $cron_lines[0] = -1;
            print "Can't close connection to crontab -l on $machine_name: $!";
          }

          if (scalar(@@cron_lines) == 0) {
            $cron_lines[0] = -1;
          }

      }  # open of crontab command

    }

  } # user is required user

  return @@cron_lines;  # first element is -1 if there were problems;

}






sub is_user_correct {
  my( $required_user ) = $_[0];

  my( $who_output ) = `whoami`;  # get username from OS
  chomp( $who_output ); # remove pesky newline
 
  # username is first white space delimited field of output from whoami
  my( $actual_username ) = (split(/\s+/,$who_output))[0];

  if ( "$required_user" eq "$actual_username" ) {
    return 1;  # return true
  } else {
    return 0; # return false
  }
}




sub ping_test {
  my $machine_name = shift;
  my $timeout = shift;

  # This subroutine can test if you have access to a certain machine.
  # It will not tell you if you can rsh to it, but it will tell you
  # if that macine is alive. Also you can specify the time interval
  # to wait, unlike rsh, which can take minutes to time out if 
  # unsuccessful.

  if (!defined($timeout)) {$timeout = 7;}  # default timeout of 7 seconds

  my $ping_ans = `/usr/sbin/ping $machine_name $timeout`;  

  chomp( $ping_ans );

  if ($ping_ans =~ /^$machine_name.*is alive$/) {
    return 1;
  } else {
    return 0;
  }
}



sub get_lines_from_file {
  my $filename = shift;
  my $reporting_mode = shift;    # can be "interactive" or "silent";
                                 # default is "interactive"

  if (!defined($reporting_mode)) { $reporting_mode = "interactive"; }

  # This routine simply reads in a file with some error checking.
  # If the the mode is "interactive", then errors are reported.
  # If the file can be opened, then any lines extracted are returned,
  # regardless of whether there were errors closing the file or not.
  # If the file can't be opened, then an empty list is returned.

  my @@file_lines = ();
  my $OK = open(FILE, $filename);
  if ($OK) {
     @@file_lines = <FILE>;
     chomp( @@file_lines );
     $OK = close( FILE );
     if (!$OK) {
        # file could not be closed
        if ($reporting_mode eq "interactive") {
         print "Could not close file $filename: $!";
         print "\n";
        }
     }
  } else {
     # file could not be opened
     if ($reporting_mode eq "interactive") {
        print "Could not open file $filename: $!";
        print "\n";
     }
  }

  return @@file_lines;
}







sub change_internal_time_to_human_format {

  my $time_in_sec = shift;

  my( $sec, $min, $hours, $mday, $mon, $year ) = localtime( $time_in_sec );

  $year += 1900;  # Perl years start at 1900
  $mon  +=1;      # Perl months start at 0

                           #  yr  mo  day  hr   min  sec
  my $time_string = sprintf "%4d:%02d:%02d:%02d:%02d:%02d",
                           $year, $mon, $mday, $hours, $min, $sec;

  return $time_string;

}







sub change_human_format_to_internal_time {

  my( $human_formatted_time ) = shift;

  # this routine will return an internal time representation
  # (i.e., num. sec since Jan. 1, 1970)
  # given a human formatted time in YYYY:MM:DD:HH:MM:SS

  my( $internal_time ) = 0;  # default time returned if there are errors


  # Policy change:
  # the person who uses this routine should first check it the time
  # sent here is OK.  The verify_time_format routine is also provided 
  # in this module.
  # if (! &verify_time_format($human_formatted_time) ) {
  #  return $internal_time;  # it is 0 still
  # }

  my( @@elements ) = split( /:/, $human_formatted_time );

  my( $year, $month_num, $month_day, $hour, $min, $sec ) = @@elements;

  # The verification of all these elements happens when the command 
  # line parameter is first read in, so they do not have to be 
  # verified again.  The Verify_chk_SOF module does this checking.

  # conversion routine requires a year number relative to 1900
  $year -= 1900;

  # decrease month number to match Unix counting scheme
  # in which Jan is month 0
  --$month_num;

  $internal_time = 
       timelocal($sec, $min, $hour, $month_day, $month_num, $year);

  # this function sets the internal time to the num of sec since Jan 1, 1970
  # or to -1 if there is an overflow (which occurs past 2038 or so)

  if ($internal_time == -1) {
    print "This time does not appear to be a valid time: ".
          "$human_formatted_time";
    $internal_time = 0;
  }

  return $internal_time;
 
}



sub verify_time_format {
  my $verb = shift;  # wether or not to print out error messages for problems
  my $human_formatted_time = shift;

  my( @@elements ) = split( /:/, $human_formatted_time );

  # example of expected format is 1998:03:12:22:59:00
  # Note that the leading zeroes do not need to be provided
  # for month, day, hour, or minute fields.
  # The date must be a 4 digit date.

  # Checks are made on the data to insure that it will pass thorugh the 
  # timelocal function with no errors.  That function simply exits 
  # (AAARRRGGGHH!) if it finds a problem.  Fortunately, it is not too 
  # smart in figuring out the number of days in a month.  If you give 
  # it Feb 30, it assumes you are talking about Mar 2.  Thus it only 
  # checks that the month number is from 1 to 31.
  #
  # Since I don't want to exit on an error, I need to make sure that all
  # the conditions for conversion are properly met.
  # if they are not, I simply use a default time of 0;

  # If the string provided has fewer than 6 componenets, 
  # that is an error, because I want to be very specific about when the
  # scripts would be turned on.  Otehrwise, if the day is left out,
  # then the script could be off every day until the given time!!!

  if ( scalar(@@elements) != 6 ) {

    show_time_error_message(
         "Invalid number of components in the user\n".
         "  specified time \"$human_formatted_time\"" ) if $verb;
    return 0;

  }

  my( $year, $month_num, $month_day, $hour, $min, $sec ) = @@elements;

  unless ( ($year =~ /^\d{4}$/)  and 
           ($year>1970 and $year<2038) )  {
    show_time_error_message(
         "Invalid year $year in $human_formatted_time" ) if $verb;
    return 0;
  }

  unless ( ($month_num =~ /^\d{1,2}$/)  and 
           ($month_num>=1 and $month_num<=12)  ) {
    show_time_error_message(
      "Invalid month number $month_num in $human_formatted_time" ) if $verb;
    return 0;
  }

  unless ( ($month_day =~ /^\d{1,2}$/)  and 
           ($month_day>=1 and $month_day <=31)  ) {
    show_time_error_message(
      "Invalid day number $month_day in $human_formatted_time" ) if $verb;
    return 0;
  }

  unless ( ($hour =~ /^\d{1,2}$/)  and
           ($hour>=0 and $hour <=23)  ) {
    show_time_error_message(
      "Invalid hour number $hour in $human_formatted_time" ) if $verb;
    return 0;
  }

  unless ( ($min =~ /^\d{1,2}$/)  and
           ($min>=0 and $min<=59)  ) {
    show_time_error_message(
      "Invalid minute number $min in $human_formatted_time" ) if $verb;
    return 0;
  }

  unless ( ($sec =~ /^\d{1,2}$/)  and
           ($sec>=0 and $sec<=59)  ) {
    show_time_error_message(
      "Invalid seconds value $sec in $human_formatted_time" ) if $verb;
    return 0;
  }

  return 1;
}


sub show_time_error_message {
  my( $msg ) = shift;

  print "$0: WARNING in verify_time_format_format:\n";
  print "  ", $msg,"\n";
  print "  Expected format for the time is YYYY:MM:DD:HH:MM:SS\n";
  print "  Each letter represents a single digit. ".
        "Leading zeros not necessary.\n";
  print "  All elements must be present!!\n";
  print "  The time will be set to zero instead.\n\n";

  return;
}








sub parse_line_with_quotes {

  my $input_line = shift;

  my @@words = ();  # returns a list of words

  # Find all words in a line preserving quoted strings as a single word.

  # The approach used is the following.  If the first character of the 
  # current word is a quote, then the current word is a quoted one -  
  # find the next quote and included everything between the quotes as 
  # the current word.
  # If the first char is not a quote, then just go to the next space 
  # and include everything up to just before the space.

  my( $boundary_char, $boundary_spot, $current_word );

  # trim off leading and trailing spaces
  $input_line =~ /^\s*(.*)\s*$/;
  my $line = $1;

  my $current_word_pos = 0;

  while ($current_word_pos < length($line)) {

    # see if the next word is delimited by a space or by double quotes
    if ( substr($line,$current_word_pos,1) eq "\"") {
      # other quoting characters could be added here...
      $boundary_char = "\"";
    } else {
      $boundary_char = " ";
    }
# 888
#print "boundary char :$boundary_char:\n";

# RATS! "index" does not work if you ask it to find a space!!!
#    my $boundary_spot = index($line, $boundary_char, $current_word_pos+1);

    # find the end of the current word by finding the next boundary character
    my $boundary_spot = $current_word_pos+1;
    while ($boundary_spot < length($line) ) {
      if ( substr($line,$boundary_spot,1) eq $boundary_char ) {
        # you could ask it to check if this character was backquoted and 
        # if so, skip it...
        last;
      }
      ++$boundary_spot;
    }
    if ($boundary_spot == length($line)) {
      if ($boundary_char ne " ") {
        # no closing quote was found - consider this a parse error
        # (if the space was the delimiter, there will not be a space
        # after the last word, so not finding a space at the end is OK)
        print "$0: ERROR parsing config file.\n".
              "Quotes unmatched on line $line\n";
        exit 1;
      }
    }

    if ($boundary_char ne " ") {
      # boundary character is a quote mark
      # the start of the current word should begin right AFTER the quote mark
      # (do not include the quote mark as part of the word)
      $current_word_pos++;  
    }

    # get the current word based on the current position and the 
    # position of the next boundary character
    $current_word = 
      substr($line, $current_word_pos, $boundary_spot - $current_word_pos);
# 888
#print "current word:$current_word:\n";

    push( @@words, $current_word );

    if ($boundary_char ne " ") {
      $boundary_spot++;  # If the boundary was at a quote, put the marker 
                         # on the character past the quote, which should 
                         # be a space
    }


    # begin thinking about the next word - reassign the current word 
    # position to the spot after the word we just found
    $current_word_pos = $boundary_spot;  # this is now at the character AFTER 
                                         # the last word, which should always 
                                         # be a space (unles we are done with
                                         # the line

    if ($current_word_pos < (length($line)) ){

      # if we get in here, then there are still more words to be found

      # first check that the current delimiter is a blank;
      # this will always be true for words whose boundary char was a space, 
      # but if the boundary char was a quote, the character following the
      # quote must also be a space
      if (!(substr($line,$current_word_pos,1) =~ /\s+/)) {
         print "$0: ERROR: no space after quote in config file line $line\n";
         exit 1;
      }

      # find start of next word
#      ++$current_word_pos;
      while ( substr($line, $current_word_pos, 1) =~ /\s+/ ) {
        if( $current_word_pos == length($line) ) {
           # no other non white space found - I don't think this can happen
           print "$0: ERROR parsing line\n$line\nin config file\n".
           "looking for more non-white space characters after $current_word";
           exit;
	}
        $current_word_pos++;
      }

    } # end check for current pos past end of line

  } # end while loop checking for pointer going past the end

  return @@words;

}






sub set_up_chk_SOF_environment {

# for testing purposes, first clear out old environment
#foreach $envkey (keys %ENV) {
#  print "Old ENV=> $envkey $ENV{$envkey}\n";
#  delete $ENV{$envkey};
#}

#
# SOC environment variables taken from SOCOPS .cshrc file on 3/24/1998
#

$ENV{"OPENWINHOME"}     = "/usr/openwin";
$ENV{"OCHOME"}          = "/socsoft/CenterLine";
$ENV{"PURIFYHOME"}      = "/socsoft/pure/purify-3.0-solaris2";
$ENV{"SOCHOME"}         = "/sochome";
#
# for old SOF locations of ASM processing
# $ENV{"POSHISTOGRAM_DB"} = "/asm/data";
$ENV{"POSHISTOGRAM_DB"} = "/xte/dwobj";
# 
# for old SOF locations of ASM processing
# $ENV{"PWD"}             = "/asm/data/spool";
$ENV{"PWD"}             = "/xte/socops/asm/spool";
$ENV{"SOCLOCAL"}        = "/socsoft/local";
$ENV{"SOCOPS"}          = "/socops";
$ENV{"SOCLOG"}          = "$ENV{SOCOPS}/log";
$ENV{"SOCFITS"}         = "$ENV{SOCOPS}/FITS";

my $archname = `/bin/uname`;
chomp($archname);
my $archnum  = `/bin/uname -r | cut -c1`;
chomp($archnum);
$archname = "$archname"."$archnum";

$ENV{"ARCH"} = "$archname";

 
$ENV{"XFILESEARCHPATH"} = "$ENV{SOCHOME}".'/lib/%T/%N%S:'."$ENV{OPENWINHOME}".'/lib/locale/%L/%T/%N%S:'."$ENV{OPENWINHOME}".'/lib/%T/%N%S';
#XUSERFILESEARCHPATH ./%N%S
#
 
#XANADU /opt/xanadu
#
# Path definition
#

my( $path_items ) =  
"/home/socops/bin:".
"/asm/bin:".
"/usr/local/bin:".
"/bin:".
"/usr/sbin:".
"/usr/ucb:".
"/home/socops/scripts:".
"/usr/ccs/bin:".
"/socsoft/langs.20:".
"/home/socops/java/bin:".
"$ENV{OPENWINHOME}/bin/xview:".
"/usr/bin:".
"$ENV{SOCHOME}/qfbin/$ENV{ARCH}:".
"$ENV{SOCHOME}/bin/$ENV{ARCH}:".
"/usr/bin/X11:".
"$ENV{OPENWINHOME}/bin:".
"/socsoft/local/bin:".
"/socsoft/local/lib/lw:".
".:".
"$ENV{SOCHOME}/bin:".
"$ENV{SOCHOME}/sparc-solaris2/bin:".
"/opt/SUNWspro/bin:".
"/home/socops/start";

# this path differs from the full SOCOPS path
# the following items were removed:
###############################################################
# /home/ehm/bin /opt/src/frame/bin      WAS AFTER /socsoft/langs.20
# /opt/hotjava/bin                      WAS AFTER /home/socops/java/bin
# /opt/.ftools/ftools_4.0/bin.host      WAS AFTER $OPENWINHOME/bin
# $XANADU/$EXT/bin /opt/gnu/bin /home/socops/SatTrack/run AFTER /opt/SUNWspro/bin
# /home/corbet/bin /test1/xtetest/RTM/bin   AFTER /home/socops/start

$ENV{"PATH"} = $path_items;



$ENV{"LD_LIBRARY_PATH"} = "$ENV{SOCHOME}/lib/$ENV{ARCH}:/usr/lib:/usr/ccs/lib:$ENV{OCHOME}/clc++/sparc-solaris2:$ENV{OCHOME}/clcc/sparc-solaris2:/socsoft/local/lib:/usr/local/lib:/opt/gnu/lib:/socsoft/motif12/usr/lib:/usr/openwin/lib:/usr/ucblib:/home/xsystem:/opt/SUNWspro/SC3.0.1/lib:/socsoft/CenterLine/clc++/sparc-solaris2/a1:/home/socops/bin/lib/";

# left out: final ${XANADU}/sol/lib

# can't use $verb here, because there has been no chance to set it yet!!
#print "using the following PATH\n$ENV{PATH}\n"


}




1;



@


1.6
log
@small changes to remove compiler warnings which came
up after moving to Perl5.00404 (mostly duplicate declarations
of variables which were somehow not detected in the previous
version of Perl - these duplicate declarations had no effect
on the operation of the code, fortunately!)
@
text
@d7 1
a7 1
# RCS: $Id: PageUtils.pm,v 1.5 1998/07/15 18:15:57 jonv Exp $
d92 1
a92 1
      $get_crontab_command = "rsh $machine_name crontab -l |";
@


1.5
log
@changed `who am i` to `whoami`
@
text
@d7 1
a7 1
# RCS: $Id: PageUtils.pm,v 1.4 1998/07/13 20:57:56 jonv Exp $
d282 1
a282 1
  my( $internal_time)= 
d294 1
a294 1
  return $ internal_time;
@


1.4
log
@calmed the chatter for empty crontabs
@
text
@d7 1
a7 1
# RCS: $Id: PageUtils.pm,v 1.3 1998/07/13 19:50:22 jonv Exp $
d141 1
a141 1
  my( $who_output ) = `who am i`;  # get username from OS
d144 1
a144 1
  # username is first white space delimited field of output from who am i
d324 1
a324 1
  # scripts whould be turned on.  Otehrwise, if the day is left out,
@


1.3
log
@added verify_time_format (which was in Verify_chk_SOF)
@
text
@d7 1
a7 1
# RCS: $Id: PageUtils.pm,v 1.2 1998/07/13 15:11:21 jonv Exp $
d106 2
a107 1
          print "Can't open $get_crontab_command on $machine_name: $!";
@


1.2
log
@added more stuff to this common area
@
text
@d7 1
a7 1
# RCS: $Id: PageUtils.pm,v 1.1 1998/06/08 18:39:08 jonv Exp jonv $
d258 4
a261 1

d296 102
@


1.1
log
@Initial revision
@
text
@d7 1
a7 1
# RCS: $Id$
d14 2
d22 1
a22 17

sub get_full_list_of_SOF_machines {


# 777
  my( @@full_machine_list ) = qw( xingest1 xdata xrtserv xsystem
                                 xhns1 xhns2 xhns3 
                                 xmm1 xmm2 
                                 xsm1 xsm2 xsm3 xsm4 xsm5 
                                 xtest xray xprod xql1 xdev  );
# for testing purposes, this can be set to CommI machines:
#  my( @@full_machine_list ) = qw( xpert xyster );

  return @@full_machine_list;

}

d55 3
a57 2
  my( $required_username ) = shift; # 2nd arg is optional username;
                                    # default value is socops
a59 2
  # the subroutine checks that you are the right user
  if ( !defined($required_username) ) { $required_username = "socops"; }
d61 4
a64 3
  if ( &is_user_correct( $required_username ) ) {
    print "find_cron_processes: This script is not being run ".
          "as user $required_username\n";
d147 1
a147 1
    return 0;  # return true
d149 1
a149 1
    return 1;  # return false
d218 302
@

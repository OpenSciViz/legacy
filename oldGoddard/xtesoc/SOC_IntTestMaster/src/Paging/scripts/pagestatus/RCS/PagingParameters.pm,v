head	1.2;
access;
symbols;
locks; strict;
comment	@# @;


1.2
date	98.07.13.15.28.27;	author jonv;	state Exp;
branches;
next	1.1;

1.1
date	98.07.13.15.11.48;	author jonv;	state Exp;
branches;
next	;


desc
@enables several paging programs to use the same set of parameters
for commonly used info
@


1.2
log
@changed initial setting of $sochome
@
text
@# ===========================================================================
# Filename    : PagingParameters.pm
# Subsystem   : Paging
# Programmer  : Jon Vandegriff, Raytheon STX
# Description : reads in parameter setting for chk_SOF and other paging scripts
#               provides a single place for environment settings such as
#               directory names, program names, and configuration info in
#               an attempt to reduce the number of dependencies between
#               programs
#
# RCS: $Id: PagingParameters.pm,v 1.1 1998/07/13 15:11:48 jonv Exp $
# ===========================================================================


package PagingParameters;

# needs no other modules to compile

use Exporter ();
@@ISA = qw(Exporter);
@@EXPORT = qw();  # do not need to export ANY symbols since they should
                 # all be accessed by the object methods


use strict;


sub new {

  my $arg1 = shift;

  my $class_name;

  if ( ref($arg1) ) {
    # called with $object->new()
    $class_name = ref( $arg1 );
  } else {
    # called with PackageName->new()
    $class_name = $arg1;
  }

  my $self = {};
  bless $self, $class_name;

  # $self has pieces 
  #            config_filename - a simple scalar
  #            parameter_list  - a hash dictionary with the key as the 
  #                                parameter name and the value as a ref to 
  #                                a list of values; note that
  # outside access function is get_values($key)

  my $sochome = $ENV{"SOCHOME"};
  if ( !defined($sochome) ) {
    $sochome = "/sochome";
    #$sochome = "/home/soc/jonv/localSOCHOME";
  }

  $self->config_filename("$sochome/etc/PAGING_PARMS.CONFIG");

  my @@lines = $self->read_in_lines();

  my $OK = 1;
  foreach (@@lines) {
    if( !$self->add_parameter_line($_) ) { $OK = 0; }
  }

  if (!$OK) {print "\nThere were problems getting the paging parameters.\n\n";}

  return $self;


}



sub config_filename {
  my $self= shift;
  my $arg = shift;
  if (defined($arg)) { $self->{"config_filename"} = $arg; }
  return $self->{"config_filename"};
}


sub read_in_lines {
  my $self = shift;
  my @@lines = ();

  my $open_OK = open(PARM_FILE, $self->config_filename() );

  if ($open_OK) {
    @@lines = <PARM_FILE>;  chomp( @@lines );
    my $close_OK = close( PARM_FILE);

    # remove comments and blanks
    @@lines = grep { !( /^\s*$/ or /^\s*#/ ) } @@lines; 

    if( $close_OK ) {
    } else {
      print "$0: ERROR: could not close the parm file ", 
             $self->config_filename(), ": $!";
  
    }
  } else {
    print "$0: ERROR:\n";
    print "Could not open file ", $self->config_filename(),
          " to access paging paramters: $!";
    print "\nThis file is required for obtaining configuration info.\n";
    print "\nIt is likely that the following program will break...\n\n";
  }

  return @@lines;

}



sub add_parameter_line {
  my $self = shift;
  my $line = shift;

  $line =~ s/^\s*//;  # remove leading white space
  $line =~ s/\s*$//;  # remove trailing white space
  my @@parts = split( /\s+/, $line );

  my $OK = 1;
  if (scalar(@@parts) < 2) {
    $OK = 0;
    print "\n$0: can't parse line from config file ", 
           $self->config_filename(),":\n";
    print "$line\n";
    print "It needs at least 2 space separated entries - \n";
    print "      the key followed by the value(s) for that key.\n";
  } else {
    my $key = shift @@parts;
    my @@values = @@parts;
    if ( $self->key_exists($key) ) {
      $OK = 0;
      print "\n$0: ERROR: duplicate key \"$key\" in the config file ", 
             $self->config_filename(), "\n";
      print "Previous use of this key used the value(s) :\n";
      print join(" ",$self->parm_values($key)),"\n";
    } else {

      $self->parm_values( $key, @@values );  #sets the entry to the given values

    } # end of check for option already existing


  } # end of check for minimum number of parts in the line

  return $OK;

}

sub key_exists {
  my $self = shift;
  my $key = shift;

  if ( defined( $self->{"parameter_list"}->{$key} ) ) {
     return 1;
  } else {
    return 0;
  }
}



sub get_values {
  my $self = shift;
  my $key = shift;

  if ($self->key_exists($key)) {
     return $self->parm_values($key);
  } else {
     return undef;
  }

}

sub parm_values {
  # sets or returns the entry for the given key, depending on 
  # wether or not you pass in a list of values
  my $self = shift;
  my $key = shift;
  my @@values = @@_;

  if (scalar(@@values) != 0) {
    $self->{"parameter_list"}->{$key} = [ @@values ];
  }
  return @@{ $self->{"parameter_list"}->{$key} };

}




sub get_keys {
  my $self = shift;

  return sort keys( %{ $self->{"parameter_list"} });
}


1;
@


1.1
log
@Initial revision
@
text
@d11 1
a11 1
# RCS: $Id$
d54 2
a55 2
    #$sochome = "/sochome";
    $sochome = "/home/soc/jonv/localSOCHOME";
@

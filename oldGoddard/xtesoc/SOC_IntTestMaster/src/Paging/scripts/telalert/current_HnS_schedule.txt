# Comments allowed in this file like this
   # or like this
# but  they are not allowed after other non-comment text on a line
# Empty lines or lines of all white space (tabs included) are also ignored.

# The first non-blank or comment line contains a space or tab separated 
# list of the names to be used in the schedule.  Each name in the 
# schedule must be found in this list, and if any name in the first 
# list is not found in the schedule, a blank scheudle is created for 
# this person.
# This is usefule because the way we have set up escalation in Telalert, there
# always needs to be a schedule defined for someone, even if they are off duty
# for a week.

Matt Angel Evan Pam Robin Toshi Brian

Jun 24: 00:00-00:59 Evan
        01:00-18:59 Angel
        19:00-23:59 Pam

Jun 25: 00:00-18:59 Pam
        19:00-23:59 Evan

Jun 26: 00:00-18:59 Pam
        19:00-23:59 Evan

Jun 27: 00:00-23:59 Evan

Jun 28: 00:00-23:59 Evan

Jun 29: 00:00-08:59 Evan
        09:00-18:59 Pam
        19:00-23:59 Evan

Jun 30: 00:00-18:59 Pam
        19:00-23:59 Evan

Jul 1:  00:00-18:59 Pam
        19:00-23:59 Angel

Jul 2:  00:00-18:59 Angel
        19:00-23:59 Evan

Jul 3:  00:00-18:59 Angel
        19:00-23:59 Evan

Jul 4:  00:00-23:59 Angel

Jul 5:  00:00-23:59 Angel

Jul 6:  00:00-18:59 Angel
        19:00-23:59 Evan

Jul 7:  00:00-18:59 Angel
        19:00-23:59 Evan

Jul 8:  00:00-18:59 Angel
        19:00-23:59 Pam


Jul 9:  00:00-08:59 Pam
        09:00-18:59 Matt
        19:00-23:59 Evan

Jul 10: 00:00-23:59 Matt

Jul 11: 00:00-23:59 Matt

Jul 12: 00:00-23:59 Matt

Jul 13: 00:00-18:59 Matt
        19:00-23:59 Evan

Jul 14: 00:00-18:59 Matt
        19:00-23:59 Evan

Jul 15: 00:00-18:59 Matt
        19:00-23:59 Angel


Jul 16: 00:00-18:59 Angel
        19:00-23:59 Evan

Jul 17: 00:00-18:59 Angel
        19:00-23:59 Evan

Jul 18: 00:00-23:59 Angel

Jul 19: 00:00-23:59 Angel

Jul 20: 00:00-18:59 Angel
        19:00-23:59 Evan

Jul 21: 00:00-18:59 Angel
        19:00-23:59 Evan

Jul 22: 00:00-18:59 Angel
        19:00-23:59 Matt



Jul 23: 00:00-18:59 Matt
        19:00-23:59 Evan

Jul 24: 00:00-23:59 Matt

Jul 25: 00:00-23:59 Matt

Jul 26: 00:00-23:59 Matt

Jul 27: 00:00-18:59 Matt
        19:00-23:59 Evan

Jul 28: 00:00-18:59 Matt
        19:00-23:59 Evan

Jul 29: 00:00-18:59 Matt
        19:00-23:59 Angel


Jul 30: 00:00-18:59 Angel
        19:00-23:59 Evan

Jul 31: 00:00-19:59 Angel
        19:00-23:59 Evan

Aug 1:  00:00-23:59 Angel

Aug 2:  00:00-23:59 Angel

Aug 3:  00:00-18:59 Angel
        19:00-23:59 Evan

Aug 4:  00:00-18:59 Angel
        19:00-23:59 Evan

Aug 5:  00:00-18:59 Angel
        19:00-23:59 Matt





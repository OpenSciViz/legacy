head	5.9;
access
	soccm
	jschwan;
symbols;
locks; strict;
comment	@# @;


5.9
date	97.03.28.20.13.19;	author jschwan;	state Exp;
branches;
next	;


desc
@Test Plan for pcastd1LC (PCA standard mode 1 light curve display).
@


5.9
log
@E-mail instructions included.
@
text
@			Functional Test Plan
			--------------------

Subsystem: PCA Science Monitoring		Date: 3/28/97

Build: 5.4

Author: Julie Schwan				Site: SOF

RCS: $Id: pcastd1LCTestPlan.txt,v 5.9 1997/03/28 20:00:04 jschwan Exp $

Test Name: Display standard mode 1 light curve data from the display
and generate both e-mail and save-to-file ascii data as well as postscript
files.

Test Goals/Objectives:

o Test the pcastd1LC binary for e-mailing ascii data.

o Test the pcastd1LC binary for saving ascii data to a file (only works if
  user has write permission to $SOCOPS/tmp or his/her own home directory).
  You can save postscript data in lieu of ascii data if you desire.
 
Requirements Verified:

[3.4.4] The SOC will carry out analyses of the portions of the science
data from the PCA and HEXTE instruments called "standard science data" (See
Section 2.7.2 of SWG-XTE-SOC-001) and the corresponding spacecraft and
instrument housekeeping data.  The "standard science data" is in a standard
format intended to provide a controlled comparison of the data on all sources.
A Set of analysis products suitable for application to all sources will be
carried out on the standard science data.  It will be called the "Standard
Analysis".  Products will be included in the data sets delivered to observers.

[3.4.5] Standard Analysis will include:
1. temporal records of housekeeping rates, orbital and spacecraft environment
parameters,
2. temporal records of selected science data (e.g. rates, histograms, measures
of spectral character, measures of variance),
3. selected science results (e.g., spectral fits, energy band fluxes, power
spectra).

[3.4.8] The SOC will maintain tools and information needed to process data
from the PCA, HEXTE, and the ASM acquired in all operating modes.


Test Description:

pcastd1LC is a binary that displays standard mode 1 data by means of a
light curve display.  The script, run_pcastd1LC, will automatically start
up the display using real-time data, which is what users will be using in
most cases.


Test Procedure:

To E-mail Data:

Select "Save" from the "File" menubar item.  A blue panel window will pop
up.  Click on the "E-mail Data" radio button and enter your e-mail address.
Then click on the "OK" button.  An E-mail of the light curve data should be
generated within at least one minute (sometimes longer if the xitserver is
down).

To Save Data to a File:

Select "Save" from the "File" menubar item.  Click on the "Save To File" button
and enter a directory path and file name for your ascii-formatted data.


Expected Results:

You should get an E-mail with 1024 elements of data (1k of data).
If you don't let the process collect data long enough, there will be
less than 1k of data, so let it run until the entire display window
has data curves filling it up.

The same is true with the file data in both ascii and postscript formats.


Verification of Results:

If you get an E-mail file and a Saved file in a directory for respective
options selected, then the test has passed.  Else, if no files are generated,
then the test has failed (or xitserv is down).
@

// ----------------------------------------------------------------------------
// File Name   : callbacks.C
// Subsystem   : SOC Science Monitoring -- Mode 1 FFT 
// Programmer  : Julie Schwan, Hughes STX/NASA GSFC
// Description :
// This has been included for use as a SOC Science Monitoring 
// pca1FFT function definition list, where new and updated
// function implementations to the pca1FFT process are added.     
//
// Most of these functions are callbacks to the Main Menu
// items for the PCA Mode 1 Fast Fourier Transform display (pca1FFT).
// This file is a collection of callback routines that
// accomodate TAE+ generated GUI (Graphical User Interface)
// functionalities.
//
// RCS: $Id: callbacks.C,v 5.9 1997/05/26 17:39:38 jschwan Exp jschwan $
// ----------------------------------------------------------------------------

// System header files
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <iostream.h>
#include <strstream.h>

// Third Party Software header files
#include <rw/cstring.h>   // RWCString class

// SOC and Local header files
#include <pcaSCommon.h>  // globals and external functions
#include <SciMonitor.h>  // monXenon object
#include "pan_pca1FFT.h" // TAEPanel* pca1FFTP
#include "pan_ownFile.h"  // externs for ownfile, etcetera

// global variable declarations
extern char *asciifile;   // ascii filename
extern char *psfile;      // postscript filename
extern char *asciidir;    // ascii directory name
extern char *psdir;       // postscript directory name
extern int selectedSum, selYaxis, selStat, compressFactor, gapShow, qPopup, qStrip;
extern SciMonitor *monXenon ;

// global function declarations
extern void emailAsciiData();        // graphics.C
extern void emailPSGraph();
extern void dumpAscii(const char *); // graphics.C
extern void doGraphics(int);         // graphics.C


// ****************
// 'File' callbacks
// ****************


//
//  This saves data via the Save File option.  An ascii file is created.
//
void save(void) {
  //cout << "Entered save callback..." << endl;

  RWCString dirascii("$SOCOPS/tmp");
  RWCString fileascii("pcaStd1Power.data");
  
  int tmp_freeze;
  tmp_freeze = isFrozen(pca1FFTP); // save original state of freeze
  if( !tmp_freeze ) toggleFreeze(pca1FFTP); // force state to frozen

  if(asciidir != NULL) dirascii = RWCString(asciidir);

  if(asciifile != NULL) fileascii = RWCString(asciifile);

  // Handle the absence of a '/' as the last character in dir
  // i.e., we want to append a '/' if there isn't one there already.
  if(dirascii[strlen(dirascii)-1] != '/')
    dirascii.append('/');

  // Handle an environment variable
  if (dirascii[0]=='$') {
      int asc = dirascii.first('/');
      RWCString envvar = dirascii(1, (asc-1));
      RWCString enviro = getenv(envvar.data());
      dirascii(0, asc) = enviro.data();
  }

  //cerr << "dirascii.data() = " << dirascii.data() << " and fileascii.data() = " << fileascii.data() << endl;

  strstream os;

  os << dirascii.data() << fileascii.data() ;

  char *fileName = os.str();

  // send the directory and file names to the saveAscii and
  // saveAsciiOwnDir functions
  dumpAscii(fileName); // this is defined in graphics.C

  delete fileName;

  // restore original state of freeze
  if( !tmp_freeze ) toggleFreeze(pca1FFTP); // toggle back
}


//
//  This sends data via the E-mail option.  No file is created.
//
void email() {
  cout << "Entered email callback" << endl;

  //temporarily freeze the display so we can get the data for the email
  int tmp_freeze; // declare tmp_freeze locally since only used here

  // save original state of freeze
  tmp_freeze = isFrozen(pca1FFTP);

  //cerr << "tmp_freeze = " << tmp_freeze << endl;

  if (!tmp_freeze) toggleFreeze(pca1FFTP); //force state to frozen

  emailAsciiData();          // graphics.C and pcaSCommon.C

  // restore original state of freeze
  if (!tmp_freeze) toggleFreeze(pca1FFTP); // toggle back to unfrozen
  cout << "Leaving email callback" << endl;
}


//
//  This sends a PostScript Graph via the E-mail option.  No file is created.
//
void emailPostScript() {
  cout << "Entered emailPostScript callback" << endl;

  //temporarily freeze the display so we can get the data for the email
  int tmp_freeze; // declare tmp_freeze locally since only used here

  // save original state of freeze
  tmp_freeze = isFrozen(pca1FFTP);

  //cerr << "tmp_freeze = " << tmp_freeze << endl;

  if (!tmp_freeze) toggleFreeze(pca1FFTP); //force state to frozen

  emailPSGraph();          // graphics.C and pcaSCommon.C

  // restore original state of freeze
  if (!tmp_freeze) toggleFreeze(pca1FFTP); // toggle back to unfrozen
  cout << "Leaving emailPostScript callback" << endl;
}


//
// This saves the data to a postscript file.  See the SciMonitor
// class for how this is actually implemented via the SciMonitor::postScript()
// function.
//
void saveAsPostScript(void) {
  //cout << "Entered saveAsPostScript callback..." << endl;
  RWBoolean isNull();
  strstream os;

  // Default path set to $SOCOPS/tmp

  RWCString dir("$SOCOPS/tmp");
  RWCString fil("pcaStd1Power.ps");

  // User specified path and/or file name

  if (psdir != NULL) {
      dir = RWCString(psdir);
      if (dir.isNull()) dir = RWCString("$SOCOPS/tmp");
  }

  if (psfile != NULL) {
      fil = RWCString(psfile);
      if (fil.isNull()) fil = RWCString("pcaStd1Power.ps");
  }

  // Handle the absence of a '/' as the last character in dir
  // i.e., we want to append a '/' if there isn't one there already.
  if(dir[strlen(dir)-1] != '/')
    dir.append('/');

  // Handle an environment variable
  if (dir[0]=='$') {
      int pos = dir.first('/');
      RWCString env_var = dir(1, (pos-1));
      RWCString env = getenv(env_var.data());
      dir(0, pos) = env.data();
  }

  //cerr << "dir.data() = " << dir.data() << " and fil.data() = " << fil.data() << endl;

  os << dir.data() << fil.data() ;

  char *fName = os.str();

  monXenon->postScript(fName);

  delete fName;
}


//
// Exit the display client
//
void quit(void) {
    //cout << "Entered quit callback..." << endl;
    char *s = getenv("PARENTPID");
    if (s) {
        //cout << "PARENTID atoi " << atoi(s) << endl;
        kill(atoi(s), SIGTERM);
    }
    pid_t pid = getppid();
    //cout << " ppid = " << pid <<  endl;
    if (pid) kill(pid, SIGTERM);
    exit(0);
}



// ******************
// 'Select' callbacks
// ******************

// callback for 'Sum All Good Xenon' under 'Select' menubar item.
//
void allGoodXenon() {
  //cout << "Entered allGoodXenon callback..." << endl;
  selectedSum = 0;
  doGraphics(False) ;
}


//
// callback for 'Sum All Very Large Events' under 'Select' menubar item.
//
void allVLE() {
  //cout << "Entered allVLE callback..." << endl;
  selectedSum = 1;
  doGraphics(False) ;
}


//
// callback for 'Sum All Propane Events'
//
void allPropane() {
  //cout << "Entered allPropane callback..." << endl;
  selectedSum = 2;
  doGraphics(False) ;
}


//
// callback for 'Sum All Other Events'
//
void allOther() {
  //cout << "Entered allOther callback..." << endl;
  selectedSum = 3;
  doGraphics(False) ;
}


//
// callback for 'PCU1'
//
void pcu1Data() {
  //cout << "Entered pcu1Data callback..." << endl;
  selectedSum = 4;
  doGraphics(False) ;
}


//
// callback for 'PCU2'
//
void pcu2Data() {
  //cout << "Entered pcu2Data callback..." << endl;
  selectedSum = 5;
  doGraphics(False) ;
}


//
// callback for 'PCU3'
//
void pcu3Data() {
  //cout << "Entered pcu3Data callback..." << endl;
  selectedSum = 6;
  doGraphics(False) ;
}


//
// callback for 'PCU4'
//
void pcu4Data() {
  //cout << "Entered pcu4Data callback..." << endl;
  selectedSum = 7;
  doGraphics(False) ;
}


//
// callback for 'PCU5'
//
void pcu5Data() {
  //cout << "Entered pcu5Data callback..." << endl;
  selectedSum = 8;
  doGraphics(False) ;
}


//
// This function gets called when 'all 5 PCUs' is selected under
// the 'Select' menubar item.
//
void pcuAll() {
  //cout << "Entered pcuAll callback..." << endl;
  selectedSum = 9;
  doGraphics(False) ;
}



// ****************
// Y AXIS callbacks
// ****************


// Linear Actual Min
void linearActual() {
  //cout << "Entered linearActual callback..." << endl;
  selYaxis = 3;
  doGraphics(False) ;
}

// Linear Zero Min
void linearFull() {
  //cout << "Entered linearFull callback..." << endl;
  selYaxis = 2;
  doGraphics(False) ;
}

// Log Actual Min 
void logActual() {
  //cout << "Entered logActual  callback..." << endl;
  selYaxis = 1;
  doGraphics(False) ;
}

// Log Zero Min
void logFull() {
  //cout << "Entered logFull callback..." << endl;
  selYaxis = 0;
  doGraphics(False) ;
}



// *******************
// 'Display' callbacks
// *******************


// Live
void live(void) {
  //cout << "Entered live callback..." << endl;
  selMode = 0 ;
  doGraphics(False) ;
}


// Integ & Ave
void integ(void) {
  //cout << "Entered integ callback..." << endl;
  selMode = 1;
  doGraphics(False) ;
}


// Late Data 
void popup(void) {
  //cout << "Entered popup callback..." << endl;
  if(qPopup == 0) qPopup = 1 ;
  else            qPopup = 0 ;
  doGraphics(False) ;
  //cout << "Entered popup callback..." << qPopup<< endl;
}


// Strip Chart
void strip(void) {
  //cout << "Entered strip callback..." << endl;
  if(qStrip == 0) qStrip = 2 ;
  else            qStrip = 0 ;
  doGraphics(False) ;
  //cout << "Entered strip callback..." << qStrip<< endl;
}



// ********************
// Intg & Ave callbacks
// ********************


//
// Start
//
void start(void) {
  //cout << "Entered start callback..." << endl;
  selInteg = 1;
}


//
// Stop
// 
void stop(void) {
  //cout << "Entered stop callback..." << endl;
  selInteg = 0;
}


//
// Clear
// 
void clear(void) {
  //cout << "Entered clear callback..." << endl;
  selInteg = 2;
  parCount = 0;
}

head	6.0;
access
	jschwan
	soccm;
symbols;
locks; strict;
comment	@ * @;


6.0
date	97.06.19.15.28.55;	author jschwan;	state Exp;
branches;
next	5.9;

5.9
date	97.05.26.21.38.19;	author jschwan;	state Exp;
branches;
next	;


desc
@Graphics functions defined here.
@


6.0
log
@added email code
@
text
@// ----------------------------------------------------------------------------
// File Name   : graphics.C
// Subsystem   : SOC Science Monitoring - pca1FFT (Mode 1 FFT)
// Programmer  : Julie Schwan, Hughes STX/NASA GSFC
// Description :
// This has been included for use as a SOC Science Monitoring
// pca1FFT function definition list, where new graphics
// function implementations to the pca1FFT process are added.
//
// Most of these functions are graphics callbacks to the Main Menu
// items for the PCA Standard Mode 1 FFT display (pca1FFT).
// This file is a collection of callback routines that
// accomodate TAE+ generated GUI (Graphical User Interface)
// functionalities.
//
// RCS: $Id: graphics.C,v 5.9 1997/05/26 21:38:19 jschwan Exp jschwan $
// ----------------------------------------------------------------------------

// System header files
#include <unistd.h>
#include <assert.h>
#include <iostream.h>
#include <stdio.h>
#include <fstream.h>
#include <strstream.h>
#include <sys/fcntl.h>

// Third Party Software header files
#include <rw/bstream.h>
#include <X11/At/XYBarPlot.h>
#include <X11/At/Plotter.h>

// SOC and Local header files
#include <pcaSCommon.h>
#include <SciMonitor.h>
#include <SciDisplay.h>
#include <std1EdsPart.h>
#include <Utilities.h>
#include <SOCbstream.h>
#include "pan_pca1FFT.h"

//for statistical information
double xenonMax[6], xenonMin[6];
double sumMax, sumMin;
int binIndex;
const int numData =std1NumTVals/2 + 1;
static char  status[80];
static String modeName[2] = {"Live","Integ & Ave" };
const String sumName[10] = {"pcuSUM","vleSum","prSum","other","PCU1","PCU2",
                            "PCU3","PCU4","PCU5","all PCUs" };

extern selectedSum, selYaxis, selDet, setOwn, compressFactor, qPopup, qStrip;
extern SciMonitor *monXenon;
// E-mail variables and constants
extern char *mailAddr;
const char *subject = "pcaStd1Power.data";
const char *subjectPS = "pcaStd1Power.ps";

static XtInputId theInputId;
static SOCbistream theSOCbis(STDIN_FILENO);
static EdsPartition edsPart;
static pcaSCommon pcacom;
static double currentTime;
static SOCTime2 socTime;
int nvals;
RWCString title;
RWCString subtitle;
RWCString xlabel;
RWCString ylabel;
SciDisplay outDisplay("pcaSMaux ");
static double monMet;
static int openf=FALSE;
static double preTime;
 
// Double buffs, index 0 of xenonCounts matrix is sum over indices 1-5		
static DoubleGenMat xenonCounts( std1NumTVals, 6, 0.0);
static DoubleVec    vleCounts(std1NumTVals,0.0,0.0);
static DoubleVec    propaneCounts(std1NumTVals, 0.0, 0.0);
static DoubleVec    otherCounts(std1NumTVals, 0.0, 0.0);
static DoubleVec    xvals(numData, 0.0, 1.0); // for athenaPlotters

// Double buffs for FFTied data array
static DoubleGenMat xenonfft(numData,6,0.0 );
static DoubleVec    vlefft(numData,0.0, 0.0);
static DoubleVec    propanefft(numData, 0.0, 0.0);
static DoubleVec    otherfft(numData, 0.0, 0.0);
 
// Double buffs for Integrated data array
static DoubleGenMat xenonIntg(numData,6,0.0 );
static DoubleVec    vleIntg(numData,0.0 );
static DoubleVec    propaneIntg(numData, 0.0 );
static DoubleVec    otherIntg(numData, 0.0 );

// Double buffs for compressed data array
static DoubleGenMat plotLC( numData, 6, 0.0);
static DoubleGenMat plotBuff( numData, 6, 0.0);
static String plotName[5] ;
static int nLC = 0 ;
 
// plot labels
static const char *titleXenon[2] = { "XworkXenon", "@@diamonds Good Xenon Event Rates (PCUs 1-5) @@diamonds"};

//static const char *titleSums[2] = {"XworkSums", "@@diamonds Selected Summed Rates (All PCUs, VLE, Propane, or Other) @@diamonds"};

//static char *sumSubtitle[10] = { "Selected: Xenon Sum Over All PCUs", "Selected: VLE Sum over All PCUs", "Selected: Propane Sum over All PCUs", "Selected: All Other Events, Sum over All PCUs", "Selected: PCU 1", "Selected: PCU 2", "Selected: PCU 3", "Selected: PCU 4", "Selected: PCU 5", "Selected: all 5 PCUs "};

static String pcuColor[5] = {"green", "blue", "orange", "magenta", "cyan"};


//
// This saves data via the Save To File option.  An ascii file is created. 
// 
void dumpAscii(const char *fileName) {
   pcacom.saveAsciiOwnDir(fileName, plotBuff, numData, nLC, status, currentTime, 1, plotName) ;
}


//
// Get Data for E-mail Ascii callback
//
void emailAsciiData() {
   pcacom.emailAscii(mailAddr, subject, plotBuff, numData, nLC, status, currentTime, 1, plotName);
}


//
// Get Data for E-mail PostScript callback
//
void emailPSGraph() {
   // monXenon is a SciMonitor object that points to an Athena Widget object
   // which retrieves plotting information for the graph on the display
   // see SciMonitor class for more detailed information
   pcacom.emailPS(mailAddr, subjectPS, monXenon->plotterWidget()); 
}


void setPlots(unsigned short *theData) { 
  //cerr << "setPlots> using theData pointer: " << (unsigned ) theData << endl;
  //cerr << "Entered setPlots" << endl;
  EdsPartition tmp = (EdsPartition) edsPart;
  std1EdsPart currentPart(tmp) ; 

  // always reset the 5 pcus

  int pcuid;

  for( pcuid = 1; pcuid < 6 ; pcuid++ ) { // short to double conversion
    DoubleVec pcu(xenonCounts(RWAll,pcuid));
    currentPart.xenon(pcuid, theData, pcu.data());
    // cerr << "Xenon pcu #" << pcuid << " " << pcu << endl;
  }

  // reset VLE, propane, other and sum of all xenons

  currentPart.vle(theData, vleCounts.data());
  currentPart.propane(theData, propaneCounts.data());
  currentPart.other(theData, otherCounts.data());

  // compute sum of all xenons

  xenonCounts( RWAll, 0) = 0.0; 
  for (pcuid = 1; pcuid < 6 ; pcuid++) { // short to int conversion
     xenonCounts( RWAll,0) += xenonCounts(RWAll,pcuid ); 
  }
  //just for the testing
  //     xenonCounts(100,1) = 5.0 ;
  //     xenonCounts(RWAll,2) = 12.00 ;
  //     xenonCounts(200,2) = 20.00 ;

  // calculate FFT        

  int check ;
  for (pcuid = 0; pcuid < 6 ; pcuid++) {
      DoubleVec pcu(xenonCounts(RWAll,pcuid ));
      DoubleVec tmp;
      check =  currentPart.leahyPower(pcu,  tmp);
      xenonfft(RWAll,pcuid) = tmp(RWAll);
  }

  check = currentPart.leahyPower(vleCounts, vlefft);
  check = currentPart.leahyPower(propaneCounts, propanefft);
  check = currentPart.leahyPower(otherCounts, otherfft);

  //---------------------------------
  // handle integration mode
  static int oldInteg = 0;

  if (selInteg >= 2){
     initialize () ;
     selInteg = oldInteg ;
  }
  if (selInteg != 0) {
     parCount += 1;
     accumulate();
     if(parCount == 1) starTime = currentTime ;
     endTime = currentTime ;
  }
 
  oldInteg = selInteg ;

  return;
} 

 
void selectRates() {
 
  //cerr << " selectRates entered -----"<<"selMode = " << selMode <<  endl;
 
  // first clear plotLC array

  for (int j =0;  j< 6  ; j ++ ) {
     plotLC(RWAll, j)  = 0.0 ;
  }

  // load the data
 
  for (j =1;  j< 6  ; j ++ ) {
      if(selMode <=0)plotLC(RWAll, j)  = xenonfft(RWAll, j ) ;
      else           plotLC(RWAll, j)  = xenonIntg(RWAll, j )/parCount ;
  }
 
  switch(selectedSum) {
       case 1: // sum of very large events all pcus
            {
               if(selMode <=0) plotLC(RWAll, 0)  = vlefft( RWAll) ;
               else            plotLC(RWAll, 0)  = vleIntg( RWAll)/parCount ;
            }
               break;
       case 2: // sum of propane events all pcus
            {
               if(selMode <=0) plotLC(RWAll, 0)  = propanefft( RWAll) ;
               else            plotLC(RWAll, 0)  = propaneIntg( RWAll)/parCount ;
            }
               break;
       case 3: // sum of all other events all pcus
            {
               if(selMode <=0) plotLC(RWAll, 0)  = otherfft( RWAll) ;
               else            plotLC(RWAll, 0)  = otherIntg( RWAll)/parCount ;
            }
               break;
       case 4: //  xenon  pcu1
       case 5: //  xenon  pcu2
       case 6: //  xenon  pcu3
       case 7: //  xenon  pcu4
       case 8: //  xenon  pcu5
            {
               selDet = selectedSum-3 ;
               if(selMode <=0)plotLC(RWAll, 0)  = xenonfft(RWAll, selDet ) ;
               else           plotLC(RWAll, 0)  = xenonIntg(RWAll,selDet )/parCount ;
            }
               break;
       case 9: //  all 5 PCUs
 
               //already have data          
 
               break;
 
       case 0: // default to sum of xenon all pcus
 
       default: // sum of all good Xenons
             {
               if(selMode <=0)plotLC(RWAll, 0)  = xenonfft(RWAll, 0 ) ;
               else           plotLC(RWAll, 0)  = xenonIntg(RWAll,0 )/parCount ;
             }
            break;
   }
 
  // set first element of the array to 2.0

  for (j =0;  j< 6  ; j ++ ) {
     plotLC(0, j)  = 2.0 ;
  }

  //cerr << " selectRates exited  -----"<<  endl;
 
  return ;
}
 

void initialize() {
  //cerr << " initialize entered -----"<< selInteg << endl;
 
  for (int pcuid =0 ; pcuid < 6 ; pcuid++){
     xenonIntg(RWAll,pcuid ) = 0.0;
  }
 
  vleIntg(RWAll) = 0.0 ;
  propaneIntg(RWAll) = 0.0 ;
  otherIntg(RWAll) = 0.0 ;

  return ;
}    

 
void accumulate() {
    //cerr << " accumulate entered"<<  endl;
    for (int pcuid =0 ; pcuid < 6 ; pcuid++){
       xenonIntg (RWAll,  pcuid) += xenonfft(RWAll, pcuid);
    }   

    vleIntg(RWAll) += vlefft(RWAll) ;
    propaneIntg(RWAll) += propanefft(RWAll) ;
    otherIntg(RWAll) += otherfft(RWAll) ;
    //cerr << " leaving accumulate" << endl;
}

 
void computeArea() {
  //cerr << " computeArea entered" << endl;
  // calculate area, minimum and maximun of each light curve
 
  for( int i = 0; i < 6; i++ ) {
    DoubleVec tmp1(plotLC(RWAll, i ));
    xenonMin[i] = minValue(tmp1) ;
    xenonMax[i] = maxValue(tmp1) ;
  }
 
   sumMax = xenonMax[0];
   sumMin = xenonMin[0] ;
   //cerr << " sumMax = " << sumMax <<","<<sumMin<< endl ;
 
   return ;
}


void resetYaxis()
{
//cerr << " resetYaxis entered -- " << " selYaxis = " << selYaxis << endl;
 
  switch(selYaxis) {

    case 1: // log in actual scale 
            //-------auto scale doesn't convert min=0 to min=0.1--------
       {
            //cerr << "case1 of resetYaxis entered" << endl;
            XtVaSetValues(monXenon->yaxisWidget(),
                XtNautoScale, True,
                XtNaxisTransform, AtTransformLOGARITHMIC, NULL);
       } 
            break;

    case 2: // linear in zeroMin scale
       { 
            //cerr << "case2 of resetYaxis entered" << endl;
            double maxy = computeYmax1();
            double ymin = 0.0 ;
 
            XtVaSetValues(monXenon->yaxisWidget(),
                XtNautoScale, False, XtNmin, &ymin, XtNmax, &maxy,
                XtNaxisTransform, AtTransformLINEAR, NULL);
       } 
            break;

    case 3: // linear in actual scale  
       { 
            //cerr << "case3 of resetYaxis entered" << endl;
            XtVaSetValues(monXenon->yaxisWidget(),
                XtNautoScale, True,
                XtNaxisTransform, AtTransformLINEAR, NULL);
       } 
            break;

    case 0: // log in zeroMin scale --- this is default
       {  
            //cerr << "case0 of resetYaxis entered" << endl;
            double maxy = computeYmax1();
            double ymin = 0.1 ;
 
            XtVaSetValues(monXenon->yaxisWidget(),
                XtNautoScale, False, XtNmin, &ymin, XtNmax, &maxy,
                XtNaxisTransform, AtTransformLOGARITHMIC, NULL);
        } 
 
     default: //
            break;
  }
  //cerr << "leaving resetYaxis..." << endl;
  return;
}


double computeYmax1() {
//cerr << " computeYmax1 entered-----"<<  endl;
  double ymax = 0.0 ;
  if (selectedSum >= 9) {
     for(int j = 1; j< 6; j++ ) {
        DoubleVec tmp1(plotLC(RWAll, j));
        double max1 = maxValue(tmp1) ;
        ymax = max(max1, ymax) ;
     }
  }

  else {
     DoubleVec tmp1(plotLC(RWAll, 0 ));
     ymax = maxValue(tmp1) ;
  }
   //cerr << "leaving computeYmax1..." << endl;
   return ymax;
}
 

void noZeros() {
  for( int i = 0; i < 6; i++ ) {
     for( int j = 0; j < numData ; j++ ) {
        if(plotLC(j,i) <= 0.0) plotLC(j, i) = 0.1;
     }
  }
  return ;
}


void attachXenon() {
  //cerr << "Entered attachXenon..." << endl;
  //cerr << "selectedSum        ..." <<selectedSum << endl;
  static String sumColor = "black";
  static int colorMap =  1;
  int pcuid;
  cerr.setf(ios::scientific,ios::floatfield);

  // Attach the application data to the plot children, xenon pcus 1-5
  if(selectedSum >= 9) {
     for( int i = 0; i < 5; i++) {
        XtVaSetValues(monXenon->lineWidget(i), XtVaTypedArg, XtNforeground,
                      XtRString, pcuColor[i], 1+strlen(pcuColor[i]), NULL);
        pcuid = i+1; 
        DoubleVec pcu(plotLC(RWAll,pcuid ));
        monXenon->drawLine(&xvals, &pcu, pcuid-1, numData);
 
        // load plotBuff array
        plotBuff(RWAll,i) = plotLC(RWAll, pcuid) ;
        plotName[i] = sumName[i+4] ;
        nLC = 5 ;
     }
  }
  else {
     if (selectedSum >= 4) {
        for( int i = 0; i < 5; i++) {
          selDet = selectedSum - 3 ;
          XtVaSetValues(monXenon->lineWidget(i),
                        XtVaTypedArg,
                        XtNforeground,
                        XtRString,
                        pcuColor[selDet-1],
                        1+strlen(pcuColor[selDet-1]),
                        NULL);
        }
     }
 
     else {
        for( int i = 0; i < 5; i++) {
           XtVaSetValues(monXenon->lineWidget(i),
                         XtVaTypedArg,
                         XtNforeground,
                         XtRString,
                         sumColor,
                         1+strlen(sumColor),
                         NULL);
        }
     }
     for( pcuid = 1; pcuid <= 5; pcuid++ ) {
       DoubleVec pcu(plotLC(RWAll, 0));
       monXenon->drawLine(&xvals, &pcu, pcuid-1, numData);
       
        // load plotBuff array
        plotBuff(RWAll,0) = plotLC(RWAll, 0) ;
        plotName[0] = sumName[selectedSum] ;
        nLC = 1 ;
     }
  }

  //cerr << "Exit attachXenon..." << endl;
  return;
}


void readData() {
  // first reset the cursor
  unsigned short* unpackedData= NULL ;
  static int first = TRUE ;

  if (!theSOCbis.eof()) {

     setCursor(pca1FFTP);
     //cerr << "readData> attempt to restore partition..." << endl;

     theSOCbis >> edsPart;

     currentTime= edsPart.partStartTime();

     //cerr << "check 3--- currenttime = " << (int)currentTime    << endl;

     if (openf) {
        unsigned int np;
        np = 1;
        DoubleVec x(np,0.0);
        DoubleVec v0(np, 0.0);
        if(!first) v0 = currentTime- 128.0-preTime ;
        DoubleGenMat dm(np, 1, 0.0);
        dm.col(0) = v0;
        if (v0(0) != 0.0) {
           cerr << " met diff = " << v0(0) << endl ;
           cerr <<"ouput double-matrix rows, cols: "<<dm.rows()<<" , "<<dm.cols()<<endl;
        }
        outDisplay.send(dm, monMet);
     }
 
     preTime = currentTime;
     first = FALSE;
     
     unpackedData = edsPart.getData();
     //cerr << "check 4-- unpacked data = " << (unsigned)unpackedData    << endl;

     if(isFrozen(pca1FFTP)) { // for now just empty the pipe and discard the partition
       //cerr << "Frozen! Ignore new partition..." << endl;
       delete unpackedData;
       unpackedData = NULL;
     }
     else if(unpackedData != NULL) {

       setPlots(unpackedData);

       // free the partition's data buff
       delete unpackedData;

       // select data to be plotted.
       selectRates();
 
       // calculate total counts
       computeArea();

       // handle yaxis "log" or "linear"

       if(selYaxis <= 1 ) noZeros();
       resetYaxis();
       attachXenon();
       updateStatus();
     } // end else if 

     // reset cursor
     unsetCursor(pca1FFTP);
  }  // end of if ( eof())
  //cerr << "leaving readData" << endl;
}


void doGraphics(int init) {
  //cerr << "entered doGraphics..." << endl;
  // entry from callbacks:
  if (!init) {
    // select data  to be plotted.
    selectRates();
    // calculate total counts
    computeArea();
    // handle yaxis "log" or "linear"
    if(selYaxis <= 1)  noZeros();
    resetYaxis();
    attachXenon();
    updateStatus();

    monMet = qPopup + qStrip ;
     
    if (monMet <= 0.0 ) {
       if (!openf) ;
       else {
         outDisplay.close();
         openf = FALSE;
       }
    }
    else {
       if (!openf) {
          outDisplay.openStrip(101, "", "", "counter index", "MET diff");
          openf = TRUE;
       }
       unsigned int np;
       np = 1;
       DoubleVec x(np,0.0);
       DoubleVec v0(np, 0.0);
       v0 = 0.0; 
       DoubleGenMat dm(np, 1, 0.0);
       dm.col(0) = v0;
       //cerr <<"ouput double-matrix rows, cols: " <<dm.rows()<< " , "<<dm.cols() <<endl;
       outDisplay.send(dm, monMet + 1000.0);
    }
  return;
  } // end if(!init)

  // entry from main:
  selectedSum = 0; // this should be the default start-up
  theInputId = 0;

  // allocate temporary memory for this object
  monXenon = new SciMonitor(pca1FFTP, titleXenon[0], "", -16, -24, "",
                            "Bin Numbers", 8 , "Amplitudes", 16, 5, 0);

  cerr << "doGraphics> created monXenon: " << (int ) monXenon << endl;

  for( int i = 0; i < 5; i++) { 
    //  cerr << "doGraphics> set line color for Xenon lineWidget: " <<
    //           (int ) monXenon->lineWidget(i) << endl;
  
    XtVaSetValues(monXenon->lineWidget(i), XtVaTypedArg, XtNforeground,
                  XtRString, pcuColor[i], 1+strlen(pcuColor[i]), NULL); 
  }

  
  // don't forget to init the xval array:
  for( i = 0; i < xvals.length(); i++ ) xvals[i] = i;

  //  cerr << "xvals= " << xvals << endl;

  //------------using new lib-----------
  monXenon->resetX2Y2Axes("Frequency (Hz)",""); 
  double x2min = 0.0  ;
  double x2max = 4.0  ;
  double interval = (x2max-x2min) /8.0  ;

  XtVaSetValues(monXenon->x2axisWidget(), XtNmirror, True, XtNautoScale, False,
                XtNmin, &x2min, XtNmax, &x2max, XtNticInterval, &interval,
                XtNticFormat, "%.4f", XtNaxisTransform, AtTransformLINEAR, NULL);
 
  XtVaSetValues(monXenon->y2axisWidget(), XtNdrawTics, False, XtNmirror, True,
                XtNdrawNumbers, False, NULL);
    
  // establish pipe i/o handler
  theInputId = monXenon->setIOhandler(readData, theSOCbis.fd());

  cerr << "leaving doGraphics..." << endl;

  // memory deleted for SciMonitor object (new MUST use delete -jschwan02/24/97)
  //delete monXenon;
  return;
}


void updateStatus () {
  //construct string for top label
  strcpy(status, modeName[selMode]);
  strcat(status,"\0");
  //cout << " selected ---"<< status  << endl;

  // check statistics option
  if (compressFactor <= 0) compressFactor = 1 ;
  double binsize = 0.125 * compressFactor ;

  // update dynamic text field
 
  socTime = SOCTime2(currentTime); // current  time
  RWCString tmp = ((RWTime)socTime).asString('\0',RWZone::utc());
  //RWCString tmp = ((RWTime)socTime).asString();

  ostrstream st_os ;
  st_os << "Status :  "<< status <<"  UT = " << tmp.data() << "  (MET = " << (int)currentTime << ")" ;
   TaeItem *statusItem = pca1FFTP->GetItem("topStat");
   statusItem->Update(st_os.str()) ;
   delete st_os.str() ;

   ostrstream st2_os ;
   st2_os << " No. of Data Integrated = " << parCount ;
   TaeItem *integItem = pca1FFTP->GetItem("integlabel");
   integItem->Update(st2_os.str()) ;
   delete st2_os.str() ; 

   ostrstream pcu1_os ;
   pcu1_os << "PCU1 : "<<"  max = " << xenonMax[1]<<"  min =  "<<xenonMin[1] ;
   TaeItem *pcu1Item = pca1FFTP->GetItem("pcu1stat");
   pcu1Item->Update(pcu1_os.str()) ;
   delete pcu1_os.str() ;
 
   ostrstream pcu2_os ;
   pcu2_os << "PCU2 : "<<"  max = " << xenonMax[2]<<"  min =  "<<xenonMin[2]  ;
   TaeItem *pcu2Item = pca1FFTP->GetItem("pcu2stat");
   pcu2Item->Update(pcu2_os.str()) ;
   delete pcu2_os.str() ;
     
   ostrstream pcu3_os ;
   pcu3_os << "PCU3 : "<<"  max = " << xenonMax[3]<<"  min =  "<<xenonMin[3]  ;
   TaeItem *pcu3Item = pca1FFTP->GetItem("pcu3stat");
   pcu3Item->Update(pcu3_os.str()) ;
   delete pcu3_os.str() ;
     
 
   ostrstream pcu4_os ;
   pcu4_os << "PCU4 : "<<"  max = " << xenonMax[4]<<"  min =  "<<xenonMin[4]  ;
   TaeItem *pcu4Item = pca1FFTP->GetItem("pcu4stat");
   pcu4Item->Update(pcu4_os.str()) ;
   delete pcu4_os.str() ;
     
 
   ostrstream pcu5_os ;
   pcu5_os << "PCU5 : "<<"  max = " << xenonMax[5]<<"  min =  "<<xenonMin[5] ;
   TaeItem *pcu5Item = pca1FFTP->GetItem("pcu5stat");
   pcu5Item->Update(pcu5_os.str()) ;
   delete pcu5_os.str() ;
 
 
   ostrstream pcuall_os ;
   if(selectedSum >= 9) {
      pcuall_os << sumName[selectedSum]<< " : "<<"  NA "  ;
   }
   else
   pcuall_os << sumName[selectedSum]<< " : "<<"  max = " << sumMax<<"  min = "<<sumMin ;
   TaeItem *pcuallItem = pca1FFTP->GetItem("pcuAll");
   pcuallItem->Update(pcuall_os.str()) ;
   delete pcuall_os.str() ;

   return ;
}
@


5.9
log
@Added e-mail and postscript code.
@
text
@d16 1
a16 1
// RCS: $Id$
d54 1
d57 1
d103 1
a103 1
static const char *titleSums[2] = {"XworkSums", "@@diamonds Selected Summed Rates (All PCUs, VLE, Propane, or Other) @@diamonds"};
d105 1
a105 7
static char *sumSubtitle[10] = { "Selected: Xenon Sum Over All PCUs",
                                 "Selected: VLE Sum over All PCUs",
            			 "Selected: Propane Sum over All PCUs",
			         "Selected: All Other Events, Sum over All PCUs", 
			         "Selected: PCU 1", "Selected: PCU 2", "Selected: PCU 3",
			         "Selected: PCU 4", "Selected: PCU 5", 
			         "Selected: all 5 PCUs "};
d119 1
a119 1
// Get Data for E-mail call in pcaSCommon.C ../common directory) and callbacks.C
d121 1
a121 1
void EmailAsciiData() {
d126 11
d594 2
a595 1
  //cerr << "doGraphics> created monXenon: " << (int ) monXenon << endl;
d627 2
d630 1
a630 4

  //cerr << "leaving doGraphics..." << endl;

  delete monXenon;
@

head	6.2;
access;
symbols;
locks; strict;
comment	@ * @;


6.2
date	98.10.09.13.23.22;	author jonv;	state Exp;
branches;
next	6.1;

6.1
date	98.05.21.18.14.59;	author jonv;	state Exp;
branches;
next	6.0;

6.0
date	97.06.19.21.17.40;	author jschwan;	state Exp;
branches;
next	5.9;

5.9
date	97.06.06.19.09.37;	author jschwan;	state Exp;
branches;
next	;


desc
@callback functions.
@


6.2
log
@removed some commented out code which was not longer used
@
text
@// ----------------------------------------------------------------------------
// File Name   : callbacks.C
// Subsystem   : SOC Science Monitoring - pcastd2LLC (Mode 2 Light Curve)
// Programmer  : Julie Schwan, Hughes STX/NASA GSFC
// Description :
// This has been included for use as a SOC Science Monitoring
// pcastd2LLC function definition list, where new and updated
// function implementations to the pcastd2LLC process are added.
//
// Most of these functions are callbacks to the Main Menu
// items for the PCA Standard Mode 2 LC display (pcastd2LLC).
// This file is a collection of callback routines that
// accomodate TAE+ generated GUI (Graphical User Interface)
// functionalities.
//
// RCS: $Id: callbacks.C,v 6.1 1998/05/21 18:14:59 jonv Exp $
// ----------------------------------------------------------------------------

// System header files
#include <stdlib.h>      // getenv() and atoi() definitions
#include <signal.h>      // kill() and SIGTERM definitions
#include <unistd.h>      // header for symbolic constants
#include <iostream.h>    // I/O stream header
#include <strstream.h>   // iostream specialized to arrays

// Third Party Software header files
#include <rw/cstring.h>   // RWCString class

// Local header files
#include "pcaSCommon.h"    // globals and external functions
#include "pan_pca2LLC.h"   // TAEPanel* pca2LLCP
#include "pan_ownFile.h"   // externs for asciifile, etcetera

// global variable declarations
extern char *asciidir, *asciifile, *psdir, *psfile;
extern SciMonitor *monXenon;
extern void emailAsciiData();
extern void emailPSGraph();
extern void dumpAscii(const char *);
extern void doGraphics(int);


// ****************
// 'File' callbacks
// ****************

//
//  This saves data via the Save To File option.  An ascii file is created.
//
void save(void) {
  //cout << "Entered save callback..." << endl;

  RWCString dirascii("$SOCOPS/tmp");
  RWCString fileascii("pcaStd2LayerLC.data");
  RWBoolean isNull();

  int tmp_freeze = isFrozen(pca2LLCP); // save original state of freeze
  if( !tmp_freeze ) toggleFreeze(pca2LLCP); // force state to frozen

  // now perform the save...

  if(asciidir != NULL) {
    dirascii = RWCString(asciidir);
    if(dirascii.isNull()) dirascii = RWCString("$SOCOPS/tmp");
  }

  if(asciifile != NULL) {
    fileascii = RWCString(asciifile);
    if(fileascii.isNull()) fileascii = RWCString("pcaStd2LayerLC.data");
  }

  // Handle the absence of a '/' as the last character in dir
  // i.e., we want to append a '/' if there isn't one there already.
  if(dirascii[strlen(dirascii)-1] != '/')
    dirascii.append('/');

  // Handle an environment variable
  if (dirascii[0]=='$') {
      int asc = dirascii.first('/');
      RWCString envvar = dirascii(1, (asc-1));
      RWCString enviro = getenv(envvar.data());
      dirascii(0, asc) = enviro.data();
  }

  cerr << "dirascii.data() = " << dirascii.data() << " and fileascii.data() = " << fileascii.data() << endl;

  strstream os;
  os << dirascii.data() << fileascii.data() ;
  char *fName = os.str();

  dumpAscii(fName); // this is defined in plot2LLC.C
 
  delete fName;

  // restore original state of freeze
  if( !tmp_freeze ) toggleFreeze(pca2LLCP); // toggle back
}


//
//  This sends ASCII data via the E-mail option.  No file is created.
//
void email(void) {
  //cout << "Entered email callback" << endl;

  // temporarily freeze the display so we can get the data for the save
  int tmp_freeze; // declare tmp_freeze locally since only used here

  // save original state of freeze
  tmp_freeze = isFrozen(pca2LLCP);

  //cerr << "tmp_freeze = " << tmp_freeze << endl;

  if (!tmp_freeze) toggleFreeze(pca2LLCP); //force state to frozen

  emailAsciiData();          // graphics.C and pcaSCommon.C

  // restore original state of freeze
  if (!tmp_freeze) toggleFreeze(pca2LLCP); // toggle back to unfrozen
  //cout << "Leaving email callback" << endl;
}


//
//  This sends the PostScript graph via the E-mail option.  No file is created.
//
void emailPostScript() {
  //cout << "Entered EmailPSGraph callback" << endl;

  // temporarily freeze the display so we can get the data for the save
  int tmp_freeze; // declare tmp_freeze locally since only used here

  // save original state of freeze
  tmp_freeze = isFrozen(pca2LLCP);

  //cerr << "tmp_freeze = " << tmp_freeze << endl;

  if (!tmp_freeze) toggleFreeze(pca2LLCP); //force state to frozen

  emailPSGraph();          // plot2LLC.C and pcaSCommon.C

  // restore original state of freeze
  if (!tmp_freeze) toggleFreeze(pca2LLCP); // toggle back to unfrozen
  //cout << "Leaving EmailPSGraph callback" << endl;
}


//
// This saves the data to a postscript file.  See the SciMonitor
// class for how this is actually implemented via the SciMonitor::postScript()
// function.
//
void saveAsPostScript(void) {

  //cout << "Entered saveAsPostScript callback..." << endl;

  RWBoolean isNull();
  strstream os;

  // Default path set to $SOCOPS/tmp

  RWCString dir("$SOCOPS/tmp");
  RWCString fil("pcaStd2LayerLC.ps");

  // User specified directory path

  if(psdir != NULL) {
    dir = RWCString(psdir);
    if(dir.isNull())
      dir = RWCString("$SOCOPS/tmp");
  }

  // User specified file name

  if(psfile != NULL) {
    fil = RWCString(psfile);
    if(fil.isNull())
      fil = RWCString("pcaStd2LayerLC.ps");
  }

  // Handle the absence of a '/' as the last character in dir
  // i.e., we want to append a '/' if there isn't one there already.

  if(dir[strlen(dir)-1] != '/') dir.append('/');


  // Handle an environment variable

  if (dir[0]=='$') {
      int pos = dir.first('/');
      RWCString env_var = dir(1, (pos-1));
      RWCString env = getenv(env_var.data());
      dir(0, pos) = env.data();
  }

  //cerr << "dir.data() = " << dir.data() << " and fil.data() = " << fil.data() << endl;

  os << dir.data() << fil.data() ;

  char *fName = os.str();

  monXenon->postScript(fName);

  delete fName;

  //cerr << "Leaving saveAsPostScript() callback..." << endl;
}


//
// Exit the display client
//
void quit(void) {
  //cout << "Entered quit callback..." << endl;
  //monXenon->quit() ;
  char* s=getenv("PARENTPID");
  if( s ){
    cout << "PARENTID atoi  " << atoi(s)<< endl ;
    kill (atoi(s),SIGTERM) ;
  }

  pid_t pid = getppid() ;
    //cout << " ppid = " << pid <<  endl ;
  if(pid) kill (pid,SIGTERM) ;
  exit(0);
}

void layer1Data() {
  //cout << "Entered layer1Data callback..." << endl;
  selLayer = 0;
  doGraphics(False) ;
}

void layer2Data() {
  //cout << "Entered layer2Data callback..." << endl;
  selLayer = 1;
  doGraphics(False) ;
}

void layer3Data() {
  //cout << "Entered layer3Data callback..." << endl;
  selLayer = 2;
  doGraphics(False) ;
}

void layerAll() {
  //cout << "Entered layerAll callback..." << endl;
  selLayer = 3;
  doGraphics(False) ;
}

void plotAll() {
  //cout << "Entered plotAll callback..." << endl;
  selLayer = 4;
  doGraphics(False) ;
}

void linearFull() {
  //cout << "Entered linearFull callback..." << endl;
  selYaxis = 0;
  doGraphics(False) ;
}

void logFull() {
  //cout << "Entered logFull callback..." << endl;
  selYaxis = 1;
  doGraphics(False) ;
}

void linearOwn() {
  selYaxis = 2;
  doGraphics(False) ;
}

void logOwn() {
  selYaxis = 3;
  doGraphics(False) ;
}


void rawData() {
  //cout << "Entered rawData  callback..." << endl;
  selStat  = 1;
  doGraphics(False) ;
}

void computedData() {
  //cout << "Entered computedData  callback..." << endl;
  selStat  = 0;
  doGraphics(False) ;
}

void optX1() {
  //cout << "Entered optX1  callback..." << endl;
  compressFactor = 1;
}

void optX2() {
  //cout << "Entered optX2  callback..." << endl;
  compressFactor = 2;
}

void optX4() {
  //cout << "Entered optX4  callback..." << endl;
  compressFactor = 4;
}

void wGaps() {
  //cout << "Entered wGaps callback..." << endl;
  gapShow = 0;
}

void woGaps() {
  //cout << "Entered woGaps callback..." << endl;
  gapShow = 1;
}

void popup(void) {
//cout << "Entered popup callback..." << endl;
  if(qPopup == 0) qPopup = 1 ;
  else            qPopup = 0 ;
  doGraphics(False) ;
  //cout << "Entered popup callback..." << qPopup<< endl;
}

void strip(void) {
//cout << "Entered strip callback..." << endl;
  if(qStrip == 0) qStrip = 2 ;
  else            qStrip = 0 ;
  doGraphics(False) ;
  //cout << "Entered strip callback..." << qStrip<< endl;
}
@


6.1
log
@added autosave feature - callbacks not really changed
the routine I was going to use is commented out
@
text
@d16 1
a16 1
// RCS: $Id: callbacks.C,v 6.0 1997/06/19 21:17:40 jschwan Exp $
a332 24



// void set_plot_chars_for_auto_save(const int layer, 
//                                 const int lowChannel, const int highChannel)
// {
// 
//   // these vars declared in ../../common/pcaSCommon.C
//   // and accessed via extern statement in ../../common/pcaSCommon.h
// 
//   selLayer = layer;  // layer 1 has best signal to noise 
//                      // (layer 1 is settting 0 here)
// 
//   chStart[0] = lowChannel;
//   chEnd[0] = highChannel;
// 
// 
//   cout << "AUTO SAVE FEATURE ENABLED WITH THE FOLLOWING OPTIONS:" << endl;
//   cout << "  layer selection set to " << layer << endl;
//   cout << "  min and max channels set to " << lowChannel << " and " 
//        << highChannel << endl;
// 
//   doGraphics(False);  // resets the graphics screen
// }
@


6.0
log
@added email code
@
text
@d16 1
a16 1
// RCS: $Id: callbacks.C,v 5.9 1997/06/06 19:09:37 jschwan Exp jschwan $
d91 1
a91 1
  dumpAscii(fName); // this is defined in graphics.C
d140 1
a140 1
  emailPSGraph();          // graphics.C and pcaSCommon.C
d218 1
a218 1
    //cout << "PARENTID atoi  " << atoi(s)<< endl ;
d333 24
@


5.9
log
@email and ps.
@
text
@d16 1
a16 1
// RCS: $Id$
d37 2
a38 1
extern void EmailAsciiData();
d101 1
a101 1
//  This sends data via the E-mail option.  No file is created.
d104 1
d106 23
a128 1
  //cout << "Entered email callback..." << endl;
d140 1
a140 1
  EmailAsciiData();          // graphics.C and pcaSCommon.C
d144 1
a144 1
  //cerr << "Leaving email callback..." << endl;
d214 1
a214 1
  cout << "Entered quit callback..." << endl;
d218 1
a218 1
    cout << "PARENTID atoi  " << atoi(s)<< endl ;
d223 1
a223 1
    cout << " ppid = " << pid <<  endl ;
d229 1
a229 1
  cout << "Entered layer1Data callback..." << endl;
d235 1
a235 1
  cout << "Entered layer2Data callback..." << endl;
d241 1
a241 1
  cout << "Entered layer3Data callback..." << endl;
d247 1
a247 1
  cout << "Entered layerAll callback..." << endl;
d253 1
a253 1
  cout << "Entered plotAll callback..." << endl;
d259 1
a259 1
  cout << "Entered linearFull callback..." << endl;
d265 1
a265 1
  cout << "Entered logFull callback..." << endl;
d282 1
a282 1
  cout << "Entered rawData  callback..." << endl;
d288 1
a288 1
  cout << "Entered computedData  callback..." << endl;
d294 1
a294 1
  cout << "Entered optX1  callback..." << endl;
d299 1
a299 1
  cout << "Entered optX2  callback..." << endl;
d304 1
a304 1
  cout << "Entered optX4  callback..." << endl;
d309 1
a309 1
  cout << "Entered wGaps callback..." << endl;
d314 1
a314 1
  cout << "Entered woGaps callback..." << endl;
d323 1
a323 1
  cout << "Entered popup callback..." << qPopup<< endl;
d331 1
a331 1
  cout << "Entered strip callback..." << qStrip<< endl;
@

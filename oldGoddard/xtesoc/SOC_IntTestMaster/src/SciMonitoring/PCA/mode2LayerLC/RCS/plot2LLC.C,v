head	6.3;
access;
symbols;
locks; strict;
comment	@ * @;


6.3
date	2000.06.18.18.31.09;	author jonv;	state Exp;
branches;
next	6.2;

6.2
date	98.10.09.13.23.22;	author jonv;	state Exp;
branches;
next	6.1;

6.1
date	98.05.21.18.19.42;	author jonv;	state Exp;
branches;
next	6.0;

6.0
date	97.06.20.17.17.15;	author jschwan;	state Exp;
branches;
next	;


desc
@contains graphics functions
@


6.3
log
@removed theInputId variable, which was set, but never used
added new functionality for saving individual PCU info with the
autosave data (tis was necessary after the demise of the propane
layer on PCU 0)
@
text
@// ----------------------------------------------------------------------------
// File Name   : plot2LLC.C 
// Subsystem   : SOC Science Monitoring - pcastd2LLC (Mode 2 Layer Light Curve) 
// Programmer  : Julie Schwan, Hughes STX/NASA GSFC
// Description :
// This has been included for use as a SOC Science Monitoring
// pcastd2LLC function definition list, where new and updated
// function implementations to the pcastd1LC process are added.
//
// Most of these functions are callbacks to the Main Menu
// items for the PCA Standard Mode 2 LC display (pcastd2LLC).
// This file is a collection of callback routines that
// accomodate TAE+ generated GUI (Graphical User Interface)
// functionalities.
//
// RCS: $Id: plot2LLC.C,v 6.2 1998/10/09 13:23:22 jonv Exp jonv $ 
// ----------------------------------------------------------------------------

// System header files
#include <unistd.h>
#include <assert.h>
#include <iostream.h>
#include <stdio.h>
#include <fstream.h>
#include <strstream.h>

// SOC and Local header files
#include <pcaSCommon.h>
#include "pan_pca2LLC.h"
#include <SciMonitor.h>
#include <SciDisplay.h>

#include <std2EdsPart.h>
#include <rw/bstream.h>
#include <sys/fcntl.h>
#include <Utilities.h>
#include <SOCbstream.h>


#include "auto_save_vars.h"

#include "Layer1Counts.h"

#define PCU_COUNT 6

const String layerName[4] = {"Layer1","Layer2","Layer3","LayerAll" };
static DoubleVec  areaLayer(4, 0.0)  ;
static DoubleVec  currentArea(4, 0.0)  ;
double lcMin[4], lcMax[4], lcArea[4];
static char  status[80] ;
static char  statusLabel[150] ;
static  int complete = False ;

const int maxN = 512 ;


int binIndex ;
static double  binTime ;
static int numData = 0, sumData= 0 ;
const int  std2XVals = 129 ;         
const int  fullXVals = 256 ;         

SOCbistream theSOCbis(STDIN_FILENO);
static EdsPartition edsPart;   
static pcaSCommon pcacom;
static double  theTime, prevTime;
static SOCTime2 socTime;

int nvals;
SciDisplay outDisplay("pcaSMaux ");
static double monMet ;
static int openf=FALSE ;
static double preTime ;

// Double buffers -- one for each layer
static DoubleArray xenonCountL(std2XVals, 3, PCU_COUNT, 0.0);            
static DoubleArray xenonCountR(std2XVals, 3, PCU_COUNT, 0.0);            
static DoubleVec xvals(maxN, 0.0);  // for athena plotters

static DoubleGenMat expandXenon(fullXVals, 3,  0.0);            
static DoubleGenMat layerCounts(std2XVals,3, 0.0);

// maxN---(512) buffered light Curve  Array data
static DoubleGenMat plotLC(maxN, 4 , 0.0);
static DoubleGenMat compLC(maxN, 4, 0.0 );
static int nLC  = 0;

//for saveAscii file---
static DoubleGenMat plotBuff(maxN, 4, 0.0 );
static String plotName[4] ;
 
// plot labels
static const char *titleXenon[2] = { "XworkXenon",
                                     "@@diamonds Good Xenon Light Curves @@diamonds"};
static char *sumSubtitle[5] = { 
                        "All PCUs Layer1",
                        "All PCUs Layer2",
                        "All PCUs Layer3",
                        "Sum All PCUs Layers",
                        "All Layers Individually"};
 
static String pcuColor[5] = { "green", "blue", "red", "black","cyan"};
//static String pcuColor[5] = {"green", "blue", "orange", "magenta", "cyan"};

// file name which becomes Subject heading for E-mail Ascii
const char *subject = "pcaStd2LayerLC.data"; 
const char *subjectPS = "pcaStd2LayerLC.ps";

// the user's e-mail address
extern char *mailAddr;  // pan_ownFile.cc

// Added a class to be able to keep track of individual PCU info in the
// autosave files.
// Also, there are some other variables needed along the way to 
// help compute the count rates.
static Layer1Counts layer1counts(maxN);
static DoubleGenMat pcu_save_layer(std2XVals, 5, 0.0);
static DoubleGenMat expand_save_layer(fullXVals, 5,  0.0);
static DoubleVec    pcu_save_data(5, 0.0);



//
// ***********************FUNCTION DEFINITIONS***********************
//

//
// Get Data for Save To File (ascii format) call in callbacks.C
// 
void dumpAscii(const char *fileName) {
   pcacom.saveAsciiOwnDir(fileName, plotBuff, numData, nLC, status, binTime, 2, plotName, 0);
}


//
// Get Data for E-mail ASCII call
//
void emailAsciiData() {
   //cout << "Entered emailAsciiData" << endl;
   pcacom.emailAscii(mailAddr, subject, plotBuff, numData, nLC, status, binTime, 2, plotName, 0);
   //cout << "Exited emailAsciiData" << endl;
}


//
// Get Data for E-mail PS call
//
void emailPSGraph() {
   //cout << "Entered emailPSGraph" << endl;
   pcacom.emailPS(mailAddr, subjectPS, monXenon->plotterWidget());
   //cout << "Exited emailPSGraph" << endl;
}


void setPlots(unsigned short * theData ) {

  //cerr << "setPlots> using theData pointer: " << (unsigned ) theData << endl;

  EdsPartition tmp = (EdsPartition) edsPart;
  std2EdsPart currentPart(tmp) ; 

  // reset the PCU_COUNT pcus
  int pcuid, layer;
  for( pcuid = 1; pcuid < 6 ; pcuid++ ) { // short to double conversion
    double *spectraL[3];
    double *spectraR[3];
    for( layer = 0; layer < 3; layer++ ) {
      DoubleVec pcuL(xenonCountL(RWAll, layer, pcuid-1 ));
      DoubleVec pcuR(xenonCountR(RWAll, layer, pcuid-1 ));
      spectraL[layer] = pcuL.data();
      spectraR[layer] = pcuR.data();
//    cerr << "Xenon pcu #" << pcuid << " Left" << pcuL << endl;
//    cerr << "Xenon pcu #" << pcuid << " Right" << pcuR << endl;
    } // set the double pointers
    currentPart.xenon(pcuid, theData, spectraL, spectraR);
  }

  // pickup last values of xenon and propane
  for( pcuid = 0; pcuid < 5 ; pcuid++ ) { // short to double conversion
    for( layer = 0; layer < 3; layer++ ) {
      xenonCountL(128, layer,pcuid ) = theData[4145 + (layer*2)  + 6*pcuid] ;
      xenonCountR(128, layer,pcuid ) = theData[4145 + (layer*2 + 1) + 6*pcuid];
    }
  }

//-----------------------------------------------------

//handle sum of all detectors

   addDetectors();
   computeSum() ;
   dataToArray() ;

 return;
} 

  
void addDetectors(){
  //cerr << " addDetectors entered -----"  << endl;
  int pcuid, layer ;

  for ( layer=0; layer<3; layer++){
    xenonCountL(RWAll, layer, 5 ) = 0.0;
    xenonCountR(RWAll, layer, 5 ) = 0.0;
  }

  for (layer=0; layer<3; layer++){
    for (pcuid =0 ; pcuid < 5 ; pcuid++){
      if( auto_save_option_on && (layer == auto_save_layer) ) {
        pcu_save_layer(RWAll, pcuid) = xenonCountL(RWAll, layer,pcuid) +
                                       xenonCountR(RWAll, layer,pcuid);
      }
      xenonCountL(RWAll,layer,5) += xenonCountL(RWAll, layer,pcuid);
      xenonCountR(RWAll,layer,5) += xenonCountR(RWAll, layer,pcuid);
    }
    layerCounts(RWAll,layer) = xenonCountL(RWAll,layer,5) +
                               xenonCountR(RWAll,layer,5) ;
  }
  return;
}


void computeSum() {

  // reset area under curves to 0 ;
  areaLayer(RWAll) = 0.0 ;

  // calculate areas under histograms
  for( int i = 0; i < 3; i++ ) {
    DoubleVec xLayer(layerCounts(RWAll,i)) ;
    pcacom.expand256 (expandXenon, xLayer, i) ;
    for (int j=chStart[0]; j<=chEnd[0]; j++ ) {
      areaLayer(i) += expandXenon(j,i ); 
    }
    areaLayer(3) += areaLayer(i) ;
    //cout << " i = "<< i << " areaLayer = " << areaLayer[i] << endl ;
  }

  if( auto_save_option_on ) {
    for(i=0; i<5; i++) {

      DoubleVec xLayer(pcu_save_layer(RWAll,i)) ;
      pcacom.expand256 (expand_save_layer, xLayer, i) ;
      pcu_save_data(i) = 0;
      for (int j=chStart[0]; j<=chEnd[0]; j++ )
        pcu_save_data(i) += expand_save_layer(j,i);
    }
    layer1counts.addData(theTime, pcu_save_data);
  }

  return;
} 
 
//-----------------------------*
void dataToArray() {
 
   static int index=0 ;
   static int saveIndex=0 ;
   static int oldFactor = 1 ;
   static int binNeed = 1 ;
   static double binsize =16.0  ;
   static int currentN = 0   ;
   static double boundTime = 0.0 ;
   int nGap  = 0 ;
   int nMove = 0 ;
   static int iGap[maxN];
 
   if (compressFactor <=0) compressFactor = 1 ;
   if (compressFactor != oldFactor || scClear >= 1) {
      initialize() ;
      index = 0 ;
      saveIndex = 0 ;
      binsize = 16.0 * compressFactor ;
      boundTime = 0.0 ;
      for( int k =index;  k< maxN ; k ++) {
         iGap [k] = 0 ;
      }
      oldFactor = compressFactor ;
      currentN = 0 ;
      scClear = 0 ;
      nGap = 0 ;
      currentArea(RWAll) = 0.0;
   }


   if (boundTime <= 0.0 ) {
      boundTime = theTime + binsize  ;
      if (compressFactor <= 1) boundTime = theTime  ;
   }
   if (theTime <  boundTime){
      // add data to the holding bin
      currentArea(RWAll) += areaLayer(RWAll);
      currentN ++ ;
      complete = False ;
      //cerr <<" currentN  complete     =  " << currentN << "  " <<complete<< endl ; 
      return ;
   }  
//------------------------------------------
   if (gapShow==0 && boundTime != 0) {
      nGap = (int) ( (theTime - boundTime) / binsize ) ;
      //cerr << " time diff  =  " << theTime-boundTime<< " nGap= " << nGap<< endl  ;
   }

   index = index + nGap ;
   //cerr << " next data  index = " << index << endl ;
   if (index >  (maxN-1)) {
      //nMove = nGap + 1;
      nMove =  index - (maxN - binNeed) ;
      //cerr << " index to  move  =  " << nMove<< endl  ;
      if (nMove < maxN) {

         for(int k =0;  k<  maxN- nMove ; k ++) {
            compLC(k,RWAll )=compLC(k+nMove,RWAll) ;
            iGap[k] = iGap[k+nMove] ;
         } 
         index = maxN-nMove ;
      }  
      else {
         index = 0 ;
      }  

      //cerr << " index to start zeros =  " << index << endl  ;
      for(int k =index;  k< maxN  ; k ++) {
         compLC(k, RWAll )=0.0 ;
         iGap[k] = 0 ;
      } 
 
      index = maxN - 1 ;
      saveIndex -=nMove ;
      //cerr << " to add data--- index = "<<index <<" saveIndex = "<<saveIndex<< endl ;
   }

//------------------------------------------
   if (compressFactor <= 1  ) {                                 
      binIndex = index  ;
      binTime = theTime ;
 
      compLC(index, RWAll ) = areaLayer(RWAll) ;
      iGap[index] = 1 ;
      complete = True ;
   }
   else {
      //cerr << " --- theTime boundTime ---  =  " << (int)theTime << "  " <<(int)boundTime<< endl ; 
      if (currentN < compressFactor) {
         currentArea(RWAll) =(currentArea(RWAll)/currentN)*(float)compressFactor;
         //cerr << " ---currentArea calculated   =  " << currentN <<  endl ; 
      }

      if(saveIndex >= 0) {
         binIndex = saveIndex ;
         binTime = boundTime-binsize ;
         //cerr <<" to add data index = "<<binIndex <<" binTime = "<<(int)binTime<< endl ;
 
         compLC(binIndex, RWAll ) = currentArea(RWAll) ;
         iGap[binIndex] = 1 ;
         complete = True ;
      } 
      else {
         complete = False ; //LC update is postponed until next data point
      }

      currentArea(RWAll) = areaLayer(RWAll);
      currentN = 1 ;
                                    
   }
//------------------------------------------
      index++ ;
      numData = index ;
      sumData = 0 ;
      for (int n = 0; n < index; n++) {
          if (iGap[n] >= 1) sumData++ ;
      }
      //cerr << " numData,  sumData           =  " << numData << "  " <<sumData<< endl ; 

      saveIndex = index ;
      boundTime += (binsize * (nGap+1)) ;
      //cerr << "  next boundTime  =  " << (int)boundTime<< endl  ;
                                    
   return;
 
}

void selectRates() {

//cerr << " selectRates entered -----"<<  endl;

// first clear plotLC array

  for (int j =0;  j< 4  ; j ++ ) {
     plotLC(RWAll, j)  = 0.0 ;
  }
//load the data
 
  switch(selLayer) {
       case 0: // layer1 data      
            {
               plotLC(RWAll,0)  = compLC( RWAll,0) ;
            }
               break;
       case 1: // layer2 data                         
            {
               plotLC(RWAll, 0)  = compLC( RWAll,1) ;
            }
               break;
       case 2: // layer3 data                       
            {
               plotLC(RWAll, 0)  = compLC( RWAll,2) ;
            }
               break;
       case 4: //  all 4 plots
            {
               for (j =0;  j< 4  ; j ++ ) {
                  plotLC(RWAll, j)  = compLC(RWAll, j ) ;
               }
            }  
               break;
 
       case 3: // default to sum of xenon all pcus
 
       default: 
             {
               plotLC(RWAll, 0)  = compLC( RWAll,3) ;
             }
            break;
   }
     
//cerr << " selectRates exited  -----"<<  endl;
 
   return ;
}
 



void initialize() {
//cerr << " initialize entered -----" << endl;

  compLC(RWAll,RWAll) = 0.0;
  return ;
}

void computeArea() {

// calculate area, minimum and maximun of each light curve

  for( int i = 0; i < 4; i++ ) {
    DoubleVec tmp1(compLC(RWAll, i ));
    lcArea[i] = sum(tmp1) ;
    lcMin[i] = minValue(tmp1) ;
    lcMax[i] = maxValue(tmp1) ;
  }

   return ;
}

                                                                         
void resetYaxis()
{
//cerr << " resetYaxis  entered -----"<< selYaxis  << endl;
 

  switch(selYaxis) {
   case 1: // log in zeroMin scale
 
      {  
            double maxy = computeYmax1();
            double ymin = 0.1 ;
 
            XtVaSetValues(monXenon->yaxisWidget(),
                XtNautoScale, False, XtNmin, &ymin, XtNmax, &maxy,
                XtNaxisTransform, AtTransformLOGARITHMIC, NULL);
       } 
            break;
    case 2: // linear in own scale  
       { 
            double maxy = ownMax ;
            double ymin = ownMin ;

            XtVaSetValues(monXenon->yaxisWidget(),
                XtNautoScale, False, XtNmin, &ymin, XtNmax, &maxy,
                XtNaxisTransform, AtTransformLINEAR, NULL);
       } 
            break;
    case 3: // log in own scale
            //-------auto scale doesn't convert min=0 to min=0.1--------
       { 
            double maxy = ownMax ;
            double ymin = max(ownMin, 0.1) ;
 
            XtVaSetValues(monXenon->yaxisWidget(),
                XtNautoScale, False, XtNmin, &ymin, XtNmax, &maxy,
                XtNaxisTransform, AtTransformLOGARITHMIC, NULL);
       } 
            break;
 
    case 0: // linear in zeroMin scale -- this is default
       { 
            double maxy = computeYmax1();
            double ymin = 0.0 ;
 
            XtVaSetValues(monXenon->yaxisWidget(),
                XtNautoScale, False, XtNmin, &ymin, XtNmax, &maxy,
                XtNaxisTransform, AtTransformLINEAR, NULL);
       } 
 
            break;
    default: //
            break;
  }
  return ;
}
double computeYmax1() {
//cerr << " computeYmax1 entered-----"<<  endl;
  double ymax = 0.0 ;
  if (selLayer >= 4) {
     DoubleVec tmp1(plotLC( RWAll, 3));
     ymax = maxValue(tmp1) ;
  }
 
  else {
     DoubleVec tmp1(plotLC(RWAll, 0 ));
     ymax = maxValue(tmp1) ;
  }
    return ymax ;
}
 
 
void noZeros() {
 
  for( int i = 0; i < 4; i++ ) {
     for( int j = 0; j <  maxN ; j++ ) {
        if(plotLC(j,i) <= 0.0) plotLC(j, i) = 0.1;
     }
  }
  return ;
}
 
//-------------------------------------**








void attachXenon() {
//cerr << "Entered attachXenon..." << endl;
//cerr << "selected Layer     ..." <<selLayer << endl;

  static int colorMap =  1;
  cerr.setf(ios::scientific,ios::floatfield);
 
  monXenon->resetTitle(sumSubtitle[selLayer]) ;
 
  XtVaSetValues(monXenon->x2axisWidget(), XtNlabel, statusLabel,
                XtNlabelSize, AtFontMEDIUM, XtNlabelColor, colorMap, NULL);
 
 
       pcacom.supressAxis2(monXenon) ;
 
       XtVaSetValues(monXenon->y2axisWidget(), XtNmirror, True,
                  XtNlabel, "", XtNdrawGrid, False, NULL) ;
 
// Attach the application data to the plot children, xenon layer 1-3  

  if(selLayer >= 4) {
     for( int i = 0; i < 4; i++) {
        XtVaSetValues(monXenon->lineWidget(i),
         XtVaTypedArg, XtNforeground, XtRString, pcuColor[i], 1+strlen(pcuColor[i]),
         NULL);

        DoubleVec pcu(plotLC(RWAll,i));
        if((selYaxis ==2 || selYaxis == 3) && setOwn>=1) {
           pcu = pcacom.setOwnmax(pcu, ownMax, numData) ;
        }
        monXenon->drawLine(&xvals, &pcu, i, numData);
 
      //load plotBuff array
        //cout << "Setting the plot buffer now (first code occurrence" << endl;
        plotBuff(RWAll,i) = plotLC(RWAll, i) ;
        plotName[i] = layerName[i] ;
     }
        nLC = 4 ;
   
  } 
  else{
     for( int i = 0; i < 4; i++) {
        XtVaSetValues(monXenon->lineWidget(i),
         XtVaTypedArg, XtNforeground, XtRString, pcuColor[selLayer], 
          1+strlen(pcuColor[selLayer]), NULL);
        DoubleVec pcu(plotLC(RWAll, 0));
        if((selYaxis ==2 || selYaxis == 3) && setOwn>=1) {
           pcu = pcacom.setOwnmax(pcu, ownMax, numData) ;
        }
        monXenon->drawLine(&xvals, &pcu, i, numData);
     }
       //load plotBuff array
       // cout <<"Setting the plot buffer now (second code occurrence" << endl;
        plotBuff(RWAll,0) = plotLC(RWAll, 0) ;
        plotName[0] = layerName[selLayer] ;
        nLC = 1 ;
  }
 
       
//  double *tmp = pcu.data();
//    for( int i = 0; i < xvals.length() ; i++ )
//      cerr << "Index " << i << " Value: " << *tmp++ << endl;
//    cerr << "pcuid= " << pcuid << " Index " << 0 << " Value: " << tmp[0] <<
// Index " << xvals.length()-1 << " Value: " << tmp[xvals.length()-1] << endl;
 
  //cerr << "Exit attachXenon..." << endl;
  return;
}






void readData() {

  static double last_autosave_time = 0;

  unsigned short* unpackedData= NULL ;
  static int first = TRUE ;

  if   ( !theSOCbis.eof() ) {

  // first reset the cursor
  setCursor(pca2LLCP);
  //cerr << "readData> attempt to restore partition..." << endl;

  prevTime = theTime ;

  theSOCbis >>  edsPart ;
  double atime= edsPart.partStartTime();
  //cerr << "check 3--- currenttime = " <<  (int)atime    << endl;

  //check if this is a old data ?
  if(atime > prevTime){

     theTime = atime ;

    //cerr << "check 3--- currenttime = " <<  (int)theTime    << endl;
    //***********************************

    if (openf) {
        unsigned int np;
        np = 1;
        DoubleVec x(np,0.0);
        DoubleVec v0(np, 0.0);
        if(!first) v0 = theTime- 16.0-preTime ;
        DoubleGenMat dm(np, 1, 0.0);
        dm.col(0) = v0;
        cerr <<"ouput double-matrix rows, cols: " <<dm.rows()<< " , "
              <<dm.cols() <<endl;
        outDisplay.send(dm, monMet+100.0);
    }
 
    preTime= theTime ;
    first = FALSE ;

    //*********************************

    unpackedData = edsPart.getData();

    //cerr << "check 4-- unpacked data = " << (unsigned)unpackedData << endl;

    if( isFrozen(pca2LLCP) ) { 

      // for now just empty the pipe and discard the partition
      cerr << "Frozen! Ignore new partition..." << endl;
      delete unpackedData ;
      unpackedData = NULL;

    } else if( unpackedData != NULL ) {
      setPlots(unpackedData);

      // free the partition's data buff
      delete unpackedData;

      // if we get here, re-draw, attaching the data to 
      // the plot widget makes it visible
      //  XBell(XtDisplay(monSpectraL[0]->plotterWidget()), 1);

      // select data  to be plotted.

      if (complete) {

         selectRates() ;

         computeArea();

         // handle yaxis "log" or "linear"

         if(selYaxis == 1 || selYaxis == 3) noZeros() ;
         resetYaxis() ;    

         updateStatus();
         attachXenon();


         // output the data periodically if autosave is on 888
         if (auto_save_option_on) { 
           // cout << "Auto save IS ON!!!" << endl;
           // cout << "atime " << (int) atime << endl;
           // cout << "last save time " << (int) last_autosave_time << endl;
           // cout << " minutes_between * 60 = secs bet = " <<  
           //            (auto_save_minutes_between_saves * 60.0) << endl;
           // cout << " auto save file name " << auto_save_filename << endl;
           if(last_autosave_time == 0) last_autosave_time = atime;

           if ( (atime - last_autosave_time) > 
                         auto_save_minutes_between_saves * 60.0 ) {


              last_autosave_time = atime;

              SOCTime2 time_stamp(atime);
              RWCString auto_save_fname(auto_save_filename);
              auto_save_fname += time_stamp.asString();

              // debugging aids:
              // cout << "AUTO-SAVING TO FILE " <<
              //         auto_save_fname.data() << endl;
              layer1counts.dumpData(auto_save_fname.data());


            }
          }

      } //end of complete
    }  
 
  } //end of time checking
     
  // reset cursor
  unsetCursor(pca2LLCP);

 }  // end of if ( eof())

 return ;

}







void doGraphics(int init) {
  //cout << "  check----1 entered doGraphics "  << endl;
  // entry from callbacks:

  if( !init ) {

    // select data to be plotted.
    selectRates() ;

    computeArea();

    // handle yaxis "log" or "linear"

    if(selYaxis == 1 || selYaxis == 3) noZeros() ;
    resetYaxis() ;    

    updateStatus();
    attachXenon();

    //***********************************
    monMet = qPopup + qStrip ;
     
    if (monMet <= 0.0 ) {
      if (!openf) ;
      else {
        outDisplay.close () ;
        openf = FALSE ;
      }
    }
    else {
      if (!openf) {
        outDisplay.openStrip(101, "", "", "counter index", "MET diff") ;
        openf = TRUE ;
      }
      unsigned int np;
      np = 1;
      DoubleVec x(np,0.0);
      DoubleVec v0(np, 0.0);
      v0 = 0.0 ;
      DoubleGenMat dm(np, 1, 0.0);
      dm.col(0) = v0  ;
      cerr <<"ouput double-matrix rows, cols: " <<dm.rows()<< " , "
           <<dm.cols() <<endl;
      outDisplay.send(dm, monMet+100.0 + 1000.0);
    }
    //*********************************

    return;
  }

  // entry from main:
 
  //monXenon = new SciMonitor(pca2LLCP, titleXenon[0], titleXenon[1], 16, -24,
  monXenon = new SciMonitor(pca2LLCP, titleXenon[0], titleXenon[1],  8, -12,
                            "","Bin Numbers", 8,
                            "Counts", 16, 5, 0);
  cerr << "doGraphics> created monXenon: " << (int ) monXenon << endl;
 
  for( int i = 0; i < 5; i++) {
 
    XtVaSetValues(monXenon->lineWidget(i), XtVaTypedArg, XtNforeground,
                  XtRString, pcuColor[i], 1+strlen(pcuColor[i]), NULL);
  }


  //-----------------------------------***

  // don't forget to set  the xvals array:
  for( i = 0; i <maxN ; i++ ) {
    xvals[i] = i ;
  }
  // give the initial values of the color band boundary

  if (!auto_save_option_on) {
 
    chStart[0] =  0   ;
    chEnd[0] =  255 ;
//  nBand = 1 ;
    selLayer = 3; // this should be the default start-up

  } else {

    // set plot parameters for autosave during startup
    chStart[0] =  auto_save_min_channel;
    chEnd[0]   =  auto_save_max_channel;
    selLayer   =  auto_save_layer;

  }
  
  // establish pipe i/o handler
  monXenon->setIOhandler(readData, theSOCbis.fd());

  return;
}


void updateStatus () {

   String area = " total cnts = ";
 
// check statistics option
      if (compressFactor <= 0) compressFactor = 1 ;
      double binsize = 16.0 * compressFactor ;
      double totalTime = binsize * numData ;
      double totsumTime = binsize * sumData ;
    //double totalTime = binsize * 1024 ;
 
   if ( selStat <=0) {
      area = " cnts/s = " ;
      for ( int i = 0; i<4 ; i++ ) {
          lcMin[i] = lcMin[i] / binsize ;
          lcMax[i] = lcMax[i] / binsize ;
          lcArea[i] = lcArea[i] / totsumTime ;
          lcMin[i] = ((int)(lcMin[i]*100)) / 100.0   ;
          lcMax[i] = ((int)(lcMax[i]*100)) / 100.0   ;
          lcArea[i] = ((int)(lcArea[i]*100)) / 100.0      ;
      }     
   }
  
   ostrstream stat_os ;
   stat_os << " binsize = " << binsize<<" sec.  "<<"compression factor = "<<compressFactor ;
   strcpy(status, stat_os.str()) ;
   delete  stat_os.str() ;
   //strcat(status,"\0");
 
  
// update dynamic text field
 
   socTime = SOCTime2(binTime); // current  time
   RWCString tmp = ((RWTime)socTime).asString('\0',RWZone::utc());
   //RWCString tmp = ((RWTime)socTime).asString();

   ostrstream stLabel_os ;
   stLabel_os << " UT = " << tmp.data()
       << "  (MET = " << (int)binTime << ")"<<"  index bin #  = "
       << binIndex <<"   binsize = "
       << binsize<<" sec  "<<" total time = "<<(int)totalTime<<" sec" 
       << " (ch# "<<chStart[0] <<" - "<<chEnd[0]<<")" ;
   strcpy(statusLabel, stLabel_os.str()) ; //used as a x2axis label
   delete stLabel_os.str() ;
  
   
   ostrstream pcu1_os ;
   pcu1_os << "L1 : "<<"  max = "<< lcMax[0]<<"  min =  "<<lcMin[0] <<area<<lcArea[0] ;
   TaeItem *layer1Item = pca2LLCP->GetItem("layer1stat");
   layer1Item->Update(pcu1_os.str()) ;
   delete pcu1_os.str() ;
 
 
   ostrstream pcu2_os ;
   pcu2_os << "L2 : "<<"  max = "<< lcMax[1]<<"  min =  "<<lcMin[1] <<area<<lcArea[1] ;
   TaeItem *layer2Item = pca2LLCP->GetItem("layer2stat");
   layer2Item->Update(pcu2_os.str()) ;
   delete pcu2_os.str() ;
 
   ostrstream pcu3_os ;
   pcu3_os << "L3 : "<<"  max = "<< lcMax[2]<<"  min =  "<<lcMin[2] <<area<<lcArea[2] ;
   TaeItem *layer3Item = pca2LLCP->GetItem("layer3stat");
   layer3Item->Update(pcu3_os.str()) ;
   delete pcu3_os.str() ;
 

   ostrstream pcuall_os ;
   pcuall_os << "L1+L2+L3 : "<<"  max = " << lcMax[3]<<"  min = "<<lcMin[3] << area  << lcArea[3] ;
   TaeItem *pcuallItem = pca2LLCP->GetItem("allstat");
   pcuallItem->Update(pcuall_os.str()) ;
   delete pcuall_os.str() ;
  
   return ;
}
@


6.2
log
@changed the autosaving so that even if the user picks a different layer
(or all layers), the autosave file will still only record the number
of layers requested in the autosave config file - this basically means
that the auto saved data is independent of the users selection of
layers to view on the screen (not that the user is not allowed to change
the channel boundaries) (that should be note on the previous line instead
of "not")
@
text
@d16 1
a16 1
// RCS: $Id: plot2LLC.C,v 6.1 1998/05/21 18:19:42 jonv Exp $ 
d42 1
d55 2
a62 1
static XtInputId theInputId;
d112 9
d159 2
a160 2
      EdsPartition tmp = (EdsPartition) edsPart;
      std2EdsPart currentPart(tmp) ; 
d162 1
a162 1
// reset the PCU_COUNT pcus
d178 1
a178 1
// pickup last values of xenon and propane
d180 4
a183 4
     for( layer = 0; layer < 3; layer++ ) {
        xenonCountL(128, layer,pcuid ) = theData[4145 + (layer*2)  + 6*pcuid] ;
        xenonCountR(128, layer,pcuid ) = theData[4145 + (layer*2 + 1) + 6*pcuid] ;
     }
d199 1
a199 1
//cerr << " addDetectors entered -----"  << endl;
d202 18
a219 14
       for ( layer=0; layer<3; layer++){
          xenonCountL(RWAll, layer, 5 ) = 0.0;
          xenonCountR(RWAll, layer, 5 ) = 0.0;
       }

     for (layer=0; layer<3; layer++){
       for (pcuid =0 ; pcuid < 5 ; pcuid++){
          xenonCountL(RWAll,layer,5) += xenonCountL(RWAll, layer,pcuid);
          xenonCountR(RWAll,layer,5) += xenonCountR(RWAll, layer,pcuid);
       }
          layerCounts(RWAll,layer) = xenonCountL(RWAll,layer,5) 
                                    +xenonCountR(RWAll,layer,5) ;
     }
   return;
d225 27
a251 14
     // reset area under curves to 0 ;
       areaLayer(RWAll) = 0.0 ;
  
      // calculate areas under histograms
       for( int i = 0; i < 3; i++ ) {
          DoubleVec xLayer(layerCounts(RWAll,i)) ;
          pcacom.expand256 (expandXenon, xLayer, i) ;
          for (int j=chStart[0]; j<=chEnd[0]; j++ ) {
              areaLayer(i) += expandXenon(j,i ); 
          }
          areaLayer(3) += areaLayer(i) ;
          //cout << " i = "<< i << " areaLayer = " << areaLayer[i] << endl ;
       }
   return;
a629 1

a677 1

d704 1
a704 1
         // output the data periodically if autosave is on
d712 2
a716 25
              // added 9/29/1998 to prevent auto save files from having
              // incorrect formats:
              // Before doing the auto save, force the display to 
              // the autosave settings in case someone has unwittingly 
              // changed them.
              // Note that channel changes are not allowed, so only the
              // layer info needs to be changed and restored.
              int keep_layer;

              keep_layer = -255; // This is the flag to say that
                                 // the displayed layer is the same as the
                                 // autosave layer.

              if ( selLayer != auto_save_layer)  {

                // if we get in here, the graphics settings have 
                // been changed, so we'll need to change them back after
                // the auto save file has been written.
                keep_layer = selLayer;

                // force plot parameters to autosave values
                selLayer   =  auto_save_layer;

                doGraphics(FALSE); // force the update
              }
d727 1
a727 2

              dumpAscii(auto_save_fname.data());
a729 8
              if ( keep_layer != -255 ) {
                // the plot was changed before doing auto save, so
                // restore plot parameters to original values
                selLayer   =  keep_layer;

                doGraphics(FALSE); // update the screen to reflect the change
              }

d756 1
d772 2
a773 2
  //***********************************
  monMet = qPopup + qStrip ;
d775 3
a777 3
  if (monMet <= 0.0 ) {
     if (!openf) ;
     else {
d780 4
a783 4
     }
  }
  else {
     if (!openf) {
d786 13
a798 12
     }
        unsigned int np;
        np = 1;
        DoubleVec x(np,0.0);
        DoubleVec v0(np, 0.0);
        v0 = 0.0 ;
        DoubleGenMat dm(np, 1, 0.0);
        dm.col(0) = v0  ;
        cerr <<"ouput double-matrix rows, cols: " <<dm.rows()<< " , "<<dm.cols() <<endl;
        outDisplay.send(dm, monMet+100.0 + 1000.0);
  }
//*********************************
d801 1
a801 4
   }

// entry from main:
  theInputId = 0;
d803 1
d805 4
a808 4
 //monXenon = new SciMonitor(pca2LLCP, titleXenon[0], titleXenon[1], 16, -24,
   monXenon = new SciMonitor(pca2LLCP, titleXenon[0], titleXenon[1],  8, -12,
            "","Bin Numbers", 8,
            "Counts", 16, 5, 0);
d814 1
a814 1
		XtRString, pcuColor[i], 1+strlen(pcuColor[i]), NULL);
d818 1
a818 1
//-----------------------------------***
d820 1
a820 1
// don't forget to set  the xvals array:
d822 3
a824 3
     xvals[i] = i ;
   }
// give the initial values of the color band boundary
d826 1
a826 1
   if (!auto_save_option_on) {
d828 4
a831 4
     chStart[0] =  0   ;
     chEnd[0] =  255 ;
 //  nBand = 1 ;
     selLayer = 3; // this should be the default start-up
d833 1
a833 1
   } else {
d835 4
d840 4
a843 4
     // set plot parameters for autosave during startup
     chStart[0] =  auto_save_min_channel;
     chEnd[0]   =  auto_save_max_channel;
     selLayer   =  auto_save_layer;
a844 6
   }
  
// establish pipe i/o handler
    theInputId = monXenon->setIOhandler(readData, theSOCbis.fd());
 
 
@


6.1
log
@auto save feature added - plot characteristics set to
auto save defaults on start up (if desired) and
an ascii dump is done every so often (also settable in
a config file)
@
text
@d16 1
a16 1
// RCS: $Id: plot2LLC.C,v 6.0 1997/06/20 17:17:15 jschwan Exp $ 
d272 1
a272 1
      nGap = (theTime - boundTime) / binsize  ;
d689 26
d721 1
d727 9
d742 1
a742 1
  } //end if time checking
a845 7

     // if we call this here, there will be an infinite loop
     // because this also calls doGraphics ...
     //extern set_plot_chars_for_auto_save (const int, const int, const int);
     //     set_plot_chars_for_auto_save(
     //                auto_save_layer, 
     //                auto_save_min_channel, auto_save_max_channel);
@


6.0
log
@added email code
@
text
@d16 1
a16 1
// RCS: $Id$ 
d40 3
d144 1
a144 1
void setPlots(unsigned short * theData ) { 
d511 8
d552 1
d571 1
d589 4
d595 1
a595 1
// first reset the cursor
d602 2
d613 1
a613 1
//check if this is a old data ?
d615 1
d618 2
a619 2
  //cerr << "check 3--- currenttime = " <<  (int)theTime    << endl;
//***********************************
d621 1
a621 1
     if (openf) {
d629 2
a630 1
        cerr <<"ouput double-matrix rows, cols: " <<dm.rows()<< " , "<<dm.cols() <<endl;
d632 1
a632 1
     }
d634 22
a655 2
  preTime= theTime ;
  first = FALSE ;
d657 9
a665 1
//*********************************
d667 1
a667 1
  unpackedData = edsPart.getData();
d669 1
a669 1
  //cerr << "check 4-- unpacked data = " << (unsigned)unpackedData    << endl;
d671 2
a672 4
  if( isFrozen(pca2LLCP) ) { // for now just empty the pipe and discard the partition
    cerr << "Frozen! Ignore new partition..." << endl;
    delete unpackedData ;
    unpackedData = NULL;
d674 2
a675 3
  }
  else if( unpackedData != NULL ) {
    setPlots(unpackedData);
a676 4
    // free the partition's data buff
    delete unpackedData;
    // if we get here, re-draw, attaching the data to the plot widget makes it visible
//  XBell(XtDisplay(monSpectraL[0]->plotterWidget()), 1);
d678 10
a687 1
// select data  to be plotted.
d689 1
a689 2
    if (complete) {
       selectRates() ;
d691 3
a693 1
       computeArea();
d695 2
a696 1
       // handle yaxis "log" or "linear"
d698 1
a698 2
       if(selYaxis == 1 || selYaxis == 3) noZeros() ;
       resetYaxis() ;    
d700 5
a704 4
       updateStatus();
       attachXenon();
     } //end of complete
  }  
d706 1
a706 1
 } //end if time checking
d708 1
a708 1
// reset cursor
d710 1
d712 1
d714 1
d717 6
d795 2
d803 16
a901 4
 



@

head	6.0;
access;
symbols;
locks; strict;
comment	@# @;


6.0
date	97.06.19.19.16.38;	author jschwan;	state Exp;
branches;
next	;


desc
@Test Plan for pcaSColor GUI
@


6.0
log
@added email code
@
text
@			Functional Test Plan
			--------------------

Subsystem: PCA Science Monitoring		Date: 6/19/97

Build: 5.4

Author: Julie Schwan				Site: SOF

RCS: $Id$

Test Name: Display standard mode 2 Color Spectral Science data from the display
and generate e-mail and save-to-file ascii and postscript files.

Test Goals/Objectives:

o Test the pcaSColor binary for e-mailing ascii and postscript data.

o Test the pcaSColor binary for saving ascii data to a file (only works if
  user has write permission to $SOCOPS/tmp or his/her own home directory).
  You can save postscript data in lieu of ascii data if you desire.
 
Requirements Verified:

[3.4.4] The SOC will carry out analyses of the portions of the science
data from the PCA and HEXTE instruments called "standard science data" (See
Section 2.7.2 of SWG-XTE-SOC-001) and the corresponding spacecraft and
instrument housekeeping data.  The "standard science data" is in a standard
format intended to provide a controlled comparison of the data on all sources.
A Set of analysis products suitable for application to all sources will be
carried out on the standard science data.  It will be called the "Standard
Analysis".  Products will be included in the data sets delivered to observers.

[3.4.5] Standard Analysis will include:
1. temporal records of housekeeping rates, orbital and spacecraft environment
parameters,
2. temporal records of selected science data (e.g. rates, histograms, measures
of spectral character, measures of variance),
3. selected science results (e.g., spectral fits, energy band fluxes, power
spectra).

[3.4.8] The SOC will maintain tools and information needed to process data
from the PCA, HEXTE, and the ASM acquired in all operating modes.


Test Description:

pcaSColor is a binary that displays standard mode 2 data by means of a
spectrum display.  The script, rtrunit, will automatically start
up the display using real-time data, which is what users will be using in
most cases.  the script, runit, will automatically start up the
display using a data file, SMode2.data.  You must rename
your apid 74 data file to this filename in order for this shell script
to execute properly.


Test Procedure:

To E-mail Data:

Select "Save" from the "File" menubar item.  A blue panel window will pop
up.  Click on the "E-mail Data" radio button and enter your e-mail address.
Then click on the "OK" button.  An E-mail of the spectral data should be
generated within at least one minute in real time mode (sometimes longer
if the xitserver is down).

To Save Data to a File:

Select "Save" from the "File" menubar item.  Click on the "Save To File" button
and enter a directory path and file name for your ascii-formatted data.


Expected Results:

You should get an E-mail with spectral data.
If you don't let the process collect data long enough, there will be
less than the maximum of data, so let it run until the entire display window
has data curves filling it up.

The same is true with the file data in both ascii and postscript formats.


Verification of Results:

If you get an E-mail file and a Saved file in a directory for respective
options selected, then the test has passed.  Else, if no emails or files
are generated, then the test has failed.
@

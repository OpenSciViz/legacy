head	4.2;
access
	jschwan
	soccm;
symbols
	Build5_3:4.2
	Build4:4.1
	Current:4.1;
locks; strict;
comment	@# @;


4.2
date	96.09.26.21.28.29;	author jschwan;	state Exp;
branches;
next	4.1;

4.1
date	94.12.21.22.58.46;	author dhon;	state Exp;
branches;
next	;


desc
@@


4.2
log
@added more XClient names to list.
@
text
@.\" ==========================================================================
.\" File Name   : scimon.1
.\" Subsystem   : Science Monitoring
.\" Programmers : Julie Schwan   Hughes STX / NASA GSFC
.\"               David Hon      Hughes STX / NASA GSFC
.\" Description : Manual page for the Science Monitoring real time
.\" server clients and how to use them.
.\" View with : nroff -man scimon.1 | more
.\" Print with: troff -man scimon.1 | dpost | /bin/lp
.\"
.\" RCS:  $Id: scimon.1,v 4.1 1994/12/21 22:58:46 dhon Exp jschwan $
.\" ==========================================================================
.TH scimon 1 "25 September 1996" "SciMonitoring" "User Commands"
.SH NAME
.BR hexteRtClient " or" " pcaRtClient"
- HEXTE or PCA Real-time Client which connects to the SOC "rtserver"
(real time server) and receives HEXTE or EDS/PCA housekeeping and
science packets to create HEXTE or EDS/PCA "partitions".  The
executables are
.B hexteRtClient
and
.B pcaRtClient
for HEXTE and EDS/PCA data, respectively. 
.sp
.br
HEXTE or PCA Xdisplays - XWindow GUIs (based on Tae+ and Motif) 
specialized for each of the "standard modes" of each instrument.
For HEXTE, these include:
.BR hexteScM1 " (HEXTE Event and Burst Lists), "
.BR hexteScM2 " (HEXTE Histogram and Multiscalar Binning), "
.BR hexteArchSpectra " (HEXTE Archive Histogram), "
.BR hexteArchLiteCrv " (HEXTE Archive Multiscalar Binning), "
.BR HEXTErock " , "
.BR HexteMMclient " , "
.BR go2hexte " , "
.BR HxArchCal " (HEXTE Archive Calibration), and "
.BR HxHnSDisplays " (HEXTE Health and Safety)."
For PCA:
.BR pcaSHist " (EDS/PCA Spectral History), "
.BR pcaStd1LeahyHist " (PCA Leahy Power History), "
.BR pcaStd1fft " (PCA Standard 1 Mode Fast Fourier Transform), "
.BR pcaSM " (PCA Science Monitoring Top-level GUI), "
.BR pcaSMaux " (PCA Science Monitoring Auxiliary), "
.BR pcastd1LC " (PCA Standard 1 Mode Light Curve), "
.BR pcastd1CalS " (EDS/PCA Standard Mode 1 Calibration Spectra), "
.B pca1FFT
(Power Spectra calculated (using FFT) from PCA
Standard Mode 1 Light Curve data as inputs.  Results are normalized
using the Leahy method.), 
.BR pcastd2 " (EDS/PCA Standard Mode 2 Spectra), "
.BR pca2Rate
(EDS/PCA Science Monitoring display client for Standard Mode 2 Rates),
.BR pcaSBase
(EDS/PCA Standard Mode 2 Xenon spectra, summed over
all 3 layers and all 5 detectors.  This is Base mode of Spectral
Science, a variation of the Standard Mode 2 monitoring.),
.BR pcaSColor
(EDS/PCA Standard Mode 2 Xenon spectra, summed over all 3 layers and
all 5 detectors.  This is 'Colors vs. Time' mode of Spectral Science,
a variation of the Standard Mode 2 monitoring.), and 
.BR pcaSRecom
(EDS/PCA Standard Mode 2 Xenon spectra, summed over all 3 layers and
all 5 detectors.  This is Recommend mode of Spectral Science,
a variation of the Standard Mode 2 monitoring.).
.sp
.br
.B smGeneric
- General purpose XWindow GUI (based on Tae+ and Motif) that supports
plotting/displaying any data that is supplied to it in the form of a
RogueWave Math++ (persistent) class. This is supported by the class
SciDisplay. Any application (real-time or not) can instance a SciDisplay
class, along with a RWMath matrix class, and supply this GUI with
information via the "SciDisplay::send()" method. Each column in the
matrix will be treated as a curve. Up to 16 curves can be plotted at
a time, each with 10(min) to 16,000(max) elements. This is supported
by the SciMonitor class. The smGeneric supports static plotting as well
as dynamic strip charting.
.sp
.br
.B starTracker
- Specialized XWindow GUI that displays the star tracker
telemetry in either a "scatter-plot" cross-hair image via the XScatter
class, or a "strip-chart" via the SciDisplay class (smGeneric).
.\" ==========================================================================
.SH SYNOPSIS
.\" ==========================================================================
.B pcaRtClient
.RB [ -host
.IR hostname ]
.RB [ -name
.IR client-name ]
.RB [ -display-client
.IR Xclient-name ]
.sp
.br
.B hexteRtClient
.RB [ -host
.IR hostname ]
.RB [ -name
.IR client-name ]
.RB [ -display-client
.IR Xclient-name ]
.\" ==========================================================================
.SH DESCRIPTION
.\" ==========================================================================
The \*r processes combine "partition" servers with XWindow GUIs to perform
the task of accumulating packets and displaying their contents in meaningful
ways in near real-time. In this scheme, there is no mingling or combining of
PCA and HEXTE data within the same process (or window).  The hexteRtClient
process is a SOC "rtclient" which connects to the SOC "rtserver" and receives
HEXTE packets (ApIds 80, 83, 86, and 89 ONLY).  The pcaRtClient connects to
the rtserver and receives EDS EA 2-4 and 5-7 and PCA packets (ApIds 52-63,
68-79, and 90-94 ONLY).  Partitions constructed by each rtclient can be sent
directly to a GUI.  For example the hexteRtClient can feed the hexteArchSpectra
GUI "raw" HEXTE housekeeping partitions.
.\" ==========================================================================
.SH OPTIONS
.\" ==========================================================================
.TP
.BR -house " or" " -h"
Process housekeeping data (HEXTE and PCA).
.TP
.BR -science " or" " -1" " or" " -2"
Process any HEXTE science or EDS standard 1 or standard 2 science data.
.TP
.B -ascii
Ascii dump each raw partition (no further processing).
.TP
.BR -display-client " or" " -display"
Send raw partition to GUI or send Augmented partition to GUI.
.TP
.BR -read-from-stdin " or" " -stdin"
Restore packets from stdin or restore partitions from stdin.
.TP
.B -stdout
Send augmented partitions to stdout.
.TP
.B -mm
Connect to MissionMonitoring at this node, for mearsured timeline services.
.TP
.B -host
Connect to rtserver at this node, for packets.
.\" ==========================================================================
.SH AUTHOR
.\" ==========================================================================
Julie Schwan   Hughes STX / NASA GSFC  jschwan@@xema.stx.com
.\" ==========================================================================
.SH SEE ALSO
.\" ==========================================================================
.B MeanAttPos EarthMoonSun ExpectCounts SciMonitor SOCbstream SciCommon
.B SciXCMap SciXImage SciDisplay Desired-Timeline Predicted-Timeline
.B Measured-Timeline PktTlm EdsPartition PcaHkPartition HxHk16Part
.B HxScPart XScatter starTracker
.\" ==========================================================================
.SH BUGS
.\" ==========================================================================
None.
.\" ==========================================================================
.SH HISTORY
.\" ==========================================================================
Build Version 5.3, September 1996
@


4.1
log
@*** empty log message ***
@
text
@d1 13
a13 9
.de Id
.ds Rv \\$3
.ds Dt \\$4
..
.Id $Id$
.ds r \&\s-1Science-Monitoring\s0
.if n .ds - \%--
.if t .ds - \(em
.TH Science-Monitoring intro. Dec.\ 1994 SOC \*(Dt SOC
d15 12
a26 13
.TP
(hexte/pca)RtClient \- Connects to the SOC "rtserver" and receives 
HEXTE/(EDS&PCA) housekeeping and science packets to create 
HEXTE/(EDS&PCA) "partitions".
.TP
(hexte/pca)Ancil \- Receives intact partitions from the associated
RtClient and augment the instrument data with ancillary data selected
from either the predicted time-line or the measured time-line (whichever
is available), the mission planning database, and the S/C housekeeping 
packets. Also performs additional quality checks on the data,
including flags for "duplicate" or "stale" (out of time-order) partitions.
.TP 
pca/hexte Xdisplays \- XWindow GUIs (based on Tae+ and Motif) 
d28 47
a74 8
.TP
smGeneric \- General puporse XWindow GUI (based on Tae+ and Motif)
that supports plotting/displaying any data that is supplied to it in the
form of a RogueWave Math++ (persistent) class. This is supported by the
class SciDisplay. Any application (real-time or not) can instance a
SciDisplay class, along with a RWMath matrix class, and supply this GUI
with information via the "SciDisplay::send()" method. Each column in 
the matrix will be treated as a curve. Up to 16 curves can be plotted at
d78 7
a84 10
.TP
SciAncilPanel \- Specialized XWindow GUI that displays the 
ancillary information gathered by the (hexte/pca)Ancil process.
This is supported by the SciAncilDisplay class.
.TP
starTracker \- Specialized XWindow GUI that displays the star 
tracker telemetry in either a "scatter-plot" cross-hair image 
via the XScatter class, or a "strip-chart" via the SciDisplay
class (smGeneric).
.B
d86 18
a103 13
.TP 
.RI "pcaRtClient [-ancil] [-ascii] [-display-client a]
[-read-from-stdin]  \|.\|.\|.
.TP
.RI "hexteRtClient [-ancil] [-ascii] [-display-client a]
[-read-from-stdin]  \|.\|.\|.
.TP
.RI "pcaAncil [-h] [-1] [-2] [-mm foo] [-host bar] [-stdin]
[-stdout] [-display a] [-display b] " \|.\|.\|.
.TP
.RI "hexteAncil [-house] [-science] [-mm foo] [-host bar]
[-stdin] [-stdout] -display a [-display b]  \|.\|.\|.
.B
d105 12
a116 20
.B 
The \*r processes combine "partition" servers with XWindow GUIs to 
perform the task of accumilating packets and displaying their contents
in meaningful ways in near real-time. In this scheme, there is no
mingling/combining of pca and hexte data within the same process (or window).
The hexteRtClient process is a SOC "rtclient" which connects to the 
SOC "rtserver" and receives HEXTE packets (ApIds 80, 83, 86, and 89 ONLY).
The pcaRtClient connects to the rtserver and receives EDS EA 2-4 and 5-7
and PCA packets (ApIds 52-63, 68-79, and 90-94 ONLY).
.SH
Partitions constructed by each rtclient can be sent directly to a 
GUI or indirectly via the corresponding instr. Ancil process. For example
the hexteRtClient can feed the hexteArchSpectra GUI "raw" hexte
housekeeping partitions or it can feed hexteAncil. In the latter,
hexteAncil will then supply (one or more) GUI(s) with "augmented"
partitions. Each (pca/hexte)Ancil process can support up to 8 GUIs
that expect to recieve THE SAME KIND OF PARTITION.
.B

.B
d118 1
d120 2
a121 5
.B \-house or \-h
Process housekeeiping data (either instrument).
.TP
.B \-science or -1 or -2
Process any hexte science or EDS standard 1 or standard 2 science data.
d123 2
a124 2
.B \-ancil
Send raw partitions to coreesponding instr. Ancil process.
d126 1
a126 1
.B \-ascii
d129 1
a129 1
.B \-display-client or \-display
d132 1
a132 7
.B \-science or -1 or -2
Process any hexte science or EDS standard 1 or standard 2 science data.
.TP
.B \-ancil
Send raw partitions to corresponding instr.Ancil process.
.TP
.B \-read-from-stdin or \-stdin
d135 1
a135 1
.B \-stdout
d138 1
a138 1
.B \-mm
d141 1
a141 1
.B \-host
d143 1
a143 2

.B
d145 3
a147 2
Author: David B. Hon, Hughes/STX, dhon@@xero.stx.com
.B
d149 6
a154 9
.B
SciAncillary SciAncilBits MeanAttPos EarthMoonSun ExpectCounts
SciMonitor SOCbstream SciCommon SciDisplay SciAncilDisplay
Desired-Timeline Predicted-Timeline Measured-Timeline
PktTlm EdsPartition PcaHkPartition HxHk16Part HxScPart
XScatter starTracker
.br
.B
.B
d156 6
a161 73
Far too numerous to mention here.
.br
.SH TEST DESCRIPTION:
.B
.SH Test Number: 0	Build: 4
.SH Test Name: Science Monitoring Simple
.SH Requirements Verified: 
.B
Can it do anything useful?
.B
.SH Test Descrption:
.B
Basic functionality - 
.TP
1. Creation of raw partitions from packets.
.TP
2. Display of raw parititions.
.TP
3. Creation of augmented partitions from raw partitions.
.TP
4. Display of augmented partitions.
.B 
.SH External Elements:
.B
A self consistent SOC database - 
.TP
0. Raw Packet files (as deliverd by rtingest).
.TP
1. Mission Planning weekly schedule and desired timeline.
.TP
2. Command Generation predicted timeline.
.TP
3. Mission Monitoring measured timeline server.
.B
.SH Procedure:
.B
Make sure that $SOCHOME/bin/$ARCH is set in your path. Also be sure to 
set SOCOPS to an appropriate area. In the SOCOPS area there should be 
the following subdirectories:
.TP 
$SOCOPS/ingest/realtime/apidnnnn/daynnnn/* 
with packets for all the relevant apids.
.TP 
$SOCOPS/planning/Weeknnn/ExpectCounts 
with appropriate observations IDs
.TP 
$SOCOPS/commanding/ptl/daynnnn/* 
with corresponding predicted timelines.
.TP
If rtingest, rtserver, and Mission Monitoring are NOT running,
test with stdin and stdout by doing the following, 
hexte is the example:
.TP 
cat $SOCOPS/ingest/realtime/apidnnnn/daynnnn/* |
hexteRtClient -read-from-stdin -ancil | 
hexteAncil -stdin 
.SH
In the (eventual) case where all subsystems are running on their nominal
nodes, the hexteAncil process will bring-up the hexteRtClient process
and the display GUI(s) and the rtserver and Mission Monitoring server 
connections will be automatic:
.TP
hexteAncil -house -display hexteArchSpectra
.B
.SH Expected Results:
.B
GUIs should display a variety of plots and status strings.
.B 
.SH Verification of Results: 
.B
Are the count rates correct?


@

head	1.1;
access
	soccm
	tgasaway;
symbols
	Build4_1:1.1;
locks; strict;
comment	@# @;


1.1
date	95.02.27.03.08.20;	author rbentley;	state Exp;
branches;
next	;


desc
@histogram bin mode display client man page
@


1.1
log
@Initial revision
@
text
@.\"
.\"  $Id: hexteScM2.man1,v 1.1 1994/05/16 04:39:37 rbentley Exp $
.\"
.TH hexteScM2 1 "26 February 1995"
.SH NAME
hexteScM2 \- HEXTE Science Monitoring display client for Histogram Bin Mode (energy spectra
at 1s to 16s intervals) and Multiscalar Bin Mode (light curves in 1 to 8 spectral bands)
.SH SYNOPSIS
\fBhexteRtClient\fP  \fB-display-client hexteScM2\fP [\fB-ascii\fP] [\fB-daemonize\fP]
 [\fB-host machine\fP] [\fB-port #\fP] [\fB-name name\fP] [\fB-read-from-stdin\fP]
.SH DESCRIPTION
The process hexteScM2 provides graphical displays for HEXTE Histogram Bin and
Multiscalar Bin Science modes.  It is run only as a child process of
the hexteRtClient
process from which it receives Histogram Bin (Mode 2) and Multiscalar Bin
(Mode 3)
16-second science partitions.  It displays the data graphically in a window
with a seperate plot for each of the two HEXTE clusters.  The on-target data
are displayed as gray histograms and background data are displayed as a red line plot.  In both modes, the user can select which detectors to sum, provided
they are not already summed by the instrument.  In Histogram Bin mode,
the user can also select which of multiple integrations to sum, while in 
Multiscalar Bin mode, the user can select which of multiple energy bands to
sum.
.SH EXAMPLES
Collect HEXTE Science telemetry data from a realtime server running on xitserv
and put up an Bin mode display:  

hexteRtClient -host xitserv.gsfc.nasa.gov -port 5094 -name HEXTE-Science -display-client hexteScM2
.SH SEE ALSO
.BR hexteRtClient (1)
.SH AUTHOR
Richard Bentley, UCSD/CASS, rbentley@@mamacass.ucsd.edu
@

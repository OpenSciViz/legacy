# ###########################################################################
#
# File Name   : Makefile
# Subsystem   : Science Monitoring
# Programmer  : Julie Schwan, Hughes STX
# Description : Makefile for the Science Monitoring Subsystem
#
# Required ENVIRONMENT variables:
#   SOCHOME           -- where integration copies reside
#   SOCOPS            -- where integration data resides
#   SOCLOCAL          -- where integration 3rd party s/w resides
#   ARCH              -- designation of machine architecture and OS
#   SOCMASTER         -- if defined, Read0only source of SOC s/w
#
# Targets:
#   pubhdrs           -- Install public header files into RCS
#   depend            -- Create Include file dependency list
#   publibs           -- Create and install libs into RCS
#   install           -- Install executables into RCS
#   clean             -- Remove *.o and *.a files
#
# RCS: $Id: Makefile,v 4.32 1996/11/22 20:17:29 soccm Exp soccm $
#
# ###########################################################################

#
# Subsystems to install
#
DIRS:=common PCA HEXTE

#
# Science Monitoring documents
#
DOCS:=SciMon.doc appendix_AtPlotter.fm appendix_tae.fm PCA_SM_Design.doc \
      PCA_SM_UsersGuide.doc TestpcaLeahyHist.txt TestpcaSHist.txt \
      TestpcaAncil.txt

#
# Science Monitoring documents
#
MAN1:=scimon.1

# ###########################################################################
# Targets
# ###########################################################################

all: pubhdrs depend publibs install

clean pubhdrs depend publibs install-test:
	@$(foreach i,$(DIRS),$(MAKE) -C $i $@;)

install: documents manpages
	@$(foreach i,$(DIRS),$(MAKE) -C $i $@;)

realclean:
	@$(foreach i,$(DIRS),$(MAKE) -C $i $@;)
	rcsclean

documents: $(DOCS)
	-cd $(SOCHOME)/doc; $(RM) $(DOCS)
	-cp $(DOCS) $(SOCHOME)/doc
	-cd $(SOCHOME)/doc; chmod 444 $(DOCS)

manpages: $(MAN1)
	-cd $(SOCHOME)/man/man1; $(RM) $(MAN1)
	-cp $(MAN1) $(SOCHOME)/man/man1
	-cd $(SOCHOME)/man/man1; chmod 444 $(MAN1)


head	5.9;
access
	soccm
	jschwan;
symbols;
locks
	jschwan:5.9; strict;
comment	@ * @;


5.9
date	97.02.25.22.20.33;	author jschwan;	state Exp;
branches;
next	;


desc
@class definition
@


5.9
log
@removed inlines
@
text
@// RCS: $Id: SciXImage.h,v 4.22 1995/08/01 03:00:27 dhon Exp $

#include <iostream.h>
//#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <SciXImage.h>

SciXImage::SciXImage(void) : UCharGenMat(), _zoom(1) {}

SciXImage::~SciXImage(void) { 
	// the colormap may be shared and should not be destroyed
	// the base class destructor will be invoked last (automatically?)
	delete _scanLines; 
	XDestroyImage(_ximage);
	XFreeGC(_scmap->_display, _gc);
}

SOCbostream& operator<<(SOCbostream& bos, const SciXImage& sim) { 
	sim.saveOn(bos.rwstrm());
	return bos;
}
SOCbistream& operator>>(SOCbistream& bis, SciXImage& sim) { 
	sim.restoreFrom(bis.rwstrm());
	return bis;
}

DoubleVec SciXImage::linearScale(UCharVec& ucv, const DoubleVec& indata) {
  DoubleVec minmax(2, 0.0);
  minmax[0] = minValue(indata);
  minmax[1] = maxValue(indata);
  double diff = minmax[1] - minmax[0];

  ucv.resize(indata.length());

  for( int i = 0; i < indata.length(); i++ ) {
    ucv[i] = (unsigned char)((_scmap->entries()-1)*(indata[i]-minmax[0])/diff);
    //cerr << "linearScale> i, power, colorCell: " << i << ", " << indata[i] 
    //     << ", " << (int) ucv[i] << endl;
  }
  return minmax;
}

DoubleVec SciXImage::logScale(UCharVec& ucv, const DoubleVec& indata) {
  DoubleVec minmax(2, 0.0);
  minmax[0] = minValue(indata);
  if( minmax[0] < 1.0 )
    minmax[0] = 1.0;
  minmax[1] = minmax[0] + maxValue(indata);
  double lgmn = log(minmax[0]);
  double diff = log(minmax[1]) - lgmn;

  ucv.resize(indata.length());

  for( int i=0; i < indata.length(); i++ ) {
    ucv[i] = (unsigned char)((_scmap->entries()-1)*(log(minmax[0]+indata[i])-lgmn)/diff);
    //cerr << "linearScale> i, power, colorCell: " << i << ", " << indata[i] 
    //     << ", " << (int) ucv[i] << endl;
  }
  return minmax;
}

// the scanline data should be within the range of colorCell[0]
// to colorCell[_scmap->entries() - 1]
void SciXImage::draw(int xsrc, int ysrc, int xdst, int ydst, Window win) {
// reset state of scanLines Vec from current state of matrix:
  if( _scanLines->length() > 0 ) {
    for( int rowNum = 0; rowNum < rows(); rowNum++ ) {
      //cerr << "SciXImage::draw> matrix dim.: " << rows() << " " << cols() << endl;
      //cerr << "SciXImage::draw> scanline Vec dim.: " << _scanLines->length() << endl;
      //cerr << "SciXImage::draw> set row# " << rowNum << " width= " << _width << endl;
      UCharVec viewRow = row(rowNum);
      UCharVec viewScan = _scanLines->slice((rowNum*_zoom)*_width, _zoom*_width);
      //cerr << "SciXImage::draw> zoom= " << _zoom 
      //     << " viewScan= " << viewScan.length()
      //     << " viewRow " << viewRow.length() << endl;
      if( _zoom == 1 ) {
	for( int colNum = 0; colNum < cols(); colNum++ )
	  viewScan[colNum] = (unsigned char) colorCellPixelAt(viewRow[colNum]);
      }
      else { // replicate
        //cerr << "SciXImage::draw> viewRepScan len: " << viewRepScan.length() << endl;
        for( int zoomRow = 0; zoomRow < _zoom; zoomRow++ ) {
          UCharVec viewRepScan = viewScan.slice(zoomRow*_width, _width);
	  for( int colNum = 0, elem = 0; elem < _width; elem++, colNum = elem/_zoom ) {
            viewRepScan[elem] = (unsigned char) colorCellPixelAt(viewRow[colNum]);
	  //cerr << "SciXImage::draw> viewRepScan elem: " << elem << endl;
	  }
	}
      }
    }
    Window w; // allow alternate window for target draw:
    if( win )
      w = win;
    else
      w = XtWindow(_widget);

    XPutImage(_scmap->_display, w, _gc, _ximage,
              xsrc, xdst, ysrc, ydst, _width, _height);
    //XFlush(_scmap->_display);
  }
}

void SciXImage::drawCMap(Window win) {
  // sets zoom to 1:
  _zoom = 1;
  // sets internal image data to linear ramp for sampling all color cells:
  int hratio = 1;
  if( _height > _scmap->entries() )
    hratio  = _height / (_scmap->entries()-1);
  else
    cerr << "SciXImage.drawCMap> insufficient pixel height for CMAP, clipping image..." << endl;
    
  for( int ir = 0, val = 0; ir < rows() && val < _scmap->entries(); ir++ ) {
    row(ir) = val = (unsigned char) (ir / hratio);
    //cerr << "row " << ir << " set to: " << ir/hratio << endl;
  }

  draw(0, 0, 0, 0, win);
}

void SciXImage::resetData(const UCharGenMat& gm) {
  UCharGenMat& tmp = *this;
  if( this != &gm ) {
    _width = _zoom*gm.cols();
    _height = _zoom*gm.rows();
    tmp.resize(_height, _width);
    tmp = gm;
  }
}

void SciXImage::_create(Dimension width, Dimension height) {
  _visual = DefaultVisual(_scmap->_display, _scmap->_screenNum);
  _gc = XCreateGC(_scmap->_display, XtWindow(_widget), 0, 0);
  _ximage = 0;
  if( width == 0 ) {
    XtVaGetValues(_widget, XtNwidth, &width, NULL);
  }
  _width = width;

  if( height == 0 ) {
    XtVaGetValues(_widget, XtNheight, &height, NULL);
  }
  _height = height;

  _scanLines = new UCharVec(_width*_height, 0);

  cerr << "SciXImage::_create> w, h = " << _width << ", " << _height << endl;
  
  resize(_height/_zoom, _width/_zoom);

  _ximage = XCreateImage(_scmap->_display, _visual, 8,  ZPixmap, 0,
			 (char*)_scanLines->data(), _width, _height, 8, _width);
/*
  if( _ximage )
    cerr << "SciXImage::_create> success creating image: w, h = " << cols() << ", " << rows() << endl;
  else
    cerr << "SciXImage::_create> failed to create image: w, h = " << cols() << ", " << rows() << endl;
*/
}

SciXImage::SciXImage(Window win, const SciXCMap* cmap, int zoom,
	             Dimension width, Dimension height) :
                     UCharGenMat() {
  _scmap = (SciXCMap*) cmap;
  _widget = XtWindowToWidget(_scmap->_display, win);
  _zoom = zoom;
  _create(width, height);
}

SciXImage::SciXImage(Widget wgt, const SciXCMap* cmap, int zoom,
	             Dimension width, Dimension height) :
                     UCharGenMat() {
  _scmap = (SciXCMap* ) cmap;
  _widget = wgt;
  _zoom = zoom;
  _create(width, height);
}

SciXImage::SciXImage(const SciXImage& rhs) : 
           UCharGenMat((const UCharGenMat&) rhs) {
  if( &rhs != this ) { // shallow copy
    _visual = rhs._visual; 
    _depth = rhs._depth; 
    _gc = rhs._gc;
    _bitmapPad = rhs._bitmapPad;
    _bytesPerLine = rhs._bytesPerLine;
    _ximage = rhs._ximage;
    _widget = rhs._widget;
    _height = rhs._height, 
    _width = rhs._width;
    _scanLines = rhs._scanLines;
    _scmap = rhs._scmap;
    _zoom = rhs._zoom;
  }
}

SciXImage& SciXImage::operator=(const UCharGenMat& rhs) {
  this->resize(rhs.rows(), rhs.cols());
  UCharGenMat *tmp = (UCharGenMat *) this;
  *tmp = (UCharGenMat& ) rhs;

  return *this;
}

SciXImage& 
SciXImage::operator=(const SciXImage& rhs) {
  if( &rhs != this ) { 
    this->resize(rhs.rows(), rhs.cols());
    UCharGenMat *tmp = (UCharGenMat *) this;
    *tmp = (UCharGenMat& ) rhs;
    _visual = rhs._visual; 
    _depth = rhs._depth; 
    _gc = rhs._gc;
    _bitmapPad = rhs._bitmapPad;
    _bytesPerLine = rhs._bytesPerLine;
    _ximage = rhs._ximage;
    _widget = rhs._widget;
    _height = rhs._height, 
    _width = rhs._width;
    _scanLines = rhs._scanLines;
    _scmap = rhs._scmap;
    _zoom = rhs._zoom;
  }
  return *this;
}

void
SciXImage::append(const UCharVec& v) {
  resize(rows(), 1+cols());
  col(cols()-1) = v;
}

void
SciXImage::prepend(const UCharVec& v) {
  resize(rows(), 1+cols()); // add a col
  for( int i = cols()-1; i > 0; i-- ) // shift right
    col(i) = col(i-1);
  col(0) = v; // set first
}

void
SciXImage::shiftForNew(const UCharVec& v) {
  for( int i = 1; i < cols(); i++ ) // shift left
    col(i-1) = col(i);

  col(cols() - 1) = v; // insert new data from v
}

void
SciXImage::shiftForOld(const UCharVec& v) {
  for( int i = cols()-1; i > 0; i-- ) // shift right 
    col(i) = col(i-1);

  col(0) = v;
}
@

head	5.9;
access
	soccm
	jschwan;
symbols;
locks
	jschwan:5.9; strict;
comment	@ * @;


5.9
date	97.02.25.22.20.33;	author jschwan;	state Exp;
branches;
next	;


desc
@class definition
@


5.9
log
@removed inlines
@
text
@// RCS:
static const char* const rcsid =
"$Id$" ;

#include <std2EdsPart.h>

std2EdsPart::std2EdsPart(): EdsPartition(),
                            SciAncilBits(),
			    ExpectCounts(),
			    MeanAttPos(),
			    EarthMoonSun() {}

std2EdsPart::std2EdsPart(const EdsPartition& eds): EdsPartition(eds),
                            SciAncilBits(),
			    ExpectCounts(),
			    MeanAttPos(),
			    EarthMoonSun() {}

std2EdsPart::std2EdsPart(const EdsPartition& eds,
			 SciAncilBits& bits,
			 ExpectCounts& ec,
			 MeanAttPos& ap,
			 EarthMoonSun& ems): EdsPartition(eds),
                                             SciAncilBits(bits),
					     ExpectCounts(ec),
					     MeanAttPos(ap),
					     EarthMoonSun(ems) {}

std2EdsPart::std2EdsPart(std2EdsPart& rhs): EdsPartition(),
                                             SciAncilBits(),
					     ExpectCounts(),
					     MeanAttPos(),
					     EarthMoonSun() {
  if( this != &rhs )
    *this = rhs;
}

void std2EdsPart::xenon(int pcuid, unsigned short* theDataBuf,
	           double* theSpectraL[3], double* theSpectraR[3]) {
//  if( theDataBuf == NULL ) 
//    return; // do nothing!

  switch( pcuid ) {
    case 1: theDataBuf += xenon1OffsetStd2;
            break;
    case 2: theDataBuf += xenon2OffsetStd2;
            break;
    case 3: theDataBuf += xenon3OffsetStd2;
            break;
    case 4: theDataBuf += xenon4OffsetStd2;
            break;
    case 5: theDataBuf += xenon5OffsetStd2;
            break;
    default:
	    break;
  }
  // storage is in all left, then all right for each layer.
#ifndef TESTME
  for( int j = 0; j < 3; j++ ) { 
     for( int i = 0; i < std2NumXVals; i++ ) { 
       *theSpectraL[j]++ = *theDataBuf++;
     }
     for( i = 0; i < std2NumXVals; i++ ) { 
       *theSpectraR[j]++ = *theDataBuf++;
     }
  }
#else
  for( int layer = 0; layer < 3; layer++ ) { 
    for( int i = 0; i < std2NumXVals; i++ ) { 
      *theSpectraL[layer] = *theDataBuf++;
      *theSpectraL[layer]++ = (layer+1)*10.0*(1.0+cos(M_PI*i/std2NumXVals));
    }
    for( i = 0; i < std2NumXVals; i++ ) { 
      *theSpectraR[layer] = *theDataBuf++;
      *theSpectraR[layer]++ = (layer+1)*10.0*(1.0+cos(M_PI*i/std2NumXVals));
    }
  }
#endif
}

void std2EdsPart::propane(int pcuid, unsigned short* theDataBuf, double *theSpectra) {
//  if( pcuid < 1 || pcuid > 5 || theDataBuf == NULL || theSpectra == NULL ) 
//    return; // do nothing!

  theDataBuf += propaneOffsetStd2 + (pcuid-1)*std2NumPVals;

#ifndef TESTME
  for( int i = 0; i < std2NumPVals; i++ ) 
    *theSpectra++ = *theDataBuf++;
#else
  for( int i = 0; i < std2NumPVals; i++ ) {
    *theSpectra = *theDataBuf++; // do the access to check for problems, then replace
    *theSpectra++ = 70.0*(1.0+cos(M_PI*i/std2NumPVals));
  }
#endif
}

void std2EdsPart::backgrnd(int pcuid, unsigned short* theDataBuf, double *theCounts) {
//  if( pcuid < 1 || pcuid > 5 || theDataBuf == NULL || theCounts == NULL ) 
//    return; // do nothing!

//theDataBuf += backgrndOffsetStd2 + (pcuid-1)*std2NumBVals;
  theDataBuf += backgrndOffsetStd2 + (pcuid-1);

#ifndef TESTME
  for( int i = 0; i < std2NumBVals; i++ ) 
//  *theCounts++ = *theDataBuf++;
    *theCounts++ = *(theDataBuf+(i*5));
#else
  for( int i = 0; i < std2NumBVals; i++ ) {
    *theCounts = *theDataBuf++; // do the access to check for problems, then replace
    *theCounts++ = 5.0*(1.0+cos(M_PI*i/std2NumBVals));
  }
#endif
}

unsigned long std2EdsPart::getStartTime() {
  return (unsigned long) partStartTime();
}

int std2EdsPart::sendData(SOCbostream& bos) {
  int retVal = 1;
  int fd = bos.fd();
  unsigned long t = getStartTime();

  int nb = write_bytes(fd, (const unsigned char*) &t, sizeof(long));

  unsigned short *unpackedData = getData();
  nb = write_bytes(fd, (const unsigned char*)unpackedData, std2PartBuffSize);
  bos.flush();
  delete unpackedData;

  if( nb < std2PartBuffSize )
    retVal = -1;

  return retVal;
}

int std2EdsPart::sendData(SOCbostream& bos,
	   const SciAncilBits& bits, const ExpectCounts& ec, 
	   const MeanAttPos& ap, const EarthMoonSun& ems) {
  int retVal = 1;
  int fd = bos.fd();

  unsigned long t = getStartTime();
  int nb = write_bytes(fd, (const unsigned char*) &t, sizeof(long));

  bos << bits;
  bos << ec;
  bos << ap;
  bos << ems;

  unsigned short *unpackedData = getData();
  nb = write_bytes(fd, (const unsigned char*) unpackedData,
		   std2PartBuffSize);
  bos.flush();
  delete unpackedData;

  if( nb < std2PartBuffSize )
    retVal = -1;

  return retVal;
}

int std2EdsPart::restoreData(SOCbistream& bis, unsigned short *theDataBuf, unsigned long& time) {
  int retVal = 1;
  int fd = bis.fd();
  int nb = read_bytes(fd, (unsigned char *)&time, sizeof(long));
  nb = read_bytes(fd, (unsigned char*)theDataBuf, std2PartBuffSize);
 
  if( nb < std2PartBuffSize )
    retVal = -1;

  return retVal;
}

int std2EdsPart::restoreData(SOCbistream& bis, unsigned short *theDataBuf,
	      SciAncilBits& bits, ExpectCounts& ec, MeanAttPos& ap,
	      EarthMoonSun& ems, unsigned long& time) {
  int retVal = 1;
  int fd = bis.fd();
  int nb = read_bytes(fd, (unsigned char *)&time, sizeof(long));

  bis >> bits;
  bis >> ec;
  bis >> ap;
  bis >> ems;

  nb = read_bytes(fd, (unsigned char*)theDataBuf, std2PartBuffSize);

  if( nb < std2PartBuffSize )
    retVal = -1;

  return retVal;
}

void std2EdsPart::xenonAll(DoubleArray& left, DoubleArray& right) {
  unsigned short* theData = getData();
  int pcuid, layer;
  for( pcuid = 1; pcuid < 6 ; pcuid++ ) { // short to double conversion
    double *spectraL[3];
    double *spectraR[3];
    for( layer = 0; layer < 3; layer++ ) {
      DoubleVec pcuL(left(RWAll, layer, pcuid-1 )); // construct a view
      DoubleVec pcuR(right(RWAll, layer, pcuid-1 )); // of a column
      spectraL[layer] = pcuL.data();
      spectraR[layer] = pcuR.data();
//    cerr << "Xenon pcu #" << pcuid << " Left" << pcuL << endl;
//    cerr << "Xenon pcu #" << pcuid << " Right" << pcuR << endl;
    } // set the double pointers
    xenon(pcuid, theData, spectraL, spectraR);
  }
// pickup last values of xenon 
  for( pcuid = 0; pcuid < 5 ; pcuid++ ) { // short to double conversion
     for( layer = 0; layer < 3; layer++ ) {
        left(128, layer,pcuid ) = theData[4145 + (layer*2)  + 6*pcuid] ;
        right(128, layer,pcuid ) = theData[4145 + (layer*2 + 1) + 6*pcuid] ;
     }
  }
  delete theData;
}

void std2EdsPart::expand256(DoubleGenMat& expandXenon, const DoubleVec& v, int n) {
// convert DoubleVec from 128 elements to 256 elements
// cerr << "Entered expand256  "<<  endl;
  int k = 0 ;
  
  for (int j=0; j<5; j++ ) {
        expandXenon (j,n) = v[k] / 5 ;                   
  }

  for ( j=5; j<54; j++ ) {
        k += 1 ;
        expandXenon (j,n) = v[k] ;                   
     }

  for ( j=54; j<136; j+=2 ) {
        k += 1 ;
        expandXenon (j,n) = v[k] /2 ;                   
        expandXenon (j+1,n) = v[k] /2 ;                   
     }

  for ( j=136; j<238; j+=3 ) {
      k += 1 ;
      for (int i = 0 ; i<3 ; i++)
        expandXenon ( j+i, n) = v[k] /3 ;                   
     }

  for ( j=238; j<250; j+=4 ) {
      k += 1 ;
      for (int i = 0 ; i<4 ; i++)
        expandXenon ( j+i, n) = v[k] /4 ;                   
     }

  for ( j=250; j<255; j+=6 ) {
      k += 1 ;
      for (int i = 0 ; i<6 ; i++)
        expandXenon ( j+i, n) = v[k] /6 ;                   
     }
}

ostream& operator<<( ostream& bos, const std2EdsPart& wa ) {
  SciAncilBits *bits = (SciAncilBits* ) &wa;
  bos << *bits;

  ExpectCounts *ec = (ExpectCounts* ) &wa;
  bos << *ec;

  MeanAttPos *ap = (MeanAttPos* ) &wa;
  bos << *ap;

  EarthMoonSun *ems = (EarthMoonSun* ) &wa;
  bos << *ems;

  EdsPartition *eds = (EdsPartition* ) &wa;
  //bos << *eds;

  return bos;
}

SOCbostream& operator<<( SOCbostream& bos, const std2EdsPart& wa ) {
  SciAncilBits *bits = (SciAncilBits* ) &wa;
  bos << *bits;

  ExpectCounts *ec = (ExpectCounts* ) &wa;
  bos << *ec;

  MeanAttPos *ap = (MeanAttPos* ) &wa;
  bos << *ap;

  EarthMoonSun *ems = (EarthMoonSun* ) &wa;
  bos << *ems;

  EdsPartition *eds = (EdsPartition* ) &wa;
  bos << *eds;

  return bos;
}

SOCpostream& operator<<( SOCpostream& pos, const std2EdsPart& wa ) {
  SciAncilBits *bits = (SciAncilBits* ) &wa;
  pos << *bits;

  ExpectCounts *ec = (ExpectCounts* ) &wa;
  pos << *ec;

  MeanAttPos *ap = (MeanAttPos* ) &wa;
  pos << *ap;

  EarthMoonSun *ems = (EarthMoonSun* ) &wa;
  pos << *ems;

  EdsPartition *eds = (EdsPartition* ) &wa;
  pos << *eds;

  return pos;
}

SOCbistream& operator>>( SOCbistream& bis, std2EdsPart& wa ) {
  SciAncilBits *bits = (SciAncilBits* ) &wa;
  bis >> *bits;

  ExpectCounts *ec = (ExpectCounts* ) &wa;
  bis >> *ec;

  MeanAttPos *ap = (MeanAttPos* ) &wa;
  bis >> *ap;

  EarthMoonSun *ems = (EarthMoonSun* ) &wa;
  bis >> *ems;

  EdsPartition *eds = (EdsPartition* ) &wa;
  bis >> *eds;

  return bis;
}

SOCpistream& operator>>( SOCpistream& pis, std2EdsPart& wa ) {
  SciAncilBits *bits = (SciAncilBits* ) &wa;
  pis >> *bits;

  ExpectCounts *ec = (ExpectCounts* ) &wa;
  pis >> *ec;

  MeanAttPos *ap = (MeanAttPos* ) &wa;
  pis >> *ap;

  EarthMoonSun *ems = (EarthMoonSun* ) &wa;
  pis >> *ems;

  EdsPartition *eds = (EdsPartition* ) &wa;
  pis >> *eds;

  return pis;
}
@

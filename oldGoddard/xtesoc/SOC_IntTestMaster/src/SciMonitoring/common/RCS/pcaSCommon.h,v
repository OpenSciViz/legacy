head	5.12;
access
	soccm
	jschwan;
symbols;
locks;
comment	@ * @;


5.12
date	97.05.29.14.22.08;	author soccm;	state Exp;
branches;
next	5.11;

5.11
date	97.05.01.21.17.38;	author jschwan;	state Exp;
branches;
next	5.10;

5.10
date	97.04.14.19.59.35;	author jschwan;	state Exp;
branches;
next	5.9;

5.9
date	97.04.03.21.20.12;	author jschwan;	state Exp;
branches;
next	;


desc
@Contains PCA global variables and function declarations.
@


5.12
log
@added emailPS function
@
text
@// ----------------------------------------------------------------------------
// File Name   : pcaSCommon.h
// Subsystem   : SOC Science Monitoring PCA Global Functions 
// Programmer  : Julie Schwan, Hughes STX/NASA GSFC
// Description :
// This has been included for use as a SOC Science Monitoring
// global function definition list, where new and updated
// global function implementations to many PCA processes are added.
// pcaSCommon.h is a public header file that gets used by many
// PCA subdirectories.
//
// RCS: $Id: pcaSCommon.h,v 5.11 1997/05/01 21:17:38 jschwan Exp $
// ----------------------------------------------------------------------------

#ifndef __PCASCOMMON_H__
#define __PCASCOMMON_H__

// customizations follow...
#ifndef __STDC__
extern int exit();
#endif

#include <strstream.h>
#include <math.h>
#include <rw/darr.h>
#include <rw/dvec.h>
#include <rw/dgenmat.h>
                                     
#include <X11/At/Plotter.h>
#include <X11/At/XYAxis.h>
#include <X11/At/Axis.h>
#include <X11/At/AxisCore.h>
#include <X11/At/XYLinePlot.h>
#include <Xm/Xm.h>

#include <SOCTime2.h>
#include <SciStream.h>
#include <SciCommon.h>
#include <SciMonitor.h>

// variable declarations for use by other PCA guis
extern int ascii, selectedSum, selectedPcu[5], gapShow;
extern int parCount;
extern int selDet, selMode, selYaxis, selRate[29];
extern float selXaxis;
extern int selInteg, selStat, compressFactor;
extern int qPopup, qStrip;
extern int selLayer, selComb;
extern int chStart[4], chEnd[4];
extern int nBand;   // for "Colors" option
extern int scClear;   // for "Colors" option
extern float slope, ofset;
extern double starTime, endTime;
extern float ownMax, ownMin;
extern int setOwn;

// variable declarations defined and used by other PCA guis
// plot objects
extern SciMonitor *monXenonL[3], *monXenonR[3], *monSums, *monRatio ,*monVp ;
extern SciMonitor *monXenon ;  //mode1 light curve
extern SciMonitor *monRateA, *monRateB;
extern SciMonitor *monChart ;  //strip chart plots
//
extern void dumpAscii(const char *fileName);
extern void setPlots(unsigned short*);
extern void addDetectors();
extern void accumulate();
extern void resetXaxis() ;
extern void resetYaxis() ;
extern void attachXenon();
extern void readData();
extern void doGraphics(int init);
extern void updateStatus();
//
// spectral science mode
//
extern void applyGain();
extern void addLayers();
//
// to add popup and strip chart
//
extern void popup();
extern void strip();
//.....
extern void computeArea();
extern void initialize();
extern void selectRates();
extern void attachXenonL();
extern void attachXenonR();
extern void noZeros() ;
extern void setColors() ;
extern double computeYmax1();
extern double computeYmax2();
extern double computeYmax3();
//.....
extern void attachSum();
extern void attachRatio();
extern void attachPropane();
extern void attachRateA();
extern void attachRateB();
extern void computeSum();
extern void expand40(const DoubleVec*) ;
extern void convertData() ;
extern void compress() ;
extern void dataToArray() ;
extern double computeYmaxp();
extern double computeYmaxs();
extern double computeYmaxr();

class pcaSCommon {

public:
  pcaSCommon();

  void saveAscii(const char *fileName,  DoubleGenMat& dgm, int ndata, int ncol,
                 char* status, double time, int mode, const String yname[]=NULL,
                 const int range = 0, const int dir = 0) ;

  void saveAsciiOwnDir(const char *fileName, DoubleGenMat& dgm, int ndata,
                 int ncol, char* status, double time, int mode,
                 const String yname[]=NULL, const int range = 0);

  void emailAscii(char *mailAddr, const char *subject, DoubleGenMat& dgm,
                 int ndata, int ncol, char* status, double time, int mode,
                 const String yname[]=NULL, const int range = 0) ;

  void emailPS(const char *mailAddr, const char *subject, Widget pw) ;

  void expand256(DoubleGenMat& expandXenon, const DoubleVec& v, int n);

  void initialize(DoubleArray& xenonL, DoubleArray& xenonR);

  double computeArea(const DoubleGenMat& exXenon, int n);

  Pixel GetColorPixel(char*, Widget) ;

  void supressAxis2(SciMonitor* monX);

  DoubleVec& setOwnmax(DoubleVec& doubleV, double doubleN, int intN) ;

};

#endif
@


5.11
log
@Added subject arg to emailAscii func for Subject heading in E-mails.
@
text
@d1 14
a14 1
// RCS: $Id: pcaSCommon.h,v 5.10 1997/04/14 19:59:35 jschwan Exp jschwan $
d29 1
d111 1
d126 2
@


5.10
log
@saveAsciiOwnDir takes one path argument instead of two.
@
text
@d1 1
a1 1
// RCS: $Id: pcaSCommon.h,v 5.9 1997/04/03 21:20:12 jschwan Exp jschwan $
d108 3
a110 3
  void emailAscii(char *mailAddr, DoubleGenMat& dgm, int ndata, int ncol,
                 char* status, double time, int mode, const String yname[]=NULL,
                 const int range = 0) ;
@


5.9
log
@Added emailAscii() and saveAsciiOwnDir() declarations.
@
text
@d1 1
a1 1
// RCS: $Id: pcaSCommon.h,v 4.2 1996/08/27 17:54:36 rhee Exp $
d104 3
a106 4
  void saveAsciiOwnDir(const char *dirName, const char *fileName,
                 DoubleGenMat& dgm, int ndata, int ncol, char* status,
                 double time, int mode, const String yname[]=NULL,
                 const int range = 0);
@

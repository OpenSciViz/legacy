head	5.9;
access
	soccm
	jschwan;
symbols;
locks
	jschwan:5.9; strict;
comment	@ * @;


5.9
date	97.02.25.22.20.33;	author jschwan;	state Exp;
branches;
next	;


desc
@class definition
@


5.9
log
@removed inlines
@
text
@// RCS: $Id: SciAttPos.h,v 4.34 1995/12/01 22:16:58 dhon Exp $

// This class inherits from MeanAttPos in order to facilitate 
// the availability of quaternions and s/c position that are 
// synchronized with PCA or HEXTE partition start and stop
// times. The typical duration of such is 8 or 16 or 128 seconds.
// For such short time intervals it is expected that the rms of
// the attitute and altitude parameters will be nearly zero,
// while the position (lat. & lon.) at the start and stop times
// might differ sufficiently to be of interest.

#include <SciAttPos.h>

SciAttPos::SciAttPos(double time, double duration) :
  MeanAttPos(time, duration),
  _boreXte(3,0.0), _borePca(3,0.0), _boreHexte(3,0.0), 
  _quat(), _qerr(), _latlon(), _altitude(),
  _ra(), _dec(), _insertions()
{
  _boreXte[0] = 1.0;
  _borePca[0] = 1.0;
  _boreHexte[0] = 1.0;
}

SciAttPos::SciAttPos(const MeanAttPos& map) : MeanAttPos(map),
  _boreXte(3,0.0), _borePca(3,0.0), _boreHexte(3,0.0), 
  _quat(), _qerr(), _latlon(), _altitude(),
  _ra(), _dec(), _insertions()
{
  _boreXte[0] = 1.0;
  _borePca[0] = 1.0;
  _boreHexte[0] = 1.0;
}

SciAttPos::SciAttPos(const SciAttPos& sap) :
  MeanAttPos((MeanAttPos&) sap), 
  _boreXte(3,0.0), _borePca(3,0.0), _boreHexte(3,0.0), 
  _quat(), _qerr(), _latlon(), _altitude(),
  _ra(), _dec(), _insertions()
{
  _boreXte[0] = 1.0;
  _borePca[0] = 1.0;
  _boreHexte[0] = 1.0;
}

void SciAttPos::_pcaGetError(int echo, double *borexte, const RWCString& file)
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*                                                         */
/*   Read data file <PCAboreError>                         */
/*   Lists data on screen if echo = 1                      */
/*                                                         */
/*   Assumes default values of (1, 0, 0) if                */
/*   file does not exist                                   */
/*                                                         */
/*                                                         */
/*   Author:  Dr.A.B.Giles (USRA)   XTE PCA group, GSFC    */
/*   Date:    13th June 1995                               */
/*                                                         */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
{
  //int i,j,k,count,num,fail;
        int j,k,fail;
        float val[3];
        FILE *fcon;

/*  try to open file  */
        fail = 1;
        fcon = fopen(file.data(),"r");    
        if (fcon == NULL) {
                if (echo == 1) {
                        printf("File  <PCAboreError>  Does Not Exist\n");
                        printf("Taking default (1, 0, 0) values\n");
                }
                *borexte     = 1.0;
                *(borexte+1) = 0.0;
                *(borexte+2) = 0.0;
                fail = -1;
        }
        if (fail == 1) {
                if (echo == 1) {
                        printf("File  <PCAboreError>  Exists\n");
                        printf("Reading data vector\n");
                }
/*  read initial comment block  */
                k = fgetc(fcon);
                while (k == 47) {
                        if (echo == 1) printf("%c",k);
                        while (k != 10) {
                                k = fgetc(fcon);
                                if (echo == 1) printf("%c",k);
                                }
                        k = fgetc(fcon);
                }

/*  read real data  part*/
                k = fgetc(fcon);
                for (j=0; j<3; j++) fscanf(fcon,"%f",&val[j]);
                if (echo == 1) {
                        for (j=0; j<3; j++) printf("%10.5f",val[j]);
                        printf("\n");
                }
                k = fgetc(fcon);
                k = fgetc(fcon);
                ungetc(k,fcon);

/*  read trailing comment block  */
                k = fgetc(fcon);
                while (k == 47) {
                        if (echo == 1) printf("%c",k);
                        while (k != 10) {
                                k = fgetc(fcon);
                                if (echo == 1) printf("%c",k);
                                }
                        k = fgetc(fcon);
                }

/*  close file  */
                fclose(fcon);
                *borexte     = val[0];
                *(borexte+1) = val[1];
                *(borexte+2) = val[2];
        }
}


void SciAttPos::_radecbore(double *raout,     double *decout,
                           double *rockPAout, double *rollout,
                           double *boreinert, double *borexte,
                           double *quat,      double *sunvec)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*                                                                            */
/*  Uses the attitude matrix to transform the boresight vector in XTE (body)  */
/*  coordinates to inerital (celestial) coordinates (RA and DEC).             */
/*  The algorithm assumes that the inertial reference coordinates (x,y,z),    */
/*  which are derived from the boresight vector via matrix multiplication     */
/*  with the inverse (transpose) of the attitude matrix are defined such      */
/*  that x=cos(dec)cos(ra), y=cos(dec)sin(ra), z=sin(dec).                    */
/*                                                                            */
/*  Inputs:                                                                   */
/*    quat[4]       quaternions                                               */
/*    borexte[3]    contains the components of the boresight in the           */
/*                  spacecraft frame (to be read from an auxiliary data file) */
/*    sunvec[3]     inertial solar position vector (not a unit vector)        */
/*                                                                            */
/*  Outputs:                                                                  */
/*    boreinert[3]  inertial x,y,z components of the boresight                */
/*    raout         right ascension of the boresight                          */
/*    decout        declination of the boresight                              */
/*    rockPAout     HEXTE rock angle (position angle on sky of +Y axis)       */
/*                                          (anti-clockwise from North)       */
/*    rollout       roll bias angle  (+ve takes Sun into +Y +Z quadrant)      */
/*                           (spacecraft slews clockwise looking at sky)      */
/*                  can be up to +- 5 deg if sun in AZ plane                  */
/*                  can be up to +-90 deg if sun at -X                        */
/*                                                                            */
/*  Output range:                                                             */
/*    DEC                -90 to  90 deg                                       */
/*    RA                   0 to 360 deg                                       */
/*    HEXTE rock PA        0 to 360 deg                                       */
/*    Roll bias angle    -90 to  90 deg                                       */
/*                                                                            */
/*                                                                            */
/*  Test Data & Results                                                       */
/*    Quaternion              Sun Vector                   Boresight          */
/*    quat[0] = -0.403051;    sunvec[1] = -0.989688707     borexte[0] = 1.0   */
/*    quat[1] = -0.531732;    sunvec[2] =  0.131414983     borexte[1] = 0.0   */
/*    quat[2] =  0.394072;    sunvec[3] =  0.05697688      borexte[2] = 0.0   */
/*    quat[3] =  0.632075;                                                    */
/* actually the results below are for these quaternions (dh):
  quat[0] =  0.801419968;
  quat[1] = -0.190376499;
  quat[2] = -0.256531608;
  quat[3] =  0.505642520;                                                                      */
/*    RA        =  324.650000                                                 */
/*    DEC       =  -12.630000                                                 */
/*    Limb      =   75.166329                                                 */
/*    Rock PA   =  338.076634                                                 */
/*    Roll bias =    5.0                                                      */
/*                                                                            */
/*                                                                            */
/*   Geometry (4 positions)                                                   */
/*                                              Front View                    */
/*                   Y-                         Detectors look along +X       */
/*                                                     (out of page)          */
/*       ----------------------------                                         */
/*       |   |   |   |   |   |  1   |                                         */
/*       |   |   |   |   |   | <--> |           Y+ = xrollout                 */
/*       | 5 | 4 | 3 | 2 | 1 |      |           Z- = mod_360((Y+)+90)         */
/*  Z-   |   |   |   |   |   |------|   Z+      Y- = mod_360((Z-)+90)         */
/*       |   |   |   |   |   |  2 ^ |  (Yaw)    Z+ = mod_360((Y-)+90)         */
/*       |-------------------|    | |                                         */
/*       |                   |    ^ |                                         */
/*       ----------------------------           Y+ to Y- is across flats      */                        /*                                              on PCA collimator             */
/*                   Y+ (Pitch)                                               */
/*                                                                            */
/*                                                                            */
/*  Development:                                                              */
/*    Fortran version by Tod Strohmayer(USRA) - PCA team    early June 1995   */
/*                                                                            */
/*    Converted to C by Barry Giles(USRA) - PCA team    V 1  13th June 1995   */
/*                                        correction    V 2  16th June 1995   */
/*                                  HEXTE Rock angle    V 3  26th June 1995   */
/*                                   roll bias angle    V 4  10th July 1995   */
/*                                                                            */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
{
  double q11, q22, q33, q44, q12, q34, q13, q24, q23, q14;
  double att[4][4], Yborexte[3], Yboreinert[3], sunn[3], sunbody[3];
  double pi, p2, crd, ay, ax, az, by, bx;
  double ra[2], dec[2], angle, raa, roll, xnorm;
  int i, j;

        pi = 3.141592653589793;
        p2 = 2.0 * pi;
        crd = 180.0 / pi;

/*   Set up attitude matrix (direction cosine matrix)  */
  q11 = *quat     * *quat;
  q22 = *(quat+1) * *(quat+1);
  q33 = *(quat+2) * *(quat+2);
  q44 = *(quat+3) * *(quat+3);
  q12 = *quat     * *(quat+1);
  q34 = *(quat+2) * *(quat+3);
  q13 = *quat     * *(quat+2);
  q24 = *(quat+1) * *(quat+3);
  q23 = *(quat+1) * *(quat+2);
  q14 = *quat     * *(quat+3);

  att[0][0] = q11 - q22 - q33 + q44;
  att[0][1] = 2.0 * (q12 + q34);
  att[0][2] = 2.0 * (q13 - q24);
  att[1][0] = 2.0 * (q12 - q34);
  att[1][1] = -q11 + q22 - q33 + q44;
  att[1][2] = 2.0 * (q23 + q14); 
  att[2][0] = 2.0 * (q13 + q24);
  att[2][1] = 2.0 * (q23 - q14);
  att[2][2] = -q11 - q22 + q33 + q44;

/*  Do transformation of boresight in body to inertial reference (J2000)  */
  for (i=0; i<3; i++) {
    *(boreinert+i) = 0.0;
    for (j=0; j<3; j++) {
      *(boreinert+i) = *(boreinert+i) + att[j][i] * 
           *(borexte+j);
    }
  }

/*  Find dec coordinate of boresight  */
  angle = *(boreinert+2);
  if (angle > 1.0) angle = 1.0;
  if (angle < -1.0) angle = -1.0;
        dec[0] = crd * asin(angle);

/*  Find ra coordinate of boresight  */
        ay = *(boreinert+1);
        by = fabs(ay);
  ax = *boreinert;
  bx = fabs(ax);
  angle = by / bx;
  angle = atan(angle);
  raa = 0.0;
        if ((ax == 0.0) && (ay > 0.0)) raa = pi / 2.0;
        if ((ax == 0.0) && (ay < 0.0)) raa = (3.0 * pi) / 2.0;
        if ((ax > 0.0) && (ay >= 0.0)) raa = angle;
/*  The next line was wrong. It was corrected on 11/9/95       */
/*  It was noticed by Tod Strohmayer & changed by Barry Giles  */
/*      if ((ay < 0.0) & (az <= 0.0)) roll = pi - angle;       */
        if ((ax < 0.0) && (ay <= 0.0)) raa = pi + angle;
        if ((ax > 0.0) && (ay <= 0.0)) raa = p2 - angle;
        ra[0] = raa * crd;
  *decout = dec[0];
  *raout = ra[0];

/*  Do transformation of +Y offset in body to inertial reference (J2000)  */
  Yborexte[0] = *(borexte);
  Yborexte[1] = *(borexte+1) + 0.01;
  Yborexte[2] = *(borexte+2);
  for (i=0; i<3; i++) {
    Yboreinert[i] = 0.0;
    for (j=0; j<3; j++) {
      Yboreinert[i] = Yboreinert[i] + att[j][i] * Yborexte[j];
    }
  }

/*  Find dec coordinate of offset position  */
  angle = Yboreinert[2];
  if (angle > 1.0) angle = 1.0;
  if (angle < -1.0) angle = -1.0;
        dec[1] = crd * asin(angle);

/*  Find ra coordinate of boresight  */
        ay = Yboreinert[1];
        by = fabs(ay);
  ax = Yboreinert[0];
  bx = fabs(ax);
  angle = by / bx;
  angle = atan(angle);
  raa = 0.0;
        if ((ax == 0.0) && (ay > 0.0)) raa = pi / 2.0;
        if ((ax == 0.0) && (ay < 0.0)) raa = (3.0 * pi) / 2.0;
        if ((ax > 0.0) && (ay >= 0.0)) raa = angle;
        if ((ax < 0.0) && (ay >= 0.0)) raa = pi - angle;
        if ((ax < 0.0) && (ay <= 0.0)) raa = pi + angle;
        if ((ax > 0.0) && (ay <= 0.0)) raa = p2 - angle;
        ra[1] = raa * crd;

/*  Calculate position angle from the two positions  */
  ax = ra[1] - ra[0];
  bx = fabs(ax);
  ay = dec[1] - dec[0];
  by = fabs(ay);
  angle = bx / by;
  angle = atan(angle);
  raa = 0.0;
        if ((ax == 0.0) && (ay > 0.0)) raa = 0.0;
        if ((ax == 0.0) && (ay < 0.0)) raa = pi;
        if ((ax > 0.0) && (ay >= 0.0)) raa = angle;
        if ((ax > 0.0) && (ay <= 0.0)) raa = pi - angle;
        if ((ax < 0.0) && (ay <= 0.0)) raa = pi + angle;
        if ((ax < 0.0) && (ay >= 0.0)) raa = p2 - angle;
        *rockPAout = raa * crd;

      // this part is optional -- dh
      if( sunvec ) {
/*  Compute roll bias angle                     */
/*                                              */
/*  Transform solar vector to spacecraft frame  */
/*  Scale first by modulus                      */ 
  xnorm = 0.0;
  for (i=0; i<3; i++) xnorm = xnorm + (*(sunvec+i) * *(sunvec+i));
  for (i=0; i<3; i++) sunn[i] = *(sunvec+i) / xnorm;
/*  Transform to spacecraft frame  */
  for (i=0; i<3; i++) {
    sunbody[i] = 0.0;
    for (j=0; j<3; j++) {
      sunbody[i] = sunbody[i] + (att[i][j] * sunn[j]);
    }
  }
        ay = sunbody[1];
  az = sunbody[2];
  angle = ay / az;
  angle = atan(angle);
  roll = 0.0;
        if ((ay == 0.0) && (az > 0.0)) roll = 0.0;
        if ((ay == 0.0) && (az < 0.0)) roll = pi;
        if ((ay > 0.0) && (az >= 0.0)) roll = angle;
        if ((ay < 0.0) && (az >= 0.0)) roll = angle;
/*  The next line was wrong. It was corrected on 11/9/95       */
/*  It was noticed by Tod Strohmayer & changed by Barry Giles  */
/*      if ((ay < 0.0) && (az <= 0.0)) roll = pi - angle;       */
        if ((ay < 0.0) && (az <= 0.0)) roll = angle - pi;
        if ((ay > 0.0) && (az <= 0.0)) roll = pi + angle;
        roll = roll * crd;
  *rollout = roll;
      }
}

void SciAttPos::_bsearth(double *bselimb, const double *boreinert,
                         const double *rxte)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*                                                                            */
/*  Computes the angle between the inertial boresight vector and the          */
/*  negative of the spacecraft position vector                                */
/*                                                                            */
/*  The output angle ranges from -half angle subtended by earth to 180-half   */
/*  angle subtended by earth, in degrees                                      */
/*                                                                            */
/*  Inputs:                                                                   */
/*    rxte[3]       contains the x,y,z components of the spacecraft position  */
/*                  vector                                                    */
/*    boreinert[3]  contains the components of the boresight in the inertial  */
/*                  frame obtained from the above routine                     */
/*                  i.e. call radecbore before calling this routine           */
/*                                                                            */
/*  Outputs:                                                                  */
/*    bselimb       contains the output angle                                 */
/*                                                                            */
/*                                                                            */
/*  Development:                                                              */
/*    Fortran version by Tod Strohmayer(USRA) - PCA team    early June 1995   */
/*                                                                            */
/*    Converted to C by Barry Giles(USRA) - PCA team         13th June 1995   */
/*                                                                            */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
{
  double re, pi, crd;
  double xnorm, dot, angle, cang, csub, a;
  int i;

  re = 6378.16;
  pi = M_PI; // 3.141592653589793;
  crd = 180.0 / pi;

/*  Compute angle between boresight (boreinert) and -xte position vector  */
  xnorm = 0.0;
  dot = 0.0;
  for (i=0; i<3; i++) {
    a = *(rxte+i);
    xnorm = xnorm + (a * a);
  }
  xnorm = sqrt(xnorm);     
  for (i=0; i<3; i++) {
    dot = dot + ( (-1.0 * *(rxte+i)) * *(boreinert+i) );
  }
  dot = dot / xnorm;
  cang = acos(dot);

/*  Compute half angle subtended by earth  */
  angle = re / xnorm;
  csub = asin(angle);

/*  Compute output angle  */
  angle = cang - csub;
  angle = crd * angle;
  *bselimb = angle;
}

static void LL2Rec(double Lat, double Long, double Alt,
       double *X, double *Y, double *Z, int Ellipse = 0) {
/****************\
*     LL2Rec     *
*     ------     *
*      Convert from latitude, longitude and altitude to rectangular 
* coordinates.  Latitude and Longitude are in degrees.  Altitude, X, Y, and Z
* are in kilometers.  If Ellipse is 0 then Altitude is the altitude above a
* spherical Earth, otherwise Altitude is the altitude above the surface of an
* ellipsoidal Earth.
\****************/
  static double EqRad = 6378.16;
  static double PolRad = 6356.775;
  static double AvgRad = 6371.2;

  double RadDeg, CosLat, SinLat, Radius;
  
  RadDeg = 4.0 * atan(1.0) / 180;

  if (Ellipse)
  {
    CosLat = cos(Lat * RadDeg);
    SinLat = sin(Lat * RadDeg);
    Radius = sqrt(EqRad * EqRad - 
      (EqRad * EqRad - PolRad * PolRad) * SinLat * SinLat);

    *Z = (Alt + PolRad * PolRad / Radius) * SinLat;
    *X = (Alt + EqRad * EqRad / Radius) * CosLat * cos(Long * RadDeg);
    *Y = (Alt + EqRad * EqRad / Radius) * CosLat * sin(Long * RadDeg);
  }
  else
  {
    *Z = (Alt + AvgRad) * sin(Lat * RadDeg);
    *X = (Alt + AvgRad) * cos(Lat * RadDeg) * cos(Long * RadDeg);
    *Y = (Alt + AvgRad) * cos(Lat * RadDeg) * sin(Long * RadDeg);
  }
}

static double McIlwainDipole(double X, double Y, double Z) {
/********************\
*      McIlwain      *
*      --------      *
*      Computes McIlwain L from the magnetic field of the Earth modelled as a
* dipole offset from the Earth's center.  "X", "Y" and "Z" are Earth-centered 
* coordinates in the right-handed coordinate system given by an x-axis through
* the prime meridian and a z-axis through the North pole.  The return value is
* MacIlwain L for (X, Y, Z) according to the model.
* X, Y & Z are provided in km
*
* The Model (IGRF90 + IGRF90S * 6 years):
*     Earth's radius      = 6371.2 km
*     Model epoch         = 1996.0
*     Model dipole moment = 7.788e+22 A m^2
*     Model dipole tilt   = 10.70 degrees from N
*                           71.44 degrees W of the prime meridian
*     Model dipole offset = (-401.2, 285.5, 194.6) km
*                           (This coordinate system is the right-handed system
*                            where the x-axis is through the prime meridian and
*                            the z-axis is through the North pole.  This offset
*                            is an offset of 529.5 km along the radial line 
*                            through 21.6 degrees N and 144.6 degrees E)
*
* Caveat:
*     The median difference between this calculation and a calculation of L
*     from the complete IGRF model is 3% for a satellite at 600 km altitude 
*     with an orbital inclination of 25 degrees.  The maximum difference is 
*     20%.
*
* References:
*     Model source: IGRF 1991
*         Langel, R. A., "International Geomagnetic Reference Field, 1991
*             Revision," J. Geomag. Geoelectr., 43, 1007, 1991
*         Also available via FTP from nssdca.gsfc.nasa.gov in /models/igrf
*     Offset dipole model
*         Fraser-Smith, R. A., "Centered and Eccentric Geomagnetic Dipoles
*             and Their Poles," Rev. Geophys., 25, 1, 1987
*
* Modifications:
*     17 Aug 1995 - M. J. Stark
*
* Variables:
*    dR - (Model parameter) The offset of the model dipole from the center of 
*         the Earth in units of Earth radii.  (The coordinate system is 
*         described above.)
*    i - An expression of my ego. ("You can count on 'i'")
*    R - The normalized coordinates of the input point with respect to the 
*        offset dipole. (Not tilted)
*    Radius - (Model parameter) The radius of the Earth for the purpose of
*             normalization of the model coordinates.
*    RD - The normalized distance from the dipole to the input point.
*    Rot - (Model parameter) The matrix defining the rotation from the 
*          coordinate system of the input point to the tilted dipole 
*          coordinates.
*    XD - The normalized coordinates of the input point in the tilted, offset-
*         dipole-centered coordinate system.
*
*
*   Test values for McIlwain():
*
*       X         Y         Z       L
*   -------------------------------------
*       0     -6318.05  -2946.16  1.23241
*       0     -6971.2       0     1.16837
*       0     -6318.05   2946.16  1.59459
*   -6318.05      0     -2946.16  1.41255
*   -6971.2       0         0     1.03936
*   -6318.05      0      2946.16  1.18583
*       0      6318.05  -2946.16  1.68792
*       0      6971.2       0     1.09581
*       0      6318.05   2946.16  1.11252
*    6318.05      0     -2946.16  1.33354
*    6971.2       0         0     1.16027
*    6318.05      0      2946.16  1.40468
*
\*********************/
  static double Radius = 6371.2;
  static double dR[3] = {-0.0629688, 0.0448155, 0.0305465};
 
  static double Rot[3][3] =
  {
    {0.31283, -0.931479, -0.185702},
    {0.947967, 0.318367, 0},
    {0.0591213, -0.176039, 0.982606}
  };

  double R[3], RD, XD[3];
  int i;

  R[0] = X / Radius;
  R[1] = Y / Radius;
  R[2] = Z / Radius;
  for (i = 0; i < 3; i++)
    R[i] -= dR[i];
  RD = sqrt(R[0] * R[0] + R[1] * R[1] + R[2] * R[2]);
  for (i = 0; i < 3; i++)
    XD[i] = Rot[i][0] * R[0] + Rot[i][1] * R[1] + Rot[i][2] * R[2];
  return RD / (1.0 - XD[2] * XD[2] / RD / RD);
}

double SciAttPos::McIlwain_L(double lat, double lon, double alt) {
  double x, y, z;
  LL2Rec(lat, lon, alt, &x, &y, &z);

  return McIlwainDipole(x, y, z);
}

void SciAttPos::_init() { 
  if( _borePca.length() <= 0 || _boreHexte.length() <= 0 ) {
    _borePca.resize(3);
    _boreHexte.resize(3);
    char *sochome = getenv("SOCHOME");
    RWCString file("PCAboreError.dat");
    if( sochome ) {
      file.prepend("/");
      file.prepend(sochome);
    }
    _pcaGetError(1, _borePca.data(), file);
    // for now, assume hexte and pca share boresight info:
    _boreHexte = _borePca;
  }
}

double SciAttPos::_boreSight(const DoubleVec& sunv, DoubleVec& bore,
          DoubleVec& radec, DoubleVec& inertial) const {
  double *sunvec=0, ra=0.0, dec=0.0, rock, roll, quat[4];
  inertial.resize(3);
  inertial = DoubleVec(3, 0.0);
  FloatVec q(4,0.0), qRms(4,0.0), qErr(4,0.0);
  int valid = getQuaternions(q, qRms, qErr);

  //cerr << "SciAttPos::_boreSight> q= " << q << endl;

  if( sunv.length() == 3 )
    sunvec = (double* ) sunv.data();

  //if( valid ) {
    quat[0] = q[0]; quat[1] = q[1]; quat[2] = q[2]; quat[3] = q[3];
    _radecbore(&ra, &dec, &rock, &roll, inertial.data(), bore.data(), quat, sunvec);
  //}
  radec.resize(2);
  radec[0] = ra;
  radec[1] = dec;

  double retVal = 0;
  if( sunvec )
    retVal = roll;
  else
    retVal =  rock;

  return retVal;
}

double SciAttPos::_boreSightStats(DoubleVec& bore,
               FloatVec& radecMin, FloatVec& radecMax) {
  double ra=0.0, dec=0.0, rock, roll, quat[4];
  DoubleVec inertial(3, 0.0);
  for( int i = 0; i < _quat.entries(); i++ ) {
    FloatVec& q = *(_quat[i]);
    quat[0] = q[0]; quat[1] = q[1]; quat[2] = q[2]; q[3] = quat[3];
    _radecbore(&ra, &dec, &rock, &roll, inertial.data(), bore.data(), quat, 0);
    _ra[i] = ra;
    _dec[i] = dec;
  }
  radecMin.resize(2);
  radecMin[0] = minValue(_ra);
  radecMin[1] = minValue(_dec);
  radecMax.resize(2);
  radecMax[0] = maxValue(_ra);
  radecMax[1] = maxValue(_dec);

  // don't forget to return the jitter:
  double radiff = radecMax[0]-radecMin[0];
  double decdiff = radecMax[1]-radecMin[1];
  return 3600.0*sqrt( radiff*radiff + decdiff*decdiff ); // units of arc sec.
}


double SciAttPos::xteBoreSight(const DoubleVec& sunv,
                                     DoubleVec& radec,
                                     DoubleVec& inertial) const
{
  return _boreSight(sunv, (DoubleVec&)_boreXte, radec, inertial);
}

double SciAttPos::pcaBoreSight(DoubleVec& radec, DoubleVec& inertial) {
  _init();
  DoubleVec sunv;
  return _boreSight(sunv, _borePca, radec, inertial);
}

double SciAttPos::hexteBoreSight(DoubleVec& radec, DoubleVec& inertial) {
  _init();
  DoubleVec sunv;
  return _boreSight(sunv, _boreHexte, radec, inertial);
}

int SciAttPos::_boreEarthAng(DoubleVec& bea, const DoubleVec& inertial) const { 
  FloatVec startLatLon, endLatLon;
  float alt;
  getPosition(startLatLon, endLatLon, alt);

  bea.resize(2);
  double ang, pos[3];

  // assuming lat-lon is in degrees?
  pos[2] = sin(startLatLon[0]/57.3);
  pos[0] = cos(startLatLon[0]/57.3)*cos(startLatLon[1]/57.3);
  pos[1] = cos(startLatLon[0]/57.3)*sin(startLatLon[1]/57.3);
  _bsearth(&ang, inertial.data(), pos);
  bea[0] = ang; // start time (of partition)

  pos[2] = sin(endLatLon[0]/57.3);
  pos[0] = cos(endLatLon[0]/57.3)*cos(endLatLon[1]/57.3);
  pos[1] = cos(endLatLon[0]/57.3)*sin(endLatLon[1]/57.3);
  _bsearth(&ang, inertial.data(), pos);
  bea[1] = ang; // end 

  return bea.length();
}

int SciAttPos::pcaBoreEarthAng(DoubleVec& bea, DoubleVec* inertial) {
  DoubleVec radec, inertialTmp;
  if( inertial == 0 ) {
    pcaBoreSight(radec, inertialTmp);
    inertial = &inertialTmp;
  }

  return _boreEarthAng(bea, *inertial);
} 

int SciAttPos::hexteBoreEarthAng(DoubleVec& bea, DoubleVec* inertial) {
  DoubleVec radec, inertialTmp;
  if( inertial == 0 ) {
    hexteBoreSight(radec, inertialTmp);
    inertial = &inertialTmp;
  }

  return _boreEarthAng(bea, *inertial);
} 

void SciAttPos::_clear() {
  _latlon.clear();
  _quat.clear();
  _qerr.clear();
  _altitude.resize(0);
  _ra.resize(0);
  _dec.resize(0);
  _insertions.resize(0);
}

IntVec& SciAttPos::insert(double time,
      const FloatVec* latlon,
      float altitude,
      const FloatVec* q,
      const FloatVec* qErr) {
  if( time <= sum(_timeAndDuration) ) { // insert values of interest:
    if( _insertions.length() <= 0 )
      _insertions.resize(4);
    if( latlon ) {
      if( latlon->length() == 2 ) {
  _latlon.insert((FloatVec* )latlon);
  _insertions[0] += 1;
      }
    }
    if( altitude > 0.0 ) {
      _altitude.resize(_altitude.length() + 1);
      _altitude[_altitude.length()-1] = altitude;
      _insertions[1] += 1;
    }

    if( q ) {
      if( q->length() == 4 ) 
  if( fabs(1.0 - fabs(dot(*q,*q))) < 0.0001 ) {
    _quat.insert((FloatVec* )q);
          _insertions[2] += 1;
  }
  else
    cerr << "SciAttPos::insert> quaternion not normalized?" << endl;
    }

    if( qErr ) {
      if( qErr->length() == 4 ) _qerr.insert((FloatVec* )qErr);
      _insertions[3] += 1;
    }
  }
  else { // assume all values of interest have been placed into orderedvecs:
    if( _latlon.entries() > 1 ) {
      setPosition(*(_latlon.first()), *(_latlon.last()), mean(_altitude));
    }
    else if( _latlon.entries() == 1 ) {
      setPosition(*(_latlon[0]), *(_latlon[0]), _altitude[0]);
    }
    FloatVec qAvg(4, 0.0), qRms(4, 0.0), qErr;
    if( _quat.entries() > 1 ) {
      _ra.resize(_quat.entries());
      _dec.resize(_quat.entries());
      FloatVec q0(_quat.entries(), 0.0);
      FloatVec q1(_quat.entries(), 0.0);
      FloatVec q2(_quat.entries(), 0.0);
      FloatVec q3(_quat.entries(), 0.0);
      for( int i = 0; i < _quat.entries(); ++i ) {
  FloatVec& val = *(_quat[i]);
  q0[i] = val[0];
  q1[i] = val[1];
  q2[i] = val[2];
  q3[i] = val[3];
      }
      qAvg[0] = mean(q0); qRms[0] = sqrt(variance(q0));
      qAvg[1] = mean(q1); qRms[1] = sqrt(variance(q1));
      qAvg[2] = mean(q2); qRms[2] = sqrt(variance(q2));
      qAvg[3] = mean(q3); qRms[3] = sqrt(variance(q3));
      // set the state:
      setQuaternions(qAvg, qRms, qErr);

      // finally, find ra-dec info:
      FloatVec radecMin, radecMax;
      float jitter;
      jitter = _boreSightStats(_boreXte, radecMin, radecMax);
      setRADecXte(radecMin, radecMax, jitter);

      jitter = _boreSightStats(_borePca, radecMin, radecMax);
      setRADecPca(radecMin, radecMax, jitter);

      jitter = _boreSightStats(_boreHexte, radecMin, radecMax);
      setRADecHexte(radecMin, radecMax, jitter);

      // once state is set, clear the buffers
      _clear();
    }
    else if( _quat.entries() == 1 ) {
      double ra=0.0, dec=0.0, rock, roll, quat[4];
      _ra.resize(1);
      _dec.resize(1);
      float jitter = 0.0;
      FloatVec radec(2, 0.0);
      qAvg = *(_quat[0]);
      FloatVec& q = qAvg;
      quat[0] = q[0]; quat[1] = q[1]; quat[2] = q[2]; quat[3] = q[3];
      
      setQuaternions(qAvg, qRms, qErr);
      // check that the set did not modify q:
      //quat[0] = q[0]; quat[1] = q[1]; quat[2] = q[2]; quat[3] = q[3];
      DoubleVec inertial(3, 0.0);
      _radecbore(&ra, &dec, &rock, &roll, inertial.data(), _boreXte.data(), quat, 0);
      radec[0] = ra;
      radec[1] = dec;
      setRADecXte(radec, radec, jitter);
      _radecbore(&ra, &dec, &rock, &roll, inertial.data(), _borePca.data(), quat, 0);
      radec[0] = ra;
      radec[1] = dec;
      setRADecPca(radec, radec, jitter);
      _radecbore(&ra, &dec, &rock, &roll, inertial.data(), _boreHexte.data(), quat, 0);
      radec[0] = ra;
      radec[1] = dec;
      setRADecHexte(radec, radec, jitter); 
    }
  }
  return (IntVec&) _insertions;
}

void SciAttPos::evaluate() {
  if( _insertions.length() > 0 ) {
    IntVec iv = insert(1.0+sum(_timeAndDuration), 0, 0, 0, 0);
    cerr << "SciAttPos::evaluate> number of insertions: " << iv << endl;
  }
  else
    cerr << "SciAttPos::evaluate> cannot evaluate, no insertions!" << endl;
}

SOCbostream& operator<<( SOCbostream& bos, const SciAttPos& ap ) {
  MeanAttPos *map = (MeanAttPos*) &ap;

  bos << *map;
  bos << ap._boreXte << ap._borePca << ap._boreHexte;

  return bos;
}

SOCpostream& operator<<( SOCpostream& pos, const SciAttPos& ap ) {
  MeanAttPos *map = (MeanAttPos*) &ap;

  pos << *map;
  pos << ap._boreXte << ap._borePca << ap._boreHexte;

  return pos;
}

SOCbistream& operator>>( SOCbistream& bis, SciAttPos& ap ) {
  MeanAttPos *map = (MeanAttPos*) &ap;

  bis >> *map;
  bis >> ap._boreXte >> ap._borePca >> ap._boreHexte;

  return bis;
}

SOCpistream& operator>>( SOCpistream& pis, SciAttPos& ap ) {
  MeanAttPos *map = (MeanAttPos*) &ap;

  pis >> *map;
  pis >> ap._boreXte >> ap._borePca >> ap._boreHexte;

  return pis;
}

@

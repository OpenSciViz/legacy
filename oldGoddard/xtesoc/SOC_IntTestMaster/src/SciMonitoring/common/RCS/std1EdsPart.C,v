head	5.9;
access
	soccm
	jschwan;
symbols;
locks
	jschwan:5.9; strict;
comment	@ * @;


5.9
date	97.02.25.22.20.33;	author jschwan;	state Exp;
branches;
next	;


desc
@class definition
@


5.9
log
@removed inlines
@
text
@// RCS: $Id$

#include "std1EdsPart.h"

std1EdsPart::std1EdsPart(): EdsPartition(),
                            SciAncilBits(),
			    ExpectCounts(),
			    MeanAttPos(),
			    EarthMoonSun() { theFFT = NULL; }


std1EdsPart::std1EdsPart(const EdsPartition& eds): EdsPartition(eds),
                         SciAncilBits(),
			 ExpectCounts(),
			 MeanAttPos(),
			 EarthMoonSun() { theFFT = NULL; }

std1EdsPart::std1EdsPart(const EdsPartition& eds,
			 SciAncilBits& bits,
			 ExpectCounts& ec,
			 MeanAttPos& ap,
			 EarthMoonSun& ems): EdsPartition(eds),
			                     SciAncilBits(bits),
					     ExpectCounts(ec),
					     MeanAttPos(ap),
					     EarthMoonSun(ems) { theFFT = NULL; }

std1EdsPart::std1EdsPart(std1EdsPart& rhs): EdsPartition(),
                                            SciAncilBits(),
				            ExpectCounts(),
				            MeanAttPos(),
				            EarthMoonSun() {
 
  if( this != &rhs )
    *this = rhs;
}

void std1EdsPart::xenon(int pcuid, unsigned short* theDataBuf, double *theCounts) {
//  if( theDataBuf == NULL || theCounts == NULL ) 
//    return; // do nothing!

  switch( pcuid ) {
    case 1: theDataBuf += xenon1OffsetStd1;
            break;
    case 2: theDataBuf += xenon2OffsetStd1;
            break;
    case 3: theDataBuf += xenon3OffsetStd1;
            break;
    case 4: theDataBuf += xenon4OffsetStd1;
            break;
    case 5: theDataBuf += xenon5OffsetStd1;
            break;
    default:
	    break;
  }

#ifndef TESTME
  for( int i = 0; i < std1NumTVals; i++ ) 
    *theCounts++ = *theDataBuf++;
#else
  for( int i = 0; i < std1NumTVals; i++ ) {
    *theCounts = *theDataBuf++; // do the access to check for problems, then replace
    *theCounts++ = pcuid*10.0*(1.0+cos(M_PI*i/std1NumTVals));
  }
#endif

}

void std1EdsPart::vle(unsigned short* theDataBuf, double *theCounts) {
//  if( theDataBuf == NULL || theCounts == NULL ) 
//    return; // do nothing!

  theDataBuf += vleOffsetStd1;

#ifndef TESTME
  for( int i = 0; i < std1NumTVals; i++ ) 
    *theCounts++ = *theDataBuf++;
#else
  for( int i = 0; i < std1NumTVals; i++ ) {
    *theCounts = *theDataBuf++; // do the access to check for problems, then replace
    *theCounts++ = 60.0*(1.0+cos(M_PI*i/std1NumTVals));
  }
#endif
}

void std1EdsPart::propane(unsigned short* theDataBuf, double *theCounts) {
//  if( theDataBuf == NULL || theCounts == NULL ) 
//    return; // do nothing!

  theDataBuf += propaneOffsetStd1;

#ifndef TESTME
  for( int i = 0; i < std1NumTVals; i++ ) 
    *theCounts++ = *theDataBuf++;
#else
  for( int i = 0; i < std1NumTVals; i++ ) {
    *theCounts = *theDataBuf++; // do the access to check for problems, then replace
    *theCounts++ = 70.0*(1.0+cos(M_PI*i/std1NumTVals));
  }
#endif

}

void std1EdsPart::other(unsigned short* theDataBuf, double *theCounts) {
//  if( theDataBuf == NULL || theCounts == NULL ) 
//    return; // do nothing!

  theDataBuf += otherOffsetStd1;

#ifndef TESTME
  for( int i = 0; i < std1NumTVals; i++ ) 
    *theCounts++ = *theDataBuf++;
#else
  for( int i = 0; i < std1NumTVals; i++ ) {
    *theCounts = *theDataBuf++; // do the access to check for problems, then replace
    *theCounts++ = 70.0*(1.0+cos(M_PI*i/std1NumTVals));
  }
#endif
}

void std1EdsPart::calibSpectra(int pcuid, unsigned short* theDataBuf,
			  double *theSpectraL[3], double *theSpectraR[3]) {
//  if( theDataBuf == NULL || theSpectraL == NULL || theSpectraR ) 
//    return; // do nothing!

  switch( pcuid ) {
    case 1: theDataBuf += calib1OffsetStd1;
            break;
    case 2: theDataBuf += calib2OffsetStd1;
            break;
    case 3: theDataBuf += calib3OffsetStd1;
            break;
    case 4: theDataBuf += calib4OffsetStd1;
            break;
    case 5: theDataBuf += calib5OffsetStd1;
            break;
    default:
	    break;
  }
  // storage is in (left, right) tuples...
#ifndef TESTME
  for( int layer = 0; layer < 3; layer++ ) {   
    for( int i = 0; i < std1NumEVals; i++ ) 
      *theSpectraL[layer]++ = *theDataBuf++;

    for( i = 0; i < std1NumEVals; i++ ) 
      *theSpectraR[layer]++ = *theDataBuf++;
  }
#else
  for( int layer = 0; layer < 3; layer++ ) { 
    for( int i = 0; i < std1NumEVals; i++ ) {
      *theSpectraL[layer] = *theDataBuf++;
      *theSpectraL[layer]++ = (layer+1)*10.0*(1.0+cos(M_PI*i/std1NumEVals));
    }
    for( i = 0; i < std1NumEVals; i++ ) {
      *theSpectraR[layer] = *theDataBuf++;
      *theSpectraR[layer]++ = (layer+1)*10.0*(1.0+cos(M_PI*i/std1NumEVals));
    }
  }
#endif
}

void std1EdsPart::allTiming(DoubleGenMat& xenonCounts,
		      DoubleVec& vleCounts,
                      DoubleVec& propaneCounts,
                      DoubleVec& otherCounts,
                      DoubleVec& xenonSum) {

  xenonCounts.resize(std1NumTVals, 5);
  vleCounts.resize(std1NumTVals);
  propaneCounts.resize(std1NumTVals);
  otherCounts.resize(std1NumTVals);
  xenonSum.resize(std1NumTVals);

  int pcuid;
  unsigned short* theData = getData();
  for( pcuid = 0; pcuid < 5 ; pcuid++ ) { // short to double conversion
    DoubleVec pcu(xenonCounts(RWAll,pcuid));
    xenon(pcuid, theData, pcu.data());
  }
// reset VLE, propane, other and sum of all xenons
  vle(theData, vleCounts.data());
  propane(theData, propaneCounts.data());
  other(theData, otherCounts.data());

//compute sum of all xenons
  xenonSum = 0.0; 
  for( pcuid = 0; pcuid < 5 ; pcuid++ ) { // short to int conversion
     xenonSum += xenonCounts(RWAll,pcuid); 
  }
}

int std1EdsPart::sendData(SOCbostream& bos) {
  int retVal = 1;
  int fd = bos.fd();
  unsigned short *unpackedData = getData();
  unsigned long t = getStartTime();
  write_bytes(fd, (const unsigned char*)&t, sizeof(long));
  int nb = write_bytes(fd, (const unsigned char*)unpackedData, std1PartBuffSize);
  bos.flush();
  delete unpackedData;

  if( nb < std1PartBuffSize )
    retVal = -1;

  return retVal;
}

int std1EdsPart::sendData(SOCbostream& bos,
	   const SciAncilBits& bits, const ExpectCounts& ec, 
	   const MeanAttPos& ap, const EarthMoonSun& ems) {
  int retVal = 1;
  int fd = bos.fd();
  unsigned long t = getStartTime();
  write_bytes(fd, (const unsigned char*)&t, sizeof(long));

  bos << bits;
  bos << ec;
  bos << ap;
  bos << ems;

  unsigned short *unpackedData = getData(); 
  int nb = write_bytes(fd, (const unsigned char*)unpackedData, std1PartBuffSize);
  bos.flush();
  delete unpackedData;

  if( nb < std1PartBuffSize )
    retVal = -1;

  return retVal;
}


int std1EdsPart::restoreData(SOCbistream& bis, unsigned short *theDataBuf, unsigned long& time) {
  int retVal = 1;
  int fd = bis.fd();
  read_bytes(fd, (unsigned  char *)&time, sizeof(long));
  int nb = read_bytes(fd, (unsigned char*)theDataBuf, std1PartBuffSize);

  if( nb < std1PartBuffSize )
    retVal = -1;

  return retVal;
}

int std1EdsPart::restoreData(SOCbistream& bis, unsigned short *theDataBuf,
	      SciAncilBits& bits, ExpectCounts& ec, MeanAttPos& ap, 
	      EarthMoonSun& ems, unsigned long& time) {
  int retVal = 1;
  int fd = bis.fd();
  read_bytes(fd, (unsigned  char *) &time, sizeof(long));

  bis >> bits;
  bis >> ec;
  bis >> ap;
  bis >> ems;

  int nb = read_bytes(fd, (unsigned char *)theDataBuf, std1PartBuffSize);

  if( nb < std1PartBuffSize )
    retVal = -1;

  return retVal;
}

ostream& operator<<( ostream& os, const std1EdsPart& wa ) {
  SciAncilBits *bits = (SciAncilBits* ) &wa;
  os << *bits;

  ExpectCounts *ec = (ExpectCounts* ) &wa;
  os << *ec;

  MeanAttPos *ap = (MeanAttPos* ) &wa;
  os << *ap;

  EarthMoonSun *ems = (EarthMoonSun* ) &wa;
  os << *ems;

  EdsPartition *eds = (EdsPartition* ) &wa;
//  os << *eds;

  return os;
}

SOCbostream& operator<<( SOCbostream& bos, const std1EdsPart& wa ) {
  SciAncilBits *bits = (SciAncilBits* ) &wa;
  bos << *bits;

  ExpectCounts *ec = (ExpectCounts* ) &wa;
  bos << *ec;

  MeanAttPos *ap = (MeanAttPos* ) &wa;
  bos << *ap;

  EarthMoonSun *ems = (EarthMoonSun* ) &wa;
  bos << *ems;

  EdsPartition *eds = (EdsPartition* ) &wa;
  bos << *eds;

  return bos;
}

SOCpostream& operator<<( SOCpostream& pos, const std1EdsPart& wa ) {
  SciAncilBits *bits = (SciAncilBits* ) &wa;
  pos << *bits;

  ExpectCounts *ec = (ExpectCounts* ) &wa;
  pos << *ec;

  MeanAttPos *ap = (MeanAttPos* ) &wa;
  pos << *ap;

  EarthMoonSun *ems = (EarthMoonSun* ) &wa;
  pos << *ems;

  EdsPartition *eds = (EdsPartition* ) &wa;
  pos << *eds;

  return pos;
}

SOCbistream& operator>>( SOCbistream& bis, std1EdsPart& wa ) {
  SciAncilBits *bits = (SciAncilBits* ) &wa;
  bis >> *bits;

  ExpectCounts *ec = (ExpectCounts* ) &wa;
  bis >> *ec;

  MeanAttPos *ap = (MeanAttPos* ) &wa;
  bis >> *ap;

  EarthMoonSun *ems = (EarthMoonSun* ) &wa;
  bis >> *ems;

  EdsPartition *eds = (EdsPartition* ) &wa;
  bis >> *eds;

  return bis;
}

SOCpistream& operator>>( SOCpistream& pis, std1EdsPart& wa ) {
  SciAncilBits *bits = (SciAncilBits* ) &wa;
  pis >> *bits;

  ExpectCounts *ec = (ExpectCounts* ) &wa;
  pis >> *ec;

  MeanAttPos *ap = (MeanAttPos* ) &wa;
  pis >> *ap;

  EarthMoonSun *ems = (EarthMoonSun* ) &wa;
  pis >> *ems;

  EdsPartition *eds = (EdsPartition* ) &wa;
  pis >> *eds;

  return pis;
}


int std1EdsPart::leahyPower(const DoubleVec& lightCurve, DoubleVec& power) {

  power.resize(lightCurve.length()/2 + 1);

  if( theFFT == NULL ) 
    theFFT = new DoubleFFTServer;

  double total = sum(lightCurve); 
  if( total != 0.0 ) {
    double leahy = 2.0/total;
    power = leahy*norm(theFFT->fourier(lightCurve));
    return power.length();
  }
  else {
    power = 0.0;
    return 0;
  }
}

int std1EdsPart::power0dc(const DoubleVec& lightCurve, DoubleVec& power) {
  int retVal;
  power.resize(lightCurve.length()/2 + 1);

  if( theFFT == NULL ) 
    theFFT = new DoubleFFTServer;

  double avg = mean(lightCurve);

  if( avg != 0.0 ) {
    power = norm(theFFT->fourier(lightCurve-avg));
    power = power / maxValue(power);
    retVal = power.length();
  }
  else {
    power = 0.0;
    retVal = 0;
  }
  return retVal;
}
@

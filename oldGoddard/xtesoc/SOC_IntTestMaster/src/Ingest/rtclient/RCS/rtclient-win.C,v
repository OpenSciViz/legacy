head	1.11;
access
	rbarnett
	soccm;
symbols
	Build4:1.11
	Build3_3:1.11
	Build3_2:1.11
	Build3_1:1.11
	Build3:1.11
	Current:1.11;
locks; strict;
comment	@ * @;


1.11
date	94.04.04.14.04.44;	author lijewski;	state Exp;
branches;
next	1.10;

1.10
date	94.03.16.22.56.32;	author lijewski;	state Exp;
branches;
next	1.9;

1.9
date	94.02.02.23.13.15;	author lijewski;	state Exp;
branches;
next	1.8;

1.8
date	94.01.31.15.49.30;	author lijewski;	state Exp;
branches;
next	1.7;

1.7
date	94.01.31.14.30.41;	author lijewski;	state Exp;
branches;
next	1.6;

1.6
date	94.01.21.14.10.14;	author lijewski;	state Exp;
branches;
next	1.5;

1.5
date	94.01.13.22.33.00;	author lijewski;	state Exp;
branches;
next	1.4;

1.4
date	94.01.11.20.57.58;	author lijewski;	state Exp;
branches;
next	1.3;

1.3
date	94.01.11.14.34.59;	author lijewski;	state Exp;
branches;
next	1.2;

1.2
date	94.01.10.22.47.13;	author lijewski;	state Exp;
branches;
next	1.1;

1.1
date	94.01.10.22.32.44;	author lijewski;	state Exp;
branches;
next	;


desc
@initial realtime client display client
@


1.11
log
@simplified read_bytes() switch
@
text
@//
// Sample X window process being driven by realtime client.  Uses the
// X Toolkit Motif.  If you're going to use this as the basis for a realtime
// client X display program, the routines you should look at are:
//
//    MakeWindows()
//    ReadData()
//    UpdateWindows()
//
// Little else should need to change.
//


static const char rcsid[] = "$Id: rtclient-win.C,v 1.10 1994/03/16 22:56:32 lijewski Exp lijewski $";


#include <fcntl.h>
#include <math.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>

#include <Xm/Label.h>

#include "Utilities.h"
#include "xte.xbm"
#include "errorPopup.h"
#include "rtclient-win.h"


#ifndef STDIN_FILENO
#define STDIN_FILENO 0
#endif

#ifndef EXIT_FAILURE
#define EXIT_FAILURE 1
#endif


static char* ProgName;
static XtAppContext appcontext;
static Widget appshell, error_popup, label;


//
// Some colorful fallback resources.  These should match up with whatever
// is in the resource file for your application.  Note that `error*label'
// is the label on the error popup which is instantiated when the pipe to
// the realtime client is broken.  Replace the given string with one
// specialized for your client.
//
static String fallbacks [] =
{
    //
    // Resources for error popup.
    //
    "*error*background:          tomato",
    "*error*foreground:          black",
    "*error*fontList:            lucidasans-bold-24",
    "*error*labelString:         The `rtclient' Process Appears To Have Died.",
    //
    // Resources for the acknowledge button.
    //
    "*acknowledge*fontList:      lucidasans-bold-18",
    "*acknowledge*foreground:    black",
    "*acknowledge*background:    ivory",
    "*acknowledge*labelString:   ---> Acknowledge <---",
    "*acknowledge*showAsDefault: 1",
    0
};


//
// Update the window(s) after receiving some data.
//

static void UpdateWindows (const RTData* data)
{
    //
    // All we do in this simple client is update the label widget.
    //
    static char buf[256];
    static XmString msg;

    (void) sprintf(buf, "App Id = %d, SeqNo = %d, Length = %d, Secs = %lu",
                   data->apid, data->seqno, data->length, data->seconds);

    XmStringFree(msg);
    msg = XmStringCreateSimple(buf);
    XtVaSetValues(label, XmNlabelString, msg, NULL);
}


//
// Read new data.  This routine is called as an alternate input procedure,
// set in main().  The use of an alternate input procedure is really the
// key idea in this client.  It allows the realtime client and its associated
// X window client to run asynchronously.
//

static void ReadData (XtPointer, int* source, XtInputId* id)
{
    RTData data;

    int n = read_bytes(*source, (unsigned char*) &data, sizeof(RTData));

    switch (n)
    {
      case 0:
      {
          //
          // No more input.  Display an error message.
          //
          Display* dsp = XtDisplay(appshell);       // The display
          XtRemoveInput(*id);
          (void) close(*source);
          XMapWindow(dsp, XtWindow(appshell));      // Uniconify the window
          XtSetSensitive(appshell, FALSE);          // Make display insensitive
          XtPopup(error_popup, XtGrabNonexclusive); // Popup the error window
#ifndef DEBUG
          for (int i = 0; i < 5; i++) // Give'm a bit o'noise.
          {
              XBell(dsp, 100); XBell(dsp, 100); XBell(dsp, 100); sleep(1);
          }
#endif
      }
        break;
      case sizeof(RTData):
        UpdateWindows(&data);
        break;
      default:
        XtRemoveInput(*id);
        (void) close(*source);
        error("File %s, line %d, read_bytes() failed; returned %d.",
              __FILE__, __LINE__, n);
        break;
    }
}


static Atom wm_delete_window;


static void quit (Widget w, XEvent* event, String*, Cardinal*)
{
    if (event->type == ClientMessage &&
        event->xclient.data.l[0] != wm_delete_window)
    {
        XBell(XtDisplay(w), 0);
        return;
    }
    XtDestroyApplicationContext(XtWidgetToApplicationContext(w));
    exit(0);
}


static XtActionsRec actions [] = { { "quit", quit } };


//
// Make the window(s) necessary for this client.
//
// For our simple example, all I'll do is make a label widget to which
// I'll output a single string containing the data from the passed
// RTData structure.
//

static void MakeWindows (Widget parent)
{
    label = XtVaCreateManagedWidget("clientLabel", xmLabelWidgetClass, parent,
                                    XmNwidth, 400,
                                    XmNlabelString,
                                    XmStringCreateSimple("Your Message Goes Here."),
                                    XmNalignment, XmALIGNMENT_CENTER,
                                    XmNstringDirection, XmSTRING_DIRECTION_L_TO_R,
                                    XmNborderWidth, 2,
                                    NULL);
}


static void usage (void)
{
    (void) fprintf(stderr, "\nusage: %s\n\n", ProgName);
    exit(EXIT_FAILURE);
}


static void parse_arguments (char* argv[])
{
    //
    // Acceptable arguments -- currently there aren't any.
    //
    if (*++argv && **argv == '-') 
    {
        (void) fprintf(stderr, "%s: unknown option `%s'.\n",
                       ProgName, *argv);
        usage();
    }
}


int main (int argc, char* argv[])
{
    Pixmap xte_pixmap;

    //
    // Set ProgName to tail of argv[0].  This is expected to be the
    // name of the resource file (if one exists) for this application.
    // The resource file should be placed in
    //
    //   $SOCHOME/lib/app-defaults/ProgName
    //
    // You also must make sure that $SOCHOME/lib/app-defaults is in your
    // XUSERFILESEARCHPATH environment variable.
    //
    if ((ProgName = strrchr(argv[0], '/')) == 0)
        ProgName = argv[0];
    else
        ProgName += 1;  // Step past the slash.

    appshell = XtAppInitialize(&appcontext,
                               ProgName,    // The name of resource file..
                               NULL,
                               0,
                               &argc,
                               argv,
                               fallbacks,   // Fallback resources.
                               NULL,
                               0);
    parse_arguments(argv);

    //
    // Ignore keyboard generated kill signals.  If you want to force this
    // process to exit, simply kill the pipe on which it's reading data.
    //
    (void) signal(SIGINT,  SIG_IGN);
    (void) signal(SIGQUIT, SIG_IGN);

    xte_pixmap = XCreateBitmapFromData(XtDisplay(appshell),
                                       RootWindowOfScreen(XtScreen(appshell)),
                                       (char*) xte_bits,
                                       xte_width,
                                       xte_height);
    if (xte_pixmap == 0)
        error("File %s, line %d, couldn't make XTE pixmap.",
              __FILE__, __LINE__);
    else
        XtVaSetValues(appshell, XtNiconPixmap, xte_pixmap, NULL);

    //
    // A hack so that f.delete will do something useful.
    //
    XtAppAddActions (appcontext, actions, XtNumber(actions));
    XtOverrideTranslations(appshell,
                           XtParseTranslationTable("<Message>WM_PROTOCOLS: quit()")); 

    //
    // Make the windows necessary for this client.
    //
    MakeWindows(appshell);

    //
    // Create the popup shell for our error message and set its
    // createPopupChildProc resource.
    //
    error_popup = XtVaCreatePopupShell("error_popup",
                                       transientShellWidgetClass,
                                       appshell,
                                       XtNcreatePopupChildProc,
                                       PopupErrorMessage,
                                       XtNwidth, 200,
                                       XtNheight, 50,
                                       XtNallowShellResize, TRUE,
                                       NULL);
    //
    // We use an alternate input procedure to read from STDIN_FILENO
    // when data becomes available.  This means that standard input of
    // istrip should be connected only to pipes, FIFOs, or sockets.
    // It doesn't really work as we'd like when used with regular files.
    // You can thus readily test this without a realtime client, by piping
    // the required data into this process.  Note that file redirection will
    // NOT work; you must use a pipe.
    //
    XtAppAddInput(appcontext,
                  STDIN_FILENO,
                  (XtPointer) XtInputReadMask,
                  ReadData,
                  (XtPointer) 0);

    XtRealizeWidget(appshell);

    wm_delete_window = XInternAtom (XtDisplay(appshell),
                                    "WM_DELETE_WINDOW",
                                    False);

    (void) XSetWMProtocols (XtDisplay(appshell),
                            XtWindow(appshell),
                            &wm_delete_window, 1);

    XtAppMainLoop(appcontext);

    return 0;
}
@


1.10
log
@substituted read_bytes() for read()
@
text
@d14 1
a14 1
static const char rcsid[] = "$Id: rtclient-win.C,v 1.9 1994/02/02 23:13:15 lijewski Exp lijewski $";
a118 4
      case -1:
        XtRemoveInput(*id);
        (void) close(*source);
        error("File %s, line %d, read_bytes() failed.", __FILE__, __LINE__);
d142 3
a144 1
        error("File %s, line %d, read_bytes() failed; returned %d bytes.",
@


1.9
log
@moved PopupErrorMessage() to -lUtil
@
text
@d14 1
a14 1
static const char rcsid[] = "$Id: rtclient-win.C,v 1.8 1994/01/31 15:49:30 lijewski Exp lijewski $";
d35 1
a82 13
static void error (const char *format, ...)
{
    char buffer[256];
    va_list ap;
    va_start(ap, format);
    (void)  fprintf(stderr, "\n%s: ", ProgName);
    (void) vfprintf(stderr, format, ap);
    (void)  fprintf(stderr, "\n\n");
    va_end(ap);
    exit(EXIT_FAILURE);
}


d115 3
a117 1
    switch (read(*source, (char*) &data, sizeof(RTData)))
d122 1
a122 1
        error("File %s, line %d, read() failed.", __FILE__, __LINE__);
d128 1
d131 3
a133 2
          XtSetSensitive(appshell, FALSE); // Make the display insensitive.
          XtPopup(error_popup, XtGrabNonexclusive);
d135 1
a135 4
          //
          // Give'm a bit o'noise.
          //
          for (int i = 0; i < 5; i++)
d137 1
a137 4
              XBell(XtDisplay(appshell), 100);
              XBell(XtDisplay(appshell), 100);
              XBell(XtDisplay(appshell), 100);
              sleep(1);
d142 3
d146 2
a147 1
        UpdateWindows(&data);
@


1.8
log
@we're now C++ code
@
text
@d14 1
a14 1
static const char rcsid[] = "$Id: rtclient-win.C,v 1.7 1994/01/31 14:30:41 lijewski Exp lijewski $";
a33 2
#include <Xm/PanedW.h>
#include <Xm/PushB.h>
d36 1
a180 63

static void acknowledge_callback (Widget, XtPointer, XtPointer)
{
    exit(0);
}


//
// Pop up an error message when the realtime client dies.  This function is
// the other important idea in this code.  We define a popup that gets
// displayed whenever the connection with the realtime client fails.  This
// notifies the user, in a very conspicuous fashion, that something is amiss.
//

static void PopupErrorMessage (Widget shell)
{
    Widget pane, errorLabel, acknowledge;
    Dimension width, height;
    Position x, y;

    pane = XtVaCreateManagedWidget("paned", xmPanedWindowWidgetClass, shell,
                                   XmNspacing,             0,
                                   XmNsashWidth,           0,
                                   XmNsashHeight,          0,
                                   XmNmarginWidth,         0,
                                   XmNseparatorOn,         FALSE,
                                   XmNallowResize,         TRUE,
                                   XmNrefigureMode,        TRUE,
                                   XmNmarginHeight,        0,
                                   XmNsashShadowThickness, 3,
                                   NULL);

    errorLabel = XtVaCreateManagedWidget("error", xmLabelWidgetClass, pane,
                                         XmNalignment,       XmALIGNMENT_CENTER,
                                         XmNlabelType,       XmSTRING,
                                         XmNborderWidth,     2,
                                         XmNrecomputeSize,   TRUE,
                                         XmNstringDirection, XmSTRING_DIRECTION_L_TO_R,
                                         NULL);

    acknowledge = XtVaCreateManagedWidget("acknowledge", xmPushButtonWidgetClass, pane,
                                          XmNlabelType,       XmSTRING,
                                          XmNmultiClick,      XmMULTICLICK_DISCARD,
                                          XmNborderWidth,     2,
                                          XmNstringDirection, XmSTRING_DIRECTION_L_TO_R,
                                          NULL);

    XtAddCallback(acknowledge, XmNactivateCallback, acknowledge_callback, 0);

    //
    // Get our height and width.
    //
    XtVaGetValues(errorLabel, XtNwidth, &width, XtNheight, &height, NULL);

    //
    // Get the width and height of the screen and then center
    // ourselves on the screen.
    //
    x = (WidthOfScreen(XtScreen(appshell)) - width)   / 2;
    y = (HeightOfScreen(XtScreen(appshell)) - height) / 2;
    XtVaSetValues(shell, XtNx, x, XtNy, y, NULL);
}

@


1.7
log
@made acknowledge button prettier
@
text
@d1 11
a11 11
/*
** Sample X window process being driven by realtime client.  Uses the
** X Toolkit Motif.  If you're going to use this as the basis for a realtime
** client X display program, the routines you should look at are:
**
**    MakeWindows()
**    ReadData()
**    UpdateWindows()
**
** Little else should need to change.
*/
d14 1
a14 1
static char rcsid[] = "$Id: rtclient-win.c,v 1.6 1994/01/21 14:10:14 lijewski Exp lijewski $";
a36 1

d55 7
a61 7
/*
** Some colorful fallback resources.  These should match up with whatever
** is in the resource file for your application.  Note that `error*label'
** is the label on the error popup which is instantiated when the pipe to
** the realtime client is broken.  Replace the given string with one
** specialized for your client.
*/
d64 3
a66 3
    /*
    ** Resources for error popup.
    */
d71 3
a73 3
    /*
    ** Resources for the acknowledge button.
    */
d96 4
d102 3
a104 3
    /*
    ** All we do in this simple client is update the label widget.
    */
d117 6
a122 6
/*
** Read new data.  This routine is called as an alternate input procedure,
** set in main().  The use of an alternate input procedure is really the
** key idea in this client.  It allows the realtime client and its associated
** X window client to run asynchronously.
*/
d124 1
a124 1
static void ReadData (XtPointer client_data, int* source, XtInputId* id)
d136 7
a142 11
        int i;
        /*
        ** No more input.  Display an error message.
        */
        XtRemoveInput(*id);
        (void) close(*source);
        XtSetSensitive(appshell, FALSE); /* Make the display insensitive. */
        XtPopup(error_popup, XtGrabNonexclusive);
        /*
        ** Give'm a bit o'noise.
        */
d144 10
a153 7
        for (i = 0; i < 5; i++)
        {
            XBell(XtDisplay(appshell), 100);
            XBell(XtDisplay(appshell), 100);
            XBell(XtDisplay(appshell), 100);
            sleep(1);
        }
d167 1
a167 1
static void quit (Widget w, XEvent* event, String* string, Cardinal* cardinal)
d183 1
a183 1
static void acknowledge_callback (Widget w, XtPointer client_data, XtPointer call_data)
d189 6
a194 6
/*
** Pop up an error message when the realtime client dies.  This function is
** the other important idea in this code.  We define a popup that gets
** displayed whenever the connection with the realtime client fails.  This
** notifies the user, in a very conspicuous fashion, that something is amiss.
*/
d231 3
a233 3
    /*
    ** Get our height and width.
    */
d236 4
a239 4
    /*
    ** Get the width and height of the screen and then center
    ** ourselves on the screen.
    */
d246 7
a252 7
/*
** Make the windows necessary for this client.
**
** For our simple example, all I'll do is make a label widget to which
** I'll output a single string containing the data from the passed
** RTData structure.
*/
d276 3
a278 3
    /*
    ** Acceptable arguments -- currently there aren't any.
    */
d292 10
a301 10
    /*
    ** Set ProgName to tail of argv[0].  This is expected to be the
    ** name of the resource file (if one exists) for this application.
    ** The resource file should be placed in
    **
    **   $SOCHOME/lib/app-defaults/ProgName
    **
    ** You also must make sure that $SOCHOME/lib/app-defaults is in your
    ** XUSERFILESEARCHPATH environment variable.
    */
d305 1
a305 1
        ProgName += 1;  /* Step past the slash */
d308 1
a308 1
                               ProgName,    /* The name of resource file. */
d313 1
a313 1
                               fallbacks,   /* Fallback resources.        */
d318 4
a321 4
    /*
    ** Ignore keyboard generated signals.  If you want to force this process
    ** to exit, simply kill the pipe on which it's reading data.
    */
a323 1
    (void) signal(SIGTSTP, SIG_IGN);
d336 3
a338 3
    /*
    ** A hack so that f.delete will do something useful.
    */
d343 3
a345 3
    /*
    ** Make the windows necessary for this client.
    */
d348 4
a351 4
    /*
    ** Create the popup shell for our error message and set its
    ** createPopupChildProc resource.
    */
d361 9
a369 9
    /*
    ** We use an alternate input procedure to read from STDIN_FILENO
    ** when data becomes available.  This means that standard input of
    ** istrip should be connected only to pipes, FIFOs, or sockets.
    ** It doesn't really work as we'd like when used with regular files.
    ** You can thus readily test this without a realtime client, by piping
    ** the required data into this process.  Note that file redirection will
    ** NOT work; you must use a pipe.
    */
@


1.6
log
@now works with MOTIF or Athema Widgets
@
text
@d3 2
a4 3
** X Toolkit and the Athena Widgets.  If you're going to use this as the
** basis for a realtime client X display program, the routines you should
** look at are:
a10 7
**
** This is ANSI C code.  It could be turned into C++ fairly easily.  The only
** question is whether or not the X include files and libraries which you'll
** be using can be compiled with a C++ compiler.  Straight X11R5 works
** properly with C++ compilers.  The previous version of the Athena Plotter
** Tools Widget Set did not, though it's rumored that the latest version
** works properly with C++ compilers.
d14 1
a14 1
static char rcsid[] = "$Id: rtclient-win.c,v 1.5 1994/01/13 22:33:00 lijewski Exp lijewski $";
a32 8
#ifdef USE_AW
#include <X11/Xaw/Box.h>
#include <X11/Xaw/Command.h>
#include <X11/Xaw/Form.h>
#include <X11/Xaw/Label.h>
#include <X11/Xaw/List.h>
#include <X11/Xaw/Paned.h>
#else /*MOTIF*/
a35 1
#endif
d65 15
a79 13
    "*error*background: tomato",
    "*error*foreground: black",
#ifdef USE_AW
    "*error*font:       lucidasans-bold-24",
    "*error*label:      The `edsLEDs' Process Appears To Have Died.",
    "*acknowledge*font: lucidasans-bold-18",
    "*acknowledge*label: ---> Acknowledge <---",
#else /*MOTIF*/
    "*error*fontList:          lucidasans-bold-24",
    "*error*labelString:       The `edsLEDs' Process Appears To Have Died.",
    "*acknowledge*fontList:    lucidasans-bold-18",
    "*acknowledge*labelString: ---> Acknowledge <---",
#endif
a102 1
#ifndef USE_AW
a103 1
#endif
a107 3
#ifdef USE_AW
    XtVaSetValues(label, XtNlabel, buf, NULL);
#else /*MOTIF*/
a110 1
#endif
a199 20
#ifdef USE_AW
    pane = XtVaCreateManagedWidget("paned", panedWidgetClass, shell,
                                   XtNorientation, XtorientVertical,
                                   NULL);

    errorLabel = XtVaCreateManagedWidget("errorLabel", labelWidgetClass, pane,
                                         XtNjustify,  XtJustifyCenter,
                                         XtNshowGrip, FALSE,
                                         NULL);

    acknowledge = XtVaCreateManagedWidget("quit", commandWidgetClass, pane,
                                          XtNlabel,        "---> Acknowledge <---",
                                          XtNfromVert,           errorLabel,
                                          XtNshowGrip,           FALSE,
                                          XtNhighlightThickness, 4,
                                          XtNinternalHeight,     5,
                                          NULL);

    XtAddCallback(acknowledge, XtNcallback, acknowledge_callback, 0);
#else /*MOTIF*/
a223 1
                                          XmNshowAsDefault,   3,
a227 1
#endif
a253 9
#ifdef USE_AW
    label = XtVaCreateManagedWidget("clientLabel", labelWidgetClass,
                                    parent,
                                    XtNwidth,       400,
                                    XtNlabel,       "Your Message Goes Here.",
                                    XtNjustify,     XtJustifyCenter,
                                    XtNborderWidth, 2,
                                    NULL);
#else /*MOTIF*/
a261 1
#endif
@


1.5
log
@fixed small bug
@
text
@d22 1
a22 1
static char rcsid[] = "$Id: rtclient-win.c,v 1.4 1994/01/11 20:57:58 lijewski Exp lijewski $";
d41 1
d48 5
a67 7
/*
** Some handy constants.
*/
const int KBIT = 1024;
const int MBIT = 1048576;


a68 5


/*
** The application context and widgets.
*/
d70 1
a70 2
static Widget appshell, error_popup;
static Widget label;
d74 5
a78 1
** Some colorful fallback resources.
d80 1
a80 1
static String fallbacks[] = 
d82 13
a94 5
    "*foreground: white",
    "*background: black",
    "*errorLabel*background: tomato",
    "*errorLabel*foreground: black",
    "*errorLabel*font: lucidasans-bold-24",
a111 6
static void quit_callback (Widget w, XtPointer client_data, XtPointer call_data)
{
    exit(0);
}


d118 3
d125 1
d127 5
d199 7
a205 1
static XtActionsRec actions[] = { { "quit", quit } };
d217 1
a217 3
    Widget pane;
    Widget errorLabel;
    Widget quit;
a218 2
    Dimension swidth, sheight;
    Screen* theScreen;
d221 1
a221 7
    /*
    ** Define the label for the error popup.
    */
    const char* fmt = "\nThe `%s' Process Appears To Have Died\n";
    char* msg = malloc(strlen(fmt) + strlen(ProgName) + 1);
    (void) sprintf(msg, fmt, ProgName);

a226 1
                                         XtNlabel,    msg,
d231 20
a250 6
    quit = XtVaCreateManagedWidget("quit", commandWidgetClass, pane,
                                   XtNlabel,        "---> Acknowledge <---",
                                   XtNfromVert,           errorLabel,
                                   XtNshowGrip,           FALSE,
                                   XtNhighlightThickness, 4,
                                   XtNinternalHeight,     5,
d253 18
a270 1
    XtAddCallback(quit, XtNcallback, quit_callback, 0);
d281 2
a282 5
    theScreen = XtScreen(appshell);
    swidth    = WidthOfScreen(theScreen);
    sheight   = HeightOfScreen(theScreen);
    x = (swidth - width)/2;
    y = (sheight - height)/2;
d288 5
a292 3
** Make the windows necessary for this client.  For our simple example, all
** I'll do is make a label widget to which I'll output a single string
** containing the data from the passed RTData structure.
d297 1
d305 10
a328 1
    **
d343 14
a356 4
    ProgName = argv[0];

    appshell = XtAppInitialize(&appcontext, ProgName, NULL, 0, &argc, argv,
                               fallbacks, NULL, 0);
d358 9
d370 2
a371 1
    ** Ignore keyboard generated signals.
d380 2
a381 2
                                       xte_width, xte_height);

d408 5
a412 2
                                       PopupErrorMessage, NULL);

d422 5
a426 2
    XtAppAddInput(appcontext, STDIN_FILENO, (XtPointer)XtInputReadMask,
                  ReadData, (XtPointer)0);
d437 1
a437 3
    /*
    ** The main loop.
    */
@


1.4
log
@removed some C++ comments
@
text
@d22 1
a22 1
static char rcsid[] = "$Id: rtclient-win.c,v 1.3 1994/01/11 14:34:59 lijewski Exp lijewski $";
d160 3
a162 3
            XBell(XtDisplay(appcontext), 100);
            XBell(XtDisplay(appcontext), 100);
            XBell(XtDisplay(appcontext), 100);
@


1.3
log
@initial working version
@
text
@d22 1
a22 1
static char rcsid[] = "$Id: rtclient-win.c,v 1.2 1994/01/10 22:47:13 lijewski Exp lijewski $";
d62 3
a64 3
//
// Some handy constants.
//
d96 1
a96 1
    char buffer[256]; // Should be large enough.
@


1.2
log
@initial working version -- not tested with realtime client
@
text
@d3 3
a5 1
** X Toolkit and the Athena WIdgets.
d7 12
a18 1
** This is ANSI C code.
d22 1
a22 1
static char rcsid[] = "$Id$";
d201 1
a201 1
static void PopupErrorMessage (void)
d210 7
a216 1
    char* Msg = "\nThe Realtime Ingest Process Appears To Have Died\n ";
d218 1
a218 1
    pane = XtVaCreateManagedWidget("paned", panedWidgetClass, error_popup,
d223 1
a223 1
                                         XtNlabel,    Msg,
d252 1
a252 1
    XtVaSetValues(error_popup, XtNx, x, XtNy, y, NULL);
d284 1
a284 1
    ** Acceptable arguments:
d287 1
a287 1
    while (*++argv && **argv == '-') 
d291 1
a292 6

    usage();

    /*
    ** Check arguments for consistency.
    */
@


1.1
log
@Initial revision
@
text
@d9 1
a9 1
static char rcsid[] = "Id$";
d147 3
a149 3
            XBell(XtDisplay(w), 100); XBell(XtDisplay(w), 100);
            XBell(XtDisplay(w), 100); XBell(XtDisplay(w), 100);
            XBell(XtDisplay(w), 100); XBell(XtDisplay(w), 100);
d210 1
a210 1
                                   XtNlabel,              "---> Acknowledge <---",
d247 2
a248 2
/*                                    XtNwidth,       width,*/
                                    XtNlabel,       "                  ",
a267 1
/*
d270 2
a271 26
        if (strcmp(*argv, "-update") == 0)
        {
            if (*++argv)
            {
                if ((UpdateRate = atoi(*argv)) <= 0)
                    error("Update # must be positive.");
            }
            else
                error("No update # supplied.");
        }
        else if (strcmp(*argv, "-points") == 0)
        {
            if (*++argv)
            {
                if ((NPoints = atoi(*argv)) <= 0)
                    error("Points # must be positive.");
            }
            else
                error("No points # supplied.");
        }
        else
        {
            (void) fprintf(stderr, "%s: unknown option `%s', exiting ...\n",
                           ProgName, *argv);
            usage();
        }
d273 2
a274 1
*/
d304 1
d332 1
@

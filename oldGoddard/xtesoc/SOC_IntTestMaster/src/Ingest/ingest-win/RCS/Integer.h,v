head	4.3;
access
	soccm;
symbols;
locks; strict;
comment	@ * @;


4.3
date	95.10.23.19.06.02;	author rbarnett;	state Exp;
branches;
next	;


desc
@Infinite precision Integer class
@


4.3
log
@checked in with -k by soccm at 1997/03/27 21:02:26
@
text
@// ===========================================================================
// File Name   : Integer.h
// Subsystem   : Data Ingest
// Programmer  : Mike Lijewski
// Description : Declaration of Data Set class
//
// RCS: $Id: Integer.h,v 4.3 1995/10/23 19:06:02 rbarnett Exp $
//
// .NAME    Integer - A Data Ingest class for infinite precision integers
// .LIBRARY none
// .HEADER  Data Ingest
// .INCLUDE Integer.h
// .FILE    Integer.C
// .VERSION $Revision: 4.3 $
// ===========================================================================

#ifndef INTEGER_H
#define INTEGER_H

#include <iostream.h>

const BSINT_MULB_CUT = 20;

enum Sign { NEGATIVE, POSITIVE };

class Integer {
public:

  Integer (void);                   // Integer foo;
  Integer (const int initValue);    // Integer foo = 12;
  Integer (const Integer& init);    // Integer bar = foo;
  Integer (const char* str);        // Integer foo = "12";

  Integer::Integer (const unsigned long initValue);

  Integer& operator= (const Integer& U);  // Assignment: foo = bar;

  ~Integer ();

  operator int (void);

  int IntegerBad (void);                    // Test for bad DigitRep

  void needModify    (void);
  void needOverwrite (void);

  int IntegerBad (void) const;

  unsigned long& operator[] (int index);  // 0 based indexing
  unsigned long& operator() (int index);  // 1 based indexing

  void setSize   (int newUsed);
  int  getSize   (void) const;
  int  getSerial (void) const;

  void insertDigit (unsigned long newDigit = 0);
  void appendDigit (unsigned long newDigit = 0, int repeat = 1);
  void removeLeadingZeros (void);

  Sign getSign    (void) const;
  void setSign    (Sign newSign);
  void changeSign (void);

  Integer& operator+= (const Integer& U);
  Integer& operator-= (const Integer& U);
  Integer& operator*= (const Integer& U);
  Integer& operator/= (const Integer& U);
  Integer& operator%= (const Integer& U);

  Integer& operator+= (const int U);
  Integer& operator-= (const int U);
  Integer& operator*= (const int U);
  Integer& operator/= (const int U);
  Integer& operator%= (const int U);
    
  Integer operator++ (void);
  Integer operator++ (int);
  Integer operator-- (void);
  Integer operator-- (int);

  friend void test (void);
  friend void IntegerSetSizes (int numSize, int cacheSize, int base);
  friend int  IntegerGetNumberSize (int base);
  friend int  IntegerGetNumberCached (void);
  friend int operator== (const Integer& U, const Integer& V);
  friend int operator<=(const Integer& U, const Integer& V);
  friend int operator< (const Integer& U, const Integer& V);
  friend ostream& operator<< (ostream& out, const Integer& number);

protected:

  struct DigitRep {
		int                refs;      // # of references to this string.
		int                used;      // # of digits used.
		int                serial;    // The serial number.
		unsigned long    digit[1];    // The actual digits.
	};

	static DigitRep**  FreeCache;
	static int         FreeCacheSize;
	static int         NumCached;
	static int         NumberSize;
	static int         NextSerialNumber;

	static Integer* head;  // Linked list of all existing Integers.
	static Integer* tail;

	static int QRNSerial, QRDSerial;

	DigitRep* dp;         // Pointer to digits.
	Sign      sign;       // The sign of this integer.
	Integer*  next;       // The links for placing on list.
	Integer*  prev;

	void newDigitRep     (void);
	void freeDigitRep    (void);
	void insertOnList    (void);
	void deleteFromList  (void);
	void setSerialNumber (void);
};

extern void    SetRandom (Integer& U, int numBits);
extern char*   ReversedDecimalString (const Integer& U);
extern char*   DecimalString (const Integer& U);
extern Integer pow (const Integer & U, const Integer & V);
extern Integer powmod (const Integer& U, const Integer& V, const Integer& M);
extern Integer mod_mp (const Integer& U, int p);

inline void Integer::setSerialNumber (void)
{
    if (++NextSerialNumber == 0)
    {
        QRNSerial = 0;
        QRDSerial = 0;
        ++NextSerialNumber;
    }
    dp->serial = NextSerialNumber;
}

inline void Integer::newDigitRep (void)
{
    if (NumCached == 0)
        dp = (DigitRep*) new unsigned long [sizeof(DigitRep)/sizeof(long)+NumberSize];
    else
        dp = FreeCache[--NumCached];
    dp->refs = 1;
    dp->used = 1;
    dp->digit[0] = 0;
    setSerialNumber();
}

inline void Integer::freeDigitRep (void)
{
    if (NumCached < FreeCacheSize)
        FreeCache[NumCached++] = dp;
    else
        delete[] (unsigned long *) dp;
}

inline void Integer::insertOnList (void)
{
    next = head;
    prev = 0;
    if (head == 0)
        tail = this;
    else
        head->prev = this;
    head = this;
}

inline void Integer::deleteFromList (void)
{
    if (next)
        next->prev = prev;
    if (prev)
        prev->next = next;
    if (head == this)
        head = next;
    if (tail == this)
        tail = prev;
}

inline Integer & Integer::operator= (const Integer& U)
{
    if (&U != this)
    {
        U.dp->refs++;
        if (--dp->refs == 0)
            freeDigitRep();
        dp = U.dp;
        sign = U.sign;
    }
    return *this;
}

inline unsigned long& Integer::operator[] (int index)
{
    return dp->digit[index];
}

inline Integer operator+ (const Integer& U, const int V)
{
    Integer result = U;
    result += V;
    return result;
}

inline Integer operator+ (const int U, const Integer& V)
{
    Integer result = V;
    result += U;
    return result;
}

inline Integer::~Integer (void)
{
    if (dp && --dp->refs == 0)
        freeDigitRep();
    deleteFromList();
}

inline Integer::operator int (void)
{
    return sign == POSITIVE ? dp->digit[dp->used-1] : -dp->digit[dp->used-1];
}

inline int Integer::getSerial (void) const
{
    return dp->serial;
}

inline int Integer::IntegerBad (void)
{
    return dp == 0;
}

inline int Integer::getSize (void) const
{
    return dp->used;
}

inline unsigned long& Integer::operator() (int index)
{
    return dp->digit[index-1];
}

inline void Integer::setSize(int newUsed)
{
    dp->used = newUsed;
    setSerialNumber();
}

inline Sign Integer::getSign (void) const
{
    return sign;
}

inline void Integer::setSign (Sign newSign)
{
    sign = newSign;
}

inline void Integer::changeSign (void)
{
    if (dp->used != 1 || dp->digit[0] != 0)
        sign = POSITIVE ? NEGATIVE : POSITIVE;
    else
        sign = POSITIVE;    // 0 is always positive.
}
    
inline void Integer::needModify (void)
{
    if (dp->refs > 1)
    {
        DigitRep* op = dp;
        newDigitRep();
        if (dp)
        {
            op->refs--;
            for (int i = 0; i < op->used; i++)
                dp->digit[i] = op->digit[i];
            dp->used = op->used;
        }
    }
    else
        setSerialNumber();
}

inline void Integer::needOverwrite (void)
{
    if ( dp->refs > 1 )
    {
        dp->refs--;
        newDigitRep();
        if ( dp )
            dp->refs = 1;
    }
    else
        setSerialNumber();
}

inline void Integer::insertDigit (unsigned long newDigit)
{
    for (int i = dp->used; i >= 1; i--)
        dp->digit[i] = dp->digit[i-1];
    dp->digit[0] = newDigit;
    dp->used++;
}

inline void Integer::appendDigit (unsigned long newDigit, int repeat)
{
    while (repeat-- > 0)
        dp->digit[dp->used++] = newDigit;
}

inline void Integer::removeLeadingZeros (void)
{
    for (int fnz = 0; fnz < dp->used; fnz++)
        if ( dp->digit[fnz] != 0 )
            break;
    if (fnz >= dp->used)
    {
        dp->used = 1;
    }
    else
    {
        if ( fnz )
            for (int i = fnz; i < dp->used; i++)
                dp->digit[i-fnz] = dp->digit[i];
        dp->used -= fnz;
    }
}

inline Integer Integer::operator++ (void)
{
    *this = *this + 1;
    return *this;
}

inline Integer Integer::operator++ (int)
{
    Integer result = *this;
    *this = *this + 1;
    return result;
}

inline Integer& Integer::operator-= (const Integer& U)
{
    Integer tmp = U;
    tmp.sign = POSITIVE ? NEGATIVE : POSITIVE;
    *this += tmp;
    return *this;
}

inline Integer& Integer::operator-= (const int U)
{
    *this += -U;
    return *this;
}

inline Integer operator- (const Integer& U, const Integer& V)
{
    Integer  result = U;
    result -= V;
    return result;
}

inline Integer operator- (const Integer& U, const int V)
{
    Integer result = U;
    result -= V;
    return result;
}

inline Integer operator- (const int U, const Integer& V)
{
    Integer result = V;
    result -= U;
    result.changeSign();
    return result;
}


inline Integer Integer::operator-- (void)
{
    *this = *this - 1;
    return *this;
}

inline Integer Integer::operator-- (int)
{
    Integer result = *this;
    *this = *this - 1;
    return result;
}
    
inline Integer operator- (const Integer& U)
{
    Integer result = U;
    result.changeSign();
    return result;
}

inline Integer operator+ (const Integer& U, const Integer& V)
{
    Integer result = U;
    result += V;
    return result;
}

inline Integer operator* (const Integer& U, const Integer& V)
{
    Integer result = U;
    result *= V;
    return result;
}


inline Integer operator* (const int U, const Integer& V)
{
    return operator*( V, U );
}

inline Integer operator/ (const Integer& U, const Integer& V)
{
    Integer result = U;
    result /= V;
    return result;
}

inline Integer operator% (const Integer& U, const Integer& V)
{
    Integer result = U;
    result %= V;
    return result;
}

inline Integer operator/ (const Integer& U, const int V)
{
    Integer result = U;
    result /= V;
    return result;
}

inline Integer operator% (const Integer& U, const int V)
{
    Integer result = U;
    result %= V;
    return result;
}

inline int operator/ (const int U, const Integer& Va)
{
    int q = 0;
    Integer V = Va;
    unsigned long r = 0;
    unsigned long absU = U < 0 ? -U : U;
    if (V.getSize() > 1)
    {
        q = 0;
        r = absU;
    }
    else
    {
        q = (int) (absU / V[0]);
        r = absU % V[0];
    }
    if (U < 0 && r != 0)
        q++;
    if ((U < 0 && V.getSign() == POSITIVE) || (U > 0 && V.getSign() == NEGATIVE))
        q = -q;
    return q;
}

inline Integer operator% (const int U, const Integer& V)
{
    Integer result = U;
    return  result % V;
}


inline int operator== (const Integer& Ua, const int V)
{
    int rc = 1;
    Integer U = Ua;
    unsigned long absV = V < 0 ? -V : V;
    if (U.getSize() != 1)
        rc = 0;
    if (U[0] != absV)
        rc = 0;
    if ((U.getSign() == POSITIVE && V < 0) || (U.getSign() == NEGATIVE && V >= 0) )
        rc = 0;
    return rc;
}

inline int operator== (const int U, const Integer& V)
{
    return V == U;
}

inline int operator<= (const Integer& Ua, const int V)
{
    int rc = 0;
    Integer U = Ua;
    unsigned long absV = V < 0 ? -V : V;
    if (U.getSign() == POSITIVE && V < 0)
        rc = 0;
    if (U.getSign() == NEGATIVE && V >= 0)
        rc = 1;
    // signs are the same
    if (U.getSize() == 1)
        rc = ((U[0] <= absV && V >= 0) || (U[0] >= absV && V < 0)) ? 1 : 0;
    else
        rc = V < 0;
    return rc;
}

inline int operator< (const Integer& Ua, const int V)
{
    int rc = 0;
    Integer U = Ua;
    unsigned long absV = V < 0 ? -V : V;
    if (U.getSign() == POSITIVE && V < 0)
        rc = 0;
    if (U.getSign() == NEGATIVE && V >= 0)
        rc = 1;
    // signs are the same
    if (U.getSize() == 1)
        rc = ((U[0] < absV && V >= 0) || (U[0] > absV && V < 0)) ? 1 : 0;
    else
        rc = V < 0;
    return rc;
}

inline int operator>= (const Integer& U, const Integer& V)
{
    return ! (U < V);
}

inline int operator>= (const Integer& U, const int V)
{
    return ! (U < V);
}

inline int operator<= (const int U, const Integer& V)
{
    return V >= U;
}

inline int operator> (const Integer& U, const Integer& V)
{
    return ! (U <= V);
}

inline int operator> (const Integer& U, const int V)
{
    return ! (U <= V);
}

inline int operator< (const int U, const Integer& V)
{
    return V > U;
}

inline int operator!=(const Integer& U, const Integer& V)
{
    return ! (U == V);
}

inline int operator!= (const Integer& U, const int V)
{
    return ! (U == V);
}

inline int operator!= (const int U, const Integer& V)
{
    return ! (U == V);
}

inline int operator> (const int U, const Integer& V)
{
    return ! (U <= V);
}

inline int operator>= (const int U, const Integer& V)
{
    return ! (U < V);
}

#endif /*INTEGER_H*/
@

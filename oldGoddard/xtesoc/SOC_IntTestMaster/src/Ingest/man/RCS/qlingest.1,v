head	4.1;
access
	soccm;
symbols
	Build4_3_2:4.1
	Build4_2:1.1
	Build4:1.1;
locks; strict;
comment	@# @;


4.1
date	95.06.20.20.17.54;	author rbarnett;	state Exp;
branches;
next	1.1;

1.1
date	95.01.09.18.48.35;	author rbarnett;	state Exp;
branches;
next	;


desc
@new man page for quicklook ingest
@


4.1
log
@updated to match reality
@
text
@.\" ==========================================================================
.\" File Name   : qlingest.1
.\" Subsystem   : Data Ingest
.\" Programmer  : Randall D. Barnette, Hughes STX
.\" Description :
.\"
.\"   Man page for the qlingest process.
.\"
.\" RCS: $Id: qlingest.1,v 2.7 1995/05/09 15:57:53 soccm Exp $
.\"
.\" ==========================================================================
.TH qlingest 1 "21 April 1995" "XTE-SOC V4.3" "Data Ingest"
.SH NAME
qlingest \- Archive QuickLook Data Sets from DDF to disk.
.\" ==========================================================================
.SH SYNOPSIS
.\" ==========================================================================
.B qlingest
.RB [ -verbose ]
.RB [ -warn ]
.RB [ -do-not-archive ]
.RB [ -do-not-check-order ]
.RB [ -do-not-check-mdul ]
.I file1
.RI [ file2
.IR "..." ]
.\" ==========================================================================
.SH DESCRIPTION
.\" ==========================================================================
.I qlingest
is a non-interactive process which archives to disk one or more QuickLook
Data Sets specified on the command line. The contents of the input Data
Sets are as specified in the Interface Control Document 560-1ICD/0993
between the XTE Science Operations Center (XTESOC) and the Sensor Data
Processing Facility (SDPF).
Production Data Sets contain "annotation" information for delivery and
quality assurance.
.I qlingest
strips out the telemetry packets, verifies their integrity, separates
them by apid, and verifies their proper ordering before writing them
to disk files.
.PP
The telemetry packets contained in the Data Sets are archived to disk
as ordered sequential files.
Each of these files only contains packets from a single ApID.
Their names are of the form:
.IR nnnnnnnnnn-nnnnnnnnnn ,
representing the Mission Elapsed Time (MET) in the CCSDS header of the
first and last packet in the file.
The full pathname of these packet files are of the form:
.IP
.BI $SOCOPS/ingest/quicklook/day DDDD /apid AAA / nnnnnnnnnn-nnnnnnnnnn;
.IP
Example:
.br
.B /socops/ingest/quicklook/day0021/apid023/0001234023-0001245678.
.PP
The day embedded in the path refers to the MET of the first
packet in the file.
.B $SOCOPS
symbolically refers to the full
pathname of the root of the SOC operations directory.
It is obtained from the environment variable
.B SOCOPS.
Note that there may be packets in such a packet file which belong to
days later than that specified in the path. Packet
files are not necessarily split on day boundaries.
.PP
On startup QuickLook Ingest changes its working directory to the root
of the directory tree on the machine on which it is started.
All QuickLook DataSet archival must be single threaded
through a single QuickLook Ingest process; there
must be no more than one QuickLook Ingest process running at any
given time. While we must rely on operating procedures to ensure that
the QuickLook Ingest process isn't started on more than one machine, we
programmatically ensure that no more than one QuickLook Ingest process
can be running on any machine at once. We do this via a locking
mechanism based on the file
.B $SOCOPS/tmp/.quicklook.lck.
No more than one process can have a lock on this file
at any given time. Indeed, if
.B $SOCOPS/tmp
is NFS
mounted on all XTESOC machines and the rpc.lockd daemon is running on
all machines, this mechanism will ensure that no
more than one QuickLook Ingest process can be running at any
given time.
Because QuickLook Ingest expects to obtain a write lock on
$SOCOPS/tmp/.quicklook.lck, the user-id of the executing QuickLook
Ingest process must have both read and write permission to this file.
.PP
The lockfile contains a string of the form:
.I hostname.pid,
where
.I hostname
is the hostname of the machine on which
.I qlingest
was started, and
.I pid
is its process ID.
This makes it straightforward to track down the process holding
the lock.
.PP
If QuickLook Ingest must be killed or halted for
any reason, it can be safely killed by sending it a SIGTERM signal;
i.e. type
.IP
kill -TERM pid
.PP
where
.I pid
is the process ID of the
to-be-killed QuickLook Ingest process. For debugging purposes, a core dump 
of QuickLook Ingest can be obtained by sending it a SIGQUIT;
i.e. type
.IP
kill -QUIT pid
.PP
.I qlingest
terminates when it has processed all input files.
.\" ==========================================================================
.SH OPTIONS
.\" ==========================================================================
.TP
.B -verbose
Turn on additional debugging output.
.TP
.B -do-not-archive
This option instructs Production Ingest to not archive telemetry to disk.
It exists for debugging purposes.
.TP
.B -warn
This option instructs Production Ingest to issue a warning for certain
non-fatal conditions.
It exists for debugging purposes.
.TP
.B -do-not-check-order
This option instructs Production Ingest to not analyze packet ordering.
It exists for debugging purposes.
.TP
.B -do-not-check-mdul
This option instructs Production Ingest to not check and report Missing
Data Units.
It exists for debugging purposes.
.\" ==========================================================================
.SH "ENVIRONMENT VARIABLES"
.\" ==========================================================================
.IP SOCHOME
This is the root directory of the XTESOC Integration and Test
directory.
There should be subdirectories
.I etc
and
.I tmp
below this directory.
.IP SOCOPS
This is the root of the XTESOC operations directory tree.
.\" ==========================================================================
.SH FILES
.\" ==========================================================================
.TP
.BI $SOCOPS/quicklook/day DDDD / filename
This is the typical form of Quicklook Production Data Set files. The exact
form and meaning of
.I filename
is specified in the ICD with SDPF.
.TP
.BI $SOCOPS/ingest/quicklook/day DDDD /apid AAA / nnnnnnnnn-nnnnnnnnn
This form is used for all output files of
.I qlingest.
.TP
.BI $SOCOPS/ingest/quicklook/day DDDD /Fill.Pkts
Fill packets are appended to files of this form.
.PP
In each case above,
.I DDDD
is the MET day (i.e. the number of 86,400 second periods since
January 1, 1993);
.I AAA
is the telemetry application ID in the range
0 through 233 inclusive; and
.I nnnnnnnnnn
is a 10-digit decimal
MET value for the start or stop packet time.
.TP
.B $SOCOPS/tmp/.quicklook.lck
The lockfile used by
.I qlingest
.\" ==========================================================================
.SH BUGS
.\" ==========================================================================
There ought to be some sort of starttime/stoptime mechanism.
.\" ==========================================================================
.SH SEE ALSO
.\" ==========================================================================
.BR rtingest (1),
.BR pringest (1)
.\" ==========================================================================
.SH AUTHOR
.\" ==========================================================================
Mike Lijewski <lijewski@@xema.stx.com> Hughes STX
.br
Randall Barnette <rbarnett@@xema.stx.com> Hughes STX
@


1.1
log
@Initial revision
@
text
@d1 5
d7 1
a7 1
.\"  $Id: qlingest.man,v 2.6 1994/03/22 19:59:59 lijewski Exp $
d9 4
a12 1
.TH INGEST 1
d14 2
a15 1
qlingest \- archive QuickLook Data Sets from DDF to disk.
d17 11
a27 2
\fBqlingest\fP [-do-not-archive] [-testdata] file1 file2 ...
.br
d29 15
a43 4
This is an interactive process which archives to disk one or more QuickLook
DataSets as specified on the command line.  The Data Sets are as
specified in the ICD between XTESOC and the SDPF.
The telemetry packets contained in the DataSets are archived to disk
d47 3
a49 8
.I nnnnnnnnnn-nnnnnnnnnn;
for example:
.I 0087667623-0087695341.
The first
numeric sequence is the alphanumeric representation of the Mission
Elapsed Time (MET) in the CCSDS header of the sequentially first packet in
the file.  Likewise, the second numeric sequence corresponds to the
MET of the last packet in the file.
d51 7
a57 3
.I $SOCOPS/ingest/quicklook/dayNNN/apidNNN/nnnnnnnnnn-nnnnnnnnnn;
for example:
.I /socops/ingest/quicklook/21/023/0001234023-0001245678.
d59 6
a64 3
packet in the file.  $SOCOPS symbolically refers to the full
pathname of the root of the SOC operations hierarchy.
It is obtained from the environment variable SOCOPS.
d66 2
a67 3
days later than that specified in the path.  That is to say, packet
files are not split on day boundaries.
We call this process QuickLook Ingest.
d72 1
a72 1
through a single QuickLook Ingest process.  Owing to this, there
d74 1
a74 1
given time.  While we must rely on operating procedures to ensure that
d77 1
a77 1
can be running on any machine at once.  We do this via a locking
d79 1
a79 3
.I $SOCOPS/tmp/.quicklook.lck.
Here $SOCOPS refers symbolically to the environment variable SOCOPS
obtained from the environment of the executing QuickLook Ingest.
d81 3
a83 1
at any given time.  Indeed, if $SOCOPS/tmp is NFS
d91 1
d107 3
a109 1
.I "kill -TERM pid,"
d113 1
a113 1
to-be-killed QuickLook Ingest process.  For debugging purposes, a core dump 
d116 12
a127 3
.I "kill -QUIT pid."
.SH SWITCHES
.TP 10
d129 2
a130 2
This option instructs QuickLook Ingest to not archive telemetry to disk.
It is here for debugging purposes.
d132 14
a145 7
.B -testdata
This option specifies whether we're expecting Test data or Realtime
data.  One of the fields in an SIT, STT and a QAC is the type of the
data -- Realtime or Test.  We only accept Test data when this option
is turned on.  Otherwise, we only accept Realtime data.  When we're
expecting one type of data and we get another, the stream over which
we read the data is closed.
d147 1
d150 1
a150 1
directory.  
d157 2
a158 1
The root of the XTESOC operations directory hierarchy.
d160 1
a160 4
.PD 0
.TP 30
.B $SOCOPS/tmp/.quicklook.lck
a lockfile
d162 11
a172 1
.B $SOCOPS/ingest/quicklook/dayNNN/Fill.Pkts
d174 16
d191 3
a193 1
The processing of QuickLook Data Sets isn't fully implemented yet.
d195 9
a203 4
.BR pringest (1),
.BR rtingest (1)
.SH AUTHORS
Written by Mike Lijewski.
@

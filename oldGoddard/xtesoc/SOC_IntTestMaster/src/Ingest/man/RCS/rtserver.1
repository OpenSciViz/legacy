.\" ==========================================================================
.\" File Name   : rtserver.1
.\" Subsystem   : Data Ingest
.\" Programmer  : Randall D. Barnette, Hughes STX
.\" Description : Man page for the rtserver process.
.\"
.\" RCS: $Id: rtserver.1,v 5.1 1995/12/27 20:24:06 rbarnett Exp $
.\"
.\" ==========================================================================
.TH rtserver 1 "27 December 1995" "XTE-SOC V5.0" "Data Ingest"
.SH NAME
rtserver \- Forwards telemetry packets to realtime clients.
.SH SYNOPSIS
.B rtserver
.RB [ -size
.IR n ]
.RB [ -server-host
.IR host ]
.RB [ -server-port
.IR port ]
.RB [ -client-port
.IR port ]
.RB [ -read-from-stdin ]
.RB [ -print-auth-table ]
.RB [ -print-client-table ]
.RB [ -alternate ]
.RB [ -do-not-use-lockfile ]
.\" ==========================================================================
.SH DESCRIPTION
.\" ==========================================================================
The Realtime Server
.I rtserver
is a process which receives packets from Realtime Ingest
.RI ( rtingest )
and passes them on to interested realtime client programs.
It has two well-known Transmission Control Protocol (TCP)
ports: (1) over which it connects to Realtime Ingest and reads XTE
CCSDS AOS telemetry packets and (2) over which it accepts
connections from realtime clients and passes on packets in which
the process is interested.
.PP
On startup the Realtime Server changes its working directory to
the root of the directory tree on the machine on which it is started.
There must be no more than one Realtime Server running at any
given time during normal operations.
While we must rely on operating procedures to ensure that
the Realtime Server process isn't started on more than one machine, we
programmatically ensure that no more than one Realtime Server
can be running on any machine at once by using a locking
mechanism based on the file
.B $SOCOPS/tmp/.rtserver.lck.
Here
.B $SOCOPS
refers symbolically to the environment variable
.B SOCOPS
obtained from the environment of the executing Realtime Server process.
No more than one process can have a lock on this file 
at any given time. Indeed, if
.B $SOCOPS/tmp
is NFS mounted on all XTE SOC machines and the
.I rpc.lockd
daemon is running on all machines, this mechanism will ensure that no
more than one Realtime Server process can be running at any given time.
.PP
The lockfile contains a string of the form:
.I hostname.pid,
where
.I hostname
is the hostname of the machine on which
.I rtserver
was started, and
.I pid
is its process ID.
This makes it straightforward to track down the process holding the lock.
.PP
The Realtime Server identifies packets along by application ID (ApID).
Realtime clients inform the Realtime Server of ApIDs in which they are
interested. Each time the Realtime Server receives a
telemetry packet, it passes it to each client which has indicated that
it wants packets of that ApID. A telemetry packet can be sent to more
than one client and clients can get packets from more than one ApID.
.PP
There also exists a mechanism by which realtime clients can specify
certain types of Spacecraft data in which they're interested.
These Spacecraft data types are translated to the respective ApID, so
that the Realtime Server deals strictly with ApIDs. The types of
Spacecraft data supported and how to use them is detailed in the Interface
Control Documents (ICD) with each of the Instrument Teams (IT).
.PP
Each realtime client informs the Realtime Server of which ApIDs and
Spacecraft data types they are interested by providing the XTE SOC a
one-line table entry that the Realtime Server reads on startup. 
The XTE SOC puts each of these entries into a table entitled
.B RT.CLIENTS
that Realtime Server expects to read from
.B $SOCHOME/etc/RT.CLIENTS.
Entries in the table take the form:
.IP
ClientName   ListOfApIDs and/or SCIDs 
.PP
.I ClientName
is a mnemonic name for the realtime client. When a realtime client
connects to the Realtime Server it passes its name to the Realtime
Server which verifies that it is one of the names of valid clients in the
.B RT.CLIENTS
table.
There can be more than one client of the same name connected to the
realtime server at one time.
.I ListOfApIDs
and/or
.I SCIDs
is a list of all the ApIDs and/or Spacecraft data types the client
wants to receive.
The ApIDs in this list, as well as each of the fields in the
line, must be separated from one another by spaces, tabs or commas.
The following is a sample
.B RT.CLIENTS:
.IP
.nf
EDS-Science      49 50 51 52 53 54 SC_ATTITUDE
EDS-HK           48, 56, 64, 72
HEXTE-SC         80 83 86 89 SC_ATTITUDE SC_POSITION
HEXTE-HK         83 89
PCA-HK           90, 91, 92, 93, 94 SC_PCU_TEMPA, SC_PCU_TEMP_B
.fi
.PP
Lines in
.B RT.CLIENTS
which begin with a # are considered comments.
.PP
XTE SOC personnel can modify
.B RT.CLIENTS
while the Realtime Server is running. Once the modifications to
.B RT.CLIENTS
are made, the Realtime Server
can be forced to reread the table by sending it a SIGUSR1 signal; i.e.
by typing
.IP
kill -USR1 pid
.PP
where
.I pid
is the process ID of the Realtime Server process.
When the Realtime Server rereads
.B RT.CLIENTS
it replaces the old version
of the table with the new one. Realtime clients that are already
connected to the Realtime Server are not affected.
.PP
If the Realtime Server must be killed or halted for
any reason, it can be safely killed by sending it a SIGTERM signal;
i.e. type
.IP
kill -TERM pid
.PP
where
.I pid
is the process ID of the
to-be-killed Realtime Server process.
.PP
For security reasons every machine that
receives XTE Telemetry, which includes realtime clients, must be
authorized by the XTE SOC to do so. 
The on-disk table of machines authorized to connect to Realtime Ingest
and the Realtime Server or to receive XTE telemetry from the Realtime
Server is the file
.B $SOCHOME/etc/AUTH.HOSTS
It must include all possible Pacor II RTOS machines, as well as any machine
that Realtime Ingest must communicate with such as the one on which the
Realtime Server runs. This further includes any DCF machine which
would need to connect to Realtime Ingest or any
telemetry generators that are run for debugging or testing purposes.
The Realtime Server reads this table on startup and maintains an
in-memory copy that it uses to do authorization checks against each
process that tries to connect to it over
.I -realtime-port.
When the Realtime Server receives a SIGUSR1 it updates its in-memory
copies of both
.B AUTH.HOSTS
and
.B RT.CLIENTS.
.\" ==========================================================================
.SH OPTIONS
.\" ==========================================================================
.TP
.BI -size " n"
This option sets the size of the shared memory used to buffer packets during
high data rate periods. The default is 64 megabytes.
.TP
.BI -server-host " host"
This option specifies the name of the machine on which Realtime Ingest
is running.
.TP
.BI -server-port " port"
This option specifies the well-known TCP port in the Realtime Ingest
process to which the Realtime Server must connect in order to receive
telemetry packets from Realtime Ingest.
It specifies the private communication channel
between Realtime Ingest and the Realtime Server.
If this option is not set, there must be a well-defined TCP service in
.B /etc/services
named 
.I ingest-private
from which the Realtime Server can determine a valid port number.
Under normal operations it is expected that a valid
.B /etc/services
entry will be supplied on all the XTE SOC machines so that this option
doesn't need to be specified.
.TP
.BI "-client-port " port
This option specifies the well-known TCP port in the Realtime Server
to which every realtime client must connect in order to receive their
telemetry packets.
If this option is not set, there must be well-defined TCP service in
.B /etc/services
named 
.I realtime-server
from which the Realtime Server can determine a valid port number.
Under normal operations it is expected that a valid
.B /etc/services
entry will be supplied on all the XTE SOC machines so that this option
doesn't need to be specified.
.TP
.B -read-from-stdin
When this option is turned on, the Realtime Server expects to read
telemetry from its standard input instead of over a TCP/IP connection
to Realtime Ingest.
.TP
.B -print-auth-table
Prints out the
.B AUTH.HOSTS
table when it is first read by the process.
This can be used to verify that the
.B AUTH.HOSTS
table is being read and interpreted properly.
.TP
.B -print-client-table
Prints out the
.B RT.CLIENTS
table when it is first read by the process.
This can be used to verify that the
.B RT.CLIENTS
table is being read and interpreted properly.
.TP
.B -alternate
If
.I rtserver
is started with this option, then the intention is to use it as an alternate
server. It connects to
.I another rtserver
rather than to
.I rtingest.
Its use allows chaining of rtservers to relieve hosts that have many
clients connected. The -server-host option must be specified.
.TP
.B -do-not-use-lockfile
This instructs the Realtime Server not to obtain an exclusive lock on
its lockfile on startup. This enables more than one realtime server
process to be running at one time in the XTE SOC, though they must be running 
on separate machines.
.\" ==========================================================================
.SH "ENVIRONMENT VARIABLES"
.\" ==========================================================================
.IP SOCHOME
This is the root directory of the XTE SOC Integration and Test
directory.
There should be subdirectories
.I etc
and
.I tmp
below this directory.
The
.I etc
directory contains the file of authorized hosts and realtime clients.
.\" ==========================================================================
.SH FILES
.\" ==========================================================================
.IP $SOCHOME/etc/RT.CLIENTS
This file is a
table mapping realtime clients to their ApIDs and Spacecraft data types.
.IP $SOCHOME/etc/AUTH.HOSTS
This file is a list of
machines authorized to connect to Realtime Ingest and the Realtime Server.
.IP $SOCOPS/tmp/.rtserver.lck
The lockfile used by rtserver.
.\" ==========================================================================
.SH BUGS
.\" ==========================================================================
The Realtime Server is implemented as a pair of processes sharing a
circular buffer. One of the processes reads packets from Realtime
Ingest and writes them to the buffer and the other reads them from the
buffer and passes them to interested realtime clients. It is very
important that both of these processes exit when either of them does.
That is to say, either both of these processes must be running or
neither of them should be running.
If the Realtime Server dies and refuses to restart it may be due to
one of the pairs of the previous Realtime Server processes not having
exited. The following UNIX shell command will kill all running
Realtime Server processes on the machine on which it is invoked:
.IP
kill `ps -e | grep rtserver | /usr/bin/awk '{ print $2 }'` 
.\" ==========================================================================
.SH SEE ALSO
.\" ==========================================================================
.BR rtingest (1)
.\" ==========================================================================
.SH AUTHORS
.\" ==========================================================================
Written by Mike Lijewski and Randall Barnette, Hughes STX.

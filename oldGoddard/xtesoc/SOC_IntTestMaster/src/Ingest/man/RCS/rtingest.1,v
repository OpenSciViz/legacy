head	4.1;
access
	soccm;
symbols
	Build4_3_2:4.1
	Build4_2:1.1
	Build4:1.1;
locks
	rbarnett:4.1; strict;
comment	@# @;


4.1
date	95.06.20.20.17.54;	author rbarnett;	state Exp;
branches;
next	1.1;

1.1
date	95.01.09.18.48.35;	author rbarnett;	state Exp;
branches;
next	;


desc
@new man page for realtime ingest
@


4.1
log
@updated to match reality
@
text
@.\" ==========================================================================
.\" File Name   : rtingest.1
.\" Subsystem   : Data Ingest
.\" Programmer  : Randall D. Barnette, Hughes STX
.\" Description :
.\"
.\"   Man page for the rtingest process.
.\"
.\" RCS: $Id: rtingest.1,v 2.22 1995/05/09 15:57:53 soccm Exp $
.\"
.\" ==========================================================================
.TH rtingest 1 "20 June 1995" "XTE-SOC V4.3" "Data Ingest"
.SH NAME
rtingest \- Ingest and archive real-time telemetry stream from DCF.
.SH SYNOPSIS
.B rtingest
.RB [ -raw ]
.RB [ -read-from-stdin ]
.RB [ -do-not-archive ]
.RB [ -do-not-authorize ]
.RB [ -daemonize ]
.RB [ -print-auth-table ]
.RB [ -archive-fill-packets ]
.RB [ -output-duplicate-info ]
.RB [ -bitwise-identity-check ]
.RB [ -ingest-host
.IR host ]
.RB [ ingest-private-port
.IR pport ]
.RB [ -realtime-port
.IR iport ]
.RB [ -recorded-port
.IR qport ]
.RB [ -realtime-server-port
.IR rport]
.RB [ -statistics ]
.SH DESCRIPTION
This is the SOC process which
reads and archives the real-time telemetry stream from DCF.
It has two well-known Transmission Control Protocol (TCP)
ports over which it reads XTE CCSDS AOS
telemetry packets. These two ports conceptually correspond to the
.I "realtime channel"
and
.I "recorded channel"
from the XTE spacecraft and are known as the
.I realtime
port and the
.I recorded
port. While normally there will only be one
connection on each of these two ports, up to two connections on each
port can be active at any one time.
This arises because the DCF connections need to be
established five minutes before the respective TDRS connection, and
hence there can be a five minute period during which there are two
active connections. 
.PP
Ingested telemetry packets are archived to disk as ordered sequential
files.
Each of these files contains packets from a single ApID.
Their names are of the form:
.I nnnnnnnnnn-nnnnnnnnnn.
.PP
The first sequence is the alphanumeric representation of the Mission
Elapsed Time (MET) in the CCSDS header of the first packet in
the file. Likewise, the second numeric sequence corresponds to the
MET of the last packet in the file.
The full pathname of these packet files are of the form:
.IP
.BI $SOCOPS/ingest/realtime/day DDDD /apid AAA / nnnnnnnnnn-nnnnnnnnnn
.IP
for example:
.br
.B /socops/ingest/realtime/day0021/apid023/0001234023-0001245678.
.PP
The full date embedded in the path refers to the MET of the first
packet in the file.
.B $SOCOPS
symbolically refers to the full
pathname of the root of the directory containing the SOC operations
hierarchy. It is obtained from the environment variable
.B SOCOPS.
If a packet arrives after the packet file to which it logically
belongs has been written to disk, that packet is appended instead to a
late packet file of the form
.IP
.BI $SOCOPS/ingest/realtime/day DDDD /apid AAA /Late.Pkts.
.PP
I.e., late packets are always appended to the file
.B Late.Pkts
in the directory to which the packet logically belongs.
.PP
On startup Realtime Ingest changes its working directory to the root
of the directory tree on the machine on which it is started.
All telemetry packet archival must be single threaded
through a single Realtime Ingest processed. There
must on be no more than one Realtime Ingest process running at any
given time. While we must rely on operating procedures to ensure that
the Realtime Ingest process isn't started on more than one machine, we
programmatically ensure that no more than one Realtime Ingest process
can be running on any machine at once. We do this via a locking
mechanism based on the file
.B $SOCOPS/tmp/.realtime.lck.
Here
.B $SOCOPS
refers symbolically to the environment variable
.B SOCOPS
obtained from the environment of the executing Realtime Ingest process.
No more than one process can have a lock on this file 
at any given time. Indeed, if
.B $SOCHOME/tmp
is NFS
mounted on all XTESOC machines and the rpc.lockd daemon is running on
all machines, this mechanism will ensure that no
more than one Realtime Ingest process can be running at any
given time. Because Realtime Ingest expects to obtain a write lock on
.B $SOCOPS/tmp/.realtime.lck,
the user-id of the executing Realtime Ingest process must have both
read and write permission to this file. 
The lockfile contains a string of the form:
.I hostname.pid,
where
.I hostname
is the hostname of the machine on which
.I rtingest
was started, and
.I pid
is its process ID.
This makes it straightforward to track down the process holding
the lock.
.PP
Every connection request to
Realtime Ingest is security checked; only certain machines are allowed
to connect to Realtime Ingest as a means of filtering out unauthorized
machines/users. 
Likewise, the Realtime Server host machine must be authorized to
receive telemetry.
The on-disk table of machines authorized to connect to or receive
telemetry from Realtime Ingest is the file
.B $SOCHOME/etc/AUTH.HOSTS.
It must include all possible Pacor II RTOS machines, as well as any machine
that Realtime Ingest must communicate with such as the one on which the
Realtime Server runs. This further includes any machine the DCF or
telemetry generators are run on for debugging or testing purposes.
Realtime Ingest reads this table on startup and maintains an in-memory
copy that it uses to do authorization checks. AUTH.HOSTS also
includes all machines which are authorized to receive telemetry from
the Realtime Server -- the machines on which authorized realtime
clients run. By sending Realtime Ingest a SIGUSR1; i.e. by typing
.I "kill -USR1 pid"
, where
.I pid
is the process ID of the
Realtime Ingest process, Realtime Ingest will re-read AUTH.HOSTS and
update its in-memory tables to match the newly read AUTH.HOSTS. In this way,
machines that have been added to and/or removed from AUTH.HOSTS can be
forced to become authorized hosts, without having to stop and restart
Realtime Ingest.
.PP
If Realtime Ingest must be killed or halted for
any reason, it can be safely killed by sending it a SIGTERM signal;
i.e. type
.I "kill -TERM pid,"
where
.I pid
is the process ID of the
to-be-killed Realtime Ingest process. For debugging purposes, a core dump 
of Realtime Ingest can be obtained by sending it a SIGQUIT;
i.e. type
.I "kill -QUIT pid."
.SH OPTIONS
.TP
.B -archive-fill-packets
Fill packets are those which contain fill data or those that were
truncated owing to a packet length field shorter than expected.
It is important to note that fill packets still have valid XTE CCSDS
AOS headers.
Normally, fill packets are silently thrown away, but when this option
is specified, they're archive to disk, together with their prepended
QAC to
.BI $SOCOPS/ingest/realtime/day DDDD /Fill.Pkts.
.TP
.B -bitwise-identity-check
For each telemetry packet in the XTE mission, the ordered triple of
application identifier (ApID), time, and sequence number (seqNo)
uniquely identifies the packet. Under normal operation, when Realtime Ingest
receives a packet with the ApID, time and seqNo of one that it has
already archived, it simply discards the packet. When this option is
on, Realtime Ingest will do a bitwise comparison on the two packets in
order to verify that the two packets are indeed bitwise identical. It is
expected that this option will be used primarily for debugging and
testing. Realtime Ingest routinely receives duplicate packets because
every telemetry packet that the spacecraft sends over the
realtime channel is duplicated over the recorded channel.
.TP
.B -do-not-archive
This option instructs Realtime Ingest to not archive telemetry to disk.
It is here for debugging purposes.
.TP
.B -do-not-authorize
This option instructs Realtime Ingest not to test whether a machine is
one authorized to connect to us or not.
In effect any machine which can connect to us is considered valid.
It is here strictly for debugging purposes and should not be used in
production.
.TP
.B -daemonize
This instruct Realtime Ingest to run as a daemon. It will then
write all its error messages and debugging messages using syslog(3).
This option is expected to always be enabled during normal operations.
It is assumed that the
.B /etc/syslog.conf
file corresponding to the machine on which the process is running 
is set up so that LOG_ERR messages are sent to the console and
appended to a log file.
When this option is not enabled, all error messages and debugging messages
instead go to the terminal.
.TP
.B "-output-duplicate-info"
Prints out a short string containing the ApID, seconds and subseconds field,
and sequence counter for each duplicate packet seen.
This should only be used for debugging purposes.
In production, we expect to see duplicate packets as some portion (all?) of 
the packets that come down over the realtime channel are duplicated over the 
recorded channel.
.TP
.B "-print-auth-table"
Prints out the
.B AUTH.HOSTS
table when it is first read by the process.
This can be used to verify that the AUTH.HOSTS table is being read and
interpreted properly.
.TP
.B -read-from-stdin
When this option is turned on, Realtime Ingest expects to read
telemetry from standard input instead of over the realtime and
recorded sockets. This option overrules
.B -daemonize.
Realtime Ingest exits when there are no more
packets to be read from stdin, or on any error.
This is here strictly for debugging purposes so that we can feed known
files of telemetry through Realtime Ingest.
.TP
.B -raw
When this option is enabled, Realtime Ingest expects to get
only raw CCSDS AOS Telemetry packets. During normal operation the
telemetry stream from DCF contains Session Initiation Tokens (SIT),
Session Termination Tokens (STT), Realtime Session Status/Summary
Tokens (RSS), and Quality Assurance Capsules (QAC), in addition to the CCSDS
AOS Telemetry packets themselves.
.TP
.BI "-realtime-port " iport
This option specifies the well-known port on which Realtime Ingest
listens for and receives telemetry over the realtime channel from DCF.
If this option is not set, there must be a well-defined TCP service in
.B "/etc/services"
named
.I ingest-realtime
from which Realtime Ingest can
determine a valid port number. Under normal operations it is
expected that a valid
.B /etc/services
entry will be supplied on all the
XTESOC machines so that this option doesn't need to be specified.
.TP
.BI "-recorded-port " qport
This option specifies the well-known port on which Realtime Ingest
listens for and receives telemetry over the recorded channel from DCF.
If this option is not set, there must be a well-defined TCP service in
.B /etc/services
named
.I ingest-recorded
from which Realtime Ingest can
determine a valid port number. Under normal operations it is
expected that a valid
.B /etc/services
entry will be supplied on all the
XTESOC machines so that this option doesn't need to be specified.
.TP
.BI "-realtime-server-port " rport
This option specifies the well-known port over which Realtime Ingest
listens for connecton requests from the Realtime Server. If this
option is not set, there must be well-defined TCP service in
.B /etc/services
named 
.I realtime-server
from which Realtime Ingest can
determine a valid port number. Under normal operations it is expected that a
valid
.B /etc/services
entry will be supplied on all the XTESOC machines
so that this option doesn't need to be specified.
.TP
.BI "-ingest-private-port " pport
This option specifies the well-known port over which Realtime Ingest
connects to the Realtime Ingest Display.
.TP
.B -statistics
This option enables statistics generation. If enabled, a special packet (see
.IR PktSOF (3))
is generated periodically and passed on to the realtime
server. It contains information about packets collected during the
past 10 seconds. This option in necessary for the ingest display client
process (see
.IR ingest (1))
to receive information.
.SH "ENVIRONMENT VARIABLES"
.TP
.B SOCHOME
This is the root directory of the XTESOC Integration and Test
directory. There should be subdirectories
.I etc
and
.I tmp
below this directory. The
.I tmp
directory contains the lock file used
to guarantee that only one process is running at a time. The
.I etc
directory contains the file of authorized hosts.
.TP
.B SOCOPS
This is the root of the SOC operations directory tree into which
files of realtime telemetry packets are archived.
.SH FILES
.TP
.B $SOCOPS/ingest/realtime
root of the directory into which all files of telemetry packets are
written by Realtime Ingest, as well as Late.Pkts files.
.TP
.B $SOCHOME/etc/AUTH.HOSTS
machines authorized to connect to Realtime Ingest
.TP
.B $SOCOPS/tmp/.rtingest.lck
a lockfile
.TP
.B $SOCOPS/ingest/realtime/RSS.Pkts
Realtime Session Summaries are appended to this file.
.TP
.BI $SOCOPS/ingest/realtime/day DDDD /Fill.Pkts
Fill packets are appended to files of this form, where
.BI day DDDD
is the
mission day according to the seconds field in the packet.
They're appended together with their prepended Quality Accounting
Capsule (QAC). 
.TP
.BI $SOCOPS/ingest/realtime/day DDDD /Late.Pkts
Late packets are appended to files of this form, where
.BI day DDDD
is the
mission day according to the seconds field in the packet.
.TP
.BI $SOCOPS/ingest/realtime/day DDDD /apid AAA / nnnnnnnnnn-nnnnnnnnnn
Packets are archived to files of this form, where
.BI day DDDD
is the
mission day according to the seconds field in the packet,
.BI apid AAA
is the apid of the packets archived, and
.I nnnnnnnnnn-nnnnnnnnnn
is MET of the first and last packet in the file.
.SH BUGS
No known bugs though there are some caveats to be aware of.
Realtime Ingest uses the ANSI C atexit() function to do some processes
when exiting. Unfortunately, SunOS 4.1.3 doesn't support this
function, so this functionality of the Realtime Ingest isn't enabled.
Hopefully, a later release of SunOS will properly support the ANSI C
function atexit().
.SH SEE ALSO
.IR rtserver (1),
.IR pringest (1),
.IR qlingest (1),
.IR ingest (1),
.SH AUTHORS
Mike Lijewski <lijewski@@xema.stx.com> Hughes STX, and
.br
Randall D. Barnette <rbarnett@@xema.stx.com> Hughes STX
@


1.1
log
@Initial revision
@
text
@d2 4
d7 1
a7 1
.\" $Id: rtingest.man,v 2.21 1994/04/05 17:56:43 lijewski Exp $
d9 2
d12 1
a12 1
.TH RTINGEST 1 "9 January 1995" "XTE-SOC V4.0" "Data Ingest"
d14 1
a14 1
rtingest \- ingest and archive real-time telemetry stream from DCF.
a24 1
.RB [ -use-x-display ]
a36 3
.RB [ -stripchart-width
.IR n ]
.br
d53 1
a53 1
This arises due to the fact that the DCF connections need to be
a92 41
If started with the 
.I -use-x-display
option Realtime Ingest starts up
an X display program to which it writes various statistics including the 
MBit rates on the I and Q-channels, as well as the number of bytes ingested 
and archived over the two channels, and which are subsequently displayed in
a (hopefully) user-friendly manner. 
The X display program is an
.I "X Window"
based process which runs on the same machine as Realtime Ingest.
Realtime Ingest communicates with the X display program over a UNIX pipe.
While the X display program
itself always runs on the same machine as Realtime Ingest, by
adjusting the DISPLAY environment variable, the X program can be made
to display on any desired machine.
.PP
The X display program depends on X11R5, MOTIF, and the Athena Tools Plotter 
Widget set.
Its application defaults file is named
.B Ingest.ad
and is in
.B $SOCHOME/lib/app-defaults.
Realtime Ingest does not accept any of the standard X Toolkit command
line options, so resources may only be set via application defaults files.
.PP
The error popup is a window which pops up when the X display program notices
that Realtime Ingest has exited for some reason. More specifically,
the error popup is displayed when the UNIX pipe over which the
X display program and Realtime Ingest are communicating is closed. This should
only happen when the Realtime Ingest process dies. It displays the
string
.I "The Realtime Ingest Process Appears To Have Died"
and remains on the screen until the
.I Acknowledge
button is clicked. In order to get an error popup on exit of Realtime
Ingest, the X display program must be in use. If the display program window is
deleted, or if
.I -use-x-display
is not specified on the command line, the error popup will not appear.
It is suggested that the X display program always be used.
.PP
d289 4
a292 3
valid /etc/services entry will be supplied on all the XTESOC machines
so that this option doesn't 
need to be specified.
d299 8
a306 26
This option specifies to output some statistics while running.
The statistics are output either to standard error or to the console
via syslogd(8).
.TP
.BI "-stripchart-width " n
This option specifies the number of maximum number of seconds worth of
updates to the stripchart embedded in the X display program that'll be
displayed in the stripchart at one time.
Figuratively, it is the maximum width of the stripchart.
If it is not specified, it defaults to 5400 seconds (90 minutes),
which is roughly one orbit of the XTE spacecraft.
There are hard limits below which and above which this number cannot
be set.
If this number is specified above or below those limits, it'll be set
to the respective limit and a message detailing that will be printed.
.TP
.B -use-x-display
When this option is not enabled, the X display program is not started or used.
Note that if the X display program is not used the error window which
normally pops up when the Realtime Ingest process dies, will not be
displayed. 
If this option is set and the X display program dies (or is killed) for some
reason, the
.I -statistics
option is dynamically enabled so that the user can continue to get the
relevant statistics via the machine console.
a308 4
.B DISPLAY
This sets the machine and screen on which the X display program will be
displayed.
.TP
d371 4
a374 4
.BR rtserver (1),
.BR pringest (1),
.BR qlingest (1)
.BR ingest-win (1)
@

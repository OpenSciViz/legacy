head	1.1;
access
	jonv
	gromeo;
symbols;
locks
	subrata:1.1; strict;
comment	@ * @;


1.1
date	97.08.06.17.52.59;	author subrata;	state Exp;
branches;
next	;


desc
@@


1.1
log
@Initial revision
@
text
@// Programmer: S. Ghosh, HSTX

// This is the only module in the rtclient code which should need to be
// modified by quicklook client writers.  Simply replace the innards of
// client_specific_initialization() and process_a_packet() with the correct
// code for your situation.  A few of the other functions in here may be
// tweaked where explicitly mentioned.
//

#ifndef __SUMMARYCLIENT_C__
#define __SUMMARYCLIENT_C__
 
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <iostream.h>
#include <fstream.h>
#include <iomanip.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include "PktTlm.h"
#include "PcaHKPart.h"
#include "HxCluster.h"
#include "HxHk16Part.h"
#include "HxHk64Part.h"
#include "HxHk128Part.h"
#include "PktAsmSensor.h"
#include "fs_if.h"
#include <Utilities.h>
#include <SOCTime2.h>
#include <rtclient.h>

// Rogue Wave headers
#include <rw/cstring.h>
#include <rw/hashdict.h>

//initial values for variables of different sizes to indicate UNKNOWN

#define UNKNOWN "-"
#define INIT_CHAR 255
#define INIT_SHORT 65535
#define INIT_INT 9999999
#define INIT_FLAG 2
#define MAXVIOLATION 300

const unsigned int ACS  = 14;
const unsigned int PCU1 = 90;
const unsigned int PCU2 = 91;
const unsigned int PCU3 = 92;
const unsigned int PCU4 = 93;
const unsigned int PCU5 = 94;
 
//The port of the Realtime Server to which we connect to get telemetry.

static short server_port;

// The machine on which the Realtime Server is executing.

static const char* server_host="xitserv";

// Should we run as a daemon?

static int run_as_daemon;


// Should we read packets from STDIN instead of from the Realtime Server?
static int read_from_stdin;
static RWBoolean _DEBUG = FALSE;

//
// This is the name we advertise to the Realtime Server.  It is the same as
// the entry in the relevant $SOCHOME/etc/RT.CLIENTS table.You will probably
// want to hardcode the standard name for your client here, instead of 
// relying on command line arguments and parse_arguments() to set it.

static const char* client_name = "ALL-PKTS";


// Our name -- set in main().  Not necessarily the same as the name we
// advertise to the Realtime Server.  This is used for reporting of 
// errors and messages.

static char* ProgName;

enum LimitStatus {lmNORMAL, lmRED, lmYELLOW, lmUNDEFINED};

struct Limit_Viol_Type 
{
   RWCollectableString* mnem;
   double val;
   LimitStatus violation;
   double lower, upper;
};
   
// Data structure for PCA limit checking

static unsigned char PrHVcmd[5], XeHVcmd[5], HRMflag[5];

static unsigned short PrHVmeas[5], XeHVmeas[5];

// Data structure for ASM limit checking

static double AsmHV[3];

static int AsmLATCH[3];

// Data structure for HEXTE limit checking

static RWHashDictionary *itemBag;
static RWHashDictionary limit_table;
static HxCmdItem theCmdItem;

static Limit_Viol_Type HexViol[2][MAXVIOLATION];
static short HexViolCount[2];

static RWCString theCompletePath;
static char * socops = getenv("SOCOPS");
char* filename= "SOC_Summary";
ofstream out;

void print(ofstream& os, unsigned long t)
{
 os.open(theCompletePath);

 os << endl << "Time: " << SOCTime2((unsigned) t) << endl << endl;

 for (int d= PktAsmSensor::Camera1; d <= PktAsmSensor::Camera3; d++)
 { 
  os << "SSC" << d << " HV ";
  (AsmHV[d-1]==INIT_INT)     ? os << UNKNOWN : os << AsmHV[d-1]; 
  os << " LATCH FLAG ";
  (AsmLATCH[d-1]==INIT_FLAG) ? os << UNKNOWN : os << AsmLATCH[d-1];
  os << endl;
 }
 os << endl;

 for (int i=0; i <=4; i++)
 { 
  os << "PCU" << (i+1) << " PrHVcmd ";
 (PrHVcmd[i] == INIT_CHAR) ? os << UNKNOWN : os << (int)PrHVcmd[i];
 os  << " PrHVmeas "; 
 (PrHVmeas[i]==INIT_SHORT) ? os << UNKNOWN : os << PrHVmeas[i];
 os << " XeHVcmd ";
 (XeHVcmd[i] == INIT_CHAR) ? os << UNKNOWN : os << (int) XeHVcmd[i];
 os << " XeHVmeas ";
 (XeHVmeas[i]==INIT_SHORT) ? os << UNKNOWN : os << XeHVmeas[i];
 os << " HRMflag ";
 (HRMflag[i]==INIT_CHAR)   ? os << UNKNOWN : os << (int) HRMflag[i];
 os << endl;}

 os << "\nHEXTE\n";
 os << "\nClusterA\n\n";

 if (HexViolCount[0] == 0) {
   os << "No Limit Violations\n";
 }
 else
 {
   for (i=0; i < HexViolCount[0]; i++)
   {
    os << "Mnem: " << setw(15) << HexViol[0][i].mnem->data() << " " << 
          " Val: " << setprecision(4) << HexViol[0][i].val << " LV: ";
    (HexViol[0][i].violation == lmYELLOW) ? 
          os << "Yellow" : os << "Red   ";
    os << " LL: "  << HexViol[0][i].lower << " " << 
          " UL: "  << HexViol[0][i].upper << endl;
   }
 } 

 os << "\nClusterB\n\n";

 if (HexViolCount[1] == 0) {
   os << "No Limit Violations\n";
 }
 else
 {
   for (i=0; i < HexViolCount[1]; i++)
   {
    os << "Mnem: " << setw(15) << HexViol[1][i].mnem->data() << " " << 
          " Val: " << setprecision(4) << HexViol[1][i].val << " LV: ";
    (HexViol[1][i].violation == lmYELLOW) ? 
           os << "Yellow" : os << "Red   ";
    os << " LL: "  << HexViol[1][i].lower << " " << 
          " UL: "  << HexViol[1][i].upper << endl;
   }
 } 
 os.close();
}

static void readLimitTable(void)
{
   // Fill the limitTable with the items
   // that have limits not equal to zero.
  
   RWHashDictionaryIterator rwhdi( *itemBag ); 
   RWCollectableString * key;
   HxTbItem * item;
 
   while ( rwhdi() )  // Iterate over the HxTbItem Bag
   {
     key  = (RWCollectableString *) rwhdi.key();
     item = (HxTbItem * )           rwhdi.value();
 
     if ((item->lowerRedLim() != 0) || (item->lowerYellowLim() != 0 ) ||
   (item->upperRedLim() != 0) || (item->upperYellowLim() != 0 ))
      {
       limit_table.insertKeyAndValue( key, item );
//       item->printLong(cerr);
      }
   }
}


static void Hx16HKhandler(const HxHk16Part * hx16 )
{
//  cerr << "in Hx16HKhandler" << endl;
}

static void Hx64HKhandler(const HxHk64Part * hx64 )
{
// cerr << "in Hx64HKhandler" << endl;
}


//
// Returns the limit status of a particular data item in the partition.
//
enum LimitStatus getLimitStatus(const double * data, const short elem,
                                HxTbItem * item)
{
   float lr, ly, uy, ur;
   //cerr << "ITEM IS: <" << item->mnem() << ">" << endl;
   lr = item->lowerRedLim();
   ly = item->lowerYellowLim();
   uy = item->upperYellowLim();
   ur = item->upperRedLim();
 
   // Since we are only dealing with converted data values,
   // all data types will be BaseType_DOUBLE according to HxHk128Part.C code.
 
   if( data != NULL ){
     double d;
     d = data[elem];
     if( d <= lr || d >= ur ) return lmRED;
     else if( d <= ly || d >= uy ) return lmYELLOW;
     else return lmNORMAL;
   }
   // --- TODO handle this problem a better way than this ---
   else
     return lmNORMAL;
}
 


static void Hx128HKhandler(const HxHk128Part * hx128 )
{
  int whichCluster = hx128->clusterID()-1;
  int count = 0;

//cerr << "-----------Partition 128: Cluster: " << whichCluster << "----------" << endl;

  HxTbItem * item;
  RWCollectableString * mnem_name;
  enum LimitStatus ls;

  //
  // Loop through list of mnemonics for Hexte.
  //
 
  RWHashDictionaryIterator hdi(limit_table);
 

  while (hdi())
  {
   mnem_name = (RWCollectableString *) hdi.key();
   item = (HxTbItem*) hdi.value();

 
   // Retrieve data from partition
   short elements=0, type=0;
 
   double*  pItemVal = (double *) hx128->getStatData( (char *) mnem_name->data(), elements,  type ) ;
   
   if (!pItemVal)
   {
    //  cerr << " No data found for " << mnem_name->data() << endl;
   }
   else
   {
     for (int i=0; i<elements;i++)
     {
      ls = getLimitStatus(pItemVal,i,item);
      if (ls != lmNORMAL)
      {
        delete HexViol[whichCluster][count].mnem;
        HexViol[whichCluster][count].mnem = new RWCollectableString(item->mnem());
        HexViol[whichCluster][count].val = pItemVal[i];
        HexViol[whichCluster][count].violation = ls;
        if (ls == lmYELLOW){
          HexViol[whichCluster][count].lower = item->lowerYellowLim();
          HexViol[whichCluster][count].upper = item->upperYellowLim();
        }
        else {
          HexViol[whichCluster][count].lower = item->lowerRedLim();
          HexViol[whichCluster][count].upper = item->upperRedLim();
        }
        count++;
       }
     }
   }
   delete(pItemVal);
  }
  HexViolCount[whichCluster] = count;
  cerr << "in Hx128HKhandler" << endl;
}

static void PcaHKhandler(const PcaHKPart * pca )
{
 int index = (int) pca->apid()-90;

 PrHVcmd[index] = pca->cmdhvPR();
 XeHVcmd[index] = pca->cmdhvXE();
 PrHVmeas[index] = pca->hvPRh();
 XeHVmeas[index] = pca->hvXEh();
 HRMflag[index] = pca->hrmflag();
}

static void AsmHKhandler(const PktAsmSensor*  a)
{
 for (int d= PktAsmSensor::Camera1; d <= PktAsmSensor::Camera3; d++)
 {
  PktAsmSensor::SscId dd = (PktAsmSensor::SscId) d;
  AsmHV[d-1] = a->sscHiVoltage(dd);
  AsmLATCH[d-1] = a->sscEventRateIsLatched(dd);
 }
}

//
// This is the routine that does the packet-specific processing.  Do not
// change the signature of this function.  Each time one of the packets in
// which you've specified an interest is received by the quicklook client
// process, this function is called -- this includes any packets containing
// S/C data in which you've indicated an interest in receiving, as well.
// The caching of S/C data is done for you in the librtclient.a code, but only
// if you specify that you want it cached.  You can specify that you want
// to get S/C data from the Realtime Server which you must then choose whether
// you want to cache it or not.  You really have three options when you're
// receiving spacecraft data:
//
//   1) cache it and ignore the S/C packets themselves;
//   2) just deal with the raw S/C packets;
//   3) both (1) & (2).
//
// For non-spacecraft data you must deal directly with the packets.
//

void process_a_packet (const PktTlm* pkt)
{
  static unsigned long latest_time=0;
 
  if (pkt->seconds() < latest_time) return;
  else latest_time = pkt->seconds();

  if (_DEBUG) {

    cout << "  App Id = "     << pkt->applicationID()
         << ", SeqNo = "      << pkt->sequenceNumber()
         << ", Length = "     << pkt->packetLength()
         << ", Secs = "       << pkt->seconds() << endl;
  }
  pkt->apply();

  if (((pkt->applicationID() >= PCU1)&& (pkt->applicationID() <= PCU5))  || ((pkt->applicationID() == 49) || (pkt->applicationID() == 65))
  || ((pkt->applicationID() == 83) || (pkt->applicationID() == 89)))
      print(out,latest_time);
} 
  
static void client_specific_initialization (void)
{

 theCompletePath.append(filename);

 out.open(theCompletePath.data());

 for (int i=0; i<5;i++)
 {
  PrHVcmd[i] = INIT_CHAR; XeHVcmd[i] = INIT_CHAR; HRMflag[i] = INIT_CHAR;
  PrHVmeas[i] = INIT_SHORT; XeHVmeas[i]= INIT_SHORT;
 }
 for (i=0; i<3;i++)
 {
  AsmHV[i] = INIT_INT;
  AsmLATCH[i] = INIT_FLAG;
 } 
 itemBag = new RWHashDictionary(theCmdItem.getTbItemBag() );
 readLimitTable();
#ifdef XTEST
  initialize_x_client(display_client);
#endif
}


//
// Print out usage string and exit.  If you change parse_arguments(),
// make the relevant changes here as well.
//

static void usage (void)
{
    cerr << "\nusage: " << ProgName
         << " [-daemonize]"
         << " -host hostname"
         << " [-port #]"
         << " -name client-name"
         << " [-read-from-stdin]\n\n";
    exit(1);
}


//
// Parse our arguments.  Feel free to add your own client-specific options.
// If you do so, make sure to update usage() appropriately.
//

static void parse_arguments (char**& argv)
{
    //
    // Parse any arguments.
    //
    //    -daemonize           -- run as a daemon
    //    -display-client name -- name of X client program to execute
    //    -host machine        -- quicklook server machine
    //    -port #              -- port number to connect toquicklook server
    //    -name name           -- the name we advertise to the quicklook server
    //    -read-from-stdin     -- read from stdin instead of over a socket

    while (*++argv && **argv == '-')
    {
        if (strcmp(*argv, "-host") ==  0)
        {
            if (!(server_host = *++argv))
                error("No hostname supplied.");
        }
        else if (strcmp(*argv, "-port") == 0)
        {
            if (*++argv)
            {
                if ((server_port = (short) atoi(*argv)) <= 0)
                    error("Port # must be positive short.");
                continue;
            }
            else
                error("No port # supplied.");
        }
        else if (strcmp(*argv, "-daemonize") == 0)
            run_as_daemon = 1;
         
        else if (strcmp(*argv, "-name") ==  0)
        {
            if (!(client_name = *++argv))
                error("No name supplied.");
        }
	      else if (strcmp( *argv, "-debug") == 0 ) 
	         _DEBUG= TRUE;
        else if (strcmp(*argv, "-read-from-stdin") == 0)
            read_from_stdin = 1;
        else
        {
            message("`%s' is not a valid option, exiting ...", *argv);
            usage();
        }
    }

    if (read_from_stdin && run_as_daemon)
    {
        run_as_daemon = 0;
        message("Using `-read-from-stdin' implies ! `-daemonize'.");

    }

    if (!server_host && !read_from_stdin)
    {
        message("`quicklook-server-host' or `read-from-stdin' must be specified.");
        usage();
    }

    if (!read_from_stdin && !client_name)
        usage();

    //
    // Check that basic environment variables are set.
    //
    if (getenv("SOCHOME") == 0)
        error("The `SOCHOME' environment variable must be set.");

    if (getenv("SOCOPS") == 0)
        error("The `SOCOPS' environment variable must be set.");
}

// Generic signal catcher.
 
static void my_catch_a_signal(int sig)
{
  //
  // We block all signals in this call.
  //
  sigset_t all_sigs;
 
  if(sigfillset(&all_sigs) < 0)
    syserror("%s, line %d, sigfillset() failed", __FILE__, __LINE__);
 
  if(sigprocmask(SIG_SETMASK, &all_sigs, 0) < 0)
    syserror("%s, line %d, sigprocmask(SIG_SETMASK) failed", __FILE__, __LINE__);

  message("Caught signal #%d, trying to exit nicely ...", sig);
 
  (PID == 0) ? kill(PPID, SIGTERM) : kill(PID, SIGTERM);

  exit(sig);
}
 
//
// Simple error handler for use by PktCCSDS.  Change this if you want
// to do something fancier.
//

static void packet_error_handler (const char* msg) { error(msg); }


//
// A simple Out-Of-Memory Exception handler for main().  Feel free
// to change this if you really can do better than exit.
//

static void free_store_exception (void)
{
    message("Out of memory, exiting ...");
    if (!read_from_stdin)
        force_exit_of_other_half();
    exit(1);
}


//
// The main routine.  Please do not change this without very good reason.
// client_specific_initialization() is placed such that you should be able
// to override many of my assumptions if you so choose.
//

int main (int, char* argv[])
{
    ProgName = argv[0];
    PcaHKPart::setDisposition(PcaHKhandler);
    PktAsmSensor::setDisposition(AsmHKhandler);
    HxHk16Part::setDisposition(Hx16HKhandler);
    HxHk64Part::setDisposition(Hx64HKhandler);
    HxHk128Part::setDisposition(Hx128HKhandler);
    initialize_signal_handlers(my_catch_a_signal);
    parse_arguments(argv);

    if (socops == NULL ) {
      cerr << "environment variable SOCOPS not set.\n"; 
      exit(-1);
    } else {
     theCompletePath.append(socops);
     theCompletePath.append("/log/");
     mkdir(theCompletePath.data(), 0755 );
    }

    if (run_as_daemon)
        //
        //Feel free to daemonize to someplace other than the filesystem
        //root if it makes sense for your application.
        //
        daemonize(client_name, "/");

    if (read_from_stdin)
    {
        //
        // Do client-specific setup.
        //
        client_specific_initialization();
        read_packets_from_stdin();
    }
    else
    {
        //
        // Now fork to create a child process. The parent process
        // (aka back-end) reads packets from the Realtime Server and
        // places them in the ring buffer.  The child process
        // (aka front-end) reads these packets and does the appropriate
        // processing.
        //
        if ((PID = fork()) == 0)
        {
            //
            // This code is only executed by the front-end portion of the
            // quicklook client; that part that is actually interested in 
            // the quicklook analysis/display of the data in the packets.
            //
            PPID = getppid();
            //
            // Do client-specific setup.
            //
            client_specific_initialization();
            //
            // The primitive event mechanism in process_packets() isn't
            // compatible with the X Event mechanism.  If you want to drive
            // X Windows directly from this code, instead of fork()ing and
            // exec()ing a standalone X Windows process to which you write
            // the required data, please talk to me so we can work out the
            // best way to do it, together.  I have some ideas.  I just
            // haven't felt like putting in the time, if it wasn't clear
            // that it was going to be used.
            //
            process_packets();
        }
        else
        {
            //
            // This code is only executed by the back-end of the quicklook
            // client.  It is the code that maintains the connection to the
            // quicklook server and gives well-built CCSDS Telemetry packets
            // to the front-end of the quicklook client.
            //
//            (void) catch_signal(SIGCHLD, rtclient_chld_catcher);
            connect_to_realtime_server(client_name, server_port, server_host);
        }
    }

    return 0;
}
#endif
@

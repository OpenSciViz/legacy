head	4.1;
access
	rbarnett
	soccm
	jonv
	gromeo;
symbols
	Build5_1:4.1
	Build4_5_3:4.1
	Build4:2.1
	Build3_3:2.1
	Build3_2:2.1
	Build3_1:2.1
	Build3:2.1
	Current:2.1
	Build2:2.1;
locks
	rbarnett:4.1; strict;
comment	@ * @;


4.1
date	95.10.23.19.44.23;	author rbarnett;	state Exp;
branches;
next	2.2;

2.2
date	95.10.23.13.53.12;	author rbarnett;	state Exp;
branches;
next	2.1;

2.1
date	93.10.08.14.59.16;	author lijewski;	state Release;
branches;
next	2.0;

2.0
date	93.08.10.19.55.59;	author lijewski;	state Exp;
branches;
next	1.3;

1.3
date	93.08.06.14.51.46;	author lijewski;	state Exp;
branches;
next	1.2;

1.2
date	93.06.29.19.55.53;	author lijewski;	state Exp;
branches;
next	1.1;

1.1
date	93.06.29.18.16.21;	author lijewski;	state Exp;
branches;
next	;


desc
@Creation
@


4.1
log
@documentation update
@
text
@// ===========================================================================
// File Name   : PktKey.h
// Subsystem   : Data Ingest
// Programmer  : Randall D. Barnette, Hughes STX
// Description : Declaration of PktKey class
//
// .NAME    PktKey - SkipList indexing items.
// .LIBRARY subclass
// .HEADER  Ingest
// .INCLUDE PktKey.h
// .VERSION $Revision 2.14 $
//
// $Id: PktKey.h,v 2.2 1995/10/20 19:29:04 soccm Exp $
//
// ===========================================================================

// .SECTION Author
// Mike Lijewski, NASA/GSFC, lijewski@@rosserv.gsfc.nasa.gov

// .SECTION Description
// The key by which items in a SkipList of packets are indexed.  We
// assume that all the Pkts in a SkipList have the same ApID, so all
// we need look at is the time and sequence number.
//
// KEYs need to have operator==(), operator!=() and operator<() defined,
// as well as a function KEY::maxKey() which returns the maximum value
// of the KEY type; i.e. for any key of type KEY `key < maxKey()' is true
// for all values of type key except the value maxKey() itself.
// When debugging, we also need a way to output keys.

#ifndef PKTKEY_H
#define PKTKEY_H

#include <limits.h>

#include <PktTlm.h>

struct PktKey {

	unsigned long int secs;     // The seconds field.
	unsigned long int subsecs;  // The seconds field.
	int seqno;                  // The sequence number.

	//
	// I've given defaults to the arguments here so that in the
	// struct Node in "SkipList.h" doesn't need any constructor.
	//
	PktKey(unsigned long sec=0, unsigned long sub=0, int seqNo=0) ;

	PktKey(const PktTlm* pkt) ;

	int operator==(const PktKey& rhs) const;
	int operator!=(const PktKey& rhs) const;
	int operator<(const PktKey& rhs) const;

	static struct PktKey maxKey(void);

	friend ostream& operator<<(ostream& os, const PktKey& key);
};

//
// Inlines for PktKey.
//

// Description:
// Construct from values.

inline PktKey::PktKey(unsigned long sec, unsigned long sub, int seqNo):
  secs(sec), subsecs(sub), seqno(seqNo) {}

// Description:
// Construct from a packet.

inline PktKey::PktKey(const PktTlm* pkt):
	secs(pkt->seconds()),
  subsecs(pkt->subSeconds()),
	seqno(pkt->sequenceNumber()) {}

// Description:
// Equality operator.

inline int PktKey::operator==(const PktKey& rhs) const
{
	return secs == rhs.secs && subsecs == rhs.subsecs && seqno == rhs.seqno;
}

// Description:
// Inequality operator.

inline int PktKey::operator!=(const PktKey& rhs) const
{
	return !(operator==(rhs));
}

// Description:
// Less than operator.

inline int PktKey::operator<(const PktKey& rhs) const
{
	return secs < rhs.secs                            ||
				(secs == rhs.secs && subsecs < rhs.subsecs) ||
        (secs == rhs.secs && subsecs == rhs.subsecs && seqno < rhs.seqno);
}

// Description:
// Return a PktKey with the maximum key value.

inline PktKey PktKey::maxKey(void)
{
	return PktKey(ULONG_MAX, ULONG_MAX, INT_MAX);
}

// Description:
// Ostream output operator.

inline ostream& operator<<(ostream& os, const PktKey& key)
{
	return os << "(" << key.secs
						<< ", " << key.subsecs
						<< ", " << key.seqno
						<< ")";
}

#endif /*PKTKEY_H*/
@


2.2
log
@*** empty log message ***
@
text
@d1 11
d13 1
a13 1
// $Id: PktKey.h,v 2.1 1993/10/08 14:59:16 lijewski Release rbarnett $
d15 1
d17 2
a18 4
#ifndef PKTKEY_H
#define PKTKEY_H

#include <limits.h>
d20 1
a20 4
#include "PktTlm.h"


//
a29 2
//
//
d31 24
a54 27
struct PktKey
{
    unsigned long int secs;     // The seconds field.
    unsigned long int subsecs;  // The seconds field.
    int seqno;                  // The sequence number.

    //
    // I've given defaults to the arguments here so that in the
    // struct Node in "SkipList.h" doesn't need any constructor.
    //
    PktKey (unsigned long seconds=0, unsigned long subseconds=0, int seqNo=0)
    {
        secs = seconds;
        subsecs = subseconds;
        seqno = seqNo;
    }

    PktKey (const PktTlm* pkt)
    {
        secs = pkt->seconds();
        subsecs = pkt->subSeconds();
        seqno = pkt->sequenceNumber();
    }

    int operator== (const PktKey& rhs) const;
    int operator!= (const PktKey& rhs) const;
    int operator<  (const PktKey& rhs) const;
d56 1
a56 1
    static struct PktKey maxKey (void);
d58 1
a58 1
    friend ostream& operator<< (ostream& os, const PktKey& key);
a60 1

d65 16
d82 1
a82 1
inline int PktKey::operator== (const PktKey& rhs) const
d84 1
a84 1
    return secs == rhs.secs && subsecs == rhs.subsecs && seqno == rhs.seqno;
d87 2
d90 1
a90 1
inline int PktKey::operator!= (const PktKey& rhs) const
d92 1
a92 1
    return !(operator==(rhs));
d95 2
d98 1
a98 1
inline int PktKey::operator< (const PktKey& rhs) const
d100 3
a102 3
    return secs < rhs.secs                             ||
           (secs == rhs.secs && subsecs < rhs.subsecs) ||
           (secs == rhs.secs && subsecs == rhs.subsecs && seqno < rhs.seqno);
d105 2
d108 1
a108 1
inline PktKey PktKey::maxKey (void)
d110 1
a110 1
    return PktKey(ULONG_MAX, ULONG_MAX, INT_MAX);
d113 2
d116 1
a116 1
inline ostream& operator<< (ostream& os, const PktKey& key)
d118 4
a121 7
    return os << "("
              << key.secs
              << ", "
              << key.subsecs
              << ", "
              << key.seqno
              << ")";
@


2.1
log
@am now sorting by seconds, subseconds and seqNo
@
text
@d2 1
a2 1
// $Id: PktKey.h,v 2.0 1993/08/10 19:55:59 lijewski Exp lijewski $
@


2.0
log
@NewBuild
@
text
@d2 1
a2 1
// $Id: PktKey.h,v 1.3 1993/08/06 14:51:46 lijewski Exp lijewski $
d28 3
a30 2
    unsigned long int secs;  // The seconds field.
    int seqno;               // The sequence number.
d36 13
a48 2
    PktKey (unsigned long seconds = 0, int seqNo = 0) { secs  = seconds;
                                                        seqno = seqNo; }
a49 3
    PktKey (const PktTlm* pkt) { secs  = pkt->seconds();
                                 seqno = pkt->sequenceNumber(); }

d67 1
a67 1
    return secs == rhs.secs && seqno == rhs.seqno;
d79 3
a81 1
    return secs < rhs.secs ? 1 : secs > rhs.secs ? 0 : seqno < rhs.seqno;
d87 1
a87 1
    return PktKey(ULONG_MAX, INT_MAX);
d93 7
a99 1
    return os << "(" << key.secs << ", " << key.seqno << ")";
@


1.3
log
@now include PktTlm.h
@
text
@d2 1
a2 1
// $Id: PktKey.h,v 1.2 1993/06/29 19:55:53 lijewski Exp lijewski $
@


1.2
log
@added constructor taking a PktTlm*
@
text
@d2 1
a2 1
// $Id: PktKey.h,v 1.1 1993/06/29 18:16:21 lijewski Exp lijewski $
d10 1
d12 1
a37 1
#ifndef TEST
a39 1
#endif
@


1.1
log
@Initial revision
@
text
@d2 1
a2 1
// $Id$
d33 2
a34 1
    PktKey (unsigned long seconds=0, int seqNo=0) {secs=seconds; seqno=seqNo;}
d36 5
d80 1
a80 1
ostream& operator<< (ostream& os, const PktKey& key)
@

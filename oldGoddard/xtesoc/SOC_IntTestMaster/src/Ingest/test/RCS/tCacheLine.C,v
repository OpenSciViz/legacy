head	2.2;
access
	rbarnett
	soccm;
symbols
	Build4_4:2.2
	Build4:2.1
	Build3_3:2.1
	Build3_2:2.1
	Build3_1:2.1
	Build3:2.1
	Current:2.1
	Build2:2.1;
locks; strict;
comment	@ * @;


2.2
date	95.08.03.21.32.36;	author rbarnett;	state Exp;
branches;
next	2.1;

2.1
date	93.11.18.18.50.56;	author lijewski;	state Release;
branches;
next	;


desc
@Creation
@


2.2
log
@First controlled delivery with test plan
@
text
@//
// Simple test case for CacheLine class.
//
// $Id: tCacheLine.C,v 2.1 1993/11/18 18:50:56 lijewski Release $
//

#include <iostream.h>
#include <math.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>

#include "CacheLine.h"


struct key
{
    unsigned long int secs;

    key (unsigned long seconds = 0) { secs  = seconds; }

    int operator== (const key& rhs) const { return secs == rhs.secs; }
    int operator!= (const key& rhs) const { return !(operator==(rhs)); }
    int operator< (const key& rhs)  const { return secs < rhs.secs; }

    static struct key maxKey (void) { return key(LONG_MAX); }

    friend ostream& operator<< (ostream& os, const key& k)
    {
           return os << k.secs;
    }
};


struct triple
{
    double x, y, z;

    triple(double a=0.0, double b=0.0, double c=0.0) : x(a), y(b), z(c) {}

    int operator== (const triple& rhs) const { return x==rhs.x && y==rhs.y && z==rhs.z; }
    int operator!= (const triple& rhs) const { return !(operator==(rhs)); }

    friend ostream& operator<< (ostream& os, const triple& t)
    {
        return os << "(" << t.x << ", " << t.y << ", " << t.z << ")";
    }
};


int main (void)
{
    cout << "Exercising CacheLine<key, triple> ...\n";
    CacheLine<key, triple> l(100);

    cout << "Exercising isEmpty() ...\n";
    if (!l.isEmpty())
    {
        cerr << "isEmpty() failed!\n";
        exit(1);
    }

    cout << "Exercising size() ...\n";
    if (l.size() != 100)
    {
        cerr << "size() != 100 -- size() failed!\n";
        exit(1);
    }
    
    //
    // Insert 0 .. 5000.
    //
    cout << "Exercising insert() ...\n";
    key k;
    for (int i = 0; i < 5000; i++)
    {
        k.secs = i;
        triple t(i*1.0, i*1.1, i*1.2);
        l.insert(k, t);
    }

    //
    // CacheLine should now contain 4900 .. 4999.
    //

    cout << "Exercising entries() ...\n";
    if (l.entries() != 100)
    {
        cerr << "entries() != 100 -- entries() failed!\n";
        exit(1);
    }

    cout << "Exercising isFull() ...\n";
    if (!l.isFull())
    {
        cerr << "CacheLine should be full -- isFull() failed!\n";
        exit(1);
    }

    cout << "Exercising smallest() ...\n";
    triple small(4900, 4900*1.1, 4900*1.2);
    if (*((triple*)l.smallest()) != small)
    {
        cerr << "smallest() failed!\n";
        exit(1);
    }

    cout << "Exercising largest() ...\n";
    triple large(4999, 4999*1.1, 4999*1.2);
    if (*((triple*)l.largest()) != large)
    {
        cerr << "largest() failed!\n";
        exit(1);
    }

    cout << "Exercising nearest() ...\n";
    if (*((triple*)l.nearest(2)) != small)
    {
        cerr << "nearest(2) failed!\n";
        exit(1);
    }
    if (*((triple*)l.nearest(10000)) != large)
    {
        cerr << "nearest(10000) failed!\n";
        exit(1);
    }
    triple middle(4910, 4910*1.1, 4910*1.2);
    if (*((triple*)l.nearest(4910)) != middle)
    {
        cerr << "nearest(4910) failed!\n";
        exit(1);
    }

    cout << "Exercising range() ...\n";
    int nrange = 0;
    triple* values;
    values = l.range(0, 100, nrange);
    if (values || nrange)
    {
        cerr << "range() failed -- 0 .. 100 should be NULL!\n";
        exit(1);
    }
    values = l.range(4950, 4940, nrange);
    if (values || nrange)
    {
        cerr << "range() failed -- 4950 .. 4940 should be NULL!\n";
        exit(1);
    }
    values = l.range(5000, 6000, nrange);
    if (values || nrange)
    {
        cerr << "range() failed -- 5000 .. 6000 should be NULL!\n";
        exit(1);
    }
    values = l.range(4999, 6000, nrange);
    if (nrange != 1)
    {
        cerr << "range() failed -- 4999 .. 6000 should contain 1 value!\n";
        exit(1);
    }
    values = l.range(0, 4900, nrange);
    if (nrange != 1)
    {
        cerr << "range() failed -- 0 .. 4900 should contain 1 value!\n";
        exit(1);
    }
    values = l.range(300, 4950, nrange);
    if (nrange != 51)
    {
        cerr << "range() failed -- 300 .. 4950 should contain 51 values!\n";
        exit(1);
    }
    values = l.range(4940, 4950, nrange);
    if (nrange != 11)
    {
        cerr << "range() failed -- 4940 .. 4950 should contain 11 values!\n";
        exit(1);
    }
    values = l.range(4990, 6000, nrange);
    if (nrange != 10)
    {
        cerr << "range() failed -- 4990 .. 6000 should contain 10 values!\n";
        exit(1);
    }

    return 0;
}

@


2.1
log
@Creation
@
text
@d4 1
a4 1
// $Id$
@

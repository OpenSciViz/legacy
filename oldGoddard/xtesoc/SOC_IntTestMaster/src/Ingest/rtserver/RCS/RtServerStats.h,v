head	4.14;
access
	dhon
	soccm;
symbols
	current:4.14
	Build4_2:4.14
	Current:4.13;
locks;
comment	@ * @;


4.14
date	95.03.22.20.23.00;	author dhon;	state Exp;
branches;
next	4.13;

4.13
date	95.03.17.20.33.32;	author dhon;	state Exp;
branches;
next	4.12;

4.12
date	95.03.16.19.43.13;	author dhon;	state Exp;
branches;
next	4.11;

4.11
date	95.03.16.15.37.26;	author dhon;	state Exp;
branches;
next	4.10;

4.10
date	95.03.14.21.27.21;	author dhon;	state Exp;
branches;
next	4.9;

4.9
date	95.03.14.20.26.47;	author dhon;	state Exp;
branches;
next	4.8;

4.8
date	95.03.14.19.12.53;	author dhon;	state Exp;
branches;
next	4.7;

4.7
date	95.03.13.19.29.50;	author dhon;	state Exp;
branches;
next	4.6;

4.6
date	95.03.13.19.24.57;	author dhon;	state Exp;
branches;
next	4.5;

4.5
date	95.03.13.19.24.11;	author dhon;	state Exp;
branches;
next	4.4;

4.4
date	95.03.13.18.41.44;	author dhon;	state Exp;
branches;
next	4.3;

4.3
date	95.03.12.20.08.08;	author dhon;	state Exp;
branches;
next	4.2;

4.2
date	95.03.12.19.26.34;	author dhon;	state Exp;
branches;
next	4.1;

4.1
date	95.03.12.02.52.57;	author dhon;	state Exp;
branches;
next	;


desc
@@


4.14
log
@PktStats now has time and valid flag
@
text
@#ifndef __RTSERVERSTATS_H__
#define __RTSERVERSTATS_H__

// RCS: $Id: RtServerStats.h,v 4.13 1995/03/17 20:33:32 dhon Exp dhon $
#include "rw/bitvec.h"
#include "rw/rwtime.h"
#include "rw/rwfile.h"
#include "rw/cstring.h"
#include "rw/collint.h"
#include "rw/dvec.h"
#include "rw/ivec.h"
#include "rw/tphdict.h"

struct PktStats: public RWTime, public RWBitVec {
  double _total;
  RWCString _name;
  IntVec _apid;
  DoubleVec _count;
  PktStats();
  PktStats(const char* nm, int maxId);
  void saveOn(RWFile& rwf) const; // _total Unknown if == 0
  double restoreFrom(RWFile& rwf);
  const char* getName() { return _name.data(); }
  unsigned int entries() const { return _apid.length(); }
  int indexOfApid(int apid);
  void validConnect(int v) { v ? setBit(0) : clearBit(0); }
  RWBoolean validConnect() const { return testBit(0); } 
};

inline 
PktStats::PktStats(): RWTime(), RWBitVec(1, 0),
                      _total(0.0), _name("Unknown") {}
 
inline 
PktStats::PktStats(const char* nm, int maxId): RWTime(), RWBitVec(1, 1),
                                               _total(0.0), _name(nm) {
  _apid.resize(maxId+1);
  _apid = IntVec(maxId+1, 0, 1); // 0->maxId
  _count.resize(maxId+1); // sets to 0.0
}

inline int PktStats::indexOfApid(int apid) {
  int retVal = _apid.length();
  while( --retVal > 0 ) // returns -1 if not found
    if( _apid[retVal] == apid ) break;

  return retVal;
}

inline void PktStats::saveOn(RWFile& rwf) const {
#ifdef DEBUG
  cerr << "PktStats> _total: " << _total 
       << " number of entries: " << entries() << " _name: " << _name << endl;
#endif
  rwf << *((RWTime*)this);
  rwf << *((RWBitVec*)this);
  rwf.Write(_total);
  rwf << _name; 
  _apid.saveOn(rwf);
  _count.saveOn(rwf);
  rwf.Flush();
}

inline double PktStats::restoreFrom(RWFile& rwf) {
  rwf >> *((RWTime*)this);
  rwf >> *((RWBitVec*)this);
  rwf.Read(_total);
  rwf.Flush();
#ifdef DEBUG
  cerr << "PktStats> _total: " << _total; 
#endif
  rwf >> _name;
  rwf.Flush();
#ifdef DEBUG
  cerr << " number of entries: " << entries();
#endif
  _apid.restoreFrom(rwf);
  rwf.Flush();
#ifdef DEBUG
  cerr << " _name: " << _name << endl;
#endif
  _count.restoreFrom(rwf);
  rwf.Flush();

  return _total;
}

#if defined(__RTSERVER_C__) || defined(__SERVERSTATS_C__)
unsigned int hashFunPktStats(const RWCollectableInt& key) {
  return key.hash();
}
#else
extern unsigned int hashFunPktStats(const RWCollectableInt& key) {
  return key.hash();
}
#endif

class PktStatsTable {
  RWTPtrHashDictionary<RWCollectableInt, PktStats> *_table;
  RWTPtrHashDictionaryIterator<RWCollectableInt, PktStats> *_iter;
public:
  PktStatsTable() {
    _table = new RWTPtrHashDictionary<RWCollectableInt, PktStats> (hashFunPktStats);
    _iter = new RWTPtrHashDictionaryIterator<RWCollectableInt, PktStats>(*_table);
  }
  void insertKeyAndValue(int fd, PktStats* stat) { 
    _table->insertKeyAndValue(new RWCollectableInt(fd), stat);
  }
  RWCollectableInt* findKeyAndValue(int fd, PktStats*& stats) {
    RWCollectableInt k(fd);
    RWCollectableInt* key = _table->find(&k);
    return _table->findKeyAndValue(key, stats);
  }
  PktStats* first() {
    _iter->reset();
    RWCollectableInt* k = (*_iter)();
    return _iter->value();
  }
  PktStats* next() {
    RWCollectableInt* k = (*_iter)();
    return _iter->value();
  }
  size_t entries() { return _table->entries(); }
  void saveOn(RWFile& rwf);
  void restoreFrom(RWFile &rwf);
};

inline void PktStatsTable::saveOn(RWFile& rwf) {
  RWCollectableInt* key;
  PktStats* stats;
  size_t cnt = entries();
#ifdef DEBUG
  cerr << "PktStatsTable> number of entries to saveOn: " << cnt << endl;
#endif
  rwf.Write(cnt);
  rwf.Flush();
  for( _iter->reset(), key = (*_iter)(); cnt > 0; key = (*_iter)(), cnt-- ) {
    rwf << (RWInteger) *key;
    rwf.Flush();
    stats = _iter->value();
    stats->saveOn(rwf);
  }
}

inline void PktStatsTable::restoreFrom(RWFile &rwf) {
  size_t cnt = 0;
  rwf.Read(cnt);
  rwf.Flush();
  while( cnt-- > 0 ) {
    RWInteger fd;
    rwf >> fd;
    rwf.Flush();
    RWCollectableInt* key = new RWCollectableInt(fd);
    PktStats* stats = new PktStats();
    stats->restoreFrom(rwf);
    _table->insertKeyAndValue(key, stats);
  }
}

class PktStatsFile {
  RWFile* _rwf;
  long _numPairs;
public:
  PktStatsFile() { _rwf = NULL; _numPairs = 0; }
  ~PktStatsFile() { if( _rwf != NULL ) delete _rwf; }
  long open(const char* filenm);
  void close() { delete _rwf; }
  const char* getName() { return _rwf->GetName(); }
  long numPairs() { return _numPairs; }
  void savePair(PktStats& ingested, PktStatsTable& outputTable);
  void restorePair(PktStats& ingested, PktStatsTable& outputTable);
};

inline long PktStatsFile::open(const char* filenm) {
  long retVal = -1; // succesful return value >= 0, current numPairs in file
  _rwf = new RWFile(filenm, "r+");
  if( _rwf->isValid() ) { // existing file, fetch numPairs:
    _rwf->Read(_numPairs);
  }
  else { // perhaps none exists, try once more:
    delete _rwf;
   _rwf = new RWFile(filenm, "w+"); // create and append
  }
  if( _rwf->isValid() ) 
    retVal = _numPairs;

  return retVal;
}

inline void PktStatsFile::savePair(PktStats& ingested, PktStatsTable& outputTable) {
  if( _rwf != NULL ) { 
    _numPairs++;
    _rwf->SeekToBegin();
    _rwf->Write(_numPairs);
    _rwf->Flush();
    _rwf->SeekToEnd();
    ingested.saveOn(*_rwf);
    outputTable.saveOn(*_rwf);
  }
}

inline void PktStatsFile::restorePair(PktStats& ingested, PktStatsTable& outputTable) {
    if( _rwf != NULL ) {
      ingested.restoreFrom(*_rwf);
      outputTable.restoreFrom(*_rwf);
    }
}

#endif // __RTSERVERSTATS_H__







@


4.13
log
@PktStatsTable::saveOn was neglecting to iterate key*!
@
text
@d4 3
a6 2
// RCS: $Id: RtServerStats.h,v 4.12 1995/03/16 19:43:13 dhon Exp dhon $

d14 1
a14 1
struct PktStats {
d20 1
a20 1
  PktStats(const char* nm, int maxIds);
d26 2
d30 3
a32 1
inline PktStats::PktStats(): _total(0.0), _name("Unknown") {}
d34 6
a39 6
inline PktStats::PktStats(const char* nm, int maxIds) {
  _total = 0.0;
  _name = nm;
  _apid.resize(maxIds+1);
  _apid = IntVec(maxIds+1, 0, 1); // 0->maxId
  _count.resize(maxIds+1); // and set = 0.0
d55 2
d65 2
@


4.12
log
@additional debugs, lots of flushes everywhere, etc.
@
text
@d4 1
a4 1
// RCS: $Id: RtServerStats.h,v 4.11 1995/03/16 15:37:26 dhon Exp dhon $
d128 1
a128 1
  for( _iter->reset(), key = (*_iter)(); cnt > 0; cnt-- ) {
@


4.11
log
@added debug
@
text
@d4 1
a4 1
// RCS: $Id: RtServerStats.h,v 4.10 1995/03/14 21:27:21 dhon Exp dhon $
d18 1
a18 1
  PktStats() {}
d23 1
a23 1
  int entries() { return _apid.length(); }
d26 2
d47 2
a48 1
  cerr << "PktStats> _total: " << _total << " _name: " << _name << endl;
d54 1
d59 4
d64 4
d69 4
d74 2
d122 1
d124 1
a124 1
  cerr << "PktStatsTable> number of entries to saveOn: " << entries() << endl;
d126 3
a128 2
  int cnt = entries();
  for( _iter->reset(), key = (*_iter)(); cnt > 0; --cnt ) {
d130 1
d137 4
a140 1
  while( !rwf.Eof() ) {
d143 1
d186 1
a189 1
    _rwf->Flush();
@


4.10
log
@indexOfApid method
@
text
@d4 1
a4 1
// RCS: $Id: RtServerStats.h,v 4.9 1995/03/14 20:26:47 dhon Exp dhon $
d20 1
a20 1
  void saveOn(RWFile& rwf) const; // total Unknown if == 0
d23 1
d43 5
a47 1
inline void PktStats::saveOn(RWFile& rwf) const { 
a48 1
  rwf.Write(_total);
d54 1
a55 1
  rwf.Read(_total);
d104 5
a108 1
  while( key = (*_iter)() ) {
@


4.9
log
@*** empty log message ***
@
text
@d4 1
a4 1
// RCS: $Id: RtServerStats.h,v 4.8 1995/03/14 19:12:53 dhon Exp dhon $
d23 1
d34 8
d133 1
a133 1
  long retVal = -1; // succesful return value >= 0, indicating current numPairs in file
@


4.8
log
@added first and next methods to table class
@
text
@d4 1
a4 1
// RCS: $Id: RtServerStats.h,v 4.7 1995/03/13 19:29:50 dhon Exp dhon $
d83 1
a88 1
  RWTPtrHashDictionaryIterator<RWCollectableInt, PktStats> iterate(*_table);
d91 1
a91 1
  while( key = iterate() ) {
d93 1
a93 1
    stats = iterate.value();
d126 1
a126 1
  if( _rwf->isValid() ) { // existing file, fetch numPairs, then set position to eof:
a127 1
    _rwf->SeekToEnd(); // append
d131 1
a131 1
   _rwf = new RWFile(filenm, "a+"); // create and append
d142 3
@


4.7
log
@*** empty log message ***
@
text
@d4 1
a4 1
// RCS: $Id: RtServerStats.h,v 4.6 1995/03/13 19:24:57 dhon Exp dhon $
d48 4
a51 2
#ifdef __RTSERVER_C__
unsigned int hashFunPktStats(const RWCollectableInt& key) { return key.hash(); }
d53 3
a55 1
extern unsigned int hashFunPktStats(const RWCollectableInt& key) { return key.hash(); }
d60 1
d64 1
d69 3
a71 1
  RWCollectableInt* findKeyAndValue(RWCollectableInt* key, PktStats*& stats) {
d73 9
@


4.6
log
@*** empty log message ***
@
text
@d4 1
a4 1
// RCS: $Id: RtServerStats.h,v 4.5 1995/03/13 19:24:11 dhon Exp dhon $
d20 1
a20 1
  void saveOn(RWFile& rwf) const; // total Unknown of == 0
d29 1
a29 1
  _apid = IntVec(maxIds+1, 0, 1); // 0-masId
@


4.5
log
@*** empty log message ***
@
text
@d4 1
a4 1
// RCS: $Id: RtServerStats.h,v 4.4 1995/03/13 18:41:44 dhon Exp dhon $
d29 1
a29 1
  _apid = IntVec(maxIds, 0, 1); // 0-masId
@


4.4
log
@eliminated default for maxIds (config.h)
@
text
@d4 1
a4 1
// RCS: $Id: RtServerStats.h,v 4.3 1995/03/12 20:08:08 dhon Exp dhon $
d29 2
a30 3
  _apid = IntVec(0, maxIds, 1);
  _count.resize(maxIds+1); 
  _count = 0;
@


4.3
log
@*** empty log message ***
@
text
@d4 1
a4 1
// RCS: $Id: RtServerStats.h,v 4.2 1995/03/12 19:26:34 dhon Exp dhon $
a5 2
#include "config.h" // MaxApID

d19 1
a19 1
  PktStats(const char* nm, int maxIds = MaxApID);
d21 2
a22 1
  double restoreFrom(RWFile& rwf); 
@


4.2
log
@first four bytes of file is a long == numPairs in file
@
text
@d4 1
a4 1
// RCS: $Id$
d16 1
d22 2
a23 2
  void saveOn(RWFile& rwf) const; 
  void restoreFrom(RWFile& rwf); 
d26 2
a27 1
inline PktStats::PktStats(const char* nm, int maxIds) { 
d36 2
a37 1
  rwf << _name;
d42 1
a42 1
inline void PktStats::restoreFrom(RWFile& rwf) {
d44 1
d47 1
@


4.1
log
@*** empty log message ***
@
text
@d91 1
d93 1
a93 1
  PktStatsFile() { _rwf = NULL; }
d95 1
a95 4
  RWBoolean open(const char* filenm, char* mode = "ab+") {
    _rwf = new RWFile(filenm, mode);
    return _rwf->isValid();
  }
d98 1
d102 18
a119 1
  
d122 1
@

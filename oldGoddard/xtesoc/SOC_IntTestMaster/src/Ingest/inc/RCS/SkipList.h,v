head	4.1;
access
	rbarnett
	soccm;
symbols
	Build4_5_3:4.1
	Build4:2.8
	Build3_3:2.8
	Build3_2:2.8
	Build3_1:2.8
	Build3:2.8
	Current:2.8
	Build2:2.5
	Build1:1.11;
locks;
comment	@ * @;


4.1
date	95.10.23.18.49.33;	author rbarnett;	state Exp;
branches;
next	2.8;

2.8
date	94.04.06.15.17.55;	author lijewski;	state Exp;
branches;
next	2.7;

2.7
date	94.02.27.19.41.14;	author lijewski;	state Exp;
branches;
next	2.6;

2.6
date	94.02.25.21.36.31;	author lijewski;	state Exp;
branches;
next	2.5;

2.5
date	93.11.17.21.13.53;	author lijewski;	state Release;
branches;
next	2.4;

2.4
date	93.11.15.21.20.21;	author lijewski;	state Exp;
branches;
next	2.3;

2.3
date	93.11.10.21.23.35;	author lijewski;	state Exp;
branches;
next	2.2;

2.2
date	93.09.02.12.20.36;	author lijewski;	state Exp;
branches;
next	2.1;

2.1
date	93.08.19.19.47.07;	author lijewski;	state Exp;
branches;
next	2.0;

2.0
date	93.08.10.19.56.20;	author lijewski;	state Exp;
branches;
next	1.13;

1.13
date	93.08.09.13.59.48;	author lijewski;	state Exp;
branches;
next	1.12;

1.12
date	93.06.29.19.54.26;	author lijewski;	state Exp;
branches;
next	1.11;

1.11
date	93.06.18.19.07.18;	author lijewski;	state Exp;
branches;
next	1.10;

1.10
date	93.06.15.13.20.57;	author lijewski;	state Exp;
branches;
next	1.9;

1.9
date	93.06.15.12.40.45;	author lijewski;	state Exp;
branches;
next	1.8;

1.8
date	93.06.08.19.33.28;	author lijewski;	state Exp;
branches;
next	1.7;

1.7
date	93.06.04.20.40.40;	author lijewski;	state Exp;
branches;
next	1.6;

1.6
date	93.06.02.20.20.35;	author lijewski;	state Exp;
branches;
next	1.5;

1.5
date	93.05.21.15.07.37;	author lijewski;	state Exp;
branches;
next	1.4;

1.4
date	93.05.21.13.09.20;	author lijewski;	state Exp;
branches;
next	1.3;

1.3
date	93.05.20.21.49.04;	author lijewski;	state Exp;
branches;
next	1.2;

1.2
date	93.05.18.19.05.03;	author lijewski;	state Exp;
branches;
next	1.1;

1.1
date	93.05.18.17.53.42;	author lijewski;	state Exp;
branches;
next	;


desc
@Creation
@


4.1
log
@New documentation
@
text
@// ===========================================================================
// File Name   : SkipList.h
// Subsystem   : Data Ingest
// Programmer  : Randall D. Barnette, Hughes STX
// Description : Declaration of Skip List class
//
// .NAME    SkipList - Templatized skiplist of (KEY,VAL) pairs
// .LIBRARY template
// .HEADER  Data Ingest
// .INCLUDE SkipList.h
// .FILE    SkipList.c
//
// RCS: $Id: SkipList.h,v 2.9 1995/10/20 19:32:10 soccm Exp $
//
// ===========================================================================

// .SECTION Author
// Mike Lijewski, NASA/GSFC, lijewski@@rosserv.gsfc.nasa.gov

// .SECTION Description:
// Skip Lists are a probabilistic alternative to balanced trees, as
// described in the June 1990 issue of CACM and were invented by 
// William Pugh in 1987.
//
// The KEY type must be a struct type with the following operators defined:
//
//   operator==()            -- equality operator
//   operator!=()            -- inequality operator
//   operator<()             -- less than operator
//   operator=(const KEY&)   -- assignment operator
//   KEY(const KEY&)         -- copy constructor
//   KEY()                   -- default constructor
//   friend operator<<(ostream&, const KEY&) -- output operator
//
// as well as a static member function:
//
//   KEY::maxKey(void)
//
// that returns the maximum value of the KEY type; i.e. for any key of type
// KEY `key < maxKey()' is true for all values of type key except the value
// maxKey() itself.  It should at the least have a default constuctor.
//
// The VAL type must have well-defined copy semantics, well-defined
// assignment semantics, and a default constructor.  That is to say, it
// must have defined (implicitly or explicitly) the following member
// functions:
//
//   VAL()                 -- default constructor
//   VAL(const VAL&)       -- copy constructor
//   operator=(const VAL&) -- assignment operator
//

#ifndef SKIPLIST_H
#define SKIPLIST_H

#include <iostream.h>
#include <math.h>
#include <stdlib.h>

//
// BitsInRandom is defined to be the number of bits returned by a call
// to random(). For most all machines with 32-bit integers, this is 31
// bits as currently set.  This, aside from the typedefs for KEY and VAL,
// is the only thing that should need to change when moving this to
// another machine.  In the best of all possible worlds this could be
// parameterized using the ANSI C RAND_MAX macro.  To bad my Sun doesn't
// define such a thing.
//
const int BitsInRandom = 31;

//
// This structure represents a node in the SkipList.  It must
// remain a C structure, with no member functions, so that we can
// take advantage of the C variable size array hack.
//

template<class KEY, class VAL> struct Node {
	KEY key;
	VAL value;
	Node* forward[1];  // Variable sized array of forward pointers.
};

//
// Class representing a SkipList.
//

template<class KEY, class VAL> class SkipList {
public:

	SkipList (unsigned int maxNumberOfLevels);

	~SkipList (void);

	int insert (const KEY& key, const VAL& value);

	int remove (const KEY& key);

	const VAL* search (const KEY& key) const;

	const VAL* nearest (const KEY& key) const;

	const VAL* largest (void) const;

	const VAL* smallest (void) const
	{
		return entries() == 0 ? 0 : &(header->forward[0]->value);
	}

	unsigned long entries (void) const
	{
		return nelements;
	}

	int isEmpty (void) const
	{
		return nelements == 0;
	}
	
	void stats (void)  const;

	static Node<KEY,VAL>* NIL;

protected:

	static int count;

	void init (void);

	Node<KEY,VAL>* node (const KEY& key) const;

	static Node<KEY,VAL>* newNodeOfLevel (int l)
	{
		char* p = new char[sizeof(Node<KEY,VAL>)+l*sizeof(Node<KEY,VAL>**)];
		Node<KEY,VAL> node;
		memcpy(p, (char*) &node, sizeof(Node<KEY,VAL>));
		return (Node<KEY,VAL>*) p;
	}

	int randomLevel (void);

	unsigned int MaxNumberOfLevels;

	int level;

	unsigned long nelements;

	long int randomsLeft;
	long int randomBits;

	Node<KEY,VAL>* header;
};

#endif /*SKIPLIST_H*/
@


2.8
log
@removed struct keyword in one place to allow Sun CC to debug properly
@
text
@d1 11
d13 1
a13 8
// $Id: SkipList.h,v 2.7 1994/02/27 19:41:14 lijewski Exp lijewski $
//
// .NAME SkipList.h - templatized skiplist of (KEY,VAL) pairs
// .LIBRARY subclass
// .HEADER
// .INCLUDE SkipList.h
// .FILE SkipList.c
// .FILE SkipList.h
d15 2
d19 1
a19 1
//
a55 1

a59 1

a70 1

d77 4
a80 5
template<class KEY, class VAL> struct Node
{
    KEY key;
    VAL value;
    Node* forward[1];  // Variable sized array of forward pointers.
a82 1

d87 2
a88 3
template<class KEY, class VAL> class SkipList
{
  public:
d90 1
a90 1
    SkipList (unsigned int maxNumberOfLevels);
d92 1
a92 1
    ~SkipList (void);
d94 1
a94 1
    int insert (const KEY& key, const VAL& value);
d96 1
a96 1
    int remove (const KEY& key);
d98 1
a98 1
    const VAL* search (const KEY& key) const;
d100 1
a100 1
    const VAL* nearest (const KEY& key) const;
d102 1
a102 1
    const VAL* largest (void) const;
d104 4
a107 4
    const VAL* smallest (void) const
    {
        return entries() == 0 ? 0 : &(header->forward[0]->value);
    }
d109 4
a112 4
    unsigned long entries (void) const
    {
        return nelements;
    }
d114 6
a119 6
    int isEmpty (void) const
    {
        return nelements == 0;
    }
    
    void stats (void)  const;
d121 1
a121 1
    static Node<KEY,VAL>* NIL;
d123 1
a123 1
  protected:
d125 1
a125 1
    static int count;
d127 1
a127 1
    void init (void);
d129 1
a129 1
    Node<KEY,VAL>* node (const KEY& key) const;
d131 7
a137 7
    static Node<KEY,VAL>* newNodeOfLevel (int l)
    {
        char* p = new char[sizeof(Node<KEY,VAL>)+l*sizeof(Node<KEY,VAL>**)];
        Node<KEY,VAL> node;
        memcpy(p, (char*) &node, sizeof(Node<KEY,VAL>));
        return (Node<KEY,VAL>*) p;
    }
d139 1
a139 1
    int randomLevel (void);
d141 1
a141 1
    unsigned int MaxNumberOfLevels;
d143 1
a143 1
    int level;
d145 1
a145 1
    unsigned long nelements;
d147 2
a148 2
    long int randomsLeft;
    long int randomBits;
d150 1
a150 1
    Node<KEY,VAL>* header;
a151 1

@


2.7
log
@beautified genman output
@
text
@d2 1
a2 1
// $Id: SkipList.h,v 2.6 1994/02/25 21:36:31 lijewski Exp lijewski $
d78 1
a78 1
    struct Node* forward[1];  // Variable sized array of forward pointers.
@


2.6
log
@genman'd
@
text
@d2 1
a2 1
// $Id: SkipList.h,v 2.5 1993/11/17 21:13:53 lijewski Release lijewski $
d109 4
a112 1
    unsigned long entries (void) const { return nelements; }
d114 4
a117 1
    int isEmpty (void) const           { return nelements == 0; }
d119 1
a119 1
    void stats (void) const;
d133 1
a133 1
        char* p = new char[sizeof(Node<KEY,VAL>) + l * sizeof(Node<KEY,VAL>**)];
@


2.5
log
@split out declaration and definition
@
text
@d2 1
a2 1
// $Id: SkipList.h,v 2.4 1993/11/15 21:20:21 lijewski Exp lijewski $
d8 1
a18 5
// This implementation assumes we have the BSD random() routine in the C
// standard library, which has the following prototype:
//
//   long random (void)
//
a89 6
    //
    // `maxNumberOfLevels' should be set such that 2**maxNumberOfLevels
    // is roughly the maximum number of elements which will be in a
    // list.  It will be silently coerced to be in the range
    // [0, BitsInRandom].
    //
a93 11
    //
    // Inserts the binding (key,value) into the List. Returns 1
    // if key was newly inserted into the List and 0 if key already
    // existed.  It the key already existed in the list, we overwrite
    // the old value.
    //
    // This routine has been implemented so as to use the
    // dirty hack described in the CACM paper: if a random level is
    // generated that is more than the current maximum level, the
    // current maximum level plus one is used instead.
    //
a95 6
    //
    // Removes any binding of key from the List.  Returns true
    // if such a binding was found and removed, else false.
    // If you need to get a handle on the VAL, you need to
    // use search() first.
    //
a97 5
    //
    // Searches for the key in List and returns the value if found,
    // else NULL.  Note that this value is valid only until the next
    // non-const operation on the SkipList.
    //
a99 9
    //
    // Returns a constant pointer to the value closest to the
    // passed KEY, or NULL if the list is empty.  In fact, this
    // really returns the value associated with the largest key
    // less than the passed key.  If the key isn't in the range
    // of the cache we return either smallest() or largest(),
    // whichever is appropriate.  The address returned is only
    // valid until the next non-const operation on the SkipList.
    //
a101 6
    //
    // Returns a constant pointer to the value associated with the
    // largest key in the list or NULL if the list is empty.  The
    // address returned is only valid until the next non-const
    // operation on the SkipList.
    //
a103 6
    //
    // Returns a constant pointer to the value associated with the
    // smallest key in the list or NULL if the list is empty.  The
    // address returned is only valid until the next non-const
    // operation on the SkipList.
    //
a114 8
    //
    // Declartion of the NIL Node pointer.  The key for this Node
    // must always compare larger than that for any of the possible
    // elements in the SkipList.  This gets set up during
    // initialization.  There only needs to be one of these for each
    // type of skiplist; i.e. each skiplist with different key and/or
    // value types.
    //
a118 6
    //
    // There is some initialization which must get done
    // once for the class as a whole.  We use this counter
    // to do that.  Specifically, the NIL node must get defined
    // only once and srandom() must only be called once.
    //
a120 6
    //
    // This does the static initialization, once for each type of
    // SkipList.  It is called by the constructor; internally it must
    // use `count' so that it only does the per-class initialization
    // once.
    //
a122 4
    //
    // Searches for the key in List and returns the Node the key is
    // in if found.  Else returns NULL.
    //
a124 3
    //
    // Allocate a new Node of level `l'.
    //
a132 5
    // 
    // Returns a random level.  This routine has been hard-coded
    // to generate random levels using p = 0.25. It can be easily
    // changed. It is used by insert().
    //
a134 6
    //
    // Levels start at zero and go up to (MaxNumberOfLevels-1).
    // `MaxNumberOfLevels' should be set such that 2**MaxNumberOfLevels
    // is roughly the maximum number of elements which will be in a
    // list.  It is set in the constructor.
    //
a136 4
    //
    // Maximum level of the List; one more than the number of levels
    // in List.
    //
a138 3
    //
    // Number of entries in the list.
    //
a140 3
    //
    // These keep track of some randomness criteria.
    //
a143 3
    //
    // Pointer to header of the SkipList.
    //
@


2.4
log
@removed a few tabs
@
text
@d2 1
a2 1
// $Id: SkipList.h,v 2.3 1993/11/10 21:23:35 lijewski Exp lijewski $
a250 335

//
// Definition of the static counter which we use to make sure that
// the NIL Node of each type of skiplist gets defined, and only once.
//

template<class KEY, class VAL> int SkipList<KEY,VAL>::count = 0;


//
// Definition of the NIL Node.  The key for this Node must always
// compare larger than that for any of the possible elements in
// the SkipList.
//

template<class KEY, class VAL> Node<KEY,VAL>* SkipList<KEY,VAL>::NIL = 0;


//
// Defines NIL and initializes the random bit source.
// This should only be called once for each type of SkipList,
// not once for each SkipList object.
//

template<class KEY, class VAL> void SkipList<KEY,VAL>::init (void)
{
    if (count++ == 0)
    {
        //
        // Initialize random().
        //
        srandom(12345);
        //
        // Make NIL point to something.
        //
        NIL = newNodeOfLevel(0);
        //
        // Set the largest value of KEY.  This assumes that
        // KEY::maxKey() exists and returns such a value.  See
        // the appended test program to see one way to do this.
        //
        NIL->key = KEY::maxKey();
    }
}


//
// Constuct a new SkipList.
//

template<class KEY, class VAL> SkipList<KEY,VAL>::SkipList (unsigned int maxNumberOfLevels)
{
    init();
    MaxNumberOfLevels = maxNumberOfLevels;
    //
    // Force into interval [0, BitsInRandom].
    //
    if (maxNumberOfLevels > BitsInRandom)
        MaxNumberOfLevels = BitsInRandom;
    level       = 0;
    randomBits  = random();
    randomsLeft = BitsInRandom / 2;
    header      = newNodeOfLevel(MaxNumberOfLevels);
    nelements   = 0;
    for (int i = 0; i < MaxNumberOfLevels; i++)
        header->forward[i] = NIL;
}


//
// Our destructor.
//

template<class KEY, class VAL> SkipList<KEY,VAL>::~SkipList (void) 
{
    Node<KEY,VAL> *p = header, *q;

    do
    {
        q = p->forward[0];
        delete [] p;
        p = q;
    }
    while (p != NIL);

    if (--count == 0)
        delete [] NIL;
}


//
// Returns a random level.  This routine has been hard-coded to generate
// random levels using p = 0.25. It can be easily changed.
//

template<class KEY, class VAL> int SkipList<KEY,VAL>::randomLevel (void)
{
    int nlevel = 0;
    long b;

    do
    {
        b = randomBits & 3;
        if (!b)
            nlevel++;
        randomBits >>= 2;
        if (--randomsLeft == 0)
        {
            randomBits = random();
            randomsLeft = BitsInRandom / 2;
        }
    }
    while (!b);

    return nlevel > MaxNumberOfLevels-1 ? MaxNumberOfLevels-1 : nlevel;
}


//
// Inserts the binding (key,value) into the List. Returns 1 if key
// was newly inserted into the List and 0 if key already existed.
//
// This routine has been implemented so as to use the
// dirty hack described in the CACM paper: if a random level is
// generated that is more than the current maximum level, the
// current maximum level plus one is used instead.
//

template<class KEY, class VAL> int SkipList<KEY,VAL>::insert (const KEY& key, const VAL& value)
{
    int k = level, rc = 1;
    Node<KEY,VAL>* update[BitsInRandom], *p = header, *q;

    do
    {
        while (q = p->forward[k], q->key < key)
            p = q;
        update[k] = p;
    }
    while (--k >= 0);

    if (q->key == key)
        rc = 0;
    else
    {
        nelements += 1;
        k = randomLevel();

        if (k > level)
        {
            k = ++level;
            update[k] = header;
        }

        q = newNodeOfLevel(k);
        q->key = key;
        q->value = value;

        do
        {
            p = update[k];
            q->forward[k] = p->forward[k];
            p->forward[k] = q;
        }
        while (--k >= 0);
    }

    return rc;
}


//
// Removes any binding of key from the List.  Returns true if such a
// binding was found and removed, else false.  If you need to get a
// handle on the VAL, you need to use search() first.
//

template<class KEY, class VAL> int SkipList<KEY,VAL>::remove (const KEY& key) 
{
  int k = level, m = k, rc = 0;
  Node<KEY,VAL>* update[BitsInRandom], *p = header, *q;

  do
  {
      while (q = p->forward[k], q->key < key)
          p = q;
      update[k] = p;
  }
  while (--k >= 0);

  if (q->key == key)
  {
      rc = 1;

      for (k = 0; k <= m && (p = update[k])->forward[k] == q; k++) 
          p->forward[k] = q->forward[k];

      delete [] q;

      while (header->forward[m] == NIL && m > 0)
          m--;

      level = m;

      nelements -= 1;
  }

  return rc;
}


//
// Searches for the key in List and returns a constant pointer to
// the value if found, else NULL.  Note that this pointer is
// valid only as long as the (key, value) pair in the SkipList
// sticks around.
//

template<class KEY, class VAL> const VAL* SkipList<KEY,VAL>::search (const KEY& key) const
{
  int k  = level;
  Node<KEY,VAL> *p = header, *q;
  VAL* val = 0;

  do
  {
      while (q = p->forward[k], q->key < key)
          p = q;
  }
  while (--k >= 0);

  if (q->key == key)
      val = &(q->value);

  return val;
}


//
// Searches for the key in List and returns the Node the key
// is in, if found.  Else returns NULL.
//

template<class KEY, class VAL> Node<KEY,VAL>* SkipList<KEY,VAL>::node (const KEY& key) const
{
  int k  = level;
  Node<KEY,VAL> *p = header, *q;

  do
  {
      while (q = p->forward[k], q->key < key)
          p = q;
  }
  while (--k >= 0);

  return  q->key == key ? q : 0;
}


//
// Returns a constant pointer to the value closest to the passed KEY, or NULL
// if the list is empty.  In fact, this really returns the value associated
// with the largest key less than the passed key.  If the key isn't in the
// range of the cache we return either smallest() or largest(), whichever is
// appropriate.  The address returned is only valid until the next non-const
// operation on the CacheLine.
//

template<class KEY, class VAL> const VAL* SkipList<KEY,VAL>::nearest (const KEY
& key) const
{
    const VAL* val = search(key);

    if (val == 0 && !isEmpty())
    {
        //
        // Just do a simple linear search.  This shouldn't be called
        // that often, relative to how often insert() gets called.
        //
        Node<KEY,VAL>* p = header->forward[0], *q = p;
        while (p != NIL && p->key < key)
        {
            q = p;
            p = p->forward[0];
        }
        val = &(q->value);
    }

    return val;
}


//
// Returns a constant pointer to the value associated with the largest key in
// the list or NULL if the list is empty.  The address returned is only valid
// until the next insert() operation.
//

template<class KEY, class VAL> const VAL* SkipList<KEY,VAL>::largest (void) const
{
    VAL* val = 0;

    if (entries())
    {
        int k   = level;
        Node<KEY,VAL> *p = header, *q;
        do
        {
            while (q = p->forward[k], q->key != SkipList<KEY,VAL>::NIL->key)
                p = q;
        }
        while (--k >= 0);

        val = &(p->value);
    }

    return val;
}


template<class KEY, class VAL> void SkipList<KEY, VAL>::stats (void) const
{
    if (isEmpty())
        cout << "The SkipList is empty.\n";
    else
    {
        cout << "The SkipList contains " << entries() << " elements.\n";
        //
        // Print out the keys; they had better be in order.
        //
        for (Node<KEY,VAL>* p = header->forward[0]; p != NIL; p = p->forward[0])
            cout << p->key << "\n";
    }
}

@


2.3
log
@no longer overwrite old value with new when duplicates are inserted
@
text
@d2 1
a2 1
// $Id: SkipList.h,v 2.2 1993/09/02 12:20:36 lijewski Exp lijewski $
d386 1
a386 1
	while (q = p->forward[k], q->key < key)
d388 1
a388 1
	update[k] = p;
d393 1
a393 1
	rc = 0;
d450 1
a450 1
      while (header->forward[m] == NIL && m > 0 )
d476 1
d479 1
@


2.2
log
@fixed problem with newNodeOfLevel(); thanks to Nick Chen for finding it.
@
text
@d2 1
a2 1
// $Id: SkipList.h,v 2.1 1993/08/19 19:47:07 lijewski Exp lijewski $
a371 1
// It the key already existed in the list, we overwrite the old value.
a392 2
    {
        q->value = value;
a393 1
    }
@


2.1
log
@added <math.h>
@
text
@d2 1
a2 1
// $Id: SkipList.h,v 2.0 1993/08/10 19:56:20 lijewski Exp lijewski $
d43 1
a43 1
// must have defined (implicitely or explicitly) the following member
d207 4
a210 2
        return (Node<KEY,VAL>*)new char[sizeof(Node<KEY,VAL>) +
                                        (l) * sizeof(Node<KEY,VAL>**)];
@


2.0
log
@NewBuild
@
text
@d2 1
a2 1
// $Id: SkipList.h,v 1.13 1993/08/09 13:59:48 lijewski Exp lijewski $
d56 1
@


1.13
log
@templatized
@
text
@d2 1
a2 1
// $Id: SkipList.h,v 1.12 1993/06/29 19:54:26 lijewski Exp lijewski $
@


1.12
log
@now more modular
@
text
@d2 1
a2 1
// $Id: SkipList.h,v 1.11 1993/06/18 19:07:18 lijewski Exp $
d4 1
a4 1
// .NAME SkipList.h - implementation of a skiplist
d25 7
a31 3
//   operator==()
//   operator!=()
//   operator<()
d41 8
a48 3
// This should really be implemented using templates -- too bad Cfront
// isn't up to the task.  In particular, Cfront 3.0.1 can't templatize a
// class containing nested structures.
a57 3
#ifndef TEST
#include "PktTlm.h"
#endif
a59 6
// Define a KEY type.
//
#include "PktKey.h"


//
d70 1
d72 3
a74 2
// When testing we have a SkipList with PktKey keys and char* values.
// Otherwise, we have PktKey keys and PktTlm* values.
d77 6
a82 1
typedef PktKey KEY;
a83 5
#ifdef TEST
typedef char*  VAL;
#else
typedef PktTlm*  VAL;
#endif
a84 1

d89 1
a89 1
class SkipList
d94 1
a94 15
    // This structure represents a node in the SkipList.  It must
    // remain a C structure, with no member functions, so that we can
    // take advantage of the C variable size array hack.  VAL can be
    // a builtin type, a pointer to such, or a user-defined type.
    // Both KEY and VAL must have default constructors.
    //
    struct Node
    {
        KEY key;
        VAL value;
        struct Node* forward[1];  // Variable sized array of forward pointers.
    };

    //
    // maxNumberOfLevels should be set such that 2**maxNumberOfLevels
d126 2
a127 2
    // else NULL.  Note that this value is valid only as long as the
    // (key, value) pair in the SkipList sticks around.
d129 1
a129 1
    VAL search (const KEY& key) const;
d132 7
a138 2
    // Searches for the key in List and returns the Node the key is
    // in if found.  Else returns NULL.
d140 1
a140 1
    Node* node (const KEY& key) const;
d142 7
a148 1
    static Node* newNodeOfLevel (int l);
d150 10
a159 1
    unsigned long entries (void) const;
d161 1
a161 1
    int isEmpty (void) const;
d163 2
a164 1
#ifdef TEST
a165 1
#endif
d175 1
a175 1
    static Node* NIL;
d195 15
d245 1
a245 1
    Node* header;
d250 266
a515 1
// Inlines.
d518 24
d543 3
a545 1
// Allocate a new Node of level `l'.
d548 1
a548 1
inline SkipList::Node* SkipList::newNodeOfLevel (int l)
d550 17
a566 2
    return (SkipList::Node*)new char[sizeof(SkipList::Node)+(l)*
                                     sizeof(SkipList::Node**)];
d569 15
a583 1
inline unsigned long SkipList::entries (void) const { return nelements; }
a584 1
inline int SkipList::isEmpty (void) const { return nelements == 0; }
@


1.11
log
@removed C funnyness from Key::operator<()
@
text
@d2 1
a2 1
// $Id: SkipList.h,v 1.10 1993/06/15 13:20:57 lijewski Exp lijewski $
d16 1
a16 9
// William Pugh in 1987.  This implementation assumes we have the BSD
// random() routine in the C standard library, which has the following
// prototype: `long random (void)'.  The type `valueType' must be set
// to the type of value you want to store in the SkipList. `valueType'
// can be any type with well-defined assignment semantics.  The key is
// defined to be a structure of two longs.  By maintaining the second
// long as some constant, the skiplist can be used with values which
// need to be indexed with only one key.  It is further assumed that longs
// are four bytes in size.
d18 23
d49 3
d54 1
a54 6
// BitsInRandom is defined to be the number of bits returned by a call to
// random(). For most all machines with 32-bit integers, this is 31 bits
// as currently set.  Levels start at zero and go up to MaxLevel, which is
// equal to (MaxNumberOfLevels-1).  `MaxNumberOfLevels' should be set such
// that 2**MaxNumberOfLevels is roughly the maximum number of elements which
// will be in a list.
d56 1
a57 3
const int BitsInRandom      = 31;
const int MaxNumberOfLevels = 16;
const int MaxLevel          = (MaxNumberOfLevels - 1);
d59 10
d71 2
a72 3
// The key by which items in the SkipList are indexed.  SLKeys need
// to have operator==(), operator!=() and operator<() defined.  When
// debugging, also need a way to output keys.
d75 1
a75 10
struct SLKey
{
    long int key1;
    long int key2;

    SLKey (long a = 0, long b = 0) { key1 = a; key2 = b; }

    int operator== (const SLKey& rhs) const;
    int operator!= (const SLKey& rhs) const;
    int operator<  (const SLKey& rhs) const;
d78 3
a80 1
    friend ostream& operator<< (ostream& os, const SLKey& key);
a81 48
};


//
// Inlines for SLKey.
//

inline int SLKey::operator== (const SLKey& rhs) const
{
    return key1 == rhs.key1 && key2 == rhs.key2;
}


inline int SLKey::operator!= (const SLKey& rhs) const
{
    return !(operator==(rhs));
}


inline int SLKey::operator< (const SLKey& rhs) const
{
    int rc = 0;

    if (key1 < rhs.key1)
        rc = 1;
    else if (key1 == rhs.key1)
        rc = key2 < rhs.key2 ? 1 : 0;

    return rc;
}


typedef struct NodeStructure *Node;


//
// This structure represents a node in the SkipList.  It must
// remain a C structure, with no member functions, so that we can
// take advantage of the C variable size array hack.  We assume
// that the value points to some object.
//

struct NodeStructure
{
    SLKey key;
    void* value;
    Node forward[1];  // Variable sized array of forward pointers.
};
d92 22
a113 1
    SkipList (void);
d127 9
a135 1
    int insert (const SLKey& key, void* value);
d138 3
a140 2
    // Removes any binding of key from the List. Returns value
    // of the key if the key was defined, else NULL.
d142 1
a142 1
    void* remove (const SLKey& key);
d145 2
a146 2
    // Searches for the key in List and returns the value if
    // found, else NULL.
d148 3
a150 1
    void* search (const SLKey& key) const;
d153 1
d161 6
a166 5
    // Declartion of the NIL Node.  The key for this Node must always
    // compare larger than that for any of the possible elements in
    // the SkipList.  This gets set up during initialization.
    // There only needs to be one of these for each type of skiplist;
    // i.e. each skiplist with different key and/or value types.
d168 1
a168 1
    static Node NIL;
d183 2
a184 1
    // use `count' so that is only does the initialization once.
a187 6
    //
    // Searches for the key in List and returns the Node the key is
    // in if found.  Else returns NULL.
    //
    Node node (const SLKey& key) const;

d196 8
d223 1
a223 1
    Node header;
d235 1
a235 1
inline Node newNodeOfLevel (int l)
d237 2
a238 1
    return (Node)new char[sizeof(struct NodeStructure) + (l) * sizeof(Node*)];
a240 1

a242 1

@


1.10
log
@bit of finetuning of genman output
@
text
@d2 1
a2 1
// $Id: SkipList.h,v 1.9 1993/06/15 12:40:45 lijewski Exp lijewski $
d90 8
a97 1
    return key1 < rhs.key1 ? 1 : (key1 > rhs.key1 ? 0 : (key2 < rhs.key2));
@


1.9
log
@set up for genman
@
text
@d2 1
a2 1
// $Id: SkipList.h,v 1.8 1993/06/08 19:33:28 lijewski Exp lijewski $
d123 11
d135 5
d141 5
d175 3
a177 3
    // This does the static initialization, once for each type of SkipList.
    // It is called by the constructor; internally it must use `count' so
    // that is only does the initialization once.
d182 2
a183 2
    // Searches for the key in List and returns the Node the key is in if
    // found.  Else returns NULL.
d187 4
a190 2
    //
    // Returns a new random level.  Used by insert().
d195 2
a196 1
    // Maximum level of the List; one more than the number of levels in List.
@


1.8
log
@moved NIL to public area
@
text
@d2 1
a2 3
// Skip Lists are a probabilistic alternative to balanced trees, as
// described in the June 1990 issue of CACM and were invented by 
// William Pugh in 1987. 
d4 5
a8 2
// This assumes we have the BSD random() routine in the C standard library,
// which has the following prototype: `long random (void)'.
d10 2
a11 3
// The type `valueType' must be set to the type of value you want to store
// in the SkipList. `valueType' can be any type with well-defined assignment
// semantics.
d13 10
a22 2
// The key is defined to be a structure of two longs.  By maintaining the
// second long as some constant, the skiplist can be used with values which
a25 2
// $Id: SkipList.h,v 1.7 1993/06/04 20:40:40 lijewski Exp lijewski $
//
@


1.7
log
@added SkipList::node()
@
text
@d18 1
a18 1
// $Id: SkipList.h,v 1.6 1993/06/02 20:20:35 lijewski Exp lijewski $
a127 2
  protected:

d137 2
@


1.6
log
@removed all underscores from CPP directives
@
text
@d18 1
a18 1
// $Id: SkipList.h,v 1.5 1993/05/21 15:07:37 lijewski Exp lijewski $
d154 6
@


1.5
log
@fixed problem with uninitialized variable
@
text
@d18 1
a18 1
// $Id: SkipList.h,v 1.4 1993/05/21 13:09:20 lijewski Exp lijewski $
d21 2
a22 2
#ifndef __SKIPLIST_H
#define __SKIPLIST_H
d201 1
a201 1
#endif /*__SKIPLIST_H*/
@


1.4
log
@the key is now a struct of two longs
@
text
@d18 1
a18 1
// $Id: SkipList.h,v 1.3 1993/05/20 21:49:04 lijewski Exp lijewski $
d21 2
d24 1
d44 1
a44 1
// The key by which items in the SkipList are indexed.  Keys need
d49 1
a49 1
struct Key
d54 1
a54 1
    Key (long a = 0, long b = 0) { key1 = a; key2 = b; }
d56 3
a58 3
    int operator== (const Key& rhs) const;
    int operator!= (const Key& rhs) const;
    int operator<  (const Key& rhs) const;
d61 1
a61 1
    friend ostream& operator<< (ostream& os, const Key& key);
d67 1
a67 1
// Inlines for Key.
d70 1
a70 1
inline int Key::operator== (const Key& rhs) const
d76 1
a76 1
inline int Key::operator!= (const Key& rhs) const
d82 1
a82 1
inline int Key::operator< (const Key& rhs) const
d100 1
a100 1
    Key key;
d117 3
a119 3
    int insert (const Key& key, void* value);
    void* remove (const Key& key);
    void* search (const Key& key) const;
d192 1
a192 1
    return (Node)new char[sizeof(struct NodeStructure) + (l) * sizeof(Node *)];
d195 1
d198 1
d200 2
@


1.3
log
@fixed a few bugs I introduced
@
text
@d10 2
a11 2
// in the SkipList.  The key must be a long; in fact a four-byte long.
// `valueType' can be any type with well-defined assignment semantics.
d13 4
a16 1
// $Id: SkipList.h,v 1.2 1993/05/18 19:05:03 lijewski Exp lijewski $
d18 2
d41 3
a43 3
// The type of the key and value.  The keyType must be a `long', as
// some of the random stuff depends on it.  The valueType can be
// any type with well-defined assignment semantics.
d46 4
a49 1
typedef long keyType;
d51 1
d53 32
d97 1
a97 1
    keyType key;
d114 3
a116 3
    int insert (keyType key, void* value);
    void* remove (keyType key);
    void* search (keyType key);
d118 2
a119 2
    unsigned long entries (void);
    int isEmpty (void);
d122 1
a122 1
    void stats (void);
d132 1
a132 1
    // i.e. each skiplist with different key and value types.
d140 1
a140 1
    // only once.
d192 1
a192 1
inline unsigned long SkipList::entries (void) { return nelements; }
d194 1
a194 1
inline int SkipList::isEmpty (void) { return nelements == 0; }
@


1.2
log
@made into generic class
@
text
@d13 1
a13 1
// $Id: SkipList.h,v 1.1 1993/05/18 17:53:42 lijewski Exp lijewski $
d128 2
a129 2
    long randomsLeft;
    long randomBits;
a134 1

d140 4
@


1.1
log
@Initial revision
@
text
@d9 3
a11 1
// $Id$
d13 2
d36 3
a38 3
// The type of the key and value.  The key must be a long, as so
// much of the random stuff depends on it.  The valueType can of
// course be anything.
a41 1
typedef long valueType;
d50 2
a51 1
// take advantage of the C variable size array hack.
d57 1
a57 1
    valueType value;
d73 3
a75 3
    int insert (keyType key, valueType value);
    int remove (keyType key);
    valueType* search (keyType key);
d90 2
a91 2
    // There only needs to be one of these for each type of skiplist; i.e.
    // each skiplist with different key and value types.
@

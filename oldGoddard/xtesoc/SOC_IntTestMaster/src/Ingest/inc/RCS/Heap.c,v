head	4.1;
access
	rbarnett
	soccm;
symbols
	Build4_5_3:4.1
	Build4_3_2:4.1
	Build4:2.5
	Build3_3:2.5
	Build3_2:2.5
	Build3_1:2.5
	Build3:2.5
	Current:2.5;
locks;
comment	@ * @;


4.1
date	95.06.20.20.27.15;	author rbarnett;	state Exp;
branches;
next	2.5;

2.5
date	94.02.27.20.06.09;	author lijewski;	state Exp;
branches;
next	2.4;

2.4
date	94.02.25.19.35.02;	author lijewski;	state Exp;
branches;
next	2.3;

2.3
date	94.02.25.14.41.51;	author lijewski;	state Exp;
branches;
next	2.2;

2.2
date	93.12.26.21.58.36;	author lijewski;	state Exp;
branches;
next	2.1;

2.1
date	93.12.22.14.20.11;	author lijewski;	state Exp;
branches;
next	;


desc
@Creation
@


4.1
log
@added genman documentation
@
text
@// ======================================================================
// File Name   : Heap.c
// Subsystem   : Data Ingest
// Programmer  : Mike Lijewski
// Description : Implementation of a templatized heap class.
//
// The objects stored in the list must have:
//
//  (1) a copy constructor     -- T::T (const T&)
//  (2) an assignment operator -- T::operator= (const T&)
//  (3) an equality operator   -- T::operator== (const T&)
//  (4) a < operator           -- T::operator< (const T&)
//  (5) an output operator     -- operator<< (ostream&, T&)
//
// RCS: $Id: Heap.c,v 2.6 1995/05/09 15:55:02 soccm Exp $
//
// ======================================================================

#include <stdlib.h>
#include <string.h>

// Description:
// The default constructor.

template<class T> Heap<T>::Heap (void)
{
	size   = ChunkSize;
	nelems = 0;
	list   = new T[size + 1];
}

// Description:
// Other constructor. `capacity_estimate' is meant to be an estimate of how
// large the Heap will get. It may be slightly more efficient to allocate
// the complete size of the Heap once, instead of expanding it as needed.
// Of course the Heap will always expand as needed when inserting a new
// element.

template<class T> Heap<T>::Heap (int capacity_estimate)
{
	if (capacity_estimate <= 0)
		error("Heap::Heap(): capacity_estimate must be positive.");
	size   = capacity_estimate;
	nelems = 0;
	list   = new T[size + 1];
}

// Description:
// The copy constructor.

template<class T> Heap<T>::Heap (const Heap<T>& rhs)
{
	size   = rhs.size;
	nelems = rhs.nelems;
	list   = ::new T[size + 1];
	memcpy(&list[1], &rhs.list[1], nelems * sizeof(T));
}

// Description:
// The destructor.

template<class T> Heap<T>::~Heap (void)
{
	delete [] list;
}

// Description:
// The assignment operator.

template<class T> Heap<T>& Heap<T>::operator=(const Heap<T>& rhs)
{
	if (this != &rhs) {
		//
		// This delete here will cause a memory leak with
		// any objects stored in the Heap that have been
		// allocate off the free store.  If this is a
		// concern, the elements on the Heap should be delete'd
		// before doing the assignment.
		//
		delete [] list;
		nelems = rhs.nelems;
		size   = rhs.size;
		list   = ::new T[size + 1];
		memcpy(&list[1], &rhs.list[1], nelems * sizeof(T));
	}
	return *this;
}

// Description:
// The equality operator.

template<class T> int Heap<T>::operator== (const Heap<T>& rhs) const
{
	if (nelems == rhs.nelems) {
		for (int i = 1; i < entries(); i++)
			if (!(list[i] == rhs.list[i])) return 0;
			return 1;
	}
	return 0;
}

// Description:
// Inserts an element into the Heap, keeping it ordered from
// largest element to smallest.  Expects to use T::operator< (const T&)
// and T::operator= (const T&).

template<class T> void Heap<T>::insert (const T& element)
{
	if (nelems == size) {
		size *= 2;
		T* nlist = new T[size + 1];
		memcpy(&nlist[1], &list[1], nelems * sizeof(T));
		delete [] list;
		list = nlist;
	}

	list[++nelems] = element;

	//
	// Re-establish the heap property.
	//
	for (int k = nelems, j = k/2; j && list[j] < list[k]; k /= 2, j /= 2) {
		T tmp   = list[j];
		list[j] = list[k];
		list[k] = tmp;
	}
}

// Description:
// Removes the largest element from the Heap and returns it.
// It is an error if the Heap is empty.

template<class T> T Heap<T>::remove (void)
{
	if (isEmpty()) error("The Heap is empty.");

	T rc = list[1];

	if (--nelems >= 1) {
		//
		// Put the `last' entry in the Heap into the root and reorder.
		//
		list[1] = list[nelems + 1];
		reorder();
	}

	return rc;
}

// Description:
// Removes the largest element from the Heap and returns it,
// while simultaneously inserting the passed element into the
// Heap. If the Heap is empty, this returns the passed element.

template<class T> T Heap<T>::replace (const T& element)
{
	if (isEmpty()) return element;

	T rc = list[1];
    
	list[1] = element;

	reorder();

	return rc;
}

// Description:
// Returns the index in the Heap of an element comparing equal to
// the passed element, or 0 if no such element is in the list.
// Uses T::operator== (const T&) and T::operator< (const T&).
// Also returns 0 if the list is empty.

template<class T> int Heap<T>::search (const T& element) const
{
	//
	// A simple linear search should be adequate.
	//
	for (int i = 1; i <= entries(); i++)
		if (list[i] == element) return i;
	return 0;
}

// Description:
// Reorder the Heap by successively replacing the parent with the
// larger of the two children when the Heap property isn't satisfied.

template<class T> void Heap<T>::reorder (void)
{
	for (int k = 1, j = 2*k; j <= nelems; j *= 2) {
		T larger = list[j];

		if (j + 1 <= nelems) {
			//
			// Both children exist.
			//
			if (list[k] < list[j] || list[k] < list[j+1]) {
				if (list[j] < list[j+1])
					//
					// The right child is the larger.
					//
					larger = list[++j];
				list[j] = list[k];
				list[k] = larger;
				k = j;
			} else
				//
				// The Heap condition is satisfied.
				//
				break;
		} else {
			//
			// Only the left child exists.
			//
			if (list[k] < list[j]) {
				list[j] = list[k];
				list[k] = larger;
				k = j;
			} else
				//
				// The Heap condition is satisfied.
				//
				break;
		}
	}
}

@


2.5
log
@beautified for genman
@
text
@d1 5
a6 2
// Implementation of a templatized heap class.
//
d15 1
a15 1
// $Id: Heap.c,v 2.4 1994/02/25 19:35:02 lijewski Exp lijewski $
d17 1
d22 1
a22 2

//
a23 1
//
d27 3
a29 3
    size   = ChunkSize;
    nelems = 0;
    list   = new T[size + 1];
d32 3
a34 4

//
// Other constructor.  `capacity_estimate' is meant to be an estimate of how
// large the Heap will get.  It may be slightly more efficient to allocate
a37 1
//
d41 5
a45 5
    if (capacity_estimate <= 0)
        error("Heap::Heap(): capacity_estimate must be positive.");
    size   = capacity_estimate;
    nelems = 0;
    list   = new T[size + 1];
a47 2

//
a49 1
//
d53 4
a56 4
    size   = rhs.size;
    nelems = rhs.nelems;
    list   = ::new T[size + 1];
    memcpy(&list[1], &rhs.list[1], nelems * sizeof(T));
a58 2

//
a60 1
//
d64 1
a64 1
    delete [] list;
a66 2

//
a68 1
//
d70 1
a70 1
template<class T> Heap<T>& Heap<T>::operator= (const Heap<T>& rhs)
d72 15
a86 16
    if (this != &rhs)
    {
        //
        // This delete here will cause a memory leak with
        // any objects stored in the Heap that have been
        // allocate off the free store.  If this is a
        // concern, the elements on the Heap should be delete'd
        // before doing the assignment.
        //
        delete [] list;
        nelems = rhs.nelems;
        size   = rhs.size;
        list   = ::new T[size + 1];
        memcpy(&list[1], &rhs.list[1], nelems * sizeof(T));
    }
    return *this;
a88 2

//
a90 1
//
d94 6
a99 8
    if (nelems == rhs.nelems)
    {
        for (int i = 1; i < entries(); i++)
            if (!(list[i] == rhs.list[i]))
                return 0;
        return 1;
    }
    return 0;
a101 2

//
a105 1
//
d109 7
a115 8
    if (nelems == size)
    {
        size *= 2;
        T* nlist = new T[size + 1];
        memcpy(&nlist[1], &list[1], nelems * sizeof(T));
        delete [] list;
        list = nlist;
    }
d117 1
a117 1
    list[++nelems] = element;
d119 8
a126 9
    //
    // Re-establish the heap property.
    //
    for (int k = nelems, j = k/2; j && list[j] < list[k]; k /= 2, j /= 2)
    {
        T tmp   = list[j];
        list[j] = list[k];
        list[k] = tmp;
    }
a128 2

//
a131 1
//
d135 1
a135 2
    if (isEmpty())
        error("The Heap is empty.");
d137 1
a137 1
    T rc = list[1];
d139 7
a145 8
    if (--nelems >= 1)
    {
        //
        // Put the `last' entry in the Heap into the root and reorder.
        //
        list[1] = list[nelems + 1];
        reorder();
    }
d147 1
a147 1
    return rc;
a149 2

//
d153 1
a153 2
// Heap.  If the Heap is empty, this returns the passed element.
//
d157 1
a157 2
    if (isEmpty())
        return element;
d159 1
a159 1
    T rc = list[1];
d161 1
a161 1
    list[1] = element;
d163 1
a163 1
    reorder();
d165 1
a165 1
    return rc;
a167 2

//
a172 1
//
d176 6
a181 8
    //
    // A simple linear search should be adequate.
    //
    for (int i = 1; i <= entries(); i++)
        if (list[i] == element)
            return i;

    return 0;
a183 2

//
a186 1
//
d190 2
a191 3
    for (int k = 1, j = 2*k; j <= nelems; j *= 2)
    {
        T larger = list[j];
d193 33
a225 40
        if (j + 1 <= nelems)
        {
            //
            // Both children exist.
            //
            if (list[k] < list[j] || list[k] < list[j+1])
            {
                if (list[j] < list[j+1])
                    //
                    // The right child is the larger.
                    //
                    larger = list[++j];
                list[j] = list[k];
                list[k] = larger;
                k = j;
            }
            else
                //
                // The Heap condition is satisfied.
                //
                break;
        }
        else
        {
            //
            // Only the left child exists.
            //
            if (list[k] < list[j])
            {
                list[j] = list[k];
                list[k] = larger;
                k = j;
            }
            else
                //
                // The Heap condition is satisfied.
                //
                break;
        }
    }
@


2.4
log
@genman'd
@
text
@d12 1
a12 1
// $Id: Heap.c,v 2.3 1994/02/25 14:41:51 lijewski Exp lijewski $
d105 1
a105 1
template<class T> int Heap<T>::operator== (const Heap<T>& rhs)
@


2.3
log
@went from long to int for simplicity
@
text
@d12 1
a12 1
// $Id: Heap.c,v 2.2 1993/12/26 21:58:36 lijewski Exp lijewski $
d50 1
d64 1
d68 4
a71 1
template<class T> Heap<T>::~Heap (void) { delete [] list; }
d75 1
d101 1
d119 1
d151 1
d177 1
d199 1
d220 1
@


2.2
log
@changed largest() to remove(). Added replace() and operator==().
@
text
@d12 1
a12 1
// $Id: Heap.c,v 2.1 1993/12/22 14:20:11 lijewski Exp lijewski $
d39 1
a39 1
template<class T> Heap<T>::Heap (long capacity_estimate)
d42 1
a42 1
        error("Heap: capacity_estimate must be positive.");
a57 12
    //
    // On our Sun Sparcs under Object Center, the third argument
    // to memcpy() is prototyped as an `int', while according to
    // ANSI it's a `size_t'.  As a result I get warnings for what
    // is legitimate code.  Since on these machines longs and ints
    // are the same size, this should be OK until it can be removed.
    //
    // TODO - remove this stuff when we go to Solaris.
    //
#if defined(__CLCC__) || defined(ultrix)
    memcpy(&list[1], &rhs.list[1], (int) (nelems * sizeof(T)));
#else
a58 1
#endif
a87 12
        //
        // On our Sun Sparcs under Object Center, the third argument
        // to memcpy() is prototyped as an `int', while according to
        // ANSI it's a `size_t'.  As a result I get warnings for what
        // is legitimate code.  Since on these machines longs and ints
        // are the same size, this should be OK until it can be removed.
        //
        // TODO - remove this stuff when we go to Solaris.
        //
#if defined(__CLCC__) || defined(ultrix)
        memcpy(&list[1], &rhs.list[1], (int) (nelems * sizeof(T)));
#else
a88 1
#endif
d102 1
a102 1
        for (long i = 1; i < entries(); i++)
a122 12
        //
        // On our Sun Sparcs under Object Center, the third argument
        // to memcpy() is prototyped as an `int', while according to
        // ANSI it's a `size_t'.  As a result I get warnings for what
        // is legitimate code.  Since on these machines longs and ints
        // are the same size, this should be OK until it can be removed.
        //
        // TODO - remove this stuff when we go to Solaris.
        //
#if defined(__CLCC__) || defined(ultrix)
        memcpy(&nlist[1], &list[1], (int) (nelems * sizeof(T)));
#else
a123 1
#endif
d133 1
a133 1
    for (long k = nelems, j = k/2; j && list[j] < list[k]; k /= 2, j /= 2)
d195 1
a195 1
template<class T> long Heap<T>::search (const T& element) const
d200 1
a200 1
    for (long i = 1; i <= entries(); i++)
d215 1
a215 1
    for (long k = 1, j = 2*k; j <= nelems; j *= 2)
@


2.1
log
@Creation
@
text
@d12 1
a12 1
// $Id$
d19 4
d30 9
d41 2
d48 1
d74 1
d81 1
d93 1
a93 1
        // allocate of the free-memory heap.  If this is a
d119 18
d140 1
a140 1
// and T::operator= (cosnt T&).
d147 1
a147 1
        size += ChunkSize;
d174 1
a174 1
        T tmp    = list[j];
d176 1
a176 1
        list[k]  = tmp;
d180 1
d186 1
a186 1
template<class T> T Heap<T>::largest (void)
d193 1
a193 1
    if (nelems-- > 1)
a194 1
        list[1] = list[nelems+1];
d196 1
a196 2
        // Reorder the Heap by successively replacing the parent with the
        // larger of the two children when the Heap property isn't satisfied.
d198 2
a199 37
        for (long k = 1, j = 2*k; j <= nelems; j *= 2)
        {
            T larger = list[j];
            if (j + 1 <= nelems)
            {
                //
                // Both children exist.
                //
                if (list[k] < list[j] || list[k] < list[j+1])
                {
                    if (list[j] < list[j+1])
                        //
                        // The right child is the larger.
                        //
                        larger = list[++j];
                    list[j] = list[k];
                    list[k] = larger;
                    k = j;
                }
                else
                    break; // The Heap condition is satisfied.
            }
            else
            {
                //
                // Only the left child exists.
                //
                if (list[k] < list[j])
                {
                    list[j] = list[k];
                    list[k] = larger;
                    k = j;
                }
                else
                    break; // The Heap condition is satisfied.
            }
        }
d207 21
d245 54
@

head	2.10;
access
	rbarnett
	soccm;
symbols
	Build4:2.9
	Build3_3:2.9
	Build3_2:2.9
	Build3_1:2.9
	Build3:2.9
	Current:2.9
	Build2:2.6;
locks;
comment	@ * @;


2.10
date	95.10.23.12.41.17;	author rbarnett;	state Exp;
branches;
next	2.9;

2.9
date	94.02.27.19.41.14;	author lijewski;	state Exp;
branches;
next	2.8;

2.8
date	94.02.25.21.52.33;	author lijewski;	state Exp;
branches;
next	2.7;

2.7
date	93.12.02.22.49.45;	author lijewski;	state Exp;
branches;
next	2.6;

2.6
date	93.10.12.20.54.20;	author lijewski;	state Release;
branches;
next	2.5;

2.5
date	93.09.27.15.35.01;	author lijewski;	state Exp;
branches;
next	2.4;

2.4
date	93.09.01.20.46.17;	author lijewski;	state Exp;
branches;
next	2.3;

2.3
date	93.08.18.15.23.50;	author lijewski;	state Exp;
branches;
next	2.2;

2.2
date	93.08.06.19.09.19;	author lijewski;	state Exp;
branches;
next	2.1;

2.1
date	93.08.06.19.03.46;	author lijewski;	state Exp;
branches;
next	;


desc
@Creation
@


2.10
log
@update test
I mean text
@
text
@// ======================================================================
// File Name   : SCDataCache.h
// Subsystem   : Data Ingest
// Programmer  : Mike Lijewski
// Programmer  : Randall D. Barnette, Hughes STX
// Description : This file contains code defining the spacecraft data cache.
//
// .NAME SCDataCache - Implementation of the spacecraft data cache.
// .LIBRARY ingest
// .HEADER  Data Ingest
// .INCLUDE SCDataCache.h
// .FILE    ../rtclient/SCDataCache.C
// .VERSION $Revision$
//
// $Id: SCDataCache.h,v 2.9 1994/02/27 19:41:14 lijewski Exp $
// ========================================================================

// .SECTION DESCRIPTION
// This is an implementation of the Spacecraft data cache to be
// to be used by realtime clients. It uses CacheLine to implement
// each the cache for each type of Spacecraft data.

// .SECTION Author
// Mike Lijewski, NASA/GSFC, lijewski@@rosserv.gsfc.nasa.gov
// Randall D. Barnette, Hughes STX, rbarnett@@xpert.stx.com

#ifndef SCDATACACHE_H
#define SCDATACACHE_H

#include "CacheLine.h"
#include "List.h"
#include "SCData.h"

//
// Class modelling a SpaceCraft Data Cache.
//

class SCDataCache {

public:

  //* Constructor

	SCDataCache(void);

  //* Destructor

	~SCDataCache(void);

  //* Public Member Functions

	void enableCaching(SCDataType type);

	void disableCaching(SCDataType type);

	void cachePacket(PktTlm* pkt);

	SCData* mostRecentValue(SCDataType type);

	SCData* valuesInRange(SCDataType type,
                        unsigned long lsec, unsigned long lsubsec,
                        unsigned long rsec, unsigned long rsubsec,
                        int& nrange);

	SCData* ceilingValue(SCDataType type,
                       unsigned long seconds, unsigned long subseconds);

	unsigned long entries(SCDataType type) const;

	void stats(void);

private:

  //* Disabled Functions

	SCDataCache(const SCDataCache&);            // Disabled.
	SCDataCache& operator=(const SCDataCache&); // Disabled.

  //* Private Data Members

	static int counter;

	List<SCDataType> enabled;

	CacheLine<SCData, SCPosition> position;
	CacheLine<SCData, SCAttitude> attitude;
	CacheLine<SCData, SCSARotationA> sa_rot_a;
	CacheLine<SCData, SCSARotationB> sa_rot_b;
	CacheLine<SCData, SCPcuTempA> pcu_temp_a;
	CacheLine<SCData, SCPcuTempB> pcu_temp_b;

} ;

#endif /*SCDATACACHE_H*/
@


2.9
log
@beautified genman output
@
text
@d1 10
a10 6
//
// $Id: SCDataCache.h,v 2.8 1994/02/25 21:52:33 lijewski Exp lijewski $
//
// .NAME SCDataCache.h - implementation of the spacecraft data cache.
// .LIBRARY subclass
// .HEADER
d12 2
a13 2
// .FILE SCDataCache.C
// .FILE SCDataCache.h
d15 8
d25 1
a25 6
//
// .SECTION Description:
// This is an implementation of the Spacecraft data cache to be
// to be used by realtime clients.  It uses CacheLine to implement
// each the cache for each type of Spacecraft data.
//
a33 1

d38 7
a44 3
class SCDataCache
{
  public:
d46 1
a46 1
    SCDataCache (void);
d48 1
a48 1
    ~SCDataCache (void);
d50 1
a50 1
    void enableCaching (SCDataType type);
d52 1
a52 1
    void disableCaching (SCDataType type);
d54 1
a54 1
    void cachePacket (PktTlm* pkt);
d56 1
a56 1
    SCData* mostRecentValue (SCDataType type);
d58 1
a58 1
    SCData* valuesInRange (SCDataType type, unsigned long lsec, unsigned long lsubsec, unsigned long rsec, unsigned long rsubsec, int& nrange);
d60 4
a63 1
    SCData* ceilingValue (SCDataType type, unsigned long seconds, unsigned long subseconds);
d65 2
a66 1
    unsigned long entries (SCDataType type) const;
d68 1
a68 1
    void stats (void);
d70 1
a70 1
  private:
d72 1
a72 2
    SCDataCache (const SCDataCache&);            // Disabled.
    SCDataCache& operator= (const SCDataCache&); // Disabled.
d74 1
a74 1
    static int counter;
d76 2
a77 1
    List<SCDataType> enabled;
d79 1
a79 1
    CacheLine<SCData, SCPosition> position;
d81 1
a81 1
    CacheLine<SCData, SCAttitude> attitude;
d83 1
a83 1
    CacheLine<SCData, SCPcuTempA> pcu_temp_a;
d85 6
a90 2
    CacheLine<SCData, SCPcuTempB> pcu_temp_b;
};
d92 1
@


2.8
log
@genman'd
@
text
@d2 1
a2 1
// $Id: SCDataCache.h,v 2.7 1993/12/02 22:49:45 lijewski Exp lijewski $
d48 1
a48 4
    SCData* valuesInRange (SCDataType type,
                           unsigned long lsec, unsigned long lsubsec,
                           unsigned long rsec, unsigned long rsubsec,
                           int& nrange);
d50 1
a50 2
    SCData* ceilingValue (SCDataType type, unsigned long seconds,
                          unsigned long subseconds);
@


2.7
log
@increased encapsulation
@
text
@d2 1
a2 1
// $Id: SCDataCache.h,v 2.6 1993/10/12 20:54:20 lijewski Release lijewski $
d8 1
a39 3
    //
    // Turn on caching for the passed data type.
    //
a41 4
    //
    // Turn off caching for the passed data type.  Also releases
    // any previously cached data for that data type.
    //
a43 4
    //
    // Extract any spacecraft data from the passed Telemetry packet
    // and cache it.
    //
a45 7
    //
    // Return the most recent value of the passed data type, else
    // NULL if there are no cached values of that type.  To use
    // the value you must cast it to the same type as you asked for.
    // The value returned is in new'd storage so it is up to the
    // user to delete the value when no longer needed.
    //
a47 22
    //
    // Return an array consisting off all the cached values for the
    // passed data type that lie in the interval
    //
    // (lsec, lsubsec) .. (rsec, rsubsec)
    // 
    // inclusive.  Here we explicitly see the fact that spacecraft
    // data are ordered by the ordered pair (seconds, subseconds)
    // from their respective CCSDS Telemetry packet.
    //
    // `nrange' is set to the number of elements in the array.  That
    // is, array[0] .. array[nrange-1] are valid values.  `nrange' is
    // set to zero if there are no cached values in the interval, as
    // well as NULL being returned.  To use the values you must cast
    // the return value to the same type as you asked for.  The
    // returned array is in new'd storage so it is up to the user to
    // delete it when no longer needed.  If t2 < t1 the range is taken
    // to be NULL.
    //
    // lsubsec == 0 and rsubsec == ULONG_MAX are useful values to use
    // if you don't have any others in mind.
    //
a52 13
    //
    // Return the value of the passed data type whose seconds and
    // subseconds fields are as large as possible but less than or
    // equal to the passed values, else NULL if there are no cached
    // values of that type.  The spacecraft data are ordered by the
    // ordered pair (seconds, subseconds) using SCData::operator<().
    // This returns either an element with matching seconds and
    // subseconds field, or that element whose seconds and subseconds
    // field are as close as possible to the passed values, yet
    // smaller.  To use the value you must cast it to the same type
    // as you asked for.  The value returned is in new'd storage so
    // it is up to the user to delete the value when no longer needed.
    //
a55 3
    //
    // Number of cached objects of the respective data type.
    //
d62 3
a64 11
    //
    // Disable the copy constructor and the assignment operator.
    //
    SCDataCache (const SCDataCache&);
    SCDataCache& operator= (const SCDataCache&);

    //
    // We want to disallow more than one instance of ourselves in any
    // running process.  It is a fatal error to try to instantiate
    // more than one SCDataCache in any given application.
    //
a66 4
    //
    // List of those data types that have been enabled; i.e. the
    // types that we're actively caching.
    //
a68 3
    //
    // Sorted list of cached positions.
    //
a70 3
    //
    // Sorted list of cached attitudes.
    //
a72 3
    //
    // Sorted list of cached PCU A Temperatures.
    //
a74 3
    //
    // Sorted list of cached PCU B Temperatures.
    //
@


2.6
log
@cms ci scdata.h SCData.h
@
text
@d2 1
a2 1
// $Id: SCDataCache.h,v 2.5 1993/09/27 15:35:01 lijewski Exp lijewski $
d42 1
a42 1
    void enableCaching (SpacecraftData type);
d48 1
a48 1
    void disableCaching (SpacecraftData type);
d63 1
a63 1
    SCData* mostRecentValue (SpacecraftData type);
d87 1
a87 1
    SCData* valuesInRange (SpacecraftData type,
d105 1
a105 1
    SCData* ceilingValue (SpacecraftData type, unsigned long seconds,
d111 1
a111 1
    unsigned long entries (SpacecraftData type) const;
d134 1
a134 1
    List<SpacecraftData> enabled;
@


2.5
log
@now caching PCU temperatures
@
text
@d2 1
a2 1
// $Id: SCDataCache.h,v 2.4 1993/09/01 20:46:17 lijewski Exp lijewski $
d24 1
a24 1
#include "scdata.h"
@


2.4
log
@now use seconds and subseconds field
@
text
@d2 1
a2 1
// $Id: SCDataCache.h,v 2.3 1993/08/18 15:23:50 lijewski Exp lijewski $
d145 10
@


2.3
log
@simplified scdata stuff
@
text
@d2 1
a2 1
// $Id: SCDataCache.h,v 2.2 1993/08/06 19:09:19 lijewski Exp lijewski $
a23 1
#include "PktTlm.h"
d67 8
a74 1
    // passed data type that lie in the interval t1 .. t2, inclusive.
d78 26
a103 16
    // well as NULL being returned.  To use the value you must cast it
    // to the same type as you asked for.  The return array is in new'd
    // storage so it is up to the user to delete it when no longer
    // needed.  Time is taken to be the MET time stamped in packets.
    // If t2 < t1 the range is taken to be NULL.
    //
    SCData* valuesInRange (SpacecraftData type, unsigned long t1,
                           unsigned long t2, int& nrange);

    //
    // Return the value of the passed data type that is closest
    // to the passed time, else NULL if there are no cached values
    // of that type.  To use the value you must cast it to the same
    // type as you asked for.  The value returned is in new'd storage
    // so it is up to the user to delete the value when no longer
    // needed.  Time is taken to be the MET time stamped in packets.
d105 2
a106 1
    SCData* nearestValue (SpacecraftData type, unsigned long t);
d139 1
a139 1
    CacheLine<PktKey, SCPosition> position;
d144 1
a144 1
    CacheLine<PktKey, SCAttitude> attitude;
@


2.2
log
@genman'd it.
@
text
@d2 1
a2 1
// $Id: SCDataCache.h,v 2.1 1993/08/06 19:03:46 lijewski Exp lijewski $
d25 1
a25 1
#include "PktKey.h"
a27 31
typedef PktKey SCData;


//
// In current documentation, Spacecraft Position comes down in
// ApID 14 every .25 seconds.  If we want to cache one hours
// worth of them, that means we need to keep 14400 of them around.
//
// TODO - once we really know how to extract the position from
//        the packet, update the constructor.
//

struct SCPosition : public SCData
{
    SCPosition (PktTlm* pkt) : SCData(pkt)
    {
        x = pkt->sequenceNumber(); y = x + 1; z = x + 2;
    }

    SCPosition (void) { x = y = z = 0.0; }

    friend ostream& operator<< (ostream& os, const SCPosition& pos);

    float x, y, z;
};


//
// In current documentation, Spacecraft Attitude comes down in
// ApID 16 every 2 seconds.  If we want to cache one hours
// worth of them, that means we need to keep 1800 of them around.
a28 20
// TODO - once we really know how to extract the attitude from
//        the packet, update the constructor.
//

struct SCAttitude : public SCData
{
    SCAttitude (PktTlm* pkt) : SCData(pkt)
    {
        x = pkt->sequenceNumber(); y = x - 1; z = x - 2; w = x - 3;
    }

    SCAttitude (void) { x = y = z = w = 0.0; }

    friend ostream& operator<< (ostream& os, const SCAttitude& att);

    float x, y, z, w;
};


//
a35 7
    //
    // An enumeration of the various types of spacecraft data that
    // we can cache.  The value of the enumeration is the ApID down
    // which the data comes in the telemetry stream.
    //
    enum DataType { ATTITUDE = 14, POSITION = 16 };

d43 1
a43 1
    void enableCaching (DataType type);
d49 1
a49 1
    void disableCaching (DataType type);
d64 1
a64 1
    SCData* mostRecentValue (DataType type);
d78 1
a78 1
    SCData* valuesInRange (DataType type, unsigned long t1,
d89 1
a89 1
    SCData* nearestValue (DataType type, unsigned long t);
d94 1
a94 1
    unsigned long entries (DataType type) const;
d117 1
a117 1
    List<DataType> enabled;
@


2.1
log
@Creation
@
text
@d2 1
a2 1
// $Id: cache.h,v 2.5 1993/08/06 19:01:31 lijewski Exp $
d4 14
@

head	2.6;
access
	rbarnett
	soccm;
symbols
	Build4:2.6
	Build3_3:2.6
	Build3_2:2.6
	Build3_1:2.6
	Build3:2.6
	Current:2.6
	Build2:2.4;
locks;
comment	@ * @;


2.6
date	94.02.25.19.35.02;	author lijewski;	state Exp;
branches;
next	2.5;

2.5
date	94.02.25.14.37.02;	author lijewski;	state Exp;
branches;
next	2.4;

2.4
date	93.11.26.17.53.16;	author lijewski;	state Release;
branches;
next	2.3;

2.3
date	93.11.18.20.49.01;	author lijewski;	state Exp;
branches;
next	2.2;

2.2
date	93.11.18.19.21.52;	author lijewski;	state Exp;
branches;
next	2.1;

2.1
date	93.11.17.21.10.53;	author lijewski;	state Exp;
branches;
next	;


desc
@Creation
@


2.6
log
@genman'd
@
text
@//
// Declarations for a fast and simple sorted dynamic list. The objects
// stored in the list must have:
//
//  (1) a copy constructor   -- T::T(const T&)
//  (2) an equality operator -- T::operator==(const T&)
//  (3) a < operator         -- T::operator<(const T&)
//
// $Id: SList.c,v 2.5 1994/02/25 14:37:02 lijewski Exp lijewski $
//


//
// Description:
// The default constructor.
//

template<class T> SList<T>::SList (int capacity_estimate)
{
    if (capacity_estimate <= 0)
        error("SList::SList(): capacity esitmate must be positive.");
    size   = capacity_estimate;
    nelems = 0;
    list   = new T[size];
}


//
// Description:
// The default constructor.
//

template<class T> SList<T>::SList (void)
{
    size   = ChunkSize;
    nelems = 0;
    list   = new T[size];
}


//
// Description:
// The copy constructor.
//

template<class T> SList<T>::SList (const SList<T>& rhs)
{
    size   = rhs.size;
    nelems = rhs.nelems;
    list   = ::new T[size];
    memcpy(list, rhs.list, nelems * sizeof(T));
}


//
// Description:
// The destructor.
//

template<class T> SList<T>::~SList (void)
{
    delete [] list;
}


//
// Description:
// The assignment operator.
//

template<class T> SList<T>& SList<T>::operator= (const SList<T>& rhs)
{
    if (this != &rhs)
    {
        delete [] list;
        nelems = rhs.nelems;
        size   = rhs.size;
        list   = ::new T[size];
        memcpy(list, rhs.list, nelems * sizeof(T));
    }
    return *this;
}
    

//
// Description:
// Inserts an element into the list, keeping the list ordered from
// smallest element to largest.  Expects to use T::operator<(const T&).
//

template<class T> void SList<T>::insert (const T& element)
{
    if (++nelems == size)
    {
        size += ChunkSize;
        T* nlist = new T[size];
        memcpy(nlist, list, nelems * sizeof(T));
        delete [] list;
        list = nlist;
    }

    //
    // Find index where new element belongs.  Uses T::operator<(const T&).
    //
    for (int index = nelems-2; index >= 0 && element < list[index]; index--)
        ;
    if (index == nelems-2)
        //
        // Just insert at end of list.
        //
        list[nelems-1] = element;
    else
    {
        for (int i = nelems-1; i > index+1; i--)
            list[i] = list[i-1];
        list[index+1] = element;
    }
}


//
// Description:
// Removes the smallest element from the list and returns it.
// It is an error if the list is empty.
//

template<class T> T SList<T>::getSmallest (void)
{
    if (isEmpty())
        error("SList::getSmallest: the SList is empty.");

    T rc = list[0];
    nelems -= 1;
    for (int i = 1; i <= nelems; i++)
        list[i-1] = list[i];
    return rc;
}


//
// Description:
// Returns the index in the SList of an element comparing equal to
// the passed element, or -1 if no such element is in the list.
// Uses T::operator==(const T&) and T::operator<(const T&).  Also
// returns -1 if the list is empty.
//

template<class T> int SList<T>::search (const T& element) const
{
    int rc = -1;

    if (!isEmpty())
    {
        //
        // Do a binary search.
        //

        int found = 0;
        int lo = 0, hi = nelems - 1, index = (lo + hi) / 2;
        
        while (lo <= hi && !found)
        {
            if (list[index] == element)
                found = 1;
            else if (list[index] < element)
                lo = index + 1;
            else
                hi = index - 1;
            index = (lo + hi) / 2;
        }

        rc = found == 0 ? -1 : index;
    }

    return rc;
}


//
// Description:
// Searches for and removes any element in the SList which compares
// equal to the passed element.  Returns true if such an element was
// found and removed, else false.  If there are more than one element
// in the SList which compares equal, which element is removed is
// undefined.
//

template<class T> int SList<T>::remove (const T& element)
{
    int rc = 0;
    int index = search(element);

    if (index != -1)
    {
        rc = 1;
        if (index != nelems - 1)
        {
            //
            // We must remove an element other than the last element in SList.
            //
            for (int i = 0; i < nelems-index-1; i++)
                list[index+i] = list[index+i+1];
        }
        --nelems;
    }

    return rc;
}
@


2.5
log
@went from size_t to int for simplicity
@
text
@d9 1
a9 1
// $Id: SList.c,v 2.4 1993/11/26 17:53:16 lijewski Release lijewski $
d14 1
d29 1
d42 1
d56 1
d60 4
a63 1
template<class T> SList<T>::~SList (void) { delete [] list; }
d67 1
d86 1
d122 1
d141 1
d180 1
@


2.4
log
@compiles on ultrix
@
text
@d9 1
a9 1
// $Id: SList.c,v 2.3 1993/11/18 20:49:01 lijewski Exp lijewski $
d17 1
a17 1
template<class T> SList<T>::SList (long capacity_estimate)
d19 2
a47 11
    //
    // On our Sun Sparcs under Object Center, the third argument
    // to memcpy() is prototyped as an `int', while according to
    // ANSI it's a `size_t'.  As a result I get warnings for what
    // legitimate code.
    //
    // TODO - remove this stuff when we go to Solaris.
    //
#if defined(__CLCC__) || defined(ultrix)
    memcpy(list, rhs.list, (int)(nelems * sizeof(T)));
#else
a48 1
#endif
a70 11
        //
        // On our Sun Sparcs under Object Center, the third argument
        // to memcpy() is prototyped as an `int', while according to
        // ANSI it's a `size_t'.  As a result I get warnings for what
        // legitimate code.
        //
        // TODO - remove this stuff when we go to Solaris.
        //
#if defined(__CLCC__) || defined(ultrix)
        memcpy(list, rhs.list, (int)(nelems * sizeof(T)));
#else
a71 1
#endif
a87 11
        //
        // On our Sun Sparcs under Object Center, the third argument
        // to memcpy() is prototyped as an `int', while according to
        // ANSI it's a `size_t'.  As a result I get warnings for what
        // legitimate code.
        //
        // TODO - remove this stuff when we go to Solaris.
        //
#if defined(__CLCC__) || defined(ultrix)
        memcpy(nlist, list, (int)(nelems * sizeof(T)));
#else
a88 1
#endif
d96 1
a96 1
    for (long index = nelems-2; index >= 0 && element < list[index]; index--)
d105 1
a105 1
        for (long i = nelems-1; i > index+1; i--)
d120 1
a120 1
        error("The SList is empty.");
d124 1
a124 1
    for (long i = 1; i <= nelems; i++)
d137 1
a137 1
template<class T> long SList<T>::search (const T& element) const
d139 1
a139 1
    long rc = -1;
d148 1
a148 1
        long lo = 0, hi = nelems - 1, index = (lo + hi) / 2;
d179 1
a179 1
    unsigned long index = search(element);
d189 1
a189 1
            for (long i = 0; i < nelems-index-1; i++)
@


2.3
log
@workaround for memcpy() warning on Sun Sparcs
@
text
@d9 1
a9 1
// $Id: SList.c,v 2.2 1993/11/18 19:21:52 lijewski Exp lijewski $
d54 1
a54 1
#ifdef sun
d89 1
a89 1
#ifdef sun
d118 1
a118 1
#ifdef sun
@


2.2
log
@stripped out test case
@
text
@d9 1
a9 1
// $Id: SList.c,v 2.1 1993/11/17 21:10:53 lijewski Exp lijewski $
d46 11
d58 1
d81 11
d93 1
d110 11
d122 1
@


2.1
log
@Creation
@
text
@d9 1
a9 1
// $Id$
a194 247

#ifdef TEST

#include <iostream.h>
#include <stdlib.h>
#include <stdio.h>
#include <new.h>
#include <sys/types.h>
#include <sys/time.h>


static void free_store_exhausted()
{
    cerr << "exiting, free store exhausted ...";
    exit(1);
}

void error (const char *format, ...)
{
    time_t t = time(0);

    va_list ap;
    va_start(ap, format);

    putc('\n', stderr);
    (void) fprintf(stderr, "%s****** Error:\n", ctime(&t));
    (void) vfprintf(stderr, format, ap);
    putc('\n', stderr);
    putc('\n', stderr);
    (void) fflush(stderr);

    va_end(ap);
    exit(1);
}

static double elapsed_seconds (struct timeval& tp)
{
    static struct timezone tzp;
    static struct timeval  otp;
    otp = tp;
    gettimeofday(&tp, &tzp);
    return (tp.tv_sec - otp.tv_sec) + (tp.tv_usec - otp.tv_usec)/1000000.0;
}

static struct timeval timer = { 0, 0 };

int main (void)
{
   set_new_handler(free_store_exhausted);

   cout << "Exercising the constructor ...\n";
   SList<int> list(5);
   
   cout << "Exercising isEmpty() ...\n";
   if (!list.isEmpty())
   {
       cerr << "isEmpty() failed!\n";
       exit(1);
   }
   
   //
   // Insert a few elements in order into the list.
   //
   cout << "Exercising insert() ...\n";
   list.insert(2); list.insert(8); list.insert(12);
   if (!list.contains(2) || !list.contains(8) || !list.contains(12))
   {
       cerr << "insert() failed!\n";
       exit(1);
   }
   
   //
   // Now try inserting an element into the beginning of the list.
   //
   list.insert(1);
   cout << "Exercising getSmallest() ...\n";
   if (list.getSmallest() != 1)
   {
       cerr << "getSmallest() != 1 -- getSmallest() or insert() failed!\n";
       exit(1);
   }
   list.insert(1);  // Add it back in.
   
   cout << "Exercising contains() ...\n";
   if (!list.contains(1))
   {
       cerr << "contains() failed!\n";
       exit(1);
   }
   
   cout << "Exercising entries() ...\n";
   if (list.entries() != 4)
   {
       cerr << "entries() failed!\n";
       exit(1);
   }
   
   //
   // Now add a few elements into the middle including some duplicates.
   //
   list.insert(5); list.insert(7); list.insert(4); list.insert(10);
   list.insert(6); list.insert(5); list.insert(4); list.insert(11);
   
   //
   // Now print them out.
   //
   cout << "Exercising operator[]() ...\n";       
   cout << "Should be `1, 2, 4, 4, 5, 5, 6, 7, 8, 10, 11, 12,' ...\n";
   for (int i = 0; i < list.entries(); i++)
       cout << list[i] << ", ";
   cout << "\n";
   
   cout << "Exercising getLargest() ...\n";
   if (list.getLargest() != 12)
   {
       cerr << "getLargest() failed!\n";
       exit(1);
   }
   
   //
   // Insert same elements into list, but in a different order.
   //
   list.insert(2); list.insert(8); list.insert(12); list.insert(1);
   list.insert(6); list.insert(5); list.insert(4); list.insert(11);
   list.insert(5); list.insert(7); list.insert(4); list.insert(10);
   cout << "Printing from smallest to largest ...\n";
   while (!list.isEmpty())
       cout << list.getSmallest() << ", ";
   cout << "\n";
   
   //
   // Now a hopefully silent test of contains() -- we first put the
   // old data back in, but in a different order.
   //
   list.insert(5); list.insert(7); list.insert(4); list.insert(10);
   list.insert(2); list.insert(8); list.insert(12); list.insert(1);
   list.insert(6); list.insert(5); list.insert(4); list.insert(11);
   for (i = 0; i < list.entries(); i++)
   {
       if (!list.contains(list[i]))
       {
           cerr << "contains() failed at index " << i << "!\n";
           exit(1);
       }
   }

   //
   // Now a hopefully silent test of remove().  Remove first element,
   // then last element, then all elements from smallest to largest.
   //
   cout << "Exercising remove() ...\n";
   if (!list.remove(list[0]))
   {
       cerr << "remove()failed on first element in list!\n";
       exit(1);
   }
   if (!list.remove(list[list.entries() - 1]))
   {
       cerr << "remove() failed on last element in list!\n";
       exit(1);
   }
   for (i = 0; i < list.entries(); i++)
   {
       if (!list.remove(list[i]))
       {
           cerr << "remove() failed on element at index " << i << " !\n";
           exit(1);
       }
   }
   
   //
   // Now try to delete a non-existing element.
   //
   list.insert(5); list.insert(7); list.insert(4); list.insert(10);
   list.insert(2); list.insert(8); list.insert(12); list.insert(1);
   list.insert(6); list.insert(5); list.insert(4); list.insert(11);
   if (list.remove(21))
   {
       cerr << "remove() removed a non-existing element!\n";
       exit(1);
   }
   
   //
   // Now a test of the copy constructor and assignment operator.
   //
   cout << "Exercising the copy constructor ...\n";
   SList<int> list2 = list;
   for (i = 0; i < list.entries(); i++)
   {
       if (list.entries() != list2.entries())
       {
           cerr << "entries() are not equal -- copy constructor failed!\n";
           exit(1);
       }
       if (list[i] != list2[i])
       {
           cerr << "list != list2 at index " << i << " !\n";
           exit(1);
       }
   }
   
   cout << "Exercising operator=() ...\n";
   SList<int> list3(100);
   list3 = list;
   for (i = 0; i < list.entries(); i++)
   {
       if (list.entries() != list3.entries())
       {
           cerr << "entries() are not equal -- operator=() failed!\n";
           exit(1);
       }
       if (list[i] != list3[i])
       {
           cerr << "list != list3 at index " << i << " !\n";
           exit(1);
       }
   }
   
   cout << "*** Testing lots of inserts and removes ...\n";
   
   const int count = 1000;
   
   elapsed_seconds(timer);
   
   srandom(13);
   
   for (i = 0; i < count; i++)
       list.insert((int)random());
   
   cout << "Got " << count / elapsed_seconds(timer)
        << " inserts/second\n";
   
   while (!list.isEmpty())
       list.remove(list[0]);
   
   cout << "Got " << count / elapsed_seconds(timer)
        << " deletes/second\n";
   
   //
   // Delete will be invoked when we exit.
   //
   cout << "Exercising delete ...\n";

   return 0;
}

#endif /*TEST*/
@

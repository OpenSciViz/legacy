// ===========================================================================
// File Name   : config.h
// Subsystem   : Data Ingest
// Programmer  : Mike Lijewski, Hughes STX
//               Randall D. Barnette, Hughes STX
// Description : Contains constants used by the Ingest Subsystem that may
//               change up to and including the In Orbit Checkout (and
//               possibly beyond that).
//
// $Id: config.h,v 2.11 1994/02/22 21:24:50 lijewski Exp $
//
// ===========================================================================

#ifndef CONFIG_H
#define CONFIG_H

// The highest valid ApID for the XTE project.
//
// We expect to be able to define arrays of the form `int X[MaxApID+1]'.
// This assumes that application IDs are non-negative, which is indeed the
// case.  If the number of valid application IDs in the XTE project changes,
// this is the only place in the Ingest Subsystem where it needs to be changed.
// It will also need to be changed in the PktCCSDS hierarchy though;
// specifically PktTlm::sane() needs to know both the smallest and largest
// largest valid ApIDs in the XTE project.  Here I'm assuming that zero is the
// smallest valid ApID.  PktTlm::sane() will check that a raw packet is a
// valid one for the XTE project before it builds it.  Of course this only
// works if you make sure and call it before attempting to build a packet from
// a raw unsigned char buffer.
//
// This should be set in concrete before launch.

const int MaxApID = 233;

// Every `UpdateRate' seconds Realtime Ingest delivers a statistics packet as
// well as checks its PktFiles for any that need their overlap timers set or
// that need to be written to disk. This is used by rtingest.C and archive.C.
//
// This should not need to be changed.
//
// If you do think it should be changed be cognizant that this number is used
// in the calculation of MaximumFileSize below.  Changing this could have an
// adverse impact on the efficiency of the Ingest process.  Know well the
// implications of what you're doing.

const unsigned int UpdateRate = 10;

// This is the maximum number of bits we expect to receive in any given second
// from the DCF, on average. This corresponds to the maximum throughput
// telemetry rate. The overall nominal rate is much smaller than this.
//
// Every `UpdateRate' seconds Realtime Ingest checks to see if any PktFiles
// need to be written to disk. We want to write enough bytes to disk each
// `UpdateRate' interval to keep up with the maximum ingest rate, but we don't
// want to write more than we have to so that we're "off the net" for as little
// time in any given `UpdateRate' interval as possible. In this way we hope
// to keep the average ingest rate curve as flat as possible. This is used in
// archive.C.
//
// If the maximum number of bits expected in any given second from DCF changes,
// this MUST be changed as well.

const unsigned long MaxBitsPerSecond = 1048576;

// This is the maximum number of seconds we want to keep a PktFile in memory,
// waiting for more packets to be inserted.
//
// That is to say, this is the maximum length of time that Ingest will keep a
// packet in memory before writing it to disk. This doesn't function as an
// exact upper limit, though it is one more or less. Logically this value
// belongs in `PktFile.h' where it is the maximum length of time before we
// start the overlap timer for the PktFile running.
//
// This number may need to be adjusted once we get some experience with the
// distribution of telemetry from the real XTE Spacecraft.

const int MaximumLiveTime = 600;

// This is the maximum number of bytes we'll accept into a PktFile before
// starting the overlap timer running.
//
// The overlap timer is turned on when:
//
//   1) the PktFile contains more than `MaximumFileSize' bytes or
//   2) when the file has been around more than `MaximumLiveTime' seconds.
//
// The below formula corresponds to half the maximum amount of data we
// expect to get from DCF in an UpdateRate interval.  The idea here is
// that at the maximum throughput rate, the telemetry will be coming down
// in at least two different ApIDs.  We want to archive files that are as
// big as possible, as this is most efficient, with a few constraints:
//
//  1) we don't want to spend too much time archiving a single packet file;
//  2) we don't to waste our time archiving lots of small packet files;
//  3) we want to get things to disk as soon as possible.
//
// The below formula corresponds to a PktFile containing half of the
// total projected amount of telemetry in an UpdateRate interval.  With
// UpdateRate == 10 and MaxBitsPerSecond == 1048576, MaximumFileSize comes
// out to 655360, which any decent local disk can do is a half-second, or
// less.

const unsigned long MaximumFileSize = UpdateRate * MaxBitsPerSecond / 16;

// The number of seconds we keep an old PktFile around waiting for any stray
// packets.  The life of a PktFile thus consists of two phases:
//
//   (1) the active phase before `MaximumLiveTime' or `MaximumFileSize' have
//       been exceeded; and
//   (2) an overlap phase when the next PktFile in sequence has been activated.
//
// We only wait a finite length of time; packets which arrive later don't get
// archived.  In fact, they will be archived somewhere, but not in the normal
// database.  This logically belongs in `PktFile.h'.
//
// When we enter ZOE (when # of connections with DCF goes to zero), we archive
// all packets in each PacketFile, up to SecondsOfOverlaps worth.  This way,
// when we leave the ZOE, we'll have some slack in which to properly order
// out-of-order packets.

const int SecondsOfOverlap = 30;

#endif /*CONFIG_H*/

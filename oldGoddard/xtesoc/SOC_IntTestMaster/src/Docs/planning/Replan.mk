<MakerFile 5.0Y>
  	
Aa�         S  � ff !             H   H   	   $   ��    d                                      H   ?   l   H       ��  ��  ��  ff  @                    �  �  �     ��                      
�  
�  
�  �  �  �  �   
  
�  �  �  �  �      d    A            	�  Footnote TableFootnote  *��  *��              .    .  / - � �  :;,.�!?      $     `  �    (    	 ` #              LOT   
TableTitle            LOF   Figure             TOC   Heading1 Heading2 Heading3    DAPs ObsID  arots@rosserv  cg cg_noinherit cg_sanction  chmod  cloneObs cp dcf� email� gawne� jlander� mlander� 
mocdeliver mp_createOb� mp_run socops xmpj xprod� xteplan@athena  A� S   A� H    A\                                              
               	                                     EquationVariables�        7Je             � �"���  �T�U&^` �V�U�8 �g�V    �i�Ve 5 �j�W	
 �l�W  �|�X � �~�X ���Y ���Y    �W�\	   �Y�\           
�
� 
�Uo        
�Vn         
�Wo  H      
�Xq   ��  ff
�Yo        
�\n              m��    m   <$paratext[Title]>  n   <$paratext[1Heading]>�  o   <$curpagenum>  p   <$lastpagenum>  q   <$monthname> <$daynum>, <$year>�    r   <$monthname> <$daynum>, <$year>     s   ;<$monthname> <$daynum>, <$year> <$hour>:<$minute00> <$ampm>T  let   "<$monthnum>/<$daynum>/<$shortyear>  u    (Continued)  g2v   + (Sheet <$tblsheetnum> of <$tblsheetcount>)g  oiw   <$paratext[title.classname]>   x   
<$marker2>  y  Page page<$pagenum>o  elz  Heading & Page  �<$paratext>� on page<$pagenum>  {  Section Number & Page )Section<$paranumonly> on page<$pagenum>     |  See Heading & Page %See �<$paratext>� on page<$pagenum>.    }  Section Alla 9Section<$paranumonly>, �<$paratext>,� on page<$pagenum>&  ~  Figure Number & Page (Figure<$paranumonly> on page<$pagenum>  �  	Table All� 7Table<$paranumonly>, �<$paratext>,� on page<$pagenum>�  �  Table Number & Page  'Table<$paranumonly> on page<$pagenum>Y    �  
Figure All 8Figure<$paranumonly>, �<$paratext>,� on page<$pagenum>  �  Section & Page %Section<$paranum> on page<$pagenum>a  pa�  Table & Page 'Table<$paranumonly> on page<$pagenum>  mo�  Figure & Page  (Figure<$paranumonly> on page<$pagenum>  �  	classname: '<typewriter><$paratext><Default � Font>m  �  paratext <$paratext>   �  requirementv \t<$paranum>\t<$paratext>>  �  reqTrace [<$paranum>]   �  	reqNumbere 
<$paranum>   �  paranumonly> <$paranumonly>  �  paranum  
<$paranum>  �  Figure Figure<$paranumonly>a          
�
�
� io          R
��� Q     Aa  mo          R
��� P                  R
��� P                  R
��� Q     A             R
��� P                  R
��� P                  R
��� Q     A   Pa          R
��� P                  R
��� P                  R
��� P     TOCx            R
��� P     LOFl            R
��� P     LOTl            R
��� Q     IX  re              
���      LOFa                
���      LOT            R
���      A>  
)
y
S G$p ��
)�a   3.l   ��
*��l    2.3l   �G
+��p    2.2�   ��
,��P      Fi ��
-�n   1.n   �X
.��u        ��
/�:   1.i   �b
0�t   2.    �c
1��       te �h
2�e   3.�   �i
3�n   4.a   �e
4�  a         ��
5��a      $p �f
6�        re �{
7�  $      m> �
8�  a      nl ��
9�  u         ��
:  u      pa ��
;�
     2.1u   9�
<�  n         9�
=�  �         �z
>��       Q �{
?��o         �m
@�  �         ��
A         R ��
B�    2.    ��
C  �         ��
D��        ��
E�P         ��
F��      � ��
G�          ��
H��         ��
I�    3.   ��
J��     2.5   ��
K��P    2.4    ��
L���         ��
M��     2.6�   ��
N�O   1.    ��
O��   2.   ��
P�    5.�   ��
Q��X         ��
R�    4.    ��
V��          ��
W��      LO ��
X��     2.7�   ��
Y��)      $p �
Z��a      3. �
[���        �
]��       � �
^�   �   �
_�P   �   �
`�n   �   �
a���    2.8   �%
b�   �   �"
c��t      2. 9�
d
  �     2.0   �&
e�   �   �+
f�n   �   �,
g��   �   �R
i�  �         �S
j�  �         �Z
k�  �         ��
l�  �         ��
m�  �         ��
n            ��
o  �        ��
p         � ��
q         � ��
r         � ��
s  Q      � ��
t	         y�� ]          d         �[y�              .            ��            �           d         ��z�                 �        ��            �  �        d         ��{�                          ��                 H   h�q�  V1     ��|�    {            H   h�q�  V1=��      '                  ' 2.          �          ' �� �� UU  
Z
   XWe have several constraints on a large scale - try to keep to less than 20 maneuver per    �� �� UU  
Z
$p Vday, and try to keep the maneuver time to less than 400 minutes per day. On the micro- �� �� UU  
Z
 `scale, we must end all 
maneuvers such that we are on the target for at least 10 minutes   *�� �� UU  
Z
 Zbefore any occultation (or SAA) occurs. This is to allow the star trackers to acquire. We  6�� �� UU  
Z
�� Xcannot break this rule. So, after making schedule adjustments, you will have to go down    B�� �� UU  
Z
   ]the line with each target, verifying that the slew ends on or after the trailing edge of its [ N�� �� UU  
Z
. Voccultation, and at least 10 minutes before the leading edge of the next occultation.  Z�� �� UU  
Z
   MSometimes this will be annoying (especially if slew times get recomputed and   f�� �� UU @
Z
   &everything is automatically shifted.)
 ~�� �� UU  
[
  STo assist in such small movement, the �nudge� gadget will move the scheduled start  se ��� �� UU  
[
on Ytime of a target 1 minute in either direction. This is easier than trying to click ultra-t ��� �� UU @
[
eu precisely.
ess ��� �� UU `
M#pe Running Command Generation
UU Ī� �� UU  
F
e  ZAt the close of mp_run
, it will give a suggested invocation of how to run command  Ъ� �� UU  
F
be Ugeneration, usually in the form of �Type �cg_noinherit -week XXX� to complete.� This   ܪ� �� UU  
F
�� Wis usually sound advice, and should be done unless there are other considerations, for     誚 �� UU @
F
   4example, a new routine to test, or errors to debug.
nd �� �� UU  
W
tr aThis will call the cg
 script, which will run all of command generation. This script (and d�� �� UU  
W
ul Rothers) are described more thoroughly in the Appendix. It has fairly robust error �� �� UU @
W
 a 0checking and should inform you of any problems.
ll6�� �� UU `
X#�� Delivery to the SOF
t F�� �� UU `
]
me RThere are several steps prior to deliver of the final products, RTS Matched DAPs.
X�� �� UU  
^
e  XDue to a bug in the cg
 script, run the routine that makes the slew list by hand isd�� �� UU  
^
UU R(typically, �source_list.pl -0YY >source_list.log�, run in the appropriate week�s p�� �� UU @
^
gi directory (~planner/week0YY).
�� �� UU  
`
�� PLog onto xprod as user �planner�, and run the �planning.pl� script to bring the � ��� �� UU 
`
   Karchives over (�planning.pl -week 0YY�). If the schedule changes have been e a��� �� UU  
`
ti Oapproved and it�s a ready-to-deliver-to-the-MOC version, include the �-public -bug��� �� UU  
`

W [replan� options with this so the email notification to the PI will be sent. If it�s just a rip��� �� UU @
`
UU (prototype, do not 
mail the PIs.
ug��� �� UU  
_
   HSend email to xteplan@athena, keith@pcasrv1, dgruber@mamacass.ucsd.edu, rmʪ� �� UU  
_
ms 6pblanco@mamacass.ucsd.edu, wheindl@mamacass.ucsd.edu, ֪� �� UU 
_
e  Mmmarlowe@xpert.stx.com, jlander, mlander, holland, gawne, ehm@space.mit.edu, �⪉ �� UU  
_
Du Kcui@space.mit.edu, and arots@rosserv informing them that new products were  by �� UU @
_
UU 7made, and giving them the product�s location on xprod.
og��� �� UU `
a#pr Delivery to the MOC
UU�� �� UU `
c
y  IDAPs get delivered to the MOC via the closed net. It�s a 4-step process.
 .�� �� UU  
b
in Kgo to $SOCOPS/command/staging and change the protection of the RTS-Matched  -w:�� �� UU@
b
sc DAP to world-accessible.
a        d         ��}�              e  y-        ��            io   i 6   h�q�  V1     ��~�    }        W  re 6   h�q�  V1E��  s    (                  (n to the        �  nt       ( �� �� UU  
B
�� UPlay around in Needle, shuffling things and adjusting times (see the separate Needle l �� �� UU  
B
,  Udocumentation for more help). When you finish, save it and print-and-select it, then . �� �� UU @
B
as exit.
 -�� �� UU `
I

_ GLet mp_run
 finish processing, then go into command generation.
.mi K�� �� UU `
K#UU *Adding a target into an existing schedule
 [�� �� UU `
L
th =This is really just a minor variant on the above. As always,
t m�� �� UU `
N
lo 2Invoke mp_run
 and have it call up Needle.
 |�� �� UU  
O
�� OOutside of Needle, check if a proposal file already exists in $SOCOPS/planning/ces ��� �� UU  
O

b Qproposed. If one already exists for an AO-1 accepted target, or for an AO-1 TOO, w ��� �� UU  
O
sc Vyou are all set. If one does not exist, you must make one. For an additional pointing  ��� �� UU 
O
 i Wof an already-schedule observation, you can use the cloneObs
 program to make a    ��� �� UU @
O
   quick clone. Usage is simply:
 Ī� �� UU `
D&in DcloneObs -primary original& -clone new& [-ratio <XX>]
 l ܪ� �� UU 
E
,  ]where the ratio
 is an optional variable, and is the amount of exposure time that the � 誜 �� UU @
E
.
 Oclone should start with, as a percentage of the original ObsID�s exposure time
gen �� �� UU  
V
�� \For a totally new pointing, the easiest approach is to clone an existing pointing as above, us�� �� UU  
V
on Sthen edit the file in $SOCOPS/planning/proposed and re-register it with the system up �� �� UU@
V
UU /(using mp_createOb
, described earlier.
fil*�� �� UU  
)
n  SIn Needle, choose the �add target� option, and enter the new ObsID into the window  ac6�� �� UU@
)
fo Zthat mp_run
 was called from, followed by a yes to verify that you want to add it.
E�� �� UU 
R
on UPlay around in Needle, shuffling things and adjusting times (see the separate Needle Q�� �� UU  
R
gr Tdocumentation for more help). When you finish, �save� it and �print-and-select� it, in]�� �� UU @
R
y  then exit.
&l�� �� UU `
P
  GLet mp_run
 finish processing, then go into command generation.
nal��� �� UU `
J#th Scheduling Constraints
 th��� �� UU  
Y
UU VWhen adjusting schedule times in Needle, there are several things to be aware of. The ��� �� UU  
Y

V [most subtle is that Needle calculates slew times for each scheduled target as it is placed ove��� �� UU  
Y

V Won the schedule. If you pull a target from the schedule, Needle will re-calculate slew up ��� �� UU  
Y
UU Ttimes and possible have to pull subsequent targets because the slew times will have hoʪ� �� UU  
Y
t� Vchanged. So, when you yank target X, it will look at X-1 and X+1, realize the slew to ֪� �� UU  
Y
ro UX+1 takes longer and hence X+1 won�t fit into the same space; so it pulls X+1, which n⪋ �� UU  
Y
li [perturbs the schedule, and so on. In practical terms, pulling a target may make several of or  �� UU  
Y
ou Sthe subsequent targets also pop out of the schedule, until such time as it finds a l����� �� UU  
Y
   Wmaneuver that�s the same length or shorter than the previously calculated one. This is #th�� �� UU  
Y
ra Y(believe it or not) intentional, though inelegant. The easiest way to prevent this is to i�� �� UU  
Y
.  Vsubtract 1ksec of time from the target you are pulling, then reschedule the following �� �� UU  
Y
 i ^target that 1ksec earlier. It is then safe to pull your target, since you�ve given a maneuver *�� �� UU  
Y
e  Tleeway. By the same token, inserting a new target into a gap will result in all the se6�� �� UU  
Y
ll Vmaneuvers being recalculated on the fly, possibly popping targets. You get used to it B�� �� UU @
Y
e  after a while.
UU        d         ��              n   f        ��             i  pu 6   h�q�  V1     ���            r   t 6   h�q�  V1 ���  .    
                  
ing a ta        �  se       
 �� �� UU `
H&
Y %chmod 777 X1996ddd_vv_S_WxxxVyyM.DAP
u �� �� UU `
e
 u (sanction it by running cg_sanction.
UU 6�� �� UU `
G&   'cg_sanction X1996ddd_vv_S_WxxxVyyM.DAP
 pr N�� �� UU  
f
d  RBecome socops and copy the file to /socops/command/mocdeliver (note the use of an  Z�� �� UU  
f
ay Uabsolute path here). This is the cross-mounted directory to the MOC. Exit so you are u f�� �� UU @
f
 r no longer �socops�.
g  ~�� �� UU  
Q& i 
su socops	 ��� �� UU  
Q&t  9cp X1996ddd_vv_S_WxxxVyyM.DAP /socops/command/mocdeliver	u ��� �� UU @
Q&
Y exit
e ��� �� UU `
g
to @Write down the name of the DAP delivered in the Green Log book.�� H   h�q�  V1     �6��    {        t   o H   h�q�  V1        l                  ����||
�  �� 6   h�q�  V1     �7��    }              6   h�q�  V1        l                   ����~~
�   pu 6   h�q�  V1     �8��            r   t 6   h�q�  V1        l                   ���  ��
�      H   h�q�  V1     �\��  �y           7  H   h�q�  V1        l                  ��    ��
�   by H   h�q�  V1     �]��    y        G     H   h�q�  V1 	�� _v                      �� UU        �  me        �� �� UUe
4
to o H   )R�           �^����y           �� H   )R�       Uab lu H   )R.   )R        d         �o��              O   E        ��!            UU  
f �   c  _   �       �p��  ��        Q   i �   c  _   �          l                  ��  ���
�  AP  �   c  _   �       �q��    �        Q  
Y �   c  _   �   	�� 
g                     e of the        �   i        �� �� UUe
7
   q        d         ����                 �q        ��                   l     �   �       �A��  ��           �7 l     �   �         l                   ��    ��
�      l     �   �       �B��  ��           �8 l     �   �         l                  ��    ��
�      H   h�q�  V1     �C��    �           �\ H   h�q�  V1        l                  ��  ���
�             d         �H��                 �]        ��                �q 6   h�q�  V1     �I��  ��        �  UU 6   h�q�  V1        l                   ��    ��
�      6   h�q�  V1     �J��    �              6   h�q�  V1 	�� R                     ��            �   E        �� �� UUe
i
UU f H   .%�   1&     �_����y        Q   i H   .%�   1&        l                  ��    ��
�  AP  H   .%�   1&     �`��    y        Q  
Y H   .%�   1& 	   
g                     e of the        �   i                 `
6   q H   A�           �a����y           �q H   A�              H   A.   A H  ̬q�           �b����y              H  ̬q�       l      H  ̬q.  ̬q H  h  �  X     �r�����        �  � H  h  �  X        l                  ���  ��
�  �� H  h  �  X     �s��    �        �     H  h  �  X     �q                                        �  �         H   )R�           �t�����           �] H   )R�              H   )R.   )R H   0$ ~          �u�����           �q H   0$ ~             l                  ��    ��
�  h�q        d         ����                 �q        ��                   H   ~  �   6       ����  ��        �  �� H   ~  �   6          l                   ��    ��
�    i H   ~  �   6       ����    �           �� H   ~  �   6   -�� %                     y              �  %        �� �� UUe
l
   "<$paranum><$paratext><$pagenum>
 ��      e
m	   "<$paranum><$paratext><$pagenum>
 )US 	UT ��e
n'  "<$paranum><$paratext><$pagenum> H   nVm �C� ^�     �������              H   nVm �C� ^� H   w� H   w�%  Table of Contents Specification�r H  R�   -       �������              H  R�   -          l                   ��    ��
�  �s H  R�   -       ����    �           �q H  R�   -   	��                                        �            �� �� UUe
o
  "<$paranum><$paratext><$pagenum> H   �PH ��� ^�     �������              H   �PH ��� ^� H   �� H   ��%  List of Figures Specification H   ~  �   �       ����  ��             H   ~  �   �          l                  ��    ��
�  �� H   ~  �   �       ����    �              H   ~  �   �   O                           6              �                    e
:   1, 2�3
          e
A  $<$symbols><$numerics><$alphabetics>
��         e
Cra 	Level3IX
t &        e
q�� 	Level2IX
m 0        e
r$p 	Level1IX
p C        e
s�� LSymbols[\ ];Numerics[0];A;B;C;D;E;F;G;H;I;J;K;L;M;N;O;P;Q;R;S;T;U;V;W;X;Y;Z
   L        e
tVm <$pagenum>w� H   lA� mN� ^�     �����  �        o  �r H   lA� mN� ^� H   u   H   u  %  Index Specification�          d         :)��              �           ��            ��  � l     �   �       :*��    �             l     �   �                                  ��        �                   d
<	   "<$paranum><$paratext><$pagenum> l  ao ��, `@     :+���  �        �     l  ao ��, `@ l   � l   �  List of Figures Specification�        d         :,��                           ��            �    l     �   �       :-��    �             l     �   �                                  6          �                   d
=	
: "<$paranum><$paratext><$pagenum> l  ao �|� `@     :.���  �              l  ao �|� `@ l   � l   �  List of Tables Specification          d         :/��              y  ol        ��            E;  G; H   h�q�  V1     :0��    �              H   h�q�  V1N��  m>   &                  & ����        �  o       &          `
d   Replan
%  )�� �� UU `
;#io Leisurely Replans
 9�� �� UU  
>
   VAlterations to an existing plan take three forms: changing the instrument modes for a  E�� �� UU  
>
   Ygiven observation, actually adjusting the scheduled observations, or adding a new target a Q�� �� UU @
>
$p @into an existing schedule. The procedure for these are similar.
�, i�� �� UU  
?
 � cIn all cases, you will want to run the mp_run
 routine, as it was last run according to the     u�� �� UU 
?
   TGreen Log book. Type this verbatim on the closed net machine of your choice (xmp is   ��� �� UU 
?
  Yusually a better choice, as it has more memory). Note that mp_run
 has copious on-. ��� �� UU 
?
  Qscreen output, and this must be proofed for errors. The important points are: no   ��� �� UU 
?
   Tmessages appear that say �error� or �warning�; no �core dump� events happen; Needle    ��� �� UU  
?
   [acknowledges that it has saved and printed the output when you exit it; and after the dots lan ��� �� UU 
?

; Vin the middle, a list of files made is reported which includes an insert file and dcf- ��� �� UU 
?
he ^making script. The final step of mp_run
 is to ask you whether it should run the newly- ɪ� �� UU 
?
   Wmade dcf-making script, and you should answer �yes� and hope for no errors (this being ese ժ� �� UU@
?
�� &the last hurdle of mission planning.)
 ��� �� UU 
5
 SThere are support materials you will want to use for replans that involve changing en  ��� �� UU 
5
s  Kscheduled times. In $SOCOPS/planning/WeekXXX (where XXX is the same as the 
 �� �� UU 
5
 c Vweek number given to mp_run
, from the Green Log book), there is a file called �� �� UU 
5
t, Uweek0YY.why, which gives a summary of time constraints for the week. There is also a  �� �� UU  
5
th Xfile called weekXXX.status, which lists the hand-edits that must be done for that week. ac)�� �� UU 
5
 h ^These hand-edits will also be presented to you on-screen by mp_run
 at the appropriate 5�� �� UU@
5
t  -time, and can be worried about at that time.
eS�� �� UU`
+#�� &Changing instrument modes in the plan
c�� �� UU `
.
ru WIf you are only changing the instrument modes for a proposal, the procedure is simple:
 scu�� �� UU`
/
ld Invoke mp_run
.
r n��� �� UU 
0
ng RIn a separate window, edit the proposal file in $SOCOPS/planning/proposed so that ��� �� UU  
0
t  Iis contains the correct modes. Register this as a system object by using ���� �� UU @
0du mp_createOb
:
P��� �� UU`
1& ( mp_createOb -f <ObsID>
the̪� �� UU  
2

5 [In mp_run
, call up Needle and immediately save/print-and-select, then exit (you do ��ت� �� UU  
2
we Tthis step because, although you are not changing the schedule, you need to tell the ��䪛 �� UU 
2
fi Venvironment which schedule you are operating on. It�s safest just to do this re-save, � �� UU 
2
 h Rbut the alternative is to read the file $SPIKEROOT/sched/saved-scheds.current and ��� �� UU@
2
�� Gverify that the proper week�s schedule is the one marked as SELECTED).
`
+�� �� UU`
3
tr GLet mp_run
 finish processing, then go into command generation.
e i)�� �� UU`
*#r  Shuffling a schedule
r9�� �� UU`
,
�� VOften you will have to move things around in an existing schedule. Again, start with:
K�� �� UU `
-
fi 2Invoke mp_run
 and have it call up Needle.
 6   )R�           �K�����        g  te 6   )R�       m o ec 6   )R   )R 6   .%�   1&     �L�����        �  UU 6   .%�   1&        l                   ��    ��
�   6   .%�   1&     �M��    �           ve 6   .%�   1& 	   n                       UU  
2        �  p                  `
jno h 6   A�           �N�����        �  �� 6   A�       Ven ro 6   A   A 6  ̬q�           �O�����            d 6  ̬q�       � �� 6  ̬q  ̬q 6  �
=�   K�     �P���  �        E  OT 6  �
=�   K�        l                   ��    ��
�  tha 6  �
=�   K�     �Q��    �        k   a 6  �
=�   K� 	   ��                     _run
        �  si                 h
k#en +�16� $�Replan� sc H  �
=�   K�     �c���  y          av H  �
=�   K�        l                  ��    ��
�   wi H  �
=�   K�     �d��    y            H  �
=�   K� 	   ed                      �K��        �  g                l
@ec -�Running H/F 2�#�#� � H   0$ ~          �v��    �              H   0$ ~      	   �                          �M        �                    m
8   �October 18, 1996�U H  [R�           �w�����        j  no H  [R�              H  [R.  [R H  ̬q�           �x�����             H  ̬q�              H  ̬q.  ̬q H  �
=�   K�     �y���  �        q     H  �
=�   K�        l                  ��    ��
�  �   H  �
=�   K�     �z��    �        �    H  �
=�   K� 	                             a 6          �                    m
9#  �#� �  H   �A� 3K� ^�     ����  �z           H   �A� 3K� ^� H   ��� H   ���%  FootnoteK� H   ���-  ��     ������z           K�        ��                 Footnote� �   �� �           ����    �              �   �� �       �   K� �   ��   �� H  ���         ������z                    ��              Ru 1Heading Rule �    �5_           ����  ��           �v �    �5_             �    �5�    �5      �5 ~           �����  �                   �5 ~                  �5 ~    �5 H   ��m SNd ^�     ������z              H   ��m SNd ^� H   ��� H   ���%  1Heading Rule  H  0�� �   
      ������z        �  �        ��                 Chapter Rule       	   �           ����    �                  	   �       �
=        	   �   	   H  X5 J�� ^�     ������z        =     H  X5 J�� ^� H  (w H  (w%  Chapter RuleK� H  c���   ��     ������z                     ��                 	Table Top �   ��_           ����  ��        �  � �   ��_       H   A� �   ���   ��     �� ~           �����  �           ��     �� ~          K�     �� ~   �� D�R�m 8� ^�     ������z           �� D�R�m 8� ^� D�[�� D�[��%  	Table Top� H   � 
,  ��     ������z        �  �        ��              � TableFootnoteu ��� �D �          ����    �        �  � ��� �D �      �   �5 ��� �D   �D H   ��] R�b ^�     ������z        �     H   ��] R�b ^� H   ��� H   ���%  TableFootnote  H  �  z   �     �����z                    ��              �� 	classname      {�         ���    �        �  �      {�                 {�   Q  VCz     N1     �����z        �  � Q  VCz     N1 Q  \�
 Q  \�
&   
= J 
��� =U� ^�     �����z        �  � J 
��� =U� ^� J 
�� J 
��%  	classname  H  ���z   �     �����z           ��        ��                 
1operation      z�         ���    �           ��      z�      � �      z�   J���� =Op ^�     �����z           �� J���� =Op ^� J�ʀ J�ʀ%  
1operation J 
�  w�
 ��     ������z        �  �                          � classname.head L� �A� \�< ^�     �����  z        �  � L� �A� \�< ^� L� �  L� � %  classname.head H  �R�   6       �������              H  �R�   6          l                   ��    ��
�  R�b H  �R�   6       ����    �        ]  �b H  �R�   6   	��                       z   �        �  �        �� �� UUe
p
� "<$paranum><$paratext><$pagenum> H  �PH �Jh ^�     �����  �             H  �PH �Jh ^� H  �� H  ��%  List of Tables Specification%  �d              � �
   Left   d           !y ��   Rightz  d              z ^�   	Reference�  d           �      TOC   ��d           �      IX  d           � on   LOF  �d             �      LOT�    d              �          d           {          d           }  ^�      �d                
      �
d           !  �      First   �%� X �                       �f    �                            � T         	                          A             R �         
TableTitle   	Table Top    Step Number6 T:TABLE <n+>.\t    .    ��                    �f    �                            �   B                                  A               ��      Bulleted       BulletSymbol B:�\tr   .  �� ��                �f    �                            �                                      A               BulletedCont             .                          �f    �                           �                                     A   	            CellHeading             .                         �f    �                            �           	                          A               CellBody             .  �   �                   �f    �                            �            	                          A   	            header             .                            �f    �                            �   �        	                          A   	            �       > footer left             .    �   �                   �f    �                            �   �        	                          A   	            header             .   �   �                   �f    �                            �   �        	                          A   	           �        footer right             .                          �f    �                            �   H Q                                 A   	            Heading1         H:<n+>.<n=0>   Body .                           �f    �                            �   H Q                                 A   	            Heading2         H:<n>.<n+>   Body .                         �f    �                            �   H Q                                 A   	            Heading3         H:<n>.<n>.<n+>   Body .                        �f    �                            �     T                                A   	            HeadingRunIn           Body .  ��                    �f    �                            �   S                                  A                ��  f    Numbered         	S:<n+>.\t    .                         �f    �                            �     P                                A               ReportTitle�           ReportAuthor .  �   �                  �f    �                            �
   @        
                          A               	FirstBody           Body .                          �f    �                            � H Q                                 A   	            Heading1         H:<n+>.<n=0>   Body .    ��                    �f    �                            �   S E                                 A                ��       	Numbered1�         
S:<n=1>.\t Numbered .   �� ��                �f    �                            �                                      A               NumberedCont             .                          �f    �                            �     P                                 A   	            ReportAuthor           Heading1 .                       �f    �                            �	                                    A               Body             .                          �f    �                            �            	                          A   	            header first             .                          �f    �                            �            	                          A   	            footer first             .  �   _R               �f    �                            �# H Q        
                          A   	          Nu ~    �    �    :  1 2Heading         H:\t<n>.<n+>\t 	FirstBody  .    ��                    �f    �                            � B                                  A                ��       Bulleted       BulletSymbol B:�\t    .  ��                    �f    �                            �	 S                                  A                ��     � Numbered         	S:<n+>.\t�   .                        �f    �                            �	                                    A               Body             .  �   �                 �f    �                            �&   P        
                          A               code           	FirstBody  .    �   �                 �f    �                            �     P        
                          A               code           	FirstBody .  H                                                         �	                                     A             �� H        �      .f 	FigureLOF              .  H                                                         �	                                     A             .  H        �      .  TableTitleLOT              .  �� ��                �f    �                            �	                                    A               NumberedCont             .  �   �                 �f    �                            �
            
                          A               Body             .  �� $                  �f    �                            �	                                    A               NumberedCont             .  ��                   �f    �                            �	                                    A               NumberedCont             .  �       ;R           �f    �                            �   H Q                                  A   	          Fi ~         �         1Heading     1Heading Rule   H:\t<n+>.<n=0>\t 	FirstBody  . 	 ��� �                  �f    �                            �   S A        
                          A                ���       1Step        Step Number  
S:<n=1>.\t Step .   �   _R               �f    �                            �   H Q        
                          A   	             ~         �    
   2Heading         H:\t<n>.<n+>\t 	FirstBodyA .   �   _R                �f    �                            �   H Q        	                          A   	             ~         �        3Heading         H:\t<n>.<n>.<n+>\t 	FirstBody  .            Z   
           �f    �                            �     @                                  A   	            Author           Purpose .    �   �                 �f    �                            �              
                          A               Body             .  �   �   ;S�           �f    �                            �     P                                 A   	             ~         �         title.processname      1Heading Rule      	FirstBody .  ��� ���                �f    �                            �              
                          A               CStep              .    ��� �                  �f    �                            �             
                          A                ���       Bullet       Bullet Symbol  �\t    .    ��� ���               �f    �                            �             
                          A               CBullet              .                         �f    �                            �             	                          A               CellBody             .                          �f    �                           �                                       A   	            CellHeading              .  �   ?                 �f    �                            �   c A        
                          A             di ~         �    B   comment�         c:\tComment <n+>\t Body .  �   �       
           �f    �                            �   E �       
                          A               Equation       Equation Number  E:(EQ <n+>)�   .  �   �                 �f    �                            �     �        	                          A               Extract              .             �����       �f    �                            �     �                                  A               
fig.bubble             .        ��                 �f    �                            �     �                                  A               fig.classattr             .                            �f    �                            �     �                                  A   	            fig.classname    classname.head 	classname      fig.classattr  .      ��                 �f    �                            �     �                                  A               fig.classoper             .        ��                 �f    �                            �     �                                  A               fig.classoper1   
1operation       fig.classoper  .                            �f    �                            �     �                                  A               fig.dataflow             .                          �f    �                            �     �                                  A               fig.datastore    	classname  	classname       .   �                       �f    �                            �   F A        	                          A                �         Figure   	Table Top    Step Number  F:FIGURE <n+>.\t Body .    �   �                  �f    �                            �     @        
                          A               	FirstBody           Body .    �   �                  �f    �                            �              	                          A               Footnote             .  �   ?                 �f    �                            �   o A        
                          A                ~         �         	openIssue          o:\tOpen Issue <n+>\t  Body .            �   .   �       �f    �                            �     @        	                          A   	            Purpose            	FirstBody  .   �   ?                  �f    �                            �     @        
                          A                ~        �         requirement            response .    ��� �                  �f    �                            �   S         
                          A                ���       Step       Step Number
 	S:<n+>.\t    .  �  �                �f    �                            �             	                          A               TableFootnote	             .    �                       �f    �                            �   T         	                          A               �         
TableTitle   	Table Top    Step Number T:TABLE <n+>.\t    .            l               �f    �                            �     @                 ���=            A               Title	     Chapter Rule     Author .  �   �   ;S�           �f    �                            �     P                                 A   	             ~         �         title.classname      1Heading Rule     	FirstBody . ir �   ?                  �f    �                            �             
                          A                ~        �         
compliance         \tCompliance\t   . be �             ��       �f    �                            �     P        
                          A   	            marginHeading            marginDummy  .    �   �      ��           �f    �                            �     P        
                          A               marginDummy            code .  ��� ���                 �f    �                            �
           
                          A               ���       ���  =    3HeadingTOC              .   ��� �                   �f    �                            �	                                     A                �         ���     2HeadingTOC             .    �   c                  �f    �                             '                                     A               ~        �         1HeadingTOC              .   �   c                   �f    �                            
           
                          A                �        	FigureLOF              .  �   c                   �f    �                            
           
                          A                �         TableTitleLOT              .                          �f    �                                       	                          A               SeparatorsIX             .                          �f    �                                       	                          A               SortOrderIX              .  $                      �f    �                                       	                          A               Level3IX             .     	                   �f    �                                       	                          A               Level2IX             .                         �f    �                                       	                          A               Level1IX             .              	            �f    �                                       	                          A   	            GroupTitlesIX�             . eT                         �f    �                            	           	                          A               IndexIX             .  �       ;R           �f    �                            
 H Q                                  A   	             ~       �         1Heading     1Heading Rule   H:\t<n+>.<n=0>\t 	FirstBody . � ��� �                  �f    �                             S A        
                          A                ���       1Step        Step Number  
S:<n=1>.\t Step .   ��� �                  �f    �                             S         
                          A                ���       Step       Step Number	 	S:<n+>.\t    .  ��� ���                �f    �                            
            
                          A               CStep	             .    ��� �                  �f    �                            (           
                          A               ���       Bullet       Bullet SymbolA �\t   . 	)                     �ڝ�	  A            
                  �ڝ�
  A                              )��  A   	                       �� �`f  A   	       BulletSymbol 	                  �[  A   	       Step Number                    �[  A   	         
                  )��  A   	                           �`f  A   	       BulletSymbol 
                  tu�  A          Emphasis 	                  �[  A   	                       �� �`f  A   	       Bullet Symbol                    �[  A   	         
                  i_mF  A          Emphasis 	                  �ڝ�  A            
                  i_mF  A          
typewriterte               �� yc�>  A          Callout� 
               �             	       Emphasis               �� �[  A   	       Equation Numberu 
              �� )��  A   	       Run-In Heading   	              �� �[  A   	       Step Number  
                �             	      	Subscript	 
                �              	      Superscriptl 
              �3� i_mF!  A          
typewriterep                 �     "                EquationVariables    
                  �[#  A   	         	                  �[$  A   		                           �[%  A    	         
                  i_mF&  A                              �ڝ�'  A                              �`f(  A   	       Bullet Symbol	�-�              �       A                  �      A                   �      A         @         �       A       F           �     A    Zt  @         �     A    Z   @         �     A  Z            �     A  Z   �               A                        A  Z                   A                         A                        A  Z                   A        ���   �     �    A    Thin         �    A	 Medium  bs  �    �    A    Double           �    A   Thicke  cr  @     �    A    	Very Thin   	�	�	�  5E6  
       ���                      5                      ��AAA              6  ���     6  ���      6  ���      6  ���  Format B         �                       8                        �AAA             6  ���      6  ���      6  ���      6  ���   Format A  �	�  	a	q	a  ���    �Co   Comment                                 UeZ  �UU
���A     �hV
���      �kW
���   @  �}X
���    A ��Y
���       �X\
���    AQI                d     A  Black                     T!B   White          d   d        AC   Red       d   d            �D   Green      d       d        	E   Blue       d                F   Cyan               d        GA Magenta           d             	H   Yellow                                                                   	   	Helvetica       Times-Roman�       Helvetica-Bold       
Times-Bold       Courier        Times-Italic       Courier-Bold       Courier-Oblique       Helvetica-BoldObliqueA        Courier   Times�  	Helvetica     Regular     Regular  	 Bold    Regular   Oblique   Italic                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      ��W�G)�Ϻߜ*~&���V��R��-mH
h}�Pr+��B&�6�]p$Gd�i��(���v~L��U��G���k�{�i��{�ϟʨ�ӧr��f܊�g�V��u��'����Ӭ�]$~�����M+*�����5�������Ր�/�AoG�q	�9�BR��\�ɫ�&w��'�;A�>    
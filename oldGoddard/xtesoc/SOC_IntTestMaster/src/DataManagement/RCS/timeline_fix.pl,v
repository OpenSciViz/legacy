head	1.2;
access
	gromeo;
symbols;
locks; strict;
comment	@# @;


1.2
date	2000.12.07.15.20.18;	author jonv;	state Exp;
branches;
next	1.1;

1.1
date	2000.06.09.19.09.27;	author jonv;	state Exp;
branches;
next	;


desc
@removes fake in|out_saa events at week boundaries
from time line files
@


1.2
log
@added another check to make sure the HR has at least one obsid!
@
text
@#!/usr/local/bin/perl -w

# ===========================================================================
# Filename    : timeline_fix.pl
# Subsystem   : Data Management
# Programmer  : Jon Vandegriff, Raytheon ITSS
# Description : removes fake in|out_saa lines from week boundary observations.
#               Mission Planning needs to put "in_saa" 
#               commands at the start of every human readable and
#               corresponding out_saa commands at the end
#               because it requires pairing of these items (for some reason??)
#               Thus the out_saa at 00:00 at the week end is fake if there
#               is a corresponding fake "in_saa" at 00:00 at the start of 
#               the next week.
#               This program removes these fake, cancelling in|out saa
#               events from the time line file.
#               In case you don't know, a time line file is just the obscat
#               terminology for a human readable file, with the exception
#               that if the day of interest is a week boundary (either
#               Thursday or Friday) then the time line will consist of
#               2 human readables concatenated together.  These "double
#               time lines" are the only ones this program is concerned with.
#               Note that all the modifications happen only in the timeline
#               file and not in the human readable itself.
#
#               
# RCS: $Id: timeline_fix.pl,v 1.1 2000/06/09 19:09:27 jonv Exp $
# ===========================================================================

use FileHandle;

use HRObservation;

use strict;

$|=1; # turn on output autoflush for STDOUT

{
  my( $tl_file, $act, $verb ) = parse_args( @@ARGV );

  my $list = HRObservation->get_observation_list( $tl_file );

  if( !$list or (scalar(@@$list) ==0) ) {
    die "$0: ERROR - can't get list of Observations from file $tl_file\n";
  }

  my $index = get_week_boundary_index( $list, $verb );
     # returns undef if there is just one week in the timeline file

  if (defined($index)) {

    warn_if_first_obs_in_week_starts_late($list, $index);

    my $saa_modified = 
     remove_canceling_commands_at_week_boundary($index, $list, "saa", $verb );

    #my $occult_modified = 
    #remove_canceling_commands_at_week_boundary($index, $list, "occult",$verb);

    if ($saa_modified ) {# or $occult_modified) {

      if( $verb ) {
	print "Here are the new events without the fake commands:\n";
	show_obs($list->[$index]);
	show_obs($list->[$index+1]);
      }

      write_new_TL_file( $tl_file, $list, $act, $verb );

    } else {
      print "$0: no need to modify $tl_file\n" if $verb;
    }
  } else {
    print "No need to check single week file $tl_file ".
      "for week boundary problems\n" if $verb;
  }

}


sub show_obs {
  my( $obs ) = @@_;
  my $fh = new FileHandle ">&STDOUT";
  HRObservation::print_boundary_line(0, $obs->start_utc, $obs->start_met,
				     $obs->obsid, $fh);
  HRObservation::print_group("EVENTS", $obs->EVENTS, $fh);
  HRObservation::print_boundary_line(1, $obs->stop_utc, $obs->stop_met,
				     $obs->obsid, $fh);
}


sub parse_args {

  my $tl_file = undef;
  my $act          = 1;
  my $verb         = 0;


  my $arg;

  while( defined($arg = shift) ) {
    if ($arg eq "-tlfile") {
      $tl_file = shift;
    } elsif ($arg eq "-n") {
      $act = 0;
    } elsif ($arg eq "-v") {
      $verb = 1;
    } else {
      warn "$0: invalid argument \"$arg\"\n";
      usage();
      exit(1);
    }
  }

  unless($tl_file) {
    warn "$0: ERROR - No Time Line filename specified on command line.\n";
    usage();
    exit(1);
  }

  return( $tl_file, $act, $verb );

}


sub usage {

  warn "usage for $0:\n".
       "-tlfile <filename of obscat TimeLine file>  [-v] [-n]\n";
}






sub get_week_boundary_index {
  my( $list, $verb ) = @@_;

  my $index = undef;

  my $N = scalar(@@$list);

  my $starting_week = $list->[0]->week();
  my $ending_week   = $list->[ $N-1 ]->week();

  if ($starting_week != $ending_week) {

    my $week_number = undef;
    if( $starting_week != $ending_week ) {
      my $i;
      for( $i=0; $i<$N-1; $i++) {
	my $this_week = $list->[$i]->week();
	if( !defined($week_number) ) {
	  $week_number = $this_week;
	} elsif ($this_week != $week_number) {
	  $index = $i-1;
	  last;
	}
      }
    }

    if(!defined($index) ) {
      die "$0: ERROR - internal error: no week transition found in ".
	  "time line file\n";
    }
  }

  return $index;

}


#sub get_obs_week_num {
#  my( $obs ) = @@_;
#
#  my $mid_obs_met  = ( $obs->start_met() + $obs->end_met() ) / 2.0;
#  my $cmd          = "time_converter -MET_to_week $mid_obs_met";
#  my $mid_obs_week = `$cmd`;
#  chomp $mid_obs_week;
#
#  unless( $mid_obs_week =~/^\d+$/ ) {
#    die "$0: ERROR - Bad result from time_converter\n".
#        "\"$cmd\" returned \"$mid_obs_week\"\n";
#  }
#
#  return $mid_obs_week;
#}


sub warn_if_first_obs_in_week_starts_late {
  my( $list, $index ) = @@_;

  my $first_obs_of_week = $list->[$index+1];
  unless( $first_obs_of_week->start_utc =~ /00:00:00$/ ) {
    warn "$0: WARNING - The first obs of week ".
      $first_obs_of_week->week()." starts at ".
      $first_obs_of_week->start_utc()."\n";
  }

}



sub remove_canceling_commands_at_week_boundary {
  my( $index, $list, $string, $verb ) = @@_;

  my $modified = 0;

  my $obs1 = $list->[$index];
  my $obs2 = $list->[$index+1];

  # Example for saa commanding:
  # if obs1 has an out_saa at 00:00 and obs2 has an in_saa at 00:00
  # then remove both the out_saa and the in_saa

  if( grep /out_$string\s+00:00:00/, @@{$obs1->EVENTS()}  and
      grep /in_$string\s+00:00:00/,  @@{$obs2->EVENTS()} )
  {
    $modified = 1;

    $obs1->EVENTS( [ grep !/out_$string\s+00:00:00/, @@{$obs1->EVENTS()} ] );
    $obs2->EVENTS( [ grep !/in_$string\s+00:00:00/,  @@{$obs2->EVENTS()} ] );

    print "Extra $string commands removed\n" if $verb;

  }

  return $modified;

}


sub write_new_TL_file {
  my( $tl_file, $list, $act, $verb ) = @@_;

  if( $act ) {
    my $fh = new FileHandle(">$tl_file");
    if( $fh ) {
      foreach my $obs (@@$list) {
	$obs->print($fh);
	print $fh "\n\n\n";
      }
      $fh->close();
      print "Wrote out modified timeline to \"$tl_file\"\n" if $verb;
    } else {
      warn "$0: ERROR could not open \"$tl_file\" for writing of ".
           "modified time line\n";
    }
  } else {
    print "Would write modified time line to \"$tl_file\"\n" if $verb;
  }
}

@


1.1
log
@Initial revision
@
text
@d27 1
a27 1
# RCS: $Id$
d43 1
a43 1
  if(!$list) {
d254 1
@

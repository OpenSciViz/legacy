head	5.1;
access
	rbarnett
	soccm
	jonv
	gromeo;
symbols
	NEW_name:5.1
	RDB:5.1
	Build5_1:5.1
	Build4_3_1:4.1
	Build4:3.8
	Build3_3:3.5
	Build3_2:3.4
	Build3_1:3.2
	Build3:3.1
	Build2:2.3
	Build1:2.0;
locks; strict;
comment	@// @;


5.1
date	96.05.07.16.49.39;	author rbarnett;	state Exp;
branches;
next	4.1;

4.1
date	95.05.19.20.27.42;	author rbarnett;	state Exp;
branches;
next	3.8;

3.8
date	95.01.23.14.54.30;	author rbarnett;	state Exp;
branches;
next	3.7;

3.7
date	94.12.02.16.56.57;	author rbarnett;	state Exp;
branches;
next	3.6;

3.6
date	94.11.30.21.10.41;	author rbarnett;	state Exp;
branches;
next	3.5;

3.5
date	94.11.02.17.12.36;	author rbarnett;	state Exp;
branches;
next	3.4;

3.4
date	94.07.26.15.26.43;	author rbarnett;	state Exp;
branches;
next	3.3;

3.3
date	94.07.21.22.28.04;	author rbarnett;	state Exp;
branches;
next	3.2;

3.2
date	94.06.28.15.23.35;	author rbarnett;	state Exp;
branches;
next	3.1;

3.1
date	94.04.26.20.18.30;	author rbarnett;	state Exp;
branches;
next	3.0;

3.0
date	94.03.18.23.09.43;	author rbarnett;	state Exp;
branches;
next	2.6;

2.6
date	94.02.22.21.21.09;	author rbarnett;	state Exp;
branches;
next	2.5;

2.5
date	94.01.13.21.12.04;	author rbarnett;	state Exp;
branches;
next	2.4;

2.4
date	93.12.16.22.26.55;	author rbarnett;	state Exp;
branches;
next	2.3;

2.3
date	93.11.03.21.34.49;	author rbarnett;	state Release;
branches;
next	2.2;

2.2
date	93.10.20.21.46.48;	author rbarnett;	state Exp;
branches;
next	2.1;

2.1
date	93.08.06.21.47.11;	author rbarnett;	state Exp;
branches;
next	2.0;

2.0
date	93.06.30.20.32.50;	author rbarnett;	state Exp;
branches;
next	1.3;

1.3
date	93.06.23.15.03.04;	author rbarnett;	state Exp;
branches;
next	1.2;

1.2
date	93.06.18.15.31.42;	author rbarnett;	state Exp;
branches;
next	1.1;

1.1
date	93.06.10.16.33.27;	author rbarnett;	state Exp;
branches;
next	;


desc
@Creation
@


5.1
log
@Document update
@
text
@// ========================================================================
// File Name   : Descriptor.h
// Subsystem   : Data Management
// Programmer  : Randall D. Barnette, Hughes STX
// Description : Declaration of Descriptor class
//
// RCS: $Id: Descriptor.h,v 4.1 1995/05/19 20:27:42 rbarnett Exp $
//
// .NAME    Descriptor - A Data Management class that provides labels for telemetry data items.
// .LIBRARY dm
// .HEADER  Data Management 
// .INCLUDE Descriptor.h
// .FILE    Descriptor.C
// .VERSION $Revision: 4.1 $
// ========================================================================

// Feature test switches
#ifndef DESCRIPTOR_H
#define DESCRIPTOR_H

// System headers
#include <rw/collstr.h>
#include <rw/ordcltn.h>

// ========================================================================
// Descriptor
// ========================================================================

// .SECTION DESCRIPTION
// The Descriptor class represents an object that provides for a single
// string tag to identify a telemetry data element.
// XTE Descriptors are GOF-defined strings which document the
// identity, source, and storage details of a data item. In general,
// they have the form:
//
// token[value]&token[value]&...&token[value]<<storage_size>>return_size
//
// Typically, Descriptors are used in the Accessor::access() (see Accessor(3))
// method and the DataSet::valueAt() (see DataSet(3)) functions.
// There are a number of specialized Descriptor methods which simplify
// extraction of Descriptor token-value pairs. The equality operators
// (operator==()) are provided for comparisons of Descriptors.
// These methods are provided to the user to isolate the issue of
// Descriptor 'equality'.
// Extraction of the Descriptor character array and subsequent
// string comparison (via strcmp() and its brethren) is not recommended.
// Presently, Descriptors are converted to uppercase and all white space
// is removed. Other syntactic 'equalities' will be added in future
// releases.

// .SECTION SEE ALSO
// xtesocintro(1), Accessor(dm), DataSet(dm), xff(1)

// .SECTION AUTHOR
// Randall D. Barnette
// <rbarnett@@xema.stx.com>
// Hughes STX Corp.

class Descriptor : public RWCollectableString {

public:

//* Constructors

  Descriptor(const char* id) ;
  Descriptor(const RWCString& id) ;

//* Copy constructor

  Descriptor(const Descriptor& rhs) ;

//* Public member functions

  //
  // Get the value for a particular Descriptor Item
  //
  const RWOrdered* getTokenList(const char* ctok) const ;
  const RWCString  getToken(const char* ctok, int start=0) const ;

//% The descriptor uses a fairly regular syntax of token[value]
//% form. These methods provide easy access to token values.

  const RWCString anodePosition(void)const ; // The 'A' Token
  const RWCString channel(void)      const ; // The 'C' Token
  const RWCString detector(void)     const ; // The 'D' Token
  const RWCString element(void)      const ; // The 'E' Token
  const RWCString frequency(void)    const ; // The 'F' Token
  const RWCString group(void)        const ; // The 'G' Token
  const RWCString housekeeping(void) const ; // The 'H' Token
  const RWCString instrument(void)   const ; // The 'I' Token
  const RWCString lag(void)          const ; // The 'L' Token
  const RWCString marker(void)       const ; // The 'M' Token
  const RWCString observatory(void)  const ; // The 'O' Token
  const RWCString phase(void)        const ; // The 'P' Token
  const RWCString status(void)       const ; // The 'S' Token
  const RWCString time(void)         const ; // The 'T' Token
  const RWCString execute(void)      const ; // The 'X' Token
  const RWCString extension(void)    const ; // The 'Z' Token

//% Descriptors may contain a specification for an item's original
//% storage size with the << operator and a specification for output
//% form with the >> operator. These methods return the text of the
//% specification.

  const RWCString inputText(void)    const ; // The << stuff
  const RWCString formatText(void)   const ; // The >> stuff

//% RWCString does not define an operator<< for pointers.
//% The Descriptor class implements it as a friend function.
//%   

  friend ostream& operator<<(ostream& s, const Descriptor* d) ;

//% Friend equality operators provide for comparisons on Descriptors.
//% They can be used to compare Descriptors to strings and RWCStrings
//% that are not necessarily lexically equal.

	friend RWBoolean operator==(const Descriptor& lhs, const char* rhs) ;
	friend RWBoolean operator==(const Descriptor& lhs, const RWCString& rhs) ;
	friend RWBoolean operator==(const Descriptor& lhs, const Descriptor& rhs) ;
	friend RWBoolean operator==(const RWCString&  lhs, const Descriptor& rhs) ;
	friend RWBoolean operator==(const char*       lhs, const Descriptor& rhs) ;

	friend RWBoolean operator!=(const Descriptor& lhs, const char* rhs) ;
	friend RWBoolean operator!=(const Descriptor& lhs, const RWCString& rhs) ;
	friend RWBoolean operator!=(const Descriptor& lhs, const Descriptor& rhs) ;
	friend RWBoolean operator!=(const RWCString&  lhs, const Descriptor& rhs) ;
	friend RWBoolean operator!=(const char*       lhs, const Descriptor& rhs) ;

} ; // End class Descriptor

// for compatability
#define getID data

//
// Inline Functions
//

// Description:
// Return the value of the anode position token, if any.

inline const RWCString Descriptor::anodePosition(void) const
{
  return getToken( "A" ) ;
}

// Description:
// Return the value of the channel token, if any.

inline const RWCString Descriptor::channel(void) const
{
  return getToken( "C" ) ;
}

// Description:
// Return the value of the detector token, if any.

inline const RWCString Descriptor::detector(void) const
{
  return getToken( "D" ) ;
}

// Description:
// Return the value of the element token, if any.

inline const RWCString Descriptor::element(void) const
{
  return getToken( "E" ) ;
}

// Description:
// Return the value of the frequency token, if any.

inline const RWCString Descriptor::frequency(void) const
{
  return getToken( "F" ) ;
}

// Description:
// Return the value of the group token, if any.

inline const RWCString Descriptor::group(void) const
{
  return getToken( "G" ) ;
}

// Description:
// Return the value of the housekeeping token, if any.

inline const RWCString Descriptor::housekeeping(void) const
{
  return getToken( "H" ) ;
}

// Description:
// Return the value of the instrument token, if any.

inline const RWCString Descriptor::instrument(void) const
{
  return getToken( "I" ) ;
}

// Description:
// Return the value of the time lag token, if any.

inline const RWCString Descriptor::lag(void) const
{
  return getToken( "L" ) ;
}

// Description:
// Return the value of the marker token, if any.

inline const RWCString Descriptor::marker(void) const
{
  return getToken( "M" ) ;
}

// Description:
// Return the value of the observatory token, if any.

inline const RWCString Descriptor::observatory(void) const
{
  return getToken( "O" ) ;
}

// Description:
// Return the value of the phase token, if any.

inline const RWCString Descriptor::phase(void) const
{
  return getToken( "P" ) ;
}

// Description:
// Return the value of the status token, if any.

inline const RWCString Descriptor::status(void) const
{
  return getToken( "S" ) ;
}

// Description:
// Return the value of the time token, if any.

inline const RWCString Descriptor::time(void) const
{
	return getToken( "T" ) ;
}

// Description:
// Return the value of the execute token, if any.

inline const RWCString Descriptor::execute(void) const
{
  return getToken( "X" ) ;
}

// Description:
// Return the value of the extension token, if any.

inline const RWCString Descriptor::extension(void) const
{
  return getToken( "Z" ) ;
}

// Description:
// Return FALSE if the operands compare equal. Return TRUE otherwise.

inline RWBoolean operator!=(const Descriptor& lhs, const char* rhs)
{
  return !operator==(lhs, rhs) ;
}

// Description:
// Return FALSE if the operands compare equal. Return TRUE otherwise.

inline RWBoolean operator!=(const Descriptor& lhs, const RWCString& rhs)
{
  return !operator==(lhs, rhs) ;
}

// Description:
// Return FALSE if the operands compare equal. Return TRUE otherwise.

inline RWBoolean operator!=(const Descriptor& lhs, const Descriptor& rhs)
{
  return !operator==(lhs, rhs) ;
}

// Description:
// Return FALSE if the operands compare equal. Return TRUE otherwise.

inline RWBoolean operator!=(const RWCString& lhs, const Descriptor& rhs)
{
  return !operator==(lhs, rhs) ;
}

// Description:
// Return FALSE if the operands compare equal. Return TRUE otherwise.

inline RWBoolean operator!=(const char* lhs, const Descriptor& rhs)
{
  return !operator==(lhs, rhs) ;
}

#endif /* DESCRIPTOR_H */
@


4.1
log
@Update for Build freeze
@
text
@d7 1
a7 1
// RCS: $Id: Descriptor.h,v 3.8 1995/01/23 14:54:30 rbarnett Exp $
d14 1
a14 1
// .VERSION $Revision$
d52 1
a52 1
// xtesocintro(1), Accessor(dm), DataSet(dm)
@


3.8
log
@added != operators.
@
text
@d7 1
a7 1
// RCS: $Id: Descriptor.h,v 3.7 1994/12/02 16:56:57 rbarnett Exp $
d14 1
a14 1
// .AUTHOR  XTE-SOC V4.1
@


3.7
log
@added #define for getID
@
text
@d7 1
a7 1
// RCS: $Id: Descriptor.h,v 3.6 1994/11/30 21:10:41 rbarnett Exp rbarnett $
d14 1
a14 1
// .AUTHOR  XTE-SOC V4.0
d18 2
a19 2
#ifndef Descriptor_h
#define Descriptor_h
d52 1
a52 1
// XTE_intro(1), Accessor(dm), DataSet(dm)
d124 5
d267 41
a307 1
#endif /* Descriptor_h */
@


3.6
log
@Added genman comments an minor fixes for functional tests
@
text
@d7 1
a7 1
// RCS: $Id: Descriptor.h,v 3.5 1994/11/02 17:12:36 rbarnett Exp rbarnett $
d127 3
@


3.5
log
@Added methods to check input and output strings.
@
text
@d7 1
a7 1
// RCS: $Id: Descriptor.h,v 3.4 1994/07/26 15:26:43 rbarnett Exp $
d9 1
a9 1
// .NAME    Descriptor - A DM class that provides labelling for telemetry data
d11 1
a11 1
// .HEADER  Data Management
d14 1
a14 1
// .AUTHOR  XTE-SOC V3.1
d30 20
a49 2
//  Descriptor is an object that provides for a single
//  string tag to identify a telemetry data element.
d52 1
a52 1
// Accessor(dm), PktTlm(pkts)
a74 5
  // Return the description string
  //
  const char* getID(void) const ;

  //
d100 5
d108 5
a112 9
//% These provide for equality checks on Descriptors. They can be used
//% to compare Descriptors to strings and RWCStrings that are not
//% lexically equal.

	friend RWBoolean operator==(const Descriptor&, const char*) ;
	friend RWBoolean operator==(const Descriptor&, const RWCString&) ;
	friend RWBoolean operator==(const Descriptor&, const Descriptor&) ;
	friend RWBoolean operator==(const RWCString& , const Descriptor&) ;
	friend RWBoolean operator==(const char*      , const Descriptor&) ;
d114 9
a122 2
//% RWCString does not define an operator<< for pointers.
//% This one is explicitly for Descriptors.
a123 1
  friend ostream& operator<<(ostream&, const Descriptor*) ;
a130 8
// Description:
// Return the Descriptor as a simple null-terminated string.

inline const char* Descriptor::getID(void) const
{
  return data() ;
}

@


3.4
log
@changed RWCString to collectabler string
@
text
@d7 1
a7 1
// RCS: $Id: Descriptor.h,v 3.3 1994/07/21 22:28:04 rbarnett Exp $
d87 3
@


3.3
log
@Update for Build3.1 and RW6 and genman++
@
text
@d7 1
a7 1
// RCS: $Id: Descriptor.h,v 3.2 1994/06/28 15:23:35 rbarnett Exp $
d22 1
a22 1
#include <rw/cstring.h>
d41 1
a41 1
class Descriptor : public RWCString {
@


3.2
log
@Rogue Wave 6.0 compatability
@
text
@d5 1
a5 1
// Description :
d7 1
a7 1
//  Declaration of Descriptor class
d9 6
a14 5
//  Descriptor is an object that provides for a single
//  string tag to identify a data element
//
// RCS: $Id: Descriptor.h,v 3.1 1994/04/26 20:18:30 rbarnett Exp rbarnett $
//
a24 4
#if RWTOOLS == 0x0520 && ! defined(RWspace)
  #define RWspace unsigned
#endif

d29 11
a39 1
class Descriptor : public RWCollectable {
d41 1
a41 4
	//
	// Rogue Wave declarations.
	// 
  RWDECLARE_COLLECTABLE(Descriptor)
d45 4
a48 4
  //
  // Constructor for RogueWave use only
  //
  Descriptor(void) {}
d50 1
a50 5
  //
  // Constructor for public use. Must provide a string description
  //
  Descriptor(const char*) ;
  Descriptor(const RWCString&) ;
d52 1
a52 4
  //
  // Copy Constructor
  //
  Descriptor(const Descriptor&) ;
d54 1
a54 4
  //
  // Destructor
  //
  virtual ~Descriptor(void) ;
d62 1
a62 13
  // == Operator
  // Two Descriptors are considered "equal" if they have identical
  // ID strings.
  //
  RWBoolean operator==(const Descriptor&) const ;
  RWBoolean operator==(const RWCString& ) const ;
  RWBoolean operator==(const char*      ) const ;
  RWBoolean operator!=(const Descriptor&) const ;
  RWBoolean operator!=(const RWCString& ) const ;
  RWBoolean operator!=(const char*      ) const ;

  //
  // Assignment operators
d64 2
a65 3
  Descriptor& operator=(const Descriptor&) ;
  Descriptor& operator=(const RWCString&) ;
  Descriptor& operator=(const char*) ;
d67 2
a68 12
  //
  // Input/Output operator
  //
  friend istream& operator>>(istream&,       Descriptor&) ;
  friend ostream& operator<<(ostream&, const Descriptor&) ;
  friend ostream& operator<<(ostream&, const Descriptor*) ;

  //
  // Get the value for a particular Descriptor Item
  //
  const RWCString  getToken(const char* , int=0) const ;
  const RWOrdered* getTokenList(const char*) const ;
a69 3
  //
  // Some shortcuts for getToken
  //
d87 9
a95 11
  //
  // Inherited from RWCollectable and reimplemented
  //

  RWspace   binaryStoreSize(void)           const ;
  int       compareTo(const RWCollectable*) const ;
  RWBoolean isEqual(const RWCollectable*)   const ;
  void      saveGuts(RWFile&)               const ;
  void      saveGuts(RWvostream&)           const ;
  void      restoreGuts(RWFile&)                  ;
  void      restoreGuts(RWvistream&)              ;
d97 2
a98 1
private:
d100 1
a100 1
  RWCString theID ;
d107 4
d113 1
a113 1
  return (const char *) theID ;
d116 3
d124 3
d132 3
d140 3
d148 3
d156 3
d164 3
d172 3
d180 3
d188 3
d196 3
d204 3
d212 3
d220 3
d225 1
a225 1
  return getToken( "T" ) ;
d228 3
d236 3
@


3.1
log
@*** empty log message ***
@
text
@d12 1
a12 1
// RCS: $Id: Descriptor.h,v 3.0 1994/03/18 23:09:43 rbarnett Exp rbarnett $
d16 1
a16 1
//  Feature test switches
d21 1
a21 1
#include <rw/collstr.h>
d24 4
d34 3
d73 12
a84 2
  RWBoolean operator==(const RWCString&) const ;
  RWBoolean operator==(const char*) const ;
d91 1
d122 8
a129 5
  unsigned binaryStoreSize(void) const ;
  void     saveGuts(RWFile&)     const ;
  void     saveGuts(RWvostream&) const ;
  void     restoreGuts(RWFile&)        ;
  void     restoreGuts(RWvistream&)    ;
d133 1
a133 1
  RWCollectableString theID ;
@


3.0
log
@NewBuild
@
text
@d1 3
a3 2
// Program Name: Descriptor.h
// Subsystem   : Data Management (Build 1)
d12 1
a12 1
// RCS: $Id: Descriptor.h,v 2.6 1994/02/22 21:21:09 rbarnett Exp rbarnett $
d14 1
d69 31
a99 31
	//
	// Input/Output operator
	//
	friend istream& operator>>(istream&,       Descriptor&) ;
	friend ostream& operator<<(ostream&, const Descriptor&) ;

	//
	// Get the value for a particular Descriptor Item
	//
	const RWCString  getToken(const char* , int=0) const ;
	const RWOrdered* getTokenList(const char*) const ;

	//
	// Some shortcuts for getToken
	//
	const RWCString anodePosition(void)const ; // The 'A' Token
	const RWCString channel(void)      const ; // The 'C' Token
	const RWCString detector(void)     const ; // The 'D' Token
	const RWCString element(void)      const ; // The 'E' Token
	const RWCString frequency(void)    const ; // The 'F' Token
	const RWCString group(void)        const ; // The 'G' Token
	const RWCString housekeeping(void) const ; // The 'H' Token
	const RWCString instrument(void)   const ; // The 'I' Token
	const RWCString lag(void)          const ; // The 'L' Token
	const RWCString marker(void)       const ; // The 'M' Token
	const RWCString observatory(void)  const ; // The 'O' Token
	const RWCString phase(void)        const ; // The 'P' Token
	const RWCString status(void)       const ; // The 'S' Token
	const RWCString time(void)         const ; // The 'T' Token
	const RWCString execute(void)      const ; // The 'X' Token
	const RWCString extension(void)    const ; // The 'Z' Token
d121 1
a121 1
	return (const char *) theID ;
d126 1
a126 1
	return getToken( "A" ) ;
d131 1
a131 1
	return getToken( "C" ) ;
d136 1
a136 1
	return getToken( "D" ) ;
d141 1
a141 1
	return getToken( "E" ) ;
d146 1
a146 1
	return getToken( "F" ) ;
d151 1
a151 1
	return getToken( "G" ) ;
d156 1
a156 1
	return getToken( "H" ) ;
d161 1
a161 1
	return getToken( "I" ) ;
d166 1
a166 1
	return getToken( "L" ) ;
d171 1
a171 1
	return getToken( "M" ) ;
d176 1
a176 1
	return getToken( "O" ) ;
d181 1
a181 1
	return getToken( "P" ) ;
d186 1
a186 1
	return getToken( "S" ) ;
d191 1
a191 1
	return getToken( "T" ) ;
d196 1
a196 1
	return getToken( "X" ) ;
d201 1
a201 1
	return getToken( "Z" ) ;
@


2.6
log
@Update for Build 3 compatability
@
text
@d11 1
a11 1
// RCS: $Id: Descriptor.h,v 2.5 1994/01/13 21:12:04 rbarnett Exp rbarnett $
@


2.5
log
@Update for demos
@
text
@d11 1
a11 1
// RCS: $Id: Descriptor.h,v 3.3 1994/01/13 20:59:12 rbarnett Exp rbarnett $
a21 3
// Local headers
#include "rw_ids.h"

@


2.4
log
@Replaced Descriptor *'s with Descriptor &'s
@
text
@d11 1
a11 1
// RCS: $Id: Descriptor.h,v 3.2 1993/12/16 22:15:13 rbarnett Exp rbarnett $
d70 6
@


2.3
log
@Update/cleanup for Build2
@
text
@d11 1
a11 1
// RCS: $Id: Descriptor.h,v 2.2 1993/10/20 21:46:48 rbarnett Exp rbarnett $
d67 2
a69 5
  //
  // Does ID string match
  //
  int contains(Descriptor&) const ;

d79 16
a94 7
	const RWCString observatory(void)  const ;
	const RWCString instrument(void)   const ;
	const RWCString detector(void)     const ;
	const RWCString group(void)        const ;
	const RWCString element(void)      const ;
	const RWCString housekeeping(void) const ;
	const RWCString channel(void)      const ;
d99 1
a99 1
  unsigned binaryStoreSize()     const ;
d102 2
a103 2
  void     restoreGuts(RWFile&) ;
  void     restoreGuts(RWvistream&) ;
d119 1
a119 1
inline const RWCString Descriptor::observatory(void) const
d121 1
a121 1
	return getToken( "O" ) ;
d124 1
a124 1
inline const RWCString Descriptor::instrument(void) const
d126 1
a126 1
	return getToken( "I" ) ;
d134 10
d149 36
a184 1
inline const RWCString Descriptor::element(void) const
d186 1
a186 1
	return getToken( "E" ) ;
d189 1
a189 1
inline const RWCString Descriptor::housekeeping(void) const
d191 1
a191 1
	return getToken( "H" ) ;
d194 1
a194 1
inline const RWCString Descriptor::channel(void) const
d196 1
a196 1
	return getToken( "C" ) ;
@


2.2
log
@Added descriptor manipulators
@
text
@d11 1
a11 1
// RCS: $Id: Descriptor.h,v 2.1 1993/08/06 21:47:11 rbarnett Exp rbarnett $
d38 1
a38 1
  Descriptor() {}
d43 2
a44 2
  Descriptor( const char* ) ;
  Descriptor( const RWCString& ) ;
d49 1
a49 1
  Descriptor( const Descriptor& ) ;
d54 1
a54 1
  virtual ~Descriptor() ;
d59 1
a59 1
  const char* getID( void ) const ;
d71 1
a71 1
  int contains( Descriptor& ) const ;
d76 2
a77 2
	const RWCString  getToken( const char* , int=0 ) const ;
	const RWOrdered* getTokenList( const char* ) const ;
d82 5
a86 5
	const RWCString observatory(void) const ;
	const RWCString instrument(void) const ;
	const RWCString detector(void) const ;
	const RWCString group(void) const ;
	const RWCString element(void) const ;
d88 1
a88 1
	const RWCString channel(void) const ;
d93 3
a95 1
  unsigned binaryStoreSize() const ;
a97 2
  void     saveGuts(RWFile&) const ;
  void     saveGuts(RWvostream&) const ;
d105 3
@


2.1
log
@ID is now RWCollectable String
@
text
@d11 1
a11 1
// RCS: $Id: Descriptor.h,v 2.0 1993/06/30 20:32:50 rbarnett Exp rbarnett $
d20 1
a32 6
private:

  RWCollectableString theID ;

protected:

d59 1
a59 1
  const char* getID( void ) const { return (const char *) theID ; }
d63 1
a63 1
  // Two SImpleDescriptors are considered "equal" if they have identical
a68 6
  // Inherited from Descriptor
  // Always answer true.
  //
  int isSimple( void ) const { return 1 ; }

  //
d73 17
d99 4
d105 40
@


2.0
log
@Start Build 2!
@
text
@d6 1
a6 1
//  Declaration of Descriptor and SimpleDescriptor class
d8 1
a8 7
//  Descriptor is an abstract base class to provide certain common
//  funtional interfaces to all Descriptors. Much of this class is
//  retained from an earlier design which provided more elaborate
//  features. See SimpleDescriptor (a subclass) for a more realistic
//  Descriptor definition.
//
//  SimpleDescriptor is a Descriptor class that provides for a single
d11 1
a11 1
// RCS: $Id: Descriptor.h,v 1.3 1993/06/23 15:03:04 rbarnett Exp rbarnett $
a29 3
  //
  // RogueWave method Declarations
  //
a31 30
public:

  //
  // Constructor
  //
  Descriptor() {}

  //
  // Destructor
  //
  virtual ~Descriptor() {}

  // Does this Descriptor "contain" argument as a sub unit
  virtual int contains( Descriptor& ) const { return 0 ; }

  //
  // This method allows for distinction of SimpleDescriptors
  //
  virtual int isSimple( void ) const { return 0 ; }

} ; // End class Descriptor

// ========================================================================
// SimpleDescriptor
// ========================================================================

class SimpleDescriptor : public Descriptor {

  RWDECLARE_COLLECTABLE(SimpleDescriptor)

d34 1
a34 1
  RWCollectableString * theID ;
d43 1
a43 1
  SimpleDescriptor() {}
d48 2
a49 1
  SimpleDescriptor( const char* ) ;
d54 1
a54 1
  SimpleDescriptor( const SimpleDescriptor& ) ;
d59 1
a59 1
  virtual ~SimpleDescriptor() ;
d64 1
a64 1
  const char* getID( void ) const { return (const char *) (*theID) ; }
d71 1
a71 1
  RWBoolean operator==(const SimpleDescriptor&) const ;
d93 1
a93 1
} ; // End class SimpleDescriptor
@


1.3
log
@*** empty log message ***
@
text
@d17 1
a17 1
// RCS: $Id: Descriptor.h,v 1.2 1993/06/18 15:31:42 rbarnett Exp rbarnett $
@


1.2
log
@Update for Build1 testing
@
text
@d17 1
a17 1
// RCS: $Id: Descriptor.h,v 1.1 1993/06/10 16:33:27 rbarnett Exp rbarnett $
d36 4
a39 1
	RWDECLARE_COLLECTABLE(Descriptor)
d43 1
a43 1
	//
d45 1
a45 1
	//
d48 1
a48 1
	//
d50 1
a50 1
	//
d53 1
a53 1
	// Does this Descriptor "contain" argument as a sub unit
d56 3
a58 3
	//
	// This method allows for distinction of SimpleDescriptors
	//
d69 1
a69 1
	RWDECLARE_COLLECTABLE(SimpleDescriptor)
d73 1
a73 1
	RWCollectableString * theID ;
d79 1
a79 1
	//
d81 1
a81 1
	//
d84 1
a84 1
	//
d86 1
a86 1
	//
d89 3
a91 3
	//
	// Copy Constructor
	//
d94 1
a94 1
	//
d96 1
a96 1
	//
d99 3
a101 3
	//
	// Return the description string
	//
d104 5
a108 5
	//
	// == Operator
	// Two SImpleDescriptors are considered "equal" if they have identical
	// ID strings.
	//
d111 4
a114 4
	//
	// Inherited from Descriptor
	// Always answer true.
	//
d117 3
a119 3
	//
	// Does ID string match
	//
d122 3
a124 3
	//
	// Inherited from RWCollectable and reimplemented
	//
d131 1
a131 1
} ;
d133 1
a133 1
#endif Descriptor_h
@


1.1
log
@Initial revision
@
text
@d17 1
a17 1
// RCS: $Id$
a52 8
/*
  virtual const Descriptor ** selections( void ) const ;
  virtual const Descriptor ** children( void ) const ;
  virtual int editable( void ) const ;
  virtual void dump( int tab=0 ) const ;
  virtual void destroy( void ) ;
*/

d84 6
a89 1
  SimpleDescriptor( const char* anID ) ;
d94 1
a94 1
  virtual ~SimpleDescriptor() {}
d101 7
@

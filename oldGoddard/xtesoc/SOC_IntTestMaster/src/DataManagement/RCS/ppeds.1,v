head	5.2;
access
	soccm
	jonv
	gromeo;
symbols
	Build5_1:5.1
	Build4_3_1:4.2;
locks; strict;
comment	@# @;


5.2
date	96.07.12.19.48.13;	author rbarnett;	state Exp;
branches;
next	5.1;

5.1
date	96.05.07.16.50.27;	author rbarnett;	state Exp;
branches;
next	4.3;

4.3
date	95.11.10.19.09.09;	author rbarnett;	state Exp;
branches;
next	4.2;

4.2
date	95.05.24.21.41.03;	author rbarnett;	state Exp;
branches;
next	4.1;

4.1
date	95.05.19.20.23.36;	author rbarnett;	state Exp;
branches;
next	;


desc
@man page for eds pre-processor script
@


5.2
log
@better use of itelics in OPTION line
@
text
@.\" ==========================================================================
.\" File Name   : ppeds.1
.\" Subsystem   : Data Management
.\" Programmer  : Randall D. Barnette, Hughes STX
.\" Description : Man page for the dmServer process.
.\" 
.\" RCS: $Id: ppeds.1,v 4.3 1995/11/10 19:09:09 rbarnett Exp $
.\" 
.\" ==========================================================================
.TH ppeds 1 "11 July 1996" "XTE-SOC V5.0" "Data Management"
.SH NAME
ppeds \- Shell script to process production EDS data.
.\" ==========================================================================
.SH SYNOPSIS
.\" ==========================================================================
.B ppeds
.RB [ -d ]
.RB [ -r ]
.RB [ -dm ]
.RB [ -ingest
.I file1
.RI [ "file2 ..." ]]
.\" ==========================================================================
.SH DESCRIPTION
.\" ==========================================================================
.B ppeds
is used to run the various steps needed for production processing.
.\" ==========================================================================
.SH OPTIONS
.\" ==========================================================================
.TP
.B -d
Turn on dubugging dribble.
.TP
.B -r
Run
.BR reTBP (1)
before proceding with
.BI dmIndex(1).
.TP
.B -ingest \fIfile
Ingest (with
.BR pringest (1))
file.
.\" ==========================================================================
.SH SEE ALSO
.\" ==========================================================================
.I Data Management Subsystem Design
.br
.BR dmReport (1),
.BR dmIndex (1),
.BR reTBP (1)
.\" ==========================================================================
.SH AUTHOR
.\" ==========================================================================
Randall D. Barnette (rbarnett@@xpert.stx.com)
@


5.1
log
@Document update
@
text
@d5 1
a5 3
.\" Description :
.\"
.\"   Man page for the dmServer process.
d10 1
a10 1
.TH ppeds 1 "7 May 1996" "XTE-SOC V5.0" "Data Management"
a31 1
.\" -------------------------------
a32 1
.\" -------------------------------
a34 1
.\" -------------------------------
d36 4
a39 2
.\" -------------------------------
Run reTBP(1) before proceding with dmIndex(1).
d41 4
a44 4
.\" -------------------------------
.BI -ingest " file"
.\" -------------------------------
ingest (with pringest(1)) file.
d48 1
a48 1
.IR "Data Management Subsystem Design" ,
@


4.3
log
@documentation update
@
text
@d9 1
a9 1
.\" RCS: $Id: ppeds.1,v 4.2 1995/05/24 21:41:03 rbarnett Exp $
d12 1
a12 1
.TH ppeds 1 "10 November 1995" "XTE-SOC V4.5" "Data Management"
@


4.2
log
@updates to documentation
@
text
@d9 1
a9 1
.\" RCS: $Id: ppeds.1,v 4.1 1995/05/19 20:23:36 rbarnett Exp $
d12 1
a12 1
.TH ppeds 1 "22 May 1995" "XTE-SOC V4.3" "Data Management"
d14 1
a14 1
ppeds \- script to process production EDS data.
d59 1
a59 1
Randall D. Barnette (rbarnett@@xema.stx.com)
@


4.1
log
@*** empty log message ***
@
text
@d2 1
a2 1
.\" File Name   : dmServer.1
d9 1
a9 1
.\" RCS: $Id: dmServer.1,v 3.2 1994/04/12 15:00:35 rbarnett Exp $
d12 1
a12 1
.TH dmServer 1 "1 April 1994" "Data Management"
d14 1
a14 1
dmServer \- Access SOC data via descriptor interface.
d18 7
a24 6
.B dmServer
.RB [ -i|interactive ]
.RB [ -d|debug ]
.RB [ -f|file
.IR commandFile ]
.RB [ -EDS|load-EDS-config-file ]
d28 2
a29 16
.B dmServer
is a process by which XTE telemetry data are retrieved from the SOC database.
While
.B dmServer
was originally concieved and implemented to serve the needs of FITS file
generation (see
.BR xff (1)),
it has sufficient flexibility to serve as a general purpose
database browser.
.PP
The communication protocol for
.BR xff (1)
is not documented here. Rather, we will concentrate on dmServer in it's
human-interaction mode using the
.B -interactive
option.
d35 1
a35 1
.B -i|interactive
d37 1
a37 2
Run the process in interactive mode, sending human-readable prompts
and error messages to standard output.
d40 1
a40 1
.B -d|debug
d42 1
a42 3
Generate status information and excessive dribble to monitor
.B dmServer
activities.
d45 1
a45 1
.B -EDS|load-EDS-config-file
d47 1
a47 224
This option specifies that EDS files are to be read and that the
EDS configuration file should be loaded. In the event that
this option is not given and EDS packets are processed, the results
are unpredictable. Most likely the error message
.br
"EDS configuration
.I n
unknown."
.br
will be issued for each EDS packet and processing will continue.
.TP
.\" -------------------------------
.BI "-f|file " commandFile
.\" -------------------------------
Read
.I commandFile
instead of standard input for interactive commands.
.\" ==========================================================================
.SH INTERACTIVE OPERATION
.\" ==========================================================================
.B dmServer
recognizes a few simple "fetch" type commands. In interactive
mode,
.B dmServer
displays a prompt to which the user supplies a single-letter
command possibly followed by parameters.
Basic operation consists of
.IP 1.
Specifying the apid and time for the desired data,
.IP 2.
Providing a list of
.B Descriptor
lables for the desired data items, and
.IP 3.
Requesting the delivery of the data.
.PP
The following commands are recognized.
.\" ==========================================================================
.SH COMMANDS
.\" ==========================================================================
.TP
.BI C " applictionID startTime stopTime"
The C command specifies the type (or
.BR C onfiguration)
of data desired.
All three parameters must be supplied. The
.I applicationID
is an integer range 0-255.
The start and stop times are MET seconds since the secondary epoch
1 January, 1994.
If such data is available, the server will respond with:
.PP
.RS
.I filename
.B
has configuration
.IR n ,
Start time
.IR start ,
Stop time
.IR stop ,
with
.I N
rows.
.PP
If no data is available, the server responds with:
.PP
dmServer: Can't find file for apid
.IR apid ,
time
.IR time .
.PP
The
.B C
command must be supplied before either the the
.BR D ,
.BR R ,
or
.B A
commands are issued.
.RE
.TP
.BI D " descriptor"
After a successful
.B C
command, the user must specify the exact data item desired.
.I descriptor
is a string conforming to the XTE DDL syntax. If the item
is available, the server will return with another prompt.
Otherwise, it responds with:
.PP
.RS
dmServer: Descriptor
.I d
is not available from this DataSet.
.RE
.TP
.B R
.br
Print a Row of data. The server maintains the list of descriptors
supplied since the last
.B C
command. The
.B R
command tells the server to fetch the data from the
.B DataSet
and print all requests in the form:
.PP
.RS
.IB d1 = value1
.br
.IB d2 = value2
.br
.I etc.
.PP
Large arrays will generate large amounts of output. Currently there is
no way to format output. All formatting is completely defined in the
DataValue class ostream operator
.RB ( operator<<() )
internals.
.PP
This command advances the the current row to the next row in the
.BR DataSet .
.RE
.TP
.B A
.br
List All descriptors available from the
.BR DataSet .
This command can
generate huge amounts of output. User beware!
.TP
.B W
.br
Reset the current row number in the
.BR DataSet .
This command
effectively "rewinds" the
.BR DataSet .
The
.B DataSet
is still
in memory (no additional
.B C
command is needed) and the
.B Descriptor
list is maintained.
.TP
.B I
.br
Clear the list of descriptors
.I and
reset the current row number. The
.B DataSet
is maintained in memory, but the
.B Descriptor
list is empty.
.TP
.B E
.br
Exit the server process.
.TP
.BI Q
.br
Same as
.BR E .
.\" ==========================================================================
.SH EXAMPLE
.\" ==========================================================================
In this example,
.B dmServer
is run in interactive mode with debug messages turned on.
.PP
1>
.B dmServer -i -d
.br
 dmServer: Debug Mode On
.br
 dmServer: Interactive Mode On
.br
OK >
.B C 80 200 400
.br
 dmServer: Config request, appid = 80, tbeg = 200
.br
 /socops/timeOrdered/realtime/day0000/apid080/I50-00000c0-000011f
.br
 has configuration 0x105, Start time 192, Stop time 272, with 6 rows.
.br
OK >
.B D time
.br
 dmServer: Descriptor request = TIME
.br
OK >
.B D all
.br
 dmServer: Descriptor request = ALL
.br
OK >
.B R
.br
 dmServer: Row request
.br
 TIME value is 192
.br
 ALL value is 0, 63, 166, 0, 0, ...
.br
OK >
.B E
.br
 dmServer exiting.
.\" ==========================================================================
.SH ENVIRONMENT
.\" ==========================================================================
.TP
.B $SOCOPS
This is the root directory under which
.B dmServer
will look for
.B DataSet
files and directories.
For SOC operations, it is expected to be
.BR /socops .
a48 49
.SH FILES
.\" ==========================================================================
.TP
.BI $SOCOPS/timeOrdered/realtime/day dddd /apid aaa /I xx-start-stop
This is the form of the files that
.B dmServer
reads from the SOC database, where
.I dddd
is the MET day (i.e. MET/86400),
.I aaa
is the packet application ID in decimal,
.I xx
is the packet application ID in hex, and
.I start-stop
is the start and stop times of the data items contained in the file.
.TP
.B $SOCOPS/reference/EdsConfigDescr
This file contains descriptions of EDS configurations.
.B dmServer
will attempt to load this file if the
.B -EDS
option is supplied.
.\" ==========================================================================
.SH DIAGNOSTICS
.\" ==========================================================================
In debug mode
.RB ( -debug
option), numerous messages appear; typically, they are an echo of the input
request, and have the form:
.PP
.B dmServer:
.IR message .
.PP
See
.B EXAMPLE
above.
.\" ==========================================================================
.SH BUGS
.\" ==========================================================================
The
.B OK>
prompts in interactive mode are not always properly flushed.
.PP
If
.B dmServer
is given a time which is between the stop time of one
.B DataSet
and the start time of another DataSet, it does not find either.
.\" ==========================================================================
d55 1
a55 1
.BR xff (1)
a59 4
.\" ==========================================================================
.SH HISTORY
.\" ==========================================================================
Version 3.0, May 1994
@

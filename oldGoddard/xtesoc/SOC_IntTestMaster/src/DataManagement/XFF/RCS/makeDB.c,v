head	1.6;
access
	soccm
	arots
	jonv
	gromeo;
symbols
	Build6_0:1.6
	Build5_4:1.6
	Build5_3:1.6
	Build5_2:1.6
	Build5_0_2:1.4
	Build5_0_0:1.4
	Build4_5_3:1.4
	Build_4_5_2:1.2
	Build4_5_1:1.2
	Build_4_4_3:1.2
	Build4_4_2:1.2;
locks;
comment	@ * @;


1.6
date	96.09.26.21.46.31;	author arots;	state Exp;
branches;
next	1.5;

1.5
date	96.04.08.12.38.17;	author arots;	state Exp;
branches;
next	1.4;

1.4
date	95.11.13.15.53.43;	author arots;	state Exp;
branches;
next	1.3;

1.3
date	95.11.03.21.11.05;	author arots;	state Exp;
branches;
next	1.2;

1.2
date	95.08.15.21.06.11;	author arots;	state Exp;
branches;
next	1.1;

1.1
date	95.07.21.20.12.34;	author arots;	state Exp;
branches;
next	;


desc
@Creates various database directories for xff.
@


1.6
log
@Better accounting of good time.
@
text
@/* RCS: $Id: makeDB.c,v 1.4 1995/11/13 15:53:43 arots Exp arots $ */
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <memory.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/errno.h>
#include <time.h>
#include "xff.h"

extern int errno ;

/*   The following statement is for the Centerline compiler
 *   which does funny things with system include files.
 *   Normally, we could simply include Externs.h from the SOC include
 *   area, but since XFF insists on being different...
 *   There are a good number of system files that compilers need
 *   properly prototyped. If this list gets longer, consider using
 *   Externs.h.
 */

#if defined(__cplusplus) && defined(__CLCC__) && defined(BSD)
extern "C" int mkdir(char*,int) ;
#endif

int nlines=25 ;
const char *shareadme[25] = {
"SHA Message Digest Files",
" ",
"This directory contains the 160-bit message digest files for each day of",
"the mission.  The file names are `sha_Daynnnn', where nnnn is the day",
"number.",
" ",
"The day files are pure ASCII and contain the 160-bit message digests,",
"calculated according to the Secure Hash Algorithm as specified in Federal",
"Information Processing Standard (FIPS) 180-1 as published by the NIST,",
"for all FITS data files (`FS' and `FH' files) that have their starting",
"time in that day.",
" ",
"There is one line per data file in the SHA day file.  The format is:",
" ",
"   hhhhhhhh hhhhhhhh hhhhhhhh hhhhhhhh hhhhhhhh  <file name>",
" ",
"hhhhhhhh is the message digest value in hexadecimal notation, displayed",
"in five groups of 32 bits.  <file name> is the path of the data file,",
"relative to the top of the database (typically $SOCFITS/FD).",
" ",
"The authenticity of any XTE data file can be verified by calculating",
"a SHS message digest and comparing it against the value in the database",
"SHA day file.",
" ",
"  - Arnold Rots"
} ;

int makeDir (char *, char *, mode_t) ;

int mkSHAreadme (char *) ;
int mkCaldir (char *, mode_t) ;

int makeDB (char *datadir)
/*----------------------------------------------------------------------
 *  Check to see whether the "left" side of the XFDB exists; create if
 *  necessary  -  ahr 950531
 *
 *  Input:  datadir        Database directory
 *
 *  Return: int            Success?
 *
 ----------------------------------------------------------------------*/
{
  char copycmd[512] ;
  mode_t mode=511 ;

  if ( !makeDir (datadir, "SlewData", mode) )
    return 0 ;

  if ( !makeDir (datadir, "ASMData", mode) )
    return 0 ;

  if ( !makeDir (datadir, "ASMProducts", mode) )
    return 0 ;

  if ( !makeDir (datadir, "ASMProducts/Cumulative", mode) )
    return 0 ;

  if ( !makeDir (datadir, "ASMProducts/Day-by-Day", mode) )
    return 0 ;

  if ( !makeDir (datadir, "OrbitEphem", mode) )
    return 0 ;

  if ( !makeDir (datadir, "Clock", mode) )
    return 0 ;

  if ( !makeDir (datadir, "Cal", mode) )
    return 0 ;

  if ( !makeDir (datadir, "SHA", mode) )
    return 0 ;

  sprintf (copycmd, "cp %s/etc/xff.readme %s00README",
	   getenv("SOCHOME"), datadir) ;
  system (copycmd) ;

  return 1 ;
}

int makeDir (char *datadir, char *subdir, mode_t mode)
/*----------------------------------------------------------------------
 *
 *  Make directory <datadir>/<subdir>, if it does not already exist.
 *
 *  Input:
 *    datadir    char*    database directory
 *    subdir     char*    directory to be checked/created
 *    mode       mode_t   mask
 *
 *  Return:      int      Success?
 *
 ----------------------------------------------------------------------*/
{

  char curpath[256] ;

  strcpy (curpath, datadir) ;
  strcat (curpath, subdir) ;
  if ( mkdir (curpath, mode) )
    {
      if ( errno != EEXIST )
	{
	  fprintf (stderr, "         ===> Could not create %s directory\n", subdir) ;
	  fprintf (stderr, "                %s\n", curpath) ;
	  return 0 ;
	}
    }
  else
    {
      fprintf (LOG, "              Created %s directory\n", subdir) ;
      fprintf (LOG, "                %s\n", curpath) ;
      if ( !strcmp (subdir, "SHA") )
	mkSHAreadme (curpath) ;
      if ( !strcmp (subdir, "Cal") )
	mkCaldir (curpath, mode) ;
    }

  return 1 ;
}

int mkSHAreadme (char *curpath)
/*----------------------------------------------------------------------
 *
 *  Create the 00README file in the SHA directory
 *
 *  Input:
 *    curpath    char*    SHA directory
 *
 *  Return:      int      Success?
 *
 ----------------------------------------------------------------------*/
{
  int i ;
  FILE *FD ;

  strcat (curpath, "/00README") ;
  if ( FD = fopen (curpath, "w" ) ) {
    for (i=0;i<nlines;i++)
      fprintf (FD, "%s\n", shareadme[i]) ;
    fclose (FD) ;
  }
  return 1 ;
}

int mkCaldir (char *curpath, mode_t mode)
/*----------------------------------------------------------------------
 *
 *  Set up basic Cal database directory.
 *
 *  Input:
 *    curpath    char*    Cal directory
 *    mode       mode_t   mask
 *
 *  Return:      int      Success?
 *
 ----------------------------------------------------------------------*/
{
  char newpath[256] ;
  ObsDescr obs ;

  obs.ra = 0.0 ;
  obs.dec = 0.0 ;
  obs.start = 0 ;
  obs.stop = (6 * 365 + 1) * 86400 ;
  strcpy (obs.obsId, "aaaaa-00-00-00") ;
  strcpy (obs.source, "setCalxxx") ;
  strcpy (obs.user, "XTE-GOF") ;

  strcat (curpath, "/") ;
  strcat (curpath, calEraDir) ;
  mkdir (curpath, mode) ;
  sprintf (newpath, "%s/pca", curpath) ;
  mkdir (newpath, mode) ;
  sprintf (newpath, "%s/hexte", curpath) ;
  mkdir (newpath, mode) ;
  sprintf (newpath, "%s/asm", curpath) ;
  mkdir (newpath, mode) ;

  pinum = 14 ;
  *aodir = 0 ;
  strcpy (prodir, "Cal/") ;
  sprintf (obsdir, "%s/", calEraDir) ;
  createPI (&obs, "01/01/94", "00:00:00", "31/12/99", "23:59:59") ;

  return 1 ;
}
@


1.5
log
@Fixed first row problem for FMI; symbolic links.
@
text
@a190 2
  strcpy (obs.obsId, "aaaaa-00-00-00") ;
  strcpy (obs.source, "setCalxxx") ;
d195 2
@


1.4
log
@Special handling of cal directory and indices.
@
text
@d1 3
a3 1
/* RCS: $Id: makeDB.c,v 1.1 1995/11/03 20:12:37 soccm Exp soccm $ */
d73 1
a73 1

d103 4
d167 5
a171 4
  FD = fopen (curpath, "w" ) ;
  for (i=0;i<nlines;i++)
    fprintf (FD, "%s\n", shareadme[i]) ;
  fclose (FD) ;
d199 2
a200 1
  strcat (curpath, "/Day0000") ;
@


1.3
log
@Support SHA Digest.
@
text
@d58 1
d138 2
d165 43
a207 1
  return 0 ;
@


1.2
log
@First complete delivery;
Last delivery before the freeze.
@
text
@d1 1
a1 1
/* RCS: $Id: makeDB.c,v 1.1 1995/07/21 20:12:34 arots Exp arots $ */
d26 29
d57 2
d97 3
d135 2
d140 23
@


1.1
log
@Initial revision
@
text
@d1 1
a1 1
/* RCS: $Id$ */
d46 9
@

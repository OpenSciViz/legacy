head	1.6;
access
	gromeo;
symbols;
locks; strict;
comment	@ * @;


1.6
date	2000.08.01.12.19.55;	author jonv;	state Exp;
branches;
next	1.5;

1.5
date	2000.07.24.17.22.39;	author jonv;	state Exp;
branches;
next	1.4;

1.4
date	2000.04.19.16.29.25;	author jonv;	state Exp;
branches;
next	1.3;

1.3
date	2000.01.10.17.27.02;	author jonv;	state Exp;
branches;
next	1.2;

1.2
date	99.11.16.20.43.26;	author jonv;	state Exp;
branches;
next	1.1;

1.1
date	99.11.10.17.47.24;	author jonv;	state Exp;
branches;
next	;


desc
@ for helping with data gaps
@


1.6
log
@removed some debug comments which were left on
@
text
@// ===========================================================================
// File Name   : GapUtilities.C
// Subsystem   : Data Management
// Programmer  : Jon Vandegriff, Raytheon ITSS
// Description:
//
// 
// 
// 
// 
//
// RCS: $Id: GapUtilities.C,v 1.5 2000/07/24 17:22:39 jonv Exp $
//
// ===========================================================================

#include <iostream.h>
#include <stdio.h>
#include <dirent.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <rw/ordcltn.h>
#include <rw/cstring.h>
#include <rw/collstr.h>
#include <rw/collint.h>
#include <rw/regexp.h>

#include "ObsReport.h"
#include "GapUtilities.h"
#include <XTETime.h>

extern const char* edsName(unsigned long) ;


static const double day0_MJD  = 49353.0;



RWOrdered *getDirListing( const char *directory_name, int show_message )
{
  RWOrdered *dir_listing = new RWOrdered();

  DIR *dir_ptr;
  struct dirent *dir_entry;

  if ( (dir_ptr = opendir(directory_name)) == NULL ) {
    if (show_message)
      cerr << "ERROR: can't open directory " << directory_name << endl;
    return NULL;
  } else {
    while ( (dir_entry = readdir(dir_ptr)) != NULL ) {
      // do not include the two entries "." and ".."
      if ( !( (strcmp(dir_entry->d_name, ".") == 0) ||
              (strcmp(dir_entry->d_name, "..") == 0) ) )
      {
        RWCollectableString *filename = 
          new RWCollectableString(dir_entry->d_name);

        dir_listing->insert( filename );
      }
    }
  }
  closedir( dir_ptr );

  return dir_listing;

}



RWOrdered *getActiveProductionDays()
{

  char *socops = getenv("SOCOPS");
  if (!socops) {
    cerr << "ERROR: $SOCOPS must be set properly." << endl;
    return NULL;
  }

  RWCString dir = "ingest/production";
  if ( socops[ strlen(socops)-1 ] != '/' )
    dir.prepend("/");
  dir.prepend(socops);

  RWOrdered *days = NULL;

  RWOrdered *files = getDirListing( dir.data(), 1 );

  if (files) {
    RWOrdered *unsortedDays = new RWOrdered( files->entries() );

    int i;
    for(i=0; i< files->entries(); i++) {
      // Find the index of the "day" text
      // then grab the next 4 chars after that.
      RWCollectableString *s = (RWCollectableString *) (*files)[i];
      size_t anchor;
      if ( (anchor = s->index("day")) != RW_NPOS ) {
        // starting 3 characters after the start of "day", grap the next
        // 4 characters and turn them into an int
        const char *day_as_chars = ((*s)(anchor+3,4)).data();
        int day_as_int = atoi( day_as_chars );
        unsortedDays->insert( new RWCollectableInt( day_as_int ) );
      }
    }

    // Now, sort them by day number
    // This is the poor mans sorting method, but we will never 
    // have more than a handfull of days at a time.

    int N = unsortedDays->entries();
    days = new RWOrdered( N ) ;

    for(i=0; i< N; i++) {

      int min = 50000;  // no day number will be bigger than this!!
      RWCollectable *minPtr = NULL;
      int j;
      for(j=0; j<unsortedDays->entries(); j++) {
        // if this is the min value left, then insert it into the bigger list
        int dayValue =  ( (RWCollectableInt *) (*unsortedDays)[j] )->value();
        if (dayValue < min) {
          minPtr = (RWCollectable *) (*unsortedDays)[j];
          min = dayValue;
        }
      }
      if (minPtr) {
        days->insert( minPtr );
        unsortedDays->remove( minPtr );
      } else {
        cerr << "Fatal ERROR - could not find minimun day value when "
             << " sorting list of days in gapFinder.C" << endl;
        exit(1);
      }
    }
    files->clearAndDestroy();
    delete files;
    delete unsortedDays; // Don't destroy it becuase we just copied 
                         // the pointers to the sorted list.
  }

  return days;

}


RWOrdered *getPacketFiles(int day, const int *apids, const int N)
{

  char *socops = getenv("SOCOPS");
  if (!socops) {
    cerr << "ERROR: $SOCOPS must be set properly." << endl;
    return NULL;
  }

  RWCString dir = "ingest/production/day";
  if ( socops[ strlen(socops)-1 ] != '/' )
    dir.prepend("/");
  dir.prepend(socops);

  char day_chars[5];
  sprintf(day_chars,"%04d", day);
  dir.append(day_chars);

  RWOrdered *pktFiles = new RWOrdered(5);

  int a,i;
  for(i=0; i<N; i++) {

    a = apids[i];

    RWCString apidDir = dir;
    apidDir.append("/apid");
    char apidText[4];
    sprintf(apidText,"%03d", a);
    apidText[3] = '\0';
    apidDir.append(apidText);
    apidDir.append("/");

    RWOrdered *files = getDirListing( apidDir.data(), 0 );
    if (files) {


      int j;
      for(j=0; j<files->entries(); j++) {

        RWCollectableString *pfile = (RWCollectableString *) (*files)[j];

        // make sure we only add packet files to the list!!
        // Look for a "-" in spot 11 (index==10)
        // and make sure its the right length (Perl is better at this...)
        if ( (pfile->first('-') == 10) && (pfile->length() == 21) ) {
          pktFiles->insert( new RWCollectableString( apidDir + *pfile ) );
        }
      }
      files->clearAndDestroy();
      delete files;

    }
  }

  if (pktFiles->entries() > 0) {
    return pktFiles;
  } else {
    delete pktFiles;
    return NULL;
  }

}



char *getProductionPacketFile(int day, int apid )
{
  char *theFile = NULL;

  // Returns a single production packet file in new'd space
  // if no files are found or more than one file is found, NULL
  // is returned.

  int theApid[1];
  theApid[0] = apid;
  

  RWOrdered *pktFiles = getPacketFiles(day, theApid, 1);

  if (pktFiles && (pktFiles->entries() == 1) ) {

    RWCollectableString *string = (RWCollectableString *)( (*pktFiles)[0] );

    theFile = new char [ string->length()+1 ];
    strcpy(theFile, string->data() );
  }

  if (pktFiles)
    pktFiles->clearAndDestroy();

  return theFile;
}


int get_hr_version(const RWCollectableString *filename)
{
  RWCRegexp regexp( "_[0-9]+\.human$" );

  size_t length;
  size_t start = filename->index(regexp, &length);

  if (start == RW_NPOS) {
    cerr << "ERROR - failed to find human readable version number in "
         << "filename " << filename->data() << endl
         << " in routine get_hr_version (called from getHRfilename) "
         << "in GapUtilities.C" << endl;
    exit(1);
  }

  // The number begins at 1 position past the starting index, and has 
  // a length 7 less than the length of the pattern match.
  // (There are 7 non number characters in the pattern.)
  // Example:
  // Wk365_045.human
  // start = 5; length = 10;
  // for the version number, the start is 6, the lenth is 3
  size_t num_start  = start + 1;
  size_t num_length = length -7;

  RWCString num( (*filename)(num_start, num_length) );
  RWCRegexp regexp2( "^[0-9]+$" );
  if( num.index(regexp2) == RW_NPOS ) {
    cerr << "ERROR - failed to find human readable version number in "
         << "filename " << filename->data() << endl
         << " in routine get_hr_version (called from getHRfilename) "
         << "in GapUtilities.C" << endl;
    exit(1);
  }

  return atof(num.data());
}




const char *getHRfilename(int day)
{
  int DAY = 365 + day;

  int WEEK = int( DAY/7 );
  int week = WEEK - 160;

  char *sochome = getenv("SOCHOME");
  if (!sochome) {
    cerr << "ERROR - you must set $SOCHOME before requesting an HR "
         << "filename from getHRfilename in GapUtilities.C" << endl;
    exit(1);
  }
  RWCString dir = "/sanction/week";
  dir.prepend(sochome);

  char weekText[4];
  sprintf(weekText,"%03d", week);
  weekText[3] = '\0';
  dir.append(weekText);
  dir.append("/");

  RWOrdered *dirListing = getDirListing( dir.data(), 0 );

  if (!dirListing) {
    cerr << "Error reading from Human Readable directory " << dir << endl;
    exit(1);
  }

  RWCString hrRegexpString = "^Wk";
  char WEEKText[4];
    sprintf(WEEKText,"%03d", WEEK);
    WEEKText[3] = '\0';
  hrRegexpString.append(WEEKText);
  hrRegexpString.append("_[0-9][0-9][0-9]\\.human$");
  RWCRegexp hrRegexp( hrRegexpString.data() );

  int i;
  int highest_version = -1;
  RWCollectableString *filename = NULL;
  for(i=0; i<dirListing->entries() ; i++ ) {
    RWCollectableString *file = (RWCollectableString *) (*dirListing)[i];
    if ( file->index(hrRegexp) != RW_NPOS ) {
      int this_version = get_hr_version(file);
      if( (highest_version == -1) || (this_version > highest_version) ) {
        filename = file;
        highest_version = this_version;
      }
    }
  }

  char *theHRfilename = NULL;
  if (filename) {
    theHRfilename = new char [ dir.length() + filename->length()+3 ];
    strcpy(theHRfilename, dir.data());
    strcat(theHRfilename, "/");
    strcat(theHRfilename, filename->data());
  }

  dirListing->clearAndDestroy();
  delete dirListing;

  return theHRfilename;

}



int getObsReportBoundaryDay( const ObsReport *hr, int start_or_stop_day)
{
  // Returns the starting or ending day of a 
  // human readable file (i.e. and ObsReport).
  // The second parameter indicates if you want the first 
  // day (use a value of 0) or the last day (use 1 or 
  // anything else non 0).

  int obs_num;
  if (start_or_stop_day == 0) {
    // user wants start day
    obs_num = 0;
  } else {
    obs_num = hr->entries()-1;
  }

  const Observation* anObs = hr->getObservation( obs_num );

  XTETime *theTime;
  int toSubtract = 0;
  if (start_or_stop_day == 0) {
    // user wants start day
    theTime = new XTETime( anObs->observation_start() );
  } else {

    // Note that the final day referenced in a HR file is at the end of
    // the last observation, and this time corresponds to the first
    // second of the day AFTER the last day covered by he HR file. 
    // Hence the subtraction by 1 for this case.

    theTime = new XTETime( anObs->observation_stop() );
    toSubtract = 1;
  }

  int day = int(theTime->UTmjd() - day0_MJD) - toSubtract;
                   //   day0_MJD  is global - see top of file



  delete theTime;

  return day;
}


char *makeTmpHRfile( int day, int prevWeek, int nextWeek ) {

  const char *filename;
  const char *prevFilename;
  const char *nextFilename;
  static const char space[2] = " ";

  int hasError = 0;

  filename = getHRfilename( day );
  if (!filename) {
    cerr << "Could not find HR filename for the week containing day "
         << day << endl;
    hasError = 1;
  }

  if (!hasError) {
    if (prevWeek) {
      prevFilename = getHRfilename( day - 7 );
      if (!prevFilename) {
        cerr << "Could not find HR filename for the week prior to the week "
             << "which includes misison day " << day << endl;
        hasError = 1;
      }
    } else {
      // put a space in there
      prevFilename = space;
    }
  }


  if (!hasError) {
    if (nextWeek) {
      nextFilename = getHRfilename( day + 7 );
      if (!nextFilename) {
        cerr << "Could not get HR file for the week prior to the week "
             << "which includes misison day " << day << endl;
        hasError = 1;
      }
    } else {
      // put a space in there
      nextFilename = space;
    }
  }

  char *finalFilename = NULL;
  if (!hasError) {
    finalFilename = new char [1000];
    time_t *nullTime = NULL;
    sprintf(finalFilename,"/tmp/tmpHR_%d_%lu", getpid(), time(nullTime) );


    // I really don't want to do all of this in C++, so I'll make it a 
    // system command.
    // The chaging of the modes is due to a bug in MakeObsPlan, which 
    // will soon be fixed...

    char cmd[1000];
    sprintf(cmd," cat %s %s %s | sed -e 's/Standard2g/Standard2f/g' > %s",
            prevFilename, filename, nextFilename, finalFilename );

    if ( system(cmd) ) {

      // An error occurred - try to remove the tmp file, then exit
      unlink( finalFilename );

      cerr << "Error while executing cmd " << cmd << endl;
      hasError = 1;
    }
  }

  // delete all the memory allocated in here:
  if (filename) delete (void *) filename;
  if (prevWeek && prevFilename) delete (void *) prevFilename;
  if (nextWeek && nextFilename) delete (void *) nextFilename;


  if (hasError) {

    if (finalFilename) delete finalFilename;
    // only delete this if we are not returning it

    return NULL;

  } else {

    // everything is OK - the file was made, and so return its name
    return finalFilename;
  }

}




ObsReport *getObsReport( int requestedMissionDay,
                         int attachPrevWeek, int attachNextWeek )
{

  ObsReport *r = NULL;

  char *HRfilename;

  HRfilename = makeTmpHRfile( requestedMissionDay, 
                              attachPrevWeek, attachNextWeek );
  if (HRfilename) {

    r = new ObsReport();  // the obs report will belong to the caller

    FILE *fp;
    if( (fp = fopen(HRfilename,"r")) == NULL ) {
      cerr << "ERROR - Cannot open file " << HRfilename << endl;
      return NULL;
    }


    if ( r->parse(fp, 0) ) {

      // if there is an error, the parsere returns 1, otherwise 0
      cerr << "Unable to parse Human readable " << HRfilename << endl;
      return NULL;
    }

    fclose(fp);

    if ( unlink(HRfilename) != 0 ) {
      cerr << "WARNING - could not remove temporary HR file " << HRfilename
           << endl;
    }

    delete HRfilename;

    return r;

  } else {
    cerr << "ERROR - unable to make tmp human readable file for mission day "
         << requestedMissionDay << endl;
    if (attachPrevWeek)
      cerr << " and the preceeding week " << endl;
    if (attachNextWeek)
      cerr << " and the following week " << endl;

    return NULL;
  }

}







ObsReport *getStaticObsReport( int requestedMissionDay,
                               int attachPrevWeek, int attachNextWeek )
{

  static ObsReport *r = NULL;

  if (r) {

    // If an obs report has already been loaded:
    // see if the mission day requested (plus any extra HRs)
    // are in the obs report which has already been loaded.

    int reportStartDay = getObsReportBoundaryDay(r, 0);
    int reportStopDay  = getObsReportBoundaryDay(r, 1);

    int requestedStartDay = requestedMissionDay - (attachPrevWeek? 7 : 0);
    int requestedStopDay  = requestedMissionDay + (attachNextWeek? 7 : 0);

    if(  ( reportStartDay <= requestedStartDay ) &&
         ( reportStopDay  >= requestedStopDay )  )
    {
      // The day(s) requested is in the obs report which has already been
      //  loaded so it can just be returned.

      return r;
    }
  }

  if (r) delete r;

  r = getObsReport( requestedMissionDay, attachPrevWeek, attachNextWeek );

  return r;

}







int getPkt(FILE *fd, unsigned char *pkt)
{
  unsigned short length;

  // read in packet header
  if( fread(pkt,sizeof(char),6,fd) != 6) {
    if( feof(fd) ) return 0;
    else {
      cerr << "FATAL ERROR - Cannot read packet header";
      exit (1);
    }
      
  }
  
  length = getPktLength(pkt) ;

  // check for invalid packet length
  if( length+6 > 1024 ) {
    cerr << "FATAL ERROR - Bad packet length" << endl;
    exit(1);
  }
  
  if( fread(&pkt[6],sizeof(char),length,fd) != length ) {
    cerr << "FATAL ERROR - Cannot read packet data" << endl;
  }

  return 1;

}


int getFirstPktFromNextDay(int todaysMissionDay, int apid, unsigned char *pkt)
{

  char *pktFile = getProductionPacketFile( todaysMissionDay+1, apid );

  int result = 0;

  if (pktFile) {
    FILE *fp;
    if ( fp = fopen(pktFile,"r") ) {
      if ( getPkt(fp, pkt) )
        result = 1;
      fclose(fp);
    }
    delete pktFile;
  }
  
  return result;

}






double *find_overlap(double a1, double a2, double b1, double b2 )
{

  // Take the two time intervals and and them together
  // return either NULL for no overlap or a pointer to 
  // static memory with the interval of the overlap.

  static double region[2];

  region[0] = 0;
  region[1] = 0;

  if ( (a2<=b1) || (b2<a1) ) {
    return NULL;
  } else {
    region[0] = ( a1>b1 ) ? a1 : b1;
    region[1] = ( a2<b2 ) ? a2 : b2;
    return region;
  }

}



int isEAforApidInDataMakingMode( int apid, const Observation *obs )
{
  int EAnumber = (apid-48)/4;

  int isDataMakingMode = 1;

  int can_EA_have_odd_mode = 0;
  int ss; // the subsystem number as defined from the Subsys enum:
          //enum Subsys {
          //  EA1 = 0,
          //  EA2, EA3, EA5, EA6, EA7,
          //  HX0, HX1,
          //  ACS,
          //  NumSubsys
          //} ;

  switch (EAnumber) {
  case 1:
    can_EA_have_odd_mode = 1;
    ss = 0;  //  Subsys EA1
    break;
  case 2:
    can_EA_have_odd_mode = 1;
    ss = 1;  //  Subsys EA2
    break;
  case 3:
    can_EA_have_odd_mode = 1;
    ss = 2;  //  Subsys EA3
    break;
  case 7:
    can_EA_have_odd_mode = 1;
    ss = 5;  //  Subsys EA7
    break;
  default:
    break;
  }

  if (can_EA_have_odd_mode) {
    const unsigned long* theConfigs = obs->configs();

    const char *modeName = edsName( theConfigs[ss] );

    // for debugging:
    //cout << "EDS mode for EA " << EAnumber << ": " << modeName << endl;

    // Modes which start with CB_ or CE_ are burst catcher modes
    // and since these modes do not always generate data, we will ignore
    // gaps which occurr during these modes.
    if ( (toupper(modeName[0]) == 'C') && (toupper(modeName[2]) == '_') &&
         (toupper(modeName[1]) == 'B') || (toupper(modeName[1]) == 'E') )
    {
      // for debugging:
      //cout << "Its a non-data making mode - no good time will be accounted to it for gap purposes" << endl;
      isDataMakingMode = 0;
    }
  }
      
  return isDataMakingMode;
}



XTRList *overlapWithGoodTimes(unsigned long gapStart,
                              unsigned long gapStop,
                           int apid, const ObsReport *hr, double min_gap_size,
                              int &obs_counter)
{
  // Look through the good time intervals of the appropriate Human Readable
  // file (or files!!) and find all the overlap of GTI's (for data 
  // producing modes only) with this gap
  // If the sum of all the overlap is bigger than the min gap size
  // then return 1
  // otherwise return 0

  // Could also look at each missed GTI and see it any of the individual
  // overlap regions are bigger than the min_gap_size...


  // Note that the obs_counter is passed in by reference so that 
  // if the calling routine wants to keep track of which obs we are on, it
  // can do that.  This makes searching for ordered time intervals
  // much faster (since the search does not have to start over at the 
  // first observation each time.)

  static XTRList *gapList = new XTRList();

  static XTETimeRange nullRange; // This is only used for its purpose below:

  gapList->andRange( nullRange );  // zero Out the gapList

  static XTETimeRange thisGap;

  int num_obs = hr->entries();

  unsigned long obs1;
  unsigned long obs2;

  double total_good_time_missed = 0;

  int j;
  double *overlap;

  while( obs_counter < num_obs ) { // main while loop

    const Observation* obs = hr->getObservation( obs_counter );

    // for debugging:
    // cout << "Looking at obs: " << obs->name() << "   "
    //     << obs->observation_start()<<" "<<obs->observation_stop() <<endl;

    obs1 = obs->observation_start();
    obs2 = obs->observation_stop();

    if (   (gapStart < obs2)   &&   (gapStop > obs1) ) {

      // Once in here, the gap overlaps some of the observation.

      if ( isEAforApidInDataMakingMode(apid, obs) ) {

        const GoodTimeList *gtl = obs->goodtime();
        if( gtl ) { 
          for(j=0; j<gtl->entries(); j++) {

            // for debugging:
            //XTETime g1( ((*gtl)[j])->from );
            //XTETime g2( ((*gtl)[j])->to );
            //cout << "Looking at GTI: "
            //     << g1.getDate(XTETime::UTC, XTETime::DATE) << " to "
            //     << g2.getDate(XTETime::UTC, XTETime::DATE) << endl;
            // end debugging section

            if ( overlap = find_overlap((double) gapStart, (double) gapStop,
                                        (double) ((*gtl)[j])->from,
                                        (double) ((*gtl)[j])->to ) )
            {

              thisGap.resetRange( overlap[0], overlap[1] );

              gapList->orRange( thisGap );

              // total_good_time_missed += overlap[1] - overlap[0];

              // Or you could be less stringent and just look at individual gaps:
              // (you have to worry about the return value if no gap is found...)
              //   => just put a return 0 in the if (obs2>gapStop) block below 

              // because you know no gap big enough has been found.
              // if ( (overlap[1] - overlap[0]) > min_gap_size) {
              //  return 1;
              // }

            }

          }
        }
      }
    }
    if ( obs2 >= gapStop ) {

      // that's it - no more observations will overlap this gap

      // if ( total_good_time_missed > min_gap_size)
      if ( gapList->totalTime() > min_gap_size) {
        return gapList;
      } else {
        return NULL;
      }
    }

    obs_counter++;
  }

  // We should never get here - the HR should always cover
  // the gaps given to it.
  // Note that it is the responsibility of the caller to 
  // insure that an appropriate HR is passed to the routine.
  //
  // Report an error and exit!!

  cerr << "Internal error - No observation found covering the gap from "
       << gapStart << " to " << gapStop << endl
       << "The human readable used covers mission day "
       << getObsReportBoundaryDay(hr, 0) << " to mission day "
       << getObsReportBoundaryDay(hr, 1) << " (inclusive)." << endl;
  exit(1);

  // We'll never get here, but this shuts up the compiler warnings...
  return NULL;

}




XTETime *make_XTETime_from_mission_day( int md ) {
  // construct and XTETime given a mission day

  // gets an XTETime with the current time
  char *null_ptr = 0;
  XTETime timeClass;

  // find the number of days between the current mission day and
  // the mission day provided by the user
  double current_MJD = timeClass.UTmjd();
  int current_mission_day = (int) (timeClass.UTmjd() - day0_MJD);
  int diff_in_days = current_mission_day - md;

  // create a new XTETime which is shifted by the same number of days
  double desired_MJD = current_MJD - (double) diff_in_days;
  // truncate - remove the franctional portion of the day
  desired_MJD = (double) (int) desired_MJD;

  return new XTETime( desired_MJD, XTETime::UTC, XTETime::MJD);
}



unsigned long metAtDayBoundary( int day, int start_or_stop_edge ) {
  // give the MET at the start or stop of a day
  // The second arg should be true for the start time and false for 
  // the final MET of the day.

  XTETime *certainDay = make_XTETime_from_mission_day( day );

  unsigned long timeAtEdge = 
    (unsigned long ) certainDay->getMET() +  (start_or_stop_edge ? 0 : 86399);

  delete certainDay;

  return timeAtEdge;
}
@


1.5
log
@HR obs could have no GOODTIME intervals, so had to allow for this
after calling the goodtime() method on Observation objects
@
text
@d12 1
a12 1
// RCS: $Id: GapUtilities.C,v 1.4 2000/04/19 16:29:25 jonv Exp $
d781 2
a782 2
    cout << "Looking at obs: " << obs->name() << "   "
         << obs->observation_start()<<" "<<obs->observation_stop() <<endl;
d798 5
a802 5
            XTETime g1( ((*gtl)[j])->from );
            XTETime g2( ((*gtl)[j])->to );
            cout << "Looking at GTI: "
                 << g1.getDate(XTETime::UTC, XTETime::DATE) << " to "
                 << g2.getDate(XTETime::UTC, XTETime::DATE) << endl;
@


1.4
log
@changed getHRfilename routine so that if it finds
2 versions of a human readable, it uses the higher version
@
text
@d12 1
a12 1
// RCS: $Id: GapUtilities.C,v 1.3 2000/01/10 17:27:02 jonv Exp $
d781 2
a782 2
    //cout << "Looking at obs: " << obs->name() << "   "
    //     << obs->observation_start()<<" "<<obs->observation_stop() <<endl;
d794 2
a795 1
        for(j=0; j<gtl->entries(); j++) {
d797 29
a825 27
          // for debugging:
          //XTETime g1( ((*gtl)[j])->from );
          //XTETime g2( ((*gtl)[j])->to );
          //cout << "Looking at GTI: "
          //     << g1.getDate(XTETime::UTC, XTETime::DATE) << " to "
          //     << g2.getDate(XTETime::UTC, XTETime::DATE) << endl;
          // end debugging section

          if ( overlap = find_overlap((double) gapStart, (double) gapStop,
                                      (double) ((*gtl)[j])->from,
                                      (double) ((*gtl)[j])->to ) )
          {
            
            thisGap.resetRange( overlap[0], overlap[1] );

            gapList->orRange( thisGap );

            // total_good_time_missed += overlap[1] - overlap[0];

            // Or you could be less stringent and just look at individual gaps:
            // (you have to worry about the return value if no gap is found...)
            //   => just put a return 0 in the if (obs2>gapStop) block below 

            // because you know no gap big enough has been found.
            // if ( (overlap[1] - overlap[0]) > min_gap_size) {
            //  return 1;
            // }
a827 1

@


1.3
log
@cleanup of old comments; removing un-needed "777" flags
@
text
@d12 1
a12 1
// RCS: $Id: GapUtilities.C,v 1.2 1999/11/16 20:43:26 jonv Exp $
d244 40
d323 1
d328 5
a332 2
      filename = file;
      break;
@


1.2
log
@fixed memory leaks associated with the getDirListing routine
@
text
@d12 1
a12 1
// RCS: $Id: GapUtilities.C,v 1.1 1999/11/10 17:47:24 jonv Exp $
a536 4
  // 777
  // cout << "LOADING NEW OBS REPORT: " << requestedMissionDay<< " (prev:"
  //     << attachPrevWeek << ")  (next:"<< attachNextWeek<<")" << endl;

a538 6
  // 777
  //if (r) 
  //  cout << "New HR loaded" << endl;
  //else
  //  cout << "New HR has a problem - its NULL" << endl;

d672 1
a672 1
    // 777
d681 1
a681 1
      // 777
d736 1
a736 1
    // 777
d752 1
a752 1
          // 777
d758 1
a758 1
          // end 777
@


1.1
log
@Initial revision
@
text
@d12 1
a12 1
// RCS: $Id$
d139 1
d199 1
d292 1
d294 1
a294 2
    char *theHRfilename = 
      new char [ dir.length() + filename->length()+3 ];
a297 3
    return theHRfilename;
  } else {
    return NULL;
d299 5
@

head	1.7;
access
	gromeo;
symbols;
locks; strict;
comment	@ * @;


1.7
date	2000.07.24.17.22.03;	author jonv;	state Exp;
branches;
next	1.6;

1.6
date	2000.07.19.00.46.52;	author jonv;	state Exp;
branches;
next	1.5;

1.5
date	99.12.14.19.50.43;	author jonv;	state Exp;
branches;
next	1.4;

1.4
date	99.12.12.22.05.06;	author jonv;	state Exp;
branches;
next	1.3;

1.3
date	99.11.19.17.13.17;	author jonv;	state Exp;
branches;
next	1.2;

1.2
date	99.11.16.20.45.29;	author jonv;	state Exp;
branches;
next	1.1;

1.1
date	99.11.10.17.47.24;	author jonv;	state Exp;
branches;
next	;


desc
@ for helping with data gaps
@


1.7
log
@fixed minor arg processeing bug
@
text
@// ===========================================================================
// File Name   : gapAnalyzer.C
// Subsystem   : Data Management
// Programmer  : Jon Vandegriff, Raytheon ITSS
// Description:
//
// This program merges the gap information with the GOODTIMEs from
// an Observation (these come from the Human Readable style files).
// It outputs a file which is to be parsed by a perl program for displaying
// gaps and the affected observations.
//
// RCS:
static const char* const rcsid =
"$Id: gapAnalyzer.C,v 1.6 2000/07/19 00:46:52 jonv Exp $";
//
// ===========================================================================


#include <iostream.h>
#include <fstream.h>
#include <string.h>  // I think ObsReport.h needs this...  - yes it does!!
#include <stdlib.h>
#include <ctype.h>
#include <rw/ordcltn.h>
#include <rw/collint.h>
#include <rw/collstr.h>
#include <rw/cstring.h>


#include <XTETime.h>
#include "GapList.h"
#include "GapUtilities.h"
#include "ObservationGaps.h"

int action = 1;
int verbose = 0;
char *gaps_from_file = NULL; // user can set for viewing most recent day
int useObsCat = 0;

RWOrdered *days     = NULL;
RWOrdered *outfiles = NULL;

RWOrdered *apidArgs;
int numApids = 28;
int stdApidList[] = { 1, 14, 15, 16, 17, 20, 24, 53, 55, 57, 59,
               61, 63, 69, 70, 73, 74, 77, 79, 80, 83, 86, 89,
               90, 91, 92, 93, 94 };
int *apids;


void printGapAndGtiIntersections( GapList *,const char *, int *, int);

void usage(const char *badArg)
{
  if (badArg)
    cerr << "Invalid argument: " << badArg << endl << endl;

  cerr << "gapAnalyzer -d day1 [day2 day3 ...] -o outfile1 "
       << "[outfile2 outfile3 ...]" << endl
       << "            [-n] [-v] [-obscat] [-force gaps_from_this_file]"
       << endl;
}

int isValidInt(char *arg) {
  for(int i=0; i<strlen(arg); i++) {
    if (!isdigit(arg[i])){
      return 0;
    }
  }
  return 1;
}

void parse_arguments( char **&argv)
{
  apidArgs = NULL;
  apids    = stdApidList;

  int hasError = 0;


  while (*++argv) {
    if (strcmp(*argv, "-n") == 0) {
      action = 0;

    } else if (strcmp(*argv, "-v") == 0) {
      verbose = 1;

    } else if (strcmp(*argv, "-obscat") == 0) {
      useObsCat = 1;


    } else if (strcmp(*argv, "-force") == 0) {
      // force reading of gaps from single file
      if( *++argv ) {
        gaps_from_file = new char [strlen(*argv)+1];
        strcpy(gaps_from_file, *argv);
      } else {
        cerr << "The -force argument requires a filename afterwards."<<endl;
        usage(NULL);
        exit(1);
      }

    } else if (strcmp(*argv, "-d") == 0) {

      days = new RWOrdered( 10 );
      int dayError = 0;
      while (*++argv) {
        if (*argv[0] == '-') {
          break;
        }
        if ( !isValidInt(*argv) ) {
          cerr << "Invalid value for a mission day: \""<<*argv << "\"" <<endl;
          dayError = 1;
        }
        days->insert( new RWCollectableInt (atoi(*argv)) );
      }
      argv--;

      if (dayError) {
        hasError = 1;
        break;
      }



    } else if (strcmp(*argv, "-o") == 0) {

      outfiles = new RWOrdered( 10 );
      while (*++argv) {
        if (*argv[0] == '-') {
          break;
        }
        outfiles->insert( new RWCollectableString(*argv) );
      }
      argv--;

    } else if (strcmp(*argv, "-a") == 0) {
      apidArgs = new RWOrdered(30);
      int apidError = 0;
      while (*++argv) {
        if (*argv[0] == '-') {
          break;
        }
        if ( !isValidInt(*argv) ) {
          cerr << "Invalid value for an apid: \"" << *argv << "\"" <<endl;
          apidError = 1;
        }
        apidArgs->insert( new RWCollectableInt (atoi(*argv)) );
      }
      argv--;

      if (apidError) {
        hasError = 1;
        break;
      }

    } else {
      usage( *argv );
      hasError = 1;
    }
  }

  if (hasError) {
    cerr << "Fatal argument parsing error - exiting." << endl;
    exit(1);
  }

  if ( !days ) {
    cerr << "The -d argument is required and it must have at least one "
         << "day after it." << endl;
    usage(NULL);
    exit(1);
  }

  if (apidArgs) {
    numApids = apidArgs->entries();
    if (!numApids) {
      cerr << "No apids specified after the -a option" << endl;
      usage(NULL);
      exit(1);
    }
    apids = new int [numApids];
    for(int i=0; i<numApids; i++) {
      apids[i] = (  (RWCollectableInt *) (*apidArgs)[i] )->value();
    }
  }

  if (!outfiles) {
    cerr << "After the -o option, you must specify one output file name "
         << "for every day " << endl 
         << "that you gave after the -d option." <<endl;
    usage(NULL);
    exit(1);
  }


  if ( days->entries() != outfiles->entries() ) {
    cerr << "There must be the same number of days after the -d option "
         << "as there are " << endl
         << "filenames after the -o option." << endl;
    usage(NULL);
    exit(1);
  }

  if( gaps_from_file ) {
    if( days->entries() != 1 ) {
      cerr << "If you use the -force option, you must also have exactly "<<endl
           << "one day after the -d option" << endl;
      usage(NULL);
      exit(1);
    }
  }

}


ObsReport *getObsCat( int day ) {

  char* socfits = getenv("SOCFITS");

  if( socfits == 0 ) {
    cerr << "Must define SOCFITS environment variable" << endl;
    exit(1);
  }

  RWCString OCname(socfits);
  OCname += "/obscat/OCday";
  char nums[10];
  sprintf(nums,"%04d", day);
  OCname += nums;

  FILE *OC;

  if( ( OC = fopen(OCname.data(), "r") ) == NULL ) {
    cerr << "Can't open file " << OCname.data() << endl;
    exit(1);
  }

  ObsReport *obsReport = new ObsReport();

  obsReport->parse(OC, verbose);

  int obsEntries = obsReport->entries() ;
  if ( obsEntries < 1 ) {
    cerr << "Error parsing ObsCat file " << OCname.data() <<endl;
    exit(1);
  }

  return obsReport;

}


void studyObservationsForGaps(int day, int Napids, int *apidList,
                              const char *filename)
{
  GapList *gapList;
  if( gaps_from_file ) {
    // this is the special case where the user wants to look at a single day
    // withput merging the day boundaries between gap lists
    gapList = new GapList(gaps_from_file, day, verbose);
  } else {
    gapList = GapList::getFilledOutGapList( day, verbose );
  }

  if (!gapList) {
    cerr << "ERROR - unable to get gap list for the day " << day << endl;
    exit(1);
  }


  ofstream outFile(filename);
  if (!outFile) {
    cerr << "ERROR - can't open " << filename << " for output" << endl;
    exit(1);
  }


  unsigned long dayStart = metAtDayBoundary( day, 1 );
  unsigned long dayStop  = metAtDayBoundary( day, 0 );

  ObsReport *hr;

  if (useObsCat)
    hr = getObsCat( day );
  else
    hr = getStaticObsReport(day, 1, 1);


  if (!hr) {
    cerr << "Unable to load human readble file for day " << day << endl;
    exit(1);
  }

  int numObs = hr->entries();
  int i = 0;


  ObservationGaps *obsGaps = new ObservationGaps [numObs];
  int obsCounter = 0;

  ObservationGaps::staticSetup(Napids, apidList, day, gapList );

  for (i=0; i< numObs; i++) {

    const Observation *anObs = hr->getObservation( i );

    // Include all the obs which start in or on the start of the day given

    if (!useObsCat) {  // (with the ObsCats, look at every observation)

      if ( anObs->observation_start() < dayStart ) {
        continue;  // skip this one since it starts before the day of interest
      }
      if ( anObs->observation_start() >= dayStop ) {
        break;  // that's it - no more observations which start in the 
                // day of interest
      }
    }

    obsGaps[obsCounter].setObs( anObs );

    obsGaps[obsCounter].fillGaps();

    obsCounter++;
  }

  ObservationGaps::printHeader( outFile );
  for(i=0; i<obsCounter; i++) {
    obsGaps[i].print( outFile );
  }

  outFile.close();

  delete gapList;
  delete [] obsGaps;

  return;

}



void main(int , char **argv)
{

  parse_arguments(argv);

  int i;
  for(i=0; i< days->entries(); i++) {

    RWCollectableString *outFile = (RWCollectableString *) ((*outfiles)[i]);
    RWCollectableInt    *thisDay = (RWCollectableInt *) ((*days)[i]);

    studyObservationsForGaps( thisDay->value(), numApids, apids,
                              outFile->data() );
  }

}

@


1.6
log
@modified the programs:
   GapList.h GapList.C gapFinder.C gapAnalyzer.C gapview.pl
to be able to show gaps for the most recent day using
a -force option on gapview.pl as in
gapview.pl -force -md 2389
The GapList class had to be modified, as did everything else
in the chain.
@
text
@d14 1
a14 1
"$Id: gapAnalyzer.C,v 1.5 1999/12/14 19:50:43 jonv Exp $";
d94 8
a101 2
      gaps_from_file = new char [strlen(*++argv)+1];
      strcpy(gaps_from_file, *argv);
@


1.5
log
@added -obscat option to the usage routine
@
text
@d14 1
a14 1
"$Id: gapAnalyzer.C,v 1.4 1999/12/12 22:05:06 jonv Exp $";
d37 1
d59 2
a60 1
       << "[outfile2 outfile3 ...]" << endl<< "            [-n] [-v] [-obscat]"
d92 4
a95 2


d199 9
d251 8
a258 1
  GapList *gapList = GapList::getFilledOutGapList( day, verbose );
d277 1
@


1.4
log
@added -obscat option for allowing obscats to be viewed with the gapview.pl program
@
text
@d14 1
a14 1
"$Id: gapAnalyzer.C,v 1.3 1999/11/19 17:13:17 jonv Exp $";
d58 1
a58 1
       << "[outfile2 outfile3 ...]" << endl << "            [-n] [-v]"
@


1.3
log
@fixed 2 small spelling errors
@
text
@d14 1
a14 1
"$Id: gapAnalyzer.C,v 1.2 1999/11/16 20:45:29 jonv Exp $";
d37 1
d82 1
d86 3
d198 37
d256 6
a261 1
  ObsReport *hr = getStaticObsReport(day, 1, 1);
d283 9
a291 6
    if ( anObs->observation_start() < dayStart ) {
      continue;  // skip this one since it starts before the day of interest
    }
    if ( anObs->observation_start() >= dayStop ) {
      break;  // that's it - no more observations which start in the 
              // day of interest
a292 1

@


1.2
log
@added capability to do more than one file at a time
@
text
@d14 1
a14 1
"$Id: gapAnalyzer.C,v 1.1 1999/11/10 17:47:24 jonv Exp $";
d199 1
a199 1
    cerr << "ERROR - unalbe to get gap list for the day " << day << endl;
d206 1
a206 1
    cerr << "ERROR - cna't open " << filename << " for output" << endl;
@


1.1
log
@Initial revision
@
text
@d14 1
a14 1
"$Id$";
d26 1
d38 2
a39 3
int day = 0;

char *outFilename = NULL;
d56 2
a57 1
  cerr << "gapAnalyzer -d day -o output_filename [-n] [-v]"
a71 3
  day = 0;
  outFilename = NULL;

d77 1
d83 4
a86 2
    } else if (strcmp(*argv, "-o") == 0) {
      outFilename = *++argv;
d90 15
a104 1
      if ( !isValidInt(*++argv) ) {
d108 13
a120 1
      day = atoi( *argv );
d153 2
a154 2
  if ( !day ) {
    cerr << "The -d argument is required and it must have a "
d173 13
a185 2
  if (!outFilename) {
    cerr << "You must specify an output file name after the -o option." <<endl;
d259 2
a260 1
  delete obsGaps;
d273 2
a274 1
  studyObservationsForGaps(day, numApids, apids, outFilename);
d276 6
@

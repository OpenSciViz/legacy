head	1.4;
access
	jonv;
symbols;
locks; strict;
comment	@# @;


1.4
date	2000.04.13.12.15.22;	author jonv;	state Exp;
branches;
next	1.3;

1.3
date	99.08.16.13.01.54;	author jonv;	state Exp;
branches;
next	1.2;

1.2
date	99.05.13.15.15.54;	author jonv;	state Exp;
branches;
next	1.1;

1.1
date	99.05.13.15.06.42;	author jonv;	state Exp;
branches;
next	;


desc
@file descrobing the process needed to make new FITS templates
from a new EdsConfigDB as delivered from MIT (usually Ed Morgan)
@


1.4
log
@added a few comments about making sure any non-EDS (such as ASM)
FITS templates get included in the FT distribution.
@
text
@Installing a new EdsConfigDB and new FITS templates

 Jon Vandegriff
 Raytheon ITSS
 May 13, 1999

RCS: $Id: making_new_EDS_templates.txt,v 1.3 1999/08/16 13:01:54 jonv Exp $

This document describes how to make and install a new EdsConfigDB and
the corresponding new EDS FITS templates.  A new database and new
templates are needed whenever new modes are installed on the
spacecraft.

Arnold originally gave me a similar set of instructions for doing this
very activity, and so this is the second generation document which
includes all the hand-written notes I made, plus other discoveries
which came up along the way.

Ed Morgan is almost always involved in this process, since he is the 
one who makes/provides the EdsConfigDB (some have though of the database
as Ed's Config DB.)  Hopefully, Ed will be present to assist with the
installation of the DB and the FITS templates, since he is one of the
few remaining on the XTE project who knows approximately what should 
happen during this installation.

PLEASE NOTE that while making the FITS templates, that only the EDS
templates should be changed.  Any modification (deleting, moving,
renaming, changing) to spacecraft templates could have disastrous
results since there is no mechanism for making them again.  (Randy
supposedly was working on getting the project data base into usable
shape, but the task proved too overwhelming.) As will be evident below,
all the templates live in several different places where they are
installed, so they could be recovered from one of their installed
locations, but it's best not to rely on this mechanism as a backup
system.

There is a separate Build tree which Arnold Rots and Randy Barnette
used for (among other things) making the FITS templates.
Unfortunately, many files in this Build are either unique or are
somewhat independent of their counterparts in the normal, real,
current Build tree.  This may be repaired in the future, but until
then, all the operations below must be done in the special build
tree. Also, the environment and file ownership of this special build
is such that Arnold's original cook book instructions began with
"log into xdev as soccm and cd to $SOCHOME/etc"
and offered no further explanation as to the reasoning for needing a
specific environment.  Hopefully this document helps explain things
more.

Since it seems a little scary to assume that the soccm .cshrc will
always in the future give you the environment you need for making FITS
templates, the .cshrc file Arnold was relying on is included below for
reference.  Key parts include the $SOCHOME and $SOCOPS variables, but
also the $PATH and $LD_LIBRARY_PATH.  Undoubtedly some and maybe much
of the .cshrc file will be outdated, but nevertheless, the environment
it contains works fine for the purpose of making FITS templates as of
this writing.

Here are the instructions:

1. Setting up:
   You will need access to the /homebck filesystem, so log into
   the machine where that resides as user soccm.  
   $SOCHOME needs to point to /homebck/Build.
   You need to get your environment set up properly.
   If things have not changed in the SOF since this writing (i.e.,
   /homebck is still on xdev, and the soccm .cshrc file has not
   been changed), then you can easily get set up by logging into 
   xdev as soccm.

   If the SOF has been rearranged in any way, then you may need
   to modify the environment to make it equivalent to
   that given in the .cshrc file provided.
   The essential feature is to do all your work in the local Build area
   which lives on the /homebck partition, setting $SOCHOME appropriately
   so that the instructions below will be performed in the right directories.

2. Installing the new EdsConfigDB into the test build area:

   In $SOCHOME/etc there should be an EdsConfigDB and and EdsConfigDB.bak
   (there may also be other versions such as a .bak.1 or .bak.2;
   the highest number is the most recent backup version).
   The plan is to get rid of the EdsConfigDB.bak direcotry, put the 
   new database into the EdsConfigDB.bak directory.  After verifying
   that differences between the currently installed database and the 
   about-to-be-installed database (in EdsConfigDB.bak) are expected, 
   the EdsConfigDB and EdsConfigDB.bak direcotries can then be swapped.
   Of course there will be differences because that's why a new one is
   being installed, but you'll probably need Ed Morgan at this point to 
   check that the only place differences exist are in the modes which 
   he has changed or added.

   Here are the steps to take:
   1. remove all the files in EdsConfigDB.bak 
        (or move the directory to EdsConfigDB.bak.N, incrementing N as 
         needed, and create another directory named EdsConfigDB.bak)

cd $SOCHOME/etc/EdsConfigDB.bak  --OR--  cd $SOCHOME/etc
rm -fr *                                 mv EdsConfigDB.bak EdsConfigDB.bak.N
                                         mkdir EdsConfigDB.bak
                                         cd EdsConfigDB.bak

   2. into the EdsConfigDB.bak directory, expand the tar file provided
      by Ed Morgan which contains the new EdsConfigDB.  Usually it is
      located in his home directory in the SOF and thus is named ~ehm/db.tar

tar xf ~ehm/db.tar

   You can specify tar xvf, but the verbose option makes tar take a lot 
   longer since there are thousands of files.

   Now see if the differences between the new config (in EdsConfigDB.bak)
   and the previous installation (in EdsConfigDB) are as expected.
   Unless you are an EDS expert, just have Ed Morgan look at the 
   differences and he can tell you if there are any surprises.
   Make sure you are in $SOCHOME/etc/EdsConfigDB.bak and type

diff -iw byName $SOCHOME/etc/EdsConfigDB/byName

   (-i ignores case differences and -w ignores white space differences)

   Assuming everything is OK with Ed, switch EdsConfigDB and EdsConfigDB.bak
   like this:

cd $SOCHOME/etc
mv EdsConfigDB.bak EdsConfigDB.new
mv EdsConfigDB     EdsConfigDB.bak
mv EdsConfigDB.new EdsConfigDB

   The EdsConfigDB is now in place and so its time to use it to make 
   FITS templates.



3. Making new FITS templates:

   FITS templates construction is done in the Project Database directory, just 
   below the XFF source code area, in the directory
   $SOCHOME/src/DataManagement/XFF/pdb.  Go there now.

cd $SOCHOME/src/DataManagement/XFF/pdb

 
   When FITS templates are made, they are placed into this pdb directory
   by procedures defined in a Makefile. Then, those newly made files
   should also be copied (just for the purpose of having a backup, I 
   suppose) into the directory neweds/ just below the pdb directory.  
   The instructions for doing this with the current installtion are below,
   but the point here is that you want to verify that this was done last
   time new EDS FITS templates were made.
   The files in question are the FTeds* files, and they should be in the
   pdb directory and the neweds directory just below the pdb direcotry.
   If these FTeds* files were not placed into neweds last time, 
   then move them there now (you may have to create the directory):

mv FTeds* neweds/

   If there are already files in the neweds/ directory, then the above mv
   won't be necessary - you can just remove the ones in the pdb directory.
   But before you remove the files from the pdb directory, its a good idea
   to verify that they are indeed the same as the file sin the neweds 
   directory.  Here are the commands to check for differences, and then 
   (assuming no differences were found) delete the files from the pdb 
   directory (using csh or tcsh syntax):

foreach f (FTeds*)
diff $f neweds/$f
end

rm FTeds*

   NOTE: Please be very careful with this deletion!!! See cautionary
         comments below.

   The oldest set of FTeds* files are kept in the directory oldeds/.
   Once you establish that the newest set of FTeds files are
   saved in neweds/, you should then remove the oldeds directory

rm -fr oldeds

   And then move the neweds/ directory to the oldeds/ directory.

mv neweds oldeds

   Then when the new EDS FITS templates are done, you will create
   another neweds directory to place the newly create, most current
   files.  (The commands for this will come later.)

   Since many people have apprehension about deleting items from the SOF,
   there may be a set of oldeds (or oldeds.bak.1, etc) directories.
   If you don't want to delete the oldeds directory, then you may
   also follow this convention, although some have argued that the 
   safety offered by such practices is outweighed by the confusion
   created...

   CAUTIONARY NOTE:
   One caution at this point cannot be overemphasized enough.  Only 
   the FTeds (i.e., EDS FITS templates) files should be messed with.
   The spacecraft (S/C) FITS templates in this directory cannot be 
   remade.  No one left on the project knows how to get these out of 
   the Project Database, and so if they are destroyed or altered, then
   the damage is unrepairable --  there will be no way to make FITS 
   files from the S/C data.  However, these templates do live in more  
   than one place.  As will be seen below, all the FITS templates, 
   including S/C and EDS templates, are installed in the SOF and at XSDC,
   so if they get corrupted in the /homebck directory, they can possibly
   be copied in from somewhere one of these two locations.  Nevertheless,
   be careful!!

   Now it is time to make new FTeds* files.  This is done as a several
   step process by a Makefile in the pdb directory.  Before running 
   the make process, you will want to move the log files from the 
   previous run out of the way.  There are 4 log files, and so if you
   want to keep them, you can use these commands

mv EDSsc.ddt     EDSsc.ddt.bak.N
mv EDSsc.ddt.log EDSsc.ddt.log.bak.N
mv EDSsc.log     EDSsc.log.bak.N
mv EDSsc.err     EDSsc.err.bak.N

   where N is the next highest number not used by the naming convention.
   These files take up little space, and it is often useful to see 
   what errors occurred in the past, because then you can tell if the 
   current errors are expected or not.  So I think the benefit of 
   keeping these files around outweighs the confusion of the multiple 
   version numbers.

   Now let the makefile do its thing:

make edstemplates

   Several steps will be taken by the makefile.  The C program used
   in the process may need to be remade (this happened when we 
   upgraded the compiler for example), but in most cases the program
   should be up to date.
   As a side note, at the time of this writing, the Centerline
   compiler we are using (ObjectCenter 2.2.0) is not fully compatible
   with Solaris 2.6.  In order to get it to work, we had to slightly
   modify a Solaris 2.6 header file.
   In case this does not get fixed, these are the differences you need
   to know in order to get the C program to compile:

   % diff /usr/include/unistd.h /usr/include/unistd.h.original 
   250,259c250
   < #ifdef __CLCC__
   < /* This if clause was added first because the Centerline
   <    compiler already has a declaration of this function
   <    and the second one here can cause compilation errors
   <    depending on the order of the include files 
   < */
   < /* This is the declaration used by Centerline for this function:
   <   extern int gethostname(char *, int);
   <  */
   < #elif defined(_XPG4_2)
   ---
   > #if defined(_XPG4_2)

   Basically, an extra #ifdef block was added to the header file to 
   keep Solaris from declaring a function already declared by Centerline.
   Hopefully this will be resolved with Centerline...

   After 5 to 10 minutes, the make edstemplates process will be done,
   and a new set of FTeds* files will be present in the pdb directory.

   Check the log and error files to see if there were any unusual 
   problems.  Ed Morgan has the ability to see if there is anything 
   odd, so probably just ask him to have a look at it.
   One check to do is to make sure that the last template created 
   was indeed the last template in the list.  The make process can 
   somehow silently fail, prematurely halting and not making some 
   of the templates.  Ed Morgan discovered this once, and the cause 
   was that one of the EdsConfigDB files had no contents (i.e. it 
   was a 0 length file).

   Now make a directory neweds

mkdir neweds

   and copy the new FTeds* files into the new directory:

cp FTeds* neweds


   Now do a diff of the neweds and oldeds directories

diff neweds oldeds

   and once again see if all the differences are expected.  Ed Morgan 
   is your resource for this determination.

   If everything looks good, then remove old FTeds templates from 
   $SOCHOME/etc/FT, and copy in the newly made ones.

rm $SOCHOME/etc/FT/FTeds*
cp FTeds* $SOCHOME/etc/FT

   Once again, be very careful not to delete any S/C FITS templates.

   Important note:
   Once, Ed delivered new configs for the ASM.  For some reason, these
   FITS templates ended up as something like  FTf8.84000027 and 
   FTf9.84000027.  Notice that these would not (and were not!) copied
   by the above instructions.  So to be very safe, make sure that all
   the newly created fits template files (FT* files with creation dates
   on the day you do this) are in the $SOCHOME/etc/FT directory.

   You will have to change the "Jan 11" date in this command, but this 
   will look for missing files.  (This requires the tcsh).
   (Be sure that if the date is a single digit day, that you put 2 spaces
   as in "Apr  4".)

foreach f ( FT* )
 ls -l $f | grep "Jan 11" > /dev/null
 if ( $? ) then
 else
   if ( -e $SOCHOME/etc/FT/$f ) then
   else
     echo NO file $f in $SOCHOME/etc/FT
   endif
 endif
 end

   The above method takes about 5 minutes.  Here is a faster (about 100 
   times faster!) and much less clear approach (which also might win an
   obfuscated Perl contest).
   (It will prompt you for the date. The same caveat from the above
   method about single digit dates applies here too.)

perl -e 'print "Enter a Month Day to match from \"ls -l\" (as in Mar  3) : "; $date=<STDIN>;chomp $date;opendir(D,".");@@F=readdir(D);closedir(D);foreach (@@F){ if(/^FT/){$t=(stat($_))[9];$mon=("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec")[ (localtime($t))[4] ]; $day=(localtime($t))[3];$str=sprintf "%s %2d",$mon,$day; if ($date =~ /$str/) { unless( -e $ENV{"SOCHOME"}."/etc/FT/$_" ) { print "No $_ from $date in \$SOCHOME/etc/FT\n"; } } } }'


   Whatever method you use, make sure that all the newly created FITS 
   templates that you want to use have been put into $SOCHOME/etc/FT,
   and don't destroy and S/C templates!



4.  Installing the new EDS configuration database and the new FITS templates.

   Now the new EdsConfigDB and the full set of FITS templates (not just
   the eds ones - we do it this way for convenience sake) should be 
   installed in two places: the SOF and XSDC.

   Create tar files of the EdsConfigDB and the FITS templates:

cd $SOCHOME/etc/FT
tar cf ~/ft.tar .

cd $SOCHOME/etc/EdsConfigDB
tar cf ~/eds.tar .

   Note that the .tar files from a previous installation may already be 
   in the soccm home directory and may need to be deleted.  Also note 
   that $SOCHOME here still refers to the /homebck/Build area.

   Now, in the SOF go to the operational area where the EdsConfigDB and 
   FITS templates are kept, and install the new files.  
   Note that we are cd'ing to /sochome, and not $SOCHOME.
   Also, note that in each case the .bak directory with the 
   oldest files is first cleared out, then filled with the new files, 
   and then finally switched with the current, non-".bak" directory.

cd /sochome/etc/EdsConfigDB.bak
rm -fr *
tar xf ~/eds.tar
cd ..
mv EdsConfigDB.bak EdsConfigDB.new
mv EdsConfigDB     EdsConfigDB.bak
mv EdsConfigDB.new EdsConfigDB

cd /sochome/etc/FT.bak
rm -fr *
tar xf ~/ft.tar
cd ..
mv FT.bak FT.new
mv FT     FT.bak
mv FT.new FT


   Now do the same thing at XSDC.  FTP to adfmm1 as sofuser, and put
   the ft.tar and eds.tar files in ~sofuser:

cd ~soccm
ftp adfmm1 (user=sofuser)
binary
put ft.tar
put eds.tar
quit

   Now telnet to adfmm1 as sofuser and go through the same steps as
   done in the SOF for installing the files in the active area.

telnet adfmm1 (log in as user sofuser)

cd /ar2vol2/sochome/etc/EdsConfigDB.bak
rm -fr *
tar xf ~/eds.tar
cd ..
mv EdsConfigDB.bak EdsConfigDB.new
mv EdsConfigDB     EdsConfigDB.bak
mv EdsConfigDB.new EdsConfigDB

cd /ar2vol2/sochome/etc/FT.bak
rm -fr *
tar xf ~/ft.tar
cd ..
mv FT.bak FT.new
mv FT     FT.bak
mv FT.new FT


5.  Notifying everyone about the changes.

   Its important to let all the relevant people know about the upgrade,
   so that they can be watchful for possible problems. Here's the list:

    Bob Patterer, all people who use the socops account in the SOF,
    Ed Morgan, the xteplan mail exploder list, the SOF/GOF
    software team.

   You can also ask these people to forward the info to others who
   might also want to know.





Appendix A - a snapshot of the soccm .cshrc file

# @@(#)cshrc 1.11 89/11/29 SMI

setenv OPENWINHOME /usr/openwin
setenv OCHOME /socsoft/CenterLine
setenv PURIFYHOME /socsoft/pure/purify-3.0-solaris2

#
# SOC environment variables
#

#setenv SOCMASTER  /sochome
#setenv SOCHOME    /sochome
setenv SOCHOME    /homebck/Build
setenv SOCLOCAL   /socsoft/local
setenv SOCOPS     $SOCHOME/socops
setenv SOCLOG     $SOCOPS/log
setenv SOCFITS    $SOCOPS/FITS
setenv ARCH       `uname``uname -r | cut -c1`
setenv SPIKEROOT  $SOCOPS/spikeroot/$ARCH
#setenv COFLAGS    -fSOC4_3
setenv XFILESEARCHPATH $SOCHOME/lib/%T/%N%S:/usr/openwin/lib/locale/%L/%T/%N%S:/usr/openwin/lib/%T/%N%S
setenv XUSERFILESEARCHPATH ./%N%S

#
# Define our path here
#
   
set path = (/bin /usr/local/bin /usr/ucb /usr/ccs/bin)
set path = ($path $SOCHOME/qfbin/$ARCH $SOCHOME/bin/$ARCH)
set path = ($path $SOCHOME/test/bin/$ARCH)
set path = ($path $OPENWINHOME/bin/xview /usr/bin)
set path = ($path /usr/lib/lp/postscript /etc /usr/etc)
set path = ($path /usr/bin/X11 $OPENWINHOME/bin)
set path = ($path /opt/ftools/bin.host)
set path = ($path /socsoft/local/bin /socsoft/local/lib/lw . ~/bin)
set path = ($path /opt/SUNWspro/bin $OCHOME/bin $OCHOME/sparc-solaris2/bin)
     
set cdpath = (.. ~ $SOCHOME/src)

#
# account customizations
#

setenv EDITOR     vi
#biff y
umask 023

#
# C shell customization
#

set history = 1000
set notify
set noclobber
#set mail = (30 /usr/spool/mail/$user)
set filec
set fignore=".o"
set prompt = "`hostname`:$user(\!) "

#
# library path
#
   
setenv LD_LIBRARY_PATH ${SOCHOME}/lib/${ARCH}:/usr/ccs/lib:$OCHOME/clc++/sparc-solaris2:$OCHOME/clcc/sparc-solaris2:/opt/SUNWspro/SC3.0.1/lib:/socsoft/local/lib:/usr/local/lib:/usr/openwin/lib:/usr/ucblib:/socsoft/motif12/usr/lib

#
# man path
#
   
setenv MANPATH /usr/man:/usr/local/man:$SOCLOCAL/man:$SOCHOME/man:$PURIFYHOME/man:/socsoft/SUNWSpro/man:/socsoft/plotter-6.0p18/man:$OCHOME/man:$OPENWINHOME/share/man:/sochome/man

#
# More SOC stuff
#
setenv CC_CORE $OCHOME/clc++/`$OCHOME/admin/platform`
setenv CCROOT_I `CCroot_I`

#
# Aliases
#
alias h         'history'
alias lo        'logout'
alias ls        'ls -F'
alias ll        'ls -laF'
alias df        'df -k'
alias cd        'cd \!*;echo $cwd'
alias U         'rcs -U RCS/*,v'
alias punt      'set x = `ps -e | grep \!* | head -1` ; kill -9 $x[1]'

alias tae       'source ~/bin/setup'
alias pdm       'objectcenter -pdm -ascii'

alias clean     'make clean     |& tee clean.log'
alias realclean 'make realclean |& tee realclean.log'
alias pubhdrs   'make pubhdrs   |& tee pubhdrs.log'
alias depend    'make depend    |& tee depend.log'
alias publibs   'make publibs   |& tee publibs.log'
alias install   'make install   |& tee install.log'
alias TG        'make \!* |& tee \!*.log'

alias sochome   'cd $SOCHOME'
alias src       'cd $SOCHOME/src'
alias CG        'cd $SOCHOME/src/CommandGeneration'
alias DM        'cd $SOCHOME/src/DataManagement'
alias XFF       'cd $SOCHOME/src/DataManagement/XFF'
alias OC        'cd $SOCHOME/src/DataManagement/XFF/Obscat'
alias HS        'cd $SOCHOME/src/HealthnSafety'
alias SM        'cd $SOCHOME/src/SciMonitoring'
alias DI        'cd $SOCHOME/src/Ingest'
alias MM        'cd $SOCHOME/src/MissionMonitoring'
alias MP        'cd $SOCHOME/src/MissionPlanning'
alias UT        'cd $SOCHOME/src/Utilities/soc'
alias PKT       'cd $SOCHOME/src/PktCCSDS'
alias PKTSC     'cd $SOCHOME/src/PktCCSDS/PktSC'
alias PKTEDS    'cd $SOCHOME/src/PktCCSDS/PktEDS'
alias PKTHEX    'cd $SOCHOME/src/PktCCSDS/PktHexte'
alias PKTPCA    'cd $SOCHOME/src/PktCCSDS/PktPca'
alias UT        'cd $SOCHOME/src/Utilities'
alias HEX       'cd $SOCHOME/src/HEXTE'
alias MIT       'cd $SOCHOME/src/MIT'

alias g+         'genman++ -e -b -t -private -o\!*.3 \!*.h'
alias n+         'nroff -man \!*.3 | more'
   

setenv TAE /socsoft/tae

# FMHOME line added by the FrameMaker user's setup program
setenv FMHOME /opt/src/frame; set path=( $FMHOME/bin $path )

setenv HOST `hostname`
unset autologout
@


1.3
log
@a few minor corrections and/or clarifications
@
text
@d7 1
a7 1
RCS: $Id: making_new_EDS_templates.txt,v 1.2 1999/05/13 15:15:54 jonv Exp $
d298 38
@


1.2
log
@added RCS tag
@
text
@d7 1
a7 1
RCS: $Id$
d26 1
a26 1
PLEASE NOTE that in making the FITS templates that only the EDS
d101 1
d118 1
a118 1
diff -iw byName $SOCHOME/EdsConfigDB/byName
d125 4
a128 3
mv EdsConfigDB     EdsConfigDB.new
mv EdsConfigDB.bak EdsConfigDB
mv EdsConfigDB.new EdsConfigDB.bak
d199 3
a201 3
   The Space Craft FITS templates in this directory cannot be remade.
   No one left on the project knows how to get these out of the 
   Project Database, and so if they are destroyed or altered, then
@


1.1
log
@Initial revision
@
text
@d7 1
@

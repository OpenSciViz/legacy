head	1.1;
access
	soccm
	arots
	jonv
	gromeo;
symbols
	Build6_0:1.1
	Build5_4:1.1
	Build5_3:1.1;
locks;
comment	@ * @;


1.1
date	96.10.28.19.26.22;	author arots;	state Exp;
branches;
next	;


desc
@Argus extractor for ObsCat.
@


1.1
log
@Initial revision
@
text
@//----------------------------------------------------------------------
//
//  File Name   : Argus.C
//  Subsystem   : XFF
//  Programmer  : Arnold Rots, USRA
//  Description : Program to make mail summaries to Argus from ObsCats
//
// .NAME    Argus - main to make summary
// .LIBRARY ObsCat
// .INCLUDE ObsCat.h Config.h Time.h
//
// .SECTION Author
//  Arnold Rots,
//  USRA,
//  <arots@@xebec.gsfc.nasa.gov>
//
// .SECTION DESCRIPTION
//
//
// .VERSION $Revision $
//----------------------------------------------------------------------

static const char* const rcsid = "$Id: Argus.C,v 1.1 1996/10/28 16:19:15 soccm Exp soccm $" ;

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <iostream.h>
#include <fstream.h>
#include <iomanip.h>
#include <limits.h>
#include <math.h>

#include "Config.h"
#include "ObsReport.h"

static char* ProgName ;
static char* socfits = 0 ;
static int theDay = 0 ;
static int debug = 0 ;
static char inFile[PATH_MAX] ;
static FILE *DAP = 0 ;


void usage(void)
{
	cerr << endl
       << "usage: " << ProgName
       << " [-d] (-day n) | (filename)"
       << endl ;
	exit(1) ;
}

//
// Parse the arguments.
//
static void parse_arguments(char**& argv)
{
  debug = 0 ;
  socfits = 0 ;
  theDay = 0 ;
 
  //
  // Parse any arguments.
  //
  //    <file>  -- read from specified file
  //
  while (*++argv ) {
    if( **argv == '-') {
 
           if(strcmp(*argv, "-day")         == 0) theDay = atoi(*++argv) ;
      else if(strcmp(*argv, "-d")           == 0) debug = 1 ;
   
      else {
        cerr << "Unknown option " << *argv << endl ;
        usage();
      }    
    } else if( !DAP ) { // Possibly a file name


      if( ( DAP = fopen(*argv, "r") ) == NULL ) {
        cerr << ProgName << ": Can't open file " << *argv << endl ;
        usage() ;
      }
    }
  }
  if( !DAP ) {
    if ( theDay && (socfits = getenv ("SOCFITS") ) ) {
      sprintf (inFile, "%s/obscat/OCday%04d", socfits, theDay) ;

      if( ( DAP = fopen(inFile, "r") ) == NULL ) {
        cerr << ProgName << ": Can't open file " << inFile << endl ;
        usage() ;
      }

    }
  }
  if( !DAP ) {
    if ( !theDay )
      cerr << ProgName << ": Must supply input ObsCat" << endl ;
    else if ( !socfits )
      cerr << ProgName << ": Must set $SOCFITS" << endl ;
    usage() ;
  }
}
 
int main(int argc, char **argv)
{
  int error = 0 ;

  ProgName = argv[0] ;

  parse_arguments(argv) ;

//
//   ----------------
// -- Make ObsReport --
//   ----------------
//
  int obs_count = 0 ;

  // Parse the input file, get some parameters

  ObsReport obsReport ;

  obsReport.parse(DAP, debug) ;

  int obsEntries = obsReport.entries() ;
  if ( obsEntries < 1 )
    error = 1 ;
  if ( error ) {
    cerr << ProgName << ": ObsCat parsing error." << endl ;
    exit (error) ;
  }
  const Observation* obs = obsReport.getObservation(0) ;
  unsigned long obsstart = obs->observation_start() ;
  obs = obsReport.getObservation(obsEntries-1) ;
  unsigned long obsstop = obs->observation_stop() ;

  fclose(DAP) ;

  if ( debug )
    cerr << "parsed file has " << obsEntries
      << " entries. The first observation is at "
      << obsstart << ", the last ends at " << obsstop << endl ;

//
//   -------------
// -- Do the Work --
//   -------------
//
  int i, n ;
  n = argc ;
  char argus[256] ;

//   This is the mailfile for Argus
  sprintf (argus, "%s/obscat/argus", getenv ("SOCFITS")) ;
  ofstream ARGUS (argus, ios::out) ;
  if ( !ARGUS )
    cerr << ProgName << ": Could not open argus file " << argus << endl ;
  else {
    ARGUS << setiosflags (ios::fixed) ;
    ARGUS.precision (5) ;
  }

//     In the following:
//       ObsType: 0 (Z), 1 (A), 2 (0), 3 ( )
//       The character is kept in seqChar

  for (i=0; i<obsEntries; i++) {

//
//   -------------------------------------
// -- Write the summary to the Argus file --
//   -------------------------------------
//
    obs = obsReport.getObservation(i) ;
    XTETime mstt ((double) obs->observation_start()) ;
    XTETime mstp ((double) obs->observation_stop()) ;
    ARGUS << obs->name() << " | " ;
    ARGUS << mstt.UTmjd() << " | " ;
    ARGUS << mstp.UTmjd() << " | " ;
    ARGUS << obs->source() << " | " ;
    ARGUS << obs->ra() << " | " ;
    ARGUS << obs->dec() << " | " ;
    ARGUS << obs->proposer() << " | " << endl ;
  }

//
//   ----------------
// -- Close and Exit --
//   ----------------
//
  ARGUS.close() ;

  exit (error) ;
}
@

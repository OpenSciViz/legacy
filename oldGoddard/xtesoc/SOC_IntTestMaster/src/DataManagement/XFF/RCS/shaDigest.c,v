head	1.2;
access
	soccm
	arots
	jonv
	gromeo;
symbols
	Build6_0:1.2
	Build5_4:1.2
	Build5_3:1.2
	Build5_2:1.2
	Build5_0_2:1.2
	Build5_0_0:1.2
	Build4_5_3:1.1;
locks;
comment	@ * @;


1.2
date	95.12.06.19.42.13;	author arots;	state Exp;
branches;
next	1.1;

1.1
date	95.11.03.21.11.05;	author arots;	state Exp;
branches;
next	;


desc
@Calculate the SHA Message Digest for each data file.
@


1.2
log
@Added RCS symbol.
@
text
@/* RCS: $Id$ */
#include <stdio.h>
#include <stdlib.h>
/*----------------------------------------------------------------------
 *
 *  shaDigest calculates the 160 bit message digest for a FITS file,
 *  using the Secure Hash Algorithm specified in FIPS 180-1
 *
 *  Usage:
 *    unsigned long *shaDigest (char *fileName) ;
 *
 *  If no argument is given, stdin is assumed.
 *  An array of five unsigned longs is returned, containing the digest.
 *
 *  Arnold Rots, USRA  -  16 October 1995
 *
 ---------------------------------------------------------------------*/
#if defined(__cplusplus) || defined(__STDC__)
int calcDigest (unsigned long *, unsigned long *) ;

unsigned long *shaDigest (char *fileName)
#else
unsigned long *shaDigest (fileName)
char *fileName ;
#endif
{
  FILE *FF ;
  unsigned long i, j ;
  int n, error ;
  static unsigned long h[5] ;
  unsigned long *hp ;
  unsigned long m[720] ;
  unsigned long *mp ;

/*    ------------------------------------------
 *    Open the input file, initialize the digest ---
 *    ------------------------------------------
 */
  error = 0 ;
  if ( !(FF = fopen (fileName, "r") ) )
    error = -1 ;

  h[0] = 0x67452301 ;
  h[1] = 0xefcdab89 ;
  h[2] = 0x98badcfe ;
  h[3] = 0x10325476 ;
  h[4] = 0xc3d2e1f0 ;

/*    --------------------------------------
 *    Read the blocks, accumulate the digest ---
 *    --------------------------------------
 */
  j = 0 ;
  while ( !feof(FF) && !error ) {
    if ( (n = fread (m, sizeof(unsigned long), 720, FF) ) != 720 ) {
      if ( n ) {
	error = 1 ;
	break ;
      }
      else
	break ;
    }
    for ( i=0, mp=m ; i<45 ; i++, mp+=16 )
      calcDigest (h, mp) ;
    j++ ;
  }

/*    ----------------------------------------
 *    Add the padding, detect zero-length file ---
 *    ----------------------------------------
 */
  if ( !error ) {
    if ( j ) {
      m[0] = 0x80000000 ;
      for ( i=1, mp=m ; i<15 ; i++ )
	*(++mp) = 0 ;
      m[15] = j * 2880 * 8 ;
      calcDigest (h, m) ;
    }
    else
      error = 2 ;
  }

/*    ---------------------------------
 *    Close the file, return the digest ---
 *    ---------------------------------
 */
  if ( error >= 0 )
    fclose (FF) ;

  switch (error) {
  case 0:
    hp = h ;
    break ;
  case -1:
    fprintf (stderr, "%s: File %s could not be opened\n",
	     __FILE__, fileName) ;
    hp = NULL ;
    break ;
  case 1:
    fprintf (stderr, "\n%s: Read only %d bytes in record %d\n",
	     __FILE__, n*sizeof(unsigned long), j) ;
    hp = NULL ;
    break ;
  case 2:
    fprintf (stderr, "\n%s: %s appears to be a zero-length file\n",
	     __FILE__, fileName) ;
    hp = NULL ;
    break ;
  default:
    fprintf (stderr, "\n%s: Error %d: unknown error condition\n",
	     __FILE__, error) ;
    hp = NULL ;
    break ;
  }

  return hp ;
}

#if defined(__cplusplus) || defined(__STDC__)
int calcDigest (unsigned long *h, unsigned long *m)
#else
int calcDigest (h, m)
unsigned long *h ;
unsigned long *m ;
#endif
{
  int i ;
  unsigned long a, b, c, d, e ;
  unsigned long w[80] ;
  unsigned long *wp ;
  unsigned long *mp ;
  unsigned long temp ;

/*    ----------------------------------------
 *    Set up the w array and the digest buffer ---
 *    ----------------------------------------
 */
  for ( i=0, mp=m, wp=w ; i<16 ; i++ )
    *(wp++) = *(mp++) ;

  for ( i=16 ; i<80 ; i++ ) {
    temp = *(wp-3) ^ *(wp-8) ^ *(wp-14) ^ *(wp-16) ;
    *(wp++) = (temp << 1) | (temp >> 31) ;
  }

  a = h[0] ;
  b = h[1] ;
  c = h[2] ;
  d = h[3] ;
  e = h[4] ;

/*    -----------------------------------------------------------
 *    Process the digest buffer through the w array in 4 sections ---
 *    -----------------------------------------------------------
 */
  wp = w ;
  for (i=0;i<20;i++) {
    temp = ( (a << 5) | (a >> 27) ) + ( (b & c) | ((~b) & d) ) +
      e + *(wp++) + 0x5a827999 ;
    e = d ;
    d = c ;
    c = (b << 30) | (b >> 2) ;
    b = a ;
    a = temp ;
  }
  for (i=0;i<20;i++) {
    temp = ( (a << 5) | (a >> 27) ) + (b ^ c ^ d) +
      e + *(wp++) + 0x6ed9eba1 ;
    e = d ;
    d = c ;
    c = (b << 30) | (b >> 2) ;
    b = a ;
    a = temp ;
  }
  for (i=0;i<20;i++) {
    temp = ( (a << 5) | (a >> 27) ) + ( (b & c) | (b & d) | (c & d) ) +
      e + *(wp++) + 0x8f1bbcdc ;
    e = d ;
    d = c ;
    c = (b << 30) | (b >> 2) ;
    b = a ;
    a = temp ;
  }
  for (i=0;i<20;i++) {
    temp = ( (a << 5) | (a >> 27) ) + (b ^ c ^ d) +
      e + *(wp++) + 0xca62c1d6 ;
    e = d ;
    d = c ;
    c = (b << 30) | (b >> 2) ;
    b = a ;
    a = temp ;
  }

/*    ----------
 *    The result ---
 *    ----------
 */
  h[0] += a ;
  h[1] += b ;
  h[2] += c ;
  h[3] += d ;
  h[4] += e ;

  return 0 ;
}

/* A test main for shaDigest

#if defined(__cplusplus) || defined(__STDC__)
int main (int argc, char **argv)
#else
int main (argc, argv)
int argc ;
char **argv ;
#endif
{
  unsigned long *md ;
  int i ;

  if ( argc > 1 ) {

    md = shaDigest (argv[1]) ;

    if ( md ) {
      fprintf (stdout, "\n Message digest for file %s:\n", argv[1]) ;
      for (i=0;i<5;i++)
	fprintf (stdout, "    %08x", md[i]) ;
      fprintf (stdout, "\n\n") ;
    }
    else
      fprintf (stdout, "\n Problems in digesting file %s\n\n", argv[1]) ;

    exit (0) ;
  }

  else {

    fprintf (stdout, "\nUsage:  shaDigest <file name>\n\n") ;
    exit (1) ;
  }
}
*/
@


1.1
log
@Initial revision
@
text
@d1 1
@

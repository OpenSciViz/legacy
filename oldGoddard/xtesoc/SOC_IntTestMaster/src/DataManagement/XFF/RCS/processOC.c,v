head	1.14;
access
	soccm
	arots
	jonv
	gromeo;
symbols
	Build6_0:1.9
	Build5_4:1.8
	Build5_3:1.7
	Build5_2:1.7
	Build5_0_2:1.5
	Build5_0_0:1.5
	Build4_5_3:1.5
	Build_4_5_2:1.4
	Build4_5_1:1.3
	Build_4_4_3:1.3
	Build4_4_2:1.2;
locks;
comment	@ * @;


1.14
date	2000.12.08.19.55.28;	author jonv;	state Exp;
branches;
next	1.13;

1.13
date	2000.08.01.12.18.23;	author jonv;	state Exp;
branches;
next	1.12;

1.12
date	98.08.02.23.32.01;	author arots;	state Exp;
branches;
next	1.11;

1.11
date	97.12.12.15.49.23;	author soccm;	state Exp;
branches;
next	1.10;

1.10
date	97.12.12.15.24.12;	author soccm;	state Exp;
branches;
next	1.9;

1.9
date	97.10.27.20.15.36;	author arots;	state Exp;
branches;
next	1.8;

1.8
date	97.03.24.17.22.49;	author soccm;	state Exp;
branches;
next	1.7;

1.7
date	96.09.26.21.46.31;	author arots;	state Exp;
branches;
next	1.6;

1.6
date	96.04.08.12.38.17;	author arots;	state Exp;
branches;
next	1.5;

1.5
date	95.11.13.15.53.43;	author arots;	state Exp;
branches;
next	1.4;

1.4
date	95.10.13.20.44.01;	author arots;	state Exp;
branches;
next	1.3;

1.3
date	95.08.24.14.29.22;	author arots;	state Exp;
branches;
next	1.2;

1.2
date	95.08.15.21.06.11;	author arots;	state Exp;
branches;
next	1.1;

1.1
date	95.07.21.20.12.34;	author arots;	state Exp;
branches;
next	;


desc
@Processes Clock and Orbit Ephemeris files for xff.
@


1.14
log
@added rcs tag so that it lives in the executable
@
text
@/*--------------------------------------------------------------------------
 *
 *   Processes Clock (time delta correlation) files.
 *
 *   Arnold Rots  -  950524
 *
 *--------------------------------------------------------------------------
 */
/*
 *   Includes---------------------------------------------------------------
 */

#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <dirent.h>
#include "xff.h"

#define DUL 1.0e7
#define DUT 864.0
#define RADEG 57.2957795131

static const char processOC_rcsid[] = 
"$Id: processOC.c,v 1.13 2000/08/01 12:18:23 jonv Exp $";

FILE *createCO (FILE *, int, int, char *) ;
double extractCTime (char *, int *) ;
double extractOTime (double *, int *) ;
int closeCFF (FILE *, char *, double, double, int, char *, char *) ;
int closeOFF (FILE *, char *, double, double, int, double *, char *) ;
int toFIO (FILE *, int, char *, int, int) ;
int updateHdrs (int, double, double, int, char *) ;
int modifyHdr (int, char *, char *, char *, char *, double, double) ;
int extract_mission_day_from_raw_clock_filename( const char *fname );

void processClock ()
{
  static char infile[256] ;
  static char outfile[256] ;
  static char infileTBP[256] ;
  static char clockTBPDir[256] ;
  static char timestr[30] ;
  int day ;
  int nrow ;
  double first ;
  double last ;
  FILE *INFF ;
  FILE *TFF ;
  FILE *FF ;
  static char header[513] ;
  RowData row ;
  static char data[520] ;
  int ipos ;
  int j ;
  unsigned long *sums = NULL ;
  char *ascii = NULL ;
  DIR *dirp ;
  struct dirent *direntp ;

  row.data = data ;
  sprintf (clockTBPDir,"%s/clock", getenv ("SOCFITS")) ;

  if ( ( dirp = opendir (clockTBPDir) ) == NULL )
    return ;

  while ( ( direntp = readdir (dirp) ) != NULL ) {
      strcpy (infile, direntp->d_name) ;
      if ( *infile == '.' )
	continue ;

      first = 1.0e12 ;
      last = -100.0 ;
      nrow = 0 ;
      sums = clearChkSum (&ipos, sums) ;
/*
 *      Construct full input file name
 */
      sprintf (infileTBP,"%s/%s", clockTBPDir, infile) ;
/*
 *      Open file and get header
 */
      INFF = fopen (infileTBP, "r") ;
      if ( !INFF ) {
	fprintf (stderr, "---> Could not open clock file %s\n", infileTBP) ;
	continue ;
      }
      fread (header, sizeof(char), 512, INFF) ;
      header[512] = 0 ;
      if ( strncmp(header+12, "usccs_time", 10) ) {
	fprintf (stderr, "---> Clock file %s is not the right type.\n", infileTBP) ;
	fclose (INFF) ;
	continue ;
      }
/*     Use the clock file name to get the mission day for the FITS file. */
/*     If that does not work, then use the midpoint. */

      day = extract_mission_day_from_raw_clock_filename( infile );

      if( day == -1 ) {

        fprintf(stderr, "---> Warning: could not get mission day from "
                "raw clock data file name.  Switching to alternate "
                "method for finding mission day (avg. of header start "
                "and stop times).\n");

/*      Unfortunately, TDC files cannot be relied upon to begin and end  */
/*      in the same day;  taking the midpoint ought to be safe.          */
        strncpy (timestr, header+24, 17) ;
        timestr[17] = 0 ;
        double time = extractCTime (timestr, &day) ;
        strncpy (timestr, header+41, 17) ;
        timestr[17] = 0 ;
        time += extractCTime (timestr, &day) ;
        time *= 0.5 ;
        time /= 86400.0 ;
        day = (int) time ;
      }
/*
 *      Find template and create FITS file
 */
      if ( (TFF = findTFF (-2, 0) ) == NULL ) {
	  fprintf (stderr, "---> Could not find template file for Clock\n") ;
	  fclose (INFF) ;
	  break ;
	}
      if ( (FF = createCO (TFF, 13, day, outfile)) == NULL ) {
	  fprintf (stderr, "---> Could not create Clock file %s\n", outfile) ;
	  fclose (INFF) ;
	  break ;
	}
/*
 *      Fill the file
 */
      while ( ( row.length = fread (data+8, sizeof(char), 512, INFF) ) == 512 ) {
	  strncpy (timestr, data+128, 25) ;
	  timestr[25] = 0 ;
	  row.time = extractCTime (timestr, &day) ;
/*           Take care of the time stamps in bad records  */
	  if ( row.time < 0.0 ) {
	    strncpy (timestr, data+103, 25) ;
	    timestr[25] = 0 ;
	    row.time = extractCTime (timestr, &day) ;
	    if ( row.time < 0.0 ) {
	      strncpy (timestr, data+8, 17) ;
	      timestr[17] = 0 ;
	      row.time = extractCTime (timestr, &day) ;
	    }
	  }
	  memcpy (data, (char *) &row.time, 8) ;
	  row.length += 8 ;
	  if ( !writeRow (FF, &row) ) {
	      fprintf (stderr, "===> Failed to write row %d of %s\n", nrow, outfile) ;
	      fclose (INFF) ;
	      break ;
	    }
	  nrow++ ;
	  accumChkSum (data, row.length, &ipos, sums) ;
	  if ( first > row.time )
	    first = row.time ;
	  last = row.time ;
	}
/*
 *      Checksum; close the file and fix it up
 */
      ascii = encodeDataChkSum (sums, ascii) ;
      j = closeCFF (FF, outfile, first, last, nrow, header, ascii) ;
      fclose (INFF) ;

      if ( unlink (infileTBP) ) {
	  fprintf (stderr, "---> Failed to remove %s\n", infileTBP) ;
	  break ;
	}

      fprintf (LOG, "%s ", wallClock()) ;
      if ( j ) {
	  fprintf (LOG, ">>>> Clock file %s\n", infileTBP) ;
	  fprintf (LOG, "              failed to write to %s\n", outfile) ;
	  fprintf (LOG, "              status: %d\n", j) ;
	}
      else {
	  fprintf (LOG, "<<<< Clock file %s\n", infileTBP) ;
	  fprintf (LOG, "              written to %s\n", outfile) ;
	}

    }

  (void) closedir (dirp) ;

  return ;
}

void processOrbit ()
{
  static char infile[256] ;
  static char outfile[256] ;
  static char infileTBP[256] ;
  static char orbitTBPDir[256] ;
  static char timestr[30] ;
  int day ;
  int nrow ;
  double time ;
  double tbeg ;
  double tend ;
  double first ;
  double last ;
  double deltat ;
  FILE *INFF ;
  FILE *TFF ;
  FILE *FF ;
  static double header[350] ;
  RowData row ;
  static double data[350] ;
  static double rdata[7] ;
  double *pdata ;
  int ipos ;
  int i ;
  int j ;
  unsigned long *sums = NULL ;
  char *ascii = NULL ;
  DIR *dirp ;
  struct dirent *direntp ;

  row.data = (char *) rdata ;
  sprintf (orbitTBPDir,"%s/orbit", getenv ("SOCFITS")) ;

  if ( ( dirp = opendir (orbitTBPDir) ) == NULL )
    return ;

  while ( ( direntp = readdir (dirp) ) != NULL )
    {
      strcpy (infile, direntp->d_name) ;
      if ( *infile == '.' )
	continue ;

      first = 1.0e12 ;
      last = -100.0 ;
      nrow = 0 ;
      sums = clearChkSum (&ipos, sums) ;
/*
 *      Construct full input file name
 */
      sprintf (infileTBP,"%s/%s", orbitTBPDir, infile) ;
/*
 *      Open file and get header
 */
      INFF = fopen (infileTBP, "r") ;
      if ( !INFF ) {
	fprintf (stderr, "---> Could not open orbit file %s\n", infileTBP) ;
	continue ;
      }
      fread (header, sizeof(char), 2800, INFF) ;
      if ( strncmp((char *) header, "EPHEM", 5) ) {
	fprintf (stderr, "---> Orbit file %s is not the right type.\n", infileTBP) ;
	fclose (INFF) ;
	continue ;
      }
      fread (data, sizeof(char), 2800, INFF) ;
      tbeg = extractOTime (header+3, &day) ;
      tend = extractOTime (header+6, &i) ;
      deltat = header[9] ;
/*
 *      Find template and create FITS file
 */
      if ( (TFF = findTFF (-3, 0) ) == NULL ) {
	  fprintf (stderr, "---> Could not find template file for Orbit\n") ;
	  fclose (INFF) ;
	  break ;
	}
      if ( (FF = createCO (TFF, 12, day, outfile)) == NULL ) {
	  fprintf (stderr, "---> Could not create Orbit file %s\n", outfile) ;
	  fclose (INFF) ;
	  break ;
	}
/*
 *      Fill the file
 */
      while ( fread (data, sizeof(double), 350, INFF) == 350 ) {
	  time = extractOTime (data, &i) ;
	  if ( first > time )
	    first = time ;
	  pdata = data + 4 ;
	  row.length = 56 ;
	  for (i=0;i<50;i++) {
	      if ( *pdata > 1000.0 )
		break ;
	      row.time = time + i * deltat ;
	      rdata[0] = row.time ;
	      for (j=1;j<7;j++,pdata++)
		rdata[j] = *pdata * DUL ;
	      for (j=4;j<7;j++)
		rdata[j] /= DUT ;
	      if ( !writeRow (FF, &row) ) {
		  fprintf (stderr, "===> Failed to write row %d of %s\n", nrow, outfile) ;
		  fclose (INFF) ;
		  break ;
		}
	      nrow++ ;
	      accumChkSum ((char *) rdata, row.length, &ipos, sums) ;
	      last = row.time ;
	    }
	}
/*
 *      Checksum; close the file and fix it up
 */
      ascii = encodeDataChkSum (sums, ascii) ;
      j = closeOFF (FF, outfile, first, last, nrow, header, ascii) ;
      fclose (INFF) ;

      if ( unlink (infileTBP) ) {
	  fprintf (stderr, "---> Failed to remove %s\n", infileTBP) ;
	  break ;
	}

      fprintf (LOG, "%s ", wallClock()) ;
      if ( j ) {
	  fprintf (LOG, ">>>> Orbit Ephemeris file %s\n", infileTBP) ;
	  fprintf (LOG, "              failed to write to %s\n", outfile) ;
	  fprintf (LOG, "              status: %d\n", j) ;
	}
      else {
	  fprintf (LOG, "<<<< Orbit Ephemeris file %s\n", infileTBP) ;
	  fprintf (LOG, "              written to %s\n", outfile) ;
	}
    }

  (void) closedir (dirp) ;

  return ;
}

double extractCTime (char *timestr, int *day)
/*--------------------------------------------------------------------------
 *
 *  Extract the time and the day number from a YYYY:DDD:HH:MM:SS string.
 *
 --------------------------------------------------------------------------*/
{
  int tyear ;
  int tday ;
  int thour ;
  int tminute ;
  unsigned long tsec ;
  int msec ;
  int usec ;
  double sec ;
  int i ;

  if ( strlen(timestr) == 25 )
    {
      sscanf (timestr, "%d:%d:%d:%d:%d:%d:%d",
	      &tyear, &tday, &thour, &tminute, &tsec, &msec, &usec) ;
      sec = 0.000001 * (1000*msec + usec) ;
    }
  else
    {
      sscanf (timestr, "%d:%d:%d:%d:%d", &tyear, &tday, &thour, &tminute, &tsec) ;
      sec = 0.0 ;
    }
  tday += 365 * (tyear - 1994) ;
  tday += (tyear - 1993) / 4 ;      /* leap days */
  for (i=0; i<numleaps; i++)        /* leap seconds */
    if ( tday > leapsMD[i] )
      tsec++ ;

//  if ( tday > 181 )                 /* leap second June 30, 1994 */
//    tsec++ ;
//  if ( tday > 730 )                 /* leap second December 31, 1995 */
//    tsec++ ;
//  if ( tday > 1277 )                /* leap second June 30, 1997 */
//    tsec++ ;

  sec += (double) tsec + (tday-1) * 86400 +
                  thour * 3600 + tminute * 60 ;  /* days are 1-relative */ ;
  *day = tday - 1 ;

  return sec ;
}

double extractOTime (double *timearray, int *day)
/*--------------------------------------------------------------------------
 *
 *  Extract the time and three doubles containing YYMMDD., Day of year, Sec of day.
 *
 --------------------------------------------------------------------------*/
{
  int tyear ;
  int tday ;
  int tsec ;
  double sec ;
  int i ;

  tyear = (int) (timearray[0] * 0.0001 + 1900.0) ;
  tday = (int) (timearray[1] + 0.1) ;
  sec = timearray[2] ;
  tday += 365 * (tyear - 1994) ;
  tday += (tyear - 1993) / 4 ;      /* leap days */
  tsec = (tday-1) * 86400 ;         /* days are 1-relative */
  for (i=0; i<numleaps; i++)        /* leap seconds */
    if ( tday > leapsMD[i] )
      tsec++ ;

//  if ( tday > 181 )                 /* leap second June 30, 1994 */
//    tsec++ ;
//  if ( tday > 730 )                 /* leap second December 31, 1995 */
//    tsec++ ;

  sec += tsec ;
  *day = tday - 1 ;

  return sec ;
}

FILE *createCO (FILE *TFF, int subsys, int day, char *filename)
/*----------------------------------------------------------------------
 *  Create/open a new FITS file and copy the contents of the template
 *  FITS file (header) to it.
 *
 *  Input:  *TFF           open FILE pointer to the template FITS file
 *          subsys         subsystem number (12=orbit, 13=clock)
 *          day            start day number of the file
 *
 *  Return: FILE           open FILE pointer to the new FITS file
 *          *filename      expanded name for the new FITS file
 *
 ----------------------------------------------------------------------*/
{
  char line[1024] ;
  FILE *FF = NULL ;
  int i, h, nkw ;
  char fullname[256] ;
  char *subdir ;

  h = 2 ;
  if ( subsys == 12 )
    subdir = "OrbitEphem" ;
  else if ( subsys == 13 )
    subdir = "Clock" ;
  else
    return FF ;

/*
 *     Open the file and rewind the template
 */
  sprintf (filename, "FP%s_Day%04d", pidir[subsys], day);
  sprintf (fullname, "%s%s/%s",
	   datadir, subdir, filename) ;
  if ( (FF = fopen (fullname, "w")) == NULL )
    return FF ;
  strcpy (filename, fullname) ;

/*
 *     The big loop on HDUs (no more than 2)
 */
  while ( !feof (TFF) && h )
  {
    h--;
    nkw = 36;
    strcpy (line, "start");
/*
 *       The line read loop
 */
    while ( strncmp (line, "END", 3) && !feof (TFF) )
    {
/*
 *         Read, pad, and write the line
 */
      if ( fscanf (TFF, "%[^\n]%*[\n]", line) )
      {
        for (i=strlen(line);i<80;i++)
          line[i] = ' ';
        fwrite (line, sizeof(char), 80, FF);
/*
 *         Count the lines
 */
        if (nkw == 36) nkw = 0;
        nkw++;
      }
/*
 *       END encountered
 */
    }
/*
 *       Pad the header
 */
    for (i=0;i<80;i++)
      line[i] = ' ';
    for (i=nkw;i<36;i++)
      fwrite (line, sizeof(char), 80, FF);
  }
/*
 *     Done
 */
  fclose (TFF);
  return FF;
}

int closeCFF (FILE *FF, char *filename, double tstart, double tstop, int nrow,
	      char *header, char *ascii)
/*----------------------------------------------------------------------
 *  Close a Clock FITS file.
 *
 *  Input:  *FF            open FILE pointer to the FITS file
 *          *filename      full name of FITS file
 *          tstart         time stamp of first row
 *          tstop          time stamp of last row
 *          nrow           number of rows in table
 *          *header        512 byte header
 *          *ascii         ASCII-encoded data checksum
 *
 *  Return: Succes?
 *
 ----------------------------------------------------------------------*/
{
  int status = 0 ;
  int endstatus ;
  int iunit = 15 ;
  char str[30] ;
  char datobs[9], timobs[9], datend[9], timend[9];

  endstatus = toFIO (FF, iunit, filename, nrow, 520) ;
  prtfiostat (endstatus, "closeCFF toFIO") ;

  if ( !endstatus )
    {
      endstatus = updateHdrs (iunit, tstart, tstop, nrow, ascii) ;
      prtfiostat (endstatus, "closeCFF updateHdrs") ;
    }

/*
 *      Fill in header record
 */
  if ( !endstatus )
    {
      strncpy (str, header, 24) ;
      str[24] = 0 ;
      fcmkys (iunit, "DVAL1", str, "&", &status) ;
      prtfiostat (status, "closeCFF DVAL1") ;
      if ( !endstatus )
	endstatus = status ;
      status = 0;

      strncpy (str, header+24, 17) ;
      str[17] = 0 ;
      fcmkys (iunit, "DVAL2", str, "&", &status) ;
      prtfiostat (status, "closeCFF DVAL2") ;
      if ( !endstatus )
	endstatus = status ;
      status = 0;

      strncpy (str, header+41, 17) ;
      str[17] = 0 ;
      fcmkys (iunit, "DVAL3", str, "&", &status) ;
      prtfiostat (status, "closeCFF DVAL3") ;
      if ( !endstatus )
	endstatus = status ;
      status = 0;

      strncpy (str, header+58, 8) ;
      str[8] = 0 ;
      fcmkys (iunit, "DVAL4", str, "&", &status) ;
      prtfiostat (status, "closeCFF DVAL4") ;
      if ( !endstatus )
	endstatus = status ;
      status = 0;

      strncpy (str, header+66, 3) ;
      str[3] = 0 ;
      fcmkys (iunit, "DVAL5", str, "&", &status) ;
      prtfiostat (status, "closeCFF DVAL5") ;
      if ( !endstatus )
	endstatus = status ;
      status = 0;

      strncpy (str, header+69, 4) ;
      str[4] = 0 ;
      fcmkys (iunit, "DVAL6", str, "&", &status) ;
      prtfiostat (status, "closeCFF DVAL6") ;
      if ( !endstatus )
	endstatus = status ;
      status = 0;


      fcucks (iunit, &status) ;
      prtfiostat (status, "closeCFF fcucks") ;
      if ( !endstatus )
	endstatus = status ;
      status = 0;

    }

  fcclos (iunit, &status);
  if ( !endstatus )
    endstatus = status ;

  return endstatus ;
}

int closeOFF (FILE *FF, char *filename, double tstart, double tstop, int nrow,
	      double *header, char *ascii)
/*----------------------------------------------------------------------
 *  Close an Orbit FITS file.
 *
 *  Input:  *FF            open FILE pointer to the FITS file
 *          *filename      full name of FITS file
 *          tstart         time stamp of first row
 *          tstop          time stamp of last row
 *          nrow           number of rows in table
 *          *header        350 double header
 *          *ascii         ASCII-encoded data checksum
 *
 *  Return: Succes?
 *
 ----------------------------------------------------------------------*/
{
  int status = 0 ;
  int endstatus ;
  int iunit = 15 ;
  int tyear ;
  int tmonth ;
  int tday ;
  int tsec ;
  static const int dms[12] = {31,28,31,30,31,30,31,31,30,31,30,31} ;
  int i ;
  double value ;
  char datobs[9], timobs[9], datend[9], timend[9];

  endstatus = toFIO (FF, iunit, filename, nrow, 56) ;
  prtfiostat (endstatus, "closeOFF toFIO") ;

  if ( !endstatus )
    {
      endstatus = updateHdrs (iunit, tstart, tstop, nrow, ascii) ;
      prtfiostat (endstatus, "closeOFF updateHdrs") ;
    }

/*
 *      Fill in header record
 */
  if ( !endstatus )
    {
      tyear = (int) (header[43] + 1900.1) ;
      tmonth = (int) (header[44] - 0.9) ;
      tday = (int) (header[45] + 0.1) ;
      if ( !(tyear % 4) && ( tmonth > 1 ) )
	tday++ ;
      for (i=0;i<tmonth;i++)
	tday =+ dms[i] ;
      value = header[46] * 3600.0 + header[47] * 60.0 + header[48] * 0.001 ;
      tday += 365 * (tyear - 1994) ;
      tday += (tyear - 1993) / 4 ;      /* leap days */
      tsec = (tday-1) * 86400 ;         /* days are 1-relative */

      for (i=0; i<numleaps; i++)        /* leap seconds */
	if ( tday > leapsMD[i] )
	  tsec++ ;

//      if ( tday > 181 )                 /* leap second June 30, 1994 */
//	tsec++ ;
//      if ( tday > 730 )                 /* leap second December 31, 1995 */
//	tsec++ ;

      value += tsec ;

      fcmkyd (iunit, "DVAL1", value, 15, "&", &status) ;
      prtfiostat (status, "closeOFF DVAL1") ;
      if ( !endstatus )
	endstatus = status ;
      status = 0;

      value = header[49] * 1000.0 ;
      fcmkyd (iunit, "DVAL2", value, 12, "&", &status) ;
      prtfiostat (status, "closeOFF DVAL2") ;
      if ( !endstatus )
	endstatus = status ;
      status = 0;

      value = header[50] ;
      fcmkyd (iunit, "DVAL3", value, 12, "&", &status) ;
      prtfiostat (status, "closeOFF DVAL3") ;
      if ( !endstatus )
	endstatus = status ;
      status = 0;

      value = header[51] * RADEG ;
      fcmkyd (iunit, "DVAL4", value, 12, "&", &status) ;
      prtfiostat (status, "closeOFF DVAL4") ;
      if ( !endstatus )
	endstatus = status ;
      status = 0;

      value = header[52] * RADEG ;
      fcmkyd (iunit, "DVAL5", value, 12, "&", &status) ;
      prtfiostat (status, "closeOFF DVAL5") ;
      if ( !endstatus )
	endstatus = status ;
      status = 0;

      value = header[53] * RADEG ;
      fcmkyd (iunit, "DVAL6", value, 12, "&", &status) ;
      prtfiostat (status, "closeOFF DVAL6") ;
      if ( !endstatus )
	endstatus = status ;
      status = 0;

      value = header[54] * RADEG ;
      fcmkyd (iunit, "DVAL7", value, 12, "&", &status) ;
      prtfiostat (status, "closeOFF DVAL7") ;
      if ( !endstatus )
	endstatus = status ;
      status = 0;

      value = header[55] * RADEG ;
      fcmkyd (iunit, "DVAL8", value, 12, "&", &status) ;
      prtfiostat (status, "closeOFF DVAL8") ;
      if ( !endstatus )
	endstatus = status ;
      status = 0;

      value = header[56] * RADEG ;
      fcmkyd (iunit, "DVAL9", value, 12, "&", &status) ;
      prtfiostat (status, "closeOFF DVAL9") ;
      if ( !endstatus )
	endstatus = status ;
      status = 0;

      value = header[57] * RADEG ;
      fcmkyd (iunit, "DVAL10", value, 12, "&", &status) ;
      prtfiostat (status, "closeOFF DVAL10") ;
      if ( !endstatus )
	endstatus = status ;
      status = 0;

      value = header[58] * RADEG ;
      fcmkyd (iunit, "DVAL11", value, 12, "&", &status) ;
      prtfiostat (status, "closeOFF DVAL11") ;
      if ( !endstatus )
	endstatus = status ;
      status = 0;

      value = header[59] * DUT ;
      fcmkyd (iunit, "DVAL12", value, 12, "&", &status) ;
      prtfiostat (status, "closeOFF DVAL12") ;
      if ( !endstatus )
	endstatus = status ;
      status = 0;

      value = header[60] * 1000.0 ;
      fcmkyd (iunit, "DVAL13", value, 12, "&", &status) ;
      prtfiostat (status, "closeOFF DVAL13") ;
      if ( !endstatus )
	endstatus = status ;
      status = 0;

      value = header[61] * 1000.0 ;
      fcmkyd (iunit, "DVAL14", value, 12, "&", &status) ;
      prtfiostat (status, "closeOFF DVAL14") ;
      if ( !endstatus )
	endstatus = status ;
      status = 0;

      value = header[62] * RADEG / DUT ;
      fcmkyd (iunit, "DVAL15", value, 12, "&", &status) ;
      prtfiostat (status, "closeOFF DVAL15") ;
      if ( !endstatus )
	endstatus = status ;
      status = 0;

      value = header[63] * RADEG / DUT ;
      fcmkyd (iunit, "DVAL16", value, 12, "&", &status) ;
      prtfiostat (status, "closeOFF DVAL16") ;
      if ( !endstatus )
	endstatus = status ;
      status = 0;

      value = header[64] * RADEG / DUT ;
      fcmkyd (iunit, "DVAL17", value, 12, "&", &status) ;
      prtfiostat (status, "closeOFF DVAL17") ;
      if ( !endstatus )
	endstatus = status ;
      status = 0;

      value = header[128] * DUL ;
      fcmkyd (iunit, "DVAL18", value, 12, "&", &status) ;
      prtfiostat (status, "closeOFF DVAL18") ;
      if ( !endstatus )
	endstatus = status ;
      status = 0;

      value = header[129] * DUL ;
      fcmkyd (iunit, "DVAL19", value, 12, "&", &status) ;
      prtfiostat (status, "closeOFF DVAL19") ;
      if ( !endstatus )
	endstatus = status ;
      status = 0;

      value = header[130] * DUL ;
      fcmkyd (iunit, "DVAL20", value, 12, "&", &status) ;
      prtfiostat (status, "closeOFF DVAL20") ;
      if ( !endstatus )
	endstatus = status ;
      status = 0;



      fcucks (iunit, &status) ;
      prtfiostat (status, "closeOFF fcucks") ;
      if ( !endstatus )
	endstatus = status ;
      status = 0;

    }

  fcclos (iunit, &status);
  if ( !endstatus )
    endstatus = status ;

  return endstatus ;
}

int toFIO (FILE *FF, int iunit, char *filename, int nrows, int nbytes)
/*----------------------------------------------------------------------
 *  Pad and close a FITS file; open again with FITSIO
 *
 *  Input:  *FF            open FILE pointer to the FITS file
 *          iunit          FITSIO unit number
 *          *filename      full name of FITS file
 *          nrows          number of rows in table
 *          nbytes         number of bytes per row in table
 *
 *  Return: status         Succes?
 *
 ----------------------------------------------------------------------*/
{
  int status = 0 ;
  char pad[2880];
  int k;
  int blk = 0 ;

/*
 *     Figure out how much padding is required
 */
  k = (int) ((nrows * nbytes - 1) / 2880 + 1) * 2880 - nrows * nbytes;
/*
 *     Pad, if required
 */
  if ( k )
    {
      memset (pad, 0, k);
      fwrite (pad, sizeof(char), k, FF);
    }
  fclose (FF);
/*
 *       Open the file again
 */
  fcopen (iunit, filename, 1, &blk, &status);
  prtfiostat (status, "toFIO") ;

  return status ;
}

int updateHdrs (int iunit, double tstart, double tstop, int nrow, char *ascii)
/*----------------------------------------------------------------------
 *  Update the headers of a Clock or orbit FITS file.
 *
 *  Input:  iunit          FITSIO unit number
 *          tstart         time stamp of first row
 *          tstop          time stamp of last row
 *          nrow           number of rows in table
 *          *ascii         ASCII-encoded data checksum
 *
 *  Return: status
 *
 ----------------------------------------------------------------------*/
{
  int status = 0 ;
  int hdutype ;
  int endstatus ;
  char datobs[24], timobs[9], datend[24], timend[9];

  convertTime (tstart, datobs, timobs);
  convertTime (tstop, datend, timend);

  status = modifyHdr (iunit, datobs, timobs, datend, timend, tstart, tstop) ;
  prtfiostat (status, "updateHdrs modifyHdr HDU0") ;

  fcpcks (iunit, &status) ;
  prtfiostat (status, "updateHdrs fcpcks HDU0") ;
  endstatus = status ;
  status = 0 ;
  
/*
 *       Go to next HDU; should be bintable (2)
 */
  fcmrhd (iunit, 1, &hdutype, &status) ;
  prtfiostat (status, "updateHdrs fcmrhd") ;

  status = modifyHdr (iunit, datobs, timobs, datend, timend, tstart, tstop) ;
  prtfiostat (status, "updateHdrs modifyHdr HDU1") ;

  fcmkyj (iunit, "NAXIS2", nrow, "Number of rows", &status);
  prtfiostat (status, "updateHdrs NAXIS2") ;

  fcpkys (iunit, "DATASUM", ascii, "Data checksum, ASCII encoded", &status) ;
  prtfiostat (status, "updateHdrs DATASUM") ;

  if ( !endstatus )
    endstatus = status ;

  return endstatus ;
}

int modifyHdr (int iunit, char *datobs, char *timobs, char *datend, char *timend,
	       double tstart, double tstop)
/*----------------------------------------------------------------------
 *  Modify a header of a Clock or orbit FITS file.
 *
 *  Input:  iunit          FITSIO unit number
 *          *datobs        start date
 *          *timobs        start time
 *          *datend        stop date
 *          *timend        stop time
 *          tstart         time stamp of first row
 *          tstop          time stamp of last row
 *
 *  Return: status
 *
 ----------------------------------------------------------------------*/
{
  int status = 0 ;
  int endstatus ;
  int yr, mon, day ;
  char date[24], comm[100] ;

  fcmkys (iunit, "CREATOR", XFFVER, " ", &status) ;
  prtfiostat (status, "modifyHdr CREATOR") ;
  endstatus = status ;
  status = 0;
  
  fcpdat (iunit, &status);

  fcgkys (iunit, "DATE", date, comm, &status);
  sscanf (date, "%d/%d/%d", &day, &mon, &yr) ;
  if ( yr < 90 )
    yr += 100 ;
  if ( yr > 98 ) {
    sprintf (date, "%04d-%02d-%02d", yr+1900, mon, day) ;
    fcmkys (iunit, "DATE", date, "&", &status);
  }

  prtfiostat (status, "modifyHdr DATE") ;
  if ( !endstatus )
    endstatus = status ;
  status = 0;

  fcmkys (iunit, "DATE-OBS", datobs, "Start date for data", &status);
  prtfiostat (status, "modifyHdr DATE-OBS") ;
  if ( !endstatus )
    endstatus = status ;
  status = 0;

  fcmkys (iunit, "TIME-OBS", timobs, "Start time for data", &status);
  prtfiostat (status, "modifyHdr TIME-OBS") ;
  if ( !endstatus )
    endstatus = status ;
  status = 0;

  fcmkys (iunit, "DATE-END", datend, "End date for data", &status);
  prtfiostat (status, "modifyHdr DATE-END") ;
  if ( !endstatus )
    endstatus = status ;
  status = 0;

  fcmkys (iunit, "TIME-END", timend, "End time for data", &status);
  prtfiostat (status, "modifyHdr TIME-END") ;
  if ( !endstatus )
    endstatus = status ;
  status = 0;

  fcmkyd (iunit, "TSTART", tstart, 15, "&", &status);
  prtfiostat (status, "modifyHdr TSTART") ;
  if ( !endstatus )
    endstatus = status ;
  status = 0;

  fcmkyd (iunit, "TSTOP", tstop, 15, "&", &status);
  prtfiostat (status, "modifyHdr TSTOP") ;
  if ( !endstatus )
    endstatus = status ;

  return endstatus ;
}


int extract_mission_day_from_raw_clock_filename( const char *fname )
{


  char utc_date[18];
  char year_str[5], day_of_year_str[4];

  // First extract the digits for the year and day of year from the 
  // raw clock data file name, which looks like this:
  //    X1999219_00_usccs_timecorr.dat

  strncpy (year_str, fname+1, 4);      year_str[4] = '\0';
  strncpy(day_of_year_str,fname+5,3);  day_of_year_str[3] = '\0';
  // Don't forget to add the trailing NULL, which strncpy does not.

  // Some error checking:
  // Verify that the characters extracted are indeed numbers and that 
  // the numbers are in the correct range.
  int year, day_of_year;
  int read_yr_ok    = sscanf(year_str, "%d", &year);
  int read_yrday_ok = sscanf(day_of_year_str, "%d", &day_of_year);

  int mission_day = -1;

  if( (read_yr_ok    == 1) && (year > 1994)     && (year < 2037) &&
      (read_yrday_ok == 1) && (day_of_year > 0) && (day_of_year < 367) )
  {

    // If everything is OK, then create a string UTC date, and use that
    // to make an XTETime.  Start 30 minutes into the day just to make 
    // sure there is no question about which day is being asked for.
    // Then convert to mission day by dividing the MET's by the total
    // number of seconds per day.

    sprintf(utc_date,"%s:%s:00:30:00", year_str, day_of_year_str);

    XTETime xt(utc_date, XTETime::UTC, XTETime::DATE);

    mission_day = (int) (xt.getMET() / 86400);
  } else {
    fprintf(stderr, "---> Warning: unable to get year and day of year "
                    "from clock file name: \"%s\"\n", fname);
  }

  return mission_day;
}
@


1.13
log
@changed the default way clock FITS files are named -
  now the date stamp in the filename is used rather than
  the data in the clock file header, which can be wrong
  or goffed up;
@
text
@a0 1
/* RCS: $Id: processOC.c,v 1.12 1998/08/02 23:32:01 arots Exp $ */
d25 3
@


1.12
log
@One inconsequential part of the code still did not use the
leap second file.
@
text
@d1 1
a1 1
/* RCS: $Id: processOC.c,v 1.11 1997/12/12 15:49:23 soccm Exp $ */
d35 1
a45 1
  double time ;
d95 12
d109 10
a118 9
      strncpy (timestr, header+24, 17) ;
      timestr[17] = 0 ;
      time = extractCTime (timestr, &day) ;
      strncpy (timestr, header+41, 17) ;
      timestr[17] = 0 ;
      time += extractCTime (timestr, &day) ;
      time *= 0.5 ;
      time /= 86400.0 ;
      day = (int) time ;
d992 48
@


1.11
log
@Correct type.
@
text
@d1 1
a1 1
/* RCS: $Id: processOC.c,v 1.10 1997/12/12 15:24:12 soccm Exp $ */
d640 10
a649 4
      if ( tday > 181 )                 /* leap second June 30, 1994 */
	tsec++ ;
      if ( tday > 730 )                 /* leap second December 31, 1995 */
	tsec++ ;
@


1.10
log
@Fixed leap second handling in orbit time calculation.
@
text
@d1 1
a1 1
/* RCS: $Id: processOC.c,v 1.9 1997/10/27 20:15:36 arots Exp $ */
d378 1
@


1.9
log
@Allow for leap seconds from file tai-utc.dat.
Allow for new DATEXXXX format.
@
text
@d1 1
a1 1
/* RCS: $Id: processOC.c,v 1.8 1997/03/24 17:22:49 soccm Exp $ */
d349 1
a349 1
  for (i=0; i<numleaps; i++)
d385 8
a392 4
  if ( tday > 181 )                 /* leap second June 30, 1994 */
    tsec++ ;
  if ( tday > 730 )                 /* leap second December 31, 1995 */
    tsec++ ;
@


1.8
log
@Allowed for leap second 1997.5.
@
text
@d1 1
a1 1
/* RCS: $Id: processOC.c,v 1.7 1996/09/26 21:46:31 arots Exp $ */
d334 1
d349 10
a358 6
  if ( tday > 181 )                 /* leap second June 30, 1994 */
    tsec++ ;
  if ( tday > 730 )                 /* leap second December 31, 1995 */
    tsec++ ;
  if ( tday > 1277 )                /* leap second June 30, 1997 */
    tsec++ ;
d855 1
a855 1
  char datobs[9], timobs[9], datend[9], timend[9];
d908 2
d917 10
@


1.7
log
@Better accounting of good time.
@
text
@d1 1
a1 1
/* RCS: $Id: processOC.c,v 1.5 1995/11/13 15:53:43 arots Exp arots $ */
d351 2
@


1.6
log
@Fixed paths.
@
text
@d95 2
d100 6
d126 11
@


1.5
log
@Decrement mission day number by 1.
@
text
@d1 1
a1 1
/* RCS: $Id: processOC.c,v 1.1 1995/11/03 20:12:37 soccm Exp soccm $ */
d67 1
a67 2
  while ( ( direntp = readdir (dirp) ) != NULL )
    {
d84 4
d90 5
d101 1
a101 2
      if ( (TFF = findTFF (-2, 0) ) == NULL )
	{
d103 1
d106 1
a106 2
      if ( (FF = createCO (TFF, 13, day, outfile)) == NULL )
	{
d108 1
d114 1
a114 2
      while ( ( row.length = fread (data+8, sizeof(char), 512, INFF) ) == 512 )
	{
d120 1
a120 2
	  if ( !writeRow (FF, &row) )
	    {
d122 1
d136 1
d138 1
a138 2
      if ( unlink (infileTBP) )
	{
d144 1
a144 2
      if ( j )
	{
d149 1
a149 2
      else
	{
d216 4
d221 5
d233 1
a233 2
      if ( (TFF = findTFF (-3, 0) ) == NULL )
	{
d235 1
d238 1
a238 2
      if ( (FF = createCO (TFF, 12, day, outfile)) == NULL )
	{
d240 1
d246 1
a246 2
      while ( fread (data, sizeof(double), 350, INFF) == 350 )
	{
d252 1
a252 2
	  for (i=0;i<50;i++)
	    {
d261 1
a261 2
	      if ( !writeRow (FF, &row) )
		{
d263 1
d276 1
d278 1
a278 2
      if ( unlink (infileTBP) )
	{
d284 1
a284 2
      if ( j )
	{
d289 1
a289 2
      else
	{
@


1.4
log
@Fix some bugs.
@
text
@d1 1
a1 1
/* RCS: $Id: processOC.c,v 1.2 1995/08/15 21:06:11 arots Exp $ */
d326 1
a326 1
  *day = tday ;
d355 1
a355 1
  *day = tday ;
@


1.3
log
@Fixed checksum calculation for orbit files.
@
text
@d321 2
d351 2
d600 2
@


1.2
log
@First complete delivery;
Last delivery before the freeze.
@
text
@d1 1
a1 1
/* RCS: $Id: processOC.c,v 1.1 1995/07/21 20:12:34 arots Exp arots $ */
d255 1
a255 1
	      accumChkSum ((char *) data, row.length, &ipos, sums) ;
d570 1
a570 1
  endstatus = toFIO (FF, iunit, filename, nrow, 520) ;
@


1.1
log
@Initial revision
@
text
@d1 1
a1 1
/* RCS: $Id$ */
d41 1
a41 1
  static char infileArch[256] ;
d62 4
a65 1
  dirp = opendir (clockTBPDir) ;
a80 1
      sprintf (infileArch,"%s/%s", clockArchDir, infile) ;
d130 1
a130 1
      if ( rename (infileTBP, infileArch) )
d132 1
a132 1
	  fprintf (stderr, "---> Failed to move %s to %s\n", infileTBP, infileArch) ;
d161 1
a161 1
  static char infileArch[256] ;
d188 4
a191 1
  dirp = opendir (orbitTBPDir) ;
a206 1
      sprintf (infileArch,"%s/%s", orbitArchDir, infile) ;
d265 1
a265 1
      if ( rename (infileTBP, infileArch) )
d267 1
a267 1
	  fprintf (stderr, "---> Failed to move %s to %s\n", infileTBP, infileArch) ;
d310 1
a310 1
      sec = 0.000001 * (100*msec + usec) ;
a318 1
  tsec += (tday-1) * 86400 + thour * 3600 + tminute * 60 ;  /* days are 1-relative */
d322 2
a323 1
  sec += tsec ;
d479 1
a479 1
      fcpkys (iunit, "DVAL1", str, "&", &status) ;
d487 1
a487 1
      fcpkys (iunit, "DVAL2", str, "&", &status) ;
d495 1
a495 1
      fcpkys (iunit, "DVAL3", str, "&", &status) ;
d503 1
a503 1
      fcpkys (iunit, "DVAL4", str, "&", &status) ;
d511 1
a511 1
      fcpkys (iunit, "DVAL5", str, "&", &status) ;
d519 1
a519 1
      fcpkys (iunit, "DVAL6", str, "&", &status) ;
d599 1
a599 1
      fcpkyd (iunit, "DVAL1", value, 15, "&", &status) ;
d606 1
a606 1
      fcpkyd (iunit, "DVAL2", value, 12, "&", &status) ;
d613 1
a613 1
      fcpkyd (iunit, "DVAL3", value, 12, "&", &status) ;
d620 1
a620 1
      fcpkyd (iunit, "DVAL4", value, 12, "&", &status) ;
d627 1
a627 1
      fcpkyd (iunit, "DVAL5", value, 12, "&", &status) ;
d634 1
a634 1
      fcpkyd (iunit, "DVAL6", value, 12, "&", &status) ;
d641 1
a641 1
      fcpkyd (iunit, "DVAL7", value, 12, "&", &status) ;
d648 1
a648 1
      fcpkyd (iunit, "DVAL8", value, 12, "&", &status) ;
d655 1
a655 1
      fcpkyd (iunit, "DVAL9", value, 12, "&", &status) ;
d662 1
a662 1
      fcpkyd (iunit, "DVAL10", value, 12, "&", &status) ;
d669 1
a669 1
      fcpkyd (iunit, "DVAL11", value, 12, "&", &status) ;
d676 1
a676 1
      fcpkyd (iunit, "DVAL12", value, 12, "&", &status) ;
d683 1
a683 1
      fcpkyd (iunit, "DVAL13", value, 12, "&", &status) ;
d690 1
a690 1
      fcpkyd (iunit, "DVAL14", value, 12, "&", &status) ;
d697 1
a697 1
      fcpkyd (iunit, "DVAL15", value, 12, "&", &status) ;
d704 1
a704 1
      fcpkyd (iunit, "DVAL16", value, 12, "&", &status) ;
d711 1
a711 1
      fcpkyd (iunit, "DVAL17", value, 12, "&", &status) ;
d718 1
a718 1
      fcpkyd (iunit, "DVAL18", value, 12, "&", &status) ;
d725 1
a725 1
      fcpkyd (iunit, "DVAL19", value, 12, "&", &status) ;
d732 1
a732 1
      fcpkyd (iunit, "DVAL20", value, 12, "&", &status) ;
@

head	1.4;
access
	soccm
	arots
	jonv
	gromeo;
symbols
	Build6_0:1.4
	Build5_4:1.4
	Build5_3:1.3
	Build5_2:1.3
	Build5_0_2:1.1;
locks;
comment	@ * @;


1.4
date	97.03.24.16.12.21;	author soccm;	state Exp;
branches;
next	1.3;

1.3
date	96.09.26.21.46.31;	author arots;	state Exp;
branches;
next	1.2;

1.2
date	96.04.08.12.38.17;	author arots;	state Exp;
branches;
next	1.1;

1.1
date	96.01.29.03.47.21;	author arots;	state Exp;
branches;
next	;


desc
@New ObsCat format.
@


1.4
log
@Now reads new human readable format
@
text
@%{
// =======================================================================
// File Name   : lexer.l
// Subsystem   : XFF/MM
// Programmer  : Randall D. Barnette, Hughes STX
// Description : LEX source to describe items in the Mission Planning
//               human-readable DAP.
//
// RCS: $Id: lexer.l,v 1.3 1996/09/26 21:46:31 arots Exp $
//
// =======================================================================
#include <string.h>

void yyline(void) ;
void yystring(void) ;
extern int input() ;

inline void yyldup(const char* s) { yylval.sval = strdup(s); }
inline void yyfdup(const char* s, char c)
{
  char* p ;
	yylval.sval = (p=strchr(s,c)) ? (p++,strdup(p)) : 0 ;
}

%}

W       [ \t]
D       [0-9]+
YEAR    [12][0-9][0-9][0-9]:
DAY     [0123][0-9][0-9]:
TIME    [012][0-9]:[0-5][0-9]:[0-5][0-9]([.]{D})?
HEX     0[xX][0-9a-fA-F]+
REAL    [+-]?({D}[.]{D}|[.]{D})([Ee][+-]?{D})?
STRING  [A-Za-z][A-Za-z0-9_]*
MSTRING [A-Za-z0-9_]+
OSTRING [0-9][0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]-[0-9][0-9]([0-9A-Za-z])?
MACFILE {MSTRING}[.]mac
DCFFILE {STRING}[.]dcf
COMMENT ^#[^\n]*

%%
to                       { /* No value */               return TO ;          }
OBSERVATION              { /* No value */               return OBSERVATION ; }
GOODTIME:                { /* No value */               return GOODTIMES ;   }
EVENTS:                  { /* No value */               return EVENTS ;      }
TARGET:                  { /* No value */               return TARGET ;      }
START                    { /* No value */               return START ;       }
END                      { /* No value */               return END ;         }
RA=                      { /* No value */               return RA ;          }
DEC=                     { /* No value */               return DEC ;         }
ROLL_BIAS=               { /* No value */               return ROLL_BIAS ;   }
SLEW_RATE=               { /* No value */               return SLEW_RATE ;   }
EDS:                     { /* No value */               return EDS ;         }
HEXTE:                   { /* No value */               return HEXTE ;       }
MACROS:                  { /* No value */               return MACRO ;       }
DCFS:                    { /* No value */               return DCFS ;        }
RASTERS:                 { /* No value */               return RASTER ;      }
TOTAL_GT:                { /* No value */               return TOTALGT ;     }
TDRSS:                   { /* No value */               return TDRSS ;       }
FORWARD                  { /* No value */               return FORWARD ;     }
ALT({W}|_)TRGT:          { /* No value */               return ALTERNATE ;   }
"hexte/"{DCFFILE} |
"pseudo/"{DCFFILE}       { yyldup(yytext) ;             return DCF_FILE ;    }
PI:{W}                   { yyline() ;                   return PROPOSER ;    }
SOURCE:{W}               { yyline() ;                   return SOURCE ;      }
LLD[AB]?=({D}|DEF)       { yyfdup(yytext,'=') ;         return LLD ;         }
ROCK[AB]?=({REAL}|0|DEF) { yyfdup(yytext,'=') ;         return ROCK ;        }
DWELL[AB]?=({D}|DEF)     { yyfdup(yytext,'=') ;         return DWELL ;       }
start_slew               { yylval.etyp = SLEW_START ;   return EVENT ;       }
end_slew                 { yylval.etyp = SLEW_END   ;   return EVENT ;       }
in_saa                   { yylval.etyp = IN_SAA     ;   return EVENT ;       }
out_saa                  { yylval.etyp = OUT_SAA    ;   return EVENT ;       }
in_occult                { yylval.etyp = IN_OCCULT  ;   return EVENT ;       }
out_occult               { yylval.etyp = OUT_OCCULT ;   return EVENT ;       }
off_source               { yylval.etyp = OFF_SOURCE ;   return EVENT ;       }
on_source                { yylval.etyp = ON_SOURCE  ;   return EVENT ;       }
ksec                     { yylval.unit = ObsUnit_KSEC;  return UNIT ;        }
"(" |
")" |
","                      { /* No value */               return (int)*yytext; }
{MACFILE}                { yyldup(yytext) ;             return MACROFILE ;   }
{YEAR}{DAY}{TIME}        { /* No value */               return UT ;          }
{TIME}                   { yyldup(yytext) ;             return TIME ;        }
{HEX}                    { yyldup(yytext) ;             return HEXSTRING ;   }
{STRING}                 { yyldup(yytext) ;             return NAME ;        }
{OSTRING}                { yyldup(yytext) ;             return OBSID ;       }
{D}                      { yylval.ival = atoi(yytext) ; return INTEGER ;     }
{REAL}                   { yylval.fval = atof(yytext) ; return REAL ;        }
\\\n |
{W}+                     { /* do nothing */ ;                                }
^\n |
"\n"                     { linecount++ ;                                     } 
\"                       { /* do nothing */ ;                                }
{COMMENT}                { /* printf( "%s\n", yytext ) */ ;                  } 

%%

static char buf[1024] ;

void yystring(void)
{
  char* p = buf;
  for( int c=0; (c=getc(yyin)),!isspace(c); *p++=c ) ;
  *p = 0 ;
  yylval.sval = strdup(buf) ;
}

void yyline(void)
{
  char* p = buf ;
  for( int c=0; (c=getc(yyin)) != '\n'; *p++=c ) ;
  *p = 0 ;
  linecount++ ;
  yylval.sval = strdup(buf) ;
}

@


1.3
log
@Better accounting of good time.
@
text
@d9 1
a9 1
// RCS: $Id: mmlex.l,v 1.3 1995/10/13 20:44:01 arots Exp arots $
d19 5
d42 36
a77 32
to                      { /* No value */               return TO ;          }
OBSERVATION             { /* No value */               return OBSERVATION ; }
GOODTIME:               { /* No value */               return GOODTIMES ;   }
EVENTS:                 { /* No value */               return EVENTS ;      }
TARGET:                 { /* No value */               return TARGET ;      }
START                   { /* No value */               return START ;       }
END                     { /* No value */               return END ;         }
RA=                     { /* No value */               return RA ;          }
DEC=                    { /* No value */               return DEC ;         }
ROLL_BIAS=              { /* No value */               return ROLL_BIAS ;   }
SLEW_RATE=              { /* No value */               return SLEW_RATE ;   }
EDS:                    { /* No value */               return EDS ;         }
HEXTE:                  { /* No value */               return HEXTE ;       }
MACROS:                 { /* No value */               return MACRO ;       }
DCFS:                   { /* No value */               return DCFS ;        }
RASTERS:                { /* No value */               return RASTER ;      }
ALT{W}TRGT:             { /* No value */               return ALTERNATE ;   }
"hexte/"{DCFFILE}  |
"pseudo/"{DCFFILE}      { yyldup(yytext) ;             return DCF_FILE ;    }
PI:{W}                  { yyline() ;                  return PROPOSER ;    }
SOURCE:{W}              { yyline() ;                  return SOURCE ;      }
LLD[AB]=({D}|DEF)       { yyldup(&yytext[5]) ;         return LLD ;         }
ROCK[AB]=({REAL}|0|DEF) { yyldup(&yytext[6]) ;         return ROCK ;        }
DWELL[AB]=({D}|DEF)     { yyldup(&yytext[7]) ;         return DWELL ;       }
start_slew              { yylval.etyp = SLEW_START ;   return EVENT ;       }
end_slew                { yylval.etyp = SLEW_END   ;   return EVENT ;       }
in_saa                  { yylval.etyp = IN_SAA     ;   return EVENT ;       }
out_saa                 { yylval.etyp = OUT_SAA    ;   return EVENT ;       }
in_occult               { yylval.etyp = IN_OCCULT  ;   return EVENT ;       }
out_occult              { yylval.etyp = OUT_OCCULT ;   return EVENT ;       }
off_source              { yylval.etyp = OFF_SOURCE ;   return EVENT ;       }
on_source               { yylval.etyp = ON_SOURCE  ;   return EVENT ;       }
d80 9
a88 9
","                     { /* No value */               return (int)*yytext; }
{MACFILE}               { yyldup(yytext) ;             return MACROFILE ;   }
{YEAR}{DAY}{TIME}       { /* No value */               return UT ;          }
{TIME}                  { /* No value */               return TIME ;        }
{HEX}                   { yyldup(yytext) ;             return HEXSTRING ;   }
{STRING}                { yyldup(yytext) ;             return NAME ;        }
{OSTRING}               { yyldup(yytext) ;             return OBSID ;       }
{D}                     { yylval.ival = atoi(yytext) ; return INTEGER ;     }
{REAL}                  { yylval.fval = atof(yytext) ; return REAL ;        }
d90 1
a90 1
{W}+                    { /* do nothing */ ;                                }
d92 3
a94 3
"\n"                    { linecount++ ;                                     } 
\"                      { /* do nothing */ ;                                }
{COMMENT}               { /* printf( "%s\n", yytext ) */ ;                  } 
@


1.2
log
@For ObsCat
@
text
@d4 1
a4 1
// Subsystem   : XFF
d14 2
a15 3
void skipBlankLines() ;
void getLine() ;
void getString() ;
d18 2
d30 1
d32 1
a32 1
MACFILE {STRING}[.]mac
d37 32
a68 37

"/* Pattern */"         { /* Action */ }

to                      { return TO ; }
OBSERVATION             { return OBSERVATION ; }
GOODTIME:               { return GOODTIMES ; }
EVENTS:                 { return EVENTS ; }
TARGET:                 { return TARGET ; }
START                   { return START ; }
END                     { return END ; }
RA=                     { return RA ; }
DEC=                    { return DEC ; }
ROLL_BIAS=              { return ROLL_BIAS ; }
SLEW_RATE=              { return SLEW_RATE ; }
EDS:                    { return EDS ; }
HEXTE:                  { return HEXTE ; }
MACROS:                 { return MACRO ; }
DCFS:                   { return DCFS ; }
RASTERS:                { return RASTER ; }
"hexte/"{DCFFILE}       { yylval.sval = strdup(yytext) ; return DCF_FILE ; }

PI:{W}                  { getString() ; return PROPOSER ; }
SOURCE:{W}              { getString() ; return SOURCE ; }

LLD[AB]=({D}|DEF)       { yylval.sval = strdup(&yytext[5]); return LLD ; }
ROCK[AB]=({REAL}|0|DEF) { yylval.sval = strdup(&yytext[6]); return ROCK ; }
DWELL[AB]=({D}|DEF)     { yylval.sval = strdup(&yytext[7]); return DWELL ; }

start_slew              { yylval.etype = SLEW_START ; return EVENT ; }
end_slew                { yylval.etype = SLEW_END   ; return EVENT ; }
in_saa                  { yylval.etype = IN_SAA     ; return EVENT ; }
out_saa                 { yylval.etype = OUT_SAA    ; return EVENT ; }
in_occult               { yylval.etype = IN_OCCULT  ; return EVENT ; }
out_occult              { yylval.etype = OUT_OCCULT ; return EVENT ; }
off_source              { yylval.etype = OFF_SOURCE ; return EVENT ; }
on_source               { yylval.etype = ON_SOURCE  ; return EVENT ; }

d71 9
a79 11
","                     { return (int) yytext[0] ; }

{MACFILE}               { yylval.sval = strdup(yytext) ; return MACROFILE ; }
{STRING}                { yylval.sval = strdup(yytext) ; return NAME ; }
{YEAR}{DAY}{TIME}       { return UT ; }
{TIME}                  { return TIME ; }
{HEX}                   { yylval.sval = strdup(yytext) ; return HEXSTRING ; }
{OSTRING}               { yylval.sval = strdup(yytext) ; return OBSID ; }
{D}                     { yylval.ival = atoi(yytext) ; return INTEGER ; }
{REAL}                  { yylval.fval = atof(yytext) ; return REAL ; }

d81 5
a85 6
{W}+                    ; /* do nothing */

^\n                     { skipBlankLines() ; }
"\n"                    { linecount++; }
\"                      ; /* do nothing */
{COMMENT}               { /* printf( "%s\n", yytext ) */ ; }
d91 1
a91 1
void getString()
d93 3
a95 1
  getLine() ;
d99 1
a99 9
void getLine()
{
  buf[0] = '\0' ;
  for( int c=0,i=0 ; (c=getc(yyin)) != '\n'; buf[i++]=c ) ;
  buf[i] = '\0' ;
  linecount++ ;
}

void skipBlankLines()
d101 3
d105 1
@


1.1
log
@Initial revision
@
text
@d29 1
a29 1
OSTRING [0-9][0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]-[0-9][0-9]([A-Z])?
d48 1
a48 1
SLEW=                   { return SLEW ; }
d53 2
a54 1
"hexte/"{DCFFILE}         { yylval.sval = strdup(yytext) ; return DCF_FILE ; }
@

head	5.2;
access
	soccm;
symbols;
locks;
comment	@ * @;


5.2
date	97.02.21.20.35.48;	author ehm;	state Exp;
branches;
next	;


desc
@Moved to PktEDS
@


5.2
log
@checked in with -k by soccm at 1997/03/11 18:49:32
@
text
@//   RCS Stamp: $Id: EARun.C,v 5.2 1997/02/21 20:35:48 ehm Exp $ 
//   Description : Describes an Event Analyser Run
//   Author      : Steve Berczuk- berczuk@@mit.edu
//   Copyright   : Massachusetts Institute of Technology
static const char rcsid[] =
"$Id: EARun.C,v 5.2 1997/02/21 20:35:48 ehm Exp $";
//#define MARK
#include <prof.h>

#include <assert.h>
#include <iostream.h>
#include "science_if.h"
#include "ObsParamBlock.h"
#include <PktEds.h>
#include <EdsPartition.h>
#include <PktPartData.h>
#include "PktPartHdr.h"
#include "ObsComp.h"
#include "PktSwHouse.h"
#include <PktPanicMsg.h>
#include "PktObsCoda.h"
#include "PktObsParam.h"

#include <ErrorManager.h>
#include "PktSequence.h"
#include <rwPersistenceUtils.h>
#include <PartListIter.h>

#include "EARun.h"
RWDEFINE_COLLECTABLE(EARun,EARUN_ID)

// forward declarations of template functions
// to ensure that they get instantiated - Is this necessary? Probably not
void persistCollection(const RWTPtrOrderedVector<ObsComp>,RWvostream&); 
void persistCollection(const RWTPtrOrderedVector<ObsComp>,RWFile&) ;


static void emptyEAFunc (const EARun*) { }


EAR_DISP_FUNC EARun::disposeEARun = emptyEAFunc;
RWBoolean EARun::assumeOrdered = FALSE;
int EARun::incompletePartsToKeep = 20;
int EARun::maxOrphanListSize =100;

// tester function to select from a list based on PartitionSequence Number
RWBoolean sequenceIs(EdsPartition* candidate,  void* number)
{
  
    RWBoolean retval=FALSE;
    if(candidate->hdrSequenceNumber() == *(const int*)number)
        retval=TRUE;
    return retval;
}


// tester function used to select from a list based on obsCompTag
RWBoolean componentIs( ObsComp* candidate,  void* number)
{
    RWBoolean retval=FALSE;
    if((int)candidate->obsCompTag() == *(int*)number)
        retval=TRUE;
    return retval;
}


// tester function to test Packets for a given sequence number
RWBoolean sequenceIs( PktEds* candidate,  void* number)
{
    RWBoolean retval=FALSE;
    if(candidate->sequenceNumber() == *(const int*)number)
        retval=TRUE;
    return retval;
}


// tester function to get the Partition with the partitionNumber
RWBoolean partNumberIs( EdsPartition* candidate,  void* number)
{
    RWBoolean retval=FALSE;
    if(candidate->partNumber() == *(const int*)number)
        retval=TRUE;
    return retval;
}

// Description:
// Default Constructor for EARun; Used only for Rogue Wave Persistence
EARun::EARun()
{

}

// Description:
// Constructor for EARun object taking an EA Run ID & EA Side
EARun::  EARun( int id, int apid):
    eaRunID(EARunID(id, apid)),
    theStartupMsg((PktStartupMsg*)0),
    theObsCoda((PktObsCoda*)0),
    thePanicMsg((PktPanicMsg*)0),
    largestPartSeq(0)
{
  
}


// Description:
// Clone Constructor [NOT FULLY IMPLEMENTED]
EARun::  EARun( const EARun & theRun):
    eaRunID(theRun.eaRunID)
{
    // copy the data somehow
}

// assign one EARun to another
/*
  EARun& EARun:: operator=( const EARun&)
  {
  }

  */
// function to delete each element of list
static void deletePart(EdsPartition* item ,void*){ delete item;}
static void deletePktEds(PktEds*item ,void*){delete item;}


// Description:
// Destructor. Deletes all components of the EARun
EARun::~EARun( )
{
    PktEds::notifyOfDelete(this);
    // tell PktEds that this earun is being deleted

    // delete the partition list
    hkPackets.clearAndDestroy();
    void* x=(void*)0;
    partitionList.clearAndDestroy();
    incompletePartList.clearAndDestroy();
    packetList.clearAndDestroy();
    delete theObsCoda;
    delete thePanicMsg;
    delete theStartupMsg;
}

// Description: 
// Add a partition header packet to this EARun Create a partition and
// check the orphan list for any packets that might need to be
// inserted.
void EARun:: addPacket(const  PktPartHdr* thePacket)
{
    assert(thePacket != 0);
    EdsPartition* thePartition = new EdsPartition(*thePacket);
//   incompletePartList.append(thePartition);
    addToIncompletePartList(thePartition);
   
    uint32 partNum=thePartition->partNumber();
    if(partNum > largestPartSeq) {
        largestPartSeq = (int)partNum;
    }
    processOrphanList();
    processPartitionLists();
   
    if(thePartition->isFull()) {
        if (incompletePartList.remove(thePartition))
	  partitionList.append(thePartition);
    }
    int parts = incompletePartList.entries();
}

// Description:
// Scan through the orphan list to see if any packets can be inserted into
// existing partitions.
void EARun::processOrphanList()
{
    if(orphanList.entries() == 0) return;

//   RWTPtrSlistIterator<PktPartData> it(orphanList);
    size_t numOrphans = orphanList.entries();
   
    PktPartData* pkt=0;
//   while (pkt = it())
    for (int i = numOrphans-1; i >=0; i--)
    {
        pkt = orphanList[i];
        if(addDataPacket( pkt))
        { // if the addition was successful, delete the orphan from the olist
            orphanList.removeAt(i);
            delete pkt; 
            pkt=0;
         
        }

    }
}

// Description:
// move completed partitions from pending list to the complete list
void EARun::processPartitionLists() 
{
    int nPending = incompletePartList.entries();
    RWTValOrderedVector<int>  holdVec(nPending) ;
    for(int i = nPending-1; i >=0; i--) {
        if(incompletePartList[i]->isFull()) {
            holdVec.append(i);
        }
    }
    // we need the 2 loops to avoid having problems with the iteration
    // if we were to remove the elements after they are found.
    int entries =holdVec.entries();
    for(int j=0; j< entries; j++) {
        EdsPartition* p =incompletePartList.removeAt(holdVec[j]);
        partitionList.append(p);
    }
}




// Description::
// Add a Partition Data packet to the orphan list. This method is called 
// when the partition to which it belongs is not immediately available.
void EARun::addToOrphanList(const PktPartData* pkt)
{
    // copy the packet and add it to the orphan list
    int orphanMaxSize = 200;
    
    PktPartData* cpyPkt = new PktPartData(*pkt);
    if(orphanList.entries() > orphanMaxSize) {
        delete (orphanList.removeFirst());
    }

    orphanList.insert(cpyPkt);
}

// Description:
// Adds a Partition Data packet to this EARun. If the packet in inserted in 
// a partition, a pointer to the partition is returned.
EdsPartition* EARun::addDataPacket(const PktPartData* pkt)
{
    assert(pkt);
    EdsPartition* retval=0;
    PktSequence ps((unsigned int)pkt->seconds(),pkt->sequenceNumber());
    EdsPartition* partA =0;
    EdsPartition* partB =0;
    int lastPartSeq = largestPartSeq;
    if(theObsCoda) // if we got the coda
    {
        lastPartSeq= theObsCoda->partitionCount();
    }

// special case: first (and perhaps only) partition 

// Check to see if the data packet : has a sequence number equal to
// the CCSDS sequence number of the partition header packet in of the
// 0th Partition && that the packet's time stamp is within 256
// seconds, where 256 seconds is the minimun time til CCSDS sequnce
// wrap due to SDS/EDS interface requirements
//
   
//check to see if this is the next packet in the current part
    partA = (EdsPartition*) getIncompletePartition(lastPartSeq);
    if(partA){// check below comparison!
        // times should be between 256 secs and pac seq >
        if((pkt->sequenceNumber() ==
            partA->hdrSequenceNumber() +1 + pkt->partitionSequence())
           && (pkt->seconds() < partA->hdrPacketTime()+256) ) {
            partA->addPacket(*pkt);
            return partA;
        }
    }
    //   }

    // search backwards to take advantage of the fact that
    // we are most likely to get a data packet from the last part  inserted.
//   for(int i = 0; i <=lastPartSeq; i++)
    if(! assumeOrdered) {
//       cout << " largest Part seq" << lastPartSeq<<endl;
        int endSearch = ((largestPartSeq - incompletePartsToKeep)>0 ?
                         (largestPartSeq - incompletePartsToKeep):0);
       
        PartitionListIterator iter(incompletePartList);
       
        // iterate over all parts in the incomplete list to find two consectutive
        // partitions
        partA = iter.next();
        while (partB=iter.next() )
        {
            // test to see if we have two consecutive partitionss.
            if(partB->partNumber() == partA->partNumber()+1)
            {
                if((PktSequence(partA->hdrPacketTime(),
                                partA->hdrSequenceNumber()) < ps) &&
                   (ps <PktSequence (partB->hdrPacketTime(),
                                     partB->hdrSequenceNumber())))
                {
                    partA->addPacket(*pkt);
                    retval=partA; 
                    break; // packet inserted
                }
            }
            // special case: last partition
            if(
                (theObsCoda != NULL) &&
                (partA != NULL) && 
                (partA->partNumber() == (theObsCoda->partitionCount() -1))
                )
            {
                partA->addPacket(*pkt);
                retval=partA; 
                break; // packet inserted
            }
            partA=partB;
        }
    }
    
    return retval;
}

// Description:
// Add a Partition Data packet to this EARun
void EARun::addPacket(const  PktPartData* thePacket)
{
    assert(thePacket != 0);
    //Insert the packet into the appropriate partition
    EdsPartition* thePartition = addDataPacket(thePacket);
   
    if(!thePartition) // packet not added to a part
    {
        addToOrphanList(thePacket);
    } 
    else {// packet was added to list; check if it filled a partition
        if(thePartition->isFull()) {
            incompletePartList.remove(thePartition);
            partitionList.append(thePartition);
            // cout << "### Adding partition\n";
        }
    }
    if(isComplete()) // check to see if the EARun is Complete
    {
        disposeAndDelete();
    }
}


// Description:
// Add a Startup Message packet to this EARun
void EARun:: addPacket(const PktStartupMsg* thePacket)
{
    assert(thePacket !=0);
    PktStartupMsg* cpyPkt = new PktStartupMsg(*thePacket);
    theStartupMsg = cpyPkt;

}

// Description:
// Add a Panic Message to an earun and dispose of it
void EARun:: addPacket(const PktPanicMsg* thePacket)
{
    assert(thePacket !=0);
    PktPanicMsg* cpyPkt = new PktPanicMsg(*thePacket);
    thePanicMsg = cpyPkt;
    if (thePacket->sequenceNumber() == 1)
      disposeAndDelete();
}


// Description:
// Add an Observation Coda packet to this EARun
void EARun:: addPacket(const  PktObsCoda* thePacket)
{
    assert(thePacket != 0);
    // we should probbaly check for null pointers here
    PktObsCoda* cpyPkt = new PktObsCoda(*thePacket);
    theObsCoda=cpyPkt;

    if(isComplete()) // check to see if the EARun is Complete
    {
        disposeAndDelete();
    }
    else
    {
        // insert any orphans
        processOrphanList();
        processPartitionLists();
      
        if(isComplete()) // check to see if the EARun is Complete
        {
            disposeAndDelete();
        }

    }

}

// Description:
// Add an Observation Component Header packet to the EARun
void EARun::addPacket(const PktObsCompHdr* thePacket)
{
    assert(thePacket != 0);
    ObsComp* theObsComp = new ObsComp(thePacket);
    // create a component fron this header packet
        
    // add the new obsComp to our list
    configDump.insert(theObsComp);

    if(isComplete()) // check to see if the EARun is Complete
    {
        disposeAndDelete();
    }

}

// Description:
// Add an Observation Component packet to the EARun
void EARun::addPacket(const PktObsParam* thePacket)
{
    assert(thePacket != 0);
// find the appropriate component
    ObsComp* theParams =  configDump.getComponent(thePacket->obsCompTag());

    if(theParams)
    {
        theParams->addPacket(thePacket);
    }
    else
    {
        // UNIMPLEMENTED
        // what should we do if it can't be found? create it?
    }
    if(isComplete()) // check to see if the EARun is Complete
    {
        disposeAndDelete();
    }

}

// Description:
// Add a generic Eds packet to the EARun. This function is a fallback
// for packets for which other dispositions have not been specified
void EARun::addPacket(const PktEds* thePacket)
{
    assert(thePacket != 0);
    // just append the packet to the packet list
    PktEds* cpyPkt = new PktEds(*thePacket);
    packetList.append(cpyPkt);

}

// Description:
// Add a Software Housekeeping packet to the EARun
void EARun::addPacket(const PktSwHouse* thePacket)
{
    assert(thePacket != 0);

    // just append the packet to the packet list
    PktSwHouse* cpyPkt = new PktSwHouse(*thePacket);
    hkPackets.append(cpyPkt);

    if(isComplete()) // check to see if the EARun is Complete
    {
        disposeAndDelete();
    }

}


// Description:
// Return an Observation Component from the observation parameter dump.
ObsComp* EARun::getComponent( e_obs_comp compType)
{
    ObsComp* retval = configDump.getComponent(compType);
    return retval;
}



// Description:
// Return a High priority packet from the EARun by CCSDS sequence number
// Assumes that there will be only one packet with that sequence
// number in the EARun.
const PktEds* EARun::getHkPacket(int sequenceCCSDS)
{
  const PktEds* retval=0;
  if (sequenceCCSDS == 0)
    retval = theStartupMsg;
  size_t num = packetList.entries();
  for (size_t ii =0; ii < num ; ii++){
    if (packetList.at(ii)->sequenceNumber() == sequenceCCSDS){
      retval = packetList.at(ii);
      break;
    }
  }
  if (!retval){
    num = hkPackets.entries();
    for (size_t ii =0; ii < num ; ii++){
      if (hkPackets.at(ii)->sequenceNumber() == sequenceCCSDS){
	retval = hkPackets.at(ii);
	break;
      }
    }
  }  
  return retval;
}

// Description:
// Return a Data packet from the EARun by CCSDS sequence number
// Assumes that there will be only one packet with that sequence
// number in the EARun.
const PktEds* EARun::getPacket(int sequenceCCSDS)
{
    const PktEds* retval=0;

    size_t numParts = incompletePartList.entries();
    EdsPartition* testPart;

    // search all the partitions first
    for(size_t i =0; i < numParts; i++){
      testPart= incompletePartList[i];
      retval=testPart->getPacket(sequenceCCSDS);
      if(retval) // we found one in this partition
	break;
    }
    
    // check the other list
    if(!retval){
      numParts = partitionList.entries();
      for( i =0; i < numParts; i++)
	{
	  testPart= incompletePartList[i];
	  retval=testPart->getPacket(sequenceCCSDS);
	  if(retval) // we found one in this partition
	    break;
	}
    }
    // now search the orphan list, the coda and the parameter dump.
    // [TBD: NOT IMPLEMENTED YET ]

    return retval;
}
  
  

// statistics methods
  
// Description:
// Return the number of started partitions in this EARun
int EARun::numPartitions() const
{
    return (incompletePartList.entries() +
            partitionList.entries());
}

static void numCompleteParts_apply(EdsPartition* ep, void *vp)
{
    int *ip = (int*)vp;
    if (ep->isFull())
	(*ip) ++;
}

// Description:
// Return the number of completed partitions in this EARun
int EARun::numCompleteParts()const 
{
    int count=0;
    EARun* self = (EARun*)this;
    // cast away constness
    self->partitionList.apply(numCompleteParts_apply, (void*)&count);
#if 0
    for(int i =0 ; i < partitionList.entries(); i++)
    {
        if(self->partitionList.at(i)->isFull())
            count++;
    }
#endif 0
    return count;
}

// Description:
// Return the number of dropped partitions from the EARun.
// Sums up dropped partition statistics from the software housekeeping
// packets.
int EARun::numDroppedParts() const
{
    // iterate over the packets in hkPackets and return the sum of
    // the dropped partition stats.
    int count=0;
    for(int i=0; i < hkPackets.entries(); i++)
    {
        count += hkPackets[i]->getStat(LOG_SCIMODE_FF_DROP_PART);
    }
    return count;
} 


// Description:
// Return the number of packets sent from the EDS to the SDS
// based on downlink interrupts
int EARun::numPktsSent() const
{
    int count=0;
    for(int i=0; i < hkPackets.entries(); i++)
    {
        count += hkPackets[i]->getStat(LOG_DMA_DOWNLINK_INTS);
        // count the downlink interrupts
    }
    return count;



}

static void numPackets_apply(EdsPartition* ep, void *vp)
{
    int *ip = (int *)vp;

    *ip += ep->numPackets();
}

// Description:
// Return the total number of packets in the EARun
int EARun::numPackets() const
{
    int t, i;
    // count the number of packets in each part
    int pktCnt=0;

    EARun* self = (EARun*)this;
    // cast away constness
    self->partitionList.apply(numPackets_apply, (void*)&pktCnt);
#if 0
    t =  partitionList.entries();
   
    for (i =0; i < t; i++)
    {
        pktCnt += partitionList.at(i)->numPackets();
    }
#endif 0
    // would use apply() for incompletePartList, but 
    // RWTPtrOrderedVector has no apply()
    t =  incompletePartList.entries();
    for ( i =0; i < t; i++)
    {
        pktCnt += incompletePartList.at(i)->numPackets();
    }

    if(theStartupMsg) pktCnt++;
    if(theObsCoda) pktCnt++;
    if(thePanicMsg) pktCnt++;
    pktCnt+=hkPackets.entries();
    return (pktCnt +configDump.numPackets() +
            packetList.entries() +orphanList.entries());
}
  

// Description:
// Set the handler function to be called when we have a full EARun.
EAR_DISP_FUNC EARun::setDisposition(EAR_DISP_FUNC theFunc)
{
    EAR_DISP_FUNC ohandler =disposeEARun;
    disposeEARun = theFunc;
    return ohandler;
}

// Description:
// return a pointer to a complete partition - by sequence number
const EdsPartition* EARun::getPartition(int whichOne) const 
{
    EdsPartition* thePart=0;
    int parts = partitionList.entries();

    for(int i = parts-1; i>=0; i--) {
        EdsPartition* tpart = partitionList[i];
        if(tpart->partNumber() == whichOne)
        {
            thePart = tpart;
            break;
        }
    }
        
    return thePart;
    
}





// Description:
//  Return a pointer to a partition by partition sequence number
const EdsPartition* EARun::getIncompletePartition(int whichOne) const
{
    EdsPartition* thePart=0;
    
    int parts = incompletePartList.entries();
    // cout << "### incomplete list: " << parts ;
    // cout << " complete list: " << partitionList.entries() <<endl;
    
//    for(int i = 0; i<parts; i++) {
    for(int i=parts-1; i>=0; i--) {
        EdsPartition* tpart = incompletePartList[i];
        if(tpart->partNumber() == whichOne)
        {
            thePart = tpart;
            break;
        }
    }
/*
  if (!thePart) // check the complete list
  {
  parts = partitionList.entries();

  for(i = parts-1; i>=0; i--) {
  EdsPartition* tpart = partitionList[i];
  if(tpart->partNumber() == whichOne)
  {
  thePart = tpart;
  break;
  }
  }
        
  }
  */    
    return thePart;
}

// Description:
// Return an identifier for an EA that corresponds to the applicationID
// contained in the EARunID;
EventAnalyserID EARunID::eaID() const
{
    switch (appID)
    {
    case 48:
    case 49:
    case 50:
    case 51:
        return(EDS_EA1);
    case 52:
    case 53:
    case 54:
    case 55:
        return(EDS_EA2);
    case 56:
    case 57:
    case 58:
    case 59:
        return(EDS_EA3);
    case 60:
    case 61:
    case 62:
    case 63:
        return(EDS_EA4);
    case 64:
    case 65:
    case 66:
    case 67:
        return(EDS_EA5);
    case 68:
    case 69:
    case 70:
    case 71:
        return(EDS_EA6);
    case 72:
    case 73:
    case 74:
    case 75:
        return(EDS_EA7);
    case 76:
    case 77:
    case 78:
    case 79:
        return(EDS_EA8);
    default:
        return (EA_OTHER);
    }

}

// Description:
// Print an EventAnalyserID on an ostream.
ostream& operator<<(ostream& os, const EventAnalyserID id)
{

    switch(id){
    case EDS_EA1:
        os << "EDS_EA1 ";
        break;
    case EDS_EA2:
        os << "EDS_EA2 ";
        break;
    case  EDS_EA3:
        os <<"EDS_EA3 ";
        break;
    case EDS_EA4:
        os <<"EDS_EA4 ";
        break;
    case EDS_EA5:
        os << "EDS_EA5 ";
        break;
    case EDS_EA6:
        os <<"EDS_EA6 ";
        break;
    case EDS_EA7:
        os << "EDS_EA7 ";
        break;
    case EDS_EA8:
        os <<"EDS_EA8 ";
        break;
    default:
        os <<" BAD_EA(" <<(int)id<<") "; 
        // print out the value which caused us to get here
    }

    return os;
}


// Description:
// Print the EARunID to an ostream in the format
// EA: <Event Analyzer> ID: <Run ID-Number> SIDE: <A or B>
ostream& operator<<(ostream& os, const EARunID& id)
{
    os <<"EA:" <<id.eaID()<<"ID: " << id.eaRunID<< " SIDE: " ;
    switch(id.eaSide)
    {
    case SIDEA:
        os << "A ";
        break;
    case SIDEB:
        os << "B ";
        break;
    default:
        os <<"?";
    }
    return os;
  
}

// Description:
// Checks to see if this EARun is complete. An EARun can be complete if
// all the data packets are there with a obsparamdump and Obs Coda OR
// if we receive a panic message and have all the packets in sequence
RWBoolean EARun::isComplete() const
{
    // determine if the EARun is complete
    // check to see if we have all the pieces
    RWBoolean retval =FALSE;

    if(theStartupMsg &&theObsCoda && configDump.isComplete())
      // check to see that we have all of the PartitionPackets
      if((numCompleteParts() + numDroppedParts()) ==  theObsCoda->partitionCount())
	if(orphanList.entries() != 0) 
	  //	    if ((numPktsSent() +1) == numPackets())
	  retval = TRUE;
    // check for panic message
 
    return retval;
}

RWBoolean EARun::isPaniced()
{
    if(thePanicMsg) 
        return TRUE;
    else
        return FALSE;
}

// Description:
// Return the start time of the partition. Obtained from any partition
// in the EARunList
uint32 EARun::startTime() const 
{
    uint32 retval =0;
    // get the obs start time from the coda for now
    if(!incompletePartList.isEmpty()) {
        EdsPartition* thePart = incompletePartList.first();
        retval = thePart->obsStartTime();
    } 
    else if (!partitionList.isEmpty())  {
        EdsPartition* thePart = partitionList.first();
        retval = thePart->obsStartTime();
    }
    return retval;
}

// Description:
// Return the end time of the run. EndTime is the time of the
// last bit of data sent from the EDS. 
// if not the time is taken to be partheader time of the last
// Complete Partition.
// If there are no complete partitions, the startTime is returned.
//

uint32 EARun::endTime() const
{
    uint32 retval =  startTime();
    if (!partitionList.isEmpty())  {
      EdsPartition* thePart = partitionList.last();
      retval = thePart->hdrPacketTime();
    }   
    return retval;  
}



// Description:
// Return the application id of the packets in the partition.
int EARun::applicationID () const
{
    int retVal =0;
    // for now get the application id from the partition list
    if(! incompletePartList.isEmpty()){
        EdsPartition* thePart = incompletePartList.first();
        retVal = thePart->applicationID();
    }
    else if (!partitionList.isEmpty()) {
        EdsPartition* thePart = partitionList.first();
        retVal = thePart->applicationID();
    }
    return retVal;
    
}

// Description:
// Add a partition to the incomplete part list
// keeping only the number we want in memory.
// assumes that thePart is already allocated.
void EARun::addToIncompletePartList(EdsPartition* thePart)
{
    int entries  = incompletePartList.entries();
//    cout << "incomplete part list : " << entries <<endl;
    
    if(entries < incompletePartsToKeep) {
        incompletePartList.append(thePart);
    } else {
        // remove the oldest, then append
        delete(incompletePartList.removeFirst());
        incompletePartList.append(thePart);
        
    }
    
}


// Description:
// Call the handler function and delete the EARun.
void EARun::disposeAndDelete()
{
    disposeEARun(this); // call handler

    // gather stats on this EARun for PktEds
    PktEds::updateEARStats(this);
    delete this; // delete this earun
  
}




// Description:
// return the size required to store this EARun. Inherited from RWCollectable 
RWspace EARun ::binaryStoreSize ( ) const
{
    RWspace space= RWCollectable::binaryStoreSize();
    space += sizeof(eaRunID);
    space += (configDump.binaryStoreSize() 
	      + collectionStoreSize(partitionList) 
	      + collectionStoreSize(hkPackets) 
	      + collectionStoreSize(packetList) 
	      + collectionStoreSize(orphanList) );
    if(theStartupMsg)
        space += theStartupMsg->binaryStoreSize();
    if(theObsCoda)
	space += theObsCoda->binaryStoreSize();
    if(thePanicMsg)
	space += thePanicMsg->binaryStoreSize();
    return space;
}
 
 
 
// Description:
//  Compare two EARuns [not completely implemented]
int EARun ::compareTo ( const RWCollectable* x ) const
{
    return RWCollectable::compareTo(x);
}
 
 
 
// Description:
// Determine if two EARuns are equal for RW purposes
RWBoolean EARun ::isEqual ( const RWCollectable* x ) const
{
    return this->RWCollectable::isEqual(x);
}
 
 
 
// Description:
// Return a hashing function for this EARun
unsigned EARun ::hash ( ) const
{
    return eaRunID.eaRunID;
}
 
// Description:
// Save an EARun to an RWFile
void EARun ::saveGuts ( RWFile& f ) const
{
    RWCollectable::saveGuts(f);
    f.Write(largestPartSeq);
    f<<configDump <<theStartupMsg << theObsCoda<< thePanicMsg;
    persistCollection(partitionList,f);
    persistCollection(incompletePartList,f);
    persistCollection(packetList,f);
    persistCollection(hkPackets,f);
    persistCollection(orphanList,f);
    f<<theStartupMsg<<theObsCoda<<thePanicMsg;
    // save the EARunID
    f.Write(eaRunID.eaRunID);
    f.Write(eaRunID.appID);
}
 
 
// Description:
// Restore and EARun from and RWFile. Method inherited from RWCollectable
void EARun ::restoreGuts ( RWFile& f )
{
    RWCollectable::restoreGuts(f);
    f.Read(largestPartSeq);
    f>>configDump >>theStartupMsg >> theObsCoda >> thePanicMsg;
    restoreCollectionFromFile(partitionList,EdsPartition,f);
    restoreCollectionFromFile(incompletePartList,EdsPartition,f);
    restoreCollectionFromFile(packetList,PktEds,f);
    restoreCollectionFromFile(hkPackets,PktSwHouse,f);
    restoreCollectionFromFile(orphanList,PktPartData,f);
    f>>theStartupMsg>>theObsCoda>>thePanicMsg;
    int runid,apid;
    f.Read(runid);
    f.Read(apid);
    eaRunID= EARunID(runid,apid);
}
 
 
 
// Description:
// Restore an EARun from a vistream. Method inherited from RWCollectable
void EARun ::restoreGuts ( RWvistream& s )
{
    RWCollectable::restoreGuts(s);
    s>>largestPartSeq;
    s>>configDump >>theStartupMsg >> theObsCoda >> thePanicMsg;
    restoreCollectionFromStream(partitionList,EdsPartition,s);
    restoreCollectionFromStream(incompletePartList,EdsPartition,s);
    restoreCollectionFromStream(packetList,PktEds,s);
    restoreCollectionFromStream(hkPackets,PktSwHouse,s);
    restoreCollectionFromStream(orphanList,PktPartData,s);
    s>>theStartupMsg>>theObsCoda>>thePanicMsg;
    // save the EARunID
    s>>eaRunID.eaRunID;
    s>>eaRunID.appID;
}
 
// Description:
//  Save an EARun from a vostream. Method inherited from RWCollectable
void EARun ::saveGuts ( RWvostream& s ) const
{
    RWCollectable::saveGuts(s);
    s<<largestPartSeq;
    s<<configDump <<theStartupMsg << theObsCoda<< thePanicMsg;
    persistCollection(partitionList,s);
    persistCollection(incompletePartList,s);
    persistCollection(packetList,s);
    persistCollection(hkPackets,s);
    persistCollection(orphanList,s);
    s<<theStartupMsg<<theObsCoda<<thePanicMsg;
    // save the EARunID
    s<<eaRunID.eaRunID;
    s<<eaRunID.appID;
}
 
 






@

head	4.4;
access
	soccm;
symbols
	Build4_3_2:4.2
	Build4_3_1:4.1
	Build4_3:1.5
	Build4:1.5
	Build3_3:1.4
	Build3_2:1.4;
locks;
comment	@ * @;


4.4
date	97.03.11.22.05.00;	author soccm;	state Exp;
branches;
next	4.3;

4.3
date	95.11.07.21.30.46;	author rbarnett;	state Exp;
branches;
next	4.2;

4.2
date	95.06.21.16.21.18;	author rbarnett;	state Exp;
branches;
next	4.1;

4.1
date	95.05.25.20.35.33;	author rbarnett;	state Exp;
branches;
next	1.5;

1.5
date	95.01.23.15.03.00;	author rbarnett;	state Exp;
branches;
next	1.4;

1.4
date	94.09.23.14.17.29;	author rbarnett;	state Exp;
branches;
next	1.3;

1.3
date	94.09.19.18.33.17;	author rbarnett;	state Exp;
branches;
next	1.2;

1.2
date	94.08.29.14.34.41;	author rbarnett;	state Exp;
branches;
next	1.1;

1.1
date	94.08.04.15.10.40;	author rbarnett;	state Exp;
branches;
next	;


desc
@accessor for EDS data
@


4.4
log
@added const types
@
text
@// ==========================================================================
// File Name   : AccEDS.h
// Subsystem   : EDS Data Management
// Author      : Steve Berczuk - berczuk@@mit.edu
//               Randall D. Barnette - rbarnett@@xema.stx.com
// Description : Base class for EDS Accessors
// Copyright   : Massachusetts Institute of Technology
//
// RCS: $Id: AccEDS.h,v 4.3 1995/11/07 21:30:46 rbarnett Exp $
//
// .NAME    AccEDS - A Data Management class to access data in an EDS partition
// .LIBRARY dmEDS
// .HEADER  Data Management 
// .INCLUDE AccEDS.h
// .FILE    AccEDS.C
// .VERSION $Revision$
// ==========================================================================

// .SECTION Author
// Steve Berczuk, MIT <berczuk@@mit.edu>
//
// Randall D. Barnette, Hughes STX, rbarnett@@xema.stx.com

// .SECTION Description
// This class moderates data access requests from the data management
// subsystem to the data in EdsPartitions.

// .SECTION See Also
// EDSDataSet(3), EARun(3), EdsPartition(3)

#ifndef ACCEDS_H
#define ACCEDS_H

// SOC headers
#include <Accessor.h>
#include <RcPointer.h>

// Local headers
#include <EdsPartition.h>
#include <EDSDataSet.h>

class AccEDS : public Accessor {

    RWDECLARE_COLLECTABLE(AccEDS)

public:
  
  //* Constructor

  //
  // Create an accessor from a partition and a pointer to a parent dataset
  //
  AccEDS(const EdsPartition* p, const EDSDataSet* d=0);

  //% Private default constructor for Rogue Wave use.
  AccEDS(void) ;

  //* Destructor

  ~AccEDS(void);

  //* Public class methods

  // Set the parent data set for this Accessor.
  void setDataSet(const EDSDataSet* ds);

  // return the sequence number of the partiton we are accessing
  unsigned long partNumber(void) const;

  // Fetch the parent data set.
  const EDSDataSet* getDataSet(void) const ;

  // flush the data cache for this accessor
  void flushCache();

  //% The following are redefined from Accessor.

  // Access the data pointed to by this accessor,
  DataValue* access(const Descriptor& d) const;
 
  // return a count of the number of items in the descriptor list
  int descriptorCount(void) const;

  // Returns 1 if the listed descriptor is available, 0 else
  int isAvailable(const Descriptor& d) const;
    
  // Return the appid of the packets in this partition
  int apid(void) const;
  
  //% The following are redefined from Rogue Wave RWCollectable.

  // compares the Accessors for ordering
  int compareTo(const RWCollectable* rhs) const;
  RWBoolean isEqual(const RWCollectable* rhs) const;
  RWspace binaryStoreSize(void) const;
  void restoreGuts(RWFile& f);
  void restoreGuts(RWvistream& s);
  void saveGuts(RWFile& f) const;
  void saveGuts(RWvostream& s) const;

private: // Methods

  //* Private Methods

  //% Access special data that may not be in the partition data directly.
  DataValue* accessEdsStandard(const Descriptor& d) const ;
  DataValue* accessEdsSpecial(const Descriptor& d) const ;
  DataValue* accessZ(const Descriptor& d) const;
  DataValue* accessCoda(const Descriptor& d) const;

private: // Data

  //* Private Data
  //% The partition being stored and accessed by this accessor.

  EdsPartition* thePartition;

  //% Data set in which this accessor is contained.

  const EDSDataSet* parentDataSet;
 
  // Cache to hold unpacked part data
  ConstRcPointer<UShortValVec> dataCache;

} ;

// Description:
// Set the parent data set.

inline void AccEDS::setDataSet(const EDSDataSet* ds)
{
  parentDataSet=ds;
}

// Description:
// Fetch the parent data set.

inline const EDSDataSet* AccEDS::getDataSet(void) const
{
  return parentDataSet ;
}

#endif /* ACCEDS_H */
@


4.3
log
@added caching
@
text
@d9 1
a9 1
// RCS: $Id: AccEDS.h,v 4.1 1995/05/25 20:35:33 rbarnett Exp $
d16 1
a16 1
// .AUTHOR  XTE SOC V4.1
d39 2
a40 2
#include "EdsPartition.h"
#include "EDSDataSet.h"
d55 1
a55 1
  //% Private default comstructor for Rogue Wave use.
d123 1
a123 1
  RcPointer<UShortValVec> dataCache;
@


4.2
log
@fixed EDS problems for production data
@
text
@d36 1
a66 6
  // Is this data set event data?
  int isEventData(void) const ;

  // Is this data set single bit data?
  int isSBData(void) const ;

d73 3
d121 3
@


4.1
log
@Build freeze
.`
@
text
@d9 1
a9 1
// RCS: $Id: AccEDS.h,v 1.5 1995/01/23 15:03:00 rbarnett Exp $
d68 3
@


1.5
log
@Added genman comments.
@
text
@d9 1
a9 1
// RCS: $Id: AccEDS.h,v 1.4 1994/09/23 14:17:29 rbarnett Exp $
@


1.4
log
@added isEqual
@
text
@d5 1
d9 1
a9 1
// RCS: $Id: AccEDS.h,v 1.3 1994/09/19 18:33:17 rbarnett Exp $
d11 1
a11 1
// .NAME    AccEDS - A DM class to access data in an EDS partition
d16 1
a16 1
// .AUTHOR  XTE SOC V3.2
d20 3
a22 1
// Steve Berczuk, berczuk, MIT, berczuk@@mit.edu
d28 3
d34 1
a34 4
class EdsPartition;
class EDSDataSet;
class DataValue;

d37 3
a39 1
#define AccEDS_ID 214
d47 13
a59 2
    AccEDS(const EdsPartition* p=0, const EDSDataSet* ds=0);
    // create an accessor from a partition and a pointer to a parent dataset
d61 1
a61 2
    ~AccEDS(void);
    //destructor
d63 2
a64 1
    void setDataSet(const EDSDataSet* ds);
d66 13
a78 4
    DataValue* access(const Descriptor&) const;
    // access the data pointed to by this accessor
    // this should be a pure virtual function, but since it is a collectable
    // class it cannot be
d80 2
a81 2
    int descriptorCount(void) const;
    // return a count of the number of items in the descriptor list
d83 2
a84 2
    int isAvailable(const Descriptor& d) const;
    // returns 1 if the listed descriptor is available, 0 else
d86 2
a87 2
    int apid(void) const;
    //return the appid of the packets in this partition
d89 1
a89 1
    // rogue wave overridden methods
d91 10
a100 5
    int compareTo(const RWCollectable*) const;
    // compares the Accessors for ordering
    
    RWBoolean isEqual(const RWCollectable*) const;
    // compares the Accessors for ordering
d102 1
a102 2
    unsigned long partNumber(void) const;
    // return the sequence number of the partiton we are accessing
d104 5
a108 2
    //Persistence methods
    RWspace binaryStoreSize(void) const;
d110 1
a110 4
    void restoreGuts(RWFile&);
    void restoreGuts(RWvistream&);
    void saveGuts(RWFile&) const;
    void saveGuts(RWvostream&) const;
d112 2
a113 1
    const EDSDataSet* getPDS(void) ;
d115 1
a115 1
private:
d117 1
a117 2
    EdsPartition* thePartition;
    // the partition being accessed
d119 1
a119 6
    const EDSDataSet* parentDataSet;
    // data set in which this accessor is contained.
    
    DataValue* accessEdsStandard(const Descriptor& d) const ;
    // return a DataValue if the descriptor is generic
    // NULL otherwise
d121 1
a121 2
    DataValue* accessEdsSpecial(const Descriptor& d) const ;
    // access Eds data that has some instrument specific behaviour
d123 2
a124 1
    DataValue* accessZ(const Descriptor& d) const;
d126 4
a129 1
    DataValue* accessCoda(const Descriptor& d) const;
d131 2
a132 1
};
d134 4
a137 2
inline void AccEDS::setDataSet(const EDSDataSet* ds) { parentDataSet=ds; }
inline const EDSDataSet* AccEDS::getPDS(void) { return parentDataSet ; }
@


1.3
log
@Eds->EDS
@
text
@d1 12
a12 8
//   RCS Stamp: $Id: AccEDS.h,v 1.2 1994/08/29 14:34:41 rbarnett Exp $
//   Description : Base class for EDS Accessors
//   Author      : Steve Berczuk - berczuk@@mit.edu
//   Copyright   : Massachusetts Institute of Technology

// .NAME AccEDS
// .LIBRARY Data Management
// .HEADER Access Data from an EdsPartition 
d14 4
a17 1
// .FILE  ../src/AccEDS.C
d20 1
d22 2
a23 2
// This class moderates data access requests from the data management subsystem
// to the data in EdsPartitions.
d25 2
a26 2
#ifndef ACCEDS_h
#define ACCEDS_h
d45 1
a45 1
    ~AccEDS();
d55 1
a55 1
    int descriptorCount() const;
d61 1
a61 1
    int apid() const;
d69 4
a72 1
    unsigned long partNumber() const;
d76 1
a76 1
    RWspace binaryStoreSize() const;
d83 2
a84 1
    const EDSDataSet* getPDS(void) { return parentDataSet ; }
d86 1
d107 1
d109 1
a109 1
#endif
@


1.2
log
@Build 3.2
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: AccEDS.h,v 1.1 1994/08/04 15:10:40 rbarnett Exp $
d21 1
a21 1
class EdsDataSet;
d34 1
a34 1
    AccEDS(const EdsPartition* p=0, const EdsDataSet* ds=0);
d40 1
a40 1
    void setDataSet(const EdsDataSet* ds);
d72 1
d77 1
a77 1
    const EdsDataSet* parentDataSet;
d93 1
a93 1
inline void AccEDS::setDataSet(const EdsDataSet* ds) { parentDataSet=ds; }
@


1.1
log
@Initial revision
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: AccEDS.h,v 3.3 1994/07/11 15:02:19 berczuk Exp $
d21 1
d34 2
a35 3
    AccEDS(const EdsPartition* =0);
    // create an accessor with a known number of energy channels
    // and a known number of Position Bins
d40 2
d61 3
d75 3
d79 1
a79 1
    DataValue* accessEdsStandard(const Descriptor&) const ;
d83 6
a88 1
    DataValue* accessZ(const Descriptor&) const;
d92 1
@

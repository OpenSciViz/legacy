head	5.4;
access
	soccm;
symbols;
locks;
comment	@ * @;


5.4
date	97.03.11.22.35.31;	author soccm;	state Exp;
branches;
next	5.3;

5.3
date	96.06.10.19.39.03;	author nessus;	state Exp;
branches;
next	;


desc
@Moved to PktEDS
@


5.4
log
@genman updates
@
text
@//   LastEdit : "Fri Feb  5 10:30:27 1993"
//   Description : Defines an EdsPartition
//   Author      : Steve Berczuk- berczuk@@mit.edu
//   Copyright   : Massachusetts Institute of Technology

//   RCS: $Id: EdsPartition.h,v 5.3 1996/06/10 19:39:03 nessus Exp $ 

// REVIEWED: 12-28-93

#ifndef EDSPARTITION_H
#define EDSPARTITION_H

// .NAME EdsPartition - a class to interface to partitions of EDS data
// .LIBRARY pkts
// .HEADER Data Access Functions
// .INCLUDE EdsPartition.h
// .FILE EdsPartition.C

// .SECTION Description
// The EdsPartition Class presents an interface to "partitions" of
// data telemetered by the Eds. This file also defines a class to
// represent the Partition Flags. For more information about Partition
// processing, refer to the Science Thread Module Specification,
// MIT Document number 64-30112.0601

// includes

#include <rw/collect.h>
#include <rw/tvvector.h>
#include <rw/sortvec.h>
#include <rw/bitvec.h>

#include <mit_td.h>
#include <ObsParamBlock.h>
#include <RcPointer.h>
#include "EARunID.h"

#define EdsPartition_ID 213

// forward declarations
class ostream;
class PktPartHdr;
class PktPartData;
class PktEds;
class EdsPartition;

typedef void (*PDISP_FUNC)( const EdsPartition*);
// Pointer to a handler to handle full partitions

// EDS Part flags is used to isolate bit fields for the partition flags in
// the Eds Partition Header; also add the code to init the vector with the
// bits of a vector

class EdsPartFlags : public RWBitVec
{

    friend ostream& operator<<(ostream& os, const EdsPartFlags& flags);
public:
    EdsPartFlags();
    EdsPartFlags(unsigned char v);
    RWBoolean suspended()  const ;
    // is suspended flag set?

    RWBoolean spillage()const;

//    RWBoolean burstTrigger() const ;
    //   RWBoolean hiBurstTrigger()const ;
    // is burst trigger flag set?
    //RWBoolean loBurstTrigger() const;

};


ostream& operator<<(ostream& os, const EdsPartFlags& f);
// ostream operator for EdsPartFlags
// dumps the state of the partition flags

inline EdsPartFlags::EdsPartFlags():
    RWBitVec(8)
{
}

// for the part flag accessors below:
// The masking to yield the values is specified in section 3.2.3.2.1
// of Doc # 64-30112.0601 (Science Thread Spec)

inline RWBoolean EdsPartFlags::suspended() const
{
    return testBit(4);
}

/* These are part of mode specific, not part flags
inline  RWBoolean EdsPartFlags::burstTrigger() const
{
    return testBit(3);
}

inline  RWBoolean EdsPartFlags::hiBurstTrigger()const
{
    return testBit(2);
}

inline  RWBoolean EdsPartFlags::loBurstTrigger() const
{
    return testBit(1);
}
*/

inline  RWBoolean EdsPartFlags::spillage()const
{
    return testBit(0);
}


typedef RWTValVector<unsigned short> UShortValVec;

class EdsPartition : public RWCollectable
{
public:
    EdsPartition(); 
   // null constructor

    virtual ~EdsPartition();
    // destructor
  
    EdsPartition(const PktPartHdr& partHdr);
    EdsPartition(const EdsPartition&);
    // clone constructor


    void addPacket(const PktPartData& thePacket);
    // add a data packet to the partition
    // the packet knows its sequence and as such it is inserted in the correct
    // place
  
//    void addPacket(const PktPartHdr& thePacket);
    // add a header packet to a previously existing partition

    int hdrSequenceNumber()const;
    // returns the sequence number of the header packet;

    uint32 hdrPacketTime() const;
    int isFull() const; 
    // returns TRUE if the partition is full

    int numPackets() const;
    // returns the number of packets in the EARun

    uint32 realPartitionSize() const;
    // returns the number of words of the data portion of the data packets 
    //  in the partition. This is calculated by summing the length of the data
    // portions of the daa packets present.
    // This is the compressed value and may or not be the same as the
    // partition length field.

    int packedSize() const;
    // return the packed size of the partition
    // This method delegates to the Part map of the configuration
  
    uint32 partNumber() const;
    // return the sequence number of this partition within the EARun

    uint32 obsStartTime() const;
    // return the  start time from the obsStartTime field of the 
    // Partition Header packet belonging to this partition.
    // This value is in seconds.

    double partStartTime() const;
    //return the calculated start time of the partition (in seconds)
  
    
    EARunID eaRunId() const;
    
    
    int applicationID () const;
    // return the application ID of the packets in this partition
  
    int partitionLength()const;
    //  returns the value of the partition length field of the header
    //  in words (1 word = 2bytes). Will be set to -1 to indicate that it 
    //  is not set

    uint32 getConfigurationID() const;
    // get the configuration ID of this partition

    unsigned char modeFlags() const;
    // return the mode flags from the partition

    const  PktEds* getPacket(const int ccsdsSequence);
    // retrieve a packet from the partition by CCSDS sequence number

    unsigned short* getData() const;
    // return an allocated buffer containing the data from the partition
    // after the partmap has been applied and the data decompressed.
    // the client is responsible for freeing the storage allocated
    // by this function
    
    virtual ConstRcPointer<UShortValVec> getDataVector() const;
    // return the data as a vector of unsigned shorts

    unsigned short* getRawData() const;
    //return an allocated buffer containing the data from the partition.
    // the client is responsible for freeing the storage allocated
    // by this function

    RcPointer<UShortValVec> getRawDataVector() const;
    // get a vector containing the raw data
    RWBoolean matches (const EdsPartition& a) const;
    // compare the data portions of two partitions -bytewise
    RWBoolean operator== (const EdsPartition&) const;
    //test for idendity using partition number 
    
    RWBoolean operator< (const EdsPartition&) const;
    // compare order of partitions based on partition number
    
    void dumpHeader(ostream& os) const;
    // display the header to os, nicely formatted

    void dumpData(ostream& os) const;
    // dump the unpacked partition data to the ostream
    // in a format appropriate to the configuration

    static PDISP_FUNC setDisposition(PDISP_FUNC);
    // set the function to call when an Partition is complete

    MitBool isError() const { return MitBool(!_errorMessage.isNull()); }
    // Returns true iff an error has occurred while building the object.

    RWCString errorMessage() const { return _errorMessage; }

    ObsParamBlockPtr config() const;
    // If the partition is from a PosHistMode config, then this method
    // will check to see if this Partition has been created from
    // telemetry or restored from a file.  In the former case, the
    // config will be restored from the param dump in the EARun.  In
    // the later case, it will be restored from the EdsConfig
    // database.  If the partition is not from a PosHistMode config,
    // then the config will be restored frrom the EdsConfig database.

    void setError(const RWCString& errorMessage);
    // This method is used if an error has occurred while building an
    // EdsPartition object.

    // rogue wave persistence functions
    RWspace binaryStoreSize() const;
    int compareTo(const RWCollectable* )const;
    unsigned hash() const;
    void restoreGuts (RWFile&);
    void restoreGuts (RWvistream&);
    
    void saveGuts (RWFile&)     const;
    void saveGuts (RWvostream&) const;

    EdsPartFlags partFlags;
      // partition bit field flags

    RWDECLARE_COLLECTABLE(EdsPartition)


private:

    //**** Private methods: ****

    void dispose() const;
    // call the disposePartition method on this partition
    // after checking that it exists

    int calculatePackedSize() const;
    // calculate the packed size based on the unpacked data size and
    // the configuration;

    ObsParamBlockPtr getConfigFromDb() const;

    RWBoolean hasHeader() const;
    // returns TRUE if we have an initialized header
    // FALSE if we hae not received a header packet

    //**** Private class variables: ****

    static  PDISP_FUNC disposePartition;
    // function called when we receive a full partition

    static ObsParamBlockPtr _configCache;
    // The config that was used to make this partition.

    static MitBool _isConfigCacheFromParamDump;
    // TODO: this is temporary, until cut-over to reference-counting is done.

    static EARunID _previousEaRunId;
    // Used for determining with the configCache is out of date.


    //**** Instance variables: ****

    PktPartHdr* theHdrPacket;

    RWSortedVector packetList;
    // ordered list containing references to the data packets

    // information from the part header packet

    uint32 configurationID;
    // The id of the configuration used in the observation
    // allows reference to configuration &  part map
  
    uint32 theObsStartTime;
    // actual start time of the observation in MajorTicks

    uint32 partitionNumber;
    // number of this partition w/in the observation (EARun)

    int partitionWords;
    // total number of WORDS (UNPACKED)in the partition
    // from the Words In Partition field of the header packet

    int theApplicationID;
    // application ID of all pkts in the partition

    int ccsdsLower;
    //lower bound of CCSDS sequence numbers;
    //the sequence number of the header packet

    uint32 theHeaderTimeStamp;
    // time stamp of the header packet (cached for reference)

    unsigned char theModeFlags;
    // mode specific flags from the partition header

    double _partStartTime;
    // the time when the partition started collected data

    RWCString _errorMessage;
    // If there has been an error while creating the EdsPartition object,
    // then this instance variable will contain an error message.

    //* Variables below are cache data that are not stored by RW but rather
    //* are computed when needed:

    int measuredLength;
    // the number of words in the partition as measured by counting
    // all the packets in the partition; updated when the partition is
    // not full

    int thePackedSize;
    // cached value of the packed size of the partition given the
    // length

    RcPointer<UShortValVec> _unpackedPartition;
    // cached unpacked partition
};


inline uint32 EdsPartition::hdrPacketTime() const
{
   return theHeaderTimeStamp;
}

inline void EdsPartition::dispose() const
{
    if(disposePartition) 
	disposePartition(this);

}

// if we create a partition without a header we set the 
// partitionWords field to -1;
inline RWBoolean EdsPartition::hasHeader() const
{
    return (theHdrPacket != NULL);
}

inline uint32 EdsPartition::getConfigurationID() const
{
    return configurationID;
} 

inline  int EdsPartition::applicationID () const
{
    return theApplicationID;
}

inline  uint32 EdsPartition::partNumber() const
{
    return partitionNumber;
}

inline int EdsPartition::partitionLength()const
{
    return partitionWords;
}

inline int EdsPartition::hdrSequenceNumber() const
{
    return   ccsdsLower; 
}





inline   uint32 EdsPartition::obsStartTime() const
{
    return theObsStartTime;
}

inline unsigned char EdsPartition::modeFlags() const
{
    return theModeFlags;
}

// Description:
// isFull returns TRUE if the partition has all of the data it is to contain
// False otherwise. The general algorithm is to compute the size of the 
// unpacked data, based upon the realPartitionLength() and the part map, and
// comparing this to the partitionLength()
inline int EdsPartition::isFull() const
{
    // we cache the measured length of the partition and update it each time
    // we add a data packet
    return(measuredLength == packedSize());
 }

#endif
@


5.3
log
@checked in with -k by soccm at 1997/03/11 18:49:32
@
text
@a0 2
//   RCS Stamp: 
// $Id: EdsPartition.h,v 5.3 1996/06/10 19:39:03 nessus Exp $ 
d5 3
d14 1
a14 1
// .LIBRARY libpkts.a
d17 1
a25 4
// .FILE ../src/EdsPartition.C



a36 5

// compat with old rw
#if RWTOOLS < 0x600 && ! defined(RWspace)
#define RWspace unsigned
#endif
@

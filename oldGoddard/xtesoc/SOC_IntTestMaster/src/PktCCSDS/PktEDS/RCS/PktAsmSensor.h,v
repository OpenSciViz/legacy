head	5.1;
access
	soccm;
symbols;
locks;
comment	@ * @;


5.1
date	96.04.06.01.16.04;	author nessus;	state Exp;
branches;
next	;


desc
@Moved to PktEDS
@


5.1
log
@checked in with -k by soccm at 1997/03/11 18:49:32
@
text
@//   RCS Stamp: $Id: PktAsmSensor.h,v 5.1 1996/04/06 01:16:04 nessus Exp $ 
//   Description : Class to define Asm Housekeeping Packets
//   Author      : Steve Berczuk -berczuk@@mit.edu 
//   Copyright   : Massachusetts Institute of Technology
// The Asm Sensor data packet format is defined in the
// Housekeeper Module Specification Doc # 64-30112.0201
// The definition of the housekeeping counters and component IDs is in 
// ASM/EDS ICD MIT Doc # 64-03006

#ifndef PKTASMSENSOR_h
#define PKTASMSENSOR_h
#define PktASMSENSOR_ID 212


#include <XteAngle.h>
#include "PktEds.h"

class ostream;
class PktAsmSensor;


typedef void (*ASMSENSOR_DISP_FUNC)(const PktAsmSensor*);
enum DriveTempSensor {
    // Drive
    GearMotor=4, Ssc1Support, Ssc2Support, UpperBearing,
    LowerBearing, MountingFlange, ControlBPCBoard
};

enum SscTempSensor{    // SSCs
    CollMask=4, CollLeftMiddle, CollLeftRear, CollLeftFront,
    CollRightMiddle, CollRightRear, CollRightFront, RearRadiator,
    TailRadiator, LvpsCase, FrontRadiator,CapsPCBoard };

ostream& operator << (ostream& os, const SscTempSensor s);
ostream& operator << (ostream& os, const DriveTempSensor s);

class PktAsmSensor : public PktEds
{
public:
    enum Device {DRIVE=0, SSC1,SSC2,SSC3};
    enum SscId {Camera1=1, Camera2, Camera3 };
    
    PktAsmSensor(Exemplar);
    // Factory constructor to be used by make();

    PktTlm* make(unsigned char* buffer);
    // return a PktAsmSensor if the buffer matches the criteria for one
 
    uint32 asmHkSeqNumber() const;
    // return the sequence number of this Housekeeping packet
    // this is the number of the housekeeping packet, as distinct
    // from the CCSDS sequence number
  
    void apply() const; 
    // process  the hk info. This will mean dumping a message to a stream

    const unsigned char* applicationData (void) const;

    unsigned short getChannel(Device whichDevice, int channel) const;
    // get the house keeping statistic corresponding to
    // the device and channel - returns the raw value from the packet
    // before shifts and calibrations were applied;

    int getValue(Device whichDevice, int channel) const;
    
    // returns the value in the channel, shifting bits as necessary
    // since data is in the top 12 bits of each 16 bit word. Since
    // values are SIGNED we cast to an int before shifting
    
    // statistics from the Drive Assy
    int driveHomeLimitFlag() const
    {
        return (getChannel(DRIVE,0) & 0x0001);
        
    }
    
    int driveAwayLimitFlag() const
    {
     return   ((getChannel(DRIVE,0) & 0x0002) >> 1);
    }
    
    uint16 rawCoarsePosition() const
    {
        return ((getChannel(DRIVE,0) & 0xFFF0) >> 4);
    }
    
    uint16 rawFinePosition() const
    {
        return ((getChannel(DRIVE,1) & 0xFFF0) >>4);
    }
    
    uint16 rawCoarseBits() const
    {
        return ((getChannel(DRIVE,0) & 0xFFF0) >> 4);
    }
    // return the bits from raw coarse position as an unsigned value

    uint16 rawFineBits() const
    {
        return ((getChannel(DRIVE,1) & 0xFFF0)>>4);
    }
    // return the bits from raw Fine position as an unsigned value
    
    double coarsePosition() const ;
    // return the coarse position statistic from the packet
    // measured in counts
    
    double finePosition() const;
    // return the fine position statistic from the packet

    XteAngle rotAngle() const;
    // return the measured - coarse - rotation angle
    
    
    double drive5vdc() const;
    // return the value of the 5vdc parameter in the drive

    double drive28vdc() const;
    // return the value from the 28vdc s/c bus
    

    // params from SSCs
    
    double sscPos12vdc(SscId ssc) const 
    { 
        return -getValue(sscIdToDevice(ssc),1)/2048.0 * 15.0;
    // if device = DRIVE we have an error
    }

    int sscEventRateIsLatched(SscId s) const
    {
        return (getChannel(sscIdToDevice(s),0) & 0x0008)>>3;
        
    }
    
    int sscSerialNumber(SscId ssc) const;
    

    double sscNeg12vdc(SscId ssc) const
    {
        return -getValue(sscIdToDevice(ssc),2)/2048.0*15.0;
    }
    
    double ssc6vdc(SscId ssc) const
    {
        return -getValue(sscIdToDevice(ssc),3)/2048.0 *9.0;
    }

    double sscHiVoltage(SscId ssc) const
    {
        return(-0.414*getValue(sscIdToDevice(ssc),0) - 1600.0 );
    }
    
    double getDriveTemp(DriveTempSensor sensor) const;
    
    double getSscTemp(SscId ssc,SscTempSensor sensor) const;
    

    void dispose() const;
    // send the packet to the appropriate handler function if one 
    // has been assigned.

    static   ASMSENSOR_DISP_FUNC setDisposition(ASMSENSOR_DISP_FUNC handler);
    // set the handler to be called when an aasm sensor pkt
    // is received

    void dumpHeader (ostream& os) const;

    void dumpData (ostream& os)   const;
    // dump the data and the header to an ostream

protected:
    RWDECLARE_COLLECTABLE(PktAsmSensor)

PktAsmSensor(unsigned char*);
    PktAsmSensor();

private:
    static ASMSENSOR_DISP_FUNC theHandler;
    // handler to be called when we receive a AsmSensor Packet
    double getDeviceTemp (Device d, int sensor) const;
    static Device sscIdToDevice(const SscId s);
    
};


ostream& operator << (ostream& os, const PktAsmSensor::Device d);
// ostream operator for Device enums

#endif
@

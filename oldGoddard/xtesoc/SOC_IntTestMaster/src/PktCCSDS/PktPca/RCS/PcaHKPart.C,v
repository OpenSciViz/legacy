head	1.7;
access
	bfoutz
	rbarnett
	savkoor
	soccm;
symbols
	Build4:1.7
	Build3_3:1.7
	Build3_2:1.6
	Build3_1:1.3
	Build3:1.3;
locks;
comment	@ * @;


1.7
date	94.11.08.22.23.20;	author savkoor;	state Exp;
branches;
next	1.6;

1.6
date	94.10.11.18.08.51;	author rbarnett;	state Exp;
branches;
next	1.5;

1.5
date	94.08.26.15.22.21;	author ramesh;	state Exp;
branches;
next	1.4;

1.4
date	94.07.22.16.43.47;	author bfoutz;	state Exp;
branches;
next	1.3;

1.3
date	94.05.06.20.36.51;	author ramesh;	state Exp;
branches;
next	1.2;

1.2
date	94.05.02.15.52.58;	author ramesh;	state Exp;
branches;
next	1.1;

1.1
date	94.01.13.21.15.04;	author rbarnett;	state Exp;
branches;
next	;


desc
@The PCA Housekeeping partition class
@


1.7
log
@*** empty log message ***
@
text
@// =======================================================================
// File        : PcaHKPart.C
// Subsystem   : PCA housekeeping Data Management
// Programmer  : Hwa-ja Rhee -- Hughes STX
//               Randall D. Barnette -- Hughes STX
// Description : Definition of PCA housekeeping telemetry packet/partition 
//
// RCS stamp
static const char rcsid[]=
"$Id: PcaHKPart.C,v 1.6 1994/10/11 18:08:51 rbarnett Exp $";
//
// =======================================================================

// System headers
#ifdef DEBUG
#include <iostream.h>
#endif /* DEBUG */

// Local headers
#include "PcaHKPart.h"

//
// The following is needed for RogueWave collectables.
//
RWDEFINE_COLLECTABLE(PcaHKPart, PcaHKPart_ID)

// =======================================================================
// PcaHKPart
// =======================================================================

//
// Declare and allocate class variable theHandler.
//
PcaHKPartCallback PcaHKPart::theHandler = 0 ;

//
// RogueWave constructor
// Initialize everything to zero
//
PcaHKPart::PcaHKPart(void) :
  theApid(0), theTime(0), theSubTime(0), theSize(0), theSequence(0), theData(0)
{}

//
// Destructor
//
PcaHKPart::~PcaHKPart(void) { delete theData ; }

//
// Constructor
//
PcaHKPart::PcaHKPart(const PktPca* thePacket) :
  theApid(thePacket->applicationID()),
  theTime(thePacket->seconds()),
  theSubTime(thePacket->subSeconds()),
  theSize(thePacket->dataLength()),
  theSequence(thePacket->sequenceNumber())
{
  theData = new unsigned char[theSize] ;
  memcpy( theData, thePacket->applicationData(), (int) theSize ) ;
}

//
// Copy constructor
//
PcaHKPart::PcaHKPart(const PcaHKPart& aPartition) :
  theApid(aPartition.theApid),
  theTime(aPartition.theTime),
  theSubTime(aPartition.theSubTime),
  theSize(aPartition.theSize),
  theSequence(aPartition.theSequence),
  theData(0) // NOTE!
{
  if( theSize ) {
    theData = new unsigned char[theSize] ;
    memcpy( theData, aPartition.theData, (int) theSize ) ;
  }
}

//
// Assignment operator
//
PcaHKPart& PcaHKPart::operator=(const PcaHKPart& part)
{
  if( this != &part ) {
    theTime = part.theTime ;
    theSubTime = part.theSubTime ;
    theApid = part.theApid ;
    theSize = part.theSize ;
    theSequence = part.theSequence ;
    delete theData ;
    if( theSize && part.theData ) {
      theData = new unsigned char[theSize] ;
      memcpy( theData, part.theData, (int) theSize ) ;
    } else {
      theData = 0 ;
    }
  }
  return *this ;
}

//
// Equality
//
int PcaHKPart::operator==(const PcaHKPart& part) const
{
  //
  // If the time, apid, sequence, and size are the same, then test
  // the packet data in the buffer
  //
  return ((theTime     == part.theTime    ) &&
          (theApid     == part.theApid    ) &&
          (theSequence == part.theSequence) &&
          (theSize     == part.theSize    ) )?

    (theData && part.theData) ?   // if both are non-zero, then compare them.
      memcmp(theData, part.theData, (int) theSize) ?
        0 :                       //   memory compares not equal
        1 :                       //   memory compares equal
      (theData || part.theData) ? // is one of them zero?
        0 :                       //   yes, not equal
        1 :                       //   no, both are zero, and therefore equal
    0 ;                           // apid, time, etc. don't match ;
}

int PcaHKPart::operator!=(const PcaHKPart& part) const
{
  return !(operator==(part)) ;
}

unsigned long PcaHKPart::getValue(int byteOff[],int bitOff[],int nbits[], int npieces) const
{
  if( !theSize ) return 0 ;
  unsigned long val=0;
  for(int i=0; i<npieces; i++) {
    if (nbits[i] <= 8)
      val = (val<<nbits[i]) | getbits(theData[byteOff[i]-14],bitOff[i],nbits[i]);
    else if (nbits[i] <= 16)
      val = (val<<nbits[i]) | getbits(getshort(theData+byteOff[i]-14),bitOff[i],nbits[i]);
    else if (nbits[i] <= 32)
      val = (val<<nbits[i]) | getbits(getint(theData+byteOff[i]-14), bitOff[i], nbits[i]);
  }
  return val;
}

int PcaHKPart::compareTo(const RWCollectable *rhs) const
{
  if( rhs->isA() != PcaHKPart_ID ) return -1 ;
  const PcaHKPart* p = (const PcaHKPart*) rhs;
  unsigned long t1 = time();
  unsigned long t2 = p->time();
  if (t1 == t2) return 0;
  else return t1 > t2 ? 1 : -1;  
}

RWBoolean PcaHKPart::isEqual(const RWCollectable *rhs) const
{
  return compareTo(rhs) ? 0 : 1 ;
}

RWspace PcaHKPart::binaryStoreSize(void) const
{
  return RWCollectable::binaryStoreSize() +
         sizeof( long ) + // packetTime
         sizeof( long ) + // subseconds packet Time 
         sizeof( long ) + // apid, stored as long
         sizeof( long ) + // theSequence, stored as long
         sizeof( long ) + // dataSize, stored as long
         theSize        ; // the data buffer
}

void PcaHKPart::saveGuts(RWFile& f) const
{
  RWCollectable::saveGuts(f) ;
  f.Write( theApid ) ;
  f.Write( theTime ) ;
  f.Write( theSubTime ) ;
  f.Write( theSequence ) ;
  f.Write( theSize ) ;
  if( theSize ) f.Write( theData, (unsigned int) theSize ) ;
  return ;
}

void PcaHKPart::saveGuts(RWvostream& s) const
{
  RWCollectable::saveGuts(s) ;
  s << theApid << theTime << theSubTime << theSequence << theSize ;
  if( theSize ) s.put( theData, (unsigned int) theSize ) ;
  return ;
}

void PcaHKPart::restoreGuts(RWFile& f)
{
  RWCollectable::restoreGuts(f) ;
  f.Read( theApid ) ;
  f.Read( theTime ) ;
  f.Read( theSubTime ) ;
  f.Read( theSequence ) ;
  f.Read( theSize ) ;
  if( theSize ) {
    theData = new unsigned char[theSize] ;
    f.Read( theData, (unsigned int) theSize ) ;
  } else
    theData = 0 ;
  return ;
}

void PcaHKPart::restoreGuts(RWvistream& s)
{
  RWCollectable::restoreGuts(s) ;
  s >> theApid >> theTime >> theSubTime >> theSequence >> theSize ;
  if( theSize ) {
    theData = new unsigned char[theSize] ;
    s.get( theData, (unsigned int) theSize ) ;
  } else
    theData = 0 ;
  return ;
}

@


1.6
log
@update for FITS processing
@
text
@d10 1
a10 1
"$Id: PcaHKPart.C,v 1.5 1994/08/26 15:22:21 ramesh Exp $";
d38 1
d40 3
a42 1
PcaHKPart::PcaHKPart(void) {}
d53 5
a57 5
	theApid(thePacket->applicationID()),
	theTime(thePacket->seconds()),
	theSubTime(thePacket->subSeconds()),
	theSize(thePacket->dataLength()),
	theSequence(thePacket->sequenceNumber())
d67 11
a77 8
	theApid(aPartition.theApid),
	theTime(aPartition.theTime),
	theSubTime(aPartition.theSubTime),
	theSize(aPartition.theSize),
	theSequence(aPartition.theSequence)
{
	theData = new unsigned char[theSize] ;
	memcpy( theData, aPartition.theData, (int) theSize ) ;
d85 15
a99 11
	if( this != &part ) {
		theTime = part.theTime ;
		theSubTime = part.theSubTime ;
		theApid = part.theApid ;
		theSize = part.theSize ;
		theSequence = part.theSequence ;
		delete theData ;
		theData = new unsigned char[theSize] ;
		memcpy( theData, part.theData, (int) theSize ) ;
	}
	return *this ;
d107 17
a123 6
	return ((theTime == part.theTime) &&
	        (theSubTime == part.theSubTime) &&
	        (theApid == part.theApid) &&
	        (theSequence == part.theSequence) &&
	        (theSize == part.theSize) &&
	        (memcmp(theData, part.theData, (int) theSize)==0)) ? 1 : 0 ;
d128 1
a128 1
	return !(operator==(part)) ;
d133 1
d149 5
a153 5
	const PcaHKPart* p = (const PcaHKPart*) rhs;
	unsigned long t1 = time();
	unsigned long t2 = p->time();
	if (t1 == t2) return 0;
	else return t1 > t2 ? 1 : -1;  
d180 1
a180 1
  f.Write( theData, (unsigned int) theSize ) ;
d188 1
a188 1
  s.put( theData, (unsigned int) theSize ) ;
d200 5
a204 6
  theData = new unsigned char[theSize] ;
  f.Read( theData, (unsigned int) theSize ) ;
#ifdef DEBUG
  cerr << "Time = " << theTime << endl
       << "Seq  = " << theSequence << endl ;
#endif /* DEBUG */
d212 5
a216 2
  theData = new unsigned char[theSize] ;
  s.get( theData, (unsigned int) theSize ) ;
d219 1
@


1.5
log
@*** empty log message ***
@
text
@d1 1
d6 1
a6 3
// Description :
//
//   PCA housekeeping telemetry packet/partition 
d9 2
a10 1
static const char rcsid[]="$Id: PcaHKPart.C,v 1.1 1994/01/13 21:15:04 rbarnett Exp $";
d12 1
d53 2
a54 1
	theSize(thePacket->dataLength())
d67 2
a68 1
	theSize(aPartition.theSize)
d84 1
d100 1
d110 29
d141 2
a142 1
  return sizeof( long ) + // packetTime
d145 1
d156 1
d165 1
a165 4
  s << theApid ;
  s << theTime ;
  s << theSubTime ;
  s << theSize ;
d176 1
a180 1
  cerr << "== Reading a partition ==" << endl ;
d182 1
a182 3
       << "subTime = " << theSubTime << endl
       << "apid = " << theApid << endl
       << "data = " << theSize << endl ;
d190 1
a190 4
  s >> theApid ;
  s >> theTime ;
  s >> theSubTime ;
  s >> theSize ;
@


1.4
log
@*** empty log message ***
@
text
@d10 1
a10 1
static const char rcsid[]="$Id: PcaHKPart.C,v 1.3 1994/05/06 20:36:51 ramesh Exp $";
d51 1
d64 1
d78 1
d94 1
a103 3
#if RWTOOLS < 0x600 && ! defined(RWspace)
#define RWspace unsigned
#endif
d105 1
a105 1
RWspace  PcaHKPart::binaryStoreSize(void) const
d108 1
a113 18
int PcaHKPart::compareTo(const RWCollectable* target) const
{
        const PcaHKPart * t = (const PcaHKPart *) target ;
        unsigned long t1 = time() ;
        unsigned long t2 = t->time() ;
        if( t1 == t2 ) return 0 ;
        return t1 > t2 ? 1 : -1 ;
      }

RWBoolean PcaHKPart::isEqual(const RWCollectable* target) const
{
        const PcaHKPart * t = (const PcaHKPart *) target ;
        if (apid() == t->apid())
           if (time() == t->time()) return 1;

        return 0;
      }

d119 1
d130 1
d141 1
d148 1
d160 1
@


1.3
log
@*** empty log message ***
@
text
@d10 1
a10 1
static const char rcsid[]="$Id: PcaHKPart.C,v 1.1 1994/01/13 21:15:04 rbarnett Exp $";
d100 3
d104 1
a104 1
unsigned PcaHKPart::binaryStoreSize(void) const
d112 18
@


1.2
log
@*** empty log message ***
@
text
@a4 1
//							 Ramesh Ponneganti - Hughes STX
d10 1
a10 1
static const char rcsid[]="$Id: PcaHKPart.C,v 1.1 1994/01/13 21:15:04 ramesh Exp $";
@


1.1
log
@Initial revision
@
text
@d5 1
d11 1
a11 1
static const char rcsid[]="$Id$";
@

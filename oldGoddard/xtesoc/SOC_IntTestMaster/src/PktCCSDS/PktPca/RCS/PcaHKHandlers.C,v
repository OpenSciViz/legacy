head	1.10;
access
	rbarnett
	savkoor
	soccm;
symbols
	Build4_5_3:1.9
	Build4:1.8
	Build3_3:1.8
	Build3_2:1.8
	Build3_1:1.7
	Build3:1.6;
locks;
comment	@ * @;


1.10
date	96.04.18.16.37.23;	author soccm;	state Exp;
branches;
next	1.9;

1.9
date	95.11.01.17.31.45;	author rbarnett;	state Exp;
branches;
next	1.8;

1.8
date	94.09.15.22.55.01;	author rbarnett;	state Exp;
branches;
next	1.7;

1.7
date	94.06.30.14.06.41;	author ramesh;	state Exp;
branches;
next	1.6;

1.6
date	94.05.18.20.56.13;	author rbarnett;	state Exp;
branches;
next	1.5;

1.5
date	94.05.18.20.01.49;	author rbarnett;	state Exp;
branches;
next	1.4;

1.4
date	94.05.18.19.52.04;	author rbarnett;	state Exp;
branches;
next	1.3;

1.3
date	94.05.18.19.43.04;	author rbarnett;	state Exp;
branches;
next	1.2;

1.2
date	94.05.18.19.30.51;	author rbarnett;	state Exp;
branches;
next	1.1;

1.1
date	94.05.18.18.56.49;	author rbarnett;	state Exp;
branches;
next	;


desc
@Instrument Team supplied Packet handler callback functions
@


1.10
log
@changed SOC_REPORT -> SOC_INFO
@
text
@// ===========================================================================
// File Name   : PcaHKHandlers.C           
// Subsystem   : PCA Housekeeping Data Management subsystem
// Programmer  : Hwa-Ja Rhee, Hughes STX (NASA/GSFC code 664)
//               Randall D. Barnette, Hughes STX (NASA/GSFC code 664)
// Description : Implementation of the PCA DM storage
//
// RCS:
static const char rcsid[]=
"$Id: PcaHKHandlers.C,v 1.9 1996/02/14 19:43:26 soccm Exp $";
//
// ===========================================================================

// System Headers
#include <stdlib.h>
#include <string.h>

// Rogue Wave Headers
#include <rw/sortvec.h>

// SOC Headers
#include <Externs.h>
#include <Utilities.h>
#include <DataSet.h>
#include <SOCMessage.h>

// Local Headers
#include "PcaHKHandlers.h"
#include "AccPcaHK.h"
 
//
// PCA HK packets arrive every 8 seconds (per apid) and are added to a list.
// EIGHT_SEC_PACKETS is the number of packets stored in a list before a dataset
// is written. The values is currently 75 but any value is possible.
// 75 packets/DataSet X 8 sec/packet = 600 sec/DataSet
// or 10 minutes per DataSet (per apid).
// Also, 142 bytes per packet X 75 packets = 10,650 bytes stored (per apid)
//
// For other values remember that larger numbers translate to fewer disk
// writes but more memory required (142 bytes/packet/apid) and usually
// better performance.
// Smaller numbers give smaller DataSets more often.
//
const int EIGHT_SEC_PACKETS = 75 ; // 8 sec/pkt X 75 pkt/DS = 600 sec/DS

//
// How many seconds to add to the time of the last packet
// in a DataSet. This guarantees that the stop time of one
// packet is one second before the start time of the next.
//
const int EIGHT_SEC_PAD = 7 ;

//
// Maintain five lists of packets; one for each PCU
// These are decalared externally so that both the handler
// and the cleanup routine can manipulate them.
//
static RWSortedVector pcu1List ;
static RWSortedVector pcu2List ;
static RWSortedVector pcu3List ;
static RWSortedVector pcu4List ;
static RWSortedVector pcu5List ;

//
// This function takes each partition (actually a reworked packet) and
// lists the apid and time. I can be made to print other things.
//
void listPcaHK(const PcaHKPart* thePart)
{
	cout << thePart->apid() << ": " << thePart->time() << endl ;
}

//
// This function actually creates the DataSet and writes it out.
//
static void writeDataSet(RWSortedVector& theList, int timePad, int count=0)
{
  int entries = theList.entries() ;

  //
  // Is the list empty?
  //
  if( entries == 0 ) {
#ifdef DEBUG
    SOCThrow(SOC_DEBUG,"Skipping PcaHK list with no entries.\n") ;
#endif
    return ;
  }

  //
  // If the count is zero then try to write everything in the list.
  //
  if( count > entries ) count = entries ;

  //
  // Get the first and last items in the list.
  //
  const Accessor* first = (const Accessor*) (theList[0]) ;
  const Accessor* last  = (const Accessor*) (theList[count-1] ) ;

  //
  // Create a DataSet
  //
  DataSet theDS(
    DataSet::generateName(first->apid(),first->time(),last->time()+timePad)) ;

  //
  // Add each item in the list to the new DataSet
  //
  for( int i=0 ; i < count ; i++ ) {
    if( theDS.addValue( (const DataValue*) (theList[0]) ) )
      SOCThrow(SOC_DEBUG,"Unsuccessful Insert to Value Array\n") ;
    theList.remove(theList[0]) ;
  }

  //
  // Write it out
  //
  theDS.archive(1) ;

  //
  // Report
  //
  SOCThrow(SOC_INFO,"%s <PCU HK>", theDS.name()) ;

  //
  // When theDS goes out of scope, the memory for the Accessors themselves
  // will be released.
  //
  return ;
}


//
// cleanUpPcaHK is called when the dmIndex process must shut down
// for any reason. It writes out any accessor lists to DataSets in
// the same manner as a normal write.
//
void cleanUpPcaHK(void)
{
  while( pcu1List.entries() )
	  writeDataSet( pcu1List, EIGHT_SEC_PAD, EIGHT_SEC_PACKETS ) ;
  while( pcu2List.entries() )
	  writeDataSet( pcu2List, EIGHT_SEC_PAD, EIGHT_SEC_PACKETS ) ;
  while( pcu3List.entries() )
	  writeDataSet( pcu3List, EIGHT_SEC_PAD, EIGHT_SEC_PACKETS ) ;
  while( pcu4List.entries() )
	  writeDataSet( pcu4List, EIGHT_SEC_PAD, EIGHT_SEC_PACKETS ) ;
  while( pcu5List.entries() )
	  writeDataSet( pcu5List, EIGHT_SEC_PAD, EIGHT_SEC_PACKETS ) ;

  AccPcaHK::destroyDescriptors() ;
	return ;
}

//
// This is the actual Data Management handler. It builds lists of partitions
// and writes them to DataSets at the appropriate time.
//
void handlePcaHK(const PcaHKPart* thePart)
{

	RWSortedVector* theList ;

	//
	// Get the right list to append to
	//
	switch( thePart->apid() ) {
	case 90 : theList = &pcu1List ; break ;
	case 91 : theList = &pcu2List ; break ;
	case 92 : theList = &pcu3List ; break ;
	case 93 : theList = &pcu4List ; break ;
	case 94 : theList = &pcu5List ; break ;
	}

	//
	// Declare an Accessor with reference to the packet
	// Add the accessor to the list
  // This assumes AccPcaHK has a valid compareTo function.
	//
	theList->insert(new AccPcaHK(*thePart)) ;

  //
  // Is it time to create a dataSet?
  //
  if( theList->entries() >= 2*EIGHT_SEC_PACKETS )
		writeDataSet( *theList, EIGHT_SEC_PAD, EIGHT_SEC_PACKETS ) ;

	return ;
}
@


1.9
log
@Better algorithm for cleanup
@
text
@d10 1
a10 1
"$Id: PcaHKHandlers.C,v 1.8 1994/09/15 22:55:01 rbarnett Exp $";
d112 1
a112 1
      SOCThrow(SOC_REPORT,"Unsuccessful Insert to Value Array\n") ;
d124 1
a124 1
  SOCThrow(SOC_REPORT,"%s <PCU HK>", theDS.name()) ;
@


1.8
log
@Added cleaUp routine
@
text
@d10 1
a10 1
"$Id: PcaHKHandlers.C,v 1.7 1994/06/30 14:06:41 ramesh Exp $";
d74 1
a74 1
// This function actuall creates the DataSet and writes it out.
d78 1
d83 1
a83 1
  if( theList.entries() == 0 ) {
d93 1
a93 1
  if( !count ) count = theList.entries() ;
d124 1
a124 1
  SOCThrow(SOC_REPORT,"%s\n", theDS.name()) ;
d141 11
a151 5
	writeDataSet( pcu1List, EIGHT_SEC_PAD ) ;
	writeDataSet( pcu2List, EIGHT_SEC_PAD ) ;
	writeDataSet( pcu3List, EIGHT_SEC_PAD ) ;
	writeDataSet( pcu4List, EIGHT_SEC_PAD ) ;
	writeDataSet( pcu5List, EIGHT_SEC_PAD ) ;
d186 2
a187 11
  if( theList->entries() >= EIGHT_SEC_PACKETS ) {

		// Get the sequence number of the first packet; increment it
    unsigned long seq = ((const AccPcaHK*)((*theList)[0]))->sequence()+1 ;

		// Check to see if the packets are in sequence; read this carefully!
    for(int i=1;(i<EIGHT_SEC_PACKETS)&&(seq==((const AccPcaHK*)((*theList)[i]))->sequence());seq++,i++);

    // If we got thru, write something out
		if( i == EIGHT_SEC_PACKETS ) writeDataSet( *theList, EIGHT_SEC_PAD, EIGHT_SEC_PACKETS ) ;
  }
@


1.7
log
@*** empty log message ***
@
text
@d1 2
a2 1
// Program Name: PcaHKHandlers.C           
d6 1
a6 1
// Description : implementation of the PCA DM storage
d10 1
a10 1
"$Id: PcaHKHandlers.C,v 1.1 1994/01/13 21:15:04 rbarnett Exp $";
d12 1
d14 1
d18 2
a19 1
#include <rw/ordcltn.h>
d21 8
a28 2
#include "Externs.h"
#include "Utilities.h"
a29 1
#include "DataSet.h"
d33 1
a33 1
// MAX_PACKETS is the number of packets stored in a list before a dataset
d44 19
a62 1
#define MAX_PACKETS 75
d74 76
a154 8
	//
	// Maintain five lists of packets
	//
	static RWOrdered theList90 ;
	static RWOrdered theList91 ;
	static RWOrdered theList92 ;
	static RWOrdered theList93 ;
	static RWOrdered theList94 ;
d156 1
a156 1
	RWOrdered * theList ;
d162 5
a166 5
	case 90 : theList = &theList90 ; break ;
	case 91 : theList = &theList91 ; break ;
	case 92 : theList = &theList92 ; break ;
	case 93 : theList = &theList93 ; break ;
	case 94 : theList = &theList94 ; break ;
a170 4
	//
	AccPcaHK* theAcc = new AccPcaHK(*thePart) ;

	//
d172 1
d174 1
a174 1
	theList->append(theAcc) ;
d176 4
a179 4
	//
	// Is it time to create a dataSet?
	//
	if( MAX_PACKETS == theList->entries() ) {
d181 2
a182 2
		Accessor* first = (Accessor*) (theList->first()) ;
		Accessor* last  = (Accessor*) (theList->last() ) ;
d184 2
a185 54
		//
		// Create the DataSet name
		//
		const char* name =
			DataSet::generateName( first->apid(), first->time(), last->time() + 7) ;

		//
		// Verify that the directory exists
		//
		char* s = strcpy( new char[strlen(name)+1], name ) ;
		char* lastSlash ;

		if( lastSlash = strrchr( s, '/' ) ) {
			*lastSlash = 0 ;
			if( !create_directory(s,0775) )
				cerr << "Failed to create directory " << s << "." << endl ;
		}

		delete s ;

		//
		// Create a DataSet
		//
		DataSet theDS(name);

		//
		// Add each item in the list to the new DataSet 
		//
		for( int i=0 ; i<MAX_PACKETS ; i++ )
			if( theDS.addValue( (DataValue*) (*theList)[i] ) )
				cerr << "Unsuccessful Insert to Value Array" << endl ;

		//
		// Only print if apid == 90
		//
		if( theList == &theList90 ) cerr << theDS.name() << endl ;

		//
		// Write the DataSet to a file
		//
		if( RWFile::Exists(theDS.name())) {
			RWFile tf( theDS.name() ) ;
			tf.Erase() ;
		}

		RWFile f( theDS.name() ) ;
		theDS.recursiveSaveOn(f) ;

		//
		// Destroy the list and release the memory
		// When theDS goes out of scope, the memory for the Accessors themselves
		// will be released.
		//
		theList->clear() ;
d187 3
a189 1
	}
@


1.6
log
@fixed check of MAX_PACKETS
@
text
@a0 1
// =======================================================================
d3 2
a4 1
// Programmer  : Randall D. Barnette, Hughes STX (NASA/GSFC code 664)
d9 1
a9 1
"$Id: PcaHKHandlers.C,v 1.5 1994/05/18 20:01:49 rbarnett Exp $";
a10 1
// =======================================================================
d12 2
a13 1
#include <rw/sortvec.h>
d15 2
d35 1
a35 94
static const int MAX_PACKETS = 75 ;

//
// The time pad is 7 seconds. This value is used to make the
// end of one DataSet be one second less than the beginning of the
// next. This value is added to the time stamp of the last packet
// in the DataSet.
//
static const int TIME_PAD = 7 ;

//
// Maintain five lists of packets; one for each PCU or apid.
// These are external so that cleanUpPcaHK can dump them.
//
static RWSortedVector theList90 ;
static RWSortedVector theList91 ;
static RWSortedVector theList92 ;
static RWSortedVector theList93 ;
static RWSortedVector theList94 ;

// =======================================================================
//
// Write a list of accessors to a DataSet. This function is used
// to create DataSets in normal and error conditions.
//
// =======================================================================
static void writeDataSet(RWSortedVector* theList, int count=0)
{

  //
  // Is the list empty?
  //
  if( theList->entries() == 0 ) {
#ifdef DEBUG
    cerr << "Skipping list with no entries." << endl ;
#endif
    return ;
  }

  //
  // If the count is zero then try to write everything in the list.
  //
  if( !count ) count = theList->entries() ;

	Accessor* first = (Accessor*) (theList->first()) ;
	Accessor* last  = (Accessor*) (theList->last() ) ;

	//
	// Create a DataSet
	//
	DataSet theDS(
		DataSet::generateName(first->apid(),first->time(),last->time()+TIME_PAD)) ;

	//
	// Add each item in the list to the new DataSet 
	//
	while( count-- ) {
		if(theDS.addValue((DataValue*)((*theList)[0])))
			cerr << "Unsuccessful Insert to Value Array" << endl ;
		theList->remove((*theList)[0]) ;
	}

	//
	// Write the DataSet to a file.
	// When theDS goes out of scope, the memory for the Accessors themselves
	// will be released.
	//
	theDS.archive(1) ;

	//
	// print name
	//
	cerr << theDS.name() << endl ;

	return ;
}

// =======================================================================
//
// cleanUpPCAHK is called when the dmIndex process must shut down
// for any reason. It writes out any accessor lists to DataSets in
// the same manner as a normal write.
//
// =======================================================================
void cleanUpPcaHK(void)
{
  writeDataSet(&theList90) ;
  writeDataSet(&theList91) ;
  writeDataSet(&theList92) ;
  writeDataSet(&theList93) ;
  writeDataSet(&theList94) ;
  AccPcaHK::destroyDescriptors() ;
  return ;
}
a36 1
// =======================================================================
d39 1
a39 1
// lists the apid and time. It can be made to print other things.
a40 1
// =======================================================================
a45 1
// =======================================================================
a49 1
// =======================================================================
d52 8
d61 1
a61 1
	RWSortedVector * theList ;
d76 4
d82 1
a82 1
	theList->insert(new AccPcaHK(*thePart) ) ;
d87 1
a87 2
	if( MAX_PACKETS <= theList->entries() )
		writeDataSet(theList, MAX_PACKETS ) ;
d89 60
a150 1

@


1.5
log
@changed ->at(0) to [0]
@
text
@d9 1
a9 1
"$Id: PcaHKHandlers.C,v 1.4 1994/05/18 19:52:04 rbarnett Exp rbarnett $";
d170 1
a170 1
	if( MAX_PACKETS >= theList->entries() )
@


1.4
log
@reworked writeDataSet
@
text
@d9 1
a9 1
"$Id: PcaHKHandlers.C,v 1.3 1994/05/18 19:43:04 rbarnett Exp $";
d90 1
a90 1
		if(theDS.addValue((DataValue*)(theList->at(0))))
d92 1
a92 1
		theList->remove(theList->at(0)) ;
@


1.3
log
@fixed writeDataSet
@
text
@d9 1
a9 1
"$Id: PcaHKHandlers.C,v 1.2 1994/05/18 19:30:51 rbarnett Exp $";
d47 5
a51 5
static RWOrdered theList90 ;
static RWOrdered theList91 ;
static RWOrdered theList92 ;
static RWOrdered theList93 ;
static RWOrdered theList94 ;
a54 29
// cleanUpPCAHK is called when the dmIndex process must shut down
// for any reason. It writes out any accessor lists to DataSets in
// the same manner as a normal write.
//
// =======================================================================
void cleanUpPcaHK(void)
{
  writeDataSet(theList90) ;
  writeDataSet(theList91) ;
  writeDataSet(theList92) ;
  writeDataSet(theList93) ;
  writeDataSet(theList94) ;
  AccPcaHK::destroyDescriptors() ;
  return ;
}

// =======================================================================
//
// This function takes each partition (actually a reworked packet) and
// lists the apid and time. It can be made to print other things.
//
// =======================================================================
void listPcaHK(const PcaHKPart* thePart)
{
	cout << thePart->apid() << ": " << thePart->time() << endl ;
}

// =======================================================================
//
d59 1
a59 1
static void writeDataSet(const RWSortedVector* theList, int count=0)
d90 1
a90 1
		if( theDS.addValue( (DataValue*) (theList->at(0)) )
d92 1
a92 1
		thList->remove(theList->at(0)) ;
d109 29
@


1.2
log
@added cleanUp routine
@
text
@d9 1
a9 1
"$Id: PcaHKHandlers.C,v 1.1 1994/05/18 18:56:49 rbarnett Exp rbarnett $";
d171 1
a171 1
		writeDS(theList, MAX_PACKETS ) ;
@


1.1
log
@Initial revision
@
text
@d1 1
d4 1
a4 2
// Programmer  : Hwa-Ja Rhee, Hughes STX (NASA/GSFC code 664)
//               Randall D. Barnette, Hughes STX (NASA/GSFC code 664)
d9 1
a9 1
"$Id: PcaHKHandlers.C,v 1.1 1994/01/13 21:15:04 rbarnett Exp socops $";
d11 1
d13 1
a13 2
#include <stdlib.h>
#include <string.h>
a14 2
#include <rw/ordcltn.h>

d33 37
a69 1
#define MAX_PACKETS 75
d71 1
d74 1
a74 1
// lists the apid and time. I can be made to print other things.
d76 1
d82 1
d84 2
a85 1
// Maintain five lists of packets
d87 51
a137 5
static RWOrdered theList90 ;
static RWOrdered theList91 ;
static RWOrdered theList92 ;
static RWOrdered theList93 ;
static RWOrdered theList94 ;
d139 1
d144 1
d148 1
a148 1
	RWOrdered * theList ;
a162 4
	//
	AccPcaHK* theAcc = new AccPcaHK(*thePart) ;

	//
d165 1
a165 1
	theList->append(theAcc) ;
d170 2
a171 59
	if( MAX_PACKETS == theList->entries() ) {

		Accessor* first = (Accessor*) (theList->first()) ;
		Accessor* last  = (Accessor*) (theList->last() ) ;

		//
		// Create the DataSet name
		//
		const char* name =
			DataSet::generateName( first->apid(), first->time(), last->time() + 7) ;

		//
		// Verify that the directory exists
		//
		char* s = strcpy( new char[strlen(name)+1], name ) ;
		char* lastSlash ;

		if( lastSlash = strrchr( s, '/' ) ) {
			*lastSlash = 0 ;
			if( !create_directory(s,0775) )
				cerr << "Failed to create directory " << s << "." << endl ;
		}

		delete s ;

		//
		// Create a DataSet
		//
		DataSet theDS(name);

		//
		// Add each item in the list to the new DataSet 
		//
		for( int i=0 ; i<MAX_PACKETS ; i++ )
			if( theDS.addValue( (DataValue*) (*theList)[i] ) )
				cerr << "Unsuccessful Insert to Value Array" << endl ;

		//
		// Only print if apid == 90
		//
		if( theList == &theList90 ) cerr << theDS.name() << endl ;

		//
		// Write the DataSet to a file
		//
		if( RWFile::Exists(theDS.name())) {
			RWFile tf( theDS.name() ) ;
			tf.Erase() ;
		}

		RWFile f( theDS.name() ) ;
		theDS.recursiveSaveOn(f) ;

		//
		// Destroy the list and release the memory
		// When theDS goes out of scope, the memory for the Accessors themselves
		// will be released.
		//
		theList->clear() ;
a172 2
	}

d175 1
@

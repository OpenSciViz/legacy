head	5.1;
access
	soccm;
symbols
	Build5_0_3:5.1
	Berczuk_final:3.2
	CodeFreeze_5_95:3.2
	Build4_3:3.2
	Build4_2b:3.2
	Build4_2a:3.2
	Build4_2:3.2
	Build4_1:3.2
	Build4:3.2
	Build3_3:3.2
	Build3_2:3.2
	Build3_1:3.1
	Current:5.1
	Build3:3.1
	Build2:2.5;
locks; strict;
comment	@ * @;


5.1
date	96.04.06.01.15.59;	author nessus;	state Exp;
branches;
next	3.2;

3.2
date	94.07.13.17.48.25;	author berczuk;	state Exp;
branches;
next	3.1;

3.1
date	94.04.29.16.34.28;	author berczuk;	state Exp;
branches;
next	2.7;

2.7
date	94.03.14.18.30.58;	author berczuk;	state Exp;
branches;
next	2.6;

2.6
date	93.12.10.19.19.30;	author berczuk;	state Exp;
branches;
next	2.5;

2.5
date	93.11.24.22.42.02;	author berczuk;	state Release;
branches;
next	2.4;

2.4
date	93.10.26.21.01.16;	author berczuk;	state Exp;
branches;
next	2.3;

2.3
date	93.10.21.19.28.11;	author berczuk;	state Exp;
branches;
next	2.2;

2.2
date	93.10.08.17.46.46;	author berczuk;	state Exp;
branches;
next	2.1;

2.1
date	93.10.07.18.48.42;	author berczuk;	state Exp;
branches;
next	1.1;

1.1
date	93.10.05.14.57.01;	author berczuk;	state Exp;
branches;
next	;


desc
@Observation Parameter Dump
@


5.1
log
@Bump version number
@
text
@//   RCS Stamp: $Id: ObsParamDump.h,v 3.2 1994/07/13 17:48:25 berczuk Exp nessus $ 
//   Description : The contents of the Obsservation Parameter dump of an
//   Author      : Steve Berczuk -berczuk@@mit.edu
//   Copyright   : Massachusetts Institute of Technology

// .NAME ObsParamDump - a list of ObsComps
// .LIBRARY Ingest
// .HEADER Data Ingest Functions
// .INCLUDE ObsParamDump.h
// .FILE ../src/ObsParamDump.C
// .SECTION Author
// Steve Berczuk, berczuk, MIT, berczuk@@mit.edu

// .SECTION Description
// The ObsParamDump keeps track of all packets downlinked in the observation
// parameter dump. The ObsParamDump can be used with configuration classes
// to create a measured config

#ifndef OBSPARAMDUMP_h
#define OBSPARAMDUMP_h

#define OBSPARAMDUMP_ID 216

#include <rw/tpordvec.h>
#include <rw/collect.h>
#include "science_if.h"
#include <mit_td.h>
#include <EARunID.h>
#include "ObsComp.h"

class ObsParamDump;
typedef void (*OBS_PDUMP_FUNC)(const ObsParamDump*);
//  function to be called when the obs param dump is completed

// The ObsParamDump class holds a list of obs components
class ObsParamDump : public RWCollectable
{
public:
  ObsParamDump();
  // Default Constructor

  ~ObsParamDump();
  // destructor
  
  void insert(ObsComp* theComp);
  // insert an obs component

  RWBoolean isComplete() const;
  // Returns TRUE if the param dump is complete
  // FALSE otherwise
  
  ObsComp* getComponent(e_obs_comp type) const;
  // get the desired component
  // returnd NULL if the desired component is not in list
  // or is not complete

  unsigned numPackets() const;
  // return the number of packets in the obsParamDump

  UINT32 configurationID() const;
  // return the configudation ID from the obs param dump

  EARunID eaRunID() const;
  // return the ea run ID

  int applicationID() const;
  // return the application id of the packets in this dump;
  // this will be used to determine which EA this dump came from
  
  int operator ==(const ObsComp&)  {return 1;};
  int operator <(const ObsComp&)  {return 1;};
  // required to use the template class
  // do I need < ?
  
  void dispose();
  // call handler if it exists

  static OBS_PDUMP_FUNC setDisposition(OBS_PDUMP_FUNC);
  // set the function to be called when an obsParamDump is full
    RWspace binaryStoreSize() const;
    int compareTo(const RWCollectable* x) const;
    RWBoolean isEqual(const RWCollectable* x) const;
    unsigned hash() const;
    void restoreGuts(RWFile& f);
    void restoreGuts (RWvistream& s);
    void saveGuts (RWFile& f) const;
    void saveGuts (RWvostream& s) const;
    
  RWDECLARE_COLLECTABLE(ObsParamDump)
private:
  RWTPtrOrderedVector<ObsComp> obsComps;
  static OBS_PDUMP_FUNC disposeObsDump;
  // function to be called when an ObsParam Dump is full
};

inline void ObsParamDump::dispose()
{
    if(disposeObsDump)
	disposeObsDump(this);
    

}

#endif










@


3.2
log
@made persistent
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: ObsParamDump.h,v 3.1 1994/04/29 16:34:28 berczuk Exp berczuk $ 
@


3.1
log
@Build3 Initial Release
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: ObsParamDump.h,v 2.7 1994/03/14 18:30:58 berczuk Exp berczuk $ 
d22 1
d25 1
d36 1
a36 1
class ObsParamDump
d80 10
a89 1
  
@


2.7
log
@made changes per code review Jan & Feb '94
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: ObsParamDump.h,v 2.6 1993/12/10 19:19:30 berczuk Exp berczuk $ 
@


2.6
log
@added desclration of descructor
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: ObsParamDump.h,v 2.5 1993/11/24 22:42:02 berczuk Release berczuk $ 
d6 1
a6 1
// .NAME EARun - A class to define Event Analyser runs
d9 4
a23 1
//#include <rw/ordcltn.h>
d25 2
a26 1

d38 1
a38 2
  // Null Constructor

a41 1

d58 1
a58 1
  unsigned long configurationID() const;
d61 3
d73 3
d79 1
a79 2
protected:
  //RWOrdered obsComps;
d85 7
a91 2
//typedef RWTPtrOrderedVector<ObsComp> RWTPtrOrderedVector_ObsComp;
// this typedef tells the compiler in 3.0.1 to create the class
@


2.5
log
@added applicationID method
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: ObsParamDump.h,v 2.4 1993/10/26 21:01:16 berczuk Exp berczuk $ 
d36 5
@


2.4
log
@renamed setXXdisposition() methods to setDisposition()
Moved the dispose functions to the self class
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: ObsParamDump.h,v 2.3 1993/10/21 19:28:11 berczuk Exp berczuk $ 
d51 7
@


2.3
log
@made the following methods const: isComplete() numPackets()
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: ObsParamDump.h,v 2.2 1993/10/08 17:46:46 berczuk Exp berczuk $ 
d56 1
a56 1
  static OBS_PDUMP_FUNC setObsDumpDisposition(OBS_PDUMP_FUNC);
@


2.2
log
@made getComponent(e_obs_comp type) method const
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: ObsParamDump.h,v 2.1 1993/10/07 18:48:42 berczuk Exp berczuk $ 
d39 1
a39 1
  RWBoolean isComplete();
d48 1
a48 1
  unsigned numPackets();
@


2.1
log
@changed the type of template container that holds templates..
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: ObsParamDump.h,v 1.1 1993/10/05 14:57:01 berczuk Exp berczuk $ 
d43 1
a43 1
  ObsComp* getComponent(e_obs_comp type);
d66 1
a66 1
typedef RWTPtrOrderedVector<ObsComp> RWTPtrOrderedVector_ObsComp;
@


1.1
log
@Initial revision
@
text
@d1 1
a1 1
//   RCS Stamp: $Id$ 
d17 4
d22 1
a22 1
#include <rw/tpslist.h>
d25 4
d48 11
d60 4
a63 1
  RWTPtrSlist<ObsComp> obsComps;
d65 4
d70 9
@

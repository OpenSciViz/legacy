head	5.1;
access
	soccm;
symbols
	Build5_0_3:5.1
	Berczuk_final:3.6
	CodeFreeze_5_95:3.6
	Build4_3:3.5
	Build4_2b:3.5
	Build4_2a:3.5
	Build4_2:3.5
	Build4_1:3.5
	Build4:3.5
	Build3_3:3.5
	Build3_2:3.3
	Build3_1:3.2
	Current:5.1
	Build3:3.1
	Build2:2.8;
locks; strict;
comment	@ * @;


5.1
date	96.04.06.01.16.37;	author nessus;	state Exp;
branches;
next	3.6;

3.6
date	95.04.27.14.24.42;	author berczuk;	state Exp;
branches;
next	3.5;

3.5
date	94.09.23.16.29.19;	author berczuk;	state Exp;
branches;
next	3.4;

3.4
date	94.08.15.14.44.13;	author berczuk;	state Exp;
branches;
next	3.3;

3.3
date	94.08.12.16.37.54;	author berczuk;	state Exp;
branches;
next	3.2;

3.2
date	94.05.06.14.30.36;	author berczuk;	state Exp;
branches;
next	3.1;

3.1
date	94.04.29.16.34.38;	author berczuk;	state Exp;
branches;
next	2.8;

2.8
date	93.11.03.17.08.58;	author berczuk;	state Release;
branches;
next	2.7;

2.7
date	93.10.08.17.37.40;	author berczuk;	state Exp;
branches;
next	2.6;

2.6
date	93.07.19.20.27.47;	author berczuk;	state Exp;
branches;
next	2.5;

2.5
date	93.07.06.15.59.05;	author berczuk;	state Exp;
branches;
next	2.4;

2.4
date	93.05.20.15.52.26;	author berczuk;	state Exp;
branches;
next	2.3;

2.3
date	93.05.12.17.40.20;	author berczuk;	state Exp;
branches;
next	2.2;

2.2
date	93.05.11.22.40.48;	author berczuk;	state Exp;
branches;
next	2.1;

2.1
date	93.05.11.15.25.30;	author berczuk;	state Exp;
branches;
next	1.4;

1.4
date	93.03.24.22.25.23;	author berczuk;	state Exp;
branches;
next	1.3;

1.3
date	93.02.12.22.44.11;	author berczuk;	state Exp;
branches;
next	1.2;

1.2
date	93.01.21.22.07.23;	author berczuk;	state Exp;
branches;
next	1.1;

1.1
date	93.01.06.22.46.40;	author berczuk;	state Exp;
branches;
next	;


desc
@Class to describe an ObsCoda Packet
@


5.1
log
@Bump version number
@
text
@//   RCS Stamp: $Id: PktObsCoda.h,v 3.6 1995/04/27 14:24:42 berczuk Exp nessus $ 
//   LastEdit : "Thu Jan 21 17:06:20 1993"
//   Description : class to describe the Observation Coda Packet
//   Author      : Steve Berczuk, berczuk@@mit.edu
//   Copyright   : Massachusetts Institute of Technology

#ifndef PKTOBSCODA_H
#define PKTOBSCODA_H
#define PktOBSCODA_ID 201
#include <mit_td.h>
#include "PktEds.h"
class PktObsCoda;

typedef void (*CODA_DISP_FUNC)(const PktObsCoda*);
class PktObsCoda:public PktEds
{
public:
  PktObsCoda(Exemplar){}
  PktTlm* make(unsigned char* buffer);
 ~PktObsCoda (void);  
  // Delete a packet;
  
  virtual void apply () const;
  // process the packet for data ingest

    uint32 configurationID() const;
  // return the configuration id from the packet data buffer

   int obsStartTime() const;
  // return the the actual observation Start Time in Major ticks 

   int partitionCount() const;
  // return the number of data partitions generated

   int typerErrorCount() const;
  // return the number of discrepancies between the encouded and actual
  // typer table

   int typerTableOffset() const;
  // return the offset which contained the first discovered discrepancy

   int typerTableValue() const;
  // return the value that was read at the typer table offset that did not 
  // match

   int recoderErrorCount() const;
  // retrun the total number of disrepancies between the encoded
  // recoder table and the actual recoder table after the observation

   int recoderTableOffset() const;
  // return the offset which contained the first discovered
  // discrepancy

   int recoderTableValue() const;
  // identifies tht value which was read at the recoder table offser
  // which did not match the encoded table
  
   int partitionMapErrors() const;
  // returns 0 if the partition map was intact at the end of the observation
  // 0 else (maybe this should be an enum)

    unsigned char obsErrorFlags() const;
    
    unsigned char modeFlags() const;
    
    void dispose() const;

    static CODA_DISP_FUNC setDisposition(CODA_DISP_FUNC);

//  const unsigned char* applicationData (void) const; 
  // Get ptr to application data--after header;
  // -- this varies from packet class to packet class
  // for this class this points to the end of the secondary header
protected:
  RWDECLARE_COLLECTABLE(PktObsCoda)
  enum {NominalPacketLength=(SecondaryHdrSize+EDSHdrSize -1 +26)};
  // This constant defines the nominal size of this type of packet
  // in a way such that it can be compared with the length field of
  // the CCSDS header (hence the absense of primary header size and 
  // presence of the -1)
  // ObsCoda packets have 26 bytes of data.

  PktObsCoda();
  PktObsCoda(unsigned char* buffer);
  void addPktToEaRun(EARun* theRun) const;
private:
    static CODA_DISP_FUNC disposeObsCoda;
};
  inline  uint32 PktObsCoda::configurationID() const
{
  return(
	 buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+2]<<24 |
	 buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+3]<<16|
	 buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize]<<8 |
	 buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+1]
	 );
}

inline   int PktObsCoda::obsStartTime() const
{
  return(
	 buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+6]<<24 |
	 buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+7]<<16|
	 buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+4]<<8 |
	 buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+5]
	 );

}


  inline  int PktObsCoda::partitionCount() const
{
    return(
	 buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+10]<<24 |
	 buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+11]<<16|
	 buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+8]<<8 |
	 buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+9]
	 );

}
inline   int PktObsCoda::typerErrorCount() const
{
  return(buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+12]<<8 |
	 buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+13]
	  );
	
}

  inline  int PktObsCoda::typerTableOffset() const
{
  return(buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+14]<<8 |
	 buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+15]
	 );
	
}
inline  int PktObsCoda::typerTableValue() const
{
  return(
	 buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+16]<<8 |
	 buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+17]
	  );
	
}

inline  int PktObsCoda::recoderErrorCount() const
{
return(
       buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+18]<<8 |
       buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+19]
       );
	
}

inline  int PktObsCoda::recoderTableOffset() const
{
  return(
	 buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+20]<<8 |
	 buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+21]
	 );
	
}

inline  int PktObsCoda::recoderTableValue() const
{
  return(
	 buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+22]<<8 |
	 buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+23]
	  );
	
}

  
inline  int PktObsCoda::partitionMapErrors() const
{
  return(buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+25] );
	
}

inline  unsigned char PktObsCoda::obsErrorFlags() const
{
      return(buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+25] );
    
}
inline  unsigned char PktObsCoda::modeFlags() const
{
      return(buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+26] );
    
}

inline void PktObsCoda::dispose() const
{
    if(disposeObsCoda)
        disposeObsCoda(this);
}
inline CODA_DISP_FUNC PktObsCoda::setDisposition(CODA_DISP_FUNC f)
{
    CODA_DISP_FUNC ohandler = disposeObsCoda;
    disposeObsCoda = f;
    return ohandler;
}


#endif

@


3.6
log
@added modeFlags() function.
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: PktObsCoda.h,v 3.5 1994/09/23 16:29:19 berczuk Exp berczuk $ 
@


3.5
log
@added handler facilities
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: PktObsCoda.h,v 3.4 1994/08/15 14:44:13 berczuk Exp berczuk $ 
d62 4
a65 2
     unsigned char obsErrorFlags() const;
     
d184 6
@


3.4
log
@changed const int/char return values to simply int/char
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: PktObsCoda.h,v 3.3 1994/08/12 16:37:54 berczuk Exp berczuk $ 
d12 1
d14 1
d63 2
a64 1
    
d66 2
d84 2
a85 1

d182 12
@


3.3
log
@added ObsErrorFlags method
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: PktObsCoda.h,v 3.2 1994/05/06 14:30:36 berczuk Exp berczuk $ 
d27 1
a27 1
  const int obsStartTime() const;
d30 1
a30 1
  const int partitionCount() const;
d33 1
a33 1
  const int typerErrorCount() const;
d37 1
a37 1
  const int typerTableOffset() const;
d40 1
a40 1
  const int typerTableValue() const;
d44 1
a44 1
  const int recoderErrorCount() const;
d48 1
a48 1
  const int recoderTableOffset() const;
d52 1
a52 1
  const int recoderTableValue() const;
d56 1
a56 1
  const int partitionMapErrors() const;
d60 1
a60 1
    const unsigned char obsErrorFlags() const;
d91 1
a91 1
inline  const int PktObsCoda::obsStartTime() const
d103 1
a103 1
  inline const int PktObsCoda::partitionCount() const
d113 1
a113 1
inline  const int PktObsCoda::typerErrorCount() const
d121 1
a121 1
  inline const int PktObsCoda::typerTableOffset() const
d128 1
a128 1
inline const int PktObsCoda::typerTableValue() const
d137 1
a137 1
  inline const int PktObsCoda::recoderErrorCount() const
d146 1
a146 1
  inline const int PktObsCoda::recoderTableOffset() const
d155 1
a155 1
  inline const int PktObsCoda::recoderTableValue() const
d165 1
a165 1
  inline const int PktObsCoda::partitionMapErrors() const
d171 1
a171 1
inline const unsigned char PktObsCoda::obsErrorFlags() const
@


3.2
log
@ConfigId now returns a uint 32
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: PktObsCoda.h,v 3.1 1994/04/29 16:34:38 berczuk Exp berczuk $ 
d60 3
d171 5
@


3.1
log
@Build3 Initial Release
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: PktObsCoda.h,v 2.8 1993/11/03 17:08:58 berczuk Release berczuk $ 
d10 1
d24 1
a24 1
  const int configurationID() const;
d78 1
a78 1
  inline const int PktObsCoda::configurationID() const
@


2.8
log
@made apply() method on packets const; made changes consistent with
a const apply() ; added destructors
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: PktObsCoda.h,v 2.7 1993/10/08 17:37:40 berczuk Exp berczuk $ 
@


2.7
log
@Fixed inline functions to read partitionCount and StartTime

(words were flipped)
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: PktObsCoda.h,v 2.6 1993/07/19 20:27:47 berczuk Exp berczuk $ 
d20 1
a20 1
  virtual void apply (void);
d74 1
a74 1
  void addPktToEaRun(EARun* theRun);
@


2.6
log
@SOC BUILD 2 BASELINE
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: PktObsCoda.h,v 2.5 1993/07/06 15:59:05 berczuk Exp berczuk $ 
d80 4
a83 4
	 buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize]<<24 |
	 buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+1]<<16|
	 buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+2]<<8 |
	 buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+3]
d90 4
a93 4
	 buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+4]<<24 |
	 buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+5]<<16|
	 buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+6]<<8 |
	 buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+7]
d102 4
a105 4
	 buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+8]<<24 |
	 buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+9]<<16|
	 buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+10]<<8 |
	 buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+11]
@


2.5
log
@Added methods and definitions for Rogue Wave Persistence
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: PktObsCoda.h,v 2.4 1993/05/20 15:52:26 berczuk Exp berczuk $ 
@


2.4
log
@Made changes to compy with changed to PktTlm class
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: PktObsCoda.h,v 2.3 1993/05/12 17:40:20 berczuk Exp berczuk $ 
d9 1
a9 1

d64 1
d72 1
@


2.3
log
@added NominalPacketLength constant to hold "correct" value of packetLength()
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: PktObsCoda.h,v 2.2 1993/05/11 22:40:48 berczuk Exp berczuk $ 
d16 1
a16 1
  PktCCSDS* make(unsigned char* buffer);
@


2.2
log
@added methods to add obscoda to EARun
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: PktObsCoda.h,v 2.1 1993/05/11 15:25:30 berczuk Exp berczuk $ 
d64 7
@


2.1
log
@New Baseline
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: PktObsCoda.h,v 1.4 1993/03/24 22:25:23 berczuk Exp berczuk $ 
d65 1
a65 1
  
@


1.4
log
@removed definition/declaration of application data for the moment
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: PktObsCoda.h,v 1.3 1993/02/12 22:44:11 berczuk Exp berczuk $ 
@


1.3
log
@added stub of apply() method
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: PktObsCoda.h,v 1.2 1993/01/21 22:07:23 berczuk Exp berczuk $ 
d59 1
a59 1
  const unsigned char* applicationData (void) const; 
@


1.2
log
@Chnaged PartMapErrors accessor function to fix byte ordering
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: PktObsCoda.h,v 1.1 1993/01/06 22:46:40 berczuk Exp berczuk $ 
d7 2
a8 2
#ifndef PKTOBSCODA_h
#define PKTOBSCODA_h
d15 1
a15 1

d20 3
d68 1
a68 1
  const int PktObsCoda::configurationID() const
d78 1
a78 1
  const int PktObsCoda::obsStartTime() const
d90 1
a90 1
  const int PktObsCoda::partitionCount() const
d100 1
a100 1
  const int PktObsCoda::typerErrorCount() const
d108 1
a108 1
  const int PktObsCoda::typerTableOffset() const
d115 1
a115 1
const int PktObsCoda::typerTableValue() const
d124 1
a124 1
  const int PktObsCoda::recoderErrorCount() const
d133 1
a133 1
  const int PktObsCoda::recoderTableOffset() const
d142 1
a142 1
  const int PktObsCoda::recoderTableValue() const
d152 1
a152 1
  const int PktObsCoda::partitionMapErrors() const
@


1.1
log
@Initial revision
@
text
@d1 2
a2 2
//   RCS Stamp: $Id$ 
//   LastEdit : "Wed Jan  6 17:25:15 1993"
d151 1
a151 1
return(buf[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize+24] );
@

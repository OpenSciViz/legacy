head	5.1;
access
	soccm;
symbols
	Build5_0_3:5.1
	Berczuk_final:3.1
	CodeFreeze_5_95:3.1
	Build4_3:3.1
	Build4_2b:3.1
	Build4_2a:3.1
	Build4_2:3.1
	Build4_1:3.1
	Build4:3.1
	Build3_3:3.1
	Build3_2:3.1
	Build3_1:3.1
	Current:5.1
	Build3:3.1
	Build2:2.6
	Build1:2.3;
locks; strict;
comment	@// @;


5.1
date	96.04.06.01.13.00;	author nessus;	state Exp;
branches;
next	3.1;

3.1
date	94.04.29.16.35.08;	author berczuk;	state Exp;
branches;
next	2.8;

2.8
date	94.03.21.19.57.25;	author berczuk;	state Exp;
branches;
next	2.7;

2.7
date	94.03.14.18.32.19;	author berczuk;	state Exp;
branches;
next	2.6;

2.6
date	93.11.03.17.07.48;	author berczuk;	state Release;
branches;
next	2.5;

2.5
date	93.10.07.21.05.21;	author berczuk;	state Exp;
branches;
next	2.4;

2.4
date	93.07.19.20.05.36;	author berczuk;	state Exp;
branches;
next	2.3;

2.3
date	93.07.06.15.57.50;	author berczuk;	state Exp;
branches;
next	2.2;

2.2
date	93.05.20.15.51.30;	author berczuk;	state Exp;
branches;
next	2.1;

2.1
date	93.05.11.15.24.49;	author berczuk;	state Exp;
branches;
next	1.15;

1.15
date	93.05.07.20.19.06;	author berczuk;	state Exp;
branches;
next	1.14;

1.14
date	93.05.07.19.57.24;	author berczuk;	state Exp;
branches;
next	1.13;

1.13
date	93.05.07.19.18.14;	author berczuk;	state Exp;
branches;
next	1.12;

1.12
date	93.05.07.15.16.43;	author berczuk;	state Exp;
branches;
next	1.11;

1.11
date	93.05.05.15.15.13;	author berczuk;	state Exp;
branches;
next	1.10;

1.10
date	93.03.24.22.22.34;	author berczuk;	state Exp;
branches;
next	1.9;

1.9
date	93.02.25.21.40.03;	author berczuk;	state Exp;
branches;
next	1.8;

1.8
date	93.02.25.20.12.15;	author berczuk;	state Exp;
branches;
next	1.7;

1.7
date	93.02.10.18.02.06;	author berczuk;	state Exp;
branches;
next	1.6;

1.6
date	93.02.03.21.18.37;	author berczuk;	state Exp;
branches;
next	1.5;

1.5
date	93.01.21.21.26.01;	author berczuk;	state Exp;
branches;
next	1.4;

1.4
date	93.01.19.20.51.54;	author berczuk;	state Exp;
branches;
next	1.3;

1.3
date	93.01.07.20.24.39;	author berczuk;	state Exp;
branches;
next	1.2;

1.2
date	93.01.07.15.26.57;	author berczuk;	state Exp;
branches;
next	1.1;

1.1
date	93.01.06.20.42.12;	author berczuk;	state Exp;
branches;
next	;


desc
@class describes all packets with stream id PKT_OBS_PARAM
@


5.1
log
@Bump version number
@
text
@//   RCS Stamp: $Id: PktObsParam.C,v 3.1 1994/04/29 16:35:08 berczuk Exp nessus $ 
//   Description : Class to model an Observation Component packet
//   Author      : steve berczuk: berczuk@@mit.edu
//   Copyright   : Massachusetts Institute of Technology

static const char rcsid[] ="$Id: PktObsParam.C,v 3.1 1994/04/29 16:35:08 berczuk Exp nessus $";

#include <iostream.h>
#include "PktObsCompHdr.h"
#include <EARun.h>
#include "fs_if.h"
// flight software constants

#include "PktObsParam.h"

RWDEFINE_COLLECTABLE(PktObsParam,PktOBSPARAM_ID)

static PktObsCompHdr obsCompHdrMaker(Exemplar());

PktObsParam::PktObsParam(unsigned char* buffer)
:PktEds(buffer)
{

  theCompTag = obsCompTag(buffer);

}

// null constructor
PktObsParam::  PktObsParam()
{
}


PktObsParam::~PktObsParam()
{


}

PktTlm* PktObsParam::make(unsigned char* buffer)
{
      PktTlm* retvalue;
    if (PktEds::streamID(buffer) == STREAM_ID_OBS_PARAM)
      {
	if(!(retvalue= obsCompHdrMaker.make(buffer)))
	  retvalue = new PktObsParam(buffer);
	// default is a PktObsParam, which is the same as a
	// ObsCompData Packet;
      }
    else
        retvalue =0;
    return retvalue;

}

// Description:
// apply adds the packet to the EARun- delegating to the EARun the decision
// of how to handle the packet.
void PktObsParam::apply() const 
{
  addToEaRun();
}

 int PktObsParam::dataLength()const
{
  return(packetLength()+1-SecondaryHdrSize-EDSHdrSize-2);
  // CCSDS length +1 = total length of packet in bytes
  // per CCSDS spec
  // total length-headers = data from this packet
}

int PktObsParam::compareTo(const RWCollectable* x) const
{
    PktObsParam* y = (PktObsParam*)x;   
    int myCompSequence = componentSequence();
    
    if(myCompSequence < y->componentSequence())
	return -1;
    else if (myCompSequence > y->componentSequence())
	return 1;
    else 
	return 0;
    
    }


void PktObsParam::dumpHeader(ostream& os) const

{
  os << "PktObsCompData; Comp Seq Number: " <<componentSequence()<<"\n";
  os << "  Component Tag "<< obsCompTag() << "\n";
}


void PktObsParam::dumpData(ostream& os) const
{
  os << "Dumping Data for PktObsParam\n";
}

e_obs_comp PktObsParam::obsCompTag (const unsigned char* buffer)
{
    int compTagVal =
	(e_obs_comp)buffer[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize];

    e_obs_comp retVal; // variable to hold the enum returned

    switch(compTagVal)     // convert the int value to a tag
    {
    case OBS_COMP_HEADER:
	retVal =  OBS_COMP_HEADER;
	break;
    case OBS_COMP_MAIN_DSP:
	retVal= OBS_COMP_MAIN_DSP;
	break;
    case OBS_COMP_AUX_DSP:
	retVal = OBS_COMP_AUX_DSP;
	break;
    case OBS_COMP_PARTMAP:
	retVal = OBS_COMP_PARTMAP;
	break;
    case OBS_COMP_PCA:
	retVal = OBS_COMP_PCA;
	break;
    case  OBS_COMP_TYPER:
	retVal =  OBS_COMP_TYPER;
	break;
    case OBS_COMP_RECODER:
	retVal = OBS_COMP_RECODER;
	break;
    case OBS_COMP_REC_CORR:
	retVal = OBS_COMP_REC_CORR;
	break;
    case OBS_COMP_REC_GROUP:
	retVal = OBS_COMP_REC_GROUP;
	break;
    case OBS_COMP_REC_POS:
	retVal = OBS_COMP_REC_POS;
	break;
    case OBS_COMP_BINNED:
	retVal =  OBS_COMP_BINNED;
	break;
    case OBS_COMP_TRIGGER:
	retVal = OBS_COMP_TRIGGER;
	break;
    case OBS_COMP_PULSAR:
	retVal = OBS_COMP_PULSAR;
	break;
    case  OBS_COMP_EVENT:
	retVal =  OBS_COMP_EVENT;
	break;
    case OBS_COMP_SINGLEBIT:
	retVal = OBS_COMP_SINGLEBIT;
	break;
    case OBS_COMP_CATCHER:
	retVal = OBS_COMP_CATCHER;
	break;
    case OBS_COMP_FFT:
	retVal = OBS_COMP_FFT;
	break;
    case OBS_COMP_POSITION:
	retVal = OBS_COMP_POSITION;
	break;
    case OBS_COMP_TIMESER:
	retVal = OBS_COMP_TIMESER;
	break;
    case OBS_COMP_ASMEVENT:
	retVal = OBS_COMP_ASMEVENT;
	break;
    default:
	cerr << __FILE__ <<" " << __LINE__ <<" Invalid Obs Comp Tag: "
	    << compTagVal << "\n";
	
    };	
	
    return(retVal);
	
}


 void PktObsParam::addPktToEaRun(EARun* theRun)const
{ theRun->addPacket(this);}





@


3.1
log
@Build3 Initial Release
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: PktObsParam.C,v 2.8 1994/03/21 19:57:25 berczuk Exp berczuk $ 
d6 1
a6 1
static const char rcsid[] ="$Id: PktObsParam.C,v 2.8 1994/03/21 19:57:25 berczuk Exp berczuk $";
@


2.8
log
@enhanced "duplicate component" message to add id of dup comp
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: PktObsParam.C,v 2.7 1994/03/14 18:32:19 berczuk Exp berczuk $ 
d6 1
a6 1
static const char rcsid[] ="$Id: PktObsParam.C,v 2.7 1994/03/14 18:32:19 berczuk Exp berczuk $";
@


2.7
log
@made changes per code reviews Jan & Feb 1994
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: PktObsParam.C,v 2.6 1993/11/03 17:07:48 berczuk Release berczuk $ 
d6 1
a6 1
static const char rcsid[] ="$Id: PktObsParam.C,v 2.6 1993/11/03 17:07:48 berczuk Release berczuk $";
d102 3
a104 1
    int compTagInt =buffer[PrimaryHdrSize+SecondaryHdrSize+EDSHdrSize];
d107 1
a107 1
    switch(compTagInt)     // convert the int value to a tag
d111 1
d114 1
d117 1
d120 1
a120 1

d123 1
d126 1
d129 1
d132 1
d135 1
d138 1
a138 1

d141 1
d144 1
a144 1

d147 1
d150 1
d153 1
d156 1
d159 1
a159 1

d162 1
d165 1
d168 1
d170 2
a171 1
	cerr << __FILE__ <<" " << __LINE__ <<" Invalid Obs Comp Tag\n";
@


2.6
log
@made apply() method const; made changes consistant with this
and added destructors
@
text
@d1 1
a1 2
//   RCS Stamp: $Id: PktObsParam.C,v 2.5 1993/10/07 21:05:21 berczuk Exp berczuk $ 
//   LastEdit : "Wed Feb 10 13:01:11 1993"
d6 1
a6 1
static const char rcsid[] ="$Id: PktObsParam.C,v 2.5 1993/10/07 21:05:21 berczuk Exp berczuk $";
d56 3
a58 7
/**
const unsigned char* PktObsParam:: applicationData()const
{
 
}
***/
// this method needs to fleshed out...
a60 3
  
// insert this packet into the appropriate EA run, in a manner similar to
// the PktEDS partition data/header packets.
d72 15
d98 1
d100 4
d105 53
a158 1

@


2.5
log
@changed declaration of rcsid to const char* to fix compiler warning
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: PktObsParam.C,v 2.4 1993/07/19 20:05:36 berczuk Exp berczuk $ 
d7 1
a7 1
static const char rcsid[] ="$Id: PktObsParam.C,v 2.4 1993/07/19 20:05:36 berczuk Exp berczuk $";
d64 1
a64 1
void PktObsParam::apply() 
d97 2
a98 1
 void PktObsParam::addPktToEaRun(EARun* theRun) { theRun->addPacket(this);}
@


2.4
log
@SOC BUILD 2 BASELINE
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: PktObsParam.C,v 2.3 1993/07/06 15:57:50 berczuk Exp berczuk $ 
d7 1
a7 1
static char rcsid[] ="$Id: PktObsParam.C,v 2.3 1993/07/06 15:57:50 berczuk Exp berczuk $";
@


2.3
log
@Added methods &c for rogue wave persistence
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: PktObsParam.C,v 2.2 1993/05/20 15:51:30 berczuk Exp berczuk $ 
d7 1
a7 1
static char rcsid[] ="$Id: PktObsParam.C,v 2.2 1993/05/20 15:51:30 berczuk Exp berczuk $";
@


2.2
log
@made changes to comply with changed to PktTlm class
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: PktObsParam.C,v 2.1 1993/05/11 15:24:49 berczuk Exp berczuk $ 
d7 1
a7 1
static char rcsid[] ="$Id: PktObsParam.C,v 2.1 1993/05/11 15:24:49 berczuk Exp berczuk $";
d16 2
@


2.1
log
@New Baseline
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: PktObsParam.C,v 1.15 1993/05/07 20:19:06 berczuk Exp berczuk $ 
d7 1
a7 1
static char rcsid[] ="$Id: PktObsParam.C,v 1.15 1993/05/07 20:19:06 berczuk Exp berczuk $";
d39 1
a39 1
PktCCSDS* PktObsParam::make(unsigned char* buffer)
d41 1
a41 1
      PktCCSDS* retvalue;
@


1.15
log
@cleaned up compile warnings
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: PktObsParam.C,v 1.14 1993/05/07 19:57:24 berczuk Exp berczuk $ 
d7 1
a7 1
static char rcsid[] ="$Id: PktObsParam.C,v 1.14 1993/05/07 19:57:24 berczuk Exp berczuk $";
@


1.14
log
@Fixed dataLength() method
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: PktObsParam.C,v 1.13 1993/05/07 19:18:14 berczuk Exp berczuk $ 
d7 1
a7 1
static char rcsid[] ="$Id: PktObsParam.C,v 1.13 1993/05/07 19:18:14 berczuk Exp berczuk $";
d25 5
@


1.13
log
@Simplified interface to EARun
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: PktObsParam.C,v 1.12 1993/05/07 15:16:43 berczuk Exp berczuk $ 
d7 1
a7 1
static char rcsid[] ="$Id: PktObsParam.C,v 1.12 1993/05/07 15:16:43 berczuk Exp berczuk $";
d67 4
a70 2
  return(packetLength()-PrimaryHdrSize-SecondaryHdrSize-EDSHdrSize-2);
  // CCSDS length - headers - header info from this packet
@


1.12
log
@fleshed out class to work with EARun
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: PktObsParam.C,v 1.11 1993/05/05 15:15:13 berczuk Exp berczuk $ 
d7 1
a7 1
static char rcsid[] ="$Id: PktObsParam.C,v 1.11 1993/05/05 15:15:13 berczuk Exp berczuk $";
d11 1
a11 1

d62 1
a62 1
  addToEaRun(this);
d88 1
a88 1

@


1.11
log
@Restructured classes so that PktObsParam replaces PktObsCmpData
since this class has the common behaviour of comp data and comp hdr.
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: PktObsParam.C,v 1.10 1993/03/24 22:22:34 berczuk Exp berczuk $ 
d7 1
a7 1
static char rcsid[] ="$Id: PktObsParam.C,v 1.10 1993/03/24 22:22:34 berczuk Exp berczuk $";
d59 1
a59 1
  cerr << "    Received an ObsParam Packet\n";
d62 2
d65 4
d74 2
a75 2
  os << "PktObsCompData; Comp Seq Number: " <<sequenceNumber()<<"\n";
  os << "Component Tag "<< obsCompTag() << "\n";
@


1.10
log
@removed declaration/Definition of applicationData() since it was null at the moment
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: PktObsParam.C,v 1.9 1993/02/25 21:40:03 berczuk Exp berczuk $ 
d7 1
a7 1
static char rcsid[] ="$Id: PktObsParam.C,v 1.9 1993/02/25 21:40:03 berczuk Exp berczuk $";
a10 1
#include "PktObsCmpData.h"
a16 1
static PktObsCompData obsCompDataMaker(Exemplar());
d23 1
a24 1

d40 3
a42 2
	  if(!(retvalue= obsCompDataMaker.make(buffer)))
	    retvalue = new PktObsParam(buffer);
d60 3
d64 1
d68 2
a69 2
  os << "Dumping Header for PktObsParam\n";

@


1.9
log
@added rcsid[] =$Id to source files
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: PktObsParam.C,v 1.8 1993/02/25 20:12:15 berczuk Exp berczuk $ 
d7 1
a7 1
static char rcsid[] ="$Id$";
d51 1
d54 1
a54 1

d56 1
a56 1

@


1.8
log
@cleaned up the "Processing a packet ObsParam" message in apply();
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: PktObsParam.C,v 1.7 1993/02/10 18:02:06 berczuk Exp berczuk $ 
d6 2
@


1.7
log
@changed dest for packet is applied from cout to cerr
@
text
@d1 1
a1 1
//   RCS Stamp: $Id: PktObsParam.C,v 1.6 1993/02/03 21:18:37 berczuk Exp berczuk $ 
d54 1
d57 1
a57 1
  cerr << "processing and ObsParam Packet\n";
@


1.6
log
@changed include of PktPbsCompData to PktObsCmpData
@
text
@d1 2
a2 2
//   RCS Stamp: $Id: PktObsParam.C,v 1.5 1993/01/21 21:26:01 berczuk Exp berczuk $ 
//   LastEdit : "Wed Feb  3 16:18:25 1993"
d56 1
a56 1
  cout << "processing and ObsParam Packet\n";
@


1.5
log
@Addeda test body to the apply() method
@
text
@d1 2
a2 2
//   RCS Stamp: $Id: PktObsParam.C,v 1.4 1993/01/19 20:51:54 berczuk Exp berczuk $ 
//   LastEdit : "Thu Jan 21 16:18:59 1993"
d9 1
a9 1
#include "PktObsCompData.h"
@


1.4
log
@added constants from fs_if.h to replace hardcoded stream ids
@
text
@d1 2
a2 2
//   RCS Stamp: $Id: PktObsParam.C,v 1.3 1993/01/07 20:24:39 berczuk Exp berczuk $ 
//   LastEdit : "Tue Jan 19 13:42:35 1993"
d54 4
@


1.3
log
@added appplicationData() method
@
text
@d1 2
a2 2
//   RCS Stamp: $Id: PktObsParam.C,v 1.2 1993/01/07 15:26:57 berczuk Exp berczuk $ 
//   LastEdit : "Thu Jan  7 12:46:06 1993"
d11 3
d37 1
a37 1
    if (PktEds::streamID(buffer) == 5)
@


1.2
log
@changed make() method so that it tries to make() subclasses
@
text
@d1 2
a2 2
//   RCS Stamp: $Id: PktObsParam.C,v 1.1 1993/01/06 20:42:12 berczuk Exp berczuk $ 
//   LastEdit : "Thu Jan  7 10:15:26 1993"
d46 4
@


1.1
log
@Initial revision
@
text
@d1 2
a2 2
//   RCS Stamp: $Id$ 
//   LastEdit : "Wed Jan  6 14:02:35 1993"
d8 3
d13 2
d35 5
a39 1
        retvalue = new PktObsParam(buffer);
@

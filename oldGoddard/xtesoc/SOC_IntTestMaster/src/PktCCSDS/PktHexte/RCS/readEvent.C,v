head	3.1;
access
	rbarnett
	mmarlowe
	rbentley
	dhon
	ghuszar
	kmurphy
	soccm
	tgasaway;
symbols
	Build4_1:3.1
	Build4:3.1
	Build3_3:3.1
	Current:3.1
	Build3_2:3.1
	Build3_1:3.1
	Build3:3.1
	Build2:2.1;
locks;
comment	@ * @;


3.1
date	94.05.09.00.41.52;	author rbentley;	state Exp;
branches;
next	2.2;

2.2
date	94.01.28.21.52.10;	author rbarnett;	state Exp;
branches;
next	2.1;

2.1
date	93.11.10.20.58.37;	author rbarnett;	state Release;
branches;
next	;


desc
@Creation
@


3.1
log
@Build 3
@
text
@// Program Name: hkAccess.C
// Subsystem   : Data Management Test
// Programmer  : Randall D. Barnette -- Hughes STX
// Description:
//
//  XFF Data Management test for APID 80 (0x50)
//
// RCS:
static const char rcsid[]="$Id: readEvent.C,v 2.2 1994/01/28 21:52:10 rbarnett Exp rbentley $";

//  Feature test switches

// System headers
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memory.h>
#include <unistd.h>

#include <iostream.h>
#include <iomanip.h>
#include <rw/rwfile.h>

// Local headers
#include "DataSet.h"
#include "AccHxSc.h"
#include "AccHxSc1.h"
#include "HxScPart.h"
#include "IDF.h"
#include "AccHxSc2.h"
#include "AccHxSc3.h"
#include "AccHxSc4.h"
#include "AccHxSc6.h"

#include <rw/tpordvec.h>

#include "CluArray.h"
#include "ObsComp.h"
#include "PktSwHouse.h"

static CluArray<int> foo1 ;
static CluArray<char> foo2 ;
static RWTPtrOrderedVector<ObsComp> foo3 ;
static RWTPtrOrderedVector<PktSwHouse> foo4 ;

// Macros

// File scope variables
const char EXIT    = 'E' ;
const char CONFIG  = 'C' ;
const char DESCRIP = 'D' ;
const char NEXTROW = 'R' ;
const char PRINT   = 'P' ;

struct Mode1Event {
  unsigned channel ;
  unsigned timef ;
  unsigned le0, le1 ;
  unsigned detector ;
  unsigned mfc ;
  unsigned cal ;
  unsigned riseTime ;
  unsigned csi ;
  unsigned vs0,vs1,vs2,vs3,vs4 ;
  unsigned uld ;
  unsigned pls ;
  unsigned spare ;
} ;

void printMode1EventHdr()
{
  cerr
  << setw(5) << "chan"
  << setw(8) << "timef"
  << setw(4) << "le0"
  << setw(4) << "le1"
  << setw(4) << "det"
  << setw(4) << "mfc"
  << setw(4) << "cal"
  << setw(5) << "rise"
  << setw(4) << "csi"
  << setw(4) << "vs0"
  << setw(4) << "vs1"
  << setw(4) << "vs2"
  << setw(4) << "vs3"
  << setw(4) << "vs4"
  << setw(4) << "uld"
  << setw(4) << "pls"
  << setw(6) << "spare"
  << setw(15) << "time" << endl ;
}

const double CLOCK_DIVS = 1.0 / ((double)(1<<17)) ;

void printMode1Event( Mode1Event* m, unsigned long packetTime )
{
  double rtime = packetTime + m->mfc + m->timef*CLOCK_DIVS ;
  cout
  << setw(5) << m->channel
  << setw(8) << m->timef
  << setw(4) << m->le0
  << setw(4) << m->le1
  << setw(4) << m->detector
  << setw(4) << m->mfc
  << setw(4) << m->cal
  << setw(5) << m->riseTime
  << setw(4) << m->csi
  << setw(4) << m->vs0
  << setw(4) << m->vs1
  << setw(4) << m->vs2
  << setw(4) << m->vs3
  << setw(4) << m->vs4
  << setw(4) << m->uld
  << setw(4) << m->pls
  << setw(6) << m->spare
  << setprecision(12) << setw(15) << rtime << endl ;
}

Mode1Event * decodeMode1Event(unsigned char * buf )
{
  Mode1Event* m = new Mode1Event ;

  m->channel  = buf[0] ;
  m->timef    = buf[1] | buf[2]<<8 | (buf[3]&0x01) << 16 ;
  m->le0      = buf[3] & 0x02 ? 1 : 0 ;
  m->le1      = buf[3] & 0x04 ? 1 : 0 ;
  m->detector = (buf[3] & 0x18) >> 3 ;
  m->mfc      = (buf[3] & 0xe0) >> 5 | (buf[4]&0x01) << 3 ;
  m->cal      = buf[4] & 0x02 ? 1 : 0 ;
  m->riseTime = (buf[4] & 0xfc) >> 2 ;
  m->csi      = buf[5] & 0x01 ? 1 : 0 ;
  m->vs0      = buf[5] & 0x02 ? 1 : 0 ;
  m->vs1      = buf[5] & 0x04 ? 1 : 0 ;
  m->vs2      = buf[5] & 0x08 ? 1 : 0 ;
  m->vs3      = buf[5] & 0x10 ? 1 : 0 ;
  m->vs4      = buf[5] & 0x20 ? 1 : 0 ;
  m->uld      = buf[5] & 0x40 ? 1 : 0 ;
  m->pls      = buf[5] & 0x80 ? 1 : 0 ;
  m->spare    = buf[6] ;
  return m ;
}

static Descriptor ** theDescriptorList = 0 ;
static int theDescriptorCount = 0 ;

static void addDesc( Descriptor * aDesc )
{
  static const int s = sizeof(Descriptor*) ;

  theDescriptorList = theDescriptorCount ?
    (Descriptor**) realloc( theDescriptorList, s*(theDescriptorCount+1) ) :
    (Descriptor**) malloc(s) ;

  theDescriptorList[theDescriptorCount++] = aDesc ;
  return ;
}

static void clearDescList(void)
{
  while( theDescriptorCount ) delete theDescriptorList[--theDescriptorCount] ;
  delete theDescriptorList ;
  theDescriptorList=0 ;
  theDescriptorCount=0 ;
  return ;
}

#define MAXLINE 4096

// External variables

// External functions

// Structures and unions

static struct RowData {
  int     length ;
  unsigned char* data ;
} rowData ;

inline void setRDbuf( int len, const char * buf )
{
  if( rowData.data ) free(rowData.data) ;
  rowData.length = len ;
  rowData.data = (unsigned char *) malloc( len ) ;
  memcpy( rowData.data, buf, len ) ;
}

inline void addToRDbuf( int len, const char * buf )
{
  if( !rowData.length ) setRDbuf( len, buf ) ;
  else {
    rowData.data=(unsigned char *) realloc(rowData.data,len+rowData.length ) ;
    memcpy( rowData.data+rowData.length, buf, len ) ;
    rowData.length += len ;
  }
}

inline void clearRDbuf() { rowData.length = 0 ; }

int write_bytes(FILE* fp, const unsigned char* buf, int nbytes)
{
    int nwritten;
    int nleft = nbytes;
    int fd = fileno(fp) ;
 
    while (nleft > 0) {
        if((nwritten = write( fd, (char*) buf, nleft )) <= 0) break;
        nleft -= nwritten;
        buf   += nwritten;
    }
    return nbytes - nleft;
}

inline void writeRD(FILE* fp)
{
  write( fileno(fp), (char *) &(rowData.length), sizeof(int));
  write_bytes( fp, rowData.data, rowData.length ) ;
}

// Signal catching functions

// Functions

// Main

int main( int argc, char** argv )
{
  int appid;
  int error = 0;
  long id = 0;
  int totalRows ;
  int i, j ;
  double tbeg;
  double tstart = 0.0;
  double tstop =  0.0;
  char ddescr[MAXLINE];
  int currentRow;
  char command ;

//
// Don't delete this!
//
  Accessor acc ;
  AccHxSc aP1 ;
  AccHxSc1 aPA ;
  DataSet aDA ;
  Descriptor aDr ;
  DataValue aDv ;
  DataType aDt ;
	HxIDFHeader idf ;
	HxScPart aPart ;

  DataSet * theDS = 0 ;
  Descriptor * theDesc = 0 ;
  const RWOrdered* theVals ;
  DataValue  * aVal ;

  static Descriptor dmode( "DataMode" ) ;
  static Descriptor ptime( "PacketTime" ) ;
  static Descriptor fitsTime( "Time" ) ;

// The Input syntax:
//
// command '\n'
//
// command :                 response:
//   'C' appid begtime         dataMode, dsStart, dsStop, itemCount
//   'D' descriptor            'F' or 'T'
//   'R'                       RowData
//   'P'                       prints row of data
//   'E'
//
// appid      : int
// begtime    : double
// descriptor : char[]
//

  //
  // Change these if you want something other that stdin/stdout
  //
  FILE* inpipe  = fdopen( STDIN_FILENO, "r" ) ;
  FILE* outpipe = fdopen( STDOUT_FILENO, "w" ) ;

  const char * filename ;

  char inputLine[1024] ;
  //
  // Loop until exit command
  //
  do {

    //
    // Read a command
    //
    cin >> command ;

    switch( command ) {

    // ===================================================================
    case CONFIG :                  // Asking for a ConfigId - new Data Set
    // ===================================================================

      //
      // Read AppID and begin time
      //
      cin >> appid >> tbeg ;

      //
      // This server serves apid 80,86 only
      //
      if( appid != 80 && appid != 86 ) {
        cerr << "dm50 can't find files for apid " << appid
             << ", time " << tbeg << "." << endl ;
        continue ;
      }
      //
      // Determine file to restore DataSet
      //
      filename = DataSet::getDataSetName( appid, tbeg ) ;
			cerr << filename << endl ;

      if( !filename ) {
        cerr << "Can't find file for apid " << appid
             << ", time " << tbeg << "." << endl ;
        continue ;
      }

      if( RWFile::Exists( filename ) ) {

        //
        // Open the file as a RWFile and read it as a DataSet
        //
        RWFile f(filename) ;

				//if( theDS ) delete theDS ;

        if( !(theDS = (DataSet *) RWCollectable::recursiveRestoreFrom(f)) ) {
          cerr << "Can't read DataSet file!" << endl ;
          continue ;
        }
      } else {
        cerr << "Can't find time " << tbeg << " for apid " << appid << endl ;
        theDS = 0 ;
        continue ;
      }

      theVals = theDS->values() ;
      totalRows = theDS->valueCount() ;

      aVal = ((Accessor*) (theVals->at(0)))->access( dmode ) ;
      if( aVal ) id = (int) aVal->getInt() ;
      delete aVal ; aVal = 0 ;

      aVal = ((Accessor*) (theVals->at(0)))->access( ptime ) ;
      if( aVal ) tstart = (double) (aVal->getInt()) ;
      delete aVal ; aVal = 0 ;

      aVal = ((Accessor*) (theVals->at(totalRows-1)))->access( ptime ) ;
      if( aVal ) tstop = (double) (aVal->getInt()) ;
      delete aVal ; aVal = 0 ;

#ifdef DEBUG

      //
      // Dribble
      //
      cerr << __FILE__ << ": client request = C, appid = "
           << appid << ", tbeg = " << tbeg << endl ;

#endif DEBUG

      //
      // Set up the Descriptor List
      //
      clearDescList() ;

      //
      // Output the response
      //
      //fprintf( outpipe, "%d %lf %lf %d ", id, tstart, tstop, totalRows ) ;

      cerr << id     << " "
           << tstart << " "
           << tstop  << " "
           << totalRows  << endl ;

      break ;

    // ===================================================================
    case DESCRIP :                        // User supplies a descriptor
    // ===================================================================

      if( !theDS ) {
        cerr << "Must use the 'C' command first" << endl ;
        theDesc = 0 ;
        continue ;
      }

      //
      // Read Descriptor string
      //
      cin.getline( ddescr, 1024 ) ;

#ifdef DEBUG

      //
      // Dribble
      //
      cerr << __FILE__ << ": Descriptor request = " << ddescr << endl ;

#endif DEBUG

      //
      // Make a real descriptor
      //
      if( !(theDesc = new Descriptor( ddescr ) ) ) { // OOPS! something failed
        cerr << "Unable to make descriptor from " << ddescr << endl ;
        continue ;
      }

      //
      // Does it exist in this DataSet
      //
      if( theDS->isAvailable( *theDesc ) ) {
        addDesc( theDesc ) ;
        currentRow = 0 ;
        cerr << 'T' << endl ;
        break ; // i.e. wait for 'R' or another 'D'
      }

#ifdef DEBUG

      cerr << "Descriptor " << ddescr << " is not available from this DataSet"
           << endl ;

#endif /* DEBUG */

      cerr << 'F' << endl ;
      delete theDesc ;

      theDesc = 0 ;

      break ;

    // ===================================================================
    case NEXTROW :
    // ===================================================================

      if( !theDS ) {
        cerr << "Must use the 'C' command first" << endl ;
        continue ;
      }
      if( !theDesc ) {
        cerr << "Must use the 'D' command first" << endl ;
        continue ;
      }

			clearRDbuf() ;

      if( currentRow >= totalRows ) {

#ifdef DEBUG
        cerr << "No more rows in this dataset" << endl ;
#endif /*  DEBUG */

        //writeRD( outpipe ) ;
        continue ;
      }

      for( i = 0 ; i < theDescriptorCount ; i++ ) {
        aVal=((Accessor*)(theVals->at(currentRow)))->access(*(theDescriptorList[i]));
        //cerr << "Item size is " << aVal->size() << endl ;
        addToRDbuf( aVal->size(), aVal->getCharArray() ) ;
        delete aVal ;
      }

      currentRow++ ;

      break ;

    // ===================================================================
    case EXIT :
    // ===================================================================
      break ;
    // ===================================================================
    case PRINT :
      // run thru rowData
      {
        unsigned char * p = rowData.data ;
        double dtime = *((double*)p) ;
        unsigned long ptime = (unsigned long) dtime ;

        cerr << "Time is " << dtime << endl ; p+=8 ;

        //cerr << "IDF is " << (unsigned) *((unsigned*)p) << endl ; p+=4 ;

        cerr << "LiveTime is " << (unsigned) *((unsigned*)p) << ", " ; p+=4 ;
        cerr << (unsigned) *((unsigned*)p) << ", " ; p+=4 ;
        cerr << (unsigned) *((unsigned*)p) << ", " ; p+=4 ;
        cerr << (unsigned) *((unsigned*)p) << endl ; p+=4 ;

        cerr << "Position is " << (int) *((char*)p) << endl ; p+=1 ;

        //cerr << "LostEvents is " << (short) *((short*)p) << ", " ; p+=2 ;
        //cerr << (short) *((short*)p) << ", " ; p+=2 ;
        //cerr << (short) *((short*)p) << ", " ; p+=2 ;
        //cerr << (short) *((short*)p) << endl ; p+=2 ;

        //cerr << "goodEvents is " << (unsigned) *((unsigned*)p) << ", "; p+=4;
        //cerr << (unsigned) *((unsigned*)p) << ", "; p+=4;
        //cerr << (unsigned) *((unsigned*)p) << ", "; p+=4;
        //cerr << (unsigned) *((unsigned*)p) << endl; p+=4;


        // the event list
        for( j = 0 ; p < rowData.data+rowData.length ; j++, p+=7 ) {
          if( !(j%25) ) printMode1EventHdr() ;
					printMode1Event( decodeMode1Event( p ), ptime ) ;
        }
#if 0
        // print 7 bytes in hex
        for( j = 0 ; j < 2 ; ) {
          printMode1EventHdr() ;
          for( i = 0 ; i < 20 ; i++, p+=7 ) {
            if( j >= (rowData.length-53) ) break ;
            cout << "Event word is "
                 << hex << setw(2) << (int) p[0] << " "
                 << hex << setw(2) << (int) p[1] << " "
                 << hex << setw(2) << (int) p[2] << " "
                 << hex << setw(2) << (int) p[3] << " "
                 << hex << setw(2) << (int) p[4] << " "
                 << hex << setw(2) << (int) p[5] << " "
                 << hex << setw(2) << (int) p[6] << endl ;
            j+=7 ;
          }
          if( i != 20 ) break ;
        }
#endif
      }
      break ;
    // ===================================================================
    default :
    // ===================================================================

      cerr << "Don't know command '" << command << "'. Bye!" << endl ;
      command = EXIT ;

    } /* End switch */

  } while( command != EXIT ) ;

  cerr << argv[0] << ": exiting." << endl ;

  exit (0);
}


@


2.2
log
@removed hx_ids.h from .h files and placed explicitly in .C files
@
text
@d9 1
a9 1
static const char rcsid[]="$Id: readEvent.C,v 2.1 1993/11/10 20:58:37 rbarnett Release rbarnett $";
@


2.1
log
@Creation
@
text
@d9 1
a9 1
static const char rcsid[]="$Id$";
d255 1
a255 1
  DataValue ** theVals = 0 ;
d258 3
a260 3
  static Descriptor * dmode = new Descriptor( "DataMode" ) ;
  static Descriptor * ptime = new Descriptor( "PacketTime" ) ;
  static Descriptor * fitsTime = new Descriptor( "Time" ) ;
a295 1
    //read( STDIN_FILENO, &command, sizeof(command) ) ;
d350 2
a351 2
      aVal = ((Accessor*) theVals[0])->access( dmode ) ;
      if( aVal ) id = aVal->getInt() ;
d354 1
a354 1
      aVal = ((Accessor*) theVals[0])->access( ptime ) ;
d358 1
a358 1
      aVal = ((Accessor*) theVals[totalRows-1])->access( ptime ) ;
d424 1
a424 1
      if( theDS->isAvailable( theDesc ) ) {
d471 1
a471 1
        aVal=((Accessor*)theVals[currentRow])->access(theDescriptorList[i]);
d495 1
a495 1
        cerr << "IDF is " << (unsigned) *((unsigned*)p) << endl ; p+=4 ;
d502 11
a512 9
        cerr << "LostEvents is " << (short) *((short*)p) << ", " ; p+=2 ;
        cerr << (short) *((short*)p) << ", " ; p+=2 ;
        cerr << (short) *((short*)p) << ", " ; p+=2 ;
        cerr << (short) *((short*)p) << endl ; p+=2 ;

        cerr << "goodEvents is " << (unsigned) *((unsigned*)p) << ", "; p+=4;
        cerr << (unsigned) *((unsigned*)p) << ", "; p+=4;
        cerr << (unsigned) *((unsigned*)p) << ", "; p+=4;
        cerr << (unsigned) *((unsigned*)p) << endl; p+=4;
a513 1
        cerr << "Position is " << (int) *((char*)p) << endl ; p+=1 ;
@

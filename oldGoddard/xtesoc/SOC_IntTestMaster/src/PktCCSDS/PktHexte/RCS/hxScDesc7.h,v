head	3.2;
access
	rbarnett
	mmarlowe
	rbentley
	dhon
	ghuszar
	kmurphy
	soccm
	tgasaway;
symbols
	Build4_3:3.2
	Build4_1:3.1
	Build4:3.1
	Build3_3:3.1
	Current:3.1
	Build3_2:3.1
	Build3_1:3.1
	Build3:3.1
	Build2:2.1;
locks;
comment	@ * @;


3.2
date	95.04.20.21.57.23;	author rbentley;	state Exp;
branches;
next	3.1;

3.1
date	94.05.09.00.41.52;	author rbentley;	state Exp;
branches;
next	2.1;

2.1
date	93.10.13.21.40.43;	author rbarnett;	state Release;
branches;
next	;


desc
@Creation
@


3.2
log
@mode 7 descriptor definition; part of George's major rewrite
@
text
@//**********************************************************************
//  hxScDesc7.h
//**********************************************************************
//  Subsystem   : HEXTE Data Management
//  Programmer  : Randall D. Barnette/Hughes STX
//  Ass't Prog. : George L. Huszar/UCSD : changes : January, 1995
//
//  Description : These are descriptor strings for HEXTE Science Mode 7
//
//  RCS: $Id: hxScDesc7.h,v 3.2 1994/11/18 23:09:19 rbentley Exp $
//**********************************************************************


#include "mkdesc.h"


// set up the "enum" table for the descriptor codes :
enum { PKTTIME,   APPID,     DATAMODE,  FITSTIME,  CLSTRPOS
     , GDEVTSD0,  GDEVTSD1,  GDEVTSD2,  GDEVTSD3,  GDEVTSSUM
     , NUMBYTES,  MEMDUMP
     } ;

const descTags  hxScDesc7[]=
//  PacketTime and AppID :
{ { "PacketTime"                           , "Packet time stamp [long]"
  , PKTTIME }
, { "AppID"                                , "Packet application ID"
  , APPID }

//  data-mode and FITS time :
//  anything with "n" must be substituted at class instantiation time :
, { "DataMode"                             , "ScienceMode<<8 & ControlTable"
  , DATAMODE }
, { "O[XTE]&I[HEXTE]&G[n]&Time"            , "FITS Time [double]"
  , FITSTIME }

//  cluster position :
, { "O[XTE]&I[HEXTE]&G[n]&H[ClstrPosition]", "the cluster position"
  , CLSTRPOS }

//  good events :
, { "O[XTE]&I[HEXTE]&G[n]&D[0]&H[goodEvents]", ""
  , GDEVTSD0 }
, { "O[XTE]&I[HEXTE]&G[n]&D[1]&H[goodEvents]", ""
  , GDEVTSD1 }
, { "O[XTE]&I[HEXTE]&G[n]&D[2]&H[goodEvents]", ""
  , GDEVTSD2 }
, { "O[XTE]&I[HEXTE]&G[n]&D[3]&H[goodEvents]", ""
  , GDEVTSD3 }
, { "O[XTE]&I[HEXTE]&G[n]&D[0:3]&H[goodEvents]", ""
  , GDEVTSSUM }

//  the memory-dump byte-count : this tells how long the memory-dump is :
//  (how do we do the descriptor string for this ? I haven't the faintest ... :)
, { "numbytes"                     , "no. bytes in the memory dump"
  , NUMBYTES }

//  the actual mode 7 data : the memory dump :
//  (how do we do the descriptor string for this ? I haven't the faintest ... :)
, { "memDump"                     , "the actual memory dump"
  , MEMDUMP }
} ;
@


3.1
log
@Build 3
@
text
@d1 61
a61 67
// Program Name: hxScDesc7.h
// Subsystem   : HEXTE Data Management
// Programmer  : Randall D. Barnette -- Hughes STX
// Description:
//
// Descriptor strings for HEXTE Science Mode 7
//
// RCS: $Id: hxScDesc7.h,v 2.1 1993/10/13 21:40:43 rbarnett Release rbentley $
//

const struct descTags {

  char * tag ;   // The name by which database access will take place
  char * desc ;  // Text description

} hxScDesc7[] = {

//
// By convention (my own of course) time and apid are always 0 and 1
//
  { "PacketTime"                             , "Packet time stamp (long)" },
  { "AppID"                                  , "Packet application ID"    },

//
// Must have these for GOF
// Anything with "n" must be substituted at class instantiation time.
// These values differ per apid.
//
  { "DataMode"                               , "ScienceMode<<8 & ControlTable"},
  { "O(XTE)&I(HEXTE)&G(n)&Time"              , "FITS Time (double)"       },
  { "All"                                    , "Everything"               },
  { "O(XTE)&I(HEXTE)&G(n)&D(0:3)&E(*|)&C(*|)", "Everything"               },

// Cluster Position
  { "O(XTE)&I(HEXTE)&G(n)&H(ClstrPosition)", "the cluster position" },

// Good Events
  { "O(XTE)&I(HEXTE)&G(n)&D(0)&H(goodEvents)", "" },
  { "O(XTE)&I(HEXTE)&G(n)&D(1)&H(goodEvents)", "" },
  { "O(XTE)&I(HEXTE)&G(n)&D(2)&H(goodEvents)", "" },
  { "O(XTE)&I(HEXTE)&G(n)&D(3)&H(goodEvents)", "" },
  { "O(XTE)&I(HEXTE)&G(n)&D(0:3)&H(goodEvents)", "" },

// IDF num
  { "O(XTE)&I(HEXTE)&G(n)&H(IDF)", "the live time IDF" },

// Live Time
  { "O(XTE)&I(HEXTE)&G(n)&D(0)&H(LiveTime)", "the live time for det 1" },
  { "O(XTE)&I(HEXTE)&G(n)&D(1)&H(LiveTime)", "the live time for det 2" },
  { "O(XTE)&I(HEXTE)&G(n)&D(2)&H(LiveTime)", "the live time for det 3" },
  { "O(XTE)&I(HEXTE)&G(n)&D(3)&H(LiveTime)", "the live time for det 4" },
  { "O(XTE)&I(HEXTE)&G(n)&D(0:3)&H(LiveTime)", "the live time array" },

// Lost Events
  { "O(XTE)&I(HEXTE)&G(n)&D(0)&H(LostEvents)", "the lost events for det 1" },
  { "O(XTE)&I(HEXTE)&G(n)&D(1)&H(LostEvents)", "the lost events for det 2" },
  { "O(XTE)&I(HEXTE)&G(n)&D(2)&H(LostEvents)", "the lost events for det 3" },
  { "O(XTE)&I(HEXTE)&G(n)&D(3)&H(LostEvents)", "the lost events for det 4" },
  { "O(XTE)&I(HEXTE)&G(n)&D(0:3)&H(LostEvents)", "the lost events array" },

// Event List
  { "O(XTE) & I(HEXTE) & G(n) & D(0~3) & E(*|) & C(*|) & T(0~16) >>         \
     E(Spare) {8}, E(PLS)     {1}, E(ULD) {1},  E(VS4,VS3,VS2,VS1,VS0) {5}, \
     E(CSI)   {1}, E(0:63)    {6}, E(CAL) {1},  T(0:15)                {4}, \
     D(0:3)   {2}, E(LE1,LE0) {2}, T(0:1) {17}, C(0:255)               {8}" ,
    "an Event List"  }

@


2.1
log
@Creation
@
text
@d8 1
a8 1
// RCS: $Id: hxScDescMode1.h,v 2.1 1993/08/09 13:10:10 rbarnett Exp rbarnett $
@

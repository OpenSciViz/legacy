head	3.5;
access
	rbarnett
	mmarlowe
	rbentley
	dhon
	ghuszar
	kmurphy
	soccm
	tgasaway;
symbols
	Build4_5_3:3.5
	Build4_1:3.4
	Build4:3.4
	Build3_3:3.4
	Current:3.4
	Build3_2:3.4
	Build3_1:3.3
	Build3:3.1
	Build2:2.4;
locks;
comment	@ * @;


3.5
date	95.11.03.15.51.49;	author rbarnett;	state Exp;
branches;
next	3.4;

3.4
date	94.07.22.19.50.07;	author rbentley;	state Exp;
branches;
next	3.3;

3.3
date	94.06.28.23.15.25;	author rbentley;	state Exp;
branches;
next	3.2;

3.2
date	94.06.28.23.04.02;	author rbentley;	state Exp;
branches;
next	3.1;

3.1
date	94.05.09.00.41.52;	author rbentley;	state Exp;
branches;
next	2.5;

2.5
date	94.01.13.21.19.05;	author rbarnett;	state Exp;
branches;
next	2.4;

2.4
date	93.10.11.16.39.12;	author rbarnett;	state Release;
branches;
next	2.3;

2.3
date	93.09.30.20.39.49;	author rbarnett;	state Exp;
branches;
next	2.2;

2.2
date	93.08.20.17.18.27;	author rbarnett;	state Exp;
branches;
next	2.1;

2.1
date	93.08.09.13.09.58;	author rbarnett;	state Exp;
branches;
next	;


desc
@Creation
@


3.5
log
@Fixed nibble problem
added dataMode method
Time is extracted from IDF not packet
update RW save/restore
@
text
@// Program Name: HxScPart.h
// Subsystem   : HEXTE Data Management
// Programmer  : Randall D. Barnette -- Hughes STX
// Description:
//
//  Declaration of HEXTE science telemetry partition
//
// RCS: $Id: HxScPart.h,v 3.5 1995/11/01 18:46:53 soccm Exp $

//  Feature test switches
#ifndef HXSCPART_H
#define HXSCPART_H

// Local Headers
#include "IDF.h"
#include "PktHxSc.h"

//
// Forward declarations
//
class HxScPart ;

//
// Create a short name for a pointer to a function that takes a
// constant pointer to a HEXTE Science Partition object and returns nothing.
//
typedef void (*HxScPartCallback)(const HxScPart*) ;

class HxScPart : public RWCollectable {

  //
  // Declare this class as a RogueWave Collectable. This macro declares some
  // functions necessary for the persistent object Factory
  //
  RWDECLARE_COLLECTABLE(HxScPart)

public:

  //
  // Constructor takes a packet that is the first in an IDF
  //
  HxScPart(PktHxSc*) ;
  HxScPart(const HxScPart& rhs) ;

  //
  // Constructor for Science Monitor to put Humpty Dumpty together again
  //
  HxScPart(HxIDFHeader* hdr, unsigned long pktTime, long apId,
           unsigned long numDataBytes, unsigned char *sciData) ;

  //
  // Default constructor for RogueWave
  //
  HxScPart(void) ;

  //
  // Destructor deletes scienceData buffer
  //
  ~HxScPart(void) ;

  //
  // Return the IDF Header
  //
  const HxIDFHeader* IDFHdr(void) const ;

  //
  // Return the science data (used by science monitor hexteRtClient)
  //
  const unsigned char * sciData(void) const ;

  //
  // Add a new packet's worth of data to the science data buffer
  //
  void insertData(PktHxSc*) ;

  unsigned long dataMode(void) const ;

// ==========================================================
// Various calls to access IDF header data from the partition
// ==========================================================

  //
  // Return the partition's science mode
  //
  int scienceMode(void) const ;

  //
  // Return the partition's science control table
  //
  int controlTable(void) const ;

  //
  // Return the cluster position value
  //
  int clusterPosition(void) const ;

  //
  // Return the number of bytes for each event word
  //
  int bytesPerEvent(void) const ;

  //
  // Return the partition good events rate
  //
  int goodEvents(int) const ;

  //
  // Print the IDF Header
  //
  void dumpIDFHeader(ostream&) const ;

  // ===============================================
  // Get values from the science data buffer
  // ===============================================

  //
  // Return a value at a given offset as various types.
  //
  unsigned long  getULong(int)   const ;
  unsigned short getUShort(int)  const ;
  unsigned char  getUChar(int)   const ;
  unsigned char  getUNibble(int) const ;
  const unsigned char* getUCharP(int) const ;

  //
  // Return the size of the data buffer
  //
  unsigned dataBufferSize(void) const ;

  // ===============================================
  // These are named the same as in the Packet class
  // ===============================================

  //
  // Return the start time of the partition. This is for now the packet time
  // stamp of the first packet.
  //
  unsigned long seconds(void) const ;

  //
  // Return the partition's appid
  //
  int applicationID(void) const ;

  //
  // The setDisposition method allows the user to supply a "callback" type
  // function that gets called anytime a HEXTE Science partition is
  // available. It returns the old callback function.
  // The user-supplied function has the following signature
  // void myHandler(const HxScPart*) ;
  // and it is passed to the system by:
  // PHxScPartF oldFunction = HxScPart::setDisposition(myHandler) ;
  //
  static HxScPartCallback setDisposition(HxScPartCallback) ;

  // ----------------------------------------------
  // The following are redefined from RWCollectable
  // ----------------------------------------------

  RWspace  binaryStoreSize() const ;
  int       compareTo(const RWCollectable*) const ;
  RWBoolean isEqual(const RWCollectable*) const ;
  void      restoreGuts(RWFile&) ;
  void      restoreGuts(RWvistream&) ;
  void      saveGuts(RWFile&) const ;
  void      saveGuts(RWvostream&) const ;

private:

  //
  // All partitions must have an Instrument Data Frame header
  //
  HxIDFHeader* header ;

  //
  // This is the time of the first packet in the partition
  //
  unsigned long packetTime ;

  //
  // The apid of the data from which this partition is formed.
  // Either 80 (Cluster I) or 86 (Cluster II)
  //
  long apid ;

  //
  // Other than the IDF header, all partition data resides in the scienceData
  // buffer. dataSize is the size of that buffer. All data in the buffer is
  // byte-swapped when the packet data is concatenated to the buffer.
  //
  unsigned long  dataSize ;
  unsigned char *scienceData ;

  //
  // This is the partition handler callback function. It is static because
  // it applies to the class. It's default value is zero in which case
  // it does not get called.
  // Typically, only the dispose method can call it, and setDisposition
  // can set it.
  // dispose is protected and is only called by apply().
  // apply() is public.
  // setDisposition() is public.
  //
  static HxScPartCallback theHandler ;

  //
  // dispose checks the value of theHandler and calls it with
  // argument "this" if it exists.
  //
  void dispose(void) const ;

  //
  // Allow PktHxSc::apply() to call dispose.
  //
  friend void PktHxSc::apply(void) const ;

  //
  // For checking that packet sequences are consistent.
  //
  unsigned long  nrPackets ;

} ;

//
// The Inline functions
//
// Return values from the private data:
//
inline const HxIDFHeader* HxScPart::IDFHdr(void) const
{
  return header ;
}

inline unsigned long HxScPart::seconds(void) const
{
  return packetTime ;
}

inline int HxScPart::applicationID(void) const
{
  return apid ;
}

inline int HxScPart::bytesPerEvent(void) const
{
  return header->bytesPerEvent() ;
}

inline const unsigned char * HxScPart::sciData(void) const
{
  return scienceData ;
}
//
// Return items from the IDF header:
//
inline int HxScPart::scienceMode(void) const
{
  return header->scienceMode() ;
}

inline int HxScPart::controlTable(void) const
{
  return header->controlTable();
}

inline int HxScPart::clusterPosition(void) const
{
  return header->clusterPos();
}

inline unsigned HxScPart::dataBufferSize(void) const
{
  return dataSize ;
}

inline int HxScPart::goodEvents(int detector) const
{
  return header->goodEvents(detector) ;
}

inline unsigned long HxScPart::getULong(int offset) const
{
  const unsigned char* c = scienceData+offset ;
  return (unsigned long) (c[3]<<24 | c[2]<<16 | c[1]<<8 | c[0]) ;
}

inline unsigned short HxScPart::getUShort(int offset) const
{
  const unsigned char* c = scienceData+offset ;
  return (unsigned short) (c[1]<<8 | c[0]) ;
}

inline unsigned char HxScPart::getUChar(int offset) const
{
  return *(scienceData+offset) ;
}

inline unsigned char HxScPart::getUNibble(int offset) const
{
  //
  // get low nibble if offset is even
  //
  return offset % 2 ?
		      *(scienceData+offset/2) >> 4   :
          *(scienceData+offset/2) & 0x0f ;
}

inline const unsigned char* HxScPart::getUCharP(int offset) const
{
  return scienceData + offset ;
}

inline void HxScPart::dumpIDFHeader(ostream& os) const
{
  header->dumpIDFHeader(os) ;
}

//
// Partition handler code 
//
inline HxScPartCallback HxScPart::setDisposition(HxScPartCallback newHandler)
{
  HxScPartCallback temp = theHandler ;
  theHandler = newHandler ;
  return temp ;
}

inline void HxScPart::dispose(void) const
{
  if( theHandler ) theHandler(this) ;
}

#endif /* HXSCPART_H */
@


3.4
log
@changed type of binaryStoreSize for RW 6
@
text
@d8 1
a8 1
// RCS: $Id: HxScPart.h,v 3.3 1994/06/28 23:15:25 rbentley Exp rbentley $
a17 4
#if RWTOOLS < 0x600 & ! defined(RWspace)
#define RWspace unsigned
#endif

d43 1
d76 2
d301 1
a301 1
  // get low nibble if offset is odd
d303 3
a305 2
  return offset % 2 ? *(scienceData+offset/2) & 0x0f :
		      *(scienceData+offset/2) >> 4   ;
@


3.3
log
@minor bug fix
@
text
@d8 1
a8 1
// RCS: $Id: HxScPart.h,v 3.2 1994/06/28 23:04:02 rbentley Exp rbentley $
d18 4
d161 1
a161 1
  unsigned  binaryStoreSize() const ;
@


3.2
log
@added constructor and sciData function for use by Science Monitor
hexteRtClient
@
text
@d8 1
a8 1
// RCS: $Id: HxScPart.h,v 3.1 1994/05/09 00:41:52 rbentley Exp rbentley $
d246 1
a246 1
inline const unsigned char * sciData(void) const
@


3.1
log
@Build 3
@
text
@d8 1
a8 1
// RCS: $Id: HxScPart.h,v 2.5 1994/01/13 21:19:05 rbarnett Exp rbentley $
d44 9
a52 3
	//
	// Default constructor for RogueWave
	//
d55 3
a57 3
	//
	// Destructor deletes scienceData buffer
	//
d66 5
d89 9
a97 9
	//
	// Return the cluster position value
	//
	int clusterPosition(void) const ;

	//
	// Return the number of bytes for each event word
	//
	int bytesPerEvent(void) const ;
d113 3
a115 3
	//
	// Return a value at a given offset as various types.
	//
d122 3
a124 3
	//
	// Return the size of the data buffer
	//
d142 10
a151 10
	//
	// The setDisposition method allows the user to supply a "callback" type
	// function that gets called anytime a HEXTE Science partition is
	// available. It returns the old callback function.
	// The user-supplied function has the following signature
	// void myHandler(const HxScPart*) ;
	// and it is passed to the system by:
	// PHxScPartF oldFunction = HxScPart::setDisposition(myHandler) ;
	//
	static HxScPartCallback setDisposition(HxScPartCallback) ;
d167 3
a169 3
	//
	// All partitions must have an Instrument Data Frame header
	//
d172 3
a174 3
	//
	// This is the time of the first packet in the partition
	//
d177 4
a180 4
	//
	// The apid of the data from which this partition is formed.
	// Either 80 (Cluster I) or 86 (Cluster II)
	//
d183 5
a187 5
	//
	// Other than the IDF header, all partition data resides in the scienceData
	// buffer. dataSize is the size of that buffer. All data in the buffer is
	// byte-swapped when the packet data is concatenated to the buffer.
	//
d191 26
a216 26
	//
	// This is the partition handler callback function. It is static because
	// it applies to the class. It's default value is zero in which case
	// it does not get called.
	// Typically, only the dispose method can call it, and setDisposition
	// can set it.
	// dispose is protected and is only called by apply().
	// apply() is public.
	// setDisposition() is public.
	//
	static HxScPartCallback theHandler ;

	//
	// dispose checks the value of theHandler and calls it with
	// argument "this" if it exists.
	//
	void dispose(void) const ;

	//
	// Allow PktHxSc::apply() to call dispose.
	//
	friend void PktHxSc::apply(void) const ;

	//
	// For checking that packet sequences are consistent.
	//
d224 2
a225 1

d228 1
a228 1
	return header ;
d233 1
a233 1
	return packetTime ;
d238 1
a238 1
	return apid ;
d241 12
d255 1
a255 1
	return header->scienceMode() ;
d260 1
a260 1
	return header->controlTable();
d265 1
a265 6
	return header->clusterPos();
}

inline int HxScPart::bytesPerEvent(void) const
{
	return header->bytesPerEvent() ;
d270 1
a270 1
	return dataSize ;
d280 2
a281 2
	const unsigned char* c = scienceData+offset ;
	return (unsigned long) (c[3]<<24 | c[2]<<16 | c[1]<<8 | c[0]) ;
d286 2
a287 2
	const unsigned char* c = scienceData+offset ;
	return (unsigned short) (c[1]<<8 | c[0]) ;
d292 1
a292 1
	return *(scienceData+offset) ;
d297 5
a301 6
	//
	// get low nibble if offset is odd
	//
	return offset % 2 ?
		*(scienceData+offset/2) & 0x0f :
		*(scienceData+offset/2) >> 4   ;
d306 1
a306 1
	return scienceData + offset ;
d319 3
a321 3
	HxScPartCallback temp = theHandler ;
	theHandler = newHandler ;
	return temp ;
d326 1
a326 1
	if( theHandler ) theHandler(this) ;
@


2.5
log
@Now has disposition handler code
@
text
@d8 1
a8 1
// RCS: $Id: HxScPart.h,v 2.4 1993/10/11 16:39:12 rbarnett Release $
@


2.4
log
@Update for FITS generation.
@
text
@d8 1
a8 1
// RCS: $Id: HxScPart.h,v 2.3 1993/09/30 20:39:49 rbarnett Exp rbarnett $
d18 10
a27 1
class AccHxSc ;
a36 35
private:

	//
	// All partitions must have an Instrument Data Frame header
	//
  HxIDFHeader   *header ;

	//
	// This is the time of the first packet in the partition
	//
  unsigned long  packetTime ;

	//
	// The apid of the data from which this partition is formed.
	// Either 80 (Cluster I) or 86 (Cluster II)
	//
  long apid ;

	//
	// Other than the IDF header, all partition data resides in the scienceData
	// buffer. dataSize is the size of that buffer. All data in the buffer is
	// byte-swapped when the packet data is concatenated to the buffer.
	//
  unsigned long  dataSize ;
  unsigned char *scienceData ;

#ifdef PARANOID

	//
	// For checking that packet sequences are consistent.
	//
  unsigned long  nrPackets ;

#endif /* PARANOID */

d47 1
a47 1
  HxScPart() ;
d52 1
a52 1
  ~HxScPart() ;
a58 5
	//
	// Return the proper accessor for this partition
	//
  AccHxSc* asAccessor(void) const ;

d131 11
d154 53
d295 15
@


2.3
log
@ Update for integration of Science and Housekeeping
@
text
@d8 1
a8 1
// RCS: $Id: HxScPart.h,v 2.2 1993/08/20 17:18:27 rbarnett Exp rbarnett $
d30 3
d34 4
d39 14
a52 1
  long           apid ;
d55 4
d60 1
a62 3
  unsigned long  dataSize ;
  unsigned char *scienceData ;

d66 1
a66 1
  // Constructor
d68 10
a77 2
  HxScPart( PktHxSc * ) ;
  HxScPart() {} // Default, for RogueWave
d85 3
d90 5
d102 1
a102 1
  inline int scienceMode() const ;
d107 1
a107 3
  inline int controlTable() const ;

	inline int clusterPosition(void) const ;
d109 9
a117 1
	inline int bytesPerEvent(void) const ;
d122 1
a122 1
  inline int goodEvents(int detector) const ;
a124 6
  // Add a new packet's worth of data to the science data buffer
  //
  void insertData( PktHxSc* aPacket ) ;
  inline unsigned dataBufferSize( void ) const ;

  //
d133 13
a145 29
  inline unsigned long getULong(int offset) const
  {
    const unsigned char* c = scienceData+offset ;
    return (unsigned long) (c[2]<<24 | c[3]<<16 | c[0]<<8 | c[1]) ;
  }

  inline unsigned short getUShort(int offset) const
  {
    const unsigned char* c = scienceData+offset ;
    return (unsigned short) (c[0]<<8 | c[1]) ;
  }

  inline unsigned char getUChar(int offset) const
  {
    return *(scienceData+offset) ;
  }

  inline unsigned char getUNibble(int offset) const
  {
		// get low nibble if offset is odd
		return offset % 2 ?
			*(scienceData+offset/2) & 0x0f :
			*(scienceData+offset/2) >> 4   ;
  }

  inline const unsigned char* getUCharP(int offset) const
  {
    return scienceData + offset ;
  }
d155 1
a155 1
  inline unsigned long seconds(void) const ;
d160 1
a160 1
  inline int applicationID(void) const ;
d163 1
a163 1
  // The following are inherited from RWCollectable
d180 31
a210 6
inline const HxIDFHeader* HxScPart::IDFHdr(void) const { return header ; }
inline unsigned long      HxScPart::seconds(void) const { return packetTime ; }
inline int HxScPart::applicationID(void) const { return apid ; }
inline int HxScPart::scienceMode(void) const { return header->scienceMode() ; }
inline int HxScPart::controlTable(void) const { return header->controlTable();}
inline int HxScPart::clusterPosition(void) const { return header->clusterPos();}
a214 1
inline unsigned HxScPart::dataBufferSize(void) const { return dataSize ; }
d216 5
d225 37
@


2.2
log
@Added new mode constructors
@
text
@d8 1
a8 1
// RCS: $Id: HxScPart.h,v 2.1 1993/08/09 13:09:58 rbarnett Exp rbarnett $
d67 9
d84 1
d162 7
@


2.1
log
@Creation
@
text
@d8 1
a8 1
// RCS: $Id$
d22 5
a26 5
	//
	// Declare this class as a RogueWave Collectable. This macro declares some
	// functions necessary for the persistent object Factory
	//
	RWDECLARE_COLLECTABLE(HxScPart)
d30 3
a32 3
	HxIDFHeader   *header ;
	unsigned long  packetTime ;
	long           apid ;
d35 1
a35 1
	unsigned long  nrPackets ;
d38 2
a39 2
	unsigned long  dataSize ;
	unsigned char *scienceData ;
d43 11
a53 11
	//
	// Constructor
	//
	HxScPart( PktHxSc * ) ;
	HxScPart() {} // Default, for RogueWave
	~HxScPart() ;

	//
	// Return the IDF Header
	//
	const HxIDFHeader* IDFHdr(void) const ;
d55 1
a55 1
	AccHxSc* asAccessor(void) const ;
d61 68
a128 34
	//
	// Return the partition's science mode
	//
	inline int scienceMode() const ;

	//
	// Return the partition good events rate
	//
	inline int goodEvents(int detector) const ;

	//
	// Add a new packet's worth of data to the science data buffer
	//
	void insertData( PktHxSc* aPacket ) ;

	//
	// Print the IDF Header
	//
	void dumpIDFHeader(ostream&) const ;

// ===============================================
// These are named the same as in the Packet class
// ===============================================

	//
	// Return the start time of the partition. This is for now the packet time
	// stamp of the first packet.
	//
	inline unsigned long seconds(void) const ;

	//
	// Return the partition's appid
	//
	inline int applicationID(void) const ;
d155 1
a155 1
	return header->goodEvents(detector) ;
@

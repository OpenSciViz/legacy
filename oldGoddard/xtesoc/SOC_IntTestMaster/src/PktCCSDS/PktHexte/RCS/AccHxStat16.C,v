head	4.1;
access
	soccm;
symbols
	Build4_5_3:4.1;
locks;
comment	@ * @;


4.1
date	95.11.03.14.49.23;	author rbarnett;	state Exp;
branches;
next	;


desc
@Accessor for HEXTE 16 second Status data
@


4.1
log
@initial version
@
text
@// ==========================================================================
// File Name        : AccHxStat16.C
// Subsystem        : HEXTE Data Management/Ingest
// Programmer       : Randall D. Barnette
// Description      : 
//
// ==========================================================================

static const char* const rcsid =
"$Id: AccHxStat16.C,v 1.1 1995/11/01 18:46:53 soccm Exp $" ;

#include <fstream.h>
#include <stdlib.h>
#include <rw/regexp.h>
#include <rw/ctoken.h>
#include <rw/cstring.h>

#include <AccHxStat16.h>

#include "hxPkts_ids.h"

RWOrdered* AccHxStat16::c1Descriptors = 0 ;
RWOrdered* AccHxStat16::c2Descriptors = 0 ;

inline int atoi(const RWCString&s)
{
  return atoi(s.data()) ;
}

RWOrdered* rh(int c)
{

  RWOrdered* theList = new RWOrdered ;
  RWCString s,st ;
  RWCString f ;
  char* p = getenv("SOCHOME") ;
  static char* t = "/etc/HkItem.table" ;
  int bits, bytes, conv ;

  char* table = strcpy( new char[strlen(p)+strlen(t)+1], p) ;
  strcat(table,t) ;

  ifstream in(table) ;

  s.readLine(in) ;
  s.readLine(in) ;
  s.readLine(in) ;
  s.readLine(in) ;
  s.readLine(in) ;
  while( s.readLine(in), !s.isNull() ) {
    RWCTokenizer next(s) ;
    st = next() ;

    if( st(0,2) == "//" ) continue ;

    // packet
    int p = atoi(st) ;
    if( c == 0 && (p == 4 || p == 5 || p == 6 ) ) continue ;
    if( c == 1 && p != 2 ) continue ;
    if( c == 2 && p != 2 ) continue ;
    if( c == 3 && p != 7 ) continue ;

    next() ;
    bytes = atoi(next()) ;
    bits = atoi(next()) ;
    next() ;
    next() ;
    if( atoi(next()) != c ) continue ; // cycle
    next() ;
    next() ;
    conv = atoi(next()) ;
    next() ;
    if( atoi(next()) == 0 ) continue ; // in telem
    next() ;
    next() ;
    next() ;
    next() ;
    next() ;
    next() ;
    next() ;
    next() ;
    next() ;
    // mnemonic
    if( (st=next()).contains("_TU") ) continue ;

// Done, write something
    char buf[100] ;

    bits += 8*bytes ;
    sprintf( buf, ",<<%d>>", bits ) ;
    if( conv ) {
      strcat( buf, "32f" ) ;
    } else {
      if( bits < 9 )  strcat( buf, "8" ) ;
      else if( bits < 17 ) strcat( buf, "16" ) ;
      else if( bits < 33 ) strcat( buf, "32s" ) ;
      else strcat( buf, "64" ) ;
    }
    st += buf ;
    theList->append(new RWCollectableString(st)) ;

  }
  in.close() ;
  return theList ;
}

static RWOrdered* checkDescriptors(RWOrdered*& d, int cluster)
{

  if( !d ) {

    static RWCString prefix = "O[XTE]&I[HEXTE]&" ;

    static RWCRegexp detPattern("Det[1-4]") ;
    const  RWCString gdesc = (cluster==1) ? "G[0]&" : "G[1]&" ;
    RWCString desc ;

    d = new RWOrdered ;

    d->append(new Descriptor("Time")) ;
    d->append(new Descriptor("Appid")) ;
    d->append(new Descriptor("datamode")) ;

    RWOrdered* list = rh(0) ;

    int j ;
    for( int i = 0, c = list->entries() ; i < c ; i++ ) {
      RWCString s(*((RWCollectableString*)list->at(i))) ;
      char f[256] ;
      int comma = s.index(",") ;
      strcpy(f,s.data()+comma+1) ;
      s.remove(comma) ;
      desc = prefix ;
      desc += gdesc ;
      if( RW_NPOS != (j=s.index(detPattern)) ) {
        switch(s(j+3,1).data()[0]) {
				case '1' : desc += "D[0]&H[" ; break ;
				case '2' : desc += "D[1]&H[" ; break ;
				case '3' : desc += "D[2]&H[" ; break ;
				case '4' : desc += "D[3]&H[" ; break ;
        }
        desc += s(0,j) ;
        desc += "]" ;
      } else {
        desc += "H[" ;
        desc += s ;
        desc += "]" ;
      }
      desc += f ;
      d->append(new Descriptor(desc)) ;
    }
  }
  return d ;
}

RWDEFINE_COLLECTABLE(AccHxStat16,AccHxStat16_ID) ;

// Description:
// Release all memory associated with static descriptor lists.

void AccHxStat16::destroyDescriptors(void)
{
  if( c1Descriptors ) {
    c1Descriptors->clearAndDestroy() ;
    delete c1Descriptors ;
  }
  if( c2Descriptors ) {
    c2Descriptors->clearAndDestroy() ;
    delete c2Descriptors ;
  }
  c1Descriptors = 0 ;
  c2Descriptors = 0 ;
}

// Description:
// Default constructor for Rogue Wave use.

AccHxStat16::AccHxStat16(void) {}

// Description:
// Construct an AccHxStat16 from a 16-second partition.

AccHxStat16::AccHxStat16(const HxHk16Part& aPart) : thePart(aPart) {}

// Description:
// Return a data value for the indicated Descriptor.

DataValue* AccHxStat16::access(const Descriptor& aSelection) const
{
  DataValue* theValue = 0 ;

  if( aSelection == "TIME" ) {
    theValue = new DataValue( BaseType_DOUBLE ) ;
    theValue->setValue( new double(time()) ) ;
  } else if( aSelection == "APPID" ) {
    theValue = new DataValue( BaseType_UINT32 ) ;
    theValue->setValue( new UINT32(apid()) ) ;
  } else if( aSelection == "DATAMODE" ) {
    theValue = new DataValue( BaseType_UINT32 ) ;
    theValue->setValue( new UINT32(0) ) ;
  }

  if( theValue ) return theValue ;

  Descriptor mnemonic = aSelection.housekeeping() ;
  RWCString det = aSelection.detector() ;

  if( !det.isNull() ) {
    switch( atoi(det) ) {
    case 0 : mnemonic += "Det1" ; break ;
    case 1 : mnemonic += "Det2" ; break ;
    case 2 : mnemonic += "Det3" ; break ;
    case 3 : mnemonic += "Det4" ; break ;
    }
  }

  short elems ;
  short xtype ;

  char buf[64] ;

  // getStatData should take a const char* first arg...but nooooo!
  strcpy(buf,mnemonic.data()) ;

  void *x = thePart.getStatData(buf,elems,xtype) ;

  if( elems != 1 ) return theValue ;

  switch( xtype ) {
  case BaseType_DOUBLE :
    theValue = new DataValue( BaseType_FLOAT ) ;
    theValue->setValue( new float(*((double*)x)) ) ;
    break ;
  case BaseType_UCHAR :
    theValue = new DataValue( BaseType_UINT8 ) ;
    theValue->setValue( new UINT8(*((unsigned int*)x)) ) ;
    break ;
  case BaseType_USHORT :
    theValue = new DataValue( BaseType_UINT16 ) ;
    theValue->setValue( new UINT16(*((unsigned int*)x)) ) ;
    break ;
  case BaseType_UINT :
    theValue = new DataValue( BaseType_INT32 ) ;
    theValue->setValue( new INT32(*((unsigned int*)x)) ) ;
    break ;
  default :
    break ;
  }

  delete x ;

  return theValue ;
}

// Description:
// Return the set of descriptors available.

const RWOrdered* AccHxStat16::descriptors(void) const
{
  return thePart.clusterID() == 1 ?
    checkDescriptors(c1Descriptors,1) :
    checkDescriptors(c2Descriptors,2) ;
}

// Description:
// Return the number of descriptors available.

int AccHxStat16::descriptorCount(void) const
{
  return descriptors()->entries() ;
}

// Description:
// Return true if the descriptor is available from this accessor.

int AccHxStat16::isAvailable(const Descriptor& aDescriptor) const
{
  return descriptors()->contains(&aDescriptor) ? 1 : 0 ;
}

// Description:
// Does nothing.

unsigned long AccHxStat16::getDataValue(void) const { return 0 ; }

// Description:
// Return the storage size of the object.

RWspace AccHxStat16::binaryStoreSize(void) const
{
  return Accessor::binaryStoreSize() +
    thePart.recursiveStoreSize() ;
}

// Description:
// Compare two Accessors. Return -1 if self is "less than" argument.
// Return 1 if self is "greater than" argument. Return zero if
// they are equivalent.

int AccHxStat16::compareTo(const RWCollectable* c) const
{
  const AccHxStat16* rhs = (const AccHxStat16*) c ;
  unsigned long t1 = time() ;
  unsigned long t2 = rhs->time() ;
  return  t1 < t2 ? -1 : t1 > t2 ?  1 : 0 ;
}

// Description:
// Return true if Accessors are equivalent.

RWBoolean AccHxStat16::isEqual(const RWCollectable* c) const
{
  return compareTo(c) ? FALSE : TRUE ;
}

// Description:
// Restore from a Rogue Wave file.

void AccHxStat16::restoreGuts(RWFile& f)
{
  Accessor::restoreGuts(f) ;
  f >> thePart ;
}

// Description:
// Restore from a Rogue Wave virtual stream.

void AccHxStat16::restoreGuts(RWvistream& s)
{
  Accessor::restoreGuts(s) ;
  s >> thePart ;
}

// Description:
// Save to Rogue Wave file.

void AccHxStat16::saveGuts(RWFile& f) const
{
  Accessor::saveGuts(f) ;
  f << thePart ;
}

// Description:
// Save to Rogue Wave virtual stream.

void AccHxStat16::saveGuts(RWvostream& s) const
{
  Accessor::saveGuts(s) ;
  s << thePart ;
}

@

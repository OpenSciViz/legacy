head	3.3;
access
	rbarnett
	mmarlowe
	rbentley
	dhon
	ghuszar
	kmurphy
	soccm
	tgasaway;
symbols
	Build4_5_3:3.3
	Build4_1:3.2
	Build4:3.2
	Build3_3:3.2
	Current:3.2
	Build3_2:3.2
	Build3_1:3.1
	Build3:3.1
	Build2:2.2;
locks;
comment	@ * @;


3.3
date	95.11.03.15.14.32;	author rbarnett;	state Exp;
branches;
next	3.2;

3.2
date	94.07.22.19.31.46;	author rbentley;	state Exp;
branches;
next	3.1;

3.1
date	94.05.09.00.41.52;	author rbentley;	state Exp;
branches;
next	2.3;

2.3
date	94.01.13.21.19.05;	author rbarnett;	state Exp;
branches;
next	2.2;

2.2
date	93.12.01.12.35.55;	author rbentley;	state Release;
branches;
next	2.1;

2.1
date	93.08.09.13.09.51;	author rbarnett;	state Exp;
branches;
next	;


desc
@Creation
@


3.3
log
@Added functions for resolving the "next" partition.
@
text
@// Program Name: AccHxSc.h
// Subsystem   : HEXTE Data Management
// Programmer  : Randall D. Barnette -- Hughes STX
// Description :
//
//  This file is the declaration of class AccHxSc. It is a subclass of
//  Accessor to retrieve values from HEXTE Science partitions.
//
// RCS: $Id: AccHxSc.h,v 3.3 1995/11/01 18:46:53 soccm Exp $
//

//  Feature test switches
#ifndef ACCHXSC_H
#define ACCHXSC_H

// Local headers
#include "Accessor.h"
#include "HxScPart.h"

//
// Forward declaration
//
class HxScPart ;

//
// HEXTE Science Accessors are derived from Data Management Accessor class
//
class AccHxSc : public Accessor {

	//
	// Declare this class as a RogueWave Collectable. This macro declares some
	// functions necessary for the persistent object Factory
	//
	RWDECLARE_COLLECTABLE(AccHxSc)

public :

	//
	// Constructor for RogueWave use (don't use!)
	//
	AccHxSc() ;

	//
	// Constructor for public use. It takes a Partition reference. All further
	// access methods operate on the partition data.
	//
	AccHxSc(const HxScPart&) ;

	//
	// Destructor.
	//
	virtual ~AccHxSc(void) ;

	//
	// Return the index of the selection in the set of descriptors
	// -1 = Not Found
	//
	virtual int descriptorIndex(const Descriptor&) const ;

	//
	// Return true or false if aDescriptor exists in the set of descriptors
	//
	virtual int isAvailable(const Descriptor&) const ;

	//
	// This is here to blow away dynamically allocated memory for 
	// the static descriptor list. It is useful in searching for memory leaks.
	//
	static void destroyDescriptors(void) ;

  void setNextPartition(const HxScPart* next) ;
  void setNextPartition(const AccHxSc* next) ;
  unsigned long scienceMode(void) const ;

  int clusterPosition(void) const ;

	// -----------------------------------------
	// The following are inherited from Accessor
	// -----------------------------------------

	//
	// Returns data for the housekeeping object
	//
	virtual DataValue* access(const Descriptor&) const ;

	//
	// Return the apid of the stored packet
	//
	virtual int apid(void) const ;

	//
	// Return the time stamp of the stored packet
	//
	virtual unsigned long time(void) const ;

	//
	// Return the list of descriptors available to this Accessor
	//
	virtual const RWOrdered* descriptors(void) const ;

	//
	// Return the number of descriptors available to this Accessor
	//
	virtual int descriptorCount(void) const ;

	// ----------------------------------------------
	// The following are inherited from RWCollectable
	// ----------------------------------------------

  RWspace  binaryStoreSize(void) const ;
  int       compareTo(const RWCollectable*) const ;
  RWBoolean isEqual(const RWCollectable*) const ;
  void      restoreGuts(RWFile&) ;
  void      restoreGuts(RWvistream&) ;
  void      saveGuts(RWFile&) const ;
  void      saveGuts(RWvostream&) const ;

protected :

	//
	// Store the partition
	//
	HxScPart* thePartition ;
	const HxScPart* nextPartition ;

} ;

//
// Inline Functions
//

inline int AccHxSc::clusterPosition(void) const
{
  return nextPartition ? nextPartition->clusterPosition() : -1 ;
}

inline unsigned long AccHxSc::scienceMode(void) const
{
  return thePartition->scienceMode() ;
}

//
// Return the apid of the stored packet
//
inline int AccHxSc::apid(void) const
{
  return thePartition->applicationID() ;
}

//
// Return the time stamp of the stored packet
//
inline unsigned long AccHxSc::time(void) const
{
  return thePartition->seconds() ;
}

//
// Return the index of the selection in the set of descriptors // -1 = Not Found
//
inline int AccHxSc::descriptorIndex(const Descriptor&) const { return -1 ; }

//
// Return true or false if aDescriptor exists in the set of descriptors
//
inline int AccHxSc::isAvailable(const Descriptor&) const { return 0 ; }

//
// Return the list of descriptors available to this Accessor
//
inline const RWOrdered* AccHxSc::descriptors(void) const { return 0 ; }

//
// Return the number of descriptors available to this Accessor
//
inline int AccHxSc::descriptorCount(void) const { return 0 ; }

inline void AccHxSc::setNextPartition(const HxScPart* next)
{
  nextPartition = next ;
}

inline void AccHxSc::setNextPartition(const AccHxSc* next)
{
  nextPartition = next->thePartition ;
}

#endif ACCHXSC_H
@


3.2
log
@changed type of binaryStoreSize for RW 6
@
text
@d9 1
a9 1
// RCS: $Id: AccHxSc.h,v 3.1 1994/05/09 00:41:52 rbentley Exp $
a19 4
#if RWTOOLS < 0x600 & ! defined(RWspace)
#define RWspace unsigned
#endif

d71 6
d124 1
d132 10
d177 10
@


3.1
log
@Build 3
@
text
@d9 1
a9 1
// RCS: $Id: AccHxSc.h,v 2.3 1994/01/13 21:19:05 rbarnett Exp rbentley $
d20 4
d108 1
a108 1
  unsigned  binaryStoreSize(void) const ;
@


2.3
log
@Now has disposition handler code
@
text
@d9 1
a9 1
// RCS: $Id: AccHxSc.h,v 2.2 1993/12/01 12:35:55 rbentley Release rbarnett $
@


2.2
log
@Build 2 delivery
@
text
@d9 1
a9 1
// RCS: $Id: AccHxSc.h,v 2.2 1993/11/10 20:57:34 rbarnett Exp mmarlowe $
d52 1
a52 1
	virtual ~AccHxSc() ;
d58 1
a58 1
	virtual int descriptorIndex(Descriptor*) const ;
d63 1
a63 1
	virtual int isAvailable(Descriptor*) const ;
a64 1
#ifdef DEBUG
a69 1
#endif DEBUG
d78 1
a78 1
	virtual DataValue* access(Descriptor*) const ;
d93 1
a93 1
	virtual const Descriptor** descriptors(void) const ;
d144 1
a144 1
inline int AccHxSc::descriptorIndex(Descriptor*) const { return -1 ; }
d149 1
a149 1
inline int AccHxSc::isAvailable(Descriptor*) const { return 0 ; }
d154 1
a154 1
inline const Descriptor** AccHxSc::descriptors(void) const { return 0 ; }
@


2.1
log
@Creation
@
text
@d9 1
a9 1
// RCS: $Id$
a35 7
protected :

	//
	// Store the partition
	//
	HxScPart* thePartition ;

d47 1
a47 1
	AccHxSc( const HxScPart& ) ;
d58 1
a58 1
	virtual int descriptorIndex(Descriptor*) const { return -1 ; }
d63 1
a63 1
	virtual int isAvailable( Descriptor* ) const { return 0 ; }
d70 1
a70 1
	static void destroyDescriptors( void ) ;
d95 1
a95 1
	virtual const Descriptor** descriptors(void) const { return 0 ; }
d100 1
a100 1
	virtual int descriptorCount(void) const { return 0 ; }
d106 1
a106 1
  unsigned  binaryStoreSize() const ;
d114 7
d122 40
@

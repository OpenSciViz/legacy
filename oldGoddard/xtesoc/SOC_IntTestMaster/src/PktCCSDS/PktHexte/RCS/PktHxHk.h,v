head	3.2;
access
	rbarnett
	mmarlowe
	rbentley
	dhon
	ghuszar
	kmurphy
	soccm
	tgasaway;
symbols
	Build4_1:3.2
	Build4:3.1
	Build3_3:3.1
	Current:3.1
	Build3_2:3.1
	Build3_1:3.1
	Build3:3.1
	Build2:2.5;
locks;
comment	@ * @;


3.2
date	95.02.23.17.52.05;	author rbentley;	state Exp;
branches;
next	3.1;

3.1
date	94.05.09.00.41.52;	author rbentley;	state Exp;
branches;
next	2.10;

2.10
date	94.05.02.00.02.39;	author rbentley;	state Exp;
branches;
next	2.9;

2.9
date	94.04.21.14.28.02;	author rbarnett;	state Exp;
branches;
next	2.8;

2.8
date	94.04.14.16.20.39;	author mmarlowe;	state Exp;
branches;
next	2.7;

2.7
date	94.01.19.21.15.11;	author rbarnett;	state Exp;
branches;
next	2.6;

2.6
date	94.01.13.21.19.05;	author rbarnett;	state Exp;
branches;
next	2.5;

2.5
date	93.11.10.20.57.28;	author rbarnett;	state Release;
branches;
next	2.4;

2.4
date	93.10.19.15.24.55;	author rbarnett;	state Exp;
branches;
next	2.3;

2.3
date	93.10.11.16.39.28;	author rbarnett;	state Exp;
branches;
next	2.2;

2.2
date	93.10.01.12.58.48;	author rbarnett;	state Exp;
branches;
next	2.1;

2.1
date	93.09.30.20.46.08;	author rbarnett;	state Exp;
branches;
next	;


desc
@Creation
@


3.2
log
@revised isLast methods to use IDF number (derived from seconds() ) instead of
cal block sequence number because the seq num doesn't behave as advertised
(in Perkin-Elmer documentation) when there is no science data.
@
text
@// Project:	XTE SOC
// Name:	PktHxHk.h  
// Subsytem:	HEXTE Data Ingest and Management
// Programmers:	Matthew Marlowe, UCSD/CASS; Rich Bentley, UCSD/CASS
// Description:
//
//  This file is the definition of class PktHxHk.  It is used to ingest Hexte
//  Housekeeping Packets and build Hexte Housekeeping Partitions.
//
// RCS: $Id: PktHxHk.h,v 3.1 1994/05/09 00:41:52 rbentley Exp $
//

#ifndef PKTHXHK_H
#define PKTHXHK_H

//--------------------------------------------------
//  The classic DEC station line!
//--------------------------------------------------
#include <sys/types.h>

//-----------------------------------------------------
// Dependencies
//-----------------------------------------------------
#include "PktHexte.h"
//
// Number of bytes of housekeeping data per packet:
//
  enum { HXHKPKTSIZE=116 } ;

//
// Incomplete class declaration needed for the declaration of
// PcaHKPartCallback
//
class PktHxHk ;

//
// Create a short name for a pointer to a function that takes a
// constant pointer to a HEXTE Housekeeping packet and returns nothing.
//
typedef void (*PktHxHkCallback)(const PktHxHk*) ;

//-------------------------------------------------------
// Class declaration of PktHxHk
//-------------------------------------------------------

class PktHxHk : public PktHexte {

  RWDECLARE_COLLECTABLE(PktHxHk)

public:

  PktHxHk( void           );
  PktHxHk( unsigned char* );
  PktHxHk( Exemplar       );
 ~PktHxHk( void           );

  const unsigned char* statusData( void ) const ;

//
// Packet status queries
//
  int    isSeq16( int ) const;
  int    isSeq64( int, int  ) const;
  int    isSeq128( int, int  ) const;
  int    isSeq512( int, int  ) const;
  int    isFirst16sPartPkt( void  ) const;
  int    isFirst64sPartPkt( void  ) const;
  int    isFirst128sPartPkt( void  ) const;
  int    isFirst512sPartPkt( void  ) const;
  int    isLast16sPkt( void  ) const;
  int    isLast64sPkt( void  ) const;
  int    isLast128sPkt( void  ) const;
  int    isFirstCalPkt( void  ) const;	//Is this ever used anywhere??
  int    isLastCalPkt( void  ) const;

  unsigned int getIDFnum(  void ) const ; // Sequence number 0 only

//
// Redefined from PktTlm
//
  void    apply(void) const ;
  PktTlm* make(unsigned char*) ;

//
// The setDisposition method allows the user to supply a "callback" type
// function that gets called anytime a HEXTE Housekeeping packet is
// available. It returns the old callback function.
// The user-supplied function has the following signature
// void myHandler(const PktHxHk*) ;
// and it is passed to the system by:
// PktHxHkCallback oldFunction = PktHxHk::setDisposition(myHandler) ;
//
  static PktHxHkCallback setDisposition(PktHxHkCallback) ;

private:

//
// This is the partition handler callback function. It is static because
// it applies to the class. It's default value is zero in which case
// it does not get called.
// Typically, only the dispose method can call it, and setDisposition
// can set it.
// dispose is private and is only called by PktHxHk::apply().
// PktHxHk::apply() is public.
// setDisposition() is public.
//
  static PktHxHkCallback theHandler ;

//
// dispose checks the value of theHandler and calls it with
// argument "this" if it exists.
//
  void dispose(void) const ;

} ; // End PktHxHk Declaration

//
// Inline functions
//

inline const unsigned char * PktHxHk::statusData( void ) const
{
  return hexteData() ;
}

//
// Return the IDF number that is in Packets with sequence number 0
//
inline unsigned int PktHxHk::getIDFnum( void ) const
{
//
// This is dangerous. Can we reasonably expect an IDF number == 0?
//
  const unsigned char* c = hexteData() ;
  return hkPacketSeq() ? 0 : (c[2]<<24) | (c[3]<<16) | (c[0]<<8) | c[1] ;
}

inline int PktHxHk::isFirst16sPartPkt( void ) const
{
	const unsigned char* c = statusData() ;
  return ((0 == hkPacketSeq()) && (0 == c[47]) &&
	        (HxHk_ID_16_sec == (HxHkDataID) c[46])) ? 1 : 0 ;
}

inline int PktHxHk::isFirst64sPartPkt( void ) const
{
	const unsigned char* c = statusData() ;
  return ((0 == hkPacketSeq()) && (!(getIDFnum() & 0x3)) &&
                (0 == c[47]) &&
	        (HxHk_ID_16_sec == (HxHkDataID) c[46])) ? 1 : 0 ;
}

inline int PktHxHk::isFirst128sPartPkt( void ) const
{
	const unsigned char* c = statusData() ;
  return ((0 == hkPacketSeq()) && (!(getIDFnum() & 0x7)) &&
                (0 == c[47]) &&
	        (HxHk_ID_16_sec == (HxHkDataID) c[46])) ? 1 : 0 ;
}

inline int PktHxHk::isFirst512sPartPkt( void ) const
{
	const unsigned char* c = statusData() ;
  return ((0 == hkPacketSeq()) && (!(getIDFnum() & 0x1f)) &&
                (0 == c[47]) &&
	        (HxHk_ID_16_sec == (HxHkDataID) c[46])) ? 1 : 0 ;
}

inline int PktHxHk::isLast16sPkt( void ) const
{
	const unsigned char* c = statusData() ;
  return ((hkPacketSeq()     == 7           ) &&
          ((HxHkDataID)c[78] == HxHk_ID_Cal ) ) ? 1 : 0 ;
}

inline int PktHxHk::isFirstCalPkt( void ) const
{
	const unsigned char* c = statusData() ;
  return ((hkPacketSeq()     == 7           ) &&
          ((HxHkDataID)c[78] == HxHk_ID_Cal ) &&
          ((int) ((seconds()/16 - 1)&31) == 0           )) ? 1 : 0 ;
}

inline int PktHxHk::isLast64sPkt( void ) const
{
	const unsigned char* c = statusData() ;
  return ((hkPacketSeq()      == 7           ) &&
          ((HxHkDataID)c[78]  == HxHk_ID_Cal ) &&
          ((int) ((seconds()/16 - 1) & 3) == 3          )) ? 1 : 0 ;
}

inline int PktHxHk::isLast128sPkt( void ) const
{
	const unsigned char* c = statusData() ;
  return ((hkPacketSeq()     == 7           ) &&
          ((HxHkDataID)c[78] == HxHk_ID_Cal ) &&
          ((int) ((seconds()/16 - 1) & 7) == 7          )) ? 1 : 0 ;
}

inline int PktHxHk::isLastCalPkt( void ) const
{
	const unsigned char* c = statusData() ;
  return ((hkPacketSeq()     == 7           ) &&
          ((HxHkDataID)c[78] == HxHk_ID_Cal ) &&
          ((int) ((seconds()/16 - 1)&31) == 31          )) ? 1 : 0 ;
}

inline int PktHxHk::isSeq16(int s) const
{
	const unsigned char* c = statusData() ;
	return (s==hkPacketSeq()) ? 1 : 0 ;
}

inline int PktHxHk::isSeq64(int s, int b) const
{
	const unsigned char* c = statusData() ;
	return (s==hkPacketSeq()) ? (s==2) ? (b==(int) c[1]) ? 1 : 0 : 1 : 0 ;
}

inline int PktHxHk::isSeq128(int s, int b) const
{
	const unsigned char* c = statusData() ;
	return (s==hkPacketSeq()) ? (s==2) ? (b==(int) c[23]) ? 1 : 0 : 1 : 0 ;
}

inline int PktHxHk::isSeq512(int s, int b) const
{
	const unsigned char* c = statusData() ;
	return (s==hkPacketSeq()) ? (s==7) ? (b==(int) ((seconds()/16 - 1)&31)) ? 1 : 0 : 1 : 0 ;
}

//
// Partition handler code
//
inline PktHxHkCallback PktHxHk::setDisposition(PktHxHkCallback newHandler)
{
  PktHxHkCallback temp = theHandler ;
  theHandler = newHandler ;
  return temp ;
}

inline void PktHxHk::dispose(void) const
{
  if( theHandler ) theHandler(this) ;
}

#endif /* PKTHXHK_H */


@


3.1
log
@Build 3
@
text
@d10 1
a10 1
// RCS: $Id: PktHxHk.h,v 2.10 1994/05/02 00:02:39 rbentley Exp rbentley $
d181 1
a181 1
          ((int)       c[79] == 0           )) ? 1 : 0 ;
d189 1
a189 1
          ((int) (c[79] & 3) == 3          )) ? 1 : 0 ;
d197 1
a197 1
          ((int) (c[79] & 7) == 7          )) ? 1 : 0 ;
d205 1
a205 1
          ((int)       c[79] == 31          )) ? 1 : 0 ;
d229 1
a229 1
	return (s==hkPacketSeq()) ? (s==7) ? (b==(int) c[79]) ? 1 : 0 : 1 : 0 ;
@


2.10
log
@modified to build 16, 64, 128, & 512 second partitions
@
text
@d10 1
a10 1
// RCS: $Id: PktHxHk.h,v 2.7 1994/01/19 21:15:11 rbarnett Exp $
@


2.9
log
@Minor update
@
text
@a61 1
  int    isSeq( int, int  ) const;
d63 7
a69 1
  int    isFirstPartPkt( void  ) const;
d71 2
d138 1
a138 1
inline int PktHxHk::isFirstPartPkt( void ) const
d145 24
d184 16
d208 13
a220 1
inline int PktHxHk::isSeq(int s, int b) const
d223 1
a223 1
	return (s==hkPacketSeq()) ? (s==7) ? (b==(int) c[79]) ? 1 : 0 : 1 : 0 ;
d226 1
a226 1
inline int PktHxHk::isSeq16(int s) const
d229 1
a229 1
	return (s==hkPacketSeq()) ? 1 : 0 ;
@


2.8
log
@*** empty log message ***
@
text
@d63 1
a63 1
  int    isSeq16( int, int  ) const;
d167 1
a167 1
inline int PktHxHk::isSeq16(int s, int b) const
@


2.7
log
@removed getint
@
text
@d1 5
d7 2
a8 5
// File:       PktHxHk.h  
// Subsytem:   HEXTE Data Ingest and Management
// Programmer: Matthew Marlowe, UCSD/CASS
// Description:
//  Class Declaration of Hexte Housekeeping Packets
d10 1
a10 1
// RCS: $Id: PktHxHk.h,v 2.6 1994/01/13 21:19:05 rbarnett Exp $
d25 4
d57 1
a57 2
  const unsigned
	char* statusData( void ) const ;
d59 3
a61 3
	//
	// Packet status queries
	//
d63 4
a67 2
  int    isFirstCalPkt( void  ) const;
  int    isFirstPartPkt( void  ) const;
d71 3
a73 3
	//
	// Redefined from PktTlm
	//
d77 9
a85 9
  //
  // The setDisposition method allows the user to supply a "callback" type
  // function that gets called anytime a HEXTE Housekeeping packet is
  // available. It returns the old callback function.
  // The user-supplied function has the following signature
  // void myHandler(const PktHxHk*) ;
  // and it is passed to the system by:
  // PktHxHkCallback oldFunction = PktHxHk::setDisposition(myHandler) ;
  //
d90 10
a99 10
  //
  // This is the partition handler callback function. It is static because
  // it applies to the class. It's default value is zero in which case
  // it does not get called.
  // Typically, only the dispose method can call it, and setDisposition
  // can set it.
  // dispose is private and is only called by PktHxHk::apply().
  // PktHxHk::apply() is public.
  // setDisposition() is public.
  //
d102 4
a105 4
  //
  // dispose checks the value of theHandler and calls it with
  // argument "this" if it exists.
  //
d116 1
a116 1
	return hexteData() ;
d124 12
a135 6
  //
  // This is dangerous. Can we reasonably expect an IDF number == 0?
  //
	const unsigned char* c = hexteData() ;
  return hkPacketSeq() ? 0 :
		(c[2]<<24) | (c[3]<<16) | (c[0]<<8) | c[1] ;
d138 1
a138 1
inline int PktHxHk::isLastCalPkt( void ) const
d142 1
a142 2
          ((HxHkDataID)c[78] == HxHk_ID_Cal ) &&
          ((int)       c[79] == 31          )) ? 1 : 0 ;
d153 1
a153 1
inline int PktHxHk::isFirstPartPkt( void ) const
d156 3
a158 2
  return ((0 == hkPacketSeq()) && (0 == c[47]) &&
	        (HxHk_ID_16_sec == (HxHkDataID) c[46])) ? 1 : 0 ;
d166 6
@


2.6
log
@Now has disposition handler code
@
text
@d8 1
a8 1
// RCS: $Id: PktHxHk.h,v 2.5 1993/11/10 20:57:28 rbarnett Release $
a111 5
inline unsigned int getint( const unsigned char* c )
{
  return c[2]<<24 | c[3]<<16 | c[0]<<8 | c[1] ;
}

d120 3
a122 1
  return hkPacketSeq() ? 0 : getint( hexteData() ) ;
@


2.5
log
@*** empty log message ***
@
text
@a0 1
///////////////////////////////////////////////////
d2 5
a6 4
//  File:       PktHxSc.h  
//  Subsytem:   HEXTE Data Ingest and Management
//  Programmer: Matthew Marlowe, UCSD/CASS
//  Description:Class Declaration of Hexte Housekeeping Packets
d8 1
a8 1
//  RCS: $Id: PktHxHk.h,v 2.4 1993/10/19 15:24:55 rbarnett Exp rbarnett $
a9 1
///////////////////////////////////////////
d20 4
d25 2
a26 1
// Dependencies
d28 1
a28 4
//-----------------------------------------------------
#include <rw/collect.h>
#include <rw/ordcltn.h>
#include <rw/dlistcol.h>
d30 5
a34 1
#include "PktHexte.h"
d42 1
a42 1
  RWDECLARE_COLLECTABLE( PktHxHk )
d52 1
a52 1
	char * statusData( void ) const ;
d70 30
a99 1
protected:
d157 16
a172 1
#endif
@


2.4
log
@*** empty log message ***
@
text
@d8 1
a8 1
//  RCS: "$Id: PktHxHk.h,v 2.3 1993/10/11 16:39:28 rbarnett Exp rbarnett $"
a9 7
//Revision 1.3  1993/09/21  14:07:21  mmarlowe
//Preparing housekeeping source code for integration with
//hexte science packet classes
//
// * Revision 1.2  1993/07/28  18:36:40  mmarlowe
// * Put under Emacs RCS
// *
d62 2
a63 2
  void         apply( void );
  PktTlm     * make( unsigned char * );
@


2.3
log
@Update for FITS generation.
@
text
@a0 1

d8 1
a8 1
//  RCS: "$Id: PktHxHk.h,v 2.2 1993/10/01 12:58:48 rbarnett Exp $"
a73 7
	//
	// These are protected because they only apply to certain
	// PktHxHk instances.
	//
	int          blockSeqNr( void ) const ; // Sequence number 7 only
	HxHkDataID   calID(      void ) const ; // Sequence number 7 only

a84 10
inline int PktHxHk::blockSeqNr( void ) const
{
  return (int) ((unsigned char) *(statusData()+79)) ;
}

inline HxHkDataID PktHxHk::calID( void ) const
{
  return (HxHkDataID) *(statusData()+78) ;
}

d103 4
a106 3
  return ((hkPacketSeq()!= 7  ) ||
          (blockSeqNr() != 31 ) ||
          (calID()      != HxHk_ID_Cal ) ) ? 0 : 1 ;
d111 4
a114 3
  return ((hkPacketSeq()!= 7 ) ||
          (blockSeqNr() != 0 ) ||
          (calID()      != HxHk_ID_Cal ) ) ? 0 : 1 ;
d119 3
a121 3
  return ((hkPacketSeq()!= 0 ) ||
          (blockSeqNr() != 0 ) ||
          (calID()      != HxHk_ID_16_sec ) ) ? 0 : 1 ;
d126 2
a127 1
	return (s==hkPacketSeq()) ? (s==7) ? (b==blockSeqNr()) ? 1 : 0 : 1 : 0 ;
@


2.2
log
@removed Log
@
text
@d9 1
a9 1
//  RCS: "$Id: PktHxHk.h,v 2.1 1993/09/30 20:46:08 rbarnett Exp rbarnett $"
a19 1

a22 1

a27 7
//---------------------------------------------------
//  We've agreed to include a file specifying HEXTE 
//  rogue wave id numbers for persistance
//---------------------------------------------------

#include "hx_ids.h"

a32 2
#include "PktCCSDS.h"
#include "PktTlm.h"
a33 1
#include "PktHexte.h"
d37 2
a42 2
typedef enum { INCR, SYNC } SYNC_STATE;

d49 4
a52 4
  PktHxHk( void                   );
  PktHxHk( unsigned char * buffer );
  PktHxHk( Exemplar               );
 ~PktHxHk( void                   );
d55 10
a64 4
	char * statusData( void ) const { return hexteData() ; }
  int    getSeqNum( void  ) const;
  int    LstCalPkt( void  ) const;
  int    FstCalPkt( void  ) const;
a65 1
	HxHkDataID   calID( void ) const ; // Sequence number 7 only
d80 27
d108 36
a143 1
};
@


2.1
log
@Creation
@
text
@d9 1
a9 1
//  RCS: "$Id: PktHxHk.h,v 1.3 1993/09/21 14:07:21 mmarlowe Exp $"
a10 1
//        $Log: PktHxHk.h,v $
@

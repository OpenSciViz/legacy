head	3.11;
access
	dhon
	rbentley
	rbarnett
	mmarlowe
	ghuszar
	kmurphy
	soccm
	tgasaway;
symbols
	Build5_0_2:3.11
	Build4_3:3.10
	Build4_2:3.9
	Build4_1:3.7
	Build4:3.5
	Build3_3:3.3
	Current:3.3
	Build3_2:3.3
	Build3_1:3.2
	Build3:3.1;
locks;
comment	@ * @;


3.11
date	96.01.26.22.37.02;	author rbentley;	state Exp;
branches;
next	3.10;

3.10
date	95.04.20.22.00.56;	author rbentley;	state Exp;
branches;
next	3.9;

3.9
date	95.03.25.01.05.24;	author rbentley;	state Exp;
branches;
next	3.8;

3.8
date	95.03.18.01.04.21;	author rbentley;	state Exp;
branches;
next	3.7;

3.7
date	95.02.24.23.34.15;	author rbentley;	state Exp;
branches;
next	3.6;

3.6
date	95.02.23.18.00.11;	author rbentley;	state Exp;
branches;
next	3.5;

3.5
date	95.01.26.01.42.17;	author rbentley;	state Exp;
branches;
next	3.4;

3.4
date	95.01.09.20.31.52;	author rbentley;	state Exp;
branches;
next	3.3;

3.3
date	94.09.20.15.07.41;	author rbentley;	state Exp;
branches;
next	3.2;

3.2
date	94.07.01.17.47.00;	author rbentley;	state Exp;
branches;
next	3.1;

3.1
date	94.05.09.00.41.52;	author rbentley;	state Exp;
branches;
next	1.3;

1.3
date	94.05.01.22.26.32;	author rbentley;	state Exp;
branches;
next	1.2;

1.2
date	94.04.25.01.05.56;	author mmarlowe;	state Exp;
branches;
next	1.1;

1.1
date	94.04.21.14.11.35;	author rbarnett;	state Exp;
branches;
next	;


desc
@512 second partition class
@


3.11
log
@added break statement to end of conversion algorithm 22 case
@
text
@// Project:	XTE SOC
// Name: 	HxHk512Part.C
// Subsystem:	HEXTE Data Management
// Programmers:
//		Randall D. Barnette, Hughes STX, rbarnett@@xema.stx.com
//		Rich Bentley,        UCSD,       rbentley@@cass140.ucsd.edu
//		Matthew Marlowe,     UCSD,       mmarlowe@@cass140.ucsd.edu
// Description: Implementation of Hexte Housekeeping 512-second Partition Class
//
//  RCS stamp  :
static const char rcsid[] =
"$Id: HxHk512Part.C,v 3.10 1995/04/20 22:00:56 rbentley Exp $" ;
//

#include <stdlib.h>
#include <string.h>
#include <iostream.h>
#include <iomanip.h>
#include <assert.h>
#include <rw/collstr.h>
#include <ctype.h>
#include <math.h>

#include "DataType.h"
#include "HxHkPart.h"
#include "HxHk512Part.h"
#include <HexDefs.h>
#include "hxPkts_ids.h"

//
// Declare the static (class) variable. This is a pointer to the function
// that gets called by PktHxHk::apply when a partition is available.
// The default is zero. Thus, the default is to do nothing.
//
HxHk512PartCallback HxHk512Part::theHandler = 0 ;

RWDEFINE_COLLECTABLE( HxHk512Part, HxHk512Part_ID )

// rogue wave constructor

HxHk512Part::HxHk512Part(void) {}

// Destructor
HxHk512Part::~HxHk512Part(void)
{
  delete [] pIDFStatus ;
}

// Description:
//  Constructor - Puts first packet into partition
//                Creates IDF Header
//
HxHk512Part::HxHk512Part(const PktHxHk* aPacket)
{
//cerr << "HxHk512Part 1st pkt constructor" << endl;
//
// Initialize the private data from CCSDS functions.
// Establishes data necessary to differentiate data sets.
//
  ApId = aPacket->applicationID() ;
  packetTime = aPacket->seconds() ;
  size = HXHK512SIZE ;
  blockCount = 0 ;

#ifdef DEBUG
  cerr << "Packet Time is: " << packetTime << endl
       << "ApID is:        " << ApId       << endl ;
#endif /* DEBUG */

// Allocate space for the partition data:
//
  pIDFStatus = new unsigned char[HXHK512SIZE] ;

// copy the data.
//
  memcpy( pIDFStatus, aPacket->statusData(), HXHKPKTSIZE ) ;
  byteSwap(pIDFStatus ) ; // put bytes back in pre-1773 order
  return ;
}

// Description:
// Copy Constructor 
//
HxHk512Part::HxHk512Part( const HxHk512Part& rhs )
{
#ifdef DEBUG
  cerr << "Copy Time is: " << rhs.packetTime << endl
       << "ApID is:      " << rhs.ApId       << endl ;
#endif /* DEBUG */
//
// Copy all data members to new object
//
  ApId = rhs.ApId ;
  packetTime = rhs.packetTime ;
  size = rhs.size ;
  blockCount = rhs.blockCount ;

// Allocate space for the partition data:
//
  pIDFStatus = new unsigned char[HXHK512SIZE] ;

//
// Copy the Packets in the list
//
  memcpy( pIDFStatus, rhs.pIDFStatus, HXHK512SIZE ) ;
  return ;
}

//
// Description:
// Get data corresponding to 'mnemonic', set 'elements' to number of elements
// for this item in the partition, and set 'type' to the appropriate type code,
// as defined in an enum in HxHkPart.h.  All data with type code of UCHAR,
// USHORT, or UINT are returned as unsigned ints, but type can be used by
// calling routines such as AccHxHk for conversion.  Items with data type code
// of DOUBLE are returned as double.
// NOTE: CALLING PROGRAM IS RESPONSIBLE FOR DELETING SPACE RETURNED AS POINTER
//
void*
HxHk512Part::getStatData(char* mnemonic, short& elements, short& type ) const
{
  void * p = NULL ;
  unsigned * pui = NULL ;
  double * pd = NULL ;
  static int repetitions[4] = { 32, 8, 4, 1 } ;   // indexed by subcom cycle code
  int mult=0, ndx1, ndx2, ndx2Init, ndx1Init, ndx1Incr ;
  int i, j, k, n=0 ;
  int numBytes ;

// The following must be modified after Matt adds the mnemonic checking class:
//  if (theCmdItem.checkMnemonic( (const) mnemonic) == NULL){
//    cerr << "mnemonic " << mnemonic << " not found in table" << endl ;
//    cerr.flush() ;
//    return NULL ;
//  }

#ifdef MNEM_DEBUG
  cerr << "HxHk512Part::getStatData> mnemonic = #" << mnemonic << "#" << endl;
  cerr.flush() ;
#endif

  const HxTbItem & item = theCmdItem.getTbItem( (const char *) mnemonic) ;

#ifdef DEBUG9
  item.printLong(cout) ;
#endif /* DEBUG9 */

  TableEntry entry ;
  item.get(entry) ;	// HxCmdItem data as a TableEntry structure
//
// Determine number of elements and type, and allocate memory for data
//
  mult = entry.multiplicity ? entry.multiplicity : 1  ;
  elements = mult * repetitions[entry.subcomCycle] ;
  numBytes = entry.numBytes + (entry.numBits ? 1 : 0) ;

#ifdef DEBUG
  assert ( numBytes <= 4) ;
#endif /* DEBUG */

  if (entry.conversionAvail) {
    type = BaseType_DOUBLE ;
  }
  else {
    switch (numBytes) {
    case 1:
      type = BaseType_UCHAR ;
      break ;
    case 2:
      type = BaseType_USHORT ;
      break ;
    case 3:
    case 4:
      type = BaseType_UINT ;
      break ;
    }
  }
  pui = new unsigned int[elements] ;
  for (i=0; i<elements; i++) *(pui+i) = 0 ;

//
// Initialize the indices to the status data array
//

  ndx1Init = entry.seqNumInCycle ;
  ndx1Incr = 32 / repetitions[entry.subcomCycle] ;
  ndx2Init = (entry.packSeqNum*HXHKPKTSIZE) + entry.offsetBytes ;

  for (i=0; i<repetitions[entry.subcomCycle]; i++) {	// for each repetition
    ndx1 = ndx1Init ;
    ndx2 = ndx2Init ;
    for (j=0; j<mult; j++) {		// for each element in the repetition
      if (entry.numBytes) {

#ifdef DEBUG
        cerr << "getting IDFStatus[" << ndx1 << "][" << ndx2 << "]" << endl ;
        cerr.flush() ;
#endif /* DEBUG */

        pui[n] = *(pIDFStatus+(HXHKBLKSIZE*ndx1)+ndx2) ;	// get LSB
        for (k=1; k<(int)entry.numBytes; k++) {    // higher order bytes (if any)
          switch (ndx2) {
          case END64S : case END128S : case ENDARCH : case ENDCAL :
            ndx2 = ndx2PP(ndx2) ;
            ndx1++ ;
            break ;
          default :
            ndx2++ ;
            break ;
          }

#ifdef DEBUG
          cerr << "getting IDFStatus[" << ndx1 << "][" << ndx2 << "]" << endl ;
          cerr.flush() ;
#endif /* DEBUG */

          pui[n] = pui[n] | *(pIDFStatus+(HXHKBLKSIZE*ndx1)+ndx2) << 8*k ;
        }
        if (entry.numBits) {	// Get any remaining high-order bits
          switch (ndx2) {
          case END64S : case END128S : case ENDARCH : case ENDCAL :
            ndx2 = ndx2PP(ndx2) ;
            ndx1++ ;
            break ;
          default :
            ndx2++ ;
            break ;
          }

#ifdef DEBUG
          cerr << "getting IDFStatus[" << ndx1 << "][" << ndx2 << "]" << endl ;
          cerr.flush() ;
#endif /* DEBUG */

          pui[n] = pui[n] | ( ((*(pIDFStatus+(HXHKBLKSIZE*ndx1)+ndx2)
                            >> entry.offsetBits) &
                            ((1<<entry.numBits)-1) ) << 8*k ) ;
        }
      }
      else {		// no whole bytes, just some bits

#ifdef DEBUG
        cerr << "getting IDFStatus[" << ndx1 << "][" << ndx2 << "]" << endl ;
        cerr.flush() ;
#endif /* DEBUG */

        pui[n]=(*(pIDFStatus+(HXHKBLKSIZE*ndx1)+ndx2)
               >> entry.offsetBits) & ((1<<entry.numBits)-1);
      }
      switch (ndx2) {	// bump to start of next element
      case END64S : case END128S : case ENDARCH : case ENDCAL :
        ndx2 = ndx2PP(ndx2) ;
        ndx1++ ;
        break ;
      default :
        ndx2++ ;
        break ;
      } 
      n++ ;
    }		// end for each element
    ndx1Init += ndx1Incr ;
  }		// end for each repetition

  if (entry.conversionAvail)
    p = convertToEngUnits(mnemonic, entry.conversionAvail, pui, elements) ; 
  else
    p = (void*) pui ;

  return p ;
}
//
// Description:
// Convert from raw telemetry units to physical engineering units.
//  
void* HxHk512Part::convertToEngUnits(char * mnemonic, unsigned short convAlg,
                                  unsigned int* pui, short elements) const
{
  double * pd = new double[elements] ;
  register int i ;
  unsigned int* pui2 ;
  unsigned int* pui3 ;
  short elt2, type2 ;
  static char gndPmeDetTu[] = "gndPmeDet _TU";
  static char gainDet[] = "gainDet " ;

  switch (convAlg) {
  case 1:			// angleApMod
    for (i=0; i<elements; i++)
      pd[i] = ( (double) pui[i] * 360) / 8192 ;
    break ;

  case 2:			// lldDetn
    {
    double AgcDacRel ;
    gndPmeDetTu[9] = mnemonic[strlen(mnemonic)-1] ;
    gainDet[7] = gndPmeDetTu[9] ;
    pui2 = (unsigned int *) getStatData(gndPmeDetTu, elt2, type2) ;
    pui3 = (unsigned int *) getStatData(gainDet, elt2, type2) ;
    for (i=0; i<elements; i++) {
      AgcDacRel = (double)( (int)(pui3[i]-pui2[i]-726) ) ;
      AgcDacRel = 1.0 + AgcDacRel/(182.0/.25) ;
      pd[i] = (double)( (int)( (pui[i]<<2) - pui2[i] ) )  ;
      pd[i] = pd[i] / AgcDacRel - 73 ;
      pd[i] = pd[i] / (653.0/45.0) + 5 ;
    }
    delete []pui2 ;
    delete []pui3 ;
    break ;
    }

  case 3:			// usdDetn, lsdDetn
    gndPmeDetTu[9] = mnemonic[strlen(mnemonic)-1] ;
    pui2 = (unsigned int *) getStatData(gndPmeDetTu, elt2, type2) ;
    for (i=0; i<elements; i++)
      pd[i] = (100 + ((double) ((int) ((pui[i]<<2) - pui2[i] )) * 700) / 757 );
    delete []pui2 ;
    break ;

  case 4:			// lldGcDetn
    pui2 = (unsigned int *) getStatData("gndMse_TU", elt2, type2) ;
    for (i=0; i<elements; i++)
      pd[i] = (1 + ((double) ((int) ((pui[i]<<2) - pui2[i] - 18)) * 51) / 890 );
    delete []pui2 ;
    break ;

  case 5:			// lldShdn
    pui2 = (unsigned int *) getStatData("gndMse_TU", elt2, type2) ;
    for (i=0; i<elements; i++)
      pd[i] = (1 + ((double) ((int) ((pui[i]<<2) - pui2[i] - 30)) * 29) / 878 );
    delete []pui2 ;
    break ;

  case 6:			// lldPmon
    pui2 = (unsigned int *) getStatData("gndMse_TU", elt2, type2) ;
    for (i=0; i<elements; i++)
      pd[i] = (21 + ((double) ((int) ((pui[i]<<2)-pui2[i] - 35)) * 529) / 873 );
    delete []pui2 ;
    break ;

  case 7:			// hvDetn
    gndPmeDetTu[9] = mnemonic[strlen(mnemonic)-1] ;
    pui2 = (unsigned int *) getStatData(gndPmeDetTu, elt2, type2) ;
    for (i=0; i<elements; i++)
      pd[i] = ((double) ((int) (pui[i] - pui2[i]) ) * 1250) / 908 ;
    delete []pui2 ;
    break ;

  case 8:			// hvGcDetn
    pui2 = (unsigned int *) getStatData("gndMse_TU", elt2, type2) ;
    for (i=0; i<elements; i++)
      pd[i] = ((double) ((int) ((pui[i]<<2) - pui2[i])) * 1250) / 908 ;
    delete []pui2 ;
    break ;

  case 9:			// tmps
    {
    static double a[6] = {20.97888, -3.3995, 0.106, -0.0133, 0.00157, -0.000078} ;
    double x, powx ;
    int j ;
    pui2 = (unsigned int *) getStatData("tmpRef", elt2, type2) ;
    for (i=0; i<elements; i++) {
      pd[i] = (double) ((int) (pui[i] - *pui2) ) ;
//      if (pd[i] > 119)
      x = 0.1 * pd[i] -1.11549 ;
      powx = 1.0 ;
      pd[i] = a[0] ;
      for (j=1; j<6 ; j++) {
         powx = powx * x ;
         pd[i] += a[j] * powx ;
      }
    }
    delete []pui2 ;
    break ;
    }

  case 10:			// vp2.5Mse
    pui2 = (unsigned int *) getStatData("vp10RtnMse", elt2, type2) ;
    for (i=0; i<elements; i++)
      pd[i] = ((double) ((int) ((pui[i]<<2) - pui2[i])) ) / 302.7 ;
    delete []pui2 ;
    break ;

  case 11:			// vp10Mse
    pui2 = (unsigned int *) getStatData("vp10RtnMse", elt2, type2) ;
    for (i=0; i<elements; i++)
      pd[i] = ((double) ((int) ((pui[i]<<2) - pui2[i])) ) / 53.2 ;
    delete []pui2 ;
    break ;

  case 12:			// vp5Mse
    pui2 = (unsigned int *) getStatData("vp10RtnMse", elt2, type2) ;
    for (i=0; i<elements; i++)
      pd[i] = ((double) ((int) ((pui[i]<<2) - pui2[i])) ) / 136.8 ;
    delete []pui2 ;
    break ;

  case 13:			// vp10m5Mse
    pui2 = (unsigned int *) getStatData("vp10RtnMse", elt2, type2) ;
    pui3 = (unsigned int *) getStatData("vp10Mse_TU", elt2, type2) ;
    for (i=0; i<elements; i++)
      pd[i] = ((double) ((int) ((pui[i]<<2) - pui2[i])) ) / 165.8
            - ((double) ((int) ((pui3[i]<<2) - pui2[i])) ) / 64.3 ;
    delete []pui2 ;
    delete []pui3 ;
    break ;

  case 14:			// vpm10Mse
    pui2 = (unsigned int *) getStatData("vp10RtnMse", elt2, type2) ;
    pui3 = (unsigned int *) getStatData("vp10Mse_TU", elt2, type2) ;
    for (i=0; i<elements; i++)
      pd[i] = ((double) ((int) ((pui[i]<<2) - pui2[i])) ) / 136.8
            - ((double) ((int) ((pui3[i]<<2) - pui2[i])) ) / 43.9 ;
    delete []pui2 ;
    delete []pui3 ;
    break ;

  case 15:			// v37Mse
    pui2 = (unsigned int *) getStatData("vp10RtnMse", elt2, type2) ;
    for (i=0; i<elements; i++)
      pd[i] = ((double) ((int) ((pui[i]<<2) - pui2[i])) ) / 17.66 ;
    delete []pui2 ;
    break ;

  case 16:			// v37Pme
    pui2 = (unsigned int *) getStatData("vp10RtnMse", elt2, type2) ;
    for (i=0; i<elements; i++)
      pd[i] = 0.5 + ((double) ((int) ((pui[i]<<2) - pui2[i])) ) / 16.66 ;
    delete []pui2 ;
    break ;

  case 17:			// vpm10Pme
    pui2 = (unsigned int *) getStatData("vRtnPme", elt2, type2) ;
    pui3 = (unsigned int *) getStatData("vp10Pme_TU", elt2, type2) ;
    for (i=0; i<elements; i++)
      pd[i] = ((double) ((int) ((pui[i]<<2) - pui2[i])) ) / 136.8
            - ((double) ((int) ((pui3[i]<<2) - pui2[i])) ) / 43.9 - 0.5 ;
    delete []pui2 ;
    delete []pui3 ;
    break ;

  case 18:			// vpm5Pme
    pui2 = (unsigned int *) getStatData("vRtnPme", elt2, type2) ;
    pui3 = (unsigned int *) getStatData("vp10Pme_TU", elt2, type2) ;
    for (i=0; i<elements; i++)
      pd[i] = ((double) ((int) ((pui[i]<<2) - pui2[i])) ) / 165.8
            - ((double) ((int) ((pui3[i]<<2) - pui2[i])) ) / 64.3 - 0.5 ;
    delete []pui2 ;
    delete []pui3 ;
    break ;

  case 19:			// vp10Pme
    pui2 = (unsigned int *) getStatData("vRtnPme", elt2, type2) ;
    for (i=0; i<elements; i++)
      pd[i] = 0.5 + ((double) ((int) ((pui[i]<<2) - pui2[i])) ) / 53.2 ;
    delete []pui2 ;
    break ;

  case 20:			// vp5Detn
    pui2 = (unsigned int *) getStatData("vRtnPme", elt2, type2) ;
    for (i=0; i<elements; i++)
      pd[i] = ((double) ((int) ((pui[i]<<2) - pui2[i])) ) / 136.8 ;
    delete []pui2 ;
    break ;

  case 21:			// ampsLvps[A,B]
    pui2 = (unsigned int *) getStatData("vRtnPme", elt2, type2) ;
    for (i=0; i<elements; i++)
      pd[i] = ((double) ((int) ((pui[i]<<2) - pui2[i])) ) / 431 ;
    delete []pui2 ;
    break ;

  case 22:			//gndPmeDetn & gndMse; alg. from G. Huszar
    for (i=0; i<elements; i++) {
      pd[i] = ( (double) (pui[i]/302.7) ) ;
    }
    break ;

  default:
      cerr << "HxHk512Part::convertToEngUnits received invalid conversion code: "
           << convAlg << endl ;
      break ;
  }
  delete []pui ;
  return (void*) pd ;
}

// Description:
// Compare times of this partition to times of partition passed as argument.
// Return -1 if this one is earlier, 0 if they are the same, and 1 if this 
// partition is later.
int HxHk512Part::compareTo( const RWCollectable * target ) const
{
	if( target->isA() != HxHk512Part_ID ) return -1 ;
  const HxHk512Part * t = ( const HxHk512Part* ) target ;
  unsigned long t1 = seconds();
  unsigned long t2 = t->seconds();
  if ( t1== t2 ) return 0;
  return t1 > t2 ? 1 : -1 ; 
}

// Description:
// Compare times of this partition to times of partition passed as argument.
// Return 1 if equal, 0 if not.
RWBoolean HxHk512Part::isEqual( const RWCollectable * target ) const
{
  return compareTo(target) ? 0 : 1 ;
}

@


3.10
log
@corrected conv. alg. 2
@
text
@d12 1
a12 1
"$Id: HxHk512Part.C,v 3.9 1995/03/25 01:05:24 rbentley Exp $" ;
d476 1
@


3.9
log
@fixed conversion algorithm 22
@
text
@d12 1
a12 1
"$Id: HxHk512Part.C,v 3.3 1994/09/20 15:07:41 rbentley Exp $" ;
d300 5
a304 3
      AgcDacRel = (1.0 + ((double)((int)(pui3[i]-pui2[i]-545)) * 0.5) / 363 ) ;
      pd[i] = (((double) ((int) ((pui[i]<<2) - pui2[i] ))) / AgcDacRel) - 73 ;
      pd[i] = pd[i] * (45.0/653) + 5 ;
@


3.8
log
@added conv. alg. 22; fixed temperature conversion
@
text
@a352 5
  case 22:			//gndPmeDetn & gndMse; alg. from G. Huszar
    for (i=0; i<elements; i++) {
      pui[i] = (pui[i] >> 2) & 255;   // make it 8 bits so it works with tmpRef
    }
    // fall through to the temp conversion alg.
d470 5
@


3.7
log
@changed mnemonic gndMse to gndMse_TU and name gndPmeDet to gndPmeDetTu
@
text
@d137 5
d353 5
d365 1
a365 1
      pd[i] = (double) ((int) ((pui[i] - pui2[i])<<2) ) ;
@


3.6
log
@changed item->printLong(cout) to item.printLong(cout) in debug code
@
text
@d278 1
a278 1
  static char gndPmeDet[] = "gndPmeDet ";
d290 3
a292 3
    gndPmeDet[9] = mnemonic[strlen(mnemonic)-1] ;
    gainDet[7] = gndPmeDet[9] ;
    pui2 = (unsigned int *) getStatData(gndPmeDet, elt2, type2) ;
d305 2
a306 2
    gndPmeDet[9] = mnemonic[strlen(mnemonic)-1] ;
    pui2 = (unsigned int *) getStatData(gndPmeDet, elt2, type2) ;
d313 1
a313 1
    pui2 = (unsigned int *) getStatData("gndMse", elt2, type2) ;
d320 1
a320 1
    pui2 = (unsigned int *) getStatData("gndMse", elt2, type2) ;
d327 1
a327 1
    pui2 = (unsigned int *) getStatData("gndMse", elt2, type2) ;
d334 2
a335 2
    gndPmeDet[9] = mnemonic[strlen(mnemonic)-1] ;
    pui2 = (unsigned int *) getStatData(gndPmeDet, elt2, type2) ;
d342 1
a342 1
    pui2 = (unsigned int *) getStatData("gndMse", elt2, type2) ;
@


3.5
log
@changed compareTo & isEqual() to fix bug
@
text
@d139 3
a141 3
#ifdef DEBUG
  item->printLong(cout) ;
#endif /* DEBUG */
@


3.4
log
@modified to use HxCmdItem
@
text
@d473 23
@


3.3
log
@fixed bugs in conversion algorithms 20 & 21
@
text
@d12 1
a12 1
"$Id: HxHk512Part.C,v 3.2 1994/07/01 17:47:00 rbentley Exp $" ;
d27 1
a27 3
#include "HexDefs.h"
#include "HxHkItem.h"
#include "HxHkTable.h"
d130 6
a135 15
  if (itemTable == NULL)
    makeItemTable();
  char mnem[40] ;
  for (i=0; i<40; i++){
    mnem[i] = mnemonic[i] ;
    if (mnemonic[i] == '\0')
       break ;
  }

  HxHkItem * item = itemTable->lookup(mnem) ;
  if (item == NULL){	// nothing found for that mnemonic
    cerr << "mnemonic " << mnemonic << " not found in table" << endl ;
    cerr.flush() ;
    return NULL ;
  }
d137 2
d144 1
a144 1
  item->get(entry) ;	// HxHkItem data as a TableEntry structure
@


3.2
log
@changed hx_ids.h to hxPkts_ids.h
@
text
@d12 1
a12 1
"$Id: HxHk512Part.C,v 3.1 1994/05/09 00:41:52 rbentley Exp $" ;
d463 1
a463 1
      pd[i] = 0.5 + ((double) ((int) ((pui[i]<<2) - pui2[i])) ) / 136.8 ;
d470 1
a470 1
      pd[i] = 0.5 + ((double) ((int) ((pui[i]<<2) - pui2[i])) ) / 431 ;
@


3.1
log
@Build 3
@
text
@d12 1
a12 1
"$Id: HxHk512Part.C,v 1.3 1994/05/01 22:26:32 rbentley Exp rbentley $" ;
d30 1
a30 1
#include "hx_ids.h"
@


1.3
log
@Modified format to improve genman output
@
text
@d12 1
a12 1
"$Id: HxHk512Part.C,v 2.7 1994/04/05 23:06:06 rbentley Exp rbentley $" ;
@


1.2
log
@*** empty log message ***
@
text
@d121 2
a122 1
void* HxHk512Part::getStatData(char* mnemonic, short& elements, short& type ) const
@


1.1
log
@Initial revision
@
text
@d24 1
d165 1
a165 1
    type = DOUBLE ;
d170 1
a170 1
      type = UCHAR ;
d173 1
a173 1
      type = USHORT ;
d177 1
a177 1
      type = UINT ;
@

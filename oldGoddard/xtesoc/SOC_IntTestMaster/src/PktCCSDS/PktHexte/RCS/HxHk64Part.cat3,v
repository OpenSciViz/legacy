head	3.1;
access
	rbarnett
	mmarlowe
	dhon
	rbentley
	ghuszar
	kmurphy
	soccm
	tgasaway;
symbols
	Build4_1:3.1
	Build4:3.1
	Build3_3:3.1
	Current:3.1
	Build3_2:3.1
	Build3_1:3.1
	Build3:3.1;
locks;
comment	@# @;


3.1
date	94.05.09.00.41.52;	author rbentley;	state Exp;
branches;
next	1.1;

1.1
date	94.05.01.22.29.22;	author rbentley;	state Exp;
branches;
next	;


desc
@@


3.1
log
@Build 3
@
text
@
HXHK64PART.H(subclass)HEXTE HOUSEKEEPING 64-SECOND PARTITION HXHK64PART.H(subclass)

                                                                 Apr 24 11:56
NAME
    HxHk64Part.h - Hexte Housekeeping 64-second Partition class

SYNOPSIS
    #include <HxHk64Part.h>

    class HxHk64Part ;

        Public members
              
               Default constructor for RogueWave use
              
            HxHk64Part(void);
              
               Construct a partition from the first packet in a partition.
              
            HxHk64Part(const PktHxHk*);
              
               Copy Constructor
              
            HxHk64Part(const HxHk64Part&);
              
               Destructor
              
            ~HxHk64Part(void);
              
               Get data for a single housekeeping item:
              
            void* getStatData(char* mnemonic, short& elements, short& type) const ;
              
               Convert data for a telemetry data for a single housekeeping item to
               engineering units:
              
            void* convertToEngUnits(char * mnemonic, unsigned short convAlg,
            unsigned int* pui, short elements) const ;
              
               The setDisposition method allows the user to supply a "callback"
               type function that gets called anytime a HEXTE Status partition
               is available. It returns the old callback function.
               The user-supplied function has the following signature
               void myHandler(const HxHk64Part*) ;
               and it is passed to the system by:
               HxHk64PartCallback
                  oldFunction = HxHk64Part::setDisposition(myHandler) ;
              
            static HxHk64PartCallback setDisposition(HxHk64PartCallback) ;
              
               This is the partition handler callback function. It is static because
               it applies to the class. It's default value is zero in which case
               it does not get called.
               Typically, only the dispose method can call it, and setDisposition
               can set it.
               dispose is private and is only called by PktHxHk::apply().
               PktHxHk::apply() is public and is a friend function.
               setDisposition() is public.
              
            static HxHk64PartCallback theHandler ;
              
               dispose checks the value of theHandler and calls it with
               argument "this" if it exists.
              
            void dispose(void) const ;
              
               Allow PktHxHk::apply() to call dispose.
              
            friend void PktHxHk::apply(void) const ;

AUTHOR
    Rich Bentley,        UCSD,       rbentley@@cass140.ucsd.edu
    

DESCRIPTION
    This class defines a Hexte Housekeeping 64-second partition and provides
    methods for accessing its data.  Only housekeeping items which are
    subcommed every 16 or 64 seconds can be reliably obtained from one of these
    partitions, but items with longer subcom cycles can be obtained if they
    are available.
    
    Subsystem:  Hexte Data Management

DEFINED MACROS
    HXHK64PART_H

INCLUDED FILES
    <rw/hashdict.h>
    "HxHkPart.h"
    "PktHxHk.h"
    <rw/collstr.h>

SOURCE FILES
    HxHk64Part.h
    HxHk64Part.C

SUMMARY
    HxHk64Part::HxHk64Part(const PktHxHk* aPacket)
        Constructor - Puts first packet into partition
        Creates IDF Header
        

    HxHk64Part::HxHk64Part( const HxHk64Part& rhs )
        Copy Constructor 
        

    void*
    HxHk64Part::getStatData(char* mnemonic, short& elements, short& type ) const
        Get data corresponding to 'mnemonic', set 'elements' to number of elements
        for this item in the partition, and set 'type' to the appropriate type code,
        as defined in an enum in HxHkPart.h.  All data with type code of UCHAR,
        USHORT, or UINT are returned as unsigned ints, but type can be used by
        calling routines such as AccHxHk for conversion.  Items with data type code
        of DOUBLE are returned as double.
        NOTE: CALLING PROGRAM IS RESPONSIBLE FOR DELETING SPACE RETURNED AS POINTER
        

    void* HxHk64Part::convertToEngUnits(char * mnemonic, unsigned short convAlg,
                                      unsigned int* pui, short elements) const
        Convert from raw telemetry units to physical engineering units.
        
@


1.1
log
@Initial revision
@
text
@@

head	4.2;
access
	soccm;
symbols
	Build4_5_3:4.1;
locks;
comment	@ * @;


4.2
date	97.03.20.22.43.38;	author soccm;	state Exp;
branches;
next	4.1;

4.1
date	95.11.03.14.46.55;	author rbarnett;	state Exp;
branches;
next	;


desc
@Accessor for HEXTE Archive data
@


4.2
log
@removed set live times
@
text
@// ==========================================================================
// File Name   : AccHxArch.h
// Subsystem   : HEXTE Data Management
// Programmer  : Randall D. Barnette, Hughes STX <rbarnett@@xema.stx.com>
// Description : Declaration of HEXTE Archive data
//
// .NAME    AccHxArch - HEXTE Archive data accessor
// .LIBRARY dmHexte
// .HEADER  HEXTE Data Management
// .INCLUDE AccHxArch.h
// .FILE    AccHxArch.C
// .VERSION $Revision: 4.2 $
//
// RCS: $Id: AccHxArch.h,v 4.2 1997/01/31 21:47:02 soccm Exp $
//
// ==========================================================================

// .SECTION DESCRIPTION
// This class provides access to the archive data stored in HEXTE
// housekeeping data.

// .SECTION SEE ALSO
// PktHexte(3)

// .SECTION AUTHOR
// Randall D. Barnette
// Hughes STX Corp.
// <rbarnett@@xema.stx.com>

#ifndef ACCHXARCH_H
#define ACCHXARCH_H

#include <Accessor.h>

#include "HxHk16Part.h"

class AccHxArch : public Accessor {

  RWDECLARE_COLLECTABLE(AccHxArch) ;

public:

  //* Constructors

  AccHxArch(void) ;
  AccHxArch(const HxHk16Part& aPart) ;

  //* Public Member Functions

  unsigned long idf(void) const ;
  unsigned long goodEvents(void) const ;
  int liveTime(int det) const ;
  const unsigned short* lowerHist(int det) const ;
  const unsigned char*  upperHist(int det) const ;
  const unsigned short* msBin(void) const ;
  const unsigned char*  msLimits(void) const ;

  static void destroyDescriptors(void) ;

  //% The following are redefined from Accessor

  unsigned long time(void) const ;
  int apid(void) const ;
  DataValue* access(const Descriptor& aSelection) const ;
  const RWOrdered* descriptors(void) const ;
  int descriptorCount(void) const ;
  int isAvailable(const Descriptor& aDescriptor) const ;
  unsigned long getDataValue(void) const ;

  //% The following are redefined from Rogue Wave RWCollectable

  RWspace   binaryStoreSize(void) const;
  int       compareTo(const RWCollectable* c) const;
  RWBoolean isEqual(const RWCollectable* c) const;
  void      restoreGuts(RWFile& f);
  void      restoreGuts(RWvistream& s);
  void      saveGuts(RWFile& f) const;
  void      saveGuts(RWvostream& s) const;
 
private:

  //* Private Data Members

  unsigned long  theTime ;
  int isCluster1;
  int liveTimes[4] ;
  unsigned long  idfNumber ;
  unsigned long  totalGoodEvents ;
  unsigned short hist1[4][32] ;
  unsigned char  hist2[4][32] ;
  unsigned short msbin[16][4] ;
  unsigned char  mslimits[5] ;
  unsigned char  clusterPos;

  //* Static Data Members

  static RWOrdered* c1Descriptors ;
  static RWOrdered* c2Descriptors ;
} ;

//
// Inline functions
//

// Description:
// Return the time of the partition. This should be on a 16 second
// boundary.

inline unsigned long AccHxArch::time(void) const { return theTime ; }

// Description:
// Return the data apid. These values are different from the original
// telemetry apids.

inline int AccHxArch::apid(void) const { return isCluster1?82:88; }

// Description:
// Return the HEXTE Archive data Instrument Data Frame number.
// This is IDF at the end of the archive data accumulation period.

inline unsigned long AccHxArch::idf(void) const { return idfNumber ; }

// Description:
// Return the total good events from the archive data.

inline unsigned long AccHxArch::goodEvents(void) const
{
  return totalGoodEvents ;
}

// Description:
// Return the live time from the indicated detector.

inline int AccHxArch::liveTime(int det) const
{
	return liveTimes[det] ;
}

// Description:
// Return a pointer to the data for the lower channels in the archive
// histogram. This is an array of 16-bit values.

inline const unsigned short* AccHxArch::lowerHist(int det) const
{
	return hist1[det] ;
}

// Description:
// Return a pointer to the data for the upper channels in the archive
// histogram. This is an array of 8-bit values.

inline const unsigned char* AccHxArch::upperHist(int det) const
{
	return hist2[det] ;
}

// Description:
// Return a pointer to the binned archive data.

inline const unsigned short* AccHxArch::msBin(void) const { return msbin[0] ; }

// Description:
// Return a pointer to the binned data limits.

inline const unsigned char* AccHxArch::msLimits(void) const { return mslimits;}

#endif /* ACCHXARCH_H */
@


4.1
log
@initial version
@
text
@d12 1
a12 1
// .VERSION $Revision: 1.1 $
d14 1
a14 1
// RCS: $Id: AccHxArch.h,v 1.1 1995/11/01 18:46:53 soccm Exp $
a68 1
  void setLiveTimes(const AccHxArch*) ;
@

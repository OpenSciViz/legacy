head	4.2;
access
	rbarnett
	soccm
	savkoor;
symbols
	Build4_5_3:4.2
	Build4_3_1:4.1
	Build4_1:3.6
	Build4:3.6
	Build3_3:3.6
	Build3_2:3.6
	Build3_1:3.2
	Build3:3.2
	Build2:2.3;
locks;
comment	@ * @;


4.2
date	95.10.23.20.55.37;	author rbarnett;	state Exp;
branches;
next	4.1;

4.1
date	95.05.25.16.08.11;	author rbarnett;	state Exp;
branches;
next	3.6;

3.6
date	94.10.11.18.22.50;	author rbarnett;	state Exp;
branches;
next	3.5;

3.5
date	94.09.21.21.17.50;	author rbarnett;	state Exp;
branches;
next	3.4;

3.4
date	94.08.16.20.38.47;	author rbarnett;	state Exp;
branches;
next	3.3;

3.3
date	94.07.21.22.44.08;	author rbarnett;	state Exp;
branches;
next	3.2;

3.2
date	94.06.21.22.20.01;	author rbarnett;	state Exp;
branches;
next	3.1;

3.1
date	94.04.26.20.11.12;	author rbarnett;	state Exp;
branches;
next	3.0;

3.0
date	94.03.18.23.00.58;	author rbarnett;	state Exp;
branches;
next	2.6;

2.6
date	94.03.18.22.46.03;	author rbarnett;	state Exp;
branches;
next	2.5;

2.5
date	94.02.02.20.25.00;	author rbarnett;	state Exp;
branches;
next	2.4;

2.4
date	94.01.13.21.22.42;	author rbarnett;	state Exp;
branches;
next	2.3;

2.3
date	93.11.10.21.13.41;	author rbarnett;	state Release;
branches;
next	2.2;

2.2
date	93.09.10.20.18.39;	author rbarnett;	state Exp;
branches;
next	2.1;

2.1
date	93.08.31.19.18.50;	author rbarnett;	state Exp;
branches;
next	;


desc
@Creation
@


4.2
log
@documentation updates
@
text
@// ========================================================================
// File Name   : AccSC.C
// Subsystem   : Spacecraft Data Management
// Programmer  : Randall D. Barnette, Hughes STX (rbarnett@@xema.stx.com)
// Description : This file is the definition of class AccSC. It is a
//               subclass of Accessor to retrieve values from Spacecraft
//               packets. It is used as a base class for all spacecraft
//               Accessors except for GenericSC.
//
// RCS:
static const char rcsid[]=
   "$Id: AccSC.C,v 3.6 1994/10/11 18:22:50 rbarnett Exp $";
//
// ========================================================================

// Local headers
#include "AccSC.h"
#include "Descriptor.h"

// Private headers
#include "sc_ids.h"

//
// Return a pointer to item in descriptor list that matches d
// Null otherwise.
//
static const Descriptor* inDescriptorList(const Descriptor& d,
                                          const RWOrdered* list)
{
  Descriptor x(d) ;
  size_t p ;
  if((p=x.index("<<")) != RW_NPOS ) x.remove(p) ;
  if((p=x.index(">>")) != RW_NPOS ) x.remove(p) ;
  return (const Descriptor*) list->find(&x) ; // May be NULL
}

// ========================================================================
// AccSC
// ========================================================================

//
// Define some RogueWave required functions
//
RWDEFINE_COLLECTABLE(AccSC, AccSC_ID)

// Description:
// Constructor for RogueWave use

AccSC::AccSC(void) {}

// Description:
// Public Constructor

AccSC::AccSC(const PktSC& thePacket) :
  theId(thePacket.applicationID()),
  theTime(thePacket.seconds()),
  theSequence(thePacket.sequenceNumber()) {}

// Description:
// Destructor

AccSC::~AccSC(void) {}

// Description:
// Return the Sequence Number

unsigned long AccSC::sequence(void) const { return theSequence ; }

// Description:
// Return the index of the selection in the set of descriptors.
// -1 = Not Found

int AccSC::descriptorIndex(const Descriptor& aDescriptor) const
{
  const RWOrdered * l = descriptors() ;
  const Descriptor* x = inDescriptorList(aDescriptor, l) ;
  return x ? l->index(x) : -1 ;
}

// -----------------------------------------
// The following are redefined from Accessor
// -----------------------------------------

// Description:
// Return the apid.

int AccSC::apid(void) const { return (int) theId ; }

// Description:
// Return the packet time

unsigned long AccSC::time(void) const { return theTime ; }

// Description:
// Return true or false if aDescriptor exists in the set of descriptors

int AccSC::isAvailable(const Descriptor& aDescriptor) const
{
  return inDescriptorList(aDescriptor,descriptors()) ? 1 : 0 ;
}

// ----------------------------------------------
// The following are redefined from RWCollectable
// ----------------------------------------------

RWspace AccSC::binaryStoreSize(void) const
{
  return Accessor::binaryStoreSize() +
    sizeof(unsigned long) +
    sizeof(unsigned long) +
    sizeof(unsigned long) ;
}

int AccSC::compareTo(const RWCollectable* target) const
{
  const AccSC * t = (const AccSC *) target ;
  unsigned long t1 = time() ;
  unsigned long t2 = t->time() ;
  if( t1 == t2 ) return 0 ;
  return t1 > t2 ? 1 : -1 ;
}

RWBoolean AccSC::isEqual(const RWCollectable* target) const
{
  return compareTo(target) ? FALSE : TRUE ;
}

void AccSC::restoreGuts(RWFile& f)
{
  Accessor::restoreGuts(f) ;
  f.Read(theId) ;
  f.Read(theTime) ;
  f.Read(theSequence) ;
}

void AccSC::restoreGuts(RWvistream& s)
{
  Accessor::restoreGuts(s) ;
  s >> theId >> theTime >> theSequence ;
}

void AccSC::saveGuts(RWFile& f) const
{
  Accessor::saveGuts(f) ;
  f.Write(theId) ;
  f.Write(theTime) ;
  f.Write(theSequence) ;
}

void AccSC::saveGuts(RWvostream& s) const
{
  Accessor::saveGuts(s) ;
  s << theId << theTime << theSequence ;
}
@


4.1
log
@Build freeze
@
text
@d46 1
a46 1
//
d48 1
a48 1
//
d51 1
a51 1
//
d53 1
a53 1
//
d59 1
a59 1
//
d61 1
a61 1
//
d64 1
a64 1
//
d66 1
a66 1
//
d69 2
a70 2
//
// Return the index of the selection in the set of descriptors
d72 1
a72 1
//
d84 3
a86 3
//
// Return the App ID
//
d89 1
a89 1
//
d91 1
a91 1
//
d94 1
a94 1
//
d96 1
a96 1
//
d106 1
a106 1
RWspace AccSC::binaryStoreSize() const
@


3.6
log
@Update for Build 3.3
@
text
@d12 1
a12 1
   "$Id: AccSC.C,v 3.5 1994/09/21 21:17:50 rbarnett Exp rbarnett $";
@


3.5
log
@Updated save/restore to RW 6 specs
@
text
@d12 1
a12 1
   "$Id: AccSC.C,v 3.4 1994/08/16 20:38:47 rbarnett Exp rbarnett $";
d23 14
d69 11
d94 8
@


3.4
log
@added vstream.h
@
text
@d4 5
a8 2
// Programmer  : Randall D. Barnette - Hughes STX
// Description :
a9 3
//  This file is the definition of class AccSC. It is a subclass of
//  Accessor to retrieve values from Spacecraft packets.
//
d12 1
a12 1
   "$Id: AccSC.C,v 3.3 1994/07/21 22:44:08 rbarnett Exp $";
a15 3
// System headers
#include <rw/vstream.h>

d92 1
a92 4
  const AccSC * t = (const AccSC *) target ;
  unsigned long t1 = time() ;
  unsigned long t2 = t->time() ;
  return( t1 == t2 ) ? 1 : 0 ;
@


3.3
log
@Update for RW6
@
text
@d12 1
a12 1
   "$Id: AccSC.C,v 3.2 1994/06/21 22:20:01 rbarnett Exp rbarnett $";
d17 1
d112 1
a112 3
  s >> theId ;
  s >> theTime ;
  s >> theSequence ;
d126 1
a126 3
  s << theId ;
  s << theTime ;
  s << theSequence ;
@


3.2
log
@Fixed use of SCDataTable.h
@
text
@d12 1
a12 1
   "$Id: AccSC.C,v 3.1 1994/04/26 20:11:12 rbarnett Exp $";
d75 1
a75 1
unsigned AccSC::binaryStoreSize() const
@


3.1
log
@Made S/C code consistent for Build 3
@
text
@d12 1
a12 1
   "$Id: AccSC.C,v 3.0 1994/03/18 23:00:58 rbarnett Exp $";
d43 3
a45 3
	theId(thePacket.applicationID()),
	theTime(thePacket.seconds()),
	theSequence(thePacket.sequenceNumber()) {}
d77 4
a80 4
	return Accessor::binaryStoreSize() +
		sizeof(unsigned long) +
		sizeof(unsigned long) +
		sizeof(unsigned long) ;
d85 5
a89 5
	const AccSC * t = (const AccSC *) target ;
	unsigned long t1 = time() ;
	unsigned long t2 = t->time() ;
	if( t1 == t2 ) return 0 ;
	return t1 > t2 ? 1 : -1 ;
d94 4
a97 4
	const AccSC * t = (const AccSC *) target ;
	unsigned long t1 = time() ;
	unsigned long t2 = t->time() ;
	return( t1 == t2 ) ? 1 : 0 ;
d103 3
a105 3
	f.Read(theId) ;
	f.Read(theTime) ;
	f.Read(theSequence) ;
d111 3
a113 3
	s >> theId ;
	s >> theTime ;
	s >> theSequence ;
d119 3
a121 3
	f.Write(theId) ;
	f.Write(theTime) ;
	f.Write(theSequence) ;
d127 3
a129 3
	s << theId ;
	s << theTime ;
	s << theSequence ;
@


3.0
log
@NewBuild
@
text
@d2 1
a2 1
// Program Name: AccSC.C
d10 1
a10 1
// RCS stamp
d12 1
a12 1
"$Id: AccSC.C,v 2.6 1994/03/18 22:46:03 rbarnett Exp rbarnett $";
a20 1
#include "sc_ids.h"
d23 1
a23 2

// Function Declarations
a28 2
// File scope variables

d50 1
a50 17
AccSC::~AccSC() {}

//
// Return the App ID
//
int AccSC::apid(void) const
{
	return theId ;
}

//
// Return the packet time
//
unsigned long AccSC::time(void) const
{
	return theTime ;
}
d53 1
a53 1
// Return the App descriptor count
d55 1
a55 4
int AccSC::descriptorCount(void) const
{
	return 0 ;
}
d58 1
a58 1
// The following are inherited from Accessor
d62 1
a62 1
// Return the list of descriptors available to this Accessor
d64 1
a64 1
const RWOrdered* AccSC::descriptors(void) const { return 0 ; }
d67 1
a67 2
// Return a value from the packet data corresponding to the descriptor
// argument
d69 1
a69 1
DataValue* AccSC::access(const Descriptor&) const { return 0 ; }
d72 1
a72 1
// The following are inherited from RWCollectable
d77 4
a80 1
	return 3*sizeof(unsigned long) + Accessor::binaryStoreSize() ;
d103 1
a103 2
	long temp ;
	f.Read(temp) ; theId = (int) temp ;
d111 1
a111 2
	long temp ;
	s >> temp ; theId = (int) temp ;
d119 1
a119 1
	f.Write((long)theId) ;
d127 1
a127 1
	s << (long) theId ;
@


2.6
log
@*** empty log message ***
@
text
@d12 1
a12 1
"$Id: AccSC.C,v 2.5 1994/02/02 20:25:00 rbarnett Exp $";
@


2.5
log
@removed sc_ids.h from public view
@
text
@d1 1
d11 2
a12 1
static const char rcsid[]="$Id: AccSC.C,v 2.4 1994/01/13 21:22:42 rbarnett Exp $";
d14 1
a16 2
#include <stdlib.h>
#include <rw/regexp.h>
d46 14
a59 1
AccSC::AccSC(const PktSC& thePacket)
d61 1
a61 3
	theId = thePacket.applicationID() ;
	theTime = thePacket.seconds() ;
	theSequence = thePacket.sequenceNumber() ;
d65 9
a73 1
// Destructor
d75 4
a78 1
AccSC::~AccSC() {}
@


2.4
log
@Added disposition handlers
@
text
@d10 1
a10 1
static const char rcsid[]="$Id: AccSC.C,v 2.3 1993/11/10 21:13:41 rbarnett Release rbarnett $";
d20 1
@


2.3
log
@*** empty log message ***
@
text
@d10 1
a10 1
static const char rcsid[]="$Id: AccSC.C,v 2.2 1993/09/10 20:18:39 rbarnett Exp rbarnett $";
d39 1
a39 1
AccSC::AccSC() {}
d63 1
a63 1
const Descriptor** AccSC::descriptors(void) const { return 0 ; }
d69 1
a69 1
DataValue * AccSC::access( Descriptor* ) const { return 0 ; }
@


2.2
log
@Saves and restores theId properly
@
text
@d10 1
a10 1
static const char rcsid[]="$Id: AccSC.C,v 2.1 1993/08/31 19:18:50 rbarnett Exp rbarnett $";
d44 1
a44 1
AccSC::AccSC(PktSC& thePacket)
@


2.1
log
@Creation
@
text
@d10 1
a10 1
static const char rcsid[]="$Id: AccSC.C,v 2.1 1993/08/09 13:42:23 rbarnett Exp rbarnett $";
d100 2
a101 1
	f.Read((long&)theId) ;
d109 2
a110 1
	s >> (long&) theId ;
@

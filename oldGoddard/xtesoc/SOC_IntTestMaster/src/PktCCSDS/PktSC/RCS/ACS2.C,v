head	4.1;
access
	soccm
	savkoor;
symbols
	Build4_3_1:4.1
	Build4_1:1.5
	Build4:1.5
	Build3_3:1.4
	Build3_2:1.4
	Build3_1:1.1
	Build3:1.1;
locks;
comment	@ * @;


4.1
date	95.05.25.16.08.11;	author rbarnett;	state Exp;
branches;
next	1.5;

1.5
date	94.12.01.14.23.52;	author rbarnett;	state Exp;
branches;
next	1.4;

1.4
date	94.09.21.21.17.50;	author rbarnett;	state Exp;
branches;
next	1.3;

1.3
date	94.08.16.20.38.47;	author rbarnett;	state Exp;
branches;
next	1.2;

1.2
date	94.07.21.22.44.08;	author rbarnett;	state Exp;
branches;
next	1.1;

1.1
date	94.04.26.20.11.12;	author rbarnett;	state Exp;
branches;
next	;


desc
@ACS-2 Partition Class
@


4.1
log
@Build freeze
@
text
@// ==========================================================================
// File Name   : ACS2.C           
// Subsystem   : Spacecraft Data Management
// Programmer  : Randall D. Barnette, Hughes STX (rbarnett@@xema.stx.com)
// Description : Implementation of the ACS2 partition class
//
// RCS:
static const char rcsid[] =
"$Id: ACS2.C,v 1.5 1994/12/01 14:23:52 rbarnett Exp $";
//
// ==========================================================================

// System headers
#include <string.h>

// Local headers
#include "ACS2.h"

// Private headers
#include "sc_ids.h"

//
// The following is needed by the RogueWave.                              
//
RWDEFINE_COLLECTABLE(ACS2, ACS2_ID)

//
// Allocate space for static members
//
ACS2Callback ACS2::theHandler = 0 ;

//
// This is the default constructor which is needed by Rogue Wave
//
ACS2::ACS2(void) {}

// Description:
// Constructor takes PktACS argument. Packet contents are copied into
// an internal buffer.

ACS2::ACS2(const PktACS& thePacket):theSeq(thePacket.sequenceNumber())
{
	// Should really check to see that apid == 16 !
  theTime = (double) (thePacket.seconds()) +
            ((double) thePacket.subSeconds()) * SC_subsecond_conversion ;
  memcpy(buf, thePacket.applicationData(), ACS2_BUF_SIZE ) ;
}

// Description:
// Destructor releases all memory associated with partition.

ACS2::~ACS2(void) {}

// ----------------------------------------------------
// The following are virtual methods from RWCollectable
// ----------------------------------------------------

int ACS2::compareTo(const RWCollectable* c) const
{
	if( c->isA() != ACS2_ID ) return -1 ;
	const ACS2* rhs = (const ACS2*) c ;
	return theTime<rhs->theTime ? -1 : theTime>rhs->theTime ? 1 : 0 ;
}

RWBoolean ACS2::isEqual(const RWCollectable* c) const
{
	return compareTo(c) ? FALSE : TRUE ;
}

RWspace ACS2::binaryStoreSize(void) const
{
  return RWCollectable::binaryStoreSize() +
         sizeof(double)        + // theTime
	       sizeof(unsigned long) + // theSeq
	       ACS2_BUF_SIZE         ; // data buffer
}

void ACS2::restoreGuts(RWFile& f)
{
  RWCollectable::restoreGuts(f) ;
  f.Read(theTime) ;
  f.Read(theSeq) ;
  f.Read(buf, ACS2_BUF_SIZE ) ;
}

void ACS2::restoreGuts(RWvistream& s)
{
  RWCollectable::restoreGuts(s) ;
	s >> theTime >> theSeq ;
  s.get(buf, ACS2_BUF_SIZE ) ;
}

void ACS2::saveGuts(RWFile& f) const
{
  RWCollectable::saveGuts(f) ;
  f.Write(theTime) ;
  f.Write(theSeq) ;
  f.Write(buf,ACS2_BUF_SIZE) ;
}

void ACS2::saveGuts(RWvostream& s) const
{
  RWCollectable::saveGuts(s) ;
	s << theTime << theSeq ;
  s.put(buf, ACS2_BUF_SIZE ) ;
}

@


1.5
log
@Updates for genman
@
text
@d9 1
a9 1
"$Id: ACS2.C,v 1.4 1994/09/21 21:17:50 rbarnett Exp $";
@


1.4
log
@Updated save/restore to RW 6 specs
@
text
@d9 1
a9 1
"$Id: ACS2.C,v 1.3 1994/08/16 20:38:47 rbarnett Exp rbarnett $";
d37 4
a40 3
//
// Constructor takes PktACS argument.
//
d49 3
a51 3
//
// Destructor
//
d70 1
a70 1
RWspace ACS2::binaryStoreSize() const
@


1.3
log
@added vstream.h
@
text
@d4 2
a5 2
// Programmer  : Randall D. Barnette, Hughes STX (NASA/GSFC code 664)
// Description :
a6 2
//  Implementation of the ACS2 partition class
//
d9 1
a9 1
"$Id: ACS2.C,v 1.2 1994/07/21 22:44:08 rbarnett Exp $";
d13 1
a14 1
#include <rw/vstream.h>
d16 1
d18 2
d61 1
a61 3
	return theTime<rhs->theTime ? -1 : theTime>rhs->theTime ? 1 :
		// Times are equal, sort on sequence number
		theSeq<rhs->theSeq ? -1 : theSeq>rhs->theSeq ? 1 : 0 ;
d66 1
a66 3
	if( c->isA() != ACS2_ID ) return FALSE ;
	const ACS2* rhs = (const ACS2*) c ;
	return theTime==rhs->theTime ? theSeq==rhs->theSeq ? TRUE : FALSE : FALSE ;
d71 2
a72 1
  return sizeof(double)        + // theTime
@


1.2
log
@Update for RW6
@
text
@d11 1
a11 1
"$Id: ACS2.C,v 1.1 1994/04/26 20:11:12 rbarnett Exp rbarnett $";
d16 1
@


1.1
log
@Initial revision
@
text
@d11 1
a11 1
"$Id: ACS2.C,v 3.0 1994/03/18 23:07:42 rbarnett Exp $";
d71 1
a71 1
unsigned ACS2::binaryStoreSize() const
@

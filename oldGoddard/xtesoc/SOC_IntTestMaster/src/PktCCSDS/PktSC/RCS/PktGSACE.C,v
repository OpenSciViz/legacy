head	1.1;
access
	soccm
	savkoor;
symbols
	Build4_5_3:1.1;
locks;
comment	@ * @;


1.1
date	95.10.23.20.57.47;	author rbarnett;	state Exp;
branches;
next	;


desc
@Packet class for GSACE packets
@


1.1
log
@Initial revision
@
text
@// ======================================================================
// File Name   : PktGSACE.C
// Subsystem   : Spacecraft Data Management
// Programmer  : Randall D. Barnette, Hughes STX (rbarnett@@xema.stx.com)
// Description : Implementation of the PktGSACE class.
//
// RCS:
static const char rcsid[]=
"$Id: PktGSACE.C,v 4.1 1995/05/25 16:08:11 rbarnett Exp $";
//
// ======================================================================

// SOC headers
#include <SOCMessage.h>

// Local headers
#include "PktGSACE.h"

//
// Include GSACE partition types
//
#include <GSACEFast.h>
//#include <GSACEMed.h>
//#include <GSACESlow.h>

// Private headers
#include "sc_ids.h"

//
// The following is needed by the RogueWave.
//
RWDEFINE_COLLECTABLE(PktGSACE, PktGSACE_ID)
 
// Description:
// Construct a packet to be filled later. Needed by RogueWave
// when building an object from disk. Should not be explicitly
// used by clients.
 
PktGSACE::PktGSACE(void) {}
 
// Description:
// Constructor taking an Exemplar.
 
PktGSACE::PktGSACE(Exemplar) {}
 
// Description:
// Constructor to create packet from data buffer. This should
// not be called directly as all packet building should use the
// make() method.
// This is the constructor which constructs all useful GSACE packets.
 
PktGSACE::PktGSACE(unsigned char* buffer) : PktSC(buffer) {}
 
// Description:
// The destructor.
 
PktGSACE::~PktGSACE(void) {}
 
// Description:
// This methods assembles the packets into Partitions and
// invokes user-defined callback functions.
 
void PktGSACE::apply(void) const
{
 
  //
  // Dispose of by individual apids
  //
  switch( applicationID() ) {
  case 190 :
  case 191 :
    { // New block to declare Partition
      GSACEFast thePart(*this) ;
      if( thePart.hasDisposition() ) thePart.dispose() ;
      else PktSC::dispose() ;
    } // Partition destructor called when partition goes out of scope.
    break ;
  case 192 :
  case 193 :
  case 194 :
  case 195 :
    // Dispose of in generic way.
    PktSC::dispose() ;
    break ;
  default:
    SOCThrow(SOC_REPORT, "Unkown GSACE Apid %d\n", applicationID() ) ;
  }
 
  return ;
}
 
// Description:
// This function is used to parse a packet from a data buffer.
// All subclasses should provide an appropriate one of these.
// It returns a packet of the appropriate type or subtype.
// This constructs a packet of the appropriate subclass, given a raw
// buffer containing a Telemetry packet.
 
PktTlm* PktGSACE::make(unsigned char* buffer)
{
  // Get ApplicationID from the packet
  int appId = applicationID(buffer) ;
  return (appId > 189 && appId < 196) ? (PktTlm*) new PktGSACE(buffer) : 0 ;
}

@

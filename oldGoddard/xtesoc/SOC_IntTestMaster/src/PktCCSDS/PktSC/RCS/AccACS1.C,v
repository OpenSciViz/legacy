head	4.2;
access
	rbarnett
	soccm
	savkoor;
symbols
	Build4_5_3:4.2
	Build4_3_1:4.1
	Build4_1:3.6
	Build4:3.6
	Build3_3:3.6
	Build3_2:3.6
	Build3_1:3.3
	Build3:3.2
	Build2:2.3;
locks;
comment	@ * @;


4.2
date	95.10.23.20.55.12;	author rbarnett;	state Exp;
branches;
next	4.1;

4.1
date	95.05.25.16.08.11;	author rbarnett;	state Exp;
branches;
next	3.7;

3.7
date	95.05.25.15.58.51;	author rbarnett;	state Exp;
branches;
next	3.6;

3.6
date	94.10.11.18.22.50;	author rbarnett;	state Exp;
branches;
next	3.5;

3.5
date	94.09.21.21.17.50;	author rbarnett;	state Exp;
branches;
next	3.4;

3.4
date	94.07.21.22.44.08;	author rbarnett;	state Exp;
branches;
next	3.3;

3.3
date	94.07.15.18.11.47;	author rbarnett;	state Exp;
branches;
next	3.2;

3.2
date	94.06.21.22.20.01;	author rbarnett;	state Exp;
branches;
next	3.1;

3.1
date	94.04.26.20.11.12;	author rbarnett;	state Exp;
branches;
next	3.0;

3.0
date	94.03.18.23.00.46;	author rbarnett;	state Exp;
branches;
next	2.7;

2.7
date	94.03.18.22.46.03;	author rbarnett;	state Exp;
branches;
next	2.6;

2.6
date	94.02.02.20.25.00;	author rbarnett;	state Exp;
branches;
next	2.5;

2.5
date	94.01.19.21.51.10;	author rbarnett;	state Exp;
branches;
next	2.4;

2.4
date	94.01.13.21.22.42;	author rbarnett;	state Exp;
branches;
next	2.3;

2.3
date	93.11.10.21.13.47;	author rbarnett;	state Release;
branches;
next	2.2;

2.2
date	93.09.10.16.53.06;	author rbarnett;	state Exp;
branches;
next	2.1;

2.1
date	93.09.08.20.46.44;	author gmanicka;	state Exp;
branches;
next	;


desc
@Creation
@


4.2
log
@doc updates
@
text
@// ============================================================================
// File Name   : AccACS1.C
// Subsystem   : Spacecraft Data Management
// Programmer  : Randall D. Barnette, Hughes STX (rbarnett@@xema.stx.com)
// Description : This file is the definition of class AccACS1.
//               It is a subclass of Accessor to retrieve values from
//               ACS-1 packets.
//
// RCS:
static const char rcsid[] =
"$Id: AccACS1.C,v 3.7 1995/05/09 16:20:43 soccm Exp $";
//
// ============================================================================

// Rogue Wave headers
#include <rw/collstr.h>

// Local headers
#include "AccACS1.h"

// Private headers
#include "SCData.h"
#include "sc_ids.h"

// ACS-1 is application ID 15
#define APID 15

// ========================================================================
// AccACS1
// ========================================================================

//
// Class variables for Descriptors declared and initialized
//
RWOrdered*        AccACS1::theDescriptors = initialize_SC_descriptors(APID);
RWHashDictionary* AccACS1::theConversions = initialize_SC_database(APID);

//
// Define some RogueWave required functions
//
RWDEFINE_COLLECTABLE(AccACS1, AccACS1_ID)

// Description:
// Constructor for RogueWave use.

AccACS1::AccACS1(void) {}

// Description:
// Public Constructor initialized with a PktACS.

AccACS1::AccACS1(const PktACS& aPacket) : AccSC(aPacket), theData(aPacket) {}

// Description:
// Public Constructor initialized with an ACS1 partition.

AccACS1::AccACS1(const ACS1& aPart) : theData(aPart)
{
  theId=aPart.apid();
  theTime=aPart.time();
  theSequence=aPart.sequence();
}

// Description:
// Destructor.

AccACS1::~AccACS1(void) {}

// Description:
// Release memory for static descriptor list.

void AccACS1::destroyDescriptors(void)
{
  if( theDescriptors) {
    theDescriptors->clearAndDestroy() ;
    delete theDescriptors ;
  }
  if( theConversions) {
    theConversions->clearAndDestroy() ;
    delete theConversions ;
  }
  theDescriptors = 0 ;
  theConversions = 0 ;
}

// -----------------------------------------
// The following are redefined from Accessor
// -----------------------------------------

// Description:
// Return the list of descriptors available to this Accessor.

const RWOrdered* AccACS1::descriptors(void) const
{
  return theDescriptors ;
}

// Description:
// Return the number of descriptors available to this Accessor.

int AccACS1::descriptorCount(void) const
{
  return theDescriptors->entries() ;
}

// Description:
// Return the list and number of descriptors available to this Accessor.
// This is a static function!

const RWOrdered* AccACS1::descriptorList(int& count) const
{
	count = theDescriptors->entries() ;
	return theDescriptors ;
}

// Description:
// Return a value from the packet data corresponding to the descriptor
// argument.

DataValue* AccACS1::access(const Descriptor& aSelection ) const
{

	DataValue* mdata = 0 ;

  //
  // Check for the simple, hard-coded Descriptors
  //
  if( aSelection == "APPID" ) {
    mdata = new DataValue( BaseType_UINT32 ) ;
    mdata->setValue( new UINT32(apid()) ) ;
  } else if( aSelection == "TIME" ) {
    mdata = new DataValue( BaseType_DOUBLE ) ;
    mdata->setValue( new double( theData.time()) ) ;
  } else if( aSelection == "DATAMODE" ) {
    mdata = new DataValue( BaseType_UINT32 ) ;
    mdata->setValue( new UINT32(0) ) ;
	}
  //
  // Check for proper descriptors
  //
  else if( aSelection.observatory() == "XTE" &&
           aSelection.instrument()  == "ACS" ){

    //
    // The Housekeeping token is the Spacecraft mnemonic. Turn it into
    // a collectable string.
    //
    RWCollectableString h(aSelection.housekeeping()) ;

    //
    // Look it up in the hash dictionary.
    //
    SCitem* item = (SCitem*) (theConversions->findValue( &h )) ;

    //
    // Test it.
    //
    if( item && (item->isA()==SCitem_ID) )
			mdata = item->asDataValue(theData.rawLookup()) ;
	}

  return mdata ;
}

// ----------------------------------------------
// The following are inherited from RWCollectable
// ----------------------------------------------

int AccACS1::compareTo(const RWCollectable* c) const
{
  if( c->isA() != AccACS1_ID ) return -1 ;
  const AccACS1* rhs = (const AccACS1*) c ;
  return theData.compareTo(&(rhs->theData)) ;
}

RWBoolean AccACS1::isEqual(const RWCollectable* c) const
{
  return compareTo(c) ? FALSE : TRUE ;
}

RWspace AccACS1::binaryStoreSize(void) const
{
  return AccSC::binaryStoreSize() +
         theData.recursiveStoreSize();
}

void AccACS1::restoreGuts(RWFile& f)
{
  AccSC::restoreGuts(f) ;
  f >> theData ;
}

void AccACS1::restoreGuts(RWvistream& s)
{
  AccSC::restoreGuts(s) ;
  s >> theData ;
}

void AccACS1::saveGuts(RWFile& f) const
{
  AccSC::saveGuts(f) ;
  f << theData ;
}

void AccACS1::saveGuts(RWvostream& s) const
{
  AccSC::saveGuts(s) ;
  s << theData ;
}
@


4.1
log
@Build freeze
@
text
@d11 1
a11 1
"$Id: AccACS1.C,v 3.7 1995/05/25 15:58:51 rbarnett Exp $";
d25 1
a25 1
// AACS-1 is application ID 15
d43 4
a46 4
//
// Constructor for RogueWave use
//
AccACS1::AccACS1() {}
d48 1
a48 1
//
d50 1
a50 1
//
d53 1
a53 1
//
d55 1
a55 1
//
d63 3
a65 3
//
// Destructor
//
d68 3
a70 3
//
// Release memory for descriptor list
//
d73 10
a82 4
  theDescriptors->clearAndDestroy() ;
  theConversions->clearAndDestroy() ;
  delete theDescriptors ;
  delete theConversions ;
d89 3
a91 3
//
// Return the list of descriptors available to this Accessor
//
d97 3
a99 3
//
// Return the number of descriptors available to this Accessor
//
d105 2
a106 2
//
// Return the list and number of descriptors available to this Accessor
d108 1
a108 1
//
d115 1
a115 1
//
d117 2
a118 2
// argument
//
d180 1
a180 1
RWspace AccACS1::binaryStoreSize() const
@


3.7
log
@removed unused descriptor items
@
text
@d11 1
a11 1
"$Id: AccACS1.C,v 3.7 1995/05/09 16:20:43 soccm Exp $";
@


3.6
log
@Update for Build 3.3
@
text
@d11 1
a11 1
"$Id: AccACS1.C,v 3.5 1994/09/21 21:17:50 rbarnett Exp rbarnett $";
d121 1
a121 1
  if( aSelection == "PACKETTIME" ) {
d123 1
a123 4
    mdata->setValue( new unsigned long(time()) ) ;
  } else if( aSelection == "APPID" ) {
    mdata = new DataValue( BaseType_UINT32 ) ;
    mdata->setValue( new unsigned long(apid()) ) ;
d129 1
a129 36
    mdata->setValue( new unsigned long(0) ) ;
  } else if( aSelection == "ALL" ) {
		const int nacs1comps = 8; // 8 record entries
		DataType *comps[nacs1comps];
		comps[0] = new ArrayDataType( BaseType_FLOAT, 2 ) ; // Cmd +HGA[2]
		comps[1] = new ArrayDataType( BaseType_FLOAT, 2 ) ; // Cmd -HGA[2]
		comps[2] = new ArrayDataType( BaseType_FLOAT, 3 ) ; // Body Rate[3]
		comps[3] = new ArrayDataType( BaseType_FLOAT, 3 ) ; // Att Error[3]
		comps[4] = new ArrayDataType( BaseType_FLOAT, 3 ) ; // Att Error Itg[3]
		comps[5] = new ArrayDataType( BaseType_FLOAT, 3 ) ; // Control Torque[3]
		comps[6] = new DataType( BaseType_UINT8 ) ; // Control Mode status
		comps[7] = new DataType( BaseType_UINT8 ) ; // Control Mode
		mdata = new DataValue( new RecordDataType( comps, nacs1comps ) ) ;
		unsigned char *dbuf = new unsigned char[mdata->size()] ;
		mdata->setValue(dbuf) ;

		float p ;
		p = theData.hgaGimbalPX() ; memcpy( dbuf+0, &p, sizeof(float) ) ;
		p = theData.hgaGimbalPY() ; memcpy( dbuf+4, &p, sizeof(float) ) ;
		p = theData.hgaGimbalNX() ; memcpy( dbuf+8, &p, sizeof(float) ) ;
		p = theData.hgaGimbalNY() ; memcpy( dbuf+12, &p, sizeof(float) ) ;
		p = theData.rateX() ; memcpy( dbuf+16, &p, sizeof(float) ) ;
		p = theData.rateY() ; memcpy( dbuf+20, &p, sizeof(float) ) ;
		p = theData.rateZ() ; memcpy( dbuf+24, &p, sizeof(float) ) ;
		p = theData.attitudeErrorX() ; memcpy( dbuf+28, &p, sizeof(float) ) ;
		p = theData.attitudeErrorY() ; memcpy( dbuf+32, &p, sizeof(float) ) ;
		p = theData.attitudeErrorZ() ; memcpy( dbuf+36, &p, sizeof(float) ) ;
		p = theData.attitudeIntegralX() ; memcpy( dbuf+40, &p, sizeof(float) ) ;
		p = theData.attitudeIntegralY() ; memcpy( dbuf+44, &p, sizeof(float) ) ;
		p = theData.attitudeIntegralZ() ; memcpy( dbuf+48, &p, sizeof(float) ) ;
		p = theData.torqueX() ; memcpy( dbuf+52, &p, sizeof(float) ) ;
		p = theData.torqueY() ; memcpy( dbuf+56, &p, sizeof(float) ) ;
		p = theData.torqueZ() ; memcpy( dbuf+60, &p, sizeof(float) ) ;
		unsigned char c ;
		c = theData.mode() ; memcpy( dbuf+64, &c, sizeof(unsigned char) ) ;
		c = theData.modeStatus() ; memcpy( dbuf+65, &c, sizeof(unsigned char) ) ;
@


3.5
log
@Updated save/restore to RW 6 specs
@
text
@d11 1
a11 1
"$Id: AccACS1.C,v 3.4 1994/07/21 22:44:08 rbarnett Exp rbarnett $";
a109 20
// Return the index of the selection in the set of descriptors
// -1 = Not Found
//
int AccACS1::descriptorIndex(const Descriptor& aDescriptor) const
{
	const RWOrdered* d = descriptors() ;
	for( int i = d->entries()-1 ; i >= 0 ; i-- )
    if( *((const Descriptor*)(d->at(i))) == aDescriptor ) return i ;
	return -1 ;
}

//
// Return true or false if aDescriptor exists in the set of descriptors
//
int AccACS1::isAvailable(const Descriptor& aDescriptor) const
{
	return (-1 == descriptorIndex(aDescriptor)) ? 0 : 1 ;
}

//
d172 2
a173 3
  else if( aSelection.observatory() == "XTE"        &&
           aSelection.instrument()  == "SC"         &&
           aSelection.detector()    == "ACS" ){
@


3.4
log
@Update for RW6
@
text
@d4 4
a7 2
// Programmer  : Randall D. Barnette - Hughes STX
// Description :
a8 3
//  This file is the definition of class AccACS1. It is a subclass of
//  Accessor to retrieve values from ACS-1 packets.
//
d11 1
a11 1
"$Id: AccACS1.C,v 3.3 1994/07/15 18:11:47 rbarnett Exp rbarnett $";
d15 3
a18 1
#include <rw/collstr.h>
d56 1
a56 2
AccACS1::AccACS1(const ACS1& aPart) :
  theData(aPart)
d144 1
a144 2
  }
  else if( aSelection == "APPID" ) {
d147 1
a147 2
  }
  else if( aSelection == "TIME" ) {
d150 1
a150 2
  }
  else if( aSelection == "DATAMODE" ) {
d153 1
a153 3
  }

	else if( aSelection == "ALL" ) {
d223 1
a223 4
  if( c->isA() != AccACS1_ID ) {
    // should have error
    return 0 ;
  }
d227 1
d230 1
a230 3
  if( c->isA() != AccACS1_ID ) return FALSE ;
  const AccACS1* rhs = (const AccACS1*) c ;
  return theData.isEqual(&(rhs->theData)) ;
d235 2
a236 1
  return AccSC::binaryStoreSize() + theData.binaryStoreSize();
d241 2
a242 2
  Accessor::restoreGuts(f) ;
  theData.restoreGuts(f) ;
d247 2
a248 2
  Accessor::restoreGuts(s) ;
  theData.restoreGuts(s) ;
d253 2
a254 2
  Accessor::saveGuts(f) ;
  theData.saveGuts(f) ;
d259 2
a260 2
  Accessor::saveGuts(s) ;
  theData.saveGuts(s) ;
@


3.3
log
@added rw/collstr.h
@
text
@d12 1
a12 1
"$Id: AccACS1.C,v 3.2 1994/06/21 22:20:01 rbarnett Exp $";
d242 1
a242 1
unsigned AccACS1::binaryStoreSize() const
@


3.2
log
@Fixed use of SCDataTable.h
@
text
@d12 1
a12 1
"$Id: AccACS1.C,v 3.1 1994/04/26 20:11:12 rbarnett Exp $";
d17 1
@


3.1
log
@Accessor for class ACS1
@
text
@d12 1
a12 1
"$Id: AccACS1.C,v 3.0 1994/03/18 23:00:46 rbarnett Exp $";
d20 1
a20 1
#include "SCDataTable.h"
d23 3
d33 2
a34 2
RWOrdered*        AccACS1::theDescriptors = initialize_SC_descriptors(APID_15);
RWHashDictionary* AccACS1::theConversions = initialize_SC_database(APID_15);
@


3.0
log
@NewBuild
@
text
@d1 2
a2 1
// Program Name: AccACS1.C
d4 1
a4 1
// Programmer  : Randall D. Barnette, Gary D. Manicka - Hughes STX
d10 3
a12 2
// RCS stamp
static const char rcsid[]="$Id: AccACS1.C,v 2.7 1994/03/18 22:46:03 rbarnett Exp rbarnett $";
d14 1
a15 3
// System headers
#include <stdlib.h>

a17 1
#include "Descriptor.h"
d20 1
a20 1
#include "ACS1Desc.h"          // The list of ACS-1 data items
a26 2
// File scope variables

d28 1
a28 1
// This static value is the number of ACS-1 descriptors in "ACS1Desc.h"
d30 2
a31 1
static const int theCount = sizeof(ACS1Descriptors)/sizeof(ACS1Descriptors[0]);
d34 1
a34 2
// This function builds the list of descriptors from the ACS-1 item
// list (ACS1Descriptor)
d36 1
a36 17
static RWOrdered* makeDescriptorList(void)
{
  // The List to return
	RWOrdered* theList = new RWOrdered ;

  // Step through the list
	for( int i = 0 ; i < theCount ; i++ )
    theList->append( new Descriptor( ACS1Descriptors[i].tag ) ) ;

	return theList ;
}

inline float getFloat(const unsigned char * buf )
{
	unsigned long temp = buf[0] << 24 | buf[1] << 16 | buf[2] << 8 | buf[3] ;
	return (float) *( (float*) &temp ) ;
}
a37 4
inline unsigned short getShort(const unsigned char * buf )
{
	return ((unsigned short)(buf[0] << 8 | buf[1])) ;
}
d39 1
a39 1
// Decoder
d41 1
a41 24
static scAcs1* decodeACS1(const unsigned char* buf)
{
	scAcs1* acs1 = new scAcs1 ;

	acs1->mode = getShort(buf) ;
	acs1->hgaP_gimbal[0] = getFloat(buf+2);
	acs1->hgaP_gimbal[1] = getFloat(buf+6);
	acs1->hgaN_gimbal[0] = getFloat(buf+10);
	acs1->hgaN_gimbal[1] = getFloat(buf+14);
	acs1->gyro[0] = getFloat(buf+18);
	acs1->gyro[1] = getFloat(buf+22);
	acs1->gyro[2] = getFloat(buf+26);
	acs1->att_err[0] = getFloat(buf+18);
	acs1->att_err[1] = getFloat(buf+22);
	acs1->att_err[2] = getFloat(buf+26);
	acs1->att_err_integral[0] = getFloat(buf+30);
	acs1->att_err_integral[1] = getFloat(buf+34);
	acs1->att_err_integral[2] = getFloat(buf+38);
	acs1->torque[0] = getFloat(buf+42);
	acs1->torque[1] = getFloat(buf+46);
	acs1->torque[2] = getFloat(buf+50);

	return acs1 ;
}
d44 1
a44 1
// Class variables for Descriptors declared and initialized
d46 1
a46 1
RWOrdered* AccACS1::theDescriptors = makeDescriptorList() ;
d49 1
a49 1
// Release memory for descriptor list
d51 2
a52 1
void AccACS1::destroyDescriptors(void)
d54 3
a56 2
	theDescriptors->clearAndDestroy() ;
	delete theDescriptors ;
d60 1
a60 1
// Define some RogueWave required functions
d62 1
a62 1
RWDEFINE_COLLECTABLE(AccACS1, AccACS1_ID)
d65 1
a65 1
// Constructor for RogueWave use
d67 1
a67 6
AccACS1::AccACS1() {}

//
// Public Constructor
//
AccACS1::AccACS1(const PktACS& aPacket) : AccSC( aPacket )
d69 4
a72 4
	//
  // decodeACS1 allocates a Data struct and returns a pointer.
	//
	theData = decodeACS1(aPacket.applicationData() ) ;
d75 4
d80 1
a80 1
// Destructor
a81 2
AccACS1::~AccACS1() { delete theData ; }

d126 4
a129 5
// value_for constructs the DataValue item for an indexed Descriptor
// This is a rather crude way to deal with the requests because the
// Descriptor list and the value_for indexing must be maintained separately.
// 
DataValue* AccACS1::value_for( int ndex ) const
d134 79
a212 100
	switch( ndex ) {
	case 0 : // PacketTime
		mdata = new DataValue( BaseType_INT ) ;
		mdata->setValue( new int(time()) ) ;
		break ;
	case 1 : // AppID
		mdata = new DataValue( BaseType_INT ) ;
		mdata->setValue( new int(apid()) ) ;
		break ;
	case 2 : // Data Mode
		mdata = new DataValue( BaseType_INT ) ;
		mdata->setValue( new int(0) ) ;
		break ;
	case 3 : // Time
		mdata = new DataValue( BaseType_DOUBLE ) ;
		mdata->setValue( new double( (double) time()) ) ;
		break ;
	case 4 :
	case 5 : // Everything!
		{ // Gary, we need this brace because of new declarations
			const int nacs1comps = 8; // 8 record entries
			DataType *comps[nacs1comps];
			comps[0] = new DataType( BaseType_SHORT ) ;
			comps[1] = new ArrayDataType( BaseType_FLOAT, 2 ) ;
			comps[2] = new ArrayDataType( BaseType_FLOAT, 2 ) ;
			comps[3] = new ArrayDataType( BaseType_FLOAT, 3 ) ;
			comps[4] = new ArrayDataType( BaseType_FLOAT, 3 ) ;
			comps[5] = new ArrayDataType( BaseType_FLOAT, 3 ) ;
			comps[6] = new ArrayDataType( BaseType_FLOAT, 3 ) ;
			comps[7] = new ArrayDataType( BaseType_FLOAT, 3 ) ;
			mdata = new DataValue( new RecordDataType( comps, nacs1comps ) ) ;

			scAcs1 *x = new scAcs1;

			x->mode = mode() ;
			hgaP_gimbal(x->hgaP_gimbal);
			hgaN_gimbal(x->hgaN_gimbal);
			gyro(x->gyro);
			att_err(x->att_err);
			att_err_integral(x->att_err_integral);
			torque(x->torque);

			mdata->setValue(x) ;
		}
		break ;
	case 6 : // mode
		mdata = new DataValue( BaseType_SHORT ) ;
		mdata->setValue( new unsigned short( mode()) ) ;
		break ;
	case 7 : // hgaP_gimbal
		{
			float *x = new float[2];
			hgaP_gimbal(x);
			mdata = new DataValue(new ArrayDataType(BaseType_FLOAT, 2)) ;
			mdata->setValue( x ) ;
		}
		break ;
	case 8 :// hgaN_gimbal
		{
			float *x = new float[2];
			hgaN_gimbal(x);
			mdata = new DataValue(new ArrayDataType(BaseType_FLOAT, 2)) ;
			mdata->setValue( x ) ;
		}
		break ;
	case 9 :// gyro
		{
			float *x = new float[3];
			gyro(x);
			mdata = new DataValue(new ArrayDataType(BaseType_FLOAT, 3)) ;
			mdata->setValue( x ) ;
		}
		break ;
	case 10 :// att_err
		{
			float *x = new float[3];
			att_err(x);
			mdata = new DataValue(new ArrayDataType(BaseType_FLOAT, 3)) ;
			mdata->setValue( x ) ;
		}
		break ;
	case 11 :// att_err_integral
		{
			float *x = new float[3];
			att_err_integral(x);
			mdata = new DataValue(new ArrayDataType(BaseType_FLOAT, 3)) ;
			mdata->setValue( x ) ;
		}
		break ;
	case 12 :// torque
		{
			float *x = new float[3];
			torque(x);
			mdata = new DataValue(new ArrayDataType(BaseType_FLOAT, 3)) ;
			mdata->setValue( x ) ;
		}
		break ;
	default :
		cerr << "Can't handle item " << ndex << " yet" << endl ;
		return 0 ;
d218 3
a220 3
// -----------------------------------------
// The following are inherited from Accessor
// -----------------------------------------
d222 1
a222 5
//
// Return a value from the packet data corresponding to the descriptor
// argument
//
DataValue* AccACS1::access(const Descriptor& aSelection ) const
d224 12
a235 3
	int which ;
	if( -1 == (which = descriptorIndex(aSelection)) ) return 0 ;
	return value_for( which ) ;
a237 4
// ----------------------------------------------
// The following are inherited from RWCollectable
// ----------------------------------------------

d240 1
a240 1
	return sizeof(scAcs1) + Accessor::binaryStoreSize() ;
d246 1
a246 2
	theData = new scAcs1 ;
	f.Read((unsigned char *) theData, sizeof(scAcs1) ) ;
d252 1
a252 2
	theData = new scAcs1 ;
	s.get((unsigned char *) theData, sizeof(scAcs1) ) ;
d258 1
a258 1
	f.Write((const unsigned char *) theData, sizeof(scAcs1) ) ;
d264 1
a264 1
	s.put((const unsigned char *) theData, sizeof(scAcs1) ) ;
@


2.7
log
@*** empty log message ***
@
text
@d10 1
a10 1
static const char rcsid[]="$Id: AccACS1.C,v 2.6 1994/02/02 20:25:00 rbarnett Exp rbarnett $";
@


2.6
log
@removed sc_ids.h from public view
@
text
@d10 1
a10 1
static const char rcsid[]="$Id: AccACS1.C,v 2.5 1994/01/19 21:51:10 rbarnett Exp $";
d129 13
d160 1
a160 1
    if( *((Descriptor*)(d->at(i))) == aDescriptor ) return i ;
@


2.5
log
@added destroyDescriptors()
@
text
@d10 1
a10 1
static const char rcsid[]="$Id: AccACS1.C,v 2.4 1994/01/13 21:22:42 rbarnett Exp rbarnett $";
d22 1
@


2.4
log
@Added disposition handlers
@
text
@d10 1
a10 1
static const char rcsid[]="$Id: AccACS1.C,v 2.3 1993/11/10 21:13:47 rbarnett Release rbarnett $";
a39 2

  //
a40 1
  //
a42 1
  //
a43 1
  //
d93 9
@


2.3
log
@*** empty log message ***
@
text
@d10 1
a10 1
static const char rcsid[]="$Id: AccACS1.C,v 2.2 1993/09/10 16:53:06 rbarnett Exp rbarnett $";
a34 5
// Class variable theDescriptorCount declared and initialized
//
const int AccACS1::theDescriptorCount = theCount ;

//
d38 1
a38 1
static Descriptor ** makeDescriptorList(void)
d44 1
a44 1
	Descriptor** theList = new Descriptor*[theCount] ;
d50 1
a50 1
    theList[i] = new Descriptor( ACS1Descriptors[i].tag ) ;
d96 1
a96 1
Descriptor ** AccACS1::theDescriptors = makeDescriptorList() ;
d128 1
a128 1
const Descriptor** AccACS1::descriptorList(int& count) const
d130 1
a130 1
	count = theDescriptorCount ;
d138 1
a138 1
int AccACS1::descriptorIndex( Descriptor* aDescriptor ) const
d140 3
a142 3
	const Descriptor** theDescriptors = descriptors() ;
	for( int i = 0 ; i < theDescriptorCount ; i++ )
		if( *(theDescriptors[i]) == *aDescriptor ) return i ;
d149 1
a149 1
int AccACS1::isAvailable( Descriptor* aDescriptor ) const
d277 1
a277 1
DataValue * AccACS1::access( Descriptor* aSelection ) const
a279 1

a280 1

@


2.2
log
@Added ACS 1 and 2
@
text
@d10 1
a10 1
static const char rcsid[]="$Id: AccACS1.C,v 2.1 1993/09/08 20:46:44 gmanicka Exp rbarnett $";
d116 1
a116 1
AccACS1::AccACS1( PktACS& aPacket ) : AccSC( aPacket )
@


2.1
log
@Creation
@
text
@d10 1
a10 1
static const char rcsid[]="$Id: AccACS1.C,v 2.2 1993/09/01 20:55:57 rbarnett Exp gmanicka $";
a20 1
#include "spacecraft.h"        // Declare scAcs1 struct
d188 22
a209 20
		const int nacs1comps = 8; // 8 record entries
		DataType *comps[nacs1comps];
		comps[0] = new DataType( BaseType_SHORT ) ;
		comps[1] = new ArrayDataType( BaseType_FLOAT, 2 ) ;
		comps[2] = new ArrayDataType( BaseType_FLOAT, 2 ) ;
		comps[3] = new ArrayDataType( BaseType_FLOAT, 3 ) ;
		comps[4] = new ArrayDataType( BaseType_FLOAT, 3 ) ;
		comps[5] = new ArrayDataType( BaseType_FLOAT, 3 ) ;
		comps[6] = new ArrayDataType( BaseType_FLOAT, 3 ) ;
		comps[7] = new ArrayDataType( BaseType_FLOAT, 3 ) ;
		mdata = new DataValue( new RecordDataType( comps, nacs1comps ) ) ;
		scAcs1 *x = new scAcs1;

		x->mode = mode() ;
		hgaP_gimbal(x->hgaP_gimbal);
		hgaN_gimbal(x->hgaN_gimbal);
		gyro(x->gyro);
		att_err(x->att_err);
		att_err_integral(x->att_err_integral);
		torque(x->torque);
d211 2
a212 1
		mdata->setValue(x) ;
d219 6
a224 4
		float *x = new float[2];
		hgaP_gimbal(x);
		mdata = new DataValue(new ArrayDataType(BaseType_FLOAT, 2)) ;
		mdata->setValue( x ) ;
d227 6
a232 4
		float *x = new float[2];
		hgaN_gimbal(x);
		mdata = new DataValue(new ArrayDataType(BaseType_FLOAT, 2)) ;
		mdata->setValue( x ) ;
d235 6
a240 4
		float *x = new float[3];
		gyro(x);
		mdata = new DataValue(new ArrayDataType(BaseType_FLOAT, 3)) ;
		mdata->setValue( x ) ;
d243 6
a248 4
		float *x = new float[3];
		att_err(x);
		mdata = new DataValue(new ArrayDataType(BaseType_FLOAT, 3)) ;
		mdata->setValue( x ) ;
d251 6
a256 4
		float *x = new float[3];
		att_err_integral(x);
		mdata = new DataValue(new ArrayDataType(BaseType_FLOAT, 3)) ;
		mdata->setValue( x ) ;
d259 6
a264 4
		float *x = new float[3];
		torque(x);
		mdata = new DataValue(new ArrayDataType(BaseType_FLOAT, 3)) ;
		mdata->setValue( x ) ;
@

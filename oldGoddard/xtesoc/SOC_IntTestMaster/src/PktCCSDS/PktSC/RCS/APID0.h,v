head	1.2;
access
	soccm
	savkoor;
symbols
	Build4_3_1:1.2
	Build4_1:1.2
	Build4:1.2
	Build3_3:1.1
	Build3_2:1.1;
locks;
comment	@ * @;


1.2
date	94.11.03.16.43.17;	author rbarnett;	state Exp;
branches;
next	1.1;

1.1
date	94.09.14.20.57.46;	author rbarnett;	state Exp;
branches;
next	;


desc
@@


1.2
log
@work in progress
@
text
@// ==========================================================================
// File Name   : APID0.h           
// Subsystem   : Spacecraft Data Management
// Programmer  : Randall D. Barnette, Hughes STX (NASA/GSFC code 664)
// Description :
//
//  Class Declaration for Spacecraft apid zero test pattern.
//
// RCS: $Id: APID0.h,v 1.1 1994/09/14 20:57:46 rbarnett Exp rbarnett $
//
// ==========================================================================

#ifndef APID0_H
#define APID0_H

// Rogue Wave headers
#include <rw/collect.h>

// Local headers
#include "PktSC.h"

//
// Forward class declaration needed for the declaration of
// APID0Callback
//
class APID0 ;

//
// Create short names for a pointer to a function that takes a
// constant pointer to a S/C APID0 Partition and returns nothing.
//
typedef void (*APID0Callback) (const APID0*) ;

//
// The currrent size of the data in an APID 0 packet
//
static const int APID0_BUF_SIZE = 56 ;

class APID0 : public RWCollectable  {

  //
  // Declare this class as a RogueWave Collectable. This macro declares some
  // functions necessary for the persistent object Factory
  //
	RWDECLARE_COLLECTABLE(APID0)

public:

	//
	// Constructor to create partition from packet.
	//
	APID0(const PktSC&);

	//
	// Destructor
	//
	~APID0(void);

	//
	// Construct a partition to be filled later.  Needed by RogueWave
	// when building an object from disk.  Should not be explicitly
	// used by clients.
	//
	APID0(void);

	//
	// Access to data
	//
	double time(void) const ;
	unsigned long sequence(void) const ;
	unsigned long apid(void) const ;

	//
	// Generic fetch function
	//
	const
	unsigned char* rawLookup(int offset, int) const ;
	unsigned char  rawLookup(int offset=0) const ;

  //
  // The setDisposition method allows the user to supply a "callback" type
  // function that gets called anytime an APID0 partition is
  // available. It returns the old callback function.
  // The user-supplied function has the following signature
  // void myHandler(const APID0*) ;
  // and it is passed to the system by:
  // APID0Callback oldFunction = APID0::setDisposition(myHandler) ;
	//
  static APID0Callback setDisposition( APID0Callback ) ;

  //
  // Return non-zero of handler function has been defined
  //
  static int hasDisposition(void) ;

	//
	// Rogue Wave comparison methods
	//
	int compareTo(const RWCollectable*) const ;
	RWBoolean isEqual(const RWCollectable*) const ;

	//
	// Rogue Wave persistence methods
	//
	RWspace binaryStoreSize(void) const ;
	void restoreGuts(RWFile&) ;
	void restoreGuts(RWvistream&) ;
	void saveGuts(RWFile&) const ;
	void saveGuts(RWvostream&) const ;

private:

  //
  // These are the packet handler callback functions. They are static because
  // they applies to the class. Default values are zero in which case
  // they does not get called.
  // Typically, only the dispose method can call them, and setDisposition
  // can set them.
  // dispose is private and is only called by PktFDS::apply().
  // PktFDS::apply() is public.
  // setDisposition() is public.
  //
  static APID0Callback theHandler ;

  //
  // dispose checks the value of theHandler and calls it with
  // argument "this" if it exists.
  //
  void dispose(void) const ;

	//
	// IPSDU packet apply method is declared friend so that it can call
	// dispose().
	//
	friend void PktFDS::apply(void) const ;

	// Instance data members
	double        theTime ;
	unsigned long theSeq ;
	unsigned char buf[APID0_BUF_SIZE] ;

};

inline double        APID0::time             (void) const { return theTime ; }
inline unsigned long APID0::sequence         (void) const { return theSeq  ; }
inline unsigned long APID0::apid             (void) const { return 1 ; }

iuc   APID0::rawLookup(int offset     ) const { return buf[offset]; }
icuc* APID0::rawLookup(int offset, int) const { return buf+offset ; }

inline APID0Callback
APID0::setDisposition(APID0Callback newHandler)
{
  APID0Callback temp = theHandler ;
  theHandler = newHandler ;
  return temp ;
}

inline void APID0::dispose(void) const { if(theHandler) theHandler(this);}

inline int APID0::hasDisposition(void) { return theHandler ? 1 : 0 ; }

#endif /* APID0_H */
@


1.1
log
@Initial revision
@
text
@d9 1
a9 1
// RCS: $Id: APID0.h,v 3.2 1994/07/21 22:44:44 rbarnett Exp $
a12 1
//  Feature test switches
d16 4
a19 1
// Include supercalss
d23 1
a23 1
// Incomplete class declaration needed for the declaration of
@

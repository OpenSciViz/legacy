head	4.1;
access
	rbarnett
	soccm
	savkoor;
symbols
	Build4_3_1:4.1
	Build4_1:3.4
	Build4:3.4
	Build3_3:3.2
	Build3_2:3.2
	Build3_1:3.1
	Build3:3.1
	Build2:2.2;
locks;
comment	@ * @;


4.1
date	95.05.25.16.08.11;	author rbarnett;	state Exp;
branches;
next	3.5;

3.5
date	95.05.25.16.00.22;	author rbarnett;	state Exp;
branches;
next	3.4;

3.4
date	95.02.14.19.14.12;	author rbarnett;	state Exp;
branches;
next	3.3;

3.3
date	94.11.03.16.43.17;	author rbarnett;	state Exp;
branches;
next	3.2;

3.2
date	94.09.21.21.17.50;	author rbarnett;	state Exp;
branches;
next	3.1;

3.1
date	94.04.26.20.11.12;	author rbarnett;	state Exp;
branches;
next	3.0;

3.0
date	94.03.18.23.01.21;	author rbarnett;	state Exp;
branches;
next	2.5;

2.5
date	94.03.18.22.49.25;	author rbarnett;	state Exp;
branches;
next	2.4;

2.4
date	94.02.02.20.23.47;	author rbarnett;	state Exp;
branches;
next	2.3;

2.3
date	94.01.13.21.22.42;	author rbarnett;	state Exp;
branches;
next	2.2;

2.2
date	93.11.10.21.13.53;	author rbarnett;	state Release;
branches;
next	2.1;

2.1
date	93.08.31.19.18.59;	author rbarnett;	state Exp;
branches;
next	;


desc
@Creation
@


4.1
log
@Build freeze
@
text
@// =========================================================================
// File Name   : PktSC.h           
// Subsystem   : Spacecraft Data Management
// Programmer  : Randall D. Barnette, Hughes STX (rbarnett@@xema.stx.com)
// Description : Class Declaration for Spacecraft packets.
//
// RCS: $Id: PktSC.h,v 3.5 1995/05/25 16:00:22 rbarnett Exp $
//
// .NAME    PktSC - A Packet class for all XTE spacecraft packets
// .LIBRARY pkts
// .HEADER  Spacecraft Data Management
// .INCLUDE PktSC.h
// .FILE    PktSC.C
// .VERSION $Revision: 3.5 $
// =========================================================================

// .SECTION DESCRIPTION
// PktSC is a subclass of PktTlm. It defines
// an XTE spacecraft telemetry packet. PktSC serves as the base class of
// many spacecraft packet subclasses, as well as the entry point into the
// spacecraft packet factory.

// .SECTION SEE ALSO
// PktACS(3), PktFDS(3), PktIPSDU(3), PktEDSDisc(3)

// .SECTION AUTHOR
// Randall D. Barnette,
// Hughes STX Corp.,
// <rbarnett@@xema.stx.com>

#ifndef PKTSC_H
#define PKTSC_H

// System headers
#include <string.h>

// SOC headers
#include <PktTlm.h>

#if defined(__CLCC__)

//
// "signed" is not implemented in CenterLine and will be
// ignored. However, it generates lots of warnings. Thus...
//
#define signed /* not implemented */

#endif /* __CLCC__ */

//
// This is the value that converts the unsigned long value in the
// spacecraft subseconds field to a valid fractional second.
//
static const double SC_subsecond_conversion = 2.32830643654e-10 ;

//
// Forward class declaration needed for the declaration of
// PktSCCallback
//
class PktSC ;

//
// Create short names for a pointer to a function that takes a
// constant pointer to a S/C Packet and returns nothing.
//
typedef void (*PktSCCallback)(const PktSC*) ;

class PktSC : public PktTlm  {

  //
  // Needed by Rogue Wave for storage and retrieval of persistant objects.
  // This and the RWDECLARE_COLLECTABLE() macro are the only two constructs
  // which are absolutely needed in the .h file of any subclass of PktTlm.
  //
  RWDECLARE_COLLECTABLE(PktSC)

public:

  //* Constructors

  // Construct a packet to be filled later. Needed by RogueWave
  // when building an object from disk. Should not be explicitly
  // used by clients.

  PktSC(void) ;

  // Dummy constructor used for exemplars.

  PktSC(Exemplar) ;

  // Constructor to create packet from data buffer. This should
  // not be called directly as all packet building should use the
  // make() method.

  PktSC(unsigned char* buffer) ;

  //* Destructor

  ~PktSC(void) ;

  //* Virtual Member Functions

  // Each subclass should provide an appropriate one of these.

  virtual void apply(void) const ;

  // This function is used to parse a packet from a data buffer.
  // All subclasses should provide an appropriate one of these.
  // It returns a packet of the appropriate type or subtype.

  virtual PktTlm* make(unsigned char* buffer) ;

  //% The setDisposition method allows the user to supply a "callback" type
  //% function that gets called anytime a spacecraft packet is
  //% available. It returns the old callback function.
  //% The user-supplied function has the following signature
  //%
  //% void myHandler(const PktSC*) ;
  //%
  //% and it is passed to the system by:
  //%
  //% PktSCCallback oldFunction = PktSC::setDisposition(myHandler) ;

  static PktSCCallback setDisposition(PktSCCallback newHandler) ;

  //* Public Conversion routines
  //% The first arg is the byte offset to the item.
  //% The second arg (if present) is the item size in bits.
  //% The third arg (if present) is the bit offset within the byte.
  //%
  //% These conform to the descriptions in the Data Format Control
  //% Document (DFCD) for the Project Data Base (PDB) Supporting
  //% the X-Ray Timing Explorer (XTE) Mission Operations Center (MOC)

  unsigned char   UB(  int B, int s=8, int b=0) const ;
  unsigned short  UI(  int B, int s=16)         const ;
  unsigned long   ULI( int B, int s=32)         const ;
  signed   char   OB(  int B)                   const ;
  signed   char   SB(  int B)                   const ;
  signed   short  OI(  int B)                   const ;
  signed   short  SI(  int B)                   const ;
  signed   long   OLI( int B)                   const ;
  signed   long   SLI( int B)                   const ;
           char*  CHAR(int B, int s=64)         const ;
           float  SFP( int B)                   const ;
           double DFP( int B)                   const ;
           double TIME(int B)                   const ;
           double MET( int B)                   const ;
           double DTIM(int B)                   const ;
  unsigned long   STIM(int B)                   const ;
  unsigned long   SMET(int B)                   const ;

  //* Static Conversion routines.

  //% These assume the first argument
  //% is the packet application data.
  //% The second arg is the byte offset to the item.
  //% The third arg (if present) is the item size in bits.
  //% The fourth arg (if present) is the bit offset within the byte.

  static unsigned char   UB(  const unsigned char* c, int B, int s=8, int b=0) ;
  static unsigned short  UI(  const unsigned char* c, int B, int s=16) ;
  static unsigned long   ULI( const unsigned char* c, int B, int s=32) ;
  static signed   char   OB(  const unsigned char* c, int B) ;
  static signed   char   SB(  const unsigned char* c, int B) ;
  static signed   short  OI(  const unsigned char* c, int B) ;
  static signed   short  SI(  const unsigned char* c, int B) ;
  static signed   long   OLI( const unsigned char* c, int B) ;
  static signed   long   SLI( const unsigned char* c, int B) ;
  static          char*  CHAR(const unsigned char* c, int B, int s=64) ;
  static          float  SFP( const unsigned char* c, int B) ;
  static          double DFP( const unsigned char* c, int B) ;
  static          double TIME(const unsigned char* c, int B) ;
  static          double MET( const unsigned char* c, int B) ;
  static          double DTIM(const unsigned char* c, int B) ;
  static unsigned long   STIM(const unsigned char* c, int B) ;
  static unsigned long   SMET(const unsigned char* c, int B) ;

private:

  //% This is the packet handler callback function. It is static because
  //% it applies to the class. Default value is zero in which case
  //% it does not get called.
  //% Typically, only the dispose method can call it, and setDisposition
  //% can set it.
  //% dispose() is private and is only called by PktSC::apply().
  //% PktSC::apply() is public.
  //% setDisposition() is public.

  static PktSCCallback theHandler ;

protected:

  // dispose checks the value of theHandler and calls it with
  // argument "this" if it exists.

  void dispose(void) const ;

};

// Description:
// Sets the partition disposition handler to newHandler. Returns the old
// handler.

inline PktSCCallback PktSC::setDisposition(PktSCCallback newHandler)
{
  PktSCCallback temp = theHandler ;
  theHandler = newHandler ;
  return temp ;
}

// Description:
// Calls the partition disposition handler if it exists.

inline void PktSC::dispose(void) const { if(theHandler) theHandler(this);}

#endif /* PKTSC_H */
@


3.5
log
@update to genman text
@
text
@d7 1
a7 1
// RCS: $Id: PktSC.h,v 3.4 1995/02/14 19:14:12 rbarnett Exp $
d14 1
a14 1
// .VERSION $Revision$
@


3.4
log
@genman in progress
@
text
@d7 1
a7 1
// RCS: $Id: PktSC.h,v 3.3 1994/11/03 16:43:17 rbarnett Exp $
d14 1
a14 1
// .VERSION XTE-SOC V4.1
d19 3
a21 1
// an XTE spacecraft telemetry packet.
d24 1
d37 2
a38 2
// Local headers
#include "PktTlm.h"
d101 2
d138 1
d140 1
d142 1
d154 1
d164 1
d166 1
d168 1
a178 9
#if defined(ONES_COMPLEMENT_SC)
  static signed   char   OB(  const unsigned char* c, int B) ;
  static signed   short  OI(  const unsigned char* c, int B) ;
  static signed   long   OLI( const unsigned char* c, int B) ;
         signed   char   OB(  int B)                   const ;
         signed   short  OI(  int B)                   const ;
         signed   long   OLI( int B)                   const ;
#endif

@


3.3
log
@Added apids for Build 4
@
text
@d7 1
a7 1
// RCS: $Id: PktSC.h,v 3.2 1994/09/21 21:17:50 rbarnett Exp $
d9 6
d17 11
d54 1
a54 1
// Incomplete class declaration needed for the declaration of
d76 1
a76 4
  //
  // Dummy constructor used for exemplars.
  //
  PktSC(Exemplar) ;
a77 1
  //
d81 1
a81 1
  //
d84 4
a87 1
  //
d91 4
a94 2
  //
  PktSC(unsigned char*) ;
a95 3
  //
  // Destructor
  //
a97 1
  //
d99 1
a99 1
  //
a101 1
  //
a104 2
  //
  virtual PktTlm* make(unsigned char*) ;
d106 1
a106 10
  //
  // The setDisposition method allows the user to supply a "callback" type
  // function that gets called anytime a spacecraft packet is
  // available. It returns the old callback function.
  // The user-supplied function has the following signature
  // void myHandler(const PktSC*) ;
  // and it is passed to the system by:
  // PktSCCallback oldFunction = PktSC::setDisposition(myHandler) ;
  //
  static PktSCCallback setDisposition( PktSCCallback ) ;
d108 58
a165 29
  //
  // Public Conversion routines.
  // The first arg is the byte offset to the item.
  // The second arg (if present) is the item size in bits.
  // The third arg (if present) is the bit offset within the byte.
  //
  // These conform to the descriptions in the Data Format Control
  // Document (DFCD) for the Project Data Base (PDB) Supporting
  // the X-Ray Timing Explorer (XTE) Mission Operations Center (MOC)
  //
  unsigned char   UB(  int, int=8, int=0) const ;
  unsigned short  UI(  int, int=16)       const ;
  unsigned long   ULI( int, int=32)       const ;
#if defined(ONES_COMPLEMENT_SC)
  signed   char   OB(  int)               const ;
  signed   short  OI(  int)               const ;
  signed   long   OLI( int)               const ;
#endif
  signed   char   SB(  int)               const ;
  signed   short  SI(  int)               const ;
  signed   long   SLI( int)               const ;
           char*  CHAR(int, int=64)       const ;
           float  SFP( int)               const ;
           double DFP( int)               const ;
           double TIME(int)               const ;
           double MET( int)               const ;
           double DTIM(int)               const ;
  unsigned long   STIM(int)               const ;
  unsigned long   SMET(int)               const ;
a166 10
  //
  // Static Conversion routines. These assume the first argument
  // is the packet application data.
  // The second arg is the byte offset to the item.
  // The third arg (if present) is the item size in bits.
  // The fourth arg (if present) is the bit offset within the byte.
  //
  static unsigned char   UB(  const unsigned char*, int, int=8, int=0) ;
  static unsigned short  UI(  const unsigned char*, int, int=16) ;
  static unsigned long   ULI( const unsigned char*, int, int=32) ;
d168 6
a173 3
  static signed   char   OB(  const unsigned char*, int) ;
  static signed   short  OI(  const unsigned char*, int) ;
  static signed   long   OLI( const unsigned char*, int) ;
a174 11
  static signed   char   SB(  const unsigned char*, int) ;
  static signed   short  SI(  const unsigned char*, int) ;
  static signed   long   SLI( const unsigned char*, int) ;
  static          char*  CHAR(const unsigned char*, int, int=64) ;
  static          float  SFP( const unsigned char*, int) ;
  static          double DFP( const unsigned char*, int) ;
  static          double TIME(const unsigned char*, int) ;
  static          double MET( const unsigned char*, int) ;
  static          double DTIM(const unsigned char*, int) ;
  static unsigned long   STIM(const unsigned char*, int) ;
  static unsigned long   SMET(const unsigned char*, int) ;
d178 9
a186 10
  //
  // This is the packet handler callback function. It is static because
  // it applies to the class. Default value is zero in which case
  // it does not get called.
  // Typically, only the dispose method can call it, and setDisposition
  // can set it.
  // dispose() is private and is only called by PktSC::apply().
  // PktSC::apply() is public.
  // setDisposition() is public.
  //
a190 1
  //
d193 1
a193 1
  //
d198 4
d209 3
@


3.2
log
@Updated save/restore to RW 6 specs
@
text
@d7 1
a7 1
// RCS: $Id: PktSC.h,v 3.1 1994/04/26 20:11:12 rbarnett Exp rbarnett $
d50 6
a55 6
	//
	// Needed by Rogue Wave for storage and retrieval of persistant objects.
	// This and the RWDECLARE_COLLECTABLE() macro are the only two constructs
	// which are absolutely needed in the .h file of any subclass of PktTlm.
	//
	RWDECLARE_COLLECTABLE(PktSC)
d59 3
a61 3
	//
	// Dummy constructor used for exemplars.
	//
d78 3
a80 3
	//
	// Destructor
	//
d108 7
a114 7
	// The first arg is the byte offset to the item.
	// The second arg (if present) is the item size in bits.
	// The third arg (if present) is the bit offset within the byte.
	//
	// These conform to the descriptions in the Data Format Control
	// Document (DFCD) for the Project Data Base (PDB) Supporting
	// the X-Ray Timing Explorer (XTE) Mission Operations Center (MOC)
d117 18
a134 16
  unsigned short  UI(  int, int=16)      const ;
  unsigned long   ULI( int, int=32)      const ;
  signed   char   OB(  int, int=8, int=0) const ;
  signed   short  OI(  int, int=16)      const ;
  signed   long   OLI( int, int=32)      const ;
  signed   char   SB(  int, int=8, int=0) const ;
  signed   short  SI(  int, int=16)      const ;
  signed   long   SLI( int, int=32)      const ;
           char*  CHAR(int, int=64)      const ;
           float  SFP( int)           const ;
           double DFP( int)           const ;
           double TIME(int)           const ;
           double MET( int)           const ;
           double DTIM(int)           const ;
  unsigned long   STIM(int)           const ;
  unsigned long   SMET(int)           const ;
d138 4
a141 4
	// is the packet application data.
	// The second arg is the byte offset to the item.
	// The third arg (if present) is the item size in bits.
	// The fourth arg (if present) is the bit offset within the byte.
d146 8
a153 6
  static signed   char   OB(  const unsigned char*, int, int=8, int=0) ;
  static signed   short  OI(  const unsigned char*, int, int=16) ;
  static signed   long   OLI( const unsigned char*, int, int=32) ;
  static signed   char   SB(  const unsigned char*, int, int=8, int=0) ;
  static signed   short  SI(  const unsigned char*, int, int=16) ;
  static signed   long   SLI( const unsigned char*, int, int=32) ;
@


3.1
log
@Made S/C code consistent for Build 3
@
text
@d2 1
a2 1
// Program Name: PktSC.h           
d4 2
a5 2
// Programmer  : Randall D. Barnette, Hughes STX (NASA/GSFC code 664)
// Description :
d7 1
a7 1
//  Class Declaration for Spacecraft packets
a8 2
// RCS: $Id: PktSC.h,v 3.0 1994/03/18 23:01:21 rbarnett Exp $
//
a10 1
//  Feature test switches
d14 1
d16 2
a17 3
//
// Include Packet Superclass
//
@


3.0
log
@NewBuild
@
text
@d9 1
a9 1
// RCS: $Id: PktSC.h,v 2.5 1994/03/18 22:49:25 rbarnett Exp rbarnett $
d17 4
d23 16
d84 1
a84 1
  ~PktSC( void ) ;
a87 1
  // The default one here does nothing.
d109 53
@


2.5
log
@*** empty log message ***
@
text
@d9 1
a9 1
// RCS: $Id: PktSC.h,v 2.4 1994/02/02 20:23:47 rbarnett Exp $
@


2.4
log
@removed sc_ids.h from public view
@
text
@d1 1
d4 1
a4 1
// Programmer  : Randall D. Barnette, Hughes STX (NAASA/GSFC code 664)
d9 1
a9 1
// RCS: $Id: PktSC.h,v 2.3 1994/01/13 21:22:42 rbarnett Exp $
d11 1
d19 12
d42 23
a64 1
  PktSC(Exemplar) ;  // Dummy constructor used for exemplars.
a66 1
  // This methods assembles the packets into TMData sets.
d77 12
a88 1
  virtual PktTlm* make( unsigned char* buffer ) ;
d90 1
a90 4
	//
	// Destructor
	//
  ~PktSC( void ) ;
d93 8
a100 3
  // Construct a packet to be filled later. Needed by RogueWave
  // when building an object from disk. Should not be explicitly
  // used by clients.
d102 3
a104 1
  PktSC( void ) ;
d107 2
a108 3
  // Constructor to create packet from data buffer. This should
  // not be called directly as all packet building should use the
  // make() method.
d110 1
a110 1
  PktSC( unsigned char* buffer ) ;
d114 10
a123 1
#endif
@


2.3
log
@Added disposition handlers
@
text
@d8 1
a8 1
// RCS: $Id: PktSC.h,v 2.2 1993/11/10 21:13:53 rbarnett Release $
a15 1
#include "sc_ids.h"
@


2.2
log
@*** empty log message ***
@
text
@d8 1
a8 1
// RCS: $Id: PktSC.h,v 2.1 1993/08/31 19:18:59 rbarnett Exp rbarnett $
a14 5
//
// The DEC stations need this.
//
#include <sys/types.h>

a17 1

d20 6
a25 6
//
// Needed by Rogue Wave for storage and retrieval of persistant objects.
// This and the RWDECLARE_COLLECTABLE() macro are the only two constructs
// which are absolutely needed in the .h file of any subclass of PktTlm.
//
RWDECLARE_COLLECTABLE(PktSC)
d45 3
@


2.1
log
@Creation
@
text
@d8 1
a8 1
// RCS: $Id: PktSC.h,v 2.1 1993/08/09 13:42:26 rbarnett Exp rbarnett $
d42 1
a42 1
  virtual void apply(void);
@

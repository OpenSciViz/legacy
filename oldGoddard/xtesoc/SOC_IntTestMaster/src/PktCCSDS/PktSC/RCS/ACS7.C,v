head	4.2;
access
	soccm
	savkoor;
symbols
	Build4_3_1:4.1
	Build4_1:1.1
	Build4:1.1;
locks;
comment	@ * @;


4.2
date	97.03.21.15.34.18;	author soccm;	state Exp;
branches;
next	4.1;

4.1
date	95.05.25.16.08.11;	author rbarnett;	state Exp;
branches;
next	1.1;

1.1
date	94.12.01.14.23.52;	author rbarnett;	state Exp;
branches;
next	;


desc
@ACS-7 partition class
@


4.2
log
@removed items and updated to PDB specs
@
text
@// ==========================================================================
// File Name   : ACS7.C           
// Subsystem   : Spacecraft Data Management
// Programmer  : Randall D. Barnette, Hughes STX (rbarnett@@xema.stx.com)
// Description : Implementation of the ACS7 partition class
//
// RCS:
static const char rcsid[] =
"$Id: ACS7.C,v 4.1 1995/05/25 16:08:11 rbarnett Exp $";
//
// ==========================================================================

// System headers
#include <string.h>

// Local headers
#include <ACS7.h>

// Private headers
#include "sc_ids.h"

//
// The following is needed by the RogueWave.                              
//
RWDEFINE_COLLECTABLE(ACS7, ACS7_ID)

//
// Allocate space for static members
//
ACS7Callback ACS7::theHandler = 0 ;

//
// This is the default constructor which is needed by Rogue Wave
//
ACS7::ACS7(void) {}

// Description:
// Constructor takes PktACS argument. The packet contents are
// copied into an internal buffer.

ACS7::ACS7(const PktACS& thePacket):theSeq(thePacket.sequenceNumber())
{
	// Should really check to see that apid == 21!
  theTime = (double) (thePacket.seconds()) +
            ((double) thePacket.subSeconds()) * SC_subsecond_conversion ;
  memcpy(buf, thePacket.applicationData(), ACS7_BUF_SIZE ) ;
}

// Description:
// The destructor releases all memory associated with the partition.

ACS7::~ACS7(void) {}

// ----------------------------------------------------
// The following are virtual methods from RWCollectable
// ----------------------------------------------------

int ACS7::compareTo(const RWCollectable* c) const
{
	if( c->isA() != ACS7_ID ) return -1 ;
	const ACS7* rhs = (const ACS7*) c ;
	return theTime<rhs->theTime ? -1 : theTime>rhs->theTime ? 1 : 0 ;
}

RWBoolean ACS7::isEqual(const RWCollectable* c) const
{
	return compareTo(c) ? FALSE : TRUE ;
}

RWspace ACS7::binaryStoreSize(void) const
{
  return RWCollectable::binaryStoreSize() +
         sizeof(double)        + // theTime
	       sizeof(unsigned long) + // theSeq
	       ACS7_BUF_SIZE         ; // data buffer
}

void ACS7::restoreGuts(RWFile& f)
{
  RWCollectable::restoreGuts(f) ;
  f.Read(theTime) ;
  f.Read(theSeq) ;
  f.Read(buf, ACS7_BUF_SIZE ) ;
}

void ACS7::restoreGuts(RWvistream& s)
{
  RWCollectable::restoreGuts(s) ;
	s >> theTime >> theSeq ;
  s.get(buf, ACS7_BUF_SIZE ) ;
}

void ACS7::saveGuts(RWFile& f) const
{
  RWCollectable::saveGuts(f) ;
  f.Write(theTime) ;
  f.Write(theSeq) ;
  f.Write(buf,ACS7_BUF_SIZE) ;
}

void ACS7::saveGuts(RWvostream& s) const
{
  RWCollectable::saveGuts(s) ;
	s << theTime << theSeq ;
  s.put(buf, ACS7_BUF_SIZE ) ;
}

@


4.1
log
@Build freeze
@
text
@d9 1
a9 1
"$Id: ACS7.C,v 1.1 1994/12/01 14:23:52 rbarnett Exp $";
d17 1
a17 1
#include "ACS7.h"
@


1.1
log
@Initial revision
@
text
@d9 1
a9 1
"$Id: ACS7.C,v 1.4 1994/09/21 21:17:50 rbarnett Exp $";
@

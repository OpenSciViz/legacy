head	1.2;
access
	soccm
	savkoor;
symbols
	Build4_3_1:1.2
	Build4_1:1.2
	Build4:1.2
	Build3_3:1.2
	Build3_2:1.2
	Build3_1:1.2
	Build3:1.2;
locks;
comment	@ * @;


1.2
date	94.06.21.22.20.01;	author rbarnett;	state Exp;
branches;
next	1.1;

1.1
date	94.04.26.20.11.12;	author rbarnett;	state Exp;
branches;
next	;


desc
@spacecraft realtime client
@


1.2
log
@Fixed use of SCDataTable.h
@
text
@//
// $Id: scRT.C,v 1.1 1994/04/26 20:11:12 rbarnett Exp $
//
// This is the only module in the rtclient code which should need to be
// modified by realtime client writers.  Simply replace the innards of
// client_specific_initialization() and process_a_packet() with the correct
// code for your situation.  A few of the other functions in here may be
// tweaked where explicitly mentioned.
//

#include <iostream.h>
#include <iomanip.h>
#include <new.h>
#include <signal.h>
#include <stdarg.h>
#include <stdlib.h>
#include <unistd.h>

#include "PktTlm.h"
#include "SCDataCache.h"
#include "Utilities.h"
#include "rtclient.h"  /* Entry points into librtclient.a */

// Include class declarations
#include "AccGenericSC.h"

#include "ACS0.h"
#include "ACS1.h"
#include "ACS2.h"
#include "IPSDUQuick.h"
#include "IPSDUStatus.h"
#include "IPSDUTemp.h"
#include "FDSMIFast.h"

//
// To work around a template bug.
//
#include "Descriptor.h"

static void doAccess( const Accessor& a, const Descriptor& d )
{
  DataValue* temp = a.access(d) ;
  if( temp ) {
    if( d.housekeeping().isNull() )
      cout << d << " is " << *temp << endl ;
    else
      cout << d.housekeeping() << " is " << *temp << endl ;
  }
  else
    cout << d << " is not available" << endl ;
  delete temp ;
}

static void pktHandler(const PktSC* thePkt)
{
  AccGenericSC acc(*thePkt) ;
  const Descriptor d1("APPID") ;
  const Descriptor d2("ALL") ;

  cout << d1 << " is " << *(acc.access(d1)) << endl ;
  //cout << d2 << " is " << *(acc.access(d2)) << endl ;
  const RWOrdered* des = acc.descriptors() ;
  int x = des->entries() ;
  for( int i = 0 ; i < x ; i++ )
    doAccess( acc, *((Descriptor*)(des->at(i))) ) ;

  return ;
}

static void acs0Handler(const ACS0* thePart)
{
  cout << "q = "
       << "[ " << setw(12) << thePart->qx()
       << ", " << setw(12) << thePart->qy()
       << ", " << setw(12) << thePart->qz()
       << ", " << setw(12) << thePart->qs()
       << " ]" << endl ;
  return ;
}
static void acs1Handler(const ACS1* thePart)
{
  cout << "rate = "
       << "[ " << setw(12) << thePart->rateX()
       << ", " << setw(12) << thePart->rateY()
       << ", " << setw(12) << thePart->rateZ()
       << " ]" << endl ;
  return ;
}
static void acs2Handler(const ACS2* thePart)
{
  cout << "pos = "
       << "[ " << setw(12) << thePart->positionX()
       << ", " << setw(12) << thePart->positionY()
       << ", " << setw(12) << thePart->positionZ()
       << " ]" << endl
       << "vel = "
       << "[ " << setw(12) << thePart->velocityX()
       << ", " << setw(12) << thePart->velocityY()
       << ", " << setw(12) << thePart->velocityZ()
       << " ]" << endl ;
  return ;
}
static void ipsduQHandler(const IPSDUQuick* thePart)
{
  cout << "quick = "
       << "[ " << (int) thePart->hexte1Current()
       << ", " << (int) thePart->hexte2Current()
       << ", " << (int) thePart->edsAsm1Current()
       << ", " << (int) thePart->edsAsm2Current()
       << " ]" << endl ;
  return ;
}
static void ipsduSHandler(const IPSDUStatus* thePart)
{
  cout << "status = "
       << "[ " << (int) thePart->eds1V()
       << ", " << (int) thePart->eds2V()
       << " ]" << endl ;
  return ;
}
static void ipsduTHandler(const IPSDUTemp* thePart)
{
  cout << "temp = "
       << "[ " << (int) thePart->pcu1Temp1()
       << ", " << (int) thePart->pcu2Temp1()
       << ", " << (int) thePart->pcu3Temp1()
       << ", " << (int) thePart->pcu4Temp1()
       << ", " << (int) thePart->pcu5Temp1()
       << " ]" << endl ;
  return ;
}
static void fds1Handler(const FDSMIFast* thePart)
{
  double time = thePart->time() ;
  double utcf = thePart->UTCF() ;
  double ctime = time+utcf ;
  double dtime = ctime/86400.0 ;
  cout << "time = "
       << "[ "  << utcf
       << " + " << time
       << " = " << ctime
       << " ("  << dtime
       << ") ]" << endl ;
  return ;
}

//
// The port of the Realtime Server to which we connect to get telemetry.
//
static short server_port;


//
// The machine on which the Realtime Server is executing.
//
static const char* server_host;


//
// Should we run as a daemon?
//
static int run_as_daemon;


//
// Should we read packets from STDIN instead of from the Realtime Server?
//
static int read_from_stdin;


//
// This is the name we advertise to the Realtime Server.  It is the same as
// the entry in the relevant $SOCHOME/etc/RT.CLIENTS table.  You will probably
// want to hardcode the standard name for your client here, instead of relying
// on command line arguments and parse_arguments() to set it.
//
static const char* client_name;


//
// Our name -- set in main().  Not necessarily the same as the name we
// advertise to the Realtime Server.  This is used for reporting of errors
// and messages.
//
static char* ProgName;

//
// This is the routine that does the packet-specific processing.  Do not
// change the signature of this function.  Each time one of the packets in
// which you've specified an interest is received by the realtime client
// process, this function is called -- this includes any packets containing
// S/C data in which you've indicated an interest in receiving, as well.
// The caching of S/C data is done for you in the librtclient.a code, but only
// if you specify that you want it cached.  You can specify that you want
// to get S/C data from the Realtime Server which you must then choose whether
// you want to cache it or not.  You really have three options when you're
// receiving spacecraft data:
//
//   1) cache it and ignore the S/C packets themselves;
//   2) just deal with the raw S/C packets;
//   3) both (1) & (2).
//
// For non-spacecraft data you must deal directly with the packets.
//

void process_a_packet (const PktTlm* pkt)
{
    //
    // You've got a valid PktTlm which will be the virtually correct type.
    // You should probably check the application ID to make sure it is one
    // of the ones that you want, just as a double check.  Then do with it
    // as you will.  If you need to cache the packet you must make a copy of
    // it.  The routine that calls process_a_packet() deletes the packet
    // when this function returns.
    //

    int appid = pkt->applicationID() ;

    if(
      ( appid >= 1 && appid <= 12 ) ||
      ( appid >= 14 && appid <= 32 ) ||
      ( appid >= 40 && appid <= 44 ) ||
      ( appid >= 121 && appid <= 125 ) ||
      ( appid >= 196 && appid <= 201 ) ||
      ( appid >= 202 && appid <= 211 ) ||
      0 ) pkt->apply() ;

  else
    cout << "Unrecognized packet in spacecraft realtime client" << endl
         << "App Id = "       << pkt->applicationID()
         << ", SeqNo = "      << pkt->sequenceNumber()
         << ", Length = "     << pkt->packetLength()
         << ", Secs = "       << pkt->seconds() << endl;

  return ;
}


//
// Put any specific initialization stuff you need into here.  This is the place
// to specify the types of S/C data to be cached.  Do not change the signature
// of this function.
//

static void client_specific_initialization (void)
{

  PktSC::setDisposition( pktHandler ) ;

  ACS0::setDisposition( acs0Handler ) ;
  ACS1::setDisposition( acs1Handler ) ;
  ACS2::setDisposition( acs2Handler ) ;
  IPSDUQuick::setDisposition( ipsduQHandler ) ;
  IPSDUStatus::setDisposition( ipsduSHandler ) ;
  IPSDUTemp::setDisposition( ipsduTHandler ) ;

  FDSMIFast::setDisposition( fds1Handler ) ;

}


//
// Print out usage string and exit.  If you change parse_arguments(),
// make the relevant changes here as well.
//

static void usage (void)
{
    cerr << "\nusage: " << ProgName
         << " [-daemonize]"
         << " -host hostname"
         << " -name client-name"
         << " [-port #]"
         << " [-read-from-stdin]\n\n";
    exit(1);
}


//
// Parse our arguments.  Feel free to add your own client-specific options.
// If you do so, make sure to update usage() appropriately.
//

static void parse_arguments (char**& argv)
{
    //
    // Parse any arguments.
    //
    //    -daemonize           -- run as a daemon
    //    -display-client name -- name of X client program to execute
    //    -host machine        -- realtime server machine
    //    -port #              -- port number to connect torealtime server
    //    -name name           -- the name we advertise to the realtime server
    //    -read-from-stdin     -- read from stdin instead of over a socket
    //
    while (*++argv && **argv == '-')
    {
        if (strcmp(*argv, "-host") ==  0)
        {
            if (!(server_host = *++argv))
                error("No hostname supplied.");
        }
        else if (strcmp(*argv, "-port") == 0)
        {
            if (*++argv)
            {
                if ((server_port = (short) atoi(*argv)) <= 0)
                    error("Port # must be positive short.");
                continue;
            }
            else
                error("No port # supplied.");
        }
        else if (strcmp(*argv, "-daemonize") == 0)
            run_as_daemon = 1;

        else if (strcmp(*argv, "-name") ==  0) {
            if (!(client_name = *++argv))
                error("No name supplied.");
        }
        else if (strcmp(*argv, "-read-from-stdin") == 0)
            read_from_stdin = 1;
        else
        {
            message("`%s' is not a valid option, exiting ...", *argv);
            usage();
        }
    }

    if (read_from_stdin && run_as_daemon)
    {
        run_as_daemon = 0;
        message("Using `-read-from-stdin' implies ! `-daemonize'.");

    }

    if (!server_host && !read_from_stdin)
    {
        message("`realtime-server-host' or `read-from-stdin' must be specified.");
        usage();
    }

    if (!read_from_stdin && !client_name)
        usage();

    //
    // Check that basic environment variables are set.
    //
    if (getenv("SOCHOME") == 0)
        error("The `SOCHOME' environment variable must be set.");

    if (getenv("SOCOPS") == 0)
        error("The `SOCOPS' environment variable must be set.");

}


//
// Simple error handler for use by PktCCSDS.  Change this if you want
// to do something fancier.
//

static void packet_error_handler (const char* msg) { error(msg); }


//
// A simple Out-Of-Memory Exception handler for main().  Feel free
// to change this if you really can do better than exit.
//

static void free_store_exception (void)
{
#ifdef ATEXIT
    error("Out of memory, exiting ...");
#else
    message("Out of memory, exiting ...");
    if (!read_from_stdin)
        force_exit_of_other_half();
    exit(1);
#endif
}


//
// The main routine.  Please do not change this without very good reason.
// client_specific_initialization() is placed such that you should be able
// to override many of my assumptions if you so choose.
//

int main (int, char* argv[])
{
    ProgName = argv[0];

    set_new_handler(free_store_exception);

    PktCCSDS::setErrorHandler(packet_error_handler);

    initialize_signal_handlers(catch_a_signal);

    parse_arguments(argv);

    if (run_as_daemon)
        //
        // Feel free to daemonize to someplace other than the filesystem
        // root if it makes sense for your application.
        //
        daemonize(client_name, "/");

#ifdef ATEXIT
    if (!read_from_stdin)
        if (atexit(force_exit_of_other_half))
            error("File %s, line %d, atexit() failed.",
                  __FILE__, __LINE__);
#endif

    if (read_from_stdin)
    {
        //
        // Do client-specific setup.
        //
        client_specific_initialization();
        read_packets_from_stdin();
    }
    else
    {
        //
        // Now fork to create a child process. The parent process
        // (aka back-end) reads packets from the Realtime Server and
        // places them in the ring buffer.  The child process
        // (aka front-end) reads these packets and does the appropriate
        // processing.
        //
        if ((PID = fork()) == 0)
        {
            //
            // This code is only executed by the front-end portion of the
            // realtime client; that part that is actually interested in 
            // the realtime analysis/display of the data in the packets.
            //
            PPID = getppid();
            //
            // Do client-specific setup.
            //
            client_specific_initialization();
            //
            // The primitive event mechanism in process_packets() isn't
            // compatible with the X Event mechanism.  If you want to drive
            // X Windows directly from this code, instead of fork()ing and
            // exec()ing a standalone X Windows process to which you write
            // the required data, please talk to me so we can work out the
            // best way to do it, together.  I have some ideas.  I just
            // haven't felt like putting in the time, if it wasn't clear
            // that it was going to be used.
            //
            process_packets();
        }
        else
            //
            // This code is only executed by the back-end of the realtime
            // client.  It is the code that maintains the connection to the
            // realtime server and gives well-built CCSDS Telemetry packets
            // to the front-end of the realtime client.
            //
            connect_to_realtime_server(client_name, server_port, server_host);
    }

    return 0;
}
@


1.1
log
@Initial revision
@
text
@d2 1
a2 1
// $Id: ipsduRT.C,v 3.0 1994/03/18 23:08:24 rbarnett Exp $
d42 10
a51 10
	DataValue* temp = a.access(d) ;
	if( temp ) {
		if( d.housekeeping().isNull() )
			cout << d << " is " << *temp << endl ;
		else
			cout << d.housekeeping() << " is " << *temp << endl ;
	}
	else
		cout << d << " is not available" << endl ;
	delete temp ;
d56 10
a65 3
	AccGenericSC acc(*thePkt) ;
	const Descriptor d1("APPID") ;
	const Descriptor d2("ALL") ;
d67 1
a67 4
	cout << d1 << " is " << *(acc.access(d1)) << endl ;
	cout << d2 << " is " << *(acc.access(d2)) << endl ;

	return ;
d72 7
a78 7
	cout << "q = "
	     << "[ " << setw(12) << thePart->qx()
	     << ", " << setw(12) << thePart->qy()
	     << ", " << setw(12) << thePart->qz()
	     << ", " << setw(12) << thePart->qs()
	     << " ]" << endl ;
	return ;
d82 6
a87 6
	cout << "rate = "
	     << "[ " << setw(12) << thePart->rateX()
	     << ", " << setw(12) << thePart->rateY()
	     << ", " << setw(12) << thePart->rateZ()
	     << " ]" << endl ;
	return ;
d91 11
a101 11
	cout << "pos = "
	     << "[ " << setw(12) << thePart->positionX()
	     << ", " << setw(12) << thePart->positionY()
	     << ", " << setw(12) << thePart->positionZ()
	     << " ]" << endl
	     << "vel = "
	     << "[ " << setw(12) << thePart->velocityX()
	     << ", " << setw(12) << thePart->velocityY()
	     << ", " << setw(12) << thePart->velocityZ()
	     << " ]" << endl ;
	return ;
d105 7
a111 7
	cout << "quick = "
	     << "[ " << (int) thePart->hexte1Current()
	     << ", " << (int) thePart->hexte2Current()
	     << ", " << (int) thePart->edsAsm1Current()
	     << ", " << (int) thePart->edsAsm2Current()
	     << " ]" << endl ;
	return ;
d115 5
a119 5
	cout << "status = "
	     << "[ " << (int) thePart->eds1V()
	     << ", " << (int) thePart->eds2V()
	     << " ]" << endl ;
	return ;
d123 8
a130 8
	cout << "temp = "
	     << "[ " << (int) thePart->pcu1Temp1()
	     << ", " << (int) thePart->pcu2Temp1()
	     << ", " << (int) thePart->pcu3Temp1()
	     << ", " << (int) thePart->pcu4Temp1()
	     << ", " << (int) thePart->pcu5Temp1()
	     << " ]" << endl ;
	return ;
d134 11
a144 11
	double time = thePart->time() ;
	double utcf = thePart->UTCF() ;
	double ctime = time+utcf ;
	double dtime = ctime/86400.0 ;
	cout << "time = "
	     << "[ "  << utcf
	     << " + " << time
	     << " = " << ctime
	     << " ("  << dtime
	     << ") ]" << endl ;
	return ;
d217 1
a217 1
		int appid = pkt->applicationID() ;
d219 15
a233 27
		if(
#ifdef APID_FDS
			( appid >= 1 && appid <= 12 ) ||
#endif
#ifdef APID_ACS
			( appid >= 14 && appid <= 32 ) ||
#endif
#ifdef APID_DEPLOY
			( appid >= 40 && appid <= 44 ) ||
#endif
#ifdef APID_SPSDU
			( appid >= 121 && appid <= 125 ) ||
#endif
#ifdef APID_IPSDU
			( appid >= 196 && appid <= 201 ) ||
#endif
#ifdef APID_PSE
			( appid >= 202 && appid <= 211 ) ||
#endif
			0 ) pkt->apply() ;

	else
		cout << "Unrecognized packet in spacecraft realtime client" << endl
				 << "App Id = "       << pkt->applicationID()
				 << ", SeqNo = "      << pkt->sequenceNumber()
				 << ", Length = "     << pkt->packetLength()
				 << ", Secs = "       << pkt->seconds() << endl;
d235 1
a235 1
	return ;
d248 1
a248 1
	PktSC::setDisposition( pktHandler ) ;
d250 6
a255 6
	ACS0::setDisposition( acs0Handler ) ;
	ACS1::setDisposition( acs1Handler ) ;
	ACS2::setDisposition( acs2Handler ) ;
	IPSDUQuick::setDisposition( ipsduQHandler ) ;
	IPSDUStatus::setDisposition( ipsduSHandler ) ;
	IPSDUTemp::setDisposition( ipsduTHandler ) ;
d257 1
a257 1
	FDSMIFast::setDisposition( fds1Handler ) ;
@

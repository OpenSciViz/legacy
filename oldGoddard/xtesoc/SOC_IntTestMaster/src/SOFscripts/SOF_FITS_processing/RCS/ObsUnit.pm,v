head	1.1;
access;
symbols;
locks; strict;
comment	@# @;


1.1
date	2000.04.17.16.06.18;	author jonv;	state Exp;
branches;
next	;


desc
@class of observation objects to be stored in a Berkeley DB by the
makefits.pl program
@


1.1
log
@Initial revision
@
text
@# ===========================================================================
# Filename    : ObsUnit.pm
# Subsystem   : SOFScripts
# Programmer  : Jon Vandegriff, Raytheon ITSS
# Description : a Perl class representing the data for each obsid 
#               which is stored in a database used for FITS processing.
#               An obsUnit is basically one observation in the Human 
#               Readable, but there is also some status info stored
#               in the object telling how the processing of each FITS mode
#               (realtime or production, typically) is going.
#               
#               
#
# RCS: $Id$
# ===========================================================================


package ObsUnit;

use ObjectMaker ( [qw( obsid met1 met2 utc1 utc2 week day2 status
		       locked )]);

# The "status" attribute is a (reference to a) hash table with the keys 
#  as mode names (such as rt or pr). The values in the hash are hash
#  tables with a key "logfile" and a key "dest_list".  The value for
#  the "dest_list" also holds a hash of status info for each destination.
#
#  See the pod documentation below for an example.


=pod

 This is how the status item looks:

 $self->{"status"} = {  "rt" -> { "logfile"   => "obsid.rt",
                                  "dest_list" => { "sof" => { "done" => "",
                                                              "date" => "",
                                                              "comment" => ""},
                                                   "gof" => { "done" => "",
                                                              "date" => "",
                                                              "comment" => ""}
                                                 }
                                },

                     {  "pr" -> { "logfile"   => "obsid.pr",
                                  "dest_list" => { "sof" => { "done" => "",
                                                              "date" => "",
                                                              "comment" => ""},
                                                   "gof" => { "done" => "",
                                                              "date" => "",
                                                              "comment" => ""}
                                                 }
                                };

=cut


use ProcessFinder;

use Sys::Hostname;
use Time::ParseDate;
use Carp;

use strict;

sub DONE {
  return 1;
}

sub CANCELLED {
  return 2;
}

sub NOTDONE {
  return 0;
}

sub TIMENOW {
  return scalar(localtime(time()));
}


sub initialize_status {
  my $self = shift;
  my( $mode_dest_hash ) = @@_;

  # Fill in status info for each mode.dest combo
  foreach my $m (keys %$mode_dest_hash) {
    foreach my $d (keys %{ $mode_dest_hash->{$m} }) {
      $self->add_or_set_status( $m, $d, 
			ObsUnit::NOTDONE, ObsUnit::TIMENOW, "initialized" );
    }
  }
}




sub print {
  my $self = shift;
  print "obsid     ",$self->{"obsid"},"\n";
  print "start met ",$self->{"met1"},"\n";
  print "stop  met ",$self->{"met2"},"\n";
  print "start utc ",$self->{"utc1"},"\n";
  print "stop  utc ",$self->{"utc2"},"\n";
  print "msn. week ",$self->{"week"},"\n";
  print "end   day ",$self->{"day2"},"\n";
  print "locked?   ",$self->lock_text(),"\n";
  print "status: \n";
  foreach my $m (keys %{ $self->{"status"} } ) {
    $self->print_mode($m);
  }
}


sub print_mode {
  my $self = shift;
  my( $mode ) = @@_;

  my $mode_hash = $self->{"status"}{$mode};
  my @@dest_list = sort keys %{ $mode_hash->{"dest_list"} };
  print "Mode name     : $mode\n";
  print "          log : ", $mode_hash->{"logfile"},"\n";
  print "    dest list : ", join(" ", @@dest_list),"\n";
  foreach my $d (@@dest_list) {
    print "   status for : $d\n";
    print "         done? ",$mode_hash->{"dest_list"}{$d}{"done"},"\n";
    print "         date: ",$mode_hash->{"dest_list"}{$d}{"date"},"\n";
    print "         comment: ",$mode_hash->{"dest_list"}{$d}{"comment"},"\n";
  }
}


sub copy {
  my $self = shift;
  my $class = ref( $self );

  my $obsUnit = $class->new ( "obsid"  => $self->{"obsid"},
			      "met1"   => $self->{"met1"},
			      "met2"   => $self->{"met2"},
			      "utc1"   => $self->{"utc1"},
			      "utc2"   => $self->{"utc2"},
			      "week"   => $self->{"week"},
			      "day2"   => $self->{"day2"},
			      "locked" => $self->{"locked"} );

  foreach my $m ($self->get_modes() ) {
    $obsUnit->{"status"}{$m}{"logfile"} =
       $self->{"status"}{$m}{"logfile"};
    foreach my $d ($self->get_dest_list($m)) {
      $obsUnit->{"status"}{$m}{"dest_list"}{$d}{"done"} = 
	 $self->{"status"}{$m}{"dest_list"}{$d}{"done"};
      $obsUnit->{"status"}{$m}{"dest_list"}{$d}{"date"} = 
	 $self->{"status"}{$m}{"dest_list"}{$d}{"date"};
      $obsUnit->{"status"}{$m}{"dest_list"}{$d}{"comment"} =
	 $self->{"status"}{$m}{"dest_list"}{$d}{"comment"};
    }
  }

  return $obsUnit;

}

sub is_same {
  my $self = shift;
  my( $other ) = @@_;

  my $same = 0;

  if ( ($self->{"obsid"}  eq $other->{"obsid"}) and
       ($self->{"met1"}   eq $other->{"met1"})  and
       ($self->{"met2"}   eq $other->{"met2"})  and
       ($self->{"utc1"}   eq $other->{"utc1"})  and
       ($self->{"utc2"}   eq $other->{"utc2"})  and
       ($self->{"week"}   eq $other->{"week"})  and
       ($self->{"day2"}   eq $other->{"day2"})  and
       ($self->{"locked"} eq $other->{"locked"}) )
  {
    $same = 1;
  }

  if($same) {
    if( join("", sort $self->get_modes() ) eq 
	join("", sort $other->get_modes()) )
    {
      foreach my $m ($self->get_modes()) {
	unless( $self->{"status"}{$m}{"logfile"} eq 
		$other->{"status"}{$m}{"logfile"} )
	{
	  $same = 0;
	  last;
	}
	unless(
	       join("", sort $self->get_dest_list($m) ) eq
	       join("", sort $other->get_dest_list($m) ) )
	{
	  $same = 0;
	  last;
	}
	foreach my $d ($self->get_dest_list($m)) {
	  my $dest_self  =  $self->{"status"}{$m}{"dest_list"}{$d};
	  my $dest_other = $other->{"status"}{$m}{"dest_list"}{$d};

	  unless( ($dest_self->{"done"}    eq $dest_other->{"done"}) and
		  ($dest_self->{"date"}    eq $dest_other->{"date"}) and
		  ($dest_self->{"comment"} eq $dest_other->{"comment"}) )
	  {
	    $same = 0;
	    last;
	  }
	}
      }
    } else {
      $same = 0;
    }
  }

  return $same;
}


sub agrees_with_hr_obs {
  my $self = shift;
  my( $hr_obs ) = @@_;

  if( ($self->{"met1"} == $hr_obs->start_met) and 
      ($self->{"met2"} == $hr_obs->stop_met) and 
      ($self->{"utc1"} eq $hr_obs->start_utc) and 
      ($self->{"utc2"} eq $hr_obs->stop_utc) and 
      ($self->{"week"} == $hr_obs->week) and 
      ($self->{"day2"} == $hr_obs->stop_day) )
  {
    return 1;
  } else {
    return 0;
  }

}

sub set_info_from_hr_obs {
  my $self = shift;
  my( $hr_obs ) = @@_;

  $self->{"met1"} = $hr_obs->start_met;
  $self->{"met2"} = $hr_obs->stop_met;
  $self->{"utc1"} = $hr_obs->start_utc;
  $self->{"utc2"} = $hr_obs->stop_utc;
  $self->{"week"} = $hr_obs->week;
  $self->{"day2"} = $hr_obs->stop_day;

}
  


sub add_or_set_status {
  my $self = shift;
  my( $mode, $dest, $done, $date, $comment ) = @@_;

  # This assumes that you only add destinations which 
  # are appropriate for this mode!

  if (!$self->{"status"}) { $self->{"status"} = {}; }

  unless( exists($self->{"status"}{$mode}{"logfile"}) ) { 
    my $log = "";
    if ( $self->{"obsid"} ) { $log = $self->{"obsid"}.".$mode.log"; }
    $self->{"status"}{$mode}{"logfile"} = $log;
  }

  $self->{"status"}{$mode}{"dest_list"}{$dest}{"done"}    = $done;
  $self->{"status"}{$mode}{"dest_list"}{$dest}{"date"}    = $date;
  $self->{"status"}{$mode}{"dest_list"}{$dest}{"comment"} = $comment;

}


sub has_mode_dest {
  my $self = shift;
  my( $mode, $dest ) = @@_;

  if ($self->{"status"} and exists($self->{"status"}{$mode}) and
      exists($self->{"status"}{$mode}{"dest_list"}{$dest}) )
  {
    return 1;
  } else {
    return 0;
  }
}

sub get_logfile_for_mode {
  my $self = shift;
  my( $mode ) = @@_;

  return $self->{"status"}{$mode}{"logfile"};
}


sub get_modes {
  my $self = shift;

  # Returns a list (NOT a list ref) of all the modes present in the status hash

  if ($self->{"status"}) {
    return (keys %{ $self->{"status"} });
  } else {
    return ();
  }

}


sub get_dest_list {
  my $self = shift;
  my( $mode ) = @@_;

  # Returns a list (NOT a list ref) of all the destinations present for
  # a particular mode in the status hash
  if ( $self->{"status"} and exists($self->{"status"}{$mode}) ) {
    return (keys %{ $self->{"status"}{$mode}{"dest_list"} });
  } else {
    return ();
  }

}

sub prepare_for_removal {
  my $self = shift;
  my( $logdir, $verb ) = @@_;

  # Remove all the log files for each mode.
  # This is done using the remove_mode method.
  # Since only the log file name is stored, the log file
  # directory is also needed.

  foreach my $m ($self->get_modes() ) {
    $self->remove_mode( $m, $logdir, $verb );
  }

}

sub remove_mode {
  my $self = shift;
  my( $mode, $logdir, $verb ) = @@_;

  # Also remove the logfile for this mode if it exists.
  if( $self->{"status"} and $self->{"status"}{$mode} ) {

    if( $self->{"status"}{$mode}{"logfile"} ) {
      my $full_file = $logdir."/".$self->{"status"}{$mode}{"logfile"};
      if ( -e $full_file ) {
	if( unlink($full_file) == 1 ) {
	  print "Deleted log file $full_file\n" if $verb;
	} else {
	  warn "Unable to delete log file $full_file: $!";
	}
      } else {
	warn "No log file found for mode $mode for obsUnit ".$self->obsid."\n"
	  if $verb;
      }

    } else {
      warn "No \"logfile\" key for \"$mode\" hash of status hash for obsUnit "
	.$self->obsid."\n" if $verb;
    }

    delete $self->{"status"}{$mode};
  } else {
    warn "No \"status\" hash \"$mode\" for obsUnit ".$self->obsid."\n"
      if $verb;
  }

}


sub remove_status {
  my $self = shift;
  my( $mode, $dest, $logdir, $verb ) = @@_;

  if ($self->{"status"} and exists($self->{"status"}{$mode}) and
      exists($self->{"status"}{$mode}{"dest_list"}{$dest}) )
  {
    # remove the destination from the list for this mode
    delete $self->{"status"}{$mode}{"dest_list"}{$dest};

    print "Removed $mode.$dest from status info of obsUnit ".$self->obsid."\n"
      if $verb;

    # If that was the last destination in the mode, also delete the mode!
    if ( scalar(keys %{ $self->{"status"}{$mode}{"dest_list"} }) == 0 ) {
      print "Last dest remove from $mode, so the mode entry will be deleted ".
	" from status info of obsUnit ".$self->obsid."\n" if $verb;
      $self->remove_mode($mode, $logdir, $verb);
    }

  } else {
    carp "Can't remove non existent mode->dest pair: $mode -> $dest";
  }
}


sub get_required_dest_list {
  my $self = shift;

  my( $m, $dest_list_ref, $config_info ) = @@_;

  my @@required_dest_list = ();

  foreach my $d (@@$dest_list_ref) {

    # For every dest., see if it is one that NEEDS to be done.

    my $regexp = $config_info->get_obs_regexp($m, $d);
    if ( $self->{"obsid"} =~ /$regexp/ ) {
      push( @@required_dest_list, $d );
    }

  }

  return @@required_dest_list;
}

sub is_cancelled {
  my $self = shift;
  my( $mode ) = @@_;
  # When an observation is cancelled, all its modes and dest are cancelled.
  # But here we just check to see if this entire mode is cancelled,
  # so it only matters that all the destinations in a particular mode get
  # cancelled.

  my $is_cancelled = 1;
  foreach my $d ($self->get_dest_list($mode) ) {
    unless($self->{"status"}{$mode}{"dest_list"}{$d}{"done"} == CANCELLED ) {
      $is_cancelled = 0;
      last;
    }
  } 
  return $is_cancelled;
}


sub is_done {
  my $self = shift;

  my( $m, $dest_list_ref, $config_info ) = @@_;

  my $super_mode = $config_info->get_skip_if_other_mode_complete($m);

  my $is_done = 0;

  if( $super_mode and 
      $self->is_done($super_mode, $dest_list_ref, $config_info) )
  {
    $is_done = 1;

  } else {

    $is_done = 1;
    foreach my $d ($self->get_required_dest_list( $m, $dest_list_ref, 
						  $config_info) )
    {
      # If any required dest is not done, the whole thing is not done
      unless( $self->{"status"}{$m}{"dest_list"}{$d}{"done"} ) {
	$is_done = 0;
	last;
      }
    }

  }
  return $is_done;
}


sub reset_all_status {
  my $self = shift;
  my( $done, $date, $comment ) = @@_;
  foreach my $m ($self->get_modes() ) {
    foreach my $d ($self->get_dest_list($m) ) {
      $self->add_or_set_status( $m, $d, $done, $date, $comment );
    }
  }
}

sub set_relevant_status_items {
  my $self = shift;
  my( $m, $dest_list_ref, $config_info, $done, $date, $comment ) = @@_;

  # Looks at a list of destinations and sets the status for each destination
  # which is required for this obsid.

  # For every required destination, set the status accordingly
  my $d;
  foreach $d ($self->get_required_dest_list($m,$dest_list_ref,$config_info)){
    $self->add_or_set_status($m, $d, $done, $date, $comment);
  }
}



sub most_recent_status_date {
  my $self = shift;

  # Dates are in this format:
  # Thu Mar 16 16:26:58 2000

  my $most_recent_date = undef;

  # First create the list of all dates.
  my @@date_list = ();
  foreach my $m (values %{ $self->{"status"} }) {
    foreach my $d (values %{ $self->{"status"}{$m}{"dest_list"} }) {
      my $date = $self->{"status"}{$m}{"dest_list"}{$d}{"date"};
      if($date) {
	push(@@date_list, $date);
      }
    }
  }

  # Now go through the list and find the most recent.
  foreach my $date (@@date_list) {
    if ($date) {
      if ( (!defined($most_recent_date)) or 
	   (parsedate($date) > parsedate($most_recent_date)) ) {
	$most_recent_date = $date;
      }
    }
  }

  # If there were no dates, then return now as the date.
  unless( defined($most_recent_date) ) {
    $most_recent_date = ObsUnit::TIMENOW;
  }

  return $most_recent_date;  # note: can return undef
}




sub lock {
  my $self = shift;
  $self->{"locked"} = hostname().":$$ ".TIMENOW();
}

sub unlock {
  my $self = shift;
  $self->{"locked"} = 0;
}

sub get_lock_info {
  my $self = shift;

  my @@info_list = ();  # will be machine name, process ID, and time stamp
                       # or empty list if not locked or bogus lock string

  if ($self->{"locked"}) {
    if ( $self->{"locked"} =~ /^(\S+):(\S+)\s+(.*)$/ ) {
      @@info_list = ( $1, $2, $3 );  # machine name, process id, time stamp
    } else {
      warn "Error interpreting lock info for obsid ", $self->{"obsid"},": \"",
           $self->{"locked"},"\"\n";
    }
  }
  return @@info_list;
}


sub get_lock_machine {
  my $self = shift;
  return ($self->get_lock_info)[0];
}

sub get_lock_process_id {
  my $self = shift;
  return ($self->get_lock_info)[1];
}

sub get_lock_time {
  my $self = shift;
  return ($self->get_lock_info)[2];
}

sub lock_text {
  my $self = shift;
  my $text = "";
  if( $self->is_locked() ) {
    $text = " by process ".$self->get_lock_process_id." on host ".
       $self->get_lock_machine." at date ".
       $self->get_lock_time;
  } else {
    $text = "no";
  }
}


sub is_locked {
  my $self = shift;
  my( $verb ) = @@_;

  # See if this entry is locked.  Not only must there be something
  # in the lock text, but the process holding the lock must still be active.

  if( $self->{"locked"} and 
      ProcessFinder::is_process_active($self->get_lock_machine,
				       $self->get_lock_process_id,
				       "makefits",
				       scalar(parsedate($self->get_lock_time)),
				         600, $verb) )
                                       # ^^^------<---------<----|
                                       # If you change the time here, also
                                       # change it in the next routine below.
  {
    return 1;
  } else {
    # The locked flag is off, so it can't be locked.
    return 0;
  }

}


sub is_lock_expired {
  my $self = shift;
  my( $verb ) = @@_;

  if( $self->{"locked"} and 
      !ProcessFinder::is_process_active($self->get_lock_machine,
					$self->get_lock_process_id,
					"makefits",
				       scalar(parsedate($self->get_lock_time)),
					600, $verb) )
  {
    # If we get in here, then yes - this lock has expired. That means
    # there is a lock recorded in the DB, but the process which locked
    # it seems to have died.

    return $self->{"locked"};  # The info in the expired lock is returned

  } else {
    return 0;
  }
}

1;



@

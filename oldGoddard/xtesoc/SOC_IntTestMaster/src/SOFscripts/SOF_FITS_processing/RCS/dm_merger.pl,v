head	1.1;
access;
symbols;
locks; strict;
comment	@# @;


1.1
date	2000.09.12.18.13.03;	author jonv;	state Exp;
branches;
next	;


desc
@replacement for ppeds to be used in the SOF's creation
of production FITS files (the ones which get made 1 day after
the data comes down)
@


1.1
log
@Initial revision
@
text
@#!/usr/local/bin/perl -w
# ===========================================================================
# Filename    : dm_merger.pl
# Subsystem   : SOFscripts
# Programmer  : Jon Vandegriff, RITSS
# Description : merge EAs and run dmIndex
#               This is kind of a replacement for ppeds
#               But it is only to be used on the realtime FITS generation
#               pipeline - the one that generates FITS data within one
#               day of the data arriving.
#
# RCS: $Id$
# ===========================================================================


use strict;

{

  unless( is_environment_OK() ) {
    die "Invalid Environment - exiting.\n";
  }

  my( $report, $act, $verb ) = parse_arguments( @@ARGV );

  if( $verb) {
    print "\n$0 running in \"verbose\" ";
    if( !$act ) { print "and \"no-action\" "; }
    print "mode\n\n";
  }

  my $daylist_for_apid = study_datasets_present( $verb );

  my $tbp_entries = get_packet_file_TBP_list();

  my( $merge_list, $dmIndex_only_list, $remove_list ) =
     study_TBP_entries( $tbp_entries, $daylist_for_apid );

# 777
#print "To remove:\n";foreach (@@$remove_list) { $_->print(); }
#print "To merge:\n";foreach (@@$merge_list) { $_->print(); }
#print "To dm_index:\n";foreach (@@$dmIndex_only_list) { $_->print(); }

 remove_TBP_entries( $remove_list, $act, $verb );

 do_EA_merging( $merge_list, $act, $verb );

 run_dmIndex( $report, $act, $verb );

 cleanse_datasets( $act, $verb );

}

sub is_environment_OK {
  if( $ENV{"SOCOPS"} ) {
    return 1;
  } else {
    return 0;
  }
}


sub parse_arguments {
  my $report = 0;
  my $act  = 1;
  my $verb = 0;

  my $arg;
  while( defined($arg = shift) ) {
    if ($arg eq "-report") {
      $report = 1;
    } elsif ($arg eq "-n") {
      $act = 0;
    } elsif ($arg eq "-v") {
      $verb = 1;
    } else {
      warn "Invalid argument: \"$arg\"\n";
      usage();
      exit();
    }
  }
  return( $report, $act, $verb );
}

sub usage {
  warn "\nUsage:\n$0 [-n] [-v]  [-report]\n";
}

sub study_datasets_present {
  my( $verb ) = @@_;

  my $daylist_for_apid = {};

  # For every day present in $SOCOPS/timeOrdered/production
  # see which apids already have datasets present.

  my $socops = $ENV{"SOCOPS"};

  my $dataset_top_dir = "$socops/timeOrdered/production";
  my $match_string    = "^day\\d+\$";

  my $dataset_day_dirs =  
    get_matching_files_from_dir($dataset_top_dir, $match_string);

# 777
#  print "Dirs:\n", join("\n", @@$dataset_day_dirs),"\n";
  my @@sorted_dirs;
  { my( $x, $y );
    @@sorted_dirs = sort { $a =~ /day(\d+)/; $x = $1;
			  $b =~ /day(\d+)/; $y = $1;
			  $x <=> $y;                  }  @@$dataset_day_dirs;
  }


  foreach my $day_dir (@@sorted_dirs) {
    my $dir = "$dataset_top_dir/$day_dir";
    unless( $day_dir =~ /day(\d+)/ ) {
      die "Invalid dataset day directory name \"$day_dir\" - ".
	  "expecting something like dayNNNN";
    }

    my $day = $1;

    print "Looking for existing datasets in day $day\n" if $verb;

    my $apid_dir_list = get_matching_files_from_dir( $dir, "^apid\\d+\$" );

    my @@sorted_apid_dirs;
    { my( $x, $y );
      @@sorted_apid_dirs = sort { $a =~ /apid(\d+)/; $x = $1;
				 $b =~ /apid(\d+)/; $y = $1;
				 $x <=> $y;                  } @@$apid_dir_list;
    }

# 777
#print "For day $day, found apid dirs: ",join(" ",@@$apid_dir_list),"\n";

    foreach my $adir (@@sorted_apid_dirs) {
      my $apid_dir = "$dir/$adir";
      my( $apid ) = $adir =~ /apid(\d+)/;
      $apid+=0; # convert to regular number

      if( is_apid_dir_complete($apid_dir) ) {
	if( !exists($daylist_for_apid->{$apid}) ) {
	  $daylist_for_apid->{$apid} = [];
	}
	push( @@{ $daylist_for_apid->{$apid} }, $day );
      }
    }
  }

  # 777 don't need this anymore since days and apids are now sorted as we go
  # sort the day list for each apid
  # foreach my $apid (keys %$daylist_for_apid) {
  #  $daylist_for_apid->{$apid} = 
  #    [ sort {$a<=>$b} @@{$daylist_for_apid->{$apid}} ];
  # }


  return $daylist_for_apid;
}

sub get_matching_files_from_dir {
  my( $dir, $match_string ) = @@_;

  opendir( D, $dir ) 
    or die "Can't access directory $dir: $!";

  my $list_ref = [];
  my $eval_string = "\@@{\$list_ref} = grep { /\$match_string/o } readdir(D); ";
  eval $eval_string;

  if( $@@ ) {
    die "Error while searching for files matching $match_string ".
      "in directory $dir\n";
  };

  closedir(D);

  return $list_ref;
}


sub is_apid_dir_complete {
  my( $apid_dir ) = @@_;

  # To see if an apid's dataset directory is complete, for now, just
  # make sure there is at least one actual dataset file in it.

  my $ds_files = get_matching_files_from_dir( $apid_dir,"^I[0-9a-f]{2}-" );

  if( scalar(@@$ds_files) > 0 ) {
    return 1;
  } else {
    return 0;
  }

}




sub get_packet_file_TBP_list {
  my $tbp_entries = [];

  # Get all packet file TBP entries for the current $SOCOPS

  my $socops = $ENV{"SOCOPS"};
  my $dir = "$socops/ingest/.to-be-processed";
  my $match_string = "$socops/ingest/production/day\\d+/apid\\d+";
  $match_string =~ s/\//\!/g;

  my $tbp_files = get_matching_files_from_dir( $dir, $match_string );

  my $sorted_tbp_files = [];
  { my( $d1,$d2,  $a1,$a2, $t11,$t12, $t21,$t22 );
    #    day       apid    start time  stop time
    @@{$sorted_tbp_files} = 
      sort { ($d1,$a1,$t11,$t12) = get_tbp_parts($a);
	     ($d2,$a2,$t21,$t22) = get_tbp_parts($b);
	     if($d1<$d2) {
	       -1;
	     } elsif( $d1>$d2) {
	       1;
	     } elsif( $a1 < $a2 ) {
	       -1;
	     } elsif( $a1 > $a2 ) {
	       1;
	     } elsif( $t11 < $t21 ) {
	       -1;
	     } elsif( $t11 > $t21 ) {
	       1;
	     } elsif( $t12 < $t22 ) {
	       -1;
	     } elsif( $t12 > $t22 ) {
	       1;
	     } else {
	       0;
	     }
	   } @@$tbp_files;
  }

  return $sorted_tbp_files;

}


sub get_tbp_parts {
  my( $tbp_entry ) = @@_;
  my( $day, $apid, $filename ) = parse_tbp_entry( $tbp_entry );

  unless( $filename =~ /(\d+)-(\d+)$/ ) {
    die "Invalid file in TBP entry list: $filename";
  }
  my( $t1, $t2 ) = ($1,$2);
  return( $day, $apid, $t1, $t2 );
}


sub study_TBP_entries {
  my ( $tbp_entries, $daylist_for_apid ) = @@_;

  my $merge_list = [];
  my $dmIndex_only_list = [];
  my $remove_list = [];

  # First see if there is more than one entry for a given day and apid
  # If there are multiple entrues, the most recent entry is the one 
  # which will be used - this is not foolproof - the most recent one
  # might be the wrong one, but usually we only get one entry, and so
  # in the interest of making this processing go fast, no human
  # intervention will be requested.

  my %entries_present = ();
  foreach my $entry (@@$tbp_entries) {
    my( $day, $apid, $filename ) = parse_tbp_entry( $entry );
    my $key = "$day-$apid";
    my $entry_info = new TBPEntry( "filename" => $filename,
				   "day" => $day,
				   "apid" => $apid,
				   "TBPname" => $entry );
    if(!exists($entries_present{$key}) ) {
      $entries_present{$key} = [];
    }
    push( @@{ $entries_present{$key} }, $entry_info  );
  }

  # Now go through all the entries found for each day-apid combo,
  # and pick just a single one to be used.
  my %to_use = ();
  my( $key, $entry_list );
  while( ($key, $entry_list) = each %entries_present ) {
    if( scalar(@@$entry_list) == 1 ) {
      $to_use{$key} = $entry_list->[0];
    } else {
      # The TBP entry to use is the most recent one.
      my $mtime = undef;
      foreach my $entry (@@$entry_list) {
	my $this_mtime = -M $entry->filename;
	if( !defined($mtime) or ( $this_mtime < $mtime ) ) {
	  $mtime = $this_mtime;
	  if( exists($to_use{$key}) ) {
	    # If there already was one selected, and it gets replaced, then
	    # add it (the previously selected one) to the remove list.
	    push( @@$remove_list, $to_use{$key} );
	  }
	  $to_use{$key} = $entry;
	} else {
	  # This is not the one to use - so remove it
	  push( @@$remove_list, $entry );
	}
      }
    }
  }

  # OK, now go through the unique list of files for each day and apid,
  # and categorize them
  my $entry;

  while( ($key, $entry) = each %to_use ) {

    my $most_recent_day = undef;
    my $oldest_day = undef;
    if( exists($daylist_for_apid->{$entry->apid}) ) {
      # -1 gets last array element, which is the highest day number in the list
      $most_recent_day = $daylist_for_apid->{$entry->apid}[-1];
      $oldest_day      = $daylist_for_apid->{$entry->apid}[0];
    }

# 777
#print "Entry day = ", $entry->day,"  apid = ", $entry->apid,"\n";

    if( !defined($most_recent_day) or ($entry->day > $most_recent_day) ) {
      if ( is_EA_apid($entry->apid) ) {
	push( @@$merge_list, $entry );
      } else {
	push( @@$dmIndex_only_list, $entry );
      }
    } elsif( $entry->day < $oldest_day ) {

      # This one is too old - don't process it here; If someone wants
      # it processed, they can process it by hand.
      push( @@$remove_list, $entry );
    } else {
      # see if this day is needed for this apid
      # if it is needed, then scheudle it for dmIndex only
      # if there is already data for this apid, then remove this entry
      my %days_for_apid_hash = ();
      foreach (@@{ $daylist_for_apid->{$entry->apid} }) {
	$days_for_apid_hash{$_} = 1;
      }
      if( exists($days_for_apid_hash{$entry->day}) ) {
	push( @@$remove_list, $entry );  # this one is already done
      } else {
	# this one is needed, but no merging will be attempted,
	# since there is already data past this day.
	push( @@$dmIndex_only_list, $entry );
      }
    } # end of check on where entry falls with respect to the day list

  } # end of loop over each entry

  # The merge list does not have to be sorted by day number because the
  # merging of EARuns is done in day order.

  return( $merge_list, $dmIndex_only_list, $remove_list );

}

sub parse_tbp_entry {
  my( $entry ) = @@_;

  my( $day, $apid, $filename ) = (undef, undef, undef);
  if( $entry =~ /day(\d+)!apid(\d+)!\d+-\d+-\d+$/ ) {
    $day = $1;
    $apid = $2 + 0; # convert to number (i.e., remove leading 0 if present)
    $filename = $entry;
    $filename =~ s/!/\//g;
    $filename =~ s/-\d+$//;
  }
  return( $day, $apid, $filename );
}




sub is_EA_apid {
  my( $apid ) = @@_;

  if( ($apid >= 48) and ($apid <= 79) ) {
    return 1;
  } else {
    return 0;
  }
}


sub remove_TBP_entries {
  my( $remove_list, $act, $verb ) = @@_;

  my $TBP_dir = $ENV{"SOCOPS"}."/ingest/.to-be-processed";

  foreach my $entry (@@$remove_list) {
    my $full_tbp_entry_file = "$TBP_dir/".$entry->TBPname;
    if ($act) {
      unless( unlink( $full_tbp_entry_file ) == 1 ) {
	warn "Warning - cannot remove TBP entry $full_tbp_entry_file: $!";
      }
    } else {
      print "Would try to remove $full_tbp_entry_file\n" if $verb;
    }
  }
}


sub do_EA_merging {
  my( $merge_list, $act, $verb ) = @@_;

  # First run dmPPeds on the packet files.
  # Then merge all EA runs within the days processed.
  # Then merge any EA runs from one day which began in the previous day.

  # Be smart about it so that only the days which need to be merged 
  # are looked at.

  my %day_list = ();
  foreach my $entry (@@$merge_list) {

    # run dmPPeds and also remember the day which was affected
    unless( exists($day_list{$entry->day}) ) { $day_list{$entry->day} = []; }
    push( @@{ $day_list{$entry->day} }, $entry->apid );

# 777 - extra verbose -d option to dmPPeds
    my $cmd = "dmPPeds -d -forceremove -files ".$entry->filename;
    if($act) {

      print "Running \"$cmd\"\n" if $verb;

      # I want to capture the output and get the exit status so I use a pipe.
      # The close() on a pipe puts the exits status into Perl's $?

      if( open( PIPE, "$cmd 2>&1 |") ) {
	my @@output = <PIPE>;
	close(PIPE);
	my $exit_status = $?;
	print @@output if $verb;
	if( $exit_status ) {
	  print "ERROR - dmPPeds returned an error\n";
	}
      } else {
	warn "Cannot open command $cmd: $!";
      }
    } else {
      print "Would execute: \"$cmd\"\n" if $verb;
    }
  }

  my $top_dir = $ENV{"SOCOPS"}."/ingest/production";
  my $TBP_dir = $ENV{"SOCOPS"}."/ingest/.to-be-processed";

  # Now do the merging of all EAruns within the days given
  foreach my $day (sort {$a <=> $b} keys %day_list) { # day loop

    my $day_dir = sprintf "%s/day%04d", $top_dir, $day;

    ###################
    # The EARuns dir name must match that used by ppeds !!!!
    ###################
    my $today_earun_dir = "$day_dir/EARuns";

    unless( -d $today_earun_dir ) {
      if( $act ) {
	system("mkdir -p $today_earun_dir");
	print "Made directory $today_earun_dir\n" if $verb;
      } else {
	print "Would make directory $today_earun_dir\n" if $verb;
      }
    }

    my $yesterday_earun_dir = sprintf "%s/day%04d/EARuns", $top_dir, $day -1;

    my %files_placed_in_common_dir = ();
    foreach my $apid (@@{ $day_list{$day} }) {  # apid loop

      my $apid_dir = sprintf "%s/apid%03d", $day_dir, $apid;
      my $earun_files = get_matching_files_from_dir( $apid_dir, "^R.*\.EA.\$");

      foreach my $earun_file (@@$earun_files) { # earun loop

	my $this_earun_file = "$apid_dir/$earun_file";
	my $merged_earun_file = "$today_earun_dir/$earun_file";

	if( exists($files_placed_in_common_dir{$earun_file}) ) {

	  # The first half is already there, so we need to do a merge
	  if( $act ) {
	    my $tmp_file = "/tmp/EAtmp.$$";
	    system("mergeEAs $this_earun_file $merged_earun_file > $tmp_file;".
	           "mv $tmp_file $merged_earun_file"); 
  	         # overwrites unmerged version

	    unless( unlink($this_earun_file) == 1 ) {
	      warn "Can't remove unmerged earun file $this_earun_file: $!";
	    }

	    print "Merged $this_earun_file with $merged_earun_file\n" if $verb;

	  } else {
	    print "Would merge $this_earun_file with $merged_earun_file\n"
	      if $verb;
	  }
	} else {
	  # This file is the first to be moved to the EARuns dir
	  if( $act ) {
	    system("mv $this_earun_file $merged_earun_file; ".
		   "dmNotify $merged_earun_file");
	    print "Moved $this_earun_file to $merged_earun_file - ".
	          "notified data management\n" if $verb;
	  } else {
	    print "Would move $this_earun_file to $merged_earun_file - ".
	          "notify data management\n" if $verb;
	  }

	  # record that its been moved.
	  $files_placed_in_common_dir{$earun_file} = 1;

	} # end of check for first version already moved

      } # end of loop over each EARun file

    } # end of loop over each apid

    # Now check all the files in this day's EARuns dir to see if they
    # have another part in the previous day's EARun dir
    if( -d $today_earun_dir ) {
      my $todays_earuns =
	  get_matching_files_from_dir( $today_earun_dir, "^R.*\.EA.\$");
      foreach my $todays_earun_file (@@$todays_earuns) {
	my $todays_version = "$today_earun_dir/$todays_earun_file";
	my $yesterdays_version = "$yesterday_earun_dir/$todays_earun_file";
	if( -e $yesterdays_version ) {
	  if( $act ) {
	    my $tmp_file = "/tmp/EAtmp.$$";
	    system("mergeEAs $yesterdays_version $todays_version > $tmp_file");
	    system("mv $tmp_file $yesterdays_version");
	    system("rm -f $todays_version $TBP_dir/*$todays_earun_file*");
	    system("dmNotify $yesterdays_version");
	    print "Merged EARun $todays_version with previous day's file ".
	          "$yesterdays_version\n" if $verb;
	  } else {
	    print "Would merge EARun $todays_version with previous day's ".
	          "file $yesterdays_version\n" if $verb;
	  }
	}
      } # end of loop over todays merged EARun files
    }
  } # end of loop over each day

}



sub run_dmIndex {
  my( $report, $act, $verb ) = @@_;
# 777 take out "-rl 20" ?
#  my $cmd = "dmIndex -rl 20 -production -once";
  my $cmd = "dmIndex -rl 20 -production -once";
  if( $report ) { $cmd .= " -report "; }
  if( $act ) {
    print "Running \"$cmd\"\n" if $verb;
    my $exit_status = system("$cmd");
    if( $exit_status ) {
      warn "Warning - dmIndex had a problem.\n";
    }
  } else {
    print "Would run \"$cmd\"\n" if $verb;
  }
}




sub cleanse_datasets {
  my( $act, $verb ) = @@_;

  # Find all datasets (within a given apid directory) 
  # which have the same start time.
  # Remove the smaller of the two, since the smaller one is incomplete.

  print "Starting a cleansing run through all datasets.\n" if $verb;

  my $dataset_top_dir = $ENV{"SOCOPS"}."/timeOrdered/production";
  my $match_string    = "^day\\d+\$";

  my $dataset_day_dirs =  
    get_matching_files_from_dir($dataset_top_dir, $match_string);

  my @@sorted_day_dirs;
  { my( $x, $y );
    @@sorted_day_dirs = sort { ($x) = $a =~ /day(\d+)/;
			      ($y) = $b =~ /day(\d+)/;
			      $x <=> $y;
			    } @@$dataset_day_dirs;
  }

  foreach my $day_dir (@@sorted_day_dirs) { # loop over days
    print "Doing dataset cleansing in directory $day_dir.\n" if $verb;

    my $dir = "$dataset_top_dir/$day_dir";
    unless( $day_dir =~ /day(\d+)/ ) {
      die "Invalid dataset day directory name \"$day_dir\" - ".
	  "expecting something like dayNNNN";
    }
    my $day = $1;
    my $apid_dir_list = get_matching_files_from_dir( $dir, "^apid\\d+\$" );

    foreach my $adir (@@$apid_dir_list) { # loop over apid dirs
      my $apid_dir = "$dir/$adir";
      my( $apid ) = $adir =~ /apid(\d+)/;

      my $ds_files = get_matching_files_from_dir( $apid_dir,"^I[0-9a-f]{2}-" );
      my $N = scalar( @@$ds_files );
      my $i = 0;
      my @@sorted_ds_files = sort { hex( (split(/-/,$a))[1] ) <=> 
				   hex( (split(/-/,$b))[1] ) } @@$ds_files;
         # Sorted by start time.

      foreach my $ds ( @@sorted_ds_files ) { # loop over ds files
	unless( ($i+1) < $N ) { last; }
	++$i;  # just a counter to be able to tell when we are at
	       # the last ds file in the list - we don't need to process it
	       # since there won't be anything past it to compare to.
	my $this_ds = "$apid_dir/$ds";
	my $next_ds = $sorted_ds_files[$i];

	my $start_time = (split(/-/,$ds))[1];
	my $next_ds_start_time = ( split(/-/,$next_ds) )[1];

	if( $start_time eq $next_ds_start_time ) {
	  $next_ds = "$apid_dir/$next_ds";
	  # See which one is smaller
	  my $to_remove;
	  if( (stat($this_ds))[7] < (stat($next_ds))[7] ) {
	    $to_remove = $this_ds;
	  } else {
	    $to_remove = $next_ds;
	  }
	  if( $act ) {
	    if( unlink($to_remove) == 1 ) {
	      print "Removed partial dataset file $to_remove\n" if $verb;
	    } else {
	      warn "Warning - could not remove redundant (smaller) ".
		    "dataset file $to_remove: $!";
	    }
	  } else {
	    print "Would remove partial dataset file $to_remove\n" if $verb;
	  }
	} # end of if-block for two files with same size

      } # end of loop over ds files

    } # end loop over apid dirs

  } # end of loop over days

}


package TBPEntry;
use ObjectMaker( [qw(filename day apid TBPname)] );

sub print {
  my $self = shift;
  print "filename: ", $self->filename,"  apid: ", $self->apid,
        "  day: ", $self->day, "  TBPname: ", $self->TBPname,"\n";
}
@

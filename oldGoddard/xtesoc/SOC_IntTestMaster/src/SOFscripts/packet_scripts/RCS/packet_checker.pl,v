head	1.2;
access;
symbols;
locks; strict;
comment	@# @;


1.2
date	99.05.04.15.37.59;	author jonv;	state Exp;
branches;
next	1.1;

1.1
date	99.03.22.19.48.10;	author jonv;	state Exp;
branches;
next	;


desc
@a file for analyzing the validity of packets developed for the
spacecraft data recorder problem which occurred on 3/14/1999 to
3/18/1999
@


1.2
log
@added a -showhex <bytes> option which Arnold found useful
during some examining of EARun files during the single bit error
recovery efforts in April 1999
@
text
@#!/usr/local/bin/perl -w

# ===========================================================================
# Filename    : packet_checker.pl
# Subsystem   : SOF Scripts
# Programmer  : Jon Vandegriff, Raytheon STX
# Description : This script is for lookign at packet files and removing bad packets.
#               Right now it is only capable of removing bad ACS packets based on the 
#               normalization of the quaternions.
# RCS: $Id: packet_checker.pl,v 1.1 1999/03/22 19:48:10 jonv Exp jonv $
# ===========================================================================




use FileHandle;

use strict;


my $SUBSEC_CONVERSION = 2**32 - 1;


{
my $infile = undef;
my $outfile = undef;
my $required_apid = undef;
my $show_hex = 0;
my $num_bytes = undef;

my $MAX_QUATERNION_ERROR = 1.0e-7;

# PARSE THE ARGUMENTS
#=========================================================================
my $arg;
while(defined($arg = shift @@ARGV)) {
  if ($arg eq "-infile") {
    $infile = shift @@ARGV;
  } elsif ($arg eq "-outfile") {
    $outfile = shift @@ARGV;
  } elsif ($arg eq "-showhex") {
    $show_hex = 1;
    $num_bytes = shift @@ARGV;
  } elsif ($arg eq "-apid") {
    $required_apid = shift @@ARGV;
  } elsif ($arg eq "-help") {
    usage();
    exit(0);
  } else {
    print STDERR "Invalid argument: $arg\n";
    usage();
    exit(0);
  }
}

sub usage {
  print STDERR "\npacket_checker.pl [-infile <packet_file>] ".
               "[-outfile <new_packet_file>]\n   [-apid <apid_number>]".
               "  [-showhex <num_bytes>]\n";
  print STDERR "\nIf no infile is specified the script reads from STDIN\n";
  print STDERR "If no outfile is specified the script writes to STDOUT\n";
  print STDERR "(you generally don't want packets written to your screen!!)\n";
  print STDERR "If you eventually want the file to be processed by dmIndex,\n".
               "it is up to you to give it the appropriate name (note that\n".
               "if leading or trailing packets were removed, the name of\n".
               "the new outfile might need to be different than the name of\n".
               "the infile.\n";
  print STDERR "\nThe -showhex <bytes> option lets you specify just an\n".
               "infile, and it writes out to the screen in hex the given\n".
               "number of bytes of the actual packet.   The hex values for\n".
               "the (fixed length) header are always written).  The header\n".
               "values appear to the left of the colon, and the hex values\n".
               "for the packet contents appear to the right of the colon.\n";
}


# OPEN REQUIRED FILES
#=========================================================================
my $fh_in;
my $fh_out;

if ($infile) {
  $fh_in = new FileHandle "$infile";
} else {
  $fh_in = new FileHandle "<&STDIN";
}

if (!$show_hex) {
  if ($outfile) {
    $fh_out = new FileHandle ">$outfile";
  } else {
    $fh_out = new FileHandle ">&STDOUT";
  }
}

# READ PACKETS FROM $fh_in AND ANALYZE THEM
#=========================================================================
my( $hdr, $pkt, $apid, @@q );

my( $amt_read, $size );


my %met_hash = (); # for keeping track of duplicate packets


while( ($amt_read = read $fh_in, $hdr, 6) == 6 ) {

  $size = get_pkt_size( $hdr );
  $amt_read = read $fh_in, $pkt, $size;

  if ($required_apid) {
    $apid = get_pkt_apid( $hdr );
    unless ($apid == $required_apid) {
      next; # this is not the apid we are looking for - skip to the next pkt
            # note that this must come after the full read of the packet
    }
  }

  if ($show_hex) {
    print get_hex_chars( $hdr, $pkt, $num_bytes, $size), "\n";
  } else {

    my $met = get_pkt_timestamp( $pkt );
    if (exists($met_hash{$met})) {
      print STDERR "Duplicate packet at MET=$met\n";
      next;
    } else {
      $met_hash{$met} = 1;
    }

    if ($apid == 14) {
      @@q = get_pkt_quaternions( $pkt );

      # quaternions are available here as
      #  qx    qy    qz    qs
      # $q[0],$q[1],$q[2],$q[3]

      my $qval = $q[0]**2 + $q[1]**2 + $q[2]**2 + $q[3]**2;
      unless ( abs($qval -1) < $MAX_QUATERNION_ERROR) {
        print STDERR " BAD Quaternions at MET=$met: Q**2 - 1 = ",$qval-1,"\n";
        next;
      }
    }

    if ( 1 ) {  # put whatever conditions you want here
      # write the packet back out
      print $fh_out $hdr;
      print $fh_out $pkt;
    }
  }

}

$fh_in->close();
if (!$show_hex) {
  $fh_out->close();
}



}


# SUBROUTINES
#=========================================================================


sub get_pkt_size {
  my $hdr = shift;

  my $pkt_len = unpack("S",substr($hdr,4,2) );
  $pkt_len += 1;

#  print "pkt_len is $pkt_len\n";
  return $pkt_len;
}

sub get_pkt_apid {
  my $pkt = shift;

  my( $junk, $apid_bits ) = unpack("B4B11",$pkt);
  my $num_as_bits = pack "B5B11","00000",$apid_bits;

  my $pkt_apid = unpack("S",$num_as_bits);

#  print "pkt_apid is $pkt_apid\n";

  return $pkt_apid;
}

sub get_pkt_timestamp {
  my $pkt = shift;
  my $MET_sec = unpack("L", substr($pkt, 0, 4) );
  my $MET_subsec = unpack("L", substr($pkt, 4, 4) );

#  print "MET = ", $MET_sec,"\n";
#  print "subsec = ", $MET_subsec,"\n";
#  print "MET = ",$MET_sec + $MET_subsec / $SUBSEC_CONVERSION,"\n";
  return $MET_sec + $MET_subsec / $SUBSEC_CONVERSION;
}


sub get_pkt_quaternions {
  my $pkt = shift;

  my @@qt = ();
  my $i;
  for( $i=0; $i<4; $i++) {
    $qt[$i] = make_single_precision( substr($pkt, $i*4+8, 4) );
  }

#  print "quaternions: \n  ", join("\n  ", @@qt),"\n";
  return @@qt;
}

sub make_single_precision {
  my $buf = shift;

  # take the bytes in the buffer and re-arrange them
  # as follows:
  # byte 2->0
  # byte 3->1
  # byte 0->2
  # byte 1->3

  my $new_buf = 
      substr($buf,2,1).substr($buf,3,1).substr($buf,0,1).substr($buf,1,1);

  # then unpack as a single precision
  my $single_precision_number = unpack("f", $new_buf);
  return $single_precision_number;
}


sub get_hex_chars {
  my ( $hdr, $pkt, $n, $len ) = @@_;

  # $n bytes past the header are shown
  # The header is to the left of the colon, the packet data is to the right
  # of the colon.

  my $h = unpack("H12",$hdr);

  if ($n > $len) { $n = $len; }
  my $num_hex_digits = $n*2;
  my $fmt_str = "H".$num_hex_digits*2;

  my $p = unpack($fmt_str,substr($pkt, 0, $n));
  return "$h:$p";

}

@


1.1
log
@Initial revision
@
text
@d10 1
a10 1
# RCS: $Id$
d28 2
d41 3
d57 3
a59 2
  print STDERR "packet_checker.pl -infile <packet_file> ".
               "-outfile <new_packet_file>  [-apid <apid_number>]\n";
d63 11
a73 2
  print STDERR "You'll have to make sure the name of the outfile is ".
                "appropriate for processing by dmIndex\n";
d88 6
a93 4
if ($outfile) {
  $fh_out = new FileHandle ">$outfile";
} else {
  $fh_out = new FileHandle ">&STDOUT";
d119 2
a120 4
  my $met = get_pkt_timestamp( $pkt );
  if (exists($met_hash{$met})) {
    print STDERR "Duplicate packet at MET=$met\n";
    next;
a121 2
    $met_hash{$met} = 1;
  }
d123 7
d131 2
a132 1
  @@q = get_pkt_quaternions( $pkt );
d134 10
a143 9
  # quaternions are available here as
  #  qx    qy    qz    qs
  # $q[0],$q[1],$q[2],$q[3]

  my $qval = $q[0]**2 + $q[1]**2 + $q[2]**2 + $q[3]**2;
  unless ( abs($qval -1) < $MAX_QUATERNION_ERROR) {
    print STDERR " BAD Quaternions at MET=$met: Q**2 - 1 = ",$qval-1,"\n";
    next;
  }
d145 5
a149 4
  if ( 1 ) {  # put whatever conditions you want here
    # write the packet back out
    print $fh_out $hdr;
    print $fh_out $pkt;
d155 3
a157 1
$fh_out->close();
d232 19
@

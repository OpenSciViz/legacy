head	1.4;
access;
symbols;
locks; strict;
comment	@# @;


1.4
date	99.02.19.22.19.12;	author socops;	state Exp;
branches;
next	1.3;

1.3
date	99.01.28.19.25.26;	author socops;	state Exp;
branches;
next	1.2;

1.2
date	99.01.28.19.21.28;	author socops;	state Exp;
branches;
next	1.1;

1.1
date	98.12.04.19.02.47;	author socops;	state Exp;
branches;
next	;


desc
@An object oriented canvas plotting package for perl/tk.
@


1.4
log
@making version in use the top RCS version
(changes were for use in progress monitor and asm client)
@
text
@
# ===========================================================================
# Filename    : CanvasPlotObj.pm 
# Subsystem   : ASM, perl/tk scripts 
# Programmer  : Brian Thomas
# Description : This is a module for a canvas "object" designed to graph/plot 
# RCS: $Id: CanvasPlotObj.pm,v 1.1 1998/12/04 19:02:47 socops Exp socops $
# ===========================================================================
 
package CanvasPlotObj;
 
use ObjectTemplate;
@@ISA = qw(ObjectTemplate);
 
# This is an API for a canvas "object" designed to graph/plot 
# data. It plots an array of vectors--> this array has the format
# of a list of hash tables (eg $plot_vector[0] refers to a hash table
# and you can dereference $plot_vector[3]->{'KEY'}). Each hash table 
# must have 4 keys (or 'plot vectors'):
# 'X' (x position), 'Y' (y position), 'XERR' (x error size), and
# 'YERR' (y error size). (Units are arbitrary of course).  
#
# To use this API in a perl/tk program, first create a canvas, then plot
# object. For example:
#
# my $canvas = $plotwin->Canvas(height => $plotheight, 
#                    width => $plotwidth, bg => 'white')->pack;
#
# my $plot_object = CanvasPlotObj->new(
#      "plotdev" => $canvas,
#      "height" => $plotheight-$bordersize,
#      "width" => $plotwidth-$bordersize,
#      "bordersize" => $bordersize,
#      "linewidth" => $plotlinewidth,
#      "nrof_xtick" => 3,
#      "nrof_ytick" => 3,
#      "minor_xticks_per_int" => 5,
#      "minor_yticks_per_int" => 5,
#      "Xmin" => $minx,
#      "Xmax" => $maxx,
#      "Ymin" => $miny,
#      "Ymax" => $maxy,
#      "title" => $v[0]->{'title'},
#      "Xlabel" => $v[0]->{'xlabel'},
#      "Ylabel" => $v[0]->{'ylabel'},
#      "labelColor" => 'firebrick',
#      "lineColor" => 'black',
#      "plotColor" => 'grey',
#      "tickColor" => 'black',
#      "borderColor" => 'black',
#      "offset" => 50,
#      "vectors" => [{}]
#  );
#
#  # Initialize the 2D array of vectors we will plot:
#  # Note that each $entry is a ref to a hash table 
#  foreach my $entry (@@v) { $plot_object->add_vector($entry); }
#
#  # Now, lets plot it:
#  $plot_object->make_datacrossplot(); 
#
# NOTE: You dont have to declare most of the properties shown above
# for the plot object, although it is CRITICAL to declare in the 
# the canvas that the plot object will use! (eg 'the plot device'; 
# $plot_object->plotdev). 
#
# Current available methods other than retrieving/setting properties
# of the plot object include:
#
# $plot_object->clear_plot;
# $plot_object->make_plot;                    # 'aliased' to datacrossplot for now 
# $plot_object->make_datacrossplot;
# $plot_object->set_marker(@@_);               # bind to a mouse button on the canvas 
# $plot_object->tag_position(@@_);             # bind to a mouse button on the canvas 
# $plot_object->set_title('string','color');  # reprints title 
# $plot_object->set_xlabel('string','color'); # reprints Xlabel on graph 
# $plot_object->set_ylabel('string','color'); # reprints Ylabel on graph 
# $plot_object->print_plot;                   # send plot to printer
# $plot_object->clear_plot;                   
# $plot_object->plot_line(x1,y1,x2,y2,color,width);  
# $plot_object->plot_datacross(x,y,xerr,yerr,width,color);  
#

use strict;

# set up object attributes.
# many of these are private variables, which the
# user doesnt see, but are calculated from inputs
# in the calculate_dimensions method. 
attributes qw(
  plotdev
  vectors
  height
  width
  bordersize 
  linewidth
  nrof_xtick
  nrof_ytick 
  minor_xticks_per_int
  minor_yticks_per_int 
  Xmin
  Xmax
  Ymin
  Ymax
  title
  subtitle
  Xlabel
  Ylabel
  labelColor
  lineColor
  plotColor
  tickColor
  borderColor
  plotxlen
  plotylen
  cylen
  cxlen
  Xfactor
  Yfactor
  plotcoord_minx
  plotcoord_miny
  plotcoord_maxx
  plotcoord_maxy
  offset
  show_mean_xline 
  show_mean_yline
  show_origin_line
  DEBUG
);


# methods

sub make_plot {
  my ($obj,$Xmin,$Xmax,$Ymin,$Ymax,$lineColor,$PlotColor,$tickColor,$linewidth) = @@_;

  $obj->make_datacrossplot($Xmin,$Xmax,$Ymin,$Ymax,$lineColor,$PlotColor,$tickColor,$linewidth) if $obj;

}

sub make_datacrossplot {
  my ($obj,$Xmin,$Xmax,$Ymin,$Ymax,$lineColor,$PlotColor,$tickColor,$linewidth) = @@_;
  
  my $g = $obj->plotdev; 

  if($obj && $g) {
    if(!$Xmin) { $Xmin = $obj->Xmin; }
    if(!$Xmax) { $Xmax = $obj->Xmax; }
    if(!$Ymin) { $Ymin = $obj->Ymin; }
    if(!$Ymax) { $Ymax = $obj->Ymax; }
    if(!$lineColor) { $lineColor= $obj->lineColor; }
    if(!$PlotColor) { $PlotColor= $obj->plotColor; }
    if(!$tickColor) { $tickColor = $obj->tickColor; }
    if(!$linewidth) { $linewidth= $obj->linewidth; }

    # set up the plot
    $obj->make_borders($Xmin,$Xmax,$Ymin,$Ymax,$tickColor,$PlotColor,$linewidth);
    $obj->set_title($obj->title,$obj->labelColor);
    $obj->set_xlabel($obj->Xlabel,$obj->labelColor);
    $obj->set_ylabel($obj->Ylabel,$obj->labelColor);

    print "PLOT MINX:$Xmin MAXX:$Xmax MINY:$Ymin MAXY:$Ymax\n" if $obj->DEBUG;

    # now, plot the data
    my @@v = @@{$obj->vectors};
    my @@plotted_ydata = ();
    my @@plotted_xdata = ();
    foreach my $i (0 .. $#v) {
      my ($x,$y) = ($v[$i]->{'X'},$v[$i]->{'Y'}); 
      # my ($xhigh,$yhigh) = ($x+$v[$i]->{'XERR'},$y+$v[$i]->{'YERR'}); 
      # my ($xlow,$ylow) = ($x-$v[$i]->{'XERR'},$y-$v[$i]->{'YERR'}); 
      # now plot only if there is going to be someting within our
      # choosen dataspace!
      #if(($xlow < $obj->Xmax || $xhigh > $obj->Xmin) && 
      #     ($ylow < $obj->Ymax || $yhigh > $obj->Ymin)) { 

      # lets plot only those points within our box 
      if($x < $obj->Xmax && $x > $obj->Xmin && 
           $y < $obj->Ymax && $y > $obj->Ymin) { 
        my $color = $v[$i]->{'color'} ? $v[$i]->{'color'} : 'black';
        $obj->plot_datacross($x,$y,$v[$i]->{'XERR'},$v[$i]->{'YERR'},$color);
        print $x, ' ', $y, ' ',
            $v[$i]->{'XERR'},' ',$v[$i]->{'YERR'},"\n" if $obj->DEBUG;
        @@plotted_ydata = (@@plotted_ydata,$y);
        @@plotted_xdata = (@@plotted_xdata,$x);
      }
    }
    # get some statistics on this data
    my $subtitle_line;
    if(@@plotted_xdata) {
      my $stddev = $#plotted_xdata > 0 ? find_std_dev_data(@@plotted_xdata) 
         : "-1";
      my $mean = find_mean(@@plotted_xdata);
      $subtitle_line = sprintf("Mean X:%8.2f \+/\-%8.2f",$mean ,$stddev)
         if $obj->show_mean_xline;
      $obj->plot_dashed_line($mean,$obj->Ymin,$mean,$obj->Ymax,100,'red',1)
         if $obj->show_mean_xline;
    }
    if(@@plotted_ydata) {
      my $stddev = $#plotted_ydata > 0 ? find_std_dev_data(@@plotted_ydata)
         : "-1";
      my $mean = find_mean(@@plotted_ydata);
      $subtitle_line .= sprintf("\tMean Y:%8.2f \+/\-%8.2f",$mean ,$stddev); 
      $obj->plot_dashed_line($obj->Xmin,$mean,$obj->Xmax,$mean,100,'red',1)
         if $obj->show_mean_yline;
      $obj->plot_dashed_line($obj->Xmin,0,$obj->Xmax,0,25,'black',1)
         if $obj->show_origin_line && ($obj->Ymin<0) && ($obj->Ymax>0);
    }
    $obj->set_subtitle($subtitle_line) if $subtitle_line;
  }
}

sub make_borders {
  my ($obj,$Xmin,$Xmax,$Ymin,$Ymax,$lineColor,$PlotColor,$linewidth) = @@_;

  my $plot = $obj->plotdev; 
  if(!$Xmin) { $Xmin = $obj->Xmin; }
  if(!$Xmax) { $Xmax = $obj->Xmax; }
  if(!$Ymin) { $Ymin = $obj->Ymin; }
  if(!$Ymax) { $Ymax = $obj->Ymax; }
  if(!$lineColor) { $lineColor= $obj->borderColor; }
  if(!$PlotColor) { $PlotColor= $obj->plotColor; }
  if(!$linewidth) { $linewidth= $obj->linewidth; }

  $obj->calculate_dimensions($Xmin,$Xmax,$Ymin,$Ymax);

  # defs for convience in calc below
  my $basex = $obj->plotcoord_minx;
  my $basey = $obj->plotcoord_miny;
  my $maxx = $obj->plotcoord_maxx;
  my $maxy = $obj->plotcoord_maxy;

  # plot boundries
  # the backgrnd of the plot area. Why do we need this?
  # so the cursor can select any area within the plot area
  # for resiziing, position location of cursor, etc.
  $plot->create(('rectangle', $basex, $basey, $maxx, $maxy),
     fill => $PlotColor, tags => 'backgnd');

  # the four sides of the graph
  $plot->create(('line', $basex, $basey, $maxx, $basey),
                fill => $lineColor, width => $linewidth, tags => 'border');
  $plot->create(('line', $basex, $maxy, $maxx, $maxy),
                fill => $lineColor, width => $linewidth, tags => 'border');
  $plot->create(('line', $basex, $basey, $basex, $maxy),
                fill => $lineColor, width => $linewidth, tags => 'border');
  $plot->create(('line', $maxx, $basey, $maxx, $maxy),
                fill => $lineColor, width => $linewidth, tags => 'border');

  my $xticksize = $obj->Xmax / $obj->nrof_xtick;
  my $yticksize = $obj->Ymax / $obj->nrof_ytick;
  my $xticklen = 10;
  my $yticklen = 10;

  foreach my $xtick (0 ... ($obj->nrof_xtick + 1)) {
    my $fract = $xtick/($obj->nrof_xtick+1);
    my $int = $Xmin < $Xmax ? $Xmin: $Xmax;
    my $coordxval = sprintf("%6.2f",($fract * $obj->cxlen) + $int);
    my $xloc = ($fract * $obj->plotxlen)+$obj->bordersize+$obj->linewidth;

    # ticks on bottom of plot 
    $plot->create(('line', $xloc, $maxy, $xloc, $maxy-$xticklen),
                fill => $lineColor, width => $linewidth, tags => 'ticks');

    # ticks on top of plot 
    $plot->create(('line', $xloc, $basey, $xloc, $basey+$xticklen),
                fill => $lineColor, width => $linewidth, tags => 'ticks');

    # minor X ticks on top and bottom of the plot
    if($fract < 1) {
      foreach my $ltick (1 ... ($obj->minor_xticks_per_int+1)) {
        my $minXloc = (($ltick/($obj->minor_xticks_per_int+1))*($obj->plotxlen/($obj->nrof_xtick + 1)))+$xloc;
        $plot->create(('line', $minXloc, $maxy, $minXloc, $maxy-($xticklen/2)),
                fill => $lineColor, width => ($linewidth/2), tags => ['mticks', 'ticks']);
        $plot->create(('line', $minXloc, $basey, $minXloc, $basey+($xticklen/2)),
                fill => $lineColor, width => ($linewidth/2), tags => ['mticks', 'ticks']);
      }
    }

    # For major ticks, we plot coord values
    $plot->create(('text', $xloc, $maxy+20), text => $coordxval,
                justify => 'center', fill => $lineColor, tags => ['ticks', 'tick_num']);

  }

 foreach my $ytick (0 ... ($obj->nrof_ytick + 1)) {
    my $fract = $ytick/($obj->nrof_ytick + 1);
    my $int = $Ymin < $Ymax ? $Ymin: $Ymax;
    my $coordyval = sprintf("%6.2f",($fract * $obj->cylen) + $int);
    my $yloc = ((1.0 - $fract) * $obj->plotylen)+$obj->bordersize+$obj->linewidth;

    # ticks on LHS of plot 
    $plot->create(('line', $basex, $yloc, $basex+$yticklen, $yloc),
                fill => $lineColor, width => $linewidth, tags => 'ticks');

    # ticks on RHS of plot 
    $plot->create(('text', $basex-20, $yloc), text => $coordyval,
                justify => 'center', fill => $lineColor, tags => ['ticks', 'tick_num']);

    # minor Y ticks on RHS and LHS of the plot
    if($fract > 0) {
      foreach my $ltick (0 ... ($obj->minor_yticks_per_int+1)) {
        my $minYloc = (($ltick/($obj->minor_yticks_per_int+1))*($obj->plotylen/($obj->nrof_ytick+1)))+$yloc;
        $plot->create(('line', $basex, $minYloc, $basex+($yticklen/2),$minYloc),
                fill => $lineColor, width => ($linewidth/2), tags => ['ticks','mticks']);
        $plot->create(('line', $maxx, $minYloc, $maxx-($yticklen/2),$minYloc),
                fill => $lineColor, width => ($linewidth/2), tags => ['ticks','mticks']);
      }
    }

    # For major ticks, we plot coord values
    $plot->create(('line', $maxx, $yloc, $maxx-$yticklen, $yloc),
                fill => $lineColor, width => $linewidth, tags => ['ticks', 'tick_num']);
  }
}

sub set_title {
  my ($obj,$label,$color) = @@_;
  my $plot = $obj->plotdev; 
  if(!$color) { $color = $obj->labelColor; }
  if(!$label) { $obj->title; } else { $obj->title($label); }
  $plot->create(('text', ($obj->width/2), ($obj->bordersize/2)),
                text => $label,
                justify => 'center', fill => $color, tags => 'title');
}

sub set_subtitle {
  my ($obj,$label,$color) = @@_;
  my $plot = $obj->plotdev; 
  if(!$color) { $color = $obj->labelColor; }
  if(!$label) { $obj->subtitle; } else { $obj->subtitle($label); }
  $plot->create(('text', ($obj->width/2), ($obj->bordersize*0.9)),
                text => $label,
                justify => 'right', fill => $color, tags => 'title');

}

sub set_xlabel {
  my ($obj,$label,$color) = @@_;
  my $plot = $obj->plotdev; 
  if(!$color) { $color = $obj->labelColor; }
  if(!$label) { $obj->Xlabel; } else { $obj->Xlabel($label); }
  $plot->create(('text', ($obj->width/2), ($obj->height-($obj->bordersize/2))),
                text => $label,
                justify => 'center', fill => $color, tags => 'labels');
}

sub set_ylabel {
  my ($obj,$label,$color) = @@_;
  my $plot = $obj->plotdev; 
  if(!$color) { $color = $obj->labelColor; }
  if(!$label) { $obj->Ylabel; } else { $obj->Ylabel($label); }
  $plot->create(('text', ($obj->bordersize/2), ($obj->height/2)),
                text => $label, width => 1,
                justify => 'center', fill => $color, tags => 'labels');
}

sub calculate_dimensions {
  my ($obj,$Xmin,$Xmax,$Ymin,$Ymax) = @@_;

  # defaults
  if(!$Xmin) { $Xmin = $obj->Xmin; }
  if(!$Xmax) { $Xmax = $obj->Xmax; }
  if(!$Ymin) { $Ymin = $obj->Ymin; }
  if(!$Ymax) { $Ymax = $obj->Ymax; }

  # set/define some useful numbers
  $obj->plotcoord_minx(($obj->linewidth+$obj->bordersize)); 
  $obj->plotcoord_miny($obj->linewidth+$obj->bordersize);
  $obj->plotcoord_maxx($obj->width+(2*$obj->linewidth)-$obj->bordersize);
  $obj->plotcoord_maxy($obj->height+(2*$obj->linewidth)-$obj->bordersize);
  $obj->plotxlen($obj->width-(2*$obj->bordersize)+$obj->linewidth); 
  $obj->plotylen($obj->height-(2*$obj->bordersize)+$obj->linewidth);
  $obj->cxlen(abs($Xmax - $Xmin));
  $obj->cylen(abs($Ymax - $Ymin));
  $obj->Xfactor(($obj->plotxlen/$obj->cxlen));
  $obj->Yfactor(($obj->plotylen/$obj->cylen));
  $obj->offset($obj->bordersize + $obj->linewidth);

}

sub plot_text {
  my ($obj,$msg,$xstart,$ystart,$color,$just) = @@_;

  $color = 'black ' if !$color;
  $just = 'center' if !$just;

  my $dev = $obj->plotdev;
  $dev->create(('text', $xstart, $ystart),
            text => $msg, fill => $color, tags => 'text', anchor => $just);

}

sub plot_line {
  my ($obj,$xmin,$ymin,$xmax,$ymax,$color,$mywidth) = @@_;

  my $pl = $obj->plotdev;

  if(!defined $color) { $color = $obj->lineColor; }
  if(!defined $mywidth) { $mywidth = $obj->linewidth; }

  my $xstart = ( (($xmin-$obj->Xmin)/$obj->cxlen)*$obj->plotxlen)+$obj->offset;
  my $xend = ((($xmax-$obj->Xmin)/$obj->cxlen)*$obj->plotxlen)+$obj->offset;

  my $ystart = ((($obj->Ymax-$ymin)/$obj->cylen)*$obj->plotylen)+$obj->offset;
  my $yend = ((($obj->Ymax -$ymax)/$obj->cylen)*$obj->plotylen)+$obj->offset;

  $pl->create(('line', $xstart, $ystart, $xend, $yend),
                fill => $color, width => $mywidth, tags => 'line');
}

sub plot_dashed_line {
  my ($obj,$xmin,$ymin,$xmax,$ymax,$ndash,$color,$mywidth) = @@_;

  if(!$ndash) { $ndash = 10; }
  if(!defined $color) { $color = 'black'; }
  if(!defined $mywidth) { $mywidth = 1; }

  my $xincr_size = ($xmax - $xmin)/($ndash*2);
  my $yincr_size = ($ymax - $ymin)/($ndash*2);
  my $xstart = $xmin;
  my $ystart = $ymin;
  foreach my $segment (1 ... $ndash) {
    $obj->plot_line($xstart,$ystart,$xstart+$xincr_size,$ystart+$yincr_size,
       $color,$mywidth);
    $xstart += $xincr_size*2;
    $ystart += $yincr_size*2;
  }

}

sub plot_symbol {
  my ($obj,$plotx,$ploty,$sym,$color,$tag,$just) = @@_;
 
  my $name = $obj->plotdev;

  if(!defined $tag) { $tag = 'symbol'; }
  if(!defined $color) { $color = 'white'; }
  if(!defined $just) { $just = 'c'; }

  $name->create(('text',$plotx,$ploty), text => $sym,
     fill => $color, tags => $tag, anchor => $just);
}

sub plot_one_symbol {
  my ($obj,$plotx,$ploty,$sym,$color,$tag) = @@_;
  
  $obj->remove_items_with_tag($tag);
  $obj->plot_symbol($plotx,$ploty,$sym,$color,$tag);

}

sub remove_items_with_tag {
  my $obj= $_[0];
  my $p = $obj->plotdev;

  my $name = $_[1];

  my $this = $p->find('withtag', $name);
  my @@list = $p->gettags($this);
  foreach my $tag (@@list) { $p->delete($tag); }
}

sub set_marker {
  my $obj = $_[0];
  my $color = $_[1];

  if(!defined $color) { $color = 'firebrick'; }

  my $name = $obj->plotdev;
  my $e = $name->XEvent;
  my ($plotx,$ploty) = $obj->winToGraphCoords($e->x,$e->y);

  if(defined $plotx && defined $ploty) {
    $obj->plot_one_symbol($e->x,$e->y,"X",'red','markerTag');
    $obj->remove_items_with_tag('title');
    $obj->set_subtitle("MARKER X:$plotx Y:$ploty");
  }

}

sub print_plot {
  my ($obj,$rotate,$filename) = @@_;

  my $plot = $obj->plotdev;
  if(! defined $rotate) {$rotate = 0; }

  if (!$filename) { 
    $filename = "/tmp/tmp.ps";
    $plot->postscript(-file => $filename, -rotate => $rotate);
    system ("lp -c $filename; rm -f $filename");
  } else {
    $plot->postscript(-file => $filename, -rotate => $rotate);
  }
}

sub winToGraphCoords {
  my ($obj, $xval, $yval) = @@_;
  my $name = $obj->plotdev;
  my $xloc = sprintf("%11.4f",(($xval - $obj->offset)/$obj->Xfactor)+$obj->Xmin);
  my $yloc = sprintf("%11.4f",(-1*($yval - $obj->offset)/$obj->Yfactor)+$obj->Ymax);
  return ($xloc,$yloc);
}

sub tag_position {
  my $obj = $_[0];
  my $name =$obj->plotdev;
  my $e = $name->XEvent;
  my ($plotx,$ploty) = $obj->winToGraphCoords($e->x,$e->y);
  if(defined $plotx && defined $ploty) {
    my $x = sprintf("%6.2f",$plotx);
    my $y = sprintf("%6.2f",$ploty);
    $obj->plot_symbol($e->x,$e->y,"X",'yellow','position');
    $obj->plot_symbol($e->x+5,$e->y," [$x,$y]",'yellow','position','w');
  }
}

sub clear_plot {
  my $obj = $_[0]; 

  if($_[1]) {
    $obj->remove_items_with_tag("$_[1]");
  } else { # generic  
    $obj->remove_items_with_tag('data');
    $obj->remove_items_with_tag('line');
    $obj->remove_items_with_tag('border');
    $obj->remove_items_with_tag('ticks');
    $obj->remove_items_with_tag('title');
    $obj->remove_items_with_tag('labels');
    $obj->remove_items_with_tag('backgnd');
  }
}

sub plot_datacross {
  my ($obj,$x,$y,$xerr,$yerr,$color,$width) = @@_;
  my $g = $obj->plotdev;

  if(!$color) { $color = 'black'; }
  if(!$width) { $width= 1; }

  my ($lowy, $highy) = ($y-abs($yerr), $y+abs($yerr));
  my ($lowx, $highx) = ($x-abs($xerr), $x+abs($xerr));
 
  # prevent data cross from over-drawing the boundry of the graph
  if($lowx < $obj->Xmin) { $lowx = $obj->Xmin; }
  if($lowy < $obj->Ymin) { $lowy = $obj->Ymin; }
  if($highx > $obj->Xmax) { $highx = $obj->Xmax; }
  if($highy > $obj->Ymax) { $highy = $obj->Ymax; }

  $obj->plot_line($x,$lowy,$x,$highy,$color,$width); # y-axis part of the data point
  $obj->plot_line($lowx,$y,$highx,$y,$color,$width); # x-axis part of the data point

}

sub add_vector {
  my $obj = $_[0];
  my $entry = $_[1];

  my $test = ${$obj->vectors}[0]->{'X'};
  my $tablenum = defined $test ? $#{$obj->vectors} + 1 : 0;
 
  ${$obj->vectors}[$tablenum] = $entry;

}

sub find_mean {
  my @@data = @@_;
  my $sum;
  foreach my $entry (@@data) { $sum += $entry; }

  return ($sum/($#data+1));
}

sub find_std_dev_data {
  my @@data = @@_;
  my $val = -1;
  my $sum = $#data + 1;
  my $sumx = 0;
  my $sigma = 0;


  foreach my $entry (@@data) { $sumx += $entry; }

  # evaluate mean and standard deviations
  my $xmean = $sumx/$sum;

  foreach my $entry (@@data) {
    $sigma += ($entry - $xmean)**2;
  }
  $sigma = ($sigma/$#data)**0.5;
  #my $sigmam = ($xmean/$sum)**0.5;

  #print "Standard deviation of mean: $sigmam\n";
  #print "Standard deviation of data: $sigma\n";
  return $sigma;
}

1;
 
@


1.3
log
@*** empty log message ***
@
text
@d3 2
a4 2
# Filename    : 
# Subsystem   : ??
d6 2
a7 2
# Description : 
# RCS: $Id: CanvasPlotObj.pm,v 1.2 1999/01/28 19:21:28 socops Exp socops $
d50 1
d80 1
a80 1
# $plot_object->plot_line(x1,y1,x2,y2,width,color);  
d113 1
d127 1
d134 5
a138 4
# specialized types of plots
sub make_symbol_plot { 
  my ($obj,$symbol,$Xmin,$Xmax,$Ymin,$Ymax,$lineColor,$PlotColor,$tickColor,$linewidth) = @@_;
  make_plot($obj,'symbol',$symbol,$Xmin,$Xmax,$Ymin,$Ymax,$lineColor,$PlotColor,$tickColor,$linewidth);
d141 1
a141 1
sub make_datacrossplot { 
a142 5
  make_plot($obj,'datacross','+',$Xmin,$Xmax,$Ymin,$Ymax,$lineColor,$PlotColor,$tickColor,$linewidth); 
}

sub make_plot {
  my ($obj,$type,$symbol,$Xmin,$Xmax,$Ymin,$Ymax,$lineColor,$PlotColor,$tickColor,$linewidth) = @@_;
a143 4
  # safeties
  $type = 'datacross' if !$type;
  $symbol = '+' if $type eq 'symbol' && !$symbol;

d170 6
d180 2
a181 7
        if($type eq 'datacross') {
           $obj->plot_datacross($x,$y,$v[$i]->{'XERR'},$v[$i]->{'YERR'});
        } elsif ($type eq 'symbol') { 
           $obj->plot_symbol($x,$y,$symbol,$PlotColor);
        } else {
           die "UNKNOWN plot type called in CanvasPlotObj.pm";
        }
d206 2
a212 1

d221 1
a221 1
  if(!$lineColor) { $lineColor= $obj->lineColor; }
d320 1
a320 1
  if(!$color) { $color = 'black'; }
d330 1
a330 1
  if(!$color) { $color = 'black'; }
d341 1
a341 1
  if(!$color) { $color = 'black'; }
d351 1
a351 1
  if(!$color) { $color = 'black'; }
d382 12
d399 2
a400 2
  if(!defined $color) { $color = 'black'; }
  if(!defined $mywidth) { $mywidth = 1; }
d483 1
a483 2
  my ($obj,$rotate) = @@_;
  my $filename = "/tmp/tmp.ps";
d488 7
a494 2
  $plot->postscript(-file => $filename, -rotate => $rotate);
  system ("lp -c $filename; rm -f $filename");
d535 1
a535 1
  my ($obj,$x,$y,$xerr,$yerr) = @@_;
d538 3
d550 2
a551 2
  $obj->plot_line($x,$lowy,$x,$highy); # y-axis part of the data point
  $obj->plot_line($lowx,$y,$highx,$y); # x-axis part of the data point
@


1.2
log
@added make_symbol_plot
@
text
@d7 1
a7 1
# RCS: $Id: CanvasPlotObj.pm,v 1.1 1998/12/04 19:02:47 socops Exp socops $
d133 2
a134 2
  my ($obj,$Xmin,$Xmax,$Ymin,$Ymax,$lineColor,$PlotColor,$tickColor,$linewidth,$symbol) = @@_;
  make_plot($obj,$Xmin,$Xmax,$Ymin,$Ymax,$lineColor,$PlotColor,$tickColor,$linewidth,'symbol',$symbol); 
d139 1
a139 1
  make_plot($obj,$Xmin,$Xmax,$Ymin,$Ymax,$lineColor,$PlotColor,$tickColor,$linewidth,'datacross'); 
d143 1
a143 1
  my ($obj,$Xmin,$Xmax,$Ymin,$Ymax,$lineColor,$PlotColor,$tickColor,$linewidth,$type,$symbol) = @@_;
@


1.1
log
@Initial revision
@
text
@d7 1
a7 1
# RCS: $Id$
d131 7
a137 1
sub make_plot {
d139 1
a139 3

  $obj->make_datacrossplot($Xmin,$Xmax,$Ymin,$Ymax,$lineColor,$PlotColor,$tickColor,$linewidth) if $obj;

d142 2
a143 2
sub make_datacrossplot {
  my ($obj,$Xmin,$Xmax,$Ymin,$Ymax,$lineColor,$PlotColor,$tickColor,$linewidth) = @@_;
d145 4
a174 6
      # my ($xhigh,$yhigh) = ($x+$v[$i]->{'XERR'},$y+$v[$i]->{'YERR'}); 
      # my ($xlow,$ylow) = ($x-$v[$i]->{'XERR'},$y-$v[$i]->{'YERR'}); 
      # now plot only if there is going to be someting within our
      # choosen dataspace!
      #if(($xlow < $obj->Xmax || $xhigh > $obj->Xmin) && 
      #     ($ylow < $obj->Ymax || $yhigh > $obj->Ymin)) { 
d179 7
a185 1
        $obj->plot_datacross($x,$y,$v[$i]->{'XERR'},$v[$i]->{'YERR'});
d214 1
@

head	1.9;
access;
symbols;
locks; strict;
comment	@% @;


1.9
date	2002.11.20.21.07.55;	author bob;	state Exp;
branches;
next	1.8;

1.8
date	2000.10.24.17.03.05;	author bob;	state Exp;
branches;
next	1.7;

1.7
date	2000.06.16.19.55.06;	author bob;	state Exp;
branches;
next	1.6;

1.6
date	2000.03.27.15.56.07;	author bob;	state Exp;
branches;
next	1.5;

1.5
date	99.09.07.20.54.04;	author bob;	state Exp;
branches;
next	1.4;

1.4
date	99.08.24.22.03.22;	author bob;	state Exp;
branches;
next	1.3;

1.3
date	99.02.23.18.56.25;	author socops;	state Exp;
branches;
next	1.2;

1.2
date	99.02.20.15.42.17;	author socops;	state Exp;
branches;
next	1.1;

1.1
date	99.02.04.19.20.18;	author socops;	state Exp;
branches;
next	;


desc
@@


1.9
log
@added new sffits directory for new ASM FITS light curve processing
and shut off old ftp links for solutions files (to ASM weather page.)
@
text
@#!/usr/local/bin/perl -w
# ===========================================================================
# Filename    : sf_process.pl
# Subsystem   : SOF Scripts - ASM
# Programmer  : Brian Thomas, Robert Schaefer
# Description : A program to be run by gpacman. It accepts files
#               and proceeds to crunch them as proscribed by
#               the "crunch_file" subroutine. This program is a
#               template for other programs which use gpacman.
#
# Modules     : ParameterManager.pm
#
my $rcsID = '$Id: sf_process.pl,v 1.8 2000/10/24 17:03:05 bob Exp bob $';
# this makes emacs quote matching behave: '
# ===========================================================================

# parse RCS id
my ($rcs_id_and_name,$vers_date_coders) = split(/,v \b/,$rcsID);
my ($version,$versdate,$verstime,$coders ) = split(" ",$vers_date_coders);

# this BEGIN block sets the @@INC path so the interpreter can
# find locally developed Perl modules
BEGIN {
  my $sochome = $ENV{"SOCHOME"};
  if (!defined($sochome)) { $sochome="/sochome"; $ENV{"SOCHOME"}=$sochome; }
  unshift( @@INC, "$sochome/lib/SunOS5" );
}

use ParameterManager; 
my $params = new ParameterManager("asm/ASMdefs.config");

use strict;

# S E T U P 

# signal handling
$SIG{'HUP'} = "my_exit";
$SIG{'INT'} = "my_exit";
$SIG{'QUIT'} = "my_exit";

# program defs
my $sochome = $ENV{"SOCHOME"};
my $xdir = "$sochome/bin/SunOS5";      # where executables live
my $ftpdir = $params->get_scalar("ftp_dir");  # ftp directory, put links to good files here 
my $sffitsdir = $params->get_scalar("sffits_dir");  # FITS making spool directory, put links to good AND confused files here
# my $sffitsdir = "/xte/socops/asm/sf4fits";
my $asmdbdir = $params->get_scalar("asmdb_spool");  # links to sf files here 
my $tmpasmdir = $params->get_scalar("tmpasmdb_spool");  # links to sf files here 
my $archdir = $params->get_scalar("sfarchive_dir");  # where we archive all processed data ultimately.
my $warn_email = $params->get_scalar("warn_email");  # where we link moved sf files to
my $spool_dir = $params->get_scalar("spool_dir"); # where files wait their turn
my $min_size = $params->get_scalar("min_sf_size"); 
my $jail_dir = $params->get_scalar("bad_sf_dir"); # Bad files go here immediately

my $process_later_string = ".to_be_processed";
my $program = "$xdir/sf_filter-single";
my $clobber_old_links = 0;
my $new_too_processing = 0; # are we processing via the new TOO stuff
my $debug = 0;

# argv loop
while ($_ = shift @@ARGV) {
  if(/-C/) { $clobber_old_links = 1; }
  if(/-N/) { $new_too_processing = 1; $clobber_old_links = 1; }
  if(/-v/) { $debug = 1; }
}


# P R O G R A M  

print STDERR "VER $version $0\n" if $debug;

# runtime loop, slurp up STDIN as its given to us
while (<STDIN>) {

  print STDERR "$_" if $debug;
  my @@array = split;      # input array is '.begin <inputfile> <outputfile>'

  &crunch_file($array[1],$array[2]);

  print STDERR ".done $array[1]\n\n" if $debug;
  print STDOUT ".done $array[1]\n"; # tell gpacman we are done w/ this file
  $| = 1; # flush STDOUT, gpacman requires this
}
print STDERR "$0 dies from no STDIN.\n" if $debug;


# S U B R O U T I N E S 

sub my_exit {
  print STDERR "$0 recieved kill signal!\n" if $debug;
  exit 0;
}

sub crunch_file {
  my ($infile, $outfile) = @@_;
  my $max_tries = 30;
  my $previously_processed = 0;

  # clip off that process_later_string crap
  if ($outfile =~ $process_later_string) {
    my @@tmp = split($process_later_string, $outfile);
    $outfile = $tmp[0];
  } 
  print STDERR "input:$infile output:$outfile\n" if $debug;

  # We check to see if the pacman process is finished with 
  # the progenitor (used to be a da file), a dw file first
  my @@names = split ('\.',$outfile);
  my @@first = split ("sf" , $names[0]);

  #if($debug){ print STDERR join(".",@@names),"\n",join(" ",@@first),"\n";}

  my $lockfile = $spool_dir . '/locked.dw' . $first[1] . '.' . $names[1] . '.locked'; 

  # if we are running standard processing and new too processing has already 
  #  beaten us to making the file, surrender gracefully and allow new TOO sf
  #  file to stand.  (At least we hope that new TOO processing made it...)
  if( (-e "$archdir/$outfile") && !($new_too_processing) ) { 
     # but do not quit if the file is empty
     if( (-s "$archdir/$outfile") > 0 ) { return; }
  }

  # This allows for other gpacman (dwfit/sf_process) to finish writing 
  # the SF file we are interested in. 
  if($new_too_processing) { # we need to see if the sf file is finished 
     $lockfile = $spool_dir . '/locked.' . $outfile . '.locked'; 
  }

  while ((-e $lockfile) && $max_tries-- ) { 
    print STDERR "$lockfile exists... $0 waiting\n" if $debug;
    sleep 60; 
  } 

  # this is a hack, sure, but I have found many SF
  # files wind up at sizes of either 0, 8192, or 16384 for a 
  # good long time while they are writing out under dafit.
  # Also, there are no good SF files of size less than
  # min_size. For all these situations, we wait around for
  # the file to change size (via writing!) 
  $max_tries = 30;
  my $size = (-s $infile);
  print STDERR "$outfile has size $size...\n" if $debug;
  while (( ($size == 8192) || ($size == 16384) || ($size == 24576) || 
    !($size) || ($size < $min_size) ) && $max_tries-- ) { 
    print STDERR "$outfile has size $size... $0 waiting\n" if $debug;
    sleep 60; 
    $size = (-s $infile);
  }

  # 1) move all files to the archive directory
  #   also, a warning check
  if ($max_tries < 1) {
    my $buf = "There was a problem obtaining the SF file:$infile ($lockfile still exists!).\n$0 aborting!";
    system "echo \"$buf\" | /usr/ucb/mail -s ASM_WARNING:sf_check_fail $warn_email";
    system("cat $infile > $jail_dir/$outfile");
    return;
  } 

  # 2) determine status of our file
  #  open a pipe from the program output...
   open(FILETEST, "$program $infile 2>&1 |"); #program writes to stderr
   my $test = <FILETEST>; 
   close(FILETEST);
   print STDERR "sf_filter test yields: $test\n" if $debug; 

  my @@stat;
  if($test) {
    @@stat = split(" ",$test); 
    system("cat $infile > $outfile"); # its now ok to move the file to outdir 
  } elsif($previously_processed) {
    # We have a problem, email watcher and move file to jail
    my $buf = "$0 failed to test file:$infile";
    system "echo \"$buf\" | /usr/ucb/mail -s ASM_WARNING:sf_check_fail $warn_email";
    system("cat $infile > $jail_dir/$outfile");
    return;
  } else {
    # We have a problem, leave it in the spool
    my $new_name = $outfile . $process_later_string;
    system("cat $infile > $spool_dir/$new_name"); 
    return;
  }

  # 3) if the file is 'good' then lets link to the ftp directory
  # so the GOF can get it.
  # 4) if the file is good add it to the asmdb for Toshi's light curve web page.
  if(@@stat && $stat[1] eq 'good') { 
    # the SOF no longer will ftp solutions files to the ASM weather map
    # page.  THe SOF will now create FITS files of the source light curves
    # and send them via a different program.
    # make_link("$archdir/$outfile","$ftpdir/$outfile");
     #    Toshi wanted the web page to do the filtering, but the current filtering
     #    is much more sophisticated than the web program, and Toshi is no longer
     #    here to maintain the web pages - so we will do the filtering here!
     make_link("$archdir/$outfile","$tmpasmdir/$outfile"); 
     make_link("$archdir/$outfile","$asmdbdir/$outfile");
     make_link("$archdir/$outfile","$sffitsdir/$outfile");
  }

   if(@@stat && $stat[1] eq 'conf') {
     make_link("$archdir/$outfile","$sffitsdir/$outfile");
   } 
 


}

sub make_link {
    my ($hard, $link) = @@_;

    if(-e $link) { # what to do if link exists
       # if $clobber is set or the link is invalid or points to an empty file
       if($clobber_old_links || !(-s $link) ) { unlink("$link"); }
       # otherwise, leave it alone
       else { return; }
    }
    print STDERR "$0: linking $link to point to $hard\n" if $debug;
    if(!symlink($hard, $link)) { 
      print STDERR "$0: failed to make link $link to $hard\n";  
    }
}

@


1.8
log
@changed rsf_filter-single to sf_filter-single
@
text
@d13 1
a13 1
my $rcsID = '$Id: sf_process.pl,v 1.7 2000/06/16 19:55:06 bob Exp bob $';
d45 2
d111 3
d188 4
a191 1
     make_link("$archdir/$outfile","$ftpdir/$outfile");
d197 1
d199 5
@


1.7
log
@updated sf_process to look fir locked.dw* file, not locked.da file
also added a check that the size is not 24576 bytes, because we
have an .SFjail direvctory full of files that size.  The real problem,
though is that since it was keying off the wrong locked.* file,
sf_process was grabbing the files before they qlgo_ra_dw was finished
writing them (had only written 24576 bytes.
@
text
@d13 1
a13 1
my $rcsID = '$Id: sf_process.pl,v 1.6 2000/03/27 15:56:07 bob Exp bob $';
d54 1
a54 1
my $program = "$xdir/rsf_filter-single";
d160 1
a160 1
   print STDERR "rsf_filter test yields: $test\n" if $debug; 
@


1.6
log
@changed program to only send good data to web page light curve database
@
text
@d13 1
a13 1
my $rcsID = '$Id: sf_process.pl,v 1.5 1999/09/07 20:54:04 bob Exp bob $';
d106 1
a106 1
  # the progenitor DA file first
d109 1
a109 1
  my $lockfile = $spool_dir . '/locked.da' . $first[1] . '.' . $names[1] . '.locked'; 
d119 1
a119 1
  # This allows for other gpacman (dafit/sf_process) to finish writing 
d139 2
a140 1
  while (( ($size == 8192) || ($size == 16384) || !($size) || ($size < $min_size) ) && $max_tries-- ) { 
@


1.5
log
@moved BEGIN block above the use ParameterManager statement.  Somehow,
  the use statement was being executed before the BEGIN block.
Also now opens a pipe from rsf_filter-single, instead of sending output
 to a file, then opening the file, reading it and deleting it...
@
text
@d13 1
a13 1
my $rcsID = '$Id: sf_process.pl,v 1.4 1999/08/24 22:03:22 bob Exp bob $';
d180 1
d183 5
a189 4
  # 4) We do this to updating the asmdb for Toshi's light curve  web page.
  #    Toshi wants to do the filtering himself so send everything!
  make_link("$archdir/$outfile","$tmpasmdir/$outfile"); 
  make_link("$archdir/$outfile","$asmdbdir/$outfile");
@


1.4
log
@new version prevents overwriting of files and links by regular
processing.
Other changes:
1) version number is now RCS version number.
2) uses BEGIN block to add $SOCHOME/lib/SunOS5.
3) looks for rsf_filter-single in $SOCHOME/bin/SunOS5.
4) checks to see if sf file already exists in archive before overwriting it.
5) make_link subroutine changes: a) if link exists and link is valid or
$clobber_links is not set - return, b) use symlink to make new links
6) as per Toshi's request asmdb now gets unfiltered sf files.
@
text
@d13 1
a13 1
my $rcsID = '$Id: sf_process.pl,v 1.3 1999/02/23 18:56:25 socops Exp bob $';
a20 7
use ParameterManager; 
my $params = new ParameterManager("asm/ASMdefs.config");

use strict;

# S E T U P 

d29 7
d47 3
a49 3
my $archdir = $params->get_scalar("sfarchive_dir");   # where we archive all processed data ultimately.
my $warn_email = $params->get_scalar("warn_email");   # where we link moved sf files to
my $spool_dir = $params->get_scalar("spool_dir"); # where files wait thier turn
d155 5
a159 10
  my $testfile = "/tmp/sffiletestresult";
  if(-e $testfile) { $testfile = "/tmp/sffileresult2"; }
 
  system "$program $infile 2> $testfile";
  print STDERR "$program $infile 2> $testfile\n" if $debug;
  open(FILE, "$testfile"); 
  my $test = <FILE>; 
  close(FILE);
  print STDERR "rsf_filter test yields: $test\n" if $debug; 
  system "rm -f $testfile"; 
@


1.3
log
@changed call to Perl so that it calls plain perl rather than perl5
@
text
@d1 19
a19 1
#!/usr/local/bin/perl -w -I/sochome/lib/SunOS5 
a20 8
# RCS: $Id: sf_process.pl,v 1.2 1999/02/20 15:42:17 socops Exp socops $

# program to be run by gpacman. It accepts files
# and proceeds to crunch them as proscribed by
# the "crunch_file" subroutine. This program is provided
# as a template for "real" programs.

# by -b.t. (thomas@@xplorer.gsfc.nasa.gov)
d28 8
d42 6
a47 7
my $version = "1.0";
my $mdir = $params->get_scalar("mit_bin_dir");   # the MIT executabe bin
my $ftpdir = $params->get_scalar("ftp_dir");       # ftp directory, move good files to here 
my $asmdbdir = $params->get_scalar("asmdb_spool");  # ftp directory, move good files to here 
my $tmpasmdir = $params->get_scalar("tmpasmdb_spool");  # links for tmpasmdb here 
my $archdir = $params->get_scalar("sfarchive_dir");   # where we archive all processed data
                                                      # ultimately. Bad files go here immediately
d49 1
a49 1
my $spool_dir = $params->get_scalar("spool_dir");
d51 1
a51 1
my $jail_dir = $params->get_scalar("bad_sf_dir");
d54 1
a54 1
my $program = "$mdir/rsf_filter-single";
d98 1
a98 1
  # clip off that process_later_str crap
d111 8
d159 1
a159 1
  print "$program $infile 2> $testfile\n" if $debug;
d162 2
a163 1
  close(FILE); 
a186 1
     make_link("$archdir/$outfile","$asmdbdir/$outfile");
d189 2
a190 1
  # 4) We do this purely for tempasmdb updating
d192 1
d199 10
a208 4
    unlink("$link") if ((-e $link) && $clobber_old_links); 
    print STDERR "$0: ln -s $hard $link\n" if $debug;
    system("ln -s $hard $link"); 

@


1.2
log
@adding RCS Id tag
@
text
@d1 1
a1 1
#!/usr/local/bin/perl5 -w -I/sochome/lib/SunOS5 
d3 1
a3 1
# RCS: $Id$
@


1.1
log
@Initial revision
@
text
@d3 2
@

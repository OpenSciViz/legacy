#! /bin/csh -f
 
# Step 3 of processing, convert too files to ebx files

# we only run this on $machine
set machine = "xtest"
 
# Watch out! DONT use a string similar to the
# 'spool' and 'analysis' directory names, this
# will cause beaucoup problems w/ known processing.
# use some other names, like those below.
set indir = "/xte/socops/asm/backprocess/analysis"
set outdir = "/xte/socops/asm/backprocess/analysis"
 
#set host = `/usr/ucb/hostname`
#if ($host != $machine ) then
#  echo " "
#  echo $0 only is to be run on $machine!
#  echo " "
#  exit 0
#endif
 
echo -n "$0 start at "
date
 
# nuke process-da nqlgo
/usr/local/bin/nuke datg2 toofit.go
rm -f $indir/lock*too*
/bin/rm -f $outdir/*too*.input
 
cp /xte/socops/asm/catalog $outdir/asmcat
/xte/bin/gpacman -1 -d -x /xte/bin/toofit.go -F too -p tlog -S $indir -O $outdir &

echo -n "$0 stop at "
date


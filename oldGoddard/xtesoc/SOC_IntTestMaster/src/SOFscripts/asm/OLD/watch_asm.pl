#! /usr/local/bin/perl -I/xte/lib/perl

# VERSION 3.5  10-8-98 
# Written by b.t. (thomas@xplorer.gsfc.nasa.gov)

# SUMMARY
# This is supposed to "continously" monitor the known TOO
# reports for real TOO.
#

# Details --
# Each time this routine executes, it adds in any new
# TOO entries from the current known_x_too.report file to 
# an archive file of entries. Then, based on an interval 
# (in days) supplied to it, this program  will remove the 
# oldest entries from the archive. Finally, we scan through 
# all of the current TOO entries in the archive to determine 
# if a source has reached "trigger" criteria. 

# Changes --
#  11/10/96 Now, we also check up on the processing in the SOF
#  A script, chk_sof_processing.pl is called to do this.
#
#  1/16/97 Triggering criteria revised so that only ssc 0,1
#  can trigger a TOO alert. 
#
#  1/24/97 Commented out duplicate source message
#
#  2/13/97 Configured program for new MIT sf file format and 
#  processing style. Watch_asm now is responsible for updating
#  the daily known_too reports. 
#
#  2-20-97 Cosmetic changes to the archive, added in current
#  MJD time.
#
#  3-7-97  Added alert for stale archive. Now, if the oldest
#  entry in the archive is older than $stale_arch_limit value
#  (in fractional days) then an alert will be sounded.
#
#  3-11-97 We now check for newtoo results and 
#  create a noise/popup window if they are ready -b.t.
#
#  7-17-97 Further compartmentalize and clean up code
#
#  7-18-97 lets save the quality of the dwell in the archive. This
#  way, we only have to determine it once.
#
#  7-24-97 Code is fully compartmentalized. Putting sharable 
#  subroutines in Lib.pl 
#
#  8-29-97 Code in ability to selectively print output. Now we 
#  can choose not to see either archive or alarms, etc.
# 
#  1-31-98 deleted the removal of asmarchfile. THis caused occasional
#  runtime problems for the asmarchtool (which couldnt find a file).
#  Now, we just overwrite the old files.

# 2/2/98 CWH changed /tmp/$archfile to /tmp/asm_archive. The former was being
#  interpreted as /tmp//asm/data/asm_archive, which it couldn't create, and
#  this was keeping the archive from being updated.
#
# 2-3-98 moved library routines to /asm/lib/perl

# 9-15-98 added in the ParameterManager -b.t.

# 10-8-98 changed sf search path, not using the day, or ftp directories
#         anymore.

# I N I T I A L I Z A T I O N

use Alarm;             # General purpose library routines 
use Lib;               # General purpose library routines 
use Trigger_Criteria;  # ASM Global trigger criteria and subroutines 
use Quality;           # General purpose library routines 
use Output;            # General purpose library routines 
use ParameterManager;

my $params = new ParameterManager("asm/ASMdefs.config");
 
# PROGRAM DEFs
$sleep_interval = 300;  # number of seconds between checks
$interval = 1.0;        # in days, after this time entries are
                        # removed from the archive of detections

$verb = 1;              # always defaults to verbose now 

$check_processing = 1;  # check the TOO processing (process-da, etc.) 


#$root = "/xte/socops/asm";   
# the root directory for day files this is also default dir for old_alerts
# and the archive files.
$root = $params->get_scalar("root_dir");

$archfile = $params->get_scalar("asmarchive");
#$archfile = "$root/asm_archive";
 
$num_det[0] = $params->get_scalar("min_dwells_for_secure_trigger"); 
$num_det[1] = $params->get_scalar("min_dwells_for_good_trigger"); 
$num_det[2] = $params->get_scalar("min_dwells_for_iffy_trigger"); 
$num_det[3] = $params->get_scalar("min_dwells_for_bad_trigger"); 

#$num_det[0] = 3;    # nrof 'secure' detections we need before alerting.
#$num_det[1] = 3;    # nrof 'good' & secure detections we need before alerting.
#$num_det[2] = 6;    # nrof good, secure and 'iffy' results before alerting.
#$num_det[3] = 0;    # nrof bad, good, secure detections needed.

# to trigger an alert, dwells must have at least 1 detection in ssc 0.
#$require_ssc0 = 0;  
$require_ssc0 = $params->get_scalar("require_ssc0");  

# who to send mail to if there is a detected TOO
$alert_mail_to = 'socops@xsm5 xteplan@athena.gsfc.nasa.gov rr@space.mit.edu';
$alert_mail_to = 'thomas@xplorer.gsfc.nasa.gov rr@space.mit.edu';

# RUN-TIME INITIALIZATION

# argv loop
foreach $i (0 .. $#ARGV) { 
# print $ARGV[$i], "\n";
  if($ARGV[$i] =~ "-h") { $help = 1; }
  if($ARGV[$i] =~ "-q") { $verb = 0; }
  if($ARGV[$i] =~ "-n") { $check_processing = 0; }
  if($ARGV[$i] =~ "-r") { $root = $ARGV[++$i]; } 
  if($ARGV[$i] =~ "-i") { $interval = $ARGV[++$i]; } 
  if($ARGV[$i] =~ "-s") { $sleep_interval = $ARGV[++$i]; } 
  if($ARGV[$i] =~ "-a") { $archfile = $ARGV[++$i]; } 
  # options for this are "archive" and "alarms"
  if($ARGV[$i] =~ "-p") { $selprint = $ARGV[++$i]; }
} 

# some help on options
if($help) {
  print "\n$0 options:\n" ;
  print "\n -a\t Path/name of archive file\n\t (default: /???/asm_archive).\n";
  print "\n -i\t Time interval over which to archive\n\t ASM detections in days (default: 1 day).\n";
  print "\n -h\t This help message. \n";
  print "\n -n\t Don't check ASM processing in SOF, just check for TOO. \n";
  print "\n -q\t Quiet mode.\n";
  print "\n -p\t Print option. Choices are: 1 (just archive) and 2 (just alarms) \n\t (default:$selprint).\n";
  print "\n -r\t Path to location of 'day' directories \n\t (default: $root/too).\n";
  print "\n -s\t How long to pause between runs in seconds\n\t (default: 300 seconds).\n\n";
  exit 0;
}

# PROGRAM BEGINS HERE
print "\n$0 running\n" if ($verb);

while (1) {

  # Step 1: Update the daily known too reports
  #
  print " Updating all daily known too reports\n" if ($verb);
  system("/xte/bin/update_day_report.pl");

  # Step 2: find the latest day directory and go there 
  #
  #$dir = &Lib'get_latest_daydir("/asm/data/too");
  $dir = &Lib'get_latest_daydir("$root/too");
  print " Processing $dir \n" if ($verb);
  print " Evaluating entries recieved over last $interval days\n" if ($verb); 

  # Step 3: Add new detections to the archive 
  #
  @data = &Lib'get_file_entries("$archfile 1"); # older data 
  push(@data,&get_known_detections("$dir"));  # combine in newer stuff 

  # Step 4. Trim the archive down to where the oldest and newest
  #         observations are within the specified $interval.
  @data = &prune_archive(@data);

  # Step 5. Look to see if individual sources have met triggering
  #         criteria (see above for criteria).
  @data = &evaluate_archive(@data);

  # Step 6. We write out the new archive...
  #
  &write_array_to_file(@data);

  # Step 7. Lets deal with the alerts now...
  #
  local(@tmpdata);
  push(@tmpdata,@data);
  push(@tmpdata,"$require_ssc0 $num_det[0] $num_det[1] $num_det[2] $num_det[3] $archfile $interval");
  push(@tmpdata,"$alert_mail_to");
  &Alarm'handle_alerts(@tmpdata);

  # Step 8. Check the asm processing pipeline. Is it ok?
  #         Note that we dont check up on the *client* programs
  #         (which are run on another machine).
  &handle_processing();
 
  # Step 9. Are the new too results ready? Make a noise
  #         and popup window if so ...
  &Lib'handle_new_too();

  # Epilogue -- We sleep, and loop back to the beginning.
  #
  print "\n$0 sleeping $sleep_interval seconds\n" if($verb);
  sleep $sleep_interval;
}


#
# S U B R O U T I N E S
#

# an important chunk of the asm code...
sub evaluate_archive {
  local(@locdata) = @_;
  local(@array) = local(@sf_file) = 0;
  local($sf_name) = local(@sfnum) = local($i) = 0; 
  local($nrofgood) = local($nrofbad) = local($nrofquest) = 0; 
  local($nrof_entries)=0;

  # print "\n Evaluating archive entries...\n" if ($verb);

  &Output'print_arch_header() if($verb);

  foreach $i (0 ... $#locdata) {
    local(@array) = split(' ',$locdata[$i]);

    # If the array lacks the quality element its time to figure it out!.. 
    if($#array < 13) { 

      # print "($locdata[$i]) (size:$#array) lacks the quality element \n";

      $sf_name = "sf$array[2].$array[3].$array[1]";
      $sfnum[0] = int($array[2]/86400);
      $sfnum[1] = int($array[2]/604800) - 108;
      $sfnum[1] = "0" . $sfnum[1] if $sfnum[1] < 100;

      # local($sf_dir) = "wk$sfnum[1]";
      # local($sf_dir2) = "day$sfnum[0]";
      # local($file_eval) = &Lib'find_sffile("$sf_dir $sf_dir2 $sf_name");

      local($file_eval) = &Lib'find_sffile("sfarchive eholding $sf_name");

      local(@file) = split(' ',$file_eval);
      if(!$file[0] && (-e "$file[1].gz")) { system"gunzip -q $file[1].gz"; }

      local(@tmp) = @array;
      push(@tmp,$file[1]); 
      push(@tmp,1);

      # print "TEMPARRAY:"; while (<@tmp>) {print "$_ ";}; print"\n";
   
      $array[13] = &Trigger_Criteria'evaluate_entry("@tmp");
      $locdata[$i] = join(' ',@array);

      # lets not do this for now (cause processing problems?) 
      # if(!$file[0]) { system "gzip $file[1]"; }
    } 

    &Output'print_entry(@array) if ($verb);

    $nrof_entries++;
    if($array[13]==0) { $nrofgood++; }      # secure, added w/ good ones
    elsif($array[13]==1) { $nrofgood++; }   # good
    elsif ($array[13]==2) { $nrofquest++; } # questionable
    elsif ($array[13]==3) { $nrofbad++; }   #  bad
 
  }

  if($verb) {
    &Output'print_arch_legend();
    printf("\nArchive entries: %d total  ",$nrof_entries);
    printf("(%d good, %d iffy, %d bad)\n\n",$nrofgood,$nrofquest,$nrofbad);
  }

  return @locdata;
}

sub handle_processing {

  if($check_processing) {
    print "\nCHECKING PROCESSING...\n" if ($verb);
    # we use the interval as the guage of staleness
    local($test_stale) = &Lib'check_stale_archive("$archfile $interval");
    system "/xte/bin/chk_sof_processing.pl $test_stale";
  }

}

sub get_known_detections {
  local($dir)= @_;
  local($file)= "$dir/known_x_too.report";
  local(@data)=();
 
  if(-e $file) {
    open(FILE, $file);
    @data=<FILE>;
    close(FILE); 
    chop(@data);
    printf(" %d known too entries in %s\n",$#data+1,$dir) if($verb);
  } else { 
    print "$file not found.\n";
  }

  return @data;
}

# writes an array to the file $archfile (a global variable)
sub write_array_to_file {
  local(@locdata) = @_;
  local($i);
  local($tfile)="/tmp/tempfile";
 
  system "touch $tfile; rm -f $tfile";

  foreach $i (0 ... $#locdata ) { system "echo $locdata[$i] >> $tfile"; }

  # system "touch $archfile; rm -f $archfile";
#  system "sort $tfile > /tmp/$archfile";
  system "sort $tfile > /tmp/asm_archive";
# system "mv /tmp/$archfile $archfile";
  system "mv /tmp/asm_archive $archfile";
  system "rm -f $tfile";

}

# In this subroutine we trim the archive down by first weeding out
# any duplicate events, then we trim to where the oldest and newest
# observations are within the specified $interval.
 
sub prune_archive {
  local(@locdata)=@_;
 
  # PRUNE DUPLICATES
  local(@locdata)=&Lib'prune_duplicates(@_);
  printf(" After pruning duplicates there are %d entries\n",$#locdata+1) if($verb);
 
  # PRUNE EXPIRED
  @locdata=&prune_old(@locdata);
  printf(" After pruning expired entries there are %d entries\n",$#locdata+1) if($verb);
 
  return @locdata;
}

# uses the global variable $interval
# we now use the current time to determine an expired entry
# (formerly we just used the last time in the archive)
sub prune_old {
  local(@locdata) = @_;
  local(@newdata);
  # local($end) = &Lib'get_mjd_time();
  local(@temp) = split(' ',$locdata[$#locdata]);
  local($end) = $temp[0];
  local($i);
    
  foreach $i (0 ... $#locdata ) {
    local(@tmp) = split(' ',$locdata[$i]);
    local($time) = $tmp[0];
    if(($end - $time) <= $interval) {
      $newdata[$#newdata+1] = $locdata[$i];
    } else {
      print "  Removing expired entry $tmp[12] ($tmp[0])\n" if ($verb);
    }
  }

  return @newdata;
}


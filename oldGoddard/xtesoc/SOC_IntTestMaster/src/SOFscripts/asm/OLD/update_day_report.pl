#!/usr/local/bin/perl

# program called by watch_asm.pl

# 6/31/98 prior change introduced bugs. -b.t.

# 3/18/1998 Jon V. changed directory structure to match MIT
# 9/20/1998 Bt - added in ParameterManager stuff 

# Version 1.00, updated 2/13/97
# Written by b.t. (thomas@xplorer.gsfc.nasa.gov)

# SUMMARY
# Using data from  the weekly known_x_too.report, we 
# update the known_x_too.report in the specified 
# daily directory.

$verb = 1; # debuging 

use ParameterManager;
my $params = new ParameterManager("asm/ASMdefs.config");

# Program DEF's
my $wkrootdir = $params->get_scalar("root_dir");   
my $wkrootdir = $params->get_scalar("da_file_dir");
                    # location of weekly sf, rs, da files and known_x_too.report
my $dayrootdir = $params->get_scalar("too_dir");

# get week dir
opendir(DIR,$wkrootdir);
push(@week,sort(grep(/^wk\d{3}/, readdir(DIR))));
closedir(DIR);

# PROLOGUE
# Idle process until asm_process_daily.pl finishes its transfer of files
$wait_sec = 2;
while (!($endloop)) {
  if(!(-e "/xte/socops/asm/.asmdailylock")) { $endloop = 1; }
  print "file is locked please wait $wait_sec seconds...\n" if ($verb);
  sleep $wait_sec;
}

# create lockfile
system("touch /xte/socops/asm/.lockknownrep");

# read in weekly known too report
# and build up daily known reports

# First get all the lines from every weeks report into one big array
print "Reading weekly reports.\n" if ($verb);
while (<@week>) { 
  chdir("$wkrootdir/$_");
  open(FILE,"known_x_too.report"); 
  @this_week_too_list = <FILE>;
  close(FILE);
  push(@all_weeks_too_lines, @this_week_too_list);
}
chomp(@all_weeks_too_lines);

# then process the array, putting each entry into the appropriate week
# each entry is put into an has of arrays, with the hash key being
#  the day number;  thus there is one array for each day number;
#  since the array maintains its order (unlike a hash) it does not need
#  to be sorted or anything
foreach $too_line (@all_weeks_too_lines) {
  local(@tmp) = split(' ',$too_line);
  local($time) = int($tmp[2]/86400);
  push( @{ $day_hash{$time} }, $too_line );
}

# for each day present in the $dayroot directory, write in a
# the known_x_too.report file (but if data from those days was found 
# in the weekly reports)
print "Writing specific day reports.\n" if ($verb);

@keys = reverse sort numerically keys %day_hash;

foreach $i (0 ... 2) {
  $day_key = $keys[$i];
  mkdir("$dayrootdir/day$day_key",755) if (!-e "$dayrootdir/day$day_key"); 
  system "chmod 755 $dayrootdir/day$day_key"; # dunno why this can be done right first time.. 
  $filename = "$dayrootdir/day$day_key/known_x_too.report";
  write_lines_to_file ($filename, $day_hash{$day_key} );
  print "Wrote file $filename\n" if ($verb);
}

system("rm -f /xte/socops/asm/.lockknownrep");


sub write_lines_to_file {
  my($file, $lines) = @_;  # $lines is a ref to an array
  open(OUTFILE,">$file") || die "Can't open $file: $!";
  foreach (@{ $lines }) {
    print OUTFILE "$_\n";
  }
  close(OUTFILE) || die "Can't close $file: $!";
}

sub numerically { $a <=> $b }



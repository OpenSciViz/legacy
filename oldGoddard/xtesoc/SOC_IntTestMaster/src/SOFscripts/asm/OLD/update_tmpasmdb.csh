#! /bin/csh -f

# 3/18/1998 Jon V. changed directory structure for MIT setup
# added new directory for asm database

unset noclobber
set clobber

set dir = "/home/socops"

if( $2 ) then
  set forceupdate = $2
else
  set forceupdate = 0
endif

set verb = $1
set lockfile = "/home/socops/.locktempasmdb"

if(-e $lockfile) then
  echo "$0 exiting ... $lockfile found (another tmporary update in progress?)"
  exit
endif

if($verb) then
  echo $0 running...adding lockfile
endif
touch $lockfile

# do we have any reason to update?
set test1 = `ls /xte/socops/asm/ftp | grep sf | wc -l`
set test2 = `ls /xte/socops/asm/analysis | grep sf | wc -l` 
if( ($test2 == 0) && ($test1 == 0) ) then 
  if( $verb ) then
    echo No SF files found, removing $lockfile and exiting
  endif
  rm -f $lockfile
  exit 0
endif

# remove all asmtemp files in /tmp ? 
set test = `ls /tmp | grep asmtemp | wc -l`
if ( $test != 0 ) then
  rm -f /tmp/asmtemp*
endif

#build list of sf files to use
# from both analysis and FTP directories
if( $verb ) then
  echo building list of files 
endif
if( $test1 ) then
  set list1 = `ls /xte/socops/asm/ftp/sf*.*.*`
else
  set list1 = ""
endif
if( $test2 ) then
  set list2 = `ls /xte/socops/asm/analysis/sf*.*.*`
else
  set list2 = ""
endif

set list = `echo $list1 $list2`

# Main program
# Clobber old temp files with these new data...
if( $verb ) then
  echo building sum database
endif
cat $list | /xte/bin/rdct_asm3 | grep -v Seq | /xte/bin/add_qlty.pl  > /tmp/asmtemp.db

if($verb) then
  echo building ch A database
endif
cat $list | /xte/bin/rdct_asm3 -band a | grep -v Seq > /tmp/asmtemp_chA.db

if($verb) then
  echo building ch B database
endif
cat $list | /xte/bin/rdct_asm3 -band b | grep -v Seq > /tmp/asmtemp_chB.db

if($verb) then
  echo building ch C database
endif
cat $list | /xte/bin/rdct_asm3 -band c | grep -v Seq  > /tmp/asmtemp_chC.db

# Clean up
# remove old files
# if($verb) then
#  echo removing old tempfiles
# endif
# rm -f $dir/asmtemp*.gz

if($verb) then
  echo compressing
endif
/usr/local/bin/gzip -q /tmp/asmtemp*.db

if($verb) then
  echo Moving to $dir
endif
mv /tmp/asmtemp*.db.gz $dir

# Free up for next process. rm lockfile
if($verb) then
  echo removing lockfile
endif
rm -f $lockfile

# Finished
if($verb) then
  echo $0 finished
endif

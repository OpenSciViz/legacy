
# This package hold general purpose output (print) subroutines 

package Output;

sub print_entry {
  local(@dat)=@_;
  local($status) = $dat[13];
  if($status > 4 || $status < 0) { $status= "ERROR! ($status)"; }
  # else {$status=$lname[$dat[13]]}
  else {$status=&Quality'name($dat[13])}
  printf("%s %s   %s   %s %6.2f  %2d  %6.2f %7.2f %6.2f  %s %6.2f  %5d %9s   %s\n",
      $dat[0],$dat[1],$dat[2],$dat[3],$dat[4],$dat[5],$dat[6],
      $dat[7],$dat[8],$dat[9],$dat[10],$dat[11],$dat[12],$status);
}
 
sub print_arch_header {
  printf("\nCURRENT ARCHIVE: $archfile (%s -- %8.3f)\n",&Lib'get_est_time,&Lib'get_mjd_time);
  print "TIME (MJD)  SSC# DWELLSEQ#  DWL# ChiSq #SRC Eangle  ";
  print "Rate    Err   Integ   Trig   Prop# Src name   Quality\n";
  print "----------- ---- ---------  ---- ----- ---- ------  ";
  print "------  -----  -----  ------ ----- ---------  ---------\n";
}
 
sub print_arch_legend {
  print "---------\n";
  print " TIME\t observed time  \t\t DWL#\t dwell number  \t\t ChiSq\t reduced chisq\n";
  print " #SRC\t # of src in FOV\t\t Eangle\t earth angle \t\t Rate\t ASM countrate\n";
  print " Err\t error on rate   \t\t Integ\t dwl time (sec)\t\t Trig\t trigger level\n";
  # print " Prop#\t proposal number\n";
}

1;

__END__


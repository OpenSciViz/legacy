#!/usr/local/bin/perl

# Version 1.01, updated 7/15/97  
# Written by b.t. (thomas@xplorer.gsfc.nasa.gov)
# Modified 3/18/1998 Jon V. changed dir names to reflect MIT structure

# SUMMARY
# We call update_1day_asmflux.pl for all good sf files 
# so that the sources can be added to the 24 hr rolling 
# flux catalogue. 

# Program DEF's
$dir = '/xte/socops/asm'; 
$Archfile = "$dir/1day_archive";
$datadir = "$dir/analysis";
$sf_list = "$datadir/all_sf.ls";
$verb = 0;
$counter = 0;
$band = 0;  # 0 = sum, 1 = chA, 2 = chB , 3 = chC 
$use_data = 0;
$debug = 0;
$ignore_ssc = -1;    # we ignore any dwells from this ssc 
$ignore_bad_quality = 1; # if true, throw away all bad entries 
$ignore_iffy_quality = 0; # throw away all iffy entries 
$interval = 1.0; # length (in days) to store in archive

require "/xte/lib/perl/Lib";
require "/xte/lib/perl/Trigger_Criteria";
require "/xte/lib/perl/Quality";

# argv loop 
foreach $i (0 .. $#ARGV) {
# print $ARGV[$i], "\n"; 
  if($ARGV[$i] =~ "-A") { $Archfile = $ARGV[++$i]; } 
  if($ARGV[$i] =~ "-d") { $datadir = $ARGV[++$i]; }
  if($ARGV[$i] =~ "-h") { $help = 1; } 
  if($ARGV[$i] =~ "-s") { $sf_list = $ARGV[++$i]; } 
  if($ARGV[$i] =~ "-t") { $dir = $ARGV[++$i]; }
  if($ARGV[$i] =~ "-v") { $verb = $ARGV[++$i]; } 
} 
 
# some help on options 
if($help) {
  print "\n$0 options:\n" ; 
  print "\n -A\t Full path to filename of 24hr archive\n\t (default: $Archfile).\n"; 
  print "\n -d\t Full path to location of sf files.\n\t (default: $datadir).\n"; 
  print "\n -h\t This help message. \n"; 
  print "\n -s\t filename of \"current\" sffiles list\n\t (default: $sf_list).\n"; 
  print "\n -v\t Verbose mode. (default: $verb)\n\n"; 
  exit 0; 
}

if($verb) {
  print "$O topdir: $dir\n archfile:$Archfile\n datadir:$datadir\n current_list:$sf_list\n";
  print " band: $band\n";
}

# P R O G R A M  B E G I N

$time = Lib'get_mjd_time();
chdir($dir); 

# We only do stuff if there are files to process
if(&load_sffile_list) {

&load_1d_archive();

&add_sffiles_to_archive();

&prune_1d_archive();

&create_1d_archive();

} 


print "$0 exiting.\n" if($verb); 
exit 0;

# S U B R O U T I N E S

sub load_sffile_list {
  if(!-e($sf_list)) { die "$0 can't find a current list of sf files!"; }
  open(FILE,$sf_list);
  local($i) = 0; 
  while(<FILE>) { chop; $sffile[$i] =$_; $i++;}; 
  close(FILE);
}

sub load_1d_archive {
  open(FILE, "$Archfile") && (@data=<FILE>) && close(FILE) && chop(@data) ||
    print "1day archive ($Archfile) is missing or empty!\n" if ($verb);
}

sub prune_1d_archive {
  &prune_1d_expired();
  &prune_1d_duplicate();
}

# check for duplicate entries, set DELETE status on one if
# that entry exists in more than one place.
sub prune_1d_duplicate {
  local($ind1)=local($ind2)=0;
  print "pruning duplicated sources from $Archfile..." if($verb); 
  while($ind1<$#data+1) {
    while($ind2<$#data+1) {
      if($ind1!=$ind2) {
        if($data[$ind1] eq $data[$ind2]) {
          # print "\nREMOVING DUPLICATE ENTRY:\n $data[$ind2]\n" if ($verb);
          $data[$ind2]=&set_delete_status($data[$ind2]);
        } 
      }
      $ind2++;
    }
    $ind2=0;
    $ind1++;
  }
  print "finished.\n" if($verb); 
}


# remove all entries that are older than the limit set
# by the variable $interval

sub prune_1d_expired {
  local($ind)=0;
  print "pruning expired sources from $Archfile..." if($verb); 
  while($ind<$#data+1) {
    local(@test)=split(' ',$data[$ind]);
    if($test[0]+$interval<$time) { 
      $data[$ind]=&set_delete_status($data[$ind]); 
    }  
    $ind++;
  }
  print "finished.\n" if($verb); 
}


# set the DELETE status on the data entry
sub set_delete_status {
  local($tmp) = @_;
  local($value) = &get_base_entry($tmp) . " DELETE";
  return $value;
}


# return the data line sans the quality marker
sub get_base_entry {
  local($tmp) = @_;
  local(@line) = split(' ',$tmp);
  local($value) = "$line[0]";
  local($index)=1;
  while ($index<$#line) {
    $value = $value . " " . $line[$index]; $index++;
  } 
  return $value;
}

# Add new source values found in the sf files
# to the archive list.

sub add_sffiles_to_archive {
  local($ind) = 0;
  print "adding new sources($#sffile) to $Archfile..." if($verb);
  while ($ind <= $#sffile) {
    local(@entry) = &get_sf_values("$sffile[$ind]");
    if($entry) {
	    print "entry: $entry\n";
      push(@data,@entry);
    }
    $ind++;
  }  
  print "finished.\n" if($verb);
}


# get new entry(s) from the indicated solutions file. If they
# are of a quality we want to ignore, then determine that in here.

sub get_sf_values { 
  local($file)=@_;
  # print "\n doing: $file\n" if ($verb);
  local($toggle)=local($stop)=local($start)=local($ftime)=0;
  local($bad_entry)=local($ssc)=local($chi)=local($dw)=0;
  local($num)=local($dwseq)=local($index)=local($new_arr)=0; 
  local($rate)=local($err)=local($exp)=local($theta)=0; 
  local($phi)=local($name)=local($tran)=local($ea)=0; 
  local(@tarr)=0;
  open(FILE, "$datadir/$file");
  while (<FILE>) {
      if(/TSTART/) { $start = &get_last_val($_); } 
      if(/TSTOP/) { $stop = &get_last_val($_); $ftime=$stop+($start-$stop)/2;}
      if(/N_SSC/) { $ssc = &get_last_val($_) - 1; } 
      if(/SRCS_SSC/) { $num = &get_last_val($_); } 
      if(/SEQUENCE/) { $dwseq = &get_last_val($_); $dw = &get_sec_val($_); } 

      if (/RDCHI_SQR/) { $chi = &get_last_val($_); }
      elsif(/OBJECT/) { $name = &get_last_val($_); $bad_entry=1;}
      elsif(/FOV_THETA/) { $theta = &get_last_val($_); } 
      elsif(/FOV_PHI/) { $phi = &get_last_val($_); } 
      elsif(/COUNT_RATE/) { $rate = &get_last_val($_); } 
      elsif(/ERROR/) { $err = &get_last_val($_); } 
      elsif(/EXPOSURE/) { $exp = &get_last_val($_); } 
      elsif(/TRANSMISSION/) { $tran = &get_last_val($_); } 
      elsif(/EARTHANG/) { 
        $ea = &get_last_val($_);
        if(!(($band + $counter) % 4)) { $use_data = 1; } else { $use_data=0; }
        $counter++;
        # print "name=$name band=$band cntr=$counter use=$use_data\n";
      } 

      if($use_data) { 
        # now, build a new archive entry. Dont use particle backgrnd 
        # or x-ray bckgnd entries 
		# print"using data:$datadir/$file ($name:$badentry)\n";
        # if($bad_entry!=1 && $name ne "prtclbg" && $name ne "xbg" ) { 
        if($name ne "prtclbg" && $name ne "xbg" ) { 
          local($dfile) = "$datadir/$file";
          # mit format
          local($line)="$start $ssc $dwseq $dw $chi $num $ea $rate $err $exp";
          local($line2)= "$theta $phi";
          local($quality) = &Trigger_Criteria'evaluate_entry("$line -1 -1 $name $dfile 0");
          local($stud) = "$line $line2 $name $quality";
				# print "STUD: $stud\n";
          $new_arr[$index] = $stud;
          $index++;
        }
        $bad_entry=0;
        $use_data=0;
      } 
  }
  close(FILE);

  # lastly, points get rated on how close they lie to others...
  # local($fov_status) = &Trigger_Criteria'check_fov("$name $file");
  # if($level != 3 && ($fov_status>$level)) { $level = $fov_status; }

  local($dd)=0; 
  while ($dd<$#new_arr+1) { 
    print " - $new_arr[$dd]\n" if ($verb); $dd++;
  }

  # returning only the best entries 
  return @new_arr;
}


# create the new archive, write to disk
sub create_1d_archive { 
  local($ind)=0; 
  print "creating new file $Archfile..." if($verb);
  system("rm -f $Archfile"); 
  system("touch $Archfile"); 
  # print "number of data:$#data\n";
  while ($ind<$#data+1) { 
    # print "entry: $data[$ind]: ";
    if(&keep_entry($data[$ind])) {
      system("echo \"$data[$ind]\" >> $Archfile");
			# print "PUT IN ARCHIVE";
    } else {
			# print "OMITTED FROM ARCHIVE";
    } 
    # print "\n";
    $ind++;
  }
  print "finished.\n" if($verb);
}

sub get_sec_val {
  local(@line) = split(' ',$_);
  return $line[1];
}

sub get_last_val {
  local($tmp)=@_;
  local(@line) = split(' ',$tmp);
  return $line[$#line];
}

# evaluate the data entry based on its quality flag
# and program parameters. IF we want to KEEP the line
# (in the new archive) we return a value of TRUE (1).
sub keep_entry {
  local($tmp)=@_;
  local($status) = &get_last_val("$tmp");
  if($status eq "DELETE") { return 0; }
  if($ignore_bad_quality && $status eq "BAD") { return 0; }
  if($ignore_iffy_quality && $status eq "IFFY") { return 0; }
  return 1;
}


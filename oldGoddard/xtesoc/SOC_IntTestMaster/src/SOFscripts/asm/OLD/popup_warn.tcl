#! /usr/local/bin/wish -f

# set line $argv

wm title . "ASM WARNING!!!"
message .msg -background yellow -width 38c -justify left -relief raised -bd 2 \
	-font -Adobe-Helvetica-Medium-R-Normal--*-180-* \
 	-text $argv
#	-text "  There have been 1 or more ASM processing\n\
#      problems. Check the socops mail!"
pack .msg
button .b -background red -text "    press me to exit    " \
	-font -Adobe-Helvetica-Medium-R-Normal--*-180-* \
	-command exit
pack .b

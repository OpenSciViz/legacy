#! /usr/local/bin/wish -f

set normfont "-Adobe-Helvetica-Medium-R-Normal--*-140-*"
set darkfont "-Adobe-Helvetica-Medium-R-Normal--*-180-*"
set bigfont "-Adobe-Helvetica-Medium-R-Normal--*-240-*"
set white #eeeeee
set black #000000
set red #ff4444
global white black

wm title . "ASM ALERT"

# --------frames for our wondrous tool -------
frame .top -relief raised -bd 2
frame .txt -relief raised -bd 2
frame .bottom -relief raised -bd 2
pack .top .txt .bottom
 
# some procs we'll use
proc doTxtScrollWidget { frame name font width height fg bg } {

  scrollbar $frame.yscroll -command "$frame$name yview"
  pack $frame.yscroll -side right -fill y
  text $frame$name -relief raised -fg $fg -bg $bg -bd 1 -width $width \
     -font $font -height $height -yscrollcommand "$frame.yscroll set" 
}

proc smallScrollTxt { frame width size font txt } {
   global white black

   if [winfo exists $frame.txt ] { destroy $frame.txt }
   if [winfo exists $frame.yscroll ] { destroy $frame.yscroll }

   doTxtScrollWidget $frame .txt $font $width $size $white $black
   pack $frame.txt
   $frame.txt insert end $txt

}

message .msg -background $red -width 38c -justify left -relief raised -bd 2 \
	-font $bigfont -text "TOO ALARM!"
pack .msg -in .top
 
smallScrollTxt .txt 110 25 $normfont $argv

button .b -background yellow -text "     press me to exit    " \
	-font -Adobe-Helvetica-Medium-R-Normal--*-180-* \
	-command exit

pack .b -in .bottom



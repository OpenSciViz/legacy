#! /usr/local/bin/perl -I/xte/lib/perl 

# A simple tool to get output of the current archive.
# used by other programs like watch_asm.tcl for example.

# bt. thomas@xplorer.gsfc.nasa.gov

use Lib; 
use Quality; 
use Output; 

$archfile = "/xte/socops/asm/asm_archive";

vec($quality,0,2) = 0;

# RUN-TIME INITIALIZATION
 
# argv loop
foreach $i (0 .. $#ARGV) {
# print $ARGV[$i], "\n";
  if($ARGV[$i] =~ "-a") { $no_end = 1; $no_header = 1; }
  if($ARGV[$i] =~ "-e") { $no_archive = 1; $no_header = 1; }
  if($ARGV[$i] =~ "-h") { $no_archive = 1; $no_end = 1; }
  if($ARGV[$i] =~ "-i") { 
    $ignore_quality=1;
    while (!($ARGV[++$i] =~ "-")) { vec($quality,$ARGV[$i],2) = 1; }
  }
}

# print "Got quality bitflag:",&printvec($quality),"\n" ;

# PROGRAM BEGINS 

if (!$no_header ) { &Output'print_arch_header(); }

if (!$no_archive) { &print_archive(); }

if ( !$no_end ) { &Output'print_arch_legend(); } 


# S U B R O U T I N E S 

sub print_archive {
  local($i);

  if(-e $archfile) {
    local(@data) = Lib'get_file_entries("$archfile 0");

    foreach $i ( 0 .. $#data) {
      local(@array) = split (' ', $data[$i]);
      if(!($ignore_quality)||!vec($quality,$array[13],2)) {
        &Output'print_entry(@array);
      }
    }
  }
}

sub printvec {
  local($v) = @_;
  local($i, $result);
 
  for ($i = (4*length($v)) - 1; $i >= 0; $i--) {
    $result .= (vec($v, $i, 2)) ? "1" : "0";
  }
  return $result;
}


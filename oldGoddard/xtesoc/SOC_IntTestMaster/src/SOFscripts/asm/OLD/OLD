#! /usr/local/bin/blt_wish -f

#our (many) global variables, ugh!
global file
global nvect ra dec
global setLimits limitTagName ux uy lx ly
global psfile printpsnow
global bright_cat bra bdec
global on_cat sra sdec
global markerTag
global onStarTags brightStarTags
global starplottoggle

# program defs
set toolname "New-TOO Sky Map Tool"
set version "v.0.4"
set versdate "11-14-97"

#starting default params and values
set setLimits 0
set markerTag ""
set nvect 0
set starplottoggle 0
set printpsnow 0
set psfile dump.ps
set file /home/socops/thomas/scripts/tcl/sm_commands
set bright_cat /asm/data/too/new/asm_bright
set on_cat /asm/data/too/new/asmcat_on

#color defs
set yellow #993
set pink #e88
set green #6c3
set blue #178
set red #c12
set grey #bbb
set white #eee
set orange #c84
set black #111

# Help message buffers
set aboutmsg "
 $toolname Version $version Date: $versdate
 Coded by Brian Thomas (thomas@xplorer.gsfc.nasa.gov)
 --------------  

   $toolname is a GUI tcl/tk program to help with 
 the analysis of ASM new TOO skymap files. 

"
set bugmsg "
  Known Bug List:
	- load time is severly slow.
	- label display for plot inconsistent
"
set futuremsg "
   What's on the \"stack\" of new features?
	- fix bugs!
  - improve speed
	- allow time selection of dwells
"

wm title . "SOF $toolname $version"
blt_debug 0

proc loadAll { name } {
  global bright_cat bra bdec
  global on_cat sra sdec
  
  # isnt there a more clever way to do this?
  if { [ filecheck $bright_cat ] } {
    set data [ exec cat $bright_cat ]
    set end [llength $data]
    set i 0
    while { $i < $end } {
      lappend bra [ lindex $data [expr $i + 1 ] ]
      lappend bdec [ lindex $data [expr $i + 2 ] ]
      incr i 14
    }
  }

  if { [ filecheck $on_cat ] } {
    set data [ exec cat $on_cat ]
    set end [llength $data]
    set i 0
    while { $i < $end } {
      lappend sra [ lindex $data [expr $i + 1 ] ]
      lappend sdec [ lindex $data [expr $i + 2 ] ]
      incr i 4
    }
  }

  loadData $name

  # clear the plot canvas
  # plotData $name

}

proc loadData { name } {
  global file

  if { [ filecheck $file ] } {

    getSkyData $file
    # newGetSkyData $file

    set msg " ASM SkyMap Dwells: ?? to ?? "
    $name config -title "File: $file $msg" 
  } 
}

proc filecheck { file } {

  set name [file rootname $file] 

  if { ! [ file exists $file ] } { 
    tmpInfoDisplay "ERROR: $file doesn't exist, perhaps incorrect path?"
    return 0
  } elseif {! [file readable $file ] } { 
    tmpInfoDisplay "ERROR: $name is unreadable by user."
    return 0
  } elseif {! [file size $file ] } { 
    tmpInfoDisplay "ERROR: $name contains no data."
    return 0
  } elseif {! [file isfile $file ] } { 
    tmpInfoDisplay "ERROR: $name is not an ordinary file."
    return 0
  }
  return 1
}

proc clearPlot { name } {
  delAllDataElem $name
  deleteAllTags $name
  if {[$name element show marker] == "marker"} { 
    $name element delete marker 
  } 
}

proc delAllDataElem { name } {
  global nvect
  if {$nvect == "0"} {return} 
  set i 0
  while {$i < $nvect } {
    set dname "data$i"
    if {[$name element show $dname] == $dname} { 
      $name element delete $dname 
    }
    incr i
  }
}

proc newGetSkyData { file } {
  global nvect ra dec

  set Prog "/asm/bin/asmSmToVect"
	set i 0

	catch "exec cat $file | grep relocate | wc -l " nvect
	puts "newGetSkyData got $nvect vectors in data"

  while {$i < $nvect} {
		catch "exec $Prog -ra -n $i <$file" ra($i)
		catch "exec $Prog -dec -n $i <$file" dec($i)
		puts "ra($i): $ra($i)" 
		puts "dec($i): $dec($i)" 
		incr i
	}
}

proc getSkyData { file } {
  global nvect ra dec

  set dataset [exec cat $file ]
  set nvect -1

  set i 0
  set end [llength $dataset]

  # sm header is useless, lets get to the sky data
  while { [lindex $dataset $i] != "relocate" } { incr i }

  # parse file sky data to our sky variables
  while {$i < $end} {
    set new [lindex $dataset $i]
    if { $new == "relocate" } { 
      incr nvect
      # reset sky global varibles
      set ra($nvect) ""
      set dec($nvect) ""
      lappend ra($nvect) [lindex $dataset [expr $i + 1 ] ]
      lappend dec($nvect) [lindex $dataset [ expr $i + 2 ] ]
    } elseif {$new == "draw" } {
      lappend ra($nvect) [lindex $dataset [expr $i + 1 ] ]
      lappend dec($nvect) [lindex $dataset [ expr $i + 2 ] ]
    } 
    incr i 3
  }
  # we dont need this, there is a spare "relocate" line at end of Ron's file
  # incr nvect
}

proc startLimitSet { name wx wy } { 
  global setLimits limitTagName lx ly yellow
  if {$setLimits == "0" } {
      set setLimits 1
      set lowerDataLimits [winToGraphCoords $name $wx $wy dummy ] 
      set lx [lindex $lowerDataLimits 0]
      set ly [lindex $lowerDataLimits 1]
      grab set -global $name
      set limitTagName [$name tag create line {0 0 0 0} -fg $yellow -bg black] 
      $name config -title "Double Click to Set Data Limits"
  }
}

proc drawLimits { name wx wy } {
  global setLimits limitTagName ux uy lx ly
  if { $setLimits == "1" } {
      set upperDataLimits [winToGraphCoords $name $wx $wy dummy ] 
      set ux [lindex $upperDataLimits 0]
      set uy [lindex $upperDataLimits 1]
      set coords { $lx $ly $lx $uy $ux $uy $ux $ly $lx $ly }
      $name tag coords $limitTagName $coords
  }
}

proc stopLimitSet { name wx wy } { 
  global setLimits limitTagName ux uy lx ly
  global binsize indextime mode indexbin
  global file

  if { $setLimits == "1" } { 
      grab release $name
      $name tag delete $limitTagName
      # rescalePlot using config axis
      if { $ux > $lx } { 
        $name xaxis config -min $lx -max $ux
      } else {
        $name xaxis config -min $ux -max $lx
      }
      if { $uy > $ly } { 
        $name yaxis config -min $ly -max $uy
      } else {
        $name yaxis config -min $uy -max $ly
      }
  }
  set setLimits 0
  set msg " Need Label Here!"
  $name config -title "File: $file $msg" 
}

proc plotStars { name } {
  global bright_cat bra bdec
  global on_cat sra sdec
  global onStarTags brightStarTags
  global orange pink
  global starplottoggle
  
  deleteAllTags $name

  if {$starplottoggle == "1" } { return }

  if { [ filecheck $bright_cat ] && $starplottoggle != "2" } {
    set i 0
    set brightStarTags ""
    set end [llength $bra]

    while {$i < $end } { 
      set xval [lindex $bra $i]
      set yval [lindex $bdec $i]
      lappend brightStarTags [$name tag create text {$xval $yval} -fg $pink -bg black -text "O"]
      incr i
    }
  }

  if { [ filecheck $on_cat ] && $starplottoggle != "3" } {
    set i 0
    set end [llength $sra]
    set onStarTags ""
    while {$i < $end } { 
      set xval [lindex $sra $i]
      set yval [lindex $sdec $i]
      lappend onStarTags [$name tag create text {$xval $yval} -fg $orange -bg black -text "*" ]
      incr i
    }
  }

}

proc plotData { name } {
  global file nvect ra dec

  if [file exists $file] {
    set i 0
    delAllDataElem $name
		puts "Plotdata got $nvect"
    while {$i < $nvect } { 
      set dname "data$i"
      set yval $dec($i)
      set xval $ra($i)
		  puts "xval: $xval"
		  puts "yval: $yval"
      $name element create "$dname" -fg white -bg black -x $xval -y $yval
      incr i
    }
  }
}

proc deleteAllTags { name } {
  set show [ $name tag ids ]
  if { $show != "" } {     
    set string [ split $show ]
    set end [llength $string]
    set i 0
    while {$i < $end} {
      $name tag delete [lindex $string $i]
      incr i
    }
  }  
}

# wheres the point we want to check?
proc plotMarker { name xm ym } {
  global markerTag red

  if { $markerTag != "" } { $name tag delete $markerTag }

  # plotData $name

  set markerTag [$name tag create text {$xm $ym} -fg $red -bg black -text "+"]
}

proc winToGraphCoords { name winx winy element } {
  return [ $name invtransform $winx $winy ]
}

proc doPointer { name winx winy } {
  global file mode binsize
  global setLimits

  if { $setLimits == "0" } { 
    set plotcord [ winToGraphCoords $name $winx $winy data0 ]
    set plotx [lindex $plotcord 0]
    set ploty [lindex $plotcord 1]
    if { $plotcord != "" } { 
      plotMarker .g $plotx $ploty 
      # set lastx $plotx
      # set lasty $ploty
      $name config -title "File:$file  MARKER RA:$plotx Dec:$ploty"
    }
  }
}

proc setDefPlotScale { name } {
  $name yaxis config -min "-90" -max "90"
  $name xaxis config -min "0" -max "360"
  plotData $name
}

# a borrowed proc from asmdbtool
proc tmpInfoDisplay {text} {
  global red white black toolname
  set name .tmpDisply
  toplevel $name -class Dialog
  wm title $name "$toolname MESSAGE"
  wm transient $name .
  frame $name.top -relief raised -bd 1
  frame $name.bot -relief raised -bd 1
  pack $name.top -side top -fill both
  pack $name.bot -side bottom -fill both
     
  message $name.msg -fg $black -bg $white -width 6i -text $text
  pack $name.msg -in $name.top
     
  button $name.exit -bg $red -text OK -command {destroy .tmpDisply}
  pack $name.exit -in $name.bot
     
  grab set .tmpDisply
  tkwait window .tmpDisply
}    

proc dumpPsfile { name } {
  global psfile printpsnow
  $name postscript $psfile -pagex 1.5i -pagey 0.25i \
    -pagewidth 6i -pageheight 10.5i -landscape 1
  if { $printpsnow } { exec lp ./$psfile } 
}

proc doSTB { name } {
  global starplottoggle

  incr starplottoggle

  if {$starplottoggle == "1" } {
    .toggleStars config -text {Plot On Cat Stars}
  } elseif {$starplottoggle == "2" } {
    .toggleStars config -text {Plot Bright Stars}
  } elseif {$starplottoggle == "3" } {
    .toggleStars config -text {  Plot All Stars }
  } else {
    set starplottoggle 0
    .toggleStars config -text { Dont Plot Stars }
  }
  plotStars $name
}

proc tagPos {name wx wy } {
  global yellow 
  set plotcords [winToGraphCoords $name $wx $wy data0 ]
  set px [expr int([lindex $plotcords 0])] 
  set py [lindex $plotcords 1]
  set xp $px

  $name tag create text {$px $py} -fg $yellow -bg black -text "+"
  $name tag create text {$px $py} -fg $yellow -bg black -text "($xp,$py)" \
    -xoffset 50
}

# Frames ...
frame .mbar -relief raised -bd 2
frame .top -relief raised -bd 1
frame .top.right -bd 1
frame .top.mid -bd 1
frame .top.left -bd 1
frame .top.left.up -bd 1
frame .top.left.down -bd 1
frame .bottom -relief raised -bd 1

# Widgets ...

blt_graph .g -plotbackground black -height 15c -width 21c
.g legend config -mapped false 
.g xaxis config -min "0" -max "360" -title "R.A. (deg)"
.g yaxis config -min "-90" -max "90" -title "Dec. (deg)"

button .toggleStars -fg $grey -bg $blue -text { Dont Plot Stars } \
   -command { doSTB .g }
button .rescale -fg $grey -bg $blue -text { Rescale Plot } \
  -command { setDefPlotScale .g }
button .load -fg $black -bg $green -text {Load ASM SM Errorbox File:} \
  -command { 
     loadData .g
     plotData .g
   }
button .psprint -fg $black -bg $pink -text {     Print PostScript Plot :      } \
  -command { dumpPsfile .g }
button .quit -fg $black -bg $red -text Quit -command exit
entry .dfileentry -width 40 -relief sunken -textvariable file
entry .psfileentry -width 40 -relief sunken -textvariable psfile

# widgets for the Menu bar ...
menubutton .mbar.options -text Options -menu .mbar.options.menu
 menu .mbar.options.menu
 .mbar.options.menu add checkbutton -label "Send PS File to Printer" \
     -variable printpsnow

menubutton .mbar.help -text Help -menu .mbar.help.menu
 menu .mbar.help.menu
 .mbar.help.menu add command -label "About This Program" \
   -command "tmpInfoDisplay {$aboutmsg}"
 .mbar.help.menu add command -label "Future Features?" \
   -command "tmpInfoDisplay {$futuremsg}"
 .mbar.help.menu add command -label "Known Bugs/Buglets" \
   -command "tmpInfoDisplay {$bugmsg}"

tk_menuBar .mbar .mbar.help .mbar.options
focus .mbar

# Bindings ... 
bind .g <Button-1> { doPointer .g %x %y }
bind .g <Double-1> { printPos }
bind .g <Button-2> { tagPos .g %x %y }
bind .g <Button-3> { startLimitSet .g %x %y }
bind .g <Double-3> { stopLimitSet .g %x %y }
bind .g <Motion> { drawLimits .g %x %y }

# Pack it up ... 
# menu
pack .mbar -side top -fill x
pack .mbar.options -side left
pack .mbar.help -side right
# main window stuff
pack .top -side top 
pack .top.left -side left
pack .top.left.up -side top
pack .top.left.down -side top
pack .top.mid -side left
pack .top.right -side left
pack .bottom -side bottom
# window widgets
pack .load .dfileentry -side left -in .top.left.up
pack .psprint .psfileentry -side left -in .top.left.down
pack .toggleStars .rescale -side left -in .top.mid
pack .quit -side left -padx 5 -in .top.right
pack .g -in .bottom -padx 5 -pady 5

# Now, lets load and plot our first file
loadAll .g
plotStars .g
plotData .g


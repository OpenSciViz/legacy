#!/bin/sh

# ===========================================================================
# Filename    : ebxpacman
# Subsystem   : SOF Scripts - ASM
# Programmer  : Brian Thomas
# Description : A generic wrapper program to use gpacman
#               with an ebx file handler (the ebx_check.pl program).
# Modules     : Tk.pm, WindowTools.pm
#
# RCS: $Id: ebxpacman,v 1.4 1999/02/08 18:21:46 socops Exp socops $
# ===========================================================================
 
# -b.t. (thomas@xplorer.gsfc.nasa.gov)

# Editable params
dir="/xte/socops/asm/too"
indir="$dir/eholding" 
outdir="$dir/errbox" 

# DONT edit below here unless you are knowledgeable
# enuff to do so, ok?

filepre="ebx"
prog="/sochome/bin/SunOS5/ebx_check.go" # wrapper script for ebx_check.pl 
nuke_what="ebx_check"

/bin/rm -f $indir/core 2>> /dev/null 
/bin/rm -f $indir/locked.*$filepre  2>> /dev/null 
/bin/rm -f $indir/$filepre\*.input  2>> /dev/null 
 
gmanargs="-x $prog -F $filepre -p ebx -d -S $indir -O $outdir"

cmd="/xte/bin/gpacman $gmanargs"

sh -c "/usr/local/bin/nuke $nuke_what" 2>> /dev/null 
sh -c "$cmd &" 2>> /dev/null 

 

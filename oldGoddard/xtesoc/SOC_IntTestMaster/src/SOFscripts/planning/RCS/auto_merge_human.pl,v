head	1.4;
access;
symbols;
locks
	bob:1.4; strict;
comment	@# @;


1.4
date	2002.02.05.22.09.38;	author bob;	state Exp;
branches;
next	1.3;

1.3
date	2002.02.05.22.07.57;	author bob;	state Exp;
branches;
next	1.2;

1.2
date	2002.02.05.22.05.05;	author bob;	state Exp;
branches;
next	1.1;

1.1
date	2002.02.05.21.59.47;	author bob;	state Exp;
branches;
next	;


desc
@This script was written by Toshi Takeshima to automatically run the
merge_human.pl script, which updates human readables with insert
fragments.  This is the configuration as it was run in the SOF.
@


1.4
log
@now with working rcs id?
@
text
@#!/usr/local/bin/perl
#
############################################################################
#  This file controls automatic runs of merge_human.pl
#  It will run merge_human when:
#     in manual or loop forever modes:
#     1) On startup, runs for all $nweek weeks starting with the current 
#          week number and going backward nweek weeks in time.
#     in forever loop mode for any of the nweek weeks: 
#     2) When a new insert file appears.
#     3) When an existing insert file changes.
#     4) When a new human readable appears.
#
#   It works by running merge_human.pl and copying the files over to the 
#  correct $SOCHOME/sanction/ subdiectory.
#    
############################################################################
my $rcsid = "$Id: $ ";  
############################################################################
# Check the updating of XTE Human Readable file
# and if updated merge fragmet file to it.
# 97/11/10 Revised from ~takeshim/bin/chksts.pl by T.Takeshima 
# 98/03/19 take care the case of no insertfile 
# 02/02/05 Big revision to allow testing and manual running. 
#          rewrote file_age_hour to use perl -M test.  
#          Headed in the direction of "use strict" but not yet compliant.

my $sochome = $ENV{"SOCHOME"};
unless (defined($sochome)) {$sochome="/sochome"; $ENV{"SOCHOME"}="$sochome";}


my $home       = "/home/planner";     # directory where temporary files are 
my $sanction   = "$sochome/sanction"; # directory where human & fragments are
my $sleeptime  = 1800;                # no. of seconds to sleep between loops
my $nweek      = 2;                   # number of weeks to check for updates
my $perl       = "/usr/local/bin/perl"; # where perl lives
my $mailto     = "xteweb\@@olegacy.gsfc.nasa.gov,bob\@@milkyway.gsfc.nasa.gov";
                                      # addresses where reports go
my $loop       = 1;                   # loop variable
my $once       = 1;                   # defaults to run only one cycle
my $debug      = 0;                   # defaults to silent mode

my $mailflag      = "on";             # whether to e-mail results or not
#my $mailflag      = "off";



# 
my @@usage = ("\nusage: $0 [-home <homedir>] [-sleep <seconds>] [-mail email]\n",
             "        [-nweek <# of weeks to check>] [-loop] [-v] [-help]\n");
my @@help = ("\n -home <homedir>    home directory, where temp files will be\n",
	    " -sleep <seconds>    number of seconds to sleep between loops\n",
	    " -mail <e-mail>      e-mail address to send update reports\n",
	    " -nweek <# weeks>    number of weeks to check for updates\n",
	    " -loop               continue to loop forever\n",
	    " -v                  verbose mode for debugging\n",
	    " -help               get this help message\n","\n");

while ($_ = shift @@ARGV) {
  # print " processing option $_\n";
  if   (/-home/)  { $home = shift @@ARGV;      }
  elsif(/-sleep/) { $sleeptime = shift @@ARGV; } 
  elsif(/-mail/)  { $mailto = shift @@ARGV;    }
  elsif(/-nweek/){ $nweek = shift @@ARGV;      }
  elsif(/-loop/)  { $once = 0;                }
  elsif(/-v/)     { $debug = 1;               }
  elsif(/-he/)    { print @@usage,@@help; exit; }
  else {  
    print @@usage,@@help;
    exit;
  }
}

if($debug){ 
  print "HOME = $home\nSOCHOME = $sochome\nsleeptime=$sleeptime\n";
  print "email report to $mailto\n";
  print "nweeks = $nweek\n";
  if($once) { print "running through loop only once\n"; }
  if($debug){ print "running in verbose mode\n"; }
}

my $localbin   = "$home/bin";
$update_report ="$home/bin/report.tmp";
$humanout      = "$home/bin/human.out";
$merge_human   = "$localbin/merge_human.pl";

while($loop){ #forever loop


   $week = `$sochome/bin/SunOS5/time_converter -current_mission_week`;
   chomp $week;

    # get directory names of last nweeks

    for ( $i = 0; $i < $nweek; $i++ ){
	$humandir[$i] = sprintf ( "$sanction/week%03d", $week-$i );
	if($debug){ 
	  printf STDERR "DEBUG: week%03d: $humandir[$i]\n", $week-$i, $humandir[$i];
	}
      }

    # get list of insert files; note we use ls -l, so we keep track of
    #  updates to existing insert files.
    @@insert_files=`ls -l $sanction/Wk*insert*`;	

    # check if merge_human needs to be run. 
    for ( $i = 0; $i < $nweek; $i++ ){
	$flag  = 0;
	$flag_insert = 0;

	if ( -e $humandir[$i] ){
	    $human[$i] = `ls -l $humandir[$i]/*.human`;

	    # note if this is the first time through, the flag will 
	    # always be 1, and merge_human will be run for each of nweeks
	    if ( $last_update_human[$i] ne $human[$i] ){ $flag = 1; } 

	    if($debug){ 
	      printf STDERR "last_update_human[$i] = $last_update_human[$i]\n";
	      printf STDERR "human[$i]             = $human[$i]\n";
	      printf STDERR "DEBUG: flag = $flag\n";
	    }
	}
	$Wk = $week + 160 - $i;
	$ninsf = 0;

        # track updates to existing insert files.

	for ( $j = 0; $j <= $#insert_files; $j++ ){
	    if ( index( $insert_files[$j], $Wk ) > 0 &&	index( $insert_files[$j], '~' ) < 0 ){
                 if ($update_ins[$i][$ninsf] ne $insert_files[$j] ){
		  if($debug){ 
		    printf STDERR "DEBUG: insert_files[$j] $insert_files[$j]\n";
		    printf STDERR "DEBUG: $update_ins[$i][$ninsf]flag=$flag\n";
		  }
		  $update_ins[$i][$ninsf] = $insert_files[$j];
		  $flag = 1;
		}
		$ninsf++;
	      }
	  }

        # run merge_human.pl if needed

	if ( $flag == 1 ){
	  if($debug){ 
	    printf STDERR "$perl $merge_human $Wk\n";
	  }
	  system "$perl $merge_human $Wk";

          # arg - this needs to be improved 
          # this makes files in $home/bin and copies them to the
          # human readable directory

	  if ( -e $humanout ){
	    $human[$i] = `ls $humandir[$i]/*.human`;
	    chop $human[$i];
	    $age = &file_age_hour( $human[$i] );
	    $age = sprintf( "%.3f", $age );
	    $humanback = $human[$i] . ".bak";
	    if ( -e $humanback ){ system "/bin/rm $humanback"; }
	    printf STDERR "human=$human[$i]\n";
	    system "mv $human[$i] $humanback";
	    system "/bin/mv $humanout $human[$i]";
	    system "echo This is output from xsm4:auto_merge_human.pl >> $update_report";
	    system "echo >> $update_report";
	    system "echo File last update $age hours ago >> $update_report";
	    $time = `/bin/date +%y/%m/%d/%H:%M`;
	    chop $time;
	    $subject = "\'update_human $time\'";
	    if ( $mailflag =~ /on/ ){
	      system "/bin/mailx -s $subject $mailto < $update_report";
	    }
	    system "rm $update_report";
	  }
	  $last_update_human[$i] = `ls -l $humandir[$i]/*.human`;
	  if($debug){ 
	   printf STDERR "last_update_human[$i] = $last_update_human[$i]\n";
	  }
	}
      }
   if($once) { $loop = 0; }
   sleep( $sleeptime );
}


sub file_age_hour{
#   &file_age_hour( file_name_to_check ); 
#   output: hours after last update
  if(-e $_[0]) {
    my $age = (-M $_[0])*24.0;
    if($debug) { print STDERR "$_[0] is $age hours old\n";}
    return $age;
  } else {
    print "$0 ERROR: file $_[0] does not exist - cannot check age\n";
    return -1;
  }
}
@


1.3
log
@Now with rcs tag!
@
text
@d18 1
a18 1
my $rcsid = " $ Id: $ ";  
@


1.2
log
@Big revision of script.  Now contains, help, command line options,
an ability to change envrionment variables, an ability to track changes
in insert fragments for more than 2 weeks.
file_age_hour function completely rewritten.
@
text
@d18 2
@


1.1
log
@Initial revision
@
text
@d2 16
d22 3
a28 8
$update_report ="/home/planner/bin/report.tmp";
$roop          = 0;
$sleeptime     = 1800;
$nweek         = 3;
$humanout      = "/home/planner/bin/human.out";
$perl          = "/usr/local/bin/perl";
$merge_human   = "/home/planner/bin/merge_human.pl";
$mailto        = "xteweb\@@olegacy.gsfc.nasa.gov";
d30 13
a42 2
$mailflag      = "on";
#$mailflag      = "off";
a44 1
while(1){
d46 40
a85 2
# these next two lines were put in to prevent us from having
# to update the week calculation code every couple of years
d87 2
a88 1
   $week = `/sochome/bin/SunOS5/time_converter -current_mission_week`;
d91 1
a91 10
# now obsolete
   # $day  = `date +%j`;
   # $year =  `date +%Y`;
   # if    ( $year == 1997 ){ $day +=  366; } 
   # elsif ( $year == 1998 ){ $day +=  731; } 
   # elsif ( $year == 1999 ){ $day += 1096; } 
   # elsif ( $year == 2000 ){ $day += 1461; } 
   # elsif ( $year == 2001 ){ $day += 1827; } 

   # $week = int ( ( $day - 26 ) / 7 );
d94 4
a97 2
	$humandir[$i] = sprintf ( "/sochome/sanction/week%03d", $week-$i );
#	printf STDERR "DEBUG: week%03d: $humandir[$i]\n", $week-$i, $humandir[$i];
d100 3
a102 3
    @@insert_files=`ls -l /sochome/sanction/Wk*insert*`;

    $roop += 1;	
d104 1
d111 3
a113 2
#	    printf STDERR "last_update_human[$i] = $last_update_human[$i]";
#	    printf STDERR "human[$i]             = $human[$i]";
d115 7
a121 2
#	    printf STDERR "DEBUG: flag = $flag\n";
	  }
d124 3
d129 4
a132 11
                if    ( $i == 0 && $update_ins0[$ninsf] ne $insert_files[$j] ){
#		    printf STDERR "DEBUG: insert_files[$j]\n";
#		    printf STDERR "DEBUG: $update_ins0[$ninsf]flag=$flag\n";
		    $update_ins0[$ninsf] = $insert_files[$j];
                    $flag = 1;
		  }
                elsif ( $i == 1 && $update_ins1[$ninsf] ne $insert_files[$j] ){
#		    printf STDERR "DEBUG: $insert_files[$j]";
#		    printf STDERR "DEBUG: $update_ins1[$ninsf]flag=$flag\n";
		    $update_ins1[$ninsf] = $insert_files[$j];
                    $flag = 1;
d134 3
d140 3
d144 33
a176 25
#	    printf STDERR "$perl $merge_human $Wk\n";
	    system "$perl $merge_human $Wk";
	    if ( -e $humanout ){
		$human[$i] = `ls $humandir[$i]/*.human`;
		chop $human[$i];
		$age = &file_age_hour( $human[$i] );
		$age = sprintf( "%.3f", $age );
		$humanback = $human[$i] . ".bak";
		if ( -e $humanback ){ system "/bin/rm $humanback"; }
#		printf STDERR "human=$human[$i]\n";
		system "mv $human[$i] $humanback";
		system "/bin/mv $humanout $human[$i]";
		system "echo This is output from xsm4:auto_merge_human.pl >> $update_report";
		system "echo >> $update_report";
		system "echo File last update $age hour ago >> $update_report";
		$time = `/bin/date +%y/%m/%d/%H:%M`;
		chop $time;
		$subject = "\'update_human $time\'";
		if ( $mailflag =~ /on/ ){
	            system "/bin/mailx -s $subject $mailto < $update_report";
		  }
		system "rm $update_report";
	      }
	    $last_update_human[$i] = `ls -l $humandir[$i]/*.human`;
#	    printf STDERR "last_update_human[$i] = $last_update_human[$i]\n";
d178 1
d180 3
a182 2
    sleep( $sleeptime );
  }
d188 7
a194 57

    local( $yy, $ddd, $hh, $mm, $hournow, @@work );
    local( $ll, $mon, $day, $tim, $fyy, $fddd, $fhh, $fmm, $fhour, $fage );

    $yy  = `/bin/date +%y`;
    $ddd = `/bin/date +%j`;
    $hh  = `/bin/date +%H`;
    $mm  = `/bin/date +%M`;
    chop $yy; chop $ddd; chop $hh; chop $mm;

    $hournow = $ddd * 24.0 + $hh + $mm / 60.0; 
#    printf STDERR "DEBUG: nowtime $yy/$ddd/$hh:$mm ($hournow)\n";

    $file_name_to_check = $_[0];
#    printf STDERR "DEBUG: file=$file_name_to_check, $_[0], $_[1] \n";
    $ll = `ls -l $file_name_to_check`;
    #print $ll, "\n";
    chop $ll;
    @@work = split( /\s+/, $ll );
    $mon = $work[5];
    $day = $work[6];
    $tim = $work[7];
#    printf STDERR "DEBUG: $ll\nDEBUG: $mon $day $tim\n";

    if ( index( $tim, ":" ) < 0 ){
        $fyy = $tim%100;
        $fhh = 0;
        $fmm = 0;
      }
    else { 
        $fyy = $yy;
        $fhh = substr( $tim, 0, 2 );
        $fmm = substr( $tim, 3, 2 );
      }
    if    ( $mon eq "Jan" ){ $fddd =       $day; }
    elsif ( $mon eq "Feb" ){ $fddd =  31 + $day; }
    elsif ( $mon eq "Mar" ){ $fddd =  59 + $day; }
    elsif ( $mon eq "Apr" ){ $fddd =  90 + $day; }
    elsif ( $mon eq "May" ){ $fddd = 120 + $day; }
    elsif ( $mon eq "Jun" ){ $fddd = 151 + $day; }
    elsif ( $mon eq "Jul" ){ $fddd = 181 + $day; }
    elsif ( $mon eq "Aug" ){ $fddd = 212 + $day; }
    elsif ( $mon eq "Sep" ){ $fddd = 243 + $day; }
    elsif ( $mon eq "Oct" ){ $fddd = 273 + $day; }
    elsif ( $mon eq "Nov" ){ $fddd = 304 + $day; }
    elsif ( $mon eq "Dec" ){ $fddd = 334 + $day; }
    if ( $fyy % 4 == 0 && $mon ne "Jan" && $mon ne "Feb" ){ $fddd++; }  


    $fhour = $fddd * 24.0 + $fhh + $fmm / 60.0;
#    printf STDERR "DEBUG: file $fyy/$fddd/$fhh:$fmm ($fhour)\n";

    if ( $fhour > $hournow && $yy == $fyy ){ $fyy--; }

    $fage = $hournow - $fhour + 8760.0 * ( $yy - $fyy );

    return( $fage );
d196 1
@

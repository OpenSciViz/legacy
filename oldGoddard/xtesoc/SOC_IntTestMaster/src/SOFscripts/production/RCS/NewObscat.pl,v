head	1.1;
access;
symbols;
locks; strict;
comment	@# @;


1.1
date	2000.12.12.14.29.18;	author jonv;	state Exp;
branches;
next	;


desc
@GUI program for letting the user change the obscat after it has been
used in processing.
@


1.1
log
@Initial revision
@
text
@#!/usr/local/bin/perl -w

# ===========================================================================
# Filename    : NewObscat.pl
# Subsystem   : SOFscripts/production
# Programmer  : Jon Vandegriff, Emergent IT
# Description : 
#
#  A visual tool whcih makes it easier to change an obscat after you
#  have processed a day's data with an incorrect obscat.
#  It removes all the old, now invalid data, and sets the databases
#  straight in terms of dealing with the new obscat and any new, changed,
#  or removed obsids.
#
#  There is some help available fromthe help menu once you run the
#  application.
#
# RCS: $Id$
# ===========================================================================


use Tk;
use Tk::FileSelect;
use Data::Compare;
use File::Path;

use ProdDB;
use ProdUtils qw( $act $verb $VERB $DEBUG);
use ParameterManager;
use HRObservation;

use strict;

# This program allows the user to change the obscat after it has already 
# been tagged with an MD5 checksum in the database and possibly used to 
# make FITS files.
#
# Issues of concern:
#  Which obsids are: removed, changed, new
#  Have the FITS files for the obsids in the older obscat already been made?
#    What does the user want to do with them?
#       Delete them all?
#       Only delete the ones which have disappeared or changed?
#       Leave them alone?
#  The following databases need to be changed to reflect the new obscat:
#         Done_Obs
#         Distributed_Obs
#         Obscats_Used

my $main;
my( $new_obscat_filename, $old_obscat_filename );
my( $new_obscat, $old_obscat );  # each holds an ObsCat.pm object
my( $new_obsids, $changed_obsids, $removed_obsids );
my $delete_changed_obsids = 1;
my $delete_removed_obsids = 1;
my $delete_new_obsids     = 1;  # just in case the obsid had been 
                                #    used somewhere else!!

my $fixLocks;
my $db_timeout;
my $no_action  = 0;
my $verbose    = 0;

# GUI components:
my( $new_oc_md5, $old_oc_md5 );
my( $new_oc_box, $old_oc_box );
my( $new_obsids_box, $changed_obsids_box, $removed_obsids_box );
my $implement_button;

{
  my $parms = ProdUtils::get_parms(\@@ARGV);

  ( $new_obscat_filename, $old_obscat_filename, $db_timeout, $fixLocks )
    = parse_args($parms, @@ARGV);

  build_GUI();

  MainLoop();

}

sub parse_args {
  my $parms = shift;
  # rest of @@_ is @@ARGV;

  my $new_oc_file = "";
  my $old_oc_file = "";
  my $local_db_timeout  = $parms->get_scalar("default_db_timeout");
  my $local_fixLocks    = 1;


  my $arg;
  while( defined($arg = shift @@_) ) {
    if( "-new" eq $arg ) {
      $new_oc_file = shift @@_;
    } elsif( $arg eq "-old" ) {
      $old_oc_file = shift @@_;
    } elsif( $arg eq "-cname" ) {
      shift @@_;
    } elsif( $arg eq "-timeout" ) {
      $local_db_timeout = shift @@_;
    } elsif( $arg eq "-nolockfix") {
      $local_fixLocks = 0;
    } else {
      usage();
      ProdUtils::exit_with_error("","Invalid argument for $0: '$arg'","");
    }
  }
  return( $new_oc_file, $old_oc_file, $local_db_timeout, $local_fixLocks );
}

sub usage {
  ProdUtils::error
    ("","usage for $0",
     "  [-cname config_name]",
     "  [-timeout db_timeout in seconds] ",
     "  [-nolockfix]   ( don't fix broken locks when opening a DB",
     "  [-new new_obscat_filename] ",
     "  [-old old_obscat_filename] ",
     "The new and old obscats do not have to be specifid on the command",
     "line. They can be chosen after the program window appears.");
}


sub build_GUI {

  $main = new MainWindow(-title => "NewObscat.pl");


  # First the top frame with all the menu buttons.
  my $top_frame = $main->Frame(-relief=>'groove', -borderwidth=>1)
    ->pack(-side=>'top', -anchor=>'w', -expand=>1, -fill=>'x');

  my $fileButton = $top_frame->Menubutton(-text => "File", -tearoff=>0)
    ->pack(side => 'left');

  my $optsButton = $top_frame->Menubutton(-text => "Options", -tearoff=>0)
    ->pack(side => 'left');

  my $helpButton = $top_frame->Menubutton(-text => "Help", -tearoff=>0)
    ->pack(side => 'right');



  $fileButton->command(-label=>"Choose New ObsCat file...",
		       -command=>[\&get_oc_file, "new"]);
  $fileButton->command(-label=>"Choose Old ObsCat file...",
		       -command=>[\&get_oc_file, "old"]);
  $fileButton->command(-label=>"Quit", -command=>sub{exit;});

  $optsButton->checkbutton(-label=>'No Action Mode',
			   -variable=> \$no_action);

  $optsButton->checkbutton(-label=>'Verbose Mode',
			   -variable=> \$verbose);

  $optsButton->checkbutton(-label=>'Fix locks when opening DB',
			   -variable=> \$fixLocks);

  my $timeout_menu = $optsButton->menu->Menu(-tearoff=>0);
  foreach ( qw(10 15 30 60 120 300) ) {
    $timeout_menu->radiobutton(-label => "$_ sec",
			       -variable => \$db_timeout,
			       -value => $_ );
  }
  $optsButton->cascade( -label=>"DB Timeout Values" );
  $optsButton->entryconfigure("DB Timeout Values", -menu=>$timeout_menu);

  $helpButton->command(-label=>"Basic Info", -command=>\&showHelp);

  # Now make the Frame with the obscat file display windows.
  my $lf = $main->Frame()->pack(-side=>'left', -expand=>1, -fill=>'both');
  my $lf1a = $lf->Frame()->pack(-side=>'top', -expand=>0, -anchor=>'w');
  $lf1a->Label(-text=>"New Obscat: ", -justify=>'left')
    ->pack(-side=>'left', -anchor=>'w',-expand=>0);
  $lf1a->Label(-textvariable=>\$new_obscat_filename, -justify=>'left')
    ->pack(-side=>'left', -anchor=>'w',-expand=>0);
  my $lf1b = $lf->Frame()->pack(-side=>'top', -expand=>0, -anchor=>'w');
  $lf1b->Label(-text=>"MD5: ", -justify=>'left')
    ->pack(-side=>'left', -anchor=>'w',-expand=>0);
  $lf1b->Label(-textvariable=>\$new_oc_md5, -justify=>'left')
    ->pack(-side=>'left', -anchor=>'w',-expand=>0);
  my $lf1c = $lf->Frame()->pack(-side=>'top', -expand=>1, -fill=>'both',
				-anchor=>'w');
  $new_oc_box = $lf1c->Scrolled('Listbox', -scrollbars=>'osoe',
				-width=>65, -height=>18)
    ->pack(-side=>'top', -anchor=>'w',-expand=>1, -fill=>'both');



  my $lf2a = $lf->Frame()
    ->pack(-side=>'top', -expand=>0, -fill=>'both');
  $lf2a->Label(-text=>"Old Obscat: ", -justify=>'left')
    ->pack(-side=>'left', -anchor=>'w',-expand=>0);
  $lf2a->Label(-textvariable=>\$old_obscat_filename, -justify=>'left')
    ->pack(-side=>'left', -anchor=>'w',-expand=>0);
  my $lf2b = $lf->Frame()->pack(-side=>'top', -expand=>0, -anchor=>'w');
  $lf2b->Label(-text=>"MD5: ", -justify=>'left')
    ->pack(-side=>'left', -anchor=>'w',-expand=>0);
  $lf2b->Label(-textvariable=>\$old_oc_md5, -justify=>'left')
    ->pack(-side=>'left', -anchor=>'w',-expand=>0);
  my $lf2c = $lf->Frame()->pack(-side=>'top', -expand=>1, -fill=>'both',
				-anchor=>'w');
  $old_oc_box = $lf2c->Scrolled('Listbox', -scrollbars=>'osoe',
				-width=>65, -height=>18)
    ->pack(-side=>'top', -anchor=>'w',-expand=>1, -fill=>'both');


  my $rf = $main->Frame(-relief=>'sunken', -borderwidth=>1)
    ->pack(-side=>'left', -expand=>0, -anchor=>'nw', -padx=>10, -pady=>10);
  $rf->Label(-text=>"Obsids affected by the new obscat")
    ->pack(-side=>'top', -anchor=>'center');
  $rf->Label(-text=>"(double click an obsids to view it)")
    ->pack(-side=>'top', -anchor=>'center');

  $rf->Label(-text=>"new obsids:")
    ->pack(-side=>'top', -anchor=>'w');

  $new_obsids_box = $rf->Scrolled('Listbox', -scrollbars=>'osoe',
				  -width=>20, -height=>4)
    ->pack(-side=>'top', -anchor=>'w',-expand=>1, -fill=>'both');

  $rf->Label(-text=>"changed obsids:")
    ->pack(-side=>'top', -anchor=>'w');

  $changed_obsids_box = $rf->Scrolled('Listbox', -scrollbars=>'osoe',
				  -width=>20, -height=>4)
    ->pack(-side=>'top', -anchor=>'w',-expand=>1, -fill=>'both');

  $rf->Label(-text=>"removed obsids:")
    ->pack(-side=>'top', -anchor=>'w');

  $removed_obsids_box = $rf->Scrolled('Listbox', -scrollbars=>'osoe',
				  -width=>20, -height=>4)
    ->pack(-side=>'top', -anchor=>'w',-expand=>1, -fill=>'both');


  # Now make the Action Frame
  my $af = $rf->Frame(-relief=>'sunken', -borderwidth=>1)
    ->pack(-side=>'top', -expand=>0, -anchor=>'nw', -padx=>10, -pady=>20);
  $af->Label(-text=>"What to do with the new ObsCat:")
    ->pack(-side=>'top', -anchor=>'center', -pady=>5);

  my $af1 = $af->Frame()
    ->pack(-side=>'top', -expand=>1, -fill=>'x', -anchor=>'w', -pady=>10);
  $af1->Label(-text=>"DELETE:")
    ->grid(-row=>0, -column=>0, -rowspan=>3, -sticky=>'ns');

  $af1->Checkbutton(-text=>"FITS files for changed obsids",
		    -variable => \$delete_changed_obsids)
    ->grid(-row=>0, -column=>1, -rowspan=>1, -sticky=>'w');

  $af1->Checkbutton(-text=>"FITS files for removed obsids",
		    -variable => \$delete_removed_obsids)
    ->grid(-row=>1, -column=>1, -rowspan=>1, -sticky=>'w');

  $af1->Checkbutton(-text=>"FITS files for new obsids",
		    -variable => \$delete_new_obsids)
    ->grid(-row=>2, -column=>1, -rowspan=>1, -sticky=>'w');

  $af->Label(-text=>"Also, the Done_Obs, Distributed_Obs,")
    ->pack(-side=>'top', -anchor=>'w');

  $af->Label(-text=>"and Obscats_Used databases will be")
    ->pack(-side=>'top', -anchor=>'w');
  $af->Label(-text=>" changed to match the new obscat.")
    ->pack(-side=>'top', -anchor=>'w');

  $implement_button = 
  $af->Button(-text=>"IMPLEMENT NEW OBSCAT", -command=>\&implement_new_obscat)
    ->pack(-side=>'top', -anchor=>'center', -pady=>15);


  $new_obsids_box->bind("<Double-Button-1>",
			sub { show_obsid($new_obsids_box, $new_oc_box); } );

  $changed_obsids_box->bind("<Double-Button-1>",
			sub { show_obsid($changed_obsids_box, $new_oc_box);
			      show_obsid($changed_obsids_box, $old_oc_box);} );

  $removed_obsids_box->bind("<Double-Button-1>",
			sub { show_obsid($removed_obsids_box, $old_oc_box);} );


  update_GUI();

}


sub get_oc_file {
  my( $type ) = @@_;

  my $popup = $main->FileSelect(-directory => $ENV{"SOCOPS"}."/keep/obscat",
				-takefocus => 1);

  my $file_chosen = $popup->Show;

  if( $file_chosen ) {
    if( $type eq "new" ) {
      $new_obscat_filename = $file_chosen;
    } elsif( $type eq "old" ) {
      $old_obscat_filename = $file_chosen;
    } else {
      ProdUtils::exit_with_error("Internal error: wrong type sent to ".
	"'get_oc_file': '$type' - type must be either 'new' or 'old'");
    }
    update_GUI();
  }

}


sub show_obsid {
  my( $obsid_box, $oc_box ) = @@_;

  my ($index) = $obsid_box->curselection();
  my $obsid   = $obsid_box->get( $index );

  my $location = 0;
  foreach ( $oc_box->get(0,'end') ) {
    if( /$obsid\s+START$/ ) {
      $oc_box->see($location);
      last;
    }
    ++$location;
  }

}



sub update_GUI {

  my $obscats_ready = 1;

  $new_oc_md5 = "";
  $old_oc_md5 = "";

  if( $new_obscat = add_obscat($new_obscat_filename, $new_oc_box) ) {
    $new_oc_md5 = $new_obscat->md5;
  } else {
    $obscats_ready = 0;
  }

  if( $old_obscat = add_obscat($old_obscat_filename, $old_oc_box) ) {
    $old_oc_md5 = $old_obscat->md5;
  } else {
    $obscats_ready = 0;
  }


  # Make sure the obscats come from the same day
  unless( obscats_come_from_same_day( $new_obscat, $old_obscat) ) {
    $obscats_ready = 0;
  }

  if( $old_obscat ) {
    unless( old_obscat_matches_obscat_in_db($old_obscat) ) {
      $obscats_ready = 0;
    }
  }

  $new_obsids_box->delete(0,'end');
  $changed_obsids_box->delete(0,'end');
  $removed_obsids_box->delete(0,'end');

  if( $obscats_ready ) {

    ( $new_obsids, $changed_obsids, $removed_obsids ) = 
      study_two_obscats( $new_obscat, $old_obscat );

    # add the obsids to the appropriate list boxes
    $new_obsids_box->insert('end',     @@$new_obsids);
    $changed_obsids_box->insert('end', @@$changed_obsids);
    $removed_obsids_box->insert('end', @@$removed_obsids);

  }

  if( $obscats_ready ) {
    $implement_button->configure(-state=>'normal');
  } else {
    $implement_button->configure(-state=>'disabled');
  }

}



sub obscats_come_from_same_day {
  my( $new_obscat, $old_obscat ) = @@_;

  my $ok = 1;

  if( $new_obscat and $old_obscat ) {
    unless( $new_obscat->day eq $old_obscat->day ) {
      $ok = 0;
      ProdUtils::error(" OBSCATS come from different days!");
    }
  }

  return $ok;
}

sub old_obscat_matches_obscat_in_db {
  my( $old_obscat ) = @@_;

  my $match = 1;

  my $db_name = ProdDB::Obscats_Used();
  
  my $db = ProdDB->make( $db_name, $db_timeout, $fixLocks );

  if( $db->connect($db_timeout, $fixLocks) ) {
    my $existing_md5 = $db->getValue($old_obscat->day);
    unless( $existing_md5 eq $old_obscat->md5 ) {
      $match = 0;
      ProdUtils::error
	("MD5 checksum for your old obscat ".$old_obscat->fullfile,
	"does not match the MD5 of the obscat used by the processing system.");
    }
    $db->disconnect();
  } else {
    $match = 0;
    ProdUtils::error("Can't open '$db_name' DB to check obscat MD5.");
  }

  return $match;
}



sub add_obscat {
  my( $oc_filename, $oc_box ) = @@_;

  my $obscat = undef;
  $oc_box->delete(0,'end');

  if( $oc_filename ) {
    $obscat = ObsCat->make_from_file( $oc_filename );
    if( defined($obscat) ) {
      my $oc_lines = $obscat->get_all_lines();
      if( scalar(@@$oc_lines) > 0 ) {
	$oc_box->insert('end', @@$oc_lines);
      } else {
	ProdUtils::error("could not get any lines from obscat ".
			 "'$oc_filename'");
	$obscat = undef;
      }
    } else {
      $obscat = undef;
    }
  } else {
    $obscat = undef;
  }

  return $obscat;
}



sub study_two_obscats {
  my ( $new, $old ) = @@_;

  my $new_hr_list = HRObservation->get_observation_list( $new->fullfile );
  my $old_hr_list = HRObservation->get_observation_list( $old->fullfile );

  my $new_hr_hash = {};
  my $old_hr_hash = {};

  foreach (@@$new_hr_list) { $new_hr_hash->{$_->obsid()} = $_; }
  foreach (@@$old_hr_list) { $old_hr_hash->{$_->obsid()} = $_; }


  my %obsid_hash_from_new_obscat = ();
  my %obsid_hash_from_old_obscat = ();
  foreach ( @@{ $new->get_obsids } ) { $obsid_hash_from_new_obscat{$_} = 0; }
  foreach ( @@{ $old->get_obsids } ) { $obsid_hash_from_old_obscat{$_} = 0; }

  my $new_obsids = [];
  my $changed_obsids = [];
  my $removed_obsids = [];

  foreach ( @@{ $new->get_obsids } ) {
    if( exists($obsid_hash_from_old_obscat{$_}) ) {
      unless( are_obsids_same_in_each($_, $new_hr_hash, $old_hr_hash) ) {
	push( @@$changed_obsids, $_ );
      }
    } else {
      push( @@$new_obsids, $_ );
    }

    # Delete all the obsids from the old list which were seen in the new list,
    # That way the only ones left are the ones which exist only in the old
    # list. (and hence have been "removed" from the new list.
    delete $obsid_hash_from_old_obscat{$_};
  }

  foreach ( @@{ $old->get_obsids } ) {
    if( exists($obsid_hash_from_old_obscat{$_}) ) {
      push( @@$removed_obsids, $_ );
    }
  }

  return( $new_obsids, $changed_obsids, $removed_obsids );

}



sub are_obsids_same_in_each {
  my( $obsid, $new_hr_hash, $old_hr_hash ) = @@_;

  my $obs1 = $new_hr_hash->{$obsid};
  my $obs2 = $old_hr_hash->{$obsid};

  my $same = 0;
  if( $obs1 and $obs2 ) {
    $same = Compare( $obs1, $obs2 ); # from Data::Compare
  }

  return $same;
}


sub implement_new_obscat {

  # Delete all FITS files requested
  # Update the databases to match the new obscat

  my $day = $new_obscat->day;

  if( $delete_changed_obsids ) {
    delete_obsids( $changed_obsids ) ;
  }

  if( $delete_removed_obsids ) {
    delete_obsids( $removed_obsids );
  }

  if( $delete_new_obsids ) {
    delete_obsids( $new_obsids );
  }

 # Now update the databases.

  update_Done_Obs_db( $new_obscat,
		      $new_obsids, $changed_obsids, $removed_obsids);

  update_Distributed_Obs_db( $new_obscat,
			     $new_obsids, $changed_obsids, $removed_obsids );

  update_obscat_db( $new_obscat );

}

sub delete_obsids {
  my( $obsids_to_delete ) = @@_;

  foreach my $obsid (@@$obsids_to_delete) {
    my $obsid_dir = ProdUtils::make_full_obsid_dirname( $obsid );
    if( -d $obsid_dir ) {
      if( $no_action) {
	print "Would remove obsid dir '$obsid_dir'\n" if $verbose;
      } else {
	my $show_files_deleted = 0;
	rmtree( $obsid_dir, $show_files_deleted );
	if( -d $obsid_dir ) {
	  ProdUtils::error("Could not remove obsid directory '$obsid_dir'",
			 "It will have to be removed by hand.");
	}
      }
    } else {
      ProdUtils::error("WARNING - dir '$obsid_dir' not found to delete!");
    }
  }
}


sub update_Done_Obs_db {
  my( $new_obscat,
      $new_obsids, $changed_obsids, $removed_obsids ) = @@_;

  my $db_name = ProdDB::Done_Obs();

  my $ok = 1;

  my $day = $new_obscat->day;

  my $db = ProdDB->make( $db_name, $db_timeout, $fixLocks );

  if( $db->connect($db_timeout, $fixLocks) ) {

    # Add the new obsids to the DB with unattempted status
    foreach my $obsid (@@$new_obsids) {
      if( $db->doesSubKeyExist($day, $obsid) ) {
	ProdUtils::error
	  ("Found obsid '$obsid' already in DB '$db_name' while trying",
	   "to add it to db '$db_name'.  Will overwrite existing entry.");
      }
      my $value = [ $new_obscat->get_start_time_utc($obsid),
		    $new_obscat->get_stop_time_utc($obsid),
		    ProdUtils::xff_status_NEW() ];

      my $value_string = "[ ".join(", ", @@$value)." ]";

      $db->setSubKeyValue($day, $obsid, $value) unless $no_action;
      print $db_name."->setSubKeyValue($day, $obsid, $value_string)\n"
	if $verbose;
    }

    # Update the changed obsids in the DB, giving them an unattempted status.
    foreach my $obsid (@@$changed_obsids) {
      unless( $db->doesSubKeyExist($day, $obsid) ) {
       ProdUtils::error
	 ("Could not find obsid '$obsid' in DB '$db_name' while trying to",
	  "update it in DB '$db_name'.  Will create new entry for it.");
      }
      my $value = [ $new_obscat->get_start_time_utc($obsid),
		    $new_obscat->get_stop_time_utc($obsid),
		    ProdUtils::xff_status_NEW() ];

      my $value_string = "[ ".join(", ", @@$value)." ]";

      $db->setSubKeyValue($day, $obsid, $value) unless $no_action;
      print $db_name."->setSubKeyValue($day, $obsid, $value_string)\n"
	if $verbose;
    }

    # Remove the obsids from the DB which have disappeared in the new obscat.
    foreach my $obsid (@@$removed_obsids) {
      if( $db->doesSubKeyExist($day, $obsid) ) {
	$db->removeSubKey($day, [$obsid]) unless $no_action;
	print $db_name."->removeSubKey($day, $obsid)\n" if $verbose;
      } else {
       ProdUtils::error
	 ("Could not find obsid '$obsid' in DB '$db_name' while trying to",
	  "delete it. Nothing will be done since its already gone!");
     }
    }

    $db->disconnect();

  } else {
    ProdUtils::error("Can't connect to DB '$db_name'");
    $ok = 0;
  }

  return $ok;
}


sub update_Distributed_Obs_db {
  my( $new_obscat,
      $new_obsids, $changed_obsids, $removed_obsids ) = @@_;

  my $db_name = ProdDB::Distributed_Obs();

  my $ok = 1;

  my $day = $new_obscat->day;

  my $db = ProdDB->make( $db_name, $db_timeout, $fixLocks );

  if( $db->connect($db_timeout, $fixLocks) ) {

    # Make sure all of the changed, added, or removed
    # obsids are taken out of the Distributed_Obs
    # database.  (They will be added in by the RunXFF.pl
    # program when it sucessfully processes them.)
    foreach my $obsid (@@$new_obsids, @@$changed_obsids, @@$removed_obsids) {
      if( $db->doesSubKeyExist($day, $obsid) ) {
	$db->removeSubKey($day, [$obsid]) unless $no_action;
	print $db_name."->removeSubKey($day, $obsid)\n" if $verbose;
      }
    }

    $db->disconnect();
  } else {
    ProdUtils::error("Can't connect to DB '$db_name'");
    $ok = 0;
  }

  return $ok;
}

sub update_obscat_db {
  my( $new_obscat ) = @@_;

  my $db_name = ProdDB::Obscats_Used();
  
  my $db = ProdDB->make( $db_name, $db_timeout, $fixLocks );

  if( $db->connect($db_timeout, $fixLocks) ) {

    my $day = $new_obscat->day;
    my $md5 = $new_obscat->md5;
    $db->setValue($day, $md5) unless $no_action;
    print $db_name."->setValue($day, $md5)\n" if $verbose;

    $db->disconnect();
  } else {
    ProdUtils::error
      ("The MD5 checksum for the new obscat was not able to be updated.",
      "You will have to try to update it manually using the database editor.");
  }

}




sub showHelp {
  my $top = $main->Toplevel(-title=>"NewObscat.pl Help");
  my $help_string = 
"This program is used to change the official version of the obscat used by
the production processing system.  If an obscat has not been used to make
FITS files, you can just replace the file, but if you have already made the
FITS files, then there are some problems which this program can help you
deal with.

First, the pipeline stores the MD5 checksum of the obscat it uses, and it
won't distribute any obsids for a day whose obscat has a different checksum.
(Note that the checksum is based on only the non-comment, non-blank lines
in the obscat.)

Second, any FITS files for obsids which are changed in the new obscat need
to be dealt with.  The new obscat can have new obsids, it can have the same
obsids but with changed information, or it can be missing some obsids that
were in the old obscat.  Any obsids which were changed or removed need to 
have their FITS files deleted.  Then all obsids which are changed or new
need to be remade into FITS and then re-distributed.

So if you want to change an obscat, do this:

  Make a backup of the old obscat and save it with a name like 
      \"OCday2420.old\"
      The file name for the old obscat must still have the format OCdayNNNN
      so that the day number can be obtained from it.

  Create the new obscat and give it the regular name:
      OCday2420

  Run this program and specify the old and new filenames on the command line:
     NewObscat.pl -old \$SOCOPS/keep/obscat/OCday2420.old
     (If you don't specify any names on the command line, you can load
      obscats into the program after its going - see the File Menu at 
      the top.)

  Make sure the new obscat has the changes you want.
    Note that the old obscat must have the same MD5 checksum as the one
    the database knows about.

  Choose which FITS files you want deleted.  The default is to delete the
    FITS files of all of the new, changed, and removed obsids.

  Select optional verbose mode, or a no-action mode, and the database
    timeout and lock fixing settings (see the Options menu at the top).

  Click on IMPLEMENT NEW OBSCAT button.
    Note that this button only becomes active when there are two legtimate
    obscats loaded (both from the same day!) and the old obscat matches the 
    MD5 checksum of the obscat know about by the database.";


  my $frame1 = $top->Frame()->pack(-side=>'top', -expand=>1, -fill=>'both',
				   -anchor=>'w', -padx=>10);
  my $text = $frame1->Scrolled('Text', -scrollbars=>'oe',
		    -width=>80, -height=> 30)
    ->pack(-side=>'top', -anchor=>'w',-pady=>5, -fill=>'both', -expand=>1);

  $text->insert('end', $help_string );
  $text->configure(-state=>'disabled');

  $frame1->Button(-text=>"Close", -command=> sub{ $top->destroy(); })
    ->pack(-side=>'left', -anchor=>'w', -padx=>300, -pady=>10);
}

@

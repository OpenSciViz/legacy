head	1.1;
access;
symbols;
locks
	bob:1.1; strict;
comment	@# @;


1.1
date	2000.12.12.14.29.18;	author jonv;	state Exp;
branches;
next	;


desc
@chooses the LZ files for a particular day; also responsible
for adding a day to the "to-be-selected" database (the Days_Selected
databse)
@


1.1
log
@Initial revision
@
text
@#!/usr/local/bin/perl -w

# ===========================================================================
# Filename    : 
# Subsystem   : SOFscripts/production
# Programmer  : Jon Vandegriff, Emergent IT
# Description : 
#
#  See the pod documentation at the bottom of the program.
#
#  Also, there is a GUI interface for selecting LZ files for specified
#  days. It is acessible from the Control.pl program.
#
# RCS: $Id$
# ===========================================================================


use ProdDB;
use ProdUtils qw( $act $verb $VERB $DEBUG);

use ParameterManager;

use strict;

{
  my $prog = "SelectLZ.pl";
  my $parms = ProdUtils::get_parms(\@@ARGV);

  my( $new_day_selection_method, $specific_days_to_do, $redo_days,
      $specific_files, $insert,
      $force_ok, $clear_ds_db, $fully_clear_ds_db,
      $days_to_add, $no_processing_when_adding,
      $db_timeout, $fixLocks ) =
	select_parse_args($prog, $parms, @@ARGV);

  my $days_selected_db = ProdDB->make( ProdDB::Days_Selected(), $db_timeout);
  my $lzfile_db        = ProdDB->make( ProdDB::LevelZero(),     $db_timeout);
  my $doneDS_db        = ProdDB->make( ProdDB::Datasets(),      $db_timeout);

  my $db_list = [ $days_selected_db, $lzfile_db, $doneDS_db ];

  if(!ProdUtils::connect_all("dblist" => $db_list, "timeout" => $db_timeout,
			     "fixLocks" => $fixLocks) )
  {
    ProdUtils::exit_with_error("$prog: can't connect to required DB's ".
			       "in $db_timeout seconds - exiting");
  }

  if( defined($days_to_add) ) {
    ProdUtils::add_days( $days_selected_db, $days_to_add, 
			 $no_processing_when_adding );
  } else {
    unless( ProdUtils::update_days_to_select($days_selected_db,
					     $new_day_selection_method) )
    {
      ProdUtils::error("error while considering wether to add any days o the",
		       $days_selected_db->name." databse");
    }
  }


  # Do either all the days in the DB queue, or just a selection of them
  # as dictated by the user (via the command line options "-day").

  my $days_attempted = {}; # Keep track of which days are attempted.
  # This prevents the program from trying again and again on the same
  # day!.

  my $day;

  while( $day = ProdUtils::get_next_key($days_selected_db,$redo_days,$force_ok,
					$specific_days_to_do, $days_attempted))
  {

    $days_attempted->{$day} = 1;  # record that this day has been attempted

    my $files_for_day; # list ref to all the LZ files for this day

    if( $specific_files ) {
      # make sure they are all legitimate LZ files for the given day
      $files_for_day = ProdUtils::examine_users_LZ_files($specific_files,$day);
    } else {
      $files_for_day = ProdUtils::grab_all_lz_files_for_given_day( $day );
    }

    my($ok, $msgs) = ProdUtils::is_file_list_acceptable($day, $files_for_day);

    if( $msgs and scalar(@@$msgs)>0 ) {
      ProdUtils::error("messages from LZ select process:",@@$msgs);
    }

    if( !$ok and $force_ok ) {
      ProdUtils::error("$prog - NOTIFICATION -",
       "Just letting you know that the selection process for LZ files",
       "on day $day failed, but the user elected to force it to be OK.");
      $ok = 1;
    }

    ProdUtils::update_select_days_dbs
      ( $prog, $days_selected_db, $lzfile_db, $doneDS_db, $insert,
	$clear_ds_db, $fully_clear_ds_db, $day, $files_for_day, $ok );

  }

  ProdUtils::disconnect_all( $db_list );

  unless( scalar(keys %$days_attempted) > 0 ) {
    print "$prog - No days to process\n" if $verb;
  }


}



sub select_parse_args {
  my( $prog, $parms, @@argv ) = @@_;

  my $selection_method    = $parms->get_scalar("lz_select_method");
  my $specific_days_to_do = undef;
  my $redo_days           = 0;
  my $specific_files      = undef;
  my $insert              = 0;
  my $force_ok            = 0;
  my $clear_ds_db         = 1;
  my $fully_clear_ds_db   = 0;
  my $days_added          = undef;
  my $add_only            = 0;
  my $db_timeout          = $parms->get_scalar("default_db_timeout");
  my $fixLocks            = 1;

  my $arg;
  while( defined($arg = shift @@argv) ) {
    if( $arg eq "-cname" ) {
      shift @@argv;
    } elsif( $arg eq "-n" ) {
      $act = 0;  # global from ProdUtils
    } elsif( $arg eq "-v" ) {
      $verb = $VERB; # global from ProdUtils
    } elsif( $arg eq "-d" ) {
      $verb = $DEBUG; # global from ProdUtils
    } elsif( $arg eq "-timeout" ) {
      $db_timeout = shift @@argv;
    } elsif( $arg eq "-nolockfix") {
      $fixLocks = 0;

    } elsif( $arg eq "-method" ) {
      $selection_method = shift @@argv;
    } elsif( $arg eq "-days" ) {
      # Define a hash table of the specific keys the user wants to do
      $specific_days_to_do = {};
      while( (scalar(@@argv)>0) and ($argv[0] !~ /^-/) ) {
	$specific_days_to_do->{ shift @@argv } = 0;
      }
    } elsif( $arg eq "-redodays" ) {
      $redo_days = 1;
    } elsif( $arg eq "-files" ) {
      # Define a hash table of the specific files the user wants to apply
      # to the given day
      $specific_files = {};
      while( (scalar(@@argv)>0) and ($argv[0] !~ /^-/) ) {
	$specific_files->{shift @@argv} = 0;
      }
    } elsif( $arg eq "-insert") {
      $insert = 1;
    } elsif( $arg eq "-forceok") {
      $force_ok = 1;
    } elsif( $arg eq "-nodsclear") {
      $clear_ds_db = 0;
    } elsif( $arg eq "-fulldsclear") {
      $fully_clear_ds_db = 1;

    } elsif( $arg eq "-add" ) {
      # The user wants to add some days into the queue.
      # To keep other days besides the ones asked for by the user 
      # from being added, the selection method is set to "none".
      $selection_method = "none";
      $days_added = [];
      while( (scalar(@@argv)>0) and ($argv[0] !~ /^-/) ) {
	push( @@$days_added, shift @@argv );
      }
    } elsif( $arg eq "-addonly" ) {
      $add_only = 1;


    } else {
      usage($prog);
      ProdUtils::exit_with_error("Unknown argument to $prog: \"$arg\"");
    }
  }

  if( $force_ok or $redo_days ) {
    unless( defined($specific_days_to_do) ) {
      die "$prog: Invalid use of '-forceok' or '-redodays' option.\n".
	"You must also specify one or more days after the '-days' option.\n";
    }
  }

  if( $specific_files ) {
    unless( defined($specific_days_to_do) and 
	    ( scalar(keys %$specific_days_to_do)==1 ) )
    {
      die "$prog: if you specify the '-files' option, then you must\n".
	"also specify exactly one day with the '-days' option.\n";
    }
  }

  if( !$clear_ds_db and $fully_clear_ds_db ) {
    die "$prog: incompatible options: '-nocleards' and '-fullcleards'\n";
  }


  if( defined($days_added) ) {
    unless( $selection_method eq "none" ) {
      die "You can't use the '-method' option if you use the '-add' option\n".
	"because the '-add' option sets the selection method to be 'none'\n";
    }
  }

  if( $add_only ) {
    unless( defined($days_added) ) {
      die "The '-addonly' option is only for use with the '-add' option.\n";
    }
  }

  if( defined($days_added) ) {
    if( defined($specific_days_to_do) ) {
      die "To keep the interworking of the options simple, you are not\n".
	"allowed to specify the '-days' option along with the '-add' ".
	"option.\n";
    }

    $specific_days_to_do = {};  # Start with an empty to-be-processed hash
                                # which dictates "process no days"
    if( !$add_only ) {
      # If the user wants to process the days that get added, then
      # put each day into the to-be-processed hash.
      foreach (@@$days_added) { $specific_days_to_do->{$_} = 0; }
    }

  }


  return( $selection_method, $specific_days_to_do, $redo_days, 
	  $specific_files, $insert,
	  $force_ok, $clear_ds_db, $fully_clear_ds_db, 
	  $days_added, $add_only,
	  $db_timeout, $fixLocks);

}

sub usage {
  ProdUtils::error
    ("","usage:",$0,
     "Optional arguments:\n".
     "  -cname <alternate config name> altername config name (see",
     "          \$SOCHOME/etc/ProdConfigs.config for the possible configs",
     "  -n   no action mode",
     "  -v   verbose mode",
     "  -d   very verbose mode",
     "  -timeout  <secs>  specify the db timeout for getting a db lock",
     "  -nolockfix        do not fix broken locks in the db when opening",
     "                     db file",
     "  -method <selection method name>  use this new day selection scheme",
     "      (the default is specified in the config file)",
     "  -days <day1 day2...>  only operate on 1 or more days in this list",
     "  -redodays  force the days in the list above to be re-done if".
     "             they are already complete",
     "  -files <file1 file2...> only use these files for the given day",
     "      (you must spoecify exactly one day after the '-days' option)",
     "  -insert  do not replace the whole list of LZ files, just insert the",
     "       files which are selected (they can overwrite existing names or",
     "       insert new names)",
     "  -forceok    even if the LZ list has problems, force it to used as OK",
     "  -nodsclear   do not clear out the Datasets db for this day",
     "  -fulldsclear fully clear out the Datasets db for this day, even if",
     "       the list of LZ files is not complete\n",
     "  -add <day1 day2 day3...>  add these days to the DB (and no others -",
     "       the selection method is changed to 'none' so no other days will",
     "       be added) \n",
     "       EACH DAY ADDED IS ALSO PROCESSED, unless the next option is ",
     "          used",
     "  -addonly  do not process the days which were just added (or any",
     "       other days), only add them (and no others) to the DB");
}


=pod



This program looks in the levelZero repository and selects the levelZero
files needed for a particular mission day.

First, the program determines if it should add any new days to the 
Days Selected database.  This databse keeps track of which days have 
already had their levelZero files selected.
The selection of LZ files is the first step in the FITS processing 
for a particular day, so its important to only select the files for 
a day when you want to process that day.

There are several methods possible for determining when a day should be
added to the list of days to select.  They are:
 one-per-day   - Each calendar day, one more day's files should be selected.
                 With this method, you specify a lag time so that the highest
                 selected mission day should always be kept at a certain 
                 offset from the current mission day.

 one-per-run   - Each time the program is run, it should select another
                 mission day's files.

 none          - The program should not add any days to the databse.


Then the program goes and looks at the whole Days Selected database.
Any day which is in there as incomplete will be processed - i.e., a 
complete set of LZ files will be sought for this day.

All the LZ files for the given day are found in the LZ repository. If the
files constitue an OK set, then they are recorded in the LevelZero 
database for this same day, and the day is marked as complete in 
Days Selected db.  Finally, any entries relating to these LZ files
in the Datasets db are cleared out, so that xff will not be able to
run on this day until the LZ files are fully processed into datasets.
For the usual case, the Datasets day will not even exist yet.

Other options:

You can force the program to only work on certain days using the
'-days <day1 day2...>' option.  These days must be already in the Days
Selected databse. If the day is already complete, then you will also 
need to use the '-redodays' option to tell the program its OK to process 
the day, even though its already complete.

If a set of LZ files is not complete, then the day will remain in its
incomplete status on the Day Selected db, and no changes will be made
to other databases - no LZ files added or removed from the LevelZero db,
no datasets removed from the Datasets db.

You can force a day's levelZero files to be considered ok using the
'-forceok' option.   The files found will be used to completely
replace any existing list of LZ files for this day in the LevelZero db.

If you want files to be added to the LevelZero list db rather than 
replacing the list, then the '-insert' option will do this.

If you want to use only specific files for a given day, you can use
the '-files <file1 file2...>' option.  If you do specify files
in this way, then you must also use the '-days' option with just one
day - obviously the day whose files you are listing.  Then, any
valid LZ files for the given day will be included.

If you want some specific days to be added to the to-be-selected DB, you 
can use the '-add' option, with a list of one or more days after it.
No other days will be added (the selection method is forcibly set to 'none'.)
The '-method' option cannot be used with this option.
If you want those days to be processed, then use the '-doadded' option,
which sets the list of days to be processed to be the same as the list of 
days which were added.  The '-days' option cannot be used at the same time
as the '-doadded' option.

Finally, there are two options to control how the Datasets databse is
cleared out.  You can specify '-nodsclear' if you do not want any entries
deleted from the Datasets db.  If you want all entries (for each each day
processed) cleared from the Datasets db, then the '-fulldsclear' option
will do this.  The latter option is useful when you want to replace a 
single apid's LZ file, but you also want all the Datasets to be 
regenerated for the given day(s).


=cut


@

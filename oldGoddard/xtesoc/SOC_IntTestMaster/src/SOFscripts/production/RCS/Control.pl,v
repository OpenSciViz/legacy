head	1.4;
access;
symbols;
locks; strict;
comment	@# @;


1.4
date	2000.12.18.13.50.50;	author jonv;	state Exp;
branches;
next	1.3;

1.3
date	2000.12.15.20.30.33;	author jonv;	state Exp;
branches;
next	1.2;

1.2
date	2000.12.12.18.42.51;	author jonv;	state Exp;
branches;
next	1.1;

1.1
date	2000.12.12.14.29.18;	author jonv;	state Exp;
branches;
next	;


desc
@main driver for the GUI interfaces of the processing pipeline
@


1.4
log
@added the help screen
@
text
@#!/usr/local/bin/perl -w
# ===========================================================================
# Filename    : Control.pl
# Subsystem   : SOFscripts/production
# Programmer  : Jon Vandegriff, Emergent IT
# Description : 
#
#  A central location for providing access to all Production pipeline
#  GUI interfaces.
#
# RCS: $Id: Control.pl,v 1.3 2000/12/15 20:30:33 jonv Exp $
# ===========================================================================


use Tk;

use ProdUtils;
use Receive;
use ViewSelect;
use RedflagView;
use ShowDB;

use strict;

my $main;
my $parms;
my( $cname, $db_timeout, $fixLocks );

{
  $parms = ProdUtils::get_parms(\@@ARGV);

  ( $cname, $db_timeout, $fixLocks ) = parse_args($parms, @@ARGV);

  $main = new MainWindow();

  my $menu_frame = $main->Frame(-relief=>'groove', -borderwidth=>1)
    ->pack(-expand=>1, -fill=>'x', -anchor=>'n');
  my $fileButton = $menu_frame->Menubutton(-text => "File", -tearoff=>0)
    ->pack(side => 'left');
  my $optsButton = $menu_frame->Menubutton(-text => "Options", -tearoff=>0)
    ->pack(side => 'left');
  my $toolsButton = $menu_frame->Menubutton(-text => "Tools", -tearoff=>0)
    ->pack(side => 'left');
  my $helpButton = $menu_frame->Menubutton(-text => "Help", -tearoff=>0)
    ->pack(side => 'right');

  $fileButton->command(-label=>"Quit", -command=>sub{exit;});


  $optsButton->checkbutton(-label=>'Fix locks when opening DB',
			   -variable=> \$fixLocks);

  my $timeout_menu = $optsButton->menu->Menu(-tearoff=>0);
  foreach ( qw(10 15 30 60 120 300) ) {
    $timeout_menu->radiobutton(-label => "$_ sec",
			       -variable => \$db_timeout,
			       -value => $_ );
  }
  $optsButton->cascade( -label=>"DB Timeout Values" );
  $optsButton->entryconfigure("DB Timeout Values", -menu=>$timeout_menu);


  $toolsButton->command(-label=>"Database Overview",
			-command=>\&display_db_overview);

  $toolsButton->command(-label=>"Redflag and Obsid Problems",
			-command=>\&display_redflag_window);

  $toolsButton->command(-label=>"Change obscats",
			-command=>\&run_new_obscat_program);

  $helpButton->command(-label=>"Info", -command=>\&show_help);

  my $frame1 = $main->Frame()->pack(-side=>'top', -anchor=>'n',-expand=>0);
  $frame1->Label(-text=>"These are the GUI interfaces for various ".
			"processing steps:")
    ->pack(-side=>'top', -pady=>10, -padx=>10);
  my $bframe = $frame1->Frame()->pack(-side=>'top', -anchor=>'nw', -expand=>0);

  $bframe->Button(-text=>"Recieve LZ", -command=>\&display_receive_window)
    ->grid(-row=>0, -column=>0, -pady=>10, -padx=>10);
  $bframe->Label(-text=>"Move LZ files among various LZ directories ".
		 "(new, holding and repository")
    ->grid(-row=>0, -column=>1, -pady=>10, -padx=>10);


  $bframe->Button(-text=>"Select LZ", -command=>\&display_select_window)
    ->grid(-row=>1, -column=>0, -pady=>10, -padx=>10);
  $bframe->Label(-text=>"Choose LZ files for specific days.  Add/Remove ".
		 "days from Days_selected DB")
    ->grid(-row=>1, -column=>1, -pady=>10, -padx=>10);

  MainLoop();

}


sub parse_args {
  my $parms = shift;
  # rest of @@_ is @@ARGV;

  my $local_cname       = "";
  my $local_db_timeout  = $parms->get_scalar("default_db_timeout");
  my $local_fixLocks    = 1;

  my $arg;
  while( defined($arg = shift @@_) ) {
    if( $arg eq "-cname" ) {
      $local_cname = shift @@_;
    } elsif( $arg eq "-timeout" ) {
      $local_db_timeout = shift @@_;
    } elsif( $arg eq "-nolockfix") {
      $local_fixLocks = 0;
    } else {
      usage();
      exit(1);
    }
  }

  return( $local_cname, $local_db_timeout, $local_fixLocks );

}


sub usage {
  ProdUtils::error
    ("","usage:",$0,
     " [-timeout <db timeout in seconds>]",
     " [-cname alt_config_name] ",
     " [-nolockfix]   (do not fix broken locks when opening the DB)");
}


sub display_db_overview {
  ShowDB::show($main, $parms, $db_timeout, $fixLocks);
}


sub display_redflag_window {
  RedflagView::show($main, $parms, $db_timeout, $fixLocks);
}


sub run_new_obscat_program {
  my $cmd = "NewObscat.pl -timeout $db_timeout ";
  if($cname)     { $cmd = "$cmd -cname $cname "; }
  if(!$fixLocks) { $cmd = "$cmd -nolockfix ";    }
  system("$cmd &")
}


sub display_receive_window {
  Receive::show($main, $parms);
}


sub display_select_window {
  ViewSelect::show($main, $parms, $db_timeout, $fixLocks);
}



sub show_help {
  my $lines = [];
  @@$lines = help_lines();
  show_lines("Information about the main Control GUI", $lines);
}


sub show_lines {
  my( $title, $lines ) = @@_;

  my @@split_lines = split(/\n/, join("\n",@@$lines));
  my $num_lines = scalar( @@split_lines );

  my $height = $num_lines + 2;
  if( $height > 30 ) { $height = 30; }

  my $help_top = $main->Toplevel(-title=>$title);
  my $text = $help_top->Scrolled('Text', -scrollbars=>'osoe',
				 -width=>80, -height=>30, -wrap=>'none')
    ->pack();
  $help_top->Button(-text=>'Close', -command=>sub{$help_top->destroy();})
    ->pack(-padx=>100,-pady=>5);
  $text->insert('end', join("\n",@@$lines) );
  $text->configure(-state=>'disabled');
}


sub help_lines {
  return
  ("Help info for the main production processing GUI:",
   "",
   "This interface is really just a launching pad for other interfaces.",
   "The buttons on the main panel provide interfaces for the typical",
   "processing steps. Each button is accompanied with a brief desription",
   "of the functionality provided by the interface it will launch.",
   "",
   "The \"Tools\" menu provides access to other interfaces, most of which are",
   "for problem cases or special handling.",
   "",
   "    Database Overview   - a visual representation of all the databases",
   "                            in the processing system.",
   "",
   "    Redflag and Obsid Problems - a mechanism for clearing the problems",
   "                            status of obsids which failed processing",
   "                            because of redflags or other problems.",
   "",
   "    Change obscats - an interface for installing a new obscat file for a",
   "                            given mission day AFTER that day has been",
   "                            processed by xff.",
   "",
   "",
   "Please note the settings under the \"Options\" menu.  These are important",
   "because they will affect how the launched programs are started.  The",
   "items on the options menu are the \"fix-locks flag\", and the Database",
   "timeout settting.  When you launch another application, it will most",
   "likely need to open one or more databases to get started.  And in doing",
   "so, it will need values for the above two settings.  So if you want the",
   "lock fix flag or the timeout flag set to non-default values, you need to",
   "change them before launching any ther tools or interfaces.",
   "");
}
@


1.3
log
@added ShowDB, the database overview GUI to the Tools menu
@
text
@d11 1
a11 1
# RCS: $Id: Control.pl,v 1.2 2000/12/12 18:42:51 jonv Exp $
d72 2
d159 64
@


1.2
log
@extracted subroutine calls into named subroutines
@
text
@d11 1
a11 1
# RCS: $Id: Control.pl,v 1.1 2000/12/12 14:29:18 jonv Exp $
d21 1
d37 1
a37 1
    ->pack(-expand=>1, -fill=>'x');
d63 3
d72 1
a72 1
  my $frame1 = $main->Frame()->pack();
d76 1
a76 1
  my $bframe = $frame1->Frame()->pack(-side=>'top', -anchor=>'w');
d131 4
@


1.1
log
@Initial revision
@
text
@d11 1
a11 1
# RCS: $Id$
d24 2
a25 2
my $open_items = {};

d29 1
a29 1
  my $parms = ProdUtils::get_parms(\@@ARGV);
d33 1
a33 1
  my $main = new MainWindow();
d62 2
a64 4

  $toolsButton->command(-label=>"Redflag and Obsid Problems",
			-command=>sub{RedflagView::show($main, $parms,
						     $db_timeout, $fixLocks)});
d66 1
a66 6
			-command=>sub{
			  my $cmd = "NewObscat.pl -timeout $db_timeout ";
			  if($cname)     { $cmd = "$cmd -cname $cname "; }
			  if(!$fixLocks) { $cmd = "$cmd -nolockfix ";    }
			  system("$cmd &")
			} );
d74 1
a74 2
  $bframe->Button(-text=>"Recieve",
		  -command=>sub{Receive::show($main, $parms);})
d81 1
a81 3
  $bframe->Button(-text=>"Select",
		  -command=>sub{ViewSelect::show($main, $parms,
						 $db_timeout, $fixLocks);})
d125 24
@

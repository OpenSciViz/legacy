head	1.1;
access;
symbols;
locks; strict;
comment	@# @;


1.1
date	2000.12.12.14.29.18;	author jonv;	state Exp;
branches;
next	;


desc
@runs dmIndex on Datasets which are ready to be processed (this
readiness determination is made by the boundary merging program
@


1.1
log
@Initial revision
@
text
@#!/usr/local/bin/perl -w

# ===========================================================================
# Filename    : MakeDatasets.pl
# Subsystem   : SOFscripts/production
# Programmer  : Jon Vandegriff, Emergent IT
# Description : 
#
#  Completes datasets and records the status in the Datasets DB.
#  Note that there is no other DB that it sends things to for processing.
#  There must be a complete set of keys before a dataset day is considered
#  complete.  The complete key set includes a list of required apids, plus
#  all the EAs (EA0 through EA7). The EAs are added to the DB by the 
#  boundary merging process, or else by hand with the '-addeas' option,
#  which is a unique option to this dataset preparation program.
#
#
# RCS: $Id$
# ===========================================================================


use ProdDB;
use ProdUtils qw( $act $verb $VERB $DEBUG);

use ParameterManager;

use strict;

{

  my $prog = "MakeDatasets.pl";
  my $parms = ProdUtils::get_parms(\@@ARGV);

  my( $specific_keys_to_do, $specific_subkeys, $redo,
      $force_ok, $db_timeout, $fixLocks, $preDelete, $procVerb, 
      $add_eas_to_ds_list, $add_eas_only ) =
    ProdUtils::parse_args($prog, $parms, @@ARGV);

  my $doneDS_db   = ProdDB->make( ProdDB::Datasets(),      $db_timeout);

  my $db_list    = [ $doneDS_db ];

  my $primary_db = $doneDS_db;

  my $lock_list_routine = \&ProdUtils::lock_just_this_days_keys;

  my $decide_about_subkey_routine = sub{ return 1; };

  my $processing_routine = \&ProdUtils::do_dmIndex;

  my $cant_connect_message_routine = \&ProdUtils::connection_error_msg;

  my $processed_ok_routine = \&update_done_ds_database;

  if( $add_eas_to_ds_list ) {
    if( $specific_keys_to_do ) {
      unless( possibly_add_ea_keys_to_days($doneDS_db, $specific_keys_to_do,
					   $db_timeout, $fixLocks) )
      {
	die "Problems while looking into adding EA's to the requested days: ",
	  join(" ", keys %$specific_keys_to_do),"\n";
      }
    } else {
      die "If you specify the '-addeas' option, you must also provide a ".
	"list of days after the '-keys' option.\n";
    }
  }

  if( $add_eas_only ) {
    print "Finished with EA adding procedure - exiting due to user's ".
      "'-addeasonly' option\n" if $verb;
    exit(0);
  }

  ProdUtils::process_pending_entries
    ($prog, $db_list, $primary_db,
     $specific_keys_to_do, $specific_subkeys, $redo,
     $force_ok, $db_timeout, $fixLocks, $preDelete, $procVerb,
     $lock_list_routine,
     $decide_about_subkey_routine,
     $processing_routine,
     $cant_connect_message_routine,
     $processed_ok_routine);
}


sub update_done_ds_database {
  my($prog, $day, $subkey, $db_list, $lock_list, $is_complete, @@return_list )
    = @@_;

  my $doneDS_db  = $db_list->[0];

  $doneDS_db->setSubKeyValue($day, $subkey, $is_complete);
  print $doneDS_db->name."->setSubKeyValue($day, $subkey, $is_complete)\n"
    if $verb;

}


sub possibly_add_ea_keys_to_days {
  my( $doneDS_db, $specific_days_to_do, $db_timeout, $fixLocks ) = @@_;

  my $ok = 1;  

  # Everything is OK if:
  #  the day is unlocked
  #  the day has at least one missing EA

  if( $doneDS_db->connect($db_timeout, $fixLocks) ) {

    # First make sure all the days are unlocked

    my $set_of_keys = {};
    foreach my $day (sort keys %$specific_days_to_do) {
      $set_of_keys->{$day}=undef;
    }

    # The lock list has one database with a list of keys.
    my $lock_list = [ { "db"   => $doneDS_db,
			"keys" => $set_of_keys } ];

    if( ProdUtils::lock_all($lock_list) ) {

      # Now see if each day has at least one missing EA.
      # Keep track of the list of EA's which need to be 
      # added for each day, so that after the check is done,
      # the info on what to add is available.

      my @@EA_list = qw( EA0 EA1 EA2 EA3 EA4 EA5 EA6 EA7 );

      my %EAs_to_add_for_given_day = ();
      my $any_days_which_have_all_EAs = 0;

      foreach my $day (sort keys %$specific_days_to_do) {

	my @@missing_EAs = ();
	foreach my $ea (@@EA_list) {
	  unless( $doneDS_db->doesSubKeyExist($day, $ea) ) {
	    push( @@missing_EAs, $ea );
	  }
	}
	if( scalar(@@missing_EAs) == 0) {
	  ProdUtils::error("day '$day' has all EA's present already!");
	  $any_days_which_have_all_EAs = 1;
	} else {
	  $EAs_to_add_for_given_day{$day} = [ @@missing_EAs ];
	}

      }

      if( $any_days_which_have_all_EAs ) {
	ProdUtils::error("Can't add EA's to requested days becuase at ".
	  "least one of the requested days","already has all its EAs - ".
	  "please submit special requests separately.");
	$ok = 0;
      } else {

	# Everything seems OK - all days are locked - 
	# 
	foreach my $day (sort keys %$specific_days_to_do) {
	  foreach my $ea ( @@{$EAs_to_add_for_given_day{$day}} ) {
	    if( $act ) {
	      $doneDS_db->setSubKeyValue($day, $ea, 0);
	      print "Added '$ea' to the datasets for day '$day'\n" if $verb;
	    } else {
	      print "Would add '$ea' to the datasets for day '$day'\n"if $verb;
	    }
	  }
	}
      }

      # Unlock the locked days
      ProdUtils::unlock_all( $lock_list );

    } else {
      ProdUtils::error("Could not lock all requsted days in the '".
		       $doneDS_db->name."' DB.");
      $ok = 0;
    }

    # Close the opened DB.
    $doneDS_db->disconnect();

  } else {
    ProdUtils::error("Could not connect to '".$doneDS_db->name."' DB.");
    $ok = 0;
  }

}



@

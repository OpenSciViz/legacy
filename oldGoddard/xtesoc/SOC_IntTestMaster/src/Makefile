# ############################################################################
# Filename    : Makefile ($SOCHOME/src)
# Subsystem   : SOC
# Programmer  : Randall D. Barnette, Hughes STX
# Description : This is the makefile that traverses the entire set of
#               SOC source code. It is commonly called the "monster make"
#               file.
#               It is used to make elements of the SOF described by SUBSYSTEMS
#
# RCS: $Id: Makefile,v 5.2 1996/02/01 20:38:31 soccm Exp $
#
# Use default GNU make built-in rules, which can be examined with the
# command: "make -p -f /dev/null".
#
# ############################################################################
 
# Try to ignore errors
.IGNORE:

# Export everything defined here
.EXPORT_ALL_VARIABLES:

#
# Set the path to the compiler's custom include files. This is for use
# in the depend target.
#
ifndef CCROOT_I

  DOMAIN_ARCH:=$(shell domainname)_$(ARCH)
  CCROOT_I:=/usr/local/include

  #set up CCROOT_I based on system
  CENTERLINE_INC:=CenterLine/clc++/sparc-sunos4/incl
  CENTERLINE_INC5:=CenterLine/clc++/sparc-solaris2/incl

  ifeq ($(DOMAIN_ARCH),space.mit.edu_mips)
    CCROOT_I:=/usr/local/USL-CC/incl
  endif
  ifeq ($(DOMAIN_ARCH),space.mit.edu_sun4)
    CCROOT_I:=/xte/vendor/$(CENTERLINE_INC) 
  endif
  ifeq ($(DOMAIN_ARCH),xtenet_SunOS4)
    CCROOT_I:=/socsoft/$(CENTERLINE_INC) 
  endif
  ifeq ($(DOMAIN_ARCH),_SunOS5)
    CCROOT_I:=/socsoft/$(CENTERLINE_INC5) 
  endif
endif

#
# Set the expected ENVIRONMENT variables in case the user didn't
#
ifndef ARCH
  ARCH := $(shell uname)$(shell uname -r | cut -c1)
endif

ifndef SOCHOME
  SOCHOME := /sochome
endif

ifndef SOCLOCAL
  SOCLOCAL := /socsoft/local
endif

ifndef SOCOPS
  SOCOPS := /socops
endif

ifndef SOCFITS
  SOCFITS := $(SOCOPS)/FITS
endif

ifndef SPIKEROOT
  SPIKEROOT := $(SOCOPS)/spikeroot/$(ARCH)
endif
 
# Where are we?
cwd := $(shell pwd)

# Where is the master copy?
#master:=/local/SOCRCS/src
master:=/socsoft/SOC_IntTestMaster/src

#
# This is the list of SOC Subsystems to be make'd
#
SUBSYSTEMS:=Utilities PktCCSDS DataManagement Ingest MIT HEXTE \
CommandGeneration MissionPlanning HealthnSafety SM2 MM2

#Simulators
 
# Some compiler items.
CC = CC
CXX=$(CC)

#
# What to do to each subsystem ($@ is the target of each make
# target in which it is used)
#
FORRULE   =$(MAKE) -C $(dir) $@;
DEPENDRULE=$(MAKE) -C $(dir) CCROOT_I=$(CCROOT_I) $@;
INITRULE  =echo ""; srcdir=$(dir:./%=%); echo $(master)/$$srcdir":"; find $(master)/$$srcdir -type d -name RCS -prune -exec ./linkRCS $(cwd) $(master) {} \; ;

#
# The directories under SOCHOME required for SOC installation
#

hdrs-dirs = $(SOCHOME)/include
libs-dirs = $(SOCHOME)/lib $(SPIKEROOT)
install-dirs = $(SOCHOME)/bin $(SOCHOME)/qfbin $(SOCHOME)/etc $(SOCHOME)/man $(SOCHOME)/doc $(SOCOPS) $(SOCFITS) $(SOCHOME)/test $(SOCHOME)/html
test-dirs = $(SOCHOME)/test

SOCDIRS = $(hdrs-dirs) $(libs-dirs) $(install-dirs) $(test-dirs)

#
# Targets
#

.PHONY: all realclean clean pubhdrs depend publibs install

all: soc_init realclean pubhdrs depend publibs install

$(SOCHOME)/include: FORCE
	mkdir -p $@

$(SOCHOME)/lib: FORCE
	mkdir -p $@/$(ARCH)
	mkdir -p $@/app-defaults
	mkdir -p $@/afm

$(SOCHOME)/bin: FORCE
	mkdir -p $@/$(ARCH)

$(SOCHOME)/etc: FORCE
	mkdir -p $@/FT

$(SOCHOME)/man: FORCE
	mkdir -p $@/man1
	mkdir -p $@/man3
	mkdir -p $@/man5

$(SOCHOME)/html: FORCE
	mkdir -p $@

$(SOCHOME)/test: FORCE
	mkdir -p $@/scripts
	mkdir -p $@/programs
	mkdir -p $@/bin/$(ARCH)
	mkdir -p $@/inputs
	mkdir -p $@/outputs

$(SOCHOME)/doc: FORCE
	mkdir -p $@

$(SOCOPS): FORCE
	mkdir -p $@/asm
	mkdir -p $@/command/ACS
	mkdir -p $@/command/EDS
	mkdir -p $@/command/EDS/EA1
	mkdir -p $@/command/EDS/EA2
	mkdir -p $@/command/EDS/EA3
	mkdir -p $@/command/EDS/EA4
	mkdir -p $@/command/EDS/EA5
	mkdir -p $@/command/EDS/EA6
	mkdir -p $@/command/EDS/EA7
	mkdir -p $@/command/EDS/EA8
	mkdir -p $@/command/EDS/PRAMA
	mkdir -p $@/command/EDS/PRAMB
	mkdir -p $@/command/EDS/dwells
	mkdir -p $@/command/EVENT
	mkdir -p $@/command/PCA
	mkdir -p $@/command/HEXTE
	mkdir -p $@/command/configs
	mkdir -p $@/command/configs/eds
	mkdir -p $@/command/configs/pca
	mkdir -p $@/command/configs/hexte
	mkdir -p $@/command/configs/acs
	mkdir -p $@/command/configs/obs
	mkdir -p $@/command/configs/EDS
	mkdir -p $@/command/configs/PCA
	mkdir -p $@/command/configs/HEXTE
	mkdir -p $@/command/configs/ACS
	mkdir -p $@/command/configs/EVENT
	mkdir -p $@/command/configs/pseudo
	mkdir -p $@/command/macros
	mkdir -p $@/command/EOR
	mkdir -p $@/command/mocdeliver
	mkdir -p $@/command/mocreceive
	mkdir -p $@/command/mocreal
	mkdir -p $@/command/realtime
	mkdir -p $@/etc
	mkdir -p $@/ingest/production
	mkdir -p $@/ingest/quicklook
	mkdir -p $@/ingest/realtime
	mkdir -p $@/levelZero
	mkdir -p $@/planning/gofreports
	mkdir -p $@/planning/observations
	mkdir -p $@/planning/proposed
	mkdir -p $@/planning/spt
	mkdir -p $@/planning/unscheduled
	mkdir -p $@/log
	mkdir -p $@/stats
	mkdir -p $@/timeOrdered/production
	mkdir -p $@/timeOrdered/quicklook
	mkdir -p $@/timeOrdered/realtime
	mkdir -p $@/tmp

$(SPIKEROOT): $(SOCOPS)
	mkdir -p $@
	mkdir -p $@/event-files
	mkdir -p $@/event-files/xte
	mkdir -p $@/fdf
	mkdir -p $@/lib
	mkdir -p $@/obs
	mkdir -p $@/report
	mkdir -p $@/sched
	mkdir -p $@/source
	mkdir -p $@/temp

FORCE:

soc_init: linkRCS dirs
	@$(foreach dir,$(SUBSYSTEMS),$(INITRULE))

#
# SOC targets
#
pubhdrs: $(hdrs-dirs)
	@$(foreach dir,$(SUBSYSTEMS),$(FORRULE))

publibs: $(libs-dirs)
	@$(foreach dir,$(SUBSYSTEMS),$(MAKE) -C $(dir) $@;)

install:$(install-dirs)
	@$(foreach dir,$(SUBSYSTEMS),$(FORRULE))

depend:
	@$(foreach dir,$(SUBSYSTEMS),$(DEPENDRULE))

clean:
	@$(foreach dir,$(SUBSYSTEMS),$(FORRULE))

realclean:
	@$(foreach dir,$(SUBSYSTEMS),$(FORRULE))
	ls -R $(SUBSYSTEMS)

install-test: $(test-dirs)
	@$(foreach dir,$(SUBSYSTEMS),$(FORRULE))

#
# Non-SOC targets
#

fresh:
	$(RM) -r $(SOCHOME)/bin
	$(RM) -r $(SOCHOME)/lib
	$(RM) -r $(SOCHOME)/include
	$(RM) -r $(SOCHOME)/etc
	$(RM) -r $(SOCHOME)/man
	$(RM) -r $(SOCHOME)/doc
	$(RM) -r $(SOCHOME)/socops
	$(RM) -r $(SOCHOME)/test
	-rcsclean

startover:
	$(foreach dir,$(SUBSYSTEMS),$(RM) -r $(dir);)
	$(RM) -r XFF

printenv:
	@echo "CXX        = $(CXX)"
	@echo "COFLAGS    = $(COFLAGS)"
	@echo "ARCH       = $(ARCH)"
	@echo "SOCHOME    = $(SOCHOME)"
	@echo "SOCLOCAL   = $(SOCLOCAL)"
	@echo "SOCOPS     = $(SOCOPS)"
	@echo "SOCFITS    = $(SOCFITS)"
	@echo "CCROOT_I   = $(CCROOT_I)"
	@echo "SUBSYSTEMS = $(SUBSYSTEMS)"

list_subsystems:
	@$(foreach dir,$(SUBSYSTEMS), echo $(dir) ;)

dirs: $(SOCDIRS)


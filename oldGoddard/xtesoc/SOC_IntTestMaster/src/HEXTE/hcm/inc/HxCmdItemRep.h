// File: HxCmdItemRep.h
// Programmer: Matthew Marlowe
// Description: Encapsulation of a real item of configuration!
//
// RCS Info: $Id: HxCmdItemRep.h,v 1.1 1996/07/26 18:26:20 socdev Exp $
//

#ifndef _HxCmdItemRep_
#define _HxCmdItemRep_



#include <iostream.h>
#include <HxTbItem.h>
#include <HexDefs.h>
#include <hx_ids.h>
#include <rw/hashdict.h>
#include <rw/collect.h>
#include <rw/pstream.h>
#include <rw/vstream.h>


#define HxCmdItemRep_ID 313

class HxCmdItemRep : public RWCollectable

{
 


    RWDECLARE_COLLECTABLE(HxCmdItemRep)
 


public:



    HxCmdItemRep( int cluster=0 );
    HxCmdItemRep( char *mnemonic, int cluster=0 );
    HxCmdItemRep( const HxCmdItemRep & );



    // get/set generic for values  (at this point all values are USI's! )
    
    int setValue( unsigned int );
    inline unsigned int getValue(              ) const;


    // retrieval of name and TbItem
    const char * getName() const;
    const HxTbItem & getTbItem() const;
    const HxTbItem & getTbItem( const char * ) const;

    // get/set flags  (flags are unsigned shorts )

    int           setFlag( unsigned short );
    inline unsigned short getFlag(                ) const;

    // get/set cluster_id 
    int getClusterId() const;
    void setClusterId();

    // retrieval methods
    unsigned short packSeqNum() const;
    unsigned short packSeqNum( const char * ) const;
    unsigned short offsetBytes() const;
    unsigned short offsetBytes( const char * ) const;
    unsigned short numBytes() const;
    unsigned short numBytes( const char * ) const;
    unsigned short numBits() const;
    unsigned short numBits( const char * ) const;
    unsigned short offsetBits() const;
    unsigned short offsetBits( const char * ) const;
    unsigned short multiplicity() const;
    unsigned short multiplicity( const char *) const;
    unsigned short subcomCycle() const;
    unsigned short subcomCycle( const char *) const;
    unsigned short seqNumInCycle() const;
    unsigned short seqNumInCycle( const char * ) const;
    unsigned short associatedCmd() const;
    unsigned short associatedCmd( const char * ) const;
    unsigned short conversionAvail() const;
    unsigned short conversionAvail( const char * ) const;
    unsigned short configItem() const;
    unsigned short configItem( const char * ) const;
    unsigned short inTelem() const;
    unsigned short inTelem( const char * ) const;
    unsigned short defaultValue() const;
    unsigned short defaultValue( const char * ) const;
    unsigned short digitalTolerance() const;
    unsigned short digitalTolerance( const char * ) const;
    unsigned short timeTolerance() const;
    unsigned short timeTolerance( const char * ) const;

    float lowerRedLim() const;
    float lowerRedLim( const char * ) const;
    float lowerYellowLim() const;
    float lowerYellowLim( const char * ) const;
    float upperYellowLim() const;
    float upperYellowLim( const char *) const;
    float upperRedLim() const;
    float upperRedLim( const char *) const;
    const char * mnem() const;
    const char * mnem( const char * ) const;
    const char * PEnumber() const;
    const char * PEnumber( const char * ) const;
    const char * descript() const;
    const char * descript( const char * ) const;
    const char * units() const;
    const char * units( const char * ) const;

    // inheritance
    void inherit( const HxCmdItemRep &des , const HxCmdItemRep &pred );

    // we print only to ostream's now

    void print(      ostream & ostr ) const;
    void printShort( ostream & ostr ) const;
    void printLong( ostream & ostr ) const;

    const RWHashDictionary & getTbItemBag() const;
    RWHashDictionary & getRawTbItemBag() ;
    
    //
    // setZeroValues sets value, flag, and mnem to zero equivs
    //
    void setZeroValues();

    // 
    // setDefaultValues sets value and flag to table defaults
    //
    void setDefaultValues();
  
    // 
    // Rogue Wave
    //

    RWspace binaryStoreSize( void ) const;
    void saveGuts( RWFile & f ) const;
    void saveGuts( RWvostream & ostr ) const;
    void restoreGuts( RWFile & f ) ;
    void restoreGuts( RWvistream & istr ) ;

    // 
    // operators
    //
    int operator==( const HxCmdItemRep & ) const;
    HxCmdItemRep & operator=( const HxCmdItemRep & ) ;

    // destructor does nothing
    ~HxCmdItemRep();

private:
    
    // Private Methods:
    void destroyTable();
    void initCmdItem();
    void initItemTable();


    // Private Data:
    unsigned int            value;     // 0 - MAXINT
    unsigned short          flag;      // DONTCARE, DONTKNOW, CARE, KNOW

    static unsigned int     numOfInstances; // 0= no instances
    static RWHashDictionary *TbItemBag; // key is mnem, value is HxTbItem

    char                    ourName[SIZEMNEM]; // mnemonic

    unsigned int            clus_id;   //  0=CLUSA, 1=CLUSB

    unsigned int            _rc;

    friend class HxCmdItem;
};

  
inline unsigned int
HxCmdItemRep::getValue() const
{
    return( value );
}


inline unsigned short
HxCmdItemRep::getFlag() const
{
    return( flag );
}

#endif




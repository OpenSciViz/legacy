head	1.3;
access
	soccm;
symbols;
locks;
comment	@//@;


1.3
date	96.10.23.14.31.28;	author soccm;	state Exp;
branches;
next	1.2;

1.2
date	96.07.26.18.57.28;	author mmarlowe;	state Exp;
branches;
next	1.1;

1.1
date	96.07.26.18.25.17;	author socdev;	state Exp;
branches;
next	;


desc
@@


1.3
log
@Removed stupid log messages!
@
text
@// File: HxCmdItemRep.C
// Programmer: Matthew Marlowe
// Description: encaps of real item of config
//
// $Id: HxCmdItemRep.C,v 1.2 1996/07/26 18:57:28 mmarlowe Exp $
//
// marlowe@@ucsd.edu  619-534-1254
//

// Version Number was added by Shalom to deal with detecting storage corruption
#define  VER_NUM   120   // version number (increment by 10)



// System Include Files
#include <string.h>
#include <stdlib.h>
#include <iostream.h>
#include <unistd.h>
#include <assert.h>

// Rogue Wave Include Files
#include <rw/rwfile.h>
#include <rw/vstream.h>
#include <rw/collstr.h>
#include <rw/collint.h>
#include <rw/collect.h>
#include <rw/pstream.h>
#include <rw/hashdict.h>

// Library Include Files
#include <hx_ids.h>
#include <HexDefs.h>
#include <getTable.h>
#include <HxTbItem.h>
#include <HxCmdItemRep.h>
#include <hcmDebug.h>



RWDEFINE_COLLECTABLE( HxCmdItemRep, HxCmdItemRep_ID )

unsigned int HxCmdItemRep::numOfInstances = 0;
RWHashDictionary * HxCmdItemRep::TbItemBag = NULL;


void
HxCmdItemRep::initItemTable()
{

    TbItemBag = new RWHashDictionary(700);

#ifdef DEBUG1
    cout << "HCM: Recreating table." << endl;
#endif

  //
  // We read each line, get key and value, and insert
  //

    char * cgOnly = getenv("HCM_CGONLY");
    
    
    RWCollectableString * key;
    char newName[SIZEMNEM + 5 ];
    char buffer[LINESIZE];
    RWFile *fp = openFile("HkItem.table");

    if (fp == NULL ) {
	cerr << "Unable to open datafile." << endl;
	exit(1);
    }

    HxTbItem * newEntry = NULL;
    while ( readLine( *fp, buffer) == 1 ) {
	if ( (*buffer != '/') && (strlen(buffer) > 10 ) ) {

#ifdef DEBUG4
	    cout << "Line:" << buffer << endl;
#endif

	    newEntry = new HxTbItem( buffer );

#ifdef DEBUG3
	    cout << "inserting " << newEntry->mnem() << endl;
#endif

	    key =  new RWCollectableString( newEntry->mnem() );
	    key->toUpper();
	
	    if ( cgOnly != NULL ) {
	      if (newEntry->configItem() == 0 ) {
		//cerr << "Skipping " << *key << endl;
		delete(newEntry);
		delete(key);
	      }
	      else {
		TbItemBag->insertKeyAndValue( key, newEntry );
	      }
	    } else {
	      TbItemBag->insertKeyAndValue( key, newEntry );
	    }

	}

	if ( fp->Eof() ) break;
    }
    
    fp->Flush();
    delete(fp);

}

HxCmdItemRep::HxCmdItemRep( int cluster )
  :clus_id( cluster ), _rc(1)
{

  hcmAssert( (cluster == 0) || (cluster == 1), HCM_BAD_CLUS_ID ); 

    if (++numOfInstances == 1)
	initItemTable();

    setZeroValues();

}

HxCmdItemRep::HxCmdItemRep( char * itemName, int cluster )
  :clus_id( cluster ), _rc(1)
{

  hcmAssert( (cluster == 0) || (cluster == 1), HCM_BAD_CLUS_ID );
  hcmAssert( itemName != NULL, HCM_NULL_PTR, "HxCmdItemRep Constructor-itemName argument." );
    

    // Algorithm: 
    //
    // if first instance, read table and initialize TbItemBag.
    // setName to itemName.
    // setDefaultValues().
    //

    if (++numOfInstances == 1)
	initItemTable();

    strcpy( ourName, itemName );
    setDefaultValues();

}

HxCmdItemRep::HxCmdItemRep( const HxCmdItemRep & source )
  : _rc(1)
{

    numOfInstances++;  // This is a copy CONSTRUCTOR! 

    clus_id = source.getClusterId();
    strcpy( ourName, source.getName() );
    value = source.getValue();
    flag  = source.getFlag();


}

void
HxCmdItemRep::setZeroValues()
{

    strcpy( ourName, "JOHN-DOE" );
    value = 0;
    flag  = DONTCARE;

}

void
HxCmdItemRep::setDefaultValues()
{

    const HxTbItem & ourTbItem = getTbItem();

    if (clus_id == 0 ) value = ourTbItem.clusADefaultValue();
    else value = ourTbItem.clusBDefaultValue();

    flag  = DONTCARE;

}

const HxTbItem & 
HxCmdItemRep::getTbItem() const
{
    
    
  RWCollectableString match( getName() );
  match.toUpper();
  
  const HxTbItem * retTbItem = (HxTbItem *) TbItemBag->findValue( &match);
  hcmAssert( retTbItem != NULL, HCM_BAD_MNEM, "HxCmdItemRep name is not a valid mnem - failed during setting of default values." );
  
  return ( (const HxTbItem &) *retTbItem );
    
}

const HxTbItem &
HxCmdItemRep::getTbItem( const char * mnem ) const
{

  hcmAssert( mnem != NULL, HCM_NULL_PTR );

  RWCollectableString match( mnem );
  match.toUpper();
  
  const HxTbItem * retTbItem = (HxTbItem * ) TbItemBag->findValue( &match) ;
  
  hcmAssert( retTbItem != NULL, HCM_BAD_MNEM, "Bad mnem passed to HxCmdItemRep::getTbItem( const char * mnem )" );

  return( (const HxTbItem &) *retTbItem );

}


void
HxCmdItemRep::destroyTable()
{

//    RWHashDictionaryIterator next( *TbItemBag );
//    while (next() ) {
//	RWCollectableString * key = (RWCollectableString *) next.key();
//	TbItemBag.removeAndDestroy( key );
//    }
//    TbItemBag.clear();

}


//
// This method returns the mnemonic associated with the 
// current instance of HxCmdItemRep
//
const char *
HxCmdItemRep::getName() const
{

    return( ourName );

}


//
// This is the method used to modify the value
// of an item of configuration.  A side affect
// is that the flag gets set for that item.
//

int
HxCmdItemRep::setValue( unsigned int val )
{
    
    value = val;
    setFlag( KNOW ); // setting a value triggers change to care status 
    return( val );

}



int
HxCmdItemRep::setFlag( unsigned short val )
{

    flag = val;
    return(flag);

}



void
HxCmdItemRep::print( ostream & ostr ) const
{
    ostr << "\t" << ourName << " = " << value << ", flag= " << flag << endl;
}

void
HxCmdItemRep::printShort( ostream & ostr ) const
{
    ostr << "\t" << ourName << " = " << value << ", flag= " << flag << endl;

}

void
HxCmdItemRep::printLong( ostream & ostr ) const
{
 
    print( ostr );
    const HxTbItem & ourTbItem = getTbItem();
    ourTbItem.printLong( ostr );

}

HxCmdItemRep::~HxCmdItemRep()
{
   
//     if (--numOfInstances == 0 ) {
// 	cout << "destroying table" << endl;
// 	destroyTable();
//     }

}



// 
// inherit was added to support command generation.  In this case,
// one inherits the values and flags from another HxCmdItemRep, except
// if the flag is set for one of your values - in which case you
// keep yours.
//
void
HxCmdItemRep::inherit( const HxCmdItemRep & des, const HxCmdItemRep & pred )
{
    //
    // dflag is flag of desired configuration item
    // pflag is flag of predicted configuration item
    //

    if ( (des == NULL) || (pred == NULL ) )
	cerr << "Error: HxCmdItemRep::inherit was given a null HxCmdItemRep" << endl;
    
    static unsigned short dflag = 0;
    static unsigned short pflag = 0;

    dflag = des.getFlag();
    pflag = pred.getFlag();

    if ( dflag == CARE ) {

	flag  = CARE;
	value = des.getValue();

    } else {

	flag  = pflag;
	value = pred.getValue();

    } /* end of if-else */


}


//
// binaryStoreSize is a method required by RogueWave
//

RWspace
HxCmdItemRep::binaryStoreSize( ) const
{
    //
    // Assuming storage of only care values, decision to store
    // is assumed to be done at higher levels
    //
    // Only mnemonic and value are saved, all other data
    // are expected to be recreated from table
    //

    RWspace sum = 0;
    
    // we save our name as a RWCollectableString, so lets querry that for size

    sum += SIZEMNEM;

    // Than we save our value as an unsigned int
    sum += sizeof( unsigned int );

    // Than we save the flag as an unsigned short
    sum += sizeof( unsigned short );

    return (sum) ;

}


void
HxCmdItemRep::saveGuts( RWFile & f ) const
{
     for ( int i=0; i<SIZEMNEM; i++ ) 
 	f.Write( ourName[i] );
  
    f.Write( value );
    f.Write( flag ); 
}


//
// Shalom added little safety regions when saving our
// data so we can tell if we or someone else overwrite 
// it.
//

void 
HxCmdItemRep::saveGuts( RWvostream & ostr ) const
{ 
    
    int  version = VER_NUM;

    ostr << version;

    RWCollectableString( ourName ).saveOn( ostr );

    ostr << version+1;

    ostr << value;
    ostr << flag;

    ostr << version+2;

}

const RWHashDictionary &
HxCmdItemRep::getTbItemBag() const
{

    assert( TbItemBag != NULL );
    return( (const RWHashDictionary &) *TbItemBag );

}

RWHashDictionary &
HxCmdItemRep::getRawTbItemBag() 
{
	assert( TbItemBag != NULL );
	return( (RWHashDictionary & ) *TbItemBag );
}

void
HxCmdItemRep::restoreGuts( RWFile & f ) 
{
    f.Read( (char *) ourName, 25);
    f.Read( value );
    f.Read( flag ); 

}

//
// Following is a little messy but its the same as above with
// some error handling.
//

void
HxCmdItemRep::restoreGuts( RWvistream & istr )
{ 

    int  version;

    istr >> version;

    if(version != VER_NUM) {
	cerr << "*** ERROR *** wrong HxCmdItemRep version=" << version << ", expecting" << (int)(VER_NUM) << endl;
    } /* end of if */
    RWCollectableString savingString;
    savingString.restoreFrom( istr );
    strcpy( ourName, savingString.data() );
    istr >> version;

    if(version != (VER_NUM+1)) {
    cerr << "*** ERROR *** wrong HxCmdItemRep version=" << version << ", expecting" << (int)(VER_NUM+1) << endl;
    } /* end of if */

    istr >> value;
    istr >> flag; 
    istr >> version;

    if(version != (VER_NUM+2)) {
	cerr << "*** ERROR *** wrong HxCmdItemRep version=" << version << ", expecting" << (int)(VER_NUM+2) << endl;
    } /* end of if */

}

unsigned short
HxCmdItemRep::packSeqNum() const
{
 
    const HxTbItem & ourItem = getTbItem();
    return( ourItem.packSeqNum() );

}

unsigned short
HxCmdItemRep::packSeqNum( const char * mnem ) const 
{

    assert( mnem != NULL );

    const HxTbItem & ourItem = getTbItem( mnem );
    return( ourItem.packSeqNum() );

}


unsigned short
HxCmdItemRep::offsetBytes() const
{
 
    const HxTbItem & ourItem = getTbItem();
    return( ourItem.offsetBytes() );

}

unsigned short
HxCmdItemRep::offsetBytes( const char * mnem ) const
{
 
    assert( mnem != NULL );

    const HxTbItem & ourItem = getTbItem( mnem );
    return( ourItem.offsetBytes() );

}

unsigned short
HxCmdItemRep::numBytes() const
{
 
    const HxTbItem & ourItem = getTbItem();
    return( ourItem.numBytes() );

}

unsigned short
HxCmdItemRep::numBytes( const char * mnem ) const
{

    assert( mnem != NULL ) ;

    const HxTbItem & ourItem = getTbItem( mnem );
    return( ourItem.numBytes() );

}

unsigned short
HxCmdItemRep::numBits() const
{
 
    const HxTbItem & ourItem = getTbItem();
    return( ourItem.numBits() );

}

unsigned short
HxCmdItemRep::numBits( const char * mnem ) const
{

    assert( mnem != NULL );

    const HxTbItem & ourItem = getTbItem( mnem );
    return( ourItem.numBits() );

}


unsigned short
HxCmdItemRep::offsetBits() const
{
 
    const HxTbItem & ourItem = getTbItem();
    return( ourItem.offsetBits() );

}

unsigned short
HxCmdItemRep::offsetBits( const char * mnem ) const
{
    assert( mnem != NULL );

    const HxTbItem & ourItem = getTbItem( mnem );
    return( ourItem.offsetBits() );

}

unsigned short
HxCmdItemRep::multiplicity() const
{
 
    const HxTbItem & ourItem = getTbItem();
    return( ourItem.multiplicity() );

}

unsigned short
HxCmdItemRep::multiplicity( const char * mnem ) const
{

    assert( mnem != NULL );

    const HxTbItem & ourItem = getTbItem( mnem );
    return( ourItem.multiplicity() );

}

 
unsigned short
HxCmdItemRep::subcomCycle() const
{
  const HxTbItem & ourItem = getTbItem();
  return( ourItem.subcomCycle() );
}

unsigned short
HxCmdItemRep::subcomCycle( const char * mnem ) const
{
  assert( mnem != NULL );
  const HxTbItem & ourItem = getTbItem( mnem );
  return( ourItem.subcomCycle() );

}

unsigned short
HxCmdItemRep::seqNumInCycle() const
{
 
    const HxTbItem & ourItem = getTbItem();
    return( ourItem.seqNumInCycle() );

}


unsigned short
HxCmdItemRep::seqNumInCycle( const char * mnem ) const
{

    assert( mnem != NULL );

    const HxTbItem & ourItem = getTbItem( mnem );
    return( ourItem.seqNumInCycle() );

}

unsigned short
HxCmdItemRep::associatedCmd() const
{
 
    const HxTbItem & ourItem = getTbItem();
    return( ourItem.associatedCmd() );

}


unsigned short
HxCmdItemRep::associatedCmd( const char * mnem ) const
{

    assert( mnem != NULL );

    const HxTbItem & ourItem = getTbItem( mnem );
    return( ourItem.associatedCmd() );

}


unsigned short
HxCmdItemRep::conversionAvail() const
{

    const HxTbItem & ourItem = getTbItem();
    return( ourItem.conversionAvail() );

}

unsigned short
HxCmdItemRep::conversionAvail( const char * mnem ) const
{

    assert( mnem != NULL );

    const HxTbItem & ourItem = getTbItem( mnem );
    return( ourItem.conversionAvail() );

}


unsigned short
HxCmdItemRep::configItem() const
{

    const HxTbItem & ourItem = getTbItem();
    return( ourItem.configItem() );

}

unsigned short
HxCmdItemRep::configItem( const char * mnem ) const
{

    assert( mnem != NULL );

    const HxTbItem & ourItem = getTbItem( mnem ) ;
    return( ourItem.configItem() );

}

unsigned short
HxCmdItemRep::inTelem() const
{

    const HxTbItem & ourItem = getTbItem();
    return( ourItem.inTelem() );

}


unsigned short
HxCmdItemRep::inTelem( const char * mnem ) const
{

    assert( mnem != NULL );

    const HxTbItem & ourItem = getTbItem( mnem );
    return( ourItem.inTelem() );

}


unsigned short
HxCmdItemRep::defaultValue() const
{

    const HxTbItem & ourItem = getTbItem();
    if ( clus_id==0 ) return(ourItem.clusADefaultValue() );
    else return( ourItem.clusBDefaultValue() );

}

unsigned short
HxCmdItemRep::defaultValue( const char * mnem ) const
{

    assert( mnem != NULL );

    const HxTbItem & ourItem = getTbItem( mnem );

    if ( clus_id==0 ) return(ourItem.clusADefaultValue() );
    else return( ourItem.clusBDefaultValue() );

}

unsigned short
HxCmdItemRep::digitalTolerance() const
{

    const HxTbItem & ourItem = getTbItem();
    return( ourItem.digitalTolerance() );

}

unsigned short
HxCmdItemRep::digitalTolerance( const char * mnem ) const
{

    assert( mnem != NULL );

    const HxTbItem & ourItem = getTbItem( mnem );
    return( ourItem.digitalTolerance() );

}

unsigned short
HxCmdItemRep::timeTolerance() const
{

    const HxTbItem & ourItem = getTbItem( );
    return( ourItem.timeTolerance() );

}

unsigned short
HxCmdItemRep::timeTolerance( const char * mnem ) const 
{

    assert( mnem != NULL );

    const HxTbItem & ourItem = getTbItem( mnem ) ;
    return( ourItem.timeTolerance() );

}

float 
HxCmdItemRep::lowerRedLim() const
{

    const HxTbItem & ourItem = getTbItem();
    return( ourItem.lowerRedLim() );

}


float
HxCmdItemRep::lowerRedLim( const char * mnem ) const
{

    assert( mnem != NULL );

    const HxTbItem & ourItem = getTbItem( mnem );
    return( ourItem.lowerRedLim() );

}


float 
HxCmdItemRep::upperRedLim() const
{

    const HxTbItem & ourItem = getTbItem();
    return( ourItem.upperRedLim() );

}


float
HxCmdItemRep::upperRedLim( const char * mnem ) const
{

    assert( mnem != NULL );

    const HxTbItem & ourItem = getTbItem( mnem );
    return( ourItem.upperRedLim() );

}


float 
HxCmdItemRep::lowerYellowLim() const
{

    const HxTbItem & ourItem = getTbItem();
    return( ourItem.lowerYellowLim() );

}


float
HxCmdItemRep::lowerYellowLim( const char * mnem ) const
{

    assert( mnem != NULL );

    const HxTbItem & ourItem = getTbItem( mnem );
    return( ourItem.lowerYellowLim() );

}



float 
HxCmdItemRep::upperYellowLim() const
{

    const HxTbItem & ourItem = getTbItem();
    return( ourItem.upperYellowLim() );

}

float 
HxCmdItemRep::upperYellowLim( const char * mnem ) const
{

    assert( mnem != NULL );

    const HxTbItem & ourItem = getTbItem( mnem ) ;
    return( ourItem.upperYellowLim() );

}


const char *
HxCmdItemRep::mnem() const
{


    const HxTbItem & ourItem = getTbItem();
    return( ourItem.mnem() );

}


const char *
HxCmdItemRep::mnem( const char * mnem ) const 
{
    assert( mnem != NULL );

    const HxTbItem & ourItem = getTbItem( mnem );
    return( ourItem.mnem() );

}


const char *
HxCmdItemRep::PEnumber() const
{

    const HxTbItem & ourItem = getTbItem();
    return( ourItem.PEnumber() );

}


const char * 
HxCmdItemRep::PEnumber( const char * mnem ) const
{

    assert( mnem != NULL );

    const HxTbItem & ourItem = getTbItem( mnem );
    return( ourItem.PEnumber() );

}


const char *
HxCmdItemRep::descript() const
{

    const HxTbItem & ourItem = getTbItem();
    return( ourItem.descript() );

}


const char *
HxCmdItemRep::descript( const char * mnem ) const
{

    assert( mnem != NULL );

    const HxTbItem & ourItem = getTbItem( mnem );
    return( ourItem.units() );

}

const char *
HxCmdItemRep::units() const
{

  const HxTbItem & ourItem = getTbItem();
  return( ourItem.units() );

}

const char *
HxCmdItemRep::units( const char * mnem ) const
{

  assert( mnem != NULL );

  const HxTbItem & ourItem = getTbItem( mnem );
  return( ourItem.units() );
}

int
HxCmdItemRep::operator==( const HxCmdItemRep & from ) const
{

    if ( getFlag() != from.getFlag() ) return FALSE;
    if ( getValue() != from.getValue() ) return FALSE;
    if ( getClusterId() != from.getClusterId() ) return FALSE;
    if ( strcmp( getName(), from.getName() ) != 0 ) return FALSE;

    return TRUE;

}



HxCmdItemRep &
HxCmdItemRep::operator=( const HxCmdItemRep & from ) 
{

    flag = from.getFlag();
    value = from.getValue();
    clus_id = from.getClusterId();
    strcpy( ourName, from.getName() );

    return( *this );
}



int
HxCmdItemRep::getClusterId() const
{

    return( clus_id );

}






@


1.2
log
@*** empty log message ***
@
text
@d5 1
a5 1
// $Id: HxCmdItemRep.C,v 1.1 1996/07/26 18:25:17 socdev Exp mmarlowe $
a7 126
//
// $Log: HxCmdItemRep.C,v $
// Revision 1.1  1996/07/26 18:25:17  socdev
// Initial revision
//
// Revision 4.5  1995/04/24 02:23:05  mmarlowe
//Revision 4.4  1995/04/24  01:03:20  mmarlowe
//Revision 4.3  1995/03/20  22:26:49  mmarlowe
//made inherit simpler
//
//Revision 4.2  1995/03/16  18:36:28  mmarlowe
//fixed problems with solaris2.4 and oc2.1 - moved static
//objects to pointers to objects
//
//Revision 4.1  1995/02/20  02:13:47  mmarlowe
//removed declarations from loops for optimization
//
//Revision 4.0  1995/01/23  20:55:21  mmarlowe
//MChecking in for Build4.0 Delivery
//
//Revision 3.102  1995/01/17  17:35:15  mmarlowe
//Revision 3.101  1995/01/09  16:53:34  mmarlowe
//added hcmAsserts to HxCmdItemRep methods.
//
//Revision 3.100  1995/01/09  14:00:00  mmarlowe
//added cmdStr and cmdFlag to cluster,
//toUppered all mnems used in hash dictionaries or lookups.
//
//Revision 3.99  1995/01/03  12:45:33  mmarlowe
//Moved the skipping of 5 lines when reading a datafile
//to getTable.C, and so deleted those lines from the initTbItemBag
//method.
//
//Revision 3.98  1994/12/28  16:06:46  mmarlowe
//changed all asserts to hcmAsserts.
//
//Revision 3.97  1994/12/21  23:38:32  mmarlowe
//created debugging versions of library
//
//Revision 3.96  1994/12/21  17:21:57  mmarlowe
//>> snapshot: HxMismatch now compiles and getMismatch code compiles
//>> in all necessary hcm files.  additions and changes to Makefiles
//>> and hx_ids.  We are now installing hx_ids.h.
//>> Fixed discovery of RCS corruption.
//>> Test routine works.  Checking in before doing some cleaning up
//>> than I will check in as a late build4.
//>> tests of HxMismatch and some other changes will follow.
//
//Revision 3.95  1994/12/19  15:17:57  mmarlowe
//Added method definitions for:
//	HxCmdItemRep::digitalTolerance() const,
//	HxCmdItemRep::digitalTolerance( const char *) const,
//	HxCmdItemRep::timeTolerance() const,
//	HxCmdItemRep::timeTolerance( const char * ) const.
//
//Revision 3.94  1994/12/04  15:48:52  mmarlowe
//debugging a rcs/emacs incompatibility
//
//Revision 3.93  1994/12/04  15:46:05  mmarlowe
//modified rcs log
//
//Revision 3.92  1994/12/04  15:35:27  mmarlowe
//fix bug in rcs
//
//Revision 3.91  1994/12/04  15:06:13  mmarlowe
//added rcs id
//
//Revision 3.90  1994/12/04  14:34:46  mmarlowe
//>> Modified to incorporate changes made to HkItem.table to support
//>> differentiation by cluster type and tolerances for mission
//>> monitoring.
//
//Revision 3.89  1994/11/13  12:37:33  mmarlowe
//inlined HxCmdItemRep::getFlag() and HxCmdItemRep::getValue()
//
//Revision 3.88  1994/11/04  22:15:35  mmarlowe
//added assert.h to includes
//
//Revision 3.87  1994/11/04  22:14:13  mmarlowe
//added unistd.h to includes
//
//Revision 3.86  1994/11/04  21:51:45  mmarlowe
//Cleanup revision.  Added checks for mnem == NULL in TbItem field retrieval
//methods.
//
//Revision 3.85  1994/10/27  11:28:36  mmarlowe
//modified print
//
//Revision 3.84  1994/10/24  23:45:20  mmarlowe
//modified print
//modified to take mnem for TbITem get methods
//modified hcm to not decrement numOfInstances and not delete.
//This is temporary till control can be taken over
//order of deconstruction of static variables.
//Fixed two minor bugs that came back from the dead:
//Bug1- Blank line in Item File results in creation of bad HxTbItem
//uninitialized memory when I save the entire contents
//
//Revision 3.80  1994/10/17  10:48:11  mmarlowe
//Revision 3.79  1994/10/13  21:01:33  mmarlowe
//
//Revision 3.78  1994/10/13  19:17:20  mmarlowe
//MAJOR REVISION PRIOR TO BUILD3_3, BROKE INHERITANCE HEIRARCHY
//
//Revision 3.77  1994/09/23  23:07:34  mmarlowe
//debugging, adding of more functionality and verbosity
//
//Revision 3.76  1994/09/13  05:06:05  mmarlowe
//fixed numerous bugs, added copy constructor, shorted
//print short, and added robustness
//
//Revision 3.75  1994/09/12  17:56:52  mmarlowe
//
//Revision 3.74  1994/07/27  02:23:16  mmarlowe
//Ported to Solaris 2.3, compiles under new SunPro CC.
//mostly meant getting rid of excess struct typedef which were wrong.
//
//Revision 3.73  1994/07/22  01:48:12  mmarlowe
//modified for RW6.0
//
//Revision 3.72  1994/07/18  04:08:29  mmarlowe
//1st quickfix to Build3.1, fix makefile and getTable
//
//Revision 3.72  1994/07/14  11:31:34  mmarlowe
//commenting/cleaning
//
@


1.1
log
@Initial revision
@
text
@d5 1
a5 1
// $Id: HxCmdItemRep.C,v 4.5 1995/04/24 02:23:05 mmarlowe Exp $
d10 3
d187 3
d216 13
a228 1
	    TbItemBag->insertKeyAndValue( key, newEntry );
@

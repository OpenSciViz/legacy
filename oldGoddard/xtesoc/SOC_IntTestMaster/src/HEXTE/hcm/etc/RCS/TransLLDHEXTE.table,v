head	1.1;
access
	soccm;
symbols;
locks; strict;
comment	@# @;


1.1
date	95.11.06.11.29.47;	author mmarlowe;	state Exp;
branches;
next	;


desc
@@


1.1
log
@Initial revision
@
text
@5   6    2.4    1
xte_ucsd_TransLLDHEXTE.table
Blanco, P.
soc_TransLLDHEXTE.table
v_1.0
1995.11.03_17:25
//  the 5 lines above comply with the ICD.
// $Id$
//
// This file contains the conversion coefficients between keV and LLD DAC
// setting for the 8 HEXTE detectors. The data have been copied from
// the PHA to LLD conversion coefficients provided by W. Heindl
// in a UCSD calibration file lld_map.dat. We are therefore assuming
// that PHA channel = keV value approximately. This should be close
// enough for most guest observers.
//
// ---------------- (edited) HEADER information from lld_map.dat ------------
// NAME = LLD to PHA mapping
// AUTHOR = William Heindl 
// DATE-DATA = 95-1-31
// DATE-VALID = pre-launch to ?? 
// DETNAM = ALL	
// COL1 = Cluster designation	// either "A" or "B" (changed to 0 or 1)
// COL2 = Phoswich Number	// Detector ID = 0,1,2, or 3
// COL3 = LLD Slope (PSA Channels per DAC step)	// 1st column (name and units)
// COL4 = LLD Offset (PSA Channels)	// 2nd column (name and units)
//
// DESCRIPTION OF THE DATA
//
// This file gives linear fits to the correspondence between the LLD
// setting and the minimum accepted pulse shape values (PHA).  The
// correspondence can be described by the following equation--
//
// PHAmin = COL3*LLD + COL4
//
// PHAmin is the minimum pulse height channel with 100% throughput for
// the given LLD setting.  Note that ~1-2 channels
// outside this range (at each end) will have non-zero response, due to 
// the finite slope of the discriminator edge.
// NOTE that the data used to generate these fits has integer PHA
// channel resolution.  Fractional output channels from the above equations
// should be rounded off.
//
//
// GENERATION OF THESE FITS
//
// On 95-1-31, histogram-bin (mode 2 4) data were accumulated with the
// pulse shape discrimination disabled to allow CsI events through.
// The LLD DAC was stepped through 14 different values ranging from 
// 0x00 to 0xff at about 5 minute intervals.
// The raw UCSD GSE ARCHIVE format data were stored in the
// following files:
//
// c1_lld_0x00.dat
// c1_lld_0x08.dat
// c1_lld_0x0c.dat
// c1_lld_0x10.dat
// c1_lld_0x14.dat
// c1_lld_0x18.dat
// c1_lld_0x1c.dat
// c1_lld_0x28.dat
// c1_lld_0x38.dat
// c1_lld_0x48.dat
// c1_lld_0x6f.dat
// c1_lld_0x8f.dat
// c1_lld_0xcf.dat
// c1_lld_0xff.dat
//
// c2_lld_0x00.dat
// c2_lld_0x08.dat
// c2_lld_0x0c.dat
// c2_lld_0x10.dat
// c2_lld_0x14.dat
// c2_lld_0x18.dat
// c2_lld_0x1c.dat
// c2_lld_0x28.dat
// c2_lld_0x38.dat
// c2_lld_0x48.dat
// c2_lld_0x6f.dat
// c2_lld_0x8f.dat
// c2_lld_0xcf.dat
// c2_lld_0xff.dat
//
//
// Where c1 indicates Cluster 1 (=A), and lld_0x1c indicates an LLD
// setting of 0x1c.
//
// To get the discriminator edges, the following procedure was performed
// individually for each detector:
//
//	1. PHA spectra were accumulated for each set of discriminator 
//	   settings. These spectra are saved in idl format in files with 
//	   names like: "c1_lld_0x1c.idl".
//
//	2. A template spectrum was formed (for each phoswich) 
//	   from the c1_lld_0x00.idl and 
//	   c2_lld_0x00.idl spectra, by normalizing to channel 80 -- far
//	   from the LLD edge.  
//
//	3. The spectra from the individual data sets were then 
//	   normalized and divided by the template spectrum.  This produced 
//	   a flat spectrum whose value in a given bin is the fractional 
//	   acceptance of events in that PHA channel for the current LLD 
//	   setting.
//
//	4. To define the LLD discriminator edges, I chose the 
//	   first bin in this flat spectrum having response 
//	   within 3 sigma of unity.  Note that there are PHA bins outside
//	   these edges, but that their throughput is statistically less
//	   than one.
// ------- End of header to lld_map.dat -----------------------------
//
// Repetition of the Column information above:
// 
// Column #: Description                              | TransLLDHEXTE variable
// Column 1: Cluster ID (0="A", 1="B")                | clID
// Column 2: Detector ID (0 to 3)                     | detID
// Column 3: Slope of PHA(or keV)/ DAC conversion     | kevSlope[clID][detID]
// Column 4: Offset of PHA(or keV) / LLD DAC relation | kevOffset[clID][detID]
//
// From above, assuming that the central energy of a PHA channel in
// keV approx.= the PHA #, we have:
//   lower energy bound (keV) = Column#3* (LLD DAC setting) + Column#4
//
0	0	0.185	4.664
0	1	0.185	5.200
0	2	0.185	4.981
0	3	0.185	3.897
1	0	0.185	4.155
1	1	0.185	3.826
1	2	0.181	4.227
1	3	0.184	4.272
//end
@

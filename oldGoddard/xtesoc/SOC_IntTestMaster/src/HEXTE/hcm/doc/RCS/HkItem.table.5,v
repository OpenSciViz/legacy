head	1.1;
access
	soccm;
symbols
	Build4_5_1:1.1
	Build4_3:1.1
	Build4_2:1.1
	coconut:1.1
	banana:1.1
	apple:1.1
	Build4_1:1.1;
locks; strict;
comment	@# @;


1.1
date	95.02.24.23.30.09;	author mmarlowe;	state Exp;
branches;
next	;


desc
@@


1.1
log
@Initial revision
@
text
@.\"
.\"
.TH Hxitem.table 1 "22 February 1995"
.SH NAME
HkItem.table \- Master List of HEXTE Data Items
.SH DESCRIPTION - FUNCTION AND MILIEU
\fIHxItems.table\fP contains a list of all units and groupings of HEXTE data
for reference by HEXTE software through the \fIhcm\fR library (\fIq. v.\fR).
The information in this table is used for all HEXTE commanding and
monitoring.  The information is not meant to be volatile, except perhaps for
the red and yellow limits for Health and Safety monitoring.  But there has been
some slow evolution of this table with improved understanding of the HEXTE.
Therefore  a version number, currently v_3.2 is placed as the fourth line of
the table, all the HEXTE software checks this version number, and a revision
of the table requires rebuilding of the HEXTE software.  This rigidity could
be eased, but quite possibly at about the time the table will no longer change.
.SH FILE ORGANIZATION
The file is flat ASCII.  At the top are quite a few comment lines which more
or less duplicate the information here.  Comment lines are marked, C++ style,
with an initial double slash. The table is organized by rows and columns.
A row corresponds to one data item, and there are 24 column entries per row.
Rows are roughly 120 characters wide. Column entries are separated by blank
characters.
.SH TELEMETRY MAPPING
The table was started as a telemetry map, thus the first 8 column entries
describe the location and size of the item in telemetry.
An item, as it appears in telemetry, can be a single bit, several
bits, a byte, a byte plus some bits, two bytes, four bytes, an array of
bytes, or an array of short integers.  
Items are grouped for convenience by their frequency and
position in status telemetry.  Groupings are introduced with initial comment
lines.  The basic unit of HEXTE status telemetry above the packet is the
IDF, the Instrument Data Frame, which consists of 8 successive packets and
recurs every 16 seconds.
Important items appear once per IDF.  Most digital and analog housekeeping
items are subcommutated with respect to the IDF, some at 64 seconds repetition
but most at 128 seconds.  The calibration spectrum, at 512 sec, is the slowest.
The headings are:
.br
(64-sec analog hk quantities) - packet sequence 2
.br
(128-sec analog hk quantities)- packet sequence 2
.br
(16-sec digital hk quantities) - packet sequence 0
.br
(64-sec digital hk quantities) - pkt seq 2
.br
(16-sec rates quantities) - packet seq 0
.br
(16-sec IDF Header quantities) - packet sequence 0
.br
Command information
.br
AGC status data (16-sec)
.br
Archive data (16-sec)
.br
Calibration Data (512-sec)

.SH COMMANDING
Commanding is served by a column which indicates whether the item's value
is directly controlled by command, and which of 13 HEXTE commands affects it.
One or two commandable data items were inadvertently left out of telemetry.
These appear in the table so they may be commanded.

.SH MNEMONICS
The most important column entry is the mnemonic, a string of up to
14 characters which provides convenient reference to the item for the
software. We have tried to make these brief, suggestive, and memorable, but
have put in also a longer ( about 40 characters) string for clearer
identification.

.SH HEALTH AND SAFETY
Some 76 analogue values which are the subject of Health and Safety monitoring
have been given values for red and yellow limits.  All other items have
aeroes in these columns

.SH DEFAULTS
Two columns contain startup (boot) values for the item in cluster A and B, 
respectively.

.SH MISSION MONITORING
 Three columns, presently containing zeroes, are reserved for possible use
as severity level,digital and temporal tolerance values for Mission
Monitoring.

.SH ARCHIVE DATA, CALIBRATION SPECTRUM
Telemetry start locations and lengths are given for the various segments of
these items, which are generally broken across two or more packets.

.SH WHAT'S NOT INCLUDED
HEXTE data items which are unique to
science data (App Id's 80 and 86) are NOT in this table; formatting of
such items is flexible, so a table approach is inappropriate.  References
to such data are part of the Data Management and Science Monitoring classes.

.SH DESCRIPTION OF COLUMNS
Column  1: packet IDF sequence number 0,1...7
.br
Column  2: offset (bytes) in packet data to start of item's field
.br
Column  3: number of bytes for item 
.br
Column  4: number of bits for item, additional to number of bytes
.br
Column  5: offset (bits) of field in byte
.br
Column  6: if !=0, multiplicity of item (spectra & time series), else 1
.br
Column  7: subcom cycle: 0 = 16-s; 1 = 64-s; 2 = 128-s; 3 = 512-s
.br
Column  8: sequence number in cycle: 0-0; 0-3;, 0-7; 0-31
.br
Column  9: opcode* of command which controls value of item. 0 if not controlled
.br
Column 10: if !=0, gives number of algorithm for conversion to physical units 
.br
Column 11: 1 if configuration item, else 0
.br
Column 12: 1 if in telemetry, else 0
.br
Column 13: default (startup) value cluster I
.br
Column 14: default (startup) value cluster II
.br
Column 15: severity level for mission monitoring
.br
Column 16: digital value tolerance for mission monitoring
.br
Column 17: time tolerance for mission monitoring (seconds? we'll see!)
.br
Column 18: lower RED limit for H&S
.br
Column 19: lower YELLOW limit for H&S
.br
Column 20: upper YELLOW limit for H&S
.br
Column 21: upper RED limit for H&S
.br
Column 22: mnemonic
.br
Column 23: Perkin-Elmer item number (a prefaces "analog", r "rate", d "digital", none "---")
.br
Column 24: longish descriptive string
.PP
* command opcode per table in PE DFM 30061-710-012, section 4 (Cmd Dictionary)
.br
00 = none
.br
01 = MEM_LOAD, uplink memory
.br
05 = IO_WRITE, uplink control values for I/O port
.br
10 = NEW_MODE, select science telemetry mode and control table
.br
16 = SET_MODUL, select target/background modulation pattern
.br
17 = SET_DACS, set discriminator levels
.br
18 = SET_1_DAC, set level for one discriminators
.br
21 = SET_1_AGC, set parameters for gain control
.br
22 = SET_AGCS, set gain control parameters, all detectors
.br
23 = SET_EVT_LOGIC, set event gating logic
.br
24 = SET_HEATER, set limits for heater control loops
.br
25 = SET_PULSER, control test pulse generator
.br
26 = SET_BURST, control burst list processing
.br
28 = SET_HVPS, set high voltage levels
.br
30 = SET_SHD, safe hold

.SH SEE ALSO
.BR hcm(3),  HkSub.table(5)

.SH AUTHOR
Duane Gruber , CASS/UCSD, dgruber@@mamacass.ucsd.edu

@

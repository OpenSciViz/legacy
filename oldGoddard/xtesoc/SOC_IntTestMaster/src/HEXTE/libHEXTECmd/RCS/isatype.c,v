head	1.2;
access
	soccm;
symbols
	Build4_1:1.1
	Build4:1.1
	Build3_3:1.1
	Build3_2:1.1
	Current:1.1;
locks;
comment	@ * @;


1.2
date	97.07.01.16.58.03;	author soccm;	state Exp;
branches;
next	1.1;

1.1
date	94.10.06.01.46.36;	author shalevy;	state Exp;
branches;
next	;


desc
@@


1.2
log
@added rcs string
@
text
@/*
 * isatype.c
 * Returns   0  Ok
 *          -4  Illegal digit
 *          -5  String too long
 *          -6  Value too large
 *          -7  Bad start of Hex value
 */

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "hexdefs.h"

static char* rcsid = "$Id$";

int
IsDecBYTE(str, bp)
char *str;
BYTE *bp;
{
    int RetVal = 0, slen;
    char *p;
    LInt i;

    *bp = 0;
    p = str;
    while (*p != '\0') {  /* For all characters in str */
        if (!isdigit((int)*p)) {  /* If a character is not a decimal digit */
            RetVal = IllegalDigit;  /* "Bad" return */
        }
        p++;
    }
    if (!RetVal) {  /* So far, so good. */
        p = str;
        /* At least one digit and at most 3 non-zero digits in a byte. */
        while ((int)strlen(p) > 1 && *p == '0') {  /* Strip leading 0's. */
            p++;
        }
        if ((slen = strlen(p)) < 4 && slen > 0) {
            i = strtoul(p, (char **)NULL, Dec);  /* Then get its value. */
            if (i > 255) {  /* Make sure it's not too big. */
                RetVal = ValueTooLarge;  /* Because too big -> "Bad" return. */
            } else {
                *bp = (BYTE)i;
            }
        } else {
            RetVal = StringTooLong;
        }
    }

    return RetVal;
}

int
IsHexBYTE(str, bp)
char *str;
BYTE *bp;
{
    int RetVal = 0, slen;
    char *p;
    LInt i;

    *bp = 0;
    if (!strncasecmp(str, "0x", 2)) {  /* First 2 chars must be 0x or 0X. */
        p = str+2;
        while (*p != '\0') {  /* For all remaining characters in str */
            if (!isxdigit((int)*p)) {  /* If a character is not a hex digit */
                RetVal = IllegalDigit;  /* "Bad" return */
            }
            p++;
        }
        if (!RetVal) {  /* So far, so good. */
            p = str+2;  /* p points at the "meat" of the number. */
            /* At least one digit and at most 2 non-zero digits in a byte. */
            while ((int)strlen(p) > 1 && *p == '0') {  /* Strip leading 0's. */
                p++;
            }
            if ((slen = strlen(p)) < 3 && slen > 0) {  /* Remember the "0x" and stripped 0's. */
                i = strtoul(p, (char **)NULL, Hex);  /* Then get its value. */
                if (i > 255) {  /* Make sure it's not too big. */
                    RetVal = ValueTooLarge;  /* Because too big -> "Bad" return. */
                } else {
                    *bp = (BYTE)i;
                }
            } else {
                RetVal = StringTooLong;
            }
        }
    } else {
        RetVal = BadHexParm;  /* "Bad" return */
    }

    return RetVal;
}

int
IsDecSInt(str, sip)
char *str;
SInt *sip;
{
    int RetVal = 0, slen;
    char *p;
    LInt i;

    *sip = 0;
    p = str;
    while (*p != '\0') {  /* For all characters in str */
        if (!isdigit((int)*p)) {  /* If a character is not a decimal digit */
            RetVal = IllegalDigit;  /* "Bad" return */
        }
        p++;
    }
    if (!RetVal) {  /* So far, so good. */
        p = str;
        /* At least one digit and at most 5 non-zero digits in a SInt. */
        while ((int)strlen(p) > 1 && *p == '0') {  /* Strip leading 0's. */
            p++;
        }
        if ((slen = strlen(p)) < 6 && slen > 0) {
            i = strtoul(p, (char **)NULL, Dec);  /* Then get its value. */
            if (i > 65535) {  /* Make sure it's not too big. */
                RetVal = ValueTooLarge;  /* Because too big -> "Bad" return. */
            } else {
                *sip = (SInt)i;
            }
        } else {
            RetVal = StringTooLong;
        }
    }

    return RetVal;
}

int
IsHexSInt(str, sip)
char *str;
SInt *sip;
{
    int RetVal = 0, slen;
    char *p;
    LInt i;

    *sip = 0;
    if (!strncasecmp(str, "0x", 2)) {  /* First 2 chars must be 0x or 0X. */
        p = str+2;
        while (*p != '\0') {  /* For all remaining characters in str */
            if (!isxdigit((int)*p)) {  /* If a character is not a hex digit */
                RetVal = IllegalDigit;  /* "Bad" return */
            }
            p++;
        }
        if (!RetVal) {  /* So far, so good. */
            p = str+2;  /* p points at the "meat" of the number. */
            /* At least one digit and at most 4 non-zero digits in a SInt. */
            while ((int)strlen(p) > 1 && *p == '0') {  /* Strip leading 0's. */
                p++;
            }
            if ((slen = strlen(p)) < 5 && slen > 0) {  /* Remember the "0x" and stripped 0's. */
                i = strtoul(p, (char **)NULL, Hex);  /* Then get its value. */
                if (i > 65535) {  /* Make sure it's not too big. */
                    RetVal = ValueTooLarge;  /* Because too big -> "Bad" return. */
                } else {
                    *sip = (SInt)i;
                }
            } else {
                RetVal = StringTooLong;
            }
        }
    } else {
        RetVal = BadHexParm;  /* "Bad" return */
    }

    return RetVal;
}

int
IsDecLInt(str, lip)
char *str;
LInt *lip;
{
    int RetVal = 0, slen;
    char *p;
    LInt i;

    *lip = 0;
    p = str;
    while (*p != '\0') {  /* For all characters in str */
        if (!isdigit((int)*p)) {  /* If a character is not a decimal digit */
            RetVal = IllegalDigit;  /* "Bad" return */
        }
        p++;
    }
    if (!RetVal) {  /* So far, so good. */
        p = str;
        /* At least one digit and at most 10 non-zero digits in a LInt. */
        while ((int)strlen(p) > 1 && *p == '0') {  /* Strip leading 0's. */
            p++;
        }
        if ((slen = strlen(p)) < 11 && slen > 0) {
            /* if str is 10 chars long, then make sure it isn't too big. */
            if (slen < 10 || strcmp(p, "4294967296") < 0) {
                i = strtoul(p, (char **)NULL, Dec);  /* Then get its value. */
                *lip = i;
            } else {
                RetVal = ValueTooLarge;
            }
        } else {
            RetVal = StringTooLong;
        }
    }

    return RetVal;
}

int
IsHexLInt(str, lip)
char *str;
LInt *lip;
{
    int RetVal = 0, slen;
    char *p;
    LInt i;

    *lip = 0;
    if (!strncasecmp(str, "0x", 2)) {  /* First 2 chars must be 0x or 0X. */
        p = str+2;
        while (*p != '\0') {  /* For all remaining characters in str */
            if (!isxdigit((int)*p)) {  /* If a character is not a hex digit */
                RetVal = IllegalDigit;  /* "Bad" return */
            }
            p++;
        }
        if (!RetVal) {  /* So far, so good. */
            p = str+2;  /* p points at the "meat" of the number. */
            /* At least one digit and at most 8 non-zero digits in a LInt. */
            while ((int)strlen(p) > 1 && *p == '0') {  /* Strip leading 0's. */
                p++;
            }
            if ((slen = strlen(p)) < 9 && slen > 0) {  /* Remember the "0x" and stripped 0's. */
                i = strtoul(p, (char **)NULL, Hex);  /* Then get its value. */
                *lip = i;
            } else {
                RetVal = StringTooLong;
            }
        }
    } else {
        RetVal = BadHexParm;  /* "Bad" return */
    }

    return RetVal;
}
@


1.1
log
@Initial revision
@
text
@d15 2
@

head	1.5;
access
	socops;
symbols
	Build4:1.5
	Build3_3:1.5
	Build3_2:1.5
	Current:1.5;
locks; strict;
comment	@# @;


1.5
date	94.10.06.02.06.33;	author shalevy;	state Exp;
branches;
next	1.4;

1.4
date	94.10.06.01.56.09;	author shalevy;	state Exp;
branches;
next	1.3;

1.3
date	94.10.06.01.53.58;	author shalevy;	state Exp;
branches;
next	1.2;

1.2
date	94.10.06.01.50.25;	author shalevy;	state Exp;
branches;
next	1.1;

1.1
date	94.10.06.01.46.36;	author shalevy;	state Exp;
branches;
next	;


desc
@HEXTE Commanding Library
@


1.5
log
@*** empty log message ***
@
text
@# RCS: $Id: Makefile,v 1.4 1994/10/06 01:56:09 shalevy Exp $
#
#	Makefile for build of hexte command translation library.
#
#  Original by Tom Gassaway
#  Modified by Shalom Halevy shalevy@@ucsd.edu
#

ifndef ARCH
  ARCH := $(shell uname)$(shell uname -r | cut -c1)
endif
ifndef CCROOT_I
  CCROOT_I := /socsoft/CenterLine/clc++/sparc-sunos4/incl
endif

CC       = CC
CFLAGS   = -g
CPPFLAGS = -D$(ARCH) -I$(SOCHOME)/include -I$(SOCLOCAL)/include

ARFLAGS  = r
HDRS     = ansidecl.h cmdse.h hexdefs.h
PUBHDRS  = 
PUBSRCS  =
PUBOBJS  =

OBJS= 	add_code.o \
	execute.o \
	hexabort.o \
	hexmacro.o \
	io_read.o \
	io_write.o \
	isatype.o \
	mem_dump.o \
	mem_load.o \
	mem_move.o \
	misccmd.o \
	new_mode.o \
	proccmdlib.o \
	rem_code.o \
	run_test.o \
	set_1_ag.o \
	set_1_da.o \
	set_agcs.o \
	set_burs.o \
	set_dacs.o \
	set_evtl.o \
	set_heat.o \
	set_hvps.o \
	set_modu.o \
	set_puls.o \
	set_shd.o \
	strtoul.o \
	xlatecmd.o

SRCS     =$(OBJS:.o=.c)
PROGS    =

LIBRARY  =$(SOCHOME)/lib/$(ARCH)/libHEXTECmd.a

.c.o: $(HDRS) $(SRCS)
	cc -c ${CFLAGS} $<

$(LIBRARY): ${OBJS} $(HDRS)
	ar cr $(LIBRARY) ${OBJS}
ifneq ($(ARCH),SunOS5)
	ranlib $@@
endif

pubhdrs: $(HDRS)
	@@true

publibs: $(LIBRARY)
	-chmod 664 $(LIBRARY)

install:
	@@true

uninstall:
	@@true

install-test:
	@@true

clean:
	$(RM) *.o make.d* *.a $(PROGS)

realclean: clean
	-rcsclean

rcsclean:
	-rcsclean

make.d:
	touch $@@

depend: $(SRCS) make.d
	makedepend -f make.d $(CPPFLAGS) -I$(CCROOT_I) $(SRCS)

include make.d
@


1.4
log
@*** empty log message ***
@
text
@d1 1
a1 1
# RCS: $Id: Makefile,v 1.3 1994/10/06 01:53:58 shalevy Exp $
d89 3
@


1.3
log
@*** empty log message ***
@
text
@d1 1
a1 1
# RCS: $Id: Makefile,v 1.2 1994/10/06 01:50:25 shalevy Exp $
d17 1
a17 1
CFLAGS   = 
a59 3
CXX      = $(CC)
CXXFLAGS = $(CFLAGS)

d61 1
a61 1
	CC -c ${CFLAGS} $<
@


1.2
log
@*** empty log message ***
@
text
@d1 1
a1 1
# RCS: $Id: Makefile,v 1.1 1994/10/06 01:46:36 shalevy Exp $
d17 1
a17 1
CFLAGS   = -g
d63 1
a63 1
.c.o:
d72 1
a72 1
pubhdrs:
@


1.1
log
@Initial revision
@
text
@d1 1
a1 1
# RCS: $Id$
d66 1
a66 1
$(LIBRARY): ${OBJS}
@

head	4.1;
access
	socdev
	soccm;
symbols
	Build4_5_1:4.1
	Build4_3:4.0
	Build4_2:4.0
	coconut:4.0
	banana:4.0
	apple:4.0
	Build4_1:4.0
	Build4:4.0;
locks; strict;
comment	@//@;


4.1
date	95.09.12.04.15.40;	author mmarlowe;	state Exp;
branches;
next	4.0;

4.0
date	95.01.23.20.58.00;	author mmarlowe;	state Exp;
branches;
next	1.1;

1.1
date	95.01.09.14.01.38;	author mmarlowe;	state Exp;
branches;
next	;


desc
@@


4.1
log
@*** empty log message ***
@
text
@//===============================================================
// File: tHxMismatch.C
// Programmer: Matthew Marlowe
// Project: XTESOC, HEXTE, CASS
// Library: libhcm.a
// Description: Test script for getMismatches methods on various
//              hcm classes and to exercise the HxMismatch class.
// RCS Info: $Id: tHxMismatch.C,v 4.0 1995/01/23 20:58:00 mmarlowe Exp $
// 
// $Log: tHxMismatch.C,v $
//Revision 4.0  1995/01/23  20:58:00  mmarlowe
//delivering for build4.0
//
//Revision 1.1  1995/01/09  14:01:38  mmarlowe
//Initial revision
//
//
//================================================================

// system header files
#include <iostream.h>
#include <stdio.h>
#include <stdlib.h>

// Rogue Wave Include Files

// HEXTE Standard Include Files
#include <hx_ids.h>
#include <HexDefs.h>

// HCM include Files
#include <hcmDebug.h>
#include <HxCluster.h>
#include <HxCmdSub.h>
#include <HxCmdItem.h>
#include <HxMismatch.h>
#include <stream.h>
#include <strstream.h>
#include <rw/pstream.h>
#include <rw/vstream.h>
#include <rw/rwfile.h>

//-------------------------------------------------------------------
// Function Prototypes
//-------------------------------------------------------------------

int main();
void testConstructors();
void testSetMethods();
void testOperators();
void testPersistance();
void saveMismatch( char * filename, const HxMismatch *);
HxMismatch * restMismatch( char * fileName);
void testPrint();

//--------------------------------------------------------------------
// Main
//--------------------------------------------------------------------
int
main() 
{

  cout << "tHxMismatch test routine.\n" << endl;

  testConstructors();
  testSetMethods();
  testOperators();
  testPersistance();
  testPrint();

  cout << "End of tHxMismatch tests.\n" << endl;
}

void
testConstructors()
{

  cerr << "\n\nCONSTRUCTOR TESTS\n\n" << endl;
  
//** Test of null constructor and printShort 

  cerr << "null constructor - ";
  HxMismatch * mis1 = new HxMismatch();
  cerr << "done." << endl;

  cerr << "Simple Print Short" << endl;
  mis1->printShort( cout );
  cerr << "done." << endl;

  delete( mis1 );

//** Test of full constructor

  cerr << "Explicit constructor - ";
  HxMismatch * mis2 = new HxMismatch( "test", 0, 2, 1, 0 );
  cerr << "done." << endl;

  cerr << "Simple Print" << endl;
  mis2->print( cout );
  cerr << "done." << endl;

//** test of copy constructor

  cerr << "Copy Constructor - "; 
  HxMismatch * mis3 = new HxMismatch( (const HxMismatch &) *mis2 );
  cerr << "done." << endl;
  
  cerr << "simple print" << endl;
  mis3->print( cout );
  cerr << "done." << endl;

  cerr << "End of Constructor TESTS." << endl;

  delete(mis2);
  delete(mis3);

}


void
testSetMethods()
{

  cerr << "\nSet/GetMethod tests!\n" << endl;

  HxMismatch mis1;
  
  // set-get name
  
  cerr << "Testing set/getName() - ";
  mis1.setName("foobar");
  if ( strcmp( "foobar", mis1.getName()) == 0 )
    cerr << "seems to work." << endl;
  else
    cerr << "Either setName() or getName() doesnt work." << endl;

// set-get value1

  cerr << "Testing set/getValue1() - ";
  mis1.setValue1(10);
  if ( mis1.getValue1() == 10 )
    cerr << "seems to work." << endl;
  else
    cerr << "Either setValue1() or getValue1() doenst work." << endl;

// set-get value2

  cerr << "Testing set/getValue2() - ";
  mis1.setValue2(20);
  if ( mis1.getValue2() == 20 )
    cerr << "seems to work." << endl;
  else
    cerr << "Either setValue2() or getValue2() doenst work." << endl;

// set-get clusterId

  cerr << "Testing set/getClusterId() - ";
  
  mis1.setClusterId( 1 );
  if ( mis1.getClusterId() == 1 )
    cerr << "seems to work." << endl;
  else 
    cerr << "Either setClusterId() or getClusterId() doesnt work." << endl;

// set-getDigTol

  cerr << "Testing set/getDigTol() - ";
  
  mis1.setDigTol(30);

  if (mis1.getDigTol() == 30 )
    cerr << "seems to work." << endl;
  else
    cerr << "Either getDigTol() or setDigTol() doesnt work." << endl;

  // END of set/get tests
  cerr << "End of set/get tests.\n" << endl;

}
  
void
testOperators()
{

  // operator tests
  cerr << "testing Operators()\n " << endl;

  HxMismatch mis1( "test", 1, 2, 3, 0 );
  HxMismatch mis2( "test", 1, 2, 2, 0 );
  HxMismatch mis3(mis1), mis4;

  if (mis3 == mis1) 
    cerr << "Equality operator reports true for equality." << endl;
  else
    cerr << "Equality operator reports false for equality." << endl;

  if (mis3 == mis2)
    cerr << "Equality operator reports true for inequality." << endl;
  else
    cerr << "Equality operator reports false for inequality." << endl;

  mis4 = mis2;
  if ( mis4 == mis2 ) 
    cerr << "assignment operator seems to work." << endl;
  else 
    cerr << "assignment operator does not work."  << endl;
  
  // end of operator methods
  cerr << "\nEnd of operator tests.\n" << endl;

}

void
testPersistance()
{

  HxMismatch * mis1 = new HxMismatch("persist", 0, 2, 3, 0 );
  saveMismatch( "mis.guts", (const HxMismatch *) mis1 );
  
  HxMismatch * mis2 = restMismatch("mis.guts");

  if ( *mis2 == *mis1 )
    cerr << "Restored mismatch equals saved mismatch." << endl;
  else
    cerr << "Restored mismatch does not equal saved mismatch." << endl;

  delete(mis1);
  delete(mis2);

}

void
saveMismatch( char * fileName, const HxMismatch * theMismatch )
{

  cerr << "Saving a HxMismatch." << endl;

  ofstream ofstrm( fileName );
  RWpostream PoMismatchStrm(ofstrm);
  
  theMismatch->saveGuts( PoMismatchStrm );

}
  

HxMismatch *
restMismatch( char * fileName )
{

  ifstream ifstrm( fileName );
  RWpistream PiMismatchStrm( ifstrm );
  
  HxMismatch * restored = new HxMismatch();
  restored->restoreGuts( PiMismatchStrm );

  return( restored );

}
  
   
void
testPrint()
{

  HxMismatch mis1( "mnem", 0, 2, 3, 0 );
  
  cerr << "Testing print methods:" << endl;
  cerr << "PRINTSHORT" << endl;
  mis1.printShort( cout );
  cerr << "PRINT" << endl;
  mis1.print(cout);
  cerr << "PRINTLONG" << endl;
  mis1.printLong( cout );

}
 
       
@


4.0
log
@delivering for build4.0
@
text
@d8 1
a8 1
// RCS Info: $Id: tHxMismatch.C,v 1.1 1995/01/09 14:01:38 mmarlowe Exp mmarlowe $
d11 3
@


1.1
log
@Initial revision
@
text
@d8 1
a8 1
// RCS Info: $Id:$
d10 3
a12 1
// $Log:$
d14 1
@

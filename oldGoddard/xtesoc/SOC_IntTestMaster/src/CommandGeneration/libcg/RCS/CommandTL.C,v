head	1.18;
access
	joel
	vick
	mmarlowe
	socdev
	soccm;
symbols
	Build4_3:1.13
	Build4_2:1.11
	Build4_1:1.12
	Build4:1.11
	Build3_3:1.10
	Build3_2:1.10
	Build3_1:1.6
	Build3:1.6
	Current:1.2;
locks;
comment	@ * @;


1.18
date	97.07.01.21.09.06;	author soccm;	state Exp;
branches;
next	1.17;

1.17
date	95.10.19.19.45.53;	author mmarlowe;	state Exp;
branches;
next	1.16;

1.16
date	95.10.16.19.40.58;	author mmarlowe;	state Exp;
branches;
next	1.15;

1.15
date	95.09.11.15.06.50;	author vick;	state Exp;
branches;
next	1.14;

1.14
date	95.08.30.17.16.25;	author joel;	state Exp;
branches;
next	1.13;

1.13
date	95.04.20.18.00.29;	author vick;	state Exp;
branches;
next	1.12;

1.12
date	94.12.20.01.16.32;	author vick;	state Exp;
branches;
next	1.11;

1.11
date	94.11.03.22.14.51;	author vick;	state Exp;
branches;
next	1.10;

1.10
date	94.09.14.13.06.47;	author prao;	state Exp;
branches;
next	1.9;

1.9
date	94.08.19.18.42.49;	author vick;	state Exp;
branches;
next	1.8;

1.8
date	94.07.15.21.18.35;	author vick;	state Exp;
branches;
next	1.7;

1.7
date	94.07.15.17.53.19;	author prao;	state Exp;
branches;
next	1.6;

1.6
date	94.06.17.14.32.57;	author prao;	state Exp;
branches;
next	1.5;

1.5
date	94.05.26.19.34.05;	author prao;	state Exp;
branches;
next	1.4;

1.4
date	94.05.17.19.42.48;	author prao;	state Exp;
branches;
next	1.3;

1.3
date	94.02.24.21.23.01;	author herreid;	state Exp;
branches;
next	1.2;

1.2
date	94.02.18.14.32.33;	author herreid;	state Exp;
branches;
next	1.1;

1.1
date	94.01.28.22.49.46;	author herreid;	state Exp;
branches;
next	;


desc
@Command Time-Line class
@


1.18
log
@added rcs string
@
text
@// Project:	 XTE SOC
// Name:	 CommandTL.C
// Subsystem:	 Comman Genartion
// Programmer:	 vick
// Description:	 Code for manipulating  AbsTimeCmds and RelTimeCmds
//

#include <iostream.h>
#include <iomanip.h>

#include "CommandTL.h"
#include "CommandScript.h"
#include "CommandString.h"
#include "CommandPacket.h"
#include "SCommandPacket.h" 
#include "PseudoConfig.h"

#include <CommandScriptElement.h>

static const char* const rcsid =
"$Id: CommandTL.C,v 1.17 1995/10/19 19:45:53 mmarlowe Exp $";

SCommandPacket dummySCommandPacket;
CommandString dummyCommandString;
CommandPacket dummyCommandPacket;

TextCommandString dummyTextCommandString;
MOCCommandString dummyMOCCommandString;

int AbsTimeCmds::strtim = 0;
int AbsTimeCmds::endtim = 0;


ostream&
mocPlanHeader (ostream& ostr, CommandTL& cmds)
{
  switch (TimeLine::_outputFormat) {
  case TimeLine::DAILY_ACTIVITY_PLAN:
    ostr << "HEADER SOURCE=SOC, TYPE=DAILY," << endl;
    break;
  case TimeLine::UPDATE_PLAN:
    ostr << "HEADER SOURCE=SOC, TYPE=UPDATE;" << endl;
    break;
  case TimeLine::PATCH_PLAN:
    ostr << "HEADER SOURCE=SOC, TYPE=PATCH," ;
    break;
  case TimeLine::MACRO_DEFINITION_PLAN:
    ostr << "HEADER SOURCE=SOC, TYPE=DEFPLN;" << endl;
    ostr << "DEFINE " << cmds.itemName() << ", TYPE=MACRO;" << endl;
    break;
  case TimeLine::RTS_DEFINITION_PLAN:
    ostr << "HEADER SOURCE=SOC, TYPE=DEFPLN;" << endl;
    ostr << "DEFINE " << cmds.itemName() << ", TYPE=RTS;" << endl;
    break;
  default:
    break;
  };
  return ostr;
}

OMANIP(CommandTLref) mocPlanHeader (CommandTL& r){
  return OMANIP(CommandTLref)(mocPlanHeader,r);
}


ostream&
mocDailyActivityPlan (ostream& ostr, AbsTimeCmds& atc)
{
  TimeLine::_outputFormat = TimeLine::DAILY_ACTIVITY_PLAN;
  ostr << mocPlanHeader(atc);
  ostr << "START=" << mocTime(atc.firstTime() ) << ", " ;
  ostr << "STOP=" << mocTime(atc.lastTime()) << ";" << endl; 
  return ostr;
}


OMANIP(AbsTimeCmdsref) mocDailyActivityPlan (AbsTimeCmds& r){
  return OMANIP(AbsTimeCmdsref)(mocDailyActivityPlan,r);
}


ostream&
mocUpdatePlan (ostream& ostr, AbsTimeCmds& atc)
{
  TimeLine::_outputFormat = TimeLine::UPDATE_PLAN;
  ostr << mocPlanHeader(atc);
  ostr << "START=" << mocTime( atc.firstTime() ) << ", " ;
  ostr << "STOP=" << mocTime( atc.lastTime() ) << ";" << endl; 
  return ostr;
}

OMANIP(AbsTimeCmdsref) mocUpdatePlan (AbsTimeCmds& r){
  return OMANIP(AbsTimeCmdsref)(mocUpdatePlan,r);
}



int
CommandTL::illegalAppIds (CommandScript&) const
{
  // examine for illegal app Ids here
  return FALSE;
}

int
CommandTL::scriptOverlap (int, CommandScript&) const
{
  // examine for script overlaps here
  return FALSE;
}

int
CommandTL::criticalCmds (CommandScript&) const
{
  //examine for critical commands here
  return FALSE;
}

RWDEFINE_COLLECTABLE(CommandTL, __COMMANDTL)


ostream&
mocPatchPlan (ostream& ostr, const PatchPlan & pp ) 
{
  TimeLine::_outputFormat = TimeLine::PATCH_PLAN;
  AbsTimeCmds dummyCommandTimeLine;
  ostr << mocPlanHeader(dummyCommandTimeLine);
  ostr << " LOADNAME=" << pp.getMapName() << ", " <<endl; 
  ostr << "START=" << mocTime( pp.firstTime() ) << ", " ;
  ostr << "STOP=" << mocTime( pp.lastTime() ) << ";" << endl; 
  return ostr;
}

ostream&
mocMacroDefinitionPlan (ostream& ostr , RelTimeCmds& )
{
  TimeLine::_outputFormat = TimeLine::MACRO_DEFINITION_PLAN;
  return ostr;
}

OMANIP(RelTimeCmdsref) mocMacroDefinitionPlan  (RelTimeCmds& r){
  return OMANIP(RelTimeCmdsref)(mocMacroDefinitionPlan,r);
}



ostream& mocRtsDefinitionPlan (ostream& ostr , RelTimeCmds& r)
{
  TimeLine::_outputFormat = TimeLine::RTS_DEFINITION_PLAN;
  ostr << mocPlanHeader(r);

  CommandTLIterator iter(r);

    while (iter()) 
    {
      RWCollectable *item = iter.key();
      if (item->isA() == dummyCommandString.isA())
	ostr <<  *(CommandString*)item;
      else if (item->isA() == dummyCommandPacket.isA())
	ostr <<  *(CommandPacket*)item;
      else if (item->isA() == dummySCommandPacket.isA())
	ostr <<  *(SCommandPacket*)item;
      else 
	cerr << "SOC INFO: Check this case for " << item->isA() << endl;
    }

  ostr << mocPlanTrailer;
  return ostr;
}

OMANIP(RelTimeCmdsref) mocRtsDefinitionPlan  (RelTimeCmds& r){
  return OMANIP(RelTimeCmdsref)(mocRtsDefinitionPlan,r);
}


typedef CommandTL& CommandTLref;
//IOMANIPdeclare(CommandTLref);


ostream&
mocPlanTrailer (ostream& ostr)
{
  switch (TimeLine::_outputFormat) {
  case TimeLine::MACRO_DEFINITION_PLAN:
  case TimeLine::RTS_DEFINITION_PLAN:
    ostr << "END_DEFINE;" << endl;
  case TimeLine::DAILY_ACTIVITY_PLAN:
  case TimeLine::UPDATE_PLAN:
  case TimeLine::PATCH_PLAN:
    ostr << "END_PLAN;" << endl;
    break;
  default:
    break;
  };
  return ostr;
}

//-----------------------------------------------------------------------------

AbsTimeCmds::AbsTimeCmds()
{
  swt = 0 ;
  enableAppIdCheck();
  enableOverlapCheck();
  enableOverlapRepair();
  enableCriticalCmdCheck();
}

int
AbsTimeCmds::repairOverlap (int&, CommandScript&)
{
  return TRUE;
}

CommandScript*
AbsTimeCmds::insertCommandScript (int startTime, CommandScript* script)
{
  if (!script) return 0;

  if (_checkAppId && illegalAppIds (*script))
    return 0;

  if (_checkCriticalCmd && criticalCmds (*script))
    return 0;

  if (_checkOverlap && scriptOverlap (startTime, *script)) {
    int repairedStartTime = startTime;

    if (_repairOverlap && repairOverlap (repairedStartTime, *script))
      script->setStartTime (repairedStartTime);
    else
      return 0;
  }
  else
    {
    script->setStartTime (startTime);
    }
  return (CommandScript*)insert (script);
}

RWDEFINE_COLLECTABLE(AbsTimeCmds, __ABSTIMECMDS)

ostream&
operator<< (ostream& ostr, AbsTimeCmds& cmds)
{
  CommandTLIterator iter(cmds);

    while (iter()) 
    {
      CommandScriptElement* item =  (CommandScriptElement*  )iter.key();
     item->PrintToPlan(ostr);
    }

  return ostr;
}

//-----------------------------------------------------------------------------

RelTimeCmds::RelTimeCmds()
{
  enableAppIdCheck();
  enableOverlapCheck();
  enableCriticalCmdCheck();
}

CommandScript*
RelTimeCmds::insertCommandScript (short startTime, CommandScript* script)
{
  if (!script) return 0;

  if (_checkAppId && illegalAppIds (*script)) {
    return 0;
  }
  if (_checkCriticalCmd && criticalCmds (*script)) {
    return 0;
  }
  if (_checkOverlap && scriptOverlap (startTime, *script)) {
    return 0;
  }

  script->setStartTime (startTime);
  return (CommandScript*)insert (script);
}

RWDEFINE_COLLECTABLE(RelTimeCmds, __RELTIMECMDS)

ostream&
operator<< (ostream& ostr, RelTimeCmds& cmds)
{
  ostr << mocPlanHeader (cmds);

  CommandTLIterator iter(cmds);

  while (iter())
    ostr << *(TimeLine*)iter.key();

  ostr << mocPlanTrailer;

  return ostr;
}



//
// Test(s) added by Joel Wisner, 8/28/95
//

#ifdef TEST_COMMANDTL

#include "CommandScript.h"
#include "CommandPacket.h"
#include "SCommandPacket.h"
#include "TimeLine.h"

#include <stdlib.h>
#include <strstream.h>


main ()
{  
  RelTimeCmds sampleRTC;

  cout << "===============================================" << endl;
  cout << "Creating an empty RelTimeCmds object." << endl;
 
  CommandScript commscr1("Sample CommandScript 1");
  
  cout << "Created a CommandScript." << endl;

  CommandPacket commpkt1((const unsigned char*) "Sample CommandPacket 1");

  cout << "Created a CommandPacket." << endl;

  commscr1.insertCommandString(1,"Sample CommandString 1","Used for testing RelTimeCmds");

  cout << "Inserted a CommandString into the CommandScript." << endl;

  commscr1.insertCommandPacket(5, (const CommandPacket*) &commpkt1, "My sample Command Packet 1", "Used for testing RelTimeCmds");

  cout << "Inserted a CommandPacket into the CommandScript." << endl;

  CommandScript* cmdscrptr1 = sampleRTC.insertCommandScript(0,&commscr1);

  cout << "Inserted the CommandScript into the RelTimeCmds object." << endl;

  if(cmdscrptr1)
    cout << "Output of the RelTimeCmds object:" << endl << sampleRTC << endl << endl;
  else
    exit(1);

  
// Now test the AbsTimeCmds class

  AbsTimeCmds sampleATC;

  cout << "===============================================" << endl;

  cout << "Creating an empty AbsTimeCmds object." << endl;
 
  CommandScript commscr2("Sample CommandScript 2");
  
  cout << "Created a CommandScript." << endl;

  CommandPacket commpkt2(32, 3, 2, (const unsigned char*)"123");

  cout << "Created a CommandPacket." << endl;

  commscr2.insertCommandString(1,"Sample CommandString 2","Used for testing AbsTimeCmds");

  cout << "Inserted a CommandString into the CommandScript." << endl;

  commscr2.insertCommandPacket(5, (const CommandPacket*) &commpkt2, "My sample SCommand Packet 2", "Used for testing AbsTimeCmds");

  cout << "Inserted a CommandPacket into the CommandScript." << endl;

  cout << "The Command Script " << endl;
  cout << "==========================================" << endl;
  cout << endl << commscr2 << endl << endl;
  cout << "==========================================" << endl;

  strstream isStr;
  int startSecs;

  isStr << "1996:001:01:01:01";
  isStr>>missionSeconds  (startSecs);

  CommandScript* cmdscrptr2 = sampleATC.insertCommandScript(startSecs,&commscr2);

  cout << "Inserted the CommandScript into the AbsTimeCmds object." << endl;

  if(cmdscrptr2){
    AbsTimeCmds* ExpandedSampleATC = (AbsTimeCmds*)sampleATC.expand();;
  
    cout << "Output of the AbsTimeCmds object:" << endl << *ExpandedSampleATC << endl << endl;
  }
  else
    exit(1);
  



} // end main

#endif




@


1.17
log
@Fix for DR#493 CG(editpatch header was missing keywords)
@
text
@d5 1
a5 2
// Description:	 Code for manipulating  AbsTimeCmds and 
//               RelTimeCmds
a7 1

a17 1

d20 2
a21 2
static const char
rcsId[] = "$Id: CommandTL.C,v 1.16 1995/10/16 19:40:58 mmarlowe Exp $";
@


1.16
log
@*** empty log message ***
@
text
@d24 1
a24 1
rcsId[] = "$Id: CommandTL.C,v 1.15 1995/09/11 15:06:50 vick Exp mmarlowe $";
d126 1
a126 1
mocPatchPlan (ostream& ostr, RWCString & mapName, int sTime, int lTime)
d131 3
a133 3
  ostr << "LOADNAME=" << mapName << ", " endl; 
  ostr << "START=" << mocTime( sTime ) << ", " ;
  ostr << "STOP=" << mocTime( lTime ) << ";" << endl; 
@


1.15
log
@Augmented test.
@
text
@d24 1
a24 1
rcsId[] = "$Id: CommandTL.C,v 1.14 1995/08/30 17:16:25 joel Exp vick $";
d48 1
a48 1
    ostr << "HEADER SOURCE=SOC, TYPE=PATCH;" << endl;
d126 1
a126 1
mocPatchPlan (ostream& ostr)
d129 5
@


1.14
log
@Added test and header.
@
text
@d24 1
a24 1
rcsId[] = "$Id: CommandTL.C,v 1.13 1995/04/20 18:00:29 vick Exp joel $";
d311 2
d314 2
d348 1
d362 1
a362 1
  CommandPacket commpkt2(32, 3, 2, (const unsigned char*)"\0\0\0");
d374 12
a385 1
  CommandScript* cmdscrptr2 = sampleATC.insertCommandScript(0,&commscr2);
d389 5
a393 2
  if(cmdscrptr2)
    cout << "Output of the AbsTimeCmds object:" << endl << sampleATC << endl << endl;
@


1.13
log
@Fixed problem w/ output to dap and otherMOC output products
and added some documentation
@
text
@d24 1
a24 1
rcsId[] = "$Id: CommandTL.C,v 1.12 1994/12/20 01:16:32 vick Exp vick $";
d299 89
@


1.12
log
@added call to isCommandScriptElement
@
text
@d1 9
d24 2
a25 1
rcsId[] = "$Id: CommandTL.C,v 1.11 1994/11/03 22:14:51 vick Exp vick $";
a217 10
  cout << "In AbsTimeCmds::insertCommandScript (int startTime, CommandScript* script)" << endl;
  cout << "the address " << script << endl;
  cout << "******* Number of entries ******** " << script->entries() << endl;

  if (!script->entries()) {
    cout << "Script NOT Inserted ....." << endl;
    return 0;
  }


a235 3

  cout << *script;
  cout << "Script Inserted ....." << endl;
d246 1
a246 3
  TimeLine* item ;
  int i = 0;
  while (item = (TimeLine*  )iter()) 
d248 3
a250 8
      //cout << endl <<  i << " ) " << item->isA() << " " << item << endl;

      if (item->isCommandScriptElement()){
	CommandScriptElement* cse  = (CommandScriptElement*) item;
	cse->PrintToPlan(cout);
      }
      else
	cout << *item;
a251 3
      i++;
    }
  //cout << "done" << endl;
a268 4
  // don't insert empty scripts
  if (script->entries() == 0)
    return 0;

a298 1

@


1.11
log
@Changed code to simplify output by added CommandScriptElement to inheritance heirarchy
@
text
@d15 1
a15 1
rcsId[] = "$Id: CommandTL.C,v 1.10 1994/09/14 13:06:47 prao Exp vick $";
d208 10
d236 3
d249 3
a251 1
    while (iter()) 
d253 10
a262 2
      CommandScriptElement* item =  (CommandScriptElement*  )iter.key();
     item->PrintToPlan(ostr);
d264 1
a264 1

d282 4
d316 1
@


1.10
log
@Fix the end time of header to reflect the last time of command in DAP
@
text
@d9 1
d11 3
d15 1
a15 1
rcsId[] = "$Id: CommandTL.C,v 1.9 1994/08/19 18:42:49 vick Exp $";
d20 3
d123 1
a123 1
mocMacroDefinitionPlan (ostream& ostr , RelTimeCmds& r)
d135 1
a135 2
ostream&
mocRtsDefinitionPlan (ostream& ostr , RelTimeCmds& r)
d146 1
a146 1
	ostr << *(CommandString*)item;
d148 1
a148 1
	ostr << *(CommandPacket*)item;
d150 1
a150 1
	ostr << *(SCommandPacket*)item;
d238 2
a239 9
      RWCollectable *item = iter.key();
      if (item->isA() == dummyCommandString.isA())
	ostr << *(CommandString*)item;
      else if (item->isA() == dummyCommandPacket.isA())
	ostr << *(CommandPacket*)item;
      else if (item->isA() == dummySCommandPacket.isA())
	ostr << *(SCommandPacket*)item;
//      else 
//	cerr << "SOC INFO: Check this case for " << item->isA() << endl;
@


1.9
log
@Added support for definition plans
@
text
@d11 1
a11 1
rcsId[] = "$Id: CommandTL.C,v 1.8 1994/07/15 21:18:35 vick Exp vick $";
d57 2
a58 2
  ostr << "START=" << mocTime(atc.strtim) << ", " ;
  ostr << "STOP=" << mocTime(atc.endtim) << ";" << endl; 
d73 2
a74 2
  ostr << "START=" << mocTime(atc.strtim) << ", " ;
  ostr << "STOP=" << mocTime(atc.endtim) << ";" << endl; 
a218 15
// add code
    if(swt == 0) 
      { 
	strtim = script->startTime();
        swt = 1;
      }
    int tmptim = script->startTime() ;
    if ( tmptim < strtim )
       {
       strtim = tmptim;
       endtim = strtim; 
       }
    else
       endtim = tmptim;  
// end code
@


1.8
log
@Added manipulators for UpdatePlan
@
text
@d11 1
a11 1
rcsId[] = "$Id: CommandTL.C,v 1.7 1994/07/15 17:53:19 prao Exp vick $";
d116 1
a116 1
mocMacroDefinitionPlan (ostream& ostr)
d122 6
d129 1
a129 1
mocRtsDefinitionPlan (ostream& ostr)
d132 18
d153 5
@


1.7
log
@Made calls to command scripts more generic
..
@
text
@d11 1
a11 1
rcsId[] = "$Id: CommandTL.C,v 1.6 1994/06/17 14:32:57 prao Exp prao $";
d20 64
a106 23
typedef AbsTimeCmds& AbsTimeCmdsref;
// IOMANIPdeclare(AbsTimeCmdsref);

ostream&
mocDailyActivityPlan (ostream& ostr, AbsTimeCmds& atc)
{
  TimeLine::_outputFormat = TimeLine::DAILY_ACTIVITY_PLAN;
  ostr << "HEADER SOURCE=SOC, TYPE=DAILY," << endl;
  ostr << "START=" << mocTime(atc.strtim) << ", " ;
  ostr << "STOP=" << mocTime(atc.endtim) << ";" << endl; 
  return ostr;
}

OMANIP(AbsTimeCmdsref) mocDailyActivityPlan (AbsTimeCmds& r) {
  return OMANIP(AbsTimeCmdsref)(mocDailyActivityPlan,r);
}

ostream&
mocUpdatePlan (ostream& ostr)
{
  TimeLine::_outputFormat = TimeLine::UPDATE_PLAN;
  return ostr;
}
a131 30
ostream&
mocPlanHeader (ostream& ostr, CommandTL& cmds)
{
  switch (TimeLine::_outputFormat) {
  case TimeLine::DAILY_ACTIVITY_PLAN:
    ostr << "HEADER SOURCE=SOC, TYPE=DAILY;" << endl;
    break;
  case TimeLine::UPDATE_PLAN:
    ostr << "HEADER SOURCE=SOC, TYPE=UPDATE;" << endl;
    break;
  case TimeLine::PATCH_PLAN:
    ostr << "HEADER SOURCE=SOC, TYPE=PATCH;" << endl;
    break;
  case TimeLine::MACRO_DEFINITION_PLAN:
    ostr << "HEADER SOURCE=SOC, TYPE=DEFPLN;" << endl;
    ostr << "DEFINE " << cmds.itemName() << ", TYPE=MACRO;" << endl;
    break;
  case TimeLine::RTS_DEFINITION_PLAN:
    ostr << "HEADER SOURCE=SOC, TYPE=DEFPLN;" << endl;
    ostr << "DEFINE " << cmds.itemName() << ", TYPE=RTS;" << endl;
    break;
  default:
    break;
  };
  return ostr;
}

OMANIP(CommandTLref) mocPlanHeader (CommandTLref r) {
  return OMANIP(CommandTLref)(mocPlanHeader,r);
}
@


1.6
log
@Incorporate absolute start and end times of daily and update plans
@
text
@d11 1
a11 1
rcsId[] = "$Id: CommandTL.C,v 1.5 1994/05/26 19:34:05 prao Exp $";
d16 4
d44 1
a44 1
IOMANIPdeclare(AbsTimeCmdsref);
d56 1
a56 1
OMANIP(AbsTimeCmdsref) mocDailyActivityPlan (AbsTimeCmds r) {
d89 1
a89 1
IOMANIPdeclare(CommandTLref);
a202 2
  ostr << mocDailyActivityPlan(cmds);

@


1.5
log
@add isA() for commandScript (temporary fix)
@
text
@d11 1
a11 1
rcsId[] = "$Id:$";
a14 1
CommandScript dummyCommandScript;
d39 3
d43 1
a43 1
mocDailyActivityPlan (ostream& ostr)
d46 3
a48 1
  ostr << "HEADER SOURCE=SOC, TYPE=DAILY;" << endl;
d52 4
d140 1
d173 1
d175 16
a190 1

d199 2
d212 2
a213 4
      else if (item->isA() == dummyCommandScript.isA())
	ostr << *(CommandScript*)item;
      else 
	cerr << "SOC INFO: Check this case for " << item->isA() << endl;
@


1.4
log
@Misc bug fixes
@
text
@d10 2
d15 1
d185 2
@


1.3
log
@elminated expandEntries() method
@
text
@d8 1
d10 1
d173 2
a174 1
    while (iter()) {
d180 4
@


1.2
log
@build 3
@
text
@a32 7
void
CommandTL::expandEntries ()
{
  distributeStartTime();
  flatten();
}

@


1.1
log
@Initial revision
@
text
@d1 3
d6 5
a10 2
#include <iostream.h>
#include <iomanip.h>
d33 7
d46 1
d108 1
a108 2
OMANIP(CommandTLref) mocPlanHeader (CommandTLref r)
{
d178 7
a184 2
  while (iter())
    ostr << *(TimeLine*)iter.key();
@

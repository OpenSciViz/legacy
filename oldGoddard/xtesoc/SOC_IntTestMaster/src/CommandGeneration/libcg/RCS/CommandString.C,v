head	3.10;
access
	joel
	vick
	mmarlowe
	socdev
	soccm;
symbols
	TEST:3.9
	Build4_1:3.8
	Build4:3.8
	Build3_3:3.7
	Build3_2:3.3
	Build3_1:3.2
	Build3:3.2
	Current:3.1
	Build2:2.1;
locks;
comment	@ * @;


3.10
date	97.07.01.21.09.06;	author soccm;	state Exp;
branches;
next	3.9;

3.9
date	95.08.25.20.25.02;	author joel;	state Exp;
branches;
next	3.8;

3.8
date	94.11.03.22.10.37;	author vick;	state Exp;
branches;
next	3.7;

3.7
date	94.10.13.18.01.47;	author vick;	state Exp;
branches;
next	3.6;

3.6
date	94.09.19.12.18.18;	author vick;	state Exp;
branches;
next	3.5;

3.5
date	94.09.09.01.14.53;	author vick;	state Exp;
branches;
next	3.4;

3.4
date	94.08.19.18.39.14;	author vick;	state Exp;
branches;
next	3.3;

3.3
date	94.07.20.17.46.23;	author prao;	state Exp;
branches;
next	3.2;

3.2
date	94.02.24.21.04.37;	author herreid;	state Exp;
branches;
next	3.1;

3.1
date	94.02.18.14.31.32;	author herreid;	state Exp;
branches;
next	3.0;

3.0
date	94.01.26.20.50.21;	author herreid;	state Exp;
branches;
next	2.1;

2.1
date	93.11.16.20.14.33;	author herreid;	state Release;
branches;
next	2.0;

2.0
date	93.11.03.16.33.34;	author herreid;	state Exp;
branches;
next	1.2;

1.2
date	93.10.21.20.54.45;	author herreid;	state Exp;
branches;
next	1.1;

1.1
date	93.10.19.20.55.05;	author herreid;	state Exp;
branches;
next	;


desc
@Command Mnemonic string
@


3.10
log
@added rcs string
@
text
@// Project:	 XTE SOC
// Name:	 CommandString.C
// Subsystem:	 Command Generation
// Programmer:	 vick, joel
// Description:	 CommandString class for building commands from strings.

#include "iostream.h"
#include "iomanip.h"

#include "CommandString.h"

static const char* const rcsid =
"$Id: CommandString.C,v 3.9 1995/08/25 20:25:02 joel Exp $";

CommandString::CommandString ()
{
  _command = "";
}

CommandString::CommandString (short time, const char* string,
			      const char* descr)
         : CommandScriptElement (time)
{
  setDescription (descr);
  _command = string;
}

ostream&
CommandString::PrintToPlan(ostream& ostr) {
  switch (TimeLine::_outputFormat) {
  case TimeLine::DAILY_ACTIVITY_PLAN:
  case TimeLine::UPDATE_PLAN:
  case TimeLine::PATCH_PLAN:
    ostr << "    " << this->commandString() << ", " 
         << "TIME=" << mocTime(this->startTime()) << ";" << endl;
    break;
  case TimeLine::MACRO_DEFINITION_PLAN:
  case TimeLine::RTS_DEFINITION_PLAN:
    ostr << "    " <<  this->commandString() << ", "
      << "DELTA=" << mocDeltaTime(this->startTime()) << ";" << endl;
    break;
  default:
    ostr << this->commandString() << endl;
    break;
  };
  return ostr;
}

ostream&
operator<< (ostream& ostr, CommandString& cmd)
{
  return ( cmd.PrintToPlan(ostr));
}

RWDEFINE_COLLECTABLE(CommandString,__COMMANDSTRING)

RWspace
CommandString::binaryStoreSize () const
{
  return _command.binaryStoreSize();
}

void
CommandString::restoreGuts(RWFile& f)
{
  TimeLine::restoreGuts(f);
  _command.restoreFrom(f);
}

void
CommandString::restoreGuts(RWvistream& s)
{
  TimeLine::restoreGuts(s);
  _command.restoreFrom(s);
}

void
CommandString::saveGuts(RWFile& f)     const
{
  TimeLine::saveGuts(f);
  _command.saveOn(f);
}

void
CommandString::saveGuts(RWvostream& s) const
{
  TimeLine::saveGuts(s);
  _command.saveOn(s);
}

#ifdef TEST_COMMANDSTRING
#include "CommandTL.h"
main ()
{
  // Make a command string
  CommandString cmd (23, "SLEW RA=23.3, DEC=-44.21", "test command");
  // Print out as a daily activity plan type of object
  TimeLine::_outputFormat = TimeLine::DAILY_ACTIVITY_PLAN;
  cmd.PrintToPlan(cout);
  cout << cmd;
}
#endif
@


3.9
log
@Added header and updated test.
@
text
@a6 2


d12 2
a13 2
static const char 
rcsId[] = "$Id: CommandString.C,v 3.8 1994/11/03 22:10:37 vick Exp joel $";
@


3.8
log
@AddPrintToPlan method
@
text
@d1 8
d15 1
a15 1
rcsId[] = "$Id: CommandString.C,v 3.7 1994/10/13 18:01:47 vick Exp vick $";
d97 5
a101 2
  CommandString cmd (23, "RA=23.3, DEC=-44.21", "test command");
  cout << mocDailyActivityPlan;
@


3.7
log
@Added TimeLine::PATCH_PLAN as a option for output of opertor<<
@
text
@d7 1
a7 1
rcsId[] = "$Id: CommandString.C,v 3.6 1994/09/19 12:18:18 vick Exp vick $";
d16 1
a16 1
: TimeLine (time)
d23 1
a23 2
operator<< (ostream& ostr, CommandString& cmd)
{
d28 2
a29 9
    ostr << cmd.commandString() << ", ";
    // If the command has zero start time then do not print out the 
    // time. Added to support literal command passed through to the MOC
    // (i.e RTSLOAD, UPLINKS etc) SDV 9/19/94
    if (cmd.startTime()) {
      ostr << ", "  << "TIME=" << mocTime(cmd.startTime()) << ";" << endl;
    }
    else
      ostr << endl;
d33 2
a34 2
    ostr << cmd.commandString() << ", "
      << "DELTA=" << mocDeltaTime(cmd.startTime()) << ";" << endl;
d37 1
a37 1
    ostr << cmd.commandString() << endl;
d42 6
@


3.6
log
@Added if statement in output to support literal command pass through
@
text
@d7 1
a7 1
rcsId[] = "$Id: CommandString.C,v 3.5 1994/09/09 01:14:53 vick Exp vick $";
d28 1
@


3.5
log
@added support for defn plans
@
text
@d7 1
a7 1
rcsId[] = "$Id: CommandString.C,v 3.4 1994/08/19 18:39:14 vick Exp vick $";
d28 9
a36 2
    ostr << cmd.commandString() << ", "
      << "TIME=" << mocTime(cmd.startTime()) << ";" << endl;
@


3.4
log
@Added support for definition plans.
@
text
@d7 1
a7 1
rcsId[] = "$Id: CommandString.C,v 3.3 1994/07/20 17:46:23 prao Exp vick $";
d28 3
d34 1
a34 1
      << "TIME=" << mocTime(cmd.startTime()) << ";" << endl;
d37 1
@


3.3
log
@Update for RW 6.0
@
text
@d7 1
a7 1
rcsId[] = "$Id: CommandString.C,v 3.2 1994/02/24 21:04:37 herreid Exp $";
d28 4
a31 2
    ostr << "TIME=" << mocTime(cmd.startTime()) << ", "
      << cmd.commandString() << ";" << endl;
@


3.2
log
@eliminated reference to CGtime.h
@
text
@d7 1
a7 1
rcsId[] = "$Id: CommandString.C,v 3.1 1994/02/18 14:31:32 herreid Exp herreid $";
d39 1
a39 1
unsigned
@


3.1
log
@build 3
@
text
@a4 1
#include "CGtime.h"
d7 1
a7 1
rcsId[] = "$Id: CommandString.C,v 3.0 1994/01/26 20:50:21 herreid Exp herreid $";
@


3.0
log
@NewBuild
@
text
@d1 3
d5 1
d8 1
a8 1
rcsId[] = "$Id: CommandString.C,v 2.1 1993/11/16 20:14:33 herreid Release herreid $";
d23 15
d73 10
@


2.1
log
@added rcsId[]
@
text
@d4 1
a4 1
rcsId[] = "$Id$";
@


2.0
log
@NewBuild
@
text
@d3 3
@


1.2
log
@distinguished between absolute and relative time
@
text
@@


1.1
log
@Initial revision
@
text
@d8 2
a9 1
CommandString::CommandString (int time, const char* string, const char* descr)
@

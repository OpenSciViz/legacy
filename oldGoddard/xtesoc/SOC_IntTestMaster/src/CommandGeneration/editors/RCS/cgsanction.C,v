head	1.9;
access
	socdev
	subrata
	soccm;
symbols;
locks
	subrata:1.9; strict;
comment	@ * @;


1.9
date	97.03.18.20.00.02;	author subrata;	state Exp;
branches;
next	1.8;

1.8
date	96.07.02.19.42.36;	author socdev;	state Exp;
branches;
next	1.7;

1.7
date	96.02.07.17.39.16;	author mmarlowe;	state Exp;
branches;
next	1.6;

1.6
date	96.02.02.21.37.30;	author mmarlowe;	state Exp;
branches;
next	1.5;

1.5
date	96.02.02.17.38.27;	author mmarlowe;	state Exp;
branches;
next	1.4;

1.4
date	96.01.22.08.34.34;	author mmarlowe;	state Exp;
branches;
next	1.3;

1.3
date	96.01.06.13.31.14;	author mmarlowe;	state Exp;
branches;
next	1.2;

1.2
date	95.12.19.17.22.59;	author mmarlowe;	state Exp;
branches;
next	1.1;

1.1
date	95.12.18.18.38.32;	author mmarlowe;	state Exp;
branches;
next	;


desc
@@


1.9
log
@this version uses mission day instead of day of year for output DAP
files.
@
text
@//=================================================================================
// Project:	 XTE SOC
// Name:	 cgsanction.C
// Subsystem:	 Command Generation
// Programmer:	 Matthew Marlowe, Hughes STX
// Description:	 Program used to create symbolic links or copy files used in the
//               sanctioning process.
//               
//
// $Id: cgsanction.C,v 1.8 1996/07/02 19:42:36 socdev Exp $
//
//====================================================================================

static const char 
rcsId[] = "$Id: cgsanction.C,v 1.8 1996/07/02 19:42:36 socdev Exp $";


// System Include Files
#include <unistd.h>
#include <iostream.h>
#include <strstream.h>
#include <limits.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <utime.h>
// 
#include <TimeLine.h>
#include <new.h>


// Rogue Wave Include Files
#include <rw/collint.h>
#include <rw/cstring.h>
#include <rw/hashdict.h>
#include <rw/regexp.h>
#include <rw/rwdate.h>
#include <rw/rwtime.h>
// included temporarily to overcome libcgeds problem
#include <rw/tpsrtvec.h>

// CG Include Files
#include <DesiredAbsTL.h>
#include <Macro.h>
#include <ObjectFiler.h>
#include <PredictedAbsTL.h>
#include <cgError.h>
#include "dummyconfigs.h"

// include the Path directory header file & SOCTime header file
#include <path.h>
#include <SOCTime2.h>

//
// static dummy variables used to instantiate possibly used objects
//

PredictedAbsTL   dummyPrt;
DesiredAbsTL     dummyDtl;
Macro            dummyMacro;
RWHashDictionary dummyDictionary;


static char * sops = getenv("SOCOPS");
// 
// Forward Declaration Of Functions
//

void             display_cgsanction_syntax(                                             );

void             sancDap(                    Path                                      );
void             sancUpd(                    Path                                      );
void             sancPrt(                    Path , int sanct_type                               );
void             sancInsert(                 Path,  int sanct_type                     );
void             sancPprts(                  Path, RWHashDictionary *,   Path );
void             sancShell(                  Path );

Path             getDapSancPath(             unsigned int dayOfDap                     );
Path             getUpdSancPath(             unsigned int dayOfUpd                     );
Path             makePrtSancPath(            unsigned int week_no,   int sanc_type     );
Path             makeInsertSancPath(         unsigned int week_no,   int sanc_type     );
Path             makePprtSancFileName(       int sanc_type                              );
void             symlink_pprt_files(  RWCString prtName, PredictedAbsTL *, Path         );

static unsigned  _determineDay(              SOCTime2 * nextTime                        );

int              helpArgPresent(             char* []                                   );

unsigned int     getMissionWeekFromPrt(      RWHashDictionary *          );

unsigned int     getMissionWeekFromInsert(   Path                        );

main (int argc, char *argv[])
{

  set_new_handler(free_store_exception);

  // cgsanction
  if (sops == NULL ) {
	cerr << "cgsanction::socops environment variable not set." << endl;
        exit(-1);
  } else {
    Path sdir(sops);
    sdir.append("/command/sanction/");
    mkdir( sdir.data(), 0755 );
  }

  //
  // cgsanction   filename.DAP
  // cgsanction   [ -cg | -rtc ] filename.prt
  //
  // The DAP file must be located in /socops/command/staging.
  // The PRT file must be located in /socops/command/Day0000
  //
  // User must provide filename suffix, so cgsanction can determine type of sanctioning.
  //


  // Do they want a syntax check only?

  if ((argc == 1)|| helpArgPresent(argv) ) {
    display_cgsanction_syntax();
    exit(950);
  }

  // Parse the command line elements.

  Path theFileName;
  Path completePath;

  int  sanct_type = 0;    // 0 = cg, 1 = rtc

  //
  // FILETYPE:
  //
  // 0 = unknown
  // 1 = dap
  // 2 = prt 
  // 3 = upd
  // 4 = insert
  // 5 = csh

  int  fileType   = 0; 

   // 0 = unknown, 1 = dap, 2 = prt, 3 = upd, 4=insert

  while (*++argv) {

    if (**argv != '-') {
      theFileName = *argv;

      if (!(strcmp(theFileName.suffix(), "prt"))) {
	fileType=2;
	completePath = theFileName.filename();
        completePath.prepend("/command/Day0000/");
	completePath.prepend(sops);
	completePath.append(".prt");
      }

      if (!(strcmp(theFileName.suffix(), "DAP"))) {
	fileType=1;
	completePath = theFileName.filename();
	completePath.prepend("/command/staging/");
	completePath.prepend(sops);
	completePath.append(".DAP");
      }

      if (!(strcmp(theFileName.suffix(), "UPD"))) {
	fileType=3;
	completePath = theFileName.filename();
	completePath.prepend("/command/staging/");
	completePath.prepend(sops);
	completePath.append(".UPD");
      }

      if ( (!(strcmp(theFileName.suffix(),"ins")))    ||
	   (!(strcmp(theFileName.suffix(),"insert"))) 
         ) {
	fileType=4;
	completePath = theFileName;
      }

      if ( !(strcmp( theFileName.suffix(), "csh")) ) {
	fileType=5;
	completePath = theFileName;
      }

    }

    else if (!strcmp(*argv, "-rtc")) {
      sanct_type=1;
    }

  }

  switch( fileType ) {
    case( 0 ) :
      cerr << "cgsanction :: error :: unknown file type." << endl;
      cerr << "Sanctioned file must have a suffix of .prt or .DAP" << endl;
      exit(-1);
    case ( 1 ) :
      sancDap( completePath );
      break;
    case( 2 ) :
      sancPrt( completePath, sanct_type );
      break;
    case( 3 ) :
      sancUpd( completePath );
      break;
    case( 4 ) :
      sancInsert( completePath, sanct_type );
      break;
    case( 5 ) :
      sancShell( completePath );
      break;

  };

  exit(0);
}

void
sancDap( Path completePath )
{
  unsigned missionday;

  Path fname( completePath.filename() ); 

  int length=0;
  while((fname[5 + length] != '_' ) && (length < 5 ) ) {
	length++;
  }

  if ((length==0) || (length > 4)) {
    cerr << "cgsanction::DAP FileName does not follow standard DAP Filename conventions." << endl;
    exit(-1);
  }

  RWCString dayOfDap = fname( 5, length );
  RWCString yearOfDap = fname(1, 4 );

  RWDate date0(atoi(dayOfDap.data()), atoi(yearOfDap.data()));
  RWTime time0(date0,0,0,0);

  SOCTime2 *time1;
  time1 = new SOCTime2(time0, 0.0);

  missionday = time1->getMissionDay();

  Path sancPath = getDapSancPath( missionday );

  cerr << completePath.data() << " ---> " <<  sancPath.data() << endl;
 
  mkdir(sancPath.directory(),0755);
  unlink(sancPath.data());
  if ( link( completePath.data(), sancPath.data() ) != 0 )
    perror("cgsanction:link:"); 
  if ( utime( sancPath.data(), NULL ) != 0 )
    perror("cgsanction:utime:");
}

void
sancShell( Path completePath )
{

  RWCRegexp    dayReg("_[0-9]+_");
  RWCSubString dayMatch = completePath( dayReg );

  if ( dayMatch == "" ) {
    cerr << "cgsanction : error : unable to retrieve week number from name of shell script." << endl;
    exit(-1);
  }

  int week_no = atoi( dayMatch.data() ); 
  
  Path sancPath("/command/sanction/csh/");

  sancPath.prepend(sops);
  sancPath.append("Week");

  strstream weeknum;
  weeknum << setfill('0') << setw(3) << week_no;
  
  sancPath.append( weeknum.str() );
  sancPath.append( "_sanct.csh" );

  cerr << completePath.data() << " ---> " <<  sancPath.data() << endl;
 
  mkdir(sancPath.directory(),0755);
  unlink(sancPath.data());

  if ( link( completePath.data(), sancPath.data() ) != 0 )
    perror("cgsanction:link:"); 
  if ( utime( sancPath.data(), NULL ) != 0 )
    perror("cgsanction:utime:");

}

void
sancUpd( Path completePath )
{

  unsigned missionday;

  Path fname( completePath.filename() ); 

  int length=0;
  while((fname[5 + length] != '_' ) && (length < 5 ) ) {
	length++;
  }

  if ((length==0) || (length > 4)) {
    cerr << "cgsanction::UPD FileName does not follow standard DAP Filename conventions." << endl;
    exit(-1);
  }

  RWCString dayOfDap = fname(5, length );
  RWCString yearOfDap = fname(1, 4 );

  RWDate date0(atoi(dayOfDap.data()), atoi(yearOfDap.data()));
  RWTime time0(date0,0,0,0);

  SOCTime2 *time1;
  time1 = new SOCTime2(time0, 0.0);

  missionday = time1->getMissionDay();

  Path sancPath = getDapSancPath( missionday );

  cerr << completePath.data() << " ---> " <<  sancPath.data() << endl;
  mkdir(sancPath.directory(),0755);
  unlink(sancPath.data());
  if ( link( completePath.data(), sancPath.data() ) != 0 )
    perror("cgsanction:link:"); 
  if ( utime( sancPath.data(), NULL ) != 0 )
    perror("cgsanction:utime:");
}

void
sancPrt( Path completePath, int sanc_type )
{

  RWHashDictionary * theDictionary = NULL;
  ObjectFiler dictionaryFiler;

  theDictionary =
    (RWHashDictionary * ) dictionaryFiler.restoreFrom(completePath.data() );

  unsigned int week_no = getMissionWeekFromPrt( theDictionary );

  Path sancPath = makePrtSancPath( week_no, sanc_type );
  Path spPath   = makePprtSancFileName( sanc_type );

  cerr << completePath.data() << " ----> " << sancPath.data() << endl;

  mkdir(sancPath.directory(),0755);
  unlink(sancPath.data());
  if (link( completePath.data(), sancPath.data() ) != 0 ) {
    perror("cgsanction::link:");
  }
  if (utime( sancPath.data(), NULL ) != 0 ) 
    perror("cgsanction::utime:");

  sancPprts( completePath,  theDictionary, spPath  );

}


void
sancInsert( Path completePath, int sanc_type )
{
  
  unsigned int week_no = getMissionWeekFromInsert( completePath );

  Path sancPath = makeInsertSancPath( week_no, sanc_type );

  cerr << completePath.data() << " ----> " << sancPath.data() << endl;

  mkdir(sancPath.directory(),0755);
  unlink(sancPath.data());
  if (link( completePath.data(), sancPath.data() ) != 0 ) {
    perror("cgsanction::link:");
  }
  if (utime( sancPath.data(), NULL ) != 0 ) 
    perror("cgsanction::utime:");

}


void 
sancPprts( Path thePrtName, RWHashDictionary* theDictionary, Path spPath ) 
{
   PredictedAbsTL* thePrt;
   ObjectFiler pprtFiler;
   
   thePrtName.change_suffix("pprt");
   spPath.change_suffix("pprt");

   RWHashDictionaryIterator iter (*theDictionary);
   RWCollectableInt *subsystemId;

   PredictedAbsTL *TablePTL[30];

  // Set the place corresponding to each SubSystem ID to NULL
  for (unsigned i = 0 ; i < 30; i++) 
    TablePTL[i]  = NULL;
   
   while (subsystemId = (RWCollectableInt*)iter() ) {
      thePrt = (PredictedAbsTL*)iter.value();

      // Note:  EDS  predicted  timelines are  mixed, 
      // sort out the individual Subsystems (EAs and PRAMA, PRAMB)
    
     if ((int) *subsystemId == EDS) {
       SubsystemId ids;
       TimeLineIterator iter (*thePrt);
       TimeLine *item;
       while (item = (TimeLine*) iter() ) {
	 ids = item->getSubsystemId();
	 // make sure that the Predicted Timeline exists for that subsys id
	 if (TablePTL[(int)ids] == NULL)
	   TablePTL[(int)ids] = new PredictedAbsTL;
	 // insert the predicted config
	 TablePTL[(int)ids]->insert(item);
       }
     }
     else if ((int) *subsystemId != NONE)
       TablePTL[(int) *subsystemId] = thePrt;
    }
   // OK now for each index in the table of Subsystems that 
   // isn't null write out indivdual days
   for (int j = 0 ; j < 30 ; j++){
     if (TablePTL[j] != NULL){
       symlink_pprt_files(thePrtName.data(),TablePTL[j], spPath );
     }
   }
 }

// Local Function to determine the day
static unsigned _determineDay (SOCTime2 *nextTime){
  unsigned misSec;
  nextTime->getMET(misSec);

  RWTime time0 (RWDate(1,1993),0,0,0);
  time0 += misSec;
  RWDate date (time0);

  return((unsigned )date.day());

}

void symlink_pprt_files(RWCString thePrtName, PredictedAbsTL* thePrt, Path spPath) {

  PredictedAbsTL* daily = new PredictedAbsTL;
  ObjectFiler pprtFiler;

  TimeLineIterator iter (*thePrt);
  TimeLine *item                 ;
  unsigned missionDay            ;
  unsigned prev                  ;

  SOCTime2 *Time     = NULL;
  SOCTime2 *nextTime = NULL;

  prev = 0;

  // loop over the values and break them up by day
  while (item = (TimeLine*) iter() ) {
    delete nextTime;
    nextTime = new SOCTime2 ((unsigned )item->startTime(),(unsigned )0, (unsigned )0) ;
    // when the day changes - write out the predicted config
    missionDay = _determineDay(nextTime);
    //cout << "Mission Day " << missionDay << endl;

    if (prev  && (missionDay != prev)){
      pprtFiler.pprt_symlink( thePrtName, daily, spPath);
      delete daily;
      daily = new PredictedAbsTL;
    }
    // insert the config
    daily->insert(item);
    // remember the day
    prev = missionDay;
  }

  pprtFiler.pprt_symlink( thePrtName, daily, spPath);

}


void 
display_cgsanction_syntax() 
{
  cerr << "Description: cgsanction is used to sanction XTE-SOC cg products." << endl;
  cerr << "Usage:       cgsanction  filename.DAP" << endl;
  cerr << "             cgsanction  filename.UPD" << endl;
  cerr << "             cgsanction  filename.csh" << endl;
  cerr << "             cgsanction  [ -cg | -rtc ] filename.[ins,insert]" << endl;
  cerr << "             cgsanction  [ -cg | -rtc ] filename.prt " << endl;
  cerr << endl;

}

Path
getDapSancPath(    unsigned int dayOfDap         )
{

  Path dapName;

  dapName.append(sops);
  dapName.append("/command/sanction/dap/day");
  
  strstream daynum;

  daynum  << setfill('0') << setw(4) << dayOfDap;
  dapName += daynum.str();
  dapName += "_sanct.dap";

  return( dapName );

}

Path
getUpdSancPath(    unsigned int dayOfDap         )
{

  Path dapName;

  dapName.append(sops);
  dapName.append("/command/sanction/upd/day");
  
  strstream daynum;

  daynum  << setfill('0') << setw(4) << dayOfDap;
  dapName += daynum.str();
  dapName += "_sanct.upd";

  return( dapName );

}

Path
makePrtSancPath(   unsigned int week_no, int sanc_type  )
{

  Path prtName;

  prtName.append(sops);
  prtName.append("/command/sanction/prt/Week");

  strstream weeknum;
  
  weeknum << setfill('0') << setw(3) << week_no;
  prtName += weeknum.str();

  if (sanc_type)
    prtName.append("_rtc_sanct.prt");
  else
    prtName.append("_cg_sanct.prt");

  return( prtName );

}


Path
makeInsertSancPath(   unsigned int week_no, int sanc_type  )
{

  Path prtName;

  prtName.append(sops);
  prtName.append("/command/sanction/insert/Week");

  strstream weeknum;
  
  weeknum << setfill('0') << setw(3) << week_no;
  prtName += weeknum.str();

  if (sanc_type)
    prtName.append("_rtc_sanct.ins");
  else
    prtName.append("_cg_sanct.ins");

  return( prtName );

}

Path
makePprtSancFileName(  int sanc_type      )
{

  if (sanc_type)
    return( Path("rtc_sanctioned.pprt"));
  else
    return( Path("cg_sanctioned.pprt") );

}


unsigned int
getMissionWeekFromPrt( RWHashDictionary * theDictionary )
{

  PredictedAbsTL *   thePrt;
  RWCollectableInt * subsystemId;
  unsigned int week_no;
  unsigned int earliestTime = UINT_MAX;
  unsigned int currentTime =  0 ;

  RWHashDictionaryIterator next( *theDictionary );
  while( next() ) {
    thePrt = (PredictedAbsTL *) next.value();
    subsystemId = (RWCollectableInt *) next.key();
    if (thePrt->entries() != 0 ) {
      if (subsystemId != NONE ) {
	currentTime = (unsigned) thePrt->firstTime();
	//cerr << "firstTime=" << currentTime << endl;
	if (currentTime < earliestTime ) earliestTime = currentTime;
      }
    }
  }
 
  if ( earliestTime != UINT_MAX ) {
    SOCTime2 mt( earliestTime, (unsigned) 0, (unsigned) 0 );
    week_no = mt.getMissionWeek();
    return( week_no );
  } else {
    cerr << "cgsanction: the predicted timeline is empty." << endl;
    cerr << "cgsanction: unable to determine mission week of timeline." << endl;
    cerr << "cgsanction: -- EXITING -- " << endl;
    exit(-1);
  }


}

unsigned int
getMissionWeekFromInsert( Path completePath )
{

 
  ifstream   ifstr( completePath.data() );
  RWCString  fileStr;

  fileStr.readFile( ifstr );

  RWCRegexp mTimeReg("^[0-9]+:[0-9]+:[0-9]+:[0-9]+:[0-9]+");
  
  RWCString mTimeStr = fileStr(mTimeReg);

  if ( mTimeStr == "" ) {
    cerr << "cgsanction : error : could not find a moc time in insert file." << endl;
    exit(-1);
  }

  SOCTime2 mSocTime;
  if ( mSocTime.parse( mTimeStr ) != 0 ) {
    cerr << "cgsanction : error : could not parse moc time. " << endl;
    exit(-1);
  }

  return( mSocTime.getMissionWeek() );

}

  


@


1.8
log
@*** empty log message ***
@
text
@d10 1
a10 1
// $Id: cgsanction.C,v 1.7 1996/02/07 17:39:16 mmarlowe Exp socdev $
d15 1
a15 1
rcsId[] = "$Id: cgsanction.C,v 1.7 1996/02/07 17:39:16 mmarlowe Exp socdev $";
d225 1
a227 1
//  cerr << "Complete Path: " << completePath << endl;
a230 1
//        cerr << fname[5+length];
d239 2
a240 1
  RWCSubString dayOfDap = fname( 5, length );
d242 9
a250 1
  Path sancPath = getDapSancPath( atoi(dayOfDap.data()) );
d303 2
a305 1
//  cerr << "Complete Path: " << completePath << endl;
a308 1
//        cerr << fname[5+length];
d317 10
a326 1
  RWCSubString dayOfDap = fname( 5, length );
d328 1
a328 1
  Path sancPath = getUpdSancPath( atoi(dayOfDap.data()) );
@


1.7
log
@added sanctioning of shell scripts
@
text
@d10 1
a10 1
// $Id: cgsanction.C,v 1.4 1996/01/22 08:34:34 mmarlowe Exp $
d15 1
a15 1
rcsId[] = "$Id: cgsanction.C,v 1.4 1996/01/22 08:34:34 mmarlowe Exp $";
d29 1
d47 1
d95 2
@


1.6
log
@*** empty log message ***
@
text
@d74 1
a80 1

d128 14
a141 1
  int  fileType   = 0;    // 0 = unknown, 1 = dap, 2 = prt, 3 = upd, 4=insert
d173 2
a174 2
	   (!(strcmp(theFileName.suffix(),"insert"))) ||
	   (!(strcmp(theFileName.suffix(),"cur")))    ) {
d178 6
a183 1
      
d209 3
d251 37
d473 6
a478 5

  cerr << "Usage:  cgsanction  filename.DAP" << endl;
  cerr << "        cgsanction  filename.UPD" << endl;
  cerr << "        cgsanction  [ -cg | -rtc ] filename.[ins,cur,insert]" << endl;
  cerr << "        cgsanction  [ -cg | -rtc ] filename.prt " << endl;
@


1.5
log
@allow for sanction of update plans
@
text
@d72 1
d78 1
d89 2
d128 1
a128 1
  int  fileType   = 0;    // 0 = unknown, 1 = dap, 2 = prt, 3 = upd
d158 7
d188 4
d277 1
a277 1
  mkdir(sancPath.directory(),0555);
d290 21
d418 1
d485 24
d558 9
d568 8
d577 11
@


1.4
log
@Modified to support latest ptl/cgsanction problems + other bug fixes
@
text
@d10 1
a10 1
// $Id: cgsanction.C,v 1.2 1995/12/19 17:22:59 mmarlowe Exp $
d15 1
a15 1
rcsId[] = "$Id: cgsanction.C,v 1.2 1995/12/19 17:22:59 mmarlowe Exp $";
d70 1
d75 1
d94 4
d99 1
d124 1
a124 1
  int  fileType   = 0;    // 0 = unknown, 1 = dap, 2 = prt
d147 8
d163 15
a177 13
  if( fileType == 0 ) {
    cerr << "cgsanction :: error :: unknown file type." << endl;
    cerr << "Sanctioned file must have a suffix of .prt or .DAP" << endl;
    exit(-1);
  }
 
  if( fileType == 1 ) {
    sancDap( completePath );
  }

  if( fileType == 2 ) {
    sancPrt(  completePath, sanct_type );
  }
d205 33
a237 1
  mkdir(sancPath.directory(),0555);
d381 1
d406 18
@


1.3
log
@hardlink not symlink
@
text
@d26 1
d73 2
a74 2
Path             getDapSancPath(             unsigned long dayOfDap                     );
Path             makePrtSancPath(            unsigned long week_no,   int sanc_type     );
d83 1
d190 1
a190 1
  if (link( completePath.data(), sancPath.data() ) != 0 )
d192 2
a193 1
  
d200 2
a201 5
  RWHashDictionary * theDictionary;
  PredictedAbsTL *   thePrt;
  ObjectFiler        dictionaryFiler;

  unsigned long week_no;
d206 1
a206 7
  RWHashDictionaryIterator next( *theDictionary );
  next();

  thePrt = (PredictedAbsTL *) next.value();

  SOCTime2 mt = (double) thePrt->lastTime();
  week_no     = mt.getMissionWeek();
d211 1
a211 1
    cerr << completePath.data() << " ----> " << sancPath.data() << endl;
d218 2
a222 1
//   cerr << "prt sanction done" << endl;
d225 1
d338 1
a338 1
getDapSancPath(    unsigned long dayOfDap         )
d358 1
a358 1
makePrtSancPath(   unsigned long week_no, int sanc_type  )
d390 41
@


1.2
log
@*** empty log message ***
@
text
@d10 1
a10 1
// $Id: cgsanction.C,v 1.1 1995/12/18 18:38:32 mmarlowe Exp $
d15 1
a15 1
rcsId[] = "$Id: cgsanction.C,v 1.1 1995/12/18 18:38:32 mmarlowe Exp $";
d188 2
a189 2
  if (symlink( completePath.data(), sancPath.data() ) != 0 )
    perror("cgsanction:symlink:"); 
d221 2
a222 2
  if (symlink( completePath.data(), sancPath.data() ) != 0 ) {
    perror("cgsanction::symlink:");
@


1.1
log
@Initial revision
@
text
@d10 1
a10 1
// $Id: ptl.C,v 1.3 1995/12/14 11:26:30 mmarlowe Exp $
d15 1
a15 1
rcsId[] = "$Id: ptl.C,v 1.3 1995/12/14 11:26:30 mmarlowe Exp $";
d24 2
a25 1

d61 1
d68 3
a70 3
void             sancDap(                    Path &                                     );
void             sancPrt(                    Path &                                     );
void             sancPprts(                  unsigned long week_no,   int sanc_type     );
d76 1
a76 1
void             symlink_pprt_files(  RWCString prtName, PredictedAbsTL &, Path         );
d87 4
a90 1

d125 3
a127 1
	completePath.prepend("/socops/command/Day0000/");
d133 3
a135 1
	completePath.prepend("/socops/command/staging/");
d157 1
a157 1
    sancPrt(  completePath, sanc_type );
d167 3
d171 4
a174 1
  while((completePath[5 + length] != '_' ) && (length < 5 ) ) length++;
d181 1
a181 1
  RWCSubString dayOfDap = completePath( 5, length );
d183 1
a183 1
  Path sancPath = getDapSancPath( dayOfDap );
d185 5
a189 1
  symlink( completePath.data(), sancPath.data() );
d204 1
a204 1
    (RWHashDictionary * ) ObjectFiler.restoreFrom(completePath.data() );
d206 1
a206 1
  RWHashDictionary next( *thePrt );
d211 1
a211 1
  SOCTime2 mt = thePrt->lastTime();
d217 7
a223 1
  symlink( completePath.data(), sancPath.data() );
d227 1
d237 2
d316 1
a316 1
      pprtFiler.symlink( thePrtName, daily, spPath);
d326 1
a326 1
  pprtFiler.symlink( thePrtName, daily, spPath);
d347 2
a348 1
  dapName.append("$SOCOPS/command/sanction/dap/day");
d353 1
a353 1
  dapName += weeknum.str();
d367 2
a368 1
  prtName.append("$SOCOPS/command/sanction/Week");
d372 1
a372 1
  weeknum << setfill('0') << setw(3) << weeknum;
@

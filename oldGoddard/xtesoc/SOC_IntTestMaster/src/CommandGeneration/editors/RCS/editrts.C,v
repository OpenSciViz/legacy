head	3.11;
access
	joel
	mmarlowe
	socdev
	subrata
	soccm;
symbols
	Build4_1:3.8
	Build4:3.7
	Build3_3:3.5
	Build3_2:3.0
	Build3_1:3.0
	Build3:3.0
	Current:3.0
	Build2:2.0;
locks; strict;
comment	@ * @;


3.11
date	96.01.06.12.37.38;	author mmarlowe;	state Exp;
branches;
next	3.10;

3.10
date	95.10.13.19.23.23;	author mmarlowe;	state Exp;
branches;
next	3.9;

3.9
date	95.10.13.19.14.14;	author mmarlowe;	state Exp;
branches;
next	3.8;

3.8
date	95.02.27.13.23.32;	author vick;	state Exp;
branches;
next	3.7;

3.7
date	95.01.17.14.54.33;	author vick;	state Exp;
branches;
next	3.6;

3.6
date	95.01.17.14.51.09;	author vick;	state Exp;
branches;
next	3.5;

3.5
date	94.10.07.20.51.43;	author vick;	state Exp;
branches;
next	3.4;

3.4
date	94.10.04.15.05.31;	author vick;	state Exp;
branches;
next	3.3;

3.3
date	94.09.09.01.02.48;	author vick;	state Exp;
branches;
next	3.2;

3.2
date	94.09.08.14.44.53;	author vick;	state Exp;
branches;
next	3.1;

3.1
date	94.08.19.19.02.28;	author vick;	state Exp;
branches;
next	3.0;

3.0
date	94.02.18.19.30.45;	author herreid;	state Exp;
branches;
next	2.0;

2.0
date	93.11.30.20.00.49;	author herreid;	state Release;
branches;
next	1.2;

1.2
date	93.11.16.17.53.34;	author herreid;	state Exp;
branches;
next	1.1;

1.1
date	93.11.05.22.37.05;	author herreid;	state Exp;
branches;
next	;


desc
@RTS editor
@


3.11
log
@modified to support pseudo desired configurations
@
text
@// Project:	 XTE SOC
// Name:	 editrts.C
// Subsystem:	 Command Generation
// Programmer:	 vick
// Description:	 command line version of the editor to create an Rts
//               persistent object.

static const char* rcsID="$Id: editrts.C,v 3.10 1995/10/13 19:23:23 mmarlowe Exp $";

#include <unistd.h>
#include <string.h>
#include <iostream.h>
#include <strstream.h>
#include <rw/cstring.h>
#include <ObjectFiler.h>
#include <Rts.h>
#include <path.h>
#include "dummyconfigs.h"

#ifndef OMIT_HEXTE
#include <HEXTEDesiredConfig.h>
HEXTEDesiredConfig hexteDummy;
#endif

#ifndef OMIT_PCA
#include <PCADesiredConfig.h>
PCADesiredConfig   pcaDummy;
#endif

void display_editrts_syntax();

main (int argc , char* argv[])
{
  RWCString      entryName("");
  short          entryTime;

  DesiredConfig* desiredConfig;
  CommandScript* commandScript;

  Path      thisRtsName("");
  Rts*           thisRts;

  ObjectFiler    filer;
  Rts            rtsDummy;

  unsigned int   removeNumber = 0;
  int            changes      = FALSE;
  int            createFlag   = FALSE;

  CommandScript dummyCommandScript;

  if (argc == 1) {
    display_editrts_syntax();
    exit(950);
  }


  while (*++argv) {

    // Retrieve the name and time of the desired configuration to insert
    // into this rts.

    if (!strcmp(*argv,"-desired")) {
      entryName = *++argv;
      if (*(argv+1))
	istrstream (*++argv) >> entryTime;
      else
	entryTime = 0;
    }


    else if (!strcmp(*argv,"-script")) {
      entryName = *++argv;
      if (*(argv+1))
	istrstream (*++argv) >> entryTime;
      else
	entryTime = 0;
    
    }
    // Retrieve the number of the entry to remove from this rts.

    else if (!strcmp(*argv,"-remove")) {
      istrstream (*++argv) >> removeNumber;
    }

    // See if we need to create a new rts file.

    else if (!strcmp(*argv,"-create")) {
      createFlag = TRUE;
    }

    // Retrieve the name of this rts.
    
    else if (**argv != '-') {
      thisRtsName = *argv;
      if ((thisRtsName.suffix() != "RTS") && (thisRtsName.suffix() != "rts")){
	display_editrts_syntax();
	cerr << "editrts: " << ": The supplied RTS filename, " << thisRtsName << ", is invalid; A RTS requires a .rts extension." << endl;
	return 9445;
      }
    }
    else
      cerr << "editrts: " << "Ignoring argument: " << *argv;

  }
  
  // We're finished parsing the command line.
  
  // If the 'create' flag is present, then create a new RTS file...

  if (createFlag ) {
    if (thisRtsName != "") {
      thisRts = new Rts (thisRtsName);
      if (!thisRts) {
	cerr << "editrts: " << ": Can't allocate enough memory.\n";
	return 996;
      }
      changes = TRUE;

      }
      else {
	cerr << "editrts: " << "Missing filename for Rts " << endl;
	return 999;
      }
    }
    // ...otherwise, read the existing file.

  else  {
    filer.cancelClassIDs ();
    filer.declareClassID (rtsDummy.isA());
    filer.declareClassID (dummyCommandScript.isA());
    filer.declareClassID (dummyPseudod.isA());
    filer.declareClassID (dummyACSd.isA());
    filer.declareClassID (dummyEDSd.isA());
    filer.declareClassID (dummyHEXTEd.isA());
    filer.declareClassID (dummyEventd.isA());
    filer.declareClassID (dummyPCAd.isA());

    thisRts = (Rts*)filer.restoreFrom (thisRtsName);
    if (!thisRts){
      cerr << "editrts: " << "Can't restore RTS object from " << thisRtsName << endl;
      return 998;
    }

  }


  // Remove the specified item.

  if (removeNumber) {
    const RWCollectable* removeEntry = thisRts->operator[](removeNumber-1);
    thisRts->removeAndDestroy (removeEntry);
    changes = TRUE;
  }

  // If the command line named a desired configuration, then insert it
  // into this rts.  Tell the object filer what classIDs we will
  // accept (namely, the ones describing desired configuration objects).
  
  if (entryName != ""){
    TimeLine* theEntry =  (TimeLine * )filer.restoreFrom (entryName);

    if (!theEntry) {
      cerr << "editrts: " << "Can't restore object from " << entryName << " to be inserted into RTS" << endl;
      return 997;
    }

    if (theEntry->isA() == dummyCommandScript.isA()){
      commandScript = (CommandScript*) theEntry;
      commandScript->setFileName (filer.fileName());
      thisRts->insertCommandScript (entryTime, *commandScript);
    }
    else {
      desiredConfig = (DesiredConfig*) theEntry;
      desiredConfig->setFileName (filer.fileName());
      thisRts->insertDesiredConfig (entryTime, *desiredConfig);
    }
    changes = TRUE;
  }

  
  // Print the Rts.

  cout << *thisRts;
  
  // Return this Rts to disk if we changed it.
  
  if (changes)
    filer.saveOn (thisRtsName, thisRts);


  return 0;
}


void display_editrts_syntax() {
  cerr << "Usage:  editrts <rts-name> [-create ] [-remove <number>] " << endl;
  cerr << "                [-desired <config> [<time>]] " << endl;
  cerr << "                [-script  <script> [<time>]] " << endl;
}
@


3.10
log
@*** empty log message ***
@
text
@d8 1
a8 1
static const char* rcsID="$Id: editrts.C,v 3.9 1995/10/13 19:14:14 mmarlowe Exp mmarlowe $";
d132 1
a132 1

@


3.9
log
@*** empty log message ***
@
text
@d8 1
a8 1
static const char* rcsID="$Id: editrts.C,v 3.8 1995/02/27 13:23:32 vick Exp $";
d98 1
a98 1
	cerr << "editrts: " << ": The supplied RTS filename " << thisRtsName << " is invalid, it lacks a .rts extension." << endl;
@


3.8
log
@Added unistd.h header
@
text
@d8 1
a8 1
static const char* rcsID="$Id: editrts.C,v 3.7 1995/01/17 14:54:33 vick Exp vick $";
d98 1
a98 1
	cerr << "editrts: " << ": <rts-name> must end in  \"RTS\" or  \"rts\""<< endl;
@


3.7
log
@Fixed bug in rcsID value from $ID$ to $Id$
@
text
@d8 1
a8 1
static const char* rcsID="$Id$";
d10 1
a29 2
static const char 
rcsId[] = "$Id: editrts.C,v 3.6 1995/01/17 14:51:09 vick Exp vick $";
@


3.6
log
@Fixed small bug for non zerp time offsets for desired configs.
@
text
@d8 1
a8 1
static const char* rcsID="$ID$";
d30 1
a30 1
rcsId[] = "$Id: editrts.C,v 3.5 1994/10/07 20:51:43 vick Exp vick $";
@


3.5
log
@Added a bit of checking for the RTS name
@
text
@d30 1
a30 1
rcsId[] = "$Id: editrts.C,v 3.4 1994/10/04 15:05:31 vick Exp vick $";
d67 1
a67 1
	istrstream (*argv++) >> entryTime;
@


3.4
log
@Changes to support default time offset, also added syntax help.
@
text
@d16 1
d30 1
a30 1
rcsId[] = "$Id: editrts.C,v 3.3 1994/09/09 01:02:48 vick Exp vick $";
d41 1
a41 1
  RWCString      thisRtsName("");
d97 5
@


3.3
log
@minor changes
@
text
@d29 2
a30 1
rcsId[] = "$Id: editrts.C,v 3.2 1994/09/08 14:44:53 vick Exp vick $";
d32 1
a32 1
main (int, char* argv[])
d52 6
d65 4
a68 1
      istrstream (*++argv) >> entryTime;
d74 4
a77 1
      istrstream (*++argv) >> entryTime;
d186 1
d189 7
@


3.2
log
@Fixed bug in rcsId string
@
text
@d16 1
d29 1
a29 1
rcsId[] = "$Id: editrts.C,v 3.1 1994/08/19 19:02:28 vick Exp vick $";
a52 1
    cout << *argv << endl;
d62 1
a62 1
    if (!strcmp(*argv,"-script")) {
d114 7
d146 1
a146 1
      cerr << "editrts: " << "Can't restore object to be inserted into RTS" << endl;
@


3.1
log
@Embellishments to support the insertion of command scripts as well as
desired configs into Rts persistent objects.
@
text
@d8 1
a8 1
static const char* rcsID=$ID$;
d28 1
a28 1
rcsId[] = "$Id: editrts.C,v 3.0 1994/02/18 19:30:45 herreid Exp vick $";
@


3.0
log
@NewBuild
@
text
@d1 9
d28 1
a28 1
rcsId[] = "$Id: editrts.C,v 2.0 1993/11/30 20:00:49 herreid Release herreid $";
d32 3
a34 2
  RWCString      desiredConfigName;
  short          desiredConfigTime;
d36 1
d38 1
a38 1
  RWCString      thisRtsName;
d48 2
d52 3
a54 2
// Retrieve the name and time of the desired configuration to insert
// into this rts.
d57 2
a58 2
      desiredConfigName = *++argv;
      istrstream (*++argv) >> desiredConfigTime;
d61 7
a67 1
// Retrieve the number of the entry to remove from this rts.
d73 1
a73 1
// See if we need to create a new rts file.
d79 1
a79 1
// Retrieve the name of this rts.
d84 2
a85 5
  }

// We're finished parsing the command line.

// If the 'create' flag is present, then create a new RTS file...
a86 4
  if (createFlag) {
    thisRts = new Rts (thisRtsName);
    if (!thisRts) cerr << argv[0] << ": Can't allocate enough memory.\n";
    changes = TRUE;
d88 21
a108 2

// ...otherwise, read the existing file.
d110 1
a110 1
  else {
d113 1
d115 5
a120 1
  if (!thisRts) return 999;
a121 1
// Remove the specified item.
d123 2
d131 11
a141 3
// If the command line named a desired configuration, then insert it
// into this rts.  Tell the object filer what classIDs we will
// accept (namely, the ones describing desired configuration objects).
d143 10
a152 13
  if (desiredConfigName != "") {
    filer.cancelClassIDs ();
#ifndef OMIT_PCA
    filer.declareClassID (pcaDummy.isA());
#endif
#ifndef OMIT_HEXTE
    filer.declareClassID (hexteDummy.isA());
#endif
    desiredConfig = (DesiredConfig*)
      filer.restoreFrom (desiredConfigName);
    if (!desiredConfig) return 2;
    desiredConfig->setFileName (filer.fileName());
    thisRts->insertDesiredConfig (desiredConfigTime, *desiredConfig);
d156 2
a157 1
// Print the Rts.
d160 3
a162 3

// Return this Rts to disk if we changed it.

@


2.0
log
@NewBuild
@
text
@d19 1
a19 1
rcsId[] = "$Id: editrts.C,v 1.2 1993/11/16 17:53:34 herreid Exp herreid $";
@


1.2
log
@added OMIT_HEXTE and OMIT_PCA
@
text
@d19 1
a19 1
rcsId[] = "$Id: editrts.C,v 1.1 1993/11/05 22:37:05 herreid Exp herreid $";
@


1.1
log
@Initial revision
@
text
@d5 4
d10 4
d15 2
a16 2
#include <ObjectFiler.h>
#include <Rts.h>
d19 1
a19 1
rcsId[] = "$Id: editrts.C,v 2.9 1993/11/05 20:30:09 herreid Exp $";
d30 6
a35 8
  ObjectFiler        filer;
  HEXTEDesiredConfig dchexte;
  PCADesiredConfig   dcpca;
  Rts                dummyRts;

  unsigned int removeNumber = 0;
  int          changes      = FALSE;
  int          createFlag   = FALSE;
d68 1
a68 1
// If the 'create' flag is present, then create a new Rts file...
d80 1
a80 1
    filer.declareClassID (dummyRts.isA());
d99 6
a104 2
    filer.declareClassID (dcpca.isA());
    filer.declareClassID (dchexte.isA());
d109 1
a109 1
    thisRts->insertDesiredConfig (desiredConfigTime, desiredConfig);
d119 2
a120 1
  if (changes) filer.saveOn (thisRtsName, thisRts);
@

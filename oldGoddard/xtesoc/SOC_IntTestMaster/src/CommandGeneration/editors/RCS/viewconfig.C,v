head	3.3;
access
	joel
	mmarlowe
	socdev
	subrata
	soccm;
symbols
	Build4_1:3.2
	Build4:3.1
	Build3_3:3.1
	Build3_2:3.1
	Build3_1:3.1
	Build3:3.1
	Current:3.0
	Build2:2.4;
locks; strict;
comment	@ * @;


3.3
date	96.01.06.12.37.38;	author mmarlowe;	state Exp;
branches;
next	3.2;

3.2
date	95.02.27.13.01.54;	author vick;	state Exp;
branches;
next	3.1;

3.1
date	94.03.03.19.40.26;	author herreid;	state Exp;
branches;
next	3.0;

3.0
date	94.02.18.19.30.53;	author herreid;	state Exp;
branches;
next	2.5;

2.5
date	93.12.07.19.44.44;	author herreid;	state Exp;
branches;
next	2.4;

2.4
date	93.12.06.17.40.27;	author herreid;	state Release;
branches;
next	2.3;

2.3
date	93.11.24.20.19.36;	author herreid;	state Release;
branches;
next	2.2;

2.2
date	93.11.15.20.49.21;	author herreid;	state Exp;
branches;
next	2.1;

2.1
date	93.11.03.16.30.40;	author herreid;	state Exp;
branches;
next	2.0;

2.0
date	93.10.14.21.56.20;	author herreid;	state Exp;
branches;
next	1.4;

1.4
date	93.10.14.21.51.44;	author herreid;	state Exp;
branches;
next	1.3;

1.3
date	93.10.12.15.01.51;	author herreid;	state Exp;
branches;
next	1.2;

1.2
date	93.10.08.21.50.50;	author herreid;	state Exp;
branches;
next	1.1;

1.1
date	93.10.01.13.39.03;	author herreid;	state Exp;
branches;
next	;


desc
@Displays configurations
@


3.3
log
@modified to support pseudo desired configurations
@
text
@// Project:	 XTE SOC
// Name:	 viewconfig.C
// Subsystem:	 Command Generation
// Programmer:	 vick
// Description:	 

#include <unistd.h>
#include <rw/cstring.h>
#include <string.h>
#include <iostream.h>
#include <ObjectFiler.h>
#include <CommandScript.h>
#include <Source.h>
#include <TelemRate.h>

#include "dummyconfigs.h"

static const char 
rcsId[] = "$Id: viewconfig.C,v 3.2 1995/02/27 13:01:54 vick Exp $";

// This program is a simple configuration viewer.  It reads
// configuration files and prints them.

void display_viewconfig_syntax();

main (int argc , char *argv[])
{

 // Do they want a syntax check only?

  if (argc == 1) {
    display_viewconfig_syntax();
    exit(950);
  }

  ObjectFiler filer;
  RWCString configName("");
  RWCString inheritName("");
  RWCString predictName("");
  int inheritFlag      = FALSE;
  int predictFlag      = FALSE;
  int scriptFlag       = FALSE;
  int deltaScriptFlag  = FALSE;
  int longFlag         = FALSE;
  int shortFlag        = FALSE;
  int telemFlag        = FALSE;

  SubsystemConfig* subsystemConfig = 0;
  DesiredConfig*   desiredConfig   = 0;
  PredictedConfig* predictedConfig = 0;
  Source           source;
  
// Retrieve the command line arguments.

  while (*++argv) {

// See if the command line asked for a command script.

    if (!strcmp(*argv,"-script")) {
      scriptFlag=TRUE;
    }

// See if the command line asked for a deltacommand script.

    if (!strcmp(*argv,"-deltascript")) {
      deltaScriptFlag=TRUE;
    }

// See if the command line asked for a telemetry rate.

    if (!strcmp(*argv,"-telemetry")) {
      telemFlag=TRUE;
    }

// See if the command line asked us to produce a predicted configuration.

    else if (!strcmp(*argv,"-predict")) {
      predictName=*++argv;
      predictFlag=TRUE;
    }

// See if the command line asked for a print in long format.

    else if (!strcmp(*argv,"-long")) {
      longFlag=TRUE;
    }

// See if the command line asked for a print in short format.

    else if (!strcmp(*argv,"-short")) {
      shortFlag=TRUE;
    }

// See if the command line asked us to inherit from a predicted configuration.

    else if (!strcmp(*argv,"-inherit")) {
      inheritName=*++argv;
      inheritFlag=TRUE;
    }

// Pick up the name of the configuration file.

    else if (**argv != '-') {
      configName = *argv;
    }
  }

// Open the named file and read the configuration object.

  filer.cancelClassIDs ();
  filer.declareClassID (dummyACSd.isA());
  filer.declareClassID (dummyACSp.isA());
  filer.declareClassID (dummyEap.isA());
  filer.declareClassID (dummyEDSd.isA());
  filer.declareClassID (dummyEDSp.isA());
  filer.declareClassID (dummyHEXTEd.isA());
  filer.declareClassID (dummyHEXTEp.isA());
  filer.declareClassID (dummyEventd.isA());
  filer.declareClassID (dummyEventp.isA());
  filer.declareClassID (dummyPCAd.isA());
  filer.declareClassID (dummyPCAp.isA());
  filer.declareClassID (dummyPramp.isA());
  filer.declareClassID (dummyPseudod.isA());

  subsystemConfig = (SubsystemConfig*)filer.restoreFrom (configName);

  if (!subsystemConfig) return 2;

  subsystemConfig->setFileName (filer.fileName());
  
  if (subsystemConfig->isA() ==   dummyACSd.isA() ||
      subsystemConfig->isA() ==   dummyEventd.isA() ||
      subsystemConfig->isA() ==   dummyHEXTEd.isA() ||
      subsystemConfig->isA() ==   dummyPCAd.isA() || 
      subsystemConfig->isA() ==   dummyEDSd.isA()    )
    {
      desiredConfig = (DesiredConfig*)subsystemConfig;
    }
  else
    {
      desiredConfig = 0;
    }
  
  if (
      subsystemConfig->isA() ==   dummyACSp.isA() ||
      subsystemConfig->isA() ==   dummyEventp.isA() ||
      subsystemConfig->isA() ==   dummyHEXTEp.isA() ||
      subsystemConfig->isA() ==   dummyPCAp.isA() || 
      subsystemConfig->isA() ==   dummyEDSp.isA() ||
      subsystemConfig->isA() ==   dummyEap.isA() ||
      subsystemConfig->isA() ==   dummyPramp.isA()   )
    {
      predictedConfig = (PredictedConfig*)subsystemConfig;
    }
  else
    {
      predictedConfig = 0;
    }

// Some command line options apply only to a desired configuration.

  if (desiredConfig) {

// If the command line requested a predicted configuration, then
// create it and write it to the specified file.

    if (predictFlag) {
      predictedConfig = desiredConfig->predictConfig ();
      filer.saveOn (predictName, predictedConfig);
      predictedConfig->print(cout);
    }
  }    // (finished with desired configuration operations)

// Some options apply only to predicted configurations.

  if (predictedConfig) {
    
// If the command line requested a predicted configuration, then
// create it and write it to the specified file.

    if (inheritFlag) {
      PredictedConfig *iPredictedConfig = (PredictedConfig*)
	filer.restoreFrom (inheritName);
      predictedConfig->inheritPrediction (*iPredictedConfig);
      filer.saveOn (configName, predictedConfig);
    }

// If the command line requested a script, then get a script and print it.

    if (deltaScriptFlag) {
      CommandScript *deltaCommandScript =
	predictedConfig->getDeltaCommandScript();
      if (deltaCommandScript) 
	cout << *deltaCommandScript;
      else
	cout << "This configuration did not return a script." << endl;
    }
  }    // (finished with predicted configuration operations)

// If the command line requested a script, then get a script and print it.

  if (scriptFlag) {
    CommandScript *commandScript = subsystemConfig->getCommandScript();
    if (commandScript) 
      cout << *commandScript;
    else
      cout << "This configuration did not return a script." << endl;
  }

// If the command line requested a telemetry rate, then get it and print.

  if (telemFlag) {
    TelemRate *telemRate = subsystemConfig->getTelemRate (source);
//    TelemRate *telemRate = 0;
    if (telemRate)
      telemRate->print(cout);
    else
      cout << "This configuration did not return a telemetry rate." << endl;
  }

// Print the configuration file.

  if (!scriptFlag && !telemFlag && !predictFlag) {

    cout << *subsystemConfig;

    if (longFlag) {
      cout << "printLong:" << endl;
      subsystemConfig->printLong (cout);
    }
    else if (shortFlag) {
      cout << "printShort:" << endl;
      subsystemConfig->printShort (cout);
    }
    else {
      cout << "print:" << endl;
      subsystemConfig->print (cout);
    }
  }
  return 0;
}

void  display_viewconfig_syntax(){
  cerr << "Usage:  viewconfig <config-name>  [-script ] [-deltascript] [-predict <name>] " <<
          " [-inherit <name>] [-short] [-long ]" << endl;


}
@


3.2
log
@Added unistd.h line as well as syntax display code
@
text
@d19 1
a19 1
rcsId[] = "$Id: viewconfig.C,v 3.1 1994/03/03 19:40:26 herreid Exp vick $";
d123 1
@


3.1
log
@introduced dummyconfigs.h
@
text
@d1 7
d19 1
a19 1
rcsId[] = "$Id: viewconfig.C,v 3.0 1994/02/18 19:30:53 herreid Exp herreid $";
d24 3
a26 1
main (int, char *argv[])
d28 8
d241 7
@


3.0
log
@NewBuild
@
text
@d9 1
a9 32
#ifndef OMIT_ACS
#include <ACSDesiredConfig.h>
#include <ACSPredictedConfig.h>
#endif

#ifndef OMIT_OBS
#include <ObsDesiredConfig.h>
#include <ObsPredictedConfig.h>
#endif

#ifndef OMIT_HEXTE
#include <HEXTEDesiredConfig.h>
#include <HEXTEPredictedConfig.h>
#endif

#ifndef OMIT_PCA
#include <PCADesiredConfig.h>
#include <PCAPredConfig.h>
#endif

#ifndef OMIT_EDS
#include <EDSDesConfig.h>
#include <EDSPredConfig.h>
#endif

#ifndef OMIT_EA
#include <EaPredConfig.h>
#endif

#ifndef OMIT_PRAM
#include <PramPredConf.h>
#endif
d12 1
a12 1
rcsId[] = "$Id: viewconfig.C,v 2.5 1993/12/07 19:44:44 herreid Exp herreid $";
a34 31

// Allocate one of each DesiredConfig and PredictedConfig object so
// that I can retrieve them from disk and so that I can use the isA()
// method on each.

#ifndef OMIT_ACS
  ACSDesiredConfig     dummyDAcs;
  ACSPredictedConfig   dummyPAcs;
#endif
#ifndef OMIT_OBS
  ObsDesiredConfig     dummyDObs;
  ObsPredictedConfig   dummyPObs;
#endif
#ifndef OMIT_HEXTE
  HEXTEDesiredConfig   dummyDHexte;
  HEXTEPredictedConfig dummyPHexte;
#endif
#ifndef OMIT_PCA
  PCADesiredConfig     dummyDPca;
  PCAPredictedConfig   dummyPPca;
#endif
#ifndef OMIT_EDS
  EDSDesiredConfig     dummyDEds;
  EDSPredictedConfig   dummyPEds;
#endif
#ifndef OMIT_EA
  EaPredictedConfig    dummyPEa;
#endif
#ifndef OMIT_PRAM
  PramPredictedConfig  dummyPPram;
#endif
d94 12
a105 28
  filer.declareClassID (dummyDAcs.isA());

#ifndef OMIT_ACS
  filer.declareClassID (dummyDAcs.isA());
  filer.declareClassID (dummyPAcs.isA());
#endif
#ifndef OMIT_OBS
  filer.declareClassID (dummyDObs.isA());
  filer.declareClassID (dummyPObs.isA());
#endif
#ifndef OMIT_HEXTE
  filer.declareClassID (dummyDHexte.isA());
  filer.declareClassID (dummyPHexte.isA());
#endif
#ifndef OMIT_PCA
  filer.declareClassID (dummyDPca.isA());
  filer.declareClassID (dummyPPca.isA());
#endif
#ifndef OMIT_EDS
  filer.declareClassID (dummyDEds.isA());
  filer.declareClassID (dummyPEds.isA());
#endif
#ifndef OMIT_EA
  filer.declareClassID (dummyPEa.isA());
#endif
#ifndef OMIT_PRAM
  filer.declareClassID (dummyPPram.isA());
#endif
d113 5
a117 17
  if (
#ifndef OMIT_ACS
      subsystemConfig->isA() ==   dummyDAcs.isA() ||
#endif
#ifndef OMIT_OBS
      subsystemConfig->isA() ==   dummyDObs.isA() ||
#endif
#ifndef OMIT_HEXTE
      subsystemConfig->isA() ==   dummyDHexte.isA() ||
#endif
#ifndef OMIT_PCA
      subsystemConfig->isA() ==   dummyDPca.isA() || 
#endif
#ifndef OMIT_EDS
      subsystemConfig->isA() ==   dummyDEds.isA() ||
#endif
      FALSE )
d127 7
a133 22
#ifndef OMIT_ACS
      subsystemConfig->isA() ==   dummyPAcs.isA() ||
#endif
#ifndef OMIT_OBS
      subsystemConfig->isA() ==   dummyPObs.isA() ||
#endif
#ifndef OMIT_HEXTE
      subsystemConfig->isA() ==   dummyPHexte.isA() ||
#endif
#ifndef OMIT_PCA
      subsystemConfig->isA() ==   dummyPPca.isA() || 
#endif
#ifndef OMIT_EDS
      subsystemConfig->isA() ==   dummyPEds.isA() ||
#endif
#ifndef OMIT_EA
      subsystemConfig->isA() ==   dummyPEa.isA() ||
#endif
#ifndef OMIT_PRAM
      subsystemConfig->isA() ==   dummyPPram.isA() ||
#endif
      FALSE )
@


2.5
log
@added the Source class and uncommented the call to getTelemRate
@
text
@d43 1
a43 1
rcsId[] = "$Id: viewconfig.C,v 2.4 1993/12/06 17:40:27 herreid Release herreid $";
@


2.4
log
@uncommented some predicted configs
@
text
@d6 1
d43 1
a43 1
rcsId[] = "$Id: viewconfig.C,v 2.3 1993/11/24 20:19:36 herreid Release herreid $";
d65 1
d300 2
a301 2
//    TelemRate *telemRate = subsystemConfig->getTelemRate (source);
    TelemRate *telemRate = 0;
@


2.3
log
@added more complete method checking
@
text
@d20 1
a20 1
//#include <HEXTEPredictedConfig.h>
d25 1
a25 1
//#include <PCAPredictedConfig.h>
d42 1
a42 1
rcsId[] = "$Id: viewconfig.C,v 2.2 1993/11/15 20:49:21 herreid Exp herreid $";
d79 1
a79 1
//  HEXTEPredictedConfig dummyPHexte;
d83 1
a83 1
//  PCAPredictedConfig   dummyPPca;
d87 1
a87 1
//  EDSPredictedConfig   dummyPEds;
d90 1
a90 1
//  EaPredictedConfig    dummyPEa;
d93 1
a93 1
//  PramPredictedConfig  dummyPPram;
d166 1
a166 1
//  filer.declareClassID (dummyPHexte.isA());
d170 1
a170 1
//  filer.declareClassID (dummyPPca.isA());
d174 1
a174 1
//  filer.declareClassID (dummyPEds.isA());
d177 1
a177 1
//  filer.declareClassID (dummyPEa.isA());
d180 1
a180 1
//  filer.declareClassID (dummyPPram.isA());
d222 1
a222 1
//      subsystemConfig->isA() ==   dummyPHexte.isA() ||
d225 1
a225 1
//      subsystemConfig->isA() ==   dummyPPca.isA() || 
d228 1
a228 1
//      subsystemConfig->isA() ==   dummyPEds.isA() ||
d231 1
a231 1
//      subsystemConfig->isA() ==   dummyPEa.isA() ||
d234 1
a234 1
//      subsystemConfig->isA() ==   dummyPPram.isA() ||
@


2.2
log
@*** empty log message ***
@
text
@d6 1
d8 1
d11 8
d21 3
a23 2
#include <ObsDesiredConfig.h>
#include <ObsPredictedConfig.h>
d25 4
a28 1
//#include <EAPredictedConfig.h>
d30 10
a39 3
//#include <EDSPredictedConfig.h>
//#include <PCAPredictedConfig.h>
//#include <PRAMPredictedConfig.h>
d42 1
a42 1
rcsId[] = "$Id: viewconfig.C,v 2.1 1993/11/03 16:30:40 herreid Exp herreid $";
d53 7
a59 5
  int inheritFlag = FALSE;
  int predictFlag = FALSE;
  int scriptFlag  = FALSE;
  int longFlag    = FALSE;
  int shortFlag   = FALSE;
d69 26
a94 8
  ACSDesiredConfig   acsDesiredConfig;
  EDSDesiredConfig   edsDesiredConfig;
  HEXTEDesiredConfig hexteDesiredConfig;
  ObsDesiredConfig   obsDesiredConfig;
  PCADesiredConfig   pcaDesiredConfig;
  
  ACSPredictedConfig acsPredictedConfig;
  ObsPredictedConfig obsPredictedConfig;
d106 12
d153 30
d189 17
a205 5
  if (subsystemConfig->isA() ==   acsDesiredConfig.isA() ||
      subsystemConfig->isA() ==   edsDesiredConfig.isA() ||
      subsystemConfig->isA() == hexteDesiredConfig.isA() ||
      subsystemConfig->isA() ==   obsDesiredConfig.isA() ||
      subsystemConfig->isA() ==   pcaDesiredConfig.isA())
d214 23
a236 2
  if (subsystemConfig->isA() == acsPredictedConfig.isA() ||
      subsystemConfig->isA() == obsPredictedConfig.isA())
d245 3
a247 4
// Some command line options, such as the mode-setting operations,
// apply to a desired configuration object.
/*
  if (dc) {
d253 3
a255 4
      pc = new ACSPredictedConfig(dc);
      RWFile pcfile(predictName);
      pc->recursiveSaveOn(pcfile);
      delete pc;
d261 1
a261 1
  if (pc) {
d267 15
a281 6
      RWFile inheritFile(inheritName);
      ipc = (ACSPredictedConfig*)
	RWCollectable::recursiveRestoreFrom(inheritFile);
      pc->inheritPrediction(*ipc);
      delete ipc;
      pc->recursiveSaveOn(configFile);
d284 22
a305 1
  */
d308 1
a308 1
  if (!scriptFlag) {
d312 2
a313 1
    if (longFlag)
d315 3
a317 1
    else if (shortFlag)
d319 3
a321 1
    else
d323 1
a323 8
  }

// If the command line requested a script, then get a script and print it.

  if (scriptFlag) {
    CommandScript *commandScript = subsystemConfig->getCommandScript();
    if (commandScript) cout << *commandScript;
    else cout << "This configuration did not return a script." << endl;
@


2.1
log
@*** empty log message ***
@
text
@d21 1
a21 1
rcsId[] = "$Id: viewconfig.C,v 2.0 1993/10/14 21:56:20 herreid Exp herreid $";
d165 3
@


2.0
log
@NewBuild
@
text
@d10 1
a10 1
#include <HEXTEPredictedConfig.h>
d15 1
a15 1
//#include <EDSDesiredConfig.h>
d21 1
a21 1
rcsId[] = "$Id: viewconfig.C,v 1.4 1993/10/14 21:51:44 herreid Exp herreid $";
d32 2
a33 2
  int inherit = FALSE;
  int predict = FALSE;
d35 2
d38 13
a50 16
  SubsystemConfig*          config = 0;
//  DesiredConfig*     dc = 0;
//  PredictedConfig*   pc = 0;

  ACSDesiredConfig*     dcacs = new ACSDesiredConfig("thing1");
  ACSPredictedConfig*   pcacs = new ACSPredictedConfig("thing2");
  ObsDesiredConfig*     dcobs = new ObsDesiredConfig();
  ObsPredictedConfig*   pcobs = new ObsPredictedConfig();
//  PCADesiredConfig*     dcpca = new PCADesiredConfig();
//  EAPredictedConfig*   pcea  = new EAPredictedConfig();
//  EDSDesiredConfig*     dceds = new EDSDesiredConfig();
//  EDSPredictedConfig*   pceds = new EDSPredictedConfig();
  HEXTEDesiredConfig*   dchex = new HEXTEDesiredConfig("thing3");
  HEXTEPredictedConfig* pchex = new HEXTEPredictedConfig("thing4");
//  PCAPredictedConfig*     pcpca = new PCAPredictedConfig();
//  PRAMPredictedConfig*   pcpram = new PRAMPredictedConfig();
d52 5
d69 13
a81 1
      predict=TRUE;
d88 1
a88 1
      inherit=TRUE;
d100 1
a100 1
  config = (SubsystemConfig*)filer.restoreFrom (configName);
d102 27
d137 1
a137 1
    if (predict) {
d152 1
a152 1
    if (inherit) {
d164 8
a171 1
  config->print(cout);
d176 3
a178 12
    CommandScript *script = config->getCommandScript();
    cout << *script;
/*    const CommandPacket *cp;
    int                  count = script->entries();
    int                  icount;

    script->print(cout);

    for (icount = 0; icount<count; ++icount) {
      cp = script->getCommandPacket(icount);
      if (cp) cp->print (cout);
    }*/
@


1.4
log
@work in progress
@
text
@d21 1
a21 1
rcsId[] = "$Id: viewconfig.C,v 1.3 1993/10/12 15:01:51 herreid Exp herreid $";
@


1.3
log
@removed PCA includes
@
text
@d5 1
d13 1
a13 1
//#include <PCADesiredConfig.h>
d21 1
a21 1
rcsId[] = "$Id: viewconfig.C,v 1.2 1993/10/08 21:50:50 herreid Exp herreid $";
d34 1
a34 1
  int script  = FALSE;
d58 1
a58 1
      script=TRUE;
d125 4
a128 3
  if (script) {
    CommandScript       *script = config->getCommandScript();
    const CommandPacket *cp;
d137 1
a137 1
    }
a138 1
  delete config;
@


1.2
log
@added ObjectFiler class
@
text
@d12 1
a12 1
#include <PCADesiredConfig.h>
d20 1
a20 1
rcsId[] = "$Id: viewconfig.C,v 1.1 1993/10/01 13:39:03 herreid Exp herreid $";
d43 1
a43 1
  PCADesiredConfig*     dcpca = new PCADesiredConfig();
d47 2
a48 2
//  HEXTEDesiredConfig*   dchex = new HEXTEDesiredConfig("thing3");
//  HEXTEPredictedConfig* pchex = new HEXTEPredictedConfig("thing4");
@


1.1
log
@Initial revision
@
text
@d4 1
a4 8
#include "ACSDesiredConfig.h"
#include "ACSPredictedConfig.h"
#include "HEXTEDesiredConfig.h"
#include "HEXTEPredictedConfig.h"
//#include "EDSDesiredConfig.h"
//#include "EDSPredictedConfig.h"
//#include "PCADesiredConfig.h"
//#include "PCAPredictedConfig.h"
d6 13
d20 1
a20 1
rcsId[] = "$Id$";
d27 1
a27 3
  int script  = FALSE;
  int predict = FALSE;
  int inherit = FALSE;
d29 1
d31 3
a33 1
  RWCString inheritName("");
d38 1
d41 4
a44 3
  HEXTEDesiredConfig*   dchex = new HEXTEDesiredConfig("thing3");
  HEXTEPredictedConfig* pchex = new HEXTEPredictedConfig("thing4");
//  PCADesiredConfig*     dcpca = new PCADesiredConfig();
d47 4
d81 1
a81 12
// Open the named file, creating it if it doesn't exist.  

  RWFile configFile(configName);

// If it did not exist, issue an error message and exit.

  if (configFile.IsEmpty()) {
    cerr << "There is no such file as \"" << configName << "\"." << endl;
    return 1;
  }

// If the file did exist, read the Config object that it contains.
d83 1
a83 1
  config = (SubsystemConfig*)RWCollectable::recursiveRestoreFrom(configFile);
@

head	1.1;
access
	mmarlowe
	soccm;
symbols
	Build3_2:1.1
	Current:1.1;
locks; strict;
comment	@ * @;


1.1
date	94.10.06.01.46.36;	author shalevy;	state Exp;
branches;
next	;


desc
@@


1.1
log
@Initial revision
@
text
@/*
 * io_read.c
 */
#include <stdio.h>
#include <sys/param.h>
#include "hexdefs.h"

extern SInt SequenceNumber;
extern char CmdBuffer[1024];
extern char GSECmdDirName[MAXPATHLEN];
extern char realInputFile[128];

#define DefaultIOReadMaxData 228
#define DefaultIOReadCmdLen 12
#define DefaultIOReadCmdMaxParmLen 2

static struct {
    BYTE Opcode;
    BYTE CmdType;
    SInt SeqNum;
    SInt Len;

    BYTE IOType;
    BYTE IOCount;
    BYTE Spare1;
    BYTE Spare2;
    BYTE IOReadBuf[DefaultIOReadMaxData];

    SInt ChkSum;
}IOReadCmd;

int
hexte_IO_Read(CmdOpcode, ParmList, ParmCnt)
int CmdOpcode;
char ParmList[][32];
int ParmCnt;
{
    int i, RetVal = 0, ParmErr = 0;
    SInt BufIndex=1, DataLen;
    LInt chksum=0;
    FILE *fp;

    if (ParmCnt == 4) {  /* Op IOType IOCount <DataFile Name> */
        /* Build the real file name including directory. */
        (void)strcpy(realInputFile, GSECmdDirName);  /* Get the directory */
        (void)strcpy(&realInputFile[(int)strlen(GSECmdDirName)], ParmList[3]);  /* Get the name */
        if ((fp = fopen(realInputFile, "rb")) != NULL) {

            IOReadCmd.Opcode = (BYTE)CmdOpcode;
            chksum = AddBYTEToChkSum(chksum, IOReadCmd.Opcode);
            IOReadCmd.CmdType = ImmediateCmdType;
            chksum = AddBYTEToChkSum(chksum, IOReadCmd.CmdType);
            IOReadCmd.SeqNum = SequenceNumber;
            chksum = AddSIntToChkSum(chksum, IOReadCmd.SeqNum);

            ParmErr += GetBYTEParm(ParmList[1], &IOReadCmd.IOType);
            chksum = AddBYTEToChkSum(chksum, IOReadCmd.IOType);
            ParmErr += GetBYTEParm(ParmList[2], &IOReadCmd.IOCount);
            chksum = AddBYTEToChkSum(chksum, IOReadCmd.IOCount);

            IOReadCmd.Spare1 = 0;
            chksum = AddBYTEToChkSum(chksum, IOReadCmd.Spare1);
            IOReadCmd.Spare2 = 0;
            chksum = AddBYTEToChkSum(chksum, IOReadCmd.Spare2);

            /* Read in the bytes of upload data from the file.   */
            /* Note: These bytes are produced by a program that  */
            /*       creates a binary list of the bytes that are */
            /*       really the LOB's and HOB's of the SInt's in */
            /*       each I/O Read structure that is to be used  */
            /*       by this command.                            */
            DataLen = fread(IOReadCmd.IOReadBuf, 1, DefaultIOReadMaxData, fp);
            (void)fclose(fp);
            /* Make the byte count even, truncating if necessary. */
            /* It had _better_ already be even.                   */
            DataLen = (DataLen/2) * 2;

            for (i=0; i<(int)DataLen; i++) {
                chksum = AddBYTEToChkSum(chksum, IOReadCmd.IOReadBuf[i]);
            }

            IOReadCmd.Len = DataLen + DefaultIOReadCmdLen;
            chksum = AddSIntToChkSum(chksum, IOReadCmd.Len);

            if (!ParmErr) {
                SequenceNumber++;
            } else {
                RetVal = IllegalParm;
            }

            /* Store the 2's complement of the checksum */
            chksum = (chksum ^ 0xffffffff) + 1;
            IOReadCmd.ChkSum = (SInt)(chksum & 0xffff);

            /* Now move the data in the command structure to a buffer in ASCII form */
            CmdBuffer[0] = StdCmdStart;

            PutBYTE(CmdBuffer, &BufIndex, IOReadCmd.Opcode);
            PutBYTE(CmdBuffer, &BufIndex, IOReadCmd.CmdType);
            PutSInt(CmdBuffer, &BufIndex, IOReadCmd.SeqNum);
            PutSInt(CmdBuffer, &BufIndex, IOReadCmd.Len);

            PutBYTE(CmdBuffer, &BufIndex, IOReadCmd.IOType);
            PutBYTE(CmdBuffer, &BufIndex, IOReadCmd.IOCount);

            PutBYTE(CmdBuffer, &BufIndex, IOReadCmd.Spare1);
            PutBYTE(CmdBuffer, &BufIndex, IOReadCmd.Spare2);

            for (i=0; i<(int)DataLen; i++) {
                PutBYTE(CmdBuffer, &BufIndex, IOReadCmd.IOReadBuf[i]);
            }

            PutSInt(CmdBuffer, &BufIndex, IOReadCmd.ChkSum);

            CmdBuffer[BufIndex] = StdCmdEnd;
            CmdBuffer[BufIndex+1] = '\0';

        } else {
            RetVal = IllegalParm;
        }
    } else {
        RetVal = BadParmCnt;
    }

    /* Make a last parameter check. */
    if (((int)(IOReadCmd.IOCount) * DefaultIOReadCmdMaxParmLen)> DefaultIOReadMaxData)
    {
        RetVal = BadParmCnt;
    }

    return RetVal;
}
@

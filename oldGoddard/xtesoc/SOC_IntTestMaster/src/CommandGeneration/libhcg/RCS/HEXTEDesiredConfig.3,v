head	1.1;
access
	soccm;
symbols;
locks; strict;
comment	@# @;


1.1
date	96.07.05.17.32.38;	author socdev;	state Exp;
branches;
next	;


desc
@@


1.1
log
@Initial revision
@
text
@.TH HEXTEDESIREDCONFIG 3 "3 July, 1996"
.SH "NAME"
HEXTEDesiredConfig  \- HEXTE Desired Configuration Class
.SH "SYNOPSIS"

.NF
#include <HEXTEDesiredConfig.h>

class HEXTEDesiredConfig : public DesiredConfig 

{
    HEXTEDesiredConfig( void );
    HEXTEDesiredConfig( const HEXTEDesiredConfig & );
    USI getValue( int clus, char * mnem ) const ;
    int setValue( int clus, char * mnem, USI val);
    USH getFlag( int clus, char * mnem ) const;
    void setFlag( int clus, char * mnem, USH value );
    const HxCluster & getClus( int num ) const;
    void setCmdStr( unsigned int, const char * );
    const char * getCmdStr( unsigned int ) const;
    void setCmdFlag( unsigned int, unsigned short );
    unsigned short getCmdFlag( unsigned int ) const;
    inline const char * getConfigName( int whichCluster ) const;
    inline void         printConfigName( ostream & ) const;
    CommandScript *  getCommandScript( void                    ) const;
    TelemRate *      getTelemRate(     const Source& sources   ) const;
    SOCErrObject *   isError(          void                    ) const;
    void             printShort(       ostream& ostr           ) const ;
    void             print(            ostream& ostr           ) const ;
    void             printLong(        ostream& ostr           ) const ;
    SubsystemId      getSubsystemId() const  ;
    PredictedConfig *   predictConfig() const;
    HEXTEPredictedConfig * newPC() const;
    RWspace          binaryStoreSize(  void                    )  const; 
    void             saveGuts(         RWFile & f              ) const ;
    void             saveGuts(         RWvostream & strm       )  const ;
    void             restoreGuts(      RWFile & f              )      ;
    void             restoreGuts(      RWvistream & strm       )      ;
    int operator==( const HEXTEDesiredConfig & from ) const;
    HEXTEDesiredConfig & operator=( const HEXTEDesiredConfig & from );
    ~HEXTEDesiredConfig(         void                              );
.PP
.SH DESCRIPTION
The HEXTE Desired Configuration Class is a required software component of
the SOC/UCSD ICD.  All Instrument Teams are required to provide the SOF with
a commanding interface through controlled instrument desired and
predicted configuration classes.

The HEXTE Desired Configuration Class encapsulates all state information
associated with a request to change the configuration of the HEXTE
Instrument.  Unspecified parameters are marked with a flag named DONT CARE.

Parameters are specified using the setItemVal() method.  To set a parameter
value, both the parameter name and desired value must be supplied.  A parameter
name is commonly known as a mnemonic.  

The HEXTE Configuration Management Library is responsible for managing 
all mnemonics in HEXTE Software.

HEXTE Desired Configurations are stored to disk using the Rogue Wave 
persistance methods such as saveGuts().

.SH FILES
$SOCHOME/etc/HkItem.table
$SOCHOME/etc/HkSub.table
 
.SH BUGS
There are no bugs known.

.SH AUTHOR
Matthew Marlowe, Hughes STX Corporation, mmarlowe@@xpert.stx.com.
    
@

//=======================================================================
// File:       RTSDAPCmd.h
// Programmer: Matthew Marlowe, Hughes STX
// Project:    XTESOC Command Generation Verification
// Date:       12/1/95
//
// Description:
//   Example class for the IT's to demonstrate how to create a subclass of DAPCmd.
// This example is for RTS's.
//
//
//===============================================================================
//
// $Id: RTSDAPCmd.h,v 1.1 1995/12/01 20:58:48 mmarlowe Exp mmarlowe $
//
// $Log: RTSDAPCmd.h,v $
// Revision 1.1  1995/12/01 20:58:48  mmarlowe
// Initial revision
//
//
//===============================================================================

#include<iostream.h>
#include<rw/collect.h>
#include<rw/rwfile.h>
#include<rw/pstream.h>
#include<rw/vstream.h>
#include<rw/cstring.h>
#include<cgdefs.h>
#include<cgvdefs.h>
#include<DAPCmdVal.h>
#include<DAPCmdValList.h>
#include<DAPCmdStat.h>
#include<DAPCmd.h>

class RTSDAPCmd : public DAPCmd 

RWDECLARE_COLLECTABLE( RTSDAPCmd )

{

 public:

  RTSDAPCmd();
  RTSDAPCmd( const char *, const char * );
  RTSDAPCmd( const RTSDAPCmd );
  
  DAPCmdValList * isValidCmd( );
  DAPCmdValList * isDangerousCmd();
  const char *    interpretCmd( TranslationType );
  DAPCmdStat    * getStatistic( CgvStatType     );

  void printShort( ostream & )const;
  void print( ostream & )const;
  void printLong( ostream & ) const;
  
  ~RTSDAPCmd();
 
 private:
  
  void freeData();
  void copyData( RTSDAPCmd & ) const;
  void setDefaults();

  static int numRTSCalls;

};


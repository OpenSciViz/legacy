
NAME
     checkslew - checks the ACS commands in an activity plan.

SYNOPSIS
     checkslew [-i] <dap_filename>

DESCRIPTION
     Checkslew reads slews out of the activity plan, computes the maneuver
     necessary to get there, and checks the safety/feasibility of each
     slew parameter.  Only the lines containing the word "SLEW" are checked.
     The quanitites that are checked, along with their default values, are:     
             0 < RA                    < 360  degrees
           -90 < Dec                   < 90  degrees
          0.01 < slew rate             < 12 degrees/minute
            -5 < roll bias angle       < +5 degrees
             2 < slew duration         < 200 minutes
                 Angle to SUN          > 30 degrees
                 Angle to Sco X-1      > 0.5 degrees
      256 sec  < length of observation < 8 hours
      The possibility of an overlap of observations is also checked.


OPTIONS
     -i   Turns on interactive mode.  The user will be asked to input
          the limits for various slew parameters, eg: maximum slew rate,
          minimum length of an observation, etc.

FILES
     dap_filename.out              file in the current directory which
                                   contains output similar to that which
                                   appears on the screen, plus more
                                   detailed information on the slews.
     dap_filename.quat             file in the current directory which
                                   contains the spacecraft pointing at regular
                                   intervals within the slew.  The format is:
                                   MET, 4 quaternions, RA, Dec.

AUTHOR
     Christina Williams, williams@rosserv.gsfc.nasa.gov.

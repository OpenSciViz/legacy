c $Id: checkslewfortran.f,v 4.0 1995/11/03 18:47:02 williams Exp $
c This program is a modified version of Frank Marshall's 'xteman.f'.
c Modified by Christina Williams, last revision on 11/2/95.
c The original xteman.f is located in ~marshall/fortran/maneuver/
c
c This program computes proposed XTE maneuver algorithm
c	start and end attitude completely defined by
c	direction of x-axis.
c	the sun must be in the x-z plane and toward the
c	positive z-axis.
c
c we assume an inertial attitude system with z toward the sun
c 
c we have assumed boresight is aligned with x-axis
c
c adapted from man3
c
c 05-may-92
c
c mod 21-jun-93 to model non-zero roll angles
c moved to sun 28-jul-95
c mod 16-aug-95 to use quaternions more extensively
c		to check sun's position in s/c coordinates
c mod 21-sept-95 to check proximity to Sco X-1 and the Sun at ALL points along 
c     the slew, not just at the endpoints!!!
c     Also added checks for RA, Dec, slew rate, roll bias angle, slew duration.
c mod 2 Nov 95 to check for observations longer than 8 hours.
c
c  ****** Now called by the program "checkdap". **********
c
c         Current limits are:    0 < RA < 360  degrees
c                              -90 < Dec < 90  degrees
c                               0.01 < slew rate < 12 degrees/minute
c                                -5 < roll bias angle < +5 degrees
c                                2  < slew duration < 200 minutes
c                                angle to SUN > 30 degrees
c                                angle to Sco X-1 > 0.5 degrees
c                                time between slews < 8 hours
c
	implicit real*8 (a-g,o-p,r-z)
        character*60 qdsk,planname,outname,quatname
	dimension cqt(4),bmat(3,3),aqt(4),aaqt(4),bqt(4)
	real*8 jd,jd1
        common/unicom/ILP,IIN,IOUT
	data rad/57.29577951d0/
	jd1=0.
	ILP=7
	iquat=2
cwrite(6,*) ' program xteman 21-sep-95 vrs. fem -> clw'
cwrite(ILP,*) ' program xteman 21-sep-95 vrs. fem -> clw'
cwrite(6,22)
22	format(' This program reads XTE attitudes',/,
     2		' and computes maneuvers.',/,
     3		' Prints to file "xteman.out".',/
     4		' Path output goes to "xteman.quat".'/)

cwrite(6,*) ' enter data input data file name (60a1)'
cread(5,*) qdsk
        qdsk="plan_slews"
	open(unit=1,file=qdsk,status='old')
        read(1,*) planname
        read(1,*) quatname
        read(1,*) outname
	read(1,*) slewratemin
	read(1,*) slewratemax
	read(1,*) rollbiasmax
	read(1,*) slewdurmin
	read(1,*) slewdurmax
	read(1,*) sunangmax
	read(1,*) scoangmax
	read(1,*) obslengthmin
	read(1,*) obslengthmax

	open (unit=iquat, file=quatname)
	open (unit=ILP, file=outname)

        obslimit = 0.333333 !day = 8 hours

c get step size and no. of points
cwrite(6,*)' enter 1 to use met seconds for quaternion file
c    1 default is yr day hr mn sc'
cread(5,*)imet
cwrite(6,37)
c37	format(' enter time step size in seconds (default=60.)')
cread(5,39)step
c39	format(4f10.0)
cif(step.le.0.) step=60.
	step = 60.
c get records to read
cwrite(6,*) ' enter 1st and last record no. (1,999)'
cread(5,*) icmin,icmax
c                
        imet=1
  	icmin=0
	icmax=999
	icnt=0
	sslew=0.
	tslew=0.
	sgam=0.
c write title
	write(ILP,92)
	write(6,92)
	write(ILP,93)
	write(6,93)
92	format('YYYY:DDD:HH:MM:SS    MET Sec.',4x,'RA',5x,'Dec   Roll  Rate' 
     .  '  RA-Sun  Dc-Sun SunAng')
93      format(80('-'))
c
c *************** MAIN LOOP, EXECUTED ONCE PER SLEW ******************
c
100	continue
	read(1,101,end=777) kyr,kday,khr,kmn,ksec,ra2,dec2,roll2,rate2
c       write(6,101) kyr,kday,khr,kmn,ksec,ra2,dec2,roll2,rate2
101	format(i5,i4,3i3,4f10.3)
	icnt=icnt+1               
	if(icnt.lt.icmin) go to 100
	if(icnt.gt.icmax) go to 777
c
c calculate julian date
c
c 0h jan 1 1994 is jd 9353.5 (+2440000)
	if(kyr.lt.100)kyr=kyr+1900
	ysec=(kday-1)*86400.d0+khr*3600.d0 + kmn*60.d0 + ksec
	jd=9352.5d0 + (kyr-1994)*365.d0 +kday+khr/24.d0 +
     1		kmn/1440.d0 + ksec/86400.d0
	if(kyr.gt.1996)jd=jd+1.d0             ! correct for leap year
	call tran1(jd,ra2,dec2,roll2,rate2,rasun,decsun,sunang,bmat) 
	timsec = (jd - 9353.5d0) * 86400.d0
        if (icnt.gt.2) then
c check if prev observation is longer than obslengthmax
	  if(jd.ge.(jd1 + obslengthmax))then
	    write(ILP,115) obslengthmax
	    write(6,115) obslengthmax
115	    format(' ***** WARNING--this observation is longer',
     .        ' than the limit of ',f7.4,' day.')
	  endif
c check if observation is shorter than obslengthmin
	  if(jd.le.(jd1 + obslengthmin))then
	    write(ILP,116) obslengthmin
	    write(6,116) obslengthmin
116	    format(' ***** WARNING--this observation is shorter',
     .        ' than the limit of ',f7.4,' day.')
	  endif
        endif  ! icnt.gt.2
	write(ILP,*) ' '
	write(ILP,106) kyr,kday,khr,kmn,ksec,timsec,ra2,dec2,roll2,rate2,rasun,
     .  decsun,sunang
	write(6,106) kyr,kday,khr,kmn,ksec,timsec,ra2,dec2,roll2,rate2,rasun,
     .  decsun,sunang
106	format(i4,':',i3,3(':',i2),f12.1,f8.2,f7.2,f6.2,f6.2,f8.2,f7.2,f8.2)
C
C  Check for non-allowed slew parameters
C
1       format(' ***** ROLL BIAS = ',f6.2,' deg, (',f6.2,
     .      ' < ROLL BIAS < ',f4.2,') !!!')
        if (abs(roll2).gt.rollbiasmax) then
            rollbiasmin = -1*abs(roll2)
            write(6,1) roll2,rollbiasmin,rollbiasmax
            write(ILP,1) roll2,rollbiasmin,rollbiasmax
        endif
2       format(" ***** SLEW RATE = ",f5.3," deg/min, too slow (",f5.3,
     .      " < RATE < ",f5.2,") !!!")
3       format(" ***** SLEW RATE = ",f6.4,
     .      " deg/min, thats pretty slow!")
        if (rate2 .lt. 1.0) then
          if (rate2 .lt. slewratemin) then
            write(6,2) rate2,slewratemin,slewratemax
            write(ILP,2) rate2,slewratemin,slewratemax
          else
            write(6,3) rate2
            write(ILP,3) rate2
          endif
        endif
4       format(' ***** SLEW RATE = ',f7.4,' deg/min, too fast (',f5.3,
     .      ' < RATE < ',f5.2,') !!!')
        if (rate2 .gt. slewratemax)  then
            write(6,4) rate2,slewratemin,slewratemax
            write(ILP,4) rate2,slewratemin,slewratemax
        endif
5       format(' ***** RA = ',f7.3,'deg, (0 < RA < 360) !!!')
        if ((ra2 .lt. 0).or.(ra2 .gt. 360)) then
           write(6,5) ra2
           write(ILP,5) ra2
        endif
6       format(" ***** DEC = ",f7.4," deg, (-90 < DEC < 90) !!!")
        if (abs(dec2) .gt. 90) then
           write(6,6) dec2
           write(ILP,6) dec2
        endif

c bmat,bqt is for new attitude
c amat,aqt is for old attitude
c at,aaqt  is inverse of amat,aqt
c cmat,cqt is rotation from old to new
	call findqt(bmat,bqt)    ! convert the rotation matrix to quaternion
	call prtqt(bqt)          ! print the quaternion
	call prtmat(bmat)        ! print the rotation matrix

c            DO THIS BLOCK ONLY AFTER 1ST PASS THROUGH
	if(icnt.ne.1)then
c check if previous maneuver has ended
	if(jd.le.jd1)then
	  write(ILP,114)
	  write(6,114)
114	  format(' ***** WARNING--this maneuver begins before',
     .      ' the previous maneuver ends.')
	endif
c compute inverse of 1st rotation matrix (inverse=transpose)
	    call qtinv(aqt,aaqt)
c multiply matrices
c cmat is rotation matrix to go from 1st position to second position
c now find quaternion corresponding to matrix
	    call qtmult(aaqt,bqt,cqt)
c check for slewing wrong direction
	    if (cqt(4).lt.0.d0) call qtneg(cqt)                          
c compute slew angle       
	    phi=dacos(cqt(4))*2.d0*rad !path length of the whole slew
	    sslew=sslew+dabs(phi)  ! sum of the path lengths slewed in this dap
c compute movement of x-axis
	    cosg=dsin(dec1/rad)*dsin(dec2/rad)+dcos(dec1/rad)
     1		*dcos(dec2/rad)*dcos((ra2-ra1)/rad)
	    gam=dacos(cosg)*rad  
	    sgam=sgam+gam
c
c "path" is the sub which computes each sub-step of the slew and checks it.
c This subroutine has been promoted from an option to a major part of the prog.
	    call path(jd,roll2,iquat,cqt,aqt,rate2,step,kyr,ysec,imet,
     .         sunangmax,scoangmax)
c
	    timphi = phi / rate2  ! the time to do the whole slew
	    tslew = tslew + timphi  ! a sum of the slew times in the whole dap
	    write(ILP,108) gam,phi,timphi,rate2
108	    format(' X-ROT.:',f8.2, ', TOTAL ROT.',f8.2,' =>',f7.2,
     1		' min at',f6.2,' deg/min')
	    ysec = ysec + timphi*60.d0
	    jd1 = jd + timphi/1440.d0  ! time at the end of the slew
	    timsec = (jd1 - 9353.5d0) * 86400.d0 !time at the end of the slew
	    call maktim(ysec,kday,khr,kmin,asec)
            write(ILP,128) kyr,kday,khr,kmin,asec,jd1,timsec
c           write(6,128) kyr,kday,khr,kmin,asec,jd1,timsec
128	    format(i6,i4,2i3,f6.2,' = ',f10.3,' = ',f13.2,
     .         ' is end of maneuver.')
7       format(" ***** SLEW DURATION = ",f6.2," minutes, too long !!!")
        if (timphi.gt.slewdurmax) then
          write(6,7) timphi
          write(ILP,7) timphi
        endif
8       format(" ***** SLEW DURATION = ",f6.4," minutes, too short !!!")
        if (timphi.lt.slewdurmin) then
          write(6,8) timphi
          write(ILP,8) timphi
        endif
	endif

c save old values
	ra1 = ra2
	dec1 = dec2
	roll1 = roll2
	call qtcopy(bqt,aqt)
	go to 100
c
c *********************** END OF MAIN LOOP *********************************
c   write out summary information
777	continue                     
c terminate program        
	av = 0.
	gamav = 0.
	tav = 0.
	if(icnt.gt.1)then
	  an = icnt
	  avsl = sslew / an
	  gamav = sgam / an
	  tav = tslew / an
	endif
	write(ILP,776)sslew,tslew,avsl,tav
	write(6,776)sslew,tslew,avsl,tav
776	format(/,' TOTAL slew length:',f9.1,' deg. which takes',f8.1,' min.',/,
     1		 ' Average slew length:',f9.1,' deg. which takes',f8.1,' min.')
	write(ILP,775)sgam,gamav
	write(6,775)sgam,gamav
775	format(/,' TOTAL x-axis path:  ',f9.1,' deg.',/,
     1		 ' Average path per slew:',f9.1,' deg.')
      stop
      end



c ***********************  BEGIN SUBROUTINES   *****************************

	subroutine tran1(jd,ra,dec,roll,rate,ras,dcs,sunang,zmat)
c            This routine computes the rotation matrix for specified ra and 
c               dec of the x-axis and roll around the x-axis.
c            Inputs are jd, ra, dec, and roll in degrees. 
c            Output is rotation matrix amat
c            jd is julian date 
c
	implicit real*8 (a-h,o-z)
	dimension zmat(3,3),sun(3),ang(2),x(3),y(3),z(3)
	real*8 jd
        common/unicom/ILP,IIN,IOUT
	data rad/57.29577951d0/
        iquat=2
	rollr = roll / rad         
	sinl = dsin(rollr)          
	cosl = dcos(rollr)
c compute sun's position
	call snradc(jd,ras,dcs)
	ang(1) = ras/rad
	ang(2) = dcs/rad
	call xyz(ang,sun)
c compute x-axis
	ang(1) = ra/rad
	ang(2) = dec/rad
	call xyz(ang,x)
c compute sun angle
        sunang = rad * dacos(dot(x,sun))
c y-axis is cross of sun and x
	call cross(sun,x,y)
	call norm(y)
	call cross(x,y,z)
	call norm(z)
c make matrix with roll around x
	do i=1,3
	  zmat(1,i) = x(i)
	  zmat(2,i) = y(i)*cosl + z(i)*sinl
	  zmat(3,i) = z(i)*cosl - y(i)*sinl
        enddo
	return
	end
c
c
	subroutine path(jd,roll2,iquat,aqt,dqt,rate,tstep,kyr,ysec,isec,
     .    sunangmax,scoangmax)
c         This routine plots the path of the x axis.
c         The path starts from the x axis given by the rotation matrix eqt
c           and ends at the product of the rotation given by the quaternion aqt
c           and eqt.
c         rate is the maneuver rate in deg/min.
c         tstep is the sample rate in seconds
c         maneuver starts at ysec of kyr
c         writes time in sec of 1994 if isec.ne.0
c	     else uses yr,day,hour,min,sec format
c         output goes to unit iquat
c               
	real*8 aqt(4),rad,phi,phir,vect(3),bqt(4),ra,dec,step,
     .	     cqt(4),dqt(4),rate,tstep,time,toff,tout,asec,ysec
        common/unicom/ILP,IIN,IOUT
        iquat=2
	data rad/57.29577951d0/
        call snradc(jd,rasun,decsun)
	phir = dacos(aqt(4))*2.d0
	phi0 = phir*rad
c step is step size in degrees
	step = tstep * rate/60.
c compute number of steps
	n = phi0 / step
	if(n.lt.1 .or. n.gt.90000)return
	time = ysec
c compute offset to translate to secs of 1994
	toff = (kyr-1994) * 31536000.d0
c correct for leap year
	if (kyr.gt.1996) toff = toff + 86400.d0
	sinp = dsin(phir/2.d0)
	vect(1) = 0.
	vect(2) = 0.
	vect(3) = 1.
	if (sinp.ne.0.) then
	    vect(1) = aqt(1)/sinp
	    vect(2) = aqt(2)/sinp
	    vect(3) = aqt(3)/sinp
	endif
	call qtxra(dqt,ra,dec)
        call proximity(rasun,decsun,ra,dec,sunangmax,scoangmax)
	if(isec.ne.0) then
	    tout = time + toff
	    write(iquat,57) tout,dqt,ra,dec
57	    format(f13.2,4f9.5,2f7.2)
	else
	    call maktim(time,kday,khr,kmin,asec)
	    write(iquat,56) kyr,kday,khr,kmin,asec,dqt,ra,dec
56	    format(i5,i4,2i3,f6.2,4f8.4,2f7.2)
	endif

c ********** THIS BLOCK IS EXECUTED FOR EACH SUB-STEP *****************

        do 50 i=1,n
            time = time + tstep
            phi = i*step/rad/2.d0
c compute new quaternion
            bqt(4) = dcos(phi)                 
            sinp = dsin(phi)           
            bqt(1) = vect(1)*sinp
            bqt(2) = vect(2)*sinp
            bqt(3) = vect(3)*sinp
c now do it with just quaternions
            call qtmult(dqt,bqt,cqt)
            call qtxra(cqt,ra,dec)
            if(isec.ne.0) then
                tout = time + toff
                write(iquat,57) tout,cqt,ra,dec
            else
                call maktim(time,kday,khr,kmin,asec)
                write(iquat,56) kyr,kday,khr,kmin,asec,cqt,ra,dec
            endif
            call proximity(rasun,decsun,ra,dec,sunangmax,scoangmax)
50      continue

c *************    END OF SUB-STEP BLOCK     *********************


c print final point
	call qtmult(dqt,aqt,cqt)
	call qtxra(cqt,ra,dec)
c change so that we print time at next time interval
	time = time + tstep
	if(isec.ne.0) then
	    tout = time + toff
            write(iquat,57) tout,cqt,ra,dec
	else
	    call maktim(time,kday,khr,kmin,asec)
            write(iquat,56) kyr,kday,khr,kmin,asec,cqt,ra,dec
	endif
        call proximity(rasun,decsun,ra,dec,sunangmax,scoangmax)
	return         
	end
c
c
	subroutine maktim(time,kday,khr,kmin,asec)
c           Input: time is real*8 seconds into year (assumed to be >=0.)
c           Output: kday,khr,kmin are integer values
c 	    asec is remainder in seconds (real*8)
c           Days start at 1 not 0
	implicit real*8(a-g,o-z)
	kday = time / 86400.d0
	asec = time - kday * 86400.d0
	khr  = asec / 3600.d0              
	asec = asec - khr * 3600.d0
	kmin = asec / 60.d0              
	asec = asec - kmin * 60.d0
	kday = kday + 1
	return
	end     

c SUBROUTINE TO CHECK THE PROXIMITY OF THE SUN AND SCOX1
        subroutine proximity(rasun,decsun,ra,dec,sunangmax,scoangmax)
	implicit real*8 (a-h,o-z)
	dimension sun(3),ang(2),x(3),sco(3)
        common/unicom/ILP,IIN,IOUT
	data rad/57.29577951d0/
        rasco = 244.979167
        decsco = -15.64000
c compute sun's position
	ang(1) = rasun/rad
	ang(2) = decsun/rad
	call xyz(ang,sun)
c compute sun's position
	ang(1) = rasco/rad
	ang(2) = decsco/rad
	call xyz(ang,sco)
c compute x-axis
	ang(1) = ra/rad
	ang(2) = dec/rad
	call xyz(ang,x)
c compute angles
	sunang = rad * dacos(dot(x,sun))
	scoang = rad * dacos(dot(x,sco))
c write(6,*) 'SUN ANGLE: ',sunang
c write(ILP,*) 'SUN ANGLE: ',sunang
c write(6,*) 'SCO X-1 ANGLE: ',scoang
c write(ILP,*) 'SCO X-1 ANGLE: ',scoang
9       format(' ***** WARNING--target is only',f7.3,
     .      ' degrees from the Sun.')
        if(sunang.lt.sunangmax) then
	  write(6,9) sunang
	  write(ILP,9) sunang
        endif
10      format(' ***** WARNING--target is only',f7.5,
     .      ' degrees from Sco X-1.')
        if(scoang.lt.scoangmax) then
	  write(6,10) scoang
	  write(ILP,10) scoang
        endif
        return
        end
c [marshall.aspect]dctohr.for                                                   
c
	subroutine dctohr(ra,dec,ihr,imn,sec,qsign,idc,idmn,dsec)
c
c this routine converts from decimal degrees ra and dec to hours
c
c mod 03-feb-88 to have ra and dec real*8
c mod 11-feb-88 to add qsign to arguments
c
	real*8 ra,dec,ra1,dc1,ahr,amn
	logical*1 qsign
c start with ra
	ra1=ra
	if(ra1.lt.0.)ra1=ra1+360.d0
	ahr=ra1/15.d0
	ihr=idint(ahr)
	amn=(ahr-ihr)*60.d0
	imn=idint(amn)
	sec=(amn-imn)*60.
c now do declination
	dc1=dabs(dec)
	idc=idint(dc1)
	amn=(dc1-idc)*60.d0                                        
	idmn=idint(amn)          
	dsec=(amn-idmn)*60.d0
	if(dec.lt.0.)then
	  qsign='-'
	else
	  qsign='+'
	endif
	return
	end
c ~marshall/fortran/utility/prtqt.f
c
c routines for printing a quaternion or rotation matrix
c
c both routines call dctohr, which can be found in
c  ~marshall/fortran/utility/dctohr.f & .o
c
	subroutine prtmat(amat)
c this subroutine prints out rotation matrix
	common/unicom/ILP,iin,iout
	real*8 amat(3,3),ra,dec,rad
	logical*1 qsign
	data rad/57.29577951d0/
        write(ILP,*) " ROTATION MATRIX:"
	write(ILP,96)((amat(i,j),j=1,3),i=1,3)
96	format(17x,3f8.3)
      do 100 i=1,3
	if(amat(i,3).ge.1.d0)then
	  dec=90.d0
	  else
	  if(amat(i,3).le.-1.d0)then
		dec=-90.d0
	  else
		dec=dasin(amat(i,3))*rad               
	  endif                   
	endif
	if(amat(i,1).ne.0. .or. amat(i,2).ne.0.)then
	  ra=datan2(amat(i,2),amat(i,1))*rad
	else
	  ra=0.
	endif
	if(ra.lt.0.)ra=ra+360.d0
	call dctohr(ra,dec,ihr,imn,sec,qsign,idc,idm,dsec)
	write(ILP,106)i,ra,dec,ihr,imn,sec,
     1		qsign,idc,idm,dsec
106	format(' AXIS ',i2,' RA & Dec:',2f9.3,
     1		1x,' = 'i2,'h',i2,'m',f3.0,'s',
     1		1x,a1,i2,'d',i2,'m',f3.0,'s')
100    continue
	return
	end
	subroutine prtqt(aqt)
c this subroutine prints out quaternion aqt
	common/unicom/ILP,iin,iout
	real*8 aqt(4),ra,dec,rad,amat(3),phi,sinp
	logical*1 qsign
	data rad/57.29577951d0/           
	phi=dacos(aqt(4))*2.d0    
	sinp=dsin(phi/2.d0)
	do 100 i=1,3
	  amat(i)=aqt(i)/sinp
100	continue
	if(amat(3).ge.1.d0)then
	  dec=90.d0
	  else
	  if(amat(3).le.-1.d0)then
		dec=-90.d0
	  else
		dec=dasin(amat(3))*rad
	  endif
	endif
	if(amat(1).ne.0. .or. amat(2).ne.0.)then
	  ra=datan2(amat(2),amat(1))*rad
	else
	  ra=0.
	endif
	if(ra.lt.0.)ra=ra+360.d0
	phi=phi*rad
	call dctohr(ra,dec,ihr,imn,sec,qsign,idc,idm,dsec)
	write(ILP,106)aqt,phi,ra,dec,ihr,imn,sec,               
     1		qsign,idc,idm,dsec,amat
106	format(' QUATERNION:',4f8.4,/,' or ROTATION by',f8.3,' deg'/,
     2		'      around RA & Dec:',2f9.3,' = (',
     1		1x,i2,'h',i2,'m',f3.0,'s, ',
     1		,a1,i2,'d',i2,'m',f3.0,'s)',/,
     3		' ROTATION VECTOR:',3f9.4)
	return
	end     

c ~marshall/fortran/utility/quaternion.f
c
c a collection of routines dealing with quaternions
c
	subroutine qtcopy(aqt,bqt)
c this routine copies quaterion aqt into bqt
c it assumes aqt is valild quaternion
	real*8 aqt(4),bqt(4)
	bqt(1)=aqt(1)
	bqt(2)=aqt(2)
	bqt(3)=aqt(3)
	bqt(4)=aqt(4)
	return
	end
	subroutine qtinv(aqt,bqt)
c this routine computes inverse of quaternion aqt
c it assumes aqt is valild quaternion
	real*8 aqt(4),bqt(4)
	bqt(1)=-aqt(1)
	bqt(2)=-aqt(2)
	bqt(3)=-aqt(3)
	bqt(4)=aqt(4)
	return
	end
	subroutine qtneg(aqt)
c this routine reverses the direction of quaternion aqt
c it assumes aqt is valild quaternion
	real*8 aqt(4)
	aqt(1)=-aqt(1)
	aqt(2)=-aqt(2)
	aqt(3)=-aqt(3)
	aqt(4)=-aqt(4)
	return
	end
	subroutine qtmult(aqt,bqt,cqt)
c this routine multiplies quaternions
c cqt corresponds to the rotation aqt followed by bqt
	real*8 aqt(4),bqt(4),cqt(4)
c we assume that we have valid quaternions
	cqt(1)=aqt(1)*bqt(4)+aqt(2)*bqt(3)-aqt(3)*bqt(2)+aqt(4)*bqt(1)
	cqt(2)=-aqt(1)*bqt(3)+aqt(2)*bqt(4)+aqt(3)*bqt(1)+aqt(4)*bqt(2)
	cqt(3)=aqt(1)*bqt(2)-aqt(2)*bqt(1)+aqt(3)*bqt(4)+aqt(4)*bqt(3)
 	cqt(4)=-aqt(1)*bqt(1)-aqt(2)*bqt(2)-aqt(3)*bqt(3)+aqt(4)*bqt(4)
 	return
	end 
	subroutine qtxra(cqt,ra,dec)
c
c this routine gets the ra & dec (in degrees) of the x-axis
c	corresponding to quaternion cqt
c  
	implicit real*8(a-g,o-z)
	dimension cqt(4),v1(3),ang1(2)
	data rad/57.29577951d0/
c
	v1(1)=cqt(1)*cqt(1)-cqt(2)*cqt(2)-cqt(3)*cqt(3)+cqt(4)*cqt(4)
	v1(2)=2.d0*(cqt(1)*cqt(2)+cqt(3)*cqt(4))
	v1(3)=2.d0*(cqt(1)*cqt(3)-cqt(2)*cqt(4))
	call angl(v1,ang1)
	ra=rad*ang1(1)
	dec=rad*ang1(2)
	return
	end
	subroutine findqt(amat,aqt)
c this routine converts rotation matrix amat into quaternion aqt
c it assumes amat is a valid rotation matrix
c this is adapted from chapter 12 by f.l.markley             
	real*8 amat(3,3),aqt(4)   
	aqt(4)=0.5d0*dsqrt(1.d0+amat(1,1)+amat(2,2)+amat(3,3))
	aqt(1)=(amat(2,3)-amat(3,2))/4.d0/aqt(4)
	aqt(2)=(amat(3,1)-amat(1,3))/4.d0/aqt(4)
	aqt(3)=(amat(1,2)-amat(2,1))/4.d0/aqt(4)
	return
	end

	subroutine snradc(jd,ra,dec)
c
c this routine computes approximate position of the sun for specified
c	julian date.
c algorithm taken from 1994 nautical almanac page c24.
c	accurate to 0.01 degrees from 1950 to 2050
c
c all variables are real*8
c
c input:
c	jd is julian date
c	can be either jd or jd-2440000.
c note that 0h jan1, 1994 is jd 2449353.5
c
c output:
c	ra is right ascension in degrees (j2000?)
c	dec is declination in degrees
c
	implicit real*8 (a-z)
c
	rad=57.29577951d0
	if(jd.gt.2000000.d0)then
	  n=jd-2451545.d0
	else
	  n=jd - 11545.d0
	endif
c l is mean longitude of sun, corrected for aberration
	l=dmod(280.466d0 + 0.9856474*n,360.d0)
c g is mean anomaly
	g=dmod(357.528d0 + 0.9856003d0*n,360.d0)
c elon is ecliptic longitude
	elon=l + dsin(g/rad)*1.915d0 + dsin(2.d0*g/rad)*0.02d0
c e is obliquity of ecliptic
	obl=23.440d0-(4.d-7)*n
	snl=dsin(elon/rad)
	ra=rad*datan2(snl*dcos(obl/rad),dcos(elon/rad))
	if(ra.lt.0.d0)ra=ra+360.d0
	dec=rad*dasin(dsin(obl/rad)*snl)
	return
	end
c [marshall.utility]vector.for
c these routines are common utilities involving vectors
       subroutine xyz(ang,v)
       implicit real*8 (a-h, o-z)
       dimension ang(2),v(3),v1(3),v2(3),ang1(2)
c  xyz computes coordinates of ra,dec
       v(3)=dsin(ang(2))
       x1=dcos(ang(2))
       v(1)=dcos(ang(1))*x1
       v(2)=dsin(ang(1))*x1
       return
c
       entry norm(v)
c converts vector v into a unit vector
       r=dsqrt(v(1)*v(1) + v(2)*v(2) +v(3)*v(3))
       v(1)=v(1)/r
       v(2)=v(2)/r
       v(3)=v(3)/r
       return
c
       entry cross(v,v1,v2)
c   computes cross product
       v2(1)=v(2)*v1(3)-v1(2)*v(3)
       v2(2)=v(3)*v1(1)-v(1)*v1(3)
       v2(3)=v(1)*v1(2)-v1(1)*v(2)
       return
c
       entry angl(v,ang1)
c   computes ra and dec from xyz
c mod 23-aug-89 to restrict ra to be positive
       ang1(1)=datan2(v(2),v(1))
	if(ang1(1).lt.0.)ang1(1)=ang1(1)+6.283185307d0
       r=dsqrt(v(1)*v(1)+v(2)*v(2)+v(3)*v(3))
       ang1(2)=dasin(v(3)/r)
       return
       end
c
       real*8 function dot(v1,v2)
c computes dot product of vectors v1 and v2
       real*8 v1(3),v2(3)
       dot=v1(1)*v2(1)+v1(2)*v2(2)+v1(3)*v2(3)
       return
       end

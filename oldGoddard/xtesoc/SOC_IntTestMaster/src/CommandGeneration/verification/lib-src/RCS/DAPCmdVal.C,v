head	1.8;
access
	soccm;
symbols;
locks;
comment	@ * @;


1.8
date	98.08.24.19.30.50;	author soccm;	state Exp;
branches;
next	1.7;

1.7
date	96.10.23.16.40.40;	author soccm;	state Exp;
branches;
next	1.6;

1.6
date	96.10.23.15.01.55;	author soccm;	state Exp;
branches;
next	1.5;

1.5
date	96.06.10.13.12.13;	author socdev;	state Exp;
branches;
next	1.4;

1.4
date	96.02.16.13.06.35;	author socdev;	state Exp;
branches;
next	1.3;

1.3
date	96.01.22.08.20.25;	author mmarlowe;	state Exp;
branches;
next	1.2;

1.2
date	95.12.29.19.52.01;	author mmarlowe;	state Exp;
branches;
next	1.1;

1.1
date	95.11.28.21.33.16;	author mmarlowe;	state Exp;
branches;
next	;


desc
@@


1.8
log
@this is the version used in Build 6; it claims to be version
1.4, but it differs a lot from that checked in version;
I am just making this version an official version for
Build 7 and beyond
@
text
@// ===========================================================================
// File:          DAPCmdVal.C
// Programmer:    Matthew Marlowe
// Project:       XTESOC Command Generation
// Subsystem:     Command Generation Verification
// Date:          11/28/95
// ===========================================================================
// Description:
//   Implementation of DAPCmdVal Class.  See DAPCmdVal.h or Command Generation
// Verification Design Document for more details.
static const char* const rcsid =
"$Id: DAPCmdVal.C,v 1.4 1996/02/16 13:06:35 socdev Exp $";

#include <iostream.h>
#include <string.h>
#include <stdlib.h>
#include <rw/collect.h>
#include <rw/rwfile.h>
#include <rw/vstream.h>
#include <rw/pstream.h>
#include <cgdefs.h>
#include <cgvdefs.h>
#include <DAPCmdVal.h>
#include <CgvEnumUtils.h>

RWDEFINE_COLLECTABLE( DAPCmdVal, DAPCmdVal_ID );


// Class Method Definitions


DAPCmdVal::DAPCmdVal()
{

  setDefaults();

}


DAPCmdVal::DAPCmdVal( const DAPCmdVal & from )
{

  from.copyData( this );

}


DAPCmdVal::DAPCmdVal( int initial_value )
{

  setDefaults(            );
  setValue( initial_value );

}


DAPCmdVal::DAPCmdVal( char * initial_string )
{

  setDefaults(              );
  setString( initial_string );

}


DAPCmdVal::DAPCmdVal( int initial_value, char * initial_string )
{

  setDefaults(             );
  setValue(  initial_value );
  setString( initial_string);
  setListType( VALIDITY_LIST );
}


RWBoolean
DAPCmdVal::isEqual( const RWCollectable * from ) const
{

  if (from->isA() != DAPCmdVal_ID ) return FALSE; 

  else {
    DAPCmdVal * tmptr = (DAPCmdVal *) from;
    return( *this == *tmptr );
  }

}

int
DAPCmdVal::operator==( const DAPCmdVal & from ) const
{

  if (this == &from ) return TRUE;
  if ( vflag != from.getVFlag() ) return FALSE;
  if ( sflag != from.getSFlag() ) return FALSE;
  if ( vflag )
    if ( value != from.getValue() ) return FALSE;
  if (sflag )
    if (strcmp((char *) string, from.getString()) != 0 ) return FALSE;
  if (listType != from.getListType() ) return FALSE;

  return( TRUE );

}


DAPCmdVal & 
DAPCmdVal::operator=( const DAPCmdVal & from )
{

  if (this ==  &from) return ( *this );
  freeData(           );
  from.copyData( this );
  return( *this       );

}


int
DAPCmdVal::getValue() const
{

  return( value          );

}


const char *
DAPCmdVal::getString() const
{

  return( (const char *) string );

}


int
DAPCmdVal::getVFlag() const
{

  return( vflag         );

}


int 
DAPCmdVal::getSFlag() const
{

  return( sflag         );

}


void
DAPCmdVal::setValue( int newValue ) 
{

  vflag = TRUE    ;
  value = newValue;

}


void
DAPCmdVal::setString( char * newString )
{

  sflag          = TRUE     ;
  strncpy( (char *) string, newString, sizeof( string ) );

}


void
DAPCmdVal::setVFlag( int flag )
{

  vflag = flag;

}


void
DAPCmdVal::setSFlag( int flag )
{

  sflag = flag;

}


void
DAPCmdVal::printShort( ostream & ostr ) const
{

  //ostr << enumMap( listType ) << " ";
  if (vflag) {
    if (listType==VALIDITY_LIST) {
      ostr << enumMap( (CgvCmdValidity ) value ) << " ";
    } 
    else {
      ostr << enumMap( (CgvCmdSeverity) value  ) << " ";
    }
  }
  if (sflag) ostr << "  \"" << (char *) string << "\"" << " ";
  ostr << endl;
  

}


void 
DAPCmdVal::print( ostream & ostr ) const
{

  printShort( ostr );

}


void
DAPCmdVal::printLong( ostream & ostr ) const
{

  printShort( ostr );

}


RWspace
DAPCmdVal::binaryStoreSize() const
{

  RWspace sum=0;
  
  sum += sizeof( int ) * 3;           // Private Data

  RWCString fs( string );
  sum += fs.binaryStoreSize();   // String

  sum += 2 * sizeof(int);             // Validation
  sum += RWCollectable::binaryStoreSize();

  sum += sizeof( int );               // listType

  return( sum );
}


void
DAPCmdVal::saveGuts( RWFile & fp ) const
{
  RWCollectable::saveGuts(fp);
  fp.Write( DAPCmdVal_ID     );
  fp.Write( vflag            );
  fp.Write( sflag            );
  fp.Write( value            );
  fp.Write( (int) listType   );

  RWCString fs( string );
  fs.saveOn( fp );

  fp.Write( DAPCmdVal_ID + 1 );

}


void
DAPCmdVal::saveGuts( RWvostream & ostr ) const
{
  RWCollectable::saveGuts(ostr);
  ostr << DAPCmdVal_ID;
  ostr << vflag;
  ostr << sflag;
  ostr << value;
  ostr << (int) listType;

  RWCString fs( string );
  ostr << fs;

  ostr << DAPCmdVal_ID + 1 ;

}

void
DAPCmdVal::restoreGuts( RWFile & fp )
{
  RWCollectable::restoreGuts(fp);
  freeData();

  int validation;

  fp.Read( validation );

  if ( validation != DAPCmdVal_ID ) {
    cerr << "DAPCmdVal::restoreGuts(RWFile &): initial validation failed.  DAPCmdVal not found." << endl;
    exit(-1);
  }

  fp.Read( vflag );
  fp.Read( sflag );
  fp.Read( value );
  int f;
  fp.Read( f );
  listType = (CgvListType) f;

  RWCString fs;
  fs.restoreFrom( fp );
  strcpy( string, fs.data() );

  fp.Read( validation );
 
  if ( validation != DAPCmdVal_ID + 1 ) {
    cerr << "DAPCmdVal::restoreGuts(RWFile &): final validation failed.  DAPCmdVal not found." << endl;
    exit(-1);
  }
  
}

void
DAPCmdVal::restoreGuts( RWvistream & istr )
{
  RWCollectable::restoreGuts( istr );
  freeData();

  int validation;

  istr >> validation;

  if ( validation != DAPCmdVal_ID ) {
    cerr << "DAPCmdVal::restoreGuts(RWvistream &): initial validation failed.  DAPCmdVal not found." << endl;
    exit(-1);
  }

  istr >> vflag;
  istr >> sflag;
  istr >> value;

  int f;
  istr >> f;
  listType = (CgvListType ) f;

  RWCString fs;
  istr >> fs;
  strncpy( string, fs.data(), 255 ); 

  istr >> validation;
 
  if ( validation != DAPCmdVal_ID + 1 ) {
    cerr << "DAPCmdVal::restoreGuts(RWvistream &): final validation failed.  DAPCmdVal not found." << endl;
    exit(-1);
  }
  
}

  
void
DAPCmdVal::setDefaults()
{
  memset( string, 0, sizeof(string) );
  strncpy( string, "", sizeof(string) );
  value  = 0;
  vflag  = 0;
  sflag  = 0;
  listType = VALIDITY_LIST;
}


void
DAPCmdVal::setListType( CgvListType x )
{
  listType = x;
}

CgvListType
DAPCmdVal::getListType() const
{
  return( listType );
}

void
DAPCmdVal::copyData( DAPCmdVal * to ) const 
{

  to->setString( (char *) string );
  to->setValue(  value          );
  to->setVFlag(  vflag          );
  to->setSFlag(  sflag          );
  to->setListType( listType     );

}


void
DAPCmdVal::freeData() 
{
}


DAPCmdVal::~DAPCmdVal()
{

  freeData();

}


//==================================================================================
//
// This file was auto-formatted by Emacs C++ Mode with Stroustrup Indentation Style
// It is best viewed using hl319 mode.
//
// Compile with " cd $SOCHOME/src/CommandGeneration/verification ; make "
//
//==================================================================================









@


1.7
log
@Removed *ALL* log messages...
@
text
@d1 1
a1 1
//================================================================================
d7 1
a7 1
//================================================================================
d9 1
a9 1
//      Implementation of DAPCmdVal Class.  See DAPCmdVal.h or Command Generation
d11 2
a12 5
//================================================================================
// Configuration Management Information:
//    $Id: DAPCmdVal.C,v 1.6 1996/10/23 15:01:55 soccm Exp soccm $
//
//================================================================================
a20 1
#include <rw/collstr.h>
d31 9
d42 3
a44 3
  classification= from.getClassification();
  description   = from.getDescription();
  listType      = from.getListType();
d47 2
a48 1
DAPCmdVal::DAPCmdVal( int initial_value, char * initial_string, CgvListType inital_type )
d51 5
a55 1
  classification = initial_value;
d57 2
a58 4
  if (initial_string == NULL )
    description = "";
  else 
    description    = initial_string;
d60 2
a61 1
  listType       = inital_type;
d66 10
d80 1
a80 3
  // 
  // Checking method arguments first
  //
d82 3
a84 8
  if (from == NULL ) {
    cerr << "DAPCmdVal::isEqual() called against NULL" << endl;
    exit(-1);
  }

  if (from->isA() != DAPCmdVal_ID ) {
    cerr << "DAPCmdVal::isEqual() called against non-DAPCmdVal" << endl;
    exit(-1);
a86 8
  //
  // There are a few ways to implement isEqual- Identity, simple
  // equality, or complicated equality.  For this class, we choose
  // simple equality.
  //

  return( *((DAPCmdVal *) from ) == *this );

d93 8
a100 1
  if ( this == &from ) return TRUE;
d102 1
a102 5
  else if (classification != from.getClassification() ) return FALSE;
  else if (description != from.getDescription() ) return FALSE;
  else if (listType != from.getListType() ) return FALSE;
  
  else return TRUE;
d112 56
a167 5
  else {
    classification = from.getClassification();
    description    = from.getDescription(   );
    listType       = from.getListType(      );
  }
d169 2
a170 1
  return( *this );
d175 5
a179 6
void DAPCmdVal::setClassification( int x )     {  classification = x; }
void DAPCmdVal::setDescription( const char * x ) { description = x;   }
void DAPCmdVal::setListType( CgvListType x )   { listType = x;        }
int  DAPCmdVal::getClassification() const      { return classification; }
const char * DAPCmdVal::getDescription() const { return description;  }
CgvListType  DAPCmdVal::getListType() const    { return listType;     }
d181 10
d197 12
a208 4
  ostr << "Statement: " << enumMap( (CgvListType) listType );
  ostr << "  Classification: " << classification << endl;
  ostr << "Description: " << description << endl;

d237 6
a242 4
  sum += sizeof(int);    // ListType
  sum += sizeof(int);    // Classification
  sum += description.binaryStoreSize();  // Description
  
d244 3
a246 1
    
a253 1

d256 8
a263 3
  fp.Write( classification );
  description.saveGuts(fp);
  fp.Write( (int) listType );
a271 1

d274 4
a277 2
  ostr << classification;
  ostr << (RWCollectable & ) description;
d279 2
a280 2
  int y = (int ) listType;
  ostr << y;
d290 1
d293 1
d295 1
d301 10
a310 2
  fp.Read(classification);
  description.restoreGuts(fp);
a311 4
  int y;
  fp.Read(y);
  listType = (CgvListType) y;
  
d313 1
a323 1

d325 1
d328 1
d330 1
d336 11
a346 2
  istr >> classification;
  istr >> (RWCollectable & ) description;
a347 4
  int y;  
  istr >> y;
  listType = (CgvListType) y;
  
d349 1
d357 42
d403 2
@


1.6
log
@Removed problematic log messages!
@
text
@d13 1
a13 9
//    $Id: DAPCmdVal.C,v 1.5 1996/06/10 13:12:13 socdev Exp $
//
// $Log: DAPCmdVal.C,v $
// * Revision 1.5  1996/06/10  13:12:13  socdev
// * *** empty log message ***
// Revision 1.4  1996/02/16 13:06:35  socdev
// Revision 1.1  1995/11/28 21:33:16  mmarlowe
// Initial revision
//
@


1.5
log
@*** empty log message ***
@
text
@d13 1
a13 1
//    $Id: DAPCmdVal.C,v 1.4 1996/02/16 13:06:35 socdev Exp $
d16 2
@


1.4
log
@/
@
text
@d13 1
a13 1
//    $Id: DAPCmdVal.C,v 1.1 1995/11/28 21:33:16 mmarlowe Exp $
d16 1
d30 1
a40 9

DAPCmdVal::DAPCmdVal()
{

  setDefaults();

}


d43 3
a45 3

  from.copyData( this );

d48 1
a48 2

DAPCmdVal::DAPCmdVal( int initial_value )
d51 1
a51 5
  setDefaults(            );
  setValue( initial_value );

}

d53 4
a56 2
DAPCmdVal::DAPCmdVal( char * initial_string )
{
d58 1
a58 2
  setDefaults(              );
  setString( initial_string );
d63 2
a64 1
DAPCmdVal::DAPCmdVal( int initial_value, char * initial_string )
d67 3
a69 5
  setDefaults(             );
  setValue(  initial_value );
  setString( initial_string);
  setListType( VALIDITY_LIST );
}
d71 4
d76 4
a79 3
RWBoolean
DAPCmdVal::isEqual( const RWCollectable * from ) const
{
d81 5
a85 1
  if (from->isA() != DAPCmdVal_ID ) return FALSE; 
d87 1
a87 4
  else {
    DAPCmdVal * tmptr = (DAPCmdVal *) from;
    return( *this == *tmptr );
  }
d95 1
a95 8
  if (this == &from ) return TRUE;
  if ( vflag != from.getVFlag() ) return FALSE;
  if ( sflag != from.getSFlag() ) return FALSE;
  if ( vflag )
    if ( value != from.getValue() ) return FALSE;
  if (sflag )
    if (strcmp((char *) string, from.getString()) != 0 ) return FALSE;
  if (listType != from.getListType() ) return FALSE;
d97 5
a101 1
  return( TRUE );
d111 5
a115 5
  freeData(           );
  from.copyData( this );
  return( *this       );

}
d117 1
a117 24

int
DAPCmdVal::getValue() const
{

  return( value          );

}


const char *
DAPCmdVal::getString() const
{

  return( (const char *) string );

}


int
DAPCmdVal::getVFlag() const
{

  return( vflag         );
d122 6
a127 5
int 
DAPCmdVal::getSFlag() const
{

  return( sflag         );
a128 1
}
d132 1
a132 21
DAPCmdVal::setValue( int newValue ) 
{

  vflag = TRUE    ;
  value = newValue;

}


void
DAPCmdVal::setString( char * newString )
{

  sflag          = TRUE     ;
  strncpy( (char *) string, newString, sizeof( string ) );

}


void
DAPCmdVal::setVFlag( int flag )
d135 3
a137 4
  vflag = flag;

}

a138 25
void
DAPCmdVal::setSFlag( int flag )
{

  sflag = flag;

}


void
DAPCmdVal::printShort( ostream & ostr ) const
{

  //ostr << enumMap( listType ) << " ";
  if (vflag) {
    if (listType==VALIDITY_LIST) {
      ostr << enumMap( (CgvCmdValidity ) value ) << " ";
    } 
    else {
      ostr << enumMap( (CgvCmdSeverity) value  ) << " ";
    }
  }
  if (sflag) ostr << "  \"" << (char *) string << "\"" << " ";
  ostr << endl;
  
d167 4
a170 6
  sum += sizeof( int ) * 3;           // Private Data

  RWCString fs( string );
  sum += fs.binaryStoreSize();   // String

  sum += 2 * sizeof(int);             // Validation
d172 1
a172 3

  sum += sizeof( int );               // listType

d180 1
d183 3
a185 8
  fp.Write( vflag            );
  fp.Write( sflag            );
  fp.Write( value            );
  fp.Write( (int) listType   );

  RWCString fs( string );
  fs.saveOn( fp );

d194 1
d197 2
a198 4
  ostr << vflag;
  ostr << sflag;
  ostr << value;
  ostr << (int) listType;
d200 2
a201 2
  RWCString fs( string );
  ostr << fs;
a210 1
  freeData();
a212 1

a213 1

d219 2
a220 10
  fp.Read( vflag );
  fp.Read( sflag );
  fp.Read( value );
  int f;
  fp.Read( f );
  listType = (CgvListType) f;

  RWCString fs;
  fs.restoreFrom( fp );
  strcpy( string, fs.data() );
d222 4
a226 1
 
d237 1
a238 1
  freeData();
a240 1

a241 1

d247 2
a248 11
  istr >> vflag;
  istr >> sflag;
  istr >> value;

  int f;
  istr >> f;
  listType = (CgvListType ) f;

  RWCString fs;
  istr >> fs;
  strncpy( string, fs.data(), 255 ); 
d250 4
a254 1
 
a261 42
  
void
DAPCmdVal::setDefaults()
{
  memset( string, 0, sizeof(string) );
  strncpy( string, "", sizeof(string) );
  value  = 0;
  vflag  = 0;
  sflag  = 0;
  listType = VALIDITY_LIST;
}


void
DAPCmdVal::setListType( CgvListType x )
{
  listType = x;
}

CgvListType
DAPCmdVal::getListType() const
{
  return( listType );
}

void
DAPCmdVal::copyData( DAPCmdVal * to ) const 
{

  to->setString( (char *) string );
  to->setValue(  value          );
  to->setVFlag(  vflag          );
  to->setSFlag(  sflag          );
  to->setListType( listType     );

}


void
DAPCmdVal::freeData() 
{
}
a265 2

  freeData();
@


1.3
log
@checkin 2
@
text
@d32 1
a32 1

d80 14
a96 1

a101 1

d108 2
a119 1

d205 11
a215 2
  if (vflag) ostr << value << endl;
  if (sflag) ostr << (char *) string << endl;
d253 2
d267 1
d285 1
d312 3
d348 4
d374 1
d379 12
d398 1
@


1.2
log
@Introduce cgv to build
@
text
@a50 1
  setDefaults(        );
d88 2
d92 4
a95 3
  if ( value != from.getValue() ) return FALSE;
  if (!(strcmp(string->data(), from.getString())) ) return FALSE;

d104 3
a106 1
 
a107 1
  setDefaults(        );
d127 1
a127 1
  return( string->data() );
d165 2
a166 3
  if (newString != NULL ) 
    *string      = newString;
  else *string   = ""       ;
d192 3
a194 3
  if (vflag) ostr << value;
  if (sflag) ostr << string;
  ostr << endl;
d225 2
a226 1
  sum += string->binaryStoreSize();   // String
d229 1
d238 1
a238 1

d243 4
a246 1
  string->saveOn( fp         );
d255 1
a255 1
  
d260 4
a263 1
  string->saveOn(ostr);
d271 1
a271 1

a272 1
  setDefaults();
d286 4
a289 1
  string->restoreFrom( fp );
d303 1
a303 1

a304 1
  setDefaults();
d319 4
a322 2
  string->restoreFrom( istr );
  
d336 2
a337 2

  string = new RWCString("");
d348 1
a348 1
  to->setString( (char *) string->data() );
a358 4

  delete( string );
  string = NULL   ;

@


1.1
log
@Initial revision
@
text
@d13 5
a17 1
//    $Id:$
a18 1
// $Log:$
d258 1
a258 1

d288 1
a288 1

d335 1
a335 1
  to->setString( string->data() );
@

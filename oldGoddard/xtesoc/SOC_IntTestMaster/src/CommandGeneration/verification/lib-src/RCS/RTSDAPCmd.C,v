head	1.2;
access
	soccm;
symbols;
locks; strict;
comment	@ * @;


1.2
date	96.02.16.13.06.35;	author socdev;	state Exp;
branches;
next	1.1;

1.1
date	96.01.22.08.20.25;	author mmarlowe;	state Exp;
branches;
next	;


desc
@@


1.2
log
@/
@
text
@//========================================================================================
// File:       RTSDAPCmd.C
// Programmer: Matthew Marlowe
// Project:    XTESOC Command Generation Verification
// Date:       12/1/95
//=========================================================================================
// $Id: RTSDAPCmd.C,v 1.1 1995/12/01 22:13:38 mmarlowe Exp $
//
// $Log: RTSDAPCmd.C,v $
// Revision 1.1  1995/12/01 22:13:38  mmarlowe
// Initial revision
//
//
//=========================================================================================

#include<iostream.h>
#include<stdlib.h>
#include<string.h>
#include<assert.h>
#include<rw/collect.h>
#include<rw/pstream.h>
#include<rw/vstream.h>
#include<rw/cstring.h>
#include<cgdefs.h>
#include<cgvdefs.h>
#include<RTSDAPCmd.h>
#include<DAPCmdVal.h>
#include<DAPCmdValList.h>
#include<DAPCmdStat.h>
#include<DAPCmd.h>

int RTSDAPCmd::numRTSCalls = 0;

RTSDAPCmd::RTSDAPCmd()
:DAPCmd()
{
  setDefaults();
}


RTSDAPCmd::RTSDAPCmd( const char * zcmd, const char * zcmdline )
:DAPCmd( zcmd, zcmdline ) 
{
  setDefaults();
}


RTSDAPCmd::RTSDAPCmd( const RTSDAPCmd & from )
:DAPCmd( from )
{
  setDefaults();
  from.copyData(*this);
}


DAPCmdValList * 
DAPCmd::isValidCmd()
{
  DAPCmdValList * zlist = new DAPCmdValList();
  zlist->setListType( VALIDITY_LIST );


  RWCTokenizer next( getCommand() );
  RWCString token;
  int wordCount = 0;

  while(!(token=next()).isNULL()) {
    wordCount++;
    if (wordCount==1) {
      if( strcmp(token.data(), "RTSCTRL") == 0 ) { 
	zlist->setValidity( CMD_IS_VALID, "RTSCTRL Command" );
      }
      else if( strcmp(token.data(), "RTSLOAD") == 0 ) {
	zlist->setValidity( CMD_IS_VALID, "RTSLOAD Command" );
      }
      else if( strcmp(token.data(), "RTSDEL") == 0 ) {
	zlist->setValidity( CMD_IS_VALID, "RTSDEL Command" );
      }
      else 
	zlist->setValidity( CMD_IS_INVALID, "Unrecognizeable RTS Command" );
    }
    if (wordcount > 2 ) 
      zlist->setValidity( CMD_IS_INVALID, "Too Many Arguments to RTS Command" );

  }

  if( wordCount == 0 ) {
    zlist->setValidity( CMD_IS_INVALID, "NULL Command" );
  }

  return( zlist );
    
  
}

DAPCmdValList *
RTSDAPCmd::isDangerousCmd() const
{
  
  DAPCmdValList * zlist = new DAPCmdValList();
  zlist->setListType(DANGER_LIST );

  // There are no dangerous RTS commands specified at the moment.
  //
  // Making sure RTSLOADS/CALLS are matches is responsibility of RTSMgr
  //

  zlist->setDanger( CMD_IS_SAFE, "No dangerous RTS Commands are known" );

  return( zlist );

  

}


const char *
RTSDAPCmd::interpretCmd( TranslationType ttype ) const
{
  RWCString cs;
  
 
  switch( ttype ) 
    {
    case( NO_TRANSLATION ) :
    {
      return(NULL);
      break;
    }
  case( IT_TRANSLATION ) :
  case( SOF_TRANSLATION) :
  case( ENGLISH_TRANSLATION):

    RWCTokenizer next( getCommand() );
    RWCString token;
    int wordCount = 0;
    
    while(!(token=next()).isNULL()) {
      wordCount++;
      if (wordCount==1) {
	if( strcmp(token.data(), "RTSCTRL") == 0 ) { 
	  cs.append("Call to RTS ");
	}
	else if( strcmp(token.data(), "RTSLOAD") == 0 ) {
	  cs.append("Loading of RTS ");
	}
	else if( strcmp(token.data(), "RTSDEL") == 0 ) {
	  cs.append("Deleting of RTS ");
	}
	else 
	  cs.append("Untranslateable RTS Command");
      }
      if (wordcount==2 ) 
	cs.append(token);
    }

  };

  return(cs.data() );
}

void
RTSDAPCmd::printShort( ostream & ostr ) const
{
  DAPCmd::printShort( ostr );
  ostr << interpretCmd(ENGLISH_TRANSLATION);
}

void
RTSDAPCmd::print( ostream & ostr ) const
{
  DAPCmd::print( ostr );
  ostr << interpretCmd(ENGLISH_TRANSLATION );
}

void
RTSDAPCmd::printLong( ostream & ostr ) const
{ 
  DAPCmd::printLong( ostr );
  ostr << interpretCmd( ENGLISH_TRANSLATION );
}


void
RTSDAPCmd::processCmd() const
{
  DAPCmd::processCmd();
  numRTSCalls++;
}


DAPCmdStat *
RTSDAPCmd::getStatistic( CgvStatType st )
{
  
  DAPCmdStat * zstat;
  
  if( st == NRTSCALLS ) {
    zstat->setType( NRTSCALLS);
    zstat->setValue(numRTSCalls);
    return( zstat );
  }

  return(NULL );

}

void
RTSDAPCmd::~RTSDAPCmd()
{
  freeData();
}

void
RTSDAPCmd::freeData()
{
}

void
RTSDAPCmd::copyData( RTSDAPCmd & to ) const
{
}

void
RTSDAPCmd::setDefaults( )
{
}

@


1.1
log
@Initial revision
@
text
@@

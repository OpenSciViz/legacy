head	1.2;
access
	mmarlowe
	soccm;
symbols
	Current:1.2;
locks
	rhee:1.2; strict;
comment	@ * @;


1.2
date	93.06.10.19.50.47;	author rhee;	state Exp;
branches;
next	1.1;

1.1
date	93.06.10.16.11.36;	author rhee;	state Exp;
branches;
next	;


desc
@Creation
@


1.2
log
@"Descriptor.h" instead of "SimpleDescriptor.h".
@
text
@// Program Name: AccPcaHK.C
// Subsystem   : Data Management (PCA (Build 1) housekeeping part)
// Programmer  : Randall D. Barnette -- Hughes STX
//               Hwa-ja Rhee -- Hughes STX
// Description :
//  This file is the definition of class AccPcaHK. It is a subclass of
//  Accessor to retrieve values from PCA housekeeping packets.
//
// RCS stamp
static const char rcsid[]="$Id: AccPcaHK.C,v 1.1 1993/06/10 16:11:36 rhee Exp rhee $";

//  Feature test switches

// System headers
#include <string.h>

// Local headers
#include "AccPcaHK.h"
#include "Descriptor.h"

// Private headers
#include "pcahk.h"              // Declare PcaHKData struct
#include "pcahkDesc.h"          // The list of PCA data items

// File scope functions

// Program name: decodePcaHk.C 
// Functions   : Decode PCA housekeeping telemetry packet to local structure.
// Author      : H. Rhee       
// Date        : 1/13/93

//
// convert unsigned char to a integer
//
inline int getint(const unsigned char* buf)
{
  return buf[0] << 24 | buf[1] << 16 | buf[2] << 8 | buf[3] ;
}

//
// convert 2 byte characters
//
inline short getshort( const unsigned char * sbuf)
{
  return sbuf[0] << 8 | sbuf[1] ;
}

//
// bit position 0 is at the right end
//
inline unsigned getbits(unsigned x, int p, int n)
{
  return ( x >> (p+1-n) ) & ~( ~0 << n ) ;
}

//
// Decode a packet's bits
//
static PcaHKData* decodePcaHK( PktPca* thePacket )
{

// We must allocate rec on the heap and return pointer to it --RDB

  PcaHKData * rec = new PcaHKData ;

// These were not set by the original decoder --RDB
  rec->reserved1 = 0 ;
  rec->spareV = 0 ;
     
  static unsigned char buf[4], temp[8], sbuf[2] ;
  const unsigned char *tlmdat = thePacket->rawData() ;
  int id, cnt, leng, psec, segflg;
  unsigned inum ;
  
// decode primary header
  
  inum=getint(tlmdat);
  id = rec->apid =getbits(inum,26,11) ; /*apid from ccsds*/
  cnt= getbits(inum,13,14); /*sequence count in ccsds*/
  segflg=getbits(inum,15,2); /*segment  flag */

  leng=getshort( &tlmdat[4]) + 1;

// decode secondary header

  psec= rec->time = getint( &tlmdat[6]) ; 

#ifdef DEBUG
  cout << "-ccsds Apid: " << id
       << " time: "         << psec
       << " seq#: "         << cnt
       << " flg: "          << segflg
       << " length: "       << leng
       << "\n" ; 
#endif DEBUG
 
// extract first 8 rates    
// decode 8 event rates used by high rate monitor
        
  rec->evL1 = getbits( getint( &tlmdat[14] ), 23, 24 ) ; // layer 1 left
  rec->evR1 = getbits( getint( &tlmdat[18] ), 23, 24 ) ; // layer 1 right
  rec->evL2 = getbits( getint( &tlmdat[22] ), 23, 24 ) ; // layer 2 left
  rec->evR2 = getbits( getint( &tlmdat[26] ), 23, 24 ) ; // layer 2 right
  rec->evL3 = getbits( getint( &tlmdat[30] ), 23, 24 ) ; // layer 3 left
  rec->evR3 = getbits( getint( &tlmdat[34] ), 23 ,24 ) ; // layer 3 right
  rec->evVx = getbits( getint( &tlmdat[38] ), 23 ,24 ) ; // xenon layer 
  rec->evVp = getbits( getint( &tlmdat[42] ), 23 ,24 ) ; // propane layer

// decode 6 event rates not used by high rate monitor
  
  rec->evVpvle   = getbits( getint( &tlmdat[46] ), 23, 24 ) ;
  rec->evXevle   = getbits( getint( &tlmdat[50] ), 23 ,24 ) ;
  rec->ev2ormore = getbits( getint( &tlmdat[54] ), 23 ,24 ) ;
  rec->evAlpha   = getbits( getint( &tlmdat[58] ), 23 ,24 ) ;
  rec->evVxvle   = getbits( getint( &tlmdat[62] ), 23 ,24 ) ;
  rec->evXegood  = getbits( getint( &tlmdat[66] ), 23, 24 ) ;

// extract various housekeeping status bits

// discriminator settings

  rec->dsR2     = getbits( tlmdat[70], 7, 2 ) ; // layer 2 right          
  rec->dsL2     = getbits( tlmdat[70], 5, 2 ) ; // layer 2 left
  rec->dsR1     = getbits( tlmdat[70], 3, 2 ) ; // layer 1 right
  rec->dsL1     = getbits( tlmdat[70], 1, 2 ) ; // layer 1 left

  rec->dsVp     = getbits( tlmdat[71], 7, 2 ) ; // propane layer          
  rec->dsVx     = getbits( tlmdat[71], 5, 2 ) ; // xenon layer
  rec->dsR3     = getbits( tlmdat[71], 3, 2 ) ; // layer 3 right
  rec->dsL3     = getbits( tlmdat[71], 1, 2 ) ; // layer 3 left

  rec->dither   = getbits( tlmdat[72], 4, 1 ) ; // dither on/off
  rec->dsVle    = getbits( tlmdat[72], 3, 2 ) ;   
  rec->dsAlpha  = getbits( tlmdat[72], 1, 2 ) ;
       
// test pulse generator status

  rec->tpgR3    = getbits( tlmdat[73], 5, 1 ) ; // layer 3 right              
  rec->tpgL3    = getbits( tlmdat[73], 4, 1 ) ; // layer 3 left
  rec->tpgR2    = getbits( tlmdat[73], 3, 1 ) ; // layer 2 right
  rec->tpgL2    = getbits( tlmdat[73], 2, 1 ) ; // layer 2 left
  rec->tpgR1    = getbits( tlmdat[73], 1, 1 ) ; // layer 1 right
  rec->tpgL1    = getbits( tlmdat[73], 0, 1 ) ; // layer 1 left
  rec->tpgAlpha = getbits( tlmdat[74], 4, 1 ) ;
  rec->tpgVp    = getbits( tlmdat[74], 3, 1 ) ;
  rec->tpgVx    = getbits( tlmdat[74], 2, 1 ) ;
  rec->tpgMode  = getbits( tlmdat[74], 1, 1 ) ; // tpg mode to ramp or mono
  rec->tpgEn    = getbits( tlmdat[74], 0, 1 ) ; // tpg enabled

  rec->hvXeset  = getbits( tlmdat[75], 7, 4 ) ; // High Voltage Xe setting  
  rec->hvPrset  = getbits( tlmdat[75], 3, 4 ) ; // High Voltage Pr setting

// high voltage and low voltage relay drive on/off

  rec->pron     = getbits( tlmdat[76], 5, 1 ) ; // Pr HV relay on            
  rec->proff    = getbits( tlmdat[76], 4, 1 ) ; // Pr HV relay off
  rec->xeon     = getbits( tlmdat[76], 3, 1 ) ; // Xe HV relay on
  rec->xeoff    = getbits( tlmdat[76], 2, 1 ) ; // Xe HV relay off
  rec->lvon     = getbits( tlmdat[76], 1, 1 ) ; // low voltage relay on
  rec->lvoff    = getbits( tlmdat[76], 0, 1 ) ; // low voltage relay off

  rec->hvPrrly  = getbits( tlmdat[77], 4, 1 ) ; // Pr Hv relay status
  rec->hvXerly  = getbits( tlmdat[77], 3, 1 ) ; // Xe Hv relay status
  rec->lvPsrly  = getbits( tlmdat[77], 2, 1 ) ; // Low Voltage relay status
  rec->hvPron   = getbits( tlmdat[77], 1, 1 ) ; // Pr Hv supply status
  rec->hvXeon   = getbits( tlmdat[77], 0, 1 ) ; // Xe Hv supply status 

  rec->selB     = getbits( tlmdat[78], 7, 1 ) ; // Select 'B' side signal
  rec->rateOv   = getbits( tlmdat[78], 6, 1 ) ; // Override watchdog rate
  rec->ppsOv    = getbits( tlmdat[78], 5, 1 ) ; // Override lpps watchdog
  rec->hrmOv    = getbits( tlmdat[78], 4, 1 ) ; // Override HRM
  rec->hvinh3   = getbits( tlmdat[78], 3, 1 ) ; // hv inhibit (rate readout)
  rec->hvinh2   = getbits( tlmdat[78], 2, 1 ) ; // hv inhibit (lpps timeout)
  rec->hvinh1   = getbits( tlmdat[78], 1, 1 ) ; // hv inhibit (hrm)    
  rec->hrmFlag  = getbits( tlmdat[78], 0, 1 ) ; // state of hrm         

// extract high rate monitor threshold

  rec->hrm = getint( &tlmdat[79]) >> 8 ;
#ifdef DEBUG
  cout << "hrm: " << rec->hrm << "\n" ;
#endif
 
// extract various analog housekeeping data information
 
  rec->tmpIF  = getshort(&tlmdat[84]) & 0x0fff ; // I/F board temperature
  rec->tmpADC = getshort(&tlmdat[86]) & 0x0fff ; // ADC temperature
  rec->tmpCSA = getshort(&tlmdat[88]) & 0x0fff ; // CSA temperature
  rec->tmpSA  = getshort(&tlmdat[90]) & 0x0fff ; // SA  temperature
  rec->tmpTPG = getshort(&tlmdat[92]) & 0x0fff ; // TPG temperature
  rec->tmpLVPS= getshort(&tlmdat[94]) & 0x0fff ; // LVPS temperature
  rec->tmpHVPS= getshort(&tlmdat[96]) & 0x0fff ; // HVPS temperature

  rec->p12va = getshort(&tlmdat[98] ) & 0x0fff ; // +12V analog      
  rec->m12va = getshort(&tlmdat[100]) & 0x0fff ; // -12V analog      
  rec->p6va  = getshort(&tlmdat[102]) & 0x0fff ; // +6V analog      
  rec->m6va  = getshort(&tlmdat[104]) & 0x0fff ; // -6V analog      
  rec->p12vd = getshort(&tlmdat[106]) & 0x0fff ; // +12V digital     
  rec->m12vd = getshort(&tlmdat[108]) & 0x0fff ; // -12V digital     
  rec->p5vd  = getshort(&tlmdat[110]) & 0x0fff ; // +5V digital     

  rec->prsXe = getshort(&tlmdat[114]) & 0x0fff ; // Xe pressure monitor
  rec->prsPr = getshort(&tlmdat[116]) & 0x0fff ; // Pr pressure monitor
  rec->xeHV  = getshort(&tlmdat[118]) & 0x0fff ; // Xe HVPS monitor
  rec->prHV  = getshort(&tlmdat[120]) & 0x0fff ; // Pr HVPS monitor

	// return  structure rec
  return rec ;
}

// ========================================================================
// AccPcaHK
// ========================================================================

// File scope variables

//
// This static value is the number of PCA descriptors in "pcahkDesc.h"
//
static const int theCount = sizeof(pca)/sizeof(struct descTags) ;

//
// Class variable theDescriptorCount declared and initialized
//
const int AccPcaHK::theDescriptorCount = theCount ;

//
// This function builds the list of descriptors from the PCA item
// list ("pcahkDesc.h")
//
static SimpleDescriptor ** makeDescriptorList()
{
	SimpleDescriptor** theList = new SimpleDescriptor*[theCount] ;
	for( int i = 0 ; i < theCount ; i++ )
		theList[i] = new SimpleDescriptor( pca[i].tag ) ;
	return theList ;
}

//
// Class variable theDescriptors declared and initialized
//
SimpleDescriptor ** AccPcaHK::theDescriptors = makeDescriptorList() ;

//
// Define some RogueWave required functions
//
RWDEFINE_COLLECTABLE(AccPcaHK, AccPcaHK_ID)

//
// Constructor for RogueWave use
//
AccPcaHK::AccPcaHK() {}

//
// Public Constructor
//
AccPcaHK::AccPcaHK( PktPca& aPacket )
{
#ifdef SELF_ARCHIVING
	//
	// If archiving, then allocate a Packet and copy contents of argument
	// In general, ingest process will delete aPacket
	//
	thePacket = new PktPca(aPacket) ;
#else
	//
	// If not archiving, keep enough info to retrieve packet
	//
	theTime = aPacket.seconds() ;
	theApid = aPacket.applicationID() ;
#endif SELF_ARCHIVING
}

//
// Destructor
//
AccPcaHK::~AccPcaHK() { }

// -----------------------------------------
// The following are inherited from Accessor
// -----------------------------------------

//
// Return the apid of the stored packet
//
int AccPcaHK::apid(void) const
{
#ifdef SELF_ARCHIVING
	return thePacket->applicationID() ;
#else
	return theApid ;
#endif SELF_ARCHIVING
}

//
// Return the time stamp of the stored packet
//
unsigned long AccPcaHK::time(void) const
{
#ifdef SELF_ARCHIVING
	return thePacket->seconds() ;
#else
	return theTime ;
#endif SELF_ARCHIVING
}

//
// Return a value from the packet data corresponding to the descriptor
// argument
//
DataValue * AccPcaHK::access( Descriptor* aSelection ) const
{

	// For now check only for "all" descriptor
	if( strcmp( "all", ((SimpleDescriptor*)aSelection)->getID() ) ) return 0 ;

  // decodePcaHK allocates a Data struct and returns pointer
  // The value stored in the MData must outlive subprogram scope
  // Allocate it here each time the accessor is called.

  PcaHKData* d = decodePcaHK( thePacket ) ;

  DataType* comps[80] ;
  int ncomps = 0 ;

  // Build a DataType Object -- PCA HK has 78 elements

  comps[ncomps++] = new DataType( BaseType_INT ) ; // apid
  comps[ncomps++] = new DataType( BaseType_INT ) ; // time
  comps[ncomps++] = new DataType( BaseType_INT ) ; // evL1
  comps[ncomps++] = new DataType( BaseType_INT ) ; // evR1
  comps[ncomps++] = new DataType( BaseType_INT ) ; // evL2

  comps[ncomps++] = new DataType( BaseType_INT ) ; // evR2
  comps[ncomps++] = new DataType( BaseType_INT ) ; // evL3
  comps[ncomps++] = new DataType( BaseType_INT ) ; // evR3
  comps[ncomps++] = new DataType( BaseType_INT ) ; // evVx
  comps[ncomps++] = new DataType( BaseType_INT ) ; // evVp

  comps[ncomps++] = new DataType( BaseType_INT ) ; // evVpvle
  comps[ncomps++] = new DataType( BaseType_INT ) ; // evXevle
  comps[ncomps++] = new DataType( BaseType_INT ) ; // ev2ormore
  comps[ncomps++] = new DataType( BaseType_INT ) ; // evAlpha
  comps[ncomps++] = new DataType( BaseType_INT ) ; // evVxle

  comps[ncomps++] = new DataType( BaseType_INT ) ; // evVxgood
  comps[ncomps++] = new DataType( BaseType_INT ) ; // hrm
  comps[ncomps++] = new DataType( BaseType_SHORT ) ; // tmpIF
  comps[ncomps++] = new DataType( BaseType_SHORT ) ; // tmpADC
  comps[ncomps++] = new DataType( BaseType_SHORT ) ; // tmpCSA

  comps[ncomps++] = new DataType( BaseType_SHORT ) ; // tmpSA
  comps[ncomps++] = new DataType( BaseType_SHORT ) ; // tmpTPG
  comps[ncomps++] = new DataType( BaseType_SHORT ) ; // tmpLVPS
  comps[ncomps++] = new DataType( BaseType_SHORT ) ; // tmpHVPS
  comps[ncomps++] = new DataType( BaseType_SHORT ) ; // p12va

  comps[ncomps++] = new DataType( BaseType_SHORT ) ; // m12va
  comps[ncomps++] = new DataType( BaseType_SHORT ) ; // p6va
  comps[ncomps++] = new DataType( BaseType_SHORT ) ; // m6va
  comps[ncomps++] = new DataType( BaseType_SHORT ) ; // p12vd
  comps[ncomps++] = new DataType( BaseType_SHORT ) ; // m12vd

  comps[ncomps++] = new DataType( BaseType_SHORT ) ; // p5vd
  comps[ncomps++] = new DataType( BaseType_SHORT ) ; // prsXe
  comps[ncomps++] = new DataType( BaseType_SHORT ) ; // prsPr
  comps[ncomps++] = new DataType( BaseType_SHORT ) ; // xeHV
  comps[ncomps++] = new DataType( BaseType_SHORT ) ; // prHV

  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // dsR2
  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // dsL2
  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // dsR1
  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // dsL1
  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // dsVp

  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // dsVx
  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // dsR3
  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // dsL3
  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // dither
  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // dsVle

  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // dsAlpha
  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // tpgR3
  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // tpgL3
  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // tpgR2
  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // tpgL2

  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // tpgR1
  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // tpgL1
  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // tpgAlpha
  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // tpgVp
  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // tpgVx

  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // tpgMode
  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // tpgEn
  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // hvXeset
  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // hvPrset
  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // pron

  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // proff
  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // xeon
  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // xeoff
  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // lvon
  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // lvoff

  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // hvPrrly
  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // hvXerly
  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // lvPsrly
  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // hvPron
  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // hvXeon

  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // selB
  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // rateOv
  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // ppsOv
  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // hrmOv
  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // hvinh3

  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // hvinh2
  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // hvinh1
  comps[ncomps++] = new DataType( BaseType_CHAR ) ; // hrmFlag

  RecordDataType * theDataType = new RecordDataType( comps, ncomps ) ;

  // MData is also accessible outside subprogram scope.
  DataValue* mdata = new DataValue( theDataType ) ;

  // setValue() copies pointer
  mdata->setValue( d ) ;

  // All Accessors return MData *
  return mdata ;

}

// ----------------------------------------------
// The following are inherited from RWCollectable
// ----------------------------------------------

unsigned AccPcaHK::binaryStoreSize() const
{
	return
#ifdef SELF_ARCHIVING
  thePacket->recursiveStoreSize() +
#else
  sizeof(unsigned long) + sizeof(int) +
#endif SELF_ARCHIVING
  Accessor::binaryStoreSize() ;
}

int AccPcaHK::compareTo(const RWCollectable* target) const
{
	const AccPcaHK * t = (const AccPcaHK *) target ;
	unsigned long t1 = time() ;
	unsigned long t2 = t->time() ;
	if( t1 == t2 ) return 0 ;
	return t1 > t2 ? 1 : -1 ;
}

RWBoolean AccPcaHK::isEqual(const RWCollectable* target) const
{
	const AccPcaHK * t = (const AccPcaHK *) target ;
	unsigned long t1 = time() ;
	unsigned long t2 = t->time() ;
	return( t1 == t2 ) ? 1 : 0 ;
}

void AccPcaHK::restoreGuts(RWFile& f)
{
  Accessor::restoreGuts(f) ;
#ifdef SELF_ARCHIVING
	thePacket = (PktPca *) recursiveRestoreFrom(f) ;
#else
	f.Read( theTime ) ;
	f.Read( theApid ) ;
#endif SELF_ARCHIVING
}

void AccPcaHK::restoreGuts(RWvistream& s)
{
  Accessor::restoreGuts(s) ;
#ifdef SELF_ARCHIVING
	thePacket = (PktPca *) recursiveRestoreFrom(s) ;
#else
	s >> theTime ;
	s >> theApid ;
#endif SELF_ARCHIVING
}

void AccPcaHK::saveGuts(RWFile& f) const
{
  Accessor::saveGuts(f) ;
#ifdef SELF_ARCHIVING
	thePacket->recursiveSaveOn(f) ;
#else
	f.Write( theTime ) ;
	f.Write( theApid ) ;
#endif SELF_ARCHIVING
}

void AccPcaHK::saveGuts(RWvostream& s) const
{
  Accessor::saveGuts(s) ;
#ifdef SELF_ARCHIVING
	thePacket->recursiveSaveOn(s) ;
#else
	s << theTime ;
	s << theApid ;
#endif SELF_ARCHIVING
}
 
@


1.1
log
@Initial revision
@
text
@d10 1
a10 1
static const char rcsid[]="$Id$";
d19 1
a19 1
#include "SimpleDescriptor.h"
@

head	1.1;
access
	mmarlowe
	soccm;
symbols
	Current:1.1;
locks
	rhee:1.1; strict;
comment	@ * @;


1.1
date	93.06.10.16.11.42;	author rhee;	state Exp;
branches;
next	;


desc
@Creation
@


1.1
log
@Initial revision
@
text
@// Program Name: PktPca.h           
// LastEdit: $Date                                                  
// Subsystem: Data Ingest subsystem

// Programmer:  Hwa-Ja Rhee, Hughes STX (NAASA/GSFC code 664)
// Description: class for PCA housekeeping packets (apId 90 - 94)
//
// RCS: "$Id$"
//
//  Feature test switches
//#define _POSIX_SOURCE 1


#ifndef __PKTPCA_H
#define __PKTPCA_H

//
// The DEC stations need this.
//
#include <sys/types.h>

#include "PktTlm.h"


//
//   PCAPKTS   -- compile in the PCA hierarchy.

//
// Needed by Rogue Wave for storage and retrieval of persistant objects.
// This and the RWDECLARE_COLLECTABLE() macro are the only two constructs
// which are absolutely needed in the .h file of any subclass of PktTlm.
//
#define PktPca_ID 301


class PktPca : public PktTlm  {

RWDECLARE_COLLECTABLE(PktPca)

public:

    PktPca (Exemplar);  // Dummy constructor used for exemplars.

    //
    // This methods assembles the packets into TMData sets.
    // Each subclass should provide an appropriate one of these.
    // The default one here does nothing.
    //
    virtual void apply();

    //
    // This function is used to parse a packet from a data buffer.
    // All subclasses should provide an appropriate one of these.
    // It returns a packet of the appropriate type or subtype.
    //
    virtual  PktTlm* make (unsigned char* buffer);

    ~PktPca (void);

    //
    // Construct a packet to be filled later.  Needed by RogueWave
    // when building an object from disk.  Should not be explicitly
    // used by clients.
    //
    PktPca (void);

    //
    // Constructor to create packet from data buffer. This should
    // not be called directly as all packet building should use the
    // make() method.
    //
    PktPca (unsigned char* buffer);


    //
    // These methods redefine those in PktCCSDS.  We're taking
    // advantage of the fact that CCSDS Telemetry packets have
    // a seconds field.
    //
    int compareTo   (const RWCollectable*)  const;
    unsigned hash   (void)                  const;
    void dumpHeader (ostream& os)           const;
    void dumpData   (ostream& os)           const;

};


#endif
@

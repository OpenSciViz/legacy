head	4.1;
access
	soccm;
symbols
	fixedStandards:4.1
	Build4_3_1:4.0
	Build4_2:4.0
	Buil4_2:4.0
	For_Build4_1:4.0
	E2e:4.0
	Build4:4.0
	Build3_3:3.1
	Build3_2:3.1
	Hardline:3.1
	Current:3.1
	Build3_1:3.1
	Stable3_a:3.1
	Build3:3.1
	Build2:2.1
	Build1:2.1;
locks
	buehler:4.1; strict;
comment	@// @;


4.1
date	95.11.13.21.42.24;	author buehler;	state submitted;
branches;
next	4.0;

4.0
date	94.12.19.22.57.09;	author buehler;	state Submitted;
branches;
next	3.3;

3.3
date	94.12.12.20.48.27;	author buehler;	state Exp;
branches;
next	3.2;

3.2
date	94.11.02.13.34.53;	author buehler;	state Submitted;
branches;
next	3.1;

3.1
date	94.01.03.13.13.22;	author buehler;	state Submitted;
branches;
next	3.0;

3.0
date	93.12.15.21.27.14;	author buehler;	state Exp;
branches;
next	2.1;

2.1
date	93.11.29.21.15.46;	author nessus;	state Release;
branches;
next	2.0;

2.0
date	93.11.29.01.52.40;	author buehler;	state Exp;
branches;
next	1.0;

1.0
date	93.07.08.01.18.50;	author buehler;	state Exp;
branches;
next	;


desc
@@


4.1
log
@Include necessary RW template.
@
text
@//========================================================================
// tEDSPredConfig.C
// RCS Stamp:
static const char rcsid[] = "$Id: tEDSPredConfig.C,v 4.0 1994/12/19 22:57:09 buehler Submitted buehler $";
// Author:    Royce Buehler   - buehler@@space.mit.edu
// Copyright: Massachusetts Institute of Technology
//
// .SECTION Description:
// Test program to exercise protocols in the class EDSPredictedConfig.
//
//========================================================================

#include "EDSPredConfig.h"
#include <rw/tpordvec.h>
#include <iostream.h>
// #include <iomanip.h>
#include <fstream.h>

static void showPC (PredictedConfig* );
static void writeCmds (ostream&, CommandScript* );
static PredictedConfig* getPrior( RWTPtrOrderedVector<PredictedConfig>,
				  int ix);

main()
{
  ofstream out("tEDSCmds.out", ios::out);
  int i, dcCnt, pcCnt;

// Construct the desired configurations we will be using.
  EDSDesiredConfig* terminateAll, *asmHist, *pcaBinned;
  terminateAll = 
    EDSDesiredConfig::fetchDesiredConfig("TerminateAll");
  asmHist = 
    EDSDesiredConfig::fetchDesiredConfig("EDS_PosHist1Only");
  pcaBinned =
    EDSDesiredConfig::fetchDesiredConfig("EDS_4msBinned_EA1Only");

// Construct the sequence of Predicted Configurations we will be using:
// Idle all EAs, run ASM, idle all again, run ASM, run binned in PCA_4,
// and idle all.
  EDSDesiredConfig dConfig[6];
  dConfig[0] = *terminateAll;
  dConfig[1] = *asmHist;
  dConfig[2] = *terminateAll;
  dConfig[3] = *asmHist;
  dConfig[4] = *pcaBinned;
  dConfig[5] = *terminateAll;
  dcCnt = 6;

// Set up predicted timeline.
// EDSPredictedConfigs inherit from their priors upon creation.
  RWTPtrOrderedVector<PredictedConfig> pConfig;
  RWTPtrOrderedVector<CommandScript> cScript;
  PredictedConfig* derivedPtr;
  PredictedConfig* prior;
  pcCnt = 0;

  int edsPcIndex;
  for ( i = 0; i < dcCnt; i++)
  {
    edsPcIndex = pcCnt;
    pConfig.append(dConfig[i].predictConfig());
    prior = getPrior( pConfig, pcCnt++);
    if (prior != 0) {
      pConfig[edsPcIndex]->inheritPrediction(*prior);
    }
    derivedPtr = pConfig[edsPcIndex]->getDerivedConfig();
    while (derivedPtr != 0) {
      pConfig.append(derivedPtr);
      derivedPtr = pConfig[edsPcIndex]->getDerivedConfig();
      prior = getPrior( pConfig, pcCnt);
      if (prior != 0) {
        pConfig[pcCnt]->inheritPrediction(*prior);
      }
      pcCnt++;
    }
  }
  for ( i = 0; i < pcCnt ; i++)
  {
    showPC (pConfig[i]);
  }

  for ( i = 0; i < pcCnt; i++)
  {
    cScript.append(pConfig[i]->getDeltaCommandScript());
    writeCmds (out, cScript[i]);
  }

  out.close();
  return (0);
}

void showPC (PredictedConfig* aPredConfig)
{
  if (aPredConfig->getSubsystemId() != EDS) {
      return;
  }
  EDSPredictedConfig* edsPredConfig = (EDSPredictedConfig*) aPredConfig;
  cout << "Original desired configuration was: " << endl;
  (edsPredConfig->originalDC())->printLong(cout);
  cout << endl;
  cout << "The desired complete state after commanding is: " << endl;
  (edsPredConfig->prediction())->printLong(cout);
}

void writeCmds (ostream& out, CommandScript* aScript)
{
  aScript->print(out);
  int pktNum = 0;
  unsigned entryCnt = aScript->entries();
  const CommandPacket *nextPacket;

  while (pktNum < entryCnt) 
	  
  {
    if  ( (nextPacket = aScript->getCommandPacket(pktNum) ) == 0)
    {
      pktNum++;
      continue;
    }
    out << aScript->getCommandString(pktNum) << endl;
    nextPacket->dumpHeader(out);
    nextPacket->printHex(out);
    out << dec << endl << endl;
    pktNum++;
  }

}

// Scan backwards through the collection of PredictedConfigs, to find
// the most recent PredictedConfig with the same subsystemId as the
// ixth item in the collection. Return a pointer to it.
// If the ixth item is the first in the collection for the subsystem,
// return a null pointer.

static PredictedConfig* getPrior
     ( RWTPtrOrderedVector<PredictedConfig> pConfigs, int ix)
{
  PredictedConfig* prior = (PredictedConfig*) 0;

  SubsystemId subsys = pConfigs[ix]->getSubsystemId();
  for (int pc = ix - 1; pc >=0; pc--) {
    if (pConfigs[pc]->getSubsystemId() == subsys) {
      prior = pConfigs[pc];
      break;
    }
  }
  return (prior);
}
@


4.0
log
@Now does full inheritance and derivation, generates nonempty command
scripts.
@
text
@d4 1
a4 1
static const char rcsid[] = "$Id: tEDSPredConfig.C,v 3.3 1994/12/12 20:48:27 buehler Exp buehler $";
d14 1
@


3.3
log
@Change names of test desired configs to match their current behavior.
@
text
@d4 1
a4 1
static const char rcsid[] = "$Id: tEDSPredConfig.C,v 3.2 1994/11/02 13:34:53 buehler Exp buehler $";
d18 1
a18 1
static void showPC (EDSPredictedConfig& );
d20 2
d26 1
a26 1
  int i;
d40 8
a47 7
  EDSPredictedConfig pConfig[6];
  pConfig[0] = EDSPredictedConfig(terminateAll);
  pConfig[1] = EDSPredictedConfig(asmHist);
  pConfig[2] = EDSPredictedConfig(terminateAll);
  pConfig[3] = EDSPredictedConfig(asmHist);
  pConfig[4] = EDSPredictedConfig(pcaBinned);
  pConfig[5] = EDSPredictedConfig(terminateAll);
d49 7
a55 1
  CommandScript* cScript[6];
d57 2
a58 2
  pConfig[0].inheritPrediction( EDSPredictedConfig(terminateAll));
  for ( i = 1; i < 6; i++)
d60 19
a78 1
    pConfig[i].inheritPrediction(pConfig[i - 1]);
d82 1
a82 1
  for ( i = 0; i < 6; i++)
d84 1
a84 1
    cScript[i] = (pConfig[i].getDeltaCommandScript());
a87 7
  pConfig[4].inheritPrediction (pConfig[2]);
  showPC (pConfig[4]);
  pConfig[4].inheritPrediction (pConfig[2]);
  showPC (pConfig[4]);
  writeCmds (out, pConfig[4].getDeltaCommandScript());
  writeCmds (out, pConfig[4].getDeltaCommandScript());

d92 1
a92 1
void showPC (EDSPredictedConfig& aPredConfig)
d94 4
d99 1
a99 1
  (aPredConfig.originalDC())->printLong(cout);
d102 1
a102 1
  (aPredConfig.prediction())->printLong(cout);
d127 21
@


3.2
log
@Replace NULL for null pointer with 0.
@
text
@d4 1
a4 1
static const char rcsid[] = "$Id: tEDSPredConfig.C,v 3.1 1994/01/03 13:13:22 buehler Exp buehler $";
d29 1
a29 1
    EDSDesiredConfig::fetchDesiredConfig("EDSB_TerminateAll");
d33 1
a33 1
    EDSDesiredConfig::fetchDesiredConfig("EDS_4msBinned_EA6Only");
@


3.1
log
@Made rcsid const to remove warning.
@
text
@d4 1
a4 1
static const char rcsid[] = "$Id: tEDSPredConfig.C,v 3.0 1993/12/15 21:27:14 buehler Exp buehler $";
d91 1
a91 1
    if  ( (nextPacket = aScript->getCommandPacket(pktNum) ) == NULL)
@


3.0
log
@Build 3. No change.
@
text
@d3 2
a4 1
// RCS Stamp: $Id: tEDSPredConfig.C,v 2.1 1993/11/29 21:15:46 nessus Exp buehler $
a11 2

static char rcsid[] = "$Id: tEDSPredConfig.C,v 2.1 1993/11/29 21:15:46 nessus Exp buehler $";
@


2.1
log
@Added 'rcsid' static variable.
@
text
@d3 1
a3 1
// RCS Stamp: $Id: tEDSPredConfig.C,v 2.0 1993/11/29 01:52:40 buehler Exp buehler $
d12 1
a12 1
static char rcsid[] = "$Id$";
@


2.0
log
@Standardized RCS tags in file header.
@
text
@d3 1
a3 1
// RCS Stamp: $Id$
d11 2
@


1.0
log
@Initial revision
@
text
@d3 5
a9 7
// $Header$
// $Author$
// $Date$
/*
 * $Log$
 */
// Revision 1.0 is the XTE Build 1 version.
d12 1
a12 1
#include "EDSPredictedConfig.h"
@

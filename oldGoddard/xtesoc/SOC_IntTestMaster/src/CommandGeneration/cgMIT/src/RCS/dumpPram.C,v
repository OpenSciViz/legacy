head	5.0;
access
	soccm;
symbols;
locks
	buehler:5.0; strict;
comment	@// @;


5.0
date	96.10.30.14.05.34;	author buehler;	state Exp;
branches;
next	;


desc
@Given the name of an edsmemory , produce a stream of
readmem command packets (with 0 seconds delay between them)
that will dump the contents of the edsmemory to telemetry.
@


5.0
log
@Initial version.
@
text
@// dumpPram.C
//   Author      : Royce Buehler
//   Copyright 1996, Massachusetts Institute of Technology
//   RCS:  
static const char rcsid[]= "$Id$"; 
// A standalone utility which takes the name of an EDSMemory object
// and generates an ASCII stream of command packets (with time
// separators of 0) to perform readmems on the entire object.
// The second parameter gives the side, A or B. (In each case,
// the dump will be performed from the ASM EA). Alternatively,
// it may specify a specific EA, using a digit from 0 to 7.

#include <iostream.h>

#include <rw/cstring.h>

#include <EdsMemory.h>

#include <IntelAddress.h>
#include <EDSCmdPkt.h>

static void emitReadMems(IntelAddress& start, int byteCount, int eaNum);

int main (int argc, char** argv)
{
  int whichEA = 0;
  RWCString edsMemName;
  EdsMemoryPtr edsMem = 0;

  if (argc < 3) {
      cerr << "Usage: dumpPram <e> <s>" << endl;
      cerr << "  where <e> is the name of an EDSMemory item in database" 
	   << endl;
      cerr << "  <s> is one of A, B [the PRAM sides], or an EA#, 0-7" 
	   << endl;     
      return 1;
  }
  edsMemName = argv[1];
  
  if (*(argv[2]) == 'A' ||
      *(argv[2]) == 'a') {
      whichEA = 0;
  } else {
      if (*(argv[2]) == 'B' ||
          *(argv[2]) == 'b') {
          whichEA = 4;
      } else {
          whichEA = (int) *(argv[2]); //Convert ASCII digit to integer.
	  whichEA -= 48;
	  if (whichEA < 0 || 
	      whichEA > 7) {
	      cerr << argv[2] << " is not a valid EDS side or EA number."
		   << endl;
	      return 2;
	  }
      }
  }

  if ( ! (edsMem = EdsMemory::restore(edsMemName))) {
      cerr << "Invalid edsmemory name " << edsMemName << endl;
      return 3;
  }

  EdsRamBlockPtr theBlock = 0;
  EdsMemoryRamBlocksIter blockIter(*edsMem);

  for (;  blockIter; ++blockIter) {
      theBlock = *blockIter;
      IntelAddress blockStart = theBlock->baseAddress();
      int bytesInBlock = theBlock->size();
      emitReadMems(blockStart, bytesInBlock, whichEA);
  }

  return 0;
}

void emitReadMems(IntelAddress& start, int byteCount, int eaNum)
{
    const int MAX_DATA_BYTES = 946;
    unsigned char databuff[8];
    UINT16 readLength;
    EDSCmdPktEASerial* nextPacket;

    databuff[0] = databuff[1] = 0;  // Identifies this as a readmem.
    while (byteCount > 0) {
	// Set up data buffer for command packet: 
	// 0 in word 1; Intel address in words 2 and 3; 
	// data length in word 4.
	start.asChars(&(databuff[2]));
	if (byteCount > MAX_DATA_BYTES) {
	    byteCount -= MAX_DATA_BYTES;;
	    readLength = MAX_DATA_BYTES;
	} else {
	    readLength = byteCount;
	    byteCount = 0;
	}
	databuff[6] = (unsigned char) (readLength >> 8);
	databuff[7] = (unsigned char) (readLength & 0x00FF);
	
	nextPacket = new EDSCmdPktEASerial(eaNum, 8, databuff);

	const unsigned char *p = nextPacket->rawData();
	int          cmdLength = nextPacket->packetLength();
// cerr << "Read length " << readLength << ", pktLength " << cmdLength
// << endl;

	for (int ch = 0; ch < 7 + cmdLength; ch++) {
	    cout << *p++;
	}
	cout << (unsigned char) 1  << (unsigned char) 0;   // 1 second wait.
	delete nextPacket;

    }
}

@

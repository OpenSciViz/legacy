head	5.0;
access
	soccm;
symbols
	fixedStandards:5.0
	Build4_3_1:4.0
	Build4_2:4.0
	Buil4_2:4.0
	For_Build4_1:4.0
	E2e:4.0
	Build4:3.4
	Build3_3:3.1
	Build3_2:3.1
	Hardline:3.1
	Current:3.1
	Build3_1:3.1
	Stable3_a:3.1
	Build3:3.1;
locks
	buehler:5.0; strict;
comment	@ * @;


5.0
date	96.04.18.22.40.37;	author buehler;	state submitted;
branches;
next	4.0;

4.0
date	95.02.09.15.56.47;	author buehler;	state Submitted;
branches;
next	3.4;

3.4
date	94.12.12.20.41.03;	author buehler;	state Submitted;
branches;
next	3.3;

3.3
date	94.12.10.21.24.07;	author buehler;	state Exp;
branches;
next	3.2;

3.2
date	94.11.02.13.33.41;	author buehler;	state Submitted;
branches;
next	3.1;

3.1
date	94.01.18.17.54.56;	author buehler;	state Submitted;
branches;
next	3.0;

3.0
date	94.01.17.02.00.26;	author buehler;	state Exp;
branches;
next	;


desc
@A test program to generate command scripts for the EDS,
and do extensive displays of the PredictedConfigs involved,
the scripts, and the command packets.
@


5.0
log
@Complain if there's a hole in the predicted config array.
@
text
@//========================================================================
// edsGenCmd.C
// RCS Stamp:
static const char rcsid[] = "$Id: edsGenCmd.C,v 4.0 1995/02/09 15:56:47 buehler Submitted buehler $";
// Author:    Royce Buehler   - buehler@@space.mit.edu
// Copyright: Massachusetts Institute of Technology
//
// .SECTION Description:
// Test program to delta generate EDS command packets.
// If environment variable TESTMODE is defined, packets are displayed in
// hex. If TESTMODE has the value  "absolute", performs both absolute
// and delta commanding. If TESTMODE has the value "verbose", 
// packet headers will be displayed in an informative long format.
// Expects input in the file edsDc.list on current directory.
// Format of edsDc.list:
//   Number of entries <endline>
//   [Name of an EDSDesiredConfig <endline>]
//========================================================================

#include <stdlib.h>

#include <iostream.h>
#include <fstream.h>

#include <rw/cstring.h>     // RW collectable strings.
#include <rw/tpordvec.h>    // RW Template, ordered vector of pointers.

#include <rom_configs.h>
#include "EDSPredConfig.h"
#include "EaPredConfig.h"
#include "PramPredConf.h"

static void showPC (EDSPredictedConfig& );
static void writeCmds (ostream&, CommandScript* );
int doDisplayScript;

static PredictedConfig* getPrior( RWTPtrOrderedVector<PredictedConfig>,
				  int ix);

static int isTestMode;
static int isVerbose;
static int absoluteCommanding;

main()
{
  int i;
  int dcCnt, pcCnt = 0;
  RWCString dcName;
  doDisplayScript = 1;  // Later get these three from environment variable.
  absoluteCommanding = 0;
  isVerbose = 0;

// Determine whether we're in test mode. (TESTMODE environment variable
// is defined and not the null string.)
  char *envValue = getenv("TESTMODE");
  if (envValue == 0) {
    isTestMode = 0;
  } else {
    RWCString envVal( envValue);
    if (envVal == "") {
      isTestMode = 0;
    } else {
      isTestMode = 1;
      if (envVal == "verbose") {
	  isVerbose = 1;
      }
      if (envVal == "absolute") {
	  absoluteCommanding = 1;
      }
    }
  }

  ifstream inf("edsDc.list", ios::in);
  
// Set up desired timeline.
  if (! inf.good() ) {
    cerr << "Couldn't open input file edsDc.list. Aborting." << endl;
    return (0);
  }

  inf >> dcCnt;
  EDSDesiredConfig* dConfig = new EDSDesiredConfig[dcCnt];
  EDSDesiredConfig* fetched;
  for (i = 0; i < dcCnt; i++) {
    inf >> dcName;
    if (! inf.good() ) {
      cerr << "Error reading " << i << "th name from edsDc.list." << endl;
      if (i) {          // Did read some successfully?
	dcCnt = i - 1;
      } else {
	return (0);
      }
    }
    fetched = EDSDesiredConfig::fetchDesiredConfig( dcName);
    dConfig[i] = *fetched;
    delete fetched;
  }

 // Construct the desired configurations we will be using.
  EDSDesiredConfig* terminateAll;
  terminateAll = 
    EDSDesiredConfig::fetchDesiredConfig("EDSB_TerminateAll");

// Set up predicted timeline.
// EDSPredictedConfigs inherit from their priors upon creation.
  RWTPtrOrderedVector<PredictedConfig> pConfig;
  RWTPtrOrderedVector<CommandScript> cScript;
  PredictedConfig* derivedPtr;
  PredictedConfig* prior;
  pcCnt = 0;

  int edsPcIndex;
  for ( i = 0; i < dcCnt; i++)
  {
    edsPcIndex = pcCnt;
    pConfig.insert(dConfig[i].predictConfig());
    prior = getPrior( pConfig, pcCnt++);
    if (prior != 0) {
      pConfig[edsPcIndex]->inheritPrediction(*prior);
    }
    derivedPtr = pConfig[edsPcIndex]->getDerivedConfig();
    while (derivedPtr != 0) {
      pConfig.insert(derivedPtr);
      pcCnt++;
      derivedPtr = pConfig[edsPcIndex]->getDerivedConfig();
    }
  }


// Produce absolute commands from predicted timeline.
  if (isTestMode && absoluteCommanding) {
      cout << endl;
      cout << "Generating absolute commands." << endl;
      for ( i = 0; i < pcCnt; i++)
      {
	  cScript.insert(pConfig[i]->getCommandScript());
	  writeCmds (cout, cScript[i]);
      }
  }

  if (isTestMode) {
    cout << endl;
    cout << "Generating delta commands." << endl;
  }

// Produce delta commands from predicted timeline.
  pConfig[0]->inheritPrediction( EDSPredictedConfig(terminateAll));
  for (i = 1; i < pcCnt; i++) {
    prior = getPrior( pConfig, i);
    if (prior != 0) {
      pConfig[i]->inheritPrediction(*prior);
    }
    if (isTestMode) {
	if (pConfig[i] == 0) {
	    cout << "Predicted config #" << i << " missing." << endl;
	} else {
	    pConfig[i]->printLong( cout);
	}
    }
  }

  cScript.clearAndDestroy();
  for ( i = 0; i < pcCnt; i++)
  {
    cScript.insert(pConfig[i]->getDeltaCommandScript());
    writeCmds (cout, cScript[i]);
  }

//  out.close();
  return (0);
}

void showPC (EDSPredictedConfig& aPredConfig)
{
  cout << "Original desired configuration was: " << endl;
  (aPredConfig.originalDC())->print(cout);
  cout << endl;
  cout << "The desired complete state after commanding is: " << endl;
  (aPredConfig.prediction())->printLong(cout);
}

void writeCmds (ostream& out, CommandScript* aScript)
{
  if (isTestMode) {
    aScript->print(out);
  }
  int pktNum = 0;
  unsigned entryCnt = aScript->entries();
  const CommandPacket *nextPacket;

  int lastDelay = 0;
  int thisDelay;
  while (pktNum < entryCnt) {
    if  ( (nextPacket = aScript->getCommandPacket(pktNum) ) == 0) {
      pktNum++;
      continue;
    }
    if (isTestMode) {
      out << aScript->getCommandString(pktNum) << endl;
      if (isVerbose) {
	  nextPacket->dumpHeader(out);
      }
      nextPacket->printHex(out);
      out << dec << endl << endl;
    } else {
      nextPacket->dump(out);
      thisDelay = aScript->getDelaySeconds(pktNum);
//      out << (UINT16) (thisDelay - lastDelay);
      lastDelay = thisDelay;
//      out << (UINT16) 0;
// TBI? Might be useful to output actual seconds here.
    }
    pktNum++;
  }
  out << flush;
}

// Scan backwards through the collection of PredictedConfigs, to find
// the most recent PredictedConfig with the same subsystemId as the
// ixth item in the collection. Return a pointer to it.
// If the ixth item is the first in the collection for the subsystem,
// return a predicted config full of dont-cares.

static PredictedConfig* getPrior
     ( RWTPtrOrderedVector<PredictedConfig> pConfigs, int ix)
{
  EDSSide theSide;
  eventAnalyzer theEa;
  PredictedConfig* prior = (PredictedConfig*) 0;

  SubsystemId subsys = pConfigs[ix]->getSubsystemId();

  // Set up dont-knows in default version of "prior".
  switch (subsys) {
  case PRAMA:
    theSide = EDS_SIDEA;
    break;
  case PRAMB:
    theSide = EDS_SIDEB;
    break;
  case EA1:
    theSide = EDS_SIDEA;
    theEa = EA_ASMA;
    break;
  case EA5:
    theSide = EDS_SIDEB;
    theEa = EA_ASMB;
    break;
  case EA2:
    theSide = EDS_SIDEA;
    theEa = EA_PCA1;
    break;
  case EA3:
    theSide = EDS_SIDEA;
    theEa = EA_PCA2;
    break;
  case EA4:
    theSide = EDS_SIDEA;
    theEa = EA_PCA3;
    break;
  case EA6:
    theSide = EDS_SIDEB;
    theEa = EA_PCA4;
    break;
  case EA7:
    theSide = EDS_SIDEB;
    theEa = EA_PCA5;
    break;
  case EA8:
    theSide = EDS_SIDEB;
    theEa = EA_PCA6;
    break;
  }

  EaDesired defaultEaDesired;
  switch (subsys) {
  case EDS:
    break;
  case PRAMA:
  case PRAMB:
    prior = new PramPredictedConfig();
    break;
  case EA1:
  case EA5:
    defaultEaDesired.setSciConfigName(eaAnyAsm);
    prior = new EaPredictedConfig(theEa, 
				  defaultEaDesired, 
				  (PramPredictedConfig*) 0);
    break;
  default:
    defaultEaDesired.setSciConfigName(eaAny);
    prior = new EaPredictedConfig(theEa, 
				  defaultEaDesired,
				  (PramPredictedConfig*) 0);
  };

  for (int pc = ix - 1; pc >=0; pc--) {
    if (pConfigs[pc]->getSubsystemId() == subsys) {
      prior = pConfigs[pc];
      break;
    }
  }
  return (prior);
}





@


4.0
log
@Correct bug enforcing 64-configuration limit.
Add "absolute" and "verbose" options on environment variable TESTMODE.
@
text
@d4 1
a4 1
static const char rcsid[] = "$Id: edsGenCmd.C,v 3.4 1994/12/12 20:41:03 buehler Submitted buehler $";
d154 5
a158 1
      pConfig[i]->printLong( cout);
@


3.4
log
@Drop for now the attempt to output time delays.
@
text
@d4 1
a4 1
static const char rcsid[] = "$Id: edsGenCmd.C,v 3.3 1994/12/10 21:24:07 buehler Exp buehler $";
d10 4
a13 3
// If environment variable TESTMODE is defined, displays additional
// information about predictedConfigs and CommandScripts, and also
// does absolute commanding.
d15 1
a15 1
// Format:
d28 1
d30 2
d41 2
d49 3
d64 6
a72 1
//  ofstream out;
a74 2
  doDisplayScript = 1;  // Later get from environment variable.

a102 2
//  asmHist = 
//    EDSDesiredConfig::fetchDesiredConfig("EDS_PosHist1Only");
d116 1
a116 1
    pConfig[pcCnt] = dConfig[i].predictConfig();
a119 3
	if (edsPcIndex == 14) {        // Temporary debug check
	   ((EDSPredictedConfig*) pConfig[edsPcIndex])->prediction();
        }
d123 2
a124 1
      pConfig[pcCnt++] = derivedPtr;
a127 16
/*
// Have Pram and EaPredictedConfigs inherit from their priors.
// Display state of all PredictedConfigs after they've inherited.
  for ( i = 0; i < pcCnt; i++) {
    if ( pConfig[i]->getSubsystemId() != EDS) {
      prior = getPrior( pConfig, i);
      if (prior != 0) {
	pConfig[i]->inheritPrediction( *prior);
      }
    }
  }
*/
  if (isTestMode) {
    cout << endl;
    cout << "Generating commands." << endl;
  }
d129 1
d131 8
a138 5
  if (isTestMode) {
  for ( i = 0; i < pcCnt; i++)
  {
    cScript[i] = pConfig[i]->getCommandScript();
    writeCmds (cout, cScript[i]);
a139 1
  }
d158 1
d161 1
a161 1
    cScript[i] = (pConfig[i]->getDeltaCommandScript());
d196 3
a198 1
      nextPacket->dumpHeader(out);
d218 1
a218 1
// return a null pointer.
d223 2
d228 65
d301 1
@


3.3
log
@Comment out absolute commanding in test mode.
@
text
@d4 1
a4 1
static const char rcsid[] = "$Id: edsGenCmd.C,v 3.2 1994/11/02 13:33:41 buehler Exp buehler $";
d206 1
a206 1
      out << (UINT16) (thisDelay - lastDelay);
@


3.2
log
@Replace NULL for null pointers with 0.
@
text
@d4 1
a4 1
static const char rcsid[] = "$Id: edsGenCmd.C,v 3.1 1994/01/18 17:54:56 buehler Exp buehler $";
d120 1
a120 1

a129 3
    if (isTestMode) {
      pConfig[i]->printLong( cout);
    }
d131 1
a131 1

d158 3
d191 2
d205 5
a209 1
      out << (UINT8) 0;
@


3.1
log
@Default behavior now issues command packet binary only;
Define env var TESTMODE to get human readable display.
@
text
@d4 1
a4 1
static const char rcsid[] = "$Id: edsGenCmd.C,v 3.0 1994/01/17 02:00:26 buehler Exp buehler $";
d47 1
a47 1
  if (envValue == NULL) {
d192 1
a192 1
    if  ( (nextPacket = aScript->getCommandPacket(pktNum) ) == NULL) {
@


3.0
log
@Initial version. Considerable overdisplay.
@
text
@d4 1
a4 1
static const char rcsid[] = "$Id$";
d9 8
a16 2
// Test program to generate EDS command packets.
//
d19 2
d36 2
d43 15
d130 3
a132 1
    pConfig[i]->printLong( cout);
d135 5
d141 1
d147 1
d149 4
a152 2
  cout << endl;
  cout << "Generating delta commands." << endl;
d184 1
a184 1
  if (doDisplayScript) {
d191 2
a192 5
  while (pktNum < entryCnt) 
	  
  {
    if  ( (nextPacket = aScript->getCommandPacket(pktNum) ) == NULL)
    {
d196 9
a204 4
    out << aScript->getCommandString(pktNum) << endl;
    nextPacket->dumpHeader(out);
    nextPacket->printHex(out);
    out << dec << endl << endl;
d207 1
a207 1

d230 4
@

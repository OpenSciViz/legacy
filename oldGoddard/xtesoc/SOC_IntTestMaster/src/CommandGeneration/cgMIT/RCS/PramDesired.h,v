head	4.2;
access
	soccm;
symbols
	fixedStandards:4.2
	Build4_3_1:4.0
	Build4_2:4.0
	Buil4_2:4.0
	For_Build4_1:3.4
	E2e:3.4
	Build4:3.4;
locks
	buehler:4.2; strict;
comment	@ * @;


4.2
date	96.04.18.22.19.00;	author buehler;	state submitted;
branches;
next	4.1;

4.1
date	95.06.08.15.49.44;	author buehler;	state Exp;
branches;
next	4.0;

4.0
date	95.03.11.20.24.45;	author buehler;	state Submitted;
branches;
next	3.4;

3.4
date	94.12.17.17.16.23;	author buehler;	state Exp;
branches;
next	3.3;

3.3
date	94.12.10.21.07.09;	author buehler;	state Exp;
branches;
next	3.2;

3.2
date	94.11.30.02.09.51;	author buehler;	state Submitted;
branches;
next	3.1;

3.1
date	94.11.25.17.57.08;	author buehler;	state Exp;
branches;
next	3.0;

3.0
date	94.11.02.17.08.19;	author buehler;	state Exp;
branches;
next	;


desc
@Desired configuration state for an EDS system manager.
@


4.2
log
@Update revision history comments.
@
text
@//==========================================================================
// PramDesired.h
//   RCS: $Id: PramDesired.h,v 4.1 1995/06/08 15:49:44 buehler Exp buehler $ 
//   Copyright 1994, Massachusetts Institute of Technology

// .NAME PramDesired.h
// .LIBRARY libcgeds
// .HEADER Desired config for one PRAM
// .INCLUDE PramDesired.h
// .FILE  ../src/PramDesired.C
// .SECTION Author
// Royce Buehler, buehler, MIT, buehler@@mit.edu
// .SECTION Description
// A PramDesired object contains information about the desired configuration
// for a single EDS system manager.
// It is intended that the only clients for PramDesired be the EDSDesiredConfig
// and PramPredConfig classes. The data elements in PramDesired are separated
// out to make code in those classes more compact and readable, not for
// the sake of encapsulation. This class's existence is an implementation
// detail; from the design point of view, all that exists are data elements
// within those two classes; which is why the members of PramDesired are 
// currently  public. 
//
// REVISION HISTORY
// 4.1,4.2   6/08/95 reb Eliminate unneeded includes.
//==========================================================================

#ifndef PRAMDESIRED_H
#define PRAMDESIRED_H
#include <rw/collect.h>

#include <EdsMemory.h>    // Memory map class, including PRAM, ROM.

#include "mit_instrum.h"  // Constants and enums describing EDS.
#include "mitCgEnums.h"   // MIT enums for command generation.

enum SscHighVoltage {SSC_HI_V_OFF, SSC_HI_V_ON, SSC_HI_V_DONT_CARE,
                     SSC_HI_V_OTHER_SIDE};
enum AsmDriveStatus {ASM_DRV_OFF, ASM_DRV_ON, ASM_DRV_DONT_CARE,
                     ASM_DRV_OTHER_SIDE};

class RWvistream;
class RWvostream;
class RWFile;
class PramDesired {
public:
  PramDesired(void);
  PramDesired(const PramDesired& other);
  // Assignment operator.
  PramDesired& operator=(const PramDesired& other);

 //% The following five methods support persistence in objects 
 //% containing a PramDesired.
  RWspace  binaryStoreSize(void) const;
  void restoreFrom( RWFile& inf);
  void restoreFrom( RWvistream& istrm);
  void saveOn( RWFile& outf) const;
  void saveOn( RWvostream& ostrm) const;

 //% Public methods
  // Test for equality of values.
  RWBoolean operator==( PramDesired& other) const;

  RWBoolean operator!=( PramDesired& other) const
    { return ( !(*this == other)); };

  void setPramVersionName( const RWCString& aName);
  const RWCString getPramVersionName(void) const;
  EdsMemoryPtr getContents(void) const;
  EdsMemoryPtr getContents(enum EDSSide aSide) const;
  SscHighVoltage getSscHigh(int n) const;
  void setSscHigh(int n, SscHighVoltage setting);
  AsmDriveStatus getAsmDriveStatus(void) const;
  void setAsmDriveStatus( AsmDriveStatus aStatus);
  static EDSSide asmCommandingSide(void) { return EDS_SIDEB;};

 //% Data members
  pramValidity state;
  RWCString pramVersionName;
private:
  void copyGuts(const PramDesired& other);
  SscHighVoltage sscHigh[3];
  AsmDriveStatus asmDrivePowered;
  EdsMemoryPtr contents;
};

//# Returns name of the PRAM version (an EDSMemory object). 
inline const RWCString PramDesired:: 
getPramVersionName(void) const
{
  return( pramVersionName);
}

#endif
@


4.1
log
@Drop unneeded includes; use forward declarations on some others.
@
text
@d3 1
a3 1
//   RCS: $Id: PramDesired.h,v 4.0 1995/03/11 20:24:45 buehler Exp buehler $ 
d23 3
@


4.0
log
@Add static function to designate the side of EDS doing ASM commanding.
@
text
@d3 1
a3 1
//   RCS: $Id: PramDesired.h,v 3.4 1994/12/17 17:16:23 buehler Submitted buehler $ 
a27 2
#include <rw/rwfile.h>
#include <rw/vstream.h>
a28 1
#include <PredictedConfig.h>
d39 3
@


3.4
log
@Add the form of getContents() which takes an EDSSide as a parameter.
@
text
@d3 1
a3 1
//   RCS: $Id: PramDesired.h,v 3.3 1994/12/10 21:07:09 buehler Exp buehler $ 
d72 1
@


3.3
log
@Provide explicit assignment operator.
@
text
@d3 1
a3 1
//   RCS: $Id: PramDesired.h,v 3.2 1994/11/30 02:09:51 buehler Exp buehler $ 
d67 1
@


3.2
log
@Include SscHighVoltage and AsmDriveStatus;
transfer setPramVersionName to .C file.
@
text
@d3 1
a3 1
//   RCS: $Id: PramDesired.h,v 3.1 1994/11/25 17:57:08 buehler Exp buehler $ 
d45 4
d76 1
@


3.1
log
@Include EDSMemory.
@
text
@d3 1
a3 1
//   RCS: $Id: PramDesired.h,v 3.0 1994/11/02 17:08:19 buehler Exp buehler $ 
d32 1
a33 1
 #include <EdsMemory.h>    // Memory map class, including PRAM, ROM.
d37 5
d62 5
a66 1
  EdsMemory* getContents(void);
d72 3
a74 1
  EdsMemory* contents;
a75 8

//# Sets PRAM version name to aName, and initializes EDSMemory ptr accordingly.
inline void PramDesired:: 
setPramVersionName( const RWCString& aName)
{
  pramVersionName = aName;
  contents = EdsMemory::restore(pramVersionName);
}
@


3.0
log
@Initial version.
@
text
@d3 1
a3 1
//   RCS: $Id$ 
d33 1
a33 2
// #include <EdsMemory.h>    // Memory map class, including PRAM, ROM.
#include "EDSDesConfig.h"
@

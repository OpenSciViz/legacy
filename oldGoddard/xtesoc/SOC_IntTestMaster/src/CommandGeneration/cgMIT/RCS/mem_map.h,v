head	1.10;
access
	soccm;
symbols
	fixedStandards:1.10
	Build4_3_1:1.10
	Build4_2:1.10
	Buil4_2:1.10
	For_Build4_1:1.10
	E2e:1.10
	Build4:1.10
	Build3_3:1.10
	Build3_2:1.10
	Hardline:1.10
	Current:1.10
	Build3_1:1.10
	Stable3_a:1.10
	Build3:1.10
	Build2:1.10
	Build1:1.10;
locks
	buehler:1.10; strict;
comment	@*   @;


1.10
date	93.04.21.13.21.02;	author jimf;	state submitted;
branches;
next	1.9;

1.9
date	93.04.01.21.15.15;	author jimf;	state Exp;
branches;
next	1.8;

1.8
date	93.02.24.17.08.35;	author jimf;	state Exp;
branches;
next	1.7;

1.7
date	93.01.26.20.00.36;	author jimf;	state Exp;
branches;
next	1.6;

1.6
date	93.01.05.03.36.55;	author jimf;	state Exp;
branches;
next	1.5;

1.5
date	92.12.31.15.25.05;	author jimf;	state Exp;
branches;
next	1.4;

1.4
date	92.12.31.14.54.45;	author jimf;	state Exp;
branches;
next	1.3;

1.3
date	92.11.09.16.07.03;	author jimf;	state Exp;
branches;
next	1.2;

1.2
date	92.09.18.13.19.44;	author jimf;	state Exp;
branches;
next	1.1;

1.1
date	92.09.18.13.13.49;	author jimf;	state Exp;
branches;
next	;


desc
@Flight Software Memory Map Include File
@


1.10
log
@Added Typer and Recoder Table Addresses and Sizes.
@
text
@/*===========================================================================
* 
* PRELIMINARY
*
*  $Source: /snebulos/h1/jimf/xtelocal/SrcPDL/New/RCS/mem_map.h,v $
*
*       MODULE NAME: Flight Software Memory Map Include File (FS)
* 
*       PURPOSE: Provides Location of all Hardware Registers
* 
*       ASSUMPTIONS: NONE
*       
*       REFERENCES:
*       1. "XTE EDS Hardware Functional Descriptions and Requrements"
*           MIT 64-02006.01 Rev. C.
* 
*       SOURCE IDENTIFICATION:          MIT 64-3xxxx.xx Rev. xx
* 
*  $Log: mem_map.h,v $
*   Revision 1.9  1993/04/01  21:15:15  jimf
*   Added MMAP_DSP_FLAT_START and MMAP_DSP_FLAT_END as delimiters for valid
*   DSP addresses mapped into the 80286 address space.
*
*   Revision 1.8  1993/02/24  17:08:35  jimf
*   Modified ASM Command/Housekeeping Register Addresses to reflect Rev. C
*   hardware design.
*
*   Revision 1.7  1993/01/26  20:00:36  jimf
*   Added some 286 RAM Memory map definitions.
*
*   Revision 1.6  1993/01/05  03:36:55  jimf
*   Checkpoint. Diddled with DSP addresses. Added leading zeros.
*
*   Revision 1.5  1992/12/31  15:25:05  jimf
*   Added leading zero to MMAP_MAX_286_ADDR.
*
*   Revision 1.4  1992/12/31  14:54:45  jimf
*   Prefixed all definitions with "MMAP_"
*
*   Revision 1.3  1992/11/09  16:07:03  jimf
*   Moved INTR_IVEC_START to intr.h.
*   Eliminated INTR_TVEC_START.
*
*  Revision 1.2  1992/09/18  13:19:44  jimf
*  Added PRELIMINARY keyword.
*
 * Revision 1.1  1992/09/18  13:13:49  jimf
 * Initial revision
 *
* 
===========================================================================*/

#ifndef MEM_MAP_H
#define MEM_MAP_H

/* ---- All Addresses are in segment:offset notation unless 
   explicitly specified otherwise
---- */

/* ---- Max. CPU Addressable Location (flat address) ---- */
#define MMAP_MAX_286_ADDR	   (0x0fffffL)

/* ---- 286 CPU Trap Table address ---- */
#define MMAP_CPU_TRAP_TBL_ADDR 	   (0x00000000L)

/* ---- 286 CPU Trap Table size in bytes ---- */
#define MMAP_CPU_TRAP_TBL_SIZE 	   (0x0400)

/* ---- 82C59 Programmable Interrupt Controller address ---- */
#define MMAP_INTR_ADDR		   (0x0e0000000L)

/* ---- 82C73A DMA Controller address ---- */
#define MMAP_DMA_ADDR 		   (0x0c0000000L)

/* ---- Min/Max transfer addresses accessable by DMA Controller (flat) ---- */
#define MMAP_DMA_MIN_TRANSFER_ADDR (0x00000L)
#define MMAP_DMA_MAX_TRANSFER_ADDR (0x01fffeL)

/* ---- CPU Register Addresses ---- */
#define MMAP_CPU_CNTL_REG      	(0x090000000L) /* Control Register (16-bit) */
#define MMAP_CPU_STAT_REG      	(0x0a0000000L) /* Status Register (16-bit) */
#define MMAP_CPU_PULSE_REG    	(0x0a0000000L) /* Pulse Register (16-bit) */
#define MMAP_MET_REG           	(0x080000000L) /* MET Register (32-bit) */

/* ---- Observation Register Addresses ---- */
#define MMAP_OBS_CNTL_REG        (0x050000008L) /* Control Register (16-bit) */
#define MMAP_OBS_STAT_REG        (0x05000000cL) /* Status Register (16-bit) */
#define MMAP_OBS_PULSE_REG       (0x05000000cL) /* Pulse Register (16-bit) */
#define MMAP_BIN_CLK_COUNT_REG   (0x050000004L) /* BinClk Duration (32-bit) */
#define MMAP_INTERVAL_COUNT_REG  (0x050000000L) /* Intvl Duration (32-bit) */
#define MMAP_ASM_CMD_REG         (0x090000002L) /* ASM Command (16-bit) */
#define MMAP_ASM_HOUSEKEEPING_REG (0x090000002L) /* ASM House (16-bit) */

/* ---- Front End Lookup Table Addresses (PCA-EA's Only) ---- */
#define MMAP_TYPER_TABLE         (0x040000000L)	/* Typer Lookup Table */
#define MMAP_TYPER_TABLE_SIZE    (0x08000)	/* Typer Tbl Size (in bytes) */
#define MMAP_RECODER_TABLE       (0x048000000L)	/* Recoder Lookup Table */
#define MMAP_RECODER_TABLE_SIZE  (0x08000)	/* Recoder Size (in bytes) */

/* ---- DSP Addresses Memory-Mapped on the 80286 ---- */
#define MMAP_DSP_SCRATCH_CODE    (0x060000000L)	/* DSP External Program RAM */
#define MMAP_DSP_SCRATCH_DATA    (0x068000000L)	/* DSP External Data RAM */
#define MMAP_DSP_PARTITION_0     (0x070000000L)	/* DSP Partition 0 */
#define MMAP_DSP_PARTITION_1     (0x078000000L)	/* DSP Partition 1 */
#define MMAP_DSP_BLOCK_SIZE      (0x08000)	/* Size of above (in bytes) */

#define MMAP_DSP_FLAT_START      (0x060000L)	/* Start of Shared DSP Mem */
#define MMAP_DSP_FLAT_END        (0x07fffeL)	/* End of Shared DSP Mem */

/* ---- 80286 RAM Memory Map ---- */
#define MMAP_ROM_COPY         (0x000000000L)	/* Start of copy of ROM */
#define MMAP_ROM_COPY_SIZE    (0x020000L)	/* Size of ROM in bytes */
#define MMAP_PRAM_COPY	      (0x020000000L)	/* Start of copy of PRAM */
#define MMAP_PRAM_COPY_SIZE   (0x010000L)	/* Size of PRAM in bytes */

#define MMAP_SCIENCE_BUFFER_0    (0x030000000L)	/* 1st 32K of Science Buf */
#define MMAP_SCIENCE_BUFFER_1	 (0x038000000L)	/* 2nd 32K of Science Buf */
#define MMAP_SCIENCE_BUF_SIZE    (0x08000)      /* Size of bufs in bytes */


/* ---- ROM Monitor related stuff ---- */
#ifdef ROM_MONITOR
#define MMAP_ROM_TRAP 	(0x40)	/* ROM Trap Number */
#define MMAP_BRKPT_TRAP (0x03)	/* Breakpoint Trap Number */
#define MMAP_TRACE_TRAP (0x01)	/* Trace (single-step) Trap Number */
#endif /* ROM_MONITOR */


#endif /* MEM_MAP_H */
@


1.9
log
@Added MMAP_DSP_FLAT_START and MMAP_DSP_FLAT_END as delimiters for valid
DSP addresses mapped into the 80286 address space.
@
text
@d5 1
a5 1
*  $Source: /snebulos/h1/jimf/xtelocal/SrcPDL/unit/RCS/mem_map.h,v $
d20 4
d93 6
@


1.8
log
@Modified ASM Command/Housekeeping Register Addresses to reflect Rev. C
hardware design.
@
text
@d5 1
a5 1
*  $Source: /snebulos/h1/jimf/flight/New/src_rcs/mem_map.h,v $
d20 4
d96 3
@


1.7
log
@Added some 286 RAM Memory map definitions.
@
text
@d15 1
a15 1
*           MIT 64-02006.01 Rev. B.
d20 3
d83 2
a84 2
#define MMAP_ASM_CMD_REG         (0x040000000L) /* ASM Command (16-bit) */
#define MMAP_ASM_HOUSEKEEPING_REG (0x040000000L) /* ASM House (16-bit) */
@


1.6
log
@Checkpoint. Diddled with DSP addresses. Added leading zeros.
@
text
@d5 1
a5 1
*  $Source: /snebulos/h1/jimf/xtelocal/SrcPDL/New/RCS/mem_map.h,v $
d20 3
d89 11
@


1.5
log
@Added leading zero to MMAP_MAX_286_ADDR.
@
text
@d5 1
a5 1
*  $Source: /snebulos/h1/jimf/flight/New/src_rcs/mem_map.h,v $
d20 3
d56 1
a56 1
#define MMAP_INTR_ADDR		   (0xe0000000L)
d59 1
a59 1
#define MMAP_DMA_ADDR 		   (0xc0000000L)
d63 1
a63 1
#define MMAP_DMA_MAX_TRANSFER_ADDR (0x1fffeL)
d66 4
a69 4
#define MMAP_CPU_CNTL_REG      	(0x90000000L) /* Control Register (16-bit) */
#define MMAP_CPU_STAT_REG      	(0xa0000000L) /* Status Register (16-bit) */
#define MMAP_CPU_PULSE_REG    	(0xa0000000L) /* Pulse Register (16-bit) */
#define MMAP_MET_REG           	(0x80000000L) /* MET Register (32-bit) */
d72 14
a85 7
#define MMAP_OBS_CNTL_REG         (0x50000008L) /* Control Register (16-bit) */
#define MMAP_OBS_STAT_REG         (0x5000000cL) /* Status Register (16-bit) */
#define MMAP_OBS_PULSE_REG        (0x5000000cL) /* Pulse Register (16-bit) */
#define MMAP_BIN_CLK_COUNT_REG    (0x50000004L) /* BinClk Duration (32-bit) */
#define MMAP_INTERVAL_COUNT_REG   (0x50000000L) /* Intvl Duration (32-bit) */
#define MMAP_ASM_CMD_REG          (0x40000000L) /* ASM Command (16-bit) */
#define MMAP_ASM_HOUSEKEEPING_REG (0x40000000L) /* ASM Housekeeping (16-bit) */
@


1.4
log
@Prefixed all definitions with "MMAP_"
@
text
@d20 3
d44 1
a44 1
#define MMAP_MAX_286_ADDR	   (0xfffff)
@


1.3
log
@Moved INTR_IVEC_START to intr.h.
Eliminated INTR_TVEC_START.
@
text
@d5 1
a5 1
*  $Source: /snebulos/h1/jimf/xtelocal/SrcPDL/src_rcs/mem_map.h,v $
d20 4
d41 1
a41 1
#define MAX_286_MEM_ADDR	(0xfffff)
d44 1
a44 1
#define CPU_TRAP_TBL_ADDR 	(0x00000000L)
d47 1
a47 1
#define CPU_TRAP_TBL_SIZE 	(0x0400)
d50 1
a50 1
#define INTR_ADDR		(0xe0000000L)
d53 1
a53 4
#define DMA_ADDR 		(0xc0000000L)

/* ---- Minimum transfer address accessable by DMA Controller (flat) ---- */
#define DMA_MIN_TRANSFER_ADDR	(0x00000L)
d55 3
a57 2
/* ---- Maximum transfer address accessable by DMA Controller (flat) ---- */
#define DMA_MAX_TRANSFER_ADDR 	(0x1fffeL)
d60 4
a63 4
#define CPU_CNTL_REG      	(0x90000000L) /* Control Register (16-bit) */
#define CPU_STAT_REG      	(0xa0000000L) /* Status Register (16-bit) */
#define CPU_PULSE_REG    	(0xa0000000L) /* Pulse Register (16-bit) */
#define MET_REG           	(0x80000000L) /* MET Register (32-bit) */
d66 7
a72 7
#define OBS_CNTL_REG         	(0x50000008L) /* Control Register (16-bit) */
#define OBS_STAT_REG         	(0x5000000cL) /* Status Register (16-bit) */
#define OBS_PULSE_REG           (0x5000000cL) /* Pulse Register (16-bit) */
#define BIN_CLK_COUNT_REG    	(0x50000004L) /* BinClk Duration (32-bit) */
#define INTERVAL_COUNT_REG   	(0x50000000L) /* Intvl Duration (32-bit) */
#define ASM_CMD_REG          	(0x40000000L) /* ASM Command (16-bit) */
#define ASM_HOUSEKEEPING_REG 	(0x40000000L) /* ASM Housekeeping (16-bit) */
d76 3
a78 3
#define ROM_TRAP 	(0x40)	/* ROM Trap Number */
#define BRKPT_TRAP 	(0x03)	/* Breakpoint Trap Number */
#define TRACE_TRAP 	(0x01)	/* Trace (single-step) Trap Number */
@


1.2
log
@Added PRELIMINARY keyword.
@
text
@d5 1
a5 1
*  $Source: /snebulos/h1/jimf/xtelocal/SrcPDL/src/RCS/mem_map.h,v $
d20 3
d43 1
a43 1
#define CPU_TRAP_TBL_SIZE 	(0x400)
a49 6

/* ---- Number of the vector starting the CPU Trap Table ---- */
#define INTR_TVEC_START 	(0)

/* ---- Number of the vector starting Maskable Interrupt Vector Table ---- */
#define INTR_IVEC_START 	(0x020)
@


1.1
log
@Initial revision
@
text
@d3 1
a3 1
*  $Source$
d5 2
d19 4
a22 1
*  $Log$
@

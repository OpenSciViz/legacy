head	4.2;
access
	soccm;
symbols
	fixedStandards:4.2
	Build4_3_1:4.1
	Build4_2:4.0
	Buil4_2:4.0
	For_Build4_1:3.10
	E2e:3.10
	Build4:3.10
	Build3_3:3.7
	Build3_2:3.6
	Hardline:3.2
	Current:3.2
	Build3_1:3.2
	Stable3_a:3.2
	Build3:3.2
	Build2:2.1
	Build1:2.1;
locks
	buehler:4.2; strict;
comment	@ * @;


4.2
date	95.06.08.16.14.09;	author buehler;	state submitted;
branches;
next	4.1;

4.1
date	95.05.09.16.18.08;	author buehler;	state Submitted;
branches;
next	4.0;

4.0
date	95.03.11.20.25.39;	author buehler;	state Submitted;
branches;
next	3.10;

3.10
date	94.11.30.02.11.13;	author buehler;	state Exp;
branches;
next	3.9;

3.9
date	94.11.02.17.08.03;	author buehler;	state Exp;
branches;
next	3.8;

3.8
date	94.10.24.17.58.33;	author buehler;	state Exp;
branches;
next	3.7;

3.7
date	94.10.13.16.18.56;	author buehler;	state Submitted;
branches;
next	3.6;

3.6
date	94.08.12.19.22.59;	author buehler;	state Submitted;
branches;
next	3.5;

3.5
date	94.08.05.19.09.45;	author buehler;	state Exp;
branches;
next	3.4;

3.4
date	94.07.21.21.18.19;	author buehler;	state Exp;
branches;
next	3.3;

3.3
date	94.07.15.19.05.07;	author buehler;	state Exp;
branches;
next	3.2;

3.2
date	94.01.31.16.21.42;	author buehler;	state Exp;
branches;
next	3.1;

3.1
date	94.01.17.01.49.54;	author buehler;	state Exp;
branches;
next	3.0;

3.0
date	93.12.15.21.37.08;	author buehler;	state Exp;
branches;
next	2.1;

2.1
date	93.11.30.19.56.38;	author buehler;	state Release;
branches;
next	2.0;

2.0
date	93.11.14.22.46.59;	author buehler;	state Exp;
branches;
next	;


desc
@Predicted state of PRAM on one side of the EDS.
@


4.2
log
@Drop dead comments.
Remove unneeded include statements.
@
text
@//==========================================================================
// PramPredConf.h
// RCS Stamp: $Id: PramPredConf.h,v 4.1 1995/05/09 16:18:08 buehler Submitted $
// Copyright: Massachusetts Institute of Technology
// .NAME PramPredConfig
// .LIBRARY libcgeds
// .HEADER EDS System Mgr predictions
// .INCLUDE PramPredConfig.h
// .FILE ../src/PramPredConf.C
// .SECTION Author
// Royce Buehler, buehler, MIT, buehler@@space.mit.edu

#ifndef __PRAMPREDICTEDCONFIG_H
#define __PRAMREDICTEDCONFIG_H

// .SECTION Description
// An EDSDesiredConfig specifies constraints on the commandable state of one
// side of the Experiment Data System. A PramPredictedConfiguration object is a
// record of how those constraints were met in the PRAM on one side of the
// EDS, and of what  configuration (concrete, commandable PRAM state) is
// supposed to result.
//
// The elements of the predicted PRAM state are:  contents of PRAM; 
// validity state of PRAM (EAs will be booted from PRAM only when it is in a 
// valid state, otherwise they are booted from ROM).
//
// A PramPredictedConfig object may be in any of 5 processing states:
//    Undefined - not yet associated with a DesiredConfig.
//    Defined   - DesiredConfig known, concrete configuration not yet
//                predicted (before inheritPrediction() is called, or
//                when inherited prediction does not provide enough 
//                information to fully specify a predicted state).
//    Inherited - Desired state is fully specified, not yet known whether
//                it is commandable.
//    Commandable - concrete configuration predicted and commandable.
//    Uncommandable - The predicted configuration cannot be commanded. Since
//                every legal desired configuration is always commandable,
//                this indicates a software error.
//
// REVISION HISTORY
// 4.2  6/08/95 reb Remove unneeded include statements, drop dead comments.
//============================================================================

#include <rw/collect.h>

#include <PredictedConfig.h>

#include "mit_instrum.h"  // Constants and enums describing EDS.
#include "mitCgEnums.h"   // MIT enums for command generation.
#include "PramDesired.h"

class RWFile;
class RWvistream;
class RWvostream;

class PramPredictedConfig 
 : public PredictedConfig
{
  RWDECLARE_COLLECTABLE(PramPredictedConfig)

public:

//============================================================================
//% The following methods are guaranteed to the SOC.
//============================================================================

  // Default constructor.
  PramPredictedConfig (void);

  // Constructor based on PramDesired.
  PramPredictedConfig(const PramDesired& pramDes, EDSSide aSide);

  // Principal constructor.
  PramPredictedConfig (const RWCString&  aPram, pramValidity aValidity,
		       EDSSide aSide);

  // Test for equality of values. Processing states must also match.
  RWBoolean operator==( PramPredictedConfig& other) const;

  //# Tests for inequality of values.
  RWBoolean operator!=( PramPredictedConfig& other) const
    { return ( !(*this == other)); };

  // Identifies this as PRAMA or PRAMB.
  virtual SubsystemId getSubsystemId( void) const;

  // Inherit from the EaPredictedConfig "aPrior".
  virtual void inheritPrediction (const PredictedConfig& aPrior);

  virtual CommandScript* getDeltaCommandScript(void) const;

  virtual CommandScript* getCommandScript(void) const;

  // Predicts telemetry rate from all four EAs on this side.
  virtual TelemRate* getTelemRate(const Source& /* sources */) const;

  // Display that identifies subsystem for this PredictedConfig.
  virtual void printShort (ostream& ostr) const;

  // Display predicted value of PRAM state, omitting don't-knows.
  virtual void print      (ostream& ostr) const;

  // Same as print() for now.
  virtual void printLong  (ostream& ostr) const;

//============================================================================
//% These public methods are internal to MIT.
//============================================================================

  // Returns the predicted address in RAM of the PRAM item with the given name.
  IntelAddress ramAddress( const RWCString& aPramItem) const;

  // Predicted state after commanding.
  PramDesired * prediction(void) const;


  PramDesired originalDC(void) const;

protected:
  // Insert command packets to load desired PRAM memory map.
  void commandPram(CommandScript* theScript, int isPartial = 1) const;

  // Insert command packets to set SSC and ASM drive voltages
  void commandVoltages(CommandScript* /* theScript */, 
		       int /* powerBits */) const;

public:
//% The followingfive methods support RW inheritance.

  RWspace   binaryStoreSize()               const;
  void      restoreGuts(RWFile& inf);
  void      restoreGuts(RWvistream& istrm);
  void      saveGuts(RWFile& outf)               const;
  void      saveGuts(RWvostream& ostrm)           const;

protected:
//% Data members
  EDSPCState       myState;
  EDSSide          mySide;         // Side A or B of the EDS
  PramDesired      initial;        // Originally desired PRAM state.
  PramDesired      inherited;      // Previously predicted PRAM state.
};
#endif         // _PRAMPREDICTEDCONFIG_H

@


4.1
log
@Drop obsolete method commandValidity.
Add isPartial argument to method commandPram.
@
text
@d3 1
a3 1
// RCS Stamp: $Id: PramPredConf.h,v 4.0 1995/03/11 20:25:39 buehler Exp buehler $
d39 3
a44 2
#include <rw/rwfile.h>
#include <rw/vstream.h>
a47 2
#include <EdsMemory.h>    // Memory map class, including PRAM, ROM.
#include "EDSDesConfig.h"
d52 4
a76 11
/*
  // Copy constructor
  PramPredictedConfig (const PramPredictedConfig&);

  // Assignment operator.
  PramPredictedConfig& operator=(const PramPredictedConfig& );

  // Destructor.
  virtual ~PramPredictedConfig(void);
*/

a93 5
// TBD: What is going to replace SOCErrObject?
//  virtual SOCErrObject* isError(void) const;
// Returns the current error object for this class, or 0 if
// preceding operation did not incur an error.

a139 1
//  EDSDesiredConfig* initialDC;    // Pointed to; do not destruct.
a141 3
// PRAMDynamic*    myDynamic;      // Managed here; predicted dynamic PRAM
// PRAMDynamic*    myPrevDynamic;  // Inherited dynamic PRAM

@


4.0
log
@Add constructor based on a PramDesired.
@
text
@d3 1
a3 1
// RCS Stamp: $Id: PramPredConf.h,v 3.10 1994/11/30 02:11:13 buehler Submitted buehler $
d134 1
a134 4
  void commandPram(CommandScript* theScript) const;

  // Insert command packets to set PRAM validity state to desired value.
  void commandValidity(CommandScript* /* theScript */) const;
@


3.10
log
@Use EdsMemory to load PRAM.
@
text
@d3 1
a3 1
// RCS Stamp: $Id: PramPredConf.h,v 3.9 1994/11/02 17:08:03 buehler Exp buehler $
d66 3
@


3.9
log
@PramDesired moved to its own file.
@
text
@d3 1
a3 1
// RCS Stamp: $Id: PramPredConf.h,v 3.8 1994/10/24 17:58:33 buehler Exp buehler $
d47 1
a47 1
// #include <EdsMemory.h>    // Memory map class, including PRAM, ROM.
d68 1
a68 1
  PramPredictedConfig (EdsMemory* /* aPram */, pramValidity aValidity,
d121 1
a121 1
  IntelAddress ramAddress( const RWCString& aPramItem, CommandScript& aScript);
a125 2
/*
  const PramDesired * originalDC(void) const;
d127 2
d131 1
a131 1
  void commandPram(CommandScript *) const;
d134 5
a138 2
  void commandValidity(CommandScript *) const;
*/
@


3.8
log
@Adjusted format and comments for use with genman++.
@
text
@d3 1
a3 1
// RCS Stamp: $Id: PramPredConf.h,v 3.7 1994/10/13 16:18:56 buehler Exp buehler $
d9 1
a9 1
// .FILE ../src/PramPredConfig.C
d47 1
a47 1
//#include <EdsMemory.h>    // Memory map class, including PRAM, ROM.
d51 1
a52 45
class PramDesired {
public:
  PramDesired(void);
//% The following five methods support persistence in objects 
//% containing a PramDesired.
//#if RWTOOLS < 0x0600 && ! defined(RWspace)
//#define RWspace unsigned
//#endif
  RWspace  binaryStoreSize(void) const;
  void restoreFrom( RWFile&);
  void restoreFrom( RWvistream&);
  void saveOn( RWFile& ) const;
  void saveOn( RWvostream& ) const;

  // Test for equality of values.
  RWBoolean operator==( PramDesired& ) const;

  RWBoolean operator!=( PramDesired& other) const
    { return ( !(*this == other)); };

  void setPramVersionName( const RWCString& aName);
  const RWCString getPramVersionName(void) const;
  EdsMemory* getContents(void);

  pramValidity state;
  RWCString pramVersionName;
private:
  EdsMemory* contents;
};

//# Sets PRAM version name to aName, and initializes EDSMemory ptr accordingly.
inline void PramDesired:: 
setPramVersionName( const RWCString& aName)
{
  pramVersionName = aName;
  contents = EdsMemory::restore(pramVersionName);
}

//# Returns name of the PRAM version (an EDSMemory object). 
inline const RWCString PramDesired:: 
getPramVersionName(void) const
{
  return( pramVersionName);
}

d68 2
a69 1
  PramPredictedConfig (EdsMemory*, pramValidity, EDSSide);
d83 1
a83 1
  RWBoolean operator==( PramPredictedConfig& ) const;
d92 2
a93 2
  // Inherit from the EaPredictedConfig "prior".
  virtual void inheritPrediction (const PredictedConfig& prior);
d105 1
a105 1
  virtual TelemRate* getTelemRate(const Source& sources) const;
d117 1
a117 1
// These public methods are internal to MIT.
d123 1
a124 1
  // Predicted state after commanding.
d138 1
a138 1
// The followingfive methods support RW inheritance.
a139 3
//#if RWTOOLS < 0x0600 && ! defined(RWspace)
//#define RWspace unsigned
//#endif
d141 4
a144 4
  void      restoreGuts(RWFile&);
  void      restoreGuts(RWvistream&);
  void      saveGuts(RWFile&)               const;
  void      saveGuts(RWvostream&)           const;
d147 1
@


3.7
log
@Change enum name from PRAMRunState to pramValidity.
@
text
@d3 1
a3 2
// RCS Stamp: $Id: PramPredConf.h,v 3.6 1994/08/12 19:22:59 buehler Exp buehler $
// Author:    Royce Buehler   - buehler@@space.mit.edu
d5 7
a11 1
//==========================================================================
a15 7
// .NAME PramPredictedConfig - Predicted configuration class for EDS instrument
// .LIBRARY libcgeds
// .HEADER PramPredictedConfig
// .INCLUDE "PramPredConf.h"
// .FILE PramPredConf.h
// .FILE PramPredConf.C

d55 5
a59 5
// The following five methods support persistence in objects 
// containing a PramDesired.
#if RWTOOLS < 0x0600 && ! defined(RWspace)
#define RWspace unsigned
#endif
d66 1
a67 1
// Test for equality of values.
d82 3
a84 2
// Set PRAM version name to aName, and initialize EDSMemory ptr accordingly.
inline void PramDesired:: setPramVersionName( const RWCString& aName)
d90 3
a92 2
// Return name of PRAM version. 
inline const RWCString PramDesired:: getPramVersionName(void) const
d105 1
a105 1
// The following methods are guaranteed to the SOC.
d108 1
a109 3
// Used implicitly when declaring arrays, this constructor leaves
// the PramPredictedConfig in an undefined state. Not to be called
// explicitly.
d111 1
a112 3
// Constructs a predicted object using the given PRAM layout, run state,
// and side of the EDS. 
// The resulting state of predicted object is "Defined".
d115 1
a116 1
// Copy constructor
d118 1
a119 1
// Assignment operator.
d121 1
a122 1
// Destructor.
d125 1
a126 1
// Test for equality of values. Processing states must also match.
d128 1
d132 1
a133 2
// Returns identifier saying for which side of the EDS we are predicting 
// PRAM state.
d135 1
a136 4
// Predicts a complete EDS state, given that preceding state was
// that predicted by "prior". State of the object becomes
// "commandable" provided all elements of the resulting state are
// known.
a138 6
// For an object in a state other than "commandable", returns an
// empty CommandScript. Otherwise, proceeds to construct commands
// to place PRAM in the predicted state. 
// Ideally, getDeltaCommandScript() will check, not that the desired
// memory map is the one in use, but that each of its ObsParamItems
// is supported by the memory map in use.
a140 2
// Works like getDeltaCommandScript, except that the desired state
// is commanded directly, without consulting the previous state.
d147 1
a148 2
// Returns telemetry rate predicted from all EAs on this side of the
// EDS.
d150 1
a151 2
// Prints a short description of this configuration on the output
// stream. Description includes subsystem name and configuration name.
d153 1
a154 2
// Prints a description of this configuration on the output stream,
// including memory map name and validity states.
d156 1
a157 3
// Prints a long description of this configuration on the output
// stream. Initially, this will have the same appearance as the
// result of print().
d163 1
a164 5
  // Return the predicted address in RAM of the PRAM item with the given name.
  // Returns the null address if item is unsupported.
  // If special actions need to be taken before the returned address will
  // be supported, packets commanding those actions will be appended to
  // aScript.
d167 1
a167 2
  // Predicted state in the form of a no-name desired configuration
  // without any dont-cares. Caller is responsible for deletion.
a170 3
// Returns a copy of the originally desired configuration with which this
// predicted configuration was initialized. Caller is responsible for
// deletion. Returns NULL if no initial DC was defined.
d173 1
a174 3
  // Insert command packets to load desired PRAM memory map.
  // This does absolute commanding
  // Must be const to be callable by getDeltaCommandScript.
d176 1
a177 3
  // Insert command packets to set PRAM validity state to desired value.
  // This does absolute commanding
  // Must be const to be callable by getDeltaCommandScript.
d181 1
a181 1
// The following methods are inherited from RWCollectable.
d183 3
a185 3
#if RWTOOLS < 0x0600 && ! defined(RWspace)
#define RWspace unsigned
#endif
@


3.6
log
@Temporarily set PramDesired member pramVersionName public for debugging.
@
text
@d3 1
a3 1
// RCS Stamp: $Id: PramPredConf.h,v 3.5 1994/08/05 19:09:45 buehler Exp buehler $
d78 1
a78 1
  PRAMRunState state;
d113 1
a113 1
  PramPredictedConfig (EdsMemory*, PRAMRunState, EDSSide);
@


3.5
log
@On restoreGuts, restore the EdsMemory pointer from the pramVersionName.
@
text
@d3 1
a3 1
// RCS Stamp: $Id: PramPredConf.h,v 3.4 1994/07/21 21:18:19 buehler Exp buehler $
d79 1
a80 1
  RWCString pramVersionName;
@


3.4
log
@Eliminated SOCErrorObject.
@
text
@d3 1
a3 1
// RCS Stamp: $Id: PramPredConf.h,v 3.3 1994/07/15 19:05:07 buehler Exp $
d88 1
a88 1
  contents = (EdsMemory*) 0;
@


3.3
log
@ Fixed binaryStoreSize() return type for RW 6.0
@
text
@d3 1
a3 1
// RCS Stamp: $Id: PramPredConf.h,v 3.2 1994/01/31 16:21:42 buehler Exp buehler $
d157 2
a158 1
  virtual SOCErrObject* isError(void) const;
@


3.2
log
@Added persistence.
@
text
@d3 1
a3 1
// RCS Stamp: $Id: PramPredConf.h,v 3.1 1994/01/17 01:49:54 buehler Exp buehler $
d59 4
a62 1
  unsigned binaryStoreSize(void) const;
d214 4
a217 1
  unsigned  binaryStoreSize()               const;
@


3.1
log
@Added getSubsystemId() method; ramAddress() now takes a commandScript
paramter. Dropped member pointer to parent EDSDesiredConfig, which was
not being used.
@
text
@d3 1
a3 1
// RCS Stamp: $Id: PramPredConf.h,v 3.0 1993/12/15 21:37:08 buehler Exp buehler $
d44 2
d54 23
a76 1
struct PramDesired {
a78 1
  PRAMRunState state;
d81 13
d97 1
a97 1
//  RWDECLARE_COLLECTABLE(PramPredictedConfig)
d126 6
d189 2
a190 1
  /*
d206 1
a211 3
//  int       compareTo(const RWCollectable*) const;
//  unsigned  hash()                          const;
  RWBoolean isEqual(const RWCollectable*)   const;
a216 1
*/
@


3.0
log
@Build 3.0. No change.
@
text
@d3 1
a3 1
// RCS Stamp: $Id: PramPredConf.h,v 2.1 1993/11/30 19:56:38 buehler Exp buehler $
d27 1
a27 1
// valid state).
d29 1
a29 1
// A PramPredictedConfig object may be in any of 5 states:
d38 3
a40 2
//    Uncommandable - EDSDesiredConfig constraints were not satisfiable
//                    given prior configuration of PRAM.
d89 5
d103 1
a103 6
// to place PRAM in the predicted state. If the predicted state
// requires writes to PRAM that will exceed available memory, enters
// "uncommandable" state. If, on exit, the PramPredictedConfig state
// is anything but "commandable",returns a CommandScript on which an
// error object has been set. If still  "commandable", returns the
// constructed commands.
d106 1
a106 1
// is supported by the one in use.
d137 1
a137 1
  IntelAddress ramAddress( RWCString& aPramItem);
d140 4
a143 1
/*
d147 1
a147 1

d152 1
a152 1
  
d164 1
a166 2
public:

d178 7
a184 6
  EDSPCState myState;
  EDSDesiredConfig* initialDC;    // Pointed to; do not destruct.
  PramDesired      initial;       // Originally desired PRAM state.
  PramDesired      inherited;     // Previously predicted PRAM state.
// PRAMDynamic* dynPRAM;          // Managed here; predicted dynamic PRAM
// PRAMDynamic* prevDynPRAM       // Inherited dynamic PRAM
@


2.1
log
@Uncommented EdsMemory data member.
Now uses correct arguments to (stubbed) principal constructor.
@
text
@d3 1
a3 1
// RCS Stamp: $Id: PramPredConf.h,v 2.0 1993/11/14 22:46:59 buehler Exp buehler $
@


2.0
log
@Initial version, most methods stubbed.
@
text
@d3 1
a3 1
// RCS Stamp: $Id$
d53 1
a53 1
//  EdsMemoryPtr contents;
d58 1
a58 1
// : public PredictedConfig
d73 4
a76 3
  PramPredictedConfig (const EDSDesiredConfig* desired);
// Constructs a predicted object corresponding to the constraints in
// "desired".  Resulting state of predicted object is "Defined".
a174 1
/*
d181 1
a181 1
*/
@

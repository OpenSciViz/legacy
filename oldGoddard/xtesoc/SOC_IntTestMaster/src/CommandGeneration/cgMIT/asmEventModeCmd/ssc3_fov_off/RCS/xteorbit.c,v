head	1.2;
access;
symbols;
locks; strict;
comment	@ * @;


1.2
date	2001.05.22.18.30.37;	author bob;	state Exp;
branches;
next	1.1;

1.1
date	2001.05.22.18.23.27;	author bob;	state Exp;
branches;
next	;


desc
@initial checkin of code delivered from MIT for asm event mode commanding.
@


1.2
log
@made rcsid static.
@
text
@/* xteorbit.c
 * Alan M. Levine (editor)
 * March 16, 1998
 *
 * The basic functions were obtained from the directory ~shirey/src/BARY
 *
 * These routines are used to predict the orbital position of XTE.
 */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "xteorbit.h"
#include "vec.h"

/* GSFC RCS id tag*/
static const char rcsid[] = "$Id: xteorbit.c,v 1.1 2001/05/22 18:23:27 bob Exp bob $";

#define DR 0.017453292   /* degrees to radians */

static int mpoints;
static double *epharray, *sctime;

/*******************************************************************************/
/* return value = 1 => Ephemeris file successfully opened & loaded
 *              = 0 =>                not opened (or some other problem)
 */

int runloadephem(char *ephemfile)
{
  FILE *pin;

  pin = fopen(ephemfile, "r");
  if (pin == 0)
    return(0);
  else
    {
      epharray = (double *)malloc(600000*sizeof(double));
      sctime = (double *)malloc(100000*sizeof(double));
      loadephem(epharray,sctime,&mpoints,pin);
      return(1);
    }
}

/*******************************************************************************/
/* For diagnostics - should have bounds checking added */

void printephemdat(int ilo, int ihi, FILE *fp)
{
  int i, j;

  for(i=ilo;i<=ihi;++i)
    {
      fprintf(fp,"%4d %16.6f",i,sctime[i]);
      for(j=0;j<3;++j)
	fprintf(fp,"  %6.1f",epharray[6*i+j]);
      fprintf(fp,"\n");
    }
}

/*******************************************************************************/
/*  loadephem.c    --  Load ephemeris data.
    02-20-96
    Deepto Chakrabarty

    [Revision 09-10-96 (Bob Shirey)
              Explicitly recast 'record' as double* before assignments.]
    [Revision 10-01-96 (Bob Shirey)
              Allow for ephemeris points outside of 1996 calendar year.
	      Add 2 leap seconds between 1994 Jan 1 and 1996 Jan 1.
	      ** New leap seconds (i.e. in 1997) must be added explicitly.
	      Output time is now TAI94: true (atomic) secs since 0h 1/1/94 UTC.]
    [Revision 03-28-97 (Bob Shirey)
              Added leap second planned for 6/30/97.
	      Added leap day for 2004 (optimistically).
	      Reduced final mpoints by 8 to cut off erroneous data at end.]

    Usage example: 

      int mpoints;
      double *array, *time;
      FILE *pin;
      pin = fopen("/nfs/cygx1/h1/ehm/sunoff/X1996001XEPHLO.01", "r");
      array = (double *)malloc(600000*sizeof(double));
      time = (double *)malloc(100000*sizeof(double));
      loadephem(array, time, &mpoints, pin)

    Note that the arrays have to be pre-allocated and the EPHEM file open.

    On output, array[6*mpoints] contains geocentric equatorial positions 
    (X,Y,Z) and velocities (XDOT,YDOT,ZDOT) in km and km/s, respectively. 
    Also, time[mpoints] contains times in TAI94 seconds. 

*/

void loadephem(double *array, double *time, int *mpoints, FILE *pin)
{
  char record[2800];
  double *doy, *sod, *dt, *temp;
  int i, npoints, yymmdd, year, leapdays94, leapsecs94;
  double *pvint1, *pvint2, *pvint3, *pvint4, *pvint5, *pvint6;

  leapdays94=0; /* leap days since 1/1/94 (as of 1/1/96)*/
                /* additional leap days added below as needed */

  leapsecs94=2; /* leap secs since 1/1/94 (as of 1/1/96)*/
                /* (June 30, 1994 and Dec. 31, 1995) */
                /* additional leap secs added below as needed */

  /* skip header records */
  fread(record, 2800, 1, pin);
  fread(record, 2800, 1, pin);

  /* Read in data */
  *mpoints = 0;

  while (fread(record, 2800, 1, pin) > 0) { /* Do each data record */
    npoints = 0;
    temp = (double*)&record[0];
    yymmdd = (int)*temp; /* YYMMDD date of first point in data record */
    year = yymmdd/10000;
    if (year > 90) 
      year += 1900;
    else 
      year += 2000;
    if (year > 1996)  leapdays94 = 1; 
    if (year > 2000)  leapdays94 = 2;
    if (year > 2004)  leapdays94 = 3;

    doy = (double*)&record[8];  /* day of year for first point in data record */
    sod = (double*)&record[16]; /* secs of day for first point in data record */
    dt = (double*)&record[24];  /* time interval between data points in secs */

    /* Add additional leap seconds here (add one line for each new leap sec)*/
    /* June 30,1997 23:59:60 */
    if ( (year > 1997) || (year==1997 && *doy>181) ) leapsecs94 = 3;

    for (i=0; i<50; i++) { /* Do each data point in record */

      /* Geocentric equatorial position of XTE in "decamegameters"(=1e7 m) */
      pvint1 = (double*)&record[32+48*i];  /* X position of XTE */
      pvint2 = (double*)&record[40+48*i];  /* Y position of XTE */
      pvint3 = (double*)&record[48+48*i];  /* Z position of XTE */

      /* Geocentric equatorial velocity in "decamegameters/centiday"(1e8 m/d)*/
      pvint4 = (double*)&record[56+48*i];  /* X velocity */
      pvint5 = (double*)&record[64+48*i];  /* Y velocity */
      pvint6 = (double*)&record[72+48*i];  /* Z velocity */

      /* Load into output array */
      if (*pvint1 < 1e15) { /* Check if valid data point or padding */
	array[6*(*mpoints)] = *pvint1*1e4;    /* positions in km */
	array[6*(*mpoints)+1] = *pvint2*1e4;
	array[6*(*mpoints)+2] = *pvint3*1e4;

	array[6*(*mpoints)+3] = *pvint4*1e6/86400.0;  /* velocities in km/s */
	array[6*(*mpoints)+4] = *pvint5*1e6/86400.0;
	array[6*(*mpoints)+5] = *pvint6*1e6/86400.0;

	/* TAI94 time (true (atomic) seconds since 1994 Jan 1.0 UTC) */
	time[*mpoints] = (*doy - 1 + (year-1994)*365 + leapdays94)*86400 + *sod + *dt*i + leapsecs94;
	(*mpoints)++;
      }
    }
  }
  (*mpoints)-=8; /* last 7 or 8 entries appear incorrect, so don't include them */ 
  fclose(pin);
}

/*******************************************************************************/
/*  findxte.c
    02-20-96
    Deepto Chakrabarty

    Dumb program to find best position of XTE. Linear interpolation
    between ephemeris points spaced by one minute. (96 minute orbit.)

    [Revision: 10-01-96 (Bob Shirey)
        Changed utc to tai94 to reflect changes in loadeph.c]

    [Revision: 03-17-97 (Bob Shirey)
        Changed 'static double best' to 'double best' and 'static imin' to 
	'int imin', so that these variables can evolve properly.
	Added imin's educated jump into array to avoid cycling through 
	whole array.]

    [Revision: 03-28-97 (Bob Shirey)
        Instead of exitting when time reaches end of ephem file, 
        look for new ephem files as needed until data is completed.
	(now using definitive versions of ephem files)]
         
    Inputs:
     TAI94             Time for position computation (true secs since 1/1/94)
     array[6*npoints]  Array of length 6*npoints obtained from loadephem,
                          with (X,Y,Z,XDOT,YDOT,ZDOT) in km and km/s.
			  Geocentric equatorial coordinates.
     sctime[npoints]   Array of length npoints with times from loadephem 
                          (TAI94 secs)
     npoints           Number of ephemeris points

    Outputs:
     rxte[3]           (X,Y,Z) of XTE at time tai94, in AU.
                       Note that 1 AU = 1.4959787e8 km.
     
*/

void findxte(double tai94, double *array, double *sctime, int npoints, 
	     double *rxte)
{

  double best=1e10; /*formerly static double*/
  int imin=0; /*formerly static*/
  int i, di;
  double diff, rbest[3], rnext[3], scale;
  FILE *pin;
  char filename[128];

  /* If MET outside spacecraft ephemeris bounds, try to find & load new ephem. */
  if (tai94<sctime[0] || tai94>sctime[npoints-1]) {
    /*if defin eph file not found (returns zero) try prelim ephem.*/
    if( ! get_xte_eph_defin(tai94, filename) ) get_xte_eph(tai94, filename);
    pin = fopen(filename, "r");
    loadephem(array, sctime, &npoints, pin);
    /*fprintf(stderr, "MET outside spacecraft ephemeris bounds.\n");*/
    /*exit(-1);*/
  }

  /* Make an educated jump into the ephemeris array*/
  imin = (int)((tai94-sctime[0])/60.0) - 1;
  if (sctime[imin]>tai94) imin=0; /* if jump was too far, just start at zero */
/*  fprintf(stderr,"findxte jumped to sctime[%ld] for tai=%lf\n",imin,tai94);*/

  /* Search array for closest entry */
  for(i=imin; i<npoints; i++) {
    diff = fabs(tai94-sctime[i]);
    if (diff < best) {
      best = diff;
      imin = i;
    }
  }

  /* Extract closest position entry */
  rbest[0] = array[6*imin];
  rbest[1] = array[6*imin+1];
  rbest[2] = array[6*imin+2];

  /* Extract next closest entry */
  if (tai94-sctime[imin] < 0) 
    di = -1;
  else 
    di = 1;
  rnext[0] = array[6*(imin+di)];
  rnext[1] = array[6*(imin+di)+1];
  rnext[2] = array[6*(imin+di)+2];

  /* Linear interpolation for best position */
  scale = (tai94-sctime[imin])/(sctime[imin+di]-sctime[imin]);
  rxte[0] = (rbest[0] + scale*(rnext[0]-rbest[0]))/1.4959787e8; /* AU */
  rxte[1] = (rbest[1] + scale*(rnext[1]-rbest[1]))/1.4959787e8; /* AU */
  rxte[2] = (rbest[2] + scale*(rnext[2]-rbest[2]))/1.4959787e8; /* AU */

}
  
/*******************************************************************************/
/*  findxte_1eph = a modified version of findxte 
    Revision: 03-16-98 (Alan M. Levine)
        Only look in one ephemeris file
	Position output in km

    Inputs:
     TAI94             Time for position computation (true secs since 1/1/94)

    Outputs:
     rxte[3]           (X,Y,Z) of XTE at time tai94, in km
     return value      0 => OK
                       1 => time earlier than ephemeris coverage
                       2 => time later than ephemeris coverage

*/

int findxte_1eph(double tai94, double rxte[3])
{

  double best;
  int imin, i, di;
  double diff, rbest[3], rnext[3], scale;

  if (tai94 < sctime[0])
    return(1);
  if(tai94 > sctime[mpoints-1])
    return(2);

  /* Make an educated jump into the ephemeris array*/
  imin = (int)((tai94-sctime[0])/60.0) - 1;
  if (imin < 0)
    imin = 0;
  if (sctime[imin] > tai94)
    imin = 0;
  /*  fprintf(stderr,"findxte jumped to sctime[%ld] for tai=%lf\n",imin,tai94);*/

  /* Search array for closest entry */
  best = 1.0e10;
  for(i=imin; i<mpoints; i++) {
    diff = fabs(tai94-sctime[i]);
    if (diff < best) {
      best = diff;
      imin = i;
    }
    else
      break;       /* assumes difference will grow */
  }

  /* Extract closest position entry */
  rbest[0] = epharray[6*imin];
  rbest[1] = epharray[6*imin+1];
  rbest[2] = epharray[6*imin+2];

  /* Extract next closest entry */
  if (tai94 < sctime[imin]) 
    di = -1;
  else 
    di = 1;
  rnext[0] = epharray[6*(imin+di)];
  rnext[1] = epharray[6*(imin+di)+1];
  rnext[2] = epharray[6*(imin+di)+2];

  /* Linear interpolation for best position */
  scale = (tai94-sctime[imin])/(sctime[imin+di]-sctime[imin]);
  rxte[0] = (rbest[0] + scale*(rnext[0]-rbest[0]));
  rxte[1] = (rbest[1] + scale*(rnext[1]-rbest[1]));
  rxte[2] = (rbest[2] + scale*(rnext[2]-rbest[2]));

  return(0);
}
  
/*******************************************************************************/
/* Copied from convert_time.c */
/* Functions for XTE time conversions */

/* MET (Mission Elapsed Time)= SCCS (Spacecraft Clock Seconds) since 1994Jan1 */

#define MJDREFI 49353          /* MJD for 1994.0(UTC) */
                               /* Also equals integer part of MJD in TT */
                               /* (MJD = JD - 2400000.5) */

#define MJDREFF 0.000696574074 /* 1994.0(UTC) in TT days (fractional part) */
                               /* 28 leap-secs (UTC->TAI) + 32.184 s (TAI->TT)*/
#define TIMEZERO 3.37843167E+00   /* Clock correction */
                                  /* (valid at least 3/6/96 - 9/1/96 */

/* UTC94 = true seconds since 1994 Jan 1 0h UTC*/
double met_to_utc94(double met){
  return( met + TIMEZERO  );
}

/*  BEWARE - THESE ARE NOT PRECISE IF LEAP SECOND ADJUSTMENTS ARE NEEDED !!!! */

double met_to_tt(double met){
  return( (met + TIMEZERO)/86400.0 + MJDREFI + MJDREFF );
}

double utc94_to_mjd(double utc94){
  return( MJDREFI + utc94/86400 );
}

/* TJD = MJD - 40000 = JD - 2440000.5  */
double utc94_to_tjd(double utc94){
  return( MJDREFI - 40000 + utc94/86400 );
}

/*******************************************************************************/
/* From Bob Shirey's get_xte_eph_defin.c */

int get_xte_eph_defin(double tai94, char* filename)
{
  int daytai94, week, doy, year, w, y, d, days_old, weeks_old, version;
  FILE *ephfile;
  double leapsecs94=2;
 
  if(tai94>110332800.) ++leapsecs94; /* 6/30/97 */ 
  daytai94 = (int)((tai94-leapsecs94)/86400.0);
  
  year = 1996;
  doy = daytai94 - 729;
  while(doy>1461) {doy-=1461; year+=4;}
  if(doy>366) {
    doy-=366; 
    ++year;
    if(doy>365) {doy-=365; ++year;}
    if(doy>365) {doy-=365; ++year;}
  }
  /*sprintf(filename,"/nfs/sco94/d1/mitop/Ephem/X%4d%03dXEPHDE.01",year,doy);*/
  sprintf(filename,"/xte/Ephem/X%4d%03dXEPHDE.01",year,doy);
  
  ephfile = fopen(filename, "r");
  
  if(ephfile == NULL) {
    fprintf(stderr,"%s not found\n",filename);
    fprintf(stderr,"Error: no definitive ephemeris file found for %lf (%d %03d)\n", tai94, year, doy); 
    return(0);
  }
 
  fprintf(stderr,"Opening ephemeris file %s for time %lf (%d day %d)\n",filename, tai94, year, doy); 
 
  fclose(ephfile);
 
  return(1);
}

/*******************************************************************************/
/* From Bob Shirey's get_xte_eph.c */

void get_xte_eph(double tai94, char* filename)
{
  int daytai94, week, doy, year, w, y, d, days_old, weeks_old;
  FILE *ephfile;
  double leapsecs94=2;
 
  if(tai94>110332800.) ++leapsecs94; /* 6/30/97 */ 
  daytai94 = (int)((tai94-leapsecs94)/86400.0);
 
  week = (int)((daytai94-755)/7); /* week000 effectively starts day 755 */
  if (week < 0 ) week = 0; /* all of IOC is called week000 so no negative week*/
  
  year = 1996;
  doy = daytai94 - 729;
  while(doy>1461) {doy-=1461; year+=4;}
  if(doy>366) {
    doy-=366; 
    ++year;
    if(doy>365) {doy-=365; ++year;}
    if(doy>365) {doy-=365; ++year;}
  }
 
  for (weeks_old=0; weeks_old<8; ++weeks_old){
    for (days_old=0; days_old<50; ++days_old) {
      w=week-weeks_old+1; /* add 1 to start checking in following week */
      if (w<0) w=0;
      y=year;
      d=doy-days_old;
      if (d<=0) {
        d+=365;
        y=year-1;
      }
      sprintf(filename,"/xte/sochome/sanction/week%03d/X%4d%03dXEPHLO.01",w,y,d);
      if( (ephfile = fopen(filename, "r")) != NULL ) break;
    }
    if( ephfile != NULL ) break;
  }
  
  if(ephfile == NULL) {
    fprintf(stderr,"Error: no ephemeris file found for %lf (%d day %d)\n", tai94, year, doy); 
    fprintf(stderr,"Last attempt was: %s\n",filename);
    exit(1);
  }
 
  fprintf(stderr,"WARNING: using preliminary spacecraft ephemeris file.\n");
  fprintf(stderr,"Opening ephemeris file %s for %lf (%d day %d)\n",filename, tai94, year, doy); 
 
  fclose(ephfile);
 
  return;
}

/*******************************************************************************/
@


1.1
log
@Initial revision
@
text
@d17 1
a17 1
const char rcsid[] = "$Id: $";
@

head	1.1;
access;
symbols;
locks; strict;
comment	@ * @;


1.1
date	2001.03.21.16.42.44;	author bob;	state Exp;
branches;
next	;


desc
@initial checkin after delivery by ehm. This is a modified version
of the SOF file of the same name.  This is used only by MIT.
@


1.1
log
@Initial revision
@
text
@//----------------------------------------------------------------------
//
//  File:        XTETime.C
//  Programmer:  Arnold Rots  -  USRA
//  Date:        31 October 1995
//  Subsystem:   XFF
//  Library:     ObsCat
//  Description: Code for XTETime, XTETimeRange, XTRList classes
//
//----------------------------------------------------------------------
//
static const char * const rcsID = "$Id: XTETime.C,v 1.5 2001/01/24 18:42:47 ehm Exp ehm $" ;
// MIT RCS id was "$XTETime.C,v 1.5 2001/01/24 18:42:47 ehm Exp ehm $" ;
#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <iostream.h>
#include <stdlib.h>
#include "XTETime.h"
#define TAIUTC "tai-utc.dat"

const double XTETime::MJD0        = 2400000.5 ;  // JD - MJD
const double XTETime::MJDREF      = 49353.0 ;    // MJD at 1994.0
const double XTETime::TT2UT       = 60.184 ;     // TT - UTC at 1994.0
int XTETime::NUMLEAPSECS = 0 ;      // Leap seconds: 1994.5, 1996.0, 1997.5, 1999.0
int XTETime::LEAPSECDAYS[] = {181, 730, 1277, 1826} ;  // Day #'s where 1994 Jan 1 = 1
     // at the end of which leap seconds occurred for UTC
double XTETime::LEAPSECS[]  = {15638400.0, 63072001.0, 110332802.0, 157766403.0} ;

static double tprecision=1.;
static char soformat[27]="%4d:%03d:%02d:%02d:%02.0lf";
static int daymonth[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31} ;
static const char * const month[12] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun",
				       "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"} ;
//
//   -----------------
// -- isLeapYr --
//   -----------------
//

// Description:
// Determine if a year is a leap year
int XTETime_isLeapYr (int year) {
  if ( (year%4) || (!(year%100) && (year%400)) )
    return(0);   // y is not a leap year
  else
    return(1);   // y is a leap year
}

//
//   -----------------
// -- numLeapYrs --
//   -----------------
//

// Description:
// Determine the number of leap years in the range 1994 to year y-1 (inclusive)
int XTETime_numLeapYrs (int y) {
  int yr;
  return(XTETime_numLeapYrs(1994,y));
}

int XTETime_numLeapYrs(int y1, int y2) {
  int yr, n;
  
  yr = y2 - 1;
  n = (yr/4) - (yr/100) + (yr/400) - ((y1/4) - (y1/100) + (y1/400)) ;
  
  return(n);
}


// Description:
// Determine the day of year for the day number where 1994 January 1 = day 1.
int XTETime_dayOfYear(int day)
{
  int year;
  year=XTETime_yearOfDay(day);
  day -= (year - 1994) * 365 ;
  day -= XTETime_numLeapYrs(year) ;
  return(day);
}

// Description:
// Determine the year of the day number where 1994 January 1 = day 1.
int XTETime_yearOfDay (int day) {
  int d, yr;

  d = (483 + 365*1994) + day - 1;
  yr = 400*(d/146097) ;
  d -= 146097*(d/146097) ;
  yr += 100*(d/36524) ;
  d -= 36524*(d/36524) ;
  yr += 4*(d/1461) ;
  d -= 1461*(d/1461) ;
  yr += (d/365) ;
  return(yr);
}

//
//   ---------------------------------------------------------
// -- XTETime::XTETime (double tt, TimeSys ts, TimeFormat tf) --
//   ---------------------------------------------------------
//

// Description:
// Constructor: most general constructor
// Default for tf is SECS
XTETime::XTETime (double tt, TimeSys ts, TimeFormat tf)
  : t (tt), timeZero (UTCF)
{
  setleaps() ;
  set (tt, ts, tf) ;
  return ;
}

//
//   ----------------------------------------------------------
// -- XTETime::XTETime (char *date, TimeSys ts, TimeFormat tf) --
//   ----------------------------------------------------------
//

// Description:
// Constructor: create from a date string
// Default for ts is UTC, for tf is DATE
XTETime::XTETime (const char *date, TimeSys ts, TimeFormat tf)
  : t (0.0), timeZero (UTCF)
{
  setleaps() ;
  set (date, ts, tf) ;
  return ;
}


//
//   --------------------------
// -- XTETime::setleaps (void) --
//   --------------------------
//

// Description:
// Function to set leap second table
void XTETime::setleaps (void)
{
  if ( !NUMLEAPSECS ) {
    FILE *FF ;
    char lsfile[256] ;
    sprintf (lsfile, "%s/etc/%s", getenv ("SOCHOME"), TAIUTC);
    FF = fopen (lsfile, "r");

    // If the file in /sochome/etc/ can't be opened, then try the 
    // current directory.
    if ( ! FF ) {
      sprintf(lsfile, "./%s", TAIUTC);
      FF = fopen (lsfile, "r");
    }

    if ( FF ) {

      int leapsMJD ;
      double JD ;
      while ( fscanf (FF, "%*d %*s  1 =JD %lf %*s %*lg S + (MJD - %*lg) X %*lg %*s",
		      &JD) == 1 ) {
	leapsMJD = (int) (JD - MJD0 - MJDREF + 0.1) ;
	if ( leapsMJD > 0 ) {
	  LEAPSECDAYS[NUMLEAPSECS] = leapsMJD ;
	  LEAPSECS[NUMLEAPSECS] = leapsMJD*86400.0 + NUMLEAPSECS ;
	  NUMLEAPSECS++ ;
	}
      }
      fclose (FF) ;
    }
    else
      NUMLEAPSECS = 4 ;          // Leap seconds: 1994.5, 1996.0, 1997.5, 1999.0

  }
  return ;
}

//
//   -----------------
// -- XTETime::numLeapSecs --
//   -----------------
//

// Description:
// Determine the number of leap seconds that have been defined for UTC
// after 1994 Jan. 1 and prior to day number nday 
// where 1994 Jan. 1 = day 1
int XTETime::numLeapSecs () const{
  int i, n ;
  double tt;
  tt  = (double) ((int) t);
  n = 0;
  for (i=0; i<NUMLEAPSECS; i++)
    if ( tt > LEAPSECS[i] )
      ++n ;

  return(n);
}

int XTETime::numLeapSecs (int nday)const {
  int i, n;

  n = 0;
  for (i=0; i<NUMLEAPSECS; i++)
    if ( nday > LEAPSECDAYS[i] )
      ++n ;

  return(n);
}

int XTETime::numLeapSecs (double tt) const {
  int i, n ;

  tt  = (double) ((int) tt);
  n = 0;
  for (i=0; i<NUMLEAPSECS; i++)
    if ( tt > LEAPSECS[i] )
      ++n ;

  return(n);
}

//
//   -----------------
// -- XTETime::inLeapSec --
//   -----------------
//

// Description:
// Determine whether the time corresponds to being in one of the leap
// seconds defined for UTC
int XTETime::inLeapSec (void) const {
  return(inLeapSec(t));   // t is not in any of the leap seconds
}

int XTETime::inLeapSec (double tt) const{
  for (int i=0; i<NUMLEAPSECS; i++)
    if ( (tt >= LEAPSECS[i]) && (tt < LEAPSECS[i] + 1.0) )
      return(1);   //tt is in this leap second
  return(0);   // tt is not in any of the leap seconds
}

int XTETime::inLeapSec (int day, int hour, int minute, double second) const {
  if (leapSecondToday(day))
    if (second >= 60.)
      if (minute==59)
	if (hour==23)
	    return(1);   //this is a leap second
  return(0);   // tt is not in any of the leap seconds
}

int XTETime::leapSecondToday(int day) const{
  for (int i=0; i<NUMLEAPSECS; i++)
    if (day == LEAPSECDAYS[i]) 
      return(1);
  return(0);
}

int XTETime::isLeapYr(void) const{
  return(isLeapYr(yearOfDay()));
}

int XTETime::numLeapYrs(void) const {
  return(numLeapYrs(yearOfDay()));
}

int XTETime::yearOfDay(void) const {
  int day= (int) (t/86400);
  return yearOfDay(day);
}

int XTETime::dayOfYear(void) const {
  int day= (int) (t/86400);
  return dayOfYear(day);
}
// Return time as date string as used in the SOF CG
// This is UTC, ingoring all leapseconds since 1994.

const char *XTETime::SOCTime(void) {
  double saveprec=tprecision;
  const char *foo;
  setPrecision(1);
  foo=getDate (SOF, DATE);
  setPrecision(saveprec);
  return ( foo ) ;
}


//
//   -----------------------------------------------------
// -- XTETime::set (double tt, TimeSys ts, TimeFormat tf) --
//   -----------------------------------------------------
//

// Description:
// General set function
void XTETime::set (double tt, TimeSys ts, TimeFormat tf)
{
  int nl;

  switch (tf) {
  case JD:
    tt -= MJD0 ;
  case MJD:
    tt -= MJDREF ;
    if (ts == UTC)
      nl = numLeapSecs( ((int) tt) + 1);
    else
      nl = 0;
    tt *= 86400.0 ;
    tt += (double) nl;
  case SECS:
    break ;
  default:                //  Error; do nothing
    return ;
  }

  switch (ts) {
  case SOF:
    break ;
  case TT:
    tt -= TT2UT ;
    break ;
  case UTC:
    break;
  default:
    return ;
  }
  t = tt ;
  return ;
}

//
//   ------------------------------------------------------
// -- XTETime::set (char *date, TimeSys ts, TimeFormat tf) --
//   ------------------------------------------------------
//

// Description:
// General set function from a date string
void XTETime::set (const char *date, TimeSys ts, TimeFormat tf)
{
  int year, day, hour, minute;
  double second ;
  int n ;
  int m = 0 ;
  char mn[4] ;

  switch (tf) {
  case DATE:
    n = sscanf (date, "%d:%d:%d:%d:%lg", &year, &day, &hour, &minute, &second) ;
    if ( n != 5 )
      return ;
    break ;
  case CALDATE:
    n = sscanf (date, "%d%c%c%c%d at %d:%d:%lg",
		&year, mn, mn+1, mn+2, &day, &hour, &minute, &second) ;
    if ( n != 8 )
      return ;
    if ( isLeapYr(year) )
      daymonth[1] = 29 ;
    else
      daymonth[1] = 28 ;
    mn[0] = toupper(mn[0]) ;
    mn[1] = tolower(mn[1]) ;
    mn[2] = tolower(mn[2]) ;
    mn[3] = 0 ;
    while ( strcmp(mn, month[m]) ) {
      if ( m > 11 )
	return ;
      day += daymonth[m++] ;
    }
    break ;
  default:
    return ;
  }
  day += (year - 1994) * 365 ;
  day += numLeapYrs(year) ;
  double nsecond = (double) (day-1) * 86400 + hour * 3600 + minute * 60 + second ;
  if (ts == UTC){
      nsecond += (double) numLeapSecs(day);
  }
  set (nsecond, ts, SECS) ;

  return ;
}

//
//   -----------------
// -- XTETime::monDay --
//   -----------------
//

// Description:
// Convert year:day of year date string to calendar date string
const char *XTETime::monDay (const char *date) {
  char d[32] ;
  int year, day ;
  int m = 0 ;

  strcpy (d, date) ;
  sscanf (d, "%d:%d", &year, &day) ;
  if ( isLeapYr(year) )
    daymonth[1] = 29 ;
  else
    daymonth[1] = 28 ;

  while ( day > daymonth[m] ) {
    day -= daymonth[m] ;
    m++ ;
  }
  sprintf (tdate, "%04d%s%02d at ", year, month[m], day) ;
  strcpy (tdate+13, d+9) ;
  return ( tdate ) ;
}


//
//   ------------------------------------------
// -- XTETime::get(TimeSys ts, TimeFormat tf) --
//   ------------------------------------------
//

// Description:
// Generalized time return function
double XTETime::get (TimeSys ts, TimeFormat tf) const {
  double tt ;
  switch (ts) {
  case UTC:
    tt = getMET () ;
    break ;
  case TT:
    tt = getTT () ;
    break ;
  case SOF:
    tt = getMET () ;
    break ;
  }
  switch (tf) {
  case MJD:
    if (ts == UTC)
      tt -= (double) numLeapSecs(tt) ;
    tt = tt / 86400.0 + MJDREF ;
    break ;
  case JD:
    if (ts == UTC)
      tt -= (double) numLeapSecs(tt) ;
    tt = tt / 86400.0 + MJDREF + MJD0 ;
    break ;
  default:
    break ;
  }
  return tt ;
}


//
//
void XTETime::setPrecision(double p)
{
  if ( (p <= 0.0) || (p > 1.0) )
    p = 1.0;
  else if (p < 1.0e-6)
    p = 1.0e-6;

  tprecision=p;
  int ii=0;
  if (p < 1)
    {
      while (p < 1){
	ii++;
	p *= 10.;
      }
      sprintf(soformat,"%s%d.%d%s","%4d:%03d:%02d:%02d:%0", ii+3, ii,"lf");
    }
}

// Description:
// Return time as MJD(UTC)
double XTETime::UTmjd (void) const {
  double tt=getMET();
  tt -= numLeapSecs(tt);
  tt /= 86400;
  int day= (int) tt;
  if (leapSecondToday(day)){
    double frac=8600*tt-86400*day;
    frac /= 86401;
    tt=day+frac;
  }
  return (tt + MJDREF ) ;
}
// Description:
// Return Week number for telemetry packets.
int XTETime::getWeek (TimeUse tu) const {
  double tt=getMET();
  if (tu == TELEMETRY)
    tt -= UTCF;
  return ((int)(tt/(86400*7))-108) ;
}
// Description:
// Return Day number for telemetry packets.
int XTETime::getDay (TimeUse tu) const {
  double tt=getMET();
  if (tu == TELEMETRY)
    tt -= UTCF;
  return ((int)(tt/86400)) ;
}




//
//   ----------------------------------------------
// -- XTETime::getDate (TimeSys ts, TimeFormat tf) --
//   ----------------------------------------------
//

// Description:
// Generalized date string return function
const char *XTETime::getDate (TimeSys ts, TimeFormat tf) {
  double tt = get(ts,SECS) ;
  int year, day, hour, minute, second ;
  double subsecond, roundsec ;

  roundsec = (tt + 0.5*tprecision) ;   // round
  second = (int) roundsec;
  subsecond = (int) (((tt -second)/tprecision) + 0.5);
  subsecond *= tprecision;

  if (ts == UTC){
    second -= numLeapSecs(tt);
    roundsec -= numLeapSecs(tt);
  }
  day = second / 86400 ;
  second %= 86400 ;
  hour = second / 3600 ;
  second %= 3600 ;
  minute = second / 60 ;
  second %= 60 ;
  day++ ;
  year = yearOfDay(day);
  day = dayOfYear(day);
  if ((ts == UTC) && (inLeapSec(roundsec))){
    second = 60 ;
    day--;
    minute=59;
    hour=23;
  }
  sprintf (tdate, soformat,
	   year, day, hour, minute, second+subsecond) ;
  switch (ts) {
  case TT:
    strcat (tdate, "TT") ;
    break ;
  case UTC:
    strcat (tdate, "UTC") ;
    break ;
  case SOF:
    break ;
  default:
    break ;
  }

  if ( tf == CALDATE )
    return ( monDay (tdate) ) ;
  else
    return tdate ;
}

//
//   ---------------------------
// -- XTETimeRange::setEmpty () --
//   ---------------------------
//

// Description:
// Determine whether range is empty
void XTETimeRange::setEmpty (void) {
  double t1=start.getMET() ;
  double t2=stop.getMET() ;
  if ( ( t1 >= t2 ) || ( t1 <= 0.0 ) || ( t2 <= 0.0 ) )
    empty = 1 ;
  else
    empty = 0 ;
  return ;
}

//
//   -----------------------------
// -- XTETimeRange::printRange () --
//   -----------------------------
//

// Description:
// A two-liner in UTC date format
void XTETimeRange::printRange (void) {
  cout << "---XTETimeRange - Empty: " << empty
       << ", Start: " << start.getMET() << " (" << UTStartDate () << ")\n"
       << "                       "
       << "  Stop:  " << stop.getMET() << " (" << UTStopDate () << ")\n" ;
  return ;
}
//
//   --------------------------------
// -- XTETimeRange::printRangeCal () --
//   --------------------------------
//

// Description:
// A two-liner in UTC calendar date format
void XTETimeRange::printRangeCal (void) {
  cout << "---XTETimeRange - Empty: " << empty
       << ", Start: " << start.getMET() << " (" << start.UTCalDate () << ")\n"
       << "                       "
       << "  Stop:  " << stop.getMET() << " (" << stop.UTCalDate () << ")\n" ;
  return ;
}

//
//   -----------------------
// -- XTRList::printList () --
//   -----------------------
//

// Description:
// Print list contents in UTC calendar date format
void XTRList::printList (void) {
  cout << "\nXTRList - Empty: " << empty << ", Number of ranges:: " << numXTRs
       << ", List range:\n" ;
  listRange.printRange () ;
  if ( numXTRs ) {
    cout << "Member ranges:\n" ;
    for (int i=0;i<numXTRs;i++)
      tr[i].printRange () ;
  }
  return ;
}

//
//   --------------------------
// -- XTRList::printListCal () --
//   --------------------------
//

// Description:
// Print list contents in UTC calendar date format
void XTRList::printListCal (void) {
  cout << "\nXTRList - Empty: " << empty << ", Number of ranges:: " << numXTRs
       << ", List range:\n" ;
  listRange.printRangeCal () ;
  if ( numXTRs ) {
    cout << "Member ranges:\n" ;
    for (int i=0;i<numXTRs;i++)
      tr[i].printRangeCal () ;
  }
  return ;
}

//
//   ----------------------------
// -- XTRList::XTRList (XTRList) --
//   ----------------------------
//

// Description:
// Copy constructor for a new TR list
XTRList::XTRList (const XTRList &trl)
{
  numXTRs = trl.numXTRs ;
  listRange = trl.listRange ;
  empty = trl.empty ;
  tr = new XTETimeRange[numXTRs] ;
  for (int i=0; i<numXTRs; i++)
    tr[i] = trl.tr[i] ;
  return ;
}

//
//   ------------------------------
// -- XTRList::operator= (XTRList) --
//   ------------------------------
//

// Description:
// Copy operator for a TR list
XTRList& XTRList::operator= (const XTRList &trl)
{
  delete [] tr ;
  numXTRs = trl.numXTRs ;
  listRange = trl.listRange ;
  empty = trl.empty ;
  tr = new XTETimeRange[numXTRs] ;
  for (int i=0; i<numXTRs; i++)
    tr[i] = trl.tr[i] ;
  return *this ;
}


//
//   -------------------------------------
// -- XTRList::XTRList (XTRList, XTRList) --
//   -------------------------------------
//

// Description:
// Construct a new TR list by "AND"ing two existing lists
XTRList::XTRList (const XTRList &trl1, const XTRList &trl2)
  : numXTRs (1), empty (1), tr (0) {

  //  Trivial cases: if one of them is empty, the result is empty

  if ( trl1.isEmpty() || trl2.isEmpty() ) {
    numXTRs = 1 ;
    empty = 1 ;
    tr = new XTETimeRange () ;
    listRange = *tr ;
    return ;
  }

  //  To minimize work, make sure the second one is the shortest

  const XTRList *list1 = &trl1 ;
  const XTRList *list2 = &trl2 ;
  int nlist1 = list1->numXTRs ;
  int nlist2 = list2->numXTRs ;
  if ( nlist1 < nlist2 ) {
    int i = nlist2 ;
    nlist2 = nlist1 ;
    nlist1 = i ;
    list1 = list2 ;
    list2 = &trl1 ;
  }

  //  Simple case: second list has only one member

  if ( nlist2 == 1 ) {
    XTRList scratchlist (*list1) ;
    scratchlist.andRange ( list2->tr[0] ) ;
    numXTRs = scratchlist.numXTRs ;
    listRange = scratchlist.listRange ;
    empty = scratchlist.empty ;
    tr = new XTETimeRange[numXTRs] ;
    for (int i=0; i<numXTRs; i++)
      tr[i] = scratchlist.tr[i] ;
    return ;
  }

  //  The full works: AND each range in list2 with all of list1
  //                  OR the resulting lists

  XTRList buildlist ;
  int i ;
  for (i=0;i<nlist2;i++) {
    XTRList scratchlist (*list1) ;
    scratchlist.andRange ( list2->tr[i] ) ;
    buildlist.orList (scratchlist) ;
  }
  numXTRs = buildlist.numXTRs ;
  listRange = buildlist.listRange ;
  empty = buildlist.empty ;
  tr = new XTETimeRange[numXTRs] ;
  for (i=0; i<numXTRs; i++)
    tr[i] = buildlist.tr[i] ;
  return ;
}

//
//   -------------------------------
// -- XTRList::isInRange (XTETime&) --
//   -------------------------------
//

// Description:
// Return 0 if in range
int XTRList::isInRange (const XTETime &T) const {
  for (int i=0;i<numXTRs;i++)
    if ( !tr[i].isInRange (T) )
      return 0 ;
  return 1 ;
}

//
//   -----------------------------
// -- XTRList::isInRange (double) --
//   -----------------------------
//

// Description:
// Return 0 if in range
int XTRList::isInRange (double t) const {
  for (int i=0;i<numXTRs;i++)
    if ( !tr[i].isInRange (t) )
      return 0 ;
  return 1 ;
}

//
//   ------------------------------
// -- XTRList::getRange (XTETime&) --
//   ------------------------------
//

// Description:
// Return range in which XTETime object <T> falls
const XTETimeRange *XTRList::getRange (const XTETime &T) const {
  for (int i=0;i<numXTRs;i++)
    if ( !tr[i].isInRange (T) )
      return tr+i ;
  return NULL ;
}

//
//   ----------------------------
// -- XTRList::getRange (double) --
//   ----------------------------
//

// Description:
// Return range in which MET time <t> falls
const XTETimeRange *XTRList::getRange (double t) const {
  for (int i=0;i<numXTRs;i++)
    if ( !tr[i].isInRange (t) )
      return tr+i ;
  return NULL ;
}

//
//   -----------------------
// -- XTRList::totalTime () --
//   -----------------------
//

// Description:
// Return total time (in seconds), covered by the list
double XTRList::totalTime (void) const {
  double tt = 0.0 ;
  if ( !empty )
    for (int i=0;i<numXTRs;i++)
      tt += tr[i].totalTime () ;
  return tt ;
}

//
//   -----------------
// -- XTRList::orList --
//   -----------------
//

// Description:
// "OR" in another XTETime Range List
void XTRList::orList (const XTRList &trl) {

  //  Do nothing if trl is empty

  if ( trl.empty )
    return ;

  //  If *this is empty, replace it by trl

  if ( empty ) {
    delete [] tr ;
    numXTRs = trl.numXTRs ;
    listRange = trl.listRange ;
    empty = trl.empty ;
    tr = new XTETimeRange[numXTRs] ;
    for (int i=0; i<numXTRs; i++)
      tr[i] = trl.tr[i] ;
  }

  //  Do the full thing

  else {
    int n = trl.numXTRs ;
    for (int i=0;i<n;i++)
      orRange ( trl.tr[i] ) ;
  }
  return ;
}

//
//   -----------------
// -- XTRList::notList --
//   -----------------
//

// Description:
// Negate a XTETime Range List over a specified time range
void XTRList::notList (const XTETimeRange &T) {

  //  If the list was empty, the answer is just T ...

  if ( empty ) {

    //  ... unless, of course, T was empty, too, in which case nothing changes

    if ( !T.isEmpty() ) {
      *tr = T ;
      listRange = T ;
      numXTRs = 1 ;
      empty = 0 ;
    }
  }

  //  "Regular" case

  else {
    XTETimeRange *ntr = new XTETimeRange[numXTRs+1] ;
    ntr[0].setStart(1000.0) ;
    for (int i=0; i<numXTRs; i++) {
      ntr[i].setStop(tr[i].TStart()) ;
      ntr[i+1].setStart(tr[i].TStop()) ;
    }
    ntr[numXTRs].setStop(1.0e20) ;
    numXTRs++ ;
    delete [] tr ;
    tr = ntr ;
    setListRange () ;
    andRange (T) ;
  }
  return ;
}


//
//   -------------------
// -- XTRList::andRange --
//   -------------------
//

// Description:
// "AND" in an extra XTETime Range
void XTRList::andRange (const XTETimeRange &T) {
  int startin=0, stopin=0 ;
  int startafter=0, stopafter=0 ;
  int zap=0 ;
  int istart, istop ;
  int i ;
  double tstart = T.METStart () ;
  double tstop = T.METStop () ;
  //
  //  First the trivial cases
  //
  if ( empty )
    return ;
  else if ( T.isEmpty () )
    zap = 1 ;
  else if ( ( tstart <= listRange.METStart () ) &&
	    ( tstop >= listRange.METStop () ) )
    return ;
  else if ( tstop < listRange.METStart () )
    zap = 1 ;
  else if ( tstart > listRange.METStop () )
    zap = 1 ;
  //
  //  See where the start and stop times fall in the existing list
  //  (add 1 to the indices)
  else {
    for (i=0;i<numXTRs;i++) {
      if ( !startin ) {
	istart = tr[i].isInRange (tstart) ;
	if ( !istart )
	  startin = i + 1 ;
	else if ( istart > 0 )
	  startafter = i + 1 ;
      }
      if ( !stopin ) {
	istop = tr[i].isInRange (tstop) ;
	if ( !istop )
	  stopin = i + 1 ;
	else if ( istop > 0 )
	  stopafter = i + 1 ;
      }
    }
    //
    //  Now figure out what to do
    //    Which range do we start in?
    //
    if ( startin ) {
      startin -- ;                     // Correct the index
      tr[startin].setStart (tstart) ;  // Adjust the time
    }

    //      In between
    else if ( !stopin && ( startafter == stopafter ) )
      zap = 1 ;

    //      Start after
    else
      startin = startafter ;

    //    Which range do we stop in?
    if ( !zap ) {
      if ( stopin ) {
	stopin-- ;
	tr[stopin].setStop (tstop) ;
      }

      //      Stop after
      else
	stopin = stopafter - 1 ;
    }
  }

  //
  //  Calculate the new length
  int newNumXTRs ;
  if ( zap ) {
    newNumXTRs = 1 ;
    empty = 1 ;
  }
  else
    newNumXTRs = stopin - startin + 1 ;

  //  No change in number of ranges: done
  if ( numXTRs == newNumXTRs ) {
    if ( zap )
      tr->resetRange (0.0, 0.0) ;
    setListRange () ;
    return ;
  }

  //
  //  Make a new set of ranges
  XTETimeRange *newXTR = new XTETimeRange[newNumXTRs] ;

  //
  //    Rearrange the ranges
  if ( !zap ) {
    //      Now copy the remaining ones
    int j=0 ;
    for (i=startin;i<=stopin;i++,j++)
      newXTR[j] = tr[i] ;
  }
  else
    newXTR->resetRange (0.0, 0.0) ;

  //
  //  Exchange the two lists
  delete [] tr ;
  tr = newXTR ;
  numXTRs = newNumXTRs ;
  setListRange () ;

  return ;
}

//
//   ------------------
// -- XTRList::orRange --
//   ------------------
//

// Description:
// "OR" in an extra XTETime Range
void XTRList::orRange (const XTETimeRange &T) {
  int startin=0, stopin=0 ;
  int startafter=0, stopafter=0 ;
  int before=0, after=0, between=0, straddle=0 ;
  int istart, istop ;
  int i ;
  double tstart = T.METStart () ;
  double tstop = T.METStop () ;

  //
  //  Handle the empties first
  //
  if ( T.isEmpty () )
    return ;
  if ( empty ) {
    numXTRs = 1 ;
    empty = 0 ;
    tr[0] = T ;
    listRange = T ;
    return ;
  }

  //
  //  First the trivial cases
  //
  if ( ( tstart <= listRange.METStart () ) &&
       ( tstop >= listRange.METStop () ) )
    straddle = 1 ;
  else if ( tstop < listRange.METStart () )
    before = 1 ;
  else if ( tstart > listRange.METStop () )
    after = 1 ;

  //
  //  See where the start and stop times fall in the existing list
  //  (add 1 to the indices)
  else {
    for (i=0;i<numXTRs;i++) {
      if ( !startin ) {
	istart = tr[i].isInRange (tstart) ;
	if ( !istart )
	  startin = i + 1 ;
	else if ( istart > 0 )
	  startafter = i + 1 ;
      }
      if ( !stopin ) {
	istop = tr[i].isInRange (tstop) ;
	if ( !istop )
	  stopin = i + 1 ;
	else if ( istop > 0 )
	  stopafter = i + 1 ;
      }
    }
    //
    //  Now figure out what to do
    //    Which range do we start in?
    //
    if ( startin ) {
      if ( startin == stopin )  // If we're stopping in the same one, return
	return ;
      startin -- ;              // Correct the index
    }

    //      In between
    else if ( !stopin && ( startafter == stopafter ) )
      between = stopafter ;

    //      Somebody's start time needs to be adjusted
    else {
      startin = startafter ;
      tr[startin].setStart (tstart) ;
    }

    //    Which range do we stop in?
    if ( stopin )
      stopin-- ;
    //      Somebody's stop time needs to be adjusted
    else
      if ( stopafter ) {
	stopin = stopafter - 1 ;
	tr[stopin].setStop (tstop) ;
      }
  }

  //
  //  The range list must now be non-empty
  empty = 0 ;

  //
  //  Calculate the new length
  int newNumXTRs ;
  if ( before + after + between )
    newNumXTRs = numXTRs + 1 ;
  else if ( straddle )
    newNumXTRs =  1;
  else
    newNumXTRs = numXTRs - stopin + startin ;

  //  No change in number of ranges: done
  if ( numXTRs == newNumXTRs ) {
    if ( straddle )
      tr[0] = T ;
    setListRange () ;
    return ;
  }

  //
  //  Make a new set of ranges
  XTETimeRange *newXTR = new XTETimeRange[newNumXTRs] ;

  //
  //    Extra range before
  if ( before ) {
    newXTR[0] = T ;
    for (i=0;i<numXTRs;i++)
      newXTR[i+1] = tr[i] ;
  }

  //
  //    Extra range after
  else if ( after ) {
    for (i=0;i<numXTRs;i++)
      newXTR[i] = tr[i] ;
    newXTR[numXTRs] = T ;
  }

  //
  //    Straddling range
  else if ( straddle )
    newXTR[0] = T ;

  //
  //    Extra range in between
  else if ( between ) {
    for (i=0;i<between;i++)
      newXTR[i] = tr[i] ;
    newXTR[between] = T ;
    for (i=between;i<numXTRs;i++)
      newXTR[i+1] = tr[i] ;
  }

  //
  //    Rearrange the ranges
  else {
    //      Cover the new part in a single range
    tr[stopin].setStart (tr[startin].METStart()) ;
    //      Now copy the remaining ones
    int j=0 ;
    for (i=0;i<startin;i++,j++)
      newXTR[j] = tr[i] ;
    for (i=stopin;i<numXTRs;i++,j++)
      newXTR[j] = tr[i] ;
  }

  //
  //  Exchange the two lists
  delete [] tr ;
  tr = newXTR ;
  numXTRs = newNumXTRs ;
  setListRange () ;

  return ;
}

//
//   -----------------------
// -- XTRList::setListRange --
//   -----------------------
//

// Description:
// Update the list range
void XTRList::setListRange (void) {
  int i, j, remove=0 ;

  if ( numXTRs )
    empty = 0 ;
  for (i=0; i<numXTRs; i++)
    if ( tr[i].isEmpty() )
      remove++ ;
  if ( remove ) {
    if ( remove >= numXTRs ) {
      numXTRs = 0 ;
      empty = 1 ;
    }
    else {
      XTETimeRange *newXTR = new XTETimeRange[numXTRs - remove] ;
      for (i=0, j=0; i<numXTRs; i++)
	if ( !tr[i].isEmpty() )
	  newXTR[j++] = tr[i] ;
      delete [] tr ;
      tr = newXTR ;
      numXTRs -= remove ;
      empty = 0 ;
    }
  }

  if ( !empty )
    listRange.resetRange (tr[0].METStart(), tr[numXTRs-1].METStop()) ;
  else
    listRange.resetRange (0.0, -1.0) ;
}
@

head	1.1;
access;
symbols;
locks; strict;
comment	@ * @;


1.1
date	2001.03.21.16.42.22;	author bob;	state Exp;
branches;
next	;


desc
@initial checkin after delivery by ehm. This is a modified version
of the SOF file of the same name.  This is used only by MIT.
@


1.1
log
@Initial revision
@
text
@// ==========================================================================
// File Name   : PktCCSDS.h
// Subsystem   : Data Ingest
// Programmer  : Mike Lijewski
// Description : Declaration of CCSDS packet
//
// .NAME    PktCCSDS - A class to define a generic CCSDS packet
// .LIBRARY pkts
// .HEADER  Data Ingest
// .INCLUDE PktCCSDS.h
// .FILE    PktCCSDS.C
// .VERSION $Revision: 4.2 $
//
// RCS: $Id: PktCCSDS.h,v 4.2 1997/06/19 15:35:08 soccm Exp $
//
// Old version before de Rogue Wavification.
// RCS: PktCCSDS.h,v 4.2 1997/06/19 15:35:08 soccm Exp 
//
// ==========================================================================

// .SECTION DESCRIPTION
// This class defines the interface to the CCSDS primary and secondary header.
// The Pkt CCSDS Class defines a packet with a CCSDS header
// as defined by document number 64-03007 with VERSION, TYPE,
// SEC HEADER FLAG, APPID, SEGFLAG, SEQUENCE and PACKET LENGTH
// fields.

// .SECTION AUTHOR
//  Steve Berczuk,
//  Jim Francis,
//  Mike Lijewski

#ifndef PKTCCSDS_H
#define PKTCCSDS_H
#include <iostream.h>

// Typedef for a error handling function taking a const char* argument.

typedef void (*PEHF)(const char*);

// Flags to define packet type.

enum PacketType { TELEMETRY = 0, COMMAND = 1 };

// Flags to indicate if secondary header is present.

enum SecondaryFlag { NOT_PRESENT = 0, PRESENT = 1 };

class PktCCSDS {

public:

  //* Constructors

  PktCCSDS(void);
  PktCCSDS(unsigned char* buffer);
  PktCCSDS(const PktCCSDS& pkt);

  //* Destructor

  virtual ~PktCCSDS(void);

  //* Public Member Functions

  PktCCSDS& operator=(const PktCCSDS& pkt);

  int operator==(const PktCCSDS& p) const;
  int operator!=(const PktCCSDS& p) const;

  const unsigned char* rawData(void) const;     

  virtual const unsigned char* applicationData(void) const; 

  const unsigned char* secondaryData(void) const;

  int applicationID(void) const;

  int sequenceNumber(void) const;

  int version(void) const;

  int packetLength(void) const;

  int secondaryHdrFlag(void) const;

  int segmentFlags(void) const;

  PacketType type(void) const;

  virtual void dumpHeader(ostream& os) const;

  virtual void dumpData(ostream& os) const ;

  //* Static Member Functions

  static int packetLength(const unsigned char* buffer);
  
  static int applicationID(const unsigned char* buffer);   

  static int version(const unsigned char* buffer);

  static PacketType type(const unsigned char* buffer);

  static int secondaryHdrFlag(const unsigned char* buffer);

  static int segmentFlags(const unsigned char* buffer);

  static int sequenceNumber(const unsigned char* buffer);

  static PEHF setErrorHandler(PEHF theFunc);

  enum {
    PrimaryHdrSize = 6 // Size of CCSDS primary header in bytes.
  };


protected:

  //* Protected Data Members

  static PEHF error;    // The default error function.

  unsigned char* buf;   // Packet buffer -- contains header and Data.
};

// Description:
// Returns the type of CCSDS packet.

inline PacketType PktCCSDS::type(void) const
{
  return (unsigned)((buf[0] & 0x010)) >> 4 == 0x00 ? TELEMETRY : COMMAND;
}

// Description:
// Returns the type of CCSDS packet.

inline PacketType PktCCSDS::type(const unsigned char* buffer)
{
  return (unsigned)((buffer[0] & 0x010)) >> 4 == 0x00 ? TELEMETRY : COMMAND;
}

// Description:
// This returns the packet length field from the CCSDS packets.
// The total length of the raw packet, including headers, is
// PacketLength + PrimaryHdrSize + 1

inline int PktCCSDS::packetLength(void) const
{
  return (buf[4]<<8) + buf[5];
}

// Description:
// This version of packetLength is meant to be used on a packet stored
// as bits in an unsigned char* buffer.

inline int PktCCSDS::packetLength(const unsigned char* buffer)
{
  return (buffer[4] << 8) + buffer[5];
}

// Description:
// Returns a pointer to the raw packet as read from telemetry..

inline const unsigned char* PktCCSDS::rawData(void) const
{
  return buf;
}

// Description:
// Returns a pointer to the secondary header in the telemetry packet.

inline const unsigned char* PktCCSDS::secondaryData (void) const
{
  return buf + PrimaryHdrSize;
}

// Description:
// Returns the version field in CCSDS primary header.

inline int PktCCSDS::version(void) const
{
  return (unsigned)((buf[0] & 0x0e0)) >> 5;
}

// Description:
// Returns the version field in CCSDS primary header.

inline int PktCCSDS::version(const unsigned char* buffer)
{
  return (unsigned)((buffer[0] & 0x0e0)) >> 5;
}

// Description:
// Returns the application ID in CCSDS primary header.

inline int PktCCSDS::applicationID(void) const
{
  return (buf[0] & 0x07) << 8 | buf[1];
}

// Description:
// Returns the application ID in CCSDS primary header.

inline int PktCCSDS::applicationID(const unsigned char* buffer)
{
  return (buffer[0] & 0x07) << 8 | buffer[1];
}

// Description:
// Returns true or false depending on whether or not the secondary header
// flag is set or not.  For the XTE mission this flag is always set to 1;
// that is, all XTE packets have secondary headers.

inline int PktCCSDS::secondaryHdrFlag(void) const
{
  return (unsigned)((buf[0] & 0x008)) >> 3 == 0x01;
}

// Description:
// Returns true or false depending on whether or not the secondary header
// flag is set or not.  For the XTE mission this flag is always set to 1;
// that is, all XTE packets have secondary headers.

inline int PktCCSDS::secondaryHdrFlag(const unsigned char* buffer)
{
  return (unsigned)((buffer[0] & 0x008)) >> 3 == 0x01;
}

// Description:
// Returns the segment flags from the CCSDS primary header.

inline int PktCCSDS::segmentFlags(void) const
{
  return (unsigned)((buf[2] & ~0x03f)) >> 6;
}

// Description:
// Returns the segment flags from the CCSDS primary header.

inline int PktCCSDS::segmentFlags(const unsigned char* buffer)
{
  return (unsigned)((buffer[2] & ~0x03f)) >> 6;
}

// Description:
// Returns the sequence number from the CCSDS primary header.

inline int PktCCSDS::sequenceNumber(void) const
{
  return (buf[2] & 0x03f) << 8 | buf[3];
}

// Description:
// Returns the sequence number from the CCSDS primary header.

inline int PktCCSDS::sequenceNumber(const unsigned char* buffer)
{
  return (buffer[2] & 0x03f) << 8 | buffer[3];
}

#endif /* PKTCCSDS_H */
@

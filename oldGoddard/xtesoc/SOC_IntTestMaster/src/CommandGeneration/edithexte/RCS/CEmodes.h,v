head	1.17;
access
	tgasaway
	mmarlowe
	rbentley
	dgruber
	vick
	socdev
	soccm;
symbols
	Build4_1:1.16
	Build4:1.16
	Build3_3:1.11
	Build3_2:1.5
	Build3_1:1.4
	Build3:1.1;
locks;
comment	@//@;


1.17
date	97.07.01.20.37.45;	author soccm;	state Exp;
branches;
next	1.16;

1.16
date	95.01.12.17.23.37;	author shalevy;	state Exp;
branches;
next	1.15;

1.15
date	95.01.10.01.27.48;	author shalevy;	state Exp;
branches;
next	1.14;

1.14
date	95.01.06.22.16.16;	author shalevy;	state Exp;
branches;
next	1.13;

1.13
date	94.12.01.23.09.02;	author shalevy;	state Exp;
branches;
next	1.12;

1.12
date	94.11.30.21.39.59;	author shalevy;	state Exp;
branches;
next	1.11;

1.11
date	94.10.28.18.17.42;	author shalevy;	state Exp;
branches;
next	1.10;

1.10
date	94.10.28.18.13.05;	author shalevy;	state Exp;
branches;
next	1.9;

1.9
date	94.10.28.18.09.44;	author shalevy;	state Exp;
branches;
next	1.8;

1.8
date	94.10.28.17.56.25;	author shalevy;	state Exp;
branches;
next	1.7;

1.7
date	94.10.14.16.14.00;	author shalevy;	state Exp;
branches;
next	1.6;

1.6
date	94.10.05.00.04.13;	author shalevy;	state Exp;
branches;
next	1.5;

1.5
date	94.09.13.16.48.28;	author shalevy;	state Exp;
branches;
next	1.4;

1.4
date	94.07.01.01.19.45;	author shalevy;	state Exp;
branches;
next	1.3;

1.3
date	94.07.01.00.47.16;	author shalevy;	state Exp;
branches;
next	1.2;

1.2
date	94.06.19.17.58.31;	author shalevy;	state Exp;
branches;
next	1.1;

1.1
date	94.04.28.20.31.27;	author shalevy;	state Exp;
branches;
next	;


desc
@@


1.17
log
@added RCS tag
@
text
@//
//  Programmer:  Shalom Halevy
//  File:        CEmodes.h
//  Subsystem:   Command Generation
//  Project:     XTE-HEXTE
//  Description: Configuration Editor - Science modes static data
//
//  RCS:  $Id: CEmodes.h,v 1.16 1995/01/12 17:23:37 shalevy Exp $
//  

#ifndef CEmodes_H
#define CEmodes_H


/*********************************************************************
 * List of HEXTE subsystems
*********************************************************************/
static TEXT *m0Systems[] = {
    "Science Mode and Table",
    "Aperture Modulation",
    "Burst List Trigger",
    "High Voltage Power Supply",
    "Set up DACs",
    "Set up individual DAC",
    "Set up AGCs",
    "Event Selection Logic",
    "Set Heater",
    "Set Test Pulser"
    };

#define NUM_SYS   (sizeof(m0Systems)/sizeof(m0Systems[0]))

static int m0SysId[NUM_SYS] = {
    0x0A,
    0x10,
    0x1A,
    0x1C,
    0x11,
    0x12,
    0x16,
    0x17,
    0x18,
    0x19
    };

static int nUser0[]={0, 1};
static int nUser1[]={0, 1, 2};
static int nUser2[]={0, 1, 2, 3};
static int nUser9[]={0, 1, 2, 3, 4, 5, 6, 7, 8, 9};


/*********************************************************************
 * List of HEXTE modes
*********************************************************************/
static TEXT *m0Tables[]={
    "Mode 0 - Idle",
    "Mode 1 - Events List",
    "Mode 2 - Histogram Bin",
    "Mode 3 - Multiscalar Bin",
    "Mode 4 - PHA/PSA",
    "Mode 5",
    "Mode 6 - Burst List",
    "Mode 7 - Memory Dump"
    };

static BOOL m0States[]=
    {1, 1, 1, 1, 1, 0, 1, 1};   // 1 - enable, 0 - disable

/*********************************************************************
 * Set Modulation tables
*********************************************************************/
static TEXT *s1PosList[] = {
     "Freeze present pos",
     "+/- 1.5 degree",
     "+/- 3.0 degree",
     "+ 1.5 degree",
     "- 1.5 degree",
     "+ 3.0 degree",
     "- 3.0 degree",
     "0 degree (+/- 1.5)",
     "0 degree (+/- 3.0)",
     "+ 1.5 degree",
     "- 1.5 degree",
     "+ 3.0 degree",
     "- 3.0 degree"
     };

// 0 - fixed, 1 - mod2, 2 - mod1
static int s1PosMod[]  = {0, 1, 1, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2};

static TEXT *s1DwellList[] = {
     "<none>",
     " 16 seconds",
     " 32 seconds",
     " 64 seconds",
     "128 seconds"
     };

/*********************************************************************
 * Mode 1 tables
*********************************************************************/

static TEXT *m0List[] = {
    "Idle",
    "Save",
    "Restore"
    };

static TEXT *m0ListCmnts[] = {
    "Idle",
    "Save Current Science\nMode and Table and\ngo to Idle",
    "Restore Science\nMode and Table"
    };

/*********************************************************************
 * Mode 1 tables
*********************************************************************/

static TEXT *m1Tables[] = {
    "ROM Table 1",
    "ROM Table 2",
    "ROM Table 3",
    "ROM Table 4",
    "ROM Table 5"
    };

static TEXT *m1TablesCmnts[] = {
    "PHA\nTime\t1.95 ms resolution\n  \trollover at 8s\nLost Events\nDetector ID",
    "Time\t7.5 us resolution\n  \trollover at 500ms",
    "PHA\nTime\t7.6 us resolution\n  \trollover at 16s\nLost Events\nDetector ID\nAGC Flag\nPSA\nShield Flags\nCsI Flag\nXULD Flag\nTPG Flag",
    "PHA\nTime\t500 ms resolution\n  \trollover at 16s\nLost Events\nDetector ID\nAGC Flag\nPSA",
    "PHA\nTime\t7.6 us resolution\n  \trollover at 256s\nLost Events\nDetector ID\nAGC Flag\nPSA\nShield Flags\nCsI Flag\nXULD Flag\nTPG Flag"
    };

// Ne objects
static int m1TablesNe[] = {0x0D, 0x06, 0x3F, 0x19, 0x7F};


/*********************************************************************
 * Mode 2 tables
*********************************************************************/

static TEXT *m2Tables[] = {
    "ROM Table 1",
    "ROM Table 2",
    "ROM Table 3",
    "ROM Table 4"
    };

static TEXT *m2TablesCmnts[] = {
    "16 integrations/IDF\n256 PHA bins per histogram\nSummed histogram\n8 bit depth per PHA bin\nMax energy PHA bin is 256",
    "4 integrations/IDF\n256 PHA bins per histogram\nIndividual histograms\n8 bit depth per PHA bin\nMax energy PHA bin is 256",
    "8 integrations/IDF\n256 PHA bins per histogram\nSummed histogram\n16 bit depth per PHA bin\nMax energy PHA bin is 256",
    "2 integrations/IDF\n256 PHA bins per histogram\nIndividual histograms\n16 bit depth per PHA bin\nMax energy PHA bin is 256"
    };

typedef struct {
    int Na,Nb,Nc,Nd,Nf;
    } MODE2DATA;

static MODE2DATA m2TablesData[] = {
    {4, 8, 0, 1, 8},
    {2, 8, 2, 1, 8},
    {3, 8, 0, 2, 8},
    {1, 8, 2, 2, 8}
    };

/*********************************************************************
 * Mode 3 tables
*********************************************************************/

static TEXT *m3Tables[] = {
    "ROM Table 1",
    "ROM Table 2",
    "ROM Table 3",
    "ROM Table 4",
    "ROM Table 5"
    };

static TEXT *m3TablesCmnts[] = {
   "128 time bins (0.125 s per bin)\n7 PHA Ranges\nOne LC for each detector\n8 bit depth\nLost Events added",
   "512 time bins (31.25 ms per bin)\n7 PHA Ranges\nOne LC for all detectors\n8 bit depth\nLost Events added",
   "2048 time bins (7.8 ms per bin)\n3 PHA Ranges\nOne LC for all detectors\n4 bit depth\nLost Events added",
   "16384 time bins (0.98 ms per bin)\n1 PHA Range\nOne LC for all detectors\n4 bit depth\nLost Events added",
   "128 time bins (0.125s per bin)\n3 PHA Ranges\nOne LC for each detector\n16 bit depth\nLost Events added"
   };

typedef struct {
    int Na,Nb,Nc,Nd,Ng,Nh[9];
    } MODE3DATA;

static MODE3DATA m3TablesData[] = {
   { 7, 7, 2, 1, 1, {15,22,33,50,75,112,168,250}},
   { 9, 7, 0, 1, 1, {15,22,33,50,75,112,168,250}},
   {11, 3, 0, 0, 1, {15,38,98,250}},
   {14, 1, 0, 0, 1, {15,250}},
   { 7, 3, 2, 2, 1, {15,38,98,250}}
   };

#define MODE3SIZE (sizeof(m3TablesData)/sizeof(m3TablesData[0]))

typedef struct {
   MODE3DATA Data;
   int Nhi;     // currently selected range
   int Nh_sel;  // currently selected limit
   int NhDirty; // dirty flag
   } MODE3CONFIG;


/*********************************************************************
 * Mode 4 tables
*********************************************************************/

// From dgruber@@cass142.UCSD.EDU Fri Oct 22 11:52 PDT 1993
// Date: Fri, 22 Oct 93 11:49:48 PDT
// From: dgruber@@cass142.UCSD.EDU (Duane Gruber)
// To: shalevy@@cass140.UCSD.EDU
// Subject: telemetry mode 4 properties

// I have taken a look at some actual data in the two tables for
// mode 4.  It requires some interpretation, but I think information is there
// that defines the properties of these two modes, which we should use.  
// In order to accomodate
// this, the configuration object needs to have one more item, psaChans.
// This has the value 2^Nf, and is 64 for either table. 

// I have changed the data in my file .../CmdGen/ConfigObject.build2
// to reflect this change.

// Mode 4 gives us three more complications:
//    1) for table 1, there are 256 energy channels, or the value 256 in phaChans, but
//       for table 2, there are 8 energy bands, or the value 8 in phaBands, and the following
//       9 values in the array phaEdges: 10,14,21,32,49,74,111,167,255.
//       Note that 4-1 bins energy like mode 2, but 4-2 does it like mode 3.
//    2) for table 2 the value in Nc, currently 0 in the data, is erroneous.  It should
//       actually be 2.  I have asked for a correction in the flight software.
//    3) Tom tells me that mode 4 table 1 will be expanded into 4 different rom tables, and
//       that for these the parameter Nc will represent the detector Id, not the number
//       of detectors, as it currently does.  This is not a concern for build 2.

// Duane

static TEXT *m4Tables[] = {
    "ROM Table 1",
    "ROM Table 2",
    "ROM Table 3",
    "ROM Table 4",
    "ROM Table 5"
    };

static TEXT *m4TablesCmnts[] = {
   "Detector 1",
   "Detector 2",
   "Detector 3",
   "Detector 4",
   "Summed detectors"
   };

//   Na,Nb,Nc,Nd,Nf
static MODE2DATA m42TablesData[] = {  // mode 2 look alike
    {4, 8, 0, 1, 6},
    {4, 8, 1, 1, 6},
    {4, 8, 2, 1, 6},
    {4, 8, 3, 1, 6}
    };

//   Na,Nb,Nc,Nd,Ng,Nh[9]
static MODE3DATA m43TablesData[] = {  // mode 4 look alike
   {4, 8, 0, 2, 0, {10,14,21,32,49,74,111,167,255}}
   };


/*********************************************************************
 * HVPS mnemonics
*********************************************************************/

static struct HVPS_tag {
    char *cmnd;
    int   count;
    int   cur;
    }  HVPS_data[] = {
    {"A",  4,   0},
    {"P",  4,   4},
    {"S",  5,   8},
    {"M",  1,   13}
    };

#endif // CEmodes_H
@


1.16
log
@Fixed mnemonics
@
text
@d8 1
a8 1
//  RCS:  $Id: CEutils.h,v 1.1 1993/10/22 10:36:39 shalevy Exp shalevy $
a9 5
//  $Log: HEXTECEmodes.h,v $
// * Revision 1.1  1993/10/22  10:36:39  shalevy
// * Initial revision
// *
//
@


1.15
log
@*** empty log message ***
@
text
@d8 1
a8 1
//  RCS:  $Id: CEmodes.h,v 1.14 1995/01/06 22:16:16 shalevy Exp $
d10 1
a10 4
//  $Log: CEmodes.h,v $
//Revision 1.14  1995/01/06  22:16:16  shalevy
//Added support for HVPS commands
//
d282 1
a282 1
struct HVPS_tag {
a291 45

static char *HVPS_Ctls[] = {
    "hvCtlDet1",
    "hvCtlDet2",
    "hvCtlDet3",
    "hvCtlDet4",

    "hvCtlGcDet1",
    "hvCtlGcDet2",
    "hvCtlGcDet3",
    "hvCtlGcDet4",

    "hvCtlShd1",
    "hvCtlShd2",
    "hvCtlShd3",
    "hvCtlShd4",
    "hvCtlShd5",

    "hvCtlPmon",
    };

static char *HVPS_Cmnds[] = {
    "cmdHvDet1",
    "cmdHvDet2",
    "cmdHvDet3",
    "cmdHvDet4",

    "cmdHvGcDet1",
    "cmdHvGcDet2",
    "cmdHvGcDet3",
    "cmdHvGcDet4",

    "cmdHvShd1",
    "cmdHvShd2",
    "cmdHvShd3",
    "cmdHvShd4",
    "cmdHvShd5",

    "cmdHvPmon",
    };

static char *HVPS_Saa[] = {
    "loThreshPmon",
    "hiThreshPmon",
    };
@


1.14
log
@Added support for HVPS commands
@
text
@d8 1
a8 1
//  RCS:  $Id: CEutils.h,v 1.1 1993/10/22 10:36:39 shalevy Exp shalevy $
d10 4
a13 1
//  $Log: HEXTECEmodes.h,v $
@


1.13
log
@Integration with Build4 libhcg
@
text
@d277 1
d279 1
a279 1
 * DACS mnemonics
a280 1
#if 0   // now part of libhcg
d282 9
a290 28
static char *DACS_mnemonics[] = {
    "cmdLldDet1",
    "cmdLldDet2",
    "cmdLldDet3",
    "cmdLldDet4",

    "cmdLsdDet1",
    "cmdLsdDet2",
    "cmdLsdDet3",
    "cmdLsdDet4",

    "cmdUsdDet1",
    "cmdUsdDet2",
    "cmdUsdDet3",
    "cmdUsdDet4",

    "cmdLldShd1",
    "cmdLldShd2",
    "cmdLldShd3",
    "cmdLldShd4",
    "cmdLldShd5",

    "cmdLldGcDet1",
    "cmdLldGcDet2",
    "cmdLldGcDet3",
    "cmdLldGcDet4",

    "cmdLldPmon"
a291 2
#endif

d293 10
a302 3
/*********************************************************************
 * HVPS mnemonics
*********************************************************************/
a303 1
static char *HVPS_mnemonics[] = {
d310 22
a331 31
    "hvShd1",
    "hvShd2",
    "hvShd3",
    "hvShd4",
    "hvShd5",

    "hvShd1_TU",
    "hvShd2_TU",
    "hvShd3_TU",
    "hvShd4_TU",
    "hvShd5_TU",

    "hvDet1",
    "hvDet2",
    "hvDet3",
    "hvDet4",

    "hvDet1_TU",
    "hvDet2_TU",
    "hvDet3_TU",
    "hvDet4_TU",

    "hvGcDet1",
    "hvGcDet2",
    "hvGcDet3",
    "hvGcDet4",

    "hvGcDet1_TU",
    "hvGcDet2_TU",
    "hvGcDet3_TU",
    "hvGcDet4_TU",
d333 1
a335 4

    "hvCtlPmon",
    "hvPmon",
    "hvPmon_TU"
a337 1

@


1.12
log
@*** empty log message ***
@
text
@d280 1
d311 1
@


1.11
log
@*** empty log message ***
@
text
@d8 1
a8 1
//  RCS:  $Id: CEmodes.h,v 1.10 1994/10/28 18:13:05 shalevy Exp $
d10 1
a10 4
//  $Log: CEmodes.h,v $
//Revision 1.10  1994/10/28  18:13:05  shalevy
//*** empty log message ***
//
d277 87
@


1.10
log
@*** empty log message ***
@
text
@d8 1
a8 1
//  RCS:  $Id: CEutils.h,v 1.1 1993/10/22 10:36:39 shalevy Exp shalevy $
d10 4
a13 1
//  $Log: HEXTECEmodes.h,v $
@


1.9
log
@*** empty log message ***
@
text
@d8 1
a8 1
//  RCS:  $Id: CEmodes.h,v 1.8 1994/10/28 17:56:25 shalevy Exp $
d10 1
a10 4
//  $Log: CEmodes.h,v $
//Revision 1.8  1994/10/28  17:56:25  shalevy
//*** empty log message ***
//
@


1.8
log
@*** empty log message ***
@
text
@d8 1
a8 1
//  RCS:  $Id: CEutils.h,v 1.1 1993/10/22 10:36:39 shalevy Exp shalevy $
d10 4
a13 1
//  $Log: HEXTECEmodes.h,v $
@


1.7
log
@Build 3_3 with the new HEXTE configuration file and the GSE
commanding interface.
@
text
@@


1.6
log
@Added support for the new libhcm/libhcg
@
text
@@


1.5
log
@*** empty log message ***
@
text
@d36 15
d74 30
@


1.4
log
@*** empty log message ***
@
text
@d8 1
a8 1
//  RCS:  $Id: CEmodes.h,v 1.3 1994/07/01 00:47:16 shalevy Exp $
d10 1
a10 4
//  $Log: CEmodes.h,v $
//Revision 1.3  1994/07/01  00:47:16  shalevy
//Build3_1 code
//
d59 16
@


1.3
log
@Build3_1 code
@
text
@d8 1
a8 1
//  RCS:  $Id: CEutils.h,v 1.1 1993/10/22 10:36:39 shalevy Exp shalevy $
d10 4
a13 1
//  $Log: HEXTECEmodes.h,v $
@


1.2
log
@*** empty log message ***
@
text
@d8 1
a8 1
//  RCS:  $Id: CEmodes.h,v 1.1 1994/04/28 20:31:27 shalevy Exp $
d10 1
a10 4
//  $Log: CEmodes.h,v $
//Revision 1.1  1994/04/28  20:31:27  shalevy
//Initial revision
//
@


1.1
log
@Initial revision
@
text
@d8 1
a8 1
//  RCS:  $Id: CEutils.h,v 1.1 1993/10/22 10:36:39 shalevy Exp shalevy $
d10 4
a13 1
//  $Log: HEXTECEmodes.h,v $
@

#include <stdio.h>
#include <gl.h>
#include <device.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#define DEBUG
#define NX 256
#define NY 256
#define X0 (1279-511)
#define XC 1280
#define Y0 0
#define YC 511

float vlist[NY][NX][3];
float nlist[NY][NX][3];
short clist[NY][NX][3];

short wrred = 0xffff, wrgrn = 0xffff, wrblu = 0xffff;
static float mat[] = { AMBIENT, .2, .2, .2,
	      DIFFUSE, .5, .5, .5,
	      SPECULAR, .1, .1, .1,
	      SHININESS, 16.,
	      LMNULL
	     };
static float lm[] = { AMBIENT, .2, .2, .2,
		      LOCALVIEWER, 0,
		      LMNULL
		    };
static float lttop[] = { LCOLOR, 1, 1, 1,
		      POSITION, 0.0, 0.0, 1.0, 0,
		      LMNULL
		    };
static float ltbot[] = { LCOLOR, 1, 1, 1,
		      POSITION, 1.0, 1.0, 1.0, 0,
		      LMNULL
		    };
static char title[]= "Laser Perspective\n";

main(argc,argv)
int argc;
char *argv[]; /* assume list of filenames separated by spaces */
{
  float dist=0.0, view_ang=0.0, azim_frmy=90.0, scale=0.2, elev=0.0;
  int i=0, win, cnt=0;
  long skip=0;
  long val;
  char *infile;
  FILE *fp;
  char *file[99999];
   
  file[cnt] = malloc(80);
  while( (gets(file[cnt]) != NULL ) )
  {
     cnt++;
     file[cnt] = malloc(80);
  }

  if( argc > 1 )
  {
    skip = 35*atoi(argv[1]);
    argc--;
    argv++;
  }
  else
  {
    printf("usage: animlook view_spec_file < file_list_file\n");
    exit(0);
  }
  if( argc > 1 )
  {
    infile = strdup(argv[1]);
    printf("reading viewing specs. from file: %s\n",infile);
    fp = fopen(infile,"r");
    if( fp == NULL ){ printf("unable to open %s\n",infile); exit(0); }
    fseek(fp,skip,SEEK_SET);
    argc--;
    argv++;
  }
  else
  {
    printf("usage: animlook view_spec_file file1 file2 file3 ...\n");
    exit(0);
  }
   prefposition(X0,XC,Y0,YC);
   win = winopen(title);

   RGBmode();
   RGBwritemask(wrred,wrgrn,wrblu);
   drawmode(NORMALDRAW);

/*   doublebuffer(); */
   gconfig();

   qdevice(LEFTMOUSE);
   qdevice(RIGHTMOUSE);
   qdevice(MIDDLEMOUSE);
   backface(TRUE); 

   lsetdepth(0,0x7fffff);
   zbuffer(TRUE);
   lRGBrange(0x0040,0x0040,0x0040,0xffff,0xffff,0xffff,0,0x7fffff);
   depthcue(FALSE);

   mmode(MVIEWING);
   lmdef(DEFMATERIAL,1,0,mat);
   lmdef(DEFLMODEL,1,0,lm);
   lmdef(DEFLIGHT,1,0,lttop);
   lmdef(DEFLIGHT,2,0,ltbot);
   lmbind(MATERIAL,1);
   lmbind(LMODEL,1);
   lmbind(LIGHT0,1);
/*   lmbind(LIGHT1,2); */

   polymode(PYM_FILL); 

   while( i <= cnt )
   {
     fscanf(fp,"%f %f %f %f %f",&dist,&scale,&view_ang,&azim_frmy,&elev);
     set_mesh(file[i],scale);
     cpack(0x00);
     clear();
     zclear();
     draw_surf(dist,view_ang,azim_frmy,elev);
     rec_frame(file[i]);
     i++;
   }
   fclose(fp);
}

int draw_surf(dist,view_ang,azim,elev)
float dist, elev, view_ang, azim;
{
  int i, j, fov=450, inclin_frm_z, azim_frm_y= -900, first=1;
  static float ident[4][4] = { {1.0, 0.0, 0.0, 0.0}, 
                             {0.0, 1.0, 0.0, 0.0}, 
                             {0.0, 0.0, 1.0, 0.0}, 
                             {0.0, 0.0, 0.0, 1.0} };
  
/*  if( first )
  {
*/
    inclin_frm_z = 10*view_ang; 
    azim_frm_y = azim_frm_y + 10*azim;
    dist = 3.0 - dist;   
    loadmatrix(ident);
    reshapeviewport();
    perspective(fov,1.0,0.001,100.0);
    polarview(dist,azim_frm_y,inclin_frm_z,0);
    translate(0.0,0.0,-elev);
/*  } */

  lmcolor(LMC_AD); 
  for( j = 0; j < NY-1; j++ )
  {
  bgntmesh();
    for( i = 0; i < NX; i++ )
    {
      c3s( clist[j][i] ); n3f( nlist[j][i] ); v3f( vlist[j][i] );
      c3s( clist[j+1][i] ); n3f( nlist[j+1][i] ); v3f( vlist[j+1][i] );
    }
  endtmesh();
  }
  lmcolor(LMC_COLOR); 

/*  if( first ) first = 0; */
  return;
}

int set_mesh(file,scale)
char *file;
float scale;
{
  int fd, i, j, k=0;
  unsigned char data[NX*NY];

  if( (fd = open(file,0)) <= 0 )
  {
    printf("Unable to open: %s\n",file);
    exit(0);
  }
  read(fd,data,NX*NY);
 
  for( i = 0; i < NY; i++ )
  {
    for( j = 0; j < NX; j++, k++ )
    {
      vlist[i][j][0] = 2.0*i/NY - 1.0;
      vlist[i][j][1] = 2.0*j/NX - 1.0;
      vlist[i][j][2] = scale*(data[k] / 255.0);
          
      clist[i][j][0] = 64 + data[k]/4;
      clist[i][j][1] = 32 + data[k]/2;
      clist[i][j][2] = data[k];
    }
  }
  for( i = 0; i < NY-1; i++ )
   for( j = 0; j < NX-1; j++ )
     vert_norm(nlist[i][j],vlist[i][j],vlist[i][j+1],vlist[i+1][j]);

  close(fd);
  return;
}


vert_norm(norm,v0,vl,vr)
float norm[3], v0[3], vl[3], vr[3];
{
  float mag;

  norm[0] = (vr[1] - v0[1]) * (vl[2] - v0[2]) -
		(vr[2] - v0[2]) * (vl[1] - v0[1]);
 
  norm[1] = (vr[2] - v0[2]) * (vl[0] - v0[0]) - 
		(vr[0] - v0[0]) * (vl[2] - vl[2]);

  norm[2] = (vr[0] - v0[0]) * (vl[1] - v0[1]) -
		(vr[1] - v0[1]) * (vl[0] - v0[0]);

  mag = sqrt(norm[0]*norm[0] + norm[1]*norm[1] + norm[2]*norm[2]);

  norm[0] = norm[0]/mag;
  norm[1] = norm[1]/mag;
  norm[2] = norm[2]/mag;
}

rec_frame(infile)
char *infile;
{
  static unsigned long parr[512*512];
  static unsigned char buff[512*512];
  char outfile[80];
  int i, fd;
  long nl;

  strcpy(outfile,infile);
  strcat(outfile,".3D");
  if( (fd = creat(outfile,0644)) <= 0 ) printf("unable to open output: %s\n",outfile);

  pixmode(PM_SIZE,32);
  readsource(SRC_AUTO);
  if( (nl = lrectread(0,0,511,511,parr)) < 512*512 ) printf("only read: %d\n",nl);
  for( i=0; i < 512*512; i++ )
    buff[i] = parr[i] / 256 / 256;  

  nl = write(fd,buff,512*512);
  close(fd);
}
 
  

  

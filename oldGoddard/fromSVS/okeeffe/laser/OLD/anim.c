#include <stdio.h>
#include <gl.h>
#include <device.h>
#include <math.h>
#include <stdlib.h>

#define DEBUG
#define NX 256
#define NY 256
float vlist[NY][NX][3];
float nlist[NY][NX][3];
short clist[NY][NX][3];

short wrred = 0xffff, wrgrn = 0xffff, wrblu = 0xffff;
static float mat[] = { AMBIENT, .2, .2, .2,
	      DIFFUSE, .5, .5, .5,
	      SPECULAR, .5, .5, .5,
	      SHININESS, 64,
	      LMNULL
	     };
static float lm[] = { /* TWOSIDE, 1, */
		      AMBIENT, .2, .2, .2,
		      LOCALVIEWER, 0,
		      LMNULL
		    };
static float lttop[] = { LCOLOR, 1, 1, 1,
		      POSITION, 0, 1.0, 0, 0,
		      LMNULL
		    };
static float ltbot[] = { LCOLOR, 1, 1, 1,
		      POSITION, 0, -1.0, 0, 0,
		      LMNULL
		    };
static char title[]= "Laser Perspective\n";

main(argc,argv)
int argc;
char *argv[]; /* assume list of filenames separated by spaces */
{
  float elev=1.0, view_ang=0.0;
  int i=1, win;
  long val;

  if( argc > 1 )
  {
    elev = atof(argv[1]);
    argc--;
    argv++;
  }
  else
  {
    printf("usage: view elev angle file1 file2 file3 ...\n");
    exit(0);
  }
  if( argc > 1 )
  {
    view_ang = atof(argv[1]);
    argc--;
    argv++;
  }
  else
  {
    printf("usage: view elev angle file1 file2 file3 ...\n");
    exit(0);
  }

   prefposition(1279-511,1279,0,511);
   win = winopen(title);

   RGBmode();
   RGBwritemask(wrred,wrgrn,wrblu);
   drawmode(NORMALDRAW);

  
/*   doublebuffer(); */
   gconfig();

   qdevice(LEFTMOUSE);
   qdevice(RIGHTMOUSE);
   qdevice(MIDDLEMOUSE);
   backface(TRUE); 

   lsetdepth(0,0x7fffff);
   zbuffer(TRUE);
   lRGBrange(0x0040,0x0040,0x0040,0xffff,0xffff,0xffff,0,0x7fffff);
   depthcue(FALSE);


   mmode(MVIEWING);
   lmdef(DEFMATERIAL,1,0,mat);
   lmdef(DEFLMODEL,1,0,lm);
   lmdef(DEFLIGHT,1,0,lttop);
   lmdef(DEFLIGHT,2,0,ltbot);
   lmbind(MATERIAL,1);
   lmbind(LMODEL,1);
   lmbind(LIGHT0,1);
   lmbind(LIGHT1,2);

/*   polymode(PYM_FILL); */

   while( i < argc )
   {
     set_mesh(argv[i++]);
     cpack(0x00);
     clear();
     zclear();
     draw_surf(elev,view_ang);
     if( argc == 2 ) qread(&val);
   }
}

int draw_surf(elev,view_ang)
float elev, view_ang;
{
  float dist;
  int i, j, fov=450, inclin_frm_z, azim_frm_y=900, first=1;
  static float ident[4][4] = { {1.0, 0.0, 0.0, 0.0}, 
                             {0.0, 1.0, 0.0, 0.0}, 
                             {0.0, 0.0, 1.0, 0.0}, 
                             {0.0, 0.0, 0.0, 1.0} };
  
  if( first )
  {
    inclin_frm_z = 10*view_ang; 
    dist = 3.0 + elev;   
    loadmatrix(ident);
    reshapeviewport();
    perspective(fov,1.0,0.001,100.0);
    polarview(dist,azim_frm_y,inclin_frm_z,0);
  }

/*  lmcolor(LMC_AD); */
  bgntmesh();
  for( j = 0; j < NY-1; j++ )
  {
    for( i = 0; i < NX; i++ )
    {
      /* c3s( clist[j][i] ); */n3f( nlist[j][i] ); v3f( vlist[j][i] );
      /* c3s( clist[j+1][i] ); */n3f( nlist[j+1][i] ); v3f( vlist[j+1][i] );
    }
  }
  endtmesh();
/*  lmcolor(LMC_COLOR); */ 

  if( first ) first = 0; 
  return;
}

int set_mesh(file)
char *file;
{
  int fd, i, j, k=0;
  float scale=0.2;
  unsigned char data[NX*NY];

  if( (fd = open(file,0)) <= 0 )
  {
    printf("Unable to open: %s\n",file);
    exit(0);
  }
  read(fd,data,NX*NY);
 
  for( i = 0; i < NY; i++ )
  {
    for( j = 0; j < NX; j++, k++ )
    {
      vlist[i][j][0] = 2.0*i/NY - 1.0;
      vlist[i][j][1] = 2.0*j/NX - 1.0;
      vlist[i][j][2] = scale*(data[k] / 255.0);
          
      clist[i][j][0] = 64 + data[k]/4;
      clist[i][j][1] = 32 + data[k]/2;
      clist[i][j][2] = data[k];
    }
  }
  for( i = 0; i < NY-1; i++ )
   for( j = 0; j < NX-1; j++ )
     vert_norm(nlist[i][j],vlist[i][j+1],vlist[i][j],vlist[i+1][j]);

  return;
}


vert_norm(norm,v0,vl,vr)
float norm[3], v0[3], vl[3], vr[3];
{
  float mag;

  norm[0] = (vr[1] - v0[1]) * (vl[2] - v0[2]) -
		(vr[2] - v0[2]) * (vl[1] - v0[1]);
 
  norm[1] = (vr[2] - v0[2]) * (vl[0] - v0[0]) - 
		(vr[0] - v0[0]) * (vl[2] - vl[2]);

  norm[2] = (vr[0] - v0[0]) * (vl[1] - v0[1]) -
		(vr[1] - v0[1]) * (vl[0] - v0[0]);

  mag = sqrt(norm[0]*norm[0] + norm[1]*norm[1] + norm[2]*norm[2]);

  norm[0] = norm[0]/mag;
  norm[1] = norm[1]/mag;
  norm[2] = norm[2]/mag;
}
  

#include <stdio.h>
#include <string.h>
#include <math.h>
/*
#include <gl.h> -- 3.1 change for 3.2
*/
#include <gl/gl.h>
#include <device.h>

#define xmax 1024
#define ymax 1024

main(argc,argv)
int argc; char **argv;
{
   unsigned char data[xmax];
   long scan[xmax], tmp;
   char *zfile;
   int nr, ng, nb, fd;
   float exec_time;
   int i, j, npix, nscan, ti, tf;
   unsigned short wrred = 0xffff, wrgrn = 0xffff, wrblu = 0xffff;
   if( argc > 1 ) 
   {
      zfile = strdup(argv[1]);
   }
   else
   {
      zfile = (char *) malloc(80);
      for( i = 0; i < 80; i++) zfile[i] = '\n';
      printf("Enter input filename: ");
      gets(zfile);
   }
   if( argc > 2 )
	npix = atoi(argv[2]);
   else
	npix = xmax;
   if( argc > 3 )
	nscan = atoi(argv[3]);
   else
	nscan = ymax;

   ti = time(0);
   
 
   prefsize(npix,nscan);
   winopen(zfile);
   RGBmode();
   RGBwritemask(wrred,wrgrn,wrblu);
   gconfig();
   cpack(0); clear();

   fd = open(zfile,0);
   for( i = 0; i < nscan; i++ )
   {
     nr = read(fd,data,npix);
     for( j = 0; j < npix; j++ ) 
       scan[j] = (long) data[j] << 8;
/*       scan[j] = (long) data[j] << 16; 
       scan[j] = (long) data[j]; */
     lrectwrite((Screencoord)0,(Screencoord)(nscan-i),(Screencoord)(npix-1),
			(Screencoord)(nscan-i),scan);
   }
   close(fd);

   while( TRUE )
   { 
     if( qread(&tmp) == REDRAW )
     {
	fd = open(zfile,0);
	for( i = 0; i < nscan; i++ )
	{
	  nr = read(fd,data,npix);
	  for( j = 0; j < npix; j++ ) 
	  scan[j] = (long) data[j];
          lrectwrite((Screencoord)0,(Screencoord)(nscan-i),(Screencoord)(npix-1),
			(Screencoord)(nscan-i),scan);
        }
        close(fd);
     }
   }
   
}


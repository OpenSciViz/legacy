#include <stdio.h>
#include <gl.h>

drwrld_(xlat_north, xlon_west, xlat_south, xlon_east)
float xlat_north, xlon_west, xlat_south, xlon_east;
/* dummy arguments for now */

{
int   npts, igid;
int   zero_cnt = 0;
float latlon[1000][2];
float llpair[2]; /* swap lat/lon values before using v2f */
float range[4];
int   nbytes;
int   i;

int mapfile;

/* Open the input worldmap dataset */
if ( (mapfile = open("/usr/people/merritt/proj/worldmap/ezmap_iris.data",0)) == -1)
{
	printf("Can't open input map dataset\n");
	exit(-1);
}

/* 
 * The organization of the map datasets is:
 *  1. There is a word specifying the number of full words to expect
 *  2. Each pair of lat/lon is within a word and is I*2 scaled by 100
 *  3. These pairs constitute a contigious line segment of the map
 */


nbytes = 1; /* to get into the while loop */
while (nbytes > 0)
{
	if((nbytes = read(mapfile, &npts, 4)) < 4) break;
	if((nbytes = read(mapfile, &igid, 4)) < 4) break;
	if((nbytes = read(mapfile, range, 16))< 16) break;
	nbytes = npts * 4;
	if((nbytes = read(mapfile, latlon, nbytes))< nbytes) break;
	if (igid == 0) zero_cnt++;
	if (zero_cnt < 2) continue;
	if (zero_cnt == 3) break;
	bgnline();
	for (i=0; i<(npts/2); i++)
	{
		llpair[0] = latlon[i][1];
		llpair[1] = latlon[i][0];
		v2f(llpair);
	}
	endline();
}
/* Close the map dataset */
close(mapfile);

return;
}

  

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <gl/gl.h>
#include <device.h>
#include <malloc.h>

#define xmax 1024
#define ymax 1024

main(argc,argv)
int argc; char **argv;
{
   unsigned char red[xmax*ymax], green[xmax*ymax], blue[xmax*ymax];
   char *rfile, *bfile, *gfile;
   int nr, ng, nb, fd_red, fd_green, fd_blue, cnt=0;
   float exec_time, zoom=1.0;
   int i=0, j, npix=512, nscan=512, invert=0, sz;
   long tmp, *scan, *img;
   unsigned short wrred = 0xffff, wrgrn = 0xffff, wrblu = 0xffff;
   char *console = getenv("HOSTNAME");
   char *VGX = getenv("VGX_OPTION");

/*
   if( strcmp(console,"okeeffe") )
   {
      printf("sorry, this version of rgbput only works on okeeffe!\n");
      exit(0);
   }
*/   
   rfile = malloc(80);
   gfile = malloc(80);
   bfile = malloc(80);

   if( argc > 1 )
     if( strcmp(argv[1],"-h") == 0 )
     {
       zoom = 2.0;
       argv++;
       argc--;
     }
   if( argc > 1 )
     if( strcmp(argv[1],"-i") == 0 )
     {
       invert=1;
       argv++;
       argc--;
     }
  
   if( argc <= 2 )
   {
     strcpy(rfile,argv[1]);
     strncpy(gfile,argv[1],strlen(rfile)-3);
     strcat(gfile,"grn");
     strncpy(bfile,argv[1],strlen(rfile)-3);
     strcat(bfile,"blu");
   }
   else if( argc <= 3 )
   {
     strcpy(rfile,argv[1]);
     strcpy(gfile,argv[2]);
     strncpy(bfile,argv[1],strlen(rfile)-3);
     strcat(bfile,"blu");
   }
   else 
   {
     strcpy(rfile,argv[1]);
     strcpy(gfile,argv[2]);
     strcpy(bfile,argv[3]);
   }
   
   if( zoom == 2.0 ) 
     prefsize(2*npix,2*nscan);
   else 
     prefsize(npix,nscan);

   winopen("RGB PUT  ");
   RGBmode();
   gconfig();
   if( zoom > 1.0 ) rectzoom(zoom,zoom);
   cpack(0); clear();
   qdevice(LEFTMOUSE);


   fd_red = open(rfile,0);
   fd_green = open(gfile,0);
   fd_blue = open(bfile,0);	
   nr = read(fd_red,red,npix*nscan);
   ng = read(fd_green,green,npix*nscan);
   nb = read(fd_blue,blue,npix*nscan);
   close(fd_red);
   close(fd_green); 
   close(fd_blue);

   if( VGX != NULL )
   {
     pixmode(PM_SIZE,8);
     if( invert ) pixmode(PM_TTOB,1);

     wrred = 0xffff; wrgrn = 0x0; wrblu = 0x0;
     RGBwritemask(wrred,wrgrn,wrblu);
     pixmode(PM_SHIFT,0); /* red */
     lrectwrite((Screencoord)(0),(Screencoord)(0),(Screencoord)(npix-1),
			(Screencoord)(nscan-1),red);

     wrred = 0x0; wrgrn = 0xffff; wrblu = 0x0;
     RGBwritemask(wrred,wrgrn,wrblu);
     pixmode(PM_SHIFT,8); /* green */
     lrectwrite((Screencoord)(0),(Screencoord)(0),(Screencoord)(npix-1),
			(Screencoord)(nscan-1),green);

     wrred = 0x0; wrgrn = 0x0; wrblu = 0xffff;
     RGBwritemask(wrred,wrgrn,wrblu);
     pixmode(PM_SHIFT,16); /* blue */
     lrectwrite((Screencoord)(0),(Screencoord)(0),(Screencoord)(npix-1),
			(Screencoord)(nscan-1),blue);
   }
   else
   {
     img = (long *) malloc(4*npix*nscan);
     if( invert )
     {
       for( i = 0; i < nscan; i++)
       {
         scan = &img[i*npix];
         for( j = 0; j < npix; j++, cnt++, scan++ ) 
         {
           *scan = (long) red[cnt];
           tmp = (long) green[cnt];
           tmp = tmp << 8;
           *scan = *scan + tmp;
           tmp = (long) blue[cnt];
           tmp = tmp << 16;
           *scan = *scan + tmp;
         }
       }
     }
     else
     {
       for( i = nscan; i > 1; i--)
       {
         scan = &img[(i-2)*npix];
         for( j = 0; j < npix; j++, cnt++, scan++ ) 
         {
           *scan = (long) red[cnt];
           tmp = (long) green[cnt];
           tmp = tmp << 8;
           *scan = *scan + tmp;
           tmp = (long) blue[cnt];
           tmp = tmp << 16;
           *scan = *scan + tmp;
         }
       }
     }
     lrectwrite((Screencoord)(0),(Screencoord)(0),(Screencoord)(npix-1),
                        (Screencoord)(nscan-1),img);
   }
     
   while( 1 )
   {  
     if( qread(&tmp) == REDRAW )
     {
       if( VGX != NULL )
       {
         wrred = 0xffff; wrgrn = 0x0; wrblu = 0x0;
         RGBwritemask(wrred,wrgrn,wrblu);
         pixmode(PM_SHIFT,0); /* red */
         lrectwrite((Screencoord)(0),(Screencoord)(0),(Screencoord)(npix-1),
			(Screencoord)(nscan-1),red);

         wrred = 0x0; wrgrn = 0xffff; wrblu = 0x0;
         RGBwritemask(wrred,wrgrn,wrblu);
         pixmode(PM_SHIFT,8); /* green */
         lrectwrite((Screencoord)(0),(Screencoord)(0),(Screencoord)(npix-1),
			(Screencoord)(nscan-1),green);

         wrred = 0x0; wrgrn = 0x0; wrblu = 0xffff;
         RGBwritemask(wrred,wrgrn,wrblu);
         pixmode(PM_SHIFT,16); /* blue */
         lrectwrite((Screencoord)(0),(Screencoord)(0),(Screencoord)(npix-1),
			(Screencoord)(nscan-1),blue);
       }
       else
         lrectwrite((Screencoord)(0),(Screencoord)(0),(Screencoord)(npix-1),
                        (Screencoord)(nscan-1),img);

     }
   }
}


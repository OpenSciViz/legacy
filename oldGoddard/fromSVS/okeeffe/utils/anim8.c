#include <stdio.h>
#include <strings.h>
#include <math.h>
#include <gl/gl.h>
#include <device.h>
#include <malloc.h>

#define xmax 1024
#define ymax 1024

main(argc,argv)
int argc; char **argv;
{
   unsigned char red[xmax*ymax], green[xmax*ymax], blue[xmax*ymax];
   char *rfile[99999], *bfile[99999], *gfile[99999];
   int nr, ng, nb, fd_red, fd_green, fd_blue, cnt=0;
   float exec_time;
   int i=0, j, npix=512, nscan=512, ti, tf, sz, i_start, i_stop;
   long tmp, ttob=0;
   unsigned short wrred = 0xffff, wrgrn = 0xffff, wrblu = 0xffff;
   int inpix=512, inscan=512;

   rfile[cnt] = malloc(80);

   while( (gets(rfile[cnt]) != NULL ) )
   {
     cnt++;
     rfile[cnt] = malloc(80);
   }

   i_start = 0;
   i_stop = cnt;

   argv++;
   while( *argv != NULL )
   {
    if( argv[0][1] == 'h' )
    {
      npix = 1024;
      nscan = 1024;
      argc--;
      argv++;
    }
    else if( argv[0][1] == 'i' )
    {
      ttob = 1;
      argc--;
      argv++;
    }
    else if( argv[0][1] == 'd' )
    {
      argv++;
      inpix = atoi(*argv);
      argv++;
      inscan = atoi(*argv);
      argv++;
    }  
   }
 
   prefsize(npix,nscan);
   winopen("RGB_Animate");
   RGBmode();
   gconfig();
   cpack(0); clear();
   pixmode(PM_SIZE,8);
   if( ttob ) pixmode(PM_TTOB,1);
   qdevice(LEFTMOUSE);
   doublebuffer();

   rectzoom(1.*npix/inpix,1.*nscan/inscan);
 
  while( 1 )
  {
    for( i = i_start; i < i_stop; i++ )
    {
     if( qtest() )
       if( qread(&tmp) == LEFTMOUSE )
	 qread(&tmp);

     fd_red = open(rfile[i],0);
     nr = read(fd_red,red,inscan*inpix);
     close(fd_red);
	wrred = 0xffff; wrgrn = 0x0, wrblu = 0x0;
        RGBwritemask(wrred,wrgrn,wrblu);
        pixmode(PM_SHIFT,0); /* red */
        lrectwrite((Screencoord)(0),(Screencoord)(0),(Screencoord)(inpix-1),
			(Screencoord)(inscan-1),red);

        wrred = 0x0; wrgrn = 0xffff, wrblu = 0x0;
        RGBwritemask(wrred,wrgrn,wrblu);
        pixmode(PM_SHIFT,8); /* green */
        lrectwrite((Screencoord)(0),(Screencoord)(0),(Screencoord)(inpix-1),
			(Screencoord)(inscan-1),red);

        wrred = 0x0; wrgrn = 0x0, wrblu = 0xffff;
        RGBwritemask(wrred,wrgrn,wrblu);
        pixmode(PM_SHIFT,16); /* blue */
        lrectwrite((Screencoord)(0),(Screencoord)(0),(Screencoord)(inpix-1),
			(Screencoord)(inscan-1),red);
        swapbuffers();
    }
 
    for( i = cnt-1; i >= 0; i-- )
    {
     if( qtest() )
       if( qread(&tmp) == LEFTMOUSE )
	 qread(&tmp);

     fd_red = open(rfile[i],0);
     nr = read(fd_red,red,inpix*inscan);
     close(fd_red);
	wrred = 0xffff; wrgrn = 0x0, wrblu = 0x0;
        RGBwritemask(wrred,wrgrn,wrblu);
        pixmode(PM_SHIFT,0); /* red */
        lrectwrite((Screencoord)(0),(Screencoord)(0),(Screencoord)(inpix-1),
			(Screencoord)(inscan-1),red);

        wrred = 0x0; wrgrn = 0xffff, wrblu = 0x0;
        RGBwritemask(wrred,wrgrn,wrblu);
        pixmode(PM_SHIFT,8); /* green */
        lrectwrite((Screencoord)(0),(Screencoord)(0),(Screencoord)(inpix-1),
			(Screencoord)(inscan-1),red);

        wrred = 0x0; wrgrn = 0x0, wrblu = 0xffff;
        RGBwritemask(wrred,wrgrn,wrblu);
        pixmode(PM_SHIFT,16); /* blue */
        lrectwrite((Screencoord)(0),(Screencoord)(0),(Screencoord)(inpix-1),
			(Screencoord)(inscan-1),red);

 	swapbuffers();
    }
  }
}


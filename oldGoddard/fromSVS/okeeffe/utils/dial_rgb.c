/* This is an example of the use of dials.

   Written by Mark Overmars

   Date: 29 Apr 1991
*/

#include <stdio.h>
#include <gl/gl.h>
#include "/usr/local/include/forms.h"

FL_FORM *form;
FL_OBJECT *button, *red, *green, *blue, *redtext, *greentext, *bluetext,
       *result;

void makeform()
{
  form = fl_bgn_form(FL_UP_BOX,300.0,330.0);
    button = fl_add_button(FL_NORMAL_BUTTON,45.0,270.0,210.0,45.0,"A Color Editor");
    fl_set_object_lsize(button,FL_LARGE_FONT);

    red = fl_add_dial(FL_LINE_DIAL,30.0,30.0,60.0,60.0,"Red");
    fl_set_dial_bounds(red,0.0,255.0);
    fl_set_dial_value(red,128.0);
    fl_set_object_color(red,1,FL_DIAL_COL2);
    redtext = fl_add_box(FL_DOWN_BOX,105.0,48.0,50.0,24.0,"");

    green = fl_add_dial(FL_LINE_DIAL,30.0,114.0,60.0,60.0,"Green");
    fl_set_dial_bounds(green,0.0,255.0);
    fl_set_dial_value(green,128.0);
    fl_set_object_color(green,2,FL_DIAL_COL2);
    greentext = fl_add_box(FL_DOWN_BOX,105.0,132.0,50.0,24.0,"");

    blue = fl_add_dial(FL_LINE_DIAL,30.0,198.0,60.0,60.0,"Blue");
    fl_set_dial_bounds(blue,0.0,255.0);
    fl_set_dial_value(blue,128.0);
    fl_set_object_color(blue,4,FL_DIAL_COL2);
    bluetext = fl_add_box(FL_DOWN_BOX,105.0,216.0,50.0,24.0,"");

    result = fl_add_box(FL_DOWN_BOX,180.0,15.0,90.0,243.0,"");
    fl_set_object_color(result,300,300);
  fl_end_form();
}

main()
{
  FL_OBJECT *ret;
  int r,g,b;
  char str[100];
  makeform();
  fl_show_form(form,FL_PLACE_SIZE,TRUE,"A Form");
  do
  {
    r = (int) fl_get_dial_value(red);
    g = (int) fl_get_dial_value(green);
    b = (int) fl_get_dial_value(blue);
    fl_mapcolor(300,r,g,b);
    fl_redraw_object(result);
    sprintf(str,"%d",r); fl_set_object_label(redtext,str);
    sprintf(str,"%d",g); fl_set_object_label(greentext,str);
    sprintf(str,"%d",b); fl_set_object_label(bluetext,str);
    ret = fl_do_forms();
  } while (ret != button);
  fl_hide_form(form);
}

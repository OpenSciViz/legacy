/* test of get_X11bitmap: */
#include <stdio.h>
main()
{
  char *map, *get_X11bitmap();
  int w, h;

  map = get_X11bitmap(&w,&h);

  write(1,map,w*h);
}


#include <malloc.h>
#include "X11.bitmap"
char *get_X11bitmap(w,h)
int *w, *h;
{
  int i=0, j=0, k=0, bitcnt=0, finished=0;
  unsigned char bits;
  char *bytemap;

  *w = X11_width;
  *h = X11_height;
  bytemap = malloc((*w) * (*h));
  

  while( ! finished )
  {
     bits = X11_bits[i];
     bits = bits << 7;
     bits = bits >> 7;
     bytemap[j*(*w) + k++] = bits; 

     bits = X11_bits[i];
     bits = bits << 6;
     bits = bits >> 7;
     bytemap[j*(*w) + k++] = bits; 

     bits = X11_bits[i];
     bits = bits << 5;
     bits = bits >> 7;
     bytemap[j*(*w) + k++] = bits; 

     bits = X11_bits[i];
     bits = bits << 4;
     bits = bits >> 7;
     bytemap[j*(*w) + k++] = bits; 

     bits = X11_bits[i];
     bits = bits << 3;
     bits = bits >> 7;
     bytemap[j*(*w) + k++] = bits; 

     bits = X11_bits[i];
     bits = bits << 2;
     bits = bits >> 7;
     bytemap[j*(*w) + k++] = bits; 

     bits = X11_bits[i];
     bits = bits << 1;
     bits = bits >> 7;
     bytemap[j*(*w) + k++] = bits; 

     bits = X11_bits[i];
     bits = bits >> 7;
     bytemap[j*(*w) + k++] = bits; 

     if( k >= (*w) ) { k = 0; j++; }
     if( j >= (*h)) finished = 1;
     i++;
  }
  return( bytemap );
}

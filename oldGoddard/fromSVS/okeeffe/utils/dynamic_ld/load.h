/*
	load.h. Declarations for the dynamic loading code.

*/

/* load_program takes path arg and returns 0 or pointer to 
   place to call.
typedef int (*load_program_funcp)();
*/

int (* load_program(char *)) ();


/* unload_program returns 0 for a successful unload and a negative
   numbef if there is a problem.
*/
int unload_program();

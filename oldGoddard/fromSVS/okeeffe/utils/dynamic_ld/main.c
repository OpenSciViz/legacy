#include <stdio.h>

#include "load.h"

main()
{
	int i;
	int (*func)();
	printf("this program loads and calls an anonymous routine. \n");

	for(i = 1; i < 4; i++) {
		int j;
		unload_program();
		func = load_program("loadable");
		if(!func) {
			printf("Could not load!\n");
			exit(1);
		}
		/* call the anonymous function */
		j = (*func)("a string");
		printf("We have returned from the loadable code %d\n",j);
		j = (*func)("a string");
		printf("We have again returned from the loadable code %d\n",j);
	}
	unload_program();

	/* here main sets a return status to shell */
	return 0;
}

/*
	Sample dynamic load code.

	Entries:

	func_addr = load_program(pathname)

	unload_program()
*/
#include <stdio.h>
#include <fcntl.h>

#undef FREAD
#undef FWRITE
#include <a.out.h>
#include <ldfcn.h>
#include <string.h>

#include <sys/mman.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
char *shmat();
#include <sys/param.h>
#include <sys/sysmips.h>

#include "load.h"



/*
 * char arrays containing the current path and entry point name.
 */
static char *pname;  /* current loadable program */
static int (*local_load_program())();

struct secmap_s {
	caddr_t addr;
	int initlen,len,prot,flags,id;
	off_t off;
	int ismapped;
} ;
struct shmmap_s {
	caddr_t addr;
	int len,id,shmflg;
	key_t key;
	int ismapped,isattached;
} ;

typedef struct secmap_s *secmap_t;

static struct secmap_s mydata,text;
static struct shmmap_s shared;

static int local_fileno;

static struct shmid_ds shmbuf;


/*
 * unload the current program
 *
 * input -none
 *
 * output-none
 */
int unload_program()
{
	int retval=0;

	if (shared.isattached) {
		/* detach the shared memory */
		if (shmdt(shared.addr)) {
			fprintf(stderr,"cannot detach shm segment");
			perror("load_program");
		}
		shared.isattached = 0;
	}
	if (shared.ismapped) {
#ifdef DEBUG
		if (shmctl(shared.id,IPC_STAT,&shmbuf)) {
			fprintf(stderr,"shmctl(IPC_STAT) denied");
			perror("");
		} 
		else
		fprintf(stderr,"shmbuf: ipc_perm: 0x%x,%x,%x,%x segsz: 0x%x\n",
			shmbuf.shm_perm,shmbuf.shm_segsz);
#endif
		/* remove the shared memory segment */
		if (shmctl(shared.id,IPC_RMID,&shmbuf)) {
			shared.ismapped = 0;
			fprintf(stderr,"cannot remove shm segment");
			perror("unload_program");
			return(-1);
		}
		shared.ismapped = 0;
	}
	if (!text.ismapped)
		return(-1);


	/* unmap the area */
	if (munmap(text.addr,text.len)) {
		perror("unload_program");
		retval = -1;
	}
	text.ismapped = 0;

	if (mydata.ismapped)
	    /* unmap the mapped data segment if it is mapped */
	    if (munmap(mydata.addr,mydata.initlen)){
		perror("unload_program");
		retval = -1;
	    }
	mydata.ismapped = 0;

	/* close the old file */
	if (text.id >= 0) {
		if (close(text.id)) {
			printf("unload_program: cant close %s",pname);
			perror("unload_program");
		}
		text.id = mydata.id = local_fileno = (-1);
	}

	free(pname);
	pname = 0;
	return(retval);
}


/*
 *
 * output-function value = function pointer of anonymous func
 * 	if ok.  0 if failure.
 *
 * notes- Once loaded, the program will remain loaded until the
 *        load_program function is called or a request to load another
 *        file is received.  This has the side effect of not refreshing
 *        data areas in the loaded module.
 */

int (*load_program(char *path))()
/* path is name used to open the loadable module*/
{
	int   i = 0;
	unsigned n;
	unsigned long   ent_offset;
	int (*fptr)() = 0;


	if(pname) {
		fprintf(stderr,"module %s loaded. Should call unload_program first.", path);
		return(0);
	}


	/* check that no module is loaded */
	if (shared.ismapped) {
		fprintf(stderr,"module %s loaded. Should call unload_program first.", path);
		return(0);
	}
	/* and save the path of the file most recently loaded */
	pname =  strdup(path);
	if(pname == 0) {
		fprintf(stderr,"strdup failed, out of memory\n");
		return 0;
	}
	/* attempt to load the module */
	if ((fptr =local_load_program()) == 0) {
	    if (shared.ismapped) 
		/* failed in part. remove any debris */
		unload_program();
	    if(pname) free(pname);
            pname = 0;
	    return(0);
	}

	/* return pointer to this function */

	return(fptr);
}

char *illegal_sections[] = {
	_TV	,
	_INIT ,
	_FINI ,
	_LIB ,
	_UCODE	,
	0
};

struct filehdr *file_hdr;
struct aouthdr aohdr, *aout_hdr;
SCNHDR *sh;
unsigned long pagemask;

/*
 *  load_program
 *
 *
 *  Exit -  0 normal return
 *         -1 error
 */

static int (*local_load_program())() 
{

	extern char end,edata,etext;
	LDFILE *ldptr = NULL;
	secmap_t secmap;
	pEXTR symend,pext;
	pCHDRR stab;
	unsigned long address;
	caddr_t paddr;
	int secindx;
	int isdatasec,odd;
	int entries_seen;
	char **secnameptr;
	int (*entry)();

	if (sh == (SCNHDR *)0) {
	    /* initialize some globals */
	    if ((sh = (SCNHDR *)calloc(1,SCNHSZ)) == 0) {
		fprintf(stderr,"cannot allocate section header");
		return(0);
	    }
	    pagemask = getpagesize() - 1;
	}

	if ((ldptr = ldopen(pname, ldptr)) == NULL)  {
		fprintf(stderr,"cant open module file %s",pname);
		return(0);
	}

	/* get pointer to file header */
	file_hdr = &HEADER(ldptr);

	/* get copy of optional header to check a.out magic number */
	if (ldohseek(ldptr) == FAILURE) {
		fprintf(stderr,"cant seek to optional header");
		return(0);
	}
	aout_hdr = &aohdr;
	FREAD(aout_hdr, sizeof(struct aouthdr), 1, ldptr);
	;
	if (aout_hdr->magic != ZMAGIC) {
		fprintf(stderr,"Module %s is not ZMAGIC",pname);
		return(0);
	}
	entry = (int (*)()) aout_hdr->entry;


	/* initialize the text and data mapping structures */
	text.addr = (caddr_t)0;
	text.len =  text.off = 0;
	text.prot = (PROT_READ|PROT_EXECUTE);
	text.flags = (MAP_SHARED|MAP_FIXED);
	mydata.addr = (caddr_t)0;
	mydata.len = mydata.initlen = mydata.off = 0;
	mydata.prot = (PROT_READ|PROT_WRITE);
	mydata.flags = (MAP_PRIVATE|MAP_FIXED);

	/* text is first in the COFF file */
	secmap = 0;

	/* accumulate info about the sections */
	for (secindx=1;secindx <= file_hdr->f_nscns; secindx++) {

		/* get the next section header */
		if (ldshread(ldptr, secindx ,sh) == FAILURE) {
			fprintf(stderr,"ldshread of %d failed\n",secindx);
			ldclose(ldptr);
			return 0;
		}

		/* check that the section isn't illegal */
		for (secnameptr = illegal_sections; *secnameptr != (char *)0;
			secnameptr++) {
		    if (!strncmp(sh->s_name,*secnameptr,8)) {
			fprintf(stderr,"cannot handle %.8s section found in Module",
				sh->s_name);
			ldclose(ldptr);
			return(0);
		    }
		}

		if(sh->s_flags == STYP_DATA) {
				secmap = &mydata;
		} else if(sh->s_flags == STYP_BSS) {
			secmap = &mydata;
		}else if(sh->s_flags == STYP_TEXT) {
			if (text.len) {
				fprintf(stderr,"Second text in Module %s",pname);
				ldclose(ldptr);
				return(0);
			}
			secmap = &text;
		} else {
			continue; /* ignore other sections */
		}
		isdatasec= (sh->s_flags & STYP_DATA) ? 1 : 0;
		/* update the region information */
		if (secmap->addr == (caddr_t)0) {
			if ((odd = ((int)sh->s_vaddr & pagemask)) != 0) {
				/* attempt to offset from start of file */
				if ((isdatasec)||
				    (sh->s_scnptr & pagemask) != odd) {
					fprintf(stderr,"badly-aligned section:");
					dump_sec(sh);
					ldclose(ldptr);
					return(0);
				}
			}
			secmap->addr = (caddr_t)((int)sh->s_vaddr - odd);
			secmap->off = sh->s_scnptr - odd;
			secmap->len += odd;
		}
		else {
			/* check that the vaddr is contiguous */
			if (sh->s_vaddr != 
			     ((int)secmap->addr + (int)secmap->len)) {
				fprintf(stderr,"non-contiguous section");
				dump_sec(sh);
				ldclose(ldptr);
				return 0;
			}
		}
		secmap->len += sh->s_size;
		if (isdatasec) secmap->initlen += sh->s_size;

	}
	if (!text.len) {
		fprintf(stderr,"no text in Module %s",pname);
		ldclose(ldptr);
		return(0);
	}
#ifdef DEBUG
	fprintf(stderr,"found 0x%x .text bytes at 0x%x\n",text.len,text.addr);
	fprintf(stderr,"found 0x%x .data bytes at 0x%x (0x%x initialized)\n",
		mydata.len,mydata.addr,mydata.initlen);
#endif

	ldclose(ldptr);


	mydata.initlen += pagemask;
	mydata.initlen &= ~pagemask;
	/* check that the addresses are region-aligned */
	if ((int)text.addr & (NBPS-1)) {
		fprintf(stderr,"text base must be multiple of 0x%x (was 0x%x)\n",
			NBPS,text.addr);
		return(0);
	}
	if ((int)mydata.addr & (NBPS-1)) {
		fprintf(stderr,"data base must be multiple of 0x%x (was 0x%x)\n",
			NBPS,mydata.addr);
		return(0);
	}

	/* check that we aren't going to overlay ourselves. */
#ifdef DEBUG
	printf("host etext = 0x%x, edata = 0x%x, end = 0x%x\n",
		&etext,&edata,&end);
#endif
	if ((text.addr < &etext)||
	    (((int)text.addr & 0xf0000000) != ((int)&etext & 0xf0000000))) {
		fprintf(stderr,"Module text area (at 0x%x)\n\toverlaps host text (below 0x%x)\n\tor is in different 256M segment\n",
		     text.addr,&etext);
		return(0);
	}
	if ((mydata.addr != (caddr_t)0) && ((mydata.addr < &end)||
	    (((int)mydata.addr & 0xf0000000) != ((int)&end & 0xf0000000)))) {
		fprintf(stderr,"Module data area (at 0x%x)\n\toverlaps host data (below 0x%x)\n\tor is in different 256M segment\n",
		     mydata.addr,&end);
		return(0);
	}

	/* open the file */
	local_fileno = open(pname,O_RDONLY);
	if (local_fileno < 0) {
		fprintf(stderr,"error re-opening Module file\n");
		perror("load_program:\n");
		return(0);
	}
	text.id = local_fileno;
	mydata.id = local_fileno;
	/* last, map the file */
	if ((mydata.len)&&(mydata.addr != (caddr_t)0)) {
		/* There is an alternative to shared memory segments
		   for data:
			Simply use malloc() for the data area.
			Don't forget to  zero out the bss area.
			And of course, you cannot ld -A till
			after doing the malloc, since only then
			do you know the correct data address.
			
			It would be wise to allocate enough malloc() space
			to allow giving ld -A a page boundary as a
			data space address.   This avoids problems with
			the present and future section alignment assumptions
			in ld.
		*/
		
		/* allocate a shmid and segment, and attach it. */

		shared.addr = mydata.addr;
		shared.len = ((mydata.len + pagemask)&(~pagemask));
		shared.key = IPC_PRIVATE;
		shared.shmflg = (IPC_NOWAIT|IPC_CREAT|IPC_EXCL|0600);
		shared.isattached = shared.ismapped = 0;
		if ((shared.id = shmget(shared.key,shared.len,shared.shmflg)) 
			< 0) {
			fprintf(stderr,"cannot allocate shared memory segment\n");
			perror("load_program:");
			return(0);
		}
		shared.ismapped = 1;
		if (shmat(shared.id,shared.addr,0) != (char *)shared.addr) {
			fprintf(stderr,"cannot attach shared memory segment\n");
			perror("load_program:");
			return(0);
		}
		shared.isattached = 1;
	}
#ifdef DEBUG
	printf("mapping text: 0x%x bytes, offset 0x%x mapped at 0x%x\n",
		text.len,text.off,text.addr);
#endif
	paddr = (caddr_t)mmap(text.addr,text.len,text.prot,text.flags,text.id,
			text.off);
	if (paddr != text.addr) {
		perror("load_program error while attempting to map text");
		return(0);
	}
	text.ismapped = 1;
	if ((mydata.initlen)&&(mydata.addr != (caddr_t)0)) {
	/* We do not map in the data portion of the program because
		we do not wish to expand the file and we cannot count on the
		bytes after the end of .data being 0. They may be bytes from
		the symbol table!
	   Instead we read initialized data into the shared memory segment.
	   New private shared memory segments are initialized to 0, 
	    and binary 0 is integer 0 and floating-point 0 and a null
            pointer so
	   we need not explicitly zero out bss data.
	*/
#ifdef DEBUG
	printf("reading data: 0x%x bytes, offset 0x%x read at 0x%x\n",
		mydata.initlen,mydata.off,mydata.addr);
#endif
		lseek(mydata.id,mydata.off,0);
		if (read(mydata.id,mydata.addr,mydata.initlen) != mydata.initlen) {
			fprintf(stderr,"cant read initialized data\n");
			perror("load_program");
			return(0);
		}

	}
	/* We need to flush the I-cache to ensure any instructions in it
	   relating to previous loaded modules are flushed out.
	   Unfortunately, this flushes the cache for the entire machine.
	   Therefore we do not want to do it too often!
	*/
	sysmips(FLUSH_CACHE);

	return(entry);
}

static dump_sec(sh)
SCNHDR *sh;
{
	printf("section %.8s: vaddr=0x%x, flags=0x%x, scnptr=0x%x, size=0x%x\n",
		sh->s_name,sh->s_vaddr,sh->s_flags,sh->s_scnptr,sh->s_size);
}




#
#  This Makefile builds a test program (driver) and a dynamically-
#  loadable module.
#  Note that the dynamically-loadable module could perfectly well
#  be created, compiled, and loaded at run-time. However, to keep
#  this example short, we create the dynamically-loadable module
#  in this makefile
#
#  If you change anything in the driver, relink loadable!
#
#  This presumes you have moved (or symbolically linked) the *_G0.a
#  libraries provided in the release to a new directory /usr/lib/G0,
#  as in:
#
#  (mkdir, ln -s must be done as superuser)
#  su
#  mkdir /usr/lib/G0
#  ln -s /usr/lib/libc_G0.a /usr/lib/G0/libc.a
#  ln -s /usr/lib/libmalloc_G0.a /usr/lib/G0/libmalloc.a
#  Or, just change -lc to /usr/lib/libc_G0.a  etc below.
#

SHELL = /bin/sh

#  These are addresses to use for text and data for the dynamically
#  loaded module.
ADDR = -T 2000000 -D 18000000
LIBDIR = -L/usr/lib/G0
LIBS = -lmalloc -lc
OBJ = loadable.o loadd2.o
CFLAGS = -g -DDEBUG

all: driver loadable

driver: main.o load.o
	$(CC) $(CFLAGS) main.o load.o -lmld -o driver

main.o: main.c 
	$(CC) $(CFLAGS) -c main.c

load.o: load.c
	$(CC) $(CFLAGS) -c load.c

loadable: $(OBJ) driver
	${LD} ${LDFLAGS} -o loadable -G 0 -A driver -z \
		${ADDR} ${OBJ} ${LIBDIR} ${LIBS}

loadable.o: loadable.c
	$(CC) -G 0 $(CFLAGS) -c loadable.c
loadd2.o: loadd2.c
	$(CC)  -G 0 $(CFLAGS) -c loadd2.c

clean:
	-rm -f *.o

clobber: clean
	-rm -f loadable driver

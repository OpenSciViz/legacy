#include <stdio.h>
#include <malloc.h>
#define DEBUG

main(argc,argv)
int argc;
char **argv;
{
  char *path;
  char *buff[1000];
  int n;

  path = argv[1];

  n = directory(path,buff);
}
  
int directory(path,buff)
char *path, *buff[];
{
  FILE *fp=NULL;
  char cmd[84];
  int i=0, n=80;

  strcpy(cmd,"ls ");
  strcat(cmd,path);

#ifdef DEBUG
  printf("directory> attempt: %s\n",cmd);
#endif

  fp = popen(cmd,"r");
  if( fp == NULL )
  {
    printf("directory> sorry, unable to run ls on %s\n",path);
    exit(0);
  }

  buff[i] = malloc(80);
  while( fgets(buff[i],n,fp) != NULL )
  {
#ifdef DEBUG
    printf("directory> %s\n",buff[i]);
#endif
    buff[++i] = malloc(80);
  }
  pclose(fp);

  printf("directory> found %d entries of %s\n",i,path);

  return( i );
}

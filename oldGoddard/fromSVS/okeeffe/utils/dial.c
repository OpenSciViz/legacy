/* This is an example of the use of dials.

   Written by Mark Overmars

   Date: 29 Apr 1991
*/

#include <stdio.h>
#include <gl/gl.h>
#include "/usr/local/include/forms.h"

FL_FORM *form;
FL_OBJECT *dial, *text, *result;

void makeform(float val)
{
  form = fl_bgn_form(FL_UP_BOX,160.0,100.0);
    dial = fl_add_dial(FL_NORMAL_DIAL,10.0,30.0,60.0,60.0,"Angle");
    fl_set_dial_bounds(dial,0.0,180.0);
    fl_set_dial_value(dial,val);
    fl_set_object_color(dial,7,FL_DIAL_COL1);
    text = fl_add_box(FL_DOWN_BOX,80.0,40.0,60.0,40.0,"");
    fl_set_object_lsize(text,FL_LARGE_FONT);

  fl_end_form();
  fl_set_form_position(form,450,10);
}

main(int argc, char **argv)
{
  FL_OBJECT *ret;
  int val;
  static char str[100], cmd[80];

  sprintf(cmd,"scrsave %s 0 645 0 485\n",argv[2]);
  printf("sending: %s",cmd);

  if( argc > 1 ) val = atoi(argv[1]);
  makeform((float)val);
  sprintf(str,"%d",val); fl_set_object_label(text,str);
  fl_show_form(form,FL_PLACE_POSITION,FALSE,NULL);
  system(cmd);
  fl_hide_form(form);
}

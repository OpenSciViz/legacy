
#include <math.h>

#define SWAP(a,b) tempr=(a); (a)=(b); (b)=tempr

int fft(data,nn,isign)
float data[];
int nn, isign;
{
   int n, mmax, m, j, istep, i;
   double wtemp, wr, wpr, wpi, wi, theta;
   float tempr, tempi;

   n = nn << 1;
   j = 1;
   for( i = 1; i < n; i += 2 )
   {
	if( j > i )
	{
	   SWAP(data[j],data[i]);
	   SWAP(data[j+1],data[i+1]);
	}
	m = n >> 1;
	while( m >= 2 && j > m )
	{
	   j -= m;
	   m >>= 1;
	}
	j += m;
   }
   mmax = 2;
   while( n > mmax )
   {
	istep = 2*mmax;
	theta = 6.28318530717959 / (isign*mmax);
	wtemp = sin(0.5*theta);
	wpr = -2.0 * wtemp * wtemp;
	wpi = sin(theta);
	wr = 1.0;
	wi = 0.0;
	for( m = 1; m < mmax; m += 2 )
	{
	   for( i = m; i < n; i += istep )
	   {
		j = i + mmax;
		tempr = wr*data[j] - wi*data[j+1];
		tempi = wr*data[j+1] + wi*data[j];
		data[j] = data[i] - tempr;
		data[j+1] = data[i+1] - tempi;
		data[i] += tempr;
		data[i+1] += tempi;
	   }
	   wtemp = wr;
	   wr = wtemp*wpr - wi*wpi*wtemp;
	   wi = wi*wpr + wtemp*wpi + wi;
	}
	mmax = istep;
   }
}

main()
{
   float tst[512*512][2];
   int nn = 512*512;
   int i, ti, tf;
   float pi2 = 2.0*3.1415926;

   for( i = 0; i < nn; i++ )
   {
	tst[i][0] = sin( 1.0*i*pi2/1024.0 );
	tst[i][1] = 0.0;
   }
   ti = time(0);
   fft(tst,nn,1);
   tf = time(0);
   printf("FFT'd 262,144 complex elements in ~ %d sec.\n",(tf - ti));
}


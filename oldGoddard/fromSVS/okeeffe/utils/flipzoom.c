#include <stdio.h>
#include <strings.h>
#include <math.h>
#include <gl/gl.h>
#include <device.h>
#include <malloc.h>
#include "/usr/local/include/forms.h"

#define xmax  640
#define ymax  485

main(argc,argv)
int argc; char **argv;
{
 unsigned char red[xmax*ymax], green[xmax*ymax], blue[xmax*ymax],
                 data[xmax*ymax];
 unsigned char clt_r[208], clt_g[208], clt_b[208];
 static char *file[9999], *substr, num[4];
 int nr, nc, fd, cnt=0, fd_clt, evnt, w_img, dial_value,t_ang=20;
 float exec_time, xzoom=1.0, yzoom=1.0;
 int i=0, j, npix=xmax, nscan=ymax, ti, tf, sz, i_start, i_stop;
 long tmp, ttob=0;
 FL_FORM *form_R, *form_T, *dial_func();

  foreground();

/* get the color table 
   fd_clt = open("/usr/people/xrhon/utils/colorg.hf",0);*/
   if( argc > 1 ) /* assume the string is the colortable */
   {
     fd_clt = open(argv[1],0);
     if( fd_clt <= 0 )
     {
	fprintf(stderr,"unable to open requested colortable: %s\n",argv[1]);
        fd_clt = open("/disk5/svs/xrhon/sar/video/color11g.hf",0);
     }
     argc--;
     argv++;
   }
   else
     fd_clt = open("/disk5/svs/xrhon/sar/video/color11g.hf",0);

   if( fd_clt <= 0 ) exit(1);
   read(fd_clt,clt_r,208);
   read(fd_clt,clt_g,208);
   read(fd_clt,clt_b,208);
   close( fd_clt );

   file[cnt] = malloc(80);

   while( (gets(file[cnt]) != NULL ) )
     file[++cnt] = malloc(80);

   fprintf(stderr,"found %d image files...\n",cnt);

   i_start = 0;
   i_stop = cnt;

  while( argc > 1 )
  {
   if( argc > 1 )
   { 
    if( argv[1][1] == 'h' )
    {
      npix = 1024;
      nscan = 768;
      xzoom = 1024.0/640.0; yzoom = 768.0/485.0;
    }
    else if( argv[1][1] == 'i' )
      ttob = 1;
    else if( argv[1][1] == 't' )
      t_ang = atoi(&argv[1][2]);

    argc--;
    argv++;
   }
  }

   prefsize(npix,nscan);
   w_img = winopen("FlipSar: Left Mouse --forward-->  Middle Mouse <--reverse--");
   RGBmode();
   gconfig();
   cpack(0); clear();
   pixmode(PM_SIZE,8);
   if( ttob ) pixmode(PM_TTOB,1);
   qdevice(LEFTMOUSE);
   qdevice(MIDDLEMOUSE);
/*   qdevice(RIGHTMOUSE); */
   qdevice(ESCKEY);
   doublebuffer();
   rectzoom(xzoom,yzoom);

  i = 0;
  fd = open(file[i],0);
  nr = read(fd,data,xmax*ymax);
  close(fd);
  for( j = 0; j < xmax*ymax; j++ )
  {
    red[j] = clt_r[data[j]];
    green[j] = clt_g[data[j]];
    blue[j] = clt_b[data[j]];
  } 

  write_framebuff(red,green,blue);
/* set the Receive dial: */
    substr = strstr(file[i],"hf");
/*    nc = (int) substr - (int) strchr(substr,'.') - 3; */
    strncpy(num,(substr + 2),3);
    dial_value = atoi(num);
/* create the dials */
  form_T = dial_func(860,875,"Transmit",t_ang);
  fl_show_form(form_T,FL_PLACE_POSITION,FALSE,NULL);
  form_R = dial_func(1070,875,"Receive",dial_value);
  fl_show_form(form_R,FL_PLACE_POSITION,FALSE,NULL);

  fprintf(stderr,"start event loop: %d  %s\n",i,file[i]);
  qreset();
  while( TRUE )
  {
    if( (evnt = qread(&tmp)) == LEFTMOUSE ) /* down (left) mouse == forward */
    {  
/* discard up mouse event */
      qread(&tmp); qreset(); 
      if( ++i >= cnt ) i = cnt-1; /* too far */
    }
    else if( (evnt = qread(&tmp)) == MIDDLEMOUSE ) /* reverse */
    {
/* discard up mouse event */
      qread(&tmp); qreset(); 
      if( --i < 0 ) i = 0; /* too far */
    }
    else if( (evnt = qread(&tmp)) == ESCKEY ) /* quit */
    {
/* kill the dials */
      fl_hide_form(form_R);
      free( form_R );
      fl_hide_form(form_T);
      free( form_T );
      exit(0);
    }
    else
      continue;

    fprintf(stderr,"%d  %s\n",i,file[i]);
    fd = open(file[i],0);
    nr = read(fd,data,xmax*ymax);
    close(fd);
    for( j = 0; j < xmax*ymax; j++ )
    {
      red[j] = clt_r[data[j]];
      green[j] = clt_g[data[j]];
      blue[j] = clt_b[data[j]];
    }    
    write_framebuff(red,green,blue);
/* set the Receive dial: */
    substr = strstr(file[i],"hf");
/*    nc = (int) substr - (int) strchr(substr,'.') - 3; */
    strncpy(num,(substr + 2),3);
    dial_value = atoi(num);
    reset_dial(form_R,dial_value);
  }
}

write_framebuff(red,green,blue)
unsigned char *red, *green, *blue;
{
  unsigned short wrred = 0xffff, wrgrn = 0xffff, wrblu = 0xffff;

    wrred = 0xffff; wrgrn = 0x0, wrblu = 0x0;
    RGBwritemask(wrred,wrgrn,wrblu);
    pixmode(PM_SHIFT,0); /* red */
    lrectwrite((Screencoord)(0),(Screencoord)(0),(Screencoord)(xmax-1),
			(Screencoord)(ymax-1),red);

    wrred = 0x0; wrgrn = 0xffff, wrblu = 0x0;
    RGBwritemask(wrred,wrgrn,wrblu);
    pixmode(PM_SHIFT,8); /* green */
    lrectwrite((Screencoord)(0),(Screencoord)(0),(Screencoord)(xmax-1),
			(Screencoord)(ymax-1),green);

    wrred = 0x0; wrgrn = 0x0, wrblu = 0xffff;
    RGBwritemask(wrred,wrgrn,wrblu);
    pixmode(PM_SHIFT,16); /* blue */
    lrectwrite((Screencoord)(0),(Screencoord)(0),(Screencoord)(xmax-1),
			(Screencoord)(ymax-1),blue);
    swapbuffers();
}

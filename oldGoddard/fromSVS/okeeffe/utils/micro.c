#include <stdio.h>
main()
{
FILE *fin, *fout;
unsigned char *scan[2], *tmp;
int i=0;

   fin = fopen("tiny_globe.pixmap","r");
   fout = fopen("micro_globe.pixmap","w");
   scan[0] = (char *) malloc(128);
   scan[1] = (char *) malloc(128);
   tmp = (char *) malloc(64);
   while( fread(scan[0],1,128,fin) > 0 )
   {
        if( fread(scan[1],1,128,fin) > 0 )
        {
	   for( i = 0; i < 128; i = i+2 ) 
		tmp[i/2] = scan[0][i]/4 + scan[1][i]/4 +
			 scan[0][i+1]/4 + scan[1][i+1]/4;
	   fwrite(tmp,1,64,fout);
        }
   }
   fclose(fin); fclose(fout);
}

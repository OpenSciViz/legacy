#include <stdio.h>
#include <strings.h>
#include <math.h>
#include <gl/gl.h>
#include <device.h>
#include <malloc.h>

#define xmax 1024
#define ymax 1024

main(argc,argv)
int argc; char **argv;
{
   unsigned char red[xmax*ymax], green[xmax*ymax], blue[xmax*ymax];
   char *rfile[99999], *bfile[99999], *gfile[99999];
   int nr, ng, nb, fd_red, fd_green, fd_blue, cnt=0;
   float exec_time;
   int i=0, j, npix=512, nscan=512, ti, tf, sz, i_start, i_stop;
   long tmp;
   unsigned short wrred = 0xffff, wrgrn = 0xffff, wrblu = 0xffff;

   rfile[cnt] = malloc(80);
   gfile[cnt] = malloc(80);
   bfile[cnt] = malloc(80);

   while( (gets(bfile[cnt]) != NULL ) && (gets(gfile[cnt]) != NULL)
       && (gets(rfile[cnt]) != NULL ) )
   {
     cnt++;
     bfile[cnt] = malloc(80);
     gfile[cnt] = malloc(80);
     rfile[cnt] = malloc(80);
   }

   i_start = 0;
   i_stop = cnt;
 /* .red, .blu, .grn not given;
assume only file prefix is given, and create the rest */
/*
   if( strstr(bfile,"blu") == NULL )
   {
     i_start = cnt;
     i_stop = 4 * cnt;
     for( i = 0; i < cnt; i++ )
     {
       bfile[cnt+i+1] = malloc(80);
       gfile[cnt+i+1] = malloc(80);
       rfile[cnt+i+1] = malloc(80);
       if( (i % 3) == 0 )
       {
	strcpy(bfile[cnt+i],bfile[i]);
     	strcpy(gfile[cnt+i],bfile[i]);
     	strcpy(rfile[cnt+i],bfile[i]);
     	strcat(bfile[cnt+i],".blu");
     	strcat(gfile[cnt+i],".grn");
     	strcat(rfile[cnt+i],".red");
       }
       else if( (i % 3) == 1 )
       {
	strcpy(bfile[cnt+i],gfile[i]);
     	strcpy(gfile[cnt+i],gfile[i]);
     	strcpy(rfile[cnt+i],gfile[i]);
     	strcat(bfile[cnt+i],".blu");
     	strcat(gfile[cnt+i],".grn");
     	strcat(rfile[cnt+i],".red");
       }
       else
       {
	strcpy(bfile[cnt+i],rfile[i]);
     	strcpy(gfile[cnt+i],rfile[i]);
     	strcpy(rfile[cnt+i],rfile[i]);
     	strcat(bfile[cnt+i],".blu");
     	strcat(gfile[cnt+i],".grn");
     	strcat(rfile[cnt+i],".red");
       }
     }
   }
*/

   if( argc > 1 ) 
    if( argv[1][1] == 'h' )
    {
	npix = 1024;
	nscan = 1024;
    }
   prefsize(npix,nscan);
   winopen("RGB_Animate");
   RGBmode();
   gconfig();
   cpack(0); clear();
   pixmode(PM_SIZE,8);
   pixmode(PM_TTOB,1);
   qdevice(LEFTMOUSE);
   doublebuffer();

   rectzoom(npix/512.0,nscan/512.0);
 
  while( 1 )
  {
    for( i = i_start; i < i_stop; i++ )
    {
     if( qtest() )
       if( qread(&tmp) == LEFTMOUSE )
	 qread(&tmp);

     fd_red = open(rfile[i],0);
     fd_green = open(gfile[i],0);
     fd_blue = open(bfile[i],0);	
     nr = read(fd_red,red,512*512);
     ng = read(fd_green,green,512*512);
     close(fd_red);
     close(fd_green); 
        
	wrred = 0xffff; wrgrn = 0x0, wrblu = 0x0;
        RGBwritemask(wrred,wrgrn,wrblu);
        pixmode(PM_SHIFT,0); /* red */
        lrectwrite((Screencoord)(0),(Screencoord)(0),(Screencoord)(511),
			(Screencoord)(511),red);

        wrred = 0x0; wrgrn = 0xffff, wrblu = 0x0;
        RGBwritemask(wrred,wrgrn,wrblu);
        pixmode(PM_SHIFT,8); /* green */
        lrectwrite((Screencoord)(0),(Screencoord)(0),(Screencoord)(511),
			(Screencoord)(511),green);

        swapbuffers();
    }
 
    for( i = cnt-1; i >= 0; i-- )
    {
     if( qtest() )
       if( qread(&tmp) == LEFTMOUSE )
	 qread(&tmp);
     fd_red = open(rfile[i],0);
     fd_green = open(gfile[i],0);
     fd_blue = open(bfile[i],0);	
     nr = read(fd_red,red,npix*nscan);
     ng = read(fd_green,green,npix*nscan);
     close(fd_red);
     close(fd_green); 

        wrred = 0xffff; wrgrn = 0x0, wrblu = 0x0;
        RGBwritemask(wrred,wrgrn,wrblu);
        pixmode(PM_SHIFT,0); /* red */
        lrectwrite((Screencoord)(0),(Screencoord)(0),(Screencoord)(511),
			(Screencoord)(511),red);

        wrred = 0x0; wrgrn = 0xffff, wrblu = 0x0;
        RGBwritemask(wrred,wrgrn,wrblu);
        pixmode(PM_SHIFT,8); /* green */
        lrectwrite((Screencoord)(0),(Screencoord)(0),(Screencoord)(511),
			(Screencoord)(511),green);

	swapbuffers();
    }
  }
}


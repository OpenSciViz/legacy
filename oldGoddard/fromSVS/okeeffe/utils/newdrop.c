#include <stdio.h>
#include <string.h>
#include <math.h>
/*
#include <gl.h> -- 3.1 change for 3.2
*/
#include <gl/gl.h>
#include <device.h>

#define xmax 1024
#define ymax 1024

main(argc,argv)
int argc; char **argv;
{
   unsigned char data[xmax];
/*
   int scan[xmax]; -- 3.1 change for 3.2
*/
   short scan[xmax];
   char *zfile;
   int nr, ng, nb, fd_red, fd_green, fd_blue;
   float exec_time;
   int i, j, npix, nscan, ti, tf;
   unsigned short wrred = 0xffff, wrgrn = 0xffff, wrblu = 0xffff;
   if( argc > 1 ) 
   {
      zfile = strdup(argv[1]);
   }
   else
   {
      zfile = (char *) malloc(80);
      for( i = 0; i < 80; i++) zfile[i] = '\n';
      printf("Enter input filename: ");
      gets(zfile);
   }
   if( argc > 2 )
	npix = atoi(argv[2]);
   else
	npix = xmax;
   if( argc > 3 )
	nscan = atoi(argv[3]);
   else
	nscan = ymax;

   ti = time(0);
   
   fd_red = open(zfile,0);
/*
   fd_green = open("cray.grn",0);
   fd_blue = open("cray.blu",0);
*/
  
   prefsize(npix,nscan);
   winopen(zfile);
/*
   RGBmode();
   RGBwritemask(wrred,wrgrn,wrblu);
*/
   gconfig();
   cpack(0); clear();
   for( i = 0; i < nscan; i++ )
   {
	nr = read(fd_red,data,npix);
	if( nr > 0 )
/*
	    for( j = 0; j < npix; j++ ) scan[j] = (int) data[j]; -- 3.1 to 3.2
*/
	    for( j = 0; j < npix; j++ ) scan[j] = (short) data[j];
/*
	ng = read(fd_green,green,npix);
	nb = read(fd_blue,blue,npix);
	printf("drop> read %d red bytes, %d green bytes, %d blue bytes\n",
		nr,ng,nb);
*/
/*
       lrectwrite(0,nscan-i,npix-1,nscan-i,scan); -- 3.1 to 3.2
*/
       rectwrite((Screencoord)0,(Screencoord)(nscan-i),(Screencoord)(npix-1),
			(Screencoord)(nscan-i),scan);
   }
   close(fd_red);
/* close(fd_green); close(fd_blue); */
  
   while( !getbutton(LEFTMOUSE) ) 
	; 
   tf = time(0);
   exec_time = (tf - ti) / 60.0;
   printf("drop> execution time= %f\n",exec_time);
   
}


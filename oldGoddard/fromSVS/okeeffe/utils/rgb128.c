#include <stdio.h>
#include <string.h>
#include <math.h>
#include <gl/gl.h>
#include <device.h>

#define xmax 1024
#define ymax 1024

main(argc,argv)
int argc; char **argv;
{
   unsigned char red[xmax*ymax], green[xmax*ymax], blue[xmax*ymax];
   long *scan, *img;
   char *zfile, *bfile, *gfile;
   int nr, ng, nb, fd_red, fd_green, fd_blue, cnt=0;
   float exec_time;
   int i, j, npix=512, nscan=512, ti, tf, sz;
   long tmp;
   unsigned short wrred = 0xffff, wrgrn = 0xffff, wrblu = 0xffff;

   sz = sizeof(long);
   img = (long *) malloc(xmax*ymax*sz);

   if( argc < 4 ) 
   {
	printf("please enter all 3 file names ... red green blue\n");
	exit(0);
   }
   if( argc > 4 ) 
   {
     npix = atoi(argv[4]);
     nscan = npix;
   }
/*
   if( argc > 1 ) 
   {
      zfile = strdup(argv[1]);
      bfile = strdup(argv[1]);
      gfile = strdup(argv[1]);
   }
   else
   {
     printf("please give a file name.\n");
     exit();
   }
   if( argc > 2 )
	npix = atoi(argv[2]);
   else
	npix = xmax;
   if( argc > 3 )
	nscan = atoi(argv[3]);
   else
	nscan = ymax;
   ti = time(0);
   fd_red = open(strcat(zfile,".red"),0);
   fd_green = open(strcat(gfile,".grn"),0);
   fd_blue = open(strcat(bfile,".blu"),0);
*/   
   fd_red = open(argv[1],0);
   fd_green = open(argv[2],0);
   fd_blue = open(argv[3],0);
	
   nr = read(fd_red,red,npix*nscan);
   ng = read(fd_green,green,npix*nscan);
   nb = read(fd_blue,blue,npix*nscan);
  
   prefsize(npix,nscan);
   winopen(zfile);
   RGBmode();
   RGBwritemask(wrred,wrgrn,wrblu);
   gconfig();
   cpack(0); clear();
   for( i = nscan; i > 1; i--)
   {
      scan = &img[(i-2)*npix];
      for( j = 0; j < npix; j++, cnt++, scan++ ) 
      {
	  *scan = (long) red[cnt];
	  tmp = (long) green[cnt];
	  tmp = tmp << 8;
	  *scan = *scan + tmp;
	  tmp = (long) blue[cnt];
	  tmp = tmp << 16;
	  *scan = *scan + tmp;
      }
   }
   lrectwrite((Screencoord)(0),(Screencoord)(0),(Screencoord)(npix-1),
			(Screencoord)(nscan-1),img);

   close(fd_red);
   close(fd_green); 
   close(fd_blue);
  
   while( TRUE )
   {
     if( qread(&tmp) == REDRAW )
       lrectwrite((Screencoord)(0),(Screencoord)(0),(Screencoord)(npix-1),
			(Screencoord)(nscan-1),img);
   }
}


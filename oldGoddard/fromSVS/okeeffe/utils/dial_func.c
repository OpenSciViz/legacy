/* This is an example of the use of dials.

   Written by Mark Overmars

   Date: 29 Apr 1991
*/

#include <stdio.h>
#include <gl/gl.h>
#include "/usr/local/include/forms.h"

FL_OBJECT *dial, *text, *title, *result;

FL_FORM *makeform(int xpos, int ypos, float val)
{
  FL_FORM *form= (FL_FORM *) malloc(sizeof(FL_FORM));
  form = fl_bgn_form(FL_UP_BOX,210.0,130.0);
    dial = fl_add_dial(FL_NORMAL_DIAL,10.0,10.0,60.0,60.0,"");
    fl_set_dial_bounds(dial,0.0,180.0);
    fl_set_dial_value(dial,val);
    fl_set_object_color(dial,7,FL_DIAL_COL1);
    text = fl_add_box(FL_DOWN_BOX,80.0,20.0,120.0,40.0,"");
    fl_set_object_lsize(text,FL_LARGE_FONT);
    title = fl_add_box(FL_DOWN_BOX,10.0,80.0,190.0,40.0,"");
    fl_set_object_lsize(title,FL_LARGE_FONT);
  fl_end_form();

  fl_set_form_position(form,xpos,ypos,900);
  return( form );
}

FL_FORM *dial_func(int xpos, int ypos, char *label, int val)
{
 FL_FORM *form;

  FL_OBJECT *ret;
  static char str[100];

  makeform(xpos,ypos,(float)val);
  sprintf(str,"%d"" (deg.)",val);
  fl_set_object_label(text,str);
  fl_set_object_label(title,label);
}

reset_dial(FL_FORM *form,int val)
{
  static char str[100];

  sprintf(str,"%d"" (deg.)",val);
  fl_addto_form(form);
    fl_set_object_label(text,str);
    fl_set_dial_value(dial,(float) val);
  fl_end_form();
}
    
/*
main()
{
 FL_FORM *form_R, *form_T;

  foreground();

  form_R = dial_func(1070,900,"Receive",180);
  fl_show_form(form_R,FL_PLACE_POSITION,FALSE,NULL);

  form_T = dial_func(860,900,"Trasmit",20);
  fl_show_form(form_T,FL_PLACE_POSITION,FALSE,NULL);

  getchar();

  fl_hide_form(form_R);
  free( form_R );
  fl_hide_form(form_T);
  free( form_T );
}
*/

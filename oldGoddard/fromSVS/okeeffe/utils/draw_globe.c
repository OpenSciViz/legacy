#include <stdio.h>
#include <string.h>
#include <math.h>
#include <gl.h>
#include <device.h>

#define xmax 1024
#define ymax 512

int drwrld()
{
int   npts, igid;
int   zero_cnt = 0;
float latlon[1000][2];
float llpair[2]; /* swap lat/lon values before using v2f */
float range[4];
int   nbytes;
int   i;

int mapfile;

/* Open the input worldmap dataset */
if ( (mapfile = open("/usr/people/xrhon/utils/ezmap_iris.data",0)) == -1)
{
	printf("Can't open input map dataset\n");
	exit(-1);
}

/* 
 * The organization of the map datasets is:
 *  1. There is a word specifying the number of full words to expect
 *  2. Each pair of lat/lon is within a word and is I*2 scaled by 100
 *  3. These pairs constitute a contigious line segment of the map
 */


nbytes = 1; /* to get into the while loop */
while (nbytes > 0)
{
	if((nbytes = read(mapfile, &npts, 4)) < 4) break;
	if((nbytes = read(mapfile, &igid, 4)) < 4) break;
	if((nbytes = read(mapfile, range, 16))< 16) break;
	nbytes = npts * 4;
	if((nbytes = read(mapfile, latlon, nbytes))< nbytes) break;
	if (igid == 0) zero_cnt++;
	if (zero_cnt < 2) continue;
	if (zero_cnt == 3) break;
	bgnline();
	for (i=0; i<(npts/2); i++)
	{
		llpair[0] = (1024/360.0)*(180.0 + latlon[i][1]);
		llpair[1] = (512/180.0)*(90.0 + latlon[i][0]);
		v2f(llpair);
	}
	endline();
}
/* Close the map dataset */
close(mapfile);

return;
}

  
main(argc,argv)
int argc; char **argv;
{
   unsigned char data[xmax];
   short scan[xmax];
   char *zfile;
   int nr, ng, nb, fd_red, fd_green, fd_blue;
   float exec_time;
   int i, j, npix, nscan, ti, tf;

   if( argc > 1 ) 
   {
      zfile = strdup(argv[1]);
   }
   else
   {
      zfile = (char *) malloc(80);
      for( i = 0; i < 80; i++) zfile[i] = '\n';
      printf("Enter input filename: ");
      gets(zfile);
   }
   fd_red = open(zfile,0);

   if( argc > 2 )
	npix = atoi(argv[2]);
   else
	npix = xmax;
   if( argc > 3 )
	nscan = atoi(argv[3]);
   else
	nscan = ymax;
   ti = time(0);
  
   prefsize(npix,nscan);
   winopen("Globe\n");
   RGBmode();
   gconfig();
   cpack(0xffffffff); 
   clear();

   for( i = 0; i < nscan; i++ )
   {
	nr = read(fd_red,data,npix);
	if( nr > 0 )
	    for( j = 0; j < npix; j++ ) scan[j] = (short) data[j];
        rectwrite(0,nscan-i,npix-1,nscan-i,scan);
   }
   close(fd_red); 
  
   cpack(0x00000000);
   drwrld();
   while( !getbutton(LEFTMOUSE) ) 
	; 
   tf = time(0);
   exec_time = (tf - ti) / 60.0;
   printf("drop> execution time= %f\n",exec_time);
   
}


/*
   
	IPC_UTILS.c

	Description:

	   Routines to handle interprocess communication

        Contains the routines:
    
	  - shm_get
	  - sem_get
	  - shm_attach
	  - shm_detach
	  - shm_remove
	  - sem_remove
	  - sem_wait
	  - sem_signal
	  - sem_get_lock
	  - sem_set_lock

	  - shm_malloc
	  - shm_free
	  - shm_get_local_address
	  - shm_get_shared_address
	  - shm_destroy_shared_address

	Authors:

	  Paul Kelaita, Sterling Federal Systems, Inc.
	  9/88

HISTORY:
	- Added shm_malloc and friends 		26 Feb 90	Al Globus

*/

/***
   INCLUDES
***/

   /*
      UNIX
   */

#include <assert.h>
#include <stdio.h>
#include <errno.h>

   /*
      FAST 
   */

#include <std.h>
#include <fast_memory.h>	/* UGLY */

#include "Shm_defs.h"

/******************************************************************************
   				SHM_GET
******************************************************************************/

int	shm_get (shm_key, shm_seg_size, create_flag)

	long	shm_key;
	int	shm_seg_size;
	int	create_flag;
{

   /***
      LOCAL VARIABLES
   ***/

   int		id;

   /***
      BEGIN
   ***/

   if (create_flag)  {
      /*
         create the shared memory segment and return the id
      */
      if ((id = shmget (shm_key, shm_seg_size, SHM_PERMFLAGS_CREAT)) == -1)  {
         perror ("ERROR: shmget\007");
	 return(-1);
      }
   }
   else  {
      /*
         locate the existing shared memory segment and return the id;
	 the size field is ignored and set to 0 because accesing an existing
	 segment does not require the segment size.
      */
      if ((id = shmget (shm_key, 0, SHM_PERMFLAGS)) == -1)  {
         perror ("ERROR: shmget\007");
         return(-1);
      }
   }
   return(id);

}



/******************************************************************************
   				SEM_GET
******************************************************************************/

int	sem_get (sem_key, create_flag)

	long	sem_key;
	int	create_flag;
{

   /***
      LOCAL VARIABLES
   ***/

   int		id;
   int		num_sems	= 1;

   /***
      BEGIN
   ***/

   if (create_flag)  {
      /*
         create semaphore, initialize semval, and return id
      */
      if ((id = semget (sem_key, num_sems, SEM_PERMFLAGS_CREAT)) == -1)  {
         perror ("ERROR: semget\007");
	 return(-1);
      }

      /*
	 initialize semval to 1
      */
      semctl (id, 0, SETVAL, 1);
   }
   else  {
      /*
         locate semaphore and return id
      */
      if ((id = semget (sem_key, num_sems, SEM_PERMFLAGS)) == -1)  {
         perror ("ERROR: semget\007");
	 return( -1 );
      }
   }
   return(id);

}



/******************************************************************************
   				SHM_ATTACH
******************************************************************************/

char	*shm_attach (shmid)

	int	shmid;
{

   /***
      LOCAL VARIABLES
   ***/

   char		*ptr;
   char		*shmat();
   char		*daddr		= NULL;


   /***
      BEGIN
   ***/

   /*
      attach a pointer to this chunk of physical memory for this process
   */
   if ((ptr = shmat(shmid, daddr, 0)) == (char *)-1)  {
/*      perror ("ERROR: shmat\007"); */
      return(NULL);
   }
   return(ptr);

}



/******************************************************************************
   				SHM_DETACH
******************************************************************************/

int	shm_detach (shm_addr)

	char	*shm_addr;
{

   /***
      LOCAL VARIABLES
   ***/

   int		shmdt();

   /***
      BEGIN
   ***/

   /*
      detach from this shared memory segment denoted by the start address
   */
   if (shmdt(shm_addr) == -1)  {
      perror ("ERROR: shmdt\007");
      return(0);
   }
   return(1);

}



/******************************************************************************
   				SHM_REMOVE
******************************************************************************/

int	shm_remove (shmid)

	int	shmid;
{

   /***
      LOCAL VARIABLES
   ***/

   int		val;
   struct	shmid_ds	*shm_buf;

   /***
      BEGIN
   ***/

   /*
      remove the shared memory segment
   */
   if ((val = shmctl(shmid, IPC_RMID, shm_buf)) == -1)  {
      perror ("ERROR: shmctl (remove)\007");
      return(-1);
   }
   return(val);

}



/******************************************************************************
   				SEM_REMOVE
******************************************************************************/

int	sem_remove (semid)

	int	semid;
{

   /***
      LOCAL VARIABLES
   ***/

   int		val;
   struct	shmid_ds	*shm_buf;

   /***
      BEGIN
   ***/

   /*
      remove the semaphore before exiting
   */
   val = semctl(semid, 0, IPC_RMID, shm_buf);
/*   assert( val != -1 ); */
   return(val);

}



/******************************************************************************
   				SEM_WAIT
******************************************************************************/

int	sem_wait (semid)

	int	semid;
{

   /***
      LOCAL VARIABLES
   ***/

   struct	sembuf	sem_wait_buf;
	int id;

   /***
      BEGIN
   ***/

   /*
      set the sembuf structure so that the semaphore is decremented by one
      if the critical section is found to be vacant or else wait if it is
      occupied
   */
   sem_wait_buf.sem_num	=  0;
   sem_wait_buf.sem_op	= -1;
   sem_wait_buf.sem_flg	= SEM_UNDO;

   id = semop(semid, &sem_wait_buf, 1);
/*	assert( id != -1 ); */
   return(id);

} 



/******************************************************************************
   				SEM_SIGNAL
******************************************************************************/

int	sem_signal (semid)

	int	semid;
{
   struct	sembuf	sem_signal_buf;
	int id;

   /*
      set the sembuf structure so that the semaphore is incremented by one
      upon leaving the critical section
   */
   sem_signal_buf.sem_num	=  0;
   sem_signal_buf.sem_op	=  1;
   sem_signal_buf.sem_flg	= SEM_UNDO;

   id = semop(semid, &sem_signal_buf, 1);
/*	assert( id != -1 ); */
   return(id);

}

/* 
These values derived from sem_get, sem_wait, sem_signal.
They are the value of semval for locked (would cause waiting) and
unlocked states.
*/
#define IS_LOCKED	0
#define NOT_LOCKED	1

	int /* boolean */
sem_get_lock( semid )
    int semid;
	{
    int id;
	int value;

	if ( semid == -1 ) return (-1);
    value = semctl( semid, 0, GETVAL, 1 );
	switch( value )
		{
		case IS_LOCKED: return 1;
		case NOT_LOCKED: return 0;
		default: return( -1 );
		}
	}

	void
sem_set_lock( semid, value )
    int semid;
	int value;	/* boolean */
	{
    int id;
	int value;

	if ( semid == -1 ) return;
    value = semctl( semid, 0, SETVAL, value ? IS_LOCKED : NOT_LOCKED );
	/* 
	POTENTIAL BUG: what happens if this runs in the middle
	of a sem_wait() or sem_signal() operation?
	*/
	}



/************************************************************************/

/*
These functions implement a malloc()/free() style model in shared
memory.  The implementation allocates four more bytes than needed and
places the shared memory ID in the first four bytes, then returns the
address of the fifth.  Thus, local and shared pointers (IDs) can be
freely converted back and forth.

This model allows for other implementations of shared memory.

NOTE: the shared memory macros are taken from Surfer for convenience.
*/


	tLocalAddress
shm_malloc( size )
	unsigned size;	/* in bytes */
	{
	tSharedAddress sharedSelf;
	tPointer self;

	if ( size <= 0 ) return (NULL);
	sharedSelf = SALLOCATE( size + sizeof(int) );

	if ( sharedSelf == NO_SHARED_ADDRESS ) return (NULL);

	self = (tPointer) shm_attach( sharedSelf );

	if ( self == (tPointer) NULL ) return (NULL);

	*(int *)self = sharedSelf;

	return (tLocalAddress) (self + sizeof(int));
	}
	
	tSharedAddress
shm_get_shared_address( self )
	tLocalAddress self;
	{
	char *temp = (tPointer)self;
	if ( temp == NULL ) return (NULL);
	temp -= sizeof(int);
	return *((tSharedAddress *)temp);
	}
	
	tLocalAddress
shm_get_local_address( self )
	tSharedAddress self;
	{
	tPointer temp;
	/*
	assert( self != -1 );
	*/
	if ( self == -1 ) return( NULL );
	if ( ( temp = (tPointer) shm_attach( self ) ) == NULL ) return( NULL );
	/*
	assert( temp );
	*/
	return (tLocalAddress)(temp + sizeof(int));
	}
	
	void
shm_destroy_local_address( self )
	tLocalAddress self;
	{
	tPointer temp = (tPointer)self;
/* this assert can dump core when exiting while looping!
	assert( temp );
*/
	if (temp == (tPointer) NULL) return;


	DETACH( temp - sizeof(int) );
	}

	void
shm_free( self )
	tLocalAddress self;
	{
	int id;
	tPointer temp;
	
	if ( self == NULL ) return;
	temp = self;
	temp -= sizeof(int);
	id = *((int *)temp);
	DETACH( temp );
	SDEALLOCATE( id );
	}

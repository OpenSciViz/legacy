#ifndef INCLUDED_FIPC.H
#define INCLUDED_FIPC.H


/*------------------  DEFINES  -----------------------*/

	/* key for all shared memory allocs */

#define	FAST_KEY			0

	/* commands from the hub */

#define	COM_MODULE_QUIT			1

	/* requests from the modules to the hub */

#define	REQ_SHMEM			1
#define	REQ_LIST_OBJECTS		2
#define	REQ_SHMEM_REMOVE		3
#define	REQ_ADD_OBJECT			4
#define	REQ_MODULE_QUIT			5
#define	REQ_ENVIRONMENT_QUIT		6
#define	REQ_MODULE_START		7
#define	REQ_ENVIRONMENT_UPDATE		8
#define	REQ_START_MODULES		9

#define	REQ_MAKE_INSERT_NODE		10
#define	REQ_DELETE_DATA_I		11
#define	REQ_DELETE_LIST			12
#define	REQ_COUNT_LIST			13
#define	REQ_RENUMBER_LIST		14
#define	REQ_COPY_LIST			15
#define	REQ_MOVE_LIST			16
#define	REQ_SEARCH_LIST_ID		17
#define	REQ_CLEAR_LIST_ID		18
#define	REQ_MOVE_LIST_ID		19
#define	REQ_COPY_LIST_ID		20
#define	REQ_GET_LIST_IDS		21

#define	REQ_GET_OBJECT			22

#define	CINFO_SIZE	1024

/*---------------------  TYPEDEFS  --------------------------*/


typedef	struct	fast_infobuf_def  {
	int	command;
	int	info1;
	int	info2;
	int	info3;
	char	cinfo[CINFO_SIZE];
}
Fast_infobuf;

Fast_infobuf	fast_infobuf;


struct	shmid_table_def  {
	int	id;
	char	*addr;
	struct	shmid_table_def	*next;
};


/*------------------------  GLOBALS  --------------------------*/


int	Module_rsock, Module_wsock;


/*-------------------------------------------------------------*/


#endif













#ifndef	INCLUDED_SHM_DEFS
#define	INCLUDED_SHM_DEFS


/*

	SHM_DEFS.H

	Description:

	  Contains global definitions for interprocess communication
	  structures and variables

	Authors:

	  Paul Kelaita, Sterling Federal Systems, Inc.
	  9/88

HISTORY:
	- Added shm_malloc() and friends	26 Feb 90	Al Globus
*/

/***
   INCLUDES
***/

#include <signal.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>

/***
   DEFINES
***/

#define			SHM_PERMFLAGS_CREAT	0666|IPC_CREAT|IPC_EXCL
#define			SEM_PERMFLAGS_CREAT	0666|IPC_CREAT|IPC_EXCL

#define			SHM_PERMFLAGS		0666
#define			SEM_PERMFLAGS		0666

int sem_get_lock( int );
void sem_set_lock( int, int );	/* boolean */


/*** object management ***/
#define NO_SHARED_ADDRESS	-1	/* used for checking */

typedef char * tLocalAddress;
typedef unsigned int tSharedAddress;
	/* 
	In segment architecture: 32 bits: 8 bits offset, 16 bits starting location.
	In current architecture: shared memory ID of data where the first 4 bytes
	are the shared memory ID and things returning local pointers return the
	address of the first 5th byte of the shared memory
	*/

tLocalAddress shm_malloc( unsigned size );
tSharedAddress shm_get_shared_address( tLocalAddress );
tLocalAddress shm_get_local_address( tSharedAddress );
void shm_destroy_get_local_address( tLocalAddress );
void shm_free( tLocalAddress );


#endif



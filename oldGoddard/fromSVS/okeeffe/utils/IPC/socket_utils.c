/*++ socket_utils.c
 *
 * PURPOSE :
 *
 *		These utilities handle everything to do with the way that
 *		Unix sockets are handled within FAST
 *
 * AUTHORS(S) :
 *
 *		Paul Kelaita
 *		NASA Ames Research Center
 *		Sterling Federal Systems, Inc.
 *
 * REVISION HISTORY :
 *
 *
 * NOTES :
 *
 *		FUNCTIONS DECLARED IN THIS FILE :
 *
 *			socket_init
 *			socket_listen
 *			socket_accept_and_establish
 *			socket_establish_and_accept
 *			socket_connect
 *			socket_read
 *			socket_write
 *
 *		EXTERNAL FUNCTIONS CALLED IN THIS FILE :
 *
 *
 *		GLOBALS USED IN THIS FILE :
 *
 *
 *		INCLUDES :
 *
 *			sys/types.h	-|
 *			sys/socket.h	-|
 *			sys/time.h	-|-- all files necessary for sockets
 *			netinet/in.h	-|
 *			netdb.h		-|
 *
 *
 *--*/


/*------------------------------- INCLUDES ---------------------------------*/

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <netdb.h>

/*-------------------------- EXTERNAL FUNCTIONS ----------------------------*/


/*--------------------------  GLOBAL VARIABLES -----------------------------*/


/*--------------------------------------------------------------------------*/


/*++ int	socket_init()
 *
 * PURPOSE :
 *
 *		initialize a stream socket of type Internet for reading
 *		or writing
 *
 * AUTHORS(S) :
 *
 *		Paul Kelaita
 *		NASA Ames Research Center
 *		Sterling Federal Systems, Inc.
 *
 * REVISION HISTORY :
 *
 *				
 * INPUT PARAMETERS :
 *
 *		None
 *		
 * OUTPUT PARAMETERS :
 *	
 *		None
 *
 * FUNCTION RETURN :
 *
 *		upon success, return a socket descriptor, else return 0
 *
 * NOTES :
 *
 *
 --*/


/*------------------------------- socket_init ------------------------------*/

int	socket_init()

{

   /***
      LOCAL VARAIBLES
   ***/

   int		sock;

   /***
      BEGIN
   ***/

   if ((sock	= socket(AF_INET, SOCK_STREAM, 0)) == -1)  {
      perror("socket\7");
      return(0);
   }

   return(sock);

}

/*--------------------------- END OF socket_init ---------------------------*/


/*++ int	socket_listen(sock, port)
 *
 * PURPOSE :
 *
 *		binds a port to a socket and initiates a willingness to
 *		accept connections on it
 *
 * AUTHORS(S) :
 *
 *		Paul Kelaita
 *		NASA Ames Research Center
 *		Sterling Federal Systems, Inc.
 *
 * REVISION HISTORY :
 *
 *				
 * INPUT PARAMETERS :
 *
 *		sock		- socket discriptor gotten from a socket_init
 *		port		- port to which to bind (0 means assign one)
 *		
 * OUTPUT PARAMETERS :
 *	
 *		None
 *
 * FUNCTION RETURN :
 *
 *		returns the port number that the socket was assiged,
 *		else returns 0
 *
 * NOTES :
 *
 *
 --*/


/*------------------------------- socket_listen ----------------------------*/

int	socket_listen(sock, port)

	int	sock, port;

{

   /***
      LOCAL VARAIBLES
   ***/

   int		length;
   struct	sockaddr_in	name;

   /***
      BEGIN
   ***/

   length	= sizeof(name);

   name.sin_family	= AF_INET;
   name.sin_addr.s_addr	= INADDR_ANY;
   name.sin_port	= htons(port);
   if (bind(sock, &name, length) == -1)  {
      perror("bind\7");
      return(0);
   }

   if (getsockname(sock, &name, &length) == -1)  {
      perror("getsockname\7");
      return(0);
   }

   if (listen(sock, 5) == -1)  {
      perror("listen\7");
      return(0);
   }

   return(name.sin_port);

}

/*--------------------------- END OF socket_listen -------------------------*/


/*++ int	socket_accept_and_establish(sock, mod_host, wsock)
 *
 * PURPOSE :
 *
 *		This routine is called by the hub.  It accepts a connection
 *		to a recently fired up module from which to read, and also
 *		establishes one to the same module in which to later write.
 *
 * AUTHORS(S) :
 *
 *		Paul Kelaita
 *		NASA Ames Research Center
 *		Sterling Federal Systems, Inc.
 *
 * REVISION HISTORY :
 *
 *				
 * INPUT PARAMETERS :
 *
 *		sock		- the socket descriptor which will be 
 *				  duplicated in order to get a new socket
 *				  to read from
 *		mod_host	- the host of that particular module (this
 *				  info came from 'fast.config')
 *		
 * OUTPUT PARAMETERS :
 *	
 *		wsock		- the write socket descriptor --
 *				  this needs to be known by hub for later use
 *
 * FUNCTION RETURN :
 *
 *		returns the read socket descriptor
 *
 * NOTES :
 *
 *
 --*/


/*------------------------------- socket_accept_and_establish --------------*/

int	socket_accept_and_establish(sock, mod_host, wsock)

	int	sock;
	char	*mod_host;
	int	*wsock;

{

   /***
      LOCAL VARAIBLES
   ***/

   int		rsock;
   int		length, mod_port;

   struct	sockaddr_in	in_name;

   /***
      BEGIN
   ***/

   /*
      accept module's port number from module
   */
   length	= sizeof(in_name);
   if ((rsock = accept(sock, &in_name, &length)) == -1)
      perror("accept\7");
   else {
      socket_read(rsock, (char *)&mod_port, sizeof(mod_port));
   }

   /*
      now create a connection for writing to the module
   */
   *wsock	= socket_init();
   /*
   sleep(1);
   */
   if (!socket_connect(*wsock, mod_host, mod_port))
      exit(1);

   /*
      return the read sock file descriptor
   */
   return(rsock);

}

/*--------------------------- END OF socket_accept_and_establish -----------*/


/*++ int	socket_establish_and_accept(hub_host, hub_port, rsock, wsock)
 *
 * PURPOSE :
 *
 *		This routine is called by each module.  It establishes a
 *		connection to the hub for writing then accepts a connection
 *		from the hub for reading
 *
 * AUTHORS(S) :
 *
 *		Paul Kelaita
 *		NASA Ames Research Center
 *		Sterling Federal Systems, Inc.
 *
 * REVISION HISTORY :
 *
 *				
 * INPUT PARAMETERS :
 *
 *		hub_host	- hostname of hub
 *		hub_port	- which port in hub to connect to
 *		
 * OUTPUT PARAMETERS :
 *	
 *		rsock		- socket descriptor used by module to read
 *		wsock		- socket descriptor used by module to write
 *
 * FUNCTION RETURN :
 *
 *		returns a module id (this is actually the write socket
 *		descriptor of the hub to this particular module)
 *
 * NOTES :
 *
 *
 --*/


/*------------------------------- socket_accept_and_establish --------------*/

int	socket_establish_and_accept(hub_host, hub_port, rsock, wsock)

	char	*hub_host;
	int	hub_port;
	int	*rsock;
	int	*wsock;

{

   /***
      LOCAL VARAIBLES
   ***/

   int		sock;
   int		length, mod_port;

   struct	sockaddr_in	in_name;

   /***
      BEGIN
   ***/

   /*
      create a socket and get port to read from hub
   */
   sock		= socket_init();
   mod_port	= socket_listen(sock, 0);

   /*
      now create a connection to hub and send my port
   */
   *wsock	= socket_init();
   /*
   sleep(1);
   */
   if (!socket_connect(*wsock, hub_host, hub_port))
      exit(1);
   socket_write(*wsock, (char *)&mod_port, sizeof(mod_port));

   /*
      accept a connection from hub
   */
   length	= sizeof(in_name);
   if ((*rsock = accept(sock, &in_name, &length)) == -1)
      perror("accept\7");

   /*
      return ok
   */
   return(1);

}

/*--------------------------- END OF socket_accept_and_establish -----------*/


/*++ int	socket_read(sock, buf, buf_size)
 *
 * PURPOSE :
 *
 *		read from a socket
 *
 * AUTHORS(S) :
 *
 *		Paul Kelaita
 *		NASA Ames Research Center
 *		Sterling Federal Systems, Inc.
 *
 * REVISION HISTORY :
 *
 *				
 * INPUT PARAMETERS :
 *
 *		sock		- read socket descriptor
 *		buf_size	- number of bytes to read
 *		
 * OUTPUT PARAMETERS :
 *	
 *		buf		- character buffer to hold data read
 *
 * FUNCTION RETURN :
 *
 *		int	number of bytes read
 *
 * NOTES :
 *
 *
 --*/


/*------------------------------- socket_read ------------------------------*/

socket_read(sock, buf, buf_size)

	int	sock;
	char	*buf;
	int	buf_size;

{

   /***
      LOCAL VARAIBLES
   ***/

   int	bytes_read = 0;

   /***
      BEGIN
   ***/

   memset(buf, '\0', buf_size);
   if ((bytes_read = read(sock, buf, buf_size)) != buf_size)  {
/*      perror("stream read\7") */ ;
   }
   return (bytes_read);
}

/*--------------------------- END OF socket_read ---------------------------*/


/*++ int	socket_write(sock, buf, buf_size)
 *
 * PURPOSE :
 *
 *		write to a socket
 *
 * AUTHORS(S) :
 *
 *		Paul Kelaita
 *		NASA Ames Research Center
 *		Sterling Federal Systems, Inc.
 *
 * REVISION HISTORY :
 *
 *				
 * INPUT PARAMETERS :
 *
 *		sock		- write socket descriptor
 *		buf		- character buffer to hold data to write
 *		buf_size	- number of bytes to write
 *		
 * OUTPUT PARAMETERS :
 *	
 *		None
 *
 * FUNCTION RETURN :
 *
 *		int	bytes written
 *
 * NOTES :
 *
 *
 --*/


/*------------------------------- socket_write -----------------------------*/

socket_write(sock, buf, buf_size)

	int	sock;
	char	*buf;
	int	buf_size;

{

   /***
      LOCAL VARAIBLES
   ***/

   int	bytes_written = 0;

   /***
      BEGIN
   ***/

   if ((bytes_written = write(sock, buf, buf_size)) != buf_size)
      perror("socket write\7");

   return (bytes_written);
}

/*-------------------------- END OF socket_write  --------------------------*/


/*++ int	socket_connect(sock, host, port)
 *
 * PURPOSE :
 *
 *		connect to a socket for future writing to
 *
 * AUTHORS(S) :
 *
 *		Paul Kelaita
 *		NASA Ames Research Center
 *		Sterling Federal Systems, Inc.
 *
 * REVISION HISTORY :
 *
 *				
 * INPUT PARAMETERS :
 *
 *		sock		- socket descriptor
 *		host		- host of machine to be writing to
 *		port		- port of machine to be writing to
 *		
 * OUTPUT PARAMETERS :
 *	
 *		None
 *
 * FUNCTION RETURN :
 *
 *		1 on success, 0 on failure
 *
 * NOTES :
 *
 *
 --*/


/*------------------------------- socket_connect ---------------------------*/

socket_connect(sock, host, port)

	int	sock;
	char	*host;
	int	port;

{

   /***
      LOCAL VARAIBLES
   ***/

   struct	sockaddr_in	name;
   struct	hostent		*hp;

   /***
      BEGIN
   ***/

   name.sin_family	= AF_INET;
   if ((hp = gethostbyname(host)) == 0)  {
      printf ("%s: unknown host\7\n", host);
      return(0);
   }

   bcopy(hp->h_addr, &(name.sin_addr.s_addr), hp->h_length);
   name.sin_port	= htons(port);

   if (connect(sock, &name, sizeof(name)) == -1)  {
      perror("stream connect\7");
      return(0);
   }

   return(1);

}

/*--------------------------- END OF socket_connect ------------------------*/


/*++ int	check_read_socket(rsock)
 *
 * PURPOSE :
 *
 *		This routine checks to see if there is anything coming in
 *		on the modules read socket
 *
 * AUTHORS(S) :
 *
 *		Paul Kelaita
 *		NASA Ames Research Center
 *		Sterling Federal Systems, Inc.
 *
 * REVISION HISTORY :
 *
 *				
 * INPUT PARAMETERS :
 *
 *		rsock		- read socket descriptor
 *		
 * OUTPUT PARAMETERS :
 *	
 *		None
 *
 * FUNCTION RETURN :
 *
 *		1 if there is something to read, 0 otherwise
 *
 * NOTES :
 *
 *
 --*/


/*------------------------------- check_read_socket ------------------------*/

int	check_read_socket(rsock)

	int	rsock;

{

   /***
      LOCAL VARAIBLES
   ***/

   fd_set	read_fds;

   struct	timeval		wait;

   int		retval;

   /***
      BEGIN
   ***/

   wait.tv_sec		= 0;
   wait.tv_usec		= 0;

   FD_ZERO(&read_fds);
   FD_SET(rsock, &read_fds);
   if ((retval = select(FD_SETSIZE, &read_fds, NULL, NULL, &wait))  == -1)
      perror("select\007");

   return(retval);
}

/*--------------------------- END OF check_read_socket ---------------------*/



/*---------------------- END OF MODULE socket_utils.c ----------------------*/

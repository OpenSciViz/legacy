#include <stdio.h>
#include <strings.h>
#include <math.h>
#include <gl/gl.h>
#include <device.h>
#include <malloc.h>

#define xmax 600
#define ymax 450

main(argc,argv)
int argc; char **argv;
{
   unsigned char gray[xmax*ymax], data[xmax*ymax];
   unsigned char clt_r[208], clt_g[208], clt_b[208];
   char *file[99999];
   int nr, ng, nb, fd, cnt=0, fd_clt;
   float exec_time;
   int i=0, j, npix=600, nscan=450, ti, tf, sz, i_start, i_stop;
   long tmp, ttob=0;
   unsigned short wrred = 0xffff, wrgrn = 0xffff, wrblu = 0xffff;
/* get the color table */
   fd_clt = open("/usr/people/xrhon/utils/colorg.hf",0);
   if( fd_clt <= 0 ) exit(1);
   read(fd_clt,clt_r,208);
   read(fd_clt,clt_g,208);
   read(fd_clt,clt_b,208);

   file[cnt] = malloc(80);

   while( (gets(file[cnt]) != NULL ) )
     file[++cnt] = malloc(80);

   i_start = 0;
   i_stop = cnt;

   if( argc > 1 )
   { 
    if( argv[1][1] == 'h' )
    {
	npix = 2*npix;
	nscan = 2*nscan;
    }
    else if( argv[1][1] == 'i' )
      ttob = 1;
   }
   if( argc > 2 ) 
   {
    if( argv[2][1] == 'h' )
    {
	npix = 2*npix;
	nscan = 2*nscan;
    }
    else if( argv[2][1] == 'i' )
      ttob = 1;
   }

   prefsize(npix,nscan);
   winopen("RGB_Animate");
   RGBmode();
   gconfig();
   cpack(0); clear();
   pixmode(PM_SIZE,8);
   if( ttob ) pixmode(PM_TTOB,1);
   qdevice(LEFTMOUSE);
   doublebuffer();


   rectzoom(npix/600.0,nscan/450.0);
 
  while( 1 )
  {
    for( i = i_start; i < i_stop; i++ )
    {
      if( qtest() )
        if( qread(&tmp) == LEFTMOUSE )
	  qread(&tmp);

      fd = open(file[i],0);
      nr = read(fd,data,600*450);
      close(fd);
      for( j = 0; j < 600*450; j++ )
        gray[j] = clt_r[data[j]];
        
	wrred = 0xffff; wrgrn = 0x0, wrblu = 0x0;
        RGBwritemask(wrred,wrgrn,wrblu);
        pixmode(PM_SHIFT,0); /* red */
        lrectwrite((Screencoord)(0),(Screencoord)(0),(Screencoord)(xmax-1),
			(Screencoord)(ymax-1),gray);

        wrred = 0x0; wrgrn = 0xffff, wrblu = 0x0;
        RGBwritemask(wrred,wrgrn,wrblu);
        pixmode(PM_SHIFT,8); /* green */
        lrectwrite((Screencoord)(0),(Screencoord)(0),(Screencoord)(xmax-1),
			(Screencoord)(ymax-1),gray);

        wrred = 0x0; wrgrn = 0x0, wrblu = 0xffff;
        RGBwritemask(wrred,wrgrn,wrblu);
        pixmode(PM_SHIFT,16); /* blue */
        lrectwrite((Screencoord)(0),(Screencoord)(0),(Screencoord)(xmax-1),
			(Screencoord)(ymax-1),gray);
        swapbuffers();
    }
 
    for( i = cnt-1; i >= 0; i-- )
    {
      if( qtest() )
        if( qread(&tmp) == LEFTMOUSE )
	  qread(&tmp);
      fd = open(file[i],0);
      nr = read(fd,data,npix*nscan);
      close(fd);

      for( j = 0; j < 600*450; j++ )
        gray[j] = clt_r[data[j]];

        wrred = 0xffff; wrgrn = 0x0, wrblu = 0x0;
        RGBwritemask(wrred,wrgrn,wrblu);
        pixmode(PM_SHIFT,0); /* red */
        lrectwrite((Screencoord)(0),(Screencoord)(0),(Screencoord)(xmax-1),
			(Screencoord)(ymax-1),gray);

        wrred = 0x0; wrgrn = 0xffff, wrblu = 0x0;
        RGBwritemask(wrred,wrgrn,wrblu);
        pixmode(PM_SHIFT,8); /* green */
        lrectwrite((Screencoord)(0),(Screencoord)(0),(Screencoord)(xmax-1),
			(Screencoord)(ymax-1),gray);

        wrred = 0x0; wrgrn = 0x0, wrblu = 0xffff;
        RGBwritemask(wrred,wrgrn,wrblu);
        pixmode(PM_SHIFT,16); /* blue */
        lrectwrite((Screencoord)(0),(Screencoord)(0),(Screencoord)(xmax-1),
			(Screencoord)(ymax-1),gray);
	swapbuffers();
    }
  }
}


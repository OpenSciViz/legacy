/* convert "globe.pixmap" to a list of dirbe resolution pixel values:
   0 - 393215 pixel numbers associated with 0 - 255 (one unsigned byte)
   values. */

#include <stdio.h>
#include <math.h>

main(argc,argv)
int argc; char **argv;
{
FILE *fp_pixmap, *fp_dirbe;
float latt, lon, uvec[3];
short res=9;
int i, j, pixel;
unsigned char data[1024], dirbe[393216];
 
   
   fp_pixmap = fopen("globe.pixmap","r");
   fp_dirbe = fopen("globe.dirbe","w");

   latt = 89.9;
   for( i = 0; i < 512; i++ )
   {
      latt = latt - 180.0 / 512.0;
      fread(data,1,1024,fp_pixmap);
      lon = -179.9;
      for( j = 0; j < 1024; j++ )
      {
	  lon = lon + 360.0 / 1024.0;
          uvec[2] = sin(latt/57.3);
	  uvec[0] = cos(latt/57.3) * cos(lon/57.3);
	  uvec[1] = cos(latt/57.3) * sin(lon/57.3);
	  upx_pixel_number(uvec,&res,&pixel);
	  dirbe[pixel] = data[j];
      }
   }

   for( i = 0; i < 393216; i++ ) fwrite(&dirbe[i],1,1,fp_dirbe);

   fclose(fp_pixmap);
   fclose(fp_dirbe);
}

; IDL Version 2.2.1 (IRIX mipseb)
; Journal File for xrhon@okeeffe
; Working directory: /usr/people/xrhon
; Date: Sat Jan 18 15:08:53 1992
 
plot,histogram(bytscl(norm))
oplot,histogram(bytscl((0.4+0.6*x^2)*sin(PI/4*xu)/cos(PI/4*xu)^2.0))
seed=1
xu=randomu(seed,100000)
norm=randomn(seed,100000)

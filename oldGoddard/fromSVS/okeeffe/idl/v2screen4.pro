pro v2screen4,zf

    if ((zf ne 1) AND (zf ne 2)) then zf=1

    set_plot,'X'
    !P.FONT=-1

    dsize=512
    window,xsize=dsize*zf,ysize=dsize*zf,retain=2
    erase
    loadct,4
    tvlct,r,g,b,/get         
    elm = (n_elements(r)) - 1
    ;!p.background = elm          ;for a white background
    r(elm) = 255
    g(elm) = r(elm)
    b(elm) = r(elm) 
    tvlct,r,g,b

    box=bytarr(dsize*zf,dsize*zf)
    box(*,*)=65
    tv,box

    topc=70
    topr=440

    box1=bytarr(405*zf,60*zf)
    box1(*,*)=40
    tv,box1,(topc-15)*zf,(topr-60)*zf

    tsize=1.3*zf
    tcolor=elm
    xyouts,(topc-1)*zf,(topr-20)*zf,'!17BEACONLESS SEARCH AND RESCUE',/device,size=tsize,$
           orientation=0,color=tcolor 
    xyouts,(topc-10)*zf,(topr-45)*zf,'USING SYNTHETIC APERTURE RADAR',/device,size=tsize,$
           orientation=0,color=tcolor 

    scolor=205

    box=bytarr(30*zf,30*zf)
    box(*,*)=40
    box(0*zf,*)=scolor
    box(29*zf,*)=scolor
    box(*,0*zf)=scolor
    box(*,29*zf)=scolor

    xyouts,(topc-22)*zf,(topr-120)*zf,'THE BENEFITS ARE:',/device,size=tsize,orientation=0,$
           color=tcolor

    tv,box,(topc-22)*zf,(topr-190)*zf
    xyouts,(topc-15)*zf,(topr-182)*zf,'1.',/device,size=tsize,orientation=0,color=tcolor
    xyouts,(topc+23)*zf,(topr-180)*zf,'Increased Survivability of Victims',/device,size=tsize,$
           orientation=0,color=tcolor

    tv,box,(topc-22)*zf,(topr-230)*zf
    xyouts,(topc-15)*zf,(topr-222)*zf,'2.',/device,size=tsize,orientation=0,color=tcolor
    xyouts,(topc+23)*zf,(topr-220)*zf,'Reduction in Search Time',/device,size=tsize,$
           orientation=0,color=tcolor

    tv,box,(topc-22)*zf,(topr-270)*zf
    xyouts,(topc-15)*zf,(topr-262)*zf,'3.',/device,size=tsize,orientation=0,color=tcolor
    xyouts,(topc+23)*zf,(topr-260)*zf,'Reduced Exposure of Search Forces',/device,size=tsize,$
           orientation=0,color=tcolor

    tv,box,(topc-22)*zf,(topr-310)*zf
    xyouts,(topc-15)*zf,(topr-302)*zf,'4.',/device,size=tsize,orientation=0,color=tcolor
    xyouts,(topc+23)*zf,(topr-300)*zf,'Result in Lower Cost/Search',/device,size=tsize,$
           orientation=0,color=tcolor
       
    return

end

      program latlon_lut	! create lat-lon table from quadsphere
      integer*4 qlut(1024,1024)
      real*4 uvec(3), lat(1024,1024), lon(1024,1024)
      integer*2 res/9/

      open(unit=10,form='unformatted',recordtype='fixed',recl=1024,
     &	   file='dirbe1024.lut',status='old')

      do i = 1, 1024
        read(10) (qlut(j,i), j=1, 1024)
      end do
      close(10)

      do i = 1, 1024
        do j = 1, 1024
          if( qlut(j,i) .ge. 0 ) then		! data here
            call upx_pixel_vector(qlut(j,i),res,uvec)
            lat(j,i) = asin(uvec(3)) / 4.0
      	    lon(j,i) = atan2(uvec(2),uvec(1)) / 4.0
          else if( qlut(j,i) .eq. -1 ) then		! mirror here
            lat(j,i) = 10.0 / 4.0
            lon(j,i) = 20.0 / 4.0
          else 					! background here
            lat(j,i) = -10.0 / 4.0
            lon(j,i) = -20.0 / 4.0
          end if
        end do
      end do

      open(unit=10,form='unformatted',recordtype='fixed',recl=1024,
     &	   file='lut3D_1024_latlon.ieee',status='new',carriagecontrol='none')

      do i = 1, 1024
        write(10) (lat(j,i), j=1, 1024)
      end do
      do i = 1, 1024
        write(10) (lon(j,i), j=1, 1024)
      end do
      close(10)

      end      

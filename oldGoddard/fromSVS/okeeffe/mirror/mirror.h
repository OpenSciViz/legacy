#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define XCYL 1024
#define YCYL  512
#define XMIRROR 1024
#define YMIRROR 1024
#define RES9 54*6144
#define BYTES 'b'
#define FLOATS 'f'

#ifdef MAIN
#define global
#else
#define global extern
#endif

global short Qcell_Res;

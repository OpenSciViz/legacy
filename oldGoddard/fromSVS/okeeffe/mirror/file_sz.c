#include "mirror.h"
#include <sys/stat.h> /* include "stat" datastructure def. */
typedef struct stat STAT;

int file_sz(char *file)
{
 STAT file_info;
 int sz;

  if( stat(file,&file_info) != 0 )
  {
    fprintf(stderr,"file_sz> Unable to get status of %s\n",file);
    exit(0);
  }
  sz = file_info.st_size;

  return( sz );
}

#include "mirror.h"

int init_3dlut(int sz, int *lut_3d)
{
  int n, fd=0;

  if( sz == 512 )
    fd = open("3Dlut.512",0);
  else if( sz == 1024 )
    fd = open("3Dlut.1024",0);

  if( fd <= 0 )
  {
    fprintf(stderr,"init_3dlut> Unable to open 3D LUT file!\n");
    exit(0);
  }

  if( (n = read(fd,lut_3d,4*sz*sz)) < 4*sz*sz )
    fprintf(stderr,"init_3dlut> Read only %d bytes from 3D LUT file!\n",n);
}


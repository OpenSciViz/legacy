#include "mirror.h"

#define SZ_512x256  131072
#define SZ_1024x512 524288

float *get_floats(int sz, char *file, int *x, int *y)
{
  float *buff;
  int i, fd = open(file,0);

  if( fd <= 0 ) 
  {
    fprintf(stderr,"get_bytes> Unable to open %s...\n",file);
    exit(0);
  }
 
  switch( sz )
  {
    case SZ_512x256: *x = 512; *y = 256;
		  buff = (float *) malloc(sz*sizeof(float));
	          read(fd,buff,sz*sizeof(float));
		  break;

    case SZ_1024x512: *x = 1024; *y = 512;
		  buff = (float *) malloc(sz*sizeof(float));
	          read(fd,buff,sz*sizeof(float));
		  break;

    default: if( sz < SZ_512x256 )
	     {
	       fprintf(stderr,"get_bytes> Input file < 512x256 bytes, padding with zeros...\n");
	       *x = 512; *y = 256;
	       buff = (float *) malloc(sz*sizeof(float));
	       read(fd,buff,sz*sizeof(float));
	       for( i = sz; i < SZ_512x256; i++ )
		 buff[i] = 0; 
	     }
	     else if( sz < SZ_1024x512 )
	     {
	       fprintf(stderr,"get_bytes> Input file < 1024x512 bytes, padding with zeros...\n");
	       *x = 1024; *y = 512;
	       buff = (float *) malloc(sz*sizeof(float));
	       read(fd,buff,sz*sizeof(float));
	       for( i = sz; i < SZ_1024x512; i++ )
		 buff[i] = 0; 
	     }
	     break;
  }

  close(fd);
  return( buff );
}

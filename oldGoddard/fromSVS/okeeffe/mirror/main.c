#define MAIN
#define DEBUG
#include "mirror.h"

main(int argc, char **argv, char **env)
{
  int fd, i, j, cell, xin, yin, xout, yout, zero_cnt, in_sz, out_sz;
  float *input, *output, *get_floats(), *get_bytes();
  static char infile[81], outfile[81];
  static float qcell[64*6144];
  static int lut_3d[XMIRROR*YMIRROR]; 

  if( argc < 4 )
  {
    fprintf(stderr,"mirror> usage: mirror [-b] [-f] infile outfile...\n");
    exit(0);
  }
  if( argv[1][0] != '-' )
  {
    fprintf(stderr,"mirror> usage: mirror [-b] [-f] infile outfile...\n");
    exit(0);
  }

/* initialize our only global: */
  Qcell_Res = 9;

  switch( argv[1][1] )
  {
    case BYTES: argv++; /* byte input file */
	                /* assume the next arguments are the names of the input & output files */
  		strcpy(infile,argv[1]);
  		strcpy(outfile,argv[2]);
		in_sz = file_sz(infile); /* in bytes */
#ifdef DEBUG
  fprintf(stderr,"mirror> input file size = %d bytes\n",in_sz);
#endif
		input = get_bytes(in_sz,infile,&xin,&yin);
		xout = xin;
		yout = 2*yin;
#ifdef DEBUG
  fprintf(stderr,"mirror> input image dimensions = %d %d\n",xin,yin);
#endif
		output = (float *) malloc(xout*yout*sizeof(float));
		break;

    case FLOATS: argv++; /* float input file */
	                 /* assume the next arguments are the names of the input & output files */
  		 strcpy(infile,argv[1]);
  		 strcpy(outfile,argv[2]);
		 in_sz = file_sz(infile)/4; /* in floats */
#ifdef DEBUG
  fprintf(stderr,"mirror> input file size = %d floats\n",in_sz);
#endif
		 input = get_floats(in_sz,infile,&xin,&yin);
		 xout = xin;
		 yout = 2*yin;
#ifdef DEBUG
  fprintf(stderr,"mirror> input image dimensions = %d %d\n",xin,yin);
#endif
		 output = (float *) malloc(xout*yout*sizeof(float));
		 break;

    default: fprintf(stderr,"mirror> usage: mirror [-b] [-f] infile outfile...\n");
	     exit(0);
	     break;
  }

  zero_cnt = fill_qcells(xin,yin,input,qcell);
#ifdef DEBUG
  fprintf(stderr,"mirror> number of unused quadsphere cells = %d\n",zero_cnt);
#endif

  init_3dlut(xout,lut_3d);

  for( j = 0; j < yout; j++ )
    for( i = 0; i < xout; i++ )
      output[j*xout + i] = qcell[lut_3d[j*xout + i]];

  fd = creat(outfile,0644);
  write(fd,output,4*xout*yout);
  close(fd);
}


#include "mirror.h"

int fill_qcells(int xin, int yin, float *input, float *qcell)
{
 int i, j, cell, zero_cnt=0, bad_cnt=0;
 float lat, lon, del_lat, del_lon, uvec[3];
 static unsigned short qcell_cnt[RES9];

  del_lat = 180.0/57.3/yin;
  lat = 90.0/57.3 - del_lat;
  lon = del_lon = 360.0/57.3/xin;
  
  for( j = 0; j < yin; j++, lat -= del_lat )
  {
    uvec[2] = sin(lat);
    for( i = 0; i < xin; i++, lon += del_lon )
    {
      uvec[0] = cos(lat)*cos(lon);
      uvec[1] = cos(lat)*sin(lon);
      upx_pixel_number_(uvec,&Qcell_Res,&cell); /* fortran call */
      if( cell >= 0 )
      {
	qcell[cell] += input[j*xin + i];
	qcell_cnt[cell] += 1;
      }
      else
	bad_cnt++;
    }
  }
  fprintf(stderr,"fill_qcells> bad quadcell number count= %d...\n",bad_cnt);

  for( cell = 0; cell < RES9; cell++ )
    if( qcell_cnt[cell] > 0 )
      qcell[cell] = qcell[cell] / qcell_cnt[cell];
    else
      zero_cnt++;

  return( zero_cnt );
}

#include "mirror.h"

#define SZ_512x256  131072
#define SZ_1024x512 524288

float *get_bytes(int sz, char *file, int *x, int *y)
{
  char *buff;
  float *ret_buff;
  int i, fd = open(file,0);

  if( fd <= 0 ) 
  {
    fprintf(stderr,"get_bytes> Unable to open %s...\n",file);
    exit(0);
  }

  buff = (unsigned char *) malloc(sz);
 
  switch( sz )
  {
    case SZ_512x256: *x = 512; *y = 256;
		  ret_buff = (float *) malloc(sz*sizeof(float));
	          read(fd,buff,sz);
	          for( i = 0; i < sz; i++ )
		    ret_buff[i] = buff[i];
		  break;

    case SZ_1024x512: *x = 1024; *y = 512;
		  ret_buff = (float *) malloc(sz*sizeof(float));
	          read(fd,buff,sz);
	          for( i = 0; i < sz; i++ )
		    ret_buff[i] = buff[i];
		  break;

    default: if( sz < SZ_512x256 )
	     {
	       fprintf(stderr,"get_bytes> Input file < 512x256 bytes, padding with zeros...\n");
	       *x = 512; *y = 256;
	       ret_buff = (float *) malloc(SZ_512x256*sizeof(float));
	       read(fd,buff,sz);
	       for( i = 0; i < sz; i++ )
		 ret_buff[i] = buff[i];
	       for( i = sz; i < SZ_512x256; i++ )
		 ret_buff[i] = 0.0; 
	     }
	     else if( sz < SZ_1024x512 )
	     {
	       fprintf(stderr,"get_bytes> Input file < SZ_1024x512 bytes, padding with zeros...\n");
	       *x = 1024; *y = 512;
	       ret_buff = (float *) malloc(SZ_1024x512*sizeof(float));
	       read(fd,buff,sz);
	       for( i = 0; i < sz; i++ )
		 ret_buff[i] = buff[i];
	       for( i = sz; i < SZ_1024x512; i++ )
		 ret_buff[i] = 0.0; 
	     }
	     break;
  }

  free( buff );
  close(fd);
  return( ret_buff );
}

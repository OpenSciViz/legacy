#define XCYL 1024
#define YCYL  512
#define XMIRROR 1024
#define YMIRROR 1024
#define BYTES 'b'
#define FLOATS 'f'

float 3dlut[XMIRROR*YMIRROR], qcell[64*6144];

main(int argc, char **argv, char **env)
{
  short res = 9;
  int i, j, cell, xin, yin, xout, yout;
  float lat, lon, *input, *output;
  static char infile[81], outfile[81];

  if( argc < 4 )
  {
    fprintf(stderr,"mirror> usage: mirror [-b] [-f] infile outfile...\n");
    exit(0);
  }
  if( argv[1][0] != '-' )
  {
    fprintf(stderr,"mirror> usage: mirror [-b] [-f] infile outfile...\n");
    exit(0);
  }
  switch( argv[1][1] )
  {
    case BYTES: argv++; /* byte input file */
	                /* assume the next arguments are the names of the input & output files */
  		strcpy(infile,argv[1]);
  		strcpy(outfile,argv[2]);
		in_sz = file_sz(infile); /* in bytes */
		out_sz = 4*2*in_sz; /* in floats */
		input = get_bytes(in_sz,infile,&xin,&yin);
		xout = xin;
		yout = 2*yin;
		output = (float *) malloc(out_sz*sizeof(float));
		fill_qcells(BYTES,xin,yin,input,qcell);
		break;

    case FLOATS: argv++; /* float input file */
	                 /* assume the next arguments are the names of the input & output files */
  		 strcpy(infile,argv[1]);
  		 strcpy(outfile,argv[2]);
		 in_sz = file_sz(infile)/4; /* in floats */
		 out_sz = 2*in_sz; /* in floats */
		 input = get_floats(in_sz,infile,&xin,&yin);
		 xout = xin;
		 yout = 2*yin;
		 output = (float *) malloc(out_sz*sizeof(float));
		 fill_qcells(FLOATS,xin,yin,input,qcell);
		 break;

    default: fprintf(stderr,"mirror> usage: mirror [-b] [-f] infile outfile...\n");
	     exit(0);
	     break;
  }

  init_3dlut(3dlut);

  for( j = 0; j < yout; j++ )
    for( i = 0; i < xout; i++ )
      output[j*xout + i] = qcell[3dlut[j*xout + i]];

  fd = creat(outfile,0644);
  write(fd,output,4*out_sz);
  close(fd);
}


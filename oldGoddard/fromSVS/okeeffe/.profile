#ident	"$Header: /SRC.H/root/usr/src/cmd/sadmin/etc/RCS/stdprofile,v 1.5 87/11/05 13:51:44 yohn Exp $"
#	This is the default standard profile provided to users.
#	They are expected to edit it to meet their own needs.

umask 022

stty line 1 erase '^H' kill '^U' intr '^C' echoe 
eval `tset -s -Q`

# list directories in columns
ls()	{ /bin/ls -C $*; }

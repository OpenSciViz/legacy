#include <math.h>  
/*#define N 128 */
#define N 4

typedef struct complex { float r; float i; } COMPLEX;
main(argc,argv)
int argc;
char **argv;
{
  COMPLEX *spectrm;
  float k1, k2, *power;
  int i, fd, Ndim[2] = { N, N };

  k1 = 2.0 * 3.1415926 / 10.0;	/* 10.0 cm is smallest wavelength */
  k2 = 2.0 * 3.1415926 / 50.0;	/* 100.0 cm is largest wavelength */
  if( argc > 1 ) k1 = atof(argv[1]);
  if( argc > 2 ) k2 = atof(argv[2]);
  spectrm = (COMPLEX *) malloc(N*N*sizeof(COMPLEX));
  power = (float *) malloc(N*N*sizeof(float));

/*  init(k1,k2,spectrm);  */
  init_spec(spectrm);

/* transform */
  fourn((&spectrm->r - 1),(Ndim-1),2,1);

  modulus(spectrm,power);

/*
  fd = creat("power.dat",0644);
  for( i = 0; i < N; i++ )
    write(fd,&power[i],N);
  close(fd);
*/    
/* inverse transform */
  fourn((&spectrm->r - 1),(Ndim-1),2,-1);

  modulus(spectrm,power);
}
 
int init_spec(c)
COMPLEX c[N][N];
{
  int i, j;

  for( i = 0; i < N; i++ )
    for( j = 0; j < N; j++ )
    {
      c[i][j].r = (1+i) * (1+j);
      c[i][j].i = 0.0;
    }
  return;
}

int init(k1,k2,c)
float k1, k2;
COMPLEX c[N][N];
{ 
  int i, j;
  float delx=1.0/N, dely=1.0/N;
  float x=0.0, y=0.0, mean=0.0;
  for( i = 0; i < N; i++ )
  { 
    for( j = 0; j < N; j++ )
    {
      c[i][j].i = 0.0;
      c[i][j].r = cos(k1*x)*cos(k1*y) + cos(k2*x)*cos(k2*y);
      mean = mean + c[i][j].r;
      x = x + delx;
    }
    y = y + dely;
  }
  mean = mean / (N-1) / (N-1);
  for( i = 0; i < N; i++ )
    for( j = 0; j < N; j++ )
      c[i][j].r = c[i][j].r - mean;

  return;
}

int modulus(c,a)
COMPLEX c[N][N];
float a[N][N];
{
  int i, j;

  for( i = 0; i < N; i++ )
    for( j = 0; j < N; j++ )
      a[i][j] = sqrt(pow(c[i][j].r,2.0) + pow(c[i][j].i,2.0));

  return;
}

int Imagin(n,a,c)
int n;
float a[];
COMPLEX c[];
{
  while( --n >= 0 )
  {
    c[n].r = 0.0;
    c[n].i = a[n];
  }
}

int Cexp(n,c)
int n;
COMPLEX c[];
{
  float tmp;

  while( --n >= 0 )
  {
    tmp = exp(c[n].r);
    c[n].r = tmp * cos(c[n].i);
    c[n].i = tmp * sin(c[n].i);
  }
}

int CRmul(n,c,a)
int n;
COMPLEX c[];
float a[];
{
  int i, nn;
  COMPLEX mean;

  nn = n;
  mean.r = 0.0;
  mean.i = 0.0;

  while( --n >= 0 )
  {
    c[n].r = a[n] * c[n].r;
    c[n].i = a[n] * c[n].i;
  }
/* and take the opportunity to remove any DC component 
  for( i = 0; i < nn; i++ )
  {
    mean.r = mean.r + c[i].r;
    mean.i = mean.i + c[i].i;
  }
  mean.r = mean.r / (nn-1);
  mean.i = mean.i / (nn-1);
  for( i = 0; i < nn; i++ )
  {
    c[i].r = c[i].r - mean.r;
    c[i].i = c[i].i - mean.i;
  }
*/

  return;
}

int Real(n,c,a)
int n;
COMPLEX c[];
float a[];
{
  while( --n >= 0 )
    a[n] = c[n].r;
}

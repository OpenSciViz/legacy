/* Pierson - Neumann spectrum given by Borrego and Machado
   in their 1985 Applied Optics paper. all units are in cm. */

#define NN 512
#include <math.h>

int PN_spctrm(windsp,N,kx,ky,array)
float windsp,kx[],ky[],array[NN][NN];
int N;
{
  float C=30500.0, g=981.0, k;
  int i, j;

  for( i = 0; i < N; i++ )
    for( j = 0; j < N; j++ )
    {
      k = sqrt( kx[i]*kx[i] + ky[j]*ky[j] );
      if( k == 0.0 )
        array[i][j] = 0.0;
      else if( kx[i] == 0.0 ) 
        array[i][j] = 0.0;
      else
        array[i][j] = C / 2.0 / pow(g,2.5) * exp(-2.0*g/k/windsp/windsp) *
      			pow(cos(atan(ky[j]/kx[i])),2.0);
    }

  return;
}

#include <stdio.h>
#include <math.h>

main(argc,argv)
int argc;
char **argv;
{
  int i, j, fd, nb;
  float sea[512][512], norm[512][512][3];
  float normalization, mean=0.0, var=0.0;
  float min = 99999999.9, max = -99999999.9, mag;

/* first record might be a header */
  if( argc > 1 )
    if( argv[1][1] == 'h' ) nb = read(0,&sea[0][0],4*512);

  for( i = 0; i < 512; i++ )
  {
    read(0,sea[i],4*512);
    if( argc > 1 )
      if( argv[1][1] == 'h' ) write(1,sea[i],4*512);
  }
  for( i = 0; i < 512; i++ )
    for( j = 0; j < 512; j++ )
    {
      mean = mean + sea[i][j];
      if( sea[i][j] > max ) max = sea[i][j];
      if( sea[i][j] < min ) min = sea[i][j];
    }

  mean = mean / 512 / 512;
  for( i = 0; i < 512; i++ )
    for( j = 0; j < 512; j++ )
      var = var + (sea[i][j] - mean)*(sea[i][j] - mean);

  var = var / 512 / 512;

  fprintf(stderr,"elevation: min= %f  max= %f  mean= %f  rms= %f\n",
		min,max,mean,sqrt(var));
  mean = 0.0;
  var = 0.0;
  min = 1.0;
  max = -1.0;/* and do the slopes too, first x: */
  for( i = 0; i < 512; i++ )
  {
    read(0,sea[i],4*512);
    if( argc > 1 )
      if( argv[1][1] == 'h' ) write(1,sea[i],4*512);
    for( j = 0; j < 512; j++ )
      norm[i][j][0] = sea[i][j];
  }
/* then y: */
  for( i = 0; i < 512; i++ )
  {
    read(0,sea[i],4*512);
    if( argc > 1 )
      if( argv[1][1] == 'h' ) write(1,sea[i],4*512);
    for( j = 0; j < 512; j++ )
    {
      norm[i][j][1] = sea[i][j];
/* and normalize: */
      mag = sqrt( 1.0 + sea[i][j]*sea[i][j] + norm[i][j][0]*norm[i][j][0] );
      norm[i][j][0] = norm[i][j][0] / mag;
      norm[i][j][1] = norm[i][j][1] / mag;
      norm[i][j][2] = 1.0 / mag;
/* find min max z components: */
      if( norm[i][j][2] > max ) max = norm[i][j][2];
      if( norm[i][j][2] < min ) min = norm[i][j][2];
      mean = mean + norm[i][j][2];
    }
  }
  mean = mean / 512 / 512;
  for( i = 0; i < 512; i++ )
    for( j = 0; j < 512; j++ )
      var = var + (norm[i][j][2] - mean)*(norm[i][j][2] - mean);

  var = var / 512 / 512;

  fprintf(stderr,"normals: min= %f  max= %f  mean= %f  rms= %f\n",min,max,mean,sqrt(var));
}

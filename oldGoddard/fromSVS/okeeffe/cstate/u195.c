/*
  the argument here is the frictional windspeed at air sea
	interface as described by pierson-stacy
	the arrays Ufrict, U10, Uat195 are defined in the block
	file windsp.h */

#include "wind.h"

float U195(ustar)
float ustar;
{
  int i;
  float fract, ret_val;
/*
  search Ufrict array for value nearest to ustar and return
  the 'appropriate' value for U195
*/

  for( i = 1; i < NSP; i++ )
  {
    if( ustar == Ufrict[i-1] )
	ret_val = Uat195[i-1];
    else if( (ustar >= Ufrict[i-1]) && (ustar <= Ufrict[i]) ) 
    {
	fract = (ustar-Ufrict[i-1])/(Ufrict[i]-Ufrict[i-1]);
	ret_val = (1.0-fract)*Uat195[i-1] + fract*Uat195[i];
    }
  }
  return(ret_val);
}

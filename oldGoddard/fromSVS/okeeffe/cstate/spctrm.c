#include <math.h>
#define NN 512
/* Pierson - Stacy semi-empirical (fully developed) sea spectrum,
given the wind speed at a fixed height above mean elevation level.
external function subroutines used are: ufrct & Uat195
coordinate system assumes wind is blowing in the positive
x direction
*/

int spctrm(windsp,elevat,N,kx,ky,array)
float windsp,elevat,kx[],ky[],array[][NN];
int N;
{
  float k,spk=0.0,spkth=0.0,p,Du,ustar,a1=0.0,a2=0.0,expnt;
  float alpha=0.0081,beta=0.73,Enug=0.0001473,k1,k2=0.359,k3=0.942;
  float knu,km=3.63,umin=12.0,U19p5;
  float pi=3.1415926, g=981.5;
  int i, j, fd;
  float Ufrct(), U195();

  ustar = Ufrct(windsp,elevat);
  U19p5 = U195(ustar) * 100.0; /* convert to cm */
  Du = 1.274 + .0268*ustar + .0000603*ustar*ustar;
  Du = Du * Du;
  p = log10(Du*umin/ustar) / log10(k3/k2);
  knu = 0.5756*km*sqrt(ustar) / exp(log(Du)/6.0); /* ie. Du to the 1/6 */
  k1 = k2*(umin/ustar);
  k1 = k1 * k1;

  for( j = 0; j < N; j++ )
    for( i = 0; i < N; i++ )
    {
	k = sqrt(kx[i]*kx[i] + ky[j]*ky[j]);
	if( k == 0.0 ) spk = 0.0;

	if( (k <= k1) && (k > 0.0) )
        {
          expnt = (g/(U19p5*U19p5)/k);
          expnt = expnt * expnt;
          expnt = exp(-beta*expnt);
	  spk = alpha/2.0/(k*k*k) * expnt;
        }
	if( (k > k1) && (k <= k2) )
	  spk = alpha/2.0/sqrt(k1)/exp(5*log(k)/2.0);
	if( (k > k2) && (k <= k3) )
      	  spk = alpha*Du/2.0/exp(p*log(k3))/exp((3.0 - p)*log(k));
	
	if( (k > k3) && (k <= knu) )
	  spk = alpha*Du/2.0/(k*k*k);

	if( k > knu )
	  spk = Enug * (ustar*ustar*ustar) * (km*km*km*km*km*km) / 
      		(k*k*k*k*k*k*k*k*k);

	if ( k > 0.0)
        {
	  expnt = exp( -g*g/2.0/k*k/pow(U19p5,4.0) );
	  spkth = 8.0/3.0/pi * pow(fabs(kx[i])/k,4.0) * (1.0 - expnt) +
     		1.0/pi * expnt; /* * (1.0 + a1*(kx[i]*kx[i] - ky[j]*ky[j])/k/k) +
      		a2*( pow((kx[i]*kx[i] - ky[j]*ky[j])/k/k,2.0) -
     			pow(2.0*kx[i]*ky[j]/k/k,2.0) ); */
        }
	array[i][j] = spk * spkth;
/*
  spectrum is zero for negative kx's, ie waves propogating downwind
	if( kx[i] < 0.0 ) 
	  array[i][j] = 0.0;
*/
     }
/*
  force spectrum to zero at origin and near nyquist kx & ky:
*/
  array[0][0] = 0.0;
  for( j = 0; j < N; j++ )
  {
    array[j][N/2] = 0.0;
    array[N/2][j] = 0.0;
  }
/* store this in a file for inspection by IDL: */
  fd = creat("piers512.t0",0644);
  write(fd,array,4*512*512);
  close(fd);
  return;
}

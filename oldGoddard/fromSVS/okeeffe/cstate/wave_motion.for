      subroutine wave_motion(N,time,kx,ky,spectrm_0,spectrm_t)
      integer*4 N
      real*4 time
      real*4 kx(N), ky(N)
      COMPLEX spectrm_0(N,N), spectrm_t(N,N)
      integer*4 i, j
      real*4 vel_min/23.2/, k_min/3.639/, root2/1.4142/, pi_2/3.1415926/
      real*4 shift, vel, k
      COMPLEX phase

      pi_2 = 2.0 * pi_2
! from Kinsman pg 175 - 177 
! vel = vel_min / root2 * sqrt( k_min / k + k / k_min );
! and given the spectrum at t=0 create it at t=time > 0 by simply
! multiplying each spectral component by its appropriate phase shift
! "exp(-2i(pi)*k*shift(t)" where shift(t) is simply the velocity of the
! wave * time.

      vel_min = vel_min / root2
      do i = 1, N
        do j = 1, N
          k = sqrt( kx(i)*kx(i) + ky(j)*ky(j) )
          if( k .gt. 0.0 ) then
            vel = vel_min * sqrt( k_min/k + k/k_min )
          else
      	    vel = 0.0
          end if
!          shift = pi_2 * k * vel * time
! shift really should be < pi at all times (i think)
          shift = k * vel * time	
          phase = cmplx(cos(shift),sin(shift))
! and finally perform the complex multiply of the phase and the spectrum 
!   components 
          spectrm_t(i,j) = phase * spectrm_0(i,j)
        end do
      end do

      return

      end

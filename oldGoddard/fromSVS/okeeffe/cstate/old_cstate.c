
#include <math.h>  
#define N 512
#define t_MAX 0.0

typedef struct complex { float r; float i; } COMPLEX;
void spctrm();
float windsp=10.75, elevat=19.5;

main(argc,argv)
int argc;
char **argv;
{
  COMPLEX *spectrm, *kx_spectrm, *ky_spectrm;
  float kmax, time=0.0, sea[N][N], slopx[N][N], slopy[N][N];
  int seed=12345678, i, fd;

  srand(seed);

  kmax = 2.0 * 3.1415926 / (0.5);	/* 0.5 cm is smallest wavelength */
  if( argc > 1 ) kmax = atof(argv[1]);
  if( argc > 2 ) windsp = atof(argv[2]);

  spectrm = (COMPLEX *) malloc(N*N*sizeof(COMPLEX));
  kx_spectrm = (COMPLEX *) malloc(N*N*sizeof(COMPLEX));
  ky_spectrm = (COMPLEX *) malloc(N*N*sizeof(COMPLEX));

  while( time <= t_MAX )
  {
    cstate(seed,kmax,time,spectrm,kx_spectrm,ky_spectrm,sea,slopx,slopy);
    time = time + 1.0;
  }
  free( spectrm );
  free( kx_spectrm );
  free( ky_spectrm );

/* write 3 output files */
  fd = creat("sea_elevat.dat",0644);
  for( i = 0; i < N; i++ )
    write(fd,&sea[i][0],4*N);
  close( fd );

  fd = creat("sea_slopx.dat",0644);
  for( i = 0; i < N; i++ )
    write(fd,&slopx[i][0],4*N);
  close( fd );

  fd = creat("sea_slopy.dat",0644);
  for( i = 0; i < N; i++ )
    write(fd,&slopy[i][0],4*N);
  close( fd );

}

int cstate(seed,kmax,time,spectrm_0,kx_spectrm_0,ky_spectrm_0,sea,slopx,slopy)
int seed;
float kmax, time;
COMPLEX *spectrm_0, *kx_spectrm_0, *ky_spectrm_0;
float sea[][N], slopx[][N], slopy[][N];
{
  float L;
  COMPLEX *spectrm_0ptr, *spectrm_t, *spectrm_tptr;
/*  int Cexp(), Imagin(), CRmul(); */
  static float kx[512], ky[512], array[512][512];
  float kx_array[512][512], ky_array[512][512];
  float k, pi=3.1415926, kmin;
  int i,j, m, *Ndim, *ivector();
  float *vec_NumC, *vector();

  vec_NumC = vector(1,N*N);	/* the silly Numerical Recipes C */
  Ndim = ivector(1,2);
  Ndim[1] = N;
  Ndim[2] = N;
/* time = 0.0 for initialization of spectra  */
  if( time == 0.0 )
  {
    kmin = kmax / (N/2);
    L = 1.0 / kmin; /*	! our fourier spatial domain length */
/*	define wave number range & arrays: down wind= +x, 
	cross wind= +-y, azim measured from downwind axis
	also define random phase matrix a la dick priest
	first define the first row & first column */	
    kx[0] = 0.0;
    ky[0] = 0.0;
    kx[N/2] = kmax;
    ky[N/2] = kmax;

    for( j = 1; j < N/2; j++ )
    {
 	kx[j] = (j - 1.0)/(N/2.0 - 1.0) * kmax;
	ky[j] = (j - 1.0)/(N/2.0 - 1.0) * kmax;
	kx[N-j+2] = -kx[j];
	ky[N-j+2] = -ky[j];
    }
/*	create random phase matrices */
    for(j = 1; j < N/2; j++ )
    {
	array[0][j] = rndphs();
	array[j][0] = rndphs();
	array[0][N-j+2] = -array[0][j];
	array[N-j+2][0] = -array[j][0];
    }

    for( i = 1; i < N/2; i++ )
      for( j = 1; j < N/2; j++ )
      {
        array[i][j] = rndphs();
        array[i][N-j+2] = rndphs();
      }

    for( i = N/2 + 1; i < N; i++ )
      for( j = 1; j < N/2; j++ )
      {
	array[i][j] = -array[N-i+2][N-j+2];
	array[i][N - j + 2] = -array[N-i+2][j];
      }

    Imagin(N*N,array,spectrm_0);
    Cexp(N*N,spectrm_0);

    for( i = 0; i < N; i++ )
      for( j = 0; j < N; j++ )
      {
/*	spectrm_0[i][j] = exp( Imagin(0.0,array[i][j]) ); */
	array[i][j] = 0.0;
      }

/* Pierson - Neumann spectrum from Borrego & Machado */
    PN_spctrm(windsp,N,kx,ky,array);

/*	must normalize spectrum with a division by k ( due to using
	cartesian coordinate instead opf polar
	also must use appropriate normalization of fourier coefficients
	(see Reif's stat. mech. pg 587: for 1d case fourier coeffs. are
	equal to sqrt(L/2pi) * power density spectrum, for 2d case use
	L/2pi, where L = 1/kmin in cm) */

    for( i = 0; i < N; i++ )
      for( j = 0; j < N; j++ )
      {
	k = sqrt( kx[i]*kx[i] + ky[j]*ky[j] );
	if(k == 0)
	{ 
	  array[i][j] = 0.0;
        }
	else
	{
/*	    array[i][j] = L/(2*pi) * sqrt(array[i][j] / k); */
	  array[i][j] =  sqrt(array[i][j] / k);
	}
      }
  } /* end if time == 0 init. */
/*
assume the surface is simply the real part of the ifft
for slope spectra simply multiply by kx & ky
according to priest the slope is defined descretely by inverting
the kx's and ky's about their central elements
normalize a la john hornstein, mult by L/4pi where L=1/kmax
*/
  L = 1.0;
/* calculate elevation spectra */
  CRmul(N*N,spectrm_0,array);
  if( time == 0.0 )
  {
    m = 1;
    for( i = 0; i < N; i++ )
      for( j = 0; j < N; j++ )
      {
        vec_NumC[m++] = spectrm_0->r;
        vec_NumC[1 + m] = spectrm_0->i;
        spectrm_0++;
      }
    fourn(vec_NumC,Ndim,2,-1);
    m = 1;
    for( i = 0; i < N; i++ )
      for( j = 0; j < N; j++ )
      {
	sea[i][j] = L * vec_NumC[m];
        m = m + 2;
      }
  }
  else
  {
    spectrm_t = wave_motion(N,time,spectrm_0);
    spectrm_tptr = spectrm_t;
    m = 1;
    for( i = 0; i < N; i++ )
      for( j = 0; j < N; j++ )
      {
        vec_NumC[m++] = spectrm_t->r;
        vec_NumC[1 + m] = spectrm_t->i;
        spectrm_t++;
      }
    fourn(vec_NumC,Ndim,2,-1);
    m = 1;
    for( i = 0; i < N; i++ )
      for( j = 0; j < N; j++ )
      {
	sea[i][j] = L * vec_NumC[m];;
        m = m + 2;
      }
    free( spectrm_tptr );
  }

/* calculate slope-x spectra */
  spectrm_0 = spectrm_0ptr;
  for( i = 0; i < N; i++ )
    for( j = 0; j < N; j++ )
	kx_array[i][j] = kx[i] * array[i][j];

  CRmul(N*N,kx_spectrm_0,kx_array);
  if( time == 0.0 )
  {
    m = 1;
    for( i = 0; i < N; i++ )
      for( j = 0; j < N; j++ )
      {
        vec_NumC[m++] = kx_spectrm_0->r;
        vec_NumC[1 + m] = kx_spectrm_0->i;
        kx_spectrm_0++;
      }
    fourn(vec_NumC,Ndim,2,-1);
    m = 1;
    for( i = 0; i < N; i++ )
      for( j = 0; j < N; j++ )
      {
	slopx[i][j] = L * vec_NumC[m];
        m = m + 2;
      }
  }
  else
  {
    spectrm_t = wave_motion(N,time,kx_spectrm_0);
    spectrm_tptr = spectrm_t;
    m = 1;
    for( i = 0; i < N; i++ )
      for( j = 0; j < N; j++ )
      {
        vec_NumC[m++] = spectrm_t->r;
        vec_NumC[1 + m] = spectrm_t->i;
        spectrm_t++;
      }
    fourn(vec_NumC,Ndim,2,-1);
    m = 1;
    for( i = 0; i < N; i++ )
      for( j = 0; j < N; j++ )
      {
	slopx[i][j] = L * vec_NumC[m];
        m = m + 2;
      }
    free( spectrm_tptr );
  }

/* calculate slope-y spectra */
  spectrm_0 = spectrm_0ptr;

  for( i = 0; i < N; i++ )
    for( j = 0; j < N; j++ )
	ky_array[i][j] = ky[j] * array[i][j];

  CRmul(N*N,ky_spectrm_0,ky_array);
  if( time == 0.0 )
  {
    m = 1;
    for( i = 0; i < N; i++ )
      for( j = 0; j < N; j++ )
      {
        vec_NumC[m++] = ky_spectrm_0->r;
        vec_NumC[1 + m] = ky_spectrm_0->i;
        ky_spectrm_0++;
      }
    fourn(vec_NumC,Ndim,2,-1);
    m = 1;
    for( i = 0; i < N; i++ )
      for( j = 0; j < N; j++ )
      {
	slopy[i][j] = L * vec_NumC[m];
        m = m + 2;
      }
  }
  else
  {
    spectrm_t = wave_motion(N,time,ky_spectrm_0);
    spectrm_tptr = spectrm_t;
    m = 1;
    for( i = 0; i < N; i++ )
      for( j = 0; j < N; j++ )
      {
        vec_NumC[m++] = spectrm_t->r;
        vec_NumC[1 + m] = spectrm_t->i;
        spectrm_t++;
      }
    fourn(vec_NumC,Ndim,2,-1);
    m = 1;
    for( i = 0; i < N; i++ )
      for( j = 0; j < N; j++ )
      {
	slopy[i][j] = L * vec_NumC[m];
        m = m + 2;
      }
    free( spectrm_tptr );
  }

  free( vec_NumC );
  return;
}

int Imagin(n,a,c)
int n;
float a[];
COMPLEX c[];
{
  while( --n >= 0 )
  {
    c[n].r = 0.0;
    c[n].i = a[n];
  }
}

int Cexp(n,c)
int n;
COMPLEX c[];
{
  float tmp;

  while( --n >= 0 )
  {
    tmp = exp(c[n].r);
    c[n].r = tmp * cos(c[n].i);
    c[n].i = tmp * sin(c[n].i);
  }
}

int CRmul(n,c,a)
int n;
COMPLEX c[];
float a[];
{
  while( --n >= 0 )
  {
    c[n].r = a[n] * c[n].r;
    c[n].i = a[n] * c[n].i;
  }
}

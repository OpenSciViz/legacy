#include "wind.h"
float Ufrct(windsp,elevat)
float windsp, elevat;
{
  float ret_val;
  int i;

  ret_val = -1;

  if( elevat == 0.0)
  {
    for( i = 0; i < NSP; i++ )
      if( Ufrict[i] == windsp )
	   ret_val = Ufrict[i];
  }
  else if( elevat == 10.0)
  {
    for( i = 0; i < NSP; i++ )
      if( Uat10[i] == windsp )
	 ret_val = Ufrict[i];
  }
  else if( elevat == 19.5)
  {
    for( i = 0; i < NSP; i++ )
      if( Uat195[i] == windsp )
	 ret_val = Ufrict[i];
  }
  return(ret_val);
}

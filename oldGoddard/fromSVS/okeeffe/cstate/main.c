
#include <math.h>  
#define t_MAX 0.0
#define NMAX 512

typedef struct complex { float r; float i; } COMPLEX;
float windsp=9.97;

main(argc,argv)
int argc;
char **argv;
{
  COMPLEX *spectrm, *kx_spectrm, *ky_spectrm;
  float kmax, time=0.0, del_time=0.001;
  float sea[NMAX][NMAX], slopx[NMAX][NMAX], slopy[NMAX][NMAX];
  long seed=12345678;
  int i, j, fd;
  float normalization=1.0, mean=0.0, var=0.0, total_power=0.0, cstate();
  float min = 99999999.9, max = -99999999.9;
  int N=512;
  char *outfile = "seadat.t0";
 
  srand48(seed);

  kmax = 1.2; /* rad/cm */

/*  if( argc > 1 ) normalization = atof(argv[1]); */
  if( argc > 1 ) N = atoi(argv[1]);
  if( argc > 2 )
  {
    time = 0.001 * atoi(argv[2]); /* assume input is in 1/1000 sec. */
    strcat(outfile,argv[2]);
  }
  if( argc > 3 ) normalization = atoi(argv[3]);
  if( argc > 4 ) kmax = atof(argv[4]);
  if( argc > 5 ) windsp = atof(argv[5]);/* m/sec) */

  spectrm = (COMPLEX *) malloc(N*N*sizeof(COMPLEX));
  kx_spectrm = (COMPLEX *) malloc(N*N*sizeof(COMPLEX));
  ky_spectrm = (COMPLEX *) malloc(N*N*sizeof(COMPLEX));

  total_power = cstate(N,seed,kmax,time,spectrm,kx_spectrm,ky_spectrm,
      				sea,slopx,slopy);
  free( spectrm );
  free( kx_spectrm );
  free( ky_spectrm );

/* normalize according to total variance ~ total power */
  for( i = 0; i < N; i++ )
  {
    for( j = 0; j < N; j++ )
    {
      mean = mean + sea[i][j];
      if( sea[i][j] > max ) max = sea[i][j];
      if( sea[i][j] < min ) min = sea[i][j];
    }
  }
  mean = mean / N / N;
  for( i = 0; i < N; i++ )
    for( j = 0; j < N; j++ )
      var = var + (sea[i][j] - mean)*(sea[i][j] - mean);

  var = var / N / N;


  normalization = normalization * sqrt( total_power / var );
  printf("cstate> pre-nomalization: min= %f\t max= %f\t mean= %f\t rms= %f\n",
		min,max,mean,sqrt(var));

  for( i = 0; i < N; i++ )
    for( j = 0; j < N; j++ )
    {
      sea[i][j] = normalization * sea[i][j];
      slopx[i][j] = normalization * slopx[i][j];
      slopy[i][j] = normalization * slopy[i][j];
    }

  min = normalization * min;  max = normalization * max;
  mean = normalization * mean;
  var = normalization * normalization * var;
  var = sqrt( var );
  printf("cstate> nomalization: %f\t min= %f\t max= %f\t mean= %f\t rms= %f\n",
		normalization,min,max,mean,sqrt(var));


/*  printf("cstate> elevation min= %f max= %f mean= %f rms= %f\n",min,max,mean,var); */

  mean = 0.0; var = 0.0;
  for( i = 0; i < N; i++ )
  {
    for( j = 0; j < N; j++ )
    {
      mean = mean + slopx[i][j];
      if( slopx[i][j] > max ) max = slopx[i][j];
      if( slopx[i][j] < min ) min = slopx[i][j];
    }
  }
  mean = mean / N / N;
  for( i = 0; i < N; i++ )
    for( j = 0; j < N; j++ )
      var = var + (slopx[i][j] - mean)*(slopx[i][j] - mean);

  var = sqrt(var / N / N);
  printf("cstate> slopx: min= %f max= %f mean= %f rms= %f\n",min,max,mean,var);

  mean = 0.0; var = 0.0;
  for( i = 0; i < N; i++ )
  {
    for( j = 0; j < N; j++ )
    {
      mean = mean + slopx[i][j];
      if( slopy[i][j] > max ) max = slopy[i][j];
      if( slopy[i][j] < min ) min = slopy[i][j];
    }
  }
  mean = mean / N / N;
  for( i = 0; i < N; i++ )
    for( j = 0; j < N; j++ )
      var = var + (slopy[i][j] - mean)*(slopy[i][j] - mean);

  var = sqrt(var / N / N);
  printf("cstate> slopy: min= %f max= %f mean= %f rms= %f\n",min,max,mean,var);

/* write output file */
  fd = creat(outfile,0644);
  for( i = 0; i < N; i++ )
    write(fd,&sea[i][0],4*N);
  for( i = 0; i < N; i++ )
    write(fd,&slopx[i][0],4*N);
  for( i = 0; i < N; i++ )
    write(fd,&slopy[i][0],4*N);

  close( fd );
}

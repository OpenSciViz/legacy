#include <math.h>

main(argc,argv)
int argc;
char **argv;
{
  int i, j, fd, nb;
  float sea[512][512], slopx[512][512], slopy[512][512];
  float normalization, mean=0.0, var=0.0;
  float min = 99999999.9, max = -99999999.9;
  fd = open("sea_9.97",0);


/* first record is header */
  nb = read(fd,&sea[0][0],4*512);
  for( i = 0; i < 512; i++ )
    read(fd,sea[i],4*512);

  for( i = 0; i < 512; i++ )
    for( j = 0; j < 512; j++ )
    {
      mean = mean + sea[i][j];
      if( sea[i][j] > max ) max = sea[i][j];
      if( sea[i][j] < min ) min = sea[i][j];
    }

  mean = mean / 512 / 512;
  for( i = 0; i < 512; i++ )
    for( j = 0; j < 512; j++ )
      var = var + (sea[i][j] - mean)*(sea[i][j] - mean);

  var = var / 512 / 512;

  printf("min= %f\t max= %f\t mean= %f\t rms= %f\n",
		min,max,mean,sqrt(var));
}

#include <stdio.h>
#include <math.h>  
#define t_MAX 0.0
#define NMAX 512

typedef struct complex { float r; float i; } COMPLEX;
void spctrm();
extern float windsp;
float elevat=19.5;

static float kx[NMAX], ky[NMAX], array[NMAX][NMAX]; /* compiler inits to 0 */

float cstate(N,seed,kmax,time,spectrm_0,kx_spectrm_0,ky_spectrm_0,
		sea,slopx,slopy)
int N;
int seed;
float kmax, time;
COMPLEX *spectrm_0, *kx_spectrm_0, *ky_spectrm_0;
float sea[][NMAX], slopx[][NMAX], slopy[][NMAX];
{
  float L;
  COMPLEX *spectrm_0ptr, *spectrm_t0, *spectrm_t;
  int wave_motion();
  float k, pi=3.1415926, kmin;
  int i,j, m;
  int Ndim[2];
  float total_power=0.0;
  float rndphs();

  Ndim[0] = N;
  Ndim[1] = N;

  kmin = kmax / (N/2);
  L = 1.0 / kmin; /*	! our fourier spatial domain length */
/*	define wave number range & arrays: down wind= +x, 
	cross wind= +-y, azim measured from downwind axis
	also define random phase matrix a la dick priest
	first define the first row & first column */	
/* static elements should be init. to 0 by compiler 
  kx[0] = 0.0;
  ky[0] = 0.0;
  kx[1] = 0.0;
  ky[1] = 0.0;
  kx[N/2] = 0.0;
  ky[N/2] = 0.0;
  kx[N-1] = 0.0;
  ky[N-1] = 0.0;
*/
  for( j = 1; j < N/2; j++ )
  {
    kx[j] = 1.0*j/(N/2.0 - 1.0) * kmax;
    ky[j] = 1.0*j/(N/2.0 - 1.0) * kmax;
    kx[N-j] = -kx[j];
    ky[N-j] = -ky[j];
  }
/*	create random phase matrices */
  for(j = 1; j < N/2; j++ )
  {
    array[0][j] = rndphs();
    array[j][0] = rndphs();
    array[0][N-j] = -array[0][j];
    array[N-j][0] = -array[j][0];
  }

  for( i = 1; i < N/2; i++ )
  {
    for( j = 1; j < N/2; j++ )
    {
      array[i][j] = rndphs();
/*      array[i][N-j] = -array[i][j]; */
      array[i][N-j] = rndphs();
    }
  }

  for( i = N/2 + 1; i < N; i++ )
  {
    for( j = 1; j < N/2; j++ )
    {
      array[i][j] = -array[N-i][N-j];
      array[i][N-j] = -array[N-i][j];
    }
  }
    
  Imagin(N,array,spectrm_0);
  Cexp(N*N,spectrm_0);

/* Pierson - Neumann spectrum from Borrego & Machado 
  PN_spctrm(windsp,N,kx,ky,array); */
  spctrm(windsp,elevat,N,kx,ky,array); /*   Pierson - Stacy */

/*	must normalize spectrum with a division by k ( due to using
cartesian coordinate instead opf polar
also must use appropriate normalization of fourier coefficients
(see Reif's stat. mech. pg 587: for 1d case fourier coeffs. are
equal to sqrt(L/2pi) * power density spectrum, for 2d case use
L/2pi, where L = 1/kmin in cm) */

  for( i = 0; i < N; i++ )
  {
    for( j = 0; j < N; j++ )
    {
      k = sqrt( kx[i]*kx[i] + ky[j]*ky[j] );
      if(k == 0)
	array[i][j] = 0.0;
      else
      {
/*	    array[i][j] = L/(2*pi) * sqrt(array[i][j] / k); */
	array[i][j] =  sqrt(array[i][j] / k);
      }
    }
  }
/*
assume the surface is simply the real part of the ifft
for slope spectra simply multiply by kx & ky
according to priest the slope is defined descretely by inverting
the kx's and ky's about their central elements
normalize a la john hornstein, mult by L/4pi where L=1/kmax
*/
/* calculate elevation spectra */
  CRmul(N,spectrm_0,array);
/* integrate the spectrum for future reference */
  spectrm_t = (COMPLEX *) malloc(N*N*sizeof(COMPLEX));
  spectrm_0ptr = spectrm_t;
  spectrm_t0 = spectrm_0;
  for( i = 0; i < N*N; i++)
  {
/* since fft replaces original array, use this secondary array */
    spectrm_t->r = spectrm_t0->r;
    spectrm_t->i = spectrm_t0->i;
    total_power = total_power + spectrm_t->r * spectrm_t->r + 
      			spectrm_t->i * spectrm_t->i;
    spectrm_t++;
    spectrm_t0++;
  }
  spectrm_t = spectrm_0ptr;
  total_power = (kmin/2.0) * (kmin/2.0) * total_power;

  if( time == 0.0 )
  {
    fourn((&spectrm_t->r - 1),(Ndim - 1),2,1); 
/* note that Num-Recipe's definition of forward -- inverse FFT is the opposite of everyone elses */
    Real(N,spectrm_t,sea);
    free( spectrm_t );
  }
  else
  {
    wave_motion(N,time,kx,ky,spectrm_0,spectrm_t);
    fourn((&spectrm_t->r - 1),(Ndim - 1),2,1);
    Real(N,spectrm_t,sea);
    free( spectrm_t );
  }

  ICmulX(N,kx,spectrm_0,kx_spectrm_0);
  if( time == 0.0 )
  {
    fourn((&kx_spectrm_0->r - 1),(Ndim - 1),2,1);
    Real(N,kx_spectrm_0,slopx);
  }
  else
  {
    wave_motion(N,time,kx,ky,kx_spectrm_0,spectrm_t);
    fourn((&spectrm_t->r - 1),Ndim - 1,2,-1);
    Real(N,spectrm_t,slopx);
    free( spectrm_t );
  }

  ICmulY(N,ky,spectrm_0,ky_spectrm_0);
  if( time == 0.0 )
  {
    fourn((&ky_spectrm_0->r - 1),(Ndim - 1),2,1);
    Real(N,ky_spectrm_0,slopy);
  }
  else
  {
    wave_motion(N,time,kx,ky,ky_spectrm_0,spectrm_t);
    fourn((&spectrm_t->r - 1),Ndim - 1,2,1);
    Real(N,spectrm_t,slopy);
    free( spectrm_t );
  }

  return(total_power);
}

int Imagin(n,a,c)
int n;
float a[][NMAX];
COMPLEX c[];
{
  int i, j, N;

  N = n;
  n = n*n;
  for( i = N-1; i >= 0; i-- )
  {
    for( j = N-1; j >=0 ; j-- )
    {
      --n;
      c[n].r = 0.0;
      c[n].i = a[i][j];
    }
  }
}

int Cexp(n,c)
int n;
COMPLEX c[];
{
  float tmp;

  while( --n >= 0 )
  {
    if( c[n].r == 0.0 && c[n].i == 0.0 )
    {
/* do nothing, this is the either a central element or an edge element
   that is "zero padded" for the fft */
    }
    else
    {  
      tmp = exp(c[n].r);
      c[n].r = tmp * cos(c[n].i);
      c[n].i = tmp * sin(c[n].i);
    }
  }
}

int CRmul(n,c,a)
int n;
COMPLEX c[];
float a[][NMAX];
{
  int i, j, N;
  COMPLEX mean;

  N = n;
  mean.r = 0.0;
  mean.i = 0.0;

  n = n*n;
  for( i = N-1; i >= 0; i-- )
  {
    for( j = N-1; j >=0 ; j-- )
    {
      --n;
      c[n].r = a[i][j] * c[n].r;
      c[n].i = a[i][j] * c[n].i;
    }
  }
/* and take the opportunity to remove any DC component 
  for( i = 0; i < N*N; i++ )
  {
    mean.r = mean.r + c[i].r;
    mean.i = mean.i + c[i].i;
  }
  mean.r = mean.r / (N-1);
  mean.i = mean.i / (N-1);
  for( i = 0; i < N; i++ )
  {
    c[i].r = c[i].r - mean.r;
    c[i].i = c[i].i - mean.i;
  }
*/

  return;
}

int Real(n,c,a)
int n;
COMPLEX c[];
float a[][NMAX];
{
  int i, j, N;

  N = n;
  n = n*n;
  for( i = N-1; i >= 0; i-- )
    for( j = N-1; j >=0 ; j-- )
      a[i][j] = c[--n].r; /* / N / N; */
}

int ICmulX(n,r,c1,c2)
int n;
float r[];
COMPLEX c1[], c2[];
{
  int i, j;

  for( i = 0; i < n; i++ )
  {
    for( j = 0; j < n; j++ )
    {
      c2[i*n + j].r = -r[i]*c1[i*n + j].i;
      c2[i*n + j].i = r[i]*c1[i*n + j].r;
    }
  }
  return;
}

int ICmulY(n,r,c1,c2)
int n;
float r[];
COMPLEX c1[], c2[];
{
  int i, j;

  for( i = 0; i < n; i++ )
  {
    for( j = 0; j < n; j++ )
    {
      c2[i*n + j].r = -r[j]*c1[i*n + j].i;
      c2[i*n + j].i = r[j]*c1[i*n + j].r;
    }
  }
  return;
}

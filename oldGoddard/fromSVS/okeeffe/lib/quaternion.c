/* These formulas are taken from "Spacecraft Attitude Determination
   and Control" pg 414-415 */

#include <math.h>

float *quaternion(float *mat)
{
  static float q[4], sign = 1.0; /* sign could be +/- */

  q[3] = sign * 0.5 * sqrt(1.0 + mat[0] + mat[4] + mat[8]); 
  q[0] = 0.25/q[3] * (mat[5] - mat[7]);
  q[1] = 0.25/q[3] * (mat[6] - mat[2]);
  q[2] = 0.25/q[3] * (mat[1] - mat[3]);

  return( &q[0] );
}

float *q_rotation(float *q)
{
  static float mat[3*3];

  mat[0] = q[0]*q[0] - q[1]*q[1] - q[2]*q[2] + q[3]*q[3];
  mat[1] = 2.0*(q[0]*q[1] + q[2]*q[3]);
  mat[2] = 2.0*(q[0]*q[2] - q[1]*q[3]);

  mat[3] = 2.0*(q[0]*q[1] - q[2]*q[3]); 
  mat[4] = -q[0]*q[0] + q[1]*q[1] - q[2]*q[2] + q[3]*q[3];
  mat[5] = 2.0*(q[1]*q[2] + q[0]*q[3]);

  mat[6] = 2.0*(q[0]*q[2] + q[1]*q[3]);
  mat[7] = 2.0*(q[1]*q[2] - q[0]*q[3]);
  mat[8] = -q[0]*q[0] - q[1]*q[1] + q[2]*q[2] + q[3]*q[3];

  return( &mat[0] );
}

float *rotate_vec(float *mat, float *vec)
{
  static float res[3];

  res[0] = mat[0]*vec[0] + mat[1]*vec[1] + mat[2]*vec[2];
  res[1] = mat[3]*vec[0] + mat[4]*vec[1] + mat[5]*vec[2];
  res[2] = mat[6]*vec[0] + mat[7]*vec[1] + mat[8]*vec[2];

  return( &res[0] );
}

quat_mult(float *q1, float *q2)
{
 static float qt[4];

  qt[0] = q2[0];
  qt[1] = q2[1];
  qt[2] = q2[2];
  qt[3] = q2[3];

  q2[3] = -q1[0]*qt[0] - q1[1]*qt[1] - q1[2]*qt[2] + q1[3]*qt[3];
  q2[0] =  q1[0]*qt[3] + q1[1]*qt[2] - q1[2]*qt[1] + q1[3]*qt[0];
  q2[1] = -q1[0]*qt[2] + q1[1]*qt[3] + q1[2]*qt[0] + q1[3]*qt[1];
  q2[2] =  q1[0]*qt[1] - q1[1]*qt[0] + q1[2]*qt[3] + q1[3]*qt[2];
  
}
/*
main(int argc,char **argv,char **envp)
{
  float ax1[3], ax2[3], ax3[3];
  float q1[4], q2[4], q3[4];
  float curve[4*100];

  ax1[0] = cos(-50.2/57.3)*cos(112.9/57.3);
  ax1[1] = cos(-50.2/57.3)*sin(112.9/57.3);
  ax1[2] = sin(-50.2/57.3);

  ax2[0] = cos(-16.85/57.3)*cos(136.8/57.3);
  ax2[1] = cos(-16.85/57.3)*sin(136.8/57.3);
  ax2[2] = sin(-16.85/57.3);

  ax1[0] = cos(0.0/57.3)*cos(112.9/57.3);
  ax1[1] = cos(0.0/57.3)*sin(112.9/57.3);
  ax1[2] = sin(0.0/57.3);

  q1[0] = ax1[0]*sin(7.70/57.3/2.0);
  q1[1] = ax1[1]*sin(7.70/57.3/2.0);
  q1[2] = ax1[2]*sin(7.70/57.3/2.0);
  q1[4] = cos(7.70/57.3/2.0);

  q2[0] = ax2[0]*sin(22.82/57.3/2.0);
  q2[1] = ax2[1]*sin(22.82/57.3/2.0);
  q2[2] = ax2[2]*sin(22.82/57.3/2.0);
  q2[4] = cos(22.82/57.3/2.0);

  q3[0] = ax3[0]*sin(35.31/57.3/2.0);
  q3[1] = ax3[1]*sin(35.31/57.3/2.0);
  q3[2] = ax3[2]*sin(35.31/57.3/2.0);
  q3[4] = cos(35.31/57.3/2.0);

  q_spline(q1,q2,q3,q4,curve); 

}
*/








	Subroutine upx_4_NEIGHBORS(pixel,res,neighbors)
C
C    Generalized routine to return the four orthogonal neighbors to the
C  input pixel at the given resolution.
C
C  This routine will work for resolutions up to 15.
C
C==============================================================================
C
C  Method:
C
C   1. Divide the pixel number up into x and y bits.  These are the cartesian
C      coordinates of the pixel (where 1 = 1 pixel) on the face.
C
C   2. Convert the (x,y) found from the original pixel number in the input
C      resolution to the equivalent positions on a face of resolution 15.
C      This is accomplished by multiplying by the variable DISTANCE, which
C      is the width of a RES pixel in resolution=15 pixels.  Resolution=15
C      is the 'intermediary' format for finding neighbors.
C
C   3. Determine the cartesian coordinates (resolution=15) of the neighbors
C      by adding the appropriate orthogonal offset (DISTANCE) to the center
C      pixel (x,y).  EDGCHK is called to adjust the new coordinates and face 
C      number in case PIXEL lies along an edge.
C
C   4. Convert the new coordinates to a resolution 15 pixel number.
C
C   5. Convert the resolution=15 pixel number to a resolution=RES pixel number
C      by dividing by the appropriate power of four (DIVISOR).
C
C   -- Steps 3-5 are repeated once for each of the four neighbors.
C
C==============================================================================
C
C  06/07/88, acr.
C  09/28/88, acr, sundry silly errors corrected.
C
C  Arguments:
C
      Integer*4        PIXEL        ! Pixel number
      Integer*2        RES          ! Resolution
      Integer*4        NEIGHBORS(4) ! Neighboring pixel numbers
C
C  Variables:
C
      Integer*4        FACE                    ! Face number of PIXEL
      Integer*4        NFACE                   ! Face number of neighbor
      Integer*4        PIXELS_PER_FACE         ! Pixels on a face
      Integer*4        RPIXEL                  ! Relative pixel number on face
      Integer*4        TWO                     ! 2
      Integer*4        FOUR                    ! 4 
      Integer*4        TWO14                   ! 2**14
      Integer*4        TWO28                   ! 2**28
      Integer*4        DIVISOR                 ! Converts resolution 15
                                               !  pixel number to input
                                               !  resolution
      Integer*4        RES_DIFF                ! Difference between 15 and
                                               !  input resolution
      Integer*4        DISTANCE                ! Distance between pixel and
                                               !  neighbor at resolution 15
      Integer*4        IX,IY                   ! Pixel cart. coordinates
                                               !  on cube face
      Integer*4        JXHI,JXLO,JYHI,JYLO    
      Integer*4        IP,ID                   ! Var. for breaking down PIXEL
      Integer*4        JX,JY                   ! Neighbor cart. coordinates
                                               !  on cube face
      Integer*4        IXTAB(128),IYTAB(128)   ! Bit tables for converting
                                               !  pixel number on face to
                                               !  cartesian coordinates

      Data IXTAB(128) /0/

      Parameter (two = 2)
      Parameter (four = 4)
      Parameter (two14 = 2**14)
      Parameter (two28 = 2**28)

      if (ixtab(128) .eq. 0) call upx_BIT_TABLE_SET(ixtab,iytab,128)

      pixels_per_face = (two**(res - 1)) ** 2
      face     = pixel / pixels_per_face
      rpixel   = pixel - face*pixels_per_face
      res_diff = 15-res
      divisor  = four**(res_diff)
      distance = two**(res_diff)

      ix = 0
      iy = 0
      ip = 1
C
C  Break pixel number down into constituent x,y coordinates:
C 
      do while (rpixel .ne. 0)

          id = mod(rpixel,2)
          rpixel = rpixel / 2
          ix = id * ip + ix

          id = mod(rpixel,2)
          rpixel = rpixel / 2
          iy = id * ip + iy

          ip = 2 * ip

        end do
C
C  Convert x,y coordinates of pixel in initial resolution to resolution
C  of 15:
C
      ix = ix * distance 
      iy = iy * distance 
C
C  Calculate coordinates of each neighbor, check for edges, and return pixel
C  number in appropriate array element:
C
      Call edgchk(face,ix+distance,iy,two14,nface,jx,jy)   ! Right 
      jxhi = jx/128
      jxlo = mod(jx,128)
      jyhi = jy/128
      jylo = mod(jy,128)
      neighbors(1) = nface * two28 + 
     +               ixtab(jxlo+1) + iytab(jylo+1) +
     +               two14 * (ixtab(jxhi+1) + iytab(jyhi+1))
      Neighbors(1) = neighbors(1) / divisor

      Call edgchk(face,ix,iy+distance,two14,nface,jx,jy)   ! Top 
      jxhi = jx/128
      jxlo = mod(jx,128)
      jyhi = jy/128
      jylo = mod(jy,128)
      neighbors(2) = nface * two28 + 
     +               ixtab(jxlo+1) + iytab(jylo+1) +
     +               two14 * (ixtab(jxhi+1) + iytab(jyhi+1))
      neighbors(2) = neighbors(2) / divisor

      Call edgchk(face,ix-distance,iy,two14,nface,jx,jy)   ! Left 
      jxhi = jx/128
      jxlo = mod(jx,128)
      jyhi = jy/128
      jylo = mod(jy,128)
      neighbors(3) = nface * two28 + 
     +               ixtab(jxlo+1) + iytab(jylo+1) +
     +               two14 * (ixtab(jxhi+1) + iytab(jyhi+1))
      neighbors(3) = neighbors(3) / divisor

      Call edgchk(face,ix,iy-distance,two14,nface,jx,jy)   ! Bottom
      jxhi = jx/128
      jxlo = mod(jx,128)
      jyhi = jy/128
      jylo = mod(jy,128)
      neighbors(4) = nface * two28 + 
     +               ixtab(jxlo+1) + iytab(jylo+1) +
     +               two14 * (ixtab(jxhi+1) + iytab(jyhi+1))
      neighbors(4) = neighbors(4) / divisor

      Return
      end

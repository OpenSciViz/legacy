
****************************************************************************

	Subroutine upx_4_NEIGHBORS(pixel,res,neighbors)
C
C    Generalized routine to return the four orthogonal neighbors to the
C  input pixel at the given resolution.
C
C  This routine will work for resolutions up to 15.
C
C==============================================================================
C
C  Method:
C
C   1. Divide the pixel number up into x and y bits.  These are the cartesian
C      coordinates of the pixel (where 1 = 1 pixel) on the face.
C
C   2. Convert the (x,y) found from the original pixel number in the input
C      resolution to the equivalent positions on a face of resolution 15.
C      This is accomplished by multiplying by the variable DISTANCE, which
C      is the width of a RES pixel in resolution=15 pixels.  Resolution=15
C      is the 'intermediary' format for finding neighbors.
C
C   3. Determine the cartesian coordinates (resolution=15) of the neighbors
C      by adding the appropriate orthogonal offset (DISTANCE) to the center
C      pixel (x,y).  EDGCHK is called to adjust the new coordinates and face 
C      number in case PIXEL lies along an edge.
C
C   4. Convert the new coordinates to a resolution 15 pixel number.
C
C   5. Convert the resolution=15 pixel number to a resolution=RES pixel number
C      by dividing by the appropriate power of four (DIVISOR).
C
C   -- Steps 3-5 are repeated once for each of the four neighbors.
C
C==============================================================================
C
C  06/07/88, acr.
C  09/28/88, acr, sundry silly errors corrected.
C
C  Arguments:
C
      Integer*4        PIXEL        ! Pixel number
      Integer*2        RES          ! Resolution
      Integer*4        NEIGHBORS(4) ! Neighboring pixel numbers
C
C  Variables:
C
      Integer*4        FACE                    ! Face number of PIXEL
      Integer*4        NFACE                   ! Face number of neighbor
      Integer*4        PIXELS_PER_FACE         ! Pixels on a face
      Integer*4        RPIXEL                  ! Relative pixel number on face
      Integer*4        TWO                     ! 2
      Integer*4        FOUR                    ! 4 
      Integer*4        TWO14                   ! 2**14
      Integer*4        TWO28                   ! 2**28
      Integer*4        DIVISOR                 ! Converts resolution 15
                                               !  pixel number to input
                                               !  resolution
      Integer*4        RES_DIFF                ! Difference between 15 and
                                               !  input resolution
      Integer*4        DISTANCE                ! Distance between pixel and
                                               !  neighbor at resolution 15
      Integer*4        IX,IY                   ! Pixel cart. coordinates
                                               !  on cube face
      Integer*4        JXHI,JXLO,JYHI,JYLO    
      Integer*4        IP,ID                   ! Var. for breaking down PIXEL
      Integer*4        JX,JY                   ! Neighbor cart. coordinates
                                               !  on cube face
      Integer*4        IXTAB(128),IYTAB(128)   ! Bit tables for converting
                                               !  pixel number on face to
                                               !  cartesian coordinates

      Data IXTAB(128) /0/

      Parameter (two = 2)
      Parameter (four = 4)
      Parameter (two14 = 2**14)
      Parameter (two28 = 2**28)

      if (ixtab(128) .eq. 0) call upx_BIT_TABLE_SET(ixtab,iytab,128)

      pixels_per_face = (two**(res - 1)) ** 2
      face     = pixel / pixels_per_face
      rpixel   = pixel - face*pixels_per_face
      res_diff = 15-res
      divisor  = four**(res_diff)
      distance = two**(res_diff)

      ix = 0
      iy = 0
      ip = 1
C
C  Break pixel number down into constituent x,y coordinates:
C 
      do while (rpixel .ne. 0)

          id = mod(rpixel,2)
          rpixel = rpixel / 2
          ix = id * ip + ix

          id = mod(rpixel,2)
          rpixel = rpixel / 2
          iy = id * ip + iy

          ip = 2 * ip

        end do
C
C  Convert x,y coordinates of pixel in initial resolution to resolution
C  of 15:
C
      ix = ix * distance 
      iy = iy * distance 
C
C  Calculate coordinates of each neighbor, check for edges, and return pixel
C  number in appropriate array element:
C
      Call edgchk(face,ix+distance,iy,two14,nface,jx,jy)   ! Right 
      jxhi = jx/128
      jxlo = mod(jx,128)
      jyhi = jy/128
      jylo = mod(jy,128)
      neighbors(1) = nface * two28 + 
     +               ixtab(jxlo+1) + iytab(jylo+1) +
     +               two14 * (ixtab(jxhi+1) + iytab(jyhi+1))
      Neighbors(1) = neighbors(1) / divisor

      Call edgchk(face,ix,iy+distance,two14,nface,jx,jy)   ! Top 
      jxhi = jx/128
      jxlo = mod(jx,128)
      jyhi = jy/128
      jylo = mod(jy,128)
      neighbors(2) = nface * two28 + 
     +               ixtab(jxlo+1) + iytab(jylo+1) +
     +               two14 * (ixtab(jxhi+1) + iytab(jyhi+1))
      neighbors(2) = neighbors(2) / divisor

      Call edgchk(face,ix-distance,iy,two14,nface,jx,jy)   ! Left 
      jxhi = jx/128
      jxlo = mod(jx,128)
      jyhi = jy/128
      jylo = mod(jy,128)
      neighbors(3) = nface * two28 + 
     +               ixtab(jxlo+1) + iytab(jylo+1) +
     +               two14 * (ixtab(jxhi+1) + iytab(jyhi+1))
      neighbors(3) = neighbors(3) / divisor

      Call edgchk(face,ix,iy-distance,two14,nface,jx,jy)   ! Bottom
      jxhi = jx/128
      jxlo = mod(jx,128)
      jyhi = jy/128
      jylo = mod(jy,128)
      neighbors(4) = nface * two28 + 
     +               ixtab(jxlo+1) + iytab(jylo+1) +
     +               two14 * (ixtab(jxhi+1) + iytab(jyhi+1))
      neighbors(4) = neighbors(4) / divisor

      Return
      end
	Subroutine upx_8_NEIGHBORS(pixel,res,
     +                             neighbors,number_of_neighbors)
C
C    Generalized routine to return the numbers of all pixels adjoining the
C  input pixel at the given resolution.
C
C  This routine will work for resolutions up to 15.
C
C==============================================================================
C
C  Method:
C
C     1.  All eight adjoining pixels are found by manipulation of the
C         cartesian coordinates of the input pixel in the plane of the
C         face on which it lies.  See UPX_4_NEIGHBORS opening comments 
C         for a detailed description of this process.
C
C     2.  Once all eight neighbors are found, the NEIGHBORS array is sorted
C         and duplicate pixel numbers are deleted.
C
C     3.  The number of unique neighbors and the array of pixel numbers
C         are returned.
C
C==============================================================================
C
C  09/28/88, acr.
C
C  Arguments:
C
      Integer*4        PIXEL                ! Pixel number
      Integer*2        RES                  ! Resolution
      Integer*4        NEIGHBORS(8)         ! Neighboring pixel numbers
      Integer*4        NUMBER_OF_NEIGHBORS  ! Number of adjoining pixels
                                            !  (4-8)
C
C  Variables:
C
      Integer*4        FACE                    ! Face number of PIXEL
      Integer*4        NFACE                   ! Face number of neighbor
      Integer*4        PIXELS_PER_FACE         ! Pixels on a face
      Integer*4        RPIXEL                  ! Relative pixel number on face
      Integer*4        TWO                     ! 2
      Integer*4        FOUR                    ! 4 
      Integer*4        TWO14                   ! 2**14
      Integer*4        TWO28                   ! 2**28
      Integer*4        DIVISOR                 ! Converts resolution 15
                                               !  pixel number to input
                                               !  resolution
      Integer*4        RES_DIFF                ! Difference between 15 and
                                               !  input resolution
      Integer*4        DISTANCE                ! Distance between pixel and
                                               !  neighbor at resolution 15
      Integer*4        IX,IY                   ! Pixel cart. coordinates
                                               !  on cube face
      Integer*4        JXHI,JXLO,JYHI,JYLO    
      Integer*4        IP,ID                   ! Var. for breaking down PIXEL
      Integer*4        JX,JY                   ! Neighbor cart. coordinates
                                               !  on cube face
      Integer*4        IXTAB(128),IYTAB(128)   ! Bit tables for converting
                                               !  pixel number on face to
                                               !  cartesian coordinates
      Integer*4        TEMP_PIXEL              ! Pixel number holding place
      Integer*4        TEMP_NBR(8)             ! Neighbor array holding place
      Integer*4        I,J                     ! Sort loop variables

      Data IXTAB(128) /0/

      Parameter (two = 2)
      Parameter (four = 4)
      Parameter (two14 = 2**14)
      Parameter (two28 = 2**28)

      if (ixtab(128) .eq. 0) call upx_BIT_TABLE_SET(ixtab,iytab,128)

      pixels_per_face = (two**(res - 1)) ** 2
      face     = pixel / pixels_per_face
      rpixel   = pixel - face*pixels_per_face
      res_diff = 15-res
      divisor  = four**(res_diff)
      distance = two**(res_diff)

      ix = 0
      iy = 0
      ip = 1
C
C  Break pixel number down into constituent x,y coordinates:
C 
      do while (rpixel .ne. 0)

          id = mod(rpixel,2)
          rpixel = rpixel / 2
          ix = id * ip + ix

          id = mod(rpixel,2)
          rpixel = rpixel / 2
          iy = id * ip + iy

          ip = 2 * ip

        end do
C
C  Convert x,y coordinates of pixel in initial resolution to resolution
C  of 15:
C
      ix = ix * distance 
      iy = iy * distance 
C                   
C  Calculate coordinates of each neighbor, check for edges, and return pixel
C  number in appropriate array element:
C
      Call edgchk(face,ix+distance,iy,two14,nface,jx,jy)   ! Right 
      jxhi = jx/128
      jxlo = mod(jx,128)
      jyhi = jy/128
      jylo = mod(jy,128)
      temp_nbr(1) = nface * two28 + 
     +              ixtab(jxlo+1) + iytab(jylo+1) +
     +              two14 * (ixtab(jxhi+1) + iytab(jyhi+1))
      temp_nbr(1) = temp_nbr(1) / divisor

      Call edgchk(face,ix,iy+distance,two14,nface,jx,jy)   ! Top 
      jxhi = jx/128
      jxlo = mod(jx,128)
      jyhi = jy/128
      jylo = mod(jy,128)
      temp_nbr(2) = nface * two28 + 
     +              ixtab(jxlo+1) + iytab(jylo+1) +
     +              two14 * (ixtab(jxhi+1) + iytab(jyhi+1))
      temp_nbr(2) = temp_nbr(2) / divisor

      Call edgchk(face,ix-distance,iy,two14,nface,jx,jy)   ! Left 
      jxhi = jx/128
      jxlo = mod(jx,128)                 
      jyhi = jy/128
      jylo = mod(jy,128)
      temp_nbr(3) = nface * two28 + 
     +              ixtab(jxlo+1) + iytab(jylo+1) +
     +              two14 * (ixtab(jxhi+1) + iytab(jyhi+1))
      temp_nbr(3) = temp_nbr(3) / divisor

      Call edgchk(face,ix,iy-distance,two14,nface,jx,jy)   ! Bottom
      jxhi = jx/128
      jxlo = mod(jx,128)
      jyhi = jy/128
      jylo = mod(jy,128)
      temp_nbr(4) = nface * two28 + 
     +              ixtab(jxlo+1) + iytab(jylo+1) +
     +              two14 * (ixtab(jxhi+1) + iytab(jyhi+1))
      temp_nbr(4) = temp_nbr(4) / divisor


      Call edgchk(face,ix+distance,iy+distance,two14,nface,jx,jy)   ! Top-Right
      jxhi = jx/128
      jxlo = mod(jx,128)
      jyhi = jy/128
      jylo = mod(jy,128)
      temp_nbr(5) = nface * two28 + 
     +              ixtab(jxlo+1) + iytab(jylo+1) +
     +              two14 * (ixtab(jxhi+1) + iytab(jyhi+1))
      temp_nbr(5) = temp_nbr(5) / divisor

      Call edgchk(face,ix-distance,iy+distance,two14,nface,jx,jy)   ! Top-Left
      jxhi = jx/128
      jxlo = mod(jx,128)
      jyhi = jy/128
      jylo = mod(jy,128)
      temp_nbr(6) = nface * two28 + 
     +              ixtab(jxlo+1) + iytab(jylo+1) +
     +              two14 * (ixtab(jxhi+1) + iytab(jyhi+1))
      temp_nbr(6) = temp_nbr(6) / divisor

      Call edgchk(face,ix-distance,iy-distance,two14,nface,jx,jy)   ! Bottom-Left 
      jxhi = jx/128
      jxlo = mod(jx,128)
      jyhi = jy/128
      jylo = mod(jy,128)
      temp_nbr(7) = nface * two28 + 
     +              ixtab(jxlo+1) + iytab(jylo+1) +
     +              two14 * (ixtab(jxhi+1) + iytab(jyhi+1))
      temp_nbr(7) = temp_nbr(7) / divisor

      Call edgchk(face,ix+distance,iy-distance,two14,nface,jx,jy)   ! Bottom-Right
      jxhi = jx/128
      jxlo = mod(jx,128)
      jyhi = jy/128
      jylo = mod(jy,128)
      temp_nbr(8) = nface * two28 + 
     +              ixtab(jxlo+1) + iytab(jylo+1) +
     +              two14 * (ixtab(jxhi+1) + iytab(jyhi+1))
      temp_nbr(8) = temp_nbr(8) / divisor
C
C  Bubble sort of neighboring pixels:
C
      do i = 1, 7
          do j = i+1, 8
              if (temp_nbr(i) .gt. temp_nbr(j)) then
                  temp_pixel  = temp_nbr(i)
                  temp_nbr(i) = temp_nbr(j)
                  temp_nbr(j) = temp_pixel
                end if
            end do
        end do
C
C  Delete duplicate pixel numbers:
C
      j = 1
      neighbors(1) = temp_nbr(1)
      do i = 2, 8
          if (neighbors(j) .ne. temp_nbr(i)) then
              j = j + 1
              neighbors(j) = temp_nbr(i)
            end if
        end do

      number_of_neighbors = j

      Return
      end
	SUBROUTINE AXISXY(C,NFACE,X,Y)
C
C	CONVERTS UNIT VECTOR C INTO NFACE NUMBER (0-5) AND X,Y
C	IN RANGE 0-1
C
	REAL*4 C(3)
	AC3=ABS(C(3))
	AC2=ABS(C(2))
	AC1=ABS(C(1))
	IF(AC3.GT.AC2) THEN
	   IF(AC3.GT.AC1) THEN
	      IF(C(3).GT.0.) GO TO 100
		GO TO 150
	   ELSE
	      IF(C(1).GT.0.) GO TO 110
		GO TO 130
	   ENDIF
	ELSE
	   IF(AC2.GT.AC1) THEN
	      IF(C(2).GT.0.) GO TO 120
		GO TO 140
	   ELSE
	      IF(C(1).GT.0.) GO TO 110
		GO TO 130
	   ENDIF
	ENDIF
100	NFACE=0
	ETA=-C(1)/C(3)
	XI=C(2)/C(3)
	GO TO 200
110	NFACE=1
	XI=C(2)/C(1)
	ETA=C(3)/C(1)
	GO TO 200
120	NFACE=2
	ETA=C(3)/C(2)
	XI=-C(1)/C(2)
	GO TO 200
130	NFACE=3
	ETA=-C(3)/C(1)
	XI=C(2)/C(1)
	GO TO 200
140	NFACE=4
	ETA=-C(3)/C(2)
	XI=-C(1)/C(2)
	GO TO 200
150	NFACE=5
	ETA=-C(1)/C(3)
	XI=-C(2)/C(3)
	GO TO 200
C
200	CALL INCUBE(XI,ETA,X,Y)
	X=(X+1.)/2.
	Y=(Y+1.)/2.
	RETURN
	END
      Subroutine upx_BIT_TABLE_SET(IX,IY,LENGTH)
C
C    Routine to set up the bit tables for use in the pixelization subroutines 
C  (extracted and generalized from existing routines).
C
C  Arguments:
C
      Integer*2   LENGTH       ! Number of elements in IX, IY
      Integer*4   IX(length)   ! X bits
      Integer*4   IY(length)   ! Y bits
C
C  Variables:
C
      Integer*4   i,j,k,ip,id  ! Loop variables
C
      Do 30 I = 1,length
          j = i - 1
          k = 0
          ip = 1
   10     if (j .eq. 0) then
              ix(i) = k
              iy(i) = 2*k
            else
              id = mod(j,2)
              j = j/2
              k = ip * id + k
              ip = ip * 4
              go to 10
            endif
   30   continue
C
      return
      end
	SUBROUTINE DIRBE_CENPIX(NPIX,C)
C
C	ROUTINE TO CONVERT A PIXEL NUMBER INTO A UNIT VECTOR IN THE CENTER
C	OF THE PIXEL
C
C	INPUT: INTEGER*4 NPIX, 1 -> 6*256**2
C	OUTPUT: REAL*4 C(3), UNIT VECTOR
C
C	TIMING: 14 MILLISECONDS ON PDP 11/34 WITH FPP
C		1.47 MS ON LEP VAX 11/780
C
	INTEGER*4 NPIX,JPIX
	REAL*4 C(3)
	PARAMETER R0=0.577350269,PPSIDE=65536,EPS=1.E-5
	NFACE=NPIX/PPSIDE
	JPIX=NPIX-NFACE*PPSIDE
	IX=0
	IY=0
	IP=1
10	IF(JPIX.EQ.0) GO TO 100			!Break up the pixel number
	  ID=MOD(JPIX,2)			!By even and odd bits to get
	  JPIX=JPIX/2				!IX and IY
	  IX=ID*IP+IX
	  ID=MOD(JPIX,2)
	  JPIX=JPIX/2
	  IY=ID*IP+IY
	  IP=2*IP
	  GO TO 10
100	X=(IX-127.5)/128.			!Database coordinates.  Range
	Y=(IY-127.5)/128.			! -1 -> 1 covers the square face
	CALL FCUBE(X*R0,Y*R0,XI,ETA)		!Distort to tangent plane
	XI=XI/R0
	ETA=ETA/R0
150	CALL INCUBE(XI,ETA,XT,YT)		!Check the back transform
	DXI=X-XT
	DETA=Y-YT
	XI=XI+DXI
	ETA=ETA+DETA
	IF(ABS(DXI)+ABS(DETA).GT.EPS) GO TO 150		!Iterate to perfection
	CALL XYAXIS(NFACE,XI,ETA,C)
	RETURN
	END
	SUBROUTINE DIRBE_NABORS(NPIX,NN,NABES)
C
C	GIVEN INPUT PIXEL NPIX, FIND ITS NEIGHBORS
C	PUT # OF NEIGHBORS INTO NN
C	AND PIXEL NUMBERS OF THE NEIGHBORS INTO NABES(1),...NABES(NN)
C
	INTEGER*4 NABES(4),NPIX,JPIX,IXTAB,IYTAB
	REAL*4 C(3)
	COMMON /DRBBIT/ IXTAB(256),IYTAB(256)
C
	IF(IXTAB(256).EQ.0) THEN		!Make sure the bit table is set
	  C(1)=1.
	  CALL DIRBE_PIXNO(C,NABES)
	ENDIF
	NN=4				!Always 4 neighbors
	NFACE=NPIX/65536		!Get face
	JPIX=NPIX-NFACE*65536
	IX=0
	IY=0
	IP=1
10	IF(JPIX.EQ.0) GO TO 100		!Break up even and odd bits
	  ID=MOD(JPIX,2)
	  JPIX=JPIX/2
	  IX=ID*IP+IX
	  ID=MOD(JPIX,2)
	  JPIX=JPIX/2
	  IY=ID*IP+IY
	  IP=2*IP
	  GO TO 10
C
100	CALL EDGCHK(NFACE,IX+1,IY,256,MFACE,JX,JY)
	NABES(1)=MFACE*65536+IXTAB(JX+1)+IYTAB(JY+1)
	CALL EDGCHK(NFACE,IX,IY+1,256,MFACE,JX,JY)
	NABES(2)=MFACE*65536+IXTAB(JX+1)+IYTAB(JY+1)
	CALL EDGCHK(NFACE,IX-1,IY,256,MFACE,JX,JY)
	NABES(3)=MFACE*65536+IXTAB(JX+1)+IYTAB(JY+1)
	CALL EDGCHK(NFACE,IX,IY-1,256,MFACE,JX,JY)
	NABES(4)=MFACE*65536+IXTAB(JX+1)+IYTAB(JY+1)
	RETURN
	END
	SUBROUTINE DIRBE_PIXNO(C,NPIX)
C
C	ROUTINE TO DEFINE DIRBE PIXELS BASED ON THE
C	QUADRILATERALIZED SPHERICAL CUBE
C
C	INPUT: REAL*4 C(3)	A unit vector specifying the LOS
C	OUTPUT: INTEGER*4 NPIX	The pixel number: 0 -> 6*256**2-1
C
C	EACH OF 6 FACES OF THE CUBE IS DIVIDED INTO 256 BY 256 PIXELS
C	THE MAX RADIUS OF A PIXEL IS 0.308 DEGREES FOR THE LONG DIAGONAL
C	AT A CORNER.
C	THE RMS RADIUS IS 0.134 DEGREES, AVERAGED OVER THE SPHERE
C
C	TIMING: 3 MILLISECONDS ON A PDP 11/34 WITH FPP
C		0.24 MS ON LEP VAX 11/780
C
	INTEGER*4 NPIX,IX,IY,K,IP
	COMMON /DRBBIT/ IX(256),IY(256)
	DATA IX(256) /0/
	IF(IX(256).EQ.0) THEN			!Set up the lookup tables
	   DO 30 I=1,256			!for converting x,y into
	      J=I-1				!pixel numbers
	      K=0
	      IP=1
10	      IF(J.EQ.0) THEN
		IX(I)=K
		IY(I)=2*K
	      ELSE
		ID=MOD(J,2)
		J=J/2
		K=IP*ID+K
		IP=IP*4
		GO TO 10
	      ENDIF
30	   CONTINUE
	ENDIF
C
	CALL AXISXY(C,NFACE,X,Y)
	I=256.*X
	J=256.*Y
	I=MIN(255,I)		!Roundoff bug fix,	26-JUN-84
	J=MIN(255,J)		!	"	"	"
	NPIX=NFACE*65536+IX(I+1)+IY(J+1)
	RETURN
	END
	SUBROUTINE DMR_CENPIX(NPIX,C)
C
C	CONVERT DMR PIXEL NUMBER NPIX INTO A UNIT VECTOR C IN THE
C	CENTER OF THE PIXEL
C
C	TIMING: 13 MILLISECONDS ON PDP 11/34 WITH FPP
C		1.3 MS ON VAX 11/780
C
	REAL*4 C(3)
	PARAMETER R0=0.577350269,EPS=1.E-5
	NFACE=NPIX/1024
	JPIX=NPIX-NFACE*1024
	IX=0
	IY=0
	IP=1
10	IF(JPIX.EQ.0) GO TO 100
	  ID=MOD(JPIX,2)
	  JPIX=JPIX/2
	  IX=ID*IP+IX
	  ID=MOD(JPIX,2)
	  JPIX=JPIX/2
	  IY=ID*IP+IY
	  IP=2*IP
	  GO TO 10
100	X=(IX-15.5)/16.
	Y=(IY-15.5)/16.
	CALL FCUBE(X*R0,Y*R0,XI,ETA)
	XI=XI/R0
	ETA=ETA/R0
150	CALL INCUBE(XI,ETA,XT,YT)
	DXI=X-XT
	DETA=Y-YT
	XI=XI+DXI
	ETA=ETA+DETA
	IF(ABS(DXI)+ABS(DETA).GT.EPS) GO TO 150
	CALL XYAXIS(NFACE,XI,ETA,C)
	RETURN
	END
	SUBROUTINE DMR_NABORS(NPIX,NN,NABES)
C
C	GIVEN INPUT PIXEL NPIX, FIND ITS NEIGHBORS
C	PUT # OF NEIGHBORS INTO NN
C	AND PIXEL NUMBERS OF THE NEIGHBORS INTO NABES(1),...NABES(NN)
C
	INTEGER NABES(4)
	REAL*4 C(3)
	COMMON /DMRBIT/ IXTAB(32),IYTAB(32)
C
	IF(IXTAB(32).EQ.0) THEN		!Make sure the bit table is set
	  C(1)=1.
	  CALL DMR_PIXNO(C,NABES)
	ENDIF
	NN=4				!Always 4 neighbors
	NFACE=NPIX/1024		!Get face
	JPIX=NPIX-NFACE*1024
	IX=0
	IY=0
	IP=1
10	IF(JPIX.EQ.0) GO TO 100		!Break up even and odd bits
	  ID=MOD(JPIX,2)
	  JPIX=JPIX/2
	  IX=ID*IP+IX
	  ID=MOD(JPIX,2)
	  JPIX=JPIX/2
	  IY=ID*IP+IY
	  IP=2*IP
	  GO TO 10
C
100	CALL EDGCHK(NFACE,IX+1,IY,32,MFACE,JX,JY)
	NABES(1)=MFACE*1024+IXTAB(JX+1)+IYTAB(JY+1)
	CALL EDGCHK(NFACE,IX,IY+1,32,MFACE,JX,JY)
	NABES(2)=MFACE*1024+IXTAB(JX+1)+IYTAB(JY+1)
	CALL EDGCHK(NFACE,IX-1,IY,32,MFACE,JX,JY)
	NABES(3)=MFACE*1024+IXTAB(JX+1)+IYTAB(JY+1)
	CALL EDGCHK(NFACE,IX,IY-1,32,MFACE,JX,JY)
	NABES(4)=MFACE*1024+IXTAB(JX+1)+IYTAB(JY+1)
	RETURN
	END
	SUBROUTINE DMR_PIXNO(C,NPIX)
C
C	ROUTINE TO CONVERT INPUT UNIT VECTOR C INTO A DMR PIXEL NUMBER
C
C	INPUT: REAL*4 C(3)	UNIT VECTOR
C	OUTPUT: INTEGER NPIX, RANGE 1 -> 6*32**2 = 6143
C
C	MAX RADIUS = 2.378 DEGREE FOR WORST CASE IN CORNER
C	RMS PIXEL RADIUS = 1.08 DEGREES AVERAGED OVER SPHERE
C
C	TIMING: 3 MILLISECONDS ON PDP 11/34
C		0.24 MS ON VAX 11/780
C
	COMMON /DMRBIT/ IX(32),IY(32)
	DATA IX(32) /0/
	IF(IX(32).EQ.0) THEN
	   DO 30 I=1,32
	      J=I-1
	      K=0
	      IP=1
10	      IF(J.EQ.0) THEN
		IX(I)=K
		IY(I)=2*K
	      ELSE
		ID=MOD(J,2)
		J=J/2
		K=IP*ID+K
		IP=IP*4
		GO TO 10
	      ENDIF
30	   CONTINUE
	ENDIF
C
	CALL AXISXY(C,NFACE,X,Y)
	I=32.*X
	J=32.*Y
	I=MIN(31,I)		!Roundoff bug fix,	26-Jun-84
	J=MIN(31,J)		!	"	"	"
	NPIX=NFACE*1024+IX(I+1)+IY(J+1)
	RETURN
	END
c*      MODULE : UPX_EDGCHK
c*
ch	Date    Version    SPR #   Programmer   Comments
ch	----    -------    -----   ----------   --------
ch	4/7/89  4.4        3280    E. Boggess   assign the temp values to
ch                                              input parameters; modified
ch                                              so that this routine works!
ch                                              basically, always forcing
ch                                              control to the beginning of
ch                                              the routine as the new IX and
ch                                              IY values are computed.
ch
	SUBROUTINE EDGCHK(NFACE,IX,IY,MAX,MFACE,JX,JY)
C
C	CHECK FOR IX, IY BEING OVER THE EDGE OF A FACE
C	RETURNS CORRECT FACE, IX, IY IN MFACE,JX,JY
C
	INTEGER*4 NFACE, IX, IY, MAX, MFACE, JX, JY
	INTEGER*4 TEMPX, TEMPY, TEMPFACE

	TEMPX = IX
	TEMPY = IY
	TEMPFACE = MOD (NFACE, 6)

99      CONTINUE	    

	IF(TEMPX.LT.0) THEN
	  GO TO (10,20,30,30,30,60), TEMPFACE+1
10	    MFACE=4
	    JY=TEMPX+MAX
	    JX=MAX-1-TEMPY
	    TEMPX = JX
	    TEMPY = JY
	    TEMPFACE = MFACE
	    GOTO 99
20	    MFACE=4
	    GO TO 35
30	    MFACE=TEMPFACE-1
35	    JX=MAX+TEMPX
	    JY=TEMPY
	    TEMPX = JX
	    TEMPFACE = MFACE
	    GOTO 99
60	    MFACE=4
	    JX=TEMPY
	    JY=-TEMPX-1
	    TEMPX = JX
	    TEMPY = JY
	    TEMPFACE = MFACE
	    GOTO 99
	ENDIF

	IF(TEMPX.GE.MAX) THEN
	  GO TO (110,120,120,120,150,160), TEMPFACE+1
110	    MFACE=2
	    JX=TEMPY
	    JY=2*MAX-1-TEMPX
	    TEMPX = JX
	    TEMPY = JY
	    TEMPFACE = MFACE
	    GOTO 99
120	    MFACE=TEMPFACE+1
125	    JY=TEMPY
	    JX=TEMPX-MAX
	    TEMPX = JX
	    TEMPFACE = MFACE
	    GOTO 99
150	    MFACE=1
	    GO TO 125
160	    MFACE=2
	    JX=MAX-1-TEMPY
	    JY=TEMPX-MAX
	    TEMPX = JX
	    TEMPY = JY
	    TEMPFACE = MFACE
	    GOTO 99
	ENDIF

	IF(TEMPY.LT.0) THEN
	  GO TO (210,220,230,240,250,260), TEMPFACE+1
210	    MFACE=1
	    JY=TEMPY+MAX
	    JX=TEMPX
	    TEMPY = JY
	    TEMPFACE = MFACE
	    GOTO 99
220	    MFACE=5
	    JY=TEMPY+MAX
	    JX=TEMPX
	    TEMPY = JY
	    TEMPFACE = MFACE
	    GOTO 99
230	    MFACE=5
	    JX=TEMPY+MAX
	    JY=MAX-1-TEMPX
	    TEMPX = JX
	    TEMPY = JY
	    TEMPFACE = MFACE
	    GOTO 99
240	    MFACE=5
	    JX=MAX-1-TEMPX
	    JY=-TEMPY-1
	    TEMPX = JX
	    TEMPY = JY
	    TEMPFACE = MFACE
	    GOTO 99
250	    MFACE=5
	    JX=-TEMPY-1
	    JY=TEMPX
	    TEMPX = JX
	    TEMPY = JY
	    TEMPFACE = MFACE
	    GOTO 99
260	    MFACE=3
	    JX=MAX-1-TEMPX
	    JY=-TEMPY-1
	    TEMPX = JX
	    TEMPY = JY
	    TEMPFACE = MFACE
	    GOTO 99
	ENDIF

	IF(TEMPY.GE.MAX) THEN
	  GO TO (310,320,330,340,350,360) TEMPFACE+1
310	    MFACE=3
	    JX=MAX-1-TEMPX
	    JY=2*MAX-1-TEMPY
	    TEMPX = JX
	    TEMPY = JY
	    TEMPFACE = MFACE
	    GOTO 99
320	    MFACE=0
	    JY=TEMPY-MAX
	    JX=TEMPX
	    TEMPY = JY
	    TEMPFACE = MFACE
	    GOTO 99
330	    MFACE=0
	    JX=2*MAX-1-TEMPY
	    JY=TEMPX
	    TEMPX = JX
	    TEMPY = JY
	    TEMPFACE = MFACE
	    GOTO 99
340	    MFACE=0
	    JX=MAX-1-TEMPX
	    JY=2*MAX-1-TEMPY
	    TEMPX = JX
	    TEMPY = JY
	    TEMPFACE = MFACE
	    GOTO 99
350	    MFACE=0
	    JX=TEMPY-MAX
	    JY=MAX-1-TEMPX
	    TEMPX = JX
	    TEMPY = JY
	    TEMPFACE = MFACE
	    GOTO 99
360	    MFACE=1
	    JX=TEMPX
	    JY=TEMPY-MAX
	    TEMPY = JY
	    TEMPFACE = MFACE
	    GOTO 99
	ENDIF

	MFACE=TEMPFACE
	JX=TEMPX
	JY=TEMPY
	RETURN
	END
	SUBROUTINE FCUBE(X,Y,XI,ETA)
C
C	THIS FUNCTION EVALUATES THE DIRECT TRANSFORMATION
C	FROM THE EQUAL AREA SQUARE TO THE TANGENT PLANE
C	SEE CSC REPORT ON THE QUADRILATERALIZED SPHERE
C
	REAL*4 X,Y,XI,ETA
	PARAMETER GAMMA=0.7236012545582
	PARAMETER DELTA=0.7904864491208
	PARAMETER OMEGA=-1.225441487984
	PARAMETER C00=-2.7217053661814,C10=-5.5842168305430,
	1   C01=2.1711174809423,C20=-3.4578627473390,
	2   C11=-6.4160151526783,C02=1.9736265758872
	PARAMETER D0=1.4833129294187,D1=1.1199726069742,
	1   D2=6.0515382161464
	PARAMETER THIRD=0.333333333333
	PARAMETER THRESHOLD=0.333334
	XX=X*X
	YY=Y*Y
	IF(AMAX1(XX,YY).GT.THRESHOLD) TYPE *,' X,Y TOO BIG=',X,Y
	RRXX=THIRD-XX
	RRYY=THIRD-YY
	XXXX=XX*XX
	YYYY=YY*YY
	XXYY=XX*YY
	XI=X*(GAMMA+(1.-GAMMA)*XX/THIRD + RRXX*(
	1  YY*(DELTA+RRYY*(C00+C10*XX+C01*YY+C20*XXXX+C11*XXYY+C02*YYYY))
	3 +XX*(OMEGA+RRXX*(D0+D1*XX+D2*XXXX)) ))
	ETA=Y*(GAMMA+(1.-GAMMA)*YY/THIRD + RRYY*(
	1 +XX*(DELTA+RRXX*(C00+C10*YY+C01*XX+C20*YYYY+C11*XXYY+C02*XXXX))
	3 +YY*(OMEGA+RRYY*(D0+D1*YY+D2*YYYY)) ))
	RETURN
	END
	SUBROUTINE FIRAS_CENPIX(NPIX,C)
C
C	FIRAS PIXELS ARE IDENTICAL TO DMR PIXELS
C
	CALL DMR_CENPIX(NPIX,C)
	RETURN
	END
	SUBROUTINE FIRAS_NABORS(NPIX,NN,NABES)
C
C	FIRAS PIXELS ARE IDENTICAL TO DMR PIXELS
C
	CALL DMR_NABORS(NPIX,NN,NABES)
	RETURN
	END
	SUBROUTINE FIRAS_PIXNO(C,NPIX)
C
C	FIRAS PIXELS ARE IDENTICAL TO DMR PIXELS
C
	CALL DMR_PIXNO(C,NPIX)
	RETURN
	END
	SUBROUTINE FORWARD_CUBE(X,Y,XI,ETA)
	REAL*4 X,Y,XI,ETA
C
C	INPUT: X,Y IN RANGE -1 TO +1 ARE DATABASE CO-ORDINATES
C
C	OUTPUT: XI, ETA IN RANGE -1 TO +1 ARE TANGENT PLANE CO-ORDINATES
C
C	BASED ON POLYNOMIAL FIT FOUND USING FCFIT.FOR
C
	PARAMETER (NP=28)
	REAL*4 P(NP)
	DATA P/
	1	 -0.27292696, -0.07629969, -0.02819452, -0.22797056,
	2	 -0.01471565,  0.27058160,  0.54852384,  0.48051509,
	3	 -0.56800938, -0.60441560, -0.62930065, -1.74114454,
	4	  0.30803317,  1.50880086,  0.93412077,  0.25795794,
	5	  1.71547508,  0.98938102, -0.93678576, -1.41601920,
	6	 -0.63915306,  0.02584375, -0.53022337, -0.83180469,
	7	  0.08693841,  0.33887446,  0.52032238,  0.14381585/
	XX=X*X
	YY=Y*Y
	XI=X*(1.+(1.-XX)*(
	1 P(1)+XX*(P(2)+XX*(P(4)+XX*(P(7)+XX*(P(11)+XX*(P(16)+XX*P(22)))))) +
	2 YY*( P(3)+XX*(P(5)+XX*(P(8)+XX*(P(12)+XX*(P(17)+XX*P(23))))) +
	3 YY*( P(6)+XX*(P(9)+XX*(P(13)+XX*(P(18)+XX*P(24)))) +
	4 YY*( P(10)+XX*(P(14)+XX*(P(19)+XX*P(25))) + 
	5 YY*( P(15)+XX*(P(20)+XX*P(26)) +
	6 YY*( P(21)+XX*P(27) + YY*P(28))))) )))
	ETA=Y*(1.+(1.-YY)*(
	1 P(1)+YY*(P(2)+YY*(P(4)+YY*(P(7)+YY*(P(11)+YY*(P(16)+YY*P(22)))))) +
	2 XX*( P(3)+YY*(P(5)+YY*(P(8)+YY*(P(12)+YY*(P(17)+YY*P(23))))) +
	3 XX*( P(6)+YY*(P(9)+YY*(P(13)+YY*(P(18)+YY*P(24)))) +
	4 XX*( P(10)+YY*(P(14)+YY*(P(19)+YY*P(25))) + 
	5 XX*( P(15)+YY*(P(20)+YY*P(26)) +
	6 XX*( P(21)+YY*P(27) + XX*P(28))))) )))
	RETURN
	END
	SUBROUTINE INCUBE(ALPHA,BETA,X,Y)
C
C	SEE CSC "EXTENDED STUDY ... NOTE THAT THE TEXT HAS TYPOS.  I HAVE
C	TRIED TO COPY THE FORTRAN LISTINGS
c*
c*     PROLOGUE:
c*
ch     Date    Version   SPR#   Programmer    Comments
ch     ----    -------   ----   ----------    --------
ch
ch   08/20/90    6.7     7003   A.C.Raugh     Minor modifications to improve
ch                                            efficiency
ch  
C
	REAL*4 ALPHA,BETA
        Real*4 GSTAR_1,M_G,C_COMB
	PARAMETER GSTAR=1.37484847732,G=-0.13161671474,
	1   M=0.004869491981,W1=-0.159596235474,C00=0.141189631152,
	2   C10=0.0809701286525,C01=-0.281528535557,C11=0.15384112876,
	3   C20=-0.178251207466,C02=0.106959469314,D0=0.0759196200467,
	4   D1=-0.0217762490699
	PARAMETER R0=0.577350269
	AA=ALPHA**2
	BB=BETA**2
	A4=AA**2
	B4=BB**2
	ONMAA=1.-AA
	ONMBB=1.-BB

        gstar_1 = 1. - gstar
        m_g     = m - g
        c_comb  = c00 + c11*aa*bb

	X = ALPHA * 
     +       (GSTAR + 
     +        AA * gstar_1 + 
     +        ONMAA * (BB * (G + (m_g)*AA + 
     +	                     ONMBB * (c_comb + C10*AA + C01*BB +
     +                                C20*A4 + C02*B4)) +
     +	               AA * (W1 - ONMAA*(D0 + D1*AA))))

	Y = BETA * 
     +       (GSTAR + 
     +        BB * gstar_1 + 
     +        ONMBB * (AA * (G + (m_g)*BB +
     +                       ONMAA * (c_comb + C10*BB + C01*AA +
     +                                C20*B4+C02*A4)) + 
     +                 BB * (W1 - ONMBB*(D0 + D1*BB))))
	RETURN
	END
      Subroutine upx_PIXEL_NUMBER(vector,resolution,pixel)
C
C    Routine to return the pixel number corresponding to the input unit
C  vector in the given resolution.  Adapted from the SUPER_PIXNO routine
C  by E. Wright, this routine determines the pixel number in the maximum
C  resolution (15), then divides by the appropriate power of 4 to determine
C  the pixel number in the desired resolution.
c*
c*     PROLOGUE:
c*
ch     Date    Version   SPR#   Programmer    Comments
ch     ----    -------   ----   ----------    --------
ch
ch  08/20/90     6.7     7003   A.C.Raugh     Minor modifications to improve
ch                                            efficiency
ch
C
      Implicit NONE
C
C  Arguments:
C
      Real*4        VECTOR(3)     ! Unit vector of position of interest
      Integer*2     RESOLUTION    ! Resolution of quad cube
      Integer*4     PIXEL         ! Pixel number (0 -> 6*((2**(res-1))**2)
C
C  Variables:
C
      Integer*2     FACE2             ! I*2 Cube face number (0-5)
      Integer*4     FACE4             ! I*4 Cube face number
      Real*4        X,Y               ! Coordinates on cube face
      Integer*4     IX(128),IY(128)   ! Bit tables for calculating I,J
      Integer*4     I,J               ! Integer pixel coordinates
      Integer*4     IP,ID       
      Integer*4     FOUR              ! I*4 '4' - reuired to avoid integer
                                      !  overflow for large pixel numbers
      Integer*2     IL,IH,JL,JH       ! High and low 2-bytes of I & J
      Integer*2     seven/7/
C
      Parameter  TWO14 = 2**14, TWO28 = 2**28
      Parameter  (FOUR = 4)
C
      Data IX(128) /0/
      if (ix(128) .eq. 0) call upx_BIT_TABLE_SET(ix,iy,128)
C
      call AXISXY(vector,face4,x,y)
c      face4 = face2
      i = 2.**14 * x
      j = 2.**14 * y
      if (i .gt. 16383) i = 16383
      if (j .gt. 16383) j = 16383
      ih = jishft(i,-seven)
      il = i - iishft(ih,seven)
      jh = jishft(j,-seven)
      jl = j - iishft(jh,seven)
      pixel = jishft(face4,28) + ix(il+1) + iy(jl+1) +
     +             jishft((ix(ih+1) + iy(jh+1)),14)
C
C     'Pixel' now contains the pixel number for a resolution of 15.  To
C     convert to the desired resolution, (integer) divide by 4 to the power
C     of the difference between 15 and the given resolution:
C
      pixel = jishft(pixel,-(30-2*resolution))  !  = pixel / 4**(15-resolution)
C
      return
      end
      Subroutine upx_PIXEL_VECTOR(pixel,resolution,vector)
C
C    Routine to return unit vector pointing to center of pixel given pixel
C  number and resolution of the cube.  Based on exisiting _CENPIX routines
C  in CSDRLIB.
c*
c*     PROLOGUE:
c*
ch     Date    Version   SPR#   Programmer    Comments
ch     ----    -------   ----   ----------    --------
ch
ch   08/20/90    6.7     7003   A.C.Raugh     Minor modifications to improve
ch                                            efficiency
ch
ch   09/11/90    6.8     7415   A.C.Raugh     Modified to handle degenerate
ch                                            cube (resolution=1) without 
ch                                            arithmetic error
ch
C
      Implicit NONE
C
C  Arguments:
C
      Integer*4      PIXEL         ! Pixel number
      Integer*2      RESOLUTION    ! Quad-cube resolution
      Real*4         VECTOR(3)     ! Unit vector to center of pixel
C
C  Variables:
C
      Integer*4      FACE             ! Face number
      Integer        JX,JY,IP,ID      ! Loop variables
      Real           X,Y              ! Face coordinates
      Real*4         SCALE              
      Integer*4      PIXELS_PER_FACE  ! Number of pixels on a single face
      Integer*4      FPIX             ! Pixel number within the face
      Real           XI,ETA           ! 'Database' coordinates
      Integer*4      ONE,TWO          ! Constants used to avoid integer
                                      !   overflow for large pixel numbers
      Integer*4      BIT_MASK         ! Bit mask to select the low order bit
C
      Parameter (ONE = 1)
      Parameter (TWO = 2)
      Parameter (BIT_MASK = 1)
C
      scale = 2**(resolution-1) / 2.0
      pixels_per_face = two**(two*(resolution-one))
C
      face = pixel/pixels_per_face          ! Note: Integer division truncates
      fpix = pixel - face*pixels_per_face
C 
C  Break pixel number down into x and y bits:
C
      jx = 0      
      jy = 0
      ip = 1
      do while (fpix .ne. 0)
          id = iand(bit_mask,fpix)  !  = mod(fpix,2)
          fpix = ishft(fpix,-1)     !  = fpix / 2
          jx = id * ip + jx
          id = iand(bit_mask,fpix)
          fpix = ishft(fpix,-1)
          jy = id * ip + jy
          ip = ip * 2
        end do
C
      x = (jx - scale + 0.5) / scale
      y = (jy - scale + 0.5) / scale
      call FORWARD_CUBE(x,y,xi,eta)
      call XYAXIS(face,xi,eta,vector)
C
      return
      end
	SUBROUTINE SPRCNPX_SET
C
	INTEGER*2 JX,JY
	COMMON /SPRCNPX/ JX(0:1023),JY(0:1023)
	DATA JX(1023)/0/
C
	SAVE /SPRCNPX/
	DO KPIX=0,1023
	  JPIX=KPIX
	  IX=0
	  IY=0
	  IP=1
	  DO WHILE (JPIX.NE.0)			!Break up the pixel number
	    ID=MOD(JPIX,2)			!By even and odd bits to get
	    JPIX=JPIX/2				!IX and IY
	    IX=ID*IP+IX
	    ID=MOD(JPIX,2)
	    JPIX=JPIX/2
	    IY=ID*IP+IY
	    IP=2*IP
	  ENDDO
	  JX(KPIX)=IX
	  JY(KPIX)=IY
	ENDDO
	RETURN
	END
	SUBROUTINE SUPER_CENPIX(NSUPER,C)
	INTEGER*4 NSUPER			!Input pixel number
	REAL*4 C(3)				!Output unit vector
	PARAMETER IT28=2**28
C
	INTEGER*2 IX,IY
	COMMON /SPRCNPX/ IX(0:1023),IY(0:1023)
	IF(IX(1023).NE.31) CALL SPRCNPX_SET
C
	NFACE=NSUPER/IT28
	N=MOD(NSUPER,IT28)
	I=MOD(N,1024)
	N=N/1024
	J=MOD(N,1024)
	K=N/1024
	JX=1024*IX(K)+32*IX(J)+IX(I)
	JY=1024*IY(K)+32*IY(J)+IY(I)
	X=(JX-8191.5)/8192.			!Database coordinates.  Range
	Y=(JY-8191.5)/8192.			! -1 -> 1 covers the square face
	CALL FORWARD_CUBE(X,Y,XI,ETA)		!Distort to tangent plane
C
C	TWO ITERATIONS TO IMPROVE THE DISTORTION
C
	CALL INCUBE(XI,ETA,XP,YP)
	XI=XI-(XP-X)
	ETA=ETA-(YP-Y)
	CALL INCUBE(XI,ETA,XP,YP)
	XI=XI-(XP-X)
	ETA=ETA-(YP-Y)
	CALL XYAXIS(NFACE,XI,ETA,C)
	RETURN
	END
	SUBROUTINE SUPER_PIXNO(C,NSUPER)
	REAL*4 C(3)	!Unit vector input
	INTEGER*4 NSUPER	!Output pixel number 1,...,6*2**28
	PARAMETER IT14=16384,IT28=2**28
C
C	PIXELIZES SPHERE TO 20" ACCURACY, EQUAL-AREA PIXELS USING
C	QUADRILATERIZED SPHERICAL CUBE
C
	INTEGER*4 NPIX,IX,IY,K,IP
	SAVE /SPRBIT/
	COMMON /SPRBIT/ IX(128),IY(128)
	DATA IX(128) /0/
	IF(IX(128).EQ.0) THEN			!Set up the lookup tables
	   DO I=1,128				!for converting x,y into
	      J=I-1				!pixel numbers
	      K=0
	      IP=1
10	      IF(J.EQ.0) THEN
		IX(I)=K
		IY(I)=2*K
	      ELSE
		ID=MOD(J,2)
		J=J/2
		K=IP*ID+K
		IP=IP*4
		GO TO 10
	      ENDIF
	   ENDDO
	ENDIF
	CALL AXISXY(C,NFACE,X,Y)
	I=IT14*X
	J=IT14*Y
	I=MIN(IT14-1,I)			!Roundoff protection for cases very
	J=MIN(IT14-1,J)			!near an edge of the cube
	IL=MOD(I,128)
	IH=I/128
	JL=MOD(J,128)
	JH=J/128
	NSUPER=NFACE*IT28+IX(IL+1)+IY(JL+1)+IT14*(IX(IH+1)+IY(JH+1))
	RETURN
	END
	SUBROUTINE XYAXIS(NFACE,XI,ETA,C)
C
C	CONVERTS FACE NUMBER NFACE (0-5) AND XI, ETA (-1. - +1.)
C	INTO A UNIT VECTOR C
c*
c*     PROLOGUE:
c*
ch     Date    Version   SPR#   Programmer    Comments
ch     ----    -------   ----   ----------    --------
ch
ch  08/20/90     6.7     7003   A.C.Raugh     Minor modifications to improve
ch                                            efficiency
ch
C
	REAL*4 C(3),XI,ETA,NORM
	GO TO (200,210,220,230,240,250), NFACE+1
200	  C(3)=1.
	  C(1)=-ETA				!Transform face to sphere
	  C(2)=XI
	  GO TO 300
210	  C(1)=1.
	  C(3)=ETA
	  C(2)=XI
	  GO TO 300
220	  C(2)=1.
	  C(3)=ETA
	  C(1)=-XI
	  GO TO 300
230	  C(1)=-1.
	  C(3)=ETA
	  C(2)=-XI
	  GO TO 300
240	  C(2)=-1.
	  C(3)=ETA
	  C(1)=XI
	  GO TO 300
250	  C(3)=-1.
	  C(1)=ETA
	  C(2)=XI
	  GO TO 300
C
 300	norm = sqrt(c(1)**2+c(2)**2+c(3)**2)
        c(1) = c(1) / norm
        c(2) = c(2) / norm
        c(3) = c(3) / norm
	RETURN
	END

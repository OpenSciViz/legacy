	Subroutine upx_8_NEIGHBORS(pixel,res,
     +                             neighbors,number_of_neighbors)
C
C    Generalized routine to return the numbers of all pixels adjoining the
C  input pixel at the given resolution.
C
C  This routine will work for resolutions up to 15.
C
C==============================================================================
C
C  Method:
C
C     1.  All eight adjoining pixels are found by manipulation of the
C         cartesian coordinates of the input pixel in the plane of the
C         face on which it lies.  See UPX_4_NEIGHBORS opening comments 
C         for a detailed description of this process.
C
C     2.  Once all eight neighbors are found, the NEIGHBORS array is sorted
C         and duplicate pixel numbers are deleted.
C
C     3.  The number of unique neighbors and the array of pixel numbers
C         are returned.
C
C==============================================================================
C
C  09/28/88, acr.
C
C  Arguments:
C
      Integer*4        PIXEL                ! Pixel number
      Integer*2        RES                  ! Resolution
      Integer*4        NEIGHBORS(8)         ! Neighboring pixel numbers
      Integer*4        NUMBER_OF_NEIGHBORS  ! Number of adjoining pixels
                                            !  (4-8)
C
C  Variables:
C
      Integer*4        FACE                    ! Face number of PIXEL
      Integer*4        NFACE                   ! Face number of neighbor
      Integer*4        PIXELS_PER_FACE         ! Pixels on a face
      Integer*4        RPIXEL                  ! Relative pixel number on face
      Integer*4        TWO                     ! 2
      Integer*4        FOUR                    ! 4 
      Integer*4        TWO14                   ! 2**14
      Integer*4        TWO28                   ! 2**28
      Integer*4        DIVISOR                 ! Converts resolution 15
                                               !  pixel number to input
                                               !  resolution
      Integer*4        RES_DIFF                ! Difference between 15 and
                                               !  input resolution
      Integer*4        DISTANCE                ! Distance between pixel and
                                               !  neighbor at resolution 15
      Integer*4        IX,IY                   ! Pixel cart. coordinates
                                               !  on cube face
      Integer*4        JXHI,JXLO,JYHI,JYLO    
      Integer*4        IP,ID                   ! Var. for breaking down PIXEL
      Integer*4        JX,JY                   ! Neighbor cart. coordinates
                                               !  on cube face
      Integer*4        IXTAB(128),IYTAB(128)   ! Bit tables for converting
                                               !  pixel number on face to
                                               !  cartesian coordinates
      Integer*4        TEMP_PIXEL              ! Pixel number holding place
      Integer*4        TEMP_NBR(8)             ! Neighbor array holding place
      Integer*4        I,J                     ! Sort loop variables

c      Data IXTAB(128) /0/

      Parameter (two = 2)
      Parameter (four = 4)
      Parameter (two14 = 2**14)
      Parameter (two28 = 2**28)

c      if (ixtab(128) .eq. 0) call upx_BIT_TABLE_SET(ixtab,iytab,128)
      if (ixtab(128) .eq. 0) call upx_BIT_TABLE_SET(ixtab,iytab)

      pixels_per_face = (two**(res - 1)) ** 2
      face     = pixel / pixels_per_face
      rpixel   = pixel - face*pixels_per_face
      res_diff = 15-res
      divisor  = four**(res_diff)
      distance = two**(res_diff)

      ix = 0
      iy = 0
      ip = 1
C
C  Break pixel number down into constituent x,y coordinates:
C 
      do while (rpixel .ne. 0)

          id = mod(rpixel,2)
          rpixel = rpixel / 2
          ix = id * ip + ix

          id = mod(rpixel,2)
          rpixel = rpixel / 2
          iy = id * ip + iy

          ip = 2 * ip

        end do
C
C  Convert x,y coordinates of pixel in initial resolution to resolution
C  of 15:
C
      ix = ix * distance 
      iy = iy * distance 
C                   
C  Calculate coordinates of each neighbor, check for edges, and return pixel
C  number in appropriate array element:
C
      Call edgchk(face,ix+distance,iy,two14,nface,jx,jy)   ! Right 
      jxhi = jx/128
      jxlo = mod(jx,128)
      jyhi = jy/128
      jylo = mod(jy,128)
      temp_nbr(1) = nface * two28 + 
     +              ixtab(jxlo+1) + iytab(jylo+1) +
     +              two14 * (ixtab(jxhi+1) + iytab(jyhi+1))
      temp_nbr(1) = temp_nbr(1) / divisor

      Call edgchk(face,ix,iy+distance,two14,nface,jx,jy)   ! Top 
      jxhi = jx/128
      jxlo = mod(jx,128)
      jyhi = jy/128
      jylo = mod(jy,128)
      temp_nbr(2) = nface * two28 + 
     +              ixtab(jxlo+1) + iytab(jylo+1) +
     +              two14 * (ixtab(jxhi+1) + iytab(jyhi+1))
      temp_nbr(2) = temp_nbr(2) / divisor

      Call edgchk(face,ix-distance,iy,two14,nface,jx,jy)   ! Left 
      jxhi = jx/128
      jxlo = mod(jx,128)                 
      jyhi = jy/128
      jylo = mod(jy,128)
      temp_nbr(3) = nface * two28 + 
     +              ixtab(jxlo+1) + iytab(jylo+1) +
     +              two14 * (ixtab(jxhi+1) + iytab(jyhi+1))
      temp_nbr(3) = temp_nbr(3) / divisor

      Call edgchk(face,ix,iy-distance,two14,nface,jx,jy)   ! Bottom
      jxhi = jx/128
      jxlo = mod(jx,128)
      jyhi = jy/128
      jylo = mod(jy,128)
      temp_nbr(4) = nface * two28 + 
     +              ixtab(jxlo+1) + iytab(jylo+1) +
     +              two14 * (ixtab(jxhi+1) + iytab(jyhi+1))
      temp_nbr(4) = temp_nbr(4) / divisor


      Call edgchk(face,ix+distance,iy+distance,two14,nface,jx,jy)   ! Top-Right
      jxhi = jx/128
      jxlo = mod(jx,128)
      jyhi = jy/128
      jylo = mod(jy,128)
      temp_nbr(5) = nface * two28 + 
     +              ixtab(jxlo+1) + iytab(jylo+1) +
     +              two14 * (ixtab(jxhi+1) + iytab(jyhi+1))
      temp_nbr(5) = temp_nbr(5) / divisor

      Call edgchk(face,ix-distance,iy+distance,two14,nface,jx,jy)   ! Top-Left
      jxhi = jx/128
      jxlo = mod(jx,128)
      jyhi = jy/128
      jylo = mod(jy,128)
      temp_nbr(6) = nface * two28 + 
     +              ixtab(jxlo+1) + iytab(jylo+1) +
     +              two14 * (ixtab(jxhi+1) + iytab(jyhi+1))
      temp_nbr(6) = temp_nbr(6) / divisor

      Call edgchk(face,ix-distance,iy-distance,two14,nface,jx,jy)   ! Bottom-Left 
      jxhi = jx/128
      jxlo = mod(jx,128)
      jyhi = jy/128
      jylo = mod(jy,128)
      temp_nbr(7) = nface * two28 + 
     +              ixtab(jxlo+1) + iytab(jylo+1) +
     +              two14 * (ixtab(jxhi+1) + iytab(jyhi+1))
      temp_nbr(7) = temp_nbr(7) / divisor

      Call edgchk(face,ix+distance,iy-distance,two14,nface,jx,jy)   ! Bottom-Right
      jxhi = jx/128
      jxlo = mod(jx,128)
      jyhi = jy/128
      jylo = mod(jy,128)
      temp_nbr(8) = nface * two28 + 
     +              ixtab(jxlo+1) + iytab(jylo+1) +
     +              two14 * (ixtab(jxhi+1) + iytab(jyhi+1))
      temp_nbr(8) = temp_nbr(8) / divisor
C
C  Bubble sort of neighboring pixels:
C
      do i = 1, 7
          do j = i+1, 8
              if (temp_nbr(i) .gt. temp_nbr(j)) then
                  temp_pixel  = temp_nbr(i)
                  temp_nbr(i) = temp_nbr(j)
                  temp_nbr(j) = temp_pixel
                end if
            end do
        end do
C
C  Delete duplicate pixel numbers:
C
      j = 1
      neighbors(1) = temp_nbr(1)
      do i = 2, 8
          if (neighbors(j) .ne. temp_nbr(i)) then
              j = j + 1
              neighbors(j) = temp_nbr(i)
            end if
        end do

      number_of_neighbors = j

      Return
      end
	SUBROUTINE AXISXY(C,NFACE,X,Y)
C
C	CONVERTS UNIT VECTOR C INTO NFACE NUMBER (0-5) AND X,Y
C	IN RANGE 0-1
C
	REAL*4 C(3)
	AC3=ABS(C(3))
	AC2=ABS(C(2))
	AC1=ABS(C(1))
	IF(AC3.GT.AC2) THEN
	   IF(AC3.GT.AC1) THEN
	      IF(C(3).GT.0.) GO TO 100
		GO TO 150
	   ELSE
	      IF(C(1).GT.0.) GO TO 110
		GO TO 130
	   ENDIF
	ELSE
	   IF(AC2.GT.AC1) THEN
	      IF(C(2).GT.0.) GO TO 120
		GO TO 140
	   ELSE
	      IF(C(1).GT.0.) GO TO 110
		GO TO 130
	   ENDIF
	ENDIF
100	NFACE=0
	ETA=-C(1)/C(3)
	XI=C(2)/C(3)
	GO TO 200
110	NFACE=1
	XI=C(2)/C(1)
	ETA=C(3)/C(1)
	GO TO 200
120	NFACE=2
	ETA=C(3)/C(2)
	XI=-C(1)/C(2)
	GO TO 200
130	NFACE=3
	ETA=-C(3)/C(1)
	XI=C(2)/C(1)
	GO TO 200
140	NFACE=4
	ETA=-C(3)/C(2)
	XI=-C(1)/C(2)
	GO TO 200
150	NFACE=5
	ETA=-C(1)/C(3)
	XI=-C(2)/C(3)
	GO TO 200
C
200	CALL INCUBE(XI,ETA,X,Y)
	X=(X+1.)/2.
	Y=(Y+1.)/2.
	RETURN
	END

c      Subroutine upx_BIT_TABLE_SET(IX,IY,LENGTH)
      Subroutine upx_BIT_TABLE_SET(IX,IY)
C
C    Routine to set up the bit tables for use in the pixelization subroutines 
C  (extracted and generalized from existing routines).
C
C  Arguments:
C
      Integer*2   LENGTH /128/      ! Number of elements in IX, IY
      Integer*4   IX(length)   ! X bits
      Integer*4   IY(length)   ! Y bits
C
C  Variables:
C
      Integer*4   i,j,k,ip,id  ! Loop variables
C
      Do 30 I = 1,length
          j = i - 1
          k = 0
          ip = 1
   10     if (j .eq. 0) then
              ix(i) = k
              iy(i) = 2*k
            else
              id = mod(j,2)
              j = j/2
              k = ip * id + k
              ip = ip * 4
              go to 10
            endif
   30   continue
C
      return
      end
	SUBROUTINE EDGCHK(NFACE,IX,IY,MAX,MFACE,JX,JY)
C
C	CHECK FOR IX, IY BEING OVER THE EDGE OF A FACE
C	RETURNS CORRECT FACE, IX, IY IN MFACE,JX,JY
C
	IF(IX.LT.0) THEN
	  GO TO (10,20,30,30,30,60), NFACE+1
10	    MFACE=4
	    JY=IX+MAX
	    JX=MAX-1-IY
	    RETURN
20	    MFACE=4
	    GO TO 35
30	    MFACE=NFACE-1
35	    JX=MAX+IX
	    JY=IY
	    RETURN
60	    MFACE=4
	    JX=IY
	    JY=-IX-1
	    RETURN
	ENDIF
	IF(IX.GE.MAX) THEN
	  GO TO (110,120,120,120,150,160), NFACE+1
110	    MFACE=2
	    JX=IY
	    JY=2*MAX-1-IX
	    RETURN
120	    MFACE=NFACE+1
125	    JY=IY
	    JX=IX-MAX
	    RETURN
150	    MFACE=1
	    GO TO 125
160	    MFACE=2
	    JX=MAX-1-IY
	    JY=IX-MAX
	    RETURN
	ENDIF
	IF(IY.LT.0) THEN
	  GO TO (210,220,230,240,250,260), NFACE+1
210	    MFACE=1
	    JY=IY+MAX
	    JX=IX
	    RETURN
220	    MFACE=5
	    JY=IY+MAX
	    JX=IX
	    RETURN
230	    MFACE=5
	    JX=IY+MAX
	    JY=MAX-1-IX
	    RETURN
240	    MFACE=5
	    JX=MAX-1-IX
	    JY=-IY-1
	    RETURN
250	    MFACE=5
	    JX=-IY-1
	    JY=IX
	    RETURN
260	    MFACE=3
	    JX=MAX-1-IX
	    JY=-IY-1
	    RETURN
	ENDIF
	IF(IY.GE.MAX) THEN
	  GO TO (310,320,330,340,350,360) NFACE+1
310	    MFACE=3
	    JX=MAX-1-IX
	    JY=2*MAX-1-IY
	    RETURN
320	    MFACE=0
	    JY=IY-MAX
	    JX=IX
	    RETURN
330	    MFACE=0
	    JX=2*MAX-1-IY
	    JY=IX
	    RETURN
340	    MFACE=0
	    JX=MAX-1-IX
	    JY=2*MAX-1-IY
	    RETURN
350	    MFACE=0
	    JX=IY-MAX
	    JY=MAX-1-IX
	    RETURN
360	    MFACE=1
	    JX=IX
	    JY=IY-MAX
	    RETURN
	ENDIF
	MFACE=NFACE
	JX=IX
	JY=IY
	RETURN
	END
	SUBROUTINE FORWARD_CUBE(X,Y,XI,ETA)
	REAL*4 X,Y,XI,ETA
C
C	INPUT: X,Y IN RANGE -1 TO +1 ARE DATABASE CO-ORDINATES
C
C	OUTPUT: XI, ETA IN RANGE -1 TO +1 ARE TANGENT PLANE CO-ORDINATES
C
C	BASED ON POLYNOMIAL FIT FOUND USING FCFIT.FOR
C
	PARAMETER (NP=28)
	REAL*4 P(NP)
	DATA P/
	1	 -0.27292696, -0.07629969, -0.02819452, -0.22797056,
	2	 -0.01471565,  0.27058160,  0.54852384,  0.48051509,
	3	 -0.56800938, -0.60441560, -0.62930065, -1.74114454,
	4	  0.30803317,  1.50880086,  0.93412077,  0.25795794,
	5	  1.71547508,  0.98938102, -0.93678576, -1.41601920,
	6	 -0.63915306,  0.02584375, -0.53022337, -0.83180469,
	7	  0.08693841,  0.33887446,  0.52032238,  0.14381585/
	XX=X*X
	YY=Y*Y
	XI=X*(1.+(1.-XX)*(
	1 P(1)+XX*(P(2)+XX*(P(4)+XX*(P(7)+XX*(P(11)+XX*(P(16)+XX*P(22)))))) +
	2 YY*( P(3)+XX*(P(5)+XX*(P(8)+XX*(P(12)+XX*(P(17)+XX*P(23))))) +
	3 YY*( P(6)+XX*(P(9)+XX*(P(13)+XX*(P(18)+XX*P(24)))) +
	4 YY*( P(10)+XX*(P(14)+XX*(P(19)+XX*P(25))) + 
	5 YY*( P(15)+XX*(P(20)+XX*P(26)) +
	6 YY*( P(21)+XX*P(27) + YY*P(28))))) )))
	ETA=Y*(1.+(1.-YY)*(
	1 P(1)+YY*(P(2)+YY*(P(4)+YY*(P(7)+YY*(P(11)+YY*(P(16)+YY*P(22)))))) +
	2 XX*( P(3)+YY*(P(5)+YY*(P(8)+YY*(P(12)+YY*(P(17)+YY*P(23))))) +
	3 XX*( P(6)+YY*(P(9)+YY*(P(13)+YY*(P(18)+YY*P(24)))) +
	4 XX*( P(10)+YY*(P(14)+YY*(P(19)+YY*P(25))) + 
	5 XX*( P(15)+YY*(P(20)+YY*P(26)) +
	6 XX*( P(21)+YY*P(27) + XX*P(28))))) )))
	RETURN
	END
	SUBROUTINE INCUBE(ALPHA,BETA,X,Y)
C
C	SEE CSC "EXTENDED STUDY ... NOTE THAT THE TEXT HAS TYPOS.  I HAVE
C	TRIED TO COPY THE FORTRAN LISTINGS
C
	REAL*4 ALPHA,BETA
	PARAMETER GSTAR=1.37484847732,G=-0.13161671474,
	1   M=0.004869491981,W1=-0.159596235474,C00=0.141189631152,
	2   C10=0.0809701286525,C01=-0.281528535557,C11=0.15384112876,
	3   C20=-0.178251207466,C02=0.106959469314,D0=0.0759196200467,
	4   D1=-0.0217762490699
	PARAMETER R0=0.577350269
	AA=ALPHA**2
	BB=BETA**2
	A4=AA**2
	B4=BB**2
	ONMAA=1.-AA
	ONMBB=1.-BB
	X=ALPHA*(GSTAR+AA*(1.-GSTAR)+ONMAA*(BB*(G+(M-G)*AA
	1   +ONMBB*(C00+C10*AA+C01*BB+C11*AA*BB+C20*A4+C02*B4))
	2   +AA*(W1-ONMAA*(D0+D1*AA))))
	Y=BETA*(GSTAR+BB*(1.-GSTAR)+ONMBB*(AA*(G+(M-G)*BB
	1   +ONMAA*(C00+C10*BB+C01*AA+C11*BB*AA+C20*B4+C02*A4))
	2   +BB*(W1-ONMBB*(D0+D1*BB))))
	RETURN
	END
      Subroutine upx_PIXEL_NUMBER(vector,resolution,pixel)
C
C    Routine to return the pixel number corresponding to the input unit
C  vector in the given resolution.  Adapted from the SUPER_PIXNO routine
C  by E. Wright, this routine determines the pixel number in the maximum
C  resolution (15), then divides by the appropriate power of 4 to determine
C  the pixel number in the desired resolution.
C
      Implicit NONE
C
C  Arguments:
C
      Real*4        VECTOR(3)     ! Unit vector of position of interest
      Integer*2     RESOLUTION    ! Resolution of quad cube
      Integer*4     PIXEL         ! Pixel number (0 -> 6*((2**(res-1))**2)
C
C  Variables:
C
      Integer*4     FACE              ! Cube face number (0-5)
      Real*4        X,Y               ! Coordinates on cube face
      Integer*4     IX(128),IY(128)   ! Bit tables for calculating I,J
      Integer*4     I,J               ! Integer pixel coordinates
      Integer*4     IP,ID       
      Integer*4     FOUR              ! I*4 '4' - reuired to avoid integer
                                      !  overflow for large pixel numbers
      Integer*2     IL,IH,JL,JH       ! High and low 2-bytes of I & J
C
      Parameter  TWO14 = 2**14, TWO28 = 2**28
      Parameter  (FOUR = 4)
C
c      Data IX(128) /0/
c      if (ix(128) .eq. 0) call upx_BIT_TABLE_SET(ix,iy,128)
      if (ix(128) .eq. 0) call upx_BIT_TABLE_SET(ix,iy)
C
      call AXISXY(vector,face,x,y)
      i = two14 * x
      j = two14 * y
      i = min(two14-1,i)
      j = min(two14-1,j)
      il = mod(i,128)
      ih = i / 128
      jl = mod(j,128)
      jh = j / 128
      pixel = face * two28 + ix(il+1) + iy(jl+1) +
     +               two14 * (ix(ih+1) + iy(jh+1))
C
C     'Pixel' now contains the pixel number for a resolution of 15.  To
C     convert to the desired resolution, (integer) divide by 4 to the power
C     of the difference between 15 and the given resolution:
C
      pixel = pixel / four**(15-resolution)
C
      return
      end
      Subroutine upx_PIXEL_VECTOR(pixel,resolution,vector)
C
C    Routine to return unit vector pointing to center of pixel given pixel
C  number and resolution of the cube.  Based on exisiting _CENPIX routines
C  in CSDRLIB.
C
      Implicit NONE
C
C  Arguments:
C
      Integer*4      PIXEL         ! Pixel number
      Integer*2      RESOLUTION    ! Quad-cube resolution
      Real*4         VECTOR(3)     ! Unit vector to center of pixel
C
C  Variables:
C
      Integer*4      FACE             ! Face number
      Integer        JX,JY,IP,ID      ! Loop variables
      Real           X,Y              ! Face coordinates
      Real*4         SCALE              
      Integer*4      PIXELS_PER_FACE  ! Number of pixels on a single face
      Integer*4      FPIX             ! Pixel number within the face
      Real           XI,ETA           ! 'Database' coordinates
      Integer*4      ONE,TWO          ! Constants used to avoid integer
                                      !   overflow for large pixel numbers
C
      Parameter (ONE = 1)
      Parameter (TWO = 2)
C
      scale = 2**(resolution - 1) / 2.0
      pixels_per_face = two**(two*(resolution-one))
C
      face = pixel/pixels_per_face
      fpix = pixel - face*pixels_per_face
C 
C  Break pixel number down into x and y bits:
C
      jx = 0      
      jy = 0
      ip = 1
      do while (fpix .ne. 0)
          id = mod(fpix,2)
          fpix = fpix / 2
          jx = id * ip + jx
          id = mod(fpix,2)
          fpix = fpix / 2
          jy = id * ip + jy
          ip = 2 * ip
        end do
C
      x = (jx - scale + 0.5) / scale
      y = (jy - scale + 0.5) / scale
      call FORWARD_CUBE(x,y,xi,eta)
      call XYAXIS(face,xi,eta,vector)
C
      return
      end
	SUBROUTINE XYAXIS(NFACE,XI,ETA,C)
C
C	CONVERTS FACE NUMBER NFACE (0-5) AND XI, ETA (-1. - +1.)
C	INTO A UNIT VECTOR C
C
	REAL*4 C(3),XI,ETA, vmag
	GO TO (200,210,220,230,240,250), NFACE+1
200	  C(3)=1.
	  C(1)=-ETA				!Transform face to sphere
	  C(2)=XI
	  GO TO 300
210	  C(1)=1.
	  C(3)=ETA
	  C(2)=XI
	  GO TO 300
220	  C(2)=1.
	  C(3)=ETA
	  C(1)=-XI
	  GO TO 300
230	  C(1)=-1.
	  C(3)=ETA
	  C(2)=-XI
	  GO TO 300
240	  C(2)=-1.
	  C(3)=ETA
	  C(1)=XI
	  GO TO 300
250	  C(3)=-1.
	  C(1)=ETA
	  C(2)=XI
	  GO TO 300
C
300     vmag = sqrt( c(1)*c(1) + c(2)*c(2) + c(3)*c(3) )         !CALL NORM(C)
        c(1) = c(1) / vmag
        c(2) = c(2) / vmag
        c(3) = c(3) / vmag
	RETURN
	END

#include <math.h>

main(argc,argv)
int argc; char **argv;
{
  short ires=2, ores;
  int i, p;
  float vec[3];

  if( argc > 1 ) ires = atoi(argv[1]);
  ores = ires - 1;

  for( i = 0; i < 6 * pow(4.0,ires); i++ )
  {
    upx_pixel_vector(&i,&ires,vec);
    upx_pixel_number(vec,&ores,&p);
  }
}

#include "globe_mesh.h"
#include "request.h"

int other_tri_mesh(req)
REQ *req;
{
   int i, j;
   static short name=1;

   i = 0;
   switch( req->render_mode[0] )
   {
     case WIRE_FRAME: polymode(PYM_LINE); 
       break;

     case SOLID: polymode(PYM_FILL);
       break;
	
     case POINTS: polymode(PYM_POINT);
       break;

     default: polymode(PYM_LINE);

   }
   bgntmesh(); 
   for( i = 0; i < NY; i++ )
   {
     for( j = 0; j < NX; j++ )      
     {
       /*loadname(name++);*/ c3s( o_clist[i][j] ); v3f( o_vlist[i][j] );
       /*loadname(name++);*/ c3s( o_clist[i+1][j] ); v3f( o_vlist[i+1][j] );
     }
   }
   endtmesh(); 
}


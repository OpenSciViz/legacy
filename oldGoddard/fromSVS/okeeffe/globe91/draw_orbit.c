#include "globe_mesh.h"
#include "request.h"

int draw_orbit(win_id,lat,lon,req)
int win_id;
float lat, lon;
REQ *req;
{
  int i, sky_cell, orb_num;
  float julian_time;
  short res=9; /* 64*DMR resolution */
  static short color_orb[3] = { 255, 255, 128 };
  float uvec[3];
  static char ephem_info[80];
  
  uvec[2] = sin(lat/57.3);
  uvec[0] = cos(lat/57.3) * cos(lon/57.3);
  uvec[1] = cos(lat/57.3) * sin(lon/57.3);
  
/* fortran routine:
  upx_pixel_number_(uvec,&res,&sky_cell);
*/
/* C version: */
  upx_pixel_number(uvec,&res,&sky_cell);

  sky_cell = sky_cell / 64;
  if( sky_cell < 0 || sky_cell >= 6144 ) 
    printf("bad quadsphere number: %d\n",sky_cell);
  else
    sprintf(ephem_info,"Orbit Number= %d   . . .  Julian Time: %f",
		eph_qpix[sky_cell].orbit,eph_qpix[sky_cell].julian_time);

  ps_other_message(ephem_info);
/*  ps_other_message("Orbit Number= ?  ... Start & Stop Time: ? -- ?"); */

  bgnclosedline();
    for( i = 0; i < 256; i++ )
    {
        orblist[i][2] = 1.10 * sin(i*PI/128.0);
        orblist[i][0] = 1.10 * cos(i*PI/128.0) * cos(lon/57.3);
	orblist[i][1] = 1.10 * cos(i*PI/128.0) * sin(lon/57.3);
	c3s(color_orb);
	v3f(orblist[i]);
    }
  endclosedline();
  return;
}

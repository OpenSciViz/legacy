#include <gl.h>
#include <device.h>

process_hits(hits,buf)
int hits;
short buf[];
{
int index, h, i, items;

  printf("hit count: %d\n",hits);
  index = 0;

  for( h=0; h < hits; h++ )
  {
    items = buf[index++];
    printf("(");
    for( i=0; i<items; i++ )
    {
	if( i != 0 ) printf(" ");
	printf("%d", buf[index++]);
    }
    printf(")");
  }
  printf("\n");
  return;
}

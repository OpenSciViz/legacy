#define MY_ITEMS_TAG 99

cdef ps_icon_wind()

systemdict /Item known not { (NeWS/liteitem.ps) run } if

/notify? false def
/notify {
    notify? {(Notify: Value=%   Label=%) [ItemValue ItemLabel] /printf messages send} if
    MY_ITEMS_TAG tagprint
    ItemValue typedprint
    ItemLabel typedprint
} def
/FillColor .75 def

/drawarrow { % x y w h a b => -
gsave
10 dict begin
    /b exch def /a exch def	% x y w h
    1 5 1 roll insetrect	% x y w h [inset by one]
    4 -2 roll translate scale
    a 0 moveto
    a b lineto
    0 b lineto
    .5 1 lineto
    1 b lineto
    1 a sub b lineto
    1 a sub 0 lineto
    closepath
    gsave ItemFillColor setcolor fill grestore
    ItemBorderColor setcolor stroke
end
grestore
} def
/ArrowWidth 16 def
/ArrowHeight 16 def
/arrowobject { % bool => -
    {0 0 ArrowWidth ArrowHeight .333 .5 drawarrow}
    {ArrowWidth ArrowHeight} ifelse
} def

/RadioSize 16 def
/RadioDelta RadioSize 4 div def
/SquareRadioItem CycleItem []
classbegin
    /new { % label loc notify can width height
        [/squareoff /squareon] 6 1 roll % get stack right for CycleItem
        /new super send begin
	    /EraseToCycle false def
            currentdict
        end
    } def
classend def
/squareradio { % draw? off?  => -
gsave
    exch {
	ItemBorderColor setcolor
	.5 0 0 RadioSize RadioSize insetrect rectpath stroke
	RadioDelta .5 add 0 0 RadioSize RadioSize insetrect rectpath stroke
	RadioDelta 1 add 0 0 RadioSize RadioSize insetrect rectpath
	{ItemFillColor setcolor} if fill
    } {pop RadioSize RadioSize} ifelse
grestore
} def
/squareoff {true squareradio} def % draw? => -
/squareon {false squareradio} def % draw? => -

/circleradio { % draw? off?  => -
gsave
    exch {
	ItemBorderColor setcolor newpath
	.5 0 0 RadioSize RadioSize insetrect ovalpath stroke
	RadioDelta 1 index {.5 add}if 0 0 RadioSize RadioSize insetrect ovalpath
	{stroke} {fill} ifelse
    } {pop RadioSize RadioSize} ifelse
grestore
} def
/circleoff {true circleradio} def % draw? => -
/circleon {false circleradio} def % draw? => -

/checkitem {cycleitem /LabelY -4 def} def
/graybuttonitem {buttonitem /ItemBorderColor .5 .5 .5 rgbcolor def} def

/createitems {
/items 50 dict dup begin
    /check1 (Pick Mode) [/panel_check_off /panel_check_on]
        /Left /notify can 0 0 /new CycleItem send 
        dup /LabelY -4 put 300 300 /move 3 index send def
        
%    /check2 (Two Concentric Spheres) [/panel_check_off /panel_check_on]
%        /Left /notify can 0 0 /new CycleItem send 
%        dup /LabelY -4 put 300 280 /move 3 index send def
        
%    /check3 (Show Worldmap) [/panel_check_off /panel_check_on]
%        /Left /notify can 0 0 /new CycleItem send 
%        dup /LabelY -4 put 300 260 /move 3 index send def
        
%    /check4 (Activate Radial Distortion) [/panel_check_off /panel_check_on]
%        /Left /notify can 0 0 /new CycleItem send 
%        dup /LabelY -4 put 300 240 /move 3 index send def
               
%    /check4 (Elevation = Intensity) [/panel_check_off /panel_check_on]
%        /Left /notify can 0 0 /new CycleItem send 
%        dup /LabelY -4 put 300 240 /move 3 index send def
        
    /check5 (Show Orbit/Ephemeris) [/panel_check_off /panel_check_on]
        /Left /notify can 0 0 /new CycleItem send 
        dup /LabelY -4 put 300 220 /move 3 index send def
        
    /check6 (Show Footprint) [/panel_check_off /panel_check_on]
        /Left /notify can 0 0 /new CycleItem send 
        dup /LabelY -4 put 300 200 /move 3 index send def
        
%    /switchitem1 (View: Exterior, Between, Central) [/toggle1 /toggle2 /toggle3 /toggle2]
%        /Left /notify can 0 0 /new CycleItem send 2 280 /move 3 index send def
    
%    /switchitem1 (Globe: Wire-Frame, Solid, Points) [/toggle1 /toggle2 /toggle3 /toggle2]
%        /Left /notify can 0 0 /new CycleItem send 2 240 /move 3 index send def
        
    /switchitem2 (Data: Wire-Frame, Solid, Points) [/toggle1 /toggle2  /toggle3 /toggle2]
        /Left /notify can 0 0 /new CycleItem send 2 200 /move 3 index send def
        
    /switchitem3 (Zoom: 1x, 10x, 100x) [/toggle1 /toggle2  /toggle3 /toggle2]
        /Left /notify can 0 0 /new CycleItem send 2 160 /move 3 index send def
        
%    /eyeitem1 (X: Field of View) [/eye1 /eye2 /eye3 /eye4 /eye3 /eye2]
%        /Left /notify can 0 0 /new CycleItem send 3 160 /move 3 index send def
        
%   /eyeitem2 (Y: Field of View) [/eye1 /eye2 /eye3 /eye4 /eye3 /eye2]
%       /Left /notify can 0 0 /new CycleItem send 3 130 /move 3 index send def
    
    /bigsliderZOOM (ZOOM: ) [1 100 1] /Right /notify can 220 20
 	/new SliderItem send dup /ItemFrame 1 put
  	160 150 /move 3 index send def
        
    /bigsliderTWIST (TWIST: ) [-180 180 0] /Right /notify can 220 20
 	/new SliderItem send dup /ItemFrame 1 put
  	450 180 /move 3 index send def
        
    /bigsliderPAN_EW (PAN E-W: ) [-90 90 0] /Right /notify can 220 20
 	/new SliderItem send dup /ItemFrame 1 put
  	450 160 /move 3 index send def

    /bigsliderPAN_NS(PAN N-S: ) [-90 90 0] /Right /notify can 220 20
 	/new SliderItem send dup /ItemFrame 1 put
  	450 140 /move 3 index send def        
        
    /bigsliderD (DIST: ) [1 100 1] /Right /notify can 220 20
	/new SliderItem send dup /ItemFrame 1 put
  	2 120 /move 3 index send def

    /bigsliderE (ELEVATE: ) [0 100 0] /Right /notify can 220 20
	/new SliderItem send dup /ItemFrame 1 put
  	240 120 /move 3 index send def
                
                
%    /bigsliderA (ALTITUDE: ) [1 100 1] /Right /notify can 220 20
%	/new SliderItem send dup /ItemFrame 1 put
%  	2 100 /move 3 index send def
                
    /bigsliderX (X Axis Rot: ) [0 360 0] /Right /notify can 220 20
 	/new SliderItem send dup /ItemFrame 1 put
 	2 80 /move 3 index send def
    /bigsliderZ (Z Axis Rot: ) [0 360 0] /Right /notify can 220 20
   	/new SliderItem send dup /ItemFrame 1 put
 	2 60 /move 3 index send def
        
%    /nameitem (Text: ) () /Right /notify can 450 0
%  	/new TextItem send 3 32 /move 3 index send def
   
   /messages /panel_text (<latitude & longitude here>) /Right {} can 450 0
   /new MessageItem send dup begin
       /ItemFrame 1 def
       /ItemBorder 4 def
   end 2 40 /move 3 index send def

   /other_messages /panel_text (<ephemeris here>) /Right {} can 450 0
   /new MessageItem send dup begin
       /ItemFrame 1 def
       /ItemBorder 4 def
   end 2 20 /move 3 index send def        
        
   /data_messages /panel_text (<data value here>) /Right {} can 450 0
   /new MessageItem send dup begin
       /ItemFrame 1 def
       /ItemBorder 4 def
   end 2 0 /move 3 index send def        
        
%    /tableitem (Table) [
%        [(One) (Two) /panel_text]
%        [(Four) (yY|_) (Six)]
%    ] /Bottom /notify can 0 0 /new ArrayItem send 300 200 /move 3 index send def
    
end def

/messages items /messages get def
/other_messages items /other_messages get def
/data_messages items /data_messages get def

} def


/slideitem { % items fillcolor item => -
gsave
    dup 4 1 roll		% item items fillcolor item
    /moveinteractive exch send	% item
    /bbox exch send		% x y w h

%    (Item: x=%, y=%, w=%, h=% Canvas: w=%, h=%) [
%	6 2 roll
%	win begin FrameWidth FrameHeight end
%    ] /printf messages send
grestore
} def

/main {

% Create and size a window.  The size is chosen to accommodate the
% items we are creating.  Right before we map the window, we ask the
% user to reshape the window.  This is atypical, but gets the items
% positioned the way we want them.

    /win framebuffer /new DefaultWindow send def	% Create a window
    {	/PaintClient {FillColor fillcanvas items paintitems} def
	/FrameLabel (Geophysical Dataset Interface: Globe) def
	/IconImage /galaxy def
	/ClientMenu [
	    (White Background)	{/FillColor   1 store /paintclient win send}
	    (Light Background)	{/FillColor .75 store /paintclient win send}
	    (Medium Background)	{/FillColor .50 store /paintclient win send}
	    (Dark Background)	{/FillColor .25 store /paintclient win send}
	    (Black Background)	{/FillColor   0 store /paintclient win send}
	    (Flip Verbose)	{/notify? notify? not store}
	] /new DefaultMenu send def
    } win send						% Install my stuff.
    200 200 700 350 /reshape win send			% Shape it.
    /can win /ClientCanvas get def			% Get the window canvas
    
% Create all the items.
    createitems
    
% Create event manager to slide around the items.
% Create a bunch of interests to move the items.
% Note we actually create toe call-back proc to have the arguments we need.
% The proc looks like: {items color "thisitem" slideitem}.
% We could also have used the interest's clientdata dict.
    /slidemgr [
%	items { % key value
%	    MiddleMouseButton {slideitem}		% key item but proc
%	    1 dict dup DownTransition 5 index put	% key item but proc dict
%	    5 -2 roll exch pop				% but proc dict item
%	    /ItemCanvas get eventmgrinterest
%	} forall
	items { % key item
	    exch pop dup /ItemCanvas get	% item can
	    MiddleMouseButton [items FillColor	% item can name [ dict color
	    6 -1 roll /slideitem cvx] cvx	% can name proc
	    DownTransition 			% can name proc action
	    4 -1 roll eventmgrinterest		% interest
	} forall
    ] forkeventmgr def
    
% Now let the user specify the window's size and position.  Then map
% the window.  (See above)  Then activate the items.
%    /ptr /ptr_m framebuffer setstandardcursor

    /reshapefromuser win send	% Reshape from user.
    /map win send		% Map the window & install window event manager.
    				% (Damage causes PaintClient to be called)
    /itemmgr items forkitems def
} def

main

cdef ps_notify(string itemlabel,int itemval) => MY_ITEMS_TAG(itemval,itemlabel)

cdef ps_message(string A_message) A_message /printf messages send

cdef ps_other_message(string Another_message) Another_message /printf other_messages send

cdef ps_data_message(string Data_message) Another_message /printf data_messages send

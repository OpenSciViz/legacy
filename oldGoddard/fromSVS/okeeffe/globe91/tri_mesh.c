#include "globe_mesh.h"
#include "request.h"
#include <time.h>

clock_t tm;
int i, j;

int tri_mesh(req)
REQ *req;
{
  static short name=0; /* 1; for lighting mode */
/*
   struct sl3 {short s1; short s2; short s3;} *cp;
   struct fl3 {float f1; float f2; float f3;} *vp;
*/
  i = 0;
  if( strcmp(getenv("VGX_OPTION"),"YES") == 0 )
  {
   switch( req->render_mode[0] )
   {
     case WIRE_FRAME: polymode(PYM_FILL); 
       break;

     case SOLID: polymode(PYM_FILL);
       break;
	
     case POINTS: polymode(PYM_FILL);
       break;

     default: polymode(PYM_FILL);
   }
/*
   cp = &clist[0][0][0];
   vp = &vlist[0][0][0]; 
   bgntmesh(); 
   for( j = 0; j < NX*NY; j++, cp++, vp++ )      
   {

     loadname(name++); c3s( cp ); v3f( vp ); 
     loadname(name++); c3s( (cp+NX) ); v3f( (vp+NX) );
   }
   endtmesh();
*/
   if( name ) 
   {
      lmcolor(LMC_AD);
   }
   bgntmesh();
   for( j = 0; j < NY-1; j++ )
   {
     for( i = 0; i < NX; i++ )
     {
	
       c3s( clist[j][i] ); n3f( nlist[j][i] ); v3f( vlist[j][i] );
       c3s( clist[j+1][i] ); n3f( nlist[j+1][i] ); v3f( vlist[j+1][i] );
     }
   }
   endtmesh();
   if( name ) 
   {
      name = 0;
      lmcolor(LMC_COLOR);
   }
   return;
  }

  switch( req->render_mode[0] )
  {
     case WIRE_FRAME: solid(); 
       break;

     case SOLID: wire();
       break;
	
     case POINTS: points();
       break;

     default: break;
   }

/*
   tm = clock();
   tm = clock();
   printf("total time (in millisec.) = %d\n",tm);
*/
}

solid()
{ 
  bgntmesh();
    for( j = 0; j < NY-1; j++ )
    {
      for( i = 0; i < NX; i++ )
      {
        c3s( clist[j][i] ); v3f( vlist[j][i] );
        c3s( clist[j+1][i] ); v3f( vlist[j+1][i] );
      }  
    }
    endtmesh();
}

wire()
{
  int k=0; 
  bgnline();
    for( j = 0; j < NY-1; j++ )
    {
      for( i = 0; i < NX; i++ )
      {
        c3s( clist[j][i] ); v3f( vlist[j][i] );
        c3s( clist[j+1][i] ); v3f( vlist[j+1][i] );
        if( k >= 255 )
        {
	  k = 0;
	  endline();
	  bgnline();
        }
	else
	  k++;
      }  
    }
  endline();
}

points()
{
  int k=0;
  bgnpoint();
    for( j = 0; j < NY-1; j++ )
    {
       for( i = 0; i < NX; i++ )
       {
         c3s( clist[j][i] ); v3f( vlist[j][i] );
         c3s( clist[j+1][i] ); v3f( vlist[j+1][i] );
        if( k >= 255 )
        {
	  k = 0;
	  endpoint();
	  bgnpoint();
        }
	else
	  k++;
       }  
     }
   endpoint();
}

#include "globe_mesh.h"
#include "request.h"

int draw_all(glob_win,lat,lon,req,prev_req)
int glob_win;
float lat, lon;
REQ *req, *prev_req;
{
  zclear();
  cpack(0);
  clear();

  winset(glob_win);
  view_trnsfrm(req);
/*
  if( req->show_footprint && (req->show_footprint != prev_req->show_footprint) )
		 footprint_color(lat,lon);
*/
  if( req->show_footprint && (req->radial_distort != prev_req->radial_distort) )
		 radial_distort(req);

  if( !req->pick_mode && !prev_req->pick_mode &&
      strncmp(req->itemlabel,"ELEV",4) != 0)
    draw_globes(glob_win,req);

  if( req->show_footprint )
		 /* footprint_color(lat,lon);*/
    draw_patch(glob_win,lat,lon,req);

  if( req->show_orbit )
    draw_orbit(glob_win,lat,lon,req);

/*  swapbuffers(); */
}

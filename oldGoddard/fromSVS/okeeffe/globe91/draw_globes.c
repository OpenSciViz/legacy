#include "globe_mesh.h"
#include "request.h"

int draw_globes(win_id,req)
int win_id;
REQ *req;
{
  int i, fov;
 
  winset(win_id);

  name = 1; 

/*  if( req->render_mode[0] != SOLID ) depthcue(TRUE); */
            
/*  for( i = 0; i < NY-1; i++ )  globe is a polygonal mesh 
poly_mesh should load a name for each region of interst 
    poly_mesh(req,i); */

  tri_mesh(req);

  if( req->N_spheres > 1 )
  {
    name = 1;       
/*    for( i = 0; i < NY-1; i++ ) data is a polygonal mesh 
      other_poly_mesh(req,i); */
    other_tri_mesh(req);
  }

/*  if( req->render_mode[0] != SOLID ) depthcue(FALSE); */

  return;
}


#include "globe_mesh.h"
#include "request.h"

int poly_mesh(req,idx)
REQ *req;
int idx;
{
   int i, cnt;
   long nlod=32767;


/*   for(i=0; i< nlod; i++) loadname(1); */
   i = 0;
   switch( req->render_mode[0] )
   {
     case WIRE_FRAME: 
       while( i < NX-2 )
       {
         cnt = 0;
         bgnline();
         while( (cnt < 256) && (i < NX-2) )
         {
           c3s( clist[idx][i] ); v3f( vlist[idx][i] );
	   c3s( clist[idx+1][i] ); v3f( vlist[idx+1][i] );
	   c3s( clist[idx][i+1] ); v3f( vlist[idx][i+1] );
	   c3s( clist[idx+1][i+1] ); v3f( vlist[idx+1][i+1] );
	   cnt = cnt + 4;
	   i = i + 2;
         }
         endline();
       }
       break;

     case SOLID:
       while( i < NX-2 )
       {
	 bgnpolygon();
           c3s( clist[idx][i] ); v3f( vlist[idx][i] );
	   c3s( clist[idx+1][i] ); v3f( vlist[idx+1][i] );
	   c3s( clist[idx+1][i+1] ); v3f( vlist[idx+1][i+1] );
	   c3s( clist[idx][i+1] ); v3f( vlist[idx][i+1] );
	   cnt = cnt + 4;
	 endpolygon();
	 i++;
       } /* and connect the last element to the first */
	bgnpolygon();
           c3s( clist[idx][i] ); v3f( vlist[idx][i] );
	   c3s( clist[idx+1][i] ); v3f( vlist[idx+1][i] );
	   c3s( clist[idx+1][0] ); v3f( vlist[idx+1][0] );
	   c3s( clist[idx][0] ); v3f( vlist[idx][0] );
	   cnt = cnt + 4;
	 endpolygon();
         i++;
       break;
	
     case POINTS:
       while( i < NX-2 )
       {
         cnt = 0;
         bgnpoint();
         while( (cnt < 256) && (i < NX-2) )
         {
           c3s( clist[idx][i] ); v3f( vlist[idx][i] );
	   c3s( clist[idx+1][i] ); v3f( vlist[idx+1][i] );
	   c3s( clist[idx][i+1] ); v3f( vlist[idx][i+1] );
	   c3s( clist[idx+1][i+1] ); v3f( vlist[idx+1][i+1] );
	   cnt = cnt + 4;
	   i = i + 2;
         }
         endpoint();
       }
       break;

     default: break;

   }
   return;
}


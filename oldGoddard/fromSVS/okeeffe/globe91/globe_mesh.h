#include <stdio.h>
#include <gl.h>
#include <device.h>
#include <math.h>
#include <stdlib.h>

#ifdef MAIN
#define global
#else
#define global extern
#endif

#define NP 360*180
#define PI 3.1415926

typedef struct ephem { float sky_cell; float julian_time; float orbit; } EPHEM;
typedef struct ephem_qpix { float julian_time; int orbit; } EPHEM_QPIX;

global EPHEM_QPIX eph_qpix[6144];

global float orblist[256][3];
global float vlist[1024][2048][3];
global float nlist[1024][2048][3];
global short clist[1024][2048][3];

global float o_vlist[1024][2048][3];
global short o_clist[1024][2048][3];

global float patch_vlist[128][128][3];
global short patch_clist[128][128][3];

global int NX;
global int NY;
global short name;
global Object view_obj;

#include <math.h>
#include <gl.h>

int make_view(view_obj)
Object view_obj;
{
  static float ident[4][4] = { {1.0, 0.0, 0.0, 0.0}, 
                             {0.0, 1.0, 0.0, 0.0}, 
                             {0.0, 0.0, 1.0, 0.0}, 
                             {0.0, 0.0, 0.0, 1.0} };
  Coord dist = 5.0;
  Coord xt=0.0, yt=0.0, zt=0.0;
  Angle fov=450, x=0, z=0, azim=0, inc=900; 

  makeobj(view_obj);
   loadmatrix(ident);
   reshapeviewport();
   perspective(fov,1.0,0.001,100.0);
   polarview(dist,azim,inc,0);
   rotate(x,'x'); 
   rotate(z,'z');
   translate(xt,yt,zt);
  closeobj();

  callobj(view_obj);
  return;
}

setup()
{
  short wrred = 0xffff, wrgrn = 0xffff, wrblu = 0xffff;

  prefposition(640,1279,512,1023);
  winopen("test_mapw");

  RGBmode();
  RGBwritemask(wrred,wrgrn,wrblu);
  drawmode(NORMALDRAW);

  gconfig();
  backface(TRUE); 

  lsetdepth(0,0x7fffff);
  zbuffer(TRUE);
  lRGBrange(0x0040,0x0040,0x0040,0xffff,0xffff,0xffff,0,0x7fffff);
}

main()
{
  int i=0;
  Object view_obj;
  long xsiz, ysiz;
  Screencoord sx, sy;
  Coord wx1, wx2, wy1, wy2, wz1, wz2;
  float uvec[3], mag, theta, phi;

  view_obj = genobj();
  setup();
  getsize(&xsiz,&ysiz);

  sx = xsiz / 2;
  sy = ysiz / 2;

  while( i++ < 10 )
  {
    make_view(view_obj);
    mapw(view_obj,sx,sy,&wx1,&wy1,&wz1,&wx2,&wy2,&wz2);
    uvec[0] = wx1 - wx2;
    uvec[1] = wy1 - wy2;
    uvec[2] = wz1 - wz2;
    mag = sqrt( uvec[0]*uvec[0] + uvec[1]*uvec[1] + uvec[2]*uvec[2] );
    uvec[0] = uvec[0] / mag;
    uvec[1] = uvec[1] / mag;
    uvec[2] = uvec[2] / mag;
    theta = acos(uvec[2]) * 57.3;
    phi = atan2(uvec[1],uvec[0]) * 57.3;
  }
}

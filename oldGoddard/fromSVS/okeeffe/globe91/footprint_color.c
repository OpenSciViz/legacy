/* #define DEBUG */
#include "globe_mesh.h"

int footprint_color(lat,lon)
float lat, lon;
{
  int i, j;
  float dot, sat_dir[3];
  float v_lat, v_lon;

  sat_dir[2] = sin(lat/57.3);
  sat_dir[0] = cos(lat/57.3) * cos(lon/57.3);
  sat_dir[1] = cos(lat/57.3) * sin(lon/57.3);
/*
  for( i = 0; i < NX; i++ )
  {
    for( j = 0; j < NY; j++ )
    {
	clist[j][i][0] = o_clist[j][i][0];
	clist[j][i][1] = o_clist[j][i][1];
	clist[j][i][2] = o_clist[j][i][2];
	dot = sat_dir[0]*vlist[j][i][0] + sat_dir[1]*vlist[j][i][1] +
		sat_dir[2]*vlist[j][i][2];
        if( dot > 0.99 ) 
	{
	  clist[j][i][0] = 255;
	  clist[j][i][1] = 255;
	  clist[j][i][2] = 128;
	}
    }
  }
*/
  for( i = 0; i < NX; i++ )
  {
    for( j = 0; j < NY; j++ )
    {
	clist[j][i][0] = o_clist[j][i][0];
	clist[j][i][1] = o_clist[j][i][1];
	clist[j][i][2] = o_clist[j][i][2];
        v_lat = 57.3 * asin(vlist[j][i][2]);
        v_lon = 57.3 * atan2(vlist[j][i][1],vlist[j][i][0]);
#ifdef DEBUG
  if( lon < 0.0 && lon > -180.0 && lat > 45.0 ) 
	printf("footprint> lon= %f,  v_lon= %f\n",lon,v_lon);
#endif
        if( (fabs(v_lat - lat) <= 20.0) && (fabs(v_lon - lon) <= 8.0) )
	{
	  clist[j][i][0] = 2*clist[j][i][0];
	  clist[j][i][1] = 2*clist[j][i][1];
	  clist[j][i][2] = 2*clist[j][i][2];
	}
    }
  }
}

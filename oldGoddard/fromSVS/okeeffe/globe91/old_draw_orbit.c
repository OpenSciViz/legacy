#include "globe_mesh.h"

int draw_orbit(lat,lon)
float lat, lon;
{
  int i,j,k;
  static short color_orb[3] = { 255, 128, 128 };
  static float rotz[3][3] = { 1.0, 0.0, 0.0,
			0.0, 1.0, 0.0,
			0.0, 0.0, 1.0 };
  static Coord pick_orb[256][3];

/* if lat & lon != 0 must redefine orblist, assuming all orbits are polar,
   simply apply a rotation of lon about z to the default orbit, which
   should be in the lon=0 plane. */
  if(lon != 0.0)
  {
    rotz[0][0] = cos(lon/57.3);
    rotz[0][1] = -sin(lon/57.3);
    rotz[1][0] = -rotz[0][1];
    rotz[1][1] = rotz[0][0];
	  
    for( i=0; i<256; i++ )
    {
      pick_orb[i][0] = 0.0;
      pick_orb[i][1] = 0.0;
      pick_orb[i][2] = 0.0;
      for( k=0; k < 3; k++ )
	for( j=0; j < 3; j++ )
	  pick_orb[i][k] = pick_orb[i][k] + rotz[k][j] * orblist[i][j];
    }
  }
  else 
  {
    for( i=0; i<256; i++ )
      for( j=0; j < 3; j++ )
	pick_orb[i][j] = orblist[i][j];
  }

  bgnline();
    for( i = 0; i < 256; i++ )
    {
	c3s(color_orb);
	v3f(pick_orb[i]);
    }
  endline();

/* and draw two other orbits, each rotated 10 degrees */
  lon = lon + 10.0;
  rotz[0][0] = cos(lon/57.3);
  rotz[0][1] = -sin(lon/57.3);
  rotz[1][0] = -rotz[0][1];
  rotz[1][1] = rotz[0][0];
  for( i=0; i<256; i++ )
  {
    pick_orb[i][0] = 0.0;
    pick_orb[i][1] = 0.0;
    pick_orb[i][2] = 0.0;
    for( k=0; k < 3; k++ )
      for( j=0; j < 3; j++ )
	  pick_orb[i][k] = pick_orb[i][k] + rotz[k][j] * orblist[i][j];
  }

  bgnline();
    for( i = 0; i < 256; i++ )
    {
	c3s(color_orb);
	v3f(pick_orb[i]);
    }
  endline();

/* and draw two other orbits, each rotated 10 degrees */
  lon = lon - 20.0;
  rotz[0][0] = cos(lon/57.3);
  rotz[0][1] = -sin(lon/57.3);
  rotz[1][0] = -rotz[0][1];
  rotz[1][1] = rotz[0][0];
  for( i=0; i<256; i++ )
  {
    pick_orb[i][0] = 0.0;
    pick_orb[i][1] = 0.0;
    pick_orb[i][2] = 0.0;
    for( k=0; k < 3; k++ )
      for( j=0; j < 3; j++ )
	  pick_orb[i][k] = pick_orb[i][k] + rotz[k][j] * orblist[i][j];
  }

  bgnline();
    for( i = 0; i < 256; i++ )
    {
	c3s(color_orb);
	v3f(pick_orb[i]);
    }
  endline();

  return;
}

#include "globe_mesh.h"
#include "request.h"

/* 
#define DEBUG
*/

int pick_mode(win_id,req,prev_req,xsize,ysize,lat,lon)
int win_id;
REQ *req, *prev_req;
int xsize, ysize;
float *lat, *lon;
{
  short dev=0, val;  
  int i, j, cnt=0;
  float dist=0.0, distsqr=0.0, tmp=0.0;
  long xorig, yorig;
  float res=0.5, radius=1.0, lnear=0.0, mag=0.0;
  float wx1=0.0,wy1=0.0,wz1=0.0, wx2=0.0,wy2=0.0,wz2=0.0;
  float dot=0.0, uvec[3], bvec[3], strike[3];
  Screencoord sx=0, sy=0, old_sx=0, old_sy;
  short finished=0;
  char message[80];
  short button_down=1;

  winset(win_id);
  getorigin(&xorig,&yorig);
    
  while( !finished )
  {    
    dev = qread(&val);
    sx = getvaluator(MOUSEX);
    sy = getvaluator(MOUSEY);
    if( dev == LEFTMOUSE )
      if( (sx > xorig) && (sx < (xorig + xsize) )
          && (sy > yorig) && (sy < (yorig + ysize) ) && (button_down > 0) )
      {
	 finished = TRUE;
	 button_down = -1 * button_down;
      }
  }
  sx = sx - xorig;
  sy = sy - yorig;
  *lat = 0.0;
  *lon = 0.0;
  while( TRUE ) /* loop until rightmouse button is pressed */
  {
#ifdef DEBUG
printf("pick_mode: sx= %d, sy= %d\tnow calling mapw...\n",sx,sy);
#endif
    mapw(view_obj,sx,sy,&wx1,&wy1,&wz1,&wx2,&wy2,&wz2);
/* assuming the first point is the nearest, use it to define a unit vector
   pointing from the viewer into the world space and cast it thru the viewing
   volume to find the intersection, if any, with the globe */
    mag = sqrt( (wx2-wx1)*(wx2-wx1) + (wy2-wy1)*(wy2-wy1) + (wz2-wz1)*(wz2-wz1) );
    uvec[0] = (wx2-wx1) / mag;
    uvec[1] = (wy2-wy1) / mag;
    uvec[2] = (wz2-wz1) / mag;
#ifdef DEBUG
printf("pick_mode: return from mapw.\nw1= %f %f %f  w2= %f %f %f\n",
        wx1,wy1,wz1,wx2,wy2,wz2);
printf("pick_mode: uvec (w1-w2)= %f %f %f\n",uvec[0],uvec[1],uvec[2]);
#endif

/* find distance to ball center */
    distsqr = wx1*wx1 + wy1*wy1 + wz1*wz1;
    dist = sqrt( distsqr );
/*
  if( dist > radius ) ray is outside or on ball 
  {
*/
    bvec[0] = -wx1 / dist;
    bvec[1] = -wy1 / dist;
    bvec[2] = -wz1 / dist;
/* angle between ray & direction of ball center */
    dot = bvec[0]*uvec[0] + bvec[1]*uvec[1] + bvec[2]*uvec[2];
    if( dot > 0.0 )
    { 
      tmp = radius*radius - distsqr * (1.0 - dot*dot); 
      if( tmp > 0.0 )
      { /* if we get here, we have a hit */
        tmp = sqrt( tmp );
        lnear = dist * dot - tmp;
/*      lfar = dist * dot + tmp; */

        strike[0] = wx1 + lnear * uvec[0];
        strike[1] = wy1 + lnear * uvec[1];
        strike[2] = wz1 + lnear * uvec[2];
        *lat = 90.0 - 57.3 * acos(strike[2]);
        *lon = 57.3 * atan2(strike[1],strike[0]);
/*
	if( *lon > 0.0 )
	  *lon = *lon - 180.0;
        else      
	  *lon = *lon + 180.0;
*/
      }
    } 
/*   
  }
*/
#ifdef DEBUG
   printf("Selected point is: lat= %f  lon= %f\n\n",*lat,*lon);
#endif
    sprintf(message,"Selected point is:   lat= %f  lon= %f",*lat,*lon);
    ps_message(message);
    ps_flush_PostScript();

/*   
    draw_all(win_id,*lat,*lon,req,prev_req);
*/ 
    if( req->show_footprint ) draw_patch(win_id,*lat,*lon,req);
    if( req->show_orbit ) draw_orbit(win_id,*lat,*lon,req);

    old_sx = sx; old_sy = sy;
    if( qtest > 0 )
    { 
      if( qread(&val) == RIGHTMOUSE )
      {
	 req->pick_mode = FALSE;
	 return;
      }
      sx = getvaluator(MOUSEX);
      sy = getvaluator(MOUSEY);

      if( dev == LEFTMOUSE &&
          (sx > xorig) && (sx < (xorig + xsize) )
          && (sy > yorig) && (sy < (yorig + ysize) ) )
      {
	sx = sx - xorig;
	sy = sy - yorig;
      }
      else
      {
	sx = old_sx; sy = old_sy;
      }

    }
  }
}


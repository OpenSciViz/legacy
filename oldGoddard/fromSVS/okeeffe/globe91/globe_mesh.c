#define MAIN
#include "globe_mesh.h"
#include <fcntl.h>

main(argc,argv)
int argc; char **argv;
{
FILE *fp_pixmap, *fp_dirbe;
int fd;
float lat, lon;
int i, j, k, maxran, seed=1, cnt=0;
unsigned char data[2048];
float scale=8;
static 	EPHEM eph;
int graphics(); 

/* init the inner sphere texture */

   if( argc > 1 ) 
   {
	if( argv[1][0] == '-' && argv[1][1] == 's' )
	{
	  NY = 1024; NX = 2048;
/*          fp_pixmap = fopen("/disk4/xrhon/mss_globe.2048x1024","r"); */
          fp_pixmap = fopen("/usr/people/xrhon/pixmaps/mss_meanglobe.2048x1024","r");
	}
	else if( argv[1][0] == '-' && argv[1][1] == 'h' )
	{
	  NY = 512; NX = 1024;
          fp_pixmap = fopen("../pixmaps/globe.pixmap","r");
	}
	else if( argv[1][0] == '-' && argv[1][1] == 'm' )
	{
	  NY = 256; NX = 512;
          fp_pixmap = fopen("../pixmaps/reduce_globe.pixmap","r");
	}
	else if( argv[1][0] == '-' && argv[1][1] == 'l' )
	{
	  NY = 128; NX = 256;
/*          fp_pixmap = fopen("../pixmaps/mini_globe.pixmap","r");*/
          fp_pixmap = fopen("/disk4/xrhon/mss_globe.256x128","r");
	}
	else if( argv[1][0] == '-' && argv[1][1] == 't' )
	{
	  NY = 64; NX = 128;
          fp_pixmap = fopen("../pixmaps/tiny_globe.pixmap","r");
	}
	else
	{

	  NX = 512; NY = 256;	  fp_pixmap = fopen(argv[1],"r");
	  if( argc > 2 ) NX = atoi(argv[2]);
	  if( argc > 3 ) NY = atoi(argv[3]);
	}
   }
   else 
   {
     NY = 32; NX = 64;
     fp_pixmap = fopen("../pixmaps/micro_globe.pixmap","r");
   }

   if( argc > 4 ) scale = atoi(argv[4]);
   
   lat = PI / 2.0 - PI / NY;
   for( i = 0; i < NY; i++ )
   {
      lat = lat - (PI / NY);
      fread(data,1,NX,fp_pixmap);
      lon = -PI;
      for( j = 0; j < NX; j++ )
      {
	lon = lon + 2*PI / NX;
        nlist[i][j][2] = sin(lat);
	nlist[i][j][0] = cos(lat) * cos(lon);
	nlist[i][j][1] = cos(lat) * sin(lon);
        if( argv[1][0] == '-' && argv[1][1] == 'l' || argv[1][1] =='s' )
	{
	  scale = 0.9 + 0.1*(data[j] / 255.0);
          vlist[i][j][0] = scale * nlist[i][j][0];
          vlist[i][j][1] = scale * nlist[i][j][1];
          vlist[i][j][2] = scale * nlist[i][j][2];
          
	  if( data[j] >= 254 )
	  {
	    clist[i][j][0] = data[j];
	    clist[i][j][1] = 100;
	    clist[i][j][2] = 150;
	  }
	  else
	  {
	    clist[i][j][0] = 64 + data[j]/4;
	    clist[i][j][1] = 32 + data[j]/2;
	    clist[i][j][2] = data[j];
	  }
	}
	else
        {
	  scale = 1.0;
          vlist[i][j][0] = nlist[i][j][0];
          vlist[i][j][1] = nlist[i][j][1];
          vlist[i][j][2] = nlist[i][j][2];
 	  if( data[j] > 0 && data[j] < 255)
	  {
	    clist[i][j][0] = scale * data[j];
	    clist[i][j][1] = 64;
	    clist[i][j][2] = 64;
	  }
	  else if( data[j] == 255 )
	  {
	    clist[i][j][0] = data[j];
	    clist[i][j][1] = data[j];
	    clist[i][j][2] = data[j];
	  }
	  else
	  {
	    clist[i][j][0] = 32;
	    clist[i][j][1] = 64;
	    clist[i][j][2] = 255;
	  }
	}
        o_clist[i][j][0] = clist[i][j][0];
        o_clist[i][j][1] = clist[i][j][1];
        o_clist[i][j][2] = clist[i][j][2];
	if( (int ) data[j] != 0 ) cnt++;
      }
   }
   fclose(fp_pixmap);
   for( i = 0; i < NY-1; i++ )
     for( j = 0; j < NX-1; j++ )
       vert_norm(nlist[i][j],vlist[i][j],vlist[i][j+1],vlist[i+1][j]);
   
   fp_pixmap = fopen("hugo_128.img","r");
/*   lat = 23.0/57.3; */
   lat = 0.0; 
   for( i = 0; i < 128; i++ )
   {
      lat = lat + (12.0/57.3 / 128.0);
      fread(data,1,128,fp_pixmap);
/*      lon = 98.0/57.3; */
      lon = 0.0;
      for( j = 0; j < 128; j++ )
      {
	  lon = lon + (12.0/57.3 / 128.0); 
          patch_vlist[127-i][j][2] = 1.05*sin(lat);
	  patch_vlist[127-i][j][0] = 1.05*cos(lat) * cos(lon);
	  patch_vlist[127-i][j][1] = 1.05*cos(lat) * sin(lon);
	  patch_clist[127-i][j][0] = data[j];
	  patch_clist[127-i][j][1] = data[j];
	  patch_clist[127-i][j][2] = data[j];

     }
   }

/* init the ephemeris info, array is sorted in DMR res. quadsphere cells */
  for( i = 0; i < 6144; i++ )
  {
    eph_qpix[i].julian_time = 0.0;
    eph_qpix[i].orbit = 0;
  }

  fd = open("ephem_qpix.dat",O_RDONLY);

  if( fd < 0 ) 
    printf("Unable to open ephem_qpix.dat\n");
  else
  {
    cnt = 0;
    while( read(fd,&eph,sizeof(EPHEM)) > 0 )
    {
      k = eph.sky_cell / 64;
      eph_qpix[k].julian_time = eph.julian_time;
      eph_qpix[k].orbit = eph.orbit;
/*    printf("qpix= %f\t time= %f\t orbit= %f\n",eph.qpix,eph.time,eph.orbit);*/
      cnt++;
    }
  }
  printf("Read cnt= %d \t ephemeris (generic) records.\n",cnt);
  close(fd);

  graphics();
}

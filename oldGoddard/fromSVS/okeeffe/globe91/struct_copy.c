int struct_copy(n,src,dst)
int n;
char *src, *dst;
{
  while( n-- > 0 ) *dst++ = *src++;
 
  return;
}

#include "globe_mesh.h"
#include "request.h"

int other_poly_mesh(req,idx)
REQ *req;
int idx;
{
   int i, cnt;
   static short name=1;

   i = 0;
   switch( req->render_mode[0] )
   {
     case WIRE_FRAME: 
       while( i < NX-2 )
       {
         cnt = 0;
         bgnline();
         while( (cnt < 256) && (i < NX-2) )
         {
           loadname(name++); c3s( o_clist[idx][i] ); v3f( o_vlist[idx][i] );
	   loadname(name++); c3s( o_clist[idx+1][i] ); v3f( o_vlist[idx+1][i] );
	   loadname(name++); c3s( o_clist[idx][i+1] ); v3f( o_vlist[idx][i+1] );
	   loadname(name++); c3s( o_clist[idx+1][i+1] ); v3f( o_vlist[idx+1][i+1] );
	   cnt = cnt + 4;
	   i = i + 2;
         }
         endline();
       }
       break;

     case SOLID:
       while( i < NX-2 )
       {
	 bgnpolygon();
           loadname(name++); c3s( o_clist[idx][i] ); v3f( o_vlist[idx][i] );
	   loadname(name++); c3s( o_clist[idx+1][i] ); v3f( o_vlist[idx+1][i] );
	   loadname(name++); c3s( o_clist[idx+1][i+1] ); v3f( o_vlist[idx+1][i+1] );
	   loadname(name++); c3s( o_clist[idx][i+1] ); v3f( o_vlist[idx][i+1] );
	   cnt = cnt + 4;
	 endpolygon();
	 i++;
       } /* and connect the last element to the first */
	bgnpolygon();
           loadname(name++); c3s( o_clist[idx][i] ); v3f( o_vlist[idx][i] );
	   loadname(name++); c3s( o_clist[idx+1][i] ); v3f( o_vlist[idx+1][i] );
	   loadname(name++); c3s( o_clist[idx+1][0] ); v3f( o_vlist[idx+1][0] );
	   loadname(name++); c3s( o_clist[idx][0] ); v3f( o_vlist[idx][0] );
	   cnt = cnt + 4;
	 endpolygon();
	 i++;
       break;
	
     case POINTS:
       while( i < NX-2 )
       {
         cnt = 0;
         bgnpoint();
         while( (cnt < 256) && (i < NX-2) )
         {
           loadname(name++); c3s( o_clist[idx][i] ); v3f( o_vlist[idx][i] );
	   loadname(name++); c3s( o_clist[idx+1][i] ); v3f( o_vlist[idx+1][i] );
	   loadname(name++); c3s( o_clist[idx][i+1] ); v3f( o_vlist[idx][i+1] );
	   loadname(name++); c3s( o_clist[idx+1][i+1] ); v3f( o_vlist[idx+1][i+1] );
	   cnt = cnt + 4;
	   i = i + 2;
         }
         endpoint();
       }
       break;

     default: break;

   }
   return;
}


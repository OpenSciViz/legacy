#include "globe_mesh.h"
#include "request.h"

#define DEBUG
short wrred = 0xffff, wrgrn = 0xffff, wrblu = 0xffff;
static float mat[] = { AMBIENT, .2, .2, .2,
	      DIFFUSE, 0, .369, .165,
	      SPECULAR, .5, .5, .5,
	      SHININESS, 64,
	      LMNULL
	     };
static float lm[] = { AMBIENT, .2, .2, .2,
		      LOCALVIEWER, 0,
		      LMNULL
		    };
static float lttop[] = { LCOLOR, 1, 1, 1,
		      POSITION, 0, 1.0, 0, 0,
		      LMNULL
		    };
static float ltbot[] = { LCOLOR, 1, 1, 1,
		      POSITION, 0, -1.0, 0, 0,
		      LMNULL
		    };

int graphics()
{
 int i, cnt, glob_win;
 int pick_mode(), footprint_color(),
     draw_orbit(), radial_distort(), struct_copy();
 int x0 = 616, y0 = 8;
 int xsize, ysize;
 static float lat, lon;
 static REQ req, prev_req;
 static char title[]= "Global Perspective\n";

   cnt = sizeof( REQ );
   req.zoom_fctr = 10;
   req.zoom_val = 1;
   req.twist = 0;

/* create the icon interface window */
  if( ps_open_PostScript() == 0 )
  {
	printf("Unable to connect to NeWS server\n");
	exit(1);
  }
  ps_icon_wind();
  ps_flush_PostScript();

/* create the 3-D graphics window 
   prefposition(x0,x0+511,y0,y0+511);
   foreground();
   keepaspect(1,1);
*/

   glob_win = winopen(title);

   RGBmode();
   RGBwritemask(wrred,wrgrn,wrblu);
   drawmode(NORMALDRAW);

  
/*   doublebuffer(); */
   gconfig();

   qdevice(LEFTMOUSE);
   qdevice(RIGHTMOUSE);
   qdevice(MIDDLEMOUSE);
/*
   qdevice(ZKEY);
   qdevice(CKEY);
   qdevice(RKEY);
   qdevice(PKEY);
*/   
   getsize(&xsize,&ysize);
/*
   mainmenu = newpup("Globe Information Retriever %t|");
   addtopup(mainmenu,"General %f|",general_info)
   initnames();  note that loadname((short) id) allows for 32k objects 
*/
   backface(TRUE); 

   lsetdepth(0,0x7fffff);
   zbuffer(TRUE);
   lRGBrange(0x0040,0x0040,0x0040,0xffff,0xffff,0xffff,0,0x7fffff);
/*
   mmode(MVIEWING);
   lmdef(DEFMATERIAL,1,0,mat);
   lmdef(DEFLMODEL,1,0,lm);
   lmdef(DEFLIGHT,1,0,lttop);
   lmdef(DEFLIGHT,2,0,ltbot);
   lmbind(MATERIAL,1);
   lmbind(LMODEL,1);
   lmbind(LIGHT0,1);
   lmbind(LIGHT1,2);
*/

   while( TRUE )
   {
     depthcue(FALSE);
     draw_all(glob_win,lat,lon,&req,&prev_req);

     struct_copy(cnt,&req,&prev_req); 
     
     get_request(&req);

     if( req.pick_mode )
		 pick_mode(glob_win,&req,&prev_req,xsize,ysize,&lat,&lon);

   }
}


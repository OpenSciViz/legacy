#include "globe_mesh.h"
#include "request.h"

int radial_distort(req)
REQ *req;
{
  int i, j;
  static float scale=1.0;

  if( req->N_spheres > 1)
  {
    for( i = 0; i < NX; i++ )
    {
      for( j = 0; j < NY; i++ )
      {
	scale = o_clist[j][i][0] + o_clist[j][i][1] + o_clist[j][i][2];
	if( !req->radial_distort ) scale = 1.0 / scale;
	o_vlist[j][i][0] = scale * o_vlist[j][i][0];
	o_vlist[j][i][1] = scale * o_vlist[j][i][1];
	o_vlist[j][i][2] = scale * o_vlist[j][i][2];
      }
    }
  }
  else 
  {
    for( i = 0; i < NX; i++ )
    {
      for( j = 0; j < NY; i++ )
      {
	scale = clist[j][i][0] + clist[j][i][1] + clist[j][i][2];
	if( !req->radial_distort ) scale = 1.0 / scale;
	vlist[j][i][0] = scale * vlist[j][i][0];
	vlist[j][i][1] = scale * vlist[j][i][1];
	vlist[j][i][2] = scale * vlist[j][i][2];
      }
    }
  }
  return;
}

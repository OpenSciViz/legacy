#include "globe_mesh.h"
#include "request.h"

int view_trnsfrm(req)
REQ *req;
{
  int i, fov=450;
  static float ident[4][4] = { {1.0, 0.0, 0.0, 0.0}, 
                             {0.0, 1.0, 0.0, 0.0}, 
                             {0.0, 0.0, 1.0, 0.0}, 
                             {0.0, 0.0, 0.0, 1.0} };
  float dist; /*  = 5.0;  delt = 0.1; */
  static float x0=0.0, y0=0.0, z0=0.0;
  
  dist = 5.0 - 0.04*req->dist;   
  z0 = 1.0/90.0 * req->pan_ns;
  x0 = 1.0/90.0 * req->pan_ew; 

  fov = 600 / log(1.0 + req->zoom_val * req->zoom_fctr);
/*  if( req->zoom_fctr == 10 ) 
    dist = 2.5;

  else if( req->zoom_fctr == 100 ) 
    dist = 1.5;
  else
    dist = 4.0;
*/

  makeobj(view_obj);

  loadmatrix(ident);
  reshapeviewport();
  perspective(fov,1.0,0.001,100.0);
/*
  if( req->twist == 0.0 )
  {
    if( x0 > 0.0 ) req->twist = 900;
    if( x0 < 0.0 ) req->twist = -900;
    if( z0 > 0.0 ) req->twist = -1800;
  }
*/
  polarview(dist,0,900,req->twist); 
/*
  lookat(0.0,dist,0.0,x0,y0,z0,req->twist);
  window(-1.0,1.0,-1.0,1.0,0.01,100.0);
*/
  translate(x0,0.0,z0);

  if( req->rot_x > 3600 ) req->rot_x = 0;
  rotate(req->rot_x,'x'); 

  if( req->rot_z > 3600 ) req->rot_z = 0;
  rotate(req->rot_z,'z'); 

  closeobj();

  callobj(view_obj);
  return;
}


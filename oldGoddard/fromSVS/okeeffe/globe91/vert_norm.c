#include <math.h>

vert_norm(norm,v0,vl,vr)
float norm[3], v0[3], vl[3], vr[3];
{
  float mag;

  norm[0] = (vr[1] - v0[1]) * (vl[2] - v0[2]) -
		(vr[2] - v0[2]) * (vl[1] - v0[1]);
 
  norm[1] = (vr[2] - v0[2]) * (vl[0] - v0[0]) - 
		(vr[0] - v0[0]) * (vl[2] - vl[2]);

  norm[2] = (vr[0] - v0[0]) * (vl[2] - v0[2]) -
		(vr[2] - v0[2]) * (vl[0] - v0[0]);

  mag = sqrt(norm[0]*norm[0] + norm[1]*norm[1] + norm[2]*norm[2]);

  norm[0] = norm[0]/mag;
  norm[1] = norm[1]/mag;
  norm[2] = norm[2]/mag;
}

#include "globe_mesh.h"
#include "request.h"

int draw_patch(win_id,lat,lon,req)
int win_id;
float lat, lon;
REQ *req;
{
  int i, j, idx, cnt=1;
  short avg=69;
  float ylat, xlon;
  static float prev_lat=0.0, prev_lon=90.0;

  winset(win_id);
  if( lat != prev_lat || lon != prev_lon || req->elevation)
  {
    prev_lat = lat;
    prev_lon = lon;
    for( i = 0; i <= 128; i = i + cnt )
    {
      ylat = lat/57.3 + i*(12.0/57.3 / 128.0);
      for( j = 0; j < 128; j = j + cnt )
      {
	  xlon = lon/57.3 + j*(12.0/57.3 / 128.0); 
          patch_vlist[i-1][j][2] = (1.02 + req->elevation/102400.0 * 
					   patch_clist[i-1][j][0]) *
					   sin(ylat);
	  patch_vlist[i-1][j][0] = (1.02 + req->elevation/102400.0 * 
					   patch_clist[i-1][j][0]) *
					   cos(ylat) * cos(xlon);
	  patch_vlist[i-1][j][1] = (1.02 + req->elevation/102400.0 * 
					   patch_clist[i-1][j][0]) *
					   cos(ylat) * sin(xlon);
      }
    }
  }

  switch( req->render_mode[1] )
  {
    case WIRE_FRAME: 
      for( i = 0; i < 128-cnt; i = i + cnt )
        for( j = 0; j < 128-cnt; j = j + cnt )
        {
          bgnline();
            c3s( patch_clist[i][j] ); v3f( patch_vlist[i][j] );
	    c3s( patch_clist[i][j+cnt] ); v3f( patch_vlist[i][j+cnt] );
            c3s( patch_clist[i+cnt][j+cnt] ); v3f( patch_vlist[i+cnt][j+cnt] );
            c3s( patch_clist[i+cnt][j] ); v3f( patch_vlist[i+cnt][j] );
          endline();
        } 
      break;

    case SOLID: 
      for( i = 0; i < 128-cnt; i = i + cnt )
        for( j = 0; j < 128-cnt; j = j + cnt )
        {
          bgnpolygon();
            c3s( patch_clist[i][j] ); v3f( patch_vlist[i][j] );
            c3s( patch_clist[i][j+cnt] ); v3f( patch_vlist[i][j+cnt] );
            c3s( patch_clist[i+cnt][j+cnt] ); v3f( patch_vlist[i+cnt][j+cnt] );
            c3s( patch_clist[i+cnt][j] ); v3f( patch_vlist[i+cnt][j] );
           endpolygon();
        } 
      break;
   
    case POINTS: 
      for( i = 0; i < 128-cnt; i = i + cnt )
        for( j = 0; j < 128-cnt; j = j + cnt )
        {
          bgnpoint();
            c3s( patch_clist[i][j] ); v3f( patch_vlist[i][j] );
            c3s( patch_clist[i+cnt][j] ); v3f( patch_vlist[i+cnt][j] );
            c3s( patch_clist[i+cnt][j+cnt] ); v3f( patch_vlist[i+cnt][j+cnt] );
	    c3s( patch_clist[i][j+cnt] ); v3f( patch_vlist[i][j+cnt] );
          endpoint();
        } 
      break;

    default: break;
  }
  return;
}

// string conversion program

#include <stdio.h>
#include <string.h>

// a static string class
class string
{
  char data[80];

  public:
    void copy(char *s) { strcpy(data,s); }
    string(char *s="") { copy(s); }
    void read(FILE *fptr);
    void write(FILE *fptr);
    void operator+=(string &s) { strcat(data, s.data); }
    friend void convert(string &s, int opt);
};

void string::read(FILE *fptr)
{
  int slen;
  fgets(data,80,fptr);
  slen = strlen(data) - 1;
  if( data[slen] == '\n' ) 
    data[slen] = 0; // remove '\n'

  return;
}

void string::write(FILE *fptr)
{
  fprintf(fptr,"%s\n",data);
}

// Friend function of string class

void convert(string &s, int opt)
{
  string tempstr; // create a string object (rather recursive!--dh)

  switch( opt )
  {
    case '1': strupr(s.data);
              break;
    case '2': stwrlwr(s.data);
              break;
    case '3': printf("Enter a new string\n");
	      tempstr.read(stdin);
              s += tempstr;  // call overloaded operator
	      break;
    default: printf("Sorry, bad option\n");
  }
}

void main()
{
  string str;  // create a string object
  char opt;

  printf("Hello friend, enter a string please\n");
  str.read(stdin);
  printf("Enter a conversion option\n");
  printf("1. Convert to upper case\n");
  printf("2. Convert to lower case\n");
  printf("3. Append to the string\n");

  opt = getch();

  convert(str, opt); 
  printf("The converted string is: \n");
  strwrite(stdout);
  
}
                                                                                                                

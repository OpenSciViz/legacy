; IDL Version 2.1.0 (IRIX mipseb)
; Journal File for xrhon@okeeffe
; Working directory: /usr/people/xrhon/crust
; Date: Sun Nov 24 13:48:59 1991
 
openr,1,'globe.sa-af
a=assoc(1,bytarr(1024,512))
globe=a(0)
lat=360.0/1024.0*indgen(1024)
lon(0:255)=90.0/256.0*(255-indgen(256))
lon(256:511)= -90.0/256.0*(indgen(256))
lat_lon=fltarr(1024,512)
for i=0,511 do lat_lon(0:1023,i)=lat
for i=0,1023 do lat_lon(i,0:511)=lon
ws = where( globe eq 1 )
wa = where( globe eq 2 )
help
africa=fltarr(20757)
sam=fltarr(13079)
africa=fltarr(20757,3)
sam=fltarr(13079,3)
lat_lon=fltarr(1024,512,2)
for i=0,511 do lat_lon(0:1023,i,0)=lat
for i=0,1023 do lat_lon(i,0:511,1)=lon

; IDL Version 2.1.0 (IRIX mipseb)
; Journal File for xrhon@okeeffe
; Working directory: /usr/people/xrhon/crust
; Date: Sun Nov 24 13:48:59 1991
 
openr,1,'globe.sa-af
a=assoc(1,bytarr(1024,512))
globe=a(0)
close,1
lon=fltarr(1024)
lon = -180.0 + 360.0/1025.0*(1 + indgen(1024))
lon = lon/57.3
lat=fltarr(512)
lat = 90.0 - 180.0/513.0*(1 + indgen(512))
lat = lat/57.3
latgrid=fltarr(1024,512)
longrid=fltarr(1024,512)
for i=0,511 do longrid(0:1023,i)=lon
for i=0,1023 do latgrid(i,0:511)=lat
ws = where( globe eq 1 )
wa = where( globe eq 2 )
africa=fltarr(3,20734)
stham=fltarr(3,13079)
;africa(2,*)=sin(latgrid(wa))
;africa(0,*)=cos(latgrid(wa))*cos(longrid(wa))
;africa(1,*)=cos(latgrid(wa))*sin(longrid(wa))
for i=0L,20733 do africa(2,i)=sin(latgrid(wa(i)))
for i=0L,20733 do africa(0,i)=cos(latgrid(wa(i)))*cos(longrid(wa(i)))
for i=0L,20733 do africa(1,i)=cos(latgrid(wa(i)))*sin(longrid(wa(i)))
;stham(2,*)=sin(latgrid(ws))
;stham(0,*)=cos(latgrid(ws))*cos(longrid(ws))
;stham(1,*)=cos(latgrid(ws(i)))*sin(longrid(ws))
for i=0L,13078 do stham(2,i)=sin(latgrid(ws(i)))
for i=0L,13078 do stham(0,i)=cos(latgrid(ws(i)))*cos(longrid(ws(i)))
for i=0L,13078 do stham(1,i)=cos(latgrid(ws(i)))*sin(longrid(ws(i)))
openw,1,'africa.verts'
a=assoc(1,fltarr(3,20757))
a(0)=africa
close,1
openw,1,'south_america.verts'
a=assoc(1,fltarr(3,13079))
a(0)=stham
close,1


#include <stdio.h>
#include <gl.h>
#include <device.h>
#include <math.h>
/* #define FALSE 0 */
#define DEBUG

#define NAF 716
#define NSA 1144
#define NROTS 31

static float ident[4][4] = { {1.0, 0.0, 0.0, 0.0}, 
			     {0.0, 1.0, 0.0, 0.0}, 
			     {0.0, 0.0, 1.0, 0.0}, 
			     {0.0, 0.0, 0.0, 1.0} };
short wrred = 0xffff, wrgrn = 0xffff, wrblu = 0xffff;
float dist = 5.0, delt = 0.1;
float q_AF[NROTS*5][4];
float q_SA[NROTS*5][4];

init_coast(float a[NAF][2], float s[NAF][2])
{
  FILE *fp;
  static char tmp[81];
  int i;

  fp = fopen("Africa.coast","r");
  fgets(tmp,80,fp);
  fgets(tmp,80,fp); /* just discard the first two lines */

  for( i = 0; i < NAF; i++ )
  {
    fgets(tmp,80,fp);
    sscanf(tmp,"%f %f",&a[i][0],&a[i][1]);
  }
  fclose(fp);

  fp = fopen("SA.coast","r");
  fgets(tmp,80,fp);
  fgets(tmp,80,fp); /* just discard the first two lines */

  for( i = 0; i < NSA; i++ )
  {
    fgets(tmp,80,fp);
    sscanf(tmp,"%f %f",&s[i][0],&s[i][1]);
  }

  fclose(fp);
}

interp_quats(int n, int n_inc)
{
 int i=1;
 float *q0_AF, *q1_AF, *q0_SA, *q1_SA;

  q0_AF = &q_AF[0][0];
  q1_AF = &q_AF[n_inc][0];
  q0_SA = &q_SA[0][0];
  q1_SA = &q_SA[n_inc][0];
  while( i < n*n_inc )
  {
    q_AF[i][0] = q0_AF[0] + 1.0/n_inc * (i % n_inc) * (q1_AF[0] - q0_AF[0]);
    q_AF[i][1] = q0_AF[1] + 1.0/n_inc * (i % n_inc) * (q1_AF[1] - q0_AF[1]);
    q_AF[i][2] = q0_AF[2] + 1.0/n_inc * (i % n_inc) * (q1_AF[2] - q0_AF[2]);
    q_AF[i][3] = q0_AF[3] + 1.0/n_inc * (i % n_inc) * (q1_AF[3] - q0_AF[3]);

    q_SA[i][0] = q0_SA[0] + 1.0/n_inc * (i % n_inc) * (q1_SA[0] - q0_SA[0]);
    q_SA[i][1] = q0_SA[1] + 1.0/n_inc * (i % n_inc) * (q1_SA[1] - q0_SA[1]);
    q_SA[i][2] = q0_SA[2] + 1.0/n_inc * (i % n_inc) * (q1_SA[2] - q0_SA[2]);
    q_SA[i][3] = q0_SA[3] + 1.0/n_inc * (i % n_inc) * (q1_SA[3] - q0_SA[3]);

    if( !(++i % n_inc) )
    {
      q0_AF = q1_AF;
      q0_SA = q1_SA;
      q1_AF = &q_AF[i + n_inc][0];
      q1_SA = &q_SA[i + n_inc][0];
      i++;
    }
  }
  i = creat("quats_Af.151",0644);
  write(i,q_AF,4*4*(NROTS*5-1));
  close( i );
  i = creat("quats_SA.151",0644);
  write(i,q_SA,4*4*(NROTS*5-1));
  close( i );
}
      
init_quaternions()
{
  float axis[3], q_prev[4];
  float age, lat, lon, ang, ang_inc;
  int i, k=0;
  FILE *fp;
  static char tmp[81];
    
/* what to do about a change in axis of rotation? quat_mult(q_prev,q_this) ??? */

/* Africa: */

  fp = fopen("/usr/local/data/africa.rot","r");
  fgets(tmp,80,fp);
  fgets(tmp,80,fp); /* just discard the first two lines */

  for( i = 0; i < NROTS; i++ )
  {
    fgets(tmp,80,fp);
    sscanf(tmp,"%f %f %f %f",&age,&lat,&lon,&ang);
    ang = ang / 57.3;
    lat = lat / 57.3;
    lon = lon / 57.3;
    axis[0] = -cos(lat)*cos(lon);
    axis[1] = -cos(lat)*sin(lon);
    axis[2] = -sin(lat);   
    q_AF[5*i][3] = cos(ang/2.0);
    q_AF[5*i][0] = axis[0]*sin(ang/2.0);
    q_AF[5*i][1] = axis[1]*sin(ang/2.0);
    q_AF[5*i][2] = axis[2]*sin(ang/2.0);
  }
  fclose(fp);

/* South America */

  fp = fopen("/usr/local/data/SA.rot","r");
  fgets(tmp,80,fp);
  fgets(tmp,80,fp); /* just discard the first two lines */
  for( i = 0; i < NROTS; i++ )
  {
    fgets(tmp,80,fp);
    sscanf(tmp,"%f %f %f %f",&age,&lat,&lon,&ang);
    ang = ang / 57.3;
    lat = lat / 57.3;
    lon = lon / 57.3;
    axis[0] = -cos(lat)*cos(lon);
    axis[1] = -cos(lat)*sin(lon);
    axis[2] = -sin(lat);   
    q_SA[5*i][3] = cos(ang/2.0);
    q_SA[5*i][0] = axis[0]*sin(ang/2.0);
    q_SA[5*i][1] = axis[1]*sin(ang/2.0);
    q_SA[5*i][2] = axis[2]*sin(ang/2.0);
  }
  fclose(fp);

/* interpolate between every 5 */
  interp_quats(NROTS-1,5);
}


draw_coast_AF(int n, float *rot_mat, float lat_lon[NAF][2])
{
  static float vert[3], color[3]={1.0,1.0,1.0};
  float *vec, *rotate_vec();

  c3f(color);
  bgnline();
  while( --n >= 0 )
  {
    vert[2] = sin(lat_lon[n][0]/57.3);
    vert[0] = cos(lat_lon[n][0]/57.3)*cos(lat_lon[n][1]/57.3);
    vert[1] = cos(lat_lon[n][0]/57.3)*sin(lat_lon[n][1]/57.3);
    vec = rotate_vec(rot_mat,vert);
    v3f(vec); 
  }
  endline();
}

draw_coast_SA(int n, float *rot_mat, float lat_lon[NSA][2])
{
  static float vert[3], color[3]={1.0,1.0,1.0};
  float *vec, *rotate_vec();

  c3f(color);
  bgnline();
  while( --n >= 0 )
  {
    vert[2] = sin(lat_lon[n][0]/57.3);
    vert[0] = cos(lat_lon[n][0]/57.3)*cos(lat_lon[n][1]/57.3);
    vert[1] = cos(lat_lon[n][0]/57.3)*sin(lat_lon[n][1]/57.3);
    vec = rotate_vec(rot_mat,vert);
    v3f(vec);
  }
  endline();
}

record_it()
{
  static cnt=0;
  static char file[81], cmd[81];
  
  if( cnt < 10 )
    sprintf(file,"frame_00""%d"".rgb",cnt);
  else if( cnt < 100 )
    sprintf(file,"frame_0""%d"".rgb",cnt);
  else if( cnt < 1000 )
    sprintf(file,"frame_""%d"".rgb",cnt);

  sprintf(cmd,"scrsave %s 0 645 0 485",file);
  system(cmd);
  cnt++;
}

main(int argc, char **argv, char **env)
{
 float Africa[NAF][2], SA[NSA][2];
 int n_r= 5*NROTS-4;
 float *rot_mat, *q_rotation();

  foreground();

  init_quaternions();

  init_coast(Africa,SA);

  prefposition(8,645,8,485);
  winopen(argv[0]);
  RGBmode();
  RGBwritemask(wrred,wrgrn,wrblu);
  drawmode(NORMALDRAW);
  doublebuffer();
  gconfig();

  mmode(MVIEWING);
  perspective(450,1.0,0.01,100.0);
  loadmatrix(ident);
  polarview(dist,10,800,0);
  lsetdepth(0,0x7fffff);
  zbuffer(TRUE);
  backface(TRUE);

  rotate(-600,'z');
  while( --n_r >= 0)
  {
    zclear();
    cpack(0);
    clear();

    rot_mat = q_rotation(q_AF[n_r]);
    pushmatrix();
    draw_coast_AF(NAF-1,rot_mat,Africa);
    popmatrix();

    rot_mat = q_rotation(q_SA[n_r]);
    pushmatrix();
    draw_coast_SA(NSA-1,rot_mat,SA);
    popmatrix();

    swapbuffers();
/*    getchar(); */
/*    record_it(); */
  }

}

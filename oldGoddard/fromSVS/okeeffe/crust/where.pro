openr,1,'globe.sa-af
a=assoc(1,bytarr(1024,512))
globe=a(0)
close,1
lon=fltarr(1024)
lon = -180.0 + 360.0/1025.0*(1 + indgen(1024))
lon = lon/57.3
lat=fltarr(512)
lat = 90.0 - 180.0/513.0*(1 + indgen(512))
lat = lat/57.3
latgrid=fltarr(1024,512)
longrid=fltarr(1024,512)
for i=0,511 do longrid(0:1023,i)=lon
for i=0,1023 do latgrid(i,0:511)=lat
ws = where( globe eq 1 )
wa = where( globe eq 2 )

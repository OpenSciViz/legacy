#include <stdio.h>
#include <gl.h>
#include <device.h>
#include <math.h>
#include <malloc.h>

/* #define FALSE 0 */
#define DEBUG
#define NYR 1
#define NAF 20734
#define NSA 13079
#define NX 1024
#define NY 512
#define MaxRows 512
#define ELEV_SCALE 0.1
#define Alpha_Trans 0xbf000000 
#define Alpha_Opaq 0xff000000
#define Alpha_H2O Alpha_Trans

typedef struct v_list { long texture; float *vert; struct v_list *next; } V_LIST;
typedef struct mesh { int id; V_LIST *head; struct mesh *next; } MESH;

/* lighting defs. */
static float default_material[] = { AMBIENT, 0.1, 0.1, 0.1,
			   	    DIFFUSE, 0.5, 0.5, 0.5,
				    SPECULAR, 0.6, 0.6, 0.6,
				    SHININESS, 128,
				  /*  EMISSION, 0.5, 0.5, 0.5, */
				    ALPHA, 1.0,
				    LMNULL };


static float default_light_src[] = { LCOLOR, 1.0, 1.0, 1.0,
				     POSITION, 0.0, -1.0, -0.3, 0, /* light src at infinity */
				     LMNULL };


static float default_light_model[] = { AMBIENT, 0.0, 0.0, 0.0,
				       LOCALVIEWER, 0, /* viewer at infinity */
				       TWOSIDE, 1,
				       ATTENUATION, 1, 0,
				       ATTENUATION2, 0,
				       LMNULL };
				       

static float ident[4][4] = { {1.0, 0.0, 0.0, 0.0}, 
			     {0.0, 1.0, 0.0, 0.0}, 
			     {0.0, 0.0, 1.0, 0.0}, 
			     {0.0, 0.0, 0.0, 1.0} };
short wrred = 0xffff, wrgrn = 0xffff, wrblu = 0xffff;
float dist = 4.0, delt = 0.1;
float q1_AF[50][4], q2_AF[50][4], q3_AF[50][4];
float q1_SA[50][4], q2_SA[50][4], q3_SA[50][4];
int V3f_Cnt=0;

int vmag_nonzero( float *v )
{
  float v_mag= sqrt( v[0]*v[0] + v[1]*v[1] + v[2]*v[2] );

  if( v_mag > 0.1 )
    return( 1 );
  else
    return( 0 );
}

float *gen_norm( float *v1, float *v2, float *v3 )
{
  static float res[3], mag;

  res[0] = (v2[1]-v1[1])*(v3[2]-v1[2]) - (v2[2]-v1[2])*(v3[1]-v1[1]);
  res[1] = (v2[2]-v1[2])*(v3[0]-v1[0]) - (v2[0]-v1[0])*(v3[2]-v1[2]);
  res[2] = (v2[0]-v1[0])*(v3[1]-v1[1]) - (v2[1]-v1[1])*(v3[0]-v1[0]);

  mag = sqrt( res[0]*res[0] + res[1]*res[1] + res[2]*res[2] );
  res[0] = res[0]/mag;
  res[1] = res[1]/mag;
  res[2] = res[2]/mag;

  return( &res[0] );
}

V_LIST *next_node(int vcnt, int top, int *Nv, float *vdata)
{
  V_LIST *node= NULL;
  int i;
  float elev;
  unsigned char elev_mask;

  if( vcnt/3 % 2 ) /* bottom vertex */
  {
    node = (V_LIST *) malloc(sizeof(V_LIST));
    node->vert = &vdata[3*Nv[top] + vcnt/2 - 1];
  }
  else /* top vertex */
  {
    node = (V_LIST *) malloc(sizeof(V_LIST));
    node->vert = &vdata[ vcnt/2 ];
  }
  if( vcnt/3 >= (Nv[top] + Nv[top+1] - 1) ) /* this should be the last vertex in the list */
    node->next = NULL;
  else
  {
    vcnt += 3; 
    node->next = next_node(vcnt,top,Nv,vdata);
  }
/* and define the color/texture of the vertex according to its elevation */
  elev = sqrt( node->vert[0]*node->vert[0] + node->vert[1]*node->vert[1] +
		node->vert[2]*node->vert[2] ) - 1.0;
  elev_mask = 255 * elev/ELEV_SCALE;
  node->texture = ((long) 255 + ((long) (128 + elev_mask/2) << 8)
		 + ((long) (64 + elev_mask/4*3) << 16)) |
		Alpha_Opaq; /* orange/brown for land */
  return( node );
}

MESH *next_vlist(int mid, int NRows, int *Nv, float *vdata)
{ /* construct vlist number "cnt". there should be NRows-1 vlists */
 MESH *m= NULL;

  if( mid >= NRows - 1 )
    return( m );
 
  m = (MESH *) malloc(sizeof(MESH));
  m->id = mid;
  m->head = (V_LIST *) malloc(sizeof(V_LIST));
  m->head->vert = &vdata[0];
  m->head->next = next_node(3,m->id,Nv,vdata);
  m->next = next_vlist(++mid,NRows,Nv,&vdata[3*Nv[m->id]]);

  return( m );
}


MESH *init_mesh(int NV, float *vdata)
{
 int n=0, NRows=1;
 static int Nv[MaxRows];
 float zprev;
 MESH *m;

/* find out how many rows there are and how many vertices in each row by
   scanning the vertex data for changes of lat (z of unit vertex = vert[2])
*/
  zprev = vdata[n+2]; 
  Nv[0] = 1;
  for( n = 1; n < MaxRows; n++ )
    Nv[n] = 0;

  for( n = 3; n < 3*NV; n += 3 )
  {
    if( fabs(vdata[n+2] - zprev) < 0.00001 )
      ++Nv[NRows-1];
    else
    {
      zprev = vdata[n+2];
      ++NRows;
      Nv[NRows-1] = 1;
    }
  }
/* NRows - 1 vlists/polygons */
  m = (MESH *) malloc(sizeof(MESH));
  m->id = 0;
  m->head = (V_LIST *) malloc(sizeof(V_LIST));
  m->head->vert = &vdata[0];
  m->head->next = next_node(3,0,Nv,vdata);
  m->next = next_vlist(1,NRows,Nv,&vdata[3*Nv[m->id]]);

  return( m );
}

trav_vlist(float *rot_mat, V_LIST *v)
{
 float *vec, *norm, *rotate_vec();

  cpack(v->texture);
  if( v->next != NULL )
    if( v->next->next != NULL )
    {
      norm = rotate_vec(rot_mat,gen_norm(v->vert,v->next->vert,v->next->next->vert)); 
      n3f(norm); 
    }
  vec = rotate_vec(rot_mat,v->vert);
  v3f(vec); V3f_Cnt++;
  if( v->next != NULL )
    trav_vlist(rot_mat, v->next);
} 
 
draw_mesh(float *rot_mat, MESH *m)
{

  bgntmesh();
    trav_vlist(rot_mat,m->head);
  endtmesh();

  if( m->next != NULL ) 
    draw_mesh(rot_mat,m->next);
}

free_vlist(V_LIST *v)
{
  if( v->next != NULL )
    free_vlist(v->next);

  free( v );
}

free_mesh(MESH *m)
{
  if( m->next != NULL )
    free_mesh(m->next);

  free_vlist(m->head);
  free( m );
} 
   
init_quaternions_AF()
{
  float ang, ax1[3], ax2[3], ax3[3];
  int i;
/* Africa: */
  ax1[0] = -cos(-50.2/57.3)*cos(112.9/57.3);
  ax1[1] = -cos(-50.2/57.3)*sin(112.9/57.3);
  ax1[2] = -sin(-50.2/57.3);

  ax2[0] = -cos(-1.23/57.3)*cos(140.4/57.3);
  ax2[1] = -cos(-1.23/57.3)*sin(140.4/57.3);
  ax2[2] = -sin(-1.23/57.3);

  ax3[0] = -cos(28.51/57.3)*cos(177.0/57.3);
  ax3[1] = -cos(28.51/57.3)*sin(177.0/57.3);
  ax3[2] = -sin(28.51/57.3);

  q1_AF[0][0] = ax1[0]*sin(7.70/57.3/2.0);
  q1_AF[0][1] = ax1[1]*sin(7.70/57.3/2.0);
  q1_AF[0][2] = ax1[2]*sin(7.70/57.3/2.0);
  q1_AF[0][3] = cos(7.70/57.3/2.0);
  for( i = 1; i < 50; i++ )
  {
    ang = 7.70/57.3 - i*7.70/57.3/49.0;
    q1_AF[i][0] = ax1[0]*sin(ang/2.0);
    q1_AF[i][1] = ax1[1]*sin(ang/2.0);
    q1_AF[i][2] = ax1[2]*sin(ang/2.0);
    q1_AF[i][3] = cos(ang/2.0);
  }

  q2_AF[0][0] = ax2[0]*sin(17.45/57.3/2.0);
  q2_AF[0][1] = ax2[1]*sin(17.45/57.3/2.0);
  q2_AF[0][2] = ax2[2]*sin(17.45/57.3/2.0);
  q2_AF[0][3] = cos(17.45/57.3/2.0);
  for( i = 1; i < 50; i++ )
  {
    ang = 17.45/57.3 - i*17.45/57.3/49.0;
    q2_AF[i][0] = ax2[0]*sin(ang/2.0);
    q2_AF[i][1] = ax2[1]*sin(ang/2.0);
    q2_AF[i][2] = ax2[2]*sin(ang/2.0);
    q2_AF[i][3] = cos(ang/2.0);
  }
/* multiply all q2's by q1[0]: */
  for( i = 0; i < 50; i++ )
    quat_mult(q1_AF[0],q2_AF[i]);
  
  q3_AF[0][0] = ax3[0]*sin(18.72/57.3/2.0);
  q3_AF[0][1] = ax3[1]*sin(18.72/57.3/2.0);
  q3_AF[0][2] = ax3[2]*sin(18.72/57.3/2.0);
  q3_AF[0][3] = cos(18.72/57.3/2.0);
  for( i = 1; i < 50; i++ )
  {
    ang = 18.72/57.3 - i*18.72/57.3/49.0;
    q3_AF[i][0] = ax3[0]*sin(ang/2.0);
    q3_AF[i][1] = ax3[1]*sin(ang/2.0);
    q3_AF[i][2] = ax3[2]*sin(ang/2.0);
    q3_AF[i][3] = cos(ang/2.0);
  }
/* multiply all q3's by q2[0]: */
  for( i = 0; i < 50; i++ )
    quat_mult(q2_AF[0],q3_AF[i]);

}

init_quaternions_SA()
{
  float ang, ax1[3], ax2[3], ax3[3];
  int i;

/* South America: */
  ax1[0] = -cos(51.65/57.3)*cos(-10.46/57.3);
  ax1[1] = -cos(51.65/57.3)*sin(-10.46/57.3);
  ax1[2] = -sin(51.65/57.3);

  ax2[0] = -cos(75.08/57.3)*cos(77.59/57.3);
  ax2[1] = -cos(75.08/57.3)*sin(77.59/57.3);
  ax2[2] = -sin(75.08/57.3);

  ax3[0] = -cos(77.21/57.3)*cos(-92.58/57.3);
  ax3[1] = -cos(77.21/57.3)*sin(-92.58/57.3);
  ax3[2] = -sin(77.21/57.3);

  q1_SA[0][0] = ax1[0]*sin(11.64/57.3/2.0);
  q1_SA[0][1] = ax1[1]*sin(11.64/57.3/2.0);
  q1_SA[0][2] = ax1[2]*sin(11.64/57.3/2.0);
  q1_SA[0][3] = cos(11.64/57.3/2.0);
  for( i = 1; i < 50; i++ )
  {
    ang = 11.64/57.3 - i*11.64/57.3/49.0;
    q1_SA[i][0] = ax1[0]*sin(ang/2.0);
    q1_SA[i][1] = ax1[1]*sin(ang/2.0);
    q1_SA[i][2] = ax1[2]*sin(ang/2.0);
    q1_SA[i][3] = cos(ang/2.0);
  }

  q2_SA[0][0] = ax2[0]*sin(17.36/57.3/2.0);
  q2_SA[0][1] = ax2[1]*sin(17.36/57.3/2.0);
  q2_SA[0][2] = ax2[2]*sin(17.36/57.3/2.0);
  q2_SA[0][3] = cos(17.36/57.3/2.0);
  for( i = 1; i < 50; i++ )
  {
    ang = 17.36/57.3 - i*17.36/57.3/49.0;
    q2_SA[i][0] = ax2[0]*sin(ang/2.0);
    q2_SA[i][1] = ax2[1]*sin(ang/2.0);
    q2_SA[i][2] = ax2[2]*sin(ang/2.0);
    q2_SA[i][3] = cos(ang/2.0);
  }
/* multiply all q2's by q1[0]: */
  for( i = 0; i < 50; i++ )
    quat_mult(q1_SA[0],q2_SA[i]);

  q3_SA[0][0] = ax3[0]*sin(15.98/57.3/2.0);
  q3_SA[0][1] = ax3[1]*sin(15.98/57.3/2.0);
  q3_SA[0][2] = ax3[2]*sin(15.98/57.3/2.0);
  q3_SA[0][3] = cos(15.98/57.3/2.0);
  for( i = 1; i < 50; i++ )
  {
    ang = 15.98/57.3 - i*15.98/57.3/49.0;
    q3_SA[i][0] = ax3[0]*sin(ang/2.0);
    q3_SA[i][1] = ax3[1]*sin(ang/2.0);
    q3_SA[i][2] = ax3[2]*sin(ang/2.0);
    q3_SA[i][3] = cos(ang/2.0);
  }
/* multiply all q3's by q2[0]: */
  for( i = 0; i < 50; i++ )
    quat_mult(q2_SA[0],q3_SA[i]);

}

record_it()
{
  static cnt=0;
  static char file[81], cmd[81];
  
  if( cnt < 10 )
    sprintf(file,"frame_00""%d"".rgb",cnt);
  else if( cnt < 100 )
    sprintf(file,"frame_0""%d"".rgb",cnt);
  else if( cnt < 1000 )
    sprintf(file,"frame_""%d"".rgb",cnt);

  sprintf(cmd,"scrsave %s 8 645 8 485",file);
  system(cmd);
  cnt++;
}

elev_adjust(float lat, float lon, float elev, float *sa, float *af)
{
 static int sa_cnt= 0, af_cnt= 0;

  if( lon < -20.0/57.3 ) /* a kludge to distinguish the two continents */
  {
    sa[sa_cnt] = elev * sa[sa_cnt];
    sa[sa_cnt+1] = elev * sa[sa_cnt+1];
    sa[sa_cnt+2] = elev * sa[sa_cnt+2];
    sa_cnt += 3;
  }
  else
  {
    af[af_cnt] = elev * af[af_cnt];
    af[af_cnt+1] = elev * af[af_cnt+1];
    af[af_cnt+2] = elev * af[af_cnt+2];
    af_cnt += 3;
  }
}

init_globe(float *v, long *t, float *sa, float *af)
{
  float lat, lon, elev=1.0;
  int i=0, fd;
  unsigned char cont_mask[NX*NY], elev_mask[NX*NY];

  fd= open("/usr/people/xrhon/crust/globe.sa-af",0);
  if( fd <= 0 ){ printf("unable to open globe.sa-af mask file...\n"); exit(0);}
  read(fd,cont_mask,NX*NY); close( fd );

  fd= open("/usr/people/xrhon/crust/globe.elev",0);
  if( fd <= 0 ){ printf("unable to open globe.elev mask file...\n"); exit(0);}
  read(fd,elev_mask,NX*NY); close( fd );

  for( lat= 90.0/57.3 - 180.0/NY/57.3; lat > -90.0/57.3; lat -= 180.0/NY/57.3 )
    for( lon= -180.0/57.3 + 360.0/NX/57.3; lon < 180.0/57.3; lon += 360.0/NX/57.3, i++ )
    {
      elev = 1.0 + ELEV_SCALE*elev_mask[i]/255.0; 
      t[i] = cont_mask[i];
      if( t[i] == 255 )
      {
        t[i] = (t[i] << 16) | Alpha_H2O; /* Alpha_Trans blue for ocean */
        v[3*i] = 0.99*cos(lat)*cos(lon);
        v[3*i+1] = 0.99*cos(lat)*sin(lon);
        v[3*i+2] = 0.99*sin(lat);
      }
      else if( t[i] == 1 || t[i] == 2 ) /* africa & south america are blanked to ocean for this mesh */
      {
	t[i] = ((long) 255 << 16) | Alpha_H2O; /* Alpha_Trans same as ocean */
        v[3*i] = 0.99*cos(lat)*cos(lon);
        v[3*i+1] = 0.99*cos(lat)*sin(lon);
        v[3*i+2] = 0.99*sin(lat);
        elev_adjust(lat,lon,elev,sa,af); /* but take the opportunity to elevate the other meshes */
      }
      else  /* all land except africa & south america */
      {
	t[i] = ((long) 255 + ((long) (128 + elev_mask[i]/2) << 8)
		 + ((long) (64 + elev_mask[i]/4*3) << 16)) |
		Alpha_Opaq; /* orange/brown for land */
        v[3*i] = elev*cos(lat)*cos(lon);
        v[3*i+1] = elev*cos(lat)*sin(lon);
        v[3*i+2] = elev*sin(lat);
      }
    }
#ifdef DEBUG
  fprintf(stderr,"init_globe> created %d vertices...\n",i);
#endif
}

draw_globe(float *v, long *t)
{ 
 int i, j;
      
  lmcolor(LMC_AD); 
/*  lmcolor(LMC_DIFFUSE); */
/*  lmcolor(LMC_AMBIENT); */
  
  for( j = 0; j < NY-1; j++ )
  {
    bgntmesh();
    for( i = 0; i < NX; i++ )
    {
      cpack( t[j*NX + i] ); 
      n3f( gen_norm(&v[3*(j*NX + i)],&v[3*((j+1)*NX + i)],&v[3*(j*NX + i + 1)]) ); 
      v3f( &v[3*(j*NX + i)] ); 
      cpack( t[(j+1)*NX + i] );
      v3f( &v[3*((j+1)*NX + i)] );
    }  
/* and close the circle */
    cpack( t[j*NX] );
    n3f( gen_norm(&v[3*j*NX ],&v[3*(j+1)*NX],&v[3*(j*NX + 1)]) ); 
    v3f( &v[3*j*NX] ); 
    cpack( t[(j+1)*NX] );
    v3f( &v[3*(j+1)*NX] ); 
    endtmesh();
  }
      
  lmcolor(LMC_COLOR);
}

main(int argc, char **argv, char **env)
{
 static float AF[NAF*3]; 
 static float SA[NSA*3];
 static float GLB[NX*NY*3];
 static float *rot_mat;
 static MESH *africa, *south_america;
 static float fogcolr[] = { 0.1, 0.0, 0.0, 0.0 };
 long GLB_TXT[NX*NY]; 
 int n_yr= 0, fd;
 float *q_rotation();

  fd= open("south_america.verts",0);
  if( fd <= 0 ){ printf("unable to open SA vertex file...\n"); exit(0);}
  read(fd,SA,NSA*4*3); close(fd);

  fd= open("africa.verts",0);
  if( fd <= 0 ){ printf("unable to open Africa vertex file...\n"); exit(0);}
  read(fd,AF,NAF*4*3); close(fd);
 
  south_america = init_mesh(NSA-5,SA); 
  africa = init_mesh(NAF-5,AF); 
  init_globe(GLB,GLB_TXT,SA,AF); 
  init_quaternions_AF();
  init_quaternions_SA();

  prefposition(8,645,8,485); 
/*  prefposition(8,1271,8,1015); */
  winopen(argv[0]);
  RGBmode();
  RGBwritemask(wrred,wrgrn,wrblu);
  drawmode(NORMALDRAW);
/*  doublebuffer(); */
  gconfig();

  mmode(MVIEWING);
/*  perspective(450,1.0,0.01,100.0); */
  ortho(-2.0,2.0,-2.0,2.0,0.01,dist+2);
  loadmatrix(ident);
/*  polarview(dist,10,800,0); */
  lookat(0.0,-dist/1.4142,-dist/1.4142/2.0,0.0,0.0,0.0,1800);
  lsetdepth(0,0x7fffff);
  zbuffer(TRUE);
/*
  lRGBrange(64,64,64,255,255,255,0,0x7f0000);
  depthcue(TRUE);
*/
/* try depth cueing via a black fog 
  fogvertex(FG_DEFINE,fogcolr);
  fogvertex(FG_ON);
*/
  backface(FALSE);
  blendfunction(BF_SA,BF_MSA); /* enable blending */

  lmdef(DEFMATERIAL,1,0,default_material); 
  lmdef(DEFLIGHT,1,0,default_light_src); 
  lmdef(DEFLMODEL,1,0,default_light_model);
  lmbind(MATERIAL,1);
  lmbind(LMODEL,1);
  lmbind(LIGHT0,1);

/*  nmode(NNORMALIZE); */

  rotate(-800,'z'); 
  scale(1.8,1.8,1.8);
  while( n_yr++ < NYR)
  {
    zclear();
    cpack(0);
    clear();

/* rest of globe (as a texturemap) */
    draw_globe(GLB,GLB_TXT); 

/* South America */
    if( n_yr < 50 ) 
      rot_mat = q_rotation(q3_SA[n_yr]);
    else if( n_yr < 100 ) 
      rot_mat = q_rotation(q2_SA[n_yr-50]);
    else if( n_yr < 150 ) 
      rot_mat = q_rotation(q1_SA[n_yr-100]);

    pushmatrix();
      lmcolor(LMC_AD); 
      V3f_Cnt = 0;
      draw_mesh(rot_mat,south_america); 
      fprintf(stderr,"South America vertex count= %d\n",V3f_Cnt);
      lmcolor(LMC_COLOR);
    popmatrix();

/* Africa */
    if( n_yr < 50 ) 
      rot_mat = q_rotation(q3_AF[n_yr]);
    else if( n_yr < 100 ) 
      rot_mat = q_rotation(q2_AF[n_yr-50]);
    else if( n_yr < 150 ) 
      rot_mat = q_rotation(q1_AF[n_yr-100]);


    pushmatrix();
      lmcolor(LMC_AD); 
      V3f_Cnt = 0;
      draw_mesh(rot_mat,africa); 
      fprintf(stderr,"Africa vertex count= %d\n",V3f_Cnt);
      lmcolor(LMC_COLOR);
    popmatrix();

/*    swapbuffers(); */

/*    record_it(); */
  }

}

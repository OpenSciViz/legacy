#include <stdio.h>
#include <gl.h>
#include <device.h>
#include <math.h>
#include <malloc.h>

/* #define FALSE 0 
#define DEBUG */
#define NROTS 31
#define NYR 150
#define NAF 20734
#define NSA 13079
#define NX 1024
#define NY 512
#define MaxRows 512
#define ELEV_SCALE 0.1
#define Alpha_Trans 0xbf000000 
#define Alpha_Opaq 0xff000000
#define Alpha_H2O Alpha_Trans
#define DIST 4.0

typedef struct poly_list { long texture; float *vert1; float *vert2; 
				float *vert3; float *vert4; 
				struct poly_list *next; } POLY_LIST;

typedef struct mesh { POLY_LIST *head; struct mesh *next; } MESH;

/* lighting defs. */
static float default_material[] = { AMBIENT, 0.0, 0.0, 0.0,
			   	    DIFFUSE, 1.0, 1.0, 1.0,
				    SPECULAR, 0.5, 0.5, 0.5,
				    SHININESS, 128,
				    EMISSION, 0.0, 0.0, 0.0, 
				    ALPHA, 1.0,
				    LMNULL };


static float default_light_src[] = { LCOLOR, 1.0, 1.0, 1.0,
				 /*    POSITION, 0.0, -1.0, -0.3, 0,  light src at infinity */
/* light src at view point: */	     POSITION, 0.0, -DIST/1.4142, -DIST/1.4142/2.0, 1,  
				     LMNULL };


static float default_light_model[] = { AMBIENT, 0.0, 0.0, 0.0,
				       LOCALVIEWER, 0, /* viewer at infinity */
				       TWOSIDE, 1,
				       ATTENUATION, 0.25, 0.25,
				       ATTENUATION2, 0.2, /* attempt attenuation for depth cue */
				       ATTENUATION2, 0,
				       LMNULL };
				       

static float ident[4][4] = { {1.0, 0.0, 0.0, 0.0}, 
			     {0.0, 1.0, 0.0, 0.0}, 
			     {0.0, 0.0, 1.0, 0.0}, 
			     {0.0, 0.0, 0.0, 1.0} };
static float AF[NAF*3]; 
static float SA[NSA*3];
static float GLB[NX*NY*3];
static long GLB_TXT[NX*NY]; 
static MESH *africa, *south_america;
static float *rot_mat;
float *q_rotation();

short wrred = 0xffff, wrgrn = 0xffff, wrblu = 0xffff;
float dist = DIST, delt = 0.1;
float q_AF[NROTS*5][4];
float q_SA[NROTS*5][4];
int V3f_Cnt=0;

float *gen_norm( float *v1, float *v2, float *v3 )
{
  static float res[3], mag;

  res[0] = (v2[1]-v1[1])*(v3[2]-v1[2]) - (v2[2]-v1[2])*(v3[1]-v1[1]);
  res[1] = (v2[2]-v1[2])*(v3[0]-v1[0]) - (v2[0]-v1[0])*(v3[2]-v1[2]);
  res[2] = (v2[0]-v1[0])*(v3[1]-v1[1]) - (v2[1]-v1[1])*(v3[0]-v1[0]);

  mag = sqrt( res[0]*res[0] + res[1]*res[1] + res[2]*res[2] );
  res[0] = res[0]/mag;
  res[1] = res[1]/mag;
  res[2] = res[2]/mag;

  return( &res[0] );
}

POLY_LIST *next_poly(int offset,int n, int k, int kk, int *Nv, float *vdata)
{
  POLY_LIST *p= NULL;
  float elev;
  unsigned char elev_mask;

  if( k >= Nv[n]-1 || kk >= (Nv[n] + Nv[n+1]-1) )
    return( p );
   
  p = (POLY_LIST *) malloc(sizeof(POLY_LIST));
  p->vert1 = &vdata[offset + 3*k];
  p->vert2 = &vdata[offset + 3*kk];
  p->vert3 = &vdata[offset + 3*(kk+1)];
  p->vert4 = &vdata[offset + 3*(k+1)];
/* and define the color/texture of the vertex according to its elevation */
  elev = sqrt( p->vert1[0]*p->vert1[0] + p->vert1[1]*p->vert1[1] +
		p->vert1[2]*p->vert1[2] ) - 1.0;
  elev_mask = 255 * elev/ELEV_SCALE;
  p->texture = ((long) 255 + ((long) (128 + elev_mask/2) << 8)
		 + ((long) (64 + elev_mask/4*3) << 16)) |
		Alpha_Opaq; /* orange/brown for land */
  p->next = next_poly(offset,n,++k,++kk,Nv,vdata);

  return( p ); 
}

MESH *next_mesh(int offset, int NRows, int n, int *Nv, float *vdata)
{
  int k=0, kk;
  MESH *m= NULL;

  if( ++n >= NRows - 1 )
    return( m );

  kk = Nv[n];
  m = (MESH *) malloc(sizeof(MESH));
  m->head = (POLY_LIST *) malloc(sizeof(POLY_LIST));
  m->head->vert1 = &vdata[offset + 3*k];
  m->head->vert2 = &vdata[offset + 3*kk];
  if( Nv[n+1] > 1 )
    m->head->vert3 = &vdata[offset + 3*(kk+1)];
  else
  { if( Nv[n] > 1 )
      m->head->vert3 = &vdata[offset + 3*(k+1)];
  }
  if( Nv[n] > 1 && Nv[n+1] > 1 )
    m->head->vert4 = &vdata[offset + 3*(k+1)];
  m->head->next = next_poly(offset,n,k,kk,Nv,vdata);
/*  m->next = next_mesh(NRows,n,Nv,&vdata[3*Nv[n]]); */
  m->next = next_mesh(offset + 3*Nv[n],NRows,n,Nv,vdata);
  return( m );
}

MESH *init_mesh(int NV, float *vdata)
{
 int n=0, k=0, kk=0, NRows=0, offset=0;
 static int Nv[MaxRows];
 float zprev;
 MESH *m;

/* find out how many rows there are and how many vertices in each row by
   scanning the vertex data for changes of lat (z of unit vertex = vert[2])
*/
  zprev = vdata[2]; 
  Nv[0] = 1;
  for( n = 1; n < MaxRows; n++ )
    Nv[n] = 0;

  for( n = 3; n < 3*NV; n += 3 )
  {
    if( fabs(vdata[n+2] - zprev) < 0.00001 )
    {
      Nv[NRows] = Nv[NRows] + 1;
    }
    else
    {
      zprev = vdata[n+2];
      NRows++;
      Nv[NRows] = 1;
    }
  }
  
  kk = Nv[0];
/* NRows - 1 poly meshes */
  m = (MESH *) malloc(sizeof(MESH));
  m->head = (POLY_LIST *) malloc(sizeof(POLY_LIST));
  m->head->vert1 = &vdata[3*k];
  m->head->vert2 = &vdata[3*kk];
  if( Nv[1] > 1 )
    m->head->vert3 = &vdata[3*(kk+1)];
  else
  { if( Nv[0] > 1 )
      m->head->vert3 = &vdata[3*(k+1)];
  }
  if( Nv[0] > 1 && Nv[1] > 1 )
    m->head->vert4 = &vdata[3*(k+1)];
  m->head->next = next_poly(offset,0,k,kk,Nv,vdata);
/*  m->next = next_mesh(NRows,0,Nv,&vdata[3*Nv[0]]); */
  offset = 3*Nv[0];
  m->next = next_mesh(offset,NRows,0,Nv,vdata);

  return( m );
}

draw_plist(float *rot_mat, POLY_LIST *p)
{
 float *vec, *norm, *rotate_vec();

  cpack(p->texture);
  bgnpolygon();
    norm = rotate_vec(rot_mat,gen_norm(p->vert1,p->vert2,p->vert3)); 
    n3f(norm); 
    vec = rotate_vec(rot_mat,p->vert1);
    v3f(vec); V3f_Cnt++;
    vec = rotate_vec(rot_mat,p->vert2);
    v3f(vec); V3f_Cnt++; 
    vec = rotate_vec(rot_mat,p->vert3);
    v3f(vec); V3f_Cnt++;
    if( p->vert4 != NULL )
    {
      norm = rotate_vec(rot_mat,gen_norm(p->vert4,p->vert2,p->vert3)); 
      n3f(norm); 
      vec = rotate_vec(rot_mat,p->vert4);
      v3f(vec); V3f_Cnt++;
    }
  endpolygon();

  if( p->next != NULL )
    draw_plist(rot_mat,p->next);

}
      
     
draw_mesh(float *rot_mat, MESH *m)
{
/*  lmbind(BACKMATERIAL,1); */
  lmcolor(LMC_AD); 
  draw_plist(rot_mat,m->head);
  if( m->next != NULL ) 
    draw_mesh(rot_mat,m->next);
  lmcolor(LMC_COLOR);
/*  lmbind(BACKMATERIAL,0); */
}
   

record_it(int cnt)
{
  static char file[81], cmd[81];
  
  if( cnt < 10 )
    sprintf(file,"frame_00""%d"".rgb",cnt);
  else if( cnt < 100 )
    sprintf(file,"frame_0""%d"".rgb",cnt);
  else if( cnt < 1000 )
    sprintf(file,"frame_""%d"".rgb",cnt);

  sprintf(cmd,"scrsave %s 8 645 8 485",file);
  system(cmd);
  cnt++;
}

elev_adjust(float lat, float lon, float elev, float *sa, float *af)
{
 static int sa_cnt= 0, af_cnt= 0;

  if( lon < -20.0/57.3 ) /* a kludge to distinguish the two continents */
  {
    sa[sa_cnt] = elev * sa[sa_cnt];
    sa[sa_cnt+1] = elev * sa[sa_cnt+1];
    sa[sa_cnt+2] = elev * sa[sa_cnt+2];
    sa_cnt += 3;
  }
  else
  {
    af[af_cnt] = elev * af[af_cnt];
    af[af_cnt+1] = elev * af[af_cnt+1];
    af[af_cnt+2] = elev * af[af_cnt+2];
    af_cnt += 3;
  }
}

init_globe(float *v, long *t, float *sa, float *af)
{
  float lat, lon, elev=1.0;
  int i=0, fd;
  unsigned char cont_mask[NX*NY], elev_mask[NX*NY];

  fd= open("/usr/people/xrhon/crust/globe.sa-af",0);
  if( fd <= 0 ){ printf("unable to open globe.sa-af mask file...\n"); exit(0);}
  read(fd,cont_mask,NX*NY); close( fd );

  fd= open("/usr/people/xrhon/crust/globe.elev",0);
  if( fd <= 0 ){ printf("unable to open globe.elev mask file...\n"); exit(0);}
  read(fd,elev_mask,NX*NY); close( fd );

  for( lat= 90.0/57.3 - 180.0/(NY+1)/57.3; lat > -90.0/57.3; lat -= 180.0/(NY+1)/57.3 )
    for( lon= -180.0/57.3 + 360.0/(NX+1)/57.3; lon < 180.0/57.3; lon += 360.0/(NX+1)/57.3, i++ )
    {
      elev = 1.0 + ELEV_SCALE*elev_mask[i]/255.0; 
      t[i] = cont_mask[i];
      if( t[i] == 255 )
      {
        t[i] = (t[i] << 16) | Alpha_H2O; /* Alpha_Trans blue for ocean */
        v[3*i] = 0.99*cos(lat)*cos(lon);
        v[3*i+1] = 0.99*cos(lat)*sin(lon);
        v[3*i+2] = 0.99*sin(lat);
      }
      else if( t[i] == 1 || t[i] == 2 ) /* africa & south america are blanked to ocean for this mesh */
      {
	t[i] = ((long) 255 << 16) | Alpha_H2O; /* Alpha_Trans same as ocean */
        v[3*i] = 0.99*cos(lat)*cos(lon);
        v[3*i+1] = 0.99*cos(lat)*sin(lon);
        v[3*i+2] = 0.99*sin(lat);
        elev_adjust(lat,lon,elev,sa,af); /* but take the opportunity to elevate the other meshes */
      }
      else /* all land except africa & south america */
      {
	t[i] = ((long) 255 + ((long) (128 + elev_mask[i]/2) << 8)
		 + ((long) (64 + elev_mask[i]/4*3) << 16)) |
		Alpha_Opaq; /* orange/brown for land */
        v[3*i] = elev*cos(lat)*cos(lon);
        v[3*i+1] = elev*cos(lat)*sin(lon);
        v[3*i+2] = elev*sin(lat);
      }
    }
#ifdef DEBUG
  fprintf(stderr,"init_globe> created %d vertices...\n",i);
#endif
}

reinit_globe(float *v, long *t)
{
  float lat, lon, elev=1.0;
  int i=0, fd;
  unsigned char cont_mask[NX*NY], elev_mask[NX*NY];

  fd= open("/usr/people/xrhon/crust/globe.sa-af",0);
  if( fd <= 0 ){ printf("unable to open globe.sa-af mask file...\n"); exit(0);}
  read(fd,cont_mask,NX*NY); close( fd );

  fd= open("/usr/people/xrhon/crust/globe.elev",0);
  if( fd <= 0 ){ printf("unable to open globe.elev mask file...\n"); exit(0);}
  read(fd,elev_mask,NX*NY); close( fd );

  for( lat= 90.0/57.3 - 180.0/(NY+1)/57.3; lat > -90.0/57.3; lat -= 180.0/(NY+1)/57.3 )
    for( lon= -180.0/57.3 + 360.0/(NX+1)/57.3; lon < 180.0/57.3; lon += 360.0/(NX+1)/57.3, i++ )
    {
      elev = 1.0 + ELEV_SCALE*elev_mask[i]/255.0; 
      t[i] = cont_mask[i];
      if( t[i] == 255 )
      {
        t[i] = (t[i] << 16) | Alpha_H2O; /* Alpha_Trans blue for ocean */
        v[3*i] = 0.99*cos(lat)*cos(lon);
        v[3*i+1] = 0.99*cos(lat)*sin(lon);
        v[3*i+2] = 0.99*sin(lat);
      }
      else 
      {
	t[i] = ((long) 255 + ((long) (128 + elev_mask[i]/2) << 8)
		 + ((long) (64 + elev_mask[i]/4*3) << 16)) |
		Alpha_Opaq; /* orange/brown for land */
        v[3*i] = elev*cos(lat)*cos(lon);
        v[3*i+1] = elev*cos(lat)*sin(lon);
        v[3*i+2] = elev*sin(lat);
      }
    }
#ifdef DEBUG
  fprintf(stderr,"reinit_globe> created %d vertices...\n",i);
#endif
}


draw_globe(float *v, long *t)
{ 
 int i, j;
      
  lmcolor(LMC_AD);
  
  for( j = 0; j < NY-1; j++ )
  {
    bgntmesh();
    for( i = 0; i < NX; i++ )
    {
      cpack( t[j*NX + i] ); 
      n3f( gen_norm(&v[3*(j*NX + i)],&v[3*((j+1)*NX + i)],&v[3*(j*NX + i + 1)]) ); 
      v3f( &v[3*(j*NX + i)] ); 
      cpack( t[(j+1)*NX + i] );
      v3f( &v[3*((j+1)*NX + i)] );
    }  
/* and close the circle */
    cpack( t[j*NX] );
    n3f( gen_norm(&v[3*j*NX ],&v[3*(j+1)*NX],&v[3*(j*NX + 1)]) ); 
    v3f( &v[3*j*NX] ); 
    cpack( t[(j+1)*NX] );
    v3f( &v[3*(j+1)*NX] ); 
    endtmesh();
  }
      
  lmcolor(LMC_COLOR);
}

interp_quats(int n, int n_inc)
{
 int i=1;
 float *q0_AF, *q1_AF, *q0_SA, *q1_SA;

  q0_AF = &q_AF[0][0];
  q1_AF = &q_AF[n_inc][0];
  q0_SA = &q_SA[0][0];
  q1_SA = &q_SA[n_inc][0];
  while( i < n*n_inc )
  {
    q_AF[i][0] = q0_AF[0] + 1.0/n_inc * (i % n_inc) * (q1_AF[0] - q0_AF[0]);
    q_AF[i][1] = q0_AF[1] + 1.0/n_inc * (i % n_inc) * (q1_AF[1] - q0_AF[1]);
    q_AF[i][2] = q0_AF[2] + 1.0/n_inc * (i % n_inc) * (q1_AF[2] - q0_AF[2]);
    q_AF[i][3] = q0_AF[3] + 1.0/n_inc * (i % n_inc) * (q1_AF[3] - q0_AF[3]);

    q_SA[i][0] = q0_SA[0] + 1.0/n_inc * (i % n_inc) * (q1_SA[0] - q0_SA[0]);
    q_SA[i][1] = q0_SA[1] + 1.0/n_inc * (i % n_inc) * (q1_SA[1] - q0_SA[1]);
    q_SA[i][2] = q0_SA[2] + 1.0/n_inc * (i % n_inc) * (q1_SA[2] - q0_SA[2]);
    q_SA[i][3] = q0_SA[3] + 1.0/n_inc * (i % n_inc) * (q1_SA[3] - q0_SA[3]);

    if( !(++i % n_inc) )
    {
      q0_AF = q1_AF;
      q0_SA = q1_SA;
      q1_AF = &q_AF[i + n_inc][0];
      q1_SA = &q_SA[i + n_inc][0];
      i++;
    }
  }
}
      
init_quaternions()
{
  float axis[3], q_prev[4];
  float age, lat, lon, ang, ang_inc;
  int i, k=0;
  FILE *fp;
  static char tmp[81];
    
/* what to do about a change in axis of rotation? quat_mult(q_prev,q_this) ??? */

/* Africa: */

  fp = fopen("/usr/local/data/africa.rot","r");
  fgets(tmp,80,fp);
  fgets(tmp,80,fp); /* just discard the first two lines */

  for( i = 0; i < NROTS; i++ )
  {
    fgets(tmp,80,fp);
    sscanf(tmp,"%f %f %f %f",&age,&lat,&lon,&ang);
    ang = ang / 57.3;
    lat = lat / 57.3;
    lon = lon / 57.3;
    axis[0] = -cos(lat)*cos(lon);
    axis[1] = -cos(lat)*sin(lon);
    axis[2] = -sin(lat);   
    q_AF[5*i][3] = cos(ang/2.0);
    q_AF[5*i][0] = axis[0]*sin(ang/2.0);
    q_AF[5*i][1] = axis[1]*sin(ang/2.0);
    q_AF[5*i][2] = axis[2]*sin(ang/2.0);
  }
  fclose(fp);

/* South America */

  fp = fopen("/usr/local/data/SA.rot","r");
  fgets(tmp,80,fp);
  fgets(tmp,80,fp); /* just discard the first two lines */
  for( i = 0; i < NROTS; i++ )
  {
    fgets(tmp,80,fp);
    sscanf(tmp,"%f %f %f %f",&age,&lat,&lon,&ang);
    ang = ang / 57.3;
    lat = lat / 57.3;
    lon = lon / 57.3;
    axis[0] = -cos(lat)*cos(lon);
    axis[1] = -cos(lat)*sin(lon);
    axis[2] = -sin(lat);   
    q_SA[5*i][3] = cos(ang/2.0);
    q_SA[5*i][0] = axis[0]*sin(ang/2.0);
    q_SA[5*i][1] = axis[1]*sin(ang/2.0);
    q_SA[5*i][2] = axis[2]*sin(ang/2.0);
  }
  fclose(fp);

/* interpolate between every 5 */
  interp_quats(NROTS-1,5);
}

main(int argc, char **argv, char **env)
{
 static float fogcolr[] = { 0.1, 0.0, 0.0, 0.0 };
 int n_yr= 5*NROTS-4, nrot= 0, fd;

  foreground();

  fd= open("south_america.verts",0);
  if( fd <= 0 ){ printf("unable to open SA vertex file...\n"); exit(0);}
  read(fd,SA,NSA*4*3); close(fd);

  fd= open("africa.verts",0);
  if( fd <= 0 ){ printf("unable to open Africa vertex file...\n"); exit(0);}
  read(fd,AF,NAF*4*3); close(fd);
 
  south_america = init_mesh(NSA,SA); 
  africa = init_mesh(NAF,AF); 
  init_globe(GLB,GLB_TXT,SA,AF); 
  init_quaternions();

  prefposition(8,645,8,485); 
/*  prefposition(8,1271,8,1015); */
  winopen(argv[0]);
  RGBmode();
  RGBwritemask(wrred,wrgrn,wrblu);
  drawmode(NORMALDRAW);
  gconfig();

  mmode(MVIEWING);
/*  perspective(450,1.0,0.01,100.0); */
  ortho(-1.3,1.3,-1.05,1.05,0.01,dist+2); 
  loadmatrix(ident);
/*  polarview(dist,10,800,0); */
  lookat(0.0,-dist/1.4142,-dist/1.4142/2.0,0.0,0.0,0.0,1800);
  lsetdepth(0,0x7fffff);
  zbuffer(TRUE);
/* try depth cueing via a black fog 
  fogvertex(FG_DEFINE,fogcolr);
  fogvertex(FG_ON);
*/
  backface(FALSE);
  blendfunction(BF_SA,BF_MSA); /* enable blending */

  lmdef(DEFMATERIAL,1,0,default_material); 
  lmdef(DEFLIGHT,1,0,default_light_src); 
  lmdef(DEFLMODEL,1,0,default_light_model);
  lmbind(MATERIAL,1);
  lmbind(LMODEL,1);
  lmbind(LIGHT0,1);

/*  nmode(NNORMALIZE); */
  rotate(-800,'z');

  while( --n_yr > 0)
    draw_multi_mesh(n_yr);

/* after the continent migration, lets do a full rotation, first redefine the lat-lon 
  grid to include africa and south america:*/

  reinit_globe(GLB,GLB_TXT); 

  while( nrot < 72 )
    draw_one_mesh(150,nrot++);

}

draw_multi_mesh(int n_yr)
{
    zclear();
    cpack(0);
    clear();

/* lat-lon grid: */
    pushmatrix();
      draw_grid(); 
    popmatrix();

/* rest of globe (as a texturemap) */
    draw_globe(GLB,GLB_TXT); 

/* South America */
    rot_mat = q_rotation(q_SA[n_yr]);

    pushmatrix();
      lmcolor(LMC_AD); 
      V3f_Cnt = 0;
      draw_mesh(rot_mat,south_america); 
      fprintf(stderr,"South America vertex count= %d\n",V3f_Cnt);
      lmcolor(LMC_COLOR);
    popmatrix();

/* Africa */
    rot_mat = q_rotation(q_AF[n_yr]);

    pushmatrix();
      lmcolor(LMC_AD); 
      V3f_Cnt = 0;
      draw_mesh(rot_mat,africa); 
      fprintf(stderr,"Africa vertex count= %d\n",V3f_Cnt);
      lmcolor(LMC_COLOR);
    popmatrix();

    record_it(NYR-n_yr); 
    fprintf(stderr,"frame # %d\n",n_yr);
}

draw_one_mesh( int n_yr, int nrot)
{
    zclear();
    cpack(0);
    clear();
    rotate(50,'z');

/* lat-lon grid: */
    pushmatrix();
      draw_grid();
    popmatrix();

    draw_globe(GLB,GLB_TXT); 

    record_it(n_yr+nrot); 
    fprintf(stderr,"frame # %d\n",n_yr+nrot);
}

draw_grid()
{
 int i=0;
 float z=0.0, r;
 Angle a= 1800/9;

  cpack(0xffffffff);

  lRGBrange(0,0,0,255,255,255,0,0x7f0000);
  depthcue(TRUE);

  z = -0.95;
  r = 0.88*sqrt(1.0 - z*z);
  translate(0.0,0.0,z);
  circ(0.0,0.0,r);
  translate(0.0,0.0,-z);

  z = -0.8;
  r = 0.89*sqrt(1.0 - z*z);
  translate(0.0,0.0,z);
  circ(0.0,0.0,r);
  translate(0.0,0.0,-z);
  
  for( z = -0.6; z <= 0.4; z += 0.2 )
  {
    r = 0.98*sqrt(1.0 - z*z);
    translate(0.0,0.0,z);
    circ(0.0,0.0,r);
    translate(0.0,0.0,-z);
  }
  z = 0.6;
  r = 0.95*sqrt(1.0 - z*z);
  translate(0.0,0.0,z);
  circ(0.0,0.0,r);
  translate(0.0,0.0,-z);

  z = 0.8;
  r = 0.88*sqrt(1.0 - z*z);
  translate(0.0,0.0,z);
  circ(0.0,0.0,r);
  translate(0.0,0.0,-z);

  z = 0.95;
  r = 0.88*sqrt(1.0 - z*z);
  translate(0.0,0.0,z);
  circ(0.0,0.0,r);
  translate(0.0,0.0,-z);

  r = 0.98;
  rotate(900,'x');
  while( i++ < 9 )
  {
    rotate(a,'y');
    circ(0.0,0.0,r);
  }

  depthcue(FALSE);

}
  
  
  






#include <math.h>
#define N 512;

typedef struct { float r; float i; } COMPLEX;

COMPLEX work[5*N/2], tmp1[N], tmp2[N];

void CCOPY(), CFFT2(), CSWAP(); /* Cray math lib */

void cray_2dfft(mode,in,out)
int mode;
COMPLEX in[], out[];
{

  int i=1, j=1, k=0, n=N;
  
/* fft each row: */
  for( k = 0; k < N*N; k += N );
  {  
    CCOPY(&n,&in[k],&i,tmp1,&j);
    CFFT2(&k,&mode,&n,tmp1,work,&out[k]);
  }

/* then each column */
  i = 512;
  for( k = 0; k < N*N; k += N );
  {  
    CCOPY(&n,&in[k],&i,tmp1,&j);
    CFFT2(&k,&mode,&n,tmp1,work,tmp2)
    CCOPY(&n,tmp2,&j,&out[k],&i);
  }
}

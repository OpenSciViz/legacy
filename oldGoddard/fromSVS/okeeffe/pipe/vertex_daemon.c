#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>

int fd;
int sender_pid = -1;
int my_pid;

void finished(sig)
int sig;
{
  close(fd);
  unlink("/tmp/xrhon_daemon.pipe");
  exit(0);
}

void getvertex(sig)
int sig;
{
  static int nb = -1;
  float v[6];
  printf("getvertex> nb= %d\n",nb);
  if( nb == -1 )
  {
    nb = read(fd,&sender_pid,4);
    printf("FIFO gave me sender pid= %d\n",sender_pid);
/* and put my pid into the FIFO, and signal the sender 
    my_pid = getpid();
    write(fd,&my_pid,4);
    kill(sender_pid,SIGUSR2); */
    return;
  }
  nb = read(fd,v,24);
  if( nb > 0 )
  {
    printf("FIFO gave me v = %f %f %f; %f %f %f\n",
	v[0],v[1],v[2],v[3],v[4],v[5]);
/* tell sender i finished reading the vertex 
    write(fd,&my_pid,4);
    kill(sender_pid,SIGUSR2); */  
    return ;
  }
  else
    finished(0);    
}


main()
{
  int stat;
  unsigned char c=0;
  float v[6]; /* vertex x,y,z,r,g,b */
  
  umask(0);
  stat = mknod("/tmp/xrhon_daemon.pipe",0666 | S_IFIFO,0); 

  if( stat < 0 ) 
  {
    printf("daemon> unable to create FIFO...\n");
    exit( stat );
  }
  printf("daemon> successfully created FIFO, sleeping on open...\n");

  fd = open("/tmp/xrhon_daemon.pipe",0);
  if( fd <= 0 ) 
    exit(1);

/* signal from vertex write routine tells me to quit 
  signal(SIGUSR1,finished); */

/* this signal is recieved form the vertex write routine and indicates
   that the FIFO should contain a vertex */
  printf("daemon> entering infinite loop...\n");
  while( 1 )
  {
/*    signal(SIGUSR1,getvertex);
    printf("daemon> returned from signal().\n");
*/
    getvertex(0);
  } 
}


#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <math.h>

#define NV 32768
#define ZMAX 5.20
#define RMAX 5.20
#define RMIN 0.39

int fd;
float v[NV][6];


float zdust(r,z)
float r, z;
{
   float w;

   w = fabs(z / r);
   w = pow(w,1.3); 
   w = exp( -2.6 * w );
   w = w / r;

   return( w );
}

float z_integ()
{
   float tot, r, z;
   int i, j, maxran;
 
   tot = 0.0; 
/*
   r = 0.1; 
   while( r < RMAX )
   {
	z = 0.0;
        r = r + 0.05;
	while( z < ZMAX )
	{
	   tot = tot + r*zdust(r,z);
	   z = z + 0.05;
	}
   }
*/
   maxran = pow(2,15) - 1;
   i = 0;
   while( i < NV )
   {
        r = RMIN + RMAX*rand()/maxran;
        z = 2.0*ZMAX*( 0.5 - 1.0*rand()/maxran );
	tot = tot + r*zdust(r,z);
	i++;
   }
   printf("integral= %f, (r,z) < MAX: %f\n",tot,r);
   return( tot );
}

zdust_init(seed)
int seed;
{
   int n, i, k, nval=0, max_nval=0;
   float bincnt, r, z, tmp;
   float phi, x, y, total=0.0;
   int maxran;
   float integral;

   integral = z_integ();

   srand(seed);
   maxran = pow(2,15) - 1;
   n = 0;
/* generate random numbers in the approp. range and populate the volume */

   while( n < NV )
   {
      r = RMIN + (RMAX-RMIN)*rand()/maxran;
      z = 2.0*ZMAX*( 0.5 - 1.0*rand()/maxran );
      tmp = r*zdust(r,z) / integral;
      total = total + tmp;
      nval = tmp * NV;
      if( nval > max_nval ) max_nval = nval;
	for( i = 0; i < nval && n < NV; i++)
	{
	   phi = 360.0 * rand() / 57.3 / maxran; 
	   x = r * cos( phi );
	   y = r * sin( phi );
	   v[n][0] = x; 
	   v[n][1] = y; 
	   v[n][2] = z; 
           colr_dust(RMAX,r,&v[n][3]);
	   n++;
	}
   }
   printf("total= %f,  n= %d, max_nval= %d\n",total,n,max_nval);
}

int colr_dust(rmax,r,colr)
float rmax, r, colr[];
{
   float tmp, tmin, tmax, del;
   float RC, CMAX;

/*   RC =  1.0 - sqrt( RMIN/r ) + sqrt( RMIN/RMAX ); */
   CMAX = sqrt( RMAX*RMAX + ZMAX*ZMAX );
   RC =  1.0 - sqrt( RMIN/r ) + sqrt( RMIN/CMAX ); 
   RC =  RC * RC;

   if( RC < 0.2 )
   {
      colr[0] = 0.75 * ( 0.2 - RC ); 
      colr[1] = 0.0;
      colr[2] = 1.0;
   }
   else if( RC < 0.4 )
   {
      colr[0] = 0.0;
      colr[1] = (RC - 0.2) / 0.2;
      colr[2] = 1.0;
   }
   else if( RC < 0.6 )
   {
      colr[0] = 0.0;
      colr[1] = 1.0;
      colr[2] = 1.0 - (RC - 0.4) / 0.2;
   }
   else if( RC < 0.8 )
   {
      colr[0] = (RC - 0.6) / 0.2;
      colr[1] = 1.0;
      colr[2] = 0.0;
   }
   else 
   {
      colr[0] = 1.0;
      colr[1] = 1.0 - (RC- 0.8) / 0.2;
      colr[2] = 0.0;
   }

   return;
}

main(argc,argv)
int argc;
char **argv;
{
  int i, j, cnt=0, tot=1;
  int my_pid, seed=1234567;

  if( argc > 1 ) tot = atoi(argv[1]);

  srand48(seed);

  fd = open("/tmp/xrhon_daemon.pipe",2);
  if( fd <= 0 )
  {
    printf("unable to open FIFO for output...\n");
    exit(1);
  }

/* put my pid into the FIFO first */
  my_pid = getpid();
  write(fd,&my_pid,4);


  while( cnt++ < tot )
  {
/*    for( i = 0; i < NV; i++ )
      for( j = 0; j < 6; j++ ) 
        v[i][j] = drand48(); */
/* initializ the dust cloud vertices */
    zdust_init(cnt);
/* send the results to the FIFO */
    write(fd,v,24*NV);
  }

  close(fd);
}

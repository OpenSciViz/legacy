#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>

#define NV 32768

int fd;
int sender_pid = -1;
float v[NV][6];

void finished(sig)
int sig;
{
  close(fd);
  unlink("/tmp/xrhon_daemon.pipe");
  exit(0);
}

void getvertex(sig)
int sig;
{
  static int nb = -1, nb_req, nvert;
/*  printf("getvertex> nb= %d\n",nb); */
  if( nb == -1 )
  {
    nb = read(fd,&sender_pid,4);
    printf("FIFO gave me sender pid= %d\n",sender_pid);
    return;
  }
  nb_req = NV*24;
  nb = read(fd,v,nb_req);
  if( nb > 0 )
  {
    nvert = nb / 24;
    while( (nvert < NV) && (nb > 0) )
    {
/*      printf("NV= %d, nvert= %d\n",NV,nvert); */
      nb_req = 24*(NV - nvert);
      nb = read(fd,v[nvert],nb_req);
      nvert = nvert + nb / 24;
    }
    printf("FIFO gave me %d vertices, v[NV/2] = %f %f %f; %f %f %f\n",nvert,
	v[NV/2][0],v[NV/2][1],v[NV/2][2],v[NV/2][3],v[NV/2][4],v[NV/2][5]);
    return ;
  }
  else
    finished(0);    
}


main()
{
  int stat;
  
  umask(0);
  stat = mknod("/tmp/xrhon_daemon.pipe",0666 | S_IFIFO,0); 

  if( stat < 0 ) 
  {
    printf("daemon> unable to create FIFO...\n");
    exit( stat );
  }
  printf("daemon> successfully created FIFO, sleeping on open...\n");

  fd = open("/tmp/xrhon_daemon.pipe",0);
  if( fd <= 0 ) 
    exit(1);

  printf("daemon> entering infinite loop...\n");
  while( 1 )
    getvertex(0);
}


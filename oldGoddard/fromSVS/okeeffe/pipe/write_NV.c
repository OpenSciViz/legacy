#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <math.h>

#define NV 32768

int fd;
float v[NV][6];

main(argc,argv)
int argc;
char **argv;
{
  int i, j, cnt=0, tot=1;
  int my_pid, seed=1234567;

  if( argc > 1 ) tot = atoi(argv[1]);

  srand48(seed);

  fd = open("/tmp/xrhon_daemon.pipe",2);
  if( fd <= 0 )
  {
    printf("unable to open FIFO for output...\n");
    exit(1);
  }

/* put my pid into the FIFO first */
  my_pid = getpid();
  write(fd,&my_pid,4);

  while( cnt++ < tot )
  {
    for( i = 0; i < NV; i++ )
      for( j = 0; j < 6; j++ ) 
        v[i][j] = drand48(); 

    write(fd,v,24*NV);
  }

  close(fd);
}

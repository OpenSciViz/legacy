#include <sys/types.h>
#include <sys/stat.h>

main()
{
  int stat, nb=1, fd;
  unsigned char c=0;

  umask(0);
  stat = mknod("/tmp/xrhon_daemon.pipe",0666 | S_IFIFO,0); 

  if( stat < 0 ) 
  {
    printf("unable to create FIFO...\n");
    exit( stat );
  } 
  fd = open("/tmp/xrhon_daemon.pipe",0);
  while( c != 255 )
  {
    nb = read(fd,&c,1);
    printf("FIFO gave me c = %d\n",c);     
  }
  close(fd);
  unlink("/tmp/xrhon_daemon.pipe");
}

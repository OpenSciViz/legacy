#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <stdio.h>
#include <gl.h>
#include <device.h>
#include <math.h>

#define FALSE 0
#define TRUE 1
#define NV 32768
#define ZMAX 5.20
#define RMAX 5.20
#define RMIN 0.39

int fd;
int sender_pid = -1;
float v[NV][6];
static float ident[4][4] = { {1.0, 0.0, 0.0, 0.0}, 
			     {0.0, 1.0, 0.0, 0.0}, 
			     {0.0, 0.0, 1.0, 0.0}, 
			     {0.0, 0.0, 0.0, 1.0} };
long xrot=700, delrot=100;
float dist = 3.5*RMAX, delt = RMAX/10.;
char do_zoom= FALSE, do_rotate= TRUE;
short wrred = 0xffff, wrgrn = 0xffff, wrblu = 0xffff;

void finished(sig)
int sig;
{
  close(fd);
  unlink("/tmp/xrhon_daemon.pipe");
  exit(0);
}

int getvertex(sig)
int sig;
{
  static int nb = -1, nb_req, nvert;
/*  printf("getvertex> nb= %d\n",nb); */
  if( nb == -1 )
  {
    nb = read(fd,&sender_pid,4);
    printf("FIFO gave me sender pid= %d\n",sender_pid);
    return( -1 );
  }
  nb_req = NV*24;
  nb = read(fd,v,nb_req);
  if( nb > 0 )
  {
    nvert = nb / 24;
    while( (nvert < NV) && (nb > 0) )
    {
/*      printf("NV= %d, nvert= %d\n",NV,nvert); */
      nb_req = 24*(NV - nvert);
      nb = read(fd,v[nvert],nb_req);
      nvert = nvert + nb / 24;
    }
    printf("FIFO gave me %d vertices, v[NV/2] = %f %f %f; %f %f %f\n",nvert,
	v[NV/2][0],v[NV/2][1],v[NV/2][2],v[NV/2][3],v[NV/2][4],v[NV/2][5]);

    return(nvert);
  }
  else
    finished(0);    
}


main()
{
  int stat, nvert;
  
  umask(0);
  stat = mknod("/tmp/xrhon_daemon.pipe",0666 | S_IFIFO,0); 

  if( stat < 0 ) 
  {
    printf("daemon> unable to create FIFO...\n");
    exit( stat );
  }
  printf("daemon> successfully created FIFO, sleeping on open...\n");

  fd = open("/tmp/xrhon_daemon.pipe",0);
  if( fd <= 0 ) 
    exit(1);
/*
prefposition(x0,x0+988,y0,y0+988);
keepaspect(1,1); 
*/
winopen("Zdust Cloud            \n");
RGBmode();
RGBwritemask(wrred,wrgrn,wrblu); 
drawmode(NORMALDRAW); 
/* doublebuffer(); */

gconfig();

mmode(MVIEWING);
perspective(350,1.0,0.01,500.0);
/* ortho( -RMAX, RMAX, -RMAX, RMAX, -RMAX, RMAX ); */
lsetdepth(0,0x7fffff);
zbuffer(TRUE);

qdevice(ZKEY);
qdevice(RKEY);
qdevice(CKEY);
qdevice(GKEY);
qdevice(LEFTMOUSE);
qdevice(MIDDLEMOUSE);
/* qdevice(RIGHTMOUSE); */
unqdevice(MOUSEX);
unqdevice(MOUSEY);

  printf("daemon> entering infinite loop...\n");
  while( 1 )
  {
    nvert = getvertex(0);
    if( nvert > 0 ) render(nvert);
  }
}

render(nvert)
int nvert;
{
  int k=0; 

  zclear();
  cpack(0);
  clear();

  pushmatrix();
  loadmatrix(ident);
  polarview(dist,0,0,0);
  rotate(xrot,'x');
   
  bgnpoint();
    while( k++ < nvert )
    {
      c3f(&v[k][3]);
      v3f(&v[k][0]);
    }
  endpoint();

/*   swapbuffers(); */

}


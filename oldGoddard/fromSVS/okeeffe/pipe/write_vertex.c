#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>

int fd;
int daemon_pid = -1;

void get_daemon_pid(sig)
int sig;
{
  int nb, pid;
 
  nb = read(fd,&pid,4);
  if( pid != daemon_pid )
  {
    printf("daemon_pid conflict! %d %d\n",pid,daemon_pid);
    exit(1);
  }
  return; 
}


main(argc,argv)
int argc;
char **argv;
{
  int i=0, j;
  static float v[6] = { 1.0, 0.95, 0.9 , 0.5, 0.45, 0.4 };
  int my_pid;

  if( argc > 1 )
    daemon_pid = atoi(argv[1]);

  fd = open("/tmp/xrhon_daemon.pipe",2);
  if( fd <= 0 )
  {
    printf("unable to open FIFO for output...\n");
    exit(1);
  }

/* put my pid into the FIFO first, and signal */
  my_pid = getpid();
  write(fd,&my_pid,4);
/*  kill(daemon_pid,SIGUSR1); */

/* wait for signal from daemon that indicates it has put its pid into the pipe: 
  signal(SIGUSR2,get_daemon_pid); */ 

  while( i++ < 10 )
  {
    write(fd,v,24);
/* send signal to indicate vertex is available 
    kill(daemon_pid,SIGUSR1); */
/*    signal(SIGUSR2,get_daemon_pid);  */
    for( j = 0; j < 6; j++ ) 
      v[j] = 2.0*v[j]; 
  }

  close(fd);
/*  kill(daemon_pid,SIGUSR1); */
}

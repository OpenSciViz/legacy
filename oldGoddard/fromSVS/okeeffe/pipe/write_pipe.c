main(argc,argv)
int argc;
char **argv;
{
  int i=0, fd;
  unsigned char c=255;

  fd = open("/tmp/xrhon_daemon.pipe",1);
  if( fd <= 0 )
  {
    printf("unable to open FIFO for output...\n");
    exit(1);
  }
  while( argv[1][i] != '\0' )
  {
    write(fd,&argv[1][i],1);
    i++;
  }
  sleep(3);
  i=0;
  while( argv[2][i] != '\0' )
  {
    write(fd,&argv[2][i],1);
    i++;
  }

  write(fd,&c,1);

  close(fd);
}

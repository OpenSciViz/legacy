#include <stdio.h>
#include <gl.h>
#include <device.h>
#include <math.h>
#define FALSE 0
#define DEBUG

#ifdef MAIN
#define global
#else
#define global extern
#endif

#define NX 256
#define NY 128
#define NP 360*180
#define PI 3.1415926

global float vlist[NY][NX][3];
global short clist[NY][NX][3];
global float voz[NP][3];
global short coz[20][NP][3];


#include "ozone.h"
static float ident[4][4] = { {1.0, 0.0, 0.0, 0.0}, 
			     {0.0, 1.0, 0.0, 0.0}, 
			     {0.0, 0.0, 1.0, 0.0}, 
			     {0.0, 0.0, 0.0, 1.0} };
short wrred = 0xffff, wrgrn = 0xffff, wrblu = 0xffff;
float dist = 5.0, delt = 0.1;
long rot_ang=0;

int graphics()
{
 int i, cnt=0, tri_mesh();
 int x0 = 16, y0 = 8;

   prefposition(x0,x0+988,y0,y0+988);
   keepaspect(1,1);
   winopen("Ozone");
   RGBmode();
   RGBwritemask(wrred,wrgrn,wrblu);
   drawmode(NORMALDRAW);
   doublebuffer();
   gconfig();
/*
   mmode(MVIEWING);
   perspective(450,1.0,0.01,100.0);
   loadmatrix(ident);
   polarview(dist,10,1700,0); 
   polarview(dist,10,900,0); */ 

   lsetdepth(0,0x7fffff);
   zbuffer(TRUE);
/*   backface(TRUE); */

   while( TRUE )
   {
      zclear();
      cpack(0);
      clear();

      if( cnt >= 20 ) cnt = 0;
      if( cnt == 0 )
      {
         mmode(MVIEWING);
         perspective(450,1.0,0.01,100.0);
         loadmatrix(ident);
         polarview(dist,10,rot_ang/2,0); 
         rot_ang = rot_ang + 200;
         if( rot_ang > 3600 ) rot_ang = 0;
         rotate(rot_ang,'z'); 
      } 
      for( i = 0; i < NY-1; i++ ) /* globe is a triangle mesh */
         tri_mesh(i);

/*      draw_ozone(cnt++); */

      swapbuffers(); 
   }
}

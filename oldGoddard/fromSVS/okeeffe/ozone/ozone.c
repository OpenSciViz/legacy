#define MAIN
#include "ozone.h"

main(argc,argv)
int argc; char **argv;
{
FILE *fp_pixmap, *fp_dirbe;
float latt, lon;
short res=9;
int i, j, k, maxran, seed=1, cnt=0;
unsigned char data[NX];
int graphics(); 
   
   fp_pixmap = fopen("/usr/people/xrhon/utils/mini_globe.pixmap","r");
   latt = 0.0;  /* PI / 2.0; */
   for( i = 0; i < NY; i++ )
   {
      latt = latt + (PI / NY);
      fread(data,1,NX,fp_pixmap);
      lon = 0.0;
      for( j = 0; j < NX; j++ )
      {
	  lon = lon + PI / NY; /* assuming NX = 2 NY */
          vlist[i][j][2] = cos(latt);
	  vlist[i][j][0] = sin(latt) * cos(lon);
	  vlist[i][j][1] = sin(latt) * sin(lon);

	  clist[i][j][0] = data[j];
	  clist[i][j][1] = data[j];
	  clist[i][j][2] = data[j];
      }
   }
   fclose(fp_pixmap);
/*
   srand(seed);
   maxran = pow(2,15) - 1;
   for( cnt = 0; cnt < NP; cnt++ )
   {
      latt = PI * rand() / maxran;
      lon = PI * rand() / maxran;
      voz[cnt][2] = -1.1 * sin(latt);
      voz[cnt][1] = 1.1 * cos(latt) * cos(lon);
      voz[cnt][0] = 1.1 * cos(latt) * sin(lon);

      coz[cnt][0] = 255.0 * voz[cnt][0] / 1.1;
      coz[cnt][1] = 255.0 * voz[cnt][1] / 1.1;
      coz[cnt][2] = 255.0 * -voz[cnt][2] / 1.1;
   }
   printf("Ozone> getting data from standard input...\n");
   for( k = 0; k < 20; k++ )
   {
   cnt = 0;
   latt = PI / 2.0;
   for( i = 0; i < 180; i++ )
   {
      latt = latt - (PI / 180.0);
      lon = -PI;
      for( j = 0; j < 360; j++ )
      {
	  seed = getchar();
	  if( latt > 100.0 )
	  {

	     if( k == 0 )
	     {
		lon = lon + PI / 180.0; 
                voz[cnt][2] = 1.1 * sin(latt);
	        voz[cnt][0] = 1.1 * cos(latt) * cos(lon);
	        voz[cnt][1] = 1.1 * cos(latt) * sin(lon);
	     }

	     oz_colr(seed/255.0,coz[k][cnt++]);
	  }

      }
   }
   }
*/

   graphics();
}

int oz_colr(RC,colr)
float RC; unsigned short colr[3];
{
   if( RC < 0.2 )
   {
      colr[0] = 255 * 0.75 * ( 0.2 - RC ); 
      colr[1] = 0;
      colr[2] = 255;
   }
   else if( RC < 0.4 )
   {
      colr[0] = 0;
      colr[1] = 255 * (RC - 0.2) / 0.2;
      colr[2] = 1;
   }
   else if( RC < 0.6 )
   {
      colr[0] = 0;
      colr[1] = 1.0;
      colr[2] = 255 * (1.0 - (RC - 0.4) / 0.2);
   }
   else if( RC < 0.8 )
   {
      colr[0] = 255 * (RC - 0.6) / 0.2;
      colr[1] = 1;
      colr[2] = 0;
   }
   else 
   {
      colr[0] = 1;
      colr[1] = 255 * (1.0 - (RC- 0.8) / 0.2);
      colr[2] = 0;
   }
}

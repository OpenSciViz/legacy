#include "ozone.h"

int tri_mesh(idx)
int idx;
{
   int i, cnt;

      i = 0;
      while( i < NX-2 )
      {
         cnt = 0;
         bgntmesh();
         while( (cnt < 256) && (i < NX-2) )
         {
            c3s( clist[idx][i] ); v3f( vlist[idx][i] );
	    c3s( clist[idx+1][i] ); v3f( vlist[idx+1][i] );
	    c3s( clist[idx][i+1] ); v3f( vlist[idx][i+1] );
	    c3s( clist[idx+1][i+1] ); v3f( vlist[idx+1][i+1] );
	    cnt = cnt + 4;
	    i = i + 2;
         }
         endtmesh();
      }
}

#include <stdio.h>
#include <gl/gl.h>
#include <gl/device.h>
#include <math.h>

#define N 256
  
float volume[8][3];
unsigned char data[3][N*N];

main(argc,argv)
int argc; char **argv;
{
  int i, j, Do_rotate=1, Do_translate= -1;
  long alph_bits, event, x=0, y=0, z=0;
  long seed=123456789;
  long alpha0=0xff000000, alpha1=0xff000000, alpha2=0x66000000;
  static long clist[3][N*N];
  static float tmp, trn=0.0, vlist[N*N][3];
  short qdata;
  int fd=0;
  unsigned char r[256], g[256], b[256]; /* rgb color indices */

  if( argc < 2 )
  {
    fprintf(stderr,"view rgbcolortable < file\n");
    exit(0);
  }
  fd = open(argv[1],0);
  read(fd,r,208);
  read(fd,g,208);
  read(fd,b,208);

  read(0,data,3*N*N);
/*  srand48(seed); */
  if( (alph_bits = getgdesc(GD_BITS_NORM_DBL_ALPHA)) == 0 )
  {
    fprintf(stderr,"Alpha plane not available on this machine\n");
    return 1;
  }
  else
    fprintf(stderr,"Alpha plane is %d bits deep...\n",alph_bits);

/* initialize vertices with random colors: */
  for( i = 0; i < N; i++ )
    for( j = 0; j < N; j++ )
    {
/*
      tmp = 16*1024*1024*drand48();
      clist[i*N + j] = alpha | (long)tmp;
*/
      clist[0][i*N + j] = alpha0 | r[3*data[0][i*N +j]] | 
				   (g[3*data[0][i*N +j]] << 8) |
				   (b[3*data[0][i*N +j]] << 16);
      clist[1][i*N + j] = alpha1 | r[3*data[1][i*N +j]] | 
				   (g[3*data[1][i*N +j]] << 8) |
				   (b[3*data[1][i*N +j]] << 16);
      clist[2][i*N + j] = alpha2 | r[3*data[2][i*N +j]] | 
				   (g[3*data[2][i*N +j]] << 8) |
				   (b[3*data[2][i*N +j]] << 16);      vlist[i*N + j][0] = ((float)i - N/2.0)/(N/2.0);
      vlist[i*N + j][1] = ((float)j - N/2.0)/(N/2.0);
    }
/* init. cube wire-frame to outline volume: */
  init_volume();

/*  keepaspect(1,1); */
  winopen("Sblended View____");
/*  subpixel(TRUE); */
  RGBmode();
  lsetdepth(0x0,0x7fffff);
  doublebuffer();
  backface(FALSE);
  gconfig();

  blendfunction(BF_SA,BF_MSA); /* enable blending */

  mmode(MVIEWING);
  perspective(600, 1.0, 0.001, 100.0);
  polarview(4.0,0,0,0); 

  polymode(PYM_FILL);
/*
  qdevice(MOUSEX);
  qdevice(MOUSEY);
  noise(MOUSEX,100);
  noise(MOUSEY,100);
*/
  qdevice(LEFTMOUSE);
  qdevice(MIDDLEMOUSE);

  while( !FALSE )
  {
    cpack(0x0);
    clear();

    event = qtest(); qreset();
    if( event == LEFTMOUSE ) /* mouse is pressed down */
    {
      event = qread(&qdata); /* mouse is released */
      Do_rotate = -Do_rotate;
      Do_translate = -Do_translate;
    }
/* define rotating image plane */
    if( Do_rotate == 1)
    {
      y = getvaluator(MOUSEX);
      x = getvaluator(MOUSEY);
    }
    else if( Do_translate == 1)
      trn = -(getvaluator(MOUSEY) - 512.0)/512.0;
      
    pushmatrix();
    rotate(5*y,'y');
    rotate(5*x,'x');

    draw_volume();

    translate(0.0,0.0,-1.0); /* rear */
    for( i = 0; i < N-1; i++ )
    {
     bgntmesh();
      for( j = 0; j < N; j++ )
      {
        cpack(clist[0][i*N + j]); v3f(vlist[i*N + j]);
        cpack(clist[0][(i+1)*N + j]); v3f(vlist[(i+1)*N + j]);
      } 
     endtmesh();
    }

    translate(0.0,0.0,2.0); 
/* forward 
    for( i = 0; i < N-1; i++ )
    {
     bgntmesh();
      for( j = 0; j < N; j++ )
      {
         cpack(clist[1][i*N + j]);  v3f(vlist[i*N + j]);
         cpack(clist[1][(i+1)*N + j]); v3f(vlist[(i+1)*N + j]);
      } 
     endtmesh();
    }
*/
/* middle */
    translate(0.0,0.0,-1.0); 
    translate(0.0,0.0,trn);
    for( i = 0; i < N-1; i++ )
    {
     bgntmesh();
      for( j = 0; j < N; j++ )
      {
         cpack(clist[2][i*N + j]);  v3f(vlist[i*N + j]);
         cpack(clist[2][(i+1)*N + j]); v3f(vlist[(i+1)*N + j]);
      } 
     endtmesh();
    }

    popmatrix();


/* display it: */
    swapbuffers();
  }
}

init_volume()
{
  volume[0][0] = -1.0;
  volume[0][1] = -1.0;
  volume[0][2] = -1.0;

  volume[1][0] = 1.0;
  volume[1][1] = -1.0;
  volume[1][2] = -1.0;

  volume[2][0] = 1.0;
  volume[2][1] = 1.0;
  volume[2][2] = -1.0;

  volume[3][0] = -1.0;
  volume[3][1] = 1.0;
  volume[3][2] = -1.0;

  volume[4][0] = -1.0;
  volume[4][1] = -1.0;
  volume[4][2] = 1.0;

  volume[5][0] = 1.0;
  volume[5][1] = -1.0;
  volume[5][2] = 1.0;

  volume[6][0] = 1.0;
  volume[6][1] = 1.0;
  volume[6][2] = 1.0;

  volume[7][0] = -1.0;
  volume[7][1] = 1.0;
  volume[7][2] = 1.0;
}     

draw_volume()
{
  cpack(0xffffffff);
  bgnclosedline();
    v3f(volume[0]);
    v3f(volume[1]);
    v3f(volume[2]);
    v3f(volume[3]);
  endclosedline();
  bgnclosedline();
    v3f(volume[4]);
    v3f(volume[5]);
    v3f(volume[6]);
    v3f(volume[7]);
  endclosedline();
  bgnline();
    v3f(volume[0]);
    v3f(volume[4]);
  endline();
  bgnline();
    v3f(volume[1]);
    v3f(volume[5]);
  endline();
  bgnline();
    v3f(volume[2]);
    v3f(volume[6]);
  endline();
  bgnline();
    v3f(volume[3]);
    v3f(volume[7]);
  endline();
}










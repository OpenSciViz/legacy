#include <stdio.h>
#include <gl/gl.h>
#include <gl/device.h>
#include <math.h>

#define N 8
  
float volume[N][3], vlist[N][3], face[4][3], wlist[4][3], 
      tseg1[4][3], tseg2[4][3], tail[5][3], rudder[6][3];
long clist[N];
int i, j;

main()
{
  long alph_bits, evnt;
  long seed=123456789, alpha=0x7f000000;
  short data;
  static float tmp;

  srand48(seed);
/*
  if( (alph_bits = getgdesc(GD_BITS_NORM_DBL_ALPHA)) == 0 )
  {
    fprintf(stderr,"Alpha plane not available on this machine\n");
    return 1;
  }
  else
    fprintf(stderr,"Alpha plane is %d bits deep...\n",alph_bits);
*/
/* initialize vertices with random colors: */
  for( i = 0; i < N; i++ )
  {
      tmp = 16*1024*1024*drand48();
      clist[i] = alpha | (long)tmp;
  }
/* init. cube wire-frame to outline volume: */
  init_volume();

  init_body_seg();

/*  keepaspect(1,1); */
  winopen("Mock View ____ ");
/*  subpixel(TRUE); */
  RGBmode();
  lsetdepth(0x0,0x7fffff);
  zbuffer(TRUE);
  doublebuffer();
  backface(FALSE);
  gconfig();

  blendfunction(BF_SA,BF_MSA); /* enable blending */

  mmode(MVIEWING);
  perspective(600, 1.0, 0.001, 100.0);
  polarview(9.0,0,0,0); 

  polymode(PYM_FILL);
  qdevice(LEFTMOUSE);

  while( !FALSE )
  {
    cpack(0x0);
    clear();
    zclear();

    evnt = qread(&data);
    if( evnt != LEFTMOUSE ) continue;
    draw_volume(); 
    draw_mock();
/* display it: */
    swapbuffers();
  }
}

draw_mock()
{
  pushmatrix();
  scale(0.5,0.5,0.5);
/* define rotating image plane */
  rotate(getvaluator(MOUSEX)*5,'y');
  rotate(getvaluator(MOUSEY)*5,'x');

  translate(-1.0,0.0,0.0); 
/* second tail segment */
  bgntmesh();
    cpack(clist[0]);  v3f(tseg1[0]); 
    cpack(clist[0]);  v3f(tseg2[0]); 
    cpack(clist[2]);  v3f(tseg1[2]); 
    cpack(clist[2]);  v3f(tseg2[2]); 
    cpack(clist[3]);  v3f(tseg1[3]); 
    cpack(clist[3]);  v3f(tseg2[3]); 
    cpack(clist[1]);  v3f(tseg1[1]); 
    cpack(clist[1]);  v3f(tseg2[1]); 
  endtmesh();
  translate(-3.,0.0,0.0); 
  draw_axes();
  translate(3.,0.0,0.0); 
 
/* first tail segment */
  bgntmesh();
    cpack(clist[0]);  v3f(face[0]); 
    cpack(clist[0]);  v3f(tseg1[0]); 
    cpack(clist[2]);  v3f(face[2]); 
    cpack(clist[2]);  v3f(tseg1[2]); 
    cpack(clist[3]);  v3f(face[3]); 
    cpack(clist[3]);  v3f(tseg1[3]); 
    cpack(clist[1]);  v3f(face[1]); 
    cpack(clist[1]);  v3f(tseg1[1]); 
  endtmesh();
  translate(-1.0,0.0,0.0); 
  draw_axes();
  translate(1.0,0.0,0.0); 

/* main fusilage */
  translate(1.0,0.0,0.0); 
  bgntmesh();
    for( i = 0; i < N; i++ )
    {  cpack(clist[i]);  v3f(vlist[i]); }
  endtmesh();
  draw_axes();

  translate(2.0,0.0,0.0); 
  bgntmesh();
    for( i = 0; i < N; i++ )
    { cpack(clist[i]);  v3f(vlist[i]); }
  endtmesh();
  draw_axes();

  translate(2.0,0.0,0.0); /* nearest */
  bgntmesh();
    for( i = 0; i < N; i++ )
    { cpack(clist[i]);  v3f(vlist[i]); }
  endtmesh();
  draw_axes();

  translate(1.0,0.0,0.0); /* close one end */
  bgntmesh();
    for( i = 0; i < 4; i++ )
    { cpack(clist[i]);  v3f(face[i]); }
  endtmesh();

/* wings */
  translate(-3.0,0.0,0.0); /* back to origin */
  bgntmesh();
    cpack(clist[0]); v3f(vlist[0]);
    cpack(clist[1]); v3f(vlist[1]);
    cpack(clist[2]); v3f(wlist[0]);
    cpack(clist[3]); v3f(wlist[1]);
  endtmesh();
  translate(0.0,-4.0,-1.3); 
  draw_axes();
  translate(0.0,4.0,1.3); 
    
  bgntmesh();
    cpack(clist[6]); v3f(vlist[6]);
    cpack(clist[7]); v3f(vlist[7]);
    cpack(clist[2]); v3f(wlist[2]);
    cpack(clist[3]); v3f(wlist[3]);
  endtmesh(); 
  translate(0.0,4.0,-1.3); 
  draw_axes();
  translate(0.0,-4.0,1.3); 

/* tail */
  translate(-6.75,0.0,0.35);
  bgntmesh();
    cpack(clist[0]); v3f(tail[0]);
    cpack(clist[1]); v3f(tail[1]);
    cpack(clist[2]); v3f(tail[2]);
    cpack(clist[3]); v3f(tail[3]);
    cpack(clist[4]); v3f(tail[4]);
  endtmesh();
  translate(6.75,0.0,-0.35);

  popmatrix();
}

init_body_seg()
{
  face[0][0] = 0.0;
  face[0][1] = -0.75;
  face[0][2] = -0.625;
  face[1][0] = 0.0;
  face[1][1] = 0.75;
  face[1][2] = -0.625;
  face[2][0] = 0.0;
  face[2][1] = -0.75;
  face[2][2] = 0.625;
  face[3][0] = 0.0;
  face[3][1] = 0.75;
  face[3][2] = 0.625;

  vlist[0][0] = -1.0;
  vlist[0][1] = -0.75;
  vlist[0][2] = -0.625;
  vlist[1][0] = 1.0;
  vlist[1][1] = -0.75;
  vlist[1][2] = -0.625;
  vlist[2][0] = -1.0;
  vlist[2][1] = -0.75;
  vlist[2][2] = 0.625;
  vlist[3][0] = 1.0;
  vlist[3][1] = -0.75;
  vlist[3][2] = 0.625;
  vlist[4][0] = -1.0;
  vlist[4][1] = 0.75;
  vlist[4][2] = 0.625;
  vlist[5][0] = 1.0;
  vlist[5][1] = 0.75;
  vlist[5][2] = 0.625;
  vlist[6][0] = -1.0;
  vlist[6][1] = 0.75;
  vlist[6][2] = -0.625;
  vlist[7][0] = 1.0;
  vlist[7][1] = 0.75;
  vlist[7][2] = -0.625;

/* wing tips */
  wlist[0][0] = -0.5;
  wlist[0][1] = -0.75 - 6.0;
  wlist[0][2] = -0.625;
  wlist[1][0] = 0.5;
  wlist[1][1] = -0.75 - 6.0;
  wlist[1][2] = -0.625;
  wlist[2][0] = -0.5;
  wlist[2][1] = 0.75 + 6.0;
  wlist[2][2] = -0.625;
  wlist[3][0] = 0.5;
  wlist[3][1] = 0.75 + 6.0;
  wlist[3][2] = -0.625;

/* 2 tapered segment towards tail */
  tseg1[0][0] = -2.0;
  tseg1[0][1] = -0.5;
  tseg1[0][2] = -0.5;
  tseg1[1][0] = -2.0;
  tseg1[1][1] = 0.5;
  tseg1[1][2] = -0.5;
  tseg1[2][0] = -2.0;
  tseg1[2][1] = -0.5;
  tseg1[2][2] = 0.5;
  tseg1[3][0] = -2.0;
  tseg1[3][1] = 0.5;
  tseg1[3][2] = 0.5;

  tseg2[0][0] = -4.0;
  tseg2[0][1] = -0.25;
  tseg2[0][2] = -0.375;
  tseg2[1][0] = -4.0;
  tseg2[1][1] = 0.25;
  tseg2[1][2] = -0.375;
  tseg2[2][0] = -4.0;
  tseg2[2][1] = -0.25;
  tseg2[2][2] = 0.375;
  tseg2[3][0] = -4.0;
  tseg2[3][1] = 0.25;
  tseg2[3][2] = 0.375;

/* tail and rudder */
  tail[0][0] = 0.0;
  tail[0][1] = -2.0;
  tail[0][2] = 0.0;

  tail[1][0] = 0.0;
  tail[1][1] = 2.0;
  tail[1][2] = 0.0;

  tail[2][0] = -0.5;
  tail[2][1] = -2.0;
  tail[2][2] = 0.0;

  tail[3][0] = -0.5;
  tail[3][1] = 2.0;
  tail[3][2] = 0.0;

  tail[4][0] = -1.0;
  tail[4][1] = 0.0;
  tail[4][2] = 0.0;
}     

init_volume()
{
  volume[0][0] = -1.0;
  volume[0][1] = -1.0;
  volume[0][2] = -1.0;

  volume[1][0] = 1.0;
  volume[1][1] = -1.0;
  volume[1][2] = -1.0;

  volume[2][0] = 1.0;
  volume[2][1] = 1.0;
  volume[2][2] = -1.0;

  volume[3][0] = -1.0;
  volume[3][1] = 1.0;
  volume[3][2] = -1.0;

  volume[4][0] = -1.0;
  volume[4][1] = -1.0;
  volume[4][2] = 1.0;

  volume[5][0] = 1.0;
  volume[5][1] = -1.0;
  volume[5][2] = 1.0;

  volume[6][0] = 1.0;
  volume[6][1] = 1.0;
  volume[6][2] = 1.0;

  volume[7][0] = -1.0;
  volume[7][1] = 1.0;
  volume[7][2] = 1.0;
}     

draw_volume()
{
  pushmatrix();
  scale(3.0,3.0,3.0);
  rotate(-900,'x');
  rotate(-900,'z');
  cpack(0xff0000ff); /* red for x axis */
  bgnline();
    v3f(volume[0]);
    v3f(volume[1]);
  endline();
  cpack(0xffffffff);
  bgnline();
    v3f(volume[1]);
    v3f(volume[2]);
  endline();
  bgnline();
    v3f(volume[2]);
    v3f(volume[3]);
  endline();
  cpack(0xff00ff00); /* green for y axis */
  bgnline();
    v3f(volume[3]);
    v3f(volume[0]);
  endline();
  cpack(0xffffffff);
  bgnclosedline();
    v3f(volume[4]);
    v3f(volume[5]);
    v3f(volume[6]);
    v3f(volume[7]);
  endclosedline();
  cpack(0xffff0000); /* blue for z axis */
  bgnline();
    v3f(volume[0]);
    v3f(volume[4]);
  endline();
  cpack(0xffffffff);
  bgnline();
    v3f(volume[1]);
    v3f(volume[5]);
  endline();
  bgnline();
    v3f(volume[2]);
    v3f(volume[6]);
  endline();
  bgnline();
    v3f(volume[3]);
    v3f(volume[7]);
  endline();
  popmatrix();
}

draw_axes()
{
  pushmatrix();
  translate(0.5,0.5,1.2);
  scale(0.5,0.5,0.5);
  rotate(-900,'x');
  rotate(-900,'z');
  cpack(0xff0000ff); /* red for x axis */
  bgnline();
    v3f(volume[0]);
    v3f(volume[1]);
  endline();
  cpack(0xff00ff00); /* green for y axis */
  bgnline();
    v3f(volume[3]);
    v3f(volume[0]);
  endline();
  cpack(0xffff0000); /* blue for z axis */
  bgnline();
    v3f(volume[0]);
    v3f(volume[4]);
  endline();
  popmatrix();
}









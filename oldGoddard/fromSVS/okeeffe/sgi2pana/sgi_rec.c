#include <gl.h>
#include <device.h>
#include <stdio.h>
#include <fcntl.h>
#include <gl/get.h>
#include <gl/cg2vme.h>
#include <gl/device.h>

#define OPEN 1
#define CLOSE -1
#define PANA_PORT "/dev/ttyd2"

void open_window(char *name)
{
 foreground();
 prefposition(4,4+XMAX170,2,2+YMAX170);
 winopen(name);
 RGBmode();
 gconfig();
 cpack(0); clear();
}

switcher( int pan_fd, char cmd)
{

 static char data[4] = {1, '1', 13, 13};
/* pan_fd = pan_open_port("/dev/ttyd2"); */
 data[1] = cmd;
 write(pan_fd,data,4);
/* close(pan_fd); */
}

main(argc,argv)
int argc;
char *argv[];
{
 int i,xdim,ydim,size,dummy,x,y,pan_fd;
 unsigned char bgr=0,bgg=0,bgb=0;
 unsigned long *data, evnt;
 short qval;
 char c, fname[256],line[256];

 if (argc>1) bgr = atoi(argv[1]);
 if (argc>2) bgg = atoi(argv[2]); else bgg = bgr;
 if (argc>3) bgb = atoi(argv[3]); else bgb = bgg;
/* system("gamma 2.2"); */
/* fprintf(stderr,"Wait for Panasonic to enter Record Mode then push Left Mouse...\n"); */
 pan_fd = pan_set_port(OPEN,"/dev/ttyd2");

 open_window(argv[0]);
 if( getvideo(CG_MODE) < 0 )
 {
   fprintf(stderr,"Warning: No genlock installed!\n");
   qdevice(WINQUIT);
 }
 cpack(bgr | bgg << 8 | bgb << 16); clear();
 cpack(0xffffffff);
 cmov2i(40,300);
 charstr("Wait for Panasonic to enter Record Mode then push Left Mouse...");
 cmov2i(200,200);
 charstr("Or hit ESC Key to Quit!");
 qdevice(ESCKEY);
 qdevice(LEFTMOUSE);

 switcher(pan_fd,'I');  /* Record from Iris */
 switcher(pan_fd,'1');  /* Control 3031 */
 pan_online(pan_fd);
 pan_display_on(pan_fd);
 pan_set_recmode(pan_fd);
/* close(pan_fd); */

 while( (evnt = qread(&qval)) != LEFTMOUSE )
 {
   if( evnt == ESCKEY ) 
   {
     pan_clear_recmode(pan_fd);
     pan_offline(pan_fd);
     switcher(pan_fd,'H');
     switcher(pan_fd,'0');
     pan_set_port(CLOSE,PANA_PORT); 
     exit(0);
   }
  }

 cpack(bgr | bgg << 8 | bgb << 16); clear();
 cursoff();
 setmonitor(NTSC);
 setvideo(0,0xa8); /* DE_R1, DER1_G_170 | DER1_UNBLANK */
 setvideo(CG_MODE, CG2_M_MODE0 | CG2_M_HORPHASE | CG2_M_SUBPHASE);
 setvideo(CG_HPHASE,180);
 blanktime(0);
 while (gets(line)) 
 { 
	sscanf(line,"%s",fname);
	if (! read_sgi_image_long(fname,&data,&xdim,&ydim,&dummy)) continue;
	xdim--; ydim--;
	x=(XMAX170-xdim)/2;   y=(YMAX170-ydim)/2;
	lrectwrite(x,y,x+xdim,y+ydim,data);
	pan_record(pan_fd);
	free(data);
 }

 curson();
 blanktime(600*getgdesc(GD_TIMERHZ));
 setmonitor(HZ60);
 pan_clear_recmode(pan_fd);
 pan_offline(pan_fd);
 switcher(pan_fd,'H');
 switcher(pan_fd,'0');
 pan_set_port(CLOSE,PANA_PORT);
}

#include "/usr/people/4Dgifts/iristools/include/image.h"

#define UCHAR unsigned char
#define UINT unsigned long

int read_sgi_image(char *fname,UCHAR **red,UCHAR **green,UCHAR **blue,
		int *xdim,int *ydim,int *zdim)
{
 register IMAGE *image;
 register int x,y;
 UCHAR *r,*g,*b;
 short *buf;
 if ((image=iopen(fname,"r")) == NULL ) {
	fprintf(stderr,"read_sgi_image: can't open input file %s\n",fname);
	return(0);
	}
 *xdim = image->xsize;
 *ydim = image->ysize;
 *zdim = image->zsize;
 buf = (short *) malloc(image->xsize*sizeof(short));
 r = *red = (UCHAR *) malloc(image->xsize*image->ysize);
 if (*zdim>1) g = *green = (UCHAR *) malloc(image->xsize*image->ysize);
 if (*zdim>2) b = *blue = (UCHAR *) malloc(image->xsize*image->ysize);
 for (y=0; y<image->ysize; y++) {
	getrow(image,buf,y,0);
	for (x=0; x<image->xsize; x++) *r++ = buf[x];
	if (*zdim > 1) {
		getrow(image,buf,y,1);
		for (x=0; x<image->xsize; x++) *g++ = buf[x];
		}
	if (*zdim > 2) {
		getrow(image,buf,y,2);
		for (x=0; x<image->xsize; x++) *b++ = buf[x];
		}
	}
 free(buf);
 iclose(image);
 return(1);
}

int read_sgi_image_long(char *fname,UINT **data,int *xdim,int *ydim,int *zdim)
{
 register IMAGE *image;
 register int x,y;
 short *rbuf,*gbuf,*bbuf;
 UINT *p;
 if ((image=iopen(fname,"r")) == NULL ) {
	fprintf(stderr,"read_sgi_image: can't open input file %s\n",fname);
	return(0);
	}
 *xdim = image->xsize;
 *ydim = image->ysize;
 *zdim = image->zsize;
 rbuf = (short *) malloc(image->xsize*sizeof(short));
 if (*zdim>1) gbuf = (short *) malloc(image->xsize*sizeof(short));
 else gbuf = rbuf;
 if (*zdim>2) bbuf = (short *) malloc(image->xsize*sizeof(short));
 else bbuf = gbuf;
 p = *data = (UINT *) malloc(image->xsize*image->ysize*sizeof(UINT));
 for (y=0; y<image->ysize; y++) {
	getrow(image,rbuf,y,0);
	if (*zdim > 1) getrow(image,gbuf,y,1);
	if (*zdim > 2) getrow(image,bbuf,y,2);
	for (x=0; x<image->xsize; x++)
		*p++ = rbuf[x] | (gbuf[x]<<8) | (bbuf[x]<<16) | 0xff000000;
	}
 free(rbuf);
 if (*zdim>1) free(gbuf);
 if (*zdim>2) free(bbuf);
 iclose(image);
 return(1);
}

int write_sgi_image(char *fname,UCHAR *red,UCHAR *green,UCHAR *blue,
		int xdim,int ydim,int zdim)
{
 int i,y;
 unsigned short *sbuf;
 IMAGE *image;
 UCHAR *rp,*gp,*bp;
 sbuf = (unsigned short *) malloc(xdim*2);
 if (!(image = iopen(fname,"w",RLE(1),3,xdim,ydim,zdim))) {
	fprintf(stderr,"write_sgi_image: can't open output file %s\n",fname);
	return(0);
	}
 for (y=0, rp=red, gp=green, bp=blue; y<ydim; y++) {
	for (i=0; i<xdim; i++) sbuf[i] = *rp++;
	putrow(image,sbuf,y,0);
	if (zdim>1) {
		for (i=0; i<xdim; i++) sbuf[i] = *gp++;
		putrow(image,sbuf,y,1);
		}
	if (zdim>2) {
		for (i=0; i<xdim; i++) sbuf[i] = *bp++;
		putrow(image,sbuf,y,2);
		}
	}
 iclose(image);
 free(sbuf);
}

int write_sgi_image_long(char *fname,UINT *data,int xdim,int ydim)
{
 int i,y;
 unsigned short *sbuf;
 IMAGE *image;
 UINT *p,*p1;
 UCHAR *rp,*gp,*bp;
 sbuf = (unsigned short *) malloc(xdim*2);
 if (!(image = iopen(fname,"w",RLE(1),3,xdim,ydim,3))) {
      fprintf(stderr,"write_sgi_image_long: can't open output file %s\n",fname);
      return(0);
      }
 for (y=0, p=data; y<ydim; y++, p+=xdim) {
	for (i=0, p1=p; i<xdim; i++) sbuf[i] = (*p1++) & 0xff;
	putrow(image,sbuf,y,0);
	for (i=0, p1=p; i<xdim; i++) sbuf[i] = ((*p1++) & 0xff00) >> 8;
	putrow(image,sbuf,y,1);
	for (i=0, p1=p; i<xdim; i++) sbuf[i] = ((*p1++) & 0xff0000) >> 16;
	putrow(image,sbuf,y,2);
	}
 iclose(image);
 free(sbuf);
}

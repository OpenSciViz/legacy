#include <stdio.h>
#include <fcntl.h>
#include <termio.h>

#define OPEN 1
#define CLOSE -1

int pan_set_port(int mode, char *fname)
{
 int fd= -1;
 static struct termio orig, raw;

 switch(mode)
 {
   case OPEN:
 	if ((fd = open(fname,O_RDWR)) == -1) {
	perror(fname);
	exit();
	}
 	ioctl(fd,TCGETA,&orig);
 	ioctl(fd,TCGETA,&raw);
 	raw.c_cflag = B1200 | CS8 | CLOCAL | CREAD;
 	raw.c_lflag &= ~ICANON;
 	raw.c_lflag &= ~ECHO;
 	raw.c_cc[VMIN] = 1; /* present each character as soon as it shows up */
 	raw.c_cc[VTIME] = 1; /* 0.1 second for timeout */
 	ioctl(fd,TCSETAF,&raw);
	break;

   case CLOSE:
	if( fd < 0 ) {
/*	printf("pan_set_port> CLOSE request on UNOPEN fd!!\n"); */
	return;
	}
	ioctl(fd,TCSETAF,&orig);
	close(fd);
	fd = -1;
	break;
 }
	
 return(fd);
}

void pan_online(int pan_fd)
{
 static unsigned char buf[7] = { 2, 'O', 'N', '@', 8, ':', 3};
 write(pan_fd,buf,7);
}

void pan_offline(int pan_fd)
{
 static unsigned char buf[5] = { 2, 'O', 'F', '@', 3};
 write(pan_fd,buf,5);
}

void pan_display_on(int pan_fd)
{
 static unsigned char buf[4] = { 2, 'D', 'S', 3};
 write(pan_fd,buf,4);
}

void pan_display_off(int pan_fd)
{
 static unsigned char buf[4] = { 2, 'D', 'R', 3};
 write(pan_fd,buf,4);
}

void pan_search(int pan_fd,int frame)
{
 static unsigned char buf[80] = { 2, 'S', 'R' };
 sprintf(buf+3,"%d:%c",frame,3);
 write(pan_fd,buf,strlen(buf));
}

void pan_f_play(int pan_fd,int frame)
{
 static unsigned char buf[80] = { 2, 'P', 'F' };
 if (frame>-1) sprintf(buf+3,"%d:%c",frame,3);
 else sprintf(buf+3,"%c",3);
 write(pan_fd,buf,strlen(buf));
}

void pan_r_play(int pan_fd,int frame)
{
 static unsigned char buf[80] = { 2, 'P', 'R' };
 if (frame>-1) sprintf(buf+3,"%d:%c",frame,3);
 else sprintf(buf+3,"%c",3);
 write(pan_fd,buf,strlen(buf));
}

void pan_f_fast(int pan_fd,int speed,int frame)
{
 static unsigned char buf[80] = { 2, 'F', 'F' };
 int len;
 sprintf(buf+3,"%d",speed);
 len=strlen(buf);
 if (frame>-1) sprintf(buf+len,":%d:%c",frame,3);
 else sprintf(buf+len,":%c",3);
 write(pan_fd,buf,strlen(buf));
}

void pan_r_fast(int pan_fd,int speed,int frame)
{
 static unsigned char buf[80] = { 2, 'F', 'R' };
 int len;
 sprintf(buf+3,"%d",speed);
 len=strlen(buf);
 if (frame>-1) sprintf(buf+len,":%d:%c",frame,3);
 else sprintf(buf+len,":%c",3);
 write(pan_fd,buf,strlen(buf));
}

void pan_f_slow(int pan_fd,int speed,int frame)
{
 static unsigned char buf[80] = { 2, 'L', 'F' };
 int len;
 sprintf(buf+3,"%d",speed);
 len=strlen(buf);
 if (frame>-1) sprintf(buf+len,":%d:%c",frame,3);
 else sprintf(buf+len,":%c",3);
 write(pan_fd,buf,strlen(buf));
}

void pan_r_slow(int pan_fd,int speed,int frame)
{
 static unsigned char buf[80] = { 2, 'L', 'R' };
 int len;
 sprintf(buf+3,"%d",speed);
 len=strlen(buf);
 if (frame>-1) sprintf(buf+len,":%d:%c",frame,3);
 else sprintf(buf+len,":%c",3);
 write(pan_fd,buf,strlen(buf));
}

void pan_f_step(int pan_fd,int speed,int frame)
{
 static unsigned char buf[80] = { 2, 'T', 'F' };
 int len;
 if (speed>-1) sprintf(buf+3,"%d",speed);
 else sprintf(buf+3,"1");
 len=strlen(buf);
 if (frame>-1) sprintf(buf+len,":%d:%c",frame,3);
 else sprintf(buf+len,":+1:%c",3);
 write(pan_fd,buf,strlen(buf));
}

void pan_r_step(int pan_fd,int speed,int frame)
{
 static unsigned char buf[80] = { 2, 'T', 'R' };
 int len;
 if (speed>-1) sprintf(buf+3,"%d",speed);
 else sprintf(buf+3,"1");
 len=strlen(buf);
 if (frame>-1) sprintf(buf+len,":%d:%c",frame,3);
 else sprintf(buf+len,":-1:%c",3);
 write(pan_fd,buf,strlen(buf));
}

void pan_f_scan(int pan_fd)
{
 static unsigned char buf[4] = { 2, 'C', 'F', 3};
 write(pan_fd,buf,4);
}

void pan_r_scan(int pan_fd)
{
 static unsigned char buf[4] = { 2, 'C', 'R', 3};
 write(pan_fd,buf,4);
}

void pan_set_recmode(int pan_fd)
{
 static unsigned char buf[4] = { 2, 'R', 'M', 3};
 write(pan_fd,buf,4);
}

void pan_clear_recmode(int pan_fd)
{
 static unsigned char buf[4] = { 2, 'R', 'C', 3};
 write(pan_fd,buf,4);
}

void pan_record(int pan_fd)
{
 int i;
 static unsigned char buf[6] = { 2, 'G', 'S', '1', ':', 3};
 static unsigned char ret_buf[7];
/* write(pan_fd,buf,6); */
 for( i = 0; i < 6; i++ )
 {
   ret_buf[i] = 0;
   write(pan_fd,&buf[i],1);
 }
/* check panasonic's response: */
 for( i = 0; i < 6 && ret_buf[i] != 3; i++ )
   read(pan_fd,&ret_buf[i],1);
 fprintf(stderr,"pan_record> panasonic response: %s\n",ret_buf);
 if( ret_buf[0] == 'E' || ret_buf[0] == 'e' ) 
 {
   for( i = 0; i < 6; i++ )
   {
     ret_buf[i] = 0;
     write(pan_fd,&buf[i],1);
   }
   for( i = 0; i < 6 && ret_buf[i] != 3; i++ )
     read(pan_fd,&ret_buf[i],1);
   fprintf(stderr,"pan_record> panasonic response: %s\n",ret_buf);
 }
 else
   return; 
 if( ret_buf[0] == 'E' || ret_buf[0] == 'e' ) 
 {
   for( i = 0; i < 6; i++ )
   {
     ret_buf[i] = 0;
     write(pan_fd,&buf[i],1);
   }
   for( i = 0; i < 6 && ret_buf[i] != 3; i++ )
     read(pan_fd,&ret_buf[i],1);
   fprintf(stderr,"pan_record> panasonic response: %s\n",ret_buf);
 }
 else
   return; 

 fprintf("pan_record> failed after 3 attempts, error= %s\n",ret_buf); 

}

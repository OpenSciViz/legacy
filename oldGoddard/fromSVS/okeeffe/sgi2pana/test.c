#include <stdio.h>
#include <fcntl.h>
#include <termio.h>

main()
{
 int fd;
 static struct termio orig, raw, test;
 
 fd = open("/dev/ttyd2",O_RDWR);
	
 ioctl(fd,TCGETA,&orig);
 ioctl(fd,TCGETA,&raw);

 raw.c_cflag = B1200 | CS8 | CLOCAL | CREAD;
 raw.c_lflag &= ~ICANON;
 raw.c_lflag &= ~ECHO;
 raw.c_cc[VMIN] = 1; /* present each character as soon as it shows up */
 raw.c_cc[VTIME] = 1; /* 0.1 second for timeout */

 ioctl(fd,TCSETAF,&raw);
 ioctl(fd,TCGETA,&test);

 ioctl(fd,TCSETAF,&orig);

 close(fd);
}

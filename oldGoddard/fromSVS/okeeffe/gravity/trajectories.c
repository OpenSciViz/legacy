int trajectories(np,ns,pnt,tnp)
int np, ns;
float pnt[][3], tnp[][3];
{
  int i, k;

  cpack(0xffffffff);

  for( i = 0 ; i < np; i++ )
  {
/*    bgnpoint();
*/
    bgnline();
      for( k = 0; k < ns; k++ ) v3f(pnt[i*ns + k]);
/*    endpoint();
*/
    endline();
    bgnline();
      for( k = 0; k < ns; k++ ) v3f(tnp[i*ns + k]);
    endline();
  }
} 

#include <math.h>
/*
#define DEBUG
*/
photon(i,mG,v1,v2,v3,v4)
int i;
float mG;
float v1[3], v2[3], v3[3], v4[3];
{
#ifdef DEBUG
  if( fabs(v1[0]) < 0.5 && fabs(v1[1]) < 0.5 && fabs(v1[2]) < 0.5 ) printf("photon> zero vertex1 : i= %d\n",i);
  if( fabs(v2[0]) < 0.5 && fabs(v2[1]) < 0.5 && fabs(v2[2]) < 0.5 ) printf("photon> zero vertex2 : i= %d\n",i);
  if( fabs(v3[0]) < 0.5 && fabs(v3[1]) < 0.5 && fabs(v3[2]) < 0.5 ) printf("photon> zero vertex3 : i= %d\n",i);
  if( fabs(v4[0]) < 0.5 && fabs(v4[1]) < 0.5 && fabs(v4[2]) < 0.5 ) printf("photon> zero vertex4 : i= %d\n",i);
#endif
  bgnpolygon();
    cpack(0xff0ff0ff);
    v3f(v1);
    v3f(v2);
    v3f(v3);
    v3f(v4);
  endpolygon();
}

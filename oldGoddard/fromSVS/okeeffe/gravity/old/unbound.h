/* this simply returns phi(r), given phi(infinity), r0, and m using the weinburg
   formula on page 190 of "Gravitation and Cosmology" */

#include <stdio.h>
#include <math.h>
#define G 1.475 /* km */
#define TRUE 1

#ifdef MAIN
#define global 
#else
#define global extern
#endif

global float initial_position, cm[3];
global float dist, delt;
global int xrot, yrot, zrot, delrot, look_dir;
global char do_rotate, do_zoom;


#define MAIN
#include "unbound.h"
#define NP 512

main(argc,argv)
int argc; char **argv;
{
  float m=1.0, phi_inf=0.0, r0[4];
  int i, k, l, n, p;
  float theta=0.02, r[4][NP], phi[4][NP];
  float pnt[8][NP][3], tnp[8][NP][3];
  static float rot[3][3], rot_z[3][3], rot_y[3][3];
  float phi_unbound();
  int graphics();

  if( argc > 1 ) m = atof(argv[1]);
  if( argc > 2 ) phi_inf = atof(argv[2]);

  init_obj(2,m,phi_inf,r0);

/* init rotation matrix */
  rot[0][0] = cos(theta);
  rot[0][2] = sin(theta);
  rot[1][1] = 1;
  rot[2][0] = -sin(theta);
  rot[2][2] = cos(theta);

  for( i = 0; i < 2; i++ )
  {
    for( k = 0; k < NP; k++ )
    {
      r[i][k] = r0[i] + initial_position - (k * initial_position/NP);
      phi[i][k] = phi_unbound(m,phi_inf,r0[i],r[i][k]);
/* construct trajectories of the particles in the plane defined by theta=0 */
      pnt[i][k][0] = r[i][k] * cos(phi[i][k]);
      pnt[i][k][1] = r[i][k] * sin(phi[i][k]);
      pnt[i][k][2] = 0;
    }
  }
/* use symmetry to calculate the remainder of the trajectories */
  other_half(2,NP,pnt,tnp);
/* construct trajectories of the particles in the plane defined by theta=? */
  for( i = 0; i < 2; i++ )
  {
    for( k = 0; k < NP; k++ )
    {
      pnt[i+2][k][0] = rot[0][0]*pnt[i][k][0] + rot[0][1]*pnt[i][k][1] +
      				rot[0][2]*pnt[i][k][2];
      pnt[i+2][k][1] = rot[1][0]*pnt[i][k][0] + rot[1][1]*pnt[i][k][1] +
      				rot[1][2]*pnt[i][k][2];
      pnt[i+2][k][2] = rot[2][0]*pnt[i][k][0] + rot[2][1]*pnt[i][k][1] +
      				rot[2][2]*pnt[i][k][2];

      tnp[i+2][k][0] = rot[0][0]*tnp[i][k][0] + rot[0][1]*tnp[i][k][1] +
      				rot[0][2]*tnp[i][k][2];
      tnp[i+2][k][1] = rot[1][0]*tnp[i][k][0] + rot[1][1]*tnp[i][k][1] +
      				rot[1][2]*tnp[i][k][2];
      tnp[i+2][k][2] = rot[2][0]*tnp[i][k][0] + rot[2][1]*tnp[i][k][1] +
      				rot[2][2]*tnp[i][k][2];
    }
  }
/* display the trajectories? */
  graphics(4,NP,m,pnt,tnp);
}




#include "unbound.h"
#include <gl.h>
#include <device.h>

#define IN    1
#define OUT   2
#define X_AX  1
#define Y_AX  2
#define Z_AX  3

int get_event()
{
long an_event; short ev_data;
static char did_zoom, did_rot;
static short mouse_ev=1;

#ifdef DEBUG
printf("waiting for event in que\n");
#endif
      if( !qtest() && mouse_ev == -1 )
      {
	if( do_rotate )
	{
	  if( did_rot == X_AX ) xrot += delrot;
	  if( did_rot == Y_AX ) yrot += delrot;
	  if( did_rot == Z_AX ) zrot += delrot;
	}
	else if( do_zoom )
	{
	  if( did_zoom == IN ) dist -= delt;
	  if( did_zoom == OUT ) dist += delt;
	}
	return;
      }
      else if( !qtest() )
	return;

      an_event = qread(&ev_data);

      if( (an_event == LEFTMOUSE) || (an_event == MIDDLEMOUSE) ||
	  (an_event == RIGHTMOUSE) ) 
	mouse_ev = -mouse_ev;
      if( an_event == CKEY )
      {
	  delrot = -delrot;
          qreset();
	  an_event = qread(&ev_data);
      }

      if( an_event == ZKEY )
      {
#ifdef DEBUG
printf("Zoom key event\n");
#endif
	 do_rotate = FALSE; do_zoom = TRUE;
         qreset();
	 an_event == qread(&ev_data);
         if( (an_event == LEFTMOUSE) || (an_event == MIDDLEMOUSE) ||
	     (an_event == RIGHTMOUSE) ) 
	      mouse_ev = -mouse_ev;
	 if( an_event != RKEY )
         {
            if( an_event == LEFTMOUSE ) /* zoom in closer */
            {
		dist = dist - delt; did_zoom = IN;  
            }
	    else if( an_event == MIDDLEMOUSE ) /* zoom out */
            {
		dist = dist + delt; did_zoom = OUT;
            }
	    else if( an_event == RIGHTMOUSE ) /* toggle look direction */
            {
		look_dir = -look_dir;
            }
	 }
      }
      else if( an_event == RKEY )
      {
#ifdef DEBUG
printf("Rotate key event\n");
#endif
	 do_rotate = TRUE; do_zoom = FALSE;
         qreset();
	 an_event == qread(&ev_data);
         if( (an_event == LEFTMOUSE) || (an_event == MIDDLEMOUSE) ||
	     (an_event == RIGHTMOUSE) ) 
	      mouse_ev = -mouse_ev;
	 if( an_event != RKEY )
         {
            if( an_event == LEFTMOUSE ) /* rotate around z axis*/
            {
		zrot += delrot; did_rot = Z_AX;
	    }
	    else if( an_event == MIDDLEMOUSE ) /* rotate arou und x axis*/
	    {
		xrot += delrot; did_rot = X_AX;
	    }
	    else if( an_event == RIGHTMOUSE ) /* rotate arou und x axis*/
	    {
		yrot += delrot; did_rot = Y_AX;
	    }
	 }
      }
      else if( an_event == LEFTMOUSE )
      {
	  if( do_rotate )
          {
#ifdef DEBUG
printf("Z rotate\n");
#endif
	      zrot += delrot; did_rot = Z_AX;
          }
          if( do_zoom ) 
          {
#ifdef DEBUG
printf("Zoom in\n");
#endif
		dist = dist - delt; did_zoom = IN;
	  }
      } 
      else if( an_event == MIDDLEMOUSE )
      {
	  if( do_rotate )
          {
#ifdef DEBUG
printf("X rotate\n");
#endif
              xrot += delrot; did_rot = X_AX;
          }
          if( do_zoom ) 
          {
#ifdef DEBUG
printf("Zoom out\n");
#endif
		dist = dist + delt; did_zoom = OUT;
	  }
      } 
      else if( an_event == RIGHTMOUSE )
      {
	  if( do_rotate )
          {
#ifdef DEBUG
printf("X rotate\n");
#endif
              yrot += delrot; did_rot = Y_AX;
          }
          if( do_zoom ) 
          {
#ifdef DEBUG
printf("Zoom out\n");
#endif
		dist = dist + delt; did_zoom = OUT;
	  }
      } 
}


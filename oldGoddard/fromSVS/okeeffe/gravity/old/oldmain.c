#define MAIN
#include "unbound.h"
#define NP 200

main(argc,argv)
int argc; char **argv;
{
  float m=1.0, phi_inf=0.0, r0[4];
  int i, k, l, n, p;
  float theta=0.2, r[4][NP], phi[4][NP];
  float pnt[8][NP][3], tnp[8][NP][3];
  static float rot[3][3], rot_z[3][3], rot_y[3][3];
  float phi_unbound();
  int graphics();

  if( argc > 1 ) m = atof(argv[1]);
  if( argc > 2 ) phi_inf = atof(argv[2]);

  init_obj(2,m,phi_inf,r0);

/* init rotation matrix */
  rot_y[0][0] = cos(theta);
  rot_y[0][2] = sin(theta);
  rot_y[1][1] = 1;
  rot_y[2][0] = -sin(theta);
  rot_y[2][2] = cos(theta);

  rot_z[2][2] = 1;

  for( i = 0; i < 2; i++ )
  {
    for( k = 0; k < NP; k++ )
    {
      r[i][k] = r0[i] + initial_position - (k * initial_position/NP);
      phi[i][k] = phi_unbound(m,phi_inf,r0[i],r[i][k]);
/* construct trajectories of the particles in the plane defined by theta=0 */
      pnt[i][k][0] = r[i][k] * cos(phi[i][k]);
      pnt[i][k][1] = r[i][k] * sin(phi[i][k]);
      pnt[i][k][2] = 0;
/* construct trajectories of the particles in the plane defined by theta=? */
      rot_z[0][0] = cos(phi[i][k]);
      rot_z[0][1] = sin(phi[i][k]);
      rot_z[1][0] = -sin(phi[i][k]);
      rot_z[1][2] = cos(phi[i][k]);
      for( l = 0; l < 3; l++ )
        for( n = 0; n < 3; n++ )
          rot[l][n] = rot_y[l][0]*rot_z[0][n] + rot_y[l][1]*rot_z[1][n] +
      			rot_y[l][2]*rot_z[2][n];

      pnt[i+2][k][0] = rot[0][0]*pnt[i][k][0] + rot[0][1]*pnt[i][k][1] +
      				rot[0][2]*pnt[i][k][2];
      pnt[i+2][k][1] = rot[1][0]*pnt[i][k][0] + rot[1][1]*pnt[i][k][1] +
      				rot[1][2]*pnt[i][k][2];
      pnt[i+2][k][2] = rot[2][0]*pnt[i][k][0] + rot[2][1]*pnt[i][k][1] +
      				rot[2][2]*pnt[i][k][2];
    }
  }
/* use symmetry to calculate the remainder of the trajectories */
/*
  other_half(2,NP,r0,pnt,tnp);
*/

/* display the trajectories? */
  graphics(4,NP,pnt,tnp);
}




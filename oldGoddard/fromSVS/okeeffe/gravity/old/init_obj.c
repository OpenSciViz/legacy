#include "unbound.h"

init_obj(nt,m,phi_inf,r0)
int nt;
float m, phi_inf, r0[];
{
  int i;
  float v[4][3];
  float r0_ref, phi_ref[4], r_ref[4];
  float find_r0();
  float size;

/*  
  phi_inf = 0.0;
  m = 1.0;
*/
  r0_ref = 3.0 * m*G;
  initial_position = 10.0 * r0_ref;
  size = 2*r0_ref;

/* use 8 values of (r,phi) to define vertices of a cube, all placed on 
   phi_inf=0 curves, in a vicinity sufficiently far away from the singularity
   where the metric is flat */
   
/* first init a unit cube vertices */
  v[0][0] = size;
  v[0][1] = 0.6*size;
  v[0][2] = 0;

  v[1][0] = size;
  v[1][1] = 0.5*size;
  v[1][2] = 0;

  v[2][0] = 0.5*size;
  v[2][1] = 0.5*size;
  v[2][2] = 0;

  v[3][0] = 0.5*size;
  v[3][1] = size;
  v[3][2] = 0;

/* translate the cube to an initial position ~100 * r0, along phi=0 axis */
  for( i = 0; i < nt; i++ )
      v[i][0] = v[i][0] + initial_position;

/* calculate the 8 phi_ref and r_ref values for the cube vertices */
  for( i = 0; i < nt; i++ )
  {
    phi_ref[i] = atan(v[i][1] / v[i][0]);
    r_ref[i] = sqrt( v[i][0]*v[i][0] + v[i][1]*v[i][1] + v[i][2]*v[i][2] );
  }

/* for each of these r,phi pairs, find best estimate for r0 */
  for( i = 0; i < nt; i++ )
    r0[i] = find_r0(m,phi_inf,r_ref[i],phi_ref[i]);
}



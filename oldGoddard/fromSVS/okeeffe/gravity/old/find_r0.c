#include "unbound.h"

float find_r0(m,phi_inf,r,phi)
float m, phi_inf, r, phi;
{
  int i=1;
/* use a simple iteration procedure, if this fails, consider a variation 
   Newton - Raphson ... */
  float r0, new_r0, del, new_del;

  r0 = i * m*G;
  new_r0 = r * sin( phi - phi_inf - 
      		    m*G/r0*( 2 - sqrt(1-r0*r0/r/r) - sqrt( (r-r0)/(r+r0) ) ) );

  del = fabs( (new_r0 - r0) / (new_r0 + r0) );
  r0 = new_r0;
  while( TRUE )
  {
    new_r0 = r * sin( phi - phi_inf - 
      		    m*G/r0*( 2 - sqrt(1-r0*r0/r/r) - sqrt( (r-r0)/(r+r0) ) ) );
    new_del = fabs( (new_r0 - r0) / (new_r0 + r0) );
    if( new_r0 < 0 || new_del > del ) /* try a different initial guess */
    {
      i++;
      r0 = i * m*G; 
    }
    else if( new_del < 0.001 )
      return( new_r0 );
    else
      r0 = new_r0;
    
    del = new_del;
  }
}



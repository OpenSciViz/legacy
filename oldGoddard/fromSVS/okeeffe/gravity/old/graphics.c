#include "unbound.h"
#include <gl.h>
#include <device.h>
#define CENTER 1

static float ident[4][4] = { {1.0, 0.0, 0.0, 0.0}, 
			     {0.0, 1.0, 0.0, 0.0}, 
			     {0.0, 0.0, 1.0, 0.0}, 
			     {0.0, 0.0, 0.0, 1.0} };
short wrred = 0xffff, wrgrn = 0xffff, wrblu = 0xffff;
long rot_ang=50;

int graphics(nt,np,m,pnt,tnp)
int nt, np;
float m, pnt[][3], tnp[][3];
{
 int i=0, cnt=0, circles(), trajectories(), tri_mesh();
 int x0 = 16, y0 = 8;

   dist = 2.0 * initial_position;
   delt = dist / 50.0;
   delrot = 10;
   do_rotate = TRUE;
   look_dir = CENTER;

   prefposition(x0,x0+988,y0,y0+988);
   keepaspect(1,1);
   winopen("Unbound Photon Orbits");
   RGBmode();
   RGBwritemask(wrred,wrgrn,wrblu);
   drawmode(NORMALDRAW);
   doublebuffer();
   gconfig();

   mmode(MVIEWING);
   perspective(450,1.0,0.01,10000.0);
   loadmatrix(ident);
/*   polarview(dist,10,450,0); */
   lookat(0.0,0.0,dist,0.0,0.0,0.0,0);

   lsetdepth(0,0x7fffff);
   zbuffer(TRUE);
/*   backface(TRUE); */

   qdevice(ZKEY);
   qdevice(RKEY);
   qdevice(YKEY);
   qdevice(CKEY);
   qdevice(LEFTMOUSE);
   qdevice(MIDDLEMOUSE);
   qdevice(RIGHTMOUSE);
   unqdevice(MOUSEX);
   unqdevice(MOUSEY);
   while( TRUE )
   {
      zclear();
      cpack(0);
      clear();
      if( ++i >= 2*np ) i = 0;

      get_event();

      pushmatrix();

      loadmatrix(ident);
/*      polarview(dist,10,450,0); */
      if( look_dir == CENTER )
        lookat(0.0,0.0,dist,0.0,0.0,0.0,0);
      else
	lookat(0.0,0.0,dist,cm[0],cm[1],cm[2],0.0);

      if( zrot >= 3600 ) zrot = 0;
      rotate(zrot,'z');
      if( xrot >= 3600 ) xrot = 0;
      rotate(xrot,'x');
      if( yrot >= 3600 ) yrot = 0;
      rotate(yrot,'y');

      trajectories(nt,np,pnt,tnp);
      circles(m*G);
      if( i < np )
      {
	cm[0] = pnt[i][0];
	cm[1] = pnt[i][1];
	cm[2] = pnt[i][2];
        photon(m*G,pnt[i],pnt[2*np+i],pnt[3*np+i],pnt[np+i]);
      }
      else
      {
	cm[0] = tnp[2*np-i][0];
	cm[1] = tnp[2*np-i][1];
	cm[2] = tnp[2*np-i][2];
        photon(m*G,tnp[2*np-i],tnp[4*np-i],tnp[5*np-i],tnp[3*np-i]);
      }
      popmatrix();
      swapbuffers(); 
   }
}

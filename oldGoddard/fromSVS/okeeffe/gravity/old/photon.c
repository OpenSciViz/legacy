photon(mG,v1,v2,v3,v4)
float mG;
float v1[3], v2[3], v3[3], v4[3];
{
  bgnpolygon();
    cpack(0xff0ff0ff);
    v3f(v1);
    v3f(v2);
    v3f(v3);
    v3f(v4);
  endpolygon();
}

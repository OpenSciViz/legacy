#include "unbound.h"
#include <gl.h>

circles(mG)
float mG;
{

   cpack(0xff00ffff);
   circ(0.0,0.0,mG);
   cpack(0xff0000ff);
   circ(0.0,0.0,2*mG);
   cpack(0xffff00ff);
   circ(0.0,0.0,3*mG);
   cpack(0xffff0000);
   circ(0.0,0.0,4*mG);
   cpack(0xffffff00);
   circ(0.0,0.0,5*mG);
   cpack(0xff00ff00);
   circ(0.0,0.0,6*mG);
}

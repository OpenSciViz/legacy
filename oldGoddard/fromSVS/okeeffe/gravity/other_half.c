#include <math.h>
#include <stdio.h>
#include "unbound.h"
#define DEBUG

int other_half(nt,np,pnt,tnp)
int nt, np;
float pnt[][3], tnp[][3];
{
/* given the array of points pnt[np*np+1][3] and the vectors r0[np],
   rotate each point pnt 180 degs. about the r0 axes to create the
   tnp array of points.
*/
  float mag, phi;
  static float rot_z[3][3], rot_x[3][3], rot[3][3], foo[3][3];
  float test[3];
  int i, j, k;

/* at this point it shouldn't matter if we discard some information about r0s */
for( k = 0; k < nt; k++ )
{
  phi = atan2(pnt[k*np + np-1][1],pnt[k*np + np-1][0]);
  rot_z[0][0] = cos(phi); 
  rot_z[0][1] = sin(phi);
  rot_z[1][0] = -rot_z[0][1];
  rot_z[1][1] = rot_z[0][0];
  rot_z[2][2] = 1.0;
#ifdef DEBUG
  test[0] = rot_z[0][0]*pnt[k*np + np-1][0] + rot_z[0][1]*pnt[k*np + np-1][1] +
		rot_z[0][2]*pnt[k*np + np-1][2];
  test[1] = rot_z[1][0]*pnt[k*np + np-1][0] + rot_z[1][1]*pnt[k*np + np-1][1] +
		rot_z[1][2]*pnt[k*np + np-1][2];
  test[2] = rot_z[2][0]*pnt[k*np + np-1][0] + rot_z[2][1]*pnt[k*np + np-1][1] +
		rot_z[2][2]*pnt[k*np + np-1][2];
#endif
  rot_x[0][0] = 1.0;
  rot_x[2][2] = -1.0;
  rot_x[1][1] = -1.0;

  for( i = 0; i < 3; i++ )
  {
    for( j = 0; j < 3; j++ )
    {
	foo[i][j] = rot_x[i][0]*rot_z[0][j] +
			rot_x[i][1]*rot_z[1][j] +
			rot_x[i][2]*rot_z[2][j];
    }
  }
  rot_z[0][1] = -rot_z[0][1];
  rot_z[1][0] = -rot_z[0][1];

  for( i = 0; i < 3; i++ )
  {
    for( j = 0; j < 3; j++ )
    {
	rot[i][j] = rot_z[i][0]*foo[0][j] +
			rot_z[i][1]*foo[1][j] +
			rot_z[j][2]*foo[2][j];
    }
  }

  for( j = 0; j < np; j++ )
  {
	tnp[k*np+j][0] = rot[0][0]*pnt[k*np+j][0] +
				rot[0][1]*pnt[k*np+j][1] +
				rot[0][2]*pnt[k*np+j][2];

	tnp[k*np+j][1] = rot[1][0]*pnt[k*np+j][0] +
				rot[1][1]*pnt[k*np+j][1] +
				rot[1][2]*pnt[k*np+j][2];

	tnp[k*np+j][2] = rot[2][0]*pnt[k*np+j][0] +
				rot[2][1]*pnt[k*np+j][1] +
				rot[2][2]*pnt[k*np+j][2];
  }
}
}

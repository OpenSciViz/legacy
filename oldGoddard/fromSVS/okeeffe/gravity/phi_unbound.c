#include "unbound.h"

float phi_unbound(m,phi_inf,r0,r)
float m, phi_inf, r0, r;
{
  float phi;

  phi = phi_inf + asin(r0/r) + 
        m*G/r0 * (2 - sqrt(1 - (r0/r)*(r0/r)) - sqrt( (r-r0)/(r+r0) ));

  return( phi );
}


/* copied directly from the new "graphics library programmers guide" v3.2
   operating systsem */

#include <gl/gl.h>

float octdata[6][3] = {
	{1.0, 0.0, 0.0},
	{0.0, 1.0, 0.0},
	{0.0, 0.0, 1.0},
	{-1.0, 0.0, 0.0},
	{0.0, -1.0, 0.0},
	{0.0, 0.0, -1.0}
};

drawocto()
{
  bgntmesh();

  color(1);
  v3f(octdata[0]);

  color(2);
  v3f(octdata[1]);

  swaptmesh();

  color(3);
  v3f(octdata[2]);

  swaptmesh();

  color(4);
  v3f(octdata[4]);

  swaptmesh();

  color(5);
  v3f(octdata[5]);

  swaptmesh();

  color(6);
  v3f(octdata[1]);

  color(7);
  v3f(octdata[3]);

  color(8);
  v3f(octdata[2]);

  swaptmesh();

  color(9);
  v3f(octdata[4]);

  swaptmesh();
  
  color(10);
  v3f(octdata[5]);

  swaptmesh();
  
  color(11);
  v3f(octdata[1]);

  endtmesh();
}


main()
{
  long iang, jang, kang;
  long exitcounter = 0;
  Screencoord x1, x2, y1, y2;
  char version[12];
  static long parray[512*512];
  static RGBvalue red[512], grn[512], blu[512];
  Colorindex colors[512];

  gversion(version);
  printf("version= %s\n",version);

  x1 = y1 = 10; x2 = y2 = 410;
  prefposition(x1,x2,y1,y2);
/*  prefposition(100,500,100,500); */
  winopen("octahedron");
 /*  ortho(-2.0, 2.0, -2.0, 2.0, -2.0, 2.0); */
  perspective(350,1.0,0.01,500.0);
  polarview(5,0,0,0);
/*  zbuffer(TRUE); */
/*  doublebuffer(); */
/*  RGBmode(); */
  gconfig();

  while( exitcounter < 999 ) 
  {
    pushmatrix();
    rotate(iang,'x');
    rotate(jang,'y');
    rotate(kang,'z');
    iang += 10;
    jang += 13;
    if( iang + jang > 3000 ) kang += 17;
    if( iang > 3600 ) iang -= 3600;
    if( jang > 3600 ) jang -= 3600;
    if( kang > 3600 ) kang -= 3600;
    cpack(0);
    zclear();

    drawocto();
    if( exitcounter++ < 1 )
    {
	int n= -1;
/*	readsource(SRC_BACK); */
	readsource(SRC_FRONT);
/*        n = readRGB(2,red,grn,blu); */
	n = readpixels(2,colors);
/*	lrectread(x1,y1,x2,y2,parray); 
	save_img("img.octo.red","img.octo.grn","img.octo.blu",x2-x1+1,
			y2-y1+1,parray); */
    } 
    swapbuffers();
    popmatrix();
  }
}

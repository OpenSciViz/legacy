#include "gl.h"
#include "device.h"

#define CIRCLE 1
#define RECT   2
#define LINE   3

int mainmenu;
int colormenu;
int shapemenu;
int curcolor;
int curshape;
int xorg, yorg;

setshape(n)
int n;
{
  curshape = n;
  return( -1 ); /* dopup will return this value */
}

setcolor(n)
int n;
{
  curcolor = n;
  return( 7 ); /* dopup will return this value */
}

main(argc,argv)
int argc; char **argv;
{
short val;
int x, y;
int pupval;

  prefsize(400,300);
  winopen("blop");
  curcolor = WHITE;
  curshape = CIRCLE;
  qdevice(LEFTMOUSE);
  qdevice(MENUBUTTON);
  shapemenu = defpup("shapes %t %F|Circle|Rect|Line",setshape);
  colormenu = defpup("colors %t %F|Blue %x4|White %x7|Red %x1",setcolor);
  mainmenu = defpup("blop %t|shapes %m|color %m| %m|clear|set",shapemenu,
							       colormenu);

  makeframe();

  while( 1 )
  {
    switch( qread(&val) )
    {
      case REDRAW: makeframe();
		   break;

      case LEFTMOUSE: if( val ) 
		      {
			x = getvaluator(MOUSEX) - xorg;
			y = getvaluator(MOUSEY) - yorg;
			drawshape(curcolor,curshape,x,y);
		      }
		      break;
      
      case MENUBUTTON: if( val )
		       {
			 pupval = dopup(mainmenu);
			 printf("dopup return %d\n",pupval);
			 switch(pupval)
			 {
			   case 3: color(BLACK);  /* clear */
				   clear();
				   break;

			   case 4: color(WHITE);  /* set */
				   clear();
				   break;

			   case 5: string = dialog();
				   break;
			 }
			}

      default: break;
    }
  }
}

drawshape(acolor,ashape,x,y)
int acolor, ashape, x, y;
{
  color(acolor);
  switch(ashape)
  {
    case CIRCLE: circfi(x,y,10);
		 break;

    case RECT: rectfi(x,y,x+15,y+10);
	       break;

    case LINE: move2i(x,y);
	       draw2i(x+20,y+20);
	       break;
  }
}

makeframe()
{
  reshapeviewport();
  getorigin(&xorg,&yorg);
  color(BLACK);
  clear();
}


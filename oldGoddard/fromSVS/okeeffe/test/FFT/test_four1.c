#include <stdio.h>
#include <math.h>  
#define t_MAX 0.0
#define NMAX 1024
#define LMAX 1024

typedef struct complex { float r; float i; } COMPLEX;

main()
{
  static float array[LMAX*NMAX]; /* compiler inits to 0 */
  static COMPLEX C_array[LMAX*NMAX];

  int i,j, m;
  int Ndim[2];

  Ndim[0] = NMAX;
  Ndim[1] = LMAX;
  array[LMAX/2*NMAX + NMAX/2] = 1.0;

  Imagin(LMAX,NMAX,array,C_array); /* generate a complex array */
  for( i = 0; i< LMAX; i++ )
    four1((&C_array[i*NMAX].r - 1),NMAX,1); /* complex to complex */
}

int Imagin(L,N,a,c)
int L, N;
float a[];
COMPLEX c[];
{
  int i, j, n;

  n = L*N;
  for( i = N-1; i >= 0; i-- )
  {
    for( j = L-1; j >=0 ; j-- )
    {
      --n;
      c[n].r = 0.0;
      c[n].i = a[i*L+j];
    }
  }
}

int Cexp(n,c)
int n;
COMPLEX c[];
{
  float tmp;

  while( --n >= 0 )
  {
    if( c[n].r == 0.0 && c[n].i == 0.0 )
    {
/* do nothing, this is the either a central element or an edge element
   that is "zero padded" for the fft */
    }
    else
    {  
      tmp = exp(c[n].r);
      c[n].r = tmp * cos(c[n].i);
      c[n].i = tmp * sin(c[n].i);
    }
  }
}

int CRmul(n,c,a)
int n;
COMPLEX c[];
float a[][NMAX];
{
  int i, j, N;
  COMPLEX mean;

  N = n;
  mean.r = 0.0;
  mean.i = 0.0;

  n = n*n;
  for( i = N-1; i >= 0; i-- )
  {
    for( j = N-1; j >=0 ; j-- )
    {
      --n;
      c[n].r = a[i][j] * c[n].r;
      c[n].i = a[i][j] * c[n].i;
    }
  }
/* and take the opportunity to remove any DC component 
  for( i = 0; i < N*N; i++ )
  {
    mean.r = mean.r + c[i].r;
    mean.i = mean.i + c[i].i;
  }
  mean.r = mean.r / (N-1);
  mean.i = mean.i / (N-1);
  for( i = 0; i < N; i++ )
  {
    c[i].r = c[i].r - mean.r;
    c[i].i = c[i].i - mean.i;
  }
*/

  return;
}

int Real(n,c,a)
int n;
COMPLEX c[];
float a[][NMAX];
{
  int i, j, N;

  N = n;
  n = n*n;
  for( i = N-1; i >= 0; i-- )
    for( j = N-1; j >=0 ; j-- )
      a[i][j] = c[--n].r; /* / N / N; */
}

int ICmulX(n,r,c1,c2)
int n;
float r[];
COMPLEX c1[], c2[];
{
  int i, j;

  for( i = 0; i < n; i++ )
  {
    for( j = 0; j < n; j++ )
    {
      c2[i*n + j].r = -r[i]*c1[i*n + j].i;
      c2[i*n + j].i = r[i]*c1[i*n + j].r;
    }
  }
  return;
}

int ICmulY(n,r,c1,c2)
int n;
float r[];
COMPLEX c1[], c2[];
{
  int i, j;

  for( i = 0; i < n; i++ )
  {
    for( j = 0; j < n; j++ )
    {
      c2[i*n + j].r = -r[j]*c1[i*n + j].i;
      c2[i*n + j].i = r[j]*c1[i*n + j].r;
    }
  }
  return;
}

#include <stdio.h>
#include <math.h>

main(argc,argv,env)
int argc;
char **argv, **env;
{
  int i;
  float tot;

  tot = atof(argv[1]);
  for( i = 1; i <= atoi(argv[2]); i++ )
    tot = ( 1.0 + atof(argv[3]) ) * tot + atof(argv[1]);

  printf("total = %f\n",tot);
}

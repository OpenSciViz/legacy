
int fp1()
{
  printf("hello 1\n");
  return( 1 );
}

int fp2()
{
  printf("hello 2\n");
  return( 2 );
}

int fp3()
{
  printf("hello 3\n");
  return( 3 );
}

void fun(n,fp)
int n;
int (*fp)();
{
  int stat;

  printf("fun> now calling function # %d\n",n);

  stat = fp();

  printf("fun> status returned from function= %d\n",stat);
}

main(argc,argv,env)
int argc;
char **argv, **env;
{
/*  int (*fp1)(), (*fp2)(), (*fp3)(); */
  void fun();

  fun(1,fp1);
  fun(2,fp2);
  fun(3,fp3);
}

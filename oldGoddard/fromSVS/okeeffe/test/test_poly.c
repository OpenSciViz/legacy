#include <stdio.h>
#include <gl.h>
#include <device.h>
#include <math.h>
static float ident[4][4] = { {1.0, 0.0, 0.0, 0.0}, 
			     {0.0, 1.0, 0.0, 0.0}, 
			     {0.0, 0.0, 1.0, 0.0}, 
			     {0.0, 0.0, 0.0, 1.0} };
short wrred = 0xffff, wrgrn = 0xffff, wrblu = 0xffff;
float dist = 5.0, delt = 0.1;

main(int argc, char **argv, char **env)
{
  float tverts[2*128][3], bverts[2*128][3];
  int i, j, zr=0;

  for( i = 0; i <= 2*64; i++ )
  {
    tverts[i][0] = (i - 2*64.0)/2/64.0;
    tverts[i][1] = 0.0;
    tverts[i][2] = tverts[i][0];
  }
  for( i = 65; i <= 2*127; i++ )
  {
    tverts[i][0] = (i - 2*64.0)/2/64.0;
    tverts[i][1] = 0.0;
    tverts[i][2] = -tverts[i][0];
  }

  for( i = 0; i <= 2*64; i++ )
  {
    bverts[i][0] = (i - 2*64.0)/2/64.0;
    bverts[i][1] = 1.0;
    bverts[i][2] = bverts[i][0];
  }
  for( i = 65; i <= 2*127; i++ )
  {
    bverts[i][0] = (i - 2*64.0)/2/64.0;
    bverts[i][1] = 1.0;
    bverts[i][2] = -bverts[i][0];
  }

  prefposition(8,645,8,485);
  winopen(argv[0]);
  RGBmode();
  RGBwritemask(wrred,wrgrn,wrblu);
  drawmode(NORMALDRAW);
  doublebuffer();
  gconfig();

  mmode(MVIEWING);
  perspective(450,1.0,0.01,100.0);
  loadmatrix(ident);
  polarview(dist,10,800,0);
  lsetdepth(0,0x7fffff);
  zbuffer(TRUE);
  backface(FALSE);
  concave(TRUE);

  while( 1 )
  {
    zclear();
    cpack(0);
    clear();
    pushmatrix();
    rotate(zr,'z');
    if( zr++ > 3600 ) zr= 0;
    bgnpolygon();
      cpack(0x00ffff);
      for( i = 0; i <= 2*127; i++ )
	v3f(tverts[i]);
      cpack(0xff00ff);
      for( i = 2*127; i >= 0; i-- )
        v3f(bverts[i]);
    endpolygon();
    popmatrix();
    swapbuffers();
  }
}

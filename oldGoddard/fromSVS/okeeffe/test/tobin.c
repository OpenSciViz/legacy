/*
 *	tobin - 
 *		Convert an Iris image to binary dump format.
 *
 *				Paul Haeberli - 1986
 */
#include <gl/image.h>

short rowbuf[4096]; 
char charbuf[4096]; 

main(argc,argv)
int argc;
char **argv;
{
    register IMAGE *image;
    register FILE *outf;
    register int xsize;
    register int i, y, ysize;
    register int z, zsize;

    if( argc<2 ) {
	fprintf(stderr,"usage: tobin inimage out.bin\n");
	exit(1);
    } 
    if( (image=iopen(argv[1],"r")) == NULL ) {
	fprintf(stderr,"tobin: can't open input file %s\n",argv[1]);
	exit(1);
    }
    if( (outf=fopen(argv[2],"w")) == NULL ) {
	fprintf(stderr,"tobin: can't open output file %s\n",argv[1]);
	exit(1);
    }
    xsize = image->xsize;
    ysize = image->ysize;
printf("tobin> xsize= %d,  ysize= %d\n",xsize,ysize);
    zsize = image->zsize;
    for(z=0; z<zsize; z++) {
	for(y=0; y<ysize; y++) {
	    getrow(image,rowbuf,y,z);
	    stoc(rowbuf,charbuf,xsize);
	    fwrite(charbuf,1,xsize,outf);
	}
    }
    fclose(outf);
}

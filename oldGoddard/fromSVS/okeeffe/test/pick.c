#include <gl.h>
#include <device.h>

void drawit()
{
  color(RED);
  loadname(1);
  rectfi(20,20,100,100);
  loadname(2);
  pushname(21);
  circi(50,500,50);
  popname();
  pushname(22);
  circi(50,530,60);
  popname();
}

main()
{
short dev, val;
short buffer[50];
int hits, xsize, ysize,  i;

  prefsize(600,600);
  winopen("pick");
  getsize(&xsize,&ysize);
  color(BLACK);
  clear();

  qdevice(LEFTMOUSE);
  qdevice(ESCKEY);

  for( i=0; i<50; i++ ) buffer[i] = 0;

  drawit();

  while( 1 )
  {
    dev = qread(&val);

    switch( dev )
    {
	case LEFTMOUSE:  /* if( val == 0 ) break; */
			 pick(buffer, 50);
/* the ortho2 call is necessary to define the size of the pick area, without
   this call the entire window is used, and all objects are always picked! */
			 ortho2(-0.5,xsize+0.5,-0.5,ysize+0.5); 
			 drawit(); /* no actual drawing takes place */
			 hits = endpick(buffer);
			 show_hits(hits,buffer);
			 break;

	case ESCKEY:  return;

	default: break;
    }
  }
}

show_hits(hits,buf)
int hits;
short buf[];
{
int index, h, i, items;

  printf("hit count: %d\n",hits);
  index = 0;

  for( h=0; h < hits; h++ )
  {
    items = buf[index++];
    printf("(");
    for( i=0; i<items; i++ )
    {
	if( i != 0 ) printf(" ");
	printf("%d", buf[index++]);
    }
    printf(")");
  }
  printf("\n");
  return;
}

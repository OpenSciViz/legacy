/*
**
**  Copyright (C) 1989,1990,1991 Ultra Network Technologies, Inc.
**
**  This file is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY.  No author or distributor accepts
**  responsibility to anyone for the consequences of using it or for
**  whether it serves any particular purpose or works at all, unless
**  he says so in writing.
** 
**  Everyone is granted permission to copy, modify and redistribute
**  this file, as long as the copyright notice, the warranty
**  disclaimer (above), and these terms granting copy and
**  redistribution rights are preserved on all copys.
**
**  $Id: sample1.c,v 1.1 91/02/28 12:41:17 shj Exp $
*/

/*
 * This is the "sample 1" program from the Ultra Frame Buffer User's Guide
 */

#include <stdio.h>
#include <ultra/ugraf.h>

struct UG_PARAM p;

struct UG_TBLK   t;

#define HSIZE    644
#define VSIZE    484

struct pixel_pair {
        unsigned char   unused1,
                        blue1,
                        green1,
                        red1,
                        unused2,
                        blue2,
                        green2,
                        red2;
} *pix;
unsigned char r[HSIZE*VSIZE], g[HSIZE*VSIZE], b[HSIZE*VSIZE];
extern char *malloc();

main()
{
        int     i,
                sts;

/*      Allocate memory for pixels   */

        pix = (struct pixel_pair *)malloc(HSIZE*VSIZE*
                                                sizeof(struct pixel_pair)/2);
        if(pix == NULL) {
                fprintf(stderr, "malloc for pix failed\n");
                exit(1);
        }

	read(0,r,HSIZE*VSIZE);
	read(0,g,HSIZE*VSIZE);
	read(0,b,HSIZE*VSIZE);
/*      Fill all pixels */
        for(i = 0; i < HSIZE*VSIZE/2; i += 2) {
                pix[i].blue1  = b[i];
                pix[i].green1 = g[i];
                pix[i].red1   = r[i];
                pix[i].blue2  = b[i+1];
                pix[i].green2 = g[i+1];
                pix[i].red2   = r[i+1];
        }

/*      Open "default" Frame Buffer, with Sony monitor */

        p.term_type = UG_NTSC; /* UG_SONY; */
        sts = ugraf(UG_OPEN, &p);
        if(sts != UGE_OK) {
                fprintf(stderr, "ugraf open failed, status %d\n", sts);
                exit(1);
        }

/*      Set up ugraf parameters to write pixels to screen */

        t.tx = 0;            /* starting X coordinate (upper left) for pixels */
        t.ty = 0;            /* starting Y coordinate (upper left) for pixels */
        t.npixel = HSIZE;    /* # pixels per line */
        t.nline  = VSIZE;    /* # lines per screen */
        t.addr = (int *)pix; /* address of pixel buffer */ 
        p.link = &t;         /* link TBLK to main PARAMS block */
        p.buf_ctl = UG_SW;   /* switch buffers after write */

/*      Write pixels to Frame Buffer  */

        sts = ugraf(UG_WRITE, &p);  /* initiate write  */
        if(sts != UGE_OK) {
                fprintf(stderr, "ugraf write failed, status %d\n", sts);
                exit(1);
        }

/*      Done, close FB   */

        sts = ugraf(UG_CLOSE, &p);
        if(sts != UGE_OK) {
                fprintf(stderr, "ugraf close operation failed, status %d\n",
                                        sts);
                exit(1);
        }

        exit(0);        /* Normal termination */

}

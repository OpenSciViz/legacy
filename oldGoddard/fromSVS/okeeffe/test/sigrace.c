#include <signal.h>

sigcatch()
{
  printf("pid %d caught one\n",getpid());

  signal(SIGINT,sigcatch);
}

main()
{
  int i, stat=0, ppid;

  signal(SIGINT,sigcatch);

  if( ppid = fork() == 0 )
  {
    sleep(5);
    ppid = getppid();
    i = ppid;
    while( --i > 0 )
    {
      if( kill(ppid,SIGINT) == -1 )
	 exit();
      sleep(1);
    }
  }
  else
  {
    nice(10);
    sleep(100);
/*    while( 1 )
	; */
  }
}


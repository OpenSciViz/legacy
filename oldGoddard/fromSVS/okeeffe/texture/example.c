#include <stdio.h>
#include <gl/gl.h>
#include <gl/device.h>

/* Texture environment */
float tevprops[] = {TV_MODULATE, TV_NULL};

/* 1D RGBA texture map representing temperatures as color and opacity */

float texheat[] = {TX_WRAP, TX_CLAMP, TX_NULL};

/* black->blue->cyan->green->yellow->red->white */

unsigned long heat[] = /* Translucent -> Opaque */
{0x0, 0x55ff0000, 0x77ffff00, 0x9900ff00,
 0xbb00ffff, 0xdd0000ff, 0xffffffff};

/* Point sampled 1 componen cheskerboard texture */
float texbgd[] = {TX_MAGFILTER, TX_POINT, TX_NULL};
unsigned long check[] = { 0xff800000, /* notice row byte padding */
		          0x80ff0000};

/* Subdivision parameters */
float scrparams[] = {0., 0., 10.};

/* define texture and vertex coordinates */
float t0[] = {0.,0.}, v0[] = {-2.,-4.,0.};
float t1[] = {.4,0.}, v1[] = {2.,-4.,0.};
float t2[] = {1.,0.}, v2[] = {2.,4.,0.};
float t3[] = {.7,0.}, v3[] = {-2.,4.,0.};

main()
{
  if( getgdesc(GD_TEXTURE) == 0 )
  {
    fprintf(stderr,"texture mapping not available on this machine\n");
    return 1;
  }
  keepaspect(1,1);
  winopen("heat");
  subpixel(TRUE);
  RGBmode();
  lsetdepth(0x0,0x7fffff);
  doublebuffer();
  gconfig();

  blendfunction(BF_SA,BF_MSA); /* enable blending */

  mmode(MVIEWING);
  perspective(600, 1, 1., 16.);

/* define sheckerboard */
  texdef2d(1,1,2,2,check,0,texbgd);
/* define heat */
  texdef2d(2,4,7,1,heat,0,texheat);
  tevdef(1,0,tevprops);
  tevbind(TV_ENV0,1);

  translate(0.,0.,-6.);

  while( !getbutton(LEFTMOUSE) )
  {
    cpack(0x0);
    clear();

    scrsubdivide(SS_OFF, scrparams); /* Subdivision off */
    texbind(TX_TEXTURE_0, 1);
    cpack(0xff102040); /* background rectangle */

    bgnpolygon();
      t2f(v0); v3f(v0); /* vertex coordinates used as texture coordinates */
      t2f(v1); v3f(v1);
      t2f(v2); v3f(v2);
      t2f(v3); v3f(v3);
    endpolygon();

    pushmatrix();
    rotate(getvaluator(MOUSEX)*5,'y');
    rotate(getvaluator(MOUSEY)*5,'x');

/* Subdivision on */
    scrsubdivide(SS_DEPTH, scrparams);
    texbind(TX_TEXTURE_0,2); /* Bind heat */
    cpack(0xffffffff); /* heated recangle base color */

    bgnpolygon(); /* draw textured rectangle */
      t2f(t0); v3f(v0); 
      t2f(t1); v3f(v1);
      t2f(t2); v3f(v2);
      t2f(t3); v3f(v3);
    endpolygon();

    popmatrix();

    swapbuffers();
  }
  texbind(TX_TEXTURE_0, 0); /* turn off texturing */
}













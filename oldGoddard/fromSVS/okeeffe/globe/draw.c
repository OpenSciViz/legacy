#include "globe.h"

float bpnt[4][3] = { { -1.5, -1.5, -1.2  }, { 1.5, -1.5, -1.2 },
      		     { 1.5, 1.5, -1.2 }, { -1.5, 1.5, -1.2 } };
float white[3] = { 1.0, 1.0, 1.0 };
float gray[3] = { 0.5, 0.5, 0.5 };
float red[3] = { 1.0, 0.0, 0.0 };
float green[3] = { 0.0, 1.0, 0.0 };
float blue[3] = { 0.0, 0.0, 1.0 };
float yellow[3] = { 1.0, 1.0, 0.0 };
float magenta[3] = { 1.0, 0.0, 1.0 };
float cyan[3] = { 0.0, 1.0, 1.0 };
float orbcolr[3] = { 0.0, 1.0, 0.0, };
float ident[4][4] = { {1.0, 0.0, 0.0, 0.0}, 
			     {0.0, 1.0, 0.0, 0.0}, 
			     {0.0, 0.0, 1.0, 0.0}, 
			     {0.0, 0.0, 0.0, 1.0} };
long arot = 0, trot = 0; 
char rotaxis = 'z';
float dist = 5.0, delt = 0.1;
float sunpos[3] = { 0.87, 0.5, 0.0 };

float *draw_orbit()
{
   static int nseg=1;
   int i;
   float *orbpos;

   if( nseg < 256 ) 
      nseg++; 
   else
   {
      arot = arot + 50;
      if( arot > 3600 ) arot = 0;
      nseg = 1;
   }
   pushmatrix();
   loadmatrix(ident);
   polarview(dist,10,800,0);
/*   rotate(arot,rotaxis); */
   bgnline();
     for( i = 0; i < nseg; i++ )
     {
        c3f(red); v3f(orbpnt[i]); 
     }
   endline();
   popmatrix();
   orbpos = &orbpnt[nseg][0];
   return( orbpos );
}

int draw_bottom()
{
   pushmatrix();
   loadmatrix(ident);
   polarview(dist,10,800,0);
   
   bgnpolygon();
   c3f(gray); v3f(bpnt[0]); 
   c3f(gray); v3f(bpnt[1]); 
   c3f(gray); v3f(bpnt[2]); 
   c3f(gray); v3f(bpnt[3]);
   endpolygon();

   popmatrix();
}

int draw_face(orbpos)
float orbpos[];
{
   int itri, inc, cnr;
   float dotprod[3], sundot[3], globcolr[3];
   static float matrx[3][3], orbrot[3][3];
   float vrtx[3][3], orb[3];
   char swtch;

/*   getmatrix(matrx);  ASSUME INVERSE IS TRANSPOSE */
/* need to apply inverse rotation Rz(-trot) before evaluating dot
   products */
   matrx[0][0] = cos(trot/573.0);
   matrx[1][0] = sin(trot/573.0);
   matrx[0][1] = -matrx[1][0];
   matrx[1][1] = matrx[0][0];
   matrx[2][2] = 1.0;

   orbrot[0][0] = cos(arot/573.0);
   orbrot[1][0] = sin(arot/573.0);
   orbrot[0][1] = -orbrot[1][0];
   orbrot[1][1] = orbrot[0][0];
   orbrot[2][2] = 1.0;
/*
   orb[0] = ( orbrot[0][0]*orbpos[0] + orbrot[0][1]*orbpos[1] +
		orbrot[0][2]*orbpos[2] ) / 1.5;
   orb[1] = ( orbrot[1][0]*orbpos[0] + orbrot[1][1]*orbpos[1] +
		orbrot[2][2]*orbpos[2] ) / 1.5;
   orb[2] = ( orbrot[2][0]*orbpos[0] + orbrot[2][1]*orbpos[1] +
		orbrot[2][2]*orbpos[2] ) / 1.5;
*/
   for( itri = 0; itri < ntri; itri++ )
   {
      for( cnr = 0; cnr < 3; cnr++ )
      {
         vrtx[cnr][0] = matrx[0][0]*triang[itri].v[cnr][0] +
		   matrx[0][1]*triang[itri].v[cnr][1] +
		   matrx[0][2]*triang[itri].v[cnr][2];
         vrtx[cnr][1] = matrx[1][0]*triang[itri].v[cnr][0] +
		   matrx[1][1]*triang[itri].v[cnr][1] +
		   matrx[1][2]*triang[itri].v[cnr][2];
         vrtx[cnr][2] = matrx[2][0]*triang[itri].v[cnr][0] +
		   matrx[2][1]*triang[itri].v[cnr][1] +
		   matrx[2][2]*triang[itri].v[cnr][2];
         dotprod[cnr] =  ( vrtx[cnr][0] * orbpos[0] + 
      		          vrtx[cnr][1] * orbpos[1] + 
      		          vrtx[cnr][2] * orbpos[2] ) / 1.25; 
         sundot[cnr] =  ( vrtx[cnr][0] * sunpos[0] + 
      		          vrtx[cnr][1] * sunpos[1] + 
      		          vrtx[cnr][2] * sunpos[2] ); 
      }

      if( dotprod[0] > 0.995 || dotprod[1] > 0.995 || dotprod[2] > 0.995
          || dotprod[3] > 0.995 ) 
	 swtch = TRUE;
      else
	 swtch = FALSE;

      bgnpolygon();
	 if( swtch )
	    c3f(orbcolr);
	 else if( sundot[0] < 0.0 )
 	 {
	    globcolr[0] = 0.5 * pntcolor[triang[itri].id[0]][0];
	    globcolr[1] = 0.5 * pntcolor[triang[itri].id[0]][1];
	    globcolr[2] = 0.5 * pntcolor[triang[itri].id[0]][2];
            c3f(globcolr); 
         }
	 else
	    c3f(pntcolor[triang[itri].id[0]]); 
	 v3f(triang[itri].v[0]);
	 if( swtch )
	    c3f(orbcolr);
	 else if( sundot[0] < 0.0 )
 	 {
	    globcolr[0] = 0.5 * pntcolor[triang[itri].id[1]][0];
	    globcolr[1] = 0.5 * pntcolor[triang[itri].id[1]][1];
	    globcolr[2] = 0.5 * pntcolor[triang[itri].id[1]][2];
            c3f(globcolr); 
         }
	 else
            c3f(pntcolor[triang[itri].id[1]]); 
	 v3f(triang[itri].v[1]);
	 if( swtch )
	    c3f(orbcolr);
	 else if( sundot[0] < 0.0 )
 	 {
	    globcolr[0] = 0.5 * pntcolor[triang[itri].id[2]][0];
	    globcolr[1] = 0.5 * pntcolor[triang[itri].id[2]][1];
	    globcolr[2] = 0.5 * pntcolor[triang[itri].id[2]][2];
            c3f(globcolr); 
         }
	 else
            c3f(pntcolor[triang[itri].id[2]]); 
	 v3f(triang[itri].v[2]);
      endpolygon();
/*
      bgntmesh();
         c3f(pntcolor[triang[itri].id[0]]); v3f(triang[itri].v[0]);
         c3f(pntcolor[triang[itri].id[1]]); v3f(triang[itri].v[1]);
	swaptmesh();
         c3f(pntcolor[triang[itri].id[2]]); v3f(triang[itri].v[2]);
         c3f(pntcolor[triang[itri].id[3]]); v3f(triang[itri].v[3]);
      endtmesh();
*/
   }
}

int draw_sphere(orbpos)
float orbpos[];
{
   pushmatrix();
   trot += 50;
   if( trot > 3600 )
      trot = 0;
   rotate(trot,rotaxis);
   draw_face(orbpos);
   popmatrix();
}


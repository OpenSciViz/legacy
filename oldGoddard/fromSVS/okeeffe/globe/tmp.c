
int init_color_info(globemap,pix)
float *globemap;
int *pix;
{
int i, *pixptr, nglobeval=1;
float globemin, globemax;
float *globeptr, rabs();

 if( globemap != NULL )	/* must set pntcolor according to globe values */
 {
    globeptr = globemap;
    pixptr = pix;

   globeptr = globemap;
   for( i = 0; i < NP; i++ )
   { 
/*      if( *pixptr++ < 0 )
      {
         pntcolor[i][0] = 0.1;
         pntcolor[i][1] = 0.1;
         pntcolor[i][2] = 0.1;
      }
*/
      if( *globeptr == 0 )
      {
            pntcolor[i][0] = 0.1;
            pntcolor[i][1] = 0.1;
            pntcolor[i][2] = 1.0;
      }
      else
      {
	pntcolor[i][0] = 0.6; /* 0.1 + 0.5 * exp(*globeptr - 64882.0); */
	pntcolor[i][1] = 0.666 * pntcolor[i][0];
	pntcolor[i][2] = 0.2; /* + 0.8 * exp( - (*globeptr) ); */
      } 
      globeptr++;
   }
   

 }
 else /* no globemap, just image the pixel numbers */
 {
   for( i = 0; i < NP; i++ )
   {
      if( i < NP/6 )
      {
            pntcolor[i][0] = (255 - (i % 256))/255.0;
            pntcolor[i][1] = 0.0;
       	    pntcolor[i][2] = 0.0;
      }
      else if( i < 2*NP/6 )
      {
            pntcolor[i][0] = 0.0;
            pntcolor[i][1] = (255 - (i % 256))/255.0;
       	    pntcolor[i][2] = 0.0;
      }
      else if( i < 3*NP/6 )
      {
            pntcolor[i][0] = 0.0;
            pntcolor[i][1] = 0.0;
       	    pntcolor[i][2] = (255 - (i % 256))/255.0;
      }
      else if( i < 4*NP/6 )
      {
            pntcolor[i][0] = (255 - (i % 256))/255.0;
            pntcolor[i][1] = (255 - (i % 256))/255.0;
       	    pntcolor[i][2] = 0.0;
      }
      else if( i < 5*NP/6 )
      {
            pntcolor[i][0] = (255 - (i % 256))/255.0;
            pntcolor[i][1] = 0.0;
       	    pntcolor[i][2] = (255 - (i % 256))/255.0;
      }
      else if( i < NP )
      {
            pntcolor[i][0] = 0.0;
            pntcolor[i][1] = (255 - (i % 256))/255.0;
       	    pntcolor[i][2] = (255 - (i % 256))/255.0;
      }
   }
  }
}


#include "globe.h"

int init_orbit()
{
   int i;
   float pi=3.1415926, pi2;

   pi2 = 2.0*pi;
   for( i = 0; i < 256; i++ )
   {
	orbpnt[i][0] = 1.25 * sin(pi2 * i / 256.0)*cos(1.65*pi);
	orbpnt[i][1] = 1.25 * sin(pi2 * i / 256.0)*sin(1.65*pi); 
	orbpnt[i][2] = 1.25 * cos(pi2 * i / 256.0);
   } 
}

int init_triang_info(maxobs,nobs,wght)
int maxobs, nobs[]; float wght[];
{
   int i, j, nval, neighb[8];
   float cenp[3], cornp[3], strch;
   int upx_8_neighbors(), upx_pixel_vector();
   char doit;
   int pixtyp;
   short res;

   doit = FALSE;
   strch = 1.0;
   ntri = 0;
   res = RES;
 
   for( i = 0; i < NP; i++ )
   {
      pixtyp = upx_8_neighbors(&i,&res,neighb,&nval);
      if( (i % 4) == 3 ) doit = TRUE;
      if( (pixtyp <= 0) && (i >= 5.0*pow(4.0,res - 1.0)) ) doit = TRUE;
      if( doit )
      {
         upx_pixel_vector(&i,&res,cenp); /* Center pixel */
	 if( pixtyp == 0 )
/*	    printf("edge pixel: %d\n",i); */
	 if( pixtyp == -1 )
	    printf("corner pixel: %d\n",i);
/*     	 if( nval < 8 ) printf("upx_8_neighbors returns: %d\n",nval); */
/*	assume neighbors are ordered: BL, B, BR, R, TR, T, TL */

         /* form triangle with BL, B, C */
      	    upx_pixel_vector(&neighb[0],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[0]] / maxobs;
      	    triang[ntri].id[0] = neighb[0];
      	    triang[ntri].v[0][0] = strch * cornp[0];
      	    triang[ntri].v[0][1] = strch * cornp[1];
      	    triang[ntri].v[0][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[1],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[1]] / maxobs;
      	    triang[ntri].id[1] = neighb[1];
      	    triang[ntri].v[1][0] = strch * cornp[0];
      	    triang[ntri].v[1][1] = strch * cornp[1];
      	    triang[ntri].v[1][2] = strch * cornp[2];

      	    triang[ntri].id[2] = i;
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[i] / maxobs;
            triang[ntri].v[2][0] = strch * cenp[0];
            triang[ntri].v[2][1] = strch * cenp[1];
            triang[ntri].v[2][2] = strch * cenp[2];

	    ntri++;
         /* form triangle with C, L, BL */
      	    triang[ntri].id[0] = i;
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[i] / maxobs;
            triang[ntri].v[0][0] = strch * cenp[0];
            triang[ntri].v[0][1] = strch * cenp[1];
            triang[ntri].v[0][2] = strch * cenp[2];

      	    upx_pixel_vector(&neighb[7],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[7]] / maxobs;
      	    triang[ntri].id[1] = neighb[7];
      	    triang[ntri].v[1][0] = strch * cornp[0];
      	    triang[ntri].v[1][1] = strch * cornp[1];
      	    triang[ntri].v[1][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[0],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[7]] / maxobs;
      	    triang[ntri].id[2] = neighb[0];
      	    triang[ntri].v[2][0] = strch * cornp[0];
      	    triang[ntri].v[2][1] = strch * cornp[1];
      	    triang[ntri].v[2][2] = strch * cornp[2];

	    ntri++;
         /* form triangle with B, BR, R */
      	    upx_pixel_vector(&neighb[1],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[1]] / maxobs;
      	    triang[ntri].id[0] = neighb[1];
      	    triang[ntri].v[0][0] = strch * cornp[0];
      	    triang[ntri].v[0][1] = strch * cornp[1];
      	    triang[ntri].v[0][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[2],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[2]] / maxobs;
      	    triang[ntri].id[1] = neighb[2];
      	    triang[ntri].v[1][0] = strch * cornp[0];
      	    triang[ntri].v[1][1] = strch * cornp[1];
      	    triang[ntri].v[1][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[3],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[3]] / maxobs;
      	    triang[ntri].id[2] = neighb[3];
      	    triang[ntri].v[2][0] = strch * cornp[0];
      	    triang[ntri].v[2][1] = strch * cornp[1];
      	    triang[ntri].v[2][2] = strch * cornp[2];

	    ntri++;
         /* form triangle with R, C, B */
      	    triang[ntri].id[0] = neighb[3];
      	    triang[ntri].v[0][0] = strch * cornp[0];
      	    triang[ntri].v[0][1] = strch * cornp[1];
      	    triang[ntri].v[0][2] = strch * cornp[2];

      	    triang[ntri].id[1] = i;
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[i] / maxobs;
            triang[ntri].v[1][0] = strch * cenp[0];
            triang[ntri].v[1][1] = strch * cenp[1];
            triang[ntri].v[1][2] = strch * cenp[2];

      	    upx_pixel_vector(&neighb[1],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[1]] / maxobs;
      	    triang[ntri].id[2] = neighb[1];
      	    triang[ntri].v[2][0] = strch * cornp[0];
      	    triang[ntri].v[2][1] = strch * cornp[1];
      	    triang[ntri].v[2][2] = strch * cornp[2];

	    ntri++;

      	 if( nval < 8 ) /* corners are short a vertex (TR), form only 3 more 
			triangles */
	 {
         /* form triangle with C, R, T */
      	    triang[ntri].id[0] = i;
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[i] / maxobs;
            triang[ntri].v[0][0] = strch * cenp[0];
            triang[ntri].v[0][1] = strch * cenp[1];
            triang[ntri].v[0][2] = strch * cenp[2];

      	    upx_pixel_vector(&neighb[3],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[3]] / maxobs;
      	    triang[ntri].id[1] = neighb[3];
      	    triang[ntri].v[1][0] = strch * cornp[0];
      	    triang[ntri].v[1][1] = strch * cornp[1];
      	    triang[ntri].v[1][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[4],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[4]] / maxobs;
      	    triang[ntri].id[2] = neighb[4];
      	    triang[ntri].v[2][0] = strch * cornp[0];
      	    triang[ntri].v[2][1] = strch * cornp[1];
      	    triang[ntri].v[2][2] = strch * cornp[2];

	    ntri++;
         /* form triangle with L, C, T */
      	    upx_pixel_vector(&neighb[6],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[6]] / maxobs;
      	    triang[ntri].id[0] = neighb[6];
      	    triang[ntri].v[0][0] = strch * cornp[0];
      	    triang[ntri].v[0][1] = strch * cornp[1];
      	    triang[ntri].v[0][2] = strch * cornp[2];

      	    triang[ntri].id[1] = i;
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[i] / maxobs;
            triang[ntri].v[1][0] = strch * cenp[0];
            triang[ntri].v[1][1] = strch * cenp[1];
            triang[ntri].v[1][2] = strch * cenp[2];

      	    upx_pixel_vector(&neighb[4],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[4]] / maxobs;
      	    triang[ntri].id[2] = neighb[4];
      	    triang[ntri].v[2][0] = strch * cornp[0];
      	    triang[ntri].v[2][1] = strch * cornp[1];
      	    triang[ntri].v[2][2] = strch * cornp[2];

	    ntri++;
         /* form triangle with T, TL, L */
      	    triang[ntri].id[0] = neighb[4];
      	    triang[ntri].v[0][0] = strch * cornp[0];
      	    triang[ntri].v[0][1] = strch * cornp[1];
      	    triang[ntri].v[0][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[5],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[5]] / maxobs;
      	    triang[ntri].id[1] = neighb[5];
      	    triang[ntri].v[1][0] = strch * cornp[0];
      	    triang[ntri].v[1][1] = strch * cornp[1];
      	    triang[ntri].v[1][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[6],&res,cornp);
      	    triang[ntri].id[2] = neighb[6];
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[6]] / maxobs;
            triang[ntri].v[2][0] = strch * cenp[0];
            triang[ntri].v[2][1] = strch * cenp[1];
            triang[ntri].v[2][2] = strch * cenp[2];

      	    ntri++;
         }
         else /*	assume neighbors are ordered: BL, B, BR, R, TR, T, TL */
         {
         /* form triangle with C, R, TR */
      	    triang[ntri].id[0] = i;
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[i] / maxobs;
            triang[ntri].v[0][0] = strch * cenp[0];
            triang[ntri].v[0][1] = strch * cenp[1];
            triang[ntri].v[0][2] = strch * cenp[2];

      	    upx_pixel_vector(&neighb[3],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[3]] / maxobs;
      	    triang[ntri].id[1] = neighb[3];
      	    triang[ntri].v[1][0] = strch * cornp[0];
      	    triang[ntri].v[1][1] = strch * cornp[1];
      	    triang[ntri].v[1][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[4],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[4]] / maxobs;
      	    triang[ntri].id[2] = neighb[4];
      	    triang[ntri].v[2][0] = strch * cornp[0];
      	    triang[ntri].v[2][1] = strch * cornp[1];
      	    triang[ntri].v[2][2] = strch * cornp[2];

	    ntri++;
         /* form triangle with TR, T, C */
      	    triang[ntri].id[0] = neighb[4];
      	    triang[ntri].v[0][0] = strch * cornp[0];
      	    triang[ntri].v[0][1] = strch * cornp[1];
      	    triang[ntri].v[0][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[5],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[5]] / maxobs;
      	    triang[ntri].id[1] = neighb[5];
      	    triang[ntri].v[1][0] = strch * cornp[0];
      	    triang[ntri].v[1][1] = strch * cornp[1];
      	    triang[ntri].v[1][2] = strch * cornp[2];

      	    triang[ntri].id[2] = i;
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[i] / maxobs;
            triang[ntri].v[2][0] = strch * cenp[0];
            triang[ntri].v[2][1] = strch * cenp[1];
            triang[ntri].v[2][2] = strch * cenp[2];

	    ntri++;
         /* form triangle with L, C, T */
      	    upx_pixel_vector(&neighb[7],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[7]] / maxobs;
      	    triang[ntri].id[0] = neighb[7];
      	    triang[ntri].v[0][0] = strch * cornp[0];
      	    triang[ntri].v[0][1] = strch * cornp[1];
      	    triang[ntri].v[0][2] = strch * cornp[2];

      	    triang[ntri].id[1] = i;
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[i] / maxobs;
            triang[ntri].v[1][0] = strch * cenp[0];
            triang[ntri].v[1][1] = strch * cenp[1];
            triang[ntri].v[1][2] = strch * cenp[2];

      	    upx_pixel_vector(&neighb[5],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[5]] / maxobs;
      	    triang[ntri].id[2] = neighb[5];
      	    triang[ntri].v[2][0] = strch * cornp[0];
      	    triang[ntri].v[2][1] = strch * cornp[1];
      	    triang[ntri].v[2][2] = strch * cornp[2];

	    ntri++;
         /* form triangle with T, TL, L */
      	    triang[ntri].id[0] = neighb[5];
      	    triang[ntri].v[0][0] = strch * cornp[0];
      	    triang[ntri].v[0][1] = strch * cornp[1];
      	    triang[ntri].v[0][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[6],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[6]] / maxobs;
      	    triang[ntri].id[1] = neighb[6];
      	    triang[ntri].v[1][0] = strch * cornp[0];
      	    triang[ntri].v[1][1] = strch * cornp[1];
      	    triang[ntri].v[1][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[7],&res,cornp);
      	    triang[ntri].id[2] = neighb[7];
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[i] / maxobs;
            triang[ntri].v[2][0] = strch * cornp[0];
            triang[ntri].v[2][1] = strch * cornp[1];
            triang[ntri].v[2][2] = strch * cornp[2];

      	    ntri++;
         }
      }
      doit = FALSE;
   }
}

int init_color_info(globemap,pix)
float *globemap;
int *pix;
{
int i, *pixptr, nglobeval=1;
float globemin, globemax;
float *globeptr, rabs();

 if( globemap != NULL )	/* must set pntcolor according to globe values */
 {
    globeptr = globemap;
    pixptr = pix;

   globeptr = globemap;
   for( i = 0; i < NP; i++ )
   { 
/*      if( *pixptr++ < 0 )
      {
         pntcolor[i][0] = 0.1;
         pntcolor[i][1] = 0.1;
         pntcolor[i][2] = 0.1;
      }
*/
      if( *globeptr == 0 )
      {
            pntcolor[i][0] = 0.1;
            pntcolor[i][1] = 0.1;
            pntcolor[i][2] = 1.0;
      }
      else
      {
	pntcolor[i][0] = 0.6 * (*globeptr);
	pntcolor[i][1] = 0.6 * pntcolor[i][0];
	pntcolor[i][2] = 0.2;
      } 
      globeptr++;
   }
   

 }
 else /* no globemap, just image the pixel numbers */
 {
   for( i = 0; i < NP; i++ )
   {
      if( i < NP/6 )
      {
            pntcolor[i][0] = (255 - (i % 256))/255.0;
            pntcolor[i][1] = 0.0;
       	    pntcolor[i][2] = 0.0;
      }
      else if( i < 2*NP/6 )
      {
            pntcolor[i][0] = 0.0;
            pntcolor[i][1] = (255 - (i % 256))/255.0;
       	    pntcolor[i][2] = 0.0;
      }
      else if( i < 3*NP/6 )
      {
            pntcolor[i][0] = 0.0;
            pntcolor[i][1] = 0.0;
       	    pntcolor[i][2] = (255 - (i % 256))/255.0;
      }
      else if( i < 4*NP/6 )
      {
            pntcolor[i][0] = (255 - (i % 256))/255.0;
            pntcolor[i][1] = (255 - (i % 256))/255.0;
       	    pntcolor[i][2] = 0.0;
      }
      else if( i < 5*NP/6 )
      {
            pntcolor[i][0] = (255 - (i % 256))/255.0;
            pntcolor[i][1] = 0.0;
       	    pntcolor[i][2] = (255 - (i % 256))/255.0;
      }
      else if( i < NP )
      {
            pntcolor[i][0] = 0.0;
            pntcolor[i][1] = (255 - (i % 256))/255.0;
       	    pntcolor[i][2] = (255 - (i % 256))/255.0;
      }
   }
  }
}


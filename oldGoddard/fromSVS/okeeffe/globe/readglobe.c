#include "globe.h"

int readglobe(filename,globemap,pix)
char *filename; float *globemap; int *pix;
{
   int i, j, nb;
   FILE *fp, *fopen();
   float *globeptr;
   int *pixptr;
   struct globerec { int n_obs; int value; } buff;
   int maxpix;
   unsigned char flag;

   maxpix = 0;
   globeptr = globemap;
   pixptr = pix;
   printf("readglobe> opening file: %s\n",filename);
   fp = fopen(filename,"r"); 

   if( fp == NULL )
   {
      printf("readglobe> can't open file: %s\n",filename);
      exit(0);
   }
   for( i = 0; i < NP; i++ )
   {
      flag = 0;
      for( j = 0; j < 16; j++) /* assuming dirbe resolution */
      {
	/* nb = fscanf(fp," %d %d",pixptr,&flag); */
	 nb = fread(&flag,1,1,fp);
         *globeptr = flag;
      }
/*      if( *pixptr > maxpix ) maxpix = *pixptr; */
      maxpix = NP - 1;
      
      if( nb < 1 )
      {
         printf("readglobe> unexpected end-of-file: %s\n",filename);
   	 exit(0);
      }
#ifdef DEBUG
/*
	if( i % 128 == 0 ) printf("readglobe> pix= %d, value= %f\n",
				   *pixptr,*globeptr);
*/
#endif
      globeptr++;
      pixptr++;
   }
   fclose( fp );
   return( maxpix );
}


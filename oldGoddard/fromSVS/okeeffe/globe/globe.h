#include <stdio.h>
#include <gl.h>
#include <device.h>
#include <math.h>
#define FALSE 0
#define DEBUG

#ifdef MAIN
#define global 
#else
#define global extern
#endif
/*
#define NP 1536
#define RES 5
#define NP 6144
#define RES 6
*/
#define NP 24576
#define RES 7

typedef struct vrtx_lst { int id[3]; float v[3][3]; } VRTX_LST;

global VRTX_LST triang[3*NP];
global float pntcolor[3*NP][3];
global float orbpnt[256][3];
global int ntri;
global char finished;


#define MAIN
#include "globe.h"

main(argc, argv)
int argc; char **argv;
{
int i, x0, y0;
char *globefile;
float *globemap;
int *pix;
int maxpix;
int readglobe(), init_triang_info(), init_color_info();
int init_orbit();
float *orbidx, *draw_orbit();
static float ident[4][4] = { {1.0, 0.0, 0.0, 0.0}, 
			     {0.0, 1.0, 0.0, 0.0}, 
			     {0.0, 0.0, 1.0, 0.0}, 
			     {0.0, 0.0, 0.0, 1.0} };
short wrred = 0xffff, wrgrn = 0xffff, wrblu = 0xffff;
float dist = 5.0, delt = 0.1;

pix = NULL;
globemap = NULL;
maxpix = 0;

if( argc > 1 ) /* get the file name */
{
   globefile = argv[1];
   globemap = (float *) malloc(NP*sizeof(float));
   pix = (int *) malloc(NP*sizeof(int));
   maxpix = readglobe(globefile,globemap,pix);
}

if( argc > 2 )
{
    x0 = atoi(argv[1]);
    y0 = atoi(argv[2]);
}
else
{
    x0 = 8;
    y0 = 8;
}

/* init unit sphere using quad-cube routines */
init_triang_info(0,pix,pix);

/* init colors */
init_color_info(globemap,pix);

init_orbit();

prefposition(x0,x0+988,y0,y0+988);
keepaspect(1,1);
winopen(argv[0]);
RGBmode();
RGBwritemask(wrred,wrgrn,wrblu);
drawmode(NORMALDRAW);
doublebuffer();
gconfig();

mmode(MVIEWING);
perspective(450,1.0,0.01,100.0);
loadmatrix(ident);
polarview(dist,10,800,0);
lsetdepth(0,0x7fffff);
zbuffer(TRUE);
backface(TRUE);

while( TRUE )
{
   zclear();
   cpack(0);
   clear();
   orbidx = draw_orbit();
   draw_sphere(orbidx);

   swapbuffers(); 
}
}

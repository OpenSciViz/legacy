#include <stdio.h>
#include <gl.h>
#include <device.h>
#include <math.h>
#define FALSE 0
#define TRUE 1

/*
#define DEBUG
#define NP 1536 
short res = 5;
#define NP 6144
short res = 6;
#define NP 4*24567
short res = 8;
*/
#define NP 24567
short res = 7;

typedef struct vrtx_lst { short nv; int id[4]; float v[4][3]; } VRTX_LST;

VRTX_LST rctng[NP];

int nrec=0;
char finished;
/*
short pntcolor[NP][3];
*/
float pntcolor[NP][3];

float bpnt[4][3] = { { -1.5, -1.5, -1.2  }, { 1.5, -1.5, -1.2 },
      		     { 1.5, 1.5, -1.2 }, { -1.5, 1.5, -1.2 } };
float orbpnt[256][3];

float white[3] = { 1.0, 1.0, 1.0 };
float gray[3] = { 0.5, 0.5, 0.5 };
float red[3] = { 1.0, 0.0, 0.0 };
float green[3] = { 0.0, 1.0, 0.0 };
float blue[3] = { 0.0, 0.0, 1.0 };
float yellow[3] = { 1.0, 1.0, 0.0 };
float magenta[3] = { 1.0, 0.0, 1.0 };
float cyan[3] = { 0.0, 1.0, 1.0 };
float orbcolr[3] = { 0.0, 1.0, 0.0, };
static float ident[4][4] = { {1.0, 0.0, 0.0, 0.0}, 
			     {0.0, 1.0, 0.0, 0.0}, 
			     {0.0, 0.0, 1.0, 0.0}, 
			     {0.0, 0.0, 0.0, 1.0} };
short wrred = 0xffff, wrgrn = 0xffff, wrblu = 0xffff;
long arot = 0, trot = 0; 
char rotaxis = 'z';
float dist = 5.0, delt = 0.1;
float sunpos[3] = { 0.87, 0.5, 0.0 };

int init_orbit()
{
   int i;
   float pi=3.1415926, pi2;

   pi2 = 2.0*pi;
   for( i = 0; i < 256; i++ )
   {
	orbpnt[i][0] = 1.25 * sin(pi2 * i / 256.0)*cos(1.6*pi);
	orbpnt[i][1] = 1.25 * sin(pi2 * i / 256.0)*sin(1.6*pi); 
	orbpnt[i][2] = 1.25 * cos(pi2 * i / 256.0);
   } 
}

float *draw_orbit()
{
   static int nseg=1;
   int i;
   float *orbpos;

   if( nseg < 256 ) 
      nseg++; 
   else
   {
      arot = arot + 50;
      if( arot > 3600 ) arot = 0;
      nseg = 1;
   }
   pushmatrix();
   loadmatrix(ident);
   polarview(dist,10,800,0);
/*   rotate(arot,rotaxis); */
   bgnline();
     for( i = 0; i < nseg; i++ )
     {
        c3f(red); v3f(orbpnt[i]); 
     }
   endline();
   popmatrix();
   orbpos = &orbpnt[nseg][0];
   return( orbpos );
}

int draw_bottom()
{
   pushmatrix();
   loadmatrix(ident);
   polarview(dist,10,800,0);
   
   bgnpolygon();
   c3f(gray); v3f(bpnt[0]); 
   c3f(gray); v3f(bpnt[1]); 
   c3f(gray); v3f(bpnt[2]); 
   c3f(gray); v3f(bpnt[3]);
   endpolygon();

   popmatrix();
}

int draw_face(iface,orbpos)
short iface;
float orbpos[];
{
   int irctng, inc, cnr;
   float dotprod[4], sundot[4], globcolr[3];
   static float matrx[3][3], orbrot[3][3];
   float vrtx[4][3], orb[3];
   char swtch;

/*   getmatrix(matrx);  ASSUME INVERSE IS TRANSPOSE */
/* need to apply inverse rotation Rz(-trot) before evaluating dot
   products */
   matrx[0][0] = cos(trot/573.0);
   matrx[1][0] = sin(trot/573.0);
   matrx[0][1] = -matrx[1][0];
   matrx[1][1] = matrx[0][0];
   matrx[2][2] = 1.0;

   orbrot[0][0] = cos(arot/573.0);
   orbrot[1][0] = sin(arot/573.0);
   orbrot[0][1] = -orbrot[1][0];
   orbrot[1][1] = orbrot[0][0];
   orbrot[2][2] = 1.0;
/*
   orb[0] = ( orbrot[0][0]*orbpos[0] + orbrot[0][1]*orbpos[1] +
		orbrot[0][2]*orbpos[2] ) / 1.5;
   orb[1] = ( orbrot[1][0]*orbpos[0] + orbrot[1][1]*orbpos[1] +
		orbrot[2][2]*orbpos[2] ) / 1.5;
   orb[2] = ( orbrot[2][0]*orbpos[0] + orbrot[2][1]*orbpos[1] +
		orbrot[2][2]*orbpos[2] ) / 1.5;
*/
   irctng = iface * NP / 6;
   for( inc = 0; inc < (NP/6); inc = inc++ )
   {
      for( cnr = 0; cnr < 4; cnr++ )
      {
         vrtx[cnr][0] = matrx[0][0]*rctng[irctng].v[cnr][0] +
		   matrx[0][1]*rctng[irctng].v[cnr][1] +
		   matrx[0][2]*rctng[irctng].v[cnr][2];
         vrtx[cnr][1] = matrx[1][0]*rctng[irctng].v[cnr][0] +
		   matrx[1][1]*rctng[irctng].v[cnr][1] +
		   matrx[1][2]*rctng[irctng].v[cnr][2];
         vrtx[cnr][2] = matrx[2][0]*rctng[irctng].v[cnr][0] +
		   matrx[2][1]*rctng[irctng].v[cnr][1] +
		   matrx[2][2]*rctng[irctng].v[cnr][2];
         dotprod[cnr] =  ( vrtx[cnr][0] * orbpos[0] + 
      		          vrtx[cnr][1] * orbpos[1] + 
      		          vrtx[cnr][2] * orbpos[2] ) / 1.25; 
         sundot[cnr] =  ( vrtx[cnr][0] * sunpos[0] + 
      		          vrtx[cnr][1] * sunpos[1] + 
      		          vrtx[cnr][2] * sunpos[2] ); 
      }

      if( dotprod[0] > 0.995 || dotprod[1] > 0.995 || dotprod[2] > 0.995
          || dotprod[3] > 0.995 ) 
	 swtch = TRUE;
      else
	 swtch = FALSE;

      bgnpolygon();
	 if( swtch )
	    c3f(orbcolr);
	 else if( sundot[0] < 0.0 )
 	 {
	    globcolr[0] = 0.5 * pntcolor[rctng[irctng].id[0]][0];
	    globcolr[1] = 0.5 * pntcolor[rctng[irctng].id[0]][1];
	    globcolr[2] = 0.5 * pntcolor[rctng[irctng].id[0]][2];
            c3f(globcolr); 
         }
	 else
	    c3f(pntcolor[rctng[irctng].id[0]]); 
	 v3f(rctng[irctng].v[0]);
	 if( swtch )
	    c3f(orbcolr);
	 else if( sundot[0] < 0.0 )
 	 {
	    globcolr[0] = 0.5 * pntcolor[rctng[irctng].id[1]][0];
	    globcolr[1] = 0.5 * pntcolor[rctng[irctng].id[1]][1];
	    globcolr[2] = 0.5 * pntcolor[rctng[irctng].id[1]][2];
            c3f(globcolr); 
         }
	 else
            c3f(pntcolor[rctng[irctng].id[1]]); 
	 v3f(rctng[irctng].v[1]);
	 if( swtch )
	    c3f(orbcolr);
	 else if( sundot[0] < 0.0 )
 	 {
	    globcolr[0] = 0.5 * pntcolor[rctng[irctng].id[2]][0];
	    globcolr[1] = 0.5 * pntcolor[rctng[irctng].id[2]][1];
	    globcolr[2] = 0.5 * pntcolor[rctng[irctng].id[2]][2];
            c3f(globcolr); 
         }
	 else
            c3f(pntcolor[rctng[irctng].id[2]]); 
	 v3f(rctng[irctng].v[2]);
	 if( swtch )
	    c3f(orbcolr);
	 else if( sundot[0] < 0.0 )
 	 {
	    globcolr[0] = 0.5 * pntcolor[rctng[irctng].id[3]][0];
	    globcolr[1] = 0.5 * pntcolor[rctng[irctng].id[3]][1];
	    globcolr[2] = 0.5 * pntcolor[rctng[irctng].id[3]][2];
            c3f(globcolr); 
         }
	 else
            c3f(pntcolor[rctng[irctng].id[3]]); 
         v3f(rctng[irctng].v[3]);
      endpolygon();
/*
      bgntmesh();
         c3f(pntcolor[rctng[irctng].id[0]]); v3f(rctng[irctng].v[0]);
         c3f(pntcolor[rctng[irctng].id[1]]); v3f(rctng[irctng].v[1]);
	swaptmesh();
         c3f(pntcolor[rctng[irctng].id[2]]); v3f(rctng[irctng].v[2]);
         c3f(pntcolor[rctng[irctng].id[3]]); v3f(rctng[irctng].v[3]);
      endtmesh();
*/
      irctng++;
   }
}

int draw_sphere(orbpos)
float orbpos[];
{
   short iface;
   static n_face=0;
   static nrot=0;

/*
   if( n_face > 0 )
   {
      nrot++;
      if( (nrot % 16) == 0 ) n_face--; 
   }
*/
   pushmatrix();
   trot += 50;
   if( trot > 3600 )
      trot = 0;
   rotate(trot,rotaxis);
   for( iface = 5; iface >= n_face; iface--) 
	draw_face(iface,orbpos);
   popmatrix();
}

int init_rctng_info(maxpix,pix)
int maxpix, pix[];
{
   int i, j, nval, neighb[8];
   float cenp[3], cornp[3], strch;
   int upx_8_neighbors(), upx_pixel_vector();

   strch = 1.0;
   for( i = 0; i < NP; i++ )
   {
      if( (i % 4) == 3 )
      {
         upx_pixel_vector(&i,&res,cenp); /* Center pixel */
         upx_8_neighbors(&i,&res,neighb,&nval);
/*
      	 if( nval < 8 ) corners are short a vertex, form triangle 
         {

         }
         else	assume neighbors are ordered: BL, B, BR, R, TR, T, TL
*/ 
         {
         /* form square with BL, B, C, L */
      	    upx_pixel_vector(&neighb[0],&res,cornp);
      	    rctng[nrec].id[0] = neighb[0];
      	    rctng[nrec].v[0][0] = strch * cornp[0];
      	    rctng[nrec].v[0][1] = strch * cornp[1];
      	    rctng[nrec].v[0][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[1],&res,cornp);
      	    rctng[nrec].id[1] = neighb[1];
      	    rctng[nrec].v[1][0] = strch * cornp[0];
      	    rctng[nrec].v[1][1] = strch * cornp[1];
      	    rctng[nrec].v[1][2] = strch * cornp[2];

      	    rctng[nrec].id[2] = i;
            rctng[nrec].v[2][0] = strch * cenp[0];
            rctng[nrec].v[2][1] = strch * cenp[1];
            rctng[nrec].v[2][2] = strch * cenp[2];

      	    upx_pixel_vector(&neighb[7],&res,cornp);
      	    rctng[nrec].id[3] = neighb[7];
      	    rctng[nrec].v[3][0] = strch * cornp[0];
      	    rctng[nrec].v[3][1] = strch * cornp[1];
      	    rctng[nrec].v[3][2] = strch * cornp[2];

      	    rctng[nrec++].nv = 4;

         /* form square with B, BR, R, C */
      	    upx_pixel_vector(&neighb[1],&res,cornp);
      	    rctng[nrec].id[0] = neighb[1];
      	    rctng[nrec].v[0][0] = strch * cornp[0];
      	    rctng[nrec].v[0][1] = strch * cornp[1];
      	    rctng[nrec].v[0][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[2],&res,cornp);
      	    rctng[nrec].id[1] = neighb[2];
      	    rctng[nrec].v[1][0] = strch * cornp[0];
      	    rctng[nrec].v[1][1] = strch * cornp[1];
      	    rctng[nrec].v[1][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[3],&res,cornp);
      	    rctng[nrec].id[2] = neighb[3];
      	    rctng[nrec].v[2][0] = strch * cornp[0];
      	    rctng[nrec].v[2][1] = strch * cornp[1];
      	    rctng[nrec].v[2][2] = strch * cornp[2];

      	    rctng[nrec].id[3] = i;
            rctng[nrec].v[3][0] = strch * cenp[0];
            rctng[nrec].v[3][1] = strch * cenp[1];
            rctng[nrec].v[3][2] = strch * cenp[2];

      	    rctng[nrec++].nv = 4;

         /* form square with R, TR, T, C */
      	    upx_pixel_vector(&neighb[3],&res,cornp);
      	    rctng[nrec].id[0] = neighb[3];
      	    rctng[nrec].v[0][0] = strch * cornp[0];
      	    rctng[nrec].v[0][1] = strch * cornp[1];
      	    rctng[nrec].v[0][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[4],&res,cornp);
      	    rctng[nrec].id[1] = neighb[4];
      	    rctng[nrec].v[1][0] = strch * cornp[0];
      	    rctng[nrec].v[1][1] = strch * cornp[1];
      	    rctng[nrec].v[1][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[5],&res,cornp);
      	    rctng[nrec].id[2] = neighb[5];
      	    rctng[nrec].v[2][0] = strch * cornp[0];
      	    rctng[nrec].v[2][1] = strch * cornp[1];
      	    rctng[nrec].v[2][2] = strch * cornp[2];

      	    rctng[nrec].id[3] = i;
            rctng[nrec].v[3][0] = strch * cenp[0];
            rctng[nrec].v[3][1] = strch * cenp[1];
            rctng[nrec].v[3][2] = strch * cenp[2];

      	    rctng[nrec++].nv = 4;

         /* form square with T, TL, L, C */
      	    upx_pixel_vector(&neighb[5],&res,cornp);
      	    rctng[nrec].id[0] = neighb[5];
      	    rctng[nrec].v[0][0] = strch * cornp[0];
      	    rctng[nrec].v[0][1] = strch * cornp[1];
      	    rctng[nrec].v[0][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[6],&res,cornp);
      	    rctng[nrec].id[1] = neighb[6];
      	    rctng[nrec].v[1][0] = strch * cornp[0];
      	    rctng[nrec].v[1][1] = strch * cornp[1];
      	    rctng[nrec].v[1][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[7],&res,cornp);
      	    rctng[nrec].id[2] = neighb[7];
      	    rctng[nrec].v[2][0] = strch * cornp[0];
      	    rctng[nrec].v[2][1] = strch * cornp[1];
      	    rctng[nrec].v[2][2] = strch * cornp[2];

      	    rctng[nrec].id[3] = i;
            rctng[nrec].v[3][0] = strch * cenp[0];
            rctng[nrec].v[3][1] = strch * cenp[1];
            rctng[nrec].v[3][2] = strch * cenp[2];

      	    rctng[nrec++].nv = 4;
         }
      }
   }
}

int init_color_info(globemap,pix)
float *globemap;
int *pix;
{
int i, *pixptr, nglobeval=1;
float globemin, globemax;
float *globeptr, rabs();

 if( globemap != NULL )	/* must set pntcolor according to globe values */
 {
    globeptr = globemap;
    pixptr = pix;

   globeptr = globemap;
   for( i = 0; i < NP; i++ )
   { 
      if( *pixptr++ < 0 )
      {
         pntcolor[i][0] = 0.1;
         pntcolor[i][1] = 0.1;
         pntcolor[i][2] = 0.1;
      }
      else
      {
	 if( *globeptr == 0 )
	 {
            pntcolor[i][0] = 0.1;
            pntcolor[i][1] = 0.1;
            pntcolor[i][2] = 1.0;
	 }
	 else
	 {
	    pntcolor[i][0] = 0.6;
	    pntcolor[i][1] = 0.4;
	    pntcolor[i][2] = 0.2;
	 } 
      }

      globeptr++;
   }
   

 }
 else /* no globemap, just image the pixel numbers */
 {
   for( i = 0; i < NP; i++ )
   {
      if( i < NP/6 )
      {
            pntcolor[i][0] = (255 - (i % 256))/255.0;
            pntcolor[i][1] = 0.0;
       	    pntcolor[i][2] = 0.0;
      }
      else if( i < 2*NP/6 )
      {
            pntcolor[i][0] = 0.0;
            pntcolor[i][1] = (255 - (i % 256))/255.0;
       	    pntcolor[i][2] = 0.0;
      }
      else if( i < 3*NP/6 )
      {
            pntcolor[i][0] = 0.0;
            pntcolor[i][1] = 0.0;
       	    pntcolor[i][2] = (255 - (i % 256))/255.0;
      }
      else if( i < 4*NP/6 )
      {
            pntcolor[i][0] = (255 - (i % 256))/255.0;
            pntcolor[i][1] = (255 - (i % 256))/255.0;
       	    pntcolor[i][2] = 0.0;
      }
      else if( i < 5*NP/6 )
      {
            pntcolor[i][0] = (255 - (i % 256))/255.0;
            pntcolor[i][1] = 0.0;
       	    pntcolor[i][2] = (255 - (i % 256))/255.0;
      }
      else if( i < NP )
      {
            pntcolor[i][0] = 0.0;
            pntcolor[i][1] = (255 - (i % 256))/255.0;
       	    pntcolor[i][2] = (255 - (i % 256))/255.0;
      }
   }
  }
}

int readglobe(filename,globemap,pix)
char *filename; float *globemap; int *pix;
{
   int i, j, nb;
   FILE *fp, *fopen();
   float *globeptr;
   int *pixptr;
   struct globerec { int n_obs; int value; } buff;
   int maxpix, flag;

   maxpix = 0;
   globeptr = globemap;
   pixptr = pix;
   printf("readglobe> opening file: %s\n",filename);
   fp = fopen(filename,"r"); 

   if( fp == NULL )
   {
      printf("readglobe> can't open file: %s\n",filename);
      exit(0);
   }
   for( i = 0; i < NP; i++ )
   {
      flag = 0;
      for( j = 0; j < 16 ; j++ ) /* skip every 16 records to get 4*6144 */
      {
	 nb = fscanf(fp," %d %d",pixptr,globeptr);
	 if( *globeptr != 0 ) flag = *globeptr; 
      }
      if( flag != 0 ) *globeptr = flag;
      if( *pixptr > maxpix ) maxpix = *pixptr;
      
      if( nb < 2 )
      {
         printf("readglobe> unexpected end-of-file: %s\n",filename);
   	 exit(0);
      }
#ifdef DEBUG
	if( i % 128 == 0 ) printf("readglobe> pix= %d, value= %f\n",
				   *pixptr,*globeptr);
#endif
      globeptr++;
      pixptr++;
   }
   fclose( fp );
   return( maxpix );
}

main(argc, argv)
int argc; char **argv;
{
int i, x0, y0;
char *globefile;
float *globemap;
int *pix;
int maxpix;
int readglobe(), init_rctng_info(), init_color_info();
int init_orbit();
float *orbidx, *draw_orbit();

pix = NULL;
globemap = NULL;
maxpix = 0;

if( argc > 1 ) /* get the file name */
{
   globefile = argv[1];
   globemap = (float *) malloc(NP*sizeof(float));
   pix = (int *) malloc(NP*sizeof(int));
   maxpix = readglobe(globefile,globemap,pix);
}

if( argc > 2 )
{
    x0 = atoi(argv[1]);
    y0 = atoi(argv[2]);
}
else
{
    x0 = 8;
    y0 = 8;
}

/* init unit sphere using quad-cube routines */
init_rctng_info(maxpix,pix);

/* init colors */
init_color_info(globemap,pix);

init_orbit();

prefposition(x0,x0+988,y0,y0+988);
keepaspect(1,1);
winopen(argv[0]);
RGBmode();
RGBwritemask(wrred,wrgrn,wrblu);
drawmode(NORMALDRAW);
doublebuffer();
gconfig();

mmode(MVIEWING);
perspective(450,1.0,0.01,100.0);
loadmatrix(ident);
polarview(dist,10,800,0);
lsetdepth(0,0x7fffff);
zbuffer(TRUE);
backface(TRUE);

while( TRUE )
{
/*
      if( dist >= 20.0 ) delt = 0.1;
      if( dist <= 0.5 ) delt = -0.1;
      dist = dist - delt;
      loadmatrix(ident);
      polarview(dist,10,600,0);
*/
      
      zclear();
      cpack(0);
      clear();
      if( getbutton(MIDDLEMOUSE) ) exit(0);
/*
printf("bottom, zrot= %d\n",zrot);
      finished = FALSE;
      while( !finished )
          if( getbutton(LEFTMOUSE) ) finished = TRUE;

      draw_bottom();

printf("sphere, zrot= %d\n",zrot);
      finished = FALSE;
      while( !finished )
          if( getbutton(LEFTMOUSE) ) finished = TRUE;
*/
      orbidx = draw_orbit();
      draw_sphere(orbidx);

      swapbuffers(); 
   }
}


/* converts unit vector c into nface number (0-5) and x,y
   in range 0-1 */
int axisxy(c,nface,x,y)
float c[3], *x, *y;
short *nface;
{
   float ac3, ac2, ac1, eta, xi;
   float rabs();
   int incube();

   ac3 = rabs(c[2]);
   ac2 = rabs(c[1]);
   ac1 = rabs(c[0]);
   if(ac3 > ac2)
   {
      if(ac3 > ac1)
      {
         if(c[2] > 0.)
         { 
      	    (*nface) = 0;
	    eta =  -c[0]/c[2];
	    xi = c[1]/c[2];
	 }
         else
         {
            (*nface) = 5;
	    eta =  -c[0]/c[2];
	    xi =  -c[1]/c[2];
         }
      }
      else
      {
         if(c[0] > 0.)
         {
	    (*nface) = 1;
	    xi = c[1]/c[0];
	    eta = c[2]/c[0];
         }
         else
         {
	    (*nface) = 3;
	    eta =  -c[2]/c[0];
	    xi = c[1]/c[0];
         }
      }
   }
   else
   {
      if(ac2 > ac1)
      {
         if(c[1] > 0.)
         {
	    (*nface) = 2;
	    eta = c[2]/c[1];
	    xi =  -c[0]/c[1];
	 }
         else
         {
	    (*nface) = 4;
	    eta =  -c[2]/c[1];
	    xi =  -c[0]/c[1];
         }
      }
      else
      {
         if(c[0] > 0.)
         {
            (*nface) = 1;
            xi = c[1]/c[0];
            eta = c[2]/c[1];
  	 }
         else
         {
	    (*nface) = 3;
	    eta =  -c[2]/c[0];
	    xi = c[1]/c[0];
         }
      }
   }
   incube(&xi,&eta,x,y);
   *x  =  ((*x) + 1. ) / 2.;
   *y  =  ((*y) + 1. ) / 2.;
   return;
}


/*
Routine to set up the bit tables for use in the pixelization subroutines 
(extracted and generalized from existing routines).
*/
int upx_bit_table_set(ix,iy,length)
short *length;       /* Number of elements in ix, iy */
int ix[];	/* x bits */
int iy[];	/* y bits */
{
   int i,j,k,ip,id; /* Loop variables */
   char finished;

   for( i = 0; i < (*length); i++ )
   {
       j = i;
       k = 0;
       ip = 1;
       finished = 0;
       while( !finished )
       {
           if( j == 0 )
           {
	      ix[i] = k;
              iy[i] = 2*k;
              finished = 1;
	   }
           else
           {
              id = j % 2;
              j = j/2;
              k = ip * id + k;
              ip = ip * 4;
           }
       }
   }
   return;
}
/*
input: x,y in range -1 to +1 are database co-ordinates
output: xi, eta in range -1 to +1 are tangent plane co-ordinates
based on polynomial fit found using fcfit.for
*/
#define np 28
int forward_cube(x,y,xi,eta)
float *x, *y, *xi, *eta;
{
   static double p[np] = { -0.27292696, -0.07629969, -0.02819452, -0.22797056,
	    	          -0.01471565,  0.27058160,  0.54852384,  0.48051509,
	    	   	  -0.56800938, -0.60441560, -0.62930065, -1.74114454,
	    	    	   0.30803317,  1.50880086,  0.93412077,  0.25795794,
	    	    	   1.71547508,  0.98938102, -0.93678576, -1.41601920,
	    	   	  -0.63915306,  0.02584375, -0.53022337, -0.83180469,
	    	    	   0.08693841,  0.33887446,  0.52032238,  0.14381585 };
   double xx, yy;

   xx = (*x) * (*x);
   yy = (*y) * (*y);
   *xi = (*x) * (1.+(1.-xx)*(
	p[0]+xx*(p[1]+xx*(p[3]+xx*(p[6]+xx*(p[10]+xx*(p[15]+xx*p[21]))))) +
	yy*( p[2]+xx*(p[4]+xx*(p[7]+xx*(p[11]+xx*(p[16]+xx*p[22])))) +
	yy*( p[5]+xx*(p[8]+xx*(p[12]+xx*(p[17]+xx*p[23]))) +
	yy*( p[9]+xx*(p[13]+xx*(p[18]+xx*p[24])) + 
	yy*( p[14]+xx*(p[19]+xx*p[25]) +
	yy*( p[20]+xx*p[26] + yy*p[27])))) )));
   *eta = (*y) * (1.+(1.-yy)*(
	p[0]+yy*(p[1]+yy*(p[3]+yy*(p[6]+yy*(p[10]+yy*(p[15]+yy*p[21]))))) +
	xx*( p[2]+yy*(p[4]+yy*(p[7]+yy*(p[11]+yy*(p[16]+yy*p[22])))) +
	xx*( p[5]+yy*(p[8]+yy*(p[12]+yy*(p[17]+yy*p[23]))) +
	xx*( p[9]+yy*(p[13]+yy*(p[18]+yy*p[24])) + 
	xx*( p[14]+yy*(p[19]+yy*p[25]) +
	xx*( p[20]+yy*p[26] + xx*p[27])))) )));
   return;
}

/*	see csc "extended study ... note that the text has typos.  i have
	tried to copy the fortran listings
*/
int incube(alpha,beta,x,y)
float *alpha, *beta, *x, *y;
{
   static double gstar =  1.37484847732, g =  -0.13161671474,
       m =  0.004869491981, w1 =  -0.159596235474, c00 = 0.141189631152,
       c10 = 0.0809701286525, c01 =  -0.281528535557, c11 = 0.15384112876,
       c20 =  -0.178251207466, c02 = 0.106959469314, d0 = 0.0759196200467,
       d1 = -0.0217762490699, r0 = 0.577350269;
   double aa, bb, a4, b4, onmaa, onmbb;

   aa = (*alpha) * (*alpha);
   bb = (*beta) * (*beta);
   a4 = aa*aa;
   b4 = bb*bb;
   onmaa = 1. - aa;
   onmbb = 1. - bb;
   *x = (*alpha) * (gstar+aa*(1.-gstar)+onmaa*(bb*(g+(m-g)*aa
      +onmbb*(c00+c10*aa+c01*bb+c11*aa*bb+c20*a4+c02*b4))
      +aa*(w1-onmaa*(d0+d1*aa))));
   *y = (*beta) * (gstar+bb*(1.-gstar)+onmbb*(aa*(g+(m-g)*bb
      +onmaa*(c00+c10*bb+c01*aa+c11*bb*aa+c20*b4+c02*a4))
      +bb*(w1-onmbb*(d0+d1*bb))));

      return;
}

/*
  Routine to return the pixel number corresponding to the input unit
  vector in the given resolution.  Adapted from the SUPER_PIXNO routine
  by E. Wright, this routine determines the pixel number in the maximum
  resolution (15), then divides by the appropriate power of 4 to determine
  the pixel number in the desired resolution.
*/
int upx_pixel_number(vector,resolution,pixel)
float vector[3];     /* Unit vector of position of interest */
short *resolution;    /* Resolution of quad cube */
int *pixel;         /* Pixel number (0 -> 6*((2**(res-1))**2) */
{
   short face;              /* Cube face number (0-5) */
   float x,y ;              /* floatinates on cube face */
   static int ix[128] = { 0 };
   static int iy[128] = { 0 };   /* Bit tables for calculating i,j */
   int i,j;               /* Integer pixel floatinates */
   int ip,id;       
/* i*4 '4' - reuired to avoid integer overflow for large pixel numbers: */
   static int two14 = 16384; /* 2^14 */
   static int two28 = 268435456; /* 2^28 */
   static int four = 4;
   short il,ih,jl,jh;       /* High and low 2-bytes of i & j */
   int upx_bit_table_set(), axisxy(), power();
   short length = 128;

   if( ix[127] == 0 ) upx_bit_table_set(ix,iy,&length);
   axisxy(vector,&face,&x,&y);
   i = two14 * x;
   j = two14 * y;
   if( i > (two14 - 1) ) i = two14 - 1;
   if( j > (two14 - 1) )j = two14 - 1;
   il = i % 128;
   ih = i / 128;
   jl = j % 128;
   jh = j / 128;
   *pixel = face * two28 + ix[il] + iy[jl] +
                    two14 * ( ix[ih] + iy[jh] );
/*
     'Pixel' now contains the pixel number for a resolution of 15.  To
     convert to the desired resolution, (integer) divide by 4 to the power
     of the difference between 15 and the given resolution:
*/
   *pixel = (*pixel) / power(four,(15 - (*resolution)));

   return;
}

/*
   Routine to return unit vector pointing to center of pixel given pixel
  number and resolution of the cube.  Based on exisiting _CENPIX routines
  in CSDRLIB.
*/
int upx_pixel_vector(pixel,resolution,vector)
int *pixel; short *resolution; float vector[3];
{
   short face;
   int jx,jy,ip,id;
   float x,y;
   float scale;
   int pixels_per_face;
   int fpix; /* Pixel number within the face */
   float xi,eta; /* 'Database' floatinates */
   static int one = 1;
   static int two = 2; /* Constants used to avoid integer */
   int forward_cube(), xyaxis(), power();

   jx = 0;
   jy = 0;
   ip = 1;
   scale = 1.0 * power(2,((*resolution) - 1)) / 2.0;
   pixels_per_face = power(two,two*((*resolution) - one));
   face = (*pixel)/pixels_per_face;
   fpix = (*pixel) - face*pixels_per_face;

   while( fpix != 0 )
   {
      id = fpix % 2;
      fpix = fpix / 2;
      jx = id * ip + jx;
      id = fpix % 2;
      fpix = fpix / 2;
      jy = id * ip + jy;
      ip = 2 * ip;
   }

   x = (jx - scale + 0.5) / scale;
   y = (jy - scale + 0.5) / scale;
   forward_cube(&x,&y,&xi,&eta);
   xyaxis(&face,&xi,&eta,vector);

   return;
}


/*	converts face number nface (0-5) and xi, eta (-1. - +1.)
	into a unit vector c
*/
int xyaxis(nface,xi,eta,c)
short *nface;
float c[3], *xi, *eta;
{
   float mag;
   if( *nface == 0 )
   {
      c[2] = 1.0;
      c[0] = -(*eta);	/*  transform face to sphere */
      c[1] = (*xi);
   }
   else if( *nface == 1 )
   {
      c[0] = 1.0;
      c[2] = (*eta);
      c[1] = (*xi);
   }
   else if( *nface == 2 )
   {
      c[1] = 1.0;
      c[2] = (*eta);
      c[0] = -(*xi);
   }
   else if( *nface == 3 )
   {
      c[0] = -1.0;
      c[2] = (*eta);
      c[1] = -(*xi);
   }
   else if( *nface == 4 )
   {
      c[1] = -1.0;
      c[2] = (*eta);
      c[0] = (*xi);
   }
   else if( *nface == 5 )
   {
      c[2] = -1.0;
      c[0] = (*eta);
      c[1] = (*xi);
   }

   mag = sqrt( c[0]*c[0] + c[1]*c[1] + c[2]*c[2] );
   c[0] = c[0] / mag;
   c[1] = c[1] / mag;
   c[2] = c[2] / mag;
}

/*
    Generalized routine to return the numbers of all pixels adjoining the
  input pixel at the given resolution.
  This routine will work for resolutions up to 15.

  Method:
     1.  All eight adjoining pixels are found by manipulation of the
         cartesian floatinates of the input pixel in the plane of the
         face on which it lies.  See UPX_4_NEIGHBORS opening comments 
         for a detailed description of this process.

     2.  Once all eight neighbors are found, the NEIGHBORS array is sorted
         and duplicate pixel numbers are deleted.

     3.  The number of unique neighbors and the array of pixel numbers
         are returned.
*/
int upx_8_neighbors(pixel,res,neighbors,number_of_neighbors)
int *pixel;                /* Pixel number */
short *res;                  /* Resolution */
int neighbors[8];         /* Neighboring pixel numbers */
int *number_of_neighbors;  /* Number of adjoining pixels (4 - 8)*/
{
int face;                    /* Face number of PIXEL */
int nface;                   /* Face number of neighbor */
int pixels_per_face;         /* Pixels on a face */
int rpixel;                  /* Relative pixel number on face */
int two = 2;                  
int four = 4;                 
int two14 = 16384;          /* 2**14 */
int two28 = 268435456;     /* 2**28 */
int divisor;      /* Converts resolution 15 pixel number to input resolution */
int res_diff;     /* Difference between 15 and input resolution */
int distance;     /* Distance between pixel and neighbor at resolution 15 */
int ix,iy;        /* Pixel cart. floatinates on cube face */
int jxhi,jxlo,jyhi,jylo;
int ip,id;	/* Var. for breaking down PIXEL */
int jx,jy;      /* Neighbor cart. floatinates on cube face */
static int ixtab[128] = { 0 };
static int iytab[128] = { 0 }; /* Bit tables for converting pixel number on 
      				face to cartesian floatinates */
int temp_pixel; /* Pixel number holding place */
int i,j, foo, tmp;  /* Sort loop variables */
short oof = 128;
int upx_bit_table_set(), edgchk(), power();

   if( ixtab[127] == 0 ) upx_bit_table_set(ixtab,iytab,&oof);

      pixels_per_face = power(two,2*(*res - 1));
      face = *pixel / pixels_per_face;
      rpixel = *pixel - face*pixels_per_face;
      res_diff = 15 - *res;
      divisor  = power(four,res_diff);
      distance = power(two,res_diff);

      ix = 0;
      iy = 0;
      ip = 1;
/*
  Break pixel number down into constituent x,y floatinates:
*/ 
      while( rpixel != 0)
      {
          id = rpixel % 2;
          rpixel = rpixel / 2;
          ix = id * ip + ix;

          id = rpixel % 2;
          rpixel = rpixel / 2;
          iy = id * ip + iy;

          ip = 2 * ip;

       }
/*
  Convert x,y floatinates of pixel in initial resolution to resolution
  of 15:
*/
      ix = ix * distance;
      iy = iy * distance;
/*                   
  Calculate floatinates of each neighbor, check for edges, and return pixel
  number in appropriate array element:
*/
      tmp = ix - distance;
      foo = iy - distance;
      edgchk(&face,&tmp,&foo,&two14,&nface,&jx,&jy); /* Bottom-Left */
      jxhi = jx / 128;
      jxlo = jx % 128;
      jyhi = jy / 128;
      jylo = jy % 128;
      neighbors[0] = nface * two28 + 
                   ixtab[jxlo] + iytab[jylo] +
                   two14 * (ixtab[jxhi] + iytab[jyhi]);
      neighbors[0] = neighbors[0] / divisor;

      tmp = iy - distance;
      edgchk(&face,&ix,&tmp,&two14,&nface,&jx,&jy); /* Bottom */
      jxhi = jx / 128;
      jxlo = jx % 128;
      jyhi = jy / 128;
      jylo = jy % 128;
      neighbors[1] = nface * two28 + 
                   ixtab[jxlo] + iytab[jylo] +
                   two14 * (ixtab[jxhi] + iytab[jyhi]);
      neighbors[1] = neighbors[1] / divisor;

      tmp = ix + distance;
      foo = iy - distance;
      edgchk(&face,&tmp,&foo,&two14,&nface,&jx,&jy); /* Bottom-Right */
      jxhi = jx / 128;
      jxlo = jx % 128;
      jyhi = jy / 128;
      jylo = jy % 128;
      neighbors[2] = nface * two28 + 
                   ixtab[jxlo] + iytab[jylo] +
                   two14 * (ixtab[jxhi] + iytab[jyhi]);
      neighbors[2] = neighbors[2] / divisor;

      tmp = ix + distance;
      edgchk(&face,&tmp,&iy,&two14,&nface,&jx,&jy); /* Right */
      jxhi = jx / 128;
      jxlo = jx % 128;
      jyhi = jy / 128;
      jylo = jy % 128;
      neighbors[3] = nface * two28 + 
                   ixtab[jxlo] + iytab[jylo] +
                   two14 * (ixtab[jxhi] + iytab[jyhi]);
      neighbors[3] = neighbors[3] / divisor;

      tmp = ix + distance;
      foo = iy + distance;
      edgchk(&face,&tmp,&foo,&two14,&nface,&jx,&jy);  /* Top-Right */
      jxhi = jx / 128;
      jxlo = jx % 128;
      jyhi = jy / 128;
      jylo = jy % 128;
      neighbors[4] = nface * two28 + 
                   ixtab[jxlo] + iytab[jylo] +
                   two14 * (ixtab[jxhi] + iytab[jyhi]);
      neighbors[4] = neighbors[4] / divisor;

      tmp = iy + distance;
      edgchk(&face,&ix,&tmp,&two14,&nface,&jx,&jy); /* Top */
      jxhi = jx / 128;
      jxlo = jx % 128;
      jyhi = jy / 128;
      jylo = jy % 128;
      neighbors[5] = nface * two28 + 
                   ixtab[jxlo] + iytab[jylo] +
                   two14 * (ixtab[jxhi] + iytab[jyhi]);
      neighbors[5] = neighbors[5] / divisor;

      tmp = ix - distance;
      foo = iy + distance;
      edgchk(&face,&tmp,&foo,&two14,&nface,&jx,&jy); /* Top-Left */
      jxhi = jx / 128;
      jxlo = jx % 128;
      jyhi = jy / 128;
      jylo = jy % 128;
      neighbors[6] = nface * two28 + 
                   ixtab[jxlo] + iytab[jylo] +
                   two14 * (ixtab[jxhi] + iytab[jyhi]);
      neighbors[6] = neighbors[6] / divisor;

      tmp = ix - distance;
      edgchk(&face,&tmp,&iy,&two14,&nface,&jx,&jy); /* left */
      jxhi = jx / 128;
      jxlo = jx % 128;
      jyhi = jy / 128;
      jylo = jy % 128;
      neighbors[7] = nface * two28 + 
                   ixtab[jxlo] + iytab[jylo] +
                   two14 * (ixtab[jxhi] + iytab[jyhi]);
      neighbors[7] = neighbors[7] / divisor;

/* any duplicates ? */
      *number_of_neighbors = 8;
      for( i = 0; i < 8; i++ )
      {
         temp_pixel = neighbors[i];
         for( j = i+1; j < 8; j++)
            if( temp_pixel == neighbors[j] ) (*number_of_neighbors)--;
      }

      return;
}

/*
check for ix, iy being over the edge of a face
returns corrctng face, ix, iy in mface,jx,jy
*/
int edgchk(nface,ix,iy,max,mface,jx,jy)
int *nface, *ix, *iy, *max, *mface, *jx, *jy;
{
   if( *ix < 0 )
   {
      switch( *nface )
      {
         case 0:
	    *mface = 4;
	    *jy = *ix + *max;
	    *jx = *max - 1 - *iy;
	    return;

      	 case 1:
	    *mface = 4;
	    *jx = *max + *ix;
	    *jy = *iy;
	    return;

      	 case 2:
	    *mface = *nface - 1;
	    *jx = *max + *ix;
	    *jy = *iy;
	    return;

      	 case 3:
	    *mface = *nface - 1;
	    *jx = *max + *ix;
	    *jy = *iy;
	    return;

      	 case 4:
	    *mface = *nface - 1;
	    *jx = *max + *ix;
	    *jy = *iy;
	    return;

         case 5:
	    *mface = 4;
	    *jx = *iy;
	    *jy = -(*ix) - 1;
	    return;

         default:
      	    printf("UPX_EDGCHK: aborting due to bad face number\n"); 
      	    exit(0);
      }
   }

   if( *ix >= *max )
   {
      switch( *nface )
      {
         case 0:
	    *mface = 2;
	    *jx = *iy;
	    *jy = 2 * (*max) - 1 - (*ix);
	    return;

         case 1:
	    *mface = *nface + 1;
	    *jy = *iy;
	    *jx = *ix - *max;
	    return;

         case 2:
	    *mface = *nface + 1;
	    *jy = *iy;
	    *jx = *ix - *max;
	    return;

         case 3:
	    *mface = *nface + 1;
	    *jy = *iy;
	    *jx = *ix - *max;
	    return;

         case 4:
	    *mface = 1;
	    *jy = *iy;
	    *jx = *ix - *max;
	    return;

         case 5:
	    *mface = 2;
	    *jx = *max - 1 - (*iy);
	    *jy = *ix - (*max);
	    return;

         default:
      	    printf("UPX_EDGCHK: aborting due to bad face number\n"); 
      	    exit(0);
      }
   }

   if( *iy < 0 )
   {
      switch( *nface )
      {
         case 0:
	    *mface = 1;
	    *jy = *iy + *max;
	    *jx = *ix;
	    return;

         case 1:
	    *mface = 5;
	    *jy = *iy + *max;
	    *jx = *ix;
	    return;

         case 2:
	    *mface = 5;
	    *jx = *iy + *max;
	    *jy = *max - 1 - (*ix);
	    return;

         case 3:
	    *mface = 5;
	    *jx = *max - 1 - (*ix);
	    *jy = -(*iy) - 1;
	    return;

         case 4:
	    *mface = 5;
	    *jx = -(*iy) - 1;
	    *jy = *ix;
	    return;

         case 5:
	    *mface = 3;
	    *jx = *max - 1 - (*ix);
	    *jy = -(*iy) - 1;
	    return;

         default:
      	    printf("UPX_EDGCHK: aborting due to bad face number\n"); 
      	    exit(0);
      }
   }

   if( *iy >= *max )
   {
      switch( *nface )
      {
         case 0:
	    *mface = 3;
	    *jx = *max - 1 - (*ix);
	    *jy = 2 * (*max) - 1 - (*iy);
	    return;

         case 1:
	    *mface = 0;
	    *jy = *iy - (*max);
	    *jx = *ix;
	    return;

         case 2:
	    *mface = 0;
	    *jx = 2 * (*max) - 1 - (*iy);
	    *jy = *ix;
	    return;

         case 3:
	    *mface = 0;
	    *jx = *max - 1 - (*ix);
	    *jy = 2 * (*max) - 1 - (*iy);
	    return;

         case 4:
	    *mface = 0;
	    *jx = *iy - (*max);
	    *jy = *max - 1 - (*ix);
	    return;

         case 5:
	    *mface = 1;
	    *jx = *ix;
	    *jy = *iy - (*max);
	    return;

         default:
      	    printf("UPX_EDGCHK: aborting due to bad face number\n"); 
      	    exit(0);
      }
   }

/* last but not least, if we get here: */

   *mface = *nface;
   *jx = *ix;
   *jy = *iy;
   return;
}


float rabs(a)
float a;
{
   float b;

   if( a >= 0.0 ) 
      b = a;
   else
      b = -a;

   return( b ); 
}

int power(x,n)
int x, n;
{
   int p;

   for( p = 1; n > 0; --n )
      p = p * x;

   return( p );
}


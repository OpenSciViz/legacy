#include "ray.h"
TEXTURE *ball_texture(norm,ball)
float norm[3]; 
SPHERE *ball;
{
   TEXTURE *texture;
   char check, check_pointing();
   int pix_per_face = 256*256;
   short res = 9; /* dmr or firas resolution=6, dirbe=9 */
   int sky_pixel;
   int upx_pixel_number();
   float rot_norm[3], lat, lon;
   int lat_idx, lon_idx;
 
   texture = NULL;
   if( ball->id < 0 ) return( texture );

   texture = ball->texture;
   texture->index = ball->id;

/* test pattern
   texture->red = fabs(norm[0]) * 255.0;
   texture->green = fabs(norm[1]) * 255.0;
   texture->blue = fabs(norm[2]) * 255.0;
*/
   rot_norm[0] = norm[0];
   rot_norm[1] = norm[1];
   rot_norm[2] = norm[2];

   if( ball->id == 0 ) /* outer sphere texturemap is in skymap format */
   {
     if( rot_ang[ball->id] > 0.0 )
     {
       rot_norm[0] = GALrot[0][0]*norm[0] + GALrot[0][1]*norm[1] + 
      			GALrot[0][2]*norm[2];
       rot_norm[1] = GALrot[1][0]*norm[0] + GALrot[1][1]*norm[1] + 
      			GALrot[1][2]*norm[2];
       rot_norm[2] = GALrot[2][0]*norm[0] + GALrot[2][1]*norm[1] + 
      			GALrot[2][2]*norm[2];
     }
/*      upx_pixel_number(rot_norm,&res,&sky_pixel); C version */
/* use fortran obj module: */
	upx_pixel_number_(rot_norm,&res,&sky_pixel);

/* put this directly into the texture map */
      texture->index = sky_pixel;
/*
      check = check_pointing(sky_pixel,n_dirbe_pix,dirbe_pointing); 
*/
     if( NCHAN == 3 )
     {
        texture->red = sky_map[3*sky_pixel];
        texture->green = sky_map[3*sky_pixel + 1];
        texture->blue = sky_map[3*sky_pixel + 2];
     }
     else
     {
        texture->red = sky_map[sky_pixel];
        texture->green = sky_map[sky_pixel];
        texture->blue = sky_map[sky_pixel];
/*
        if( sky_map[sky_pixel] == 0 )
        {
          texture->red = 0;
          texture->green = 0;
          texture->blue = 0;
        }
        else
          color_ramp(sky_map[sky_pixel],texture);
*/
     }
   }
   else if( ball->id == 1 ) /* inner sphere is the world-map texture pattern */
   {
     if( rot_ang[ball->id] > 0.0 )
     {
       rot_norm[0] = EQrot[0][0]*norm[0] + EQrot[0][1]*norm[1] + 
      			EQrot[0][2]*norm[2];
       rot_norm[1] = EQrot[1][0]*norm[0] + EQrot[1][1]*norm[1] + 
      			EQrot[1][2]*norm[2];
       rot_norm[2] = EQrot[2][0]*norm[0] + EQrot[2][1]*norm[1] + 
      			EQrot[2][2]*norm[2];
     }
      lat = acos( rot_norm[2] ); /* 0 to PI */
      if( rot_norm[0] != 0.0 )
         lon = atan2(rot_norm[0], rot_norm[1]) + 3.1415926; /* 0 to 2PI */
      else
         lon = 0.0;
/* note that lat = lon = 0 in our world map is coord [255][512] */
      lat_idx = 255 + 256*(lat - 3.1415926/2) / (3.1415926/2);
      lon_idx = 512 + 512*(lon - 3.1415926) / 3.1415926; /* this needs to be 
reversed: */
      lon_idx = 1023 - lon_idx;
      if( world_map[lat_idx][lon_idx] > 0 ) /* brown for land */
      {
        texture->red = 255;
        texture->green = 150;
        texture->blue = 25;
      }
      else	/* blue-green for water */
      {
        texture->red = 0;
        texture->green = 175;
        texture->blue = 255;
      }
/*
      texture->red = world_map[lat_idx][lon_idx];
      texture->green = world_map[lat_idx][lon_idx];
      texture->blue = world_map[lat_idx][lon_idx];
      color_ramp(world_map[lat_idx][lon_idx],texture);
*/
   }



   return( texture );
}


/*	STDIO - UNIX 'Standard I/O' Definitions 	 */
# ifndef __FILE
# define __FILE 1

# define _NFILE 20
# define BUFSIZ 512

	extern	struct	_iobuf 
		{
		int	_cnt;
		char	*_ptr;
		char	*_base;
		char	_flag;

#define	_IOREAD		0x01
#define	_IOWRT		0x02
#define	_IONBF		0x04
#define	_IOMYBUF	0x08
#define	_IOEOF		0x10
#define	_IOERR		0x20
#define	_IOSTRG		0x40
#define _IORW		0x80

		char	_file;

		};

typedef struct _iobuf*	FILE;
extern noshare	FILE	*stdin, *stdout, *stderr;

#define	NULL		0
#define	EOF		(-1)
#define	TRUE		1
#define	FALSE		0

#define L_ctermid	64
#define L_cuserid	16
#define L_tmpnam	256
#define L_lcltmpnam	256
#define L_nettmpnam	256

#define getc(p)		fgetc(p)
#define getchar()	fgetc(stdin)
#define putc(x,p)	fputc(x,p)
#define putchar(x)	fputc(x,stdout)
#define feof(p)		(((*p)->_flag&_IOEOF)!=0)
#define ferror(p)	(((*p)->_flag&_IOERR)!=0)
#define fileno(p)	((*p)->_file)
#define clearerr(p)	((*p)->_flag &= ~(_IOERR|_IOEOF))

char	*fgets(),*fgetname();

long	ftell();

FILE	*fopen(), *fdopen(), *freopen();

# endif

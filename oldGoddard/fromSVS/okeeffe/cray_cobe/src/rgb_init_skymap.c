
#define N_CELLS 6*256*256 

#include <math.h>
#include <sys/file.h>
#include <string.h>

int init_skymap(chan,file,skymap)
short chan;
char *file;
unsigned char skymap[];
{
  int fd=0, nb, i, j, sz=65536, cnt=0;
  float scale;
  float min, max, mean, rms;
  char *infile;
  unsigned char buff[65536];

  infile = strdup(file);
  strcat(infile,"25.byte.dirbe");
  printf("init_skymap> reading skymap file: %s\n",infile);
  fd = open(infile,0);
  if( fd <= 0 ) printf("init_skymap> unable to open zodi skymap\n");

  i = 0; nb = 1;
  while( (i < 3*393216) && (nb > 0) )
  {
    nb = read(fd,buff,sz);
    if( nb > 0 )
      for( j = 0; (j < nb) && (i < 3*393216); j++, i = i + 3 ) 
	skymap[i] = buff[j];
  }
  close( fd );
  free(infile);

  infile = strdup(file);
  strcat(infile,"60.byte.dirbe");
  printf("init_skymap> reading skymap file: %s\n",infile);
  fd = open(infile,0);
  if( fd <= 0 ) printf("init_skymap> unable to open zodi skymap\n");

  i = 1; nb = 1;
  while( (i < 3*393216) && (nb > 0) )
  {
    nb = read(fd,buff,sz);
    if( nb > 0 )
      for( j = 0; (j < nb) && (i < 3*393216); j++, i = i + 3 ) 
	skymap[i] = buff[j];
  }
  close( fd );
  free(infile);
  
  infile = strdup(file);
  strcat(infile,"100.byte.dirbe");
  printf("init_skymap> reading skymap file: %s\n",infile);
  fd = open(infile,0);
  if( fd <= 0 ) printf("init_skymap> unable to open zodi skymap\n");

  i = 2; nb = 1;
  while( (i < 3*393216) && (nb > 0) )
  {
    nb = read(fd,buff,sz);
    if( nb > 0 )
      for( j = 0; (j < nb) && (i < 3*393216); j++, i = i + 3 ) 
	skymap[i] = buff[j];
  }
  close( fd );
  free(infile);
  
  return( i );
}    

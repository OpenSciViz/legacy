#include "ray.h"
SURFACE *hit_cone(con,emin,ray)
CONE *con;
float emin[3], ray[3];
{
    SURFACE *hit;
    float c_axis[3], cvec[3];
    float ec, rc, er, rt, ct, te, ee, tt, rr, cc;
    float mag, strike[3], normal[3];
    float sa, sb, s; /* stretch factor */
    float cosa;
    float a, b, c;

    hit = NULL;
    cosa = cos(atan(con->r/con->h));
    cosa = cosa * cosa; /* since the square is the only thing we need */

/* all possible dot products will be needed */
    rc = ray[0]*con->axis->x + ray[1]*con->axis->y + ray[2]*con->axis->z;
    er = ray[0]*emin[0] + ray[1]*emin[1] + ray[2]*emin[2];
    ec = emin[0]*con->axis->x + emin[1]*con->axis->y + emin[2]*con->axis->z;
    rt = ray[0]*con->top->x + ray[1]*con->top->y + ray[2]*con->top->z;
    ct = con->axis->x*con->top->x + con->axis->y*con->top->y + con->axis->z*con->top->z;
    te = emin[0]*con->top->x + emin[1]*con->top->y + emin[2]*con->top->z;
    ee = emin[0]*emin[0] + emin[1]*emin[1] + emin[2]*emin[2];
    tt = con->top->x*con->top->x + con->top->y*con->top->y + 
         con->top->z*con->top->z;
    rr = ray[0]*ray[0] + ray[1]*ray[1] + ray[2]*ray[2];

/* the following algorithm assumes the emination point (emin) of the ray
   lies outside the surface of the cone, & the ray strikes the outside:
   ie there are two non-negative solutions to the quadratic, and choosing
   the nearest means we have struck the outside of the cone and need to
   calculate the surface normal accordingly. if the ray eminates from
   inside the cone, there should only be one non-negative intersection.
   although it is also possible to have only one non-negative soln. from
   a ray eminating outside the cone. all negative solns. are presumably
   due to scenarios with no true intersection or only one. determining whether
   the intersection is inside or outside the cone is another matter. */
/* using quadratic equation: ax^2 + bx + c = 0 soln:
      x = -b/2a +- sqrt(b^2 - 4ac)/2a, where
      a = ray^2 - (c_axis*ray)^2
      b = 2*(ray.emin - ray.tip - (ray.c_axis)(emin.c_axis - tip.c_axis)
      c = emin^2 + tip^2 + 2*(emin.c_axis)(tip.c_axis) - (emin.c_axis)^2 -
      	  (tip.c_axis)^2 - 2*emin.tip	*/

    a = rr - rc*rc;
    if( rabs(a) < 0.0000001 ) 
      	return( hit );	/* otherwise we have something un-solvable */
        
    b = 2.0 * (er - rt - rc*ec + rc*ct); 
    c = ee + tt + 2.0*ec*ct - ec*ec - ct*ct - 2.0*te;

/* stretch factor is nearest positive soln to quadratic equation above */
    sa = -b/a/2.0 - sqrt(b*b - 4.0*a*c)/2.0;
    sb = -b/a/2.0 + sqrt(b*b - 4.0*a*c)/2.0;
    if( (sa < 0) && (sb < 0) ) return( hit );
    if( (sa > 0)  && (sb < 0) ) s = sa;
    if( (sb > 0)  && (sa < 0) ) s = sb;
    if( (sa > 0) && (sb > 0) ) 
    {
        if( sa > sb )
	    s = sb;
        else
	    s = sa;
    }
    if( s > VISIBILITY ) return( hit );

    strike[0] = s * ray[0] + emin[0];
    strike[1] = s * ray[1] + emin[1];
    strike[2] = s * ray[2] + emin[2];
    cvec[0] = strike[0] - con->top->x;
    cvec[1] = strike[1] - con->top->y;
    cvec[2] = strike[2] - con->top->z;
    mag = sqrt(cvec[0]*cvec[0] + cvec[1]*cvec[1] + cvec[2]*cvec[2]);
    cvec[0] = cvec[0]/mag;
    cvec[1] = cvec[1]/mag;
    cvec[2] = cvec[2]/mag;
    cc = cvec[0]*con->axis->x + cvec[1]*con->axis->y + cvec[2]*con->axis->z;

/* if we get here we need to allocate a surface structure to return */
    hit = (SURFACE *) malloc(sizeof(SURFACE));
    hit->type = &descr_cone[0];
    hit->id = con->id;
    hit->pos.x = strike[0];
    hit->pos.y = strike[1];
    hit->pos.z = strike[2];
/* to calc the normal of the cone at strike, first define u-vec on surface
   of cone directed towards intersection */
/* normal on the outside of the cone: */
    normal[0] = cvec[0] - 2.0*cc*con->axis->x;
    normal[1] = cvec[1] - 2.0*cc*con->axis->y;
    normal[2] = cvec[2] - 2.0*cc*con->axis->z;
    if( (normal[0]*ray[0] + normal[1]*ray[1] + normal[2]*ray[2]) < 0.0 )
    {	/* surface normal & ray directions should be in opposite directions */
        hit->norm.x = normal[0];
        hit->norm.y = normal[1];
        hit->norm.z = normal[2];
    }
    else	/* normal on the inside of the cone: */
    {
        hit->norm.x = cvec[0] + 2.0*cc*con->axis->x;
        hit->norm.y = cvec[1] + 2.0*cc*con->axis->y;
        hit->norm.z = cvec[2] + 2.0*cc*con->axis->z;
    }
    mag = sqrt(hit->norm.x*hit->norm.x + hit->norm.y*hit->norm.y +
		hit->norm.z*hit->norm.z);
    hit->norm.x = hit->norm.x / mag;
    hit->norm.y = hit->norm.y / mag;
    hit->norm.z = hit->norm.z / mag;
    hit->refl = con->refl;
    hit->tran = con->tran;
    return( hit );
}

#include "ray.h"
/* one ball and three square mirrors */
int iras_scene()
{
/* make use of these globals:
global POLY *polys[N_POLYS];
global VERTEX_LIST *verts[N_VERTS];
global REFLECT *refl[N_SURFS];
global int NB, NP;
*/
int i;

NFXY = 0;
NCYL = 0;
NCON = 0;
NB = 2;
NP = 3;

init_world_map("claips:[aips.other]globe.pixmap",world_map);
n_dirbe_pix = 4*8*60*103; /* 8 measurement/sec for 4*103 minutes == 4 orbits */
init_iras_map("claips:[aips.other.iras]iras_12micron.dirbe",iras_map,
      			n_dirbe_pix,dirbe_pointing);

/* diffuse light is also white, but not as bright */
diffuse.red = 64;
diffuse.green = 64;
diffuse.blue = 64;

/* a gray surface with NO specular reflection */
   refl[0] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[0]->spec_pwr = -1.0;
   refl[0]->red_coeff = 1.0;
   refl[0]->green_coeff = 1.0;
   refl[0]->blue_coeff = 1.0;
/* a gray surface with specular reflection */
   refl[1] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[1]->spec_pwr = 10.0;
   refl[1]->red_coeff = .6;
   refl[1]->green_coeff = 0.6;
   refl[1]->blue_coeff = 0.6;
/* a yellow surface */
   refl[2] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[2]->spec_pwr = 1.0;
   refl[2]->red_coeff = 0.5;
   refl[2]->green_coeff = 0.5;
   refl[2]->blue_coeff = 0.5;
/* a magenta surface */
   refl[3] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[3]->spec_pwr = 1.0;
   refl[3]->red_coeff = 0.9;
   refl[3]->green_coeff = 0.1;
   refl[3]->blue_coeff = 0.9;
/* a cyan surface */
   refl[4] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[4]->spec_pwr = 1.0;
   refl[4]->red_coeff = 0.1;
   refl[4]->green_coeff = 0.9;
   refl[4]->blue_coeff = 0.9;

/* place the earth sphere inside the celestial sphere */
   balls[0].r = 1.0;
   balls[0].id = 0;
   balls[0].thick = 0;
   balls[0].refl = NULL;	
   balls[0].tran = NULL;
   balls[0].texture = (TEXTURE *) malloc(sizeof(TEXTURE));
   balls[0].texture->index = -1.0;
   balls[0].texture->red = 0.0;
   balls[0].texture->green = 0.0;
   balls[0].texture->blue = 0.0;
   balls[0].x = 0.0;
   balls[0].y = 0.0;
   balls[0].z = 0.0;

/* place the celestial sphere in the center */
   balls[1].r = 2.5;
   balls[1].id = 101;
   balls[1].thick = 0.1;
   balls[1].refl = refl[1];
   balls[1].tran = (REFRACT *) malloc(sizeof(REFRACT)); 
   balls[1].tran->refract_indx = 1.5;
   balls[1].tran->red_coeff = 0.25;
   balls[1].tran->green_coeff = 0.25;
   balls[1].tran->blue_coeff = 0.25;
   balls[1].texture = (TEXTURE *) malloc(sizeof(TEXTURE));
   balls[1].texture->index = -1;
   balls[1].texture->red = 0.0;
   balls[1].texture->green = 0.0;
   balls[1].texture->blue = 0.0;
   balls[1].x = 0.0;
   balls[1].y = 0.0;
   balls[1].z = 0.0;

/* init light source postion just outside the celestial sphere */
lamp.z = 1.1 * balls[1].r * 0.1825408;
lamp.y = 1.1 * balls[1].r * 0.9831982;
lamp.x = 1.1 * balls[1].r * 0.000011166;
/* lamp is white */
lamp.intens.red = 255;
lamp.intens.green = 255;
lamp.intens.blue = 255;

   polys[0] = (POLY *) malloc(sizeof(POLY));
   polys[1] = (POLY *) malloc(sizeof(POLY));
   polys[2] = (POLY *) malloc(sizeof(POLY));
/* since none of the polygons share vertices in this scene, need to allocate
   12 distinct vertices */
   for( i=0; i<12; i++ )
   {
      verts[i] = (VERTEX_LIST *) malloc(sizeof(VERTEX_LIST));
      coords[i] = (TRIPLET *) malloc(sizeof(TRIPLET));
   }

/* root polygon is bottom square: */
   polys[0]->id = 0;
   polys[0]->n_vert = 4;
   polys[0]->norm.x = 0;
   polys[0]->norm.y = 0;
   polys[0]->norm.z = 1;
   polys[0]->tran = NULL;
   polys[0]->refl = refl[0]; /* gray */
   polys[0]->texture = NULL;
   polys[0]->vertex = verts[0];
   verts[0]->coord = coords[0];
   coords[0]->x = -4;
   coords[0]->y = -4;
   coords[0]->z = -3.0;
   verts[0]->next = verts[1];
   verts[1]->coord = coords[1];
   coords[1]->x = 5.5;
   coords[1]->y = -4;
   coords[1]->z = -3.0;
   verts[1]->next = verts[2];
   verts[2]->coord = coords[2];
   coords[2]->x = 5.5;
   coords[2]->y = 5.5;
   coords[2]->z = -3.0;
   verts[2]->next = verts[3];
   verts[3]->coord = coords[3];
   coords[3]->x = -4;
   coords[3]->y = 5.5;
   coords[3]->z = -3.0;
   verts[3]->next = verts[0];

/* next polygon: */   
   polys[1]->id = 1;
   polys[1]->n_vert = 4;
   polys[1]->norm.x = 0;
   polys[1]->norm.y = 1;
   polys[1]->norm.z = 0;
   polys[1]->tran = NULL;
   polys[1]->refl = refl[0]; /* gray */
   polys[1]->texture = NULL;
   polys[1]->vertex = verts[4];
   verts[4]->coord = coords[4];
   coords[4]->x = -3.75;
   coords[4]->y = -4.0;
   coords[4]->z = -2.5;
   verts[4]->next = verts[5];
   verts[5]->coord = coords[5];
   coords[5]->x = -3.75;
   coords[5]->y = -4.0;
   coords[5]->z = 5;
   verts[5]->next = verts[6];
   verts[6]->coord = coords[6];
   coords[6]->x = 5.5;
   coords[6]->y = -4.0;
   coords[6]->z = 5;
   verts[6]->next = verts[7];
   verts[7]->coord = coords[7];
   coords[7]->x = 5.5;
   coords[7]->y = -4.0;
   coords[7]->z = -2.5;
   verts[7]->next = verts[4];

/* next polygon: */   
   polys[2]->id = 2;
   polys[2]->n_vert = 4;
   polys[2]->norm.x = 1;
   polys[2]->norm.y = 0;
   polys[2]->norm.z = 0;
   polys[2]->tran = NULL;
   polys[2]->refl = refl[0]; /* gray */
   polys[2]->texture = NULL;
   polys[2]->vertex = verts[8];
   verts[8]->coord = coords[8];
   coords[8]->x = -4.0;
   coords[8]->y = -3.75;
   coords[8]->z = -2.5;
   verts[8]->next = verts[9];
   verts[9]->coord = coords[9];
   coords[9]->x = -4.0;
   coords[9]->y = 5.5;
   coords[9]->z = -2.5;
   verts[9]->next = verts[10];
   verts[10]->coord = coords[10];
   coords[10]->x = -4.0;
   coords[10]->y = 5.5;
   coords[10]->z = 5;
   verts[10]->next = verts[11];
   verts[11]->coord = coords[11];
   coords[11]->x = -4.0;
   coords[11]->y = -3.75;
   coords[11]->z = 5;
   verts[11]->next = verts[8];
}

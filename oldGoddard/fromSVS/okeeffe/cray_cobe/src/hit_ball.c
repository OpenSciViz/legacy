#include "ray.h"
SURFACE *hit_ball(ball,base,ray)
SPHERE *ball;
float base[3], ray[3];
{
    SURFACE *hit;
    TEXTURE *ball_texture();
    float dist, distsqr, costh, tmp, lnear, lfar, rmag;
    float bvec[3], strike[3], normal[3];
    float elev;

#ifdef DEBUG
printf("hit_ball> checking for hit with ball # %d\n",ball->id);
#endif
    hit = NULL;
/* find distance to ball center */
    distsqr = (ball->x - base[0]) * (ball->x - base[0]) + 
		(ball->y - base[1]) * (ball->y - base[1]) +
		(ball->z - base[2]) * (ball->z - base[2]);
    dist = sqrt( distsqr );
    bvec[0] = (ball->x - base[0]) / dist;
    bvec[1] = (ball->y - base[1]) / dist;
    bvec[2] = (ball->z - base[2]) / dist;
/* angle between ray & direction of ball center */
    costh = bvec[0]*ray[0] + bvec[1]*ray[1] + bvec[2]*ray[2];
    tmp = ball->r*ball->r - distsqr * (1-costh*costh); 

    if( dist > ball->r ) /* ray is outside or on ball */
    {
	if( costh <= 0.0 ) 
	    return( hit );
	if( tmp <= 0.0 )
	    return( hit );
/* if we get here, we have a hit */
#ifdef DEBUG
printf("hit_ball> hit ball # %d\n",ball->id);
#endif
	tmp = sqrt( tmp );
	lnear = dist * costh - tmp;
/*	lfar = dist * costh + tmp; */

	strike[0] = base[0] + lnear * ray[0];
	strike[1] = base[1] + lnear * ray[1];
	strike[2] = base[2] + lnear * ray[2];
/* construct normal of ball at point of intersection (strike) */
        normal[0] = strike[0] - ball->x;
        normal[1] = strike[1] - ball->y;
        normal[2] = strike[2] - ball->z;
        rmag = sqrt( normal[0]*normal[0] + normal[1]*normal[1] + normal[2]*normal[2] );
        normal[0] = normal[0]/rmag;
        normal[1] = normal[1]/rmag;
        normal[2] = normal[2]/rmag;
    }
    else /* emamation point is inside the ball */
    {
	if( tmp < 0.0 )
	{
	  printf("hit_ball> bad soln. from inside ball # %d\n",ball->id);
	  return( hit );
	}
	lnear = dist*costh + sqrt( tmp );
 	strike[0] = base[0] + lnear * ray[0];
	strike[1] = base[1] + lnear * ray[1];
	strike[2] = base[2] + lnear * ray[2];
/* if this is not the right choice, use the other solution of the quadratiC */
	if( ((strike[0]-base[0])*ray[0] + (strike[1]-base[1])*ray[1] + 
		(strike[2]-base[2])*ray[2]) < 0.0 )
	{
	  lfar = dist*costh - sqrt( tmp );
 	  strike[0] = base[0] + lfar * ray[0];
	  strike[1] = base[1] + lfar * ray[1];
	  strike[2] = base[2] + lfar * ray[2];
	}
        normal[0] = strike[0] - ball->x;
        normal[1] = strike[1] - ball->y;
        normal[2] = strike[2] - ball->z;
        rmag = sqrt( normal[0]*normal[0] + normal[1]*normal[1] + normal[2]*normal[2] );
/*
        normal[0] = -normal[0]/rmag;
        normal[1] = -normal[1]/rmag;
        normal[2] = -normal[2]/rmag;
*/
        normal[0] = normal[0]/rmag;
        normal[1] = normal[1]/rmag;
        normal[2] = normal[2]/rmag;
    }

/* if we get this far, we have a hit. now allocate the surface structure and set values */
    hit = (SURFACE *) malloc(sizeof(SURFACE));
    if( ball->texture != NULL )
       hit->texture = ball_texture(normal,ball); 
    else
       hit->texture = NULL;

    hit->type = descr_ball;
    hit->id = ball->id;
    hit->thick = ball->thick;
    hit->pos.x = strike[0];
    hit->pos.y = strike[1];
    hit->pos.z = strike[2];
    hit->norm.x = normal[0];
    hit->norm.y = normal[1];
    hit->norm.z = normal[2];
    hit->refl = ball->refl;
    hit->tran = ball->tran;
/*
    if( ball->tran != NULL ) this is more work 
    {
      if( ball->thick = 0.0 )  solid 
      {

      }
      else	think of it as two concentric spheres 
      {
         t = ball->thick * ball->r; radial dist. between surfaces 
      }
    }
*/
      
    return( hit );
}


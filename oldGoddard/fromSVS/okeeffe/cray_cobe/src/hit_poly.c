 
#include "ray.h"
#define TWO_PI	6.28	/* something slightly less than 2*pi will do */
SURFACE *hit_poly(poly,base,dir)
POLY *poly;
float base[3], dir[3];
{
    SURFACE *hit;
    VERTEX_LIST *ivert, *iprev, *vnear, *vfar, *vprev, *vnext;
    TEXTURE *poly_texture();
    float dist, tmp;
    float strike[3];
    float distnear, distfar, stretch;
    float vec1[3], vec2[3], vec3[3];
    float ang_12, ang_13, ang_23;
    float cos_ang, tot_ang;
    int i;

#ifdef DEBUG
printf("hit_poly> checking for hit with poly # %d\n",ipol);
#endif
    hit = NULL;
/* first check orientation of polygon, if facing away from us, return */
   cos_ang = poly->norm.x * dir[0] + 
      		poly->norm.y * dir[1] +
      			poly->norm.z * dir[2];
   if( cos_ang >= 0.0 ) 
/* surface faces away from dir */
      return( hit );

/* the procedure for finding the dir intersection with the polygon
  is such:
  1.) stretch ray dir vector to intersect polygonal plane,
      strike = base + stretch*dir, with stretch factor defined by: */
    vec1[0] = poly->vertex->coord->x - base[0];
    vec1[1] = poly->vertex->coord->y - base[1];
    vec1[2] = poly->vertex->coord->z - base[2];
    tmp = poly->norm.x * vec1[0] +
	     	poly->norm.y * vec1[1] +
	    	   poly->norm.z * vec1[2];
    stretch = tmp / cos_ang;
    if( stretch <= 0.0 )
	return( hit );

    strike[0] = base[0] + stretch * dir[0];
    strike[1] = base[1] + stretch * dir[1];
    strike[2] = base[2] + stretch * dir[2];

/* 2.) find vertex nearest to strike vnear, and its two neighbors vprev & vnext
	as well as the distance between nearest vertex and farthest */
    distnear = (strike[0] - poly->vertex->coord->x) * 
      		(strike[0] - poly->vertex->coord->x) + 
		(strike[1] - poly->vertex->coord->y) * 
      		(strike[1] - poly->vertex->coord->y) +
		(strike[2] - poly->vertex->coord->z) * 
      		(strike[2] - poly->vertex->coord->z);
    distfar = distnear;
    vnear = poly->vertex;
    vfar = vnear;
    vprev = poly->vertex;
    vnext = poly->vertex->next->next;
    iprev = vprev;
    ivert = poly->vertex->next;
/* start the loop with the second vertex in the list */
    for( i = 1; i <= poly->n_vert; i++ ) /* init. distances & prev pointer*/
    {
	tmp = (strike[0] - ivert->coord->x) * (strike[0] - ivert->coord->x) + 
		(strike[1] - ivert->coord->y) * (strike[1] - ivert->coord->y) +
		(strike[2] - ivert->coord->z) * (strike[2] - ivert->coord->z);
	if( tmp >= distfar )
	{
	    distfar = tmp;
	    vfar = ivert;
	}
	if( tmp <= distnear )
	{
	    distnear = tmp;
	    vnear = ivert;
	    vprev = iprev;
            vnext = vnear->next;
	}
     
	iprev = ivert;
        ivert = ivert->next;
    }
/* use a bounding circle centered at nearest vertex of radius */
    dist = (vnear->coord->x - vfar->coord->x) * (vnear->coord->x - vfar->coord->x) +
	(vnear->coord->y - vfar->coord->y) * (vnear->coord->y - vfar->coord->y) +
	(vnear->coord->z - vfar->coord->z) * (vnear->coord->z - vfar->coord->z);
      if( distnear > dist ) 
	return( hit ); /*  stike is outside of bounding circle 
   equal to distance between nearest and farthest vertex */

/* 3. define vector from strike point to nearest vertex  */
    vec1[0] = vnear->coord->x - strike[0];
    vec1[1] = vnear->coord->y - strike[1];
    vec1[2] = vnear->coord->z - strike[2];
    dist = sqrt( vec1[0]*vec1[0]+vec1[1]*vec1[1]+vec1[2]*vec1[2] );
    vec1[0] = vec1[0]/dist;
    vec1[1] = vec1[1]/dist;
    vec1[2] = vec1[2]/dist;
 /* and vector from strike to prev. vertex */
    vec2[0] = vprev->coord->x - strike[0];
    vec2[1] = vprev->coord->y - strike[1];
    vec2[2] = vprev->coord->z - strike[2];
    dist = sqrt( vec2[0]*vec2[0]+vec2[1]*vec2[1]+vec2[2]*vec2[2] );
    vec2[0] = vec2[0]/dist;
    vec2[1] = vec2[1]/dist;
    vec2[2] = vec2[2]/dist;
/* and vector from strike to next vertex */
    vec3[0] = vnext->coord->x - strike[0];
    vec3[1] = vnext->coord->y - strike[1];
    vec3[2] = vnext->coord->z - strike[2];
    dist = sqrt( vec3[0]*vec3[0]+vec3[1]*vec3[1]+vec3[2]*vec3[2] );
    vec3[0] = vec3[0]/dist;
    vec3[1] = vec3[1]/dist;
    vec3[2] = vec3[2]/dist;
/* angle between 1 & 2 */
    cos_ang = vec1[0]*vec2[0]+vec1[1]*vec2[1]+vec1[2]*vec2[2];
    ang_12 = acos(cos_ang);
/* angle between 2 & 3 */
    cos_ang = vec2[0]*vec3[0]+vec2[1]*vec3[1]+vec2[2]*vec3[2];
    ang_23 = acos(cos_ang);
/* angle between 1 & 3 */
    cos_ang = vec1[0]*vec3[0]+vec1[1]*vec3[1]+vec1[2]*vec3[2];
    ang_13 = acos(cos_ang);
    tot_ang = ang_12 + ang_23 + ang_13;
    if( tot_ang < TWO_PI)
        return( hit ); /* strike outside of polygon if the sum of these
angles is not 'sufficiently' identical to 2 PI*/

/* if we get this far, we have a hit, now allocate the surface structure
   and set values */
#ifdef DEBUG
printf("hit_poly> hit poly # %d\n",poly->id);
#endif
	hit = (SURFACE *) malloc(sizeof(SURFACE));
/*
        if( poly->texture != NULL )
      	   hit->texture = poly_texture(strike,poly); 
        else
*/
        hit->texture = NULL; 
        hit->type = descr_poly; 
	hit->refl = poly->refl;
	hit->tran = poly->tran;
	hit->id = poly->id;
	hit->thick = poly->thick;
	hit->pos.x = strike[0];
	hit->pos.y = strike[1];
	hit->pos.z = strike[2];
	hit->norm.x = poly->norm.x;
	hit->norm.y = poly->norm.y;
	hit->norm.z = poly->norm.z;
    return( hit );
}


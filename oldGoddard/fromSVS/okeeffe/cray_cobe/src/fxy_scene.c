#include "ray.h"
int fxy_scene(filenm)
char *filenm;
{
/* makes use of:
global REFLECT *refl[N_SURFS];
global int NFXY;
global float *fxy_surf_ptr;
*/
FILE *fp, *fopen();
unsigned char byte;
int i=0;
float min=255.0, max=0.0;

NB = 0; NP = 0; NCON = 0; NCYL = 0;
NFXY = 1;

fxy_surf_ptr = (float *) malloc(512*512*sizeof(float));
fp = fopen(filenm,"r");

while( byte = getc(fp) != EOF )
{
   fxy_surf_ptr[i] = byte;
   if( max < byte ) max = byte;
   if( min > byte ) min = byte;
   i++;
}

/* init light source postion  */
lamp.x = -256.0;
lamp.y = -256.0;
lamp.z = 256.0;
lamp.r = 10.0;
/* lamp is white */
lamp.intens.red = 255;
lamp.intens.green = 255;
lamp.intens.blue = 255;
/* diffuse light is also white, but not as bright */
diffuse.red = 127;
diffuse.green = 127;
diffuse.blue = 127;

/* a gray surface */
   refl[0] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[0]->spec_pwr = 1.0;
   refl[0]->red_coeff = 0.5;
   refl[0]->green_coeff = 0.5;
   refl[0]->blue_coeff = 0.5;
/* a yellow surface */
   refl[1] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[1]->spec_pwr = 1.0;
   refl[1]->red_coeff = 0.9;
   refl[1]->green_coeff = 0.9;
   refl[1]->blue_coeff = 0.1;
/* a magenta surface */
   refl[2] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[2]->spec_pwr = 1.0;
   refl[2]->red_coeff = 0.9;
   refl[2]->green_coeff = 0.1;
   refl[2]->blue_coeff = 0.9;
/* a cyan surface */
   refl[3] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[3]->spec_pwr = 1.0;
   refl[3]->red_coeff = 0.1;
   refl[3]->green_coeff = 0.9;
   refl[3]->blue_coeff = 0.9;
}

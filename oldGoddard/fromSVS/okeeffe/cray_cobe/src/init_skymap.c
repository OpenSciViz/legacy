#include "ray.h"
#define N_CELLS 6144 /* 6*256*256 */

int init_skymap(chan,infile,skymap)
short chan;
char *infile;
unsigned char skymap[];
{
  int fd=0, nb, i;
  unsigned char buff[N_CELLS];

  printf("init_skymap> reading skymap file: %s\n",infile);
  fd = open(infile,0);
  if( fd <= 0 )
  { 
    printf("init_skymap> unable to open skymap\n");
    exit(0);
  }
/*  nb = read(fd,skymap,NCHAN*N_CELLS); */
  nb = read(fd,buff,N_CELLS);
  close( fd );
 /* and expand it into the dirbe res global buffer */
  for( i= 0; i < 393216; i++ )
    skymap[i] = buff[i/64]; 
  return;
}    

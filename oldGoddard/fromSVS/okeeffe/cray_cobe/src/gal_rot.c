int gal_rot(ang,R)
float ang;
float R[3][3];
{
 int i, j;
/* equatorial to galactic trans.: */
 static float EtoG[3][3] = {-0.0669884,0.4927285,-0.8676007,
                      -0.8727557,-0.4503467,-0.1883749,
                      -0.4835390, 0.7445847, 0.4601998}

 static float Eq[3][3];

/* First roatate in Eq. coord. sys. (ang about Z axis) */
 Eq[0][0] = cos(ang);
 Eq[0][1] = sin(ang);
 Eq[1][0] = -Eq[0][1];
 Eq[1][1] = Eq[0][0];
 Eq[2][2] = 1.0;

 for( i = 0; i < 3; i++ )
  for( j = 0; j < 3; j++ )
    R[i][j] = EtoG[i][0] * Eq[0][j] + 
      		EtoG[i][1]*Eq[1][j] + 
      		EtoG[i][2]*EtoG[2][j];
 return;
}

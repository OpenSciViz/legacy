#include "ray.h"
char shadow(pos)
TRIPLET *pos;
{
   SURFACE *object, *raystrike();
   char boolean;
   float mag, base[3], dir[3];

   object = NULL;

/* make unit vector out of direction to light source from here */
   dir[0] = lamp.x - pos->x;
   dir[1] = lamp.y - pos->y;
   dir[2] = lamp.z - pos->z;
   mag = dir[0] * dir[0] +
		dir[1] * dir[1] +
			dir[2] * dir[2];
   mag = sqrt(mag);
   dir[0] = dir[0] / mag;
   dir[1] = dir[1] / mag;
   dir[2] = dir[2] / mag;
   base[0] = pos->x;
   base[1] = pos->y;
   base[2] = pos->z;
   if( (object = raystrike(base,dir)) != NULL )
   {
	boolean = TRUE;
        free(object);
   }
   else
	boolean = FALSE;

   return( boolean );
}


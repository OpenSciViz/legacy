int usage()
{
printf("ray> Sorry, need some arguments, e.g.:\n");
printf(
"ray NRAYS FofV ViewX ViewY ViewZ outfile Trans_Coeff IRAS_Chan Radsky Radearth Rotsky Rotearth Mirror_shift\n");
printf(
"    (int)(deg.) (cm)  (cm)  (cm) ....... (1/100)     (12,25,.) (1/100) (1/100)   (deg.)  (deg.)   (1/100)\n");
}

int init_logo_map(file,out_buff)
char *file;
unsigned char *out_buff;
{
unsigned char in_scan[512];
int i=0;
int fd, nb, len, sz=512;
char finished=0;

   printf("init_logo_map> reading logo map texture file: %s\n",file);
/*   fd = open(file,0); */
   len = strlen(file);
   for_open(&fd,file,&len);

   while( !finished )
   {
      for_read(&fd,in_scan);
      if( i > 511 )
         finished = 1; 
      else
      {
         bcopy(sz,in_scan,&out_buff[i*sz]);
         i++;
      }
   }
   for_close(&fd);
   return;
}


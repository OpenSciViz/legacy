pro smooth{31}_8video,foo
pana_open
!order=1
ramp=indgen(256)
loadct,115,r,g,b
r(1)=128 & b(1)=128 & g(1)=128
tvlct,r,g,b
openr,1,'smooth31_rot5.8bit'
a=assoc(1,bytarr(512,512))
s=a(0)
close,1
tv,s
pana_rec
openr,1,'smooth31_rot10.8bit'
a=assoc(1,bytarr(512,512))
s=a(0)
close,1
tv,s
pana_rec
openr,1,'smooth31_rot15.8bit'
a=assoc(1,bytarr(512,512))
s=a(0)
close,1
tv,s
pana_rec

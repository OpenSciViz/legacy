
# line 2 "calc.y"
#include <stdio.h>
#include <ctype.h>

typedef struct interval { double lo, hi; } INTERVAL;

INTERVAL vmul(), vdiv();

double atof();

double dreg[26];
INTERVAL vreg[26];

# line 17 "calc.y"
typedef union  { int ival; double dval; INTERVAL vval; } YYSTYPE;
# define DREG 257
# define VREG 258
# define CONST 259
# define UMINUS 260
#define yyclearin yychar = -1
#define yyerrok yyerrflag = 0
extern int yychar;
extern int yyerrflag;
#ifndef YYMAXDEPTH
#define YYMAXDEPTH 150
#endif
YYSTYPE yylval, yyval;
typedef int yytabelem;
# define YYERRCODE 256

# line 101 "calc.y"


#define BSZ 50	/* buffer size for floating point numbers */

/* lexical analysis */

yylex()
{
  register int c;

  while( (c = getchar()) == ' ' ) { /* skip blanks */ }
 
  if( isupper( c ) )
  {
    yylval.ival = c - 'A';
    return( VREG );
  }
  if( islower( c ) )
  {
    yylval.ival = c - 'a';
    return( DREG );
  }

  if( isdigit( c ) || c == '.' )
  { /* gobble up digits, points, exponents */
    char buf[BSZ+1], *cp = buf;
    int dot = 0, exp = 0;

    for( ; (cp-buf) < BSZ; ++cp, c = getchar() )
    {
	*cp = c;
	if( isdigit( c ) ) continue;
	if( c == '.' )
	{
	  if( dot++ || exp ) return('.'); /* will cause syntax error */
	  continue;
	}
	if( c == 'e' )
	{
	  if( exp++ ) return('e'); /* will cause syntax error */
	  continue;
	}
/* end of number */
	break;
    }
    *cp = '0';
    if( (cp-buf) >= BSZ )
      printf("constant too long: truncated\n");
    else
      ungetc( c, stdin ); /* push back last char read */

    yylval.dval = atof( buf );
    return( c );
  }
}

INTERVAL hilo( a, b, c, d ) 
double a, b, c, d;
{ /* returns the smallest interval containing a, b, c, and d */
  INTERVAL v;

  if( a > b )
  {
    v.hi = a; v.lo = b;
  }
  else
  {
    v.hi = b; v.lo = a;
  }

  if( c > d )
  {
    if( c > v.hi ) v.hi = c;
    if( d < v.lo ) v.lo = d;
  }
  else
  {
    if( d > v.hi ) v.hi = d;
    if( c < v.lo ) v.lo = c;
  }
  return( v );
}

INTERVAL vmul( a, b, v ) 
double a, b;
INTERVAL v;
{
  INTERVAL u;
  
  u = hilo( a*v.hi, a*v.lo, b*v.hi, b*v.lo );
  return( u );
}

int dcheck( v )
INTERVAL v;
{
  if( v.hi >= 0. && v.lo <= 0. )
  {
    printf("divisor interval contains 0.\n");
    return( 1 );
  }
  return( 0 );
}

INTERVAL vdiv( a, b, v )
double a, b;
INTERVAL v;
{
  INTERVAL u;

  u = hilo( a/v.hi, a/v.lo, b/v.hi, b/v.lo );
  return( u );
}





yytabelem yyexca[] ={
-1, 1,
	0, -1,
	-2, 0,
	};
# define YYNPROD 29
# define YYLAST 226
yytabelem yyact[]={

    10,    22,    39,    10,     3,     9,    45,    21,     9,    57,
    23,    44,    24,    28,     2,    30,    32,    34,    36,    47,
    14,    12,    48,    13,    43,    15,    63,    53,    51,    50,
    52,    53,    54,    47,    53,    51,    54,    52,     1,    54,
    19,    19,    17,    16,    18,    20,    20,    55,    56,    11,
     0,    58,     0,    14,    59,    60,    61,    62,    15,     0,
     0,    53,    51,     0,    52,     0,    54,    49,    19,    17,
     0,    18,     0,    20,     0,    19,    17,     0,    18,     0,
    20,    14,    12,     4,    13,     0,    15,    14,    12,     0,
    13,     0,    15,    25,    29,     0,    31,    33,    35,    37,
     0,    38,    40,    41,    42,     0,    46,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     7,     5,     6,     8,
    26,    27,     8,    26,     0,     8 };
yytabelem yypact[]={

 -1000,   -40, -1000,    39,    33,   -54,   -60,     0, -1000,   -37,
   -37, -1000,   -37,   -37,   -37,   -37, -1000,   -37,   -37,   -37,
   -37,   -34,   -37, -1000, -1000, -1000, -1000, -1000,   -22,    26,
    11,    -2,    11,    -2, -1000, -1000, -1000, -1000,    -2,    45,
    -2, -1000, -1000,    19,   -34,   -34,    -1, -1000,   -34, -1000,
 -1000,   -34,   -34,   -34,   -34, -1000,    -8, -1000,   -15,   -11,
   -11, -1000, -1000, -1000 };
yytabelem yypgo[]={

     0,    38,     2,    83,    14 };
yytabelem yyr1[]={

     0,     1,     1,     4,     4,     4,     4,     4,     2,     2,
     2,     2,     2,     2,     2,     2,     3,     3,     3,     3,
     3,     3,     3,     3,     3,     3,     3,     3,     3 };
yytabelem yyr2[]={

     0,     0,     4,     5,     5,     9,     9,     5,     2,     3,
     7,     7,     7,     7,     5,     7,     3,    11,     3,     7,
     7,     7,     7,     7,     7,     7,     7,     5,     7 };
yytabelem yychk[]={

 -1000,    -1,    -4,    -2,    -3,   257,   258,   256,   259,    45,
    40,    10,    43,    45,    42,    47,    10,    43,    45,    42,
    47,    61,    61,    10,    -2,    -3,   257,   258,    -2,    -3,
    -2,    -3,    -2,    -3,    -2,    -3,    -2,    -3,    -3,    -2,
    -3,    -3,    -3,    -2,    45,    40,    -3,    41,    44,    41,
    10,    43,    45,    42,    47,    -2,    -2,    10,    -2,    -2,
    -2,    -2,    -2,    41 };
yytabelem yydef[]={

     1,    -2,     2,     0,     0,     9,    18,     0,     8,     0,
     0,     3,     0,     0,     0,     0,     4,     0,     0,     0,
     0,     0,     0,     7,    14,    27,     9,    18,     0,     0,
    10,    20,    11,    22,    12,    24,    13,    26,    19,    16,
    21,    23,    25,     0,     0,     0,     0,    15,     0,    28,
     5,     0,     0,     0,     0,    14,     0,     6,     0,    10,
    11,    12,    13,    17 };
typedef struct { char *t_name; int t_val; } yytoktype;
#ifndef YYDEBUG
#	define YYDEBUG	0	/* don't allow debugging */
#endif

#if YYDEBUG

yytoktype yytoks[] =
{
	"DREG",	257,
	"VREG",	258,
	"CONST",	259,
	"+",	43,
	"-",	45,
	"*",	42,
	"/",	47,
	"UMINUS",	260,
	"-unknown-",	-1	/* ends search */
};

char * yyreds[] =
{
	"-no such reduction-",
	"lines : /* empty */",
	"lines : lines line",
	"line : dexp '\n'",
	"line : vexp '\n'",
	"line : DREG '=' dexp '\n'",
	"line : VREG '=' vexp '\n'",
	"line : error '\n'",
	"dexp : CONST",
	"dexp : DREG",
	"dexp : dexp '+' dexp",
	"dexp : dexp '-' dexp",
	"dexp : dexp '*' dexp",
	"dexp : dexp '/' dexp",
	"dexp : '-' dexp",
	"dexp : '(' dexp ')'",
	"vexp : dexp",
	"vexp : '(' dexp ',' dexp ')'",
	"vexp : VREG",
	"vexp : vexp '+' vexp",
	"vexp : dexp '+' vexp",
	"vexp : vexp '-' vexp",
	"vexp : dexp '-' vexp",
	"vexp : vexp '*' vexp",
	"vexp : dexp '*' vexp",
	"vexp : vexp '/' vexp",
	"vexp : dexp '/' vexp",
	"vexp : '-' vexp",
	"vexp : '(' vexp ')'",
};
#endif /* YYDEBUG */
/* 
 *	Copyright 1987 Silicon Graphics, Inc. - All Rights Reserved
 */

/* #ident	"@(#)yacc:yaccpar	1.10" */
#ident	"$Header: /d2/3.1Esource/root/usr/src/cmd/yacc/RCS/yaccpar,v 1.4 87/11/30 17:32:21 deb Exp $"

/*
** Skeleton parser driver for yacc output
*/

/*
** yacc user known macros and defines
*/
#define YYERROR		goto yyerrlab
#define YYACCEPT	return(0)
#define YYABORT		return(1)
#define YYBACKUP( newtoken, newvalue )\
{\
	if ( yychar >= 0 || ( yyr2[ yytmp ] >> 1 ) != 1 )\
	{\
		yyerror( "syntax error - cannot backup" );\
		goto yyerrlab;\
	}\
	yychar = newtoken;\
	yystate = *yyps;\
	yylval = newvalue;\
	goto yynewstate;\
}
#define YYRECOVERING()	(!!yyerrflag)
#ifndef YYDEBUG
#	define YYDEBUG	1	/* make debugging available */
#endif

/*
** user known globals
*/
int yydebug;			/* set to 1 to get debugging */

/*
** driver internal defines
*/
#define YYFLAG		(-1000)

/*
** global variables used by the parser
*/
YYSTYPE yyv[ YYMAXDEPTH ];	/* value stack */
int yys[ YYMAXDEPTH ];		/* state stack */

YYSTYPE *yypv;			/* top of value stack */
int *yyps;			/* top of state stack */

int yystate;			/* current state */
int yytmp;			/* extra var (lasts between blocks) */

int yynerrs;			/* number of errors */
int yyerrflag;			/* error recovery flag */
int yychar;			/* current input token number */



/*
** yyparse - return 0 if worked, 1 if syntax error not recovered from
*/
int
yyparse()
{
	register YYSTYPE *yypvt;	/* top of value stack for $vars */

	/*
	** Initialize externals - yyparse may be called more than once
	*/
	yypv = &yyv[-1];
	yyps = &yys[-1];
	yystate = 0;
	yytmp = 0;
	yynerrs = 0;
	yyerrflag = 0;
	yychar = -1;

	goto yystack;
	{
		register YYSTYPE *yy_pv;	/* top of value stack */
		register int *yy_ps;		/* top of state stack */
		register int yy_state;		/* current state */
		register int  yy_n;		/* internal state number info */

		/*
		** get globals into registers.
		** branch to here only if YYBACKUP was called.
		*/
	yynewstate:
		yy_pv = yypv;
		yy_ps = yyps;
		yy_state = yystate;
		goto yy_newstate;

		/*
		** get globals into registers.
		** either we just started, or we just finished a reduction
		*/
	yystack:
		yy_pv = yypv;
		yy_ps = yyps;
		yy_state = yystate;

		/*
		** top of for (;;) loop while no reductions done
		*/
	yy_stack:
		/*
		** put a state and value onto the stacks
		*/
#if YYDEBUG
		/*
		** if debugging, look up token value in list of value vs.
		** name pairs.  0 and negative (-1) are special values.
		** Note: linear search is used since time is not a real
		** consideration while debugging.
		*/
		if ( yydebug )
		{
			register int yy_i;

			printf( "State %d, token ", yy_state );
			if ( yychar == 0 )
				printf( "end-of-file\n" );
			else if ( yychar < 0 )
				printf( "-none-\n" );
			else
			{
				for ( yy_i = 0; yytoks[yy_i].t_val >= 0;
					yy_i++ )
				{
					if ( yytoks[yy_i].t_val == yychar )
						break;
				}
				printf( "%s\n", yytoks[yy_i].t_name );
			}
		}
#endif /* YYDEBUG */
		if ( ++yy_ps >= &yys[ YYMAXDEPTH ] )	/* room on stack? */
		{
			yyerror( "yacc stack overflow" );
			YYABORT;
		}
		*yy_ps = yy_state;
		*++yy_pv = yyval;

		/*
		** we have a new state - find out what to do
		*/
	yy_newstate:
		if ( ( yy_n = yypact[ yy_state ] ) <= YYFLAG )
			goto yydefault;		/* simple state */
#if YYDEBUG
		/*
		** if debugging, need to mark whether new token grabbed
		*/
		yytmp = yychar < 0;
#endif
		if ( ( yychar < 0 ) && ( ( yychar = yylex() ) < 0 ) )
			yychar = 0;		/* reached EOF */
#if YYDEBUG
		if ( yydebug && yytmp )
		{
			register int yy_i;

			printf( "Received token " );
			if ( yychar == 0 )
				printf( "end-of-file\n" );
			else if ( yychar < 0 )
				printf( "-none-\n" );
			else
			{
				for ( yy_i = 0; yytoks[yy_i].t_val >= 0;
					yy_i++ )
				{
					if ( yytoks[yy_i].t_val == yychar )
						break;
				}
				printf( "%s\n", yytoks[yy_i].t_name );
			}
		}
#endif /* YYDEBUG */
		if ( ( ( yy_n += yychar ) < 0 ) || ( yy_n >= YYLAST ) )
			goto yydefault;
		if ( yychk[ yy_n = yyact[ yy_n ] ] == yychar )	/*valid shift*/
		{
			yychar = -1;
			yyval = yylval;
			yy_state = yy_n;
			if ( yyerrflag > 0 )
				yyerrflag--;
			goto yy_stack;
		}

	yydefault:
		if ( ( yy_n = yydef[ yy_state ] ) == -2 )
		{
#if YYDEBUG
			yytmp = yychar < 0;
#endif
			if ( ( yychar < 0 ) && ( ( yychar = yylex() ) < 0 ) )
				yychar = 0;		/* reached EOF */
#if YYDEBUG
			if ( yydebug && yytmp )
			{
				register int yy_i;

				printf( "Received token " );
				if ( yychar == 0 )
					printf( "end-of-file\n" );
				else if ( yychar < 0 )
					printf( "-none-\n" );
				else
				{
					for ( yy_i = 0;
						yytoks[yy_i].t_val >= 0;
						yy_i++ )
					{
						if ( yytoks[yy_i].t_val
							== yychar )
						{
							break;
						}
					}
					printf( "%s\n", yytoks[yy_i].t_name );
				}
			}
#endif /* YYDEBUG */
			/*
			** look through exception table
			*/
			{
				register int *yyxi = yyexca;

				while ( ( *yyxi != -1 ) ||
					( yyxi[1] != yy_state ) )
				{
					yyxi += 2;
				}
				while ( ( *(yyxi += 2) >= 0 ) &&
					( *yyxi != yychar ) )
					;
				if ( ( yy_n = yyxi[1] ) < 0 )
					YYACCEPT;
			}
		}

		/*
		** check for syntax error
		*/
		if ( yy_n == 0 )	/* have an error */
		{
			/* no worry about speed here! */
			switch ( yyerrflag )
			{
			case 0:		/* new error */
				yyerror( "syntax error" );
				goto skip_init;
			yyerrlab:
				/*
				** get globals into registers.
				** we have a user generated syntax type error
				*/
				yy_pv = yypv;
				yy_ps = yyps;
				yy_state = yystate;
				yynerrs++;
			skip_init:
			case 1:
			case 2:		/* incompletely recovered error */
					/* try again... */
				yyerrflag = 3;
				/*
				** find state where "error" is a legal
				** shift action
				*/
				while ( yy_ps >= yys )
				{
					yy_n = yypact[ *yy_ps ] + YYERRCODE;
					if ( yy_n >= 0 && yy_n < YYLAST &&
						yychk[yyact[yy_n]] == YYERRCODE)					{
						/*
						** simulate shift of "error"
						*/
						yy_state = yyact[ yy_n ];
						goto yy_stack;
					}
					/*
					** current state has no shift on
					** "error", pop stack
					*/
#if YYDEBUG
#	define _POP_ "Error recovery pops state %d, uncovers state %d\n"
					if ( yydebug )
						printf( _POP_, *yy_ps,
							yy_ps[-1] );
#	undef _POP_
#endif
					yy_ps--;
					yy_pv--;
				}
				/*
				** there is no state on stack with "error" as
				** a valid shift.  give up.
				*/
				YYABORT;
			case 3:		/* no shift yet; eat a token */
#if YYDEBUG
				/*
				** if debugging, look up token in list of
				** pairs.  0 and negative shouldn't occur,
				** but since timing doesn't matter when
				** debugging, it doesn't hurt to leave the
				** tests here.
				*/
				if ( yydebug )
				{
					register int yy_i;

					printf( "Error recovery discards " );
					if ( yychar == 0 )
						printf( "token end-of-file\n" );
					else if ( yychar < 0 )
						printf( "token -none-\n" );
					else
					{
						for ( yy_i = 0;
							yytoks[yy_i].t_val >= 0;
							yy_i++ )
						{
							if ( yytoks[yy_i].t_val
								== yychar )
							{
								break;
							}
						}
						printf( "token %s\n",
							yytoks[yy_i].t_name );
					}
				}
#endif /* YYDEBUG */
				if ( yychar == 0 )	/* reached EOF. quit */
					YYABORT;
				yychar = -1;
				goto yy_newstate;
			}
		}/* end if ( yy_n == 0 ) */
		/*
		** reduction by production yy_n
		** put stack tops, etc. so things right after switch
		*/
#if YYDEBUG
		/*
		** if debugging, print the string that is the user's
		** specification of the reduction which is just about
		** to be done.
		*/
		if ( yydebug )
			printf( "Reduce by (%d) \"%s\"\n",
				yy_n, yyreds[ yy_n ] );
#endif
		yytmp = yy_n;			/* value to switch over */
		yypvt = yy_pv;			/* $vars top of value stack */
		/*
		** Look in goto table for next state
		** Sorry about using yy_state here as temporary
		** register variable, but why not, if it works...
		** If yyr2[ yy_n ] doesn't have the low order bit
		** set, then there is no action to be done for
		** this reduction.  So, no saving & unsaving of
		** registers done.  The only difference between the
		** code just after the if and the body of the if is
		** the goto yy_stack in the body.  This way the test
		** can be made before the choice of what to do is needed.
		*/
		{
			/* length of production doubled with extra bit */
			register int yy_len = yyr2[ yy_n ];

			if ( !( yy_len & 01 ) )
			{
				yy_len >>= 1;
				yyval = ( yy_pv -= yy_len )[1];	/* $$ = $1 */
				yy_state = yypgo[ yy_n = yyr1[ yy_n ] ] +
					*( yy_ps -= yy_len ) + 1;
				if ( yy_state >= YYLAST ||
					yychk[ yy_state =
					yyact[ yy_state ] ] != -yy_n )
				{
					yy_state = yyact[ yypgo[ yy_n ] ];
				}
				goto yy_stack;
			}
			yy_len >>= 1;
			yyval = ( yy_pv -= yy_len )[1];	/* $$ = $1 */
			yy_state = yypgo[ yy_n = yyr1[ yy_n ] ] +
				*( yy_ps -= yy_len ) + 1;
			if ( yy_state >= YYLAST ||
				yychk[ yy_state = yyact[ yy_state ] ] != -yy_n )
			{
				yy_state = yyact[ yypgo[ yy_n ] ];
			}
		}
					/* save until reenter driver code */
		yystate = yy_state;
		yyps = yy_ps;
		yypv = yy_pv;
	}
	/*
	** code supplied by user is placed in this switch
	*/
	switch( yytmp )
	{
		
case 3:
# line 40 "calc.y"
{printf("%f\n", yypvt[-1].dval);} break;
case 4:
# line 42 "calc.y"
{printf("(%f, %f)\n", yypvt[-1].vval.lo, yypvt[-1].vval.hi);} break;
case 5:
# line 44 "calc.y"
{dreg[yypvt[-3].ival] = yypvt[-1].dval;} break;
case 6:
# line 46 "calc.y"
{vreg[yypvt[-3].ival] = yypvt[-1].vval;} break;
case 7:
# line 48 "calc.y"
{yyerrok;} break;
case 9:
# line 53 "calc.y"
{yyval.dval = dreg[yypvt[-0].ival];} break;
case 10:
# line 55 "calc.y"
{yyval.dval = yypvt[-2].dval + yypvt[-0].dval;} break;
case 11:
# line 57 "calc.y"
{yyval.dval = yypvt[-2].dval - yypvt[-0].dval;} break;
case 12:
# line 59 "calc.y"
{yyval.dval = yypvt[-2].dval * yypvt[-0].dval;} break;
case 13:
# line 61 "calc.y"
{yyval.dval = yypvt[-2].dval / yypvt[-0].dval;} break;
case 14:
# line 63 "calc.y"
{yyval.dval = -yypvt[-0].dval;} break;
case 15:
# line 65 "calc.y"
{yyval.dval = yypvt[-1].dval; } break;
case 16:
# line 69 "calc.y"
{yyval.vval.hi = yyval.vval.lo = yypvt[-0].dval; } break;
case 17:
# line 71 "calc.y"
{ yyval.vval.lo = yypvt[-3].dval; yyval.vval.hi = yypvt[-1].dval;
			  if( yyval.vval.lo > yyval.vval.hi )
			  { printf("interval out of order\n"); YYERROR; }
			} break;
case 18:
# line 76 "calc.y"
{yyval.vval = vreg[yypvt[-0].ival];} break;
case 19:
# line 78 "calc.y"
{ yyval.vval.hi = yypvt[-2].vval.hi + yypvt[-0].vval.hi; yyval.vval.lo = yypvt[-2].vval.lo + yypvt[-0].vval.lo; } break;
case 20:
# line 80 "calc.y"
{ yyval.vval.hi = yypvt[-2].dval + yypvt[-0].vval.hi; yyval.vval.lo = yypvt[-2].dval + yypvt[-0].vval.lo; } break;
case 21:
# line 82 "calc.y"
{ yyval.vval.hi = yypvt[-2].vval.hi - yypvt[-0].vval.hi; yyval.vval.lo = yypvt[-2].vval.lo - yypvt[-0].vval.lo; } break;
case 22:
# line 84 "calc.y"
{ yyval.vval.hi = yypvt[-2].dval - yypvt[-0].vval.hi; yyval.vval.lo = yypvt[-2].dval - yypvt[-0].vval.lo; } break;
case 23:
# line 86 "calc.y"
{ yyval.vval = vmul(yypvt[-2].vval.lo, yypvt[-2].vval.hi, yypvt[-0].vval); } break;
case 24:
# line 88 "calc.y"
{ yyval.vval = vmul(yypvt[-2].dval, yypvt[-2].dval, yypvt[-0].vval); } break;
case 25:
# line 90 "calc.y"
{ if( dcheck(yypvt[-0].vval) ) YYERROR;
			  yyval.vval = vdiv(yypvt[-2].vval.lo, yypvt[-2].vval.hi, yypvt[-0].vval); } break;
case 26:
# line 93 "calc.y"
{ if( dcheck(yypvt[-0].vval) ) YYERROR;
			  yyval.vval = vdiv(yypvt[-2].dval, yypvt[-2].dval, yypvt[-0].vval); } break;
case 27:
# line 96 "calc.y"
{ yyval.vval.hi = -yypvt[-0].vval.lo; yyval.vval.lo = -yypvt[-0].vval.hi; } break;
case 28:
# line 98 "calc.y"
{ yyval.vval = yypvt[-1].vval; } break;
	}
	goto yystack;		/* reset registers in driver code */
}

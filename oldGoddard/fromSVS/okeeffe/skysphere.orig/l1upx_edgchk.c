/*
check for ix, iy being over the edge of a face
returns correct face, ix, iy in mface,jx,jy
*/
int edgchk(nface,ix,iy,max,mface,jx,jy)
int *nface, *ix, *iy, *max, *mface, *jx, *jy;
{
   if( *ix < 0 )
   {
      switch( *nface )
      {
         case 0:
	    *mface = 4;
	    *jy = *ix + *max;
	    *jx = *max - 1 - *iy;
	    return;

      	 case 1:
	    *mface = 4;
	    *jx = *max + *ix;
	    *jy = *iy;
	    return;

      	 case 2:
	    *mface = *nface - 1;
	    *jx = *max + *ix;
	    *jy = *iy;
	    return;

      	 case 3:
	    *mface = *nface - 1;
	    *jx = *max + *ix;
	    *jy = *iy;
	    return;

      	 case 4:
	    *mface = *nface - 1;
	    *jx = *max + *ix;
	    *jy = *iy;
	    return;

         case 5:
	    *mface = 4;
	    *jx = *iy;
	    *jy = -(*ix) - 1;
	    return;

         default:
      	    printf("UPX_EDGCHK: aborting due to bad face number\n"); 
      	    exit(0);
      }
   }

   if( *ix >= *max )
   {
      switch( *nface )
      {
         case 0:
	    *mface = 2;
	    *jx = *iy;
	    *jy = 2 * (*max) - 1 - (*ix);
	    return;

         case 1:
	    *mface = *nface + 1;
	    *jy = *iy;
	    *jx = *ix - *max;
	    return;

         case 2:
	    *mface = *nface + 1;
	    *jy = *iy;
	    *jx = *ix - *max;
	    return;

         case 3:
	    *mface = *nface + 1;
	    *jy = *iy;
	    *jx = *ix - *max;
	    return;

         case 4:
	    *mface = 1;
	    *jy = *iy;
	    *jx = *ix - *max;
	    return;

         case 5:
	    *mface = 2;
	    *jx = *max - 1 - (*iy);
	    *jy = *ix - (*max);
	    return;

         default:
      	    printf("UPX_EDGCHK: aborting due to bad face number\n"); 
      	    exit(0);
      }
   }

   if( *iy < 0 )
   {
      switch( *nface )
      {
         case 0:
	    *mface = 1;
	    *jy = *iy + *max;
	    *jx = *ix;
	    return;

         case 1:
	    *mface = 5;
	    *jy = *iy + *max;
	    *jx = *ix;
	    return;

         case 2:
	    *mface = 5;
	    *jx = *iy + *max;
	    *jy = *max - 1 - (*ix);
	    return;

         case 3:
	    *mface = 5;
	    *jx = *max - 1 - (*ix);
	    *jy = -(*iy) - 1;
	    return;

         case 4:
	    *mface = 5;
	    *jx = -(*iy) - 1;
	    *jy = *ix;
	    return;

         case 5:
	    *mface = 3;
	    *jx = *max - 1 - (*ix);
	    *jy = -(*iy) - 1;
	    return;

         default:
      	    printf("UPX_EDGCHK: aborting due to bad face number\n"); 
      	    exit(0);
      }
   }

   if( *iy >= *max )
   {
      switch( *nface )
      {
         case 0:
	    *mface = 3;
	    *jx = *max - 1 - (*ix);
	    *jy = 2 * (*max) - 1 - (*iy);
	    return;

         case 1:
	    *mface = 0;
	    *jy = *iy - (*max);
	    *jx = *ix;
	    return;

         case 2:
	    *mface = 0;
	    *jx = 2 * (*max) - 1 - (*iy);
	    *jy = *ix;
	    return;

         case 3:
	    *mface = 0;
	    *jx = *max - 1 - (*ix);
	    *jy = 2 * (*max) - 1 - (*iy);
	    return;

         case 4:
	    *mface = 0;
	    *jx = *iy - (*max);
	    *jy = *max - 1 - (*ix);
	    return;

         case 5:
	    *mface = 1;
	    *jx = *ix;
	    *jy = *iy - (*max);
	    return;

         default:
      	    printf("UPX_EDGCHK: aborting due to bad face number\n"); 
      	    exit(0);
      }
   }

/* last but not least, if we get here: */

   *mface = *nface;
   *jx = *ix;
   *jy = *iy;
   return;
}

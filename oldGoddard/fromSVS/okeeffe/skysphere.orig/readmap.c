#include "sphere.h"

int readmap(filename,skymap,nobs,wght)
char *filename; float *skymap; int *nobs; float *wght;
{
   int i, nb;
   FILE *fp, *fopen();
   float *skyptr;
   int *nobsptr;
   float *wghtptr;
/*   struct dmr_skyrec { int n_obs; int value; } buff; */
   struct mit_skyrec { float wght; float value; } buff;
   int maxobs;

   maxobs = 0;
   skyptr = skymap;
   nobsptr = nobs;
   wghtptr = wght;
   printf("readmap> opening file: %s\n",filename);
   fp = fopen(filename,"r"); 

   if( fp == NULL )
   {
      printf("readmap> can't open file: %s\n",filename);
      exit(0);
   }
   for( i = 0; i < NP; i++ )
   {
      if( nobs != NULL)
      {
	nb = fscanf(fp," %d %f",nobsptr,skyptr); 
        if( *nobsptr > maxobs ) maxobs = *nobsptr;
      }
/* mit map has weights, not nobs: */
      if( wght != NULL)
      {
	nb = fscanf(fp," %f %f",wghtptr,skyptr); 
      }
      if( nb < 2 )
      {
         printf("readmap> unexpected end-of-file: %s\n",filename);
   	 return(maxobs);
      }
#ifdef DEBUG
	if( nobs != NULL && i % 128 == 0 ) printf("readmap> n_obs= %d, value= %f\n",
				   *nobsptr,*skyptr);
	if( wght != NULL && i % 128 == 0 ) printf("readmap> n_obs= %f, value= %f\n",
				   *wghtptr,*skyptr);
#endif
      skyptr++;
      nobsptr++;
      wghtptr++;
   }
   fclose( fp );
   return( maxobs );
}


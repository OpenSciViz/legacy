#define MAIN

#include "sphere.h"

main(argc, argv)
int argc; char **argv;
{
int i, x0, y0;
char *skyfile;
float *skymap, *wght;
int *nobs;
int maxobs;
int readmap(), init_triang_info(), init_color_info();
static float ident[4][4] = { {1.0, 0.0, 0.0, 0.0}, 
			     {0.0, 1.0, 0.0, 0.0}, 
			     {0.0, 0.0, 1.0, 0.0}, 
			     {0.0, 0.0, 0.0, 1.0} };
float dist = 5.0, delt = 0.1;

short wrred = 0xffff, wrgrn = 0xffff, wrblu = 0xffff;

nobs = NULL;
skymap = NULL;
wght = NULL;
maxobs = 0;

if( argc > 1 ) /* should be "-w" or "-n" to indicate %f,%f or %d,%f pairs */
{
   switch( argv[1][1] )
   {
	case 'w':
		   wght = (float *) malloc(NP*sizeof(float));
		   for(i=0; i<NP; i++) wght[0] = 0.0;
		   break;

	case 'n':
		   nobs = (int *) malloc(NP*sizeof(int));
		   for(i=0; i<NP; i++) nobs[0] = 0.0;
		   break;

	default: printf("Only '-w' or '-n' are acceptable, followed by input file.\n");
		 exit(0);
   }
}


if( argc > 2 ) /* get the file name */
{
   skyfile = argv[2];
   skymap = (float *) malloc(NP*sizeof(float));
   for(i=0; i<NP; i++) skymap[0] = 0.0;
   maxobs = readmap(skyfile,skymap,nobs,wght);
}

if( argc > 2 )
{
    x0 = atoi(argv[1]);
    y0 = atoi(argv[2]);
}
else
{
    x0 = 8;
    y0 = 8;
}

/* init unit sphere using quad-cube routines */
init_triang_info(maxobs,nobs,wght);

/* init colors */
init_color_info(skymap,nobs,wght);

prefposition(x0,x0+988,y0,y0+988);
keepaspect(1,1);
winopen(argv[0]);
RGBmode();
RGBwritemask(wrred,wrgrn,wrblu);
drawmode(NORMALDRAW);
doublebuffer();
gconfig();

mmode(MVIEWING);
perspective(450,1.0,0.01,100.0);
loadmatrix(ident);
polarview(dist,10,800,0);
lsetdepth(0,0x7fffff);
zbuffer(TRUE);

while( TRUE )
{
      zclear();
      cpack(0);
      clear();

      draw_bottom();

      draw_sphere();

      swapbuffers(); 
   }
}


/*
check for ix, iy being over the edge of a face
returns correct face, ix, iy in mface,jx,jy
*/
int edgchk(nface,ix,iy,max,mface,jx,jy)
int *nface, *ix, *iy, *max, *mface, *jx, *jy;
{
 int tempx, tempy, tempface;

   tempx = *ix;
   tempy = *iy;
   tempface = *nface % 6;

force_it:	/* simplest translation of fortran version by Ed Boggess */
	    
   if( tempx < 0 )
   {
      switch( tempface )
      {
         case 0:
	    *mface = 4;
	    *jy = tempx + *max;
	    *jx = *max - 1 - tempy;
      	    tempx = *jx;
      	    tempy = *jy;
	    goto force_it;

      	 case 1:
	    *mface = 4;
	    *jx = *max + tempx;
	    *jy = tempy;
      	    tempx = *jx;
      	    tempy = *jy;
	    goto force_it;

      	 case 2:
	    *mface = tempface - 1;
	    *jx = *max + tempx;
	    *jy = tempy;
      	    tempx = *jx;
      	    tempy = *jy;
	    goto force_it;

      	 case 3:
	    *mface = tempface - 1;
	    *jx = *max + tempx;
	    *jy = tempy;
      	    tempx = *jx;
      	    tempy = *jy;
	    goto force_it;

      	 case 4:
	    *mface = *nface - 1;
	    *jx = *max + tempx;
	    *jy = tempy;
      	    tempx = *jx;
      	    tempy = *jy;
	    goto force_it;

         case 5:
	    *mface = 4;
	    *jx = tempy;
	    *jy = -(tempx) - 1;
      	    tempx = *jx;
      	    tempy = *jy;
	    goto force_it;

         default:
	    printf("UPX_EDGCHK: aborting due to bad face number\n"); 
      	    exit(0);
      }
   }

   if( tempx >= *max )
   {
      switch( tempface )
      {
         case 0:
	    *mface = 2;
	    *jx = tempy;
	    *jy = 2 * (*max) - 1 - (tempx);
      	    tempx = *jx;
      	    tempy = *jy;
	    goto force_it;

         case 1:
	    *mface = tempface + 1;
	    *jy = tempy;
	    *jx = tempx - *max;
      	    tempx = *jx;
      	    tempy = *jy;
	    goto force_it;

         case 2:
	    *mface = tempface + 1;
	    *jy = tempy;
	    *jx = tempx - *max;
      	    tempx = *jx;
      	    tempy = *jy;
	    goto force_it;

         case 3:
	    *mface = tempface + 1;
	    *jy = tempy;
	    *jx = tempx - *max;
      	    tempx = *jx;
      	    tempy = *jy;
	    goto force_it;

         case 4:
	    *mface = 1;
	    *jy = tempy;
	    *jx = tempx - *max;
      	    tempx = *jx;
      	    tempy = *jy;
	    goto force_it;

         case 5:
	    *mface = 2;
	    *jx = *max - 1 - (tempy);
	    *jy = tempx - (*max);
      	    tempx = *jx;
      	    tempy = *jy;
	    goto force_it;

         default:
      	    printf("UPX_EDGCHK: aborting due to bad face number\n"); 
      	    exit(0);
      }
   }

   if( tempy < 0 )
   {
      switch( tempface )
      {
         case 0:
	    *mface = 1;
	    *jy = tempy + *max;
	    *jx = tempx;
      	    tempx = *jx;
      	    tempy = *jy;
	    goto force_it;

         case 1:
	    *mface = 5;
	    *jy = tempy + *max;
	    *jx = tempx;
      	    tempx = *jx;
      	    tempy = *jy;
	    goto force_it;

         case 2:
	    *mface = 5;
	    *jx = tempy + *max;
	    *jy = *max - 1 - (tempx);
      	    tempx = *jx;
      	    tempy = *jy;
	    goto force_it;

         case 3:
	    *mface = 5;
	    *jx = *max - 1 - (tempx);
	    *jy = -(tempy) - 1;
      	    tempx = *jx;
      	    tempy = *jy;
	    goto force_it;

         case 4:
	    *mface = 5;
	    *jx = -(tempy) - 1;
	    *jy = tempx;
      	    tempx = *jx;
      	    tempy = *jy;
	    goto force_it;

         case 5:
	    *mface = 3;
	    *jx = *max - 1 - (tempx);
	    *jy = -(tempy) - 1;
      	    tempx = *jx;
      	    tempy = *jy;
	    goto force_it;

         default:
      	    printf("UPX_EDGCHK: aborting due to bad face number\n"); 
      	    exit(0);
      }
   }

   if( tempy >= *max )
   {
      switch( tempface )
      {
         case 0:
	    *mface = 3;
	    *jx = *max - 1 - (tempx);
	    *jy = 2 * (*max) - 1 - (tempy);
      	    tempx = *jx;
      	    tempy = *jy;
	    goto force_it;

         case 1:
	    *mface = 0;
	    *jy = tempy - (*max);
	    *jx = tempx;
      	    tempx = *jx;
      	    tempy = *jy;
	    goto force_it;

         case 2:
	    *mface = 0;
	    *jx = 2 * (*max) - 1 - (tempy);
	    *jy = tempx;
      	    tempx = *jx;
      	    tempy = *jy;
	    goto force_it;

         case 3:
	    *mface = 0;
	    *jx = *max - 1 - (tempx);
	    *jy = 2 * (*max) - 1 - (tempy);
      	    tempx = *jx;
      	    tempy = *jy;
	    goto force_it;

         case 4:
	    *mface = 0;
	    *jx = tempy - (*max);
	    *jy = *max - 1 - (tempx);
      	    tempx = *jx;
      	    tempy = *jy;
	    goto force_it;

         case 5:
	    *mface = 1;
	    *jx = tempx;
	    *jy = tempy - (*max);
      	    tempx = *jx;
      	    tempy = *jy;
	    goto force_it;

         default:
      	    printf("UPX_EDGCHK: aborting due to bad face number\n"); 
      	    exit(0);
      }
   }

/* last but not least, if we get here: */

   *mface = tempface;
   *jx = tempx;
   *jy = tempy;
   return;
}

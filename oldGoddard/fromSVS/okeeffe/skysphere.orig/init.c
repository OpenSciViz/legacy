#include "sphere.h"

int init_triang_info(maxobs,nobs,wght)
int maxobs, nobs[]; float wght[];
{
   int i, j, nval, neighb[8];
   float cenp[3], cornp[3], strch;
   int upx_8_neighbors(), upx_pixel_vector();
   char doit;
   int pixtyp;

   doit = FALSE;
   strch = 1.0;
   ntri = 0;
   res = RES;
 
   for( i = 0; i < NP; i++ )
   {
      pixtyp = upx_8_neighbors(&i,&res,neighb,&nval);
      if( (i % 4) == 3 ) doit = TRUE;
      if( (pixtyp <= 0) && (i >= 5.0*pow(4.0,res - 1.0)) ) doit = TRUE;
      if( doit )
      {
         upx_pixel_vector(&i,&res,cenp); /* Center pixel */
	 if( pixtyp == 0 )
/*	    printf("edge pixel: %d\n",i); */
	 if( pixtyp == -1 )
	    printf("corner pixel: %d\n",i);
/*     	 if( nval < 8 ) printf("upx_8_neighbors returns: %d\n",nval); */
/*	assume neighbors are ordered: BL, B, BR, R, TR, T, TL */

         /* form triangle with BL, B, C */
      	    upx_pixel_vector(&neighb[0],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[0]] / maxobs;
      	    triang[ntri].id[0] = neighb[0];
      	    triang[ntri].v[0][0] = strch * cornp[0];
      	    triang[ntri].v[0][1] = strch * cornp[1];
      	    triang[ntri].v[0][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[1],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[1]] / maxobs;
      	    triang[ntri].id[1] = neighb[1];
      	    triang[ntri].v[1][0] = strch * cornp[0];
      	    triang[ntri].v[1][1] = strch * cornp[1];
      	    triang[ntri].v[1][2] = strch * cornp[2];

      	    triang[ntri].id[2] = i;
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[i] / maxobs;
            triang[ntri].v[2][0] = strch * cenp[0];
            triang[ntri].v[2][1] = strch * cenp[1];
            triang[ntri].v[2][2] = strch * cenp[2];

	    ntri++;
         /* form triangle with C, L, BL */
      	    triang[ntri].id[0] = i;
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[i] / maxobs;
            triang[ntri].v[0][0] = strch * cenp[0];
            triang[ntri].v[0][1] = strch * cenp[1];
            triang[ntri].v[0][2] = strch * cenp[2];

      	    upx_pixel_vector(&neighb[7],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[7]] / maxobs;
      	    triang[ntri].id[1] = neighb[7];
      	    triang[ntri].v[1][0] = strch * cornp[0];
      	    triang[ntri].v[1][1] = strch * cornp[1];
      	    triang[ntri].v[1][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[0],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[7]] / maxobs;
      	    triang[ntri].id[2] = neighb[0];
      	    triang[ntri].v[2][0] = strch * cornp[0];
      	    triang[ntri].v[2][1] = strch * cornp[1];
      	    triang[ntri].v[2][2] = strch * cornp[2];

	    ntri++;
         /* form triangle with B, BR, R */
      	    upx_pixel_vector(&neighb[1],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[1]] / maxobs;
      	    triang[ntri].id[0] = neighb[1];
      	    triang[ntri].v[0][0] = strch * cornp[0];
      	    triang[ntri].v[0][1] = strch * cornp[1];
      	    triang[ntri].v[0][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[2],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[2]] / maxobs;
      	    triang[ntri].id[1] = neighb[2];
      	    triang[ntri].v[1][0] = strch * cornp[0];
      	    triang[ntri].v[1][1] = strch * cornp[1];
      	    triang[ntri].v[1][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[3],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[3]] / maxobs;
      	    triang[ntri].id[2] = neighb[3];
      	    triang[ntri].v[2][0] = strch * cornp[0];
      	    triang[ntri].v[2][1] = strch * cornp[1];
      	    triang[ntri].v[2][2] = strch * cornp[2];

	    ntri++;
         /* form triangle with R, C, B */
      	    triang[ntri].id[0] = neighb[3];
      	    triang[ntri].v[0][0] = strch * cornp[0];
      	    triang[ntri].v[0][1] = strch * cornp[1];
      	    triang[ntri].v[0][2] = strch * cornp[2];

      	    triang[ntri].id[1] = i;
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[i] / maxobs;
            triang[ntri].v[1][0] = strch * cenp[0];
            triang[ntri].v[1][1] = strch * cenp[1];
            triang[ntri].v[1][2] = strch * cenp[2];

      	    upx_pixel_vector(&neighb[1],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[1]] / maxobs;
      	    triang[ntri].id[2] = neighb[1];
      	    triang[ntri].v[2][0] = strch * cornp[0];
      	    triang[ntri].v[2][1] = strch * cornp[1];
      	    triang[ntri].v[2][2] = strch * cornp[2];

	    ntri++;

      	 if( nval < 8 ) /* corners are short a vertex (TR), form only 3 more 
			triangles */
	 {
         /* form triangle with C, R, T */
      	    triang[ntri].id[0] = i;
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[i] / maxobs;
            triang[ntri].v[0][0] = strch * cenp[0];
            triang[ntri].v[0][1] = strch * cenp[1];
            triang[ntri].v[0][2] = strch * cenp[2];

      	    upx_pixel_vector(&neighb[3],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[3]] / maxobs;
      	    triang[ntri].id[1] = neighb[3];
      	    triang[ntri].v[1][0] = strch * cornp[0];
      	    triang[ntri].v[1][1] = strch * cornp[1];
      	    triang[ntri].v[1][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[4],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[4]] / maxobs;
      	    triang[ntri].id[2] = neighb[4];
      	    triang[ntri].v[2][0] = strch * cornp[0];
      	    triang[ntri].v[2][1] = strch * cornp[1];
      	    triang[ntri].v[2][2] = strch * cornp[2];

	    ntri++;
         /* form triangle with L, C, T */
      	    upx_pixel_vector(&neighb[6],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[6]] / maxobs;
      	    triang[ntri].id[0] = neighb[6];
      	    triang[ntri].v[0][0] = strch * cornp[0];
      	    triang[ntri].v[0][1] = strch * cornp[1];
      	    triang[ntri].v[0][2] = strch * cornp[2];

      	    triang[ntri].id[1] = i;
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[i] / maxobs;
            triang[ntri].v[1][0] = strch * cenp[0];
            triang[ntri].v[1][1] = strch * cenp[1];
            triang[ntri].v[1][2] = strch * cenp[2];

      	    upx_pixel_vector(&neighb[4],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[4]] / maxobs;
      	    triang[ntri].id[2] = neighb[4];
      	    triang[ntri].v[2][0] = strch * cornp[0];
      	    triang[ntri].v[2][1] = strch * cornp[1];
      	    triang[ntri].v[2][2] = strch * cornp[2];

	    ntri++;
         /* form triangle with T, TL, L */
      	    triang[ntri].id[0] = neighb[4];
      	    triang[ntri].v[0][0] = strch * cornp[0];
      	    triang[ntri].v[0][1] = strch * cornp[1];
      	    triang[ntri].v[0][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[5],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[5]] / maxobs;
      	    triang[ntri].id[1] = neighb[5];
      	    triang[ntri].v[1][0] = strch * cornp[0];
      	    triang[ntri].v[1][1] = strch * cornp[1];
      	    triang[ntri].v[1][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[6],&res,cornp);
      	    triang[ntri].id[2] = neighb[6];
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[6]] / maxobs;
            triang[ntri].v[2][0] = strch * cenp[0];
            triang[ntri].v[2][1] = strch * cenp[1];
            triang[ntri].v[2][2] = strch * cenp[2];

      	    ntri++;
         }
         else /*	assume neighbors are ordered: BL, B, BR, R, TR, T, TL */
         {
         /* form triangle with C, R, TR */
      	    triang[ntri].id[0] = i;
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[i] / maxobs;
            triang[ntri].v[0][0] = strch * cenp[0];
            triang[ntri].v[0][1] = strch * cenp[1];
            triang[ntri].v[0][2] = strch * cenp[2];

      	    upx_pixel_vector(&neighb[3],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[3]] / maxobs;
      	    triang[ntri].id[1] = neighb[3];
      	    triang[ntri].v[1][0] = strch * cornp[0];
      	    triang[ntri].v[1][1] = strch * cornp[1];
      	    triang[ntri].v[1][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[4],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[4]] / maxobs;
      	    triang[ntri].id[2] = neighb[4];
      	    triang[ntri].v[2][0] = strch * cornp[0];
      	    triang[ntri].v[2][1] = strch * cornp[1];
      	    triang[ntri].v[2][2] = strch * cornp[2];

	    ntri++;
         /* form triangle with TR, T, C */
      	    triang[ntri].id[0] = neighb[4];
      	    triang[ntri].v[0][0] = strch * cornp[0];
      	    triang[ntri].v[0][1] = strch * cornp[1];
      	    triang[ntri].v[0][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[5],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[5]] / maxobs;
      	    triang[ntri].id[1] = neighb[5];
      	    triang[ntri].v[1][0] = strch * cornp[0];
      	    triang[ntri].v[1][1] = strch * cornp[1];
      	    triang[ntri].v[1][2] = strch * cornp[2];

      	    triang[ntri].id[2] = i;
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[i] / maxobs;
            triang[ntri].v[2][0] = strch * cenp[0];
            triang[ntri].v[2][1] = strch * cenp[1];
            triang[ntri].v[2][2] = strch * cenp[2];

	    ntri++;
         /* form triangle with L, C, T */
      	    upx_pixel_vector(&neighb[7],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[7]] / maxobs;
      	    triang[ntri].id[0] = neighb[7];
      	    triang[ntri].v[0][0] = strch * cornp[0];
      	    triang[ntri].v[0][1] = strch * cornp[1];
      	    triang[ntri].v[0][2] = strch * cornp[2];

      	    triang[ntri].id[1] = i;
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[i] / maxobs;
            triang[ntri].v[1][0] = strch * cenp[0];
            triang[ntri].v[1][1] = strch * cenp[1];
            triang[ntri].v[1][2] = strch * cenp[2];

      	    upx_pixel_vector(&neighb[5],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[5]] / maxobs;
      	    triang[ntri].id[2] = neighb[5];
      	    triang[ntri].v[2][0] = strch * cornp[0];
      	    triang[ntri].v[2][1] = strch * cornp[1];
      	    triang[ntri].v[2][2] = strch * cornp[2];

	    ntri++;
         /* form triangle with T, TL, L */
      	    triang[ntri].id[0] = neighb[5];
      	    triang[ntri].v[0][0] = strch * cornp[0];
      	    triang[ntri].v[0][1] = strch * cornp[1];
      	    triang[ntri].v[0][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[6],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[6]] / maxobs;
      	    triang[ntri].id[1] = neighb[6];
      	    triang[ntri].v[1][0] = strch * cornp[0];
      	    triang[ntri].v[1][1] = strch * cornp[1];
      	    triang[ntri].v[1][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[7],&res,cornp);
      	    triang[ntri].id[2] = neighb[7];
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[i] / maxobs;
            triang[ntri].v[2][0] = strch * cornp[0];
            triang[ntri].v[2][1] = strch * cornp[1];
            triang[ntri].v[2][2] = strch * cornp[2];

      	    ntri++;
         }
      }
      doit = FALSE;
   }
}

int init_color_info(skymap,nobs,wght)
float *skymap, *wght;
int *nobs;
{
int i, *nobsptr, nskyval=1;
float skymin, skymax, skymean, skyrms, spread, minspread, maxspread;
float *wghtptr, *skyptr, rabs();

 if( skymap != NULL )	/* must set pntcolor according to sky values */
 {
    skyptr = skymap;
    nobsptr = nobs;
    wghtptr = wght;
 /* first find min,max,etc */
    skymin = *skyptr; 
    skymax = skymin ;
    skymean = 0.0;
   
/* refedine dynamic range, window: 0.001 to 1000.0 */ 
    while( nskyval < NP )
    {
/*
       if( *skyptr < 0.001 ) *skyptr = 0.001;
       if( *skyptr > 1000.0 ) *skyptr = 1000.0;
*/
       skymean = skymean + *skyptr;
       if( *skyptr > skymax ) skymax = *skyptr;
       if( *skyptr < skymin ) skymin = *skyptr;
       nskyval++;
       skyptr++;
    }
    skymean = skymean / nskyval;
    spread = skymax - skymin;
    minspread = skymean - skymin;
    maxspread = skymax - skymean;
    skyrms = 0.0;

/* define colors: linear Red ramp = 255 at skymin, 0 at skymax; linear
   green ramp = 0 at skymin, 255 at skymean, 0 at skymax; linear blue ramp =
   0 at skymin, 255 at skymax. */

   skyptr = skymap;
   for( i = 0; i < NP; i++ )
   { 
      pntcolor[i][0] = 1.0 - ( ((*skyptr) - skymin) / spread );
      pntcolor[i][2] = ((*skyptr) - skymin) / spread;
      if( pntcolor[i][0] > pntcolor[i][2] )
         pntcolor[i][1] = 2.0 * pntcolor[i][2];
      else
         pntcolor[i][1] = 2.0 * pntcolor[i][0];
/*
         if( *skyptr < skymean )
            pntcolor[i][1] = (skymean - (*skyptr)) / minspread;
         else
            pntcolor[i][1] = 1.0 - ( ((*skyptr) - skymean) / maxspread);
*/
      if( nobs != NULL )
	if( *nobsptr++ <= 0 )
        {
           pntcolor[i][0] = 0.1;
           pntcolor[i][1] = 0.1;
           pntcolor[i][2] = 0.1;
        }
      else if( wght != NULL )
	if( *wghtptr++ == 0.0 )
        {
           pntcolor[i][0] = 0.1;
           pntcolor[i][1] = 0.1;
           pntcolor[i][2] = 0.1;
        }
      skyptr++;
   }
   

 }
 else /* no skymap, just image the pixel numbers */
 {
   for( i = 0; i < NP; i++ )
   {
      if( i < NP/6 )
      {
            pntcolor[i][0] = (255 - (i % 256))/255.0;
            pntcolor[i][1] = 0.0;
       	    pntcolor[i][2] = 0.0;
      }
      else if( i < 2*NP/6 )
      {
            pntcolor[i][0] = 0.0;
            pntcolor[i][1] = (255 - (i % 256))/255.0;
       	    pntcolor[i][2] = 0.0;
      }
      else if( i < 3*NP/6 )
      {
            pntcolor[i][0] = 0.0;
            pntcolor[i][1] = 0.0;
       	    pntcolor[i][2] = (255 - (i % 256))/255.0;
      }
      else if( i < 4*NP/6 )
      {
            pntcolor[i][0] = (255 - (i % 256))/255.0;
            pntcolor[i][1] = (255 - (i % 256))/255.0;
       	    pntcolor[i][2] = 0.0;
      }
      else if( i < 5*NP/6 )
      {
            pntcolor[i][0] = (255 - (i % 256))/255.0;
            pntcolor[i][1] = 0.0;
       	    pntcolor[i][2] = (255 - (i % 256))/255.0;
      }
      else if( i < NP )
      {
            pntcolor[i][0] = 0.0;
            pntcolor[i][1] = (255 - (i % 256))/255.0;
       	    pntcolor[i][2] = (255 - (i % 256))/255.0;
      }
   }
  }
}


#include "sphere.h"

float bpnt[4][3] = { { -1.5, -1.5, -1.2  }, { -1.5, 1.5, -1.2 },
      		     { 1.5, 1.5, -1.2 }, { 1.5, -1.5, -1.2 } };
float white[3] = { 1.0, 1.0, 1.0 };
float gray[3] = { 0.5, 0.5, 0.5 };
float red[3] = { 1.0, 0.0, 0.0 };
float green[3] = { 0.0, 1.0, 0.0 };
float blue[3] = { 0.0, 0.0, 1.0 };
float yellow[3] = { 1.0, 1.0, 0.0 };
float magenta[3] = { 1.0, 0.0, 1.0 };
float cyan[3] = { 0.0, 1.0, 1.0 };
float ident[4][4] = { {1.0, 0.0, 0.0, 0.0}, 
			     {0.0, 1.0, 0.0, 0.0}, 
			     {0.0, 0.0, 1.0, 0.0}, 
			     {0.0, 0.0, 0.0, 1.0} };
static long arot = 50, trot = 0; 
char rotaxis = 'z';
float dist = 5.0, delt = 0.1;

int draw_bottom()
{
   pushmatrix();
   loadmatrix(ident);
   polarview(dist,10,800,0);
   
   bgnpolygon();
   c3f(gray); v3f(bpnt[0]); 
   c3f(gray); v3f(bpnt[1]); 
   c3f(gray); v3f(bpnt[2]); 
   c3f(gray); v3f(bpnt[3]);
   endpolygon();

   popmatrix();
}

int draw_triang()
{
   int itriang, inc;
   itriang = ntri;
   while( itriang >= 0 )
   {
      bgnpolygon();
         c3f(pntcolor[triang[itriang].id[0]]); v3f(triang[itriang].v[0]);
         c3f(pntcolor[triang[itriang].id[1]]); v3f(triang[itriang].v[1]);
         c3f(pntcolor[triang[itriang].id[2]]); v3f(triang[itriang].v[2]);
         itriang--;
      endpolygon();
   }
}

int draw_sphere()
{
   short iface;
   static n_face=5;
   static nrot=0;

/*
   pushmatrix();
*/
   trot += 50;
   if( trot > 1800 )
   {
      if( rotaxis == 'x' ) 
	  rotaxis = 'z';
      else if( rotaxis == 'z' )
	  rotaxis = 'x';
      trot = 0;
   }
   rotate(arot,rotaxis);
/*   scale(2.0,2.0,2.0); */
   draw_triang();
/*
   popmatrix();
*/
}


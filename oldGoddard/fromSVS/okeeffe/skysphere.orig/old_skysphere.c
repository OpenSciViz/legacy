#include <stdio.h>
#include <gl.h>
#include <device.h>
#include <math.h>
#define FALSE 0

#define DEBUG

/*
#define NP 1536 
#define NP 24567
#define NP 6*16
short res = 3;
*/
#define NP 6144
short res = 6;

typedef struct vrtx_lst { int id[3]; float v[3][3]; } VRTX_LST;

VRTX_LST triang[3*NP];

int ntri=0;
char finished;
/*
short pntcolor[NP][3];
*/
float pntcolor[NP][3];

float bpnt[4][3] = { { -1.5, -1.5, -1.2  }, { -1.5, 1.5, -1.2 },
      		     { 1.5, 1.5, -1.2 }, { 1.5, -1.5, -1.2 } };
float white[3] = { 1.0, 1.0, 1.0 };
float gray[3] = { 0.5, 0.5, 0.5 };
float red[3] = { 1.0, 0.0, 0.0 };
float green[3] = { 0.0, 1.0, 0.0 };
float blue[3] = { 0.0, 0.0, 1.0 };
float yellow[3] = { 1.0, 1.0, 0.0 };
float magenta[3] = { 1.0, 0.0, 1.0 };
float cyan[3] = { 0.0, 1.0, 1.0 };
static float ident[4][4] = { {1.0, 0.0, 0.0, 0.0}, 
			     {0.0, 1.0, 0.0, 0.0}, 
			     {0.0, 0.0, 1.0, 0.0}, 
			     {0.0, 0.0, 0.0, 1.0} };
short wrred = 0xffff, wrgrn = 0xffff, wrblu = 0xffff;
long arot = 50, trot = 0; 
char rotaxis = 'z';
float dist = 5.0, delt = 0.1;

int draw_bottom()
{
   pushmatrix();
   loadmatrix(ident);
   polarview(dist,10,800,0);
   
   bgnpolygon();
   c3f(gray); v3f(bpnt[0]); 
   c3f(gray); v3f(bpnt[1]); 
   c3f(gray); v3f(bpnt[2]); 
   c3f(gray); v3f(bpnt[3]);
   endpolygon();

   popmatrix();
}

int draw_triang()
{
   int itriang, inc;
   itriang = ntri;
   while( itriang >= 0 )
   {
      bgnpolygon();
         c3f(pntcolor[triang[itriang].id[0]]); v3f(triang[itriang].v[0]);
         c3f(pntcolor[triang[itriang].id[1]]); v3f(triang[itriang].v[1]);
         c3f(pntcolor[triang[itriang].id[2]]); v3f(triang[itriang].v[2]);
         itriang--;
      endpolygon();
   }
}

int draw_sphere()
{
   short iface;
   static n_face=5;
   static nrot=0;

/*
   pushmatrix();
*/
   trot += 50;
   if( trot > 1800 )
   {
      if( rotaxis == 'x' ) 
	  rotaxis = 'z';
      else if( rotaxis == 'z' )
	  rotaxis = 'x';
      trot = 0;
   }
   rotate(arot,rotaxis);
/*   scale(2.0,2.0,2.0); */
   draw_triang();
/*
   popmatrix();
*/
}

int init_triang_info(maxobs,nobs,wght)
int maxobs, nobs[]; float wght[];
{
   int i, j, nval, neighb[8];
   float cenp[3], cornp[3], strch;
   int upx_8_neighbors(), upx_pixel_vector();
   char doit;
   int pixtyp;

   doit = FALSE;
   strch = 1.0;
 
   for( i = 0; i < NP; i++ )
   {
      pixtyp = upx_8_neighbors(&i,&res,neighb,&nval);
      if( (i % 4) == 3 ) doit = TRUE;
      if( (pixtyp <= 0) && (i >= 5.0*pow(4.0,res - 1.0)) ) doit = TRUE;
      if( doit )
      {
         upx_pixel_vector(&i,&res,cenp); /* Center pixel */
	 if( pixtyp == 0 )
	    printf("edge pixel: %d\n",i);
	 if( pixtyp == -1 )
	    printf("corner pixel: %d\n",i);
     	 if( nval < 8 ) printf("upx_8_neighbors returns: %d\n",nval); 
/*	assume neighbors are ordered: BL, B, BR, R, TR, T, TL */

         /* form triangle with BL, B, C */
      	    upx_pixel_vector(&neighb[0],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[0]] / maxobs;
      	    triang[ntri].id[0] = neighb[0];
      	    triang[ntri].v[0][0] = strch * cornp[0];
      	    triang[ntri].v[0][1] = strch * cornp[1];
      	    triang[ntri].v[0][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[1],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[1]] / maxobs;
      	    triang[ntri].id[1] = neighb[1];
      	    triang[ntri].v[1][0] = strch * cornp[0];
      	    triang[ntri].v[1][1] = strch * cornp[1];
      	    triang[ntri].v[1][2] = strch * cornp[2];

      	    triang[ntri].id[2] = i;
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[i] / maxobs;
            triang[ntri].v[2][0] = strch * cenp[0];
            triang[ntri].v[2][1] = strch * cenp[1];
            triang[ntri].v[2][2] = strch * cenp[2];

	    ntri++;
         /* form triangle with C, L, BL */
      	    triang[ntri].id[0] = i;
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[i] / maxobs;
            triang[ntri].v[0][0] = strch * cenp[0];
            triang[ntri].v[0][1] = strch * cenp[1];
            triang[ntri].v[0][2] = strch * cenp[2];

      	    upx_pixel_vector(&neighb[7],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[7]] / maxobs;
      	    triang[ntri].id[1] = neighb[7];
      	    triang[ntri].v[1][0] = strch * cornp[0];
      	    triang[ntri].v[1][1] = strch * cornp[1];
      	    triang[ntri].v[1][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[0],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[7]] / maxobs;
      	    triang[ntri].id[2] = neighb[0];
      	    triang[ntri].v[2][0] = strch * cornp[0];
      	    triang[ntri].v[2][1] = strch * cornp[1];
      	    triang[ntri].v[2][2] = strch * cornp[2];

	    ntri++;
         /* form triangle with B, BR, R */
      	    upx_pixel_vector(&neighb[1],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[1]] / maxobs;
      	    triang[ntri].id[0] = neighb[1];
      	    triang[ntri].v[0][0] = strch * cornp[0];
      	    triang[ntri].v[0][1] = strch * cornp[1];
      	    triang[ntri].v[0][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[2],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[2]] / maxobs;
      	    triang[ntri].id[1] = neighb[2];
      	    triang[ntri].v[1][0] = strch * cornp[0];
      	    triang[ntri].v[1][1] = strch * cornp[1];
      	    triang[ntri].v[1][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[3],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[3]] / maxobs;
      	    triang[ntri].id[2] = neighb[3];
      	    triang[ntri].v[2][0] = strch * cornp[0];
      	    triang[ntri].v[2][1] = strch * cornp[1];
      	    triang[ntri].v[2][2] = strch * cornp[2];

	    ntri++;
         /* form triangle with R, C, B */
      	    triang[ntri].id[0] = neighb[3];
      	    triang[ntri].v[0][0] = strch * cornp[0];
      	    triang[ntri].v[0][1] = strch * cornp[1];
      	    triang[ntri].v[0][2] = strch * cornp[2];

      	    triang[ntri].id[1] = i;
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[i] / maxobs;
            triang[ntri].v[1][0] = strch * cenp[0];
            triang[ntri].v[1][1] = strch * cenp[1];
            triang[ntri].v[1][2] = strch * cenp[2];

      	    upx_pixel_vector(&neighb[1],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[1]] / maxobs;
      	    triang[ntri].id[2] = neighb[1];
      	    triang[ntri].v[2][0] = strch * cornp[0];
      	    triang[ntri].v[2][1] = strch * cornp[1];
      	    triang[ntri].v[2][2] = strch * cornp[2];

	    ntri++;

      	 if( nval < 8 ) /* corners are short a vertex (TR), form only 3 more 
			triangles */
	 {
         /* form triangle with C, R, T */
      	    triang[ntri].id[0] = i;
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[i] / maxobs;
            triang[ntri].v[0][0] = strch * cenp[0];
            triang[ntri].v[0][1] = strch * cenp[1];
            triang[ntri].v[0][2] = strch * cenp[2];

      	    upx_pixel_vector(&neighb[3],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[3]] / maxobs;
      	    triang[ntri].id[1] = neighb[3];
      	    triang[ntri].v[1][0] = strch * cornp[0];
      	    triang[ntri].v[1][1] = strch * cornp[1];
      	    triang[ntri].v[1][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[4],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[4]] / maxobs;
      	    triang[ntri].id[2] = neighb[4];
      	    triang[ntri].v[2][0] = strch * cornp[0];
      	    triang[ntri].v[2][1] = strch * cornp[1];
      	    triang[ntri].v[2][2] = strch * cornp[2];

	    ntri++;
         /* form triangle with L, C, T */
      	    upx_pixel_vector(&neighb[6],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[6]] / maxobs;
      	    triang[ntri].id[0] = neighb[6];
      	    triang[ntri].v[0][0] = strch * cornp[0];
      	    triang[ntri].v[0][1] = strch * cornp[1];
      	    triang[ntri].v[0][2] = strch * cornp[2];

      	    triang[ntri].id[1] = i;
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[i] / maxobs;
            triang[ntri].v[1][0] = strch * cenp[0];
            triang[ntri].v[1][1] = strch * cenp[1];
            triang[ntri].v[1][2] = strch * cenp[2];

      	    upx_pixel_vector(&neighb[4],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[4]] / maxobs;
      	    triang[ntri].id[2] = neighb[4];
      	    triang[ntri].v[2][0] = strch * cornp[0];
      	    triang[ntri].v[2][1] = strch * cornp[1];
      	    triang[ntri].v[2][2] = strch * cornp[2];

	    ntri++;
         /* form triangle with T, TL, L */
      	    triang[ntri].id[0] = neighb[4];
      	    triang[ntri].v[0][0] = strch * cornp[0];
      	    triang[ntri].v[0][1] = strch * cornp[1];
      	    triang[ntri].v[0][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[5],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[5]] / maxobs;
      	    triang[ntri].id[1] = neighb[5];
      	    triang[ntri].v[1][0] = strch * cornp[0];
      	    triang[ntri].v[1][1] = strch * cornp[1];
      	    triang[ntri].v[1][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[6],&res,cornp);
      	    triang[ntri].id[2] = neighb[6];
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[6]] / maxobs;
            triang[ntri].v[2][0] = strch * cenp[0];
            triang[ntri].v[2][1] = strch * cenp[1];
            triang[ntri].v[2][2] = strch * cenp[2];

      	    ntri++;
         }
         else /*	assume neighbors are ordered: BL, B, BR, R, TR, T, TL */
         {
         /* form triangle with C, R, TR */
      	    triang[ntri].id[0] = i;
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[i] / maxobs;
            triang[ntri].v[0][0] = strch * cenp[0];
            triang[ntri].v[0][1] = strch * cenp[1];
            triang[ntri].v[0][2] = strch * cenp[2];

      	    upx_pixel_vector(&neighb[3],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[3]] / maxobs;
      	    triang[ntri].id[1] = neighb[3];
      	    triang[ntri].v[1][0] = strch * cornp[0];
      	    triang[ntri].v[1][1] = strch * cornp[1];
      	    triang[ntri].v[1][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[4],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[4]] / maxobs;
      	    triang[ntri].id[2] = neighb[4];
      	    triang[ntri].v[2][0] = strch * cornp[0];
      	    triang[ntri].v[2][1] = strch * cornp[1];
      	    triang[ntri].v[2][2] = strch * cornp[2];

	    ntri++;
         /* form triangle with TR, T, C */
      	    triang[ntri].id[0] = neighb[4];
      	    triang[ntri].v[0][0] = strch * cornp[0];
      	    triang[ntri].v[0][1] = strch * cornp[1];
      	    triang[ntri].v[0][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[5],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[5]] / maxobs;
      	    triang[ntri].id[1] = neighb[5];
      	    triang[ntri].v[1][0] = strch * cornp[0];
      	    triang[ntri].v[1][1] = strch * cornp[1];
      	    triang[ntri].v[1][2] = strch * cornp[2];

      	    triang[ntri].id[2] = i;
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[i] / maxobs;
            triang[ntri].v[2][0] = strch * cenp[0];
            triang[ntri].v[2][1] = strch * cenp[1];
            triang[ntri].v[2][2] = strch * cenp[2];

	    ntri++;
         /* form triangle with L, C, T */
      	    upx_pixel_vector(&neighb[7],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[7]] / maxobs;
      	    triang[ntri].id[0] = neighb[7];
      	    triang[ntri].v[0][0] = strch * cornp[0];
      	    triang[ntri].v[0][1] = strch * cornp[1];
      	    triang[ntri].v[0][2] = strch * cornp[2];

      	    triang[ntri].id[1] = i;
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[i] / maxobs;
            triang[ntri].v[1][0] = strch * cenp[0];
            triang[ntri].v[1][1] = strch * cenp[1];
            triang[ntri].v[1][2] = strch * cenp[2];

      	    upx_pixel_vector(&neighb[5],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[5]] / maxobs;
      	    triang[ntri].id[2] = neighb[5];
      	    triang[ntri].v[2][0] = strch * cornp[0];
      	    triang[ntri].v[2][1] = strch * cornp[1];
      	    triang[ntri].v[2][2] = strch * cornp[2];

	    ntri++;
         /* form triangle with T, TL, L */
      	    triang[ntri].id[0] = neighb[5];
      	    triang[ntri].v[0][0] = strch * cornp[0];
      	    triang[ntri].v[0][1] = strch * cornp[1];
      	    triang[ntri].v[0][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[6],&res,cornp);
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[neighb[6]] / maxobs;
      	    triang[ntri].id[1] = neighb[6];
      	    triang[ntri].v[1][0] = strch * cornp[0];
      	    triang[ntri].v[1][1] = strch * cornp[1];
      	    triang[ntri].v[1][2] = strch * cornp[2];

      	    upx_pixel_vector(&neighb[7],&res,cornp);
      	    triang[ntri].id[2] = neighb[7];
            if( maxobs > 0 )
      	    strch = 1.0 + 2.0 / 10.0 * nobs[i] / maxobs;
            triang[ntri].v[2][0] = strch * cornp[0];
            triang[ntri].v[2][1] = strch * cornp[1];
            triang[ntri].v[2][2] = strch * cornp[2];

      	    ntri++;
         }
      }
      doit = FALSE;
   }
}

int init_color_info(skymap,nobs,wght)
float *skymap, *wght;
int *nobs;
{
int i, *nobsptr, nskyval=1;
float skymin, skymax, skymean, skyrms, spread, minspread, maxspread;
float *wghtptr, *skyptr, rabs();

 if( skymap != NULL )	/* must set pntcolor according to sky values */
 {
    skyptr = skymap;
    nobsptr = nobs;
    wghtptr = wght;
 /* first find min,max,etc */
    skymin = *skyptr; 
    skymax = skymin ;
    skymean = 0.0;
   
/* refedine dynamic range, window: 0.001 to 1000.0 */ 
    while( nskyval < NP )
    {
/*
       if( *skyptr < 0.001 ) *skyptr = 0.001;
       if( *skyptr > 1000.0 ) *skyptr = 1000.0;
*/
       skymean = skymean + *skyptr;
       if( *skyptr > skymax ) skymax = *skyptr;
       if( *skyptr < skymin ) skymin = *skyptr;
       nskyval++;
       skyptr++;
    }
    skymean = skymean / nskyval;
    spread = skymax - skymin;
    minspread = skymean - skymin;
    maxspread = skymax - skymean;
    skyrms = 0.0;

/* define colors: linear Red ramp = 255 at skymin, 0 at skymax; linear
   green ramp = 0 at skymin, 255 at skymean, 0 at skymax; linear blue ramp =
   0 at skymin, 255 at skymax. */

   skyptr = skymap;
   for( i = 0; i < NP; i++ )
   { 
      pntcolor[i][0] = 1.0 - ( ((*skyptr) - skymin) / spread );
      pntcolor[i][2] = ((*skyptr) - skymin) / spread;
      if( pntcolor[i][0] > pntcolor[i][2] )
         pntcolor[i][1] = 2.0 * pntcolor[i][2];
      else
         pntcolor[i][1] = 2.0 * pntcolor[i][0];
/*
         if( *skyptr < skymean )
            pntcolor[i][1] = (skymean - (*skyptr)) / minspread;
         else
            pntcolor[i][1] = 1.0 - ( ((*skyptr) - skymean) / maxspread);
*/
      if( nobs != NULL )
	if( *nobsptr++ <= 0 )
        {
           pntcolor[i][0] = 0.1;
           pntcolor[i][1] = 0.1;
           pntcolor[i][2] = 0.1;
        }
      else if( wght != NULL )
	if( *wghtptr++ == 0.0 )
        {
           pntcolor[i][0] = 0.1;
           pntcolor[i][1] = 0.1;
           pntcolor[i][2] = 0.1;
        }
      skyptr++;
   }
   

 }
 else /* no skymap, just image the pixel numbers */
 {
   for( i = 0; i < NP; i++ )
   {
      if( i < NP/6 )
      {
            pntcolor[i][0] = (255 - (i % 256))/255.0;
            pntcolor[i][1] = 0.0;
       	    pntcolor[i][2] = 0.0;
      }
      else if( i < 2*NP/6 )
      {
            pntcolor[i][0] = 0.0;
            pntcolor[i][1] = (255 - (i % 256))/255.0;
       	    pntcolor[i][2] = 0.0;
      }
      else if( i < 3*NP/6 )
      {
            pntcolor[i][0] = 0.0;
            pntcolor[i][1] = 0.0;
       	    pntcolor[i][2] = (255 - (i % 256))/255.0;
      }
      else if( i < 4*NP/6 )
      {
            pntcolor[i][0] = (255 - (i % 256))/255.0;
            pntcolor[i][1] = (255 - (i % 256))/255.0;
       	    pntcolor[i][2] = 0.0;
      }
      else if( i < 5*NP/6 )
      {
            pntcolor[i][0] = (255 - (i % 256))/255.0;
            pntcolor[i][1] = 0.0;
       	    pntcolor[i][2] = (255 - (i % 256))/255.0;
      }
      else if( i < NP )
      {
            pntcolor[i][0] = 0.0;
            pntcolor[i][1] = (255 - (i % 256))/255.0;
       	    pntcolor[i][2] = (255 - (i % 256))/255.0;
      }
   }
  }
}

int readmap(filename,skymap,nobs,wght)
char *filename; float *skymap; int *nobs; float *wght;
{
   int i, nb;
   FILE *fp, *fopen();
   float *skyptr;
   int *nobsptr;
   float *wghtptr;
/*   struct dmr_skyrec { int n_obs; int value; } buff; */
   struct mit_skyrec { float wght; float value; } buff;
   int maxobs;

   maxobs = 0;
   skyptr = skymap;
   nobsptr = nobs;
   wghtptr = wght;
   printf("readmap> opening file: %s\n",filename);
   fp = fopen(filename,"r"); 

   if( fp == NULL )
   {
      printf("readmap> can't open file: %s\n",filename);
      exit(0);
   }
   for( i = 0; i < NP; i++ )
   {
      if( nobs != NULL)
      {
	nb = fscanf(fp," %d %f",nobsptr,skyptr); 
        if( *nobsptr > maxobs ) maxobs = *nobsptr;
      }
/* mit map has weights, not nobs: */
      if( wght != NULL)
      {
	nb = fscanf(fp," %f %f",wghtptr,skyptr); 
      }
      if( nb < 2 )
      {
         printf("readmap> unexpected end-of-file: %s\n",filename);
   	 return(maxobs);
      }
#ifdef DEBUG
	if( nobs != NULL && i % 128 == 0 ) printf("readmap> n_obs= %d, value= %f\n",
				   *nobsptr,*skyptr);
	if( wght != NULL && i % 128 == 0 ) printf("readmap> n_obs= %f, value= %f\n",
				   *wghtptr,*skyptr);
#endif
      skyptr++;
      nobsptr++;
      wghtptr++;
   }
   fclose( fp );
   return( maxobs );
}

main(argc, argv)
int argc; char **argv;
{
int i, x0, y0;
char *skyfile;
float *skymap, *wght;
int *nobs;
int maxobs;
int readmap(), init_triang_info(), init_color_info();

nobs = NULL;
skymap = NULL;
wght = NULL;
maxobs = 0;

if( argc > 1 ) /* should be "-w" or "-n" to indicate %f,%f or %d,%f pairs */
{
   switch( argv[1][1] )
   {
	case 'w':
		   wght = (float *) malloc(NP*sizeof(float));
		   for(i=0; i<NP; i++) wght[0] = 0.0;
		   break;

	case 'n':
		   nobs = (int *) malloc(NP*sizeof(int));
		   for(i=0; i<NP; i++) nobs[0] = 0.0;
		   break;

	default: printf("Only '-w' or '-n' are acceptable, followed by input file.\n");
		 exit(0);
   }
}


if( argc > 2 ) /* get the file name */
{
   skyfile = argv[2];
   skymap = (float *) malloc(NP*sizeof(float));
   for(i=0; i<NP; i++) skymap[0] = 0.0;
   maxobs = readmap(skyfile,skymap,nobs,wght);
}

if( argc > 2 )
{
    x0 = atoi(argv[1]);
    y0 = atoi(argv[2]);
}
else
{
    x0 = 8;
    y0 = 8;
}

/* init unit sphere using quad-cube routines */
init_triang_info(maxobs,nobs,wght);

/* init colors */
init_color_info(skymap,nobs,wght);

prefposition(x0,x0+988,y0,y0+988);
keepaspect(1,1);
winopen(argv[0]);
RGBmode();
RGBwritemask(wrred,wrgrn,wrblu);
drawmode(NORMALDRAW);
doublebuffer();
gconfig();

mmode(MVIEWING);
perspective(450,1.0,0.01,100.0);
loadmatrix(ident);
polarview(dist,10,800,0);
lsetdepth(0,0x7fffff);
zbuffer(TRUE);

while( TRUE )
{
/*
      if( dist >= 20.0 ) delt = 0.1;
      if( dist <= 0.5 ) delt = -0.1;
      dist = dist - delt;
      loadmatrix(ident);
      polarview(dist,10,600,0);
*/
      
      zclear();
      cpack(0);
      clear();
      if( getbutton(MIDDLEMOUSE) ) exit(0);
/*
printf("bottom, zrot= %d\n",zrot);
      finished = FALSE;
      while( !finished )
          if( getbutton(LEFTMOUSE) ) finished = TRUE;
*/
      draw_bottom();
/*
printf("sphere, zrot= %d\n",zrot);
      finished = FALSE;
      while( !finished )
          if( getbutton(LEFTMOUSE) ) finished = TRUE;
*/
      draw_sphere();

      swapbuffers(); 
   }
}


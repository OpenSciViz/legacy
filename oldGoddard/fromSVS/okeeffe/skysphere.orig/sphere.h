#include <stdio.h>
#include <gl.h>
#include <device.h>
#include <math.h>
#define FALSE 0
#define DEBUG

#ifdef MAIN
#define global 
#else
#define global extern
#endif

#define NP 6144
#define RES 6

typedef struct vrtx_lst { int id[3]; float v[3][3]; } VRTX_LST;

global VRTX_LST triang[3*NP];
global float pntcolor[NP][3];
global short res;
global int ntri;
global char finished;


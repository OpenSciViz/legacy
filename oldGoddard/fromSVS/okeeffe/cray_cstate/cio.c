#include <stdio.h>

int copen(lun)
int lun[4];
{
   lun[0] = creat("claips:[aips.other]iras1.dirbe",2);
   lun[1] = creat("claips:[aips.other]iras2.dirbe",2);
   lun[2] = creat("claips:[aips.other]iras3.dirbe",2);
   lun[3] = creat("claips:[aips.other]iras4.dirbe",2);

}

int cclose(lun)
int lun[4];
{
   close(lun[0]);
   close(lun[1]);
   close(lun[2]);
   close(lun[3]);
}

int cwrite(lun,buff)
int lun[4];
char buff[][4];
{
   write(lun[0],&buff[0][0],4);
   write(lun[1],&buff[1][0],4);
   write(lun[2],&buff[2][0],4);
   write(lun[3],&buff[3][0],4);

}

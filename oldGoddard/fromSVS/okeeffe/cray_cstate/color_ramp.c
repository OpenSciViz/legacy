#include "ray.h"

int color_ramp(val,texture)
unsigned char val;
TEXTURE *texture;
{
   float frct, r, g, b;

   frct = val / 255.0;

   if( frct < 0.2 )
   {
      r = frct / 0.2;
      g = 0.0;
      b = frct / 0.2;
   }
   else if( frct < 0.4 )
   {
      r = (0.4 - frct) / 0.2;
      g = (frct - 0.2) / 0.2;
      b = 1.0;
   }
   else if( frct < 0.6 )
   {
      r = (frct - 0.4) / 0.2;
      g = 1.0 - 0.5*(frct - 0.4) / 0.2;
      b = (0.6 - frct) / 0.2;
   }
   else if( frct < 0.8 )
   {
      r = (frct - 0.6) / 0.2;
      g = 0.5*(0.8 - frct)/0.2;
      b = 0.0;
   }
   else
   {
      r = 1.0;
      g = 0.5 * (1.0 - frct) / 0.2;
      b = g;
   }
   texture->red = 255 * r;
   texture->green = 255 * g;
   texture->blue = 255 * b;
}

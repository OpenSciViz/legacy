#include "ray.h"

int init_rot(ang,rot)
float ang;
float rot[3][3];
{
  float pi=3.1415926;
  static float rz[3][3], ry[3][3], tmp1[3][3], tmp2[3][3];
  int i, j, k;

  rz[0][0] = cos(pi/4.0);
  rz[0][1] = sin(pi/4.0);
  rz[1][0] = -sin(pi/4.0);
  rz[1][1] = rz[0][0];
  rz[2][2] = 1.0;

  ry[0][0] = cos(pi/4.0);
  ry[0][2] = sin(pi/4.0);
  ry[2][0] = -sin(pi/4.0);
  ry[2][2] = rz[0][0];
  ry[1][1] = 1.0;

  tmp2[0][0] = cos(ang);
  tmp2[0][1] = sin(ang);
  tmp2[1][0] = -sin(ang);
  tmp2[1][1] = tmp2[0][0];
  tmp2[2][2] = 1.0;
 
  for( i = 0; i < 3; i++ )
    for( j = 0; j < 3; j++ )
      for( k = 0; k < 3; k++ )
         tmp1[i][j] = tmp1[i][j] + ry[i][k] * rz[k][j];

  for( i = 0; i < 3; i++ )
    for( j = 0; j < 3; j++ )
      for( k = 0; k < 3; k++ )
         rot[i][j] = rot[i][j] + tmp2[i][k] * tmp1[k][j];

  return;
}


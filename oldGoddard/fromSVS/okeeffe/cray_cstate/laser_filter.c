#include <stdio.h>
#include <string.h>
#include <math.h>

main(argc, argv)
int argc; char **argv;
{
  int fd, nb, i, j, k=0;
  unsigned char buff[256*256];
  float mag, surf[256*256], slopx[256*256], slopy[256*256];
  float surf512[512*512], slopx512[512*512], slopy512[512*512];
  char outfile[80];

  if( (fd = open(argv[1],0)) <= 0 ) 
  {
    printf("unable to open input file: %s\n",argv[1]);
    exit(0);
  }

  if( (nb = read(fd,buff,256*256)) < 256*256 ) 
    printf("only read %d bytes...\n",nb);

  close(fd);

  for( i=0; i < 256; i++ )
    for( j=0; j < 256; j++ )
    {
      surf[k] = buff[k];
      if( i < 255 && j < 255 )
      { 
        slopy[k] = surf[k+1] - surf[k];
        slopx[k] = surf[k+256] - surf[k];
        mag = sqrt( slopx[k]*slopx[k] + slopy[k]*slopy[k] );
        slopx[k] = slopx[k]/mag;
        slopy[k] = slopy[k]/mag;
      }
      else
      {
        slopx[k] = slopx[k-256];
        slopy[k] = slopy[k-256];
      }
      surf512[2*k] = surf[k];
      surf512[2*k+1] = surf[k];
      surf512[2*k+512] = surf[k];
      surf512[2*k+1+512] = surf[k];
      slopx512[2*k] = slopx[k];
      slopx512[2*k+1] = slopx[k];
      slopx512[2*k+512] = slopx[k];
      slopx512[2*k+1+512] = slopx[k];
      slopy512[2*k] = slopy[k];
      slopy512[2*k+1] = slopy[k];
      slopy512[2*k+512] = slopy[k];
      slopy512[2*k+1+512] = slopy[k];
      k++;
    }

  strcpy(outfile,argv[1]);
  strcat(outfile,".norm");
  if( (fd = creat(outfile,0644)) <= 0 ) 
  {
    printf("unable to open ouput file: %s\n",outfile);
    exit(0);
  }

  if( (nb = write(fd,surf512,4*512*512)) < 4*512*512 ) 
    printf("only read %d bytes...\n",nb);
  if( (nb = write(fd,slopx512,4*512*512)) < 4*512*512 ) 
    printf("only read %d bytes...\n",nb);
  if( (nb = write(fd,slopy512,4*512*512)) < 4*512*512 ) 
    printf("only read %d bytes...\n",nb);

  close(fd);
}

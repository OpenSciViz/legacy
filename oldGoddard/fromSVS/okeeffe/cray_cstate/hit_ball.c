#include "ray.h"

SURFACE *hit_ball(ball,base,ray)
SPHERE *ball;
float base[3], ray[3];
{
    static SURFACE hit;
    TEXTURE *ball_texture();
    float dist, distsqr, costh=0.0, tmp=0.0, lnear, lfar, rmag;
    float bvec[3], strike[3], normal[3];

#ifdef DEBUG
printf("hit_ball> checking for hit with ball # %d\n",ball->id);
#endif
/* find distance to ball center */
    distsqr = (ball->x - base[0]) * (ball->x - base[0]) + 
		(ball->y - base[1]) * (ball->y - base[1]) +
		(ball->z - base[2]) * (ball->z - base[2]);
    dist = sqrt( distsqr );

    if( dist > 0.999*ball->r ) /* ray is outside ball */
    {
	bvec[0] = (ball->x - base[0]) / dist;
	bvec[1] = (ball->y - base[1]) / dist;
	bvec[2] = (ball->z - base[2]) / dist;
/* angle between ray & direction of ball center */
        costh = bvec[0]*ray[0] + bvec[1]*ray[1] + bvec[2]*ray[2];
	if( costh <= 0.0 ) 
	    return( (SURFACE *) NULL );
	tmp = ball->r*ball->r - distsqr * (1-costh*costh); 
	if( tmp <= 0.0 )
	    return( (SURFACE *) NULL );
/* if we get here, we have a hit */
#ifdef DEBUG
printf("hit_ball> hit ball # %d\n",ball->id);
#endif
	tmp = sqrt( tmp );
	lnear = dist * costh - tmp;
	strike[0] = base[0] + lnear * ray[0];
	strike[1] = base[1] + lnear * ray[1];
	strike[2] = base[2] + lnear * ray[2];
/* construct normal of ball at point of intersection (strike) */
	normal[0] = strike[0] - ball->x;
	normal[1] = strike[1] - ball->y;
	normal[2] = strike[2] - ball->z;
	rmag = sqrt( normal[0]*normal[0] + 
			normal[1]*normal[1] + 
			normal[2]*normal[2] );
	normal[0] = normal[0]/rmag;
	normal[1] = normal[1]/rmag;
	normal[2] = normal[2]/rmag;

/*
        if( ball->tran != NULL ) this is more work 
        {
      	    if( ball->thick = 0.0 )  solid 
      	    {

            }
            else	think of it as two concentric spheres 
            {
                t = ball->thick * ball->r; radial dist. between surfaces 
            }
        }
*/
    }
    else /* ray emanates from inside ball */
    {
/* check angle ray makes with ball (external) normal */
      normal[0] = (base[0] - ball->x) / dist;
      normal[1] = (base[1] - ball->y) / dist;
      normal[2] = (base[2] - ball->z) / dist;
      costh = normal[0]*ray[0] + normal[1]*ray[1] + normal[2]*ray[2];
/* if the vectors are nearly parallel, we want the "near" solution,
 if anti-parallel the "far" solution */
      tmp = ball->r*ball->r - distsqr * (1-costh*costh);
      if( tmp <= 0.0 ) 
      {
	printf("hit_ball> bad news inside ball..\n");
	return( (SURFACE *) NULL );
      } 
      tmp = sqrt( tmp );
      if( costh > 0.0 )
      {
	lnear = dist * costh - tmp;
	strike[0] = base[0] + lnear * ray[0];
	strike[1] = base[1] + lnear * ray[1];
	strike[2] = base[2] + lnear * ray[2];
      }
      else
      { 
	lfar = dist * costh + tmp;
	strike[0] = base[0] + lfar * ray[0];
	strike[1] = base[1] + lfar * ray[1];
	strike[2] = base[2] + lfar * ray[2];
      }
/*
 since we are inside the ball use the internal normal 
*/
      normal[0] = -normal[0];
      normal[1] = -normal[1];
      normal[2] = -normal[2];
    }
/* now set surface structure values */
    if( ball->texture != NULL )
      hit.texture = ball_texture(normal,ball); 
    else
      hit.texture = NULL;

    hit.type = descr_ball;
    hit.id = ball->id;
    hit.thick = ball->thick;
    hit.pos.x = strike[0];
    hit.pos.y = strike[1];
    hit.pos.z = strike[2];
    hit.norm.x = normal[0];
    hit.norm.y = normal[1];
    hit.norm.z = normal[2];
    hit.refl = ball->refl;
    hit.tran = ball->tran;

    return( &hit );
}


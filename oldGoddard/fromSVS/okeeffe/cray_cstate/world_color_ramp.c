#include "ray.h"

int world_color_ramp(val,texture)
unsigned char val;
TEXTURE *texture;
{
   if( val == 0 )
   {
      texture->red = 0;
      texture->green = 0;
      texture->blue = 255;
   }
   else
   {
      texture->red = 128;
      texture->green = 128;
      texture->blue = 64;
   }
}

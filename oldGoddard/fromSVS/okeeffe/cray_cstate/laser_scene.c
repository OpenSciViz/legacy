#include "ray.h"

int laser_scene(kmax,nsurf_elem,time,filenm)
float kmax;
int nsurf_elem;
float time;
char *filenm;
{
/* makes use of:
global REFLECT *refl[N_SURFS];
global int NFXY;
global float *fxy_surf , *fxy_slopx, *fxy_slopy;
*/
int fd;
char *sea_file = "../cstate/sea_elev_slopxy.dat";
unsigned char byte;
int i=0, j=0, nb=0;
float scale_xy, newscale;
SURFACE *sea, *get_fxy();

NCON = 0; NCYL = 0; 
NP = 0;
NL = 1;
NB = 1;
NFXY = 1;
NSURF_ELEM = nsurf_elem;
KMAX = kmax;
scale_xy = KMAX; /* kmax = 1.2 rad/cm for pierson-stacy */
/* scale_xy = scale_xy / 256; */
scale_xy = 2.0 * 3.1415926 / scale_xy; /* smallest wavelength in cm */
FACETLEN = scale_xy / 2.0; /* fraction of smallest wavelength present */

/* allocate surface arrays and ruler texture map */
fxy_surf = (float *) malloc(NSURF_ELEM*NSURF_ELEM*sizeof(float));
fxy_slopx = (float *) malloc(NSURF_ELEM*NSURF_ELEM*sizeof(float));
fxy_slopy = (float *) malloc(NSURF_ELEM*NSURF_ELEM*sizeof(float));

/* floating point valued surface: */
if( strlen(filenm) > 0 )
{ 
  fd = open(filenm,0);
  if( fd > 0 ) 
    printf("ray> opened %s for input\n",filenm);
  else
  {
    printf("ray> sorry could not open %s",filenm);
    exit(0);
  }
}
else
{
    printf("ray> bad input file name! %s",filenm);
    exit(0);
}

nb =  read(fd,fxy_surf,sizeof(float)*NSURF_ELEM*NSURF_ELEM);
nb =  read(fd,fxy_slopx,sizeof(float)*NSURF_ELEM*NSURF_ELEM);
nb =  read(fd,fxy_slopy,sizeof(float)*NSURF_ELEM*NSURF_ELEM);
close( fd );

/* set min, max and facet length */
HITEMAX = 0.0;
HITEMIN = 0.0;
for( i = 0; i < NSURF_ELEM*NSURF_ELEM; i++ )
{
  if( fxy_surf[i] > HITEMAX ) HITEMAX = fxy_surf[i];
  if( fxy_surf[i] < HITEMIN ) HITEMIN = fxy_surf[i];
/* and redefine slopes according to the FACETLEN: */
  fxy_slopx[i] = fxy_slopx[i] / FACETLEN;
  fxy_slopy[i] = fxy_slopy[i] / FACETLEN;
}

/* diffuse light is also white, but not as bright */
diffuse.red = 128;
diffuse.green = 128;
diffuse.blue = 128;

/* a gray surface */
   refl[0] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[0]->spec_pwr = 12.0;
   refl[0]->red_coeff = 1.0;
   refl[0]->green_coeff = 1.0;
   refl[0]->blue_coeff = 1.0;
/* a red surface */
   refl[1] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[1]->spec_pwr = 12.0;
   refl[1]->red_coeff = 1.0;
   refl[1]->green_coeff = 0.2;
   refl[1]->blue_coeff = 0.2;
/* a green surface */
   refl[2] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[2]->spec_pwr = 12.0;
   refl[2]->red_coeff = 0.2;
   refl[2]->green_coeff = 1.0;
   refl[2]->blue_coeff = 0.2;
/* a blue surface */
   refl[3] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[3]->spec_pwr = 12.0;
   refl[3]->red_coeff = 0.2;
   refl[3]->green_coeff = 0.2;
   refl[3]->blue_coeff = 1.0;

  if( NB > 0 ) /* place bouyees */
  {
   balls[0].id = 0;
   balls[0].r = 16.0 * scale_xy;
   balls[0].x = -4*balls[0].r;
   balls[0].y = 4*balls[0].r;
   sea = get_fxy(1,balls[0].x,balls[0].y);
   balls[0].z = balls[0].r + sea->pos.z;
   balls[0].thick = 0;
   balls[0].refl = refl[1]; /* red */
   balls[0].tran = NULL; /* (REFRACT *) malloc(sizeof(REFRACT)); 
   balls[0].tran->refract_indx = 1.5;
   balls[0].tran->red_coeff = time_trns;
   balls[0].tran->green_coeff = time_trns;
   balls[0].tran->blue_coeff = time_trns; */
   balls[0].texture = NULL;

   balls[1].id = 1;
   balls[1].r = 16.0 * scale_xy;
   balls[1].x = 4.0*balls[1].r;
   balls[1].y = 4.0*balls[1].r;
   sea = get_fxy(1,balls[1].x,balls[1].y);
   balls[1].z = balls[1].r + sea->pos.z;
   balls[1].thick = 0;
   balls[1].refl = refl[2]; /* green */
   balls[1].tran = NULL; /* (REFRACT *) malloc(sizeof(REFRACT)); 
   balls[1].tran->refract_indx = 1.5;
   balls[1].tran->red_coeff = time_trns;
   balls[1].tran->green_coeff = time_trns;
   balls[1].tran->blue_coeff = time_trns; */
   balls[1].texture = NULL;

   balls[2].id = 2;
   balls[2].r = 16.0 * scale_xy;
   balls[2].x = -4.0*balls[2].r;
   balls[2].y = -4.0*balls[2].r;
   sea = get_fxy(1,balls[2].x,balls[2].y);
   balls[2].z = balls[2].r + sea->pos.z;
   balls[2].thick = 0;
   balls[2].refl = refl[3]; /* blue */
   balls[2].tran = NULL; /* (REFRACT *) malloc(sizeof(REFRACT)); 
   balls[2].tran->refract_indx = 1.5;
   balls[2].tran->red_coeff = time_trns;
   balls[2].tran->green_coeff = time_trns;
   balls[2].tran->blue_coeff = time_trns; */
   balls[2].texture = NULL;

   printf("%f %f %f\n",balls[0].z,balls[1].z,balls[2].z);
  }
/* init light source postion  */
lamp.x = (time-100.0)*scale_xy;
lamp.y = (100.0-time)*scale_xy;
lamp.z = 4*HITEMAX;
lamp.r = 8.0 * scale_xy;
/* lamp is white */
lamp.intens.red = 255;
lamp.intens.green = 255;
lamp.intens.blue = 255;

/* the ruler */
 if( NP > 0 )
 {
   fd = open("/usr/local/data/ruler.100m",0);
   nb = read(fd,ruler_map,200*1060);
   close( fd );

   polys[0] = (POLY *) malloc(sizeof(POLY));
   for( i=0; i<4; i++ )
   {
      verts[i] = (VERTEX_LIST *) malloc(sizeof(VERTEX_LIST));
      coords[i] = (TRIPLET *) malloc(sizeof(TRIPLET));
   }

   polys[0]->id = -1;
   polys[0]->n_vert = 4;
   polys[0]->norm.x = 0;
   polys[0]->norm.y = -1;
   polys[0]->norm.z = 0;
   polys[0]->tran = NULL;
   polys[0]->texture = (TEXTURE *) malloc(sizeof(TEXTURE)); 
   polys[0]->refl = refl[0];  
   polys[0]->vertex = verts[0];
   verts[0]->coord = coords[0];
   coords[0]->x = -6.0*balls[0].r;
   coords[0]->y = 7.0*balls[0].r;
   coords[0]->z = balls[0].r;
   verts[0]->next = verts[1];
   verts[1]->coord = coords[1];
   coords[1]->x = 5.0*balls[0].r;
   coords[1]->y = 7.0*balls[0].r;
   coords[1]->z = balls[0].r;
   verts[1]->next = verts[2];
   verts[2]->coord = coords[2];
   coords[2]->x = 5.0*balls[0].r;
   coords[2]->y = 7.0*balls[0].r;
   coords[2]->z = 2.5*balls[0].r;
   verts[2]->next = verts[3];
   verts[3]->coord = coords[3];
   coords[3]->x = -6.0*balls[0].r;
   coords[3]->y = 7.0*balls[0].r;
   coords[3]->z = 2.5*balls[0].r;
   verts[3]->next = verts[0];
 }
}

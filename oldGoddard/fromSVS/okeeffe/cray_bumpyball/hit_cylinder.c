#include "ray.h"
SURFACE *hit_cylinder(cyl,emin,ray)
CYLINDER *cyl;
float emin[3], ray[3];
{
    SURFACE *hit;
    float c_axis[3], cvec[3];
    float re, rt, rb, eb, et, ee, cc;
    float mag, normal[3],a_strike[3], b_strike[3], strike[3];
    float sa, sb, s; /* stretch factor */
    float a, b, c, d;

    hit = NULL;
/* all possible dot products will be needed */
    re = ray[0]*emin[0] + ray[1]*emin[1] + ray[2]*emin[2];
    rt = ray[0]*cyl->top->x + ray[1]*cyl->top->y + ray[2]*cyl->top->z;
    rb = ray[0]*cyl->bot->x + ray[1]*cyl->bot->y + ray[2]*cyl->bot->z;
    ee = emin[0]*emin[0] + emin[1]*emin[1] + emin[2]*emin[2];
    et = emin[0]*cyl->top->x + emin[1]*cyl->top->y + emin[2]*cyl->top->z;
    eb = emin[0]*cyl->bot->x + emin[1]*cyl->bot->y + emin[2]*cyl->bot->z;
/* using quadratic equation: ax^2 + bx + c = 0 soln: */

    a = cyl->h * cyl->h;
    if( a <= 0.0 ) 
      	return( hit );	/* otherwise we have something un-solvable */
        
    b = 2.0*a*re + (1.0 - 2.0*a)*rb - rt;
    c = a*ee + (1.0 - 2.0*a)*eb - et + (a - 1.0)*cyl->bb + cyl->bt - cyl->r_sqr;
    d = b*b - 4.0*a*c;
/*    d = fabs(d); */
    if( d < 0.0 ) return( hit ); 
    d = sqrt(d);
/* stretch factor is nearest positive soln to quadratic equation above */
    sa = (-b - d) / (2.0*a);
    sb = (-b + d) / (2.0*a);
/*
    if( (sa < 0) && (sb < 0) ) return( hit );
    if( (sa > 0)  && (sb < 0) ) s = sa;
    if( (sb > 0)  && (sa < 0) ) s = sb;
    if( (sa > 0) && (sb > 0) ) 
    {
        if( sa > sb )
	    s = sb;
        else
	    s = sa;
    }
    if( s > VISIBILITY ) return( hit );
*/

    a_strike[0] = sa * ray[0] + emin[0];
    a_strike[1] = sa * ray[1] + emin[1];
    a_strike[2] = sa * ray[2] + emin[2];
    sa = (a_strike[0] - emin[0]) * (a_strike[0] - emin[0]) +
      	  (a_strike[1] - emin[1]) * (a_strike[1] - emin[1]) +
      	   (a_strike[2] - emin[2]) * (a_strike[2] - emin[2]);
    b_strike[0] = sb * ray[0] + emin[0];
    b_strike[1] = sb * ray[1] + emin[1];
    b_strike[2] = sb * ray[2] + emin[2];
    sa = (a_strike[0] - emin[0]) * (a_strike[0] - emin[0]) +
      	  (a_strike[1] - emin[1]) * (a_strike[1] - emin[1]) +
      	   (a_strike[2] - emin[2]) * (a_strike[2] - emin[2]);

    if( sa < sb ) 
    {
       strike[0] = a_strike[0];
       strike[1] = a_strike[1];
       strike[2] = a_strike[2];
    }
    else
    {
       strike[0] = b_strike[0];
       strike[1] = b_strike[1];
       strike[2] = b_strike[2];
    }
    cvec[0] = strike[0] - cyl->bot->x;
    cvec[1] = strike[1] - cyl->bot->y;
    cvec[2] = strike[2] - cyl->bot->z;
    cc = cvec[0]*cyl->axis->x + cvec[1]*cyl->axis->y + cvec[2]*cyl->axis->z;
/* check if hit lies within end points of the cylinder */
    if( cc < 0.0 ) return( hit );
    if( cc > cyl->h ) return( hit );
/* normal on the outside of the cylinder: */
    normal[0] = cvec[0] - cc*cyl->axis->x;
    normal[1] = cvec[1] - cc*cyl->axis->y;
    normal[2] = cvec[2] - cc*cyl->axis->z;
    mag = sqrt(normal[0]*normal[0] + normal[1]*normal[1] +
      		normal[2]*normal[2]);
    normal[0] = normal[0] / mag;
    normal[1] = normal[1] / mag;
    normal[2] = normal[2] / mag;

/* if we get here we need to allocate a surface structure to return */
    hit = (SURFACE *) malloc(sizeof(SURFACE));
    if( (normal[0]*ray[0] + normal[1]*ray[1] + normal[2]*ray[2]) < 0.0 )
    {	/* surface normal & ray directions should be in opposite directions */
        hit->norm.x = normal[0];
        hit->norm.y = normal[1];
        hit->norm.z = normal[2];
    }
    else	/* normal on the inside */
    {
        hit->norm.x = -normal[0];
        hit->norm.y = -normal[1];
        hit->norm.z = -normal[2];
    }
    hit->type = &descr_cyl[0];
    hit->id = cyl->id;
    hit->pos.x = strike[0];
    hit->pos.y = strike[1];
    hit->pos.z = strike[2];
    hit->refl = cyl->refl;
    hit->tran = cyl->tran;
    return( hit );
}

#include "ray.h"

SURFACE *hit_fxy(base,ray)
float base[3], ray[3];
{
  float x, y, z, temp, s;
  float start, castray[3];
  short castest=TRUE;
  SURFACE *surf=NULL, *get_fxy();

#ifdef DEBIG
  printf("hit_fxy> eman: %f %f %f  ray: %f %f %f\n",base[0],base[1],
		base[2],ray[0],ray[1],ray[2]);
#endif

  if( (base[2] > HITEMAX) && (ray[2] >= 0.0) ) 
    return( surf );

  if( base[2] > HITEMAX ) /* emanation point is above surface */
  {
    s = -(base[2] - HITEMAX)/ ray[2];
    x = s * ray[0] + base[0];
    y = s * ray[1] + base[1];
    z = s * ray[2] + base[2];

    if( fabs(base[0] - x) >= VISIBILITY  || 
      	    fabs(base[1] - y) >= VISIBILITY  || 
      	    fabs(base[2] - z) >= VISIBILITY ) return( surf );

/* find where (x,y) lies in 'virtual patch' and get height
and slopes as well as texture map  */
    surf = get_fxy(1,x,y);
    if( surf == NULL ) /* check the HITEMIN boundary */
    {
      s = -(base[2] - HITEMIN) / ray[2];
      surf = get_fxy(1,s*ray[0]+base[0],s*ray[1]+base[1]);
      if( surf == NULL ) /* we can't even hit the bounding volume! */
        return( surf );
    }
/* the following logic applies only to mean zero surfaces, i.e.
   HITEMIN is guaranteed to be < 0 and HITEMAX > 0 */
    while( castest && (z >= 1.001*HITEMIN) && (z <= 1.001*HITEMAX) )
    {
      z = z + FACETLEN * ray[2];
      x = x + FACETLEN * ray[0];
      y = y + FACETLEN * ray[1];
/* find where (x,y) lies in 'virtual patch' and get height */
      surf = get_fxy(1,x,y);
      if( surf != NULL )
      {
        if( fabs(surf->pos.z - z) <= FACETLEN ) /* hit surface */
          castest = FALSE;
        else
          surf = NULL;
      }
    }
  }
/*
  else  emanation is somewhere on or near the surface 
  {
    x = base[0];
    y = base[1];
    z = base[2];
    while( castest && (z >= 1.001*HITEMIN) && (z <= 1.001*HITEMAX) )
    {
      z = z + FACETLEN * ray[2];
      x = x + FACETLEN * ray[0];
      y = y + FACETLEN * ray[1];
 find where (x,y) lies in 'virtual patch' and get height 
      surf = get_fxy(1,x,y);
      if( surf != NULL )
      {
        if( fabs(surf->pos.z - z) <= FACETLEN ) 
          castest = FALSE;
        else
          surf = NULL;
      }
    }
  }
*/
/* once we get here, we either have a hit (surf != NULL) or we missed,
   i.e. castest is still true, but we exited the loop */

  if( castest )
    return( (SURFACE *) NULL );
  else
    return( surf );
}

#include "ray.h"
raycolor(base,red,green,blue)
float base[3]; float *red, *green, *blue;
{
   float mag, unit_dir[3];
   static RAY root;
   SURFACE *prev_surf=NULL;

#ifdef DEBUG
   printf("raycolor> viewpnt: %g %g %g\n",vwpnt[0],vwpnt[1],vwpnt[2]);
   printf("raycolor> base: %g %g %g\n",base[0],base[1],base[2]);
#endif
   prev_surf = NULL;
   root.depth = 1;

/* for a standard perspective, the direction to cast the ray is defined by: */

   unit_dir[0] = base[0] - vwpnt[0];
   unit_dir[1] = base[1] - vwpnt[1];
   unit_dir[2] = base[2] - vwpnt[2];

/* for an orthographic projection all rays are parallel: 
   unit_dir[0] = (- vwpnt[0]);
   unit_dir[1] = (- vwpnt[1]);
   unit_dir[2] = (- vwpnt[2]);
*/

   mag = unit_dir[0]*unit_dir[0] + 
		unit_dir[1]*unit_dir[1] + 
			unit_dir[2]*unit_dir[2];
   mag = sqrt(mag);
   unit_dir[0] = unit_dir[0] / mag;
   unit_dir[1] = unit_dir[1] / mag;
   unit_dir[2] = unit_dir[2] / mag;

/* the root ray is extended from the viewpoint */
   root.dir[0] = unit_dir[0];
   root.dir[1] = unit_dir[1];
   root.dir[2] = unit_dir[2];
   root.base[0] = base[0];
   root.base[1] = base[1];
   root.base[2] = base[2];

#ifdef DEBUG
printf("raycolor> root.base= %f %f %f\n",base[0],base[1],base[2]);
printf("raycolor> root.unit_dir= %f %f %f\n",unit_dir[0],unit_dir[1],unit_dir[2]);
#endif

/* cast the ray recursively defining its net intensity. the 'root' ray
   can be thought of as the ray 'transmitted' thru the camera pin-hole. */
   raycast(prev_surf,&root);

   *red = root.intens.red;
   *green = root.intens.green;
   *blue = root.intens.blue;

   return(0);
}


#include "ray.h"

int init_rot(ang)
float ang[2];
{
/* for an arbitrary rotation:?
  float pi=3.1415926;
  static float rz[3][3], ry[3][3], tmp1[3][3], tmp2[3][3];
  int i, j, k;

  rz[0][0] = cos(pi/4.0);
  rz[0][1] = sin(pi/4.0);
  rz[1][0] = -sin(pi/4.0);
  rz[1][1] = rz[0][0];
  rz[2][2] = 1.0;

  ry[0][0] = cos(pi/4.0);
  ry[0][2] = sin(pi/4.0);
  ry[2][0] = -sin(pi/4.0);
  ry[2][2] = rz[0][0];
  ry[1][1] = 1.0;

  tmp2[0][0] = cos(ang);
  tmp2[0][1] = sin(ang);
  tmp2[1][0] = -sin(ang);
  tmp2[1][1] = tmp2[0][0];
  tmp2[2][2] = 1.0;
 
  for( i = 0; i < 3; i++ )
    for( j = 0; j < 3; j++ )
      for( k = 0; k < 3; k++ )
         tmp1[i][j] = tmp1[i][j] + ry[i][k] * rz[k][j];

  for( i = 0; i < 3; i++ )
    for( j = 0; j < 3; j++ )
      for( k = 0; k < 3; k++ )
         rotate[i][j] = rotate[i][j] + tmp2[i][k] * tmp1[k][j];
*/
 int i, j;
/* equatorial to galactic trans.: 
 static float EtoG[3][3] = {-0.0669884,0.4927285,-0.8676007,
                      -0.8727557,-0.4503467,-0.1883749,
                      -0.4835390, 0.7445847, 0.4601998};
*/
 static float EtoG[3][3] = {-0.0669884, -0.8727557, -0.4835390,
				0.4927285, -0.4503467, 0.74458847,
				-0.8676007, -0.1883749, 0.4601998};

/* First rotate in Eq. coord. sys. (ang about Z axis) */
 EQrot[0][0] = cos(ang[0]);
 EQrot[0][1] = -sin(ang[0]);
 EQrot[1][0] = -EQrot[0][1];
 EQrot[1][1] = EQrot[0][0];
 EQrot[2][2] = 1.0;

 for( i = 0; i < 3; i++ )
  for( j = 0; j < 3; j++ )
    GALrot[i][j] = EtoG[i][0] * EQrot[0][j] + 
			EtoG[i][1] * EQrot[1][j] + 
			EtoG[i][2] * EQrot[2][j]; 

/* and redefine EQrot for ang[1] 
 EQrot[0][0] = cos(ang[1]);
 EQrot[0][1] = sin(ang[1]);
 EQrot[1][0] = -EQrot[0][1];
 EQrot[1][1] = EQrot[0][0];
 EQrot[2][2] = 1.0;
*/
  return;
}


#include <stdio.h>
#include <math.h>

main()
{
 char *file;
 unsigned char oz;
 FILE *fp_in, *fp_out;
 int i, j, k;
 unsigned char ozinp[180][360];
 float uvec[3], min=255, max=0;
 short res = 9;
 int colatt, azim;

   printf("create_ozone_maps> reformatting ozone data to dirbe skymaps...\n");
   fp_in = fopen("claips:[aips.other]ozone.oct87","r"); /* all 20 days worth */

   for( k = 0; k < 20; k++ )
   {
      for( i = 0; i < 180; i++ )
      {
         for( j = 0; j < 360; j++ )
         {
      	    ozinp[i][j] = getc(fp_in);
      	    if( ozinp[i][j] > max ) max = ozinp[i][j];
      	    if( ozinp[i][j] < min ) min = ozinp[i][j];
         }
      }

      fp_out = fopen("clscratch:[xrhon]ozone.dirbe","w"); /* one day worth */
      for( i = 0; i < 393216; i++ ) 
      {
      	  upx_pixel_vector(&i,&res,uvec);
          colatt = acos( uvec[2] ) * 57.3;
      	  azim = atan2( uvec[0], uvec[1] ) * 57.3 + 180.0;
          oz = 255 * (ozinp[colatt][azim] - min ) / (max - min);
          putc(oz,fp_out);
      }
      close(fp_out);
   }
   fclose(fp_in);
   printf("init_ozone_map> finished constructing ozone map, npix= %d\n",i);
}




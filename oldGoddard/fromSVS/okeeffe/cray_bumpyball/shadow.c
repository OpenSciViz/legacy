#include "ray.h"
char shadow(pos)
TRIPLET *pos;
{
   SURFACE *object=NULL, *raystrike();
   char boolean=FALSE;
   float mag, dir[3];

/* make unit vector out of direction to light source from here */
   dir[0] = lamp.x - pos->x;
   dir[1] = lamp.y - pos->y;
   dir[2] = lamp.z - pos->z;
   mag = dir[0]*dir[0] + dir[1]*dir[1] + dir[2]*dir[2];
   mag = sqrt(mag);
   dir[0] = dir[0] / mag;
   dir[1] = dir[1] / mag;
   dir[2] = dir[2] / mag;

#ifdef DEBUG
  printf("shadow> eman: %f %f %f  ray: %f %f %f\n",base[0],base[1],
	base[2],dir[0],dir[1],dir[2]);
#endif
   if( (object = raystrike((float *)pos,dir)) != NULL )
     if( object->type != descr_light )
       boolean = TRUE;

   return( boolean );
}


#include "ray.h"
#define HITEMAX 2.0
#define HITEMIN 0.0 
#define EPS 0.3

SURFACE *hit_bumpy_ball(ball,base,ray)
SPHERE *ball;
float base[3], ray[3];
{
  float x, y, z, xtmp, ytmp, ztmp, stretch;
  int i, castest, lat_idx, lon_idx;
  static SURFACE hit;
  float dist, distsqr, costh, tmp, lnear, rmag;
  float bvec[3], strike[3], normal[3];
  float FACETLEN;
  float R0, Rtheta, Rphi, dtheta=180.0/57.3/512.0, dphi=360.0/57.3/1024.0;
  float get_bump();

/* find distance to ball center */
  distsqr = (ball->x - base[0]) * (ball->x - base[0]) + 
		(ball->y - base[1]) * (ball->y - base[1]) +
		(ball->z - base[2]) * (ball->z - base[2]);
  dist = sqrt( distsqr );
  bvec[0] = (ball->x - base[0]) / dist;
  bvec[1] = (ball->y - base[1]) / dist;
  bvec[2] = (ball->z - base[2]) / dist;
/* angle between ray & direction of ball center */
  costh = bvec[0]*ray[0] + bvec[1]*ray[1] + bvec[2]*ray[2];
/* use a radius that is determined by the maximum radial
   distortion on the globe: */
  tmp = (1+EPS)*ball->r * (1+EPS)*ball->r - distsqr * (1-costh*costh); 

  if( dist > (1+EPS)*ball->r ) /* ray is outside ball */
  {
    if( costh <= 0.0 ) 
	return( (SURFACE *) NULL );
    if( tmp <= 0.0 )
	return( (SURFACE *) NULL );
/* if we get here, we have a hit */
#ifdef DEBUG
printf("hit_ball> hit ball # %d\n",ball->id);
#endif
    tmp = sqrt( tmp );
    lnear = dist * costh - tmp;
    strike[0] = base[0] + lnear * ray[0];
    strike[1] = base[1] + lnear * ray[1];
    strike[2] = base[2] + lnear * ray[2];
/* construct normal of ball at point of intersection (strike) */
    normal[0] = strike[0] - ball->x;
    normal[1] = strike[1] - ball->y;
    normal[2] = strike[2] - ball->z;
    rmag = sqrt( normal[0]*normal[0] + normal[1]*normal[1] + normal[2]*normal[2] );

    normal[0] = normal[0]/rmag;
    normal[1] = normal[1]/rmag;
    normal[2] = normal[2]/rmag;

    R0 = get_bump(EPS,normal,ball,&lat_idx,&lon_idx);
    if( R0 > rmag ) /* somehow the bounding sphere is underneath the bump...
			something went wrong! */
      return( (SURFACE *)NULL );

/* now step forwards along ray direction to find strike with rough
   surface */
    xtmp = strike[0];
    ytmp = strike[1];
    ztmp = strike[2];
    castest = TRUE;
    FACETLEN = ball->r / 100.0;

    while( castest )
    {
      xtmp = xtmp + FACETLEN * ray[0];
      ytmp = ytmp + FACETLEN * ray[1];
      ztmp = ztmp + FACETLEN * ray[2];
      rmag = sqrt( xtmp*xtmp + ytmp*ytmp + ztmp*ztmp );
      normal[0] = (xtmp - ball->x) / rmag;
      normal[1] = (ytmp - ball->y) / rmag;
      normal[2] = (ztmp - ball->z) / rmag;
      R0 = get_bump(EPS,normal,ball,&lat_idx,&lon_idx);
      if( (R0 >= rmag) || (rmag < ball->r) || (rmag >= (1+EPS)*ball->r) ) /* then surface intersect */
	castest = FALSE;
    }

    if( (rmag >= ball->r) && (rmag <= (1+EPS)*ball->r) && (R0 >= rmag) ) /* then we definitely have a hit */
    {
      hit.type = descr_ball;
      hit.id = ball->id;
      hit.thick = ball->thick;
      hit.refl = ball->refl;
      hit.tran = ball->tran;
      hit.texture = ball->texture;
      hit.pos.x = xtmp;
      hit.pos.y = ytmp;
      hit.pos.z = ztmp;
      if( world_map[lat_idx][lon_idx] < 253 &&
        world_map[lat_idx][lon_idx] > 0 )
      {
        hit.texture->red = (R0 - ball->r) / (EPS*ball->r) * 150.0;
        hit.texture->green = (R0 - ball->r) / (EPS*ball->r) * 175.0;
        hit.texture->blue = (R0 - ball->r) / (EPS*ball->r) * 255.0;
      }
      else
      {
        hit.texture->red = 175.0;
        hit.texture->green = 100.0;
        hit.texture->blue = 75.0;
      }
      Rtheta = (1 + EPS*world_map[lat_idx][lon_idx+1]/255.0) * ball->r;
      Rphi = (1 + EPS*world_map[lat_idx+1][lon_idx]/255.0) * ball->r;
      bumpy_norm(&hit.pos,R0,Rtheta,Rphi,dtheta,dphi,&hit.norm);
      hit.norm.x = normal[0];
      hit.norm.y = normal[1];
      hit.norm.z = normal[2];
      return( &hit );
    }
    else
      return( (SURFACE *) NULL );
  }
  else /* ray emanates from surface of or inside bumpy ball */
  {
    normal[0] = base[0] - ball->x;
    normal[1] = base[1] - ball->y;
    normal[2] = base[2] - ball->z;
    rmag = sqrt( normal[0]*normal[0] + normal[1]*normal[1] + normal[2]*normal[2] );
    normal[0] = normal[0]/rmag;
    normal[1] = normal[1]/rmag;
    normal[2] = normal[2]/rmag;
    xtmp = base[0];
    ytmp = base[1];
    ztmp = base[2];

/* check whether ray is pointed inward or outward: */
    costh = normal[0]*ray[0] + normal[1]*ray[1] + normal[2]*ray[2];
    if( costh < 0.0 ) /* ray is pointing inward, for now we quit */
      return( (SURFACE *)NULL );
    else
    {
      castest = TRUE;
      FACETLEN = ball->r / 100.0;
    }

    while( castest )
    {
      xtmp = xtmp + FACETLEN * ray[0];
      ytmp = ytmp + FACETLEN * ray[1];
      ztmp = ztmp + FACETLEN * ray[2];
      rmag = sqrt( xtmp*xtmp + ytmp*ytmp + ztmp*ztmp );
      normal[0] = (xtmp - ball->x) / rmag;
      normal[1] = (ytmp - ball->y) / rmag;
      normal[2] = (ztmp - ball->z) / rmag;
      R0 = get_bump(EPS,normal,ball,&lat_idx,&lon_idx);
      if( (R0 <= rmag) || (rmag >= (1+EPS)*ball->r) ) /* possible surface intersect */
	castest = FALSE;
    }
    if( rmag < (1+EPS)*ball->r ) /* hit surface */
    {
      hit.type = descr_ball;
      hit.id = ball->id;
      hit.thick = ball->thick;
      hit.refl = ball->refl;
      hit.tran = ball->tran;
      hit.texture = ball->texture;
      hit.pos.x = xtmp;
      hit.pos.y = ytmp;
      hit.pos.z = ztmp;
      if( world_map[lat_idx][lon_idx] < 253 &&
        world_map[lat_idx][lon_idx] > 0 )
      {
        hit.texture->red = (R0 - ball->r) / (EPS*ball->r) * 150.0;
        hit.texture->green = (R0 - ball->r) / (EPS*ball->r) * 175.0;
        hit.texture->blue = (R0 - ball->r) / (EPS*ball->r) * 255.0;
      }
      else
      {
        hit.texture->red = 175.0;
        hit.texture->green = 100.0;
        hit.texture->blue = 75.0;
      }
      Rtheta = (1 + EPS*world_map[lat_idx][lon_idx+1]/255.0) * ball->r;
      Rphi = (1 + EPS*world_map[lat_idx+1][lon_idx]/255.0) * ball->r;
      bumpy_norm(&hit.pos,R0,Rtheta,Rphi,dtheta,dphi,&hit.norm);
      hit.norm.x = normal[0];
      hit.norm.y = normal[1];
      hit.norm.z = normal[2];
      return( &hit );
    }
    else
      return( (SURFACE *) NULL );

  }
    
}

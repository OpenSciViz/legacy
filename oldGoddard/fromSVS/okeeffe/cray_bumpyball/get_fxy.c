#include "ray.h"

SURFACE *get_fxy(id,x,y)
int id;
float x, y;
{
  int i, j, k;
  float mag, max_length, kmax, kmin;
  static SURFACE surf;

  kmax = KMAX; /* pierson-stacy = 1.2; */
  max_length = NSURF_ELEM * 3.1415926 / kmax; 
/*   max_length = HITEMAX * atan(FIELD_OF_VIEW/2.0); */

  surf.texture = NULL;
  surf.tran = NULL;
  surf.refl = refl[0];
  surf.type = descr_fxy;
  surf.id = id;
  surf.pos.x = x;
  surf.pos.y = y;

/*   i = fabs(x); j = fabs(y); 
  i = (NSURF_ELEM - 1)*fabs(x)/max_length;
  j = (NSURF_ELEM - 1)*fabs(y)/max_length;
*/
  i = NSURF_ELEM/2 + NSURF_ELEM/2*x/max_length;
  j = NSURF_ELEM/2 + NSURF_ELEM/2*y/max_length;

/* do not repeat the surface: */
  if( (i > (NSURF_ELEM - 1)) || (j > (NSURF_ELEM - 1)) || (i < 0 ) || (j < 0) )
    return( (SURFACE *) NULL );

  k = (i % NSURF_ELEM)*NSURF_ELEM + (j % NSURF_ELEM); 
  surf.pos.z = fxy_surf[k];
  surf.norm.z = 1.0;
  surf.norm.x = fxy_slopx[k];
  surf.norm.y = fxy_slopy[k];
  mag = sqrt( surf.norm.z * surf.norm.z +
      		surf.norm.x * surf.norm.x +
      		surf.norm.y * surf.norm.y );
  surf.norm.x = -surf.norm.x / mag;
  surf.norm.y = -surf.norm.y / mag;
  surf.norm.z = surf.norm.z / mag;

/*
   {
      surf.pos.z = 1.0 + sin(x)*sin(y);
      surf.norm.x = -cos(x)*sin(y);
      surf.norm.y = -sin(x)*cos(y);
      surf.norm.z = sqrt( 1.0 - (surf.norm.x)*(surf.norm.x) - 
      			(surf.norm.y)*(surf.norm.y) );
   }
*/
   return( &surf );
}

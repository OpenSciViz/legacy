#define MAIN
#include "ray.h"
/*
#include "file.h"
*/
#define Y_PIXELS	1024
#define X_PIXELS	1024
#define SCREEN_HEIGHT	1.0	/*	 1 meter screen*/
#define SCREEN_WIDTH	1.0
#define PI		3.141592654
/* #define SCANLINE  */

main(argc,argv)
int argc;
char *argv[];
{
   unsigned char red[X_PIXELS], green[X_PIXELS], blue[X_PIXELS];
/*   unsigned gray[X_PIXELS]; */
   float pix_v[3], pix_w[3];
   float r, g, b;
   int fd_red, fd_green, fd_blue, nrw, ngw, nbw;
   int npix, nscan, i_scan, j_pix;
   int trnsfrm(), vec_trnsfm(), raycolor();
/*   int unfor_open(), unfor_write(); */
   float HALF_HEIGHT, HALF_WIDTH, SCREEN_DISTANCE;
   float fnpix, fnscan, mag;
   int outflag, fn_len;
   static char rfile[81], gfile[81], bfile[81];
   long time(), ti, tf;
   float exec_time, mirror_shift, kmax=1.2, time_frame=0.0; /* 0 <= time <= 60 */

   ti = time(0);
/* init some global strings */
   descr_light = "light-source";
   descr_poly = "polygon";
   descr_ball = "ball";
   descr_cone = "cone";
   descr_cyl = "cylinder";
   descr_fxy = "z=f(x,y)";

   npix = X_PIXELS;
   nscan = Y_PIXELS;
   HALF_HEIGHT = SCREEN_HEIGHT / 2.0;
   HALF_WIDTH = SCREEN_WIDTH / 2.0;
   FIELD_OF_VIEW = PI/4.0;
 /* 45 deg. field of view requires placement of photographic plate here 
    in viewing coord sys. */
   SCREEN_DISTANCE = HALF_HEIGHT / tan(0.5*FIELD_OF_VIEW);

/* init viewpoint, units are meters */
   vwpnt[0] = 21.0;
   vwpnt[1] = 21.0;
   vwpnt[2] = 21.0;

   if( argc > 1 ) 
   {
       npix = atoi(argv[1]);
       nscan = npix;
   }
   printf("cray> traycing %d by %d rays:\n",npix,npix);
   if( argc > 2 )
   {
      FIELD_OF_VIEW = atoi(argv[2]) / 57.3;
      SCREEN_DISTANCE = HALF_HEIGHT / tan(0.5*FIELD_OF_VIEW);
   }
   if( argc > 5 )
   {
	vwpnt[0] = atoi(argv[3]);
	vwpnt[1] = atoi(argv[4]);
	vwpnt[2] = atoi(argv[5]);
   }
   printf("cray> using (world x,y,z) viewpoint: %f %f %f\n",
		vwpnt[0],vwpnt[1],vwpnt[2]);
/* put the photgraphic plate (projection screen) at the viewpoint, and
   redefine the viewpoint as the focal point: */
   mag = sqrt( vwpnt[0]*vwpnt[0] + vwpnt[1]*vwpnt[1] + vwpnt[2]*vwpnt[2] );
   vwpnt[0] = vwpnt[0] * ( 1.0 + SCREEN_DISTANCE / mag );
   vwpnt[1] = vwpnt[1] * ( 1.0 + SCREEN_DISTANCE / mag );
   vwpnt[2] = vwpnt[2] * ( 1.0 + SCREEN_DISTANCE / mag );

/* define the viewing transformation */
   trnsfrm();

   if( argc > 6 )
   {
      strcpy(rfile,argv[6]);
      strcpy(gfile,argv[6]);
      strcpy(bfile,argv[6]);
      strcat(rfile,".red");
      strcat(gfile,".grn");
      strcat(bfile,".blu");
   }
   else
   {
      strcpy(rfile,"cray.red");
      strcpy(gfile,"cray.grn");
      strcpy(bfile,"cray.blu");
   }
   rot_ang[0] = 0.0;
   rot_ang[1] = 0.0;
   mirror_shift = 0.0;

   if( argc > 7 )
     time_frame = atoi(argv[7])/100;
   if( argc > 8 )
     kmax = atof(argv[8]);

#ifdef OUTTABLE
   outflag = 1;
#else
   outflag = -1;
#endif
/*
   unfor_open(&outflag,&npix,outfile,&fn_len); 
*/

/* create & open the output file standard C style: */
   fd_red = creat(rfile,0644);
   fd_green = creat(gfile,0644);
   fd_blue = creat(bfile,0644);

/* initialize the scene
   iras_scene();
   logo_scene();
   aipsites_scene();
   lut_scene(mirror_shift);
   spahr_scene();
   cstate_scene(kmax,npix,time_frame,argv[6]);
*/
   globe_scene(time_frame);

/* define location of screen pixels in world coordinates, and cast a ray
   for each pixel, the raycast routine should return the pixel's color */
   pix_v[2] = -SCREEN_DISTANCE;
   fnscan = nscan-1; fnpix = npix-1;

   for( i_scan = nscan-1; i_scan >= 0; i_scan-- )
   {
	pix_v[1] = i_scan * SCREEN_HEIGHT / fnscan - HALF_HEIGHT;
 	for( j_pix = npix-1; j_pix >= 0; j_pix-- )
	{
#ifdef OUTTABLE
           textel = -99;	/* global used in ivas_fullres */
#endif
	   pix_v[0] =  j_pix * SCREEN_WIDTH / fnpix - HALF_WIDTH;
	   vec_trnsfm(pix_v,pix_w);
#ifdef DEBUG
  printf("ray> pix_v: %g %g %g\n",pix_v[0],pix_v[1],pix_v[2]);
  printf("ray> pix_w: %g %g %g\n",pix_w[0],pix_w[1],pix_w[2]);
#endif
/*
       if( i_scan == 2 && j_pix == 6 ) 
       {
         printf("something bad is about to happen!\n");
         getchar();
       }
*/
  	   raycolor(pix_w,&r,&g,&b);
#ifdef OUTTABLE
      	   ivas_fullres[j_pix] = textel;      	   
#endif
/* convert float colors to bytes */
	   if(r > 255.0) r = 255.0; red[j_pix] = r;
	   if(g > 255.0) g = 255.0; green[j_pix] = g;
	   if(b > 255.0) b = 255.0; blue[j_pix] = b;
/*
	   gray[j_pix] = red[j_pix] | green[j_pix] | blue[j_pix];
*/
	}
/* and write to disk files */
	nrw = write(fd_red,red,npix);
	ngw = write(fd_green,green,npix);
	nbw = write(fd_blue,blue,npix);
/*	printf("cray> finished scanline: %d\tcenter rgb: %d %d %d\n",i_scan,
		red[npix/2],green[npix/2],blue[npix/2]); 
	printf("cray> wrote %d red bytes, %d green bytes, %d blue bytes\n",
		nrw,ngw,nbw); */
/*
#ifdef OUTTABLE
	unfor_write(&outflag,&npix,ivas_fullres);
#else
	unfor_write(&outflag,&npix,red,green,blue);
#endif
*/
#ifdef SCANLINE
	for( j_pix = 0; j_pix < npix; j_pix++ )
	{
	   printf("pixel: %d\t file (rgb): %d %d %d\n",j_pix,red[j_pix],
			green[j_pix],blue[j_pix]);
	}
#endif
   }
   close(fd_red); close(fd_green); close(fd_blue); 

   tf = time(0);
   exec_time = (tf - ti)*1.0 / 60.0;
   printf("Execution time in CPU minutes= %f\n",exec_time);
}


#include <math.h>

bumpy_norm(R,R0,Rtheta,Rphi,dtheta,dphi,Norm)
float R[3], R0, Rtheta, Rphi, dtheta, dphi, *Norm;
{
  double N, rho, A, Az, B, x, y, z;

  rho = R[0]*R[0] + R[1]*R[1];

  A = (Rtheta - R0) / (dtheta * R0 * R0);
  B = (Rphi - R0) / (dphi * rho);

  rho = sqrt(rho);
  Az = 1.0 / R0 - R[2] * A / rho;

  x = R[0] * Az + R[1] * B;
  y = R[1] * Az - R[0] * B;
  z = R[2] / R0 + A * rho;

  N = sqrt(x*x + y*y + z*z);

  Norm[0] = x / N;
  Norm[1] = y / N;
  Norm[2] = z / N;
}

#include <math.h>
typedef struct triplet { float x; float y; float z; } TRIPLET;

TRIPLET *poly_norm(v1,v2,v3)
TRIPLET *v1,*v2,*v3;
{
  TRIPLET *norm, va, vb;
  float mag;

  norm = (TRIPLET *) malloc(sizeof(TRIPLET));

  va.x = v2->x - v1->x;
  va.y = v2->y - v1->y;
  va.z = v2->z - v1->z;

  vb.x = v3->x - v1->x;
  vb.y = v3->y - v1->y;
  vb.z = v3->z - v1->z;
 
/* cross product of va and vb: */

  norm->x = va.y*vb.z - va.z*vb.y;
  norm->y = va.z*vb.x - va.x*vb.z;
  norm->z = va.x*vb.y - va.y*vb.x;

  mag = sqrt( norm->x * norm->x +
      		norm->y * norm->y +
      		  norm->z * norm->z );

  norm->x = norm->x / mag;
  norm->y = norm->y / mag;
  norm->z = norm->z / mag;

  return( norm );
}

#include "ray.h"
int cstate_ball_scene(time,filenm)
float time;
char *filenm;
{
/* makes use of:
global REFLECT *refl[N_SURFS];
global int NFXY;
global float *fxy_surf , *fxy_slopx, *fxy_slopy;
*/
int fd_sea;
char *sea_file = "../cstate/sea_elev_slopxy.dat";
unsigned char byte;
int i=0, j=0, sl=0;
float time_refl, time_trns, time_light, time_radius;

/* assuming 3 seconds (60 frames) of animation, each frame should set
   new values for the time_etc numbers, and the x,y,z position of the
   sphere -- light source. */
NP = 0; NCON = 0; NCYL = 0;
NFXY = 1;
NB = 1;

fxy_surf = (float *) malloc(512*512*sizeof(float));
fxy_slopx = (float *) malloc(512*512*sizeof(float));
fxy_slopy = (float *) malloc(512*512*sizeof(float));

sl = strlen(filenm);
/*
get_seagen(fxy_surf,fxy_slopx,fxy_slopy,&time,&sl,filenm);
*/
/* floating point valued surface: */
fd_sea = open(sea_file,0);
for( i = 0; i < 512; i = i + 512 )
  read(fd_sea,&fxy_surf[i],4*512);

for( i = 0; i < 512; i = i + 512 )
  read(fd_sea,&fxy_slopx[i],4*512);

for( i = 0; i < 512; i = i + 512 )
  read(fd_sea,&fxy_slopy[i],4*512);


/* set min, max and facet length */
HITEMAX = 0.0;
HITEMIN = 0.0;
for( i = 0; i < 512*512; i++ )
{
  if( fxy_surf[i] > HITEMAX ) HITEMAX = fxy_surf[i];
  if( fxy_surf[i] < HITEMIN ) HITEMIN = fxy_surf[i];
}

FACETLEN = (HITEMAX - HITEMIN) / 256;
if( time < 20 )
{
  time_refl = 0.5 + time/60.0;
  time_trns = 1.0 - time_refl;
  time_radius = 50 * FACETLEN;
  time_light = 0.0;
  balls[0].x = 0.0;
  balls[0].y = 0.0;
  balls[0].z = HITEMIN + time/30.0*(HITEMAX-HITEMIN + time_radius);
} 
else if( time < 40 )
{ 
  time_light = (40-time)/20.0 * 255.0; 
  time_radius = (40-time)/20.0 * 50 * FACETLEN;
}
else if( time < 60 )
  balls[0].y = -(time-40)/20.0 * 3500.0;
  

/* a gray surface with NO specular reflection */
   refl[0] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[0]->spec_pwr = -1.0;
   refl[0]->red_coeff = 1.0;
   refl[0]->green_coeff = 1.0;
   refl[0]->blue_coeff = 1.0;
/* a gray surface with specular reflection */
   refl[1] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[1]->spec_pwr = 10.0;
   refl[1]->red_coeff = time_refl;
   refl[1]->green_coeff = time_refl;
   refl[1]->blue_coeff = time_refl;

/* place the celestial sphere in the center */
   balls[0].r = time_radius;
   balls[0].id = 0;
   balls[0].thick = 0.1;
   balls[0].refl = refl[1];
   balls[0].tran = (REFRACT *) malloc(sizeof(REFRACT));
   balls[0].tran->refract_indx = 1.5;
   balls[0].tran->red_coeff = time_trns;
   balls[0].tran->green_coeff = time_trns;
   balls[0].tran->blue_coeff = time_trns;
   balls[0].texture = NULL;

/* diffuse light is white, but not too bright */
diffuse.red = 191;
diffuse.green = 191;
diffuse.blue = 191;
/* init light source postion  */
lamp.x = balls[0].x;
lamp.y = balls[0].y;
lamp.z = balls[0].z;
lamp.r = balls[0].r;
/* lamp is white */
lamp.intens.red = time_light;
lamp.intens.green = time_light;
lamp.intens.blue = time_light;

return;
}

#include "ray.h"

SURFACE *hit_light(base,ray)
float base[3], ray[3];
{
  static SURFACE hit;
  float dist, distsqr, costh=0.0, tmp=0.0, lnear, lfar, rmag;
  float bvec[3], strike[3], normal[3];

#ifdef DEBUG
printf("hit_ball> checking for hit with ball # %d\n",ball->id);
#endif
/* find distance to ball center */
  distsqr = (lamp.x - base[0]) * (lamp.x - base[0]) + 
		(lamp.y - base[1]) * (lamp.y - base[1]) +
		(lamp.z - base[2]) * (lamp.z - base[2]);
  dist = sqrt( distsqr );

  if( dist < 0.999*lamp.r ) /* ray is inside ball ! */
    return( (SURFACE *) NULL );

  bvec[0] = (lamp.x - base[0]) / dist; 
  bvec[1] = (lamp.y - base[1]) / dist;
  bvec[2] = (lamp.z - base[2]) / dist;
/* angle between ray & direction of ball center */
  costh = bvec[0]*ray[0] + bvec[1]*ray[1] + bvec[2]*ray[2];
  if( costh <= 0.0 ) 
    return( (SURFACE *) NULL );
  tmp = lamp.r*lamp.r - distsqr * (1-costh*costh); 
  if( tmp <= 0.0 )
    return( (SURFACE *) NULL );
/* if we get here, we have a hit */
#ifdef DEBUG
printf("hit_light> hit light # %d\n",0);
#endif
  tmp = sqrt( tmp );
  lnear = dist * costh - tmp;
  strike[0] = base[0] + lnear * ray[0];
  strike[1] = base[1] + lnear * ray[1];
  strike[2] = base[2] + lnear * ray[2];
  normal[0] = strike[0] - lamp.x;
  normal[1] = strike[1] - lamp.y;
  normal[2] = strike[2] - lamp.z;
  rmag = sqrt( normal[0]*normal[0] + normal[1]*normal[1] + normal[2]*normal[2] );
  normal[0] = normal[0]/rmag;
  normal[1] = normal[1]/rmag;
  normal[2] = normal[2]/rmag;
    
  hit.type = descr_light;
  hit.id = 0;
/* use thickness value to modulate light brightness */
  hit.thick = 1.1*(-normal[0]*bvec[0] - normal[1]*bvec[1] - normal[2]*bvec[2]);
  hit.pos.x = strike[0];
  hit.pos.y = strike[1];
  hit.pos.z = strike[2];

  return( &hit );
}


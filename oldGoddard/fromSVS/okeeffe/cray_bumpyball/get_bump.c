#include "ray.h"

float get_bump(eps,normal,ball,lat_idx,lon_idx)
float eps, normal[3];
SPHERE *ball;
int *lat_idx, *lon_idx;
{
  float val, lat, lon;
  float rot_norm[3];
  
  if( rot_ang[0] > 0.0 )
  {
    rot_norm[0] = EQrot[0][0]*normal[0] + EQrot[0][1]*normal[1] +
			EQrot[0][2]*normal[2];
    rot_norm[1] = EQrot[1][0]*normal[0] + EQrot[1][1]*normal[1] +
			EQrot[1][2]*normal[2];
    rot_norm[2] = EQrot[2][0]*normal[0] + EQrot[2][1]*normal[1] +
			EQrot[2][2]*normal[2];
    lat = acos( rot_norm[2] ); /* 0 to PI */
    if( rot_norm[0] != 0.0 )
      lon = atan2(rot_norm[0], rot_norm[1]) + 3.1415926; /* 0 to 2PI */
    else
      lon = 0.0;
  }
  else
  {
    lat = acos( normal[2] ); /* 0 to PI */
    if( normal[0] != 0.0 )
      lon = atan2(normal[0], normal[1]) + 3.1415926; /* 0 to 2PI */
    else
      lon = 0.0;
  }

/* note that lat = lon = 0 in our world map is coord [255][512] */
  *lat_idx = 255 + 256*(lat - 3.1415926/2) / (3.1415926/2);
  *lon_idx = 512 + 512*(lon - 3.1415926) / 3.1415926;
/* this needs to be reversed: */
  *lon_idx = 1023 - (*lon_idx);
      
  val = (1 + eps*world_map[*lat_idx][*lon_idx]/255.0) * ball->r;

  return( val );
}

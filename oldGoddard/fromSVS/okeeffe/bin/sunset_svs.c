#include "gl.h"
#include "device.h"

/*
A way cool background that looks like the twilight sky.
Start it up in user.ps

Howard Look
howardl@sgi.com
*/

main()
{
	int gid;
	
	imakebackground() ;

	gid = winopen("") ;
/*        drawmode(OVERDRAW); */
	RGBmode();
	gconfig();
	shademodel(GOURAUD);
	
	ortho2(0.0, 1.0, 0.0, 1.0) ;

	qenter(REDRAW,gid);

	while (1)
	{
		short val ;
		long dev = qread(&val) ;
		if (dev == REDRAW)
		{
		  draw_background();
		  system("/usr/people/xrhon/bin/svs_rgb.ps");
		}
	}
}

#define Y1 0.0
#define Y2 .2

draw_background()
{
    int i,j;
	static int orange[] = {255,72,0};
	static int blueish[] = {0,110,189};
	static int black[] = {0,0,0};
	static int red[] = {255,0,0};
	float v1[2],v2[2],v3[2],v4[2];

	v1[0] = v4[0] = 0.0;
	v2[0] = v3[0] = 1.0;

	v1[1] = v2[1] = 0.0;
	v3[1] = v4[1] = Y1;
	bgnpolygon();
		c3i(red);
		v2f(v1);
		v2f(v2);
		c3i(orange);
		v2f(v3);
		v2f(v4);
	endpolygon();

	v1[1] = v2[1] = Y2;
	bgnpolygon();
		c3i(orange);
		v2f(v4);
		v2f(v3);
		c3i(blueish);
		v2f(v2);
		v2f(v1);
	endpolygon();

	v3[1] = v4[1] = 1.0;
	bgnpolygon();
		c3i(blueish);
		v2f(v1);
		v2f(v2);
		c3i(black);
		v2f(v3);
		v2f(v4);
	endpolygon();

	cpack(0xFFFFFF);

    for (j=0; j<10; j++)
    {
        bgnpoint();
        for (i=0; i<256; i++)
        {
			float x[2];
			x[0] = ((float)rand())/32767.0;
			x[1] = ((float)rand())/32767.0;
			v2f(x);
        }
        endpoint();
    }

    for (j =0; j<200; j++)
    {
		float x[2],r;
		x[0] = ((float)rand())/32767.0;
		x[1] = ((float)rand())/32767.0;
		r = ((float)rand())/32767.0/500.0;
		circf(x[0],x[1],r);
    }
}



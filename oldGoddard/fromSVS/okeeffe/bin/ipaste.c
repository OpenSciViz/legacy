/*
 *	ipaste - 
 *		Display an image on the iris screen.
 *
 *			Paul Haeberli - 1984	
 *
 */
#include "image.h"
#include "gl.h"
#include "device.h"
#include "port.h"
#include "dispimg.h"

#define XBORDER		8
#define TOPBORDER	8
#define BOTBORDER	40

typedef struct PASTEIMAGE {
    int xsize, ysize, zsize;
    int xorg, yorg;
    int sxmode, dpp;
    int xframe, yframe;
    int xdraw, ydraw;
    DISPIMAGE *di;
} PASTEIMAGE;

PASTEIMAGE *pi;
PASTEIMAGE *readit();

int drawit();
int yflip= 1, xflip= 1, zoom= 1;

main(argc,argv)
int argc;
char **argv;
{
    register int i;
    short val;
    int wx, wy, preforg, sxmode;

    if( argc<2 ) {
	fprintf(stderr,"usage: ipaste inimage [-sx] [-n] [-o xorg yorg]\n");
	exit(1);
    } 
    sxmode = 0;
    for(i=1; i<argc; i++) {
	if( !strcmp(argv[i],"-yflip") || !strcmp(argv[i],"-y") ) 
	    yflip = -1;
	if( !strcmp(argv[i],"-xflip") || !strcmp(argv[i],"-x") ) 
	    xflip = -1;
	if( !strcmp(argv[i],"-zoom") || !strcmp(argv[i],"-z") )
	  zoom = atoi(argv[i+1]);
	if(strcmp(argv[i],"-sx") == 0)
	    sxmode++;
	if(strcmp(argv[i],"-n") == 0)
	    noborder();
	if(strcmp(argv[i],"-o") == 0) {
	    i++;
	    wx = atoi(argv[i]);
	    i++;
	    wy = atoi(argv[i]);
	    preforg = 1;
	}
    }
    pi = readit(argv[1],sxmode,wx,wy,preforg);
    drawfunc(drawit);
}

PASTEIMAGE *readit(filename,sxmode,wx,wy,preforg)
char *filename;
int sxmode, wx, wy, preforg;
{
    register IMAGE *image;
    register PASTEIMAGE *pi;
    register int y;
    register short *srow;
    unsigned char *rrow, *grow, *brow;

/* allocate the image struct, and open the input file */
    pi = (PASTEIMAGE *)malloc(sizeof(PASTEIMAGE));
    if( (image=iopen(filename,"r")) == NULL ) {
	fprintf(stderr,"paste: can't open input file %s\n",filename);
	exit(1);
    }

/* calculate the window size */
    pi->xsize = zoom * image->xsize;
    pi->ysize = zoom * image->ysize;
    if(sxmode) {
	pi->sxmode = 1;
	pi->xframe = pi->xsize+XBORDER+XBORDER;
	pi->yframe = pi->ysize+BOTBORDER+TOPBORDER;
	if(pi->xframe>XMAXSCREEN+1)
	    pi->xframe = XMAXSCREEN+1;
	if(pi->yframe>YMAXSCREEN+1)
	    pi->yframe = YMAXSCREEN+1;
 	pi->xdraw = pi->xframe-XBORDER-XBORDER;
 	pi->ydraw = pi->yframe-BOTBORDER-TOPBORDER;
	pi->xorg = XBORDER;
	pi->yorg = BOTBORDER;
	noborder();
    } else {
	pi->sxmode = 0;
	pi->xframe = pi->xsize;
	pi->yframe = pi->ysize;
	if(pi->xframe>XMAXSCREEN+1)
	    pi->xframe = XMAXSCREEN+1;
	if(pi->yframe>YMAXSCREEN+1)
	    pi->yframe = YMAXSCREEN+1;
 	pi->xdraw = pi->xframe;
 	pi->ydraw = pi->yframe;
	pi->xorg = 0;
	pi->yorg = 0;
    }

/* open the window */
    if(preforg) {
	prefposition(wx,wx+pi->xframe-1,wy,wy+pi->yframe-1);
	winopen("ipaste");
	wintitle(filename);
	prefsize(pi->xframe,pi->yframe);
	winconstraints();
    } else {
	prefsize(pi->xframe,pi->yframe);
	winopen("ipaste");
	wintitle(filename);
    }

/* set the display mode for the image */

    setimagemode(image);
    rgb(0.5,0.5,0.5);
    clear();
    makeframe(pi);
    pi->di = makedisprgn(image,0,pi->xdraw-1,0,pi->ydraw-1,1,pi->xorg,pi->yorg);
    iclose(image);
    return pi;
}

drawit()
{
    makeframe(pi);
    drawimage(pi->di,pi->xorg,pi->yorg);
}

makeframe(pi)
PASTEIMAGE *pi;
{
    reshapeviewport();
    viewport(0,pi->xframe-1,0,pi->yframe-1);
    ortho2(-0.5,pi->xframe-0.5,-0.5,pi->yframe-0.5);
    if(pi->sxmode) {
	grey(1.0);
	rectfi(0,0,pi->xframe-1,BOTBORDER-1);
	rectfi(0,0,XBORDER-1,pi->yframe-1);
	rectfi(pi->xframe-XBORDER,0,pi->xframe-1,pi->yframe-1);
	rectfi(0,pi->yframe-TOPBORDER,pi->xframe-1,pi->yframe-1);
	grey(0.0);
	recti(XBORDER-1,BOTBORDER-1,pi->xframe-XBORDER,pi->yframe-TOPBORDER);
	grey(0.0);
        recti(0,0,pi->xframe-1,pi->yframe-1);
    }
}

#include <stdio.h>

main(argc,argv)
int argc;
char **argv;
{
  FILE *fp;
  char cmd[80];
  int n=10;
  char buf[10];
  int sz;

  strcpy(cmd,"ls -l ");
  strcat(cmd,argv[1]);
  strcat(cmd," | awk '{print $5}'");
  
  fp = popen(cmd,"r");

  fgets(buf, n, fp);

  sz=atoi(buf);

  printf("sz = %d",sz);

  fclose(fp);
}

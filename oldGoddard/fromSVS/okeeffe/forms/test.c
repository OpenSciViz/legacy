/* This demo show the different boxtypes */

#include "forms.h"

FL_FORM *form;
FL_OBJECT *obj[11], *exitob;

void change(FL_OBJECT *ob, long arg)
{
  int i;
  for (i=0; i<11; i++) fl_set_object_boxtype(obj[i], (int) arg);
  fl_redraw_form(form);
}

void create_the_forms()
{
  FL_OBJECT *ob;
  form = fl_bgn_form(FL_UP_BOX,500.0,300.0);

  fl_add_box(FL_DOWN_BOX,10.0,10.0,480.0,220.0,"");
  obj[0] = fl_add_box(FL_UP_BOX,50.0,20.0,110.0,70.0,"Box");
  obj[1] = fl_add_text(FL_NORMAL_TEXT,50.0,110.0,110.0,30.0,"Text");
  obj[2] = fl_add_input(FL_NORMAL_INPUT,50.0,160.0,110.0,30.0,"Input");
  obj[3] = fl_add_button(FL_PUSH_BUTTON,175.0,20.0,80.0,40.0,"Button");
  obj[4] = fl_add_lightbutton(FL_PUSH_BUTTON,175.0,70.0,80.0,40.0,"Button");
  obj[5] = fl_add_roundbutton(FL_PUSH_BUTTON,175.0,120.0,80.0,40.0,"Button");
  obj[6] = fl_add_slider(FL_VERT_SLIDER,270.0,40.0,30.0,160.0,"Slider");
  obj[7] = fl_add_dial(FL_LINE_DIAL,315.0,40.0,70.0,70.0,"Dial");
  obj[8] = fl_add_clock(FL_ROUND_CLOCK,315.0,130.0,70.0,70.0,"Clock");
  obj[9] = fl_add_positioner(FL_NORMAL_POSITIONER,400.0,40.0,70.0,70.0,"Positioner");
  obj[10] = fl_add_menu(FL_PUSH_MENU,400.0,150.0,70.0,40.0,"Menu");
    fl_set_menu(obj[10],"item 1|item 2|item 3");

  fl_add_box(FL_DOWN_BOX,10.0,230.0,480.0,60.0,"");
  ob = fl_add_button(FL_RADIO_BUTTON,20.0+55.0*0,245.0,55.0,30.0,"no_box");
    fl_set_call_back(ob,change,FL_NO_BOX);
    fl_set_object_lsize(ob,FL_SMALL_FONT);
  ob = fl_add_button(FL_RADIO_BUTTON,20.0+55.0*1,245.0,55.0,30.0,"up_box");
    fl_set_call_back(ob,change,FL_UP_BOX);
    fl_set_object_lsize(ob,FL_SMALL_FONT);
  ob = fl_add_button(FL_RADIO_BUTTON,20.0+55.0*2,245.0,55.0,30.0,"down_box");
    fl_set_call_back(ob,change,FL_DOWN_BOX);
    fl_set_object_lsize(ob,FL_SMALL_FONT);
  ob = fl_add_button(FL_RADIO_BUTTON,20.0+55.0*3,245.0,55.0,30.0,"flat_box");
    fl_set_call_back(ob,change,FL_FLAT_BOX);
    fl_set_object_lsize(ob,FL_SMALL_FONT);
  ob = fl_add_button(FL_RADIO_BUTTON,20.0+55.0*4,245.0,55.0,30.0,"border_box");
    fl_set_call_back(ob,change,FL_BORDER_BOX);
    fl_set_object_lsize(ob,FL_SMALL_FONT);
  ob = fl_add_button(FL_RADIO_BUTTON,20.0+55.0*5,245.0,55.0,30.0,"shadow_box");
    fl_set_call_back(ob,change,FL_SHADOW_BOX);
    fl_set_object_lsize(ob,FL_SMALL_FONT);
  ob = fl_add_button(FL_RADIO_BUTTON,20.0+55.0*6,245.0,55.0,30.0,"frame_box");
    fl_set_call_back(ob,change,FL_FRAME_BOX);
    fl_set_object_lsize(ob,FL_SMALL_FONT);

  exitob = fl_add_button(FL_NORMAL_BUTTON,425.0,245.0,55.0,30.0,"Exit");
  fl_end_form();
}

main()
{
  FL_OBJECT *ob;
  create_the_forms();
  fl_show_form(form,FL_PLACE_ASPECT,TRUE,"Box types");
  do ob = fl_do_forms(); while (ob != exitob);
  fl_hide_form(form);
}

#include "mpl.h"
#include "math.h"

typedef struct { float r, i; } fcomplex;

#define DEBUG_RCMUL

plural fcomplex *Complex(a,b)
plural float a;
plural float b;
{
  static plural fcomplex c;
  
  c.r = a;
  c.i = b;
#ifdef DEBUG
  winprtf_(&a,0,0,4,4," %g ",1);
  winprtf_(&b,0,0,4,4," %g ",1);
  winprtf_(&c.r,0,0,4,4," %g ",1);
  winprtf_(&c.i,0,0,4,4," %g ",1);
#endif
  return( &c );
}

plural fcomplex *RCmul(x,a)
plural float x;
plural fcomplex *a;
{	
  static plural fcomplex c;
	
  c.r = x*a->r;
  c.i = x*a->i;

#ifdef DEBUG_
  winprtf_(&x,0,0,4,4," %g ",1);
  winprtf_(&a->r,0,0,4,4," %g ",1);
  winprtf_(&a->i,0,0,4,4," %g ",1);
  winprtf_(&c.r,0,0,4,4," %g ",1);
  winprtf_(&c.i,0,0,4,4," %g ",1);
#endif

  return( &c );
}

plural fcomplex *Cexp(c)
plural fcomplex *c;
{
  static plural fcomplex val;
  plural float mag;

  mag = p_exp(c->r);
  val.r = mag * p_cos(c->i);
  val.i = mag * p_sin(c->i);

  return( &val );
}

plural fcomplex *Cdiv(a,b)
plural fcomplex *a, *b;
{	
 static plural fcomplex c;
	 plural float r;
 plural float den;
	
  if( p_fabs(b->r) >= p_fabs(b->i) ) 
  {
		    r = b->i / b->r;
		    den = b->r + r*b->i;
		    c.r = (a->r + r*a->i) / den;
		    c.i = (a->i - r*a->r) / den;
	  } 
  else 
  {
		    r = b->r / b->i;
		    den = b->i + r*b->r;
    		c.r = (a->r*r + a->i) / den;
		    c.i = (a->i*r - a->r) / den;
	  }
	  return( &c );
}



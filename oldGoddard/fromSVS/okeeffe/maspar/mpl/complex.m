#include "mpl.h"
#include "math.h"

typedef struct { double r, i; } dcomplex;

#define DEBUG_

plural dcomplex *Complex(a,b)
plural double a;
plural double b;
{
  static plural dcomplex c;
  
  c.r = a;
  c.i = b;
#ifdef DEBUG_PACKER
  winprtd_(&a,0,0,4,4," %g ",1);
  winprtd_(&b,0,0,4,4," %g ",1);
  winprtd_(&c.r,0,0,4,4," %g ",1);
  winprtd_(&c.i,0,0,4,4," %g ",1);
#endif
  return( &c );
}

plural dcomplex *RCmul(x,a)
plural double x;
plural dcomplex *a;
{	
  static plural dcomplex c;
	
  c.r = x*a->r;
  c.i = x*a->i;
	
  return( &c );
}

plural dcomplex *Cexp(c)
plural dcomplex *c;
{
  static plural dcomplex val;
  plural double mag;

  mag = p_exp(c->r);
  val.r = mag * p_cos(c->i);
  val.i = mag * p_sin(c->i);

  return( &val );
}

plural dcomplex *Cdiv(a,b)
plural dcomplex *a, *b;
{	
 static plural dcomplex c;
	 plural double r;
 plural double den;
	
  if( p_fabs(b->r) >= p_fabs(b->i) ) 
  {
		    r = b->i / b->r;
		    den = b->r + r*b->i;
		    c.r = (a->r + r*a->i) / den;
		    c.i = (a->i - r*a->r) / den;
	  } 
  else 
  {
		    r = b->r / b->i;
		    den = b->i + r*b->r;
    		c.r = (a->r*r + a->i) / den;
		    c.i = (a->i*r - a->r) / den;
	  }
	  return( &c );
}



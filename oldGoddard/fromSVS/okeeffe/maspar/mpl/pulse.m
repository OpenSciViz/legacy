#include "scatter.h"

void pulse()
{
 float time=0.0;
 fcomplex *signal, *scatter();
 int i=0;

  while( i++ < DIGITIZE )
  {
    time = (i - DIGITIZE/2) * DEL_TIME;
    signal = scatter(time);
    pulse_array[i].r = signal->r;
    pulse_array[i].i = signal->i;
    if( !(i % (DIGITIZE/10)) ) 
      fprintf(stderr,"pulse> finished element %d...\n",i);
  }
}

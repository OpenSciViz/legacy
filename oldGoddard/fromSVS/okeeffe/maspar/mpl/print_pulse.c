#include <stdio.h>
typedef struct { float r, i; } fcomplex;

main()
{
  int i=0, fd=0;
  fcomplex tmp;

  while( i++ < 128*64 )
  {
    read(fd,&tmp,8); 
    fprintf(stdout," %d %f %f\n",i,tmp.r,tmp.i);
  }
}
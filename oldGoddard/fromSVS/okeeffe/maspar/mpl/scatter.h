#define N 128 /* number of scattering cells */
#define M 64
#include "stdio.h"
#include "mpl.h"
#include "reduce.h"
#include "math.h"

#define H 10000.0 /* altitude of sensor ~ 10,000 meters */
#define PI 3.1415926
#define C 300000000.0 /* speed of light in m/s */
#define PULSE_LN 0.000001 /* 1 micro-sec pulse length */
#define PRF 1000.0 /* 1 KHz */
#define INTERPULSE_LN 1.0/PRF /* 1 milli-sec between each pulse */
#define GRND_SPD 100.0 /* 100 m/sec */
#define P_BAND 450000000.0 /* 450.0 MHz */
#define BAND_WIDTH 40000000.0 /* 40 MHz */ 
#define MAX_PULSE 1024 /*  seconds worth using 1 milli-sec "inter-pulse period */
#define DIGITIZE 1024
#define SWATH_WIDTH C*PULSE_LN/1.4142
#define DEL_TIME PULSE_LN/DIGITIZE
#define X0 0
#define Y0 -H
#define Z0 H

#define NPROC 128*64

/* complex number & functions: */
typedef struct { double r, i; } dcomplex;  /* straight out of K&R pg 200 */
typedef struct { float r, i; } fcomplex;
plural fcomplex *Complex();
plural fcomplex *RCmul();
plural fcomplex *Cexp();
plural fcomplex *Cdiv();

/* globals */
#ifdef MAIN
#define global 
#else
#define global extern
#endif

/* global singulars */
global float w0;
global float Freq_Rate;
global float vel[3];
global fcomplex pulse_array[1024];

/* global plurals */
global plural float x, y, sc_amp;




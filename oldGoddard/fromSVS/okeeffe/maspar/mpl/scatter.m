#include "scatter.h"

/* PI, C, P_BAND, GRND_SPD, H, X0, Y0, Z0, and any other constants */
/* each scatterer "sees" the chirp at a different point in it's frequency modulation,
   this difference amounts to the difference in time-delays. use t0 at a nominal
   position, say the point of closest approach to the target and caclulate the
   difference in time delays */ 

#define DEBUG_RCMUL

fcomplex *scatter(t)
float t;
{
 plural float range, t0, t_delay, dot, w_chrp, w_prm, pointing[3];
 plural float pzero= 0.0, pone = 1.0, ptwo= 2.0;
 plural float x0= X0, y0= Y0, z0= Z0, c= C, h= H, pi= PI;
 plural fcomplex *signal, *tmp;
 static fcomplex tot_signal;

/* at the point of closest approach, the aircraft is at pos=(0,-H,H): */ 
  t0 = 2.0 * fp_sqrt(x*x + (y - h)*(y - h) + h*h) / C;

  pointing[0] = x - (x0 + vel[0]*t);
  pointing[1] = y - (y0 + vel[1]*t);
  pointing[2] = -(z0 + vel[2]*t);  /* all scatteres are at z=0 for now */

  range = fp_sqrt( pointing[0]*pointing[0] + pointing[1]*pointing[1] + pointing[2]*pointing[2] );
  dot = pointing[0]*vel[0] + pointing[1]*vel[1] + pointing[2]*vel[2];
  dot = dot / range;

/* for evaluation of the retarded/time-delayed doppler shifted pulse/field from scatterers */
  t_delay = t + (t0 - ptwo*range/c);  

/* a chirp that goes from low to high: */
  w_chrp = pi * Freq_Rate * t_delay; /* + w0 */

  w_prm = w_chrp * (pone + dot/c) * (pone + dot/c);  /* galilean doppler shift */

/* re-use the dot variable: */
  dot = w_prm * t_delay;

  tmp = Complex( pzero, dot );

#ifdef DEBUG

  winprtf_(&w_prm,0,0,4,1," %g ",1);
  winprtf_(&t_delay,0,0,4,1," %g ",1);
  winprtf_(&dot,0,0,4,1," %g ",1);
  winprtf_(&tmp->r,0,0,4,1," %g ",1);
  winprtf_(&tmp->i,0,0,4,1," %g ",1);

#endif

  tmp = Cexp( tmp );

#ifdef DEBUG_

  winprtf_(&tmp->r,0,0,4,4," %g ",1);
  winprtf_(&tmp->i,0,0,4,4," %g ",1);
  winprtf_(&sc_amp,0,0,4,4," %g ",1);

#endif

  signal = RCmul( sc_amp, tmp );

#ifdef DEBUG_

  winprtf_(&signal->r,0,0,4,4," %g ",1);
  winprtf_(&signal->i,0,0,4,4," %g ",1);

#endif

/* and mix with local oscillator to remove the carier w0: 
  signal = Cdiv( signal, Cexp( Complex( pzero, w0*t_delay ) ) );  
*/

/* this library routine sums all the plural scattering amplitudes into the net field at the sensor */
  tot_signal.r = reduceAddf(signal->r);
  tot_signal.i = reduceAddf(signal->i);

  return( &tot_signal );
} 
#define MAIN
#include "scatter.h"

main(argc, argv)
int argc; char **argv;
{
 float x_init[NPROC], y_init[NPROC], sc_amp_init[NPROC];
 float rmin=9999999.0, rmax= -9999999.0;
 float imin=9999999.0, imax= -9999999.0;
 int i, j, fd;
 unsigned char outbuff[2*DIGITIZE];
 void pulse();

/* a test initialization */
  w0 = P_BAND;
  Freq_Rate = BAND_WIDTH/PULSE_LN;
  vel[0] = GRND_SPD; vel[1] = 0.0; vel[2] = 0.0;

  for( j = 0; j < nyproc; j++ ) /* nyproc = 64 */
   for( i = 0; i < nxproc; i++ ) /* nxproc = 128 */
   {
     x_init[j*nxproc + i] = i * SWATH_WIDTH/nxproc;
     y_init[j*nxproc + i] = j * SWATH_WIDTH/nyproc;
     sc_amp_init[j*nxproc + i] = 1.0;
   }
/* init all the parallel processors */
  for( j = 0; j < nyproc; j++ ) /* nyproc = 64 */
   for( i = 0; i < nxproc; i++ ) /* nxproc = 128 */
   {
     proc[j][i].x = x_init[j*nxproc + i];
     proc[j][i].y = y_init[j*nxproc + i];
     proc[j][i].sc_amp = sc_amp_init[j*nyproc + i];
   }

  fprintf(stderr,"main> finished initialization...\n");

  pulse();

  fd = creat("/scratch2/xrhon/a_pulse.8bit",0644);
/*
  write(fd, pulse_array, sizeof(fcomplex)*DIGITIZE);
*/
  for( i = 0; i < DIGITIZE; i++ )
  {
    if( pulse_array[i].r > rmax ) rmax = pulse_array[i].r;
    if( pulse_array[i].r < rmin ) rmin = pulse_array[i].r;
    if( pulse_array[i].i > imax ) imax = pulse_array[i].i;
    if( pulse_array[i].i < imin ) imin = pulse_array[i].i;
  }
  for( i = 0; i < DIGITIZE; i += 2 )
  {
    outbuff[i] = 255 * (pulse_array[i/2].r - rmin) / (rmax - rmin);
    outbuff[i+1] = 255 * (pulse_array[i/2].i - imin) / (imax - imin);
  }
  write(fd, outbuff, 2*DIGITIZE);
  close( fd );
}



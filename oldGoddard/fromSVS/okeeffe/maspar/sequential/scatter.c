#include "scatter.h"

/* PI, C, P_BAND, GRND_SPD, H, X0, Y0, Z0, and any other constants */
/* each scatterer "sees" the chirp at a different point in it's frequency modulation,
   this difference amounts to the difference in time-delays. use t0 at a nominal
   position, say the point of closest approach to the target and caclulate the
   difference in time delays */ 

scatter(t)
float t;
{
 plural float range, t0, t_delay, dot, w_chrp, w_prm, pointing[3];
 plural fcomplex *signal;

/* at the point of closest approach, the aircraft is at pos=(0,-H,H): */ 
  t0 = 2.0*sqrt(x*x + (y - H)*(y - H) + H*H) / C;

  pointing[0] = x - (X0 + vel[0]*t);
  pointing[1] = y - (Y0 + vel[1]*t);
  pointing[2] = -(Z0 + vel[2]*t);  /* all scatteres are at z=0 for now */
  range = sqrt( pointing[0]*pointing[0] + pointing[1]*pointing[1] + pointing[2]*pointing[2] );
  dot = pointing[0]*vel[0] + pointing[1]*vel[1] + pointing[2]*vel[2];
  dot = dot / range;

/* for evaluation of the retarded/time-delayed doppler shifted pulse/field from scatterers */
  t_delay = t + (t0 - 2*range/C);  

/* a chirp that goes from low to high: */
  w_chrp = PI*Freq_Rate*t_delay; /* + w0; */

  w_prm = w_chrp * (1.0 + dot/C) * (1.0 + dot/C);  /* galilean doppler shift */

  signal = RCmul(sc_amp, Cexp( Complex( 0.0, w_prm*t_delay ) ) );  
/*
  signal = Cdiv(signal, Cexp( Complex( 0.0, w0*t_delay ) ) );
*/

  if( x < SWATH_WIDTH )
    x = SWATH_WIDTH/nxproc + x;
  else
  {
    if( y >= SWATH_WIDTH )
      return;
    x = 0.0;
    y = SWATH_WIDTH/nyproc + y;
  }

/* accumulate into static buffer */
  tot_signal.r = tot_signal.r + signal->r; 
  tot_signal.i = tot_signal.i + signal->i;

  scatter(t);
} 

#define MAIN
#include "scatter.h"

main(argc, argv)
int argc; char **argv;
{
 float x_init[NPROC], y_init[NPROC], sc_amp_init[NPROC];
 int i, j, fd;
 void pulse();
  
  w0 = P_BAND;
  Freq_Rate = BAND_WIDTH/PULSE_LN;
  vel[0] = GRND_SPD; vel[1] = 0.0; vel[2] = 0.0;

/*
  for( i = 0; i < nxproc; i++ )
   for( j = 0; j < nyproc; j++ ) 
   {
     x[i*nyproc + j] = i;
     y[i*nyproc + j] = j;
     sc_amp[i*nyproc + j] = 1.0;
   }

  for( i = 0; i < nxproc; i++ ) 
   for( j = 0; j < nyproc; j++ )
   {
     proc[j][i].x = x_init[i*nyproc + j];
     proc[j][i].y = y_init[i*nyproc + j];
     proc[j][i].sc_amp = sc_amp_init[i*nyproc + j];
   }
*/
  x = y = 0.0;
  sc_amp = 1.0;

  fprintf(stderr,"main> finished initialization...\n");
  pulse(0.0);

  fd = creat("a_pulse.dat",0644);
  write(fd,pulse_array,8*1024);
  close( fd );

}

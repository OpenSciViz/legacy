#include <math.h>

typedef struct float_complex { float r; float i; } fcomplex;

fcomplex *Complex(r,i)
float r, i;
{
 static fcomplex c;

  c.r = r;
  c.i = i;
  return( &c );
}

fcomplex *RCmul(x,a)
float x;
fcomplex *a;
{	
 static fcomplex c;
	
  c.r = x*a->r;
  c.i = x*a->i;
	
  return( &c );
}

fcomplex *Cexp(c)
fcomplex *c;
{
 static fcomplex val;
 float mag;

  mag = exp(c->r);
  val.r = mag * cos(c->i);
  val.i = mag * sin(c->i);

  return( &val );
}

fcomplex *Cadd(c1,c2)
fcomplex *c1, *c2;
{
 static fcomplex val;

  val.r = c1->r + c2->r;
  val.i = c1->i + c2->i;

  return( &val );
}

fcomplex *Cdiv(a,b)
fcomplex *a, *b;
{	
 static fcomplex c;
 float r;
 float den;
	
  if( fabs(b->r) >= fabs(b->i) ) 
  {
    r = b->i / b->r;
    den = b->r + r*b->i;
    c.r = (a->r + r*a->i) / den;
    c.i = (a->i - r*a->r) / den;
  } 
  else 
  {
    r = b->r / b->i;
    den = b->i + r*b->r;
    c.r = (a->r*r + a->i) / den;
    c.i = (a->i*r - a->r) / den;
  }
  return( &c );
}

#include "scatter.h"

void pulse(float t_offset)
{
 float time;
 int i=0;

  while( i++ < DIGITIZE )
  {
    x = 0.0; y = 0.0;
    tot_signal.r = 0.0;
    tot_signal.i = 0.0;
    time = t_offset + (i - DIGITIZE/2) * DEL_TIME;
    scatter(time);
    pulse_array[i].r = tot_signal.r;
    pulse_array[i].i = tot_signal.i;
    if( !(i % (DIGITIZE/10)) )
      fprintf(stderr,"pulse> finished element %d  (%f, %f)...\n",i,
		tot_signal.r,tot_signal.i);
    
  }
}

/*****************************************************************************/
/*                                                                           */
/*                                lookat.c                                   */
/*                                                                           */
/*  This routine performs the actual steps necessary to allow the viewing    */
/*  of an image.                                                             */ 
/*                                                                           */
/*  Dave Pape                                                                */
/*  NASA/GSFC                                                                */
/*  May 1989                                                                 */
/*                                                                           */
/*  Eric Weisstein                                                           */
/*  USRA Summer Graduate Student Program                                     */
/*  NASA/Goddard Space Flight Center                                         */
/*  August 1990                                                              */
/*                                                                           */
/*****************************************************************************/

#include <gl.h>
#include <math.h>

#define PI 3.14159265

static Matrix identity = { 1.0, 0.0, 0.0, 0.0,
			   0.0, 1.0, 0.0, 0.0,
			   0.0, 0.0, 1.0, 0.0,
			   0.0, 0.0, 0.0, 1.0 };

new_lookat(float vx,float vy,float vz,float tx,float ty,float tz,int twist)
{
 float dx,dy,dz,dist,az,el;

 dx=vx-tx;
 dy=vy-ty;
 dz=vz-tz;
 if (dx!=0.0) az=180.0/PI*atan(dy/dx)+90.0;
 else if (dy>0.0) az=180.0;
 else az=0.0;
 if (dx<0.0) az-=180.0;
 if ((dx*dx+dy*dy)==0.0) el=0.0;
 else el=90.0-180.0/PI*atan(dz/sqrt(dx*dx+dy*dy));
 dist=sqrt(dx*dx+dy*dy+dz*dz);
 loadmatrix(identity);
 polarview(dist,(int)(az*10.0),(int)(el*10.0),twist);
 translate(-tx,-ty,-tz);
}

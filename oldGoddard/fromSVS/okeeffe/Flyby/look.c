/*****************************************************************************/
/*                                                                           */
/*                                  look.c                                   */
/*                                                                           */
/*  These routines allow the user to manipulate and view flyby images.       */
/*  Available functions include zooming, translating, rotating, and          */
/*  stretching.  Colormaps may also be loaded, and images may be saved to    */
/*  disk for display with the drop program.                                  */ 
/*                                                                           */
/*  Ron Klasky                                                               */
/*  STX/NASA/GSFC                                                            */
/*  (original routines)                                                      */
/*                                                                           */
/*  Dave Pape                                                                */
/*  NASA/GSFC                                                                */
/*  May 1989 (modifications)                                                 */
/*                                                                           */
/*  Eric Weisstein                                                           */
/*  USRA Summer Graduate Student Program                                     */
/*  NASA/Goddard Space Flight Center                                         */
/*  August 1990                                                              */
/*                                                                           */
/*****************************************************************************/

#include "externs.h"
#include "colormap.h"
#include <device.h>
#include <malloc.h>
#include <sys/types.h>

#define SPIN      2
#define STRETCH   3
#define DRAW      5
#define QUIT      6
#define SAVE      7
#define SAVE_UPSIDEDOWN 11
#define RESET    13
#define CM_MODE  14
#define RGB_MODE 15
#define FBPARMS  17
#define MFLTPTH  18
#define MFLYBY   19
#define SAVEFILE 23
#define LIGHTS   25
#define CYCLEDATA 26

long menu;
extern Matrix rot_mat;
Matrix idmat={1.0,0.0,0.0,0.0,
                 0.0,1.0,0.0,0.0,
                 0.0,0.0,1.0,0.0,
                 0.0,0.0,0.0,1.0};

void create_menu(land_t *land)
{
 long colormapm,dumpm,modem,modulem;
 char str[250];
 int  i;

 qdevice(RIGHTMOUSE);
 qdevice(MIDDLEMOUSE);
 qdevice(LEFTMOUSE);

 strcpy(str,"display mode %t|   colormap %x14|   RGB %x15");
 if (colormap_mode) arrow(str,17);
 else arrow(str,34);
 modem=defpup(str);
 colormapm=colormap_menu();
 dumpm=defpup("Dump Options %t| save normal... %x7| save upsidedown... %x11");
 modulem=defpup(" flightpath %x18| flyby %x19| look");
 setpup(modulem,3,PUP_GREY); 
 strcpy(str,"Look Options %t| spin %x2| stretch %x3| render %x5|"
	" cycle data files %x26|"
	" display mode %m| colormap %m| dump %m| save %x23|");
 if (land->header->draw_mode == DM_LIGHT)
   strcat(str," lights (on) ... %x25|");
 else
   strcat(str," lights (off) ... %x25|");
 strcat(str," parameters... %x17| reset %x13| module %m%l| quit %x6");
 menu = defpup(str,modem,colormapm,dumpm,modulem);

 if (!rgb_allowed) setpup(modem,2,PUP_GREY);
 if (!colormap_mode) setpup(menu,6,PUP_GREY);
}

/*
int cycle_datafiles(land_t *land,int index)
{
 int num_land;
 ARROWCURSOR;
 if (!rgb_allowed) { doublebuffer(); gconfig(); }
 num_land = land->header->num_land;
 while (1)
   if (getbutton(LEFTMOUSE)) {
	index = (index + num_land - 1) % num_land;
	draw_quick(&land[index],TRUE);
	}
   else if (getbutton(MIDDLEMOUSE)) {
	index = (index + 1) % num_land;
	draw_quick(&land[index],TRUE);
	}
   else if (getbutton(RIGHTMOUSE)) break;
 qreset();
 qenter(RIGHTMOUSE,1);
 DEFAULTCURSOR;
 return(index);
}
*/

int cycle_datafiles(land_t *land,int index)
{
 int num_land,delta,dev;
 short val;
 ARROWCURSOR;
 if (!rgb_allowed) { doublebuffer(); gconfig(); }
 num_land = land->header->num_land;
 delta=0;
 qdevice(UPARROWKEY);
 qdevice(DOWNARROWKEY);
 qdevice(LEFTARROWKEY);
 qdevice(RIGHTARROWKEY);
 qdevice(SKEY);
 while (1) {
    index = (index + delta) % num_land;
    draw_quick(&land[index],TRUE);
    if (getbutton(RIGHTMOUSE)) break;
    while (qtest()) {
	dev = qread(&val);
	if (val) switch (dev) {
	   case UPARROWKEY: delta = 1; break;
	   case DOWNARROWKEY: delta = num_land - 1; break;
	   case LEFTARROWKEY: index = (index + num_land - 1) % num_land;
			      delta = 0;
			      break;
	   case RIGHTARROWKEY: index = (index + 1) % num_land;
			       delta = 0;
			       break;
	   case SKEY: delta = 0; break;
	   }
	}
    }
 unqdevice(UPARROWKEY);
 unqdevice(DOWNARROWKEY);
 unqdevice(LEFTARROWKEY);
 unqdevice(RIGHTARROWKEY); 
 unqdevice(SKEY);
 qreset();
 qenter(RIGHTMOUSE,1);
 DEFAULTCURSOR;
 return(index);
}

#include <sys/time.h>

draw_and_time(land)
land_t *land;
{
 struct timeval start_time,end_time;
 float delta_time;

  WATCHCURSOR;
  if (!rgb_allowed) {
    singlebuffer();
    gconfig();
    }
  gettimeofday(&start_time,NULL);
  draw_world(land,TRUE);
  gettimeofday(&end_time,NULL);
  delta_time=(float)(end_time.tv_sec-start_time.tv_sec)
		+ (float)(end_time.tv_usec - start_time.tv_usec)/1000000.0;
  fprintf(stderr,"%dx%d elements [%d] rendered in %f seconds:",
		land->header->xres,land->header->yres,
		land->header->xres*land->header->yres,delta_time);
  fprintf(stderr," %.1f elements/s\n",
		(float)(land->header->xres*land->header->yres)/delta_time);
  qreset();
  DEFAULTCURSOR;
}

static land_t *reread_data(land_t *land)
{
 int old_xres = land->header->xres;
 int old_z_scale = land->header->z_scale;
 header_t *header = land->header;
 int i;
 WATCHCURSOR;
 for (i=0; i < header->num_land; i++) {
	if (i < header->z_count) free(land[i].zmap);
	if (i < header->tex_count) free(land[i].tmap);
	if (land[i].norm) free(land[i].norm);
	if (land[i].lores_norm) free(land[i].lores_norm);
	}
 free(land);
 land = read_datafiles(header);
 DEFAULTCURSOR;
 return(land);
}

int do_look(land)
land_t *land;
{
  char   str[80];
  FILE   *fp;
  short  val,menuval,dummy;
  int    land_index;

  wintitle("Look");
  create_menu(land);
  init_view();
  land_index=0;
  draw_quick(&land[land_index],TRUE);
  while (1) {
      val = qread(&dummy);
      switch(val) {
        case WINQUIT:
          gexit();
          exit(0);
          break;
        case REDRAW:
          draw_quick(&land[land_index],TRUE);
          qreset();
          break;
        case RIGHTMOUSE:
          menuval=dopup(menu);
          switch(menuval) {
            case SAVEFILE:
              write_header(land->header); 
              break;
            case FBPARMS:
              qdevice(WINQUIT);
              if (panel_dialog(1,land->header)) {
			land = reread_data(land);
			land_index = 0;
			}
              unqdevice(WINQUIT);
              winpop();
              create_menu(land);
              break;
            case LIGHTS:
		light_dialog(&land[land_index]);
		create_menu(land);
		draw_quick(&land[land_index],TRUE);
		break;
            case CM_MODE:
              colormap_mode=1;
              create_menu(land);
              cmode();
              gconfig();
              draw_quick(&land[land_index],TRUE);
	      break;
            case RGB_MODE:
 	      if(rgb_allowed) {
  	        colormap_mode=0;
                RGBmode();
                gconfig();
	        create_menu(land);
                draw_quick(&land[land_index],TRUE);
              }
	      break;
            case SPIN: spin(land,land_index,draw_quick,TRUE,draw_world_inc); break;
            case STRETCH: stretch(&land[land_index]); break;
            case RESET:
		   init_view();
                   draw_quick(&land[land_index],TRUE);
                   qreset();
                   break;
            case DRAW: draw_and_time(&land[land_index]); break;
	    case CYCLEDATA: land_index = cycle_datafiles(land,land_index);
			    break;
            case SAVE_UPSIDEDOWN:
loadmatrix(idmat);
rot(-2.5,'y');
multmatrix(rot_mat);
getmatrix(rot_mat);
draw_world(land,TRUE);
rgbdump("Flyby-image1");
loadmatrix(idmat);
rot(5.0,'y');
multmatrix(rot_mat);
getmatrix(rot_mat);
draw_world(land,TRUE);
rgbdump("Flyby-image2");
/*
                  getwd(str);
                  strcat(str,"/");
                  get_text(str,"Enter name of the upsidedown save file");
                  suffix(str,".imr");
                  if((fp=fopen(str,"w"))==NULL) {
                    printf("Cannot open %s for writing\n",str);
                    break;
                  }
                  readsource(SRC_FRONT);
                  dumpwindow_upsidedown(fp,1);
                  fclose(fp);
                  fprintf(stderr,"File %s saved to disk.\n",str); 
*/
                  break;
            case SAVE:
                  getwd(str);
                  strcat(str,"/");
                  get_text(str,"Enter name of the save file");
                  suffix(str,".img");
                  if((fp=fopen(str,"w"))==NULL) {
                    printf("Cannot open %s for writing\n",str);
                    break;
                  }
                  readsource(SRC_FRONT);
                  dumpwindow(fp,0); 
                  fclose(fp);
                  fprintf(stderr,"File %s saved to disk.\n",str); 
                  break;
	    case CM_RESET: reset_colormap(land[land_index].header); break;
	    case CM_BLUE: case CM_CYAN: case CM_GRAY: case CM_GRAY2:
	    case CM_GRAYI: case CM_GRAYP: case CM_GREEN: case CM_LILAC:
	    case CM_RAINBOW: case CM_RED: case CM_VIOLET: case CM_YELLOW:
	    case CM_OTHER: set_colormap(land[land_index].header,menuval);
			   break;
            case QUIT: reset_colormap(land[land_index].header); exit();
            case MFLTPTH: return(FLIGHTPATH);
            case MFLYBY: return(FLYBY);
            }
      }
  }
}

/*****************************************************************************/
/*                                                                           */
/*                                  main.c                                   */
/*                                                                           */
/*  These routines initialize relevant parameters, enabling the user to      */
/*  interactively switch between the flightpath, flyby, and look modules.    */
/*  Before the graphics window is configured, the program determines how     */
/*  many bitplanes are available on the current system.  It would like to    */
/*  use doublebuffered RGB mode, but will adjust its behavior as necessary.  */
/*  Unlike previous versions, this version of flyby is completely menu       */
/*  driven to greatly simplify ease-of use.                                  */
/*                                                                           */
/*  The routines required for the complete flyby program are main.c,         */
/*  dialog.c, draw.c, dump.c, externs.h, flightpath.c, flyby.c, input.c,     */
/*  lights.c, look.c, lookat.c, misc.c, norms.c, panel.h, read.c, and        */
/*  write.c.                                                                 */
/*                                                                           */
/*  Eric Weisstein                                                           */
/*  USRA Summer Graduate Student Program                                     */
/*  NASA/Goddard Space Flight Center                                         */
/*  August 1990                                                              */
/*                                                                           */
/*****************************************************************************/

#include "flyby.h"
#include <math.h>
#include <string.h>
#include <sys/time.h>

#include "panel.h"
#include "psio.h"

int   colormap_mode=0,rgb_allowed=1;
int   num_cpts,num_pts;

unsigned short watch[128] = {
  0x07f0, 0x07f0, 0x07f0, 0x07f0, 0x0808, 0x1004,
  0x1004, 0x11c6, 0x1046, 0x1044, 0x1044, 0x0808,
  0x07f0, 0x07f0, 0x07f0, 0x07f0};
unsigned short arrows[128] = {
   0x0000, 0x0000, 0x0000, 0x0000, 0x0820, 0x1830,
   0x3ff8, 0x7ffc, 0xfffe, 0x7ffc, 0x3ff8, 0x1830,
   0x0820, 0x0000, 0x0000, 0x0000};

main(argc,argv)
int  argc;
char *argv[];
{
 land_t *land;
 header_t *header;
 int flyby=LOOK;
 char hfile[255];

 if (argc==2) {
	strcpy(hfile,argv[1]);
	suffix(hfile,".hdr");
	header = read_header(hfile);
	land = read_datafiles(header);
	}
 else {
    header=(header_t *) malloc(sizeof(header_t));
    strcpy(header->header_name,"fby/hugo.hdr");
    strcpy(header->z_name,"data/hugo.ir");
    strcpy(header->tex_name,"data/hugo.vis");
    header->xdim = 512; header->ydim = 512;
    header->xres = 512; header->yres = 512;
    if (!panel_dialog(0,header))
          exit(0); 
    ps_close_PostScript();
    land = read_datafiles(header);
    write_header(land->header);
    }

 setup_window();
 if (land->header->cmap_name[0]) read_colormap(land->header->cmap_name);
 if (land->header->tex_type!=TT_UNDEF) {
	int ntex,i;
	if (header->tex_count) ntex=header->tex_count;
	else ntex=1;
	for (i=0; i<ntex; i++)
		define_texture(land[i].texmap_id,land[i].header->xres,
				land[i].header->yres,land[i].tmap);
	}
 drawmode(CURSORDRAW);
 defcursor(1,watch);
 defcursor(2,arrows);
 drawmode(NORMALDRAW);

 while (1) {
    switch (flyby) {
      case FLYBY:	flyby = do_flyby(land); break;
      case LOOK:	flyby = do_look(land); adjust_z_scale(land); break;
      case FLIGHTPATH:	flyby = do_flightpath(land); break;
      default:		fprintf(stderr,"Error in main: flyby=%d\n",flyby);
			flyby = LOOK;
    }
  }
}

void adjust_z_scale(land)
land_t *land;
{
 int i,j;
 float dz;
 extern Matrix scale_mat;
 path_t *p;
 if ((dz = scale_mat[2][2]) != 1.0) {
	printf("Adjusting Z scale\n");
	WATCHCURSOR;
	for (i=0; i<land->header->xres*land->header->yres; i++)
		land->zmap[i].z *= dz;
	p = land->header->control_pts;
	if (p) for (i=0; i<p->num_pts; i++) {
		p->eye[i].z *= dz;
		p->look_at[i].z *= dz;
		}
	p = land->header->path;
	if (p) for (i=0; i<p->num_pts; i++) {
		p->eye[i].z *= dz;
		p->look_at[i].z *= dz;
		}
	DEFAULTCURSOR;
	}
}

/*****************************************************************************/
/*                                                                           */
/*                                  read.c                                   */
/*                                                                           */
/*  These routines read flyby parameter files, as well as image data files,  */
/*  from disk.  Note that a flyby file may either contain only header info,  */
/*  in which case it has a .hdr extension, or may contain the header info    */
/*  together with height and texture files (for ease of transporting), in    */
/*  which case it has a .fby extension.                                      */ 
/*                                                                           */
/*  Ron Klasky                                                               */
/*  STX/NASA/GSFC                                                            */
/*  April 1989 (original program)                                            */
/*                                                                           */
/*  Dave Pape                                                                */
/*  NASA/GSFC                                                                */
/*  May 1989 (modifications)                                                 */
/*                                                                           */
/*  Eric Weisstein                                                           */
/*  USRA Summer Graduate Student Program                                     */
/*  NASA/Goddard Space Flight Center                                         */
/*  August 1990                                                              */
/*                                                                           */
/*****************************************************************************/

#include "externs.h"
#include <malloc.h>
#include <string.h>

land_t *read_datafiles(header)
header_t *header;
{
 land_t *land;
 int i;
 if (header->z_count > header->tex_count) header->num_land = header->z_count;
 else header->num_land = header->tex_count;
 if (!header->num_land) header->num_land = 1;
 land = (land_t *) malloc(sizeof(land_t) * header->num_land);
 for (i=0; i<header->num_land; i++) {
    land[i].header = header;
    land[i].norm = land[i].lores_norm = NULL;
    read_elv_file(header,i,land);
    if (header->tex_type == TT_GREY) read_tex_file(header,i,land);
    else if (header->tex_type == TT_RGB) read_rgbtex_file(header,i,land);
    else land[i].tmap = NULL;
    }
 return(land);
}

void read_elv_file(header,i,land)
header_t *header;
int i;
land_t *land;
{
 FILE *datafile;
 char filename[80],prefix[80],*suffix;
 if ((header->z_count) && (header->z_count <= i)) {
	land[i].zmap = land[header->z_count-1].zmap;
	land[i].lores_zmap = land[header->z_count-1].lores_zmap;
	}
 else if ((i) && (header->z_count <= i)) {
	land[i].zmap = land[0].zmap;
	land[i].lores_zmap = land[0].lores_zmap;
	}
 else {
    if (header->z_count) {
	split_name(header->z_name,prefix,&suffix);
	sprintf(filename,"%s%d%s",prefix,i+1,suffix);
	}
    else strcpy(filename,header->z_name);
    if ((datafile=fopen(filename,"r"))==NULL) {
	fprintf(stderr,"Cannot open elevation file \"%s\".\n",filename);
	exit(1);
	}
    read_elv(datafile,&land[i]);
    fclose(datafile);
    }
}

void read_tex_file(header,i,land)
header_t *header;
int i;
land_t *land;
{
 FILE *datafile;
 char filename[80],prefix[80],*suffix;
 if ((header->tex_count) && (header->tex_count <= i)) {
	land[i].tmap = land[header->tex_count-1].tmap;
	land[i].texmap_id = header->tex_count;
	}
 else if ((i) && (header->tex_count <= i)) {
	land[i].tmap = land[0].tmap;
	land[i].texmap_id = 1;
	}
 else {
    if (header->tex_count) {
	split_name(header->tex_name,prefix,&suffix);
	sprintf(filename,"%s%d%s",prefix,i+1,suffix);
	}
    else strcpy(filename,header->tex_name);
    if ((datafile=fopen(filename,"r"))==NULL) {
	fprintf(stderr,"Cannot open texture file \"%s\".\n",filename);
	exit(1);
	}
    read_tex(datafile,&land[i]);
    fclose(datafile);
    land[i].texmap_id = i+1;
    }
}

read_rgbtex_file(header,i,land)
header_t *header;
int i;
land_t *land;
{
 FILE *rfile,*gfile,*bfile;
 char rname[80],gname[80],bname[80],prefix[80],*suffix;
 if ((header->tex_count) && (header->tex_count <= i)) {
	land[i].tmap = land[header->tex_count-1].tmap;
	land[i].texmap_id = header->tex_count;
	}
 else if ((i) && (header->tex_count <= i)) {
	land[i].tmap = land[0].tmap;
	land[i].texmap_id = 1;
	}
 else {
    if (header->tex_count) {
	split_name(header->tex_rname,prefix,&suffix);
	sprintf(rname,"%s%d%s",prefix,i+1,suffix);
	split_name(header->tex_gname,prefix,&suffix);
	sprintf(gname,"%s%d%s",prefix,i+1,suffix);
	split_name(header->tex_bname,prefix,&suffix);
	sprintf(bname,"%s%d%s",prefix,i+1,suffix);
	}
    else {
	strcpy(rname,header->tex_rname);
	strcpy(gname,header->tex_gname);
	strcpy(bname,header->tex_bname);
	}
    if ((rfile=fopen(rname,"r"))==NULL) {
	fprintf(stderr,"Cannot open texture file \"%s\".\n",rname);
	exit(1);
	}
    if ((gfile=fopen(gname,"r"))==NULL) {
	fprintf(stderr,"Cannot open texture file \"%s\".\n",gname);
	exit(1);
	}
    if ((bfile=fopen(bname,"r"))==NULL) {
	fprintf(stderr,"Cannot open texture file \"%s\".\n",bname);
	exit(1);
	}
    read_rgbtex(rfile,gfile,bfile,&land[i]);
    fclose(rfile);
    fclose(gfile);
    fclose(bfile);
    land[i].texmap_id = i+1;
    }
}

void split_name(name,prefix,suffix)
char *name,*prefix,**suffix;
{
 register char *p;
 for (p=name, *suffix=NULL; *p; p++)
	if (*p=='.') *suffix=p;
 if (!suffix) strcpy(name,prefix);
 else {
	for (p=name; p != *suffix; ) *prefix++ = *p++;
	*prefix='\0';
	}
}

static void reduce_elv(land_t *land)
{
 int i,j;
 float stepx,stepy;
 Point *lz;
 land->lores_zmap=(Point *) malloc(land->header->lores_xres *
				land->header->lores_yres * sizeof(Point));
 stepx = (float)land->header->xres / (float)land->header->lores_xres;
 stepy = (float)land->header->yres / (float)land->header->lores_yres;
 for (j=0, lz=land->lores_zmap; j < land->header->lores_yres; j++)
	for (i=0; i < land->header->lores_xres; i++, lz++)
		*lz = land->zmap[(int)(j*stepy)*land->header->xres + (int)(i*stepx)];
}

void read_elv(infile,land)
FILE *infile;
land_t *land;
{
 int i,j,line;
 float xinc,yinc;
 unsigned char *tmp;

 tmp=(unsigned char *)malloc(land->header->xdim*sizeof(unsigned char));
 land->zmap=(Point *) malloc(land->header->xres * land->header->yres *
				sizeof(Point));
 xinc = (float)land->header->xdim / (float)land->header->xres;
 yinc = (float)land->header->ydim / (float)land->header->yres;
 for (i=0, line=0; i<land->header->yres; i++) {
    fread(tmp,land->header->xdim,1,infile);
    for (j=0; j<land->header->xres; j++) {
	land->zmap[(land->header->yres-1-i)*land->header->xres+j].x =
			COORD(j,land->header->xres);
	land->zmap[(land->header->yres-1-i)*land->header->xres+j].y =
			COORD(land->header->yres-1-i,land->header->yres);
	land->zmap[(land->header->yres-1-i)*land->header->xres+j].z =
			land->header->z_scale*((float)tmp[(int)(j*xinc)]/255.0);
	}
    for (line++; line < (int)(i*yinc); line++)
       fread(tmp,land->header->xdim,1,infile);
    }
 reduce_elv(land);
 free(tmp);
 fprintf(stderr,"Elevation data read and blocked from %dx%d to %dx%d.\n",
		land->header->xdim,land->header->ydim,land->header->xres,
		land->header->yres);
}

void read_tex(infile,land)
FILE *infile;
land_t *land;
{
 int i,j,line;
 float xinc,yinc;
 unsigned char *tmp;

 tmp=(unsigned char *)malloc(land->header->xdim*sizeof(unsigned char));
 land->tmap=(unsigned long *)malloc(land->header->xres * land->header->yres *
					sizeof(unsigned long));
 xinc=(float)land->header->xdim / (float)land->header->xres;
 yinc=(float)land->header->ydim / (float)land->header->yres;
 for (i=0, line=0; i<land->header->yres; i++) {
    fread(tmp,land->header->xdim,1,infile);
    for (j=0; j<land->header->xres; j++)
      land->tmap[(land->header->yres-1-i)*land->header->xres+j] =
			tmp[(int)(j*xinc)] | (tmp[(int)(j*xinc)]<<8) |
			(tmp[(int)(j*xinc)]<<16) |
			((tmp[(int)(j*xinc)] & 0xf0) << 24);
    for (line++; line < (int)(i*yinc); line++)
       fread(tmp,land->header->xdim,1,infile);
  }
 free(tmp);
 fprintf(stderr,"Texture data read and blocked from %dx%d to %dx%d.\n",
		land->header->xdim,land->header->ydim,land->header->xres,
		land->header->yres);
}

read_rgbtex(rfile,gfile,bfile,land)
FILE *rfile,*gfile,*bfile;
land_t *land;
{
 int i,j,line;
 float xinc,yinc;
 unsigned char *rtmp,*gtmp,*btmp;

 rtmp=(unsigned char *)malloc(land->header->xdim*sizeof(unsigned char));
 gtmp=(unsigned char *)malloc(land->header->xdim*sizeof(unsigned char));
 btmp=(unsigned char *)malloc(land->header->xdim*sizeof(unsigned char));
 land->tmap=(unsigned long *)malloc(land->header->xres * land->header->yres *
					sizeof(unsigned long));
 xinc=(float)land->header->xdim / (float)land->header->xres;
 yinc=(float)land->header->ydim / (float)land->header->yres;
 for (i=0, line=0; i<land->header->yres; i++) {
    fread(rtmp,land->header->xdim,1,rfile);
    fread(gtmp,land->header->xdim,1,gfile);
    fread(btmp,land->header->xdim,1,bfile);
    for (j=0; j<land->header->xres; j++)
      land->tmap[(land->header->yres-1-i)*land->header->xres+j] =
			rtmp[(int)(j*xinc)] | (gtmp[(int)(j*xinc)]<<8) |
			(btmp[(int)(j*xinc)]<<16) | (rtmp[(int)(j*xinc)]<<24);
    for (line++; line < (int)(i*yinc); line++) {
	fread(rtmp,land->header->xdim,1,rfile);
	fread(gtmp,land->header->xdim,1,gfile);
	fread(btmp,land->header->xdim,1,bfile);
	}
  }
 free(rtmp);  free(gtmp);  free(btmp);
 fprintf(stderr,"Texture data read and blocked from %dx%d to %dx%d.\n",
		land->header->xdim,land->header->ydim,land->header->xres,
		land->header->yres);
}

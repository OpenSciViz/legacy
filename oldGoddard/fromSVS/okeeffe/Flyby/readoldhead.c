#include "externs.h"

static void read_cnt_points(FILE *infile,int num_pts,header_t *head);
static void read_path_points(FILE *infile,int num_pts,header_t *head);

header_t *read_old_header(filename)
char *filename;
{
 char str[80],fullname[80];
 int  i,crop,num_cpts,num_pts;
 FILE *infile;
 header_t *head;
 float bscale;  /* dummy value */
 light_t *light;

 strcpy(fullname,filename);
 suffix(fullname,".hdr");
 if ((infile=fopen(fullname,"r"))==NULL) {
	fprintf(stderr,"Could not open flyby file \"%s\".  Aborting.\n",
		fullname);
	exit(1);
	}
 head = (header_t *) malloc(sizeof(header_t));
 strcpy(head->header_name,fullname);

 fscanf(infile,"%s",str);
 if (!strcmp(str,"*flyby_header*"))
	;
 else {
    fprintf(stderr,"File %s is not a flyby file!",fullname);
    fclose(infile);
    exit(1);
    }
 fprintf(stderr,"Reading header file \"%s\"\n",fullname);
 fscanf(infile,"%d%d",&head->xdim,&head->ydim);
 fscanf(infile,"%d%d",&head->xres,&head->yres);
 if (head->xres > QUICKRES) head->lores_xres = QUICKRES;
 else head->lores_xres = head->xres;
 if (head->yres > QUICKRES) head->lores_yres = QUICKRES;
 else head->lores_yres = head->yres;
 fgets(str,80,infile);
 fgets(str,80,infile);
 head->z_count = 0;
 sscanf(str,"%s%d",head->z_name,&head->z_count);
 fgets(str,80,infile);
 head->tex_count = 0;
 sscanf(str,"%s%d",head->tex_name,&head->tex_count); 
 if (fscanf(infile,"%d%f%f",&crop,&bscale,&head->z_scale) != 3)
		fprintf(stderr,"Read wrong # of arguments.\n");
 fscanf(infile,"%d",&head->draw_mode);
 fprintf(stderr,"Header information read.\n");
 head->light=NULL;
 for (i=0; i<4; i++) {
    light = (light_t *) malloc(sizeof(light_t));
    fscanf(infile,"%f%f%f ",&light->x,&light->y,&light->z);
    fgets(light->name,80,infile);
    if (light->name[strlen(light->name)-1]=='\n')
         light->name[strlen(light->name)-1]='\0';
    } 
 fprintf(stderr,"Light data read.\n");
 fscanf(infile,"%d",&num_cpts);
 if (num_cpts) {
    read_cnt_points(infile,num_cpts,head);
    fprintf(stderr,"%d control points read.\n",num_cpts);
    }
 fscanf(infile,"%d",&num_pts);
 if (num_pts) {
    read_path_points(infile,num_pts,head);
    fprintf(stderr,"%d path points read.\n",num_pts);
    }
 fclose(infile);
 head->tex_type = TT_GREY;
 return(head);
}

static void read_path_points(infile,num_pts,head)
FILE *infile;
int num_pts;
header_t *head;
{
 int i,max_size;
 path_t *p;
 head->path = p = (path_t *) malloc(sizeof(path_t));
 p->num_pts = num_pts;
 for (max_size = PATH_CHUNK_SIZE; max_size <= num_pts;
	max_size += PATH_CHUNK_SIZE) ;
 p->max_pts = max_size;
 p->eye = (Point *) malloc(max_size * sizeof(Point));
 p->look_at = (Point *) malloc(max_size * sizeof(Point));
 for (i=0; i<num_pts; i++) 
    fscanf(infile,"%f%f%f",&p->eye[i].x,&p->eye[i].y,&p->eye[i].z);
 for (i=0; i<num_pts; i++) 
    fscanf(infile,"%f%f%f",&p->look_at[i].x,&p->look_at[i].y,&p->look_at[i].z);
}

static void read_cnt_points(infile,num_pts,head)
FILE *infile;
int num_pts;
header_t *head;
{
 int i,max_size;
 path_t *p;
 head->control_pts = p = (path_t *) malloc(sizeof(path_t));
 p->num_pts = num_pts;
 for (max_size = CPTS_CHUNK_SIZE; max_size <= num_pts;
	max_size += CPTS_CHUNK_SIZE) ;
 p->max_pts = max_size;
 p->eye = (Point *) malloc(max_size * sizeof(Point));
 p->look_at = (Point *) malloc(max_size * sizeof(Point));
 for (i=0; i<num_pts; i++) 
    fscanf(infile,"%f%f%f",&p->eye[i].x,&p->eye[i].y,&p->eye[i].z);
 for (i=0; i<num_pts; i++) 
    fscanf(infile,"%f%f%f",&p->look_at[i].x,&p->look_at[i].y,&p->look_at[i].z);
} 

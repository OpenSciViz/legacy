#ifndef _flyby_
#define _flyby_

#include <stdio.h>
#include <gl.h>
#include "defines.h"

#define FLYBY      1
#define LOOK       2
#define FLIGHTPATH 3 

#define SCALE   200

#define MOUSECOORD(i,res)   (int)(((float)i/(float)SCALE+0.5)*(float)(res-1))
#define COORD(i,res)        ((((float)i)/(float)(res-1)-0.5)*SCALE)

#define QUICKRES 50

#define PATH_CHUNK_SIZE 2000
#define CPTS_CHUNK_SIZE 100

#define DEFAULTCURSOR setcursor(0,0,0)
#define WATCHCURSOR setcursor(1,0,0)
#define ARROWCURSOR setcursor(2,0,0)

typedef struct { Coord x,y,z; } Point;

typedef struct _light {
		float red,green,blue;
		float x,y,z;
		char name[80];
		int on;
		int id;
		struct _light *next;
		} light_t;

typedef struct {
		int   num_pts;
		int   max_pts;
		Point *eye;
		Point *look_at;
		} path_t;

typedef struct {
		char header_name[80];
		int  num_land;
		int  xres,yres,xdim,ydim;
		int  lores_xres,lores_yres;
		char z_name[80];
		int  z_count;
		float z_scale;
		int  tex_type;
		char tex_name[80];
		char tex_rname[80], tex_gname[80], tex_bname[80];
		int  tex_count;
		char cmap_name[80];
		int  draw_mode;
		path_t *control_pts;
		path_t *path;
		light_t *light;
		} header_t;

typedef struct { float nx,ny,nz; } norm_t;

typedef struct {
		Point *zmap;		/* height data */
		Point *lores_zmap;
		unsigned long *tmap;	/* texture map */
		long texmap_id;
		norm_t *norm;		/* normals */
		norm_t *lores_norm;
		header_t *header;
		} land_t;

#include "protos.h"
#endif

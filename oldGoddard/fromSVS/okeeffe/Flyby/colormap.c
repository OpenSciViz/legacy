#include "colormap.h"
#include "flyby.h"
#include <gl.h>
#include <stdio.h>
#include <string.h>

#define NUM_COLORS 12

static char *color_name[NUM_COLORS] = {
  "blue","cyan","gray","gray_doubleramp","gray_inverted","gray_patch",
  "green","lilac","rainbow","red","violet","yellow"};
static int color_id[NUM_COLORS] = {
	CM_BLUE, CM_CYAN, CM_GRAY, CM_GRAY2, CM_GRAYI, CM_GRAYP,
	CM_GREEN, CM_LILAC, CM_RAINBOW, CM_RED, CM_VIOLET, CM_YELLOW };

int read_colormap(char *fname)
{
 FILE  *colorfile;
 int   i,red,green,blue;

 if ((colorfile=fopen(fname,"r"))==NULL) {
    fprintf(stderr,"Error: Could not open %s\n",fname);
    return(0);
    }
 for (i=0; i<256; i++)
    if (fscanf(colorfile,"%d%d%d",&red,&green,&blue)!=3) {
	fprintf(stderr,"Error: Could not read colormap entry %d\n",i);
	return(0);
	}
    else mapcolor(i,red,green,blue);
 fclose(colorfile);
 return(1);
}

void set_colormap(header_t *header,int id)
{
 char newmap[256];
 int mapnum;
 if (id==CM_OTHER) {
	sprintf(newmap,DEFAULT_CM_DIR);
	get_text(newmap,"Colormap to load:");
	if (newmap[0]=='\0') return;
	}
 else {
	for (mapnum=0; mapnum<NUM_COLORS; mapnum++)
		if (id == color_id[mapnum]) break;
	sprintf(newmap,"%s/%s.cm",DEFAULT_CM_DIR,color_name[mapnum]);
	}
 if (read_colormap(newmap))
    strcpy(header->cmap_name,newmap); 
}

void reset_colormap(header_t *header)
{
 header->cmap_name[0]='\0';
 mapcolor(BLACK,0,0,0);
 mapcolor(RED,255,0,0);
 mapcolor(GREEN,0,255,0);
 mapcolor(YELLOW,255,255,0);
 mapcolor(BLUE,0,0,255);
 mapcolor(MAGENTA,255,0,255);
 mapcolor(CYAN,0,255,255);
 mapcolor(WHITE,255,255,255);
}

int colormap_menu()
{
 char tempstr[100],str[300];
 int  i,submenu,menu;

 strcpy(str,"Colormaps %t");
 for (i=0; i<NUM_COLORS; i++) {
	sprintf(tempstr,"| %s %%x%d",color_name[i],color_id[i]);
	strcat(str,tempstr);
	}
 sprintf(tempstr,"%l| other %%x%d",CM_OTHER);
 strcat(str,tempstr);
 submenu=defpup(str);
 menu=defpup("Colormap Options %t|load colormap %m|reset colormap %x3000",
		submenu);
 return(menu);
}

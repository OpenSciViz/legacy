#include <fcntl.h>
#include <gl.h>

#define XRES 1024
#define YRES 1024

void rgbdump(basename)
char *basename;
{
 char name[80];
 int fd,i;
 unsigned int image[XRES*YRES];
 unsigned char out[XRES*YRES];
/* pixmode(PM_TTOB,1); */
 readsource(SRC_FRONT);
 lrectread(0,0,XRES-1,YRES-1,image);
 sprintf(name,"%s.red",basename);
 if ((fd=creat(name,0644)) == -1) {
	printf("failed to create \"%s\"\n",name);
	return;
	}
 for (i=0; i<XRES*YRES; i++) out[i] = image[i] & 0xff;
 write(fd,out,XRES*YRES);
 close(fd);
 sprintf(name,"%s.gre",basename);
 if ((fd=creat(name,0644)) == -1) {
	printf("failed to create \"%s\"\n",name);
	return;
	}
 for (i=0; i<XRES*YRES; i++) out[i] = (image[i]>>8) & 0xff;
 write(fd,out,XRES*YRES);
 close(fd);
 sprintf(name,"%s.blu",basename);
 if ((fd=creat(name,0644)) == -1) {
	printf("failed to create \"%s\"\n",name);
	return;
	}
 for (i=0; i<XRES*YRES; i++) out[i] = (image[i]>>16) & 0xff;
 write(fd,out,XRES*YRES);
 close(fd);
}

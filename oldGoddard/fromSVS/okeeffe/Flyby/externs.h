/*****************************************************************************/
/*                                                                           */
/*                               externs.h                                   */
/*                                                                           */
/*  This file contains the global variables used in the flyby program.       */
/*  They are initialized at the beginning of the main.c portion              */ 
/*                                                                           */
/*  Dave Pape                                                                */
/*  NASA/GSFC                                                                */
/*  May 1989 (original flyby_externs.h)                                      */
/*                                                                           */
/*  Eric Weisstein                                                           */
/*  USRA Summer Graduate Student Program                                     */
/*  NASA/Goddard Space Flight Center                                         */
/*  August 1990                                                              */
/*                                                                           */
/*****************************************************************************/

#include <stdio.h>
#include "flyby.h"

extern int    colormap_mode,rgb_allowed;

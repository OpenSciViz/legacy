#include "flyby.h"
#include "tokens.h"

double atof();
static int parse_command(char *token);
static void get_dimensions(FILE *fp, header_t *header);
static void get_resolution(FILE *fp, header_t *header);
static void get_zmap(FILE *fp, header_t *header);
static void get_texmap(FILE *fp, header_t *header);
static void get_rgbtexmap(FILE *fp, header_t *header);
static void get_light(FILE *fp, header_t *header);
static void get_colormap(FILE *fp, header_t *header);
static void get_use(FILE *fp, header_t *header);
static void get_controlpts(FILE *fp, header_t *header);
static void get_path(FILE *fp, header_t *header);

char *next_token(FILE *fp,char *token)
{
 static char c=0;
 int i;
 if (!c)
	if ((c=getc(fp))==(char)EOF) return(NULL);
 while (isspace(c))
	if ((c=getc(fp))==(char)EOF) return(NULL);
 token[0] = c;
 if ((c=='(') || (c==')') || (c==',')) {
	token[1] = '\0';
	c = 0;
	}
 else {
	i=1;
	c=getc(fp);
	while (!isspace(c) && (c!='(') && (c!=')') && (c!=',') &&
			(c!=(char)EOF)) {
		token[i++] = c;
		c = getc(fp);
		}
	token[i]='\0';
	}
 return(token);
}

int is_old_header(char *fname)
{
 FILE *fp;
 char s[80];
 if ((fp = fopen(fname,"r")) == NULL) {
	fprintf(stderr,"Could not open %s\n",fname);
	exit();
	}
 fscanf(fp,"%s",s);
 return(!strcmp(s,"*flyby_header*"));
}

header_t *read_header(char *fname)
{
 header_t *header;
 FILE *fp;
 char token[80];
 if (is_old_header(fname)) return(read_old_header(fname));
 if ((fp = fopen(fname,"r")) == NULL) {
	fprintf(stderr,"Could not open %s\n",fname);
	return(NULL);
	}
 header = (header_t *) malloc(sizeof(header_t));
 strcpy(header->header_name,fname);
 header->xdim = header->ydim = 512;
 header->xres = header->yres = 0;
 header->z_scale = 1.0;
 header->light = NULL;
 header->path = NULL;
 header->z_name[0] = header->tex_name[0] = header->tex_rname[0] =
   header->tex_gname[0] = header->tex_bname[0] = header->cmap_name[0] = '\0';
 header->tex_type = TT_UNDEF;
 header->z_count = header->tex_count = 0;
 header->draw_mode = DM_TEXMAP;
 while (next_token(fp,token))
    switch (parse_command(token)) {
	case C_DIMENSION: get_dimensions(fp,header); break;
	case C_RESOLUTION: get_resolution(fp,header); break;
	case C_ZMAP: get_zmap(fp,header); break;
	case C_TEXMAP: get_texmap(fp,header); break;
	case C_RGBTEXMAP: get_rgbtexmap(fp,header); break;
	case C_LIGHT: get_light(fp,header); break;
	case C_COLORMAP: get_colormap(fp,header); break;
	case C_USE: get_use(fp,header); break;
	case C_CONTROLPTS: get_controlpts(fp,header); break;
	case C_PATH: get_path(fp,header); break;
	case C_UNKNOWN: fprintf(stderr,"token \"%s\" unknown - ignored\n",
				token);
	}
 fclose(fp);
 if (!header->xres) {
	header->xres = header->xdim;
	header->yres = header->ydim;
	}
 if (header->xres > QUICKRES) header->lores_xres = QUICKRES;
 else header->lores_xres = header->xres;
 if (header->yres > QUICKRES) header->lores_yres = QUICKRES;
 else header->lores_yres = header->yres;
 return(header);
}

static struct _token command_list[] = {
	{ "dimension", C_DIMENSION },
	{ "resolution", C_RESOLUTION },
	{ "zmap", C_ZMAP },
	{ "texmap", C_TEXMAP },
	{ "rgbtexmap", C_RGBTEXMAP },
	{ "light", C_LIGHT },
	{ "colormap", C_COLORMAP },
	{ "use", C_USE },
	{ "controlpts", C_CONTROLPTS },
	{ "path", C_PATH },
	{ NULL, C_UNKNOWN }
	};

static int parse_command(char *token)
{
 int i;
 for (i=0; command_list[i].string != NULL; i++)
	if (!strcmp(token,command_list[i].string))
		return(command_list[i].val);
 return(C_UNKNOWN);
}

#define ERROR(command) { fprintf(stderr,"error parsing \"%s\"\n",command); \
			 return; }
#define GET_TOKEN(fp,s,command)    if (!next_token(fp,s)) ERROR(command)
#define GET_LPAREN(fp,s,command)   { GET_TOKEN(fp,s,command); \
				     if (strcmp(s,"(")) ERROR(command); }
#define GET_RPAREN(fp,s,command)   { GET_TOKEN(fp,s,command); \
				     if (strcmp(s,")")) ERROR(command); }
#define GET_COMMA(fp,s,command)    { GET_TOKEN(fp,s,command); \
				     if (strcmp(s,",")) ERROR(command); }

static void get_dimensions(FILE *fp, header_t *header)
{
 char s[80];
 int x,y;
 GET_LPAREN(fp,s,"dimension");
 GET_TOKEN(fp,s,"dimension");	x = atoi(s);
 GET_COMMA(fp,s,"dimension");
 GET_TOKEN(fp,s,"dimension");	y = atoi(s);
 GET_RPAREN(fp,s,"dimension");
 if ((x<1) || (y<1))
	fprintf(stderr,"error: invalid dimensions (%d,%d)\n",x,y);
 else {
	header->xdim = x;
	header->ydim = y;
	}
}

static void get_resolution(FILE *fp, header_t *header)
{
 char s[80];
 int x,y;
 GET_LPAREN(fp,s,"resolution");
 GET_TOKEN(fp,s,"resolution");		x = atoi(s);
 GET_COMMA(fp,s,"resolution");
 GET_TOKEN(fp,s,"resolution");		y = atoi(s);
 GET_RPAREN(fp,s,"resolution");
 if ((x<1) || (y<1))
	fprintf(stderr,"error: invalid resolution (%d,%d)\n",x,y);
 else {
	header->xres = x;
	header->yres = y;
	}
}

static void get_zmap(FILE *fp, header_t *header)
{
 char s[80],fname[80];
 float scale=1.0;
 int count=0;
 GET_LPAREN(fp,s,"zmap");
 GET_TOKEN(fp,fname,"zmap");
 GET_TOKEN(fp,s,"zmap");
 if (!strcmp(s,",")) {
	GET_TOKEN(fp,s,"zmap");		scale = atof(s);
	GET_TOKEN(fp,s,"zmap");
	if (!strcmp(s,",")) {
		GET_TOKEN(fp,s,"zmap");		count = atoi(s);
		GET_RPAREN(fp,s,"zmap");
		}
	else if (strcmp(s,")")) ERROR("zmap");
	}
 else if (strcmp(s,")")) ERROR("zmap");
 strcpy(header->z_name,fname);
 header->z_scale = scale;
 header->z_count = count;
}

static void get_texmap(FILE *fp, header_t *header)
{
 char s[80],fname[80];
 int count=0;
 GET_LPAREN(fp,s,"texmap");
 GET_TOKEN(fp,fname,"texmap");
 GET_TOKEN(fp,s,"texmap");
 if (!strcmp(s,",")) {
	GET_TOKEN(fp,s,"texmap");		count = atoi(s);
	GET_RPAREN(fp,s,"texmap");
	}
 else if (strcmp(s,")")) ERROR("texmap");
 strcpy(header->tex_name,fname);
 header->tex_count = count;
 header->tex_type = TT_GREY;
}

static void get_rgbtexmap(FILE *fp, header_t *header)
{
 char s[80],rname[80],gname[80],bname[80];
 int count=0;
 GET_LPAREN(fp,s,"rgbtexmap");
 GET_TOKEN(fp,rname,"rgbtexmap");
 GET_COMMA(fp,s,"rgbtexmap");
 GET_TOKEN(fp,gname,"rgbtexmap");
 GET_COMMA(fp,s,"rgbtexmap");
 GET_TOKEN(fp,bname,"rgbtexmap");
 GET_TOKEN(fp,s,"rgbtexmap");
 if (!strcmp(s,",")) {
	GET_TOKEN(fp,s,"rgbtexmap");		count = atoi(s);
	GET_RPAREN(fp,s,"rgbtexmap");
	}
 else if (strcmp(s,")")) ERROR("rgbtexmap");
 strcpy(header->tex_rname,rname);
 strcpy(header->tex_gname,gname);
 strcpy(header->tex_bname,bname);
 header->tex_count = count;
 header->tex_type = TT_RGB;
}

static void get_light(FILE *fp, header_t *header)
{
 char s[80],name[80];
 float x,y,z,red=1.0,green=1.0,blue=1.0;
 int on;
 light_t *light,*p;
 GET_LPAREN(fp,s,"light");
 GET_TOKEN(fp,name,"light");
 GET_COMMA(fp,s,"light");
 GET_TOKEN(fp,s,"light");	x = atof(s);
 GET_COMMA(fp,s,"light");
 GET_TOKEN(fp,s,"light");	y = atof(s);
 GET_COMMA(fp,s,"light");
 GET_TOKEN(fp,s,"light");	z = atof(s);
 GET_COMMA(fp,s,"light");
 GET_TOKEN(fp,s,"light");	on = atoi(s);
 GET_TOKEN(fp,s,"light");
 if (!strcmp(s,",")) {
	GET_TOKEN(fp,s,"light");	red = atof(s);
	GET_COMMA(fp,s,"light");
	GET_TOKEN(fp,s,"light");	green = atof(s);
	GET_COMMA(fp,s,"light");
	GET_TOKEN(fp,s,"light");	blue = atof(s);
	GET_RPAREN(fp,s,"light");
	}
 else if (strcmp(s,")")) ERROR("light");
 light = (light_t *) malloc(sizeof(light_t));
 strcpy(light->name,name);
 light->x = x;
 light->y = y;
 light->z = z;
 light->red = red;
 light->green = green;
 light->blue = blue;
 light->on = on;
 light->next = NULL;
 if (header->light) {
	for (p=header->light; p->next; p=p->next) ;
	p->next = light;
	}
 else header->light = light;
}

static void get_colormap(FILE *fp, header_t *header)
{
 char s[80],fname[80];
 GET_LPAREN(fp,s,"colormap");
 GET_TOKEN(fp,fname,"colormap");
 GET_RPAREN(fp,s,"colormap");
 strcpy(header->tex_name,fname);
}

static void get_use(FILE *fp, header_t *header)
{
 char s[80],mode[80];
 GET_LPAREN(fp,s,"use");
 GET_TOKEN(fp,mode,"use");
 GET_RPAREN(fp,s,"use");
 if (!strcmp(mode,"texmap")) header->draw_mode = DM_TEXMAP;
 else if (!strcmp(mode,"light")) header->draw_mode = DM_LIGHT;
 else if (!strcmp(mode,"colormap")) header->draw_mode = DM_COLORMAP;
}

static void get_controlpts(FILE *fp, header_t *header)
{
 char s[80];
 int i,n,max_size;
 path_t *path;
 GET_LPAREN(fp,s,"controlpts");
 GET_TOKEN(fp,s,"controlpts");
 n = atoi(s);
 path = (path_t *) malloc(sizeof(path_t));
 path->num_pts = n;
 for (max_size = CPTS_CHUNK_SIZE; max_size <= n; max_size+=CPTS_CHUNK_SIZE) ;
 path->max_pts = max_size;
 path->eye = (Point *) malloc(max_size * sizeof(Point));
 path->look_at = (Point *) malloc(max_size * sizeof(Point));
 for (i=0; i<n; i++) {
	GET_COMMA(fp,s,"controlpts");
	GET_TOKEN(fp,s,"controlpts");	path->eye[i].x = atof(s);
	GET_COMMA(fp,s,"controlpts");
	GET_TOKEN(fp,s,"controlpts");	path->eye[i].y = atof(s);
	GET_COMMA(fp,s,"controlpts");
	GET_TOKEN(fp,s,"controlpts");	path->eye[i].z = atof(s);
	GET_COMMA(fp,s,"controlpts");
	GET_TOKEN(fp,s,"controlpts");	path->look_at[i].x = atof(s);
	GET_COMMA(fp,s,"controlpts");
	GET_TOKEN(fp,s,"controlpts");	path->look_at[i].y = atof(s);
	GET_COMMA(fp,s,"controlpts");
	GET_TOKEN(fp,s,"controlpts");	path->look_at[i].z = atof(s);
	}
 GET_RPAREN(fp,s,"controlpts");
 header->control_pts = path;
}

static void get_path(FILE *fp, header_t *header)
{
 char s[80];
 int i,n,max_size;
 path_t *path;
 GET_LPAREN(fp,s,"path");
 GET_TOKEN(fp,s,"path");
 n = atoi(s);
 path = (path_t *) malloc(sizeof(path_t));
 path->num_pts = n;
 for (max_size = PATH_CHUNK_SIZE; max_size <= n; max_size+=PATH_CHUNK_SIZE) ;
 path->max_pts = max_size;
 path->eye = (Point *) malloc(max_size * sizeof(Point));
 path->look_at = (Point *) malloc(max_size * sizeof(Point));
 for (i=0; i<n; i++) {
	GET_COMMA(fp,s,"path");
	GET_TOKEN(fp,s,"path");	path->eye[i].x = atof(s);
	GET_COMMA(fp,s,"path");
	GET_TOKEN(fp,s,"path");	path->eye[i].y = atof(s);
	GET_COMMA(fp,s,"path");
	GET_TOKEN(fp,s,"path");	path->eye[i].z = atof(s);
	GET_COMMA(fp,s,"path");
	GET_TOKEN(fp,s,"path");	path->look_at[i].x = atof(s);
	GET_COMMA(fp,s,"path");
	GET_TOKEN(fp,s,"path");	path->look_at[i].y = atof(s);
	GET_COMMA(fp,s,"path");
	GET_TOKEN(fp,s,"path");	path->look_at[i].z = atof(s);
	}
 GET_RPAREN(fp,s,"path");
 header->path = path;
}

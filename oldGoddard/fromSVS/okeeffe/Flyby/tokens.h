struct _token { char *string; int val; } ;

#define C_UNKNOWN	0
#define C_DIMENSION	1
#define C_RESOLUTION	2
#define C_ZMAP		3
#define C_TEXMAP	4
#define C_RGBTEXMAP	5
#define C_LIGHT		6
#define C_COLORMAP	7
#define C_USE		8
#define C_CONTROLPTS	9
#define C_PATH		10

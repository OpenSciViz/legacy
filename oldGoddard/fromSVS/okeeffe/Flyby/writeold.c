/*****************************************************************************/
/*                                                                           */
/*                                  write.c                                  */
/*                                                                           */
/*  These routines write flyby parameter files to disk.  Note that a flyby   */
/*  file may either contain only header info, in which case it has a .hdr    */
/*  extension, or may contain the header info together with height and       */
/*  texture files (for ease of transporting), in which case it has a .fby    */
/*  extension.                                                               */
/*                                                                           */ 
/*  Eric Weisstein                                                           */
/*  USRA Summer Graduate Student Program                                     */
/*  NASA/Goddard Space Flight Center                                         */
/*  August 1990                                                              */
/*                                                                           */
/*****************************************************************************/

#include "externs.h"

write_flyby_file(land)
land_t *land;
{
  char outname[100];
  FILE *flyby_file;
  int  i,crop=0;
 float bscale=1.0;

  sprintf(outname,"%s",land->header->header_name);
  suffix(outname,".hdr");
  sprintf(land->header->header_name,"%s",outname);
  if ((flyby_file=fopen(land->header->header_name,"w"))==NULL) {
    fprintf(stderr,"Cannot open flyby file %s for writing\n",
		land->header->header_name);
    exit(1); 
    }
  fprintf(flyby_file,"%s\n","*flyby_header*");
  fprintf(flyby_file,"%d %d\n",land->header->xdim,land->header->ydim);
  fprintf(flyby_file,"%d %d\n",land->header->xres,land->header->yres);
  fprintf(flyby_file,"%s %d\n",land->header->z_name,land->header->z_count);
  fprintf(flyby_file,"%s %d\n",land->header->tex_name,land->header->tex_count);
  fprintf(flyby_file,"%d %f %f\n",crop,bscale,land->header->z_scale);
  fprintf(flyby_file,"%d\n",land->header->draw_mode);
  for (i=0; i<4; i++)
    fprintf(flyby_file,"%f %f %f %s\n",lights[i][5],lights[i][6],
		lights[i][7],ltname[i]);
  fclose(flyby_file);
  fprintf(stderr,"File \"%s\" written.\n",land->header->header_name);
  return(0);
}

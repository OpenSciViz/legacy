/*****************************************************************************/
/*                                                                           */
/*                                  flyby.c                                  */
/*                                                                           */
/*  These routines render a 3-D flyby, optionally saving each frame to       */
/*  disk as a movie.  The movie may then be viewed using the replay          */
/*  program, which will go much more quickly than the original rendering.    */
/*  The path used in the flyby is specified by the user with the             */
/*  flightpath module.                                                       */
/*                                                                           */
/*  Ron Klasky                                                               */
/*  STX/NASA/GSFC                                                            */
/*  April 1989 (original program)                                            */
/*                                                                           */
/*  Dave Pape                                                                */
/*  NASA/GSFC                                                                */
/*  May 1989 (modifications)                                                 */
/*                                                                           */
/*  Eric Weisstein                                                           */
/*  USRA Summer Graduate Student Program                                     */
/*  NASA/Goddard Space Flight Center                                         */
/*  August 1990                                                              */
/*                                                                           */
/*****************************************************************************/

#include "externs.h"
#include <device.h>
#include <string.h>
#include <sys/time.h>

#define ONEBYTE      0 
#define TWOBYTE      1
#define FILES        2 
#define UPSIDEDOWN   3 
#define OPTDISK      4 
#define START        5 
#define STOP         6 
#define QUIT         7 
#define MFLIGHTPATH  8
#define MLOOK        9
#define SHADED      10
#define WIREFRAME   11
#define RESTART     13

static char  dump_name[5][200];
static FILE *dump_file[5];
static int  dump_flag[5],frames[5];
static long menu;
static int draw_wire=1;

int do_flyby(land)
land_t *land;
{
 char  str[200],prompt[200],size_string[100];
 int   i,running=0;
 long  xsize,ysize;
 short val,dummy,menuval;
 Point *eye = land->header->path->eye, *look = land->header->path->look_at;
 int num_pts = land->header->path->num_pts;

  for (i=0; i<5; i++) frames[i] = dump_flag[i] = 0;
  wintitle("Flyby"); 
  mmode(MVIEWING);
  perspective(600,1.0,0.001,100000.0);
  create_flyby_menu();
  qdevice(RIGHTMOUSE);
  qreset();
  if (!num_pts) return(LOOK);
  if (!rgb_allowed) {
    if (draw_wire) doublebuffer();
    else singlebuffer();
    gconfig();
    }
  for (i=0; i<num_pts; i++) {
    if (running) {
	new_lookat(eye[i].x,eye[i].y,eye[i].z,look[i].x,look[i].y,look[i].z,0);
	draw_it(land);
	if (dump_flag[ONEBYTE]) {
		dumpwindow(dump_file[ONEBYTE],0);
		frames[ONEBYTE]++;
		}
	if (dump_flag[TWOBYTE]) {
		dump_2byte(dump_file[TWOBYTE],0);
		frames[TWOBYTE]++;
		}
	if (dump_flag[UPSIDEDOWN]) {
		dumpwindow_upsidedown(dump_file[UPSIDEDOWN],0);
		frames[UPSIDEDOWN]++;
		}
	if (dump_flag[FILES]) {
		sprintf(str,"%s%04d",dump_name[FILES],frames[FILES]++);
		rgbdump(str);
		}
	}
    else
      i--;
    if (qtest()) {
      val=qread(&dummy);
      switch(val) {
        case REDRAW:
          draw_it(land);
          create_flyby_menu();
          break;
        case RIGHTMOUSE:
          menuval=dopup(menu);
          switch(menuval) {
            case FILES:
              dump_flag[FILES]=!dump_flag[FILES];
              if (dump_flag[FILES]) {
                getwd(dump_name[FILES]);
                strcat(dump_name[FILES],"/");
                get_text(dump_name[FILES],"Enter base name of disk files:");
                if(*dump_name[FILES]==0)
                  dump_flag[FILES]=0;
                else {
                  fprintf(stderr,"Movie with base name %s ready for writing\n",
                    dump_name[FILES]); 
                  getsize(&xsize,&ysize);
                }
              }
              else {
                fprintf(stderr,"%d frames saved to %s\n",
                  frames[FILES],dump_name[FILES]);
              }
              create_flyby_menu();
              break;
            case UPSIDEDOWN:
              dump(UPSIDEDOWN,"Enter name of mirrored disk output file:",".mvr");
              break;
            case ONEBYTE:
              dump(ONEBYTE,"Enter name of 1-byte output file:",".mov");
              break;
            case TWOBYTE:
              dump(TWOBYTE,"Enter name of 2-byte output file:",".mov2");
              break;
            case OPTDISK:
              fprintf(stderr,"Optical disk not currently supported");
              break;
            case START:
               running=1;
               break;
             case STOP:
               running=0;
               break;
             case QUIT:
               exit();
             case MFLIGHTPATH: return(FLIGHTPATH);
             case MLOOK: return(LOOK);
             case SHADED:
               draw_wire=0;
               if (!rgb_allowed) {singlebuffer(); gconfig(); }
               draw_it(land);
               create_flyby_menu();
               break;
             case WIREFRAME:
               draw_wire=1;
               if (!rgb_allowed) {doublebuffer(); gconfig(); }
               draw_it(land);
               create_flyby_menu();
               break;
             }
      }
    } 
  }
  for (i=0; i<5; i++) {
    if (dump_flag[i]) {
      fclose(dump_file[i]);
      fprintf(stderr,"%d frames saved to %s.\n",frames[i]-1,dump_name[i]);
      dump_flag[i]=frames[i]=0;
      }
    }
 return(FLYBY);
}

dump(flag,prompt,suff)
char *prompt,*suff;
int  flag;
{
  long  xsize,ysize;

  dump_flag[flag]=!dump_flag[flag];
  if(dump_flag[flag]) {
    getwd(dump_name[flag]);
    strcat(dump_name[flag],"/");
    get_text(dump_name[flag],prompt);
    if(*dump_name[flag]!=0) {
      suffix(dump_name[flag],suff);
      if((dump_file[flag]=fopen(dump_name[flag],"w"))==NULL) {
        fprintf(stderr,"Cannot open %s for writing.\n",dump_name[flag]);
        dump_flag[flag]=0;
      }
      else {
        fprintf(stderr,"Movie file %s opened for writing.\n",dump_name[flag]);
        getsize(&xsize,&ysize);
        readsource(SRC_FRONT);
/*        fprintf(dump_file[flag],"%ld %ld\n",xsize,ysize); */
      }
    }
    else
      dump_flag[flag]=0;
  }
  else {
    fclose(dump_file[flag]);
    fprintf(stderr,"%d frames saved to %s\n",
      frames[flag],dump_name[flag]);
    frames[flag]=0;
    winconstraints();
    winconstraints();
  }
  create_flyby_menu();
}

draw_it(land)
land_t *land;
{
  if (draw_wire) draw_quick(land,FALSE);
  else draw_world(land,FALSE);
}

pos[5]={1,13,32,50,73};

create_flyby_menu()
{
 char menustr[200],str[80];
 int  i;
 long xsize,ysize;
 long dumpm,displaym,modulem;

 strcpy(str,"   disk %x0|   disk 2-byte %x1|   disk files %x2|   disk upsidedown %x3|   optical disk %x4");
 for(i=0;i<5;i++)
   if(dump_flag[i])
     arrow(str,pos[i]); 
 dumpm=defpup(str);
 modulem=defpup(" flightpath %x8| flyby| look %x9"); 
 setpup(modulem,2,PUP_GREY);
 setpup(dumpm,5,PUP_GREY);
 strcpy(str,"Display options %t|   full render %x10|   quick render %x11");
 if (draw_wire) arrow(str,40);
 else arrow(str,20); 
 displaym=defpup(str);
 getsize(&xsize,&ysize);
 strcpy(menustr,"Flyby Options %t| display options %m| dump %m|"
		" start %x5| stop %x6| module %m%l| quit %x7");
 menu=defpup(menustr,displaym,dumpm,modulem);
}

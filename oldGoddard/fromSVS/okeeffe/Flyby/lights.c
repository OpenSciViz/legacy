#include "externs.h"
#include "lights.h"
#include <malloc.h>
#include "psio.h"
#include <string.h>

double atof();
static void define_lighting(header_t *header);
static void bind_lighting(header_t *header);

void setup_lights(header_t *header)
{
 light_t *light;
 int i;
 mmode(MVIEWING);
 for (light=header->light, i=0; light; light=light->next, i++) ;
 while (i<4) {
	light=(light_t *)malloc(sizeof(light_t));
	strcpy(light->name,"dummy");
	light->x = light->y = 0.0;
	light->z = 1.0;
	light->red = light->green = light->blue = 1.0;
	light->next = header->light;
	header->light = light;
	i++;
	}
 define_lighting(header);
 bind_lighting(header);
}

static void define_lighting(header_t *header)
{
 static float light_data[10] = { { LCOLOR, 1.0, 1.0, 1.0,
				   POSITION, 0.0, 0.0, 10.0, 0.0,
				   LMNULL } };
 static float white_material[]={ DIFFUSE, 0.5,0.5,0.5, LMNULL };
 light_t *light;
 int i;
 for (light=header->light, i=1; light; light=light->next, i++) {
	light_data[1] = light->red;
	light_data[2] = light->green;
	light_data[3] = light->blue;
	light_data[5] = light->x;
	light_data[6] = light->y;
	light_data[7] = light->z;
	light->id = i;
	lmdef(DEFLIGHT,light->id,10,light_data);
	}
 lmdef(DEFLMODEL,1,0,NULL);
 lmdef(DEFMATERIAL,1,0,white_material);
}

static void bind_lighting(header_t *header)
{
 int i;
 light_t *light;
 lmbind(LMODEL,1);
 for (light=header->light, i=0; light; light=light->next, i++)
	if (light->on) lmbind(LIGHT0+i,light->id);
	else lmbind(LIGHT0+i,0);
}

void light_dialog(land_t *land)
{
  char lightx[4][20],lighty[4][20],lightz[4][20],ltname[4][40];
  char string[80];
  int  i,light,coord,color,state;
  int  init_draw_mode=(land->header->draw_mode==DM_LIGHT),init_lton[5];
 light_t *lightp,*lt[5];
 
  for (lightp=land->header->light, i=0; (lightp) && (i<4); lightp=lightp->next,
		i++) {
	sprintf(lightx[i],"%.2f",lightp->x);
	sprintf(lighty[i],"%.2f",lightp->y);
	sprintf(lightz[i],"%.2f",lightp->z);
	strcpy(ltname[i],lightp->name);
	init_lton[i+1]=lightp->on;
	lt[i+1] = lightp;
	}
  if (!ps_open_PostScript())
    fprintf(stderr,"Unable to connect to NeWS server\n");
  ps_light_wind((land->header->draw_mode==DM_LIGHT),
    init_lton[1],ltname[0],lightx[0],lighty[0],lightz[0],
    init_lton[2],ltname[1],lightx[1],lighty[1],lightz[1],
    init_lton[3],ltname[2],lightx[2],lighty[2],lightz[2],
    init_lton[4],ltname[3],lightx[3],lighty[3],lightz[3]);
  while (!psio_eof(PostScriptInput)) {
    if (ps_toggle(&light,&state))
         init_lton[light] ? (lt[light]->on=!state) : (lt[light]->on=state);
    if (ps_name(string,&light)) 
         strcpy(lt[light]->name,string); 
    if (ps_color(&light,&color))
         fprintf(stderr,"Sorry, colored lights not implemented...\n");
    if (ps_coord(string,&light,&coord)) 
	switch(coord) {
		case 5: lt[light]->x = atof(string); break;
		case 6: lt[light]->y = atof(string); break;
		case 7: lt[light]->z = atof(string); break;
		}
    if (ps_apply_lights())
         draw_and_time(land);
    if (ps_close_lights()) { setup_lights(land->header); return; }
    if (ps_mode_switch(&state))
	 if (init_draw_mode)
		if (state) land->header->draw_mode = DM_TEXMAP;
		else land->header->draw_mode = DM_LIGHT;
	 else
		if (state) land->header->draw_mode = DM_LIGHT;
		else land->header->draw_mode = DM_TEXMAP;
  }
}

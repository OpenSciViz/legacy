/*****************************************************************************/
/*                                                                           */
/*                               flightpath.c                                */
/*                                                                           */
/*  These routines display a line drawing of the terrain scene and allow a   */
/*  user to drop 3d control points over it to form a flight path.  The user  */
/*  can then connect those points with a 3d cardinal spline, save them to    */
/*  the flyby file, and use them to create a 3-D flyby.                      */
/*                                                                           */
/*  Ron Klasky                                                               */
/*  STX/NASA/GSFC                                                            */
/*  April 1989 (original program)                                            */
/*                                                                           */
/*  Dave Pape                                                                */
/*  NASA/GSFC                                                                */
/*  May 1989 (modifications)                                                 */
/*                                                                           */
/*  Eric Weisstein                                                           */
/*  USRA Summer Graduate Student Program                                     */
/*  NASA/Goddard Space Flight Center                                         */
/*  August 1990                                                              */
/*									     */
/*****************************************************************************/

#include "externs.h"
#include <device.h>
#include <math.h>

#define MINHEIGHT 1.0

static int view_window=0;

int    drawpath=1,drawaxis=0;
float  dz=5.0;
long   menu;
Matrix flight_mat={1.0,0.0,0.0,0.0,
                   0.0,1.0,0.0,0.0,
                   0.0,0.0,1.0,0.0,
                   0.0,0.0,0.0,1.0};

#define SPIN         1
#define DROP_PTS     5
#define PROCESS_PTS  6
#define SHOW_PATH    7
#define PROC_RESTR   8
#define MFLYBY       9
#define MLOOK       10
#define RESET       11
#define DELETE      12
#define QUIT        99

create_flightpath_menu()
{
 long modulem,processm;
 char str[200];

 modulem=defpup("module %t| flightpath | flyby %x9| look %x10");
 setpup(modulem,1,PUP_GREY); 
 processm=defpup("process points %t| unrestricted %x6| restrict to top %x8");
 strcpy(str,"Flight Path %t| drop points %x5| delete point %x12| process points %m|");
 if (!drawpath) strcat(str," path show %x7|");
 else strcat(str," path hide %x7|");
 strcat(str," reset points %x11| spin %x1| module %m%l| quit %x99");
 menu=defpup(str,processm,modulem);
}

static void draw_path(land_t *land)
{
 int i,num_pts = land->header->path->num_pts;
 float ver[3];
 Point *path1 = land->header->path->eye, *path2 = land->header->path->look_at;
 if (num_pts==0) return;
 if (colormap_mode) color(YELLOW);
 else cpack(0xffff);
 bgnclosedline();
   for (i=0; i<num_pts; i++) v3f((float *)&path1[i]);
 endclosedline();
 if (colormap_mode) color(MAGENTA);
 else cpack(0xff00ff);
 bgnclosedline();
   for (i=0;i<num_pts;i++) v3f((float *)&path2[i]);
 endclosedline();
}

static void draw_axis(land_t *land, int axis_color, int axis_x, int axis_y,
			int axis_z)
{
 float ver[3];
 frontbuffer(TRUE);
 if (colormap_mode)
	if (axis_color==0xff00) color(GREEN);
	else color(RED); 
 else cpack(axis_color);
 bgnline();
   ver[0]=30.0+axis_x; ver[1]=axis_y; ver[2]=axis_z;
   v3f(ver);
   ver[0]=axis_x;
   v3f(ver);
   ver[1]=30.0+axis_y;
   v3f(ver);
 endline();
 bgnline();
   ver[1]=axis_y;
   v3f(ver);
   ver[2]=30.0+axis_z;
   v3f(ver);
 endline();

 cmovs(29+axis_x,axis_y,axis_z+1);
 charstr("x");
 cmovs(axis_x,29+axis_y,axis_z+1);
 charstr("y");
 cmovs(axis_x+1,axis_y,29+axis_z);
 charstr("z");
 frontbuffer(FALSE);
}

static void draw_viewvec(land_t *land, int axis_x, int axis_y, int axis_z)
{
 Point eye;
 float ver[3];
 eye=land->header->control_pts->eye[land->header->control_pts->num_pts];
 frontbuffer(TRUE);
 if (colormap_mode) color(GREEN);
 else cpack(0xff00);
 bgnpolygon();
     ver[0]=eye.x;   ver[1]=eye.y+1;   ver[2]=eye.z;
     v3f(ver);
     ver[0]=eye.x+1; ver[1]=eye.y-1;
     v3f(ver);
     ver[0]=eye.x-1;
     v3f(ver);
 endpolygon();
 if (colormap_mode) color(RED);
 else cpack(0xff);
 bgnline();
     ver[0]=eye.x;   ver[1]=eye.y;   ver[2]=eye.z;
     v3f(ver);
     ver[0]=axis_x;  ver[1]=axis_y;  ver[2]=axis_z;
     v3f(ver);
 endline();
 frontbuffer(FALSE);
{ int mainwin = winget();
 winset(view_window);
 new_lookat(eye.x,eye.y,eye.z,axis_x,axis_y,axis_z,0);
 draw_quick(land,FALSE);
 winset(mainwin);
}
}


static void draw_points(land_t *land)
{
 int i,num_cpts;
 float ver[3];
 Point *eye,*look;
 eye = land->header->control_pts->eye;
 look = land->header->control_pts->look_at;
 num_cpts = land->header->control_pts->num_pts;
 if (colormap_mode) color(GREEN);
 else cpack(0xff00);
 for(i=0; i<num_cpts; i++) {
    bgnpolygon();
      ver[0] = eye[i].x;   ver[1] = eye[i].y+1; ver[2] = eye[i].z; v3f(ver);
      ver[0] = eye[i].x+1; ver[1] = eye[i].y-1; v3f(ver);
      ver[0] = eye[i].x-1; v3f(ver);
    endpolygon();
    }
 if (colormap_mode) color(RED);
 else cpack(0xff);
 for(i=0;i<num_cpts;i++) {
    bgnpolygon();
      ver[0] = look[i].x;   ver[1] = look[i].y+1; ver[2] = look[i].z; v3f(ver);
      ver[0] = look[i].x+1; ver[1] = look[i].y-1; v3f(ver);
      ver[0] = look[i].x-1; v3f(ver);
    endpolygon();
    bgnline();
      ver[0] = eye[i].x;  ver[1] = eye[i].y;  ver[2] = eye[i].z;  v3f(ver);
      ver[0] = look[i].x; ver[1] = look[i].y; ver[2] = look[i].z; v3f(ver);
    endline();
   }
}

static void draw_all(land_t *land)
{
 draw_quick(land,TRUE);
 frontbuffer(TRUE);
 if (drawpath) draw_path(land);
 draw_points(land);
 frontbuffer(FALSE);
}

#define SPLINE_RES 20

static void generate_curv(land_t *land)
{
 float u,blend1,blend2,blend3,blend4;
 int   i,sub1,sub2,sub3,sub4,num_pts,num_cpts;
 path_t *path = land->header->path, *cpts = land->header->control_pts;
 Point *c_eye,*c_look,*p_eye,*p_look;

 if (path->max_pts <= (cpts->num_pts * SPLINE_RES)) {
    while (path->max_pts <= (cpts->num_pts * SPLINE_RES))
	path->max_pts += PATH_CHUNK_SIZE;
    path->eye = (Point *) realloc(path->eye,path->max_pts);
    path->look_at = (Point *) realloc(path->look_at,path->max_pts);
    }
 num_pts=0;
 num_cpts = cpts->num_pts;
 c_eye = cpts->eye;   c_look = cpts->look_at;
 p_eye = path->eye;   p_look = path->look_at;
 for (i=0; i < num_cpts; i++)
    for (u=0.0; u<1.0; u+=1.0/SPLINE_RES) {  /* SPLINE_RES lines between cp's */
	blend1 = -0.5*u*u*u +     u*u - 0.5*u;
	blend2 =  1.5*u*u*u - 2.5*u*u         + 1.0;
	blend3 = -1.5*u*u*u + 2.0*u*u + 0.5*u;
	blend4 =  0.5*u*u*u - 0.5*u*u;
	sub1 = (i-1) % num_cpts;   sub2 = ( i ) % num_cpts; 
	sub3 = (i+1) % num_cpts;   sub4 = (i+2) % num_cpts;
	p_eye[num_pts].x = (blend1*c_eye[sub1].x + blend2*c_eye[sub2].x +
                           blend3*c_eye[sub3].x + blend4*c_eye[sub4].x);
	p_eye[num_pts].y = (blend1*c_eye[sub1].y + blend2*c_eye[sub2].y +
                           blend3*c_eye[sub3].y + blend4*c_eye[sub4].y);
	p_eye[num_pts].z = (blend1*c_eye[sub1].z + blend2*c_eye[sub2].z +
                             blend3*c_eye[sub3].z + blend4*c_eye[sub4].z);
	p_look[num_pts].x = (blend1*c_look[sub1].x + blend2*c_look[sub2].x +
                           blend3*c_look[sub3].x + blend4*c_look[sub4].x);
	p_look[num_pts].y = (blend1*c_look[sub1].y + blend2*c_look[sub2].y +
                           blend3*c_look[sub3].y + blend4*c_look[sub4].y);
	p_look[num_pts++].z = (blend1*c_look[sub1].z + blend2*c_look[sub2].z +
                             blend3*c_look[sub3].z + blend4*c_look[sub4].z);
	}
 path->num_pts = num_pts;
}

static int convert_coord(disp,max,axis,m,land)
int disp,max,axis,*m;
land_t *land;
{
 int t,res;

 if (axis)  res=land->header->yres;
 else  res=land->header->xres;
 if (disp<0)  disp=0;
 else if (disp>=max)  disp=max-1;
 disp=(res-1)*(float)disp/(float)(max-1);
 *m=disp;
 if (axis)  t=COORD(disp,land->header->yres);
 else  t=COORD(disp,land->header->xres);
 return(t);
}

static int get_point(Point *point,land_t *land,int axis_color,int draw_view)
{
 char  back_str[50];
 int   i,done=0,xorg,xsize,ysize,yorg,pick=0;
 int   mx,my,axis_x,axis_y,axis_z;
 short val;

 strcpy(back_str,"");
 for(i=0;i<49;i++)
   strcat(back_str,"\b"); 
 qdevice(UPARROWKEY);
 qdevice(DOWNARROWKEY);
 qdevice(AKEY);
 qdevice(ZKEY);
 tie(LEFTMOUSE,MOUSEX,MOUSEY);
 qreset();
 while(!pick && !done) {
  getorigin(&xorg,&yorg);
  getsize(&xsize,&ysize);
  axis_x=convert_coord(getvaluator(MOUSEX)-xorg,xsize,0,&mx,land);
  axis_y=convert_coord(getvaluator(MOUSEY)-yorg,ysize,1,&my,land);
  axis_z=land->zmap[mx+my*land->header->xres].z+dz;
  fprintf(stderr,"screen: (%3d,%3d,%3d) data: (%3d,%3d) height: %2.0f%s",
    axis_x,axis_y,axis_z,mx,my,dz,back_str);
  while (qtest())
    switch (qread(&val)) {
      case RIGHTMOUSE: done=1; break;
      case LEFTMOUSE:	pick=1;
			getorigin(&xorg,&yorg);
			getsize(&xsize,&ysize);
			qread(&val);
			axis_x=convert_coord(val-xorg,xsize,0,&mx,land);
			qread(&val);
			axis_y=convert_coord(val-yorg,ysize,1,&my,land);
			axis_z=land->zmap[mx+my*land->header->xres].z+dz;
			break;
      case UPARROWKEY:	dz+=1.0;
			qread(&val);
			break;
      case DOWNARROWKEY: dz-=1.0;
			qread(&val);
			break;
      case AKEY: dz+=10.0;
		 qread(&val);
		 break;
      case ZKEY: dz-=10.0;
		 qread(&val);
		 break;
      }
   draw_all(land);
   draw_axis(land,axis_color,axis_x,axis_y,axis_z);
   if (draw_view) draw_viewvec(land,axis_x,axis_y,axis_z);
   }
 unqdevice(UPARROWKEY);
 unqdevice(DOWNARROWKEY);
 unqdevice(AKEY);
 unqdevice(ZKEY);
 tie(LEFTMOUSE,0,0);
 qreset();
 if (done) qenter(RIGHTMOUSE,1);
 point->x=(Coord)axis_x;
 point->y=(Coord)axis_y;
 point->z=(Coord)axis_z;
 return(!done);
}

static void drop_points(land_t *land)
{
 path_t *cpts = land->header->control_pts;
 int done=0, axis_color=0xff00, draw_view=0;
 while ((get_point(&cpts->eye[cpts->num_pts],land,axis_color,draw_view))
      && (!done)) {
	axis_color=0xff;
	draw_view=1;
	done= !get_point(&cpts->look_at[cpts->num_pts],land,axis_color,
				draw_view);
	cpts->num_pts++;
	if (cpts->num_pts == cpts->max_pts) {
		cpts->max_pts += CPTS_CHUNK_SIZE;
		cpts->eye = (Point *) realloc(cpts->eye,cpts->max_pts);
		cpts->look_at = (Point *) realloc(cpts->look_at,cpts->max_pts);
		}
	draw_view=0;
	axis_color=0xff00;
	}
}

static void process_points(land_t *land, int restrain)
{
 WATCHCURSOR;
 generate_curv(land);
 if (restrain) {
     int bump=0, xres=land->header->xres, yres=land->header->yres, x, y, i;
     path_t *path = land->header->path;
     for (i=0; i<path->num_pts; i++) {
	x=MOUSECOORD(path->eye[i].x,xres);
	y=MOUSECOORD(path->eye[i].y,yres);
	if (path->eye[i].z < MINHEIGHT + land->zmap[x+y*xres].z) {
		path->eye[i].z = MINHEIGHT + land->zmap[x+y*xres].z;
		bump++;
		}
	}
     fprintf(stderr,"Bumped %d points\n",bump);
     }
 write_header(land->header);
 fprintf(stderr,"Points and path written to \"%s\".\n",
		land->header->header_name);
 DEFAULTCURSOR;
}

static void open_view_window()
{
 int mainwin = winget();
 static float tevprops[1] = { TV_NULL };
 prefsize(300,300);
 view_window=winopen("View");
 winconstraints();
 lsetdepth(getgdesc(GD_ZMIN),getgdesc(GD_ZMAX));
 zbuffer(TRUE);
 zclear();
 doublebuffer();
 RGBmode();
 gconfig();
 subpixel(TRUE);
 cpack(0);
 clear();
 swapbuffers();
 clear();
 if (getgdesc(GD_TEXTURE)) {
	tevdef(1,0,tevprops);
	tevbind(TV_ENV0,1);
	}
 mmode(MVIEWING);
 perspective(600,1.0,0.001,100000.0);
 winset(mainwin);
}

int do_flightpath(land)
land_t *land;
{
 int   i,done,bump;
 short dev,val,menuval;
 float tx,ty,tz;

 wintitle("Flightpath");
 init_view();
 if (!view_window) open_view_window();
 if (!rgb_allowed) { doublebuffer(); gconfig(); }
 create_flightpath_menu();
 qdevice(RIGHTMOUSE);
 qdevice(MIDDLEMOUSE);
 qdevice(LEFTMOUSE);

 if (!land->header->control_pts) {
	land->header->control_pts = (path_t *) malloc(sizeof(path_t));
	land->header->control_pts->num_pts = 0;
	land->header->control_pts->max_pts = CPTS_CHUNK_SIZE;
	land->header->control_pts->eye =
		(Point *) malloc(CPTS_CHUNK_SIZE * sizeof(Point));
	land->header->control_pts->look_at =
		(Point *) malloc(CPTS_CHUNK_SIZE * sizeof(Point));
	}
 if (!land->header->path) {
	land->header->path = (path_t *) malloc(sizeof(path_t));
	land->header->path->num_pts = 0;
	land->header->path->max_pts = PATH_CHUNK_SIZE;
	land->header->path->eye =
		(Point *) malloc(PATH_CHUNK_SIZE * sizeof(Point));
	land->header->path->look_at =
		(Point *) malloc(PATH_CHUNK_SIZE * sizeof(Point));
	}

 while (TRUE) {
   draw_all(land);
   dev=qread(&val);
   switch(dev) {
     case REDRAW: draw_all(land); break;
     case RIGHTMOUSE: menuval=dopup(menu);
	   switch (menuval) {
		case SPIN: spin(land,0,draw_all,0,NULL); break;
		case DROP_PTS: drop_points(land); break;
		case PROCESS_PTS: process_points(land,0); break;
		case PROC_RESTR: process_points(land,1); break;
		case SHOW_PATH: drawpath=!drawpath;
				create_flightpath_menu();
				break;
		case QUIT: exit();
		case MFLYBY: return(FLYBY);
		case MLOOK: return(LOOK);
		case RESET: land->header->path->num_pts=0;
			    land->header->control_pts->num_pts=0;
			    break;
		case DELETE: if (land->header->control_pts->num_pts)
				   land->header->control_pts->num_pts--;
			     break;
		}
		break;
     }
 }
}

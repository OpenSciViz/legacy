/*****************************************************************************/
/*                                                                           */
/*                                 dump.c                                    */
/*                                                                           */
/*  These routines read an image from the monitor and write it to a file as  */
/*  as short (2-byte) image with dimensions corresponding to those of the    */
/*  display window.  The dimensions are included as the first line if the    */
/*  dims flag is set to 1.  Images saved using these routines may be         */
/*  displayed using the drop program.                                        */
/*                                                                           */
/*  Dave Pape                                                                */
/*  NASA/GSFC                                                                */
/*  (original routines)                                                      */
/*                                                                           */
/*  Eric Weisstein                                                           */
/*  USRA Summer Graduate Student Program                                     */
/*  NASA/Goddard Space Flight Center                                         */
/*  August 1990                                                              */
/*                                                                           */
/*****************************************************************************/

#include <gl.h>
#include <malloc.h>
#include <stdio.h>

dumpwindow_upsidedown(fp,dims)    /* Image is stored bottom line first */
FILE *fp;
int  dims;
{
  long           xsize,ysize;
  register int   i;
  Colorindex *s;
  unsigned char  *c;

  getsize(&xsize,&ysize);
  if(dims) {
    fprintf(stderr,"size (%ld,%ld)\n",xsize,ysize);
    fprintf(fp,"%ld %ld\n",xsize,ysize);
  }
  if((s=(Colorindex *)malloc(ysize*xsize*sizeof(Colorindex)))==NULL) {
    printf("Error allocating colorindex array.\n");
    exit(1);
  }
  if((c=(unsigned char *)malloc(ysize*xsize*sizeof(unsigned char)))==NULL) {
    printf("Error allocating unsigned char array.\n");
    exit(1);
  }
  rectread(0,0,xsize-1,ysize-1,s);
  for(i=0;i<xsize*ysize;i++)
    c[i]=(unsigned char)s[i];
  fwrite(c,xsize,ysize,fp);
  free(c);
  free(s);
}

dump_2byte(fp,dims)
FILE *fp;
int  dims;
{
  long          xsize,ysize;
  register int  i;
  Colorindex    *s;

  getsize(&xsize,&ysize);
  if(dims) {
    fprintf(stderr,"size (%ld,%ld)\n",xsize,ysize);
    fprintf(fp,"%ld %ld\n",xsize,ysize);
  }
  if((s=(Colorindex *)malloc(ysize*xsize*sizeof(Colorindex)))==NULL) {
    printf("Error allocating colorindex array.\n");
    exit(1);
  }
  rectread(0,0,xsize-1,ysize-1,s);
  for(i=0;i<xsize*ysize;i++)
    s[i]=(unsigned char)s[i];
  fwrite(s,xsize*sizeof(Colorindex),ysize,fp);
  free(s);
}

dumpwindow(fp,dims)
FILE *fp;
int  dims;
{
  long          xsize,ysize;
  register int  i,j;
  Colorindex    *s;
  unsigned char *c;

  getsize(&xsize,&ysize);
  if((s=(Colorindex *)malloc(ysize*xsize*sizeof(Colorindex)))==NULL) {
    printf("Error allocating Colorindex array.\n");
    exit(1);
  }
  if((c=(unsigned char *)malloc(ysize*xsize*sizeof(unsigned char)))==NULL) {
    printf("Error allocating unsigned char array.\n");
    exit(1);
  }
  if(dims) {
    fprintf(stderr,"size (%ld,%ld)\n",xsize,ysize);
    fprintf(fp,"%ld %ld\n",xsize,ysize);
  }
  rectread(0,0,xsize-1,ysize-1,s);
  for(i=0;i<ysize;i++)
    for(j=0;j<xsize;j++)
      c[(ysize-1-i)*xsize+j]=(unsigned char)s[i*xsize+j];
  fwrite(c,xsize,ysize,fp);
  free(c);
  free(s);
}

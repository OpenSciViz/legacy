#define DEFAULT_CM     "/usr/people/dave/fly4/colormaps/gray.cm"
#define DEFAULT_CM_DIR "/usr/people/dave/fly4/colormaps"

#define CM_RESET	3000
#define CM_BLUE		3001
#define CM_CYAN		3002
#define CM_GRAY		3003
#define CM_GRAY2	3004
#define CM_GRAYI	3005
#define CM_GRAYP	3006
#define CM_GREEN	3007
#define CM_LILAC	3008
#define CM_RAINBOW	3009
#define CM_RED		3010
#define CM_VIOLET	3011
#define CM_YELLOW	3012
#define CM_OTHER	3099

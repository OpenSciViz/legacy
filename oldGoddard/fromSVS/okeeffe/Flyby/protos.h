int read_colormap(char *fname);
void reset_colormap(header_t *header);
void set_colormap(header_t *header,int id);
int colormap_menu();

panel_dialog(int mode,header_t *header);

void draw_world(land_t *,int);
void draw_world_inc(land_t *);
void do_lvertex(int i,int j,land_t *land);
void do_tvertex(int i,int j,land_t *land);
void draw_quick(land_t *,int);
void init_view();
void setup_window();
void define_texture(int id,int xres,int yres,unsigned long *tmap);

dump_2byte(FILE *fp,int dims);
dumpwindow(FILE *fp,int dims);

int do_flightpath(land_t *land);

int do_flyby(land_t *land);
dump(int flag,char *prompt,char *suff);
draw_it(land_t *land);
create_flyby_menu();

get_text(char *name,char *prompt);

void setup_lights(header_t *header);
void light_dialog(land_t *land);

void create_menu(land_t *land);
spin(land_t *land,int index,void (*drawfn)(),int arg,void (*inc_drawfn)());
stretch(land_t *land);
draw_and_time(land_t *land);
int do_look(land_t *land);

new_lookat(float vx,float vy,float vz,float tx,float ty,float tz,int twist);

void adjust_z_scale(land_t *land);

void compute_normals(land_t *land);

header_t *read_header(char *filename);
header_t *read_old_header(char *filename);
land_t *read_datafiles(header_t *header);
void read_elv(FILE *infile,land_t *land);
void read_tex(FILE *infile,land_t *land);
void read_elv_file(header_t *header, int i, land_t *land);
void read_tex_file(header_t *header, int i, land_t *land);
void split_name(char *name, char *prefix, char **suffix);

void write_header(header_t *header);

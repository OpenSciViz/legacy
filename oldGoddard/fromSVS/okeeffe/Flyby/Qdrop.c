#include <gl.h>
#include <device.h>
#include <stdio.h>
#include <fcntl.h>

main(argc,argv)
int argc;
char *argv[];
{
 int i,vgx,xdim=512,ydim=512,size;
 long *data;
 char ver[80];
 if (argc<5) {
	fprintf(stderr,"Usage: %s quad1 quad2 quad3 quad4\n",argv[0]);
	exit();
	}
 foreground();
 prefposition(0,xdim*2,0,ydim*2);
 noborder();
 winopen(argv[0]);
 gconfig();
 color(BLACK); clear();
 for (i=0; i<256; i++) mapcolor(i+256,i,i,i);
 size = xdim * ydim;
 data = (long *) malloc(size);
 xdim--; ydim--;
 gversion(ver);
 vgx = (strncmp(ver,"GL4DVGX",7) == 0);
 if (vgx) {
	pixmode(PM_SIZE,8);
	pixmode(PM_ADD24,256);
	}
 read_data(argv[1],data,size);
 if (vgx) lrectwrite(xdim+1,ydim+1,xdim*2+1,ydim*2+1,data);
 else do_rectwrite(xdim+1,ydim+1,xdim*2+1,ydim*2+1,data,size);
 read_data(argv[2],data,size);
 if (vgx) lrectwrite(0,ydim+1,xdim,ydim*2+1,data);
 else do_rectwrite(0,ydim+1,xdim,ydim*2+1,data,size);
 read_data(argv[3],data,size);
 if (vgx) lrectwrite(0,0,xdim,ydim,data);
 else do_rectwrite(0,0,xdim,ydim,data,size);
 read_data(argv[4],data,size);
 if (vgx) lrectwrite(xdim+1,0,xdim*2+1,ydim,data);
 else do_rectwrite(xdim+1,0,xdim*2+1,ydim,data,size);
 sleep(60);
}

read_data(fname,data,size)
char *fname;
long *data;
int size;
{
 int fd;
 if ((fd = open(fname,O_RDONLY)) == -1) {
	fprintf(stderr,"Failed to open %s\n",fname);
	exit();
	}
 read(fd,data,size);
 close(fd);
}

do_rectwrite(x1,x2,y1,y2,data,size)
int x1,x2,y1,y2;
unsigned char *data;
{
 short *sdata;
 int i;
 sdata = (short *) malloc(size*sizeof(short));
 for (i=0; i<size; i++)
	sdata[i] = data[i]+256;
 rectwrite(x1,x2,y1,y2,sdata);
}

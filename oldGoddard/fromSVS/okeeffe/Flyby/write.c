#include "externs.h"

void write_header(header)
header_t *header;
{
 char fname[80];
 FILE *fp;
 int i;
 light_t *light;
 path_t *path;
 Point *eye,*look_at;

 strcpy(fname,header->header_name);
 suffix(fname,".hdr");
 if ((fp=fopen(fname,"w"))==NULL) {
	fprintf(stderr,"Cannot open header file %s for writing\n",fname);
	exit(1); 
	}
 fprintf(fp,"dimension(%d,%d)\n",header->xdim,header->ydim);
 fprintf(fp,"resolution(%d,%d)\n",header->xres,header->yres);
 fprintf(fp,"zmap(%s,%f,%d)\n",header->z_name,header->z_scale,header->z_count);
 if (header->tex_type == TT_GREY)
	fprintf(fp,"texmap(%s,%d)\n",header->tex_name,header->tex_count);
 else if (header->tex_type == TT_RGB)
	fprintf(fp,"rgbtexmap(%s,%s,%s,%d)\n",header->tex_rname,
		header->tex_gname,header->tex_bname,header->tex_count);
 if (*header->cmap_name) fprintf(fp,"colormap(%s)\n",header->cmap_name);
 switch (header->draw_mode) {
	case DM_TEXMAP: fprintf(fp,"use(texmap)\n"); break;
	case DM_LIGHT: fprintf(fp,"use(light)\n"); break;
	case DM_COLORMAP: fprintf(fp,"use(colormap)\n"); break;
	}
 for (light=header->light; light; light=light->next)
	fprintf(fp,"light(%s, %f,%f,%f, %d, %f,%f,%f)\n",light->name,light->x,
	     light->y,light->z,light->on,light->red,light->green,light->blue);
 if ((path=header->control_pts)!=NULL) {
     fprintf(fp,"controlpts(%d,\n",path->num_pts);
     eye = path->eye; look_at = path->look_at;
     for (i=0; i < path->num_pts-1; i++)
	fprintf(fp,"  %f,%f,%f, %f,%f,%f,\n", eye[i].x,eye[i].y,eye[i].z,
		look_at[i].x,look_at[i].y,look_at[i].z);
     fprintf(fp,"  %f,%f,%f, %f,%f,%f)\n", eye[i].x,eye[i].y,eye[i].z,
		look_at[i].x,look_at[i].y,look_at[i].z);
     }
 if ((path=header->path)!=NULL) {
     fprintf(fp,"path(%d,\n",path->num_pts);
     eye = path->eye; look_at = path->look_at;
     for (i=0; i < path->num_pts-1; i++)
	fprintf(fp,"  %f,%f,%f, %f,%f,%f,\n", eye[i].x,eye[i].y,eye[i].z,
		look_at[i].x,look_at[i].y,look_at[i].z);
     fprintf(fp,"  %f,%f,%f, %f,%f,%f)\n", eye[i].x,eye[i].y,eye[i].z,
		look_at[i].x,look_at[i].y,look_at[i].z);
     }
 fclose(fp);
 fprintf(stderr,"File \"%s\" written.\n",fname);
}

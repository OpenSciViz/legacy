#include "externs.h"

Matrix trans_mat, rot_mat, scale_mat;

static int texture_mapping;
static int max_z;
static int inc_row = 0;

void draw_world_inc(land_t *land)
{
 int i,maxi;
 static Point *elv;
 static unsigned long *tex;
 static norm_t *norm;
 if (inc_row == land->header->yres-1) return;
 if ((land->header->draw_mode==DM_LIGHT) && (!land->norm)) return;
 if (!inc_row) {
	reshapeviewport();
	czclear(0,max_z);
	loadmatrix(trans_mat);
	multmatrix(rot_mat);
	multmatrix(scale_mat);
	elv = land->zmap;
	norm = land->norm;
	tex = land->tmap;
	}
 maxi = land->header->xres;
 if (land->header->draw_mode==DM_LIGHT) {
	lmbind(MATERIAL,1);
	bgntmesh();
	for (i=0; i<maxi; i++, norm++, elv++) {
		n3f((float *)norm);
		v3f((float *)elv);
		n3f((float *)(norm+maxi));
		v3f((float *)(elv+maxi));
		}
	endtmesh();
	lmbind(MATERIAL,0);
	}
 else {
	bgntmesh();
	for (i=0; i<maxi; i++, tex++, elv++) {
		cpack(*tex);
		v3f((float *)elv);
		cpack(*(tex+maxi));
		v3f((float *)(elv+maxi));
		}
	endtmesh();
	}
 if (++inc_row == land->header->yres-1) swapbuffers();
}

void draw_world(land_t *land,int use_transform)
{
 int i,j,maxi,maxj;
 Point *elv;
 unsigned long *tex;
 norm_t *norm;
static int pass=0;
 inc_row = 0;
 reshapeviewport();
/*
sleep(5);
switch (pass) {
	case 0: viewport(-1024,1023,-1024,1023); pass++; break;
	case 1: viewport(0,2047,-1024,1023); pass++; break;
	case 2: viewport(0,2047,0,2047); pass++; break;
	case 3: viewport(-1024,1023,0,2047); pass=0; break;
	}
*/
 czclear(0xff8000,max_z);
 if (use_transform) {
	loadmatrix(trans_mat);
	multmatrix(rot_mat);
	multmatrix(scale_mat);
	}
 elv = land->zmap;
 maxj = land->header->yres-1;
 maxi = land->header->xres;
 if (land->header->draw_mode==DM_LIGHT) {
	if (!land->norm) compute_normals(land);
	lmbind(MATERIAL,1);
	for (j=0, norm=land->norm; j<maxj; j++) {
	   bgntmesh();
	   for (i=0; i<maxi; i++, norm++, elv++) {
		n3f((float *)norm);
		v3f((float *)elv);
		n3f((float *)(norm+maxi));
		v3f((float *)(elv+maxi));
		}
	   endtmesh();
	   }
	lmbind(MATERIAL,0);
	}
 else {
	for (j=0, tex=land->tmap; j<maxj; j++) {
	   bgntmesh();
	   for (i=0; i<maxi; i++, tex++, elv++) {
		cpack(*tex);
		v3f((float *)elv);
		cpack(*(tex+maxi));
		v3f((float *)(elv+maxi));
		}
	   endtmesh();
	   }
	}
 swapbuffers();
}

static void draw_quick_lighted(land)
land_t *land;
{
 int i,j,maxi,maxj;
 norm_t *norm;
 Point *elv;
 if (!land->lores_norm)
	compute_lores_normals(land);
 lmbind(MATERIAL,1);
 norm = land->lores_norm;
 elv = land->lores_zmap;
 maxi = land->header->lores_xres;
 maxj = land->header->lores_yres - 1;
 for (j=0; j < maxj; j++) {
	bgntmesh();
	for (i=0; i < maxi; i++, norm++, elv++) {
		n3f((float *)norm);
		v3f((float *)elv);
		n3f((float *)(norm+maxi));
		v3f((float *)(elv+maxi));
		}
	endtmesh();
	}
 lmbind(MATERIAL,0);
}

static void draw_quick_textured(land)
land_t *land;
{
 int i,j,maxi,maxj;
 float t[2],dtx,dty,ty;
 Point *elv;
 cpack(0xffffff);
 texbind(TX_TEXTURE_0,land->texmap_id);
 dtx = 1.0 / (float)land->header->lores_xres;
 dty = 1.0 / (float)land->header->lores_yres;
 elv = land->lores_zmap;
 maxi = land->header->lores_xres;
 maxj = land->header->lores_yres - 1;
 for (j=0, ty=0.0; j < maxj; j++, ty+=dty) {
	bgntmesh();
	for (i=0, t[0]=0.0; i < maxi; i++, elv++, t[0]+=dtx) {
		t[1]=ty;
		t2f(t);
		v3f((float *)elv);
		t[1]+=dty;
		t2f(t);
		v3f((float *)(elv+maxi));
		}
	endtmesh();
	}
 texbind(TX_TEXTURE_0,0);
}

static void draw_quick_wireframe(land)
land_t *land;
{
 int i,j;
 Point *elv;
 cpack(0xffffff);
 for (j=0, elv=land->lores_zmap; j < land->header->lores_yres; j++) {
    bgnline();
    for (i=0; i < land->header->lores_xres; i++, elv++)
	v3f((float *) elv);
    endline();
    }
 for (i=0; i < land->header->lores_xres; i++) {
    bgnline();
    elv = land->lores_zmap+i;
    for (j=0; j < land->header->lores_yres; j++, elv+=land->header->lores_xres)
	v3f((float *) elv);
    endline();
    }
}

void draw_quick(land_t *land,int use_transform)
{
 inc_row = 0;
 reshapeviewport();
 czclear(0,max_z);
 if (use_transform) {
	loadmatrix(trans_mat);
	multmatrix(rot_mat);
	multmatrix(scale_mat);
	}
/* For now, assume no texture mapping == (not VGX) == too slow for polygons */
 if (land->header->draw_mode==DM_LIGHT) draw_quick_lighted(land);
 else if (!texture_mapping) draw_quick_wireframe(land);
 else draw_quick_textured(land);
 swapbuffers();
}

static Matrix idmat = { 1.0, 0.0, 0.0, 0.0,
			0.0, 1.0, 0.0, 0.0,
			0.0, 0.0, 1.0, 0.0,
			0.0, 0.0, 0.0, 1.0 };

void init_view()
{
 mmode(MVIEWING);
 loadmatrix(idmat);
 getmatrix(scale_mat);
 getmatrix(rot_mat);
 translate(0.0,0.0,-200.0);
 getmatrix(trans_mat);
 perspective(600,1.0,0.1,1000.0);
 lookat(0.0,0.0,200.0,0.0,0.0,0.0,0);
}

void setup_window()
{
 long planes;
 static float tevprops[1] = { TV_NULL };

 foreground();
 prefsize(512,512);
prefposition(0,1023,0,1023);
 winopen("");
 winconstraints();
 icontitle("flyby");
 lsetdepth(getgdesc(GD_ZMIN),getgdesc(GD_ZMAX));
 zbuffer(TRUE);
 zclear();
 doublebuffer();
 RGBmode();
 gconfig();
 fprintf(stderr,"\n");
 fprintf(stderr,"%ld doublebuffered bitplanes available\n",planes=getplanes());
 if (planes<12 || colormap_mode) {
   if (!colormap_mode) {
     printf("Insufficient bitplanes; using color map mode.\n");
     colormap_mode=1;
     rgb_allowed=0;
     }
   cmode();
   gconfig();
   if (planes<8) {
     fprintf(stderr,"Insufficient bitplanes for doublebuffered colormap.\n");
     fprintf(stderr,"Flyby will alternate modes as appropriate.\n");
     fprintf(stderr,"NOTE:       This will result in ugly rendered flybys.\n");
     }
   }
 if (getzbuffer()==FALSE) {
   fprintf(stderr,"WARNING:    This machine has no Z buffer!\n");
   fprintf(stderr,"            Hidden surface removal cannot be performed!\n");
   ringbell();
   }
 if (texture_mapping = getgdesc(GD_TEXTURE)) {
	tevdef(1,0,tevprops);
	tevbind(TV_ENV0,1);
	}
 max_z = getgdesc(GD_ZMAX);
 subpixel(TRUE);
/* afunction(0,AF_NOTEQUAL); */
 cpack(0);
 clear();
 swapbuffers();
 clear();
}

void define_texture(texid,xres,yres,tmap)
int texid,xres,yres;
unsigned long *tmap;
{
 static float texprops[5] = { TX_MINFILTER, TX_POINT, TX_MAGFILTER, TX_POINT,
                                TX_NULL };
 if (texture_mapping)
       	texdef2d(texid,4,xres,yres,tmap,0,texprops);
}

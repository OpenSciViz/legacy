#include <stdio.h>
#include <fcntl.h>

main(argc,argv)
int argc;
char *argv[];
{
 int i,vgx,xdim=1024,ydim=1024;
 int fd1,fd2,fd3,fd4,ofd;
 unsigned char *data;
 if (argc<6) {
	fprintf(stderr,"Usage: %s quad1 quad2 quad3 quad4 output [x y]\n",argv[0]);
	exit();
	}
 if (argc>6) xdim = atoi(argv[6]);
 if (argc>7) ydim = atoi(argv[7]);
 data = (unsigned char *) malloc(2*xdim);
 if ((ofd = creat(argv[5],0644)) == -1) {
	fprintf(stderr,"Failed to open %s\n",argv[5]);
	exit();
	}
 if ((fd1 = open(argv[1],O_RDONLY)) == -1) {
	fprintf(stderr,"Failed to open %s\n",argv[1]);
	exit();
	}
 if ((fd2 = open(argv[2],O_RDONLY)) == -1) {
	fprintf(stderr,"Failed to open %s\n",argv[2]);
	exit();
	}
 for (i=0; i<ydim; i++) {
	if (read(fd2,data,xdim) < xdim) {
		fprintf(stderr,"Quadrant 2 short of data\n");
		exit();
		}
	if (read(fd1,data+xdim,xdim) < xdim) {
		fprintf(stderr,"Quadrant 1 short of data\n");
		exit();
		}
	write(ofd,data,xdim*2);
	}
 close(fd1);
 close(fd2);
 if ((fd3 = open(argv[3],O_RDONLY)) == -1) {
	fprintf(stderr,"Failed to open %s\n",argv[3]);
	exit();
	}
 if ((fd4 = open(argv[4],O_RDONLY)) == -1) {
	fprintf(stderr,"Failed to open %s\n",argv[4]);
	exit();
	}
 for (i=0; i<ydim; i++) {
	if (read(fd3,data,xdim) < xdim) {
		fprintf(stderr,"Quadrant 3 short of data\n");
		exit();
		}
	if (read(fd4,data+xdim,xdim) < xdim) {
		fprintf(stderr,"Quadrant 4 short of data\n");
		exit();
		}
	write(ofd,data,xdim*2);
	}
 close(fd3);
 close(fd4);
}

/*****************************************************************************/
/*                                                                           */
/*                                input.c                                    */
/*                                                                           */
/*  This routine pops up a window which allows the user to enter text into   */
/*  a pseudo-edit field.  It is based on a routine from the iman program     */
/*  written by Jim Braatz.                                                   */
/*                                                                           */
/*  Jim Braatz                                                               */
/*  NASA/GSFC                                                                */
/*  (original routine)                                                       */
/*                                                                           */
/*  Eric Weisstein                                                           */
/*  USRA Summer Graduate Student Program                                     */
/*  NASA/Goddard Space Flight Center                                         */
/*  August 1990                                                              */
/*                                                                           */
/*****************************************************************************/

#include "input.h"
#include <psio.h>
#include <stdio.h>

get_text(name,prompt)
char name[80],prompt[];
{
 char string[100];

 if(!ps_open_PostScript())
    fprintf(stderr,"Unable to connect to NeWS server\n");
 ps_input_wind(name,prompt);
 while(!psio_eof(PostScriptInput)) {
   if(ps_input(name));
   else if(ps_close_input())
     return;
 }
}

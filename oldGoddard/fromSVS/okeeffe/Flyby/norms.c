/*****************************************************************************/
/*                                                                           */
/*                                  norms.c                                  */
/*                                                                           */
/*  These routines compute normals to the flyby surface.  The normals are    */
/*  needed if the user is using lights to illuminate the surface, rather     */
/*  than a texture map.                                                      */
/*                                                                           */
/*  Dave Pape                                                                */
/*  NASA/GSFC                                                                */
/*  May 1989                                                                 */
/*                                                                           */
/*  Eric Weisstein                                                           */
/*  USRA Summer Graduate Student Program                                     */
/*  NASA/Goddard Space Flight Center                                         */
/*  August 1990                                                              */
/*                                                                           */
/*****************************************************************************/

#include <gl.h>
#include <math.h>
#include "externs.h"
#include "flyby.h"

static void get_norm(float x1,float y1,float z1,float x3,float y3,float z3,
	float x2,float y2,float z2,float *n)
{
 float z12,z23,z31,len;
 z31=z3-z1;
 z23=z2-z3;
 z12=z1-z2;
 n[0]= y1*z23+y2*z31+y3*z12;
 n[1]= -x1*z23-x2*z31-x3*z12;
 n[2]= x1*(y2-y3)+x2*(y3-y1)+x3*(y1-y2);
 len=sqrt(n[0]*n[0]+n[1]*n[1]+n[2]*n[2]);
 n[0]/=len;
 n[1]/=len;
 n[2]/=len;
}

#include <sys/time.h>

void compute_normals(land)
land_t *land;
{
 int i,j,xres,yres;
 norm_t *n1,*n2,*n3,*n4,*norm;
 Point *p1,*p2,*p3,*p4;
 float n[3],len;
struct timeval start_time,end_time;
float delta_time;
gettimeofday(&start_time,NULL);
 fprintf(stderr,"Computing normals...\n"); 

 if (land->norm) free(land->norm);
 xres = land->header->xres;
 yres = land->header->yres;
 land->norm=(norm_t *) malloc(xres * yres * sizeof(norm_t));
 for (j=0, norm=land->norm; j < yres; j++)
    for (i=0; i < xres; i++, norm++)
	norm->nx = norm->ny = norm->nz = 0.0;
 norm = land->norm;
 p1 = land->zmap;	n1 = land->norm;
 p2 = p1+1;		n2 = land->norm+1;
 p3 = p1+xres;		n3 = land->norm+xres;
 p4 = p1+xres+1;	n4 = land->norm+xres+1;
 for (j=0; j < yres-1; j++)
    for (i=0; i < xres-1; i++, p1++,p2++,p3++,p4++, n1++,n2++,n3++,n4++) {
	get_norm(p1->x,p1->y,p1->z, p3->x,p3->y,p3->z, p2->x,p2->y,p2->z, n);
	n1->nx += n[0];   n1->ny += n[1];   n1->nz += n[2];
	n2->nx += n[0];   n2->ny += n[1];   n2->nz += n[2];
	n3->nx += n[0];   n3->ny += n[1];   n3->nz += n[2];
	get_norm(p4->x,p4->y,p4->z, p2->x,p2->y,p2->z, p3->x,p3->y,p3->z, n);
	n4->nx += n[0];   n4->ny += n[1];   n4->nz += n[2];
	n2->nx += n[0];   n2->ny += n[1];   n2->nz += n[2];
	n3->nx += n[0];   n3->ny += n[1];   n3->nz += n[2];
	}
 for (j=0, norm=land->norm; j < yres; j++)
    for (i=0; i < xres; i++, norm++) {
	len = sqrt(norm->nx*norm->nx + norm->ny*norm->ny + norm->nz*norm->nz);
	norm->nx /= len;
	norm->ny /= len;
	norm->nz /= len;
	}
 gettimeofday(&end_time,NULL);
 delta_time=(float)(end_time.tv_sec-start_time.tv_sec)
                + (float)(end_time.tv_usec - start_time.tv_usec)/1000000.0;
 fprintf(stderr,"Normals computed.   (%f sec)\n",delta_time);
}

void compute_lores_normals(land)
land_t *land;
{
 int i,j,xres,yres;
 norm_t *n1,*n2,*n3,*n4,*norm;
 Point *p1,*p2,*p3,*p4;
 float n[3],len;
 if (land->lores_norm) free(land->lores_norm);
 xres = land->header->lores_xres;
 yres = land->header->lores_yres;
 land->lores_norm=(norm_t *) malloc(xres * yres * sizeof(norm_t));
 for (j=0, norm=land->lores_norm; j < yres; j++)
    for (i=0; i < xres; i++, norm++)
	norm->nx = norm->ny = norm->nz = 0.0;
 norm = land->lores_norm;
 p1 = land->lores_zmap;	n1 = land->lores_norm;
 p2 = p1+1;		n2 = n1+1;
 p3 = p1+xres;		n3 = n1+xres;
 p4 = p1+xres+1;	n4 = n1+xres+1;
 for (j=0; j < yres-1; j++)
    for (i=0; i < xres-1; i++, p1++,p2++,p3++,p4++, n1++,n2++,n3++,n4++) {
	get_norm(p1->x,p1->y,p1->z, p3->x,p3->y,p3->z, p2->x,p2->y,p2->z, n);
	n1->nx += n[0];   n1->ny += n[1];   n1->nz += n[2];
	n2->nx += n[0];   n2->ny += n[1];   n2->nz += n[2];
	n3->nx += n[0];   n3->ny += n[1];   n3->nz += n[2];
	get_norm(p4->x,p4->y,p4->z, p2->x,p2->y,p2->z, p3->x,p3->y,p3->z, n);
	n4->nx += n[0];   n4->ny += n[1];   n4->nz += n[2];
	n2->nx += n[0];   n2->ny += n[1];   n2->nz += n[2];
	n3->nx += n[0];   n3->ny += n[1];   n3->nz += n[2];
	}
 for (j=0, norm=land->lores_norm; j < yres; j++)
    for (i=0; i < xres; i++, norm++) {
	len = sqrt(norm->nx*norm->nx + norm->ny*norm->ny + norm->nz*norm->nz);
	norm->nx /= len;
	norm->ny /= len;
	norm->nz /= len;
	}
}

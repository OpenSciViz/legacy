#include "externs.h"
#include <gl/device.h>

extern Matrix trans_mat, rot_mat, scale_mat;

static Matrix idmat = { 1.0, 0.0, 0.0, 0.0,
			0.0, 1.0, 0.0, 0.0,
			0.0, 0.0, 1.0, 0.0,
			0.0, 0.0, 0.0, 1.0 };

spin(land_t *land,int index,void (*drawfn)(),int arg,void (*inc_drawfn)())
{
 short val,dev;
 int prevx,prevy,dx,dy,curx,cury;

 ARROWCURSOR;
 if (!rgb_allowed) { doublebuffer(); gconfig(); }
 qdevice(MOUSEX);
 qdevice(MOUSEY);
 prevx = getvaluator(MOUSEX);
 prevy = getvaluator(MOUSEY);
 while (1) {
   if (qtest()) {
	dev=qread(&val);
	if (getbutton(RIGHTMOUSE)) break;
	if ((dev == MOUSEX) || (dev == MOUSEY)) {
		curx = getvaluator(MOUSEX);
		dx = curx - prevx;
		if ((dx>5) || (dx<-5)) prevx = curx;
		else dx=0;
		cury = getvaluator(MOUSEY);
		dy = cury - prevy;
		if ((dy>5) || (dy<-5)) prevy = cury;
		else dy=0;
		if ((!dx) && (!dy)) continue;
		if (getbutton(LEFTMOUSE)) {
			if (getbutton(MIDDLEMOUSE)) ztrans_world(dx,dy);
			else trans_world(dx,dy);
			(*drawfn)(&land[index],arg);
			}
		else if (getbutton(MIDDLEMOUSE)) {
			rotate_world(dx,dy);
			(*drawfn)(&land[index],arg);
			}
		}
	}
    else if (inc_drawfn) (*inc_drawfn)(&land[index]);
    }
 unqdevice(MOUSEX);
 unqdevice(MOUSEY);
 qreset();
 qenter(RIGHTMOUSE,1);
 DEFAULTCURSOR;
}

stretch(land_t *land)
{
 short val,dev;
 int prevy,dy,cury;

 ARROWCURSOR;
 if (!rgb_allowed) { doublebuffer(); gconfig(); }
 qdevice(MOUSEY);
 prevy = getvaluator(MOUSEY);
 noise(MOUSEY,10);
 while (1) {
   if (qtest()) {
	dev=qread(&val);
	if (getbutton(RIGHTMOUSE)) break;
	if (dev == MOUSEY) {
		cury = getvaluator(MOUSEY);
		dy = cury - prevy;
		prevy = cury;
		if (getbutton(LEFTMOUSE)) {
			stretch_world(dy);
			draw_quick(land,TRUE);
			}
		}
	}
    else draw_world_inc(land);
    }
 unqdevice(MOUSEY);
 qreset();
 qenter(RIGHTMOUSE,1);
 DEFAULTCURSOR;
}

stretch_world(dy)
int dy;
{
 float dz;
 dz = 1.0 + (float)dy/100.0;
 loadmatrix(idmat);
 scale(1.0,1.0,dz);
 multmatrix(scale_mat);
 getmatrix(scale_mat);
}

ztrans_world(dx,dy)
int dx,dy;
{
 Coord dz;
 dz = (float)dy;
 loadmatrix(idmat);
 translate(0.0,0.0,dz);
 multmatrix(trans_mat);
 getmatrix(trans_mat);
}

trans_world(dx,dy)
int dx,dy;
{
 Coord tx,ty;
 tx = (float)dx;
 ty = (float)dy;
 loadmatrix(idmat);
 translate(dx,dy,0.0);
 multmatrix(trans_mat);
 getmatrix(trans_mat);
}

rotate_world(dx,dy)
int dx,dy;
{
 loadmatrix(idmat);
 rotate(-dy*5,'x');
 rotate(dx*5,'y');
 multmatrix(rot_mat);
 getmatrix(rot_mat);
}

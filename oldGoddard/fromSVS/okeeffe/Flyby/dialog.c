/*****************************************************************************/
/*                                                                           */
/*                                dialog.c                                   */
/*                                                                           */
/*  These routines set up and open a NeWS window to prompt the user for      */
/*  parameters needed by the flyby program.  The PostScript code for         */
/*  creating the window is contained in the file panel.h, a cps-processed    */
/*  c-includable header of the PostScript code panel.cps.  The tags method   */
/*  is used to pass information back and forth.  For further (although       */
/*  almost incomprehensible) description of this method, see Ch. 8: "Mixing  */
/*  PostScript and C" of the IRIS-4D Series "4Sight Programmer's Guide."     */
/*                                                                           */
/*  Eric Weisstein                                                           */
/*  USRA Summer Graduate Student Program                                     */
/*  NASA/Goddard Space Flight Center                                         */
/*  August 1990                                                              */
/*                                                                           */
/*****************************************************************************/

#include "externs.h"
#include <ctype.h>
#include <string.h>
#include "panel.h"
#include "psio.h"

static int crop=0;  /* dummy variable */

panel_dialog(int mode,header_t *header)
{
  char itemtext[80],itemlabel[80];
  char xdimstr[10],ydimstr[10],xresstr[10],yresstr[10];
  char cropstr[20],bstr[25],zstr[20]; 
  int  i,j,cancel;
  int  itemval,read_flag=0;

  char  z_name_temp[80],tex_name_temp[80];
  int   xdim_temp=header->xdim, ydim_temp=header->ydim,
	xres_temp=header->xres, yres_temp=header->yres;
  int   crop_temp=crop;
  float bscale_temp=1.0,z_scale_temp=header->z_scale;
  char  header_name_temp[80];
  int   draw_mode_temp=header->draw_mode,
	header_file_temp=1;

  strcpy(z_name_temp,header->z_name);
  strcpy(tex_name_temp,header->tex_name);
  strcpy(header_name_temp,header->header_name);

  sprintf(xdimstr,"%d",header->xdim);
  sprintf(ydimstr,"%d",header->ydim);
  sprintf(xresstr,"%d",header->xres);
  sprintf(yresstr,"%d",header->yres);
  sprintf(cropstr,"%d",crop);
  sprintf(bstr,"%f",bscale_temp);
  sprintf(zstr,"%f",header->z_scale);
  if(ps_open_PostScript()==0)
    fprintf(stderr,"Unable to connect to NeWS server\n");
  ps_icon_wind(mode,
    header->z_name,header->tex_name,
    xdimstr,ydimstr,
    xresstr,yresstr,
    header->draw_mode,header_file_temp,
    cropstr,bstr,zstr,
    header->header_name);
  while(!psio_eof(PostScriptInput)) {
    if(ps_character_typed(itemtext,itemlabel)) {
      if(!strcmp(itemlabel,"Elevation File: ")) {
        strcpy(z_name_temp,itemtext);
        read_flag=1;
      }
      if(!strcmp(itemlabel,"Texture File: ")) {
        strcpy(tex_name_temp,itemtext);
        read_flag=1;
      } 

      if(!strcmp(itemlabel,"X Dimension: ")) 
        xdim_temp=atoi(itemtext);
      if(!strcmp(itemlabel,"Y Dimension: ")) 
        ydim_temp=atoi(itemtext);
      
      if(!strcmp(itemlabel,"X Resolution: ")) {
        xres_temp=atoi(itemtext); 
        read_flag=1;
      }
      if(!strcmp(itemlabel,"Y Resolution: ")) { 
        yres_temp=atoi(itemtext);
        read_flag=1;
      }
      if(!strcmp(itemlabel,"Cropping: "))    
        crop_temp=atoi(itemtext);
      if(!strcmp(itemlabel,"Brightness Scale: ")) {
        sscanf(itemtext,"%f",&bscale_temp);
         read_flag=1;
      }
      if(!strcmp(itemlabel,"Z Scale: ")) {
        sscanf(itemtext,"%f",&z_scale_temp); 
         read_flag=1;
      }
      if(!strcmp(itemlabel,"Flyby File: ")) { 
        strcpy(header_name_temp,itemtext);
      }
    }
    else if(ps_notify(itemlabel,&itemval)) {
      if (!strcmp(itemlabel,"Lighting Model: ")) {
	if (draw_mode_temp==DM_LIGHT) draw_mode_temp=DM_TEXMAP;
	else draw_mode_temp=DM_LIGHT;
	}
      else if (!strcmp(itemlabel,"Flyby File Type: "))
        header_file_temp=!header_file_temp;
    }
    else if(ps_close_window(&cancel)) {
      if(!cancel) {
        strcpy(header->z_name,z_name_temp);
        strcpy(header->tex_name,tex_name_temp);
        header->xdim=xdim_temp;
        header->ydim=ydim_temp;
        header->xres=xres_temp;
        header->yres=yres_temp;
        crop=crop_temp;
        header->z_scale=z_scale_temp;
        strcpy(header->header_name,header_name_temp);
        header->draw_mode=draw_mode_temp;
        if (!mode)
          read_flag=1;      /* read data even if no typed changes */
      }
      else
        read_flag=0;
      return(read_flag);
    }
  }
  return(read_flag);
}

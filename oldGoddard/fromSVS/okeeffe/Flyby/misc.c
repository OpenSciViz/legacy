/*****************************************************************************/
/*                                                                           */
/*                                  misc.c                                   */
/*                                                                           */
/*  These are miscellaneous routines used by the flyby program.              */ 
/*                                                                           */
/*  Eric Weisstein                                                           */
/*  USRA Summer Graduate Student Program                                     */
/*  NASA/Goddard Space Flight Center                                         */
/*  August 1990                                                              */
/*                                                                           */
/*****************************************************************************/

#include <stdio.h>
#include <string.h>

arrow(str,loc)      /* put an arrow at position loc in string str */
char *str;
int loc;
{
  str[loc]='-';
  str[loc+1]='>';
}

suffix(str,suf)       /* check to see if suffix is present; add it if not */
char *str,*suf;
{
  char *temp;
  if((temp=strrchr(str,'.'))==NULL || strcmp(temp,suf))
    strcat(str,suf);
}

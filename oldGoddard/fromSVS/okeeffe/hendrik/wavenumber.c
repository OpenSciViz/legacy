/* there are 26 frequencies in the input spectrum, the lowest is 1.1/100 Hz,
each successive frequency is 1.1 times the previous */

#include <math.h>
#include "nrutil.h"
#define N 512
#define PI 3.1415926

wavenumber(sp_orig,kx,ky,sp)
float sp_orig[24][26];
float kx[512], ky[512];
float sp[512][512];
{
  float freq[26], phi[24], del_phi=15.0/57.3;
  float **ya, **y2a, fmax, phmax, fx, fy, f, p;
  float sqrt_G, sqrt_2PI; 
  int i,j,fd;
  char outfile[80];

  sqrt_G = sqrt(981.5);
  sqrt_2PI = sqrt( 2*PI );
  sprintf(outfile,"spect512.t0"); 
 
  ya = matrix(1,24,1,26);
  y2a = matrix(1,24,1,26);
  
  freq[0] = 1.1 / 100.0;
  for( i = 1; i < 26; i++ )
    freq[i] = 1.1 * freq[i-1];

  fmax = freq[25];
  
  phi[0] = 0.0;
  for( i = 1; i < 24; i++ )
    phi[i] = i * del_phi;

  phmax = phi[23];

/* it may be necessary to define the spectrum by multiplying the input by the
   frequency: */
  for( i = 1; i <= 24; i++ )
    for( j = 1; j <= 26; j++ )
      ya[i][j] = sp_orig[i-1][j-1]; /* * freq[j-1]; */

  splie2(phi-1,freq-1,ya,24,26,y2a);

  for( i = 0; i < 512; i++ )
  {
    fx = sqrt(fabs(kx[i])) * sqrt_G / sqrt_2PI;
    for( j = 0; j < 512; j++ )
    {
      fy = sqrt(fabs(ky[j])) * sqrt_G / sqrt_2PI;
      f = sqrt( fx*fx + fy*fy );
      p = PI - atan2(fy,fx);
      if( f >= freq[0] && f <= freq[25] )
        splin2(phi-1,freq-1,ya,y2a,24,26,p,f,&sp[i][j]);
      else
        sp[i][j] = 0.0;
      if( sp[i][j] < 0.0 ) sp[i][j] = 0.0; /* make sure we don't interpolate to a negative number */
    }
  }
/*
  force spectrum to zero at origin and near nyquist kx & ky:
*/
  sp[0][0] = 0.0;
  for( j = 0; j < N; j++ )
  {
    sp[j][N/2] = 0.0;
    sp[N/2][j] = 0.0;
  }

/* save the result */
  fd = creat(outfile,0644);
  write(fd,sp,4*512*512);
  close(fd);

  free_matrix(ya,1,24,1,26);
  free_matrix(y2a,1,24,1,26);
}

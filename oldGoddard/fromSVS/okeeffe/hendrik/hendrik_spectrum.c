/* this is a filter that converts Hendrik's power spectrum,
   which is provided as a function of frequency and angle, to
   my internal representation, which requires a function of
   wave vector: Kx, Ky. Note that a linear interpolation
   will be performed to produce a 512 x 512 output. */

#include <stdio.h>
#include <math.h>
#include <sys/file.h>

float hendrik[24][26];

hendrik_spectrum(hour,kx,ky,spect_k)
int hour;
float kx[512], ky[512];
float spect_k[512][512];
{
  int fd=0;

  fd = open("hendrik.data",0);
  if( hour > 0 ) lseek(fd,hour*4*24*26,L_SET);

  read(fd,hendrik,4*24*26);
  
/* convert to wave-number function, using phase velocity for
   gravity waves: c(k) = sqrt(2pi*g / k) or kinsman's combined
   gravity & capillary waves: c(k) = sqrt( k^2/km^2 + km^2/k^2 ).
   The first choice is simplest, since c(k) == w/k for phase
   velocity, or w = 2pi*f = k*c(k) ==> f = sqrt( g*k / 2pi ) is
   our required translation. assume that the vector F,
   ie (Fx,Fy) has components (fcos,fsin), find the min and
   max of the associated wave numbers and constuct the k matrix
   by interpolation  */

  wavenumber(hendrik,kx,ky,spect_k); 
}

#include <math.h>

typedef struct complex { float r; float i; } COMPLEX;

int wave_motion(N,time,kx,ky,spectrm_0,spectrm_t)
int N; float time;
float kx[], ky[];
COMPLEX *spectrm_0, *spectrm_t;
{
  int i, j;
  float vel_min = 23.2, k_min = 3.639, root2 = 1.4142, pi_2 = 2*3.1415926;
  float shift, vel, k;
  COMPLEX phase, dbg0[16][16], dbgt[16][16];

/* from Kinsman pg 175 - 177 
vel = vel_min / root2 * sqrt( k_min / k + k / k_min );
and given the spectrum at t=0 create it at t=time > 0 by simply
multiplying each spectral component by its appropriate phase shift
"exp(-i*k*shift(t)" where shift(t) is simply the velocity of the
wave * time.
*/
/* first  allocate the memory for the new spectrum 
  spectrm_t = (COMPLEX *) malloc(N*N*sizeof(COMPLEX));
  ret_ptr = spectrm_t;
*/
  vel_min = vel_min / root2;
  for( i = 0; i < N; i++ )
  { 
    for( j = 0; j < N; j++ )
    {
      k = sqrt( kx[i]*kx[i] + ky[j]*ky[j] );
      if( k > 0.0 )
	vel = vel_min * sqrt( k_min/k + k/k_min );
      else
	vel = 0.0;

      shift = k * vel * time;
      phase.r = cos(shift);
      phase.i = sin(shift);
/* and finally perform the complex multiply of the phase and the spectrum 
   components */
      spectrm_t->r = phase.r * spectrm_0->r - phase.i * spectrm_0->i;
      spectrm_t->i = phase.r * spectrm_0->r + phase.i * spectrm_0->i;
/* and don't neglect to increment our pointers */
      if( N <= 16 ) 
      {
	dbg0[i][j].r = spectrm_0->r;
	dbg0[i][j].i = spectrm_0->i;
	dbgt[i][j].r = spectrm_t->r;
	dbgt[i][j].i = spectrm_t->i;
      }
      spectrm_0++;
      spectrm_t++;
    }
  }
/*
  return( ret_ptr );
*/
  if( N <= 16 )
  {
    printf("\n Wave_motion> T0 Spectrm:\n");
    for( i = 0; i < N; i++ )
      printf("%f,%f %f,%f %f,%f %f,%f %f,%f %f,%f %f,%f %f,%f %f,%f %f,%f %f,%f %f,%f %f,%f %f,%f %f,%f %f,%f\n",
      dbg0[i][0].r,dbg0[i][0].i,dbg0[i][1].r,dbg0[i][1].i,dbg0[i][2].r,dbg0[i][2].i,dbg0[i][3].r,dbg0[i][3].i,
      dbg0[i][4].r,dbg0[i][4].i,dbg0[i][5].r,dbg0[i][5].i,dbg0[i][6].r,dbg0[i][6].i,dbg0[i][7].r,dbg0[i][7].i,
      dbg0[i][8].r,dbg0[i][8].i,dbg0[i][9].r,dbg0[i][9].i,dbg0[i][10].r,dbg0[i][10].i,dbg0[i][11].r,dbg0[i][11].i,
      dbg0[i][12].r,dbg0[i][12].i,dbg0[i][13].r,dbg0[i][13].i,dbg0[i][14].r,dbg0[i][14].i,dbg0[i][15].r,dbg0[i][15].i);
    printf("\n T Spectrm:\n");
    for( i = 0; i < N; i++ )
      printf("%f,%f %f,%f %f,%f %f,%f %f,%f %f,%f %f,%f %f,%f %f,%f %f,%f %f,%f %f,%f %f,%f %f,%f %f,%f %f,%f\n",
      dbgt[i][0].r,dbgt[i][0].i,dbgt[i][1].r,dbgt[i][1].i,dbgt[i][2].r,dbgt[i][2].i,dbgt[i][3].r,dbgt[i][3].i,
      dbgt[i][4].r,dbgt[i][4].i,dbgt[i][5].r,dbgt[i][5].i,dbgt[i][6].r,dbgt[i][6].i,dbgt[i][7].r,dbgt[i][7].i,
      dbgt[i][8].r,dbgt[i][8].i,dbgt[i][9].r,dbgt[i][9].i,dbgt[i][10].r,dbgt[i][10].i,dbgt[i][11].r,dbgt[i][11].i,
      dbgt[i][12].r,dbgt[i][12].i,dbgt[i][13].r,dbgt[i][13].i,dbgt[i][14].r,dbgt[i][14].i,dbgt[i][15].r,dbgt[i][15].i);
  }
    
}

/* there are 26 frequencies in the input spectrum, the lowest is 1.1/100 Hz,
each successive freqency is 1.1 times the previous */

#include <math.h>
#include "nrutil.h"

interp(n,sp_orig,sp)
int n;
float sp_orig[24][26];
float sp[64][64];
{
  float freq[26], phi[24], del_phi=15.0/57.3;
  float **ya, **y2a, fmax, phmax, fx[64], fy[64], f, p;
  int i,j,fd;
  char outfile[80];

  sprintf(outfile,"spect64.xy_%d",n);
  
  ya = matrix(1,26,1,24);
  y2a = matrix(1,26,1,24);

  freq[0] = 1.1 / 100.0;
  for( i = 1; i < 26; i++ )
    freq[i] = 1.1 * freq[i-1];

  fmax = freq[25] / sqrt(2.0);
  
  phi[0] = 0.0;
  for( i = 1; i < 24; i++ )
    phi[i] = i * del_phi;

  phmax = phi[23];

  for( i = 1; i <= 24; i++ )
    for( j = 1; j <= 26; j++ )
      ya[i][j] = sp_orig[i-1][j-1];

  splie2(phi-1,freq-1,ya,24,26,y2a);

  for( i = 32; i < 64; i++ )
  {
    fx[i] = freq[0] + (i-32)/31.0 * (fmax - freq[0]);
    fx[63-i] = -fx[i];
    fy[i] = fx[i];
    fy[63-i] = -fy[i];
  } 
  for( i = 0; i < 64; i++ )
  {
    for( j = 0; j < 64; j++ )
    {
      f = sqrt( fx[i]*fx[i] + fy[j]*fy[j] );
      p = atan2(fy[j],fx[i]);
      if( p < 0.0 ) p = 2*3.1415926 + p;
      splin2(phi-1,freq-1,ya,y2a,24,26,p,f,&sp[i][j]);
    }
  }
/* save the result */
  fd = creat(outfile,0644);
  write(fd,sp,4*64*64);
  close(fd);

  free_matrix(ya,1,26,1,24);
  free_matrix(y2a,1,26,1,24);
}

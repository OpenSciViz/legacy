#include <math.h>
/*	returns a 'random' number between 0 & 2pi */
float rndphs()
{
  static float pi = 3.1415926, vax_scale=2147480000.0, scale=32767.0;
  float ret_val=0.0;

  ret_val = drand48();
/*  ret_val = ret_val/scale * 2.0 * pi; */
  ret_val = ret_val * 2.0 * pi;

  return(ret_val);
}

#include <stdio.h>
#include <gl/addrs.h>
#include <gl/cg2vme.h>
#define TRUE 1
#define FALSE 0

int is_stereo()
{
  int RV1 = TRUE;
  long rw1, rw2;
  int i;

  for( i=1; i<10; i++ )
  {
    rw1 = getvideo(DE_R1);
    rw1 = rw1^DER1_STEREO;
    rw2 = rw1;
    setvideo(DE_R1, rw2);
    rw2 = getvideo(DE_R1);
    rw2 = getvideo(DE_R1);
    RV1 = (rw1 == rw2) ? TRUE : FALSE;
    if( !RV1 )
      break;
    rw2 = rw2^DER1_STEREO;
    rw1 = rw2;
    setvideo(DE_R1, rw2);
    rw2 = getvideo(DE_R1);
    RV1 = (rw1 == rw2) ? TRUE : FALSE;
    if( !RV1 ) break;
  }

  return( RV1 == TRUE ) ? FALSE : TRUE;
}

    

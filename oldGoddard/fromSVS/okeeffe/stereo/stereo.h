/* stereo.h */

#ifndef STEREO_H
#define STEREO_H

#define YMAXSTEREO 491
#define YOFFSET 532

extern void stereopersp( int, float, float, float, float, float);

/* stereopersp(fovy, aspect, near, far, conv, eye)
 *
 * fovy, aspect, near, far - work just like the 'perspective'
 * command.
 *
 * conv - the plan at which the left and right image will converge 
 * on the screen. If conv is equal to the near plane, the stereo image
 * will appear to be behind the plan of the screen. If conv is set to 
 * far plane, the stereo image will appear in front of the screen.
 *
 * eye - the distance (in world coordinates) the eye is off-center (half
 * the eye separation).
 *
 */

#endif

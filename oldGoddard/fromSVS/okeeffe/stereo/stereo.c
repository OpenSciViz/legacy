#include "stereo.h"
#include <math.h>
#include <gl.h>

static float idmat[4][4] = { {1.0, 0.0, 0.0, 0.0},
			     {0.0, 1.0, 0.0, 0.0},
			     {0.0, 0.0, 1.0, 0.0},
			     {0.0, 0.0, 0.0, 1.0} };

void stereopersp(fovy, aspect, near, far, conv, eye)
int fovy;
float aspect, near, far, conv, eye;
{
  float left, right, top, bottom;
  float gltan;

  gltan = tan(fovy/20.0*M_PI/180.0);

  left = -gltan*near - eye/conv*near;
  right = gltan*near - eye/conv*near;

  window(left, right, bottom, top, near, far);

  if( getmmode() == MVIEWING ) loadmatrix(idmat);
  translate(-eye, 0.0, 0.0);

  return;
}

#include <gl.h>
#include <device.h>
#include <get.h>
#include "stereo.h"

static Matrix objmat[4][4] = { {1.0, 0.0, 0.0, 0.0},
			     {0.0, 1.0, 0.0, 0.0},
			     {0.0, 0.0, 1.0, 0.0},
			     {0.0, 0.0, 0.0, 1.0} };

static Matrix idmat[4][4] = { {1.0, 0.0, 0.0, 0.0},
			     {0.0, 1.0, 0.0, 0.0},
			     {0.0, 0.0, 1.0, 0.0},
			     {0.0, 0.0, 0.0, 1.0} };

int fovy = 300;
float aspect = (float)XMAXSCREEN/(float)YMAXSTEREO/2.0;
float near=4.0, far=8.0, conv=6.0, eye=0.2, dist=6.0;

int mode=0;
#define NOTHING 0
#define ORIENT 1

int is_stereo();

long monitor;

int omx, mx, omy, my;

main()
{
  int dev;
  short val;

  initialize();

  draw_scene();

  while( TRUE )
  {
    switch( dev = qread(&val) )
    {
      case ESCKEY: if( val ) break;

      case WINQUIT: setvaluator(MOUSEY, omy, 0, YMAXSCREEN);
		    setmonitor(monitor);
		    exit(0);

      case REDRAW: reshapeviewport();
		   draw_scene();
		   break;

      case MIDDLEMOUSE: omx = mx; omy = my;
			if( val )
			  mode = ORIENT;
			else
			  mode = NOTHING;
			break;

      case MOUSEX: omx = mx; mx = val;
		   update_scene();
		   break;

      case MOUSEY: omy = my; my = val;
		   update_scene();
		   break;
    }
  }
}

initialize()
{
  prefposition(0, XMAXSCREEN, 0, YMAXSCREEN);
  winopen("stereo example");

  doublebuffer();
  RGBmode();
  gconfig();

  qdevice(ESCKEY);
  qdevice(WINQUIT);
  qdevice(MIDDLEMOUSE);
  qdevice(LEFTMOUSE);
  qdevice(MOUSEX);
  qdevice(MOUSEY);

  viewport(0, XMAXSCREEN, 0, YMAXSCREEN);
  frontbuffer(TRUE);
  RGBcolor(0,0,0);
  clear();
  frontbuffer(FALSE);
  setvaluator(MOUSEY, YMAXSTEREO/2, 0, YMAXSTEREO);

  monitor = getmonitor();

/* check to see if the new stereovire hardware is available. if it is, do a 
   setmonitor. If it is not, assume the old stereo hardware (switched by hand)
   is used. The routine 'is_stereo' is at the end of the program and will
   be replaced with a call to getgdesc(GD_STEREO). */

  if( is_stereo() ) setmonitor(STR_RECT);
}

update_scene()
{
  switch( mode )
  {
    case ORIENT: orient();
		 break;
  }
  if( mode ) draw_scene();
}

orient()
{
  pushmatrix();
  loadmatrix( idmat );
  rotate(mx-omx, 'y');
  rotate(omy-my, 'x');

  multmatrix(objmat);
  getmatrix(objmat);

  popmatrix();
}

draw_scene()
{
  int i;

/* right eye */

  viewport(0, XMAXSCREEN, 0, YMAXSTEREO);
  RGBcolor(255, 255, 255);
  clear();
  stereopersp(fovy,aspect,near,far,conv,eye);
  translate(0.0,0.0,-dist);
  multmatrix(objmat);
  for( i=0; i<10; i++ )
  {
    scale(0.8,0.8,0.8);
    rotate(50,'x');
    rotate(70,'y');
    rotate(90,'z');
    draw_cube();
  }

/* left eye */

  viewport(0, XMAXSCREEN, YOFFSET, YOFFSET+YMAXSTEREO);
  RGBcolor(255,255,255);
  clear();

  stereopersp(fovy, aspect, near, far, conv, -eye);
  translate(0.0,0.0,-dist);
  multmatrix(objmat);
  for( i=0; i<10; i++ )
  {
    scale(0.8,0.8,0.8);
    rotate(50,'x');
    rotate(70,'y');
    rotate(90,'z');
    draw_cube();
  }

  swapbuffers();
}

float cube_vert[8][3] = { {1.0, 1.0, -1.0,},
			  {1.0, -1.0, -1.0,},
			  {-1.0, -1.0, -1.0,},
			  {-1.0, 1.0, -1.0,},
			  {1.0, 1.0, -1.0,},
			  {1.0, -1.0, 1.0,},
			  {-1.0, -1.0, 1.0,},
			  {-1.0, 1.0, 1.0,}, };

draw_cube()
{
  RGBcolor(0,0,0);


  bgnclosedline();
   v3f(cube_vert[0]);
   v3f(cube_vert[1]);
   v3f(cube_vert[2]);
   v3f(cube_vert[3]);
  endclosedline();

  bgnclosedline();
   v3f(cube_vert[4]);
   v3f(cube_vert[5]);
   v3f(cube_vert[6]);
   v3f(cube_vert[7]);
  endclosedline();

  bgnline();
   v3f(cube_vert[0]);
   v3f(cube_vert[4]);
  endline();

  bgnline();
   v3f(cube_vert[1]);
   v3f(cube_vert[5]);
  endline();

  bgnline();
   v3f(cube_vert[2]);
   v3f(cube_vert[6]);
  endline();

  bgnline();
   v3f(cube_vert[3]);
   v3f(cube_vert[7]);
  endline();
}






/*
 * This file is a product of Sun Microsystems, Inc. and is provided for
 * unrestricted use provided that this legend is included on all tape
 * media and as a part of the software program in whole or part.  Users
 * may copy or modify this file without charge, but are not authorized to
 * license or distribute it to anyone else except as part of a product
 * or program developed by the user.
 * 
 * THIS FILE IS PROVIDED AS IS WITH NO WARRANTIES OF ANY KIND INCLUDING THE
 * WARRANTIES OF DESIGN, MERCHANTIBILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE, OR ARISING FROM A COURSE OF DEALING, USAGE OR TRADE PRACTICE.
 * 
 * This file is provided with no support and without any obligation on the
 * part of Sun Microsystems, Inc. to assist in its use, correction,
 * modification or enhancement.
 * 
 * SUN MICROSYSTEMS, INC. SHALL HAVE NO LIABILITY WITH RESPECT TO THE
 * INFRINGEMENT OF COPYRIGHTS, TRADE SECRETS OR ANY PATENTS BY THIS FILE
 * OR ANY PART THEREOF.
 * 
 * In no event will Sun Microsystems, Inc. be liable for any lost revenue
 * or profits or other special, indirect and consequential damages, even
 * if Sun has been advised of the possibility of such damages.
 * 
 * Sun Microsystems, Inc.
 * 2550 Garcia Avenue
 * Mountain View, California  94043
 */
static char sccsid[] = "@(#) news_sel.c 9.5 88/01/19";
/*-
	news_sel.c:  gets the news-server's idea
	of the selection and prints it on stdout,
	or writes stdin to the specified selection

		Jerry Farrell, Sun Microsystems
		Mon Apr 20 16:27:34 PST 1987
-*/

#ifdef REF
#include <sys/types.h>
#include <ref/config.h>
#endif
#include <stdio.h>
#include <psio.h>
#include "news_sel.h"

#define complain(string) fprintf(stderr, string)

PSFILE         *PostScript,
	       *PostScriptInput;

extern char    *malloc();

static char    *status_strings[] = {
    "O.K.\n",				/* this one doesn't get displayed */
    "Can't contact NeWS server.\n",
    "That NeWS selection doesn't exist.\n",
    "The NeWS process can't convert its selection to ascii.\n",
    "Couldn't set selection to that value (more than 32K characters?)\n"
};

static int	 load_buffer(),
		 set_selection_name();
static void	 read_response(),
		 usage();

static enum Action { Get, Set, Clear };

main(argc, argv)
    char          **argv;
{
    enum Action     action = Get;
    int             buflen, parsed_rank = 0, status = OK;
    char           *buffer, *program_name, selection_name[80];

    program_name = argv[0];
    if (argc > 3)
	usage(program_name);
    strcpy(selection_name, "PrimarySelection");
    for (--argc, ++argv; argc > 0; --argc, ++argv) {
	if (strcmp(*argv, "-g") == 0 || strcmp(*argv, "-get") == 0) {
	    action = Get;
	    continue;
	}
	if (strcmp(*argv, "-s") == 0 || strcmp(*argv, "-set") == 0) {
	    action = Set;
	    if (!parsed_rank)
		strcpy(selection_name, "ShelfSelection");
	    continue;
	}
	if (strcmp(*argv, "-c") == 0 || strcmp(*argv, "-clear") == 0) {
	    action = Clear;
	    continue;
	}
	if (set_selection_name(*argv, selection_name))
	    parsed_rank = 1;
	else
	    usage(program_name);
    }
    if (PostScript == 0 && ps_open_PostScript() == 0) {
	complain("Cannot connect to NeWS server\n");
	exit(1);
    }
    switch (action) {
      case Get:
	request_selection(&status, selection_name);
	if (status == OK)
	    read_response();
	else
	    complain(status_strings[status]);
	break;
      case Set:
	status = load_buffer(&buffer, &buflen);
	if (status == OK)
	    set_selection(buffer, buflen, selection_name);
	else
	    complain(status_strings[status]);
	break;
      case Clear:
	clear_selection(selection_name);
	status = OK;
    }
    if (status != NoServer)
	ps_close_PostScript();
}

static void
usage(name)
    char           *name;
{
    fprintf(stderr, "usage: %s [-s[et] | -c[lear]] [-1 | -2 | -3 | -b[0-9] ]\n",
    	    name);
    exit(1);
}

#define FIRSTBUF	1024
#define MAXSTRING      32767
static int
load_buffer(resultp, lenptr)
    register char **resultp;
    int            *lenptr;
{
    register int    cursize = FIRSTBUF,
		    curlen = 0;
    register char  *bufp = malloc(FIRSTBUF);

    if (bufp == 0)
	return BadSource;
    while (cursize < MAXSTRING) {
	*resultp = bufp;
	bufp += curlen;
	while (curlen < cursize) {
	    register unsigned int c;

	    if ((c = getchar()) == EOF) {
		*bufp = '\0';
		*lenptr = curlen;
		return OK;
	    }
	    *bufp++ = (char) c;
	    curlen += 1;
	}
	cursize *= 2;
	bufp = malloc(cursize);
	if (bufp == 0)
	    break;
	bcopy(*resultp, bufp, curlen);
	free(*resultp);
    }
    return BadSource;
}


static int
set_selection_name(arg, name)
    register char   *arg, *name;
{
    if (*arg++ != '-')
	return 0;
    switch (*arg++) {
      case '1':
	strcpy(name,"PrimarySelection");
	break;
      case '2':
	strcpy(name,"SecondarySelection");
	break;
      case '3':
	strcpy(name,"ShelfSelection");
	break;
      case 'b':
      case 'B':
	strcpy(name,"SelectionBuffer");
	strcat(name, arg);
	break;
      default:
	return 0;
    }
    return 1;
}

static void 
read_response()
{
    register int    c;

    c = psio_getc(PostScriptInput);
    while (!psio_error(PostScriptInput) && !psio_eof(PostScriptInput)) {
	putchar(c);
	c = psio_getc(PostScriptInput);
    }
}

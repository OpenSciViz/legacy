%
% This file is a product of Sun Microsystems, Inc. and is provided for
% unrestricted use provided that this legend is included on all tape
% media and as a part of the software program in whole or part.  Users
% may copy or modify this file without charge, but are not authorized to
% license or distribute it to anyone else except as part of a product
% or program developed by the user.
% 
% THIS FILE IS PROVIDED AS IS WITH NO WARRANTIES OF ANY KIND INCLUDING THE
% WARRANTIES OF DESIGN, MERCHANTIBILITY AND FITNESS FOR A PARTICULAR
% PURPOSE, OR ARISING FROM A COURSE OF DEALING, USAGE OR TRADE PRACTICE.
% 
% This file is provided with no support and without any obligation on the
% part of Sun Microsystems, Inc. to assist in its use, correction,
% modification or enhancement.
% 
% SUN MICROSYSTEMS, INC. SHALL HAVE NO LIABILITY WITH RESPECT TO THE
% INFRINGEMENT OF COPYRIGHTS, TRADE SECRETS OR ANY PATENTS BY THIS FILE
% OR ANY PART THEREOF.
% 
% In no event will Sun Microsystems, Inc. be liable for any lost revenue
% or profits or other special, indirect and consequential damages, even
% if Sun has been advised of the possibility of such damages.
% 
% Sun Microsystems, Inc.
% 2550 Garcia Avenue
% Mountain View, California  94043
%
%
%  @(#) news_sel.cps 9.3 88/01/19
%
% news_selection.cps
% Copyright (c) 1987 by Sun Microsystems, Inc.
% Jerry Farrell Tue Apr 21 12:01:59 PST 1987
%

#define ps_STATUS_TAG	0200
#define ps_OK		0
#define ps_NoServer	1
#define ps_NoSelection	2
#define ps_CantFormat	3
#define ps_BadSource	4

C: #define STATUS_TAG	ps_STATUS_TAG
C: #define OK		ps_OK
C: #define NoServer	ps_NoServer
C: #define NoSelection	ps_NoSelection
C: #define CantFormat	ps_CantFormat
C: #define BadSource	ps_BadSource

cdef request_selection(status,string selection_name) => ps_STATUS_TAG (status)
    /Rank selection_name cvn	def
    /request 16 dict dup begin
	/ContentsAscii dup def
    end def
    /get_response {	%  response_dict => -
	dup null eq {
	    ps_NoSelection
	} {
	    /ContentsAscii get
	    dup type /stringtype eq {
	        ps_OK
	    } {
	        pop ps_CantFormat
	    } ifelse
	} ifelse
    } def
    systemdict /Selections known {
	request Rank selectionrequest
	get_response
    } {
	ps_NoServer
    } ifelse
    ps_STATUS_TAG tagprint
    dup typedprint
    ps_OK eq {
	print
    } if
    flush
    quit

cdef set_selection(cstring str, string selection_name)
    str selection_name cvn			% str rank
    dup clearselection
    exch 10 dict begin				% rank str dict
	/ContentsAscii		exch	def	% rank
	/SelectionObjSize	1	def
	currentdict				% rank dict 
    end exch setselection
    quit

cdef clear_selection(string selection_name)
    selection_name cvn clearselection
    quit

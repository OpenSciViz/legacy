/*
 * This file is a product of Sun Microsystems, Inc. and is provided for
 * unrestricted use provided that this legend is included on all tape
 * media and as a part of the software program in whole or part.  Users
 * may copy or modify this file without charge, but are not authorized to
 * license or distribute it to anyone else except as part of a product
 * or program developed by the user.
 * 
 * THIS FILE IS PROVIDED AS IS WITH NO WARRANTIES OF ANY KIND INCLUDING THE
 * WARRANTIES OF DESIGN, MERCHANTIBILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE, OR ARISING FROM A COURSE OF DEALING, USAGE OR TRADE PRACTICE.
 * 
 * This file is provided with no support and without any obligation on the
 * part of Sun Microsystems, Inc. to assist in its use, correction,
 * modification or enhancement.
 * 
 * SUN MICROSYSTEMS, INC. SHALL HAVE NO LIABILITY WITH RESPECT TO THE
 * INFRINGEMENT OF COPYRIGHTS, TRADE SECRETS OR ANY PATENTS BY THIS FILE
 * OR ANY PART THEREOF.
 * 
 * In no event will Sun Microsystems, Inc. be liable for any lost revenue
 * or profits or other special, indirect and consequential damages, even
 * if Sun has been advised of the possibility of such damages.
 * 
 * Sun Microsystems, Inc.
 * 2550 Garcia Avenue
 * Mountain View, California  94043
 */

#ifndef lint
static char sccsid[] = "@(#)io.c 9.7 88/01/19 Copyright 1985 Sun Micro";
#endif


/*
 * Copyright (c) 1985 by Sun Microsystems, Inc. 
 */

#ifdef SYSVREF
#include <stropts.h>
#include <poll.h>
#endif
#ifdef REF
#include <sys/types.h>
#include <ref/config.h>
#endif
#include <stdio.h>
#include <psio.h>
#include <errno.h>
#include <fcntl.h>

#define	MAX(a,b)	((a)>(b)?(a):(b))
#define	KBDBUFSIZE	4096
#define	PTYBUFSIZE	8192

static	char *kbdbuf, *kbdfront, *kbdback, *kbdend;
#define	BUFSETUP(which,size) { \
    which/**/buf = malloc(size); which/**/end = which/**/buf + size; \
    which/**/front = which/**/back = which/**/buf; \
}
static	char *ptybuf;		/* buffer for reading from pty slave */
static	int wfproto;		/* prototype for write select mask */
extern	int Mfd;
extern	int errno;
extern	char *malloc();
extern	PSFILE *PostScript;
extern	int PageMode;
extern	int PageFull;

/*
 * Input dispatcher: take data from postscript
 * program and pty, dispatching each to the
 * appropriate handler.
 */
terminal(cfd, kfd)
{
    register int n;
#ifndef SYSVREF
    int rf, wf;
    int max;
#else
#define PTY 0
#define KEYBOARD 1
#define POSTSCRIPT 2
#define NFDS 3
    struct pollfd rwf[NFDS];
    int i;
#endif
    int keyboard, pty, postscript, ndeferred;
    char *deferred;

    PageFull = 0;			/* not inhibiting scrolling */
    ndeferred = 0;			/* no deferred output */
    keyboard = 1<<kfd;
    pty = 1<<cfd;
    postscript = 1<<psio_fileno(PostScript);
#if !defined(SYSVREF) && !defined(sgi)
    (void) fcntl(cfd, F_SETFL, fcntl(cfd, F_GETFL, 0)|FNDELAY);
    (void) fcntl(kfd, F_SETFL, fcntl(kfd, F_GETFL, 0)|FNDELAY);
#else
    (void) fcntl(cfd, F_SETFL, fcntl(cfd, F_GETFL, 0)|O_NDELAY);
    (void) fcntl(kfd, F_SETFL, fcntl(kfd, F_GETFL, 0)|O_NDELAY);
#endif
#ifndef SYSVREF
    max = MAX(kfd, cfd) + 1;
#endif
    BUFSETUP(kbd, KBDBUFSIZE);
    ptybuf = malloc(PTYBUFSIZE);
    wfproto = 0;
    for (;;) {
	/*
	 * Don't poll for input to be sent to the display
	 * if we have a full screen, or we are blocked already
	 * trying to transmit to the server.
	 */
#ifndef SYSVREF
	rf = (PageFull || (wfproto & postscript)) ? 0 : pty;
	wf = wfproto;
	if (kbdfront != kbdend)		/* space to read from kbd */
	    rf |= keyboard;
	if (select(max, &rf, &wf, 0, 0) < 0) {
	    if (errno == EINTR)
		continue;
	    perror("select");
	    break;
	}
#else
	rwf[PTY].fd = -1;
	rwf[PTY].events = 0;
	rwf[KEYBOARD].fd = -1;
	rwf[KEYBOARD].events = 0;
	rwf[POSTSCRIPT].fd = -1;
	rwf[POSTSCRIPT].events = 0;

	if (!(PageFull || (wfproto & postscript))) {
		rwf[PTY].fd = cfd;
		rwf[PTY].events |= POLLIN;
	}

	if (wfproto & pty) {
		rwf[PTY].fd = cfd;
		rwf[PTY].events |= POLLOUT;
	}
	if (wfproto & postscript) {
		rwf[POSTSCRIPT].fd = psio_fileno(PostScript);
		rwf[POSTSCRIPT].events |= POLLOUT;
	}

	if (kbdfront != kbdend) {		/* space to read from kbd */
		rwf[KEYBOARD].fd = kfd;
		rwf[KEYBOARD].events |= POLLIN;
	}
	if (poll(rwf, NFDS, -1) < 0) {
	    if (errno == EINTR)
		continue;
	    perror("poll");
break_here:
	    break;
	}

	/* check to see if any connections were hung up */
	if (rwf[KEYBOARD].revents & POLLHUP || rwf[PTY].revents & POLLHUP
					|| rwf[POSTSCRIPT].revents & POLLHUP)
		break;

	/*  look for exceptional conditions on fd's */
	for (i = 0; i < NFDS; i++) {
		if (rwf[i].revents & (POLLERR | POLLNVAL)) {
			fprintf(stderr, "Error on an fd in poll ");
			fprintf(stderr, "[POLLERR | POLLNVAL]\n");
			goto break_here;	/* should be break, but C has */
						/* no multi-level break */
		}
	}
#endif
	/*
	 * Flush PostScript descriptor.
	 */
#ifndef SYSVREF
	if (wf & postscript && (psio_flush(PostScript) == 0))
#else
	if (rwf[POSTSCRIPT].revents & POLLOUT && (psio_flush(PostScript) == 0))
#endif
	    wfproto &= ~postscript;
	/*
	 * Try to flush pty, if clogged, before reading from keyboard.
	 */
#ifndef SYSVREF
	if (wf & pty)
#else
	if (rwf[PTY].revents & POLLOUT)
#endif
	{
	    wfproto &= ~pty;
	    pty_out();
	}
	/*
	 * Take keyboard input.
	 */
#ifndef SYSVREF
	if (rf & keyboard)
#else
	if (rwf[KEYBOARD].revents & POLLIN)
#endif
	{
	    errno = 0;
	    n = read(kfd, kbdfront, kbdend-kbdfront);
	    if (n <= 0) {
		if (errno != EWOULDBLOCK) {
#ifndef SYSVREF	/* Connection Reset is checked above in SYSVREF */
		    if (n < 0 && errno != ECONNRESET)
#endif
			    perror("keyboard");
		    break;
		}
	    } else if (n > 0)
		kbd_input(n);
	}
	/*
	 * If pty_out or kbd_input changed
	 * PageFull, loop to inhibit output.
	 */
	if (PageFull)			/* loop if inhibiting output */
	    continue;
	/*
	 * If screen output was blocked due to previous PageFull condition,
	 * resume it.  Note that this may leave us back in PageFull mode,
	 * and interrupted tcap ops may still need the chars in ptybuf.
	 */
	if (ndeferred) {
	    n = tc_display(deferred, ndeferred);
	    if (n > 0)
		deferred += (ndeferred - n);
	    ndeferred = n;
	    tc_refresh(0);
	    if (PageFull)		/* if PageFull, can't touch ptybuf */
		continue;
	}
	/*
	 * Finally, take pty stuff and send it to the display
	 * except when we're already flush with output.
	 */
#ifndef SYSVREF
	if ((wfproto & postscript) == 0 && rf & pty)
#else
	if ((wfproto & postscript) == 0 && rwf[PTY].revents & POLLIN)
#endif
	{
	    errno = 0;
	    n = read(cfd, ptybuf, PTYBUFSIZE);
#ifndef SYSVREF
	    if (n < 0) {
		if (errno != EIO) {
/* BEGIN SUN BUG WORKAROUND */
			if (errno == EWOULDBLOCK)
			    continue;
/* END SUN BUG WORKAROUND */
			perror("pty");
		}
		break;
	    }
#else
	    if (n == 0)
		continue;
	    if (n < 0) {
		if (errno == EAGAIN)
			continue;
		perror("pty");
		break;
	    }
#endif
	    ndeferred = tc_display(ptybuf, n);
	    if (ndeferred)
		deferred = ptybuf + (n - ndeferred);
	    tc_refresh(0);
	}
#ifdef notdef
if (wfproto & postscript) fprintf(stderr, "blocked\n");
#endif
    }
}

/*
 * Flush PostScript destined for window.
 * If all data was not written, then mark
 * the select mask to find out when data
 * has been flushed to the network.
 */
FlushPostScript()
{

    if (!(psio_flush(PostScript) == 0))
	wfproto |= 1<<psio_fileno(PostScript);
}

/*
 * Flush output to pty.
 */
static
pty_out()
{
	register int cc;

	if ((cc = kbdfront - kbdback) > 0) {
	    if (PageMode) {
		if (PageFull) {
		    switch (*kbdback) {
		    case '\r':
			--cc, kbdback++;
			scrollreset(1);
			break;
		    case ' ':
			--cc, kbdback++;
			/* fall thru... */
		    default:
			scrollreset(0);
			break;
		    }
		    if (cc < 1) {
			kbdfront = kbdback = kbdbuf;
			return;
		    }
		} else
		    scrollreset(0);
	    }
	    cc = write(Mfd, kbdback, cc);
	    if (cc < 0) {
		if (errno != EWOULDBLOCK)
		    perror("master pty");
	    } else
		kbdback += cc;
	}
	if (kbdfront != kbdback)
	    wfproto |= 1<<Mfd;		/* must explicity reenable */
	else
	    kbdfront = kbdback = kbdbuf;
}

#define EVENT_TOGGLEAM		0374	/* toggle auto-margins */
#define EVENT_TOGGLEPM		0375	/* toggle page mode */
#define EVENT_REPAIR		0376	/* window resized or damaged */
#define	EVENT_SETSEL		0200	/* set selection start */
#define	EVENT_EXTENDSEL		0201	/* extend current selection */
#define	EVENT_DESELECT		0202	/* remove current selection */
#define	EVENT_SELREQUEST	0203	/* request held selection */

/*
 * Handle keyboard input + postscript events.
 */
static
kbd_input(n)
    register int n;
{
    register unsigned char *p, *d, **dp;
    register n2;
    int	col, row, rank, dorefresh = 0;

#define NEXTVAL		while (*p != '\n') ++p, --n; ++p, --n;
    for (p = (unsigned char *)kbdfront, n2 = n; --n2 >= 0;) {
	switch (*p++) {
	case EVENT_TOGGLEAM:
	    toggleautomargins();
	    n--;
	    break;
	case EVENT_TOGGLEPM:
	    togglepagemode();
	    if (PageFull) {		/* resume output if blocked */
		scrollreset(0);
		dorefresh++;
	    }
	    n--;
	    break;
	case EVENT_REPAIR:
	    do_display_resize();
	    d = p - 1; n--;
	    while (--n2 >= 0)
		if (*p == EVENT_REPAIR)
		    n--, p++;
		else
		    *d++ = *p++;
	    break;
	case EVENT_SETSEL:		/* col row rank size mode preview */
	    --n; col = atoi(p);
	    NEXTVAL; row = atoi(p);
	    NEXTVAL; rank = atoi(p);
	    NEXTVAL; tc_select_at(col, row, rank);
	    dorefresh++;
	    break;
	case EVENT_EXTENDSEL:		/* col row rank size mode preview */
	    --n; col = atoi(p);
	    NEXTVAL; row = atoi(p);
	    NEXTVAL; rank = atoi(p);
	    NEXTVAL; tc_extend_to(col, row, rank);
	    dorefresh++;
	    break;
	case EVENT_DESELECT:		/* rank */
	    --n; rank = atoi(p);
	    NEXTVAL; tc_deselect(rank);
	    dorefresh++;
	    break;
	case EVENT_SELREQUEST:		/* rank */
	    fprintf(stderr, "got a SELECTIONREQUEST\n");
	    break;
	}
    }
    if (n > 0)
	kbdfront += n;
    if (kbdfront != kbdback)
	pty_out();
    if (dorefresh)
	tc_refresh(0);
}

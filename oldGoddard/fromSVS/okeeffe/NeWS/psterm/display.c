/*
 * This file is a product of Sun Microsystems, Inc. and is provided for
 * unrestricted use provided that this legend is included on all tape
 * media and as a part of the software program in whole or part.  Users
 * may copy or modify this file without charge, but are not authorized to
 * license or distribute it to anyone else except as part of a product
 * or program developed by the user.
 * 
 * THIS FILE IS PROVIDED AS IS WITH NO WARRANTIES OF ANY KIND INCLUDING THE
 * WARRANTIES OF DESIGN, MERCHANTIBILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE, OR ARISING FROM A COURSE OF DEALING, USAGE OR TRADE PRACTICE.
 * 
 * This file is provided with no support and without any obligation on the
 * part of Sun Microsystems, Inc. to assist in its use, correction,
 * modification or enhancement.
 * 
 * SUN MICROSYSTEMS, INC. SHALL HAVE NO LIABILITY WITH RESPECT TO THE
 * INFRINGEMENT OF COPYRIGHTS, TRADE SECRETS OR ANY PATENTS BY THIS FILE
 * OR ANY PART THEREOF.
 * 
 * In no event will Sun Microsystems, Inc. be liable for any lost revenue
 * or profits or other special, indirect and consequential damages, even
 * if Sun has been advised of the possibility of such damages.
 * 
 * Sun Microsystems, Inc.
 * 2550 Garcia Avenue
 * Mountain View, California  94043
 */

#ifndef lint
static char sccsid[] = "@(#)display.c 9.5 88/01/19 Copyright 1985 Sun Micro";
#endif

/*
 * Copyright (c) 1985 by Sun Microsystems, Inc. 
 */

/*-
	display.c

	display.c, Wed Mar 26 15:56:31 1986

		David Rosenthal,
		Sun Microsystems
 */

#ifdef	notdef
#define Diagnose
#endif
/*
 * Screen display module 
 *
 * The external interface of this module is the routine tc_refresh(), and the
 * initialization routine tc_init_screen(). 
 */

#include	<sys/types.h>
#ifdef REF
#include	<ref/config.h>
#endif
#include	"screen.h"
#include	"tcap.h"

#define CHARS_PER_LINE		80
#define LINES_PER_SCREEN	24
#define CHARS_PER_SCREEN	1920

u_short     CharsPerLine = CHARS_PER_LINE;
u_short     LinesPerScreen = LINES_PER_SCREEN;
struct pair Dot = {0, 0};

static Repairing;
static struct pair ScreenDot = {0, 0};
static char *CursorChar = " ";
extern int PageFull;

static
RemoveCursor()
{
    if (ScreenDot.x >= 0) {
	register struct line *l = screen[ScreenDot.y].lineptr;

	if (! (l->prop[ScreenDot.x] & ReverseVideoLook)) {
	    if (l->usedtobe != ScreenDot.y
		|| l->changeposition >= CharsPerLine
		|| ScreenDot.x <= l->changeposition
		|| ScreenDot.x >= screen[ScreenDot.y].visible
		|| ScreenDot.x >= l->length) {
		CursorDown(ScreenDot.x, ScreenDot.y, CursorChar, 1);
	    }
	    else {
		ScreenDot.x = 999;
	    }
	}
	ScreenDot.x = -1;
    }
}

static
PlaceCursor(x, y)
{
    register struct line *l;

    if (x >= CharsPerLine)
	ScreenDot.x = CharsPerLine - 1;
    else if (x < 0)
	ScreenDot.x = 0;
    else
	ScreenDot.x = x;
    if (y >= LinesPerScreen)
	ScreenDot.y = LinesPerScreen - 1;
    else if (y < 0)
	ScreenDot.y = 0;
    else
	ScreenDot.y = y;
    l = screen[ScreenDot.y].lineptr;
    CursorChar = (ScreenDot.x < l->length ? l->body + ScreenDot.x : " ");
    if (!PageFull)			/* hide cursor if output blocked */
	CursorUp(ScreenDot.x, ScreenDot.y, CursorChar, 1);
}

/* --------------- External Routines Below ------------------- */

do_display_resize() {
    register i;
    tc_refresh(0);
    BeginRepair();
    Repairing++;
    ReInitialize();
    for (i = 0; i<LinesPerScreen; i++) {
	screen[i].lineptr->changeposition = 0;
	screen[i].visible = 0;
    }
    tc_refresh(0);
}

PSFILE       *
tc_init_screen(term, fixedsize, xorg, yorg, framelabel, iconlabel)
    char *term;
    char *framelabel, *iconlabel;
{
    register int i;
    PSFILE       *f;
    char        frametitle[100];
    char        icontitle[100];
    char	host[100];
    extern char *malloc();
    extern PSFILE *ps_open_PostScript();

    if ((f = ps_open_PostScript()) == NULL) {
	return (NULL);
    }

    tc_init_selection();
    lines = (struct line *) malloc(LinesPerScreen * sizeof(struct line));
    screen = (struct screen *) malloc(LinesPerScreen * sizeof(struct screen));
    for (i = 0; i < LinesPerScreen; i++) {
	lines[i].body = malloc(CharsPerLine+1);
	lines[i].prop = (u_char *) malloc(CharsPerLine+1);
	lines[i].length = 0;
	lines[i].changeposition = 0;
	lines[i].usedtobe = i;
	*lines[i].body = ' ';
	screen[i].lineptr = lines + i;
	screen[i].visible = 0;
    }
    if (framelabel == NULL) {
	sprintf(frametitle, "%s terminal emulator", term);
	framelabel = frametitle;
    }
    if (iconlabel == NULL) {
        gethostname(host, sizeof (host));
	iconlabel = host;
    }
    Initialize(xorg, yorg, CharsPerLine, LinesPerScreen, fixedsize,
	framelabel, iconlabel);
    StartInput();
    /* XXX - set up PostScript process for i/p etc. */
    psio_flush(PostScript);
    return (f);
}

/*
 * Flush a run of lines to be copied.
 */
#define	FlushRun(start, current, delta) { \
    CopyLines(start - delta, delta, CharsPerLine, current-start); \
    for (; start < current; start++) \
	if (start - delta < LinesPerScreen) \
	    screen[start].visible = (delta<0 ? 0:screen[start+delta].visible);\
}

tc_refresh(full)
    int full;
{
    register struct line *l;
    register int i, pos, c;
    register u_char *cp;
    register struct screen *sp;
    int delta, curdelta, len;
    u_char *tp, *ep, hit[128];

    bzero(hit, LinesPerScreen);
    RemoveCursor();
    /*
     * Figure out which lines have moved and by
     * how much.  Accumulate delta line movements
     * and copy lines to perform scrolling.
     */
    curdelta = 0;
    sp = screen;
    for (i = 0; i < LinesPerScreen; i++) {
	l = (sp++)->lineptr;
	delta = 0;
	if ((pos = l->usedtobe) != i) {
	    /*
	     * Line has moved.  Either the contents are still
	     * the same, in which case they can be copied to
	     * their new location, or they have changed, in
	     * which case we must repaint and/or erase.
	     */
	    if (l->changeposition > 0) {
		/*
		 * Line contents are untouched, if they're
		 * still on the display, calculate the distance
		 * they've moved (the delta).  Otherwise,
		 * set the ``change position'' to force the
		 * entire line to be repainted below.
		 */
		if (!hit[pos] && i < pos) {
		    delta = i - pos;
		    hit[i] = 1;			/* mark line contents void */
		} else
		    l->changeposition = 0;
	    }
	}
	l->usedtobe = i;
	/*
	 * If the delta changes, we need to start a new
	 * run of lines to copy.  If there was a previous
	 * run, then copy those first.
	 */
	if (delta != curdelta) {
	    if (curdelta != 0)
		FlushRun(c, i, curdelta);
	    curdelta = delta;		/* delta for current run */
	    c = i;			/* starting line of run */
	}
    }
    /*
     * Catch any trailing run needing to be copied.
     */
    if (curdelta != 0)
	FlushRun(c, i, curdelta);

    /*
     * Finally, perform any erasures
     * and/or text painting required.
     */
    for (sp = screen, i = 0; i < LinesPerScreen; sp++, i++) {
	l = sp->lineptr;
	/*
	 * If the line shrank, erase visible text past eol.
	 */
	pos = l->changeposition;
	if (pos < sp->visible)
	    EraseRectangle(pos, i, sp->visible - pos);
	if (pos < l->length + 1) {
	    l->body[l->length] = ' ';
	    ep = &l->prop[l->length+1];
	    for (cp = &l->prop[pos]; cp < ep; pos += len) {
		/*
		 * Calculate longest sub-string of
		 * changed text with identical properties.
		 */
		tp = cp;
		for (c = *cp++; c == *cp && cp < ep; cp++)
		    ;
		len = cp - tp;
		/*
		 * Paint sub-string according to properties.
		 */
		if (c & ReverseVideoLook)
		    CursorUp(pos, i, &l->body[pos], len);
		else
		    PaintChars(pos, i, &l->body[pos], len);
		if (c & UnderlineLook) {
		    if (c & ReverseVideoLook)
			WhiteLine(pos, i, len);
		    else
			UnderLine(pos, i, len);
		}
	    }
	    l->changeposition = pos;
	}
	sp->visible = pos;
    }

    PlaceCursor(Dot.x, Dot.y);
    if (Repairing) {
	EndRepair();
	Repairing = 0;
    }
    FlushPostScript();
}

%
% This file is a product of Sun Microsystems, Inc. and is provided for
% unrestricted use provided that this legend is included on all tape
% media and as a part of the software program in whole or part.  Users
% may copy or modify this file without charge, but are not authorized to
% license or distribute it to anyone else except as part of a product
% or program developed by the user.
% 
% THIS FILE IS PROVIDED AS IS WITH NO WARRANTIES OF ANY KIND INCLUDING THE
% WARRANTIES OF DESIGN, MERCHANTIBILITY AND FITNESS FOR A PARTICULAR
% PURPOSE, OR ARISING FROM A COURSE OF DEALING, USAGE OR TRADE PRACTICE.
% 
% This file is provided with no support and without any obligation on the
% part of Sun Microsystems, Inc. to assist in its use, correction,
% modification or enhancement.
% 
% SUN MICROSYSTEMS, INC. SHALL HAVE NO LIABILITY WITH RESPECT TO THE
% INFRINGEMENT OF COPYRIGHTS, TRADE SECRETS OR ANY PATENTS BY THIS FILE
% OR ANY PART THEREOF.
% 
% In no event will Sun Microsystems, Inc. be liable for any lost revenue
% or profits or other special, indirect and consequential damages, even
% if Sun has been advised of the possibility of such damages.
% 
% Sun Microsystems, Inc.
% 2550 Garcia Avenue
% Mountain View, California  94043
%
%
% "@(#)tcap.cps 9.5 88/01/19 SMI
%
% Copyright (c) 1985 by Sun Microsystems, Inc.
%/
#define	MENU_PAGEMODE	1
#define	MENU_MARGINS	2

cdef Initialize(x,y,c,l,fixedsize,string framelabel, string iconlabel)
%    100 dict begin		% XXX - is this necessary?
%    /Last_pix_ht 0 def
%    /Last_pix_wd 0 def
%    /Last_font_scale_fac 1000 def
    /lines_per_screen l def
    /childprocess null def
    % Name key-value pairs
    /MyInputMap 3 dict dup begin
	/SetSelectionAt		(\200) def
	/ExtendSelectionTo	(\201) def
	/DeSelect		(\202) def
    end def
    % rank key-value pairs
    /InputFocus		0 def
    /PrimarySelection	1 def
    /SecondarySelection	2 def
    /ShelfSelection	3 def
    
    % Back mapping of rank indices to keys
    /MyRankBackmap [
	/InputFocus
	/PrimarySelection
	/SecondarySelection
	/ShelfSelection
    ] def
    
    % Array of selection dicts
    /MySelections [
	0			% InputFocus
%	10 dict dup begin	% InputFocus
%	    /ContentsAscii		null	def
%	    /SelectionObjSize		1	def
%	end
	10 dict dup begin	% PrimarySelection
	    /ContentsAscii		null	def
	    /SelectionObjSize		1	def
	end
	10 dict dup begin	% SecondarySelection
	    /ContentsAscii		null	def
	    /SelectionObjSize		1	def
	end
%	10 dict dup begin	% ShelfSelection
%	    /ContentsAscii		null	def
%	    /SelectionObjSize		1	def
%	end
    ] def
    /MyWindow framebuffer /new DefaultWindow send def
    fixedsize 0 eq { 
	/reshapefromuser MyWindow send 
    } { 
% XXX calculate reasonable window size from rows and columns
	LiteWindow begin
 	x y c 7 mul BorderLeft BorderRight add add
	l .3 add 15.4 cvi mul BorderBottom BorderTop add add
	end
	/reshape MyWindow send
    } ifelse
    {
	/FrameLabel framelabel def
	/IconImage /terminal def
	/IconLabel iconlabel def
	/FixFrame {(\376) print} def
	/PaintClient {(\376) print} def
	/ClientMenu [
	    (Stuff)		{
		/PrimarySelection getselection
		dup null eq {pop} 		% See if null selection
		{   dup /ContentsAscii known	% Get ascii contents
		    {/ContentsAscii get print}
		    {pop} ifelse
		} ifelse
		}
	    (Page Mode On)	{(\375) print}
	    (Auto-Margins On)	{(\374) print}
	    (Frame =>)		MyWindow /FrameMenu get
	] /new DefaultMenu send def
    } MyWindow send
    MyWindow /ClientCanvas get setcanvas
    /MySetTransform createevent def
    MySetTransform begin
	/Name /SetTransform def
	/Canvas currentcanvas def
    end

    /BRP { {gsave
	FrameCanvas setcanvas damagepath clipcanvas PaintFrame
	% initialize client canvas with background color
	ClientCanvas setcanvas backgroundcolor fillcanvas
	grestore}  MyWindow send } def
    /ERP { clipcanvas } def
    /CD { 	% c x y cursordown
	gsave
	maxy add
	translate
	dup length dup
	0 0 moveto
	0 1 rlineto
	0 rlineto	% x component is length of c
	0 lineto	% x component is length of c
	closepath
	backgroundcolor setcolor
	fill
	textcolor setcolor
	0 maxy neg moveto
	show
	grestore
    } def
    /CU { 	% c x y cursorup
	gsave
	maxy add
	translate
	dup length dup
	0 0 moveto
	0 1 rlineto
	0 rlineto	% x component is length of c
	0 lineto	% x component is length of c
	closepath
	textcolor setcolor
	fill
	backgroundcolor setcolor
	0 maxy neg moveto
	show
	grestore
    } def
    /UL { 	% w x y underline
	gsave
	1 maxy add 2 div add
	translate
	0 0 moveto
	0 rlineto
	textcolor setcolor
	stroke
	grestore
    } def
    /WL { 	% w x y whiteline
	gsave
	1 maxy add 2 div add
	translate
	0 0 moveto
	0 rlineto
	backgroundcolor setcolor
	stroke
	grestore
    } def
    /ER {	% w x y ER
	gsave
	maxy add
	translate		% w
	0 0 moveto		% w
	0 1 rlineto		% w
	dup 0 rlineto	
	0 lineto
	closepath
	backgroundcolor setcolor
	fill
	textcolor setcolor
	grestore
    } def
    /CL {	% yby w yfrom nl CopyLine
	gsave
	/NumLin exch def	% yby w yfrom
	maxy add 0 exch translate	% yby w
	0 0 moveto
	0 NumLin rlineto	% yby w
	dup 0 rlineto		% yby w 
	0 lineto		% yby
	closepath
	0 exch copyarea
	grestore
    } def
    /SL {			% s SetFrameLabel
	{ /FrameLabel exch def
	  gsave
	  FrameCanvas setcanvas
	  BorderLeft 1 add
	  FrameHeight BorderTop sub 1 add
	  FrameWidth BorderLeft BorderRight add sub 2 sub
	  BorderTop 2 sub
	  rectpath FrameFillColor setcolor fill		% clear label area
	  /paintframelabel self send PaintFocus		% new label & focus
	  grestore
	} MyWindow send
    } def
    /VB {			% visible bell
#ifdef OVERLAYOPS
	overlaydraw clippath fill
	overlayerase clippath fill
#else
	gsave
	5 setrasteropcode				% PIX_NOT(PIX_SRC) = 5
	clippath fill clippath fill
	grestore
#endif
    } def
    /AM {			% onoff => -		(define auto margins)
	0 ne {(Auto-Margins Off)}{(Auto-Margins On)} ifelse
	MENU_MARGINS exch {(\374) print}
	    /changeitem MyWindow /ClientMenu get send
    } def
    /PM {			% onoff => -		(define page mode)
	0 ne {(Page Mode Off)}{(Page Mode On)} ifelse
	MENU_PAGEMODE exch {(\375) print}
	    /changeitem MyWindow /ClientMenu get send
    } def
    /resetscale {
%	7 dict begin	% local storage
	initgraphics
	clippath pathbbox
%	dup /can_pix_ht exch def		% Remember canvas height
%	exch dup /can_pix_wd exch def exch	% Remember canvas width
	0 1 index translate 1 -1 scale
	l .3 add div floor 1 max exch c div floor 1 max exch scale pop pop
	0 1 translate
	textcolor setcolor
	% See if can avoid all the font rescaling interations
	/font_scale_fac -1 def	
%	systemdict /pstdict currentdict put
	{ % Loop looking for a font that fits the screen without overlap
		/Courier findfont dup setfont
		[ ( ) stringwidth pop 1 exch dup 0 eq { pop 1 } { div } ifelse
		0 0 font_scale_fac 0 0]
		makefont dup setfont
		% See if the font is going to fit
		fontheight 1 le { exit } if
		font_scale_fac -.1 ge { exit } if
		/font_scale_fac font_scale_fac .1 add def
	} loop
	% Remember lastest values
%	/Last_font_scale_fac font_scale_fac store
%	/Last_pix_ht can_pix_ht store
%	/Last_pix_wd can_pix_wd store
%	end	% of local storage
	/maxy currentfont fontascent neg def
	clear
	childprocess dup null ne {
	    MySetTransform createevent copy dup begin
		/Action matrix currentmatrix def
		/Timestamp lasteventtime def
	    end sendevent
	} {
	    pop
	} ifelse
    } def
    /setselcontents {	% str start-ix last-ix rank-ix
	dup MyRankBackmap exch get 5 1 roll
	MySelections exch get dup begin
	    5 1 roll
	    % We can't maintain these values so skip it
	    pop % /SelectionLastIndex		exch	def
	    pop % /SelectionStartIndex	exch		def
	    /ContentsAscii		exch		def
	    /SelectionHolder		childprocess	def
	    /Canvas			currentcanvas	def
	end
	setselection
    } def
    resetscale
    ( ) 0 0 CU
cdef StartInput() {
    systemdict /Selections known {
        currentcanvas addkbdinterests pop
        currentcanvas addselectioninterests pop
	currentcanvas addfunctionstringsinterest pop 
    } {
        /Ascii 128 dict def
        /Template createevent def
        Ascii begin
        0 1 127 { dup def } for
        end
        Template
        Ascii seteventname
        currentcanvas seteventcanvas
        expressinterest
    } ifelse
    MySetTransform expressinterest
    /map MyWindow send

    /MyEventActions 20 dict dup begin
    
	/Ignore {
	} def
	
	/AcceptFocus {
    %       true /reflectfocus MyWindow send
	} def
	/RestoreFocus {
    %       true /reflectfocus MyWindow send
	} def
    
	/SetTransform {
	    Action dup null ne {
		setmatrix
	    } {
		pop
	    } ifelse
	} def
	    
	/SetSelectionAt {
	   MyInputMap /SetSelectionAt get print
	   Action begin
	       X =
	       Y maxy sub =
	       Rank load =
    %          Size =
    %          PendingDelete load =
    %          Preview load =
	   end
	} def
	
	/ExtendSelectionTo {
	   MyInputMap /ExtendSelectionTo get print
	   Action begin
	       X =
	       Y maxy sub =
	       Rank load =
    %          Size =
    %          PendingDelete load =
    %          Preview load =
	   end
	} def
	    
	/DeSelect {
	    Action /InputFocus eq {
    %		    false /reflectfocus MyWindow send
	    } {
		MyInputMap /DeSelect get print
		Action load =
	    } ifelse
	} def
	
	/SelectionRequest {
    %              MyInputMap /SelectionRequest get print
    %              % beware race condition because we don't wait for the reply
	} def
	
	/InsertValue        {
	    Action print
	} def
	
	/Default    {
	    ( ) dup 0 Name put print
	} def
    end def
        
    {   awaitevent
%	systemdict /_termlogger known { _termlogger } if
        begin		% event
        MyEventActions Name 2 copy known not {
            pop /Default
        } if
        get exec
        end
    } loop
} fork /childprocess exch store
cdef ReInitialize() resetscale
cdef CursorUp(x,y,cstring c) c x y CU
cdef CursorDown(x,y,cstring c) c x y CD
cdef PaintChars(x,y,cstring str)	x y moveto str show
cdef UnderLine(x,y,w)	w x y UL
cdef WhiteLine(x,y,w)	w x y WL
cdef EraseRectangle(x,y,w)	w x y ER
cdef CopyLines(yfrom, yby, w, nl) yby w yfrom nl CL
cdef BeginRepair() BRP
cdef EndRepair() ERP
cdef SetFrameLabel(string str) str SL
cdef SetSelContents(r, s, l, cstring str)	str s l r setselcontents
cdef RingBell() VB		% no audible bell as yet
cdef VisibleBell() VB
cdef SetPageMode(onoff) onoff PM
cdef SetAutoMargins(onoff) onoff AM

/*
 * This file is a product of Sun Microsystems, Inc. and is provided for
 * unrestricted use provided that this legend is included on all tape
 * media and as a part of the software program in whole or part.  Users
 * may copy or modify this file without charge, but are not authorized to
 * license or distribute it to anyone else except as part of a product
 * or program developed by the user.
 * 
 * THIS FILE IS PROVIDED AS IS WITH NO WARRANTIES OF ANY KIND INCLUDING THE
 * WARRANTIES OF DESIGN, MERCHANTIBILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE, OR ARISING FROM A COURSE OF DEALING, USAGE OR TRADE PRACTICE.
 * 
 * This file is provided with no support and without any obligation on the
 * part of Sun Microsystems, Inc. to assist in its use, correction,
 * modification or enhancement.
 * 
 * SUN MICROSYSTEMS, INC. SHALL HAVE NO LIABILITY WITH RESPECT TO THE
 * INFRINGEMENT OF COPYRIGHTS, TRADE SECRETS OR ANY PATENTS BY THIS FILE
 * OR ANY PART THEREOF.
 * 
 * In no event will Sun Microsystems, Inc. be liable for any lost revenue
 * or profits or other special, indirect and consequential damages, even
 * if Sun has been advised of the possibility of such damages.
 * 
 * Sun Microsystems, Inc.
 * 2550 Garcia Avenue
 * Mountain View, California  94043
 */

#ifndef lint
static char sccsid[] = "@(#)selection.c 9.4 88/01/19 Copyright 1987 Sun Micro";
#endif

/*
 * Copyright (c) 1987 by Sun Microsystems, Inc. 
 */

/*-
	selection.c

	selection.c, Fri Feb 27 23:51:49 1987

		Robin Schaufler,
		Sun Microsystems
 */

#ifdef	notdef
#define Diagnose
#endif
/*
 * Selection module 
 *
 * The external interfaces of this module are
 * tc_select_at(col, row, rank)
 * tc_extend_to(col, row, rank)
 * tc_deselect(rank)
 */

#include	<sys/types.h>
#ifdef REF
#include	<ref/config.h>
#endif
#include	"screen.h"
#include	"tcap.h"

extern u_short     CharsPerLine;
extern u_short     LinesPerScreen;
extern struct pair Dot;
static char	  *contents = NULL;

static u_char      rankModes[4] = { 0, PrimSelMode, SecnSelMode, 0 };

tc_init_selection()
{

	contents = (char *)malloc((CharsPerLine * LinesPerScreen) + 1);
}

/*
 * For simplicity sake, selections are maintained as character properties.
 * This way, when the screen contents change, no ranges need to be updated.
 * However this is slow, when doing extends, exactly the operation you
 * want to be fast; eventually a set of ranges should be maintained.
 */

static int
SelectAt(col, row, rank, contents)
    register int	col, row, rank;
    char *contents;
{
    register struct line *linep;

    if (col >= CharsPerLine)
	col = CharsPerLine - 1;
    else if (col < 0)
	col = 0;
    if (row >= LinesPerScreen)
	row = LinesPerScreen - 1;
    else if (row < 0)
	row = 0;
    linep = screen[row].lineptr;
    if (col > linep->length) {
	col = linep->length;
	linep->body[col] = ' ';
    }
    linep->prop[col] |= rankModes[rank];
    if (linep->changeposition > col)
	linep->changeposition = col;
    if (contents) {
	*contents = linep->body[col];
	if (col == linep->length)
	    *contents = '\n';
    }
    return 1;
}

static
Deselect(rank)
    register int rank;
{
    register int i;
    for (i = 0; i < LinesPerScreen; i++) {
	register struct line *l = screen[i].lineptr;
	register int j;

	for (j = 0; j <= l->length; j++) {
	    if (l->prop[j] & rankModes[rank]) {
		if (l->changeposition > j)
		    l->changeposition = j;
		l->prop[j] &= ~rankModes[rank];
	    }
	}
    }
}

static struct range
FindRange(rank)
    register int rank;
{
    register int i;
    struct range range;

    range.first.x = range.first.y = -1;
    range.last_plus_one.x = range.last_plus_one.y = -1;

    for (i = 0; i < LinesPerScreen; i++) {
	register struct line *l = screen[i].lineptr;
	register int j;

	for (j = 0; j <= l->length; j++) {
	    if (range.first.x == -1) {
		if (l->prop[j] & rankModes[rank]) {
		    range.first.x = j;
		    range.first.y = i;
		}
	    } else {
		if (! (l->prop[j] & rankModes[rank])) {
		    range.last_plus_one.x = j;
		    range.last_plus_one.y = i;
		    break /* for j */;
		}
	    }
	}
	if (range.last_plus_one.x > -1)
	    break;
    }
    if (range.first.x > -1 && range.last_plus_one.x == -1) {
	range.last_plus_one.y = LinesPerScreen - 1;
	range.last_plus_one.x
	    = screen[LinesPerScreen-1].lineptr->length;
    }
    return range;
}

static int
GetRangeContent(rangep, contents)
    register struct range *rangep;
    char *contents;
{
    register int i, len = -1;
    register struct line *l;
    register char *cp = contents;

    if (!contents || !rangep || rangep->first.x == -1)
	return -1;
    i = rangep->first.y;
    l = screen[i].lineptr;
    if (rangep->last_plus_one.y == i) {
	len = rangep->last_plus_one.x;
	if (len > l->length)
	    len = l->length;
    } else {
	len = l->length;
    }
    len -= rangep->first.x;
    strncpy(cp, l->body + rangep->first.x, len);
    cp += len;

    for (i = rangep->first.y + 1; i < rangep->last_plus_one.y; i++) {
	l = screen[i].lineptr;
	if (screen[i-1].lineptr->length < CharsPerLine)
	    *cp++ = '\n';
	strncpy(cp, l->body, l->length);
	cp += l->length;
    }

    if (i == rangep->last_plus_one.y) {
	l = screen[i].lineptr;
	if (i > rangep->first.y) {
	    if (screen[i-1].lineptr->length < CharsPerLine)
		*cp++ = '\n';
	    if ((len = rangep->last_plus_one.x) > 0) {
		if (len > l->length)
		    len = l->length;
		strncpy(cp, l->body, len);
		cp += len;
	    }
	}
    } else {
	i = rangep->last_plus_one.y;
	l = screen[i].lineptr;
    }
    if (rangep->last_plus_one.x > l->length)
	*cp++ = '\n';
    len = cp - contents;
    return len;
}

#define RNG_INVALID	0x0000
#define RNG_BEFORE	0x0001
#define RNG_AFTER	0x0002
#define RNG_WITHOUT	(RNG_BEFORE|RNG_AFTER)
#define RNG_AT_LEFT	0x0004
#define RNG_AT_RIGHT	0x0008
#define RNG_AT_AN_END	(RNG_AT_LEFT|RNG_AT_RIGHT)
#define RNG_NEAR_LEFT	0x0010
#define RNG_NEAR_RIGHT	0x0020
#define RNG_WITHIN	(RNG_NEAR_LEFT|RNG_NEAR_RIGHT)
#define RNG_FIRST_FREE	0x0040

static unsigned int
Relate(row, col, rangep)
    register int	row, col;
    register struct range *rangep;
{
    int len1, len2;
    struct range newrange;
    register struct line *l;

    if (!rangep || rangep->first.x == -1)
	return RNG_INVALID;
    if (row < rangep->first.y)
	return RNG_BEFORE;
    if (row > rangep->last_plus_one.y)
	return RNG_AFTER;
    if (row == rangep->first.y) {
	if (col < rangep->first.x)
	    return RNG_BEFORE;
	if (col == rangep->first.x)
	    return RNG_AT_LEFT;
    }
    if (row == rangep->last_plus_one.y) {
	if (col >= rangep->last_plus_one.x)
	    return RNG_AFTER;
	if (col+1 == rangep->last_plus_one.x)
	    return RNG_AT_RIGHT;
    }
    /*
     * relation is RNG_WITHIN.  are we RNG_NEAR_LEFT or RNG_NEAR_RIGHT?
     * following code is slow but easy to write.
     */
    newrange.first = rangep->first;
    newrange.last_plus_one.y = row;
    l = screen[row].lineptr;
    if (col > l->length)
	col = l->length;
    newrange.last_plus_one.x = col;
    len1 = GetRangeContent(&newrange, contents);
    newrange.last_plus_one = rangep->last_plus_one;
    len2 = GetRangeContent(&newrange, contents);
    if (len1 < (len2 - len1))
	return RNG_NEAR_LEFT;
    else
	return RNG_NEAR_RIGHT;
}

static void
SetLineProp(linep, start, length, rank, relation)
    register struct line *linep;
    register int start, length, rank, relation;
{
    register int i;

    if (linep->changeposition > start)
	linep->changeposition = start;
    for (i = start; i < start + length; i++) {
	if (relation & RNG_WITHOUT)
	    linep->prop[i] |= rankModes[rank];
	else
	    linep->prop[i] &= ~rankModes[rank];
    }
}

static void
SetRange(rangep, rank, relation)
    struct range *rangep;
    int rank;
    unsigned int relation;
{
    register int i, len = -1;
    register struct line *l;

    if (!rangep || rangep->first.x == -1)
	return;
    i = rangep->first.y;
    l = screen[i].lineptr;
    if (rangep->last_plus_one.y == i) {
	len = rangep->last_plus_one.x;
    } else {
	len = l->length;
    }
    len -= rangep->first.x;
    SetLineProp(l, rangep->first.x, len, rank, relation);

    for (i = rangep->first.y + 1; i < rangep->last_plus_one.y; i++) {
	SetLineProp(l, l->length, 1, rank, relation);
	l = screen[i].lineptr;
	SetLineProp(l, 0, l->length, rank, relation);
    }

    if (i == rangep->last_plus_one.y && i > rangep->first.y) {
	SetLineProp(l, l->length, 1, rank, relation);
	l = screen[i].lineptr;
	SetLineProp(l, 0, rangep->last_plus_one.x, rank, relation);
    }
}

static int
ExtendTo(col, row, rank, contents)
    register int	col, row, rank;
    char *contents;
{
    register struct line *linep;
    struct range oldrange, diffrange, newrange;
    unsigned int relation;

    if (col >= CharsPerLine)
	col = CharsPerLine - 1;
    else if (col < 0)
	col = 0;
    if (row >= LinesPerScreen)
	row = LinesPerScreen - 1;
    else if (row < 0)
	row = 0;
    oldrange = FindRange(rank);
    if (oldrange.first.x == -1)
	return SelectAt(col, row, rank, contents);
    linep = screen[row].lineptr;
    if (col > linep->length)
	col = linep->length + 1;
    relation = Relate(row, col, &oldrange);
    if (relation & RNG_AT_AN_END) {
	/* nothing to do */
	return -1;
    }
    switch (relation) {
      case RNG_BEFORE:
	diffrange.first.y = row;
	diffrange.first.x = col;
	diffrange.last_plus_one = oldrange.first;
	newrange.first = diffrange.first;
	newrange.last_plus_one = oldrange.last_plus_one;
	break;
      case RNG_AFTER:
	diffrange.first = oldrange.last_plus_one;
	diffrange.last_plus_one.y = row;
	diffrange.last_plus_one.x = ++col;
	newrange.first = oldrange.first;
	newrange.last_plus_one = diffrange.last_plus_one;
	break;
      case RNG_NEAR_LEFT:
	diffrange.first = oldrange.first;
	diffrange.last_plus_one.y = row;
	diffrange.last_plus_one.x = col;
	newrange.first = diffrange.last_plus_one;
	newrange.last_plus_one = oldrange.last_plus_one;
	break;
      case RNG_NEAR_RIGHT:
	diffrange.first.y = row;
	diffrange.first.x = ++col;
	diffrange.last_plus_one = oldrange.last_plus_one;
	newrange.first = oldrange.first;
	newrange.last_plus_one = diffrange.first;
	break;
    }
    linep = screen[diffrange.first.y].lineptr;
    if (diffrange.first.x > linep->length)
	diffrange.first.x = linep->length;
    linep = screen[newrange.first.y].lineptr;
    if (newrange.first.x > linep->length)
	newrange.first.x = linep->length;
    SetRange(&diffrange, rank, relation);
    return GetRangeContent(&newrange, contents);
}

/* --------------- External Routines Below ------------------- */

tc_select_at(col, row, rank)
    int	col, row, rank;
{
    int		content_length;

    content_length = SelectAt(col, row, rank, contents);
    if (content_length > -1)
	SetSelContents(rank, 0,
	    content_length-1, contents, content_length);
}

tc_extend_to(col, row, rank)
    int	col, row, rank;
{
    int		content_length;

    content_length = ExtendTo(col, row, rank, contents);
    if (content_length > -1)
	SetSelContents(rank, 0,
	    content_length-1, contents, content_length);
}

tc_deselect(rank)
    int rank;
{
    if (rank == 1 || rank == 2)
	Deselect(rank);
}

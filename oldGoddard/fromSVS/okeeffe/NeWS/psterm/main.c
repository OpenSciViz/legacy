/*
 * This file is a product of Sun Microsystems, Inc. and is provided for
 * unrestricted use provided that this legend is included on all tape
 * media and as a part of the software program in whole or part.  Users
 * may copy or modify this file without charge, but are not authorized to
 * license or distribute it to anyone else except as part of a product
 * or program developed by the user.
 * 
 * THIS FILE IS PROVIDED AS IS WITH NO WARRANTIES OF ANY KIND INCLUDING THE
 * WARRANTIES OF DESIGN, MERCHANTIBILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE, OR ARISING FROM A COURSE OF DEALING, USAGE OR TRADE PRACTICE.
 * 
 * This file is provided with no support and without any obligation on the
 * part of Sun Microsystems, Inc. to assist in its use, correction,
 * modification or enhancement.
 * 
 * SUN MICROSYSTEMS, INC. SHALL HAVE NO LIABILITY WITH RESPECT TO THE
 * INFRINGEMENT OF COPYRIGHTS, TRADE SECRETS OR ANY PATENTS BY THIS FILE
 * OR ANY PART THEREOF.
 * 
 * In no event will Sun Microsystems, Inc. be liable for any lost revenue
 * or profits or other special, indirect and consequential damages, even
 * if Sun has been advised of the possibility of such damages.
 * 
 * Sun Microsystems, Inc.
 * 2550 Garcia Avenue
 * Mountain View, California  94043
 */

#ifndef lint
static	char sccsid[] = "@(#)main.c 9.7 88/01/19 Copyright 1985 Sun Micro";
#endif

/*
 * Copyright (c) 1985 by Sun Microsystems, Inc.
 */


#include <stdio.h>
#include <psio.h>
#include <signal.h>
#include <pwd.h>
#include <fcntl.h>
#include <sys/param.h>
#include <sys/ioctl.h>
#ifdef SYSVREF
#ifdef INTERLANTCP
#include <interlan/il_types.h>
#include <interlan/socket.h>
#include <interlan/in.h>
#include <interlan/netdb.h>
#else
#include <sys/types.h>
#endif
#else
#ifdef REF
#include <sys/types.h>
#endif
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#endif

#ifdef REF
#include <ref/config.h>
#endif

#if defined(SVR0) || defined(SVR3)
#define SYSVREF
#endif

#ifdef SYSVREF
#define	SIGCHLD	SIGCLD
#else
#include <sys/wait.h>
#endif

int	console;
static	int KillChild();
static	int ReapChild();
static	char *term = NULL;
static	int seed = 0;
static	int Persist;
static	int loginshell = 0;
static	int fixedsize = 0;
static	int tflag = 0;
static	char *framelabel = NULL;
static	char *iconlabel = NULL;
static	int xorg = -1;
static	int yorg = -1;
int	BackGround = 0;
int	PageMode = 0;			/* "pause on end of page" mode */
int	userCharsPerLine = -1;
int	userLinesPerScreen = -1;

main(argc,argv)
    int	argc;
    char **argv;
{
    char *s_name, **s_args, *termcap, *argv0 = argv[0];
    FILE *Client;
    PSFILE *Keyboard;
    extern char *getenv();
    extern PSFILE *tc_init_screen();
    extern FILE *spawn_slave();

    for (argc--, argv++; argc > 0 && argv[0][0] == '-'; argc--, argv++) {
	if (strcmp(argv[0], "-li") == 0) {
	    if (--argc > 0)
		userLinesPerScreen = atoi(*++argv);
	    continue;
	}
	if (strcmp(argv[0], "-co") == 0) {
	    if (--argc > 0)
		userCharsPerLine = atoi(*++argv);
	    continue;
	}
	if (strcmp(argv[0], "-pm") == 0 || strcmp(argv[0], "-ps") == 0) {
	    PageMode++;			/* enable page mode */
	    continue;
	}
	if (strcmp(argv[0], "-bg") == 0 || strcmp(argv[0], "-ga") == 0) {
	    BackGround++;		/* put ourself in the background */
	    continue;
	}
	if (strcmp(argv[0], "-w") == 0) {
	    Persist++;
	    continue;
	}
	if (strcmp(argv[0], "-t") == 0) {
	    if (--argc > 0) {
		term = *++argv;
		tflag++;
	    }
	    continue;
	}
	if (strcmp(argv[0], "-C") == 0) {
	    console++;
	    continue;
	}
	if (strcmp(argv[0], "-s") == 0) {
	    if (--argc > 0)
		seed = atoi(*++argv);
	    continue;
	}
	if (strcmp(argv[0], "-f") == 0) {
	    fixedsize++;
	    continue;
	}
	if (strcmp(argv[0], "-ls") == 0) {
	    loginshell++;
	    continue;
	}
	if (strcmp(argv[0], "-fl") == 0) {
	    if (--argc > 0)
		framelabel = *++argv;
	    continue;
	}
	if (strcmp(argv[0], "-il") == 0) {
	    if (--argc > 0)
		iconlabel = *++argv;
	    continue;
	}
	if (strcmp(argv[0], "-xy") == 0) {
	    if (--argc > 0)
		xorg = atoi(*++argv);
	    if (--argc > 0)
		yorg = atoi(*++argv);
	    if (xorg >= 0 && yorg >= 0)
		continue;
	}
	fprintf(stderr, "Usage: %s %s \\\n\t%s \\\n\t%s\n", argv0,
	     "[-w] [-f] [-t termtype] [-C] [-s seed]",
	     "[-fl framelabel] [-il iconlabel] [-li lines] [-co columns]",
	     "[-bg] [-ls] [-pm] [-xy x y] [command]");
	exit(-1);
    }
    if (loginshell) {
	char *s, *p, *malloc();
#ifndef SYSVREF
	char *rindex(), *index();
#else
#define index(s, c)		(char *)strchr(s, c)
#ifdef sgi /* Bug fix */
#define rindex(s, c)		(char *)strrchr(s, c)
#endif
#endif
	struct passwd *pw, *getpwuid();
	static char *loginargv[4];

	s_name = "/bin/sh";		/* default shell name */
	s_args = loginargv;

	s_args[0] = "-sh";
	s_args[1] = 0;
	if (pw = getpwuid(getuid())) {
	    if (pw->pw_dir)
		putenv("HOME", pw->pw_dir);
	    if (pw->pw_name) {
#ifdef SYSVREF
		putenv("LOGNAME", pw->pw_name);
#else
	        putenv("USER", pw->pw_name);
#endif
	    }
	    if (pw->pw_shell) {
		if (s = rindex(pw->pw_shell,'/')) {
		    char *p = malloc (strlen(s)+4);
		    if (p) {
			sprintf(p, "-%s", s+1);
			s_args[0] = p;
		    }
		    p = malloc(strlen(pw->pw_shell)+4);
		    if (p) {
			s_name = p;
			strcpy(s_name, pw->pw_shell);
		    }
		    putenv("SHELL",pw->pw_shell);
		}
	    }
	    endpwent();
	}
	if (argc > 0) {
	    int i, count = 0;
	    /*
	     * This goofy business in case someone wants to run complex
	     * shell commands after login-shell initialization...
	     */
	    for (i=0; i<argc; i++) {
		count += strlen(argv[i]);
	    }
	    if (s_args[2] = malloc(count+argc+4)) {
		s_args[2][0] = 0;
		for (i=0; i<argc; i++) {
		    if (i)
			strcat(s_args[2], " ");
		    strcat(s_args[2], argv[i]);
		}
		s_args[0]++;
		s_args[1] = "-c";
		s_args[3] = 0;
	    }
	}
	/*
	 * if $NEWSSERVER isn't defined and stdin is a socket, chances
	 * are we're being invoked with rsh, so figure out where the
	 * rsh command came from and use that for NEWSSERVER.
	 */
	if (getenv("NEWSSERVER")==0) {
	    struct hostent *hp;
	    struct sockaddr_in remote;
	    int n = sizeof remote;
	    if (getpeername(0, &remote, &n) == 0) {
		if (remote.sin_family == AF_INET) {
		    char srv[128];
		    sprintf(srv, "%lu.%d;",
			    ntohl(remote.sin_addr.s_addr), 2000);
		    hp = gethostbyaddr((char*) &remote.sin_addr,
				       sizeof (remote.sin_addr),
				       remote.sin_family);
		    if (hp)
			strcat(srv,hp->h_name);
		    putenv("NEWSSERVER", srv);
		}
	    }
	}
	if (framelabel == NULL) {
	    char *p;
	    static char labelbuf[128];
	    gethostname(labelbuf, sizeof labelbuf);
	    if ((p = index(labelbuf, '.')) != NULL)
		*p = 0;
	    if (argc > 0) {
		strcat(labelbuf, " ");
		strcat (labelbuf, argv[0]);
	    }	    
	    framelabel = labelbuf;
	}
	if (iconlabel == NULL)
	    iconlabel = framelabel;
    } else if (argc > 0) {
	s_name = *argv;
	s_args = argv;
    } else {
#ifndef SYSVREF
	static char *def_argv[] = { "csh", NULL};
#else
	static char *def_argv[] = { "-sh", NULL};
#endif

	s_args = def_argv;
	if ((s_name = getenv("SHELL")) == NULL) {
#ifndef SYSVREF
	    s_name = *def_argv;
	    s_args[0] = s_name;
#else
	    s_name = "sh";
#endif
	}
	else {
	    s_args[0] = s_name;
	}
    }
    if (term == NULL && (term = getenv("TERM")) == NULL
	|| strcmp(term,"PostScript")==0
	|| strcmp(term,"emacswindow")==0
	|| strcmp(term,"PS")==0) {
#ifdef RandomBehaviour
	/* Choose one at random - should scan termcap? */
	static char * def_term[] = {
	    "ansi", "h19", "wyse",
	};

	if (seed == 0)
	    seed = getpid();
	srand(seed);
	term = def_term[(rand()>>4)%((sizeof def_term)/(sizeof def_term[0]))];
#else
#ifdef sgi	/* XXX Our h19 terminfo entry causes tgetent to crash */
	term = "vt100";
#else
	term = "h19";
#endif
#endif
    }
    if (tc_initialize(term))
	Fatal("%s: Unknown terminal type or bad termcap description", term);
    if (BackGround) {
	int i;
	/*
	 * Close any extraneous files.
	 */
	for (i = getdtablesize(); i > 2; i--)
	    close(i);
    }
    if (xorg >= 0 && yorg >= 0 &&
	userLinesPerScreen > 0 && userCharsPerLine > 0)
	fixedsize = 1;
    if (xorg < 0)
	xorg = 0;
    if (yorg < 0)
	yorg = 0;
    Keyboard =
	tc_init_screen(term, fixedsize, xorg, yorg, framelabel, iconlabel);
    if (Keyboard == NULL)
	Fatal("Host does not respond");
    tc_initmodemenu();
    putenv("TERM", term);
    /* Flush cached TERMCAP value if not a filename and -t specified*/
    if (termcap = getenv("TERMCAP")) {
	int termcapfd;

	if (tflag && ((termcapfd = open(termcap, O_RDONLY, 0)) < 0))
	    unsetenv("TERMCAP");
	else
	    close(termcapfd);
    }
    if ((Client = spawn_slave(s_name, s_args)) == NULL)
	Fatal("Cannot spawn %s", s_name);
    if (signal(SIGINT, SIG_IGN) != SIG_IGN)
	signal(SIGINT, KillChild);
    if (signal(SIGQUIT, SIG_IGN) != SIG_IGN)
	signal(SIGQUIT, KillChild);
    signal(SIGHUP, KillChild);
    signal(SIGTERM, KillChild);
    signal(SIGCHLD, ReapChild);
    terminal(fileno(Client), psio_fileno(Keyboard));
    Exit(0);
}

Fatal(fmt, a1, a2, a3)
{

    fprintf(stderr, "psterm: ");
    fprintf(stderr, fmt, a1, a2, a3);
    fprintf(stderr, ".\n");
    exit(1);
}

Exit(status)
{

    CleanupPty();
    exit(status);
}

static
KillChild(sig)
{
    extern int pgrp;

    if (pgrp != 0)
	killpg(pgrp, sig);
    CleanupPty();
    signal(sig, SIG_DFL);
    kill(0, sig);
}

static
ReapChild()
{
#ifdef SYSVREF
    int status, pid = wait(&status);
    signal(SIGCHLD, ReapChild);
#else
    union wait status;
    int pid = wait3(&status, WNOHANG, 0);
#endif

    if (pid < 0)
	perror("fruitless wait3");
    else if (Persist == 0)
	Exit(0);
}

#ifndef HAVEPUTENV
putenv(name, value)
char       *name, *value; {
    register char *p;
    register    len;
    register char **ap;
    register char **new;
    register char *buf;
    static      alloced = 0;
    extern char **environ;

    len = strlen(name);
    buf = (char *) malloc(len + strlen(value) + 2);
    sprintf(buf, "%s=%s", name, value);
    for (ap = environ; *ap; ap++)
	if (strncmp(*ap, buf, len+1) == 0) {
	    *ap = buf;
	    return;
	}
    len = ap - environ;
    new = (char **) malloc((len + 2) * sizeof(char *));
    bcopy(environ, new, len * sizeof(char *));
    new[len] = buf;
    new[len + 1] = 0;
    if (alloced)
	free(environ);
    environ = new;
}

unsetenv(name)
char       *name; {
    register    len;
    register char **ap;
    register char **new;
    extern char **environ;

    len = strlen(name);
    for (new = ap = environ; *ap; ap++) {
	if (strncmp(*ap, name, len) != 0) {
	    *new++ = *ap;
	}
    }
    if (new < ap) {
	*new = 0;
    }
}
#endif

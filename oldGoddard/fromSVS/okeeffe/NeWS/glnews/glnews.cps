#define SPIN_TAG 0
cdef ps_create_panel() 

systemdict /Item known not { (/usr/NeWS/lib/NeWS/liteitem.ps) run } if


/upnotify {
	getvalue 1 eq {0 setvalue} if	
	SPIN_TAG tagprint
	1 typedprint
} def

/downnotify {
	getvalue 1 eq {0 setvalue} if	
	SPIN_TAG tagprint
	2 typedprint
} def

/leftnotify {
	getvalue 1 eq {0 setvalue} if	
	SPIN_TAG tagprint
	3 typedprint
} def

/rightnotify {
	getvalue 1 eq {0 setvalue} if	
	SPIN_TAG tagprint
	4 typedprint
} def

/centernotify {
	getvalue 1 eq {0 setvalue} if	
	SPIN_TAG tagprint
	5 typedprint
} def

/FillColor .64 .64 .75 rgbcolor def
/RadioSize 20 def
/SquareRadioItem CycleItem []
classbegin
    /new { % label loc notify can width height
        [/squareoff /squareon] 6 1 roll % get stack right for CycleItem
        /new super send begin
	    /EraseToCycle false def
	    /LabelX 0 def
	    /LabelY 0 def
	    /LabelHeight 0 def
	    /LabelWidth 0 def
	    /LabelGap 0 def
	    /ItemBorder 1 def
            /ItemFillColor .64 .64 .75 rgbcolor def
%	    /ItemLabelFont /Helvetica-BoldOblique findfont 14 scalefont def
            currentdict
        end
    } def
classend def

/squareradio { % draw? off?  => -
    gsave
    exch {
	.35 .35 .35 rgbcolor setcolor
	4 0 moveto 16 16 rect fill
	1 setlinewidth
	0 setgray
	0 3 moveto
	16 16 rect stroke
	.5 3.5 moveto 15 15 rect
	1 .7 .7 rgbcolor setcolor
	{.6 0 1 rgbcolor setcolor} if fill 
    }
    {pop RadioSize RadioSize} ifelse
    grestore
} def

/squareoff {true squareradio} def % draw? => -
/squareon {false squareradio} def % draw? => -

/UpRadioItem CycleItem []
classbegin
    /new { % label loc notify can width height
        [/upoff /upon] 6 1 roll % get stack right for CycleItem
        /new super send begin
	    /EraseToCycle false def
            /ItemFillColor .64 .64 .75 rgbcolor def
	    /ItemLabelFont /Helvetica-BoldOblique findfont 14 scalefont def
            currentdict
        end
    } def
classend def

/upradio { % draw? off?  => -
    gsave
    exch {
	.35 .35 .35 rgbcolor setcolor
	4 0 moveto 12 14 lineto 20 0 lineto 4 0 lineto fill
	0 3 moveto 8 17 lineto 16 3 lineto 0 3 lineto 
	1 .7 .7 rgbcolor setcolor
	{.5 0 1 rgbcolor setcolor} if fill 
	0 0 0 setrgbcolor
	1 setlinewidth
	0 3 moveto 8 17 lineto 16 3 lineto 0 3 lineto stroke
    }
    {pop RadioSize RadioSize} ifelse
    grestore
} def

/upoff {true upradio} def % draw? => -
/upon {false upradio} def % draw? => -


/DownRadioItem CycleItem []
classbegin
    /new { % label loc notify can width height
        [/downoff /downon] 6 1 roll % get stack right for CycleItem
        /new super send begin
	    /EraseToCycle false def
            /ItemFillColor .64 .64 .75 rgbcolor def
	    /ItemLabelFont /Helvetica-BoldOblique findfont 14 scalefont def
            currentdict
        end
    } def
classend def

/downradio { % draw? off?  => -
    gsave
    exch {
	.35 .35 .35 rgbcolor setcolor
	4 14 moveto 12 0 lineto 20 14 lineto 4 14 lineto fill
	0 17 moveto 8 3 lineto 16 17 lineto 0 17 lineto 
	1 .7 .7 rgbcolor setcolor
	{.5 0 1 rgbcolor setcolor} if fill 
	0 0 0 setrgbcolor
	1 setlinewidth
	0 17 moveto 8 3 lineto 16 17 lineto 0 17 lineto stroke
    }
    {pop RadioSize RadioSize} ifelse
    grestore
} def

/downoff {true downradio} def % draw? => -
/downon {false downradio} def % draw? => -

/LeftRadioItem CycleItem []
classbegin
    /new { % label loc notify can width height
        [/leftoff /lefton] 6 1 roll % get stack right for CycleItem
        /new super send begin
	    /EraseToCycle false def
            /ItemFillColor .64 .64 .75 rgbcolor def
	    /ItemLabelFont /Helvetica-BoldOblique findfont 14 scalefont def
            currentdict
        end
    } def
classend def

/leftradio { % draw? off?  => -
    gsave
    exch {
	.35 .35 .35 rgbcolor setcolor
	4 8 moveto 18 0 lineto 18 16 lineto 4 8 lineto fill
	0 11 moveto 14 19 lineto 14 3 lineto 0 11 lineto 
	1 .7 .7 rgbcolor setcolor
	{.5 0 1 rgbcolor setcolor} if fill 
	0 0 0 setrgbcolor
	1 setlinewidth
	0 11 moveto 14 19 lineto 14 3 lineto 0 11 lineto stroke
    }
    {pop RadioSize RadioSize} ifelse
    grestore
} def

/leftoff {true leftradio} def % draw? => -
/lefton {false leftradio} def % draw? => -

/RightRadioItem CycleItem []
classbegin
    /new { % label loc notify can width height
        [/rightoff /righton] 6 1 roll % get stack right for CycleItem
        /new super send begin
	    /EraseToCycle false def
            /ItemFillColor .64 .64 .75 rgbcolor def
	    /ItemLabelFont /Helvetica-BoldOblique findfont 14 scalefont def
            currentdict
        end
    } def
classend def

/rightradio { % draw? off?  => -
    gsave
    exch {
	.35 .35 .35 rgbcolor setcolor
	18 8 moveto 4 0 lineto 4 16 lineto 18 8 lineto fill
	14 11 moveto 0 3 lineto 0 19 lineto 14 11 lineto 
	1 .7 .7 rgbcolor setcolor
	{.5 0 1 rgbcolor setcolor} if fill 
	0 0 0 setrgbcolor
	1 setlinewidth
	14 11 moveto 0 3 lineto 0 19 lineto 14 11 lineto stroke
    }
    {pop RadioSize RadioSize} ifelse
    grestore
} def

/rightoff {true rightradio} def % draw? => -
/righton {false rightradio} def % draw? => -

/createitems {
    /items 60 dict dup begin

        /radiocenter () /Left /centernotify can 0 0
        /new SquareRadioItem send 67 55 /move 3 index send def

        /radiouparrow (Up) /Bottom /upnotify can 0 0
        /new UpRadioItem send 66 80 /move 3 index send def

        /radiodownarrow (Down) /Top /downnotify can 0 0
        /new DownRadioItem send 57 7 /move 3 index send def

        /radioleftarrow (Left) /Right /leftnotify can 0 0
        /new LeftRadioItem send 9 54 /move 3 index send def

        /radiorightarrow (Right) /Left /rightnotify can 0 0
        /new RightRadioItem send 95 54 /move 3 index send def

    end def
} def




% Create and size a window.

    /win framebuffer /new DefaultWindow send def
    {
	/CloseControl null def
	/StretchControl null def
	/CreateFrameControls { } def
	/PaintFrameControls { } def
	/MoveFrameControls { } def
    	/PaintClient {FillColor fillcanvas items paintitems} def
        /PaintFrame {FrameFillColor fillcanvas items paintitems} def
        /BorderLeft 4 def
        /BorderRight 5 def
        /BorderTop 5 def
        /BorderBottom 4 def
        /FrameFillColor .3 .3 .3 rgbcolor def
        /FrameLabel () def
        /IconLabel (Panel) def
        /CreateFrameMenu { % - => - (Create frame menu)
 	  /FrameMenu [
	    (Pop)		{/totop ThisWindow send}
	    (Push)		{/tobottom ThisWindow send}
	    (Move)		{/slide ThisWindow send}
	    (Stow) 		{/flipiconic ThisWindow send}
	  ] /new DefaultMenu send def
	  (Panel) /settitle FrameMenu send
        } def
    } win send				
    400 350 180 140 /reshape win send		% Shape it.
    /can win /ClientCanvas get def		% Get the window canvas
    
% Create button.
    createitems

% Map the window and create item event manager.

    /map win send		
    /itemmgr items forkitems def

cdef ps_rotate_obj(int spin) => SPIN_TAG (spin)

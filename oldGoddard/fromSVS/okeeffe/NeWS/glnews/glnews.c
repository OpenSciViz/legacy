/*  GL/NeWS UI Demo
/*  
/*  9/16/88
/*
/*  Dave Ciemiewicz:  Geometry
/*  Andrew Myers:     Lighting Models
/*  Robert Reimann:   NeWS Interface and Controls  */


#include "sys/types.h"
#include "sys/time.h"
#include "gl.h"
#include "device.h"
#include "psio.h"
#include "glnews.h"
#include "math.h"

#define SHIFT 0.10
#define UP 1
#define DOWN 2
#define LEFT 3
#define RIGHT 4
#define STOP 5
#define OURLIGHT 1
#define OURMODEL 1

 Matrix idmat = {{1.,0.,0.,0.},{0.,1.,0.,0.},{0.,0.,1.,0.},{0.,0.,0.,1.}};
float ourlight[] = {	LCOLOR, 1., 1., 1.,
			POSITION, 1., 1., 1., 0.,
			LMNULL
		  };

float ourmodel[] = {	
			LOCALVIEWER, 0.0,
			AMBIENT, 0.4, 0.4, 0.4,
			LMNULL
		};

float blue_material[] = {	AMBIENT, 0.0, 0.3, 1.0,
			SPECULAR, 1., 1., 1.,
			DIFFUSE, 0.0, 0.3, 1.0,
			SHININESS, 64.0,
			LMNULL
		};
float red_material[] = {	AMBIENT, 1.0, 0.0, 0.2,
			SPECULAR, 1., 1., 1.,
			DIFFUSE, 1.0, 0.0, 0.2,
			SHININESS, 64.0,
			LMNULL
		};
float green_material[] = {	AMBIENT, 0.0, 1.0, 0.3,
			SPECULAR, 1., 1., 1.,
			DIFFUSE, 0.0, 1.0, 0.3,
			SHININESS, 64.0,
			LMNULL
		};

float	pyramidface1[][3] = {
		 0.20,  0.00,  0.00,
		 0.50+SHIFT, -0.40, -0.40,
		 0.50+SHIFT,  0.40, -0.40
	};

float	pyramidface2[][3] = {
		 0.20,  0.00,  0.00,
		 0.50+SHIFT,  0.40, -0.40,
		 0.50+SHIFT,  0.40,  0.40
	};

float	pyramidface3[][3] = {
		 0.20,  0.00,  0.00,
		 0.50+SHIFT,  0.40,  0.40,
		 0.50+SHIFT, -0.40,  0.40
	};

float	pyramidface4[][3] = {
		 0.20,  0.00,  0.00,
		 0.50+SHIFT, -0.40,  0.40,
		 0.50+SHIFT, -0.40, -0.40
	};

float	pyramidbase[][3] = {
		 0.50+SHIFT, -0.40, -0.40,
		 0.50+SHIFT, -0.40,  0.40,
		 0.50+SHIFT,  0.40,  0.40,
		 0.50+SHIFT,  0.40, -0.40
	};

float	pipe[][3] = {
		 0.50+SHIFT,  0.05, -0.05,
		 0.50+SHIFT, -0.05, -0.05,
		 1500.0, -0.05, -0.05,
		 1500.0,  0.05, -0.05,
	};

 float facex,facey,facez;
 void calcnorm()
 {
 	float ax,ay,az,bx,by,bz,cx,cy,cz,s;
 	ax = pyramidface1[1][0] - pyramidface1[0][0];
 	ay = pyramidface1[1][1] - pyramidface1[0][1];
 	az = pyramidface1[1][2] - pyramidface1[0][2];
 
 	bx = pyramidface1[2][0] - pyramidface1[1][0];
 	by = pyramidface1[2][1] - pyramidface1[1][1];
 	bz = pyramidface1[2][2] - pyramidface1[1][2];
 
 	cx = ay*bz - az*by;
 	cy = az*bx - ax*bz;
 	cz = ax*by - ay*bx;
 
 	s = sqrt(cx*cx + cy*cy + cz*cz);
 	facex = -cx/s;
 	facey = -cy/s;
 	facez = -cz/s;
 }
 
 mypolf(n, parray, x, y, z)
 
 int n;
 float *parray;
 float x,y,z;
 
 {
 	float *p,temp[3];
	int i;
 	temp[0] = x;
 	temp[1] = y;
 	temp[2] = z;
 	p = parray;
 	bgnpolygon();
 	for (i=0;i<n;i++) {
 		n3f(temp);
 		v3f(p);
 		p += 3;
 	}
 	endpolygon();
 }

void
drawpyramidf()
{
 	mypolf(3, pyramidface1, facex, facey, facez);
 	mypolf(3, pyramidface2, facex, -facez, facey);
 	mypolf(3, pyramidface3, facex, -facey, -facez);
 	mypolf(3, pyramidface4, facex, facez, -facey);
 	mypolf(4, pyramidbase, 1., 0., 0.);

	pushmatrix();
	mypolf(4, pipe, 0.,0.,-1.);
	rotate(900, 'x');
	mypolf(4, pipe, 0.,0.,-1.);
	rotate(900, 'x');
	mypolf(4, pipe, 0.,0.,-1.);
	rotate(900, 'x');
	mypolf(4, pipe, 0.,0.,-1.);
	popmatrix();
}


void
drawarrows()
{
	pushmatrix();

	lmbind(MATERIAL,RED);
	drawpyramidf();
	rotate(1800, 'z');
	drawpyramidf();

	lmbind(MATERIAL,GREEN);
	rotate(900, 'z');
	drawpyramidf();
	rotate(1800, 'z');
	drawpyramidf();

	lmbind(MATERIAL,BLUE);
	rotate(900, 'y');
	drawpyramidf();
	rotate(1800, 'y');
	drawpyramidf();

	popmatrix();
}

long	gid;

void
main()
{
	short val;
	int dev;
	int mx;
	int dx;
	int my;
	int dy;
	int pressed;
	int spin;
	int psqueue;
	struct timeval timeout;
	fd_set readfds;

	ps_open_PostScript();
/*	noborder();
	prefposition(285, 520, 465, 647);
*/
	keepaspect(40,31);

	gid = winopen("GL/NeWS");
	wintitle("GL/NeWS Demo");

	calcnorm();
 	lmdef(DEFLIGHT,OURLIGHT,0,ourlight);
 	lmdef(DEFLMODEL,OURMODEL,0,ourmodel);
 	lmdef(DEFMATERIAL,GREEN,0,green_material);
 	lmdef(DEFMATERIAL,BLUE,0,blue_material);
 	lmdef(DEFMATERIAL,RED,0,red_material);

	ps_create_panel();
	ps_flush_PostScript();
 	lmbind(LMODEL,OURMODEL);
 	mmode(MPROJECTION);
	perspective(400, 1.33, 1.0, 10.0);
 	mmode(MVIEWING);
 	loadmatrix(idmat);
	lookat(0.0, 0.0, -5.0, 0.0, 0.0, 0.0, 0);
 	lmbind(LIGHT1,OURLIGHT);
	zfunction(ZF_LEQUAL);
	setdepth(0xC000, 0x3FFF);
	zbuffer(1);
	RGBmode();
	doublebuffer();
	gconfig();
	mx = 0;
	my = 0;
	dx = 0;
	dy = 0;
	timeout.tv_sec = 0;
	timeout.tv_usec = 1;
  	FD_ZERO(&readfds);
  	
	while (1) {

		while (qtest()) {
			dev = qread(&val);
			switch (dev) {
			case REDRAW:
				reshapeviewport();
				break;
			default:
				break;
			}
		}
	
		zclear();
		RGBcolor(162,162,192);
		clear();

		mmode(MPROJECTION);
		perspective(400, 1.33, 1.0, 10.0);
		mmode(MVIEWING);
		loadmatrix(idmat);
		lookat(0.0, 0.0, -5.0, 0.0, 0.0, 0.0, 0);
		lmbind(LIGHT1,OURLIGHT);

		pushmatrix();
			mx = mx + dx;
			my = my + dy;
			rotate(mx*3600/1000, 'y');
			rotate(my*3600/1000, 'x');
			drawarrows();
		popmatrix();

		swapbuffers();

		psqueue = psio_fileno(PostScriptInput);
		FD_SET(psqueue, &readfds);
		if (select(32,&readfds,0,0,&timeout) > 0) {
		if (psio_eof(PostScriptInput)) {
			gexit();
                } else
		if (ps_rotate_obj(&spin)) {
			switch (spin) {
			case UP:
				dy = dy + 1;
				break;
			case DOWN:
				dy = dy - 1;
				break;
			case LEFT:
				dx = dx - 1;
				break;
			case RIGHT:
				dx = dx + 1;
				break;
			case STOP:
				dx = 0;
				dy = 0;
				mx = 0;
				my = 0;
				break;
			default:
				break;
			} 
		}  
		} 
	}
}

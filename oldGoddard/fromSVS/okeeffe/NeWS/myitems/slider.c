#include "sys/types.h"
#include "sys/time.h"
#include "gl.h"
#include "device.h"
#include "psio.h"
#include "slider.h"
#include "math.h"

main()
{
int tag, itemval;
char *itemlabel="                                                             foo";

  if( ps_open_PostScript() == 0 )
  {
	printf("Unable to connect to NeWS server\n");
	exit(1);
  }
  ps_init();
  ps_flush_PostScript();
  	
  while( TRUE ) 
  {
    if( psio_eof(PostScriptInput) ) 
    {
	ps_close_PostScript();
	gexit();
    }
    else
    {
	if( tag = ps_notify(itemlabel,&itemval) )
	{
	  printf("tag= %d\n",tag);
	  printf("itemlabel= %s\n",itemlabel);
	  printf("itemval= %d\n",itemval);
	}
	else printf("funny tag returned= %d\n",tag);
    }
  }
}

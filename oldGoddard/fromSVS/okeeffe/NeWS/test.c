#include "test.h"

main()
{
float stargray, fillgray;

  if( ps_open_PostScript() == NULL )
  {
    fprintf(stderr,"Cannot connect to NeWS server\n");
    exit(1);
  }

  init();

  while( !psio_error(PostScriptInput) )
  {
    if( get_grays(&stargray, &fillgray) )
	set_grays(stargray, fillgray);
    else if( psio_eof(PostScriptInput) )
	break;
    else
    {
	fprintf("Strange stuff!\n");
	break;
    }
  }
  ps_close_PostScript();
}	

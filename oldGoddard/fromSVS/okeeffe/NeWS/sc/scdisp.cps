%/*
% * This file is a product of Sun Microsystems, Inc. and is provided for
% * unrestricted use provided that this legend is included on all tape
% * media and as a part of the software program in whole or part.  Users
% * may copy or modify this file without charge, but are not authorized to
% * license or distribute it to anyone else except as part of a product
% * or program developed by the user.
% * 
% * THIS FILE IS PROVIDED AS IS WITH NO WARRANTIES OF ANY KIND INCLUDING THE
% * WARRANTIES OF DESIGN, MERCHANTIBILITY AND FITNESS FOR A PARTICULAR
% * PURPOSE, OR ARISING FROM A COURSE OF DEALING, USAGE OR TRADE PRACTICE.
% * 
% * This file is provided with no support and without any obligation on the
% * part of Sun Microsystems, Inc. to assist in its use, correction,
% * modification or enhancement.
% * 
% * SUN MICROSYSTEMS, INC. SHALL HAVE NO LIABILITY WITH RESPECT TO THE
% * INFRINGEMENT OF COPYRIGHTS, TRADE SECRETS OR ANY PATENTS BY THIS FILE
% * OR ANY PART THEREOF.
% * 
% * In no event will Sun Microsystems, Inc. be liable for any lost revenue
% * or profits or other special, indirect and consequential damages, even
% * if Sun has been advised of the possibility of such damages.
% * 
% * Sun Microsystems, Inc.
% * 2550 Garcia Avenue
% * Mountain View, California  94043
% */

#define redraw_tag 1
#define char_tag 2
#define mouse_tag 3

cdef ps_initialize()
    /win framebuffer /new DefaultWindow send def
    false setprintermatch
    { 	/FrameLabel (Spreadsheet) def
	/FixFrame {	1 tagprint
			ClientCanvas setcanvas clippath pathbbox
			typedprint typedprint pop pop
		} def
	/IconLabel FrameLabel def
    } win send
    /reshapefromuser win send
    win /ClientCanvas get /Color get {
	/BackGround 1 1 1 rgbcolor def
	/ForeGround 0 0 0 rgbcolor def
	/ErrText 0 1 1 hsbcolor def
	/Boundaries 0 25 1 hsbcolor def
    } { 
	/BackGround 1 1 1 rgbcolor def
	/ForeGround 0 0 0 rgbcolor def
	/ErrText ForeGround def
	/Boundaries ForeGround def
    } ifelse
    % win /FrameFillColor BackGround put			% sgi
    /map win send
    /font /Times-Roman findfont 12 scalefont def
    /BRP { gsave win /FrameCanvas get setcanvas
	   damagepath clipcanvas /paintframe win send
	   grestore } def
    /ERP { gsave win /FrameCanvas get setcanvas
	   clipcanvas grestore } def
    /DRL { 0 exch moveto (   ) cvs show } def
    /STU { win /ClientCanvas get setcanvas ForeGround setcolor font setfont } def
    /RSH { moveto dup stringwidth pop neg 0 rmoveto show } def
    /ERS { gsave BackGround setcolor moveto rect fill grestore } def
    /ERA { gsave BackGround setcolor clippath fill grestore } def
    /stx 0 def /sty 0 def
    /MouseEvent {
	ThisEvent /Action get /DownTransition eq {
	    {
		ovl setcanvas
		ThisEvent /YLocation get
		ThisEvent /XLocation get
		dup 2 index
		/GA_ExitTransition UpTransition def		% sgi
		getrect waitprocess aload pop exch
		/GA_ExitTransition DownTransition def		% sgi
		mouse_tag tagprint typedprint typedprint typedprint
		typedprint typedprint
	    } fork pop
	} if
    } def
    /LeftMouseButton { 0 MouseEvent } def
    /MiddleMouseButton { 1 MouseEvent } def
    /RightMouseButton { 2 MouseEvent } def
    {	win /ClientCanvas get setcanvas
	/ovl currentcanvas createoverlay def
	currentcanvas addkbdinterests pop
	/OtherInterests 10 dict def
	OtherInterests begin
		/LeftMouseButton dup def
		/MiddleMouseButton dup def
		/RightMouseButton dup def
	end
	createevent dup begin
		/Name OtherInterests def
		/Canvas currentcanvas def
		end expressinterest
	{
		clear
		/ThisEvent awaitevent def
		ThisEvent /Name get
		dup type /integertype eq
		{ char_tag tagprint typedprint }
		{ { cvx exec } stopped } ifelse
	} loop
    } fork pop

cdef ps_redraw(w,h) => redraw_tag (h, w)
cdef ps_mouse(b,x0,y0,x1,y1) => mouse_tag (x0,y0,x1,y1,b)
cdef ps_char(c) => char_tag (c)
cdef ps_eraseall()
    ERA
cdef ps_drlineno(y, i)
    i y DRL
cdef ps_StartRedraw()
    BRP
cdef ps_EndRedraw()
    ERP
cdef ps_StartUpdate()
    STU
cdef ps_rshow(x, y, string s)
    s x y RSH
cdef ps_lshow(x, y, string s)
    x y moveto s show
cdef ps_bgcolor() BackGround setcolor
cdef ps_fgcolor() ForeGround setcolor
cdef ps_errcolor() ErrText setcolor
cdef ps_bndcolor() Boundaries setcolor
cdef ps_box(x, y, w, h) x y moveto w h rect stroke
cdef ps_fillbox(x, y, w, h) x y moveto w h rect fill
cdef ps_eraserect(x, y, w, h) w h x y ERS


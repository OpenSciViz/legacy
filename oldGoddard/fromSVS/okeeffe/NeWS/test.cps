#define SET_GRAYS_TAG 1

cdef init()
	/starpath {
		matrix currentmatrix 5 1 roll
		4 2 roll translate scale
		.2 0 moveto .5 1 lineto .8 0 lineto
		0 .65 lineto 1 .65 lineto closepath
		setmatrix
	} def

	/SetStarGrays {
		SET_GRAYS_TAG tagprint typedprint typedprint
	} def

	/win framebuffer /new DefaultWindow send def
	{
		/FrameLabel (Tag Test) def
		/PaintIcon {.24 .75 FillCanvasWithStar} def
		/ClientMenu [ 
			(Black on White)	{0 1 SetStarGrays}
			(Black on Gray) 	{0 .5 SetStarGrays}
			(Gray on White) 	{.5 1 SetStarGrays}
			(Gray on Black) 	{.5 0 SetStarGrays}
			(White on Black)	{1 0 SetStarGrays}
			(White on Gray) 	{1 .5 SetStarGrays}
		] /new DefaultMenu send def
	} win send

	/reshapefromuser win send
	/map win send

cdef get_grays(float star, float fill) => SET_GRAYS_TAG(fill,star)

cdef set_grays(float star, float fill)
	win /PaintClient {star fill FillCanvasWithStar} put
	/paintclient win send


%
% This file is a product of Sun Microsystems, Inc. and is provided for
% unrestricted use provided that this legend is included on all tape
% media and as a part of the software program in whole or part.  Users
% may copy or modify this file without charge, but are not authorized to
% license or distribute it to anyone else except as part of a product
% or program developed by the user.
% 
% THIS FILE IS PROVIDED AS IS WITH NO WARRANTIES OF ANY KIND INCLUDING THE
% WARRANTIES OF DESIGN, MERCHANTIBILITY AND FITNESS FOR A PARTICULAR
% PURPOSE, OR ARISING FROM A COURSE OF DEALING, USAGE OR TRADE PRACTICE.
% 
% This file is provided with no support and without any obligation on the
% part of Sun Microsystems, Inc. to assist in its use, correction,
% modification or enhancement.
% 
% SUN MICROSYSTEMS, INC. SHALL HAVE NO LIABILITY WITH RESPECT TO THE
% INFRINGEMENT OF COPYRIGHTS, TRADE SECRETS OR ANY PATENTS BY THIS FILE
% OR ANY PART THEREOF.
% 
% In no event will Sun Microsystems, Inc. be liable for any lost revenue
% or profits or other special, indirect and consequential damages, even
% if Sun has been advised of the possibility of such damages.
% 
% Sun Microsystems, Inc.
% 2550 Garcia Avenue
% Mountain View, California  94043
 
#define PAGE_TAG 0 
#define EXIT_TAG 1
cdef ps_initialize(w, h, cx, cy)
	/FG 0 0 0 rgbcolor def
	/BG 1 1 1 rgbcolor def
	/errordict 10 dict def
	errordict /rangecheck { stop } put
	systemdict /settransfer /pop load def
	/settransfer {pop} def 
	/currenttransfer { { } } def
	/currentscreen { 0 0 { } } def
	/setscreen { pop pop pop } def
	/definefont { exch dup type /keywordtype ne { (                                    ) cvs cvn } if
			exch definefont } def
	/StandardEncoding magic:AdobeSequence def
	magic:fontdict /Encoding StandardEncoding put
    /PGC { } def
    /DoBox true def
    /privudict 200 dict def
    /@Dicts 0 array def
    /paint_pscanvas {
	PSCanvas setcanvas
	initmatrix
	clippath pathbbox
	h div exch w div min dup scale pop pop
	pathbbox 2 div exch 2 div exch translate pop pop
	cx neg cy neg translate
	FG setcolor
	DoBox { cx w 2 div sub cy h 2 div sub moveto w h rect stroke } if
    }def 
    /ps_paint {
	ClientCanvas setcanvas BG fillcanvas
	paint_pscanvas
	PSItemUp { /paint PSItem send } if
        FrameBorderColor strokecanvas 
	startmatrix concat
	PGC
    } def
    [1 0 0 1 0 0] setmatrix
cdef ps_ditroff_fix()
    /p { initmatrix xi } def
cdef ps_nobox() /DoBox false def
cdef ps_rgbcolor(r, g, b) r 256 div g 256 div b 256 div rgbcolor
cdef ps_hsbcolor(h, s, b) h 256 div s 256 div b 256 div hsbcolor
cdef ps_defFG() /FG exch def
cdef ps_defBG() /BG exch def
cdef ps_redefine_colors()
	/setgray { 
		dup 0 eq
		 { pop FG setcolor } 
		 { dup 1 eq { pop BG setcolor } { setgray } ifelse } 
		ifelse
	} def
cdef ps_setupwindow(string name, fixedsize, width, height, float scale)
    /startmatrix 6 array currentmatrix def
% 
	systemdict /Item known not { (NeWS/liteitem.ps) run } if
	/new_page { % new page number => -
		    dup /PageCount exch store
		    PAGE_TAG tagprint typedprint
		    PSItem /ItemValue PageCount put
		    /paint PSItem send
		  } def 
	/NEXT	  { PageCount 1 add dup MAX gt {pop}
		    { new_page } ifelse } def
	/PREV	   { PageCount 1 sub dup MIN lt {pop}
		    { new_page } ifelse } def
	/REDIS     { PageCount new_page } def
 	/FIRST     { MIN new_page } def
 	/LAST      { MAX new_page } def
	/EXIT      { EXIT_TAG tagprint } def
	/paintfr {
	  win /FrameLabel fr_label put
	  /paint win send
	} def  
	/fr_label {(%) [fr_data] sprintf } def 
	/fr_data name def 
%
	/PSWindow DefaultWindow      
        /PSCanvas	null def	% the 2 sub windows
        /PSItem		null def	%
	/PSItemUp	false def
  	/PageCount	1  def		% current page number, initially = 1 
	/MIN		1  def 	        % MIN always = 1, MAX sent by client
%
   dictbegin 	        
	/FrameLabel fr_label def
	/PaintClient { ps_paint } def
	/IconHeight 15 def
%
%	gsave
%	    IconFont setfont
%	    FrameLabel stringwidth pop 4 add 64 max
%	    /IconWidth exch def
%	grestore
%
	/PaintIcon { 
	    gsave IconCanvas setcanvas
	    IconFillColor fillcanvas IconBorderColor strokecanvas
	    IconTextColor setcolor IconFont setfont
	    IconWidth 2 div 3 moveto FrameLabel cshow
	    grestore
	} def 
	/ClientMenu Menu /new DefaultMenu send def   
   dictend
%  
   classbegin

        % Two class variables for defining the subwindow geometry 
        /PSItemHeight   30 def   % Height of the slider area
        /PSInset        20 def   % Inset of the PSCanvas from ClientCanvas
	% if PageCount != ItemValue, repaint SliderItem
	/PSContinue {
	    createevent dup
	    /Name /PSContinue put
	    sendevent
	} def
        /PSset { % page number => -   
	    dictbegin
	    /page exch def
	    page MAX gt page MIN lt page PageCount eq or or {pop}
	    { page new_page } ifelse
	    dictend
        } def
	% Override two methods: the Client create & shape methods
    	/CreateClientCanvas {
    	    % create the ClientCanvas:
    	    /CreateClientCanvas super send   	    
    	    % Create the slider item:
	    CreatePSItem
	    % Create the PostScript subwindow:
	    /PSCanvas ClientCanvas newcanvas store
	    PSCanvas /Mapped true put
    	} def
    	/ShapeClientCanvas {	   
    	    % [Re] Shape the ClientCanvas:
	    /ShapeClientCanvas super send
	    ClientCanvas setcanvas 	    
	    % Move the slider:
	    20 5 /move PSItem send	    
	    % [Re] Shape the PostScript subwindow:
	    gsave
	       ClientCanvas setcanvas clippath pathbbox
	       PSInset PSItemHeight translate
	       PSItemHeight sub PSInset sub exch PSInset 2 mul sub exch
	       rectpath PSCanvas reshapecanvas  
	    grestore
    	} def
    classend def    
    /MySlide { % - => -  (Interactively move window)
	    GetCanvas setcanvas
	    InteractionLock { interactivemove } monitor
	    currentcanvas ParentCanvas setcanvas getcanvaslocation
	    Iconic?
		{/IconX /IconY}
		{/FrameX /FrameY} ifelse
	    3 -1 roll def exch def
    } def
    gsave
	initmatrix
	/win framebuffer /new PSWindow send def		
	fixedsize 0 ne { 
	    10 10 width scale mul height scale mul /reshape win send
	    /MySlide load /doit win send
	} {
	    /reshapefromuser win send
	} ifelse
    grestore
    /map win send 
%
% Create handlers for each key
    /handlers 100 dict def
    handlers begin
	32 { NEXT } def	% ' '
	43 { NEXT } def	% '+'
	78 { NEXT } def	% 'N'
	110 { NEXT } def % 'n'
	8  { PREV } def	% '\b'
	45 { PREV } def	% '-'
	66 { PREV } def	% 'B'
	98 { PREV } def	% 'b'
	80 { PREV } def	% 'P'
	112 { PREV } def % 'p'
	48 { FIRST} def	% '0'
	60 { FIRST} def	% '<'
	44 { FIRST} def	% ','
	62 { LAST } def	% '>'
	46 { LAST } def	% ','
	36 { LAST } def	% '$'
	3  { EXIT } def	% ^C
	113 {EXIT } def	% 'q'
	/LeftMouseButton {NEXT} def
    end

{ 		% done as a separate process so the file reading
		% process can exit
    win /ClientCanvas get setcanvas

% Start the input handler
    systemdict /Selections known {	% The new extended input system
        currentcanvas addkbdinterests pop
    } if
    createevent dup begin
        /Name 200 dict dup begin
	    0 1 127 { dup def } for
	    /LeftMouseButton dup def
	end def
	/Action /DownTransition def
	/Canvas currentcanvas def
    end expressinterest

% Input handling loop
    {   clear
	/CurrentEventX awaitevent def
	CurrentEventX /Name get dup
	handlers exch known {
	    handlers exch get cvx exec
	} if
    } loop
} fork clear
%

cdef ps_defineslideritem()
/CreatePSItem {
    % Create the slider item:
    /Item_len 200 def 
    /PSItem (Page Number: ) [ MIN MAX PageCount ]
    /Right {ItemValue /PSset win send}
    ClientCanvas Item_len 0 /new SliderItem send
    dup /ItemFrame 1 put
    20 5 /move 3 index send store 
    % Activate the slider:
    /PSItemUp true def
    [PSItem] forkitems pop	    
} def
cdef ps_definebuttonitem()
/CreatePSItem {
    /PSItem (Next Page) {{PSContinue} win send}
    ClientCanvas 40 0 /new ButtonItem send
    dup begin
	/ItemFrame 1 store
	/ItemFillColor .7 .7 .7 rgbcolor store
	/ItemFont /Times-Roman findfont 12 scalefont def
    end
    20 0 /move 3 index send store
    [PSItem] forkitems pop	    
} def
cdef ps_defineshowpage()
/showpage {
    % Activate the button:
    /PSItemUp true def
    {ItemCanvas /Mapped true put paint} PSItem send
    2 dict begin
	/myevent createevent dup dup
	/Name /PSContinue put
	expressinterest def
	awaitevent pop
	myevent revokeinterest
    end
    ClientCanvas setcanvas BG fillcanvas
    paint_pscanvas
    /PSItemUp false def
    {ItemCanvas /Mapped false put} PSItem send
} def
cdef ps_definepagemenu()
/Menu [							   
    (Next Page)    	{ NEXT  } 
    (Previous Page)     { PREV  }
%   (Redisplay)         { REDIS }
    (First Page)	{ FIRST }
    (Last Page)  	{ LAST  }
    (Quit)         	{ EXIT  }    	     
] def
cdef ps_definequitmenu()
/Menu [
    (Quit)         	{ EXIT  }    	     
] def

cdef ps_startpage()
    /@DictHeight countdictstack def
    @Dicts length 1 sub -1 0 { @Dicts exch get begin } for
    /PGC {
	/@DictHeight countdictstack def
	@Dicts length 1 sub -1 0 { @Dicts exch get begin } for
	200 dict begin
	0 0 moveto
cdef ps_endpage()
	end
	{ countdictstack @DictHeight le { exit } if
	  end } loop
    }
    { countdictstack @DictHeight le { exit } if
      end } loop
    def
cdef ps_startprolog()
	/@DictHeight countdictstack def
	privudict begin
cdef ps_endprolog()
    [ { countdictstack @DictHeight le { exit } if
	currentdict end } loop ]
    /@Dicts exch def
cdef ps_damageall()
    createevent begin
        /Name /Damaged def
        /Canvas win /FrameCanvas get def
        currentdict
    end null exch sendevent
cdef get_exit() => EXIT_TAG
cdef get_page_selection(selection) => PAGE_TAG(selection)

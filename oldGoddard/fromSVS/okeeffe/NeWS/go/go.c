/*
 * This file is a product of Sun Microsystems, Inc. and is provided for
 * unrestricted use provided that this legend is included on all tape
 * media and as a part of the software program in whole or part.  Users
 * may copy or modify this file without charge, but are not authorized to
 * license or distribute it to anyone else except as part of a product
 * or program developed by the user.
 * 
 * THIS FILE IS PROVIDED AS IS WITH NO WARRANTIES OF ANY KIND INCLUDING THE
 * WARRANTIES OF DESIGN, MERCHANTIBILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE, OR ARISING FROM A COURSE OF DEALING, USAGE OR TRADE PRACTICE.
 * 
 * This file is provided with no support and without any obligation on the
 * part of Sun Microsystems, Inc. to assist in its use, correction,
 * modification or enhancement.
 * 
 * SUN MICROSYSTEMS, INC. SHALL HAVE NO LIABILITY WITH RESPECT TO THE
 * INFRINGEMENT OF COPYRIGHTS, TRADE SECRETS OR ANY PATENTS BY THIS FILE
 * OR ANY PART THEREOF.
 * 
 * In no event will Sun Microsystems, Inc. be liable for any lost revenue
 * or profits or other special, indirect and consequential damages, even
 * if Sun has been advised of the possibility of such damages.
 * 
 * Sun Microsystems, Inc.
 * 2550 Garcia Avenue
 * Mountain View, California  94043
 */

#ifndef lint
static	char sccsid[] = "@(#)go.c 9.4 88/01/19 Copyright 1987 Sun Micro";
#endif

/*
 * Copyright (c) 1987 by Sun Microsystems, Inc.
 */

#include <stdio.h>
#include "psio.h"
#include "go.h"

enum    stone {CROSS=0,BLACK=1,WHITE=2};
static  enum stone board[BOARD_SIZE][BOARD_SIZE];

enum stone pickastone ()
{
    extern int rand();
    int i = rand() % 7;
    return (i>2 ? CROSS : (enum stone) i);
}

main()
{    
    int x, y, id, cmd;

    if (ps_open_PostScript() == 0 ) {
	fprintf(stderr,"Cannot connect to NeWS server\n");
	exit(1);
    }

    initialize();

    execute();
    /*ps_flush_PostScript(); I don't seem to need this! */

    while (!psio_error(PostScriptInput)) {
	if (get_black(&id, &x, &y)) { 
            if (board[x][y]==BLACK) {
	        board[x][y] = CROSS;
	        cross(id,x,y);
            } else {
	        board[x][y] = BLACK;
	        black_stone(id,x,y);
            };
	} else if (get_white(&id, &x, &y)) { 
            if (board[x][y]==WHITE) {
	        board[x][y] = CROSS;
	        cross(id,x,y);
            } else {
	        board[x][y] = WHITE;
	        white_stone(id,x,y);
            };
	} else if (get_menu(&cmd)) { 
	    for (x = 0; x < BOARD_SIZE; x++)
		for (y = 0; y < BOARD_SIZE; y++)
		    board[x][y] = (cmd == FILL_CMD ? pickastone() : CROSS);
	    repaint();
	} else if (get_damage(&id)) {
	    draw_board(id);
	    for (x = 0; x < BOARD_SIZE; x++)
		for (y = 0; y < BOARD_SIZE; y++)
		    switch(board[x][y]) {
                    case BLACK:  black_stone(id,x,y);
                                 break;
                    case WHITE:  white_stone(id,x,y);
                                 break;
		    } 
	    repaired(id);
	} else if (done() || psio_eof(PostScriptInput)) 
	    break;
    } 
    ps_close_PostScript();
    exit(0);
}


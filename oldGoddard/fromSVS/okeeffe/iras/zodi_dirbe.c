/* create dirbe & dmr resolution files */

typedef struct qcell_sort { float m12;
			  float m25;
			  float m60;
			  float m100;  } QCELL_SORT;

#define IN_CELLS 1572864 /* 6*512*512 */

#include <math.h>
#include <sys/file.h>

main()
{
  short chan, inres=10, outres=9;
  int i=0, j=0, offset=0, dirbe_pix, cnt=0;
  float dirbe_avg, dmr_avg, uvec[3];
  int fd=0, nb=1, sz, fd_dirbe, fd_dmr, n_dirb, n_dmr;
  QCELL_SORT zodi[4096];
  static QCELL_SORT dirbe[393216];
  static unsigned char nval[393216];

  sz = 4096 * sizeof(QCELL_SORT);
  fd = open("/usr/people/xrhon/iras/zodi_sorted.dat",0);
  if( fd <= 0 ) printf("readzodi> unable to open zodi skymap\n");

  fd_dirbe = creat("zodi.dirbe",0644);
/*  dmr = creat("zodi.dmr",0644); */

  while( i < IN_CELLS )
  {
    nb = read(fd,zodi,sz);
    for( j=0; j < 4096; j++ )
    {
       if( zodi[j].m12 > 0.0 ) 
       {
	 cnt++;
	 upx_pixel_vector_(&i,&inres,uvec);
	 upx_pixel_number_(uvec,&outres,&dirbe_pix);
	 nval[dirbe_pix] = 1 + nval[dirbe_pix]; 
         dirbe[dirbe_pix].m12 = dirbe[dirbe_pix].m12 + zodi[j].m12;
         dirbe[dirbe_pix].m25 = dirbe[dirbe_pix].m25 + zodi[j].m25;
         dirbe[dirbe_pix].m60 = dirbe[dirbe_pix].m60 + zodi[j].m60;
         dirbe[dirbe_pix].m100 = dirbe[dirbe_pix].m100 + zodi[j].m100;
       }
       i++;
    }
  }
  close( fd );
  i = 0;
  while( i < 393216 )
  {
    if( nval[i] > 0 )
    {
       dirbe[i].m12 = dirbe[i].m12 / nval[i];
       dirbe[i].m25 = dirbe[i].m25 / nval[i];
       dirbe[i].m60 = dirbe[i].m60 / nval[i];
       dirbe[i].m100 = dirbe[i].m100 / nval[i];
    }
    if( (i != 0 ) && ((i % 4095) == 0) ) 
	write(fd_dirbe,&dirbe[i-4095],sz);
    i++;
  }

}    

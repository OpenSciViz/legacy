/* render one face as a 512x512 rgb image, assumes file
   is sorted by quadsphere cells at resoltion = 10, ie
   512x512 cells per face */
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <gl/gl.h>
#include <device.h>

#define xmax 256
#define ymax 256
#define IRAS 6*512*512
#define COLOR_MAX 1677215

main(argc,argv)
int argc; char **argv;
{
   int n, fd;
   int i, j, offset=0, qcell;
   long tmp;
   unsigned short wrred = 0xffff, wrgrn = 0xffff, wrblu = 0xffff;
   int face_nm=0, face=0;
   float  uvec[3], x, y, min, max, mean, rms;
   char *sky;
   short res=9;
   unsigned int img[256][256];
   int event=REDRAW;
   char *title="IRAS Zodi. Quadsphere Face= ";


   printf("512x512 Image of QuadSphere Face: %d\n",face_nm);
   sky = (char *) malloc(256*256);

/*
   n = readzodi_offset(atoi(argv[1]),offset,sky,&min,&max,&mean,&rms);
*/
   fd=open(argv[1],0);

   printf("format image...\n");     
   qcell = offset;
   while( qcell < (offset + 256*256) )
   {
     read(fd,&sky[qcell],1);
     upx_pixel_vector_(&qcell,&res,uvec);
     axisxy_(uvec,&face,&x,&y);
     if( face != face_nm ) printf("face != face_nm !! %d  %d\n",face,face_nm);
     i = 255 * x;
     j = 255 * y;
     img[i][j] = sky[qcell];
     qcell++;
   }

   printf("open graphics window...\n");
   strcat(title,argv[1]);
   prefsize(xmax,ymax);
   winopen(title);
   RGBmode();
   RGBwritemask(wrred,wrgrn,wrblu);
   gconfig();
   cpack(0); 
   clear();
   while( TRUE )
   {
     event = qread(&tmp);
     if( event == REDRAW )
     {
       for( i = 0; i < ymax; i++ )
         lrectwrite((Screencoord)0,(Screencoord)(ymax-i),(Screencoord)(xmax-1),
			(Screencoord)(ymax-i),img[i]);
     }
   }
}




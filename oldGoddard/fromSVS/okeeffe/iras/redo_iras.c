#include <math.h>

typedef struct zodi_hist { int quadcell; 
			  short incl;
			  short elong;
		 	  float m12;
			  float m25;
			  float m60;
			  float m100;  } ZODI_HIST;

ZODI_HIST zodi;

#define N_REC 4303190	/* my estimate of what was retrieved */
#define N_CELLS 6291460 /* 6*1024*1024 */

main()
{
  int fd, fd_1, fd_out, i, sz, nb, nb_1=1;
  char buff[28];
  static unsigned short n_vals[N_CELLS];
  static float skymap[N_CELLS];
  int tot_cnt=0;
  float mean=0, rms=0;
  
  sz = sizeof(zodi);

  fd_1 = open("iras_zodi_1.dat",0);
  fd = open("iras_zodi_hist.dat",0);
  fd_out = creat("iras_zodi.dat",0644);

  while( nb_1 > 0 )
  {
    tot_cnt++;
    nb_1 = read(fd_1,buff,sz);
    write(fd_out,buff,sz);
    nb = read(fd,&zodi,sz);
  }
  close(fd_1);
  printf("finished _1 file, tot_rec= %d\n",--tot_cnt);
/* switch inputs */
  while( nb > 0 )
  {
    tot_cnt++;
    write(fd_out,buff,sz);
    nb = read(fd,&zodi,sz);
  }
  close(fd);
  close(fd_out);
  printf("finished all, tot_rec= %d\n",tot_cnt);
}




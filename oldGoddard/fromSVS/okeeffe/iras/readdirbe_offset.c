typedef struct qcell_sort { float m12;
			  float m25;
			  float m60;
			  float m100;  } QCELL_SORT;

#define N_CELLS 6*256*256 

#include <math.h>
#include <sys/file.h>

int readdirbe_offset(chan,offset,sky,min,max,mean,rms)
short chan;
int offset;
float sky[];
float *min, *max, *mean, *rms;
{
  int fd=0, nb=1, i=0, sz, cnt=0;
  QCELL_SORT zodi;

  sz = sizeof(QCELL_SORT);
  fd = open("/usr/people/xrhon/iras/zodi.dirbe",0);
  if( fd <= 0 ) printf("readzodi> unable to open zodi skymap\n");

  lseek(fd,offset*16,L_SET);
  while( i < N_CELLS/6 )
  {
    nb = read(fd,&zodi,sz);
    switch( chan )
    {
      case 12: sky[i++] = zodi.m12;
	       break;
      case 25: sky[i++] = zodi.m25;
	       break;
      case 60: sky[i++] = zodi.m60;
	       break;
      case 100: sky[i++] = zodi.m100;
	        break;
      default: sky[i++] = zodi.m12 + zodi.m25 + zodi.m60 + zodi.m100;
		break;
    }
  }
  close( fd );

  *min = 9999999999.9;
  *max = -9999999999.9;
  *mean = 0.0;
  for( nb = 0; nb < i; nb++ )
  {
    if( sky[nb] > 0.0 )
    {
      if( sky[nb] > *max ) *max = sky[nb];
      if( sky[nb] < *min ) *min = sky[nb];
      *mean = *mean + sky[nb];
      cnt++;
    }
  }

  *mean = *mean / cnt;

  return( i );
}    

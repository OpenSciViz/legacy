/* render one face as a 256x256 rgb image, assumes file
   is sorted by quadsphere cells at resoltion = 10, ie
   256x256 cells per face */
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <gl/gl.h>
#include <device.h>

#define xmax 256
#define ymax 256
#define DIRBE 6*256*256
#define COLOR_MAX 1677215

main(argc,argv)
int argc; char **argv;
{
   int n, fd;
   int i, j, offset, qcell;
   long tmp;
   unsigned short wrred = 0xffff, wrgrn = 0xffff, wrblu = 0xffff;
   int face_nm=1, f_face;
   short c_face;
   float *sky, uvec[3], x, y, min, max, mean, rms;
   short res=9;
   unsigned int img[256][256];
   int event=REDRAW;
   char *title="IRAS Zodi. Quadsphere Face= ";

   if( argc > 1 ) 
     face_nm = atoi(argv[1]);

   offset = face_nm * 256*256;
     
   printf("256x256 Image of QuadSphere Face: %d\n",face_nm);
   if( argc > 2 )
     printf("Iras wavelength: %s\n",argv[2]);

   sky = (float *) malloc(256*256*sizeof(float));

   n = readdirbe_offset(atoi(argv[2]),offset,sky,&min,&max,&mean,&rms);

   printf("format image...\n");     
   qcell = offset;
   while( qcell < (offset + 256*256) )
   {
     upx_pixel_vector_(&qcell,&res,uvec);
     axisxy_(uvec,&f_face,&x,&y);
     if( f_face != face_nm ) printf("face != face_nm !! %d  %d\n",f_face,face_nm); 
     i = 255 * x;
     j = 255 * y;
     img[i][j] = COLOR_MAX * (sky[qcell-offset] - min) / (max-min);
     qcell++;
   }

   printf("open graphics window...\n");
   strcat(title,argv[1]);
   prefsize(xmax,ymax);
   winopen(title);
   RGBmode();
   RGBwritemask(wrred,wrgrn,wrblu);
   gconfig();
   cpack(0); 
   clear();
   while( TRUE )
   {
     event = qread(&tmp);
     if( event == REDRAW )
     {
       for( i = 0; i < ymax; i++ )
         lrectwrite((Screencoord)0,(Screencoord)(ymax-i),(Screencoord)(xmax-1),
			(Screencoord)(ymax-i),img[i]);
     }
   }
}

#include <math.h>

typedef struct zodi_hist { int quadcell; 
			  short incl;
			  unsigned char foo;
			  short elong;
		 	  float m12;
			  float m25;
			  float m60;
			  float m100;  } ZODI_HIST;

typedef struct qcell_sort { float m12;
			  float m25;
			  float m60;
			  float m100;  } QCELL_SORT;

ZODI_HIST zodi;

#define N_REC 4303190	/* my estimate of what was retrieved */
#define N_CELLS 1572864 /* 6*512*512 */

main()
{
  int fd, fd_out, i, sz, nb=1;
  char buff[28];
  static unsigned short n_vals[N_CELLS];
  static QCELL_SORT skymap[N_CELLS], zero;
  int tot_cnt=0;
  static float mean[4], rms[4];
  
/*
  zero.incl = 0; zero.elong = 0;
  zero.m12 = 0.0; zero.m25 = 0.0; zero.m60 = 0.0; zero.m100 = 0.0;
*/
  sz = sizeof(zodi);
  printf("sz= %d\n",sz);
  fd = open("zodi_most.vax",0);

  while( nb > 0 )
  {
/*
    nb = read(fd,&zodi,sz);
*/
    nb = 0;
    nb = read(fd,buff,sz);
    if( nb == sz ) 
    {
      swab(sz,buff,&zodi);
/* for now select the 25 micron map: */
      if( zodi.m12 > 0.0 && zodi.m25 > 0.0 && zodi.m60 > 0.0 && zodi.m100 > 0.0 )
      {
        ++tot_cnt;
        ++n_vals[zodi.quadcell/4];
        skymap[zodi.quadcell/4].m12 = skymap[zodi.quadcell/4].m12 + zodi.m12;
        skymap[zodi.quadcell/4].m25 = skymap[zodi.quadcell/4].m25 + zodi.m25;
        skymap[zodi.quadcell/4].m60 = skymap[zodi.quadcell/4].m60 + zodi.m60;
        skymap[zodi.quadcell/4].m100 = skymap[zodi.quadcell/4].m100 + zodi.m100;
      }
    }
  }
  close(fd);

  printf("total number of records read: %d\n",tot_cnt);
  tot_cnt = 0;
  for( i = 0; i < N_CELLS; i++ )
  {
    if( n_vals[i] > 0 )
    {
      skymap[i].m12 = skymap[i].m12 / n_vals[i];
      mean[0] = mean[0] + skymap[i].m12;
      skymap[i].m25 = skymap[i].m25 / n_vals[i];
      mean[1] = mean[1] + skymap[i].m25;
      skymap[i].m60 = skymap[i].m60 / n_vals[i];
      mean[2] = mean[2] + skymap[i].m60;
      skymap[i].m100 = skymap[i].m100 / n_vals[i];
      mean[3] = mean[3] + skymap[i].m100;
      tot_cnt++;
    }
  }
  printf("total number of unobserved pixels: %d\n",(N_CELLS - tot_cnt));

  fd_out = creat("zodi_sorted.dat",0644);
  sz = sizeof(QCELL_SORT);
  mean[0] = mean[0] / tot_cnt;
  mean[1] = mean[1] / tot_cnt;
  mean[2] = mean[2] / tot_cnt;
  mean[3] = mean[3] / tot_cnt;
  for( i = 0; i < N_CELLS; i++ )
  {
    if( n_vals[i] > 0 )
    {
      tot_cnt++; 
      rms[0] = rms[0] + (mean[0] - skymap[i].m12)*(mean[0] - skymap[i].m12);
      rms[1] = rms[1] + (mean[1] - skymap[i].m25)*(mean[1] - skymap[i].m25);
      rms[2] = rms[2] + (mean[2] - skymap[i].m60)*(mean[2] - skymap[i].m60);
      rms[3] = rms[3] + (mean[3] - skymap[i].m100)*(mean[3] - skymap[i].m100);
      write(fd_out,&skymap[i],sz);
    }
    else
      write(fd_out,&zero,sz);
  }
  rms[0] = sqrt(rms[0] / tot_cnt);
  rms[1] = sqrt(rms[1] / tot_cnt);
  rms[2] = sqrt(rms[2] / tot_cnt);
  rms[3] = sqrt(rms[3] / tot_cnt);
  printf("mean & rms of skymap: %f  %f\n",mean,rms); 
  close(fd_out);
}

int swab(n,in,out)
short n;
char in[], out[];
{
  while( n >= 6 )
  {
    n = n - 2;
    out[n] = in[n+1];
    out[n+1] = in[n];
  }

  out[3] = in[0];
  out[2] = in[1];
  out[1] = in[2];
  out[0] = in[3];

}
    

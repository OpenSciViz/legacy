/*
    Generalized routine to return the numbers of all pixels adjoining the
  input pixel at the given resolution.
  This routine will work for resolutions up to 15.

  Method:
     1.  All eight adjoining pixels are found by manipulation of the
         cartesian coordinates of the input pixel in the plane of the
         face on which it lies.  See UPX_4_NEIGHBORS opening comments 
         for a detailed description of this process.

     2.  Once all eight neighbors are found, the NEIGHBORS array is sorted
         and duplicate pixel numbers are deleted.

     3.  The number of unique neighbors and the array of pixel numbers
         are returned.
*/
int upx_8_neighbors(pixel,res,neighbors,number_of_neighbors)
int *pixel;                /* Pixel number */
short *res;                  /* Resolution */
int neighbors[8];         /* Neighboring pixel numbers */
int *number_of_neighbors;  /* Number of adjoining pixels (4 - 8)*/
{
int face;                    /* Face number of PIXEL */
int nface;                   /* Face number of neighbor */
int pixels_per_face;         /* Pixels on a face */
int rpixel;                  /* Relative pixel number on face */
int two = 2;                  
int four = 4;                 
int two14 = 16384;          /* 2**14 */
int two28 = 268435456;     /* 2**28 */
int divisor;      /* Converts resolution 15 pixel number to input resolution */
int res_diff;     /* Difference between 15 and input resolution */
int distance;     /* Distance between pixel and neighbor at resolution 15 */
int ix,iy;        /* Pixel cart. coordinates on cube face */
int jxhi,jxlo,jyhi,jylo;
int ip,id;	/* Var. for breaking down PIXEL */
int jx,jy;      /* Neighbor cart. coordinates on cube face */
static int ixtab[128] = { 0 };
static int iytab[128] = { 0 }; /* Bit tables for converting pixel number on 
      				face to cartesian coordinates */
int temp_pixel; /* Pixel number holding place */
int i,j, foo, tmp;  /* Sort loop variables */
short oof = 128;
int upx_bit_table_set(), edgchk(), power();

   if( ixtab[127] == 0 ) upx_bit_table_set(ixtab,iytab,&oof);

      pixels_per_face = power(two,2*(*res - 1));
      face = *pixel / pixels_per_face;
      rpixel = *pixel - face*pixels_per_face;
      res_diff = 15 - *res;
      divisor  = power(four,res_diff);
      distance = power(two,res_diff);

      ix = 0;
      iy = 0;
      ip = 1;
/*
  Break pixel number down into constituent x,y coordinates:
*/ 
      while( rpixel != 0)
      {
          id = rpixel % 2;
          rpixel = rpixel / 2;
          ix = id * ip + ix;

          id = rpixel % 2;
          rpixel = rpixel / 2;
          iy = id * ip + iy;

          ip = 2 * ip;

       }
/*
  Convert x,y coordinates of pixel in initial resolution to resolution
  of 15:
*/
      ix = ix * distance;
      iy = iy * distance;
/*                   
  Calculate coordinates of each neighbor, check for edges, and return pixel
  number in appropriate array element:
*/
      tmp = ix - distance;
      foo = iy - distance;
      edgchk(&face,&tmp,&foo,&two14,&nface,&jx,&jy); /* Bottom-Left */
      jxhi = jx / 128;
      jxlo = jx % 128;
      jyhi = jy / 128;
      jylo = jy % 128;
      neighbors[0] = nface * two28 + 
                   ixtab[jxlo] + iytab[jylo] +
                   two14 * (ixtab[jxhi] + iytab[jyhi]);
      neighbors[0] = neighbors[0] / divisor;

      tmp = iy - distance;
      edgchk(&face,&ix,&tmp,&two14,&nface,&jx,&jy); /* Bottom */
      jxhi = jx / 128;
      jxlo = jx % 128;
      jyhi = jy / 128;
      jylo = jy % 128;
      neighbors[1] = nface * two28 + 
                   ixtab[jxlo] + iytab[jylo] +
                   two14 * (ixtab[jxhi] + iytab[jyhi]);
      neighbors[1] = neighbors[1] / divisor;

      tmp = ix + distance;
      foo = iy - distance;
      edgchk(&face,&tmp,&foo,&two14,&nface,&jx,&jy); /* Bottom-Right */
      jxhi = jx / 128;
      jxlo = jx % 128;
      jyhi = jy / 128;
      jylo = jy % 128;
      neighbors[2] = nface * two28 + 
                   ixtab[jxlo] + iytab[jylo] +
                   two14 * (ixtab[jxhi] + iytab[jyhi]);
      neighbors[2] = neighbors[2] / divisor;

      tmp = ix + distance;
      edgchk(&face,&tmp,&iy,&two14,&nface,&jx,&jy); /* Right */
      jxhi = jx / 128;
      jxlo = jx % 128;
      jyhi = jy / 128;
      jylo = jy % 128;
      neighbors[3] = nface * two28 + 
                   ixtab[jxlo] + iytab[jylo] +
                   two14 * (ixtab[jxhi] + iytab[jyhi]);
      neighbors[3] = neighbors[3] / divisor;

      tmp = ix + distance;
      foo = iy + distance;
      edgchk(&face,&tmp,&foo,&two14,&nface,&jx,&jy);  /* Top-Right */
      jxhi = jx / 128;
      jxlo = jx % 128;
      jyhi = jy / 128;
      jylo = jy % 128;
      neighbors[4] = nface * two28 + 
                   ixtab[jxlo] + iytab[jylo] +
                   two14 * (ixtab[jxhi] + iytab[jyhi]);
      neighbors[4] = neighbors[4] / divisor;

      tmp = iy + distance;
      edgchk(&face,&ix,&tmp,&two14,&nface,&jx,&jy); /* Top */
      jxhi = jx / 128;
      jxlo = jx % 128;
      jyhi = jy / 128;
      jylo = jy % 128;
      neighbors[5] = nface * two28 + 
                   ixtab[jxlo] + iytab[jylo] +
                   two14 * (ixtab[jxhi] + iytab[jyhi]);
      neighbors[5] = neighbors[5] / divisor;

      tmp = ix - distance;
      foo = iy + distance;
      edgchk(&face,&tmp,&foo,&two14,&nface,&jx,&jy); /* Top-Left */
      jxhi = jx / 128;
      jxlo = jx % 128;
      jyhi = jy / 128;
      jylo = jy % 128;
      neighbors[6] = nface * two28 + 
                   ixtab[jxlo] + iytab[jylo] +
                   two14 * (ixtab[jxhi] + iytab[jyhi]);
      neighbors[6] = neighbors[6] / divisor;

      tmp = ix - distance;
      edgchk(&face,&tmp,&iy,&two14,&nface,&jx,&jy); /* left */
      jxhi = jx / 128;
      jxlo = jx % 128;
      jyhi = jy / 128;
      jylo = jy % 128;
      neighbors[7] = nface * two28 + 
                   ixtab[jxlo] + iytab[jylo] +
                   two14 * (ixtab[jxhi] + iytab[jyhi]);
      neighbors[7] = neighbors[7] / divisor;

/* any duplicates ? */
      *number_of_neighbors = 8;
      for( i = 0; i < 8; i++ )
      {
         temp_pixel = neighbors[i];
         for( j = i+1; j < 8; j++)
            if( temp_pixel == neighbors[j] ) (*number_of_neighbors)--;
      }

      return;
}
/* converts unit vector c into nface number (0-5) and x,y
   in range 0-1 */
int axisxy(c,nface,x,y)
float c[3], *x, *y;
short *nface;
{
   float ac3, ac2, ac1, eta, xi;
   float rabs();
   int incube();

   ac3 = rabs(c[2]);
   ac2 = rabs(c[1]);
   ac1 = rabs(c[0]);
   if(ac3 > ac2)
   {
      if(ac3 > ac1)
      {
         if(c[2] > 0.)
         { 
      	    (*nface) = 0;
	    eta =  -c[0]/c[2];
	    xi = c[1]/c[2];
	 }
         else
         {
            (*nface) = 5;
	    eta =  -c[0]/c[2];
	    xi =  -c[1]/c[2];
         }
      }
      else
      {
         if(c[0] > 0.)
         {
	    (*nface) = 1;
	    xi = c[1]/c[0];
	    eta = c[2]/c[0];
         }
         else
         {
	    (*nface) = 3;
	    eta =  -c[2]/c[0];
	    xi = c[1]/c[0];
         }
      }
   }
   else
   {
      if(ac2 > ac1)
      {
         if(c[1] > 0.)
         {
	    (*nface) = 2;
	    eta = c[2]/c[1];
	    xi =  -c[0]/c[1];
	 }
         else
         {
	    (*nface) = 4;
	    eta =  -c[2]/c[1];
	    xi =  -c[0]/c[1];
         }
      }
      else
      {
         if(c[0] > 0.)
         {
            (*nface) = 1;
            xi = c[1]/c[0];
            eta = c[2]/c[1];
  	 }
         else
         {
	    (*nface) = 3;
	    eta =  -c[2]/c[0];
	    xi = c[1]/c[0];
         }
      }
   }
   incube(&xi,&eta,x,y);
   *x  =  ((*x) + 1. ) / 2.;
   *y  =  ((*y) + 1. ) / 2.;
   return;
}

/*
Routine to set up the bit tables for use in the pixelization subroutines 
(extracted and generalized from existing routines).
*/
int upx_bit_table_set(ix,iy,length)
short *length;       /* Number of elements in ix, iy */
int ix[];	/* x bits */
int iy[];	/* y bits */
{
   int i,j,k,ip,id; /* Loop variables */
   char finished;

   for( i = 0; i < (*length); i++ )
   {
       j = i;
       k = 0;
       ip = 1;
       finished = 0;
       while( !finished )
       {
           if( j == 0 )
           {
	      ix[i] = k;
              iy[i] = 2*k;
              finished = 1;
	   }
           else
           {
              id = j % 2;
              j = j/2;
              k = ip * id + k;
              ip = ip * 4;
           }
       }
   }
   return;
}

/*
check for ix, iy being over the edge of a face
returns correct face, ix, iy in mface,jx,jy
*/
int edgchk(nface,ix,iy,max,mface,jx,jy)
int *nface, *ix, *iy, *max, *mface, *jx, *jy;
{
   if( *ix < 0 )
   {
      switch( *nface )
      {
         case 0:
	    *mface = 4;
	    *jy = *ix + *max;
	    *jx = *max - 1 - *iy;
	    return;

      	 case 1:
	    *mface = 4;
	    *jx = *max + *ix;
	    *jy = *iy;
	    return;

      	 case 2:
	    *mface = *nface - 1;
	    *jx = *max + *ix;
	    *jy = *iy;
	    return;

      	 case 3:
	    *mface = *nface - 1;
	    *jx = *max + *ix;
	    *jy = *iy;
	    return;

      	 case 4:
	    *mface = *nface - 1;
	    *jx = *max + *ix;
	    *jy = *iy;
	    return;

         case 5:
	    *mface = 4;
	    *jx = *iy;
	    *jy = -(*ix) - 1;
	    return;

         default:
      	    printf("UPX_EDGCHK: aborting due to bad face number\n"); 
      	    exit(0);
      }
   }

   if( *ix >= *max )
   {
      switch( *nface )
      {
         case 0:
	    *mface = 2;
	    *jx = *iy;
	    *jy = 2 * (*max) - 1 - (*ix);
	    return;

         case 1:
	    *mface = *nface + 1;
	    *jy = *iy;
	    *jx = *ix - *max;
	    return;

         case 2:
	    *mface = *nface + 1;
	    *jy = *iy;
	    *jx = *ix - *max;
	    return;

         case 3:
	    *mface = *nface + 1;
	    *jy = *iy;
	    *jx = *ix - *max;
	    return;

         case 4:
	    *mface = 1;
	    *jy = *iy;
	    *jx = *ix - *max;
	    return;

         case 5:
	    *mface = 2;
	    *jx = *max - 1 - (*iy);
	    *jy = *ix - (*max);
	    return;

         default:
      	    printf("UPX_EDGCHK: aborting due to bad face number\n"); 
      	    exit(0);
      }
   }

   if( *iy < 0 )
   {
      switch( *nface )
      {
         case 0:
	    *mface = 1;
	    *jy = *iy + *max;
	    *jx = *ix;
	    return;

         case 1:
	    *mface = 5;
	    *jy = *iy + *max;
	    *jx = *ix;
	    return;

         case 2:
	    *mface = 5;
	    *jx = *iy + *max;
	    *jy = *max - 1 - (*ix);
	    return;

         case 3:
	    *mface = 5;
	    *jx = *max - 1 - (*ix);
	    *jy = -(*iy) - 1;
	    return;

         case 4:
	    *mface = 5;
	    *jx = -(*iy) - 1;
	    *jy = *ix;
	    return;

         case 5:
	    *mface = 3;
	    *jx = *max - 1 - (*ix);
	    *jy = -(*iy) - 1;
	    return;

         default:
      	    printf("UPX_EDGCHK: aborting due to bad face number\n"); 
      	    exit(0);
      }
   }

   if( *iy >= *max )
   {
      switch( *nface )
      {
         case 0:
	    *mface = 3;
	    *jx = *max - 1 - (*ix);
	    *jy = 2 * (*max) - 1 - (*iy);
	    return;

         case 1:
	    *mface = 0;
	    *jy = *iy - (*max);
	    *jx = *ix;
	    return;

         case 2:
	    *mface = 0;
	    *jx = 2 * (*max) - 1 - (*iy);
	    *jy = *ix;
	    return;

         case 3:
	    *mface = 0;
	    *jx = *max - 1 - (*ix);
	    *jy = 2 * (*max) - 1 - (*iy);
	    return;

         case 4:
	    *mface = 0;
	    *jx = *iy - (*max);
	    *jy = *max - 1 - (*ix);
	    return;

         case 5:
	    *mface = 1;
	    *jx = *ix;
	    *jy = *iy - (*max);
	    return;

         default:
      	    printf("UPX_EDGCHK: aborting due to bad face number\n"); 
      	    exit(0);
      }
   }

/* last but not least, if we get here: */

   *mface = *nface;
   *jx = *ix;
   *jy = *iy;
   return;
}
/*
input: x,y in range -1 to +1 are database co-ordinates
output: xi, eta in range -1 to +1 are tangent plane co-ordinates
based on polynomial fit found using fcfit.for
*/
#define np 28
int forward_cube(x,y,xi,eta)
float *x, *y, *xi, *eta;
{
   static double p[np] = { -0.27292696, -0.07629969, -0.02819452, -0.22797056,
	    	          -0.01471565,  0.27058160,  0.54852384,  0.48051509,
	    	   	  -0.56800938, -0.60441560, -0.62930065, -1.74114454,
	    	    	   0.30803317,  1.50880086,  0.93412077,  0.25795794,
	    	    	   1.71547508,  0.98938102, -0.93678576, -1.41601920,
	    	   	  -0.63915306,  0.02584375, -0.53022337, -0.83180469,
	    	    	   0.08693841,  0.33887446,  0.52032238,  0.14381585 };
   double xx, yy;

   xx = (*x) * (*x);
   yy = (*y) * (*y);
   *xi = (*x) * (1.+(1.-xx)*(
	p[0]+xx*(p[1]+xx*(p[3]+xx*(p[6]+xx*(p[10]+xx*(p[15]+xx*p[21]))))) +
	yy*( p[2]+xx*(p[4]+xx*(p[7]+xx*(p[11]+xx*(p[16]+xx*p[22])))) +
	yy*( p[5]+xx*(p[8]+xx*(p[12]+xx*(p[17]+xx*p[23]))) +
	yy*( p[9]+xx*(p[13]+xx*(p[18]+xx*p[24])) + 
	yy*( p[14]+xx*(p[19]+xx*p[25]) +
	yy*( p[20]+xx*p[26] + yy*p[27])))) )));
   *eta = (*y) * (1.+(1.-yy)*(
	p[0]+yy*(p[1]+yy*(p[3]+yy*(p[6]+yy*(p[10]+yy*(p[15]+yy*p[21]))))) +
	xx*( p[2]+yy*(p[4]+yy*(p[7]+yy*(p[11]+yy*(p[16]+yy*p[22])))) +
	xx*( p[5]+yy*(p[8]+yy*(p[12]+yy*(p[17]+yy*p[23]))) +
	xx*( p[9]+yy*(p[13]+yy*(p[18]+yy*p[24])) + 
	xx*( p[14]+yy*(p[19]+yy*p[25]) +
	xx*( p[20]+yy*p[26] + xx*p[27])))) )));
   return;
}

/*	see csc "extended study ... note that the text has typos.  i have
	tried to copy the fortran listings
*/
int incube(alpha,beta,x,y)
float *alpha, *beta, *x, *y;
{
   static double gstar =  1.37484847732, g =  -0.13161671474,
       m =  0.004869491981, w1 =  -0.159596235474, c00 = 0.141189631152,
       c10 = 0.0809701286525, c01 =  -0.281528535557, c11 = 0.15384112876,
       c20 =  -0.178251207466, c02 = 0.106959469314, d0 = 0.0759196200467,
       d1 = -0.0217762490699, r0 = 0.577350269;
   double aa, bb, a4, b4, onmaa, onmbb;

   aa = (*alpha) * (*alpha);
   bb = (*beta) * (*beta);
   a4 = aa*aa;
   b4 = bb*bb;
   onmaa = 1. - aa;
   onmbb = 1. - bb;
   *x = (*alpha) * (gstar+aa*(1.-gstar)+onmaa*(bb*(g+(m-g)*aa
      +onmbb*(c00+c10*aa+c01*bb+c11*aa*bb+c20*a4+c02*b4))
      +aa*(w1-onmaa*(d0+d1*aa))));
   *y = (*beta) * (gstar+bb*(1.-gstar)+onmbb*(aa*(g+(m-g)*bb
      +onmaa*(c00+c10*bb+c01*aa+c11*bb*aa+c20*b4+c02*a4))
      +bb*(w1-onmbb*(d0+d1*bb))));

      return;
}

/*
  Routine to return the pixel number corresponding to the input unit
  vector in the given resolution.  Adapted from the SUPER_PIXNO routine
  by E. Wright, this routine determines the pixel number in the maximum
  resolution (15), then divides by the appropriate power of 4 to determine
  the pixel number in the desired resolution.
*/
int upx_pixel_number(vector,resolution,pixel)
float vector[3];     /* Unit vector of position of interest */
short *resolution;    /* Resolution of quad cube */
int *pixel;         /* Pixel number (0 -> 6*((2**(res-1))**2) */
{
   short face;              /* Cube face number (0-5) */
   float x,y ;              /* Coordinates on cube face */
   static int ix[128] = { 0 };
   static int iy[128] = { 0 };   /* Bit tables for calculating i,j */
   int i,j;               /* Integer pixel coordinates */
   int ip,id;       
/* i*4 '4' - reuired to avoid integer overflow for large pixel numbers: */
   static int two14 = 16384; /* 2^14 */
   static int two28 = 268435456; /* 2^28 */
   static int four = 4;
   short il,ih,jl,jh;       /* High and low 2-bytes of i & j */
   int upx_bit_table_set(), axisxy(), power();
   short length = 128;

   if( ix[127] == 0 ) upx_bit_table_set(ix,iy,&length);
   axisxy(vector,&face,&x,&y);
   i = two14 * x;
   j = two14 * y;
   if( i > (two14 - 1) ) i = two14 - 1;
   if( j > (two14 - 1) )j = two14 - 1;
   il = i % 128;
   ih = i / 128;
   jl = j % 128;
   jh = j / 128;
   *pixel = face * two28 + ix[il] + iy[jl] +
                    two14 * ( ix[ih] + iy[jh] );
/*
     'Pixel' now contains the pixel number for a resolution of 15.  To
     convert to the desired resolution, (integer) divide by 4 to the power
     of the difference between 15 and the given resolution:
*/
   *pixel = (*pixel) / power(four,(15 - (*resolution)));

   return;
}

/*
   Routine to return unit vector pointing to center of pixel given pixel
  number and resolution of the cube.  Based on exisiting _CENPIX routines
  in CSDRLIB.
*/
int upx_pixel_vector(pixel,resolution,vector)
int *pixel; short *resolution; float vector[3];
{
   short face;
   int jx,jy,ip,id;
   float x,y;
   float scale;
   int pixels_per_face;
   int fpix; /* Pixel number within the face */
   float xi,eta; /* 'Database' coordinates */
   static int one = 1;
   static int two = 2; /* Constants used to avoid integer */
   int forward_cube(), xyaxis(), power();

   jx = 0;
   jy = 0;
   ip = 1;
   scale = 1.0 * power(2,((*resolution) - 1)) / 2.0;
   pixels_per_face = power(two,two*((*resolution) - one));
   face = (*pixel)/pixels_per_face;
   fpix = (*pixel) - face*pixels_per_face;

   while( fpix != 0 )
   {
      id = fpix % 2;
      fpix = fpix / 2;
      jx = id * ip + jx;
      id = fpix % 2;
      fpix = fpix / 2;
      jy = id * ip + jy;
      ip = 2 * ip;
   }

   x = (jx - scale + 0.5) / scale;
   y = (jy - scale + 0.5) / scale;
   forward_cube(&x,&y,&xi,&eta);
   xyaxis(&face,&xi,&eta,vector);

   return;
}

/*	converts face number nface (0-5) and xi, eta (-1. - +1.)
	into a unit vector c
*/
#include <math.h>
int xyaxis(nface,xi,eta,c)
short *nface;
float c[3], *xi, *eta;
{
   float mag;
   if( *nface == 0 )
   {
      c[2] = 1.0;
      c[0] = -(*eta);	/*  transform face to sphere */
      c[1] = (*xi);
   }
   else if( *nface == 1 )
   {
      c[0] = 1.0;
      c[2] = (*eta);
      c[1] = (*xi);
   }
   else if( *nface == 2 )
   {
      c[1] = 1.0;
      c[2] = (*eta);
      c[0] = -(*xi);
   }
   else if( *nface == 3 )
   {
      c[0] = -1.0;
      c[2] = (*eta);
      c[1] = -(*xi);
   }
   else if( *nface == 4 )
   {
      c[1] = -1.0;
      c[2] = (*eta);
      c[0] = (*xi);
   }
   else if( *nface == 5 )
   {
      c[2] = -1.0;
      c[0] = (*eta);
      c[1] = (*xi);
   }

   mag = sqrt( c[0]*c[0] + c[1]*c[1] + c[2]*c[2] );
   c[0] = c[0] / mag;
   c[1] = c[1] / mag;
   c[2] = c[2] / mag;
}


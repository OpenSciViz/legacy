#include "ray.h"
TEXTURE *poly_texture(pos,poly)
float pos[3]; 
POLY *poly;
{
   TEXTURE *texture;
   int pix_per_face = 256*256, res = 5; /* dmr or firas resolution=6, dirbe=9 */
   int sky_pixel= 0;
   int upx_pixel_number_();
   float mag, vec[3], xaxis[3], yaxis[3], xmag, ymag, x, y;
   int x_index, y_index, tmp;
 
   texture = NULL;
   if( poly->id < 0 ) return( texture );

   texture = poly->texture;
   texture->index = poly->id;

   if( poly->id > 2 )
   {
      vec[0] = pos[0];
      vec[1] = pos[1];
      vec[2] = pos[2] - 0.5;
      mag = sqrt(vec[0]*vec[0] + vec[1]*vec[1] + vec[2]*vec[2]);
      vec[0] = vec[0] / mag;
      vec[1] = vec[1] / mag;
      vec[2] = vec[2] / mag;
      upx_pixel_number_(vec,&res,&sky_pixel);
      texture->red = texture_map[sky_pixel][0];
      texture->green = texture_map[sky_pixel][1];
      texture->blue = texture_map[sky_pixel][2];
/*
      switch( sky_pixel % 6 )
      {
        case 0: texture->red = 127*rand()/65535 + 128*sky_pixel / 6143;
      		texture->green = 0 + 128*sky_pixel / 6143;
      		texture->blue = 0 + 128*sky_pixel / 6143;
      		break;

        case 1: texture->red = 0 + 128*sky_pixel / 6143;
      		texture->green = 127*rand()/65535 + 128*sky_pixel / 6143;
      		texture->blue = 0 + 128*sky_pixel / 6143;
      		break;

        case 2: texture->red = 0 + 128*sky_pixel / 6143;
      		texture->green = 0 + 128*sky_pixel / 6143;
      		texture->blue = 127*rand()/65535 + 128*sky_pixel / 6143;
      		break;

        case 3: texture->red = 127*rand()/65535 + 128*sky_pixel / 6143;
      		texture->green = 0 + 128*sky_pixel / 6143;
      		texture->blue = 127*rand()/65535 + 128*sky_pixel / 6143;
      		break;

        case 4: texture->red = 127*rand()/65535 + 128*sky_pixel / 6143;
      		texture->green = 127*rand()/65535 + 128*sky_pixel / 6143;
      		texture->blue = 0 + 128*sky_pixel / 6143;
      		break;

        case 5: texture->red = 0 + 128*sky_pixel / 6143;
      		texture->green = 127*rand()/65535 + 128*sky_pixel / 6143;
      		texture->blue = 127*rand()/65535 + 128*sky_pixel / 6143;
      		break;

        defaul: break;
      }
*/
      return( texture );
   }
   
   vec[0] = pos[0] - poly->vertex->coord->x;
   vec[1] = pos[1] - poly->vertex->coord->y;
   vec[2] = pos[2] - poly->vertex->coord->z;
   xaxis[0] = poly->vertex->next->coord->x - poly->vertex->coord->x;
   xaxis[1] = poly->vertex->next->coord->y - poly->vertex->coord->y;
   xaxis[2] = poly->vertex->next->coord->z - poly->vertex->coord->z;
   xmag = sqrt( xaxis[0]*xaxis[0] + xaxis[1]*xaxis[1] + xaxis[2]*xaxis[2] );
   yaxis[0] = poly->vertex->next->next->coord->x - poly->vertex->next->coord->x;
   yaxis[1] = poly->vertex->next->next->coord->y - poly->vertex->next->coord->y;
   yaxis[2] = poly->vertex->next->next->coord->z - poly->vertex->next->coord->z;
   ymag = sqrt( yaxis[0]*yaxis[0] + yaxis[1]*yaxis[1] + yaxis[2]*yaxis[2] );

   if( poly->id == 1 )
   {
      x = 1.0 / xmag / xmag * ( vec[0]*xaxis[0] +
      				  vec[1]*xaxis[1] + vec[2]*xaxis[2] );

      x_index = 511 - 511*x;

      y = 1.0 / ymag / ymag * ( vec[0]*yaxis[0] +
      				  vec[1]*yaxis[1] + vec[2]*yaxis[2] );
      y_index = 255*y;

      y_index = 255 * (x-0.5) + 127;
      x_index = -511 * (y-0.5) + 255;

      texture->red = sinusoid_map[0][y_index][x_index];
      texture->green = sinusoid_map[1][y_index][x_index];
      texture->blue = sinusoid_map[2][y_index][x_index];
   }
   else if( poly->id == 2 )
   {
      x_index = 511 / xmag / xmag * ( vec[0]*xaxis[0] +
      				  vec[1]*xaxis[1] + vec[2]*xaxis[2] );

/*      x_index = 511 - x_index; */

      y_index = 511 / ymag / ymag * ( vec[0]*yaxis[0] +
      				  vec[1]*yaxis[1] + vec[2]*yaxis[2] );

      y_index = 511 - y_index;

      if( logo_map[y_index][x_index] != 0 )
      {
          texture->red = 128;
          texture->green = 128;
          texture->blue = 128;
      }
      else
      {
          texture->red = 0;
          texture->green = 0;
          texture->blue = 0;
      }
   }
   else if( poly->id == 0 )
   {
      x_index = 511 / xmag / xmag * ( vec[0]*xaxis[0] +
      				  vec[1]*xaxis[1] + vec[2]*xaxis[2] );

/*      x_index = 511 - x_index; */

      y_index = 511 / ymag / ymag * ( vec[0]*yaxis[0] +
      				  vec[1]*yaxis[1] + vec[2]*yaxis[2] );

      y_index = 511 - y_index;
/*
 since th above result is rotated 90 deg, swap x,y: (511,511) -> (511,0)
   (0,511) -> (511,511)  (0,0) -> (0,511) and (511,0) -> (0,0) 
*/
      tmp = -(y_index - 255) + 255;
      y_index = tmp;
      tmp = -(x_index - 255) + 255;
      x_index = tmp;

      
      if( nrao_map[y_index][x_index] == 0 )
      {
          texture->red = 0;
          texture->green = 0;
          texture->blue = 0;
      }
      else
      {
          texture->red = 128;
          texture->green = 128;
          texture->blue = 128;
      }
   }
   else if( poly->id == 3 )
   {
      y_index = 255 / xmag / xmag * ( vec[0]*xaxis[0] +
      				  vec[1]*xaxis[1] + vec[2]*xaxis[2] );

      y_index = 255 - y_index;

      x_index = 511 / ymag / ymag * ( vec[0]*yaxis[0] +
      				  vec[1]*yaxis[1] + vec[2]*yaxis[2] );

      texture->red = sinusoid_map[0][y_index][x_index];
      texture->green = sinusoid_map[1][y_index][x_index];
      texture->blue = sinusoid_map[2][y_index][x_index];
   }
   else if( poly->id == 4 )
   {
      y_index = 255 / xmag / xmag * ( vec[0]*xaxis[0] +
      				  vec[1]*xaxis[1] + vec[2]*xaxis[2] );

      y_index = 255 - y_index;

      x_index = 511 / ymag / ymag * ( vec[0]*yaxis[0] +
      				  vec[1]*yaxis[1] + vec[2]*yaxis[2] );

      texture->red = sinusoid_map[0][y_index][x_index];
      texture->green = sinusoid_map[1][y_index][x_index];
      texture->blue = sinusoid_map[2][y_index][x_index];
   }

   return( texture );
}


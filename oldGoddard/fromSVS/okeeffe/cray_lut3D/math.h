/*	MATH - RTL Math Function Declarations	 */

extern double
	acos(),
	asin(),
	atan(),
	atan2(),
	atof(),
	cabs(),
	ceil(),
	cos(),
	cosh(),
	exp(),
	fabs(),
	floor(),
	frexp(),
	hypot(),
	ldexp(),
	log(),
	log10(),
	modf(),
	pow(),
	sin(),
	sinh(),
	sqrt(),
	tan(),
	tanh();

#if CC$gfloat
# define HUGE 8.988465674311578540726371186585E+307
#else
#define HUGE 1.70141183460469229e38
#endif


#define FALSE 0
#define TRUE 1

char check_pointing(sky_pix,npix,pointing)
int sky_pix, npix, pointing[];
{
  char result;   
  int i;

  result = FALSE;
  i = 0;
  while( (!result) && i < npix )
  {
      if( pointing[i] == sky_pix ) result = TRUE;
      i++;
  }

  return( result );
}

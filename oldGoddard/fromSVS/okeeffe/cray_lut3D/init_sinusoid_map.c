int init_sinusoid_map(file,out_buff)
char *file;
unsigned char *out_buff;
{
unsigned char in_scan[512];
int i=0;
int fd, nb, sz=512;
char finished=0;

   printf("init_sinusoid_map> reading sinusoid map texture file: %s\n",file);
   fd = open(file,0);

   while( ! finished )
   {
      nb = read(fd,in_scan,sz);
      if( nb < sz || i > 255)
         finished = 1; 
      else
      { 
         bcopy(sz,in_scan,&out_buff[i*sz]);
         i++;
      }
   }
   close(fd);
   return;
}


#include "ray.h"
char hit_bound(bs,base,ray)
BOUNDING_SURF *bs;
float base[3], ray[3];
{
    char bound;
    float dist, distsqr, costh, tmp, bvec[3];
#ifdef DEBUG
printf("hit_bound> checking for hit with bounding surface\n");
#endif
    bound = FALSE;
/* find distance to ball center */
    distsqr = (bs->x - base[0]) * (bs->x - base[0]) + 
		(bs->y - base[1]) * (bs->y - base[1]) +
		(bs->z - base[2]) * (bs->z - base[2]);
    dist = sqrt( distsqr );

    if( dist > bs->r ) /* ray is outside or on ball */
    {
	bvec[0] = (bs->x - base[0]) / dist;
	bvec[1] = (bs->y - base[1]) / dist;
	bvec[2] = (bs->z - base[2]) / dist;
/* angle between ray & direction of ball center */
	costh = bvec[0]*ray[0] + bvec[1]*ray[1] + bvec[2]*ray[2];
	if( costh <= 0.0 ) 
	    return( bound );
	tmp = bs->r*bs->r - distsqr * (1-costh*costh); 
	if( tmp <= 0.0 )
	    return( bound );
/* if we get here, we have a hit */
        bound = TRUE;
#ifdef DEBUG
printf("hit_bound> hit bound\n",);
#endif
    }

   return( bound );
}


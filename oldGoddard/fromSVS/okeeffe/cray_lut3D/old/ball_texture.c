#include "ray.h"
TEXTURE *ball_texture(norm,ball)
float norm[3]; 
SPHERE *ball;
{
   TEXTURE *texture;
   float rabs();
   char check, check_pointing();
   int pix_per_face = 256*256, res = 9; /* dmr or firas resolution=6, dirbe=9 */
   int sky_pixel;
   int upx_pixel_number();
   float rot_norm[3], lat, lon;
   int lat_idx, lon_idx;
 
   texture = NULL;
   if( ball->id < 0 ) return( texture );

   texture = ball->texture;
   texture->index = ball->id;

/* test pattern
   texture->red = rabs(norm[0]) * 255.0;
   texture->green = rabs(norm[1]) * 255.0;
   texture->blue = rabs(norm[2]) * 255.0;
*/
   rot_norm[0] = norm[0];
   rot_norm[1] = norm[1];
   rot_norm[2] = norm[2];
   if( rot_ang > 0.0 )
   {
      rot_norm[0] = rotate[0][0]*norm[0] + rotate[0][1]*norm[1] + 
      			rotate[0][2]*norm[2];
      rot_norm[1] = rotate[1][0]*norm[0] + rotate[1][1]*norm[1] + 
      			rotate[1][2]*norm[2];
      rot_norm[2] = rotate[2][0]*norm[0] + rotate[2][1]*norm[1] + 
      			rotate[2][2]*norm[2];
   }
   if( ball->id == 0 ) /* inner sphere is the world-map texture pattern */
   {
      lat = acos( rot_norm[2] ); /* 0 to PI */
      if( rot_norm[0] != 0.0 )
         lon = atan2(rot_norm[0], rot_norm[1]) + 3.1415926; /* 0 to 2PI */
      else
         lon = 0.0;
/* note that lat = lon = 0 in our world map is coord [255][512] */
      lat_idx = 255 + 256*(lat - 3.1415926/2) / (3.1415926/2);
      lon_idx = 512 + 512*(lon - 3.1415926) / 3.1415926; /* this needs to be 
reversed: */
      lon_idx = 1023 - lon_idx;
      if( world_map[lat_idx][lon_idx] > 0 ) /* brown for land */
      {
        texture->red = 255;
        texture->green = 150;
        texture->blue = 25;
      }
      else	/* blue-green for water */
      {
        texture->red = 0;
        texture->green = 175;
        texture->blue = 255;
      }
/*
      texture->red = world_map[lat_idx][lon_idx];
      texture->green = world_map[lat_idx][lon_idx];
      texture->blue = world_map[lat_idx][lon_idx];
      color_ramp(world_map[lat_idx][lon_idx],texture);
*/
   }

   if( ball->id == 101 ) /* outer sphere texturemap is in skymap format */
   {
      upx_pixel_number(rot_norm,&res,&sky_pixel); /* use fortran obj module */

/* put this directly into the texture map */
      texture->index = sky_pixel;
/*
      check = check_pointing(sky_pixel,n_dirbe_pix,dirbe_pointing); 
*/
      if( iras_map[sky_pixel] == 0 )
      {
         texture->red = 0;
         texture->green = 0;
         texture->blue = 0;
      }
      else
      {
         color_ramp(iras_map[sky_pixel],texture); 
      }
   }

   return( texture );
}


#include "ray.h"
SURFACE *raystrike(base,unit_vec)
float base[3], unit_vec[3];
{ 
    SURFACE *b_hit;
    SURFACE *con_hit;
    SURFACE *cyl_hit;
    SURFACE *p_hit;  
    SURFACE *fxy_hit;
/* the scene is described by a simple list of surfaces -- z=f(xy), balls, 
   cones, polygons */
    SURFACE *hit;
    SURFACE *hit_ball(), *hit_poly(), *hit_zcone(), *hit_cylinder(),
            *hit_fxy();
    char tmp, hit_bound();
    int b_id, p_id, con_id, cyl_id, octant;
    double dist, near;

#ifdef DEBUG
printf("raystrike> casting from base: %f %f %f \n",base[0],base[1],base[2]);
printf("raystrike> casting direction: %f %f %f \n",unit_vec[0],unit_vec[1],
unit_vec[2]);
#endif
    hit = NULL;
    b_hit = NULL;
    p_hit = NULL;
    con_hit = NULL;
    cyl_hit = NULL;
    fxy_hit = NULL;
    near = VISIBILITY*VISIBILITY;

/* first find nearest intersection with bounding volume 
   for a small database, a linear list is convenient */
    for( b_id = 0; b_id < NB; b_id++)
    {
	b_hit = hit_ball(&balls[b_id],base,unit_vec);
	if( b_hit != NULL )
	{
	    dist = (b_hit->pos.x - base[0])*(b_hit->pos.x - base[0]) +
		   (b_hit->pos.y - base[1])*(b_hit->pos.y - base[1]) +
		   (b_hit->pos.z - base[2])*(b_hit->pos.z - base[2]);
   	    if( dist < near )
	    {
      	        if( hit != NULL ) free( hit );
	        hit = b_hit;
	        near = dist;
	    }
            else
      	       free( b_hit );
	}
    } 
    for( con_id = 0; con_id < NCON; con_id++)
    {
	con_hit = hit_cone(&cones[con_id],base,unit_vec);
	if( con_hit != NULL )
	{
	    dist = (con_hit->pos.x - base[0])*(con_hit->pos.x - base[0]) +
		   (con_hit->pos.y - base[1])*(con_hit->pos.y - base[1]) +
		   (con_hit->pos.z - base[2])*(con_hit->pos.z - base[2]);
   	    if( dist < near )
	    {
      	        if( hit != NULL ) free( hit );
	        hit = con_hit;
	        near = dist;
	    }
            else
      	       free( con_hit );
	}
    } 
    for( cyl_id = 0; cyl_id < NCYL; cyl_id++)
    {
	cyl_hit = hit_cylinder(&cyls[cyl_id],base,unit_vec);
	if( cyl_hit != NULL )
	{
	    dist = (cyl_hit->pos.x - base[0])*(cyl_hit->pos.x - base[0]) +
		   (cyl_hit->pos.y - base[1])*(cyl_hit->pos.y - base[1]) +
		   (cyl_hit->pos.z - base[2])*(cyl_hit->pos.z - base[2]);
   	    if( dist < near )
	    {
      	        if( hit != NULL ) free( hit );
	        hit = cyl_hit;
	        near = dist;
	    }
            else
      	       free( cyl_hit );
	}
    } 

/* later we'll need to recursively walk thru lists of nearby polygons,
   and keep track of the poly-hit found by the previous ray/pixel */
    for( p_id = 0; p_id < NP; p_id++ )
    {
/*
        if( (tmp = hit_bound(p_objs[p_id].bs,base,unit_vec)) == TRUE )
        {
	    p_hit = hit_poly(p_objs[p_id].root,base,unit_vec);
*/
	    p_hit = hit_poly(polys[p_id],base,unit_vec);
	    if( p_hit != NULL )
	    {
	        dist = (p_hit->pos.x - base[0]) * (p_hit->pos.x - base[0]) +
		   (p_hit->pos.y - base[1]) * (p_hit->pos.y - base[1]) +
		   (p_hit->pos.z - base[2]) * (p_hit->pos.z - base[2]);
   	        if( dist < near )
	        {
      	            if( hit != NULL ) free( hit );
	            hit = p_hit;
	            near = dist;
	        }
                else
      	           free( p_hit );
	    }
/*
            for( octant = 0; p_objs[p_id].n_poly > 1 && octant < 8; octant++)
            {
      		if( p_objs[p_id].root->near[octant] != NULL )
      		{
                   p_hit = hit_poly(p_objs[p_id].root->near[octant],base,unit_vec);
	           if( p_hit != NULL )
	           {
	               dist = (p_hit->pos.x - base[0]) * 
      				(p_hit->pos.x - base[0]) +
		       (p_hit->pos.y - base[1]) * (p_hit->pos.y - base[1]) +
		       (p_hit->pos.z - base[2])*(p_hit->pos.z - base[2]);
   	               if( dist < near )
	               {
	                   hit = p_hit;
	                   near = dist;
	               }
	            }
      		}
            }
*/
    }
/* return the nearest intersection */
    return( hit );
}


/* simple algorithm to find intersection with a cone that has its axis 
   aligned with the z axis. given the height and base radius h, r... */
#include "ray.h"
SURFACE *hit_zcone(con,emin,ray)
CONE *con;
float emin[3], ray[3];
{
    SURFACE *hit;
    float cc, mag, cvec[3], strike[3], normal[3];
    float sa, sb, s; /* stretch factor */
    float a, b, c, ratio;

    hit = NULL;

    ratio = con->r / con->h;
    ratio = ratio * ratio;
    a = ray[0]*ray[0] + ray[1]*ray[1] - ratio*ray[2]*ray[2];
    if( rabs(a) < 0.0000001 ) 
      	return( hit );	/* otherwise we have something un-solvable */
        
    b = 2.0 * (emin[0]*ray[0] + emin[1]*ray[1] - ratio*emin[2]*ray[2]);
    c = emin[0]*emin[0] + emin[1]*emin[1] - ratio*emin[2]*emin[2];

/* stretch factor is nearest positive soln to quadratic equation above */
    sa = -b/a/2.0 - sqrt(b*b - 4.0*a*c)/a/2.0;
    sb = -b/a/2.0 + sqrt(b*b - 4.0*a*c)/a/2.0;
    if( (sa < 0) && (sb < 0) ) return( hit );
    if( (sa > 0)  && (sb < 0) ) s = sa;
    if( (sb > 0)  && (sa < 0) ) s = sb;
    if( (sa > 0) && (sb > 0) ) 
    {
        if( sa > sb )
	    s = sb;
        else
	    s = sa;
    }
    if( s > VISIBILITY ) return( hit );
/* point of intersection */
    strike[0] = s * ray[0] + emin[0];
    strike[1] = s * ray[1] + emin[1];
    strike[2] = s * ray[2] + emin[2];
/* define u-vec on surface of cone directed towards intersection */
    cvec[0] = strike[0] - con->top->x;
    cvec[1] = strike[1] - con->top->y;
    cvec[2] = strike[2] - con->top->z;
    cc = cvec[0]*con->axis->x + cvec[1]*con->axis->y + cvec[2]*con->axis->z;
/* check bounds */
    if( cc < 0.0 ) return( hit );
    if( cc > con->h ) return ( hit );
/* if we get here we need to allocate a surface structure to return */
    hit = (SURFACE *) malloc(sizeof(SURFACE));
    hit->type =  &descr_cone[0];
    hit->id = con->id;
    hit->pos.x = strike[0];
    hit->pos.y = strike[1];
    hit->pos.z = strike[2];
/* normal on the outside (?) of the cone: */
    cc = con->h / con->r / sqrt( strike[0]*strike[0] + strike[1]*strike[1] );
    normal[0] = cc * strike[0];
    normal[1] = cc * strike[1];
    normal[2] = 1;
    if( (normal[0]*ray[0] + normal[1]*ray[1] + normal[2]*ray[2]) < 0.0 )
    {	/* surface normal & ray directions should be in opposite directions */
        hit->norm.x = normal[0];
        hit->norm.y = normal[1];
        hit->norm.z = normal[2];
    }
    else	/* normal on the inside of the cone: */
    {
        hit->norm.x = (- normal[0]);
        hit->norm.y = (- normal[1]);
        hit->norm.z = (- normal[2]);
    }
    mag = sqrt(hit->norm.x*hit->norm.x + hit->norm.y*hit->norm.y +
		hit->norm.z*hit->norm.z);
    hit->norm.x = hit->norm.x / mag;
    hit->norm.y = hit->norm.y / mag;
    hit->norm.z = hit->norm.z / mag;
    hit->refl = con->refl;
    hit->tran = con->tran;
    return( hit );
}

#include "nrao_64.bit"

int init_nrao_map(nrao)
char nrao[512][512];
{
  int i=0, j=0, k=0, bitcnt=0, finished=0;
  unsigned char bits;
  char nrao_64x64[64][64];

  while( ! finished )
  {
      bits = nrao_64_bits[i];
      bits >> 7;
      nrao_64x64[j][k++] = bits;

      bits = nrao_64_bits[i];
      bits << 1;
      bits >> 7;
      nrao_64x64[j][k++] = bits;

      bits = nrao_64_bits[i];
      bits << 2;
      bits >> 7;
      nrao_64x64[j][k++] = bits;

      bits = nrao_64_bits[i];
      bits << 3;
      bits >> 7;
      nrao_64x64[j][k++] = bits;

      bits = nrao_64_bits[i];
      bits << 4;
      bits >> 7;
      nrao_64x64[j][k++] = bits;

      bits = nrao_64_bits[i];
      bits << 5;
      bits >> 7;
      nrao_64x64[j][k++] = bits;

      bits = nrao_64_bits[i];
      bits << 6;
      bits >> 7;
      nrao_64x64[j][k++] = bits;

      bits = nrao_64_bits[i];
      bits << 7;
      bits >> 7;
      nrao_64x64[j][k++] = bits;

      if( k > 63 ) { k = 0; j++; }
      if( ++i > 511) finished = 1;
   }

/* magnify it into 512x512 standard */
  for( i = 0; i < 512; i++ )
    for( k = 0; k < 512; k++ )
      nrao[i][k] = nrao_64x64[i/8][k/8];

}

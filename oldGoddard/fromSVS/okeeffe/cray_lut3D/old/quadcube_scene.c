#include "ray.h"
int quadcube_scene()
{
/* make use of these globals:
global POLY *polys[N_POLYS];
global VERTEX_LIST *verts[n_vertS];
global REFLECT *refl[N_SURFS];
global int NB, NP;
*/
int i;

NFXY = 0;
NCYL = 0;
NCON = 0;
NB = 0;
NP = 9;

/* init the texture map */
for( i = 0; i < 6144/4; i++ )
{
  if( (i % 4) == 0 )
  {
    texture_map[i][0] = 200 + 55 * rand()/65535;
    texture_map[i][1] = 0;
    texture_map[i][2] = 0;
  }
  else if( (i % 4) == 1 )
  {
    texture_map[i][0] = 200 + 55 * rand()/65535;
    texture_map[i][1] = 200 + 55 * rand()/65535;
    texture_map[i][2] = 0;
  }
  else if( (i % 4) == 2 )
  {
    texture_map[i][0] = 0;
    texture_map[i][1] = 200 + 55 * rand()/65535;
    texture_map[i][2] = 200 + 55 * rand()/65535;
  }
  else if( (i % 4) == 3 )
  {
    texture_map[i][0] = 0;
    texture_map[i][1] = 0;
    texture_map[i][2] = 200 + 55 * rand()/65535;
  }
      
/*
  if( i < 256 )
  {
    texture_map[i][0] = 255 * rand()/65535;
    texture_map[i][1] = 0;
    texture_map[i][2] = 0;
  }
  else if( i < (256+256) )
  {
    texture_map[i][0] = 0;
    texture_map[i][1] = 255 * rand()/65535;
    texture_map[i][2] = 0;
  }
  else if( i < (256+2*256) )
  {
    texture_map[i][0] = 0;
    texture_map[i][1] = 0;
    texture_map[i][2] = 255 * rand()/65535;
  }
  else if( i < (256+3*256) )
  {
    texture_map[i][0] = 255 * rand()/65535;
    texture_map[i][1] = 255 * rand()/65535;
    texture_map[i][2] = 0;
  }
  else if( i < (256+4*256) )
  {
    texture_map[i][0] = 0;
    texture_map[i][1] = 255 * rand()/65535;
    texture_map[i][2] = 255 * rand()/65535;
  }
  else if( i < (256+5*256) )
  {
    texture_map[i][0] = 255 * rand()/65535;
    texture_map[i][1] = 0;
    texture_map[i][2] = 255 * rand()/65535;
  }
*/
}

/* init light source postion  */
lamp.x = 300.0;
lamp.y = 700.0;
lamp.z = 1000.;
/* lamp is white */
lamp.intens.red = 255;
lamp.intens.green = 255;
lamp.intens.blue = 255;
/* diffuse light is also white, but not as bright */
diffuse.red = 64;
diffuse.green = 64;
diffuse.blue = 64;

/* a gray surface with NO specular reflection */
   refl[0] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[0]->spec_pwr = -1.0;
   refl[0]->red_coeff = 1.0;
   refl[0]->green_coeff = 1.0;
   refl[0]->blue_coeff = 1.0;
/* a gray surface with specular reflection */
   refl[1] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[1]->spec_pwr = 10.0;
   refl[1]->red_coeff = 0.5;
   refl[1]->green_coeff = 0.5;
   refl[1]->blue_coeff = 0.5;
/* a yellow surface */
   refl[2] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[2]->spec_pwr = 1.0;
   refl[2]->red_coeff = 0.9;
   refl[2]->green_coeff = 0.9;
   refl[2]->blue_coeff = 0.1;
/* a magenta surface */
   refl[3] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[3]->spec_pwr = 1.0;
   refl[3]->red_coeff = 0.9;
   refl[3]->green_coeff = 0.1;
   refl[3]->blue_coeff = 0.9;
/* a cyan surface */
   refl[4] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[4]->spec_pwr = 1.0;
   refl[4]->red_coeff = 0.1;
   refl[4]->green_coeff = 0.9;
   refl[4]->blue_coeff = 0.9;

   for( i=0; i<9; i++ )
     polys[i] = (POLY *) malloc(sizeof(POLY));
   for( i=0; i<36; i++ )
     verts[i] = (VERTEX_LIST *) malloc(sizeof(VERTEX_LIST));
   for( i=0; i<20; i++ )
     coords[i] = (TRIPLET *) malloc(sizeof(TRIPLET));

/* root polygon is bottom square: */
   polys[0]->id = 0;
   polys[0]->n_vert = 4;
   polys[0]->norm.x = 0;
   polys[0]->norm.y = 0;
   polys[0]->norm.z = 1;
   polys[0]->tran = NULL;
   polys[0]->texture = NULL;
   polys[0]->refl = refl[0];  
   polys[0]->vertex = verts[0];
   verts[0]->coord = coords[0];
   coords[0]->x = -4;
   coords[0]->y = -4;
   coords[0]->z = -3.0;
   verts[0]->next = verts[1];
   verts[1]->coord = coords[1];
   coords[1]->x = 5.5;
   coords[1]->y = -4;
   coords[1]->z = -3.0;
   verts[1]->next = verts[2];
   verts[2]->coord = coords[2];
   coords[2]->x = 5.5;
   coords[2]->y = 5.5;
   coords[2]->z = -3.0;
   verts[2]->next = verts[3];
   verts[3]->coord = coords[3];
   coords[3]->x = -4;
   coords[3]->y = 5.5;
   coords[3]->z = -3.0;
   verts[3]->next = verts[0];

/* next polygon: */   
   polys[1]->id = 1;
   polys[1]->n_vert = 4;
   polys[1]->norm.x = 0;
   polys[1]->norm.y = 1;
   polys[1]->norm.z = 0;
   polys[1]->tran = NULL;
   polys[1]->texture = NULL;
   polys[1]->refl = refl[0]; 
   polys[1]->vertex = verts[4];
   verts[4]->coord = coords[4];
   coords[4]->x = -3.75;
   coords[4]->y = -4.0;
   coords[4]->z = -2.5;
   verts[4]->next = verts[5];
   verts[5]->coord = coords[5];
   coords[5]->x = -3.75;
   coords[5]->y = -4.0;
   coords[5]->z = 5;
   verts[5]->next = verts[6];
   verts[6]->coord = coords[6];
   coords[6]->x = 5.5;
   coords[6]->y = -4.0;
   coords[6]->z = 5;
   verts[6]->next = verts[7];
   verts[7]->coord = coords[7];
   coords[7]->x = 5.5;
   coords[7]->y = -4.0;
   coords[7]->z = -2.5;
   verts[7]->next = verts[4];

/* next polygon: */   
   polys[2]->id = 2;
   polys[2]->n_vert = 4;
   polys[2]->norm.x = 1;
   polys[2]->norm.y = 0;
   polys[2]->norm.z = 0;
   polys[2]->tran = NULL;
   polys[2]->texture = NULL;
   polys[2]->refl = refl[0]; 
   polys[2]->vertex = verts[8];
   verts[8]->coord = coords[8];
   coords[8]->x = -4.0;
   coords[8]->y = -3.75;
   coords[8]->z = -2.5;
   verts[8]->next = verts[9];
   verts[9]->coord = coords[9];
   coords[9]->x = -4.0;
   coords[9]->y = 5.5;
   coords[9]->z = -2.5;
   verts[9]->next = verts[10];
   verts[10]->coord = coords[10];
   coords[10]->x = -4.0;
   coords[10]->y = 5.5;
   coords[10]->z = 5;
   verts[10]->next = verts[11];
   verts[11]->coord = coords[11];
   coords[11]->x = -4.0;
   coords[11]->y = -3.75;
   coords[11]->z = 5;
   verts[11]->next = verts[8];

/* face 0 of cube==top: */   
   polys[3]->id = 3;
   polys[3]->n_vert = 4;
   polys[3]->norm.x = 0;
   polys[3]->norm.y = 0;
   polys[3]->norm.z = 1;
   polys[3]->tran = NULL;
   polys[3]->texture = (TEXTURE *) malloc(sizeof(TEXTURE));
   polys[3]->texture->red = 0;
   polys[3]->texture->green = 0;
   polys[3]->texture->blue = 0;
   polys[3]->refl = NULL; 
   polys[3]->vertex = verts[12];
   verts[12]->coord = coords[12];
   coords[12]->x = -2.0;
   coords[12]->y = -2.0;
   coords[12]->z = 2.5;
   verts[12]->next = verts[13];
   verts[13]->coord = coords[13];
   coords[13]->x = 2.0;
   coords[13]->y = -2.0;
   coords[13]->z =  2.5;
   verts[13]->next = verts[14];
   verts[14]->coord = coords[14];
   coords[14]->x =  2.0;
   coords[14]->y =  2.0;
   coords[14]->z =  2.5;
   verts[14]->next = verts[15];
   verts[15]->coord = coords[15];
   coords[15]->x = -2.0;
   coords[15]->y =  2.0;
   coords[15]->z =  2.5;
   verts[15]->next = verts[12];

/* face 5 of cube==bottom: */   
   polys[8]->id = 8;
   polys[8]->n_vert = 4;
   polys[8]->norm.x = 0;
   polys[8]->norm.y = 0;
   polys[8]->norm.z = -1;
   polys[8]->tran = NULL;
   polys[8]->texture = (TEXTURE *) malloc(sizeof(TEXTURE));
   polys[8]->texture->red = 0;
   polys[8]->texture->green = 0;
   polys[8]->texture->blue = 0;
   polys[8]->refl = NULL; 
   polys[8]->vertex = verts[16];
   verts[16]->coord = coords[16];
   coords[16]->x = -2.0;
   coords[16]->y = -2.0;
   coords[16]->z = -1.5;
   verts[16]->next = verts[17];
   verts[17]->coord = coords[17];
   coords[17]->x = -2.0;
   coords[17]->y =  2.0;
   coords[17]->z = -1.5;
   verts[17]->next = verts[18];
   verts[18]->coord = coords[18];
   coords[18]->x =  2.0;
   coords[18]->y =  2.0;
   coords[18]->z = -1.5;
   verts[18]->next = verts[19];
   verts[19]->coord = coords[19];
   coords[19]->x =  2.0;
   coords[19]->y = -2.0;
   coords[19]->z = -1.5;
   verts[19]->next = verts[16];

/* face 1 of cube==x axis: */   
   polys[4]->id = 4;
   polys[4]->n_vert = 4;
   polys[4]->norm.x = 1;
   polys[4]->norm.y = 0;
   polys[4]->norm.z = 0;
   polys[4]->tran = NULL;
   polys[4]->texture = (TEXTURE *) malloc(sizeof(TEXTURE));
   polys[4]->texture->red = 0;
   polys[4]->texture->green = 0;
   polys[4]->texture->blue = 0;
   polys[4]->refl = NULL; 
   polys[4]->vertex = verts[20];
   verts[20]->coord = coords[19];
   verts[20]->next = verts[21];
   verts[21]->coord = coords[18];
   verts[21]->next = verts[22];
   verts[22]->coord = coords[14];
   verts[22]->next = verts[23];
   verts[23]->coord = coords[13];
   verts[23]->next = verts[20];

/* face 2 of cube==y axis: */   
   polys[5]->id = 5;
   polys[5]->n_vert = 4;
   polys[5]->norm.x = 0;
   polys[5]->norm.y = 1;
   polys[5]->norm.z = 0;
   polys[5]->tran = NULL;
   polys[5]->texture = (TEXTURE *) malloc(sizeof(TEXTURE));
   polys[5]->texture->red = 0;
   polys[5]->texture->green = 0;
   polys[5]->texture->blue = 0;
   polys[5]->refl = NULL; 
   polys[5]->vertex = verts[24];
   verts[24]->coord = coords[18];
   verts[24]->next = verts[25];
   verts[25]->coord = coords[17];
   verts[25]->next = verts[26];
   verts[26]->coord = coords[15];
   verts[26]->next = verts[27];
   verts[27]->coord = coords[14];
   verts[27]->next = verts[24];

/* face 2 of cube== -x axis: */   
   polys[6]->id = 6;
   polys[6]->n_vert = 4;
   polys[6]->norm.x = -1;
   polys[6]->norm.y = 0;
   polys[6]->norm.z = 0;
   polys[6]->tran = NULL;
   polys[6]->texture = (TEXTURE *) malloc(sizeof(TEXTURE));
   polys[6]->texture->red = 0;
   polys[6]->texture->green = 0;
   polys[6]->texture->blue = 0;
   polys[6]->refl = refl[0]; 
   polys[6]->vertex = verts[28];
   verts[28]->coord = coords[17];
   verts[28]->next = verts[29];
   verts[29]->coord = coords[16];
   verts[29]->next = verts[30];
   verts[30]->coord = coords[12];
   verts[30]->next = verts[31];
   verts[31]->coord = coords[15];
   verts[31]->next = verts[28];

/* face 2 of cube== -y axis: */   
   polys[7]->id = 7;
   polys[7]->n_vert = 4;
   polys[7]->norm.x = 0;
   polys[7]->norm.y = -1;
   polys[7]->norm.z = 0;
   polys[7]->tran = NULL;
   polys[7]->texture = (TEXTURE *) malloc(sizeof(TEXTURE));
   polys[7]->texture->red = 0;
   polys[7]->texture->green = 0;
   polys[7]->texture->blue = 0;
   polys[7]->refl = NULL; 
   polys[7]->vertex = verts[32];
   verts[32]->coord = coords[12];
   verts[32]->next = verts[33];
   verts[33]->coord = coords[16];
   verts[33]->next = verts[34];
   verts[34]->coord = coords[19];
   verts[34]->next = verts[35];
   verts[35]->coord = coords[13];
   verts[35]->next = verts[32];

}

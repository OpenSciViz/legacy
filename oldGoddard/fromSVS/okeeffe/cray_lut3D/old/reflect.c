 
#include "ray.h"
/* a = 2*abs( ray dot norm ), reflected ray is defined as ray + a * norm */
reflect(ray,norm,reflct)
float ray[3], reflct[3];
TRIPLET *norm;
{
   float a, rmag;
   float rabs();
   double sqrt();

   a = ray[1]*norm->y + ray[2]*norm->z + ray[0]*norm->x;
   a = 2.0 * rabs(a);
   reflct[1] = ray[1] + a*norm->y;
   reflct[2] = ray[2] + a*norm->z;
   reflct[0] = ray[0] + a*norm->x;
   rmag = reflct[1]*reflct[1] + reflct[2]*reflct[2] + reflct[0]*reflct[0];
   rmag = (float) sqrt( rmag );
   if( rmag > 0 )
   {
	reflct[1] = reflct[1] / rmag;
	reflct[2] = reflct[2] / rmag;
	reflct[0] = reflct[0] / rmag;
   }
   else
   {
	reflct[1] = 0;
	reflct[2] = 0;
	reflct[0] = 0;
   }
}
 


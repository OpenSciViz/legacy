/* calc. emination point and direction of refracted ray, assuming
   the index of refraction 'outside' of the surface is unity. knows
   about 'planar' and 'spherical' surfaces. given surface type and
   thikness... */

#include "ray.h"

int transmit(surf,incident,transmitted,emin)
SURFACE *surf;
float incident[3], transmitted[3], emin[3];
{

   if( surf->type == descr_poly ) /* translucent polygon */
      		planar_trans(surf->tran->refract_indx,surf->thick,
      			     &(surf->pos),&(surf->norm),incident,
      			     emin,transmitted);
   else if( surf->type == descr_ball )/* translucent ball or sphere surface */
                spheric_trans(surf->tran->refract_indx,surf->thick,
      			      &(surf->pos),&(surf->norm),incident,
      			      emin,transmitted);
   else /* unknown surface type ! */
   {
      	printf("transmit> Unknown serface type! surface type, id: %s %d\n",
      			surf->type,surf->id);
        return(-1);
   }
   return(0);
}

int planar_trans(refrct_indx,t,pos,norm,inc,emin,trans)
float refrct_indx, t;
TRIPLET *pos, *norm;
float inc[3], emin[3], trans[3];
{
 float propogat[3];
 float refr_ang, inc_ang, dot;
 float cos_inc, cos_refr, cos_diff, coeff_norm, coeff_inc;

/* polygon surface is flat, so transmitted ray (once it has
exited the surface, and assuming index of refraction
outside the surface is constant) is in the same direction as
the incident ray, but has a different emination point
than the original strike point */

   trans[0] = inc[0];
   trans[1] = inc[1];
   trans[2] = inc[2];
 
   dot = inc[0]*norm->x + inc[1]*norm->y + 
      		inc[2]*norm->z;
   dot = -dot;
   inc_ang = (float) acos( dot );
   refr_ang = ( (float) sin( inc_ang ) ) / refrct_indx;
   refr_ang = (float) asin( refr_ang );

   cos_inc = cos(inc_ang);
   cos_refr = cos(refr_ang);
   cos_diff = cos(inc_ang - refr_ang);

/* the direction of propogation of the light ray through the medium is in
   the plane defined by the incident ray and the surface normal, this
   vector is simply a weighted sum of these two given unit vectors */

   coeff_inc = cos_inc*cos_refr + cos_diff;

   coeff_norm = -( cos_refr*(1 + cos_inc*cos_inc) + cos_diff );

   propogat[0] = coeff_norm * norm->x + coeff_inc * inc[0];
   propogat[1] = coeff_norm * norm->y + coeff_inc * inc[1];
   propogat[2] = coeff_norm * norm->z + coeff_inc * inc[2];

/* use 'dot' to store the magnitude of propogat, for unit direction */
   dot = sqrt( propogat[0]*propogat[0] + propogat[1]*propogat[1] +
      		propogat[2]*propogat[2] );

/* after propogating a distance defined by the thickness and the refraction
   angle, the ray exists the tranparant medium at the point emin(ation) */
   t = t / cos_refr / dot;

   emin[0] = pos->x + t * propogat[0];
   emin[1] = pos->y + t * propogat[1];
   emin[2] = pos->z + t * propogat[2];

}

int spheric_trans(refrct_indx,t,pos,norm,inc,emin,trans)
float refrct_indx, t;
TRIPLET *pos, *norm;
float inc[3], emin[3], trans[3];
{
 float propogat[3];
 float refr_ang, inc_ang, dot;
 float cos_inc, cos_refr, cos_diff, coeff_norm, coeff_inc;
 
   dot = inc[0]*norm->x + inc[1]*norm->y + 
      		inc[2]*norm->z;
   dot = -dot;
   inc_ang = (float) acos( dot );
   refr_ang = ( (float) sin( inc_ang ) ) / refrct_indx;
   refr_ang = (float) asin( refr_ang );

   cos_inc = cos(inc_ang);
   cos_refr = cos(refr_ang);
   cos_diff = cos(inc_ang - refr_ang);

/* the direction of propogation of the light ray through the medium is in
   the plane defined by the incident ray and the surface normal, this
   vector is simply a weighted sum of these two given unit vectors */

   coeff_inc = cos_inc*cos_refr + cos_diff;

   coeff_norm = -( cos_refr*(1 + cos_inc*cos_inc) + cos_diff );

   propogat[0] = coeff_norm * norm->x + coeff_inc * inc[0];
   propogat[1] = coeff_norm * norm->y + coeff_inc * inc[1];
   propogat[2] = coeff_norm * norm->z + coeff_inc * inc[2];
/* use 'dot' to store the magnitude of propogat, for unit direction */
   dot = sqrt( propogat[0]*propogat[0] + propogat[1]*propogat[1] +
      		propogat[2]*propogat[2] );

/* after propogating a distance defined by the thickness and the refraction
   angle and the curvature/radius of the spherical shell, the ray exits the 
   tranparant medium at the point emin(ation) */
/* for now, approximate for thick/radius << 1.0 */

   trans[0] = inc[0];
   trans[1] = inc[1];
   trans[2] = inc[2];
 
   t = t / cos(refr_ang) / dot;

   emin[0] = pos->x + t * propogat[0];
   emin[1] = pos->y + t * propogat[1];
   emin[2] = pos->z + t * propogat[2];

}

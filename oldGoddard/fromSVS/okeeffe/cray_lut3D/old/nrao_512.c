main()
{
  char nrao[512][512];
  char outfile[] = "clscratch:[xrhon]nrao_512";
  int i, len, flag=1, nval=128;

  len = strlen(outfile);

  init_nrao_map(nrao);

  unfor_open(&flag,&nval,outfile,&len);

  for( i = 0; i < 512; i++ )
    unfor_write(&flag,&nval,&nrao[i][0]);

}

#
set path = (. ~/bin ~/utils /usr/bsd /bin /usr/bin /usr/sbin \
/usr/local/bin /usr/demos /usr/local/etc /usr/etc /etc /usr/bin/X11 \
/usr/people/sar/idl/bin /usr/sbin/vadmin.tools)
#
set filec
set fignore = (.o .out)
set savehist = 500
set time  = 10
set notify
#set today = `date | awk '{print $2,$3}'`
set today = `date | cut -c5-10`
set julian = `date +%Y:%j`
limit coredumpsize 0
#
setenv TTY `tty | sed 's/.*\///'`
#
set hostname = `hostname`
#	prevent accidental file overwriting by I/O redirection
set noclobber
#	set up default user prompt to "HOSTNAME.USER [#]>"
set prompt=`hostname`'.'hon"[\!]> "
#set prompt="[`hostname`.hon][\@a \@h \@d \@y][\!]> "
#	set up history to remember last 100 commands
set history=100
# add DISPLAY default value for X Window System - NCCS - 5/4/90
setenv DISPLAY ':0'
# set up aliases
alias alaska 'telnet 137.229.30.142'
alias his 'history -r | more'
alias dir 'ls -Flaq'
alias dirt 'ls -alF \!* | grep "`date | cut -c5-10`"'
alias log 'logout'
if( $TERM == "vt100" ) then
  alias edit vi
else
  alias edit jot -f CrrB16
endif
# nuulua is the SGI 4D/35 with C++
alias nuulua 'telnet nuulua.gsfc.nasa.gov'
#alias howvi cat ~alan/.introvi
alias phone 'grep \!* /usr/local/data/phonebook'
alias locate 'find / -name \!* -print'
alias csdr 'telnet 128.183.10.201'
alias riemann 'telnet 192.43.240.83'
alias amarna 'telnet 128.183.112.2'
alias dirac 'telnet dirac'
#alias Xterm 'xterm -s -sb -fn courier.24 -bg darkslategray -fg white &' 
alias Xterm 'xterm -s -sb -fn 12x24 -bg black -fg white \!* &' 
#alias Xterm 'xterm -s -sb -bg black -fg white -fn -adobe-courier-medium-r-normal--18-180-75-75-m-110-iso8859-1 \!* &' 
alias Xedit 'xedit -bg darkslategray -fg white -fn 12x24 \!* &'
alias cray 'telnet 192.43.240.10'
#alias Xstart 'xinit xcalc -rpn -- Xsgi :0 -gl &'
#alias rem 'rsh \!* "setenv DISPLAY `./remotehost`:0.0; xterm -s -sb -fn courier.24 -bg darkslategray -fg white <innull >>&outnull &" &'
#alias remote 'rsh \!* "xterm -s -sb -fn courier.24 -bg darkslategray -fg white </dev/null >&/dev/null &" &'
alias sysmon 'gr_osview'
alias batch 'at now + 1 minute > batch.log >& batch.log << \!*'
alias sgirec 'if( `hostname` == cassatt ) ls \!* | ~xrhon/sgi2pana/sgi_rec'
alias nibbles 'rsh nibbles xterm -bg darkslategray -fg orange -fn courier-bold.24 -n Nibbles -d okeeffe:0.0 &'
# shell script to set up the IDL environment variables and  aliases.
#
source /usr/people/sar/idl/idl_setup


\documentstyle[11pt,aip]{article}	% Specifies the document style 
% Declare the document's title.
\title{A Computer Laboratory for Physical Oceanography}  
\author{David Hon\thanks{On-site contract at NASA Goddard Space Flight Center,
Code 936, Greenbelt MD. 20771.}
\\ ST Systems Corp.}
%\date{December 27, 1956}   % Deleting this command produces today's date.
\begin{document}	   % End of preamble and beginning of text.
\maketitle		   % Produces the title.


\begin{abstract}
	Modern computer graphics and high powered workstations make possible 
the development of a "software" laboratory for oceanographic research.
Software can evaluate models of ocean surface time evolution, and
fully explore the issues of remote sensing. 3D graphics can provide 
visualizations of ocean state dynamics, derived from 
data or models. The vexing problem of calibrating remotely sensed sea-state
parameters with "ground truth" can be explored. 
The correspondence between the time evolution of the ocean 
surface and the time evolution of the remotely sensed or imaged data (optical, 
infra-red, and potentially radar) can be studied. The development of such
software is ongoing and open-ended. Current capabilities are described, with 
examples, along with future directions.
\end{abstract}


\section{Introduction}	% Produces section heading.  Lower-level

	In his preface to the Dover edition of "Wind Waves", Blair Kinsman
wrote:\begin{quote} ...from time to time I suggest that you lay aside theory and go wave-
watching. I want to make this an urgent recommendation instead of just a
suggestion...there is more to be seen and much that is unaccounted for. There
lies the opportunity for new discovery, deeper understanding, and creative
satisfaction.\end{quote} The advent of modern computer graphics and high powered
workstations make possible a form of "observational" oceanography that
can be tightly coupled with theory, model, and measurement. With the aid
of a sophisticated software system, Kinsman's
recommendation can become an integral part of an oceanographer's 
every-day activity. The elements of such a software "workbench" would include:
1. an ability to translate a model into a surface (elevation and normals).
2. specification of the time evolution of the system. 3. a complete radiance
or backscatter calculation. and 4. any standard image processing package
such as IDL.


	A complete specification for the time evolution of an ocean surface
would require identification of all the constituent contributions: the wind
vector field in the area of interest and its coupling with the surface, the
boundary conditions and flux of waves/swell originating elsewhere, the
influence of currents, etc. Previous efforts have focussed on only one element,
either the slope spectrum of the elevation spectrum of a fully developed sea. A
more complete picture requires simultaneous specification of the elevation and
slope spectra, and should provide for their time evolution (i.e. a "developing
sea"). While still an incomplete picture, this provides considerable
flexibility. The "instantaneous" spectra will have significant power
distributed across more than 4 orders of magnitude of wave numbers, spanning
capillary and gravity waves. This generally requires some compromises when
working with a digital representation, where limited amounts of CPU time and
memory impose finite spectral windows. 

	Study of the remotely sensed surface requires a full understanding of
the optical, or infrared, or radar scattering properties of the surface. 
Remote sensing calculations should
include surface self-shadowing, diffuse and specular illumination,
(multiple) reflections, transmission, (and possibly emission), with proper 
reflectance and transmittance (and emissivity) functions.
The ray trace algorithm provides a general solution to the incoherent scattering
problem and can in principle be modified to deal with coherent scattering.
Again, while compromises must be made when implementing the technique on a
computer with finite resources, the calculations can be made sufficiently accurate.
 
	A recipe for constructing a distribution of sea surface normals from a
slope spectrum model was described by Chapman and Irani (Applied Optics '81), and
also used by Wilf and Manor (Applied Optics '84). Simple radiance calculations,
based on the distribution of normals, were explored for optical ('81) and
infrared ('82) wavelengths. A related technique was
described by Mastin, Watterberg, and Mareda (IEEE Computer Graphics and
Applications '87) for constructing surface elevations from a power spectrum.
Additionally, Mastin et. al. described some rudimentary methods for simulating
the time evolution of the ocean surface (gravity waves only). Furthermore,
Masten et. al. performed a more sophisticated radiance calculation -- a ray
trace, and constructed some fairly realistic imagery. The work described below
represents a logical evolution of these techniques. First, a brief review is
presented along with a discussion of the digital ``realization''. Next,
examples from two different models are provided. Finally, the limitations
of the method is analyzed with regard to future enhancements.


\section{The surface evolution recipe, simulation of the remote sensing via a
ray trace, and issues regarding the digital implementation}
for references: Jackson, Kinsman.

	Borrowing freely and liberally from Kinsman and Jackson, here is a brief review of
the assumptions implicit in our ``specification for a random sea''. The formulation of the
recipe starts with the hydrodynamic equations:

(K p72): 
\( \frac{1}{c^{2}}\frac{\partial^{2} z}{\partialt^{2} - \bigtriangledown^{2} z =
frac{1}{\rho g}\bigtriangledown^{2} p_{0} - frac{1}{g}\bigtriangledown \cdot ( S + B ) \)

where \( \bigtriangledown \equiv \hat{x}\frac{\partial}{\partial x} +
	 \hat{y}\frac{\partial}{\partial y} \) and
\( \bigtriangledown^{2} \equiv \frac{\partial^{2}}{\partial x^{2}} +
                                \frac{\partial^{2}}{\partial y^{2}}

The terms involving the Laplacian of the surface pressure force and the divergence
of the surface and body forces represent energy sources or sinks for waves. For
a ``random sea'' the elements of the right hand side of the equation are stochastic functions, and
the solutions can be considered as forced oscillations where the forces are random. 

In the all important \emphasize{Small Amplitude Approximation}, the pressure
differential at the air-sea interface can be directly related to the surface tension
(K p171):
\(	p'' - p' = T_{s}\frac{\partial^{2} z}{\partial x^{2}} \)
And if the only forces considered are gravity, pressure, and surface tension, the solution can
be expressed as a fourier integral:
\(	z(x,y,t) = \int{d\vec{k}A(\vec{k},t)\exp{i(\vec{k} \cdot \vec{r} - w(\vec{k})t)}

with the assumptions that we are dealing with a dispersive, non-dissipative, 
(isotropic?) and homogenous system. A dispersive medium allows us to view the wavenumber as
the independent variable and frequency as a general function of it: $w=w(\vec{k})$. The 
non-dissipative nature restricts k and $w(\vec{k})$ to real numbers. Isotropy implies 
w must be an even function of \vec{k}, $w(\vec{k}) = w(-\vec{k}) = w(k)$, and consequently 
the velocity function $c(\vec{k}) = c(k)$ depends only on the wave-number amplitude, and not 
the direction of propogation. 

	For the air-sea interface the pressure p' above the surface
is much smaller than below: p' << p'' and the square of the phase speed is:
\( c^{2} = \frac{pg/k + kT_{s})}{pcoth(kh)} \). In the deep water scenario, h >> k, the
coth(kh) term is approx. 1, and \( c^{2} = \frac{g}{k} + T_{s}\frac{k}{p} \). Kinsman
provides a useful form for the velocity function in terms of the ``minimum phase speed'':
\( c^{2} = \frac{c^{2}_{m}}{2}(\frac{k_{m}}{k} + \frac{k}{k_{m}}) \). This encompasses the
entire range of capillary and gravity waves. For the air-water interface at 20C the surface
tension is approx. 74 dynes/cm, and using p = 1 g/cm and g = 980 cm/s, Kinsman
supplies the two constants:\( k_{m} = 3.639 rad/cm \) and \(c^{m} = 23.2 cm/s\).
  
        2. Stationary statistics.

	3. Three dimensional, two dimensional, and one-dimensional spectra:
(Wave-number versus Frequency Spectrum and a realistic appraisal of what can and 
cannot be measured.)
	There is little value in a theory or model that cannot be evaluated with 
reliable measurment. While the full description of a sea state requires
the three dimensional spectrum, Kinsman recommends: ``Since the three-dimensional 
spectrum is unlikely to be measured soon, let's look at the two-dimensional 
wave-number spectrum...we shall consider only instantaneous pictures of the sea surface 
and concern ourselves with simultaneous observation of the surface displacement at 
different spatial points.''
	Assuming a homogenius sea


The recipe:
	1. the square root of the elevation spectrum, and also
the slope spectrum.

	2. the assumption of uniformly random initial phases.

	3. the constraint for "everywhere real surfaces" -- conjugate
	   symmetry of the fourier transform.

	4. the box-car window, width(s) and position.

\section {Example of a stationary wave-number spectrum (fully developed sea) --- The Modified 
Pierson-Stacy Spectrum}
	a. uniformly random initial phases
	b. weakly correlated initial phases
	c. figures:
		1. 2D spectra as a color image, and as 3D mesh, perhaps
		   the 2D image should be put in the x-y plane of the 3D mesh.
		2. 2D diagonal profile of surface patch, with normals derived
		   from slope spectrum
		3. 3D mesh (course grid) of surface, compared to raytrace
		4. series of frames (5?) showing time evolution of
		   raytraced surface and each image's spectrum
		5. ditto for weakly correlated phases, show behavior
		   as correlations grow.

\section {Example of a stationary frequency spectrum (fully developed sea) --- 
Pierson-Moskowitz Spectrum}

\section{Example of a developing spectrum --- The Tolman model}

\section{Future directions}

\end{document}             % End of document.

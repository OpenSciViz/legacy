#include <stdio.h>
#include <gl.h>
#include <device.h>
#include <math.h>
#include <malloc.h>

/* #define FALSE 0 
#define DEBUG */
#define NX 96
#define NY NX
#define NZ 112
#define Alpha_Opaq 0xff000000
#define Alpha_Min 0x01000000
#define Alpha_Mid 0x02000000
#define Alpha_Max 0x03000000
#define DIST 3.5

/* lighting defs. */
static float default_material[] = { AMBIENT, 0.0, 0.0, 0.0,
			   	    DIFFUSE, 1.0, 1.0, 1.0,
				    SPECULAR, 0.0, 0.0, 0.0,
				    SHININESS, 128,
				    EMISSION, 0.0, 0.0, 0.0, 
				    ALPHA, 1.0,
				    LMNULL };


static float default_light_src[] = { LCOLOR, 1.0, 1.0, 1.0,
/* light src at infinity: 	     POSITION, 0.0, 0.0, 1.0, 0.0,  */
/* light src at view point: */	     POSITION, 0.0, DIST/1.4, DIST/1.4, 1,  
				     LMNULL };

static float altern_light_src[] = { LCOLOR, 1.0, 1.0, 1.0,
/* light src at infinity: 	     POSITION, 0.0, 0.0, -1.0, 0.0, */ 
/* light src at -view point: */ 	     POSITION, 0.0, -DIST/1.4, -DIST/1.4, 1,  
				     LMNULL };


static float default_light_model[] = { AMBIENT, 0.0, 0.0, 0.0,
				       LOCALVIEWER, 0, /* viewer at infinity */
				       TWOSIDE, 1,
				       LMNULL };
				       

static float ident[4][4] = { {1.0, 0.0, 0.0, 0.0}, 
			     {0.0, 1.0, 0.0, 0.0}, 
			     {0.0, 0.0, 1.0, 0.0}, 
			     {0.0, 0.0, 0.0, 1.0} };
static float default_norm[3] = { 0.0, 0.0, -1.0 };

static float GLB[NX*NY][3];
static long GLB_TXT[NZ][NX*NY]; 
short wrred = 0xffff, wrgrn = 0xffff, wrblu = 0xffff;
float dist = DIST;

float *radial_norm(texture)
long texture;
{
  static float res[3], mag;
  long mask= 0x000000ff;
  float deflect;

  res[0] = 0.0;
  res[1] = -DIST/1.4;
  res[2] = -DIST/1.4; /* point normal directly towards viewpoint & light1 */
/* for values of velocity that are near zero, (red & yellow texture near
   their respective minima: ~150) deflect the normals accordingly */ 
  deflect = (255 - (mask & texture)) / 155.0; /* should be 0->1 */
  res[1] = (1.0 - 1.5*deflect) * res[1];
  res[2] = (1.0 - 1.5*deflect) * res[2];
  mag = sqrt( res[0]*res[0] + res[1]*res[1] + res[2]*res[2] );
  res[0] = res[0]/mag;
  res[1] = res[1]/mag;
  res[2] = res[2]/mag;

  return( res );
}

float *gen_norm( float *v1, float *v2, float *v3 )
{
  static float res[3], mag;

  res[0] = (v2[1]-v1[1])*(v3[2]-v1[2]) - (v2[2]-v1[2])*(v3[1]-v1[1]);
  res[1] = (v2[2]-v1[2])*(v3[0]-v1[0]) - (v2[0]-v1[0])*(v3[2]-v1[2]);
  res[2] = (v2[0]-v1[0])*(v3[1]-v1[1]) - (v2[1]-v1[1])*(v3[0]-v1[0]);

  mag = sqrt( res[0]*res[0] + res[1]*res[1] + res[2]*res[2] );
  res[0] = res[0]/mag;
  res[1] = res[1]/mag;
  res[2] = res[2]/mag;

  return( &res[0] );
}

record_it(int cnt)
{
  static char file[81], cmd[81];
  
  if( cnt < 10 )
    sprintf(file,"frame_000""%d"".rgb",cnt);
  else if( cnt < 100 )
    sprintf(file,"frame_00""%d"".rgb",cnt);
  else if( cnt < 1000 )
    sprintf(file,"frame_0""%d"".rgb",cnt);
  else if( cnt < 10000 )
    sprintf(file,"frame_""%d"".rgb",cnt);

  sprintf(cmd,"scrsave %s 8 645 8 485",file);
  system(cmd);
  cnt++;
}

init_it(buff)
unsigned char buff[NX*NY][NZ];
{
 float x, y, z;
 int i, j, k, n=0;
 long t1, t2;
 long alpha_trans = Alpha_Mid; 


   for( i = 0, y = -1.0; i < NY-1; i += 2, y += 2*2.0/NY )
     for( j = 0, x = -1.0; j < NX; j++, x += 2.0/NX  )
     {
       GLB[i*NX + j][0] = x;
       GLB[i*NX + j][1] = y;
       GLB[i*NX + j][2] = -0.25;
       
       GLB[(i+1)*NX + j][0] = x;
       GLB[(i+1)*NX + j][1] = y + 2.0/NY;
       GLB[(i+1)*NX + j][2] = -0.25;
     }

   for( i = 0; i < NY-1; i += 2 )
     for( j = 0; j < NX; j++  )
       for( k = 0; k < NZ; k++ )
       {
/*	  if( k < NZ/10 )
 	    alpha_trans = Alpha_Max;
          else if( k < 9*NZ/10 )
 	    alpha_trans = Alpha_Mid;
	  else 
 	    alpha_trans = Alpha_Min;
*/
	  if( k < 8*NZ/10 )
 	    alpha_trans = Alpha_Mid;
	  else 
 	    alpha_trans = Alpha_Min;
 
/* define texture/color: for solar convection data zero velocity is ~130 or ~150
  consider a two color channel scheme where zero velocity has alpha 0 and color 
  black (no color), negative vel. are reddish and positive vel. are yellowish
  each with increasing alpha, but alpha should never be more than some as yet
  to be determined maximum (ie never draw an opaque surface */
 
       t1 = buff[i*NX +j][k];
       t2 = buff[(i+1)*NX + j][k];

/* red: 155 -> 255, yellow: (r150,g118) -> (r255,g222) */
       if( t1 <= 150 ) /* scale red and alpha */
         GLB_TXT[k][i*NX +j] = ( (255-t1) + (((long) 32) << 8) + (((long) 64) << 16) ) |
		      		alpha_trans; /* ( (150-t1) << 24 ); */
       else  /* scale red and green (ie yellow) and alpha */
         GLB_TXT[k][i*NX +j] = ( t1 + ((t1-32) << 8) + (((long) 64) << 16) ) |
		      		alpha_trans; /* ( (t1-155) << 24 ); */
       if( t2 <= 150 ) /* scale red and alpha */
         GLB_TXT[k][(i+1)*NX +j] = ( (255-t2) + (((long) 32) << 8) + (((long) 64) << 16) ) |
		      		alpha_trans; /* ( (150-t2) << 24 ); */
       else  /* scale red and green (ie yellow) and alpha */
         GLB_TXT[k][(i+1)*NX +j] = ( t2 + ((t2-32) << 8) + (((long) 64) << 16) ) |
		      		alpha_trans; /* ( (t2-155) << 24 ); */
     }
}

draw_it()
{ 
 int i, j, k; 
 float z;
 static float box[4][3] = { -1.0, -1.0, 0.0,
			    1.0, -1.0, 0.0,
			    1.0, 1.0, 0.0,
			    -1.0, 1.0, 0.0 };
  lmcolor(LMC_DIFFUSE); 

  cpack(0x00000000);
  clear();
  zclear();
     
  cpack(0xffffffff);
  pushmatrix();
  translate(0.0, 0.0, -0.5); 
  bgnclosedline();
    v3f(box[0]); v3f(box[1]); v3f(box[2]); v3f(box[3]);
  endclosedline();
  popmatrix();

  pushmatrix();
  translate(0.0, 0.0, 0.6); 
  bgnclosedline();
    v3f(box[0]); v3f(box[1]); v3f(box[2]); v3f(box[3]);
  endclosedline();
  popmatrix();

  for( k = NZ-1; k >= 0; k-- )
  {
   z = 0.3 - k*0.6/NZ;
   pushmatrix();
   translate(0.0,0.0,z);
   for( j = 0; j < NY-1; j++ )
   {
    bgntmesh();
    for( i = 0; i < NX; i++ )
    {
      cpack( GLB_TXT[k][j*NX + i] ); 
      n3f( radial_norm( GLB_TXT[k][j*NX + i] ) );
      v3f( GLB[j*NX + i] ); 
      cpack( GLB_TXT[k][(j+1)*NX + i] );
      n3f( radial_norm( GLB_TXT[k][(j+1)*NX + i] ) );
      v3f( GLB[(j+1)*NX + i] );
    }  
    endtmesh();
   }
   popmatrix();
  }
/* and mirror it to continue up */
  for( k = 0; k < NZ; k++ )
  {
   z = 0.3 + k*0.6/NZ;
   pushmatrix();
   translate(0.0,0.0,z);
   for( j = 0; j < NY-1; j++ )
   {
    bgntmesh();
    for( i = 0; i < NX; i++ )
    {
      cpack( GLB_TXT[k][j*NX + i] ); 
      n3f( radial_norm(GLB_TXT[k][j*NX + i]) );
      v3f( GLB[j*NX + i] ); 
      cpack( GLB_TXT[k][(j+1)*NX + i] );
      n3f( radial_norm(GLB_TXT[k][(j+1)*NX + i]) );
      v3f( GLB[(j+1)*NX + i] );
    }  
    endtmesh();
   }
   popmatrix();
  }
      
}

main(int argc, char **argv, char **env)
{
 int n, cnt, fd;
 static unsigned char buff[NX*NY][NZ];
 static char file[81];

  foreground();

  cnt = atoi(argv[1]);
  
  if( cnt < 10 )
    sprintf(file,"cube000""%d",cnt);
  else if( cnt < 100 )
    sprintf(file,"cube00""%d",cnt);
  else if( cnt < 1000 )
    sprintf(file,"cube0""%d",cnt);
  else if( cnt < 10000 )
    sprintf(file,"cube""%d",cnt);

  prefposition(8,645,8,485); 
/*  prefposition(8,1271,8,1015); */
  winopen(argv[0]);
  RGBmode();
  RGBwritemask(wrred,wrgrn,wrblu);
  drawmode(NORMALDRAW);
  gconfig();

  mmode(MVIEWING);
  perspective(450,1.0,0.01,100.0); 
/*  ortho(-1.1,1.1,-1.1,1.1,0.01,dist+2); */
  loadmatrix(ident);
/*  polarview(dist,10,800,0); */
  lookat(0.0,dist/1.4,dist/1.4,0.0,0.0,0.0,1800);
  lsetdepth(0,0x7fffff);
  zbuffer(TRUE);
  backface(FALSE);
  blendfunction(BF_SA,BF_MSA); /* enable blending */

  lmdef(DEFMATERIAL,1,0,default_material); 
  lmdef(DEFLIGHT,1,0,default_light_src); 
  lmdef(DEFLIGHT,2,0,altern_light_src); 
  lmdef(DEFLMODEL,1,0,default_light_model);
  lmbind(MATERIAL,1);
  lmbind(LMODEL,1);
  lmbind(LIGHT1,1);
/*  lmbind(LIGHT2,2); */

  fd= open(file,0);
  if( fd <= 0 ){ printf("unable to open file: %s...\n",file); exit(0);}
  n = read(fd,buff,NX*NY*NZ);
  if( n < NX*NY*NZ ) fprintf(stderr,"Read fewer bytes than expected: %d\n",n);

  init_it(buff); 
  draw_it();

  record_it(cnt); 
}


  
  
  






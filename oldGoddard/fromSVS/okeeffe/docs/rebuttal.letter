Dear Dr. Mays, and Reviewer,

	I thank you for a thorough review of my submission. In the
following I attempt to address some of the questions and comments
of the review. 



1. Techinical details:
 
We are dealing with a unit sphere centered at the origiin, accordingly there 
are only two independent coordinates. Regardless, all details of the Quadsphere
transformation have been removed, relegated to the reference.

============ 

The method/mapping does not intrinsically discard any input. It merely assumes the
the data has been pre-processed (binned and sorted) into a suitable quadsphere 
organization. Clearly the software should be designed to provide the most faithful
representation of the data. "Anti-aliasing" techniqiues, whether in
in a ray trace or other algorithm, essentially perform weighted
averaging in order to produce image pixels that contain as much
of the scene information as possible. In effect, whether the original data
is continuous or descrete, the image will either be a "many to one" or
a "one to many" descretization. To avoid any suprises, it is encumbant
upon the user/scientist to prepare the data first, i.e. not just
assigning quad-cell numbers and sorting, but also to create files (database)
that have been pre-processed to include or exclude the desired features.

One must keep in mind the eventual output could be a 512x512 or 640x480
or 1024x1024 pixel image, or whatever. The output resolution dictates the
selection of Quadsphere resolution. The original data must then be
messaged in a very specific fashion, unique to the dataset and to the scientist's
goals. A high resolution map of the entire sphere, for example the IRAS
Zodiacal History dataset that has a resolution of 2 arc minutes by 30
arc minutes, could never be fully and uniquely sampled by a 512x512 image
(never mind what projection scheme is used). The software cannot and should
not decide for the scientist how to average/squeeze the data into the image.
One must also keep in mind that the application described is meant to
be "interactive". Accordingly every effort should be made to keep the
amount of "on-the-fly" calculations to a minimum. Any aliasing and other
sampling problems should be dealt with in the pre-processing stage.

The anti aliasing technique described by the reviewer would be a good 
starting point for the database pre-processor, which should also include
the instrument beam profile pattern, and calibration history. Although
the pre-processor does not have to be table driven, the reviewer has
suggested a viable scheme. The first table in the two table scheme
described by the reviewer, however, could grow prohibitively large 
if the input data is a very high resolution skymap with multiple observations
in each direction.

============= 
 
The method assumes a fixed viewpoint, and finite orientations of the sphere. The 
use of the term "arbitrary" may be somewhat lax, but is meant to convey the idea
that rotations can be performed around many different axes.


2. Implementation Details:

	Most of the implementation details have been 
removed, including paragraphs describing the suggested XWindows implimentation,
which were meant to address the issue of portablity. I believe the XWindow
designers have hit upon a very useful scheme for portability that many visualization 
applications could exploit. But perhaps there are better mediums than this article 
for a discussion of this topic. 

The reviewer seems to take issue with my description of an XWindow bitmap file as
an ASCII file, I feel compelled to provide an example.
The C code that accesses the bitmap array can actually inititalize
the array at compile time with a statement like:
   
#include "test16.bit"

The bitmap is contained in the ASCII file "test16.bit", which 
contains a simple C declaration that initializes a 16x16 bitmap. The
content of the bitmap file is shown below:

/* begin bitmap file */
#define test16_width 16
#define test16_height 16
static char test16_bits[] = {
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xc0, 0x03, 0xc0, 0x03, 0xc0, 0x03,
   0xc0, 0x03, 0xc0, 0x03, 0xc0, 0x03, 0xc0, 0x03, 0xc0, 0x03, 0xc0, 0x03,
   0xc0, 0x03, 0xc0, 0x03, 0xc0, 0x03, 0xc0, 0x03};
/* end bitmap file */

If you have access to an XWindow system that has the public domain
BITMAP program, run it with this file as argument. 


From settle@nemo  Tue Dec 18 15:49:32 1990
Received: from nemo.gsfc.nasa.gov by okeeffe.gsfc.nasa.gov (5.52/890607.SGI)
	(for xrhon) id AA25950; Tue, 18 Dec 90 15:49:32 EST
Received: Tue, 18 Dec 90 16:44:22 EST by nemo.gsfc.nasa.gov (4.0/1.5)
Date: Tue, 18 Dec 90 16:44:22 EST
From: karen settle - x4759 <settle@nemo>
Message-Id: <9012182144.AA04586@nemo.gsfc.nasa.gov>
To: xrhon@[128.183.112.68]
Subject: 1/8 Deg Mean Sea Surface Data



David,

mss8-gem10b_s901119_rect.dat

    LATITUDE    LONGITUDE   MIN   MAX  LNS  PTS  LATGR LONGR   SCREENS UNITS

   70.0 -72.0   0.0 360.0 -7664  8665 1137 2880  0.12  0.12    32767  (cm)

   1/8 degree GEOS/SEASAT MEAN SEA SURFACE (CM) - GEM10B(12,12) RECTANGULAR 12/14/90


 mss8-gem10b_s901119_rect.map

   70.0 -72.0   0.0 360.0     5   255 1137 2880  0.12  0.12     1     (cm)
                          -1524  1088
  1/8 degree GEOS/SEASAT MEAN SEA SURFACE (CM) - GEM10B(12,12) RECTANGULAR 12/14/90


Both the 8-bit and 16-bit formats cover the area from 0,360 longitude
and 70 to -72 latitude.  Both files contain 1137 lines and 2880 data points
rastered to the nearest 512 bytes (i.e. dead space at the end of each line).
The 8-bit file contains data for the first 2880 bytes for each 3072 byte
line.  The 16-bit file contains data for the first (2880*2) bytes for each
6144 byte line. Both files also have label records you will have to skip.

                  #byte in label     #bytes/line     #lines     total bytes

8-bit format        ( 1024 ) +       ( 3072     *       1137)  = 3,493,890

16-bit format       (  512 ) +       ( 6144     *       1137)  = 6,986,240 


The 8-bit format is a scaled version of the 16-bit data.  It is scaled between
[5,255] which corresponds to + and - .1% of the histogram of the
16-bit data or data values [-1524,1088] CM.

Please let me know if you have any problems reading this data.  We do have
IDL routines to read and write our format, the user being allowed to set
the "buffer size" to allow for virtual memory constraints.  I don't know
if you want to open this can of worms or not.  

Karen Settle, X4759

p.s.  We can also subset any portion of the data, write it without
      label records or wrap it with respect to longitude if this that
      would be easier for your software to deal with.


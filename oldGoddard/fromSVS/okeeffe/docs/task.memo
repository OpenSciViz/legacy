What is the SVS charter? What is it's mission and what are
its goals and responsibilities? How might these be achieved
by its mixture of civil servant and contractor personnel?
Below are my views (as one of the on site contractors) on these 
issues.

The SVS mission is to provide the Goddard scientific community
(and perhaps to a lesser extent, its engineering community)
with a state of the art environment for data/science visualization,
and related activities pertinent to NASA space and earth science
research.
This requires state of the art hardware, software, and a highly
trained (and dedicated) TEAM (and I wish to emphasize this choice 
of word) to provide the expertise required to accomplish the 
mission. 

To date the SVS TEAM has been comprised of a roughly equal number
of on-site contractors and civil servants. For management and
administrative purposes, it is of interest to attempt to identify
and quantify the specific duties and responsibilities of each 
member of the TEAM and the relationships between them.

A recent discussion/argument between myself and Jim Strong (one of 
the civil servants) arose from what was essentially a difference of
opinion on some of the "artistic" or "conceptual"  elements of the 
Crustal Dynamics movie. A number of interesting viewpoints were 
revealed by this interaction. I felt I had been assigned, by Horace,
the responsibility to work WITH (again I emphasize this diction) 
Jim Strong on this project. Jim made it clear that
he felt I was to work FOR him. When our difference of opinion
on some of the details of the work arose, I suggested to
Jim that he get more directly involved with the details
of creating the movie. For example, in my opinion, it is
better to display the globe with some level of translucence
to better portray the 3D "feel". He preferred an opaque sphere.
I explained that the software I had written could be simply
toggled to perform either rendition and I would be happy to
show him how to run it to produce his preference. He refused,
claiming in his words: "that is not my job, it is your job, David. 
It is what you were hired to do." He expressed the opinion, 
essentially, that as a contractor my principle role was do what he
tells me to do and to do it precisely how he wants it done; and 
his role, as a civil servant, was to direct me.

While there were other elements to the argument/discussion between
Jim Strong and myself, the above quote of Jim Strong raises the 
most serious issue. Is the relationship between civil servant and
contractor (regardless of the details of the project) "Master --
Slave"? Should the contractor staff of the SVS have a fairly loose
contract task statement like "Do whatever the civil servant tells
your to do" or the other extreme: "do whatever you think is best, whether
or not it is congruent with civil servant's opinion" or something in 
between?  Should the task be placed on a more formal footing with a
contractual milestone chart detailing precise goals, detailed assignments,
deadlines, and no flexibility on either assignments or methods used to 
achieve them (i.e. the civil servants cannot change their minds about
what is to be done or how, and the contractors merely perform each task 
in an inflexible way, allowing for little innovation or creativity)?

I personally believe that a "TEAM" relationship among the staff of the
SVS is far more desirable and will be far more successful than a
"Master -- Slave". The team members should be highly motivated self
starters, capable of taking the initiative in performing the "bread and 
butter" responsibilities like making movies and insuring all the hardware 
and software is well integrated and maintained. In addition, some 
fraction of a team member's time should be reserved for "visualization
research" into new, innovative, and hopefully more productive ways to 
achieve our mission. A "state of the art" facility can never stand still. 
Time must be devoted to keeping abreast of the state of the art, which is
best done by not just reading the appropriate journals, but also by 
actively participating in (appropriate) research and developing
custom software. 

While this TEAM will in all likelyhood be a civil servant -- contractor
mix for the foreseeable future, the task definition need not be an extremely
formal contractual one. That is, some amount of ambiguity is desirable.
While too much ambiguity will clearly lead to further friction between
the staff, too strict a task definition will be oppressive. In order
to devise this optimal task description we need to itemize all those
immediate and long term goals we are aware of.

1. Consolidation of the Convex - SGI network into a coherent interactive
   visualization environment by:
	a. installation and enhancement of "off the shelf software"
	b. development of custom software.

2. Consolidation of the Movie making and hardcopy equipment into a well
   integrated facility with:
	a. a mechanism allowing visiting scientists to get what they
           want easily, with minimal effort.
	b. a production pipeline with staff support for more complex
	   requirements (like the Crustal Dynamics Movie).
	c. integration of audio capability (and better annotation in 
	   in general).
	d. collaboration with Goddard TV.

3. Direct involvement of the SVS staff with selected projects, on a short
   term or indefinite term basis, with an emphasis on visualization,
   providing more direct feedback of user needs and a framework for 
   defining enhancements to the facility (my work with Search & Rescue,
   for example).

4. Keeping up with the state of the art:
	a. Collaborations with other visualization groups; active pursuit 
	   of publications; participation in conferences (the annual IEEE 
	   Visualization and ACM SIGGRAPH)
	b. Active review of the latest advances in digital video and 
	   audio technology and computer graphics hardware -- either
	   by regular meetings with industry representatives, or by
	   equipment "loaners".



\documentstyle[11pt,aip]{article}	% Specifies the document style 
% Declare the document's title.
\title{A New Method for Visualizing Data on a Sphere}  
\author{David Hon\thanks{On-site contract at NASA Goddard Space Flight Center,
Code 936, Greenbelt MD. 20771.}
\\ ST Systems Corp.}
%\date{December 27, 1956}   % Deleting this command produces today's date.
\begin{document}	   % End of preamble and beginning of text.
\maketitle		   % Produces the title.

\begin{abstract}
A new method for visualizing data on a globe or unit sphere is described.
Information that is distributed over a sphere -- global oceanographic or 
geographic measurements, all-sky astronomy observations, or any quantities 
that are best represented in spherical coordinates -- can benefit from this
technique.
Retaining a better sense of the geometry and information content of the data, 
3D graphics can provide an unobstructed view of the entire sphere, without 
undo deformation of its surface area. A ``parameterized ray trace''  
produces lookup tables (LUTs) that can be used for all visualizations. The
ray trace result shows one or more spheres with the data as a texture-map 
and three reflecting rectangles that ``mirror'' the far sides of the sphere(s) 
into view. The LUTs need only be created once, and a general purpose computer 
will do. No special purpose hardware is required beyond a PC or workstation 
that supports color.
Examples from astronomical and geophysical datasets, which are commonly
displayed with an area deforming (2D) projection, are presented.
\end{abstract}


\section{Introduction}	% Produces section heading.  Lower-level
				    % sections are begun with similar 
				    % \subsection and \subsubsection commands.

      The techniques of computer generated 3D graphics have advanced 
considerably in the past decade and are now becoming standard tools in
many areas of scientific and engineering research. Researchers are
turning to these techniques to aid in the visualization and analysis of
multi-channel data, and also to aid in the modeling of complex physical
systems. These methods are of particular interest when very large quantities
of data must be reduced and examined. NASA's space science projects of
the 90's, for example, present serious demands on data processing and
visualization abilities.

      Spacecraft borne projects in astrophysics, such as the Cosmic 
Background Explorer (COBE), the Roentgen Satellite (ROSAT), the Advanced
X-Ray Astrophysical Facility (AXAF) and others, will produce high-resolution
multi-channel sky surveys. The Upper Atmospheric Remote Sensing
Satellite (UARS), the Earth Observing System (EOS) and others referred
to as ``Mission Earth'', will produce tremendous quantities of multi-channel 
Earth surveys. To cope with such large volumes of data,
researchers will rely heavily on advanced graphical methods.

      Most graphics techniques, however, require specialized hardware 
(designed for solid modeling and ``fast'' generation of shaded images) to 
render in 3D. While visualizing scientific data from measurements or models can 
make use of these established methods, general methods can be employed that 
minimize the need for specialized hardware. The method described here can 
be used on any general purpose computer that has a color or grayscale terminal. 

      The goal is to provide a look-up table (LUT) with which spherical data
can be ``merged'' to produce a raster image that displays the data in 3D.
The LUT can be constructed on any general purpose computer and
the resultant raster can be displayed on any monitor -- an XWindows terminal
would be ideal, but not mandatory. The image will display a sphere on 
which the data is a texture-map. Regions of the sphere that are not
directly visible will be reflected into view by appropriately placed mirrors.
The result is an image that displays the entire surface of the sphere in 
perspective, with minimal distortion of its surface area. This kind of scene 
can be readily created by solving the geometric optics with a standard 3D
graphics technique known as ``Ray Tracing'' (Whitted$^{1}$, Roth$^{2}$). 
With some simple modifications, the ray trace algorithm can be used to generate
LUTs that can efficiently provide a fixed viewpoint rendition of the sphere.
The ray trace technique has the ability to render one or more translucent sphere(s) 
enveloping an opaque sphere, to provide additional flexibility in displaying multi-channel
data. A brief description of the parameterization scheme for the LUT is provided. Some 
``interactive''applications that make use of the LUT are also described.



\section{``2D vs. 3D'' and the Modified Ray Trace}

      Astrophysical and geophysical data have one thing in common:
each represents information spatially distributed on a sphere. Geophysical data
is located by ``longitude and latitude''. Astrophysical data is located by
``right ascension and declination'' (when using equatorial coordinates, one of 
many astronomical coordinate systems). Properties associated with
a measurement in a given direction, i.e. a point on the globe or the celestial
sphere, can be associated with a color. Traditional methods for display of
geophysical or astrophysical data  (Maling$^{3}$, Garver$^{4}$) rely on 2D
maps such as the Hammer-Aitoff map of the world in figure 1.

     The formula for the Hammer-Aitoff projection could be used directly to 
render any data on a sphere.$^{5}$
The more efficient approach, however, is to construct a LUT. Preparation and
utilization of a Hammer-Aitoff (2D) LUT is a useful example, and presents a 
good introduction to the 3D method. First decide on the 
desired resolution of the image, say 511x255. Allocate an array of 511x255
tuples, each tuple representing a latitude and longitude pair: Tuple[i][j][0]
and Tuple[i][j][1]. Invert the formula to give latitude and longitude
as a function of x and y (image coordinates).$^{6}$
Associate the center of the ellipse with the central element of the
tuple element -- Tuple[255][127] -- and each cell in the array with an
(x,y) coordinate. Populate the cells in the tuple whose associated coordinates
lie outside the ellipse with some constant that can be recognized later as 
an invalid latitude and longitude. Populate all other cells in the 
tuple array with values from the inverted Hammer-Aitoff formula and store the 
array in a file. 

      The Hammer-Aitoff LUT can later be read from the file, along with 
data from another file, to create an image. The technique is most efficient 
when the data is sorted by latitude and longitude and can be held in memory 
for direct access. The LUT array is scanned as a raster image. If an entry
is an invalid tuple, the corresponding image pixel is set to any desired
constant (background) color. If an entry is a valid tuple, convert the
latitude and longitude values to indices into the sorted data array, then
use the selected data value to scale the corresponding image pixel. 
The 3D LUT described below is used in a fashion analogous to the Hammer-Aitoff.
The solution to the sphere and mirror optics, however, cannot be reduced to a 
simple formula. Additionally, the LUT is devised as a two dimensional array of 
integers associated with equal area cells on the sphere (figure 2).

	Generally, a ray trace indiscriminately solves the
inverse scattering problem, collecting all contributions from reflections
and refractions, to faithfully evaluate the optical solution. Such a solution
to our ``sphere and mirrors'' scene will have many undesirable components.
Secondary reflections of one mirror by another will confuse the issue, and
must be suppressed. The reflection of the sphere off each mirror, however,
must be faithfully transmitted to the eye. Thus, the light from the mirrors should
contain a diffuse component only when there is no specular component
from the sphere present in the reflection. This allows the mirrors to be
viewed in perspective (yielding important depth cues), while reflecting the
information on the sphere(s) without distortion.
Figure 3 shows the result of this specialized optics -- ozone data from the
Total Ozone Mapping Satellite (TOMS) appears on the outer (translucent)
sphere with the Earth's continents below on the inner (opaque) sphere.

      Finally, one should note that there are a number of degrees of freedom 
in the ``optical solution'' of the sphere and mirrors scene. Placement of 
the sphere, it's size relative to the mirrors, placement of the mirrors and 
the viewpoint, etc., all contribute to the final result. A careful inspection 
of Figure 4, which shows the familiar world map, reveals a blind spot in the 
3D representation. Australia is directly along the line of side but on the
far side of the globe from the viewpoint. In order to bring it into view,
consider adjusting the angle or tilt of the mirrors. Figure 5 shows
the left and right mirrors rotated slightly around their (respective) far 
vertical edge, away from the viewpoint. The resultant optical solution reveals
Australia, but also ``slides'' each virtual (reflected) sphere closer to
the center of the image.

\section{Using the Modified Ray Tracer to Create LUTs}

      The goal is to make use of this visualization technique
on large quantities and varieties of data, without individually ray tracing 
each and every data-texture-map. This can be accomplished 
by a parametric technique such as one described by Sequin and Smyrl$^{7}$
(1989).
The result of our parameterization would be one or more LUTs that 
would be used in conjunction with properly organized datasets.

      An organization of spherical data that lends itself to these
needs is the ``Quadrilateralized Sphere'' (Chan and O'Neill$^{8}$ 1975 and
O'Neill and Laubscher$^{9}$ 1976)
The unit sphere is represented by a deformed cube (figures 6 and 7) in which 
each face is divided into equal-area cells via a recursive subdivision of each 
quadrant. For example, a recursive subdivision into cells $\frac{1}{3}$ 
degree wide produces 393,216 ``square equal area cells'' on the 
sphere, and is the resolution used in our examples. Each cell is 
labelled by a unique integer (4 bytes). This integer identifier is associated 
with a unique latitude and longitude (or unit pointing vector) and provides 
our key parameterization. Figures 6 and 7 show 
the cube before and after it is deformed into a sphere. Notice
the cells near the center of each face (figure 6) are smaller than
the cells near the edges. After the transformation (figure 7), all the cells 
on the sphere have equal area.	 								    

      The 3D LUT is created by a ray trace of the ``Quadsphere''. That
is, rather than using the data directly on the sphere, use the texture map 
comprised of the list of Quadsphere cell {\em identifiers}. When a ray strikes
a point on the sphere the point will lie within a unique Quadsphere cell.
That cell's identifier is then associated with the ray. If the ray has
emanated directly from the viewpoint the algorithm is done. If, however, it 
is the reflection from a mirror, the special optics logic is invoked to insure
that the cell identifier is passed up the ray-tree. If a ray {\em does not}
strike the sphere, but {\em does} strike a mirror, it is assigned a number that
is not a legal cell identifier. Finally, if a ray strikes nothing whatsoever,
it is assigned yet another unique constant.

      An example of the parameterization is shown in figure 8.
This figure shows a single central sphere representing the sky (i.e. the
celestial sphere), displaying the data from  the Infrared Astrophysical
Satellite (IRAS). The ray trace was only performed once to produce one LUT
using 65536 cells per cube face. The LUT was subsequently used to
render the image. The LUT represents an image
grid of 512x512 pixels. Each pixel location in the grid holds an integer that
is either a valid cell identifier ( $\geq 0$ ), or a special number ( $< 0$ )
that indicates the background or mirror colors. Figure 8 is the result of 
merging the three IRAS (25, 60, and 100 micron) wavelengths into a 24 bit 
color image. 

\section{Applications}
      While the static 3D image produced with one LUT can be extremely
useful for visual inspection of the large scale features of the data,
ideally one would like have to interactive control. Interactive capabilities
might include switching between the channels of the multi-spectral
measurements or the parameters of a model, arbitrary rotations of the sphere,
stepping backward and forward in time, or merely pointing and clicking
on image pixels for additional information.

      The LUT method can be used to implement an interactive application,
however, it would be facilitated by a system with large amounts of memory and fast I/O.
Suppose a 512x512 image is desired. A LUT that uses 4 byte integers for the
Quadsphere parameterization of this image size would be 1 Mbyte. Assuming the
data represents an all-globe or all-sky observation at 1/3 degree resolution, 
the number of Quadsphere cells would be \( N = 6 * 4^{8} = 393,216 \). If
the data is simply stored as a sorted array of Quadsphere values at single
precision, approximately 1.5 Mbytes of additional storage is required. The
fastest utilization of the LUT method demands that the entire LUT as well 
as at least one all-sphere array of measurements can be stored in processor
memory. As an interactive application, the essential system demands of the LUT
method are: 1. fast I/O, 2. substantial processor and video memory, 3.
fast raster write and clear operations, and 4. a good graphical user interface.
In the near future, we can expect all of these elements to be common place
in modern workstations.

      One of the simplest interactive applications is to step
back and forth in time using a fixed viewpoint. Such an application 
would require only one LUT at all times. Hopefully the operating system
would allow the application to ``lock'' the LUT in memory to avoid
unnecessary paging. A fast technique would require enough memory to retain
the previous, current, and next time step (data) arrays also in memory, but not
locked. Scrolling back and forth would require replacement of the previous or
next data arrays while displaying whichever is the current.

      Another interactive application is the arbitrary rotation.
This requires multiple LUTs, but hopefully only one dataset. A database of LUTs
--- the result of arbitrary rotations of the sphere --- can be prepared
and set aside for further use. Each ``rotated'' LUT represents a sphere on
which a particular Quadsphere element has been rotated in such a fashion as to
bring it directly into the line of sight. Each unique Quadsphere cell has
associated with it a unit vector that points at its center. Each rotated LUT is
generated by, and subsequently identified by, the cell number who's central
unit vector has been aligned with the (fixed) viewpoint direction (figure 9).
By making use of the Quadsphere scheme, the entire suite of Quadsphere
algorithms are available to organize and access the database of LUTs. Each
Quadsphere cell has a list of nearest neighbors. Other lists are also readily
generated, for example, one might wish to fetch all cells that are within a
given angular distance from a specified cell. Since each LUT is tagged by a
Quadsphere cell number, LUTs can be identified, and images generated, via
Quadsphere cell manipulations. The set of rotated LUTs, associated with an
array of Quadsphere cells, can be accessed arbitrarily to produce the desired
rotation. 

      Pointing and clicking with a mouse on a region or point of the sphere is
simplified by the Quadsphere algorithms. The mouse provides an (x,y) 
index into the LUT that yields a Quadsphere cell number. The cell number
allows direct access to the data array. Adjacent cell numbers
can be readily determined by the nearest neighbor algorithm to help
select a region on the sphere. The selected information can then be used
in a variety of ways. This, combined with the rotation LUT database, would be
an excellent user interface for an on-line database. 


      A number of areas in the domain of NASA's current and future space 
science activities can benefit from this technique. Satellite instruments are 
generally meant to perform for long periods of time. During the instrument's 
lifetime there can be changes in calibration characteristics. There will 
also be many environmental or seasonal changes (passing through Earth's shadow, 
through the Earth's magnetic field, etc.) that can affect the operational 
characteristics of the instruments. With a single lookup table, derived from 
a carefully parameterized ray trace, one can perform animations of either 
time elapsed geophysical or astrophysical data. Such animations can be useful 
for the detection of systematic errors in the instruments that collected the 
data. Mission planning and operations, data integrity and validation, data
reduction and modeling, to name a few, can benefit from interactive applications
based on this visualization method.

	 While the examples discussed above have emphasized some areas
of interest to NASA, this visualization method is general and should benefit a 
broad group of applications. The modified ray trace technique 
provides a general approach to visualization, allowing presentation of data 
on a sphere in a way that preserves the geometry of the measurements. 
The lookup tables obviate the need for special purpose (3D) graphics hardware. 
     

\newpage
\section{Acknowledgements}

      I thank Drs. Mike Hollis, Jim Strong, Richard White, and Milt
Halem for encouraging this effort. I thank Loyd Treinish
for his quick reply to my request for representative ozone data. I also
thank Gene Feldman for the world map, 
Bill Spiesman for the IRAS skymaps, and Ned Wright for his comments on 
the Hammer-Aitoff LUT. All work was performed at NASA's Goddard
Space Flight Center, Space Data and Computing division. 

\newpage
\section{References and Footnotes}
1. Turner Whitted, ``An Improved Illumination Model for Shaded Display'',
\underline{Comm. ACM 23}, 343-349 (1980).
\( \\ \\ \)
2. Roth, S. D., ``Ray casting for modeling solids'', 
\underline{Computer Graphics and Image Processing 18}, 109-144 (1982).
\( \\ \\ \)
3. D. H. Maling Jr., \underline{Coordinate Systems and Map Projections}
(George Philip and Son, Ltd., London, 1973), pp. 77-80.
\( \\ \\ \)
4. John B. Garver Jr., ``New Perspective on the World'', 
\underline{National Geographic}, 911-914 (Dec. 1988).
\( \\ \\ \)
5. The Hammer-Aitoff mapping (for 
\( -\pi \leq \lambda < \pi \: and \: -\pi/2 \leq \phi \leq \pi/2 \, \) ):
\[ \\ x = \frac{2\, C\, \sin(\lambda/2) \, \cos(\phi)}
{\sqrt(1 + \cos(\phi) \, cos(\lambda/2)} \,\,\,\,\,\,\,\,\,\,\,\,
and \,\,\,\,\,\,\,\,\,\,\,\,
y = \frac{C \, \sin(\phi)}{\sqrt(1 + \cos(\phi) \, \cos(\lambda/2)} \]
\( \\ \\ \)
6. The (2D) LUT makes use of the inverse mapping: \( \lambda(x,y) \) and 
\( \phi(x,y) \):
\[ \\ \phi = arcsin( \, 4y \, \sqrt(\frac{1}{2} - y^{2} - \frac{x^{2}}{4}) \, ) 
\,\,\,\,\,\,\,\,\,\,\,\,
and \,\,\,\,\,\,\,\,\,\,\,
\lambda = 2 \, arcsin( \frac{x}{2 \, y} \, tan(\phi) ) \\ \]
where $C$ has been chosen for \( -1 < x < 1 \) and \( -0.5 < y < 0.5 \).
A projected point falls within the bounds:
\( x^{2} + \frac{y^{2}}{(0.5)^{2}} \, \leq \, 1.0 \)
\( \\ \\ \)
7. C. H. Sequin and Eliot K. Smyrl, ``Parameterized Ray Tracing'', 
Proceedings of SIGGRAPH '89 (Boston Massachusetts, July 31 - Aug. 4),
\underline{Computer Graphics 23, 3}, 307-314 (1989).
\( \\ \\ \)
8. F. K. Chan and E. M. O'Neill, \( \\ \) 
\underline{Feasibility Study of a Quadrilateralized Spherical Cube Earth 
Database}, Computer Sciences Corporation, EPRF Technical Report 2-75 (CSC)
(1975).
\( \\ \\ \)
9. E. M. O'Neill and R.E. Laubscher, \( \\ \) 
\underline{Extended Studies of a Quadrilateralized SphericaL Cube Earth 
Database}, Computer Sciences Corporation, NEPRF Technical Report 3-76 (CSC)
(1976).
\( \\ \\ \)

\newpage
\section{Figure Captions}
Figure 1. Hammer-Aitoff world map.
\( \\ \)
Figure 2. 2D and 3D LUTs. Both the Hammer-Aitoff LUT and the 3D LUT are two
dimensional arrays with dimensions equal to the desired image size. Each
( i,j ) element of a LUT is either a valid index ( $>$ 0 ) into the data array 
or a constant ( $<$ 0 ) that is interpreted as the background color.
\( \\ \)
Figure 3. Ray trace of two spheres with three mirrors. Modified optics logic 
suppresses secondary reflections. The texture map on the translucent sphere
shows the low density region of the Ozone layer above Antarctica.
\( \\ \)
Figure 4. Ray trace of one sphere with three mirrors. The texture map on the 
sphere is the same one used in figure 1. Australia is in the blind spot.
\( \\ \)
Figure 5. Ray trace of two slightly rotated mirrors. The blind spot is eliminated.
\( \\ \)
Figure 6. Ray trace of the undeformed Quadsphere. Each face is subdivided 
into 256 cells. The diffuse component of the right mirror was was not 
suppressed by the optics logic.
\( \\ \)
Figure 7. Ray trace of the Quadsphere. The cells are equal area.
\( \\ \)
Figure 8. Three IRAS channels (25, 60, and 100 micron) shown merged into 24 bit 
``RGB'' image of the celestial sphere. The Quadsphere LUT was used
at a resolution of 393216 cells.
\( \\ \)
Figure 9. Rotation of the Quadsphere. The Quadsphere numbering scheme is shown
on one face of the cube in its pre-rotated orientation. The rotated
cube has a cell aligned with the view direction, and is identified by
the cell number. Rotation of the cube about the viewing axis (or twist) is 
another degree of freedom that can be restricted, to keep the size of the
database of rotated LUTs small.


\end{document}		   % End of document

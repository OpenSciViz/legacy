From XRHON@AMARNA  Mon Jul  8 14:45:25 1991
Received: from amarna.gsfc.nasa.gov by okeeffe.gsfc.nasa.gov (5.52/890607.SGI)
	(for xrhon) id AA08917; Mon, 8 Jul 91 14:45:25 EDT
Date:    Mon, 8 Jul 1991 14:46:58 EDT
From: XRHON@AMARNA (David Hon, NASA/GSFC B28 R269, 286-4583)
Message-Id: <910708144658.20200281@AMARNA.GSFC.NASA.GOV>
Subject: idl linkimage
To: xrhon
X-Vmsmail-To: SMTP%"xrhon@okeeffe"

>From:	SMTP%"idl@footprince.colorado.edu" 18-APR-1991 18:28:59.15
To:	xrhon@amarna.gsfc.nasa.gov
CC:	
Subj:	

Date: Thu, 18 Apr 91 16:30:30 -0600
>From: RSI <idl@footprince.colorado.edu>
Message-Id: <9104182230.AA00461@footprince.colorado.edu>
To: xrhon@amarna.gsfc.nasa.gov

Pls send this to:

xrhon@amarna.gsfc.nasa.gov



Dear David:

Here are the files we use to check our loadable drivers under Sun OS
4.1.
pro test_linkimage
;
; Add a procedure, function, and device driver:
;
;Is this a 4.1 or later system?  If not, quit now.

a = findfile('/usr/lib/libdl.so*')
if a(0) eq '' then return
directory = '/home/idl/test'
spawn,'cd '+directory+'; make -f test_linkimage.mak'
default = 'xxx'
exe = directory + '/test_linkimage.so'

linkimage, 'test_dev', exe, 2, def = default  ;Loads the driver

set_plot,'test'		;Test it
plot,[0,1]
plot,[0,1]
end


***** makefile:


test_linkimage.so :  test_linkimage.o
	ld -o test_linkimage.so -assert pure-text \
		test_linkimage.o

test_linkimage.o :   test_linkimage.c
	cc -pic -c test_linkimage.c



***** C program:  (similar to gr_ivas.c which you have)


/* Test the linkimage and the call_external procedures */



#include <stdio.h>
#ifdef VMS
#include "[idl.current]export.h"
#else
#include "../current/export.h"
#endif


/* These modules are used to test modules other than the driver: */
VPTR fun_to_str(argc, argv)
     /* Return the string equivalent of param 1. */
     int argc;
     VPTR *argv[];
{
  VPTR tmp;
  printf("Fun_to_str\n");
  tmp = string(1, argv, 0);
  return(tmp);
}

void pro_to_str(argc, argv)
     /* Replace p1 with a string copy */
     int argc;
     VPTR *argv[];
{
  VPTR tmp;
  printf("PRO_TO_STR\n");
  tmp = string(1,argv,0);
  var_copy(tmp, argv[0]);
}


#ifdef VMS
float fun_fsum(f, n)
     float *f;
     int *n;
{
  float s = 0.0;
  int i;
  for (i= *n; i--; ) s += *f++;
  return(s);
}

int fun_by_value(str1, str2)
     STRING *str1;
     char *str2;
{
  strcpy(str1->s, str2);
  return(strlen(str2));
}

#else
float fun_fsum(argc, argv)
     int argc;
     void *argv[];
{
  float *f = (float *) argv[0];
  int *n = (int *) argv[1];
  float s = 0.0;
  int i;


#ifdef _AIX
  void additional();
  printf("%d fun_fsum\n", argc);
  if (!argc) {			/* Enter routines in tables */
    additional();
    return;
  }

#endif

  for (i= *n; i--; ) s += *f++;
  return(s);
}

int fun_by_value(argc, argv)
     int argc;
     void *argv[];
{
  STRING *str1 = (STRING *) argv[0];
  char *str2 = (char *) argv[1];

  strcpy(str1->s, str2);
  return(strlen(str2));
}
#endif


/***** Here's the Driver: *****/



static count = 0;

private void test_draw(p0, p1, a)   /* Draw a line... */
     POINT *p0, *p1;		/* Endpoints, must be in device coords.. */
     ATTR_STRUCT *a;		/* Line attributes */
{
  if (!count++)
    printf("First point from: (%6d, %6d), to (%6d,%6d)\n",
	   p0->i.x, p0->i.y, p1->i.x, p1->i.y);
}

private void test_erase(a)
     ATTR_STRUCT *a;
{
  printf("Erase, %d vectors.\n", count);
  count = 0;
}



global DEVICE_DEF test_dev = {	  /* Graphics device that does nothing: */
  {4,0,"TEST"},			/* Name */
  {1000,1000},			/* Device size */
  {1000,1000},			/* Visible area */
  {8,13},			/* Text size */
  {13.,13.},			/* Px/cm */
  256,				/* Colors */
  256,				/* Table elements */
  1,				/* Fill interval */
  -1,				/* Window id */
  0,				/* Unit # */
  D_COLOR,			/* Advertise limitations and abilities */
  {0,0},			/* Origin */
  {1,1},			/* ZOOM */
  1.0,				/* Aspect ratio = v_size[0] / v_size[1] */
  {
    test_draw,			/* ^ to draw routine */
    0,				/* text output, or NULL */
    test_erase,			/* erase */
    0,				/* cursor inquire and set, or NULL */
    0,				/* Fill irregular polygon, or NULL */
    0,				/* Return to interactive mode, or NULL */
    0,				/* Flush entry, or NULL */
    0,				/* Load color tables, or NULL */
    0,				/* Pixel input/output, or NULL */
    0,				/* Rout. to call from DEVICE proc, or NULL.*/
    0,				/* Rout. to call for HELP,/DEVICE, or NULL */
  },
  {
    0,				/* Create a window */
    0,				/* Delete a window */
    0,				/* Expose a window */
    0,				/* Set the current window */
    0,				/* Menu function */
  }
};


#ifdef _AIX
void additional()
{
  DEVICE_DEF *d, **pd;

  static SYSFUN_DEF new_pros[] = {	/* Define new procedures */
    { (FUN_RET) pro_to_str, "PRO_TO_STR", 1, 1},
  };

  static SYSFUN_DEF new_fcns[] = {	/* Define new functions */
    { fun_to_str, "FUN_TO_STR", 1, 1 },
  };

  if (! add_system_routine(new_pros, FALSE,
			   sizeof(new_pros) / sizeof(SYSFUN_DEF)) ||
      ! add_system_routine(new_fcns, TRUE,
			   sizeof(new_fcns) / sizeof(SYSFUN_DEF)))
    message(M_GENERIC, 0, MSG_RET,
	    "Error adding system routines");
  
				/* Insert fake device driver  */
  for (pd = dev_list; *pd; pd++);   /* Search for empty slot */
  *pd = &test_dev;		/* Insert it */
  printf("Modules added\n");
}

#endif				/* AIX */


*******

I hope that this helps......


David Stern


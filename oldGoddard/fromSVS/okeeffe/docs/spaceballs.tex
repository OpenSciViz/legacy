\documentstyle[11pt,aip]{article}	% Specifies the document style 

\title{A New Method for Visualizing Data on a Sphere}  % Declares the document's title.
\author{David Hon\thanks{On-site contract at NASA Goddard Space Flight Center,
Code 936, Greenbelt MD. 20771.}
\\ ST Systems Corp.}
%\date{December 27, 1956}   % Deleting this command produces today's date.
\begin{document}	   % End of preamble and beginning of text.
\maketitle		   % Produces the title.

\begin{abstract}
A new method for visualizing data on a globe or unit sphere is described.
Information that is distributed over a sphere -- global oceanographic or 
geographic measurements, all-sky astronomy observations, or any quantities 
that are best represented in spherical coordinates -- can benefit from this
visualization technique. Examples from astronomical and geophysical
datasets, often displayed with an area deforming (2D) projection, are
presented.
Retaining a better sense of the geometry and information content of the data, 
3D graphics can provide an unobstructed view of the entire sphere, without 
undo deformation of its surface area. A ``parameterized ray trace''  
produces lookup tables (LUTs) that can be used for all visualizations. The
ray trace result shows one or more spheres with the data as a texture-map 
and three reflecting rectangles that ``mirror'' the far sides of the sphere(s) 
into view. The LUTs need only be created once, and a general purpose computer 
will do. No special purpose hardware is required beyond a PC or workstation 
that supports color.
\end{abstract}


\section{Introduction}	% Produces section heading.  Lower-level
				    % sections are begun with similar 
				    % \subsection and \subsubsection commands.

      The techniques of computer generated 3D graphics have advanced 
considerably in the past decade and are now becoming standard tools in
many areas of scientific and engineering research. Researchers are
turning to these techniques to aid in the visualization and analysis of
multi-channel data, and also to aid in the modeling of complex physical
systems. These methods are of particular interest when very large quantities
of data must be reduced and examined. NASA's space science projects of
the 90's, for example, present serious demands on data processing and
visualization abilities.

      Spacecraft borne projects in astrophysics, such as the Cosmic 
Background Explorer (COBE), the Roentgen Satellite (ROSAT), the Advanced
X-Ray Astrophysical Facility (AXAF) and others, will produce high-resolution
multi-channel sky surveys. The Upper Atmospheric Remote Sensing
Satellite (UARS), the Earth Observing System (EOS) and others referred
to as``Mission Earth'', will produce tremendous quantities of multi-channel 
Earth surveys. To cope with such large volumes of data,
researchers will rely heavily on advanced graphical methods.

      Most graphics techniques, however, require specialized hardware 
(designed for solid modeling and ``fast'' generation of shaded images) to 
render in 3D. While visualizing scientific data from measurements or models can 
make use of these established methods, general methods can be employed that 
minimize the need for specialized hardware. The method described here can 
be used on any general purpose computer that has a color or grayscale terminal. 

      The goal is to provide a look-up table (LUT) with which spherical data
can be ``merged'' to produce a raster image that displays the data in 3D.
The LUT can be constructed on any general purpose computer and
the resultant raster can be displayed on any monitor -- an XWindows terminal
would be ideal, but not mandatory. The image will display a sphere on 
which the data is a texture-map. Regions of the sphere that are not
directly visible will be reflected into view by appropriately placed mirrors.
The result is an image that displays the entire surface of the sphere in 
perspective, with minimal distortion of its surface area. This kind of scene 
can be readily created by solving the geometric optics with a standard 3D
graphics technique known as ``ray tracing''(Whitted$^{1}$ 1980 and Roth$^{2}$
1982). With some simple 
modifications, the ray trace algorithm can be used to generate LUTs that can
efficiently provide a fixed viewpoint rendition of the sphere.

      Some issues regarding the selection of the modified ray trace technique 
should be mentioned. The traditional perspective transformation algorithm is 
the simplest means of creating rudimentary 3D graphics. The perspective 
algorithm defines a viewing volume and projection screen or window into the 
volume. For example, define a coordinate system centered with the rectangular
window located in the x-y plane, each x,y,z point in the volume is transformed 
into a point on this window by division through z: \( x' = x/z \) 
and \( y' = y/z \) and \( z' = 0 \) . The primed coordinates are interpreted
as viewport or image coordinates. If the points in the viewing volume
are transformed in order from largest z to smallest, the last set of projections
will produce a view with hidden surfaces dealt with properly --- the nearer
objects will obscure, by simple replacement, all other objects within the
same line of sight. This scheme is slow but simple. One can accordingly create 
a LUT in this manner by positioning multiple viewing windows with associated 
volumes and performing the perspective transformations. The resultant images 
of the sphere and mirror rectangles could then be cleverly pieced together
into one image that might closely resemble the product of a ray trace. This
approach would require a fair amount of clever cutting and pasting and
perhaps reorganization of picture elements. 

      The ray trace algorithm, however, provides an accurate result directly. 
While ray tracing solves the general problems of geometric optics, it also 
solves our particular task without resorting to devious and tedious 
approximations. Furthermore, it provides the ability to include translucence. 
The ability to render one or more translucent sphere(s) enveloping an opaque 
sphere provides additional flexibility in displaying multi-channel data. 
A brief description of the modified ray trace method is provided, along with 
the parameterization scheme for the LUT. Some ``interactive'' applications 
that make use of the LUT are also described, along with issues regarding
portability.




\section{``2D vs. 3D'' and the Modified Raytrace Algorithm}

      Astrophysical and geophysical data have one thing in common:
each represents information spatially distributed on a sphere. Geophysical data
is located by ``longitude and latitude''. Astrophysical data is located by
``right ascension and declination'' (when using equatorial coordinates, one of 
many astronomical coordinate systems). Properties associated with
a measurement in a given direction, i.e. a point on the globe or the celestial
sphere, can be associated with a color. Traditional methods for display of
geophysical or astrophysical data rely on 2D maps such as the Hammer-Aitoff
map of the world in figure 1.

      The algorithm for the Hammer-Aitoff projection (McDonnel$^{3}$, also 
Garver$^{4}$ 1988) is conveniently described by the following formula 
(for \( 0 \leq \lambda < 2\pi \: and \: 0 \leq \phi 
\leq \pi \, \) ):
\[ \\ x = \frac{2\, C\, \sin(\lambda/2) \, \cos(\phi)}
{\sqrt(1 + \cos(\phi) \, cos(\lambda/2)} \\ \]
and
\[ \\ y = \frac{C \, \sin(\phi)}{\sqrt(1 + \cos(\phi) \, \cos(\lambda/2)} \\ 
\\ \]

      While the formula could be used directly to render any data on a sphere,
the most efficient approach is to construct a LUT. Preparation and
utilization of a Hammer-Aitoff LUT is a simple example --- and presents a 
useful introduction to the more general 3D method. First decide on the 
desired resolution of the image, say 511x255. Allocate an array of 511x255
tuples, each tuple representing a latitude and longitude pair: Tuple[i][j][0]
and Tuple[i][j][1]. Invert the formula to give \( \lambda(x,y) \) and 
\( \phi(x,y) \):
\[ \\ \phi = arcsin( \, y \, \sqrt(2 - y^{2} - \frac{x^{2}}{4}) \, ) \\ \]
and
\[ \\ \lambda = 2 \, arcsin( \frac{x}{2 \, y} \, tan(\phi) ) \\ \]
where $C$ has been chosen for \( -1 < x < +1 \) and \( -0.5 < y < +0.5 \).
A valid projection point falls within the bounds of the elliptic area:
\[ \\ \\
x^{2} + \frac{y^{2}}{(0.5)^{2}} \, \leq \, 1.0 
\\ \\ \]

Associate the center of the above ellipse with the central element of the
tuple element -- Tuple[255][127] -- and each cell in the array with an
(x,y) coordinate. Populate the cells in the tuple whose associated coordinates
lie outside the ellipse with some constant that can be recognized later as 
an invalid latitude and longitude. Populate all other cells in the 
tuple array with values from the inverted Hammer-Aitoff formula and store the 
array in a file. 

      The Hammer-Aitoff LUT can later be read from the file, along with 
data from another file, to create an image. The technique is most efficient 
when the data is sorted by latitude and longitude and can be held in memory 
for direct access. The LUT array is scanned as a raster image. If an entry
is an invalid tuple, the corresponding image pixel is set to any desired
constant (background) color. If an entry is a valid tuple, convert the
latitude and longitude values to indices into the sorted data array, then
use the selected data value to scale the corresponding image pixel. The 3D
LUT described below is constructed and used in an analogous fashion. The 
solution to the sphere and mirror optics, however, cannot be reduced to a 
simple formula. Additionally, the LUT is devised as a two dimensional array of 
integers associated with equal area cells on the sphere (figure 2).

      The most general and accurate 3D computer graphics method is the
now standard ray trace (Upson and Keeler$^{5}$ 1988, also Sabella$^{6}$ 1988).
3D graphics methods rely on not only on the perspective ``depth queue'', 
to guide the eye, but also the positioning of light and shadow. The ray 
trace technique can be employed for the task of 
constructing shadows a well as reflections (and refractions). Figure 3 shows
a 3mm all-sky map in 3D --- here the algorithm shows a shadow cast by the
sphere and allows the mirrors to reflect each other as well as the sphere,
consequently the shadow is also reflected. While the ray trace algorithm is 
not fast, we need only use it to produce our LUTs. Once created, the LUTs 
can be stored indefinitely for future use.

      The ray trace algorithm is an inverse scattering algorithm. 
Diverging rays are ``cast'' from the viewpoint into the volume containing
objects (surfaces) to be rendered. Each pixel in the rendered image corresponds
to a ``root'' ray cast from the viewpoint. Each root ray is propagated through
the volume until it strikes a surface or exits the volume. If the root ray
strikes a surface, additional rays can be constructed, using the laws of
reflection and refraction, that emanate from the intersection and are also
propagated through the volume. This iterative construction of rays is usually
implemented as a ``recursive cast''. Binary trees are constructed by
associating a ray-surface intersection with a node and the subsequent reflected
and refracted rays and their surface intersections as child nodes. For example,
if all surfaces are equally reflecting and transmitting, the result is a
balanced binary (ray) tree (figure 4). If all surfaces are opaque, however, 
each ray node would only have one child, thus producing a singly linked list.

      By sampling the volume of space surrounding the collection of surfaces
with a large number of rays, one can solve the optics of the scene to an
arbritrary precision. Once (or while) the ray tree is constructed, the optical
properties of all the ``struck'' surfaces are gathered together to evaluate the
final intensity of the root ray --- which is then associated with an image
pixel. The simplest ray cast, in which the root rays are propagated through
the volume to find the first surface of intersection, but no subsequent
iterations are allowed, produces a result identical to the traditional
perspective algorithm. Figure 5 is the result of such a solution, where the
color of the rectangles struck by the rays, and hence the color of the rays,
derive from data-texture-maps associated with each rectangle. The bottom 
rectangle displays the world map in a cylindrical equidistant projection;
the left rectangle displays a radio wavelength (19 GHz) ``skymap'' in a 
global sinusoidal projection.
For \( \leq \lambda < 2\pi \) and \( 0 \leq \phi \leq \pi \, \) the 
cylindrical equidistant projection is:
\[ \\ \\
x = C \, \lambda \,\,\,\,\,\, and \,\,\,\,\,\,  y = C \, \phi \\ \]
and the global sinusoidal is: 
\[ \\ x = C \, \lambda \, \sin(\phi) \,\,\,\,\,\ and \,\,\,\,\,\, y = C \, \phi \\ \]

The application should provide the most realistic and informative presentation 
of the data. Accordingly, some liberties are taken when performing the ray 
trace, in order to provide an unobscured view of the data while attempting to 
retain as many depth queues a possible. For this purpose, it is sufficient 
to define the net color of our root rays (which correspond to our final image 
pixels) as an integration ($I$) over all reflected ($R$), refracted ($T$) and 
emitted ($E$) information from the $N$ surfaces struck by the ray tree. 
\[ \\ \\
 I = \sum_{j=1}^{N} ( W_{j}^{r} * R_{j}(\theta) + 
      	      W_{j}^{t} * T_{j}(\theta) + 
      		 W_{j}^{e} * E_{j}(\theta) ) 
\\ \\ \]
where each component of the total intensity is a function of the angle $\theta$
the ray makes with the surface normal.
The $W$'s are weights that allow us to enforce our {\em optics
logic}. The weights provide information not only about optical surface 
properties but also how, if at all, to make use of each surface. 
\section{Further Details of the Modified Ray Trace}

      Figure 5. shows an example of the simplest type of ray trace:
ray-surface intersections trees are limited to a depth of one, so refraction
and specular reflection are not apparent. This image shows three rectangles,
each with its own ``data-texture-map'' (emission). These are typical data on a
sphere that can be viewed as such. Futhermore, more than one channel or dataset 
can be viewed at one time. 

      The task of viewing two spherical datasets is accomplished by
constructing a scene with two concentric spheres, allowing the outer sphere
some amount of transmittance as well as emittance (figure 4). This requires 
the ray tracer to generate (surface intersection) trees of a depth 
considerably greater than one, and consequently images far too complex! 
Figure 6 shows a scene with two concentric spheres ``emitting'' 
data-texture-maps. All ray trees are cast to a pre-defined depth. 

      This confusing image clearly demonstrates the need for additional logic
in the optics. The desired image should present the information accurately
and without distracting side-effects. Note in figure 6 how the reflected data 
texture-maps, as well as the the reflected mirrors, are brighter than the 
originals --- due to the diffuse component of the mirrors. This increase in 
brightness distorts the actual data values. The diffuse
component, however, is necessary --- the mirrors shown in perspective provide
a useful depth cue in the 3D image. The reflections of the mirrors and the
secondary reflections of the texture-maps, which result from allowing the
ray-tree to propagate to a sufficient depth, are also distracting. 
Rays that strike the mirrors and reflect into the outer sphere and refract
through to the inner sphere are desired, but these rays must {\em exclude} 
the diffuse contribution of light of the mirrors. Each mirror must be seen
in perspective {\em without} the confusing specular reflections of the
other mirrors. Additions to the standard ray trace method that 
accommodate these requirements are incorporated in the modified optics
logic.

      In order to properly define the optics logic (weights) the ray cast
must retain sufficient information about each ray's history. A ray emanates 
from one surface and strikes another. Information about the position of the 
ray's end points and the surface normals, as well as surface optical 
properties, is stored in the SURFACE data structures. A ray's color
is determined by the the surface's optical properties, the ray's angle
of incidence, and the color of the ray's (reflected and refracted) children. 
This optics logic and ray cast history might make use of the following pair of 
data structures in the 'C' language:

\begin{verbatim}

/* some contents of "ray.h" */
typedef struct surface {char *type;
      			int id; 
      			float thick;
      			float pos[3]; 
      			float norm[3]; 
      			REFLECT *refl;
      			REFRACT *tran; 
      			TEXTURE *texture; 
      			} SURFACE;

typedef struct ray {	short depth;
      			float dir[3];
      			COLOR intens;
      			SURFACE *emanate;
      			SURFACE *strike;
      			struct ray *refl;
      			struct ray *tran;
      			} RAY;

\end{verbatim}


      The algorithm performs a recursive cast where the optics is 
evaluated as an in-order traversal of the ray tree (reflection == left child, 
transmission == right child). The optics subroutine is provided with
the {\em current} ray, sets its color, and constructs and applies 
the specialized optical logic coefficients.
The optics routine compares a ray's {\em emanation}
surface to its {\em struck} surface type. To prevent reflections
of the mirrors by the mirrors, simply set the reflection weight to
zero if the surfaces match. For the mirrors to reflect the
sphere texture maps perfectly while suppressing all other contributions,
check the emanation and struck surface identifications.
Figure 7 shows the end result of the specialized optics -- ozone data from the
Total Ozone Mapping Satellite (TOMS) appears on the outer (translucent)
sphere with the Earth's continents below on the inner (opaque) sphere.

      Finally, one should note that there are a number of degrees of freedom 
in the ``optical solution'' of the sphere and mirrors scene. Placement of 
the sphere, it's size relative to the mirrors, placement of the mirrors and 
the viewpoint, etc., all contribute to the final result. A careful inspection 
of Figure 8, which shows the familiar worldmap, reveals a blind spot in the 
3D representation. Australia is directly along the line of side but on the
far side of the globe from the viewpoint. In order to bring it into view,
consider adjusting the angle or tilt of the mirrors. Figure 9 shows
the left and right mirrors rotated slightly around their (respective) far 
vertical edge, away from the viewpoint. The resultant optical solution reveals
Australia, but also ``slides'' each virtual (reflected) sphere closer to
the center.

\section{Using the Modified Ray Tracer to Create LUTs}

      The goal is to make use of this visualization technique
on large quantities and varieties of data, without individually ray tracing 
each and every data-texture-map. This can be accomplished 
by a parametric technique such as one described by Sequin and Smyrl$^{7}$
(1989).
The result of our parameterization would be one or more LUTs that 
would be used in conjunction with properly organized datasets.

      An organization of spherical data that lends itself to these
needs is the ``quadrilateralized sphere'' (Chan and O'Neill$^{8}$ 1975 and
O'Neill and Laubscher$^{9}$ 1976)
The unit sphere is represented by a deformed cube (figure 10) in which 
each face is divided into equal-area cells via a recursive subdivision of each 
quadrant. For example, a recursive subdivision into cells $\frac{1}{3}$ 
degree wide produces 393,216 ``square equal area cells'' on the 
sphere, and is the resolution used in our examples. Each cell is 
labelled by a unique integer (4 bytes). This integer identifier is associated 
with a unique latitude and longitude (or unit pointing vector) and provides 
our key parameterization. 

      The transformation from the rectangular coordinates of the undeformed
face \(\, (x,y) \, \) to the curvilinear coordinates of the spherically 
deformed face \( \, (\xi,\eta) \, \):
\[ \\ \xi = f(x,y) \: and \: \eta = f(y,x) \\ \] (figures 11a and 11b)
together with its inverse:
\[ \\ x = f^{-1}(\xi,\eta) \: and \: y = f^{-1}(\eta,\xi) \\ \\ \]
can be approximated to a high precision. This function is given below without 
derivation:
\[ \\	f(x,y) = \gamma x + \frac{(1-\gamma)}{r_{0}^{2}} x^{3} + 
      	 x y^{2} ( r_{0}^{2} - x^{2} ) \, [ \,\delta + (r_{0}^{2} - y^{2})
\sum_{i,j = 0}^{2} c_{ij} x^{2i} y^{2j} \, ] \, 
      		x^{3} (r_{0}^{2} - x^{2}) [\omega + (r_{0}^{2} - x^{2})
\sum_{i=0}^{2} d_{i} x^{2i} \, ] \\ \]
where
\[ \\	\delta = \frac{1}{4r_{0}^{4}}[-(\mu + 2\gamma) + 
\sqrt{\mu^{2} - 4\mu\gamma + 4\gamma^{2} + 16\sqrt{2}\gamma^{2}} \approx 0.79048
\\ \]
and
\[ \\ \omega = \frac{1}{4r_{0}^{4}}[3 - 2\gamma - \mu - 2r_{0}^{4}\gamma \approx 
-1.2254 \\ \]
with
\[ \\ \gamma = \sqrt{\frac{\pi}{6}} , \,\,  \mu = \sqrt{\frac{\sqrt{3}\pi}{2}} ,
\,\, \, and \,\,\, r_{0} = \frac{1}{\sqrt{3}} \\ \]

The coefficients $c_{ij}$ and $d_{i}$ are tabulated:
$\underline{c_{00} = -2.7217}$, $\underline{c_{10} = -5.5842}$,
$\underline{c_{01} = 2.1711}$, $\underline{c_{20} = -3.4578}$,
$\underline{c_{21} = -6.4160}$, $\underline{c_{22} = 1.9736}$,
$\underline{d_{0} = 1.4833}$, $\underline{d_{1} = 1.1199}$,
$\underline{d_{2} = 6.0515}$.
Figure 12 shows the undeformed cube.
Each face is subdivided into 256 cells to provide a low resolution partitioning
of the sphere into 1536 cells. Notice the cells near the center of each
face are smaller than those near the edges and corners. Figure 13 shows
the the cube after it is deformed into a sphere -- all the cells now have
equal area.	 

      Rather than using the data directly on the sphere, the texture map 
is simply the list of quadsphere cell {\em identifiers}. When a ray strikes
a point on the sphere the point will lie within a unique quadsphere cell.
That cell's identifier is then associated with the ray. If the ray has
emanated directly from the viewpoint the algorithm is done. If, however, it 
is the reflection from a mirror, the special optics logic is ivoked to insure
that the cell identifier is passed up the ray-tree. If a ray {\em does not}
strike the sphere, but {\em does} strike a mirror, it is assigned a number that
is not a legal cell identifier. Finally, if a ray strikes nothing whatsoever,
it is assigned yet another unique constant.

      Some examples of our parameterization are shown in figures 14a -- 14b. 
These figures show a single central sphere representing the sky (i.e. the
celestial sphere), displaying the data from  the Infrared Astrophysical
Satellite (IRAS). The ray trace was only performed once to produce one LUT
using 65536 cells per cube face. The LUT was subsequently used to
render all four images. The contents of the LUT represent an image
grid of 512x512 pixels. Each pixel location in the grid holds an integer that
is either a valid cell identifier ( $\geq 0$ ), or a special number ( $< 0$ )
that indicates the background or mirror colors. Figures 14a, 14b, and 14c show 8
bit pseudo color images of the IRAS 20, 60 and 100 micron wavelengths. Figure
14d is the result of merging the three wavelengths into a 24 bit true color
image. 

      By appropriate selection of integer cell identifiers, the quadsphere 
representation can provide fast indexing to any arbitrary point or region
on the sphere. Each face is represented by a balanced quadtree. The leaf 
nodes of the quadtrees are the individual cells. Chan and O'Neill recommend
a very useful numbering scheme for the cell identifiers (figure 15). As an
example of the numbering logic, subdivide each face into 16 cells, start with
lower left corner cell and assign it the number 0. Move to the nearest east
neighbor and assign it the identifier 1. Move to 0's nearest north neighbor and
assign it 2. Move to 0's northeast neighbor and assign it 3. Now move to the
east neighbor of 1 and assign it 4, then use the same sequence for cell 4 as
for cell 0 to assign cells 5, 6, and 7. Next northwest to the north neighbor of
2 and assign it 8 and apply the same sequence for 8 as 0 and 4 to define 9, 10,
and 11. Finally finish the northeast quadrant of the face, assigning 11, 13,
14, and 15 in the same order. The even bits of a cell identifier can be
collected to give it's x position on the face and the odd bits taken together
yield the cell's y position. Each cell identifier is a cleverly coded
(x,y) coordinate on a face. This scheme becomes useful for very high resolution
measurements and is the basis for a suite of quadsphere cell manipulation
algorithms. 


\section{Issues for Interactive Applications}

      While the static 3D image produced with one LUT can be extremely
useful for visual inspection of the large scale features of the data,
ideally one would like have to interactive control. Interactive capabilities
might include switching between the channels of the multi-spectral
measurements or the parameters of a model, arbitrary rotations of the sphere,
stepping backward and forward in time, or merely pointing and clicking
on image pixels for additional information.

      The LUT method can be used to implement an interactive application,
however, it would be facilitated by a system with large amounts of memory and fast I/O.
Suppose a 512x512 image is desired. A LUT that uses 4 byte integers for the
quadsphere parameterization of this image size would be 1 Mbyte. Assuming the
data represents an all-globe or all-sky observation at 1/3 degree resolution, 
the number of quadsphere cells would be \( N = 6 * 4^{8} = 393,216 \). If
the data is simply stored as a sorted array of quadsphere values at single
precision, approximately 1.5 Mbytes of additional storage is required. The
fastest utilization of the LUT method demands that the entire LUT as well 
as at least one all-sphere array of measurements can be stored in processor
memory.

      One of the simplest interactive applications is to step
back and forth in time using a fixed viewpoint. Such an application 
would require only one LUT at all times. Hopefully the operating system
would allow the application to ``lock'' the LUT in memory to avoid
unnecessary paging. The fast technique would require enough memory to retain
the previous, current, and next time step arrays also in memory, but not
locked. Scrolling back and forth would require exchanging the data arrays
with those on disk via the quickest I/O channels. For instance, if one
had multiple disks available, one could attempt to evenly distribute the
data across the multiple disks to reduce head contention. The algorithm should
replace the previous or next data arrays while displaying whichever
is the current.

      Another interactive application is the arbitrary rotation.
This requires multiple LUTs, but hopefully only one dataset. The LUTs can
be prepared ahead of time. A database of LUTs --- the
result a set of arbitrary rotations of the sphere --- can be prepared and
set aside for further use. First decide on the desired angular step
between successive rotations. The least painful choice is something on
the order of 20 degrees. The idea is to generate the fewest number of
LUTs while retaining the ability to display a rotation of $\sim$ 20 degrees
about an arbitrary axis. Consider a quadsphere partition of the sphere into
\( N = 6 * 4^{2} = 96 \) cells, with centers having an
angular separation of approximately \( \Theta \sim \frac{360}{16} = 22.5 \)
degrees. Arbitrary rotations of $\sim$ 20 degrees thus require a library of 96 
{\em re-aligned} LUTs. Rotations about the next smaller angle 
(in the quadsphere sense) of 10.25 degrees would require a set of 
\( 4 * 96 = 384 \) LUTs.

      Each ``rotated'' LUT represents a sphere on which a particular
quadsphere element has been rotated in such a fashion as to bring it 
directly into the line of sight. Each unique quadsphere cell has associated 
with it a unit vector that points at its center. Each rotated LUT is generated 
by, and subsequently identified by, the cell number who's central unit vector 
has been aligned with the (fixed) viewpoint direction (figure 15). By making 
use of the quadsphere scheme, the entire suite of quadsphere algorithms are
available to organize and access the database of LUTs. Each quadsphere cell 
has a list of nearest neighbors. Other lists are also readily generated, 
for example, one might wish to fetch all cells that are within a given angular 
distance from a specified cell. Since each LUT is tagged by a quadsphere cell 
number, LUTs can be identified, and images generated, via quadsphere cell 
manipulations. The set of rotated LUTs, associated with an
array of quadsphere cells, can be accessed arbitrarily to produce the desired
rotation (figure 16).

      Pointing and clicking with a mouse on a region or point of the sphere is
simplified by the quadsphere algorithms. The mouse provides an (x,y) 
index into the LUT that yields a quadsphere cell number. The cell number
allows direct access to the data array. Adjacent cell numbers
can be readily determined by the nearest neighbor algorithm to help
select a region on the sphere. The selected information can then be used
in a variety of ways.

      As an interactive application, the essential system demands of the LUT
method are: 1. fast I/O, 2. substantial processor and video memory, 3.
fast raster write and clear operations, and 4. a good graphical user interface.
In the near future, we can expect all of these elements to be common place
in modern workstations. XWindow terminals and workstations present an 
opportunity for development of highly portable applications. The XWindows 
data structures (XImage) and application utility functions (XCreateImage,
XGetImage, XPutImage, XGetPixel, etc.) provide many of the essential
elements. While the XWindow system provides functions for manipulating
bitmaps that include a standard file format and I/O routines, no such
functions exist currently for ``pixmaps'' (Scheifler, et al.$^{10}$ 1988).

      If a small number of LUTs are sufficient, one
might consider storing each as a set of XWindow bitmaps (until XWindows
pixmap file I/O routines arrive). The quadsphere index scheme
requires an array of positive integers, with the total number of cell numbers
depending on the level of resolution required. 512x512 LUTs are adequately
represented with 19 bit integers (\(N = 393,216 = 6 * 4^{8} = 1.5 * 2^{18}\)). 
This scheme would require 19 XWindow bitmap
files that would be converted to one pixmap within the application. The bitmaps
are ASCII files(a C language ``\#define'' statement), hence readily portable.
The routine that converts them into one 3D LUT would 
simply ``include'' each bitmap at compile time and apply bit set operations 
to a ``0'' pixmap of depth 19 (again, with considerable help from the standard 
XWindow procedures, XReadBitmapFile, XCreatePixmapFromBitmap, etc.). This 
requires a fair amount of memory, but has the advantage of 
isolating some of the I/O associated with the LUTs to the compilation. While 
the LUTs can be represented as XWindows data structures, the data itself can 
be in any format and will likely represent the bulk of the I/O load.

\section{In Conclusion}

      While the examples shown above have emphasized some areas of
interest to NASA, this visualization method is general and should benefit a 
broad group of applications. The modified ray trace technique 
provides a general approach to visualization, allowing presentation of data 
on a sphere in a way that preserves the geometry of the measurements. 
Lookup tables obviate the need for special purpose graphics hardware. If 
one has access to a workstation (or XWindows terminal) with a large amount 
of memory, one can load multiple LUTs for fast interactive viewing of 
rotations, time steps, or multiple channels. Interactive interfaces 
to any spherical data, and to the models used to interpret the data, can be 
developed. Portable applications can be developed by adoption of industry
standards, such as XWindows, that allow storage of the LUT(s) as ASCII
files and provide the mechanism for their access and utilization.

      A number of areas in the domain of NASA's current and future space 
science activities can benefit from this technique. Satellite instruments are 
generally meant to perform for long periods of time. During the instrument's 
lifetime there can be changes in calibration characteristics. There will 
also be many environmental or seasonal changes (passing through Earth's shadow, 
through the earth's magnetic field, etc.) that can affect the operational 
characteristics of the instruments. With a single lookup table, derived from 
a carefully parameterized ray trace, one can perform animations of either 
time elapsed geophysical or astrophysical data. Such animations can be useful 
for the detection of systematic errors in the instruments that collected the 
data. Mission planning and operations, data integrity and validation, data
reduction and modeling, to name a few, can benefit from interactive applications
based on this visualization method.

\newpage
\section{Acknowledgements}

      I thank Drs. Mike Hollis, Jim Strong, Richard White, and Milt
Halem for encouraging this effort. I thank Loyd Treinish
for his quick reply to my request for representative ozone data. I also
thank Gene Feldman for the world map, Ed Cheng for the 19 GHz skymap, 
Bill Spiesman for the IRAS skymaps, and Ned Wright for his comments on 
the Hammer-Aitoff LUT. All work was performed at NASA's Goddard
Space Flight Center, Space Data and Computing division. 



\newpage
\section{References}

1. Whitted, T. "An Improved Illumination Model for Shaded Display". Comm.
ACM, 23, pp 343 (June 1980).
\( \\ \\ \)
2. Roth, S. D. "Ray casting for modeling solids". Computer Graphics and
Image Processing 18, pp 109 (1982).
\( \\ \\ \)
3. Porter W. McDonnell, Jr., "Introduction To Map Projections".
\( \\ \\ \)
4. John B. Garver, Jr., "New Perspective on the World". National Geographic
(Dec. 1988).
\( \\ \\ \)
5. Upson and Keeler, "V-Buffer: Visible Volume Rendering", Siggraph (1988). 
\( \\ \\ \)
6. Sabella, "A Rendering Algorithm for Visualizing 3D Scalar Field", 
Siggraph (1988).
\( \\ \\ \)
7. Sequin and Smyrl, "Parameterized Ray Tracing'', Siggraph (1989).
\( \\ \\ \)
8. F. K. Chan and E. M. O'Neill, "Feasibility Study of a Quadrilateralized 
Spherical Cube Earth Database", Computer Sciences Corporation (1975).
\( \\ \\ \)
9. E. M. O'Neill and R.E. Laubscher, "Extended Studies of a Quadrilateralized
SphericaL Cube Earth Database", Computer Sciences Corporation (1976).
\( \\ \\ \)
10. Scheifler, Gettys, and Newman, "Note that no functions have been defined, 
as yet, to read and write images to and from disk files." X Window System C 
Library and Protocol Reference, pp. 317 (1988). 


\newpage
\section{Figure Captions}

Figure 1. Hammer-Aitoff worldmap.
\( \\ \\ \)
Figure 2. 2D and 3D LUTs.
\( \\ \\ \)
Figure 3. 3mm (Cosmic Background) Skymap on sphere. The sphere, its shadow, and 
each mirror panel are reflected.
\( \\ \\ \)
Figure 4. Example ray trees.
\( \\ \\ \)
Figure 5. Perspective of three rectangles. Left: global sinusoidal 19 GHz 
skymap. Bottom: equidistant cylindrical worldmap.
\( \\ \\ \)
Figure 6. Ray trace of two spheres with three mirrors. Outer sphere is 
translucent and shows Ozone density. Inner sphere is the (opaque) worldmap.
All rays are cast indiscriminantly.
\( \\ \\ \)
Figure 7. Ray trace of two spheres with three mirrors. Modified optics logic 
suppresses secondary reflections.
\( \\ \\ \)
Figure 8. Ray trace of one sphere with three mirrors. Texture map on the 
sphere is the same one use in figure 1.
\( \\ \\ \)
Figure 9. Ray trace of two slightly rotated mirrors.
\( \\ \\ \)
Figure 10. Construction of sphere from cube.
\( \\ \\ \)
Figure 11a. Rectilinear to Curvilinear Transformation on a cube face.
\( \\ \\ \)
Figure 11b. A spherically deformed face.
\( \\ \\ \)
Figure 12. Ray trace of the undeformed quadsphere. Each face is subdivided 
into 256 cells. The diffuse component of the right mirror was was not 
suppressed by the optics logic.
\( \\ \\ \)
Figure 13. Ray trace of the Quadsphere. Cells are equal area.
\( \\ \\ \)
Figure 14a. 3D LUT used on IRAS 25 micron skymap with 8 bit color table. 
\( \\ \\ \)
Figure 14b. 3D LUT used on IRAS 60 micron skymap with same 8 bit color table.
\( \\ \\ \)
Figure 14c. 3D LUT used on IRAS 100 micron skymap with same 8 bit color table.
\( \\ \\ \)
Figure 14d. Previous 3 IRAS channels merged into 24 bit ``RGB'' image.
\( \\ \\ \)
Figure 15. Rotated Quadsphere.
\( \\ \\ \)
Figure 16. Database of LUTs for interactive applications.


\end{document}		   % End of document

I.

A. TECHNICAL TOPIC AREA(s): "Computational Tools AND Scalable Libraries of 
			     Computational Algorithms"

   TITLE: "A Distributed Environment for Computing and Visualizing
	   Fluid Dynamics"
   TITLE: "Library Tools for Distributed Computing in a Networked Environment 
	  with Applications to Fluid Dynamics"
   TITLE: "A Scalable Architecture Independent Programming Environment
      	   for Supercomputing Applications"
Tor: Please pick one of the above.

/********************************************************************/


II Summary of Proposal:



A. Innovative Claims for the proposed Research { Pages: 1 }

Innovative applications of parallel processing to a variety of 
compute-intensive problems exist, however, they are usually bound to
a specific architecture. Such an approach is frequently not portable and
limited in scalability. We propose to distribute the computations across a 
multitude of computing platforms where every computational segment 
of an algorithm can be mapped into an architecture that has the
least order of complexity for the given segment. By taking advantage of 
advances in networking technology, the communication delays
across different computing platforms can be made negligible.

There is a strong need to analyze techniques for decomposing a given algorithm 
over a network of heterogeneous parallel architectures. 

The execution-speed of a given application will increase considerably.

/********************************************************************/

B. Deliverables {2 pages} 
        Modeling & visualization ocean surfaces (wind
        waves)


        Modeling of a non-linear plasma jet (Reformulate the
        compute intensive for a heterogeneous environment)

    Each of those above projects will try to address the need
    for doing further research such as what we are trying to
    do and conclude that accessing a library of very commonly used
    functions in different parallel computing platforms will
    enhance the performance of all of the above applications
    and they also remain highly scalable. 

A library of several compute intensive algorithms will be made available 
across different architectures (super scalar, SIMD, MIMD).

/********************************************************************/

C. TBD by Tor

/********************************************************************/

D. Technical Rationale { Pages: 3 }


. Networking
    - Implementation of libraries
    - Technical Discussion on how to coordinate segments (X-Windows; Ultranet)
    - Visualization of Network Activity (Run time and Post-Mortem)
. Technical Details of Fluid Dynamic Problems for an application
    - Modeling & visualization ocean surfaces (wind waves)
    - Modeling of a non-linear plasma jet (Reformulate the compute 
      intensive for a heterogeneous environment)
. Visualization of Calculations
    - Distributed/Embedded Ray-Tracer
    - Interactive Volume Visualizer
. Proposed development's hardware configuration



Several multiprocessor-manufacturing vendors have compilers
for both vectorising and concurrentization. Most parallelizing compilers
analyze code with only one architecture in mind.  It is often
the case that a given compute intensive problem has several 
small pieces of computations that can be mapped
onto different architectures more efficiently. So when
the parallelizing compilers try to map all
such computations onto a single architecture they do so with
varying levels of success for different types of computations. 
People have not thought about splitting a compute intensive
algorithm across a network of parallel architectures.  There
is a strong need to perform theoretical studies and analyze
techniques of decomposing a given algorithm across heterogeneous
architectures. One such tool that needs to be developped is for
the domain decomposition problem  that analyzes code and isolates
pieces of code segments that are meant to be executed in
different architectures and different machines.  This determines 
which type of a machine and architecture combination the code 
will best run on. Application would be for automated routing or at 
least guidance for routing.  We consider specific applications 
that make use of the different algorithms and instrument them 
with RPC calls, or the like, to send data and invoke the different 
library functions on different parallel architectures. Work has 
been done on how to do such things so the development of such tools 
is possible and would be useful.


Each of those above projects will try to address the need
for doing further research such as what we are trying to
do and conclude that accessing a library of very commonly used
functions in different parallel computing platforms will
enhance the performance of all of the above applications
and they also remain highly scalable. 


Computation Algorithms for Scalable Libraries -
**********************************************

It is proposed to develop and integrate very commonly used
mathematical and computational library modules in different 
platforms for build an application development environment
for solving computationally intensive problemsu.  Such an
application development environment will emphasize visualization. 
The environment will consist of run-time libraries with a 
portable graphical user interface.  Once we have such an
environment we propose to test and refine the environment on 
a selected set of applications which have been identified
earlier in the section.  Benchmark studies of executing such
algorithms without our proposed environment and with our 
environment will help us do performance analysis of each
of those applications on different platforms.
	
After proving our technology with benchmark studies on
performance and scalability we propose to develop tools
that will do automatic domain decomposition if necessary
with either user feedback. 


Developing tools for a heterogeneous computational environment. 
--------------------------------------------------------------

Several multiprocessor manufacturing vendors have compilers
for both vectorising and concurrentization.  The performance
of such compilers varies with the degree of sophistication
in the code analysis stage.  Most parallelizing compilers
analyze code with only one architecture in mind.  It is often
the case that a given compute intensive problem has several 
small pieces of computations that can inherently be mapped
onto several custom architectures very efficiently. So when
the parallelizing compilers referred above try to map all
such computations onto a single architecture they do so with
varying levels of success for different types of computations. 
People have not thought about splitting a compute intensive
algorithm across a network of parallel architectures.  There
is a strong need to perform theoretical studies and analyze
techniques of decomposing a given algorithm across heterogeneous
architectures. One such tool that needs to be developped is for
the domain decomposition problem  that analyzes code and isolates
pieces of code segments that are meant to be executed in
different architectures and different machines.  This determines 
which type of a machine and architecture combination the code 
will best run on. Application would be for automated routing or at 
least guidance for routing.  We consider specific applications 
that make use of the different algorithms and instrument them 
with RPC calls, or the like, to send data and invoke the different 
library functions on different parallel architectures. Work has 
been done on how to do such things so the development of such tools 
is possible and would be useful.

A method of implementing the routing system is to  
interactively partition the problem among the available 
processors. The system would report on progress and allow the 
user to modify the results. Also it would query the user about
problems that the computer cannot adequately decide (either 
because of a near equality of the options or to high a 
complexity for the analysis).

There are standard techniques for analyzing source code and 
parallelizing them. (building data dependance graphs and control 
dependance graphs) which help to analyze 
the nature of the dependencies within an algorithm. These
dependency analyses tests can easily be augmented with features 
to parallelize code across a heterogeneous architecture.
What is needed to be developed are filtering techniques to 
detect and isolate 'custom-computations' and appropriate
hand-shaking techniques across such individual computational
units via message passing.

Visualization of the internal operation on a parallel machine 
for debugging purposes.  Incorporate visualization tools which 
will highlight how the computation is being carried out over 
an heterogeneous environment. One example is showing the memory 
accesses in matrix operations to show the processor usage.


/***********************************************************/
E. General Discussion of other research in this Area

Demands for faster computing throughput have increased
perpetually over the later half of the century.  Researchers
who addressed the above issue in improving hardware performance 
have very quickly reached the limits of electromagnetic speed in a
conducting medium.  Concurrent focus was to shy away from
von Neumann computational style and come up with newer
architectural designs.  Over the last twenty years we have
seen extensive research having been carried out in the design
of parallel algorithms, parallel architectures,  their
performance issues and their scalability in general. 

Today, we have a variety of parallel computing architectures
such as SIMD architectures, MIMD architectures,  Vector
Computers, Data Flow architectures,  RISC architectures etc.
All of the above architectural designs have something unique
in their very high number crunching style.  Given an algorithm
and given that the hardware performance is the same in all of 
the above architectures, we can very naturally expect each of 
the above architectures to perform differently which in a way
can be attributed to the efficiency of their design.  There
exist well defined upper bounds for every algorithm on every
parallel architecture.  Also, an algorithm a:1 may map very
efficiently on architecture A:1 but very poorly on A:4.  

/***********************************************************/

III F. Proposer's Previous Accomplishments and Related Work

As an experienced team of professional software engineers in
scientific computing we have experience working with several
compute-intensive applications on the Cray-YMP, MasPar, Convex,
SGI-IRIS, CM-2, iPSC, and Transputer.  To mention a few 

(i)	Scientific visualization, Graphics models, 
	Integrated visualization and computations environment, 
	Visualization of purely computational results,
(ii)	Signal/Image Processing,
(iii)	Simulation of complex fluid flow
(iv)	Numerical Computing and linear algebra,
(v)	Neural Network Simulations, 


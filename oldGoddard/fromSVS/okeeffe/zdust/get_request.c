#include "zdust.h"

int get_request(req)
REQ *req;
{
  int tag, itemval;
  static char itemlabel[80];


    if( psio_eof(PostScriptInput) ) 
    {
	ps_close_PostScript();
	gexit();
    }
    else
    {
	if( tag = ps_notify(itemlabel,&itemval) )
	{
	  printf("tag= %d\n",tag);
	  printf("itemlabel= %s\n",itemlabel);
	  printf("itemval= %d\n",itemval);
	}
	else
        {
	  printf("funny tag returned= %d\n",tag);
	  exit(1);
	}
    }
/* process the request */
/* pitch the globe */
    if( strncmp(itemlabel,"X Axis",6) == 0 )
        req->rot_x = -10 * itemval;
/*
    else if( strncmp(itemlabel,"Y Ax",4) == 0 )
	req->rot_y = 10 * itemval;
*/
/* spin the globe on its axis of ratation */
    else if( strncmp(itemlabel,"Z Ax",4) == 0 )
	req->rot_z = 10 * itemval;
/* scale the zoom factor x1, x10, x100 */
    else if( strncmp(itemlabel,"Zoom",4) == 0 )
    {
	if( itemval == 0 ) req->zoom_fctr = 1;
	if( itemval == 1 ) req->zoom_fctr = 10;
	if( itemval == 3 ) req->zoom_fctr = 10;
	if( itemval == 2 ) req->zoom_fctr = 100;
    }
/* continuous zoom factor is between 1 and 100, scaled by above */
    else if( strncmp(itemlabel,"ZOOM",4) == 0 )
	req->zoom_val = itemval;

    return;
}















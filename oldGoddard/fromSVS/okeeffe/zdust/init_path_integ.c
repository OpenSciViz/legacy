#include "zdust.h"

init_path_integ()
{
   int i, k, n, nval=0, max_nval=0;
   float bincnt, r, z, tmp;
   float phi, x, y, total=0.0;
   float integral;
   static float vm[3], roty[3][3], rotz[3][3];

/*
   integral = z_integ();
   srand(seed);
   maxran = pow(2,15) - 1;
*/
   n = 0;
   vm[0] = cos(3.14159265 / 6.0);
   vm[1] = -sin(3.14156265 / 6.0);
   vm[2] = 0.0;
   dustpoly[0][0] = vm[0];
   dustpoly[0][1] = vm[1];
   dustpoly[0][2] = vm[2];
   dustpoly[NPOLY-32][0] = dustpoly[0][0];
   dustpoly[NPOLY-32][1] = dustpoly[0][1];
   dustpoly[NPOLY-32][2] = dustpoly[0][2];
/* rotation about z axis 60/32 deg. */
   rotz[0][0] = cos(1.0/32.0/3.0 * 3.14159265);
   rotz[0][1] = -sin(1.0/32.0/3.0 * 3.14159265);
   rotz[1][0] = -rotz[0][1];
   rotz[1][1] = rotz[0][0];
   rotz[2][2] = 1.0;
/* tri-mesh derived by 128 rotations of 32 vectors derived from vm */
   for( i = 1; i < 32; i++ ) /*generate vertices for triangular mesh at Jupitor */
   {
     dustpoly[i][0] = rotz[0][0] * dustpoly[i-1][0] +
                        rotz[0][1] * dustpoly[i-1][1] +
                           rotz[0][2] * dustpoly[i-1][2];

     dustpoly[i][1] = rotz[1][0] * dustpoly[i-1][0] +
                        rotz[1][1] * dustpoly[i-1][1] +
                           rotz[1][2] * dustpoly[i-1][2];

     dustpoly[i][2] = rotz[2][0] * dustpoly[i-1][0] +
                        rotz[2][1] * dustpoly[i-1][1] +
                           rotz[2][2] * dustpoly[i-1][2];

     dustpoly[NPOLY-32+i][0] = dustpoly[i][0];
     dustpoly[NPOLY-32+i][1] = dustpoly[i][1];
     dustpoly[NPOLY-32+i][2] = dustpoly[i][2];
   }
/* rotation about y axis 360/128 deg. */
   roty[0][0] = cos(2.0*3.14159265/128.0);
   roty[0][2] = -sin(2.0*3.14159265/128.0);
   roty[2][0] = -roty[0][2];
   roty[2][2] = roty[0][0];
   roty[1][1] = 1.0;
   for( i = 32; i < NPOLY-32; i++ )
   {
     dustpoly[i][0] = roty[0][0] * dustpoly[i-32][0] +
                        roty[0][1] * dustpoly[i-32][1] +
                           roty[0][2] * dustpoly[i-32][2];

     dustpoly[i][1] = roty[1][0] * dustpoly[i-32][0] +
                        roty[1][1] * dustpoly[i-32][1] +
                           roty[1][2] * dustpoly[i-32][2];

     dustpoly[i][2] = roty[2][0] * dustpoly[i-32][0] +
                        roty[2][1] * dustpoly[i-32][1] +
                           roty[2][2] * dustpoly[i-32][2];

   }
   mintemp = 99999999.9;
   maxtemp = -999999999.9;
   for( i=0; i < NPOLY; i++ )
   {
 /* evaluate path integral from RMIN to RMAX*dustpoly[i] */
     dusttemp[i] = 0.0;
     for( n = 0; n < 100; n++ )
     {
       vm[0] = RMIN*dustpoly[i][0] + (n * (RMAX-RMIN)/99.0)*dustpoly[i][0];
       vm[1] = RMIN*dustpoly[i][1] + (n * (RMAX-RMIN)/99.0)*dustpoly[i][1];
       vm[2] = RMIN*dustpoly[i][2] + (n * (RMAX-RMIN)/99.0)*dustpoly[i][2];
       z = vm[2];
       r = sqrt( vm[0]*vm[0] + vm[1]*vm[1] );
       dusttemp[i] = dusttemp[i] + zdust(r,z);
     }
     if( dusttemp[i] > maxtemp ) maxtemp = dusttemp[i];
     if( dusttemp[i] < mintemp ) mintemp = dusttemp[i];
/* redefine dustpoly for use in triangular mesh */
     dustpoly[i][0] = RMAX * dustpoly[i][0];
     dustpoly[i][1] = RMAX * dustpoly[i][1];
     dustpoly[i][2] = RMAX * dustpoly[i][2];
   }
   for( i=0; i < NPOLY; i++ )
         colr_path_integ(dusttemp[i],dustcolr[i]);

   return;
}



#define MAIN
#include "zdust.h"

main()
{
   int i, k, nval=0, max_nval=0;
   float bincnt, r, z, tmp;
   float phi, x, y, total=0.0;
   int maxran, seed=1;
   float integral;

/* init some globals */
   delrot = 150;
   dist = 3.5*RMAX;
   delt = RMAX / 10.0;
   do_zoom = FALSE;
   do_rotate = TRUE;
   colr_md = DUST_COLOR;
   wrred = 0xffff;
   wrgrn = 0xffff;
   wrblu = 0xffff;

/* create the icon interface window */
   if( ps_open_PostScript() == 0 )
   {
        printf("Unable to connect to NeWS server\n");
        exit(1);
   }
   ps_icon_wind();
   ps_flush_PostScript();
        
   integral = z_integ();

   srand(seed);
 
   printf("zdust> init the dust distribution...\n");
   init_dust_points(integral);

   printf("zdust> evaluate the path integral...\n");
   init_path_integ();
   
   printf("zdust> two graphics windows...\n");
   render(RMAX);
}



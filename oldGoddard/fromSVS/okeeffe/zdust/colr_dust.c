#include "zdust.h"

int colr_dust(rmax,r,colr)
float rmax, r, colr[];
{
   float tmp, tmin, tmax, del;
   float RC, CMAX;

   if( colr_md == DUST_GRAY )
   {
      tmp = 1.0 / sqrt( r );
      tmin = 1.0 / sqrt( RMAX*RMAX + ZMAX*ZMAX );
      tmax = 1.0 / sqrt( RMIN );
      del = tmax - tmin;

      colr[0] = 0.5; /*0.1 + 0.9*(tmp - tmin)/del;  */
      colr[1] = colr[0];
      colr[2] = colr[0];

      return;
   }

/*   RC =  1.0 - sqrt( RMIN/r ) + sqrt( RMIN/RMAX ); */
   CMAX = sqrt( RMAX*RMAX + ZMAX*ZMAX );
   RC =  1.0 - sqrt( RMIN/r ) + sqrt( RMIN/CMAX );
   RC =  RC * RC;

   if( RC < 0.2 )
   {
      colr[0] = 0.75 * ( 0.2 - RC );
      colr[1] = 0.0;
      colr[2] = 1.0;
   }
   else if( RC < 0.4 )
   {
      colr[0] = 0.0;
      colr[1] = (RC - 0.2) / 0.2;
      colr[2] = 1.0;
   }
   else if( RC < 0.6 )
   {
      colr[0] = 0.0;
      colr[1] = 1.0;
      colr[2] = 1.0 - (RC - 0.4) / 0.2;
   }
   else if( RC < 0.8 )
   {
      colr[0] = (RC - 0.6) / 0.2;
      colr[1] = 1.0;
      colr[2] = 0.0;
   }
   else
   {
      colr[0] = 1.0;
      colr[1] = 1.0 - (RC- 0.8) / 0.2;
      colr[2] = 0.0;
   }

   return;
}

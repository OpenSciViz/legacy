#include <stdio.h>
#include <gl.h>
#include <device.h>
#include <math.h>
#define FALSE 0
#define TRUE 1
#define NP 32676

float white[3] = { 1.0, 1.0, 1.0 };
float gray[3] = { 0.5, 0.5, 0.5 };
float red[3] = { 1.0, 0.0, 0.0 };
float green[3] = { 0.0, 1.0, 0.0 };
float blue[3] = { 0.0, 0.0, 1.0 };
float yellow[3] = { 1.0, 1.0, 0.0 };
float magenta[3] = { 1.0, 0.0, 1.0 };
float cyan[3] = { 0.0, 1.0, 1.0 };
static float ident[4][4] = { {1.0, 0.0, 0.0, 0.0}, 
			     {0.0, 1.0, 0.0, 0.0}, 
			     {0.0, 0.0, 1.0, 0.0}, 
			     {0.0, 0.0, 0.0, 1.0} };
float dustpnt[NP][3];
long arot = 50, trot = 0; 
char rotaxis = 'z';
float dist = 5.0, delt = 0.1;
short wrred = 0xffff, wrgrn = 0xffff, wrblu = 0xffff;

float zdust(r,z)
float r, z;
{
   float w;

   w = fabs(z / r);
   w = pow(w,1.3); 
   w = exp( -2.6 * w );
   w = w / r;

   return( w );
}

main()
{
   int i, j, k, n=0, nval=0;
   float bincnt, r, z, tmp;
   float phi, x, y, total=0.0;
   int maxran, seed=1;

   srand(seed);
   maxran = pow(2,15) - 1;
   for( j = 0; j < 40 && n < NP && total < NP; j++ )
   {
      z = (j - 20) * 0.02;	    
      for( i = 0; total < NP && n < NP; i++ )
      {
 	   r = 0.1 + 0.9 * i / (1.0*NP/40.0);
/*        tmp = sqrt( 1.0 / r ); */
	   bincnt = zdust(r,z);
	   nval = bincnt;
	   total = total + bincnt;
	   for( k = 0; k < nval && n < NP; k++ )
	   {
		phi = 360.0 * rand() / 57.3;
		x = r * cos( phi );
		y = r * sin( phi );
		dustpnt[n][0] = x;
		dustpnt[n][1] = y;
		dustpnt[n][2] = z;
		n++;
	   }
	}
   }
   printf("total= %f, n= %d, r= %f, z= %f\n",total,n,r,z);
   getchar();
   render(n);
}

int render(n)
int n;
{
float r, colr[3];
int i, k, x0=10, y0=10;

prefposition(x0,x0+988,y0,y0+988);
keepaspect(1,1);
winopen("Zdust");
RGBmode();
RGBwritemask(wrred,wrgrn,wrblu);
drawmode(NORMALDRAW);
doublebuffer();
gconfig();

mmode(MVIEWING);
perspective(350,1.0,0.01,100.0);
loadmatrix(ident);
polarview(dist,10,800,0);
lsetdepth(0,0x7fffff);
zbuffer(TRUE);

while( TRUE )
{
      
   zclear();
   cpack(0);
   clear();

   if( getbutton(MIDDLEMOUSE) ) exit(0);
   if( getbutton(LEFTMOUSE) ) sleep(5);
   pushmatrix();
   rotate(trot,'x'); /*xrotaxis); */
   k = 0;
   while( k < n )
   {
      bgnpoint();
      for( i = 0; i < 256; i++, k++ )
      {
	 r = dustpnt[k][0]*dustpnt[k][0] + dustpnt[k][1]*dustpnt[k][1];
	 if( r < 0.2 )
	 {
	    colr[0] = 0.2 - r;
	    colr[1] = 0.0;
	    colr[2] = 1.0;
	 }
	 else if( r < 0.4 )
	 {
	    colr[0] = 0.0;
	    colr[1] = (r - 0.2) / 0.2;
	    colr[2] = 1.0;
	 }
	 else if( r < 0.6 )
	 {
	    colr[0] = 0.0;
	    colr[1] = 1.0;
	    colr[2] = 1.0 - (r - 0.6) / 0.2;
	 }
	 else if( r < 0.8 )
	 {
	    colr[0] = (r - 0.8) / 0.2;
	    colr[1] = 1.0;
	    colr[2] = 0.0;
	 }
	 else if( r <= 1.0 )
	 {
	    colr[0] = 1.0;
	    colr[1] = 1.0 - (r - 1.0) / 0.2;
	    colr[2] = 0.0;
	 }
	 c3f(colr);
	 v3f(dustpnt[k]);
      }
   }
   popmatrix();
   trot = trot + 50; 
	 
   swapbuffers(); 
}
}

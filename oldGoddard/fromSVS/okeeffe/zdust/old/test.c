#include <stdio.h>
#include <gl.h>
#include <device.h>
#include <math.h>
#define FALSE 0
#define TRUE 1
#define NP 65352

float white[3] = { 1.0, 1.0, 1.0 };
float gray[3] = { 0.5, 0.5, 0.5 };
float red[3] = { 1.0, 0.0, 0.0 };
float green[3] = { 0.0, 1.0, 0.0 };
float blue[3] = { 0.0, 0.0, 1.0 };
float yellow[3] = { 1.0, 1.0, 0.0 };
float magenta[3] = { 1.0, 0.0, 1.0 };
float cyan[3] = { 0.0, 1.0, 1.0 };
static float ident[4][4] = { {1.0, 0.0, 0.0, 0.0}, 
			     {0.0, 1.0, 0.0, 0.0}, 
			     {0.0, 0.0, 1.0, 0.0}, 
			     {0.0, 0.0, 0.0, 1.0} };
float dustpnt[NP][3];
long arot = 50, trot = 0; 
char rotaxis = 'z';
float dist = 5.0, delt = 0.1;
short wrred = 0xffff, wrgrn = 0xffff, wrblu = 0xffff;
int n=0;

float zdust(r,z)
float r, z;
{
   float w;

   w = fabs(z / r);
   w = pow(w,1.3); 
   w = exp( -2.6 * w );
   w = w / r;

   return( w );
}

main()
{
   float f, r, z;
   while( TRUE )
   {
      f = zdust(r,z);
      printf("dens= %f\n",f);
   }
}

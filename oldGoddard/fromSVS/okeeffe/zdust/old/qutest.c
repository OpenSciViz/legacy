#include <stdio.h>    /*  qutest.c  */
#include <gl.h>
#include <device.h>
#include "mouse.h"     /*  contains struct coord def  */

      /* This program tests the call to qdevice */
  main()
  {
  int i;
  struct mycoord x_y, * mouse_posit(), * ptcoord;
  ptcoord = &x_y;
  /*
  Device Rmouse, Mousx, Mousy;
  Rmouse = RIGHTMOUSE;
  Mousx = MOUSEX;
  Mousy = MOUSEY;
  */
  printf(" About to call three qdevice functions\n");
  qdevice(MOUSE1);
  /*
  qdevice(Rmouse);
  qdevice(Mousx);
  qdevice(Mousy);
  
  printf(" About to call prefposition and winopen\n");
  prefposition(0,0,511,511);
  winopen("Hotsy-totsy");
  gconfig();
  color(BLACK);
  clear();
  printf(" About to call mouse_posit\n");
  ptcoord = mouse_posit();
  */
  }    /*  Last line of main  */

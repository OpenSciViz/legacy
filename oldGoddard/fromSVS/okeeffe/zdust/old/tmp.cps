
/createitems {
/items 50 dict dup begin
    /check1 (Color/Grayscale) [/panel_check_off /panel_check_on]
        /Left /notify can 0 0 /new CycleItem send 
        dup /LabelY -4 put 300 260 /move 3 index send def
        
    /check2 (Intensity= Temperature/Density) [/panel_check_off /panel_check_on]
        /Left /notify can 0 0 /new CycleItem send 
        dup /LabelY -4 put 250 240 /move 3 index send def
        
 %   /check3 (Show Worldmap) [/panel_check_off /panel_check_on]
 %       /Left /notify can 0 0 /new CycleItem send 
 %       dup /LabelY -4 put 300 260 /move 3 index send def
 %       
 %   /check4 (Activate Radial Distortion) [/panel_check_off /panel_check_on]
 %       /Left /notify can 0 0 /new CycleItem send 
 %       dup /LabelY -4 put 300 240 /move 3 index send def
 %       
 %   /check5 (Show Orbit/Ephemeris) [/panel_check_off /panel_check_on]
 %       /Left /notify can 0 0 /new CycleItem send 
 %       dup /LabelY -4 put 300 220 /move 3 index send def
 %       
 %   /check6 (Show Satellite Footprint) [/panel_check_off /panel_check_on]
 %       /Left /notify can 0 0 /new CycleItem send 
 %       dup /LabelY -4 put 300 200 /move 3 index send def
        
    /switchitem2 (Cylindrical, Spherical, Cubic Volume) [/toggle1 /toggle2  /toggle3 /toggle2]
        /Left /notify can 0 0 /new CycleItem send 2 280 /move 3 index send def
        
    /switchitem1 (32K, 64K, 128K Particles) [/toggle1 /toggle2 /toggle3 /toggle2]
        /Left /notify can 0 0 /new CycleItem send 2 185 /move 3 index send def
        
    /switchitem3 (Zoom: 1x, 10x, 100x) [/toggle1 /toggle2  /toggle3 /toggle2]
        /Left /notify can 0 0 /new CycleItem send 2 135 /move 3 index send def
        
    /switchitem4 (Window: Left, Right, Both) [/toggle1 /toggle2  /toggle3 /toggle2]
        /Left /notify can 0 0 /new CycleItem send 250 180 /move 3 index send def
        
%    /eyeitem1 (X: Field of View) [/eye1 /eye2 /eye3 /eye4 /eye3 /eye2]
 %       /Left /notify can 0 0 /new CycleItem send 3 160 /move 3 index send def
 %       
 %   /eyeitem2 (Y: Field of View) [/eye1 /eye2 /eye3 /eye4 /eye3 /eye2]
 %       /Left /notify can 0 0 /new CycleItem send 3 130 /move 3 index send def
    
    /bigsliderZOOM (ZOOM: ) [1 100 1] /Right /notify can 220 20
 	/new SliderItem send dup /ItemFrame 1 put
  	168 140 /move 3 index send def
        
    /bigsliderX (X Axis Rot: ) [0 360 0] /Right /notify can 220 20
 	/new SliderItem send dup /ItemFrame 1 put
 	2 110 /move 3 index send def
        
    /bigsliderY (Y Axis Rot: ) [0 360 0] /Right /notify can 220 20
 	/new SliderItem send dup /ItemFrame 1 put
  	2 90 /move 3 index send def
        
    /bigsliderZ (Z Axis Rot: ) [0 360 0] /Right /notify can 220 20
   	/new SliderItem send dup /ItemFrame 1 put
 	2 70 /move 3 index send def
        
    /bigsliderRMN (R_MIN) [1 90 1] /Right /notify can 220 20
 	/new SliderItem send dup /ItemFrame 1 put
  	2 255 /move 3 index send def
        
    /bigsliderRMX (R_MAX, x10) [10 100 100] /Right /notify can 220 20
 	/new SliderItem send dup /ItemFrame 1 put
  	2 235 /move 3 index send def
    
    /bigsliderT1 (Par. T1) [1 100 1] /Right /notify can 220 20
 	/new SliderItem send dup /ItemFrame 1 put
  	500 240 /move 3 index send def
    
    /bigsliderT2 (Par. T2) [1 100 1] /Right /notify can 220 20
 	/new SliderItem send dup /ItemFrame 1 put
  	500 220 /move 3 index send def
    
    /bigsliderT3 (Par. T3) [1 100 1] /Right /notify can 220 20
 	/new SliderItem send dup /ItemFrame 1 put
  	500 200 /move 3 index send def
    
    /bigsliderT4 (Par. T4) [1 100 1] /Right /notify can 220 20
 	/new SliderItem send dup /ItemFrame 1 put
  	500 180 /move 3 index send def
    
    /bigsliderD1 (Par. D1) [1 100 1] /Right /notify can 220 20
 	/new SliderItem send dup /ItemFrame 1 put
  	730 240 /move 3 index send def
    
    /bigsliderD2 (Par. D2) [1 100 1] /Right /notify can 220 20
 	/new SliderItem send dup /ItemFrame 1 put
  	730 220 /move 3 index send def
    
    /bigsliderD3 (Par. D3) [1 100 1] /Right /notify can 220 20
 	/new SliderItem send dup /ItemFrame 1 put
  	730 200 /move 3 index send def
    
    /bigsliderD4 (Par. D4) [1 100 1] /Right /notify can 220 20
 	/new SliderItem send dup /ItemFrame 1 put
  	730 180 /move 3 index send def
    
    /bigsliderD5 (Par. D5) [1 100 1] /Right /notify can 220 20
 	/new SliderItem send dup /ItemFrame 1 put
  	730 160 /move 3 index send def
    
    /bigsliderD6 (Par. D6) [1 100 1] /Right /notify can 220 20
 	/new SliderItem send dup /ItemFrame 1 put
  	730 140 /move 3 index send def
    
    /bigsliderD7 (Par. D7) [1 100 1] /Right /notify can 220 20
 	/new SliderItem send dup /ItemFrame 1 put
  	730 120 /move 3 index send def
        
    /nameitemD (Density Function: ) () /Right /notify can 450 0
    	/new TextItem send 500 295 /move 3 index send def
        
    /nameitemT (Temperature Function: ) () /Right /notify can 450 0
    	/new TextItem send 500 270 /move 3 index send def
    
    /messages /panel_text (<messages come here>) /Right {} can 450 0
    /new MessageItem send dup begin
        /ItemFrame 1 def
        /ItemBorder 4 def
    end 2 2 /move 3 index send def
        
%    /tableitem (Table) [
%        [(One) (Two) /panel_text]
%        [(Four) (yY|_) (Six)]
%    ] /Bottom /notify can 0 0 /new ArrayItem send 300 200 /move 3 index send def
    
end def
/messages items /messages get def
} def


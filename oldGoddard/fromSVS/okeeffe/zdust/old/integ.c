#include <stdio.h>
#include <gl.h>
#include <device.h>
#include <math.h>
#define FALSE 0
#define TRUE 1
#define NP 65352
#define DUST_GRAY -1
#define DUST_COLOR 1
#define RMAX 1.0
#define ZMAX 1.0

float white[3] = { 1.0, 1.0, 1.0 };
float gray[3] = { 0.5, 0.5, 0.5 };
float red[3] = { 1.0, 0.0, 0.0 };
float green[3] = { 0.0, 1.0, 0.0 };
float blue[3] = { 0.0, 0.0, 1.0 };
float yellow[3] = { 1.0, 1.0, 0.0 };
float magenta[3] = { 1.0, 0.0, 1.0 };
float cyan[3] = { 0.0, 1.0, 1.0 };
static float ident[4][4] = { {1.0, 0.0, 0.0, 0.0}, 
			     {0.0, 1.0, 0.0, 0.0}, 
			     {0.0, 0.0, 1.0, 0.0}, 
			     {0.0, 0.0, 0.0, 1.0} };
float dustpnt[NP][3];
long xrot=0, delrot=150;
float dist = 5.0, delt = 0.5;
char do_zoom= FALSE, do_rotate= TRUE, col_md= DUST_GRAY;
short wrred = 0xffff, wrgrn = 0xffff, wrblu = 0xffff;
int n=0;

float zdust(r,z)
float r, z;
{
   float w;

   w = fabs(z / r);
   w = pow(w,1.3); 
   w = exp( -2.6 * w );
   w = w / r;

   return( w );
}

float z_integ()
{
   float tot, r, z;
   int i, j;
  
   r = 0.1; 
   while( r < RMAX )
   {
	z = 0.0;
        r = r + 0.01;
	while( z < ZMAX )
	{
	   tot = tot + zdust(r,z);
	   z = z + 0.01;
	}
   }
   printf("integral r,z <= 1.0: %f\n",total);
}

main()
{
   int i, k, nval=0;
   float bincnt, r, z, tmp;
   float phi, x, y, total=0.0;
   int maxran, seed=1;
   float integral;

   integral = z_integ();

   srand(seed);
   maxran = pow(2,15) - 1;
   r = 0.1 + 1.0*rand()/maxran;
   z = 2.0*( 0.5 - 1.0*rand()/maxran );
   n = 0;
/* generate random numbers in the approp. range and populate the volume */

   while( n < NP )
   {
	tmp = zdust(r,z) / integral;
	n_val = tmp * NP;
	for( i = 0; i <i= n_val; i++)
	{
	   total = total + tmp;
	   phi = 360.0 * rand() / 57.3 / maxran; 
	   x = r * cos( phi );
	   y = r * sin( phi );
	   dustpnt[n][0] = x; 
	   dustpnt[n][1] = y; 
	   dustpnt[n][2] = z; 
	   n++;
	}
   }
}

#include <stdio.h>
#include <gl.h>
#include <device.h>
#include <math.h>
#define FALSE 0
#define TRUE 1
#define NP 65352/2
#define DUST_GRAY 0 
#define DUST_COLOR 1
#define RMAX 10.0
#define ZMAX 10.0

float white[3] = { 1.0, 1.0, 1.0 };
float gray[3] = { 0.5, 0.5, 0.5 };
float red[3] = { 1.0, 0.0, 0.0 };
float green[3] = { 0.0, 1.0, 0.0 };
float blue[3] = { 0.0, 0.0, 1.0 };
float yellow[3] = { 1.0, 1.0, 0.0 };
float magenta[3] = { 1.0, 0.0, 1.0 };
float cyan[3] = { 0.0, 1.0, 1.0 };
static float ident[4][4] = { {1.0, 0.0, 0.0, 0.0}, 
			     {0.0, 1.0, 0.0, 0.0}, 
			     {0.0, 0.0, 1.0, 0.0}, 
			     {0.0, 0.0, 0.0, 1.0} };
float dustpnt[NP][3];
long xrot=0, delrot=150;
float dist = 5.0*RMAX, delt = RMAX/10.;
char do_zoom= FALSE, do_rotate= TRUE, colr_md= DUST_COLOR;
short wrred = 0xffff, wrgrn = 0xffff, wrblu = 0xffff;
int n=0;

float zdust(r,z)
float r, z;
{
   float w;

   w = fabs(z / r);
   w = pow(w,1.3); 
   w = exp( -2.6 * w );
   w = w / r;

   return( w );
}

float z_integ()
{
   float tot, r, z;
   int i, j, maxran;
 
   tot = 0.0; 
/*
   r = 0.1; 
   while( r < RMAX )
   {
	z = 0.0;
        r = r + 0.05;
	while( z < ZMAX )
	{
	   tot = tot + r*zdust(r,z);
	   z = z + 0.05;
	}
   }
*/
   maxran = pow(2,15) - 1;
   i = 0;
   while( i < NP )
   {
        r = 0.1 + RMAX*rand()/maxran;
        z = 2.0*ZMAX*( 0.5 - 1.0*rand()/maxran );
	tot = tot + r*zdust(r,z);
	i++;
   }
   printf("integral= %f, (r,z) < MAX: %f\n",tot,r);
   return( tot );
}

main()
{
   int i, k, nval=0, max_nval=0;
   float bincnt, r, z, tmp;
   float phi, x, y, total=0.0;
   int maxran, seed=1;
   float integral;

   integral = z_integ();

   srand(seed);
   maxran = pow(2,15) - 1;
   n = 0;
/* generate random numbers in the approp. range and populate the volume */

   while( n < NP )
   {
      r = 0.1 + RMAX*rand()/maxran;
      z = 2.0*ZMAX*( 0.5 - 1.0*rand()/maxran );
	tmp = r*zdust(r,z) / integral;
	total = total + tmp;
	nval = tmp * NP;
	if( nval > max_nval ) max_nval = nval;
	for( i = 0; i < nval && n < NP; i++)
	{
	   phi = 360.0 * rand() / 57.3 / maxran; 
	   x = r * cos( phi );
	   y = r * sin( phi );
	   dustpnt[n][0] = x; 
	   dustpnt[n][1] = y; 
	   dustpnt[n][2] = z; 
	   n++;
	}
   }
   printf("total= %f,  n= %d, max_nval= %d\n",total,n,max_nval);
   render(r);
}


int colr_dust(rmax,r,colr)
float rmax, r, colr[];
{
   float tmp, tmin, tmax, del;

   if( colr_md == DUST_GRAY )
   {
      tmp = 1.0 / sqrt( r );
      tmin = 1.0 / sqrt( RMAX*RMAX + ZMAX*ZMAX );
      tmax = 1.0 / sqrt( 0.1 );
      del = tmax - tmin;

      colr[0] = 0.5; /*0.1 + 0.9*(tmp - tmin)/del;  */
      colr[1] = colr[0];
      colr[2] = colr[0];

      return;
   }


   if( r < 0.2*RMAX )
   {
      colr[0] = 0.2 - r;
      colr[1] = 0.0;
      colr[2] = 1.0;
   }
   else if( r < 0.4*RMAX )
   {
      colr[0] = 0.0;
      colr[1] = (r - 0.2*RMAX) / 0.2 / RMAX;
      colr[2] = 1.0;
   }
   else if( r < 0.6*RMAX )
   {
      colr[0] = 0.0;
      colr[1] = 1.0;
      colr[2] = 1.0 - (r - 0.4*RMAX) / 0.2 / RMAX;
   }
   else if( r < 0.8*RMAX )
   {
      colr[0] = (r - 0.6*RMAX) / 0.2 / RMAX;
      colr[1] = 1.0;
      colr[2] = 0.0;
   }
   else 
   {
      colr[0] = 1.0;
      colr[1] = 1.0 - (r - 0.8*RMAX) / 0.2 / RMAX;
      colr[2] = 0.0;
   }

   return;
}

int get_event()
{
long an_event; short ev_data;

#ifdef DEBUG
printf("waiting for event in que\n");
#endif

      an_event = qread(&ev_data);

/*      if( an_event == RIGHTMOUSE  ) exit(0); */
      if( an_event == GKEY )
      {
	  colr_md = DUST_GRAY;
          qreset();
/*	  an_event = qread(&ev_data); */
      }
      if( an_event == CKEY )
      {
	  colr_md = DUST_COLOR;
          qreset();
/*	  an_event = qread(&ev_data); */
      }


      if( an_event == ZKEY )
      {
#ifdef DEBUG
printf("Zoom key event\n");
#endif
	 do_rotate = FALSE; do_zoom = TRUE;
         qreset();
	 an_event == qread(&ev_data);
	 if( an_event != RIGHTMOUSE || an_event != RKEY )
         {
            if( an_event == LEFTMOUSE ) /* zoom in closer */
            {
		dist = dist - delt;
            }
	    else if( an_event == MIDDLEMOUSE ) /* zoom out */
            {
		dist = dist + delt;
            }
	 }
      }
      else if( an_event == RKEY )
      {
#ifdef DEBUG
printf("Roatate key event\n");
#endif
	 do_rotate = TRUE; do_zoom = FALSE;
         qreset();
	 an_event == qread(&ev_data);
	 if( an_event != RIGHTMOUSE || an_event != ZKEY )
         {
            if( an_event == LEFTMOUSE ) 
                xrot -= delrot;
	    else if( an_event == MIDDLEMOUSE ) 
		xrot += delrot;
	 }
      }
      else if( an_event == LEFTMOUSE )
      {
	  if( do_rotate )
          {
	      xrot -= delrot;
          }
          if( do_zoom ) 
          {
	      dist = dist - delt;
	  }
       } 
      else if( an_event == MIDDLEMOUSE )
      {
	  if( do_rotate )
          {
              xrot += delrot;
          }
          if( do_zoom ) 
          {
	      dist = dist + delt;
	  }
       } 
}


int render(rmax)
float rmax;
{
float r, colr[3];
int i, k, x0=10, y0=10;

prefposition(x0,x0+988,y0,y0+988);
keepaspect(1,1);
winopen("Zdust");
RGBmode();
RGBwritemask(wrred,wrgrn,wrblu);
drawmode(NORMALDRAW);
doublebuffer();
gconfig();

mmode(MVIEWING);
perspective(350,1.0,0.01,500.0);
/* ortho( -RMAX, RMAX, -RMAX, RMAX, -RMAX, RMAX ); */
lsetdepth(0,0x7fffff);
zbuffer(TRUE);

qdevice(ZKEY);
qdevice(RKEY);
qdevice(CKEY);
qdevice(GKEY);
qdevice(LEFTMOUSE);
qdevice(MIDDLEMOUSE);
/* qdevice(RIGHTMOUSE); */
unqdevice(MOUSEX);
unqdevice(MOUSEY);
   
  while( TRUE )
  {
   
   zclear();
   cpack(0);
   clear();

   pushmatrix();
   loadmatrix(ident);
   polarview(dist,0,850,0);
   rotate(xrot,'z');
   xrot = xrot + 100;
   if( xrot > 3600 ) xrot = 0;
 
   k = 0;
   while( k < n )
   {
      bgnpoint();
      for( i = 0; i < 256; i++, k++ )
      {
	 r = dustpnt[k][0]*dustpnt[k][0] + dustpnt[k][1]*dustpnt[k][1] +
		dustpnt[k][2]*dustpnt[k][2];
         r = sqrt( r );
         colr_dust(rmax,r,colr);
	 c3f(colr);
	 v3f(dustpnt[k]);
      }
      endpoint();
   }
   cpack(0xffffff);
   circ(0.0,0.0,RMAX*0.39/5.20);
   circ(0.0,0.0,RMAX*0.72/5.20);
   circ(0.0,0.0,RMAX*1.0/5.20);
   circ(0.0,0.0,RMAX*1.52/5.20);
   circ(0.0,0.0,RMAX*2.9/5.20);
   circ(0.0,0.0,RMAX*1.0);
      
   popmatrix();

   swapbuffers(); 
/*   
   get_event();
*/
  }
}

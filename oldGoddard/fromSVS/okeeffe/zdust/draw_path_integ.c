#include "zdust.h"

int draw_path_integ()
{
 int i, k;
 float colr[3];

  for( k = 0; k < NPOLY-33; k = k+32 )
    for( i = 0; i < 31; i++ )
    {
       bgnpolygon();
	 c3f(dustcolr[k+i]);
	 v3f(dustpoly[k+i]);
	 c3f(dustcolr[k+i+1]);
	 v3f(dustpoly[k+i+1]);
	 c3f(dustcolr[k+i+33]);
	 v3f(dustpoly[k+i+33]);
	 c3f(dustcolr[k+i+32]);
	 v3f(dustpoly[k+i+32]);
       endpolygon();
    }

   colr_temp(0.39*EARTH,colr);	
   c3f(colr);
   circ(0.0,0.0,0.39*EARTH);
   colr_temp(0.72*EARTH,colr);	
   c3f(colr);
   circ(0.0,0.0,0.72*EARTH);
   colr_temp(1.0*EARTH,colr);	
   c3f(colr);
   circ(0.0,0.0,1.0*EARTH);
   colr_temp(1.52*EARTH,colr);	
   c3f(colr);
   circ(0.0,0.0,1.52*EARTH);
   colr_temp(2.9*EARTH,colr);	
   c3f(colr);
   circ(0.0,0.0,2.9*EARTH);
   colr_temp(5.2*EARTH,colr);	
   c3f(colr);
   circ(0.0,0.0,5.2*EARTH);

   return;
}

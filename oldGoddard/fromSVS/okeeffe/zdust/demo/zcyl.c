#include <stdio.h>
#include <gl.h>
#include <device.h>
#include <math.h>
#define FALSE 0
#define TRUE 1
/* #define NP 65352 */
#define NP 32676
#define DUST_GRAY 0 
#define DUST_COLOR 1
#define JUPITOR 5.20
#define EARTH 1.0
#define ZMAX 5.20
#define RMAX 5.20
#define RMIN 0.39

float white[3] = { 1.0, 1.0, 1.0 };
float gray[3] = { 0.5, 0.5, 0.5 };
float red[3] = { 1.0, 0.0, 0.0 };
float green[3] = { 0.0, 1.0, 0.0 };
float blue[3] = { 0.0, 0.0, 1.0 };
float yellow[3] = { 1.0, 1.0, 0.0 };
float magenta[3] = { 1.0, 0.0, 1.0 };
float cyan[3] = { 0.0, 1.0, 1.0 };
static float ident[4][4] = { {1.0, 0.0, 0.0, 0.0}, 
			     {0.0, 1.0, 0.0, 0.0}, 
			     {0.0, 0.0, 1.0, 0.0}, 
			     {0.0, 0.0, 0.0, 1.0} };
float dustpnt[NP][3];
long xrot=0, delrot=100;
float dist = 3.5*RMAX, delt = RMAX/10.;
char do_zoom= FALSE, do_rotate= TRUE, colr_md= DUST_COLOR;
short wrred = 0xffff, wrgrn = 0xffff, wrblu = 0xffff;
int n=0;

float zdust(r,z)
float r, z;
{
   float w;

   w = fabs(z / r);
   w = pow(w,1.3); 
   w = exp( -2.6 * w );
   w = w / r;

   return( w );
}

float z_integ()
{
   float tot, r, z;
   int i, j, maxran;
 
   tot = 0.0; 
/*
   r = 0.1; 
   while( r < RMAX )
   {
	z = 0.0;
        r = r + 0.05;
	while( z < ZMAX )
	{
	   tot = tot + r*zdust(r,z);
	   z = z + 0.05;
	}
   }
*/
   maxran = pow(2,15) - 1;
   i = 0;
   while( i < NP )
   {
        r = RMIN + RMAX*rand()/maxran;
        z = 2.0*ZMAX*( 0.5 - 1.0*rand()/maxran );
	tot = tot + r*zdust(r,z);
	i++;
   }
   printf("integral= %f, (r,z) < MAX: %f\n",tot,r);
   return( tot );
}

main()
{
   int i, k, nval=0, max_nval=0;
   float bincnt, r, z, tmp;
   float phi, x, y, total=0.0;
   int maxran, seed=1;
   float integral;

   integral = z_integ();

   srand(seed);
   maxran = pow(2,15) - 1;
   n = 0;
/* generate random numbers in the approp. range and populate the volume */

   while( n < NP )
   {
      r = RMIN + (RMAX-RMIN)*rand()/maxran;
      z = 2.0*ZMAX*( 0.5 - 1.0*rand()/maxran );
	tmp = r*zdust(r,z) / integral;
	total = total + tmp;
	nval = tmp * NP;
	if( nval > max_nval ) max_nval = nval;
	for( i = 0; i < nval && n < NP; i++)
	{
	   phi = 360.0 * rand() / 57.3 / maxran; 
	   x = r * cos( phi );
	   y = r * sin( phi );
	   dustpnt[n][0] = x; 
	   dustpnt[n][1] = y; 
	   dustpnt[n][2] = z; 
	   n++;
	}
   }
   printf("total= %f,  n= %d, max_nval= %d\n",total,n,max_nval);
   render(r);
}


int colr_dust(rmax,r,colr)
float rmax, r, colr[];
{
   float tmp, tmin, tmax, del;
   float RC, CMAX;

   if( colr_md == DUST_GRAY )
   {
      tmp = 1.0 / sqrt( r );
      tmin = 1.0 / sqrt( RMAX*RMAX + ZMAX*ZMAX );
      tmax = 1.0 / sqrt( RMIN );
      del = tmax - tmin;

      colr[0] = 0.5; /*0.1 + 0.9*(tmp - tmin)/del;  */
      colr[1] = colr[0];
      colr[2] = colr[0];

      return;
   }

/*   RC =  1.0 - sqrt( RMIN/r ) + sqrt( RMIN/RMAX ); */
   CMAX = sqrt( RMAX*RMAX + ZMAX*ZMAX );
   RC =  1.0 - sqrt( RMIN/r ) + sqrt( RMIN/CMAX ); 
   RC =  RC * RC;

   if( RC < 0.2 )
   {
      colr[0] = 0.75 * ( 0.2 - RC ); 
      colr[1] = 0.0;
      colr[2] = 1.0;
   }
   else if( RC < 0.4 )
   {
      colr[0] = 0.0;
      colr[1] = (RC - 0.2) / 0.2;
      colr[2] = 1.0;
   }
   else if( RC < 0.6 )
   {
      colr[0] = 0.0;
      colr[1] = 1.0;
      colr[2] = 1.0 - (RC - 0.4) / 0.2;
   }
   else if( RC < 0.8 )
   {
      colr[0] = (RC - 0.6) / 0.2;
      colr[1] = 1.0;
      colr[2] = 0.0;
   }
   else 
   {
      colr[0] = 1.0;
      colr[1] = 1.0 - (RC- 0.8) / 0.2;
      colr[2] = 0.0;
   }

   return;
}

int get_event()
{
long an_event; short ev_data;

#ifdef DEBUG
printf("waiting for event in que\n");
#endif

      an_event = qread(&ev_data);

/*      if( an_event == RIGHTMOUSE  ) exit(0); */
      if( an_event == GKEY )
      {
	  colr_md = DUST_GRAY;
          qreset();
/*	  an_event = qread(&ev_data); */
      }
      if( an_event == CKEY )
      {
	  colr_md = DUST_COLOR;
          qreset();
/*	  an_event = qread(&ev_data); */
      }


      if( an_event == ZKEY )
      {
#ifdef DEBUG
printf("Zoom key event\n");
#endif
	 do_rotate = FALSE; do_zoom = TRUE;
         qreset();
	 an_event == qread(&ev_data);
	 if( an_event != RIGHTMOUSE || an_event != RKEY )
         {
            if( an_event == LEFTMOUSE ) /* zoom in closer */
            {
		dist = dist - delt;
            }
	    else if( an_event == MIDDLEMOUSE ) /* zoom out */
            {
		dist = dist + delt;
            }
	 }
      }
      else if( an_event == RKEY )
      {
#ifdef DEBUG
printf("Roatate key event\n");
#endif
	 do_rotate = TRUE; do_zoom = FALSE;
         qreset();
	 an_event == qread(&ev_data);
	 if( an_event != RIGHTMOUSE || an_event != ZKEY )
         {
            if( an_event == LEFTMOUSE ) 
                xrot -= delrot;
	    else if( an_event == MIDDLEMOUSE ) 
		xrot += delrot;
	 }
      }
      else if( an_event == LEFTMOUSE )
      {
	  if( do_rotate )
          {
	      xrot -= delrot;
          }
          if( do_zoom ) 
          {
	      dist = dist - delt;
	  }
       } 
      else if( an_event == MIDDLEMOUSE )
      {
	  if( do_rotate )
          {
              xrot += delrot;
          }
          if( do_zoom ) 
          {
	      dist = dist + delt;
	  }
       } 
}


int render(rmax)
float rmax;
{
float r, colr[3];
int i, k, x0=10, y0=10;
float unit_ray[3], base_a[3], base_b[3], delt;
float path_a1[256][3], path_a2[256][3], path_a3[256][3], 
	path_b1[256][3], path_b2[256][3], path_b3[256][3],
	path_c1[256][3];
float a1_int, a2_int, b1_int, b2_inti, c1_int;
float a1_colr[3], a2_colr[3], b1_colr[3], b2_colr[3], c1_colr[3], tmp[3];
float a1v[3][3], b1v[3][3], c1v[3][3];

/* define at least two optical paths, a and b, that eminate from points 30 deg
   apart on the earth's orbit but point in the same direction (observe the
   same celestial point) */
unit_ray[0] = 0.0;
unit_ray[1] = 1.0;
unit_ray[2] = 0.0;
base_a[0] = EARTH * cos(30.0/57.3);
base_a[1] = -EARTH * sin(30.0/57.3);
base_a[2] = 0.0;
base_b[0] = EARTH;
base_b[1] = 0.0;
base_b[2] = 0.0;
delt = (EARTH*sin(30.0/57.3) + sqrt(RMAX*RMAX - EARTH*EARTH)) / 256.0;
a1_int = 0.0;
a1_colr[0] = 0.0;
a1_colr[1] = 0.0;
a1_colr[2] = 0.0;
c1_int = 0.0;
c1_colr[0] = 0.0;
c1_colr[1] = 0.0;
c1_colr[2] = 0.0;
for( i = 0; i < 256; i++ )
{
   path_a1[i][0] = base_a[0] + i*delt*unit_ray[0];
   path_a1[i][1] = base_a[1] + i*delt*unit_ray[1];
   path_a1[i][2] = base_a[2] + i*delt*unit_ray[2];
   r = sqrt(path_a1[i][0]*path_a1[i][0] + path_a1[i][1]*path_a1[i][1]);
   a1_int = a1_int + zdust(r,path_a1[i][2]);
   if( r > EARTH ) c1_int = c1_int + zdust(r,path_a1[i][2]);

   r = sqrt( r*r + path_a1[i][2]*path_a1[i][2] ); 
   colr_dust(rmax,r,tmp);
   a1_colr[0] = a1_colr[0] + tmp[0];
   a1_colr[1] = a1_colr[1] + tmp[1];
   a1_colr[2] = a1_colr[2] + tmp[2];
   if( r > EARTH ) 
   {
      c1_colr[0] = c1_colr[0] + tmp[0];
      c1_colr[1] = c1_colr[1] + tmp[1];
      c1_colr[2] = c1_colr[2] + tmp[2];
   }
}
a1_colr[0] = a1_colr[0] / a1_int;
a1_colr[1] = a1_colr[1] / a1_int;
a1_colr[2] = a1_colr[2] / a1_int;
/* make a1 bluer: */
a1_colr[0] = 0.8 * a1_colr[0];
a1_colr[1] = 0.8 * a1_colr[1];
a1_colr[2] = 1.2 * a1_colr[2];
c1_colr[0] = c1_colr[0] / c1_int;
c1_colr[1] = c1_colr[1] / c1_int;
c1_colr[2] = c1_colr[2] / c1_int;
/* make c1 reder: */
c1_colr[0] = 1.4 * c1_colr[0];
c1_colr[1] = 0.6 * c1_colr[1];
c1_colr[2] = 0.6 * c1_colr[2];

delt = sqrt(RMAX*RMAX - EARTH*EARTH) / 256.0;
b1_int = 0.0;
b1_colr[0] = 0.0;
b1_colr[1] = 0.0;
b1_colr[2] = 0.0;
for( i = 0; i < 256; i++ )
{
   path_b1[i][0] = base_b[0] + i*delt*unit_ray[0];
   path_b1[i][1] = base_b[1] + i*delt*unit_ray[1];
   path_b1[i][2] = base_b[2] + i*delt*unit_ray[2];
   r = sqrt(path_b1[i][0]*path_b1[i][0] + path_b1[i][1]*path_b1[i][1]);
   b1_int = b1_int + zdust(r,path_b1[i][2]);
   r = sqrt( r*r + path_b1[i][2]*path_b1[i][2] ); 
   colr_dust(rmax,r,tmp);
   b1_colr[0] = b1_colr[0] + tmp[0];
   b1_colr[1] = b1_colr[1] + tmp[1];
   b1_colr[2] = b1_colr[2] + tmp[2];
}
b1_colr[0] = b1_colr[0] / b1_int;
b1_colr[1] = b1_colr[1] / b1_int;
b1_colr[2] = b1_colr[2] / b1_int;

unit_ray[0] = cos(30.0/57.3);
unit_ray[1] = sin(30.0/57.3);
unit_ray[2] = 0.0;
delt = (sqrt(RMAX*RMAX - EARTH*EARTH) - EARTH*sin(30.0/57.3)) / 256.0;
for( i = 0; i < 256; i++ )
{
   path_a2[i][0] = base_a[0] + i*delt*unit_ray[0];
   path_a2[i][1] = base_a[1] + i*delt*unit_ray[1];
   path_a2[i][2] = base_a[2] + i*delt*unit_ray[2];
}

unit_ray[0] = cos(120.0/57.3);
unit_ray[1] = sin(120.0/57.3);
unit_ray[2] = 0.0;
delt = (EARTH*sin(30.0/57.3) + sqrt(RMAX*RMAX - EARTH*EARTH)) / 256.0;
for( i = 0; i < 256; i++ )
{
   path_b2[i][0] = base_b[0] + i*delt*unit_ray[0];
   path_b2[i][1] = base_b[1] + i*delt*unit_ray[1];
   path_b2[i][2] = base_b[2] + i*delt*unit_ray[2];
}

unit_ray[0] = cos(60.0/57.3);
unit_ray[1] = sin(60.0/57.3);
unit_ray[2] = 0.0;
delt = (-EARTH*sin(30.0/57.3) + sqrt(RMAX*RMAX - EARTH*EARTH)) / 256.0;
for( i = 0; i < 256; i++ )
{
   path_b3[i][0] = base_b[0] + i*delt*unit_ray[0];
   path_b3[i][1] = base_b[1] + i*delt*unit_ray[1];
   path_b3[i][2] = base_b[2] + i*delt*unit_ray[2];
}

delt = sqrt(RMAX*RMAX - EARTH*EARTH) / 256.0;
for( i = 0; i < 256; i++ )
{
   path_a3[i][0] = base_a[0] + i*delt*unit_ray[0];
   path_a3[i][1] = base_a[1] + i*delt*unit_ray[1];
   path_a3[i][2] = base_a[2] + i*delt*unit_ray[2];
}

/*
prefposition(x0,x0+988,y0,y0+988);
keepaspect(1,1); 
*/
winopen("Zdust Cloud            \n");
RGBmode();
RGBwritemask(wrred,wrgrn,wrblu); 
drawmode(NORMALDRAW); 
doublebuffer();

gconfig();

mmode(MVIEWING);
perspective(350,1.0,0.01,500.0);
/* ortho( -RMAX, RMAX, -RMAX, RMAX, -RMAX, RMAX ); */
lsetdepth(0,0x7fffff);
zbuffer(TRUE);

qdevice(ZKEY);
qdevice(RKEY);
qdevice(CKEY);
qdevice(GKEY);
qdevice(LEFTMOUSE);
qdevice(MIDDLEMOUSE);
/* qdevice(RIGHTMOUSE); */
unqdevice(MOUSEX);
unqdevice(MOUSEY);
   
  while( TRUE )
  {
   
   zclear();
   cpack(0);
   clear();

   pushmatrix();
   loadmatrix(ident);
   polarview(dist,0,0,0);
   rotate(xrot,'x');
   
   k = 0;
   while( k < n )
   {
      bgnpoint();
      for( i = 0; i < 256; i++, k++ )
      {
	 r = dustpnt[k][0]*dustpnt[k][0] + dustpnt[k][1]*dustpnt[k][1] +
		dustpnt[k][2]*dustpnt[k][2];
         r = sqrt( r );
         colr_dust(rmax,r,colr);
	 c3f(colr);
	 v3f(dustpnt[k]);
      }
      endpoint();
   }
   cpack(0xffffff);
/*
   circ(0.0,0.0,0.39/5.20);
   circ(0.0,0.0,RMAX*0.72/5.20);
   circ(0.0,0.0,RMAX*1.0/5.20);
   circ(0.0,0.0,RMAX*1.52/5.20);
   circ(0.0,0.0,RMAX*2.9/5.20);
   circ(0.0,0.0,RMAX*1.0);
*/      
   circ(0.0,0.0,0.39*EARTH);
   circ(0.0,0.0,0.72*EARTH);
   circ(0.0,0.0,1.0*EARTH);
   circ(0.0,0.0,1.52*EARTH);
   circ(0.0,0.0,2.9*EARTH);
   circ(0.0,0.0,5.2*EARTH);

/* draw the optical path rays */
   bgnline();
      for( i = 0; i < 256 ; i++ )
      {
	  r = path_a1[i][0]*path_a1[i][0] + path_a1[i][1]*path_a1[i][1] +
		path_a1[i][2]*path_a1[i][2];
	  r = sqrt( r );
	  colr_dust(rmax,r,colr);
	  c3f(colr);
	  v3f(path_a1[i]);
      }
   endline();
/* draw a triangle at the end of the path and color it with the path integral */
   a1v[0][0] = path_a1[255][0];
   a1v[0][1] = 1.05*path_a1[255][1];
   a1v[0][2] = path_a1[255][2];
   a1v[1][0] = 0.85*path_a1[255][0];
   a1v[1][1] = 1.10*path_a1[255][1];
   a1v[1][2] = path_a1[255][2];
   a1v[2][0] = path_a1[255][0];
   a1v[2][1] = 1.15*path_a1[255][1];
   a1v[2][2] = path_a1[255][2];
   bgnpolygon();
	c3f(a1_colr);
	v3f(a1v[0]); v3f(a1v[1]); v3f(a1v[2]);
   endpolygon();
/* draw a triangle at the end of the path and color it with the path integral */
   c1v[0][0] = path_a1[255][0];
   c1v[0][1] = 1.16*path_a1[255][1];
   c1v[0][2] = path_a1[255][2];
   c1v[1][0] = 0.85*path_a1[255][0];
   c1v[1][1] = 1.21*path_a1[255][1];
   c1v[1][2] = path_a1[255][2];
   c1v[2][0] = path_a1[255][0];
   c1v[2][1] = 1.26*path_a1[255][1];
   c1v[2][2] = path_a1[255][2];
   bgnpolygon();
	c3f(c1_colr);
	v3f(c1v[0]); v3f(c1v[1]); v3f(c1v[2]);
   endpolygon();

   bgnline();
      for( i = 0; i < 256 ; i++ )
      {
	  r = path_b1[i][0]*path_b1[i][0] + path_b1[i][1]*path_b1[i][1] +
		path_b1[i][2]*path_b1[i][2];
	  r = sqrt( r );
	  colr_dust(rmax,r,colr);
	  c3f(colr);
	  v3f(path_b1[i]);
      }
   endline();
/* draw a triangle at the end of the path and color it with the path integral */
   b1v[0][0] = path_b1[255][0];
   b1v[0][1] = 1.1*path_b1[255][1];
   b1v[0][2] = path_b1[255][2];
   b1v[1][0] = 1.15*path_b1[255][0];
   b1v[1][1] = 1.15*path_b1[255][1];
   b1v[1][2] = path_b1[255][2];
   b1v[2][0] = path_b1[255][0];
   b1v[2][1] = 1.2*path_b1[255][1];
   b1v[2][2] = path_b1[255][2];
   bgnpolygon();
	c3f(b1_colr);
	v3f(b1v[0]); v3f(b1v[1]); v3f(b1v[2]);
   endpolygon();

   bgnline();
      for( i = 0; i < 256 ; i++ )
      {
	  r = path_a2[i][0]*path_a2[i][0] + path_a2[i][1]*path_a2[i][1] +
		path_a2[i][2]*path_a2[i][2];
	  r = sqrt( r );
	  colr_dust(rmax,r,colr);
	  c3f(colr);
	  v3f(path_a2[i]);
      }
   endline();

   bgnline();
      for( i = 0; i < 256 ; i++ )
      {
	  r = path_b2[i][0]*path_b2[i][0] + path_b2[i][1]*path_b2[i][1] +
		path_b2[i][2]*path_b2[i][2];
	  r = sqrt( r );
	  colr_dust(rmax,r,colr);
	  c3f(colr);
	  v3f(path_b2[i]);
      }
   endline();

   bgnline();
      for( i = 0; i < 256 ; i++ )
      {
	  r = path_a3[i][0]*path_a3[i][0] + path_a3[i][1]*path_a3[i][1] +
		path_a3[i][2]*path_a3[i][2];
	  r = sqrt( r );
	  colr_dust(rmax,r,colr);
	  c3f(colr);
	  v3f(path_a3[i]);
      }
   endline();

   bgnline();
      for( i = 0; i < 256 ; i++ )
      {
	  r = path_b3[i][0]*path_b3[i][0] + path_b3[i][1]*path_b3[i][1] +
		path_b3[i][2]*path_b3[i][2];
	  r = sqrt( r );
	  colr_dust(rmax,r,colr);
	  c3f(colr);
	  v3f(path_b3[i]);
      }
   endline();

   popmatrix();

   swapbuffers(); 
   
   get_event();
  }
}

#include "zdust.h"

float white[3] = { 1.0, 1.0, 1.0 };
float gray[3] = { 0.5, 0.5, 0.5 };
float red[3] = { 1.0, 0.0, 0.0 };
float green[3] = { 0.0, 1.0, 0.0 };
float blue[3] = { 0.0, 0.0, 1.0 };
float yellow[3] = { 1.0, 1.0, 0.0 };
float magenta[3] = { 1.0, 0.0, 1.0 };
float cyan[3] = { 0.0, 1.0, 1.0 };
float ident[4][4] = { {1.0, 0.0, 0.0, 0.0},
                             {0.0, 1.0, 0.0, 0.0},
                             {0.0, 0.0, 1.0, 0.0},
                             {0.0, 0.0, 0.0, 1.0} };

int draw_dust(rmax)
float rmax;
{
float r, colr[3];
int i, k;

   k = 0;
   while( k < NP )
   {
      bgnpoint();
      for( i = 0; i < 256; i++, k++ )
      {
         r = dustpnt[k][0]*dustpnt[k][0] + dustpnt[k][1]*dustpnt[k][1] +
                dustpnt[k][2]*dustpnt[k][2];
         r = sqrt( r );
         colr_dust(rmax,r,colr);
         c3f(colr);
         v3f(dustpnt[k]);
      }
      endpoint();
   }
   cpack(0xffffff);
   circ(0.0,0.0,0.39*EARTH);
   circ(0.0,0.0,0.72*EARTH);
   circ(0.0,0.0,1.0*EARTH);
   circ(0.0,0.0,1.52*EARTH);
   circ(0.0,0.0,2.9*EARTH);
   circ(0.0,0.0,5.2*EARTH);

/* draw the optical path rays */
   bgnline();
      for( i = 0; i < 256 ; i++ )
      {
          r = path_a1[i][0]*path_a1[i][0] + path_a1[i][1]*path_a1[i][1] +
                path_a1[i][2]*path_a1[i][2];
          r = sqrt( r );
          colr_dust(rmax,r,colr);
          c3f(colr);
          v3f(path_a1[i]);
      }
   endline();
/* draw a triangle at the end of the path and color it with the path integral */
   a1v[0][0] = path_a1[255][0];
   a1v[0][1] = 1.05*path_a1[255][1];
   a1v[0][2] = path_a1[255][2];
   a1v[1][0] = 0.85*path_a1[255][0];
   a1v[1][1] = 1.10*path_a1[255][1];
   a1v[1][2] = path_a1[255][2];
   a1v[2][0] = path_a1[255][0];
   a1v[2][1] = 1.15*path_a1[255][1];
   a1v[2][2] = path_a1[255][2];
   bgnpolygon();
        c3f(a1_colr);
        v3f(a1v[0]); v3f(a1v[1]); v3f(a1v[2]);
   endpolygon();
/* draw a triangle at the end of the path and color it with the path integral */
   c1v[0][0] = path_a1[255][0];
   c1v[0][1] = 1.16*path_a1[255][1];
   c1v[0][2] = path_a1[255][2];
   c1v[1][0] = 0.85*path_a1[255][0];
   c1v[1][1] = 1.21*path_a1[255][1];
   c1v[1][2] = path_a1[255][2];
   c1v[2][0] = path_a1[255][0];
   c1v[2][1] = 1.26*path_a1[255][1];
   c1v[2][2] = path_a1[255][2];
   bgnpolygon();
        c3f(c1_colr);
        v3f(c1v[0]); v3f(c1v[1]); v3f(c1v[2]);
   endpolygon();

   bgnline();
      for( i = 0; i < 256 ; i++ )
      {
          r = path_b1[i][0]*path_b1[i][0] + path_b1[i][1]*path_b1[i][1] +
                path_b1[i][2]*path_b1[i][2];
          r = sqrt( r );
          colr_dust(rmax,r,colr);
          c3f(colr);
          v3f(path_b1[i]);
      }
   endline();
/* draw a triangle at the end of the path and color it with the path integral */
   b1v[0][0] = path_b1[255][0];
   b1v[0][1] = 1.1*path_b1[255][1];
   b1v[0][2] = path_b1[255][2];
   b1v[1][0] = 1.15*path_b1[255][0];
   b1v[1][1] = 1.15*path_b1[255][1];
   b1v[1][2] = path_b1[255][2];
   b1v[2][0] = path_b1[255][0];
   b1v[2][1] = 1.2*path_b1[255][1];
   b1v[2][2] = path_b1[255][2];
   bgnpolygon();
        c3f(b1_colr);
        v3f(b1v[0]); v3f(b1v[1]); v3f(b1v[2]);
   endpolygon();

   bgnline();
      for( i = 0; i < 256 ; i++ )
      {
          r = path_a2[i][0]*path_a2[i][0] + path_a2[i][1]*path_a2[i][1] +
                path_a2[i][2]*path_a2[i][2];
          r = sqrt( r );
          colr_dust(rmax,r,colr);
          c3f(colr);
          v3f(path_a2[i]);
      }
   endline();

   bgnline();
      for( i = 0; i < 256 ; i++ )
      {
          r = path_b2[i][0]*path_b2[i][0] + path_b2[i][1]*path_b2[i][1] +
                path_b2[i][2]*path_b2[i][2];
          r = sqrt( r );
          colr_dust(rmax,r,colr);
          c3f(colr);
          v3f(path_b2[i]);
      }
   endline();

   bgnline();
      for( i = 0; i < 256 ; i++ )
      {
          r = path_a3[i][0]*path_a3[i][0] + path_a3[i][1]*path_a3[i][1] +
                path_a3[i][2]*path_a3[i][2];
          r = sqrt( r );
          colr_dust(rmax,r,colr);
          c3f(colr);
          v3f(path_a3[i]);
      }
   endline();

   bgnline();
      for( i = 0; i < 256 ; i++ )
      {
          r = path_b3[i][0]*path_b3[i][0] + path_b3[i][1]*path_b3[i][1] +
                path_b3[i][2]*path_b3[i][2];
          r = sqrt( r );
          colr_dust(rmax,r,colr);
          c3f(colr);
          v3f(path_b3[i]);
      }
   endline();

   return;
}


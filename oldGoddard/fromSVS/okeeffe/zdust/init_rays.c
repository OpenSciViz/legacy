#include "zdust.h"

int init_rays()
{
float rmax, r, colr[3];
int i, k, x0=10, y0=10;
float unit_ray[3], base_a[3], base_b[3], delt;
float path_a1[256][3], path_a2[256][3], path_a3[256][3],
        path_b1[256][3], path_b2[256][3], path_b3[256][3],
        path_c1[256][3];
float a1_int, a2_int, b1_int, b2_inti, c1_int;
float a1_colr[3], a2_colr[3], b1_colr[3], b2_colr[3], c1_colr[3], tmp[3];
float a1v[3][3], b1v[3][3], c1v[3][3];

/* define at least two optical paths, a and b, that emanate from points 30 deg
   apart on the earth's orbit but point in the same direction (observe the
   same celestial point) */
unit_ray[0] = 0.0;
unit_ray[1] = 1.0;
unit_ray[2] = 0.0;
base_a[0] = EARTH * cos(30.0/57.3);
base_a[1] = -EARTH * sin(30.0/57.3);
base_a[2] = 0.0;
base_b[0] = EARTH;
base_b[1] = 0.0;
base_b[2] = 0.0;
delt = (EARTH*sin(30.0/57.3) + sqrt(RMAX*RMAX - EARTH*EARTH)) / 256.0;
a1_int = 0.0;
a1_colr[0] = 0.0;
a1_colr[1] = 0.0;
a1_colr[2] = 0.0;
c1_int = 0.0;
for( i = 0; i < 256; i++ )
{
   path_a1[i][0] = base_a[0] + i*delt*unit_ray[0];
   path_a1[i][1] = base_a[1] + i*delt*unit_ray[1];
   path_a1[i][2] = base_a[2] + i*delt*unit_ray[2];
   r = sqrt(path_a1[i][0]*path_a1[i][0] + path_a1[i][1]*path_a1[i][1]);
   a1_int = a1_int + zdust(r,path_a1[i][2]);
   if( r > EARTH ) c1_int = c1_int + zdust(r,path_a1[i][2]);

   r = sqrt( r*r + path_a1[i][2]*path_a1[i][2] );
   colr_dust(rmax,r,tmp);
   a1_colr[0] = a1_colr[0] + tmp[0];
   a1_colr[1] = a1_colr[1] + tmp[1];
   a1_colr[2] = a1_colr[2] + tmp[2];
   if( r > EARTH )
   {
      c1_colr[0] = c1_colr[0] + tmp[0];
      c1_colr[1] = c1_colr[1] + tmp[1];
      c1_colr[2] = c1_colr[2] + tmp[2];
   }
}
a1_colr[0] = a1_colr[0] / a1_int;
a1_colr[1] = a1_colr[1] / a1_int;
a1_colr[2] = a1_colr[2] / a1_int;
c1_colr[0] = c1_colr[0] / c1_int;
c1_colr[1] = c1_colr[1] / c1_int;
c1_colr[2] = c1_colr[2] / c1_int;

delt = sqrt(RMAX*RMAX - EARTH*EARTH) / 256.0;
b1_int = 0.0;
b1_colr[0] = 0.0;
b1_colr[1] = 0.0;
b1_colr[2] = 0.0;
for( i = 0; i < 256; i++ )
{
   path_b1[i][0] = base_b[0] + i*delt*unit_ray[0];
   path_b1[i][1] = base_b[1] + i*delt*unit_ray[1];
   path_b1[i][2] = base_b[2] + i*delt*unit_ray[2];
   r = sqrt(path_b1[i][0]*path_b1[i][0] + path_b1[i][1]*path_b1[i][1]);
   b1_int = b1_int + zdust(r,path_b1[i][2]);
   r = sqrt( r*r + path_b1[i][2]*path_b1[i][2] );
   colr_dust(rmax,r,tmp);
   b1_colr[0] = b1_colr[0] + tmp[0];
   b1_colr[1] = b1_colr[1] + tmp[1];
   b1_colr[2] = b1_colr[2] + tmp[2];
}
b1_colr[0] = b1_colr[0] / b1_int;
b1_colr[1] = b1_colr[1] / b1_int;
b1_colr[2] = b1_colr[2] / b1_int;

unit_ray[0] = cos(30.0/57.3);
unit_ray[1] = sin(30.0/57.3);
unit_ray[2] = 0.0;
delt = (sqrt(RMAX*RMAX - EARTH*EARTH) - EARTH*sin(30.0/57.3)) / 256.0;
for( i = 0; i < 256; i++ )
{
   path_a2[i][0] = base_a[0] + i*delt*unit_ray[0];
   path_a2[i][1] = base_a[1] + i*delt*unit_ray[1];
   path_a2[i][2] = base_a[2] + i*delt*unit_ray[2];
}

unit_ray[0] = cos(120.0/57.3);
unit_ray[1] = sin(120.0/57.3);
unit_ray[2] = 0.0;
delt = (EARTH*sin(30.0/57.3) + sqrt(RMAX*RMAX - EARTH*EARTH)) / 256.0;
for( i = 0; i < 256; i++ )
{
   path_b2[i][0] = base_b[0] + i*delt*unit_ray[0];
   path_b2[i][1] = base_b[1] + i*delt*unit_ray[1];
   path_b2[i][2] = base_b[2] + i*delt*unit_ray[2];
}

unit_ray[0] = cos(60.0/57.3);
unit_ray[1] = sin(60.0/57.3);
unit_ray[2] = 0.0;
delt = (-EARTH*sin(30.0/57.3) + sqrt(RMAX*RMAX - EARTH*EARTH)) / 256.0;
for( i = 0; i < 256; i++ )
{
   path_b3[i][0] = base_b[0] + i*delt*unit_ray[0];
   path_b3[i][1] = base_b[1] + i*delt*unit_ray[1];
   path_b3[i][2] = base_b[2] + i*delt*unit_ray[2];
}

delt = sqrt(RMAX*RMAX - EARTH*EARTH) / 256.0;
for( i = 0; i < 256; i++ )
{
   path_a3[i][0] = base_a[0] + i*delt*unit_ray[0];
   path_a3[i][1] = base_a[1] + i*delt*unit_ray[1];
   path_a3[i][2] = base_a[2] + i*delt*unit_ray[2];
}

return;
}

#include "zdust.h"

int init_dust_points(integral)
float integral;
{
   int i, n=0, nval, max_nval=0;
   float r, z, tmp;
   float phi, x, y, total=0.0;
   int maxran, seed=1;

   maxran = pow(2,15) - 1;
   while( n < NP )
   {
      r = RMIN + (RMAX-RMIN)*rand()/maxran;
      z = 2.0*ZMAX*( 0.5 - 1.0*rand()/maxran );
        tmp = r*zdust(r,z) / integral;
        total = total + tmp;
        nval = tmp * NP;
        if( nval > max_nval ) max_nval = nval;
        for( i = 0; i < nval && n < NP; i++)
        {
           phi = 360.0 * rand() / 57.3 / maxran;
           x = r * cos( phi );
           y = r * sin( phi );
           dustpnt[n][0] = x;
           dustpnt[n][1] = y;
           dustpnt[n][2] = z;
           n++;
        }
   }
   return;
}



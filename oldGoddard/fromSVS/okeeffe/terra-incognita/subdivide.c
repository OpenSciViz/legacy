#include <stdio.h>
#include <math.h>
#include <gl/gl.h>
#include "nr.h"
#include "nrutil.h"

#define NX 4096
#define NEIGHB 4
#define PI 3.1415926

#define DEBUG

static float Acoef[NEIGHB], Rmat[NEIGHB][NEIGHB], Rhalf[NEIGHB];
static float var=0.5, freq=10, elev[NX];
short wrred = 0xffff, wrblu = 0xffff, wrgrn = 0xffff;
static int dummy= -1; 

/* simple reproduction of the 1-D example in lewis' paper: "Generalized Stochastic Subdivision"
  in ACM Transactions on Graphics 1987
  lewis' example uses the autocorrelation: Rcorr(x) = cos(10*x)exp(-abs(x))  ( -PI<x<PI ?? )
  we'll use something from Numerical Recipes to find the inverse of lewis' "R" matrix Rmat  
*/

main(int argc, char **argv, char **env)
{
 int i, j, fd;
 float x;

  if( argc > 1 )
    elev[NX/2] = var = atof(argv[1]);
  else
    elev[NX/2] = var;

/*   elev[NX/2] = var * gasdev(&dummy); */
/* recursively define */
  left_subdiv(1, NX/4, 0.0, 0.0, elev[NX/2], 0.0); /* assuming NEIGHB == 4 */

  right_subdiv(1, 3*NX/4, 0.0, elev[NX/2], 0.0, 0.0);

  fd = creat(
}

float autocorr(float lag)
{
 float ret_val;


/* Markovian autocorrelation should produce standard fractal subdivision: */
  ret_val = exp(-fabs(lag)); 
  ret_val = cos(freq*(lag)) * ret_val; /* periodic */

 return( ret_val );
}

int left_subdiv(level,val_indx,far_left,near_left,near_right,far_right)
int level, val_indx;
float far_left, near_left, near_right, far_right;
{
 float val, gran, x_inc, x_half_inc, scale=1.0;
 int left_val_indx, right_val_indx;

  x_inc = PI/pow(2.0,1.0*level);
  x_half_inc = x_inc/2;

  if( x_half_inc <= PI/NX )
  {
    return; /* exceeded desired res. */
  }
/* define the autocorrelation derived toeplitz matrix for this level */ 
  Rmat[0][0] = scale * autocorr(0.0);
  Rmat[0][1] = scale * autocorr(1*x_inc);
  Rmat[0][2] = scale * autocorr(2*x_inc);
  Rmat[0][3] = scale * autocorr(3*x_inc);

  Rmat[1][1] = scale * autocorr(0.0);
  Rmat[1][0] = scale * autocorr(1*x_inc);
  Rmat[1][2] = scale * autocorr(1*x_inc);
  Rmat[1][3] = scale * autocorr(2*x_inc);

  Rmat[2][2] = scale * autocorr(0.0);
  Rmat[2][0] = scale * autocorr(2*x_inc);
  Rmat[2][1] = scale * autocorr(1*x_inc);
  Rmat[2][3] = scale * autocorr(1*x_inc);

  Rmat[3][3] = scale * autocorr(0.0);
  Rmat[3][0] = scale * autocorr(3*x_inc);
  Rmat[3][1] = scale * autocorr(2*x_inc);
  Rmat[3][2] = scale * autocorr(1*x_inc);

/* init. the right hand side of the matrix eqn. */
  Rhalf[0] = scale * autocorr(2*x_inc - x_half_inc);
  Rhalf[1] = scale * autocorr(1*x_inc - x_half_inc);
  Rhalf[2] = scale * autocorr(1*x_inc - x_half_inc);
  Rhalf[3] = scale * autocorr(2*x_inc - x_half_inc);

/* solve the eqn: Rmat * Acoef = Rhalf for the unknown coeffs. with the Numerical Recipes
  black box: */

/*  toeplz(Rmat-1,Acoef-1,Rhalf-1,NEIGHB); */
  A_soln(Rmat,Rhalf,Acoef);

/* the value is a weighted sum (via the Acoef) of the 4 neighbors and a random number
  from a selected gaussian probability distribution */

  gran = var * gasdev(&dummy);  /* Numerical Recipes unit gaussian random number generator */
  gran = gran / pow(2.0,1.0*level); 

  val = gran + 1.0*( Acoef[0] * far_left + Acoef[1] * near_left +
		Acoef[2] * near_right + Acoef[3] * far_right );

/* and put the result into the elevation array */
  elev[val_indx] = val;

#ifdef DEBUG1
 fprintf(stderr,"left> level, indx, val: %d %d %f\n",level,val_indx,val);
 sleep(1);
#endif

/* recursively call myself and the right hand routine for further refinements at smaller scales: */
  level++;

  left_val_indx = val_indx - NX/pow(2.0,1.0+level);
  left_subdiv(level, left_val_indx, far_left, near_left, val, near_right); 

  right_val_indx = val_indx + NX/pow(2.0,1.0+level) ;
  right_subdiv(level, right_val_indx, near_left, val, near_right, far_right); 

}


int right_subdiv(level,val_indx,far_left,near_left,near_right,far_right)
int level, val_indx;
float far_left, near_left, near_right, far_right;
{
 float val, gran, x_inc, x_half_inc, scale=1.0;
 int left_val_indx, right_val_indx;

  x_inc = PI/pow(2.0,1.0*level);
  x_half_inc = x_inc/2;

  if( x_half_inc <= PI/NX )
  {
    return; /* exceeded desired res. */
  }
/* define the autocorrelation derived toeplitz matrix for this level */ 
  Rmat[0][0] = scale * autocorr(0.0);
  Rmat[0][1] = scale * autocorr(1*x_inc);
  Rmat[0][2] = scale * autocorr(2*x_inc);
  Rmat[0][3] = scale * autocorr(3*x_inc);

  Rmat[1][1] = scale * autocorr(0.0);
  Rmat[1][0] = scale * autocorr(1*x_inc);
  Rmat[1][2] = scale * autocorr(1*x_inc);
  Rmat[1][3] = scale * autocorr(2*x_inc);

  Rmat[2][2] = scale * autocorr(0.0);
  Rmat[2][0] = scale * autocorr(2*x_inc);
  Rmat[2][1] = scale * autocorr(1*x_inc);
  Rmat[2][3] = scale * autocorr(1*x_inc);

  Rmat[3][3] = scale * autocorr(0.0);
  Rmat[3][0] = scale * autocorr(3*x_inc);
  Rmat[3][1] = scale * autocorr(2*x_inc);
  Rmat[3][2] = scale * autocorr(1*x_inc);

/* init. the right hand side of the matrix eqn. */
  Rhalf[0] = scale * autocorr(2*x_inc - x_half_inc);
  Rhalf[1] = scale * autocorr(1*x_inc - x_half_inc);
  Rhalf[2] = scale * autocorr(1*x_inc - x_half_inc);
  Rhalf[3] = scale * autocorr(2*x_inc - x_half_inc);

/* solve the eqn: Rmat * Acoef = Rhalf for the unknown coeffs. with the Numerical Recipe
  black box: */

/*  toeplz(Rmat-1,Acoef-1,Rhalf-1,NEIGHB); */
  A_soln(Rmat,Rhalf,Acoef);

/* the value is a weighted sum (via the Acoef) of the 4 neighbors and a random number
  from a selected gaussian probability distribution */

  gran = var * gasdev(&dummy);  /* Numerical Recipes unit gaussian random number generator */
  gran = gran / pow(2.0,1.0*level); 

  val = gran + 1.0*( Acoef[0] * far_left + Acoef[1] * near_left +
		  Acoef[2] * near_right + Acoef[3] * far_right );

/* and put the result into the elevation array */
  elev[val_indx] = val;

#ifdef DEBUG1
 fprintf(stderr,"right> level, indx, val: %d %d %f\n",level,val_indx,val);
 sleep(1);
#endif
/* recursively call myself and the left hand routine for further refinements at smaller scales: */
  level++;

  left_val_indx = val_indx - NX/pow(2.0,1.0+level);
  left_subdiv(level, left_val_indx, far_left, near_left, val, near_right); 

  right_val_indx = val_indx + NX/pow(2.0,1.0+level) ;
  right_subdiv(level, right_val_indx, near_left, val, near_right, far_right); 

}

A_soln(Mat,Rhalf,A)
float Mat[NEIGHB][NEIGHB], Rhalf[NEIGHB], A[NEIGHB];
{
 float **a, **y, *col, d;
 int i, j, k, *indx;

  a = matrix(1,NEIGHB,1,NEIGHB);
  y = matrix(1,NEIGHB,1,NEIGHB);
  col = vector(1,NEIGHB);
  indx = ivector(1,NEIGHB);

  for( j = 1; j <= NEIGHB; j++ )
    for( i = 1; i <= NEIGHB; i++ )
      a[i][j] = Mat[i-1][j-1];

  ludcmp(a,NEIGHB,indx,&d);
  for( j = 1; j <= NEIGHB; j++ )
  {
    for( i = 1; i <= NEIGHB; i++ )
      col[i] = 0.0;
    col[j] = 1.0;
    lubksb(a,NEIGHB,indx,col);
    for( i = 1; i <= NEIGHB; i++ )
      y[i][j] = col[i];
  }

  for( j = 0; j < NEIGHB; j++ )
  {
    A[j] = 0.0;  
    for( i = 0; i < NEIGHB; i++ )
      A[j] = A[j] + y[i+1][j+1] * Rhalf[j];
  }
#ifdef DEBUG2
{
 float c[NEIGHB][NEIGHB];       
  for( i = 0; i < NEIGHB; i++ )
  {
    for( j = 0; j < NEIGHB; j++ )
    {
      c[i][j] = 0.0;
      for( k = 0; k < NEIGHB; k++ )
        c[i][j] = c[i][j] + y[i+1][k+1]*Mat[k][j];
    }
  }
}
#endif
  free_vector(col,1,NEIGHB); 
  free_ivector(indx,1,NEIGHB); 
  free_matrix(a,1,NEIGHB,1,NEIGHB);
  free_matrix(y,1,NEIGHB,1,NEIGHB);
}








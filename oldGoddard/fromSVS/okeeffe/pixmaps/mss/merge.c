/* there are [380][960] elements in the input  */
#include <stdio.h>
#include <math.h>
#include <malloc.h>
float mss[380][960];

float interp(lat,lon)
float lat, lon;
{
  int lat_idx, lon_idx;
  float del_lat=142.0/380.0, del_lon=360.0/960.0;
  float val;

  lat_idx = (142.0 - (lat + 72.0)) / del_lat;
  lon_idx = (lon + 180.0) / del_lon;
  if( lon_idx >= 380 ) 
    lon_idx = lon_idx - 380;
  else
    lon_idx = lon_idx + 380;

  val = mss[lat_idx][lon_idx];
  return( val );
}
   
main()
{
  int i, j, fd, nb;
  char *outfile="mss_globe.8bit";/* save the result */
  unsigned char globe[512*1024], bmss[380][960];
  float lat, lon;

  fd = open("/usr/people/xrhon/pixmaps/globe.pixmap",0);
  nb = read(fd,globe,1024*512);   
  close(fd);
  fd = open("avg3x3.8bit",0);
  nb = read(fd,bmss,960*380);
  close(fd);
  for( i = 0; i < 380; i++ )
    for( j = 0; j < 960; j++ )
      mss[i][j] = bmss[i][j];

  lat = 90.0 + 180.0/512.0;
  for( i = 0; i < 512; i++ )
  {
    lat = lat - 180.0/512.0;
    lon = -180.0 - 360.0/1024.0;
    for( j = 0; j < 1024; j++ )
    {
      lon = lon + 360.0/1024.0;
      if( globe[i*1024 + j] >= 127 ) 
	globe[i*1024 + j] = 255;
      if( (lat <= 70.0) && (lat >= -72.0) )
        if( globe[i*1024 + j] == 0 ) globe[i*1024 + j] = (char) interp(lat,lon);
     }
  }
  fd = creat(outfile,0644);
  write(fd,globe,1024*512);
  close(fd);

}

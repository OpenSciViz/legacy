#include "globe_mesh.h"
#include "request.h"
#include <time.h>

/* clock_t tm; */
float tm;

int tri_mesh(req)
REQ *req;
{
   int i, j;
   static short name=1;
   struct sl3 {short s1; short s2; short s3;} *cp;
/*   struct fl3 {float f1; float f2; float f3;} *vp; */
   VERT *vp;

   i = 0;
   switch( req->render_mode[0] )
   {
     case WIRE_FRAME: polymode(PYM_LINE); 
       break;

     case SOLID: polymode(PYM_FILL);
       break;
	
     case POINTS: polymode(PYM_POINT);
       break;

     default: polymode(PYM_LINE);

   }
   tm = clock();
   vp = verts;
   cpack(0xffffff);
/*   for( i = 0; i < 2*NL - 2; i++ )
   { */
     bgntmesh(); 
       for( j = 0; j < NP*(2*NL-2); j++, vp++ )      
       { v3f( vp ); }
     endtmesh();
/*   } */
   tm = clock() / 1000000.0 - tm;
   printf("Number of Triangles/sec = %f\n",(NP*(NL-1))/tm);
}


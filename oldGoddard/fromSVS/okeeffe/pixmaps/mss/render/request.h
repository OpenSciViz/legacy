
#include "psio.h"
#include "ps_icon_wind.h"

typedef struct request { long rot_x; long rot_y; long rot_z; 
			short view_pnt;
			short render_mode[2];
			short pick_mode;
			short N_spheres;
			short show_worldmap;
			short radial_distort;
			short elevation;
			short show_orbit;
			short show_footprint;
			int zoom_fctr;
			int twist;
			int pan_ew;
			int pan_ns;
			float dist;
			float alt;
			float zoom_val;
			float x_field;
			float y_field;
			char itemlabel[80];
			char notice[80];
			} REQ;

#define WIRE_FRAME 0
#define SOLID 1
#define POINTS 2

#define EXTERIOR 0
#define BETWEEN 1
#define CENTRAL 2

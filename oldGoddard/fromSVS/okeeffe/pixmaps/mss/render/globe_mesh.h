#include <stdio.h>
#include <gl.h>
#include <device.h>
#include <math.h>

#ifdef MAIN
#define global
#else
#define global extern
#endif

#define PI 3.1415926
#define NP 960
#define NL 379

typedef struct { float x; float y; float z; } VERT;

global int NX;
global int NY;
global Object view_obj;
global VERT *verts;

#include "globe_mesh.h"
#include "request.h"
#include "psio.h"
#include "icon_wind.h"

int get_request(req)
REQ *req;
{
  int tag, itemval;

    if( psio_eof(PostScriptInput) ) 
    {
	ps_close_PostScript();
	gexit();
    }
    else
    {
	if( tag = ps_notify(req->itemlabel,&itemval) )
	{
#ifdef DEBUG
	  printf("tag= %d\n",tag);
	  printf("req->itemlabel= %s\n",req->itemlabel);
	  printf("itemval= %d\n",itemval);
#endif
	}
	else
        {
	  printf("funny tag returned= %d\n",tag);
	  exit(1);
	}
    }
/* process the request */
/* pitch the globe */
    if( strncmp(req->itemlabel,"X Axis",6) == 0 )
        req->rot_x = 10 * itemval;
/* spin the globe on its axis of ratation */
    else if( strncmp(req->itemlabel,"Z Ax",4) == 0 )
	req->rot_z = 10 * itemval;
    else if( strncmp(req->itemlabel,"PAN E",5) == 0 )
        req->pan_ew = itemval;
    else if( strncmp(req->itemlabel,"PAN N",5) == 0 )
        req->pan_ns = itemval;
/* twist (view axis rotation? */
    else if( strncmp(req->itemlabel,"TWIST",5) == 0 )
	req->twist = 10 * itemval;
/* dist */
    else if( strncmp(req->itemlabel,"DIST",4) == 0 )
	req->dist = itemval;
/* altitude */
    else if( strncmp(req->itemlabel,"ALT",3) == 0 )
	req->alt = itemval;
/* view point, outside, inside, or central? */
    else if( strncmp(req->itemlabel,"View",4) == 0 )
    {
	if( itemval > CENTRAL ) 
	  req->view_pnt = BETWEEN;
	else
	  req->view_pnt = itemval;
    }
/* inner sphere: wire frame or solid or points? */
    else if( strncmp(req->itemlabel,"Globe: W",8) == 0 )
    {
	if( itemval > POINTS ) 
	  req->render_mode[0] = SOLID;
	else
	  req->render_mode[0] = itemval;
    }
/* outer sphere: wire frame or solid or points? */
    else if( strncmp(req->itemlabel,"Data:",5) == 0 )
    {
	if( itemval > POINTS ) 
	  req->render_mode[1] = SOLID;
	else
	  req->render_mode[1] = itemval;
    }
/* scale the zoom factor x1, x10, x100 */
    else if( strncmp(req->itemlabel,"Zoom",4) == 0 )
    {
	if( itemval == 0 ) req->zoom_fctr = 1;
	if( itemval == 1 ) req->zoom_fctr = 10;
	if( itemval == 3 ) req->zoom_fctr = 10;
	if( itemval == 2 ) req->zoom_fctr = 100;
    }
/* continuous zoom factor is between 1 and 100, scaled by above */
    else if( strncmp(req->itemlabel,"ZOOM",4) == 0 )
	req->zoom_val = itemval;
    else if( strncmp(req->itemlabel,"Show F",6) == 0 )
    {
	if( itemval == 0 ) req->show_footprint = FALSE;
	if( itemval == 1 ) req->show_footprint = TRUE;
    }
    else if( strncmp(req->itemlabel,"ELEV",4) == 0 )
	req->elevation = itemval;

    else if( strncmp(req->itemlabel,"Show O",6) == 0 )
    {
	if( itemval == 0 ) req->show_orbit = FALSE;
	if( itemval == 1 ) req->show_orbit = TRUE;
    }
    else if( strncmp(req->itemlabel,"Acti",4) == 0 )
    {
	if( itemval == 0 ) req->radial_distort = FALSE;
	if( itemval == 1 ) req->radial_distort = TRUE;
    }
    else if( strncmp(req->itemlabel,"Show W",6) == 0 )
    {
	if( itemval == 0 ) req->show_worldmap = FALSE;
	if( itemval == 1 ) req->show_worldmap = TRUE;
    }
    else if( strncmp(req->itemlabel,"Two ",4) == 0 )
    {
	if( itemval == 0 ) req->N_spheres = 1;
	if( itemval == 1 ) req->N_spheres = 2;
    }
    else if( strncmp(req->itemlabel,"Pick",4) == 0 )
    {
	if( itemval == 0 ) req->pick_mode = FALSE;
	if( itemval == 1 ) req->pick_mode = TRUE;
    }

    return;
}















#include "globe_mesh.h"
#include "request.h"

int draw_globe(win_id,req)
int win_id;
REQ *req;
{
  int i, fov;
 
  winset(win_id);
  view_trnsfrm(req);

  tri_mesh(req);

  return;
}


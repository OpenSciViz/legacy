/* there are [380][960] elements in the input  */
#include <stdio.h>
#include <math.h>
#include <malloc.h>
#include "nrutil.h"
float **ya, **y2a;
float m_lat[380], m_lon[960];

setup_interp(mss)
float **mss;
{
  float del_lon=360.0/960.0/57.3, del_lat=142.0/380.0/57.3;
  int i,j;

  ya = convert_matrix(mss,1,380,1,960);
  y2a = matrix(1,380,1,960);

  m_lat[0] = 70.0/57.3;
  for( i = 1; i < 380; i++ )
    m_lat[i] = m_lat[i-1] - del_lat;

  m_lon[0] = -180.0/57.3;
  for( i = 1; i < 960; i++ )
    m_lon[i] = m_lon[i-1] + del_lon;

  splie2(m_lat-1,m_lon-1,ya,380,960,y2a);

  return;
}

float interp(lat,lon)
float lat, lon;
{
  float val;
      
  splin2(m_lat-1,m_lon-1,ya,y2a,380,960,lat,lon,&val);
  if( val > 255.0 ) val = 255.0;
  if( val < 0.0 ) val = 0.0;
  return( val );

}

main()
{
  int i, j, fd, nb;
  char *outfile="mss_globe.8bit";/* save the result */
  unsigned char globe[512*1024], bmss[380][960];
  float mss[380][960];
  float lat, lon;

  fd = open("/usr/people/xrhon/pixmaps/globe.pixmap",0);
  nb = read(fd,globe,1024*512);   
  close(fd);
  fd = open("avg3x3.8bit",0);
  nb = read(fd,bmss,960*380);
  close(fd);
  for( i = 0; i < 380; i++ )
    for( j = 0; j < 960; j++ )
      mss[i][j] = bmss[i][j];

  setup_interp(mss);

  lat = 90.0/57.3 + 180.0/512.0/57.3;
  for( i = 0; i < 512; i++ )
  {
    lat = lat - 180.0/512.0/57.3;
    lon = -180.0/57.3 - 360.0/1024.0/57.3;
    for( j = 0; j < 1024; j++ )
    {
      lon = lon + 360.0/1024.0/57.3;
      if( globe[i*1024 + j] >= 127 ) 
	globe[i*1024 + j] = 255;
      if( lat <= 70.0/57.3 && lat >= -72.0/57.3 )
        if( globe[i*1024 + j] == 0 ) globe[i*1024 + j] = (char) interp(lat,lon);
     }
  }
  fd = creat(outfile,0644);
  write(fd,globe,1024*512);
  close(fd);

  free_convert_matrix(ya,1,380,1,960);
  free_matrix(y2a,1,380,1,960);
}

main(argc,argv,env)
int argc;
char **argv, **env;
{
  int i, nb, fd;
  int pix, nn, neighb[8];
  unsigned char inbuff[6144], outbuff[6144];
  static unsigned short res=7, skyavg[6144];
  char outfile[80];

  if( argc > 1 )
  {
    strcpy(outfile,argv[1]);
    strcat(outfile,".smooth");
    if( (fd = open(argv[1],0)) <= 0 )
    { 
      printf("unable to open: %s\n",argv[1]);
      exit(0);
    }
  }
  else
  {
    printf("need a DMR resolution quadcell sorted file NAME!\n");
    exit(0);
  }

  if( (nb = read(fd,inbuff,6144)) < 6144 ) printf("hey, i only found %d values!\n",nb);  
  close(fd);

  for( pix = 0; pix < nb; pix++ )
  {
    skyavg[pix] = inbuff[pix];
/*     upx_8_neighbors_(&pix,&res,neighb,&nn); fortran module */
/*    upx_8_neighbors(&pix,&res,neighb,&nn);   */
    upx_4_neighbors_(&pix,&res,neighb); 
    nn = 4;
    for( i = nn-1; i >= 0; i-- )
    {
      if( neighb[i] < 6144 )
        skyavg[pix] = skyavg[pix] + inbuff[neighb[i]];
      else
        nn--;
    }
    skyavg[pix] = skyavg[pix] / (nn+1);
    outbuff[pix] = skyavg[pix];
  }

  fd = creat(outfile,0644);
  write(fd,outbuff,6144);
}

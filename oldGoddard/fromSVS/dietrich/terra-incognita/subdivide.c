#include <stdio.h>
#include <math.h>
#define NX 512
#define NY 512
#define NCOEF 2

float elev[NXxNY], coef[NCOEFxNCOEF];

main(int argc, char **argv, char **env)
{
/* later we must change the coef to be defined by the
   autocorrlation function at each scale and use
   a gaussian random number generator with an appropriate
   sigma */
  coef[0] = coef[1] = coef[2] = coef[3] = 0.25;

  elev[NY/2*NX + NX/2] = rand() / pow( 2.0, 15.0 ) - 0.5;
  elev[NY/2*NX + NX/2] = rand() / pow( 2.0, 15.0 ) - 0.5;
  elev[NY/2*NX + NX/2] = rand() / pow( 2.0, 15.0 ) - 0.5;
  elev[NY/2*NX + NX/2] = rand() / pow( 2.0, 15.0 ) - 0.5;

  elev[NY/2*NX + NX/2] = rand() / pow( 2.0, 15.0 ) - 0.5 +
			 coef[0] * elev[0] +
			 coef[1] * elev[NX - 1] +
			 coef[2] * elev[NY*NX - NX] +
			 coef[3] * elev[NY*NX - 1];

  quadrant_1(0,elev[NY/2*NX + NX/2]);
  quadrant_2(0,elev[NY/2*NX + NX/2]);
  quadrant_3(0,elev[NY/2*NX + NX/2]);
  quadrant_4(0,elev[NY/2*NX + NX/2]);


/* if the above four recursive functions complete sans error,
   our elevation array is now fully defined and we can render
   it */

  render(NX,NY,elev);
}

render( int nx, int ny, float *elev )
{ 
/* first find the min and max values to get an idea of the
   scale, and choose an appropriate horizontal regeme */
 float min=99999999.0, max= -99999999.0, scale= 0.25, del_xy;
 float verts[NX*NY][3];
 int i, j;

  for( i = 0; i < NX; i++ )
    for( j = 0; j < NY; j++ )
    {
      if( min > elev[j*NX + i] ) min = elev[j][i];
      if( max < elev[j*NX + i] ) max = elev[j][i];
    }
      
  del_xy = (max - min) / NX / scale;
  for( i = 0; i < NX; i++ )
  {
    verts[j*NX + i][0] = i*del_xy;
    for( j = 0; j < NY; j++ )
    {
      verts[j*NX + i][1] = i*del_xy;
      verts[j*NX + i][2] = elev[j*NX + i][2];
    }
  }

  draw_verts(nx,ny,verts);
}


  
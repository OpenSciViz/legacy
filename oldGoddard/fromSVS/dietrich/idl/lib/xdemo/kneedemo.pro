;------------------------------------------------------------------------------
;   procedure kneedemo_event
;------------------------------------------------------------------------------

	;This routine is used to process the events being dispatched
	;by the Xmanager.  It uses the uservalue of the widget where
	;the event occured to select the appropriate action.  When 
	;the slider is selected and its value has changed, the xray's
	;indicator must be redrawn as well as the slice.  When displaying
	;the help file, the main routine widget ID which is the base
	;widgets id.  By passing this to the display routine, the
	;routine can then register itself as having the kneedemo
	;as its leader so the help will go away when the kneedemo 
	;dies.

PRO kneedemo_event, event

COMMON KNEECOM, slice, slices, $		;the common block is for the 
		right, left, $
		fullimage, top, indpos, $
		inuseflag

WIDGET_CONTROL, GET_UVALUE = retval, event.id	;use the uservalue to find the
						;event
CASE retval OF
  "SLICESLIDE" : $				;if the slider was hit, redraw
	BEGIN					;the xray where the old 
	   WIDGET_CONTROL, event.id, $		;indicator was and then 
			GET_VALUE = newslice	;redraw the new cross section
	   IF (newslice NE slice) THEN BEGIN	;and the new indicator
	     slice = newslice
	     WSET, right
	     TV, slices(slice)
	     WSET, left
	     TV, fullimage(*,indpos), $
		 0, indpos
	     indpos = ((slice + 0.5)/35.0)*256.0
	     PLOTS, [0, 255],[indpos, indpos], $
		    COLOR=top, /DEVICE
	   ENDIF
	END

  "KNEEHELP" : XDisplayFile, filepath("kneedemo.txt", SUBDIR = "help"), $
		TITLE = "Knee Xray Demo Help", $;call the xlib routine that 
		GROUP = event.top, $		;displays a files contents
		WIDTH = 55, $			;In this case it is a help file
		HEIGHT = 16

  "KNEECADJ" : xloadct, $			;call the xlib routine to 
		/SILENT, $			;adjust the color table
		GROUP = event.top

  "KNEEEXIT" : BEGIN
	WIDGET_CONTROL, $		;done button was pressed so 
		event.top, $			;destroy the widget
		/DESTROY
	a = fstat(slices)
	free_lun, a.unit
	ENDCASE

  ELSE: MESSAGE, "Received a bad event <" + retval + ">"

ENDCASE

END ;====================== procedure kneedemo_event =========================



;------------------------------------------------------------------------------
;   procedure kneedemo
;------------------------------------------------------------------------------

	;This example IDL procedure draws an Xray of the knee and 
	;lets the user then choose where to view a slice of data taken
	;from that knee.  It uses Widgets to create a window, drawing 
	;areas and a slider for slice selection.  The basic action is
	;to wait for the user to choose a slice and then redraw both
	;the slice indicator and the actual slice once a new selection
	;has been made.  This involves switching back and forth from
	;the two draw areas in the kneebase widget using the idl call
	;wset.

PRO kneedemo, GROUP = GROUP

COMMON KNEECOM, slice, slices, right, left, fullimage, top, indpos
COMMON colors, r_orig, g_orig, b_orig, r_curr, g_curr, b_curr

IF(XRegistered("kneedemo") NE 0) THEN GOTO, FINISH

IF N_ELEMENTS(r_orig) LE 0 THEN loadct, 0	;1st color tbl?
top = !D.N_COLORS - 1				;Last color

XGetData, slices, $
		FILENAME = "knee_slices.dat", $
		DIMENSIONS = dims, /ASSOC_IT

slice = dims(2) / 2				;set the first slice to be the
						;middle of the X-ray

kneebase = WIDGET_BASE( $
		TITLE = "Knee X-ray Example", $
		/COLUMN, $
		XPAD = 20, $
		YPAD = 20, $
		SPACE = 20)

XPdMenu, [	'"Exit Knee X-ray Example"	 	KNEEEXIT', $
		'"Adjust Color Table"			KNEECADJ', $
		'"Help"					KNEEHELP'], $
	 kneebase

dispbase = WIDGET_BASE(kneebase, $		;there is a row of displays
		/ROW)				;across the bottom

leftbase = WIDGET_BASE(dispbase, $		;on the left is a column for 
		/COLUMN, $			;the first display
		/FRAME)

leftlabel = WIDGET_LABEL(leftbase, $		;this labels the first display
		VALUE = "Cross Section")

kneeslice = WIDGET_DRAW(leftbase, $		;the slice draw widget is where
		XSIZE = 256, $			;the slices are drawn.  Its IDL
		YSIZE = 256, $			;window is found and recorded 
		RETAIN = 2)			;below

kneeslide = WIDGET_SLIDER(dispbase, $		;in declaring the slider, the
		YSIZE = 256, $			;drag keyword is set to ask
		MAXIMUM = dims(2) - 1, $	;for events whenever the slider
		MINIMUM = 0, $			;moves whether or not the user
		VALUE = slice, /VERTICAL, $	;lets go of the slider
		UVALUE = "SLICESLIDE", $
		/DRAG, $
		/SUPPRESS_VALUE)

rightbase = WIDGET_BASE(dispbase, $		;on the right hand side of the
		/COLUMN, $			;display row is a column for 
		/FRAME)				;the rightmost display

leftlabel = WIDGET_LABEL(rightbase, $		;the label for the right
		VALUE = "Xray Image")		;display

kneexray = WIDGET_DRAW(rightbase, $		;the X-ray draw widget's IDL
		XSIZE = 256, $			;window is found below as well
		YSIZE = 256, $
		RETAIN = 2)

WIDGET_CONTROL, kneebase, /REALIZE		;register all the widget things
						;with Xwindows

WIDGET_CONTROL, kneexray, GET_VALUE = left	;get the window ids of both 
WIDGET_CONTROL, kneeslice, GET_VALUE = right	;drawing window to allow 
						;switching back and forth

WSET, left					;set the drawing to the X-ray

XGetData, fullimage, $
		FILENAME = "knee_xray.dat"
TV, fullimage					;display the X-ray

indpos = ((slice + 0.5)/32.0) * 256.0		;calculate where the indicator
PLOTS, [0, 255], $				;lies on the X-ray and 
	[indpos, indpos], $			;draw the indicator on the 
	COLOR=top, /DEVICE			;xray

WSET, right					;set the drawing to the slice
TV, slices(slice)				;window and draw the slice

XManager, "kneedemo", kneebase, $		;register the kneedemo with
	      GROUP_LEADER = GROUP		;the Xmanager and pass group on

FINISH:

END ;========================= procedure kneedemo =============================







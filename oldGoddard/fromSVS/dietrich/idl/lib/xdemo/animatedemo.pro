;+
; NAME:
;	ANIMATEDEMO
; PURPOSE:
;	Demonstrates the use of the XInteractive animation library procedure
;	and the XGetData procedure for selection of data.
; CATEGORY:
;	Widgets, Demo
; CALLING SEQUENCE:
;	ANIMATEDEMO
; KEYWORD PARAMETERS:
;	GROUP = The widget ID of the widget that leads the animatedemo.  If the
;		leader is destroyed, the XManager will also destroy the 
;		animatedemo.
; SIDE EFFECTS:
;	Initiates the XManager if it is not already invoked.
; RESTRICTIONS:
;	Can only have one instance running at one time.
; PROCEDURE:
;	Get the data to be displayed and then display it using the 
;	XInterAnimate function
; MODIFICATION HISTORY: Written by Steve Richards,  December, 1990
;-

PRO animatedemo, GROUP = GROUP

IF(XRegistered("xinteranimate") EQ 0) THEN BEGIN 
  XGetData, frames, DESCRIPTION = description, $
		DIMENSIONS = dimens, $
		/THREE_DIM, $
		TITLE = "Please Select Animation Data", /ASSOC_IT

  s = SIZE(frames)
  if s(0) gt 1 then begin
    pictsize = dimens(0) * dimens(1)
    fact = (1000000 / (pictsize * dimens(2))) > 1 < 5	;Zoom factor ~ 1MB

    XInteranimate, SET = [dimens(0) * fact, dimens(1) * fact, dimens(2)], $
		   TITLE = description, /SHOWLOAD
    IF fact ne 1 then FOR index = 0, dimens(2)-1 DO $
        XInteranimate, image = REBIN(frames(index), $
		dimens(0) * fact, dimens(1) * fact), frame = index $
    ELSE FOR index = 0, dimens(2)-1 DO $
          XInteranimate, image = frames(index), frame = index

    junk = fstat(frames)
    free_lun, junk.unit
    XInteranimate, GROUP = GROUP
  ENDIF
ENDIF

END






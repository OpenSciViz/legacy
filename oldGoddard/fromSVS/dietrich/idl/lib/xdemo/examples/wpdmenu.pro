; This is the code for a simple pull-down menu.
; In this example, the pull-down menu appears as two buttons
; labeled "Colors" and "Quit".  If "Colors" is selected, a 
; pull-down menu appears.  Selecting a color causes its name to
; be printed in the IDL window.  If "Quit" is selected, the
; widget is destroyed.


PRO wpdmenu_event, event
; This procedure is the event handler for a simple pull-down menu. 

; Use WIDGET_CONTROL to get the VALUE of any action and put it into 'value':

WIDGET_CONTROL, event.id, GET_VALUE = value

; If Quit is pressed, destroy all widgets:

IF (value EQ 'Quit') THEN WIDGET_CONTROL, event.top, /DESTROY

; For Menu items, any VALUE returned will be the text of the menu item.
; Therefore, we can just print out the words in the IDL window as they
; are selected:

PRINT, value, ' selected.'

END



PRO wpdmenu, GROUP = GROUP

; This COMMON block is not really necessary if we only have a menu:

COMMON wpdmenublock, hi

; Make the top-level base widget:

base = WIDGET_BASE(TITLE = 'Pull-Down Menu Example', /COLUMN, XSIZE = 300)

; Make 'items' a 1-dimensional text array containing the description for
; the menu items. Items are in two sets of quotes, because that's the way
; XPDMENU likes it (see XPDMENU's documentation for details):

items = ['"Colors" {','"Red" {','"Candy Apple"','"Medium"','"Dark"','}', $
	'"Orange"','"Yellow"','}','"Quit"']

; The XMENU procedure will automatically create the menu items for us.
; We only have to give it the menu item labels (in the array 'items')
; and, optionally, the base to which the menu belongs (here we use 'base',
; the top-level base widget):

XPDMENU, items, base


; Realize the widgets:
WIDGET_CONTROL, base, /REALIZE

; Hand off to the XMANAGER:
XMANAGER, 'wpdmenu', base, GROUP_LEADER = GROUP

END

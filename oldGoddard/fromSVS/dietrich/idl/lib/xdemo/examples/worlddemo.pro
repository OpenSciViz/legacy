PRO WORLDDEMO_EVENT, EVENT
;THIS IS THE WORLDROT EVENT HANDLER

;COMMON BLOCK
;

common world_block,projection,minlon,maxlon,minlat,maxlat,viewlat,width,$
                   height,numframes,cna,dne,theproj,base,names,help,$
		   rotangle,message,adjust,display,concol,gridcol,$
		   drawcon,nocon,drawgrid,nogrid,im,latsamp,lonsamp,$
		   bilinear,nointerp,grid,con,bilin, imsize

; This IF THEN ELSE handles clicking on the exclusive menu of projections.
; The variable 'projection' was set with the 'buttons' keyword when the 
; exclusive menu of projections was created.
; The WHERE expression sets the value of 'projflag' to 1 if one of the 
; exclusive buttons is clicked on.  The variable 'projselection' will also
; hold the index of the button touched.

projselection = WHERE(projection EQ event.id, projflag)

IF (projflag NE 0) THEN BEGIN
  theproj = projselection(0)
 
ENDIF ELSE BEGIN

;If something other than a exclusive button has been touched, manage the event
;with this CASE statement.

WIDGET_CONTROL, event.id, GET_UVALUE = eventval 
CASE eventval OF
	"CREATE":BEGIN

		;If Xinteranimate is already going, DON'T DO THIS:
		IF (XREGISTERED("XInterAnimate") NE 0) THEN RETURN

		;If the Create New Animation Button is pressed, 
		;read the values of all of the widgets from the tool.
		;Projection number is already in THEPROJ.

		;Read the Info about Image fields:
		lon1 = -180
                lon2 = 180
                lat1 = -90
                lat2 = 90

		;Read the Latitude to be Centered slider:
                WIDGET_CONTROL, viewlat, GET_VALUE   = vlat
	
		;The Rotation of North slider:
		WIDGET_CONTROL, rotangle,GET_VALUE   = rot

		;The size of the animation window:
                WIDGET_CONTROL, width, GET_VALUE     = wide
                WIDGET_CONTROL, height, GET_VALUE    = high
		
		;The number of frames to be generated:
                WIDGET_CONTROL, numframes, GET_VALUE = nframes

		;The grid and continent colors:
		concolor  = !D.N_COLORS-1
		gridcolor = !D.N_COLORS-1

		;The Latitude and Longitude sampling parameters:
		lts = 30 < imsize(1)
		lns = 30 < imsize(0)
		
		;These numbers get returned as arrays. Turn them into scalars:
		nframes = nframes(0)
		high = high(0)
		wide = wide(0)
		lon1=lon1(0)
		lon2=lon2(0)
		lat1=lat1(0)
		lat2=lat2(0)
		vlat=vlat(0)
		rot=rot(0)


		;Make the Worldrot Widget insensitive.
		WIDGET_CONTROL, base, SENSITIVE=0

		;Convert THEPROJ to correct argument to proj keyword.
		;CONVECT maps the projection menu into correct proj keyvalue:
		convect = [6,8,5,4,9,10,2,14,1]
		p = convect(theproj)

		;Make the title for XinterAnimate window:
		title = 'Rotating ' + names(theproj) + ' Projection'

		;Make Animation Frames:
        	XINTERANIMATE, SET = [wide, high, nframes], $
				     TITLE=title, GROUP = event.top, $
				     /SHOWLOAD
		step = 360/nframes
	
        	FOR i=0,nframes-1 DO BEGIN
			;Make the 'Frame n of x created.' message:
                	mess = 	'Frame '+ STRING(i+1, format='(I0)') + $
				' of ' + STRING(nframes, FORMAT='(I0)')+' created.'

			;Set up the map projection:
			MAP_SET, vlat, i*step-180, rot, PROJ=p
			im1 = BYTSCL(im)

			;Warp the image and return it as 'img':
			img = MAP_IMAGE(im1, startx, starty, LATSAMP=lts, $
					LONSAMP=lns, LONMIN=lon1, LONMAX=lon2,$
					LATMIN=lat1, LATMAX=lat2, $
					BILINEAR = bilin)

			;Display the image on the map:
			TV, img, startx, starty

			;Draw the continents and gridlines if we're supposed to:
			IF (grid EQ 1) THEN BEGIN
				DEVICE, SET_GRAPHICS_FUNCTION=6 ;XOR
				MAP_GRID, COLOR = gridcolor
				DEVICE, SET_GRAPHICS_FUNCTION=3 ;NORMAL
			ENDIF

			IF (con EQ 1) THEN BEGIN
				DEVICE, SET_GRAPHICS_FUNCTION=6 ;XOR
			 	MAP_CONTINENTS, COLOR = concolor
				DEVICE, SET_GRAPHICS_FUNCTION=3 ;NORMAL
			ENDIF

			;Put the message in the message window:
			WIDGET_CONTROL, message, SET_VALUE = mess

			;Put the new frame into the Xinteranimate[B tool:
                	XINTERANIMATE, FRAME = i, WINDOW = !D.WINDOW

        		END

			
		;Call the Animation Tool & display new message
		WIDGET_CONTROL, message, SET_VALUE=display
		XINTERANIMATE, 80, GROUP = event.top 
		
		;Resensitize the Worldrot Widget
 
		WIDGET_CONTROL, base, /SENSITIVE
		WIDGET_CONTROL, message, SET_VALUE = adjust
		
		END ;Create case

    "DRAWGRID": grid = 1	;Turn grid drawing ON.
      "NOGRID": grid = 0	;Turn grid drawing OFF.

     "DRAWCON": con = 1		;Turn continent drawing ON.
       "NOCON": con = 0		;Turn continent drawing OFF.

    "BILINEAR": bilin = 1	;Turn bilinear interpolation ON.
    "NOINTERP": bilin = 0	;Turn bilinear interpolation OFF.

	"HELP": BEGIN 
		;If HELP is pressed, display the help file.
		XDISPLAYFILE, FILEPATH('wordemohelp.txt', SUBDIR=['lib','xdemo','examples']), $
					GROUP = event.top, TITLE = 'World Rotation Demo Help'
		END ;Help case

	"FILE": BEGIN
		;If "Get a New Image" is pressed, use XGETDATA to retrieve the new image:
			oldim = im
			oldimsize = imsize
			XGETDATA, im, /TWO_DIM, $
			TITLE='Select an Image to Warp to the Map', $
			DIMENSIONS = imsize
			;If the "Cancel" button on XGETDATA is pressed, keep the old image:
			s = SIZE(im)
			IF s(0) EQ 0 THEN BEGIN
				im = oldim
				imsize = oldimsize
			ENDIF
		END

	"DONE": WIDGET_CONTROL, event.top, /DESTROY	;If 'Done' is pressed,
							;destroy all widgets
							;and return to IDL.

	  ELSE: donothing=0	;If nothing is pressed, don't do anything.

	ENDCASE

ENDELSE ;(projflag NE 0)

END



; !!! MAKE THE ACTUAL WIDGETS !!!

PRO worlddemo, GROUP = GROUP


; COMMON BLOCK
COMMON world_block,projection,minlon,maxlon,minlat,maxlat,viewlat,width,$
                   height,numframes,cna,dne,theproj,base,names,help,$
		   rotangle,message,adjust,display,concol,gridcol,$
		   drawcon,nocon,drawgrid,nogrid,im,latsamp,lonsamp,$
		   bilinear,nointerp,con,grid,bilin, imsize

; Only one copy of WORLDROT can run at a time due to the COMMON block.
; Check for other copies and do nothing if WORLDROT is already running:

IF(XRegistered("worldrot") NE 0) THEN RETURN

; Read the inial data file to warp:

XGETDATA, im, /TWO_DIM, DIMENSIONS = imsize, TITLE='Select an Image to Warp to the Map'

; If XGETDATA's cancel button was hit, don't continue.  Otherwise, do it to it:
s = SIZE(im)
IF s(0) GT 0 THEN BEGIN

;MAIN WIDGET BASE
base = WIDGET_BASE(TITLE='World Rotation Demo', /ROW)

;WORLD ROTATION TOOL HAS 3 MAIN COLUMNS
lcol = WIDGET_BASE(base, /FRAME, /COLUMN)	;Left column.
mcol = WIDGET_BASE(base, /FRAME, /COLUMN)	;Middle column.
rcol = WIDGET_BASE(base, /FRAME, /COLUMN)	;Right column.

;LEFT COLUMN IS THE EXCLUSIVE MENU OF PROJECTION TYPES, BUTTONS, AND OPTIONS
lpad = WIDGET_BASE(lcol, /FRAME, /ROW)

;The SPIN IT button:

spin = 		[				$
		[000B, 000B, 000B, 000B, 000B, 000B, 000B, 000B, 000B, 000B],			$
		[000B, 000B, 000B, 000B, 000B, 000B, 000B, 000B, 000B, 000B],			$
		[000B, 000B, 000B, 000B, 000B, 000B, 000B, 000B, 000B, 000B],			$
		[000B, 000B, 000B, 000B, 000B, 000B, 000B, 000B, 000B, 000B],			$
		[000B, 000B, 000B, 000B, 000B, 000B, 000B, 000B, 000B, 000B],			$
		[000B, 000B, 000B, 000B, 000B, 000B, 000B, 000B, 000B, 000B],			$
		[000B, 000B, 000B, 000B, 000B, 000B, 000B, 000B, 000B, 000B],			$
		[000B, 000B, 000B, 000B, 000B, 000B, 000B, 000B, 000B, 000B],			$
		[000B, 000B, 000B, 000B, 000B, 000B, 000B, 048B, 000B, 000B],			$
		[000B, 000B, 000B, 000B, 000B, 000B, 000B, 240B, 000B, 000B],			$
		[000B, 000B, 000B, 000B, 000B, 000B, 000B, 240B, 003B, 000B],			$
		[153B, 057B, 207B, 231B, 231B, 207B, 127B, 254B, 015B, 000B],			$
		[153B, 057B, 207B, 231B, 231B, 207B, 127B, 254B, 063B, 000B],			$
		[153B, 057B, 207B, 231B, 231B, 207B, 127B, 254B, 255B, 000B],			$
		[153B, 057B, 207B, 231B, 231B, 207B, 127B, 254B, 255B, 003B],			$
		[153B, 057B, 207B, 231B, 231B, 207B, 127B, 254B, 255B, 015B],			$
		[153B, 057B, 207B, 231B, 231B, 207B, 127B, 254B, 255B, 031B],			$
		[153B, 057B, 207B, 231B, 231B, 207B, 127B, 254B, 255B, 063B],			$
		[153B, 057B, 207B, 231B, 231B, 207B, 127B, 254B, 255B, 063B],			$
		[153B, 057B, 207B, 231B, 231B, 207B, 127B, 254B, 255B, 015B],			$
		[153B, 057B, 207B, 231B, 231B, 207B, 127B, 254B, 255B, 003B],			$
		[153B, 057B, 207B, 231B, 231B, 207B, 127B, 254B, 255B, 000B],			$
		[153B, 057B, 207B, 231B, 231B, 207B, 127B, 254B, 063B, 000B],			$
		[153B, 057B, 207B, 231B, 231B, 207B, 127B, 254B, 031B, 000B],			$
		[153B, 057B, 207B, 231B, 231B, 207B, 127B, 254B, 007B, 000B],			$
		[000B, 000B, 000B, 000B, 000B, 000B, 000B, 240B, 001B, 000B],			$
		[000B, 000B, 000B, 000B, 000B, 000B, 000B, 112B, 000B, 000B],			$
		[252B, 249B, 099B, 134B, 129B, 249B, 007B, 048B, 000B, 000B],			$
		[252B, 249B, 103B, 142B, 129B, 249B, 007B, 000B, 000B, 000B],			$
		[006B, 024B, 102B, 142B, 129B, 193B, 000B, 000B, 000B, 000B],			$
		[006B, 024B, 102B, 158B, 129B, 193B, 000B, 000B, 000B, 000B],			$
		[006B, 024B, 102B, 150B, 129B, 193B, 000B, 000B, 000B, 000B],			$
		[124B, 248B, 099B, 182B, 129B, 193B, 000B, 000B, 000B, 000B],			$
		[248B, 248B, 099B, 166B, 129B, 193B, 000B, 000B, 000B, 000B],			$
		[128B, 025B, 096B, 230B, 129B, 193B, 000B, 000B, 000B, 000B],			$
		[128B, 025B, 096B, 198B, 129B, 193B, 000B, 000B, 000B, 000B],			$
		[128B, 025B, 096B, 198B, 129B, 193B, 000B, 000B, 000B, 000B],			$
		[254B, 024B, 096B, 134B, 129B, 193B, 000B, 000B, 000B, 000B],			$
		[254B, 024B, 096B, 134B, 129B, 193B, 000B, 000B, 000B, 000B],			$
		[000B, 000B, 000B, 000B, 000B, 000B, 000B, 000B, 000B, 000B]			$
		]



dne = WIDGET_BUTTON(lpad, VALUE = 'Done', UVALUE = 'DONE')

; Make the cool bitmap SPIN IT button if Motif, if Open Look, don't bother:

IF (!VERSION.OS EQ 'sunos') THEN $
cna = WIDGET_BUTTON(lpad, VALUE = 'Spin World', UVALUE = 'CREATE') $
ELSE $
cna = WIDGET_BUTTON(lpad, VALUE = spin, UVALUE = 'CREATE')
file = WIDGET_BUTTON(lpad, VALUE = 'Get a New Image', UVALUE = 'FILE')
help = WIDGET_BUTTON(lpad, VALUE = 'Help', UVALUE = 'HELP')


;The text for the exclusive projection buttons:
names = ['Azimuthal', 'Cylindrical', 'Gnomonic', 'Lambert', $
        'Mercator', 'Mollweide', 'Orthographic', 'Sinusoidal', $
        'Stereographic']

;Make the exclusive projection list. Turn release events off:
XMENU, names, lcol, /EXCLUSIVE, /FRAME, $
        TITLE = 'Map Projection', BUTTONS = projection, /NO_RELEASE

drawlabel = WIDGET_LABEL(lcol, VALUE = 'Drawing Options:')

;This is the two-button 'toggle' switch for continent drawing:
conbase = WIDGET_BASE(lcol, /COLUMN, /FRAME, /EXCLUSIVE)
drawcon = WIDGET_BUTTON(conbase, VALUE='Draw Continents', UVALUE='DRAWCON',$
			/NO_RELEASE)
nocon = WIDGET_BUTTON(conbase, VALUE='No Continents', UVALUE='NOCON',$
		      /NO_RELEASE)

;This is the two-button 'toggle' switch for grid drawing:
gridbase = WIDGET_BASE(lcol, /COLUMN, /FRAME, /EXCLUSIVE)
drawgrid = WIDGET_BUTTON(gridbase, VALUE='Draw Grid', UVALUE='DRAWGRID',$
			 /NO_RELEASE)
nogrid = WIDGET_BUTTON(gridbase, VALUE='No Grid', UVALUE='NOGRID',$
		       /NO_RELEASE)

;This is the two-button 'toggle' switch for bilinear interpolation:
bilinbase = WIDGET_BASE(lcol, /COLUMN, /FRAME, /EXCLUSIVE)
bilinear = WIDGET_BUTTON(bilinbase, VALUE='Bilinear Interpolation', $
			 UVALUE='BILINEAR', /NO_RELEASE)
nointerp = WIDGET_BUTTON(bilinbase, VALUE='No Interpolation', $
			 UVALUE='NOINTERP', /NO_RELEASE)


;MIDDLE COLUMN HAS LOTS OF STUFF

viewlabel = WIDGET_LABEL(mcol, VALUE = 'Viewing Options:')

mcol2 = WIDGET_BASE(mcol, /FRAME, /COLUMN)

viewlat = WIDGET_SLIDER(mcol2, TITLE = 'Latitude to be Centered', $
                        MINIMUM = -90, MAXIMUM = 90, VALUE = 0, $
			UVALUE = 'latslider')

;The 'Rotation of North' slider:
rotangle = WIDGET_SLIDER(mcol2, TITLE = 'Rotation of North', $
			 MINIMUM = -180, MAXIMUM = 180, VALUE = 0, $
			 UVALUE = 'rotslider', XSIZE = 255)



;RIGHT COLUMN HAS SLIDER AND BUTTONS

;The 'Animation Window Size' fields:
winbase =  WIDGET_BASE(rcol, /COLUMN, /FRAME)
wintitle = WIDGET_LABEL(winbase, VALUE='Animation Window Size:')
w5 = WIDGET_BASE(winbase, /ROW)

w15 = WIDGET_LABEL(w5, VALUE = 'Width')
width = WIDGET_TEXT(w5, XSIZE=3, YSIZE=1, /EDITABLE, VALUE='400', $
                    UVALUE = 'setwidth')

w35 = WIDGET_LABEL(w5, VALUE = 'Height')
height = WIDGET_TEXT(w5, XSIZE=3, YSIZE=1, /EDITABLE, VALUE='400', $
                     UVALUE = 'setheight')

;The 'Number of Frames' slider:
numframes = WIDGET_SLIDER(rcol, TITLE = 'Number of Frames', $
                          MAXIMUM=36, MINIMUM=2,VALUE=16, /FRAME, $
		 	  UVALUE = 'frameslider')

;The 'Messages' window:
instruct = WIDGET_LABEL(rcol, VALUE='Messages:')
message = WIDGET_TEXT(rcol, XSIZE=28, YSIZE=2, UVALUE='createtext', FRAME=2)

;Create some default messages:
adjust=['Adjust parameters and/or','select the SPIN button.']
display=['Displaying Animation Tool']

;REALIZE THE WIDGETS:
WIDGET_CONTROL, base, /REALIZE

;Set the default to be ORTHOGRAPHIC
theproj = 6
WIDGET_CONTROL, projection(theproj), /SET_BUTTON

;Put the 'Adjust values...' message in the message window:
WIDGET_CONTROL, message, SET_VALUE = adjust

;Set Grid drawing to ON:
WIDGET_CONTROL, drawgrid, /SET_BUTTON
grid = 1

;Set Continent drawing to ON:
WIDGET_CONTROL, drawcon, /SET_BUTTON
con = 1

;Set Interpolation to OFF:
WIDGET_CONTROL, nointerp, /SET_BUTTON
bilin = 0

;HAND THINGS OFF TO THE X MANAGER:
XMANAGER, 'worlddemo', base, GROUP_LEADER = GROUP

ENDIF
END


































































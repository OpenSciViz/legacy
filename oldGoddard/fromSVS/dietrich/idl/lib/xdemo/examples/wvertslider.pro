PRO wvertical_event, event
; This is the event handler for a basic vertical slider.

; The COMMON block is used because both wvertslider and wvertslider_event must
; know about all widgets that will generate values:
COMMON widgetblock, slider

; Use WIDGET_CONTROL to get the user value of any widget touched and put
; that value into 'eventval':
WIDGET_CONTROL, event.id, GET_UVALUE = eventval

; The movement of sliders is easily handled with a CASE statement.
; When the slider is moved, the value of 'eventval' becomes 'SLIDE':

CASE eventval OF
	'SLIDE':BEGIN
		
		; Get the current value of the slider and put it in the
		; variable 's':
		WIDGET_CONTROL, slider, GET_VALUE = s
		
		; Print the slider value to the IDL window:
		PRINT, s
		END
ENDCASE
END



PRO wvertical
; This is the procedure that creates a single slider.

; The COMMON block is used because both wslider and wslider_event must
; know about all widgets that will generate values:
COMMON widgetblock, slider

; A top-level base widget with the title "Vertical Slider Example" will
; hold the slider:
base = WIDGET_BASE(TITLE = 'Vertical Slider Example', /COLUMN)

; Often, a slider's minimum and maximum values are defined by an expression:
minvalue = 0
maxvalue = 100

; A widget called 'slider' is created. It has the title 'Slider Widget', 
; a frame around it, and a user value of 'SLIDE':
slider = WIDGET_SLIDER(base, $
			MINIMUM = minvalue, $		;The minimum value.
			MAXIMUM = maxvalue, $		;The maximum value.
		        TITLE = 'Slider Widget', $	;The slider title.
			/FRAME, $			;Put a frame around the slider.
		  	UVALUE = 'SLIDE',$		;Set the User Value to 'SLIDE'.
			/VERTICAL, $			;Make the slider vertical.
			XSIZE = 300) 

; Realize the widgets:
WIDGET_CONTROL, base, /REALIZE

; Hand off control of the widget to the XMANAGER:
XMANAGER, "wvertical", base

END
; This is the code for a widget that performs a background task.
; Background tasks are performed while events are not being processed.
; Whenever the event loop is idle and waiting for new input, the
; background routines that are registered are called.  For example, 
; things like animations can run in the background without forcing
; other widgets to be inactive.

; Here, a text window appears and the current system time is displayed 
; repeatedly.  Select the "Done" button to exit.




PRO wback_bck, topid

;This routine does the background action when the XManager calls it because
;the event loop is idle.

; The COMMON block is used because the event handler and the background
; process both need the widget id of the text widget:

COMMON wbackblock, text1

; This is the task that the widget performs:

temp_string = SYSTIME(0)
WIDGET_CONTROL, text1, SET_VALUE=temp_string, /APPEND

END


PRO wback_event, event
; This is the event handler for a background task widget.

; The COMMON block is used because the event handler and the background
; process both need the widget id of the text widget:

COMMON wbackblock, text1

; If a widget has been selected, put its User Value into 'eventval':

WIDGET_CONTROL, event.id, GET_UVALUE = eventval

; Perform actions based on the user value of the event:

CASE eventval OF

   'DONE' : WIDGET_CONTROL, event.top, /DESTROY

   'ERASE': WIDGET_CONTROL, text1, SET_VALUE = ''

ENDCASE

END



PRO wback, GROUP=GROUP

; This is the procedure that creates a widget which has a background
; process.

; The COMMON block is used because the event handler needs
; the widget id of the text widget:

COMMON wbackblock, text1

; A top-level base widget with the title "Background Process Widget Example"
; is created:

base = WIDGET_BASE(TITLE = 'Background Process Widget Example', $
	/COLUMN)

; Make the 'DONE' button:

button1 = WIDGET_BUTTON(base, $
		UVALUE = 'DONE', $
		VALUE = 'DONE')

; Make the text widget:

text1 = WIDGET_TEXT(base, $		; create a display only text widget
		XSIZE=30, $
		YSIZE=30, $
		/SCROLL)

; Make a button which will clear the text file.

button2 = WIDGET_BUTTON(base, $
		UVALUE = 'ERASE', $
		VALUE = 'ERASE')

; Realize the widgets:
WIDGET_CONTROL, base, /REALIZE

; Hand off control of the widget to the XMANAGER and
; register the background process:
XMANAGER, "wback", base, BACKGROUND='wback_bck', GROUP_LEADER=GROUP

END



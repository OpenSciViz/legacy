
; Copyright (c) 1991, Research Systems, Inc.  All rights reserved.
;	Unauthorized reproduction prohibited.
;+
; NAME:
;	WEXMASTER.PRO
; PURPOSE:
;	This routine is the main menu for the "Simple Widget Examples".
;	Click on any of the buttons in the menu to see helpful examples
;	of Widgets programming.
;
;	The examples under "Simple Widget Examples:" demonstrate the creation
;	and management of just one or two kinds of widgets at one time.
;	For more complex examples, select the examples under "Example Widget
;	Applications".
;
;	Select "About the Simple Widget Examples" to see a more information on
;	the simple widget examples.  Select "Widget Programming Tips and Tech-
;	niques" to see helpful information on programming with the IDL/widgets,
;	and widgets documentation updates.
;
;	Hope this helps!
; CATEGORY:
;	Widgets
; CALLING SEQUENCE:
;	WEXMASTER
; KEYWORD PARAMETERS:
;	GROUP = The widget ID of the widget that calls WEXMASTER.  When this
;		ID is specified, a death of the caller results in a death of
;		WEXMASTER.
; SIDE EFFECTS:
;	Initiates the XManager if it is not already running.
; RESTRICTIONS: Only one copy may run at a time to avoid confusion.
; PROCEDURE:
;	Create and register the widget and then exit.
; MODIFICATION HISTORY:
;	WEXMASTER and associated examples by Keith R Crosley, August, 1991
;	Created from a template written by: Steve Richards,	January, 1991
;-





;------------------------------------------------------------------------------
PRO wexmaster_event, event

WIDGET_CONTROL, event.id, GET_UVALUE = eventval		;find the user value
							;of the widget where
							;the event occured
; Set the width of the XDISPLAYFILE windows:
X=90

CASE eventval OF


  "XLOADCT": XLoadct, GROUP = event.top			;XLoadct is the library
							;routine that lets you
							;select and adjust the
							;color palette being
							;used.

  "XPALETTE": XPalette, GROUP = event.top		;XPalette is the
							;library routine that
							;lets you adjust 
							;individual color
							;values in the palette.

  "XMANTOOL": XManagerTool, GROUP = event.top		;XManTool is a library
							;routine that shows 
							;which widget
							;applications are 
							;currently registered
							;with the XManager as
							;well as which
							;background tasks.

  "SLIDER"	: WSLIDER, GROUP = event.top
  "SLIDERCODE"	: XDISPLAYFILE, FILEPATH('wslider.pro', SUBDIR=['lib','xdemo','examples']), $
					GROUP = event.top, WIDTH=X
  "VSLIDER"	: WVERTICAL, GROUP = event.top
  "VSLIDERCODE"	: XDISPLAYFILE, FILEPATH('wvertical.pro', SUBDIR=['lib','xdemo','examples']),GROUP = event.top, WIDTH=X
  "BUTTONS"	: WBUTTONS, GROUP = event.top
  "BUTTONSCODE"	: XDISPLAYFILE, FILEPATH('wbuttons.pro', SUBDIR=['lib','xdemo','examples']),GROUP = event.top, WIDTH=X
  "BITMAP"	: WBITMAP, GROUP = event.top
  "BITMAPCODE"	: XDISPLAYFILE, FILEPATH('wbitmap.pro', SUBDIR=['lib','xdemo','examples']),GROUP = event.top, WIDTH=X
  "TEXT"	: WTEXT, GROUP = event.top
  "TEXTCODE"	: XDISPLAYFILE, FILEPATH('wtext.pro', SUBDIR=['lib','xdemo','examples']),GROUP = event.top, WIDTH=X
  "LABEL"	: WLABEL, GROUP = event.top
  "LABELCODE"	: XDISPLAYFILE, FILEPATH('wlabel.pro', SUBDIR=['lib','xdemo','examples']),GROUP = event.top, WIDTH=X
  "LABTEXT"	: WLABTEXT, GROUP = event.top
  "LABTEXTCODE" : XDISPLAYFILE, FILEPATH('wlabtext.pro', SUBDIR=['lib','xdemo','examples']),GROUP = event.top, WIDTH=X
  "LIST"	: WLIST, GROUP = event.top
  "LISTCODE"	: XDISPLAYFILE, FILEPATH('wlist.pro', SUBDIR=['lib','xdemo','examples']),GROUP = event.top, WIDTH=X
  "MENU"	: WMENU, GROUP = event.top
  "MENUCODE"	: XDISPLAYFILE, FILEPATH('wmenu.pro', SUBDIR=['lib','xdemo','examples']),GROUP = event.top, WIDTH=X
  "PDMENU"	: WPDMENU, GROUP = event.top
  "PDMENUCODE"	: XDISPLAYFILE, FILEPATH('wpdmenu.pro', SUBDIR=['lib','xdemo','examples']),GROUP = event.top, WIDTH=X
  "TOGGLE"	: WTOGGLE, GROUP = event.top
  "TOGGLECODE"	: XDISPLAYFILE, FILEPATH('wtoggle.pro', SUBDIR=['lib','xdemo','examples']),GROUP = event.top, WIDTH=X
  "DRAW"	: WDRAW, GROUP = event.top
  "DRAWCODE"	: XDISPLAYFILE, FILEPATH('wdraw.pro', SUBDIR=['lib','xdemo','examples']),GROUP = event.top, WIDTH=X
  "SCROLL"	: WDR_SCRL, GROUP = event.top
  "SCROLLCODE"	: XDISPLAYFILE, FILEPATH('wdr_scrl.pro', SUBDIR=['lib','xdemo','examples']),GROUP = event.top, WIDTH=X
  "MOTION"      : WMOTION, GROUP = event.top
  "MOTIONCODE"  : XDISPLAYFILE, FILEPATH('wmotion.pro', SUBDIR=['lib','xdemo','examples']),GROUP = event.top, WIDTH=X
  "EXCLUSIVE"	: WEXCLUS, GROUP = event.top
  "EXCLUSCODE"	: XDISPLAYFILE, FILEPATH('wexclus.pro', SUBDIR=['lib','xdemo','examples']),GROUP = event.top, WIDTH=X
  "POPUP"	: WPOPUP, GROUP = event.top
  "POPUPCODE"	: XDISPLAYFILE, FILEPATH('wpopup.pro', SUBDIR=['lib','xdemo','examples']),GROUP = event.top, WIDTH=X
  "TWOMENU"	: W2MENUS, GROUP = event.top
  "TWOMENUCODE"	: XDISPLAYFILE, FILEPATH('w2menus.pro', SUBDIR=['lib','xdemo','examples']),GROUP = event.top, WIDTH=X
  "BACKGROUND"  : WBACK, GROUP = event.top
  "BACKCODE"    : XDISPLAYFILE, FILEPATH('wback.pro', SUBDIR=['lib','xdemo','examples']),GROUP = event.top, WIDTH=X
  "SENSITIZE"	: WSENS, GROUP = event.top
  "SENSCODE"	: XDISPLAYFILE, FILEPATH('wsens.pro', SUBDIR=['lib','xdemo','examples']),GROUP = event.top, WIDTH=X
  "XREGISTER"	: WXREG, GROUP = event.top
  "XREGCODE"	: XDISPLAYFILE, FILEPATH('wxreg.pro', SUBDIR=['lib','xdemo','examples']),GROUP = event.top, WIDTH=X

  "ROTATE"	: WORLDROT, dist(100), GROUP = event.top
  "ROTATECODE"  : XDISPLAYFILE, FILEPATH('worldrot.pro', SUBDIR=['lib','xdemo','examples']),GROUP = event.top
  "SLOTS"	: SLOTS, GROUP = event.top
  "SLOTCODE"    : XDISPLAYFILE, FILEPATH('slots.pro', SUBDIR=['lib','xdemo','examples']),GROUP = event.top

  "XMNG_TMPL"	: XMNG_TMPL, GROUP = event.top
 "XMNG_TMPLCODE": XDISPLAYFILE, FILEPATH('xmng_tmpl.pro', SUBDIR=['lib', 'widgets']), GROUP = event.top
  "XEXAMPLE"	: XEXAMPLE, GROUP = event.top
  "XEXAMPLECODE": XDISPLAYFILE, FILEPATH('xexample.pro', SUBDIR=['lib', 'xdemo']), GROUP = event.top 
  "XSURFACE"	: XSURFACE, DIST(20), GROUP = event.top
  "XSURFACECODE": XDISPLAYFILE, FILEPATH('xsurface.pro', SUBDIR=['lib','widgets']), GROUP = event.top
  "XBM_EDIT"	: XBM_EDIT, GROUP = event.top
  "XBM_EDITCODE": XDISPLAYFILE, FILEPATH('xbm_edit.pro', SUBDIR=['lib','widgets']), GROUP = event.top

  "EXIT": WIDGET_CONTROL, event.top, /DESTROY		;There is no need to
							;"unregister" a widget
							;application.  The
							;XManager will clean
							;the dead widget from
							;its list.
  "ABOUT"	: XDISPLAYFILE,FILEPATH('simpwidg.txt', SUBDIR=['lib','xdemo','examples']),$
		  TITLE = 'About the Simple Widget Examples', GROUP=event.top
  "TIPS"	: XDISPLAYFILE, FILEPATH('wtips.txt',SUBDIR=['lib','xdemo','examples']), $
		  TITLE = 'Widget Programming Tips and Techniques',GROUP=event.top

  ELSE: MESSAGE, "Event User Value Not Found"		;When an event occurs
							;in a widget that has
							;no user value in this
							;case statement, an
							;error message is shown
ENDCASE

END ;============= end of WEXMASTER event handling routine task =============




;------------------------------------------------------------------------------
PRO wexmaster, GROUP = GROUP

IF(XRegistered("wexmaster") NE 0) THEN RETURN		;only one instance of
							;the Wexmaster widget
							;is allowed.  If it is
							;already managed, do
							;nothing and return

XMng_tmplbase = WIDGET_BASE(TITLE = "Simple Widget Examples", /COLUMN)	;The main base.


XPdMenu, [	'"Done"				EXIT',		$
		'"Tools"	{',				$
				'"XLoadct"	XLOADCT',	$
				'"XPalette"	XPALETTE',	$
				'"XManagerTool"	XMANTOOL',	$
				'}',				$
		'"About the Simple Widget Examples" ABOUT',	$
		'"Widget Programming Tips and Techniques" TIPS'],$
	 	XMng_tmplbase


; Make a new sub-base:
base = WIDGET_BASE(xmng_tmplbase, /FRAME, /ROW)

lcol = WIDGET_BASE(base, /FRAME, /COLUMN)	;The left column.
mcol = WIDGET_BASE(base, /FRAME, /COLUMN)	;The middle column.


; Create buttons for all of the little examples with 'See My Code'
; buttons next to them in the left column.

llabel = WIDGET_LABEL(lcol, VALUE = 'Simple Widget Examples:')
sidebyside = WIDGET_BASE(lcol, /ROW)
col1 = WIDGET_BASE(sidebyside, /COLUMN)
col2 = WIDGET_BASE(sidebyside, /COLUMN)

slider =  WIDGET_BUTTON(col1, VALUE = 'Horizontal Slider', UVALUE = 'SLIDER')
slidercode = WIDGET_BUTTON(col2, VALUE = 'See Horizontal Slider Code', UVALUE = 'SLIDERCODE')

vslider =  WIDGET_BUTTON(col1, VALUE = 'Vertical Slider', UVALUE = 'VSLIDER')
vslidercode = WIDGET_BUTTON(col2, VALUE = 'See Vertical Slider Code', UVALUE = 'VSLIDERCODE')

buttons =  WIDGET_BUTTON(col1, VALUE = 'Action Buttons', UVALUE = 'BUTTONS')
buttonscode = WIDGET_BUTTON(col2, VALUE = 'See Action Buttons Code', UVALUE = 'BUTTONSCODE')

bitmap =  WIDGET_BUTTON(col1, VALUE = 'Bitmap Button', UVALUE = 'BITMAP')
bitmapcode = WIDGET_BUTTON(col2, VALUE = 'See Bitmap Button Code', UVALUE = 'BITMAPCODE')

toggle =  WIDGET_BUTTON(col1, VALUE = 'Toggle Buttons', UVALUE = 'TOGGLE')
togglecode = WIDGET_BUTTON(col2, VALUE = 'See Toggle Buttons Code', UVALUE = 'TOGGLECODE')

text =  WIDGET_BUTTON(col1, VALUE = 'Text Widgets', UVALUE = 'TEXT')
textcode = WIDGET_BUTTON(col2, VALUE = 'See Text Widgets Code', UVALUE = 'TEXTCODE')

label =  WIDGET_BUTTON(col1, VALUE = 'Label Widgets', UVALUE = 'LABEL')
labelcode = WIDGET_BUTTON(col2, VALUE = 'See Label Widgets Code', UVALUE = 'LABELCODE')

labtext = WIDGET_BUTTON(col1, VALUE = 'Combination Label/Text Widgets', UVALUE = 'LABTEXT')
labtextcode = WIDGET_BUTTON(col2, VALUE = 'See Label/Text Code', UVALUE = 'LABTEXTCODE')

list =  WIDGET_BUTTON(col1, VALUE = 'List Widget', UVALUE = 'LIST')
listcode = WIDGET_BUTTON(col2, VALUE = 'See List Widget Code', UVALUE = 'LISTCODE')

menu =  WIDGET_BUTTON(col1, VALUE = 'Non-Exclusive Menu', UVALUE = 'MENU')
menucode = WIDGET_BUTTON(col2, VALUE = 'See Non-Exclusive Menu Code', UVALUE = 'MENUCODE')

pdmenu =  WIDGET_BUTTON(col1, VALUE = 'Pull-Down Menu', UVALUE = 'PDMENU')
pdmenucode = WIDGET_BUTTON(col2, VALUE = 'See Pull-Down Menu Code', UVALUE = 'PDMENUCODE')

exclusive = WIDGET_BUTTON(col1, VALUE = 'Exclusive Menu', UVALUE = 'EXCLUSIVE')
exclusivecode = WIDGET_BUTTON(col2, VALUE = 'See Exclusive Menu Code', UVALUE = 'EXCLUSCODE')

twomenu = WIDGET_BUTTON(col1, VALUE = 'Two Menus', UVALUE = 'TWOMENU')
twomenucode = WIDGET_BUTTON(col2, VALUE = 'See Two Menus Code', UVALUE = 'TWOMENUCODE')

popup = WIDGET_BUTTON(col1, VALUE = 'Pop-Up Widget', UVALUE = 'POPUP')
popupcode = WIDGET_BUTTON(col2, VALUE = 'See Pop-Up Code', UVALUE = 'POPUPCODE')

draw = WIDGET_BUTTON(col1, VALUE = 'Draw Widget', UVALUE = 'DRAW')
drawcode = WIDGET_BUTTON(col2, VALUE = 'See Draw Widget Code', UVALUE = 'DRAWCODE')

scrolldraw = WIDGET_BUTTON(col1, VALUE = 'Scrolling Draw Widget', UVALUE = 'SCROLL')
scrolldrawcode = WIDGET_BUTTON(col2, VALUE = 'See Scrolling Draw Code', UVALUE = 'SCROLLCODE')

motion = WIDGET_BUTTON(col1, VALUE = 'Motion Events Draw Widget', UVALUE = 'MOTION')
motioncode = WIDGET_BUTTON(col2, VALUE = 'See Motion Events Code', UVALUE = 'MOTIONCODE')

sensitize = WIDGET_BUTTON(col1, VALUE = 'Sensitizing / Desensitizing', UVALUE = 'SENSITIZE')
sensitizecode = WIDGET_BUTTON(col2, VALUE = 'See Sensitizing / Desensitizing Code', $
			      UVALUE = 'SENSCODE')

background = WIDGET_BUTTON(col1, VALUE = 'Background Task Widget', UVALUE = 'BACKGROUND')
backgroundcode = WIDGET_BUTTON(col2, VALUE = 'See Background Task Code', $
			       UVALUE = 'BACKCODE')

xregister = WIDGET_BUTTON(col1, VALUE = 'Multiple Copies of a Widget', UVALUE = 'XREGISTER')
xregcode = WIDGET_BUTTON(col2, VALUE = 'See Multiple Copies Code', UVALUE = 'XREGCODE')



; Right column has the super-cool example programs written by Keith:

mlabel = WIDGET_LABEL(mcol, VALUE = 'Example Widget Applications:')

side2 = WIDGET_BASE(mcol, /ROW)
col3  = WIDGET_BASE(side2, /COLUMN)
col4  = WIDGET_BASE(side2, /COLUMN)

rotate = WIDGET_BUTTON(col3, VALUE = 'World Rotation Tool', UVALUE = 'ROTATE')

;Slot machine not available under OPENWIN, so check before creating the button:
IF (!VERSION.OS NE 'sunos') THEN $
slot   = WIDGET_BUTTON(col3, VALUE = 'Slot Machine Demo', UVALUE = 'SLOTS')

rotatecode = WIDGET_BUTTON(col4, VALUE = 'See World Rotation Code', UVALUE = 'ROTATECODE')

;Slot machine not available under OPENWIN, so check before creating the button:
IF (!VERSION.OS NE 'sunos') THEN $
slotcode   = WIDGET_BUTTON(col4, VALUE = 'See Slot Machine Code', UVALUE = 'SLOTCODE')

template = WIDGET_BUTTON(col3, VALUE = 'Widgets Template', UVALUE = 'XMNG_TMPL')
templatecode = WIDGET_BUTTON(col4, VALUE = 'See Widgets Template Code', UVALUE = 'XMNG_TMPLCODE')

xexample = WIDGET_BUTTON(col3, VALUE = 'All Widget Types Example', UVALUE = 'XEXAMPLE')
xexamplecode = WIDGET_BUTTON(col4, VALUE = 'See Widget Types Code', UVALUE = 'XEXAMPLECODE')

;XSURFACE and XBM_EDIT do not currently work right under OPENWIN, so check before creating the buttons:
IF (!VERSION.OS NE 'sunos') THEN BEGIN
xsurface = WIDGET_BUTTON(col3, VALUE = 'XSurface Tool', UVALUE = 'XSURFACE')
xsurfacecode = WIDGET_BUTTON(col4, VALUE = 'See XSurface Code', UVALUE = 'XSURFACECODE')

xbm_edit = WIDGET_BUTTON(col3, VALUE = 'Bit Map Editor', UVALUE = 'XBM_EDIT')
xbM_editcode = WIDGET_BUTTON(col4, VALUE = 'See Bit Map Editor Code', UVALUE = 'XBM_EDITCODE')

ENDIF


; Realize the widgets:
WIDGET_CONTROL, XMng_tmplbase, /REALIZE		

XManager, "wexmaster", XMng_tmplbase, $			;register the widgets,
		GROUP_LEADER = GROUP  			;with the XManager
							;and pass through the
							;group leader if this
							;routine is to be 
							;called from some group
							;leader.

END ;==================== end of WEXMASTER main routine =======================
























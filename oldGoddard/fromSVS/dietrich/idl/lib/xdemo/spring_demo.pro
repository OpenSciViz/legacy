; spring_demo.pro
; Written: March, 1991.  DMS / RSI.
; Copyright (c) 1991, Research Systems, Inc.  All rights reserved.
;	Unauthorized reproduction prohibited.
;----------------------------------------
pro mark_square, x, y, color   ; Draw square in mark window at cell (x,y)
p0 = fix(convert_coord(x,y,/data, /to_device)) + 2
p1 = fix(convert_coord(x+1, y+1, /data, /to_device))-2
polyfill, [p0(0),p1(0),p1(0),p0(0)],[p0(1),p0(1),p1(1),p1(1)],$
	color=color, /dev
end

;----------------------------------------
pro spring_row_menu, base, names, values, title, initial
; Make a row menu, 1 column high. Exclusive ...
tmp = WIDGET_BASE(base, /ROW)
if strlen(title) gt 0 then junk = WIDGET_LABEL(tmp, value = title)
junk = WIDGET_BASE(tmp, /EXCLUSIVE, /ROW)
for i=0, n_elements(values)-1 do begin
	junk1 = WIDGET_BUTTON(junk, VALUE=strtrim(names(i),2), $
			UVALUE = values(i))
	if values(i) eq initial(0) then WIDGET_CONTROL, junk1, set_button=1
	ENDFOR
end

;----------------------------------------
pro edit_update			;Update the surface display in the edit window

COMMON spring_demo, nframes, wsize, kspring, max_spring, ngrid, $
	method_names, win_sizes, num_frames, ax, az, WIN_SIZE_BIT, $
	NUM_FRAMES_BIT, METHOD_BUTTON_BIT, GRID_BUTTON_BIT, grid_sizes, $
	start_grid, edit_window, edit_pos, mark_window, mark_id, views, $
	view_names, spring_demobase, space_widget

old_window = !d.window
xmsave = !x.margin		;Clear margins
ymsave = !y.margin
!x.margin=0
!y.margin=0
wset, edit_window
surface, start_grid, ax = ax, az = az, xst=4, yst=4, zst=4
!x.margin = xmsave		;Restore margins
!y.margin = ymsave
wset, old_window
end

;----------------------------------------
pro spring_demo_edit, GROUP = group  ;Edit the springs

COMMON spring_demo, nframes, wsize, kspring, max_spring, ngrid, $
	method_names, win_sizes, num_frames, ax, az, WIN_SIZE_BIT, $
	NUM_FRAMES_BIT, METHOD_BUTTON_BIT, GRID_BUTTON_BIT, grid_sizes, $
	start_grid, edit_window, edit_pos, mark_window, mark_id, views, $
	view_names, spring_demobase, space_widget

if XRegistered("spring_demo_edit") then RETURN	;Only one activation

se_base = WIDGET_BASE(TITLE = "Spring Edit")	;Make edit base

top_base = WIDGET_BASE(se_base, /column)


XPdMenu, [ '" Done "		EDIT_DONE', $
	   '" Reset "		EDIT_RESET'], top_base


htslider = WIDGET_SLIDER(top_base, xsize=256, minim = -100, maxim = 100, $
	title = 'Spring Displacement', value = edit_pos(2), $
	uvalue = 'HTSLIDER')

row_base = WIDGET_BASE(top_base, /row)
t_base = WIDGET_BASE(row_base, /column)
junk = WIDGET_TEXT(t_base, ysize = 1, value = $
	'Double click on cell to set displacement')
mark_id = WIDGET_DRAW(t_base, xsize=384, ysize=384, /BUTTON_EVENTS, ret=2)
cdrawable = WIDGET_DRAW(row_base, xsize = 384, ysize=384, ret=2)

WIDGET_CONTROL, se_base, /REALIZE
WIDGET_CONTROL, cdrawable, get_value = edit_window
WIDGET_CONTROL, mark_id, get_value = mark_window

wset, mark_window

!x.s = [ 1. , 1.]/(ngrid+2.)		;Set scaling for grid
!y.s = !x.s

for iy = 0,ngrid do plots,[0,ngrid],[iy,iy]	;Draw the grid
for ix=  0,ngrid do plots,[ix,ix],[0,ngrid]
edit_update				;Show the surface

XManager, "spring_demo_edit", se_base, $	;And fly with it
	EVENT_HANDLER = "spring_demo_ev", $
	GROUP_LEADER = group
end



;----------------------------------------
pro spring_demo_do, GROUP = group
;Does the calculations & drawing for spring demo

COMMON spring_demo, nframes, wsize, kspring, max_spring, ngrid, $
	method_names, win_sizes, num_frames, ax, az, WIN_SIZE_BIT, $
	NUM_FRAMES_BIT, METHOD_BUTTON_BIT, GRID_BUTTON_BIT, grid_sizes, $
	start_grid, edit_window, edit_pos, mark_window, mark_id, views, $
	view_names, spring_demobase, space_widget

; 0 for surface
; 1 for shaded surface (light_source)
; 2 for altitude shaded surface
; 3 for velocity shaded surface
; 4 for image of displacement
; 5 for image of abs velocity
; 6 for contour
; 7 for squares shaded by displacement
; 8 for squares shaded by velocity

nviews = fix(total(views ge 0))
if nviews le 0 then return		;Anything?

WIDGET_CONTROL, spring_demobase, SENSITIVE = 0  ;Buttons inactive

if views(0) ge 0 then method = views(0) else method = views(1)

win = !d.window
xmsave = !x.margin
ymsave = !y.margin
!x.margin=0
!y.margin=0

s = fltarr(ngrid+2, ngrid+2)	;Starting displacements, allow for reflects
s(1,1) = start_grid		;Insert starting vels
v = fltarr(ngrid+2, ngrid+2)	;Starting velocity

sq21 = 1./sqrt(2)		;Make kernel for convolution
ker = kspring * [[ sq21, 1, sq21], [1, 0, 1], [sq21, 1, sq21]]
ker(1,1) = -total(ker)
				;Stack in proper direction
if wsize ge 192 then pview = [ nviews, 1] else pview = [1, nviews]
;				Set up animation
xinteranimate, set=[wsize * pview(0), wsize * pview(1), nframes], $
			title = method_names(method), /SHOWLOAD

if nviews gt 1 then window, xs=wsize, ys=wsize, $
	/free, title=method_names(method)
cwin = !d.window		;The drawing window
rebin_size = (wsize / ngrid) * ngrid
erase

ss = fltarr(ngrid,ngrid,nframes, /nozero)	;Do all at once, displacement
vv = ss				;Velocity

for iter = 0, nframes-1 do begin	;Each iteration
	v = v + convol(s, ker)	;New velocity
	s = s + v		;New displacement
	vv(0,0,iter) = v(1:ngrid, 1:ngrid)	;Save for scaling
	ss(0,0,iter) = s(1:ngrid, 1:ngrid)
	endfor

vmax = max(vv, min=vmin)		;Get scaling
smax = max(ss, min=smin)
if (!d.n_colors gt 2) and (!d.n_colors le 256) then top = !d.n_colors-1 $
else top = 256

for iter = 0, nframes-1 do $		;Each frame
  for kview = 0, nviews-1 do begin	;Each view
	if views(kview) lt 0 then iview = kview+1 else iview = kview
	s = ss(*,*,iter)
	v = vv(*,*,iter)

case views(iview) of			;Which rendering?

; Surface
0: 	surface,s, zrange=[smin,smax],xstyle=4, ystyle=4, zstyle=4, $
		ax=ax, az=az

; Shaded surface (light source)
1: 	shade_surf,s, zrange=[smin,smax],xstyle=4, ystyle=4, zstyle=4, $
		ax=ax, az = az

; altitude shaded surface
2:	shade_surf,s, shades=bytscl(s, top=top, min=smin, max=smax),$
		zrange=[smin,smax], xstyle=4, ystyle=4, zstyle=4, $
		ax = ax, az = az
; velocity shaded surface
3:	shade_surf,s, shades=bytscl(v, top = top, $
		max=vmax, min=vmin),$
		zrange=[smin,smax], xstyle=4, ystyle=4, zstyle=4, $
		ax = ax, az = az
; image of displacement
4:	tv, rebin(bytscl(s, max=smax, min = smin), rebin_size, rebin_size)

; image of velocity
5:	tv, rebin(bytscl(v, max=vmax, min=vmin), rebin_size, rebin_size)

; contour
6:	contour,s,lev=findgen(16.)/8.-1., xstyle=5, ystyle=5, $
		c_color = top * 0.5 * findgen(16)/8.

; squares by displacement
7: 	begin
	z = bytscl(s, min = smin, max = smax, top = top)	;shades
 shade_squares:
	surfr, AX = ax, AZ = az		;Set the scaling
	!x.s = [ 0, 1. / ngrid ]
	!y.s = !x.s
	!z.s = [ -smin, smax] / (smax-smin)
	erase
	del = 0.04			;Fudge factor to cover surface
	px = [ -del, 1.+del, 1.+del, -del]	;Basic polygon
	py = [ -del, -del, 1.+del, 1.+del]
	for iy = ngrid-1, 0, -1 do for ix = 0, ngrid-1 do $ ;Draw squares
		polyfill, px + ix, py + iy, replicate(ss(ix,iy,iter), 4), $
			/t3d, color=z(ix,iy)
	endcase			;squares

; squares by velocity
8: 	begin	
	z = bytscl(abs(v), min =0, max = vmax, top = top)
	goto, shade_squares
	endcase			;squares by vel
  endcase

  if kview eq 0 then xyouts, 5,5, /dev, 'Frame '+strtrim(iter,2) ;label it
  xinteranimate, frame=iter, window=cwin, $
	xoffset = wsize * kview * (pview(0)-1), $
	yoffset = wsize * kview * (pview(1)-1)
  endfor

!x.margin = xmsave
!y.margin = ymsave
if nviews gt 1 then wdelete, cwin
WIDGET_CONTROL, spring_demobase, SENSITIVE = 1
xinteranimate, GROUP = group, 85
end



;----------------------------------------
PRO spring_demo_ev, event

COMMON spring_demo, nframes, wsize, kspring, max_spring, ngrid, $
	method_names, win_sizes, num_frames, ax, az, WIN_SIZE_BIT, $
	NUM_FRAMES_BIT, METHOD_BUTTON_BIT, GRID_BUTTON_BIT, grid_sizes, $
	start_grid, edit_window, edit_pos, mark_window, mark_id, views, $
	view_names, spring_demobase, space_widget

WIDGET_CONTROL, event.id, GET_UVALUE = eventval		;find the user value
							;of the widget where
							;the event occured
s = size(eventval)
if s(s(0)+1) eq 2 then begin		;Button type = int
	i = eventval and 15		;Button index
	if (eventval and METHOD_BUTTON_BIT) ne 0 then begin  ;Methods?
		j = (eventval and 16)/16	;0 for left, 1 for right
		if event.select eq 1 then views(j) = i
		if event.select eq 0 and views(j) eq i then views(j) = -1
		goto, update_siz
		endif
;			Check other buttons
	if event.select eq 0 then return   ;These don't care about releases
	if (eventval and WIN_SIZE_BIT) ne 0 then begin
		wsize = win_sizes(i)
update_siz:	WIDGET_CONTROL, space_widget, SET_VALUE = $
			strtrim(long(wsize) * wsize * nframes * $
			fix(total(views ge 0)) / 1000L,2) + $
			' KB required for Pixmap    '
		return
		end

	if (eventval and NUM_FRAMES_BIT) ne 0 then begin
		nframes = num_frames(i)
		goto, update_siz
		endif
	if (eventval and GRID_BUTTON_BIT) ne 0 then $
	  if grid_sizes(i) ne ngrid then begin ;changing grid size
		ngrid = grid_sizes(i)
		start_grid = fltarr(ngrid, ngrid)  ;New Starting grid
		start_grid(ngrid/3,ngrid/3) = 1.
		endif
	return
	end


if event.id eq mark_id then begin	;Mark the edit window?
	if event.press eq 0 then return	;Ignore release
	old_win = !d.window
	wset, mark_window
	!x.s = [ 1. , 1.]/(ngrid+2.)
	!y.s = !x.s
	p = convert_coord(event.x, event.y, /device, /to_data) ; get coords
	p(0) = fix(p(0)) > 0 < (ngrid-1)
	p(1) = fix(p(1)) > 0 < (ngrid-1)
	if (p(0) eq edit_pos(0)) and (p(1) eq edit_pos(1)) then begin ;Mark???
		start_grid(edit_pos(0), edit_pos(1)) = edit_pos(2)/100.
		edit_update
	endif else begin
		mark_square, edit_pos(0),edit_pos(1),0  ;Erase old
		edit_pos(0) = p(0)
		edit_pos(1) = p(1)
		mark_square, edit_pos(0),edit_pos(1), !d.n_colors-1
	endelse
	wset, old_win
	return
	endif

				; Discard button releases
if tag_names(event, /structure) eq 'WIDGET_BUTTON' then begin
	if event.select eq 0 then return
	endif

CASE eventval OF
  "KSPRING_SLIDER" : kspring = event.value * max_spring / 100
  "HTSLIDER" :  edit_pos(2) = event.value
  "AX_SLIDER" : begin
		ax = event.value
		if XRegistered("spring_demo_edit") then edit_update
		endcase
  "AZ_SLIDER" : begin
		az = event.value
		if XRegistered("spring_demo_edit") then edit_update
		endcase		
  "EDIT_DONE" : WIDGET_CONTROL, event.top, /DESTROY
  "EDIT_RESET" : begin
		start_grid = fltarr(ngrid, ngrid)
		edit_update
		endcase
  "EXIT": begin
	WIDGET_CONTROL, event.top, /DESTROY
	if XRegistered("XInterAnimate") then xinteranimate, /close
	endcase

  "GO" : if xregistered("XInterAnimate") eq 0 then $
		spring_demo_do, GROUP = event.top
  "STOP" : xinteranimate, /close
  "EDIT" : spring_demo_edit, GROUP = event.top
  "HELP" : XDisplayFile, FILEPATH("spring_demo.txt", subdir='help'),	$
		TITLE = "Spring Demo Help", $
		GROUP = event.top, $
		WIDTH = 55, $
		HEIGHT = 16
  ELSE: MESSAGE, "Event User Value Not Found"		;When an event occurs
							;in a widget that has
							;no user value in this
							;case statement, an
							;error message is shown
ENDCASE

END ;============= end of spring_demo event handling routine task =============



;----------------------------------------
PRO spring_demo, GROUP = group

COMMON spring_demo, nframes, wsize, kspring, max_spring, ngrid, $
	method_names, win_sizes, num_frames, ax, az, WIN_SIZE_BIT, $
	NUM_FRAMES_BIT, METHOD_BUTTON_BIT, GRID_BUTTON_BIT, grid_sizes, $
	start_grid, edit_window, edit_pos, mark_window, mark_id, views, $
	view_names, spring_demobase, space_widget

;		Set up defaults and constants:
if n_elements(nframes) le 0 then begin
	nframes = 16			;Initial values
	wsize = 256			;Window size
	max_spring = 0.2		;Largest possible spring const
	kspring = 0.05			;Initial spring const
	ngrid = 16			;Init grid size
	mark_id = 0
	views = [0,-1]			;Initial views & selected view
	
	start_grid = fltarr(ngrid, ngrid)  ;Starting grid
	start_grid(ngrid/3,ngrid/3) = 1.

	edit_pos = [ 0, 0, 100]
	win_sizes = [ 128, 192, 256 ]		;Size of windows we support
	num_Frames = [ 16, 32, 64 ]
	
	grid_sizes = [ 8, 16, 24, 32]

	GRID_BUTTON_BIT = 256			;Constants to identify buttons
	NUM_FRAMES_BIT = 128
	WIN_SIZE_BIT = 64
	METHOD_BUTTON_BIT = 32
	
	method_names = [ $
		"Mesh Surface",$
		"Light Source Shaded Surface", $
		"Displacement Shaded Surface", $
		"Velocity Shaded Surface",$
		"Displacement Image",$
		"Velocity Image",$
		"Contour Plot", $
		"Displacement Shaded Squares",$
		"Velocity Shaded Squares"]	
	ax = 30			;X axis rotation
	az = 30			;Z axis rotation
endif				;Inited

IF XRegistered("spring_demo") THEN RETURN		;only one instance
IF XRegistered("XInterAnimate") THEN BEGIN
	Print, "Only one animation at a time please."
	RETURN
	ENDIF

spring_demobase = WIDGET_BASE(TITLE = "Spring Demo")	;create the main base


row_base = WIDGET_BASE(spring_demobase, /row)

c_base = WIDGET_BASE(row_base, /column)
r_base = WIDGET_BASE(row_base, /column)

XPdMenu, [	'" Done "			EXIT',		$
		'"   Go!   "			GO',		$
		'" Stop "			STOP', 		$
		'" Edit "			EDIT',		$
		'" Help "			HELP'		$
	],  c_base

c0_base = WIDGET_BASE(c_base, /column, /frame)
r0_base = WIDGET_BASE(c0_base, /ROW)
c1_base = WIDGET_BASE(r0_base, /COLUMN)
c2_base = WIDGET_BASE(r0_base, /COLUMN)

junk = WIDGET_LABEL(c1_base, VALUE = 'Left')
junk = WIDGET_LABEL(c2_base, VALUE = 'Right        Rendering Method')
c1_base = WIDGET_BASE(c1_base, /COLUMN, /EXCLUSIVE)
c2_base = WIDGET_BASE(c2_base, /COLUMN, /EXCLUSIVE)

FOR i = 0, n_elements(method_names)-1 DO BEGIN
	junk = WIDGET_BUTTON(c1_base, $
		UVALUE = i+METHOD_BUTTON_BIT, VALUE = ' ')
	if i eq views(0) then WIDGET_CONTROL, junk, set_button=1
	junk = WIDGET_BUTTON(c2_base, $
			UVALUE = i+16+METHOD_BUTTON_BIT, $
			VALUE= ' ' + method_names(i))
	if i eq views(1) then WIDGET_CONTROL, junk, set_button=1
	ENDFOR


kspring_slider = WIDGET_SLIDER(r_base, $
	XSIZE = 256,$
	MINIMUM = 1, $
	MAXIMUM = 100, $
	VALUE = 100 * kspring / max_spring, $
	TITLE = 'Spring Strength',$
	UVALUE = "KSPRING_SLIDER")

axslider = WIDGET_SLIDER(r_base, $
	XSIZE = 256,$
	MINIMUM = -90, $
	MAXIMUM = 90, $
	VALUE = ax, $
	TITLE = 'X axis rotation',$
	UVALUE = "AX_SLIDER")

azslider = WIDGET_SLIDER(r_base, $
	XSIZE = 256,$
	MINIMUM = 0, $
	MAXIMUM = 90, $
	VALUE = az, $
	TITLE = 'Z axis rotation',$
	UVALUE = "AZ_SLIDER")

spring_row_menu, r_base, strtrim(win_sizes,2), $
	indgen(n_elements(win_sizes))+WIN_SIZE_BIT, $
	'View area size: ', where(win_sizes eq wsize) + WIN_SIZE_BIT

spring_row_menu, r_base, strtrim(num_frames,2), $
	indgen(n_elements(num_frames))+NUM_FRAMES_BIT,$
	'Number of frames: ', where(num_frames eq nframes)+NUM_FRAMES_BIT

spring_row_menu, r_base, strtrim(grid_sizes,2), $
	indgen(n_elements(grid_sizes))+GRID_BUTTON_BIT, $
	'Grid size: ', where(grid_sizes eq ngrid) + GRID_BUTTON_BIT

space_widget = WIDGET_LABEL(r_base, VALUE = $
	strtrim(long(wsize) * wsize * nframes * $
		fix(total(views ge 0)) / 1000L,2) + $
		' KB required for Pixmap')

WIDGET_CONTROL, spring_demobase, /REALIZE	;create the widgets


XManager, "spring_demo", spring_demobase, $
		EVENT_HANDLER = "spring_demo_ev", $
		GROUP_LEADER = GROUP
							;group leader if this
							;routine is to be 
							;called from some group
							;leader.

END

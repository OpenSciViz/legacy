; Copyright (c) 1991, Research Systems, Inc.  All rights reserved.
;	Unauthorized reproduction prohibited.
;
;	MakeMountains
;
;		This example demonstrates many of the possibilities
;	using IDL and Widgets.  It allows the user to view simple
;	data generated using IDL in many ways.
;
;

function makehill
return, 0
end

function makehill, ground, iterations

if(iterations gt 0) then begin
  newhill = fltarr(3,3)

  newhill(0,0) = ground(0,0)
  newhill(0,2) = ground(0,1)
  newhill(2,0) = ground(1,0)
  newhill(2,2) = ground(1,1)

  newhill(0,1) = (ground(0,0) + ground(0,1)) / 2
  newhill(1,0) = (ground(0,0) + ground(1,0)) / 2
  newhill(1,2) = (ground(0,1) + ground(1,1)) / 2
  newhill(2,1) = (ground(1,0) + ground(1,1)) / 2

  newhill(1,1) = ((ground(0,1) + $
		   ground(0,1) + $
		   ground(1,0) + $
		   ground(1,1))/4) + randomu(seed)*(iterations^5)

  if(iterations gt 1) then begin
    sub1 = makehill(newhill(0:1,0:1), iterations - 1)
    sub2 = makehill(newhill(0:1,1:2), iterations - 1)
    sub3 = makehill(newhill(1:2,0:1), iterations - 1)
    sub4 = makehill(newhill(1:2,1:2), iterations - 1)
    size = sqrt(n_elements(sub1))
    ground = fltarr(size * 2 - 1, size * 2 - 1)
    ground(0,0) = sub1
    ground(0,size - 1) = sub2
    ground(size - 1, 0) = sub3
    ground(size -1,size-1) = sub4
    return, ground
  endif else return, newhill
endif else return, ground
end


function mountain, iterations

hill = fltarr(2,2)

hill = makehill(hill, iterations)

return, hill

end

;------------------------------------------------------
;   procedure dodraw
;------------------------------------------------------

PRO dodraw, style				;this procedure generates a
						;new mountain if needed and
						;then draws it in the current
						;display mode

COMMON hill, data, generate_new_flag, $		;this procedure creates a 
	     mountbase, drawing_style, $	;widget that lets the user
	     iteration_value, labelid, $
	     mountdisplay, trans, x, y
						;mountain does not get 
						;regenerated when the only 
						;view is switched

IF(generate_new_flag NE 0) THEN BEGIN		;if the flag is set, a new
  data = mountain(iteration_value)		;mountain data set is created
  generate_new_flag = 0
ENDIF

WSET, mountdisplay

CASE style OF					;for each drawing style, do
  0: SURFACE, data, $				;the drawing with the axes
		XSTYLE = 4, $			;suppressed which is style 4
		YSTYLE = 4, $
		ZSTYLE = 4, $
		XMARGIN = [0,0], $
		YMARGIN = [0,0], $
		COLOR = 215

  1: SHADE_SURF, data, $
		XMARGIN = [0,0], $
		YMARGIN = [0,0], $
		XSTYLE = 4, $
		YSTYLE = 4, $
		ZSTYLE = 4

  2: IF(iteration_value NE 0) THEN $
       CONTOUR, data, $
		XSTYLE = 1, $
		YSTYLE = 1, $
		XMARGIN = [0,0], $
		YMARGIN = [0,0], $
	  	/SPLINE, $
		LEVELS = (INDGEN(7)/7.0) * MAX(data) $
     ELSE ERASE

  3: BEGIN
       levels = (INDGEN(7)/7.0) * MAX(data)
       CONTOUR, data, $
		/SPLINE, $
		PATH = "cont.dat", $
		XMARGIN = [0,0], $
		YMARGIN = [0,0], $
		XSTYLE = 1, $
		YSTYLE = 1, $
		LEVELS = levels
       POLYCONTOUR, "cont.dat", $
		COLOR_INDEX = (INDGEN(9) * (!D.N_COLORS / 9.0)), $
		/DELETE_FILE
       CONTOUR, data, $
		/SPLINE, $
		/NOERASE, $
		XMARGIN = [0,0], $
		YMARGIN = [0,0], $
		XSTYLE = 1, YSTYLE = 1, $
		LEVELS = levels
     END

  4: BEGIN
       SURFACE, data, /SAVE, $
		XMARGIN = [0,0], $
		YMARGIN = [0,0], $
		XSTYLE = 4, $
		YSTYLE = 4, $
		ZSTYLE = 4, $
		COLOR = !D.N_COLORS / 2
       trans = !P.T
     END

  5: show3, data, /INTERP

ENDCASE

END ;================ procedure dodraw ====================


;------------------------------------------------------
;   procedure mkmnt_event
;------------------------------------------------------

PRO mkmnt_event, event

COMMON hill, data, generate_new_flag, $		;this procedure creates a 
	     mountbase, drawing_style, $	;widget that lets the user
	     iteration_value, labelid, $
	     mountdisplay, trans, x, y

WIDGET_CONTROL, GET_UVALUE = retval, event.id

CASE retval OF
    "ITERSLIDE": BEGIN
		   WIDGET_CONTROL, event.id, GET_VALUE = number_of_iter
		   IF(number_of_iter NE iteration_value) THEN BEGIN
		     iteration_value = number_of_iter
		     generate_new_flag = 1
		     dodraw, drawing_style
		   ENDIF
		 END

    "DRAWMNTSURF": IF(drawing_style NE 0) THEN BEGIN
		     drawing_style = 0
		     WIDGET_CONTROL, labelid, $
			SET_VALUE = "Wire Frame Surface of Mountain"
		     dodraw, drawing_style
		   ENDIF

    "SHADEMNTSURF": IF(drawing_style NE 1) THEN BEGIN
		     drawing_style = 1
		     WIDGET_CONTROL, labelid, $
			SET_VALUE = "Shaded Surface of Mountain"
		     dodraw, drawing_style
		   ENDIF

    "CONTOURMNT": IF(drawing_style NE 2) THEN BEGIN
		     drawing_style = 2
		     WIDGET_CONTROL, labelid, $
			SET_VALUE = "Contour Plot of Mountain"
		     dodraw, drawing_style
		   ENDIF

    "SHADECNTMNT": IF(drawing_style NE 3) THEN BEGIN
		      drawing_style = 3
		      WIDGET_CONTROL, labelid, $
			SET_VALUE = "Shaded Contour Plot of Mountian"
		      dodraw, drawing_style
		    ENDIF

    "ROAMPLOT": IF(drawing_style NE 4) THEN BEGIN
		  drawing_style = 4
		  WIDGET_CONTROL, labelid, $
		    SET_VALUE = "Interactive View of Mountain (position mouse over plot)"
		  dodraw, drawing_style
		ENDIF

    "COMPOSVIEW": IF(drawing_style NE 5) THEN BEGIN
		  drawing_style = 5
		  WIDGET_CONTROL, labelid, $
		    SET_VALUE = "Composite View of Mountain"
		  dodraw, drawing_style
		ENDIF

    "MNTDRAW": IF(drawing_style EQ 4) THEN BEGIN
		 WSET, mountdisplay
		 save = !P.T
		 !P.T = trans
                 width = N_ELEMENTS(data(0,*))
                 xpos = INDGEN(width)
                 ypos = INTARR(width)
		 xval = FIX((x / 400.) * width)
		 yval = FIX((y / 400.) * width)
	         PLOTS, ypos + xval, xpos, data(xval, *), COLOR = 80, /T3D
	         PLOTS, xpos, ypos + yval, data(*, yval), COLOR = 80, /T3D
	         PLOTS, xval, yval, data(xval, yval), COLOR = 0, /T3D, $
			PSYM = 4
		 x = 0 > event.x < 399
		 y = 0 > event.y < 399
		 xval = FIX((x / 400.) * width)
		 yval = FIX((y / 400.) * width)
	         PLOTS, ypos + xval, xpos, data(xval, *), COLOR = 150, /T3D
		 PLOTS, xpos, ypos + yval, data(*, yval), COLOR = 200, /T3D
		 PLOTS, xval, yval, data(xval, yval), COLOR = 250, /T3D, $
			PSYM = 4
 		 !P.T = save
               ENDIF

    "MNTHELP": XDisplayFile, filepath("makemountains.txt", subdir='help'), $
		TITLE = "Mountain Example Help", $
		GROUP = event.top, $
		WIDTH = 55, $
		HEIGHT = 16

    "COLADJ": XLoadct, GROUP = event.top

    "EXIT": BEGIN
	      WIDGET_CONTROL, event.top, /DESTROY
	      labelid = 0
	    END
    ELSE:
ENDCASE
END


;------------------------------------------------------
;   procedure makemountains
;------------------------------------------------------

PRO makemountains, GROUP = GROUP

COMMON hill, data, generate_new_flag, $		;this procedure creates a 
	     mountbase, drawing_style, $	;widget that lets the user
	     iteration_value, labelid, $	;view a simple fractal mountain
	     mountdisplay, trans, x, y		;using widgets.  The user can
						;choose the number of
						;iterations in the mountain's
						;generation

IF(Xregistered("makemountains") NE 0) THEN GOTO, FINISH

						;defaults:
iteration_value = 3				;	3 iterations
drawing_style = 0				;	surface mesh drawing
generate_new_flag = 1				;	need new mountain data
						;	  to start off with

						;below a new base is created
						;with the size and title set
						
mountbase = WIDGET_BASE(/COLUMN, $
		SPACE = 20, $
		XPAD = 20, $
		YPAD = 20, $
	        TITLE = "Mountain Example")

XPdMenu, [	'"Exit Mountain Example"	EXIT',		$
		'"Views" {		',			$
			'"Wire Frame Surface"	DRAWMNTSURF',	$
			'"Shaded Surface"	SHADEMNTSURF',	$
			'"Contour Plot"		CONTOURMNT',	$
			'"Shaded Contour Plot"	SHADECNTMNT',	$
			'"Interactive View"	ROAMPLOT',	$
			'"Composite View"	COMPOSVIEW',	$
			'}',					$
		'"Adjust Color Palette"		COLADJ',	$
		'"Help"				MNTHELP'],	$
	 mountbase

mountiterat = WIDGET_SLIDER(mountbase, $
			TITLE = "Number of Iterations", $
			MAXIMUM = 5, $
			MINIMUM = 0, $
			VALUE = iteration_value, $
			UVALUE = "ITERSLIDE")

drawbase = WIDGET_BASE(mountbase, /FRAME, $
				/COLUMN)
labelid = WIDGET_LABEL(drawbase, VALUE = "Wire Frame Surface of Mountain")

						;below a mountain is
mountpicture = WIDGET_DRAW(drawbase, XSIZE = 400, YSIZE = 400, $
				      XOFFSET = 20, YOFFSET = 180, $
				      /BUTTON_EVENTS, UVALUE = "MNTDRAW", $
				      /MOTION_EVENTS, RETAIN = 2)

WIDGET_CONTROL, mountbase, /REALIZE
WIDGET_CONTROL, mountpicture, GET_VALUE = mountdisplay

dodraw, drawing_style

x = 100
y = 100

Xmanager, "makemountains", mountbase, EVENT_HANDLER = "mkmnt_event", $
	  GROUP_LEADER = GROUP

FINISH:

END ;================ procedure dodraw ====================


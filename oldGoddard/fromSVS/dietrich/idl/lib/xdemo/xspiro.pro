; Copyright (c) 1991, Research Systems, Inc.  All rights reserved.
;	Unauthorized reproduction prohibited.
;+
; NAME:
;	XSPIRO
; PURPOSE:
;	A Widget front end to the SPIRO procedure to draw
;	"Spirograph" (TM) patterns
; CATEGORY:
;	demo, widgets
; CALLING SEQUENCE:
;	XSPIRO
; OUTPUTS:
;	None.
; COMMON BLOCKS:
;	None.
; SIDE EFFECTS:
;	Uses a widget based interface to allow dynamic manipulation of
;	"Spirograph" patterns.
; RESTRICTIONS:
;	None.
; MODIFICATION HISTORY:
;	13, December, 1990, A.B.
;
;-


; The init and event procedures use the following uvalue codes
; to communicate intentions:
;
;	0 - Line Style 0
;	1 - Line Style 1
;	2 - Line Style 2
;	3 - Line Style 3
;	4 - Line Style 4
;	5 - Line Style 5
;	6 - Done
;	7 - Reset
;	8 - Outer Ring
;	9 - Inner Ring
;	10 - Distance 
;	11 - Color





pro xspiro_event, ev
  COMMON xspiro_com, base, sr_w, rr_w, dist_w, draw_win, c_w, $
	sr, rr, dist, color, linestyle, solid_lstyle, ls_base

  WIDGET_CONTROL, ev.id, get_uvalue=uval
  if (uval lt 6) then begin
    if (not ev.select) then goto, done
    linestyle = uval
  endif else begin
    case (uval) of
    6: begin
      WIDGET_CONTROL, /DESTROY, base
      goto, done
      end
    7: begin
	sr = 23
	rr = 17
	dist = 11
	color = !p.color
	linestyle = 0
	WIDGET_CONTROL, sr_w, set_value=sr
	WIDGET_CONTROL, rr_w, set_value=rr
	WIDGET_CONTROL, dist_w, set_value=dist
	WIDGET_CONTROL, ls_base, set_button=0
	WIDGET_CONTROL, solid_lstyle, set_button=1
	WIDGET_CONTROL, c_w, set_value=color
	end
    8: sr = ev.value
    9: rr = ev.value
    10: dist = ev.value
    11: begin
	color = ev.value
	WIDGET_CONTROL, c_w, set_value=color
	end
;    l_w: linestyle = ev.value
    endcase
  endelse

  cur_win = !d.window
  wset, draw_win
  spiro, sr, rr, dist, color=color, linestyle=linestyle
  if (cur_win ne -1) then wset, cur_win

done:
end







pro xspiro, GROUP = GROUP
  COMMON xspiro_com, base, sr_w, rr_w, dist_w, draw_win, c_w, $
	sr, rr, dist, color, linestyle, solid_lstyle, ls_base

  if (XREGISTERED('XSPIRO')) then return	; Only one copy at a time

  lstyles = ['Solid', 'Dotted', 'Dashed', 'Dash Dot', $
	'Dash Dot Dot Dot', 'Long Dashes' ]
  sr = 23
  rr = 17
  dist = 11
  linestyle = !p.linestyle

  base = widget_base(/column, title='XSpiro')
    draw_w = widget_draw(base, xsize=256, ysize=256, /frame)
    c_base = widget_base(base, /ROW)
    c_base_1 = widget_base(c_base, /COLUMN, space=50)
      done_w = widget_button(c_base_1,value='Done', uvalue=6)
      reset_w = widget_button(c_base_1,value='Reset', uvalue=7)
    c_base_2 = widget_base(c_base, /column, xpad = 20)
      tmp = widget_label(c_base_2, value="Line Style")
      ls_base = widget_base(c_base_2, /column, /exclusive)
      solid_lstyle = widget_button(ls_base, value=lstyles(0),uvalue=0)
      for i = 1, 5 do tmp = widget_button(ls_base, value=lstyles(i),uvalue=i)
    c_base_3 = widget_base(base, /column, xsize= 256)
      sr_w = widget_slider(c_base_3, title='Stable Radius', value=sr, uvalue=8)
      rr_w = widget_slider(c_base_3, title='Rotating Radius', value = rr, $
	uvalue=9)
      dist_w = widget_slider(c_base_3, title='Distance', value=dist, uvalue=10)
      c_w = widget_slider(c_base_3, title='Color Index', $
	max = !d.table_size-1, uvalue=11)

  widget_control, /real, base
  widget_control, draw_w, get_value=draw_win
  widget_control, solid_lstyle, set_button=1
  color = !p.color
  widget_control, c_w, set_value = color

  cur_win = !d.window
  wset, draw_win
  spiro, sr, rr, dist, color=color, linestyle=linestyle
  if (cur_win ne -1) then wset, cur_win
  XMANAGER, 'XSPIRO', base, GROUP_LEADER = GROUP
end


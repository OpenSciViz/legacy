; Copyright (c) 1991, Research Systems, Inc.  All rights reserved.
;	Unauthorized reproduction prohibited.
	;------------------------------------------------------
	;   procedure setsliders
	;------------------------------------------------------
	; this routine sets the sliders that are data dependent.
	; The keyword NEW tells whether to label the new sliders
	; or destroy old ones before making replacements.
	;------------------------------------------------------

PRO setsliders, new = new

COMMON fourier, image, imagetrans, view, cutofflabel, bandwidthlabel, $
	        VIEWSIZE, order, cutoff, bandwidth, filter, filteredimage, $
		filteredspectrum, NVIEWS, fourierbase, waitlabel, bandtype, $
		BUTTER, EXPON, IDEAL, LOWPASS, HIGHPASS, BANDPASS, $
		BANDREJECT, cutoffslider, bandwidthslider, FILTERCUTOFF, $
		FILTERBANDWIDTH, oldxmargins, oldymargins, VIEWOPTBASE, $
		VIEWOPTTOP, NVIEWOPTS, FIRSTBANDTYPE, LASTBANDTYPE, $
		FIRSTFILTER, LASTFILTER, HELP, LOAD, COLADJUST, FILTERORDER, $
		FILTERTYPE, ORDERSLIDER, EXIT, sliderbase, statusbase

dimens = SIZE(image)
IF(KEYWORD_SET(NEW)) THEN BEGIN
  sliderwidth = (NVIEWS*VIEWSIZE) + ((NVIEWS+1)*20) - 670
  cutoffslider = WIDGET_SLIDER(sliderbase, $
		XSIZE = sliderwidth, $
		VALUE = cutoff / dimens(1) * 2. * 10000., $
		MINIMUM = 1, $
		MAXIMUM = 10000, $
		UVALUE = FILTERCUTOFF, $
		/SUPPRESS_VALUE)
  cutofflabel = WIDGET_LABEL(sliderbase, $
		VALUE = "Cutoff    = " + $
			STRING(FIX(cutoff)))
  bandwidthslider = WIDGET_SLIDER(sliderbase, $
		XSIZE = sliderwidth, $
		VALUE = bandwidth / dimens(1) * 4. * 10000., $
		MINIMUM = 0, $
		MAXIMUM = 10000, $
		UVALUE = FILTERBANDWIDTH, $
		/SUPPRESS_VALUE)
  WIDGET_CONTROL, bandwidthslider, $
		SENSITIVE = 0
  bandwidthlabel = WIDGET_LABEL(sliderbase, $
		VALUE = "BandWidth    = " + $
			STRING(FIX(bandwidth)))
  WIDGET_CONTROL, bandwidthlabel, $
		SENSITIVE = 0
ENDIF ELSE BEGIN
  WIDGET_CONTROL, cutoffslider, SET_VALUE = 2000.
  cutoff = 20.
  WIDGET_CONTROL, cutofflabel, $
		SET_VALUE = "Cutoff    = " + $
			STRING(FIX(cutoff))
  WIDGET_CONTROL, bandwidthslider, SET_VALUE = 1000.
  bandwidth = 10.
  WIDGET_CONTROL, bandwidthlabel, $
		SET_VALUE = "BandWidth    = " + $
			STRING(FIX(bandwidth))
ENDELSE
END
	;------------ end of procedure setsliders -------------


	;------------------------------------------------------
	;   procedure loaddata
	;------------------------------------------------------
	; This routine gets the name of the new data file to be
	; loaded using the procedure getfile.  It then loads the
	; data, resets the controls, and computes the new FFT of
	; the new data.  It also erases the old filtered image
	; and the old filtered power spectrum.
	;
	; KEYWORDS:
	;	NAME - if name is specified, the user is not
	;	  prompted with a choice and name is used in its
	;	  place.
	;------------------------------------------------------

PRO loaddata, NEW = NEW

COMMON fourier, image, imagetrans, view, cutofflabel, bandwidthlabel, $
	        VIEWSIZE, order, cutoff, bandwidth, filter, filteredimage, $
		filteredspectrum, NVIEWS, fourierbase, waitlabel, bandtype, $
		BUTTER, EXPON, IDEAL, LOWPASS, HIGHPASS, BANDPASS, $
		BANDREJECT, cutoffslider, bandwidthslider, FILTERCUTOFF, $
		FILTERBANDWIDTH, oldxmargins, oldymargins, VIEWOPTBASE, $
		VIEWOPTTOP, NVIEWOPTS, FIRSTBANDTYPE, LASTBANDTYPE, $
		FIRSTFILTER, LASTFILTER, HELP, LOAD, COLADJUST, FILTERORDER, $
		FILTERTYPE, ORDERSLIDER, EXIT, sliderbase, statusbase

XGetData, newdata, DESCRIPTION = datadesc, $
		DIMENSIONS = datadim, $
		/ONE_DIM, $
		/TWO_DIM, $
		TITLE = "Please Select Sample For Filtering"

IF(KEYWORD_SET(newdata)) THEN BEGIN
  IF(datadim(1) NE 1) THEN BEGIN
    IF(datadim(0) LT (VIEWSIZE / 2)) THEN BEGIN
      datadim(0) = datadim(0) * (VIEWSIZE / datadim(0))
      datadim(1) = datadim(1) * (VIEWSIZE / datadim(1))
      image = REBIN(newdata, datadim(0), datadim(1))
    ENDIF ELSE IF (datadim(0) GT VIEWSIZE) THEN BEGIN
      datadim(0) = datadim(0) / (1 + datadim(0)/VIEWSIZE)
      datadim(1) = datadim(1) / (1 + datadim(1)/VIEWSIZE)
      image = REBIN(newdata, datadim(0), datadim(1))
    ENDIF ELSE image = newdata
  ENDIF ELSE image = newdata
ENDIF

IF(NOT(KEYWORD_SET(NEW))) THEN setsliders
IF(KEYWORD_SET(image)) THEN imagetrans = FFT(image, -1)
filteredimage = 0
filteredspectrum = 0

END
	;------------- end of procedure loaddata --------------


	;------------------------------------------------------
	;   procedure resetfilter
	;------------------------------------------------------
	; This procedure computes the correct filter depending
	; on the bandpass type.  Here a high pass filter is 
	; one minus the low pass filter.
	;------------------------------------------------------

PRO resetfilter, NEW = NEW

COMMON fourier, image, imagetrans, view, cutofflabel, bandwidthlabel, $
	        VIEWSIZE, order, cutoff, bandwidth, filter, filteredimage, $
		filteredspectrum, NVIEWS, fourierbase, waitlabel, bandtype, $
		BUTTER, EXPON, IDEAL, LOWPASS, HIGHPASS, BANDPASS, $
		BANDREJECT, cutoffslider, bandwidthslider, FILTERCUTOFF, $
		FILTERBANDWIDTH, oldxmargins, oldymargins, VIEWOPTBASE, $
		VIEWOPTTOP, NVIEWOPTS, FIRSTBANDTYPE, LASTBANDTYPE, $
		FIRSTFILTER, LASTFILTER, HELP, LOAD, COLADJUST, FILTERORDER, $
		FILTERTYPE, ORDERSLIDER, EXIT, sliderbase, statusbase

COMMON fourier_dist, distfun


filteredimage = 0				;reset the filtered image and
						;the filtered spectrum since
filteredspectrum = 0				;changing the filter affects
						;them
imagesize = SIZE(image)

IF N_ELEMENTS(distfun) ne N_ELEMENTS(image) then begin
	IF(imagesize(0) EQ 1) THEN BEGIN	;compute either a single 
	  distfun = FINDGEN(imagesize(1))	;dimensional or two dimensional
	  distfun = distfun < (imagesize(1) - distfun)	;euclidean dist fcn
	ENDIF ELSE BEGIN		;2D case
	  distfun = DIST(imagesize(1))		;depending on the data dim.
	ENDELSE
	distfun(0) = 1e-4			;Avoid division by 0
ENDIF						;New dist fcn

WIDGET_CONTROL, waitlabel, $			;Display wait message while
		SET_VALUE = "PLEASE WAIT"	;computing filter

CASE FILTERTYPE OF

		;Butterworth filter

  BUTTER: BEGIN
	    IF(bandtype EQ LOWPASS) THEN $
	      filter = 1.0 / (1.0 + (distfun / cutoff)^(2 * order)) $
	    ELSE IF(bandtype EQ HIGHPASS) THEN $
	      filter = 1.0 / (1.0 + (cutoff / distfun)^(2 * order)) $
	    ELSE BEGIN
	      filter = distfun * distfun - cutoff^2  ;Dist squared
	      zeroes = where(filter eq 0.0, count)
	      if count ne 0 then filter(zeroes) = 1e-6  ;Avoid divide by 0
	      filter = 1.0 / (1.0 + ((distfun*bandwidth)/ filter) $
			^ (2 * order))
	      IF bandtype eq BANDPASS then filter = 1.0 - filter
	    ENDELSE
           END


  EXPON: BEGIN				;Exponential filter
	   IF(bandtype EQ LOWPASS) THEN $
	     filter = exp((-(distfun / cutoff)^order)) $
	   ELSE IF(bandtype EQ HIGHPASS) THEN $
	     filter = 1.0 - exp((-(distfun / cutoff)^order)) $
	   ELSE BEGIN	;Bandpass / reject,  avoid underflow in exp fcn
	     filter = (distfun * distfun - cutoff^2) / (distfun * bandwidth)
	     filter = exp(-(filter ^ (2 * order) < 25))
	     IF(bandtype EQ BANDREJECT) THEN filter = 1.0 - filter
	   ENDELSE
         END


  IDEAL: BEGIN				;Ideal filter
	   IF(bandtype EQ LOWPASS) THEN $	
	     filter = (distfun le cutoff) $
           ELSE IF(bandtype EQ HIGHPASS) THEN $
	     filter = 1.0 - (distfun le cutoff) $
	   ELSE IF(bandtype EQ BANDREJECT) THEN $
	     filter = ((distfun lt (cutoff - bandwidth / 2.0)) or $
		     (distfun gt (cutoff + bandwidth / 2.0))) $
	   ELSE $
	     filter = ((distfun ge (cutoff - bandwidth / 2.0)) and $
		     (distfun le (cutoff + bandwidth / 2.0)))
END

  ELSE: MESSAGE, "resetfilter: Bad Filter Type"
ENDCASE

FOR i = 0, NVIEWS - 1 DO $			;since the filter has changed
  IF(view(i).viewtype GT 1) THEN resetview, $	;redraw all the windows that
		i, UPDATE_LABEL = NEW		;depend on the filter

WIDGET_CONTROL, waitlabel, $
		SET_VALUE = "   READY   "	;clear the wait flag

END
	;---------------- end of resetfilter ------------------


	;------------------------------------------------------
	;   procedure resetview
	;------------------------------------------------------
	; This function resets the view number passed in.  It 
	; is assumed that the structure associated with the 
	; passed in view has been altered in view type so it
	; needs to be redrawn.  In the case of one dimensional
	; data, there is no filter intensity image and cross -
	; sections of the filter are the same as filter plots
	; so they just call those routines.  Once the view has
	; been updated, the button that controls that view is
	; renamed to reflect the new view type.
	;------------------------------------------------------

pro resetview, windowindex, UPDATE_LABEL = UPDATE_LABEL

COMMON fourier, image, imagetrans, view, cutofflabel, bandwidthlabel, $
	        VIEWSIZE, order, cutoff, bandwidth, filter, filteredimage, $
		filteredspectrum, NVIEWS, fourierbase, waitlabel, bandtype, $
		BUTTER, EXPON, IDEAL, LOWPASS, HIGHPASS, BANDPASS, $
		BANDREJECT, cutoffslider, bandwidthslider, FILTERCUTOFF, $
		FILTERBANDWIDTH, oldxmargins, oldymargins, VIEWOPTBASE, $
		VIEWOPTTOP, NVIEWOPTS, FIRSTBANDTYPE, LASTBANDTYPE, $
		FIRSTFILTER, LASTFILTER, HELP, LOAD, COLADJUST, FILTERORDER, $
		FILTERTYPE, ORDERSLIDER, EXIT, sliderbase, statusbase

wset, view(windowindex).windownum		;set to draw in passed in view
if(view(windowindex).viewtype ne 8) then $	;if not a saved view then 
  erase, color = 100				;erase the view

imagesize = size(image)

case view(windowindex).viewtype of 
		
		;Original Image Drawing
  0: begin 
       if(imagesize(0) GT 1) then begin
         tvscl, image, (VIEWSIZE - imagesize(1)) / 2, $
		(VIEWSIZE - imagesize(2)) / 2 
         if(keyword_set(UPDATE_LABEL)) then $
           widget_control, view(windowindex).controlid, $
				set_value = "Original Image View"
       endif else begin
         plot, image, /xst, /yst
         if(keyword_set(UPDATE_LABEL)) then $
           widget_control, view(windowindex).controlid, $
				set_value = "Original Signal Plot"
       endelse
     end

		;Image Spectrum Drawing
  1: begin 
       if(imagesize(0) GT 1) then $
         tvscl, shift(alog(abs(imagetrans)), $
		imagesize(1) / 2, imagesize(2) / 2),$
		(VIEWSIZE - imagesize(1)) / 2, $
		(VIEWSIZE - imagesize(2)) / 2 $
       else begin
         plot_io, shift(abs(imagetrans), imagesize(1)/2), /xst
       endelse
       if(keyword_set(UPDATE_LABEL)) then $
         widget_control, view(windowindex).controlid, $
				set_value = "Log Power Spectrum"
     end

		;Filter Plot Drawing
  2: begin 
	grid_size = 32
       if(imagesize(0) GT 1) then begin
         thinfilter = congrid(filter, grid_size, grid_size)
         surface, shift(thinfilter, grid_size/2, grid_size/2),$
		xstyle = 4, ystyle = 4, zstyle = 4
       endif else begin
         plot, shift(filter, imagesize(1) / 2), /xst
       endelse
       if(keyword_set(UPDATE_LABEL)) then $
         widget_control, view(windowindex).controlid, $
				set_value = "Plot of Filter"
       empty
     end

		;Filter Intensity Drawing
  3: begin 
       if(imagesize(0) GT 1) then begin
         tvscl, shift(filter, imagesize(1) / 2, imagesize(2) / 2), $
		(VIEWSIZE - imagesize(1)) / 2, $
		(VIEWSIZE - imagesize(2)) / 2
         if(keyword_set(UPDATE_LABEL)) then $
           widget_control, view(windowindex).controlid, $
				set_value = "Filter Intensity Image"
       endif else begin
         view(windowindex).viewtype = 2
	 resetview, windowindex, UPDATE_LABEL = UPDATE_LABEL
       endelse
     end

		;Filter Cross Section Drawing
  4: begin 
       if(imagesize(0) GT 1) then begin
         plot, shift(filter(*, 0), imagesize(1) / 2), /xst
         if(keyword_set(UPDATE_LABEL)) then $
	   widget_control, view(windowindex).controlid, $
				set_value = "Filter Cross Section"
         empty
       endif else begin
	 view(windowindex).viewtype = 2
	 resetview, windowindex, UPDATE_LABEL = UPDATE_LABEL
       endelse
     end

		;Filter Shade Surface Drawing
  5: begin 
	grid_size = 48
       if(imagesize(0) GT 1) then begin
         thinfilter = congrid(filter, grid_size, grid_size)
         shade_surf, shift(thinfilter, grid_size/2, grid_size/2), $
		     xstyle = 4, ystyle = 4, zstyle = 4
       endif else begin
         plot, shift(filter, imagesize(1) / 2), /xst
       endelse
       if(keyword_set(UPDATE_LABEL)) then $
         widget_control, view(windowindex).controlid, $
				set_value = "Shaded Surface of Filter"
       empty
     end

		;Filtered Spectrum drawing
  6: begin 
       if(n_elements(filteredspectrum) le 1) then $
	 filteredspectrum = imagetrans * filter
       if(imagesize(0) GT 1) then $
         tvscl, shift(alog(abs(filteredspectrum)> 1e-10), $
		imagesize(1) / 2, imagesize(2) / 2), $
		(VIEWSIZE - imagesize(1)) / 2, $
		(VIEWSIZE - imagesize(2)) / 2 $
       else begin
         plot_io, shift(abs(filteredspectrum), imagesize(1)/2), $
		XSTYLE = 4, $
		YSTYLE = 4
         axis, imagesize(1) / 2, 0, /yaxis, /noerase, ycharsize = 0.001, $
		yticklen = .02
         axis, 0, 0, /xaxis, /noerase, xcharsize = .001, xticklen = .02
       endelse
       if(keyword_set(UPDATE_LABEL)) then $
         widget_control, view(windowindex).controlid, $
				set_value = "Log Filtered Power Spectrum"
     end

		;Filtered Image drawing
  7: begin 
       if(n_elements(filteredimage) le 1) then $
	 filteredimage = float(FFT(imagetrans * filter, 1))
       if(imagesize(0) GT 1) then begin
         tvscl, filteredimage, $
		(VIEWSIZE - imagesize(1)) / 2, $
		(VIEWSIZE - imagesize(2)) / 2
       if(keyword_set(UPDATE_LABEL)) then $
         widget_control, view(windowindex).controlid, $
				set_value = "Filtered Image"
       endif else begin
         plot, filteredimage
         if(keyword_set(UPDATE_LABEL)) then $
           widget_control, view(windowindex).controlid, $
				set_value = "Filtered Signal"
       endelse
     end

		;Saved View Drawing
   8: if(keyword_set(UPDATE_LABEL)) then $
        widget_control, view(windowindex).controlid, $
				set_value = "Saved View"

  else: widget_control, view(windowindex).controlid, $
				set_value = "Unsupported View"
endcase
end
	;------------------- end of resetview -----------------

	;------------------------------------------------------
	;   procedure four_event
	;------------------------------------------------------
	; This is the main event handler for the fourier demo.
	;------------------------------------------------------

pro four_event, event

common fourier, image, imagetrans, view, cutofflabel, bandwidthlabel, $
	        VIEWSIZE, order, cutoff, bandwidth, filter, filteredimage, $
		filteredspectrum, NVIEWS, fourierbase, waitlabel, bandtype, $
		BUTTER, EXPON, IDEAL, LOWPASS, HIGHPASS, BANDPASS, $
		BANDREJECT, cutoffslider, bandwidthslider, FILTERCUTOFF, $
		FILTERBANDWIDTH, oldxmargins, oldymargins, VIEWOPTBASE, $
		VIEWOPTTOP, NVIEWOPTS, FIRSTBANDTYPE, LASTBANDTYPE, $
		FIRSTFILTER, LASTFILTER, HELP, LOAD, COLADJUST, FILTERORDER, $
		FILTERTYPE, ORDERSLIDER, EXIT, sliderbase, statusbase

dimens = SIZE(image)

widget_control, get_uvalue = retval, event.id

	;check if view change was selected and if so, reset the affected view

  if(retval ge VIEWOPTBASE and retval lt viewopttop) then begin
    viewindex = fix((retval - VIEWOPTBASE)/NVIEWOPTS)
    setting = ((retval - VIEWOPTBASE) mod NVIEWOPTS)
    if(view(viewindex).viewtype ne setting) then begin
      view(viewindex).viewtype = setting
      resetview, viewindex, /UPDATE_LABEL
    endif

	;next see if the band type was changed and if so, update filter

  endif else if (retval ge FIRSTBANDTYPE and retval le LASTBANDTYPE) then begin
    if(event.select eq 1 and bandtype ne retval) then begin
      bandtype = retval
      resetfilter
    endif
    IF(bandtype NE BANDPASS) AND (bandtype NE BANDREJECT) THEN BEGIN
      WIDGET_CONTROL, bandwidthslider, $
		SENSITIVE = 0
      WIDGET_CONTROL, bandwidthlabel, $
		SENSITIVE = 0

    ENDIF else BEGIN
      WIDGET_CONTROL, bandwidthslider, $
		/SENSITIVE
      WIDGET_CONTROL, bandwidthlabel, $
		/SENSITIVE
    ENDELSE


	;reset the filter if the filter type was changed

  endif else if (retval ge FIRSTFILTER and retval le LASTFILTER) then begin
    if(event.select eq 1 and filtertype ne retval) then begin
      filtertype = retval
      resetfilter
    endif
  endif else case retval of

	;the other choices are unique so deal with each in the case statement

    HELP : XDisplayFile, FILEPATH("fourierdemo.txt", subdir='help'),	$
		TITLE = "Fourier Demo Help", $
		GROUP = event.top, $
		WIDTH = 55, $
		HEIGHT = 16

    LOAD : begin					;load new data file
	     widget_control, fourierbase, sensitive = 0
	     loaddata
	     resetfilter, /NEW
	     for i = 0, NVIEWS - 1 do $
	 	if(view(i).viewtype lt 2) then resetview, i
	     widget_control, fourierbase, /sensitive
	   end

    COLADJUST : xloadct, /SILENT, GROUP = event.top	;adjust the colors

    FILTERORDER : begin					;reset filter if order
		  widget_control, ORDERSLIDER, $	;value changed
				  get_value = temp
		  if(temp ne order) then begin
		    order = temp
		    resetfilter
		  endif
  		end

    FILTERCUTOFF : begin				;reset filter if freq.
		  widget_control, cutoffslider, $	;cutoff changed
				  get_value = temp
		  cutoff = temp/10000. * (dimens(1) / 2.)
  		  WIDGET_CONTROL, cutofflabel, $
		    SET_VALUE = "Cutoff    = " + $
				STRING(FIX(cutoff))

		    resetfilter
  		end
							;reset filter if bandw.
							;has changed and also
							;if bandwidth matters
    FILTERBANDWIDTH : begin				;with current filter
			if((bandtype eq BANDPASS) or $
	   		   (bandtype eq BANDREJECT))then begin
		          widget_control, bandwidthslider, get_value = temp
			  bandwidth = temp/10000. * (dimens(1) / 4.)
			  WIDGET_CONTROL, bandwidthlabel, $
			  SET_VALUE = "BandWidth    = " + $
						STRING(FIX(bandwidth))
			  resetfilter
		  	endif
  		      end

    EXIT : begin
 	     widget_control, fourierbase, /destroy  ;free up the widgets used
	     !X.margin = oldxmargins		    ;restore previous margins
	     !Y.margin = oldymargins
	   end
    else: 
  endcase
end


	;------------------------------------------------------
	;   procedure fourierdemo
	;------------------------------------------------------
	; This is the main procedure for the fourier demo.
	;------------------------------------------------------

PRO fourierdemo, GROUP = GROUP

COMMON fourier, image, imagetrans, view, cutofflabel, bandwidthlabel, $
	        VIEWSIZE, order, cutoff, bandwidth, filter, filteredimage, $
		filteredspectrum, NVIEWS, fourierbase, waitlabel, bandtype, $
		BUTTER, EXPON, IDEAL, LOWPASS, HIGHPASS, BANDPASS, $
		BANDREJECT, cutoffslider, bandwidthslider, FILTERCUTOFF, $
		FILTERBANDWIDTH, oldxmargins, oldymargins, VIEWOPTBASE, $
		VIEWOPTTOP, NVIEWOPTS, FIRSTBANDTYPE, LASTBANDTYPE, $
		FIRSTFILTER, LASTFILTER, HELP, LOAD, COLADJUST, FILTERORDER, $
		FILTERTYPE, ORDERSLIDER, EXIT, sliderbase, statusbase

IF (XRegistered("fourierdemo") NE 0) THEN GOTO, FINISH

oldxmargins = !X.MARGIN
oldymargins = !Y.MARGIN
!X.MARGIN = [6, 2]
!Y.MARGIN = [2, 2]

filteredimage = 0				;initialization
filteredspectrum = 0
waitlabel = 0

HELP		= 0				;constants for user choices
LOAD		= 1
FILTERORDER 	= 2
FILTERCUTOFF	= 3
FILTERBANDWIDTH	= 4
COLADJUST	= 5	
EXIT		= 6


FIRSTBANDTYPE	= 7				;constants for the types of
  LOWPASS	= 7				;filtering passes
  HIGHPASS	= 8
  BANDPASS	= 9
  BANDREJECT	= 10
LASTBANDTYPE	= 10

FIRSTFILTER	= 11				;constants for the different
  BUTTER	= 11				;filter types
  EXPON		= 12
  IDEAL		= 13
LASTFILTER	= 13

order = 2					;default filter order, type
filtertype = BUTTER				;pass and width
cutoff = 20.
bandwidth = 10.
bandtype = LOWPASS

DEVICE, GET_SCREEN = screendims			;determine the device size in
						;pixels
IF(screendims(0) LT 1155) THEN $
  VIEWSIZE = 200 $				;number of views and view
ELSE $						;size in pixels (device)
  VIEWSIZE = 256
NVIEWS = 4	

loaddata, /NEW					;load in the initial data

IF(NOT(KEYWORD_SET(image))) THEN GOTO, FINISH	;if no data was selected, drop
						;out of routine

viewinstance = {vinst, 	viewtype:0b, 	$	;structure for each view
			controlid:0L, 	$
			windowid:0L, 	$
			windownum:0}

view = REPLICATE(viewinstance, NVIEWS)		;array of view structures

view(0).viewtype = 0				;the default view settings
IF(NVIEWS GE 2) THEN view(1).viewtype = 1	;that are used for differing
IF(NVIEWS GE 3) THEN view(2).viewtype = 2	;numbers of views
IF(NVIEWS GE 4) THEN view(3).viewtype = 7
IF(NVIEWS GE 5) THEN view(4).viewtype = 6
IF(NVIEWS GE 6) THEN view(5).viewtype = 0

instance = {vopt, viewname:'', $
		  viewid:0}			;structures describing each
NVIEWOPTS = 9					;type of view are kept in 
VIEWOPTBASE = 20				;an array and they have user
viewopttop = VIEWOPTBASE + (NVIEWOPTS*NVIEWS)	;values ranging from viewbase
viewoptions = REPLICATE(instance, NVIEWOPTS)	;to NVIEWS * NVIEWOPTS

viewoptions(0).viewname = "Original Signal"	
viewoptions(0).viewid = 0
viewoptions(1).viewname = "Log Power Spectrum"
viewoptions(1).viewid = 1
viewoptions(2).viewname = "Filter Plot"
viewoptions(2).viewid = 2
viewoptions(3).viewname = "Filter Intensity Image"
viewoptions(3).viewid = 3
viewoptions(4).viewname = "Filter Cross Section"
viewoptions(4).viewid = 4
viewoptions(5).viewname = "Shaded Surface of Filter"
viewoptions(5).viewid = 5
viewoptions(6).viewname = "Log Filtered Power Spectrum"
viewoptions(6).viewid = 6
viewoptions(7).viewname = "Filtered Signal"
viewoptions(7).viewid = 7
viewoptions(8).viewname = "Save View"
viewoptions(8).viewid = 8

fourierbase = WIDGET_BASE(TITLE = "Fourier Filtering Example", $
		/COLUMN, $
		XPAD = 20, $
		YPAD = 20, $
		SPACE = 20)

fouriercontrol = WIDGET_BASE(fourierbase, /ROW)
control_exit = WIDGET_BUTTON(fouriercontrol, $
		VALUE = "Exit Fourier Demo", $
		UVALUE = EXIT)
control_data = WIDGET_BUTTON(fouriercontrol, $
		VALUE = "Load New Data", $
		UVALUE = LOAD)
control_color = WIDGET_BUTTON(fouriercontrol, $
		VALUE = "Adjust Color Table", $
		UVALUE = COLADJUST)
control_help = WIDGET_BUTTON(fouriercontrol, $
		VALUE = "Help", $
		UVALUE = HELP)

;------------------ the view windows are defined ---------------------

IF(!VERSION.OS EQ 'vms') THEN $			;account for differences
  initialname = "Log Filtered Power Spectrum" $	;between fonts with vms
ELSE $						;and the rest of the world
  initialname = "                          "

viewbase = WIDGET_BASE(fourierbase, $
		/ROW, $
		SPACE = 20)

FOR viewindex = 0, NVIEWS - 1 DO BEGIN
  tempbase = WIDGET_BASE(viewbase, /FRAME, $
		/COLUMN)
  view(viewindex).controlid = WIDGET_BUTTON(tempbase, $
		VALUE = initialname, $
		MENU = 2)
  view(viewindex).windowid = WIDGET_DRAW(tempbase, $
		XSIZE = VIEWSIZE, $
		YSIZE = VIEWSIZE, $
		RETAIN = 2)
  FOR i = 0, NVIEWOPTS - 1 DO BEGIN
     dummy = WIDGET_BUTTON(view(viewindex).controlid, $
		VALUE = viewoptions(i).viewname, $
		UVALUE = viewoptions(i).viewid + $ 
			 VIEWOPTBASE + (NVIEWOPTS*viewindex))  
  ENDFOR
ENDFOR

;----------------- the filter controls are defined ------------------

controlbase = WIDGET_BASE(fourierbase, $
		/ROW, $
		SPACE = 40)

funtypes = ["Butterworth Filter", "Exponential Filter", "Ideal Filter"]
funusers = [BUTTER, EXPON, IDEAL]
XMenu, funtypes, $
		controlbase, $
		UVALUE = funusers, $
		/EXCLUSIVE, $
		/COLUMN, $
		BUTTONS = bandset, $
		TITLE = "Filter Function"

;---------------- the bandpass controls are defined -----------------

filttypes = ["Low Pass", "High Pass", "Band Pass", "Band Reject"]
filtusers = [LOWPASS, HIGHPASS, BANDPASS, BANDREJECT]
XMenu, filttypes, $
		controlbase, $
		UVALUE = filtusers, $
		/EXCLUSIVE, $
		/COLUMN, $
		BUTTONS = filtset, $
		TITLE = "Filter Type"

;-------------------- the order control is defined -------------------

orderbase = WIDGET_BASE(controlbase, $
		/COLUMN)
orderlabel = WIDGET_LABEL(orderbase, $
		VALUE = "Filter Order")
ORDERSLIDER = WIDGET_SLIDER(orderbase, $
		YSIZE = 140, $
		/VERTICAL, $
		VALUE = order, $
		MINIMUM = 1, $
		MAXIMUM = 10, $
		UVALUE = FILTERORDER)

;--------- the sliders for filter width and cutoff are defined -------

sliderbase = WIDGET_BASE(controlbase, $
		/COLUMN)

setsliders, /NEW				;builds the cutoff and 
						;bandwidth liders

;--------------- the computation status label is defined --------------

statusbase = WIDGET_BASE(controlbase, $
		/FRAME, $
		/COLUMN, $
		XPAD = 10, $
		SPACE = 30)
statuslabel = WIDGET_LABEL(statusbase, $
		VALUE = "Status")
waitlabel = WIDGET_LABEL(statusbase, $
		VALUE = "PLEASE WAIT")

WIDGET_CONTROL, fourierbase, /REALIZE		;register all the widget things
						;with Xwindows

WIDGET_CONTROL, filtset(0), /SET_BUTTON		;set the default buttons in the
WIDGET_CONTROL, bandset(0), /SET_BUTTON		;toggle button selections

FOR i = 0, NVIEWS - 1 DO BEGIN			;now that they exist, get the
  WIDGET_CONTROL, view(i).windowid, $		;drawing area numbers and set
		GET_VALUE = temp		;the view structure array so 
  view(i).windownum = temp			;these values can be referenced
ENDFOR						;when drawing

						;display wait while seting up

resetview, 0, /UPDATE_LABEL			;draws the default views that
resetview, 1, /UPDATE_LABEL			;don't rely on the filter and
resetfilter, /NEW				;then reset the filter which
						;automatically draws the 
						;filter's dependent views

WIDGET_CONTROL, waitlabel, $
		SET_VALUE = "   READY   "	;remove wait, ready to go!

XManager, "fourierdemo", $
		fourierbase, $
		EVENT_HANDLER = "four_event", $
	  	GROUP_LEADER = GROUP

FINISH:

END
	;================ procedure fourierdemo ====================


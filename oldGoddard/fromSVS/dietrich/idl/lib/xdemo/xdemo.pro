; Copyright (c) 1991, Research Systems, Inc.  All rights reserved.
;	Unauthorized reproduction prohibited.
;	XDemo
;
;		This program can be run from anywhere as long as the 
;	IDL_DIR environmental variable has been set.  This program is
;	intended to provide the user with a demonstration of IDL using
;	widgets as well as some simple examples to get the user started
;	in programming in widgets. 
;		While XDemo can be called from anywhere, the examples
;	that it calls can only be called from the directory that they 
;	are found in.
;		To run the XDemo, just type XDemo at the IDL prompt.
;

PRO xdemo_event, event

; This is the event processing routine that takes care of the events being
; sent to it from the XManager.

WIDGET_CONTROL, GET_UVALUE=uvalue, event.id

CASE uvalue OF
    "MAINHELP":	XDisplayFile, filepath('xdemo.txt', subdir='help'), $
		TITLE = "Main Demo Help", $
		HEIGHT = 16, $ 
		WIDTH = 55, $
		GROUP = event.top

    "IDLDESC": XDisplayFile, filepath('idldesc.txt', subdir='help'), $
		TITLE = "What is IDL", $
		HEIGHT = 16, $ 
		WIDTH = 60, $
		GROUP = event.top

    "EXIT": WIDGET_CONTROL, event.top, /DESTROY
    
    ELSE:  q = EXECUTE(uvalue + ',GROUP = event.top')  ;Other procs
ENDCASE
END


PRO xdemo

; This is the main program that creates the widget for the XDemo and then 
; registers it with the XManager.

message, 'Entering the Xdemo.', /INFO

base = WIDGET_BASE(TITLE = "IDL Demo", $
		/COLUMN)

XPdMenu, [	'"Quit"					EXIT',		$
		'"Examples"	{',					$
			'"Animation"			ANIMATEDEMO',	$
			'"Fourier Filtering"		FOURIERDEMO',	$
	        	'"Knee Xray"			KNEEDEMO',	$
			'"Map Projections"		MAP_DEMO',	$
			'"Mountain"			MAKEMOUNTAINS',	$
			'"Slicer Demo"			SLICER_DEMO',	$
			'"Spirograph"			XSPIRO',	$
			'"Spring Demo"			SPRING_DEMO',	$
			'"Sun Image"	 		SUNDEMO',	$
			'"The People at RSI"		PEOPLE_DEMO',   $
			'"Widget Types"			XEXAMPLE',	$
			'"Widget Examples (Simple)"	WEXMASTER',	$
			'"XLoadCt Color Table Chooser"	XLOADCT',	$
			'"XManager Tool"		XMANAGERTOOL',	$
			'"XPalette Color Editor"	XPALETTE',	$
			'}',						$
		'"What is IDL"	IDLDESC',				$
		'"Help"		MAINHELP'],				$
	 base

draw = WIDGET_DRAW(base, $
		XSIZE = 512, $
		YSIZE = 385, $
		RETAIN = 2)

logo = BYTARR(512,384)

OPENR, unit, filepath("xdemo.dat", SUBDIRECTORY = "images"), /GET_LUN
READU, unit, logo
FREE_LUN, unit

WIDGET_CONTROL, /REALIZE, base

TV, logo

XManager, "xdemo", base

END

PRO slicer_demo, GROUP = group
COMMON volume_data, a

if XRegistered("slicer") THEN RETURN
a = 0
i = 0

base = WIDGET_BASE(TITLE = 'Slicer Demo', /column)
junk = WIDGET_LABEL(base, VALUE = 'Pick one:')
choices = ['Knee X Rays','Hydrodynamic Simulation', 'Canned Demo', 'Cancel']
junk = WIDGET_LIST(base, FONT = '*times-bold-r-*180*', $
	VALUE = choices, YSIZE = n_elements(choices))
WIDGET_CONTROL, base, /realize
e = WIDGET_EVENT(base, BAD_ID = i)
widget_control, base, /destroy

case e.index of		;Read the data
0: BEGIN
	openr,1,filepath('knee_slices.dat', subdir='images')
	a = bytarr(256,256,35,/nozero)
   ENDCASE
1: BEGIN
	openr,1,filepath('jet.dat', subdir='images')
	a = bytarr(81,40,101,/nozero)
   ENDCASE

2: BEGIN
  commands = [ $
	'COLOR 5 0 100 20', $
	'TRANS 0 0', $
	'SLICE 0 51 0', $
	'SLICE 1 26 0', $
	'SLICE 2 23 0', $
	'TRANS 1 35', $
	'SLICE 0 27 0', $
	'SLICE 1 15 0', $
	'SLICE 1 9 0', $
	'SLICE 0 13 0', $
	'WAIT 4', $
	'ORI 0 1 2 1 1 0 30 30 1', $
	'TRANS 0 36', $
	'ISO 36 1', $
	'CUBE 2 0 0 20 10 25 60 39 100', $
	'COLOR 0 0 100 20', $
	'WAIT 5', $
	'UNDO 0', $
	'CUBE 2 0 1 20 10 25 60 39 100', $
	'TRANS 1 36', $
	'SLICE 0 38', $
	'SLICE 1 23', $
	'TRANS 1 44', $
	'SLICE 1 31', $
	'SLICE 1 12', $
	'COLOR 3 0 89 20', $
	'TRANS 0 0' ]
	openr,1,filepath('jet.dat', subdir='images')
	a = bytarr(81,40,101,/nozero)
  ENDCASE
3: return
ENDCASE

readu,1,a		;Read it
close,1
slicer, GROUP = GROUP, COMMANDS = commands	;Do it
end

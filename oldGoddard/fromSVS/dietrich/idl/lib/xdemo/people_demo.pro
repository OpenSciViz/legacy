pro people_events, ev
common people_common, base, window, labels, names, hair

widget_control, ev.id, get_uvalue=b		;Get the event's value
if b eq 'Colors' then begin			;Load color tables?
	xloadct, group = ev.top
	return
	endif
if b eq 'Quit' then begin			;Done?
	widget_control, base, /destroy
	return
	endif
i= where(b eq names)				;Index of name

						;Load a new picture
openr,lun, /get, filepath('people.dat', subdir='images'), /BLOCK
;;;;openr,lun, /get, 'people.dat', /BLOCK

c = bytarr(192,192, /nozero)			;Define array
point_lun,lun, i(0) * 192L * 192L
readu,lun,c					;Read it
wset, window
tv,rebin(bytscl(c, min=0, max=220, top=!d.n_colors-1), 384,384)
widget_control, labels(0), set_value=b		;Set name label
widget_control, labels(6), set_value=hair(i(0))	;Set hair attribute
free_lun, lun
end

pro people_demo, group = group			;Main people procedure
common people_common, base, window, labels, names, hair


openr,lun, /GET, filepath('people.dat', subdir='images'), /BLOCK ;Get tables
; openr,lun, /GET, 'people.dat', /BLOCK ;Get tables

a = fstat(lun)
nimages = a.size / (192L * 192L)	;# of images
names = strarr(nimages)
hair = names
a = bytarr(40)			;Size of header...
for i=0,nimages-1 do begin
	point_lun, lun, i * (192L * 192L)
	readu, lun, a
	names(i) = string(a(0:19))
	hair(i) = string(a(20:39))
	endfor
close, lun


base = widget_base(title='RSI People', /row)	;Main widget
left = widget_base(base, /column, /frame)	;Left side buttons
  junk = widget_button(left, value='Quit', uvalue='Quit')
  junk = widget_button(left, value='Colors', uvalue='Colors')
  left = widget_base(left, /column, /frame, /exclusive)  ;People buttons
  for i=0,n_elements(names)-1 do $		;Make the people buttons
	junk = widget_button(left, Value = names(i), $
	   uvalue = names(i), /NO_RELEASE)

right = widget_base(base, /column, /frame)	;right side
helb24 = '*helvetica-bold-r*240*'		;Fonts we use
helb18 = '*helvetica-bold-r*180*'
helb14 = '*helvetica-bold-r*140*'
hel14 =  '*helvetica-medium-r*140*'

junk = widget_label(right, value='SLEAZO ASKS ...', $  ;Titles
	font=helb14)
junk1 = widget_label(right, value='HAVE YOU SEEN ME?', font=helb24)
right1 = widget_base(right, /row)		;Drawable & annotations
junk = widget_base(right1, /frame)		;Frame around window
draw = widget_draw(junk, xsize=384, ysize=384, retain=2) ;Image drawable
txt = widget_base(right1, /column)		;Annotations
			;Attributes:
label_names = ['NAME: ', 'DATE MISSING: ', 'DOB: ', 'AGE: ', 'HT: ', $
	'EYES: ', 'HAIR: ', 'WT: ', 'SEX: ', 'FROM: ']
			;Values
label_values = ['    ', 'Tuesday', 'Yesterday', 'Ancient', 'Short', $
	'Beady', 'Unkempt', 'Secret', 'Yes', 'Left Field' ]
labels = lonarr(n_elements(label_names))	;Make the labels on right
blanks = string(replicate(32b, 24))
for i=0,n_elements(labels)-1 do begin
	junk = widget_base(txt, /row)
	junk1 = widget_label(junk, value=label_names(i) , font=hel14)
	labels(i) = widget_label(junk, value=label_values(i), font=hel14)
	endfor

junk = widget_label(right, value='CALL 1-800-555-1212', font=helb24)
junk = widget_label(right, font=helb14, value = $
	'National Center for Missing and Exploited Programmers')
junk = widget_label(right, font=hel14, value = $
	'Over 50 programmers featured have been recovered.')


widget_control, /realize, base
widget_control, draw, get_value = window
wset, window
XManager, "people", base, EVENT_HANDLER = 'people_events', group = group
end


pro drawcirc, rot, lon0, color
;Draw a great circle with given rotation & offset

n = 360		;# of points
rota = rot * !DTOR		;Radians

t = findgen(n+1) * (2 * !PI/n)
sint = sin(t)
y = cos(t)
x = sint * sin(rota)
z = sint * cos(rota)
lat = asin(z) * !RADEG
lon = atan(x,y) * !RADEG + lon0
b = where(lon le (-180.), count)
if count ne 0 then lon(b) = lon(b) + 360.
b = where(lon gt 180., count)
if count ne 0 then lon(b) = lon(b)-360.

plots,lon,lat, color = color, thick=2
return
end



pro cir_2p, p1, p2   ;Connect two points, in the form of [lon, lat] with
		; a great circle
COMMON map_demo_com, projs, iproj, map_window, lat0, lon0, rot0, $
	cont_button, do_cont, cir , drawable, ll_label, lat_slider, $
	lon_slider, sat_params, sat_base, rot_slider,  city_pos, $
	great_cir, conn_two, mode_base


r_earth = 6371.007	;Radius of earth, KM
km_mile = 0.621		;Km per mile

p1r = p1 * !dtor	;To radians
p2r = p2 * !dtor

twopi = 2 * !pi
dlon = twopi + p2r(0) - p1r(0)			;delta longitude
while dlon gt !pi do dlon = dlon - twopi	;to -pi to +pi

; Great Circle Distance:
cosd = sin(p1r(1))*sin(p2r(1)) + cos(p1r(1))*cos(p2r(1))*cos(dlon)
dst = r_earth * acos(cosd)	;Distance in km

lon = [p1r(0), p2r(0)]
lat = [p1r(1), p2r(1)]
x = cos(lat) * sin(lon)		;To xyz space
y = cos(lat)* cos(lon)
z = sin(lat)

a = z(0) * y(1) - y(0) * z(1)	;Plane containing center of earth & 2 pnts
b = z(1) * x(0) - x(1) * z(0)	; aX + bY = Z

lon0 = -atan(b/a)		;Equator crossing
rot = atan(tan(lat(1)) / sin(lon(1) - lon0))
rot = 90 - rot * !radeg

cir.lon0 = !RADEG * lon0
cir.rot = rot

d = 'Dist = ' + STRING(dst, FORMAT='(i5)')+'km, ' + $  ;The distance label
		STRING(dst * km_mile, FORMAT = '(I5)') + 'mi'
WIDGET_CONTROL, ll_label, SET_VALUE = d
WIDGET_CONTROL, cir.crot_slider, SET_VALUE = cir.rot
WIDGET_CONTROL, cir.clon_slider, SET_VALUE = cir.lon0

cir.color = cir.color+1
if cir.color ge 32 then cir.color = 4
drawcirc, cir.rot, cir.lon0, cir.color
plots, p1(0), p1(1), psym=5		;Mark the points
plots, p2(0), p2(1), psym=5

end


FUNCTION city_mark, i		;Mark the Ith city, return [lon, lat]
COMMON map_demo_com, projs, iproj, map_window, lat0, lon0, rot0, $
	cont_button, do_cont, cir , drawable, ll_label, lat_slider, $
	lon_slider, sat_params, sat_base, rot_slider,  city_pos, $
	great_cir, conn_two, mode_base

bad = [1e12, 1e12]		;LL of city not on map
lon = city_pos.pos(1,i)
lat = city_pos.pos(0,i)

; Check lat/lon and draw only if on map
if (lat lt !map.out(4)) or (lat gt !map.out(5)) THEN RETURN, bad
d1 = ((lon - !map.out(2))+ 360) mod 360		;Min lon
d2 = ((!map.out(3) - !map.out(2))+ 360) mod 360	;Lon range (0 == all)
if (d2 ne 0) and (d1 gt d2) then RETURN, bad

p = convert_coord(lon, lat, /data, /to_device)
IF p(0) GT (!d.x_size-1) THEN RETURN, bad	;On screen?
plots, p(0), p(1), /device, psym=4
xyouts, p(0), p(1)- 3*!d.y_ch_size/4, /device, city_pos.names(i), ali=0.5
RETURN, [lon, lat]
END





pro map_demo_event, event

common colors, r_orig, g_orig, b_orig, r_curr, g_curr, b_curr

COMMON map_demo_com, projs, iproj, map_window, lat0, lon0, rot0, $
	cont_button, do_cont, cir , drawable, ll_label, lat_slider, $
	lon_slider, sat_params, sat_base, rot_slider,  city_pos, $
	great_cir, conn_two, mode_base


WIDGET_CONTROL, event.id, GET_UVALUE = eventval
s = size(eventval)

wset, map_window
if event.id eq drawable then begin	;Update lat/lon text widget?
	if event.press eq 0 then return
	if (!x.s(1) eq 0) or (!x.type ne 2) then return
	if iproj eq 7 then begin	;Satellite
		WIDGET_CONTROL, ll_label, set_value = [ $
		  'Inverse Satellite not', 'implemented. ']
		return
		endif
	p = convert_coord(event.x, event.y, /device, /to_data) ;Get coords
  set_ll:
	WIDGET_CONTROL, ll_label, set_value = [ $
		'Latitude  = ' + string(p(1),format='(F6.1)'), $
		'Longitude = ' + string(p(0),format='(F6.1)')]
	IF ABS(p(0)) gt 360 THEN RETURN	;Valid point?
	if cir.llflag eq 2 then begin	;2nd POINT?
		cir_2p, cir.ll, p(0:1)
		cir.llflag = 0
		ENDIF
	if cir.llflag eq 1 then begin	;Marking great circle?
		WIDGET_CONTROL, ll_label, SET_VALUE = $
		 'Mark second point.'
		cir.ll = p(0:1)
		cir.llflag = 2
		ENDIF
	return
endif					;Cursor hit on map

if s(1) eq 7 then CASE eventval OF	;String value?
	"LAT_SLIDER":	lat0 = event.value
	"LON_SLIDER":	lon0 = event.value
	"ROT_SLIDER":	rot0 = event.value
	"SALT"      :	sat_params(0) = event.value / 10.
	"SALPHA"    :   sat_params(1) = event.value
	"SBETA"	    :   sat_params(2) = event.value
	"CROT_SLIDER" : begin
			cir.rot = event.value
			return
			endcase
	"CLON_SLIDER":	begin
			cir.lon0 = event.value
			if cir.lon0 lt 0 then cir.lon0 = 180+cir.lon0
			return
			endcase
	"CIR_DRAW":  begin
			cir.color = cir.color+1
			if cir.color ge 32 then cir.color = 4
			drawcirc, cir.rot, cir.lon0, cir.color
			return
		     endcase
	"CIR_GTCIR" : begin
			cir.llflag = 1		;Expecting 1st point
			WIDGET_CONTROL, ll_label, SET_VALUE = $
			  'Mark first point.'
			return
			endcase
	"CITY_ALL" : BEGIN
		for i=0, n_elements(city_pos.names)-1 do p = city_mark(i)
		RETURN
		ENDCASE
	"CITY_SELECT": BEGIN
		p = city_mark(event.index)		;The item selected
		goto, set_ll
		ENDCASE
	"RESET" : begin			;Set center / rotation to 0.
		lat0 = 0
		lon0 = 0
		rot0 = 0
		WIDGET_CONTROL, LAT_SLIDER, SET_VALUE = lat0
		WIDGET_CONTROL, LON_SLIDER, SET_VALUE = lon0
		WIDGET_CONTROL, ROT_SLIDER, SET_VALUE = rot0
		endcase
	"EXIT" : begin 			;Adios!!  Restore prev color tbls
		IF N_ELEMENTS(r_curr) GT 0 THEN TVLCT, r_curr, g_curr, b_curr
		WIDGET_CONTROL, event.top, /DESTROY
		RETURN
		endcase
	"HELP" : begin			;Display help text
		XDisplayFile, FILEPATH("map_demo.txt", subdir='help'), $
			TITLE = "Map Demo Help", $
			GROUP = event.top, $
			WIDTH = 72, HEIGHT = 24
		return
		endcase
	"PARAMETERS": begin		;Show map parameters control panel
		WIDGET_CONTROL, mode_base(1), MAP=0
		WIDGET_CONTROL, mode_base(0), MAP=1
		return
		endcase
	"CITIES": begin			;Show city/circle control panel
		WIDGET_CONTROL, mode_base(0), MAP=0
		WIDGET_CONTROL, mode_base(1), MAP=1
		return
		endcase
	ELSE: MESSAGE, "Event user value not found"

endcase else begin		;Must be projection #
	if event.select eq 0 then return	;Ignore releases
	if eventval eq n_elements(projs) then begin ;Toggle continental outls
		do_cont =  1 - do_cont
		WIDGET_CONTROL, cont_button, SET_VALUE= $
		  "Turn Continental Outlines " + (["On", "Off"])(do_cont)
		return
		endif
	last_p = iproj
	iproj = eventval+1
	if iproj eq 11 then iproj = 14	;Adjust for sinusodial
	if last_p eq iproj then return	;Nothing to do?
	last_p = iproj
	if iproj eq 7 then begin	;Extras for satellite?
		IF (XRegistered("map_demo_circle")) THEN BEGIN
		  WIDGET_CONTROL, conn_two, SENSITIVE = 0
		  WIDGET_CONTROL, great_cir, SENSITIVE = 0
		ENDIF
		slide_wid = 250
		sat_base = lonarr(5)
		sat_base(0) = WIDGET_BASE( $
			title='Satellite Projection Parameters',/COLUMN)
		sat_base(1) = WIDGET_SLIDER(sat_base(0), $
			XSIZE = slide_wid, MINIMUM = 1, MAXIMUM = 100, $
			VALUE= sat_params(0)*10, TITLE = 'Altitude (R x 10)', $
			UVALUE = "SALT")
		sat_base(2) = WIDGET_SLIDER(sat_base(0), $
			XSIZE = slide_wid, MINIMUM = -180, MAXIMUM = 180, $
			VALUE= sat_params(1), TITLE = 'Alpha', $
			UVALUE = "SALPHA")
		sat_base(3) = WIDGET_SLIDER(sat_base(0), $
			XSIZE = slide_wid, MINIMUM = -180, MAXIMUM = 180, $
			VALUE= sat_params(2), TITLE = 'Beta', $
			UVALUE = "SBETA")
		WIDGET_CONTROL, sat_base(0), /REALIZE
		XManager, "map_demo_satellite", sat_base(0), $
			EVENT_HANDLER = "MAP_DEMO_EVENT", $
			GROUP_LEADER = event.top
		return
	ENDIF ELSE BEGIN	; projection not satellite
		IF (XRegistered("map_demo_circle")) THEN BEGIN
		  WIDGET_CONTROL, conn_two, SENSITIVE = 1
		  WIDGET_CONTROL, great_cir, SENSITIVE = 1
		ENDIF
		IF sat_base(0) NE 0 THEN BEGIN
			;Kill satellite base if active
			widget_info, WIDGET_ID = sat_base(0), VALID=temp
			if temp eq 1 then WIDGET_CONTROL, sat_base(0),/DESTROY
			sat_base(0) = 0
			ENDIF
	ENDELSE		
ENDELSE


lon1 = lon0
lat1 = lat0

;		Take care of special cases:
;
IF iproj EQ 3 THEN BEGIN	;conic?
	lat1 = (lat1 > (-85)) < 85	;Adjust ranges
	lon1 = (lon1 > (-85)) < 85
	if lat1 eq lon1 then $
		if lat1 ge 0 then BEGIN lat1=20 & lon1 = 60
		ENDIF ELSE BEGIN lat1 = -20 & lon1 = -60 & ENDELSE
	if (lat1 * lon1) lt 0 then goto, illgl_conic
	if abs(lon1-lat1) le 5 then begin
illgl_conic:  	WIDGET_CONTROL, ll_label, SET_VALUE = [ $
		  'Illegal values for Conic','  ']
		return
		endif
	WIDGET_CONTROL, LAT_SLIDER, SET_VALUE = lat1	;Update the sliders
	WIDGET_CONTROL, LON_SLIDER, SET_VALUE = lon1
	WIDGET_CONTROL, ll_label, SET_VALUE = [' ', ' ']
ENDIF
			
IF iproj EQ 7 THEN BEGIN	;satellite?  Fake it
	lat1 = 0
	lat0 = 0
	WIDGET_CONTROL, LAT_SLIDER, SET_VALUE = lat0
ENDIF

IF iproj EQ 14 THEN lat1 = 0	;Sinusoidal?

;**********	Draw the map....

if iproj eq 14 then j = 11 else j = iproj	;Kludge

wset, map_window
tek_color		;Load graphics style color tables

IF iproj EQ 3 THEN BEGIN	;Conic
	if (lat1 lt 0) or (lon1 lt 0) THEN limit = [-80,-180,0,180] $
	else limit = [0,-180,80,180]
ENDIF ELSE limit = 0		;Let map_set make limits for others

map_set, lat1, lon1, rot0, PROJ = iproj, cont=do_cont, $
	sat_p = sat_params, con_color = 2, mlinethick=1, $
	title = projs(j-1), limit = limit

temp = !map.out
if !map.out(4) eq (-90) then !map.out(4) = -75	;Don't do polar regions
if !map.out(5) eq 90 then !map.out(5) = 75
map_grid, latdel = 15, londel = 15, color=3
!map.out = temp			;Restore limits

RETURN
END




PRO map_demo, GROUP = GROUP
COMMON map_demo_com, projs, iproj, map_window, lat0, lon0, rot0, $
	cont_button, do_cont, cir , drawable, ll_label, lat_slider, $
	lon_slider, sat_params, sat_base, rot_slider,  city_pos, $
	great_cir, conn_two, mode_base



IF XRegistered("map_demo") THEN RETURN		;only one instance

projs = ["Stereographic", "Orthographic", "Conic", "Lambert's Equal Area", $
	"Gnomonic", "Azimuthal Equidistant", "Satellite", $
	"Cylindrical", "Mercator", "Mollweide", "Sinusoidal" ]

iproj = 1
xsize = 768
ysize = xsize * 4 / 5
sliderwidth = 200
lat0 = 0
lon0 = 0
rot0 = 0
do_cont = 1
cir = { CIRCLE_PARAMS, base : 0L, lon0 : 0.0, rot : 0.0, color : 4, $
	crot_slider : 0L, clon_slider : 0L, ll : [0., 0.], llflag : 0 }

sat_params = [1., 0, 0]  ;Salt, salpha, sbeta
sat_base = lonarr(5)

map_demobase = WIDGET_BASE(title = "Map Demo")
row_base = WIDGET_BASE(map_demobase, /ROW)
l_base = WIDGET_BASE(row_base, /COLUMN)		;Left side widgets
r_base = WIDGET_BASE(row_base, /COLUMN)		;Right side

drawable = WIDGET_DRAW(r_base, XSIZE = xsize, YSIZE = ysize, $
		RET=2, /BUTTON_EVENTS)

XPdMenu, [	'" Done "			EXIT',		$
		'" Reset "			RESET',		$
		'" Help "			HELP'		$
	],  l_base

XPdMenu, [  '"Map Parameters"			PARAMETERS', $
	    '"Cities/Circles"			CITIES' $
	],  l_base

mode_base = lonarr(2)				;Bases for modes
junk = WIDGET_BASE(l_base)
for i=0,1 do  mode_base(i) = WIDGET_BASE(junk, $
		uvalue=0L, /COLUMN)

p_base = WIDGET_BASE(mode_base(0), /COLUMN, /FRAME)
junk = WIDGET_LABEL(p_base, VALUE = "Projection Type:")
p_base = WIDGET_BASE(p_base, /COLUMN, /EXCLUSIVE)
for i=0, n_elements(projs)-1 do begin
	junk = WIDGET_BUTTON(p_base, uvalue = i, VALUE=projs(i))
	endfor

cont_button = widget_button(mode_base(0), uvalue = n_elements(projs), $
	VALUE = "Turn Continental Outlines Off")
lat_slider = WIDGET_SLIDER(mode_base(0), $
	XSIZE = sliderwidth, MINIMUM = -90, MAXIMUM = 90, VALUE = lat0, $
	TITLE = 'Center Latitude', uvalue = "LAT_SLIDER")
lon_slider = WIDGET_SLIDER(mode_base(0), $
	XSIZE = sliderwidth, MINIMUM = -180, MAXIMUM = 180, VALUE = lon0, $
	TITLE = 'Center Longitude', uvalue = "LON_SLIDER")
rot_slider = WIDGET_SLIDER(mode_base(0), $
	XSIZE = sliderwidth, MINIMUM = -90, MAXIMUM = 90, VALUE = rot0, $
	TITLE = 'Rotation', uvalue = "ROT_SLIDER")


; **** Cities base widget ***

IF N_ELEMENTS(city_pos) LE 0 THEN BEGIN		;Already got city data?
	OPENR, unit, FILEPATH('cities.dat', subdir = "maps"), /GET_LUN
	city_names = strarr(100)
	city_pos = fltarr(2,100)
	i = 0
	x=0. & y=0. & z = ''
	WHILE NOT EOF(unit) DO BEGIN
		readf,unit, x, y, z
		city_names(i) = z
		city_pos(0,i) = x	;Latitude
		city_pos(1,i) = y
		i = i + 1
		endwhile
	FREE_LUN, unit
	;	Now, correct for the file being in degrees.minutes
	city_pos = city_pos(*,0:i-1)
	icity = fix(city_pos)
	fcity = city_pos - fix(city_pos)	;Decimal fractions
	city_pos = icity + (fcity * (100./60.)) ;Cvt minutes to 100ths
	
	city_pos = { CITY_POS, names : strtrim(city_names(0:i-1),2), $
		pos : city_pos }
	ENDIF


junk = WIDGET_LIST(mode_base(1), VALUE = city_pos.names, $
	YSIZE = 10, UVALUE = "CITY_SELECT")
junk = WIDGET_BUTTON(mode_base(1), uvalue = "CITY_ALL", $
	value = "Mark All Cities")
				;	*** Great circle ***
great_cir = WIDGET_BUTTON(mode_base(1), uvalue = "CIR_DRAW", $
	VALUE = "Draw Great Circle")
conn_two = WIDGET_BUTTON(mode_base(1), uvalue = "CIR_GTCIR", $
	value = "Connect two points")

junk = WIDGET_BASE(mode_base(1), /COLUMN)
junk1 = WIDGET_LABEL(junk, VALUE = "Great Circle Parameters")
cir.crot_slider = WIDGET_SLIDER(junk, $
	XSIZE = sliderwidth, MINIMUM = 0, MAXIMUM = 180, VALUE = cir.rot, $
	TITLE = 'Inclination', uvalue = "CROT_SLIDER")
cir.clon_slider = WIDGET_SLIDER(junk, $
	XSIZE = sliderwidth, MINIMUM = -179, MAXIMUM = 179, VALUE = cir.lon0, $
	TITLE = 'Equatorial Crossing', uvalue = "CLON_SLIDER")


widget_control, mode_base(1), MAP=0
; *****

ll_label = WIDGET_TEXT(l_base, XSIZE = 23, YSIZE=2, /FRAME, $
		VALUE = ['Click Mouse on map','for inverse transforms'])

WIDGET_CONTROL, map_demobase, /REALIZE
WIDGET_CONTROL, drawable, get_value = map_window


XManager, "map_demo", map_demobase, $
	EVENT_HANDLER = map_demo_events, $
	GROUP_LEADER = group
end

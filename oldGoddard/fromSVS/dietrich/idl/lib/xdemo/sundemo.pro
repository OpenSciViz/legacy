; Copyright (c) 1991, Research Systems, Inc.  All rights reserved.
;	Unauthorized reproduction prohibited.
;
;	SunDemo
;
;		This example is meant to show how IDL and widgets can
;	be used to display images. In this case, images of the sun.
;	The SunDemo illustrates how to create more than widget at the
;	same time of the same type by using the user value of the base
;	widget to contain the structure that describes each instance of
;	the widget.
;
;

PRO sundemo_event, event

WIDGET_CONTROL, GET_UVALUE = instance, $	; first get hold of a copy of
		event.top			; the instance structure of 
						; this widget
WIDGET_CONTROL, GET_UVALUE = retval, event.id	; and get the value of the 
						; child where the event occured

CASE retval OF					; for each of the view types,
						; make sure that the view isnt
						; already visible and if not
						; draw the view and reset the
						; instance structure to reflect
						; the changes

    "SUNINT": IF(instance.drawing_type NE "SUNINT") THEN BEGIN
		instance.drawing_type = "SUNINT"
		WSET, instance.sundraw
		TV, instance.int_image
		WIDGET_CONTROL, instance.sunlbl, $
			SET_VALUE = "Intensity Image of the Sun"
		WIDGET_CONTROL, instance.sunint, $
			SENSITIVE = 0
		WIDGET_CONTROL, instance.sunvel, $
			SENSITIVE = 1
		WIDGET_CONTROL, SET_UVALUE = instance, event.top
	      END

    "SUNVEL": IF(instance.drawing_type NE "SUNVEL") THEN BEGIN
		instance.drawing_type = "SUNVEL"
		WSET, instance.sundraw
		TV, instance.vel_image
		WIDGET_CONTROL, instance.sunlbl, $
			SET_VALUE = "Velocity Image of the Sun"
		WIDGET_CONTROL, instance.sunint, $
			SENSITIVE = 1
		WIDGET_CONTROL, instance.sunvel, $
			SENSITIVE = 0
		WIDGET_CONTROL, SET_UVALUE = instance, event.top
	      END

    "SUNHELP": XDisplayFile, filepath("sundemo.txt", subdir='help'), $
			TITLE = "Sun Demo Help", $
			GROUP = event.top, $
			WIDTH = 55, $
			HEIGHT = 16

    "EXIT": WIDGET_CONTROL, event.top, /destroy
    ELSE:
ENDCASE

END


;------------------------------------------------------
;   procedure sundemo
;------------------------------------------------------

PRO sundemo, GROUP = GROUP

size = 400

XGetData, intensity, FILENAME = "sunint.dat"
XGetData, velocity, FILENAME = "sunvel.dat"

sun_str  = {sds, vel_image:bytarr(size,size), $	; this structure is created for
		 int_image:bytarr(size,size), $	; each instance of the demo
		 sundraw:0, drawing_type:"", $	; and is stored in the bases
		 sunint:0L, sunvel:0L, $	; user value
		 sunlbl:0L}

sunbase = WIDGET_BASE(TITLE = "Sun Image Example", $
		/COLUMN, $
		XPAD = 20, $
		YPAD = 20, $
		SPACE = 10)

sunmenu = WIDGET_BASE(sunbase, /ROW)
sunexit = WIDGET_BUTTON(sunmenu, $
		VALUE = "Exit Sun Example", $
		UVALUE = "EXIT")
sun_str.sunint = WIDGET_BUTTON(sunmenu, $
		VALUE = "Intensity", $
		UVALUE = "SUNINT")
sun_str.sunvel = WIDGET_BUTTON(sunmenu, $
		VALUE = "Velocity", $
		UVALUE = "SUNVEL")
sunexit = WIDGET_BUTTON(sunmenu, $
		VALUE = "Help", $
		UVALUE = "SUNHELP")

sundrawbase = WIDGET_BASE(sunbase, $
		/COLUMN, $
		/FRAME)

sun_str.sunlbl = WIDGET_LABEL(sundrawbase, $
		VALUE = "Intensity Image of the Sun")

sunpicture = WIDGET_DRAW(sundrawbase, $		; then create the drawable 
			XSIZE = size, $		; widget for the sun images
			YSIZE = size, $
			RETAIN = 2)

sun_str.int_image = REBIN(intensity, size, size)
sun_str.vel_image = REBIN(velocity, size, size)

WIDGET_CONTROL, sun_str.sunint, SENSITIVE = 0	; desensitize the current view

WIDGET_CONTROL, sunbase, /REALIZE		; realize the widgets

TV, sun_str.int_image				; draw the first image

sun_str.sundraw = !D.WINDOW			; get the idl window number for
						; future updates to this 
						; instance and set the drawing
sun_str.drawing_type = "SUNINT"			; type

WIDGET_CONTROL, sunbase, SET_UVALUE = sun_str	; record the structure for this
						; instance in its base

xmanager, "sundemo", sunbase, $			; register the base with the 
	  GROUP_LEADER = GROUP			; widget manager

END ;================ procedure sundemo ====================

; Copyright (c) 1991, Research Systems, Inc.  All rights reserved.
;	Unauthorized reproduction prohibited.

PRO xexample_bck, topid

;this routine does the background action when the XManager calls it because
;the event loop is idle.  It does little more than moving a scrollbar.

widget_control, topid, get_uvalue = slideid, BAD_ID = BAD
if (BAD ne 0) then goto, finish
widget_control, slideid, get_value = temp, BAD_ID = BAD
if (BAD ne 0) then goto, finish
if temp ge 91 then temp = 0 else temp = temp + 10
widget_control, slideid, set_value = temp, BAD_ID = BAD
finish:

END


PRO xexample_event, event

;this routine just destroys the XExample widget if the user has pressed the 
;done button.  It is the event handler routine that the XManager calls when
;a widget event occurs for this widget.

WIDGET_CONTROL, event.id, GET_UVALUE = USERVALUE


case USERVALUE of
  "DONE": WIDGET_CONTROL, event.top, /DESTROY
  else:
endcase

END

;+
; NAME:
;	XEXAMPLE
; PURPOSE:
;		This example is intended to show each of the widget
;	types possible with IDL.  They do nothing of interest but
;	show what is available in widgets using the XManager.  This 
;	example demonstrates the capacity to have multiple copies of
;	one widget type.  It also demonstrates background tasks which 
;	are things that happen when the XManager is waiting for events.
;
; CATEGORY:
;	Widgets
; CALLING SEQUENCE:
;	XEXAMPLE
; KEYWORD PARAMETERS:
;	GROUP = The widget ID of the widget that calls xexample.  When this
;		ID is specified, a death of the caller results in a death of
;		xexample.
; OUTPUTS:
; OPTIONAL OUTPUT PARAMETERS:
; COMMON BLOCKS:
; SIDE EFFECTS:
;	Initiates the XManager if it is not already running.
; RESTRICTIONS:
; PROCEDURE:
;	Create and register the widget and then exit.
; MODIFICATION HISTORY:
;	Created from a template written by: Steve Richards,	January, 1991
;-

PRO xexample, GROUP = GROUP

;this is the main XExample routine that creates all the widgets it uses and 
;registers the widgets with the Xmanager.

base = WIDGET_BASE(TITLE = "Example Base", $
			/COLUMN, $
			XPAD = 10, YPAD = 10, $
			SPACE = 10)

label = WIDGET_LABEL(base, VALUE = "Example Label")

menubutton = WIDGET_BUTTON(base, $
			VALUE = "Example Pop Up Menu", $
			MENU = 2, $
			UVALUE = "MENU")
listsize = 10
listlabels = strarr(listsize)
for i = 0, listsize - 1 do begin
  listlabels(i) = "list item number " + string(i)
  toss = WIDGET_BUTTON(menubutton, $
			VALUE = listlabels(i), $
			UVALUE = string(i))
endfor

slideid = WIDGET_SLIDER(base, $
			MAXIMUM = 100, $
			MINIMUM = 0, $
			VALUE = 0, $
			UVALUE = "BACKGROUND", $
			/SUPPRESS_VALUE)

label = WIDGET_LABEL(base, VALUE = "Background Event")

listsize = 10
listlabels = strarr(listsize)
for i = 0, listsize - 1 do $
  listlabels(i) = "list item number " + string(i)
list = WIDGET_LIST(base, VALUE = listlabels, $
		   ysize = listsize / 2, $
		   UVALUE = "LIST")

draw = WIDGET_DRAW(base, YSIZE = 150, $
			RETAIN = 2, $
			COLOR = 50)

text = WIDGET_TEXT(base, YSIZE = 3, $
	VALUE = "Example Editable Text", $
			/EDITABLE, $
			/SCROLL, $
			XSIZE = 25, $
			UVALUE = "TEXT")

button = WIDGET_BUTTON(base, $
			VALUE = "Sample Button", $
			UVALUE = "BUTTON")

slide = WIDGET_SLIDER(base, UVALUE = "SLIDER", $
			TITLE = "Example Slider")

donebut = WIDGET_BUTTON(base, VALUE = "Done Button", $
			UVALUE = "DONE")

WIDGET_CONTROL, base, /REALIZE

!X.margin = [0,0] & !Y.margin = [0,0]
plot, [0,3,1,2,1,3,0]
xyouts, 40, 125, "Example Draw", /DEVICE
xyouts, 48, 105, "   Widget", /DEVICE

WIDGET_CONTROL, base, SET_UVALUE = slideid

xmanager, "xexample", base, $				;XExample is registered
		GROUP_LEADER = GROUP, $			;for uses by the 
		BACKGROUND = "xexample_bck"		;XManager.

END
pro box_cursor, x0, y0, nx, ny, INIT = init, FIXED_SIZE = fixed_size, $
	MESSAGE = message
;+
; NAME:
;	BOX_CURSOR
;
; PURPOSE:
;	Emulate the operation of a variable-sized box cursor (also known as
;	a "marquee" selector).
;
; CATEGORY:
;	Interactive graphics.
;
; CALLING SEQUENCE:
;	BOX_CURSOR, x0, y0, nx, ny [, INIT = init] [, FIXED_SIZE = fixed_size]
;
; INPUTS:
;	No required input parameters.
;
; OPTIONAL INPUT PARAMETERS:
;	x0, y0, nx, and ny give the initial location (x0, y0) and 
;	size (nx, ny) of the box if the keyword INIT is set.  Otherwise, the 
;	box is initially drawn in the center of the screen.
;
; KEYWORD PARAMETERS:
;	INIT:  If this keyword is set, x0, y0, nx, and ny contain the initial
;	parameters for the box.
;
;	FIXED_SIZE:  If this keyword is set, nx and ny contain the initial
;	size of the box.  This size may not be changed by the user.
;
;	MESSAGE:  If this keyword is set, print a short message describing
;	operation of the cursor.
;
; OUTPUTS:
;	x0:  X value of lower left corner of box.
;	y0:  Y value of lower left corner of box.
;	nx:  width of box in pixels.
;	ny:  height of box in pixels. 
;
;	The box is also constrained to lie entirely within the window.
;
; COMMON BLOCKS:
;	None.
;
; SIDE EFFECTS:
;	A box is drawn in the currently active window.  It is erased
;	on exit.
;
; RESTRICTIONS:
;	Works only with window system drivers.
;
; PROCEDURE:
;	The graphics function is set to 6 for eXclusive OR.  This
;	allows the box to be drawn and erased without disturbing the
;	contents of the window.
;
;	Operation is as follows:
;	Left mouse button:   Move the box by dragging or clicking.
;	Middle mouse button: Resize the box by dragging or clicking.
;	Right mouse button:  Exit this procedure, returning the 
;			     current box parameters.
;
; MODIFICATION HISTORY:
;	DMS, April, 1990.
;-

device, get_graphics = old, set_graphics = 6  ;Set xor

if keyword_set(message) then begin
	print, "Left button to move box."
	print, "Middle button to resize box."
	print, "Right button when done."
	endif

if keyword_set(init) eq 0 then begin  ;Supply default values for box:
	if keyword_set(fixed_size) eq 0 then begin
		nx = !d.x_size/8   ;no fixed size.
		ny = !d.x_size/8
		endif
	x0 = !d.x_size/2 - nx/2
	y0 = !d.y_size/2 - ny/2
	endif

x00 = x0
y00 = y0
old_corner = (corner = 0)
goto, middle

while 1 do begin
	cursor, x, y, 2, /dev	;Wait for a button
	if !err eq 1 then begin  ;New corner?
		if corner and 1 then x00 = (x - nx) > 0 else $
			x00 = x
		if (corner and 2) ne 0 then y00 =  (y-ny) > 0 else $
			y00 = y
		x0 = x00 & y0 = y00
		endif
	if (!err eq 2) and (keyword_set(fixed_size) eq 0) then begin ;New size?
		corner = 0
		if x le x00 then begin
			x0 = x & x1 = x00
		endif else begin
			x0 = x00 & x1 = x & corner = 1	;Right side
		endelse
		if y le y00 then begin
			y0 = y & y1 = y00
		endif else begin
			y0 = y00 & y1 = y   & corner = corner + 2
		endelse
		nx = abs(x0-x1) > 1
		ny = abs(y0-y1) > 1
		endif
	plots, px, py, col=255, /dev	;Erase previous box
	if old_corner ne corner then empty	;Decwindow bug
	old_corner = corner
	if !err eq 4 then begin  ;Quitting?
		device,set_graphics = old
		return
		endif
middle:
	x0 = x0 < (!d.x_size-1 - nx)	;Never outside window
	y0 = y0 < (!d.y_size-1 - ny)

	px = [x0, x0 + nx, x0 + nx, x0, x0] ;X points
	py = [y0, y0, y0 + ny, y0 + ny, y0] ;Y values

	plots,px, py, col=255, /dev  ;Draw the box
	wait, .1		;Dont hog it all
	endwhile
end

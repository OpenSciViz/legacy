Function Defroi, Sx, Sy, Xverts, Yverts, X0=x0, Y0=y0, ZOOM = ZOOM, $
	NOREGION = Noregion

;+
; NAME:
;	DEFROI
;
; PURPOSE:
;	Define an irregular region-of-interest of an image
;	using the image display system and the cursor/mouse.
;
; CATEGORY:
;	Image processing.
;
; CALLING SEQUENCE:
;	Result = DEFROI(Sx, Sy, [Xverts, Yverts])
;
; INPUTS:
;	Sx, Sy:  X and Y size of image, in pixels.
;
; KEYWORD PARAMETERS:
;	X0, Y0:	Device coordinates of the lower-left corner of the image on
;		display.  If omitted, (0,0) is assumed.
;
;	ZOOM:	Zoom factor.  If omitted, 1 is assumed.
;
;	NOREGION:  Inhibits the return of the pixel subscripts.
;
; OUTPUTS:
;	This function returns a vector containing the subscripts of pixels 
;	inside the selected region.  Note that these are "linear" subscripts,
;	not "X, Y" pairs.  See the EXAMPLE below.
;
;	Side effect: The lowest bit in which the write mask is enabled
;	is changed.
;
; OPTIONAL OUTPUTS:
;	Xverts, Yverts:  Optional output parameters that contain
;	the vertices enclosing the region.  Setting the keyword
;	NOREGION inhibits the return of the pixel subscripts.
;
; COMMON BLOCKS:
;	COLORS:	Used to obtain the current color table which is modified
;		and then restored.
;
; SIDE EFFECTS:
;	For this implementation, bit 0 of each pixel is used to draw
;	the outline of the region.  You WILL have to change this
;	to fit the capabilities and procedures of your display.
;
; RESTRICTIONS:
;	Originally written for the DT-2651.
;	Modified for Sun by DMS.
;
; PROCEDURE:
;	The write mask for the display is set so that only bit 0 may be 
;	written.  Bit 0 is erased for all pixels.  The color tables are loaded
; 	with odd values complemented and even values unchanged.
;
;	A message is printed that describes the effect of the three mouse 
;	buttons.  The operator marks the vertices of the region using the 
;	mouse.
;
;	When the operator is finished, the region is filled using 
;	the TVPOLY function, and the polyfillv function is used
;	to compute the subscripts within the region.
;
; EXAMPLE:
;	This example shows how you can "cut out" a region of an image using 
;	DEFROI and then display that region.  First, create an image and 
;	display it by entering:
;
;		D = DIST(512)
;		TV, D
;
;	Now use DEFROI to mark any region of the image that you want.  Enter:
;
;		CUTOUT = DEFROI(512, 512)
;
;	Move the mouse cursor over the image.  Mark points with the left 
;	button.  To close the region, press the right button.
;	Erase the drawing window by entering:
;
;		ERASE
;
;	Make a new, empty array the same size as the previous image. Enter:
;
;		NEW = BYTARR(512, 512)
;
;	CUTOUT contains a list of the selected pixels.  Put only the selected
;	pixels from the original image D into the image NEW.  Enter:
;
;		NEW(CUTOUT) = D(CUTOUT)
;
;	Display the new image:
;
;		TV, NEW
;
; MODIFICATION HISTORY:
;	DMS, March, 1987.
; 	Revised for SunView, DMS, Nov, 1987.
;-
common colors,orig_red, orig_green, orig_blue,red,green,blue
;
on_error,2		;Return to caller if error
nc = !d.table_size	;# of colors available
if nc eq 0 then message, 'Device has static color tables, Won''t work.'

if n_elements(red) le 0 then begin ;Define bw table?
	red = indgen(nc) & green = red & blue = red & endif
;
if n_elements(x0) le 0 then x0 =0
if n_elements(y0) le 0 then y0 =0
if n_elements(zoom) le 0 then zoom = 1
;
device,get_write = old_mask	;inhibit writing all bits except 1

if !d.n_colors gt 256 then $ ;Special case for true color?
	new_mask = 1L * (1 + 256 + 256L*256) $
else begin
	i = 0L
	while ((old_mask and 2^i) eq 0) do i=i+1
	new_mask = 2^i		;Mask we want for our roi bit
	endelse

device,set_write= new_mask		;Set new write mask
erase			;Erase bottom bit for all pixels.
;
;	Set up color tables where the odd elements are for the ROI,
;	and the evens are unchanged.
;
odd = where((indgen(nc) and new_mask) ne 0) ;Odd subscripts

rr=red & gg=green & bb=blue	;save color tables
rr(odd) = 255
gg(odd) = 255
bb(odd) = 0

tvlct,rr,gg,bb			;load new table with odd elements diddled.
;
again:
erase	;Clear overlay
n = 0
print,'Left button to mark point'
print,'Middle button to erase previous point'
print,'Right button to close region'
maxpnts = 1000			;max # of points.
xverts = intarr(maxpnts)		;arrays
yverts = intarr(maxpnts)
xprev = -1
yprev = -1
;
tvrdc,xx,yy,1,/dev		;Get 1st point with wait
repeat begin
	xx = (xx - x0) / zoom	;To image coords
	yy = (yy - y0) / zoom
	if (xx ge 0) and (xx lt sx) and (yy ge 0) and (yy lt sy) $
	and (!err eq 1) and ((xx ne xprev) or (yy ne yprev)) then begin
		xprev = xx
		yprev = yy
		if n ge (maxpnts-1) then begin
			print,'Too many points'
			n = n-1
			endif
		n = n + 1
		xverts(n-1) = xx	;In pixel coords
		yverts(n-1) = yy
		if n gt 1 then $
		  plots,xverts(n-2:n-1)*zoom+x0,yverts(n-2:n-1)*zoom + y0, $
			/dev,color=new_mask,/nocl
		endif
	if (!err eq 2) and (n gt 0) then begin
		n = n-1
		if n gt 0 then begin
		 plots,xverts(n-1:n)*zoom+x0,yverts(n-1:n)*zoom+y0,color=0,$
			/dev,/noclip
		endif
		wait,.1		;dont erase too quickly
	  endif
	tvrdc,xx,yy,0,/dev	;get x,y, no wait, device coords.
	endrep until !err eq 4

if n lt 3 then begin
	print,'ROI - Must have 3 points for region.  Try again.'
	goto,again
	endif
xverts = xverts(0:n-1)		;truncate
yverts = yverts(0:n-1)
plots,[xverts(0),xverts(n-1)]*zoom+x0,[yverts(0),yverts(n-1)]*zoom+y0,$
	/dev,color = new_mask,/noclip ;Complete polygon

if !order ne 0 then yverts = sy-1-yverts	;Invert Y?
device,set_write = old_mask	;Re-enable writing to all bit planes.
empty			;Sun view mask handling is flakey.
tvlct,red,green,blue	;Restore color tables
if keyword_set(noregion) then a = 0 $
  else a = polyfillv(xverts,yverts,sx,sy)	; get subscripts inside area.

return,a
end











pro zoom,xsize=xs, ysize=ys, fact = fact, interp = interp, continuous = cont
;+
; NAME:	
;	ZOOM
;
; PURPOSE:
;	Display part of an image (or graphics) from the current window
;	enlarged in another window.
;
;	The cursor is used to mark the center of the zoom.
;
; CATEGORY:
;	Image display.
;
; CALLING SEQUENCE:
;	ZOOM [, FACT = Fact, /INTERP, XSIZE = Xs, YSIZE = Ys, /CONTINUOUS]
;
; INPUTS:
;	All input parameters are passed as keywords.
;
; KEYWORDS:
;	FACT:	Zoom factor.  This parameter must be an integer.  The default
;		zoom factor is 4.
;
;	INTERP:	Set this keyword to use bilinear interpolation, otherwise 
;		pixel replication is used.
;
;	XSIZE:	The X size of the zoom window.  The default is 512.
;
;	YSIZE:	The Y size of the zoom window.  The default is 512.
;
;   CONTINUOUS:	Set this keyword to make the zoom window track the mouse
;		without requiring the user to press the left mouse button.
;		This feature only works well on fast computers.
;
; OUTPUTS:
;	No explicit outputs.   A new window is created and destroyed when
;	the procedure is exited.
;
; COMMON BLOCKS:
;	None.
;
; SIDE EFFECTS:
;	A window is created/destroyed.
;
; RESTRICTIONS:
;	ZOOM only works with color systems.
;
; PROCEDURE:
;	Straightforward.
; 
; MODIFICATION HISTORY:
;	?
;-
on_error,2              ;Return to caller if an error occurs
if n_elements(xs) le 0 then xs = 512
if n_elements(ys) le 0 then ys = 512
if n_elements(fact) le 0 then fact=4
if keyword_set(cont) then waitflg = 2 else waitflg = 3
ifact = fact
old_w = !d.window
zoom_w = -1		;No zoom window yet
tvcrs,1			;enable cursor
ierase = 0		;erase zoom window flag
print,'Left for zoom center, Middle for new zoom factor, Right to quit'
again:
	tvrdc,x,y,waitflg,/dev	;Wait for change
	case !err of
4:	goto, done
2:	if !d.name eq 'SUN' or !d.name eq 'X' then begin	;Sun view?
		s  = ['New Zoom Factor:',strtrim(indgen(19)+2,2)]
		ifact = wmenu(s, init=ifact-1,title=0)+1
		tvcrs,x,y,/dev	;Restore cursor
		ierase = 1
	endif else begin
		Read,'Current factor is',ifact+0,'.  Enter new factor: ',ifact
		if ifact le 0 then begin
			ifact = 4
			print,'Illegal Zoom factor.'
			endif
			ierase = 1	;Clean out previous display
	endelse
else:	begin
	x0 = 0 > (x-xs/(ifact*2)) 	;left edge from center
	y0 = 0 > (y-ys/(ifact*2)) 	;bottom
	nx = xs/ifact			;Size of new image
	ny = ys/ifact
	nx = nx < (!d.x_vsize-x0)
	ny = ny < (!d.y_size-y0)
	x0 = x0 < (!d.x_vsize - nx)
	y0 = y0 < (!d.y_vsize - ny)
	a = tvrd(x0,y0,nx,ny)		;Read image
	if zoom_w lt 0 then begin	;Make new window?
		window,/free,xsize=xs,ysize=ys,title='Zoomed Image'
		zoom_w = !d.window
	endif else begin
		wset,zoom_w
		if ierase then erase		;Erase it?
		ierase = 0
	endelse
	xss = nx * ifact	;Make integer rebin factors
	yss = ny * ifact
	tv,rebin(a,xss,yss,sample=1-keyword_set(interp))
	wset,old_w
	endcase
endcase
goto,again

done:
if zoom_w ge 0 then wdelete,zoom_w		;Done with window
end



pro mapp_set, latmin, latmax, lonmin, lonmax, p0lon,p0lat,rot,iproj, $
	      border = border, title = title, sat_p = sat_p

if iproj eq 3 then begin		;Conic?
	!map.phioc = rot		;Parameter order is diff
	if (p0lat + p0lon)/2. lt 0.0 then !map.sino = -1 else !map.sino = 1
	chi1 = (90. - !map.sino * p0lat) * !dtor
	if p0lat eq p0lon then begin
		!map.coso = cos(chi1)
	   endif else begin
		chi2 = (90. - !map.sino * p0lon) * !dtor
		!map.coso = alog(sin(chi1)/sin(chi2))/ $
			alog(tan(0.5*chi1)/tan(0.5*chi2))
	   endelse
        !map.out(8)=p0lat*!dtor
        !map.out(9)=p0lon*!dtor
	goto, get_bounds
endif

;		Not conic, all other projections

x1 = rot * !dtor
x2 = p0lat * !dtor

if(iproj ge 8) then  BEGIN
 !map.out(8) = x1
 !map.out(11) = x2
ENDIF ELSE !map.out(8)=x2

!map.out(9)=p0lon*!dtor
sinr = sin(x1)
cosr = cos(x1)
sino = sin(x2)
coso = cos(x2)
del1 = .0001
!map.sat = 0.0		;Clear satellite proj params

if iproj ge 8 and iproj lt 14 then begin
                               	;Cylindrical projections
  if (abs(p0lat) ge del1) or $	;Simple case?
	(abs(rot) ge del1)       $
     and (abs(abs(rot)-180.) ge del1)  then begin
	sint = coso * cosr	;NO...
	cost = sqrt(1.-sint*sint)
	tmp1 =  sinr / cost
	tmp2 =  sino / cost
	!map.phioc = p0lon - atan(tmp1, - cosr * tmp2) * !radeg
	sinr = tmp1 * coso
	cosr = -tmp2
	sino = sint
	coso = cost
  endif else begin		;Simple cases
	if abs(rot) lt del1 then sino = 1.0 else begin 
	   sino = -1.
	   !map.phioc = p0lon + 180.
	   endelse
	coso = 0.0
	sinr = 0.0
	cosr = 1.0
	!map.projection = iproj+3	;Make a simple projection
  endelse
endif			;Cylindrical projections

if iproj eq 7 then begin	;Special params for satellite projection.
	!map.sat(0) = sat_p(0)	;Salt = sat_p(0)
		;Save em
	!map.sat(1) = sqrt(sat_p(0) * sat_p(0) - 1.)  ;srss
	!map.sat(2) = sin(!dtor * sat_p(1))	;Salpha = sat_p(1)
	!map.sat(3) = cos(!dtor * sat_p(1))
	!map.sat(4) = sin(!dtor * sat_p(2))	;Sbeta = sat_p(2)
	!map.sat(5) = cos(!dtor * sat_p(2))
	endif	

!map.coso =  coso
!map.sinr = sinr
!map.sino = sino
!map.cosr = cosr
if(iproj eq 2) then !map.sat(0) =0;


get_bounds:

;	  stereo	ortho	     conic
bounds = [[-2,-2,2,2], [-1,-1,1,1], [-1,-1,1,1]]
;			lamb	gnomic		azimuthal
bounds = [[bounds], [-2,-2,2,2],[-2,-2,2,2],[-!pi,-!pi,!pi,!pi]]
;			satell	cylindrical	mercator
bounds = [[bounds], [-1,-1,1,1],[-180,-90,180,90],[-!pi,-!pi,!pi,!pi]]
;			mollweide  Sinusoidal
bounds = [[bounds], [-2,-1,2,1], [-!pi,-!pi/2,!pi,!pi/2]]

iproj1 = (iproj < 11) - 1
umin = bounds(2,iproj1)         ;MAXIMUM USEFUL AREA 
vmin = bounds(3,iproj1)
umax = bounds(0,iproj1)
vmax = bounds(1,iproj1)

xmin = 1
xmax = 0
ymin = 1
ymax = 0

latdel = (latmax-latmin)/10. 

if latdel gt 10. THEN BEGIN  ;We dont want stepsizes too large
del = float(fix(latmax-latmin)/10)
latdel  = (latmax-latmin)/del
ENDIF

londel = (lonmax - lonmin)/10.
if londel gt 10. THEN BEGIN
del = float(fix(lonmax - lonmin)/10)
londel  = (lonmax - lonmin)/del
ENDIF
eps = 1.e-5 

latlim = latmax + (latmax-latmin)*eps
lonlim = lonmax + (lonmax-lonmin)*eps

;Find max u and v coordinates
for lat =float(latmin),latlim,latdel  do $  
    for lon = float(lonmin),lonlim, londel do begin
        p00 = convert_coord(lon,lat,/to_norm)
        p0  = !map.out(0:1)
        if p0(0) LT 1.e12 and p0(1) LT 1.e12 THEN BEGIN
          umin = umin < p0(0)
          umax = umax > p0(0)
          vmin = vmin < p0(1)
          vmax = vmax > p0(1)
          endif
    endfor

  if iproj eq 1 or iproj eq 5 then  BEGIN
     umin = -2 > umin
     vmin = -2 > vmin
     umax = 2  < umax 
     vmax = 2 < vmax
  ENDIF


if iproj eq 3 then  BEGIN 
    ueps = 135.              	   ;For conical
    umin=-1 & umax=1
endif  else ueps = 0.75 * (umax-umin)
veps = 0.75 * (vmax-vmin)

!map.out(6) = ueps
!map.out(7) = veps
 

if n_elements(limit) eq 10 then begin	;Specify lower left upper right corners
  	p0 = convert_coord(limit(1),limit(0),/to_norm)
	p0 = !map.out(0:1)
	p1 = convert_coord(limit(3),limit(2),/to_norm)

	p1 = !map.out(0:1)
	umax = p0(0) > p1(0)
	umin = p0(0) < p1(0)
	vmax = p0(1) > p1(1)
	vmin = p0(1) < p1(0)
endif else if n_elements(limit) eq 8 then begin ;4 points specified?
	s = size(limit)
	if (s(0) ne 2) or (s(1) ne 2) or (s(2) ne 4) then begin
		print,'MAP_SET - illegal dimensions for limit'
		return
		endif
	p0 = fltarr(2,4)		;for the 4 corners
	for i=0,3 do begin
		p1 = convert_coord(limit(1,i),limit(0,i),/to_norm)
		p0(0,i) = !map.out(0:1)
		endfor
	umin = min(p0(0,*))
	umax = max(p0(0,*))
	vmin = min(p0(1,*))
	vmax = max(p0(1,*))
endif
if not N_Elements(title) THEN title =" "
plot,[umin,umax],[vmin,vmax],xsty=5, ysty=5, /nodata,    $
      xmargin=[1,1], $
      ymargin=[1,2], title=title, font=0,/noerase

if keyword_set(border) then  $
	plots ,!x.window([0,1,1,0,0]),    $
	       !y.window([0,0,1,1,0]), /norm, /noclip

!x.type=2			;Set transform to map type
return
end



pro map_grid ,label=label,  latdel = latdel,$
       londel=londel, glinestyle = glinestyle, glinethick = glinethick,   $
       color = color
                                       ;Draw a grid
if (!x.type NE 2) THEN GOTO,DONE  ; make sure we have mapping coordinates
latmin = !map.out(4)
latmax = !map.out(5)
lonmin = !map.out(2)
lonmax = !map.out(3)

if N_Elements(glinestyle) EQ 0 THEN glinestyle =1
if N_Elements(glinethick) EQ 0 THEN glinethick =1
if n_elements(latdel) eq 0 then latdel = (latmax - latmin)/10. < 10	
                                 ; Delta = grid spacing.
if n_elements(londel) eq 0 then $
	 londel = (lonmax-lonmin)/10. < 10 ;Delta = grid spacing.

if n_elements(color) le 0 then begin	;Default color?
	if (!d.flags and 512) ne 0 then color = 0 else color = !d.n_colors-1
	endif

if N_Elements(label) NE 0 THEN printno = 1  $
else printno = -1                  ; a flag to signal printing
                                    ; of grid numbers
if n_elements(step) le 0 then step = 4 < (latmax - latmin)/10.

len = (latmax-latmin) / step + 1

lati = findgen(len) * step + latmin

if lati(len-1) ne latmax THEN BEGIN
lati = [lati,latmax]
len = len + 1
ENDIF

First = 1

for lon = lonmin, lonmax, londel do begin   

    if (lon lt -180) then lon2 =lon +360  $
    else if (lon gt 180) then lon2 = -360 +lon $
    else lon2 = lon
    slon = strtrim(string(lon2,format='(i4)'),2)
    pres = convert_coord(lon,latmin,/to_norm)
    pres = !map.out(0:1)
    pres1 = convert_coord(lon,latmax,/to_norm)
    pres1 = !map.out(0:1)
    lon1 = lon
    if First eq 1  THEN First = 0 else $
      if abs(pres(0) - past(0)) GE !map.out(6)   OR  $
         abs(pres(1) - past(1)) GE !map.out(7)   OR  $
         abs(pres1(0) - past1(0)) GE !map.out(6) OR  $
         abs(pres1(1) - past1(1)) GE !map.out(7)     $
      THEN BEGIN
       if(lon ge 0) then dd = .0001 else dd = -.0001
       lon1 = lon - dd
      ENDIF  
    past = pres
    past1 = pres1
    loni = Replicate(lon1,len)  
    plots,loni,lati,NOCLIP=0,linestyle =glinestyle,thick = glinethick, $
	color = color

    if printno eq 1 then xyouts,lon,      $
       (latmin+latmax)/2., ali=0.5,$
        strtrim(string(lon2,format='(i4)'),2)
    printno  = 1- printno
 endfor

  !map.out(3) = lonmax 

if N_Elements(label) NE 0 THEN printno = 1 $
  else printno = -1

step = 4 < (lonmax - lonmin)/10.
len = (lonmax-lonmin)/step + 1
loni = findgen(len)*step + lonmin


if (loni(len-1) NE lonmax) THEN  BEGIN
    loni = [loni, lonmax]
    len = len + 1
ENDIF

for lat = float(latmin), latmax, latdel do begin

   if printno eq 1 then xyouts,(lonmin+lonmax)/2, lat, ali=0.5, $
		strtrim(string(lat, format='(i4)'),2)
   printno = 1 - printno
   lati = Replicate(lat,len)
   plots,loni,lati,NOCLIP=0,linestyle=glinestyle,color = color, $
         thick=glinethick
 endfor
    !map.out(5) = latmax

return
DONE: print,$
'map_grid---Current ploting device must have mapping coordinates'
return
end



pro map_continents,  $
	mlinestyle = mlinestyle, color = color, $
	mlinethick = mlinethick, US = us, CONTINENTS = cont

if (!x.type NE 2) THEN GOTO,DONE  ; make sure we have mapping coordinates
latmin = !map.out(4)
latmax = !map.out(5)
lonmin = !map.out(2)
lonmax = !map.out(3)

if N_Elements(mlinestyle) EQ 0 THEN mlinestyle=0
if N_Elements(mlinethick) EQ 0 THEN mlinethick=1
if n_elements(color) le 0 then begin	;Default color?
	if (!d.flags and 512) ne 0 then color = 0 else color = !d.n_colors-1
	endif

maxlat=0. & minlat=0. & maxlon=0. & minlon=0.
close,1
openr, lun, /get, FILEPATH('supmap.dat',subdir = "maps"),/xdr,/stream
npts = 0L
; 	cont us_only  both
fbyte = [ 0, 71612L, 165096L]
nsegs = [ 283, 325, 594 ]


if n_elements(us) eq 0 then us = 0
if n_elements(cont) eq 0 then cont = 0

ij = 0			;Default = continents only
if us ne 0 then begin
	if cont then ij = 2 else ij = 1
	endif
point_lun, lun, fbyte(ij)

for i=1,nsegs(ij) do begin
	READU, lun, npts,maxlat,minlat,maxlon,minlon
	npts = npts / 2		;# of points
	xy = fltarr(2,npts)
	READU, lun, xy
	if (maxlat lt latmin) or (minlat gt latmax) then goto,skipit
	
	if (maxlon lt lonmin) or (minlon gt lonmax) then BEGIN
          if (lonmax gt 180 and maxlon + 360 ge lonmin) then goto,goon
          if ( lonmin lt -180 and minlon -360 le lonmax) then goto,goon
          goto, skipit
        END
     

goon:	plots, xy(1,*), xy(0,*), NOCLIP=0, $
		THICK=mlinethick,linestyle=mlinestyle, color = color

skipit:

       	endfor
FREE_LUN, lun
return
DONE:
print, $
'map_grid---Current ploting device must have mapping coordinates'
end



pro checkparam, latmin, latmax, lonmin, lonmax, p0lon,p0lat, limit ,  $
                iproj, Rot

NOLIMIT = 0.0
if (N_Elements(limit) NE 4) then BEGIN
  if (iproj NE 3) THEN BEGIN
    lonmin = -180.0 + p0lon
    lonmax  = 180.0 + p0lon
    latmax  = 90 
    latmin = -90 
  ENDIF ELSE BEGIN
        lonmin = -180 + P0lon
        lonmax = 180 + P0lon
    ENDELSE
  NOLIMIT = 1
ENDIF

if (lonmin EQ -180 AND lonmax EQ 180) THEN BEGIN
  if iproj ne 3 THEN BEGIN
    lonmin = -180 + P0lon
    lonmax = 180 + P0lon
  ENDIF ELSE BEGIN
    lonmin = -180 + Rot
    lonmax = 180 + rot
  ENDELSE
ENDIF

case 1 of

 iproj eq 1 or iproj eq 2 or iproj eq 4 :BEGIN 

        if (NOLIMIT eq 1) and p0lat eq 0.0 then BEGIN
                                     ; center on equator
           lonmax = p0lon + 90
           lonmin = p0lon - 90
        ENDIF

        if (NOLIMIT eq 1) and p0lat eq 90.0   then  begin   ;center at a pole
            latmin = 0.0 
            latmax = 90.0
          ENDIF else if (NOLIMIT eq 1) and p0lat eq -90.0 then BEGIN
                      latmin = -90.0
                      latmax = 0.0
                    ENDIF
    END
 iproj eq 5: BEGIN
  if (NOLIMIT eq 1) and p0lat eq 0.0 then BEGIN
                                     ; center on equator
           lonmax = p0lon + 60
           lonmin = p0lon - 60
           latmin = p0lat - 60
           latmax = p0lat + 60
  ENDIF
  if (NOLIMIT eq 1) and p0lat eq 90.0  then  begin
     latmin = 30
     latmax = 90.0
   ENDIF else if (NOLIMIT eq 1) and p0lat eq -90.0 then BEGIN
                latmin = -90.0
                latmax = -30.0
                ENDIF
    END

 iproj eq 3: BEGIN
               if NOLIMIT eq 1 THEN BEGIN
                latmin = p0lat < p0lon
                latmax = p0lon >p0lat
                lonmin = -180 + Rot
                lonmax = 180  + Rot
                ENDIF
                END

 iproj eq 9 or iproj eq 12: if NOLIMIT eq 1 THEN BEGIN
		latmin = -80
		latmax = 80
		ENDIF
          
      ELSE:
ENDCASE
RETURN
END

           
        
         
              
  



pro map_set, p0lat, p0lon, rot, proj = proj, $
        Query = interg,                $
	CYLINDRICAL = cyl,      MERCATOR = merc,  $
	MOLLWEIDE = moll,       STEREOGRAPHIC = stereo,  $
        ORTHOGRAPHIC = ortho,   Conic = cone,            $
	LAMBERT = lamb,         GNOMIC = gnom,           $
        AZIMUTHAL = azim,       SATELLITE = satel,       $
        SINUSOIDAL = sinu,                               $
  latdel = latdel, londel = londel, limit = limit,       $
  Sat_P = Sat_p, title = title, noborder = noborder,        $
  noerase = noerase, label = label, $
  glinestyle = glinestyle, glinethick=glinethick,       $
  mlinestyle=mlinestyle, mlinethick=mlinethick,		$
  color = color, con_color = con_color,                 $
  continents = continent, grid = grid

;+
; NAME:
;      map_set
; PURPOSE:
;        The procedure map_set establishes
;        the axis type and coordinate conversion mechanism
;        for mapping  points on the earth's surface, expressed
;        in latitude and longitude, to points on a plane, according
;        to one of ten possible map projections. The user may 
;        may select the map projection, the map center, polar rotation
;        and geographical limits.
;
;        The user may request map_set to plot the
;        graticule and continental boundaries by setting
;        the keywords Grid and Continent.
;
; CATEGORY:
;           Mapping
; CALLING SEQUENCE:
;                 map_set, p0lat, p0lon, rot, proj = proj,            $
;                    Query = interg,                                  $
;              	     CYLINDRICAL = cyl,      MERCATOR = merc,         $
;   	             MOLLWEIDE = moll,       STEREOGRAPHIC = stereo,  $
;                    ORTHOGRAPHIC = ortho,   Conic = cone,            $
;   	             LAMBERT = lamb,         GNOMIC = gnom,           $
;                    AZIMUTHAL = azim,       SATELLITE = satel,       $
;                    SINUSOIDAL = sinu,                               $
;                    latdel = latdel, londel = londel, limit = limit, $
;                    Sat_p = Sat_p, $
;                     title = title, noborder = noborder, $
;                    noerase = noerase, label = label,                $
;                    glinestyle = glinestyle, glinethick=glinethick,  $
;                    mlinestyle=mlinestyle, mlinethick=mlinethick,    $
;                    color = color, con_color = con_color, 	      $
;		     continents = continent, grid = grid
;
; OPTIONAL INPUT:
;               P0lat--- For all but Lambert's conformal conic projection
;                        with two standard parallels, P0lat should be set
;                        to the latitude of the point on the earth's surface
;                        to be mapped to the center of the projection plane.
;                        If the projection type is sinusoidal, P0lat must be
;                        0. -90 <= P0lat <= 90. If P0lat is not set, the
;                        default value is 0. If the user has selected
;                        Lambert's conformal conic projection with two
;                        standard parallels, P0lat should be set to the
;                        latitude in degrees of one of the parallels.
;                        If not both P0lat and P0lon are defined, P0lat is
;                        20 by default.
;
;               P0lon--- For all but Lambert's conformal conic projection with
;                        two standard parallels, P0lon should be set
;                        by the user to the longitude of the point on the
;                        earth's surface to be mapped to the center of
;                        the map projection. -180 <= P0lon <= 180. If P0lon
;                        is not set, the default value is zero.
;
;              Rot-----  The user should set the argument Rot to the angle
;                        through which the North direction should be
;                        rotated around the line L between the earth's center
;                        and the point (P0lat, P0lon). Rot is measured in 
;                        degrees with the positive direction being clockwise
;                         rotation around L. Default value is 0.
;
; KEYWORDS:
;         Azimuthal     = azimuthal equidistant projection
;         Conic         = Conic projection
;         Cylindrical   = cylindrcal equidistant projection
;         Gnomic        = gnomonic projection
;         Lambert       = Lambert's equal area projection
;         Mercator      = Mercator's projection
;         Mollweide     = Mollweide type projection
;         Orthographic  = Orthographic projection
;         Sinusoidal    = sinsoidal projection
;         Stereographic = stereographic projection
;
;         Color         = color of the map boundary.
;	  con_color	= color of continents.
;         Continents    = flag to signal that continental boundaries
;                         should be drawn.         
;         Glinestyle    = linestyle of gridlines. (default = 1 = dotted)
;         Glinethick    = thickness of gridlines. (default = 1 = normal)
;         Grid          = flag to signal that gridlines should be drawn.
;         Label         = flag to signal labeling of parallels and 
;                         meridians in the grid.
;                         
;         Latdel        = if set, spacing of parallels
;                         drawn on grid. Default is 
;                         minimum(10, (latmin - latmax) / 10).
;         Limit         =  A four element vector = [latmin, lonmin, latmax,
;                          lonmax] specifying the boundaries of the region
;                          to be mapped. (latmin, lonmin) and (latmax,
;                          lonmax) are the latitudes and longitudes of
;                           two diagonal points on the boundary with
;                           latmin < latmax and lonmin <lonmax.
;        Londel         = if set, spacing of meridians drawn on grid.
;        Mlinethick     = thickness of continental boundaries. Default is 1.
;        Mlinestyle     = linestyle of continental boundaries. Default is
;                         0 = normal.
;        Noborder       = if set, no border is drawn around the map.
;        Noerase        = flag to signal that map_set should not erase the
;                         current plot window. Default is to erase.
; 
;       Title           = Title of plot to be centered over plot window.
;
; Modification history:
;			Written, Anne Bateson, 1991
;	SMR, 10-'91	Added a check for additional satelite parameters
;			   when /SATELITE is set.
;
;-
if (N_ELements(interg) eq 0) THEN BEGIN                ; DONT INTERROGATE 

IF(KEYWORD_SET(satel)) THEN BEGIN
  IF(NOT(KEYWORD_SET(sat_p))) THEN $
    MESSAGE, "Satellite parameters must be set for satellite projection."
ENDIF

if n_elements(proj) ne 0 then iproj = proj $        
else if keyword_set(stereo) then iproj =1		$
else if keyword_set(ortho ) then iproj =2		$
else if keyword_set(cone  ) then iproj =3		$
else if keyword_set(lamb  ) then iproj =4		$
else if keyword_set(gnom  ) then iproj =5		$
else if keyword_set(azim  ) then iproj =6		$
else if keyword_set(satel ) then iproj =7		$
else if keyword_set(merc  ) then iproj =9		$
else if keyword_set(moll  ) then iproj =10		$
else if keyword_set(sinu  ) then iproj =14              $
else iproj=8		                         	;Assume cylindrical

if keyword_set(noborder) eq 0 then border =1
if (n_elements (limit) ne 4) then NOLIM = 1              $
else BEGIN

     latmin = limit(0)
     latmax = limit(2)
     lonmin = limit(1)
     lonmax = limit(3)                        
     if latmin GE latmax or lonmin GE lonmax then BEGIN
       print,'map_set--- minimum latitude and longitude must be smaller than'
       print,'maximum latitude and longitude'
       return
     ENDIF
     NOLIM  = 0
ENDELSE

if n_elements(rot) eq 0 then rot = 0.
if n_elements(p0lat) eq 0 then  $
  if iproj NE 3 THEN p0lat = 0.0 $
    else  BEGIN 
          p0lat = 20
          p0lon = 60
    ENDELSE
if n_elements(p0lon) eq 0 then   $
 if iproj NE 3 THEN p0lon = 0.0 $
 ELSE BEGIN
     p0lon = 60
     p0lat = 20
 ENDELSE
ENDIF

if (iproj eq 14) THEN BEGIN
  if N_Elements(P0lat) NE 0 THEN $
    if P0lat NE 0 THEN  $
      print,'map_set--- Sinusoidal projection must have P0lat = 0'
  if N_Elements(Rot) NE 0 THEN  $
    if Rot NE 0 THEN   $
     print,'map_set--- Sinusoidal projection must have Rot = 0'

 p0lat = 0
 rot   = 0
endif

if N_Elements(interg) ne 0 THEN BEGIN
iproj = 0
read,'Projection number ',iproj
read,'p0lat, p0lon, rot:  ',p0lat,p0lon,rot
read,'latmin, latmax: ', latmin, latmax
read,'lonmin, lonmax: ', lonmin, lonmax
ENDIF



!map.projection = iproj
!x.type = 2
!map.phioc = p0lon

if not N_Elements(noerase) THEN erase
checkparam, latmin, latmax, lonmin, lonmax, p0lon, p0lat, limit,    $
            iproj, Rot

!map.out(2) = lonmin
!map.out(3) = lonmax
!map.out(4) = latmin
!map.out(5) = latmax

mapp_set, latmin, latmax, lonmin, lonmax, p0lon,p0lat,rot,iproj,   $
         border=border, title =title, sat_p = sat_p


if keyword_set(grid) then $		;Grid?
	map_grid,label=label, latdel=latdel,$
          londel = londel , glinestyle = glinestyle, $
          glinethick = glinethick, color = color

if keyword_set(continent) then begin		;Continents?
	if n_elements(con_color) le 0 then begin
	    if n_elements(color) gt 0 then con_color = color $
		else begin
		  if (!d.flags and 512) ne 0 then color = 0 $
		    else color = !d.n_colors-1
		endelse
	    endif		;con_color
	map_continents, color = con_color, $
		mlinestyle = mlinestyle, mlinethick = mlinethick  
	endif
RETURN
END




FUNCTION DETERM,ARRAY
;+
; NAME:
;	DETERM
;
; PURPOSE:
;	Calculate the determinant of a square matrix
;
; CATEGORY:
;	F3 - Determinants.
;
; CALLING SEQUENCE:
;	Result = DETERM(Array)
;
; INPUTS:
;	Array:  A square input array of any type except string
;
; OUTPUTS:
;	Returns the determinant of Array.
;
; COMMON BLOCKS:
;	None.
;
; SIDE EFFECTS:
;	None.
;
; PROCEDURE:
;	Standard determinant formula.
;
; MODIFICATION HISTORY:
; 	VERSION 1, created by I. Dean Ahmad, August 10, 1979
;-
;
;
ON_ERROR,2              ;Return to caller if an error occurs
S=SIZE(ARRAY)
S1=S(1)-1
IF (S(1) NE S(2)) OR (S(0) NE 2) THEN message, 'Array is not square.'

;  Make floating if not dbl
if s(s(0)+1) ne 4 then ARR = float(ARRAY) else arr = array
DET=1.
FOR K=0,S1 DO BEGIN
;
; INTERCHANGE COLUMNS IF DIAGONAL ELEMENT IS 0
;
    IF ARR(K,K) EQ 0 THEN BEGIN
      J=K
      WHILE (J LT S1) AND (ARR(K,J) EQ 0) DO J=J+1
;
; IF MATRIX IS SINGULAR SET DET=0 AND END PROCEDURE
;
      IF ARR(K,J) EQ 0. THEN RETURN,0.  ;ZERO DETERMINANT
;
;  IF NONZERO DIAGONAL ELEMENT FOUND, SWAP
;
    SAVE = ARR(K:*,J)	;SWAP
    ARR(K,J)=ARR(K:*,K)
    ARR(K,K)=SAVE
    DET=-DET	; COLUMN SWAP CHANGES SIGN OF DETERMINANT
    END ; ARR(K,K)
;
;SUBTRACT ROW K FROM LOWER ROWS TO GET DIAGONAL MATRIX
;
  ARRKK=ARR(K,K)
  DET=DET*ARRKK
    IF K LT S1 THEN BEGIN	; IF NOT AT LAST ROW, PROCEED
      K1=K+1
      FOR I=K1,S1 DO begin
	s = arr(i,k)/arrkk
	arr(i,k1) = arr(i,k1:*) - s * arr(k,k1:*)
	endfor
      END ; K LT S1
  END ; K
RETURN,DET
END


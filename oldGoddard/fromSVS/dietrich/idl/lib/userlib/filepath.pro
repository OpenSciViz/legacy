FUNCTION FILEPATH, FILENAME, SUBDIRECTORY=subdir, $
	TERMINAL = TERMINAL, TMP = TMP
;+
; NAME:
;	FILEPATH
;
; PURPOSE:
;	Given the name of a file in the IDL distribution,
;	FILEPATH returns the fully-qualified path to use in
;	opening the file. Operating system dependencies
;	are taken into consideration. This routine is used by RSI to
;	make the User Library portable.
;
; CATEGORY:
;	File Management.
;
; CALLING SEQUENCE:
;	Result = FILEPATH('filename' [, SUBDIRECTORY = subdir]) 
;
; INPUTS:
;    filename:	The lowercase name of the file to be opened. No device
;		or directory information should be included.
;
; KEYWORDS:
; SUBDIRECTORY:	The name of the subdirectory in which the file
;		should be found. If this keyword is omitted, the main 
;		directory is used.  This variable can be either a scalar 
;		string or a string array with the name of each level of 
;		subdirectory depth represented as an element of the array.
;
;		For example, to get a path to the file DETERM in the "userlib"
;		subdirectory to the IDL "lib" subdirectory, enter:
;
;		path = FILEPATH("determ", SUBDIRECTORY = ["lib", "userlib"])
;
;    TERMINAL:	Return the filename of the user's terminal.
;
;	  TMP:	The file is a scratch file.  Return a path to the
;		proper place for temporary files under the current operating
;		system.
;
; OUTPUTS:
;	The fully-qualified file path is returned.  If one of the subdirectory
;	keywords is not specified, the file is assumed to exist in the
;	main distribution directory.
;
; COMMON BLOCKS:
;	None.
;
; RESTRICTIONS:
;	Don't specify more than one of the keywords in a single call.
;
;
; EXAMPLE:
;	To get a path to the file DETERM in the "userlib" subdirectory to the 
;	IDL "lib" subdirectory, enter:
;
;		path = FILEPATH("determ", SUBDIRECTORY = ["lib", "userlib"])
;
;	The variable "path" contains a string that is the fully-qualified file
;	path for the file DETERM.
;
; MODIFICATION HISTORY:
;	December, 1989, AB, RSI (Formalized from original by DMS)
;	October, 1990, SG, RSI (added support for MSDOS)
;	February, 1991, SMR, RSI (added string array support for multi-level
;				  directories)
;-


ON_ERROR,2		                        ;Return to caller if an error 
						;occurs

do_tmp = KEYWORD_SET(TMP)			;get temporary path if existing
path = ''

IF (KEYWORD_SET(TERMINAL)) THEN BEGIN
  CASE !VERSION.OS OF
    'vms': RETURN,'SYS$OUTPUT:'
    'DOS': RETURN,'con'
    ELSE: RETURN,'/dev/tty'
  ENDCASE
ENDIF

IF (do_tmp) THEN BEGIN
  CASE !VERSION.OS OF
    'vms': root = 'SYS$LOGIN:'
    'DOS': root = '\tmp'
    ELSE: root = '/tmp'
  ENDCASE
ENDIF ELSE BEGIN
  root = !DIR
  IF (KEYWORD_SET(SUBDIR)) THEN BEGIN		;if the SUBDIR keyword is set
    SUBDIR = STRLOWCASE(SUBDIR)			;then include each level in the
    CASE !VERSION.OS OF				;path separated by the correct
      'vms': divider = "."			;directory level delimiter for
      'DOS': divider = "\"			;the current O.S.
      ELSE: divider = "/"
    ENDCASE
    FOR i = 0, N_ELEMENTS(SUBDIR) - 1 DO BEGIN
	path = path + SUBDIR(i)
	IF(i NE N_ELEMENTS(SUBDIR) - 1) THEN $
	  path = path + divider
    ENDFOR
  ENDIF
ENDELSE

CASE !VERSION.OS OF
  'vms': BEGIN
      IF (NOT do_tmp) THEN BEGIN
        IF (path EQ '') THEN path = '000000'
        path = '[' + path + ']'
      ENDIF
    END
  'DOS': BEGIN
      path = '\' + path
      IF (path NE '\') THEN path = path + '\'
    END
  ELSE: BEGIN
      path = '/' + path
      IF (path NE '/') THEN path = path + '/'
    END
ENDCASE
RETURN, root + path + filename

END

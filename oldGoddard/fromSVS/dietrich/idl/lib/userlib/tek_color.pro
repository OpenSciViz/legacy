pro tek_color
;+
; NAME:
;	TEK_COLOR
;
; PURPOSE:
;	Load a color table similar to the default Tektronix 4115 color table.
;
; CATEGORY:
;	Graphics.
;
; CALLING SEQUENCE:
;	TEK_COLOR
;
; INPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	None.
;
; OUTPUTS:
;	No explicit outputs.
;
; COMMON BLOCKS:
;	None.
;
; SIDE EFFECTS:
;	The first 32 elements of the color table are loaded with
;	the Tektronix 4115 default color map.
;
; RESTRICTIONS:
;	None.
;
; PROCEDURE:
;	Just copy the 32 colors.  This table is useful for the
;	display of graphics in that the colors are distinctive.
;
; MODIFICATION HISTORY:
;	DMS, Jan, 1989.
;-
on_error,2                      ;Return to caller if an error occurs
r = bytscl([ 0,100,100,0,0,0,100,100,100,60,0,0,55,100,33,67, $
	100,75,45,17,25,50,75,100,67,40,17,17,17,45,75,90])
g = bytscl([ 0,100,0,100,0,100,0,100,50,83,100,50,0,0,33,67, $
	100,100,100,100,83,67,55,33,90,90,90,67,50,33,17,9])
b = bytscl([ 0,100,0,0,100,100,83,0,0,0,60,100,83,55,33,67, $
	33,45,60,75,83,83,83,90,45,55,67,90,100,100,100,100])
tvlct,r,g,b
end

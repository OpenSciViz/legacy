function congrid, image, xs, ys, interp=int ;VAX IDL CONGRID function
;+
; NAME:
;	CONGRID
;
; PURPOSE:
;	Shrink or expand the size of an image by an arbitrary amount.
;	This IDL procedure simulates the action of the VAX/VMS 
;	CONGRID/CONGRIDI function.
;
; CATEGORY:
;	Image processing.
;
; CALLING SEQUENCE:
;	Result = CONGRID(Image, Xs, Ys [, INTERP = Interp])
;
; INPUTS:
;	Image:  The 2D array to resample.
;	Xs:     The number of columns for the result.
;	Ys:     The number of rows for the result.
;
; KEYWORD PARAMETERS:
;	INTERP: Keyword that, if set, causes bilinear interpolation to be
;		used.  Otherwise, the nearest-neighbor method is used.  
;
; OUTPUTS:
;	Returns an image of the same type as input, of size (Xs, Ys).
;
; COMMON BLOCKS:
;	None.
;
; SIDE EFFECTS:
;	None.
;
; RESTRICTIONS:
;	This routine doesn't completely emulate the VAX/VMS CONGRID.  The case 
;	of a rectangular grid of control points is not implemented. This 
;	method can be simulated using multiple calls to POLY_2D.
;
; PROCEDURE:
;	Calls POLY_2D with the warping coefficients.
;	Given an input image A of dimensions (m,n), the result of
;	B = CONGRID(A, p, q) is:
;		B(i,j) = G(A(i*m/p, j*n/q))
;	where G is the interpolation function, either nearest neighbor
;	or bilinear interpolation.
;
; EXAMPLE:
;	To resize the 100 by 100-element image "im" to be 140 by 300 elements
;	and store the result in the variable "newim", enter the command:
;		newim = CONGRID(im, 140, 300, /INTERP)
;		
; MODIFICATION HISTORY:
;	DMS, Sept. 1988.
;-

on_error,2              ;Return to caller if an error occurs

if n_params() gt 3 then begin ;Cant warp over small rectangles
	print,"CONGRID - Can't warp over control grid."
	return,undef
	endif

s = size(image)
if s(0) ne 2 then begin		;2d image?
	print,'CONGRID - Image parameter not 2 dimensional'
	return,undef
	endif

if n_elements(int) le 0 then int = 0 ;Default = no interp

return, poly_2d(image, [[0,0],[s(1)/float(xs),0]], $ ;Use poly_2d
	  [[0,s(2)/float(ys)],[0,0]],int,xs,ys)
end

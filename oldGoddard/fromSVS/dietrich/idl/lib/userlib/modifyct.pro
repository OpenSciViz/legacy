PRO MODIFYCT, ITAB, NAME, R, G, B	;MODIFY COLOR TABLE IN FILE
;+
; NAME:
;	MODIFYCT 
;
; PURPOSE:
;	Update the distribution color table file "colors.tbl" with a 
;	new table.
;
; CATEGORY:
;	Z4 - Image processing, color table manipulation.
;
; CALLING SEQUENCE:
;	MODIFYCT, Itab, Name, R, G, B
;
; INPUTS:
;	Itab:	The table to be updated, numbered from 0 to 15.  The table 
;		can be loaded with the IDL command:  LOADCT, Itab.
;
;	Name:	A string up to 32 characters long that contains the name for 
;		the new color table.
;
;	R:	A 256-element vector that contains the values for the red
;		color gun.
;
;	G:	A 256-element vector that contains the values for the green
;		color gun.
;
;	B:	A 256-element vector that contains the values for the blue
;		color gun.
;
; OUTPUTS:
;	None.
;
; COMMON BLOCKS:
;	None.
;
; SIDE EFFECTS:
;	The distribution file "colors.tbl" is modified with the new table.
;
; PROCEDURE:
;	Straightforward.
;
; MODIFICATION HISTORY:
;	Aug, 1982, DMS, Written.
;	Unix version, 1987, DMS.
;-
  ON_ERROR,2                    ;Return to caller if an error occurs
  IF (ITAB LT 0)OR(ITAB GT 15) THEN message,'Color table number out of range.'

  GET_LUN,IUNIT		;GET A LOGICAL UNIT
  OPENU,IUNIT, FILEPATH('colors.tbl'),/BLOCK  ;OPEN FILE in IDL directory
  AA=ASSOC(IUNIT,BYTARR(32,16))	;UPDATE NAME RECORD.
  a = aa(0)
  a(0,itab) = byte(name)		;Store name converted to bytes
  aa(0)=a				;Write names out

  AA=ASSOC(IUNIT,BYTARR(512))	;UPDATE VECTORS.
  AA(ITAB*2+1) = [R,G]		;PUT RED AND GREEN IN 1ST
  AA(ITAB*2+2) = B		;BLUE IN 2ND BLOCK
  FREE_LUN,IUNIT
  RETURN
END

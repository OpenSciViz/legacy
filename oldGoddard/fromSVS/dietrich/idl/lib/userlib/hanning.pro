function Hanning, N1, N2 ;Hanning window function, 1 or 2 d.
;+
; NAME:
;	HANNING
;
; PURPOSE:
;	Window function for Fourier Transform filtering.
;
; CATEGORY:
;	Signal, image processing.
;
; CALLING SEQUENCE:
;	Result = HANNING(N1) ;For 1 dimension.
;
;	Result = HANNING(N1, N2) ;For 2 dimensions.
;
; INPUTS:
;	N1:	The number of columns of the result.
;
;	N2:	The number of rows of the result.
;
; OUTPUTS:
;	Result(i) = 1/2 [1 - COS(2 PI i / (N-1)]
;
;	For two dimensions, the result is the same except that "i" is replaced
;	with "i*j", where i and j are the row and column subscripts.
;
; COMMON BLOCKS:
;	None.
;
; SIDE EFFECTS:
;	None.
;
; RESTRICTIONS:
;	None.
;
; PROCEDURE:
;	Straightforward.
;
; MODIFICATION HISTORY:
;	DMS, May, 1987.
;-

on_error,2                              ;Return to caller if an error occurs
a = 2 * !pi / (N1 -1)			;scale factor
If n_params(0) eq 1 then begin		;1d?
	return,.5*(1.-cos(findgen(N1)*a))
   endif else begin			;2d case
	b = 2 * !pi / (n2-1)		;dim 2 scale fact
	row = .5*(1.-cos(findgen(n1)*a))	;One row
	col = .5*(1.-cos(findgen(n2)*b))	;One column
	RETURN,row # col
   endelse
end

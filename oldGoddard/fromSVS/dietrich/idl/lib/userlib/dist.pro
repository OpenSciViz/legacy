function dist,n	;Return a square array in which each pixel = euclidian
		;distance from the corner.
;+
; NAME:
;	DIST
;
; PURPOSE:
;	Create a square array in which each element is proportional
;	to its frequency.  This array may be used for a variety
;	of purposes, including frequency-domain filtering and
;	making pretty pictures.
;
; CATEGORY:
;	Signal Processing.
;
; CALLING SEQUENCE:
;	Result = DIST(N)
;
; INPUTS:
;	N = size of result.
;
; OUTPUTS:
;	Returns an (N,N) floating array in which:
;
;	R(i,j) = SQRT(F(i)^2 + F(j)^2)   where:
;		 F(i) = i  IF 0 <= i <= n/2
;		      = n-i  IF i > n/2
;
; SIDE EFFECTS:
;	None.
;
; RESTRICTIONS:
;	None.
;
; PROCEDURE:
;	Straightforward.  The computation is done a row at a time.
;
; MODIFICATION HISTORY:
;	Very Old.
; 	SMR, March 27, 1991 - Added the NOZERO keyword to increase efficiency.
;				(Recomended by Wayne Landsman)
;-
on_error,2              ;Return to caller if an error occurs
x=findgen(n)		;Make a row
x = (x < (n-x)) ^ 2	;column squares
a = fltarr(n,n,/NOZERO)	;Make array
for i=0L,n/2 do begin	;Row loop
	y = sqrt(x + i^2) ;Euclidian distance
	a(0,i) = y	;Insert the row
	if i ne 0 then a(0,n-i) = y ;Symmetrical
	endfor
return,a
end



pro loadct, table_number, silent = silent
;+
; NAME:
;	LOADCT
;
; PURPOSE:
;	Load predefined color tables.
;
; CATEGORY:
;	Image display.
;
; CALLING SEQUENCE:
;	LOADCT [, Table]
;
; OPTIONAL INPUTS:
;	Table:	The number of the pre-defined color table to load, from 0 
;		to 15.  If this value is omitted, a menu of the available 
;		tables is printed and the user is prompted to enter a table 
;		number.
;
; KEYWORD PARAMETERS:
;	Silent:	If this keyword is set, the Color Table message is suppressed.
;
; OUTPUTS:
;	No explicit outputs.
;
; COMMON BLOCKS:
;	COLORS:	The IDL color common block.
;
; SIDE EFFECTS:
;	The color tables of the currently-selected device are modified.
;
; RESTRICTIONS:
;	Works from the file: $IDL_DIR/colors.tbl.
;
; PROCEDURE:
;	The file "colors.tbl" is read.  A maximum of 16 entries may 
;	be defined.  The titles of each table are contained in the first 
;	16 * 32 characters/name bytes.  If the currently selected device
;	doesn't have 256 colors, the color data is interpolated from
;	256 colors to the number of colors available.
;
;	The colors loaded into the display are saved in the common
;	block COLORS, as both the current and original color vectors.
;
;	You can use the procedure MODIFYCT to save your own color tables
;	in the file "colors.tbl".
;
;	Interpolation:  If the current device has less than 256 colors,
;	the color table data is interpolated to cover the number of
;	colors in the device.
;
; MODIFICATION HISTORY:
;	Old.  For a widgetized version of this routine, see XLOADCT in the IDL
;	widget library.
;-
common colors, r_orig, g_orig, b_orig, r_curr, g_curr, b_curr

nc = !d.table_size		;# of colors in this device.

if nc eq 0 then message, 'Device has static color tables.  Can''t load.'

on_ioerror, bad
on_error, 2		;Return to caller if error
get_lun, lun

filename = filepath('colors.tbl')

openr,lun, filename, /block
aa=assoc(lun, bytarr(32))	;Get name
if n_params() lt 1 then begin	;Summarize table?
	for i=0,15 do print,i,' - ',string(aa(i))
	read,'Enter table number: ',table_number
	endif

if (table_number ge 16) or (table_number lt 0) then begin
  message, 'Table number must be from 0 to 15.'
  end


if keyword_set(silent) eq 0 then $
	message,'Loading table ' + string(aa(table_number)),/INFO
aa=assoc(lun, bytarr(256),512)	;Read 256 long ints
r_orig = aa(table_number*4)
g_orig = aa(table_number*4+1)
b_orig = aa(table_number*4+2)

if nc ne 256 then begin	;Interpolate
	p = (lindgen(nc) * 255) / (nc-1)
	r_orig = r_orig(p)
	g_orig = g_orig(p)
	b_orig = b_orig(p)
	endif

r_curr = r_orig
g_curr = g_orig
b_curr = b_orig
tvlct,r_orig, g_orig, b_orig
goto, close_file

bad:
  message,'Error reading file: ' + filename + ', ' + !err_string

close_file:
  free_lun,lun
  return
end

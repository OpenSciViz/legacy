FUNCTION HIST_EQUAL ,A,MINV = MINV,MAXV = MAXV, BINSIZE = binsize, TOP = top
;
;+
; NAME:
;	HIST_EQUAL
;
; PURPOSE:
;	Return a histogram-equalized image or vector.
;
; CATEGORY:
;	Z1 - Image processing, spatially invariant.
;
; CALLING SEQUENCE:
;	Result = HIST_EQUAL(A [, MINV = minv] [, MAXV = maxv])
;
; INPUTS:
;	A:	The array to be histogram-equalized.
;
; KEYWORD PARAMETERS:
;	MINV:	The minimum value to consider.  If this keyword is omitted,
;		zero is used.  Input elements less than or equal to MINV are
;		output as zero.  MINV should be greater than or equal to 0.
;
;	MAXV:	The maximum value to consider.  If this keyword is omitted,
;		the maximum element is used.  Input elements greater than or
;		equal to MAXV are output as 255.
;
;     BINSIZE:	Size of bin to use.  If this keyword is omitted, the value 1
;		is used.  This parameter is ignored for byte type data.
;
;	TOP:	The maximum value to scale the output array. If this keyword 
;		is omitted, 255 is used.
;
; OUTPUTS:
;	A histogram-equalized byte array is returned.
;
; COMMON BLOCKS:
;	None.
;
; SIDE EFFECTS:
;	None.
;
; RESTRICTIONS:
;	The output array is always of byte type and is scaled from 0 to TOP. 
;	Floating-point arrays should not have small ranges, (e.g., less than
;	around 255) unless a binsize is specified.
;
; PROCEDURE:
;	The HISTOGRAM function is used to obtain the density distribution of
;	the input array.  The histogram is integrated to obtain the 
;	cumulative density-propability function and finally the lookup 
;	function is used to transform to the output image.
;
; EXAMPLE:
;	Create a sample image using the IDL DIST function and display it by
;	entering:
;
;		IMAGE = DIST(100)
;		TV, DIST
;	
;	Create a histogram-equalized version of the byte array, IMAGE, and
;	display the new version.  Use a minumum input value of 10, a maximum 
;	input value of 200, and limit the top value of the output array to 
;	220.  Enter:
;
;		NEW = HIST_EQUAL(IMAGE, MINV = 10, MAXV = 200, TOP = 220)
;		TV, NEW 
;
; MODIFICATION HISTORY:
;	August, 1982. Written by DMS, RSI.
;	Feb, 1988, Revised for Sun, DMS.
;-
;
	on_error,2                        ;Return to caller if an error occurs
	if n_elements(binsize) le 0 then binsize = 1
	if n_elements(top) eq 0 then top = 255
	S = SIZE(A)		;Type of var?
	If s(s(0)+1) eq 1 then begin	;byte var?
		p = histogram(a,binsize = binsize)
		p(0)=0
		up = n_elements(p)-1	;max value
		if n_elements(minv) ne 0 then p(0:minv) = 0 ;set min?
		if n_elements(maxv) ne 0 then if up gt maxv then p(maxv:*)=0
		for i=1,up do p(i)=p(i)+p(i-1) ;integrate
		P=bytscl(p,top=top)
		RETURN,P(A)			;Transform & return.
	   endif else begin
		if (n_elements(minv) ne 0) and (n_elements(maxv) ne 0) then $
		   p = histogram(a,min=minv,max=maxv, omax=vmax, $
				omin=vmin,bin=binsize) $
		else if n_elements(minv) ne 0 then $
		   p = histogram(a,min=minv, omax=vmax, $
				omin=vmin,bin=binsize) $
		else if n_elements(maxv) ne 0 then $
		   p = histogram(a,max=maxv, omax=vmax, $
				omin=vmin,bin=binsize) $
		else p = histogram(a, omin=vmin, omax = vmax,bin=binsize)

		for i=1L,n_elements(p)-1 do p(i)=p(i)+p(i-1)
		P=bytscl(p, top = top)
		if binsize eq 1 then begin
			if vmin eq 0 then return,p(a) else RETURN,P(A-vmin)
		endif else if binsize eq 1 then return,p(a/binsize) $
		else return, P((a-vmin)/binsize)
	endelse
end


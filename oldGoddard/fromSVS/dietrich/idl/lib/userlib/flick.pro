pro flick,a,b,rate	;Flicker between the two output frames at a given rate.
			; a and b = images scaled from 0 to 255.
			;To terminate, type any key except F or S.
			;Rate = frames per second.
			; Type F to flick faster by a factor of 2
			;  S to flick slower by a factor of 2.
;+
; NAME:
;	FLICK
;
; PURPOSE:
;	Flicker between two output images at a given rate.
;
; CATEGORY:
;	Image display, animation.
;
; CALLING SEQUENCE:
;	FLICK, A, B, Rate
;
; INPUTS:
;	A:	Byte image number 1, scaled from 0 to 255.
;	B:	Byte image number 2, scaled from 0 to 255.
;
; OPTIONAL INPUT PARAMETERS:
;	Rate:	The flicker rate.  The default is 1.0 sec/frame
;
; KEYWORD PARAMETERS:
;	None.
;
; OUTPUTS:
;	No explicit outputs.
;
; COMMON BLOCKS:
;	None.
;
; SIDE EFFECTS:
;	Modifies the display, changes the write mask.
;
; RESTRICTIONS:
;	Works for SunView, but will not work with all X implementations.
;	FLICK usually requires a private color table.
;
; PROCEDURE:
;	Image A is written to the bottom 4 bits of the display.
;	Image B is written to the top 4 bits.
;	Two color tables are created from the current table, one that
;	shows the low 4 bits using 16 of the original colors, and one
;	that shows the high 4 bits.  The color table is changed to
;	switch between images.
;
; MODIFICATION HISTORY:
;	DMS, 3/ 88.
;-
common colors, r_orig, g_orig, b_orig, r_curr, g_curr, b_curr
on_error,2                        ;Return to caller if an error occurs
if n_elements(r_orig) eq 0 then begin	;colors defined?
	r_orig=indgen(256) & g_orig = r_orig & b_orig = r_orig
	endif

p1 = 16 * [[ lindgen(256)/16], [ lindgen(256) and 15]] ;(256,2)

device, set_write=240	;load top 4 bits
tv,a
empty
device, set_write=15	;load bottom 4 bits
tv,b/16b
empty
device,set_write=255	;re-enable all 8 bits
;;;;;;;;;tv, ((a/16) and 15) + (b and 240) ;slow way

if n_elements(rate) eq 0 then rate = 1.0 ;Parameter there?
ichl = 0
while 1 do begin	;loop infinitely over each chl
	p = p1(*,ichl)	;get appropriate table
	tvlct,r_orig(p), g_orig(p), b_orig(p) ;load 4 bit table
	wait,1./rate	;This also empties the graphics buffer
	chr = get_kbrd(0) ;Read character
	case strupcase(chr) of
"F":	rate = rate*2.	;Faster
"S": 	rate = rate/2.	;Slower
"":	ichl = 1 - ichl	;Other image
else:	goto,done
	endcase
	endwhile
;
done:	tvlct, r_orig, g_orig, b_orig
	empty
	return
end

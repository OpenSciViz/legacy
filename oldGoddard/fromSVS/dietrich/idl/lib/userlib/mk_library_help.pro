pro MK_LIBRARY_HELP, source, outfile, verbose=verbose
;+
; NAME:
;	MK_LIBRARY_HELP
;
; PURPOSE:
;	Given a directory or a VMS text library containing IDL user library 
;	procedures, this procedure generates a file in the IDL help format
;	that contains the documentation for those routines that contain
;	a DOC_LIBRARY style documentation template.  The output file is
;	compatible with the IDL "?" command.
;
;	A common problem encountered with the DOC_LIBRARY user library
;	procedure is that the user needs to know what topic to ask for
;	help on before anything is possible.  The XDL widget library
;	procedure is one solution to this "chicken and egg" problem.
;	The MK_LIBRARY_HELP procedure offers another solution.
;
; CATEGORY:
;	Help, documentation.
;
; CALLING SEQUENCE:
;	MK_LIBRARY_HELP, Source, Outfile
;
; INPUTS:
;      SOURCE:	The directory or VMS text library containing the
;		.pro files for which help is desired.  If SOURCE is
;		a VMS text library, it must include the ".TLB" file
;		extension, as this is what MK_LIBRARY_HELP uses to
;		tell the difference.
;
;     OUTFILE:	The name of the output file which will be generated.
;		If you place this file in the help subdirectory of the
;		IDL distribution, and if it has a ".help" extension, then
;		the IDL ? command will find the file and offer it as one
;		of the availible help categories. Note that it uses the
;		name of the file as the name of the topic.
;
; KEYWORDS:
;     VERBOSE:	Normally, MK_LIBRARY_HELP does its work silently.
;		Setting this keyword to a non-zero value causes the procedure
;		to issue informational messages that indicate what it
;		is currently doing.
;
; OUTPUTS:
;	None.
;
; COMMON BLOCKS:
;	None.
;
; SIDE EFFECTS:
;	A help file with the name given by the OUTFILE argument is
;	created.
;
; RESTRICTIONS:
;	If you put the resulting file into the "help" subdirectory of
;	the IDL distribution, it will be overwritten when you install a
;	new release of IDL.  To avoid this problem, keep a backup copy of 
;	your created help files in another directory and move them into 
;	the new distribution.
;
;	Since this routine copies the documentation block from the
;	functions and procedures, subsequent changes will not be
;	reflected in the help file unless you run MK_LIBRARY_HELP
;	again.
;
;	The following rules must be followed in formating the .pro
;	files which are searched.
;		(a) The first line of the documentation block contains
;		    only the characters ";+", starting in column 1.
;		(b) The last line of the documentation block contains
;		    only the characters ";-", starting in column 1.
;		(c) Every other line in the documentation block contains
;		    a ";" in column 1.
;
;	No reformatting of the documentation is done.
;
; MODIFICATION HISTORY:
;	17, April, 1991, Written by AB, RSI.
;-
;
;

  if (not keyword_set(verbose)) then verbose = 0

  ; Open two temporary files, one for the index and one for the text.
  openw, idx_file, filepath('userlib.idx', /TMP), /get_lun, /delete
  openw, txt_file, filepath('userlib.txt', /TMP), /get_lun, /delete

  ; This is the current index into the text file
  txt_pos = 0L

  ; This is the number of files that actually contain a documentation block
  num_used = 0

  if (verbose ne 0) then message, /INFO, 'searching ' + source

  ; Is it a VMS text library?
  vms_tlb = 0				; Assume not
  if (!version.os eq 'vms') then begin
    ; If this is VMS, decide if SOURCE is a directory or a text library.
    if ((strlen(SOURCE) gt 4) and $
        (strupcase(strmid(SOURCE, strlen(SOURCE)-4,4)) eq '.TLB')) then $
      vms_tlb = 1
  endif

  ; If it is a text library, get a list of routines by spawning
  ; a LIB/LIST command. Otherwise, just search for .pro files.
  if (vms_tlb) then begin
    spawn,'LIBRARY/TEXT/LIST ' + SOURCE, FILES
    count = n_elements(files)
    i = 0
    while ((i lt count) and (strlen(files(i)) ne 0)) do i = i + 1
    count = count - i - 1
    if (count gt 0) then files = files(i+1:*)
  endif else begin		  ; Search for .pro files
    if (!version.os eq 'vms') then begin
      tmp = ''
    endif else if (!version.os eq 'DOS') then begin
      tmp = '\'
    endif else begin					; Unix
      tmp = '/'
    endelse
    files = findfile(source + tmp + '*.pro', count=count)
    if (!version.os eq 'vms') then begin
      if (count ne 0) then prefix_length = STRPOS(files(0), ']')+1
    endif else begin
      prefix_length = strlen(source) + 1
    endelse
  endelse

  for i = 0, count-1 do begin
    if (vms_tlb) then begin
      ; We do a separate extract for each potential routine. This is
      ; pretty slow, but easy to implement. This routine is generally
      ; run once in a long while, so I think it's OK
      subject = files(i)
      name = filepath('mklibhelp.scr', /tmp)
      spawn, 'LIBRARY/TEXT/EXTRACT='+subject+'/OUT='+name+' '+SOURCE
      openr, in_file, name, /get_lun, /delete
    endif else begin
      name = files(i)
      if (!version.os eq 'vms') then begin
        subject = STRMID(name, prefix_length, 1000)
        subject = STRMID(subject, 0, STRPOS(subject, '.'))
      endif else begin
        subject = strupcase(strmid(name, prefix_length, $
	  strlen(name) - prefix_length-4))
      endelse
      openr, in_file, name, /get_lun
    endelse

  ; Find the opening line
  tmp = ''
  found = 0
  while (not eof(in_file)) and (not found) do begin
    readf, in_file, tmp
    if (strmid(tmp, 0, 2) eq ';+') then found = 1
  endwhile

  if (found) then begin
    if (verbose ne 0) then message, /INFO, subject
    num_used = num_used + 1
    printf, idx_file, string(subject, txt_pos, format='(A, T16, I0)')
    printf, txt_file, ';+'
    txt_pos = txt_pos + 3
    found = 0
    while (not found) do begin
      readf, in_file, tmp
      if (strmid(tmp, 0, 2) eq ';-') then begin
        found = 1
      endif else begin
	len = strlen(tmp)
	if (len eq 0) then len = 1
	txt_pos = txt_pos + len
        printf, txt_file, strmid(tmp, 1, 1000)
      endelse
    endwhile

    printf, txt_file, ';-'
    txt_pos = txt_pos + 3
    endif

    free_lun, in_file

  endfor

  ; Rewind the scratch files.
  point_lun, idx_file, 0
  point_lun, txt_file, 0

  ; Build the final file from the various pieces.
  openw, final_file, outfile, /stream, /get_lun
  if (verbose ne 0) then message, /INFO, 'building ' + outfile

  ; Under DOS, formatted output ends up with a carriage return linefeed
  ; pair at the end of every record. The resulting file would not be
  ; compatible with Unix. Therefore, we use unformatted output, and
  ; explicity add the linefeed, which has ASCII value 10.
  LF=10B

  writeu, final_file, string(num_used), LF	; The subject count

  while (not eof(idx_file)) do begin		; Copy the index
    readf, idx_file, tmp
    writeu, final_file, tmp, LF
  endwhile
  free_lun, idx_file				; This deletes the index file

  tmp = ' '
  while (not eof(txt_file)) do begin		; Copy the help text
    readf, txt_file, tmp
    writeu, final_file, tmp, LF
  endwhile
  free_lun, txt_file, final_file		; Deletes the text file
  

end

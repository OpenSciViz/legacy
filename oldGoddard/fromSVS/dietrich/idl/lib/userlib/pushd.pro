;+
; NAME:
;	PUSHD
;
; PURPOSE:
;	Push a directory onto the top of the directory stack maintained
;	by the PUSHD and POPD User Library procedures.
;
; CALLING SEQUENCE:
;	PUSHD [, Dir]
;
; OPTIONAL INPUTS:
;	Dir:	The directory to be pushed. If DIR is not present, or is
;		an undefined variable, PUSHD will move to the users home
;		directory.
;
; SIDE EFFECTS:
;	The directory has been pushd onto the directory stack.
;	It will be the next directory used by POPD.
;
; COMMON BLOCKS:
;	DIR_STACK:  Contains the stack.
;
; MODIFICATION HISTORY:
;	17, July, 1989, Written by AB, RSI.
;-
;
;
pro pushd,dir

COMMON DIR_STACK, DEPTH, STACK
on_error, 2		; Return to caller on error

if (n_elements(DEPTH) eq 0) then depth = 0
CD, dir, CURRENT=cwd

if (DEPTH eq 0) then STACK = [CWD] else STACK = [CWD, STACK]
DEPTH = DEPTH + 1
end

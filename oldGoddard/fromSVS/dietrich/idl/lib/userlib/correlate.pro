Function Correlate,x,y
;
;+
; NAME:
;	CORRELATE
;
; PURPOSE:
;	Compute the simple correlation coefficient of two arrays.
;
; CATEGORY:
;	G2 - Correlation and regression analysis.
;
; CALLING SEQUENCE:
;	Result = CORRELATE(X, Y)
;
; INPUTS:
;	X, Y:  	Input arrays that may be of any type except string.
;		Both arrays must have the same number of points.
;
; OUTPUTS:
;	Returns the simple correlation coefficient of X and Y.
;
; COMMON BLOCKS:
;	None.
;
; SIDE EFFECTS:
;	None.
;
; RESTRICTIONS:
;	Input arrays must have the same number of data elements.
;
; PROCEDURE:
;	R  = TOTAL(xx*yy)/SQRT(TOTAL(xx^2)*TOTAL(yy^2))
;
; MODIFICATION HISTORY:
;	DMS, RSI, Sept, 1983.
;-
;
	n = n_elements(x)		;# of data points
        on_error,2                      ;return to caller if an error occurs
	if n ne n_elements(y) then $
	  message, 'Parameters must have the same number of points'

	xmean = total(x)/n		;get means
	ymean = total(y)/n
	xx = x-xmean			;deviations
	yy = y-ymean

	return,total(xx*yy)/sqrt(total(xx^2)*total(yy^2)) ;do it.
end

d


function warp_tri, xo, yo, xi, yi, im_in, OUTPUT_SIZE = output_size
; xo, yo = coordinates of tie points in output image.
; xi, yi = coordinates of tie points in im_in.
;+
; NAME:			WARP_TRI
; PURPOSE:		Warp image using control (tie) points.
; CATEGORY:		Image processing, geometric transformation.
; CALLING SEQUENCE:
;	Result = WARP_TRI(xo, yo, xi, yi, Im_in)
; INPUTS:
;	(xo,yo) = Vectors containing the locations of the tie points
;		in the output image.
;	(xi,yi) = Vectors containing the location of the tie points
;		in the input image (Im_in).  xi,yi must be the same
;		length as xo,y0.
;	Im_in = image to be warped. May be any type of data.
; KEYWORD PARAMETERS:
;	Output_size = 2 element vector containing the size of the
;		output image.  If omitted, the output image is the
;		same size as Im_in.
; OUTPUTS:
;	Function result = Image with with the geometric correction
;	applied.  Points at locations (xi,yi) are shifted to (xo,yo).
; COMMON BLOCKS:
;	None.
; SIDE EFFECTS:
;	None.
; RESTRICTIONS:
; PROCEDURE:
;	The irregular grid defined by (xo,yo) is triangulated
;	using TRIANGULATE.  Then the surfaces defined by (xo,yo,xi)
;	and (xo,yo,yi) are interpolated using TRIGRID to get
;	the locations in the input image of each pixel in the output
;	image.  Finally, INTERPOLATE is called to obtain the result.
;	Linear interpolation is used.
; MODIFICATION HISTORY:
;	DMS, Jan, 1992.
;-

s = SIZE(im_in)
if s(0) ne 2 then MESSAGE, 'Warp_tri - Im_in param must be 2D'

TRIANGULATE, xo, yo, tr

if n_elements(output_size) ge 2 then begin
	nx = output_size(0)
	ny = output_size(1)
endif else begin
	nx = s(1)
	ny = s(2)
endelse

return, INTERPOLATE(im_in, $
  TRIGRID(xo, yo, xi, tr, [1,1], [0,0, nx-1, ny-1]), $
  TRIGRID(xo, yo, yi, tr, [1,1], [0,0, nx-1, ny-1]))
end


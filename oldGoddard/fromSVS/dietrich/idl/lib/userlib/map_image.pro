
function  map_image, image_orig, startx, starty,        $
                     latmin = latmin,   latmax = latmax,  $
                     lonmin1 = lonmin1,   lonmax1 = lonmax1,  $
                     latsamp = latsamp, lonsamp = lonsamp, $
                     simple  = simple,   bilinear  = bilin
;+
;NAME:
;     map_image
;PURPOSE:
;       This function returns the image_orig image warped to fit
;       the map specified by the system variable !Map. Image_orig
;       must be centered at 0.
;Category:
;        Mapping
;Calling Sequence:
;                map_image, image_orig
;INPUT:
;      image_orig--- A two-dimensional array representing geographical
;                    image to be overlayed on map.
;KEYWORDS:
;        latmin --- the latitude corresponding to the first row of image_orig.
;                   The default value is -90.
;        latmax --- the latitude corresponding to last row of image_orig. The
;                   default is  90. -90<= latmin < latmax <= 90
;        lonmin --- the longitude corresponding to the first column of
;                   image_orig. The default value is -180.
;        lonmax --- the longitude corresponding to the last column
;                   of image_orig. The default is  180. Lonmax must
;                   exceed lonmin. For images crossing the international
;                   dateline, either lonmin will be less than -180 or
;                   lonmax greater than 180.  
;        latsamp --- the number of evenly spaced latitude values to
;                    use for the warping. The default is 10. For best
;                    results, set latsamp to the row dimension of
;                    image_orig.
;        lonsamp---  the number of evenly spaced longitude values to
;                    use for the warping. The default is 10. For best
;                    results set lonsamp equal to 
;                        360./(lonmin - latmin) *
;                                  (column dimension of image_orig).
;                   Smaller values of lonsamp will produce faster but
;                   possibly not as pleasing results.
;        simple --- A flag to signal the use of warping algorithm
;                   more appropriate and efficient for simple cylindrical
;                   maps
;        bilinear--- A flag, if set, to request bilinear interpolation. The
;                    default is nearest neighbor.
; Optional Output Parameters:
;          xstart --- the  x coordinate where the left edge of the image
;                     should be placed on the screen
;          ystart --- the y coordinate where th bottom edge of the image
;                     should be placed on the screen. 

;Output:
;      The warped image is returned.
;-

ON_ERROR,2

if (!x.type NE 2) THEN  $        ;Need Mapping Coordinates
   message,"Current window must have map coordinates"
   
 if N_ELEMENTS(latmin) EQ 0 THEN latmin = -90.
 if N_ELEMENTS(lonmin1) EQ 0 THEN lonmin1 = -180.
 if N_ELEMENTS(latmax) EQ 0 THEN latmax = 90.
 if N_ELEMENTS(lonmax1) EQ 0 THEN lonmax1 = 180.


 latmin = float(latmin) & lonmin = float(lonmin1) & latmax = float(latmax)
 lonmax = float(lonmax1)

 if latmin lt -90 or latmin ge latmax or latmax gt 90 THEN $
   message, "Latitude bounds are inconsistent."
 if lonmin ge lonmax THEN $
   message, "Longitude bounds are inconsistent."




 s = size(image_orig)
  if s(0) ne 2 THEN $
   message, " Image must be a two- dimensional array."
   
 s1 = s(1)           ; # of columns
 s2 = s(2)           ; # of rows

if s(1) le 1 or s(2) le 1 THEN $
    message, 'Each dimension must be greater than 1."

deglon = abs((lonmax - lonmin)/(s(1)-1.))  ;# of degrees between columns
deglat =abs( (latmax - latmin)/(s(2)-1.))  ;# of degrees between rows

if not keyword_set(latsamp) THEN latsamp = 10 
if not keyword_set(Lonsamp) THEN lonsamp = 10 

 
s1 = fix(360./deglon + 1.5)
im     = bytarr(s1, s2)      ;extend image from -180 to 180 degrees

doit=1                       ; doit = flag indicating images doesn't cross
                             ; dateline

; Extend and adjust image does cross international dateline.


if lonmin lt -180  or lonmax gt 180 THEN $ 
   if lonmin lt -180 THEN $ 
      if lonmax lt -180 THEN BEGIN   ; doesn't really cross dateline
         lonmin = lonmin + 360
         lonmax = lonmax + 360
      ENDIF ELSE BEGIN               ; crosses dateline
        temp = (540 + lonmin)/deglon
        temp2 = (-180-lonmin) /deglon
        im(temp,0) = image_orig(0:temp2,*)
        im(0,0) = image_orig(temp2:*,*)
        lonmin = fix(temp2) * deglon
        lonmax =  360 +lonmin
        doit = 0
     ENDELSE ELSE $         ; doesn't really cross dateline
         if lonmin gt 180 THEN BEGIN
         lonmin = lonmin - 360
         lonmax = lonmax - 360
      ENDIF ELSE BEGIN      ; crosses dateline
        temp =  (lonmin + 180)/deglon 
        temp2 = (180-lonmin)/deglon 
        im(temp,0) = image_orig(0:temp2,*)
        im(0,0) = image_orig(temp2:*,*)
        lonmin =  fix(temp2) * deglon
        lonmax =  360 + lonmin
        doit = 0
     ENDELSE

;extend image that doesn't cross dateline
if (doit eq 1) THEN $     
im((180 + lonmin)/deglon,0) = image_orig

s = size(im)
s1 = s(1)
s2 = s(2)

lnmin = !map.out(2)
lnmax = !map.out(3)

p0lon1 = (lnmin + lnmax)/2.    ; p0lon = center of map
p0lon  = p0lon1
if p0lon1 gt 180  THEN p0lon =  p0lon1 - 360  ;adjust for rotation
if p0lon1 lt -180 THEN p0lon =  360 + p0lon1
lonmin2 = -180


;We must handle case of off-center longitude separately if it splits image
 if p0lon1 ne 0 THEN Begin
    c  = p0lon/deglon
    if doit eq 5 THEN $
      if c>0 THEN c = c+.5 else c = c -.5

    im(0,0) = shift(im(0:s1-2,*),-c,0)
    im(s1-1,*) = im(0,*)
    lonmin2 = lonmin2+ fix(c) * deglon
    endif      

; Find limits of map from !Map
  ltmin = !map.out(4) > latmin 
  ltmax = !map.out(5) < latmax

; compute starting index in final array
  lnmini = lnmin - lonmin2
  if lnmini gt 180   THEN $
     lnmini = lnmini - 360 $
  else if lnmini lt -180 THEN $
     lnmini = lnmini +360
  
  lnmini = fix((lnmini)/deglon)>0   ;starting col index
  
  lnmaxi = lnmini + fix((lnmax-lnmin)/deglon)    ;last col index
  ltmini = fix((ltmin -latmin)/deglat)     ;starting  row index
  ltmaxi = fix((ltmax - latmin)/deglat) < (s2 - 1)   ;last row index

;extract a subimage to cover area on the map

    im1 = im(lnmini:lnmaxi, ltmini:ltmaxi) 
    

  
 ; Adjust image if edge of image after shifting is to the right of map edge.
 ; That is , we must add a column to fill in gap on the left between image
 ; and map edge. 

 if lonmin2 gt lnmin THEN BEGIN
   adjust = 1
   if lnmini gt 0 THEN $
     im = [im(lnmini-1,*),im1] $
  else $
      im = [im(s1-2,*),im1] 
  endif else begin
       im = im1
       adjust=0
  ENDELSE
 
  s = size(im1)
  s1 = s(1)
  s2 = s(2)

;compute longitude and latitude values corresponding to rows and
;columns of im

  xorig = findgen(s1 - adjust)  
  yorig = findgen(s2)
 
  lons = (xorig + lnmini)*deglon + lonmin2 >lnmin<lnmax; vector of longitudes
  lats = (yorig + ltmini)*deglat + latmin    ; vector of latitudes
  lons = [lnmin,lons,lnmax]


  if keyword_set(simple) THEN BEGIN
   interp = 1
   xmargin = [1,-1]
   ymargin = [1,-2]
   xm1 = xmargin * !d.x_ch_size + [0,!d.x_size]
   ym1 = ymargin * !d.y_ch_size + [0,!d.y_size]
   x0 = min(xm1) & y0 = min(ym1)
   sx = max(xm1) -x0+1 & sy = max(ym1) -y0 + 1
   s = size(im)
       
   xorig = [0., s(1)-1, 0, s(1)-1]
   yorig = [0., 0., s(2)-1, s(2)-1]
   xm1   = [xm1,xm1]
   ym1  =  [ym1(0),ym1(0),ym1(1),ym1(1)]
   startx = x0
   starty = y0
   polywarp,  xorig,yorig,xm1-x0, ym1-y0, 1, kx, ky
   return ,poly_2d(im, kx, ky, 0, sx, sy, miss=0)
 ENDIF

; Determine sampling values xnew and ynew --- device coordinates in the
; graphic window of the map

 latsamp = latsamp <(s2-1)
 lonsamp = lonsamp <(s1-1)

;xorig records column indices in im for sampled longitudes
 xorig = [0,1 + findgen(lonsamp + 1) * ((s1-1.)/lonsamp),s1 + 1]      $
                  # replicate(1.,latsamp + 2)

;likewise, yorig records row indices in im for sampled longitudes
 yorig = replicate(1.,lonsamp + 3) #   $
         [findgen(latsamp +1) * ((s(2)-1.)/(latsamp)), s(2)-1]

 xnew  = convert_coord ( lons(xorig),lats(yorig), /To_Device);
 ynew = transpose(xnew(1,*))
 xnew = transpose(xnew(0,*)) 
 ind  = where(xnew lt 1.e12 and ynew lt 1.e12 and xnew ge 0 and ynew ge 0, $
             count);
;adjust xorig in case new column was added to fill gap.

xorig = [0,findgen(lonsamp + 1) * ((s1-1.)/lonsamp) + adjust, s1-1 +adjust]      $
                  # replicate(1.,latsamp + 2)
 
 if count ne 0 then begin ;remove bad values
  xnew = xnew(ind)
  ynew = ynew(ind)
 endif
 reps = 1
 triangulate,xnew,ynew, tr, repeats= reps

 s = size(ind)
 xmargin = [1,1] * [1, -1]		;X margin in char units, rt is neg
 ymargin = [1,2] * [1, -1]

 xm   = xmargin * !d.x_ch_size + [0,!d.x_size] ; + min(xnew)
 ym   = ymargin * !d.y_ch_size + [0,!d.y_size] ; + min(ynew)

 startx = xm(0)
 starty = ym(0)
 bounds = [xm(0), ym(0),  xm(1),  ym(1)]
 
 gry = trigrid(xnew,ynew,reform(yorig(ind),s(1)),tr,   $
               [1,1],bounds, missing = s2)

  grx = trigrid(xnew,ynew,reform(xorig(ind),s(1)),tr,    $
               [1,1],bounds, missing = s1)

 im1 = fltarr(s1+1,s2+1)
 im1(0,0) = im
 if KEYWORD_SET(bilin) eq 0 THEN $
  im = im1(long(grx), long(gry)) $
 else im = interpolate(im1,grx,gry)

 return, im


 end
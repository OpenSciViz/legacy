;  Copyright (c) 1991, Research Systems Inc.  All rights
;  reserved. Unauthorized reproduction prohibited.


function gammaln,a1
 
a = a1
b = a + 4.5
b = b - (a -0.5)*alog(b)
mul = [76.18009173,  -86.50532033, 24.01409822,-1.231739516,0.120858003e-2,$
     -0.536382e-5]
Div = 1/(fINDGEN(6) +a)
del = Total(Div *mul) + 1
return, -b + alog(2.50662827465 * del )
END

function gamma1,a

return, gammaln(a)
end


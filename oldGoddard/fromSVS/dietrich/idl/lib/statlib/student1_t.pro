;  Copyright (c) 1991, Research Systems Inc.  All rights
;  reserved. Unauthorized reproduction prohibited.


function student1_t ,X, DF
;+
;NAME:
;     student1_t
;PURPOSE: 
;       Student1_T returns the probability that an
;       observed value from the Student's t distribution 
;       with DF degrees of freedom is less than |X|.
;INPUT:
;     X = cutoff
;     DF = degrees of freedom
;OUTOUT:
;       The probability of |X| or something smaller.
;CATEGORY:STATISTICS
;CALLING SEQUENCE:
;     PROB  = STUDENT1_T(X,DF)
;-


    if DF lt 0 THEN BEGIN
       print,'student1_t - degrees of freedom'
       print, ' must be larger than 0.'
       return,-1
    ENDIF
    return, 1 - .5*betai (DF/(DF + X^2), DF/2.0, .5) 
    end 


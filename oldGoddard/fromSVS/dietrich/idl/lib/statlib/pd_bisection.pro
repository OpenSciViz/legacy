;  Copyright (c) 1991, Research Systems Inc.  All rights
;  reserved. Unauthorized reproduction prohibited.



function pd_bisection,a,funct, U, L,del
; pd_bisections uses a simple bisection technique on probabilty
; distribution funct to find the cutoff point x so that the probabilty of
; an observation from the given distribution less than x is a(0). U and L are
; respectively upper and lower limits for x. a(1) and a(2) are df`s if
; appropriate. Funct is a string.

 SA = size(a)

 if (N_Elements(del) EQ 0) then del =.000001

 if SA(0) GT 0 then p = a(0) else p=a
 if( p LT 0 or p GT 1) then return, -1

 Up = U
 Low = L
 Mid = L + (U-L)*p

 if ( SA(1) EQ 1) then  FunStr= ('z = ' + funct + '(Mid)')  $
  else if (SA(1) EQ 2) then  FunStr=('z = ' + funct + '(Mid,a(1))')  $
  else if (SA(1) EQ 3) then FunStr = ('z=' + funct + '(Mid,a(1),a(2))') $
  else return,-1
 count =1
 if execute (FunStr) EQ 0 THEN return, -1
  
  while (abs(up - low )) GT del*mid and count lt 100 DO BEGIN
   if z GT p then  Up = Mid else Low = Mid
   Mid = (Up + Low) /2
  if execute( FunStr) EQ 0 THEN return,-1
  count = count + 1
 endwhile
 return,Mid
 END


  
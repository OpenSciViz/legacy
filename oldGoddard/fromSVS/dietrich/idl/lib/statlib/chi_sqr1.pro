;  Copyright (c) 1991, Research Systems Inc.  All rights
;  reserved. Unauthorized reproduction prohibited.


function chi_sqr1,X,DF
;+
;
;NAME:
;     chi_sqr1
;PURPOSE: 
;     Chi_sqr1 returns the probabilty of observing X or
;     something smaller from a chi square distribution
;     with DF degrees of freedom.

;CATEGORY:STATISTICS
;CALLING SEQUENCE:
;     PROB = CHI_SQR(X,DF)
;INPUTS:
;     X = the cut off point
;     DF = the degrees of freedom
;-

if X le 0 THEN return,0 ELSE BEGIN
  gres = rsi_gammai(DF/2.0,x/2.0)
  if gres NE -1 then return, gres else return,-1
  END
end

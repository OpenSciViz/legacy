pro stacked_bar_demo
;+
;
;   Procedure to demonstrate the stacking of bars using BAR_PLOT.
;
;-
seed=72367843l					;Initialize seed value
data=fix(10*randomu(seed,5,8)+4.5)		;Create random data
ymax=0						;Init. max Y-axis value
for i=0,4 do ymax=ymax>total(data(i,*))		;Compute max Y value
!y.range=[0,ymax]				;Set up scale
base=intarr(8)					;Base increased each time
barnames=['1982','1983','1984','1985','1986']   ;Example bar names
loadct,15					;Use standard table 15
rcolors=fix(255*randomu(seed,8))		;Randomize color indices
rcolors=rcolors(sort(rcolors))			;Sort to increase
for i=0,7 do begin				;Loop for each row
   bar_plot,data(*,i)+base,colors=intarr(8)+rcolors(i),baselines=base, $
     barwidth=0.75,barspace=0.25,over=(i gt 0),barnames=barnames, $
     title='Stacked vertical bars (simulated data)', $
     xtitle='Year-to-year variability',ytitle='8 segments per bar'
   base=base+data(*,i)				;Base accumulates
endfor
!y.range=[0,0]					;Set Y-axis to auto-scaling
return
end
;
;
;
;
;
;
;
;
pro benchmarks_bar_demo
;+
;
;   Procedure to demonstrate the ability to rotate bars, outline them, and
;      create multiple groupings on the same graph, using BAR_PLOT.
;
;-
sys_names=['Sun 3/80, cg8 24 bit, OS4','Sun 3/110, OS4','Sun SPARC 1+']
sys_names=[sys_names,'VAXStation 3100']
sys_names=[sys_names,'MicroVAX II, VMS 5.1','MicroVAX II, Ultrix 3.0']
sys_names=[sys_names,'DecStation 3100','Mips RS2030','HP 9000, Model 375']
sys_names=[sys_names,'Silicon Graphics 4D/25','IBM 6000, Model 325']
sys_times=[282,391,66,235,637,616,150,246,163,51,74]		;Comp. times
sys_geo=[6.03,8.19,1.81,5.13,14.4,13.9,4.00,3.67,4.14,1.38,1.81];Geo. Avg
gra_times=[89.3,32.0,7.0,24.3,39.9,58.1,17.6,14.6,20.8,19.4,8.7];Graphics times
gra_geo=[21.7,7.81,1.64,3.71,6.57,8.27,3.23,2.62,3.37,2.44,1.34];Geo. Avg
sort_order=reverse(sort(sys_geo))				;Decr. speed
sys_names=sys_names(sort_order)					;Sort to decr.
num_sys=n_elements(sys_names)					;# of systems
sys_names(0)='!6'+sys_names(0)					;Switch font
sys_names(num_sys-1)=sys_names(num_sys-1)+'!3'                  ;Switch back
sys_times=sys_times(sort_order)					;Match order
sys_geo=sys_geo(sort_order)					;Match order
gra_times=gra_times(sort_order)					;Match order
gra_geo=gra_geo(sort_order)					;Match order
!x.range=[0,1.05*(max(sys_geo)>max(gra_geo))]			;X-axis scale
old_xmargin=!x.margin						;Save old
!x.margin=[25,3]						;Label space
loadct,15							;Standard table
bar_plot,sys_geo,barnames=sys_names,baserange=0.4,/rotate,/outline, $
  xtitle='!6Geometric Mean Times (seconds)!3', $
  title='!6IDL (v. 2.0.10) System Timings!3'
bar_plot,gra_geo,barnames=sys_names,baserange=0.4,baroffset=(5./4.)*num_sys, $
  /outline,/over,/rotate
xyouts,0.67,0.35,'!6Computational!3',/normal			;Annotate lower
xyouts,0.67,0.80,'!6Graphical!3',/normal			;Annotate upper
!x.range=[0,0]							;Reset autoscl.
!x.margin=old_xmargin						;Reset margin
return
end
;
;
;
;
;
;
;
;
pro demo_bar
;+
; NAME:
;	DEMO_BAR
; PURPOSE:
;	To demonstrate capabilities and use of the PLOT_BAR procedure.
; CATEGORY:
; CALLING SEQUENCE:
;	Three procedures are provided: DEMO_BAR, STACKED_BAR_DEMO, and
;	 BENCHMARKS_BAR_DEMO.  DEMO_BAR calls the other two procedures,
;	 which may be called individually if desired.
; INPUTS:
; OPTIONAL INPUT PARAMETERS:
; KEYWORD PARAMETERS:
; OUTPUTS:
;	STACKED_BAR_DEMO creates graphs showing vertically stacked bars.
;	 BENCHMARKS_BAR_DEMO creates a graph which displays IDL timing
;	 results for many different computers.  Times are for both
;	 computational and graphics tests.
; OPTIONAL OUTPUT PARAMETERS:
; COMMON BLOCKS:
; SIDE EFFECTS:
; RESTRICTIONS:
;	For use on a display which supports windowing and color maps.
;
;	Bars will have the best rendition on a color monitor, since some shades
;	 of gray may be too dark to see well.
; PROCEDURE:
; MODIFICATION HISTORY:
;	August 1990, T.J. Armitage, RSI, initial programming.
;-
window,0
stacked_bar_demo
window,2
benchmarks_bar_demo
return
end
